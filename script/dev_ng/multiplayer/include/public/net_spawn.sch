USING "globals.sch"

USING "net_include.sch"
USING "NET_SCORING_Common.sch"

USING "net_mission.sch"
USING "net_blips.sch"
 
USING "commands_zone.sch"
USING "commands_network.sch"
USING "net_spectator_cam_common.sch"
USING "net_rank_rewards.sch"
USING "net_nodes.sch"				// KGM 20/9/11: Added by Keith - extracts some useful functions from this header to make them publically available
USING "net_ambience.sch"
USING "Screens_header.sch"
USING "net_cutscene.sch"
USING "Transition_Controller.sch"
USING "net_big_message.sch"
USING "net_kill_strip.sch"
USING "Net_stat_tracking.sch"
USING "net_common_functions.sch"
USING "PM_MissionCreator_Public.sch"
//USING "vehicle_public.sch"
USING "net_crew_vehicles_spawning.sch"
USING "net_crew_updates.sch"
USING "mp_skycam.sch"
USING "net_realty_details.sch"
USING "net_spawn_vehicle.sch"
USING "net_spawn_private.sch"
USING "net_spawn_activities.sch"
USING "net_kill_cam.sch"
USING "tattoo_private.sch"
USING "net_spawn_group_data.sch"
#IF IS_DEBUG_BUILD
USING "net_debug.sch"
#ENDIF

#IF USE_TU_CHANGES
USING "net_cloud_reward_system.sch"
#ENDIF

USING "freemode_events_header_private.sch"

USING "net_private_yacht.sch"

USING "net_spawn_debug.sch"

#IF FEATURE_FREEMODE_ARCADE
USING "arcade.sch"
#ENDIF

#IF IS_DEBUG_BUILD
FUNC STRING GET_MANUAL_RESPAWN_STATE_STRING(MANUAL_RESPAWN_STATE eManualRespawnState)
	SWITCH eManualRespawnState
		CASE MRS_NULL
			RETURN("MRS_NULL")
		BREAK
		CASE MRS_EXIT_SPECTATOR_AFTERLIFE				
			RETURN("MRS_EXIT_SPECTATOR_AFTERLIFE")
		BREAK
		CASE MRS_RESPAWN_PLAYER_HIDDEN				
			RETURN("MRS_RESPAWN_PLAYER_HIDDEN")
		BREAK
		CASE MRS_READY_FOR_PLACEMENT  
			RETURN("MRS_READY_FOR_PLACEMENT")
		BREAK
		CASE MRS_FINISH_RESPAWN
			RETURN("MRS_FINISH_RESPAWN")
		BREAK
	ENDSWITCH
	RETURN("UNKNOWN")
ENDFUNC
#ENDIF

//PROC SET_USE_SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION(BOOL bSet)
//	IF (bSet)
//		IF NOT (g_SpawnData.MissionSpawnDetails.bUseNearCurrentCoordsForSpawnArea)
//			PRINTLN("[spawning] SET_USE_SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION - set to TRUE")		
//			g_SpawnData.MissionSpawnDetails.bUseNearCurrentCoordsForSpawnArea = TRUE
//		ENDIF
//	ELSE
//		IF (g_SpawnData.MissionSpawnDetails.bUseNearCurrentCoordsForSpawnArea)
//			PRINTLN("[spawning] SET_USE_SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION - set to FALSE")		
//			g_SpawnData.MissionSpawnDetails.bUseNearCurrentCoordsForSpawnArea = FALSE
//		ENDIF
//	ENDIF
//ENDPROC

FUNC MANUAL_RESPAWN_STATE GET_MANUAL_RESPAWN_STATE()
	RETURN g_SpawnData.eManualRespawnState
ENDFUNC

PROC SET_MANUAL_RESPAWN_STATE(MANUAL_RESPAWN_STATE eManualRespawnState)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] SET_MANUAL_RESPAWN_STATE(") 
		NET_PRINT(GET_MANUAL_RESPAWN_STATE_STRING(eManualRespawnState))
		NET_PRINT(") - called by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()	
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	g_SpawnData.eManualRespawnState = eManualRespawnState
	
ENDPROC

PROC DISABLE_SPAWN_FLASHING_FOR_REMOTE_PLAYER(PLAYER_INDEX RemotePlayerID)
	IF NOT (RemotePlayerID = PLAYER_ID())
		NETWORK_DISABLE_INVINCIBLE_FLASHING(RemotePlayerID, TRUE)
		SET_BIT(g_SpawnData.iBS_FlashingDisabled, NATIVE_TO_INT(RemotePlayerID))
		g_SpawnData.timerFlashingDisabled[NATIVE_TO_INT(RemotePlayerID)] = GET_NETWORK_TIME()
	ENDIF
ENDPROC

PROC REENABLE_INVINCIBLE_FLASHING_FOR_REMOTE_PLAYER(PLAYER_INDEX RemotePlayerID)
	IF NOT (RemotePlayerID = PLAYER_ID())
		NETWORK_DISABLE_INVINCIBLE_FLASHING(RemotePlayerID, FALSE)
		CLEAR_BIT(g_SpawnData.iBS_FlashingDisabled, NATIVE_TO_INT(RemotePlayerID))
	ENDIF
ENDPROC

PROC REENABLE_INVINCIBLE_FLASHING_FOR_ALL_REMOTE_PLAYERS()
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		REENABLE_INVINCIBLE_FLASHING_FOR_REMOTE_PLAYER(INT_TO_NATIVE(PLAYER_INDEX, iPlayer))
	ENDREPEAT
ENDPROC

FUNC BOOL SHOULD_REENABLE_FLASHING_FOR_REMOTE_PLAYER(PLAYER_INDEX RemotePlayerID)
	IF NOT (RemotePlayerID = PLAYER_ID())
		IF IS_BIT_SET(g_SpawnData.iBS_FlashingDisabled, NATIVE_TO_INT(RemotePlayerID))
			IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.timerFlashingDisabled[NATIVE_TO_INT(RemotePlayerID)]) > 60000
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC MANAGE_SPAWN_FLASHING_FOR_REMOTE_PLAYER(PLAYER_INDEX RemotePlayerID)
	IF SHOULD_REENABLE_FLASHING_FOR_REMOTE_PLAYER(RemotePlayerID)
		REENABLE_INVINCIBLE_FLASHING_FOR_REMOTE_PLAYER(RemotePlayerID)
	ENDIF
ENDPROC

// **********************************************************************************************************************************
// **********************************************************************************************************************************
//											PRIVATE FUNCTIONS
// **********************************************************************************************************************************
// **********************************************************************************************************************************






FUNC BOOL IS_LOCAL_PLAYER_ON_DEATHMATCH()
	IF IS_PLAYER_ON_ANY_DEATHMATCH(PLAYER_ID())
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



#IF IS_DEBUG_BUILD
//PURPOSE: Sets the players weapon damage modifier widget to match the server when they first join
PROC Initialise_MP_Damage_Modifiers()

	
	lw_fPlayerWeaponDamageModifier = GlobalServerBD.lw_fServerWeaponDamageModifier
	lw_fPlayerVehicleDamageModifier = GlobalServerBD.lw_fServerVehicleDamageModifier
	NET_PRINT(" - Initialise_MP_Damage_Modifiers - GlobalServerBD.lw_fServerWeaponDamageModifier = ") NET_PRINT_FLOAT(GlobalServerBD.lw_fServerWeaponDamageModifier) NET_NL()
	NET_PRINT(" - Initialise_MP_Damage_Modifiers - lw_fPlayerWeaponDamageModifier = ") NET_PRINT_FLOAT(lw_fPlayerWeaponDamageModifier) NET_NL()
	NET_PRINT(" - Initialise_MP_Damage_Modifiers - DONE ")  NET_NL()
ENDPROC
#ENDIF

//PURPOSE: Returns the Player Weapon Damage Modifier based on their current mode.
FUNC FLOAT GET_PLAYER_WEAPON_DAMAGE_MODIFIER()

	//NET_PRINT(" - GET_PLAYER_WEAPON_DAMAGE_MODIFIER - FREEMODE_PLAYER_WEAPON_DAMAGE = ") NET_PRINT_FLOAT(FREEMODE_PLAYER_WEAPON_DAMAGE) NET_NL()
	RETURN FREEMODE_PLAYER_WEAPON_DAMAGE

ENDFUNC

//PURPOSE: Returns the Player Vehicle Damage Modifier based on their current mode.
FUNC FLOAT GET_PLAYER_VEHICLE_DAMAGE_MODIFIER()
	RETURN FREEMODE_PLAYER_VEHICLE_DAMAGE
ENDFUNC

//PURPOSE: Sets the players Weapon and Vehicle Damage Modifiers
PROC SET_PLAYER_DAMAGE_MODIFIERS()

	IF (IS_ON_DEATHMATCH_GLOBAL_SET() AND g_e_Chosen_Perk = KS_PP_TRIANGLE_QUAD)
		PRINTLN(" - CS_DMG], SET_PLAYER_DAMAGE_MODIFIERS - lw_fPlayerWeaponDamageModifier bypass as (IS_ON_DEATHMATCH_GLOBAL_SET() AND g_e_Chosen_Perk = KS_PP_TRIANGLE_QUAD) ")
		EXIT
	ENDIF
	PRINTLN("CS_DMG], SET_PLAYER_DAMAGE_MODIFIERS")
	SET_PLAYER_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), GET_PLAYER_WEAPON_DAMAGE_MODIFIER())
	SET_PLAYER_VEHICLE_DAMAGE_MODIFIER(PLAYER_ID(), GET_PLAYER_VEHICLE_DAMAGE_MODIFIER())
	
	// Widgets for changing the Damage Modifiers
	#IF IS_DEBUG_BUILD

		SET_PLAYER_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), lw_fPlayerWeaponDamageModifier)
		NET_PRINT(" - SET_PLAYER_DAMAGE_MODIFIERS - lw_fPlayerWeaponDamageModifier = ") NET_PRINT_FLOAT(lw_fPlayerWeaponDamageModifier) NET_NL()
		SET_PLAYER_VEHICLE_DAMAGE_MODIFIER(PLAYER_ID(), lw_fPlayerVehicleDamageModifier)
		NET_PRINT(" - SET_PLAYER_DAMAGE_MODIFIERS - lw_fPlayerVehicleDamageModifier = ") NET_PRINT_FLOAT(lw_fPlayerVehicleDamageModifier) NET_NL()

	#ENDIF
ENDPROC

PROC FORCE_RESPAWN(BOOL bForceThroughJoining=FALSE)
	
	#IF IS_DEBUG_BUILD 
	NET_PRINT("[spawning] FORCE_RESPAWN() called") NET_NL()
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	g_SpawnData.bForceRespawn = TRUE
	g_SpawnData.bForceThroughJoining = bForceThroughJoining
		
ENDPROC


PROC ResetWarpRequestForPlayer(INT iPlayer)
	PRINTLN("[spawning] ResetWarpRequestForPlayer - called for player ", iPlayer) 
	GlobalServerBD.g_vWarpRequest[iPlayer] = <<0.0, 0.0, 0.0>> // reset these each frame	
	GlobalServerBD.bWarpRequestTimeIntialised[iPlayer] = FALSE
	GlobalServerBD.bHasUpdatedWarpRequestTime[iPlayer] = FALSE
ENDPROC

PROC SERVER_MAINTAIN_WARP_REQUESTS() 
	
	PLAYER_INDEX PlayerID
	INT iTimeDiff
	
	// ************ PLAYER WARPING *************
	
	// NOTE: for the warp requests treat the iParticipant as player int
	IF (GlobalServerBD.bWarpRequestTimeIntialised[iStaggeredWarpRequestPlayer])
	
		// has someone just requested to warp to this coord?
		iTimeDiff = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), GlobalServerBD.g_iWarpRequestTime[iStaggeredWarpRequestPlayer])
		
		IF (iTimeDiff < 40000)
			PlayerId = INT_TO_NATIVE(PLAYER_INDEX, iStaggeredWarpRequestPlayer)
			IF IS_NET_PLAYER_OK(PlayerID, FALSE, FALSE)
			
				// check player is alive
				IF IS_NET_PLAYER_OK(PlayerID, TRUE, FALSE)
					
					// check player is not in the process of respawning
					IF NOT IS_PLAYER_RESPAWNING(PlayerID)
					
						IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = 0 // player has finished their warp
						
						
							// because respawning and warping can take a long time we want to reset the warp request time from after they have finished, 
							// so that the time below doesnt get hit immediately. 2573829
							IF NOT (GlobalServerBD.bHasUpdatedWarpRequestTime[iStaggeredWarpRequestPlayer])
								NET_PRINT("[spawning] SERVER_MAINTAIN_WARP_REQUESTS - updating warp request time.") 
								NET_PRINT(" PlayerID = ") NET_PRINT_INT(NATIVE_TO_INT(PlayerId)) 						
								NET_NL()	
								GlobalServerBD.g_iWarpRequestTime[iStaggeredWarpRequestPlayer] = GET_NETWORK_TIME()
								iTimeDiff = 0
								GlobalServerBD.bHasUpdatedWarpRequestTime[iStaggeredWarpRequestPlayer] = TRUE
							ENDIF
								
						
							IF (iTimeDiff > 10000) // don't clear the warp request for at least 10 seconds
								IF ((PlayerID = PLAYER_ID()) AND NOT (IS_PLAYER_TELEPORT_ACTIVE()))
								OR (PlayerID != PLAYER_ID())	
									ResetWarpRequestForPlayer(iStaggeredWarpRequestPlayer)
									NET_PRINT("[spawning] SERVER_MAINTAIN_WARP_REQUESTS - clearing warp request, player has arrived.") 
									NET_PRINT(" PlayerID = ") NET_PRINT_INT(NATIVE_TO_INT(PlayerId))
									NET_NL()
								ELSE
									NET_PRINT("[spawning] SERVER_MAINTAIN_WARP_REQUESTS - waiting for player teleport to complete.") 
									NET_PRINT(" PlayerID = ") NET_PRINT_INT(NATIVE_TO_INT(PlayerId))
									NET_NL()
								ENDIF
							ELSE
								NET_PRINT("[spawning] SERVER_MAINTAIN_WARP_REQUESTS - waiting for player teleport to commence.") 
								NET_PRINT(" PlayerID = ") NET_PRINT_INT(NATIVE_TO_INT(PlayerId)) NET_PRINT(", iTimeDiff = ") NET_PRINT_INT(iTimeDiff)						
								NET_NL()	
							ENDIF
						
						ELSE	
							GlobalServerBD.bHasUpdatedWarpRequestTime[iStaggeredWarpRequestPlayer] = FALSE // so timer will get reset after warp finishes.
							NET_PRINT("[spawning] SERVER_MAINTAIN_WARP_REQUESTS - waiting for player to finish warp.") 
							NET_PRINT(" PlayerID = ") NET_PRINT_INT(NATIVE_TO_INT(PlayerId))
							NET_NL()
						ENDIF
						
					ELSE
						GlobalServerBD.bHasUpdatedWarpRequestTime[iStaggeredWarpRequestPlayer] = FALSE // so timer will get reset after respawn finishes.
						NET_PRINT("[spawning] SERVER_MAINTAIN_WARP_REQUESTS - waiting for player to finish respawning.") 
						NET_PRINT(" PlayerID = ") NET_PRINT_INT(NATIVE_TO_INT(PlayerId))
						NET_NL()	
					ENDIF
					
				ELSE					
					NET_PRINT("[spawning] SERVER_MAINTAIN_WARP_REQUESTS - waiting for player to be alive.") 
					NET_PRINT(" PlayerID = ") NET_PRINT_INT(NATIVE_TO_INT(PlayerId))
					NET_NL()							
				ENDIF

			ELSE
				ResetWarpRequestForPlayer(iStaggeredWarpRequestPlayer)
				NET_PRINT("[spawning] SERVER_MAINTAIN_WARP_REQUESTS - clearing warp request, player is not ok.")
				NET_PRINT(" PlayerID = ") NET_PRINT_INT(NATIVE_TO_INT(PlayerId))
				NET_NL()
			ENDIF
		ELSE
			ResetWarpRequestForPlayer(iStaggeredWarpRequestPlayer)
			NET_PRINT("[spawning] SERVER_MAINTAIN_WARP_REQUESTS - clearing warp request, timed out.")
			NET_PRINT(" PlayerID = ") NET_PRINT_INT(NATIVE_TO_INT(PlayerId))
			NET_NL()
		ENDIF
	ENDIF
	
	
	// warp to vehicle requests
	IF NOT (GlobalServerBD.g_iWarpRequestVehicle[iStaggeredWarpRequestPlayer] = -1)
		PlayerId = INT_TO_NATIVE(PLAYER_INDEX, iStaggeredWarpRequestPlayer)
		IF NOT IS_PLAYER_RESPAWNING(PlayerId)
		AND NOT IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(PlayerId)
			GlobalServerBD.g_iWarpRequestVehicle[iStaggeredWarpRequestPlayer] = -1
			GlobalServerBD.g_iWarpRequestVehicleSeat[iStaggeredWarpRequestPlayer] = -1
			NET_PRINT("[spawning] SERVER_MAINTAIN_WARP_REQUESTS - clearing warp into vehicle request.")
			NET_PRINT(" PlayerID = ") NET_PRINT_INT(NATIVE_TO_INT(PlayerId))
			NET_NL()
		ENDIF		
	ENDIF
		
		
	// DON'T CLEAR THE VEHICLE WARPING REQUESTS - JUST LEAVE IN MEMORY UNTIL NEXT TIME IS CALLED. 
		// as we never know when the player will move the vehicle away. To fix 1513136
				
		
//	// ********** VEHICLE WARPING ***********
//	IF (GlobalServerBD.bSpawnVehicleRequestTimeIntialised[iStaggeredWarpRequestPlayer])	
//		// has someone just requested to warp to this coord?
//		iTimeDiff = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), GlobalServerBD.g_iSpawnVehicleRequestTime[iStaggeredWarpRequestPlayer])		
//		IF (iTimeDiff < 40000)
//			PlayerId = INT_TO_NATIVE(PLAYER_INDEX, iStaggeredWarpRequestPlayer)
//			IF IS_NET_PLAYER_OK(PlayerID, FALSE, FALSE)			
//				// check player is alive
//				IF IS_NET_PLAYER_OK(PlayerID, TRUE, FALSE)														
//					IF (iTimeDiff > 20000) // don't clear the warp request for at least 10 seconds						
//						GlobalServerBD.g_vSpawnVehicleRequest[iStaggeredWarpRequestPlayer] = <<0.0, 0.0, 0.0>> // reset these each frame	
//						GlobalServerBD.bSpawnVehicleRequestTimeIntialised[iStaggeredWarpRequestPlayer] = FALSE
//						NET_PRINT("[spawning] SERVER_MAINTAIN_WARP_REQUESTS - clearing SpawnVehicle request, player has arrived.") 
//						NET_PRINT(" PlayerID = ") NET_PRINT_INT(NATIVE_TO_INT(PlayerId))
//						NET_NL()
//					ELSE
//						NET_PRINT("[spawning] SERVER_MAINTAIN_WARP_REQUESTS - waiting for SpawnVehicle to commence.") 
//						NET_PRINT(" PlayerID = ") NET_PRINT_INT(NATIVE_TO_INT(PlayerId)) NET_PRINT(", iTimeDiff = ") NET_PRINT_INT(iTimeDiff)						
//						NET_NL()	
//					ENDIF										
//				ENDIF
//			ELSE
//				GlobalServerBD.g_vSpawnVehicleRequest[iStaggeredWarpRequestPlayer] = <<0.0, 0.0, 0.0>> // reset these each frame	
//				GlobalServerBD.bSpawnVehicleRequestTimeIntialised[iStaggeredWarpRequestPlayer] = FALSE
//				NET_PRINT("[spawning] SERVER_MAINTAIN_WARP_REQUESTS - clearing SpawnVehicle request, player is not ok.")
//				NET_PRINT(" PlayerID = ") NET_PRINT_INT(NATIVE_TO_INT(PlayerId))
//				NET_NL()
//			ENDIF
//		ELSE
//			GlobalServerBD.g_vSpawnVehicleRequest[iStaggeredWarpRequestPlayer] = <<0.0, 0.0, 0.0>> // reset these each frame	
//			GlobalServerBD.bSpawnVehicleRequestTimeIntialised[iStaggeredWarpRequestPlayer] = FALSE
//			NET_PRINT("[spawning] SERVER_MAINTAIN_WARP_REQUESTS - clearing SpawnVehicle request, timed out.")
//			NET_PRINT(" PlayerID = ") NET_PRINT_INT(NATIVE_TO_INT(PlayerId))
//			NET_NL()
//		ENDIF
//	ENDIF	

		
	iStaggeredWarpRequestPlayer += 1
	IF (iStaggeredWarpRequestPlayer >= NUM_NETWORK_PLAYERS)
		iStaggeredWarpRequestPlayer = 0 
	ENDIF
		


ENDPROC

FUNC BOOL IS_MY_WARP_REQUEST_STILL_VALID(VECTOR vPointToWarpTo)

	IF (GlobalServerBD.bWarpRequestTimeIntialised[NATIVE_TO_INT(PLAYER_ID())])
	AND VDIST(vPointToWarpTo, GlobalServerBD.g_vWarpRequest[NATIVE_TO_INT(PLAYER_ID())]) < 1.0
		NET_PRINT("[spawning] IS_MY_WARP_REQUEST_STILL_VALID returning TRUE")
		RETURN(TRUE)	
	ENDIF
	NET_PRINT("[spawning] IS_MY_WARP_REQUEST_STILL_VALID returning FALSE")
	RETURN(FALSE)
ENDFUNC







PROC CLEAR_FALLBACK_SPAWN_POINTS()
	ClearFallbackSpawnPoints()
	NET_PRINT("[spawning] CLEAR_FALLBACK_SPAWN_POINTS() - called.") NET_NL() 
ENDPROC


FUNC INT GET_NUMBER_OF_PLAYERS_IN_SAME_TUTORIAL_SESSION()
	INT i
	INT iCount
	PLAYER_INDEX PlayerID
	
	REPEAT NUM_NETWORK_PLAYERS i
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, i)
		IF NOT (PlayerID = PLAYER_ID())
			IF IS_NET_PLAYER_OK(PlayerID, FALSE, TRUE)
				IF NETWORK_IS_PLAYER_A_PARTICIPANT(PlayerID)								
					IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)
						iCount += 1
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN(iCount)

ENDFUNC






FUNC BOOL IS_POINT_IN_GLOBAL_EXCLUSION_AREA(VECTOR vCoord)
	INT i
	REPEAT MAX_NUMBER_OF_GLOBAL_EXCLUSION_AREAS i 
		IF (GlobalExclusionArea[i].ExclusionArea.bIsActive = TRUE)								
			IF IsPointInsideSpawnArea(vCoord, GlobalExclusionArea[i].ExclusionArea, 0.1)
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC




PROC PROCESS_EVENT_QUERY_SPECIFIC_SPAWN_POINT(INT paramEventID)
	SCRIPT_EVENT_DATA_QUERY_SPECIFIC_SPAWN_POINT sEventStruct
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, sEventStruct, SIZE_OF(sEventStruct)))
		SCRIPT_ASSERT("PROCESS_EVENT_OHD_IS_VOTING_RESET(): FAILED TO RETRIEVE EVENT DATA. Tell Neil.")
		EXIT
	ELSE
		NET_PRINT("[spawning] PROCESS_EVENT_QUERY_SPECIFIC_SPAWN_POINT: received event from player ")NET_PRINT_INT(NATIVE_TO_INT(sEventStruct.Details.FromPlayerIndex))NET_NL()		
		IF IS_ENTITY_AT_COORD (PLAYER_PED_ID(), sEventStruct.vPos, <<150.0, 150.0, 150.0>> )
			IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
				IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(sEventStruct.vPos, 6.0, 20.0, 0.5, 1.0, TRUE, TRUE, TRUE, 120.0, FALSE, -1, TRUE, 15.0)	
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_START_SPAWN_POINT_OK, SPECIFIC_PLAYER(sEventStruct.Details.FromPlayerIndex)) 
				ELSE
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_START_SPAWN_POINT_NOT_OK, SPECIFIC_PLAYER(sEventStruct.Details.FromPlayerIndex))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PROC SET_SWOOP_CAMERA_TYPE_FOR_RESPAWN(PLAYER_SPAWN_LOCATION SpawnLoc, VECTOR vFromCoords)
//	g_SpawnData.vStartSwoop = vFromCoords
//	g_SpawnData.vApproxEndSwoop = GetApproxCoordsForSpawnLocation(SpawnLoc, vFromCoords) 
//ENDPROC



///PURPOSE: for finding a safe spawnable coords for creating a mission entity. Will do offscreen checks etc. 
///    note: vFacingCoords - if you want the entity to face a particular coord, use this, otherwise pass in <<0.0, 0.0, 0.0>>
///    iFriendlyTeam = if you set this to a team it will face points closer to this team and also perform as strict on screen checks for that team.
FUNC BOOL GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(VECTOR vCentreCoord, FLOAT fRadius, VECTOR &vReturnCoords, FLOAT &fReturnHeading, SPAWN_SEARCH_PARAMS &Params)
	
	#IF IS_DEBUG_BUILD
		g_SpawnData.LastGetSafeCoordsForCreatingEntity.vCoords1 = vCentreCoord
		g_SpawnData.LastGetSafeCoordsForCreatingEntity.fFloat = fRadius
		g_SpawnData.LastGetSafeCoordsForCreatingEntity.bIsActive = TRUE
		g_SpawnData.LastGetSafeCoordsForCreatingEntity.iShape = SPAWN_AREA_SHAPE_CIRCLE
		g_SpawnData.LastGetSafeCoordsForCreatingEntity.bConsiderCentrePointAsValid = Params.bConsiderOriginAsValidPoint
	#ENDIF	
	
	// make sure fMinDistFromPlayer is not bigger than the radius (or within 20m)
	IF (Params.fMinDistFromPlayer > (fRadius - 20.0))
		Params.fMinDistFromPlayer = fRadius - 20.0
	ENDIF
	
//	// if we're passing in coords to avoid, then make sure 
//	IF (VMAG(Params.vAvoidCoords) > 0.0)
//	AND (Params.bCloseToOriginAsPossible)
//		// if center point is within avoid coords
//		IF (VDIST(Params.vAvoidCoords, vCentreCoord) < Params.fAvoidRadius)
//		AND (VDIST(Params.vAvoidCoords, vCentreCoord) > fRadius)  
//			Params.bCloseToOriginAsPossible = FALSE
//			#IF IS_DEBUG_BUILD
//				NET_PRINT("[spawning] GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY - switching off bCloseToOriginAsPossible as too near to vAvoidCoords" ) NET_NL()
//			#ENDIF	
//		ENDIF
//	ENDIF
	
	SPAWN_RESULTS SpawnResults
	PRIVATE_SPAWN_SEARCH_PARAMS PrivateParams
	PrivateParams.vSearchCoord = vCentreCoord
	PrivateParams.fRawHeading = Params.fHeadingForConsideredOrigin
	PrivateParams.fSearchRadius = fRadius
	PrivateParams.bIsForLocalPlayerSpawning = FALSE
	PrivateParams.bDoTeamMateVisCheck = TRUE
	PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_CIRCLE
	PrivateParams.bForAVehicle = FALSE
	PrivateParams.bDoVisibleChecks = TRUE
	PrivateParams.bIgnoreTeammatesForMinDistCheck = FALSE
	
	
	//IF DoRespawnSearchForCoord(vCentreCoord, Params.fHeadingForConsideredOrigin, fRadius, SpawnResults, FALSE, Params.vFacingCoords, TRUE, Params.bConsiderInteriors, Params.bPreferPointsCloserToRoads, Params.fMinDistFromPlayer, Params.bCloseToOriginAsPossible, FALSE, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.0, Params.bConsiderOriginAsValidPoint, Params.bSearchVehicleNodesOnly, Params.bUseOnlyBoatNodes, FALSE, Params.bEdgesOnly, TRUE, FALSE)	
	IF DoRespawnSearchForCoord(PrivateParams, Params, SpawnResults)
		
		// if spawn location is directly below the raw coords then assume its been placed under a bridge
		IF (Params.fMaxZBelowRaw > 0.0)
		AND (Params.bConsiderOriginAsValidPoint)
		AND (Params.bSearchVehicleNodesOnly)
			VECTOR vec  = vCentreCoord - SpawnResults.vCoords[0]
			IF (vec.z > Params.fMaxZBelowRaw)
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY - looks like this point is directly under the raw point, assuming under a bridge and returning raw." ) NET_NL()
				#ENDIF					
				SpawnResults.vCoords[0] = vCentreCoord
				SpawnResults.fHeading[0] = Params.fHeadingForConsideredOrigin
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			StoreReturnedSafeCoordsForCreatingEntity(SpawnResults.vCoords[0], SpawnResults.fHeading[0]) 
		#ENDIF
		vReturnCoords = SpawnResults.vCoords[0] 
		fReturnHeading = SpawnResults.fHeading[0]
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY - returning point " ) NET_PRINT_VECTOR(vReturnCoords) NET_PRINT(", heading = ") NET_PRINT_FLOAT(fReturnHeading) NET_NL()
		#ENDIF		
		RETURN(TRUE)
	ELSE
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY - waiting for DoRespawnSearchForCoord()" ) NET_NL()
		#ENDIF	
	ENDIF
	
	RETURN(FALSE)

ENDFUNC

///PURPOSE: for finding a safe spawnable coords for creating a mission entity. Will do offscreen checks etc. 
///    note: vFacingCoords - if you want the entity to face a particular coord, use this, otherwise pass in <<0.0, 0.0, 0.0>>
///    iFriendlyTeam = if you set this to a team it will face points closer to this team and also perform as strict on screen checks for that team.
FUNC BOOL GET_SAFE_COORDS_IN_ANGLED_AREA_FOR_CREATING_ENTITY(VECTOR vPoint1, VECTOR vPoint2, FLOAT fWidth, VECTOR &vReturnCoords, FLOAT &fReturnHeading, SPAWN_SEARCH_PARAMS Params)

	#IF IS_DEBUG_BUILD
		g_SpawnData.LastGetSafeCoordsForCreatingEntity.vCoords1 = vPoint1
		g_SpawnData.LastGetSafeCoordsForCreatingEntity.vCoords2 = vPoint2
		g_SpawnData.LastGetSafeCoordsForCreatingEntity.fFloat = fWidth
		g_SpawnData.LastGetSafeCoordsForCreatingEntity.bIsActive = TRUE
		g_SpawnData.LastGetSafeCoordsForCreatingEntity.iShape = SPAWN_AREA_SHAPE_ANGLED
		g_SpawnData.LastGetSafeCoordsForCreatingEntity.bConsiderCentrePointAsValid = FALSE
	#ENDIF
	
	// make sure fMinDistFromPlayer is not within 20m
	IF (Params.fMinDistFromPlayer < 20.0)
		Params.fMinDistFromPlayer = 20.0	
	ENDIF
	
	SPAWN_RESULTS SpawnResults
	
	PRIVATE_SPAWN_SEARCH_PARAMS PrivateParams
	PrivateParams.bIsForLocalPlayerSpawning = FALSE
	PrivateParams.bDoTeamMateVisCheck = TRUE
	//PrivateParams.bIsAngledArea = TRUE
	PrivateParams.iAreaShape = SPAWN_AREA_SHAPE_ANGLED
	PrivateParams.vAngledAreaPoint1 = vPoint1
	PrivateParams.vAngledAreaPoint2 = vPoint2
	PrivateParams.fAngledAreaWidth = fWidth
	PrivateParams.bForAVehicle = FALSE
	PrivateParams.bDoVisibleChecks = TRUE
	PrivateParams.bIgnoreTeammatesForMinDistCheck = FALSE	
	
	Params.bConsiderOriginAsValidPoint = FALSE
	
	//IF DoRespawnSearchForCoord(<<0.0,0.0,0.0>>, 0.0, 0.0, SpawnResults, FALSE, Params.vFacingCoords, TRUE, Params.bConsiderInteriors, Params.bPreferPointsCloserToRoads, Params.fMinDistFromPlayer, Params.bCloseToOriginAsPossible, TRUE, vPoint1, vPoint2, fWidth, FALSE,  Params.bSearchVehicleNodesOnly, Params.bUseOnlyBoatNodes, FALSE, Params.bEdgesOnly, TRUE, FALSE)
	
	IF DoRespawnSearchForCoord(PrivateParams, Params, SpawnResults)
		#IF IS_DEBUG_BUILD
			StoreReturnedSafeCoordsForCreatingEntity(SpawnResults.vCoords[0], SpawnResults.fHeading[0]) 
		#ENDIF
		vReturnCoords = SpawnResults.vCoords[0] 
		fReturnHeading = SpawnResults.fHeading[0]
		RETURN(TRUE)
	ELSE
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] GET_SAFE_COORDS_IN_ANGLED_AREA_FOR_CREATING_ENTITY - waiting for DoRespawnSearchForCoord()" ) NET_NL()
		#ENDIF	
	ENDIF
	
	RETURN(FALSE)

ENDFUNC


PROC CLEAR_VEHICLE_SPAWN_INFO()
	ClearVehicleSpawnInfo()
ENDPROC

PROC STORE_VEHICLE_SPAWN_INFO_FROM_THIS_CAR(VEHICLE_INDEX CarID, GAMER_HANDLE GamerHandle)
	DEBUG_PRINTCALLSTACK()
	NET_PRINT("[spawning] STORE_VEHICLE_SPAWN_INFO_FROM_THIS_CAR called ") NET_NL()
	StoreVehicleSpawnInfoFromThisCar(CarID, GamerHandle)
ENDPROC



PROC RESTORE_VEHICLE_FROM_SPAWN_INFO(VEHICLE_INDEX CarID, BOOL bSetLockStatus=TRUE)
	DEBUG_PRINTCALLSTACK()
	NET_PRINT("[spawning] RESTORE_VEHICLE_FROM_SPAWN_INFO called ") NET_NL()
	RestoreVehicleFromSpawnInfo(CarID, bSetLockStatus)
ENDPROC


PROC CLEAR_SPECIFIC_SPAWN_LOCATION()
	IF IS_THREAD_ACTIVE(g_SpecificSpawnLocation.Thread_ID)
		IF NOT (g_SpecificSpawnLocation.Thread_ID = GET_ID_OF_THIS_THREAD())
			DEBUG_PRINTCALLSTACK()
			NET_PRINT("[spawning] CLEAR_SPECIFIC_SPAWN_LOCATION - in use by another script!") NET_NL()
			SCRIPT_ASSERT("CLEAR_SPECIFIC_SPAWN_LOCATION - in use by another script!")
			EXIT
		ENDIF
	ENDIF
	
	SPECIFIC_SPAWN_LOCATION EmptyData
	g_SpecificSpawnLocation = EmptyData
	g_SpecificSpawnLocation.Thread_ID = INT_TO_NATIVE(THREADID, -1)
	
	NET_PRINT("[spawning] CLEAR_SPECIFIC_SPAWN_LOCATION - called by ") 
	NET_PRINT(GET_THIS_SCRIPT_NAME()) 
	NET_NL()	
	DEBUG_PRINTCALLSTACK()
	
ENDPROC

PROC CLEAR_SPECIFIC_SPAWN_LOCATION_FROM_THIS_SCRIPT()
	IF (g_SpecificSpawnLocation.Thread_ID = GET_ID_OF_THIS_THREAD())	
		CLEAR_SPECIFIC_SPAWN_LOCATION()
	ENDIF
ENDPROC

/// PURPOSE:
///    adds a decorator to let the spawning system know that this is a vehicle that can be respawned in.
PROC SET_VEHICLE_AS_A_SPECIFIC_RESPAWN_VEHICLE(VEHICLE_INDEX VehicleID)
	DEBUG_PRINTCALLSTACK()
	IF DOES_ENTITY_EXIST(VehicleID)
	AND NOT IS_ENTITY_DEAD(VehicleID)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)
			IF DECOR_IS_REGISTERED_AS_TYPE("RespawnVeh", DECOR_TYPE_INT)	
				INT iRand
				IF DECOR_EXIST_ON(VehicleID, "RespawnVeh")
					iRand = DECOR_GET_INT(VehicleID, "RespawnVeh")
					PRINTLN("[spawning] SET_VEHICLE_AS_A_SPECIFIC_RESPAWN_VEHICLE - decorator already exist for vehicle ", NATIVE_TO_INT(VehicleID), " with value ", iRand)
				ELSE	 				
					iRand = GET_RANDOM_INT_IN_RANGE()				
					DECOR_SET_INT(VehicleID, "RespawnVeh", iRand)
					PRINTLN("[spawning] SET_VEHICLE_AS_A_SPECIFIC_RESPAWN_VEHICLE - adding decor to vehicle ", NATIVE_TO_INT(VehicleID), " with value ", iRand)
				ENDIF						
			ENDIF
		ELSE
			ASSERTLN("SET_VEHICLE_AS_A_SPECIFIC_RESPAWN_VEHICLE - can only be called by player who owns vehicle, best to do as vehicle is created.")
		ENDIF
	ENDIF
ENDPROC

PROC SET_PLAYER_TO_RESPAWN_IN_SPECIFIC_RESPAWN_VEHICLE(VEHICLE_INDEX VehicleID, INT VehicleSeat = VS_INVALID)
	DEBUG_PRINTCALLSTACK()
	IF DOES_ENTITY_EXIST(VehicleID)
	AND NOT IS_ENTITY_DEAD(VehicleID)
	
		#IF IS_DEBUG_BUILD
		IF DECOR_IS_REGISTERED_AS_TYPE("RespawnVeh", DECOR_TYPE_INT)
		AND NOT DECOR_EXIST_ON(VehicleID, "RespawnVeh")
			//IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)
				ASSERTLN("SET_PLAYER_TO_RESPAWN_IN_SPECIFIC_RESPAWN_VEHICLE - vehicle has not been declared as a specific respawn vehicle - it's network owner must call SET_VEHICLE_AS_A_SPECIFIC_RESPAWN_VEHICLE on it first.") 			
			//ENDIF
		ENDIF
		#ENDIF
		
		g_SpawnData.SpecificVehicleID = VehicleID
		g_SpawnData.SpecificVehicleSeat = ENUM_TO_INT(VehicleSeat)
		
		PRINTLN("[spawning] SET_PLAYER_TO_RESPAWN_IN_SPECIFIC_RESPAWN_VEHICLE - called. vehicle id = ", NATIVE_TO_INT(VehicleID))
		#IF IS_DEBUG_BUILD
		VECTOR vCoords = GET_ENTITY_COORDS(VehicleID)
		PRINTLN("[spawning] SET_PLAYER_TO_RESPAWN_IN_SPECIFIC_RESPAWN_VEHICLE - vehicle coords = ", vCoords)
		#ENDIF
		
	ENDIF
ENDPROC

PROC CLEAR_SPECIFIC_VEHICLE_TO_RESPAWN_IN()
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[spawning] CLEAR_SPECIFIC_VEHICLE_TO_RESPAWN_IN - called.")
	g_SpawnData.SpecificVehicleID = NULL
ENDPROC

PROC DISABLE_CUSTOM_VEHICLE_NODES_FOR_ALL_PROPERTIES(BOOL bDisable)
	DEBUG_PRINTCALLSTACK()
	g_SpawnData.MissionSpawnDetails.bDisablePropertyCustomNodes = bDisable
	PRINTLN("[spawning] DISABLE_CUSTOM_VEHICLE_NODES_FOR_ALL_PROPERTIES - called with ", bDisable)
ENDPROC

PROC SET_MIN_DIST_TO_SPAWN_FROM_ENEMY_PLAYER_NEAR_DEATH(FLOAT fMinDist)
	g_SpawnData.MissionSpawnDetails.fMinDistFromEnemyNearDeath = fMinDist
	NET_PRINT("[spawning] SET_MIN_DIST_TO_SPAWN_FROM_ENEMY_PLAYER_NEAR_DEATH - set to ") NET_PRINT_FLOAT(fMinDist) NET_NL() 
ENDPROC

PROC SET_MIN_DIST_TO_SPAWN_FROM_DEATH_NEAR_DEATH(FLOAT fMinDist)
	g_SpawnData.MissionSpawnDetails.fMinDistFromDeathNearDeath = fMinDist
	NET_PRINT("[spawning] SET_MIN_DIST_TO_SPAWN_FROM_DEATH_NEAR_DEATH - set to ") NET_PRINT_FLOAT(fMinDist) NET_NL() 
ENDPROC

PROC SET_RADIUS_TO_AVOID_SPAWNING(VECTOR vCoords, FLOAT fRadius)
	g_SpawnData.MissionSpawnDetails.vAvoidCoords = vCoords	
	g_SpawnData.MissionSpawnDetails.fAvoidRadius = fRadius
	NET_PRINT("[spawning] SET_RADIUS_TO_AVOID_SPAWNING - set to ") NET_PRINT_VECTOR(vCoords) NET_PRINT(", ") NET_PRINT_FLOAT(fRadius) NET_NL() 
ENDPROC

PROC CLEAR_RADIUS_TO_AVOID_SPAWNING()
	g_SpawnData.MissionSpawnDetails.vAvoidCoords = <<0.0, 0.0, 0.0>>
	g_SpawnData.MissionSpawnDetails.fAvoidRadius = 0.0
	NET_PRINT("[spawning] CLEAR_RADIUS_TO_AVOID_SPAWNING - called ")  NET_NL() 
ENDPROC

PROC SET_DO_VISIBILITY_CHECKS_FOR_SPAWNING(BOOL bSet)
	g_SpawnData.MissionSpawnDetails.bDoVisibilityChecks = bSet
	NET_PRINT("[spawning] SET_DO_VISIBILITY_CHECKS_FOR_SPAWNING - set to ") NET_PRINT_BOOL(bSet) NET_NL() 
ENDPROC

PROC DISABLE_GLOBAL_EXCLUSION_ZONES_FOR_SPAWNING(BOOL bSet)
	IF NOT (bSet = g_SpawnData.MissionSpawnDetails.bGlobalExclusionIsIgnored)
		g_SpawnData.MissionSpawnDetails.bGlobalExclusionIsIgnored = bSet
		NET_PRINT("[spawning] DISABLE_GLOBAL_EXCLUSION_ZONES_FOR_SPAWNING - set to ") NET_PRINT_BOOL(bSet) NET_NL() 
		DEBUG_PRINTCALLSTACK()
	ENDIF	
ENDPROC

#IF IS_DEBUG_BUILD
PROC PRINT_SPECIFIC_SPAWN_DETAILS(SPECIFIC_SPAWN_LOCATION &SpecificSpawnDetails)

	PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION vCoords = ", SpecificSpawnDetails.vCoords)
    PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION fMinRadius = ", SpecificSpawnDetails.fMinRadius)
    PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION fRadius = ", SpecificSpawnDetails.fRadius)
    PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION fHeading = ", SpecificSpawnDetails.fHeading)
    PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION bDoVisibleChecks = ", SpecificSpawnDetails.bDoVisibleChecks)
    PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION bDoNearARoadChecks = ", SpecificSpawnDetails.bDoNearARoadChecks)
    PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION bConsiderInteriors = ", SpecificSpawnDetails.bConsiderInteriors)
    PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION bUseAngledArea = ", SpecificSpawnDetails.bUseAngledArea)
    PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION vAngledCoords1 = ", SpecificSpawnDetails.vAngledCoords1)
    PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION vAngledCoords2 = ", SpecificSpawnDetails.vAngledCoords2)
    PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION fWidth = ", SpecificSpawnDetails.fWidth)
    PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION vFacing = ", SpecificSpawnDetails.vFacing)
    PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION fMinDistFromOtherPlayers = ", SpecificSpawnDetails.fMinDistFromOtherPlayers)
	PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION bNearCentrePoint = ", SpecificSpawnDetails.bNearCentrePoint)
	PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION bAllowFallbackToUseInteriors = ", SpecificSpawnDetails.bAllowFallbackToUseInteriors)

ENDPROC

PROC PRINT_SPECIFIC_SPAWN_DETAIL_CHANGES(SPECIFIC_SPAWN_LOCATION &SpecificSpawnDetails)

	IF NOT (VMAG(g_SpecificSpawnLocation.vCoords) = VMAG(SpecificSpawnDetails.vCoords))
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed vCoords = ", SpecificSpawnDetails.vCoords)
	ENDIF
	
	IF NOT (g_SpecificSpawnLocation.fMinRadius = SpecificSpawnDetails.fMinRadius)
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed fMinRadius = ", SpecificSpawnDetails.fMinRadius)	
	ENDIF
	
	IF NOT (g_SpecificSpawnLocation.fRadius = SpecificSpawnDetails.fRadius)
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed fRadius = ", SpecificSpawnDetails.fRadius)	
	ENDIF

	IF NOT (g_SpecificSpawnLocation.fHeading = SpecificSpawnDetails.fHeading)
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed fHeading = ", SpecificSpawnDetails.fHeading)	
	ENDIF
    
	IF NOT (g_SpecificSpawnLocation.bDoVisibleChecks = SpecificSpawnDetails.bDoVisibleChecks)
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed bDoVisibleChecks = ", SpecificSpawnDetails.bDoVisibleChecks)	
	ENDIF

	IF NOT (g_SpecificSpawnLocation.bDoNearARoadChecks = SpecificSpawnDetails.bDoNearARoadChecks)
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed bDoNearARoadChecks = ", SpecificSpawnDetails.bDoNearARoadChecks)	
	ENDIF

	IF NOT (g_SpecificSpawnLocation.bConsiderInteriors = SpecificSpawnDetails.bConsiderInteriors)
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed bConsiderInteriors = ", SpecificSpawnDetails.bConsiderInteriors)	
	ENDIF
   
   	IF NOT (g_SpecificSpawnLocation.bUseAngledArea = SpecificSpawnDetails.bUseAngledArea)
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed bConsiderInteriors = ", SpecificSpawnDetails.bUseAngledArea)	
	ENDIF
    
	IF NOT (VMAG(g_SpecificSpawnLocation.vAngledCoords1) = VMAG(SpecificSpawnDetails.vAngledCoords1))
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed vAngledCoords1 = ", SpecificSpawnDetails.vAngledCoords1)	
	ENDIF
	
	IF NOT (VMAG(g_SpecificSpawnLocation.vAngledCoords2) = VMAG(SpecificSpawnDetails.vAngledCoords2))
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed vAngledCoords2 = ", SpecificSpawnDetails.vAngledCoords2)	
	ENDIF
	
	IF NOT (g_SpecificSpawnLocation.fWidth = SpecificSpawnDetails.fWidth)
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed fWidth = ", SpecificSpawnDetails.fWidth)	
	ENDIF
		
	IF NOT (VMAG(g_SpecificSpawnLocation.vFacing) = VMAG(SpecificSpawnDetails.vFacing))
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed vFacing = ", SpecificSpawnDetails.vFacing)	
	ENDIF
	
	IF NOT (g_SpecificSpawnLocation.fMinDistFromOtherPlayers = SpecificSpawnDetails.fMinDistFromOtherPlayers)
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed fMinDistFromOtherPlayers = ", SpecificSpawnDetails.fMinDistFromOtherPlayers)	
	ENDIF
	
	IF NOT (g_SpecificSpawnLocation.bNearCentrePoint = SpecificSpawnDetails.bNearCentrePoint)
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed bNearCentrePoint = ", SpecificSpawnDetails.bNearCentrePoint)	
	ENDIF

	IF NOT (g_SpecificSpawnLocation.bAllowFallbackToUseInteriors = SpecificSpawnDetails.bAllowFallbackToUseInteriors)
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed bAllowFallbackToUseInteriors = ", SpecificSpawnDetails.bAllowFallbackToUseInteriors)	
	ENDIF
	
	IF NOT (g_SpecificSpawnLocation.bVehiclesCanConsiderInactiveNodes = SpecificSpawnDetails.bVehiclesCanConsiderInactiveNodes)
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed bVehiclesCanConsiderInactiveNodes = ", SpecificSpawnDetails.bVehiclesCanConsiderInactiveNodes)	
	ENDIF
	
	IF NOT (g_SpecificSpawnLocation.fPitch = SpecificSpawnDetails.fPitch)
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed fPitch = ", SpecificSpawnDetails.fPitch)	
	ENDIF
	
	IF NOT (g_SpecificSpawnLocation.fUpperZLimitForNodes = SpecificSpawnDetails.fUpperZLimitForNodes)
		PRINTLN("[spawning] SPECIFIC_SPAWN_LOCATION changed bIgnoreZDiff = ", SpecificSpawnDetails.fUpperZLimitForNodes)	
	ENDIF
	
ENDPROC

#ENDIF

PROC SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA(VECTOR vAngledCoords1, 
												VECTOR vAngledCoords2, 
												FLOAT fAngledWidth, 
												VECTOR vFacingCoord, 
												BOOL bDoVisibleChecks=TRUE, 
												BOOL bConsiderInteriors = FALSE, 
												BOOL bDoNearARoadChecks=TRUE, 
												FLOAT MinDistFromOtherPlayers=65.0, 
												BOOL bNearCentrePoint = FALSE, 
												BOOL bVehiclesCanConsiderInactiveNodes = FALSE,
												FLOAT fPitch=0.0,
												BOOL bOverideAllowFallbackToUseInteriors = TRUE,
												FLOAT fUpperZLimitForNodes=-1.0)

	SPECIFIC_SPAWN_LOCATION SpecificSpawnDetails	

	IF IS_THREAD_ACTIVE(g_SpecificSpawnLocation.Thread_ID)
		IF NOT (g_SpecificSpawnLocation.Thread_ID = GET_ID_OF_THIS_THREAD())
			DEBUG_PRINTCALLSTACK()
			NET_PRINT("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA - in use by another script!") NET_NL()
			SCRIPT_ASSERT("SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA - in use by another script!")
			EXIT
		ENDIF
	ELSE
		g_SpecificSpawnLocation.Thread_ID = GET_ID_OF_THIS_THREAD()
		
		NET_PRINT("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA - setup by ") 
		NET_PRINT(GET_THIS_SCRIPT_NAME()) 
		NET_PRINT(" vAngledCoords1  = ") NET_PRINT_VECTOR(vAngledCoords1) 
		NET_PRINT(" vAngledCoords2 = ") NET_PRINT_VECTOR(vAngledCoords2)
		NET_PRINT(" fAngledWidth = ") NET_PRINT_FLOAT(fAngledWidth) 
		NET_NL()	

		DEBUG_PRINTCALLSTACK()		
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF VMAG(vAngledCoords1) = 0.0
			NET_PRINT("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA - vAngledCoords1 is at origin!") NET_NL()
			SCRIPT_ASSERT("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA - vAngledCoords1 is at origin!")
		ENDIF
		IF VMAG(vAngledCoords2) = 0.0
			NET_PRINT("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA - vAngledCoords2 is at origin!") NET_NL()
			SCRIPT_ASSERT("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA - vAngledCoords2 is at origin!")
		ENDIF
		IF (VMAG(vAngledCoords1) > 10000.0)
			NET_PRINT("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA - vAngledCoords1 are a long, long way from home!") NET_PRINT_VECTOR(vAngledCoords1) NET_NL()
			SCRIPT_ASSERT("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA - vAngledCoords1 are a long, long way from home. Why do you want to spawn here? speak to Neil F. ")
		ENDIF	
		IF (VMAG(vAngledCoords2) > 10000.0)
			NET_PRINT("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA - vAngledCoords2 are a long, long way from home!") NET_PRINT_VECTOR(vAngledCoords2) NET_NL()
			SCRIPT_ASSERT("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION_ANGLED_AREA - vAngledCoords2 are a long, long way from home. Why do you want to spawn here? speak to Neil F. ")
		ENDIF
	#ENDIF	

	SpecificSpawnDetails.Thread_ID = g_SpecificSpawnLocation.Thread_ID
	SpecificSpawnDetails.fHeading = 0.0
	SpecificSpawnDetails.fRadius = 0.0
	SpecificSpawnDetails.bDoVisibleChecks = bDoVisibleChecks
	SpecificSpawnDetails.fMinRadius = 0.0
	SpecificSpawnDetails.bDoNearARoadChecks = bDoNearARoadChecks
	SpecificSpawnDetails.bConsiderInteriors = bConsiderInteriors
	SpecificSpawnDetails.bUseAngledArea = TRUE
	SpecificSpawnDetails.vAngledCoords1 = vAngledCoords1
	SpecificSpawnDetails.vAngledCoords2 = vAngledCoords2
	SpecificSpawnDetails.fWidth = fAngledWidth	
	SpecificSpawnDetails.vCoords = (SpecificSpawnDetails.vAngledCoords1 + SpecificSpawnDetails.vAngledCoords2)/2.0
	SpecificSpawnDetails.vFacing = vFacingCoord
	SpecificSpawnDetails.fMinDistFromOtherPlayers = MinDistFromOtherPlayers
	SpecificSpawnDetails.bNearCentrePoint = bNearCentrePoint
	SpecificSpawnDetails.bVehiclesCanConsiderInactiveNodes = bVehiclesCanConsiderInactiveNodes
	IF IsPlayerOnAMissionOrEventForSpawningPurposes(PLAYER_ID())
		IF bOverideAllowFallbackToUseInteriors
		OR bConsiderInteriors
			PRINTLN("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION - bAllowFallbackToUseInteriors has been set to TRUE. IsPlayerOnAMissionOrEventForSpawningPurposes = TRUE")
			SpecificSpawnDetails.bAllowFallbackToUseInteriors = TRUE
		ELSE
			PRINTLN("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION - bAllowFallbackToUseInteriors has been overriden to FALSE. IsPlayerOnAMissionOrEventForSpawningPurposes = TRUE")
			SpecificSpawnDetails.bAllowFallbackToUseInteriors = FALSE
		ENDIF		
	ELSE
		SpecificSpawnDetails.bAllowFallbackToUseInteriors = FALSE
	ENDIF
	IF (SpecificSpawnDetails.fMinDistFromOtherPlayers < 1.0)
		SpecificSpawnDetails.fMinDistFromOtherPlayers = 1.0
	ENDIF	
	SpecificSpawnDetails.fPitch = fPitch
	SpecificSpawnDetails.fUpperZLimitForNodes = fUpperZLimitForNodes
	
	#IF IS_DEBUG_BUILD
		PRINT_SPECIFIC_SPAWN_DETAIL_CHANGES(SpecificSpawnDetails)
	#ENDIF	
	
	g_SpecificSpawnLocation = SpecificSpawnDetails
	
	
ENDPROC

PROC SET_SPECIFIC_SPAWN_LOCATION_FACING(VECTOR vFacing)

	IF IS_THREAD_ACTIVE(g_SpecificSpawnLocation.Thread_ID)
		IF NOT (g_SpecificSpawnLocation.Thread_ID = GET_ID_OF_THIS_THREAD())	
			DEBUG_PRINTCALLSTACK()
			NET_PRINT("[spawning] SET_SPECIFIC_SPAWN_LOCATION_FACING - in use by another script!") NET_NL()
			SCRIPT_ASSERT("SET_SPECIFIC_SPAWN_LOCATION_FACING - in use by another script!")
			EXIT
		ENDIF
	ENDIF
	
	g_SpecificSpawnLocation.vFacing = vFacing
	PRINTLN("[spawning] SET_SPECIFIC_SPAWN_LOCATION_FACING - vFacing = ", g_SpecificSpawnLocation.vFacing)
			
	
ENDPROC

PROC SETUP_SPECIFIC_SPAWN_LOCATION(VECTOR vCoords, 
									FLOAT fHeading, 
									FLOAT fSearchRadius=100.0, 
									BOOL bDoVisibleChecks=TRUE, 
									FLOAT fMinDistFromCoords=0.0, 
									BOOL bConsiderInteriors = FALSE, 
									BOOL bDoNearARoadChecks=TRUE, 
									FLOAT MinDistFromOtherPlayers=65.0, 
									BOOL bNearCentrePoint=TRUE, 
									BOOL bIgnoreGlobalExclusionZones=FALSE, 
									FLOAT fPitch = 0.0,
									BOOL bOverideAllowFallbackToUseInteriors = TRUE,
									FLOAT fUpperZLimitForNodes=-1.0)
	
	SPECIFIC_SPAWN_LOCATION SpecificSpawnDetails	
	
	IF IS_THREAD_ACTIVE(g_SpecificSpawnLocation.Thread_ID)
		IF NOT (g_SpecificSpawnLocation.Thread_ID = GET_ID_OF_THIS_THREAD())	
			DEBUG_PRINTCALLSTACK()
			NET_PRINT("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION - in use by another script!") NET_NL()
			SCRIPT_ASSERT("SETUP_SPECIFIC_SPAWN_LOCATION - in use by another script!")
			EXIT
		ENDIF
	ENDIF
	
	IF VMAG(vCoords) = 0.0
		NET_PRINT("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION - vCoords is at origin!") NET_NL()
		SCRIPT_ASSERT("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION - vCoords is at origin!")		
		EXIT
	ENDIF	
	
	IF NOT IS_THREAD_ACTIVE(g_SpecificSpawnLocation.Thread_ID) 
		g_SpecificSpawnLocation.Thread_ID = GET_ID_OF_THIS_THREAD()
	
		NET_PRINT("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION - setup by ") 
		NET_PRINT(GET_THIS_SCRIPT_NAME()) 
		NET_PRINT(" vCoords  = ") NET_PRINT_VECTOR(vCoords) 
		NET_PRINT(" fHeading = ") NET_PRINT_FLOAT(fHeading)
		NET_PRINT(" fSearchRadius = ") NET_PRINT_FLOAT(fSearchRadius) 
		NET_NL()

		DEBUG_PRINTCALLSTACK()	
		
	ENDIF
	
	

		
	#IF IS_DEBUG_BUILD	
		IF (VMAG(vCoords) > 10000.0)
			NET_PRINT("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION - vCoords are a long, long way from home!") NET_PRINT_VECTOR(vCoords) NET_NL()
			SCRIPT_ASSERT("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION - Coords are a long, long way from home. Why do you want to spawn here? speak to Neil F. ")
		ENDIF
	#ENDIF	
	
	SpecificSpawnDetails.Thread_ID = g_SpecificSpawnLocation.Thread_ID
	SpecificSpawnDetails.vCoords = vCoords
	SpecificSpawnDetails.fHeading = fHeading
	SpecificSpawnDetails.fRadius = fSearchRadius
	SpecificSpawnDetails.bDoVisibleChecks = bDoVisibleChecks
	SpecificSpawnDetails.fMinRadius = fMinDistFromCoords
	SpecificSpawnDetails.bDoNearARoadChecks = bDoNearARoadChecks
	SpecificSpawnDetails.bConsiderInteriors = bConsiderInteriors
	SpecificSpawnDetails.bUseAngledArea = FALSE
	SpecificSpawnDetails.vAngledCoords1 = <<0.0, 0.0, 0.0>>
	SpecificSpawnDetails.vAngledCoords2 = <<0.0, 0.0, 0.0>>
	SpecificSpawnDetails.fWidth = 0.0	
	SpecificSpawnDetails.fMinDistFromOtherPlayers = MinDistFromOtherPlayers
	SpecificSpawnDetails.bNearCentrePoint = bNearCentrePoint
	IF IsPlayerOnAMissionOrEventForSpawningPurposes(PLAYER_ID())
		IF bOverideAllowFallbackToUseInteriors
		OR bConsiderInteriors
			PRINTLN("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION - bAllowFallbackToUseInteriors has been set to TRUE. IsPlayerOnAMissionOrEventForSpawningPurposes = TRUE")
			SpecificSpawnDetails.bAllowFallbackToUseInteriors = TRUE
		ELSE
			PRINTLN("[spawning] SETUP_SPECIFIC_SPAWN_LOCATION - bAllowFallbackToUseInteriors has been overriden to FALSE. IsPlayerOnAMissionOrEventForSpawningPurposes = TRUE")
			SpecificSpawnDetails.bAllowFallbackToUseInteriors = FALSE
		ENDIF
	ELSE
		SpecificSpawnDetails.bAllowFallbackToUseInteriors = FALSE
	ENDIF
	IF (SpecificSpawnDetails.fMinDistFromOtherPlayers < 1.0)
		SpecificSpawnDetails.fMinDistFromOtherPlayers = 1.0
	ENDIF
	SpecificSpawnDetails.bIgnoreGlobalExclusionZones = bIgnoreGlobalExclusionZones
	SpecificSpawnDetails.fPitch = fPitch
	SpecificSpawnDetails.fUpperZLimitForNodes = fUpperZLimitForNodes
	
	#IF IS_DEBUG_BUILD
		PRINT_SPECIFIC_SPAWN_DETAIL_CHANGES(SpecificSpawnDetails)
	#ENDIF		
	
	g_SpecificSpawnLocation = SpecificSpawnDetails
	
ENDPROC


FUNC BOOL MOVE_POINT_OUTSIDE_EXCLUSION_ZONES(VECTOR &vCoords, BOOL bGlobalExclusionAreas, BOOL bMissionExclusionAreas, BOOL bGangAttackZones, BOOL bMissionCoronas)

	BOOL bReturn = FALSE
	
	IF (bGlobalExclusionAreas)
		IF IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(vCoords, TRUE)
			NET_PRINT("[spawning] MOVE_POINT_OUTSIDE_EXCLUSION_ZONES - moved outside global exclusion azones ") 
			bReturn = TRUE
		ENDIF
	ENDIF

	IF (bMissionExclusionAreas)
		IF IS_POINT_IN_MISSION_EXCLUSION_ZONE(vCoords, TRUE)
			NET_PRINT("[spawning] MOVE_POINT_OUTSIDE_EXCLUSION_ZONES - moved outside mission exclusion azones ") 
			bReturn = TRUE
		ENDIF
	ENDIF
	
	IF (bGangAttackZones)
	OR (bMissionCoronas)
		Get_Nearest_Mission_Coronas_And_Gang_Attacks_To_Point(vCoords, g_SpawnData.SearchTempData.NearestCoronas, g_SpawnData.SearchTempData.NearestGangAttack)		
	ENDIF
	
	IF (bGangAttackZones)
		IF IS_POINT_IN_NEAREST_GANG_ATTACK_BLIP_RADIUS(vCoords, g_SpawnData.SearchTempData.NearestGangAttack, TRUE)
			NET_PRINT("[spawning] MOVE_POINT_OUTSIDE_EXCLUSION_ZONES - moved outside gang attack blip ") 
			bReturn = TRUE
		ENDIF
	ENDIF	
	
	IF (bMissionCoronas)
		IF IS_POINT_IN_NEAREST_MISSION_CORONA(vCoords, g_SpawnData.SearchTempData.NearestCoronas, TRUE)
			NET_PRINT("[spawning] MOVE_POINT_OUTSIDE_EXCLUSION_ZONES - moved outside mission corona ") 
			bReturn = TRUE
		ENDIF
	ENDIF
	
	RETURN bReturn
ENDFUNC

PROC MOVE_SPECIFIC_SPAWN_LOCATION_OUTSIDE_EXCLUSION_ZONES(BOOL bGlobalExclusionAreas=TRUE, BOOL bMissionExclusionAreas=TRUE, BOOL bGangAttackZones=TRUE, BOOL bMissionCoronas=TRUE)


	IF IS_THREAD_ACTIVE(g_SpecificSpawnLocation.Thread_ID)
		IF NOT (g_SpecificSpawnLocation.Thread_ID = GET_ID_OF_THIS_THREAD())	
			DEBUG_PRINTCALLSTACK()
			NET_PRINT("[spawning] MOVE_SPECIFIC_SPAWN_LOCATION_OUTSIDE_EXCLUSION_ZONES - in use by another script!") NET_NL()
			SCRIPT_ASSERT("MOVE_SPECIFIC_SPAWN_LOCATION_OUTSIDE_EXCLUSION_ZONES - in use by another script!")
			EXIT
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] MOVE_SPECIFIC_SPAWN_LOCATION_OUTSIDE_EXCLUSION_ZONES - called with: ") NET_NL()
	NET_PRINT("    bGlobalExclusionAreas = ") NET_PRINT_BOOL(bGlobalExclusionAreas) NET_NL()
	NET_PRINT("    bMissionExclusionAreas = ") NET_PRINT_BOOL(bMissionExclusionAreas) NET_NL()
	NET_PRINT("    bGangAttackZones = ") NET_PRINT_BOOL(bGangAttackZones) NET_NL()
	NET_PRINT("    bMissionCoronas = ") NET_PRINT_BOOL(bMissionCoronas) NET_NL()
	NET_PRINT("    g_SpecificSpawnLocation.vCoords = ") NET_PRINT_VECTOR(g_SpecificSpawnLocation.vCoords) NET_NL()
	DEBUG_PRINTCALLSTACK()
	
	IF (g_SpecificSpawnLocation.bUseAngledArea)
		NET_PRINT("MOVE_SPECIFIC_SPAWN_LOCATION_OUTSIDE_EXCLUSION_ZONES - can't be called on angled area specific location")
		SCRIPT_ASSERT("MOVE_SPECIFIC_SPAWN_LOCATION_OUTSIDE_EXCLUSION_ZONES - can't be called on angled area specific location")
	ENDIF
	
	#ENDIF
	
	
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA].ExclusionArea.bIsActive = TRUE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA_UNDER_CHILLIAD].ExclusionArea.bIsActive = TRUE

	IF MOVE_POINT_OUTSIDE_EXCLUSION_ZONES(g_SpecificSpawnLocation.vCoords, bGlobalExclusionAreas, bMissionExclusionAreas, bGangAttackZones, bMissionCoronas)
		g_SpecificSpawnLocation.fUpperZLimitForNodes = 9999.9 
		PRINTLN("[spawning] MOVE_SPECIFIC_SPAWN_LOCATION_OUTSIDE_EXCLUSION_ZONES - moved point, so ignoring z limit for nodes.")
	ENDIF

	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA].ExclusionArea.bIsActive = FALSE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA_UNDER_CHILLIAD].ExclusionArea.bIsActive = FALSE

ENDPROC



PROC SET_RACE_CORONA_SECONDARY_RESPAWN_COORDS(VECTOR& vCoords[])
	INT i
	REPEAT FMMC_MAX_NUM_RACERS i
		IF (i < COUNT_OF(vCoords))
			g_SpawnData.MissionSpawnDetails.vSecondaryRaceRespawn[i] = vCoords[i]
			NET_PRINT("SET_RACE_CORONA_SECONDARY_RESPAWN_COORDS - setting vSecondaryRaceRespawn[") NET_PRINT_INT(i) NET_PRINT("] = ") NET_PRINT_VECTOR(g_SpawnData.MissionSpawnDetails.vSecondaryRaceRespawn[i]) NET_NL()
		ELSE
			g_SpawnData.MissionSpawnDetails.vSecondaryRaceRespawn[i] = <<0.0, 0.0, 0.0>>
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF (g_SpawnData.bIgnoreRaceSecondaryRespawns)
				g_SpawnData.MissionSpawnDetails.vSecondaryRaceRespawn[i] = <<0.0, 0.0, 0.0>>
			ENDIF
		#ENDIF		
	ENDREPEAT
ENDPROC

PROC SET_SPAWN_OCCLUSION_SPHERE_FOR_PLAYERS_NOT_ON_THIS_MISSION(VECTOR vCoords, FLOAT fRadius, BOOL bActive, BOOL bIgnoreMissionCheck = FALSE)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF (bIgnoreMissionCheck)
		OR IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())			
			BROADCAST_SPAWN_OCCLUSION_AREA_FOR_THIS_MISSION(vCoords, <<0.0, 0.0, 0.0>>, fRadius, SPAWN_AREA_SHAPE_CIRCLE, GET_MP_MISSION_PLAYER_IS_ON(PLAYER_ID()), GET_MP_MISSION_INSTANCE_PLAYER_IS_ON(PLAYER_ID()), bActive)
		ELSE
			SCRIPT_ASSERT("SET_SPAWN_OCCLUSION_AREA_FOR_PLAYERS_NOT_ON_THIS_MISSION - player is not on a mission.")
		ENDIF
	ELSE
		SCRIPT_ASSERT("SET_SPAWN_OCCLUSION_AREA_FOR_PLAYERS_NOT_ON_THIS_MISSION - Should only be called by mission host.")
	ENDIF
ENDPROC

PROC SET_SPAWN_OCCLUSION_BOX_FOR_PLAYERS_NOT_ON_THIS_MISSION(VECTOR vMinCoords, VECTOR vMaxCoords, BOOL bActive, BOOL bIgnoreMissionCheck = FALSE )
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()		
		IF (bIgnoreMissionCheck)
		OR IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())			
			MakeMinMaxValuesSafe(vMinCoords, vMaxCoords)
			BROADCAST_SPAWN_OCCLUSION_AREA_FOR_THIS_MISSION(vMinCoords, vMaxCoords, 0.0, SPAWN_AREA_SHAPE_BOX, GET_MP_MISSION_PLAYER_IS_ON(PLAYER_ID()), GET_MP_MISSION_INSTANCE_PLAYER_IS_ON(PLAYER_ID()), bActive)
		ELSE
			SCRIPT_ASSERT("SET_SPAWN_OCCLUSION_AREA_FOR_PLAYERS_NOT_ON_THIS_MISSION - player is not on a mission.")
		ENDIF
	ELSE
		SCRIPT_ASSERT("SET_SPAWN_OCCLUSION_AREA_FOR_PLAYERS_NOT_ON_THIS_MISSION - Should only be called by mission host.")
	ENDIF
ENDPROC

PROC SET_SPAWN_OCCLUSION_ANGLED_AREA_FOR_PLAYERS_NOT_ON_THIS_MISSION(VECTOR vCoords1, VECTOR vCoords2, FLOAT fWidth, BOOL bActive, BOOL bIgnoreMissionCheck = FALSE )
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()		
		IF (bIgnoreMissionCheck)
		OR IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())			
			BROADCAST_SPAWN_OCCLUSION_AREA_FOR_THIS_MISSION(vCoords1, vCoords2, fWidth, SPAWN_AREA_SHAPE_ANGLED, GET_MP_MISSION_PLAYER_IS_ON(PLAYER_ID()), GET_MP_MISSION_INSTANCE_PLAYER_IS_ON(PLAYER_ID()), bActive)
		ELSE
			SCRIPT_ASSERT("SET_SPAWN_OCCLUSION_AREA_FOR_PLAYERS_NOT_ON_THIS_MISSION - player is not on a mission.")
		ENDIF
	ELSE
		SCRIPT_ASSERT("SET_SPAWN_OCCLUSION_AREA_FOR_PLAYERS_NOT_ON_THIS_MISSION - Should only be called by mission host.")
	ENDIF
ENDPROC


PROC CLEAR_SPAWN_OCCLUSION_AREA_FOR_PLAYERS_NOT_ON_THIS_MISSION()
	INT i
	REPEAT MAX_NUM_MISSIONS_ALLOWED i
		IF (GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.bIsActive)
			IF (GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].Mission = GET_MP_MISSION_PLAYER_IS_ON(PLAYER_ID()))
			AND (GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].iMissionInstance = GET_MP_MISSION_INSTANCE_PLAYER_IS_ON(PLAYER_ID()))					
				SWITCH GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.iShape
					CASE SPAWN_AREA_SHAPE_CIRCLE 	
						SET_SPAWN_OCCLUSION_SPHERE_FOR_PLAYERS_NOT_ON_THIS_MISSION(<<0.0,0.0,0.0>>, 0.0, FALSE)
					BREAK
					CASE SPAWN_AREA_SHAPE_BOX		
						SET_SPAWN_OCCLUSION_BOX_FOR_PLAYERS_NOT_ON_THIS_MISSION(<<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, FALSE)
					BREAK
					CASE SPAWN_AREA_SHAPE_ANGLED	 
						SET_SPAWN_OCCLUSION_ANGLED_AREA_FOR_PLAYERS_NOT_ON_THIS_MISSION(<<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, 0.0, FALSE)
					BREAK
				ENDSWITCH			
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


PROC SET_MISSION_SPAWN_SPHERE(VECTOR vCoord, FLOAT fSpawnRadius, INT iIndex=0, BOOL bConsiderCentrePointAsValid=FALSE, BOOL bIgnoreMissionCheck = FALSE, FLOAT fIncreaseDistForEachFallback=0.0)
	#IF IS_DEBUG_BUILD
		IF (iIndex < 0)
			SCRIPT_ASSERT("SET_MISSION_SPAWN_SPHERE - iIndex < 0")
		ENDIF
		IF (iIndex >= MAX_NUMBER_OF_MISSION_SPAWN_AREAS)
			SCRIPT_ASSERT("SET_MISSION_SPAWN_SPHERE - iIndex >= MAX_NUMBER_OF_MISSION_SPAWN_AREAS")
		ENDIF
	#ENDIF
	//SetMissionSpawnArea(vCoord, fSpawnRadius, FALSE, <<0.0,0.0,0.0>>, iIndex)
	SetMissionSpawnArea(vCoord, <<0.0, 0.0, 0.0>>, fSpawnRadius, SPAWN_AREA_SHAPE_CIRCLE, iIndex, bIgnoreMissionCheck, bConsiderCentrePointAsValid, fIncreaseDistForEachFallback)
ENDPROC

PROC SET_MISSION_SPAWN_BOX(VECTOR vMinCoords, VECTOR vMaxCoords, INT iIndex=0)
	#IF IS_DEBUG_BUILD
		IF (iIndex < 0)
			SCRIPT_ASSERT("SET_MISSION_SPAWN_BOX - iIndex < 0")
		ENDIF
		IF (iIndex >= MAX_NUMBER_OF_MISSION_SPAWN_AREAS)
			SCRIPT_ASSERT("SET_MISSION_SPAWN_BOX - iIndex >= MAX_NUMBER_OF_MISSION_SPAWN_AREAS")
		ENDIF
	#ENDIF	
	//SetMissionSpawnArea(vMinCoords, 0.0, TRUE, vMaxCoords, iIndex)	
	SetMissionSpawnArea(vMinCoords, vMaxCoords, 0.0, SPAWN_AREA_SHAPE_BOX, iIndex)
ENDPROC

PROC SET_MISSION_SPAWN_ANGLED_AREA(VECTOR vAngledCoords1, VECTOR vAngledCoords2, FLOAT fWidth, INT iIndex=0, BOOL bIgnoreMissionCheck = FALSE)
	#IF IS_DEBUG_BUILD
		IF (iIndex < 0)
			SCRIPT_ASSERT("SET_MISSION_SPAWN_ANGLED_AREA - iIndex < 0")
		ENDIF
		IF (iIndex >= MAX_NUMBER_OF_MISSION_SPAWN_AREAS)
			SCRIPT_ASSERT("SET_MISSION_SPAWN_ANGLED_AREA - iIndex >= MAX_NUMBER_OF_MISSION_SPAWN_AREAS")
		ENDIF
	#ENDIF	
	//SetMissionSpawnArea(vMinCoords, 0.0, TRUE, vMaxCoords, iIndex)	
	SetMissionSpawnArea(vAngledCoords1, vAngledCoords2, fWidth, SPAWN_AREA_SHAPE_ANGLED, iIndex, bIgnoreMissionCheck)
ENDPROC


PROC SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_ON_THIS_MISSION(BOOL bBool, BOOL bForceVal = FALSE)
	IF IsPlayerOnAMissionOrEventForSpawningPurposes(PLAYER_ID())
	OR bForceVal
		g_SpawnData.MissionSpawnDetails.bConsiderInteriors = bBool
	ELSE
		PRINTLN("SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_ON_THIS_MISSION - player not on a mission!")
		SCRIPT_ASSERT("SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_ON_THIS_MISSION - player not on a mission!")	
	ENDIF
ENDPROC

PROC SET_PLAYERS_RESPAWN_TO_SPAWN_CLOSER_TO_ROADS_ON_THIS_MISSION(BOOL bBool)
	IF IsPlayerOnAMissionOrEventForSpawningPurposes(PLAYER_ID())
		g_SpawnData.MissionSpawnDetails.bDoNearARoadChecks = bBool
	ELSE
		PRINTLN("SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_ON_THIS_MISSION - player not on a mission!")
		SCRIPT_ASSERT("SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_ON_THIS_MISSION - player not on a mission!")	
	ENDIF
ENDPROC

PROC SET_PLAYER_RESPAWN_TO_CONSIDER_INTERIORS_FOR_DEATHMATCH(BOOL bBool)
	g_SpawnData.MissionSpawnDetails.bConsiderInteriors = bBool
ENDPROC

PROC SET_PLAYER_WILL_SPAWN_FACING_COORDS(VECTOR vCoords, BOOL bFaceCoords, BOOL bFaceAwayFromCoords)
	#IF IS_DEBUG_BUILD
	IF (bFaceCoords)
	AND (bFaceAwayFromCoords)
		SCRIPT_ASSERT("SET_PLAYER_WILL_SPAWN_FACING_COORDS - can't face coords AND face away from coords!")
	ENDIF
	
	IF (VDIST2(g_SpawnData.MissionSpawnDetails.vFacing, vCoords) > 0.0)
		PRINTLN("[spawning] SET_PLAYER_WILL_SPAWN_FACING_COORDS - setting new facing coords of ", vCoords)
		DEBUG_PRINTCALLSTACK()
	ENDIF
	
	#ENDIF
	g_SpawnData.MissionSpawnDetails.vFacing = vCoords
	g_SpawnData.MissionSpawnDetails.bFacePoint = bFaceCoords
	g_SpawnData.MissionSpawnDetails.bFaceAwayFromPoint = bFaceAwayFromCoords
ENDPROC

PROC SET_USE_RACE_RESPOT_AFTER_RESPAWNS(BOOL bSet)
	PRINTLN("[spawning] SET_USE_RACE_RESPOT_AFTER_RESPAWNS - called with ", bSet)	
	DEBUG_PRINTCALLSTACK()
	g_SpawnData.MissionSpawnDetails.bUseRaceRespotAfterRespawns = bSet
ENDPROC

PROC SET_PLAYER_WILL_SPAWN_USING_SAME_NODE_TYPE_AS_TARGET(BOOL bSet)
	PRINTLN("[spawning] SET_PLAYER_WILL_SPAWN_USING_SAME_NODE_TYPE_AS_TARGET - called with ", bSet)
	g_SpawnData.MissionSpawnDetails.bUseOffRoadChecking = bSet	
ENDPROC

PROC SET_PLAYER_VEHICLE_SPAWNING_CAN_USE_NAVMESH_FALLBACK(BOOL bSet)
	PRINTLN("[spawning] SET_PLAYER_VEHICLE_SPAWNING_CAN_USE_NAVMESH_FALLBACK - called with ", bSet)
	g_SpawnData.MissionSpawnDetails.bUseNavMeshFallback = bSet	
ENDPROC

PROC SET_CAR_NODE_SEARCH_LOWER_Z_LIMIT(FLOAT fLowerZValue)
	PRINTLN("[spawning] SET_CAR_NODE_SEARCH_WILL_USE_LESS_STRICT_VISIBILITY_CHECKS - called with ", fLowerZValue)
	g_SpawnData.MissionSpawnDetails.fCarNodeLowerZLimit = fLowerZValue	
ENDPROC



PROC SET_PLAYER_WILL_FACE_OCCLUSION_SPHERE(BOOL bFacePoint, BOOL bFaceAwayFromPoint=FALSE)

	VECTOR vCentre

	SWITCH g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[0].iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE 	
			SET_PLAYER_WILL_SPAWN_FACING_COORDS(g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[0].vCoords1, bFacePoint, bFaceAwayFromPoint)
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX		
		CASE SPAWN_AREA_SHAPE_ANGLED	
			vCentre = (g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[0].vCoords1 + g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[0].vCoords2) * 0.5
			SET_PLAYER_WILL_SPAWN_FACING_COORDS(vCentre, bFacePoint, bFaceAwayFromPoint)
		BREAK
	ENDSWITCH

ENDPROC

PROC SET_PLAYER_FACES_AWAY_FROM_OCCLUSION_SPHERE(BOOL bSet)
	IF (bSet)
		SET_PLAYER_WILL_FACE_OCCLUSION_SPHERE(FALSE, TRUE)	
	ELSE
		SET_PLAYER_WILL_FACE_OCCLUSION_SPHERE(g_SpawnData.MissionSpawnDetails.bFacePoint, FALSE)
	ENDIF
ENDPROC

PROC SET_MISSION_SPAWN_OCCLUSION_SPHERE(VECTOR vCoord, FLOAT fExclusionRadius = 0.0, INT iIndex=0, BOOL bIgnoreMissionCheck = FALSE)
	#IF IS_DEBUG_BUILD
		IF (iIndex < 0)
			SCRIPT_ASSERT("SET_MISSION_SPAWN_OCCLUSION_SPHERE - iIndex < 0")
		ENDIF
		IF (iIndex >= MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS)
			SCRIPT_ASSERT("SET_MISSION_SPAWN_OCCLUSION_SPHERE - iIndex >= MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS")
		ENDIF
	#ENDIF		
	SetMissionExclusionArea(vCoord, <<0.0, 0.0, 0.0>>, fExclusionRadius, SPAWN_AREA_SHAPE_CIRCLE, iIndex, bIgnoreMissionCheck)
	//SET_PLAYER_FACES_AWAY_FROM_OCCLUSION_SPHERE(FALSE)
ENDPROC

FUNC INT ADD_MISSION_SPAWN_OCCLUSION_SPHERE(VECTOR vCoord, FLOAT fRadius, BOOL bIgnoreMissionCheck = FALSE)
	INT iReturn = -1
	IF (GetTotalMissionExclusionAreas() < MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS)
		iReturn = GetFreeMissionExclusionIndex()
		SET_MISSION_SPAWN_OCCLUSION_SPHERE(vCoord, fRadius, iReturn, bIgnoreMissionCheck)
		PRINTLN("[spawning] ADD_MISSION_SPAWN_OCCLUSION_SPHERE(", vCoord, ", ", fRadius, ") - iReturn = ", iReturn) 
	ELSE
		PRINTLN("[spawning] ADD_MISSION_SPAWN_OCCLUSION_SPHERE - hit MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS")
		SCRIPT_ASSERT("[spawning] ADD_MISSION_SPAWN_OCCLUSION_SPHERE - hit MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS")
	ENDIF
	RETURN iReturn
ENDFUNC

PROC SET_MISSION_SPAWN_OCCLUSION_BOX(VECTOR vMinCoords, VECTOR vMaxCoords, INT iIndex=0, BOOL bIgnoreMissionCheck = FALSE)
	#IF IS_DEBUG_BUILD
		IF (iIndex < 0)
			SCRIPT_ASSERT("SET_MISSION_SPAWN_OCCLUSION_BOX - iIndex < 0")
		ENDIF
		IF (iIndex >= MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS)
			SCRIPT_ASSERT("SET_MISSION_SPAWN_OCCLUSION_BOX - iIndex >= MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS")
		ENDIF
	#ENDIF	
	SetMissionExclusionArea(vMinCoords, vMaxCoords, 0.0, SPAWN_AREA_SHAPE_BOX, iIndex, bIgnoreMissionCheck)
	//SET_PLAYER_FACES_AWAY_FROM_OCCLUSION_SPHERE(FALSE)
ENDPROC

FUNC INT ADD_MISSION_SPAWN_OCCLUSION_BOX(VECTOR vMinCoords, VECTOR vMaxCoords, BOOL bIgnoreMissionCheck = FALSE)
	INT iReturn = -1
	IF (GetTotalMissionExclusionAreas() < MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS)
		iReturn =  GetFreeMissionExclusionIndex()
		SET_MISSION_SPAWN_OCCLUSION_BOX(vMinCoords, vMaxCoords, iReturn, bIgnoreMissionCheck)
		PRINTLN("[spawning] ADD_MISSION_SPAWN_OCCLUSION_BOX(", vMinCoords, ", ", vMinCoords, ") - iReturn = ", iReturn) 
	ELSE
		PRINTLN("[spawning] ADD_MISSION_SPAWN_OCCLUSION_BOX - hit MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS")
		SCRIPT_ASSERT("[spawning] ADD_MISSION_SPAWN_OCCLUSION_BOX - hit MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS")
	ENDIF
	RETURN iReturn
ENDFUNC

PROC SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(VECTOR vAngledCoords1, VECTOR vAngledCoords2, FLOAT fWidth, INT iIndex=0, BOOL bIgnoreMissionCheck = FALSE)
	#IF IS_DEBUG_BUILD
		IF (iIndex < 0)
			SCRIPT_ASSERT("SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA - iIndex < 0")
		ENDIF
		IF (iIndex >= MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS)
			SCRIPT_ASSERT("SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA - iIndex >= MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS")
		ENDIF
	#ENDIF	
	SetMissionExclusionArea(vAngledCoords1, vAngledCoords2, fWidth, SPAWN_AREA_SHAPE_ANGLED, iIndex, bIgnoreMissionCheck)
	//SET_PLAYER_FACES_AWAY_FROM_OCCLUSION_SPHERE(FALSE)
ENDPROC

FUNC INT ADD_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(VECTOR vAngledCoords1, VECTOR vAngledCoords2, FLOAT fWidth, BOOL bIgnoreMissionCheck = FALSE)
	INT iReturn = -1
	IF (GetTotalMissionExclusionAreas() < MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS)
		iReturn = GetFreeMissionExclusionIndex()
		SET_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(vAngledCoords1, vAngledCoords2, fWidth, iReturn, bIgnoreMissionCheck)
		PRINTLN("[spawning] ADD_MISSION_SPAWN_OCCLUSION_ANGLED_AREA(", vAngledCoords1, ", ", vAngledCoords2, ", ", fWidth, ") - iReturn = ", iReturn) 
	ELSE
		PRINTLN("[spawning] ADD_MISSION_SPAWN_OCCLUSION_ANGLED_AREA - hit MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS")
		SCRIPT_ASSERT("[spawning] ADD_MISSION_SPAWN_OCCLUSION_ANGLED_AREA - hit MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS")
	ENDIF
	RETURN iReturn
ENDFUNC

PROC CLEAR_MISSION_SPAWN_OCCLUSION_AREA(INT iIndex)
	IF ((iIndex > -1) AND (iIndex < MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS))
		IF (g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[iIndex].bIsActive)
			SPAWN_AREA BlankSpawnArea
			g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[iIndex] = BlankSpawnArea
			PRINTLN("[spawning] CLEAR_MISSION_SPAWN_OCCLUSION_AREA - cleared index = ", iIndex)
		ELSE
			PRINTLN("[spawning] CLEAR_MISSION_SPAWN_OCCLUSION_AREA - already inactive. index = ", iIndex)
		ENDIF
	ELSE
		PRINTLN("[spawning] CLEAR_MISSION_SPAWN_OCCLUSION_AREA - invalid index = ", iIndex)
		SCRIPT_ASSERT("CLEAR_MISSION_SPAWN_OCCLUSION_AREA - invalid index")		
		DEBUG_PRINTCALLSTACK()
	ENDIF
ENDPROC

PROC SET_MISSION_DEFAULT_RESPAWN_LOCATION(PLAYER_SPAWN_LOCATION NextSpawnLocation)
	NET_PRINT("SET_MISSION_DEFAULT_RESPAWN_LOCATION - called by ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_PRINT(" set to value of ") NET_PRINT_INT(ENUM_TO_INT(NextSpawnLocation)) NET_NL()
	IF IS_THREAD_ACTIVE(g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocationThreadID)
	AND NOT (GET_ID_OF_THIS_THREAD() = g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocationThreadID)
		NET_PRINT("SET_MISSION_DEFAULT_RESPAWN_LOCATION - already set by another running script") NET_NL()
		SCRIPT_ASSERT("SET_MISSION_DEFAULT_RESPAWN_LOCATION - already set by another running script")
	ELSE
		g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocationThreadID = GET_ID_OF_THIS_THREAD()
		g_SpawnData.MissionSpawnGeneralBehaviours.DefaultSpawnLocation = NextSpawnLocation
	ENDIF
ENDPROC


PROC SET_COPS_WILL_RESPAWN_IN_CAR(BOOL bSet)
	IF (bSet)
		g_SpawnData.MissionSpawnDetails.bWillNotRespawnCopsInVehicles = FALSE
	ELSE
		g_SpawnData.MissionSpawnDetails.bWillNotRespawnCopsInVehicles = TRUE
	ENDIF
ENDPROC



PROC SET_PLAYER_CAN_SPAWN_IN_VIEW_OF_TEAM_MATES(BOOL bSet)
	IF (bSet)
		g_SpawnData.bDoVisibilityChecksOnTeammates = FALSE	
	ELSE
		g_SpawnData.bDoVisibilityChecksOnTeammates = TRUE
	ENDIF
ENDPROC

PROC SET_SPECIFIC_SPAWN_LOCATION_RACE_CORONA_OVERRIDE_INFO(FLOAT fOverrideDistFromLastCheckpoint, VECTOR vNextCheckpoint)
	IF (fOverrideDistFromLastCheckpoint < 1500.0)
		fOverrideDistFromLastCheckpoint = 1500.0
	ENDIF
  	g_SpawnData.MissionSpawnDetails.fSpecificSpawnLocationOverride = fOverrideDistFromLastCheckpoint
	g_SpawnData.MissionSpawnDetails.vNextRaceCheckpoint = vNextCheckpoint
ENDPROC

PROC SET_PLAYER_CAN_BE_KNOCKED_OFF_VEHICLE(VEHICLE_KNOCKOFF KnockOffState)
	NET_PRINT("[spawning] SET_PLAYER_CAN_BE_KNOCKED_OFF_VEHICLE(") NET_PRINT_INT(ENUM_TO_INT(KnockOffState)) NET_PRINT(")") NET_NL()
	DEBUG_PRINTCALLSTACK()
	g_SpawnData.MissionSpawnDetails.VehicleKnockOffState = KnockOffState
	SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), g_SpawnData.MissionSpawnDetails.VehicleKnockOffState)
	IF (g_SpawnData.MissionSpawnDetails.VehicleKnockOffState = KNOCKOFFVEHICLE_NEVER)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_FallsOutOfVehicleWhenKilled, TRUE)
	ELSE
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_FallsOutOfVehicleWhenKilled, FALSE)	
	ENDIF
ENDPROC

/// PURPOSE: 
///    If you want the player to respawn in a vehicle set this to true. 
/// PARAMS:
///    bBool - TRUE / FALSE
///    CarModel - optional - if you want them to spawn in a specific model, pass it in here. 
///    Otherwise the default gang model will be used or whatever model can be found in memory. 
/// 
///    NOTE: if the car model is not loaded in it will just choose a model that already is.
///   
PROC SET_PLAYER_RESPAWN_IN_VEHICLE(BOOL bBool, 
									MODEL_NAMES CarModel = DUMMY_MODEL_FOR_SCRIPT, 
									BOOL bUseLastCarIfDriveable=FALSE, 
									BOOL bDontUseVehicleNodes=FALSE, 
									BOOL bUsePersonalVehicleIfAvailable=FALSE,
									BOOL bSetVehicleStrong = FALSE,
									INT iColour = -1, 
									BOOL bSetTyresBulletProof = FALSE, 
									BOOL bSetAsNonSaveable=FALSE, 
									BOOL bSetAsNonModable = FALSE,
									BOOL bSetFireAfterGangBossMissionEnds = FALSE,
									BOOL bCanFaceOncomingTraffic = FALSE)
	
	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] calling SET_PLAYER_RESPAWN_IN_VEHICLE(") 
	IF (bBool)
	NET_PRINT(" TRUE, ")
	ELSE
	NET_PRINT(" FALSE, ")
	ENDIF
	IF NOT (CarModel = DUMMY_MODEL_FOR_SCRIPT)
		NET_PRINT(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(CarModel))
		NET_PRINT("(hash ") NET_PRINT_INT(ENUM_TO_INT(CarModel)) NET_PRINT(")")
	ELSE
		NET_PRINT("DUMMY_MODEL_FOR_SCRIPT")
	ENDIF
	NET_PRINT(", ")
	NET_PRINT_BOOL(bUseLastCarIfDriveable)
	
	NET_PRINT(", ")
	NET_PRINT_BOOL(bDontUseVehicleNodes)
	
	NET_PRINT(", ")
	NET_PRINT_BOOL(bUsePersonalVehicleIfAvailable)	
	
	NET_PRINT(", ")
	NET_PRINT_BOOL(bSetVehicleStrong)
	
	NET_PRINT(", ")
	NET_PRINT_INT(iColour)
	
	NET_PRINT(", ")
	NET_PRINT_BOOL(bSetTyresBulletProof)
	
	NET_PRINT(", ")
	NET_PRINT_BOOL(bSetAsNonSaveable)

	NET_PRINT(", ")
	NET_PRINT_BOOL(bSetAsNonModable)
	
	NET_PRINT(", ")
	NET_PRINT_BOOL(bSetFireAfterGangBossMissionEnds)

	NET_PRINT(", ")
	NET_PRINT_BOOL(bCanFaceOncomingTraffic)
	
	NET_PRINT(") from script ") NET_PRINT(GET_THIS_SCRIPT_NAME())
	NET_NL()
	#ENDIF	
	
	IF NOT (CarModel = DUMMY_MODEL_FOR_SCRIPT)
	AND NOT IS_MODEL_IN_CDIMAGE(CarModel)
		CarModel = TAILGATER
		NET_PRINT("[spawning] SET_PLAYER_RESPAWN_IN_VEHICLE - safety fallback, setting model as TAILGATER. ") 
	ENDIF
	
	g_SpawnData.MissionSpawnDetails.bSpawnInVehicle = bBool
	g_SpawnData.MissionSpawnDetails.SpawnModel = CarModel
	g_SpawnData.MissionSpawnDetails.bSpawnInLastVehicleIfDriveable = bUseLastCarIfDriveable
	g_SpawnData.MissionSpawnDetails.bDontUseVehicleNodes = bDontUseVehicleNodes
	g_SpawnData.MissionSpawnDetails.bUsePersonalVehicleToSpawn = bUsePersonalVehicleIfAvailable
	g_SpawnData.MissionSpawnDetails.bSetVehicleStrong = bSetVehicleStrong
	g_SpawnData.MissionSpawnDetails.bSetTyresBulletProof = bSetTyresBulletProof
	g_SpawnData.MissionSpawnDetails.iColour = iColour
	g_SpawnData.MissionSpawnDetails.bSetAsNonSaveable = bSetAsNonSaveable
	g_SpawnData.MissionSpawnDetails.bSetAsNonModable = bSetAsNonModable
	g_SpawnData.MissionSpawnDetails.bSetFireAfterGangBossMissionEnds = bSetFireAfterGangBossMissionEnds
	g_SpawnData.MissionSpawnDetails.bCanFaceOncomingTraffic = bCanFaceOncomingTraffic
	
	IF NOT (bBool)
		ClearVehicleSpawnInfo()
	ENDIF
	
	IF (bUseLastCarIfDriveable)
	
		VEHICLE_INDEX CarID 
		BOOL bSetAsMissionEntity
		
		CarID = GET_PLAYERS_LAST_USED_VEHICLE()
	
		IF DOES_ENTITY_EXIST(CarID)
			IF NOT IS_ENTITY_A_MISSION_ENTITY(CarID)
				IF NETWORK_GET_ENTITY_IS_LOCAL(CarID)
					SET_ENTITY_AS_MISSION_ENTITY(CarID, FALSE, TRUE )
					NET_PRINT("[spawning] SET_PLAYER_RESPAWN_IN_VEHICLE - setting vehicle as mission entity") NET_NL()
					bSetAsMissionEntity = TRUE
				ELSE
					NET_PRINT("[spawning] SET_PLAYER_RESPAWN_IN_VEHICLE - entity is not local") NET_NL()
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD(CarID)
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(CarID)
					IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(CarID)		
						NET_PRINT("[spawning] SET_PLAYER_RESPAWN_IN_VEHICLE - requesting control") NET_NL()
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(VEH_TO_NET(CarID))
					ELSE
						NET_PRINT("[spawning] SET_PLAYER_RESPAWN_IN_VEHICLE - already has control") NET_NL()	
					ENDIF
				ELSE
					NET_PRINT("[spawning] SET_PLAYER_RESPAWN_IN_VEHICLE - vehicle belongs to another script.") NET_NL()
				ENDIF
			ELSE
				NET_PRINT("[spawning] SET_PLAYER_RESPAWN_IN_VEHICLE - vehicle is dead.") NET_NL()
			ENDIF
			IF NETWORK_HAS_CONTROL_OF_ENTITY(CarID)	
				IF (bSetAsMissionEntity)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(CarID)
					NET_PRINT("[spawning] SET_PLAYER_RESPAWN_IN_VEHICLE - setting vehicle as no longer needed") NET_NL()
				ENDIF
				IF bSetVehicleStrong
					IF NOT IS_ENTITY_DEAD(CarID)
						SET_VEHICLE_STRONG(CarID,TRUE)
						NET_PRINT("[spawning] SET_PLAYER_RESPAWN_IN_VEHICLE - Setting vehicle as strong.") NET_NL()
					ENDIF
				ENDIF
				IF bSetTyresBulletProof
					IF NOT IS_ENTITY_DEAD(CarID)
						SET_VEHICLE_TYRES_CAN_BURST(CarID, FALSE)
						NET_PRINT("[spawning] SET_PLAYER_RESPAWN_IN_VEHICLE - Setting tyres as bullet proof") NET_NL()
					ENDIF
				ENDIF
				IF iColour > -1	
					PRINTLN( "[JJT MISSION][SPWANING] iColour equals: ", iColour )
					SET_VEHICLE_COLOURS( CarID, iColour, iColour )
					SET_VEHICLE_EXTRA_COLOURS( CarID, iColour, iColour )
				ENDIF
				IF (bSetFireAfterGangBossMissionEnds)
					ADD_DECORATOR_TO_SET_FIRE_AFTER_GANG_BOSS_MISSION_ENDS(CarID)
					PRINTLN( "[spawning] SET_PLAYER_RESPAWN_IN_VEHICLE bSetFireAfterGangBossMissionEnds")
				ENDIF
			ENDIF
				
		ENDIF
	
	ENDIF
ENDPROC

PROC CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()
	IF (g_SpawnData.NextSpawn.Thread_ID = GET_ID_OF_THIS_THREAD())
		NET_PRINT("[spawning] CLEAR_PLAYER_NEXT_RESPAWN_LOCATION - called from ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		NEXT_SPAWN_LOCATION_INFO EmptyNextLocation
		g_SpawnData.NextSpawn = EmptyNextLocation
	ENDIF
ENDPROC

PROC USE_CUSTOM_SPAWN_POINTS(BOOL bSet, 
							BOOL bIgnorePreviousSpawnPoint=TRUE, 
							BOOL bIgnoreObstructionChecks=FALSE, 
							FLOAT fPedClearanceRadius=CUSTOM_SPAWN_PED_CLEARANCE_RADIUS, 
							FLOAT fVehicleClearanceRadius=CUSTOM_SPAWN_VEHICLE_CLEARANCE_RADIUS, 
							FLOAT fWorldObjectClearance=CUSTOM_SPAWN_WORLD_OBJECT_CLEARANCE_RADIUS, 
							FLOAT fScriptObjectClearance=CUSTOM_SPAWN_SCRIPT_OBJECT_CLEARANCE_RADIUS, 
							FLOAT fMinDistWarper = CUSTOM_SPAWN_MIN_DIST_WARPER, 
							FLOAT fMinDistEnemy = CUSTOM_SPAWN_MIN_DIST_ENEMY, 
							FLOAT fMinDistTeammate = CUSTOM_SPAWN_MIN_DIST_TEAMMATE, 
							BOOL bIgnoreDistanceChecks = FALSE, 
							BOOL bNeverFallBackToNavMesh = FALSE,
							BOOL bAvoidHostileNPCs = TRUE, 
							BOOL bAvoidResultClumping = FALSE,
							FLOAT fMaxDistFromDeath = CUSTOM_SPAWN_MAX_DIST_FROM_DEATH)
	
	#IF IS_DEBUG_BUILD			
	NET_PRINT("[spawning] called USE_CUSTOM_SPAWN_POINTS(")
	NET_PRINT_BOOL(bSet)
	NET_PRINT(", ") 
	NET_PRINT_BOOL(bIgnorePreviousSpawnPoint)
	NET_PRINT(", ") 
	NET_PRINT_BOOL(bIgnoreObstructionChecks)
	NET_PRINT(", ") 
	NET_PRINT_FLOAT(fPedClearanceRadius)
	NET_PRINT(", ") 
	NET_PRINT_FLOAT(fVehicleClearanceRadius)
	NET_PRINT(", ") 
	NET_PRINT_FLOAT(fWorldObjectClearance)
	NET_PRINT(", ") 
	NET_PRINT_FLOAT(fScriptObjectClearance)
	NET_PRINT(", ") 
	NET_PRINT_FLOAT(fMinDistWarper)
	NET_PRINT(", ") 
	NET_PRINT_FLOAT(fMinDistEnemy)
	NET_PRINT(", ") 
	NET_PRINT_FLOAT(fMinDistTeammate)
	NET_PRINT(", ") 
	NET_PRINT_BOOL(bIgnoreDistanceChecks)
	NET_PRINT(", ") 
	NET_PRINT_BOOL(bNeverFallBackToNavMesh)
	NET_PRINT(", ") 
	NET_PRINT_BOOL(bAvoidHostileNPCs)	
	NET_PRINT(", ") 
	NET_PRINT_BOOL(bAvoidResultClumping)	
	NET_PRINT(", ")
	NET_PRINT_FLOAT(fMaxDistFromDeath)
	
	NET_PRINT(")  from script ") NET_PRINT(GET_THIS_SCRIPT_NAME())
	NET_NL()	
	DEBUG_PRINTCALLSTACK()
	#ENDIF
		
	IF (bSet)
						
		IF ARE_CUSTOM_SPAWN_POINTS_BEING_USED_BY_ANOTHER_THREAD()
			NET_PRINT("[spawning] g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID = ") NET_PRINT_INT(NATIVE_TO_INT(g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID)) NET_NL()
			PRINTLN("[spawning] USE_CUSTOM_SPAWN_POINTS(TRUE) - another thread is using custom spawn points!")
			STORE_CUSTOM_SPAWN_POINT_SETTINGS()					
		ENDIF							
		g_SpawnData.CustomSpawnPointInfo.CustomSpawnThreadID = GET_ID_OF_THIS_THREAD() 	
		#IF IS_DEBUG_BUILD
		g_SpawnData.CustomSpawnPointInfo.LastAddCustomSpawnTime = GET_NETWORK_TIME()
		g_SpawnData.CustomSpawnPointInfo.ScriptUsingCustomSpawnPoints = GET_THIS_SCRIPT_NAME()				
		#ENDIF			
		g_SpawnData.CustomSpawnPointInfo.bIgnorePreviousSpawnPoint = bIgnorePreviousSpawnPoint
		g_SpawnData.CustomSpawnPointInfo.bIgnoreObstructionChecks = bIgnoreObstructionChecks
		g_SpawnData.CustomSpawnPointInfo.bIgnoreDistanceChecks = bIgnoreDistanceChecks
		CLEAR_CUSTOM_SPAWN_POINTS()
		SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_CUSTOM_SPAWN_POINTS)	
		g_SpawnData.CustomSpawnPointInfo.bNeverFallBackToNavMesh = bNeverFallBackToNavMesh
		g_SpawnData.CustomSpawnPointInfo.fCustomSpawnPedClearance = fPedClearanceRadius
		g_SpawnData.CustomSpawnPointInfo.fCustomSpawnVehicleClearance = fVehicleClearanceRadius		
		g_SpawnData.CustomSpawnPointInfo.fCustomSpawnWorldObjectClearance = fWorldObjectClearance
		g_SpawnData.CustomSpawnPointInfo.fCustomSpawnScriptObjectClearance = fScriptObjectClearance
		g_SpawnData.CustomSpawnPointInfo.fCustomSpawnMinDistWarper = fMinDistWarper
		g_SpawnData.CustomSpawnPointInfo.fCustomSpawnMinDistEnemy = fMinDistEnemy
		g_SpawnData.CustomSpawnPointInfo.fCustomSpawnMinDistTeammate = fMinDistTeammate	
		g_SpawnData.CustomSpawnPointInfo.fCustomMaxDistFromDeath = fMaxDistFromDeath
		g_SpawnData.CustomSpawnPointInfo.bAvoidHostileNPCs = bAvoidHostileNPCs
		g_SpawnData.CustomSpawnPointInfo.bAvoidResultClumping = bAvoidResultClumping
		
		g_SpawnData.bUseCustomSpawnPoints = TRUE
	ELSE
	
		TurnOffCustomSpawnPoints()
		
		
		
	ENDIF
ENDPROC

PROC ADD_FALLBACK_SPAWN_POINT(VECTOR vPos, FLOAT fHeading #IF IS_DEBUG_BUILD, FLOAT fOverrideDistanceCheck = FALLBACK_SPAWN_POINT_MIN_DIST #ENDIF )
	AddFallbackSP(vPos,
					fHeading
					#IF IS_DEBUG_BUILD, fOverrideDistanceCheck #ENDIF
					)
					
ENDPROC

PROC ADD_CUSTOM_SPAWN_POINT(VECTOR vPos, FLOAT fHeading, FLOAT fWeighting=1.0 #IF IS_DEBUG_BUILD, FLOAT fOverrideDistanceCheck = CUSTOM_SPAWN_POINT_MIN_DIST #ENDIF )
	AddCustomSP(vPos, 
				fHeading, 
				fWeighting 
				#IF IS_DEBUG_BUILD, fOverrideDistanceCheck #ENDIF 
				)
ENDPROC

FUNC INT GET_NUMBER_OF_CUSTOM_SPAWN_POINTS()
	RETURN g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints
ENDFUNC

PROC CLEAR_MISSION_SPAWN_EXCLUSION_AREAS()
	
	PRINTLN("[spawning] CLEAR_MISSION_SPAWN_EXCLUSION_AREAS called")
	DEBUG_PRINTCALLSTACK()
	
	COPY_SCRIPT_STRUCT(g_SpawnData.MissionSpawnExclusionAreas, BlankMissionSpawnExclusionAreas, SIZE_OF(MISSION_SPAWN_EXCLUSION_AREAS)) // to help reduce stack size
	//g_SpawnData.MissionSpawnExclusionAreas = BlankMissionSpawnExclusionAreas
	
ENDPROC


/// PURPOSE:
///    Removes any defined spawn area for the player. Gets called automatically in the mission cleanup.
PROC CLEAR_SPAWN_AREA(BOOL bClearExclusionAreas=TRUE, BOOL bResetRestOfMissionData=TRUE, BOOL bKeepCustomSpawnPoints = FALSE)
	#IF IS_DEBUG_BUILD
	PRINTLN("[spawning] CLEAR_SPAWN_AREA(",bClearExclusionAreas, ", ", bResetRestOfMissionData, ")")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	//MISSION_SPAWN_DETAILS BlankMissionSpawnDetails // moved to a global in mp_globals_spawning.sch as it was breaking mission stacks when called.

	IF (bResetRestOfMissionData)
		COPY_SCRIPT_STRUCT(g_SpawnData.MissionSpawnDetails, BlankMissionSpawnDetails, SIZE_OF(MISSION_SPAWN_DETAILS)) // to help reduce stack size
		//g_SpawnData.MissionSpawnDetails = BlankMissionSpawnDetails
	ELSE	
		g_SpawnData.MissionSpawnDetails.SpawnArea = BlankMissionSpawnDetails.SpawnArea		
		g_SpawnData.MissionSpawnDetails.vFacing = BlankMissionSpawnDetails.vFacing
		g_SpawnData.MissionSpawnDetails.bFacePoint = BlankMissionSpawnDetails.bFacePoint
		g_SpawnData.MissionSpawnDetails.bFaceAwayFromPoint = BlankMissionSpawnDetails.bFaceAwayFromPoint			
	ENDIF
	
	IF (bClearExclusionAreas)
		CLEAR_MISSION_SPAWN_EXCLUSION_AREAS()
	ENDIF
	
	IF NOT bKeepCustomSpawnPoints
		USE_CUSTOM_SPAWN_POINTS(FALSE)
	ENDIF
	
	SET_PLAYER_CAN_SPAWN_IN_VIEW_OF_TEAM_MATES(FALSE)	
	
	// clear next spawn location 
	CLEAR_PLAYER_NEXT_RESPAWN_LOCATION()

ENDPROC


PROC SET_NEXT_RESPAWN_INVINCIBLE_TIME(INT iPlayerTime, INT iPlayerVehicleTime)

	#IF IS_DEBUG_BUILD
	IF NOT (g_SpawnData.iNextRespawnFlashTime = iPlayerTime)
	OR NOT (g_SpawnData.iNextRespawnVehicleRespotTime = iPlayerVehicleTime)
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[spawning] SET_NEXT_RESPAWN_INVINCIBLE_TIME - called with ", iPlayerTime, ", ", iPlayerVehicleTime)		
	ENDIF
	#ENDIF
	
	g_SpawnData.iNextRespawnFlashTime = iPlayerTime
	g_SpawnData.iNextRespawnVehicleRespotTime = iPlayerVehicleTime

ENDPROC


PROC RESET_NEXT_RESPAWN_INTERACTION()
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[spawning][interactions] RESET_NEXT_RESPAWN_INTERACTION - called  ")
	#ENDIF
	g_SpawnData.iRespawnInteractionType = -1
	g_SpawnData.iRespawnSelectedAnim = -1
	g_SpawnData.bRespawnInteractionFullBody = FALSE
	g_SpawnData.fRespawnInteractionStartPhase = 0.0
	g_SpawnData.bRespawnInteractionForceWeaponVisible = FALSE
ENDPROC

PROC SET_NEXT_RESPAWN_INTERACTION(INT iInteractionType, INT iSelectedAnim, BOOL bFullBody=FALSE, FLOAT fStartPhase = 0.0, BOOL bForceWeaponVisible=FALSE, BOOL bPersistForDurationOfScript = FALSE)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[spawning][interactions] SET_NEXT_RESPAWN_INTERACTION - called with ", iInteractionType, ", ", iSelectedAnim, ", ", bFullBody, ", ", fStartPhase, ", ", bForceWeaponVisible, ", ", bPersistForDurationOfScript)
	#ENDIF
	g_SpawnData.iRespawnInteractionType = iInteractionType
	g_SpawnData.iRespawnSelectedAnim = iSelectedAnim
	g_SpawnData.bRespawnInteractionFullBody = bFullBody
	g_SpawnData.fRespawnInteractionStartPhase = fStartPhase
	g_SpawnData.bRespawnInteractionForceWeaponVisible = bForceWeaponVisible
	g_SpawnData.bRespawnInteractionUsesMatchStart = FALSE // avoid any conflict
	g_SpawnData.RespawnInteractionThreadID = GET_ID_OF_THIS_THREAD()
	g_SpawnData.bRespawnInteractionPersists = bPersistForDurationOfScript	
ENDPROC


PROC SET_NEXT_RESPAWN_INTERACTION_USES_MATCHSTARTS(BOOL bSet, BOOL bPersistForDurationOfScript=FALSE)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[spawning][interactions] SET_NEXT_RESPAWN_INTERACTION_USES_MATCHSTARTS - called with ", bSet, ", ", bPersistForDurationOfScript)
	#ENDIF	
	IF (bSet)
		RESET_NEXT_RESPAWN_INTERACTION() // avoid any conflict
		g_SpawnData.bRespawnInteractionUsesMatchStart = TRUE
		g_SpawnData.RespawnInteractionThreadID = GET_ID_OF_THIS_THREAD()
		g_SpawnData.bRespawnInteractionPersists = bPersistForDurationOfScript
	ELSE
		g_SpawnData.bRespawnInteractionUsesMatchStart = FALSE
	ENDIF

ENDPROC


PROC RESET_NEXT_RESPAWN_INVINCIBLE_TIME()
	SET_NEXT_RESPAWN_INVINCIBLE_TIME(0, 0)	
ENDPROC


PROC CLEAR_SPAWNING_MISSION_DATA()
	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] CLEAR_SPAWNING_MISSION_DATA() called") NET_NL()
	DEBUG_PRINTCALLSTACK()
	#ENDIF

	COPY_SCRIPT_STRUCT(g_SpawnData.MissionSpawnGeneralBehaviours, BlankMissionSpawnGeneralBehaviours, SIZE_OF(MISSION_SPAWN_GENERAL_BEHAVIOURS)) // to help reduce stack size
	COPY_SCRIPT_STRUCT(g_SpawnData.MissionSpawnPersonalVehicleData, BlankMissionSpawnPersonalVehicleData, SIZE_OF(MISSION_SPAWN_PERSONAL_VEHICLE_DATA)) // to help reduce stack size
	//g_SpawnData.MissionSpawnGeneralBehaviours = BlankMissionSpawnGeneralBehaviours
	//g_SpawnData.MissionSpawnPersonalVehicleData = BlankMissionSpawnPersonalVehicleData

	CLEAR_FALLBACK_SPAWN_POINTS()

	CLEAR_SPAWN_AREA()

	RESET_NEXT_RESPAWN_INVINCIBLE_TIME()

ENDPROC



PROC AddSpawnResultsToServerRefusalList(SPAWN_RESULTS &SpawnResults)
	INT i
	NET_PRINT("[spawning] AddSpawnResultsToServerRefusalList - called...") NET_NL()
	REPEAT NUM_OF_STORED_SPAWN_RESULTS i
		AddPointToServerRefusalList(SpawnResults.vCoords[i])
	ENDREPEAT
ENDPROC

FUNC BOOL HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE(VEHICLE_INDEX VehicleID, INT &iSeat, INT preferredSeat = VS_INVALID)

	BOOL bReturn = FALSE
	
	IF NOT (GET_ID_OF_THIS_THREAD() = g_SpawnData.WarpRequestIntoVehicleThreadID)
		IF NOT (g_SpawnData.iWarpIntoVehicleState = WARP_STATE_INIT)
			IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iHasGotSpecificSeatUpdateTime) < SpawningFunctionTimeoutValue())
			
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE - another HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE already started, returning FALSE - new call from ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
				#ENDIF					
				
				// return FALSE, to give the previous net_warp a chance to finish
				RETURN(FALSE)
				
			ELSE		
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE - another HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE already started, but not been updated in a while, so starting new one from script ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
				#ENDIF
				g_SpawnData.iWarpIntoVehicleState = WARP_STATE_INIT
			ENDIF
		ENDIF
	ELSE
		IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iWarpIntoVehicleRequestTime) > 30000)
			g_SpawnData.iWarpIntoVehicleState = WARP_STATE_INIT
			#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE - resetting state to INIT") NET_NL()
			#ENDIF	
		ENDIF
	ENDIF
	
	// is this the first time this has been called?
	IF (g_SpawnData.iWarpIntoVehicleState = WARP_STATE_INIT)
		g_SpawnData.iWarpIntoVehicleSeat = 0
		g_SpawnData.iWarpIntoVehicleServerRequestCount = 0 // reset this to zero	
		g_SpawnData.iWarpIntoVehicleServerTimeoutCount = 0		
		g_SpawnData.iWarpIntoVehicleRequestTime = GET_NETWORK_TIME()
		g_SpawnData.WarpRequestIntoVehicleThreadID = GET_ID_OF_THIS_THREAD()		
		g_SpawnData.iWarpIntoVehicleState = WARP_STATE_SEND_REQUEST		
		g_SpawnData.iHasGotSpecificSeatUpdateTime = GET_NETWORK_TIME()
		g_SpawnData.iAttemptWarpIntoVehicle = 0
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE - initialised by ")  NET_PRINT(GET_THIS_SCRIPT_NAME())  NET_NL()
		#ENDIF
	ENDIF	
	
	SWITCH g_SpawnData.iWarpIntoVehicleState
	
		// send reqeust
		CASE WARP_STATE_SEND_REQUEST		

			g_SpawnData.iWarpIntoVehicleSeat = GetFirstFreeSeatOfVehicle(VehicleID, DEFAULT, DEFAULT, DEFAULT, preferredSeat)

			BROADCAST_REQUEST_TO_WARP_INTO_VEHICLE(VehicleID, g_SpawnData.iWarpIntoVehicleSeat)	
			g_SpawnData.bServerRepliedToWarpIntoVehicleRequest = FALSE
			g_SpawnData.bServerAgreedToWarpIntoVehicleRequest = FALSE								
			g_SpawnData.iWarpIntoVehicleRequestSendTime = GET_NETWORK_TIME()
			g_SpawnData.iWarpIntoVehicleRequestTime = GET_NETWORK_TIME()
			g_SpawnData.iWarpIntoVehicleState = WARP_STATE_WAITNG_FOR_RESPONSE														
			#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE - sent request, attempt ") 
			NET_PRINT_INT(g_SpawnData.iWarpIntoVehicleServerRequestCount) NET_PRINT(", iWarpIntoVehicleRequestSendTime = ") NET_PRINT_INT(NATIVE_TO_INT(g_SpawnData.iWarpIntoVehicleRequestSendTime))
			NET_NL()
			#ENDIF	

		BREAK	
		
		// wait for server response
		CASE WARP_STATE_WAITNG_FOR_RESPONSE
			IF (g_SpawnData.bServerRepliedToWarpIntoVehicleRequest)
				IF (g_SpawnData.bServerAgreedToWarpIntoVehicleRequest)											
					
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE - success! server said ok.") NET_NL()
					#ENDIF
					bReturn = TRUE

				ELSE
					// server said no, should we give up or try again?
					IF (g_SpawnData.iWarpIntoVehicleServerRequestCount < MAX_NUM_OF_SERVER_WARP_REQUESTS)

						// send another request
						g_SpawnData.iWarpIntoVehicleServerRequestCount++
						g_SpawnData.iAttemptWarpIntoVehicle++
						g_SpawnData.iWarpIntoVehicleState = 	WARP_STATE_SEND_REQUEST
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE - server said no, send another request") NET_NL()
						#ENDIF
					ELSE
						// give up and just go with what we've got
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE - gave up because server kept saying no.") NET_NL()
						#ENDIF
						g_SpawnData.iWarpIntoVehicleState = 	WARP_STATE_GAVE_UP
					ENDIF
				ENDIF
				

			ELSE
				// has the server taken too long?
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iWarpIntoVehicleRequestSendTime) > MAX_SERVER_WARP_RESPONSE_TIME)
				OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iWarpIntoVehicleRequestSendTime) < 0)
					
					IF (g_SpawnData.iWarpIntoVehicleServerTimeoutCount < MAX_NUM_OF_SERVER_WARP_TIMEOUTS)
						
						// send another request
						g_SpawnData.iWarpIntoVehicleServerTimeoutCount++
						g_SpawnData.iWarpIntoVehicleState = WARP_STATE_SEND_REQUEST
						
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE - server taking too long to reply, sending again.") NET_NL()
						#ENDIF
						
					ELSE
						// just ok the request
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE - server took too long to reply.") NET_NL()
						#ENDIF
						g_SpawnData.iWarpIntoVehicleState = 	WARP_STATE_GAVE_UP
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE - waiting for server response.... time=") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iWarpIntoVehicleRequestSendTime)) NET_NL()
					#ENDIF	
				ENDIF
			ENDIF
		BREAK
			
		CASE WARP_STATE_GAVE_UP				
			g_SpawnData.iWarpIntoVehicleSeat = GetFirstFreeSeatOfVehicle(VehicleID)
			#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE - going to WARP_STATE_WAIT_FRAME after WARP_STATE_GAVE_UP.") NET_NL()
			#ENDIF
			g_SpawnData.iWarpIntoVehicleState = WARP_STATE_WAIT_FRAME
		BREAK		
	
		CASE WARP_STATE_WAIT_FRAME
			#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE - returning TRUE after WARP_STATE_GAVE_UP.") NET_NL()
			#ENDIF
			bReturn = TRUE
		BREAK
			
	ENDSWITCH
	
	g_SpawnData.iHasGotSpecificSeatUpdateTime = GET_NETWORK_TIME()
	
	IF (bReturn)
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE - returning TRUE, warping into seat ") NET_PRINT_INT(g_SpawnData.iWarpIntoVehicleSeat) NET_NL()
		#ENDIF
				
		iSeat = g_SpawnData.iWarpIntoVehicleSeat
		
		g_SpawnData.iAttemptWarpIntoVehicle = 0
		g_SpawnData.iWarpIntoVehicleState = WARP_STATE_INIT
		
		RETURN(TRUE)
	ELSE
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE - returning FALSE") NET_NL()
		#ENDIF
		RETURN(FALSE)
	ENDIF
	
	
ENDFUNC

PROC TryAgainToRequestSpawnLocation(INT iMaxAttempt=-1)

	// server said no, should we give up or try again?
	IF (g_SpawnData.iWarpServerRequestCount < MAX_NUM_OF_SERVER_WARP_REQUESTS)
		
		IF (iMaxAttempt = -1)
		OR ((iMaxAttempt > -1) AND (g_SpawnData.iAttempt < iMaxAttempt))
		
			// send another request
			g_SpawnData.iWarpServerRequestCount++
			g_SpawnData.iAttempt++
			g_SpawnData.iWarpState = 	WARP_STATE_SEND_REQUEST
			#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - TryAgainToRequestSpawnLocation - tring again") NET_NL()
			#ENDIF
			
		ELSE
		
			#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - TryAgainToRequestSpawnLocation - hit max attempts.") NET_PRINT_INT(iMaxAttempt) NET_NL()
			#ENDIF
			g_SpawnData.iWarpState = 	WARP_STATE_GAVE_UP		
		
		ENDIF
	ELSE
		// give up and just go with what we've got
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - TryAgainToRequestSpawnLocation - gave up.") NET_NL()
		#ENDIF
		g_SpawnData.iWarpState = 	WARP_STATE_GAVE_UP
	ENDIF
						
ENDPROC


/// PURPOSE: 
///    Finds a suitable and safe point for spawning.
/// PARAMS:
///	   vCoords - pass in by ref. Function will set this to suitable coord.
///    fHeading - pass in by ref. 
///    SpawnLocation = where do we want the player to spawn (see enum PLAYER_SPAWN_LOCATION)
///    bSpawnInVehicle - if passed in true, will return coords of a suitable road node. 
/// RETURNS:
///    TRUE - when a safe point has been found and verified by the server.
FUNC BOOL HAS_GOT_SPAWN_LOCATION(	VECTOR &vCoords, 
									FLOAT &fHeading, 
									PLAYER_SPAWN_LOCATION SpawnLocation = SPAWN_LOCATION_AUTOMATIC, 
									BOOL bForVehicle = FALSE, 
									BOOL bDoTeammateVisCheck=TRUE, 
									BOOL bDontAskPermission=FALSE, 
									BOOL bDontUseVehicleNodes = FALSE, 
									BOOL bUseCarNodeOffset=TRUE, 
									BOOL bUseStartOfMissionPVSpawn = FALSE, 
									FLOAT fMinEnemyDist=0.0, 
									BOOL bCheckEntityArea=FALSE, 
									MODEL_NAMES eOverrideModel=DUMMY_MODEL_FOR_SCRIPT )

	BOOL bReturn = FALSE
	BOOL bEntityAreaCreated = FALSE
	VECTOR vec
	FLOAT fEntityAreaRadius
	
	/*
	#IF IS_DEBUG_BUILD
	NET_PRINT("[Spawning] HAS_GOT_SPAWN_LOCATION - vCoords = ") NET_PRINT_VECTOR(vCoords) NET_NL()
	NET_PRINT("[Spawning] HAS_GOT_SPAWN_LOCATION - fHeading = ") NET_PRINT_FLOAT(fHeading) NET_NL()
	NET_PRINT("[Spawning] HAS_GOT_SPAWN_LOCATION - SpawnLocation = ") NET_PRINT_INT(ENUM_TO_INT(SpawnLocation)) NET_NL()
	NET_PRINT("[Spawning] HAS_GOT_SPAWN_LOCATION - bForVehicle = ") NET_PRINT_BOOL(bForVehicle) NET_NL()
	NET_PRINT("[Spawning] HAS_GOT_SPAWN_LOCATION - bDoTeammateVisCheck = ") NET_PRINT_BOOL(bDoTeammateVisCheck) NET_NL()
	NET_PRINT("[Spawning] HAS_GOT_SPAWN_LOCATION - bDontAskPermission = ") NET_PRINT_BOOL(bDontAskPermission) NET_NL()
	NET_PRINT("[Spawning] HAS_GOT_SPAWN_LOCATION - bDontUseVehicleNodes = ") NET_PRINT_BOOL(bDontUseVehicleNodes) NET_NL()
	NET_PRINT("[Spawning] HAS_GOT_SPAWN_LOCATION - bUseCarNodeOffset = ") NET_PRINT_BOOL(bUseCarNodeOffset) NET_NL()
	NET_PRINT("[Spawning] HAS_GOT_SPAWN_LOCATION - bUseStartOfMissionPVSpawn = ") NET_PRINT_BOOL(bUseStartOfMissionPVSpawn) NET_NL()
	NET_PRINT("[Spawning] HAS_GOT_SPAWN_LOCATION - fMinEnemyDist") NET_PRINT_FLOAT(fMinEnemyDist) NET_NL()
	NET_PRINT("[Spawning] HAS_GOT_SPAWN_LOCATION - bCheckEntityArea = ") NET_PRINT_BOOL(bCheckEntityArea) NET_NL()
	NET_PRINT("[Spawning] HAS_GOT_SPAWN_LOCATION - eOverrideModel = ") NET_PRINT_INT(ENUM_TO_INT(eOverrideModel)) NET_NL()
	#ENDIF
	*/

	#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("[spawning] HAS_GOT_SPAWN_LOCATION - g_SpawnData.iWarpState = ", g_SpawnData.iWarpState)
	#ENDIF 	
	
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	// incase the transition tries to call this too early
	IF NOT IS_FAKE_MULTIPLAYER_MODE_ALLOWING_RESPAWNS()
		IF NOT IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
		AND NOT (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iGameState = MAIN_GAME_STATE_HOLD_FOR_TRANSITION)
			#IF IS_DEBUG_BUILD		
				NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - returning FALSE early.") NET_NL()
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
					NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT = FALSE ") NET_NL()
				ENDIF
				NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iGameState = ") NET_PRINT_INT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iGameState) NET_NL()
			#ENDIF
			
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[spawning] HAS_GOT_SPAWN_LOCATION - returning FALSE early")
				PRINTLN_FINAL("[spawning] GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iGameState = ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iGameState)
			#ENDIF
			RETURN(FALSE)	
		ENDIF
	ENDIF
	
	// if a HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS is ongoing then wait until it is finished.
	IF NOT (g_SpawnData.iSpawnVehicleState = SPAWN_VEHICLE_INIT)
	AND (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iHasGotVehicleSpawnLocationUpdateTime) < SpawningFunctionTimeoutValue())
		#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - another HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS is on-going ") NET_NL()
		#ENDIF
		#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("[spawning] HAS_GOT_SPAWN_LOCATION - another HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS is on-going")
		#ENDIF
		g_SpawnData.iHAS_GOT_SPAWN_LOCATION_waiting_frame = GET_FRAME_COUNT()
		RETURN(FALSE)
	ENDIF

	// should we check the entity area?
	IF (bCheckEntityArea)
		IF IS_PLAYER_SWITCH_IN_PROGRESS() 
			#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - setting bCheckEntityArea to FALSE due to IS_PLAYER_SWITCH_IN_PROGRESS ") NET_NL()
			#ENDIF	
			bCheckEntityArea = FALSE
		ENDIF
	ENDIF

	
	IF NOT (GET_ID_OF_THIS_THREAD() = g_SpawnData.WarpRequestThreadID)
		IF NOT (g_SpawnData.iWarpState = WARP_STATE_INIT)
			IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iHasGotSpawnLocationUpdateTime) < SpawningFunctionTimeoutValue())
			
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - another HAS_GOT_SPAWN_LOCATION already started, returning FALSE - new call from ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
				#ENDIF	
				#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[spawning] HAS_GOT_SPAWN_LOCATION - another HAS_GOT_SPAWN_LOCATION already started, returning FALSE - new call from ", GET_THIS_SCRIPT_NAME())
				#ENDIF				
				
				// return FALSE, to give the previous net_warp a chance to finish
				RETURN(FALSE)
				
			ELSE		
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - another HAS_GOT_SPAWN_LOCATION already started, but not been updated in a while, so starting new one from script ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
				#ENDIF
				g_SpawnData.iWarpState = WARP_STATE_INIT
			ENDIF
		ENDIF
	ELSE
		IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iWarpRequestTime) > 30000)
			g_SpawnData.iWarpState = WARP_STATE_INIT
			#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - resetting state to INIT") NET_NL()
			#ENDIF	
		ENDIF
	ENDIF
	
	
	// is this the first time this has been called?
	IF (g_SpawnData.iWarpState = WARP_STATE_INIT)
		g_SpawnData.iWarpServerRequestCount = 0 // reset this to zero	
		g_SpawnData.iWarpServerTimeoutCount = 0
		g_SpawnData.iServerSpawnResultsSlot = 0 		
		g_SpawnData.iWarpRequestTime = GET_NETWORK_TIME()
		g_SpawnData.WarpRequestThreadID = GET_ID_OF_THIS_THREAD()		
		g_SpawnData.iWarpState = WARP_STATE_SEND_REQUEST		
		g_SpawnData.iHasGotSpawnLocationUpdateTime = GET_NETWORK_TIME()
		g_SpawnData.bDontAskPermission = bDontAskPermission
		g_SpawnData.iAttempt = 0
		ClearServerRefusalList()
		IF (bCheckEntityArea)
			Cleanup_Entity_Area_For_Spawning_Vehicle()
		ENDIF
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - initialised by ")  NET_PRINT(GET_THIS_SCRIPT_NAME())  NET_NL()
		#ENDIF
	ENDIF

	SWITCH g_SpawnData.iWarpState
	
		// send reqeust
		CASE WARP_STATE_SEND_REQUEST		
			IF GetSpawnLocationCoords(g_SpawnData.RequestedSpawnResults, SpawnLocation, bForVehicle, bDoTeammateVisCheck, bDontUseVehicleNodes, bUseCarNodeOffset, bUseStartOfMissionPVSpawn, eOverrideModel)				
				// no need to go any further if we are using specific coords
				IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS)
				OR (g_SpawnData.bDontAskPermission) // don't ask server permission, used for initial spawning of dm
					#IF IS_DEBUG_BUILD
						IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS)
							NET_PRINT("HAS_GOT_SPAWN_LOCATION - SPAWN_LOCATION_AT_SPECIFIC_COORDS - returning TRUE ")NET_NL()
						ENDIF
						IF (g_SpawnData.bDontAskPermission)
							NET_PRINT("HAS_GOT_SPAWN_LOCATION - bDontAskPermission - returning TRUE ")NET_NL()
						ENDIF
					#ENDIF		
					bReturn = TRUE								
				ELSE	
					
					BROADCAST_REQUEST_TO_WARP(g_SpawnData.RequestedSpawnResults, g_SpawnData.iRequestToWarpID, fMinEnemyDist, (g_SpawnData.SpawnLocation = SPAWN_LOCATION_NEAR_CURRENT_POSITION_AS_POSSIBLE), eOverrideModel)	
					g_SpawnData.bServerRepliedToWarpRequest = FALSE
					g_SpawnData.bServerAgreedToWarpRequest = FALSE								
					g_SpawnData.iWarpRequestSendTime = GET_NETWORK_TIME()
					g_SpawnData.iWarpRequestTime = GET_NETWORK_TIME()
					g_SpawnData.iWarpState = WARP_STATE_WAITNG_FOR_RESPONSE														
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - sent request, attempt ") 
					NET_PRINT_INT(g_SpawnData.iWarpServerRequestCount) NET_PRINT(", iWarpRequestSendTime = ") NET_PRINT_INT(NATIVE_TO_INT(g_SpawnData.iWarpRequestSendTime))
					NET_PRINT(" iRequestToWarpID = ") NET_PRINT_INT(g_SpawnData.iRequestToWarpID)
					NET_NL()
					#ENDIF				
				ENDIF				
			ENDIF
		BREAK
	
		
		// wait for server response
		CASE WARP_STATE_WAITNG_FOR_RESPONSE
		
			#IF IS_DEBUG_BUILD
				IF IS_FAKE_MULTIPLAYER_MODE_ALLOWING_RESPAWNS()
					g_SpawnData.iWarpState = WARP_STATE_CHECK_ENTITY_AREA
					PRINTLN("[spawning] HAS_GOT_SPAWN_LOCATION - (g_Private_MultiplayerCreatorAllowDeathAndRespawning) Moving straight to WARP_STATE_CHECK_ENTITY_AREA.")
					RETURN FALSE
				ENDIF
			#ENDIF
			
			IF (g_SpawnData.bServerRepliedToWarpRequest)
				IF (g_SpawnData.iResponseToWarpID = g_SpawnData.iRequestToWarpID)
			
					IF (g_SpawnData.bServerAgreedToWarpRequest)											
					AND NOT (g_SpawnData.iServerSpawnResultsSlot = -1)	
						
						// if we are are trying to spawn near gang members, check if any have since arrived.
						IF (g_SpawnData.bSpawningOnOwnInGang)
						AND NOT IsPlayerAloneInGang() 
						AND AreAnyOtherGangMembersAliveOrRespawningInArea(g_SpecificSpawnLocation.vCoords, g_SpecificSpawnLocation.fRadius, vec, TRUE)
						AND (g_SpawnData.iAttempt = 0) // only try this on the first attempt

							#IF IS_DEBUG_BUILD
							PRINTLN("[spawning] HAS_GOT_SPAWN_LOCATION - trying again to find point close to gang member")
							#ENDIF
							
							AddPointToServerRefusalList(g_SpawnData.RequestedSpawnResults.vCoords[g_SpawnData.iServerSpawnResultsSlot])
							TryAgainToRequestSpawnLocation()
							
						ELSE
							IF (bCheckEntityArea)

								#IF IS_DEBUG_BUILD
								PRINTLN("[spawning] HAS_GOT_SPAWN_LOCATION - fEntityAreaRadius = ", fEntityAreaRadius) 
								#ENDIF
							
								IF (eOverrideModel != DUMMY_MODEL_FOR_SCRIPT)
									PRINTLN("[spawning] HAS_GOT_SPAWN_LOCATION - using model for entity area, eOverrideModel ", ENUM_TO_INT(eOverrideModel))
									bEntityAreaCreated = Setup_Entity_Area_For_Spawning_Vehicle_Model(g_SpawnData.RequestedSpawnResults.vCoords[g_SpawnData.iServerSpawnResultsSlot], g_SpawnData.RequestedSpawnResults.fHeading[g_SpawnData.iServerSpawnResultsSlot], eOverrideModel, TRUE, 0.5) 
									
									IF !bEntityAreaCreated
										bReturn = TRUE
									ENDIF
								ELSE
									IF (bForVehicle)
										fEntityAreaRadius = 2.0
									ELSE
										fEntityAreaRadius = 0.3	
									ENDIF
									PRINTLN("[spawning] HAS_GOT_SPAWN_LOCATION - using fEntityAreaRadius entity area, fEntityAreaRadius ", fEntityAreaRadius)
									bEntityAreaCreated = Setup_Entity_Area_For_Spawning_Vehicle(g_SpawnData.RequestedSpawnResults.vCoords[g_SpawnData.iServerSpawnResultsSlot], fEntityAreaRadius)
									
									IF !bEntityAreaCreated
										bReturn = TRUE
									ENDIF
								ENDIF
								
								IF bEntityAreaCreated
									g_SpawnData.iWarpState = WARP_STATE_CHECK_ENTITY_AREA
									g_SpawnData.iSpawnVehicleEntityAreaTime = GET_NETWORK_TIME_ACCURATE()
									#IF IS_DEBUG_BUILD
										NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - going to check entity area.") NET_NL()
									#ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - success! server said ok.") NET_NL()
								#ENDIF
								bReturn = TRUE
							ENDIF
						ENDIF

					ELSE
						// server said no, should we give up or try again?
						#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - server said no, send another request") NET_NL()
						#ENDIF
						AddSpawnResultsToServerRefusalList(g_SpawnData.RequestedSpawnResults)
						TryAgainToRequestSpawnLocation()
							
					ENDIF
				
				ELSE
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - response and request id's mismatch.  ") 
						NET_PRINT(" g_SpawnData.iResponseToWarpID = ") NET_PRINT_INT(g_SpawnData.iResponseToWarpID)
						NET_PRINT(" g_SpawnData.iRequestToWarpID = ") NET_PRINT_INT(g_SpawnData.iRequestToWarpID)
						NET_NL()
					#ENDIF
					// response and request id's dont match, ignore this server response.
					g_SpawnData.bServerRepliedToWarpRequest = FALSE
					g_SpawnData.bServerAgreedToWarpRequest = FALSE
					g_SpawnData.iServerSpawnResultsSlot = -1				
				ENDIF
			ELSE
				// has the server taken too long?
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iWarpRequestSendTime) > MAX_SERVER_WARP_RESPONSE_TIME)
				OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iWarpRequestSendTime) < 0)
					
					IF (g_SpawnData.iWarpServerTimeoutCount < MAX_NUM_OF_SERVER_WARP_TIMEOUTS)
						
						// send another request
						g_SpawnData.iWarpServerTimeoutCount++
						g_SpawnData.iWarpState = WARP_STATE_SEND_REQUEST
						
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - server taking too long to reply, sending again.") NET_NL()
						#ENDIF
						
					ELSE
						// just ok the request
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - server took too long to reply.") NET_NL()
						#ENDIF
						g_SpawnData.iWarpState = 	WARP_STATE_GAVE_UP
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - waiting for server response.... time=") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iWarpRequestSendTime)) NET_NL()
					#ENDIF	
				ENDIF
			ENDIF
		BREAK
		
		CASE WARP_STATE_CHECK_ENTITY_AREA
			IF (bCheckEntityArea)
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iSpawnVehicleEntityAreaTime) < 10000)	
					IF NETWORK_ENTITY_AREA_DOES_EXIST(g_SpawnData.iActiveSpawnVehicleEntityArea)
						IF NETWORK_ENTITY_AREA_HAVE_ALL_REPLIED(g_SpawnData.iActiveSpawnVehicleEntityArea )
							IF NOT NETWORK_ENTITY_AREA_IS_OCCUPIED(g_SpawnData.iActiveSpawnVehicleEntityArea)									

								// all good, let's go!
								#IF IS_DEBUG_BUILD
									NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - WARP_STATE_CHECK_ENTITY_AREA - good! go,go,go!") NET_NL()
								#ENDIF	
								bReturn = TRUE

							ELSE
								#IF IS_DEBUG_BUILD
									PRINTLN("[spawning] HAS_GOT_SPAWN_LOCATION - WARP_STATE_CHECK_ENTITY_AREA - area is occupied, try again.") 
								#ENDIF	
								AddPointToServerRefusalList(g_SpawnData.RequestedSpawnResults.vCoords[g_SpawnData.iServerSpawnResultsSlot])
								TryAgainToRequestSpawnLocation()
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - WARP_STATE_CHECK_ENTITY_AREA - waiting for all to reply") NET_NL()
							#ENDIF	
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - WARP_STATE_CHECK_ENTITY_AREA - entity area does not exist. Try again.") NET_NL()
						#ENDIF	
						TryAgainToRequestSpawnLocation()
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - WARP_STATE_CHECK_ENTITY_AREA - taking too long. Try again.") NET_NL()
					#ENDIF
					TryAgainToRequestSpawnLocation(1)	
				ENDIF	
			ELSE
				PRINTLN("[spawning] HAS_GOT_SPAWN_LOCATION - WARP_STATE_CHECK_ENTITY_AREA - bCheckEntityArea = ", bCheckEntityArea)
				bReturn = TRUE
			ENDIF		
		BREAK
	
	
		CASE WARP_STATE_GAVE_UP					
			IF GetSpawnLocationCoords(g_SpawnData.RequestedSpawnResults, SpawnLocation, bForVehicle, bDoTeammateVisCheck, bDontUseVehicleNodes, bUseCarNodeOffset, bUseStartOfMissionPVSpawn, eOverrideModel)
				#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - going to WARP_STATE_WAIT_FRAME after WARP_STATE_GAVE_UP.") NET_NL()
				#ENDIF
				g_SpawnData.iWarpState = 	WARP_STATE_WAIT_FRAME
				g_SpawnData.iServerSpawnResultsSlot = 0	
			ENDIF
		BREAK		
	
		CASE WARP_STATE_WAIT_FRAME
			#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - returning TRUE after WARP_STATE_GAVE_UP.") NET_NL()
			#ENDIF
			bReturn = TRUE
		BREAK
	
	ENDSWITCH
	
	g_SpawnData.iHasGotSpawnLocationUpdateTime = GET_NETWORK_TIME()

	IF (bReturn)
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - returning TRUE. ") NET_NL()
		#ENDIF
		

		
		IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS)
			g_SpawnData.CustomSpawnPointInfo.iLastUsedCustomSpawnPoint = g_SpawnData.CustomSpawnPointInfo.iLastCustomSpawnPoint	
			NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - setting iLastUsedCustomSpawnPoint to ") NET_PRINT_INT(g_SpawnData.CustomSpawnPointInfo.iLastCustomSpawnPoint) NET_NL()
		ENDIF
		IF (g_SpawnData.iServerSpawnResultsSlot = -1)
			g_SpawnData.iServerSpawnResultsSlot = 0
		ENDIF
		vCoords = g_SpawnData.RequestedSpawnResults.vCoords[g_SpawnData.iServerSpawnResultsSlot]
		fHeading = g_SpawnData.RequestedSpawnResults.fHeading[g_SpawnData.iServerSpawnResultsSlot]
		
		
		PRINTLN("[spawning] HAS_GOT_SPAWN_LOCATION - g_SpawnData.MissionSpawnDetails.bFacePoint = ",g_SpawnData.MissionSpawnDetails.bFacePoint)
		PRINTLN("[spawning] HAS_GOT_SPAWN_LOCATION - g_SpawnData.SpawnLocation = ", g_SpawnData.SpawnLocation)
		PRINTLN("[spawning] HAS_GOT_SPAWN_LOCATION - g_SpawnData.MissionSpawnDetails.vFacing = ",g_SpawnData.MissionSpawnDetails.vFacing)
		
		// if we have a facing value set and its a mission location then always turn to face. 
		IF (g_SpawnData.MissionSpawnDetails.bFacePoint)
			IF (g_SpawnData.SpawnLocation = SPAWN_LOCATION_MISSION_AREA)
			AND VMAG(g_SpawnData.MissionSpawnDetails.vFacing) > 0.0
				
				IF (bForVehicle)
					
					NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - is for a vehicle so not altering coords, heading should have be chosen appropriately from the node search ")  NET_NL()	
				
				ELSE
					NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - setting heading to face coords ")  NET_NL()
					
					
					vec = g_SpawnData.MissionSpawnDetails.vFacing - vCoords
					fHeading = GET_HEADING_FROM_VECTOR_2D(vec.x, vec.y)
					IF g_SpawnData.MissionSpawnDetails.bFaceAwayFromPoint
						fHeading += 180.0 
						NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - setting heading to face AWAY coords ")  NET_NL()
					ENDIF
					
					g_SpawnData.bDoShapeTest = TRUE
				ENDIF
				
			ENDIF	
		ENDIF		
		g_SpawnData.iAttempt = 0
		g_SpawnData.iWarpState = WARP_STATE_INIT
		ClearServerRefusalList()
		IF (bCheckEntityArea)
			IF NOT IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(PLAYER_ID())
				Cleanup_Entity_Area_For_Spawning_Vehicle()
			ELSE
				PRINTLN("[spawning] HAS_GOT_SPAWN_LOCATION - player is doing a warp to spawn location, dont remove entity area yet")
			ENDIF
		ENDIF
		
		RETURN(TRUE)
	ELSE
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] HAS_GOT_SPAWN_LOCATION - returning FALSE") NET_NL()
		#ENDIF
		RETURN(FALSE)
	ENDIF

ENDFUNC

PROC CLEAR_WARP_TO_AREA(BOOL bBroadcast=FALSE)
	ClearAreaAroundPointForSpawning(g_SpawnData.vWarpToLocationCoords, bBroadcast)
ENDPROC

PROC MAKE_GAME_CAM_NEAR_TO_PLAYER()
	NET_PRINT("MAKE_SPAWN_CAM_NEAR_TO_PLAYER - called")
	DEBUG_PRINTCALLSTACK()
	g_SpawnData.bUpdateSpawnCam = TRUE
ENDPROC

PROC SetGameCamBehindPlayer(FLOAT fRelativeHeading=0.0, BOOL bIgnoreTransitionCheck=FALSE)
	IF NOT IS_TRANSITION_ACTIVE()
	OR (bIgnoreTransitionCheck)
		IF (IS_SCREEN_FADED_OUT() OR IS_PLAYER_SWITCH_IN_PROGRESS())
			IF NOT IS_CINEMATIC_FIRST_PERSON_VEHICLE_INTERIOR_CAM_RENDERING()
				// if player is under the map then skip. 3095605
				VECTOR vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
				IF (vPlayerPos.z > -80.0) 
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0) 
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(fRelativeHeading)
				ELSE
					NET_PRINT("[spawning] SetGameCamBehindPlayer - is under map, skipping.") NET_NL()
				ENDIF
			ELSE
				NET_PRINT("[spawning] SetGameCamBehindPlayer - in FP cam skipping") NET_NL()
			ENDIF
		ELSE
			NET_PRINT("[spawning] SetGameCamBehindPlayer - IS_SCREEN_FADED_OUT() OR IS_PLAYER_SWITCH_IN_PROGRESS()") NET_NL()
		ENDIF
	ELSE
		NET_PRINT("[spawning] SetGameCamBehindPlayer - IS_TRANSITION_ACTIVE") NET_NL()
	ENDIF
ENDPROC

PROC SetPlayerHeadingAndUpdateCamera(FLOAT fHeading)
	IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
		SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading)
	ENDIF
	SetGameCamBehindPlayer(0.0, TRUE)
ENDPROC

PROC ChooseDirectionWithLowestScore(INT iLowestDirection, FLOAT fPlayerHeading, VECTOR vVecToRoad)
	// choose direction with lowest score
	SWITCH iLowestDirection
		CASE 0 // forward							
			NET_PRINT("[spawning] ChooseDirectionWithLowestScore - forward has lowest score")NET_NL() 
			EXIT
		BREAK
		CASE 1 // forward right
			NET_PRINT("[spawning] ChooseDirectionWithLowestScore - forward right has lowest score")NET_NL()	
			fPlayerHeading += -45.0
			SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
			NET_PRINT("[spawning] ChooseDirectionWithLowestScore - going to forward right, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
			EXIT
		BREAK
		CASE 2 // forward left
			NET_PRINT("[spawning] ChooseDirectionWithLowestScore - forward left has lowest score")NET_NL()	
			fPlayerHeading += 45.0
			SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
			NET_PRINT("[spawning] ChooseDirectionWithLowestScore - going to forward left, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
			EXIT
		BREAK
		CASE 3 // right
			NET_PRINT("[spawning] ChooseDirectionWithLowestScore - right has lowest score")NET_NL()	
			fPlayerHeading += -90.0
			SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
			NET_PRINT("[spawning] ChooseDirectionWithLowestScore - going to right, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
			EXIT
		BREAK
		CASE 4 // left
			NET_PRINT("[spawning] ChooseDirectionWithLowestScore - left has lowest score")NET_NL()	
			fPlayerHeading += 90.0
			SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
			NET_PRINT("[spawning] ChooseDirectionWithLowestScore - going to left, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
			EXIT
		BREAK
		CASE 5 // road
			NET_PRINT("[spawning] ChooseDirectionWithLowestScore - road has lowest score")NET_NL()
			fPlayerHeading = GET_HEADING_FROM_VECTOR_2D(vVecToRoad.x, vVecToRoad.y)	
			SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
			NET_PRINT("[spawning] ChooseDirectionWithLowestScore - going to road, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
			EXIT
		BREAK
	ENDSWITCH
					
ENDPROC

FUNC BOOL IsDriverOfVehicleInvolvedInSameGroupWarp(VEHICLE_INDEX VehicleID)
	
	PED_INDEX PedID
	PLAYER_INDEX PlayerID
	
	IF NOT IS_VEHICLE_SEAT_FREE(VehicleID)
		PedID = GET_PED_IN_VEHICLE_SEAT(VehicleID)
		IF DOES_ENTITY_EXIST(PedID)
		AND NOT IS_ENTITY_DEAD(PedID)		
			IF IS_PED_A_PLAYER(PedID)
				PlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(PedID)
				PRINTLN("[spawning] IsDriverOfVehicleInvolvedInSameGroupWarp - ped is player ", NATIVE_TO_INT(PlayerID))
				IF IS_NET_PLAYER_OK(PlayerID, FALSE, FALSE)
					IF IS_PLAYER_IN_SAME_GROUP_WARP_AS_LOCAL_PLAYER(PlayerID)
						PRINTLN("[spawning] IsDriverOfVehicleInvolvedInSameGroupWarp - in same group warp. player ", NATIVE_TO_INT(PlayerID))
						RETURN TRUE
					ELSE
						PRINTLN("[spawning] IsDriverOfVehicleInvolvedInSameGroupWarp - NOT in same group warp. player ", NATIVE_TO_INT(PlayerID))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IsVehicleTheTrailerOfVehiclePlayerIsIn(VEHICLE_INDEX VehicleID)

	VEHICLE_INDEX PlayerVehicleID
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		PlayerVehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PlayerVehicleID)
			VEHICLE_INDEX TrailerID
			IF GET_VEHICLE_TRAILER_VEHICLE(PlayerVehicleID, TrailerID)
			
				PRINTLN("[spawning] IsVehicleTheTrailerOfVehiclePlayerIsIn - the player is in a vehicle with a trailer!")
			
				IF (TrailerID = VehicleID)
					PRINTLN("[spawning] IsVehicleTheTrailerOfVehiclePlayerIsIn - returning TRUE")	
					RETURN TRUE
				ENDIF
			ELSE
				PRINTLN("[spawning] IsVehicleTheTrailerOfVehiclePlayerIsIn - the player is NOT in a vehicle with a trailer")
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL HasSpawnedInsideNearbyPlayer()

	INT iPlayer
	PLAYER_INDEX PlayerID
	FLOAT fDist
	VECTOR vPlayer = GET_PLAYER_COORDS(PLAYER_ID())
	
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
		IF IS_NET_PLAYER_OK(PlayerID)
			IF IS_PED_ON_FOOT(GET_PLAYER_PED(PlayerID))
				IF NOT (PlayerID = PLAYER_ID())				
					IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)				
						fDist = VMAG(GET_PLAYER_COORDS(PlayerID) - vPlayer)
						IF (fDist < 0.2)
							PRINTLN("[spawning] HasSpawnedInsideNearbyPlayer - returning true for player ", iPlayer)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("[spawning] HasSpawnedInsideNearbyPlayer - returning false.")
	RETURN FALSE

ENDFUNC

FUNC BOOL HasSpawnedInsideNearbyVehicle(VECTOR vPos)
	//VEHICLE_INDEX VehicleID[8]
	INT iTotalVehicles
	iTotalVehicles = GET_ALL_VEHICLES(g_PoolVehicles) //GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), VehicleID)
	VECTOR vVehCoords
	FLOAT fVehHeading
	MODEL_NAMES VehModel
	INT i
	PRINTLN("[spawning] HasSpawnedInsideNearbyVehicle - iTotalVehicles ", iTotalVehicles, "  vPos = ", vPos)
	
	VEHICLE_INDEX PlayerVehicleID
	MODEL_NAMES PlayerVehicleModel
	VECTOR vPlayerVehicleCoords
	FLOAT fPlayerVehicleHeading
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		PlayerVehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())	
		IF NOT IS_ENTITY_DEAD(PlayerVehicleID)
			PlayerVehicleModel = GET_ENTITY_MODEL(PlayerVehicleID)
			vPlayerVehicleCoords = GET_ENTITY_COORDS(PlayerVehicleID)
			fPlayerVehicleHeading = GET_ENTITY_HEADING(PlayerVehicleID)			
			PRINTLN("[spawning] HasSpawnedInsideNearbyVehicle - PlayerVehicleModel ", GET_MODEL_NAME_FOR_DEBUG(PlayerVehicleModel), ", vPlayerVehicleCoords ", vPlayerVehicleCoords, ", fPlayerVehicleHeading ", fPlayerVehicleHeading)
		ENDIF
	ENDIF
	
	REPEAT iTotalVehicles i
		IF DOES_ENTITY_EXIST(g_PoolVehicles[i])		
			IF NOT IS_ENTITY_DEAD(g_PoolVehicles[i])
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_PoolVehicles[i])
				
				#IF IS_DEBUG_BUILD
					vVehCoords = GET_ENTITY_COORDS(g_PoolVehicles[i], FALSE)
					VehModel = GET_ENTITY_MODEL(g_PoolVehicles[i])
				#ENDIF
			
				PRINTLN("[spawning] HasSpawnedInsideNearbyVehicle - player is actually in this model ", GET_MODEL_NAME_FOR_DEBUG(VehModel), ", at coords ", vVehCoords)	
			ELIF IsVehicleTheTrailerOfVehiclePlayerIsIn(g_PoolVehicles[i])
			
				#IF IS_DEBUG_BUILD
					vVehCoords = GET_ENTITY_COORDS(g_PoolVehicles[i], FALSE)
					VehModel = GET_ENTITY_MODEL(g_PoolVehicles[i])
				#ENDIF
			
				PRINTLN("[spawning] HasSpawnedInsideNearbyVehicle - this is the trailer of the vehicle the player is in. vVehCoords = ", vVehCoords, ", vehModel = ", GET_MODEL_NAME_FOR_DEBUG(VehModel))	
			
			
			ELSE
			

				
				IF NOT IsDriverOfVehicleInvolvedInSameGroupWarp(g_PoolVehicles[i])
				
					vVehCoords = GET_ENTITY_COORDS(g_PoolVehicles[i], FALSE)
					fVehHeading = GET_ENTITY_HEADING(g_PoolVehicles[i])
					VehModel = GET_ENTITY_MODEL(g_PoolVehicles[i])				
				
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF DoVehicleBoundsOverlap(vVehCoords, fVehHeading, VehModel, vPlayerVehicleCoords, fPlayerVehicleHeading, PlayerVehicleModel)
							PRINTLN("[spawning] HasSpawnedInsideNearbyVehicle - DoVehicleBoundsOverlap = TRUE - model ", GET_MODEL_NAME_FOR_DEBUG(VehModel), ", at coords ", vVehCoords)
							RETURN TRUE
						ELSE
							PRINTLN("[spawning] HasSpawnedInsideNearbyVehicle - DoVehicleBoundsOverlap = FALSE - model ", GET_MODEL_NAME_FOR_DEBUG(VehModel), ", at coords ", vVehCoords)	
						ENDIF
					ELSE				
						IF IsPointWithinModelBounds(vPos, vVehCoords, fVehHeading, VehModel)				
							PRINTLN("[spawning] HasSpawnedInsideNearbyVehicle - inside model ", GET_MODEL_NAME_FOR_DEBUG(VehModel), ", at coords ", vVehCoords)
							RETURN TRUE
						ELSE
							PRINTLN("[spawning] HasSpawnedInsideNearbyVehicle - player is NOT this model ", GET_MODEL_NAME_FOR_DEBUG(VehModel), ", at coords ", vVehCoords)		
						ENDIF
					ENDIF
					
				ELSE
					PRINTLN("[spawning] HasSpawnedInsideNearbyVehicle - vehicle driver in same group warp ", GET_MODEL_NAME_FOR_DEBUG(VehModel), ", at coords ", vVehCoords)		
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	PRINTLN("[spawning] HasSpawnedInsideNearbyVehicle = FALSE ")
	RETURN FALSE
ENDFUNC

PROC DoSpawningShapeTest()

	NET_PRINT("[spawning] DoSpawningShapeTest - called...") NET_NL()
	DEBUG_PRINTCALLSTACK()

	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())

		INT i

		VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)

		VECTOR vNearestCarNode
		BOOL bDoRoadShapeTest
		

		// only face road if a specific facing point hasn't been set up. 2430825
		IF NOT (g_SpawnData.MissionSpawnDetails.bFacePoint)
		AND NOT (g_SpawnData.MissionSpawnDetails.bFaceAwayFromPoint)		
		AND NOT (g_SpawnData.bIgnoreFaceRoad)
		
			IF GET_CLOSEST_VEHICLE_NODE(vPlayerCoords , vNearestCarNode) 
				vNearestCarNode.z += 1.0
				bDoRoadShapeTest = TRUE		
				// check it's within 20m
				IF NOT (VDIST(vPlayerCoords, vNearestCarNode) < SPAWN_SHAPETEST_MAX_ROAD_DIST)
				OR (g_SpawnData.MissionSpawnDetails.bFacePoint)
					bDoRoadShapeTest = FALSE
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] DoSpawningShapeTest - dont do road test - dist = ") NET_PRINT_FLOAT(VDIST(vPlayerCoords, vNearestCarNode))
						NET_PRINT(", vPlayerCoords = ") NET_PRINT_VECTOR(vPlayerCoords)
						NET_PRINT(", vNearestCarNode = ") NET_PRINT_VECTOR(vNearestCarNode)					
						NET_NL()					
					#ENDIF
				ENDIF
			ELSE
				NET_PRINT("[spawning] DoSpawningShapeTest - dont do road test - GET_CLOSEST_VEHICLE_NODE=FALSE") NET_NL()	
			ENDIF

		ELSE
			NET_PRINT("[spawning] DoSpawningShapeTest - dont do road test - facing point has been set.") NET_NL()	
			IF (g_SpawnData.bIgnoreFaceRoad)
				NET_PRINT("[spawning] DoSpawningShapeTest - setting g_SpawnData.bIgnoreFaceRoad = FALSE.") NET_NL()
				g_SpawnData.bIgnoreFaceRoad = FALSE
			ENDIF
		ENDIF

		
		
		FLOAT fPlayerHeading
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
		ENDIF

		FLOAT fProbeDist
		
//		IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
//			fProbeDist = SPAWNING_SHAPE_TEST_PROBE_DIST - 1.0
//		ELSE
			fProbeDist = SPAWNING_SHAPE_TEST_PROBE_DIST 
//		ENDIF

		// get forward vector from player	
		VECTOR vForwardVec = GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID())
		vForwardVec /= VMAG(vForwardVec)
		vForwardVec *= fProbeDist
		
		VECTOR vForwardRightVec = vForwardVec
		RotateVec(vForwardRightVec, <<0.0, 0.0, -45.0>>)
		
		VECTOR vRightVec = vForwardVec
		RotateVec(vRightVec, <<0.0, 0.0, -90.0>>)
		
		VECTOR vForwardLeftVec = vForwardVec
		RotateVec(vForwardLeftVec, <<0.0, 0.0, 45.0>>)
		
		VECTOR vLeftVec = vForwardVec
		RotateVec(vLeftVec, <<0.0, 0.0, 90.0>>)
		
		
		VECTOR vVecToRoad
		VECTOR vRoadOffset
		IF (bDoRoadShapeTest)
			vVecToRoad = vNearestCarNode - vPlayerCoords
			vRoadOffset = CROSS_PRODUCT(vVecToRoad, <<0.0, 0.0, 1.0>>)
			vRoadOffset /= VMAG(vRoadOffset)	
			vRoadOffset *= SPAWN_SHAPETEST_WIDTH
		ENDIF
		
		VECTOR vForwardPoint = vPlayerCoords + vForwardVec
		VECTOR vForwardRightPoint = vPlayerCoords + vForwardRightVec
		VECTOR vRightPoint = vPlayerCoords + vRightVec
		VECTOR vForwardLeftPoint = vPlayerCoords + vForwardLeftVec
		VECTOR vLeftPoint = vPlayerCoords + vLeftVec
		
		//VECTOR vRightOffset = (vRightVec / VMAG(vRightVec)) * SPAWN_SHAPETEST_WIDTH
		//VECTOR vLeftOffset = (vLeftVec / VMAG(vLeftVec)) * SPAWN_SHAPETEST_WIDTH
		//VECTOR vForwardOffset = (vForwardVec / VMAG(vForwardVec)) * SPAWN_SHAPETEST_WIDTH
		//VECTOR vBackwardOffset = (vForwardVec / VMAG(vForwardVec)) * (SPAWN_SHAPETEST_WIDTH * -1.0)
		
		//VECTOR vForwardRightOffest = (vForwardRightVec/ VMAG(vForwardRightVec)) * SPAWN_SHAPETEST_WIDTH 
		//VECTOR vForwardRightOffest_reverse = (vForwardRightVec/ VMAG(vForwardRightVec)) * (SPAWN_SHAPETEST_WIDTH * -1.0) 
		//VECTOR vForwardLeftOffest = (vForwardLeftVec/ VMAG(vForwardLeftVec)) * SPAWN_SHAPETEST_WIDTH 
		//VECTOR vForwardLeftOffest_reverse = (vForwardLeftVec/ VMAG(vForwardLeftVec)) * (SPAWN_SHAPETEST_WIDTH * -1.0)
		
		NET_PRINT("[spawning] DoSpawningShapeTest - fPlayerHeading = ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
		NET_PRINT("[spawning] DoSpawningShapeTest - vPlayerCoords = ") NET_PRINT_VECTOR(vPlayerCoords) NET_NL()
		NET_PRINT("[spawning] DoSpawningShapeTest - vForwardPoint = ") NET_PRINT_VECTOR(vForwardPoint) NET_NL()
		NET_PRINT("[spawning] DoSpawningShapeTest - vRightPoint = ") NET_PRINT_VECTOR(vRightPoint) NET_NL()
		NET_PRINT("[spawning] DoSpawningShapeTest - vLeftPoint = ") NET_PRINT_VECTOR(vLeftPoint) NET_NL()
		NET_PRINT("[spawning] DoSpawningShapeTest - vForwardRightPoint = ") NET_PRINT_VECTOR(vForwardRightPoint) NET_NL()
		NET_PRINT("[spawning] DoSpawningShapeTest - vForwardLeftPoint = ") NET_PRINT_VECTOR(vForwardLeftPoint) NET_NL()
		NET_PRINT("[spawning] DoSpawningShapeTest - vNearestCarNode = ") NET_PRINT_VECTOR(vNearestCarNode) NET_NL()
		
		SHAPETEST_INDEX ForwardShapeTestResult
		SHAPETEST_INDEX ForwardRightShapeTestResult
		SHAPETEST_INDEX RightShapeTestResult
		SHAPETEST_INDEX ForwardLeftShapeTestResult
		SHAPETEST_INDEX LeftShapeTestResult
		SHAPETEST_INDEX RoadShapeTestResult
		

		// forward shape tests
		ForwardShapeTestResult = START_SHAPE_TEST_CAPSULE(vPlayerCoords, vForwardPoint, SPAWN_SHAPETEST_WIDTH, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_FOLIAGE, PLAYER_PED_ID(), SCRIPT_SHAPETEST_OPTION_DEFAULT)

		#IF IS_DEBUG_BUILD
			g_SpawnData.vShapeTestStartForward = vPlayerCoords
			g_SpawnData.vShapeTestEndForward = vForwardPoint
		#ENDIF	
		
		// forward right shape tests
		ForwardRightShapeTestResult = START_SHAPE_TEST_CAPSULE(vPlayerCoords, vForwardRightPoint, SPAWN_SHAPETEST_WIDTH, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_FOLIAGE, PLAYER_PED_ID(), SCRIPT_SHAPETEST_OPTION_DEFAULT)

		#IF IS_DEBUG_BUILD
			g_SpawnData.vShapeTestStartForwardRight = vPlayerCoords
			g_SpawnData.vShapeTestEndForwardRight = vForwardRightPoint
		#ENDIF			
		
		// Right shape tests
		RightShapeTestResult = START_SHAPE_TEST_CAPSULE(vPlayerCoords, vRightPoint, SPAWN_SHAPETEST_WIDTH, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_FOLIAGE, PLAYER_PED_ID(), SCRIPT_SHAPETEST_OPTION_DEFAULT)

		#IF IS_DEBUG_BUILD
			g_SpawnData.vShapeTestStartRight = vPlayerCoords
			g_SpawnData.vShapeTestEndRight = vRightPoint
		#ENDIF	
		
		// forward left shape tests
		ForwardLeftShapeTestResult = START_SHAPE_TEST_CAPSULE(vPlayerCoords, vForwardLeftPoint, SPAWN_SHAPETEST_WIDTH, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_FOLIAGE, PLAYER_PED_ID(), SCRIPT_SHAPETEST_OPTION_DEFAULT)

		#IF IS_DEBUG_BUILD
			g_SpawnData.vShapeTestStartForwardLeft = vPlayerCoords
			g_SpawnData.vShapeTestEndForwardLeft = vForwardLeftPoint
		#ENDIF			

		// Left shape tests		
		LeftShapeTestResult = START_SHAPE_TEST_CAPSULE(vPlayerCoords, vLeftPoint, SPAWN_SHAPETEST_WIDTH, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_FOLIAGE, PLAYER_PED_ID(), SCRIPT_SHAPETEST_OPTION_DEFAULT)

		#IF IS_DEBUG_BUILD
			g_SpawnData.vShapeTestStartLeft = vPlayerCoords
			g_SpawnData.vShapeTestEndLeft = vLeftPoint
		#ENDIF	
		
		// Road shape test
		IF (bDoRoadShapeTest)
			RoadShapeTestResult = START_SHAPE_TEST_CAPSULE(vPlayerCoords, vNearestCarNode, SPAWN_SHAPETEST_WIDTH, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_FOLIAGE, PLAYER_PED_ID(), SCRIPT_SHAPETEST_OPTION_DEFAULT)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			g_SpawnData.vShapeTestStartRoad = vPlayerCoords
			g_SpawnData.vShapeTestEndRoad = vNearestCarNode
		#ENDIF		
		
		INT iForwardResult
		INT iForwardRightResult
		INT iRightResult
		INT iForwardLeftResult
		INT iLeftResult
		INT iRoadResult
		
		VECTOR vResult
		VECTOR vNormal
		ENTITY_INDEX EntityResult
		
		SHAPETEST_STATUS ShapeTestStatus
		

		ShapeTestStatus = GET_SHAPE_TEST_RESULT(ForwardShapeTestResult, iForwardResult, vResult, vNormal, EntityResult)
		NET_PRINT("[spawning] DoSpawningShapeTest - Forward - ShapeTestStatus = ") NET_PRINT_INT(ENUM_TO_INT(ShapeTestStatus)) NET_PRINT(", iForwardResult") NET_PRINT_INT(iForwardResult) NET_NL()	
		
		#IF IS_DEBUG_BUILD
			g_SpawnData.vShapeTestFailCoords_Forward = vResult
		#ENDIF
		
		ShapeTestStatus = GET_SHAPE_TEST_RESULT(ForwardRightShapeTestResult, iForwardRightResult, vResult, vNormal, EntityResult)
		NET_PRINT("[spawning] DoSpawningShapeTest - ForwardRight - ShapeTestStatus = ") NET_PRINT_INT(ENUM_TO_INT(ShapeTestStatus)) NET_PRINT(", iForwardRightResult") NET_PRINT_INT(iForwardRightResult) NET_NL()				
		
		#IF IS_DEBUG_BUILD
			g_SpawnData.vShapeTestFailCoords_ForwardRight = vResult
		#ENDIF
		
		ShapeTestStatus = GET_SHAPE_TEST_RESULT(RightShapeTestResult, iRightResult, vResult, vNormal, EntityResult)
		NET_PRINT("[spawning] DoSpawningShapeTest - Right - ShapeTestStatus = ") NET_PRINT_INT(ENUM_TO_INT(ShapeTestStatus)) NET_PRINT(", iRightResult") NET_PRINT_INT(iRightResult) NET_NL()
		
		#IF IS_DEBUG_BUILD
			g_SpawnData.vShapeTestFailCoords_Right = vResult
		#ENDIF
		
		ShapeTestStatus = GET_SHAPE_TEST_RESULT(ForwardLeftShapeTestResult, iForwardLeftResult, vResult, vNormal, EntityResult)
		NET_PRINT("[spawning] DoSpawningShapeTest - ForwardLeft - ShapeTestStatus = ") NET_PRINT_INT(ENUM_TO_INT(ShapeTestStatus)) NET_PRINT(", iForwardLeftResult") NET_PRINT_INT(iForwardLeftResult) NET_NL()				
		
		#IF IS_DEBUG_BUILD
			g_SpawnData.vShapeTestFailCoords_ForwardLeft = vResult
		#ENDIF
																	
		ShapeTestStatus = GET_SHAPE_TEST_RESULT(LeftShapeTestResult, iLeftResult, vResult, vNormal, EntityResult)
		NET_PRINT("[spawning] DoSpawningShapeTest - Left - ShapeTestStatus = ") NET_PRINT_INT(ENUM_TO_INT(ShapeTestStatus)) NET_PRINT(", iLeftResult") NET_PRINT_INT(iLeftResult) NET_NL()
		
		#IF IS_DEBUG_BUILD
			g_SpawnData.vShapeTestFailCoords_Left = vResult
		#ENDIF
		
		IF (bDoRoadShapeTest)
			ShapeTestStatus = GET_SHAPE_TEST_RESULT(RoadShapeTestResult, iRoadResult, vResult, vNormal, EntityResult)
			NET_PRINT("[spawning] DoSpawningShapeTest - Road - ShapeTestStatus = ") NET_PRINT_INT(ENUM_TO_INT(ShapeTestStatus)) NET_PRINT(", iRoadResult") NET_PRINT_INT(iRoadResult) NET_NL()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF iForwardResult = 0
				g_SpawnData.bShapeTestFailedForward = FALSE
			ELSE
				g_SpawnData.bShapeTestFailedForward = TRUE
			ENDIF
			IF iForwardRightResult = 0
				g_SpawnData.bShapeTestFailedForwardRight = FALSE
			ELSE
				g_SpawnData.bShapeTestFailedForwardRight = TRUE
			ENDIF
			IF iRightResult = 0
				g_SpawnData.bShapeTestFailedRight = FALSE
			ELSE
				g_SpawnData.bShapeTestFailedRight = TRUE
			ENDIF
			IF iForwardLeftResult = 0
				g_SpawnData.bShapeTestFailedForwardLeft = FALSE
			ELSE
				g_SpawnData.bShapeTestFailedForwardLeft = TRUE
			ENDIF
			IF iLeftResult = 0
				g_SpawnData.bShapeTestFailedLeft = FALSE
			ELSE
				g_SpawnData.bShapeTestFailedLeft = TRUE
			ENDIF
			IF (bDoRoadShapeTest)
				IF iRoadResult = 0
					g_SpawnData.bShapeTestFailedRoad = FALSE
				ELSE
					g_SpawnData.bShapeTestFailedRoad = TRUE
				ENDIF
			ELSE
				g_SpawnData.bShapeTestFailedRoad = TRUE
			ENDIF
		#ENDIF

		
		IF ShapeTestStatus = INT_TO_ENUM(SHAPETEST_STATUS, 0)
			// do nothing, release build fix
		ENDIF
		
		INT iForwardTotal
		INT iForwardRightTotal
		INT iRightTotal
		INT iForwardLeftTotal
		INT iLeftTotal
		INT iRoadTotal
		

		iForwardTotal += iForwardResult	
		iForwardRightTotal += iForwardRightResult	
		iRightTotal += iRightResult
		iForwardLeftTotal += iForwardLeftResult
		iLeftTotal += iLeftResult
		IF (bDoRoadShapeTest)
			iRoadTotal += iRoadResult
		ENDIF

		
		NET_PRINT("[spawning] DoSpawningShapeTest - iForwardTotal = ") NET_PRINT_INT(iForwardTotal) NET_NL()
		NET_PRINT("[spawning] DoSpawningShapeTest - iForwardRightTotal = ") NET_PRINT_INT(iForwardRightTotal) NET_NL()
		NET_PRINT("[spawning] DoSpawningShapeTest - iRightTotal = ") NET_PRINT_INT(iRightTotal) NET_NL()
		NET_PRINT("[spawning] DoSpawningShapeTest - iForwardLeftTotal = ") NET_PRINT_INT(iForwardLeftTotal) NET_NL()
		NET_PRINT("[spawning] DoSpawningShapeTest - iLeftTotal = ") NET_PRINT_INT(iLeftTotal) NET_NL()	
		NET_PRINT("[spawning] DoSpawningShapeTest - iRoadTotal = ") NET_PRINT_INT(iRoadTotal) NET_NL()	
		
		INT iLowestScore = 99
		INT iLowestDirection = -1  // 0 = forward, 1 = forward right, 2 = forward left, 3 = right, 4 = left, 5 = road
		
		// what has lowest score
		IF (iForwardTotal < iLowestScore)
			iLowestScore = iForwardTotal	
			iLowestDirection = 0
		ENDIF
		IF (iForwardRightTotal < iLowestScore)
			iLowestScore = iForwardRightTotal	
			iLowestDirection = 1
		ENDIF
		IF (iForwardLeftTotal < iLowestScore)
			iLowestScore = iForwardLeftTotal	
			iLowestDirection = 2
		ENDIF
		IF (iRightTotal < iLowestScore)
			iLowestScore = iRightTotal	
			iLowestDirection = 3
		ENDIF
		IF (iLeftTotal < iLowestScore)
			iLowestScore = iLeftTotal	
			iLowestDirection = 4
		ENDIF
		IF (bDoRoadShapeTest)
			IF (iRoadTotal < iLowestScore)
				iLowestScore = iRoadTotal	
				iLowestDirection = 5
			ENDIF
		ENDIF
		NET_PRINT("[spawning] DoSpawningShapeTest - iLowestDirection = ") NET_PRINT_INT(iLowestDirection) NET_PRINT(", iLowestScore = ") NET_PRINT_INT(iLowestScore) NET_NL()
		
		IF (bDoRoadShapeTest)
		AND (iRoadTotal = 0)
			fPlayerHeading = GET_HEADING_FROM_VECTOR_2D(vVecToRoad.x, vVecToRoad.y)	
			SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
			NET_PRINT("[spawning] DoSpawningShapeTest - going to road, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
			EXIT 
		ELSE
		
			IF (iForwardTotal = 0)
				NET_PRINT("[spawning] DoSpawningShapeTest - Forward is ok - exiting") NET_NL()
				EXIT
			ELSE
			
				IF (g_SpawnData.MissionSpawnDetails.bFacePoint)
				
					// which direction is preferrable? left or right
					VECTOR vToFacingCoord
					vToFacingCoord = g_SpawnData.MissionSpawnDetails.vFacing - vPlayerCoords
					
					FLOAT fDotProd  = DOT_PRODUCT(vToFacingCoord, vRightPoint)
					
					PRINTLN("[spawning] DoSpawningShapeTest - vToFacingCoord = ", vToFacingCoord, ", dot prod = ", fDotProd)
					
					// prefer the right?
					IF fDotProd <= 0 
						NET_PRINT("[spawning] DoSpawningShapeTest - Prefers to the right ")  NET_NL()
						IF (iForwardRightTotal = 0)
							fPlayerHeading += -45.0
							SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
							NET_PRINT("[spawning] DoSpawningShapeTest - going to forward right, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
							EXIT
						ELIF (iRightTotal = 0)	
							fPlayerHeading += -90.0
							SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
							NET_PRINT("[spawning] DoSpawningShapeTest - going to right, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
							EXIT 
						ELIF (iForwardLeftTotal = 0)
							fPlayerHeading += 45.0
							SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
							NET_PRINT("[spawning] DoSpawningShapeTest - going to forward left, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
							EXIT 
						ELIF (iLeftTotal = 0)
							fPlayerHeading += 90.0
							SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
							NET_PRINT("[spawning] DoSpawningShapeTest - going to left, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
							EXIT
						ELSE
							ChooseDirectionWithLowestScore(iLowestDirection, fPlayerHeading, vVecToRoad)							
						ENDIF
					ELSE
						NET_PRINT("[spawning] DoSpawningShapeTest - Prefers to the left ")  NET_NL()
						IF (iForwardLeftTotal = 0)
							fPlayerHeading += 45.0
							SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
							NET_PRINT("[spawning] DoSpawningShapeTest - going to forward left, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
							EXIT 
						ELIF (iLeftTotal = 0)
							fPlayerHeading += 90.0
							SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
							NET_PRINT("[spawning] DoSpawningShapeTest - going to left, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
							EXIT 
						ELIF (iForwardRightTotal = 0)
							fPlayerHeading += -45.0
							SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
							NET_PRINT("[spawning] DoSpawningShapeTest - going to forward right, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
							EXIT
						ELIF (iRightTotal = 0)	
							fPlayerHeading += -90.0
							SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
							NET_PRINT("[spawning] DoSpawningShapeTest - going to right, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
							EXIT 
						ELSE
							ChooseDirectionWithLowestScore(iLowestDirection, fPlayerHeading, vVecToRoad)				
						ENDIF				
					ENDIF
				
				ELSE
					
					NET_PRINT("[spawning] DoSpawningShapeTest - no face point - choose random pref ")  NET_NL()
					
					i = GET_RANDOM_INT_IN_RANGE(0, 2)
					
					IF (i = 0)
						NET_PRINT("[spawning] DoSpawningShapeTest - Prefers to the right ")  NET_NL()
						IF (iRightTotal = 0)	
							fPlayerHeading += -90.0
							SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
							NET_PRINT("[spawning] DoSpawningShapeTest - going to right, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
							EXIT 
						ELIF (iLeftTotal = 0)
							fPlayerHeading += 90.0
							SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
							NET_PRINT("[spawning] DoSpawningShapeTest - going to left, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
							EXIT 
						ELSE
							ChooseDirectionWithLowestScore(iLowestDirection, fPlayerHeading, vVecToRoad)					
						ENDIF			
					ELSE
						NET_PRINT("[spawning] DoSpawningShapeTest - Prefers to the left ")  NET_NL()
						IF (iLeftTotal = 0)
							fPlayerHeading += 90.0
							SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
							NET_PRINT("[spawning] DoSpawningShapeTest - going to left, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
							EXIT 
						ELIF (iRightTotal = 0)	
							fPlayerHeading += -90.0
							SetPlayerHeadingAndUpdateCamera(fPlayerHeading)
							NET_PRINT("[spawning] DoSpawningShapeTest - going to right, setting heading of ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
							EXIT 
						ELSE
							ChooseDirectionWithLowestScore(iLowestDirection, fPlayerHeading, vVecToRoad)					
						ENDIF				
					ENDIF
			
					
				ENDIF
			ENDIF
			
		ENDIF
	ELSE
		NET_PRINT("[spawning] DoSpawningShapeTest - player in a vehicle ")  NET_NL()
	ENDIF			
		
	NET_PRINT("[spawning] DoSpawningShapeTest - finishing - not setting any heading ")  NET_NL()
	
ENDPROC

PROC CLEANUP_SPAWN_VEHICLE()
	PRINTLN("[spawning] CLEANUP_SPAWN_VEHICLE - called - g_SpawnData.MissionSpawnDetails.SpawnedVehicle = ", NATIVE_TO_INT(g_SpawnData.MissionSpawnDetails.SpawnedVehicle))
	DEBUG_PRINTCALLSTACK()
	IF IS_VEHICLE_MY_PERSONAL_VEHICLE(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
		//SET_VEHICLE_AS_NO_LONGER_NEEDED(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
		NET_PRINT("[spawning] CLEANUP_SPAWN_VEHICLE() - my personal vehicle, not cleaning up.") NET_NL()
		g_SpawnData.MissionSpawnDetails.SpawnedVehicle = INT_TO_NATIVE(VEHICLE_INDEX, -1)
	ELSE
		VEHICLE_INDEX vehID
		vehID = g_SpawnData.MissionSpawnDetails.SpawnedVehicle // we dont want to lose the handle to this vehicle (url:bugstar:7421339)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehID)	
		PRINTLN("[spawning] CLEANUP_SPAWN_VEHICLE - SET_VEHICLE_AS_NO_LONGER_NEEDED g_SpawnData.MissionSpawnDetails.SpawnedVehicle = ", NATIVE_TO_INT(g_SpawnData.MissionSpawnDetails.SpawnedVehicle))
	ENDIF
	PRINTLN("[spawning] CLEANUP_SPAWN_VEHICLE - g_SpawnData.MissionSpawnDetails.SpawnedVehicle = ", NATIVE_TO_INT(g_SpawnData.MissionSpawnDetails.SpawnedVehicle))
ENDPROC

PROC WarpToSpawnLocation_CleanupNetWarp()
	IF NOT g_SpawnData.bCanSetDelayTime
		g_SpawnData.bCanSetDelayTime = TRUE
	ENDIF

	CLEAR_WARP_TO_AREA()
	
	#IF IS_DEBUG_BUILD
		NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - NET WARP - finished.") NET_NL()
	#ENDIF							

	// do shape test to avoid facing wall etc.
	IF (g_SpawnData.bDoShapeTest)
		DoSpawningShapeTest()
		g_SpawnData.bDoShapeTest = FALSE
		NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - g_SpawnData.bDoShapeTest set to FALSE.") NET_NL()
	ENDIF
		
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_CLEANUP
	#IF IS_DEBUG_BUILD
		NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_DO_NET_WARP - warp finished, going to WARP_TO_SPAWN_STATE_CLEANUP") NET_NL()
	#ENDIF
ENDPROC

PROC UPDATE_RACE_RESPOT_AT_RESPAWN(BOOL bDontMarkAsNoLongerNeeded=FALSE)

	IF (g_b_On_Race)
	OR (g_SpawnData.MissionSpawnDetails.bUseRaceRespotAfterRespawns)
		IF (g_SpawnData.MissionSpawnDetails.bJustRespawnedInRace)
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				
				FLOAT fDist2
			
				IF (g_b_Is_Air_Race)
					fDist2 = 100.0 // 10m
				ELSE
					fDist2 = 36.0 // 6m
				ENDIF
				
				
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND NOT IS_PED_BEING_JACKED(PLAYER_PED_ID())
				AND	((VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), g_SpawnData.MissionSpawnDetails.vRaceRespawnPos) < fDist2)
					OR IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(PLAYER_ID())
					OR IS_PLAYER_RESPAWNING(PLAYER_ID()))
					
					IF PlayerIsInBlazer5()
					OR PlayerIsInRuiner2()
					OR PlayerIsInThruster()
						// Is player lowering or raising its wheels.
						IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ROCKET_BOOST)
							g_SpawnData.MissionSpawnDetails.TimerRaceRespot = GET_NETWORK_TIME_ACCURATE()
							MakeVehicleFlashIfDriver(1000, DEFAULT, TRUE)	
						ELSE
							IF (ABSI(GET_TIME_DIFFERENCE(g_SpawnData.MissionSpawnDetails.TimerRaceRespot, GET_NETWORK_TIME_ACCURATE())) > 2000)
								PRINTLN("[Spawning] UPDATE_RACE_RESPOT_AT_RESPAWN - Calling MakeVehicleFlashIfDriver(2500)")									
								MakeVehicleFlashIfDriver(2500, DEFAULT, bDontMarkAsNoLongerNeeded)	
								g_SpawnData.MissionSpawnDetails.TimerRaceRespot = GET_NETWORK_TIME_ACCURATE()
							ENDIF
						ENDIF
					ELSE
						g_SpawnData.MissionSpawnDetails.TimerRaceRespot = GET_NETWORK_TIME_ACCURATE()
						MakeVehicleFlashIfDriver(1000, DEFAULT, bDontMarkAsNoLongerNeeded)	
					ENDIF
				ELSE
					g_SpawnData.MissionSpawnDetails.vRaceRespawnPos = <<0.0, 0.0, 0.0>>
					g_SpawnData.MissionSpawnDetails.bJustRespawnedInRace = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		// if our last spawn vehicle still exists keep trying to delete it
		IF DOES_ENTITY_EXIST(g_SpawnData.MissionSpawnDetails.LastSpawnedVehicle)
			PRINTLN("[Spawning] UPDATE_RACE_RESPOT_AT_RESPAWN - last spawn vehicle still exists, try and delete it.")
			SetVehicleToDelete(g_SpawnData.MissionSpawnDetails.LastSpawnedVehicle)		
		ENDIF
		
	ENDIF
	
ENDPROC

PROC SET_PLAYER_HAS_JUST_RESPAWNED_IN_RACE()
	g_SpawnData.MissionSpawnDetails.bJustRespawnedInRace = TRUE	
	g_SpawnData.MissionSpawnDetails.vRaceRespawnPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	g_SpawnData.MissionSpawnDetails.TimerRaceRespot = GET_TIME_OFFSET(GET_NETWORK_TIME_ACCURATE(), -9999)
	NET_PRINT("[spawning] SET_PLAYER_HAS_JUST_RESPAWNED_IN_RACE - called with ") NET_PRINT_VECTOR(g_SpawnData.MissionSpawnDetails.vRaceRespawnPos) NET_NL()

	UPDATE_RACE_RESPOT_AT_RESPAWN(TRUE)
ENDPROC

PROC SET_PLAYER_HAS_JUST_RESPAWNED_IN_RACE_AT_COORDS(VECTOR vCoords)
	g_SpawnData.MissionSpawnDetails.bJustRespawnedInRace = TRUE	
	g_SpawnData.MissionSpawnDetails.vRaceRespawnPos = vCoords
	g_SpawnData.MissionSpawnDetails.TimerRaceRespot = GET_TIME_OFFSET(GET_NETWORK_TIME_ACCURATE(), -9999)
	NET_PRINT("[spawning] SET_PLAYER_HAS_JUST_RESPAWNED_IN_RACE_AT_COORDS - called with ") NET_PRINT_VECTOR(g_SpawnData.MissionSpawnDetails.vRaceRespawnPos) NET_NL()

	UPDATE_RACE_RESPOT_AT_RESPAWN(TRUE)
ENDPROC


/// PURPOSE: 
///    Easy way of warping the player to a spawn location.
/// PARAMS:
///    SpawnLocation = where do we want the player to spawn (see enum PLAYER_SPAWN_LOCATION)
///    bLeavePedBehind = set to true if you want a NPC to be left behind.
///    bSpawnInVehicle = TRUE if you want the ped to spawn in a vehicle. 
///    bUseLastCarIfDriveable = TRUE if you want the player to keep their vehicle
/// RETURNS:
///    TRUE - when the player has been safely warped to the spawn location.
FUNC BOOL WARP_TO_SPAWN_LOCATION(	PLAYER_SPAWN_LOCATION SpawnLocation = SPAWN_LOCATION_AUTOMATIC, 
									BOOL bLeavePedBehind = TRUE, 
									BOOL bWarpIntoSpawnVehicle = FALSE, 
									BOOL bDoVisibleTeammateCheck=FALSE, 
									BOOL bDoQuickWarp=FALSE, 
									BOOL bDontAskPermission=FALSE, 
									BOOL bDontUseVehicleNodes=FALSE, 
									BOOL bKeepCurrentVehicle = FALSE, 
									BOOL bUseCarNodeOffset=TRUE, 
									INT iRespotTime=VEHICLE_RESPOT_TIME, 
									BOOL bSetGameCamNearPlayerIfBackCloseToWall=TRUE, 
									BOOL bMinTimeHasPassed=TRUE, 
									BOOL bUseStartOfMissionPVSpawn=FALSE, 
									BOOL bCheckEntityArea=FALSE, 
									BOOL bKeepInvisibleAfterFinish=FALSE, 
									BOOL bMoveIfSpawnedInsideAnotherVehicleOrPlayer=FALSE,
									BOOL bSnapToGround = TRUE,
									MODEL_NAMES eOverrideModel=DUMMY_MODEL_FOR_SCRIPT)

//	PLAYER_INDEX PartnerID
	
	FLOAT fGroundZ
	BOOL bKeepVehicle
	VEHICLE_INDEX CarID
	
	#IF IS_DEBUG_BUILD
	IF (bDoQuickWarp)
		NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - called with quick warp!") NET_NL()
	ENDIF
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - NETWORK_IS_GAME_IN_PROGRESS = FALSE !!!!") NET_NL()
	ENDIF
	#ENDIF
	
	DISABLE_SELECTOR_THIS_FRAME()
	
	// if player is in process of respawning then return false (but not if they are doing the manual respawn placement
	IF IS_PLAYER_RESPAWNING(PLAYER_ID(), TRUE)
	
		// if it is a quickwarp and the player is alive but somehow in the wait stage of respawning during a player switch, then allow a quick warp to take place. (see 1669794)
		IF (bDoQuickWarp)
		AND IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, FALSE)
		AND IS_PLAYER_SWITCH_IN_PROGRESS()
		AND GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_WAIT_FOR_DECISION
			#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - called when player is in process of respawning - but player is alive and in a player switch, so allowing quick warp.  ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
			#ENDIF
		ELSE
		
			IF (GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_POST_BINK_VIDEO_WARP)
				#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - called when player is in process of respawning - but  TRANSITION_STATE_POST_BINK_VIDEO_WARP.  ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - called when player is in process of respawning - called by  ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
					DEBUG_PRINTCALLSTACK()
				#ENDIF		
				RETURN(FALSE)
			ENDIF
		ENDIF
	ENDIF	
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID()) // player must be alive to warp
	
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SuppressInAirEvent, TRUE) // stop player ped from falling (when respawning in air)
	
		// is this a new call?
		IF NOT (g_SpawnData.WarpToSpawnLocation = SpawnLocation)
		
			// is this a fallback from custom spawn point spawning in a vehicle?
			IF (bMoveIfSpawnedInsideAnotherVehicleOrPlayer)
			AND (g_SpawnData.WarpToSpawnLocation = SPAWN_LOCATION_NEAR_CURRENT_POSITION_AS_POSSIBLE)
			
				// do nothing.
				
			ELSE
			
				// is this a restart?
				IF NOT (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_INIT)

				
					// another WARP_TO_SPAWN_LOCATION is happening
					// is the other ongoing warp still being updated? 
					IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iWarpToLocationTime) < SpawningFunctionTimeoutValue())
					
						#IF IS_DEBUG_BUILD
						NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - another warp already started, returning FALSE - new warp called from ") NET_PRINT(GET_THIS_SCRIPT_NAME()) 
						NET_PRINT(", SpawnLocation = ") NET_PRINT_INT(ENUM_TO_INT(SpawnLocation))
						NET_NL()
						#ENDIF					
						
						// return FALSE, to give the previous WARP_TO_SPAWN_LOCATION a chance to finish
						RETURN(FALSE)
					
					ENDIF	
				
			
					#IF IS_DEBUG_BUILD
					NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - another warp already started, but not been updated in a while, so starting new one from script ") NET_PRINT(GET_THIS_SCRIPT_NAME()) 
					NET_PRINT(", SpawnLocation = ") NET_PRINT_INT(ENUM_TO_INT(SpawnLocation))
					NET_NL()
					#ENDIF		
				
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_INIT
				
				ENDIF
				
			ENDIF
			
		ENDIF

			
		// ------ INITIALISE -----------
		IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_INIT)
			
			g_SpawnData.WarpToSpawnLocation = SpawnLocation
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_FIND_POINT	

			IF (bWarpIntoSpawnVehicle)
				IF NOT IS_SPAWNING_IN_VEHICLE()
					bWarpIntoSpawnVehicle = FALSE
					SCRIPT_ASSERT("WARP_TO_SPAWN_LOCATION - Told to warp into spawn vehicle, but spawn vehicle not setup.")
				ENDIF
			ENDIF	
			
			#IF IS_DEBUG_BUILD
				NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_INIT - g_SpawnData.WarpToSpawnLocation = ") NET_PRINT_INT(ENUM_TO_INT(g_SpawnData.WarpToSpawnLocation)) NET_NL()
				NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_INIT - going to WARP_TO_SPAWN_STATE_FIND_POINT.") NET_NL()
			#ENDIF	
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_FIND_POINT

			g_SpawnData.bHasQueriedSpecificPosition = FALSE // for if it needs to do a query
			g_SpawnData.iQuestSpecificSpawnReply = 0
			g_SpawnData.iQuerySpecificSpawnState = 0
			g_SpawnData.bHasAttemptedFallbackUsingCustomPoints = FALSE
		
			#IF FEATURE_GEN9_EXCLUSIVE
			g_SpawnData.bHasCalledMpTutorialBikerBusinessEntitySet = FALSE
			#ENDIF
		
			#IF IS_DEBUG_BUILD		
			g_SpawnData.iWarpStartTime = GET_NETWORK_TIME()		
			#ENDIF
		ENDIF
		
		// ------ FIND A SUITABLE SPAWN POINT -----------
		IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_FIND_POINT)
		
			BOOL bIsForAVehicle
		
			IF (bWarpIntoSpawnVehicle)
			OR (bKeepCurrentVehicle)
				bIsForAVehicle = TRUE
			ENDIF
		
			IF (g_SpawnData.WarpToSpawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS)
				g_SpawnData.vWarpToLocationCoords = g_SpecificSpawnLocation.vCoords
				g_SpawnData.fWarpToLocationHeading = g_SpecificSpawnLocation.fHeading
				g_SpawnData.fPitch = g_SpecificSpawnLocation.fPitch
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = 	WARP_TO_SPAWN_STATE_CHECK_WARP_VEHICLE
				#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_FIND_POINT - at specific loation.  g_SpawnData.vWarpToLocationCoords = ") NET_PRINT_VECTOR(g_SpawnData.vWarpToLocationCoords) NET_NL()	
				#ENDIF
			ELSE
		
				IF HAS_GOT_SPAWN_LOCATION(g_SpawnData.vWarpToLocationCoords, g_SpawnData.fWarpToLocationHeading, g_SpawnData.WarpToSpawnLocation, bIsForAVehicle, bDoVisibleTeammateCheck, bDontAskPermission, bDontUseVehicleNodes, bUseCarNodeOffset, bUseStartOfMissionPVSpawn, DEFAULT, bCheckEntityArea, eOverrideModel)
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = 	WARP_TO_SPAWN_STATE_CHECK_WARP_VEHICLE			
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - Has got spawn location = true - going to WARP_TO_SPAWN_STATE_CHECK_WARP_VEHICLE ") NET_NL()	
					#ENDIF
				ENDIF
			ENDIF
		ENDIF

		// ----- CHECK IF IN THE CORRECT WARP VEHICLE ----
		IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_CHECK_WARP_VEHICLE)
			
			IF (bMinTimeHasPassed)
		
				IF (bWarpIntoSpawnVehicle)
			
					// if we are not keeping the vehicle then delete whatever vehicle the player is in
	//				IF NOT (bKeepCurrentVehicle)
	//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	//						CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	//						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	//						DeleteVehicleIfPossible(CarID)
	//					ENDIF
	//				ENDIF		
			
	//				IF (bKeepCurrentVehicle)
	//				AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	//				AND IsVehicleSuitableToUseAsWarpVehicle(GET_PLAYERS_LAST_VEHICLE())
	//					// we don't actually use the current vehicle if the player isn't in it (too difficult to get the syncing looking good)
	//					// so we just delete it and let the new vehiclce get created.
	//					CarID = GET_PLAYERS_LAST_VEHICLE()
	//					SetVehicleToDelete(CarID)
	//					#IF IS_DEBUG_BUILD
	//						NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_CHECK_WARP_VEHICLE - using players last vehicle (actually deleting it).") NET_NL()
	//					#ENDIF
	//				ENDIF


					// we don't actually use the current vehicle if the player isn't in it (too difficult to get the syncing looking good)
					// so we just delete it and let the new vehiclce get created.
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						DeleteVehicleIfPossible(CarID)
						#IF IS_DEBUG_BUILD
							NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_CHECK_WARP_VEHICLE - deleting vehicle player is in.") NET_NL()
						#ENDIF					
					ELSE
						CarID = GET_PLAYERS_LAST_USED_VEHICLE()
						SetVehicleToDelete(CarID)		
						#IF IS_DEBUG_BUILD
							NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_CHECK_WARP_VEHICLE - deleting players last vehicle.") NET_NL()
						#ENDIF	
					ENDIF
												
				
					IF IS_SPAWNING_IN_VEHICLE()					
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
							IF NOT (GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = g_SpawnData.MissionSpawnDetails.SpawnModel)
								VEHICLE_INDEX VehicleID
								VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
								IF (VehicleID = g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
									CLEANUP_SPAWN_VEHICLE()
								ENDIF
								CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
								#IF IS_DEBUG_BUILD
								NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_CHECK_WARP_VEHICLE - player in vehicle, but not spawn model.") NET_NL()
								#ENDIF
							ENDIF
						ENDIF
					ENDIF				
						
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())				
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_WARP_INTO_VEHICLE	
						#IF IS_DEBUG_BUILD
						NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_CHECK_WARP_VEHICLE - going to WARP_TO_SPAWN_STATE_WARP_INTO_VEHICLE") NET_NL()
						#ENDIF
					ELSE
					
						CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						
						IF NETWORK_HAS_CONTROL_OF_ENTITY(CarID)						
							SetSpawnVehicleAttributes(CarID)
							CLEAR_WARP_TO_AREA(TRUE)
							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_DO_NET_WARP
							#IF IS_DEBUG_BUILD
							NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_CHECK_WARP_VEHICLE - going to WARP_TO_SPAWN_STATE_DO_NET_WARP (already in vehicle)") NET_NL()
							#ENDIF												
						ELSE
							NETWORK_REQUEST_CONTROL_OF_ENTITY(CarID)
							#IF IS_DEBUG_BUILD
							NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_CHECK_WARP_VEHICLE - requesting control of entity...") NET_NL()
							#ENDIF	
						ENDIF		
					
						

					ENDIF
						
					
					
				ELSE
								
					CLEAR_WARP_TO_AREA(TRUE)				
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_DO_NET_WARP
					
					#IF IS_DEBUG_BUILD
						NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - going to WARP_TO_SPAWN_STATE_DO_NET_WARP") NET_NL()
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - bMinTimeHasPassed = FALSE") NET_NL()
				#ENDIF
			ENDIF
				
		ENDIF


		// ----- WARP INTO APPROPRIATE VEHICLE ----
		IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_WARP_INTO_VEHICLE)

			IF HasLoadedVehicleSpawnModelIfNecessary()
				IF g_SpawnData.MissionSpawnDetails.bSpawnInVehicle
					IF WarpPlayerIntoSpawnVehicle(<<g_SpawnData.vWarpToLocationCoords.x, g_SpawnData.vWarpToLocationCoords.y, GetHideZForModel(g_SpawnData.MissionSpawnDetails.SpawnModel)>>, g_SpawnData.fWarpToLocationHeading, FALSE)
						g_SpawnData.iWarpIntoVehicleTime = GET_NETWORK_TIME()
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState =  WARP_TO_SPAWN_STATE_WAIT_TO_WARP_INTO_VEHICLE						
						#IF IS_DEBUG_BUILD
						NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP INTO APPROPRIATE VEHICLE - created vehicle.") NET_NL()
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP INTO APPROPRIATE VEHICLE - waiting to warp player into spawn vehicle.") NET_NL()
						#ENDIF	
					ENDIF
				ELSE
					NET_PRINT("[Spawning] WARP_TO_SPAWN_LOCATION - Unable to warp player into spawn vehicle, bSpawnInVehicle = FALSE") NET_NL()
										
					IF IS_ON_FOOT_TRANSFORM_MODEL(g_SpawnData.MissionSpawnDetails.SpawnModel)
					OR g_SpawnData.MissionSpawnDetails.SpawnModel = DUMMY_MODEL_FOR_SCRIPT
						PRINTLN("[Spawning] WARP_TO_SPAWN_LOCATION - On Foot Model. Sending to Warp State.")
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_DO_NET_WARP
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP INTO APPROPRIATE VEHICLE - loading model.") NET_NL()
				#ENDIF
			ENDIF

		ENDIF
		
		// ------ WARP_TO_SPAWN_STATE_WAIT_TO_WARP_INTO_VEHICLE -----------
		IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_WAIT_TO_WARP_INTO_VEHICLE)

			// check if player for some reason has lost the task of warping into the vehicle
			#IF IS_DEBUG_BUILD
				NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_WAIT_TO_WARP_INTO_VEHICLE - GET_SCRIPT_TASK_STATUS = ") NET_PRINT_INT(ENUM_TO_INT(GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE ))) NET_NL()
			#ENDIF

			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SuppressInAirEvent, TRUE) // make sure the ped is not doing the falling anim. 3114610

			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())				
				//CLEAR_WARP_TO_AREA() // removed this due to 1605433, it was clearing the area too early.
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_DO_NET_WARP		
				#IF IS_DEBUG_BUILD
					NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_WAIT_TO_WARP_INTO_VEHICLE - not on rally, going to WARP_TO_SPAWN_STATE_DO_NET_WARP") NET_NL()
				#ENDIF
				IF (g_SpawnData.MissionSpawnDetails.bSpawningCreatedNewCar)
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bHasRespawnedInVehicle = TRUE
				ELSE
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bHasRespawnedInVehicle = FALSE	
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_WAIT_TO_WARP_INTO_VEHICLE - player ped is not in a vehicle! ") 
					NET_PRINT("time = ") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iWarpIntoVehicleTime))
					NET_NL()
				#ENDIF		
				
				IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iWarpIntoVehicleTime) > 5000)
					#IF IS_DEBUG_BUILD
						NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_WAIT_TO_WARP_INTO_VEHICLE - player ped is not in a vehicle - TIMED OUT") NET_NL()
						SCRIPT_ASSERT("WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_WAIT_TO_WARP_INTO_VEHICLE - timed out, why?")
					#ENDIF	
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_WARP_INTO_VEHICLE	
				ELSE
				
					// if player has somehow lost the warp into car task
					IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE ) = FINISHED_TASK)
						IF DOES_ENTITY_EXIST(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
						AND IS_VEHICLE_DRIVEABLE(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
							#IF IS_DEBUG_BUILD
								NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_WAIT_TO_WARP_INTO_VEHICLE - player lost warp into car task, warping into car") NET_NL()
							#ENDIF	
							WarpPlayerIntoCar(PLAYER_PED_ID(), g_SpawnData.MissionSpawnDetails.SpawnedVehicle, VS_DRIVER)	
						ELSE
							#IF IS_DEBUG_BUILD
								NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_WAIT_TO_WARP_INTO_VEHICLE - player lost warp into car task, and car doesn't exist or is dead") NET_NL()
							#ENDIF
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_WARP_INTO_VEHICLE	
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF

		ENDIF
		
		// ------ DO THE NET WARP -----------
		IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_DO_NET_WARP)
				
			#IF IS_DEBUG_BUILD
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - doing net warp - player in vehicle.") NET_NL()
				ELSE
					NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - doing net warp - player NOT in vehicle.") NET_NL()
				ENDIF
			#ENDIF
		
			g_SpawnData.iWarpToLocationTime = GET_NETWORK_TIME()

			IF g_SpawnData.bCanSetDelayTime
				g_SpawnData.iWarpDelayTime = GET_NETWORK_TIME()
				g_SpawnData.bCanSetDelayTime = FALSE
			ENDIF

			IF (bKeepCurrentVehicle)
				bKeepVehicle = TRUE
			ELSE
				bKeepVehicle = bWarpIntoSpawnVehicle	
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF (bKeepVehicle)
					NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - doing net warp - bKeepVehicle = TRUE.") NET_NL()
				ELSE
					NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - doing net warp - bKeepVehicle = FALSE.") NET_NL()
				ENDIF
			#ENDIF
			
			// Fix for 2882948 - adding delay before warping player.
			BOOL bMinDelayTimePassed
			
			IF ABSI((GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iWarpDelayTime))) >= 350 // Fix for B*3308286, upping delay by 50ms
				bMinDelayTimePassed = TRUE
			ELSE
				PRINTLN("[RSPWN] Waiting to warp player - time = ", GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iWarpDelayTime))
				bMinDelayTimePassed = FALSE
			ENDIF	
			
			// if we are on a race, start the respot timer early, to avoid 4484234
			IF (g_b_On_Race)
				SET_PLAYER_HAS_JUST_RESPAWNED_IN_RACE_AT_COORDS(g_SpawnData.vWarpToLocationCoords)
				PRINTLN("[spawning] - starting respot timer early")
			ENDIF
			
			// the players current vehicle dies before we start the warp then clear the task.
			IF (bKeepCurrentVehicle)
			AND NOT (MPGlobals.g_NetWarpStarted)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(CarID)
						IF IS_ENTITY_DEAD(CarID)
						OR NOT IS_VEHICLE_DRIVEABLE(CarID)
						OR IS_VEHICLE_FUCKED_MP(CarID)
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							#IF IS_DEBUG_BUILD
								NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - bKeepCurrentVehicle true but vehicle is fucked, clearing ped tasks.") NET_NL()
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
	
			IF bMinDelayTimePassed
			
				#IF FEATURE_GEN9_EXCLUSIVE
				IF (SpawnLocation = SPAWN_LOCATION_TUTORIAL_BUSINESS)
				AND NOT (g_SpawnData.bHasCalledMpTutorialBikerBusinessEntitySet)
					ACTIVATE_SIMPLE_INTERIOR_FOR_BIKER_BUSINESS_PRE_WARP()
					g_SpawnData.bHasCalledMpTutorialBikerBusinessEntitySet = TRUE
				ENDIF
				#ENDIF			
			
				IF NET_WARP_TO_COORD(g_SpawnData.vWarpToLocationCoords, 
										g_SpawnData.fWarpToLocationHeading, 
										bKeepVehicle, 
										bLeavePedBehind, 
										FALSE, 
										FALSE, 
										TRUE, 
										bDoQuickWarp,
										bSnapToGround,
										TRUE,
										g_SpawnData.fPitch)			
			
					// special case for warping into biker businesses during tutorial.
					#IF FEATURE_GEN9_EXCLUSIVE
					IF (SpawnLocation = SPAWN_LOCATION_TUTORIAL_BUSINESS)
						ACTIVATE_SIMPLE_INTERIOR_FOR_BIKER_BUSINESS_POST_WARP()	
					ENDIF
					#ENDIF
			
			
					// if we are spawning at custom spawn point and can fallback to the navmesh and have ended up inside a vehicle, then move.
					IF (bMoveIfSpawnedInsideAnotherVehicleOrPlayer)				
					//AND NOT ((g_SpawnData.WarpToSpawnLocation = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS) AND (g_SpawnData.CustomSpawnPointInfo.bNeverFallBackToNavMesh))
					
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_POST_NET_WARP_CHECK
						PRINTLN("[spawning] WARP_TO_SPAWN_LOCATION - NET WARP - going to WARP_TO_SPAWN_STATE_POST_NET_WARP_CHECK")
						RETURN FALSE
						
					ELSE
			
						WarpToSpawnLocation_CleanupNetWarp()

					ENDIF
	
				ENDIF
			ENDIF
		ENDIF	
		
		// ------ post warp check, if we've spawning inside a vehicle or other player -----------
		IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_POST_NET_WARP_CHECK)
			IF (g_SpawnData.WarpToSpawnLocation = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS)
			AND (g_SpawnData.CustomSpawnPointInfo.bNeverFallBackToNavMesh)
			AND (g_SpawnData.bHasAttemptedFallbackUsingCustomPoints)
				
				PRINTLN("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_POST_NET_WARP_CHECK - already tried custom spawn point fallback. ")
				
				WarpToSpawnLocation_CleanupNetWarp()
			ELSE
				IF HasSpawnedInsideNearbyVehicle(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
				OR HasSpawnedInsideNearbyPlayer()
					
					PRINTLN("[spawning] WARP_TO_SPAWN_LOCATION - have spawned inside a vehicle, moving player to nearest position possible. ")
				
					// we have spawned inside a vehicle, warp nearby.	
					IF (g_SpawnData.WarpToSpawnLocation = SPAWN_LOCATION_CUSTOM_SPAWN_POINTS)
					AND NOT (g_SpawnData.bHasAttemptedFallbackUsingCustomPoints)							
						g_SpawnData.bHasAttemptedFallbackUsingCustomPoints = TRUE
						PRINTLN("[spawning] WARP_TO_SPAWN_LOCATION - have spawned inside a personal vehicle, attempting custom spawn points again.")
					ELSE						
						g_SpawnData.WarpToSpawnLocation = SPAWN_LOCATION_NEAR_CURRENT_POSITION_AS_POSSIBLE
					ENDIF 
						
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_FIND_POINT	

					g_SpawnData.bHasQueriedSpecificPosition = FALSE // for if it needs to do a query
					g_SpawnData.iQuestSpecificSpawnReply = 0
					g_SpawnData.iQuerySpecificSpawnState = 0
				ELSE
					WarpToSpawnLocation_CleanupNetWarp()		
				ENDIF
			ENDIF
		ENDIF
		
		// ------ check the ground z if warping to a race corona ------
		IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_CHECK_GROUND_Z_FOR_RACE_CORONAS)							
			IF VDIST(g_SpecificSpawnLocation.vCoords, g_SpawnData.vWarpToLocationCoords) > 0.1						
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())	
					IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(CarID))
					OR IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(CarID))
					
						GET_GROUND_Z_FOR_3D_COORD(<<g_SpawnData.vWarpToLocationCoords.x, g_SpawnData.vWarpToLocationCoords.y, g_SpawnData.vWarpToLocationCoords.z>>, fGroundZ)
						IF (fGroundZ = 0.0)
						OR (g_SpawnData.vWarpToLocationCoords.z - fGroundZ > 1.5)
							IF VDIST(g_SpecificSpawnLocation.vCoords, g_SpawnData.vWarpToLocationCoords) > 15.0
								g_SpawnData.vWarpToLocationCoords = g_SpecificSpawnLocation.vCoords
								IF NETWORK_HAS_CONTROL_OF_ENTITY(CarID)
									SET_ENTITY_COORDS(CarID, <<g_SpawnData.vWarpToLocationCoords.x, g_SpawnData.vWarpToLocationCoords.y, fGroundZ + GET_MODEL_HEIGHT(GET_ENTITY_MODEL(CarID))>> )													
									SET_VEHICLE_ON_GROUND_PROPERLY(CarID)
								ENDIF
								GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_CLEANUP	
								#IF IS_DEBUG_BUILD
									NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_CHECK_GROUND_Z_FOR_RACE_CORONAS - couldn't find a safe z. fGroundZ = ") NET_PRINT_FLOAT(fGroundZ) NET_NL()
								#ENDIF									
							ELSE
								g_SpawnData.vWarpToLocationCoords.z += 1.0
								#IF IS_DEBUG_BUILD
									NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_CHECK_GROUND_Z_FOR_RACE_CORONAS - incrementing g_SpawnData.vWarpToLocationCoords.z, fGroundZ = ") NET_PRINT_FLOAT(fGroundZ) NET_NL()
								#ENDIF		
							ENDIF
						ELSE
							IF NETWORK_HAS_CONTROL_OF_ENTITY(CarID)
								SET_ENTITY_COORDS(CarID, <<g_SpawnData.vWarpToLocationCoords.x, g_SpawnData.vWarpToLocationCoords.y, fGroundZ + GET_MODEL_HEIGHT(GET_ENTITY_MODEL(CarID))>> )													
								SET_VEHICLE_ON_GROUND_PROPERLY(CarID)
							ENDIF
							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_CLEANUP	
							#IF IS_DEBUG_BUILD
								NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_CHECK_GROUND_Z_FOR_RACE_CORONAS - ground z is fine, moving to WARP_TO_SPAWN_STATE_CLEANUP. fGroundZ = ") NET_PRINT_FLOAT(fGroundZ) NET_NL()
							#ENDIF	
						ENDIF
						
					ELSE
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_CLEANUP	
						#IF IS_DEBUG_BUILD
							NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_CHECK_GROUND_Z_FOR_RACE_CORONAS - ground z is fine, moving to WARP_TO_SPAWN_STATE_CLEANUP. not a car or bike ") NET_NL()
						#ENDIF	
					ENDIF
				ELSE
					GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_CLEANUP	
					#IF IS_DEBUG_BUILD
						NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_CHECK_GROUND_Z_FOR_RACE_CORONAS - ground z is fine, moving to WARP_TO_SPAWN_STATE_CLEANUP. not in vehicle ")  NET_NL()
					#ENDIF	
				ENDIF
			ELSE
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_CLEANUP	
				#IF IS_DEBUG_BUILD
					NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - WARP_TO_SPAWN_STATE_CHECK_GROUND_Z_FOR_RACE_CORONAS - ground z is fine, moving to WARP_TO_SPAWN_STATE_CLEANUP. using original corona coords ")  NET_NL()
				#ENDIF
			ENDIF
		ENDIF

		
		// ------ cleanup warp -----------
		IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = WARP_TO_SPAWN_STATE_CLEANUP)
		
			IF NOT(bKeepInvisibleAfterFinish)		
				IF DOES_ENTITY_EXIST(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
				AND NETWORK_HAS_CONTROL_OF_ENTITY(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
					SET_ENTITY_VISIBLE(g_SpawnData.MissionSpawnDetails.SpawnedVehicle, TRUE)
					NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - SET_ENTITY_VISIBLE(g_SpawnData.MissionSpawnDetails.SpawnedVehicle, TRUE).") NET_NL()
				ENDIF		
			ELSE
				PRINTLN("[spawning] WARP_TO_SPAWN_STATE_CLEANUP - bKeepInvisibleAfterFinish = TRUE")				
			ENDIF			

			IF (bWarpIntoSpawnVehicle)
			OR (bKeepCurrentVehicle)
				IF (iRespotTime > 0)
					MakeVehicleFlashIfDriver(iRespotTime) // should maybe only call if on race?
				ENDIF
			ENDIF			
			
			ClearAreaAroundPointForSpawning(GET_PLAYER_COORDS(PLAYER_ID()))
			
			IF DOES_ENTITY_EXIST(g_SpawnData.MissionSpawnDetails.SpawnedVehicle) 
				CLEANUP_SPAWN_VEHICLE()				
				#IF IS_DEBUG_BUILD
					NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - cleanup - g_SpawnData.MissionSpawnDetails.SpawnedVehicle MARKED AS NOT NEEDED and not frozen.") NET_NL()
				#ENDIF	
			ENDIF
			
			IF NOT (IS_ON_FOOT_TRANSFORM_MODEL(g_SpawnData.MissionSpawnDetails.SpawnModel) OR g_SpawnData.MissionSpawnDetails.SpawnModel = DUMMY_MODEL_FOR_SCRIPT)
			AND NOT g_SpawnData.NextSpawn.bIsAerialCheckpoint
				SetPlayerOnGroundProperly()
				PRINTLN("[spawning] WARP_TO_SPAWN_STATE_CLEANUP - called SetPlayerOnGroundProperly")
			ENDIF
			
			// set camera
			SetGameCamBehindPlayer()	
			
			IF (bSetGameCamNearPlayerIfBackCloseToWall)
				MAKE_GAME_CAM_NEAR_TO_PLAYER()						
			ENDIF
			
			RESET_LAST_DAMAGER()
			
			#IF IS_DEBUG_BUILD
				NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - returning TRUE - time to complete = ") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME() , g_SpawnData.iWarpStartTime)) NET_NL()
			#ENDIF
			
				UPDATE_ALL_YACHTS_I_AM_ON_OR_NEAR()
			
			IF (bCheckEntityArea)
				Cleanup_Entity_Area_For_Spawning_Vehicle()
			ENDIF			
			
			RESET_WARP_TO_SPAWN_LOCATION_GLOBALS()
			RETURN(TRUE)			
		ENDIF
	
	ELSE
		// if player died and we were midwarp then clean up.
		IF NOT (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState = 0)
		
			#IF IS_DEBUG_BUILD
				NET_PRINT_FRAME() NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - Player died mid-warp, cleaning up.") NET_NL()
			#ENDIF	
			
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			
			// clear spawn vehicle if as we could have been in the process of creating it.
			NET_PRINT("[spawning] WARP_TO_SPAWN_LOCATION - calling SetVehicleToDelete(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)") NET_NL()
			SetVehicleToDelete(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
			
			RESET_WARP_TO_SPAWN_LOCATION_GLOBALS()
			
		ENDIF
	ENDIF
	
	g_SpawnData.iWarpToLocationTime = GET_NETWORK_TIME()
	RETURN(FALSE)
ENDFUNC

///// PURPOSE:
/////   Give player weapon at start/respawn of activity
//PROC SET_STARTING_WEAPON_DEATHMATCH()
//
//	INT iAmmoToGive
//	INT starting_weapon_bullets
//	NET_NL() NET_PRINT("SET_PLAYER_ATTRIBUTES_AFTER_SPAWN FMMC_TYPE_DEATHMATCH  / FMMC_TYPE_SURVIVAL")
//	iAmmoToGive = (GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), GET_DEATHMATCH_RESPAWN_WEAPON()) * 2)
//	starting_weapon_bullets = GET_MP_INT_CHARACTER_STAT( GET_AMMO_CURRENT_FOR_WEAPON(GET_DEATHMATCH_RESPAWN_WEAPON()))
//		
//	//IF DOES_PLAYER_HAVE_WEAPON(GET_DEATHMATCH_RESPAWN_WEAPON(g_sDM_SB_CoronaOptions.iStartWeapon))
//	IF DOES_PLAYER_HAVE_WEAPON(GET_DEATHMATCH_RESPAWN_WEAPON())
//		IF starting_weapon_bullets <iAmmoToGive
//			iAmmoToGive -=starting_weapon_bullets
//			//ADD_AMMO_TO_PED(PLAYER_PED_ID(), GET_DEATHMATCH_RESPAWN_WEAPON(g_sDM_SB_CoronaOptions.iStartWeapon), iAmmoToGive)
//			ADD_AMMO_TO_PED(PLAYER_PED_ID(), GET_DEATHMATCH_RESPAWN_WEAPON(), iAmmoToGive)
//			NET_NL() NET_PRINT("SPAWNING GET_DEATHMATCH_RESPAWN_WEAPON 1")
//		ENDIF
//	ELSE
//	
//	//	GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GET_DEATHMATCH_RESPAWN_WEAPON(g_sDM_SB_CoronaOptions.iStartWeapon), iAmmoToGive)
//		GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GET_DEATHMATCH_RESPAWN_WEAPON(), iAmmoToGive)
//		
//		NET_NL() NET_PRINT("SPAWNING GET_DEATHMATCH_RESPAWN_WEAPON 2")
//		
//		IF GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_PISTOL
//		OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_APPISTOL
//	    OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_COMBATPISTOL
//		// Sub machine guns
//	    OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_MICROSMG 
//	    OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_SMG
//	    // Assault rifles
//	    OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_ASSAULTRIFLE 
//	    OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_CARBINERIFLE 
//	    OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_ADVANCEDRIFLE 
//		// Light machine guns
//		OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_MG
//		OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_COMBATMG
//		// Shotguns
//	    OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_PUMPSHOTGUN 
//	    OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_SAWNOFFSHOTGUN 
//	    OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_ASSAULTSHOTGUN 
//		// Sniper rifles
//	    OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_SNIPERRIFLE
//	    OR GET_DEATHMATCH_RESPAWN_WEAPON() =  WEAPONTYPE_HEAVYSNIPER 
//	    OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_REMOTESNIPER
//		// Heavy weapons
//	    OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_GRENADELAUNCHER
//	    OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_RPG 
//	    OR GET_DEATHMATCH_RESPAWN_WEAPON() =  WEAPONTYPE_MINIGUN 
//	    OR GET_DEATHMATCH_RESPAWN_WEAPON() = WEAPONTYPE_STINGER 
//			GIVE_WEAPON_EQUIPPED_TINT(GET_DEATHMATCH_RESPAWN_WEAPON())
//			GIVE_PLAYER_ALL_EQUIPPED_WEAPON_ADDONS_FOR_WEAPON(GET_DEATHMATCH_RESPAWN_WEAPON())
//		ENDIF
//	ENDIF
//	
//	//SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_DEATHMATCH_RESPAWN_WEAPON(g_sDM_SB_CoronaOptions.iStartWeapon), TRUE)
//	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_DEATHMATCH_RESPAWN_WEAPON(), TRUE)
//	PUT_WEAPON_IN_HAND(WEAPONINHAND_LASTWEAPON_BOTH)
//	NET_NL() NET_PRINT("SPAWNING GIVE PLAYER FMMC_TYPE_SURVIVAL Weapon AMMO ") NET_PRINT_INT(iAmmoToGive) NET_NL()
//		
//ENDPROC

//PROC ADD_AMMO_AND_EQUIP_WEAPON(WEAPON_TYPE wtWeapon)
//	INT iAmmoToGive
//	INT iCurrentAmmo
//	BOOL bForcedWeaponJob
//	IF IS_JOB_FORCED_WEAPON_ONLY()
//	OR IS_JOB_FORCED_WEAPON_PLUS_PICKUPS()
//		bForcedWeaponJob = TRUE
//	ENDIF
//	IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), wtWeapon)
//		IF bForcedWeaponJob
//			// Give two clips
//			iAmmoToGive = (GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), wtWeapon) * 2)
//			// Overwrite if two clips unsuitable
//			IF GIVE_SPECIAL_AMMO_AMOUNT(wtWeapon) 				
//				iAmmoToGive = GET_AMMO_AMOUNT_FOR_MP_PICKUP(wtWeapon)
//			ENDIF
//			
//			iCurrentAmmo = GET_MP_INT_CHARACTER_STAT(GET_AMMO_CURRENT_FOR_WEAPON(wtWeapon))
//			PRINTLN("[CS_WEAPON] ADD_AMMO_AND_EQUIP_WEAPON, before, iAmmoToGive =  ", iAmmoToGive, "  iCurrentAmmo = ", iCurrentAmmo)
//			IF (iCurrentAmmo < iAmmoToGive)
//				iAmmoToGive = (iAmmoToGive - iCurrentAmmo)
//				PRINTLN("[CS_WEAPON] IF NOT bInitial iAmmoToGive = ", iAmmoToGive)
//				ADD_AMMO_TO_PED(PLAYER_PED_ID(), wtWeapon, iAmmoToGive)
//				PRINTLN("[CS_WEAPON]ADD_AMMO_AND_EQUIP_WEAPON ADDING AMMO: ", iAmmoToGive)
//			ENDIF
//			PRINTLN("[CS_WEAPON] ADD_AMMO_AND_EQUIP_WEAPON, after, iAmmoToGive =  ", iAmmoToGive, "  iCurrentAmmo = ", iCurrentAmmo)
//		ENDIF
//		
//		// Failsafe
//		INT iWeaponAmmo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), wtWeapon)
//		PRINTLN("[CS_WEAPON] ADD_AMMO_AND_EQUIP_WEAPON, GET_AMMO_IN_PED_WEAPON = ", iWeaponAmmo)
//		
//		// Give two clips
//		iAmmoToGive = (GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), wtWeapon) * 2)
//		// Overwrite if two clips unsuitable
//		IF GIVE_SPECIAL_AMMO_AMOUNT(wtWeapon) 				
//			iAmmoToGive = GET_AMMO_AMOUNT_FOR_MP_PICKUP(wtWeapon)
//		ENDIF
//		
//		BOOL bSetAmmo
//		IF bForcedWeaponJob
//			IF wtWeapon = GET_DEATHMATCH_RESPAWN_WEAPON()
//			OR (iWeaponAmmo < iAmmoToGive)
//				bSetAmmo = TRUE
//			ENDIF
//		ENDIF
//		
//		IF iWeaponAmmo = 0
//		OR bSetAmmo
//			SET_PED_AMMO(PLAYER_PED_ID(), wtWeapon, iAmmoToGive)
//			PRINTLN("[CS_WEAPON] ADD_AMMO_AND_EQUIP_WEAPON, SET_PED_AMMO: ", iAmmoToGive)
//		ENDIF
//		
//		// Equip
//		GIVE_WEAPON_EQUIPPED_TINT(wtWeapon)
//		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWeapon, TRUE)
//	ENDIF
//ENDPROC

FUNC BOOL IS_THROWN_WEAPON(WEAPON_TYPE wt)
	SWITCH wt
		CASE WEAPONTYPE_GRENADE 
	  	CASE WEAPONTYPE_SMOKEGRENADE
		CASE WEAPONTYPE_BZGAS 
	    CASE WEAPONTYPE_STICKYBOMB 
	    CASE WEAPONTYPE_MOLOTOV
	    CASE WEAPONTYPE_BALL  
	    CASE WEAPONTYPE_FLARE 
		CASE WEAPONTYPE_DLC_PROXMINE 
		
			RETURN TRUE
		BREAK		
	ENDSWITCH

	RETURN FALSE
ENDFUNC

FUNC INT GET_AMMO_FOR_WEAPON(WEAPON_TYPE wtWeapon, INT iClipsToGive = 4)
	INT iAmmoToGive = 0
	
	IF IS_WEAPON_VALID(wtWeapon)
		// Give two clips
		iAmmoToGive = (GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), wtWeapon) * iClipsToGive)
		// Overwrite if two clips unsuitable
		IF GIVE_SPECIAL_AMMO_AMOUNT(wtWeapon) 				
			iAmmoToGive = GET_AMMO_AMOUNT_FOR_MP_PICKUP(wtWeapon)
		ENDIF
	ENDIF
	
	RETURN iAmmoToGive
ENDFUNC

//PROC GIVE_PLAYER_LAST_WEAPON_USED()
//	PRINTNL()
//	PRINTLN("[CS_WEAPON] GIVE_PLAYER_LAST_WEAPON_USED_START_______________________________________________")
//	PRINTNL()
//	
//	// First try and grab last weapon used
//	WEAPON_TYPE wtWeaponToGive
//	
//	BOOL bForcedWeapon
//	IF NOT IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS()
//		bForcedWeapon = TRUE
//	ENDIF
//	
//	// Give ammo
//	IF bForcedWeapon = TRUE
//		wtWeaponToGive = GET_DEATHMATCH_RESPAWN_WEAPON()
//		PRINTLN("[CS_WEAPON] bForcedWeapon weapn locked to  ", GET_WEAPON_NAME(wtWeaponToGive))
//		IF NOT DOES_PLAYER_HAVE_WEAPON(wtWeaponToGive)
//			PRINTLN("[CS_WEAPON] Does not have a  ", GET_WEAPON_NAME(wtWeaponToGive))
//			INT iAmmoToGive = GET_AMMO_FOR_WEAPON(wtWeaponToGive)
//			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), wtWeaponToGive, iAmmoToGive)
//			PRINTLN("[CS_WEAPON] GIVE_WEAPON_TO_PED  ", GET_WEAPON_NAME(wtWeaponToGive))
//		ENDIF
//	ELSE
//		wtWeaponToGive = GET_PLAYER_CURRENT_HELD_WEAPON()
//		PRINTLN("[CS_WEAPON] Player's last weapon was a  ", GET_WEAPON_NAME(wtWeaponToGive))
//		IF IS_THROWN_WEAPON(wtWeaponToGive) // 1641858
//			wtWeaponToGive = WEAPONTYPE_PISTOL
//		ENDIF
//		IF NOT DOES_PLAYER_HAVE_WEAPON(wtWeaponToGive)
//			// Or else go with starting weapon
//			PRINTLN("[CS_WEAPON] Player does not have a ", GET_WEAPON_NAME(wtWeaponToGive))
//			wtWeaponToGive = GET_DEATHMATCH_RESPAWN_WEAPON()
//			PRINTLN("[CS_WEAPON] Try to give starting weapon instead ", GET_WEAPON_NAME(wtWeaponToGive))
//			IF NOT DOES_PLAYER_HAVE_WEAPON(wtWeaponToGive)
//				PRINTLN("[CS_WEAPON] Does not have a  ", GET_WEAPON_NAME(wtWeaponToGive))
//				// Pistol if all else fails
//				wtWeaponToGive = WEAPONTYPE_PISTOL
//				IF NOT DOES_PLAYER_HAVE_WEAPON(wtWeaponToGive)
//					INT iAmmoToGive = GET_AMMO_FOR_WEAPON(wtWeaponToGive)
//					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), wtWeaponToGive, iAmmoToGive)
//					PRINTLN("[CS_WEAPON] GIVE_PLAYER_PISTOL ")
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	PRINTLN("[CS_WEAPON] WEAPON_GIVING_AMMO_TO IS  ", GET_WEAPON_NAME(wtWeaponToGive))
//	
//	REFILL_AMMO_INSTANTLY(PLAYER_PED_ID())
//	GIVE_WEAPON_EQUIPPED_TINT(wtWeaponToGive)
//	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWeaponToGive, TRUE)
//
//	PRINTNL()
//	PRINTLN("[CS_WEAPON] GIVE_PLAYER_LAST_WEAPON_USED_END_______________________________________________")
//	PRINTNL()
//ENDPROC

// Give the player ammo if 0 or below the set amount for the forced weapon chosen
PROC AMMO_FAILSAFE(WEAPON_TYPE& wtWeapon, BOOL bForcedWeaponJob)

	// Failsafe
	INT iWeaponAmmo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), wtWeapon)
	PRINTLN("[FORCE_WEAPON] ADD_AMMO_AND_EQUIP_WEAPON, GET_AMMO_IN_PED_WEAPON = ", iWeaponAmmo)
	
	INT iAmmoToGive = GET_AMMO_FOR_WEAPON(wtWeapon)
	INT iForceWeaponOverride = -1
	IF IS_TEAM_DEATHMATCH()
		iForceWeaponOverride = g_FMMC_STRUCT.iTeamForcedWeapon[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen]
	ENDIF
	
	BOOL bSetAmmo
	
	IF bForcedWeaponJob
		IF wtWeapon = GET_DEATHMATCH_RESPAWN_WEAPON(iForceWeaponOverride)
		OR (iWeaponAmmo < iAmmoToGive)
			bSetAmmo = TRUE
		ENDIF
	ENDIF
	
	IF iWeaponAmmo = 0
	OR bSetAmmo
		SET_PED_AMMO(PLAYER_PED_ID(), wtWeapon, iAmmoToGive)
		PRINTLN("[FORCE_WEAPON] ADD_AMMO_AND_EQUIP_WEAPON, SET_PED_AMMO: ", iAmmoToGive)
	ENDIF
 ENDPROC

FUNC BOOL SHOULD_PLAYER_RETAIN_EXACT_AMMO_ON_RESPAWN()

	IF NETWORK_IS_ACTIVITY_SESSION()
		IF IS_CONTENT_IN_OR_AFTER_DLC(ciDLC_CASINO_HEIST)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DEAL_WITH_FORCED_WEAPON_AND_AMMO(BOOL bInitialSpawn, INT& iWeaponBitSet, WEAPON_TYPE& wtWeaponToRemove, BOOL bStartingInventory = FALSE)


	PRINTNL()
	PRINTLN("[FORCE_WEAPON]  DEAL_WITH_FORCED_WEAPON_AND_AMMO - START_______________________________ ")
	PRINTNL()
	
	WEAPON_TYPE wtWeaponToGive

	BOOL bForcedWeapon
	
	IF NOT Is_Player_Currently_On_MP_Heist(PLAYER_ID())
	AND NOT Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()) 
		IF NOT IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS()
			bForcedWeapon = TRUE
		ENDIF
	ENDIF
	
	
	PRINTLN("[RCC MISSION][FORCE_WEAPON] DEAL_WITH_FORCED_WEAPON_AND_AMMO bInitialSpawn = ", bInitialSpawn, " bStartingInventory = ", bStartingInventory, " bForcedWeapon = ", bForcedWeapon)
	
	// Check for new DLC weapons as we need to remove any that were dished out
	IF bInitialSpawn
		IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			IF NOT IS_PLAYER_ON_IMPROMPTU_DM()
			AND NOT IS_PLAYER_ON_BOSSVBOSS_DM() 
				DEAL_WITH_DLC_WEAPONS(iWeaponBitSet, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	// Special case for Missions
	IF bStartingInventory
	OR (IS_KING_OF_THE_HILL() AND NOT bInitialSpawn AND NOT CONTENT_IS_USING_ARENA())
		
		IF NOT bInitialSpawn OR NOT bForcedWeapon
			PRINTLN("[RCC MISSION][FORCE_WEAPON]  DEAL_WITH_FORCED_WEAPON_AND_AMMO - iStartingInventory != 0, giving starting mission weapons...")
			
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciMISSION_OVERRIDE_AMMUNATION_FORCE_CLOSE)
				g_bMissionInventory = TRUE
			ENDIF
			
			IF IS_CHECKPOINT_ANIM_GLOBAL_SET()
				GIVE_PLAYER_STARTING_MISSION_WEAPONS(WEAPONINHAND_FIRSTSPAWN_HOLSTERED, DEFAULT, DEFAULT, bForcedWeapon, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciWEAPONS_FORCE_NO_ADDONS_START_MISSION_INVENTORY))
				PRINTLN("[RCC MISSION][FORCE_WEAPON] NOT GIVE_PLAYER_STARTING_MISSION_WEAPONS, SHOULD_USE_CHECKPOINT_ANIMS ")
			ELSE
				IF bInitialSpawn
				AND IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
					PRINTLN("DEAL_WITH_FORCED_WEAPON_AND_AMMO - Initial Spawn = TRUE ")
					PRINTLN("[RCC MISSION][FORCE_WEAPON] Initial spawn and n a heist - WEAPONINHAND_FIRSTSPAWN_HOLSTERED")
					GIVE_PLAYER_STARTING_MISSION_WEAPONS(WEAPONINHAND_FIRSTSPAWN_HOLSTERED, DEFAULT, DEFAULT, bForcedWeapon, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciWEAPONS_FORCE_NO_ADDONS_START_MISSION_INVENTORY))
				ELSE
					PRINTLN("DEAL_WITH_FORCED_WEAPON_AND_AMMO - Initial Spawn = FALSE ")
					PRINTLN("[RCC MISSION][FORCE_WEAPON]  Not on a heist  / not bInitialSpawn - WEAPONINHAND_LASTWEAPON_BOTH")
				
					// commented out. We do not want to give weapons to player when they die and respawn KW 10/2/2015
					IF bInitialSpawn
						GIVE_PLAYER_STARTING_MISSION_WEAPONS(WEAPONINHAND_LASTWEAPON_BOTH, DEFAULT, DEFAULT, bForcedWeapon, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciWEAPONS_FORCE_NO_ADDONS_START_MISSION_INVENTORY))
						PRINTLN("[RCC MISSION][FORCE_WEAPON]  bInitialSpawn = TRUE - WEAPONINHAND_LASTWEAPON_BOTH")
					ENDIF
					
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRESPAWN_WITH_LAST_WEAPON)
						PRINTLN("[RCC MISSION][FORCE_WEAPON] ciRESPAWN_WITH_LAST_WEAPON")
						IF IS_BIT_SET(g_FMMC_STRUCT.iRuleOptionsBitSet, ciGIVE_WEAPONS_ON_RESPAWN) 
							GIVE_PLAYER_STARTING_MISSION_WEAPONS(WEAPONINHAND_LASTWEAPON_BOTH, FALSE, DEFAULT, bForcedWeapon, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciWEAPONS_FORCE_NO_ADDONS_START_MISSION_INVENTORY))
						ENDIF
						
						WEAPON_TYPE wtWeapon
						
						#IF IS_DEBUG_BUILD
						INT iDb = ENUM_TO_INT(gweapon_type_CurrentlyHeldWeapon)
						PRINTLN("[RCC MISSION][FORCE_WEAPON] gweapon_type_CurrentlyHeldWeapon = ", iDb)
						#ENDIF
						
						// In King of the Hill, give the player their weapon so that they can spawn holding what they were holding before they died
						IF IS_KING_OF_THE_HILL()
						AND NOT bInitialSpawn
						AND IS_WEAPON_VALID(GET_PLAYER_CURRENT_HELD_WEAPON())
						AND GET_PLAYER_CURRENT_HELD_WEAPON() != WEAPONTYPE_UNARMED
							IF NOT g_bMissionEnding
							AND IS_SKYSWOOP_AT_GROUND()
								INT iAmmo = 0 //g_iCurrentlyHeldWeapon_Ammo //GET_AMMO_FOR_WEAPON(GET_PLAYER_CURRENT_HELD_WEAPON(), 1) //GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), GET_PLAYER_CURRENT_HELD_WEAPON())
								PRINTLN("[KOTH][FORCE_WEAPON] Giving gweapon_type_CurrentlyHeldWeapon to player! gweapon_type_CurrentlyHeldWeapon: ", GET_WEAPON_NAME(gweapon_type_CurrentlyHeldWeapon), " || Ammo: ", iAmmo)
								GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GET_PLAYER_CURRENT_HELD_WEAPON(), iAmmo, TRUE)
								SET_PED_AMMO(PLAYER_PED_ID(), GET_PLAYER_CURRENT_HELD_WEAPON(), g_iCurrentlyHeldWeapon_Ammo)
								PRINTLN("[KOTH][FORCE_WEAPON] Setting ammo to ", g_iCurrentlyHeldWeapon_Ammo)
							ELSE
								PRINTLN("[KOTH][FORCE_WEAPON] Not giving gweapon_type_CurrentlyHeldWeapon to player because the mission is ending or skyswoop is not at the ground")
							ENDIF
						ENDIF
						
						IF DOES_PLAYER_HAVE_WEAPON(GET_PLAYER_CURRENT_HELD_WEAPON())
							wtWeapon = GET_PLAYER_CURRENT_HELD_WEAPON()
							IF wtWeapon != WEAPONTYPE_INVALID
								PRINTLN("[RCC MISSION][FORCE_WEAPON] wtWeapon = VALID | Calling SET_CURRENT_PED_WEAPON for ", ENUM_TO_INT(wtWeapon))
								SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWeapon, TRUE)
							ELIF wtWeapon = WEAPONTYPE_UNARMED
								PRINTLN("[RCC MISSION][FORCE_WEAPON] wtWeapon = WT_UNARMED")
								wtWeapon = GET_MISSION_STARTING_WEAPON(g_FMMC_STRUCT.sFMMCEndConditions[g_i_Mission_team].iStartingInventoryStartWep)
								
								IF wtWeapon = WEAPONTYPE_UNARMED //no start weapon set
									PRINTLN("[RCC MISSION][FORCE_WEAPON] wtWeapon = WT_UNARMED - no start weapon set")
									wtWeapon = GET_BEST_PED_WEAPON(PLAYER_PED_ID(), FALSE)
								ENDIF
								
								IF wtWeapon != WEAPONTYPE_UNARMED
									PRINTLN("[RCC MISSION][FORCE_WEAPON] wtWeapon = WT_UNARMED - ped has a good weapon")
									SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWeapon, TRUE)
									SET_PLAYER_CURRENT_HELD_WEAPON(wtWeapon)
									
								ENDIF
							ELSE
								PRINTLN("[RCC MISSION][FORCE_WEAPON] wtWeapon = INVALID")
							ENDIF
						ELSE
							PRINTLN("[RCC MISSION][FORCE_WEAPON] NOT DOES_PLAYER_HAVE_WEAPON")
						ENDIF
						
					ELSE
						IF IS_BIT_SET(g_FMMC_STRUCT.iRuleOptionsBitSet, ciGIVE_WEAPONS_ON_RESPAWN) 
							GIVE_PLAYER_STARTING_MISSION_WEAPONS(WEAPONINHAND_LASTWEAPON_BOTH, DEFAULT, DEFAULT, bForcedWeapon, DEFAULT, IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetNine, ciWEAPONS_FORCE_NO_ADDONS_START_MISSION_INVENTORY)) 
							PRINTLN("[RCC MISSION][FORCE_WEAPON]  (g_FMMC_STRUCT.iRuleOptionsBitSet, ciGIVE_WEAPONS_ON_RESPAWN) = TRUE - WEAPONINHAND_LASTWEAPON_BOTH")
						ENDIF	
					ENDIF		
					
					PUT_WEAPON_IN_HAND(WEAPONINHAND_LASTWEAPON_BOTH)
				ENDIF
			ENDIF
			
			EXIT
		ENDIF
	ENDIF
	
	// Remove all weapons for initial spawn
	IF bInitialSpawn
	OR (CONTENT_IS_USING_ARENA() AND NOT IS_THIS_MISSION_ROCKSTAR_CREATED())
		IF NOT IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS()				
		OR (CONTENT_IS_USING_ARENA() AND NOT IS_THIS_MISSION_ROCKSTAR_CREATED())
			REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
			PRINTLN("[FORCE_WEAPON]  DEAL_WITH_FORCED_WEAPON_AND_AMMO - IS_JOB_FORCED_WEAPON_ONLY, REMOVE_ALL_PED_WEAPONS ")
			SET_BLOCK_AMMO_GLOBAL()
		ENDIF
	ENDIF
	
	// Try and get starting weapon
	INT iForceWeaponOverride = -1
	IF IS_TEAM_DEATHMATCH()
		iForceWeaponOverride = g_FMMC_STRUCT.iTeamForcedWeapon[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen]
	ENDIF
	wtWeaponToGive = GET_DEATHMATCH_RESPAWN_WEAPON(iForceWeaponOverride)
	PRINTLN("[FORCE_WEAPON]  DEAL_WITH_FORCED_WEAPON_AND_AMMO - GET_DEATHMATCH_RESPAWN_WEAPON - ",  GET_WEAPON_NAME(INT_TO_ENUM(WEAPON_TYPE, wtWeaponToGive)), " Weapon Override: ",iForceWeaponOverride)
	
	// Backup pistol
	IF wtWeaponToGive = WEAPONTYPE_UNARMED
	AND NOT IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS()
	AND NOT bInitialSpawn
		wtWeaponToGive = WEAPONTYPE_PISTOL
		PRINTLN("[FORCE_WEAPON]  DEAL_WITH_FORCED_WEAPON_AND_AMMO - Giving player backup pistol: ",  GET_WEAPON_NAME(INT_TO_ENUM(WEAPON_TYPE, wtWeaponToGive)))
	ENDIF
	
	IF (CONTENT_IS_USING_ARENA() AND NOT IS_THIS_MISSION_ROCKSTAR_CREATED())
		wtWeaponToGive = WEAPONTYPE_UNARMED
		PRINTLN("[FORCE_WEAPON] [ARENA] - Forcing Unarmed ")
		EXIT
	ENDIF
	
//	IF NOT bStartingInventory
//	AND Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())
//	AND NOT IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS()
//		wtWeaponToGive = WEAPONTYPE_UNARMED
//		PRINTLN("[FORCE_WEAPON]  DEAL_WITH_FORCED_WEAPON_AND_AMMO - Setting player as unarmed url:bugstar:2405836")
//	ENDIF
	
	INT iAmmoToGive = 0
	
	// Give weapon
	IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
	AND IS_WEAPON_VALID(wtWeaponToGive)
		IF NOT IS_MP_WEAPON_EQUIPPED(wtWeaponToGive)
		OR NOT IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS() 
		
			BOOL bInhand = FALSE
			IF bForcedWeapon
			AND wtWeaponToGive <> WEAPONTYPE_PETROLCAN
				bInhand = TRUE
			ENDIF
			
			IF IS_WEAPON_VALID(wtWeaponToGive)
				IF bInitialSpawn
					IF HAS_CORONA_WEAPON_GOT_GUNRUNNING_AMMO_TYPE(wtWeaponToGive)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWeaponToGive, bInhand)
						PRINTLN("[FORCE_WEAPON] bInitialSpawn, SET_CURRENT_PED_WEAPON  ", GET_WEAPON_NAME(wtWeaponToGive))
					ELSE
						iAmmoToGive = GET_AMMO_FOR_WEAPON(wtWeaponToGive)
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), wtWeaponToGive, iAmmoToGive, bInhand)
					ENDIF
				ENDIF
			ENDIF
			
			IF bInitialSpawn
				IF NOT IS_MP_WEAPON_EQUIPPED(wtWeaponToGive)
				AND wtWeaponToGive <> WEAPONTYPE_UNARMED
					wtWeaponToRemove = wtWeaponToGive
					PRINTLN("[FORCE_WEAPON]  DEAL_WITH_FORCED_WEAPON_AND_AMMO - PLAYER_DIDNT_HAVE_WEAPON_GIVING ",  GET_WEAPON_NAME(INT_TO_ENUM(WEAPON_TYPE, wtWeaponToGive)))
				ENDIF
			ELSE
				IF bForcedWeapon = TRUE
				AND NOT g_bMissionEnding
				
					wtWeaponToGive = GET_DEATHMATCH_RESPAWN_WEAPON()
					PRINTLN("[FORCE_WEAPON] bForcedWeapon weapon locked to  ", GET_WEAPON_NAME(wtWeaponToGive))
					
					IF IS_WEAPON_VALID(wtWeaponToGive)
						IF NOT DOES_PLAYER_HAVE_WEAPON(wtWeaponToGive)
							PRINTLN("[FORCE_WEAPON] Does not have a  ", GET_WEAPON_NAME(wtWeaponToGive))
							
							IF IS_WEAPON_VALID(wtWeaponToGive)
								IF HAS_CORONA_WEAPON_GOT_GUNRUNNING_AMMO_TYPE(wtWeaponToGive)
									SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWeaponToGive)
									PRINTLN("[FORCE_WEAPON] SET_CURRENT_PED_WEAPON  ", GET_WEAPON_NAME(wtWeaponToGive))
								ELSE
									iAmmoToGive = GET_AMMO_FOR_WEAPON(wtWeaponToGive)
									GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), wtWeaponToGive, iAmmoToGive)
									PRINTLN("[FORCE_WEAPON] GIVE_WEAPON_TO_PED  ", GET_WEAPON_NAME(wtWeaponToGive))
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF g_bMissionEnding
						PRINTLN("[FORCE_WEAPON] Not doing GIVE_WEAPON_TO_PED due to g_bMissionEnding")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// For non forced weapon modes
	IF NOT bInitialSpawn
	AND IS_JOB_OWNED_WEAPONS_PLUS_PICKUPS()
		wtWeaponToGive = GET_PLAYER_CURRENT_HELD_WEAPON()
		PRINTLN("[FORCE_WEAPON] Player's last weapon was a  ", GET_WEAPON_NAME(wtWeaponToGive))
		IF IS_THROWN_WEAPON(wtWeaponToGive) // 1641858
		OR wtWeaponToGive = WEAPONTYPE_INVALID
			wtWeaponToGive = WEAPONTYPE_PISTOL
		ENDIF
		IF NOT DOES_PLAYER_HAVE_WEAPON(wtWeaponToGive)
			PRINTLN("[FORCE_WEAPON] Player does not have a ", GET_WEAPON_NAME(wtWeaponToGive))
			// Pistol if all else fails
			wtWeaponToGive = WEAPONTYPE_PISTOL
			iAmmoToGive = GET_AMMO_FOR_WEAPON(wtWeaponToGive)
			
			IF IS_WEAPON_VALID(wtWeaponToGive)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), wtWeaponToGive, iAmmoToGive)
			ENDIF
			PRINTLN("[FORCE_WEAPON] GIVE_PLAYER_PISTOL ")
		ENDIF
	ENDIF
	
	IF wtWeaponToGive != WEAPONTYPE_INVALID
		// Finish up and put weapon in hand
		AMMO_FAILSAFE(wtWeaponToGive, bForcedWeapon)
		REFILL_AMMO_INSTANTLY(PLAYER_PED_ID())
		GIVE_WEAPON_EQUIPPED_TINT(wtWeaponToGive)
	ENDIF

	IF NOT bForcedWeapon
		IF g_SpawnData.MyLastWeapon != WEAPONTYPE_INVALID
			GIVE_PLAYER_ALL_EQUIPPED_WEAPON_ADDONS_FOR_WEAPON(g_SpawnData.MyLastWeapon)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciDISABLE_ARMING) 
		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
	ELSE
		IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciRESPAWN_WITH_LAST_WEAPON)
		AND NOT (IS_KING_OF_THE_HILL() AND bInitialSpawn)
			IF GET_PLAYER_CURRENT_HELD_WEAPON() = WEAPONTYPE_UNARMED
			AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFour, ciRESPAWN_WITH_BEST_WEAPON)
				IF GET_BEST_PED_WEAPON(PLAYER_PED_ID(), FALSE) != WEAPONTYPE_INVALID
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_BEST_PED_WEAPON(PLAYER_PED_ID(), FALSE), TRUE)
					PRINTLN("[FORCE_WEAPON] SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_BEST_PED_WEAPON(PLAYER_PED_ID(), FALSE), TRUE)")
				ENDIF
			ELSE
				PRINTLN("[FORCE_WEAPON] SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_PLAYER_CURRENT_HELD_WEAPON(), TRUE)")
				IF DOES_PLAYER_HAVE_WEAPON(GET_PLAYER_CURRENT_HELD_WEAPON())
					IF IS_WEAPON_VALID(GET_PLAYER_CURRENT_HELD_WEAPON())
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), GET_PLAYER_CURRENT_HELD_WEAPON(), TRUE)
					ENDIF
				ELSE
					PRINTLN("[FORCE_WEAPON] Error: Player does not have this weapon! ", GET_PLAYER_CURRENT_HELD_WEAPON())
				ENDIF
			ENDIF
		ELSE
			IF IS_WEAPON_VALID(wtWeaponToGive)
				IF DOES_PLAYER_HAVE_WEAPON(wtWeaponToGive)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtWeaponToGive, TRUE)
				ELSE
					PRINTLN("[FORCE_WEAPON] DOES_PLAYER_HAVE_WEAPON(wtWeaponToGive) FALSE ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF SHOULD_PLAYER_RETAIN_EXACT_AMMO_ON_RESPAWN()
		WEAPON_TYPE wtPlayerWeapon
		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtPlayerWeapon, FALSE)
		
		IF IS_WEAPON_VALID(wtPlayerWeapon)
		AND wtPlayerWeapon != WEAPONTYPE_INVALID
		AND NOT IS_WEAPON_A_MELEE_WEAPON(wtPlayerWeapon)
			INT iCurrentAmmo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), wtPlayerWeapon)
			INT iClipAmmoToGive = GET_WEAPON_CLIP_SIZE(wtPlayerWeapon)
			
			IF iClipAmmoToGive > 0
				PRINTLN("[FORCE_WEAPON] Giving player extra ammo (", iClipAmmoToGive, ") on top of their current ammo (", iCurrentAmmo, ") to account for the clip of ammo they're about to lose")
				SET_PED_AMMO(PLAYER_PED_ID(), wtPlayerWeapon, iCurrentAmmo + iClipAmmoToGive, TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	PRINTNL()
	PRINTLN("[FORCE_WEAPON]  DEAL_WITH_FORCED_WEAPON_AND_AMMO - END_______________________________ ")
	PRINTNL()
ENDPROC

FUNC BOOL DOES_VEHICLE_USE_FOLDING_WINGS(VEHICLE_INDEX viVeh)
	IF DOES_ENTITY_EXIST(viVeh)
		IF GET_ENTITY_MODEL(viVeh) = AKULA
		OR GET_ENTITY_MODEL(viVeh) = ANNIHILATOR2
			PRINTLN("DOES_VEHICLE_USE_FOLDING_WINGS - Returning TRUE")
			RETURN TRUE
		ELSE
			PRINTLN("DOES_VEHICLE_USE_FOLDING_WINGS - Returning FALSE - Player's current vehicle wasn't in the vehicle list in DOES_VEHICLE_USE_FOLDING_WINGS")
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the player's vehicle to be in the Stealth Mode state it was before the player respawned if it uses folding wings
PROC HANDLE_VEH_STEALTH_SPAWNING(BOOL bPutIntoStealthMode)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	
		VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())

		IF DOES_VEHICLE_USE_FOLDING_WINGS(viVeh)
			//B* 4137773 Tell stealth script that it isn't allowed to force the stealth mode (it usually does during startup/cleanup)
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iBSVehStealthMode, GLOBAL_BS_VEH_STEALTH_DISABLE_SCRIPT_FORCE_STEALTH_THIS_FRAME)
			PRINTLN("[SPAWNING] Setting new vehicle's stealth mode to ", bPutIntoStealthMode)
			
			IF bPutIntoStealthMode
				//In Stealth Mode, hide the wings
				IF ARE_FOLDING_WINGS_DEPLOYED(viVeh)
					//B* 4137773 Changed arg bInstant to FALSE because TRUE is just broken.
					SET_DEPLOY_FOLDING_WINGS(viVeh, FALSE, FALSE)
					PRINTLN("[SPAWNING] Hiding folding wings now!")
				ELSE
					PRINTLN("[TMS] Folding wings are already hidden!")
				ENDIF
			ELSE
				//In Non-stealth Mode, deploy the folding wings
				IF NOT ARE_FOLDING_WINGS_DEPLOYED(viVeh)
					SET_DEPLOY_FOLDING_WINGS(viVeh, TRUE, FALSE)
					PRINTLN("[SPAWNING] Deploying folding wings now!")
				ELSE
					PRINTLN("[TMS] Folding wings are already deployed!")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Set up any appearances changes from default that a player should have when they spawn
PROC SET_UP_PLAYER_DECORATION()
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
		EXIT
	ENDIF
	
	// switch bag off by default
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0) 
	
	SWITCH g_SpawnData.eDecoration
	
		CASE SPDEC_DUFFELBAG	
			// Holdall used by some heist missions (like in 3lc / Heat)
			#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] SET_UP_PLAYER_DECORATION duffelbag \n") 
			#ENDIF
			
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 1, 0) 
		BREAK
		
		
		CASE SPDEC_PARLEY_SNIPER
			
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER, 20, TRUE)
			
		BREAK
		
	ENDSWITCH
ENDPROC




/// PURPOSE:
///    Apply any tattoos or patches that the player has acquired
PROC SET_MP_PLAYER_TATTOOS_AND_PATCHES(BOOL bClearDecorations = TRUE)
	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] SET_MP_PLAYER_TATTOOS_AND_PATCHES() - called...") NET_NL()
	#ENDIF

	IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, FALSE)
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] SET_MP_PLAYER_TATTOOS_AND_PATCHES() - NOT IS_NET_PLAYER_OK - EXIT ") NET_NL()
		#ENDIF
	//IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE) //fix 1333416
		EXIT
	ENDIF
	
	IF bClearDecorations	
		#IF NOT USE_TU_CHANGES
		CLEAR_PED_DECORATIONS(PLAYER_PED_ID())
		#ENDIF
		#IF USE_TU_CHANGES
		CLEAR_PED_DECORATIONS_LEAVE_SCARS(PLAYER_PED_ID())
		#ENDIF
	ENDIF
	
	TATTOO_DATA_STRUCT sTattooData
	TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
	
	#IF USE_TU_CHANGES
		BOOL bBlockChestTattoos = SHOULD_CHEST_TATTOOS_BE_BLOCKED_FOR_OUTFIT(PLAYER_PED_ID())
		BOOL bBlockCrewEmblems = SHOULD_CREW_EMBLEMS_BE_BLOCKED_FOR_OUTFIT(PLAYER_PED_ID())
		BOOL bBlockBackTattoos = SHOULD_BACK_TATTOOS_BE_BLOCKED_FOR_OUTFIT(PLAYER_PED_ID())
		BOOL bBlockTorsoDecals = SHOULD_TORSO_DECALS_BE_BLOCKED_FOR_OUTFIT(PLAYER_PED_ID())
	#ENDIF
	
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTattooDebug")
			PRINTLN("sc_ExtraTattooDebug: SET_MP_PLAYER_TATTOOS_AND_PATCHES: bBlockCrewEmblems=", bBlockCrewEmblems, ", bBlockBackTattoos=", bBlockBackTattoos, ", bBlockChestTattoos=", bBlockChestTattoos, ", bBlockTorsoDecals=", bBlockTorsoDecals)
			DEBUG_PRINTCALLSTACK()
		ENDIF
	#ENDIF
	
	INT i
	REPEAT MAX_NUMBER_OF_TATTOOS i
		IF GET_TATTOO_DATA(sTattooData, INT_TO_ENUM(TATTOO_NAME_ENUM, i), eFaction, PLAYER_PED_ID())
			IF IS_MP_TATTOO_CURRENT(INT_TO_ENUM(TATTOO_NAME_ENUM, i))
				#IF USE_TU_CHANGES
				IF IS_TATTOO_SAFE_TO_APPLY(PLAYER_PED_ID(), sTattooData.sLabel, INT_TO_ENUM(TATTOO_NAME_ENUM, i), sTattooData.iCollection, sTattooData.iUpgradeGroup, bBlockChestTattoos, bBlockCrewEmblems, bBlockTorsoDecals, bBlockBackTattoos)
				#ENDIF
				
				ADD_PED_DECORATION_FROM_HASHES(PLAYER_PED_ID(), sTattooData.iCollection, sTattooData.iPreset)
				
				#IF USE_TU_CHANGES
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF USE_TU_CHANGES
	
		// Apply fuzz overlay that was added for Beach Bum
		IF IS_MP_TATTOO_CURRENT(TATTOO_MP_FM_HO_FUZZ)
			IF GET_PED_DECORATION_ZONE_FROM_HASHES(HASH("mpBeach_overlays"), HASH("FM_Hair_Fuzz")) != PDZ_INVALID
				ADD_PED_DECORATION_FROM_HASHES(PLAYER_PED_ID(), HASH("mpBeach_overlays"), HASH("FM_Hair_Fuzz"))
			ENDIF
		ENDIF
		
		// Apply the DLC tats.
		INT iDLCIndex
		INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
		sTattooShopItemValues sDLCTattooData
		TATTOO_NAME_ENUM eDLCTattoo
		
		REPEAT iDLCCount iDLCIndex
			IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
				IF NOT IS_CONTENT_ITEM_LOCKED(sDLCTattooData.m_lockHash)
					eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex)
					IF IS_MP_TATTOO_CURRENT(eDLCTattoo)
						IF IS_TATTOO_SAFE_TO_APPLY(PLAYER_PED_ID(), sDLCTattooData.Label, eDLCTattoo, sDLCTattooData.Collection, sDLCTattooData.UpdateGroup, bBlockChestTattoos, bBlockCrewEmblems, bBlockTorsoDecals, bBlockBackTattoos)
							ADD_PED_DECORATION_FROM_HASHES(PLAYER_PED_ID(), sDLCTattooData.Collection, sDLCTattooData.Preset)
							ADD_SECONDARY_PED_DECORATION_FROM_HASHES(PLAYER_PED_ID(), sDLCTattooData.Collection, sDLCTattooData.Preset)
						ENDIF
					ENDIF					
				ENDIF
			ENDIF
		ENDREPEAT
	#ENDIF
ENDPROC





#IF USE_TU_CHANGES

PROC DISPLAY_TSHIRT_AWARD_MESSGE_AFTER_TRANSITION(PED_COMP_TYPE_ENUM typeofgift)
	g_award_event_tshirt = TRUE

	g_award_event_tshirt_TYPE = typeofgift // 1 = masks

	PRINTLN("DISPLAY_TSHIRT_AWARD_MESSGE_AFTER_TRANSITION() calledg_award_event_tshirt_TYPE =",ENUM_TO_INT(g_award_event_tshirt_TYPE) )
ENDPROC

PROC DISPLAY_AMMO_AWARD_MESSGE_AFTER_TRANSITION()
	g_award_event_ammo_LTS = TRUE
	
	
	
	PRINTLN("DISPLAY_AMMO_AWARD_MESSGE_AFTER_TRANSITION() called")
ENDPROC


PROC UNLOCK_BIKER_PACK_REWARDS()

			IF g_sMPTunables.bTSHIRT_WESTERN_BIG_LOGO_WHITE    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_WESTERN_BIG_LOGO_WHITE)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_WESTERN_BIG_LOGO_WHITE, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt0", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS bTSHIRT_WESTERN_BIG_LOGO_WHITE shirt unlocked ")
				ENDIF
			ENDIF
			
			
			IF g_sMPTunables.bTSHIRT_WESTERN_BIG_LOGO_BLACK    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_WESTERN_BIG_LOGO_BLACK)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_WESTERN_BIG_LOGO_BLACK, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt1", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3",DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_BOOL_TSHIRT_WESTERN_BIG_LOGO_BLACK shirt unlocked ")
				ENDIF
			ENDIF
			
			IF g_sMPTunables.bTSHIRT_WESTERN_NAME_WHITE    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_WESTERN_NAME_WHITE)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_WESTERN_NAME_WHITE, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt2", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_BOOL_TSHIRT_WESTERN_NAME_WHITE shirt unlocked ")
				ENDIF
			ENDIF
			
			IF g_sMPTunables.bTSHIRT_WESTERN_NAME_BLACK    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_WESTERN_NAME_BLACK)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_WESTERN_NAME_BLACK, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt3", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_BOOL_TSHIRT_WESTERN_NAME_BLACK shirt unlocked ")
				ENDIF
			ENDIF
			
			IF g_sMPTunables.bTSHIRT_STEEL_HORSE_BIG_LOGO_SOLID    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_STEEL_HORSE_BIG_LOGO_SOLID)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_STEEL_HORSE_BIG_LOGO_SOLID, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt4", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_BOOL_TSHIRT_STEEL_HORSE_BIG_LOGO_SOLID shirt unlocked ")
				ENDIF
			ENDIF
			IF g_sMPTunables.bTSHIRT_STEEL_HORSE_BIG_LOGO    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_STEEL_HORSE_BIG_LOGO)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_STEEL_HORSE_BIG_LOGO, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt5", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_BOOL_TSHIRT_STEEL_HORSE_BIG_LOGO shirt unlocked ")
				ENDIF
			ENDIF
			IF g_sMPTunables.bTSHIRT_STEEL_HORSE_NAME_WHITE    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_STEEL_HORSE_NAME_WHITE)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_STEEL_HORSE_NAME_WHITE, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt6", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_BOOL_TSHIRT_STEEL_HORSE_NAME_WHITE shirt unlocked ")
				ENDIF
			ENDIF
			
			IF g_sMPTunables.bTSHIRT_STEEL_HORSE_NAME_BLACK    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_STEEL_HORSE_NAME_BLACK)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_STEEL_HORSE_NAME_BLACK, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt7", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_BOOL_TSHIRT_STEEL_HORSE_NAME_BLACK shirt unlocked ")
				ENDIF
			ENDIF
			
			IF g_sMPTunables.bTSHIRT_NAGASAKI_LOGO_WHITE_SHIRT_WHITE_STAR    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_NAGASAKI_LOGO_WHITE_SHIRT_WHITE_STAR)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_NAGASAKI_LOGO_WHITE_SHIRT_WHITE_STAR, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt8", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_BOOL_TSHIRT_NAGASAKI_LOGO_WHITE_SHIRT_WHITE_STAR shirt unlocked ")
				ENDIF
			ENDIF
			
			IF g_sMPTunables.bTSHIRT_NAGASAKI_LOGO_WHITE_SHIRT_RED_STAR    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_NAGASAKI_LOGO_WHITE_SHIRT_RED_STAR)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_NAGASAKI_LOGO_WHITE_SHIRT_RED_STAR, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt9", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_BOOL_TSHIRT_NAGASAKI_LOGO_WHITE_SHIRT_RED_STAR shirt unlocked ")
				ENDIF
			ENDIF
			
			
			
			IF g_sMPTunables.bTSHIRT_NAGASAKI_LOGO_BLACK_SHIRT_WHITE_STAR    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_NAGASAKI_LOGO_BLACK_SHIRT_WHITE_STAR)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_NAGASAKI_LOGO_BLACK_SHIRT_WHITE_STAR, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt10", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(PACKED_MP_BOOL_TSHIRT_NAGASAKI_LOGO_BLACK_SHIRT_WHITE_STAR)
				ENDIF
			ENDIF
			
			
			IF g_sMPTunables.bTSHIRT_PURPLE_HELMETS_LOGO    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_PURPLE_HELMETS_LOGO)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_PURPLE_HELMETS_LOGO, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt11", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(PACKED_MP_BOOL_TSHIRT_PURPLE_HELMETS_LOGO)
				ENDIF
			ENDIF
			
			IF g_sMPTunables.bTSHIRT_PRINCIPLE_LOGO    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_PRINCIPLE_LOGO)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_TSHIRT_PRINCIPLE_LOGO, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt12", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(PACKED_MP_BOOL_TSHIRT_PRINCIPLE_LOGO)
				ENDIF
			ENDIF
			
			IF g_sMPTunables.bHOODIE_STEEL_HORSE_LOGO_BLACK    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_STEEL_HORSE_LOGO_BLACK)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_STEEL_HORSE_LOGO_BLACK, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt13", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN("UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_BOOL_HOODIE_STEEL_HORSE_LOGO_BLACK ")
				ENDIF
			ENDIF
			
			IF g_sMPTunables.bHOODIE_STEEL_HORSE_LOGO_WHITE    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_STEEL_HORSE_LOGO_WHITE)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_STEEL_HORSE_LOGO_WHITE, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt14", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN("UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_BOOL_HOODIE_STEEL_HORSE_LOGO_WHITE")
				ENDIF
			ENDIF
			
			IF g_sMPTunables.bHOODIE_WESTERN_LOGO_BLACK    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_WESTERN_LOGO_BLACK)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_WESTERN_LOGO_BLACK, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt15", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(PACKED_MP_BOOL_HOODIE_WESTERN_LOGO_BLACK)
				ENDIF
			ENDIF
			
			IF g_sMPTunables.bHOODIE_WESTERN_LOGO_WHITE    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_WESTERN_LOGO_WHITE)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_WESTERN_LOGO_WHITE, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt16", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(PACKED_MP_BOOL_HOODIE_WESTERN_LOGO_WHITE)
				ENDIF
			ENDIF
			
			

			IF g_sMPTunables.bHOODIE_NAGASAKI_WHITE_STAR_LOGO_WHITE    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_NAGASAKI_WHITE_STAR_LOGO_WHITE)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_NAGASAKI_WHITE_STAR_LOGO_WHITE, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt17", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(PACKED_MP_BOOL_HOODIE_NAGASAKI_WHITE_STAR_LOGO_WHITE)
				ENDIF
			ENDIF
			
			IF g_sMPTunables.bHOODIE_NAGASAKI_RED_STAR_LOGO_WHITE    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_NAGASAKI_RED_STAR_LOGO_WHITE)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_NAGASAKI_RED_STAR_LOGO_WHITE, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt18", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(PACKED_MP_BOOL_HOODIE_NAGASAKI_RED_STAR_LOGO_WHITE)
				ENDIF
			ENDIF
			
			IF g_sMPTunables.bHOODIE_NAGASAKI_WHITE_STAR_LOGO_BLACK    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_NAGASAKI_WHITE_STAR_LOGO_BLACK)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_NAGASAKI_WHITE_STAR_LOGO_BLACK, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt19", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(PACKED_MP_BOOL_HOODIE_NAGASAKI_WHITE_STAR_LOGO_BLACK)
				ENDIF
			ENDIF
			
			IF g_sMPTunables.bHOODIE_PURPLE_HELMET_LOGO    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_PURPLE_HELMET_LOGO)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_PURPLE_HELMET_LOGO, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt20", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(PACKED_MP_BOOL_HOODIE_PURPLE_HELMET_LOGO)
				ENDIF
			ENDIF
			
			IF g_sMPTunables.bHOODIE_PRINCIPLE_LOGO    = TRUE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_PRINCIPLE_LOGO)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HOODIE_PRINCIPLE_LOGO, TRUE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"bikshirt21", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
					PRINTLN(PACKED_MP_BOOL_HOODIE_PRINCIPLE_LOGO)
				ENDIF
			ENDIF
			
			
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DISPLAY_SHOTARO_MESSAGE)
				IF IS_MP_VEHICLE_UNLOCKED(PLAYER_VEHICLE_SHOTARO)	
					PRINTLN("[KW] Unlocking Shotaro")
					SET_MP_VEHICLE_UNLOCKED(PLAYER_VEHICLE_SHOTARO, TRUE, TRUE)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DISPLAY_SHOTARO_MESSAGE, TRUE)
				ENDIF
			ENDIF
		
ENDPROC


PROC UNLOCK_BUSINESS_BATTLES_PACK_REWARDS(BOOL bOkForFreemodeHelp, BOOL bInCutscene)
	
	IF bInCutscene
	OR NOT bOkForFreemodeHelp
		EXIT
	ENDIF
	
	IF g_sMPTunables.bAWARD_EMOTION_983_TSHIRT			
		IF NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_Emotion983TShirt)			
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_Emotion983TShirt,			TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_STAT_BOOL_AWARD_Emotion983TShirt,		   unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN0", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF	
	ENDIF
	
	IF g_sMPTunables.bAWARD_CROCS_BAR_TSHIRT
		IF NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_CrocsBarTShirt)            
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_CrocsBarTShirt,           TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_STAT_BOOL_AWARD_CrocsBarTShirt,             unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN1", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF		
	IF g_sMPTunables.bAWARD_BASE5_TSHIRT
		IF NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_Base5TShirt)                
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_Base5TShirt,               TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_STAT_BOOL_AWARD_Base5TShirt,                 unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN2", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF		
	IF g_sMPTunables.bAWARD_BOBO_TSHIRT 
		IF NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_BOBOTShirt)                 
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_BOBOTShirt,                TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_STAT_BOOL_AWARD_BOBOTShirt,                  unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN3", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF		
	IF g_sMPTunables.bAWARD_BITCHN_DOGFOOD_TSHIRT 
		IF NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_BitchnDogFoodTShirt)       
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_BitchnDogFoodTShirt,      TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_STAT_BOOL_AWARD_BitchnDogFoodTShirt,        unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN4", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	IF g_sMPTunables.bAWARD_KJAH_RADIO_TSHIRT 
		IF NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_KJAHRadioTShirt)           
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_KJAHRadioTShirt,          TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_STAT_BOOL_AWARD_KJAHRadioTShirt,            unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN5", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF		
	IF g_sMPTunables.bAWARD_VIVISECTION_TSHIRT 
		IF NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_VivisectionTShirt)          
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_VivisectionTShirt,         TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_STAT_BOOL_AWARD_VivisectionTShirt,           unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN6", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
		
	IF g_sMPTunables.bAWARD_KROSE_TSHIRT
		IF NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_KROSE)                
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_KROSE,               TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_STAT_BOOL_AWARD_KROSE,                 unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN7", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
		
	IF g_sMPTunables.bAWARD_FLASH_TSHIRT
		IF NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_FlashTShirt)                
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_FlashTShirt,               TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_STAT_BOOL_AWARD_FlashTShirt,                 unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN8", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
		
	IF g_sMPTunables.bAWARD_VINYL_COUNTDOWN_TSHIRT 
		IF NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_VinylCountdownTShirt)      
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_VinylCountdownTShirt,     TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_STAT_BOOL_AWARD_VinylCountdownTShirt,       unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN9", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	IF g_sMPTunables.bAWARD_FEVER_105_TSHIRT 
		IF NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_Fever105TShirt)            
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_Fever105TShirt,           TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_STAT_BOOL_AWARD_Fever105TShirt,             unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN10", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	IF g_sMPTunables.bAWARD_VICTORY_FIST_TSHIRT
		IF NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_VictoryFistTshirt)         
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_VictoryFistTshirt,        TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_STAT_BOOL_AWARD_VictoryFistTshirt,          unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN11", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	IF g_sMPTunables.bAWARD_KDST_TSHIRT 
		IF NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_KDSTTShirt)                 
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_KDSTTShirt,                TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_STAT_BOOL_AWARD_KDSTTShirt,                  unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN12", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	IF g_sMPTunables.bAWARD_BOUNCEFM_TSHIRT
		IF NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_BounceFMTShirt)             
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_BounceFMTShirt,            TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_STAT_BOOL_AWARD_BounceFMTShirt,              unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN13", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	IF g_sMPTunables.bAWARD_HOMIES_SHARP_TSHIRT
		IF NOT GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_HomiesSharpTShirt)         
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_AWARD_HomiesSharpTShirt,        TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_STAT_BOOL_AWARD_HomiesSharpTShirt,          unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN14", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	IF g_sMPTunables.bAWARD_PLEASE_STOP_ME_MASK  
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCK_LTS_REWARD_MASK)         
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCK_LTS_REWARD_MASK,        TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_STAT_UNLOCK_LTS_REWARD_MASK,          unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN15", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	IF g_sMPTunables.bAWARD_HIGH_FLYER_CHUTE_BAG 
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCK_PILOT_SCHOOL_REWARD_BAG )        
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCK_PILOT_SCHOOL_REWARD_BAG ,       TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_STAT_UNLOCK_PILOT_SCHOOL_REWARD_BAG ,         unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN16", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,FALSE)
		ENDIF
	ENDIF
	IF g_sMPTunables.bAWARD_FAKE_PERSEUS_TSHIRT  
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_FAKE_PERSEUS_TSHIRT)         
			SET_PACKED_STAT_BOOL(PACKED_MP_FAKE_PERSEUS_TSHIRT,        TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_FAKE_PERSEUS_TSHIRT,          unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN17", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	IF g_sMPTunables.bAWARD_FAKE_SANTO_CAPRA_TSHIRT
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_FAKE_SANTO_CAPRA_TSHIRT)     
			SET_PACKED_STAT_BOOL(PACKED_MP_FAKE_SANTO_CAPRA_TSHIRT,    TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_FAKE_SANTO_CAPRA_TSHIRT,      unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN18", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	IF g_sMPTunables.bAWARD_FAKE_DIX_WHITE_TSHIRT
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_FAKE_DIX_WHITE_TSHIRT)       
			SET_PACKED_STAT_BOOL(PACKED_MP_FAKE_DIX_WHITE_TSHIRT,      TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_FAKE_DIX_WHITE_TSHIRT,        unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN19", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	IF g_sMPTunables.bAWARD_FAKE_LE_CHIEN_CREW_TSHIRT 
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_FAKE_LE_CHIEN_CREW_TSHIRT)   
			SET_PACKED_STAT_BOOL(PACKED_MP_FAKE_LE_CHIEN_CREW_TSHIRT,  TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_FAKE_LE_CHIEN_CREW_TSHIRT,    unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN20", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	IF g_sMPTunables.bAWARD_FAKE_ENEMA_TSHIRT  
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_FAKE_ENEMA_TSHIRT_TSHIRT)           
			SET_PACKED_STAT_BOOL(PACKED_MP_FAKE_ENEMA_TSHIRT_TSHIRT,          TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_FAKE_ENEMA_TSHIRT_TSHIRT,            unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN21", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	IF g_sMPTunables.bAWARD_FAKE_DIX_GOLD_TSHIRT
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_FAKE_DIX_GOLD_TSHIRT)        
			SET_PACKED_STAT_BOOL(PACKED_MP_FAKE_DIX_GOLD_TSHIRT,       TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_FAKE_DIX_GOLD_TSHIRT,         unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN22", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	IF g_sMPTunables.bAWARD_FAKE_LE_CHIEN_NO2_TSHIRT
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_FAKE_LE_CHIEN_NO2_TSHIRT)    
			SET_PACKED_STAT_BOOL(PACKED_MP_FAKE_LE_CHIEN_NO2_TSHIRT,   TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_FAKE_LE_CHIEN_NO2_TSHIRT,     unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN23", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bAWARD_FAKE_SESSANTA_NOVE_TSHIRT
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_FAKE_SESSANTA_NOVE_TSHIRT)   
			SET_PACKED_STAT_BOOL(PACKED_MP_FAKE_SESSANTA_NOVE_TSHIRT,  TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_FAKE_SESSANTA_NOVE_TSHIRT,    unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN24", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bAWARD_FAKE_DIDIER_SACHS_TSHIRT
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_FAKE_DIDIER_SACHS_TSHIRT)    
			SET_PACKED_STAT_BOOL(PACKED_MP_FAKE_DIDIER_SACHS_TSHIRT,   TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_FAKE_DIDIER_SACHS_TSHIRT,     unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN25", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF	
			
	IF g_sMPTunables.bAWARD_FAKE_VAPID_TSHIRT 
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_FAKE_VAPID_TSHIRT)           
			SET_PACKED_STAT_BOOL(PACKED_MP_FAKE_VAPID_TSHIRT,			TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_FAKE_VAPID_TSHIRT,			  unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN26", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)																																	
		ENDIF
	ENDIF
	
	IF NOT  GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_KIFFOLM_MESSAGING	)	 
		IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_DRUNK_SPAWN_EPSILON_UNLOCK)
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_KIFFOLM_MESSAGING,			TRUE)  
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_BOOL_KIFFOLM_MESSAGING,			  unlocked ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHM_DECL_0", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)																																	
			
		ENDIF
	ENDIF
	 		 
	IF  g_sMPTunables.bMaisonette_Los_Santos_tshirt	  IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT1)   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT1,   TRUE)    PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   MP_STAT_BBNCSHIRT1,     unlocked ")	SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBNCSHIRT1", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)  ENDIF  ENDIF
	IF  g_sMPTunables.bStudio_Los_Santos_tshirt		  IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT2)   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT2,   TRUE)    PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   MP_STAT_BBNCSHIRT2,     unlocked ")	SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBNCSHIRT2", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)  ENDIF  ENDIF
	IF  g_sMPTunables.bGalaxy_tshirt		          IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT3)   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT3,   TRUE)    PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   MP_STAT_BBNCSHIRT3,     unlocked ")	SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBNCSHIRT3", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)  ENDIF  ENDIF
	IF  g_sMPTunables.bGefangis_tshirt		          IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT4)   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT4,   TRUE)    PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   MP_STAT_BBNCSHIRT4,     unlocked ")	SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBNCSHIRT4", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)  ENDIF  ENDIF
	IF  g_sMPTunables.bOmega_tshirt		         	  IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT5)   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT5,   TRUE)    PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   MP_STAT_BBNCSHIRT5,     unlocked ")	SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBNCSHIRT5", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)  ENDIF  ENDIF
	IF  g_sMPTunables.bTechnologie_tshirt		      IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT6)   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT6,   TRUE)    PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   MP_STAT_BBNCSHIRT6,     unlocked ")	SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBNCSHIRT6", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)  ENDIF  ENDIF
	IF  g_sMPTunables.bParadise_tshirt		          IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT7)   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT7,   TRUE)    PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   MP_STAT_BBNCSHIRT7,     unlocked ")	SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBNCSHIRT7", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)  ENDIF  ENDIF
	IF  g_sMPTunables.bThe_Palace_tshirt		      IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT8)   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT8,   TRUE)    PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   MP_STAT_BBNCSHIRT8,     unlocked ")	SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBNCSHIRT8", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)  ENDIF  ENDIF
	IF  g_sMPTunables.bTonys_Fun_House_tshirt		  IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT9)   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BBNCSHIRT9,   TRUE)    PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   MP_STAT_BBNCSHIRT9,     unlocked ")	SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBNCSHIRT9", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)  ENDIF  ENDIF
		 
	 
	IF g_sMPTunables.bAWARD_LS_UR										IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_LS_UR						) 	SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_LS_UR							, TRUE)	 PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_LS_UR						unlocked ")    SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN27", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_NONSTOPPOP_FM                               IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_NONSTOPPOP_FM              )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_NONSTOPPOP_FM                 , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_NONSTOPPOP_FM              unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN28", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_RADIO_LOS_SANTOS                            IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_RADIO_LOS_SANTOS           )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_RADIO_LOS_SANTOS              , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_RADIO_LOS_SANTOS           unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN29", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_LOS_SANTOS_ROCK_RADIO                       IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_LOS_SANTOS_ROCK_RADIO      )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_LOS_SANTOS_ROCK_RADIO         , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_LOS_SANTOS_ROCK_RADIO      unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN30", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_BLONDED_LOS_SANTOS_978_FM                   IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_BLONDED_LOS_SANTOS_978_FM  )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_BLONDED_LOS_SANTOS_978_FM     , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_BLONDED_LOS_SANTOS_978_FM  unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN31", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_WEST_COAST_TALK_RADIO                       IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_WEST_COAST_TALK_RADIO      )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_WEST_COAST_TALK_RADIO         , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_WEST_COAST_TALK_RADIO      unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN32", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_RADIO_MIRROR_PARK                           IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_RADIO_MIRROR_PARK          )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_RADIO_MIRROR_PARK             , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_RADIO_MIRROR_PARK          unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN33", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_REBEL_RADIO                                 IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_REBEL_RADIO                )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_REBEL_RADIO                   , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_REBEL_RADIO                unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN34", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_CHANNEL_X                                   IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_CHANNEL_X                  )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_CHANNEL_X                     , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_CHANNEL_X                  unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN35", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_VINEWOOD_BOULEVARD_RADIO                    IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_VINEWOOD_BOULEVARD_RADIO   )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_VINEWOOD_BOULEVARD_RADIO      , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_VINEWOOD_BOULEVARD_RADIO   unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN36", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_FLYLO_FM                                    IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_FLYLO_FM                   )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_FLYLO_FM                      , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_FLYLO_FM                   unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN37", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_SPACE_1032                                  IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_SPACE_1032                 )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_SPACE_1032                    , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_SPACE_1032                 unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN38", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_WEST_COAST_CLASSICS                         IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_WEST_COAST_CLASSICS        )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_WEST_COAST_CLASSICS           , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_WEST_COAST_CLASSICS        unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN39", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_EAST_LOS_FM                                 IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_EAST_LOS_FM                )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_EAST_LOS_FM                   , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_EAST_LOS_FM                unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN40", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_THE_LAB                                     IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_THE_LAB                    )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_THE_LAB                       , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_THE_LAB                    unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN41", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_THE_LOWDOWN_911                             IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_THE_LOWDOWN_911            )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_THE_LOWDOWN_911               , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_THE_LOWDOWN_911            unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN42", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_WORLDWIDE_FM                                IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_WORLDWIDE_FM               )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_WORLDWIDE_FM                  , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_WORLDWIDE_FM               unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN43", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_SOULWAX_FM                                  IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_SOULWAX_FM                 )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_SOULWAX_FM                    , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_SOULWAX_FM                 unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN44", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_BLUE_ARK                                    IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_BLUE_ARK                   )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_BLUE_ARK                      , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_BLUE_ARK                   unlocked ")     SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN45", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
    IF g_sMPTunables.bAWARD_BLAINE_COUNTY_RADIO			                IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_BLAINE_COUNTY_RADIO		)   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SHIRT_BLAINE_COUNTY_RADIO		    , TRUE)  PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_SHIRT_BLAINE_COUNTY_RADIO			unlocked ")    SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN46", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
                                                                                                                                                                                                                                                                                                                                                                              
	  IF g_sMPTunables.bENABLE_SHIRT_SOL_1	IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_055_DJ_SOLOMUN			)   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_055_DJ_SOLOMUN			, TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_055_DJ_SOLOMUN		unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_54", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_SOL_2  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_061_DJ_SOLOMUN           )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_061_DJ_SOLOMUN           , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_061_DJ_SOLOMUN        unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_60", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_SOL_3  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_059_DJ_SOLOMUN           )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_059_DJ_SOLOMUN           , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_059_DJ_SOLOMUN        unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_58", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_SOL_4  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_057_DJ_SOLOMUN           )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_057_DJ_SOLOMUN           , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_057_DJ_SOLOMUN        unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_56", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_SOL_5  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_060_DJ_SOLOMUN           )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_060_DJ_SOLOMUN           , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_060_DJ_SOLOMUN        unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_59", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_SOL_6  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_062_DJ_SOLOMUN           )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_062_DJ_SOLOMUN           , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_062_DJ_SOLOMUN        unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_61", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_DIX_1  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_041_DJ_DIXON             )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_041_DJ_DIXON             , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_041_DJ_DIXON          unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_40", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_DIX_2  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_042_DJ_DIXON             )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_042_DJ_DIXON             , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_042_DJ_DIXON          unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_41", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_DIX_3  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_045_DJ_DIXON             )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_045_DJ_DIXON             , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_045_DJ_DIXON          unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_44", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_DIX_4  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_039_DJ_DIXON             )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_039_DJ_DIXON             , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_039_DJ_DIXON          unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_38", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_DIX_5  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_040_DJ_DIXON             )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_040_DJ_DIXON             , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_040_DJ_DIXON          unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_39", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_DIX_6  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_044_DJ_DIXON             )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_044_DJ_DIXON             , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_044_DJ_DIXON          unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_43", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_TOS_1  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_047_DJ_TALE_OF_US        )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_047_DJ_TALE_OF_US        , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_047_DJ_TALE_OF_US     unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_46", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_TOS_2  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_048_DJ_TALE_OF_US        )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_048_DJ_TALE_OF_US        , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_048_DJ_TALE_OF_US     unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_47", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_TOS_3  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_053_DJ_TALE_OF_US        )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_053_DJ_TALE_OF_US        , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_053_DJ_TALE_OF_US     unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_52", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_TOS_4  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_049_DJ_TALE_OF_US        )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_049_DJ_TALE_OF_US        , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_049_DJ_TALE_OF_US     unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_48", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_TOS_5  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_051_DJ_TALE_OF_US        )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_051_DJ_TALE_OF_US        , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_051_DJ_TALE_OF_US     unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_50", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_TOS_6  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_052_DJ_TALE_OF_US        )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_052_DJ_TALE_OF_US        , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_052_DJ_TALE_OF_US     unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_51", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_TBM_1  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_031_DJ_BLACK_MADONNA     )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_031_DJ_BLACK_MADONNA     , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_031_DJ_BLACK_MADONNA  unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_30", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_TBM_2  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_032_DJ_BLACK_MADONNA     )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_032_DJ_BLACK_MADONNA     , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_032_DJ_BLACK_MADONNA  unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_31", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_TBM_3  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_033_DJ_BLACK_MADONNA     )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_033_DJ_BLACK_MADONNA     , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_033_DJ_BLACK_MADONNA  unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_32", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_TBM_4  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_035_DJ_BLACK_MADONNA     )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_035_DJ_BLACK_MADONNA     , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_035_DJ_BLACK_MADONNA  unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_34", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_TBM_5  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_037_DJ_BLACK_MADONNA     )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_037_DJ_BLACK_MADONNA     , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_037_DJ_BLACK_MADONNA  unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_36", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
	  IF g_sMPTunables.bENABLE_SHIRT_TBM_6  IF NOT GET_PACKED_STAT_BOOL( PACKED_MP_BOOL_MP_BATTLE_CLOTHING_038_DJ_BLACK_MADONNA     )   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_038_DJ_BLACK_MADONNA     , TRUE)   PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_CLOTHING_038_DJ_BLACK_MADONNA  unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_37", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF
		 
	  
	  IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_MSG_CLOTHING_058_DJ_SOLOMUN)				 IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_058_DJ_SOLOMUN)        SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_MSG_CLOTHING_058_DJ_SOLOMUN, TRUE)			 PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_MSG_CLOTHING_058_DJ_SOLOMUN  unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_57", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF	
	  IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_MSG_CLOTHING_050_DJ_TALE_OF_US)           IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_050_DJ_TALE_OF_US)     SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_MSG_CLOTHING_050_DJ_TALE_OF_US, TRUE)		 PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_MSG_CLOTHING_050_DJ_TALE_OF_US  unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_49", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF	   
	  IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_MSG_CLOTHING_043_DJ_DIXON		)        IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_043_DJ_DIXON		)     SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_MSG_CLOTHING_043_DJ_DIXON, TRUE)		     PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_MSG_CLOTHING_043_DJ_DIXON  unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_42", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF	
	  IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_MSG_CLOTHING_036_DJ_BLACK_MADONNA)        IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_CLOTHING_036_DJ_BLACK_MADONNA)  SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MP_BATTLE_MSG_CLOTHING_036_DJ_BLACK_MADONNA, TRUE)	 PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS  PACKED_MP_BOOL_MP_BATTLE_MSG_CLOTHING_036_DJ_BLACK_MADONNA  unlocked ")   SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_BHF_DECL_35", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE) ENDIF ENDIF		
	  		 
				 	 
	  IF  g_sMPTunables.bKIFFLOMTEE_LOGINAWARD		  IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_KIFFLOMTEE_LOGINAWARD)   SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_KIFFLOMTEE_LOGINAWARD,   TRUE)    PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_BOOL_KIFFLOMTEE_LOGINAWARD,     unlocked ")	SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"AW_KIFFLOMTEE", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)  ENDIF  ENDIF
		 
ENDPROC


PROC UNLOCK_BUSINESS_BATTLES_PACK_REWARDS2(BOOL bOkForFreemodeHelp, BOOL bInCutscene)
	
	IF bInCutscene
	OR NOT bOkForFreemodeHelp
		EXIT
	ENDIF
	
	IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
	AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
	AND NOT IS_HELP_MESSAGE_FADING_OUT()
	AND NOT IS_PLAYER_IN_ANY_PART_OF_CASINO(PLAYER_ID())
	#IF FEATURE_CASINO_HEIST
	AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
	#ENDIF
//		IF g_sMPTunables.bENABLE_BLIMP3             IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_BLIMP3           )  SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_BLIMP3        ,	TRUE) PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_BOOL_MESSAGE_BLIMP3       unlocked ")    PRINT_HELP("BBTICKUNLO1")    EXIT ENDIF   ENDIF
//		IF g_sMPTunables.bENABLE_FREECRAWLER        IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_FREECRAWLER      )  SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_FREECRAWLER   ,	TRUE) PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_BOOL_MESSAGE_FREECRAWLER  unlocked ")    PRINT_HELP("BBTICKUNLO2")   EXIT ENDIF   ENDIF
//		IF g_sMPTunables.bENABLE_MENACER            IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_MENACER          )  SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_MENACER       ,	TRUE) PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_BOOL_MESSAGE_MENACER      unlocked ")    PRINT_HELP("BBTICKUNLO3") EXIT   ENDIF   ENDIF
//		IF g_sMPTunables.bENABLE_SCRAMJET           IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_SCRAMJET         )  SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_SCRAMJET      ,	TRUE) PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_BOOL_MESSAGE_SCRAMJET     unlocked ")    PRINT_HELP("BBTICKUNLO4")   EXIT ENDIF   ENDIF
//		IF g_sMPTunables.bENABLE_STRIKEFORCE        IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_STRIKEFORCE      )  SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_STRIKEFORCE   ,	TRUE) PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_BOOL_MESSAGE_STRIKEFORCE  unlocked ")    PRINT_HELP("BBTICKUNLO5") EXIT   ENDIF   ENDIF
//		IF g_sMPTunables.bENABLE_TERBYTE            IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_TERBYTE          )  SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_TERBYTE       ,	TRUE) PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_BOOL_MESSAGE_TERBYTE      unlocked ")    PRINT_HELP("BBTICKUNLO6")  EXIT  ENDIF   ENDIF
//		IF g_sMPTunables.bENABLE_HEARSE             IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_HEARSE           )  SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_HEARSE        ,	TRUE) PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_BOOL_MESSAGE_HEARSE       unlocked ")    PRINT_HELP("BBTICKUNLO7")  EXIT  ENDIF   ENDIF
//		IF g_sMPTunables.bENABLE_PATRIOT            IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_PATRIOT          )  SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_PATRIOT       ,	TRUE) PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_BOOL_MESSAGE_PATRIOT      unlocked ")    PRINT_HELP("BBTICKUNLO8")  EXIT  ENDIF   ENDIF
//		IF g_sMPTunables.bENABLE_SERRANO            IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_SERRANO          )  SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_SERRANO       ,	TRUE) PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_BOOL_MESSAGE_SERRANO      unlocked ")    PRINT_HELP("BBTICKUNLO9")  EXIT  ENDIF   ENDIF
//		IF g_sMPTunables.bENABLE_FUTO               IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_FUTO             )  SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_FUTO          ,	TRUE) PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_BOOL_MESSAGE_FUTO         unlocked ")    PRINT_HELP("BBTICKUNLO10") EXIT  ENDIF   ENDIF
//		IF g_sMPTunables.bENABLE_PRAIRIE            IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_PRAIRIE          )  SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_PRAIRIE       ,	TRUE) PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_BOOL_MESSAGE_PRAIRIE      unlocked ")    PRINT_HELP("BBTICKUNLO11") EXIT  ENDIF   ENDIF
//		IF g_sMPTunables.bENABLE_PENUMBRA_MODS      IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_PENUMBRA_MODS    )  SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSAGE_PENUMBRA_MODS ,	TRUE) PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_BOOL_MESSAGE_PENUMBRA_MODSunlocked ")    PRINT_HELP("BBTICKUNLO12") EXIT  ENDIF   ENDIF
//		IF g_sMPTunables.bENABLE_STAFFORD      		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_EMUS_STATFORD    )  SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_EMUS_STATFORD ,	TRUE) PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   PACKED_MP_BOOL_EMUS_STATFORD ")    PRINT_HELP("BBTICKUNLO13") EXIT  ENDIF   ENDIF
//		

		IF NOT g_bmusic_mixdisplay

//			IF g_sMPTunables.bENABLE_LSUR_SOL  
//				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_SOLCOUNTER)   < 3   
//					INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_SOLCOUNTER)
//					PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   MP_STAT_SOLCOUNTER,			  unlocked ")
//					//SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN26", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)																																	
//					PRINT_HELP("SOLUNLOCK")  //A new Radio Station, LSUR, is now available.
//					g_bmusic_mixdisplay = TRUE
//					EXIT
//				ENDIF
//			ENDIF
//			
//			IF g_sMPTunables.bENABLE_LSUR_TOS  
//				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_TOSCOUNTER)   < 3   
//					INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_TOSCOUNTER)
//					PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   MP_STAT_TOSCOUNTER		  unlocked ")
//					g_bmusic_mixdisplay = TRUE				
//					PRINT_HELP("TOSUNLOCK")  //A new Radio Station, , is now available.
//					EXIT
//				ENDIF
//			ENDIF
//			
//			IF g_sMPTunables.bENABLE_LSUR_DIX   
//				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_DIXCOUNTER)   < 3   
//					INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_DIXCOUNTER)
//					PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   MP_STAT_DIXCOUNTER,			  unlocked ")
//					g_bmusic_mixdisplay = TRUE				
//					PRINT_HELP("DIXUNLOCK")  //A new Radio Station, , is now available.
//					EXIT
//				
//				ENDIF
//			ENDIF
//			
//			
//			IF g_sMPTunables.bENABLE_LSUR_BM  
//				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_BMCOUNTER)   < 3 
//					INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_BMCOUNTER)
//					PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS   MP_STAT_BMCOUNTER,			  unlocked ")
//					g_bmusic_mixdisplay = TRUE				
//					PRINT_HELP("BMUNLOCK")  //A new Radio Station, , is now available.
//					EXIT	
//				ENDIF	
//			ENDIF
			g_bmusic_mixdisplay = TRUE
		ENDIF

	ENDIF
	

ENDPROC

PROC UPDATE_NEW_INTERACTION_HELP(BOOL bOkForFreemodeHelp, BOOL bInCutscene)
	
	IF bInCutscene
	OR NOT bOkForFreemodeHelp
		EXIT
	ENDIF
	
	IF (MPGlobalsInteractions.bAnyInteractionIsPlaying)
	AND NOT (g_bInteractionHelp_new)
	AND (MPGlobalsInteractions.bQuickplayButtonPressed) // to avoid spawning interactions triggering it
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND NOT IS_HELP_MESSAGE_FADING_OUT()	      
		AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) // B* url:bugstar:5233354
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_INTERACTION_HELP1)  
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_INTERACTION_HELP1,TRUE) 
				PRINTLN(" UPDATE_NEW_INTERACTION_HELP   PACKED_MP_BOOL_INTERACTION_HELP1   ")    
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					PRINT_HELP("INTERACTIONP1")
				ELSE
					PRINT_HELP("INTERACTIONH1")    
				ENDIF
				g_bInteractionHelp_new = TRUE
				EXIT 
			ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_INTERACTION_HELP2)  
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_INTERACTION_HELP2,TRUE) 
				PRINTLN(" UPDATE_NEW_INTERACTION_HELP   PACKED_MP_BOOL_INTERACTION_HELP2   ")    
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					PRINT_HELP("INTERACTIONP1")
				ELSE
					PRINT_HELP("INTERACTIONH1")    
				ENDIF
				g_bInteractionHelp_new = TRUE
				EXIT 
			ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_INTERACTION_HELP3)  
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_INTERACTION_HELP3,TRUE) 
				PRINTLN(" UPDATE_NEW_INTERACTION_HELP   PACKED_MP_BOOL_INTERACTION_HELP2   ")    
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					PRINT_HELP("INTERACTIONP1")
				ELSE
					PRINT_HELP("INTERACTIONH1")    
				ENDIF
				g_bInteractionHelp_new = TRUE
				EXIT 
			ENDIF   
		ENDIF
	ENDIF
ENDPROC


PROC UNLOCK_GUNRUNNING_PACK_REWARDS()

		
		IF  g_sMPTunables.baward_black_ammunation_cap						
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_AMMUNATION_CAP)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_AMMUNATION_CAP, TRUE)		
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_BLACK_AMMUNATION_CAP")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_PH_6_8", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_black_ammunation_hoodie        	       
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_AMMUNATION_HOODIE)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_AMMUNATION_HOODIE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_BLACK_AMMUNATION_HOODIE")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_DECL_27", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		
		
		IF  g_sMPTunables.baward_black_ammunation_tee                      
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_AMMUNATION_TEE)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_AMMUNATION_TEE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_BLACK_AMMUNATION_TEE CLO_GRM_DECL_15 ")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_DECL_15", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
					
		
		IF  g_sMPTunables.baward_black_coil_cap                             
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_COIL_CAP)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_COIL_CAP, TRUE)   
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_PH_6_7", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_BLACK_COIL_CAP CLO_GRF_PH_6_7")
		ENDIF
		ENDIF
		
					
		IF  g_sMPTunables.baward_black_coil_tee                             
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_COIL_TEE)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_COIL_TEE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_BLACK_COIL_TEE CLO_GRM_DECL_14")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_DECL_14", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
					
		
		IF  g_sMPTunables.baward_black_hawk_and_little_hoodie               
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_HAWK_AND_LITTLE_HOODIE)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_HAWK_AND_LITTLE_HOODIE, TRUE)   
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_DECL_20", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_BLACK_HAWK_AND_LITTLE_HOODIE CLO_GRM_DECL_20")
		ENDIF
		ENDIF
		
					
		
		IF  g_sMPTunables.baward_black_hawk_and_little_logo_tee             
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_HAWK_AND_LITTLE_LOGO_TEE       )     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_HAWK_AND_LITTLE_LOGO_TEE      , TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_BLACK_HAWK_AND_LITTLE_LOGO_TEE CLO_GRF_DECL_0")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_DECL_0", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
					
		
		IF  g_sMPTunables.baward_black_hawk_and_little_tee                  
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_HAWK_AND_LITTLE_TEE)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_HAWK_AND_LITTLE_TEE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_BLACK_HAWK_AND_LITTLE_TEE CLO_GRF_DECL_3")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_DECL_3", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
					
		IF  g_sMPTunables.baward_black_shrewsbury_hoodie                    
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_SHREWSBURY_HOODIE)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_SHREWSBURY_HOODIE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_BLACK_SHREWSBURY_HOODIE CLO_GRF_DECL_22 ")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_DECL_22", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
					
		
		IF  g_sMPTunables.baward_black_vom_feuer_cap                        
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_VOM_FEUER_CAP)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_VOM_FEUER_CAP, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_BLACK_VOM_FEUER_CAP CLO_GRF_PH_6_5")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_PH_6_5", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_black_warstock_hoodie                      
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_WARSTOCK_HOODIE)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BLACK_WARSTOCK_HOODIE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_BLACK_WARSTOCK_HOODIE CLO_GRM_DECL_29")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_DECL_29", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_green_vom_feuer_tee                       
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_GREEN_VOM_FEUER_TEE)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_GREEN_VOM_FEUER_TEE, TRUE)
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_GREEN_VOM_FEUER_TEE CLO_GRF_DECL_11")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_DECL_11", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_red_hawk_and_little_cap                   
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RED_HAWK_AND_LITTLE_CAP)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RED_HAWK_AND_LITTLE_CAP, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_RED_HAWK_AND_LITTLE_CAP CLO_GRF_PH_6_0")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_PH_6_0", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		IF  g_sMPTunables.baward_warstock_cap                              
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WARSTOCK_CAP)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WARSTOCK_CAP, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_WARSTOCK_CAP CLO_GRM_PH_6_10")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_PH_6_10", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_white_ammunation_tee                       
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_AMMUNATION_TEE)    
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_AMMUNATION_TEE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_WHITE_AMMUNATION_TEE CLO_GRM_DECL_16")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_DECL_16", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		IF  g_sMPTunables.baward_white_coil_hoodie                          
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_COIL_HOODIE)    
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_COIL_HOODIE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_WHITE_COIL_HOODIE")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_DECL_25", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		IF  g_sMPTunables.baward_white_coil_tee                            
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_COIL_TEE)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_COIL_TEE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_WHITE_COIL_TEE CLO_GRM_DECL_25")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_DECL_12", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		IF  g_sMPTunables.baward_white_hawk_and_little_hoodie              
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_HAWK_AND_LITTLE_HOODIE)    
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_HAWK_AND_LITTLE_HOODIE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_WHITE_HAWK_AND_LITTLE_HOODIE CLO_GRF_DECL_19")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_DECL_19", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		IF  g_sMPTunables.baward_white_hawk_and_little_tee                 
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_HAWK_AND_LITTLE_TEE)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_HAWK_AND_LITTLE_TEE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_WHITE_HAWK_AND_LITTLE_TEE CLO_GRF_DECL_3")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_DECL_2", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		IF  g_sMPTunables.baward_white_shrewbury_tee                       
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_SHREWBURY_TEE)    
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_SHREWBURY_TEE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_WHITE_SHREWBURY_TEE CLO_GRF_DECL_6 ")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_DECL_6", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		IF  g_sMPTunables.baward_white_shrewsbury_cap                       
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_SHREWSBURY_CAP)    
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_SHREWSBURY_CAP, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_WHITE_SHREWSBURY_CAP CLO_GRF_PH_6_2")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_PH_6_2", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_white_shrewsbury_hoodie                    
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_SHREWSBURY_HOODIE)    
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_SHREWSBURY_HOODIE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_WHITE_SHREWSBURY_HOODIE CLO_GRM_DECL_21")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_DECL_21", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_white_shrewsbury_logo_tee                 
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_SHREWSBURY_LOGO_TEE)    
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_SHREWSBURY_LOGO_TEE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_WHITE_SHREWSBURY_LOGO_TEE CLO_GRF_DECL_5")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_DECL_5", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_white_vom_feuer_cap                        
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_VOM_FEUER_CAP)    
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_VOM_FEUER_CAP, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_WHITE_VOM_FEUER_CAP CLO_GRF_PH_6_4")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_PH_6_4", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_white_vom_feuer_hoodie                    
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_VOM_FEUER_HOODIE)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WHITE_VOM_FEUER_HOODIE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_WHITE_VOM_FEUER_HOODIE CLO_GRM_DECL_23")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_DECL_23", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_wine_coil_cap                             
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WINE_COIL_CAP)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_WINE_COIL_CAP, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_WINE_COIL_CAP CLO_GRF_PH_6_6 ")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_PH_6_6", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_yellow_vom_feuer_logo_tee                 
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_YELLOW_VOM_FEUER_LOGO_TEE)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_YELLOW_VOM_FEUER_LOGO_TEE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_YELLOW_VOM_FEUER_LOGO_TEE CLO_GRM_DECL_9")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_DECL_9", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		IF  g_sMPTunables.baward_yellow_vom_feuer_tee                      
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_YELLOW_VOM_FEUER_TEE)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_YELLOW_VOM_FEUER_TEE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_YELLOW_VOM_FEUER_TEE CLO_GRM_DECL_10")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_DECL_10", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_yellow_warstock_tee                        
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_YELLOW_WARSTOCK_TEE)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_YELLOW_WARSTOCK_TEE, TRUE)   
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_DECL_17", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_YELLOW_WARSTOCK_TEE CLO_GRM_DECL_17")
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_class_of_98_blue                          
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CLASS_OF_98_BLUE)    
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CLASS_OF_98_BLUE, TRUE)   
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_CLASS_OF_98_BLUE")
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_U_25_0", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_class_of_98_red                            
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CLASS_OF_98_RED)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CLASS_OF_98_RED, TRUE)   
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_U_25_1", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_CLASS_OF_98_RED")
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_rstar_logo_interference                  
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RSTAR_LOGO_INTERFERENCE)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RSTAR_LOGO_INTERFERENCE, TRUE)  
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_DECL_35", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_RSTAR_LOGO_INTERFERENCE")
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_rockstar_games_interference              
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ROCKSTAR_GAMES_INTERFERENCE)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ROCKSTAR_GAMES_INTERFERENCE, TRUE)   
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_DECL_36", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_ROCKSTAR_GAMES_INTERFERENCE")
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_rockstar_pink_blade                      
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ROCKSTAR_PINK_BLADE)    
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ROCKSTAR_PINK_BLADE, TRUE)   
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_DECL_33", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_ROCKSTAR_PINK_BLADE")
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_rstar_camo_logo_grey                      
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RSTAR_CAMO_LOGO_GREY)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RSTAR_CAMO_LOGO_GREY, TRUE)   
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_DECL_31", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_RSTAR_CAMO_LOGO_GREY")
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_rstar_camo_logo_white                    
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RSTAR_CAMO_LOGO_WHITE                )     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RSTAR_CAMO_LOGO_WHITE, TRUE)   
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_DECL_32", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_RSTAR_CAMO_LOGO_WHITE")
		ENDIF
		ENDIF
		
		
//		IF  g_sMPTunables.baward_rstar_logo_pattern                        
//		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RSTAR_LOGO_PATTERN)    
//		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RSTAR_LOGO_PATTERN, TRUE)   
//		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_U_2_24", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
//		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_RSTAR_LOGO_PATTERN")
//		ENDIF
//		ENDIF
		
		
		IF  g_sMPTunables.baward_rstar_logo_pattern_pocket                 
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RSTAR_LOGO_PATTERN_POCKET)     
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RSTAR_LOGO_PATTERN_POCKET, TRUE)   
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRM_U_20_0", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_RSTAR_LOGO_PATTERN_POCKET")
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_rstar_logo_black                         
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RSTAR_LOGO_BLACK)    
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RSTAR_LOGO_BLACK, TRUE)   
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_DECL_30", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_RSTAR_LOGO_BLACK")
		ENDIF
		ENDIF
		
		
		IF  g_sMPTunables.baward_rstar_logo_white                        
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RSTAR_LOGO_WHITE)    
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_RSTAR_LOGO_WHITE, TRUE)  
		SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_GRF_DECL_34", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		PRINTLN("UNLOCK_BIKER_PACK_REWARDS  UNLOCKED PACKED_MP_BOOL_RSTAR_LOGO_WHITE")
		ENDIF
		ENDIF
			
	
ENDPROC


PROC UNLOCKING_SPECIAL_EVENT_ITEMS_AT_LOG_IN()

	BOOL bIsPlayerMale = (GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0)
	
	IF g_sMPTunables.bUnlockbitchndogfoodtshirtEvent	
		IF bIsPlayerMale
			//GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("FM_Hip_F_Retro_004"), TATTOO_MP_FM_F)

			SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(DLC_SHIRT_RETRO_BITCHN,bIsPlayerMale), TATTOO_MP_FM), TRUE, FALSE)
			PRINTLN("MAINTAIN_UNLOCK_SPECIAL_EVENT_ITEMS - UNLOCK_SPECIAL_EVENT_ITEMS UNlock DLC_SHIRT_RETRO_BITCHN t shirt for a boy")
			
		ELSE
			SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(DLC_SHIRT_RETRO_BITCHN,bIsPlayerMale), TATTOO_MP_FM_F), TRUE, FALSE)

			PRINTLN("MAINTAIN_UNLOCK_SPECIAL_EVENT_ITEMS - UNLOCK_SPECIAL_EVENT_ITEMS UNlock DLC_SHIRT_RETRO_BITCHN t shirt for a girl")
		ENDIF
		

		
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_UNLOCK_HIPSTER_TSHIRT_DOG) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_UNLOCK_HIPSTER_TSHIRT_DOG, TRUE)
			SET_BIT(g_iMAINTAIN_UNLOCK_SPECIAL_EVENT_BitSet, ciMAINTAIN_UNLOCK_SPECIAL_EVENT_ITEMS_UNLOCK_AWRD_SHIRT1)
			PRINTLN("MAINTAIN_UNLOCK_SPECIAL_EVENT_ITEMS SET_BIT(g_iMAINTAIN_UNLOCK_SPECIAL_EVENT_BitSet, ciMAINTAIN_UNLOCK_SPECIAL_EVENT_ITEMS_UNLOCK_AWRD_SHIRT1)")			
			//PRINT_TICKER("UNLOCK_AWRD_SHIRT1")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,GET_DLC_SHIRT_NAME(DLC_SHIRT_RETRO_BITCHN), GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_BITCHN), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_BITCHN),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		
		ENDIF
	ENDIF


	IF g_sMPTunables.bUnlockvinylcountdowntshirtEvent	

		IF bIsPlayerMale 
			SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(DLC_SHIRT_RETRO_VINYL,bIsPlayerMale), TATTOO_MP_FM), TRUE, FALSE)
			PRINTLN("MAINTAIN_UNLOCK_SPECIAL_EVENT_ITEMS - UNLOCK_SPECIAL_EVENT_ITEMS UNlock DLC_SHIRT_RETRO_VINYL t shirt for a boy")
		ELSE
			SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(DLC_SHIRT_RETRO_VINYL,bIsPlayerMale), TATTOO_MP_FM_F), TRUE, FALSE)
			

			
			
			PRINTLN("MAINTAIN_UNLOCK_SPECIAL_EVENT_ITEMS - UNLOCK_SPECIAL_EVENT_ITEMS UNlock DLC_SHIRT_RETRO_VINYL t shirt for a girl")
		ENDIF

		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_UNLOCK_HIPSTER_TSHIRT_VINYL) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_UNLOCK_HIPSTER_TSHIRT_VINYL, TRUE)
			SET_BIT(g_iMAINTAIN_UNLOCK_SPECIAL_EVENT_BitSet, ciMAINTAIN_UNLOCK_SPECIAL_EVENT_ITEMS_UNLOCK_AWRD_SHIRT2)
			PRINTLN("MAINTAIN_UNLOCK_SPECIAL_EVENT_ITEMS SET_BIT(g_iMAINTAIN_UNLOCK_SPECIAL_EVENT_BitSet, ciMAINTAIN_UNLOCK_SPECIAL_EVENT_ITEMS_UNLOCK_AWRD_SHIRT2)")			
			//PRINT_TICKER("UNLOCK_AWRD_SHIRT2")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,GET_DLC_SHIRT_NAME(DLC_SHIRT_RETRO_VINYL), GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_VINYL), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_VINYL),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		
		ENDIF
		
	ENDIF

	IF g_sMPTunables.bUnlockhomiessharptshirtEvent	
		IF bIsPlayerMale
			SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(DLC_SHIRT_RETRO_HOMIES,bIsPlayerMale), TATTOO_MP_FM), TRUE, FALSE)

			PRINTLN("UNLOCK_SPECIAL_EVENT_ITEMS UNlock Retro homies t shirt for a boy")
		ELSE
			SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(DLC_SHIRT_RETRO_HOMIES,bIsPlayerMale), TATTOO_MP_FM_F), TRUE, FALSE)

			PRINTLN("UNLOCK_SPECIAL_EVENT_ITEMS UNlock Retro homies t shirt for a girl")
			
		ENDIF
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_UNLOCK_HIPSTER_TSHIRT_MESS) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_UNLOCK_HIPSTER_TSHIRT_MESS, TRUE)
			SET_BIT(g_iMAINTAIN_UNLOCK_SPECIAL_EVENT_BitSet, ciMAINTAIN_UNLOCK_SPECIAL_EVENT_ITEMS_UNLOCK_AWRD_SHIRT3)
			PRINTLN("MAINTAIN_UNLOCK_SPECIAL_EVENT_ITEMS SET_BIT(g_iMAINTAIN_UNLOCK_SPECIAL_EVENT_BitSet, ciMAINTAIN_UNLOCK_SPECIAL_EVENT_ITEMS_UNLOCK_AWRD_SHIRT3)")			
			//PRINT_TICKER("UNLOCK_AWRD_SHIRT3")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,GET_DLC_SHIRT_NAME(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		
		ENDIF
	ENDIF
	
	
	// INDEPENDENCE DAY ITEMS

	IF g_sMPTunables.bUnlockindependence_beer_hat_1 	
		IF GET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_BEER_HAT_1) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_BEER_HAT_1, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_BHAT1", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3",DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	
	
	IF g_sMPTunables.bUnlockindependence_beer_hat_2 
		IF GET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_BEER_HAT_2) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_BEER_HAT_2, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_BHAT2", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	
	
	
	IF g_sMPTunables.bUnlockindependence_beer_hat_3
		IF GET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_BEER_HAT_3) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_BEER_HAT_3, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_BHAT3", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	
	
	
	IF g_sMPTunables.bUnlockindependence_beer_hat_4 
		IF GET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_BEER_HAT_4) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_BEER_HAT_4, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_BHAT4", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bUnlockindependence_beer_hat_5 
		IF GET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_BEER_HAT_5) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_BEER_HAT_5, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_BHAT5", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bUnlockindependence_beer_hat_6 
		IF GET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_BEER_HAT_6) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_BEER_HAT_6, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_BHAT6", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bUnlockindependence_Statue_happiness_shirt
		IF GET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_STATUE_OF_HAPPINESS_TEE) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_STATUE_OF_HAPPINESS_TEE, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_SHIRT14", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
	ENDIF
	
	IF g_sMPTunables.baward_low_hats_magnetics_script 
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_MAGNETICS_SCRIPT) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_MAGNETICS_SCRIPT, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWHATS0", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			
			
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_F_PHEAD_1_0"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_M_PHEAD_1_0"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF

		ENDIF
	ENDIF	
	
	
	IF g_sMPTunables.baward_low_hats_magnetics_block  	
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_MAGNETICS_BLOCK) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_MAGNETICS_BLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWHATS1", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_F_PHEAD_1_1"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_M_PHEAD_1_1"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF

		
		ENDIF
	ENDIF		
	IF g_sMPTunables.baward_low_hats_low_santos   
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_LOW_SANTOS) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_LOW_SANTOS, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWHATS2", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_F_PHEAD_1_2"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_M_PHEAD_1_2"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
		
		
		
		ENDIF
	ENDIF		

	IF g_sMPTunables.baward_low_hats_boars 
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_BOARS) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_BOARS, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWHATS3", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3",DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_F_PHEAD_1_3"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_M_PHEAD_1_3"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
			
		ENDIF
	ENDIF	
	IF g_sMPTunables.baward_low_hats_bennys    
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_BENNYS) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_BENNYS, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWHATS4", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_F_PHEAD_1_4"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_M_PHEAD_1_4"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
		ENDIF
	ENDIF	
	IF g_sMPTunables.baward_low_hats_westside     
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_WESTSIDE) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_WESTSIDE, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWHATS5", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_F_PHEAD_1_5"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_M_PHEAD_1_5"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
		ENDIF
	ENDIF	
	IF g_sMPTunables.baward_low_hats_eastside    
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_EASTSIDE) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_EASTSIDE, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWHATS6", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_F_PHEAD_1_6"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_M_PHEAD_1_6"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
	
		ENDIF
	ENDIF	
	IF g_sMPTunables.baward_low_hats_strawberry    
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_STRAWBERRY) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_STRAWBERRY, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWHATS7", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_F_PHEAD_1_7"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_M_PHEAD_1_7"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
	
		ENDIF
	ENDIF	
	IF g_sMPTunables.baward_low_hats_sa       
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_SA) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_SA, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWHATS8", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_F_PHEAD_1_8"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_M_PHEAD_1_8"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
	
		ENDIF
	ENDIF	
	IF g_sMPTunables.baward_low_hats_davis   
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_DAVIS) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_HATS_DAVIS, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWHATS9", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_F_PHEAD_1_9"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_LOW_M_PHEAD_1_9"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
	
		ENDIF
	ENDIF	


	IF g_sMPTunables.baward_low_tshirt_vinewood_zombie	
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_VINEWOOD_ZOMBIE) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_VINEWOOD_ZOMBIE, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWTSHIRT0", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)

			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_001_F"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_001_M"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
		
		ENDIF
	ENDIF	
	IF g_sMPTunables.baward_low_tshirt_knife_after_dark  
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_KNIFE_AFTER_DARK) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_KNIFE_AFTER_DARK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWTSHIRT1", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_007_F"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_007_M"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
		
		ENDIF
	ENDIF	
	IF g_sMPTunables.baward_low_tshirt_the_simian      
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_THE_SIMIAN) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_THE_SIMIAN, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWTSHIRT2", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_008_F"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_008_M"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
		
		ENDIF
	ENDIF	
	IF g_sMPTunables.baward_low_tshirt_zombie_liberals_in_the_midwest   
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_ZOMBIE_LIBERALS_IN_THE_MIDWEST) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_ZOMBIE_LIBERALS_IN_THE_MIDWEST, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWTSHIRT3", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_003_F"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_003_M"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
		
		ENDIF
	ENDIF	

	IF g_sMPTunables.baward_low_tshirt_twilight_knife
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_TWILIGHT_KNIFE) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_TWILIGHT_KNIFE, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWTSHIRT4", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_006_F"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_006_M"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
		ENDIF
	ENDIF	
	IF g_sMPTunables.baward_low_tshirt_butchery_and_other_hobbies  
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_BUTCHERY) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_BUTCHERY, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWTSHIRT5", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_009_F"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_009_M"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
		ENDIF
	ENDIF	
	IF g_sMPTunables.baward_low_tshirt_bombshell_bloodbath_cheerleader_massacre_3 
			IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_BOMBSHELL_BLOODBATH_CHEERLEADER_MASSACRE_3) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_BOMBSHELL_BLOODBATH_CHEERLEADER_MASSACRE_3, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWTSHIRT6", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_012_F"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_012_M"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
		
		ENDIF
	ENDIF
	IF g_sMPTunables.baward_low_tshirt_cannibal_clown      
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_CANNIBAL_CLOWN) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_CANNIBAL_CLOWN, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWTSHIRT7", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_011_F"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_011_M"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
		
		ENDIF
	ENDIF	
	IF g_sMPTunables.baward_low_tshirt_hot_serial_killer_stepmom 
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_HOT_SERIAL_KILLER_STEPMOM) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_HOT_SERIAL_KILLER_STEPMOM, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWTSHIRT8", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_005_F"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_005_M"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
		ENDIF
	ENDIF	
	IF g_sMPTunables.baward_low_tshirt_splatter_and_shot   
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_SPLATTER_AND_SHOT) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_SPLATTER_AND_SHOT, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWTSHIRT9", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_000_F"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_000_M"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
		
		ENDIF
	ENDIF	
	IF g_sMPTunables.baward_low_tshirt_meathook_for_mommy     
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_MEATHOOK_FOR_MOMMY) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_MEATHOOK_FOR_MOMMY, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWTSHIRT10", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_004_F"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_004_M"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
		ENDIF
	ENDIF	
	IF g_sMPTunables.baward_low_tshirt_psycho_swingers     
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_PSYCHO_SWINGERS) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_PSYCHO_SWINGERS, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWTSHIRT11", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_002_F"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_002_M"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
		ENDIF
	ENDIF	
	IF g_sMPTunables.baward_low_tshirt_vampires_on_the_beach 
		IF GET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_VAMPIRES_ON_THE_BEACH) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_AWARD_LOW_TSHIRT_VAMPIRES_ON_THE_BEACH, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOKLOWTSHIRT12", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			IF NOT bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_010_F"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("HW_Tee_010_M"), PED_COMPONENT_ACQUIRED_SLOT, TRUE, bIsPlayerMale)
			ENDIF
		ENDIF
	ENDIF	
	
	



	IF GET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCK_PILOT_SCHOOL_REWARD_BAG) 
		IF GET_PACKED_STAT_BOOL(PACKED_MP_STAT_HAS_HIGH_FLYER_MESSAGE_DISPLAYED) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_HAS_HIGH_FLYER_MESSAGE_DISPLAYED, TRUE)
			IF NOT g_sMPTunables.bAWARD_HIGH_FLYER_CHUTE_BAG 
			
				SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BBSHIRTUN16", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,FALSE)
			ENDIF
			NET_PRINT("\n UNLOCK_SPECIAL_EVENT_ITEMS Unlock pilot reward bag - KEVIN WONG") NET_NL()
		ELSE
			NET_PRINT("\n UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_STAT_HAS_HIGH_FLYER_MESSAGE_DISPLAYED  = TRUE") NET_NL()
		ENDIF
	ELSE
	//	NET_PRINT("\n UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_STAT_UNLOCK_PILOT_SCHOOL_REWARD_BAG  = false") NET_NL()
	ENDIF
	
	IF GET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCK_PILOT_SCHOOL_REWARD_SHIRT) 
		IF GET_PACKED_STAT_BOOL(PACKED_MP_STAT_HAS_UNLOCK_PILOT_SCHOOL_REWARD_SHIRT) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_HAS_UNLOCK_PILOT_SCHOOL_REWARD_SHIRT, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_SHIRT15", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			NET_PRINT("\n UNLOCK_SPECIAL_EVENT_ITEMS Unlock pilot reward shirt - KEVIN WONG") NET_NL()
		ELSE
			NET_PRINT("\n UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_STAT_HAS_UNLOCK_PILOT_SCHOOL_REWARD_SHIRT  = TRUE") NET_NL()
		ENDIF
	ELSE
	//	NET_PRINT("\n UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_STAT_HAS_UNLOCK_PILOT_SCHOOL_REWARD_SHIRT  = false") NET_NL()
	ENDIF
		
	IF g_sMPTunables.bdlc_shirt_meltdown
		IF GET_PACKED_STAT_BOOL(PACKED_MP_FILM_MELTDOWN) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_FILM_MELTDOWN, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_SHIRTMELT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_DLC_SHIRT_MELTDOWN shirt unlocked ")
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_001_F"),TATTOO_MP_FM_F), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_001_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF

	
	
	IF g_sMPTunables.bdlc_shirt_vinewood_zombie
		IF GET_PACKED_STAT_BOOL(PACKED_MP_FILM_VINEWOOD_ZOMBIE) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_FILM_VINEWOOD_ZOMBIE, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_SHIRTZOMB", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_DLC_SHIRT_VINEWOOD_ZOMBIE shirt unlocked ")
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_000_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_000_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	
	IF g_sMPTunables.bdlc_shirt_i_married_my_dad
		IF GET_PACKED_STAT_BOOL(PACKED_MP_FILM_I_MARRIED_MY_DAD) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_FILM_I_MARRIED_MY_DAD, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_SHIRTMARRIED", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS gPACKED_MP_DLC_SHIRT_I_MARRIED_MY_DAD shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_002_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_002_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	
	IF g_sMPTunables.bdlc_shirt_die_already_4
		IF GET_PACKED_STAT_BOOL(PACKED_MP_FILM_DIE_ALREADY_4) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_FILM_DIE_ALREADY_4, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_SHIRTALREAD", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS PACKED_MP_DLC_SHIRT_DIE_ALREADY_4 shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_003_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_003_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	
	IF g_sMPTunables.bDLC_shirt_The_Shoulder_Of_Orion_II			 = TRUE
		
		SET_PACKED_STAT_BOOL(PACKED_MP_FILM_SHOULDER_OF_ORION, TRUE)
			
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FILM4SHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FILM4SHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_FILM4SRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS FILM4SHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_004_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_004_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	
	
	
	
	
	
	IF g_sMPTunables.bDLC_shirt_Nelson_In_Naples                     = TRUE
		
		SET_PACKED_STAT_BOOL(PACKED_MP_FILM_NELSON_IN_NAPLES, TRUE)
		
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FILM5SHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FILM5SHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_FILM5SRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS FILM5SHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_005_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_005_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bDLC_shirt_The_Many_Wives_of_Alfredo_Smith      = TRUE
		
		SET_PACKED_STAT_BOOL(PACKED_MP_FILM_ALFRED_SMITH, TRUE)
		
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FILM6SHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FILM6SHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_FILM6SRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS FILM6SHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_006_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_006_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	
	IF g_sMPTunables.bDLC_shirt_An_American_Divorce                  = TRUE
		
		SET_PACKED_STAT_BOOL(PACKED_MP_FILM_AMERICAN_DIVORCE, TRUE)
		
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FILM7SHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FILM7SHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_FILM7SRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS FILM7SHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_007_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_007_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	
	IF g_sMPTunables.bDLC_shirt_The_Loneliest_Robot                  = TRUE
		
		SET_PACKED_STAT_BOOL(PACKED_MP_FILM_LONLIEST_ROBOT, TRUE)
		
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FILM8SHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FILM8SHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_FILM8SRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS FILM8SHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_008_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_008_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	IF g_sMPTunables.bDLC_shirt_Capolavoro                           = TRUE
		
		SET_PACKED_STAT_BOOL(PACKED_MP_FILM_CAPOLAVORO, TRUE)
		
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FILM9SHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FILM9SHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_FILM9SRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS FILM9SHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_009_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_FILM_009_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	
	IF g_sMPTunables.bAccountantshirtEvent    = TRUE
		SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_ACCOUNTANT, TRUE)
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_ACCOUNTANTSHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_ACCOUNTANTSHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"ACCOUNTANTSHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS MP_STAT_ACCOUNTANTSHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_000_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_000_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bBahamaMamasshirtEvent    = TRUE
		SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_BAHAMAMAMAS, TRUE)
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_BAHAMAMAMASHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_BAHAMAMAMASHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"BAHAMAMAMASSHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS MP_STAT_BAHAMAMAMASHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_001_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_001_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bdronetshirtEvent    = TRUE
		SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_DRONE, TRUE)
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_DRONESHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_DRONESHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"DRONESHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS MP_STAT_DRONESHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_002_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_002_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bGrottitshirtEvent    = TRUE
		SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_GROTTI, TRUE)
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_GROTTISHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_GROTTISHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"GROTTISHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS MP_STAT_GROTTISHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_003_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_003_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bGolftshirtEvent    = TRUE
		SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_GOLF, TRUE)
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_GOLFSHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_GOLFSHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"GOLFSHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS MP_STAT_GOLFSHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_004_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_004_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	
	IF g_sMPTunables.bMAISONETTEtshirtEvent    = TRUE
		SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_MAISONETTE, TRUE)
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_MAISONETTESHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_MAISONETTESHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"MAISONETTESHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS MP_STAT_MAISONETTESHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_005_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_005_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF

	IF g_sMPTunables.bmanopausetshirtEvent    = TRUE
		SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_manopause, TRUE)
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_manopauseSHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_manopauseSHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"manopauseSHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS MP_STAT_manopauseSHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_006_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_006_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bMELTDOWNtshirtEvent    = TRUE
		SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_MELTDOWN, TRUE)
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_MELTDOWNSHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_MELTDOWNSHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"MELTDOWNSHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS MP_STAT_MELTDOWNSHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_008_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_008_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bPACIFICBLUFFStshirtEvent    = TRUE
		SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_PACIFICBLUFFS, TRUE)
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PACIFICBLUFFSSHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PACIFICBLUFFSSHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"PACIFICBLUFFSSHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS MP_STAT_PACIFICBLUFFSSHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_009_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_009_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bPROLAPStshirtEvent    = TRUE
		SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_PROLAPS, TRUE)
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_PROLAPSSHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_PROLAPSSHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"PROLAPSSHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS MP_STAT_PROLAPSSHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_010_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_010_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bTENNIStshirtEvent    = TRUE
		SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_TENNIS, TRUE)
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_TENNISSHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_TENNISSHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"TENNISSHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS MP_STAT_TENNISSHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_011_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_011_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bTOESHOEStshirtEvent    = TRUE
		SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_TOESHOES, TRUE)
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_TOESHOESSHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_TOESHOESSHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"TOESHOESSHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS MP_STAT_TOESHOESSHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_012_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_012_M"),TATTOO_MP_FM ), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bVANILLAUNICORNtshirtEvent    = TRUE
		SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_VANILLAUNICORN, TRUE)
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_VANILLAUNICORNSHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_VANILLAUNICORNSHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"VANILLAUNISHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS MP_STAT_VANILLAUNICORNSHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_014_M"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_014_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bMARLOWEtshirtEvent    = TRUE
		SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_MARLOWE, TRUE)
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_MARLOWESHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_MARLOWESHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"MARLOWESHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS MP_STAT_MARLOWESHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_007_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_007_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bCRESTtshirtEvent    = TRUE
		SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_CREST, TRUE)
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CRESTSHIRTUNLOCK) = FALSE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CRESTSHIRTUNLOCK, TRUE)
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CRESTSHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS MP_STAT_CRESTSHIRTUNLOCK shirt unlocked ")
			
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_013_F"),TATTOO_MP_FM_F ), TRUE, FALSE)
			ELSE
				SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(HASH("MP_exec_prizes_013_M"), TATTOO_MP_FM), TRUE, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	UNLOCK_BIKER_PACK_REWARDS()
	UNLOCK_GUNRUNNING_PACK_REWARDS()
	
	IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SEEKING_TRUTH_TOILET_TIP)
		//
	ENDIF
	IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CHASING_TRUTH_TOILET_TIP)
		PED_COMP_NAME_ENUM eMedalianItem	= GET_TOILET_ATTENTENT_TIP_REWARD_PED_COMP_NAME(PLAYER_PED_ID(), COMP_TYPE_TEETH)
		IF NOT IS_PED_COMP_ITEM_ACQUIRED_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_TEETH, eMedalianItem)
			SET_PED_COMP_ITEM_ACQUIRED_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_TEETH, eMedalianItem, TRUE)
			PRINTLN("<UNLOCK_SPECIAL_EVENT_ITEMS> [$$$] MAINTAIN_PAY_FOR_TOILET_ATTENDANT - aquired COMP_TYPE_TEETH ", eMedalianItem)
		ENDIF
	ENDIF
	IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BEARING_TRUTH_TOILET_TIP)
		PED_COMP_NAME_ENUM eRobesItem		= GET_TOILET_ATTENTENT_TIP_REWARD_PED_COMP_NAME(PLAYER_PED_ID(), COMP_TYPE_OUTFIT)
		IF NOT IS_PED_COMP_ITEM_ACQUIRED_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_OUTFIT, eRobesItem)
			SET_PED_COMP_ITEM_ACQUIRED_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_OUTFIT, eRobesItem, TRUE)
			PRINTLN("<UNLOCK_SPECIAL_EVENT_ITEMS> [$$$] MAINTAIN_PAY_FOR_TOILET_ATTENDANT - aquired COMP_TYPE_OUTFIT ", eRobesItem)
		ENDIF
	ENDIF
	
	
	IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_COLLECTED_IMPOTANT_RAGE_OUTFIT)
		PED_COMP_NAME_ENUM eImpRageItem		= GET_IMPOTENT_RAGE_OUTFIT_PED_COMP_NAME(PLAYER_PED_ID())
		IF NOT IS_PED_COMP_ITEM_ACQUIRED_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_OUTFIT, eImpRageItem)
			SET_PED_COMP_ITEM_ACQUIRED_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_OUTFIT, eImpRageItem, TRUE)
			PRINTLN("<UNLOCK_SPECIAL_EVENT_ITEMS> [$$$] MAINTAIN_IMPOTENT_RAGE_OUTFIT - aquired COMP_TYPE_OUTFIT ", eImpRageItem)
		ENDIF
	ENDIF
	
ENDPROC
  


PROC UNLOCK_SPECIAL_EVENT_ITEMS(BOOL bdisplay_ticker = FALSE )
	//Set a bool to see if the player is male

	// LTS event awards
	
		

	
	IF g_award_event_tshirt
		//SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_AWD_SHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		g_award_event_tshirt = FALSE
		
		PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS Unlocked event t shirt KW g_award_event_tshirt_TYPE =  ", ENUM_TO_INT(g_award_event_tshirt_TYPE))
		STRING event_shirt_unlock_type
		
		IF g_award_event_tshirt_TYPE = COMP_TYPE_BERD
			event_shirt_unlock_type = "UNLOCK_AWD_MASK"
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS g_award_event_tshirt_TYPE = UNLOCK_AWD_MASK ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,event_shirt_unlock_type, GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
		ELIF g_award_event_tshirt_TYPE = COMP_TYPE_OUTFIT
			event_shirt_unlock_type = "UNLOCK_AWD_OUTFIT"
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS g_award_event_tshirt_TYPE = UNLOCK_AWD_OUTFIT ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,event_shirt_unlock_type, GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
		ELIF g_award_event_tshirt_TYPE = COMP_TYPE_PROPS
			event_shirt_unlock_type = "UNLOCK_AWD_ACCES"
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS g_award_event_tshirt_TYPE = COMP_TYPE_PROPS ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,event_shirt_unlock_type, GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
		ELSE
			event_shirt_unlock_type = "UNLOCK_AWD_SHIRT"
			PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS g_award_event_tshirt_TYPE = UNLOCK_AWD_SHIRT ")
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,event_shirt_unlock_type, GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF
		
		g_award_event_tshirt_TYPE = COMP_TYPE_HEAD // reset type
		
	ENDIF
	
	IF g_award_event_ammo_LTS
		g_award_event_ammo_LTS = FALSE
		// PRINT_TICKER_WITH_FIVE_INTS("UNLOCK_AWD_AMMO1", g_sMPTunables.iSHOTGUN_AMMO_GIFTING, g_sMPTunables.iRIFLE_AMMO_GIFTING, g_sMPTunables.iSNIPER_AMMO_GIFTING ,g_sMPTunables.iGRENADE_AMMO_GIFTING	, g_sMPTunables.iSTKYBMB_AMMO_GIFTING	)
		//PRINT_TICKER_WITH_SIX_INTS("UNLOCK_AWD_AMMO1", g_sMPTunables.iSHOTGUN_AMMO_GIFTING, g_sMPTunables.iRIFLE_AMMO_GIFTING, g_sMPTunables.iSNIPER_AMMO_GIFTING ,g_sMPTunables.iGRENADE_AMMO_GIFTING	,g_sMPTunables.iMOLOTOV_AMMO_GIFTING, g_sMPTunables.iSTKYBMB_AMMO_GIFTING	)
		NET_PRINT("\n UNLOCK_SPECIAL_EVENT_ITEMS Unlocked event AMMO KW ") NET_NL()
		DISPLAY_AMMO_REWARD_FOR_PLAYLIST_MESSAGE()
	ENDIF
	
	IF g_award_event_heist_complete_strand_bonus
		g_award_event_heist_complete_strand_bonus = FALSE
		GIVE_LOCAL_PLAYER_FM_CASH(g_sMPTunables.iheist_First_Time_Bonus)
		PRINTLN("[MJM] AWARD - COMPLETE STRAND - GIVE CASH")
		//give cash
	ENDIF
	
	IF g_award_event_heist_flow_order
		g_award_event_heist_flow_order = FALSE
		GIVE_LOCAL_PLAYER_FM_CASH(g_sMPTunables.iheist_Order_Bonus)
		PRINTLN("[MJM] AWARD - FLOW ORDER - GIVE CASH")
		//give cash
	ENDIF
	
	IF g_award_event_heist_same_team
		g_award_event_heist_same_team = FALSE
		GIVE_LOCAL_PLAYER_FM_CASH(g_sMPTunables.iheist_Same_Team_Bonus)
		PRINTLN("[MJM] AWARD - SAME TEAM - GIVE CASH")
		//give cash
	ENDIF
	
	IF g_award_event_heist_ultimate_challenge
		g_award_event_heist_ultimate_challenge = FALSE
		GIVE_LOCAL_PLAYER_FM_CASH(g_sMPTunables.iheist_Ultimate_Challenge)
		PRINTLN("[MJM] AWARD - ULTIMATE CHALLENGE - GIVE CASH")
		//give cash
	ENDIF
	IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
		UNLOCK_SPECIAL_TUNABLE_ITEMS()
	ENDIF
	IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCKED_MESSAGE_HUMANE)
		IF 	GET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCKED_FLAREGUN )
		AND GET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCKED_REBREATHER )
		AND GET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCKED_NIGHTVISION)
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCKED_MESSAGE_HUMANE, TRUE)
			IF bdisplay_ticker
				PRINT_TICKER("UNLOCK_HUMANE")
			ENDIF
		ENDIF
	ENDIF

	IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCKED_MESSAGE_FLEECA)
	OR NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_UNLOCKED_MESSAGE_FLEECA)
		IF 	GET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCKED_SKI_MASK )
		AND GET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCKED_GAS_MASK )
		AND GET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCKED_LCD_EARPIECE)
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCKED_MESSAGE_FLEECA, TRUE)
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_UNLOCKED_MESSAGE_FLEECA, TRUE)
			IF bdisplay_ticker
				PRINT_TICKER("UNLOCK_FLEECA")
				PRINTLN("2234067, bdisplay_ticker, UNLOCK_FLEECA ")
			ENDIF
		ELSE
			PRINTLN("2234067, PACKED_MP_STAT_UNLOCKED_SKI_MASK ")
		ENDIF
	ELSE
		PRINTLN("2234067, PACKED_MP_STAT_UNLOCKED_MESSAGE_FLEECA ")
	ENDIF

ENDPROC

#ENDIF




PROC DLC_MP_PATCH_UPDATES_FOR_DLC_CONTENT()
	// Clothing updates for new patch - Kenneth's stuff.
	BOOL bInitialPatchChecks = (g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth = 0)
	//////////////////////////////
	///      BUSINESS 1 PATCH
	IF IS_MP_BUSINESS_PACK_PRESENT()
		IF NOT IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth, 0)
			
			// Need to set a few items as acquired again to pick up DLC changes.
			SWITCH GET_ENTITY_MODEL(PLAYER_PED_ID())
				CASE MP_M_FREEMODE_01
					// Check Shirts
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_0)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_0, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_1)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_1, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_2)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_2, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_3)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_3, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_4)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_4, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_5)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_5, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_6)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_6, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_7)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_7, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_8)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_8, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_9)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_9, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_10)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_10, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_11)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_11, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_12)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_12, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_13)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_13, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_14)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_14, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_15)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_15, TRUE)
					ENDIF
					
					// Ties
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_TEETH, TEETH_FMM_10_0)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_TEETH, TEETH_FMM_10_0, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_TEETH, TEETH_FMM_10_1)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_TEETH, TEETH_FMM_10_1, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_TEETH, TEETH_FMM_10_2)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_TEETH, TEETH_FMM_10_2, TRUE)
					ENDIF
					
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_TEETH, TEETH_FMM_12_0)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_TEETH, TEETH_FMM_12_0, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_TEETH, TEETH_FMM_12_1)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_TEETH, TEETH_FMM_12_1, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_TEETH, TEETH_FMM_12_2)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_TEETH, TEETH_FMM_12_2, TRUE)
					ENDIF
				BREAK
				CASE MP_F_FREEMODE_01
					// Camisoles
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_F_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMF_12_7)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_F_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMF_12_7, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_F_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMF_12_8)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_F_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMF_12_8, TRUE)
					ENDIF
					IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_F_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMF_12_9)
						SET_PED_COMP_ITEM_ACQUIRED_MP(MP_F_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMF_12_9, TRUE)
					ENDIF
				BREAK
			ENDSWITCH
			
			PRINTLN("[KR PATCH] - Patching data in Business 1 release")
			SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth, 0)
		ENDIF
	ELSE
		CLEAR_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth, 0)
	ENDIF
	
	//////////////////////////////
	///      CTF CREATOR PATCH
	IF NOT IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth, 1)
		
		// Need to set a few items as available if we set the DLC items as acquired.
		SWITCH GET_ENTITY_MODEL(PLAYER_PED_ID())
			CASE MP_M_FREEMODE_01
				// Check Shirts
				IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_0)
					SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_0, TRUE)
				ENDIF
				IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_1)
					SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_1, TRUE)
				ENDIF
				IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_2)
					SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_2, TRUE)
				ENDIF
				IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_3)
					SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_3, TRUE)
				ENDIF
				IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_4)
					SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_4, TRUE)
				ENDIF
				IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_5)
					SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_5, TRUE)
				ENDIF
				IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_6)
					SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_6, TRUE)
				ENDIF
				IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_7)
					SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_7, TRUE)
				ENDIF
				IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_8)
					SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_8, TRUE)
				ENDIF
				IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_9)
					SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_9, TRUE)
				ENDIF
				IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_10)
					SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_10, TRUE)
				ENDIF
				IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_11)
					SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_11, TRUE)
				ENDIF
				IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_12)
					SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_12, TRUE)
				ENDIF
				IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_13)
					SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_13, TRUE)
				ENDIF
				IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_14)
					SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_14, TRUE)
				ENDIF
				IF IS_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_15)
					SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_14_15, TRUE)
				ENDIF
			BREAK
		ENDSWITCH
		
		// Give player Gusenberg back
		IF IS_MP_WEAPON_PURCHASED(WEAPONTYPE_UNARMED)
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_GUSENBERG, TRUE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_GUSENBERG, TRUE)
			
			// Set all the default components as equipped
			SET_MP_WEAPON_ADDON_EQUIPPED(WEAPONCOMPONENT_DLC_GUSENBERG_CLIP_01, WEAPONTYPE_DLC_GUSENBERG, TRUE)
			SET_MP_WEAPON_ADDON_PURCHASED(WEAPONCOMPONENT_DLC_GUSENBERG_CLIP_01, WEAPONTYPE_DLC_GUSENBERG, TRUE)
			
			// Cleanup the unarmed stats.
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_UNARMED, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_UNARMED, FALSE)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_GUSENBERG, 250)
			ENDIF
			
			PRINTLN("[KR PATCH] - Giving player the Gusenberg back")
		ENDIF
		
		PRINTLN("[KR PATCH] - Patching data in CTF Creator release")
		SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth, 1)
	ENDIF
	
	//////////////////////////////
	///      BUSINESS 2 PATCH
	IF IS_MP_BUSINESS_2_PACK_PRESENT()
		IF NOT IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth, 2)
		
			PRINTLN("[KR PATCH] - Patching data in Business 2 release")
			SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth, 2)
		ENDIF
	ELSE
		CLEAR_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth, 2)
	ENDIF
	
	////////////////////////////////
	///      INDEPENDENCE DAY PATCH
	IF IS_MP_INDEPENDENCE_PACK_PRESENT()
		IF NOT IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth, 3)
		
			// Flag current tattoos as purchased.
			TATTOO_DATA_STRUCT sTattooData
			TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
			
			INT i
			REPEAT MAX_NUMBER_OF_TATTOOS i
				IF GET_TATTOO_DATA(sTattooData, INT_TO_ENUM(TATTOO_NAME_ENUM, i), eFaction, PLAYER_PED_ID())
					IF IS_MP_TATTOO_CURRENT(INT_TO_ENUM(TATTOO_NAME_ENUM, i))
						SET_MP_TATTOO_PURCHASED(INT_TO_ENUM(TATTOO_NAME_ENUM, i), TRUE)
					ENDIF
				ENDIF
			ENDREPEAT
			
			#IF USE_TU_CHANGES
			INT iDLCIndex
			INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
			sTattooShopItemValues sDLCTattooData
			TATTOO_NAME_ENUM eDLCTattoo
			
			REPEAT iDLCCount iDLCIndex
				IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
					IF NOT IS_CONTENT_ITEM_LOCKED(sDLCTattooData.m_lockHash)
						eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex)
						IF IS_MP_TATTOO_CURRENT(eDLCTattoo)
							SET_MP_TATTOO_PURCHASED(eDLCTattoo, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			#ENDIF
			
			// Give player items to work with tailcoat
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
				SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_LEGS, LEGS_FMM_0_0, TRUE)
				SET_PED_COMP_ITEM_ACQUIRED_MP(MP_M_FREEMODE_01, COMP_TYPE_JBIB, JBIB_FMM_5_0, TRUE)
			ENDIF
		
			PRINTLN("[KR PATCH] - Patching data in Independence Day release")
			SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth, 3)
		ENDIF
	ELSE
		CLEAR_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth, 3)
	ENDIF
	
	////////////////////////////////
	///      PILOT SCHOOL
	IF IS_MP_PILOT_SCHOOL_PACK_PRESENT()
		IF NOT IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth, 4)
		
			SET_PACKED_STAT_INT(PACKED_MP_STAT_RESERVE_PARACHUTE_TINT, 5)
			
			
			INT iVehSlot
			REPEAT MAX_MP_SAVED_VEHICLES iVehSlot
				IF g_MpSavedVehicles[iVehSlot].vehicleSetupMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
				AND g_MpSavedVehicles[iVehSlot].iPricePaid = 0
				AND IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS,MP_SAVED_VEHICLE_BOUGHT_ONLINE)
				AND NOT IS_BIT_SET(g_MpSavedVehicles[iVehSlot].iVehicleBS,MP_SAVED_VEHICLE_FREE_VEHICLE)
					PRINTLN("[BGSCRIPT] FIX_FOR_1966121 - Vehicle in slot [", iVehSlot, "] was bought online for free, set the flag!")
					SET_BIT(g_MpSavedVehicles[iVehSlot].iVehicleBS,MP_SAVED_VEHICLE_FREE_VEHICLE)
				ENDIF
			ENDREPEAT
		
			PRINTLN("[KR PATCH] - Patching data in Pilot School release")
			SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth, 4)
		ENDIF
	ELSE
		CLEAR_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth, 4)
	ENDIF
	
	////////////////////////////////
	///      LTS CREATOR
	IF IS_MP_LTS_PACK_PRESENT()
	
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_STAT_PATCH_UPDATE_LTS)
			/*
				BEFORE PILOT SCHOOL PACK
				========================
				WEAPON_DLC_VPISTOL_BIT_FIELD 	= 52 : stat=MP_STAT_CHAR_WEAP_FM_PURCHASE2, bit=20
				WEAPON_DLC_FLAREGUN_BIT_FIELD 	= 53 : stat=MP_STAT_CHAR_WEAP_FM_PURCHASE2, bit=21
				WEAPON_DLC_MUSKET_BIT_FIELD 	= 54 : stat=MP_STAT_CHAR_WEAP_FM_PURCHASE2, bit=22
				WEAPON_DLC_FIREWORK_BIT_FIELD 	= 55 : stat=MP_STAT_CHAR_WEAP_FM_PURCHASE2, bit=23
				
				AFTER PILOT SCHOOL PACK
				========================
				WEAPON_DLC_VPISTOL_BIT_FIELD 	= 52 : stat=MP_STAT_CHAR_WEAP_FM_PURCHASE2, bit=20
				WEAPON_DLC_MUSKET_BIT_FIELD 	= 53 : stat=MP_STAT_CHAR_WEAP_FM_PURCHASE2, bit=21
				WEAPON_DLC_FIREWORK_BIT_FIELD 	= 54 : stat=MP_STAT_CHAR_WEAP_FM_PURCHASE2, bit=22
				
				To fix this we need to copy the value of bit 23->22 and 22->21
				We can use bit 31 to check if this has been patched - we will then clear this out in the following TU.
			
			*/
			
			#IF NOT FEATURE_GEN9_STANDALONE
			INT iStatOriginal = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_WEAP_FM_PURCHASE2)
			INT iStatUpdated = iStatOriginal
			
			// Flatten the data in the slots we are going to update
			CLEAR_BIT(iStatUpdated, 21)
			CLEAR_BIT(iStatUpdated, 22)
			CLEAR_BIT(iStatUpdated, 23)
			CLEAR_BIT(iStatUpdated, 31) // We set this in the BG script
			
			// Now copy in the correct values from the original stats
			IF IS_BIT_SET(iStatOriginal, 22)
				SET_BIT(iStatUpdated, 21)
				PRINTLN("...Player purchased the Musket so we are giving it back to them!")
			ENDIF
			IF IS_BIT_SET(iStatOriginal, 23)
				SET_BIT(iStatUpdated, 22)
				PRINTLN("...Player purchased the Firework Launcher so we are giving it back to them!")
			ENDIF
			
			// Store this back in the stat
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_WEAP_FM_PURCHASE2, iStatUpdated)
			#ENDIF
			
			// Fix for 1990641 - 'Free' clothes need to create a spend event
			// Players no longer get items that cost $0 automatically, instead they have to acquire from the shop.
			// To avoid confusion with existing players we need to give all the beach bum items.
			IF NOT bInitialPatchChecks
				IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_FEET0_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_FEET0_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_FEET0_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_FEET0_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_FEET0_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_FEET0_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_FEET0_6"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_FEET0_7"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_FEET0_8"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_FEET0_9"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_FEET0_10"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_FEET0_11"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR0_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR0_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR0_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR0_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR0_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR0_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR0_6"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR0_7"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR0_8"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR0_9"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR0_10"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR0_11"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR1_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR1_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR1_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR1_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR1_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR1_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR1_6"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR1_7"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR1_8"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR1_9"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR1_10"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR2_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR2_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR2_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR2_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR2_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR2_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR2_6"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR2_7"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR2_8"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR2_9"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR2_10"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_LOWR2_11"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_TEETH0_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_TEETH0_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_TEETH0_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_TEETH1_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_TEETH1_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_TEETH1_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_JBIB0_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_JBIB0_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_JBIB0_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_JBIB1_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_JBIB1_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_JBIB1_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_JBIB1_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_JBIB1_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_JBIB1_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_ACCS0_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_ACCS0_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_ACCS0_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_ACCS1_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_ACCS1_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_ACCS1_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_ACCS1_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_ACCS1_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_ACCS1_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_ACCS2_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_ACCS2_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_ACCS2_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_EYES0_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_EYES0_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_EYES0_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_EYES0_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_EYES0_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_EYES0_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_EYES0_6"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_HEAD0_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_HEAD0_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_HEAD0_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_HEAD0_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_HEAD0_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_HEAD0_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_HEAD1_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_HEAD1_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_HEAD1_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_HEAD1_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_HEAD1_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_HEAD1_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_HEAD1_6"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_HEAD1_7"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_WATCH_1_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_WATCH_1_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_WATCH_1_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_WATCH_1_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_M_WATCH_1_4"), PED_COMPONENT_ACQUIRED_SLOT)
				ELIF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB0_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB0_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB0_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB0_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB0_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB0_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB0_6"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB1_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB2_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB2_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB2_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB2_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB2_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB2_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB2_6"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB2_7"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB2_8"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB2_9"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB2_10"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_JBIB2_11"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS0_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS0_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS0_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS0_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS0_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS0_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS0_6"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS2_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS2_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS2_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS2_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS2_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS2_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS2_6"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS2_7"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS2_8"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS2_9"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS2_10"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_ACCS2_11"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_TEETH0_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_TEETH0_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_TEETH0_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_TEETH0_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_TEETH1_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_TEETH1_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_TEETH1_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_TEETH1_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR6"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR7"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR8"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR9"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR10"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR11"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR100"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR101"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR102"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR103"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR104"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR105"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR106"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR107"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR108"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR109"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR110"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_LOWR111"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_FEET000"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_FEET001"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_FEET002"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_FEET003"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_FEET004"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_FEET005"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_FEET006"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_FEET007"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_FEET008"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_FEET009"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_FEET010"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_FEET011"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_EYES0_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_EYES0_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_EYES0_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_EYES0_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_EYES0_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_EYES0_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_EYES0_6"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_EYES1_"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_EYES1_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_EYES1_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_EYES1_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_EYES1_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_EYES1_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_EYES1_6"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD2_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD2_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD2_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD2_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD2_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD2_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD2_6"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD3_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD3_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD3_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD3_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD3_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD3_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD3_6"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD4_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD4_1"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD4_2"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD4_3"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD4_4"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD4_5"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_BEACH_F_HEAD4_6"), PED_COMPONENT_ACQUIRED_SLOT)
				ENDIF
			ENDIF
			
			PRINTLN("[KR PATCH] - Patching data in LTS Creator release")
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PATCH_UPDATE_LTS, TRUE)
		ENDIF
	ELSE
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PATCH_UPDATE_LTS, FALSE)
	ENDIF
	
	////////////////////////////////
	///      HEIST PACK
	IF IS_MP_HEIST_PACK_PRESENT()
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_STAT_PATCH_UPDATE_HEIST)
			// New untucked shirts have been added for male characters so we need to ensure they are acquired
			IF IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_HIPS_M_ACCS6_0"), PED_COMPONENT_ACQUIRED_SLOT)
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_HEIST_M_SPECIAL_8_12"), PED_COMPONENT_ACQUIRED_SLOT)
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_HEIST_M_SPECIAL_9_12"), PED_COMPONENT_ACQUIRED_SLOT)
			ENDIF
			IF IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_HIPS_M_ACCS6_1"), PED_COMPONENT_ACQUIRED_SLOT)
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_HEIST_M_SPECIAL_8_13"), PED_COMPONENT_ACQUIRED_SLOT)
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_HEIST_M_SPECIAL_9_13"), PED_COMPONENT_ACQUIRED_SLOT)
			ENDIF
			IF IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_HIPS_M_ACCS6_2"), PED_COMPONENT_ACQUIRED_SLOT)
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_HEIST_M_SPECIAL_8_14"), PED_COMPONENT_ACQUIRED_SLOT)
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_HEIST_M_SPECIAL_9_14"), PED_COMPONENT_ACQUIRED_SLOT)
			ENDIF
			IF IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_HIPS_M_ACCS6_3"), PED_COMPONENT_ACQUIRED_SLOT)
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_HEIST_M_SPECIAL_8_15"), PED_COMPONENT_ACQUIRED_SLOT)
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_HEIST_M_SPECIAL_9_15"), PED_COMPONENT_ACQUIRED_SLOT)
			ENDIF
			
			// Pilot outfit parts now sold separately so make sure players who bought it don't need to re-buy.
			IF IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_PILOT_M_OUTFIT_0"), PED_COMPONENT_ACQUIRED_SLOT)
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_PILOT_M_LEGS_0_0"), PED_COMPONENT_ACQUIRED_SLOT)
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_PILOT_M_FEET_0_0"), PED_COMPONENT_ACQUIRED_SLOT)
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_PILOT_M_JBIB_0_0"), PED_COMPONENT_ACQUIRED_SLOT)
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_PILOT_M_TEETH_0_0"), PED_COMPONENT_ACQUIRED_SLOT)
			ENDIF
			IF IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_PILOT_F_OUTFIT_0"), PED_COMPONENT_ACQUIRED_SLOT)
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_PILOT_F_LEGS_0_0"), PED_COMPONENT_ACQUIRED_SLOT)
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_PILOT_F_FEET_0_0"), PED_COMPONENT_ACQUIRED_SLOT)
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_PILOT_F_JBIB_0_0"), PED_COMPONENT_ACQUIRED_SLOT)
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_PILOT_F_TEETH_0_0"), PED_COMPONENT_ACQUIRED_SLOT)
			ENDIF
			
			PRINTLN("[KR PATCH] - Patching data in Heist release")
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PATCH_UPDATE_HEIST, TRUE)
		ENDIF
	ELSE
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PATCH_UPDATE_HEIST, FALSE)
	ENDIF

	
	////////////////////////////////
	///      XMAS 2014 PACK to Heast Weapons patch. 
	///      Proximity mine and Homing missle purchase bitset fields have to be resynced to NG
	///      
	
	IF IS_MP_HEIST_PACK_PRESENT()
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_STAT_PATCH_UPDATE_XMAS_HEIST_WEAPONS)
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PATCH_UPDATE_XMAS_HEIST_WEAPONS, TRUE)
			// Just patch weapons in CG				
			#IF NOT IS_NEXTGEN_BUILD	
				
				/*
				BEFORE HEIST PACK IN CG
				========================
				WEAPON_DLC_HOMINGLAUNCH_BIT_FIELD	= 57 : stat=MP_STAT_CHAR_WEAP_FM_PURCHASE2, bit=25
				WEAPON_DLC_PROXMINE_BIT_FIELD 	    = 58 : stat=MP_STAT_CHAR_WEAP_FM_PURCHASE2, bit=26
				WEAPON_DLC_FLAREGUN_BIT_FIELD	    = 59 : stat=MP_STAT_CHAR_WEAP_FM_PURCHASE2, bit=27
				
				AFTER HEIST PACK IN CG
				========================
				WEAPON_DLC_FLAREGUN_BIT_FIELD,		= 57		
				WEAPON_DLC_HATCHET_BIT_FIELD, 		= 58		
				WEAPON_DLC_RAILGUN_BIT_FIELD, 		= 59		
				WEAPON_DLC_PROXMINE_BIT_FIELD,		= 60
				WEAPON_DLC_HOMINGLAUNCH_BIT_FIELD	= 61
								
				To fix this we need to copy the value of bit 23->22 and 22->21
				We can use bit 31 to check if this has been patched - we will then clear this out in the following TU.
			
			*/
				IF IS_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FLAREGUN)
				OR 	(GET_MP_INT_CHARACTER_STAT(MP_STAT_HOMLNCH_FM_AMMO_BOUGHT) > 0
				AND GET_MP_INT_CHARACTER_STAT(MP_STAT_HOMLNCH_FM_AMMO_CURRENT) > 0)
					SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_HOMINGLAUNCHER, TRUE)
					SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_HOMINGLAUNCHER, TRUE)

					// Cleanup the unarmed stats.
					IF GET_MP_INT_CHARACTER_STAT(MP_STAT_FLAREGUN_FM_AMMO_BOUGHT) = 0
					AND GET_MP_INT_CHARACTER_STAT(MP_STAT_FLAREGUN_FM_AMMO_CURRENT) = 0
						SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FLAREGUN, FALSE)
						SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FLAREGUN, FALSE)
					ENDIF	
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_HOMINGLAUNCHER, GET_MP_INT_CHARACTER_STAT(MP_STAT_HOMLNCH_FM_AMMO_CURRENT))
					ENDIF
					PRINTLN("[KR PATCH] [KW PATCH] - Patching data in XMAS 2014 release. Returning WEAPONTYPE_DLC_HOMINGLAUNCHER")
				ENDIF

				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_BOUGHT) > 0
				AND GET_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_CURRENT) > 0
					SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_PROXMINE, TRUE)
					SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_PROXMINE, TRUE)
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_PROXMINE, GET_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_CURRENT))
					ENDIF
					PRINTLN("[KR PATCH] [KW PATCH] - Patching data in XMAS 2014 release. Returning WEAPONTYPE_DLC_HATCHET")
				ENDIF
			

			#ENDIF
		ENDIF
	ENDIF
	
	////////////////////////////////
	///      STUNT PACK
	IF IS_MP_STUNT_PACK_PRESENT()
		// Switch over to the new mask and helmets
		INT iLoop
		FOR iLoop = 0 TO (MAX_CUSTOM_OUTFIT_SLOTS_FOR_PLAYER-1)
			IF IS_MP_OUTFIT_STORED_IN_SLOT(iLoop)
				IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSavedOutfit_CompDraw[iLoop][PED_COMP_BERD] = 28
					// Same index values for male and female
					PRINTLN("[KR PATCH] - Updating LTS mask stored in outfit slot ", iLoop)
					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSavedOutfit_CompDraw[iLoop][PED_COMP_BERD] = 89
				ENDIF
				
				IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSavedOutfit_PropID[iLoop][ANCHOR_HEAD] = 18
					// Same index values for male and female
					PRINTLN("[KR PATCH] - Updating Helmet stored in outfit slot ", iLoop)
					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iSavedOutfit_PropID[iLoop][ANCHOR_HEAD] = 81
				ENDIF
			ENDIF
		ENDFOR
		
		IF GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD) = 28
			PRINTLN("[KR PATCH] - Updating LTS mask on player")
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 89, GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD))
		ENDIF
		
		IF GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD) = 18
			PRINTLN("[KR PATCH] - Updating Helmet on player")
			SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD, 81)
		ENDIF
	ENDIF
	
	////////////////////////////////
	///      SMUGGLER PACK
	IF IS_MP_SMUGGLER_PACK_PRESENT()
	
		IF IS_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_HEAVYSNIPER_MK2)
			SET_MP_WEAPON_ADDON_UNLOCKED(WEAPONCOMPONENT_AT_SCOPE_MAX, WEAPONTYPE_DLC_HEAVYSNIPER_MK2, TRUE)
			SET_MP_WEAPON_ADDON_PURCHASED(WEAPONCOMPONENT_AT_SCOPE_MAX, WEAPONTYPE_DLC_HEAVYSNIPER_MK2, TRUE)
		ENDIF
		
				
		IF NOT IS_BIT_SET(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth, 5)
			IF IS_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_HEAVYSNIPER_MK2) 
				IF IS_MP_WEAPON_ADDON_PURCHASED(WEAPONCOMPONENT_INVALID, WEAPONTYPE_DLC_HEAVYSNIPER_MK2)
					SET_MP_WEAPON_ADDON_UNLOCKED(WEAPONCOMPONENT_DLC_AT_SCOPE_LARGE_MK2, WEAPONTYPE_DLC_HEAVYSNIPER_MK2, TRUE)
					SET_MP_WEAPON_ADDON_PURCHASED(WEAPONCOMPONENT_DLC_AT_SCOPE_LARGE_MK2, WEAPONTYPE_DLC_HEAVYSNIPER_MK2, TRUE)
					PRINTLN("[KR PATCH] - restore WEAPONCOMPONENT_DLC_AT_SCOPE_LARGE_MK2 ")
				ENDIF
			ENDIF
			SET_BIT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iPatchUpdatesForKenneth, 5)
		ENDIF
	ENDIF
ENDPROC


PROC SET_PLAYERS_HAIR_STAT_ON_PC() //Fix for 2291259 [PUBLIC][REPORT] My female character's hair is a different style while on Heist missions/cutscenes.

	INT packed_stat_hair_value =  GET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT)
	INT stat_hair_value = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA)
	
	PRINTLN("SET_PLAYERS_HAIR_STAT_ON_PC packed_stat_hair_value (PACKED_CHAR_CURRENT_HAIRCUT)= ", packed_stat_hair_value) 
	PRINTLN("SET_PLAYERS_HAIR_STAT_ON_PC stat_hair_value (MP_STAT_CHAR_FM_STORED_HAIRDO_SA)= ", stat_hair_value ) 

	IF (INT_TO_ENUM(PED_COMP_NAME_ENUM, packed_stat_hair_value) != DUMMY_PED_COMP)
		PED_COMP_NAME_ENUM eGRHairItem = DUMMY_PED_COMP
		IF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01)
			eGRHairItem = GET_MALE_HAIR(INT_TO_ENUM(PED_COMP_NAME_ENUM, packed_stat_hair_value))
		ELIF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01)
			eGRHairItem = GET_FEMALE_HAIR(INT_TO_ENUM(PED_COMP_NAME_ENUM, packed_stat_hair_value))
		ENDIF
		
		IF (eGRHairItem != DUMMY_PED_COMP)
		AND (INT_TO_ENUM(PED_COMP_NAME_ENUM, packed_stat_hair_value) != eGRHairItem)
			CPRINTLN(DEBUG_SHOPS,"SET_PLAYERS_HAIR_STAT_ON_PC gr_hair: replacing ", PICK_STRING(GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01, "male", "female"), " hair packstat ", packed_stat_hair_value, " with gunrunning hair packstat ", eGRHairItem)
			packed_stat_hair_value = ENUM_TO_INT(eGRHairItem)
		ENDIF
	ENDIF 

	IF IS_PC_VERSION()
		IF GET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT) !=0	// fix for missing hair bug 2247698 KW 10/3/15
			IF  packed_stat_hair_value !=	stat_hair_value
			
				SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO,packed_stat_hair_value)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA,packed_stat_hair_value)
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_USING_HAIR_SA_STAT, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL IS_ON_ACTIVITY_SHOULD_BLOCK_CLOTHING_SAVES()

		IF NOT IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())  
		AND NOT Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())
		AND FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) != FMMC_TYPE_HUNT_THE_BEAST
		AND NOT IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY() 
		AND NOT IS_THIS_TRANSFORM_INFERNO()
		AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_fmmc_struct.iAdversaryModeType)
		AND NOT CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
		AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyEight, ciOptionsBS28_PreventClothingBlock)
			#IF IS_DEBUG_BUILD
			PRINTLN("IS_ON_ACTIVITY_SHOULD_BLOCK_CLOTHING_SAVES is FALSE")
			#ENDIF
		
			RETURN FALSE
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("IS_ON_ACTIVITY_SHOULD_BLOCK_CLOTHING_SAVES is TRUE")
				
				IF IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())  
					PRINTLN("IS_ON_ACTIVITY_SHOULD_BLOCK_CLOTHING_SAVES IS_PLAYER_PLAYING_OR_PLANNING_HEIST = TRUE")
				ENDIF
				IF Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())  
					PRINTLN("IS_ON_ACTIVITY_SHOULD_BLOCK_CLOTHING_SAVES Is_Player_Currently_On_MP_Versus_Mission = TRUE")
				ENDIF	
				
				IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID())  = FMMC_TYPE_HUNT_THE_BEAST
					PRINTLN("IS_ON_ACTIVITY_SHOULD_BLOCK_CLOTHING_SAVES FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_HUNT_THE_BEAST")
				ENDIF	
				
				IF IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY()  
					PRINTLN("IS_ON_ACTIVITY_SHOULD_BLOCK_CLOTHING_SAVES IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY = TRUE")
				ENDIF
				
				IF IS_THIS_TRANSFORM_INFERNO()  
					PRINTLN("IS_ON_ACTIVITY_SHOULD_BLOCK_CLOTHING_SAVES IS_THIS_TRANSFORM_INFERNO = TRUE")
				ENDIF
				
				IF 	IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_fmmc_struct.iAdversaryModeType)  
					PRINTLN("IS_ON_ACTIVITY_SHOULD_BLOCK_CLOTHING_SAVES IS_THIS_ROCKSTAR_MISSION_NEW_VS_SLASHERS(g_fmmc_struct.iAdversaryModeType) ")
				ENDIF
				
				IF CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE()
					PRINTLN("IS_ON_ACTIVITY_SHOULD_BLOCK_CLOTHING_SAVES CASINO_HEIST_FLOW_IS_CURRENT_MISSION_CASINO_HEIST_FINALE ")
				ENDIF
				
			#ENDIF
			RETURN TRUE
			
		ENDIF
ENDFUNC

#IF FEATURE_GEN9_EXCLUSIVE
PROC FIX_HSW_SA_STAT()
	IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_STAT_FIX_HSW_UPGRADE_GIFT_STAT)
		INT iStatValue = GET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_HSW_UPGRADE_GIFT)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_DISCOUNT_HSW_UPG_GIFT_NEW, iStatValue)
		SET_PACKED_STAT_BOOL(PACKED_MP_STAT_FIX_HSW_UPGRADE_GIFT_STAT, TRUE)
		PRINTLN("FIX_HSW_SA_STAT iStatValue: ", iStatValue)
	ENDIF
ENDPROC
#ENDIF

PROC SET_PLAYER_ATTRIBUTES_AFTER_SPAWN()

	BOOL bBulletProof = FALSE
	BOOL bFlameProof = FALSE // changed 22/1/13 - les now wants players to die from flames
	BOOL bExplosionProof = FALSE
	BOOL bCollisionProof = FALSE
	BOOL bMeleeProof = FALSE
			
	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] SET_PLAYER_ATTRIBUTES_AFTER_SPAWN() - called...") NET_NL()
	#ENDIF
	#IF IS_DEBUG_BUILD
	PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("[KW]SET_PLAYER_ATTRIBUTES_AFTER_SPAWN - start")
	#ENDIF		
	
	WEAPONINHAND HowToRespawnWeapons = WEAPONINHAND_LASTWEAPON_BOTH 

	INITIALISE_TOTAL_NUMBER_OF_MEDALS(GET_STAT_CHARACTER_TEAM())
	GET_NUMBER_OF_VEHICLES_UNLOCKED(GET_FM_RANK_FROM_XP_VALUE(GET_STAT_CHARACTER_FM_XP()))
	//Removed for bug 1547042
	/*IF g_b_On_Deathmatch = FALSE
	AND g_b_On_Race = FALSE
		DISABLE_MP_PASSIVE_MODE()
	ENDIF*/
	
	IF NOT IS_PLAYER_DEAD( PLAYER_ID())
		CLEAR_PLAYER_WANTED_LEVEL( PLAYER_ID() )
	ELSE
		CLEAR_PLAYER_WANTED_LEVEL( PLAYER_ID() )
	ENDIF
	INT iCharSlot = GET_ACTIVE_CHARACTER_SLOT()
	
	
	#IF IS_DEBUG_BUILD
		IF GET_CURRENT_GAMEMODE() != GAMEMODE_MPTESTBED
		AND GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR
				
			IF g_MPCharData.bIsFirstSpawn
				CHECK_PLAYERKIT_ENUM_IS_IN_SYNC()
				CHECK_STATS_PACKED_IS_IN_SYNC()
				CHECK_WEAPON_BITSET_ENUM_IS_IN_SYNC()
			ENDIF
		ENDIF
	#ENDIF
	
	IF GET_CURRENT_GAMEMODE() != GAMEMODE_MPTESTBED
	AND GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR
			
		INITIALISE_LOCAL_PLAYER_FM_XP()
		REFRESH_LOCAL_PLAYER_FM_RANK()
		
		IF IS_PC_VERSION()
			SET_PLAYERS_HAIR_STAT_ON_PC()
		ENDIF
		IF g_MPCharData.bIsFirstSpawn
		AND g_bUsedRPBoost = FALSE
			RUN_INITIAL_TATTOO_MESSAGE()
			RUN_INITIAL_VEHICLE_MESSAGE()
			g_awardcheckloginawards = FALSE
		//	RUN_INITIAL_HAIR_MESSAGE_FM()

			
			
			RUN_INITIAL_BOOLCHAR_AWARD_BRONZE_MESSAGE()
			RUN_INITIAL_INTCHAR_AWARD_BRONZE_MESSAGE()
			//RUN_INITIAL_FLOATCHAR_AWARD_BRONZE_MESSAGE()
			RUN_INITIAL_PLYINT_AWARD_BRONZE_MESSAGE()
			//RUN_INITIAL_PLYFLOAT_AWARD_BRONZE_MESSAGE()
			
			RUN_INITIAL_BOOLCHAR_AWARD_SILVER_MESSAGE()
			RUN_INITIAL_INTCHAR_AWARD_SILVER_MESSAGE()
			//RUN_INITIAL_FLOATCHAR_AWARD_SILVER_MESSAGE()
			RUN_INITIAL_PLYINT_AWARD_SILVER_MESSAGE()
			//RUN_INITIAL_PLYFLOAT_AWARD_SILVER_MESSAGE()
			
			RUN_INITIAL_BOOLCHAR_AWARD_GOLD_MESSAGE()
			RUN_INITIAL_INTCHAR_AWARD_GOLD_MESSAGE()
			//RUN_INITIAL_FLOATCHAR_AWARD_GOLD_MESSAGE()
			RUN_INITIAL_PLYINT_AWARD_GOLD_MESSAGE()
			//RUN_INITIAL_PLYFLOAT_AWARD_GOLD_MESSAGE()
			
			RUN_INITIAL_BOOLCHAR_AWARD_PLATINUM_MESSAGE()
			RUN_INITIAL_INTCHAR_AWARD_PLATINUM_MESSAGE()
			//RUN_INITIAL_FLOATCHAR_AWARD_PLATINUM_MESSAGE()
			RUN_INITIAL_PLYINT_AWARD_PLATINUM_MESSAGE()
			
			RUN_INITIAL_PLYBOOL_AWARD_PLATINUM_MESSAGE()
			REMOVE_ANIM_DICT("MP_WEAPON_DROP")
		//RUN_INITIAL_PLYFLOAT_AWARD_PLATINUM_MESSAGE()
		ENDIF
		
		#IF IS_DEBUG_BUILD
		PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("SET_PLAYER_ATTRIBUTES_AFTER_SPAWN - after RUN_INITIAL_ calls")
		#ENDIF			
		
		g_bUsedRPBoost = FALSE
		
		//REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
		//If the player is not is deathmatch then give them weapons
		
		IF  IS_CHARACTER_NEW_FOR_UNLOCKS()
		AND GET_PACKED_STAT_BOOL(PACKED_MP_STATS_RECORD_PLAYERS_INITIAL_CLOTHING_AND_GIVE_HELMET) = FALSE
			SET_PLAYER_INITIAL_SHOP_ITEMS()
		ELSE
			#IF IS_DEBUG_BUILD
				IF g_ShouldShiftingTutorialsBeSkipped 
					PRINTLN("SHIFT Fing into game. GIVE_PLAYER_A_HELMET_AT_START_OF_GAME")
					GIVE_PLAYER_A_HELMET_AT_START_OF_GAME()
				ENDIF
			#ENDIF
		ENDIF
		
		IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_NOTUT_DONE) = TRUE
		AND GET_PACKED_STAT_BOOL(PACKED_MP_STATS_RECORD_PLAYERS_INITIAL_CLOTHING_AND_GIVE_HELMET) = FALSE
			SET_PLAYER_INITIAL_SHOP_ITEMS()
		ELSE
		
			#IF IS_DEBUG_BUILD
				IF g_ShouldShiftingTutorialsBeSkipped 
					PRINTLN("SHIFT Fing into game. GIVE_PLAYER_A_HELMET_AT_START_OF_GAME")
					GIVE_PLAYER_A_HELMET_AT_START_OF_GAME()
				ENDIF
			#ENDIF
		ENDIF
		
		SET_CHARACTER_NEW_FOR_UNLOCKS(FALSE)
		

		

		// remove xmas clothes
		/*
		IF g_sMPTunables.bTogglexmascontent = FALSE
			IF IS_PED_WEARING_CHRISTMAS_CLOTHES(PLAYER_PED_ID())
				SET_CURRENT_MP_SLOT_FOR_CLOTHES(GET_ACTIVE_CHARACTER_SLOT())
				REMOVE_CHRISTMAS_CLOTHING_ITEM(PLAYER_ID())
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_XMAS_NORM_CLOTHES_SAVED, FALSE)
				//GIVE_PED_VALID_STORED_ITEMS(PLAYER_PED_ID(),-1)
			ENDIF
		ENDIF
		*/
		
		// save clothes before xmas
		
		IF g_sMPTunables.bTogglexmascontent = TRUE
			IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_XMAS_NORM_CLOTHES_SAVED)
				IF NOT IS_PED_WEARING_CHRISTMAS_CLOTHES(PLAYER_PED_ID())
					SET_CURRENT_MP_SLOT_FOR_CLOTHES(GET_ACTIVE_CHARACTER_SLOT())
					SAVE_CLOTHES_BEFORE_CHRISTMAS_ITEM(PLAYER_ID())
					
					SET_MP_BOOL_CHARACTER_STAT(MP_STAT_XMAS_NORM_CLOTHES_SAVED, TRUE)
				ENDIF
				
			ENDIF
		ENDIF
		
		// Check if player is wearing valid clothes (fix missing limbs)
 			IF NOT IS_ON_ACTIVITY_SHOULD_BLOCK_CLOTHING_SAVES()
				SET_CURRENT_MP_SLOT_FOR_CLOTHES(GET_ACTIVE_CHARACTER_SLOT())
				g_bRUN_VALIDATE_PLAYERS_CLOTHING_CHECK = TRUE // calls REMOVE_LOCKED_CLOTHING_ITEM
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_XMAS_NORM_CLOTHES_SAVED, FALSE)
			ELSE
				IF g_bRUN_VALIDATE_PLAYERS_CLOTHING_CHECK = TRUE 
					g_bRUN_VALIDATE_PLAYERS_CLOTHING_CHECK = FALSE // calls REMOVE_LOCKED_CLOTHING_ITEM
					PRINTLN("SET_PLAYER_ATTRIBUTES_AFTER_SPAWN() g_bRUN_VALIDATE_PLAYERS_CLOTHING_CHECK = FALSE")
				ENDIF
			ENDIF
		//ENDIF
			
		IF IS_MP_VALENTINES_PACK_PRESENT()
			IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_VALENTINES_REC_CLOTHES) = FALSE
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_VALENTINES_REC_CLOTHES, TRUE)
			ENDIF
		ELSE
			IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_VALENTINES_REC_CLOTHES) = TRUE
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_VALENTINES_REC_CLOTHES, FALSE)
				GIVE_PED_VALID_STORED_ITEMS()
			ENDIF
		
		ENDIF
		
		IF IS_MP_BUSINESS_PACK_PRESENT()
			REMOVE_PLAYERS_TOP_HAT_IF_IN_A_VEHICLE()
		ENDIF
		
		// Ensure our hair is valid as we enter the game (Possible review this Kev: 1670776)
		IF NOT IS_LOCAL_PLAYER_AN_ANIMAL()
		AND NOT IS_USING_CUSTOM_PLAYER_MODEL()
			IF  GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState != RESPAWN_STATE_JOINING
				VALIDATE_PEDS_STORED_HAIR(PLAYER_PED_ID(), iCharSlot)
			ENDIF
			// remove female beards
			IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
				SET_PED_HEAD_OVERLAY(PLAYER_PED_ID(), HOS_FACIAL_HAIR, 0, 0.0)
				SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_BEARD_PC,0.0)
				SET_PED_COMP_ITEM_AVAILABLE_MP(MP_F_FREEMODE_01, COMP_TYPE_BERD, BERD_FMF_0_0, TRUE, TRUE)
				SET_PED_COMP_ITEM_AVAILABLE_MP(MP_F_FREEMODE_01, COMP_TYPE_TORSO, torso_FMF_0_0, TRUE, TRUE)
				SET_PED_COMP_ITEM_AVAILABLE_MP(MP_F_FREEMODE_01, COMP_TYPE_TORSO, TORSO_FMF_3_0, TRUE, TRUE)
				
			ELSE
				SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_BERD, BERD_FMM_0_0, TRUE, TRUE)
				SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_TORSO, torso_FMM_0_0, TRUE, TRUE)
				SET_PED_COMP_ITEM_AVAILABLE_MP(MP_M_FREEMODE_01, COMP_TYPE_TORSO, TORSO_FMM_4_0, TRUE, TRUE)
			ENDIF
			
		ENDIF
		
		DLC_MP_PATCH_UPDATES_FOR_DLC_CONTENT()
		
		#IF FEATURE_GEN9_EXCLUSIVE
		FIX_HSW_SA_STAT()
		#ENDIF
	ENDIF
	
	
	IF GET_CURRENT_GAMEMODE() != GAMEMODE_MPTESTBED
	AND GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR
		IF g_MPCharData.bIsFirstSpawn
			#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] SET_PLAYER_ATTRIBUTES_AFTER_SPAWN() - first spawn into FM session, WEAPONINHAND_FIRSTSPAWN_HOLSTERED") NET_NL()
			#ENDIF
			
										// Reset global until next time (or team swap)
			HowToRespawnWeapons = WEAPONINHAND_FIRSTSPAWN_HOLSTERED
			GIVE_MP_REWARD_WEAPON_IN_FREEMODE(TRUE, g_MPCharData.bIsCharNew[iCharSlot], HowToRespawnWeapons, FALSE,TRUE, g_MPCharData.bIsFirstSpawn )

			g_MPCharData.bIsFirstSpawn = FALSE
			
			FORCE_SHOP_RESET_ALL()
			
			//IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_CUT_DONE) = FALSE

			//ENDIF
			
			
		ELSE
			
			HowToRespawnWeapons = WEAPONINHAND_FIRSTSPAWN_HOLSTERED	// now spawns with weapon holstered as per request of bug 997789
			
			IF LOCAL_PLAYER_IS_ON_AGGRESSVE_FM_EVENT()  //bug 2381205
				PRINTLN("Respawn with a good weapon LOCAL_PLAYER_IS_ON_AGGRESSVE_FM_EVENT ")
				HowToRespawnWeapons = WEAPONINHAND_LASTWEAPON_BOTH
			ENDIF
			
			GIVE_MP_REWARD_WEAPON_IN_FREEMODE(TRUE, g_MPCharData.bIsCharNew[iCharSlot], HowToRespawnWeapons, FALSE,FALSE, FALSE )
			
		ENDIF	
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("SET_PLAYER_ATTRIBUTES_AFTER_SPAWN - after  GIVE_MP_REWARD_WEAPON_IN_FREEMODE")
	#ENDIF			
	
	IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_FM_TRANSITION_CREATE_PLAYER
	//	GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 48)
	//	GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, 120)
	ELSE
		
		
		IF IS_ON_FOOT_RACE_GLOBAL_SET()
		OR IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_GANGHIDEOUT)
		OR IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MISSION_CTF )
		OR IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MISSION_LTS )
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUGBY(g_FMMC_STRUCT.iRootContentIdHash)
		OR GB_IS_PLAYER_ON_FM_GANGOPS_MISSION(PLAYER_ID())
		OR GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW() 
		OR FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_BUSINESS_BATTLE(PLAYER_ID())
		OR ( GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID()) AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_YACHT_ROBBERY )

			// Bug 1313474
			IF NOT IS_PLAYER_IN_CORONA()
				IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RUGBY(g_FMMC_STRUCT.iRootContentIdHash)
					PUT_WEAPON_IN_HAND(WEAPONINHAND_LASTWEAPON_BOTH)
				ELSE
					PUT_WEAPON_IN_HAND(WEAPONINHAND_LASTWEAPON_BOTH)
				ENDIF
			ENDIF
			NET_NL() NET_PRINT("SET_PLAYER_ATTRIBUTES_AFTER_SPAWN FMMC_TYPE_GANGHIDEOUT ")
		ELIF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MISSION)
		OR g_bFM_ON_TEAM_MISSION = TRUE
//				GIVE_PLAYER_LAST_WEAPON_USED()
				NET_NL() NET_PRINT("SET_PLAYER_ATTRIBUTES_AFTER_SPAWN FMMC_TYPE_MISSION ")

		ELIF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_SURVIVAL)
		OR g_b_On_Deathmatch =TRUE 
		OR IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_DEATHMATCH)
			
//			SET_STARTING_WEAPON_DEATHMATCH()

//			GIVE_PLAYER_LAST_WEAPON_USED()
		ELSE
			IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FM_INTRO_MISS_DONE)
				
				//GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 48)
	
			ENDIF
		ENDIF

	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("SET_PLAYER_ATTRIBUTES_AFTER_SPAWN - after  SET_CURRENT_PED_WEAPON")
	#ENDIF	
	
	
//			SET_MP_PLAYERS_START_CLOTHES(g_MPCharData.bIsCharNew[iCharSlot], TSHIRT_STATES_INGAME_USE_STORED, PLAYER_PED_ID(), iCharSlot)

	IF GET_CURRENT_GAMEMODE() != GAMEMODE_MPTESTBED
	AND GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR
//			SET_DEATHMATCH_HEALTH()
	
		
		RUN_INITIAL_WEAPON_MESSAGE()
		RUN_INITIAL_WEAPON_ADDON_MESSAGE()

		//RUN_INITIAL_KIT_MESSAGE()
		//RUN_INITIAL_ABILITY_MESSAGE()
		RUN_INITIAL_CARMOD_MESSAGE()
		RUN_INITIAL_HEALTH_MESSAGE()
		
		#IF IS_DEBUG_BUILD
		PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("SET_PLAYER_ATTRIBUTES_AFTER_SPAWN - after  RUN_INITIAL_ calls")
		#ENDIF			
		
	ENDIF
//		


	
	//BC:08/08/2012. THIS IS DONE IN THE TRANSITION MENU
//			//Saves the players clothes
//			SET_MP_PLAYERS_START_CLOTHES(g_MPCharData.bIsCharNew)
//			SET_MP_PLAYERS_START_FACE(PLAYER_PED_ID())
//			GIVE_PED_STORED_HAIR(PLAYER_PED_ID(),-1)

	
	

	IF GET_CURRENT_GAMEMODE() != GAMEMODE_MPTESTBED
	AND GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR
		// Set tattoos and patches last as they may require specific clothes to be worn.
		

		IF NOT Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		AND NOT Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
		AND NOT IS_PLAYER_PED_BIGFOOT()
		AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
		AND NOT IS_LOCAL_PLAYER_AN_ANIMAL()
		AND NOT IS_USING_CUSTOM_PLAYER_MODEL()
			//IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SiegeMentality(g_FMMC_STRUCT.iRootContentIDHash)
			IF Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID()) //Maybe add this for all siege missions
			OR IS_LOADED_MISSION_TYPE_FLOW_MISSION()  
				INT iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen
				INT iOutfit
				
				// Only grab the outfit if the player has a valid team at this point
				IF iTeam >= 0
				AND iTeam < MAX_MISS_CONTROLER_TEAMS
					iOutfit = GET_LOCAL_DEFAULT_OUTFIT(iTeam)
				ENDIF
				
				IF iOutfit != ENUM_TO_INT(OUTFIT_MP_DEFAULT)
				AND iOutfit != ENUM_TO_INT(OUTFIT_MP_FREEMODE)
					PRINTLN("[KW SPAWN] SET_PLAYER_ATTRIBUTES_AFTER_SPAWN, player on a Siege Mentality MIssion. Do not call SET_MP_PLAYER_TATTOOS_AND_PATCHES")
				ELSE
					SET_MP_PLAYER_TATTOOS_AND_PATCHES(TRUE)
					PRINTLN("[KW SPAWN] SET_PLAYER_ATTRIBUTES_AFTER_SPAWN, player on a Siege Mentality MIssion but wearing own clothes SET_MP_PLAYER_TATTOOS_AND_PATCHES is called ")
				ENDIF
			ELIF GB_GET_GANGOPS_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = GOV_UNDER_CONTROL
				PRINTLN("[KW SPAWN] SET_PLAYER_ATTRIBUTES_AFTER_SPAWN, player on Gang Ops - Under Control. Do not call SET_MP_PLAYER_TATTOOS_AND_PATCHES")
			ELSE
				SET_MP_PLAYER_TATTOOS_AND_PATCHES(TRUE)
				PRINTLN("[KW SPAWN] SET_PLAYER_ATTRIBUTES_AFTER_SPAWN, player not on a heist SET_MP_PLAYER_TATTOOS_AND_PATCHES is called ")
			ENDIF
		ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_MISSION()
			PRINTLN("[KW SPAWN] SET_PLAYER_ATTRIBUTES_AFTER_SPAWN, player on gang ops mission. Do not call SET_MP_PLAYER_TATTOOS_AND_PATCHES")
		ELIF IS_PLAYER_PED_BIGFOOT()
			CHANGE_PLAYER_PED_TO_BIGFOOT_FLAGS_ONLY()
		ELSE
			IF NOT IS_LOCAL_PLAYER_AN_ANIMAL()
			AND NOT IS_USING_CUSTOM_PLAYER_MODEL()
				MP_OUTFITS_APPLY_DATA sApplyOutfitData
				sApplyOutfitData.pedID = PLAYER_PED_ID() // set player ped as the ped we are assigning clothes to
				sApplyOutfitData.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, GET_LOCAL_DEFAULT_OUTFIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen))
				sApplyOutfitData.eApplyStage = AOS_SET
				SET_PED_MP_OUTFIT(sApplyOutfitData,false,false,false,false,false,true)
				
				PRINTLN("[KW SPAWN] SET_PLAYER_ATTRIBUTES_AFTER_SPAWN, player on a heist SET_PED_MP_OUTFIT is called ")
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
		PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("SET_PLAYER_ATTRIBUTES_AFTER_SPAWN - after  SET_MP_PLAYER_TATTOOS_AND_PATCHES ")
		#ENDIF	
		
		IF NOT IS_LOCAL_PLAYER_AN_ANIMAL()
		AND NOT IS_USING_CUSTOM_PLAYER_MODEL()
			// KW for saving players helmet 1428634
			PED_COMP_ITEM_DATA_STRUCT sItemDetails
			MODEL_NAMES player_model_gender
			IF GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 1
				player_model_gender = MP_F_FREEMODE_01
			ELSE
				player_model_gender = MP_M_FREEMODE_01
			ENDIF
			
			
			sItemDetails = GET_PED_COMP_DATA_FOR_ITEM_MP(player_model_gender, COMP_TYPE_PROPS, INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_MP_INT_CHARACTER_STAT(MP_STAT_HELMET_CURRENT_COLOR)))
			IF NOT IS_PED_WEARING_HELMET(PLAYER_PED_ID())
				SET_PED_HELMET_PROP_INDEX(PLAYER_PED_ID(), sItemDetails.iDrawable, FALSE)	
				//	B* 2312133 - Should fix Assert 4.
				IF sItemDetails.iTexture >= 0
				AND sItemDetails.iTexture < GET_NUMBER_OF_PED_PROP_TEXTURE_VARIATIONS(PLAYER_PED_ID(), ANCHOR_HEAD, sItemDetails.iDrawable)
					SET_PED_HELMET_TEXTURE_INDEX(PLAYER_PED_ID(), sItemDetails.iTexture)
				ENDIF
				
				STORE_HELMET_VISOR_PROP_INDICES(PLAYER_PED_ID(), sItemDetails.iDrawable, sItemDetails.iTexture, FALSE)
			ENDIF
		ENDIF
		#IF IS_DEBUG_BUILD
		PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("SET_PLAYER_ATTRIBUTES_AFTER_SPAWN - after  SET__PED_HELMET_PROP_INDEX ")
		#ENDIF	
		
		
		
		
	ENDIF
	IF NOT IS_LOCAL_PLAYER_AN_ANIMAL()
	AND NOT IS_USING_CUSTOM_PLAYER_MODEL()
		IF GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0 
			SET_PLAYER_GENDER(PLAYER_ID(), TRUE)
		ELIF GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 1
			SET_PLAYER_GENDER(PLAYER_ID(), FALSE)
		ENDIF
	ENDIF
	#IF IS_DEBUG_BUILD
	PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("SET_PLAYER_ATTRIBUTES_AFTER_SPAWN - after  SET_PLAYER_GENDER ")
	#ENDIF		
	
//	IF g_MPCharData.bIsCharNew[iCharSlot]
//		REQUEST_SAVE(STAT_SAVETYPE_END_CREATE_NEWCHAR)
//	ENDIF
	#IF IS_DEBUG_BUILD
	PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("SET_PLAYER_ATTRIBUTES_AFTER_SPAWN - after  REQUEST_SAVE ")
	#ENDIF		
	
	
	//g_MPCharData.bIsCharNew[iCharSlot] = FALSE
//			FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
	
	SET_PED_CAN_LOSE_PROPS_ON_DAMAGE(PLAYER_PED_ID(), FALSE)
	
	SET_ENTITY_PROOFS(PLAYER_PED_ID(), bBulletProof, bFlameProof, bExplosionProof, bCollisionProof, bMeleeProof)
	NETWORK_APPLY_PED_SCAR_DATA(PLAYER_PED_ID(), GET_ACTIVE_CHARACTER_SLOT())
	
	SET_AMBIENT_VOICE_NAME(PLAYER_PED_ID(), "NO_VOICE")
	
	// Set the ped's armour to the armour they had previously (will
	// persist accross sessions but will be zero upon respawn after death) url:bugstar:2289553.
	PRINTLN("[ST_DEBUG] CHAR_CURRENT_ARMOUR ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_ARMOUR))
	SET_PED_ARMOUR(PLAYER_PED_ID(), GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_ARMOUR)) 
	MPGlobalsStats.bDISPLAY_LOW_ON_AMMO_MESSAGE = FALSE
	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] SET_PLAYER_ATTRIBUTES_AFTER_SPAWN() - finished with GAMEMODE_FM") NET_NL()
	#ENDIF
	
	//Set Damage Effect if they Kill themselves without enough cash to pay the full amount
	IF MPGlobals.KillYourselfData.bDoDamageEffect = TRUE
		APPLY_PED_DAMAGE_PACK(PLAYER_PED_ID(), "Explosion_Med", 0, 1.0)
		MPGlobals.KillYourselfData.bDoDamageEffect = FALSE
		NET_PRINT("[spawning] SET_PLAYER_ATTRIBUTES_AFTER_SPAWN() - KILL YOURSELF - bDoDamageEffect - DONE") NET_NL()
	ENDIF
	

	MPGlobalsInteractions.PlayerInteraction.iCurrentAlcoholShots = 0
	MPGlobalsHud_TitleUpdate.unlock_delay_heists_ticker_main_timer = 0
	gbFM_start_tracking_weapons = TRUE
	gbFM_start_tracking_armour = TRUE

	CLEANUP_KILL_CAM()
	
	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] SET_PLAYER_ATTRIBUTES_AFTER_SPAWN() - g_SpawnData.fCamHeading = ") NET_PRINT_FLOAT(g_SpawnData.fCamHeading) NET_NL()
	#ENDIF
	SetGameCamBehindPlayer(g_SpawnData.fCamHeading)
	
	g_SpawnData.fCamHeading = 0.0
	g_SpawnData.bSpawnTimerInitialised = FALSE      

	FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
	
	// NOTE(Owain): If the player is wearing heavy armour and a tunable is set, give them a damage reduction
	DO_HEIST_HEAVY_ARMOUR_CHECKS() 
	
	// Give player rebreather if playing salvage
	IF g_give_player_a_rebreather = FALSE
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(player_id()) =FMMC_TYPE_GB_SALVAGE 
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT) < 1
				g_give_player_a_rebreather = TRUE
				NET_PRINT("[spawning] SET_PLAYER_ATTRIBUTES_AFTER_SPAWN() - global_give_player_a_rebreather = true ")
			ENDIF
			
		ENDIF
	ENDIF
	
	IF NOT HAS_ACHIEVEMENT_BEEN_PASSED(ENUM_TO_INT(ACH31))  // fix for 2717214
		IF GET_FM_RANK_FROM_XP_VALUE(GET_STAT_CHARACTER_FM_XP())   >=  25
			AWARD_ACHIEVEMENT(ACH31) // Three-Bit Gangster
		ENDIF
	ENDIF
	
	IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
	AND GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
	AND NOT GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
		g_sMagnateOutfitStruct.bPerformGeneralRefresh = TRUE
		PRINTLN("[ST_GANGBOSS] [GANG_OUTFITS] - SET_PLAYER_ATTRIBUTES_AFTER_SPAWN - Setting g_sMagnateOutfitStruct.bPerformGeneralRefresh, lets refresh our outfit after death")
	ENDIF
	
	IF MPGlobalsAmbience.bEquipBallisticsIsEquiped = TRUE
		
		MPGlobalsAmbience.bRemoveBallisticsDrop = TRUE
		MPGlobalsAmbience.bRemoveBallisticsDropAtSpawn = TRUE
	ENDIF
	
	
	
	
	
ENDPROC

PROC DISABLE_ALL_CONTROLS_FOR_PLAYER_THIS_FRAME()
	NET_PRINT("DISABLE_ALL_CONTROLS_FOR_PLAYER - called controls disabled without calling NET_SET_PLAYER_CONTROL") NET_NL() 
	DisableAllControlsForPlayer()
ENDPROC

//PROC HIDE_PLAYER_AND_VEHICLE_FOR_RESPAWN()
//	NET_PRINT("HIDE_PLAYER_AND_VEHICLE_FOR_RESPAWN - called...") NET_NL() 
//	HidePlayerAndVehicleForRespawn()
//ENDPROC

PROC HIDE_PLAYER_AND_VEHICLE_FOR_WARP(BOOL bRepositionUnderMap=TRUE, BOOL bSetToDeleteWhenDone=TRUE)
	NET_PRINT("HIDE_PLAYER_AND_VEHICLE_FOR_WARP - called...") NET_NL() 
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		HidePlayerAndVehicleForWarp(bRepositionUnderMap, bSetToDeleteWhenDone)
	ELSE
		NET_PRINT("HIDE_PLAYER_AND_VEHICLE_FOR_WARP - IS_PLAYER_SWITCH_IN_PROGRESS = TRUE") NET_NL() 
	ENDIF
ENDPROC


PROC UPDATE_RESET_DIED_IN_MISSION_SPAWN_AREA_COUNT()
	IF (g_SpawnData.MissionSpawnDetails.iDiedInMissionSpawnAreaCount > 0)
	AND NOT IS_PLAYER_RESPAWNING(PLAYER_ID())
		IF NOT IsPlayerOnAMissionOrEventForSpawningPurposes(PLAYER_ID())
		OR NOT IS_PLAYER_IN_MISSION_SPAWN_AREA()
			g_SpawnData.MissionSpawnDetails.iDiedInMissionSpawnAreaCount = 0
			NET_PRINT("UPDATE_RESET_DIED_IN_MISSION_SPAWN_AREA_COUNT - resetting to 0") NET_NL()
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_INVINCIBLE_FLASH_TIME()
	INT iReturn 
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciDONT_INCREASE_SPAWN_PROTECTION_TIME)
		iReturn = 4000
	ELSE
		iReturn = DEFAULT_SPAWN_PROTECTION_TIME // default
	ENDIF
	
	// if there is a spawn area setup, did the player die inside it? 
	IF IsPlayerOnAMissionOrEventForSpawningPurposes(PLAYER_ID())
	AND (g_SpawnData.MissionSpawnDetails.iDiedInMissionSpawnAreaCount > 0)
	AND NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciDONT_INCREASE_SPAWN_PROTECTION_TIME)
		NET_PRINT("[spawning] GET_INVINCIBLE_FLASH_TIME - iDiedInMissionSpawnAreaCount ") NET_PRINT_INT(g_SpawnData.MissionSpawnDetails.iDiedInMissionSpawnAreaCount) NET_NL()
		iReturn	+= (g_SpawnData.MissionSpawnDetails.iDiedInMissionSpawnAreaCount * DEFAULT_SPAWN_PROTECTION_TIME)
		IF (iReturn > MAXIMUM_SPAWN_PROTECTION_TIME)
			iReturn = MAXIMUM_SPAWN_PROTECTION_TIME
		ENDIF		
	ENDIF
	
	IF (g_b_On_Deathmatch)
	AND NOT IS_PLAYER_USING_ARENA()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_KING_OF_THE_HILL() // url:bugstar:5900226 - KOTH - The time the players spend passive after respawning seems excessive in this mode
			OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetEight, ciDONT_INCREASE_SPAWN_PROTECTION_TIME)
				iReturn = 4000
			ELSE
				iReturn= MAXIMUM_SPAWN_PROTECTION_TIME
			ENDIF
		ENDIF
	ENDIF
	NET_PRINT("[spawning] GET_INVINCIBLE_FLASH_TIME - returning ") NET_PRINT_INT(iReturn) NET_NL()
	DEBUG_PRINTCALLSTACK()
	
	IF (g_SpawnData.iNextRespawnFlashTime > 0)
		iReturn = g_SpawnData.iNextRespawnFlashTime
		NET_PRINT("[spawning] GET_INVINCIBLE_FLASH_TIME - returning g_SpawnData.iNextRespawnFlashTime ") NET_PRINT_INT(iReturn) NET_NL()	
	ENDIF
	
	RETURN iReturn
ENDFUNC

PROC MAKE_PLAYER_FLASH_INVINCIBLE(INT iVehicleRespotTime)
	NET_PRINT("[spawning] MAKE_PLAYER_FLASH_INVINCIBLE - called...") NET_NL() 
//	IF (IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID()))
		
		INT iPlayerInvincibleTime = GET_INVINCIBLE_FLASH_TIME()
		
		NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(iPlayerInvincibleTime)

		SET_PLAYER_DAMAGE_MODIFIERS()
		
		IF (g_SpawnData.iNextRespawnVehicleRespotTime > 0)
			iVehicleRespotTime = g_SpawnData.iNextRespawnVehicleRespotTime
			PRINTLN("[spawning] MAKE_PLAYER_FLASH_INVINCIBLE - using iNextRespawnVehicleRespotTime = ", g_SpawnData.iNextRespawnVehicleRespotTime)
		ENDIF
		
		IF IS_SPAWNING_IN_VEHICLE()			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())				
				MakeVehicleFlashIfDriver(iVehicleRespotTime, iPlayerInvincibleTime)		
			ELSE
				NET_PRINT("[spawning] MAKE_PLAYER_FLASH_INVINCIBLE - ped not in any vehicle") NET_NL()				
			ENDIF
		ENDIF
		
//	ELSE
//		g_SpawnData.bFlashingInvincible = FALSE
//	ENDIF
ENDPROC

FUNC INT GetVehicleRespotTime()
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARS(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
		RETURN 100
	ENDIF
	
	RETURN VEHICLE_RESPOT_TIME
ENDFUNC


PROC REVEAL_PLAYER_AND_VEHICLE_FOR_WARP(BOOL bIsPassenger=FALSE)
	NET_PRINT("[spawning] REVEAL_PLAYER_AND_VEHICLE_FOR_WARP - called...") NET_NL() 
	
	INT iRespotTime = GetVehicleRespotTime()
	
	IF NOT (bIsPassenger)
		RevealPlayersVehicleForRespawn(iRespotTime)
	ENDIF
	IF (IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID()))
		MAKE_PLAYER_FLASH_INVINCIBLE(iRespotTime)
	ENDIF
	ClearAreaAroundPointForSpawning(GET_PLAYER_COORDS(PLAYER_ID()))
ENDPROC



//FUNC BOOL IS_PLAYER_FLASHING_INVINCIBLE()
//	IF (g_SpawnData.bFlashingInvincible)
//		IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.InvincibleStartTime) < INVINCIBLE_FLASH_TIME)
//			RETURN(TRUE)
//		ELSE
//			g_SpawnData.bFlashingInvincible = FALSE	
//		ENDIF
//	ENDIF
//	RETURN(FALSE)
//ENDFUNC

//FUNC INT GET_REMAINING_PLAYER_INVINCIBLE_TIME()
//	IF (g_SpawnData.bFlashingInvincible)
//		INT iReturn = GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.InvincibleStartTime)
//		NET_PRINT("[spawning] GET_REMAINING_PLAYER_INVINCIBLE_TIME = ") NET_PRINT_INT(iReturn) NET_NL()
//		RETURN iReturn
//	ENDIF
//	RETURN(0)
//ENDFUNC


PROC STORE_PLAYER_JUST_SPAWNED_INFO(PLAYER_INDEX playerID)
	IF IS_NET_PLAYER_OK(playerID, FALSE, TRUE)
		IF (GET_PLAYER_TEAM(playerID) > -1)
			IF NOT (GET_PLAYER_TEAM(playerID) = GET_PLAYER_TEAM(PLAYER_ID()))
				IF NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), PlayerID)	
					AddPointToPlayerLastSpawnCoords(GET_PLAYER_COORDS(playerID))	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC MAKE_HEADING_SAFE(FLOAT &fHeading)
	WHILE (fHeading < 0.0)
		fHeading += 360.0
	ENDWHILE
	WHILE (fHeading > 360.0)
		fHeading += -360.0
	ENDWHILE
ENDPROC

FUNC BOOL ShouldSpawningLeaveDeadPedBehind(VECTOR vSpawnLoc)

	IF IS_THIS_ROCKSTAR_MISSION_NEW_COOP_BLEEP_TEST_STEALTH(g_FMMC_STRUCT.iAdversaryModeType) // Fix for B*3322036
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TRADING_PLACES_REMIX(g_FMMC_STRUCT.iAdversaryModeType)
		PRINTLN("[spawning] ShouldSpawningLeaveDeadPedBehind - FALSE 3")
		RETURN FALSE
	ELSE
		IF IS_ON_GTA_RACE_GLOBAL_SET()
		OR (g_b_On_Deathmatch)	
			// if respawning near to where died then done leave ped behind
			IF VDIST(g_SpawnData.vBeforeRespawnCoords, vSpawnLoc) > 10.0
			AND IS_ANY_PLAYER_NEAR_POINT(g_SpawnData.vBeforeRespawnCoords, 120.0, TRUE, TRUE, FALSE, TRUE, TRUE)
			AND NOT (g_SpawnData.MissionSpawnDetails.VehicleKnockOffState = KNOCKOFFVEHICLE_NEVER)
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				PRINTLN("[spawning] ShouldSpawningLeaveDeadPedBehind - TRUE 1")
				RETURN TRUE
			ELSE
				PRINTLN("[spawning] ShouldSpawningLeaveDeadPedBehind - FALSE 1")
				RETURN FALSE
			ENDIF				
		ELSE
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
					PRINTLN("[spawning] ShouldSpawningLeaveDeadPedBehind - TRUE 2")
					RETURN TRUE
				ELSE
					PRINTLN("[spawning] ShouldSpawningLeaveDeadPedBehind - FALSE 3")
					RETURN FALSE
				ENDIF
			ELSE			
				PRINTLN("[spawning] ShouldSpawningLeaveDeadPedBehind - FALSE 2")
				RETURN FALSE
			ENDIF
		ENDIF	
	ENDIF
	
	PRINTLN("[spawning] ShouldSpawningLeaveDeadPedBehind - TRUE 3")
	RETURN TRUE	
ENDFUNC

ENUM SPAWN_REASON
	SPAWN_REASON_DEATH,
	SPAWN_REASON_TRANSITION,
	SPAWN_REASON_MANUAL,
	SPAWN_REASON_RESTORE_CHARACTER,
	SPAWN_REASON_IN_VEHICLE
ENDENUM

/// PURPOSE:
///    Spawns the player at the desired location
/// PARAMS:
///    vSpawnLoc - The location to spawn the palyer
///    fHeading - The spawn heading
///    vehModel - If this a valid model, we'll look for a veh node, create the veh, and spawn the player in it.
/// RETURNS: True if respawn happened
PROC NET_SPAWN_PLAYER(VECTOR vSpawnLoc, FLOAT fHeading, SPAWN_REASON spawnReason, PLAYER_SPAWN_LOCATION spawnLocation, BOOL bSpawnInVehicle=FALSE, BOOL swapModel = TRUE, BOOL bSkipAttributesAfterSpawn = FALSE) 


	#IF FEATURE_FREEMODE_ARCADE
	IF IS_LAUNCHING_ARCADE_LOBBY()
		PRINTLN("NET_SPAWN_PLAYER - launching arcade lobby")
		EXIT
	ENDIF
	#ENDIF

	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] NET_SPAWN_PLAYER Spawning player at ")
	NET_PRINT_VECTOR(vSpawnLoc)
	NET_PRINT(", heading = ")
	NET_PRINT_FLOAT(fHeading)
	IF (bSpawnInVehicle)
		NET_PRINT(", bSpawnInVehicle=TRUE ")
	ELSE
		NET_PRINT(", bSpawnInVehicle=FALSE ")
	ENDIF
	IF (swapModel)
		NET_PRINT(", swapModel=TRUE ")
	ELSE
		NET_PRINT(", swapModel=FALSE ")
	ENDIF
	IF (bSkipAttributesAfterSpawn)
		NET_PRINT(", bSkipAttributesAfterSpawn=TRUE ")
	ELSE
		NET_PRINT(", bSkipAttributesAfterSpawn=FALSE ")
	ENDIF	
	NET_NL()
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("NET_SPAWN_PLAYER - start")
	#ENDIF		
	
	MAKE_HEADING_SAFE(fHeading)
	
	
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!  
	// NOTE!! As of 25/8/2011 I changed this from a func to a proc. This is because
	// the spawn point has already been worked out and therefore we can't hang about, 
	// we need to create the player right now!
	// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	
	// try and force some scenarios to load in.
	SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(vSpawnLoc, 50.0, 30)

	g_SpawnData.vBeforeRespawnCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)

	
	IF (g_MPCharData.bPerformingTeamSwap) 
	OR (g_MPCharData.bPlayerWasDead)
		IF NOT IS_PLAYER_DEAD(PLAYER_ID())
			NETWORK_LEAVE_PED_BEHIND_BEFORE_WARP(PLAYER_ID(), vSpawnLoc)	// this shouldn't be called if just coming from single player	
		ENDIF
	ENDIF
	
	ClearAreaAroundPointForSpawning(vSpawnLoc, TRUE)	
	
	#IF IS_DEBUG_BUILD
	PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("NET_SPAWN_PLAYER - ClearAreaAroundPointForSpawning")
	#ENDIF	
	
	BOOL bUnpauseRenderphase = TRUE
	IF GET_CELEB_BLOCK_RESURRECT_RENDERPHASE_UNPAUSE()
		bUnpauseRenderphase = FALSE
		PRINTLN("[spawning] NET_SPAWN_PLAYER - setting bUnpauseRenderphase = FALSE")
	ENDIF
	
	INT iInvincibleFlashTime = GET_INVINCIBLE_FLASH_TIME()
	BOOL bShouldLeaveDeadPedBehind = ShouldSpawningLeaveDeadPedBehind(vSpawnLoc)
	
	IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
	OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
	OR (Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID()) OR Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID()))
	OR IS_PLAYER_USING_ARENA()
		bUnpauseRenderphase = FALSE 
		PRINTLN("[spawning] net_spawn_player - bUnpauseRenderphase = FALSE")
	ENDIF
	
	// if the player was actually dead, then use DEATH as the respawn reason
	IF IS_ENTITY_DEAD(PLAYER_PED_ID())
		spawnReason = SPAWN_REASON_DEATH
		PRINTLN("[spawning] net_spawn_player - player was dead, using SPAWN_REASON_DEATH")
	ENDIF
	
 	// if player is on death match make the player invincible for a few seconds
	IF IS_PROPERTY_SKYCAM()
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vSpawnLoc)
		SET_ENTITY_HEADING(PLAYER_PED_ID(),fHeading)
		NET_NL()NET_PRINT("[spawning] net_spawn_player - NETWORK_RESURRECT_LOCAL_PLAYER Skipped as IS_PROPERTY_SKYCAM = TRUE ")	
	ELSE
		// NOTE: NETWORK_RESURRECT_LOCAL_PLAYER asserts if we call it 3 times or more 
		// within 5 seconds with a position within 5m of prior calls.

		CDEBUG1LN(DEBUG_SPAWNING, "[Spawning] net_spawn_player - Calling NETWORK_RESURRECT_LOCAL_PLAYER with ", vSpawnLoc, ", ", fHeading, ", ", iInvincibleFlashTime, ", ", bShouldLeaveDeadPedBehind, ",", bUnpauseRenderphase)
		NETWORK_RESURRECT_LOCAL_PLAYER(vSpawnLoc, fHeading, iInvincibleFlashTime, bShouldLeaveDeadPedBehind, bUnpauseRenderphase, ENUM_TO_INT(spawnLocation), ENUM_TO_INT(spawnReason))		
	ENDIF
	
	NET_PRINT("[spawning] net_spawn_player - coords after resurrect = ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) NET_NL()
	NET_PRINT("[spawning] net_spawn_player - heading after resurrect = ") NET_PRINT_FLOAT(GET_ENTITY_HEADING(PLAYER_PED_ID())) NET_NL()
	
	RESET_OVERHEAD_LOGIC_BITSET(PLAYER_ID())
	RESET_OVERHEAD_EVENT_BITSET_FOR_PLAYER(PLAYER_ID())
	REFRESH_ALL_OVERHEAD_DISPLAY()
	
	#IF IS_DEBUG_BUILD
	PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("NET_SPAWN_PLAYER - after resurrect")
	#ENDIF
		
	// set players model and coords
	IF swapModel
		IF GET_CURRENT_GAMEMODE() = GAMEMODE_MPTESTBED
			REQUEST_MODEL(MP_M_FREEMODE_01)
			SET_NET_PLAYER_MODEL(MP_M_FREEMODE_01)
		ELSE
			IF IS_USING_CUSTOM_PLAYER_MODEL()
				REQUEST_MODEL(GET_CUSTOM_PLAYER_MODEL())
				SET_NET_PLAYER_MODEL(GET_CUSTOM_PLAYER_MODEL())					
			ELSE		
				REQUEST_MODEL(GET_PLAYER_MODEL_FOR_TEAM( GET_STAT_CHARACTER_TEAM()))
				SET_NET_PLAYER_MODEL(GET_PLAYER_MODEL_FOR_TEAM( GET_STAT_CHARACTER_TEAM()))	
			ENDIF
		ENDIF
	ENDIF

	 
	IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
	
		//Clear players wanted level
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)		
			NET_PRINT("[spawning] NET_SPAWN_PLAYER I was wanted, clearing my wanted level. \n") 			
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		ENDIF
	
		// stop player from starting in the stealth position
		SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
		
		// stop player from being on fire.
		STOP_ENTITY_FIRE(PLAYER_PED_ID())
			
		
	ENDIF
	
	//Set TimeCycle Modifier (Team Filter)
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
	//AND NOT GET_IS_TIMECYCLE_TRANSITIONING_OUT() 
		IF (GET_TIMECYCLE_TRANSITION_MODIFIER_INDEX() = -1)
		AND (GET_TIMECYCLE_MODIFIER_INDEX() = -1)
			SET_TRANSITION_TIMECYCLE_MODIFIER("", 3000)
		ENDIF
	ENDIF
	
	
	
	
	CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	REMOVE_PARTICLE_FX_FROM_ENTITY(PLAYER_PED_ID())

	IF IS_ENTITY_ON_FIRE(PLAYER_PED_ID())
		STOP_ENTITY_FIRE(PLAYER_PED_ID())
	ENDIF

	CLEAR_PED_WETNESS(PLAYER_PED_ID())

	SET_PLAYER_CAN_USE_COVER(PLAYER_ID(), TRUE)	
	
	#IF IS_DEBUG_BUILD
	PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("NET_SPAWN_PLAYER - after natives")
	#ENDIF	
	

	SET_PLAYER_DAMAGE_MODIFIERS()
	#IF IS_DEBUG_BUILD
	PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("NET_SPAWN_PLAYER - after SET_PLAYER_DAMAGE_MODIFIERS")
	#ENDIF	


	//Set up ped lock on (cops never target civilians)
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableLockonToRandomPeds, FALSE)
	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] NET_SPAWN_PLAYER - Crims can lock onto peds") NET_NL()
	#ENDIF
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillCommandeerRatherThanJack, FALSE)
	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] NET_SPAWN_PLAYER - crooks dont commandeer vehicles") NET_NL()
	#ENDIF
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WillNotHotwireLawEnforcementVehicle, FALSE)
	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] NET_SPAWN_PLAYER - Cops wont hotwire police cars") NET_NL()
	#ENDIF
	
	IF NOT g_bDisableTextAnimOnSpawn
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PhoneDisableTextingAnimations, TRUE)
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] NET_SPAWN_PLAYER - disabling texting animations on player in MP") NET_NL()
		#ENDIF
	ENDIF
	
	IF IsTeamModeSetForRespawning()
	AND IsRespawnPassenger()
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, TRUE )
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] NET_SPAWN_PLAYER - rally passenger, so not shuffling into other seat.") NET_NL()
		#ENDIF
	ENDIF

	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PlayerCanJackFriendlyPlayers, TRUE)
	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] NET_SPAWN_PLAYER - player can jack friendly players") NET_NL()
	#ENDIF
			
	#IF IS_DEBUG_BUILD
	PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("NET_SPAWN_PLAYER - after config flags")
	#ENDIF				
	

	// if player is to respawn in a vehicle do so here
	IF (bSpawnInVehicle)
		IF NOT WarpPlayerIntoSpawnVehicle(vSpawnLoc, fHeading, TRUE)	
			NET_PRINT("NET_SPAWN_PLAYER - failed to warp player into spawn vehicle this frame. bug neil f.") NET_NL()
			SCRIPT_ASSERT("NET_SPAWN_PLAYER - failed to warp player into spawn vehicle this frame. bug neil f.")
		ENDIF
	ELSE
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bHasRespawnedInVehicle = FALSE
	ENDIF
	
	
	
	
	#IF IS_DEBUG_BUILD
	PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("NET_SPAWN_PLAYER - after WarpPlayerIntoSpawnVehicle")
	#ENDIF	
	
	// reset the spawn area if off mission
	IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
	AND NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		// for deathmatch don't clear (as we're never on a mission)
		IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		AND NOT IsPlayerOnAMissionOrEventForSpawningPurposes(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		AND NOT (g_SpawnData.bDontClearMissionSpawnPersonalVehicleData)
		#ENDIF
			CLEAR_SPAWN_AREA()
		ENDIF
	ENDIF
	
	IF bSkipAttributesAfterSpawn = FALSE
		SET_PLAYER_ATTRIBUTES_AFTER_SPAWN()
	ENDIF
	
	#IF FEATURE_FREEMODE_ARCADE
	IF IS_FREEMODE_ARCADE()
		SETUP_PLAYER_WITH_ARCADE_STATS(GET_ARCADE_MODE(), GET_PLAYER_TEAM(PLAYER_ID()))
	ENDIF
	#ENDIF
	
	// next gen only
	IF IS_MP_PASSIVE_MODE_ENABLED()
		SET_LOCAL_PLAYER_AS_GHOST(TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("NET_SPAWN_PLAYER - after SET_PLAYER_ATTRIBUTES_AFTER_SPAWN")
	#ENDIF	
	
	//Brenda, Andy said to remove this, Les doesn't want it. 
	//IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState != RESPAWN_STATE_PLAYING
//	IF IS_PLAYER_RESPAWNING(PLAYER_ID())
//	AND IS_TRANSITION_ACTIVE() = FALSE
//		NET_NL()NET_PRINT("NET_SPAWN_PLAYER - SET_ENTITY_HEALTH = ")NET_PRINT_INT(GET_ENTITY_MAX_HEALTH(PLAYER_PED_ID()))
//		SET_ENTITY_HEALTH(PLAYER_PED_ID(), ROUND(TO_FLOAT(GET_ENTITY_MAX_HEALTH(PLAYER_PED_ID())+100) * 0.5))
//		SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_HEALTH, ROUND(TO_FLOAT(GET_ENTITY_MAX_HEALTH(PLAYER_PED_ID())+100) * 0.5) )
//		
//	ENDIF

	
	// don't reset back to automatic if using the custom spawn
	IF NOT (g_SpawnData.bUseCustomSpawnPoints)
		IF NOT (g_SpawnData.NextSpawn.bIsPersistant)	
			g_SpawnData.NextSpawn.Location = SPAWN_LOCATION_AUTOMATIC
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF NOT (g_SpawnData.NextSpawn.bIsPersistant)	
		g_SpawnData.iNextSpawnLocation = 0
		ENDIF
	#ENDIF
	
	g_SpawnData.vSpawnCoords = vSpawnLoc
	g_SpawnData.fSpawnHeading = fHeading
	
	RESET_ON_SPAWN EmptyResetOnSpawn
	g_SpawnData.ResetOnSpawn = EmptyResetOnSpawn
	
	//Fix for 1827833 24/04/2014 BC
	// Set this to false as we now have spawned
//	IF NOT IS_TRANSITION_ACTIVE()
//		RESET_PAUSE_MENU_TRANSITION_GLOBAL()
//		RESET_PAUSE_MENU_WARP_GLOBAL()
//	ENDIF
	
	
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PLAYER_PED_ID(), TRUE)
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
	ENDIF
	
	// broadcast to everyone that we've just spawned
	BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_JUST_SPAWNED, ALL_PLAYERS())

	// clear flags for bens's rockets
	CLEANUP_SPECIAL_PICKUPS()

	// what will the end swoop be
	//g_SpawnData.eSwitchTypeEnd = GET_SWITCH_TYPE_FOR_START_AND_END_COORDS(g_SpawnData.vStartSwoop, g_SpawnData.vSpawnCoords)

	// cleanup any drunkness .
	NET_PRINT("NET_SPAWN_PLAYER - sobering player up") NET_NL()

	Force_Cleanup_Drunk_Ped(PLAYER_PED_ID()) 
	
	MPGlobalsAmbience.bUpdatePlayerWalk = TRUE
	
	IF IS_LOCAL_PLAYER_RUNNING_PASSIVE_MODE_CUTSCENE()
	OR CONTENT_IS_USING_ARENA()
		Quit_Drunk_Camera_Immediately_Without_Clearing_Time_Cycle() // Dave W. DOn't call CLEAR_TIMECYCLE_MODIFIER in Quit_Drunk_Camera_Immediately if doing passive mode cutscene
	ELSE
		Quit_Drunk_Camera_Immediately()
	ENDIF

	// if on a dm then start in action mode
	IF IS_NET_PLAYER_OK(PLAYER_ID())		
		IF (g_b_On_Deathmatch)
		AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (g_bDoJSkip)
			NET_PRINT("NET_SPAWN_PLAYER - cleaning up dangling j skip") NET_NL()
			g_bDoJSkip = FALSE
		ENDIF
	#ENDIF

	#IF IS_DEBUG_BUILD
	NET_PRINT_TIME() NET_PRINT("[spawning] NET_SPAWN_PLAYER spawn done. \n")
	#ENDIF
	
//	SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), g_SpawnData.MissionSpawnDetails.VehicleKnockOffState) // restore the vehicle knock off state
//	IF (g_SpawnData.MissionSpawnDetails.VehicleKnockOffState = KNOCKOFFVEHICLE_NEVER)
//		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_FallsOutOfVehicleWhenKilled, TRUE)
//	ELSE
//		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_FallsOutOfVehicleWhenKilled, FALSE)	
//	ENDIF	
	SET_PLAYER_CAN_BE_KNOCKED_OFF_VEHICLE(g_SpawnData.MissionSpawnDetails.VehicleKnockOffState) // restore the vehicle knock off state
		
	g_SpawnData.iAttempt = 0
	g_SpawnData.bIgnoreGlobalExclusionCheck = FALSE
	g_SpawnData.iDontSpawnPersonalVehicleThisFrame = GET_FRAME_COUNT() // to avoid doing too much in one frame (see	1652743)	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].LastRespawnTime = GET_NETWORK_TIME() 
	ENDIF
			
	// is this the first time spawning into the game? Make sure there is no saved vehicle slot set. 2033505
	IF (g_MPCharData.bIsCharNew[GET_ACTIVE_CHARACTER_SLOT()])
		SET_LAST_USED_VEHICLE_SLOT(-1)
	ENDIF
	#IF FEATURE_FREEMODE_ARCADE
		#IF FEATURE_COPS_N_CROOKS
			IF IS_FREEMODE_ARCADE()
			AND GET_ARCADE_MODE() = ARC_COPS_CROOKS
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			ENDIF
		#ENDIF
	#ENDIF
	
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_HEIST_DEEPLINK
		PRINTLN("[spawning] NET_SPAWN_PLAYER - clearing spawn activity SPAWN_ACTIVITY_HEIST_DEEPLINK")
		SET_SPAWN_ACTIVITY(SPAWN_ACTIVITY_NOTHING)
	ENDIF
	#ENDIF
	
	
		
ENDPROC


// **************************************************************************************************************************
// **************************************************************************************************************************
//												OTHER RANDOM FUNCTIONS
// **************************************************************************************************************************
// **************************************************************************************************************************




// **************************************************************************************************************************
// **************************************************************************************************************************
//								CLIENT FUNCTIONS - called from main script
// **************************************************************************************************************************
// **************************************************************************************************************************

//FUNC BOOL IS_PLAYER_STANDING_WITH_BACK_TO_WALL()
//	
//	NET_PRINT("[spawning] IS_PLAYER_STANDING_WITH_BACK_TO_WALL - called...") NET_NL()
//
//	FLOAT fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
//
//	// get forward vector from player	
//	VECTOR vForwardVec = GET_ENTITY_FORWARD_VECTOR(PLAYER_PED_ID())
//	vForwardVec *= SPAWNING_SHAPE_TEST_PROBE_DIST
//	
//	VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
//	VECTOR vBehindPoint = vPlayerCoords - vForwardVec
//	
//	NET_PRINT("[spawning] IS_PLAYER_STANDING_WITH_BACK_TO_WALL - fPlayerHeading = ") NET_PRINT_FLOAT(fPlayerHeading) NET_NL()
//	NET_PRINT("[spawning] IS_PLAYER_STANDING_WITH_BACK_TO_WALL - vPlayerCoords = ") NET_PRINT_VECTOR(vPlayerCoords) NET_NL()
//	NET_PRINT("[spawning] IS_PLAYER_STANDING_WITH_BACK_TO_WALL - vBehindPoint = ") NET_PRINT_VECTOR(vBehindPoint) NET_NL()
//
//	SHAPETEST_INDEX BehindShapeTestResult
//
//	//BehindShapeTestResult = START_EXPENSIVE_SYNCHRONOUS_SHAPE_TEST_LOS_PROBE(vPlayerCoords, vBehindPoint, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_FOLIAGE, PLAYER_PED_ID(), SCRIPT_SHAPETEST_OPTION_DEFAULT)
//	BehindShapeTestResult = START_SHAPE_TEST_CAPSULE(vPlayerCoords, vBehindPoint, 0.3, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_FOLIAGE, PLAYER_PED_ID(), SCRIPT_SHAPETEST_OPTION_DEFAULT)
//
//	INT iBehindResult
//	
//	VECTOR vResult
//	VECTOR vNormal
//	ENTITY_INDEX EntityResult
//	SHAPETEST_STATUS ShapeTestStatus
//	
//	ShapeTestStatus = GET_SHAPE_TEST_RESULT(BehindShapeTestResult, iBehindResult, vResult, vNormal, EntityResult)
//	NET_PRINT("[spawning] IS_PLAYER_STANDING_WITH_BACK_TO_WALL - Behind - ShapeTestStatus = ") NET_PRINT_INT(ENUM_TO_INT(ShapeTestStatus)) NET_PRINT(", iBehindResult = ") NET_PRINT_INT(iBehindResult) NET_NL()
//
//	
//	IF NOT (iBehindResult = 0)
//		NET_PRINT("[spawning] IS_PLAYER_STANDING_WITH_BACK_TO_WALL - returning TRUE") NET_NL()
//		RETURN(TRUE)
//	ENDIF
//	
//	NET_PRINT("[spawning] IS_PLAYER_STANDING_WITH_BACK_TO_WALL - returning FALSE") NET_NL()
//	RETURN(FALSE)
//
//ENDFUNC

PROC MAKE_PLAYER_NOT_FACE_WALL()
	NET_PRINT("[spawning] MAKE_PLAYER_NOT_FACE_WALL called") NET_NL()
	IF (g_SpawnData.bDoShapeTest)
		IF IS_SCREEN_FADED_OUT()
			DoSpawningShapeTest()
		ELSE
			NET_PRINT_FRAME() NET_PRINT("[spawning] MAKE_PLAYER_NOT_FACE_WALL - not faded out.") NET_NL()	
		ENDIF		
		g_SpawnData.bDoShapeTest = FALSE
		NET_PRINT_FRAME() NET_PRINT("[spawning] MAKE_PLAYER_NOT_FACE_WALL - g_SpawnData.bDoShapeTest set to FALSE.") NET_NL()
	ENDIF	
ENDPROC

PROC DeployParachuteStateOfRuiner2()	
	#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_MISSION, "[spawning] DeployParachuteStateOfRuiner2 - Is checkpoint an aerial checkpoint: ", BOOL_TO_INT(g_SpawnData.NextSpawn.bIsAerialCheckpoint))
	#ENDIF
	PRINTLN("[spawning] DeployParachuteStateOfRuiner2 - Is checkpoint an aerial checkpoint: ", BOOL_TO_INT(g_SpawnData.NextSpawn.bIsAerialCheckpoint))
	
	IF (g_SpawnData.NextSpawn.bIsAerialCheckpoint)	
		PRINTLN("[spawning] DeployParachuteStateOfRuiner2 - deploying")	
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF GET_VEHICLE_HAS_PARACHUTE(vehIndex)
				IF (GET_ENTITY_MODEL(vehIndex) = RUINER2)
					g_SpawnData.bDeployParachute = TRUE
					PRINTLN("[spawning] DeployParachuteStateOfRuiner2 - deploying")
				ELSE
					PRINTLN("[spawning] DeployParachuteStateOfRuiner2 - not RUINER2")
				ENDIF
			ELIF GET_ENTITY_MODEL(vehIndex) = OPPRESSOR
				g_SpawnData.bDeployParachute = TRUE
				PRINTLN("[spawning] DeployParachuteStateOfRuiner2 (oppressor) - deploying")
			ELSE
				PRINTLN("[spawning] DeployParachuteStateOfRuiner2 - not GET_VEHICLE_HAS_PARACHUTE")
			ENDIF
		ELSE
			PRINTLN("[spawning] DeployParachuteStateOfRuiner2 - not in vehicle")
		ENDIF
	ELSE
		PRINTLN("[spawning] DeployParachuteStateOfRuiner2 - Checkpoint is not an aerial checkpoint.")
	ENDIF
ENDPROC

PROC SetWheelStateOfBlazer(BOOL bRetract)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX vehIndex = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF GET_HAS_RETRACTABLE_WHEELS(vehIndex)
			IF(GET_ENTITY_MODEL(vehIndex) = BLAZER5)
				IF (bRetract)
					IF NOT GET_IS_WHEELS_RETRACTED(vehIndex)
						g_SpawnData.bRetractVehicleWheelsInstantly = TRUE
						g_SpawnData.bExtendVehicleWheelsInstantly = FALSE
						PRINTLN("[spawning] SetWheelStateOfBlazer - g_SpawnData.bRetractVehicleWheelsInstantly = TRUE")
					ENDIF
				ELSE
					g_SpawnData.bExtendVehicleWheelsInstantly = TRUE
					g_SpawnData.bRetractVehicleWheelsInstantly = FALSE
					PRINTLN("[spawning] SetWheelStateOfBlazer - g_SpawnData.bExtendVehicleWheelsInstantly = TRUE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IsEntityInWaterCheckpoint(VEHICLE_INDEX VehicleID)
	IF (g_b_On_Race)
		IF(GET_ENTITY_MODEL(VehicleID) = BLAZER5)
			PRINTLN("[spawning] IsEntityInWaterCheckpoint - Blazer5 - returning ", g_SpawnData.NextSpawn.bIsWaterCheckpoint) 
			RETURN g_SpawnData.NextSpawn.bIsWaterCheckpoint
		ELSE
			//PRINTLN("[spawning] IsEntityInWaterCheckpoint - not a Blazer5")
		ENDIF
	ENDIF
	RETURN IS_ENTITY_IN_WATER(VehicleID)	
ENDFUNC

FUNC BOOL SHOULD_SET_VEHICLE_ON_GROUND_PROPERLY_AFTER_SPAWN()
	
	#IF FEATURE_DLC_1_2022
	IF IS_BIT_SET(g_sSummer2022Flow.iBitSet, ciSUM22_FLOW_BITSET_SPAWNING_IN_VEHICLE_FOR_INTRO_VEH_RADIO)
		RETURN FALSE
	ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC

PROC SetMyVehicleOnGroundProperly()
	NET_PRINT("[spawning] SetMyVehicleOnGroundProperly - called.") NET_NL()
	
	VEHICLE_INDEX CarID
	VECTOR vCoord
	FLOAT fGroundZ
	FLOAT fDiff
	BOOL bSetAsMissionEntity
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF CAN_EDIT_THIS_ENTITY(CarID, bSetAsMissionEntity)
			
			IF NOT IsEntityInWaterCheckpoint(CarID)
				FLOAT fGroundDiff = 1.5
				FLOAT fHeightSampleRangeUp = 5
				FLOAT fHeightIncrease = 0.3
				IF IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(CarID))
				AND IS_TRANSFORM_RACE()
					//Having issues with multiple bikes failing this check
					fGroundDiff = 5
					fHeightSampleRangeUp = 7.5
					fHeightIncrease = 0.6
				ENDIF
				vCoord = GET_ENTITY_COORDS(CarID)

				IF  SHOULD_SET_VEHICLE_ON_GROUND_PROPERLY_AFTER_SPAWN()
					IF GET_GROUND_Z_FOR_3D_COORD(vCoord, fGroundZ)
						fDiff = vCoord.z - fGroundZ	
						IF (ABSF(fDiff) < fGroundDiff)
							IF SET_VEHICLE_ON_GROUND_PROPERLY(CarID, fHeightSampleRangeUp)
								NET_PRINT("[spawning] SetMyVehicleOnGroundProperly - successfully set on ground.") NET_NL()
								g_SpawnData.iSetMyVehicleOnGroundAttempts = 0
							ELSE
								NET_PRINT("[spawning] SetMyVehicleOnGroundProperly - SET_VEHICLE_ON_GROUND_PROPERLY failed.") NET_NL()
								
								// try moving entity up by 0.3
								IF (g_SpawnData.iSetMyVehicleOnGroundAttempts < 3)
									SET_ENTITY_COORDS(CarID, <<vCoord.x, vCoord.y, vCoord.z + fHeightIncrease>>, FALSE, TRUE, TRUE)
									g_SpawnData.iSetMyVehicleOnGroundAttempts++
									PRINTLN("[spawning] SetMyVehicleOnGroundProperly - calling agains, attempt ", g_SpawnData.iSetMyVehicleOnGroundAttempts)
									SetMyVehicleOnGroundProperly()
								ENDIF
							ENDIF
						ELSE
							NET_PRINT("[spawning] SetMyVehicleOnGroundProperly - z diff too great: ") NET_PRINT_FLOAT(fDiff) NET_NL()
							g_SpawnData.iSetMyVehicleOnGroundAttempts = 0						
						ENDIF
					ELSE
						NET_PRINT("[spawning] SetMyVehicleOnGroundProperly - GET_GROUND_Z_FOR_3D_COORD failed.") NET_NL()
						g_SpawnData.iSetMyVehicleOnGroundAttempts = 0
					ENDIF
				ENDIF
				
				DeployParachuteStateOfRuiner2()				
				SetWheelStateOfBlazer(FALSE)
			ELSE
				SetWheelStateOfBlazer(TRUE)
				
				NET_PRINT("[spawning] SetMyVehicleOnGroundProperly - IS_ENTITY_IN_WATER.") NET_NL()
				g_SpawnData.iSetMyVehicleOnGroundAttempts = 0
			ENDIF
		ENDIF
		
		BOOL bMultiVehicle = FALSE
		IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION 
		AND NOT g_bMissionEnding 
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen < FMMC_MAX_TEAMS 
			AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen > -1		
				IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
					bMultiVehicle = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF (bSetAsMissionEntity)
		AND NOT bMultiVehicle
			SET_VEHICLE_AS_NO_LONGER_NEEDED(CarID)
			NET_PRINT("[spawning] SetMyVehicleOnGroundProperly - SET_VEHICLE_AS_NO_LONGER_NEEDED.") NET_NL()
		ENDIF
	ENDIF
ENDPROC



PROC RESTORE_RADIO_FOR_SPAWNED_VEHICLE(BOOL bOverrideScore=FALSE)

	IF g_SpawnData.bdisableradiosettinginspawn = TRUE
		g_SpawnData.bdisableradiosettinginspawn = FALSE
		PRINTLN("[STARTER_PACK_BROWSEPAUSEMENU] [spawning] RESTORE_RADIO_FOR_SPAWNED_VEHICLE  disabled after closing CESP")
		EXIT
	ENDIF

	// set the radio station
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT (g_SpawnData.ResetOnSpawn.bHasSetRadio)
		
			PRINTLN("[spawning] RESTORE_RADIO_FOR_SPAWNED_VEHICLE  g_SpawnData.iRadioStation = ", g_SpawnData.iRadioStation) 
		
			IF NOT (g_SpawnData.iRadioStation = -1)
				IF (g_SpawnData.iRadioStation < 255) // they want the radio station preserved now (see 1638108)
				AND NOT IS_PLAYER_HEARING_CTF_COUNTDOWN_MUSIC()
				AND NOT IS_BIT_SET(g_FMMC_STRUCT.iRaceBitSet, ciRACE_SCORE_PLAYING) //Replacing SCORE_MUSIC check with ciRACE_SCORE_PLAYING (see 2752618) //AND NOT IS_PLAYER_HEARING_MISSION_SCORE_MUSIC()
				AND NOT (AUDIO_IS_SCRIPTED_MUSIC_PLAYING() AND (g_SpawnData.bRadioStationOverScore = FALSE AND bOverrideScore = FALSE))
					
					PRINTLN("[spawning] RESTORE_RADIO_FOR_SPAWNED_VEHICLE, SET_RADIO_TO_STATION_INDEX ", g_SpawnData.iRadioStation)
					PRINTLN("[spawning] RESTORE_RADIO_FOR_SPAWNED_VEHICLE g_SpawnData.bRadioStationOverScore = ", g_SpawnData.bRadioStationOverScore)
					SET_RADIO_TO_STATION_INDEX(g_SpawnData.iRadioStation)
					

				ELSE
					
					#IF IS_DEBUG_BUILD
						IF IS_PLAYER_HEARING_CTF_COUNTDOWN_MUSIC()
							NET_PRINT("[spawning] RESTORE_RADIO_FOR_SPAWNED_VEHICLE, IS_PLAYER_HEARING_CTF_COUNTDOWN_MUSIC = TRUE") NET_NL() 
						ENDIF
						IF IS_BIT_SET(g_FMMC_STRUCT.iRaceBitSet, ciRACE_SCORE_PLAYING)
							NET_PRINT("[spawning] RESTORE_RADIO_FOR_SPAWNED_VEHICLE, ciRACE_SCORE_PLAYING bit is set") NET_NL()
						ENDIF
						IF (AUDIO_IS_SCRIPTED_MUSIC_PLAYING())
							NET_PRINT("[spawning] RESTORE_RADIO_FOR_SPAWNED_VEHICLE, AUDIO_IS_SCRIPTED_MUSIC_PLAYING = ") NET_PRINT_BOOL(AUDIO_IS_SCRIPTED_MUSIC_PLAYING()) NET_NL()
						ENDIF
					#ENDIF
				
					IF (g_SpawnData.iRadioStation >= 255)
						PRINTLN("[spawning] RESTORE_RADIO_FOR_SPAWNED_VEHICLE, SET_RADIO_TO_STATION_NAME('OFF')")
						SET_RADIO_TO_STATION_NAME("OFF")
						
						VEHICLE_INDEX VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						IF DOES_ENTITY_EXIST(VehicleID)
							IF NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)
								SET_VEH_RADIO_STATION(VehicleID, "OFF") 
								PRINTLN("[spawning] RESTORE_RADIO_FOR_SPAWNED_VEHICLE, SET_VEH_RADIO_STATION('OFF')")
							ENDIF
						ENDIF

					ENDIF
				ENDIF
			ENDIF

			
			PRINTLN("[spawning] RESTORE_RADIO_FOR_SPAWNED_VEHICLE - setting g_SpawnData.ResetOnSpawn.bHasSetRadio = TRUE")
			
			g_SpawnData.iRadioStation = -1
			g_SpawnData.ResetOnSpawn.bHasSetRadio = TRUE

		ENDIF
		
	ENDIF
ENDPROC



#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_RESPAWN_STATE_NAME(INT iRespawnState)

	SWITCH iRespawnState
		CASE RESPAWN_STATE_KILL_STRIP							RETURN "RESPAWN_STATE_KILL_STRIP"
		CASE RESPAWN_STATE_DEAD									RETURN "RESPAWN_STATE_DEAD"
		CASE RESPAWN_STATE_WAIT_FOR_DECISION					RETURN "RESPAWN_STATE_WAIT_FOR_DECISION"
		CASE RESPAWN_STATE_JOINING								RETURN "RESPAWN_STATE_JOINING"
		CASE RESPAWN_STATE_WARP_INTO_PARTNERS_CAR				RETURN "RESPAWN_STATE_WARP_INTO_PARTNERS_CAR"
		CASE RESPAWN_STATE_WAIT_TO_BE_IN_PARTNERS_CAR			RETURN "RESPAWN_STATE_WAIT_TO_BE_IN_PARTNERS_CAR"
		CASE RESPAWN_STATE_WARP_INTO_SPECIFIC_CAR				RETURN "RESPAWN_STATE_WARP_INTO_SPECIFIC_CAR"
		CASE RESPAWN_STATE_WAIT_TO_BE_IN_SPECIFIC_CAR			RETURN "RESPAWN_STATE_WAIT_TO_BE_IN_SPECIFIC_CAR"
		CASE RESPAWN_STATE_MOVE_SPAWN_CAR_BACK_UP				RETURN "RESPAWN_STATE_MOVE_SPAWN_CAR_BACK_UP"
		CASE RESPAWN_STATE_WAIT_FOR_PLAYER_TO_BE_IN_CAR			RETURN "RESPAWN_STATE_WAIT_FOR_PLAYER_TO_BE_IN_CAR"
		CASE RESPAWN_STATE_LOAD_SCENE							RETURN "RESPAWN_STATE_LOAD_SCENE"
		CASE RESPAWN_STATE_LOAD_PERSONAL_VEHICLE				RETURN "RESPAWN_STATE_LOAD_PERSONAL_VEHICLE"
		CASE RESPAWN_STATE_LOAD_SPAWN_VEHICLE					RETURN "RESPAWN_STATE_LOAD_SPAWN_VEHICLE"
		CASE RESPAWN_STATE_WAITING_FOR_PARTNER					RETURN "RESPAWN_STATE_WAITING_FOR_PARTNER"
		CASE RESPAWN_STATE_WAIT_FOR_MANUAL_RESPAWN_PLACEMENT	RETURN "RESPAWN_STATE_WAIT_FOR_MANUAL_RESPAWN_PLACEMENT"	
		CASE RESPAWN_STATE_PLAYING 								RETURN "RESPAWN_STATE_PLAYING"
	ENDSWITCH

	RETURN ""
ENDFUNC
#ENDIF

PROC SET_LOCAL_PLAYER_RESPAWN_STATE(INT iRespawnState)
	INT iCurrentState = GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState
	GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = iRespawnState
	#IF IS_DEBUG_BUILD
	PRINTLN("[Spawning] SET_LOCAL_PLAYER_RESPAWN_STATE - Current respawn state: ", DEBUG_GET_RESPAWN_STATE_NAME(iCurrentState))
	PRINTLN("[Spawning] SET_LOCAL_PLAYER_RESPAWN_STATE - New respawn state: ", DEBUG_GET_RESPAWN_STATE_NAME(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState))
	#ENDIF
	g_SpawnData.RespawnStateTimer = GET_NETWORK_TIME_ACCURATE()
	UNUSED_PARAMETER(iCurrentState)
ENDPROC

PROC GOTO_RESPAWN_STATE_PLAYING(BOOL bFixupVehicle=TRUE)

	NET_PRINT("GOTO_RESPAWN_STATE_PLAYING called...") NET_NL()
	DEBUG_PRINTCALLSTACK()

	SET_MANUAL_RESPAWN_STATE(MRS_NULL)
	
	MAKE_PLAYER_NOT_FACE_WALL()
	
	CLEANUP_ALL_KILL_STRIP()
		
	
//	IF HAS_KILL_STRIP_BEEN_SHOWN()
//		CLEANUP_KILL_STRIP()
//	ENDIF
//
//	// Cleanup our wasted message
//	CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_WASTED)

	g_SpawnData.bSpawnPersonalVehicle = FALSE	
	
	ClearAreaAroundPointForSpawning(GET_PLAYER_COORDS(PLAYER_ID()))
	RevealPlayersVehicleForRespawn(GetVehicleRespotTime())		
	
	// fix for 2336487 - if died when passed out drunk, only the first respawn needs to do load scene.
	IF (g_SpawnData.bPassedOutDrunk)
		g_SpawnData.bPassedOutDrunk = FALSE
		PRINTLN("[spawning] GOTO_RESPAWN_STATE_PLAYING - clearing g_SpawnData.bPassedOutDrunk flag")
	ENDIF
	
	
	
	// set player control
	IF NOT IS_TRANSITION_ACTIVE()
	AND NOT MPGlobalsAmbience.bRunningFmIntroCut
	AND NOT IS_TRANSITION_SESSION_LAUNCHING() 
		IF g_bResetAlphaOnSpawn
			RESET_ENTITY_ALPHA(PLAYER_PED_ID())
			g_bResetAlphaOnSpawn = FALSE
			PRINTLN("[spawning] GOTO_RESPAWN_STATE_PLAYING - clearing g_bResetAlphaOnSpawn flag after reseting alpha")
		ENDIF
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
			IF NOT IS_PLAYER_SCTV(PLAYER_ID())
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] GOTO_RESPAWN_STATE_PLAYING - swoop cam is active, NOT setting player control") NET_NL()
				#ENDIF
			ENDIF
		ELSE
			IF (g_b_OnLeaderboard)
			OR (g_b_OnResults) // 1072410
			OR (g_bMissionEnding) // 2070505
				#IF IS_DEBUG_BUILD
					IF (g_b_OnLeaderboard)
						NET_PRINT("[spawning] GOTO_RESPAWN_STATE_PLAYING - on leaderboard DO NOT turn on player control") NET_NL()
					ENDIF
					IF (g_b_OnResults)
						NET_PRINT("[spawning] GOTO_RESPAWN_STATE_PLAYING - on results DO NOT turn on player control") NET_NL()
					ENDIF
					IF (g_bMissionEnding)
						NET_PRINT("[spawning] GOTO_RESPAWN_STATE_PLAYING - mission ending DO NOT turn on player control") NET_NL()
					ENDIF
				#ENDIF			
				IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())
				AND NOT (g_SpawnData.bForceThroughJoining)
					IF (g_bMissionEnding)
						IF g_SpawnData.MissionSpawnDetails.bAbortSpawnInVehicle = TRUE
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
							NET_PRINT_FRAME() NET_PRINT("[Spawning] GOTO_RESPAWN_STATE_PLAYING - Setting player control off, passing flags: INVISIBLE | CLEAR_TASKS | NO_COLLISION | FREEZE_POSITION") NET_NL()
						ELSE
							// B*4390336, prevent changes to player visibility or physics if they're spectating. 
							IF IS_PLAYER_SPECTATING(PLAYER_ID())
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_PREVENT_VISIBILITY_CHANGES | NSPC_PREVENT_COLLISION_CHANGES | NSPC_PREVENT_FROZEN_CHANGES)
								NET_PRINT_FRAME() NET_PRINT("[Spawning] GOTO_RESPAWN_STATE_PLAYING - Setting player control off, passing flags: PREVENT_VISIBILITY_CHANGES | PREVENT_COLLISION_CHANGES | PREVENT_FROZEN_CHANGES")
							ELSE
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								NET_PRINT_FRAME() NET_PRINT("[Spawning] GOTO_RESPAWN_STATE_PLAYING - Setting player control off, passing flags: NONE.") NET_NL()
							ENDIF
						ENDIF
					ELSE
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
						NET_PRINT_FRAME() NET_PRINT("[Spawning] GOTO_RESPAWN_STATE_PLAYING - Setting player control off, passing flags: LEAVE_CAMERA_CONTROL_ON") NET_NL()
					ENDIF
				ENDIF
			ELSE
				//If we should join a launched mission
				IF SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION()
					IF NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION )	
						NET_PRINT("[spawning] SHOULD_TRANSITION_SESSION_JOINED_LAUNCHED_MISSION don't turn control back on") NET_NL()
					ELSE
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
						NET_PRINT("[spawning] DID_I_JOIN_MISSION_AS_SPECTATOR hide player") NET_NL()
					ENDIF
				//Mo, then it's all normal
				ELIF NOT IS_TRANSITION_SESSION_LAUNCHING()
					BOOL bLaunchingTransition 
					IF NATIVE_TO_INT(PLAYER_ID()) != -1
						IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION
						OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_IN_CORONA
							bLaunchingTransition = TRUE
						ENDIF
					ENDIF
					IF NOT bLaunchingTransition
						IF DID_I_JOIN_MISSION_AS_SPECTATOR()	
						OR DID_SPEC_BAILED_FOR_TRANSITION()
						OR (IS_ANY_EVENT_PLAYLIST_LAUNCHING() AND NOT NETWORK_IS_ACTIVITY_SESSION()) // 2382994, added (AND NOT NETWORK_IS_ACTIVITY_SESSION)
						OR IS_PROPERTY_SKYCAM_DOING_EXTERNAL_SHOT() // fix for 2278198
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
							#IF IS_DEBUG_BUILD
								IF DID_I_JOIN_MISSION_AS_SPECTATOR()	
									PRINTLN("[spawning] GOTO_RESPAWN_STATE_PLAYING - respawning, hiding DID_I_JOIN_MISSION_AS_SPECTATOR")	
								ENDIF
								IF DID_SPEC_BAILED_FOR_TRANSITION()	
									PRINTLN("[spawning] GOTO_RESPAWN_STATE_PLAYING - respawning, hiding DID_SPEC_BAILED_FOR_TRANSITION")	
								ENDIF
								IF IS_ANY_EVENT_PLAYLIST_LAUNCHING()	
									PRINTLN("[spawning] GOTO_RESPAWN_STATE_PLAYING - respawning, hiding IS_ANY_EVENT_PLAYLIST_LAUNCHING")	
								ENDIF
								IF IS_PROPERTY_SKYCAM_DOING_EXTERNAL_SHOT()	
									PRINTLN("[spawning] GOTO_RESPAWN_STATE_PLAYING - respawning, hiding IS_PROPERTY_SKYCAM_DOING_EXTERNAL_SHOT")	
								ENDIF
							#ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
								NET_PRINT("[spawning] GOTO_RESPAWN_STATE_PLAYING - respawning, flash player and turn ON player control") NET_NL()
							#ENDIF
							MAKE_PLAYER_FLASH_INVINCIBLE(GetVehicleRespotTime())
							IF NOT (g_SpawnData.bDisableFadeInAndTurnOnControls)
							AND NOT (g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDoingCelebrationTransition)
							
							AND NOT (GET_HEIST_NONRESET_AUTO_CONFIGURE())
							
							AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState != FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION
								IF NOT ((IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType) OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)) AND (IS_PLAYER_SCTV(PLAYER_ID()) OR USING_HEIST_SPECTATE()))
								OR NOT (IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType) OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType))
									IF IS_PLAYER_SPECTATING(PLAYER_ID())
										NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE, NSPC_PREVENT_FROZEN_CHANGES | NSPC_PREVENT_COLLISION_CHANGES)
										#IF IS_DEBUG_BUILD
										PRINTLN("[Spawning] GOTO_RESPAWN_STATE_PLAYING - Playing is spectating, preventing collision and frozen changes.")
										#ENDIF
									ELSE
										NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
									ENDIF
								ENDIF
							ELSE
								PRINTLN("[spawning] GOTO_RESPAWN_STATE_PLAYING - respawning, not returning player control g_SpawnData.bDisableFadeInAndTurnOnControls")
								#IF IS_DEBUG_BUILD
									IF  (g_SpawnData.bDisableFadeInAndTurnOnControls)
										NET_PRINT("[spawning] GOTO_RESPAWN_STATE_PLAYING - g_SpawnData.bDisableFadeInAndTurnOnControls = TRUE") NET_NL()
									ENDIF
									IF (g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDoingCelebrationTransition)
										NET_PRINT("[spawning] GOTO_RESPAWN_STATE_PLAYING - g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDoingCelebrationTransition = TRUE") NET_NL()
									ENDIF
								#ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] not setting player control. ") NET_NL()
			NET_PRINT("[spawning] GOTO_RESPAWN_STATE_PLAYING - IS_TRANSITION_ACTIVE() = ") NET_PRINT_BOOL(IS_TRANSITION_ACTIVE()) NET_NL()
			NET_PRINT("[spawning] GOTO_RESPAWN_STATE_PLAYING - MPGlobalsAmbience.bRunningFmIntroCut = ") NET_PRINT_BOOL(MPGlobalsAmbience.bRunningFmIntroCut) NET_NL()
			NET_PRINT("[spawning] GOTO_RESPAWN_STATE_PLAYING - IS_TRANSITION_SESSION_LAUNCHING() = ") NET_PRINT_BOOL(IS_TRANSITION_SESSION_LAUNCHING()) NET_NL()
		#ENDIF
	ENDIF
	
	IF IS_SPAWNING_IN_VEHICLE()
	
		IF g_b_Is_Air_Race
			BoostAirVehiclePlayerIsIn()
		ELSE
			// if respawning in a plane, by default they will be in the air
			IF IS_SPAWNING_IN_VEHICLE()
			AND (IS_THIS_MODEL_A_PLANE(g_SpawnData.MissionSpawnDetails.SpawnModel))
			//AND IS_PLAYER_ON_IMPROMPTU_DM()
				BoostAirVehiclePlayerIsIn()				
			ENDIF
		ENDIF	
		
		IF g_SpawnData.bDeluxoFlightBoost
			VEHICLE_INDEX viDeluxo = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
			
			IF DOES_ENTITY_EXIST(viDeluxo)
			AND GET_ENTITY_MODEL(viDeluxo) = DELUXO
				PRINTLN("[DELUXO] Getting out the Deluxo wings in mid-air now!")
				SET_HOVER_MODE_WING_RATIO(viDeluxo, 1.0)
			ELSE
				PRINTLN("[DELUXO] viDeluxo doesn't exist!")
			ENDIF

            g_SpawnData.bDeluxoFlightBoost = FALSE
		ENDIF
		
		IF NOT IS_NON_ARENA_KING_OF_THE_HILL()
			IF DOES_ENTITY_EXIST(g_SpawnData.MissionSpawnDetails.SpawnedVehicle) 
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_SpawnData.MissionSpawnDetails.SpawnedVehicle, FALSE)
					CLEANUP_SPAWN_VEHICLE()	
				ENDIF
				#IF IS_DEBUG_BUILD
					NET_PRINT_FRAME() NET_PRINT("[spawning] GOTO_RESPAWN_STATE_PLAYING - cleanup - g_SpawnData.MissionSpawnDetails.SpawnedVehicle MARKED AS NOT NEEDED.") NET_NL()
				#ENDIF	
			ENDIF	
		ENDIF
		
		RESTORE_RADIO_FOR_SPAWNED_VEHICLE()
		
	ENDIF
	
	IF (g_b_On_Race)
	OR (g_SpawnData.MissionSpawnDetails.bUseRaceRespotAfterRespawns)
		SET_PLAYER_HAS_JUST_RESPAWNED_IN_RACE()
	ENDIF
	
	// if spawning at hospital then give immunity for 5 secs
	IF (g_SpawnData.bSpawnedInHospital)
		MAKE_PLAYER_FLASH_INVINCIBLE(GetVehicleRespotTime())
	ENDIF	
	
	// reset spawning at hospital flag

	SetPlayerOnGroundProperly()
	PRINTLN("[spawning] GOTO_RESPAWN_STATE_PLAYING - called SetPlayerOnGroundProperly")
	
	g_SpawnData.bDisableFadeInAndTurnOnControls = FALSE
	PRINTLN("[spawning] GOTO_RESPAWN_STATE_PLAYING - cleanup - g_SpawnData.bDisableFadeInAndTurnOnControls = FALSE")	
	
	g_SpawnData.bForceThroughJoining = FALSE
	PRINTLN("[spawning] GOTO_RESPAWN_STATE_PLAYING - cleanup - g_SpawnData.bForceThroughJoining = FALSE")
	
	// make sure camera is set behind player (But not when entering the game, it's messing with code's camera - Brenda)
	SetGameCamBehindPlayer()

	IF (g_SpawnData.bHasDoneInitialSpawn)
		MAKE_GAME_CAM_NEAR_TO_PLAYER()
	ENDIF
	

	IF (bFixupVehicle)
		NET_PRINT("[spawning] GOTO_RESPAWN_STATE_PLAYING - bFixupVehicle = TRUE ")  NET_NL()
		// if spawning into the game in a vehicle then fix it up.
		FixUpMyVehicle()
	ENDIF
	
	// set landing gear to appropriate state
	SetLandingGearOfMyVehicle()	
	
	// if i'm spawning in a car make sure it is properly placed on the ground
	SetMyVehicleOnGroundProperly()


	// try and force some scenarios to load in.
	SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 50.0, 30)	
	
	// delete old car if possible (for races only)
	IF (g_b_On_Race)
	OR IS_PLAYER_USING_ARENA()
	OR g_bDMUsingSpawnVehicle
		IF DOES_ENTITY_EXIST(g_SpawnData.MyLastVehicle)
			SetVehicleToDelete(g_SpawnData.MyLastVehicle)
		ELSE
			SetVehicleToDelete(g_SpawnData.LastUsedVehicleID)
		ENDIF
	ENDIF
				
	RESET_LAST_DAMAGER()	
	
	//If we are not launching a transition session
	//We need the focus to be at the corona, and there is a load scene going on
	IF NOT IS_TRANSITION_SESSION_LAUNCHING()
		IF NOT g_bInMissionControllerCutscene		
			IF NOT IS_PLAYER_SCTV(PLAYER_ID())
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NET_PRINT("GOTO_RESPAWN_STATE_PLAYING - clearing a load scene") NET_NL()
					NEW_LOAD_SCENE_STOP()
				ENDIF
				CLEAR_FOCUS()
			ENDIF
		ELSE
			NET_PRINT("GOTO_RESPAWN_STATE_PLAYING - NOT clearing a load scene beacuse g_bInMissionControllerCutscene = TRUE") NET_NL()
		ENDIF
	ENDIF
	
	// make sure player doesn't rejoin with a wanted level
	CLEAR_PLAYER_WANTED_LEVEL( PLAYER_ID() )
	
	PRINTLN("[spawning] GOTO_RESPAWN_STATE_PLAYING called...")
	
	
	g_SpawnData.bUpdateSpawnCamRequestSent = FALSE
	g_SpawnData.bHasDoneInitialSpawn = TRUE
	
	g_SpawnData.MissionSpawnDetails.bAbortSpawnInVehicle =  FALSE
	
	SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_PLAYING)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bPlayerIsHiddenForWarp = FALSE
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].LastRespawnTime = GET_NETWORK_TIME()
	
	IF ARE_ALL_INSTANCED_TUTORIALS_COMPLETE() // Dave W, Don't re-enable store if tutorial race / mission not done  - 1846166
		SET_STORE_ENABLED(TRUE) // re-enable the ps3 store (see 1643198)
	ELSE
		PRINTLN("[spawning] Not enabling store because tutorial race / mission not done!")
	ENDIF
	
	IF g_bResetAlphaOnSpawn
		g_bResetAlphaOnSpawn = FALSE
		PRINTLN("[spawning] GOTO_RESPAWN_STATE_PLAYING - clearing g_bResetAlphaOnSpawn flag")
	ENDIF
ENDPROC

PROC StartFadeInAfterRespawn(BOOL bDoSwoop = TRUE)
	
	IF (g_ImpromptuSpawn)
		IF NOT (g_SpawnData.bImpromptuBeingSetup)
			g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()
			g_SpawnData.bImpromptuBeingSetup = TRUE
		ELSE
			IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer) > 20000
				PRINTLN("StartFadeInAfterRespawn, g_ImpromptuSpawn is not being set back to false. Why? Bug chris speirs")
				SCRIPT_ASSERT("StartFadeInAfterRespawn - g_ImpromptuSpawn is not being set back to false. Why? Bug chris speirs.")
				SET_IMPROMPTU_SPAWN_GLOBAL(FALSE)
			ELSE
				// Wait for impromptu to be setup.		
				NET_PRINT("[spawning] StartFadeInAfterRespawn - waiting for impromptu to be setup. ") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer)) NET_NL()
			ENDIF			
		ENDIF
	ELSE
		IF (g_SpawnData.bImpromptuBeingSetup)
			SetGameCamBehindPlayer(0.0)
			g_SpawnData.bImpromptuBeingSetup = FALSE
		ENDIF
	
		IF IS_SPAWNING_IN_VEHICLE()
			SetGameCamBehindPlayer(0.0)	
			PRINTLN("[spawning] StartFadeInAfterRespawn, spawning in vehicle, make sure camera is behind.")
		ENDIF
	
		IF NOT (g_SpawnData.bUpdateSpawnCamRequestSent)
			MAKE_GAME_CAM_NEAR_TO_PLAYER()
			g_SpawnData.bUpdateSpawnCamRequestSent = TRUE
		ENDIF
	
		MAKE_PLAYER_NOT_FACE_WALL()
		
	//	IF IS_SPAWN_ACTIVITY_READY()
	//	OR (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_WAIT_SPAWN_ACTIVITY)
		
			CLEANUP_ALL_KILL_STRIP()

			IF NOT (bDoSwoop)
			OR FadeInAfterRespawn()
			
				NET_PRINT("[spawning] StartFadeInAfterRespawn calling GOTO_RESPAWN_STATE_PLAYING ") NET_NL()
				
				IF NOT (bDoSwoop)
					GOTO_RESPAWN_STATE_PLAYING()
				ELSE
					GOTO_RESPAWN_STATE_PLAYING(FALSE)
				ENDIF
			ENDIF
	//	ELSE
	//		PRINTLN("[spawning] StartFadeInAfterRespawn - going to RESPAWN_STATE_WAIT_SPAWN_ACTIVITY.")
	//		g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()
	//		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_WAIT_SPAWN_ACTIVITY
	//	ENDIF
	ENDIF
ENDPROC

//FUNC BOOL SHOULD_SWOOP_CAM_DO_A_LOAD_SCENE(VECTOR vStart, VECTOR vEnd)
//	
//	FLOAT fDist = VDIST(vStart, vEnd)	
//	SWITCH_TYPE eSwitchTypeEnd = GET_SWITCH_TYPE_FOR_START_AND_END_COORDS(vStart, vEnd)
//
//	NET_PRINT("[spawning] - SHOULD_DO_A_LOAD_SCENE - vStart = ") NET_PRINT_VECTOR(vStart) NET_NL()
//	NET_PRINT("[spawning] - SHOULD_DO_A_LOAD_SCENE - vEnd = ") NET_PRINT_VECTOR(vEnd) NET_NL()
//	NET_PRINT("[spawning] - SHOULD_DO_A_LOAD_SCENE - fDist = ") NET_PRINT_FLOAT(fDist) NET_NL()
//	NET_PRINT("[spawning] - SHOULD_DO_A_LOAD_SCENE - g_SpawnData.eSwitchTypeStart = ") NET_PRINT_INT(ENUM_TO_INT(g_SpawnData.eSwitchTypeStart)) NET_NL()
//	NET_PRINT("[spawning] - SHOULD_DO_A_LOAD_SCENE - eSwitchTypeEnd = ") NET_PRINT_INT(ENUM_TO_INT(eSwitchTypeEnd)) NET_NL()
//	
//	// warping into or out of a garage
//	IF NOT (IS_POINT_IN_A_PROPERTY_GARAGE_INTERIOR(vStart) = IS_POINT_IN_A_PROPERTY_GARAGE_INTERIOR(vEnd))
//		NET_PRINT("[spawning] - SHOULD_SWOOP_CAM_DO_A_LOAD_SCENE = TRUE - warping in or out of a garage ") NET_NL()
//		RETURN(TRUE)
//	ENDIF
//	
//	IF (g_SpawnData.eSwitchTypeStart = SWITCH_TYPE_SHORT)
//	AND (eSwitchTypeEnd <> SWITCH_TYPE_SHORT)		
//		IF (fDist > RESPAWN_DO_LOAD_SCENE_DIST)	
//			NET_PRINT("[spawning] - SHOULD_SWOOP_CAM_DO_A_LOAD_SCENE = TRUE ") NET_NL()
//			RETURN(TRUE)		
//		ENDIF
//	ENDIF
//	
////	// if end point is in an interior
////	IF (g_SpawnData.eSwitchTypeStart = SWITCH_TYPE_SHORT)
////		IF IS_VALID_INTERIOR(GET_INTERIOR_AT_COORDS(vEnd))
////			NET_PRINT("[spawning] - SHOULD_SWOOP_CAM_DO_A_LOAD_SCENE, player in interior, do load scene ") NET_NL()
////			RETURN(TRUE)
////		ENDIF
////	ENDIF
//	
//	NET_PRINT("[spawning] - SHOULD_SWOOP_CAM_DO_A_LOAD_SCENE = FALSE ") NET_NL()
//
//	RETURN(FALSE)
//ENDFUNC

FUNC BOOL SHOULD_RESPAWN_DO_A_LOAD_SCENE(VECTOR vStart, VECTOR vEnd)
	
	FLOAT fDist = VDIST(vStart, vEnd)	

	NET_PRINT("[spawning] - SHOULD_RESPAWN_DO_A_LOAD_SCENE - vStart = ") NET_PRINT_VECTOR(vStart) NET_NL()
	NET_PRINT("[spawning] - SHOULD_RESPAWN_DO_A_LOAD_SCENE - vEnd = ") NET_PRINT_VECTOR(vEnd) NET_NL()
	NET_PRINT("[spawning] - SHOULD_RESPAWN_DO_A_LOAD_SCENE - fDist = ") NET_PRINT_FLOAT(fDist) NET_NL()

	// warping into or out of a garage
	IF NOT (IS_POINT_IN_A_PROPERTY_GARAGE_INTERIOR(vStart) = IS_POINT_IN_A_PROPERTY_GARAGE_INTERIOR(vEnd))
		NET_PRINT("[spawning] - SHOULD_RESPAWN_DO_A_LOAD_SCENE = TRUE - warping in or out of a garage ") NET_NL()
		RETURN(TRUE)
	ENDIF
		
	IF (fDist > RESPAWN_DO_LOAD_SCENE_DIST)	
		NET_PRINT("[spawning] - SHOULD_RESPAWN_DO_A_LOAD_SCENE = TRUE ") NET_NL()
		RETURN(TRUE)		
	ENDIF

	
	NET_PRINT("[spawning] - SHOULD_RESPAWN_DO_A_LOAD_SCENE = FALSE ") NET_NL()

	RETURN(FALSE)
ENDFUNC

FUNC MODEL_NAMES GET_RANDOM_BASIC_CAR_MODEL()
	INT iRand
	GET_RANDOM_INT_IN_RANGE(0,4)
	SWITCH iRand
		CASE 0 RETURN ASTEROPE
		CASE 1 RETURN PRAIRIE
		CASE 2 RETURN PREMIER
		CASE 3 RETURN PHOENIX
	ENDSWITCH
	RETURN ASTEROPE
ENDFUNC

PROC CleanupRandomSpawnVehicle()
	NET_PRINT("[spawning] CleanupRandomSpawnVehicle called...") NET_NL()
	
	IF NOT (g_SpawnData.RandomVehicleModel = DUMMY_MODEL_FOR_SCRIPT)
		SET_MODEL_AS_NO_LONGER_NEEDED(g_SpawnData.RandomVehicleModel)
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(g_SpawnData.RandomVehicleNetID) 
	AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(g_SpawnData.RandomVehicleNetID) 
		CLEANUP_NET_ID(g_SpawnData.RandomVehicleNetID)
	ENDIF
	
	g_SpawnData.iSpawnNearbyRandomState = 0
	g_TransitionSpawnData.bSpawnNearbyRandomVehicle = FALSE
	g_TransitionSpawnData.bSpawnNearbyRandomVehicleUseRoadOffset = TRUE // set back to default value
	g_SpawnData.RandomVehicleModel = DUMMY_MODEL_FOR_SCRIPT
ENDPROC

PROC CleanupSpecificSpawnVehicle()
	NET_PRINT("[spawning] CleanupSpecificSpawnVehicle called...") NET_NL()
	
	IF NOT (g_TransitionSpawnData.SpecificVehicleModel = DUMMY_MODEL_FOR_SCRIPT)
		SET_MODEL_AS_NO_LONGER_NEEDED(g_TransitionSpawnData.SpecificVehicleModel)
	ENDIF
	
	IF NETWORK_DOES_NETWORK_ID_EXIST(g_SpawnData.RandomVehicleNetID) 
	AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(g_SpawnData.RandomVehicleNetID) 
		CLEANUP_NET_ID(g_SpawnData.RandomVehicleNetID)
	ENDIF
	
	g_SpawnData.iSpawnNearbyRandomState = 0
	g_TransitionSpawnData.bSpawnSpecificVehicle = FALSE
	g_TransitionSpawnData.vSpecificVehicleCoords = <<0,0,0>>
	g_TransitionSpawnData.fSpecificVehicleHeading= 0.0
	
	g_TransitionSpawnData.SpecificVehicleModel = DUMMY_MODEL_FOR_SCRIPT
ENDPROC

FUNC MODEL_NAMES GET_NEARBY_RANDOM_CAR_MODEL()
	VEHICLE_INDEX VehicleID[50]
	INT i
	INT iStart
	INT iTotal
	INT iThis
	IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			iTotal = GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), VehicleID)
			NET_PRINT("GET_NEARBY_RANDOM_CAR_MODEL - iTotal = ") NET_PRINT_INT(iTotal) NET_NL()
			iStart = GET_RANDOM_INT_IN_RANGE(0, iTotal)
			REPEAT iTotal i
				iThis = iStart + i
				IF (iThis >= iTotal)
					iThis -= iTotal
				ENDIF
				IF DOES_ENTITY_EXIST(VehicleID[iThis])
					IF NOT IS_ENTITY_A_MISSION_ENTITY(VehicleID[iThis])
					AND NOT DECOR_EXIST_ON(VehicleID[iThis], "Player_Vehicle")
 					AND IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(VehicleID[iThis]))
 						RETURN(GET_ENTITY_MODEL(VehicleID[iThis]))
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	MODEL_NAMES ModelName
	INT iClass
	GET_RANDOM_VEHICLE_MODEL_IN_MEMORY(TRUE, ModelName, iClass)
	NET_PRINT("GET_NEARBY_RANDOM_CAR_MODEL - ModelName = ") NET_PRINT_INT(ENUM_TO_INT(ModelName)) NET_NL()
	IF IS_THIS_MODEL_A_CAR(ModelName)
		RETURN(ModelName)
	ENDIF
	GET_RANDOM_VEHICLE_MODEL_IN_MEMORY(FALSE, ModelName, iClass)
	NET_PRINT("GET_NEARBY_RANDOM_CAR_MODEL - ModelName2 = ") NET_PRINT_INT(ENUM_TO_INT(ModelName)) NET_NL()
	IF IS_THIS_MODEL_A_CAR(ModelName)
		RETURN(ModelName)
	ENDIF	
	RETURN(DUMMY_MODEL_FOR_SCRIPT)
ENDFUNC

FUNC BOOL IS_PERSONAL_VEHICLE_NEARBY(FLOAT fDistance = 300.0)
	
	IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCasinoCarPark,CASINO_CAR_PARK_BS_VEH_IN)
		PRINTLN("IS_PERSONAL_VEHICLE_NEARBY - returing false as in valet car park")
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_TUNER
	IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoCarMeetCarPark, CAR_MEET_CAR_PARK_BS_VEH_IN)
		PRINTLN("IS_PERSONAL_VEHICLE_NEARBY - returing false as in car meet car park")
		
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MPGlobals.VehicleData.iBSWarpIntoPrivateCarMeetCarPark, PRIVATE_CAR_MEET_CAR_PARK_BS_VEH_IN)
		PRINTLN("IS_PERSONAL_VEHICLE_NEARBY - returing false as in private car meet car park")
		
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) = GET_HASH_OF_LAUNCH_SCRIPT_NAME_SCRIPT()
		IF NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
			IF NOT IS_ENTITY_DEAD(PERSONAL_VEHICLE_ID())
				IF (VDIST(GET_ENTITY_COORDS(PERSONAL_VEHICLE_ID()), GET_PLAYER_COORDS(PLAYER_ID())) < fDistance)
					RETURN(TRUE)
				ENDIF
			ENDIF
		ENDIF
		RETURN(FALSE)
	ELSE
		RETURN MPGlobals.VehicleData.bIsNearLocalPlayer
	ENDIF
ENDFUNC

PROC UPDATE_MP_SAVED_VEHICLE_SPAWNING()

	MPGlobals.VehicleData.bIsNearLocalPlayer = IS_PERSONAL_VEHICLE_NEARBY()

	// spawn a random vehicle nearby
	IF (g_TransitionSpawnData.bSpawnNearbyRandomVehicle)
		IF NOT (g_TransitionSpawnData.bSpawnNearbySavedVehicle)
		AND NOT IS_PERSONAL_VEHICLE_NEARBY()
			IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
				IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
					
					IF CAN_REGISTER_MISSION_VEHICLES(1)
						IF NOT IS_ANY_TYPE_OF_CUTSCENE_PLAYING() // don't create during a cutscene - see 2119400
						
							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							
								IF (g_SpawnData.iSpawnNearbyRandomState = 0)
									
									IF (g_SpawnData.RandomVehicleModel = DUMMY_MODEL_FOR_SCRIPT)
										g_SpawnData.RandomVehicleModel = GET_NEARBY_RANDOM_CAR_MODEL()
										#IF IS_DEBUG_BUILD
											IF NOT (g_SpawnData.RandomVehicleModel = DUMMY_MODEL_FOR_SCRIPT)
												PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - using nearby model - ", ENUM_TO_INT(g_SpawnData.RandomVehicleModel))																		
											ENDIF
										#ENDIF
									ENDIF
									IF (g_SpawnData.RandomVehicleModel = DUMMY_MODEL_FOR_SCRIPT)
										g_SpawnData.RandomVehicleModel = GET_RANDOM_BASIC_CAR_MODEL()
										#IF IS_DEBUG_BUILD
											IF NOT (g_SpawnData.RandomVehicleModel = DUMMY_MODEL_FOR_SCRIPT)
												PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - could not find valid model, choosing basic - ", ENUM_TO_INT(g_SpawnData.RandomVehicleModel))																		
											ENDIF
										#ENDIF
									ENDIF
									
									IF NOT (g_SpawnData.RandomVehicleModel = DUMMY_MODEL_FOR_SCRIPT)								
										REQUEST_MODEL(g_SpawnData.RandomVehicleModel) // make sure it stays in memory							
										IF HAS_MODEL_LOADED(g_SpawnData.RandomVehicleModel)
											PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - model loaded = ", GET_MODEL_NAME_FOR_DEBUG(g_SpawnData.RandomVehicleModel))
											g_SpawnData.iSpawnNearbyRandomState++
										ENDIF
									ENDIF
									
								ENDIF
								
								IF (g_SpawnData.iSpawnNearbyRandomState = 1)
									
									VEHICLE_SPAWN_LOCATION_PARAMS Params
									Params.fMinDistFromCoords = 5.0				
								
									IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID()),<<0.0, 0.0, 0.0>>, g_SpawnData.RandomVehicleModel, g_TransitionSpawnData.bSpawnNearbyRandomVehicleUseRoadOffset, g_SpawnData.vRandomVehicleCoords, g_SpawnData.fRandomVehicleHeading, Params)
										PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - got random vehicle spawn coords")
										g_SpawnData.iSpawnNearbyRandomState++	
									ENDIF
								ENDIF
								
								IF (g_SpawnData.iSpawnNearbyRandomState = 2)
									IF CREATE_NET_VEHICLE(g_SpawnData.RandomVehicleNetID, g_SpawnData.RandomVehicleModel, g_SpawnData.vRandomVehicleCoords, g_SpawnData.fRandomVehicleHeading, FALSE, TRUE, FALSE) 
										PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - random vehicle created at: ", g_SpawnData.vRandomVehicleCoords)		
										
										// settings for ambient vehicle.
										SET_VEHICLE_MAY_BE_USED_BY_GOTO_POINT_ANY_MEANS(NET_TO_VEH(g_SpawnData.RandomVehicleNetID),FALSE)
										SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(NET_TO_VEH(g_SpawnData.RandomVehicleNetID),FALSE)
										SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(NET_TO_VEH(g_SpawnData.RandomVehicleNetID),FALSE)
										SET_VEHICLE_IS_STOLEN(NET_TO_VEH(g_SpawnData.RandomVehicleNetID),FALSE) 
										SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(g_SpawnData.RandomVehicleNetID, FALSE, FALSE)
										
										CleanupRandomSpawnVehicle()
									ENDIF
								ENDIF
								
							ENDIF
						ELSE
							PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - IS_ANY_TYPE_OF_CUTSCENE_PLAYING - true")
						ENDIF
					ELSE
						PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - CAN_REGISTER_MISSION_VEHICLES - false")
					ENDIF				
				ENDIF
			ELSE
				PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - bSpawnNearbyRandomVehicle - on mission")	
			ENDIF
		ELSE
			PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - bSpawnNearbySavedVehicle=TRUE")	
			CleanupRandomSpawnVehicle()
		ENDIF
	ENDIF
	
	// spawn a specific vehicle nearby
	IF (g_TransitionSpawnData.bSpawnSpecificVehicle)
		IF NOT (g_TransitionSpawnData.bSpawnNearbySavedVehicle)
		AND NOT IS_PERSONAL_VEHICLE_NEARBY()
			IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
				IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
					
					IF CAN_REGISTER_MISSION_VEHICLES(1)
						IF NOT IS_ANY_TYPE_OF_CUTSCENE_PLAYING() // don't create during a cutscene - see 2119400
						
							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							
								IF (g_SpawnData.iSpawnNearbyRandomState = 0)									
									IF (g_TransitionSpawnData.SpecificVehicleModel = DUMMY_MODEL_FOR_SCRIPT)
									OR VMAG(g_TransitionSpawnData.vSpecificVehicleCoords) <= 0.0
										CleanupSpecificSpawnVehicle()
									ELSE
										REQUEST_MODEL(g_TransitionSpawnData.SpecificVehicleModel) // make sure it stays in memory							
										IF HAS_MODEL_LOADED(g_TransitionSpawnData.SpecificVehicleModel)
											PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - model loaded = ", GET_MODEL_NAME_FOR_DEBUG(g_TransitionSpawnData.SpecificVehicleModel))
											g_SpawnData.iSpawnNearbyRandomState++
										ENDIF									
									ENDIF																		
								ENDIF
								
								IF (g_SpawnData.iSpawnNearbyRandomState = 1)
									IF CREATE_NET_VEHICLE(g_SpawnData.RandomVehicleNetID, g_TransitionSpawnData.SpecificVehicleModel, g_TransitionSpawnData.vSpecificVehicleCoords, g_TransitionSpawnData.fSpecificVehicleHeading, FALSE, TRUE, FALSE) 
										PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - specific vehicle created at: ", g_TransitionSpawnData.vSpecificVehicleCoords)		
										
										// settings for ambient vehicle.
										SET_VEHICLE_MAY_BE_USED_BY_GOTO_POINT_ANY_MEANS(NET_TO_VEH(g_SpawnData.RandomVehicleNetID),FALSE)
										SET_VEHICLE_CAN_BE_USED_BY_FLEEING_PEDS(NET_TO_VEH(g_SpawnData.RandomVehicleNetID),FALSE)
										SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(NET_TO_VEH(g_SpawnData.RandomVehicleNetID),FALSE)
										SET_VEHICLE_IS_STOLEN(NET_TO_VEH(g_SpawnData.RandomVehicleNetID),FALSE) 
										SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(g_SpawnData.RandomVehicleNetID, FALSE, FALSE)
										
										// we should not be able to save this vehicle?
										DECOR_SET_INT(NET_TO_VEH(g_SpawnData.RandomVehicleNetID), "Not_Allow_As_Saved_Veh", 1)
										
										g_SpawnData.iSpawnNearbyRandomState++
									ENDIF
								ENDIF
								
								IF (g_SpawnData.iSpawnNearbyRandomState = 2)
									IF IS_NET_PLAYER_OK(PLAYER_ID())
									AND IS_SCREEN_FADED_IN()
									
										// should anchor?
										IF NOT IS_ENTITY_DEAD(NET_TO_VEH(g_SpawnData.RandomVehicleNetID))
											IF IS_ENTITY_IN_WATER(NET_TO_VEH(g_SpawnData.RandomVehicleNetID))
												IF CAN_ANCHOR_BOAT_HERE_IGNORE_PLAYERS(NET_TO_VEH(g_SpawnData.RandomVehicleNetID)) 
													SET_BOAT_ANCHOR(NET_TO_VEH(g_SpawnData.RandomVehicleNetID), TRUE)
													PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - anchoring specific vehicle.")														
												ELSE
													PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - cannot anchor specific vehicle.")
												ENDIF
											ELSE
												PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - specific vehicle not in water.")
											ENDIF	
										ENDIF
										
										g_SpawnData.iSpawnNearbyRandomState++
										CleanupSpecificSpawnVehicle()
									ENDIF
								ENDIF
								
							ENDIF
						ELSE
							PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - IS_ANY_TYPE_OF_CUTSCENE_PLAYING - true")
						ENDIF
					ELSE
						PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - CAN_REGISTER_MISSION_VEHICLES - false")
					ENDIF				
				ENDIF
			ELSE
				PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - bSpawnSpecificVehicle - on mission")	
			ENDIF
		ELSE
			PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - bSpawnSpecificVehicle=TRUE")	
			CleanupSpecificSpawnVehicle()
		ENDIF
	ENDIF	

	// spawn a saved vehicle nearby
	IF (g_TransitionSpawnData.bSpawnNearbySavedVehicle)
		IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
			IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, FALSE)
			
				INT iSaveSlot = CURRENT_SAVED_VEHICLE_SLOT()
				
				IF (iSaveSlot > -1)
					IF NOT DOES_VEHICLE_SLOT_BELONG_IN_ANY_GARAGE(iSaveSlot)
						PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - car doesn't belong in any garage.")
						iSaveSlot = -1	
						SET_LAST_USED_VEHICLE_SLOT(iSaveSlot)
					ENDIF
				ENDIF
				
				IF (iSaveSlot > -1)
				AND NOT (g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT)
				AND NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_DESTROYED)
				AND NOT IS_BIT_SET(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_IMPOUNDED)
				AND NOT IS_LOCAL_PLAYER_IN_PERSONAL_VEHICLE()
				
					IF (g_SpawnData.iSpawnNearbySavedState = 0)
						IF REQUEST_LOAD_MODEL(g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel)
							g_SpawnData.iSpawnNearbySavedState++
						ENDIF
					ENDIF			
				
					IF (g_SpawnData.iSpawnNearbySavedState = 1)
						#IF IS_DEBUG_BUILD
							IF (bForceJoinAtCoords)
								g_SpawnData.vSavedVehicleCoords = vForceJoinCoords
								g_SpawnData.fSavedVehicleHeading = fForceJoinHeading
								PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - using forced coords ")
								g_SpawnData.iSpawnNearbySavedState++
							ELSE
						#ENDIF
								VEHICLE_SPAWN_LOCATION_PARAMS Params
								Params.fMinDistFromCoords=5.0
								Params.bConsiderHighways=g_TransitionSpawnData.bSpawnNearbySavedVehicleConsiderHighways
								Params.bUseExactCoordsIfPossible=g_TransitionSpawnData.bSpawnVehicleInExactCoordsIfPossible
								Params.fMaxDistance=g_TransitionSpawnData.fSpawnVehicleMaxRange							
								IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID()),<<0.0, 0.0, 0.0>>, g_MpSavedVehicles[iSaveSlot].vehicleSetupMP.VehicleSetup.eModel, g_TransitionSpawnData.bSpawnNearbySavedVehicleUseRoadOffset, g_SpawnData.vSavedVehicleCoords, g_SpawnData.fSavedVehicleHeading, Params)
									PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - got vehicle spawn coords")
									g_SpawnData.iSpawnNearbySavedState++	
								ENDIF
						#IF IS_DEBUG_BUILD
							ENDIF
						#ENDIF
					ENDIF
					
					IF (g_SpawnData.iSpawnNearbySavedState = 2)
						
						BOOL bForceExactCoords
						bForceExactCoords = FALSE
						#IF IS_DEBUG_BUILD
							IF (bForceJoinAtCoords)
								bForceExactCoords = TRUE
							ENDIF
						#ENDIF
					
						IF CREATE_MP_SAVED_VEHICLE(g_SpawnData.vSavedVehicleCoords, g_SpawnData.fSavedVehicleHeading, TRUE, iSaveSlot, TRUE, bForceExactCoords)
							g_SpawnData.iSpawnNearbySavedState = 0
							g_TransitionSpawnData.bSpawnNearbySavedVehicle = FALSE
							g_TransitionSpawnData.bSpawnNearbySavedVehicleUseRoadOffset = TRUE // set back to default value
							g_TransitionSpawnData.bSpawnNearbySavedVehicleConsiderHighways = FALSE
							g_TransitionSpawnData.bSpawnVehicleInExactCoordsIfPossible = FALSE
							g_TransitionSpawnData.fSpawnVehicleMaxRange = 150.0
							
							SET_BIT(g_MpSavedVehicles[iSaveSlot].iVehicleBS,MP_SAVED_VEHICLE_OUT_GARAGE)
							MP_SAVE_VEHICLE_SLOT_STATS_FROM_SAVEGAME(iSaveSlot,g_MpSavedVehicles[iSaveSlot] ,TRUE)
							REQUEST_SAVE(SSR_REASON_PV_SPAWN, STAT_SAVETYPE_IMMEDIATE_FLUSH)
							PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - CREATE_MP_SAVED_VEHICLE = TRUE ")
						ELSE
							PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - waiting for CREATE_MP_SAVED_VEHICLE")
						ENDIF
					ENDIF
					
				ELSE
					#IF IS_DEBUG_BUILD
						IF IS_LOCAL_PLAYER_IN_PERSONAL_VEHICLE()
							PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - IS_LOCAL_PLAYER_IN_PERSONAL_VEHICLE = TRUE ")
						ENDIF
					#ENDIF
				
					PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - doesnt have a saved vehicle")	
					g_TransitionSpawnData.bSpawnNearbySavedVehicle = FALSE
				ENDIF

			
			ENDIF
		ELSE
			PRINTLN("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - bSpawnNearbySavedVehicle - on mission")	
		ENDIF
	ENDIF
	
	IF (g_TransitionSpawnData.bWarpPlayerIntoSavedVehicle)
		IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
			IF NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(PERSONAL_VEHICLE_NET_ID())
					IF IS_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_ID())
						WarpPlayerIntoCar(PLAYER_PED_ID(), PERSONAL_VEHICLE_ID(), VS_DRIVER)
						g_TransitionSpawnData.bWarpPlayerIntoSavedVehicle = FALSE
					ELSE
						g_TransitionSpawnData.bSwitchSavedVehicleEngineOn = FALSE
						NET_PRINT("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - bWarpPlayerIntoSavedVehicle - vehicle not driveable")  NET_NL()
					ENDIF
				ELSE
					NET_PRINT("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING -  bWarpPlayerIntoSavedVehicle - NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID = FALSE")  NET_NL()	
				ENDIF
			ELSE
				NET_PRINT("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING -  bWarpPlayerIntoSavedVehicle - NETWORK_DOES_NETWORK_ID_EXIST = FALSE")  NET_NL()		
			ENDIF
		ELSE
			NET_PRINT("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - bWarpPlayerIntoSavedVehicle - on mission") NET_NL()
		ENDIF
	ENDIF
	
	IF (g_TransitionSpawnData.bSwitchSavedVehicleEngineOn)
		IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
			IF NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(PERSONAL_VEHICLE_NET_ID())
					IF IS_VEHICLE_DRIVEABLE(PERSONAL_VEHICLE_ID())
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PERSONAL_VEHICLE_ID())
							SET_VEHICLE_ENGINE_ON(PERSONAL_VEHICLE_ID(), TRUE, TRUE)
							g_TransitionSpawnData.bSwitchSavedVehicleEngineOn = FALSE
						ENDIF
					ELSE
						g_TransitionSpawnData.bSwitchSavedVehicleEngineOn = FALSE
						NET_PRINT("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - g_TransitionSpawnData.bSwitchSavedVehicleEngineOn - NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID = FALSE")  NET_NL()	
					ENDIF
				ELSE
					NET_PRINT("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - g_TransitionSpawnData.bSwitchSavedVehicleEngineOn - NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID = FALSE")  NET_NL()	
				ENDIF
			ELSE
				NET_PRINT("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - g_TransitionSpawnData.bSwitchSavedVehicleEngineOn - NETWORK_DOES_NETWORK_ID_EXIST = FALSE")  NET_NL()		
			ENDIF
		ELSE
			NET_PRINT("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - bSwitchSavedVehicleEngineOn - on mission") NET_NL()
		ENDIF
	ENDIF
	
	IF (g_TransitionSpawnData.bFreezeSavedVehicle)
		IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
			IF NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(PERSONAL_VEHICLE_NET_ID())
					
					FREEZE_ENTITY_POSITION(PERSONAL_VEHICLE_ID(), TRUE)
					SET_ENTITY_CAN_BE_DAMAGED(PERSONAL_VEHICLE_ID(), FALSE)
				
					g_TransitionSpawnData.bFreezeSavedVehicle = FALSE
					NET_PRINT("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - donee ")  NET_NL()	
				ELSE
					NET_PRINT("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - g_TransitionSpawnData.bFreezeSavedVehicle - NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID = FALSE")  NET_NL()	
				ENDIF
			ELSE
				NET_PRINT("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - g_TransitionSpawnData.bFreezeSavedVehicle - NETWORK_DOES_NETWORK_ID_EXIST = FALSE")  NET_NL()		
			ENDIF	
		ELSE
			NET_PRINT("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - bFreezeSavedVehicle - on mission") NET_NL()
		ENDIF
	ENDIF
	
	
	IF (g_TransitionSpawnData.bMakeSureVehicleCanBeDamaged)
		IF NETWORK_DOES_NETWORK_ID_EXIST(PERSONAL_VEHICLE_NET_ID())
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(PERSONAL_VEHICLE_NET_ID())
				SET_ENTITY_CAN_BE_DAMAGED(PERSONAL_VEHICLE_ID(), TRUE)
				g_TransitionSpawnData.bMakeSureVehicleCanBeDamaged = FALSE
				NET_PRINT("[spawning] UPDATE_MP_SAVED_VEHICLE_SPAWNING - bMakeSureVehicleCanBeDamaged - applied") NET_NL()
			ENDIF
		ENDIF
	ENDIF


		

ENDPROC


PROC UPDATE_DELETE_VEHICLE_FROM_RESPAWN()
	VEHICLE_INDEX vehId
	IF DOES_ENTITY_EXIST(g_SpawnData.VehicleToDelete)
		IF NETWORK_GET_ENTITY_IS_NETWORKED(g_SpawnData.VehicleToDelete)
		AND NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(g_SpawnData.VehicleToDelete)
			g_SpawnData.NetVehicleToDelete = VEH_TO_NET(g_SpawnData.VehicleToDelete) // store the network id, as the vehicle id can expire.
		ENDIF
		
		vehId = g_SpawnData.VehicleToDelete
	ELIF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(g_SpawnData.NetVehicleToDelete)
		vehId = NET_TO_VEH(g_SpawnData.NetVehicleToDelete)
		PRINTLN("UPDATE_DELETE_VEHICLE_FROM_RESPAWN - using stored net entity")
	ENDIF
	DeleteVehicleIfPossible(vehId)
ENDPROC


PROC GOTO_JOINING_RESPAWN_STATE()
	#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] GOTO_JOINING_RESPAWN_STATE - called...") NET_NL()
	#ENDIF

	
	SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_JOINING)
	g_SpawnData.bIsSwoopedUp = FALSE  // url:bugstar:5386688 - Arena - Arcade Race I - Camera sees under the map during transition after manually respawning
	
//	IF NOT (GET_MANUAL_RESPAWN_STATE() = MRS_RESPAWN_PLAYER_HIDDEN)
//		SET_SWOOP_CAMERA_TYPE_FOR_RESPAWN(SPAWN_LOCATION_AUTOMATIC, g_SpawnData.vMyDeadCoords)
//	ENDIF	
ENDPROC

FUNC BOOL IS_TOILET_RESPAWN_SAFE()
	IF IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PLAYER_ID())
		INT iParticipant
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
				PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
				
				IF PlayerID <> PLAYER_ID()
					IF NOT ARE_PLAYERS_IN_SAME_PROPERTY_BUILDING_BUT_DIFFERENT_INSTANCES(PLAYER_ID(), PlayerID)
						IF IS_NET_PLAYER_OK(PlayerId)
							IF IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[NATIVE_TO_INT(PlayerID)].propertyDetails.iCurrentlyInsideProperty, PROPERTY_CLUBHOUSE_1_BASE_A)
								IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(PlayerID), <<1125.195068, -3161.408594, -37.970226>>, <<1124.024414, -3161.408594, -35.970226>>, 2.0)
									PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - respawn activity is not safe to use 1")
									
									RETURN FALSE
								ENDIF
							ELSE
								IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(PlayerID), <<1015.430908, -3149.844629, -39.831039>>, <<1016.597412, -3149.844629, -37.831039>>, 2.0)
									PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - respawn activity is not safe to use 2")
									
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ELIF IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
		INT iBlockedToiletCount = 0
		
		INT i
		REPEAT 3 i
			INT iParticipant
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
					PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
					
					IF PlayerID <> PLAYER_ID()
						IF ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(PLAYER_ID(), PlayerID, TRUE)
							IF IS_NET_PLAYER_OK(PlayerID)
								VECTOR vToiletPosition1
								VECTOR vToiletPosition2
								
								SWITCH i
									CASE 0
										vToiletPosition1 = <<-1609.017456,-3012.378174,-79.996368>>
										vToiletPosition2 = <<-1609.012207,-3014.232178,-77.987259>>
									BREAK
									
									CASE 1
										vToiletPosition1 = <<-1609.004150,-3014.461426,-80.006065>>
										vToiletPosition2 = <<-1608.983398,-3015.961182,-77.982025>>
									BREAK
									
									CASE 2
										vToiletPosition1 = <<-1609.015991,-3016.177734,-80.006065>>
										vToiletPosition2 = <<-1609.028564,-3017.689697,-77.953873>>
									BREAK
								ENDSWITCH
								
								IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(PlayerID), vToiletPosition1, vToiletPosition2, 2.5)
									PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - nightclub respawn activity is not safe to use")
									
									iBlockedToiletCount++
									iParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
		
		IF iBlockedToiletCount = 3
			PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - respawn activity is not safe to use 3")
			
			RETURN FALSE
		ENDIF
		
		IF IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
			PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - respawn activity is not safe to use - player walking out")
			RETURN FALSE
		ENDIF
	#IF FEATURE_CASINO
	ELIF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		INT iBlockedToiletCount = 0
		
		INT i
		REPEAT 2 i
			INT iParticipant
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
					PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
					
					IF PlayerID <> PLAYER_ID()
						IF ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(PLAYER_ID(), PlayerID, TRUE)
							IF IS_NET_PLAYER_OK(PlayerID)
								VECTOR vToiletPosition1
								VECTOR vToiletPosition2
								
								SWITCH i
									CASE 0
										vToiletPosition1 = <<983.755066,74.338402,115.164139>>
										vToiletPosition2 = <<984.882019,76.126808,118.164139>>
									BREAK
									
									CASE 1
										vToiletPosition1 = <<980.898560,76.768150,115.164139>>
										vToiletPosition2 = <<982.414917,75.806702,118.164139>>
									BREAK
								ENDSWITCH
								
								IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(PlayerID), vToiletPosition1, vToiletPosition2, 3.25)
								OR ((i = 0) AND GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner != PLAYER_ID())
								OR ((i = 1) AND (GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID() OR NOT HAS_PLAYER_PURCHASED_CASINO_APARTMENT_BEDROOM(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)))
									PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - casino respawn activity is not safe to use")
									
									iBlockedToiletCount++
									iParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
		
		IF iBlockedToiletCount = 2
			PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - respawn activity is not safe to use 3")
			
			RETURN FALSE
		ENDIF
		
		IF IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
			PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - respawn activity is not safe to use - player walking out")
			RETURN FALSE
		ENDIF
	ELIF IS_PLAYER_IN_CASINO(PLAYER_ID())
		INT iBlockedToiletCount = 0
		
		INT i
		REPEAT 4 i
			INT iParticipant
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
					PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
					
					IF PlayerID <> PLAYER_ID()
						IF ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(PLAYER_ID(), PlayerID, TRUE)
							IF IS_NET_PLAYER_OK(PlayerID)
								VECTOR vToiletPosition1
								VECTOR vToiletPosition2
								
								SWITCH i
									CASE 0
										vToiletPosition1 = <<1160.113037,252.248596,-52.040874>>
										vToiletPosition2 = <<1161.580444,252.259689,-50.040874>>
									BREAK
									
									CASE 1
										vToiletPosition1 = <<1161.797974,252.286957,-52.040874>>
										vToiletPosition2 = <<1163.295166,252.316650,-50.040874>>
									BREAK
									
									CASE 2
										vToiletPosition1 = <<1134.399902,279.530273,-52.040764>>
										vToiletPosition2 = <<1134.372925,278.045593,-50.040764>>
									BREAK
									
									CASE 3
										vToiletPosition1 = <<1134.404785,281.266846,-52.029781>>
										vToiletPosition2 = <<1134.403564,279.770203,-50.040764>>
									BREAK
								ENDSWITCH
								
								IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(PlayerID), vToiletPosition1, vToiletPosition2, 2.5)
									PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - casino respawn activity is not safe to use")
									
									iBlockedToiletCount++
									iParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
		
		IF iBlockedToiletCount = 4
			PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - respawn activity is not safe to use 3")
			
			RETURN FALSE
		ENDIF
		
		IF IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
			PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - respawn activity is not safe to use - player walking out")
			RETURN FALSE
		ENDIF
	#ENDIF
	#IF FEATURE_CASINO_HEIST
	ELIF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		INT iBlockedToiletCount
		
		INT iParticipant
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
				PLAYER_INDEX PlayerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
				
				IF PlayerID <> PLAYER_ID()
					IF ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(PLAYER_ID(), PlayerID, TRUE)
						IF IS_NET_PLAYER_OK(PlayerID)
							IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(PlayerID), <<2725.311523,-377.143494,-48.400040>>, <<2722.700928,-377.159271,-46.132896>>, 3.5)
								iBlockedToiletCount++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF iBlockedToiletCount > 0
			PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - respawn activity is not safe to use 4")
			
			RETURN FALSE
		ENDIF
		
		IF IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
			PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - respawn activity is not safe to use - player walking out")
			RETURN FALSE
		ENDIF
	#ENDIF
	#IF FEATURE_CASINO_NIGHTCLUB
	ELIF IS_PLAYER_IN_CASINO_NIGHTCLUB(PLAYER_ID())
		INT iBlockedToiletCount = 0
		
		INT i
		REPEAT 3 i
			INT iParticipant
			REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
				IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
					PLAYER_INDEX PlayerId = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
					
					IF PlayerID <> PLAYER_ID()
						IF ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(PLAYER_ID(), PlayerID, TRUE)
							IF IS_NET_PLAYER_OK(PlayerID)
								VECTOR vToiletPosition1
								VECTOR vToiletPosition2
								
								SWITCH i
									CASE 0
									vToiletPosition1 = <<1545.173706,249.820999,-50.005981>>
									vToiletPosition2 = <<1545.189453,248.166458,-48.005981>>
								BREAK
								
								CASE 1
									vToiletPosition1 = <<1545.198975,248.033646,-50.005981>>
									vToiletPosition2 = <<1545.206665,246.636322,-48.005981>>
								BREAK
								
								CASE 2
									vToiletPosition1 = <<1545.197021,246.319489,-50.005981>>
									vToiletPosition2 = <<1545.199585,244.838928,-48.005981>>
								BREAK
								ENDSWITCH
								
								IF IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(PlayerID), vToiletPosition1, vToiletPosition2, 2.5)
									PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - casino nightclub respawn activity is not safe to use")
									
									iBlockedToiletCount++
									iParticipant = NETWORK_GET_MAX_NUM_PARTICIPANTS()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
		
		IF iBlockedToiletCount = 3
			PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - respawn activity is not safe to use 3")
			
			RETURN FALSE
		ENDIF
		
		IF IS_LOCAL_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR()
			PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - respawn activity is not safe to use - player walking out")
			RETURN FALSE
		ENDIF
	#ENDIF
	ENDIF
	
	PRINTLN("[toilet_respawn] IS_TOILET_RESPAWN_SAFE - respawn activity is safe to use")
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_TOILET_PROPERTY(PLAYER_INDEX pPlayer)
	IF IS_PLAYER_IN_CLUBHOUSE_PROPERTY(pPlayer)
	OR IS_PLAYER_IN_NIGHTCLUB(pPlayer)
	OR IS_PLAYER_IN_CASINO(pPlayer)
	OR IS_PLAYER_IN_CASINO_APARTMENT(pPlayer)
	OR IS_PLAYER_IN_ARCADE_PROPERTY(pPlayer)
	#IF FEATURE_CASINO_NIGHTCLUB
	OR IS_PLAYER_IN_CASINO_NIGHTCLUB(pPlayer)
	#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_NIGHTCLUB_DRUNK_SPAWN_LOCATION(PLAYER_SPAWN_LOCATION &spawnLocation)
	VECTOR vPos
	FLOAT fHeading
	MP_OUTFITS_APPLY_DATA   sApplyData
	sApplyData.eApplyStage 	= AOS_SET
    sApplyData.pedID        = PLAYER_PED_ID()
	
	IF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_DRUNK_AWAKEN
		IF (GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON)
			SET_FOLLOW_PED_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
			g_TransitionSpawnData.bFirstPersonCam = TRUE
		ELSE
			g_TransitionSpawnData.bFirstPersonCam = FALSE
		ENDIF
		
		spawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS
		INT iRand
		
		IF IS_BIT_SET(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_SPAWN_DRUNK_MACBETH)
			IF g_sMPTunables.bBB_NIGHTCLUB_DISABLE_NIGHTCLUB_PASS_OUT_EASTER_EGG_LOCATIONS
				iRand = GET_RANDOM_INT_IN_RANGE(0, 32)
			ELSE
				iRand = GET_RANDOM_INT_IN_RANGE(0, 33)
			ENDIF
		ELSE
			iRand = GET_RANDOM_INT_IN_RANGE(0, 16)
		ENDIF
			
		#IF IS_DEBUG_BUILD
		IF g_iNightClubDrunkSpawnLocation >= 3
			IF g_iNightClubDrunkSpawnLocation = 3
				iRand = 0
			ELIF g_iNightClubDrunkSpawnLocation = 4
				iRand = 8
			ELIF g_iNightClubDrunkSpawnLocation >= 5
			AND g_iNightClubDrunkSpawnLocation <= 20
				iRand = 16 + (g_iNightClubDrunkSpawnLocation - 5)
			ELSE
				iRand = 32
			ENDIF
		ENDIF
		#ENDIF
		
		IF iRand < 16
			SWITCH iRand / 8 // Nightclub locations (8x chance)
				CASE 0
					// outside club
					SWITCH GET_PLAYERS_OWNED_NIGHTCLUB(GET_OWNER_OF_THIS_NIGHTCLUB())
						CASE NIGHTCLUB_LA_MESA
							vPos = <<727.5640, -1334.3356, 25.2523>>
							fHeading = 232.2852
						BREAK
						CASE NIGHTCLUB_MISSION_ROW
							vPos = <<271.2675, -993.7343, 28.3214>>
							fHeading = 169.689
						BREAK
						CASE NIGHTCLUB_STRAWBERRY_WAREHOUSE
							vPos = <<-155.8180, -1254.5532, 30.2960>>
							fHeading = 4.4486
						BREAK
						CASE NIGHTCLUB_WEST_VINEWOOD
							vPos = <<-16.1817, 217.7581, 105.7443>>
							fHeading = 93.4225
						BREAK
						CASE NIGHTCLUB_CYPRESS_FLATS
							vPos = <<893.8176, -2135.6902, 29.4780>>
							fHeading = 172.1012
						BREAK
						CASE NIGHTCLUB_LSIA_WAREHOUSE
							vPos = <<-701.2170, -2441.9922, 12.9444>>
							fHeading = 61.4064
						BREAK
						CASE NIGHTCLUB_ELYSIAN_ISLAND
							vPos = <<194.6711, -3213.9399, 4.7903>>
							fHeading = 9.4029
						BREAK
						CASE NIGHTCLUB_DOWNTOWN_VINEWOOD
							vPos = <<343.1679, 261.2132, 102.0987>>
							fHeading = 6.5083
						BREAK
						CASE NIGHTCLUB_DEL_PERRO_BUILDING
							vPos =  <<-1257.0354, -666.5515, 25.1249>>
							fHeading = 317.3767
						BREAK
						CASE NIGHTCLUB_VESPUCCI_CANALS
							vPos =  <<-1159.1149, -1149.7639, 1.7631>>
							fHeading = 11.9829
						BREAK
					ENDSWITCH
				BREAK
				CASE 1
				 	// loading bay
					vPos =  <<-1635.9442, -3001.7007, -79.1438>>
					fHeading = 11.9829
					g_TransitionSpawnData.bNightClubSpawn = TRUE
				BREAK
			ENDSWITCH
		ELIF iRand < 32
			
			iRand += -16
		
			// the sea is extra rare as its so remote. Only a 10th as likely as one of the other locatios
			#IF IS_DEBUG_BUILD
			IF (g_iNightClubDrunkSpawnLocation = -1)
			#ENDIF
			IF (iRand = 15)
				IF (GET_RANDOM_INT_IN_RANGE(0, 10) != 0)
					iRand = GET_RANDOM_INT_IN_RANGE(0, 15)
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD
			ENDIF
			#ENDIF
		
			SWITCH iRand // Common locations
				CASE 0	// Vinewood
					vPos =  <<711.1994, 1197.9501, 347.5316>>
					fHeading = 322.2852
				BREAK
				CASE 1	// University
					vPos =  <<-1756.5938, 187.2821, 63.3711>>
					fHeading = 252.2852
				BREAK
				CASE 2	// Fountain
					vPos =  <<185.4465, -665.3698, 42.2529>>
					fHeading = 232.2852
				BREAK
				CASE 3	// Train
					vPos =   <<1130.4802, -2209.2454, 30.6986>>
					fHeading = 305.2119
				BREAK
				CASE 4	// Golf
					vPos =  <<-1190.8842, 10.5776, 47.7357>>
					fHeading = 232.2852
				BREAK
				CASE 5	// Maze
					vPos =  <<-2302.4290, 216.8912, 166.6017>>
					fHeading = 232.2852
				BREAK
				CASE 6	// Pier
					vPos = <<-1643.6943, -1036.1293, 4.3628>>
					fHeading = 232.2852
				BREAK
				CASE 7	// Ship
					vPos = <<1011.2819, -2895.5874, 38.1619>>
					fHeading = 122.9082
				BREAK
				CASE 8	// Site
					vPos =  <<-497.6255, -1014.4177, 51.4764>>
					fHeading = 326.5754
				BREAK
				CASE 9	// Park
					vPos =  <<-770.9928, 878.1907, 202.5160>>
					fHeading = 52.2852
				BREAK
				CASE 10	// Dam
					vPos =  <<1662.3973, -52.1696, 167.3342>>
					fHeading = 315.1909
				BREAK
				CASE 11	// Underpass
					vPos =  <<-17.9905, -1222.9749, 28.5318>>
					fHeading = 232.2852
				BREAK
				CASE 12	// Stripclub
					vPos =  <<125.4297, -1276.0613, 38.6041>>
					fHeading = 298.1766
				BREAK
				CASE 13 //  Lago Zancudo
					vPos =  <<-1716.8940, 2705.0991, 0.2510>>
					fHeading = 359.9997
				BREAK
				CASE 14 // Omega's trailer
					vPos =  <<2333.5198, 2575.9441, 45.6677>>
					fHeading = 25.5624
				BREAK
				CASE 15 // sea, above sunken ufo - rare
					vPos =  <<759.8017, 7392.5156, -0.2317>>
					fHeading = 34.8039
					
					g_TransitionSpawnData.bSpawnSpecificVehicle = TRUE
					g_TransitionSpawnData.SpecificVehicleModel = SUBMERSIBLE2
					g_TransitionSpawnData.vSpecificVehicleCoords = <<758.6, 7386.9, -0.2>>
					g_TransitionSpawnData.fSpecificVehicleHeading = 78.0
					
				BREAK
			ENDSWITCH
		ELSE // Rare locations
			iRand = GET_RANDOM_INT_IN_RANGE(0, 2)
	
			#IF IS_DEBUG_BUILD
			IF g_iNightClubDrunkSpawnLocation = 21
				iRand = 0
			ELIF g_iNightClubDrunkSpawnLocation = 22
				iRand = 1
			ENDIF
			#ENDIF
			
			PED_COMP_NAME_ENUM eCurrentJbib
			SWITCH iRand
				CASE 0	// RARE - Epsilon
					vPos =  <<-718.3353, 47.4099, 64.1932>>
					fHeading = 52.2852
					
				    sApplyData.eOutfit = OUTFIT_NIGHTCLUB_KIFFLOM_0
					SET_PED_MP_OUTFIT(sApplyData)
					
					eCurrentJbib = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(sApplyData.pedID, COMP_TYPE_JBIB)
					IF eCurrentJbib != DUMMY_PED_COMP
						PRINTLN("SET_NIGHTCLUB_DRUNK_SPAWN_LOCATION - Setting Kifflom t-shirt jbib, enum=", eCurrentJbib)
						SET_PED_COMP_ITEM_CURRENT_MP(sApplyData.pedID, COMP_TYPE_JBIB, eCurrentJbib, FALSE)
					ENDIF
					
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_DRUNK_SPAWN_EPSILON_UNLOCK, TRUE)
				BREAK
				CASE 1 // RARE - Chiliad
					vPos =  <<425.9054, 5614.3174, 765.4604>>
					fHeading = 277.9661
					
					iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
					SWITCH iRand
						CASE 0
				    		sApplyData.eOutfit = OUTFIT_NIGHTCLUB_CHILIAD_0
						BREAK
						CASE 1
					    	sApplyData.eOutfit = OUTFIT_NIGHTCLUB_CHILIAD_1
						BREAK
						CASE 2
					    	sApplyData.eOutfit = OUTFIT_NIGHTCLUB_CHILIAD_2
						BREAK
					ENDSWITCH
					SET_PED_MP_OUTFIT(sApplyData)
					
					eCurrentJbib = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(sApplyData.pedID, COMP_TYPE_JBIB)
					IF eCurrentJbib != DUMMY_PED_COMP
						PRINTLN("SET_NIGHTCLUB_DRUNK_SPAWN_LOCATION - Setting Chiliad t-shirt jbib, enum=", eCurrentJbib)
						SET_PED_COMP_ITEM_CURRENT_MP(sApplyData.pedID, COMP_TYPE_JBIB, eCurrentJbib, FALSE)
					ENDIF
					
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_NIGHTCLUB_DRUNK_SPAWN_CHILIAD_UNLOCK, TRUE)
				BREAK
				CASE 2 // RARE - sea
				
				BREAK
			ENDSWITCH
		ENDIF
		
		SETUP_SPECIFIC_SPAWN_LOCATION(vPos, // VECTOR vCoords, 
										fHeading,  //	FLOAT fHeading, 
										50.0, //	FLOAT fSearchRadius=100.0, 
										TRUE,  //	BOOL bDoVisibleChecks=TRUE, 
										0.0,	 	//	FLOAT fMinDistFromCoords=0.0, 
										TRUE, 	 //	BOOL bConsiderInteriors = FALSE, 
										FALSE, //	BOOL bDoNearARoadChecks=TRUE, 
										65, 	 //	FLOAT MinDistFromOtherPlayers=65.0, 
										TRUE, 	//	BOOL bNearCentrePoint=TRUE, 
										TRUE)	 //	BOOL bIgnoreGlobalExclusionZones=FALSE)
	ELIF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_NOTHING
		spawnLocation = SPAWN_LOCATION_NEAREST_HOSPITAL
	ELSE
		spawnLocation = SPAWN_LOCATION_NIGHTCLUB
	ENDIF
ENDPROC

#IF FEATURE_CASINO
PROC SET_CASINO_DRUNK_SPAWN_LOCATION(PLAYER_SPAWN_LOCATION &spawnLocation, INT iMission = -1)
	VECTOR vPos
	FLOAT fHeading
	
	IF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_DRUNK_AWAKEN
		IF (GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON)
			SET_FOLLOW_PED_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
			g_TransitionSpawnData.bFirstPersonCam = TRUE
		ELSE
			g_TransitionSpawnData.bFirstPersonCam = FALSE
		ENDIF
		
		spawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS
		
		IF iMission != -1
			SWITCH iMission
				CASE 0
					vPos = <<799.4338, 4489.4336, 50.1044>>
					fHeading = 234.7151
				BREAK
				CASE 1
					vPos = <<-1780.2240, -898.1380, 5.5230>>
					fHeading = 0.0
				BREAK
				CASE 2
					vPos = <<548.493, 2675.173, 42.1963>>
					fHeading = -29.160
				BREAK
				CASE 3
					vPos = <<-2510.7041, 1715.8790, 153.9460>>
					fHeading = 0.0
				BREAK
			ENDSWITCH
		ELSE
			INT iRand = GET_RANDOM_INT_IN_RANGE(0, 7)
			
			#IF IS_DEBUG_BUILD
			IF g_iCasinoDrunkSpawnLocation >= 2
				iRand = g_iCasinoDrunkSpawnLocation - 2
			ENDIF
			#ENDIF
			
			SWITCH iRand
				CASE 0
					vPos = <<1095.7798, 42.0084, 79.8909>>
					fHeading = 4.1868
				BREAK
				
				CASE 1
					vPos = <<1234.7437, 348.7420, 80.9909>>
					fHeading = 64.0722
				BREAK
				
				CASE 2
					vPos = <<1078.3181, 203.7619, 86.9908>>
					fHeading = 287.8687
				BREAK
				
				CASE 3
					vPos = <<882.1516, -280.3958, 65.4134>>
					fHeading = 322.2067
				BREAK
				
				CASE 4
					vPos = <<1101.1200, -706.6118, 55.8202>>
					fHeading = 302.1545
				BREAK
				
				CASE 5
					vPos = <<958.2682, 72.5168, 111.5540>>
					fHeading = 235.6346
				BREAK
				
				CASE 6
					vPos = <<919.9286, 23.3805, 113.1937>>
					fHeading = 128.1919
				BREAK
			ENDSWITCH
		ENDIF
		
		SETUP_SPECIFIC_SPAWN_LOCATION(vPos, // VECTOR vCoords, 
										fHeading,  //	FLOAT fHeading, 
										50.0, //	FLOAT fSearchRadius=100.0, 
										TRUE,  //	BOOL bDoVisibleChecks=TRUE, 
										0.0,	 	//	FLOAT fMinDistFromCoords=0.0, 
										TRUE, 	 //	BOOL bConsiderInteriors = FALSE, 
										FALSE, //	BOOL bDoNearARoadChecks=TRUE, 
										65, 	 //	FLOAT MinDistFromOtherPlayers=65.0, 
										TRUE, 	//	BOOL bNearCentrePoint=TRUE, 
										TRUE)	 //	BOOL bIgnoreGlobalExclusionZones=FALSE)
	ELIF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_NOTHING
		spawnLocation = SPAWN_LOCATION_NEAREST_HOSPITAL
	ELIF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_BED
		spawnLocation = SPAWN_LOCATION_CASINO_APARTMENT
	ELSE
		spawnLocation = SPAWN_LOCATION_CASINO
	ENDIF
ENDPROC

PROC SET_CASINO_APARTMENT_DRUNK_SPAWN_LOCATION(PLAYER_SPAWN_LOCATION &spawnLocation)
	IF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_NOTHING
		spawnLocation = SPAWN_LOCATION_NEAREST_HOSPITAL
	ELSE
		spawnLocation = SPAWN_LOCATION_CASINO_APARTMENT
	ENDIF
ENDPROC
#ENDIF

#IF FEATURE_CASINO_HEIST
PROC SET_ARCADE_DRUNK_SPAWN_LOCATION(PLAYER_SPAWN_LOCATION &spawnLocation)
	VECTOR vPos
	FLOAT fHeading
	
	IF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_DRUNK_AWAKEN
		IF (GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON)
			SET_FOLLOW_PED_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
			g_TransitionSpawnData.bFirstPersonCam = TRUE
		ELSE
			g_TransitionSpawnData.bFirstPersonCam = FALSE
		ENDIF
		
		spawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS
		
		INT iRand = GET_RANDOM_INT_IN_RANGE(0, 13)
		
		#IF IS_DEBUG_BUILD
		IF g_iArcadeDrunkSpawnLocation >= 0
			IF g_iArcadeDrunkSpawnLocation < 2
				iRand = GET_RANDOM_INT_IN_RANGE(3, 13)
			ELSE
				iRand = g_iArcadeDrunkSpawnLocation - 2
			ENDIF
		ENDIF
		#ENDIF
		
		IF iRand = 1
			IF NOT HAS_PLAYER_PURCHASED_CASINO_VAULT_DOOR(GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))
				iRand = GET_RANDOM_INT_IN_RANGE(3, 13)
			ENDIF
		ELIF iRand = 2
			IF NOT HAS_PLAYER_PURCHASED_CASINO_KEYPAD(GET_OWNER_OF_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))
				iRand = GET_RANDOM_INT_IN_RANGE(3, 13)
			ENDIF
		ENDIF
		
		IF iRand = 0
			vPos = <<2713.5205, -369.6516, -55.7809>>
			fHeading = 268.3680
		ELIF iRand = 1
			vPos = <<2688.9507, -369.5088, -55.7809>>
			fHeading = 91.5922
		ELIF iRand = 2
			vPos = <<2692.9663, -371.0657, -55.7809>>
			fHeading = 179.1462
		ELSE
			SWITCH GET_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()
				CASE SIMPLE_INTERIOR_ARCADE_PALETO_BAY
					SWITCH iRand
						CASE 3	vPos = <<-381.2343, 6087.4692, 43.2553>>	fHeading = 250.3838		BREAK
						CASE 4	vPos = <<-425.5418, 6021.9863, 30.4892>>	fHeading = 324.5831		BREAK
						CASE 5	vPos = <<-203.9980, 6571.5278, 9.9830>>		fHeading = 57.1827		BREAK
						CASE 6	vPos = <<-218.4132, 6291.4180, 30.8026>>	fHeading = 232.5823		BREAK
						CASE 7	vPos = <<-168.9162, 6285.6729, 32.1609>>	fHeading = 223.5823		BREAK
						CASE 8	vPos = <<-115.3204, 6135.6450, 30.3338>>	fHeading = 94.9821		BREAK
						CASE 9	vPos = <<-286.3890, 6018.2905, 30.5139>>	fHeading = 328.9819		BREAK
						CASE 10	vPos = <<-339.1764, 6169.3633, 31.2380>>	fHeading = 275.1812		BREAK
						CASE 11	vPos = <<-248.0970, 6330.9292, 31.4262>>	fHeading = 243.1810		BREAK
						CASE 12	vPos = <<-32.7349, 6309.1079, 30.3751>>		fHeading = 73.1871		BREAK
					ENDSWITCH
				BREAK
				
				CASE SIMPLE_INTERIOR_ARCADE_GRAPESEED
					SWITCH iRand
						CASE 3	vPos = <<1710.7552, 4791.7739, 40.9839>>	fHeading = 288.9804		BREAK
						CASE 4	vPos = <<1667.2904, 4912.5542, 37.2549>>	fHeading = 258.3803		BREAK
						CASE 5	vPos = <<1849.9841, 4573.6758, 30.0707>>	fHeading = 143.3299		BREAK
						CASE 6	vPos = <<1796.4946, 4606.3188, 36.1828>>	fHeading = 332.9294		BREAK
						CASE 7	vPos = <<1708.1884, 4725.8447, 41.1803>>	fHeading = 58.1289		BREAK
						CASE 8	vPos = <<1648.1738, 4890.8340, 47.6073>>	fHeading = 201.7286		BREAK
						CASE 9	vPos = <<1860.3162, 4969.1870, 51.9288>>	fHeading = 282.9284		BREAK
						CASE 10	vPos = <<1900.5627, 4915.3789, 53.8172>>	fHeading = 350.9277		BREAK
						CASE 11	vPos = <<1802.0336, 4771.0054, 32.9380>>	fHeading = 65.7275		BREAK
						CASE 12	vPos = <<1710.0496, 4927.9937, 41.0782>>	fHeading = 326.1272		BREAK
					ENDSWITCH
				BREAK
				
				CASE SIMPLE_INTERIOR_ARCADE_DAVIS
					SWITCH iRand
						CASE 3	vPos = <<-142.1464, -1866.9198, 0.4590>>	fHeading = 327.1995		BREAK
						CASE 4	vPos = <<124.4862, -1945.2262, 19.7527>>	fHeading = 347.1993		BREAK
						CASE 5	vPos = <<-53.2032, -1748.5211, 28.4210>>	fHeading = 318.5987		BREAK
						CASE 6	vPos = <<-60.4643, -1748.7953, 28.3319>>	fHeading = 228.7987		BREAK
						CASE 7	vPos = <<-88.3165, -1616.4154, 28.5607>>	fHeading = 174.9975		BREAK
						CASE 8	vPos = <<-280.0523, -1913.2723, 28.9461>>	fHeading = 309.5966		BREAK
						CASE 9	vPos = <<-303.1479, -1641.6226, 31.1064>>	fHeading = 210.5959		BREAK
						CASE 10	vPos = <<-235.6025, -1680.5985, 32.7475>>	fHeading = 128.7958		BREAK
						CASE 11	vPos = <<35.9701, -1782.9384, 46.7003>>		fHeading = 305.1954		BREAK
						CASE 12	vPos = <<-131.8969, -1657.4611, 32.1084>>	fHeading = 179.1952		BREAK
					ENDSWITCH
				BREAK
				
				CASE SIMPLE_INTERIOR_ARCADE_WEST_VINEWOOD
					SWITCH iRand
						CASE 3	vPos = <<-622.2095, 311.5841, 82.9342>>		fHeading = 192.9910		BREAK
						CASE 4	vPos = <<-664.3320, 239.5435, 156.1313>>	fHeading = 173.9910		BREAK
						CASE 5	vPos = <<-603.1324, 245.4072, 81.0443>>		fHeading = 0.7904		BREAK
						CASE 6	vPos = <<-561.6140, 294.5927, 86.4986>>		fHeading = 267.7899		BREAK
						CASE 7	vPos = <<-478.0753, 177.1927, 82.1619>>		fHeading = 64.5897		BREAK
						CASE 8	vPos = <<-655.8604, 414.8935, 100.2315>>	fHeading = 253.7894		BREAK
						CASE 9	vPos = <<-507.8819, 393.8386, 100.2710>>	fHeading = 246.5893		BREAK
						CASE 10	vPos = <<-340.6525, 266.8995, 84.6795>>		fHeading = 124.7891		BREAK
						CASE 11	vPos = <<-369.1306, 221.3598, 83.3430>>		fHeading = 119.1886		BREAK
						CASE 12	vPos = <<-583.2770, 97.9745, 67.2581>>		fHeading = 222.3884		BREAK
					ENDSWITCH
				BREAK
				
				CASE SIMPLE_INTERIOR_ARCADE_ROCKFORD_HILLS
					SWITCH iRand
						CASE 3	vPos = <<-1288.2439, -249.0781, 59.6541>>	fHeading = 268.5882		BREAK
						CASE 4	vPos = <<-1314.0894, -136.2907, 49.4698>>	fHeading = 278.1880		BREAK
						CASE 5	vPos = <<-1428.3971, -211.2872, 45.5004>>	fHeading = 97.5875		BREAK
						CASE 6	vPos = <<-1299.1909, -390.6986, 44.4827>>	fHeading = 100.5873		BREAK
						CASE 7	vPos = <<-1118.3445, -269.1775, 38.0402>>	fHeading = 306.7865		BREAK
						CASE 8	vPos = <<-1237.4744, -189.5266, 40.6373>>	fHeading = 306.5865		BREAK
						CASE 9	vPos = <<-1519.9441, -408.9171, 34.5970>>	fHeading = 202.1859		BREAK
						CASE 10	vPos = <<-1196.8682, -496.7150, 34.5207>>	fHeading = 273.4812		BREAK
						CASE 11	vPos = <<-1433.2263, -447.2096, 48.5659>>	fHeading = 216.9854		BREAK
						CASE 12	vPos = <<-1483.4082, -315.2310, 45.9316>>	fHeading = 318.7846		BREAK
					ENDSWITCH
				BREAK
				
				CASE SIMPLE_INTERIOR_ARCADE_LA_MESA
					SWITCH iRand
						CASE 3	vPos = <<726.5114, -791.1262, 23.9457>>		fHeading = 110.4701		BREAK
						CASE 4	vPos = <<708.6356, -826.8448, 23.5924>>		fHeading = 275.7948		BREAK
						CASE 5	vPos = <<704.7261, -797.5947, 48.3916>>		fHeading = 27.9944		BREAK
						CASE 6	vPos = <<621.4761, -633.9512, 13.0702>>		fHeading = 132.7940		BREAK
						CASE 7	vPos = <<659.8043, -689.5753, 25.1006>>		fHeading = 344.5935		BREAK
						CASE 8	vPos = <<824.2381, -800.0920, 25.3342>>		fHeading = 203.9931		BREAK
						CASE 9	vPos = <<839.4452, -851.1993, 25.6199>>		fHeading = 11.3927		BREAK
						CASE 10	vPos = <<906.2900, -971.5958, 58.0942>>		fHeading = 58.9927		BREAK
						CASE 11	vPos = <<685.2653, -959.4067, 22.2786>>		fHeading = 186.7921		BREAK
						CASE 12	vPos = <<843.2097, -1020.0344, 26.5320>>	fHeading = 192.9913		BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		ENDIF
		
		SETUP_SPECIFIC_SPAWN_LOCATION(vPos, // VECTOR vCoords, 
										fHeading,  //	FLOAT fHeading, 
										50.0, //	FLOAT fSearchRadius=100.0, 
										TRUE,  //	BOOL bDoVisibleChecks=TRUE, 
										0.0,	 	//	FLOAT fMinDistFromCoords=0.0, 
										TRUE, 	 //	BOOL bConsiderInteriors = FALSE, 
										FALSE, //	BOOL bDoNearARoadChecks=TRUE, 
										65, 	 //	FLOAT MinDistFromOtherPlayers=65.0, 
										TRUE, 	//	BOOL bNearCentrePoint=TRUE, 
										TRUE)	 //	BOOL bIgnoreGlobalExclusionZones=FALSE)
	ELSE
		spawnLocation = SPAWN_LOCATION_ARCADE
	ENDIF
ENDPROC
#ENDIF

#IF FEATURE_CASINO_NIGHTCLUB
PROC SET_CASINO_NIGHTCLUB_DRUNK_SPAWN_LOCATION(PLAYER_SPAWN_LOCATION &spawnLocation, INT iMission = -1)
	VECTOR vPos
	FLOAT fHeading
	
	IF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_DRUNK_AWAKEN
		IF (GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON)
			SET_FOLLOW_PED_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
			g_TransitionSpawnData.bFirstPersonCam = TRUE
		ELSE
			g_TransitionSpawnData.bFirstPersonCam = FALSE
		ENDIF
		
		spawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS
		
		IF iMission != -1
			SWITCH iMission
				CASE 0
					vPos = <<799.4338, 4489.4336, 50.1044>>
					fHeading = 234.7151
				BREAK
				CASE 1
					vPos = <<-1780.2240, -898.1380, 5.5230>>
					fHeading = 0.0
				BREAK
				CASE 2
					vPos = <<548.493, 2675.173, 42.1963>>
					fHeading = -29.160
				BREAK
				CASE 3
					vPos = <<-2510.7041, 1715.8790, 153.9460>>
					fHeading = 0.0
				BREAK
			ENDSWITCH
		ELSE
			INT iRand = GET_RANDOM_INT_IN_RANGE(0, 7)
			
			#IF IS_DEBUG_BUILD
			IF g_iCasinoNightclubDrunkSpawnLocation >= 1
				iRand = g_iCasinoNightclubDrunkSpawnLocation - 1
			ENDIF
			#ENDIF
			
			SWITCH iRand
				CASE 0
					vPos = <<1095.7798, 42.0084, 79.8909>>
					fHeading = 4.1868
				BREAK
				
				CASE 1
					vPos = <<1234.7437, 348.7420, 80.9909>>
					fHeading = 64.0722
				BREAK
				
				CASE 2
					vPos = <<1078.3181, 203.7619, 86.9908>>
					fHeading = 287.8687
				BREAK
				
				CASE 3
					vPos = <<882.1516, -280.3958, 65.4134>>
					fHeading = 322.2067
				BREAK
				
				CASE 4
					vPos = <<1101.1200, -706.6118, 55.8202>>
					fHeading = 302.1545
				BREAK
				
				CASE 5
					vPos = <<958.2682, 72.5168, 111.5540>>
					fHeading = 235.6346
				BREAK
				
				CASE 6
					vPos = <<919.9286, 23.3805, 113.1937>>
					fHeading = 128.1919
				BREAK
			ENDSWITCH
		ENDIF
		
		SETUP_SPECIFIC_SPAWN_LOCATION(vPos, // VECTOR vCoords, 
										fHeading,  //	FLOAT fHeading, 
										50.0, //	FLOAT fSearchRadius=100.0, 
										TRUE,  //	BOOL bDoVisibleChecks=TRUE, 
										0.0,	 	//	FLOAT fMinDistFromCoords=0.0, 
										TRUE, 	 //	BOOL bConsiderInteriors = FALSE, 
										FALSE, //	BOOL bDoNearARoadChecks=TRUE, 
										65, 	 //	FLOAT MinDistFromOtherPlayers=65.0, 
										TRUE, 	//	BOOL bNearCentrePoint=TRUE, 
										TRUE)	 //	BOOL bIgnoreGlobalExclusionZones=FALSE)
	ELIF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_NOTHING
		spawnLocation = SPAWN_LOCATION_NEAREST_HOSPITAL
	ELSE
		spawnLocation = SPAWN_LOCATION_CASINO_NIGHTCLUB
	ENDIF
ENDPROC
#ENDIF

#IF FEATURE_HEIST_ISLAND
PROC SET_ISLAND_DRUNK_SPAWN_LOCATION(PLAYER_SPAWN_LOCATION &spawnLocation)
	VECTOR vPos
	FLOAT fHeading
	
	IF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_DRUNK_AWAKEN
		IF (GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON)
			SET_FOLLOW_PED_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
			g_TransitionSpawnData.bFirstPersonCam = TRUE
		ELSE
			g_TransitionSpawnData.bFirstPersonCam = FALSE
		ENDIF
		
		spawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS
		
		INT iRand
		
		IF GB_IS_PLAYER_ON_ISLAND_HEIST_SCOPING_MISSION(PLAYER_ID())
			iRand = GET_RANDOM_INT_IN_RANGE(0, 11)
		ELSE
			iRand = GET_RANDOM_INT_IN_RANGE(0, 100)
			
			IF iRand >= 95
				iRand = GET_RANDOM_INT_IN_RANGE(11, 17)
			ELSE
				iRand = GET_RANDOM_INT_IN_RANGE(0, 11)
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF g_iBeachPartyDrunkSpawnLocation != -1
			iRand = g_iBeachPartyDrunkSpawnLocation
		ENDIF
		#ENDIF
		
		SWITCH iRand
			CASE 0
				vPos = <<4901.6558, -4962.1660, 2.3865>>
				fHeading = 322.1187
				CLEAR_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
			
			CASE 1
				vPos = <<4907.8857, -4950.0596, 2.3594>>
				fHeading = 165.4940
				CLEAR_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
			
			CASE 2
				vPos = <<4913.5400, -4933.4331, 2.3674>>
				fHeading = 28.6042
				CLEAR_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
			
			CASE 3
				vPos = <<4930.0210, -4915.6489, 2.6046>>
				fHeading = 79.9281
				CLEAR_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
			
			CASE 4
				vPos = <<4892.8833, -4890.1636, 2.7236>>
				fHeading = 237.4997
				CLEAR_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
			
			CASE 5
				vPos = <<4879.4727, -4893.1270, 2.6050>>
				fHeading = 217.5422
				CLEAR_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
			
			CASE 6
				vPos = <<4860.7817, -4902.5039, 0.7511>>
				fHeading = 260.3797
				CLEAR_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
			
			CASE 7
				vPos = <<4866.5356, -4900.0527, 1.8086>>
				fHeading = 206.0599
				CLEAR_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
			
			CASE 8
				vPos = <<4884.0366, -4960.4380, 3.1734>>
				fHeading = 351.5502
				CLEAR_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
			
			CASE 9
				vPos = <<4948.7402, -4886.1313, 2.7244>>
				fHeading = 108.8048
				CLEAR_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
			
			CASE 10
				vPos = <<4936.1680, -4901.3242, 2.6151>>
				fHeading = 109.0038
				CLEAR_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
			
			CASE 11
				vPos = <<-1355.0309, -1674.0433, 1.1252>>
				fHeading = 313.8870
				g_TransitionSpawnData.iSyncScenePosition = 0
				SET_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
			
			CASE 12
				vPos = <<-1409.1108, -1574.2098, 1.1427>>
				fHeading = 302.7025
				g_TransitionSpawnData.iSyncScenePosition = 1
				SET_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
			
			CASE 13
				vPos = <<-1559.4794, -1230.7761, 0.6671>>
				fHeading = 302.0941
				g_TransitionSpawnData.iSyncScenePosition = 2
				SET_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
			
			CASE 14
				vPos = <<-1545.4159, -1251.4567, 1.0141>>
				fHeading = 309.1964
				g_TransitionSpawnData.iSyncScenePosition = 3
				SET_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
			
			CASE 15
				vPos = <<-1516.5511, -1326.3306, 1.0825>>
				fHeading = 290.5325
				g_TransitionSpawnData.iSyncScenePosition = 4
				SET_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
			
			CASE 16
				vPos = <<-1500.7712, -1391.5809, 0.8837>>
				fHeading = 287.5468
				g_TransitionSpawnData.iSyncScenePosition = 5
				SET_BIT(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_SPAWNING_OFF_ISLAND)
			BREAK
		ENDSWITCH
		
		SETUP_SPECIFIC_SPAWN_LOCATION(vPos, // VECTOR vCoords, 
										fHeading,  //	FLOAT fHeading, 
										50.0, //	FLOAT fSearchRadius=100.0, 
										TRUE,  //	BOOL bDoVisibleChecks=TRUE, 
										0.0,	 	//	FLOAT fMinDistFromCoords=0.0, 
										TRUE, 	 //	BOOL bConsiderInteriors = FALSE, 
										FALSE, //	BOOL bDoNearARoadChecks=TRUE, 
										65, 	 //	FLOAT MinDistFromOtherPlayers=65.0, 
										TRUE, 	//	BOOL bNearCentrePoint=TRUE, 
										TRUE)	 //	BOOL bIgnoreGlobalExclusionZones=FALSE)
	ENDIF
ENDPROC
#ENDIF

#IF FEATURE_FIXER
#IF FEATURE_MUSIC_STUDIO
PROC SET_MUSIC_STUDIO_DRUNK_SPAWN_LOCATION(PLAYER_SPAWN_LOCATION &spawnLocation, INT iMission = -1)
	UNUSED_PARAMETER(iMission)
	
	VECTOR vPos
	FLOAT fHeading
			
	IF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_DRUNK_AWAKEN
		IF (GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON)
			SET_FOLLOW_PED_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON)
			g_TransitionSpawnData.bFirstPersonCam = TRUE
		ELSE
			g_TransitionSpawnData.bFirstPersonCam = FALSE
		ENDIF
		
		spawnLocation = SPAWN_LOCATION_AT_SPECIFIC_COORDS

		INT iRand = GET_RANDOM_INT_IN_RANGE(0, 11)
		
		#IF IS_DEBUG_BUILD
		IF g_iMusicStudioDrunkSpawnLocation >= 1
			iRand = g_iMusicStudioDrunkSpawnLocation - 1
		ENDIF
		#ENDIF
		
		MP_SPAWN_POINT spawnPoints[11]
		GetMusicStudioDrunkRespawns(spawnPoints)
		
		vPos = spawnPoints[iRand].Pos
		fHeading = spawnPoints[iRand].Heading
		
		
		SETUP_SPECIFIC_SPAWN_LOCATION(vPos, // VECTOR vCoords, 
										fHeading,  //	FLOAT fHeading, 
										50.0, //	FLOAT fSearchRadius=100.0, 
										TRUE,  //	BOOL bDoVisibleChecks=TRUE, 
										0.0,	 	//	FLOAT fMinDistFromCoords=0.0, 
										TRUE, 	 //	BOOL bConsiderInteriors = FALSE, 
										FALSE, //	BOOL bDoNearARoadChecks=TRUE, 
										65, 	 //	FLOAT MinDistFromOtherPlayers=65.0, 
										TRUE, 	//	BOOL bNearCentrePoint=TRUE, 
										TRUE)	 //	BOOL bIgnoreGlobalExclusionZones=FALSE)
	ELIF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_NOTHING
		spawnLocation = SPAWN_LOCATION_NEAREST_HOSPITAL
	ELSE
		spawnLocation = SPAWN_LOCATION_MUSIC_STUDIO
	ENDIF
ENDPROC
#ENDIF
#ENDIF

PROC UPDATE_DRUNK_RESPAWN()

	BOOL bCleanup = FALSE
	
	IF (g_SpawnData.bPassedOutDrunk)
	
		// if we are getting pulled into a group warp then cleanup immediately. 3038397
		IF IS_LOCAL_PLAYER_DOING_A_GROUP_WARP()
		OR IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		OR IS_PLAYER_IN_CORONA()
			PRINTLN("[group_warp] UPDATE_DRUNK_RESPAWN - pulled into a group warp, cleanup flags immediately.")
			g_SpawnData.bPassedOutDrunk = FALSE
			g_SpawnData.iDrunkRespawnYachtState = 0
			g_SpawnData.iDrunkRespawnState = 0
			g_SpawnData.eDrunkSpawnLocation = SPAWN_LOCATION_AUTOMATIC
			g_SpawnData.bPassedOutDrunkWithWantedLevel = FALSE
			RESET_SPAWNING_INTERACTION_GLOBALS()
			EXIT
		ENDIF
	
		IF NOT Is_Player_Currently_On_MP_Heist(PLAYER_ID())
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		ELSE
			PRINTLN("[UPDATE_DRUNK_RESPAWN] Not clearing wanted level as on heist")
		ENDIF
		
		IF IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_PEYOTE)
			IF (g_SpawnData.iDrunkRespawnState = 0)
				PRINTLN("[UPDATE_DRUNK_RESPAWN] PEYOTE - Setting coords")
				
				INT iRandomPeyote = GET_RANDOM_INT_IN_RANGE(0, 76)
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_COLLECTABLE_Peyote_COORDINATES(iRandomPeyote))
				
				i_Peyote_collectables_currently_close_to = iRandomPeyote
				
				PRINTLN("[UPDATE_DRUNK_RESPAWN] PEYOTE - i_Peyote_collectables_currently_close_to = ", i_Peyote_collectables_currently_close_to)
				
				g_SpawnData.iDrunkRespawnState++
			ELIF (g_SpawnData.iDrunkRespawnState = 1)
				collectables_missiondata_main.bturnintoanimal = TRUE
				
				PRINTLN("[UPDATE_DRUNK_RESPAWN] PEYOTE - bturnintoanimal = TRUE (1)")
				
				g_SpawnData.iDrunkRespawnState++
			ELIF (g_SpawnData.iDrunkRespawnState = 2)
				IF IS_LOCAL_PLAYER_AN_ANIMAL()
				AND NOT collectables_missiondata_main.bturnintoanimal
					PRINTLN("[UPDATE_DRUNK_RESPAWN] PEYOTE - Player is now animal, cleaning up")
					
					bCleanup = TRUE
				ENDIF
			ENDIF
		ELIF IS_LOCAL_PLAYER_ON_ANY_YACHT()
			IF IS_LOCAL_PLAYER_ON_THEIR_YACHT()
			
				CPRINTLN(DEBUG_YACHT, "UPDATE_DRUNK_RESPAWN - respawning on own yacht. g_SpawnData.iDrunkRespawnYachtState = ", g_SpawnData.iDrunkRespawnYachtState)
			
				// set spawn activity
				IF (g_SpawnData.iDrunkRespawnYachtState = 0)
					SET_SPAWN_ACTIVITY(SPAWN_ACTIVITY_BED)
					g_TransitionSpawnData.bIgnoreSpawnActivity = FALSE
					g_SpawnData.iDrunkRespawnYachtState++
				ENDIF
			
				// do warp
				IF (g_SpawnData.iDrunkRespawnYachtState = 1)
					IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_PRIVATE_YACHT_APARTMENT, FALSE)
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "UPDATE_DRUNK_RESPAWN: WARP_TO_SPAWN_LOCATION = TRUE")
						g_SpawnData.iDrunkRespawnYachtState++
//					ELSE
//						CDEBUG1LN(DEBUG_SAFEHOUSE, "UPDATE_DRUNK_RESPAWN: WARP_TO_SPAWN_LOCATION = FALSE")
					ENDIF
				ENDIF
			
				// wait for spawn activity to be ready
				IF (g_SpawnData.iDrunkRespawnYachtState = 2)
					
					IF IS_SPAWN_ACTIVITY_READY() 
					OR GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_NOTHING
					
						g_SpawnData.iDrunkRespawnYachtState = 0
						g_SpawnData.bPassedOutDrunk = FALSE
						bTransitionSpawningInProperty = FALSE
					
					ENDIF
				ENDIF
				
			ELSE
				g_SpawnData.iYachtToWarpTo = GET_YACHT_LOCAL_PLAYER_IS_ON()
				
				CPRINTLN(DEBUG_YACHT, "UPDATE_DRUNK_RESPAWN - respawning on friend yacht ", g_SpawnData.iYachtToWarpTo)
				
				IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_PRIVATE_FRIEND_YACHT, FALSE)
					bCleanup = TRUE
				ENDIF
			ENDIF
		ELIF (IS_PLAYER_IN_TOILET_PROPERTY(PLAYER_ID())
		#IF FEATURE_HEIST_ISLAND
		OR IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
		#ENDIF
		#IF FEATURE_FIXER
		#IF FEATURE_MUSIC_STUDIO
		OR IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
		#ENDIF
		#ENDIF
		OR g_SpawnData.eDrunkSpawnLocation != SPAWN_LOCATION_AUTOMATIC)
		AND g_SpawnData.bDrunkRespawnSafe
			IF (g_SpawnData.iDrunkRespawnState = 0)
				IF NOT IS_TOILET_RESPAWN_SAFE()
				#IF FEATURE_CASINO
				AND NOT IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
				#ENDIF
					g_SpawnData.bDrunkRespawnSafe = FALSE
					
					EXIT
				ENDIF
				
				IF IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PLAYER_ID())
					// inside own property?
					IF (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty = GET_OWNED_PROPERTY(PROPERTY_OWNED_SLOT_CLUBHOUSE))
					
					ELSE
						// someone else's property?
						g_SpawnData.iPropertyToSpawnIntoToilet = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
					ENDIF
				ENDIF
				
				g_SpawnData.iDrunkRespawnState++
			ENDIF
			
			// set spawn activity
			IF (g_SpawnData.iDrunkRespawnState = 1)
				
				IF IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
					DECIDE_ACITIVITY_FOR_SPAWNING_DRUNK_FROM_NIGHTCLUB()
					SET_NIGHTCLUB_DRUNK_SPAWN_LOCATION(g_SpawnData.eDrunkSpawnLocation)
					CLEAR_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_SPAWN_DRUNK_MACBETH)
				ELIF IS_PLAYER_IN_CASINO(PLAYER_ID())
					DECIDE_ACITIVITY_FOR_SPAWNING_DRUNK_FROM_CASINO()
					
					IF NOT g_SpawnData.bLaunchMission
						SET_CASINO_DRUNK_SPAWN_LOCATION(g_SpawnData.eDrunkSpawnLocation)
						CLEAR_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_SPAWN_DRUNK_MACBETH)
					ENDIF
				ELIF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
					DECIDE_ACITIVITY_FOR_SPAWNING_DRUNK_FROM_CASINO_APARTMENT()
					SET_CASINO_APARTMENT_DRUNK_SPAWN_LOCATION(g_SpawnData.eDrunkSpawnLocation)
					CLEAR_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_SPAWN_DRUNK_MACBETH)
				ELIF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
					DECIDE_ACITIVITY_FOR_SPAWNING_DRUNK_FROM_ARCADE()
					SET_ARCADE_DRUNK_SPAWN_LOCATION(g_SpawnData.eDrunkSpawnLocation)
					CLEAR_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_SPAWN_DRUNK_MACBETH)
				#IF FEATURE_CASINO_NIGHTCLUB
				ELIF IS_PLAYER_IN_CASINO_NIGHTCLUB(PLAYER_ID())
					DECIDE_ACITIVITY_FOR_SPAWNING_DRUNK_FROM_CASINO_NIGHTCLUB()
					
					IF NOT g_SpawnData.bLaunchMission
						SET_CASINO_NIGHTCLUB_DRUNK_SPAWN_LOCATION(g_SpawnData.eDrunkSpawnLocation)
						CLEAR_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_SPAWN_DRUNK_MACBETH)
					ENDIF
				#ENDIF
				#IF FEATURE_HEIST_ISLAND
				ELIF IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
					DECIDE_ACITIVITY_FOR_SPAWNING_DRUNK_FROM_ISLAND()
					SET_ISLAND_DRUNK_SPAWN_LOCATION(g_SpawnData.eDrunkSpawnLocation)
					CLEAR_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_SPAWN_DRUNK_MACBETH)
				#ENDIF
				#IF FEATURE_FIXER
				#IF FEATURE_MUSIC_STUDIO
				ELIF IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
					DECIDE_ACITIVITY_FOR_SPAWNING_DRUNK_FROM_MUSIC_STUDIO()
					SET_MUSIC_STUDIO_DRUNK_SPAWN_LOCATION(g_SpawnData.eDrunkSpawnLocation)
					CLEAR_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_SPAWN_DRUNK_MACBETH)
				#ENDIF
				#ENDIF			
				ELSE
					SET_SPAWN_ACTIVITY(SPAWN_ACTIVITY_TOILET_SPEW)
				ENDIF
				
				IF IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PLAYER_ID())
					g_SpawnData.eDrunkSpawnLocation = SPAWN_LOCATION_CLUBHOUSE
				ENDIF
				
				g_TransitionSpawnData.bIgnoreSpawnActivity = FALSE		
				g_SpawnData.iDrunkRespawnState++	
			ENDIF
			
			// warp
			IF (g_SpawnData.iDrunkRespawnState = 2)				
				PRINTLN("UPDATE_DRUNK_RESPAWN - spawning at ", g_SpawnData.eDrunkSpawnLocation)
				
				BOOL bDoQuickWarp = FALSE
				
				#IF FEATURE_CASINO
				IF (IS_PLAYER_IN_CASINO(PLAYER_ID()) AND GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_DRUNK_AWAKEN)
				#IF FEATURE_CASINO_NIGHTCLUB
				OR (IS_PLAYER_IN_CASINO(PLAYER_ID()) AND GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_DRUNK_AWAKEN)
				#ENDIF
				#IF FEATURE_FIXER
				#IF FEATURE_MUSIC_STUDIO
				OR (IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID()) AND GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_DRUNK_AWAKEN)
				#ENDIF
				#ENDIF
					IF NOT IS_SPAWN_ACTIVITY_READY()
						EXIT
					ELSE
						CLEAR_BIT(g_simpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_SPAWN_DRUNK_MACBETH)
					ENDIF
				ENDIF
				#ENDIF
				
				IF (IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
				AND g_SpawnData.eDrunkSpawnLocation = SPAWN_LOCATION_NIGHTCLUB)
				ELIF (IS_PLAYER_IN_CASINO(PLAYER_ID())
				AND g_SpawnData.eDrunkSpawnLocation = SPAWN_LOCATION_CASINO)
				ELIF (IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
				AND g_SpawnData.eDrunkSpawnLocation = SPAWN_LOCATION_ARCADE)
				#IF FEATURE_CASINO_NIGHTCLUB
				ELIF (IS_PLAYER_IN_CASINO_NIGHTCLUB(PLAYER_ID())
				AND g_SpawnData.eDrunkSpawnLocation = SPAWN_LOCATION_CASINO_NIGHTCLUB)
				#ENDIF
				#IF FEATURE_FIXER
				#IF FEATURE_MUSIC_STUDIO
				ELIF (IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
				AND g_SpawnData.eDrunkSpawnLocation = SPAWN_LOCATION_MUSIC_STUDIO)
				#ENDIF
				#ENDIF
					bDoQuickWarp = TRUE
				ENDIF
				
				IF WARP_TO_SPAWN_LOCATION(g_SpawnData.eDrunkSpawnLocation, FALSE, DEFAULT, DEFAULT, bDoQuickWarp, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
					IF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_BED
					AND g_SpawnData.eDrunkSpawnLocation = SPAWN_LOCATION_CASINO_APARTMENT
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
					ENDIF
					
					g_SpawnData.iDrunkRespawnState++	
				ENDIF
			ENDIF
			
			// wait for spawn activity to be ready
			IF (g_SpawnData.iDrunkRespawnState = 3)				
				IF NOT (g_bNightClubPedsReadyToView = FALSE AND g_SpawnData.eDrunkSpawnLocation = SPAWN_LOCATION_NIGHTCLUB)
				#IF FEATURE_CASINO_HEIST
				AND NOT (g_bInitArcadePedsCreated = FALSE AND g_SpawnData.eDrunkSpawnLocation = SPAWN_LOCATION_ARCADE)
				#ENDIF
					IF IS_SPAWN_ACTIVITY_READY() 
					OR GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_NOTHING
						IF GET_SPAWN_ACTIVITY() != SPAWN_ACTIVITY_TOILET_SPEW
							IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
								#IF FEATURE_CASINO
								IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_CASINO
									SWITCH INT_TO_ENUM(GBC_SUBVARIATION, GB_GET_CONTRABAND_SUBVARIATION_PLAYER_IS_ON(PLAYER_ID()))
										CASE CSS_PO_CHILIAD
											SET_GAMEPLAY_CAM_RELATIVE_HEADING(5.0473)
											SET_GAMEPLAY_CAM_RELATIVE_PITCH(-12.9612)
										BREAK
										
										CASE CSS_PO_VESPUCCI_BEACH
											SET_GAMEPLAY_CAM_RELATIVE_HEADING(-14.7322)
											SET_GAMEPLAY_CAM_RELATIVE_PITCH(-15.7239)
										BREAK
										
										CASE CSS_PO_HARMONY
											SET_GAMEPLAY_CAM_RELATIVE_HEADING(138.4551)
											SET_GAMEPLAY_CAM_RELATIVE_PITCH(-20.1889)
										BREAK
										
										CASE CSS_PO_TONGVA_HILLS
											SET_GAMEPLAY_CAM_RELATIVE_HEADING(31.5901)
											SET_GAMEPLAY_CAM_RELATIVE_PITCH(-28.1197)
										BREAK
									ENDSWITCH
								ENDIF
								#ENDIF
								
								bCleanup = TRUE
							ELSE
								PRINTLN("[toilet_respawn] UPDATE_DRUNK_RESPAWN - Waiting for load scene")
							ENDIF
						ELSE
							g_SpawnData.iDrunkRespawnState = 0
							g_SpawnData.eDrunkSpawnLocation = SPAWN_LOCATION_AUTOMATIC
							g_SpawnData.bPassedOutDrunk = FALSE			
							bTransitionSpawningInProperty = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT g_SpawnData.bDrunkRespawnSafe
				PRINTLN("[toilet_respawn] UPDATE_DRUNK_RESPAWN - toilet respawn is unsafe, defaulting to police/hospital")
				IF GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_TOILET_SPEW
					CLEAR_SPAWN_ACTIVITY_FLAGS()
					SET_SPAWN_ACTIVITY(SPAWN_ACTIVITY_NOTHING)
				ENDIF
			ENDIF
			
			IF (g_SpawnData.bPassedOutDrunkWithWantedLevel)
				IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_NEAREST_POLICE_STATION, FALSE)
					bCleanup = TRUE
				ENDIF
			ELSE
				IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_NEAREST_HOSPITAL, FALSE)
					bCleanup = TRUE	
				ENDIF
			ENDIF
		
		ENDIF
			
		IF (bCleanup)
			IF IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
			
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			// try and force some scenarios to load in.
			SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), 50.0, 30)
			
			// make sure camera is set behind player
			SetGameCamBehindPlayer()				
			
			IF NOT g_SpawnData.bSkipClearingSpawn
				CLEAR_SPECIFIC_SPAWN_LOCATION()
			ENDIF
			
			g_SpawnData.iDrunkRespawnState = 0
			g_SpawnData.eDrunkSpawnLocation = SPAWN_LOCATION_AUTOMATIC
			g_TransitionSpawnData.bCasinoAptSpawn = FALSE
			g_TransitionSpawnData.bArcadeSpawn = FALSE		
			g_TransitionSpawnData.bSubmarineSpawn = FALSE
			g_TransitionSpawnData.bCarMeetSpawn = FALSE
			g_TransitionSpawnData.bAutoShopSpawn = FALSE			
			#IF FEATURE_FIXER
			g_TransitionSpawnData.bFixerHQSpawn = FALSE
			#IF FEATURE_MUSIC_STUDIO
			g_TransitionSpawnData.bMusicStudioSpawn = FALSE
			#ENDIF
			#ENDIF
			
			g_SpawnData.bPassedOutDrunk = FALSE
			bTransitionSpawningInProperty = FALSE
			g_SpawnData.bDrunkRespawnSafe = TRUE
			
			CLEAR_BIT(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_PEYOTE)
			CLEAR_BIT(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_DRANK_PEYOTE)
		ENDIF
			
	ENDIF
ENDPROC

FUNC BOOL IS_SPAWNING_IN_HELI_NON_RACE()
	IF IS_SPAWNING_IN_VEHICLE()
	AND IS_THIS_MODEL_A_HELI(g_SpawnData.MissionSpawnDetails.SpawnModel)
	AND NOT (g_b_On_Race)
		NET_PRINT("IS_SPAWNING_IN_HELI_NON_RACE = TRUE") NET_NL()
		RETURN(TRUE)
	ENDIF
	NET_PRINT("IS_SPAWNING_IN_HELI_NON_RACE = FALSE") NET_NL()
	RETURN(FALSE)
ENDFUNC

PROC PLAYER_RESPAWN_DO_LOAD_SCENE_IF_NECESSARY()
	
	NET_PRINT("PLAYER_RESPAWN_DO_LOAD_SCENE_IF_NECESSARY - called... ") NET_NL()
	DEBUG_PRINTCALLSTACK()

	// if far away from death position do load scene	
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		IF SHOULD_RESPAWN_DO_A_LOAD_SCENE(g_SpawnData.vBeforeRespawnCoords, g_SpawnData.vCandSpawnPoint)	
			SET_FOCUS_POS_AND_VEL(g_SpawnData.vCandSpawnPoint, <<0,0,0>>)
			IF IS_SPAWNING_IN_HELI_NON_RACE()
				NEW_LOAD_SCENE_START_SPHERE(g_SpawnData.vCandSpawnPoint, 600.0)
			ELSE
				FLOAT fDist
				fDist = VDIST(<<g_SpawnData.vBeforeRespawnCoords.x, g_SpawnData.vBeforeRespawnCoords.y, 0.0>>, <<g_SpawnData.vCandSpawnPoint.x, g_SpawnData.vCandSpawnPoint.y, 0.0>>)
				IF (fDist < 50.0)
					fDist = 50.0
				ENDIF
				IF (fDist > 4500.0)
					fDist = 4500.0
				ENDIF
				NEW_LOAD_SCENE_START(g_SpawnData.vCandSpawnPoint, GET_HEADING_AS_VECTOR(g_SpawnData.fCandSpawnHeading), fDist)	
			ENDIF
			g_SpawnData.bDoLoadScene = TRUE
			g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()	
			#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] PLAYER_RESPAWN_DO_LOAD_SCENE_IF_NECESSARY - doing a load scene.") NET_NL()
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			NET_PRINT("[spawning] PLAYER_RESPAWN_DO_LOAD_SCENE_IF_NECESSARY - player is near death scene") NET_NL()
			#ENDIF
			g_SpawnData.iLoadSceneTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), (LOAD_SCENE_TIME_OUT+1)*-1)    
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		NET_PRINT("[spawning] PLAYER_RESPAWN_DO_LOAD_SCENE_IF_NECESSARY - IS_PLAYER_SWITCH_IN_PROGRESS=TRUE") NET_NL()
		#ENDIF
		g_SpawnData.iLoadSceneTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), (LOAD_SCENE_TIME_OUT+1)*-1)    	
	ENDIF
													
	SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_LOAD_SCENE)
	#IF IS_DEBUG_BUILD
	NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_JOINING - going to RESPAWN_STATE_LOAD_SCENE") NET_NL()
	#ENDIF
ENDPROC

PROC RESET_SPAWN_CAM()
	IF DOES_CAM_EXIST(g_SpawnData.SpawnCam)
		DESTROY_CAM(g_SpawnData.SpawnCam)
	ENDIF
	g_SpawnData.iSpawnCamState = 0
	g_SpawnData.bUpdateSpawnCam = FALSE
ENDPROC

PROC UPDATE_SPAWN_CAM()
	VECTOR vec
	FLOAT fDist

	IF (g_SpawnData.bUpdateSpawnCam)
	
		IF IS_TRANSITION_ACTIVE()
		OR IS_PLAYER_SWITCH_IN_PROGRESS()
		OR NETWORK_IS_IN_MP_CUTSCENE()
		OR IS_CUTSCENE_PLAYING()
		OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		OR IS_PLAYER_IN_CORONA()
		OR (g_SpawnData.MissionSpawnDetails.bSpawnInVehicle)
		OR (GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON) // 2169532
		OR (g_TransitionSessionNonResetVars.sGlobalCelebrationData.bBlockSpawnFadeInForMissionFail) // 2209433
		OR IS_PLAYER_SPECTATING(PLAYER_ID())
		OR GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iWarpState > 0
		OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty > 0		
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_TURF_WARZONE(g_FMMC_STRUCT.iAdversaryModeType) // Fix for B*3370787
		OR (IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID()) AND NOT NETWORK_IS_ACTIVITY_SESSION())
			RESET_SPAWN_CAM()
			NET_PRINT("UPDATE_SPAWN_CAM - cannot update spawn cam at this time.") NET_NL()	
		ELSE
	
			SWITCH g_SpawnData.iSpawnCamState
				CASE 0
					IF NOT IS_PLAYER_STANDING_WITH_BACK_TO_WALL()
					#IF IS_DEBUG_BUILD
					AND NOT (g_SpawnData.bSpawnCamIgnoreShapeTest)
					#ENDIF
						RESET_SPAWN_CAM()
						NET_PRINT("UPDATE_SPAWN_CAM - IS_PLAYER_STANDING_WITH_BACK_TO_WALL = FALSE.") NET_NL()
					ELSE
						g_SpawnData.SpawnCam =  CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")

						vec = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<SPAWN_CAM_OFFSET_X, SPAWN_CAM_OFFSET_Y, SPAWN_CAM_OFFSET_Z>>)
						SET_CAM_COORD(g_SpawnData.SpawnCam, vec)
						
						vec = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<SPAWN_CAM_POINT_OFFSET_X, SPAWN_CAM_POINT_OFFSET_Y, SPAWN_CAM_POINT_OFFSET_Z>>)
						POINT_CAM_AT_COORD(g_SpawnData.SpawnCam, vec)
					
						SET_CAM_FOV(g_SpawnData.SpawnCam, SPAWN_CAM_FOV)
					
						SET_CAM_ACTIVE(g_SpawnData.SpawnCam, TRUE) 
						
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						g_SpawnData.iSpawnCamState++
						NET_PRINT("UPDATE_SPAWN_CAM - completed stage 0") NET_NL()
					ENDIF
				BREAK
				CASE 1	
					IF DOES_CAM_EXIST(g_SpawnData.SpawnCam)
						vec = GET_CAM_COORD(g_SpawnData.SpawnCam)
						PRINTLN("UPDATE_SPAWN_CAM - spawn cam coords ", vec)
					ENDIF
					
					fDist = VDIST(GET_GAMEPLAY_CAM_COORD(), vec)
					PRINTLN("UPDATE_SPAWN_CAM - fDist ", fDist)
					
					IF (fDist < 20.0) 
					AND NOT IS_SCREEN_FADED_OUT()
					AND g_iFMMCCurrentStaticCam = -1 //Current scripted camera is going to be much further away if these are on
						STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(TRUE)
					ELSE
						PRINTLN("UPDATE_SPAWN_CAM - gameplay cam too far from spawn cam to do a catchup")
					ENDIF
					
					RESET_SPAWN_CAM()
					NET_PRINT("UPDATE_SPAWN_CAM - completed stage 1") NET_NL()
				BREAK
				DEFAULT
					NET_PRINT("UPDATE_SPAWN_CAM - unknown state") NET_NL()
					g_SpawnData.iSpawnCamState = 0	
				BREAK
			ENDSWITCH
		
		ENDIF
		
	ENDIF
ENDPROC



FUNC BOOL GET_FREE_RACE_TURRET_VEHICLE_SEAT_RESPAWN(VEHICLE_INDEX VehicleID, VEHICLE_SEAT &vs_ToAssign)
	INT i
	IF DOES_ENTITY_EXIST(VehicleID)
	AND NOT IS_ENTITY_DEAD(VehicleID)
		REPEAT GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(VehicleID) i
			IF IS_VEHICLE_SEAT_FREE(VehicleID, INT_TO_ENUM(VEHICLE_SEAT, i))
				IF IS_TURRET_SEAT(VehicleID, INT_TO_ENUM(VEHICLE_SEAT, i))
					vs_ToAssign = INT_TO_ENUM(VEHICLE_SEAT, i)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC




FUNC INT GET_LOCAL_PLAYER_RESPAWN_STATE()
	RETURN GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState
ENDFUNC


PROC SET_PERSONAL_VEHICLE_SPAWN_OVERRIDE_VECTOR(VECTOR vOverrideValue)
	
	#IF IS_DEBUG_BUILD
	IF (VDIST (g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPvSpawnPosOverride, vOverrideValue) > 0.01)
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[personal_vehicle] SET_PERSONAL_VEHICLE_SPAWN_OVERRIDE_VECTOR - setting vPvSpawnPosOverride to ", vOverrideValue)		
	ENDIF
	#ENDIF
	
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPvSpawnPosOverride = vOverrideValue
	
	IF IS_POINT_IN_DISPLACED_GLOBAL_EXCLUSION_ZONE(g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPvSpawnPosOverride, TRUE)
		PRINTLN("[personal_vehicle] SET_PERSONAL_VEHICLE_SPAWN_OVERRIDE_VECTOR - moved to ", g_TransitionSessionNonResetVars.sPostMissionCleanupData.vPvSpawnPosOverride)	
	ENDIF
	
ENDPROC

PROC FINISH_FORCED_RESPAWN()
	g_SpawnData.MissionSpawnDetails.bDoForcedRespawn 				= FALSE
	g_SpawnData.MissionSpawnDetails.bForcedRespawnTimerInitialised 	= TRUE
	g_SpawnData.MissionSpawnDetails.timerForcedRespawn 				= GET_NETWORK_TIME_ACCURATE()
	PRINTLN("FINISH_FORCED_RESPAWN - bDoForcedRespawn = FALSE")
ENDPROC

FUNC BOOL HAS_FORCED_RESPAWN_RECENTLY_FINISHED(INT iTimeInMS)
	IF g_SpawnData.MissionSpawnDetails.bDoForcedRespawn = FALSE
	AND g_SpawnData.MissionSpawnDetails.bForcedRespawnTimerInitialised = TRUE
		RETURN (ABSI(GET_TIME_DIFFERENCE(g_SpawnData.MissionSpawnDetails.timerForcedRespawn, GET_NETWORK_TIME_ACCURATE())) > iTimeInMS)
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL UPDATE_RESTORE_PLAYER_MODEL_FROM_CUSTOM()
	
	// this is for restoring the player model after using a custom player model.
	IF (g_TransitionSpawnData.iTriggerNetSpawnFrame != 0)
	AND (g_TransitionSpawnData.iTriggerNetSpawnFrame < GET_FRAME_COUNT())
		IF (g_TransitionSpawnData.StoredModelName != DUMMY_MODEL_FOR_SCRIPT)
		
			// load and change model
			IF NOT (GET_PLAYER_MODEL() = g_TransitionSpawnData.StoredModelName)
				REQUEST_MODEL(g_TransitionSpawnData.StoredModelName)
				IF HAS_MODEL_LOADED(g_TransitionSpawnData.StoredModelName)					
					PRINTLN("[spawning] UPDATE_RESTORE_PLAYER_MODEL_FROM_CUSTOM - setting model.")					
					RestorePlayerModelFromCustom()
					SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
				ELSE
					PRINTLN("[spawning] UPDATE_RESTORE_PLAYER_MODEL_FROM_CUSTOM - waiting on model to load")
				ENDIF
			ELSE
			
				// wait for head blend to finish
				IF NOT HAS_PED_HEAD_BLEND_FINISHED(PLAYER_PED_ID())
				OR NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
					PRINTLN("[spawning] UPDATE_RESTORE_PLAYER_MODEL_FROM_CUSTOM - waiting on HAS_PED_HEAD_BLEND_FINISHED ")
				ELSE				
					// trigger spawn
					PRINTLN("[spawning] UPDATE_RESTORE_PLAYER_MODEL_FROM_CUSTOM - calling NET_SPAWN_PLAYER")
					NET_SPAWN_PLAYER(GET_PLAYER_COORDS(PLAYER_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()), SPAWN_REASON_RESTORE_CHARACTER, SPAWN_LOCATION_AT_CURRENT_POSITION)
					APPLY_CHARACTER_CREATOR_FEATURES_FROM_STATS(PLAYER_PED_ID(), 0, FALSE) // NeilF. Added for url:bugstar:7434237 
					g_TransitionSpawnData.iTriggerNetSpawnFrame = 0
					g_TransitionSpawnData.StoredModelName = DUMMY_MODEL_FOR_SCRIPT
					RETURN TRUE
				ENDIF
			
			ENDIF
			
			// EXIT out of this function so we dont accidently call net_spawn_player further down below
			RETURN FALSE
			
		ELSE
			PRINTLN("[spawning] UPDATE_RESTORE_PLAYER_MODEL_FROM_CUSTOM - g_SpawnData.StoredModelName == DUMMY_MODEL_FOR_SCRIPTd")
			g_TransitionSpawnData.iTriggerNetSpawnFrame = 0
			g_TransitionSpawnData.StoredModelName = DUMMY_MODEL_FOR_SCRIPT
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

/// PURPOSE:
///    Spawning / Afterlife logic. 
PROC UPDATE_PLAYER_RESPAWN_STATE()

//	WEAPON_TYPE weaponUsed

	INT iAdditionalLoadsceneTime
	
	IF NOT UPDATE_RESTORE_PLAYER_MODEL_FROM_CUSTOM()
		PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - UPDATE_RESTORE_PLAYER_MODEL_FROM_CUSTOM = FALSE")
		EXIT
	ENDIF
	
	
	IF IS_TRANSITION_ACTIVE()
		PAUSE_DEATH_ARREST_RESTART(TRUE) 
		NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - IS_TRANSITION_ACTIVE = TRUE - calling PAUSE_DEATH_ARREST_RESTART") NET_NL()
		EXIT
	ENDIF
	
	// if the player is visiting the ps store then halt any respawning activity
	IF IS_COMMERCE_STORE_OPEN()
	AND NOT IS_PC_VERSION()
		NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - IS_COMMERCE_STORE_OPEN = TRUE - exiting.") NET_NL()
		EXIT
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	OPEN_SCRIPT_PROFILE_MARKER_GROUP("UPDATE_PLAYER_RESPAWN_STATE")
	#ENDIF	
	#ENDIF
	
	PLAYER_INDEX PartnerID
	PLAYER_INDEX PlayerID
	VEHICLE_INDEX CarID
	VEHICLE_INDEX PartnerVehicleID
	
	INT i, y
	BOOL bContinue
	VECTOR vec
	//BOOL bSetAsMissionVehicle
	BOOL MoveOnFromRejoin = TRUE
	INT iSeat
	PED_PARACHUTE_STATE eCurrentParachuteState = PPS_INVALID
	
	BOOL bChangedBackFromBigfoot = FALSE
	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, FALSE)
		eCurrentParachuteState = GET_PED_PARACHUTE_STATE(PLAYER_PED_ID())
	ENDIF	
	
	#IF IS_DEBUG_BUILD
		INT stored_iBS_InCarWith
	#ENDIF
	
	MaintainRespawnSearchCleanup() // this will cleanup any respawn searches that were left dangling
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MaintainRespawnSearchCleanup")
	#ENDIF
	#ENDIF			
	
//	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - player in a vehicle.") NET_NL()
//	ELSE
//		NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - player not in a vehicle.") NET_NL()	
//	ENDIF
	
	// If we are not in a playing state we can't bring up map etc.
	IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState != RESPAWN_STATE_PLAYING
		DISABLE_DPADDOWN_THIS_FRAME()
	
		// Maintain our death effects
		IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
		AND NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
			MAINTAIN_KILL_STRIP_DEATH_EFFECT()
		ENDIF
	
		IF GET_GAME_STATE_ON_DEATH() != AFTERLIFE_JUST_AUTO_REJOIN
		AND GET_GAME_STATE_ON_DEATH() != AFTERLIFE_SET_SPECTATORCAM
			IF MAINTAIN_KILL_STRIP()
			OR IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID()) // If we do not need killstrip (1918704)
			OR g_SpawnData.bForceRespawn = TRUE 			// Speirs added (557749)
				
				#IF IS_DEBUG_BUILD
				IF NOT g_SpawnData.bPlayerWantsToRespawn
					PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - Setting: g_SpawnData.bPlayerWantsToRespawn = TRUE")
				ENDIF
				#ENDIF
				
				g_SpawnData.bPlayerWantsToRespawn = TRUE
			ENDIF
		ENDIF
	
	ENDIF	
	
	// clear this flag
	IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bJustKilledSelf) 
		IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState != RESPAWN_STATE_PLAYING)	
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bJustKilledSelf	= FALSE
			PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - clearing GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bJustKilledSelf")
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_KILL_STRIP_DEATH_EFFECT")
	#ENDIF
	#ENDIF		
	
//	IF IS_TRANSITION_ACTIVE() 
//		NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - transition is active! ") NET_NL()
//		
//		// stop load scene if one is on-going
//		IF (g_SpawnData.bDoLoadScene)					
//			IF IS_NEW_LOAD_SCENE_ACTIVE()
//				NEW_LOAD_SCENE_STOP()
//			ENDIF
//			CLEAR_FOCUS()
//			g_SpawnData.bDoLoadScene = FALSE
//		ENDIF
//		
//		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_DEAD
//		EXIT
//	ENDIF
	
	// Delete respawn vehicle when abort respawn is called.
	IF g_SpawnData.MissionSpawnDetails.bAbortSpawnInVehicle = TRUE
		IF DOES_ENTITY_EXIST(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
			IF NETWORK_HAS_CONTROL_OF_ENTITY(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
			AND NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
					#IF IS_DEBUG_BUILD
					PRINTLN("[Spawning] UPDATE_PLAYER_RESPAWN_STATE - Player is in respawn vehicle, waiting to delete vehicle.")
					#ENDIF
				ELSE
					DELETE_VEHICLE(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
					SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE)
					PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - Delete respawn vehicle on abort.")
					GOTO_RESPAWN_STATE_PLAYING()
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	SWITCH GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState
	
		// *******************************************************************
		//  SPAWN STATE KILL STRIP
		// *******************************************************************		
		CASE RESPAWN_STATE_KILL_STRIP
			
			// Remain viewing game for 3s
			IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.killStripTimer)) >= 3000
				MakeScreenBlurredOutForSpawn()		// Begin blur out
			ENDIF
			
			// If killstrip returned TRUE then begin player respawn
			IF g_SpawnData.bPlayerWantsToRespawn									
				IF NOT g_bMissionInstantRespawn
					PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_KILL_STRIP. Killstrip finished - move on: RESPAWN_STATE_DEAD, clear ticker / feed")
					THEFEED_FLUSH_QUEUE()
				ELSE
					PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_KILL_STRIP. Killstrip finished - move on: RESPAWN_STATE_DEAD, DO NOT clear ticker / feed as ciBS2_ALLOW_INSTANT_RESPAWN_ON_DEATH is set.")
				ENDIF
				
				#IF FEATURE_FREEMODE_ARCADE
				IF IS_FREEMODE_ARCADE()
				#IF FEATURE_COPS_N_CROOKS
				AND GET_ARCADE_MODE() = ARC_COPS_CROOKS
				AND GET_PLAYER_TEAM(PLAYER_ID()) = 0 // Cops
				#ENDIF
					g_SpawnData.bForceFastTravel = TRUE
				ENDIF
				#ENDIF
				
				SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_DEAD)
			ENDIF
			
			ALLOW_PAUSE_WHEN_NOT_IN_STATE_OF_PLAY_THIS_FRAME()
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("RESPAWN_STATE_KILL_STRIP")
			#ENDIF
			#ENDIF	
			
		BREAK
			
	
		// *******************************************************************
		//  SPAWN STATE IN LIMBO
		// *******************************************************************
		CASE RESPAWN_STATE_DEAD
		
			// if player is told to consider interiors then store the current interior
			IF (g_SpawnData.MissionSpawnDetails.bConsiderInteriors)
			
				IF NOT IS_COLLISION_MARKED_OUTSIDE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
			
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - getting interior at ") NET_PRINT_VECTOR(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) NET_NL()
					NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - interior is ") NET_PRINT_INT(NATIVE_TO_INT(GET_INTERIOR_AT_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)))) NET_NL()
					NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - interior group is ") NET_PRINT_INT(GET_INTERIOR_GROUP_ID(GET_INTERIOR_AT_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)))) NET_NL()
					#ENDIF
					g_SpawnData.MissionSpawnDetails.DeathInterior = GET_INTERIOR_AT_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
					IF IS_VALID_INTERIOR(g_SpawnData.MissionSpawnDetails.DeathInterior)
						g_SpawnData.MissionSpawnDetails.iDeathInteriorGroup	= GET_INTERIOR_GROUP_ID(g_SpawnData.MissionSpawnDetails.DeathInterior)
					ENDIF
				
				ELSE
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - player is outside") NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		
		
			IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, FALSE)
			
				IF (GET_MANUAL_RESPAWN_STATE() = MRS_READY_FOR_PLACEMENT)
				AND IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
					SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_WAIT_FOR_MANUAL_RESPAWN_PLACEMENT)
				ELSE
				
					IF (IS_PLAYER_TELEPORT_ACTIVE()	OR IS_NEW_LOAD_SCENE_ACTIVE() OR (IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(PLAYER_ID()) AND (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iWarpToLocationTime) < 100)) OR ( IS_NET_WARP_ACTIVE() AND (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.g_NetWarpLastUpdateTime) < 100)))
					AND (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.RespawnStateTimer)) < 10000)
					
						#IF IS_DEBUG_BUILD
						IF IS_PLAYER_TELEPORT_ACTIVE()	
							NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_DEAD - player not actually dead, but waiting on IS_PLAYER_TELEPORT_ACTIVE") NET_NL()
						ENDIF
						IF IS_NEW_LOAD_SCENE_ACTIVE()	
							NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_DEAD - player not actually dead, but waiting on IS_NEW_LOAD_SCENE_ACTIVE") NET_NL()
						ENDIF
						IF IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION(PLAYER_ID())
							NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_DEAD - player not actually dead, but waiting on IS_PLAYER_DOING_WARP_TO_SPAWN_LOCATION") NET_NL()						
						ENDIF
						IF IS_NET_WARP_ACTIVE()
							NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_DEAD - player not actually dead, but waiting on IS_NET_WARP_ACTIVE") NET_NL()						
						ENDIF
						#ENDIF
				
					ELSE
				
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_DEAD - player not actually dead, going to RESPAWN_STATE_PLAYING") NET_NL()
						#ENDIF
						
				
						CLEANUP_ALL_KILL_STRIP() // make sure we're not stuck in a kill strip. 2647318
						
						RestoreScreenFromSpawnBlur()
						
						GOTO_RESPAWN_STATE_PLAYING() // if we're not actually dead then skip straight to playing.	
					ENDIF
					
				ENDIF
			ELSE
				
				SET_STORE_ENABLED(FALSE)
							
				IF (g_SpawnData.bForceRespawn)
					GOTO_JOINING_RESPAWN_STATE()	
					g_SpawnData.bForceRespawn = FALSE
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - g_SpawnData.bForceRespawn = TRUE - going to RESPAWN_STATE_JOINING") NET_NL()
					#ENDIF
				ELSE
			
					CLEAR_FACIAL_IDLE_ANIM_OVERRIDE(PLAYER_PED_ID()) // to stop dead ped from blinking
					
					NET_PRINT("[spawning] RESPAWN_STATE_DEAD- GET_CURRENT_GAMEMODE() = ") NET_PRINT_INT(ENUM_TO_INT(GET_CURRENT_GAMEMODE())) NET_NL()
			
			
					SWITCH GET_CURRENT_GAMEMODE()
			
						CASE GAMEMODE_FM
						CASE GAMEMODE_MPTESTBED
						#IF IS_DEBUG_BUILD
						CASE GAMEMODE_CREATOR
						#ENDIF
						
							IF GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_HasHelmet)
							AND NOT GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet)
								REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
							ENDIF
	
							IF NOT (g_SpawnData.bSpawnTimerInitialised)
								
								//NET_PRINT("[spawning] iSpawnTimer = ")NET_PRINT_INT(iSpawnTimer) NET_NL()
								
								#IF IS_DEBUG_BUILD
//								IF NETWORK_IS_IN_CODE_DEVELOPMENT_MODE()
								IF GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_NET_TEST
									g_SpawnData.SpawnTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), -1)      
									g_SpawnData.bSpawnTimerInitialised = TRUE
								ENDIF
								#ENDIF
								
								
								SWITCH GET_CURRENT_GAMEMODE()
									CASE GAMEMODE_FM
									#IF IS_DEBUG_BUILD
									CASE GAMEMODE_CREATOR
									#ENDIF
										IF GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_RACE
										OR GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()) = FMMC_TYPE_RACE_TO_POINT
											g_SpawnData.SpawnTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), RACE_SPAWN_DELAY)
										ELSE
											g_SpawnData.SpawnTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), 0)      
										ENDIF
										g_SpawnData.bSpawnTimerInitialised = TRUE
									BREAK

									CASE GAMEMODE_MPTESTBED									
										g_SpawnData.SpawnTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), -1)      
										g_SpawnData.bSpawnTimerInitialised = TRUE
									BREAK
								
								ENDSWITCH
									
								
								

							ELIF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), g_SpawnData.SpawnTimer)     
								// At this point we are within the standard 5 second death cam
							ELIF IS_TIME_LESS_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(g_SpawnData.SpawnTimer, GET_RESPAWN_DELAY()) )
								// At this point we are within the extended death counter (for increased spawn times during heists
							ELSE
//								IF NOT (MPGlobalsHud.MissionOver.bDisplaying) // don't display the mp hud until any 'mission over' text has been displayed
									
									g_SpawnData.vMyDeadCoords = GET_PLAYER_COORDS(PLAYER_ID())
									NET_PRINT("[spawning] RESPAWN_STATE_DEAD - Setting g_SpawnData.vMyDeadCoords at ") NET_PRINT_VECTOR(g_SpawnData.vMyDeadCoords) NET_NL()
									
									IF IsPlayerOnAMissionOrEventForSpawningPurposes(PLAYER_ID()) 
									AND IS_POINT_IN_MISSION_SPAWN_AREA(g_SpawnData.vMyDeadCoords)
										g_SpawnData.MissionSpawnDetails.iDiedInMissionSpawnAreaCount++	
										NET_PRINT("[spawning] RESPAWN_STATE_DEAD - dead coords inside mission spawn area, increasing iDiedInMissionSpawnAreaCount to ") NET_PRINT_INT(g_SpawnData.MissionSpawnDetails.iDiedInMissionSpawnAreaCount) NET_NL()									
									ENDIF
								
									#IF IS_DEBUG_BUILD
										IF  (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_NET_TEST)
//										OR NETWORK_IS_IN_CODE_DEVELOPMENT_MODE()
											#IF IS_DEBUG_BUILD
											NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_DEAD - (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_NET_TEST)") NET_NL()
											#ENDIF
											SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
										ENDIF
									#ENDIF
									
//									//COMMENT OUT ONCE PEOPLE HAVE CHANGED OVER TO SET_GAME_STATE_ON_DEATH
//									IF MPGlobalsCNC.RaceRespawn.bInRace
//										#IF IS_DEBUG_BUILD
//										NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_DEAD - MPGlobalsCNC.RaceRespawn.bInRace")
//										#ENDIF
//										SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
//									ENDIF
//									/////////
								
									SWITCH GET_GAME_STATE_ON_DEATH()
						
										CASE AFTERLIFE_BRINGUPHUD
											#IF IS_DEBUG_BUILD
											NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_DEAD - AFTERLIFE_BRINGUPHUD") NET_NL()
											#ENDIF
											TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
										BREAK
										
										CASE AFTERLIFE_BRINGUPHUD_AFTER_TIME
											#IF IS_DEBUG_BUILD
											NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_DEAD - AFTERLIFE_BRINGUPHUD_AFTER_TIME") NET_NL()
											#ENDIF
											g_SpawnData.GameStateTimer = GET_NETWORK_TIME()
										BREAK
									
										CASE AFTERLIFE_SET_SPECTATORCAM
										CASE AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP
											#IF IS_DEBUG_BUILD
											NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_DEAD - AFTERLIFE_SET_SPECTATORCAM/_AFTER_KILLSTRIP - g_SpawnData.bSafeForSpectatorCam = FALSE") NET_NL()
											#ENDIF
											
											g_SpawnData.bSafeForSpectatorCam = FALSE
										BREAK
										
										CASE AFTERLIFE_SET_SPECTATORCAM_AFTER_TIME
											// Commented out (725058)
//											#IF IS_DEBUG_BUILD
//											NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_DEAD - AFTERLIFE_SET_SPECTATORCAM_AFTER_TIME") NET_NL()
//											#ENDIF
											g_SpawnData.GameStateTimer = GET_NETWORK_TIME()
										BREAK
										
										CASE AFTERLIFE_JUST_REJOIN
										CASE AFTERLIFE_JUST_AUTO_REJOIN
											#IF IS_DEBUG_BUILD
											NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_DEAD - AFTERLIFE_JUST_REJOIN") NET_NL()
											#ENDIF
											
											IF GET_GAME_STATE_ON_DEATH() = AFTERLIFE_JUST_REJOIN
												NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_DEAD - AFTERLIFE_JUST_REJOIN") NET_NL()
											ELSE
												NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_DEAD - AFTERLIFE_JUST_AUTO_REJOIN") NET_NL()
											ENDIF
										BREAK
									
										CASE AFTERLIFE_JUST_REJOIN_AFTER_TIME
											#IF IS_DEBUG_BUILD
											NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_DEAD - AFTERLIFE_JUST_REJOIN_AFTER_TIME") NET_NL()
											#ENDIF
											g_SpawnData.GameStateTimer = GET_NETWORK_TIME()
										BREAK
										CASE AFTERLIFE_JUST_REJOIN_INTO_LAST_VEHICLE
											#IF IS_DEBUG_BUILD
											NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_DEAD - AFTERLIFE_JUST_REJOIN_INTO_LAST_VEHICLE") NET_NL()
											#ENDIF
											IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_USED_VEHICLE())											
												SET_PLAYER_RESPAWN_IN_VEHICLE(TRUE, GET_ENTITY_MODEL(GET_PLAYERS_LAST_USED_VEHICLE()), TRUE)
											ENDIF
										
										BREAK
										CASE AFTERLIFE_SPAWN_IN_AS_BIGFOOT
											#IF IS_DEBUG_BUILD
											NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_DEAD - AFTERLIFE_SPAWN_IN_AS_BIGFOOT") NET_NL()
											#ENDIF
					
										BREAK
									
									ENDSWITCH
									
									g_SpawnData.AfterlifeValueOnDeath = GET_GAME_STATE_ON_DEATH()
									
									//COMMENT OUT ONCE PEOPLE HAVE CHANGED OVER TO SET_GAME_STATE_ON_DEATH
									IF GET_GAME_STATE_ON_DEATH() = AFTERLIFE_BRINGUPHUD
										// reasons not to bring up the hud
										IF IS_A_SPECTATOR_CAM_ACTIVE()
//										OR (MPGlobalsCNC.RaceRespawn.bInRace)
										#IF IS_DEBUG_BUILD
											OR (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_NET_TEST)
										#ENDIF
			
											g_SpawnData.bDontBringUpHudWhenDead = TRUE
											
										ELSE
											g_SpawnData.bDontBringUpHudWhenDead = FALSE
											TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
										ENDIF
									ENDIF
									////////////
									
									#IF IS_DEBUG_BUILD
										g_SpawnData.DecisionPrint_Done = FALSE
									#ENDIF
														
									SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_WAIT_FOR_DECISION)
//								ENDIF
							ENDIF

							
						BREAK
					
						DEFAULT
							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] RESPAWN_STATE_DEAD - GameMode not recognised, check logs for last <<<<<<< SET_CURRENT_GAMEMODE to") NET_NL()
							SCRIPT_ASSERT("UPDATE_PLAYER_RESPAWN_STATE - GameMode not recognised, check logs for last <<<<<<< SET_CURRENT_GAMEMODE to") 
							#ENDIF
							SET_CURRENT_GAMEMODE(GAMEMODE_FM)
						BREAK
						
					ENDSWITCH
					
				ENDIF

			ENDIF
		
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("RESPAWN_STATE_DEAD")
			#ENDIF
			#ENDIF			
		
		BREAK
		
		
		// *******************************************************************
		//  RESPAWN_STATE_WAIT_FOR_DECISION
		// *******************************************************************
		CASE RESPAWN_STATE_WAIT_FOR_DECISION
			// wait for player to make choices in MP hud or the system chooses where / when to respawn	
						
	
			SWITCH GET_CURRENT_GAMEMODE()
				
				CASE GAMEMODE_FM
				CASE GAMEMODE_MPTESTBED
				#IF IS_DEBUG_BUILD
				CASE GAMEMODE_CREATOR
				#ENDIF	
				
					// Failsafe timer for bug 1717705. Temp fix until Neil is back and can look into a proper fix. #LimboBug
					IF (g_SpawnData.AfterlifeValueOnDeath <> GET_GAME_STATE_ON_DEATH())
                    AND (GET_GAME_STATE_ON_DEATH() <> AFTERLIFE_MANUAL_RESPAWN)
                        IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, FALSE)
                            #IF IS_DEBUG_BUILD
                                NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - g_SpawnData.AfterlifeValueOnDeath <> GET_GAME_STATE_ON_DEATH() send to state RESPAWN_STATE_DEAD") NET_NL()
                                NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - g_SpawnData.AfterlifeValueOnDeath = ") NET_PRINT_INT(ENUM_TO_INT(g_SpawnData.AfterlifeValueOnDeath)) NET_PRINT(" and GET_GAME_STATE_ON_DEATH() = ") NET_PRINT_INT(ENUM_TO_INT(GET_GAME_STATE_ON_DEATH())) NET_NL()
                            #ENDIF          
							
							// if the previous afterlife was for specatator cam and the spectator cam is active then set back to this. 2699674
							IF ((g_SpawnData.AfterlifeValueOnDeath = AFTERLIFE_SET_SPECTATORCAM) 
								OR (g_SpawnData.AfterlifeValueOnDeath = AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP)
								OR (g_SpawnData.AfterlifeValueOnDeath = AFTERLIFE_SET_SPECTATORCAM_AFTER_TIME))
							AND IS_A_SPECTATOR_CAM_ACTIVE()
								PRINTLN("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - state on death mismatch, but specatator cam active, setting back to AFTERLIFE_SET_SPECTATORCAM")
								SET_GAME_STATE_ON_DEATH(g_SpawnData.AfterlifeValueOnDeath)
							ENDIF
							
							SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_DEAD)
                        ELSE
                            #IF IS_DEBUG_BUILD
                                NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - PLAYER IS OK. g_SpawnData.AfterlifeValueOnDeath = ") NET_PRINT_INT(ENUM_TO_INT(g_SpawnData.AfterlifeValueOnDeath)) NET_PRINT(" and GET_GAME_STATE_ON_DEATH() = ") NET_PRINT_INT(ENUM_TO_INT(GET_GAME_STATE_ON_DEATH())) NET_NL()
                            #ENDIF
                        ENDIF
                    ENDIF
					
					// if the manual respawn hasn't been cleaned up					
					IF (GET_MANUAL_RESPAWN_STATE() != MRS_NULL)
						IF NOT (GET_GAME_STATE_ON_DEATH() = AFTERLIFE_SET_SPECTATORCAM)
						AND NOT (GET_GAME_STATE_ON_DEATH() = AFTERLIFE_MANUAL_RESPAWN)
						AND NOT (GET_GAME_STATE_ON_DEATH() = AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP)
							#IF IS_DEBUG_BUILD
								NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - manual respawn state not valid for this afterlife. Afterlife ") NET_PRINT_INT(ENUM_TO_INT(GET_GAME_STATE_ON_DEATH())) NET_NL()
							#ENDIF
							SET_MANUAL_RESPAWN_STATE(MRS_NULL)
						ENDIF
					ENDIF
				
					SWITCH GET_GAME_STATE_ON_DEATH()
						
						CASE AFTERLIFE_BRINGUPHUD
							#IF IS_DEBUG_BUILD
							IF g_SpawnData.DecisionPrint_Done = FALSE
								NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - AFTERLIFE_BRINGUPHUD") NET_NL()
								g_SpawnData.DecisionPrint_Done = TRUE
							ENDIF
							#ENDIF
							
						BREAK
						
						CASE AFTERLIFE_BRINGUPHUD_AFTER_TIME
							#IF IS_DEBUG_BUILD
							IF g_SpawnData.DecisionPrint_Done = FALSE
								NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - AFTERLIFE_BRINGUPHUD_AFTER_TIME") NET_NL()
								g_SpawnData.DecisionPrint_Done = TRUE
							ENDIF
							#ENDIF
							IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.GameStateTimer) > GET_GAME_STATE_ON_DEATH_TIME()
								TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
							ENDIF
							
						BREAK
				
						CASE AFTERLIFE_SET_SPECTATORCAM
						CASE AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP
						
							#IF IS_DEBUG_BUILD
							IF g_SpawnData.DecisionPrint_Done = FALSE
								NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - AFTERLIFE_SET_SPECTATORCAM ") NET_NL()
								NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - g_SpawnData.bHasSpectatorCamBeenActive: ") NET_PRINT_BOOL(g_SpawnData.bHasSpectatorCamBeenActive) NET_NL()
								g_SpawnData.DecisionPrint_Done = TRUE
							ENDIF
							#ENDIF
							
							IF g_SpawnData.bSafeForSpectatorCam = FALSE
								NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - setting g_SpawnData.bSafeForSpectatorCam = TRUE") NET_NL()
								g_SpawnData.bSafeForSpectatorCam = TRUE
								//RESET_NET_TIMER(g_SpawnData.SpectatorTimeout)
								//NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - g_SpawnData.SpectatorTimeout RESET") NET_NL()
							ENDIF
							
							// IF NOT IS_A_SPECTATOR_CAM_RUNNING() - JA: switched back for 997028 (issue was g_SpawnData.bHasSpectatorCamBeenActive not being reset)
							IF NOT IS_A_SPECTATOR_CAM_ACTIVE()
							AND NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()		
							AND NOT IS_BIT_SET(g_iBS1_Mission, ciBS1_Mission_HoldUpRespawn)
								// if the spectator cam never actually triggers but we go into a skyswoop then consider it having been active. 2066799
								IF IS_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP()
									g_SpawnData.bHasSpectatorCamBeenActive = TRUE
									NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - IS_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP - setting g_SpawnData.bHasSpectatorCamBeenActive .")NET_NL()
								ENDIF	
								
								IF g_SpawnData.bHasSpectatorCamBeenActive							
								
									NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - spectator cam not active but has been -> RESPAWN_STATE_JOINING") NET_NL()
								
									g_SpawnData.bHasSpectatorCamBeenActive = FALSE
									g_SpawnData.bSafeForSpectatorCam = FALSE
									NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - setting g_SpawnData.bSafeForSpectatorCam = FALSE") NET_NL()
									
									// should we go from the spectator cam to the leaderboard?
									IF (GET_MANUAL_RESPAWN_STATE() = MRS_EXIT_SPECTATOR_AFTERLIFE)
									OR (GET_MANUAL_RESPAWN_STATE() = MRS_RESPAWN_PLAYER_HIDDEN)
										SET_GAME_STATE_ON_DEATH(AFTERLIFE_MANUAL_RESPAWN)
									ELSE
										GOTO_JOINING_RESPAWN_STATE()
									ENDIF
								ELSE
									NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - g_SpawnData.bHasSpectatorCamBeenActive = FALSE.")NET_NL()
									
									//Mission Ended
									IF g_bMissionOver
										IF NOT IS_BIG_MESSAGE_BEING_DISPLAYED(BIG_MESSAGE_WASTED)
											NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - MISSION HAS ENDED FOR EVERYONE")NET_NL()
											// should we go from the spectator cam to the leaderboard?
											IF (GET_MANUAL_RESPAWN_STATE() = MRS_EXIT_SPECTATOR_AFTERLIFE)
											OR (GET_MANUAL_RESPAWN_STATE() = MRS_RESPAWN_PLAYER_HIDDEN)
												SET_GAME_STATE_ON_DEATH(AFTERLIFE_MANUAL_RESPAWN)
											ELSE
												GOTO_JOINING_RESPAWN_STATE()
											ENDIF
										ELSE
											NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - waiting for wasted message to finish")NET_NL()
										ENDIF
										
									//Timeout	//AFTER SPEAKING TO NEIL WE ARE COMMENTING OUT FOR NOW AS WE MAY HAVE SOME SITUATIONS WHERE WE WANT TO STAY IN THIS STATE
									/*ELIF HAS_NET_TIMER_EXPIRED(g_SpawnData.SpectatorTimeout, 30000)
										NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - TIMEOUT - g_SpawnData.SpectatorTimeout")NET_NL()
										// should we go from the spectator cam to the leaderboard?
										IF (GET_MANUAL_RESPAWN_STATE() = MRS_EXIT_SPECTATOR_AFTERLIFE)
										OR (GET_MANUAL_RESPAWN_STATE() = MRS_RESPAWN_PLAYER_HIDDEN)
											SET_GAME_STATE_ON_DEATH(AFTERLIFE_MANUAL_RESPAWN)
										ELSE
											GOTO_JOINING_RESPAWN_STATE()
										ENDIF*/
									ENDIF
								ENDIF
								
							ELSE
								/*IF g_SpawnData.bHasSpectatorCamBeenActive = FALSE
									NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - spectator cam active for first time") NET_NL()
									
									CLEANUP_BIG_MESSAGE()
									
									CLEANUP_ALL_KILL_STRIP()
								ENDIF*/
								
								IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
									NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE - Special Spectator Handling Respawn.")NET_NL()
								ENDIf
								
								NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - IS_A_SPECTATOR_CAM_ACTIVE - setting g_SpawnData.bHasSpectatorCamBeenActive .")NET_NL()

								
								g_SpawnData.bHasSpectatorCamBeenActive = TRUE
							ENDIF
						BREAK
						
						CASE AFTERLIFE_SET_SPECTATORCAM_AFTER_TIME
							#IF IS_DEBUG_BUILD
							IF g_SpawnData.DecisionPrint_Done = FALSE
								NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - AFTERLIFE_SET_SPECTATORCAM_AFTER_TIME") NET_NL()
								
								g_SpawnData.DecisionPrint_Done = TRUE
							ENDIF
							#ENDIF

							IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.GameStateTimer) > GET_GAME_STATE_ON_DEATH_TIME()
							OR HAS_KILL_STRIP_BEEN_SHOWN() // Speirs added 29/08/12, players were being given double respawn times
										
								IF NOT IS_A_SPECTATOR_CAM_ACTIVE()
									IF g_SpawnData.bHasSpectatorCamBeenActive
										g_SpawnData.bHasSpectatorCamBeenActive = FALSE
									ENDIF
								ELSE
									IF g_SpawnData.bHasSpectatorCamBeenActive = FALSE

									ENDIF
									g_SpawnData.bHasSpectatorCamBeenActive = TRUE
								ENDIF

								GOTO_JOINING_RESPAWN_STATE()

							ENDIF
						BREAK
						
						CASE AFTERLIFE_JUST_REJOIN
						CASE AFTERLIFE_JUST_AUTO_REJOIN
							#IF IS_DEBUG_BUILD
							IF g_SpawnData.DecisionPrint_Done = FALSE
								IF GET_GAME_STATE_ON_DEATH() = AFTERLIFE_JUST_REJOIN
									NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - AFTERLIFE_JUST_REJOIN / ") NET_NL()
								ELSE
									NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - AFTERLIFE_JUST_AUTO_REJOIN / ") NET_NL()
								ENDIF
								g_SpawnData.DecisionPrint_Done = TRUE
							ENDIF
							#ENDIF
							
							
							
							IF IS_PLAYER_PED_BIGFOOT() 
							OR g_i_BigfootTransformStages > 0
								bChangedBackFromBigfoot = TRUE 
							ENDIF
							
							IF CHANGE_PLAYER_PED_TO_HUMAN(FALSE)
								MoveOnFromRejoin = TRUE
								NET_NL()NET_PRINT("[MORPHMODEL] RESPAWN_STATE_WAIT_FOR_DECISION - CHANGE_PLAYER_PED_TO_HUMAN = TRUE ")
								IF bChangedBackFromBigfoot
									SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
								ENDIF
							ELSE
								NET_NL()NET_PRINT("[MORPHMODEL] RESPAWN_STATE_WAIT_FOR_DECISION - CHANGE_PLAYER_PED_TO_HUMAN = FALSE ")
								MoveOnFromRejoin = FALSE
							ENDIF
							
							IF MoveOnFromRejoin 
								GOTO_JOINING_RESPAWN_STATE()
							ENDIF
						BREAK
					
						CASE AFTERLIFE_JUST_REJOIN_AFTER_TIME
							#IF IS_DEBUG_BUILD
							IF g_SpawnData.DecisionPrint_Done = FALSE
								NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - AFTERLIFE_JUST_REJOIN_AFTER_TIME") NET_NL()
								g_SpawnData.DecisionPrint_Done = TRUE
							ENDIF
							#ENDIF
							IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.GameStateTimer) > GET_GAME_STATE_ON_DEATH_TIME()
								GOTO_JOINING_RESPAWN_STATE()
							ENDIF
						BREAK
						
						CASE AFTERLIFE_JUST_REJOIN_INTO_LAST_VEHICLE
							#IF IS_DEBUG_BUILD
							IF g_SpawnData.DecisionPrint_Done = FALSE
								NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - AFTERLIFE_JUST_REJOIN_INTO_LAST_VEHICLE") NET_NL()
								g_SpawnData.DecisionPrint_Done = TRUE
							ENDIF
							#ENDIF
							GOTO_JOINING_RESPAWN_STATE()							
						BREAK
						
						CASE AFTERLIFE_MANUAL_RESPAWN
							#IF IS_DEBUG_BUILD
							IF g_SpawnData.DecisionPrint_Done = FALSE
								NET_NL() NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - AFTERLIFE_MANUAL_RESPAWN") NET_NL()
								g_SpawnData.DecisionPrint_Done = TRUE
							ENDIF
							#ENDIF
							IF (GET_MANUAL_RESPAWN_STATE() = MRS_RESPAWN_PLAYER_HIDDEN)
								GOTO_JOINING_RESPAWN_STATE()
							ELSE
//								#IF IS_DEBUG_BUILD
//									NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - AFTERLIFE_MANUAL_RESPAWN - waiting on g_SpawnData.eManualRespawnState, current value is") NET_PRINT(GET_MANUAL_RESPAWN_STATE_STRING(GET_MANUAL_RESPAWN_STATE())) NET_NL()	
//								#ENDIF
							ENDIF
						BREAK
						
						CASE AFTERLIFE_SPAWN_IN_AS_BIGFOOT
							#IF IS_DEBUG_BUILD
							NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - AFTERLIFE_SPAWN_IN_AS_BIGFOOT") NET_NL()
							#ENDIF
							
							
							
							IF FadeOutForRespawn(500, FALSE)	
								IF CHANGE_PLAYER_PED_TO_BIGFOOT()
									SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
									SET_GAME_STATE_ON_DEATH(AFTERLIFE_JUST_REJOIN)
									GOTO_JOINING_RESPAWN_STATE()
									
								ENDIF
								
								IF IS_PLAYER_PED_BIGFOOT() 
								OR g_i_BigfootTransformStages > 0
									bChangedBackFromBigfoot = TRUE 
								ENDIF
								
							ENDIF
							
						BREAK
						
						#IF FEATURE_FREEMODE_ARCADE
						CASE AFTERLIFE_FAST_TRAVEL_RESPAWN
							IF NOT g_SpawnData.bForceFastTravel
								#IF IS_DEBUG_BUILD
								IF g_SpawnData.DecisionPrint_Done = FALSE
									NET_NL()NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_DECISION - AFTERLIFE_FAST_TRAVEL_RESPAWN - Spawn point selected") NET_NL()
									
									g_SpawnData.DecisionPrint_Done = TRUE
								ENDIF
								#ENDIF
								
								GOTO_JOINING_RESPAWN_STATE()
							ENDIF
						BREAK
						#ENDIF
					ENDSWITCH
				
				
					// if not in a team then do visibility checks on team mates
					IF (GET_PLAYER_TEAM(PLAYER_ID()) = -1)
						SET_PLAYER_CAN_SPAWN_IN_VIEW_OF_TEAM_MATES(FALSE)
					ENDIF

					
					// if player has chosen to swap team then progress
					IF (g_MPCharData.bPerformingTeamSwap = TRUE)			
						// only progress if player has left mission
						IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_FOR_DECISION - going to RESPAWN_STATE_JOINING 2") NET_NL()
							#ENDIF
							GOTO_JOINING_RESPAWN_STATE()	
						ELSE
							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_FOR_DECISION - bPerformingTeamSwap = TRUE, waiting for player to be off mission. ") NET_NL()
							#ENDIF
						ENDIF	
					ENDIF
					
				BREAK
				
				
			ENDSWITCH

	

			
			// if player is alive, they have been resurrected elsewhere then go to playing state
			IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, FALSE)
			AND bChangedBackFromBigfoot = FALSE
			AND IS_PLAYER_PED_BIGFOOT() = FALSE
			
				// if something previous in this frame has changed this state from waiting but the player is alive then set it back to waiting here to deal with it correctly.
				IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState != RESPAWN_STATE_WAIT_FOR_DECISION)
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_FOR_DECISION - player not actually dead and iRespawnState has changed, resetting state back to RESPAWN_STATE_WAIT_FOR_DECISION") NET_NL()
					#ENDIF
					SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_WAIT_FOR_DECISION)
				ENDIF
				
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bRezzedElsewhere_Bug1717705 = TRUE // Failsafe timer for bug 1717705. Temp fix until Neil is back and can look into a proper fix. #LimboBug
				
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					IF NOT g_bCelebrationScreenIsActive
						IF NOT g_bInMissionControllerCutscene
							IF HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID())
								#IF IS_DEBUG_BUILD
								NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_FOR_DECISION - player not actually dead, going to RESPAWN_STATE_PLAYING") NET_NL()
								#ENDIF
								GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bRezzedElsewhere_Bug1717705 = FALSE	// Failsafe timer for bug 1717705. Temp fix until Neil is back and can look into a proper fix. #LimboBug
								RestoreScreenFromSpawnBlur()				
								GOTO_RESPAWN_STATE_PLAYING() // if we're not actually dead then skip straight to playing.		
							ELSE
								#IF IS_DEBUG_BUILD
								NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_FOR_DECISION - player not actually dead, going to RESPAWN_STATE_PLAYING - but waiting for collision") NET_NL()
								#ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_FOR_DECISION - g_bInMissionControllerCutscene, waiting...") NET_NL()
							#ENDIF						
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_FOR_DECISION - g_bCelebrationScreenIsActive, waiting...") NET_NL()
						#ENDIF
					ENDIF
				ELSE
					// player switch has started so just set the player to playing.
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_FOR_DECISION - player not actually dead but player switch in progress, waiting....") NET_NL()
					#ENDIF
					
					IF IS_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP()
						PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_FOR_DECISION - player not actually dead but IS_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP, going straight to state_playing") NET_NL()				
						GOTO_RESPAWN_STATE_PLAYING() // if we're not actually dead then skip straight to playing.						
					ENDIF
				ENDIF
										
				
			ENDIF	
			
			// decision has been made elsewhere to start the respawn
			IF (g_SpawnData.bForceRespawn)
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_FOR_DECISION - going to RESPAWN_STATE_JOINING - 3") NET_NL()
				#ENDIF
				
				GOTO_JOINING_RESPAWN_STATE()	
				g_SpawnData.bForceRespawn = FALSE
			ENDIF					
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("RESPAWN_STATE_WAIT_FOR_DECISION")
			#ENDIF
			#ENDIF				
			
		BREAK
		
		// *******************************************************************
		//  SPAWN STATE JOINING
		// *******************************************************************
		CASE RESPAWN_STATE_JOINING			
				
			#IF IS_DEBUG_BUILD
				g_SpawnData.DecisionPrint_Done = FALSE
			#ENDIF

			g_SpawnData.bDoLoadScene = FALSE
			
			IF (g_MPCharData.bPerformingTeamSwap = TRUE)	
				// if player has chosen to swap team then progress
				GOTO_RESPAWN_STATE_PLAYING()
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_JOINING - g_MPCharData.bPerformingTeamSwap = TRUE") NET_NL()
				#ENDIF
			ELSE	

				IF (GET_MANUAL_RESPAWN_STATE() = MRS_RESPAWN_PLAYER_HIDDEN) 
				OR (g_SpawnData.bForceThroughJoining)
					
					IF IsPlayersModelLoaded()
					
						vec = GET_PLAYER_COORDS(PLAYER_ID())
						
						IF NOT (g_SpawnData.bForceThroughJoining)
							vec.z = -90.0
						ENDIF
						
						NET_SPAWN_PLAYER(vec, 0.0, SPAWN_REASON_MANUAL, SPAWN_LOCATION_AT_CURRENT_POSITION)						
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
						
						IF NOT (g_SpawnData.bForceThroughJoining)
							SET_MANUAL_RESPAWN_STATE(MRS_READY_FOR_PLACEMENT)
							SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_WAIT_FOR_MANUAL_RESPAWN_PLACEMENT)
						ELSE
							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_JOINING - g_SpawnData.bForceThroughJoining=TRUE calling GOTO_RESPAWN_STATE_PLAYING") NET_NL()
							#ENDIF
							GOTO_RESPAWN_STATE_PLAYING()
						ENDIF
					ENDIF
				ELSE
					IF FadeOutForRespawn(500, FALSE)				
						
						IF IsPlayersModelLoaded()
						AND HasLoadedVehicleSpawnModelIfNecessary()
						AND NOT IS_PED_BEING_JACKED(PLAYER_PED_ID())							
							
							IF NOT NETWORK_IS_IN_SPECTATOR_MODE()								
								
								IF DoesSpecificRespawnVehicleExist()
									
									// player is a rally passenger, wait for other player to have respawned, respawn next to him then warp into car.
									vec = GET_ENTITY_COORDS(g_SpawnData.SpecificVehicleID)
									vec.z = -90.0
									NET_SPAWN_PLAYER(vec, 0.0, SPAWN_REASON_IN_VEHICLE, SPAWN_LOCATION_AT_SPECIFIC_COORDS)									
									
									NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
									g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()	
									
									IF GET_ENTITY_MODEL(g_SpawnData.SpecificVehicleID) = THRUSTER
									AND (GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE
									OR GANGOPS_FLOW_GET_CURRENT_GANGOPS_MISSION() = ciGANGOPS_FLOW_MISSION_MISSILE_SILO_FINALE_P2)
										IF NETWORK_HAS_CONTROL_OF_ENTITY(g_SpawnData.SpecificVehicleID)
											NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_JOINING - Clearing crash task for Thruster.")
											CLEAR_VEHICLE_CRASH_TASK(g_SpawnData.SpecificVehicleID)
										ELSE
											NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_JOINING - Waiting for control of Thruster.")
											NETWORK_REQUEST_CONTROL_OF_ENTITY(g_SpawnData.SpecificVehicleID)
											EXIT
										ENDIF
									ENDIF

									SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_WARP_INTO_SPECIFIC_CAR)
									
									IF IsSpecificRespawnVehicleVisible()
										GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bPlayerIsHiddenForWarp = FALSE
									ELSE
										GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bPlayerIsHiddenForWarp = TRUE	
									ENDIF
								ELSE
								
									IF IsTeamModeSetForRespawning()						
									AND DoIHaveAVehiclePartner(PartnerID)	
									AND IsRespawnPassenger()
										
										IF GlobalplayerBD[NATIVE_TO_INT(PartnerID)].iRespawnState > RESPAWN_STATE_JOINING
											// player is a rally passenger, wait for other player to have respawned, respawn next to him then warp into car.
											vec = GET_PLAYER_COORDS(PartnerID)
											vec.z = -90.0
											NET_SPAWN_PLAYER(vec, 0.0, SPAWN_REASON_IN_VEHICLE, SPAWN_LOCATION_AT_SPECIFIC_COORDS)
											NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)																		
											g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()	
											
											SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_WARP_INTO_PARTNERS_CAR)
											
											GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bPlayerIsHiddenForWarp = TRUE
										ELSE
											#IF IS_DEBUG_BUILD
											NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_JOINING - waiting for partner to be in joining state") NET_NL()
											#ENDIF	
										ENDIF
									ELSE
										// if this is a rally driver and his partner is still alive in the car then respawn next to him then warp into car
										IF IsTeamModeSetForRespawning()
										AND DoIHaveAVehiclePartner(PartnerID)
										AND NOT IsRespawnPassenger()
										AND GlobalplayerBD[NATIVE_TO_INT(PartnerID)].iRespawnState > RESPAWN_STATE_JOINING
										AND GlobalplayerBD[NATIVE_TO_INT(PartnerID)].iRespawnState <> RESPAWN_STATE_WARP_INTO_PARTNERS_CAR
										AND ((NOT IS_ON_GTA_RACE_GLOBAL_SET()) OR IsPlayerInDriveableCar(PartnerID))										
											// player is a rally passenger, wait for other player to have respawned, respawn next to him then warp into car.
											vec = GET_PLAYER_COORDS(PartnerID)
											vec.z = -90.0
											NET_SPAWN_PLAYER(vec, 0.0, SPAWN_REASON_IN_VEHICLE, SPAWN_LOCATION_AT_SPECIFIC_COORDS)
											NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
											g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()	

											SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_WARP_INTO_PARTNERS_CAR)
											
											GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bPlayerIsHiddenForWarp = TRUE
										ELSE
											IF (GoodToUsePersonalVehicleToSpawn() AND HAS_CLEANED_UP_PREVIOUS_SAVED_VEHICLE_AND_IS_GOOD_TO_CREATE())
											OR NOT (GoodToUsePersonalVehicleToSpawn())
											OR NOT IS_SPAWNING_IN_VEHICLE()
											
												BOOL bUseNodeOffsets
												bUseNodeOffsets = TRUE
												
												IF IS_SPAWNING_IN_VEHICLE()
													IF IS_THIS_MODEL_A_HELI(g_SpawnData.MissionSpawnDetails.SpawnModel)
													OR IS_THIS_MODEL_A_PLANE(g_SpawnData.MissionSpawnDetails.SpawnModel)
														bUseNodeOffsets = FALSE	
													ENDIF
												ENDIF
											
												IF HAS_GOT_SPAWN_LOCATION(g_SpawnData.vCandSpawnPoint, g_SpawnData.fCandSpawnHeading, SPAWN_LOCATION_AUTOMATIC, g_SpawnData.MissionSpawnDetails.bSpawnInVehicle, g_SpawnData.bDoVisibilityChecksOnTeammates, DEFAULT, g_SpawnData.MissionSpawnDetails.bDontUseVehicleNodes, bUseNodeOffsets, DEFAULT, g_SpawnData.MissionSpawnDetails.fMinDistFromEnemyNearDeath, DEFAULT, g_SpawnData.MissionSpawnDetails.SpawnModel)
															
													#IF IS_DEBUG_BUILD
													PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("RESPAWN_STATE - after HAS_GOT_SPAWN_LOCATION returned TRUE")
													#ENDIF												
															
													UPDATE_PLAYERS_AMMO_ON_LAST_WEAPON()
													gbFM_start_tracking_weapons = FALSE
													gbFM_start_tracking_armour = FALSE
													//NET_PRINT(" [kw] CAN_FM_AMMO_TRACKER_RUN() CAnt drop weapon   gbFM_start_tracking_weapons = FALSE 2")
													NET_PRINT("[spawning] RESPAWN_STATE_JOINING - candidate point: ") NET_PRINT_VECTOR(g_SpawnData.vCandSpawnPoint) NET_NL()
														
													#IF IS_DEBUG_BUILD
													PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("RESPAWN_STATE - after UPDATE_PLAYERS_AMMO_ON_LAST_WEAPON")
													#ENDIF																															
																																
													// if player is respawning at gang house then try and recreate the personal vehicle.
													g_SpawnData.bSpawnPersonalVehicle = FALSE
													
													IF IsSpawnLocationAtGanghouse(FALSE)
														g_SpawnData.bSpawnPersonalVehicle = TRUE
													ENDIF					
																									
													IF IS_SPAWNING_IN_VEHICLE()					
													AND CAN_REGISTER_MISSION_VEHICLES(1) // if we can't actually create a new car then spawn on foot.
														// if spawning in vehicle then do it underground
														NET_SPAWN_PLAYER(<<g_SpawnData.vCandSpawnPoint.x, g_SpawnData.vCandSpawnPoint.y, GetHideZForModel(g_SpawnData.MissionSpawnDetails.SpawnModel)>>, g_SpawnData.fCandSpawnHeading, SPAWN_REASON_IN_VEHICLE, g_SpawnData.SpawnLocation, TRUE)
														NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
														ClearAreaAroundPointForSpawning(g_SpawnData.vCandSpawnPoint, TRUE) // clear the area where the car will be spawned.

														SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_MOVE_SPAWN_CAR_BACK_UP)
													ELSE
														#IF IS_DEBUG_BUILD
															IF IS_SPAWNING_IN_VEHICLE()					
															AND NOT CAN_REGISTER_MISSION_VEHICLES(1) // if we can't actually create a new car then spawn on foot.	
																SCRIPT_ASSERT("UPDATE_PLAYER_RESPAWN_STATE - want to spawn in vehicle but can't register any more mission vehicles. ")
																NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - want to spawn in vehicle but can't register any more mission vehicles. ") NET_NL()
																NET_PRINT(" Number of reserved vehicles = ") NET_PRINT_INT(GET_NUM_RESERVED_MISSION_VEHICLES()) NET_NL()
																NET_PRINT(" Current number of created mission vehicles = ") NET_PRINT_INT(GET_NUM_CREATED_MISSION_VEHICLES()) NET_NL()
															ENDIF
														#ENDIF
														
														IF g_SpawnData.bCargobobUseBehaviour
															
															g_SpawnData.iClosestCBPlayerIndex = -1
															g_SpawnData.fClosestCBPlayerDist = -1
															
															PRINTLN("=*=*=*=*=*= [JJT][spawning] Trying for cargobob respawn point =*=*=*=*=*=")
															FOR i = 0 TO 3
																PRINTLN("[JJT][spawning] >>>> Respawn point ", i)
																
																IF NOT ARE_VECTORS_EQUAL(g_SpawnData.vCargobobSpawnCoord[i], <<-1,-1,-1>>)
																	
																	//Get the closest ped to the spawn point
																	FOR y = 0 TO NETWORK_GET_MAX_NUM_PARTICIPANTS()-1
																		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(y))
																		AND NOT (PARTICIPANT_ID() = INT_TO_PARTICIPANTINDEX(y))
																		 	IF NOT IS_PED_INJURED(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(y))))
																				IF g_SpawnData.fClosestCBPlayerDist = -1
																					g_SpawnData.fClosestCBPlayerDist = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(y)))), g_SpawnData.vCargobobSpawnCoord[i])
																					g_SpawnData.iClosestCBPlayerIndex = y
																					
																					#IF IS_DEBUG_BUILD
																						PRINTLN("[JJT][spawning] ",y,": Distance to closest player not set. Setting to player ", y, ". Distance = ", g_SpawnData.fClosestCBPlayerDist)
																					#ENDIF
																				ELSE
																					IF VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(y)))), g_SpawnData.vCargobobSpawnCoord[i]) 
																					< g_SpawnData.fClosestCBPlayerDist
																					
																						g_SpawnData.iClosestCBPlayerIndex = y
																						g_SpawnData.fClosestCBPlayerDist = VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(y)))), g_SpawnData.vCargobobSpawnCoord[i])
																					
																						#IF IS_DEBUG_BUILD
																							PRINTLN("[JJT][spawning] ",y,": Setting to player ", y, ". Distance is ", g_SpawnData.fClosestCBPlayerDist, ".")
																							
																						#ENDIF
																					ELSE
																						#IF IS_DEBUG_BUILD
																							PRINTLN("[JJT][spawning] ",y,": Player distance is ",VDIST(GET_ENTITY_COORDS(GET_PLAYER_PED(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(y)))), g_SpawnData.vCargobobSpawnCoord[i]) 
																									," which isn't closer than player ", g_SpawnData.iClosestCBPlayerIndex, ".")
																						#ENDIF
																					ENDIF
																				ENDIF
																			#IF IS_DEBUG_BUILD
																			ELSE
																				PRINTLN("[JJT][spawning] ",y,": Ped is injured.")
																			#ENDIF
																			ENDIF
																		ENDIF
																	ENDFOR
																	
																	//1.002768 is on the spawn point
																	//1.111159 is right next to the spawn point
																	//1.350259 is distance between slot 1 and 2
																	IF g_SpawnData.fClosestCBPlayerDist > 1.2
																		PRINTLN("[JJT][spawning] Closest player is player ", g_SpawnData.iClosestCBPlayerIndex, " to spawn slot ", i, ". They are above the 1.2m cutoff so spawning here.")
																		g_SpawnData.vCandSpawnPoint = g_SpawnData.vCargobobSpawnCoord[i]
																		g_SpawnData.fCandSpawnHeading = g_SpawnData.fCargobobSpawnHeading[i]
																		g_SpawnData.iClosestCBPlayerIndex = CB_VALID_SLOT_CONFIRM
																		i = 4
																	ENDIF
																ELSE
																	PRINTLN("[JJT][spawning] Respawn point ", i, " is equal to <<-1,-1,-1>>! Not good.")
																ENDIF
																
																//If it hasn't found a valid slot and is moving onto the next one, reset these
																IF g_SpawnData.iClosestCBPlayerIndex != CB_VALID_SLOT_CONFIRM
																	
																	g_SpawnData.iClosestCBPlayerIndex = -1
																	g_SpawnData.fClosestCBPlayerDist = -1
																ENDIF
															ENDFOR
															
															IF g_SpawnData.iClosestCBPlayerIndex != CB_VALID_SLOT_CONFIRM
																PRINTLN("[JJT][spawning] We haven't found a suitable spawn slot so using the backup one.")
																g_SpawnData.vCandSpawnPoint = g_SpawnData.vCargobobBackupCoord
																g_SpawnData.fCandSpawnHeading = g_SpawnData.fCargobobSpawnHeading[0]
															ENDIF
														ENDIF

														NET_SPAWN_PLAYER(g_SpawnData.vCandSpawnPoint, g_SpawnData.fCandSpawnHeading, SPAWN_REASON_DEATH, g_SpawnData.SpawnLocation)
														NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION)
														SET_ENTITY_COORDS(PLAYER_PED_ID(), g_SpawnData.vCandSpawnPoint)
														PLAYER_RESPAWN_DO_LOAD_SCENE_IF_NECESSARY()
													ENDIF
												ENDIF	
												
												#IF IS_DEBUG_BUILD
												PRINT_NUMBER_OF_COMMANDS_EXECUTED_SINCE_LAST_CALL("RESPAWN_STATE - after HAS_GOT_SPAWN_LOCATION returned FALSE")
												#ENDIF										
											ELSE
												#IF IS_DEBUG_BUILD
												NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_JOINING - waiting on previous pv to cleanup") NET_NL()
												#ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
								NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_JOINING - waiting on spectator mode to end.") NET_NL()
								#ENDIF
								CLEANUP_KILL_CAM()
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_JOINING - waiting on load models") NET_NL()
							#ENDIF	
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_JOINING - waiting on swoop") NET_NL()
						#ENDIF						
					ENDIF
				ENDIF	
			ENDIF			
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("RESPAWN_STATE_JOINING")
			#ENDIF
			#ENDIF		
		BREAK
		
		
		// *******************************************************************
		//  RESPAWN_STATE_WARP_INTO_PARTNERS_CAR
		// *******************************************************************	
		CASE RESPAWN_STATE_WARP_INTO_PARTNERS_CAR
			BOOL bMultiVehicle
			bMultiVehicle = FALSE
			IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
			AND NOT g_bMissionEnding 
				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen < FMMC_MAX_TEAMS 
				AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen > -1		
					IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen].iTeamBitSet3, ciBS3_ENABLE_MULTIPLE_PLAYER_SAME_VEHICLE_RESPAWN)
						bMultiVehicle = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_NET_PLAYER_OK(PLAYER_ID())
			AND IsTeamModeSetForRespawning()	
			AND DoIHaveAVehiclePartner(PartnerID)
				IF PartnerID != INVALID_PLAYER_INDEX()
					// if partner is also in this state then the driver should create car
					IF ((GlobalplayerBD[NATIVE_TO_INT(PartnerID)].iRespawnState = RESPAWN_STATE_WARP_INTO_PARTNERS_CAR) AND NOT IsRespawnPassenger())				
						GOTO_JOINING_RESPAWN_STATE()
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - both in this state, driver will create car.") NET_NL()
						#ENDIF	
					ELSE
						IF IsPlayerInDriveableCar(PartnerID)
						
							PartnerVehicleID = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PartnerID))
						
							// if I'm warping into the driver seat, make sure i have network control
							IF NOT bMultiVehicle
								IF NOT IsRespawnPassenger()
								AND NOT IsPlayerDriverInDriveableCar(PartnerID)
									WarpPlayerIntoCar(PLAYER_PED_ID(), PartnerVehicleID, VS_DRIVER)	
									g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()	
									
									SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_WAIT_TO_BE_IN_PARTNERS_CAR)
									
									NET_PRINT("[Spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - Warping into partners car in vehicle seat VS_DRIVER") NET_NL()
								ELSE
									VEHICLE_SEAT vs_ToUse
									vs_ToUse = VS_FRONT_RIGHT
										
									
									IF IS_TARGET_ASSAULT_RACE()
										IF IS_THIS_MODEL_A_TURRET_VEHICLE(GET_ENTITY_MODEL(PartnerVehicleID))
											PRINTLN("[Spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - Is a turret vehicle. Looking for Turret Seat for Veh: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(PartnerVehicleID)))
											IF GET_FREE_RACE_TURRET_VEHICLE_SEAT_RESPAWN(PartnerVehicleID, vs_ToUse)
												PRINTLN("[Spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - Turret Seat")
												IF IS_TURRET_SEAT(PartnerVehicleID,vs_ToUse)
													IF NOT IS_ENTITY_UPRIGHT(PartnerVehicleID, 50.0)
														PRINTLN("[Spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - vehicle not upright. EXIT.")
														EXIT
													ENDIF
												ENDIF
											ELSE						
												PRINTLN("[Spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - No Free Seat at the moment. EXIT.")
												EXIT
											ENDIF
										ENDIF
									ENDIF
								
									g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()
									WarpPlayerIntoCar(PLAYER_PED_ID(), PartnerVehicleID, vs_ToUse)	
																		
									SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_WAIT_TO_BE_IN_PARTNERS_CAR)
									
									NET_PRINT("[Spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - Warping into partners car in vehicle seat: ") NET_PRINT_INT(ENUM_TO_INT(vs_ToUse)) NET_NL()
								ENDIF
							ELSE
							 	VEHICLE_SEAT vs_ToUse
								vs_ToUse = INT_TO_ENUM(VEHICLE_SEAT, GetFirstFreeSeatOfVehicle(PartnerVehicleID, TRUE))
								
								IF IS_VEHICLE_SEAT_FREE(PartnerVehicleID, INT_TO_ENUM(VEHICLE_SEAT, GET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE()))
									vs_ToUse = INT_TO_ENUM(VEHICLE_SEAT, GET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE())
								ENDIF
								
								PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - VehicleSeat: ", GET_PLAYER_RESPAWN_VEHICLE_SEAT_PREFERENCE())
								
								WarpPlayerIntoCar(PLAYER_PED_ID(), PartnerVehicleID, vs_ToUse, TRUE)
								g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()

								SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_WAIT_TO_BE_IN_PARTNERS_CAR)
								
								NET_PRINT("[Spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - Warping into partners car in preferred seat: ") NET_PRINT_INT(ENUM_TO_INT(vs_ToUse)) NET_NL()
							ENDIF
						ELSE
							NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - IsPlayerInDriveableCar = FALSE ") NET_NL()	
							IF IS_ON_GTA_RACE_GLOBAL_SET()
							OR bMultiVehicle
								// were they in same vehicle?
								IF WasPlayerInSameVehicleAsPlayer(PartnerID)
								OR isPlayerPartnerBackInVehicle(PartnerID) // For Adversary Mode.
									#IF IS_DEBUG_BUILD
									NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - IS_ON_GTA_RACE_GLOBAL_SET, but were in same car, waiting...") NET_NL()
									#ENDIF
									
									// if timeout has expired and partner is not in the process of respawning then go back to joining stage
									IF NOT IS_PLAYER_RESPAWNING(PartnerID)
										IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer) > 3000)	
											#IF IS_DEBUG_BUILD
											NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - partner is not respawning and timeout expired.") NET_NL()
											#ENDIF
											g_SpawnData.iBS_InCarWith = 0
											GOTO_JOINING_RESPAWN_STATE()
										ENDIF
									ELSE
										IF (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer) > 10000)	
											#IF IS_DEBUG_BUILD
											NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - partner is respawning and timeout expired.") NET_NL()
											#ENDIF
											g_SpawnData.iBS_InCarWith = 0
											GOTO_JOINING_RESPAWN_STATE()
										ENDIF
									ENDIF
									
								ELSE
									IF (bMultiVehicle = FALSE)
										GOTO_JOINING_RESPAWN_STATE()
										#IF IS_DEBUG_BUILD
										NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - IS_ON_GTA_RACE_GLOBAL_SET going to RESPAWN_STATE_JOINING") NET_NL()
										#ENDIF
									ELSE
										#IF IS_DEBUG_BUILD
										NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - bMultiVehicle = true ") NET_NL()
										#ENDIF
									ENDIF
								ENDIF
							ELSE
								// if partner is not in a car and is in a playing state then just go to playing state (race respawn will then warp him into car)
								IF (GlobalplayerBD[NATIVE_TO_INT(PartnerID)].iRespawnState = RESPAWN_STATE_PLAYING)
									IF NOT(g_b_On_Race)
										RestoreScreenFromSpawnBlur()
									ENDIF
									GOTO_RESPAWN_STATE_PLAYING()
									#IF IS_DEBUG_BUILD
									NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - IS_ON_GTA_RACE_GLOBAL_SET = FALSE, partner is still playing going to GOTO_RESPAWN_STATE_PLAYING") NET_NL()
									#ENDIF
								ENDIF
							ENDIF
							
							
							
						ENDIF
					ENDIF
				ENDIF
			ELSE
				GOTO_JOINING_RESPAWN_STATE()
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_PARTNERS_CAR - going to RESPAWN_STATE_JOINING") NET_NL()
				#ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("RESPAWN_STATE_WARP_INTO_PARTNERS_CAR")
			#ENDIF
			#ENDIF		
			
		BREAK
		
		// *******************************************************************
		//  RESPAWN_STATE_WAIT_TO_BE_IN_PARTNERS_CAR
		// *******************************************************************			
		CASE RESPAWN_STATE_WAIT_TO_BE_IN_PARTNERS_CAR
			IF IS_NET_PLAYER_OK(PLAYER_ID())
			AND (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer) < 10000)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					// load the scene
					//IF SHOULD_SWOOP_CAM_DO_A_LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), g_SpawnData.vStartSwoop)	
					IF SHOULD_RESPAWN_DO_A_LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), g_SpawnData.vBeforeRespawnCoords)	
						NEW_LOAD_SCENE_START(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<SIN(GET_ENTITY_HEADING(PLAYER_PED_ID())), COS(GET_ENTITY_HEADING(PLAYER_PED_ID())), 0.0>>, 50.0)																
						g_SpawnData.bDoLoadScene = TRUE
						g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()
					ELSE
						g_SpawnData.iLoadSceneTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), (LOAD_SCENE_TIME_OUT+1)*-1) 
					ENDIF
					
					SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_LOAD_SCENE)
				ELSE
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_TO_BE_IN_PARTNERS_CAR - IS_PED_IN_ANY_VEHICLE = FALSE") NET_NL()
					#ENDIF		
					// if player has somehow lost the warp into car task
					IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE ) = FINISHED_TASK)
						#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_TO_BE_IN_PARTNERS_CAR - lost task - going back to RESPAWN_STATE_WARP_INTO_PARTNERS_CAR") NET_NL()
						#ENDIF	
						SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_WARP_INTO_PARTNERS_CAR)
					ENDIF
				ENDIF
			ELSE
				GOTO_JOINING_RESPAWN_STATE()
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_TO_BE_IN_PARTNERS_CAR - going to RESPAWN_STATE_JOINING") NET_NL()
				#ENDIF
			ENDIF	

			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("RESPAWN_STATE_WAIT_TO_BE_IN_PARTNERS_CAR")
			#ENDIF
			#ENDIF			
			
		BREAK
		
	
		// *******************************************************************
		//  RESPAWN_STATE_WARP_INTO_SPECIFIC_CAR
		// *******************************************************************		
	
		CASE RESPAWN_STATE_WARP_INTO_SPECIFIC_CAR	
			IF IS_NET_PLAYER_OK(PLAYER_ID())
			AND DoesSpecificRespawnVehicleExist()

				// need to find which seat to warp into
				IF HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE(g_SpawnData.SpecificVehicleID, iSeat, g_SpawnData.SpecificVehicleSeat)
				
					WarpPlayerIntoCar(PLAYER_PED_ID(), g_SpawnData.SpecificVehicleID, INT_TO_ENUM(VEHICLE_SEAT, iSeat))
					g_SpawnData.SpecificVehicleSeat = VS_INVALID
					g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()	
					
					SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_WAIT_TO_BE_IN_SPECIFIC_CAR)
					
					PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_SPECIFIC_CAR - warping into specific car in seat ", iSeat) 
				ELSE
					PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_SPECIFIC_CAR - waiting on HAS_GOT_SEAT_FOR_SPECIFIC_RESPAWN_VEHICLE") 
				ENDIF

			ELSE
				GOTO_JOINING_RESPAWN_STATE()
				#IF IS_DEBUG_BUILD
					IF NOT DoesSpecificRespawnVehicleExist()
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_SPECIFIC_CAR - DoesSpecificRespawnVehicleExist = FALSE") NET_NL()	
					ENDIF
					IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WARP_INTO_SPECIFIC_CAR - IS_NET_PLAYER_OK = FALSE ") NET_NL()
					ENDIF
				#ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("RESPAWN_STATE_WARP_INTO_SPECIFIC_CAR")
			#ENDIF
			#ENDIF				
		BREAK
		
		CASE RESPAWN_STATE_WAIT_TO_BE_IN_SPECIFIC_CAR							
			IF IS_NET_PLAYER_OK(PLAYER_ID())
			AND DoesSpecificRespawnVehicleExist()
			AND (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer) < 10000)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					// load the scene
					//IF SHOULD_SWOOP_CAM_DO_A_LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), g_SpawnData.vStartSwoop)	
					IF SHOULD_RESPAWN_DO_A_LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), g_SpawnData.vBeforeRespawnCoords)	
						NEW_LOAD_SCENE_START(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<SIN(GET_ENTITY_HEADING(PLAYER_PED_ID())), COS(GET_ENTITY_HEADING(PLAYER_PED_ID())), 0.0>>, 50.0)																
						g_SpawnData.bDoLoadScene = TRUE
						g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()
					ELSE
						g_SpawnData.iLoadSceneTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), (LOAD_SCENE_TIME_OUT+1)*-1) 
					ENDIF

					SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_LOAD_SCENE)
				ELSE
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_TO_BE_IN_SPECIFIC_CAR - IS_PED_IN_ANY_VEHICLE = FALSE") NET_NL()
					#ENDIF		
					// if player has somehow lost the warp into car task
					IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE ) = FINISHED_TASK)
						#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_TO_BE_IN_SPECIFIC_CAR - lost task - going back to RESPAWN_STATE_WARP_INTO_SPECIFIC_CAR") NET_NL()
						#ENDIF	
						
						SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_WARP_INTO_SPECIFIC_CAR)
					ENDIF
				ENDIF
			ELSE
				GOTO_JOINING_RESPAWN_STATE()
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_TO_BE_IN_SPECIFIC_CAR - going to RESPAWN_STATE_JOINING") NET_NL()
				#ENDIF
			ENDIF	

			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("RESPAWN_STATE_WAIT_TO_BE_IN_SPECIFIC_CAR")
			#ENDIF
			#ENDIF				
		BREAK

//		// *******************************************************************
//		//  RESPAWN_STATE_DO_NET_WARP_WITH_CAR
//		// *******************************************************************	
//		CASE RESPAWN_STATE_DO_NET_WARP_WITH_CAR
//			IF IS_NET_PLAYER_OK(PLAYER_ID())
//				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//				OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer) > 5000)
//					IF NET_WARP_TO_COORD(g_SpawnData.vCandSpawnPoint, g_SpawnData.fCandSpawnHeading, TRUE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
//						g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()
//						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_LOAD_SPAWN_VEHICLE
//						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_DO_NET_WARP_WITH_CAR - going to RESPAWN_STATE_LOAD_SPAWN_VEHICLE ") NET_NL()
//					ELSE
//						#IF IS_DEBUG_BUILD
//						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_DO_NET_WARP_WITH_CAR - waiting for net warp to coord - time = ") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer)) NET_NL()
//						#ENDIF
//					ENDIF
//				ELSE
//					#IF IS_DEBUG_BUILD
//					NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_DO_NET_WARP_WITH_CAR - waiting for player to be in vehicle... ") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer)) NET_NL()
//					#ENDIF
//				ENDIF
//			ELSE
//				#IF IS_DEBUG_BUILD
//				NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_DO_NET_WARP_WITH_CAR = player dead or timeout hit... ") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer)) NET_NL()
//				#ENDIF
//				GOTO_JOINING_RESPAWN_STATE()
//			ENDIF
//		BREAK

		// *******************************************************************
		//  MOVE SPAWN CAR BACK UP TO THE PROPER Z AFTER PLAYER IS IN IT
		// *******************************************************************	
		CASE RESPAWN_STATE_MOVE_SPAWN_CAR_BACK_UP
			IF IS_SPAWNING_IN_VEHICLE()
				IF IS_NET_PLAYER_OK(PLAYER_ID())
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_MOVE_SPAWN_CAR_BACK_UP = player is in car, proceeding.") NET_NL()
						#ENDIF	
						CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						SET_ENTITY_COORDS(CarID, g_SpawnData.vCandSpawnPoint,FALSE,TRUE,TRUE)
						IF (g_SpawnData.fPitch != 0.0)
							SET_ENTITY_ROTATION(CarID, <<g_SpawnData.fPitch, 0.0, g_SpawnData.fCandSpawnHeading>>)
							PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - SET_ENTITY_ROTATION with ", <<g_SpawnData.fPitch, 0.0, g_SpawnData.fCandSpawnHeading>>)
						ENDIF
						IF (g_SpawnData.MissionSpawnDetails.bSpawningCreatedNewCar)
							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bHasRespawnedInVehicle = TRUE
						ELSE
							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bHasRespawnedInVehicle = FALSE
						ENDIF
						//GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_WAIT_FOR_PLAYER_TO_BE_IN_CAR
						PLAYER_RESPAWN_DO_LOAD_SCENE_IF_NECESSARY()
						
						// if we are on a race, start the respot timer early, to avoid 4484234
						IF (g_b_On_Race)
							SET_PLAYER_HAS_JUST_RESPAWNED_IN_RACE_AT_COORDS(g_SpawnData.vCandSpawnPoint)
							PRINTLN("[spawning] - starting respot timer early")
						ENDIF
						
					ELSE
						IF DOES_ENTITY_EXIST(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
						AND NOT IS_ENTITY_DEAD(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
							IF NOT IS_PED_IN_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE(PLAYER_PED_ID(), g_SpawnData.MissionSpawnDetails.SpawnedVehicle) 
								#IF IS_DEBUG_BUILD
								NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_MOVE_SPAWN_CAR_BACK_UP - IS_PED_IN_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE = FALSE") NET_NL()
								#ENDIF	
								WarpPlayerIntoCar(PLAYER_PED_ID(), g_SpawnData.MissionSpawnDetails.SpawnedVehicle, VS_DRIVER)
							ELSE
								#IF IS_DEBUG_BUILD
								NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_MOVE_SPAWN_CAR_BACK_UP = waiting for player ped to be in car") NET_NL()
								#ENDIF	
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_MOVE_SPAWN_CAR_BACK_UP = g_SpawnData.MissionSpawnDetails.SpawnedVehicle does not exist") NET_NL()
							#ENDIF
							
							// is the pv in the process of being created?
							IF IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_BEING_CREATED)
							OR IS_SAVED_VEHICLE_FLAG_SET(MP_SAVED_VEH_FLAG_JUST_CREATED)
								PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_MOVE_SPAWN_CAR_BACK_UP - waiting on personal vehicle to be created")
								
								// does it exist? then set it as the vehicle for the player to warp into.
								SetVehicleForWarpIntoSpawnVehicle(PERSONAL_VEHICLE_ID())
								
							ELSE
								GOTO_JOINING_RESPAWN_STATE()
							ENDIF
						ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_MOVE_SPAWN_CAR_BACK_UP = player dead") NET_NL()
					#ENDIF
					GOTO_JOINING_RESPAWN_STATE()				
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_MOVE_SPAWN_CAR_BACK_UP - IS_SPAWNING_IN_VEHICLE()") NET_NL()
				#ENDIF
				PLAYER_RESPAWN_DO_LOAD_SCENE_IF_NECESSARY()
			ENDIF
		BREAK
		
//		// *******************************************************************
//		//  after the SET_ENTITY_COORDS must wait for player to actually be in car
//		// *******************************************************************	
//		CASE RESPAWN_STATE_WAIT_FOR_PLAYER_TO_BE_IN_CAR
//			IF (g_SpawnData.MissionSpawnDetails.bSpawnInVehicle)
//				IF IS_NET_PLAYER_OK(PLAYER_ID())
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						#IF IS_DEBUG_BUILD
//						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_FOR_PLAYER_TO_BE_IN_CAR = player is in car, proceeding.") NET_NL()
//						#ENDIF	
//						PLAYER_RESPAWN_DO_LOAD_SCENE_IF_NECESSARY()
//					ELSE
//						#IF IS_DEBUG_BUILD
//						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_FOR_PLAYER_TO_BE_IN_CAR = waiting for player ped to be in car") NET_NL()
//						#ENDIF	
//					ENDIF
//				ELSE
//					#IF IS_DEBUG_BUILD
//					NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAIT_FOR_PLAYER_TO_BE_IN_CAR = player dead") NET_NL()
//					#ENDIF
//					GOTO_JOINING_RESPAWN_STATE()				
//				ENDIF
//			ELSE
//				PLAYER_RESPAWN_DO_LOAD_SCENE_IF_NECESSARY()
//			ENDIF
//		BREAK		
		
		// *******************************************************************
		//  LOAD SCENE
		// *******************************************************************			
		CASE RESPAWN_STATE_LOAD_SCENE
					
			bContinue = FALSE
			g_SpawnData.bIsDoingLoadScene = FALSE
			
			IF (g_SpawnData.bDoLoadScene)
			
				// An attempt to fix bug 1734099 KW 28/8/14
				IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, FALSE)
					IF eCurrentParachuteState != PPS_SKYDIVING
					OR eCurrentParachuteState != PPS_PARACHUTING
					OR eCurrentParachuteState != PPS_DEPLOYING
						MakePlayerVisibleLocally() // make player visible locally behind the blur
						eCurrentParachuteState= PPS_INVALID
					ELSE
						PRINTLN("MakePlayerVisibleLocally = FALSE in spawn because player is parachuting KW")
					
					ENDIF
				ENDIF
				IF IS_SPAWNING_IN_HELI_NON_RACE()
					iAdditionalLoadsceneTime = 3000
				ELSE
					iAdditionalLoadsceneTime += 0
				ENDIF
			
				// if player is moving then abandon load scene
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					IF (GET_ENTITY_SPEED(PLAYER_PED_ID()) > 0.5)
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - abandoning load scene because player is moving ")  NET_NL()
						NEW_LOAD_SCENE_STOP()	
					ENDIF
				ENDIF
			
				IF (((IS_NEW_LOAD_SCENE_ACTIVE() AND IS_NEW_LOAD_SCENE_LOADED()) OR NOT IS_NEW_LOAD_SCENE_ACTIVE()) AND HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID()))
				OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer) >   (LOAD_SCENE_TIME_OUT + iAdditionalLoadsceneTime))
					
					#IF IS_DEBUG_BUILD
						IF IS_NEW_LOAD_SCENE_ACTIVE()
							IF IS_NEW_LOAD_SCENE_LOADED()
								NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_LOAD_SCENE = IS_NEW_LOAD_SCENE_LOADED() = TRUE ")  NET_NL()
							ELSE
								NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_LOAD_SCENE = IS_NEW_LOAD_SCENE_LOADED() = FALSE ")  NET_NL()
							ENDIF
						ENDIF
					#ENDIF	
					
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					ENDIF
					CLEAR_FOCUS()
					
					g_SpawnData.bDoLoadScene = FALSE
					bContinue = TRUE
					
				ELSE
					#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_LOAD_SCENE - ")
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NET_PRINT("IS_NEW_LOAD_SCENE_LOADED = ") NET_PRINT_BOOL(IS_NEW_LOAD_SCENE_LOADED())
					ELSE
						NET_PRINT("IS_NEW_LOAD_SCENE_ACTIVE = FALSE") NET_PRINT_BOOL(IS_NEW_LOAD_SCENE_LOADED())
					ENDIF
					NET_PRINT(" HAS_COLLISION_LOADED_AROUND_ENTITY = ") NET_PRINT_BOOL(HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID())) 					
					NET_PRINT(" - time ") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer))															
					#ENDIF	
				ENDIF
			ELSE
				bContinue = TRUE
			ENDIF
			
			IF (bContinue)

				IF (g_SpawnData.bSpawnPersonalVehicle)					
					g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()
					
					SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_LOAD_PERSONAL_VEHICLE)
				ELSE	
					IF IS_SPAWNING_IN_VEHICLE()
						IF IsTeamModeSetForRespawning()	
						AND IsRespawnPassenger()
							// wait for passenger. 
							g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()
							
							SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_WAITING_FOR_PARTNER)
						ELSE
							g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()

							SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_LOAD_SPAWN_VEHICLE)
						ENDIF
					ELSE
						StartFadeInAfterRespawn()
					ENDIF
				ENDIF	
			
			ELSE
				
				g_SpawnData.bIsDoingLoadScene = TRUE	
				
			ENDIF
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("RESPAWN_STATE_LOAD_SCENE")
			#ENDIF
			#ENDIF				
			
		BREAK
		

		
		// *******************************************************************
		//  LOAD PERSONAL VEHICLE
		// *******************************************************************			
		CASE RESPAWN_STATE_LOAD_PERSONAL_VEHICLE
						
			IF (g_SpawnData.bSpawnPersonalVehicle)				
				IF WARP_EXISTING_MP_SAVED_VEHICLE_NEARBY()
				OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer) > 5000)	
					StartFadeInAfterRespawn()	
				ENDIF
			ENDIF

			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("RESPAWN_STATE_LOAD_PERSONAL_VEHICLE")
			#ENDIF
			#ENDIF			
			
		BREAK

		
		// *******************************************************************
		//  RESPAWN_STATE_LOAD_SPAWN_VEHICLE
		// *******************************************************************
		CASE RESPAWN_STATE_LOAD_SPAWN_VEHICLE
			IF IS_NET_PLAYER_OK(PLAYER_ID())

				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SuppressInAirEvent, TRUE) // make sure player can't fall from the sky
	
				#IF IS_DEBUG_BUILD
					NET_PRINT_FRAME() NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_LOAD_SPAWN_VEHICLE - GET_SCRIPT_TASK_STATUS = ") NET_PRINT_INT(ENUM_TO_INT(GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE ))) NET_NL()
				#ENDIF
			
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
				OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer) > 5000)
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_LOAD_SPAWN_VEHICLE - StartFadeInAfterRespawn called") NET_NL()
					#ENDIF
					// if on a rally goto wait for partner
					IF IsTeamModeSetForRespawning()
					AND NOT IsRespawnPassenger()
							
						#IF IS_DEBUG_BUILD
							IF IsTeamModeSetForRespawning()
								NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - IsTeamModeSetForRespawning = TRUE") NET_NL()
							ENDIF
							IF IsRespawnPassenger()
								NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - IsRespawnPassenger = TRUE") NET_NL()
							ENDIF
						#ENDIF
																	
						// wait for passenger. 
						g_SpawnData.iLoadSceneTimer = GET_NETWORK_TIME()

						SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_WAITING_FOR_PARTNER)
					ELSE
						StartFadeInAfterRespawn()
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_LOAD_SPAWN_VEHICLE - waiting for ped to be in vehicle") NET_NL()
						
						IF DOES_ENTITY_EXIST(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
							NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_LOAD_SPAWN_VEHICLE - g_SpawnData.MissionSpawnDetails.SpawnedVehicle EXISTS") NET_NL()	
						ELSE
							NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_LOAD_SPAWN_VEHICLE - g_SpawnData.MissionSpawnDetails.SpawnedVehicle DEOS NOT EXIST") NET_NL()	
						ENDIF
						
					#ENDIF
				ENDIF
			ELSE
				StartFadeInAfterRespawn()
				#IF IS_DEBUG_BUILD					
					NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_LOAD_SPAWN_VEHICLE - player is not ok") NET_NL()
				#ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("RESPAWN_STATE_LOAD_SPAWN_VEHICLE")
			#ENDIF
			#ENDIF
			
		BREAK		
		
		// *******************************************************************
		//  RESPAWN_STATE_WAITING_FOR_PARTNER
		// *******************************************************************	
		CASE RESPAWN_STATE_WAITING_FOR_PARTNER
			IF IS_NET_PLAYER_OK(PLAYER_ID())
			AND (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer) < 10000)
			AND IsTeamModeSetForRespawning()	
			AND DoIHaveAVehiclePartner(PartnerID)
				//IF IsMyCarVisible()
					IF GlobalplayerBD[NATIVE_TO_INT(PartnerID)].iRespawnState >= RESPAWN_STATE_WAITING_FOR_PARTNER
						StartFadeInAfterRespawn()
					ELSE
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAITING_FOR_PARTNER - GlobalplayerBD[NATIVE_TO_INT(PartnerID)].iRespawnState = ") NET_PRINT_INT(GlobalplayerBD[NATIVE_TO_INT(PartnerID)].iRespawnState) NET_NL()	
						// check my car isn't locked for partner
						IF NOT IS_ENTITY_DEAD(g_SpawnData.MissionSpawnDetails.SpawnedVehicle)
							IF GET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(g_SpawnData.MissionSpawnDetails.SpawnedVehicle, PartnerID)								
								SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(g_SpawnData.MissionSpawnDetails.SpawnedVehicle, PartnerID, FALSE)	
								PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAITING_FOR_PARTNER - unlocking spawn car")
							ELSE
								PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAITING_FOR_PARTNER - spawn car is unlocked")	
							ENDIF
						ENDIF
					ENDIF
				//ELSE
				//	NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAITING_FOR_PARTNER - waiting for car to be visible ")  NET_NL()					
				//ENDIF
			ELSE
				StartFadeInAfterRespawn()
				#IF IS_DEBUG_BUILD					
					IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAITING_FOR_PARTNER - player is not ok") NET_NL()
					ENDIF
					IF NOT (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer) < 10000)
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAITING_FOR_PARTNER - timeout") NET_NL()
					ENDIF
					IF NOT IsTeamModeSetForRespawning()	
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAITING_FOR_PARTNER - IsTeamModeSetForRespawning = fALSE") NET_NL()
					ENDIF
					IF NOT DoIHaveAVehiclePartner(PartnerID)	
						NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_WAITING_FOR_PARTNER - DoIHaveAVehiclePartner(PartnerID) = fALSE") NET_NL()
					ENDIF
				#ENDIF
			ENDIF

			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("RESPAWN_STATE_WAITING_FOR_PARTNER")
			#ENDIF
			#ENDIF			
			
		BREAK
		
		// *******************************************************************
		//   wait for leaderboard to finish placing player
		// *******************************************************************		
		CASE RESPAWN_STATE_WAIT_FOR_MANUAL_RESPAWN_PLACEMENT

			IF (GET_MANUAL_RESPAWN_STATE() = MRS_FINISH_RESPAWN)
				StartFadeInAfterRespawn()
			ELSE				
				#IF IS_DEBUG_BUILD																			
					NET_PRINT("[spawning] RESPAWN_STATE_WAIT_FOR_MANUAL_RESPAWN_PLACEMENT - waiting on g_SpawnData.eManualRespawnState, current value is") NET_PRINT(GET_MANUAL_RESPAWN_STATE_STRING(GET_MANUAL_RESPAWN_STATE())) NET_NL()	
				#ENDIF
				IF NOT (GET_MANUAL_RESPAWN_STATE() = MRS_READY_FOR_PLACEMENT)					
//					#IF IS_DEBUG_BUILD
//						SCRIPT_ASSERT("RESPAWN_STATE_WAIT_FOR_MANUAL_RESPAWN_PLACEMENT - eManualRespawnState is not MRS_READY_FOR_PLACEMENT - something has gone wrong, my money is on William breaking it. ")
//					#ENDIF					
					SET_MANUAL_RESPAWN_STATE(MRS_READY_FOR_PLACEMENT)
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("RESPAWN_STATE_WAIT_FOR_MANUAL_RESPAWN_PLACEMENT")
			#ENDIF
			#ENDIF				
			
		BREAK

//		// *******************************************************************
//		//  SPAWN ACTIVITY
//		// *******************************************************************		
//		CASE RESPAWN_STATE_WAIT_SPAWN_ACTIVITY
//			IF IS_SPAWN_ACTIVITY_READY()
//			OR (GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer) > 10000)
//				StartFadeInAfterRespawn()								
//			ELSE
//				#IF IS_DEBUG_BUILD																			
//					NET_PRINT("[spawning] RESPAWN_STATE_WAIT_SPAWN_ACTIVITY - waiting on IS_SPAWN_ACTIVITY_READY, time = ") NET_PRINT_INT(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.iLoadSceneTimer)) NET_NL()	
//				#ENDIF	
//			ENDIF
//		BREAK

		// *******************************************************************
		//  SPAWN STATE PLAYING
		// *******************************************************************			
		CASE RESPAWN_STATE_PLAYING			
											
			IF (g_SpawnData.bForceRespawn)
				g_SpawnData.bForceRespawn = FALSE
				PRINTLN("[spawning] RESPAWN_STATE_PLAYING - resetting bForceRespawn ")
			ENDIF
			IF (g_SpawnData.bForceThroughJoining)
				g_SpawnData.bForceThroughJoining = FALSE
				PRINTLN("[spawning] RESPAWN_STATE_PLAYING - resetting bForceThroughJoining ")
			ENDIF
					
			// if player has just respawned and invincible, disable him from firing.
			IF IS_NET_PLAYER_OK(PLAYER_ID())		
				
				UPDATE_DRUNK_RESPAWN() // if player has passed out drunk
				
				IF NETWORK_IS_LOCAL_PLAYER_INVINCIBLE()
					DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_ATTACK)	
					DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_DETONATE)	
					
					// if player is entering a vehicle then remove invincibility
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), FALSE)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
							NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(0)
							NET_PRINT("[spawning] RESPAWN_STATE_PLAYING - removing invincibility as player is entering vehicle.") NET_NL()
						ENDIF
					ENDIF
					
				ENDIF
				
				// if player is in a car, store the network id, so partners can tell if they were in the same car for gta races			
				#IF IS_DEBUG_BUILD
					stored_iBS_InCarWith = g_SpawnData.iBS_InCarWith
				#ENDIF
				
				
				IF IS_SCREEN_FADED_IN()
					g_SpawnData.iBS_InCarWith = 0 // only clear this if fully faded in
				ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					CarID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
									
					// check driver
					IF NOT IS_VEHICLE_SEAT_FREE(CarID, VS_DRIVER )
						PlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(CarID, VS_DRIVER))
						IF NOT (PlayerID = PLAYER_ID())
							IF NOT (PlayerID = INT_TO_NATIVE(PLAYER_INDEX, -1))
								SET_BIT(g_SpawnData.iBS_InCarWith, NATIVE_TO_INT(PlayerID))
							ENDIF
						ENDIF
					ENDIF
						
					// loop through passenger
					REPEAT GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(CarID) i
						IF NOT IS_VEHICLE_SEAT_FREE(CarID, INT_TO_ENUM(VEHICLE_SEAT, i) )
							PlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_IN_VEHICLE_SEAT(CarID, INT_TO_ENUM(VEHICLE_SEAT, i)))
							IF NOT (PlayerID = PLAYER_ID())
								IF NOT (PlayerID = INT_TO_NATIVE(PLAYER_INDEX, -1))
									SET_BIT(g_SpawnData.iBS_InCarWith, NATIVE_TO_INT(PlayerID))
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					g_SpawnData.MyLastVehicle = CarID
					IF (GET_PED_IN_VEHICLE_SEAT(CarID, VS_DRIVER) = PLAYER_PED_ID())
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bIWasDriving = TRUE
					ELSE
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bIWasDriving = FALSE	
					ENDIF
					// store vehicle weapon if has one. (for air races)
					IF (g_b_Is_Air_Race) 
					OR (g_bFM_ON_VEH_DEATHMATCH)
					OR (g_b_IsGTARace)
					OR g_SpawnData.bRememberVehicleWeaponOnSpawn
						IF NOT (g_SpawnData.ResetOnSpawn.bHasSetVehicleWeapon)					
						//OR g_SpawnData.VehicleWeaponModel != GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							// set the vehicle weapons
							IF NOT IS_PLAYER_USING_ARENA()
								IF SetVehicleWeapon()
									g_SpawnData.ResetOnSpawn.bHasSetVehicleWeapon = TRUE
								ENDIF
							ELSE	
								// done via APPLY_ARENA_SETTINGS_TO_PLAYER_VEH for Arena.
								g_SpawnData.ResetOnSpawn.bHasSetVehicleWeapon = TRUE
							ENDIF
							
							
						ELSE
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								GET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(), g_SpawnData.VehicleWeapon)								g_SpawnData.UseNonHomingMissiles = IS_PLAYER_VEHICLE_WEAPON_TOGGLED_TO_NON_HOMING(NETWORK_GET_PLAYER_INDEX_FROM_PED(PLAYER_PED_ID()))
								g_SpawnData.VehicleWeaponModel = GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								// Remember the Vehicle stealth mode setting
								g_SpawnData.bUseVehStealthMode = IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(GET_PLAYER_INDEX())].iBSVehStealthMode, GLOBAL_BS_VEH_STEALTH_MODE_ON)
								PRINTLN("g_SpawnData.bUseVehStealthMode: ", g_SpawnData.bUseVehStealthMode)
							ENDIF
						ENDIF
						
						IF NOT (g_SpawnData.ResetOnSpawn.bHasSetStealthMode)
							//Handles Vehicle Stealth mode
							HANDLE_VEH_STEALTH_SPAWNING(g_SpawnData.bUseVehStealthMode)
							g_SpawnData.ResetOnSpawn.bHasSetStealthMode = TRUE
						ENDIF
					ENDIF
					
					
					
					// set the radio station
					RESTORE_RADIO_FOR_SPAWNED_VEHICLE()
					
				ELSE						
					
					// only reset when fully faded in
					IF IS_SCREEN_FADED_IN()
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bIWasDriving = FALSE	
						g_SpawnData.MyLastVehicle = INT_TO_NATIVE(VEHICLE_INDEX, -1)
					ENDIF
					
					g_SpawnData.ResetOnSpawn.bHasSetVehicleWeapon = FALSE
				ENDIF
				
				
				// store last used vehicle (properly - there is a bug with GET_PLAYERS_LAST_VEHICLE() - see 2403649)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT IS_PLAYER_SPECTATING(PLAYER_ID())				
						g_SpawnData.LastUsedVehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					ENDIF
				ENDIF
				
				#IF IS_DEBUG_BUILD
					IF (stored_iBS_InCarWith != g_SpawnData.iBS_InCarWith)
						PRINTLN("[spawning] RESPAWN_STATE_PLAYING - g_SpawnData.iBS_InCarWith changed to ", g_SpawnData.iBS_InCarWith)
					ENDIF
				#ENDIF
				//NET_PRINT("g_SpawnData.iBS_InCarWith = ") NET_PRINT_INT(g_SpawnData.iBS_InCarWith) NET_NL()
				
				
				// if player is ok and has control, make sure the fade out for interior switch flags are not left dangling.
				IF (g_bDoHideSwoopUpForInteriorsCleanupInNetSpawn)
					IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					AND IS_SKYSWOOP_AT_GROUND()
					AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
					AND NOT IS_PLAYER_IN_CORONA()
					AND NOT (g_bCelebrationScreenIsActive)
					AND NOT (g_bMissionEnding)
					AND GET_TOGGLE_PAUSED_RENDERPHASES_STATUS() // we are NOT paused.  url:bugstar:6188512 - Finale - Subterfuge_2B -  The Local player became stuck after quitting Subterfuge_2B using the phone after a quick restart. Remote players became stuck in skycam.
					AND HAS_MIN_RUN_TIME_EXPIRED_FOR_TRANSITION_FADE() //url:bugstar:6263334 - Stuck on a black screen leaving the doomsday heist activity session on the outfit select screen.
					
						IF HAS_TRANSITION_FADE_OUT_FOR_INTERIOR_SWITCH_STARTED()
							#IF IS_DEBUG_BUILD
								NET_PRINT("[spawning] RESPAWN_STATE_PLAYING - HAS_TRANSITION_FADE_OUT_FOR_INTERIOR_SWITCH_STARTED is true, TIME_SINCE_FADE_OUT_FOR_INTERIOR_STARTED = ")
								NET_PRINT_INT(TIME_SINCE_FADE_OUT_FOR_INTERIOR_STARTED())
								NET_NL()
							#ENDIF
							
							PRINTLN("[spawning] RESPAWN_STATE_PLAYING - HAS_TRANSITION_FADE_OUT_FOR_INTERIOR_SWITCH_STARTED is true, calling CLEAR_TRANSITION_FADE_OUT_FOR_INTERIOR_SWITCH")
							CLEAR_TRANSITION_FADE_OUT_FOR_INTERIOR_SWITCH()	
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF		
			
			
			STORE_CURRENT_RADIO_STATION_FOR_RESPAWN()


						
			// Has the player died?
			IF NOT IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, FALSE)
				
				// Store weapon player died with to reattach components on spawn B* 2342502
				GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), g_SpawnData.MyLastWeapon, FALSE)
				
				IF NOT IS_BIT_SET(g_sShopSettings.iProperties[CARMOD_SHOP_SUPERMOD], SHOP_NS_FLAG_SHOP_SCRIPT_RUNNING)
					STORE_RADIO_STATION_FOR_RESPAWN()
				ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					// store radio station
					//g_SpawnData.iRadioStation = GET_PLAYER_RADIO_STATION_INDEX()
					//PRINTLN("[spawning] RESPAWN_STATE_PLAYING - g_SpawnData.iRadioStation = ", GET_RADIO_STATION_NAME(g_SpawnData.iRadioStation))
					
//					// store the vehicle weapon
//					IF NOT GET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(), g_SpawnData.VehicleWeapon)
//						g_SpawnData.VehicleWeapon = WEAPONTYPE_INVALID
//						NET_PRINT("[spawning] RESPAWN_STATE_PLAYING - GET_CURRENT_PED_VEHICLE_WEAPON = FALSE ")  NET_NL()
//					ENDIF
																			
					SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT) // make sure dead players can ragdoll fall from vehicle

					
					NET_PRINT("[spawning] RESPAWN_STATE_PLAYING - g_SpawnData.VehicleWeapon = ") NET_PRINT_INT(ENUM_TO_INT(g_SpawnData.VehicleWeapon)) NET_NL()
				ELSE
					NET_PRINT("[spawning] RESPAWN_STATE_PLAYING - IS_PED_IN_ANY_VEHICLE = FALSE")  NET_NL()	
				ENDIF
			
				//...and player is not on the transition HUD
				IF NOT IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
			
					SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_DEAD)
					
	//				INCREMENT_DEATH_STAT()
					// Have I done killed myself?
	//				IF NETWORK_GET_KILLER_OF_PLAYER(PLAYER_ID(), weaponUsed) = PLAYER_ID()
	//					INCREMENT_SUICIDE_STAT()
	//				ENDIF

					// 735447 Clean up pause menu when I die
					IF IS_PAUSE_MENU_ACTIVE()
						SET_FRONTEND_ACTIVE(FALSE)
					ENDIF
					
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
							
					SWITCH GET_CURRENT_GAMEMODE()
						CASE GAMEMODE_FM
						
							#IF FEATURE_FREEMODE_ARCADE
							IF NOT IS_FREEMODE_ARCADE()
								DROP_CASH_WHEN_DEAD()
							ENDIF
							#ENDIF
							
							#IF NOT FEATURE_FREEMODE_ARCADE
							DROP_CASH_WHEN_DEAD()
							#ENDIF
							
						BREAK
						DEFAULT
							NET_PRINT("[spawning] RESPAWN_STATE_PLAYING - GET_CURRENT_GAMEMODE() = ") NET_PRINT_INT(ENUM_TO_INT(GET_CURRENT_GAMEMODE())) NET_NL()
						BREAK
					ENDSWITCH
					g_SpawnData.bSpawnedInHospital = FALSE
					// Reset spectator flag as need to wait for it to be active
					g_SpawnData.bHasSpectatorCamBeenActive = FALSE
					
					// Default our respawn value for all modes that skip killstrip
					g_SpawnData.bPlayerWantsToRespawn = TRUE
					g_SpawnData.bSpawnLocHadFinalCheck = FALSE
					
					// Update on screen visuals based on mode
					SWITCH GET_CURRENT_GAMEMODE()
						CASE GAMEMODE_FM
						#IF IS_DEBUG_BUILD
						CASE GAMEMODE_CREATOR
						#ENDIF
							IF g_b_SkipKillStrip = FALSE
														
								// Always apply our death effect
								TRIGGER_KILL_STRIP_DEATH_EFFECT_STAGE_1()

								// Player will view kill strip before continuing with respawn
								IF GET_GAME_STATE_ON_DEATH() != AFTERLIFE_JUST_AUTO_REJOIN
								AND GET_GAME_STATE_ON_DEATH() != AFTERLIFE_SET_SPECTATORCAM
									g_SpawnData.bPlayerWantsToRespawn = FALSE
									
									INITIALISE_KILL_STRIP()
									SET_LOCAL_PLAYER_RESPAWN_STATE(RESPAWN_STATE_KILL_STRIP)
									
									g_SpawnData.killStripTimer = GET_NETWORK_TIME()
								ELSE
									NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_PLAYING - no killstrip go to STATE_DEAD") NET_NL()
									SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_WASTED)
								ENDIF
								
								IF MPGlobals.g_KillStrip.killerData.killerID != INVALID_PLAYER_INDEX()
									IF IS_BIT_SET(GlobalPlayerBD[NATIVE_TO_INT(MPGlobals.g_KillStrip.killerData.killerID)].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CANNON)
									AND SHOULD_LOCAL_PLAYER_SPAWN_AT_NEAREST_HOSPITAL()
										NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - SPAWN_LOCATION_NEAREST_HOSPITAL - player killed by Orbital Cannon") NET_NL()
										SET_PLAYER_NEXT_RESPAWN_LOCATION(SPAWN_LOCATION_NEAREST_HOSPITAL)
									ENDIF
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
					
					// have i died during a heist? 
//					IF IS_PLAYER_CRIMINAL(PLAYER_ID())
//						IF IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
//							IF IS_MP_MISSION_OF_TYPE(GET_MP_MISSION_PLAYER_IS_ON(PLAYER_ID()), eTYPE_HEIST)
//								INCREMENT_PLAYER_SKILL_FLOAT(PLAYER_SKILL_RELIABILITY,REL_DEC_DIED_ON_HEIST,TRUE,"RELSKLD7")
//							ENDIF
//						ENDIF
//					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					//...occasionally print out why we are waiting
					IF (GET_GAME_TIMER() % 2500) < 75
						PRINTLN("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_PLAYING - IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD) is TRUE")
					ENDIF
				#ENDIF
				ENDIF
			ENDIF
			
			// url:bugstar:5340490 - [PUBLIC] [EXPLOIT] Respawn in current location - joining sessions through system UI and cancelling alerts.
			IF (g_SpawnData.MissionSpawnDetails.bDoForcedRespawn)
				#IF IS_DEBUG_BUILD
					NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - RESPAWN_STATE_PLAYING, bDoForcedRespawn=TRUE - going to RESPAWN_STATE_JOINING") NET_NL()
				#ENDIF
				FINISH_FORCED_RESPAWN()
				GOTO_JOINING_RESPAWN_STATE()	
			ENDIF
			
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("RESPAWN_STATE_PLAYING")
			#ENDIF
			#ENDIF			
			
		BREAK
		
		DEFAULT
			NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - unknown respawn state! ") NET_PRINT_INT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState) NET_NL()
			SCRIPT_ASSERT("UPDATE_PLAYER_RESPAWN_STATE - unknown respawn state") 
		BREAK

	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
	IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState != RESPAWN_STATE_PLAYING)
		NET_PRINT("[spawning] UPDATE_PLAYER_RESPAWN_STATE - g_SpawnData.RespawnStateTimer = ")
		NET_PRINT_INT(ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_SpawnData.RespawnStateTimer)))
		NET_NL()
	ENDIF
	#ENDIF
	
	// keep respot active until player moves away.
	UPDATE_RACE_RESPOT_AT_RESPAWN(g_b_On_Race)	
	
	// update delete vehicle
	UPDATE_DELETE_VEHICLE_FROM_RESPAWN()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_DELETE_VEHICLE_FROM_RESPAWN")
	#ENDIF
	#ENDIF

	UPDATE_SPAWN_CAM()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_SPAWN_CAM")
	#ENDIF
	#ENDIF
	
	UPDATE_RESET_DIED_IN_MISSION_SPAWN_AREA_COUNT()
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("UPDATE_RESET_DIED_IN_MISSION_SPAWN_AREA_COUNT")
	#ENDIF
	#ENDIF	
	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE 
	CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
	#ENDIF	
	#ENDIF

//	#IF IS_DEBUG_BUILD
//	IF (g_SpawnData.bShowAdvancedSpew)
//	NET_PRINT("[spawning] debug - ")
//	IS_PED_IN_ANY_VEHICLE_OR_WAITING_TO_START_TASK_ENTER_VEHICLE(PLAYER_PED_ID())
//	ENDIF
//	#ENDIF


ENDPROC

PROC MC_UPDATE_PLAYER_RESPAWN_STATE()
	
	IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("fm_mission_creator")) = 0
		EXIT
	ENDIF
	
	UPDATE_PLAYER_RESPAWN_STATE()
	
ENDPROC

// **************************************************************************************************************************
// **************************************************************************************************************************
//								SERVER FUNCTIONS - called from main script
// **************************************************************************************************************************
// **************************************************************************************************************************

PROC CLEANUP_ANY_OFF_MISSION_EXCLUSION_AREAS()
	INT i
	REPEAT MAX_NUM_MISSIONS_ALLOWED i
		IF (GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.bIsActive)
			IF (GET_NUM_PLAYERS_RUNNING_MP_MISSION_INSTANCE(GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].Mission, GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].iMissionInstance) = 0)
				GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.bIsActive = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL SET_OFF_MISSION_SPAWN_OCCLUSION_AREA(MP_MISSION thisMission, INT iThisInstance, VECTOR vCoord1, VECTOR vCoord2, FLOAT fFloat, INT iShape,  BOOL bSet)
	
	INT i

	// first cleanup the current list
	CLEANUP_ANY_OFF_MISSION_EXCLUSION_AREAS()	

	// see if this mission is allready listed?
	REPEAT MAX_NUM_MISSIONS_ALLOWED i
		IF (GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.bIsActive)
			IF (GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].Mission = thisMission)
			AND (GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].iMissionInstance = iThisInstance)
				IF (bSet)
					GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.vCoords1 = vCoord1
					GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.vCoords2 = vCoord2
					GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.fFloat = fFloat
					GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.iShape = iShape
					RETURN(TRUE)
				ELSE
					GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.bIsActive = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// find the first free slot and add it
	IF (bSet)
		REPEAT MAX_NUM_MISSIONS_ALLOWED i
			IF NOT (GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.bIsActive)
				GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.vCoords1 = vCoord1
				GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.vCoords2 = vCoord2	
				GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.fFloat = fFloat
				GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.iShape = iShape							
				GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.bIsActive = TRUE
				RETURN(TRUE)
			ENDIF
		ENDREPEAT	
	ENDIF
	
	RETURN(FALSE)

ENDFUNC


FUNC BOOL IS_ANY_OTHER_PLAYER_USING_VEHICLE(VEHICLE_INDEX CarID)
	RETURN IsAnyOtherPlayerUsingVehicle(CarID)
ENDFUNC

FUNC BOOL IS_ANY_OTHER_PLAYER_SPAWNING_OR_WARPING_NEAR_COORDS(VECTOR vCoords, FLOAT fRadius)
	RETURN IsAnotherPlayerCurrentlyWarpingToPosition(vCoords, fRadius, PLAYER_ID())
ENDFUNC


FUNC BOOL IsYachtVehicleSpawnBlockedByAnotherVehicle(INT iVehicle)
	VECTOR vCoords
	FLOAT fHeading

	// if this vehicle has been created and not taken then it must be still there
	CPRINTLN(DEBUG_YACHT, "IsYachtVehicleSpawnBlockedByAnotherVehicle - flag value for vehicle ", iVehicle, " is ", gPrivateYachtSpawnVehicleDetails[iVehicle].iFlags)
	IF (IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_CREATED) OR IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_DONT_SPAWN))
		IF NOT IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_CREATED_ON_LAND)
		AND NOT IS_PRIVATE_YACHT_VEHICLE_FLAG_SET(iVehicle, PYVF_HAS_TAKEN)
			CPRINTLN(DEBUG_YACHT, "IsYachtVehicleSpawnBlockedByAnotherVehicle - returning FALSE, flags.")
			RETURN(FALSE)
		ENDIF
	ENDIF
	
	GET_PRIVATE_YACHT_SPAWN_VEHICLE_LOCATION(iVehicle, g_SpawnData.iYachtToWarpFrom, vCoords, fHeading, FALSE)		
	vCoords.z += (gPrivateYachtSpawnVehicleDetails[iVehicle].fClearRadius * 0.5)			
	IF IS_ANY_VEHICLE_NEAR_POINT(vCoords, gPrivateYachtSpawnVehicleDetails[iVehicle].fClearRadius )
		// another vehicle is here
		RETURN(TRUE)
	ENDIF
	
	RETURN(FALSE)

ENDFUNC

PROC CheckYachtVehicleSpawnLocations()
	INT iVehicle
	REPEAT NUMBER_OF_PRIVATE_YACHT_VEHICLES iVehicle
		IF IsYachtVehicleSpawnBlockedByAnotherVehicle(iVehicle)
			g_SpawnData.bYachtVehicleSpawnBlocked[iVehicle] = TRUE
			CPRINTLN(DEBUG_YACHT, "CheckYachtVehicleSpawnLocations - vehicle ", iVehicle, " is blocked by another vehicle")	
		ELSE
			g_SpawnData.bYachtVehicleSpawnBlocked[iVehicle] = FALSE	
			CPRINTLN(DEBUG_YACHT, "CheckYachtVehicleSpawnLocations - vehicle ", iVehicle, " is NOT blocked by another vehicle")
		ENDIF
	ENDREPEAT
ENDPROC

PROC DoLastVehicleYachtWarp()
	
	VECTOR vCoords
	VECTOR vStoredCoords
	
	IF DOES_ENTITY_EXIST(g_SpawnData.YachtWarpVehicleID)
	AND NOT IS_ENTITY_DEAD(g_SpawnData.YachtWarpVehicleID)
		IF HAS_YACHT_MODEL_LOADED()
			IF IS_VEHICLE_EMPTY(g_SpawnData.YachtWarpVehicleID, TRUE)
				
				vCoords = GET_ENTITY_COORDS(g_SpawnData.YachtWarpVehicleID)
				vStoredCoords = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(g_SpawnData.iYachtToWarpFrom, g_SpawnData.vOffsetCoordsOfYachtWarpVehicle)
				
				CPRINTLN(DEBUG_YACHT, "DoLastVehicleYachtWarp - vCoords = ", vCoords, ", vStoredCoords = ", vStoredCoords)	
				
				IF (VDIST(vCoords, vStoredCoords) < 1.0)				
					IF NETWORK_HAS_CONTROL_OF_ENTITY(g_SpawnData.YachtWarpVehicleID)											
					
						SET_ENTITY_VISIBLE_IN_CUTSCENE(g_SpawnData.YachtWarpVehicleID, TRUE, TRUE)
					
						IF IS_PRIVATE_YACHT_ACTIVE(g_SpawnData.iYachtToWarpTo)
							
							FLOAT fModelHeight = GET_MODEL_HEIGHT(GET_ENTITY_MODEL(g_SpawnData.YachtWarpVehicleID))
							
							vCoords = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(g_SpawnData.iYachtToWarpTo, g_SpawnData.vOffsetCoordsOfYachtWarpVehicle) 
							vCoords.z += (fModelHeight*0.5)
							
							SET_ENTITY_COORDS(g_SpawnData.YachtWarpVehicleID, vCoords)
							SET_ENTITY_HEADING(g_SpawnData.YachtWarpVehicleID, GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING(g_SpawnData.iYachtToWarpTo, g_SpawnData.fOffsetHeadingOfYachtWarpVehicle))
							FREEZE_ENTITY_POSITION(g_SpawnData.YachtWarpVehicleID, FALSE)
							
							// url:bugstar:2615687 - As we are only saving heading and no other angles, if the parked vehicle 
							// is not perpendicular to the yacht (which might happen in case of a boat) it might end up floating in
							// the air. By forcing physics update on that entity (applying small impulse is a way to do this)
							// we can ensure its "correct" placement
							APPLY_FORCE_TO_ENTITY(g_SpawnData.YachtWarpVehicleID, APPLY_TYPE_IMPULSE, <<0.0, 0.0, 0.001>>, <<0.0, 0.0, 0.0>>, 0, TRUE, TRUE, FALSE)
							
							CPRINTLN(DEBUG_YACHT, "DoLastVehicleYachtWarp - moved vehicle to yacht ", g_SpawnData.iYachtToWarpTo)	
							g_SpawnData.YachtWarpVehicleID = NULL
						ELSE
							FREEZE_ENTITY_POSITION(g_SpawnData.YachtWarpVehicleID, TRUE)
							CPRINTLN(DEBUG_YACHT, "DoLastVehicleYachtWarp - yacht not active ", g_SpawnData.iYachtToWarpTo)	
						ENDIF
					ELSE
						CPRINTLN(DEBUG_YACHT, "DoLastVehicleYachtWarp - requesting control of last vehicle.")	
						NETWORK_REQUEST_CONTROL_OF_ENTITY(g_SpawnData.YachtWarpVehicleID)	
					ENDIF
				ELSE
					CPRINTLN(DEBUG_YACHT, "DoLastVehicleYachtWarp - last vehicle has moved position since stored. vCoords = ", vCoords, ", vStoredCoords = ", vStoredCoords)	
					g_SpawnData.YachtWarpVehicleID = NULL
				ENDIF
			ELSE
				CPRINTLN(DEBUG_YACHT, "DoLastVehicleYachtWarp - last vehicle is not empty.")	
			ENDIF
		ELSE
			CPRINTLN(DEBUG_YACHT, "DoLastVehicleYachtWarp - HAS_YACHT_MODEL_LOADED = FALSE.")
		ENDIF
	ENDIF
	
ENDPROC

PROC UpdateStoreLastVehicleOnYacht()
	// store last vehicle (if it is on yacht)
	g_SpawnData.YachtWarpVehicleID = GET_PLAYERS_LAST_VEHICLE()
	IF DOES_ENTITY_EXIST(g_SpawnData.YachtWarpVehicleID)
	AND NOT IS_ENTITY_DEAD(g_SpawnData.YachtWarpVehicleID)
		IF IS_VEHICLE_ON_YACHT(g_SpawnData.YachtWarpVehicleID, g_SpawnData.iYachtToWarpFrom, 5.0)	
		
			// if it is an plane or heli, check it has landed.
			IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(g_SpawnData.YachtWarpVehicleID))
			OR IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(g_SpawnData.YachtWarpVehicleID))
				IF NOT IS_VEHICLE_ON_ALL_WHEELS(g_SpawnData.YachtWarpVehicleID)
					g_SpawnData.YachtWarpVehicleID = NULL
					CPRINTLN(DEBUG_YACHT, "UpdateStoreLastVehicleOnYacht - last vehicle is plane or heli and not landed.")		
					EXIT
				ENDIF
			ENDIF
			
			// if this is an existing yacht vehicle of this yacht then dont store it as it will be recreated.
			IF DECOR_IS_REGISTERED_AS_TYPE("PYV_Yacht", DECOR_TYPE_INT)
				IF DECOR_EXIST_ON(g_SpawnData.YachtWarpVehicleID, "PYV_Yacht")
					IF (DECOR_GET_INT(g_SpawnData.YachtWarpVehicleID, "PYV_Yacht") = g_SpawnData.iYachtToWarpFrom)
						g_SpawnData.YachtWarpVehicleID = NULL
						CPRINTLN(DEBUG_YACHT, "UpdateStoreLastVehicleOnYacht - last vehicle is existing yacht vehicle.")		
						EXIT
					ENDIF
				ENDIF
			ENDIF

			IF IS_VEHICLE_ON_YACHT(g_SpawnData.YachtWarpVehicleID, g_SpawnData.iYachtToWarpFrom, 0)	
				IF NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(g_SpawnData.YachtWarpVehicleID))
				AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(g_SpawnData.YachtWarpVehicleID))
					g_SpawnData.YachtWarpVehicleID = NULL
					CPRINTLN(DEBUG_YACHT, "UpdateStoreLastVehicleOnYacht - last vehicle isn't a valid on yacht vehicle. Exit. ")		
					EXIT
				ENDIF
			ENDIF
		
			VECTOR vNewCoords = GET_COORDS_AS_OFFSET_FROM_YACHT_GIVEN_WORLD_COORDS(g_SpawnData.iYachtToWarpFrom, GET_ENTITY_COORDS(g_SpawnData.YachtWarpVehicleID))
			VECTOR vDiff = g_SpawnData.vOffsetCoordsOfYachtWarpVehicle - vNewCoords
			
			IF (VMAG(vDiff) > 0.0)
		
				g_SpawnData.vOffsetCoordsOfYachtWarpVehicle = GET_COORDS_AS_OFFSET_FROM_YACHT_GIVEN_WORLD_COORDS(g_SpawnData.iYachtToWarpFrom, GET_ENTITY_COORDS(g_SpawnData.YachtWarpVehicleID))
				g_SpawnData.fOffsetHeadingOfYachtWarpVehicle = GET_HEADING_AS_OFFSET_FROM_YACHT_GIVEN_WORLD_HEADING(g_SpawnData.iYachtToWarpFrom, GET_ENTITY_HEADING(g_SpawnData.YachtWarpVehicleID))				
		
				CPRINTLN(DEBUG_YACHT, "UpdateStoreLastVehicleOnYacht - last vehicle on yacht. storing offset coords ", g_SpawnData.vOffsetCoordsOfYachtWarpVehicle)	
				
			ENDIF
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(g_SpawnData.YachtWarpVehicleID)
			
				// add decor marking this vehicle as a warp vehicle.
				IF DECOR_IS_REGISTERED_AS_TYPE("PYV_WarpFrom", DECOR_TYPE_INT)					
					IF DECOR_EXIST_ON(g_SpawnData.YachtWarpVehicleID, "PYV_WarpFrom")
					AND (DECOR_GET_INT(g_SpawnData.YachtWarpVehicleID, "PYV_WarpFrom") = g_SpawnData.iYachtToWarpFrom)
						CPRINTLN(DEBUG_YACHT, "UpdateStoreLastVehicleOnYacht - vehicle already got warp decor set to ", g_SpawnData.iYachtToWarpFrom)	
					ELSE
						DECOR_SET_INT(g_SpawnData.YachtWarpVehicleID, "PYV_WarpFrom", g_SpawnData.iYachtToWarpFrom)
						CPRINTLN(DEBUG_YACHT, "UpdateStoreLastVehicleOnYacht - setting vehicle warp decor to ", g_SpawnData.iYachtToWarpFrom)	
					ENDIF
				ENDIF
			
				CPRINTLN(DEBUG_YACHT, "UpdateStoreLastVehicleOnYacht - got control of last vehicle.")
			ELSE
				CPRINTLN(DEBUG_YACHT, "UpdateStoreLastVehicleOnYacht - requesting control of last vehicle.")	
				NETWORK_REQUEST_CONTROL_OF_ENTITY(g_SpawnData.YachtWarpVehicleID)
			ENDIF
			
			
		ELSE
			g_SpawnData.YachtWarpVehicleID = NULL	
			CPRINTLN(DEBUG_YACHT, "UpdateStoreLastVehicleOnYacht - last vehicle NOT on yacht.")	
		ENDIF
	ENDIF	
ENDPROC

PROC UPDATE_PRIVATE_YACHT_WARPING(PRIVATE_YACHT_LOCAL_DATA &LocalData, INT &iScene, SceneYachtFunc customYachtCutscene)

	INT iPlayer = NATIVE_TO_INT(PLAYER_ID())
	INT iDiff
	INT iYachtID
	PLAYER_INDEX OwnerID
	STRING AnimDictName = "ANIM@MP_YACHT@YACHT_TRANS@"
	STRING outboundAnimName = "OUTBOUND_CAM", inboundAnimName = "INBOUND_CAM"
	STRING outboundSoundName = "Leave_R_L", inboundSoundName = "Arrive_R_L"
	VECTOR outboundSceneOffset = <<-0.030, 57.720, -0.860>>
	VECTOR inboundSceneOffset = outboundSceneOffset
	VECTOR vDesiredCoords
	
	CONST_INT iWarp_0	0		// wait for a warp to start
	CONST_INT iWarp_1	1		// load ourbound audio scene
	CONST_INT iWarp_2	2		// play establishing shot
	CONST_INT iWarp_3	3		// play outbound animated camera
	CONST_INT iWarp_4	4		// 
	CONST_INT iWarp_5	5		// 
	CONST_INT iWarp_6	6		// switch off player control and do fade
	CONST_INT iWarp_7	7		// wait for screen to fade out
	CONST_INT iWarp_8	8		// wait for server to assign new location
	CONST_INT iWarp_9	9		// do warp
	CONST_INT iWarp_10	10		// fade out for warp to beach	
	CONST_INT iWarp_96	96		// play inbound animated camera
	CONST_INT iWarp_97	97		// 
	CONST_INT iWarp_98	98		// 
	CONST_INT iWarp_99	99		// restore gameplay / cleanup
	CONST_INT iWarp_100	100		// make sure owner has finished.
	
	// wait for a warp to start
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_0)	
		IF IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(PLAYER_ID())				
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - I have started a yacht warp. old pos = ", g_SpawnData.iYachtToWarpFrom)
			iYachtID = g_SpawnData.iYachtToWarpFrom
			g_SpawnData.bHasAccessToYachtWarp = TRUE
			CheckYachtVehicleSpawnLocations()
			Private_Get_NET_YACHT_SCENE(iYachtID, LocalData.sMoveScene
				#IF IS_DEBUG_BUILD
				, &GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS
				, &GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING
				#ENDIF
				)
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_1
			g_SpawnData.bInEmptyYachtSpace = FALSE
			LocalData.bDoneWarpHelpCheck = FALSE
		ELIF IS_LOCAL_PLAYER_ON_ANY_YACHT(TRUE)		
			// store the owner id, only if it is valid.
			iYachtID = GET_YACHT_LOCAL_PLAYER_IS_ON(TRUE)
			OwnerID = GET_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID) 
			
			// if owner id is invalid then check last owner id.
			IF (OwnerID = INVALID_PLAYER_INDEX())			
				OwnerID = GET_LAST_PLAYER_ASSIGNED_TO_PRIVATE_YACHT(iYachtID) 
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - owner invalid, using last owner ", NATIVE_TO_INT(OwnerID))
			ENDIF
			
			IF NOT (OwnerID = INVALID_PLAYER_INDEX())
			AND NOT (g_SpawnData.WarpOwnerID = OwnerID)
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - I am on yacht ", iYachtID, " belonging to = ", NATIVE_TO_INT(OwnerID))
				g_SpawnData.WarpOwnerID = OwnerID
			ENDIF
			IF NOT (g_SpawnData.WarpOwnerID = INVALID_PLAYER_INDEX())
				IF (g_SpawnData.bInEmptyYachtSpace)
					// they were in the space where a new yacht is about to appear, warp them out the way.
					CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - I am in yacht space. ", iYachtID, " belonging to = ", NATIVE_TO_INT(OwnerID))
				ELSE
					IF IS_NET_PLAYER_OK(g_SpawnData.WarpOwnerID, FALSE, FALSE)
						IF IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(g_SpawnData.WarpOwnerID)
							CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - yacht owner has started yacht warp, owner = ", NATIVE_TO_INT(g_SpawnData.WarpOwnerID))
							
							g_SpawnData.iYachtToWarpFrom =  iYachtID
							
							g_SpawnData.bHasAccessToYachtWarp = DOES_LOCAL_PLAYER_HAVE_ACCESS_TO_YACHT(iYachtID)
							
							CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - bHasAccessToYachtWarp = ", g_SpawnData.bHasAccessToYachtWarp, ", owner = ", NATIVE_TO_INT(g_SpawnData.WarpOwnerID))
							
							Private_Get_NET_YACHT_SCENE(iYachtID, LocalData.sMoveScene
								#IF IS_DEBUG_BUILD
								, &GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS
								, &GET_OFFSET_HEADING_FROM_YACHT_AS_WORLD_HEADING
								#ENDIF
								)
							GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_1														
						ENDIF
					ENDIF	
				ENDIF
			ELSE
				g_SpawnData.bInEmptyYachtSpace = TRUE
			ENDIF
		ELIF IS_LOCAL_PLAYER_IN_ANY_YACHT_SPACE()
			g_SpawnData.bInEmptyYachtSpace = TRUE			
		ELSE
			g_SpawnData.bInEmptyYachtSpace = FALSE
		ENDIF	
	ENDIF
	
	// load ourbound audio scene
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_1)
	
		UpdateStoreLastVehicleOnYacht()
		
		IF NOT DOES_ANIM_DICT_EXIST(AnimDictName)
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - bypass establishing shot, owner = ", NATIVE_TO_INT(g_SpawnData.WarpOwnerID))
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_5
		ELSE
			REQUEST_ANIM_DICT(AnimDictName)
			CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - waiting for outbound stream \"", outboundSoundName, "\"")
			IF LOAD_STREAM(outboundSoundName,"DLC_Apartment_Yacht_Streams_Soundset")
			AND REQUEST_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_Yacht_01")
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - finished loading stream ", outboundSoundName, ", owner = ", NATIVE_TO_INT(g_SpawnData.WarpOwnerID))
				LocalData.iCutStage = 0
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_2
			ENDIF
		ENDIF
	ENDIF
	
	// play establishing shot
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_2)
	
		UpdateStoreLastVehicleOnYacht()
		
		REQUEST_ANIM_DICT(AnimDictName)
		
		INT iPrevCutStage = LocalData.iCutStage
		IF CALL customYachtCutscene(LocalData.iCutStage, LocalData.sMoveScene, LocalData.hCam0, LocalData.hCam1, FALSE)
			IF HAS_ANIM_DICT_LOADED(AnimDictName)
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - finished playing establishing shot, owner = ", NATIVE_TO_INT(g_SpawnData.WarpOwnerID))
				IF (g_SpawnData.bHasAccessToYachtWarp)
					GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_3
				ELSE
					g_SpawnData.iYachtToWarpTo = g_SpawnData.iYachtToWarpFrom
					GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_10
					NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - dont have access, so will resapwn on beacht. owener = ", NATIVE_TO_INT(g_SpawnData.WarpOwnerID))
				ENDIF
			ENDIF
		ENDIF
		
		IF iPrevCutStage = 0
			iYachtID = g_SpawnData.iYachtToWarpFrom
			PLAY_STREAM_FROM_POSITION(GET_COORDS_OF_PRIVATE_YACHT(iYachtID))
			START_AUDIO_SCENE("DLC_APT_Yacht_Leave_Scene")
			PLAY_SOUND_FROM_COORD(-1, "Leave_Horn", GET_COORDS_OF_PRIVATE_YACHT(iYachtID), "DLC_Apartment_Yacht_Streams_Soundset")
		ENDIF
	ENDIF
	
	// play outbound animated camera
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_3)
		
		UpdateStoreLastVehicleOnYacht()
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpFrom)
		
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		START_MP_CUTSCENE(TRUE)
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
		iYachtID = g_SpawnData.iYachtToWarpFrom
		VECTOR vOutboundSceneCoord = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, outboundSceneOffset)
		iScene = CREATE_SYNCHRONIZED_SCENE(
				vOutboundSceneCoord,
				<<0,0,GET_HEADING_OF_PRIVATE_YACHT(iYachtID)>>)
		SET_SYNCHRONIZED_SCENE_PHASE(iScene, 0.0)
		LocalData.hCam0 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		PLAY_SYNCHRONIZED_CAM_ANIM(LocalData.hCam0, iScene, outboundAnimName, AnimDictName)
		
		CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - start outbound synch scene ", iScene, " camera \"", outboundAnimName, "\", \"", AnimDictName, "\", ", vOutboundSceneCoord)
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_4
	ENDIF
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_4)
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpFrom)
	
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
			IF GET_SYNCHRONIZED_SCENE_PHASE(iScene) <= 0.85
				//
				#IF IS_DEBUG_BUILD
				IF g_bEditSynchSceneOffsets
					iYachtID = g_SpawnData.iYachtToWarpFrom
					SET_SYNCHRONIZED_SCENE_PHASE(iScene, g_vSynchScenePhase)
					SET_SYNCHRONIZED_SCENE_ORIGIN(iScene,
							GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, outboundSceneOffset+g_vSynchSceneCoordOffset),
							<<0,0,GET_HEADING_OF_PRIVATE_YACHT(iYachtID)>>+g_vSynchSceneRotOffset)
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, outboundSceneOffset+g_vSynchSceneCoordOffset), 0.125)
				ENDIF
				#ENDIF
			ELSE
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_5
			ENDIF
		ELSE
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_5
		ENDIF
	ENDIF
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_5)
	
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpFrom)
		
		DO_SCREEN_FADE_OUT(1000)
		CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - finished synch scene ", iScene, " camera, fade out")
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_6
	ENDIF
	
	// switch off player control and do fade
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_6)
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpFrom)
	
		g_SpawnData.YachtWarpTimer = GET_NETWORK_TIME_ACCURATE()
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		IF NOT IS_SCREEN_FADED_OUT()
		AND NOT IS_SCREEN_FADING_OUT()
			DO_SCREEN_FADE_OUT(500)
		ENDIF
		g_SpawnData.iYachtToWarpTo = g_SpawnData.iYachtToWarpFrom // so it has a valid number set
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_7
	ENDIF

	// wait for screen to fade out
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_7)
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpFrom)
		
		IF IS_SCREEN_FADED_OUT()		
		
		
			g_SpawnData.YachtWarpTimer = GET_NETWORK_TIME_ACCURATE()
			
			IF IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(PLAYER_ID())				
				vDesiredCoords = GET_COORDS_FOR_YACHT_SPAWN_ZONE(INT_TO_ENUM(PRIVATE_YACHT_SPAWN_ZONE_ENUM, g_SpawnData.iYachtZoneToWarpTo))
				YACHT_APPEARANCE Appearance
				GET_APPEARANCE_OF_YACHT(g_SpawnData.iYachtToWarpFrom, Appearance)
				BROADCAST_REASSIGN_MY_PRIVATE_YACHT_NEAR_COORDS(vDesiredCoords, Appearance)
				SET_LOCAL_PLAYER_BD_DESIRED_YACHT_LOCATION(vDesiredCoords)
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
				DETACH_SYNCHRONIZED_SCENE(iScene)
			ENDIF
			iScene = -1
			
			IF DOES_CAM_EXIST(LocalData.hCam0)
				DESTROY_CAM(LocalData.hCam0)
			ENDIF
			IF DOES_CAM_EXIST(LocalData.hCam1)
				DESTROY_CAM(LocalData.hCam1)
			ENDIF
			
			// make sure we are clear of any tasks
			STOP_STREAM()
			STOP_AUDIO_SCENE("DLC_APT_Yacht_Leave_Scene")
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			
			IF NETWORK_IS_IN_MP_CUTSCENE()
				NETWORK_SET_IN_MP_CUTSCENE(FALSE)
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - not longer setting as network cutscene, so yacht vehicles can get created..")
			ENDIF
			
			LOAD_STREAM(inboundSoundName,"DLC_Apartment_Yacht_Streams_Soundset")			
			
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_8
		ELSE
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(500)
			ELSE
				CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - doing fade out.")
			ENDIF		
		ENDIF
	ENDIF
	
	// wait for server to assign new location
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_8)
		
		//UpdateStoreLastVehicleOnYacht()	
	
		IF IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(PLAYER_ID())
			// has server assigned a new yacht position?
			IF (g_SpawnData.iYachtToWarpFrom != LOCAL_PLAYER_PRIVATE_YACHT_ID())				
				g_SpawnData.iYachtToWarpTo = LOCAL_PLAYER_PRIVATE_YACHT_ID()
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - server has reassigned yacht. to position ", g_SpawnData.iYachtToWarpTo)	
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_9
			ELSE
				iDiff = GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), g_SpawnData.YachtWarpTimer)
				IF (iDiff > 30000)
					CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - server timed out. use existing position")	
					GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_9
				ELSE
					CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - waiting for server to assign new location, time = ", iDiff)	
				ENDIF
			ENDIF
		ELSE
			// wait until owner has passed this stage.
			iDiff = GET_TIME_DIFFERENCE(GET_NETWORK_TIME_ACCURATE(), g_SpawnData.YachtWarpTimer)
			
			IF IS_NET_PLAYER_OK(g_SpawnData.WarpOwnerID, FALSE, FALSE)
			AND (iDiff < 30000)
			
				IF ((GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(g_SpawnData.WarpOwnerID) != g_SpawnData.iYachtToWarpFrom) 
				AND NOT (GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(g_SpawnData.WarpOwnerID) = -1))
				OR (iDiff > 20000)		
					
					IF (g_SpawnData.bHasAccessToYachtWarp)
						IF NOT (GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(g_SpawnData.WarpOwnerID) = -1)
							g_SpawnData.iYachtToWarpTo = GET_PRIVATE_YACHT_ASSIGNED_TO_PLAYER(g_SpawnData.WarpOwnerID)
							CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - owner has reassigned yacht. new position ", g_SpawnData.iYachtToWarpTo)	
						ELSE
							g_SpawnData.iYachtToWarpTo = g_SpawnData.iYachtToWarpFrom
							CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - fallback using warp from. ", g_SpawnData.iYachtToWarpTo)
						ENDIF
					ELSE
						g_SpawnData.iYachtToWarpTo = g_SpawnData.iYachtToWarpFrom
						CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - no access, keeping same warpto. ", g_SpawnData.iYachtToWarpTo)
					ENDIF
					
					GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_9
				ELSE
					CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - waiting for owner to get past this stage.")	
				ENDIF
			ELSE
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - owner is no longer ok, bail")	
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_96
			ENDIF
		ENDIF
	ENDIF
	
	// do warp
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_9)
	
		DoLastVehicleYachtWarp()
	
		IF (GlobalServerBD_BlockC.PYYachtDetails[g_SpawnData.iYachtToWarpTo].bYachtIsActive)
			IF IS_PRIVATE_YACHT_ACTIVE(g_SpawnData.iYachtToWarpTo)
				IF IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(PLAYER_ID())
					//IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_PRIVATE_YACHT_APARTMENT, FALSE)
					IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_PRIVATE_FRIEND_YACHT, FALSE)		
					
						// make sure vehicles at old location are deleted
						BROADCAST_DELETE_YACHT_VEHICLES_NOW(g_SpawnData.iYachtToWarpFrom)
					
						GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_96
					ELSE
						CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - waiting to warp to private yacht apartment ", g_SpawnData.iYachtToWarpTo)		
					ENDIF
				ELSE
					IF (g_SpawnData.bHasAccessToYachtWarp)
						IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_PRIVATE_FRIEND_YACHT, FALSE)
							GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_96		
						ELSE
							CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - waiting to warp to friend yacht ", g_SpawnData.iYachtToWarpTo)		
						ENDIF
					ELSE
						IF (g_SpawnData.bInEmptyYachtSpace)
							IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_PRIVATE_YACHT_NEAR_SHORE, FALSE)
							
								IF NOT IS_SCREEN_FADED_IN()
								AND NOT IS_SCREEN_FADING_IN()
									DO_SCREEN_FADE_IN(1000)
								ENDIF						
							
								GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_98		
							ELSE
								CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - waiting to warping to shore ", g_SpawnData.iYachtToWarpTo)		
							ENDIF
						ELSE
							IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_PRIVATE_YACHT_NEAR_SHORE, FALSE)
							
								IF NOT IS_SCREEN_FADED_IN()
								AND NOT IS_SCREEN_FADING_IN()
									DO_SCREEN_FADE_IN(1000)
								ENDIF						
							
								GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_98		
							ELSE
								CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - waiting to warping to shore ", g_SpawnData.iYachtToWarpTo)		
							ENDIF
						ENDIF
					ENDIF
				ENDIF											
			ELSE
				CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - waiting for ipls to load for yacht ", g_SpawnData.iYachtToWarpTo)		
			ENDIF
		ELSE
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - server says this yacht is no longer active, bailing. ")	
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_96
		ENDIF
	ENDIF
	// wait for scereen to fade out
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_10)
		IF IS_SCREEN_FADED_OUT()
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - screen is faded out, do warp. ")	
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_9	
		ELSE
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(500)
			ELSE
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - screen is fadeing out... ")
			ENDIF
		ENDIF
	ENDIF
	
	// play inbound animated camera
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_96)
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpTo)
	
		IF NOT DOES_ANIM_DICT_EXIST(AnimDictName)
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - bypass synch scene camera")
			
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(1000)
			ENDIF
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_98
		ELSE
			IF NOT LOAD_STREAM(inboundSoundName,"DLC_Apartment_Yacht_Streams_Soundset")
			OR (MPGlobalsPrivateYacht.bUseYachtObjectsOptimisation AND NOT IS_PRIVATE_YACHT_FULLY_LOADED(g_SpawnData.iYachtToWarpTo))
				CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - waiting for inbound stream \"", inboundSoundName, "\" and yacht to load")
			ELSE
				iYachtID = g_SpawnData.iYachtToWarpTo
				VECTOR vInboundSceneCoord = GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, inboundSceneOffset)
				iScene = CREATE_SYNCHRONIZED_SCENE(
						vInboundSceneCoord, <<0,0,GET_HEADING_OF_PRIVATE_YACHT(iYachtID)>>)
				SET_SYNCHRONIZED_SCENE_PHASE(iScene, 0.0)
				LocalData.hCam0 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				PLAY_SYNCHRONIZED_CAM_ANIM(LocalData.hCam0, iScene, inboundAnimName, AnimDictName)
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - start inbound synch scene ", iScene, " camera \"", inboundAnimName, "\", \"", AnimDictName, "\", ", vInboundSceneCoord)
				
				
				PLAY_STREAM_FROM_POSITION(GET_COORDS_OF_PRIVATE_YACHT(iYachtID))
				START_AUDIO_SCENE("DLC_APT_Yacht_Arrive_Scene")
				PLAY_SOUND_FROM_COORD(-1, "Arrive_Horn", GET_COORDS_OF_PRIVATE_YACHT(iYachtID), "DLC_Apartment_Yacht_Streams_Soundset")
				
				IF NOT IS_SCREEN_FADED_IN()
				AND NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_IN(1000)
				ENDIF
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_97
			ENDIF
		ENDIF
	ENDIF
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_97)
	
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpTo)
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iScene)
			IF GET_SYNCHRONIZED_SCENE_PHASE(iScene) <= 0.95
				//
				#IF IS_DEBUG_BUILD
				IF g_bEditSynchSceneOffsets
					iYachtID = g_SpawnData.iYachtToWarpTo
					SET_SYNCHRONIZED_SCENE_PHASE(iScene, g_vSynchScenePhase)
					SET_SYNCHRONIZED_SCENE_ORIGIN(iScene,
							GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, inboundSceneOffset+g_vSynchSceneCoordOffset),
							<<0,0,GET_HEADING_OF_PRIVATE_YACHT(iYachtID)>>+g_vSynchSceneRotOffset)
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_YACHT_IN_WORLD_COORDS(iYachtID, inboundSceneOffset+g_vSynchSceneCoordOffset), 0.125)
				ENDIF
				#ENDIF
				
				IF DOES_CAM_EXIST(LocalData.hCam0)
					IF IS_CAM_RENDERING(LocalData.hCam0)
						CDEBUG3LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - hCam0 rendering")
					ELSE
						CERRORLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - hCam0 not rendering")
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - hCam0 doesn't exist")
				ENDIF
				
				IF IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(PLAYER_ID())
					IF NOT (LocalData.bDoneWarpHelpCheck)
						IF IS_SCREEN_FADED_IN()
							IF (GET_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_DOCK_HELP_MESSAGES) < 3)
								PRINT_HELP("PY_DOCK2")
								INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_YACHT_DOCK_HELP_MESSAGES)
							ENDIF
							LocalData.bDoneWarpHelpCheck = TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ELSE
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - finished inbound synch scene ", iScene, " - phase > 0.95")
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_98
			ENDIF
		ELSE
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - finished inbound synch scene ", iScene, " - not running")
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_98
		ENDIF
	ENDIF
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_98)
		
		HIDE_ALL_DOCKED_YACHT_VEHICLES_LOCALLY(g_SpawnData.iYachtToWarpTo)
		
		CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - finished synch scene ", iScene, " camera, clean up")
		IF DOES_CAM_EXIST(LocalData.hCam0)
			DESTROY_CAM(LocalData.hCam0)
		ENDIF
		IF DOES_CAM_EXIST(LocalData.hCam1)
			DESTROY_CAM(LocalData.hCam1)
		ENDIF
		IF DOES_ANIM_DICT_EXIST(AnimDictName)
			REMOVE_ANIM_DICT(AnimDictName)
		ENDIF
		
		STOP_STREAM()
		STOP_AUDIO_SCENE("DLC_APT_Yacht_Arrive_Scene")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_APARTMENT/APT_Yacht_01")
		
		CLEANUP_MP_CUTSCENE()
	
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(),TRUE)
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		
		CLEANUP_MP_CUTSCENE()
		
		
		
		GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_99
	ENDIF
	
	// restore gameplay / cleanup
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_99)
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		IF IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(PLAYER_ID())
		
			// did we manage to get a docking place in our desired location? 2656865
			INT iDesiredYachtID = GET_MP_INT_CHARACTER_STAT(MP_STAT_YACHTPREFERREDAREA)
			iDesiredYachtID += -1 // the stat default is zero, so we use this as NULL and add 1 when storing
			IF NOT (GET_ZONE_YACHT_IS_IN(iDesiredYachtID) = GET_ZONE_YACHT_IS_IN(g_SpawnData.iYachtToWarpTo))
				PRINT_HELP("PY_DOCK1")
			ENDIF
		
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_0
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - finished cleanup. ")
		ELSE
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_100	
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - going to wait for owner cleanup confirmation. ")
		ENDIF
		
		GlobalplayerBD[iPlayer].PrivateYachtDetails.bWarpActive = FALSE
	ENDIF
	
	// make sure owner has finished.
	IF (GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_100)
		IF (g_SpawnData.bHasAccessToYachtWarp)
			IF IS_NET_PLAYER_OK(g_SpawnData.WarpOwnerID, FALSE, FALSE)
				IF IS_PLAYER_WARPING_THEIR_OWN_PRIVATE_YACHT(g_SpawnData.WarpOwnerID) = FALSE
					CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - owner has cleaned up. ")
					GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_0
				ENDIF
			ELSE
				CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - owner is not ok. ")
				GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_0
			ENDIF
		ELSE	
			CPRINTLN(DEBUG_YACHT, "UPDATE_PRIVATE_YACHT_WARPING - finished warpint to shore ")
			GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState = iWarp_0
		ENDIF
	ENDIF
	
	IF GlobalplayerBD[iPlayer].PrivateYachtDetails.iWarpState > iWarp_0
		DISABLE_DPADDOWN_THIS_FRAME()
		
		DISPLAY_AMMO_THIS_FRAME(FALSE)			
		HUD_FORCE_WEAPON_WHEEL(FALSE)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
		SET_CLEAR_ON_CALL_HUD_THIS_FRAME()

		DISABLE_SELECTOR_THIS_FRAME()
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
		
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
		
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		SET_CLEAR_ON_CALL_HUD_THIS_FRAME(TRUE)
		
		SET_IDLE_KICK_DISABLED_THIS_FRAME()
	ENDIF
ENDPROC



#IF IS_DEBUG_BUILD
PROC UPDATE_SPAWNING_WIDGETS()
	
	INT i, j
	VECTOR vCentre
	TEXT_LABEL_63 str
	INT iActiveMissionSpawn
	INT iActiveMissionOcclusion
	INT iActiveOffMissionOcclusion
	INT iRows
	FLOAT fDrawX, fDrawY
	FLOAT fAngle
	VECTOR vec
	INT r,g,b,a
	VECTOR vPlayerCoords
	FLOAT fPlayerHeading
	VECTOR vForward
	VECTOR vCornerPoint[4]	
	VECTOR vNearCornerPoint[4]	
	//FLOAT zDiff
	VECTOR vCross
	FLOAT fDist
	VEHICLE_INDEX VehicleID
	
	IF (g_SpawnData.bGetSpecificCoordsFromPlayerPos)
		g_SpecificSpawnLocation.vCoords = GET_PLAYER_COORDS(PLAYER_ID())
		g_SpecificSpawnLocation.fHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
		g_SpawnData.bGetSpecificCoordsFromPlayerPos = FALSE
	ENDIF
	
	
	// call the command SET_PLAYER_RESPAWN_IN_VEHICLE
	IF (g_SpawnData.bUpdate_SET_PLAYER_RESPAWN_IN_VEHICLE)	
		SET_PLAYER_RESPAWN_IN_VEHICLE(g_SpawnData.bRespawnInVehicle, 
										INT_TO_ENUM(MODEL_NAMES, g_SpawnData.iSpawnVehicleModel),  
										g_SpawnData.bUseLastCarIfDriveable, 
										g_SpawnData.bDontUseVehicleNodes, 
										g_SpawnData.bUsePersonalVehicleIfPossible,
										g_SpawnData.bSetVehicleStrong,
										g_SpawnData.iColour,
										g_SpawnData.bSetTyresBulletProof,
										g_SpawnData.bSetAsNonSaveable, 
										g_SpawnData.bSetAsNonModable,
										g_SpawnData.bSetFireAfterGangBossMissionEnds																		
										)	
										
		g_SpawnData.bUpdate_SET_PLAYER_RESPAWN_IN_VEHICLE = FALSE	
	ENDIF
	
	IF (g_SpawnData.iSelectedSpawnLocation != g_SpawnData.iSpawnLocation)
		SET_CONTENTS_OF_TEXT_WIDGET(g_SpawnData.SpawnTextWidgetID, GET_SPAWN_LOCATION_DEBUG_NAME(INT_TO_ENUM(PLAYER_SPAWN_LOCATION, g_SpawnData.iSpawnLocation)))	
		g_SpawnData.iSelectedSpawnLocation = g_SpawnData.iSpawnLocation
	ENDIF
	
	IF (g_SpawnData.bTestWarp)
		IF WARP_TO_SPAWN_LOCATION(INT_TO_ENUM(PLAYER_SPAWN_LOCATION, g_SpawnData.iSpawnLocation), g_SpawnData.bLeavePedBehind, g_SpawnData.bWarpIntoCar, g_SpawnData.bDoVisibilityChecksOnTeammates, g_SpawnData.bDoQuickWarp)
			g_SpawnData.bTestWarp = FALSE	
		ENDIF
	ENDIF
	
	IF NOT (g_SpawnData.iNextSpawnLocation = 0)
		g_SpawnData.NextSpawn.Location = INT_TO_ENUM(PLAYER_SPAWN_LOCATION, g_SpawnData.iNextSpawnLocation)
		g_SpawnData.NextSpawn.Thread_ID = GET_ID_OF_THIS_THREAD()
	ENDIF
	
	REPEAT MAX_NUMBER_OF_MISSION_SPAWN_AREAS i	
		IF (g_SpawnData.MissionSpawnDetails.SpawnArea[i].bShow)
		OR (g_SpawnData.MissionSpawnDetails.bToggleSpawnView)
			IF (g_SpawnData.MissionSpawnDetails.SpawnArea[i].bIsActive)
			
				IF NOT (g_SpawnData.bDebugLinesActive)
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
					g_SpawnData.bDebugLinesActive = TRUE
				ENDIF
				str = "SPAWNING AREA " 
				str += i
				
				DRAW_DEBUG_SPAWN_AREA(str, g_SpawnData.MissionSpawnDetails.SpawnArea[i], 0, 255, 0, g_SpawnData.iAlphaValue)
								
				iActiveMissionSpawn += 1
			ENDIF
		ENDIF
	ENDREPEAT
	
//	REPEAT TOTAL_NUMBER_OF_INTERIOR_BLEEDS i	
//		IF (ProblemArea_InteriorBleed[i].bShow)
//			
//			IF NOT (g_SpawnData.bDebugLinesActive)
//				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
//				g_SpawnData.bDebugLinesActive = TRUE
//			ENDIF			
//		
//			str = "PROBLEM AREA - Interior Bleed " 
//			str += i
//			
//			DRAW_DEBUG_PROBLEM_AREA(str, ProblemArea_InteriorBleed[i], 255, 0, 0, g_SpawnData.iAlphaValue)
//							
//		ENDIF
//	ENDREPEAT
//	IF (g_SpawnData.bShowAllInteriorBleeds)
//		REPEAT TOTAL_NUMBER_OF_INTERIOR_BLEEDS i	
//			ProblemArea_InteriorBleed[i].bShow = TRUE
//		ENDREPEAT
//		IF NOT (g_SpawnData.bDebugLinesActive)
//			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
//			g_SpawnData.bDebugLinesActive = TRUE
//		ENDIF
//		g_SpawnData.bShowAllInteriorBleeds = FALSE
//	ENDIF

	REPEAT NUMBER_OF_SPAWNING_SECTORS i	
		REPEAT g_iTotalForbiddenProblemAreasForSector[i] j
			IF (ProblemArea_Forbidden[i][j].bShow)
				
				IF NOT (g_SpawnData.bDebugLinesActive)
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
					g_SpawnData.bDebugLinesActive = TRUE
				ENDIF			
			
				str = "ProblemArea_Forbidden[" 
				str += i
				str += "]["
				str += j
				str += "]"
				
				DRAW_DEBUG_PROBLEM_AREA(str, ProblemArea_Forbidden[i][j], 255, 128, 20, g_SpawnData.iAlphaValue)
								
			ENDIF
		ENDREPEAT
	ENDREPEAT
	IF (g_SpawnData.bShowAllForbiddenAreas)
		REPEAT NUMBER_OF_SPAWNING_SECTORS i	
			REPEAT g_iTotalForbiddenProblemAreasForSector[i] j	
				ProblemArea_Forbidden[i][j].bShow = TRUE
			ENDREPEAT
		ENDREPEAT
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF
		g_SpawnData.bShowAllForbiddenAreas = FALSE
	ENDIF	
	IF (g_SpawnData.bShowAllSpawningSectors)

		DRAW_DEBUG_BOX_FOR_RELEASE(<<LOWEST_INT, g_fSpawningSectorYLine[0], 0.0>>, <<HIGHEST_INT, HIGHEST_INT, 0.0>> ,30, 30, 240, 50)
		DRAW_DEBUG_BOX_FOR_RELEASE(<<LOWEST_INT, g_fSpawningSectorYLine[1], 0.0>>, <<g_fSpawningSectorXLine[0], g_fSpawningSectorYLine[0], 0.0>> ,60, 60, 210, 50)
		DRAW_DEBUG_BOX_FOR_RELEASE(<<g_fSpawningSectorXLine[0], g_fSpawningSectorYLine[1], 0.0>>, <<HIGHEST_INT, g_fSpawningSectorYLine[0], 0.0>> ,90, 90, 180, 50)
		DRAW_DEBUG_BOX_FOR_RELEASE(<<LOWEST_INT, g_fSpawningSectorYLine[2], 0.0>>, <<g_fSpawningSectorXLine[1], g_fSpawningSectorYLine[1], 0.0>> ,120, 120, 150, 50)
		DRAW_DEBUG_BOX_FOR_RELEASE(<<g_fSpawningSectorXLine[1], g_fSpawningSectorYLine[2], 0.0>>, <<g_fSpawningSectorXLine[2], g_fSpawningSectorYLine[1], 0.0>> ,150, 150, 120, 50)
		DRAW_DEBUG_BOX_FOR_RELEASE(<<g_fSpawningSectorXLine[2], g_fSpawningSectorYLine[2], 0.0>>, <<g_fSpawningSectorXLine[3], g_fSpawningSectorYLine[1], 0.0>> ,180, 180, 90, 50)
		DRAW_DEBUG_BOX_FOR_RELEASE(<<g_fSpawningSectorXLine[3], g_fSpawningSectorYLine[2], 0.0>>, <<HIGHEST_INT, g_fSpawningSectorYLine[1], 0.0>> ,210, 210, 60, 50)
		DRAW_DEBUG_BOX_FOR_RELEASE(<<LOWEST_INT, LOWEST_INT, 0.0>>, <<HIGHEST_INT, g_fSpawningSectorYLine[2], 0.0>> ,240, 240, 30, 50)
		
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF
	ENDIF
	IF (g_SpawnData.bDrawVectorMap)
		DRAW_VECTOR_MAP_LOCAL_PLAYER_CAM()
		DRAW_VECTOR_MAP_PED(PLAYER_PED_ID())
	ENDIF
	
	IF (g_SpawnData.bTest_NET_WARP_TO_COORD)
		IF NET_WARP_TO_COORD(g_SpecificSpawnLocation.vCoords, 
							g_SpecificSpawnLocation.fHeading, 
							g_SpawnData.b_NET_WARP_TO_COORD_keepvehicle,
							g_SpawnData.b_NET_WARP_TO_COORD_LeavePedBehind,
							g_SpawnData.b_NET_WARP_TO_COORD_DontSetCameraBehindPlayer,
							g_SpawnData.b_NET_WARP_TO_COORD_KillLeftBehindPed,
							g_SpawnData.b_NET_WARP_TO_COORD_KeepPortableObjects,
							g_SpawnData.b_NET_WARP_TO_COORD_DoQuickWarp,
							g_SpawnData.b_NET_WARP_TO_COORD_SnapToGround,
							g_SpawnData.b_NET_WARP_TO_COORD_IgnoreAlreadyThereCheck)		
			g_SpawnData.bTest_NET_WARP_TO_COORD = FALSE
		ENDIF
	ENDIF
		
	REPEAT NUMBER_OF_SPAWNING_SECTORS i	
		REPEAT g_iTotalNodesProblemAreasForSector[i] j
			IF (ProblemArea_Nodes[i][j].bShow)
				
				IF NOT (g_SpawnData.bDebugLinesActive)
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
					g_SpawnData.bDebugLinesActive = TRUE
				ENDIF			
			
				str = "ProblemArea_Nodes[" 
				str += i
				str += "]["
				str += j
				str += "]"
				
				DRAW_DEBUG_PROBLEM_AREA(str, ProblemArea_Nodes[i][j], 255, 0, 20, g_SpawnData.iAlphaValue)
								
			ENDIF
		ENDREPEAT
	ENDREPEAT
	IF (g_SpawnData.bShowAllProblemNodeAreas)
		REPEAT NUMBER_OF_SPAWNING_SECTORS i	
			REPEAT g_iTotalNodesProblemAreasForSector[i] j	
				ProblemArea_Nodes[i][j].bShow = TRUE
			ENDREPEAT
		ENDREPEAT
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF
		g_SpawnData.bShowAllProblemNodeAreas = FALSE
	ENDIF		
	
	REPEAT NUMBER_OF_PROBLEM_NODESEARCH_AREAS i	
		IF (ProblemArea_NodeSearch[i].bShow)
			
			IF NOT (g_SpawnData.bDebugLinesActive)
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
				g_SpawnData.bDebugLinesActive = TRUE
			ENDIF			
		
			str = "PROBLEM AREA - NodesSearch " 
			str += i
			
			DRAW_DEBUG_PROBLEM_AREA(str, ProblemArea_NodeSearch[i], 0, 200, 200, g_SpawnData.iAlphaValue)
							
		ENDIF
	ENDREPEAT
	
	REPEAT NUMBER_OF_MIN_AIRPLANE_HEIGHT_AREAS i	
		IF (ProblemArea_MinAirplaneHeight[i].bShow)
			
			IF NOT (g_SpawnData.bDebugLinesActive)
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
				g_SpawnData.bDebugLinesActive = TRUE
			ENDIF			
		
			str = "PROBLEM AREA - Min Airplane Height " 
			str += i
			
			DRAW_DEBUG_PROBLEM_AREA(str, ProblemArea_MinAirplaneHeight[i], 20, 64, 189, g_SpawnData.iAlphaValue)
							
		ENDIF
	ENDREPEAT
	
	REPEAT NUMBER_OF_CUSTOM_VEHICLE_NODE_AREAS i	
		IF (ProblemArea_CustomVehicleNodes[i].bShow)
			
			IF NOT (g_SpawnData.bDebugLinesActive)
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
				g_SpawnData.bDebugLinesActive = TRUE
			ENDIF			
		
			str = "PROBLEM AREA - Custom vehicle nodes " 
			str += i
			
			DRAW_DEBUG_PROBLEM_AREA(str, ProblemArea_CustomVehicleNodes[i], 180, 222, 76, g_SpawnData.iAlphaValue)
							
		ENDIF
	ENDREPEAT
	
	IF (g_SpawnData.bShowAllProblemMinAirplaneHeight)
		REPEAT NUMBER_OF_MIN_AIRPLANE_HEIGHT_AREAS i	
			ProblemArea_MinAirplaneHeight[i].bShow = TRUE
		ENDREPEAT
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF
		g_SpawnData.bShowAllProblemMinAirplaneHeight = FALSE
	ENDIF	
	
	
	IF (g_SpawnData.MissionSpawnDetails.bToggleSpawnView)
		IF (g_SpawnData.MissionSpawnDetails.bFacePoint)
			DRAW_DEBUG_CROSS (g_SpawnData.MissionSpawnDetails.vFacing, 1.5, 0, 255, 0, 255)
			DRAW_DEBUG_TEXT ("SPAWN FACING THIS POINT", g_SpawnData.MissionSpawnDetails.vFacing, 0, 255, 0, 255)
		ENDIF
		IF (g_SpawnData.MissionSpawnDetails.bFaceAwayFromPoint)
			DRAW_DEBUG_CROSS (g_SpawnData.MissionSpawnDetails.vFacing, 1.5, 255,0,0, 255)
			DRAW_DEBUG_TEXT ("SPAWN FACING AWAY FROM THIS POINT", g_SpawnData.MissionSpawnDetails.vFacing, 255,0,0, 255)
		ENDIF
	ENDIF
	
	IF (g_SpawnData.MissionSpawnDetails.bSetFacePointOnPlayer)
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			g_SpawnData.MissionSpawnDetails.vFacing = GET_PLAYER_COORDS(PLAYER_ID())
		ENDIF
		g_SpawnData.MissionSpawnDetails.bSetFacePointOnPlayer = FALSE
	ENDIF
	
	IF (g_SpawnData.MissionSpawnDetails.bToggleSpawnView)
	
		DRAW_DEBUG_CROSS (g_SpawnData.vMyDeadCoords, 1.5, 255, 106, 0, 255)
		DRAW_DEBUG_TEXT ("SPAWNING - Dead Coords", g_SpawnData.vMyDeadCoords, 255, 106, 0, 255)
	
		DRAW_DEBUG_SPAWN_POINT(g_SpawnData.PrivateParams.vSearchCoord, g_SpawnData.PrivateParams.fRawHeading, HUD_COLOUR_WHITE, 0.25)
		DRAW_DEBUG_TEXT ("SPAWNING - Raw Coords", g_SpawnData.PrivateParams.vSearchCoord, 255, 255, 255, 255)
		
		DRAW_DEBUG_SPAWN_POINT(g_SpawnData.vSpawnCoords, g_SpawnData.fSpawnHeading, HUD_COLOUR_YELLOW, 0.5)
		DRAW_DEBUG_TEXT ("SPAWNING - Spawn Coords", <<g_SpawnData.vSpawnCoords.x, g_SpawnData.vSpawnCoords.y, g_SpawnData.vSpawnCoords.z + 1.0 >>, 0, 255, 255, 255)
		
		
	ENDIF
	
	MODEL_NAMES tempModel 
	IF (g_SpawnData.bCallSetCustomPlayerModel_Franklin)		
		tempModel = INT_TO_ENUM(MODEL_NAMES, HASH("p_franklin_02"))
		REQUEST_MODEL(tempModel)
		IF HAS_MODEL_LOADED(tempModel)
			SET_CUSTOM_PLAYER_MODEL(tempModel, TRUE, TRUE)
			g_SpawnData.bCallSetCustomPlayerModel_Franklin = FALSE
		ENDIF
	ENDIF
	
	IF (g_SpawnData.bCallSetCustomPlayerModel_Lamar)		
		tempModel = INT_TO_ENUM(MODEL_NAMES, HASH("IG_LamarDavis_02"))
		REQUEST_MODEL(tempModel)
		IF HAS_MODEL_LOADED(tempModel)
			SET_CUSTOM_PLAYER_MODEL(tempModel, TRUE, TRUE)
			g_SpawnData.bCallSetCustomPlayerModel_Lamar = FALSE
		ENDIF
	ENDIF
	
	IF (g_SpawnData.bCallClearCustomPlayerModel)
		CLEAR_CUSTOM_PLAYER_MODEL()
		g_SpawnData.bCallClearCustomPlayerModel = FALSE
	ENDIF
	
	IF (g_SpawnData.bCallFinalizeHeadBlend)
		FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
		g_SpawnData.bCallFinalizeHeadBlend = FALSE
	ENDIF
	
	IF (g_SpawnData.bCallNetSpawnPlayer)
		NET_SPAWN_PLAYER(GET_PLAYER_COORDS(PLAYER_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()), SPAWN_REASON_MANUAL, SPAWN_LOCATION_AT_SPECIFIC_COORDS)
		g_SpawnData.bCallNetSpawnPlayer = FALSE
	ENDIF	
	
	IF ( g_SpawnData.bRenderSpecificCoords)
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF	
		IF NOT ( g_SpecificSpawnLocation.bUseAngledArea)
			DRAW_DEBUG_SPAWN_POINT(g_SpecificSpawnLocation.vCoords, g_SpecificSpawnLocation.fHeading, HUD_COLOUR_BLUE, 0.25)
			DRAW_DEBUG_TEXT ("SPAWNING - Specific Coords", g_SpecificSpawnLocation.vCoords, 255, 255, 255, 255)
			
			REPEAT FMMC_MAX_NUM_RACERS i
				DRAW_DEBUG_SPAWN_POINT(g_SpawnData.MissionSpawnDetails.vSecondaryRaceRespawn[i], g_SpecificSpawnLocation.fHeading, HUD_COLOUR_BLUELIGHT, 0.25)	
				str = "vSecondaryRaceRespawn["
				str += i
				str += "]"
				DRAW_DEBUG_TEXT (str, g_SpawnData.MissionSpawnDetails.vSecondaryRaceRespawn[i], 255, 255, 255, 255)
			ENDREPEAT
			
		ELSE
			DRAW_DEBUG_ANGLED_AREA(g_SpecificSpawnLocation.vAngledCoords1, g_SpecificSpawnLocation.vAngledCoords2, g_SpecificSpawnLocation.fWidth, 0, 0, 255, g_SpawnData.iAlphaValue)
			DRAW_DEBUG_TEXT ("SPAWNING - Specific Coords", g_SpecificSpawnLocation.vCoords, 255, 255, 255, 255)
		ENDIF
		
	ENDIF
	
	// render all race checkpoints
	IF (g_SpawnData.bRenderAllRaceCheckpoints)
		VECTOR vCamPos
		FLOAT fHeading
		VECTOR vDiff
		
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF		
		
		vCamPos = GET_CAM_COORD(GET_RENDERING_CAM())		
		REPEAT GET_FMMC_MAX_NUM_CHECKPOINTS() i

			IF (i < (GET_FMMC_MAX_NUM_CHECKPOINTS() - 1))
				vDiff = g_FMMC_STRUCT.sPlacedCheckpoint[i+1].vCheckPoint - g_FMMC_STRUCT.sPlacedCheckpoint[i].vCheckPoint
				fHeading = GET_HEADING_FROM_VECTOR_2D( vDiff.x, vDiff.y)
			ENDIF 

		
			IF (VDIST(vCamPos, g_FMMC_STRUCT.sPlacedCheckpoint[i].vCheckPoint) < g_SpawnData.iRenderAllRaceCheckpointsDrawDistance)
			AND VMAG(g_FMMC_STRUCT.sPlacedCheckpoint[i].vCheckPoint) > 0.0			
				DRAW_DEBUG_SPAWN_POINT(g_FMMC_STRUCT.sPlacedCheckpoint[i].vCheckPoint, fHeading, HUD_COLOUR_GREEN, 0.25)	
				str = "g_FMMC_STRUCT.sPlacedCheckpoint["
				str += i
				str += "].vCheckpoint"
				DRAW_DEBUG_TEXT (str, g_FMMC_STRUCT.sPlacedCheckpoint[i].vCheckPoint, 0, 255, 0, 255)
			ENDIF
			
			
			REPEAT FMMC_MAX_NUM_CHECKPOINT_RESPAWN j
				IF (VDIST(vCamPos, g_FMMC_STRUCT.sPlacedCheckpoint[i].sCheckpointRespawn.vRespawnPos[j]) < g_SpawnData.iRenderAllRaceCheckpointsDrawDistance)
				AND VMAG(g_FMMC_STRUCT.sPlacedCheckpoint[i].sCheckpointRespawn.vRespawnPos[j]) > 0.0																
					DRAW_DEBUG_SPAWN_POINT(g_FMMC_STRUCT.sPlacedCheckpoint[i].sCheckpointRespawn.vRespawnPos[j], fHeading, HUD_COLOUR_YELLOW, 0.25)	
					str = "g_FMMC_STRUCT.sCheckpointRespawn["
					str += i
					str += "].vRespawnPos["
					str += j
					str += "]"
					str += "(ckp "
					str += i
					str += ")"
					DRAW_DEBUG_TEXT (str, g_FMMC_STRUCT.sPlacedCheckpoint[i].sCheckpointRespawn.vRespawnPos[j], 255, 255, 0, 255)	
				ENDIF
			ENDREPEAT
				

		ENDREPEAT
	
	ENDIF
	
	IF (g_SpawnData.bGenerateRandomCustomSpawnPoints_Circle)
	
		IF NOT (g_SpawnData.bUseCustomSpawnPoints)
			USE_CUSTOM_SPAWN_POINTS(TRUE, DEFAULT, g_SpawnData.bCustomSpawnPointsIgnoreDistanceCheck)
		ENDIF
		
		vCentre = GET_PLAYER_COORDS(PLAYER_ID())
		fAngle = 360.0 / TO_FLOAT(g_SpawnData.iCustomSpawnPointsToGenerate)
		
		REPEAT g_SpawnData.iCustomSpawnPointsToGenerate i
			IF (g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints < MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS)
					
				vec.x = (SIN((fAngle * TO_FLOAT(i)))) * g_SpawnData.fGenerateRandomCustomSpawnPointsDist
				vec.y = (COS((fAngle * TO_FLOAT(i)))) * g_SpawnData.fGenerateRandomCustomSpawnPointsDist
				vec.z = 100.0	
				
				vec += vCentre
			
				IF NOT GET_GROUND_Z_FOR_3D_COORD(vec, vec.z)
					vec.z += -100.0
				ENDIF
			
				ADD_CUSTOM_SPAWN_POINT(vec, (360.0 + ((fAngle * TO_FLOAT(i)) * -1.0))  )
		
			ENDIF
		ENDREPEAT
		
		g_SpawnData.bGenerateRandomCustomSpawnPoints_Circle = FALSE
	ENDIF
	
	IF (g_SpawnData.bGenerateRandomCustomSpawnPoints_Cluster)
	
		IF NOT (g_SpawnData.bUseCustomSpawnPoints)
			USE_CUSTOM_SPAWN_POINTS(TRUE, DEFAULT, g_SpawnData.bCustomSpawnPointsIgnoreDistanceCheck)
		ENDIF
		
		vCentre = GET_PLAYER_COORDS(PLAYER_ID())
		fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
		
		REPEAT g_SpawnData.iCustomSpawnPointsToGenerate i
		
			IF (g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints < MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS)
					
				IF (i = 0)
					vec =  vCentre	
				ELSE
					fAngle = (TO_FLOAT(i) * 90.0)										
					SWITCH i
						CASE 1
						CASE 2
						CASE 3
						CASE 4
							fAngle += 22.5
							fDist = g_SpawnData.fGenerateRandomCustomSpawnPointsDist * 1.0
						BREAK
						CASE 5
						CASE 6
						CASE 7
						CASE 8
							fAngle += 67.5
							fDist = g_SpawnData.fGenerateRandomCustomSpawnPointsDist * (1.4142 * 1.0)
						BREAK
						CASE 9
						CASE 10
						CASE 11
						CASE 12
							fAngle += 22.5
							fDist = g_SpawnData.fGenerateRandomCustomSpawnPointsDist * 2.0
						BREAK
						CASE 13
						CASE 14
						CASE 15
						CASE 16
							fAngle += 67.5
							fDist = g_SpawnData.fGenerateRandomCustomSpawnPointsDist * (1.4142 * 2.0)
						BREAK
						CASE 17
						CASE 18
						CASE 19
						CASE 20
							fAngle += 22.5
							fDist = g_SpawnData.fGenerateRandomCustomSpawnPointsDist * 3.0
						BREAK
						CASE 21
						CASE 22
						CASE 23
						CASE 24
							fAngle += 67.5
							fDist = g_SpawnData.fGenerateRandomCustomSpawnPointsDist * (1.4142 * 3.0)
						BREAK
						CASE 25
						CASE 26
						CASE 27
						CASE 28
							fAngle += 22.5
							fDist = g_SpawnData.fGenerateRandomCustomSpawnPointsDist * 4.0
						BREAK
						CASE 29
						CASE 30
						CASE 31
						CASE 32
							fAngle += 67.5
							fDist = g_SpawnData.fGenerateRandomCustomSpawnPointsDist * (1.4142 * 4.0)
						BREAK

					ENDSWITCH
					
					vec.x = (SIN((fAngle ))) * fDist
					vec.y = (COS((fAngle ))) * fDist
					vec.z = 0.0
					
					vec += vCentre
					vec.z += 1.0
					
												
				ENDIF
				
				IF NOT GET_GROUND_Z_FOR_3D_COORD(vec, vec.z)
					vec.z = vCentre.z
				ENDIF
				
				ADD_CUSTOM_SPAWN_POINT(vec, fPlayerHeading  )
				
			ENDIF
		

		ENDREPEAT
		
		g_SpawnData.bGenerateRandomCustomSpawnPoints_Cluster = FALSE
	ENDIF
	
	// This is for a realtime preview of heading
	IF g_SpawnData.fGenerateRandomCustomSpawnPointsGridHeading != g_SpawnData.fGenerateRandomCustomSpawnPointsGridHeading_Previous
		g_SpawnData.bGenerateRandomCustomSpawnPoints_Grid = TRUE
		g_SpawnData.fGenerateRandomCustomSpawnPointsGridHeading_Previous = g_SpawnData.fGenerateRandomCustomSpawnPointsGridHeading
		USE_CUSTOM_SPAWN_POINTS(FALSE)
	ENDIF
	
	// This will create spawn points on an even grid with player position being bottom center point
	IF (g_SpawnData.bGenerateRandomCustomSpawnPoints_Grid)
		IF NOT (g_SpawnData.bUseCustomSpawnPoints)
			USE_CUSTOM_SPAWN_POINTS(TRUE, DEFAULT, g_SpawnData.bCustomSpawnPointsIgnoreDistanceCheck)
		ENDIF
		
		vCentre = GET_PLAYER_COORDS(PLAYER_ID())
		fPlayerHeading = g_SpawnData.fGenerateRandomCustomSpawnPointsGridHeading //GET_ENTITY_HEADING(PLAYER_PED_ID())
		
		VECTOR vFrwd = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCentre, fPlayerHeading, <<0.0, g_SpawnData.fGenerateRandomCustomSpawnPointsDist, 0.0>>) - vCentre
		VECTOR vSide = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCentre, fPlayerHeading, <<g_SpawnData.fGenerateRandomCustomSpawnPointsDist, 0.0, 0.0>>) - vCentre
		
		INT iGridWidth = g_SpawnData.iGenerateRandomCustomSpawnPointsGridWidth
		INT iGridHeight = CEIL(TO_FLOAT(g_SpawnData.iCustomSpawnPointsToGenerate) / TO_FLOAT(iGridWidth))
		INT x
		INT y
		INT iTotalPoints = 0
		
		VECTOR vPoint
		
		CDEBUG1LN(DEBUG_PROPERTY, "SPAWN GRID POINTS: iGridWidth: ", iGridWidth)
		CDEBUG1LN(DEBUG_PROPERTY, "SPAWN GRID POINTS: iGridHeight: ", iGridHeight)
		
		INT iEvenIndex = 0
		
		// ALIGNMENT: 0 = left, 1 = center, 2 = right
		
		REPEAT iGridHeight y
			iEvenIndex = 0
			REPEAT iGridWidth x
				IF (g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints < MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS)
					IF iTotalPoints < g_SpawnData.iCustomSpawnPointsToGenerate
					
						CDEBUG1LN(DEBUG_PROPERTY, "SPAWN GRID POINTS: Generating point x: ", x, ", y: ", y)
						
						FLOAT fSign = 1.0
						
						// LEFT | RIGHT
						IF g_SpawnData.iGenerateRandomCustomSpawnPointsGridAlignment = 0
						OR g_SpawnData.iGenerateRandomCustomSpawnPointsGridAlignment = 2
						
							IF g_SpawnData.iGenerateRandomCustomSpawnPointsGridAlignment = 2
								fSign = -1.0
							ENDIF
						
							vPoint = vCentre + (TO_FLOAT(x) * fSign * vSide) + (TO_FLOAT(y) * vFrwd)
						
						// CENTER
						ELIF g_SpawnData.iGenerateRandomCustomSpawnPointsGridAlignment = 1
							IF x % 2 = 0
								fSign = -1.0
							ENDIF
							
							// iEvenIndex increases only on even indices, sign alternates between + and -, this way we center points at x and the first point is always at players coords
							vPoint = vCentre + (TO_FLOAT(iEvenIndex) * fSign * vSide) + (TO_FLOAT(y) * vFrwd)
							
							IF x % 2 = 0
								iEvenIndex += 1
							ENDIF
						ENDIF
						
						iTotalPoints = iTotalPoints + 1
						
						IF NOT GET_GROUND_Z_FOR_3D_COORD(vPoint, vPoint.z)
							vPoint.z = vCentre.z
						ENDIF
						
						ADD_CUSTOM_SPAWN_POINT(vPoint, fPlayerHeading)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDREPEAT
		
		g_SpawnData.bGenerateRandomCustomSpawnPoints_Grid = FALSE
	ENDIF
	
	// This will generate a single spawn point where player is standing and with their heading
	IF g_SpawnData.bGenerateSingleCustomSpawnPoint
		INT iPointIndex = g_SpawnData.iSelectedSpawnPointIndex
		IF iPointIndex > -1 AND iPointIndex < MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS
		
			vec = GET_PLAYER_COORDS(PLAYER_ID())
			fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
			
			IF NOT GET_GROUND_Z_FOR_3D_COORD(vec, vec.z)
				vec = GET_PLAYER_COORDS(PLAYER_ID())
			ENDIF
			
			g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[iPointIndex].vPos = vec
			g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[iPointIndex].fHeading = fPlayerHeading
			g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[iPointIndex].fWeighting = 1.0
		
			IF iPointIndex >= g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints
				g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints = iPointIndex + 1
			ENDIF
			
			IF g_SpawnData.bAutoincrementGenerateSinglePointIndex
				g_SpawnData.iSelectedSpawnPointIndex = g_SpawnData.iSelectedSpawnPointIndex + 1
			ENDIF
		ENDIF
		g_SpawnData.bGenerateSingleCustomSpawnPoint = FALSE
	ENDIF
	
	// Allow moving the points about using mouse
	IF g_SpawnData.bAllowPointsMousePicking
		IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)
			VECTOR vMouse2DPos, vPoint2DPos, vXoffset2D, vYOffset2D, vThePoint
			INT iClosestPoint
			FLOAT fCurrentDist, fMinDist, fXMoveSpeed, fYMoveSpeed
		
			GET_MOUSE_POSITION(vMouse2DPos.x, vMouse2DPos.y)
			
			//GET_SCREEN_COORD_FROM_WORLD_COORD(paramVars.vMinMaxCornerMax, vMax2DPos.x, vMax2DPos.y)
				
			IF g_SpawnData.iSelectedSpawnPointIndex = -1
				fMinDist = 99999.9
				iClosestPoint = -1
				// See if we are grabbing any point
				REPEAT g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints i
					vThePoint = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos + <<0.0, 0.0, 1.0>>
					GET_SCREEN_COORD_FROM_WORLD_COORD(vThePoint, vPoint2DPos.x, vPoint2DPos.y)
					fCurrentDist = VDIST(vMouse2DPos, vPoint2DPos)
					//PRINTLN("[CUSTOM_SPAWN_POINT_PICKING] Checking point ", i, " - dist to mouse is ", fCurrentDist)
					IF fCurrentDist < 0.02
					AND fCurrentDist < fMinDist
						fMinDist = fCurrentDist
						iClosestPoint = i
					ENDIF
				ENDREPEAT
				
				IF iClosestPoint > -1
					g_SpawnData.iSelectedSpawnPointIndex = iClosestPoint
				ENDIF
			ELSE
				/*
				ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
				ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
				SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				*/
				
				g_SpawnData.bCurrentlyDraggingSpawnPoint = TRUE
				
				vThePoint = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.iSelectedSpawnPointIndex].vPos + <<0.0, 0.0, 1.0>>
				GET_SCREEN_COORD_FROM_WORLD_COORD(vThePoint, vPoint2DPos.x, vPoint2DPos.y)
				
				VECTOR vXOffset = vThePoint + <<1.0, 0.0, 0.0>>
				VECTOR vYOffset = vThePoint + <<0.0, 1.0, 0.0>>
				GET_SCREEN_COORD_FROM_WORLD_COORD(vXOffset, vXoffset2D.x, vXoffset2D.y)
				GET_SCREEN_COORD_FROM_WORLD_COORD(vYOffset, vYoffset2D.x, vYoffset2D.y)
			
				VECTOR vDesiredXOffset = GET_CLOSEST_POINT_ON_LINE(vMouse2DPos, vPoint2DPos, vXOffset2D, FALSE)
				FLOAT fDistFromDesiredXOffsetToCentre = VDIST(vDesiredXOffset, vPoint2DPos)
				FLOAT fDistFromMaxXToCentre = VDIST(vXOffset2D, vPoint2DPos)
				fXMoveSpeed = fDistFromDesiredXOffsetToCentre / fDistFromMaxXToCentre
				
				IF DOT_PRODUCT(vDesiredXOffset - vPoint2DPos, vXOffset2D - vPoint2DPos) < 0.0
					fXMoveSpeed = -fXMoveSpeed
				ENDIF
				
				VECTOR vDesiredYOffset = GET_CLOSEST_POINT_ON_LINE(vMouse2DPos, vPoint2DPos, vYOffset2D, FALSE)
				FLOAT fDistFromDesiredYOffsetToCentre = VDIST(vDesiredYOffset, vPoint2DPos)
				FLOAT fDistFromMaxYToCentre = VDIST(vYOffset2D, vPoint2DPos)
				fYMoveSpeed = fDistFromDesiredYOffsetToCentre / fDistFromMaxYToCentre
				
				IF DOT_PRODUCT(vDesiredYOffset - vPoint2DPos, vYOffset2D - vPoint2DPos) < 0.0
					fYMoveSpeed = -fYMoveSpeed
				ENDIF

				vThePoint.x += fXMoveSpeed
				vTHePoint.y += fYMoveSpeed
				
				// Place point properly on ground
				IF NOT GET_GROUND_Z_FOR_3D_COORD(vThePoint, vThePoint.z)
					PRINTLN("[CUSTOM_SPAWN_POINT_PICKING] Cant get Z for the point ", vThePoint)
					vThePoint.z = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.iSelectedSpawnPointIndex].vPos.z
				ENDIF
				
				g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.iSelectedSpawnPointIndex].vPos = vThePoint
				
				// Adjust heading if wheel scroll is moved
				IF IS_DEBUG_KEY_PRESSED(KEY_LEFT, KEYBOARD_MODIFIER_NONE, "Rotating spawn point left")
				OR IS_DEBUG_KEY_PRESSED(KEY_LEFT, KEYBOARD_MODIFIER_SHIFT, "Rotating spawn point left slower")
					FLOAT fAmnt = 125.0
					IF IS_DEBUG_KEY_PRESSED(KEY_LEFT, KEYBOARD_MODIFIER_SHIFT, "Rotating spawn point left slower")
						fAmnt = 25.0
					ENDIF
					FLOAT fTmpHeading = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.iSelectedSpawnPointIndex].fHeading + fAmnt * GET_FRAME_TIME()
					IF fTmpHeading > 360.0
						fTmpHeading -= 360.0
					ENDIF
					g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.iSelectedSpawnPointIndex].fHeading = fTmpHeading
				ENDIF
				
				IF IS_DEBUG_KEY_PRESSED(KEY_RIGHT, KEYBOARD_MODIFIER_NONE, "Rotating spawn point right")
				OR IS_DEBUG_KEY_PRESSED(KEY_RIGHT, KEYBOARD_MODIFIER_SHIFT, "Rotating spawn point right slower")
					FLOAT fAmnt = 125.0
					IF IS_DEBUG_KEY_PRESSED(KEY_RIGHT, KEYBOARD_MODIFIER_SHIFT, "Rotating spawn point right slower")
						fAmnt = 25.0
					ENDIF
					FLOAT fTmpHeading = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.iSelectedSpawnPointIndex].fHeading - fAmnt * GET_FRAME_TIME()
					IF fTmpHeading < 0.0
						fTmpHeading += 360.0
					ENDIF
					g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.iSelectedSpawnPointIndex].fHeading = fTmpHeading
				ENDIF
				
				/*
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
				OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
					PRINTLN("[CUSTOM_SPAWN_POINT_PICKING] Scroll wheel is up")
					FLOAT fTmpHeading = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.iSelectedSpawnPointIndex].fHeading + 1.0
					IF fTmpHeading > 360.0
						fTmpHeading -= 360.0
					ENDIF
					g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.iSelectedSpawnPointIndex].fHeading = fTmpHeading
				ENDIF
				
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
					FLOAT fTmpHeading = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.iSelectedSpawnPointIndex].fHeading - 1.0
					IF fTmpHeading < 0.0
						fTmpHeading += 360.0
					ENDIF
					g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.iSelectedSpawnPointIndex].fHeading = fTmpHeading
				ENDIF
				*/
			ENDIF
		ELSE
			IF g_SpawnData.iSelectedSpawnPointIndex > -1
				g_SpawnData.iSelectedSpawnPointIndex = -1
				g_SpawnData.bCurrentlyDraggingSpawnPoint = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	// Swaping spawn points
	IF g_SpawnData.bSwapCustomSpawnPoints
		IF g_SpawnData.iSelectedSpawnPointIndex > -1 AND g_SpawnData.iSelectedSpawnPointIndex < MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS
		AND g_SpawnData.iSpawnPointToSwapWith > -1 AND g_SpawnData.iSpawnPointToSwapWith < MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS
			CUSTOM_SPAWN_POINT tmpSpawnPoint = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.iSelectedSpawnPointIndex]
			g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.iSelectedSpawnPointIndex] = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.iSpawnPointToSwapWith]
			g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.iSpawnPointToSwapWith] = tmpSpawnPoint
		ENDIF
		
		g_SpawnData.bSwapCustomSpawnPoints = FALSE
	ENDIF
	
	// Deleting spawn point
	IF g_SpawnData.bDeleteSelectedSpawnPoint
		IF g_SpawnData.iSelectedSpawnPointIndex > -1 AND g_SpawnData.iSelectedSpawnPointIndex < MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS
			CUSTOM_SPAWN_POINT tmpSpawnPoint
			FOR i = g_SpawnData.iSelectedSpawnPointIndex TO g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints - 1
				g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i] = g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i+1]
			ENDFOR
			g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints-1] = tmpSpawnPoint
			g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints = g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints - 1
		ENDIF
		g_SpawnData.bDeleteSelectedSpawnPoint = FALSE
	ENDIF
	
	//IF (g_SpawnData.bUseCustomSpawnPoints)	
		IF (g_SpawnData.bShowCustomSpawnPoints)		
			IF NOT (g_SpawnData.bDebugLinesActive)
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
				g_SpawnData.bDebugLinesActive = TRUE
			ENDIF
			HUD_COLOURS hudColorArrow = HUD_COLOUR_PINK
			REPEAT g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints i
				IF g_SpawnData.bCurrentlyDraggingSpawnPoint
				AND g_SpawnData.iSelectedSpawnPointIndex = i
					hudColorArrow = HUD_COLOUR_WHITE
				ELSE
					hudColorArrow = HUD_COLOUR_PINK
				ENDIF
				DRAW_DEBUG_SPAWN_POINT(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos, g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].fHeading, hudColorArrow, 1.0)							
				str = "SPAWNING - Custom Point "
				str += i
				IF NOT g_SpawnData.bCurrentlyDraggingSpawnPoint
				AND g_SpawnData.bShowCustomSpawnPointLabels
					GET_HUD_COLOUR(HUD_COLOUR_PINK, r,g,b,a)
					DRAW_DEBUG_TEXT (str, <<g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos.x, g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos.y, g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos.z+1.0>>, r,g,b,a)
				ENDIF
			ENDREPEAT	
		ENDIF
	//ENDIF
	
	IF (g_SpawnData.bClearCustomSpawnPoints)
		USE_CUSTOM_SPAWN_POINTS(FALSE)
		g_SpawnData.bClearCustomSpawnPoints = FALSE
	ENDIF
	
	IF (g_SpawnData.bCustomSpawnPointsOutputCoords)
		REPEAT g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints i	
			NET_PRINT("[spawning] Custom Spawn Point ") 
			NET_PRINT_INT(i)
			NET_PRINT(" ")
			NET_PRINT_VECTOR(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos)
			NET_PRINT(", ") 
			NET_PRINT_FLOAT(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].fHeading)
			NET_NL()
		ENDREPEAT
		g_SpawnData.bCustomSpawnPointsOutputCoords = FALSE
	ENDIF
	
	IF (g_SpawnData.bCustomSpawnPointOutputProperty)
		OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("		// -- -- OUTPUT SPAWN LOCATIONS FOR PROPERTY  -- -- -- //")SAVE_NEWLINE_TO_DEBUG_FILE()
		REPEAT g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints i	
			SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_INT_TO_DEBUG_FILE(i) SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("    mpOffset.vLoc 		= ") SAVE_VECTOR_TO_DEBUG_FILE(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos) SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("    mpOffset.vRot		= ") SAVE_VECTOR_TO_DEBUG_FILE(<<0,0,0>>) SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("    mpOffset.vRot.z 	= ") SAVE_FLOAT_TO_DEBUG_FILE(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].fHeading) SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("BREAK") SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()	
		CLOSE_DEBUG_FILE()
		g_SpawnData.bCustomSpawnPointOutputProperty = FALSE
	ENDIF
	
	IF g_SpawnData.bCustomSpawnPointOutputPostDeliveryScene
		OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("		// -- -- OUTPUT SPAWN LOCATIONS FOR POST SCENE  -- -- -- //")SAVE_NEWLINE_TO_DEBUG_FILE()
		REPEAT g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints i	
			SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_INT_TO_DEBUG_FILE(i) 
			SAVE_STRING_TO_DEBUG_FILE("    vSpawnPoint 	= ") SAVE_VECTOR_TO_DEBUG_FILE(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos) 
			SAVE_STRING_TO_DEBUG_FILE("    fSpawnHeading 	= ") SAVE_FLOAT_TO_DEBUG_FILE(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].fHeading) 
			SAVE_STRING_TO_DEBUG_FILE("    BREAK") SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()	
		CLOSE_DEBUG_FILE()
		g_SpawnData.bCustomSpawnPointOutputPostDeliveryScene = FALSE
	ENDIF
		
	IF (g_SpawnData.bCustomSpawnPointOutputSimpleInterior)		
		OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("		// -- -- OUTPUT SPAWN LOCATIONS: SIMPLE INTERIOR FORMAT -- -- -- //")SAVE_NEWLINE_TO_DEBUG_FILE()
		REPEAT g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints i	
			SAVE_STRING_TO_DEBUG_FILE("CASE ") SAVE_INT_TO_DEBUG_FILE(i) 
			SAVE_STRING_TO_DEBUG_FILE("	vSpawnPoint = ") SAVE_VECTOR_TO_DEBUG_FILE(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos) 
			SAVE_STRING_TO_DEBUG_FILE(" fSpawnHeading = ") SAVE_FLOAT_TO_DEBUG_FILE(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].fHeading) 
			SAVE_STRING_TO_DEBUG_FILE("	RETURN TRUE")
			SAVE_STRING_TO_DEBUG_FILE(" BREAK") SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()	
		CLOSE_DEBUG_FILE()
		g_SpawnData.bCustomSpawnPointOutputSimpleInterior = FALSE
	ENDIF
	
	REPEAT MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS i
		IF (g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i].bShow)
		OR (g_SpawnData.MissionSpawnDetails.bToggleSpawnView)
			IF (g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i].bIsActive)
				IF NOT (g_SpawnData.bDebugLinesActive)
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
					g_SpawnData.bDebugLinesActive = TRUE
				ENDIF
				str = "SPAWNING OCCLUSION AREA " 
				str += i
				
				DRAW_DEBUG_SPAWN_AREA(str, g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i], 255, 0, 0, g_SpawnData.iAlphaValue)								
				
				iActiveMissionOcclusion += 1
			ENDIF
		ENDIF
	ENDREPEAT

	REPEAT MAX_NUM_MISSIONS_ALLOWED i		
		IF (g_SpawnData.MissionSpawnDetails.bToggleSpawnView)
			IF (GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.bIsActive)
				IF NOT (g_SpawnData.bDebugLinesActive)
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
					g_SpawnData.bDebugLinesActive = TRUE
				ENDIF
				str = "OFF MISSION OCCLUSION AREA " 
				str += i
				
				DRAW_DEBUG_SPAWN_AREA(str, GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea, 255, 0, 255, g_SpawnData.iAlphaValue)
												
				iActiveOffMissionOcclusion += 1
			ENDIF
		ENDIF		
	ENDREPEAT
	
	
	REPEAT MAX_NUMBER_OF_GLOBAL_EXCLUSION_AREAS i
		IF (g_SpawnData.MissionSpawnDetails.bToggleSpawnView)
			IF (GlobalExclusionArea[i].ExclusionArea.bIsActive)
				IF NOT (g_SpawnData.bDebugLinesActive)
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
					g_SpawnData.bDebugLinesActive = TRUE
				ENDIF
				str = "GLOBAL OCCLUSION AREA " 
				str += i
				
				DRAW_DEBUG_SPAWN_AREA(str, GlobalExclusionArea[i].ExclusionArea, 255, 128, 64, g_SpawnData.iAlphaValue)
								
			ENDIF		
		ENDIF
	ENDREPEAT
	
	IF (g_SpawnData.LastGetSafeCoordsForCreatingEntity.bShow)
		DRAW_DEBUG_SPAWN_AREA("Last Get Safe Coords Area", g_SpawnData.LastGetSafeCoordsForCreatingEntity, 255, 216, 0, g_SpawnData.iAlphaValue)	
		REPEAT NUMBER_OF_RESULTS_STORED_FOR_GETTING_SAFE_COORDS_DEBUG i 
			DRAW_DEBUG_SPAWN_POINT(g_SpawnData.LastCoordsReturnedForGetSafeCoords[i].Pos, g_SpawnData.LastCoordsReturnedForGetSafeCoords[i].Heading, HUD_COLOUR_YELLOW, 1.0)	
			str = "Get Safe Coord "
			str += i
			DRAW_DEBUG_SPAWN_TEXT(str, g_SpawnData.LastCoordsReturnedForGetSafeCoords[i].Pos, 255, 0, 0, 255)	
		ENDREPEAT		
	ENDIF
	
	IF (g_SpawnData.bShowAllPoliceStation)		
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF		
		REPEAT TOTAL_NUMBER_OF_RESPAWN_POLICE_STATIONS i
			str = "POLICE STATION RESPAWN AREA " 
			str += i
			DRAW_DEBUG_SPAWN_AREA(str, SpawnAreas_PoliceStation[i], 25, 25, 255, g_SpawnData.iAlphaValue)	
		ENDREPEAT	
	ENDIF
	
	IF (g_SpawnData.bShowAllHospital)		
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF		
		REPEAT TOTAL_NUMBER_OF_RESPAWN_HOSPITALS i
			str = "HOSPITAL RESPAWN AREA " 
			str += i
			DRAW_DEBUG_SPAWN_AREA(str, SpawnAreas_Hospitals[i], 255, 25, 255, g_SpawnData.iAlphaValue)	
		ENDREPEAT	
	ENDIF
	
	IF (g_SpawnData.bShowAllHotels)		
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF		
		REPEAT TOTAL_NUMBER_OF_RESPAWN_HOTELS i
			str = "HOTEL RESPAWN AREA " 
			str += i
			DRAW_DEBUG_SPAWN_AREA(str, SpawnAreas_Hotels[i], 255, 25, 25, g_SpawnData.iAlphaValue)	
		ENDREPEAT	
	ENDIF
	
	// toggle the displaying of spawn areas on and off
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_G, KEYBOARD_MODIFIER_SHIFT, "Show spawning areas")
		IF NOT (g_SpawnData.MissionSpawnDetails.bToggleSpawnView)	
			ToggleSpawningAreaVisible(TRUE)
		ELSE
			ToggleSpawningAreaVisible(FALSE)
		ENDIF
	ENDIF
	
	// render some info on screen
	IF (g_SpawnData.MissionSpawnDetails.bToggleSpawnView)
		DRAW_RECT(g_SpawnData.MissionSpawnDetails.SpawnWindowX, g_SpawnData.MissionSpawnDetails.SpawnWindowY, g_SpawnData.MissionSpawnDetails.SpawnWindowW, g_SpawnData.MissionSpawnDetails.SpawnWindowH, 0, 0, 0, 128)
		
		
		IF (g_SpawnData.bUseCustomSpawnPoints)
				
			// draw title
			fDrawX = g_SpawnData.MissionSpawnDetails.SpawnWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
			fDrawY = g_SpawnData.MissionSpawnDetails.SpawnWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
			SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
			GET_HUD_COLOUR(HUD_COLOUR_PINK, r,g,b,a)
			SET_TEXT_COLOUR(r,g,b,a)
			DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Using Custom Spawn Points", HUD_COLOUR_PINK)
			iRows += 1
		
		
			IF NOT (g_SpawnData.CustomSpawnPointInfo.iLastUsedCustomSpawnPoint = -1)
				
				// last spawn point
				fDrawX = g_SpawnData.MissionSpawnDetails.SpawnWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
				fDrawY = g_SpawnData.MissionSpawnDetails.SpawnWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
				SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
				GET_HUD_COLOUR(HUD_COLOUR_PINK, r,g,b,a)
				SET_TEXT_COLOUR(r,g,b,a)
				DISPLAY_TEXT_WITH_NUMBER(fDrawX, fDrawY, "SPN_LAST", g_SpawnData.CustomSpawnPointInfo.iLastUsedCustomSpawnPoint)
				iRows += 1			
				
				// last spawn point method
				fDrawX = g_SpawnData.MissionSpawnDetails.SpawnWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
				fDrawY = g_SpawnData.MissionSpawnDetails.SpawnWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
				SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
				GET_HUD_COLOUR(HUD_COLOUR_PINK, r,g,b,a)
				SET_TEXT_COLOUR(r,g,b,a)
				iRows += 1
				
			ENDIF
		
			// draw title
			fDrawX = g_SpawnData.MissionSpawnDetails.SpawnWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
			fDrawY = g_SpawnData.MissionSpawnDetails.SpawnWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
			SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
			GET_HUD_COLOUR(HUD_COLOUR_PINK, r,g,b,a)
			SET_TEXT_COLOUR(r,g,b,a)
			DISPLAY_TEXT_WITH_NUMBER(fDrawX, fDrawY, "SPN_CSP", g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints)
			iRows += 1			
			
			// draw details
			REPEAT g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints i	

				fDrawX = g_SpawnData.MissionSpawnDetails.SpawnWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
				fDrawY = g_SpawnData.MissionSpawnDetails.SpawnWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
				
				SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
				GET_HUD_COLOUR(HUD_COLOUR_PINK, r,g,b,a)
				SET_TEXT_COLOUR(r,g,b,a)
			
				DISPLAY_TEXT_WITH_VECTOR(fDrawX, fDrawY, "SPN_V", g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos, 1)
				
				SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
				GET_HUD_COLOUR(HUD_COLOUR_PINK, r,g,b,a)
				SET_TEXT_COLOUR(r,g,b,a)

				DISPLAY_TEXT_WITH_FLOAT(fDrawX + g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX, fDrawY, "SPN_HD", g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].fHeading, 1)
			
				iRows += 1
					
			ENDREPEAT			
		
		ELSE
		
			IF (iActiveMissionSpawn = 0)
			AND (iActiveMissionOcclusion = 0)
			AND (iActiveOffMissionOcclusion = 0)
			AND (g_SpawnData.MissionSpawnDetails.bFacePoint = FALSE)
			AND (g_SpawnData.NextSpawn.Location = SPAWN_LOCATION_AUTOMATIC)
				
				fDrawX = g_SpawnData.MissionSpawnDetails.SpawnWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX 
				fDrawY = g_SpawnData.MissionSpawnDetails.SpawnWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY
			
				SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
				SET_TEXT_COLOUR(255,0,0, 255)
				DISPLAY_TEXT(fDrawX, fDrawY, "SPN_NONE")	
			ELSE
				
				IF (iActiveMissionSpawn > 0)
					
					// draw title
					fDrawX = g_SpawnData.MissionSpawnDetails.SpawnWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
					fDrawY = g_SpawnData.MissionSpawnDetails.SpawnWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
					SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
					SET_TEXT_COLOUR(0,255,0, 255)
					DISPLAY_TEXT(fDrawX, fDrawY, "SPN_1")
					iRows += 1
					
					// draw details
					REPEAT MAX_NUMBER_OF_MISSION_SPAWN_AREAS i	
						IF (g_SpawnData.MissionSpawnDetails.SpawnArea[i].bIsActive)

							fDrawX = g_SpawnData.MissionSpawnDetails.SpawnWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
							fDrawY = g_SpawnData.MissionSpawnDetails.SpawnWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
							
							SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
							SET_TEXT_COLOUR(0,255,0, 255)
						
							DISPLAY_TEXT_WITH_VECTOR(fDrawX, fDrawY, "SPN_V", g_SpawnData.MissionSpawnDetails.SpawnArea[i].vCoords1, 1)
							
							SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
							SET_TEXT_COLOUR(0,255,0, 255)
							
							SWITCH g_SpawnData.MissionSpawnDetails.SpawnArea[i].iShape
								CASE SPAWN_AREA_SHAPE_CIRCLE 	
									DISPLAY_TEXT_WITH_FLOAT(fDrawX + g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX, fDrawY, "SPN_R", g_SpawnData.MissionSpawnDetails.SpawnArea[i].fFloat, 1)							
								BREAK
								CASE SPAWN_AREA_SHAPE_BOX		
									DISPLAY_TEXT_WITH_VECTOR(fDrawX + g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX, fDrawY, "SPN_V", g_SpawnData.MissionSpawnDetails.SpawnArea[i].vCoords2, 1)							
								BREAK
								CASE SPAWN_AREA_SHAPE_ANGLED									
									DISPLAY_TEXT_WITH_VECTOR(fDrawX + g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX, fDrawY, "SPN_V", g_SpawnData.MissionSpawnDetails.SpawnArea[i].vCoords2, 1)							
								BREAK
							ENDSWITCH
							
						
							iRows += 1
						ENDIF
					ENDREPEAT
				
				ENDIF
				
				IF (iActiveMissionOcclusion > 0)
					
					// draw title
					fDrawX = g_SpawnData.MissionSpawnDetails.SpawnWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
					fDrawY = g_SpawnData.MissionSpawnDetails.SpawnWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
					SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
					SET_TEXT_COLOUR(255,0,0, 255)
					DISPLAY_TEXT(fDrawX, fDrawY, "SPN_2")
					iRows += 1
					
					// draw details
					REPEAT MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS i	
						IF (g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i].bIsActive)

							fDrawX = g_SpawnData.MissionSpawnDetails.SpawnWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
							fDrawY = g_SpawnData.MissionSpawnDetails.SpawnWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
							
							SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
							SET_TEXT_COLOUR(255,0,0, 255)
						
							DISPLAY_TEXT_WITH_VECTOR(fDrawX, fDrawY, "SPN_V", g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i].vCoords1, 1)
							
							SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
							SET_TEXT_COLOUR(255,0,0, 255)
							
							
							SWITCH g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i].iShape
								CASE SPAWN_AREA_SHAPE_CIRCLE 	
									DISPLAY_TEXT_WITH_FLOAT(fDrawX + g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX, fDrawY, "SPN_R", g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i].fFloat, 1)
								BREAK
								CASE SPAWN_AREA_SHAPE_BOX		
									DISPLAY_TEXT_WITH_VECTOR(fDrawX + g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX, fDrawY, "SPN_V", g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i].vCoords2, 1)
								BREAK
								CASE SPAWN_AREA_SHAPE_ANGLED									
									DISPLAY_TEXT_WITH_VECTOR(fDrawX + g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX, fDrawY, "SPN_V", g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i].vCoords2, 1)								
								BREAK
							ENDSWITCH						
						
							iRows += 1
						ENDIF
					ENDREPEAT
				
				ENDIF		
							
				
				// facing details
				IF (g_SpawnData.MissionSpawnDetails.bFacePoint)
					
					// title
					fDrawX = g_SpawnData.MissionSpawnDetails.SpawnWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
					fDrawY = g_SpawnData.MissionSpawnDetails.SpawnWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)							
					SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
					SET_TEXT_COLOUR(0,255,255, 255)			
					DISPLAY_TEXT(fDrawX, fDrawY, "SPN_4")
					iRows += 1				
					
					// vector
					fDrawX = g_SpawnData.MissionSpawnDetails.SpawnWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
					fDrawY = g_SpawnData.MissionSpawnDetails.SpawnWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)							
					SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
					IF (g_SpawnData.MissionSpawnDetails.bFaceAwayFromPoint)
						SET_TEXT_COLOUR(255,0,0, 255)	
					ELSE
						SET_TEXT_COLOUR(0,255,255, 255)						
					ENDIF
					DISPLAY_TEXT_WITH_VECTOR(fDrawX, fDrawY, "SPN_V", g_SpawnData.MissionSpawnDetails.vFacing, 1)	
					iRows += 1
				ENDIF			
				
				// next respawn
				IF NOT (g_SpawnData.NextSpawn.Location = SPAWN_LOCATION_AUTOMATIC)
					fDrawX = g_SpawnData.MissionSpawnDetails.SpawnWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
					fDrawY = g_SpawnData.MissionSpawnDetails.SpawnWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
					SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
					GET_HUD_COLOUR(HUD_COLOUR_PINK, r,g,b,a)
					SET_TEXT_COLOUR(r,g,b,a)
					str = "custom next respawn location = "
					str += ENUM_TO_INT(g_SpawnData.NextSpawn.Location)
					DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", str, HUD_COLOUR_PINK)
				ENDIF
				
				
				IF (iActiveOffMissionOcclusion > 0)
					
					// draw title
					fDrawX = g_SpawnData.MissionSpawnDetails.SpawnWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
					fDrawY = g_SpawnData.MissionSpawnDetails.SpawnWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
					SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
					SET_TEXT_COLOUR(255,0,255, 255)
					DISPLAY_TEXT(fDrawX, fDrawY, "SPN_3")
					iRows += 1
					
					// draw details
					REPEAT MAX_NUM_MISSIONS_ALLOWED i	
						IF (GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.bIsActive)

							fDrawX = g_SpawnData.MissionSpawnDetails.SpawnWindowX + g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX
							fDrawY = g_SpawnData.MissionSpawnDetails.SpawnWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
							
							SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
							SET_TEXT_COLOUR(255,0,255, 255)
						
							DISPLAY_TEXT_WITH_VECTOR(fDrawX, fDrawY, "SPN_V", GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.vCoords1, 1)
							
							SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
							SET_TEXT_COLOUR(255,0,255, 255)
													
							SWITCH GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.iShape
								CASE SPAWN_AREA_SHAPE_CIRCLE 
									DISPLAY_TEXT_WITH_FLOAT(fDrawX + g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX, fDrawY, "SPN_R", GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.fFloat, 1)
								BREAK
								CASE SPAWN_AREA_SHAPE_BOX		
									DISPLAY_TEXT_WITH_VECTOR(fDrawX + g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX, fDrawY, "SPN_V", GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.vCoords2, 1)
								BREAK
								CASE SPAWN_AREA_SHAPE_ANGLED	
									DISPLAY_TEXT_WITH_VECTOR(fDrawX + g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX, fDrawY, "SPN_V", GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea.vCoords2, 1)
								BREAK
							ENDSWITCH
						
							iRows += 1
						ENDIF
					ENDREPEAT
				
				ENDIF		

				
				
			ENDIF
			
		ENDIF
	ENDIF

	
	// testing the safe coord
	IF (g_SpawnData.bGetSafeCoord)
		
		g_SpawnData.vTestCoordOutput = g_SpawnData.vTestCoordInput
		
		//GetSafeCoord(g_SpawnData.vTestCoordOutput ) //, g_SpawnData.bUseNonPavementCheckTest)
		
		g_SpawnData.bGetSafeCoord = FALSE
	ENDIF
	
	// test GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY
	IF (g_SpawnData.bTestSafeCoordsInArea)
		SWITCH g_SpawnData.iSafeCoordsShape
			CASE SPAWN_AREA_SHAPE_CIRCLE 					
				IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(g_SpawnData.vSafeCoordsPoint1, g_SpawnData.fSafeCoordsFloat, g_SpawnData.vSafeCoordsOut, g_SpawnData.fSafeHeadingOut, g_SpawnData.SafeSpawnSearchParams)
					g_SpawnData.bTestSafeCoordsInArea = FALSE
				ENDIF
			BREAK
			CASE SPAWN_AREA_SHAPE_BOX		
				// do nothing
				g_SpawnData.bTestSafeCoordsInArea = FALSE
			BREAK
			CASE SPAWN_AREA_SHAPE_ANGLED	
				IF GET_SAFE_COORDS_IN_ANGLED_AREA_FOR_CREATING_ENTITY(g_SpawnData.vSafeCoordsPoint1, g_SpawnData.vSafeCoordsPoint2, g_SpawnData.fSafeCoordsFloat, g_SpawnData.vSafeCoordsOut, g_SpawnData.fSafeHeadingOut, g_SpawnData.SafeSpawnSearchParams)
					g_SpawnData.bTestSafeCoordsInArea = FALSE
				ENDIF	
			BREAK
		ENDSWITCH
		
		//SET_ENTITY_COORDS(PLAYER_PED_ID(), g_SpawnData.vSafeCoordsOut, FALSE) 
		//SET_ENTITY_HEADING(PLAYER_PED_ID(), g_SpawnData.fSafeHeadingOut)

	ENDIF
	
	// test GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY
	IF (g_SpawnData.bCreateMissionPedWithSafeCoords)
		SWITCH g_SpawnData.iSafeCoordsShape
			CASE SPAWN_AREA_SHAPE_CIRCLE 					
				IF GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY(g_SpawnData.vSafeCoordsPoint1, g_SpawnData.fSafeCoordsFloat, g_SpawnData.vSafeCoordsOut, g_SpawnData.fSafeHeadingOut, g_SpawnData.SafeSpawnSearchParams)
					CREATE_NET_PED(g_SpawnData.NetIdDebug, PEDTYPE_MISSION, GET_ENTITY_MODEL(PLAYER_PED_ID()), g_SpawnData.vSafeCoordsOut, g_SpawnData.fSafeHeadingOut )
					g_SpawnData.bCreateMissionPedWithSafeCoords = FALSE
				ENDIF
			BREAK
			CASE SPAWN_AREA_SHAPE_BOX		
				// do nothing				
				g_SpawnData.bCreateMissionPedWithSafeCoords = FALSE
			BREAK
			CASE SPAWN_AREA_SHAPE_ANGLED	
				IF GET_SAFE_COORDS_IN_ANGLED_AREA_FOR_CREATING_ENTITY(g_SpawnData.vSafeCoordsPoint1, g_SpawnData.vSafeCoordsPoint2, g_SpawnData.fSafeCoordsFloat, g_SpawnData.vSafeCoordsOut, g_SpawnData.fSafeHeadingOut, g_SpawnData.SafeSpawnSearchParams)
					CREATE_NET_PED(g_SpawnData.NetIdDebug, PEDTYPE_MISSION, GET_ENTITY_MODEL(PLAYER_PED_ID()), g_SpawnData.vSafeCoordsOut, g_SpawnData.fSafeHeadingOut )
					g_SpawnData.bCreateMissionPedWithSafeCoords = FALSE
				ENDIF	
			BREAK
		ENDSWITCH
		
		//SET_ENTITY_COORDS(PLAYER_PED_ID(), g_SpawnData.vSafeCoordsOut, FALSE) 
		//SET_ENTITY_HEADING(PLAYER_PED_ID(), g_SpawnData.fSafeHeadingOut)

	ENDIF
	
	IF (g_SpawnData.MissionSpawnDetails.bToggleSpawnView)
		DRAW_DEBUG_SPAWN_POINT(g_SpawnData.vSafeCoordsOut, g_SpawnData.fSafeHeadingOut, HUD_COLOUR_GREEN, 1.0)	
		DRAW_DEBUG_SPAWN_TEXT("vSafeCoordsOut", g_SpawnData.vSafeCoordsOut, 255, 255, 255, 255)
	ENDIF

	
	IF (g_SpawnData.bTestShapeTest)
		DoSpawningShapeTest()	
		g_SpawnData.bTestShapeTest = FALSE
	ENDIF
	IF (g_SpawnData.bRenderShapeTest)
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF	

		IF (g_SpawnData.bShapeTestFailedForward)
			DRAW_DEBUG_ANGLED_AREA(g_SpawnData.vShapeTestStartForward, g_SpawnData.vShapeTestEndForward, SPAWN_SHAPETEST_WIDTH, 255, 0, 0, 64)
			DRAW_DEBUG_SPHERE(g_SpawnData.vShapeTestFailCoords_Forward, 0.1, 255, 0, 0)
		ELSE
			DRAW_DEBUG_ANGLED_AREA(g_SpawnData.vShapeTestStartForward, g_SpawnData.vShapeTestEndForward, SPAWN_SHAPETEST_WIDTH, 0, 255, 0, 64)
		ENDIF
		IF (g_SpawnData.bShapeTestFailedForwardRight)
			DRAW_DEBUG_ANGLED_AREA(g_SpawnData.vShapeTestStartForwardRight, g_SpawnData.vShapeTestEndForwardRight, SPAWN_SHAPETEST_WIDTH, 255, 0, 0, 64)
			DRAW_DEBUG_SPHERE(g_SpawnData.vShapeTestFailCoords_ForwardRight, 0.1, 255, 0, 0)
		ELSE
			DRAW_DEBUG_ANGLED_AREA(g_SpawnData.vShapeTestStartForwardRight, g_SpawnData.vShapeTestEndForwardRight, SPAWN_SHAPETEST_WIDTH, 255, 128, 0, 64)
		ENDIF
		IF (g_SpawnData.bShapeTestFailedRight)
			DRAW_DEBUG_ANGLED_AREA(g_SpawnData.vShapeTestStartRight, g_SpawnData.vShapeTestEndRight, SPAWN_SHAPETEST_WIDTH, 255, 0, 0, 64)
			DRAW_DEBUG_SPHERE(g_SpawnData.vShapeTestFailCoords_Right, 0.1, 255, 0, 0)
		ELSE
			DRAW_DEBUG_ANGLED_AREA(g_SpawnData.vShapeTestStartRight, g_SpawnData.vShapeTestEndRight, SPAWN_SHAPETEST_WIDTH, 255, 255, 0, 64)
		ENDIF
		IF (g_SpawnData.bShapeTestFailedForwardLeft)
			DRAW_DEBUG_ANGLED_AREA(g_SpawnData.vShapeTestStartForwardLeft, g_SpawnData.vShapeTestEndForwardLeft, SPAWN_SHAPETEST_WIDTH, 255, 0, 0, 64)
			DRAW_DEBUG_SPHERE(g_SpawnData.vShapeTestFailCoords_ForwardLeft, 0.1, 255, 0, 0)
		ELSE
			DRAW_DEBUG_ANGLED_AREA(g_SpawnData.vShapeTestStartForwardLeft, g_SpawnData.vShapeTestEndForwardLeft, SPAWN_SHAPETEST_WIDTH, 0, 128, 255, 64)
		ENDIF
		IF (g_SpawnData.bShapeTestFailedLeft)
			DRAW_DEBUG_ANGLED_AREA(g_SpawnData.vShapeTestStartLeft, g_SpawnData.vShapeTestEndLeft, SPAWN_SHAPETEST_WIDTH, 255, 0, 0, 64)
			DRAW_DEBUG_SPHERE(g_SpawnData.vShapeTestFailCoords_Left, 0.1, 255, 0, 0)
		ELSE
			DRAW_DEBUG_ANGLED_AREA(g_SpawnData.vShapeTestStartLeft, g_SpawnData.vShapeTestEndLeft, SPAWN_SHAPETEST_WIDTH, 0, 255, 255, 64)
		ENDIF
		IF (g_SpawnData.bShapeTestFailedRoad)
			DRAW_DEBUG_ANGLED_AREA(g_SpawnData.vShapeTestStartRoad, g_SpawnData.vShapeTestEndRoad, SPAWN_SHAPETEST_WIDTH, 255, 0, 0, 64)
			DRAW_DEBUG_SPHERE(g_SpawnData.vShapeTestFailCoords_Road, 0.1, 255, 0, 0)
		ELSE
			DRAW_DEBUG_ANGLED_AREA(g_SpawnData.vShapeTestStartRoad, g_SpawnData.vShapeTestEndRoad, SPAWN_SHAPETEST_WIDTH, 255, 0, 255, 64)
		ENDIF

	ENDIF
	
	
	IF  (g_SpawnData.bShowPropertySpawnInfo)
	
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF	
		
		REPEAT 5 i		
			DRAW_DEBUG_SPAWN_POINT(mpProperties[ g_SpawnData.iHighlightedProperty].garage.vCarExitLoc[i], mpProperties[ g_SpawnData.iHighlightedProperty].garage.fCarExitHeading[i], HUD_COLOUR_PURPLEDARK, 1.0)	
			str = "vCarExitLoc["
			str += i
			str += "]"
			DRAW_DEBUG_SPAWN_TEXT(str, mpProperties[ g_SpawnData.iHighlightedProperty].garage.vCarExitLoc[i], 255, 255, 255, 255)	
		ENDREPEAT
		
	ENDIF
	
	IF ( bTestSpawnActivity)
		
		IF (iTestSpawnActivityState = 0)
			SET_SPAWN_ACTIVITY(INT_TO_ENUM(PLAYER_SPAWN_ACTIVITY, g_TransitionSpawnData.iSpawnActivity))
			iTestSpawnActivityState++
		ENDIF
		
		IF (iTestSpawnActivityState = 1)
			IF IS_SPAWN_ACTIVITY_READY()	
				bTestSpawnActivity = FALSE
				iTestSpawnActivityState = 0
			ENDIF		
		ENDIF
		
	ENDIF
	
	IF (g_SpawnData.bGetCustomVehicleNodesForPlayerPosition)
		CLEAR_CUSTOM_VEHICLE_NODES()
		SETUP_ANY_CUSTOM_VEHICLE_NODES_NEAR_POINT(GET_PLAYER_COORDS(PLAYER_ID()), ESSKEY)
		g_SpawnData.bRenderCustomVehicleNodesForPlayerPosition = TRUE
		g_SpawnData.bGetCustomVehicleNodesForPlayerPosition = FALSE
	ENDIF
	IF (g_SpawnData.bClearCustomVehicleNodesForPlayerPosition)
		CLEAR_CUSTOM_VEHICLE_NODES()
		g_SpawnData.bClearCustomVehicleNodesForPlayerPosition = FALSE
	ENDIF
	IF (g_SpawnData.bRenderCustomVehicleNodesForPlayerPosition)
	
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF		
	
		REPEAT g_SpawnData.iNumberOfCustomVehicleNodes i
			DRAW_DEBUG_SPAWN_POINT(g_SpawnData.CustomVehicleNodes[i].vPos, g_SpawnData.CustomVehicleNodes[i].fHeading, HUD_COLOUR_ORANGE, 1.5)	
			str = "CUSTOM_VEHICLE_NODE "
			str += i
			DRAW_DEBUG_SPAWN_TEXT(str, g_SpawnData.CustomVehicleNodes[i].vPos, 255, 255, 255, 255)	
		ENDREPEAT
	ENDIF

	
	// ************ FOR TESTING HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS ******************
	
	IF (g_SpawnData.bUsePlayerPositionForSpawnVehicle)
		g_SpawnData.vSpawnVehicleStartCoords = GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID()) //GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		g_SpawnData.bUsePlayerPositionForSpawnVehicle = FALSE
	ENDIF
	IF (g_SpawnData.bTestSpawnVehicle)
	
		VEHICLE_SPAWN_LOCATION_PARAMS Params
		Params.fMinDistFromCoords=g_SpawnData.fSpawnVehicleMinDistFromCoords
		Params.bConsiderHighways=g_SpawnData.bSpawnVehicleConsiderHighways
		Params.bUseExactCoordsIfPossible=g_SpawnData.bSpawnVehicleInExactCoordsIfPossible
		Params.fMaxDistance=g_SpawnData.fSpawnVehicleMaxRange
		Params.bConsiderOnlyActiveNodes=g_SpawnData.bTestOnlyActiveNodes
		Params.bAvoidSpawningInExclusionZones=g_SpawnData.bTestAvoidExclusion
		Params.bBoatNodesOnly=g_SpawnData.bTestBoatNodesOnly
//		Params.fHigherZLimit=4.0
//		Params.bAllowFallbackToInactiveNodes=TRUE
//		Params.bStartOfMissionPVSpawn=FALSE
//		Params.bCheckEntityArea=FALSE
//		Params.bCheckOwnVisibility=TRUE
//		Params.bIsForPV=FALSE	
	
		IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(	
		g_SpawnData.vSpawnVehicleStartCoords,
		g_SpawnData.vSpawnVehicleFacingCoords,
		DUMMY_MODEL_FOR_SCRIPT,
		g_SpawnData.bSpawnVehicleUseRoadOffset, 
		g_SpawnData.vSpawnVehicleCoords, 
		g_SpawnData.fSpawnVehicleHeading, 
		Params)
		
			g_SpawnData.bTestSpawnVehicle = FALSE	
		ENDIF
	ENDIF
	
	IF (g_SpawnData.bRenderSpawnVehicleResult)
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF		
		
		DRAW_DEBUG_SPAWN_POINT(g_SpawnData.vSpawnVehicleCoords, g_SpawnData.fSpawnVehicleHeading, HUD_COLOUR_REDLIGHT, 1.0)	
		DRAW_DEBUG_SPAWN_TEXT("vehicle spawn location", g_SpawnData.vSpawnVehicleCoords, 255, 255, 255, 255)
		
	ENDIF
	
	IF (g_SpawnData.bTestMoveSpecificCoords)
		MOVE_SPECIFIC_SPAWN_LOCATION_OUTSIDE_EXCLUSION_ZONES(g_SpawnData.bMoveOutOfGlobalExclusion, g_SpawnData.bMoveOutOfMissionExclusion, g_SpawnData.bMoveOutOfGangAttack, g_SpawnData.bMoveOutOfMissionCorona)
		g_SpawnData.bTestMoveSpecificCoords = FALSE
	ENDIF

	IF (g_SpawnData.bTestFlashInvincible)
		NETWORK_SET_LOCAL_PLAYER_INVINCIBLE_TIME(GET_INVINCIBLE_FLASH_TIME())
		g_SpawnData.bTestFlashInvincible = FALSE
	ENDIF
	
	IF (g_SpawnData.bCallSetSpawnLocation)
		SET_MP_SPAWN_POINT_SETTING(INT_TO_ENUM(MULTIPLAYER_SETTING_SPAWN, g_SpawnData.iSpawnLocationToSet))
		g_SpawnData.bCallSetSpawnLocation = FALSE
	ENDIF
	
	INT iPositions
	VECTOR vPos
	FLOAT fHeading
	IF (g_SpawnData.bRenderGroupWarpPositions)
		
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF			
		
		REPEAT ENUM_TO_INT(GROUP_WARP_LOCATION_END) i		
			
			iPositions = GET_NUMBER_OF_POSITIONS_FOR_GROUP_WARP_LOCATION(INT_TO_ENUM(GROUP_WARP_LOCATION, i))
			
			REPEAT iPositions j				
				GET_SPAWN_COORDS_FOR_GROUP_WARP_LOCATION(INT_TO_ENUM(GROUP_WARP_LOCATION, i), 0, j, vPos, fHeading)				
				DRAW_DEBUG_SPAWN_POINT(vPos, fHeading, HUD_COLOUR_BLUELIGHT)	
				str = "Group Warp Spawn Point "
				str += j
				DRAW_DEBUG_SPAWN_TEXT(str, vPos, 0, 0, 0, 255)
			ENDREPEAT
						
		ENDREPEAT	
		
	ENDIF
	
	
	// fallback spawn points	
	IF (g_SpawnData.bShowFallbackSpawnPoints)
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF	
		REPEAT g_SpawnData.iNumberOfFallbackSpawnPoints i		
			DRAW_DEBUG_SPAWN_POINT(g_SpawnData.FallbackSpawnPoints[i].vPos, g_SpawnData.FallbackSpawnPoints[i].fHeading, HUD_COLOUR_BLUELIGHT)	
			str = "Fallback Spawn Point "
			str += i
			DRAW_DEBUG_SPAWN_TEXT(str, g_SpawnData.FallbackSpawnPoints[i].vPos, 0, 0, 0, 255)			
		ENDREPEAT	
	ENDIF
	IF (g_SpawnData.bAddFallbackSpawnPoint)
		ADD_FALLBACK_SPAWN_POINT(g_SpawnData.vFallbackSpawnPointPos, g_SpawnData.fFallbackSpawnPointHeading)
		g_SpawnData.bAddFallbackSpawnPoint = FALSE
	ENDIF
	IF (g_SpawnData.bClearFallbackSpawnPoints)
		CLEAR_FALLBACK_SPAWN_POINTS()
		g_SpawnData.bClearFallbackSpawnPoints = FALSE
	ENDIF
	
	
	// safe sea
	IF (g_SpawnData.bShowAllSafeSea)
		
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF
		
		REPEAT NUM_SAFE_SPAWN_IN_SEA i
			DRAW_DEBUG_SPAWN_POINT(SafeSpawn_Sea[i], 0.0, HUD_COLOUR_BLUELIGHT)	
			str = "Safe Sea Point "
			str += i
			DRAW_DEBUG_SPAWN_TEXT(str, SafeSpawn_Sea[i], 0, 0, 0, 255)
			
			IF NOT DOES_BLIP_EXIST(SafeSeaBlip[i])
				SafeSeaBlip[i] = ADD_BLIP_FOR_COORD(SafeSpawn_Sea[i])
			ELSE
				SET_BLIP_COORDS(SafeSeaBlip[i], SafeSpawn_Sea[i])
			ENDIF
			
		ENDREPEAT
		
		// warp player
		IF (g_SpawnData.iWarpToSafeSea > -1)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), SafeSpawn_Sea[g_SpawnData.iWarpToSafeSea], FALSE)
		ENDIF
		
		// output
		IF (g_SpawnData.bOutputAllSafeSea)
			IF OPEN_DEBUG_FILE()
				REPEAT NUM_SAFE_SPAWN_IN_SEA i
					str = "SafeSpawn_Sea["
					str += i
					str += "] = <<"
					SAVE_STRING_TO_DEBUG_FILE(str)
					SAVE_FLOAT_TO_DEBUG_FILE(SafeSpawn_Sea[i].x)
					SAVE_STRING_TO_DEBUG_FILE(", ")
					SAVE_FLOAT_TO_DEBUG_FILE(SafeSpawn_Sea[i].y)
					SAVE_STRING_TO_DEBUG_FILE(", ")
					SAVE_FLOAT_TO_DEBUG_FILE(SafeSpawn_Sea[i].z)
					SAVE_STRING_TO_DEBUG_FILE(">>"	)
					SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDREPEAT
				CLOSE_DEBUG_FILE()
				g_SpawnData.bOutputAllSafeSea = FALSE
			ENDIF
		ENDIF
		
		// record sea
		IF (g_SpawnData.bRecordSeaPoints)
			
			IF (g_SpawnData.iRecordedSeaPoint > 0)
				IF (g_SpawnData.iRecordedSeaPoint < NUM_SAFE_SPAWN_IN_SEA)
					IF (VDIST(GET_PLAYER_COORDS(PLAYER_ID()), g_SpawnData.vRecordedSeaPoint) > g_SpawnData.iRecordingSeaDist)
						SafeSpawn_Sea[g_SpawnData.iRecordedSeaPoint] = GET_PLAYER_COORDS(PLAYER_ID())
						g_SpawnData.vRecordedSeaPoint = SafeSpawn_Sea[g_SpawnData.iRecordedSeaPoint]
						g_SpawnData.iRecordedSeaPoint++
					ENDIF	
				ENDIF
			ELSE
				SafeSpawn_Sea[g_SpawnData.iRecordedSeaPoint] = GET_PLAYER_COORDS(PLAYER_ID())			
				g_SpawnData.vRecordedSeaPoint = SafeSpawn_Sea[g_SpawnData.iRecordedSeaPoint]
				g_SpawnData.iRecordedSeaPoint++
			ENDIF
		
		ENDIF
		
	ENDIF
	
	IF (bGrabForceJoinCoords)
		vForceJoinCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		FLOAT fTemp
		IF GET_GROUND_Z_FOR_3D_COORD(vForceJoinCoords, fTemp)
			vForceJoinCoords.z = fTemp	
		ENDIF
		fForceJoinHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
		bGrabForceJoinCoords = FALSE
	ENDIF
	
	IF (g_SpawnData.bCallSpawnRandomVehicleNearPlayer)
		SPAWN_RANDOM_VEHICLE_NEAR_PLAYER(TRUE)
		g_SpawnData.bCallSpawnRandomVehicleNearPlayer = FALSE		
	ENDIF
	IF (g_SpawnData.bCallSpawnSavedVehicleNearPlayer)
		IF SPAWN_PERSONAL_VEHICLE_NEAR_PLAYER(TRUE)
			g_SpawnData.bCallSpawnSavedVehicleNearPlayer = FALSE
		ENDIF
	ENDIF
	
//	// safe air
//	IF (g_SpawnData.bShowAllSafeAir)
//		
//		IF NOT (g_SpawnData.bDebugLinesActive)
//			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
//			g_SpawnData.bDebugLinesActive = TRUE
//		ENDIF
//		
//		REPEAT NUM_SAFE_SPAWN_IN_AIR i
//			DRAW_DEBUG_SPAWN_POINT(SafeSpawn_Air[i], 0.0, HUD_COLOUR_BLUELIGHT)	
//			str = "Safe Air Point "
//			str += i
//			DRAW_DEBUG_SPAWN_TEXT(str, SafeSpawn_Air[i], 0, 0, 0, 255)
//			
//			IF NOT DOES_BLIP_EXIST(SafeAirBlip[i])
//				SafeAirBlip[i] = ADD_BLIP_FOR_COORD(SafeSpawn_Air[i])
//			ELSE
//				SET_BLIP_COORDS(SafeAirBlip[i], SafeSpawn_Air[i])
//			ENDIF
//			
//		ENDREPEAT
//		
//		// warp player
//		IF NOT (g_SpawnData.bGrabAirGroundZ)
//			IF (g_SpawnData.iWarpToSafeAir > -1)
//				SET_ENTITY_COORDS(PLAYER_PED_ID(), SafeSpawn_Air[g_SpawnData.iWarpToSafeAir], FALSE)
//			ENDIF
//		ENDIF
//			
//		// grab the ground z
//		FLOAT fTempZ
//		IF (g_SpawnData.bGrabAirGroundZ)
//			IF (g_SpawnData.iGrabAirGroundZ < NUM_SAFE_SPAWN_IN_AIR)
//				//SET_ENTITY_COORDS(PLAYER_PED_ID(), SafeSpawn_Air[g_SpawnData.iGrabAirGroundZ], FALSE)	
//				IF NET_WARP_TO_COORD(SafeSpawn_Air[g_SpawnData.iGrabAirGroundZ], 0.0, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
//					GET_GROUND_Z_FOR_3D_COORD(<<SafeSpawn_Air[g_SpawnData.iGrabAirGroundZ].x, SafeSpawn_Air[g_SpawnData.iGrabAirGroundZ].y, 1000.0>>, fTempZ)
//					SafeSpawn_Air[g_SpawnData.iGrabAirGroundZ].z = fTempZ + 200.0
//					g_SpawnData.iGrabAirGroundZ++
//				ENDIF
//			ELSE
//				g_SpawnData.bGrabAirGroundZ = FALSE
//			ENDIF
//		ELSE
//			g_SpawnData.iGrabAirGroundZ = 0	
//		ENDIF
//		
//		// output
//		IF (g_SpawnData.bOutputAllSafeAir)
//			IF OPEN_DEBUG_FILE()
//				REPEAT NUM_SAFE_SPAWN_IN_AIR i
//					str = "SafeSpawn_Air["
//					str += i
//					str += "] = <<"
//					SAVE_STRING_TO_DEBUG_FILE(str)
//					SAVE_FLOAT_TO_DEBUG_FILE(SafeSpawn_Air[i].x)
//					SAVE_STRING_TO_DEBUG_FILE(", ")
//					SAVE_FLOAT_TO_DEBUG_FILE(SafeSpawn_Air[i].y)
//					SAVE_STRING_TO_DEBUG_FILE(", ")
//					SAVE_FLOAT_TO_DEBUG_FILE(SafeSpawn_Air[i].z)
//					SAVE_STRING_TO_DEBUG_FILE(">>"	)
//					SAVE_NEWLINE_TO_DEBUG_FILE()
//				ENDREPEAT
//				CLOSE_DEBUG_FILE()
//				g_SpawnData.bOutputAllSafeAir = FALSE
//			ENDIF
//		ENDIF
//		
//	ENDIF

	IF (g_SpawnData.bGiveHelmet)
		GIVE_PLAYER_STORED_HELMET()	
		g_SpawnData.bGiveHelmet = FALSE
	ENDIF
	
	IF (g_SpawnData.bClearVehSpawnTimer)
		SET_VEHICLE_CLEAR_JUST_PURCHASED(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.iNewVehPurchased)
		g_SpawnData.bClearVehSpawnTimer = FALSE
	ENDIF
	
	IF (g_SpawnData.bClearPVNetID)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_PV = INT_TO_NATIVE(NETWORK_INDEX, -1)
		g_SpawnData.bClearPVNetID = FALSE
	ENDIF
	
	IF (g_SpawnData.bTestInteriorCoord)
		INTERIOR_INSTANCE_INDEX InteriorInst
		InteriorInst = GET_INTERIOR_AT_COORDS(g_SpawnData.vTestInteriorCoord)
		
		NET_PRINT("[spawning] Test interior at coords ") 
		NET_PRINT_VECTOR(g_SpawnData.vTestInteriorCoord) 
		NET_PRINT(", Interior Inst = ") 
		NET_PRINT_INT(NATIVE_TO_INT(InteriorInst))
		NET_PRINT(", IS_VALID_INTERIOR = ")
		IF IS_VALID_INTERIOR(InteriorInst)
			NET_PRINT("TRUE")
		ELSE
			NET_PRINT("FALSE")
		ENDIF
		NET_PRINT(", IS_COLLISION_MARKED_OUTSIDE = ")
		IF IS_COLLISION_MARKED_OUTSIDE(g_SpawnData.vTestInteriorCoord)
			NET_PRINT("TRUE")
		ELSE
			NET_PRINT("FALSE")
		ENDIF
		NET_PRINT(", GET_INTERIOR_GROUP_ID = ")
		NET_PRINT_INT(GET_INTERIOR_GROUP_ID(InteriorInst))

		NET_NL()
			
		g_SpawnData.bTestInteriorCoord = FALSE 
	ENDIF
	
	IF (g_SpawnData.bCallCarNodeSearch)
	
		VECTOR vVec
		vVec = g_SpawnData.vCarNodeSearchInput
		fHeading = g_SpawnData.fCarNodeSearchInputHeading
		
		GetNearestCarNode(vVec, fHeading, g_SpawnData.CarNodeSearchParams)
		
		g_SpawnData.vCarNodeSearchOutput = vVec
		g_SpawnData.fCarNodeSearchOutputHeading = fHeading
		
		g_SpawnData.bCallCarNodeSearch = FALSE
	ENDIF
	
	IF (g_SpawnData.bCall_IS_SPAWN_AREA_ENTIRELY_INSIDE_ANGLED_AREA)
	
		IF IS_SPAWN_AREA_ENTIRELY_INSIDE_ANGLED_AREA(g_SpawnData.MissionSpawnDetails.SpawnArea[0], g_SpawnData.vCoords1_TestSAEIAA, g_SpawnData.vCoords2_TestSAEIAA, g_SpawnData.fFloat_TestSAEIAA)
			
		ENDIF
	
		g_SpawnData.bCall_IS_SPAWN_AREA_ENTIRELY_INSIDE_ANGLED_AREA = FALSE
	ENDIF
	
	IF (g_SpawnData.bCall_WarpPlayerIntoCar)
		
		PLAYER_INDEX PartnerID = INT_TO_NATIVE(PLAYER_INDEX, g_SpawnData.iPlayers_car)
		VEHICLE_INDEX PartnerVehicleID
		
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
			IF (g_SpawnData.bWarpPlayerIntoCar_LastCar)			
				PartnerVehicleID = GET_PLAYERS_LAST_VEHICLE()
			ELSE			
				IF PartnerID != INVALID_PLAYER_INDEX()
					IF IsPlayerInDriveableCar(PartnerID)						
						PartnerVehicleID = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(PartnerID))				
					ENDIF
				ENDIF			
			ENDIF
			
			IF DOES_ENTITY_EXIST(PartnerVehicleID)
			AND NOT IS_ENTITY_DEAD(PartnerVehicleID)
				WarpPlayerIntoCar(PLAYER_PED_ID(), PartnerVehicleID, INT_TO_ENUM(VEHICLE_SEAT, g_SpawnData.iCar_seat), g_SpawnData.bWarpPlayerIntoCar_Force)		
			ELSE
				g_SpawnData.bCall_WarpPlayerIntoCar = FALSE
			ENDIF
						
							
		ELSE
			g_SpawnData.bCall_WarpPlayerIntoCar = FALSE
		ENDIF
		
	ENDIF
	
	IF (g_SpawnData.bTest_GET_CLOSEST_VEHICLE)
	
		VehicleID = GET_CLOSEST_VEHICLE(g_SpawnData.vGET_CLOSEST_VEHICLE_coords, g_SpawnData.fGET_CLOSEST_VEHICLE_dist, INT_TO_ENUM(MODEL_NAMES, g_SpawnData.iGET_CLOSEST_VEHICLE_modelHash), g_SpawnData.iGET_CLOSEST_VEHICLE_searchflags)
		
		IF DOES_ENTITY_EXIST(VehicleID)
		AND NOT IS_ENTITY_DEAD(VehicleID)				
			g_SpawnData.vGET_CLOSEST_VEHICLE_return = GET_ENTITY_COORDS(VehicleID, FALSE)
		ELSE
			g_SpawnData.vGET_CLOSEST_VEHICLE_return = << -1.0, -1.0, -1.0>>	
		ENDIF
		
		g_SpawnData.bTest_GET_CLOSEST_VEHICLE = FALSE
	ENDIF
			
	
	IF (g_SpawnData.bRenderRespotSphere)
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF	
		DRAW_DEBUG_FLOAT_CIRCLE(g_SpawnData.MissionSpawnDetails.vRaceRespawnPos, 6.0, 255, 0, 110, g_SpawnData.iAlphaValue)		
		str = "Respot Sphere"
		DRAW_DEBUG_SPAWN_TEXT(str, g_SpawnData.MissionSpawnDetails.vRaceRespawnPos, 255, 0, 110, 255)
	ENDIF	
	
	// viewing cone
	IF (g_SpawnData.bShowViewingCone)
		
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF		
		
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			
			vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
			fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
			
			fPlayerHeading *= -1.0
			fPlayerHeading += 360.0
			
			vForward.x = SIN(fPlayerHeading)
			vForward.y = COS(fPlayerHeading)
			vForward.z = 0.0
			vForward /= VMAG(vForward)
			
			vForward *= SPAWN_VIEWING_RANGE
		
			vCross = CROSS_PRODUCT(vForward, <<0.0, 0.0, 1.0>>)
			vCross /= VMAG(vCross)
			vCross *= (SPAWN_VIEWING_WIDTH * 0.5)
		
			vCornerPoint[0] = (vPlayerCoords + vForward) + vCross
			vCornerPoint[0].z += SPAWN_VIEWING_MAX_Z_DIFF
			
			vCornerPoint[1] = (vPlayerCoords + vForward) + vCross
			vCornerPoint[1].z += -1.0 * SPAWN_VIEWING_MAX_Z_DIFF
		
			vCornerPoint[2] = (vPlayerCoords + vForward) - vCross
			vCornerPoint[2].z += -1.0 * SPAWN_VIEWING_MAX_Z_DIFF
		
			vCornerPoint[3] = (vPlayerCoords + vForward) - vCross
			vCornerPoint[3].z += SPAWN_VIEWING_MAX_Z_DIFF
		
			vNearCornerPoint[0] = vPlayerCoords + vCross
			vNearCornerPoint[0].z += SPAWN_VIEWING_MAX_Z_DIFF 
			
			vNearCornerPoint[1] = vPlayerCoords + vCross
			vNearCornerPoint[1].z += -1.0 * SPAWN_VIEWING_MAX_Z_DIFF
			
			vNearCornerPoint[2] = vPlayerCoords - vCross
			vNearCornerPoint[2].z += -1.0 * SPAWN_VIEWING_MAX_Z_DIFF
			
			vNearCornerPoint[3] = vPlayerCoords - vCross
			vNearCornerPoint[3].z += SPAWN_VIEWING_MAX_Z_DIFF 

						
			IF IsAnyEnemyInViewingRangeOfPoint(GET_PLAYER_COORDS(PLAYER_ID()), GET_ENTITY_HEADING(PLAYER_PED_ID()), SPAWN_VIEWING_RANGE, SPAWN_VIEWING_WIDTH, SPAWN_VIEWING_MAX_Z_DIFF)
				GET_HUD_COLOUR(HUD_COLOUR_RED, r,g,b,a)
			ELSE
				GET_HUD_COLOUR(HUD_COLOUR_GREEN, r,g,b,a)
			ENDIF
						
			DRAW_DEBUG_LINE(vCornerPoint[0], vCornerPoint[1], r,g,b,a)
			DRAW_DEBUG_LINE(vCornerPoint[1], vCornerPoint[2], r,g,b,a)
			DRAW_DEBUG_LINE(vCornerPoint[2], vCornerPoint[3], r,g,b,a)
			DRAW_DEBUG_LINE(vCornerPoint[3], vCornerPoint[0], r,g,b,a)
			
			DRAW_DEBUG_LINE(vNearCornerPoint[0], vNearCornerPoint[1], r,g,b,a)
			DRAW_DEBUG_LINE(vNearCornerPoint[1], vNearCornerPoint[2], r,g,b,a)
			DRAW_DEBUG_LINE(vNearCornerPoint[2], vNearCornerPoint[3], r,g,b,a)
			DRAW_DEBUG_LINE(vNearCornerPoint[3], vNearCornerPoint[0], r,g,b,a)
			
			DRAW_DEBUG_LINE(vCornerPoint[0], vNearCornerPoint[0], r,g,b,a)
			DRAW_DEBUG_LINE(vCornerPoint[1], vNearCornerPoint[1], r,g,b,a)
			DRAW_DEBUG_LINE(vCornerPoint[2], vNearCornerPoint[2], r,g,b,a)
			DRAW_DEBUG_LINE(vCornerPoint[3], vNearCornerPoint[3], r,g,b,a)
			
		ENDIF
	
	ENDIF
	
	// screen blurring	
	IF (g_SpawnData.bBlurForSpawn)	
		IF MakeScreenBlurredOutForSpawn()
			g_SpawnData.bBlurForSpawn = FALSE	
		ENDIF
	ENDIF
	IF (g_SpawnData.bClearBlurForSpawn)
		RestoreScreenFromSpawnBlur()
		g_SpawnData.bClearBlurForSpawn = FALSE
	ENDIF

	IF (g_bDoWarpToReAssign)
		START_WARP_PRIVATE_YACHT_TO_ZONE(INT_TO_ENUM(PRIVATE_YACHT_SPAWN_ZONE_ENUM, g_iReassignZone))
		g_bDoWarpToReAssign = FALSE
	ENDIF

	IF (g_SpawnData.bSetCurrentCarAsRespawnVehicle)
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
			VehicleID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF NETWORK_HAS_CONTROL_OF_ENTITY(VehicleID)
				SET_VEHICLE_AS_A_SPECIFIC_RESPAWN_VEHICLE(VehicleID)
			ENDIF
			
			SET_PLAYER_TO_RESPAWN_IN_SPECIFIC_RESPAWN_VEHICLE(VehicleID)
		
		ENDIF
		
		g_SpawnData.bSetCurrentCarAsRespawnVehicle = FALSE
	ENDIF
	
	#IF FEATURE_FIXER
	

	IF (g_iPostMissionSpawnScenario > -1)
		g_TransitionSessionNonResetVars.sPostMissionCleanupData.PostMissionSpawnScenario = INT_TO_ENUM(POST_MISSION_SPAWN_SCENARIO, g_iPostMissionSpawnScenario)	
	ENDIF
	
	
	#ENDIF

ENDPROC
#ENDIF

#IF USE_TU_CHANGES
PROC DEAL_WITH_DAILY_OBJECTIVES_GIFT_ACTIVE()
	
	IF g_sMPTunables.iDailyObjectiveRewardMale != 0
	OR g_sMPTunables.iDailyObjectiveRewardFemale != 0
		INT iTuneableHashValue

		
		PED_COMP_TYPE_ENUM ePedCompTypeEnum
		PED_COMP_NAME_ENUM ePedCompNameEnum
		
		IF GET_PLAYER_MODEL() = MP_M_FREEMODE_01
			iTuneableHashValue = g_sMPTunables.iDailyObjectiveRewardMale
			PRINTLN("DEAL_WITH_DAILY_OBJECTIVES_GIFT_ACTIVE - GET_PLAYER_MODEL() = MP_M_FREEMODE_01 - iTuneableHashValue = g_sMPTunables.iDailyObjectiveRewardMale = ", iTuneableHashValue)
		ELSE
			iTuneableHashValue = g_sMPTunables.iDailyObjectiveRewardFemale
			PRINTLN("DEAL_WITH_DAILY_OBJECTIVES_GIFT_ACTIVE - GET_PLAYER_MODEL() != MP_M_FREEMODE_01 - iTuneableHashValue = g_sMPTunables.iDailyObjectiveRewardFemale = ", iTuneableHashValue)
		ENDIF
		// Is the item a piece of valid clothing?
		IF GET_MP_REWARD_CLOTHING_FROM_TUNABLE(GET_PLAYER_MODEL(), iTuneableHashValue, ePedCompTypeEnum, ePedCompNameEnum)		
			PRINTLN("DEAL_WITH_DAILY_OBJECTIVES_GIFT_ACTIVE - GET_MP_REWARD_CLOTHING_FROM_TUNABLE(GET_PLAYER_MODEL(), ", iTuneableHashValue, ", ePedCompTypeEnum, ePedCompNameEnum)")
			TEXT_LABEL_31 tlRewardLabel
			//...yes, give the item to the player
			GIVE_MP_REWARD_CLOTHING(ePedCompTypeEnum, ePedCompNameEnum, tlRewardLabel)
			PRINTLN("DEAL_WITH_DAILY_OBJECTIVES_GIFT_ACTIVE - Rewarded item associated with this daily objective (Clothing). ", tlRewardLabel)
			DISPLAY_TSHIRT_AWARD_MESSGE_AFTER_TRANSITION(ePedCompTypeEnum)
		ENDIF
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL GET_LSIA_GENERAL_DELIVERY_SCENE_SPAWN_COORD(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)	
	SWITCH iSpawnPoint
        CASE 0	vSpawnPoint = <<-1168.9139, -2751.2900, 13.0290>>	fSpawnHeading = 293.599	RETURN TRUE BREAK
		CASE 1	vSpawnPoint = <<-1170.4399, -2751.1650, 13.0290>>	fSpawnHeading = 293.599	RETURN TRUE BREAK
		CASE 2	vSpawnPoint = <<-1169.3510, -2753.1599, 13.0290>>	fSpawnHeading = 293.599	RETURN TRUE BREAK
		CASE 3	vSpawnPoint = <<-1170.3010, -2752.4541, 13.0290>>	fSpawnHeading = 286.399	RETURN TRUE BREAK
		CASE 4	vSpawnPoint = <<-1170.3409, -2754.7070, 13.0280>>	fSpawnHeading = 301.399	RETURN TRUE BREAK
		CASE 5	vSpawnPoint = <<-1171.3929, -2754.0320, 13.0280>>	fSpawnHeading = 290.399	RETURN TRUE BREAK
		CASE 6	vSpawnPoint = <<-1171.1660, -2755.8140, 13.0280>>	fSpawnHeading = 301.799	RETURN TRUE BREAK
		CASE 7	vSpawnPoint = <<-1172.0601, -2752.8550, 13.0290>>	fSpawnHeading = 290.198	RETURN TRUE BREAK
		CASE 8	vSpawnPoint = <<-1172.1490, -2751.6450, 13.0290>>	fSpawnHeading = 290.198	RETURN TRUE BREAK
		CASE 9	vSpawnPoint = <<-1171.0010, -2750.1589, 13.0300>>	fSpawnHeading = 248.798	RETURN TRUE BREAK
		CASE 10	vSpawnPoint = <<-1173.0510, -2750.5559, 13.0300>>	fSpawnHeading = 267.998	RETURN TRUE BREAK
		CASE 11	vSpawnPoint = <<-1174.5081, -2750.6450, 13.0290>>	fSpawnHeading = 267.998	RETURN TRUE BREAK
		CASE 12	vSpawnPoint = <<-1173.7920, -2751.7700, 13.0290>>	fSpawnHeading = 278.998	RETURN TRUE BREAK
		CASE 13	vSpawnPoint = <<-1173.8571, -2753.4729, 13.0290>>	fSpawnHeading = 278.998	RETURN TRUE BREAK
		CASE 14	vSpawnPoint = <<-1173.3240, -2754.5630, 13.0280>>	fSpawnHeading = 300.197	RETURN TRUE BREAK
		CASE 15	vSpawnPoint = <<-1172.9780, -2755.6270, 13.0280>>	fSpawnHeading = 300.197	RETURN TRUE BREAK
		CASE 16	vSpawnPoint = <<-1172.6490, -2757.0420, 13.0270>>	fSpawnHeading = 284.597	RETURN TRUE BREAK
		CASE 17	vSpawnPoint = <<-1172.1200, -2758.7949, 13.0270>>	fSpawnHeading = 310.997	RETURN TRUE BREAK
		CASE 18	vSpawnPoint = <<-1171.1730, -2757.4900, 13.0270>>	fSpawnHeading = 299.997	RETURN TRUE BREAK
		CASE 19	vSpawnPoint = <<-1173.0150, -2760.1780, 13.0260>>	fSpawnHeading = 311.997	RETURN TRUE BREAK
		CASE 20	vSpawnPoint = <<-1173.8060, -2761.7041, 13.0260>>	fSpawnHeading = 311.997	RETURN TRUE BREAK
		CASE 21	vSpawnPoint = <<-1174.3790, -2760.4890, 13.0260>>	fSpawnHeading = 284.797	RETURN TRUE BREAK
		CASE 22	vSpawnPoint = <<-1173.7670, -2758.5940, 13.0270>>	fSpawnHeading = 284.797	RETURN TRUE BREAK
		CASE 23	vSpawnPoint = <<-1174.4960, -2756.7910, 13.0270>>	fSpawnHeading = 284.797	RETURN TRUE BREAK
		CASE 24	vSpawnPoint = <<-1174.4930, -2755.5669, 13.0280>>	fSpawnHeading = 257.597	RETURN TRUE BREAK
		CASE 25	vSpawnPoint = <<-1175.2710, -2758.2690, 13.0270>>	fSpawnHeading = 284.796	RETURN TRUE BREAK
		CASE 26	vSpawnPoint = <<-1175.7419, -2756.6379, 13.0270>>	fSpawnHeading = 284.796	RETURN TRUE BREAK
		CASE 27	vSpawnPoint = <<-1175.2760, -2754.1941, 13.0280>>	fSpawnHeading = 284.796	RETURN TRUE BREAK
		CASE 28	vSpawnPoint = <<-1175.3490, -2752.4709, 13.0290>>	fSpawnHeading = 257.596	RETURN TRUE BREAK
		CASE 29	vSpawnPoint = <<-1176.5170, -2755.0569, 13.0280>>	fSpawnHeading = 276.396	RETURN TRUE BREAK
    ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_SANDY_AIRFIELD_POST_DELIVERY_SCENE_SPAWN_POINT(INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)
	SWITCH iSpawnPoint
		CASE 0	vSpawnPoint 	= <<1765.9814, 3286.6099, 40.2033>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 1	vSpawnPoint 	= <<1767.1337, 3284.3911, 40.2520>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 2	vSpawnPoint 	= <<1764.8292, 3288.8286, 40.1870>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 3	vSpawnPoint 	= <<1768.2859, 3282.1724, 40.3492>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 4	vSpawnPoint 	= <<1763.6770, 3291.0474, 40.1685>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 5	vSpawnPoint 	= <<1769.4381, 3279.9536, 40.3880>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 6	vSpawnPoint 	= <<1768.2001, 3287.7622, 40.2260>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 7	vSpawnPoint 	= <<1769.3523, 3285.5435, 40.2467>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 8	vSpawnPoint 	= <<1767.0479, 3289.9810, 40.2093>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 9	vSpawnPoint 	= <<1770.5045, 3283.3247, 40.4095>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 10	vSpawnPoint 	= <<1765.8956, 3292.1997, 40.1874>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 11	vSpawnPoint 	= <<1771.6567, 3281.1060, 40.4493>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 12	vSpawnPoint 	= <<1770.4187, 3288.9146, 40.2548>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 13	vSpawnPoint 	= <<1771.5709, 3286.6958, 40.2780>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 14	vSpawnPoint 	= <<1769.2665, 3291.1333, 40.2380>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 15	vSpawnPoint 	= <<1772.7231, 3284.4771, 40.4224>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 16	vSpawnPoint 	= <<1768.1143, 3293.3521, 40.2018>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 17	vSpawnPoint 	= <<1773.8754, 3282.2583, 40.5232>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 18	vSpawnPoint 	= <<1772.6373, 3290.0669, 40.2870>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 19	vSpawnPoint 	= <<1773.7896, 3287.8481, 40.3220>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 20	vSpawnPoint 	= <<1771.4851, 3292.2856, 40.2707>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 21	vSpawnPoint 	= <<1774.9418, 3285.6294, 40.4517>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 22	vSpawnPoint 	= <<1770.3329, 3294.5044, 40.2364>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 23	vSpawnPoint 	= <<1776.0940, 3283.4106, 40.5946>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 24	vSpawnPoint 	= <<1774.8560, 3291.2192, 40.3209>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 25	vSpawnPoint 	= <<1776.0082, 3289.0005, 40.3806>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 26	vSpawnPoint 	= <<1773.7037, 3293.4380, 40.3052>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 27	vSpawnPoint 	= <<1777.1604, 3286.7817, 40.5302>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 28	vSpawnPoint 	= <<1772.5515, 3295.6567, 40.2816>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 29	vSpawnPoint 	= <<1778.3126, 3284.5630, 40.6445>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 30	vSpawnPoint 	= <<1777.0746, 3292.3716, 40.3575>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		CASE 31	vSpawnPoint 	= <<1778.2268, 3290.1528, 40.5076>>	fSpawnHeading 	= 297.4450	RETURN TRUE	BREAK
		DEFAULT RETURN FALSE
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
