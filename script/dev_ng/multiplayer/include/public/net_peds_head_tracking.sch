//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_HEAD_TRACKING.sch																				//
// Description: Header file containing head tracking functionality for peds.											//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		26/02/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "net_peds_common.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════╡ SET HEAD TRACKING DATA ╞════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Sets the local ped head tracking data.
///    Populates the local head tracking array. Array size: MAX_NUM_LOCAL_HEAD_TRACKING_PEDS
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All local ped data to query.
PROC SET_PED_HEAD_TRACKING_LOCAL_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	INT iArrayID
	REPEAT MAX_NUM_LOCAL_HEAD_TRACKING_PEDS iArrayID
		RESET_PED_HEAD_TRACKING_DATA(LocalData.HeadTrackingData[iArrayID])
		SET_PED_HEAD_TRACKING_DATA(ePedLocation, LocalData.HeadTrackingData[iArrayID], ServerBD.iLayout, iArrayID, LocalData.iHeadTrackingPedID)
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Sets the network ped head tracking data.
///    Populates the network head tracking array. Array size: MAX_NUM_NETWORK_HEAD_TRACKING_PEDS
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All local ped data to query.
PROC SET_PED_HEAD_TRACKING_NETWORK_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
	
	INT iArrayID
	REPEAT MAX_NUM_NETWORK_HEAD_TRACKING_PEDS iArrayID
		RESET_PED_HEAD_TRACKING_DATA(ServerBD.HeadTrackingData[iArrayID])
		SET_PED_HEAD_TRACKING_DATA(ePedLocation, ServerBD.HeadTrackingData[iArrayID], ServerBD.iLayout, iArrayID, ServerBD.iHeadTrackingPedID, TRUE)
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Initialises the ped head tracking local array.
///    Sets all elements to -1.
/// PARAMS:
///    LocalData - Local ped data to set.
PROC INITIALISE_PED_HEAD_TRACKING_LOCAL_ARRAY_ID(SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iPed
		LocalData.iHeadTrackingPedID[iPed] = -1
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Initialises the ped head tracking network array.
///    Sets all elements to -1.
/// PARAMS:
///    ServerBD - Server ped data to set.
PROC INITIALISE_PED_HEAD_TRACKING_NETWORK_ARRAY_ID(SERVER_PED_DATA &ServerBD)
	
	INT iPed
	REPEAT MAX_NUM_TOTAL_NETWORK_PEDS iPed
		ServerBD.iHeadTrackingPedID[iPed] = -1
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Sets the head tracking data for all peds.
///    Populates the head tracking arrays. 
///    Local array max: MAX_NUM_LOCAL_HEAD_TRACKING_PEDS
///    Network array size: MAX_NUM_NETWORK_HEAD_TRACKING_PEDS
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All local ped data to query.
PROC SET_ALL_PED_HEAD_TRACKING_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	INITIALISE_PED_HEAD_TRACKING_LOCAL_ARRAY_ID(LocalData)
	SET_PED_HEAD_TRACKING_LOCAL_DATA(ePedLocation, ServerBD, LocalData)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		INITIALISE_PED_HEAD_TRACKING_NETWORK_ARRAY_ID(ServerBD)
		SET_PED_HEAD_TRACKING_NETWORK_DATA(ePedLocation, ServerBD)
	ENDIF
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ TASK HEAD TRACKING ╞══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    To be used within UPDATE_PED_HEAD_TRACKING().
///    Head tracks the nearest player infront of the ped.
/// PARAMS:
///    Data - Ped data to query.
///    HeadTrackingData - Head tracking data to query.
PROC PERFORM_PED_HEAD_TRACKING_PLAYER(INT iPedID, PEDS_DATA &Data, HEAD_TRACKING_DATA &HeadTrackingData, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	
	// Head track the nearest player to the ped
	PLAYER_INDEX nearestPlayerToPed = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY(Data.PedID))
	IF (nearestPlayerToPed = INVALID_PLAYER_INDEX())
		EXIT
	ENDIF
	
	BOOL bShouldHeadTrackEntity = FALSE
	ENTITY_INDEX playerPedID = GET_PLAYER_PED(nearestPlayerToPed)
	
	IF IS_ENTITY_ALIVE(playerPedID)
		IF IS_ENTITY_IN_RANGE_ENTITY(Data.PedID, playerPedID, HeadTrackingData.fHeadTrackingRange)
			VECTOR vPedDirection = GET_ENTITY_FORWARD_VECTOR(Data.PedID)
			VECTOR vDirectionToEntity = NORMALISE_VECTOR(GET_ENTITY_COORDS(playerPedID) - GET_ENTITY_COORDS(Data.PedID))
			
			IF DOT_PRODUCT(vPedDirection, vDirectionToEntity) > 0.1
				bShouldHeadTrackEntity = TRUE
			ENDIF
			
			IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HEAD_TRACKING_ONLY_WITH_BASE_ANIM)
			AND bShouldHeadTrackEntity
				IF Data.animData.iCurrentClip != 0
					bShouldHeadTrackEntity = FALSE
				ENDIF
			ENDIF	
			
			IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HEAD_TRACKING_WITH_CONDITIONS)
			AND bShouldHeadTrackEntity
				bShouldHeadTrackEntity = CAN_PED_HEAD_TRACK(ePedLocation, PLAYER_PED_ID(), iPedID, 0)
			ENDIF
		ENDIF
				
		
		IF bShouldHeadTrackEntity
			IF NOT IS_PED_HEADTRACKING_ENTITY(Data.PedID, playerPedID)
				TASK_LOOK_AT_ENTITY(Data.PedID, playerPedID, HeadTrackingData.iHeadTrackingTime, HeadTrackingData.eHeadTrackingLookFlag, HeadTrackingData.eHeadTrackingLookPriority)
			ENDIF
		ELSE
			IF IS_PED_HEADTRACKING_ENTITY(Data.PedID, playerPedID)
				TASK_CLEAR_LOOK_AT(Data.PedID)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    To be used within UPDATE_PED_HEAD_TRACKING().
/// PARAMS:
///    Data - Ped data to query.
PROC PERFORM_PED_HEAD_TRACKING_COORDS(PEDS_DATA &Data, HEAD_TRACKING_DATA &HeadTrackingData)
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SET_HEAD_TRACKING_COORDS)
		SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SET_HEAD_TRACKING_COORDS)
		TASK_LOOK_AT_COORD(Data.PedID, HeadTrackingData.vHeadTrackingCoords, HeadTrackingData.iHeadTrackingTime, HeadTrackingData.eHeadTrackingLookFlag, HeadTrackingData.eHeadTrackingLookPriority)
	ENDIF
ENDPROC

/// PURPOSE:
///    Main update function for the local ped head tracking.
///    Performs either entity or coord head tracking on a ped.
/// PARAMS:
///    LocalData - All local ped data to query.
///    Data - Ped data to query.
PROC UPDATE_LOCAL_PED_HEAD_TRACKING(PED_LOCATIONS ePedLocation, SCRIPT_PED_DATA &LocalData, INT iPedID)
	
	IF NOT IS_PEDS_BIT_SET(LocalData.PedData[iPedID].iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
	AND NOT IS_PEDS_BIT_SET(LocalData.PedData[iPedID].iBS, BS_PED_DATA_USE_HEAD_TRACKING_COORDS)
		EXIT
	ENDIF
	
	IF (LocalData.iHeadTrackingPedID[iPedID] = -1)
		EXIT
	ENDIF
	
	IF IS_PEDS_BIT_SET(LocalData.PedData[iPedID].iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
		PERFORM_PED_HEAD_TRACKING_PLAYER(iPedID, LocalData.PedData[iPedID], LocalData.HeadTrackingData[LocalData.iHeadTrackingPedID[iPedID]], ePedLocation)
		
	ELIF IS_PEDS_BIT_SET(LocalData.PedData[iPedID].iBS, BS_PED_DATA_USE_HEAD_TRACKING_COORDS)
		PERFORM_PED_HEAD_TRACKING_COORDS(LocalData.PedData[iPedID], LocalData.HeadTrackingData[LocalData.iHeadTrackingPedID[iPedID]])
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Main update function for the network ped head tracking.
///    Performs either entity or coord head tracking on a ped.
/// PARAMS:
///    ServerBD - Server ped data to query.
///    Data - Ped data to query.
PROC UPDATE_NETWORK_PED_HEAD_TRACKING(SERVER_PED_DATA &ServerBD, INT iPedID)
	
	IF NOT IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
	AND NOT IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_COORDS)
		EXIT
	ENDIF
	
	IF (ServerBD.iHeadTrackingPedID[iPedID] = -1)
		EXIT
	ENDIF
	
	IF IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
		PERFORM_PED_HEAD_TRACKING_PLAYER(iPedID, ServerBD.NetworkPedData[iPedID].Data, ServerBD.HeadTrackingData[ServerBD.iHeadTrackingPedID[iPedID]])
		
	ELIF IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_COORDS)
		PERFORM_PED_HEAD_TRACKING_COORDS(ServerBD.NetworkPedData[iPedID].Data, ServerBD.HeadTrackingData[ServerBD.iHeadTrackingPedID[iPedID]])
	ENDIF
	
ENDPROC
#ENDIF	// FEATURE_HEIST_ISLAND
