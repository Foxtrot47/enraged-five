USING "net_spawn.sch"
USING "SceneTool_public.sch"

USING "cutscene_public.sch"
USING "rc_helper_functions.sch"

#IF IS_DEBUG_BUILD
USING "net_realty_scene_debug.sch"
#ENDIF

ENUM enumNET_REALTY_4_SCENEPans
	NET_REALTY_4_SCENE_PAN_walkOut,
	NET_REALTY_4_SCENE_PAN_MAX
ENDENUM

ENUM enumNET_REALTY_4_SCENECuts
	NET_REALTY_4_SCENE_CUT_catchup = 0,
	NET_REALTY_4_SCENE_CUT_MAX
ENDENUM

ENUM enumNET_REALTY_4_SCENEMarkers
	NET_REALTY_4_SCENE_MARKER_walkTo,
	NET_REALTY_4_SCENE_MARKER_MAX
ENDENUM

ENUM enumNET_REALTY_4_SCENEPlacers
	NET_REALTY_4_SCENE_PLACER_resetCoords,
	NET_REALTY_4_SCENE_PLACER_occupiedSphere,
	NET_REALTY_4_SCENE_PLACER_MAX
ENDENUM

CONST_INT WOS_INIT						0
CONST_INT WOS_WAIT_FOR_DOOR_CONTROL		1
CONST_INT WOS_GIVE_SEQUENCE				2
CONST_INT WOS_WAIT_FOR_SEQUENCE_END		3
CONST_INT WOS_RETURN_CONTROL_GRACEFULLY	4
CONST_INT WOS_WARP_TO_UNOCCUPIED_AREA	5
CONST_INT WOS_SET_IN_UNOCCUPIED_AREA	6
CONST_INT WOS_CLEANUP					7

STRUCT STRUCT_NET_REALTY_4_SCENE
	structSceneTool_Pan		mPans[NET_REALTY_4_SCENE_PAN_MAX]
	structSceneTool_Cut		mCuts[NET_REALTY_4_SCENE_CUT_MAX]
	
	structSceneTool_Marker	mMarkers[NET_REALTY_4_SCENE_MARKER_MAX]
	structSceneTool_Placer	mPlacers[NET_REALTY_4_SCENE_PLACER_MAX]
	
	BOOL					bEnablePans[NET_REALTY_4_SCENE_PAN_MAX]
	FLOAT					fExitDelay
ENDSTRUCT

FUNC BOOL Private_Fake_NET_REALTY_4_SCENE(STRUCT_NET_REALTY_4_SCENE& scene)
	
	scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos
	scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0
	
	RETURN TRUE
ENDFUNC


FUNC BOOL Private_Get_NET_REALTY_4_SCENE(INT iBuildingID, STRUCT_NET_REALTY_4_SCENE& scene	#IF ACTIVATE_PROPERTY_CS_DEBUG	,	BOOL bIgnoreAssert = FALSE	#ENDIF	)
	SWITCH iBuildingID
		CASE 0	RETURN FALSE	BREAK
		
		CASE MP_PROPERTY_BUILDING_1
			
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-779.846680,307.215790,86.100006>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<2.534921,0.000000,-22.975626>>
			
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-778.645813,306.863953,86.106171>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<1.323177,-0.000000,-20.878082>>
			
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 40.0
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.0
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 5.000 - 0.6
			
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake
			
			scene.bEnablePans[NET_REALTY_4_SCENE_PAN_walkOut] = FALSE
			
			scene.fExitDelay = 0.66+0.0000
			
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-777.0378, 311.5292, 84.6992>>
			
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-777.4117, 317.9462, 84.6690>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 180.9339
			
			Private_Fake_NET_REALTY_4_SCENE(scene)
			
			RETURN TRUE
		BREAK
		CASE MP_PROPERTY_BUILDING_2          //High apt 5,6
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-255.3428, -975.6434, 33.2047>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-9.2821, -0.0000, 48.3619>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-257.4192, -976.5851, 32.2040>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-6.9527, -0.0000, 46.9130>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 50.0000
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 6.5000 - 0.4
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//<<-252.5713, -949.9199, 37.7210>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-260.9806, -972.6545, 30.2196>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-264.7780, -964.3018, 30.2236>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 206.2500
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.66+0.0000
			
			Private_Fake_NET_REALTY_4_SCENE(scene)
			
			RETURN TRUE
		BREAK
		
		
		//Joe Rubino , Tue 17/09/2013 22:43
		CASE MP_PROPERTY_BUILDING_18	//Low apt 2
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-206.1010, 188.4548, 96.4279>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-76.4265, 0.0000, -136.0800>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-205.8278, 188.1450, 94.7147>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-76.4265, -0.0000, -144.4217>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 28.0785
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 4.2000 - 0.6
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//<<-206.7293, 184.1420, 86.8279>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-204.7485, 184.3757, 79.3274>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-200.6764, 186.3481, 79.5104>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 91.4400
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<-206.1342, 184.3563, 79.3274>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.66
			RETURN TRUE
		BREAK
		
		//Forrest Karbowski, Mon 26/08/2013 23:25
		CASE MP_PROPERTY_BUILDING_19        //Low apt 3
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-816.6025, -991.1185, 17.0888>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<17.6505, -0.0000, -41.4345>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-815.9750, -990.4141, 14.3000>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<2.2736, 0.0000, -23.4331>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 50.0000
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 6.5000 - 0.8
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//<<-811.7045, -984.1961, 20.6538>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-814.5579, -983.6636, 13.0743>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-809.5139, -978.3112, 13.2278>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 123.4800
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.66+0.0000
			
			Private_Fake_NET_REALTY_4_SCENE(scene)
			
			RETURN TRUE
		BREAK
		//Forrest Karbowski, Mon 26/08/2013 23:25
		CASE MP_PROPERTY_BUILDING_20        //Low apt 4
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-665.1188, -847.3347, 26.1009>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<56.3143, 0.0000, -149.4329>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-665.1449, -847.2905, 25.4584>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-6.1796, -0.0000, -145.2366>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 50.0000
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 5.5000 - 0.4
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//<<-664.0032, -853.6744, 30.9325>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-662.7435, -851.4297, 23.4288>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-662.3965, -858.1040, 23.5232>>
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.66+0.0000
			
			Private_Fake_NET_REALTY_4_SCENE(scene)
			
			RETURN TRUE
		BREAK
		
		//Joe Rubino, FRI 20/9/13 21:26
		CASE MP_PROPERTY_BUILDING_21	//Low apt 5
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-1540.0833, -321.7534, 48.0299>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-3.4209, -0.0000, -123.1820>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-1540.5493, -323.2066, 48.0117>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-8.0173, 0.0000, -104.9244>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 37.5853
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 4.9400
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0.0000, 0.0000, 0.0000>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-1536.5195, -324.0229, 46.4567>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-1532.1157, -327.7559, 46.9200>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 48.9600
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<-1535.4785, -324.9415, 46.4819>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.6600
			RETURN TRUE
		BREAK
		
		//Forrest Karbowski, Mon 26/08/2013 23:25
		CASE MP_PROPERTY_BUILDING_22        //Low apt 6
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-1554.0779, -411.0318, 44.6944>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<43.0312, 0.0000, 83.3163>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-1553.9454, -411.0573, 43.3586>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-5.7502, -0.0000, 79.0932>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 44.5739
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 6.5000 - 0.4
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//<<-1561.3810, -412.1974, 48.8890>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-1561.3427, -408.4821, 41.3890>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-1567.6069, -403.7278, 41.3882>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 231.8400
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.66+0.0000
			
			Private_Fake_NET_REALTY_4_SCENE(scene)
			
			RETURN TRUE
		BREAK
		
		//Joe Rubino, Thu 12/09/2013 22:21
		CASE MP_PROPERTY_BUILDING_23        //Low apt 7
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-1614.6853, -422.4536, 49.5392>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<16.4600, -0.0000, -147.0865>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-1614.9767, -422.9563, 47.7871>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-33.5123, 0.0000, -144.5396>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 43.8106
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 5.5000 - 0.66
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//<<-1608.8514, -429.1840, 46.9390>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
            scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-1608.4954, -430.5093, 39.4370>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-1602.3040, -435.7079, 39.4220>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 48.9600
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<-1611.7537, -427.8397, 39.5634>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.66+0.0000
            RETURN TRUE
		BREAK
		
		//Joe Rubino, Fri 20/9/13 22:31
		CASE MP_PROPERTY_BUILDING_8	//Med apt 1
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<276.8639, -161.5447, 65.0431>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-1.6793, 0.0000, -79.3308>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<277.2520, -162.8801, 65.0431>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-1.6793, 0.0000, -69.0721>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 37.5362
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 3.8000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0.0000, 0.0000, 0.0000>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<284.5421, -159.6689, 63.6221>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<289.5417, -162.3463, 64.3987>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 63.8973
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<284.5421, -159.6689, 63.6221>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.6600
			RETURN TRUE
		BREAK
		
		//Joe Rubino - Tue 17/09/2013 22:43
		CASE MP_PROPERTY_BUILDING_9	//Med apt 2
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<4.5824, 23.2871, 86.0296>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-53.7934, 0.0000, 8.2595>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<4.1416, 25.9133, 81.5619>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-47.7373, 0.0000, 9.5287>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 37.7091
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 4.1000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//<<2.8889, 35.7762, 78.0349>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<3.0264, 35.6280, 70.5353>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<4.6261, 39.9890, 70.5326>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 157.3200
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<2.8828, 35.4128, 70.5353>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.6600
			RETURN TRUE
		BREAK
		//Joe Rubino - Tue 17/09/2013 22:43
		CASE MP_PROPERTY_BUILDING_10	//Med apt 3
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<20.6232, 87.0676, 80.5134>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-9.3961, -0.0000, 112.3034>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<19.0930, 89.4920, 80.5134>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-12.1385, -0.0000, 123.8808>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 33.5446
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 4.5000-0.66
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//<<9.7400, 84.6906, 84.8975>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<10.9772, 83.9295, 77.3977>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<8.6638, 79.0654, 77.4448>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 340.9200
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<10.9806, 83.8818, 77.3977>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.6600
			RETURN TRUE
		BREAK
		//Joe Rubino - Tue 17/09/2013 22:43
		CASE MP_PROPERTY_BUILDING_11	//Med apt 4
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-524.0706, 119.4098, 74.7327>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-36.6703, 0.0000, -121.0795>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-522.6709, 118.2410, 73.3864>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-36.6703, -0.0000, -123.2925>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 35.9940
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 4.0000-0.66
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//<<-512.1465, 111.2223, 69.8510>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-511.6992, 109.2992, 62.8006>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-510.8555, 104.0593, 62.8006>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 8.6400
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<-511.6992, 109.2992, 62.8006>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.6600
			RETURN TRUE
		BREAK
		
		//Joe Rubino - Fri 20/9/13 21:48
		CASE MP_PROPERTY_BUILDING_12	//Med apt 5
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-199.7267, 95.2666, 70.8220>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-2.9825, -0.0000, -146.0882>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-199.3750, 93.9299, 70.7064>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-8.5711, -0.0000, -156.9829>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 41.2497
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 4.8400
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0.0000, 0.0000, 0.0000>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-197.5728, 89.4184, 68.6857>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-198.2581, 83.3431, 68.7591>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 349.5600
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<-197.4908, 89.8990, 68.6668>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.6600
			RETURN TRUE
		BREAK

		//Joe Rubino - Fri 20/9/13 21:52
		CASE MP_PROPERTY_BUILDING_13	//Med apt 6
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-632.8743, 165.0295, 61.7998>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-4.4891, -0.0000, -50.0760>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-632.8743, 165.0295, 61.7998>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-6.2407, 0.0000, -33.6965>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 40.8737
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 3.6400
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0.0000, 0.0000, 0.0000>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-630.4570, 169.5246, 60.2563>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-626.3204, 169.9248, 60.1623>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 88.2000
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<-630.4570, 169.5246, 60.2563>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.6600
			RETURN TRUE
		BREAK
		
		//Joe Rubino - Tue 17/09/2013 22:43
		CASE MP_PROPERTY_BUILDING_14	//Med apt 7
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-988.0358, -1438.1102, 12.7701>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-13.3248, 0.0000, -69.8776>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-987.1445, -1437.7834, 12.5081>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-21.6723, 0.0000, -69.8776>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 47.2249
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 4.2000-0.66
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//<<-973.3757, -1429.4247, 14.1791>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-973.1356, -1432.4341, 6.6791>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-967.3808, -1430.2212, 6.7684>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 111.6000
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<-972.9456, -1432.4485, 6.6791>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.6600
			RETURN TRUE
		BREAK
		
		//Joe Rubino - Fri 20/09/2013 22:22
		//----------------------- Private_Get_NET_REALTY_4_SCENE() - MP_PROPERTY_BUILDING_15	//Med apt 8
		CASE MP_PROPERTY_BUILDING_15	//Med apt 8
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-834.7097, -856.9741, 21.4362>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-4.3502, -0.0000, -148.7740>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-834.0713, -856.9313, 21.4116>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-9.4005, 0.0000, -148.1172>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 43.7555
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 3.6400
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0.0000, 0.0000, 0.0000>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-831.6066, -859.9925, 19.6946>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-831.3633, -863.9606, 19.7126>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 0.0000
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<-831.8629, -859.9764, 19.6946>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.6600
			RETURN TRUE
		BREAK

		//Joe Rubino - Fri 20/09/2013 22:22
		CASE MP_PROPERTY_BUILDING_16	//Med apt 9
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-751.8182, -758.0746, 29.8423>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-11.6783, -0.0589, 67.2833>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-751.3698, -756.5670, 29.8342>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-11.0220, -0.0589, 74.6871>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 33.5567
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 3.6400
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0.0000, 0.0000, 0.0000>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-760.9564, -753.8917, 26.8736>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-764.8309, -754.4858, 26.8752>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 270.3600
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<-760.9564, -753.8917, 26.8736>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.6600
			RETURN TRUE
		BREAK
		
		//Forrest Karbowski, Wed 28/08/2013 20:38
		CASE MP_PROPERTY_BUILDING_6          //High apt 14,15
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-936.6873, -393.2527, 39.9279>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-5.4851, -0.0000, -20.4644>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-938.1174, -391.9020, 39.8544>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-7.9852, 0.0000, -18.0689>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 41.2177
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 6.5000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//<<-932.7474, -383.9246, 45.4613>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-936.3890, -386.1136, 37.9613>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-932.6791, -383.5796, 37.9613>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 115.9200
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.66+0.0000
			
			Private_Fake_NET_REALTY_4_SCENE(scene)
			
			RETURN TRUE
		BREAK
		//Joe Rubino, Tue 17/09/2013 19:15
		CASE MP_PROPERTY_BUILDING_7          //High apt 16,17
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-630.8286, 21.9468, 49.2013>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-14.3535, 0.0000, -49.1429>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-628.7056, 19.8417, 49.2013>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-15.9722, 0.0000, -39.9208>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 33.6203
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 5.7000 - 0.5
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//<<-619.1315, 37.8841, 50.0883>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
            scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-615.5634, 33.2746, 42.5307>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-614.7665, 40.6905, 42.5906>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 180.7200
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<-615.7649, 31.3210, 42.5301>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.6600
            RETURN TRUE
		BREAK
		
		//Blake Buck , Thu 29/08/2013 00:35
		CASE MP_PROPERTY_BUILDING_5          //High apt 12,13
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-52.1442, -583.0818, 37.8780>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-0.6010, 0.0000, -146.6836>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-53.0051, -583.8224, 36.6284>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-1.2727, 0.0000, -140.0944>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 50.0000
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 6.5000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//<<-47.3145, -585.9766, 44.4593>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-51.6116, -586.7893, 35.6002>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-46.3888, -588.5487, 37.1670>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 0.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.66+0.0000
			
			Private_Fake_NET_REALTY_4_SCENE(scene)
			
			RETURN TRUE
		BREAK
		
		//Joe Rubino , Thu 29/08/2013 01:24
		CASE MP_PROPERTY_BUILDING_3          //High apt 7,8
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-1440.4741, -574.1239, 46.5883>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-26.4383, -0.0000, -0.7259>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-1440.4498, -572.2144, 45.6387>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-23.8785, 0.0000, 2.6070>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 31.8038
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 6.5000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//<<<-1441.8569, -545.1111, 35.3482>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-12.9633, 0.0000, 166.7685>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-1441.8468, -547.4501, 33.7424>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-1445.2860, -541.5842, 33.7415>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 218.5200
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.66+0.0000
			
			Private_Fake_NET_REALTY_4_SCENE(scene)
			
			RETURN TRUE
		BREAK
		//Joe Rubino , Thu 29/08/2013 01:24
		CASE MP_PROPERTY_BUILDING_4          //High apt 9,10,11
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-914.7648, -481.3169, 39.6928>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-5.0805, 0.0000, 3.7868>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-915.7421, -480.7480, 39.8551>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-5.0805, 0.0000, 0.9599>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 31.0261
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 6.2550 - 1.0
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//<<-913.8500, -455.1392, 46.0999>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-917.0003, -458.0423, 38.5999>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-914.9155, -452.8455, 38.5999>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 169.5600
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.66+0.0000
			
			Private_Fake_NET_REALTY_4_SCENE(scene)
			
			RETURN TRUE
		BREAK
		
		//Joe Rubino , Tue Sep 17 18:30:26 
		CASE MP_PROPERTY_BUILDING_17	//Low apt 1
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-49.8329, -47.6051, 64.3165>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<6.6436, -0.0000, -173.8716>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-48.1860, -47.6119, 64.3158>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-4.3669, 0.0000, -152.5805>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 35.7644
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 4.6000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<0,0,0>>			//<<-45.1289, -57.0925, 69.7531>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-42.8798, -57.8034, 62.4512>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-37.7157, -59.7536, 63.0682>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 68.7600
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<-43.6420, -57.4815, 62.3966>>
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
			scene.bEnablePans[0] = FALSE
			scene.fExitDelay = 0.6600
			RETURN TRUE
		BREAK
		
//		//CDM: 18/6/14 
//		CASE MP_PROPERTY_BUILDING_57	//PROPERTY_IND_DAY_MEDIUM_1
//			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-1502.2263, 514.2335, 124.7684>>
//			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-25.8887, -0.0000, -28.9099>>
//			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-1500.7262, 517.0760, 120.8247>>
//			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-24.6620, -0.0000, -21.6929>>
//			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 50.0422
//			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.2500
//			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 6.5000
//			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<-1500.6051, 523.1385, 124.7722>>
//			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-9.4999, 0.0000, 94.2595>>
//			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
//			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.2500
//			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-1498.6650, 519.6085, 117.2722>>
//			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-1500.6051, 523.1385, 117.2722>>
//			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 0.0000
//			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<-1499.6051, 523.1385, 117.2722>>
//			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
//			scene.bEnablePans[0] = FALSE
//			scene.fExitDelay = 0.6600
//			RETURN TRUE
//		BREAK

		CASE MP_PROPERTY_BUILDING_57        //IND_DAY_MEDIUM_1
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-1406.8398, 526.6141, 124.5208>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-5.1604, 0.0000, -14.7551>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-1406.2139, 526.5825, 124.5119>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-4.8760, 0.0256, 1.7898>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 50.0000
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.0
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 2.7500
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<-1406.2139, 526.5825, 124.5119>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-4.8760, 0.0256, 1.7898>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.0000
            scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-1406.3076, 529.7269, 122.8361>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-1405.4445, 526.5302, 122.8361>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 22.5000
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<-1499.6051, 523.1385, 117.2722>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.6600
            RETURN TRUE
		BREAK
		
		CASE MP_PROPERTY_BUILDING_58        //IND_DAY_MEDIUM_2
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<1340.2795, -1578.7097, 54.8223>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-5.4591, 0.0000, -125.5653>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<1341.6482, -1578.9459, 54.7106>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-2.6626, 0.0000, -132.5410>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 50.0000
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.0
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 3.6750
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<1341.6482, -1578.9459, 54.7106>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-2.6626, 0.0000, -132.5410>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.0
            scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<1344.1912, -1580.7418, 53.0549>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<1341.4930, -1577.8148, 53.4443>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 221.2500
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<1342.5518, -1578.0366, 53.4443>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.6600
            RETURN TRUE
		BREAK
		
		CASE MP_PROPERTY_BUILDING_59        //IND_DAY_MEDIUM_3
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-103.8299, 6528.9521, 30.5451>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-4.7791, -0.0000, 50.8526>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-105.1552, 6528.6353, 30.5075>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-4.9733, 0.0000, 23.5478>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 50.0000
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.0
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 4.0000
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<-105.1552, 6528.6353, 30.5075>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-4.9733, 0.0000, 23.5478>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.0
            scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-106.5943, 6532.0054, 28.8582>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-105.8078, 6528.3330, 29.3718>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 0.0000
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<-14.2580, 6000.0000, 32.2454>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.6600
            RETURN TRUE
		BREAK

		CASE MP_PROPERTY_BUILDING_60        //IND_DAY_MEDIUM_4
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-300.4659, 6328.5054, 33.2355>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-5.0230, 0.0000, 64.9785>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-302.0653, 6328.3496, 33.1190>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-4.5385, -0.0000, 51.7653>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 50.0000
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.0
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 3.7750
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<-302.0653, 6328.3496, 33.1190>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-4.5385, -0.0000, 51.7653>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.0
            scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-304.5024, 6330.1440, 31.4893>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-301.7189, 6327.2710, 31.8926>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 45.0000
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<-301.3985, 6000.0000, 31.8918>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.6600
            RETURN TRUE
		BREAK
		
		CASE MP_PROPERTY_BUILDING_61        //IND_DAY_LOW_1
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<-16.0685, 6558.2686, 33.6771>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-8.6469, -0.0000, -81.0994>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<-14.5951, 6558.5684, 33.4494>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-20.5931, -0.0000, -59.6802>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 50.0000
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.0
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 4.0000
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<-14.5951, 6558.5684, 33.4494>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-20.5931, -0.0000, -59.6802>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.0
            scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<-12.4015, 6560.2305, 30.9710>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<-15.6693, 6557.3003, 32.2455>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 311.2500
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<-14.2580, 6000.0000, 32.2454>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.6600
            RETURN TRUE
		BREAK
		
        CASE MP_PROPERTY_BUILDING_62        //IND_DAY_LOW_2
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<1899.5564, 3780.1938, 33.2782>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-2.2800, 0.0000, -30.6051>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<1899.1544, 3781.1560, 33.2566>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-2.2800, 0.0000, -43.9225>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 50.0000
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.0
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 3.5000
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<1899.1544, 3781.1560, 33.2566>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-2.2800, 0.0000, -43.9225>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.0
            scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<1901.0154, 3783.2876, 31.7961>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<1898.5573, 3781.5977, 31.9072>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 303.7500
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<1900.6729, 3773.1785, 31.8829>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.6600
            RETURN TRUE
		BREAK

		CASE MP_PROPERTY_BUILDING_63        //IND_DAY_LOW_3
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = <<1663.3756, 4777.4126, 43.0227>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<-10.7709, 0.0000, -165.2197>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = <<1662.7836, 4775.8545, 42.7935>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-7.8945, -0.0000, -145.3202>>
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = 50.0000
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.0
            scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 4.0000
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = <<1662.7836, 4775.8545, 42.7935>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = <<-7.8945, -0.0000, -145.3202>>
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = 50.0000
            scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = 0.0
            scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = <<1664.7622, 4772.8311, 40.9904>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = <<1661.9733, 4776.7261, 41.0075>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 213.7500
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos = <<1663.1211, 4776.3169, 41.0075>>
            scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot = 4.0000
            scene.bEnablePans[0] = FALSE
            scene.fExitDelay = 0.6600
            RETURN TRUE
		BREAK
		
		DEFAULT
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vPos = GET_MP_PROPERTY_BUILDING_WORLD_POINT(iBuildingID)			+ <<0.0, 0.0, 5.0>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mStart.vRot = <<89.4999, -0.0000, 94.2595>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos = GET_MP_PROPERTY_BUILDING_WORLD_POINT(iBuildingID)			+ <<0.0, 0.0, 7.5>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot = <<-9.4999, -0.0000, 94.2595>>
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov = GET_GAMEPLAY_CAM_FOV()
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake = 0.25
			scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration = 6.500
			scene.bEnablePans[NET_REALTY_4_SCENE_PAN_walkOut] = FALSE
			
			scene.fExitDelay = 0.66+0.0000
			
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos = scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vPos
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vRot = scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].mEnd.vRot
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fFov = scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fFov
			scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].fShake = scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fShake
			
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos = GET_MP_PROPERTY_BUILDING_WORLD_POINT(iBuildingID)
			scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot = 0
			
			scene.mMarkers[NET_REALTY_4_SCENE_PAN_walkOut].vPos = scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos			+ <<1.0, 0.0, 0.0>>
			
			Private_Fake_NET_REALTY_4_SCENE(scene)
			
			#IF ACTIVATE_PROPERTY_CS_DEBUG
			IF NOT bIgnoreAssert
				TEXT_LABEL_63 str
				str = "Private_Get_NET_REALTY_4_SCENE() invalid ID "
				str += GET_STRING_FROM_STRING(Private_Get_Net_Realty_Building_Name(iBuildingID), GET_LENGTH_OF_LITERAL_STRING("MP_PROPERTY_"), GET_LENGTH_OF_LITERAL_STRING(Private_Get_Net_Realty_Building_Name(iBuildingID)))
				SCRIPT_ASSERT(str)
			ENDIF
			#ENDIF
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


//PURPOSE: Controls the player walking out of a property through a door
FUNC BOOL WALK_OUT_OF_THIS_PROPERTY(STRUCT_NET_REALTY_4_SCENE &scene, INT iCurrentProperty,
		INT &iWalkOutStage,
		CAMERA_INDEX &WalkOutCam0, CAMERA_INDEX &WalkOutCam1,
		SCRIPT_TIMER &iWalkOutTimer,
		SCRIPT_TIMER &iWalkOutCamInterpDelayTimer,
		SCRIPT_TIMER &iWalkOutCamEndTimer, BOOL bAllExit)
	
	CONST_INT WALK_OUT_TIMEOUT	10000
	VECTOR vWalkToOffset
	//Check to see if we should cleanup
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
	OR IS_PLAYER_SWITCH_IN_PROGRESS()
		iWalkOutStage = WOS_CLEANUP
		PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - iWalkOutStage = WOS_CLEANUP - A")
	ENDIF
	
	BOOL bWarpWhenBlocked
	//Debug End
	/*#IF IS_DEBUG_BUILD
	IF bEndPlayerWalkOut
		iWalkOutStage = WOS_CLEANUP
		PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - iWalkOutStage = WOS_CLEANUP - C")
	ENDIF
	#ENDIF*/
	INT iPositionIndex[2]
	FLOAT fDistance = 0.3
	MP_PROPERTY_NON_AXIS_DETAILS entranceArea
	
	//Process Stages
	SWITCH iWalkOutStage
		CASE WOS_INIT
		
			//  #1574802 //  //  //  //  //  //
//			VECTOR vPoint, vMin, vMax
//			
//			vPoint = scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos
//			vPoint.z += AREA_OCCUPIED_RADIUS
//			
//			vMin = vPoint + (<< AREA_OCCUPIED_RADIUS, AREA_OCCUPIED_RADIUS, 0.5 * AREA_OCCUPIED_RADIUS>>) * -1.0
//			vMax = vPoint + (<< AREA_OCCUPIED_RADIUS, AREA_OCCUPIED_RADIUS, 0.5 * AREA_OCCUPIED_RADIUS>>)
			
	
			
			
			IF IS_POSITION_OCCUPIED(
					scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].vPos,
					scene.mPlacers[NET_REALTY_4_SCENE_PLACER_occupiedSphere].fRot,
					FALSE, TRUE, FALSE, FALSE, FALSE)
			OR (GET_PROPERTY_SIZE_TYPE(mpProperties[iCurrentProperty].iIndex) != PROP_SIZE_TYPE_LARGE_APT
			AND bAllExit)
				//SCRIPT_ASSERT("area '4' occupied!")
				PRINTLN("area '4' occupied!")
				IF (GET_PROPERTY_SIZE_TYPE(mpProperties[iCurrentProperty].iIndex) != PROP_SIZE_TYPE_LARGE_APT
				AND bAllExit)
					PRINTLN("all exit for low/med!")
				ENDIF
//				IF NOT IS_SCREEN_FADED_OUT()
//				AND NOT IS_SCREEN_FADING_OUT()
//					DO_SCREEN_FADE_OUT(500)
//				ENDIF
				
//				IF mpProperties[iCurrentProperty].entrance[iEntrancePlayerIsUsing].iType = ENTRANCE_TYPE_GARAGE 
//					SET_PLAYER_IN_PROPERTY(FALSE,TRUE,FALSE)  
//					SETUP_SPECIFIC_SPAWN_LOCATION(mpProperties[iCurrentProperty].garage.vExitPlayerPos,mpProperties[iCurrentProperty].garage.fExitPlayerHeading, 10, FALSE)
//				ELSE 
					SET_PLAYER_IN_PROPERTY(FALSE,FALSE,FALSE)
					IF iCurrentProperty != 0
						//
					ENDIF
					
//				ENDIF
				SETUP_SPECIFIC_SPAWN_LOCATION(mpProperties[iCurrentProperty].house.exits[0].vOutPlayerLoc,mpProperties[iCurrentProperty].house.exits[0].fOutPlayerHeading, 10, FALSE)
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					iWalkOutStage = WOS_SET_IN_UNOCCUPIED_AREA
					PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - iWalkOutStage = WOS_SET_IN_UNOCCUPIED_AREA")
				ELSE
					
					iWalkOutStage = WOS_WARP_TO_UNOCCUPIED_AREA
					PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - iWalkOutStage = WOS_WARP_TO_UNOCCUPIED_AREA")
				ENDIF
				RETURN FALSE
			ENDIF
			//  //  //  //  //  //  //  //  //
			
			//Create Camera
//			IF NOT DOES_CAM_EXIST(WalkOutCam)
//				WalkOutCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
//				SET_CAM_ACTIVE(WalkOutCam, TRUE)
//				//SET_CAM_PARAMS(WalkOutCam,  <<-777.239197,315.293304,86.445915>>,<<-0.848446,-0.000000,172.837280>>,50.0)
//				
//				SET_CAM_PARAMS(WalkOutCam,  <<-779.846680,307.215790,86.100006>>,<<2.534921,0.000000,-22.975626>>, 40.0)
//				SET_CAM_PARAMS(WalkOutCam,  <<-778.645813,306.863953,86.106171>>,<<1.323177,-0.000000,-20.878082>>, 	40.0, 5000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			ENDIF
			SceneTool_ExecutePan(scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut], WalkOutCam0, WalkOutCam1)
			
			//Temp warp player
			SET_ENTITY_HEADING(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].fRot)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), scene.mPlacers[NET_REALTY_4_SCENE_PLACER_resetCoords].vPos)
			
			//SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SearchForClosestDoor, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OpenDoorArmIK, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_AllowOpenDoorIkBeforeFullMovement, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IgnoreNavigationForDoorArmIK, TRUE)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			REINIT_NET_TIMER(iWalkOutCamInterpDelayTimer)
			REINIT_NET_TIMER(iWalkOutCamEndTimer)
			
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(500)
			ENDIF
			
			iWalkOutStage = WOS_WAIT_FOR_DOOR_CONTROL
			PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - iWalkOutStage = WOS_WAIT_FOR_DOOR_CONTROL - A")
		BREAK
		
		CASE WOS_WAIT_FOR_DOOR_CONTROL
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			iWalkOutStage = WOS_GIVE_SEQUENCE
			PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - iWalkOutStage = WOS_GIVE_SEQUENCE - A")
		BREAK
		
		CASE WOS_GIVE_SEQUENCE
			
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE)
//			iPositionIndex[0] =  16
//			
//			IF iPositionIndex[0] > 0
//				IF iPositionIndex[0]%2 = 0
//					//+x
//					iPositionIndex[1] = CEIL(TO_FLOAT(iPositionIndex[0])/4)
//					vWalkToOffset.x = iPositionIndex[1]*fDistance
//					IF iPositionIndex[0]%4 = 0
//						//+y
//						vWalkToOffset.y = fDistance
//					ELSE
//						//-y
//						vWalkToOffset.y = -fDistance
//					ENDIF
//				ELSE
//					//-x
//					iPositionIndex[1] = CEIL(TO_FLOAT(iPositionIndex[0])/4)
//					vWalkToOffset.x = iPositionIndex[1]*-fDistance
//					IF iPositionIndex[0]%4 = 3
//						//+y
//						vWalkToOffset.y = fDistance
//					ELSE
//						//-y
//						vWalkToOffset.y = -fDistance
//					ENDIF
//				ENDIF
//			ENDIF
//			scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos + vWalkToOffset
			//SEQUENCE_INDEX siTemp
			
			//OPEN_SEQUENCE_TASK(siTemp)
				TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos, PEDMOVEBLENDRATIO_WALK, -1)	//-777.0496, 309.7655, 84.6992
			//CLOSE_SEQUENCE_TASK(siTemp)
			//TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), siTemp)
			PRINTLN("WALK_OUT_OF_PROPERTY: Told player to walk to: ",scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos)
			
			//SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SearchForClosestDoor, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OpenDoorArmIK, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_AllowOpenDoorIkBeforeFullMovement, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IgnoreNavigationForDoorArmIK, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DontWalkRoundObjects, TRUE)
			
			REINIT_NET_TIMER(iWalkOutTimer)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			iWalkOutStage = WOS_WAIT_FOR_SEQUENCE_END
			PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - iWalkOutStage = WOS_WAIT_FOR_SEQUENCE_END - A")
		BREAK
		
		CASE WOS_WAIT_FOR_SEQUENCE_END

			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SearchForClosestDoor, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OpenDoorArmIK, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_AllowOpenDoorIkBeforeFullMovement, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IgnoreNavigationForDoorArmIK, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DontWalkRoundObjects, TRUE)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			//INTERP TO GAME CAM
			/*IF bInterpStartStarted = FALSE
				IF HAS_NET_TIMER_EXPIRED(iWalkOutCamInterpDelayTimer, 0)
					RENDER_SCRIPT_CAMS(FALSE, TRUE, 2000)
					bInterpStartStarted = TRUE
					REINIT_NET_TIMER(iWalkOutCamEndTimer)
					PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - bInterpStartStarted = TRUE")
				ENDIF
			ELSE*/
			entranceArea = GET_BUILDING_EXT_ENTRANCE_AREA(GET_PROPERTY_BUILDING(iCurrentProperty),0)
			
			IF NOT IS_ENTITY_IN_ANGLED_AREA(GET_PLAYER_PED(PLAYER_ID()),entranceArea.vPos1,entranceArea.vPos2,entranceArea.fWidth)
			AND NOT IS_PROPERTY_MISSING_OPENING_DOORS(iCurrentProperty)
				iWalkOutStage = WOS_RETURN_CONTROL_GRACEFULLY
				RESET_NET_TIMER(iWalkOutTimer)
				RESET_NET_TIMER(iWalkOutCamEndTimer)
				PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - iWalkOutStage = WOS_RETURN_CONTROL_GRACEFULLY -RT")
//				IF HAS_NET_TIMER_EXPIRED(iWalkOutTimer, 1000,TRUE)
//					iPositionIndex[0] =  16
//				
//					IF iPositionIndex[0] > 0
//						IF iPositionIndex[0]%2 = 0
//							//+x
//							iPositionIndex[1] = CEIL(TO_FLOAT(iPositionIndex[0])/4)
//							vWalkToOffset.x = iPositionIndex[1]*fDistance
//							IF iPositionIndex[0]%4 = 0
//								//+y
//								vWalkToOffset.y = fDistance
//							ELSE
//								//-y
//								vWalkToOffset.y = -fDistance
//							ENDIF
//						ELSE
//							//-x
//							iPositionIndex[1] = CEIL(TO_FLOAT(iPositionIndex[0])/4)
//							vWalkToOffset.x = iPositionIndex[1]*-fDistance
//							IF iPositionIndex[0]%4 = 3
//								//+y
//								vWalkToOffset.y = fDistance
//							ELSE
//								//-y
//								vWalkToOffset.y = -fDistance
//							ENDIF
//						ENDIF
//					ENDIF
//					scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos + vWalkToOffset
//					TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos, PEDMOVEBLENDRATIO_WALK, -1)
//					iWalkOutStage = WOS_CLEANUP
//					PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - iWalkOutStage = WOS_CLEANUP - TX")
//				ELSE
//					PRINTLN("WALK_OUT_OF_PROPERTY-Out of area waiting for timer.")			
//				ENDIF
			ELSE
				FLOAT fWALK_OUT_duration	//5000
				fWALK_OUT_duration  = scene.mPans[NET_REALTY_4_SCENE_PAN_walkOut].fDuration
				fWALK_OUT_duration += scene.fExitDelay
				IF HAS_NET_TIMER_EXPIRED(iWalkOutCamEndTimer, ROUND(fWALK_OUT_duration * 1000.0))
					
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) != WAITING_TO_START_TASK
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
						//SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, FALSE)
						
						IF NOT ARE_VECTORS_EQUAL(scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos, <<0,0,0>>)
							SceneTool_ExecuteCut(scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup], WalkOutCam0)
						ELSE
							WalkOutCam0 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
							
							ATTACH_CAM_TO_ENTITY(WalkOutCam0, PLAYER_PED_ID(), <<0.2624, -0.8617, 0.5789>>)
							POINT_CAM_AT_ENTITY(WalkOutCam0, PLAYER_PED_ID(), <<0.1000, 2.1250, 0.3471>>)
							SET_CAM_FOV(WalkOutCam0, 50.0000)
							
							SET_CAM_ACTIVE(WalkOutCam0, TRUE)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
						ENDIF
						IF DOES_CAM_EXIST(WalkOutCam1)
							IF IS_CAM_ACTIVE(WalkOutCam1)
								SET_CAM_ACTIVE(WalkOutCam1, FALSE)
							ENDIF
							DESTROY_CAM(WalkOutCam1)
						ENDIF
						
						iWalkOutStage = WOS_CLEANUP
						PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - iWalkOutStage = WOS_CLEANUP - B")
					ELIF HAS_NET_TIMER_EXPIRED(iWalkOutTimer, WALK_OUT_TIMEOUT,TRUE)
						//SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, FALSE) 
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						
						IF NOT ARE_VECTORS_EQUAL(scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup].mCam.vPos, <<0,0,0>>)
							SceneTool_ExecuteCut(scene.mCuts[NET_REALTY_4_SCENE_CUT_catchup], WalkOutCam0)
						ELSE
							WalkOutCam0 = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
							
							ATTACH_CAM_TO_ENTITY(WalkOutCam0, PLAYER_PED_ID(), <<0.2624, -0.8617, 0.5789>>)
							POINT_CAM_AT_ENTITY(WalkOutCam0, PLAYER_PED_ID(), <<0.1000, 2.1250, 0.3471>>)
							SET_CAM_FOV(WalkOutCam0, 50.0000)
							
							SET_CAM_ACTIVE(WalkOutCam0, TRUE)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
						ENDIF
						IF DOES_CAM_EXIST(WalkOutCam1)
							IF IS_CAM_ACTIVE(WalkOutCam1)
								SET_CAM_ACTIVE(WalkOutCam1, FALSE)
							ENDIF
							DESTROY_CAM(WalkOutCam1)
						ENDIF
						
						iWalkOutStage = WOS_CLEANUP
						PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - iWalkOutStage = WOS_CLEANUP - BX")
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE WOS_RETURN_CONTROL_GRACEFULLY
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SearchForClosestDoor, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_OpenDoorArmIK, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_AllowOpenDoorIkBeforeFullMovement, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IgnoreNavigationForDoorArmIK, TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DontWalkRoundObjects, TRUE)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			
			entranceArea = GET_BUILDING_EXT_ENTRANCE_AREA(GET_PROPERTY_BUILDING(iCurrentProperty),0)
			IF NOT HAS_NET_TIMER_STARTED(iWalkOutCamEndTimer)
			
				IF HAS_NET_TIMER_EXPIRED(iWalkOutTimer, 200,TRUE)
					vWalkToOffset = <<0,0,0>>
					IF bAllExit
						iPositionIndex[0] =  PARTICIPANT_ID_TO_INT()
					
						IF iPositionIndex[0] > 0
							IF iPositionIndex[0]%2 = 0
								//+x
								iPositionIndex[1] = CEIL(TO_FLOAT(iPositionIndex[0])/4)
								vWalkToOffset.x = iPositionIndex[1]*fDistance
								IF iPositionIndex[0]%4 = 0
									//+y
									vWalkToOffset.y = fDistance
								ELSE
									//-y
									vWalkToOffset.y = -fDistance
								ENDIF
							ELSE
								//-x
								iPositionIndex[1] = CEIL(TO_FLOAT(iPositionIndex[0])/4)
								vWalkToOffset.x = iPositionIndex[1]*-fDistance
								IF iPositionIndex[0]%4 = 3
									//+y
									vWalkToOffset.y = fDistance
								ELSE
									//-y
									vWalkToOffset.y = -fDistance
								ENDIF
							ENDIF
							PRINTLN("WALK_OUT_OF_PROPERTY: applying offset for occupied area")
						ENDIF
					ENDIF
					REINIT_NET_TIMER(iWalkOutCamEndTimer,TRUE)
					
					scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos = scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos + vWalkToOffset
					
					//GET_HEADING_BETWEEN_VECTORS_2D(GET_PLAYER_COORDS(PLAYER_ID()),scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos)
					TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos, PEDMOVEBLENDRATIO_WALK, -1)
					
					PRINTLN("WALK_OUT_OF_PROPERTY: SIMULATE_PLAYER_INPUT_GAIT: ",scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos )
				ELSE
					PRINTLN("WALK_OUT_OF_PROPERTY-Out of area waiting for timer.")			
				ENDIF
			ELSE
				IF HAS_NET_TIMER_EXPIRED(iWalkOutTimer, 500,TRUE)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000, GET_HEADING_BETWEEN_VECTORS_2D(GET_PLAYER_COORDS(PLAYER_ID()),scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos),FALSE)
					iWalkOutStage = WOS_CLEANUP
					PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - iWalkOutStage = WOS_CLEANUP - BGW")	
				ENDIF
			ENDIF
		BREAK
		
		CASE WOS_WARP_TO_UNOCCUPIED_AREA
		CASE WOS_SET_IN_UNOCCUPIED_AREA
//			IF IS_SCREEN_FADED_OUT()
				IF iWalkOutStage = WOS_SET_IN_UNOCCUPIED_AREA
					PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - IN HERE-")
					bWarpWhenBlocked = TRUE
					IF (NOT IS_NEW_LOAD_SCENE_ACTIVE() OR IS_NEW_LOAD_SCENE_LOADED())
						PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - IF (NOT IS_NEW_LOAD_SCENE_ACTIVE() OR NOT IS_NEW_LOAD_SCENE_LOADED()) -")
					ELSE
						PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - waiting for new load scene to be finished before setting player coords -")
						RETURN FALSE
					ENDIF
				ENDIF
				IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE, FALSE, FALSE, FALSE,bWarpWhenBlocked)
					CLEAR_SPECIFIC_SPAWN_LOCATION()
					IF NOT IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					iWalkOutStage = WOS_CLEANUP
					PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - iWalkOutStage = WOS_CLEANUP - C")
				ELSE
					PRINTLN("     ----->>>>>     WALK_OUT_OF_PROPERTY - Trying to warp to spawn location-")
				ENDIF
//			ENDIF
		BREAK
				
		CASE WOS_CLEANUP
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),entranceArea.vPos1,entranceArea.vPos2,entranceArea.fWidth)
				SET_ENTITY_COORDS(PLAYER_PED_ID() ,scene.mMarkers[NET_REALTY_4_SCENE_MARKER_walkTo].vPos)
				PRINTLN("CDM: warping player still in entrance area.")
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ELSE
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ENDIF
			iWalkOutStage = WOS_INIT
			
			IF DOES_CAM_EXIST(WalkOutCam0) AND NOT DOES_CAM_EXIST(WalkOutCam1)
				//IF NOT IS_PROPERTY_MISSING_OPENING_DOORS(iCurrentProperty)
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
				//ENDIF
				IF DOES_CAM_EXIST(WalkOutCam0)
					IF IS_CAM_ACTIVE(WalkOutCam0)
						SET_CAM_ACTIVE(WalkOutCam0, FALSE)
					ENDIF
					DESTROY_CAM(WalkOutCam0)
				ENDIF
				IF DOES_CAM_EXIST(WalkOutCam1)
					IF IS_CAM_ACTIVE(WalkOutCam1)
						SET_CAM_ACTIVE(WalkOutCam1, FALSE)
					ENDIF
					DESTROY_CAM(WalkOutCam1)
				ENDIF
			ELSE
				IF DOES_CAM_EXIST(WalkOutCam0)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					IF IS_CAM_ACTIVE(WalkOutCam0)
						SET_CAM_ACTIVE(WalkOutCam0, FALSE)
					ENDIF
					DESTROY_CAM(WalkOutCam0)
				ENDIF
				IF DOES_CAM_EXIST(WalkOutCam1)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					IF IS_CAM_ACTIVE(WalkOutCam1)
						SET_CAM_ACTIVE(WalkOutCam1, FALSE)
					ENDIF
					DESTROY_CAM(WalkOutCam1)
				ENDIF
			ENDIF
			
			//bInterpStartStarted = FALSE
			
			//SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_OpenDoorArmIK, FALSE)
			
			/*#IF IS_DEBUG_BUILD
			bDoPlayerWalkOut = FALSE
			bEndPlayerWalkOut = FALSE
			#ENDIF*/
			
			PRINTLN("     ----->>>>>     WALK_OUT_CLEANUP - DONE")
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


#IF ACTIVATE_PROPERTY_CS_DEBUG
USING "NET_REALTY_4_SCENE_debug.sch"
#ENDIF
