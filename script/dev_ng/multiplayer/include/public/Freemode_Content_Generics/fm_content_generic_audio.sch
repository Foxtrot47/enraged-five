USING "fm_content_generic_support.sch"


//----------------------
//	GENERIC OVERRIDES
//----------------------

#IF NOT DEFINED(MISSION_SHOULD_MAINTAIN_MUSIC_TRIGGERS)
FUNC BOOL MISSION_SHOULD_MAINTAIN_MUSIC_TRIGGERS()
	RETURN TRUE
ENDFUNC
#ENDIF

//////////////////////
/// SOUND FX
//////////////////////

#IF MAX_NUM_SOUND_TRIGGERS

PROC SET_SOUND_AS_TRIGGERED(INT iSound)
	IF NOT IS_ARRAYED_BIT_SET(sSoundTriggerLocal.iBitset, iSound)
		SET_ARRAYED_BIT(sSoundTriggerLocal.iBitset, iSound)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [SOUND_TRIGGER_", iSound,"] SET_SOUND_AS_TRIGGERED")
	ENDIF
ENDPROC

PROC CLEAR_SOUND_AS_TRIGGERED(INT iSound)
	IF IS_ARRAYED_BIT_SET(sSoundTriggerLocal.iBitset, iSound)
		CLEAR_ARRAYED_BIT(sSoundTriggerLocal.iBitset, iSound)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [SOUND_TRIGGER_", iSound,"] CLEAR_SOUND_AS_TRIGGERED")
	ENDIF
ENDPROC

PROC CLEANUP_SOUND_TRIGGERS()
	INT iSound
	REPEAT MAX_NUM_SOUND_TRIGGERS iSound
		STOP_AND_RELEASE_SOUND(sSoundTriggerLocal.iSoundId[iSound])
		CLEAR_SOUND_AS_TRIGGERED(iSound)
	ENDREPEAT
ENDPROC

PROC MAINTAIN_SOUND_TRIGGERS()
	INT iSound
	REPEAT MAX_NUM_SOUND_TRIGGERS iSound
		IF logic.Audio.SoundTrigger.Trigger != NULL
		AND CALL logic.Audio.SoundTrigger.Trigger(iSound)
			IF NOT HAS_SOUND_BEEN_TRIGGERED(iSound)
				IF logic.Audio.SoundTrigger.Play != NULL
				AND CALL logic.Audio.SoundTrigger.Play(sSoundTriggerLocal.iSoundId[iSound], iSound)
					SET_SOUND_AS_TRIGGERED(iSound)
					EXIT
				ENDIF
			ELSE
				IF logic.Audio.SoundTrigger.ShouldLoop != null
				AND CALL logic.Audio.SoundTrigger.ShouldLoop(iSound)
					IF HAS_SOUND_FINISHED(sSoundTriggerLocal.iSoundId[iSound])
						STOP_AND_RELEASE_SOUND(sSoundTriggerLocal.iSoundId[iSound])
						CLEAR_SOUND_AS_TRIGGERED(iSound)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF HAS_SOUND_BEEN_TRIGGERED(iSound)
				STOP_AND_RELEASE_SOUND(sSoundTriggerLocal.iSoundId[iSound])
				CLEAR_SOUND_AS_TRIGGERED(iSound)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

#ENDIF

//////////////////////
/// MUSIC
//////////////////////

#IF MAX_NUM_MUSIC_TRIGGERS

PROC PLAY_MUSIC_EVENT(INT iMusicEvent)
	IF logic.Audio.MusicTrigger.EventString != null
		IF NOT IS_STRING_NULL_OR_EMPTY(CALL logic.Audio.MusicTrigger.EventString(iMusicEvent))
			TRIGGER_MUSIC_EVENT(CALL logic.Audio.MusicTrigger.EventString(iMusicEvent))
			
			#IF IS_DEBUG_BUILD
			sDebugVars.MusicTriggers.tl63CurrentTrigger = CALL logic.Audio.MusicTrigger.EventString(iMusicEvent)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [MUSIC_EVENT_", iMusicEvent,"] PLAY_MUSIC_EVENT - Triggered ", sDebugVars.MusicTriggers.tl63CurrentTrigger)
			#ENDIF
		ELSE
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [MUSIC_EVENT_", iMusicEvent,"] PLAY_MUSIC_EVENT - No event string set up for index.")
			ASSERTLN ("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [MUSIC_TRIGGER] SET_MUSIC_TRIGGER - No event string set up. String is empty.")
		ENDIF
	ELSE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [MUSIC_EVENT_", iMusicEvent,"] PLAY_MUSIC_EVENT - No event strings set up. logic.Audio.MusicTrigger.EventString = null")
		ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [MUSIC_TRIGGER] PLAY_MUSIC_EVENT - No event strings set up. logic.Audio.MusicTrigger.EventString = null")
	ENDIF
ENDPROC

PROC SETUP_AUDIO_FLAGS(BOOL bSet)
	IF bSet
		IF NOT IS_GENERIC_BIT_SET(eGENERICBITSET_SETUP_AUDIO_FLAGS)
			SET_AUDIO_FLAG("DisableFlightMusic", TRUE) 
			SET_AUDIO_FLAG("WantedMusicDisabled", TRUE)
			SET_GENERIC_BIT(eGENERICBITSET_SETUP_AUDIO_FLAGS)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] DisableFlightMusic and WantedMusicDisabled set to TRUE.")
		ENDIF
	ELSE
		IF IS_GENERIC_BIT_SET(eGENERICBITSET_SETUP_AUDIO_FLAGS)
			SET_AUDIO_FLAG("DisableFlightMusic", FALSE) 
			SET_AUDIO_FLAG("WantedMusicDisabled", FALSE)
			CLEAR_GENERIC_BIT(eGENERICBITSET_SETUP_AUDIO_FLAGS)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] DisableFlightMusic and WantedMusicDisabled set to FALSE.")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_MUSIC_SAFETY_CHECK()

	IF logic.Audio.MusicTrigger.DoSafetyCheck != null
		RETURN CALL logic.Audio.MusicTrigger.DoSafetyCheck()
	ENDIF

	RETURN TRUE
ENDFUNC

PROC MAINTAIN_MUSIC_TRIGGERS()

//	IF NOT bSafeToDisplay
//	AND DO_MUSIC_SAFETY_CHECK()
//		IF sMusicTriggerLocal.iCurrentEvent > 0
//			SETUP_AUDIO_FLAGS(FALSE)
//			
//			IF logic.Audio.MusicTrigger.StopString != null
//				TRIGGER_MUSIC_EVENT(CALL logic.Audio.MusicTrigger.StopString())
//				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [MUSIC_TRIGGER] MAINTAIN_MUSIC_TRIGGERS - IS_SAFE_FOR_CLIENT_MODE_STATE_LOGIC = FALSE - Triggered ", CALL logic.Audio.MusicTrigger.StopString())
//			ENDIF
//		ENDIF
//		EXIT
//	ENDIF

	SETUP_AUDIO_FLAGS(TRUE)

	IF NOT MISSION_SHOULD_MAINTAIN_MUSIC_TRIGGERS()
		EXIT
	ENDIF

	IF logic.Audio.MusicTrigger.Event != null
		INT iNextEvent = CALL logic.Audio.MusicTrigger.Event(sMusicTriggerLocal.iCurrentEvent)
		IF iNextEvent != sMusicTriggerLocal.iCurrentEvent AND iNextEvent != -1
			sMusicTriggerLocal.iCurrentEvent = iNextEvent
			PLAY_MUSIC_EVENT(sMusicTriggerLocal.iCurrentEvent)
		ENDIF
	ENDIF
ENDPROC

#IF NOT DEFINED(GET_MISSION_MUSIC_STOP_STRING)
FUNC STRING GET_MISSION_MUSIC_STOP_STRING()
	RETURN ""
ENDFUNC
#ENDIF

#IF NOT DEFINED(GET_MISSION_MUSIC_FAIL_STRING)
FUNC STRING GET_MISSION_MUSIC_FAIL_STRING()
	RETURN ""
ENDFUNC
#ENDIF

FUNC STRING GET_MUSIC_STOP_STRING()

	IF logic.Audio.MusicTrigger.StopString != null
		RETURN CALL logic.Audio.MusicTrigger.StopString()
	ENDIF
	
	RETURN GET_MISSION_MUSIC_STOP_STRING()
	
ENDFUNC

FUNC STRING GET_MUSIC_FAIL_STRING()

	IF logic.Audio.MusicTrigger.FailString != null
		RETURN CALL logic.Audio.MusicTrigger.FailString()
	ENDIF
	
	RETURN GET_MISSION_MUSIC_FAIL_STRING()
	
ENDFUNC

PROC TRIGGER_MUSIC_EVENT_ON_END()

	STRING sMusic

	IF IS_GENERIC_BIT_SET(eGENERICBITSET_I_WON)
		sMusic = GET_MUSIC_STOP_STRING()
	ELSE
		sMusic = GET_MUSIC_FAIL_STRING()
	ENDIF
	
	IF sMusicTriggerLocal.iCurrentEvent != (-1)
		IF NOT IS_STRING_NULL_OR_EMPTY(sMusic)
			TRIGGER_MUSIC_EVENT(sMusic)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [MUSIC_TRIGGER] TRIGGER_MUSIC_EVENT_ON_END - Triggered ", sMusic)
		ELSE
			ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [MUSIC_TRIGGER] TRIGGER_MUSIC_EVENT_ON_END - No stop/fail string set up. sMusic = null")
		ENDIF
	ELSE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [MUSIC_TRIGGER] TRIGGER_MUSIC_EVENT_ON_END - sMusicTriggerLocal.iCurrentEvent = -1, no need for end strings.")
	ENDIF
	
	SETUP_AUDIO_FLAGS(FALSE)
ENDPROC

#ENDIF

PROC MAINTAIN_AUDIO()
	#IF MAX_NUM_SOUND_TRIGGERS 			MAINTAIN_SOUND_TRIGGERS()		#ENDIF
	#IF MAX_NUM_MUSIC_TRIGGERS 			MAINTAIN_MUSIC_TRIGGERS()		#ENDIF
ENDPROC
