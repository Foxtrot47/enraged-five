USING "fm_content_generic_support.sch"

//----------------------
//	Mission Pickups
//----------------------

#IF MAX_NUM_PICKUPS

FUNC BOOL SHOULD_MAINTAIN_MISSION_PICKUPS()
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CREATE_MISSION_PICKUP(INT iPickup)
	UNUSED_PARAMETER(iPickup)
	RETURN (GET_CLIENT_MODE_STATE() = eMODESTATE_COLLECT_PICKUP)
ENDFUNC

FUNC VECTOR GET_MISSION_PICKUP_COORDS(INT iPickup)
	IF logic.Pickup.Coords != NULL
		RETURN CALL logic.Pickup.Coords(iPickup)
	ENDIF
	
	INT iEntity = data.Pickup.Pickups[iPickup].iEntity
	IF iEntity != -1
		SWITCH data.Pickup.Pickups[iPickup].eEntityType
			CASE ET_PED
				RETURN GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sPed[iEntity].netId))
			CASE ET_VEHICLE
				RETURN GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sVehicle[iEntity].netId))
			CASE ET_OBJECT
				RETURN GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sProp[iEntity].netId))
		ENDSWITCH
	ENDIF
	
	RETURN data.Pickup.Pickups[iPickup].vCoords
ENDFUNC

FUNC FLOAT GET_MISSION_PICKUP_HEADING(INT iPickup)
	IF logic.Pickup.Heading != NULL
		RETURN CALL logic.Pickup.Heading(iPickup)
	ENDIF

	RETURN data.Pickup.Pickups[iPickup].fHeading
ENDFUNC

FUNC VECTOR GET_MISSION_PICKUP_ROTATION(INT iPickup)
	IF logic.Pickup.Rotation != NULL
		RETURN CALL logic.Pickup.Rotation(iPickup)
	ENDIF

	RETURN data.Pickup.Pickups[iPickup].vRotation
ENDFUNC

FUNC BOOL HAS_THIS_MISSION_PICKUP_BEEN_COLLECTED(INT iPickup)

	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sPickup.sPickup[iPickup].netID)
	AND IS_ENTITY_ATTACHED(NET_TO_ENT(serverBD.sPickup.sPickup[iPickup].netID))
	AND GET_ENTITY_ATTACHED_TO(NET_TO_ENT(serverBD.sPickup.sPickup[iPickup].netID)) = CONVERT_TO_ENTITY_INDEX(LOCAL_PED_INDEX)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_MISSION_PICKUP_BE_BLIPPED(INT iPickup)
	RETURN NOT HAS_THIS_MISSION_PICKUP_BEEN_COLLECTED(iPickup)
ENDFUNC

FUNC BOOL SHOULD_UNFREEZE_PORTABLE_MISSION_PICKUP(INT iPickup)
	
	MODEL_NAMES eModel = data.Pickup.Pickups[iPickup].model
	
	IF eModel = INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_powdercleaner_01a"))
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_PORTABLE_MISSION_PICKUP(INT iPickup)
	VECTOR vSpawnLocation = GET_MISSION_PICKUP_COORDS(iPickup)
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sPickup.sPickup[iPickup].netID) 
		REQUEST_MODEL(data.Pickup.Pickups[iPickup].model)
		IF HAS_MODEL_LOADED(data.Pickup.Pickups[iPickup].model)
			IF CAN_REGISTER_MISSION_PICKUPS(1)
				#IF IS_DEBUG_BUILD
				IF IS_VECTOR_ZERO(vSpawnLocation)
					ASSERTLN ("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_PORTABLE_MISSION_PICKUP - data.Pickup.NetPickups[iPickup].vCoords is zero")
				ENDIF
				#ENDIF
				
				// PICKUP_PORTABLE_CRATE_UNFIXED_INCAR prevents pickup falling through ground.
				serverBD.sPickup.sPickup[iPickup].netID = OBJ_TO_NET(CREATE_PORTABLE_PICKUP(PICKUP_PORTABLE_CRATE_FIXED_INCAR, vSpawnLocation, FALSE, data.Pickup.Pickups[iPickup].model))
		        SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(serverBD.sPickup.sPickup[iPickup].netID, TRUE)
				NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(serverBD.sPickup.sPickup[iPickup].netID), TRUE)
		        SET_ENTITY_INVINCIBLE(NET_TO_OBJ(serverBD.sPickup.sPickup[iPickup].netID), TRUE)
				
				// fixes url:bugstar:6812959 - Ignorable Assert - Error: Assertf(FreezeFlag || !pModelInfo->GetIsFixed()) FAILED: SCRIPT: Script Name = fm_content_island_heist : Program Counter = 7082:FREEZE_ENTITY_POSITION - can't unfreeze an object that the map says is fixed
				IF SHOULD_UNFREEZE_PORTABLE_MISSION_PICKUP(iPickup)
					FREEZE_ENTITY_POSITION(NET_TO_ENT(serverBD.sPickup.sPickup[iPickup].netID), FALSE)
				ENDIF
				
				IF NOT IS_MISSION_PICKUP_DATA_BIT_SET(iPickup, eMISSIONPICKUPDATABITSET_DISABLE_PHYSICS)
					SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(NET_TO_OBJ(serverBD.sPickup.sPickup[iPickup].netID), TRUE)
	    			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_OBJ(serverBD.sPickup.sPickup[iPickup].netID), FALSE)
	    			ACTIVATE_PHYSICS(NET_TO_OBJ(serverBD.sPickup.sPickup[iPickup].netID))
					SET_ENTITY_LOAD_COLLISION_FLAG(NET_TO_ENT(serverBD.sPickup.sPickup[iPickup].netID), TRUE)
					SET_ENTITY_DYNAMIC(NET_TO_ENT(serverBD.sPickup.sPickup[iPickup].netID),TRUE)
				ENDIF
				
				IF NOT IS_MISSION_PICKUP_DATA_BIT_SET(iPickup, eMISSIONPICKUPDATABITSET_DISABLE_PLACE_ON_GROUND)
					PLACE_OBJECT_ON_GROUND_OR_OBJECT_PROPERLY(NET_TO_OBJ(serverBD.sPickup.sPickup[iPickup].netID))
				ENDIF
				
				IF logic.Pickup.Attributes != NULL
					CALL logic.Pickup.Attributes(iPickup, NET_TO_ENT(serverBD.sPickup.sPickup[iPickup].netID))
				ENDIF
				
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_PORTABLE_MISSION_PICKUP, done at ", vSpawnLocation, " iPickup = ", iPickup)
			ENDIF
		ENDIF
	ENDIF
			
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sPickup.sPickup[iPickup].netID)  
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - CREATE_PORTABLE_MISSION_PICKUP - Created portable pickup ", iPickup," at ", vSpawnLocation," model ",GET_MODEL_NAME_FOR_DEBUG(data.Pickup.Pickups[iPickup].model))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL CREATE_MISSION_PICKUP(INT iPickup)
	IF REQUEST_LOAD_MODEL(data.Pickup.Pickups[iPickup].model)
		IF NOT NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sPickup.sPickup[iPickup].netID)
			CREATE_PORTABLE_MISSION_PICKUP(iPickup)
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sPickup.sPickup[iPickup].netID)
ENDFUNC

FUNC BLIP_SPRITE GET_MISSION_PICKUP_BLIP_SPRITE(INT iPickup)
	IF logic.Pickup.Blip.Sprite != NULL
		RETURN CALL logic.Pickup.Blip.Sprite(iPickup)
	ENDIF
	
	RETURN RADAR_TRACE_INVALID
ENDFUNC

FUNC HUD_COLOURS GET_MISSION_PICKUP_BLIP_COLOUR(INT iPickup)
	IF logic.Pickup.Blip.Colour != NULL
		RETURN CALL logic.Pickup.Blip.Colour(iPickup)
	ENDIF
	
	RETURN HUD_COLOUR_GREEN
ENDFUNC

FUNC STRING GET_MISSION_PICKUP_BLIP_NAME(INT iPickup)
	IF logic.Pickup.Blip.Name != NULL
		RETURN CALL logic.Pickup.Blip.Name(iPickup)
	ENDIF
	
	RETURN "Pickup"
ENDFUNC

FUNC FLOAT GET_MISSION_PICKUP_BLIP_SCALE(INT iPickup)
	IF logic.Pickup.Blip.Scale != NULL
		RETURN CALL logic.Pickup.Blip.Scale(iPickup)
	ENDIF
	
	RETURN 1.0
ENDFUNC

FUNC BOOL GET_MISSION_PICKUP_BLIP_SHORT_RANGE(INT iPickup)
	IF logic.Pickup.Blip.ShortRange != NULL
		RETURN CALL logic.Pickup.Blip.ShortRange(iPickup)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_MISSION_PICKUP_BLIP_FLASH_ON_CREATION(INT iPickup)
	IF logic.Pickup.Blip.FlashOnCreation != NULL
		RETURN CALL logic.Pickup.Blip.FlashOnCreation(iPickup)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ADD_MISSION_PICKUP_BLIP(INT iPickup, ENTITY_INDEX entID)
	IF NOT DOES_BLIP_EXIST(sPickupLocal.sPickup[iPickup].Blip)
		ADD_ENTITY_BLIP(sPickupLocal.sPickup[iPickup].Blip,
						entID,
						GET_MISSION_PICKUP_BLIP_SPRITE(iPickup),
						GET_MISSION_PICKUP_BLIP_COLOUR(iPickup),
						GET_MISSION_PICKUP_BLIP_NAME(iPickup),
						GET_MISSION_PICKUP_BLIP_SCALE(iPickup),
						DEFAULT, 
						GET_MISSION_PICKUP_BLIP_SHORT_RANGE(iPickup),
						GET_MISSION_PICKUP_BLIP_FLASH_ON_CREATION(iPickup))
	ENDIF
ENDPROC

PROC REMOVE_MISSION_PICKUP_BLIP(INT iPickup)
	IF DOES_BLIP_EXIST(sPickupLocal.sPickup[iPickup].Blip)
		REMOVE_BLIP(sPickupLocal.sPickup[iPickup].Blip)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_DRAW_MISSION_PICKUP_MARKER(INT iPickup, ENTITY_INDEX entID)
	UNUSED_PARAMETER(entID)
	
	RETURN DOES_BLIP_EXIST(sPickupLocal.sPickup[iPickup].Blip)
ENDFUNC

FUNC FLOAT GET_MISSION_PICKUP_MARKER_SCALE(INT iPickup)
	IF logic.Pickup.Blip.MarkerScale != NULL
		RETURN CALL logic.Pickup.Blip.MarkerScale(iPickup)
	ENDIF
	RETURN 0.5
ENDFUNC

PROC DRAW_MISSION_PICKUP_MARKER(INT iPickup, ENTITY_INDEX entID)
	IF NOT CALL logic.Pickup.Blip.DrawMarker(iPickup, entID)
		EXIT
	ENDIF
	
	INT iR, iG, iB, iA
	GET_HUD_COLOUR(GET_BLIP_HUD_COLOUR(sPickupLocal.sPickup[iPickup].Blip), iR, iG, iB, iA)
	FM_EVENTDRAW_MARKER_ABOVE_ENTITY(entID, iR, iG, iB, GET_MISSION_PICKUP_MARKER_SCALE(iPickup))
ENDPROC

#IF NOT DEFINED(MAINTAIN_MISSION_PICKUPS_UI)
PROC MAINTAIN_MISSION_PICKUPS_UI()
	INT iPickup
	REPEAT data.Pickup.iCount iPickup
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPickup.sPickup[iPickup].netID)
		AND CALL logic.Pickup.Blip.Enable(iPickup)
			ENTITY_INDEX entID = NET_TO_ENT(serverBD.sPickup.sPickup[iPickup].netID)
			
			ADD_MISSION_PICKUP_BLIP(iPickup, entID)
			DRAW_MISSION_PICKUP_MARKER(iPickup, entID)
		ELSE
			REMOVE_MISSION_PICKUP_BLIP(iPickup)
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

#IF NOT DEFINED(MAINTAIN_MISSION_PICKUPS_SERVER)
PROC MAINTAIN_MISSION_PICKUPS_SERVER()
	IF logic.Pickup.Enable != NULL
	AND NOT CALL logic.Pickup.Enable()
		EXIT
	ENDIF

	INT iPickup
	REPEAT data.Pickup.iCount iPickup
		IF NOT IS_SERVER_PICKUP_BIT_SET(iPickup, eMISSIONSERVERPICKUPBITSET_CREATED)
			IF CALL logic.Pickup.ShouldCreatePickup(iPickup)
				IF CREATE_MISSION_PICKUP(iPickup)
					FLOAT fHeading = GET_MISSION_PICKUP_HEADING(iPickup)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_PICKUPS_SERVER, set fHeading to ", fHeading, " iPickup = ", iPickup)
					SET_ENTITY_HEADING(NET_TO_OBJ(serverBD.sPickup.sPickup[iPickup].netID), fHeading)
					VECTOR vRotation = GET_MISSION_PICKUP_ROTATION(iPickup)
					IF NOT IS_VECTOR_ZERO(vRotation)
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_PICKUPS_SERVER, set rotation to ", vRotation, " iPickup = ", iPickup)
						SET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.sPickup.sPickup[iPickup].netID), vRotation, EULER_XYZ)
					ENDIF
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_MISSION_PICKUPS_SERVER - Created pickup ", iPickup)
					SET_SERVER_PICKUP_BIT(iPickup, eMISSIONSERVERPICKUPBITSET_CREATED)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF
#IF NOT DEFINED(MAINTAIN_MISSION_PICKUPS_CLIENT)
PROC MAINTAIN_MISSION_PICKUPS_CLIENT()
	IF logic.Pickup.Enable != NULL
	AND NOT CALL logic.Pickup.Enable()
		EXIT
	ENDIF

	MAINTAIN_MISSION_PICKUPS_UI()
	
	INT iPickup
	REPEAT data.Pickup.iCount iPickup
		IF IS_SERVER_PICKUP_BIT_SET(iPickup, eMISSIONSERVERPICKUPBITSET_CREATED)
			IF NOT IS_SERVER_PICKUP_BIT_SET(iPickup, eMISSIONSERVERPICKUPBITSET_COLLECTED)
				IF HAS_THIS_MISSION_PICKUP_BEEN_COLLECTED(iPickup)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "]- MAINTAIN_MISSION_PICKUPS_CLIENT - Collected pickup ", iPickup)
					SET_CLIENT_PICKUP_BIT(iPickup, eMISSIONCLIENTPICKUPBITSET_COLLECTED,LOCAL_PARTICIPANT_INDEX_AS_INT)
					
					IF logic.Pickup.OnCollection != NULL
						CALL logic.Pickup.OnCollection(iPickup)
					ENDIF
				ELSE
					IF NOT IS_MISSION_PICKUP_DATA_BIT_SET(iPickup, eMISSIONPICKUPDATABITSET_DISABLE_PLACE_ON_GROUND)
					AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPickup.sPickup[iPickup].netID)
					AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPickup.sPickup[iPickup].netID)
						PLACE_OBJECT_ON_GROUND_OR_OBJECT_PROPERLY(NET_TO_OBJ(serverBD.sPickup.sPickup[iPickup].netID))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF IS_SERVER_PICKUP_BIT_SET(iPickup, eMISSIONSERVERPICKUPBITSET_COLLECTED)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPickup.sPickup[iPickup].netID)
			AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPickup.sPickup[iPickup].netID)
				DELETE_NET_ID(serverBD.sPickup.sPickup[iPickup].netID)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF
#IF NOT DEFINED(MAINTAIN_MISSION_PICKUPS_LOOP)
PROC MAINTAIN_MISSION_PICKUPS_LOOP(INT iPart)
	IF logic.Pickup.Enable != NULL
	AND NOT CALL logic.Pickup.Enable()
		EXIT
	ENDIF

	INT iPickup
	REPEAT data.Pickup.iCount iPickup
		IF IS_CLIENT_PICKUP_BIT_SET(iPickup, eMISSIONCLIENTPICKUPBITSET_COLLECTED,iPart)
			serverBD.sPickup.sPickup[iPickup].iPartHolding = iPart
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

PROC CLEANUP_MISSION_PICKUPS()
	INT iPickup
	REPEAT MAX_NUM_PICKUPS iPickup
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPickup.sPickup[iPickup].netID)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPickup.sPickup[iPickup].netID)
				DELETE_NET_ID(serverBD.sPickup.sPickup[iPickup].netID)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - CLEANUP_MISSION_PICKUPS - Deleted pickup object ", iPickup)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

//----------------------
//	Portable Pickups
//----------------------

PROC MAINTAIN_NUM_ALLOWED_PICKUPS()
	BOOL bSafeToDisplayMissionUi = IS_SAFE_TO_DISPLAY_MISSION_UI(FALSE)
	BOOL bModifiedNumPickupsAllowHeld = IS_GENERIC_BIT_SET(eGENERICBITSET_MODIFIED_NUM_PICKUPS_ALLOW_HELD)
	INT iMissionEntity
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntities.netId[iMissionEntity])
		AND NOT IS_ENTITY_DEAD(NET_TO_ENT(serverBD.sMissionEntities.netId[iMissionEntity]))
			IF bSafeToDisplayMissionUi
			AND NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(iMissionEntity,LOCAL_PARTICIPANT_INDEX,eMISSIONENTITYCLIENTBITSET_PREVENT_COLLECTION) 
				SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(data.MissionEntity.MissionEntities[0].model, GET_MISSION_ENTITY_MAX_HOLD_COUNT_FOR_THIS_VARIATION())
				PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.sMissionEntities.netId[iMissionEntity]), FALSE, TRUE)
				SET_LOCAL_PLAYER_PERMITTED_TO_COLLECT_PICKUPS_WITH_MODEL(data.MissionEntity.MissionEntities[0].model, TRUE)
				SET_GENERIC_BIT(eGENERICBITSET_MODIFIED_NUM_PICKUPS_ALLOW_HELD)
				bModifiedNumPickupsAllowHeld = TRUE
			ELSE
				IF bModifiedNumPickupsAllowHeld
					SET_MAX_NUM_PORTABLE_PICKUPS_CARRIED_BY_PLAYER(data.MissionEntity.MissionEntities[0].model, -1)
					CLEAR_GENERIC_BIT(eGENERICBITSET_MODIFIED_NUM_PICKUPS_ALLOW_HELD)
					bModifiedNumPickupsAllowHeld = FALSE
				ENDIF
				
				PREVENT_COLLECTION_OF_PORTABLE_PICKUP(NET_TO_OBJ(serverBD.sMissionEntities.netId[iMissionEntity]), TRUE, TRUE)
				IF IS_MISSION_ENTITY_CLIENT_BIT_SET(iMissionEntity,LOCAL_PARTICIPANT_INDEX,eMISSIONENTITYCLIENTBITSET_COLLECTED)
				AND HAS_MISSION_ENTITY_LEFT_CARRIER_VEHICLE(iMissionEntity)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sMissionEntities.netId[iMissionEntity])
						DETACH_ENTITY(NET_TO_OBJ(serverBD.sMissionEntities.netId[iMissionEntity]))
						PRINTLN("[",scriptName,"] - DETACH_ENTITY - NOT IS_SAFE_TO_DISPLAY_MISSION_UI")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

//----------------------
//	Weapon Pickups
//----------------------

#IF MAX_NUM_WEAPON_PICKUPS
FUNC BOOL SHOULD_CREATE_WEAPON_PICKUP(INT iPickup)
	IF logic.WeaponPickup.ShouldCreate != null
		RETURN CALL logic.WeaponPickup.ShouldCreate(iPickup)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BLIP_SPRITE GET_WEAPON_PICKUP_SPRITE(PICKUP_TYPE pickupType)
	SWITCH pickupType
		CASE PICKUP_AMMO_BULLET_MP				RETURN RADAR_TRACE_PICKUP_MACHINEGUN			
	ENDSWITCH
	RETURN GET_CORRECT_BLIP_SPRITE(pickupType)
ENDFUNC

PROC REMOVE_WEAPON_PICKUP_BLIP(INT iPickup)
	IF DOES_BLIP_EXIST(sWeaponPickupLocal[iPickup].pickupBlip)
		REMOVE_BLIP(sWeaponPickupLocal[iPickup].pickupBlip)
	ENDIF
ENDPROC

PROC MAINTAIN_WEAPON_PICKUP_BLIPS(INT iPickup, PICKUP_TYPE pickupType)
	IF DOES_PICKUP_EXIST(sWeaponPickupLocal[iPickup].pickupId)
		IF DOES_PICKUP_OBJECT_EXIST(sWeaponPickupLocal[iPickup].pickupId)
		AND DOES_ENTITY_EXIST(GET_PICKUP_OBJECT(sWeaponPickupLocal[iPickup].pickupId))
			IF NOT DOES_BLIP_EXIST(sWeaponPickupLocal[iPickup].pickupBlip)
				sWeaponPickupLocal[iPickup].pickupBlip = ADD_BLIP_FOR_ENTITY(GET_PICKUP_OBJECT(sWeaponPickupLocal[iPickup].pickupId))
				SET_BLIP_SPRITE(sWeaponPickupLocal[iPickup].pickupBlip, GET_WEAPON_PICKUP_SPRITE(pickupType))
				SET_BLIP_SCALE(sWeaponPickupLocal[iPickup].pickupBlip, BLIP_SIZE_NETWORK_PICKUP)				
				SET_BLIP_AS_SHORT_RANGE(sWeaponPickupLocal[iPickup].pickupBlip,TRUE)
				SET_BLIP_PRIORITY(sWeaponPickupLocal[iPickup].pickupBlip, GET_CORRECT_BLIP_PRIORITY(BP_WEAPON))
				SHOW_HEIGHT_ON_BLIP(sWeaponPickupLocal[iPickup].pickupBlip, TRUE)
				IF IS_PICKUP_HEALTH(GET_WEAPON_PICKUP_TYPE(iPickup))
					SET_BLIP_COLOUR(sWeaponPickupLocal[iPickup].pickupBlip, BLIP_COLOUR_GREEN)
				ELIF IS_PICKUP_ARMOUR(GET_WEAPON_PICKUP_TYPE(iPickup))
					SET_BLIP_COLOUR(sWeaponPickupLocal[iPickup].pickupBlip, BLIP_COLOUR_BLUE)
				ENDIF
			ENDIF
		ELSE
			REMOVE_WEAPON_PICKUP_BLIP(iPickup)
		ENDIF
	ELSE
		REMOVE_WEAPON_PICKUP_BLIP(iPickup)
	ENDIF
ENDPROC

PROC MAINTAIN_WEAPON_PICKUPS()
	INT iPickup
	REPEAT MAX_NUM_WEAPON_PICKUPS iPickup
		IF SHOULD_CREATE_WEAPON_PICKUP(iPickup)
			IF data.WeaponPickup[iPickup].eType != PICKUP_TYPE_INVALID
			AND NOT IS_VECTOR_ZERO(data.WeaponPickup[iPickup].vCoord)
				IF NOT DOES_PICKUP_EXIST(sWeaponPickupLocal[iPickup].pickupId)
					sWeaponPickupLocal[iPickup].pickupId = CREATE_PICKUP(data.WeaponPickup[iPickup].eType, data.WeaponPickup[iPickup].vCoord, data.WeaponPickup[iPickup].iPlacementFlags, DEFAULT, FALSE)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_WEAPON_PICKUPS - Created weapon pickup #", iPickup, " at ", data.WeaponPickup[iPickup].vCoord)
				ELSE
					sWeaponPickupLocal[iPickup].bCreatedOnce = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		MAINTAIN_WEAPON_PICKUP_BLIPS(iPickup, data.WeaponPickup[iPickup].eType)
	ENDREPEAT
ENDPROC

PROC REMOVE_WEAPON_PICKUPS_AND_THEIR_BLIPS()

	INT i
	REPEAT MAX_NUM_WEAPON_PICKUPS i
		IF i < (MAX_NUM_WEAPON_PICKUPS - 1)
			IF DOES_BLIP_EXIST(sWeaponPickupLocal[i].pickupBlip)
				REMOVE_BLIP(sWeaponPickupLocal[i].pickupBlip)
			ENDIF
			
			IF DOES_PICKUP_EXIST(sWeaponPickupLocal[i].pickupId)
				IF NETWORK_HAS_CONTROL_OF_PICKUP(sWeaponPickupLocal[i].pickupId)
					REMOVE_PICKUP(sWeaponPickupLocal[i].pickupId)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

#ENDIF
