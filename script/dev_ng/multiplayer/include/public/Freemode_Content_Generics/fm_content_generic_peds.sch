USING "fm_content_generic_support.sch"
USING "fm_content_generic_ambush.sch"

PROC SETUP_DEFAULT_PED_TRIGGERS(INT iPed)

	IF IS_DATA_BIT_SET(eDATABITSET_ENABLE_STEALTH)
		ADD_PED_TRIGGERS_STEALTH(iPed)
		EXIT
	ENDIF

	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_BEEN_TARGETTED)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_TRIGGER_AREA_BEEN_ACTIVATED)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_BEEN_DAMAGED)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_VEHICLE_BEEN_DAMAGED)
	ADD_PED_TRIGGER(iPed, &PED_TRIGGER_HAS_PED_RECEIVED_PED_EVENT)

ENDPROC

FUNC BOOL IS_PED_A_COP(PED_INDEX pedId)
	IF DOES_ENTITY_EXIST(pedId)
		IF GET_PED_TYPE(pedId) = PEDTYPE_COP
		OR GET_PED_TYPE(pedId) = PEDTYPE_ARMY
		OR GET_PED_TYPE(pedId) = PEDTYPE_SWAT
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_PED_REACTION_DELAY(INT iPed)
	IF data.Ped.Peds[iPed].iMaxReactionDelay = -1 
		IF iPed < PED_REACTION_DELAY_HALF_PEDS
			RETURN ((iPed+1) * (PED_REACTION_DELAY_MAX / data.Ped.iCount))
		ELSE
			RETURN (((iPed+1)-PED_REACTION_DELAY_HALF_PEDS) * (PED_REACTION_DELAY_MAX / data.Ped.iCount))
		ENDIF
	ENDIF
	
	RETURN GET_RANDOM_INT_IN_RANGE(0, data.Ped.Peds[iPed].iMaxReactionDelay)
ENDFUNC

PROC INITIALISE_PED_REACTION_TIMES()
	INT iPed
	REPEAT data.Ped.iCount iPed
		serverBD.sPed[iPed].iReactionDelay = GET_PED_REACTION_DELAY(iPed)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] INITIALISE_PED_REACTION_TIMES - iPed #", iPed, "'s reaction delay set to ", serverBD.sPed[iPed].iReactionDelay, "ms.")
	ENDREPEAT
ENDPROC

PROC SET_PED_DEBUG_NAME(INT iPed)

	UNUSED_PARAMETER(iPed)

	#IF IS_DEBUG_BUILD
	TEXT_LABEL_15 tl15Ped
	tl15Ped = "Ped: "
	tl15Ped += iPed
	SET_PED_NAME_DEBUG(NET_TO_PED(serverBD.sPed[iPed].netId), tl15Ped)
	#ENDIF

ENDPROC

PROC SELECT_RANDOM_MAGIC_BULLET_DIRECTION(VECTOR vPedCoords, VECTOR& vBulletStart, VECTOR& vBulletEnd, BOOL bUseRandomOffSet)
	FLOAT StartRange = 0.1
	INT iRandom = GET_RANDOM_INT_IN_RANGE(0, 6)
	FLOAT fOffSetX = 0.5
	FLOAT fOffSetY = 0.1
	FLOAT fOffSetZ = 0.4
	
	IF bUseRandomOffSet
		INT iRandomOffSet = GET_RANDOM_INT_IN_RANGE(0, 5)
		fOffSetX = TO_FLOAT(iRandomOffSet)/10.0
		IF GET_RANDOM_BOOL()
			fOffSetX *= -1.0
		ENDIF
		
		iRandomOffSet = GET_RANDOM_INT_IN_RANGE(0, 2)
		fOffSetY = TO_FLOAT(iRandomOffSet)/15.0
		IF GET_RANDOM_BOOL()
			fOffSetY *= -1.0
		ENDIF
		
		iRandomOffSet = GET_RANDOM_INT_IN_RANGE(0, 6)
		fOffSetZ = TO_FLOAT(iRandomOffSet)/10.0
		IF GET_RANDOM_BOOL()
			fOffSetZ *= -1.0
		ENDIF
		
		PRINTLN("[CH_PREP] - [", GET_CURRENT_VARIATION_STRING(), "] SELECT_RANDOM_MAGIC_BULLET_DIRECTION - iRandomOffSet = ", iRandomOffSet, " - fOffSetX = ", fOffSetX, ", fOffSetY = ", fOffSetY, ", fOffSetZ = ", fOffSetZ)
	ENDIF
	
	SWITCH iRandom
		CASE 0
			vBulletStart = <<vPedCoords.x + StartRange, vPedCoords.y + fOffSetY, vPedCoords.z + fOffSetZ>>
			vBulletEnd = <<vPedCoords.x - fOffSetX, vPedCoords.y - fOffSetY, vPedCoords.z + fOffSetZ>>
		BREAK
		CASE 1
			vBulletStart = <<vPedCoords.x + StartRange, vPedCoords.y + fOffSetY, vPedCoords.z + fOffSetZ>>
			vBulletEnd = <<vPedCoords.x - fOffSetX, vPedCoords.y - fOffSetY, vPedCoords.z + fOffSetZ>>
		BREAK
		CASE 2
			vBulletStart = <<vPedCoords.x - StartRange, vPedCoords.y + fOffSetY, vPedCoords.z + fOffSetZ>>
			vBulletEnd = <<vPedCoords.x + fOffSetX, vPedCoords.y - fOffSetY, vPedCoords.z + fOffSetZ>>
		BREAK
		CASE 3
			vBulletStart = <<vPedCoords.x - StartRange, vPedCoords.y - fOffSetY, vPedCoords.z + fOffSetZ>>
			vBulletEnd = <<vPedCoords.x + fOffSetX, vPedCoords.y + fOffSetY, vPedCoords.z + fOffSetZ>>
		BREAK
		CASE 4
			vBulletStart = <<vPedCoords.x - StartRange, vPedCoords.y - fOffSetY, vPedCoords.z + fOffSetZ>>
			vBulletEnd = <<vPedCoords.x + fOffSetX, vPedCoords.y + fOffSetY, vPedCoords.z + fOffSetZ>>
		BREAK
		CASE 5
			vBulletStart = <<vPedCoords.x + StartRange, vPedCoords.y + fOffSetY, vPedCoords.z + fOffSetZ>>
			vBulletEnd = <<vPedCoords.x - fOffSetX, vPedCoords.y - fOffSetY, vPedCoords.z + fOffSetZ>>
		BREAK
	ENDSWITCH
ENDPROC

PROC KILL_PED_WITH_MAGIC_BULLET(PED_INDEX pedId, INT iPed, BOOL bForceRagdoll = TRUE, BOOL bFallOutVehicleOnDeath = FALSE, BOOL bApplyBloodEffect = TRUE, BOOL bSmashWindscreen = TRUE)
	IF NOT IS_ENTITY_DEAD(pedId)

		IF data.Ped.Peds[iPed].iVehicle != -1
			IF bSmashWindscreen
				VECTOR vStart = GET_PED_BONE_COORDS(pedId, BONETAG_HEAD, << 0.0, -5.0, 0.0 >>)
				VECTOR vEnd = GET_PED_BONE_COORDS(pedId, BONETAG_HEAD, << 0.0, 0.0, 0.0 >>)
				
				SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vStart, vEnd, 25, TRUE, WEAPONTYPE_HEAVYSNIPER)
				SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vEnd, vStart, 25, TRUE, WEAPONTYPE_ASSAULTSHOTGUN)
			ENDIF
			
			IF bFallOutVehicleOnDeath
				SET_PED_CONFIG_FLAG(pedId, PCF_ForceRagdollUponDeath, bForceRagdoll)
			ENDIF
			SET_PED_CONFIG_FLAG(pedId, PCF_FallsOutOfVehicleWhenKilled, bFallOutVehicleOnDeath)
		ELSE
			VECTOR vPedCoords = GET_ENTITY_COORDS(pedId, FALSE)
			VECTOR vStart, vEnd 
			
			SELECT_RANDOM_MAGIC_BULLET_DIRECTION(vPedCoords, vStart, vEnd, FALSE)
			
			SHOOT_SINGLE_BULLET_BETWEEN_COORDS(vStart, vEnd, 300, TRUE, WEAPONTYPE_HEAVYSNIPER, NULL, FALSE, FALSE, 80)
			
			IF bFallOutVehicleOnDeath
				SET_PED_CONFIG_FLAG(pedId, PCF_ForceRagdollUponDeath, bForceRagdoll)
			ENDIF
			SET_PED_CONFIG_FLAG(pedId, PCF_FallsOutOfVehicleWhenKilled, bFallOutVehicleOnDeath)
			
		ENDIF
		
		IF bApplyBloodEffect
			APPLY_RANDOM_BLOOD_DAMAGE_TO_PED(pedId)
		ENDIF
	ENDIF
ENDPROC

//----------------------
//	CREATION
//----------------------

FUNC BOOL CAN_PED_BE_CREATED(INT iPed)

	IF NOT IS_ENTITY_SPAWN_AVAILABLE(eENTITYTYPE_PED, iPed)
		PRINTLN("CAN_PED_BE_CREATED (", iPed, ") - FALSE - NOT IS_ENTITY_SPAWN_AVAILABLE. Currently spawn entity: eType = ",serverBD.eEntitySpawnType," iIndex = ", serverBD.iEntitySpawnIndex)
		RETURN FALSE
	ENDIF

	// If ped should spawn in vehicle, wait for vehicle to exist
	IF data.Ped.Peds[iPed].iVehicle != -1
		IF GET_VEHICLE_STATE(data.Ped.Peds[iPed].iVehicle) != eVEHICLESTATE_DRIVEABLE
			PRINTLN("CAN_PED_BE_CREATED (", iPed, ") - FALSE - Vehicle #", data.Ped.Peds[iPed].iVehicle, " is not driveable.")
			RETURN FALSE
		ENDIF
	ENDIF

	// If ped spawns in interior, wait until interior is ready
	#IF MAX_NUM_MISSION_INTERIORS
	INT iInterior = data.Ped.Peds[iPed].iInterior
	IF iInterior != -1
		IF NOT IS_MISSION_INTERIOR_READY(data.Interior.eInteriors[iInterior].eType)
		OR NOT HAS_NET_TIMER_EXPIRED(serverBD.stSpawnTimer,2000)
			PRINTLN("CAN_PED_BE_CREATED (", iPed, ") - FALSE - NOT IS_INTERIOR_READY(",GET_FMC_MISSION_INTERIOR_NAME(data.Interior.eInteriors[iInterior].eType))
			RETURN FALSE
		ENDIF
	ENDIF	
	#ENDIF
	
	IF NETWORK_IS_PLAYER_IN_MP_CUTSCENE(LOCAL_PLAYER_INDEX)
		PRINTLN("CAN_PED_BE_CREATED (", iPed, ") - FALSE - NETWORK_IS_PLAYER_IN_MP_CUTSCENE")
		RETURN FALSE
	ENDIF
	
	IF logic.Ped.CanBeCreated != NULL
	AND NOT CALL logic.Ped.CanBeCreated(iPed)
		PRINTLN("CAN_PED_BE_CREATED (", iPed, ") - logic.Ped.CanBeCreated = FALSE")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PED_RESPAWN(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN TRUE
ENDFUNC

FUNC VECTOR GET_PED_RESPAWN_COORDS(INT iPed)
	RETURN data.Ped.Peds[iPed].vCoords
ENDFUNC

FUNC FLOAT GET_PED_RESPAWN_HEADING(INT iPed)
	RETURN data.Ped.Peds[iPed].fHeading
ENDFUNC

FUNC MODEL_NAMES GET_PED_MODEL(INT iPed)
	RETURN data.Ped.Peds[iPed].model
ENDFUNC

FUNC VECTOR GET_PED_COORDS(INT iPed)
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_DIED_ONCE)
	AND CALL logic.Ped.Respawn.Enable(iPed)
		RETURN CALL logic.Ped.Respawn.Coords(iPed)
	ENDIF
	RETURN data.Ped.Peds[iPed].vCoords
ENDFUNC

FUNC FLOAT GET_PED_HEADING(INT iPed)
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_DIED_ONCE)
	AND CALL logic.Ped.Respawn.Enable(iPed)
		RETURN CALL logic.Ped.Respawn.Heading(iPed)
	ENDIF
	RETURN data.Ped.Peds[iPed].fHeading
ENDFUNC

FUNC WEAPON_TYPE GET_PED_WEAPON(INT iPed)
	RETURN data.Ped.Peds[iPed].weapon
ENDFUNC

FUNC BOOL SHOULD_EQUIP_WEAPON_IN_HAND(INT iPed)	

	// Can be used to overwrite the weapon in hand for peds
	IF logic.Ped.ShouldEquipWeaponInHand != NULL
		RETURN CALL logic.Ped.ShouldEquipWeaponInHand(iPed)
	ENDIF

	IF NOT IS_STRING_NULL_OR_EMPTY(CALL logic.Ped.Task.DoScenario.ScenarioName(iPed))
	AND ARE_STRINGS_EQUAL(CALL logic.Ped.Task.DoScenario.ScenarioName(iPed), "CODE_HUMAN_PATROL_2H")
		RETURN TRUE
	ENDIF
	
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_PATROL_PED)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC INT GET_START_HEALTH_FOR_PED()
	
	SWITCH serverBD.eMissionDifficulty
		CASE FM_CONTENT_DIFFICULTY_MEDIUM		RETURN 200
		CASE FM_CONTENT_DIFFICULTY_HARD			RETURN 300
		CASE FM_CONTENT_DIFFICULTY_VERY_HARD	RETURN 300
	ENDSWITCH
	
	RETURN 200
ENDFUNC

FUNC COMBAT_ABILITY_LEVEL GET_COMBAT_ABILITY_FOR_PED()

	SWITCH serverBD.eMissionDifficulty
		CASE FM_CONTENT_DIFFICULTY_MEDIUM			RETURN CAL_AVERAGE
		CASE FM_CONTENT_DIFFICULTY_HARD				RETURN CAL_PROFESSIONAL
		CASE FM_CONTENT_DIFFICULTY_VERY_HARD		RETURN CAL_PROFESSIONAL
	ENDSWITCH
	RETURN CAL_AVERAGE
ENDFUNC

FUNC INT GET_PLACED_PED_ACCURACY(INT iPed)

	IF CALL logic.Ped.Weapon(iPed) = WEAPONTYPE_RPG
	OR CALL logic.Ped.Weapon(iPed) = WEAPONTYPE_DLC_RAILGUN
		RETURN 2
	ENDIF

	SWITCH serverBD.eMissionDifficulty
		CASE FM_CONTENT_DIFFICULTY_MEDIUM			RETURN 10
		CASE FM_CONTENT_DIFFICULTY_HARD				RETURN 25
		CASE FM_CONTENT_DIFFICULTY_VERY_HARD		RETURN 40
	ENDSWITCH
	RETURN 10
ENDFUNC

FUNC INT GET_PED_SHOOT_RATE()

	SWITCH serverBD.eMissionDifficulty
		CASE FM_CONTENT_DIFFICULTY_MEDIUM			RETURN 60
		CASE FM_CONTENT_DIFFICULTY_HARD				RETURN 80
		CASE FM_CONTENT_DIFFICULTY_VERY_HARD		RETURN 100
	ENDSWITCH
	RETURN 60
ENDFUNC

FUNC INT GET_NUM_PED_GROUPS()
	INT iPed, iMaxGroups
	REPEAT data.Ped.iCount iPed
		IF data.Ped.Peds[iPed].iGroup > iMaxGroups
			iMaxGroups = data.Ped.Peds[iPed].iGroup
		ENDIF
	ENDREPEAT	
	
	RETURN (iMaxGroups+1)
ENDFUNC

FUNC INT GET_NUM_PED_SCENARIOS()

	INT iCount
	
	#IF MAX_NUM_SCENARIOS
	INT iUniqueScenarios[MAX_NUM_SCENARIOS], iScenario
	BOOL bAddScenario
	REPEAT MAX_NUM_SCENARIOS iScenario
		iUniqueScenarios[iScenario] = -1
	ENDREPEAT
	
	INT iPed
	REPEAT data.Ped.iCount iPed
		IF data.Ped.Peds[iPed].iScenarioIndex != -1
			bAddScenario = TRUE
			REPEAT MAX_NUM_SCENARIOS iScenario
				IF iUniqueScenarios[iScenario] = data.Ped.Peds[iPed].iScenarioIndex
					bAddScenario = FALSE
				ENDIF
			ENDREPEAT
			
			IF bAddScenario
				iUniqueScenarios[iCount] = data.Ped.Peds[iPed].iScenarioIndex 
				iCount++
			ENDIF
		ENDIF
	ENDREPEAT
	#ENDIF
	
	RETURN iCount
ENDFUNC

FUNC BOOL DOES_PED_HAVE_ANY_DEFENSIVE_AREA(INT iPed)
	INT iGroup = data.Ped.Peds[iPed].iGroup
	
	INT i
	REPEAT MAX_NUM_PED_GROUP_TRIGGERS i
		INT iArea = data.Ped.Group[iGroup].Trigger[i].iArea
		IF iArea != -1
		AND IS_TRIGGER_AREA_DATA_BIT_SET(iArea, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
			RETURN TRUE	
		ENDIF
	ENDREPEAT
	
	RETURN data.Ped.Peds[iPed].fDefensiveRadius >= cfFMC_MINIMUM_DEFENSIVE_SPHERE
ENDFUNC

PROC SETUP_PED_DEFENSIVE_AREAS(INT iPed, PED_INDEX pedId)
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] SETUP_PED_DEFENSIVE_AREAS - Avoiding defensive areas for ambush.")
		EXIT
	ENDIF
	
	IF data.Ped.Peds[iPed].fDefensiveRadius >= cfFMC_MINIMUM_DEFENSIVE_SPHERE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] SETUP_PED_DEFENSIVE_AREAS - Ped has fDefensiveRadius value (", data.Ped.Peds[iPed].fDefensiveRadius, "), prioritising over area.")
		SET_PED_SPHERE_DEFENSIVE_AREA(pedId, GET_ENTITY_COORDS(pedId, FALSE), data.Ped.Peds[iPed].fDefensiveRadius)
		EXIT
	ELIF data.Ped.Peds[iPed].fDefensiveRadius < cfFMC_MINIMUM_DEFENSIVE_SPHERE
	AND data.Ped.Peds[iPed].fDefensiveRadius > (-1.0)
		ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] SETUP_PED_DEFENSIVE_AREAS - Invalid defensive radius set in the creator file. Ped may not be acting as intended. Radius: ", data.Ped.Peds[iPed].fDefensiveRadius)
	ENDIF
	
	INT iGroup = data.Ped.Peds[iPed].iGroup

	INT i
	REPEAT MAX_NUM_PED_GROUP_TRIGGERS i
		INT iArea = data.Ped.Group[iGroup].Trigger[i].iArea
		IF iArea != -1
			IF IS_TRIGGER_AREA_DATA_BIT_SET(iArea, eTRIGGERAREADATABITSET_DEFENSIVE_AREA)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] SETUP_PED_DEFENSIVE_AREAS - Ped has a defensive area (", iArea, "), calling SET_PED_ANGLED_DEFENSIVE_AREA.")
				
				MISSION_PLACEMENT_DATA_TRIGGER_AREA triggerArea = GET_TRIGGER_AREA_DATASET(iArea)
				SET_PED_ANGLED_DEFENSIVE_AREA(pedID, triggerArea.vMin, triggerArea.vMax, triggerArea.fWidth)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC REL_GROUP_HASH GET_PED_RELATIONSHIP_GROUP(INT iPed)

	#IF MAX_NUM_RELATIONSHIP_GROUPS
	INT iCustomGroup = data.Ped.Group[data.Ped.Peds[iPed].iGroup].Relationship.iCustomGroup
	IF iCustomGroup != -1
		RETURN sRelationships[iCustomGroup].eGroup
	ENDIF
	#ENDIF
	
	RETURN data.Ped.Group[data.Ped.Peds[iPed].iGroup].Relationship.eGroup

ENDFUNC

#IF MAX_NUM_AMBIENT_SPEECH_PEDS
FUNC INT GET_AMBIENT_SPEECH_VOICE_HASH(INT iPed)
	IF logic.Ped.AmbientSpeech.VoiceHash != NULL
		RETURN CALL logic.Ped.AmbientSpeech.VoiceHash(iPed)
	ENDIF
	
	RETURN data.Ped.AmbientSpeech[data.Ped.Peds[iPed].iAmbientSpeechIndex].iVoiceHash
ENDFUNC
#ENDIF

FUNC BOOL SHOULD_PED_ONLY_EXIST_FOR_PARTICIPANTS(INT iPed)

	UNUSED_PARAMETER(iPed)

	#IF MAX_NUM_MISSION_INTERIORS
	IF data.Ped.Peds[iPed].iInterior != -1
		IF IS_FMC_INTERIOR_A_WARP_INTERIOR(data.Interior.eInteriors[data.Ped.Peds[iPed].iInterior].eType)
			RETURN TRUE
		ENDIF
	ENDIF
	#ENDIF
	
	IF IS_DATA_BIT_SET(eDATABITSET_ENABLE_HEIST_ISLAND)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC SET_PED_ATTRIBUTES(INT iPed, PED_INDEX pedId)
	
	IF SHOULD_PED_ONLY_EXIST_FOR_PARTICIPANTS(iPed)
		NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(serverBD.sPed[iPed].netId), TRUE)
	ENDIF
	
	IF NOT IS_PED_A_COP(pedId)
		SET_PED_CONFIG_FLAG(pedId, PCF_DontBehaveLikeLaw, TRUE)
	ENDIF
	
	IF data.Ped.Peds[iPed].model = MP_G_M_Pros_01
		SWITCH GET_PED_DRAWABLE_VARIATION(pedId, PED_COMP_HEAD)
			CASE 0		SET_PED_VOICE_GROUP(pedId, HASH("MP_BLACKOPS_BLACK_PVG"))		BREAK
			CASE 1		SET_PED_VOICE_GROUP(pedId, HASH("MP_BLACKOPS_WHITE_PVG"))		BREAK
			CASE 2		SET_PED_VOICE_GROUP(pedId, HASH("MP_BLACKOPS_LATINO_PVG"))		BREAK
		ENDSWITCH
	ENDIF
	
	#IF MAX_NUM_AMBIENT_SPEECH_PEDS
	IF DOES_PED_HAVE_AMBIENT_SPEECH(iPed)
		INT iPVGHash = GET_AMBIENT_SPEECH_VOICE_HASH(iPed)
		
		IF iPVGHash != 0
			SET_PED_VOICE_GROUP(pedID, iPVGHash)
		ENDIF
		
		IF NOT IS_AMBIENT_SPEECH_DATA_BIT_SET(data.Ped.Peds[iPed].iAmbientSpeechIndex, eAMBIENTSPEECHDATABITSET_DISABLE_STOP_PED_SPEAKING)
			STOP_PED_SPEAKING_SYNCED(pedID, TRUE)
		ENDIF
	ENDIF
	#ENDIF
	
	// TODO: Replace with DATABIT after review play
	IF data.Ped.Peds[iPed].iInterior != (-1)
		SET_PED_BIT(iPed, ePEDBITSET_INSIDE_MISSION_INTERIOR)
		
		ACTIVATE_PHYSICS(pedID)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedID, TRUE)
		SET_ENTITY_LOAD_COLLISION_FLAG(pedID, TRUE)
	ENDIF
	
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_FADE_IN)
		NETWORK_FADE_IN_ENTITY(pedID, TRUE)
	ENDIF

	WEAPON_TYPE weaponType = CALL logic.Ped.Weapon(iPed)
	IF weaponType != WEAPONTYPE_INVALID
		IF weaponType = WEAPONTYPE_UNARMED
			REMOVE_ALL_PED_WEAPONS(pedId)
		ELSE
			GIVE_WEAPON_TO_PED(pedId, weaponType, 9999999, SHOULD_EQUIP_WEAPON_IN_HAND(iPed))
		ENDIF
		
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] SET_PED_ATTRIBUTES - Giving ped weaponType = ", GET_STRING_FROM_TEXT_FILE(GET_WEAPON_NAME(weaponType)))
	ENDIF
	
	SET_PED_HIGHLY_PERCEPTIVE(pedId, TRUE)
	SET_PED_TARGET_LOSS_RESPONSE(pedId, TLR_NEVER_LOSE_TARGET)
	
	SET_PED_SEEING_RANGE(pedID, data.Ped.Peds[iPed].fSeeingRange)
	SET_PED_VISUAL_FIELD_CENTER_ANGLE(pedID, data.Ped.Peds[iPed].fSeeingAngle)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] SET_PED_ATTRIBUTES - fSeeingRange = ", data.Ped.Peds[iPed].fSeeingRange, ". fSeeingAngle = ", data.Ped.Peds[iPed].fSeeingAngle)
	
	SET_PED_HEARING_RANGE(pedID, data.Ped.Peds[iPed].fHearingRange)
	SET_PED_CONFIG_FLAG(pedId, PCF_BlockDroppingHealthSnacksOnDeath, TRUE)
	
	// Relationships
	REL_GROUP_HASH relGroup = GET_PED_RELATIONSHIP_GROUP(iPed)
	IF relGroup != RELGROUPHASH_COP
		SET_PED_RELATIONSHIP_GROUP_HASH(pedId, relGroup)
	ENDIF

ENDPROC

#IF NOT DEFINED(CAN_PED_USE_VEHICLE_ATTACK)
FUNC BOOL CAN_PED_USE_VEHICLE_ATTACK(INT iPed, PED_INDEX pedId)

	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_ALLOW_BUZZARD_CHAINGUN_USE)
		RETURN TRUE
	ENDIF
		
	IF IS_PED_USING_TURRET(pedId)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF

PROC SET_AMBUSH_PED_ATTRIBUTES(INT iPed, PED_INDEX pedId)

	SET_DRIVER_AGGRESSIVENESS(pedId, 1.0)
	SET_DRIVER_RACING_MODIFIER(pedId, 1.0)
	SET_COMBAT_FLOAT(pedId, CCF_AUTOMOBILE_SPEED_MODIFIER, 5.0)
	SET_PED_KEEP_TASK(pedId, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedId, CA_DO_DRIVEBYS, TRUE)
	SET_PED_CONFIG_FLAG(pedId, PCF_DontBlipCop, TRUE)
	SET_PED_DIES_INSTANTLY_IN_WATER(pedId, TRUE)
	SET_PED_CONFIG_FLAG(pedId, PCF_DontInfluenceWantedLevel, TRUE)
	//SET_COMBAT_FLOAT(pedId, CCF_TIME_BETWEEN_AGGRESSIVE_MOVES_DURING_VEHICLE_CHASE, -1.0)
	SET_DRIVER_ABILITY(pedId, 1.0)
	
	SET_PED_ACCURACY(pedId, GET_PLACED_PED_ACCURACY(iPed))
	SET_PED_SHOOT_RATE(pedId, GET_PED_SHOOT_RATE())
	SET_ENTITY_MAX_HEALTH(pedId, GET_START_HEALTH_FOR_PED())
	SET_ENTITY_HEALTH(pedId, GET_START_HEALTH_FOR_PED())
	SET_PED_COMBAT_ABILITY(pedId, GET_COMBAT_ABILITY_FOR_PED())
	
	SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedID, KNOCKOFFVEHICLE_HARD)

ENDPROC

FUNC BOOL SHOULD_DISABLE_PED_VOICE(INT iPed)
	IF IS_DATA_BIT_SET(eDATABITSET_REDUCE_NUM_PED_VOICES) 
		#IF MAX_NUM_AMBIENT_SPEECH_PEDS
		IF DOES_PED_HAVE_AMBIENT_SPEECH(iPed)
			RETURN FALSE
		ENDIF
		#ENDIF
		
		RETURN ((iPed % 2) = 0)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_PLACED_PED_ATTRIBUTES(INT iPed, PED_INDEX pedId)
	
	// Tasking
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
	SET_PED_KEEP_TASK(pedId, TRUE)
	
	// Pathing
	SET_PED_PATH_CAN_USE_CLIMBOVERS(pedId, FALSE)
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_ALLOW_LADDERS)
		SET_PED_CONFIG_FLAG(pedId, PCF_DisableLadderClimbing, FALSE)
		SET_PED_PATH_CAN_USE_LADDERS(pedID, TRUE)
	ELSE
		SET_PED_CONFIG_FLAG(pedId, PCF_DisableLadderClimbing, TRUE)
		SET_PED_PATH_CAN_USE_LADDERS(pedID, FALSE)
	ENDIF
	
	// Combat
	SET_PED_COMBAT_ATTRIBUTES(pedId, CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, TRUE)
	SET_PED_COMBAT_MOVEMENT(pedId, data.Ped.Peds[iPed].eMovement)
	SET_PED_COMBAT_ABILITY(pedId, GET_COMBAT_ABILITY_FOR_PED())
	SET_PED_ACCURACY(pedId, GET_PLACED_PED_ACCURACY(iPed)) 
	SET_PED_SHOOT_RATE(pedId, GET_PED_SHOOT_RATE())
	SET_PED_COMBAT_ATTRIBUTES(pedId, CA_USE_PROXIMITY_FIRING_RATE, FALSE)
	SET_ENTITY_MAX_HEALTH(pedId, GET_START_HEALTH_FOR_PED())
	SET_ENTITY_HEALTH(pedId, GET_START_HEALTH_FOR_PED())
	SET_PED_DIES_WHEN_INJURED(pedId, TRUE)
	
	IF SHOULD_DISABLE_PED_VOICE(iPed)
		STOP_PED_SPEAKING_SYNCED(pedId, TRUE)
		PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] SET_PLACED_PED_ATTRIBUTES - STOP_PED_SPEAKING_SYNCED called due to SHOULD_DISABLE_PED_VOICE.")
	ENDIF

	// Defensive Areas
	SETUP_PED_DEFENSIVE_AREAS(iPed, pedId)
	
ENDPROC

PROC SET_PED_SPAWN_PROPERTIES(INT iPed)
	
	PED_INDEX pedID = NET_TO_PED(serverBD.sPed[iPed].netId)
	BOOL bAmbush = IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
	
	SET_PED_ATTRIBUTES(iPed, pedID)
	
	IF bAmbush
		SET_AMBUSH_PED_ATTRIBUTES(iPed, pedID)
	ELSE
		SET_PLACED_PED_ATTRIBUTES(iPed, pedID)
	ENDIF
	
	IF CAN_PED_USE_VEHICLE_ATTACK(iPed, pedId)
		SET_PED_COMBAT_ATTRIBUTES(pedId, CA_USE_VEHICLE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedID, CA_USE_VEHICLE_ATTACK, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedID, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS, TRUE)
		SET_PED_FIRING_PATTERN(pedId, FIRING_PATTERN_BURST_FIRE)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] SET_PED_SPAWN_PROPERTIES - CAN_PED_USE_VEHICLE_ATTACK = TRUE - Setting vehicle combat attributes.")
	ENDIF
	
	IF logic.Ped.Attributes != NULL
		CALL logic.Ped.Attributes(iPed, pedID, bAmbush)
	ENDIF
	
	// If the ped should be killed on spawn, do it appropriately.
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_I_AM_A_CORPSE)
	
		// Can't freeze a ped while they are in a vehicle
		IF NOT IS_PED_IN_ANY_VEHICLE(pedID)
			FREEZE_ENTITY_POSITION(pedID, TRUE)
		ENDIF
		
		// Lower the peds damage so we can ensure the magic bullet can kill the ped regardless of whether he's in a vehicle or not 
		SET_ENTITY_HEALTH(pedID, 101)
		
		KILL_PED_WITH_MAGIC_BULLET(pedId, iPed, DEFAULT, FALSE, TRUE, TRUE)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] SET_PED_BEHAVIOUR_ATTRIBUTES_FOR_SPAWN - iPed #", iPed, " is a spawned corpse. Killing with a magic bullet.")
	ENDIF
	
ENDPROC

FUNC BOOL SHOULD_RUN_COORD_OK_CHECKS(INT iPed)

	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_RESPAWN_PED)
	AND IS_PED_BIT_SET(iPed, ePEDBITSET_DIED_ONCE)
	AND NOT IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_SKIP_POINT_OK_CHECKS)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_MISSION_PED(INT iPed)

	SET_ENTITY_SPAWNING(eENTITYTYPE_PED, iPed)

	MODEL_NAMES pedModel = CALL logic.Ped.Model(iPed)
	
	#IF MAX_NUM_SPAWN_POSITIONS
	// url:bugstar:7523499 - Freemode Creator - Investigate better ways to handle when we want an entity to spawn randomly out of several locations
	// define random spawn index for this spawn
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_NEED_SPAWN_VARIATION) AND serverBD.iEntitySpawnPositionIndex = (-1)
		IF logic.Ped.GetSpawnPositionVariationIndex != NULL
			serverBD.iEntitySpawnPositionIndex = CALL logic.Ped.GetSpawnPositionVariationIndex(iPed)
		ELSE
			serverBD.iEntitySpawnPositionIndex = GET_RANDOM_ENTITY_POSITION_INDEX(iPed, ET_PED)
		ENDIF
	ENDIF
	#ENDIF
		
	IF REQUEST_LOAD_MODEL(pedModel) 
		#IF IS_DEBUG_BUILD
		IF sDebugVars.bDoPedPrints
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] CREATE_MISSION_PED - REQUEST_LOAD_MODEL(", GET_MODEL_NAME_FOR_DEBUG(pedModel), ") = TRUE")
		ENDIF
		#ENDIF
		
		// ******************
		// In vehicle peds
		// ******************
		INT iVehicle = data.Ped.Peds[iPed].iVehicle
		IF iVehicle != -1		
			IF TAKE_CONTROL_OF_NET_ID(serverBD.sVehicle[iVehicle].netId)
				IF CREATE_NET_PED_IN_VEHICLE(serverBD.sPed[iPed].netId, serverBD.sVehicle[iVehicle].netId, PEDTYPE_MISSION, pedModel, data.Ped.Peds[iPed].eSeat)
					SET_PED_SPAWN_PROPERTIES(iPed)
					SET_MODEL_AS_NO_LONGER_NEEDED(pedModel)
					CLEAR_ENTITY_SPAWNING()
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] CREATE_MISSION_PED - Created iPed #", iPed, " in vehicle.")
					RETURN TRUE
				ENDIF
			ENDIF
		
		// ******************
		// On foot peds
		// ******************	
		ELSE
			VECTOR vSpawnCoord = CALL logic.Ped.Coords(iPed)
			FLOAT fSpawnHeading = CALL logic.Ped.Heading(iPed)
			
			#IF MAX_NUM_SPAWN_POSITIONS
				// Use spawn position variations
				IF IS_PED_BIT_SET(iPed, ePEDBITSET_NEED_SPAWN_VARIATION) AND serverBD.iEntitySpawnPositionIndex > (-1)
					vSpawnCoord = data.SpawnPosition.Positions[serverBD.iEntitySpawnPositionIndex].vCoords
					fSpawnHeading = data.SpawnPosition.Positions[serverBD.iEntitySpawnPositionIndex].vRotation.z
				ENDIF
			#ENDIF
			
			IF NOT IS_VECTOR_ZERO(vSpawnCoord)
				IF NOT SHOULD_RUN_COORD_OK_CHECKS(iPed)
				OR IS_POINT_OK_FOR_NET_ENTITY_CREATION(vSpawnCoord)
					IF CREATE_NET_PED(serverBD.sPed[iPed].netId, PEDTYPE_MISSION, pedModel, vSpawnCoord, fSpawnHeading, TRUE, TRUE, TRUE)
						SET_PED_SPAWN_PROPERTIES(iPed)
						CLEAR_ENTITY_SPAWNING()
						SET_MODEL_AS_NO_LONGER_NEEDED(pedModel)
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] CREATE_MISSION_PED - Created iPed #", iPed, " on foot.")
						RETURN TRUE
					ENDIF
				ELSE
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] CREATE_MISSION_PED - Spawn coord is not ok: ", vSpawnCoord)
					CLEAR_ENTITY_SPAWNING()
					RETURN FALSE
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF sDebugVars.bDoPedPrints
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] CREATE_MISSION_PED - serverBD.vSpawnPedCoords = <<0.0, 0.0, 0.0>>")
				ENDIF
				#ENDIF
			ENDIF

		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF sDebugVars.bDoPedPrints
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [FMP_PEDS ", iPed, "] CREATE_MISSION_PED - REQUEST_LOAD_MODEL(", GET_MODEL_NAME_FOR_DEBUG(pedModel), ") = FALSE")
		ENDIF
		#ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_ACTIVATE_PED(INT iPed)

	#IF MAX_NUM_AMBUSH_VEHICLES
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_BlockFMContentAmbush")
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] SHOULD_ACTIVATE_PED - BLOCKED VIA sc_BlockFMContentAmbush.")
			RETURN FALSE
		ENDIF
		#ENDIF
		
		#IF MAX_NUM_AMBUSH_VEHICLES_VARIATIONS
		// url:bugstar:7467758 - Freemode Creator: Could we add the option to customise the waves to not use identical vehicle setups for every ambush?
		// Importend to deteminate the ambush peds that actually should spawn, rather then all
		// ambush vehicle variation behaviour
		IF SHOULD_ACTIVATE_AMBUSH()
			INT iAmbush
			REPEAT MAX_NUM_AMBUSH_VEHICLES iAmbush
				IF IS_AMBUSH_VEHICLE_AVAILABLE_FOR_WAVE(iAmbush, data.Ped.Peds[iPed].iVehicle)
					RETURN TRUE
				ENDIF
			ENDREPEAT
			
			RETURN FALSE
		ENDIF
		#ENDIF
		
		// Default Behaviour - spawn all flagged ambush peds when ambush available
		RETURN SHOULD_ACTIVATE_AMBUSH()
	ENDIF
	#ENDIF
	
	IF logic.Ped.Activate != NULL
		RETURN CALL logic.Ped.Activate(iPed)
	ENDIF
	
	RETURN TRUE

ENDFUNC

//----------------------
//	TASKS
//----------------------

FUNC BOOL DEFAULT_SHOULD_PLAY_SCENARIO_AT_POSITION(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_START_SCENARIO_AT_POSITION(INT iPed, STRING sScenarioName)
	
	RETURN CALL logic.Ped.Task.DoScenario.PlayAtPosition(iPed)
		OR ARE_STRINGS_EQUAL(sScenarioName, "PROP_HUMAN_SEAT_SUNLOUNGER")
		OR ARE_STRINGS_EQUAL(sScenarioName, "WORLD_HUMAN_SEAT_LEDGE")
		OR ARE_STRINGS_EQUAL(sScenarioName, "WORLD_HUMAN_SEAT_LEDGE_EATING")
		OR ARE_STRINGS_EQUAL(sScenarioName, "WORLD_HUMAN_SEAT_STEPS")
		OR ARE_STRINGS_EQUAL(sScenarioName, "WORLD_HUMAN_SEAT_WALL")
		OR ARE_STRINGS_EQUAL(sScenarioName, "WORLD_HUMAN_SEAT_WALL_EATING")
ENDFUNC

FUNC STRING GET_PED_SCENARIO_NAME(INT iPed)

	UNUSED_PARAMETER(iPed)

	#IF MAX_NUM_SCENARIOS
	IF data.ped.Peds[iPed].iScenarioIndex != -1
		RETURN CONVERT_TEXT_LABEL_TO_STRING(data.Scenario[data.ped.Peds[iPed].iScenarioIndex].sName)
	ENDIF
	#ENDIF
	
	RETURN ""
ENDFUNC

FUNC VECTOR GET_PED_SCENARIO_POSITION(INT iPed)
	RETURN CALL logic.Ped.Coords(iPed)
ENDFUNC

FUNC FLOAT GET_PED_SCENARIO_HEADING(INT iPed)
	RETURN CALL logic.Ped.Heading(iPed)
ENDFUNC

FUNC BOOL SHOULD_PED_HEAD_TRACK(INT iPed)
	
	IF logic.Ped.Task.DoScenario.ShouldHeadTrack != NULL
		RETURN CALL logic.Ped.Task.DoScenario.ShouldHeadTrack(iPed)
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC GENERIC_TASK_DO_SCENARIO(INT iPed, PED_INDEX pedId)

	IF NOT IS_STRING_NULL_OR_EMPTY(CALL logic.Ped.Task.DoScenario.ScenarioName(iPed))
		IF SHOULD_START_SCENARIO_AT_POSITION(iPed, CALL logic.Ped.Task.DoScenario.ScenarioName(iPed))
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_START_SCENARIO_AT_POSITION)
			AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
				TASK_START_SCENARIO_AT_POSITION(pedId, CALL logic.Ped.Task.DoScenario.ScenarioName(iPed), CALL logic.Ped.Task.DoScenario.Position(iPed), CALL logic.Ped.Task.DoScenario.Heading(iPed), DEFAULT, TRUE)
				
				#IF IS_DEBUG_BUILD
				VECTOR vPosition 
				vPosition = CALL logic.Ped.Task.DoScenario.Position(iPed)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_DO_SCENARIO - iPed #", iPed, " using TASK_START_SCENARIO_AT_POSITION with ScenarioName = ", CALL logic.Ped.Task.DoScenario.ScenarioName(iPed), ", vPosition = ", vPosition, ", fHeading = ", CALL logic.Ped.Task.DoScenario.Heading(iPed))
				#ENDIF
			ENDIF
		ELSE
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_START_SCENARIO_IN_PLACE)
			AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
				TASK_START_SCENARIO_IN_PLACE(pedId, CALL logic.Ped.Task.DoScenario.ScenarioName(iPed), DEFAULT, FALSE)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_DO_SCENARIO - iPed #", iPed, " using TASK_START_SCENARIO_IN_PLACE with ScenarioName = ", CALL logic.Ped.Task.DoScenario.ScenarioName(iPed))
			ENDIF
		ENDIF
		
		IF SHOULD_PED_HEAD_TRACK(iPed)
			IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_START_SCENARIO_AT_POSITION)
			OR IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_START_SCENARIO_IN_PLACE)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
					IF NOT IS_PED_HEADTRACKING_PED(pedId, LOCAL_PED_INDEX)
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_DO_SCENARIO - iPed #", iPed, " Told to head track local player")
						TASK_LOOK_AT_ENTITY(pedId,LOCAL_PED_INDEX, -1, SLF_WHILE_NOT_IN_FOV)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_DO_SCENARIO - iPed #", iPed, " using NULL or EMPTY scenario!")
	ENDIF
ENDPROC

PROC GENERIC_TASK_FLEE_ON_FOOT(INT iPed, PED_INDEX pedId)

	IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_SMART_FLEE_POINT)
		
		IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
					
			DISABLE_PED_PAIN_AUDIO(pedId, FALSE)
																					
			TASK_SMART_FLEE_COORD(pedId, GET_ENTITY_COORDS(pedId, FALSE), 10000.0, 999999)
			SET_PED_FLEE_ATTRIBUTES(pedId, FA_UPDATE_TO_NEAREST_HATED_PED, TRUE)
			
			SET_PED_FLEE_ATTRIBUTES(pedId, FA_USE_VEHICLE, FALSE)
			SET_PED_FLEE_ATTRIBUTES(pedId, FA_FORCE_EXIT_VEHICLE, TRUE)
			
		ENDIF
	ENDIF

ENDPROC

PROC GENERIC_TASK_COWER(INT iPed, PED_INDEX pedId)

	IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
		IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_COWER)
			TASK_COWER(pedId)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_COWER - iPed #", iPed)
		ENDIF
	ENDIF

ENDPROC

FUNC VECTOR GET_VEHICLE_WANDER_COORD(INT iPed)
	VECTOR vCoord = data.Vehicle.Vehicles[data.Ped.Peds[iPed].iVehicle].vCoords
	SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)
		CASE 0
			vCoord += <<1000.0, 0.0, 0.0>>
		BREAK
		CASE 1
			vCoord -= <<1000.0, 0.0, 0.0>>
		BREAK
		CASE 2
			vCoord += <<0.0, 1000.0, 0.0>>
		BREAK
		CASE 3
			vCoord -= <<0.0, 1000.0, 0.0>>
		BREAK
	ENDSWITCH
	RETURN vCoord
ENDFUNC

PROC GENERIC_TASK_VEHICLE_WANDER_IN_AREA(INT iPed, PED_INDEX pedId)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId)
		VEHICLE_INDEX vehId = NET_TO_VEH(serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId)
		
		IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_VEHICLE_MISSION)
		OR GET_ACTIVE_VEHICLE_MISSION_TYPE(vehId) != MISSION_GOTO
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
				VECTOR vWanderCoord = GET_VEHICLE_WANDER_COORD(iPed)
				IF IS_THIS_MODEL_A_BOAT(data.Vehicle.Vehicles[data.Ped.Peds[iPed].iVehicle].model)
					TASK_BOAT_MISSION(pedId, vehId, NULL, NULL, vWanderCoord, MISSION_GOTO, 20.0, DRIVINGMODE_AVOIDCARS, 10.0, BCF_AvoidShore | BCF_PreferForward | BCF_UseWanderRoute)
				ELIF IS_THIS_MODEL_A_HELI(data.Vehicle.Vehicles[data.Ped.Peds[iPed].iVehicle].model)
					TASK_HELI_MISSION(pedId, vehId, NULL, NULL, vWanderCoord, MISSION_GOTO, 20.0, 1.0, -1, CEIL(vWanderCoord.z), 10, DEFAULT, HF_StartEngineImmediately)
				ELSE
					TASK_VEHICLE_MISSION_COORS_TARGET(pedId, vehId, vWanderCoord, MISSION_GOTO, 20.0, DRIVINGMODE_AVOIDCARS, 10.0, 1.0, FALSE)
				ENDIF
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_WANDER_IN_AREA - Started wandering toward ", vWanderCoord)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// --- TASK - TASK_WANDER_ON_FOOT ---

/// PURPOSE:
///    Function to start TASK_WANDER_ON_FOOT_X for ped
/// PARAMS:
///    iPed - 
///    pedId - 
///    bWithAnim - 
PROC GENERIC_TASK_WANDER_ON_FOOT_INTERNAL(INT iPed, PED_INDEX pedId, BOOL bWithAnim)

	IF NOT (IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_WANDER_STANDARD) OR IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_WANDER_IN_AREA) OR 
			IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_WANDER_SPECIFIC))
			
		IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
		
			// --- Get Type ---
			SCRIPT_TASK_NAME eScriptTaskName
			
			IF bWithAnim
				eScriptTaskName = SCRIPT_TASK_WANDER_SPECIFIC // Animation only works with Type _SPECIFIC
			ELSE
				eScriptTaskName = CALL logic.Ped.Task.WanderOnFoot.Type(iPed)
			ENDIF
		
			// --- Start Task ---
			SWITCH eScriptTaskName
				CASE SCRIPT_TASK_WANDER_STANDARD
				
					// -- Start WANDER_ON_FOOT - STANDARD --
					TASK_WANDER_STANDARD(pedId, CALL logic.Ped.Task.WanderOnFoot.Heading(iPed), CALL logic.Ped.Task.WanderOnFoot.Flags(iPed))
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_WANDER_ON_FOOT - STANDARD - Started")
					
					BREAK
				CASE SCRIPT_TASK_WANDER_IN_AREA
				
					IF logic.Ped.Task.WanderOnFoot.AreaCoord != NULL AND logic.Ped.Task.WanderOnFoot.AreaRadius != NULL AND
							NOT IS_VECTOR_ZERO(CALL logic.Ped.Task.WanderOnFoot.AreaCoord(iPed)) AND CALL logic.Ped.Task.WanderOnFoot.AreaRadius(iPed) > 0.0
							
						// -- Start WANDER_ON_FOOT - IN_AREA --
						TASK_WANDER_IN_AREA(pedId, CALL logic.Ped.Task.WanderOnFoot.AreaCoord(iPed), CALL logic.Ped.Task.WanderOnFoot.AreaRadius(iPed), 
												CALL logic.Ped.Task.WanderOnFoot.AreaMinWaitTime(iPed), CALL logic.Ped.Task.WanderOnFoot.AreaMaxWaitTime(iPed))
												
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_WANDER_ON_FOOT - IN_AREA - Started")
						
					ELSE
						// When Param AreaCoord and AreaRadius Missing
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_WANDER_ON_FOOT - IN_AREA - Skiped - missing definition AreaCoord/AreaRadius")
					ENDIF
					
					BREAK
				CASE SCRIPT_TASK_WANDER_SPECIFIC
				
					IF logic.Ped.Task.WanderOnFoot.AnimGroup != NULL AND logic.Ped.Task.WanderOnFoot.Anim != NULL AND
						NOT IS_STRING_NULL_OR_EMPTY(CALL logic.Ped.Task.WanderOnFoot.AnimGroup(iPed)) AND NOT IS_STRING_NULL_OR_EMPTY(CALL logic.Ped.Task.WanderOnFoot.Anim(iPed))
						
						// -- Start WANDER_ON_FOOT - SPECIFIC --
						TASK_WANDER_SPECIFIC(pedId, CALL logic.Ped.Task.WanderOnFoot.AnimGroup(iPed), CALL logic.Ped.Task.WanderOnFoot.Anim(iPed), CALL logic.Ped.Task.WanderOnFoot.Heading(iPed))
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_WANDER_ON_FOOT - SPECIFIC - Started")
						
					ELSE
						// When Param AnimGroup or Anim Missing
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_WANDER_ON_FOOT - SPECIFIC - Skiped - missing definition AnimGroup/Anim")
					ENDIF
					
					BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE: 
///    To execute ePEDTASK_WANDER_ON_FOOT
/// PARAMS:
///    iPed - Ped Index in data.Ped-Array
///    pedId - PedID quique to world spawn
PROC GENERIC_TASK_WANDER_ON_FOOT(INT iPed, PED_INDEX pedId)
	GENERIC_TASK_WANDER_ON_FOOT_INTERNAL(iPed, pedId, FALSE)
ENDPROC

/// PURPOSE:
///     To execute ePEDTASK_WANDER_ON_FOOT_WITH_ANIM
/// PARAMS:  
///    iPed - Ped Index in data.Ped-Array
///    pedId - PedID quique to world spawn
PROC GENERIC_TASK_WANDER_ON_FOOT_WITH_ANIM(INT iPed, PED_INDEX pedId)
	GENERIC_TASK_WANDER_ON_FOOT_INTERNAL(iPed, pedId, TRUE)
ENDPROC

FUNC VECTOR GET_HELI_LANDING_COORD(INT iPed, VEHICLE_INDEX vehId)
	IF logic.Ped.Task.LandHeli.Coords != null
		RETURN CALL logic.Ped.Task.LandHeli.Coords(iPed)
	ENDIF
	
	// Default, land below the current coord
	VECTOR vCoord = GET_ENTITY_COORDS(vehId, FALSE)
	FLOAT fZCoord
	IF NOT GET_GROUND_Z_FOR_3D_COORD(vCoord,fZCoord)
		fZCoord = GET_APPROX_FLOOR_FOR_POINT(vCoord.x, vCoord.y)
	ENDIF
	vCoord.z = fZCoord
	RETURN vCoord
ENDFUNC

FUNC FLOAT GET_HELI_LANDING_HEADING(INT iPed)
	IF logic.Ped.Task.LandHeli.Heading != null
		RETURN CALL logic.Ped.Task.LandHeli.Heading(iPed)
	ENDIF
	RETURN -1.0
ENDFUNC

FUNC FLOAT GET_HELI_LANDING_REACH_DITSANCE(INT iPed)
	IF logic.Ped.Task.LandHeli.ReachDistance != null
		RETURN CALL logic.Ped.Task.LandHeli.ReachDistance(iPed)
	ENDIF
	RETURN 7.5
ENDFUNC

FUNC BOOL SHOULD_DISABLE_HELI_LAND_SLOWDOWN(INT iPed)
	IF logic.Ped.Task.LandHeli.DisableSlowdown != null
		RETURN CALL logic.Ped.Task.LandHeli.DisableSlowdown(iPed)
	ENDIF
	RETURN FALSE
ENDFUNC
	
PROC GENERIC_TASK_LAND_HELI(INT iPed, PED_INDEX pedId)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId)
		VEHICLE_INDEX vehId = NET_TO_VEH(serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId)
		
		IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_VEHICLE_MISSION)
		OR GET_ACTIVE_VEHICLE_MISSION_TYPE(vehId) != MISSION_LAND
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
				VECTOR vLandingCoord = GET_HELI_LANDING_COORD(iPed, vehId)
				FLOAT fReachDistance = GET_HELI_LANDING_REACH_DITSANCE(iPed)
				TASK_HELI_MISSION(pedID, vehId, NULL, NULL, vLandingCoord, MISSION_LAND, 30.0, fReachDistance, GET_HELI_LANDING_HEADING(iPed), 10, 10, DEFAULT, HF_AttainRequestedOrientation | HF_LandOnArrival | HF_IgnoreHiddenEntitiesDuringLand)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_LAND_HELI - Started landing at ", vLandingCoord, " reach distance: ", fReachDistance, "m")
			ENDIF
		ELIF SHOULD_DISABLE_HELI_LAND_SLOWDOWN(iPed)
		AND GET_ACTIVE_VEHICLE_MISSION_TYPE(vehId) = MISSION_LAND
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId)
				SET_SHORT_SLOWDOWN_FOR_LANDING(vehId)
				SET_USE_DESIRED_Z_CRUISE_SPEED_FOR_LANDING(vehId, TRUE)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_LAND_HELI - Setting SET_SHORT_SLOWDOWN_FOR_LANDING for Vehicle ", data.Ped.Peds[iPed].iVehicle)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_VEHICLE_TO_ENTER(INT iPed)
	RETURN data.Ped.Peds[iPed].iVehicle
ENDFUNC

FUNC INT GET_ENTER_VEHICLE_WARP_TIME(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN -1
ENDFUNC

FUNC VEHICLE_SEAT GET_VEHICLE_SEAT_TO_ENTER(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN data.Ped.Peds[iPed].eSeat
ENDFUNC

FUNC FLOAT GET_ENTER_VEHICLE_PED_MOVE_BLEND_RATIO(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN PEDMOVE_SPRINT
ENDFUNC

FUNC ENTER_EXIT_VEHICLE_FLAGS GET_ENTER_EXIT_VEHICLE_FLAGS(INT iPed)
	UNUSED_PARAMETER(iPed)
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		RETURN ECF_JACK_ANYONE | ECF_RESUME_IF_INTERRUPTED
	ENDIF
	RETURN ECF_RESUME_IF_INTERRUPTED
ENDFUNC

PROC ON_ENTER_VEHICLE(PED_INDEX pedId)
	CLEAR_PED_TASKS(pedID)
ENDPROC

FUNC INT GET_EXIT_VEHICLE_DELAY_TIME(INT iPed)
	IF logic.Ped.Task.ExitVehicle.DelayTime != NULL
		RETURN CALL logic.Ped.Task.ExitVehicle.DelayTime(iPed)
	ENDIF
	RETURN 0
ENDFUNC

FUNC ENTER_EXIT_VEHICLE_FLAGS GET_EXIT_VEHICLE_FLAGS(INT iPed)
	IF logic.Ped.Task.ExitVehicle.EnterExitVehicleFlags != NULL
		RETURN CALL logic.Ped.Task.ExitVehicle.EnterExitVehicleFlags(iPed)
	ENDIF
	RETURN INT_TO_ENUM(ENTER_EXIT_VEHICLE_FLAGS, 0)
ENDFUNC

PROC GENERIC_TASK_EXIT_VEHICLE(INT iPed, PED_INDEX pedId)

	IF IS_PED_LOCAL_BIT_SET(iPed, ePEDLOCALBITSET_IN_ANY_VEHICLE)
		IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_LEAVE_ANY_VEHICLE)
				TASK_LEAVE_ANY_VEHICLE(pedID, GET_EXIT_VEHICLE_DELAY_TIME(iPed), GET_EXIT_VEHICLE_FLAGS(iPed))
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_EXIT_VEHICLE - iPed #", iPed, " has been told to leave the vehicle.")
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_EXIT_VEHICLE - iPed #", iPed, " attempting to task ped to leave vehicle when not in vehicle.")
	ENDIF

ENDPROC

FUNC BOOL GET_VEHICLE_FOR_ENTER_TASK(VEHICLE_INDEX &vehicleID, INT &iVehicle, INT iPed)

	iVehicle = CALL logic.Ped.Task.EnterVehicle.Vehicle(iPed)
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		iVehicle = data.Ped.Peds[iPed].iVehicle
	ENDIF
	
	IF iVehicle != -1
	AND NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sVehicle[iVehicle].netId)
		vehicleID = NET_TO_VEH(serverBD.sVehicle[iVehicle].netId)
	ELIF logic.Ped.Task.EnterVehicle.CustomVehicle != NULL
		vehicleID = CALL logic.Ped.Task.EnterVehicle.CustomVehicle(iPed)
	ENDIF
	
	RETURN (vehicleID != NULL)

ENDFUNC

PROC GENERIC_TASK_ENTER_VEHICLE(INT iPed, PED_INDEX pedId)

	VEHICLE_INDEX vehicleID
	INT iVeh

	IF NOT IS_PED_LOCAL_BIT_SET(iPed, ePEDLOCALBITSET_IN_ANY_VEHICLE)
		IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
			
			IF GET_VEHICLE_FOR_ENTER_TASK(vehicleID, iVeh, iPed)
				IF DOES_ENTITY_EXIST(vehicleID)
					IF IS_VEHICLE_DRIVEABLE(vehicleID)
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_ENTER_VEHICLE)
						OR GET_VEHICLE_PED_IS_ENTERING(pedID) != vehicleID
							VEHICLE_SEAT eSeat = VS_DRIVER
							IF logic.Ped.Task.EnterVehicle.Seat != NULL
								eSeat = CALL logic.Ped.Task.EnterVehicle.Seat(iPed)
							ENDIF
							IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
								eSeat = data.Ped.Peds[iPed].eSeat
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
							ELSE
								CALL logic.Ped.Task.EnterVehicle.OnTask(pedId)	
							ENDIF
							
							TASK_ENTER_VEHICLE(pedID, vehicleID, CALL logic.Ped.Task.EnterVehicle.WarpTime(iPed), eSeat, CALL logic.Ped.Task.EnterVehicle.MoveBlendRatio(iPed), CALL logic.Ped.Task.EnterVehicle.EnterExitVehicleFlags(iPed))
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_ENTER_VEHICLE - iPed #", iPed, " has been told to enter iVehicle #", iVeh)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF logic.Ped.Task.EnterVehicle.LeaveCurrentVehicle != NULL
		AND CALL logic.Ped.Task.EnterVehicle.LeaveCurrentVehicle(iPed)
			IF GET_VEHICLE_FOR_ENTER_TASK(vehicleID, iVeh, iPed) 
				IF GET_VEHICLE_PED_IS_IN(pedId) != vehicleID
					GENERIC_TASK_EXIT_VEHICLE(iPed, pedId)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC GENERIC_TASK_STOP_VEHICLE(INT iPed, PED_INDEX pedId)

	IF IS_PED_LOCAL_BIT_SET(iPed, ePEDLOCALBITSET_IN_ANY_VEHICLE)
		IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_VEHICLE_PARK)
				TASK_VEHICLE_PARK(pedID, GET_VEHICLE_PED_IS_IN(pedId), GET_ENTITY_COORDS(pedId), GET_ENTITY_HEADING(pedId), PARK_TYPE_PERPENDICULAR_NOSE_IN)									
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_STOP_VEHICLE - iPed #", iPed, " has been told to stop the vehicle.")
			ENDIF
		ENDIF
	ELSE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_STOP_VEHICLE - iPed #", iPed, " attempting to task ped to stop vehicle when not in vehicle.")
	ENDIF

ENDPROC

FUNC STRING GET_PED_ANIM_DICT(INT iPed)
	
	UNUSED_PARAMETER(iPed)
	
	#IF MAX_NUM_ANIMATION_DICTIONARIES
	IF data.Ped.Peds[iPed].iAnimIndex != -1
		INT iDict = data.Animation.Anim[data.Ped.Peds[iPed].iAnimIndex].iDictIndex
		IF iDict != -1
			RETURN CONVERT_TEXT_LABEL_TO_STRING(data.Animation.Dict[iDict].sName)
		ENDIF
	ENDIF
	#ENDIF
	
	RETURN ""
	
ENDFUNC

FUNC STRING GET_PED_ANIM(INT iPed)
	
	UNUSED_PARAMETER(iPed)
	
	#IF MAX_NUM_ANIMATIONS
	IF data.Ped.Peds[iPed].iAnimIndex != -1
		RETURN CONVERT_TEXT_LABEL_TO_STRING(data.Animation.Anim[data.Ped.Peds[iPed].iAnimIndex].sName)
	ENDIF
	#ENDIF
	
	RETURN ""
	
ENDFUNC

FUNC ANIMATION_FLAGS GET_PED_ANIM_FLAGS(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN AF_LOOPING
ENDFUNC

PROC GENERIC_TASK_PLAY_ANIM(INT iPed, PED_INDEX pedId)

	STRING animDict = CALL logic.Ped.Task.PlayAnim.AnimDict(iPed)
	STRING anim = CALL logic.Ped.Task.PlayAnim.Anim(iPed)

	IF (NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_PLAY_ANIM) 
	AND NOT IS_STRING_NULL_OR_EMPTY(animDict))
	OR NOT IS_ENTITY_PLAYING_ANIM(pedId, animDict, anim, ANIM_SCRIPT)
		
		REQUEST_ANIM_DICT(animDict)

		IF HAS_ANIM_DICT_LOADED(animDict) AND NOT IS_STRING_NULL_OR_EMPTY(anim)
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
				TASK_PLAY_ANIM(pedId, animDict, anim, DEFAULT, DEFAULT, DEFAULT, CALL logic.Ped.Task.PlayAnim.AnimFlags(iPed))
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_PLAY_ANIM - iPed #", iPed, " using TASK_PLAY_ANIM with pAnimDictName = ", animDict, " and pAnimName = ", anim)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC ON_ATTACK_HATED_TARGETS(INT iPed, PED_INDEX pedId)
	IF IS_STRING_NULL_OR_EMPTY(CALL logic.Ped.Task.DoScenario.ScenarioName(iPed))
	OR ARE_STRINGS_EQUAL(CALL logic.Ped.Task.DoScenario.ScenarioName(iPed), "WORLD_HUMAN_PUSH_UPS")
	OR ARE_STRINGS_EQUAL(CALL logic.Ped.Task.DoScenario.ScenarioName(iPed), "WORLD_HUMAN_BUM_SLUMPED")
	OR ARE_STRINGS_EQUAL(CALL logic.Ped.Task.DoScenario.ScenarioName(iPed), "WORLD_HUMAN_SUNBATHE_BACK")
	OR ARE_STRINGS_EQUAL(CALL logic.Ped.Task.DoScenario.ScenarioName(iPed), "WORLD_HUMAN_SEAT_LEDGE_EATING")
	OR ARE_STRINGS_EQUAL(CALL logic.Ped.Task.DoScenario.ScenarioName(iPed), "WORLD_HUMAN_SEAT_STEPS")
	OR ARE_STRINGS_EQUAL(CALL logic.Ped.Task.DoScenario.ScenarioName(iPed), "WORLD_HUMAN_SEAT_WALL")
	OR ARE_STRINGS_EQUAL(CALL logic.Ped.Task.DoScenario.ScenarioName(iPed), "WORLD_HUMAN_SEAT_LEDGE")
		EXIT
	ENDIF
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] ON_ATTACK_HATED_TARGETS - Calling SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT for iPed #", iPed)
	SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedId)
ENDPROC

FUNC VECTOR GET_ATTACK_HATED_TARGETS_COORDS(INT iPed)
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iPed].netId)
		IF NOT IS_ENTITY_DEAD(NET_TO_PED(serverBD.sPed[iPed].netId))
			RETURN GET_ENTITY_COORDS(NET_TO_PED(serverBD.sPed[iPed].netId))
		ENDIF
	ENDIF
	
	ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_ATTACK_HATED_TARGETS_COORDS - Ped must be dead or does not exist when being tasked. Returning zero vector.")
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC FLOAT GET_ATTACK_HATED_TARGETS_RANGE(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN 299.9
ENDFUNC

PROC SET_ADDITIONAL_ATTACK_ATTRIBUTES(INT iPed, PED_INDEX pedID)
	IF NOT DOES_PED_HAVE_ANY_DEFENSIVE_AREA(iPed)
		REMOVE_PED_DEFENSIVE_AREA(pedID)
	ELSE
		SETUP_PED_DEFENSIVE_AREAS(iPed, pedID)
	ENDIF
	
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, FALSE)
	ENDIF
	
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_ALLOW_BUZZARD_CHAINGUN_USE)
		IF SET_CURRENT_PED_VEHICLE_WEAPON(pedID, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
			SET_PED_COMBAT_ATTRIBUTES(pedID, CA_USE_VEHICLE_ATTACK, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedID, CA_USE_VEHICLE_ATTACK_IF_VEHICLE_HAS_MOUNTED_GUNS, TRUE)			
			SET_PED_FIRING_PATTERN(pedID, FIRING_PATTERN_BURST_FIRE_HELI)
			SET_PED_COMBAT_ATTRIBUTES(pedID, CA_FORCE_CHECK_ATTACK_ANGLE_FOR_MOUNTED_GUNS, TRUE)	 
		ENDIF
	ENDIF
ENDPROC

PROC GENERIC_TASK_COMBAT_HATED_TARGETS_IN_AREA(INT iPed, PED_INDEX pedId)
	
	IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
		IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
			SET_ADDITIONAL_ATTACK_ATTRIBUTES(iPed, pedID)
			
			CALL logic.Ped.Task.AttackHatedTargets.OnTask(iPed, pedId)
	
        	TASK_COMBAT_HATED_TARGETS_IN_AREA(pedId, CALL logic.Ped.Task.AttackHatedTargets.Coords(iPed), CALL logic.Ped.Task.AttackHatedTargets.Range(iPed))
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_ATTACK_HATED_TARGETS - iPed #", iPed, " using TASK_COMBAT_HATED_TARGETS_IN_AREA.")
		ENDIF
	ENDIF
	
ENDPROC

PROC GENERIC_TASK_AIM_AT_COORD(INT iPed, PED_INDEX pedId)

	IF logic.Ped.Task.AimAtCoord.Coords != NULL
		IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_AIM_GUN_AT_COORD)
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
				SET_PED_COMBAT_ATTRIBUTES(pedID, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
				
				TASK_AIM_GUN_AT_COORD(pedID, CALL logic.Ped.Task.AimAtCoord.Coords(iPed), -1)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_AIM_AT_COORD - iPed #", iPed, " using task TASK_AIM_GUN_AT_COORD.")
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC GENERIC_TASK_AIM_AT_ENTITY(INT iPed, PED_INDEX pedId)

	IF logic.Ped.Task.AimAtEntity.Entity != NULL
		IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_AIM_GUN_AT_ENTITY)
			ENTITY_INDEX entId = CALL logic.Ped.Task.AimAtEntity.Entity(iPed)
			
			IF DOES_ENTITY_EXIST(entId)
			AND NOT IS_ENTITY_DEAD(entId)
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
					SET_PED_COMBAT_ATTRIBUTES(pedID, CA_CAN_SHOOT_WITHOUT_LOS, TRUE)
					
					TASK_AIM_GUN_AT_ENTITY(pedID, entId, -1)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_AIM_AT_ENTITY - iPed #", iPed, " using task TASK_AIM_GUN_AT_ENTITY.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC FIRING_TYPE GET_SHOOT_AT_TARGET_FIRING_TYPE()
	RETURN FIRING_TYPE_CONTINUOUS
ENDFUNC

PROC GENERIC_TASK_SHOOT_AT_TARGET(INT iPed, PED_INDEX pedId)
	
	IF logic.Ped.Task.ShootAtTarget.Entity != NULL
		IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_SHOOT_AT_ENTITY)
			NETWORK_INDEX netId = CALL logic.Ped.Task.ShootAtTarget.Entity(iPed, pedId)
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netId)
			AND NOT IS_ENTITY_DEAD(NET_TO_ENT(netId))
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
					TASK_SHOOT_AT_ENTITY(pedId, NET_TO_ENT(netId), -1, CALL logic.Ped.Task.ShootAtTarget.FiringType())
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_SHOOT_AT_TARGET - iPed #", iPed, " using TASK_SHOOT_AT_ENTITY.")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC VECTOR GET_THROW_PROJECTILE_COORD(INT iPed, PED_INDEX pedId)
	IF logic.Ped.Task.ThrowProjectile.Coord != NULL
		RETURN CALL logic.Ped.Task.ThrowProjectile.Coord(iPed, pedId)
	ENDIF
	RETURN ZERO_VECTOR()
ENDFUNC

FUNC FLOAT GET_THROW_PROJECTILE_MIN_DISTANCE_FROM_COORD(INT iPed)
	IF logic.Ped.Task.ThrowProjectile.MinDistance != NULL
		RETURN CALL logic.Ped.Task.ThrowProjectile.MinDistance(iPed)
	ENDIF
	RETURN -1.0
ENDFUNC

FUNC WEAPON_TYPE GET_THROW_PROJECTILE_WEAPON_TYPE(INT iPed)
	IF logic.Ped.Task.ThrowProjectile.WeaponType != NULL
		RETURN CALL logic.Ped.Task.ThrowProjectile.WeaponType(iPed)
	ENDIF
	RETURN WEAPONTYPE_INVALID
ENDFUNC

FUNC INT GET_THROW_PROJECTILE_AMMO_COUNT(INT iPed)
	IF logic.Ped.Task.ThrowProjectile.AmmoCount != NULL
		RETURN CALL logic.Ped.Task.ThrowProjectile.AmmoCount(iPed)
	ENDIF
	RETURN 1
ENDFUNC

FUNC BOOL SHOULD_MARK_THROW_PROJECTILE_AS_DONE(INT iPed)
	IF logic.Ped.Task.ThrowProjectile.MarkAsDone != NULL
		RETURN CALL logic.Ped.Task.ThrowProjectile.MarkAsDone(iPed)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GENERIC_TASK_THROW_PROJECTILE(INT iPed, PED_INDEX pedId)

	IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_THROWN_PROJECTILE)
	AND NOT IS_PED_CLIENT_BIT_SET(iPed, LOCAL_PARTICIPANT_INDEX, ePEDCLIENTBITSET_THROWN_PROJECTILE)
		VECTOR vCoord = GET_THROW_PROJECTILE_COORD(iPed, pedId)
	
		IF NOT IS_VECTOR_ZERO(vCoord)
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
				IF GET_THROW_PROJECTILE_MIN_DISTANCE_FROM_COORD(iPed) != -1.0
				AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(pedID, vCoord) > GET_THROW_PROJECTILE_MIN_DISTANCE_FROM_COORD(iPed)
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
					//IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD)
						//TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(pedId, vCoord, vCoord, PEDMOVEBLENDRATIO_RUN, FALSE, DEFAULT, DEFAULT, DEFAULT, ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						TASK_FOLLOW_NAV_MESH_TO_COORD(pedID, vCoord, PEDMOVEBLENDRATIO_RUN, DEFAULT, GET_THROW_PROJECTILE_MIN_DISTANCE_FROM_COORD(iPed), ENAV_GO_FAR_AS_POSSIBLE_IF_TARGET_NAVMESH_NOT_LOADED)
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_THROW_PROJECTILE - iPed #", iPed, " using TASK_FOLLOW_NAV_MESH_TO_COORD as they are too far away.")
					ENDIF
				ELSE
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_THROW_PROJECTILE)
						
						GIVE_WEAPON_TO_PED(pedId, GET_THROW_PROJECTILE_WEAPON_TYPE(iPed),  GET_THROW_PROJECTILE_AMMO_COUNT(iPed), TRUE)
						SET_CURRENT_PED_WEAPON(pedId, GET_THROW_PROJECTILE_WEAPON_TYPE(iPed), TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( pedId, TRUE )
						
						TASK_THROW_PROJECTILE(pedId, vCoord, DEFAULT, TRUE)
						
						IF SHOULD_MARK_THROW_PROJECTILE_AS_DONE(iPed)
							SET_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_THROWN_PROJECTILE)
						ENDIF
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_THROW_PROJECTILE - iPed #", iPed, " using TASK_THROW_PROJECTILE.")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

FUNC NETWORK_INDEX GET_CRASH_VEHICLE_NET_ID(INT iPed, PED_INDEX pedId)
	
	IF data.Ped.Peds[iPed].iVehicle != -1
		RETURN serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId
	ENDIF
	
	VEHICLE_INDEX vehId 
	IF IS_PED_IN_ANY_VEHICLE(pedId)
		vehId = GET_VEHICLE_PED_IS_IN(pedId)
		RETURN VEH_TO_NET(vehId)
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC VECTOR GET_CRASH_TASK_COORDS(INT iPed, PED_INDEX pedId)

	IF logic.Ped.Task.Crash.Coords != NULL
		RETURN CALL logic.Ped.Task.Crash.Coords(iPed)
	ENDIF
	
	RETURN GET_ENTITY_COORDS(pedId)

ENDFUNC

PROC GENERIC_TASK_CRASH(INT iPed, PED_INDEX pedId)

	NETWORK_INDEX vehicleNetId = GET_CRASH_VEHICLE_NET_ID(iPed, pedId)
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(vehicleNetId)
		IF NOT IS_ENTITY_DEAD(NET_TO_VEH(vehicleNetId))
			IF GET_ACTIVE_VEHICLE_MISSION_TYPE(NET_TO_VEH(vehicleNetId)) != MISSION_CRASH
				IF IS_PED_IN_ANY_PLANE(pedId)
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
						IF logic.Ped.Task.Crash.OnTask != NULL
							CALL logic.Ped.Task.Crash.OnTask(iPed)
						ENDIF
						
						VECTOR vCrashCoords = GET_CRASH_TASK_COORDS(iPed, pedId)
						TASK_PLANE_MISSION(pedId, NET_TO_VEH(vehicleNetId), NULL, NULL, vCrashCoords, MISSION_CRASH, 50.0, 0, -1, 0, 0)
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_CRASH - iPed #", iPed, " has been told to crash vehicle at ", vCrashCoords)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC NETWORK_INDEX GET_VEHICLE_GO_TO_POINT_VEHICLE(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.Vehicle != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.Vehicle(iPed)
	ENDIF
	
	IF data.Ped.Peds[iPed].iVehicle != -1
		RETURN serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC INT GET_NUM_VEHICLE_GO_TO_POINT_COORDS(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.NumCoords != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.NumCoords(iPed)
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC VECTOR GET_VEHICLE_GO_TO_POINT_COORDS(INT iPed, INT iCheckpointOverride = -1)
	INT iCheckpointStage = serverBD.sPed[iPed].iCheckpointStage
	
	IF iCheckpointOverride != -1
		iCheckpointStage = iCheckpointOverride
	ENDIF
	
	VECTOR vCoords
	
	IF logic.Ped.Task.VehicleGoToPoint.Coords != NULL
		vCoords = CALL logic.Ped.Task.VehicleGoToPoint.Coords(iPed, iCheckpointStage)
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_VECTOR_ZERO(vCoords)
		ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_VEHICLE_GO_TO_POINT_COORDS returning <<0.0, 0.0, 0.0>>.")
	ENDIF
	#ENDIF
	
	RETURN vCoords
ENDFUNC

FUNC FLOAT GET_VEHICLE_GO_TO_POINT_SPEED(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.Speed != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.Speed(iPed)
	ENDIF
	
	RETURN 35.0
ENDFUNC

FUNC BOATMODE GET_VEHICLE_GO_TO_POINT_BOAT_SETTINGS(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.BoatSettings != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.BoatSettings(iPed)
	ENDIF
	
	RETURN BCF_DEFAULTSETTINGS
ENDFUNC

FUNC FLOAT GET_VEHICLE_GO_TO_POINT_RADIUS_SERVER(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.RadiusServer != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.RadiusServer(iPed)
	ENDIF
	
	RETURN 10.0
ENDFUNC

FUNC FLOAT GET_VEHICLE_GO_TO_POINT_RADIUS_CLIENT(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.RadiusClient != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.RadiusClient(iPed)
	ENDIF
	
	RETURN 5.0
ENDFUNC

FUNC BOOL SHOULD_USE_LONG_RANGE_VEHICLE_GO_TO_POINT_TASK(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.ShouldUseLongRangeTask != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.ShouldUseLongRangeTask(iPed)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC SCRIPT_TASK_NAME GET_VEHICLE_GO_TO_POINT_TASK(INT iPed)
	IF SHOULD_USE_LONG_RANGE_VEHICLE_GO_TO_POINT_TASK(iPed)
		RETURN SCRIPT_TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED
	ENDIF
	
	RETURN SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD
ENDFUNC

FUNC DRIVINGSTYLE GET_VEHICLE_GO_TO_POINT_DRIVING_STYLE(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.DriveStyle != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.DriveStyle(iPed)
	ENDIF
	
	RETURN DRIVINGSTYLE_NORMAL
ENDFUNC

FUNC DRIVINGMODE GET_VEHICLE_GO_TO_POINT_DRIVING_MODE(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.DriveMode != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.DriveMode(iPed)
	ENDIF
	
	RETURN DF_SwerveAroundAllCars | DF_SteerAroundObjects | DF_UseShortCutLinks | DF_UseSwitchedOffNodes
ENDFUNC

FUNC FLOAT GET_VEHICLE_GO_TO_POINT_STRAIGHT_LINE_DISTANCE(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.StraightLineDistance != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.StraightLineDistance(iPed)
	ENDIF
	
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_VEHICLE_GO_TO_POINT_MOVE_BLEND_RATIO(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.MoveBlendRatio != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.MoveBlendRatio(iPed)
	ENDIF
	
	RETURN 0.0
ENDFUNC

FUNC TASK_GO_TO_COORD_ANY_MEANS_FLAGS GET_VEHICLE_GO_TO_POINT_TGCAM_FLAGS(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.TgcamFlags != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.TgcamFlags(iPed)
	ENDIF
	
	RETURN TGCAM_NEVER_ABANDON_VEHICLE | TGCAM_REMAIN_IN_VEHICLE_AT_DESTINATION
ENDFUNC

FUNC BOOL SHOULD_DO_VEHICLE_GO_TO_POINT_ON_FOOT(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.ShouldDoOnFoot != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.ShouldDoOnFoot(iPed)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_REENTER_VEHICLE_DURING_VEHICLE_GO_TO_POINT_TASK()
	IF logic.Ped.Task.VehicleGoToPoint.ShouldReenterVehicle != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.ShouldReenterVehicle()
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_VEHICLE_GO_TO_POINT_VEHICLE_FORWARD_SPEED(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.ForwardSpeed != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.ForwardSpeed(iPed)
	ENDIF
	
	RETURN -1.0
ENDFUNC

FUNC FLOAT GET_VEHICLE_GO_TO_POINT_HELI_CRUISE_SPEED(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.HeliCruiseSpeed != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.HeliCruiseSpeed(iPed)
	ENDIF
	
	RETURN 30.0
ENDFUNC

FUNC FLOAT GET_VEHICLE_GO_TO_POINT_HELI_FLIGHT_HEIGHT()
	IF logic.Ped.Task.VehicleGoToPoint.HeliFlightHeight != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.HeliFlightHeight()
	ENDIF
	
	RETURN 80.0
ENDFUNC

FUNC INT GET_VEHICLE_GO_TO_POINT_HELI_MIN_FLIGHT_HEIGHT_ABOVE_TERRAIN()
	IF logic.Ped.Task.VehicleGoToPoint.HeliMinFlightHeightAboveTerrain != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.HeliMinFlightHeightAboveTerrain()
	ENDIF
	
	RETURN 80
ENDFUNC

FUNC FLOAT GET_VEHICLE_GO_TO_POINT_HELI_ORIENTATION(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.HeliOrientation != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.HeliOrientation(iPed)
	ENDIF
	
	RETURN -1.0
ENDFUNC

FUNC BOOL SHOULD_CLEAR_PED_TASKS_DURING_VEHICLE_GO_TO_POINT(INT iPed)
	IF logic.Ped.Task.VehicleGoToPoint.ShouldClearTasks != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.ShouldClearTasks(iPed)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_VEHICLE_GO_TO_POINT_VEHICLE_ATTRIBUTES(VEHICLE_INDEX vehId)
	IF IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehId))
		SET_BOAT_ANCHOR(vehId, FALSE)
	ENDIF
	
	IF IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(vehId))
		IF IS_ENTITY_IN_AIR(vehId)
			CONTROL_LANDING_GEAR(vehId, LGC_RETRACT)
		ENDIF
	ENDIF
	
	SET_VEHICLE_ENGINE_ON(vehId, TRUE, TRUE)
ENDPROC

FUNC BOOL HAS_PED_REACHED_VEHICLE_GO_TO_POINT_CHECKPOINT_SERVER(INT iPed)
	
	PED_INDEX pedID = NET_TO_PED(serverBD.sPed[iPed].netId)
	VECTOR vCheckpoint = GET_VEHICLE_GO_TO_POINT_COORDS(iPed)
	
	IF IS_ENTITY_IN_RANGE_COORDS(pedID, vCheckpoint, GET_VEHICLE_GO_TO_POINT_RADIUS_SERVER(iPed))
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] HAS_PED_REACHED_CHECKPOINT_SERVER - IS_ENTITY_IN_RANGE_COORDS = TRUE for iPed #", iPed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_VEHICLE_GO_TO_COORDS_SHOULD_CHECK_3D_DISTANCE(INT iPed, INT iCheckpointStage)
	IF logic.Ped.Task.VehicleGoToPoint.ShouldCheck3dDistance != NULL
		RETURN CALL logic.Ped.Task.VehicleGoToPoint.ShouldCheck3dDistance(iPed,iCheckpointStage)
	ENDIF

	RETURN FALSE
ENDFUNC

PROC GENERIC_TASK_VEHICLE_GO_TO_POINT_SERVER(INT iPed, PED_INDEX pedId)
	VECTOR vNextCheckpoint = GET_VEHICLE_GO_TO_POINT_COORDS(iPed)
	
	FLOAT fDistanceToCheckpoint
	
	IF logic.Ped.Task.VehicleGoToPoint.ShouldUsePedBoneCoords != NULL
	AND CALL logic.Ped.Task.VehicleGoToPoint.ShouldUsePedBoneCoords(iPed)
		fDistanceToCheckpoint = GET_DISTANCE_BETWEEN_COORDS(vNextCheckpoint, GET_PED_BONE_COORDS(pedId, BONETAG_NULL, <<0.0, 0.0, 0.0>>), GET_VEHICLE_GO_TO_COORDS_SHOULD_CHECK_3D_DISTANCE(iPed, serverBD.sPed[iPed].iCheckpointStage))
	ELSE
		fDistanceToCheckpoint = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(pedID, vNextCheckpoint, GET_VEHICLE_GO_TO_COORDS_SHOULD_CHECK_3D_DISTANCE(iPed,serverBD.sPed[iPed].iCheckpointStage))
	ENDIF
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_GO_TO_POINT_SERVER - iPed #", iPed, ", vNextCheckpoint = ", vNextCheckpoint, ", fDistanceToCheckpoint = ", fDistanceToCheckpoint)
	
	// Used for logic that should be run when the ped is nearing a checkpoint, not for when it has been reached.
	IF logic.Ped.Task.VehicleGoToPoint.MaintainNearCheckpoint != NULL
		CALL logic.Ped.Task.VehicleGoToPoint.MaintainNearCheckpoint(iPed, pedId, fDistanceToCheckpoint)
	ENDIF
	
	IF logic.Ped.Task.VehicleGoToPoint.OnCheckpointTimeoutExpired != NULL
		IF NOT HAS_NET_TIMER_STARTED(serverBD.sPed[iPed].stCheckpointTimeoutTimer)
			START_NET_TIMER(serverBD.sPed[iPed].stCheckpointTimeoutTimer)
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED(serverBD.sPed[iPed].stCheckpointTimeoutTimer, 60000)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_GO_TO_POINT_SERVER - Checkpoint timeout has expired for iPed #", iPed)
			CALL logic.Ped.Task.VehicleGoToPoint.OnCheckpointTimeoutExpired(iPed, pedId)
		ENDIF
	ENDIF
	
	// Used for when the ped has actually reached the checkpoint.
	IF fDistanceToCheckpoint <= GET_VEHICLE_GO_TO_POINT_RADIUS_SERVER(iPed)
	OR HAS_PED_REACHED_VEHICLE_GO_TO_POINT_CHECKPOINT_SERVER(iPed)
		IF logic.Ped.Task.VehicleGoToPoint.OnReachedCheckpoint != NULL
			CALL logic.Ped.Task.VehicleGoToPoint.OnReachedCheckpoint(iPed, pedId)
		ENDIF
		
		IF HAS_NET_TIMER_STARTED(serverBD.sPed[iPed].stCheckpointTimeoutTimer)
			RESET_NET_TIMER(serverBD.sPed[iPed].stCheckpointTimeoutTimer)
		ENDIF
		
		serverBD.sPed[iPed].iCheckpointStage++
		IF serverBD.sPed[iPed].iCheckpointStage >= GET_NUM_VEHICLE_GO_TO_POINT_COORDS(iPed)
			serverBD.sPed[iPed].iCheckpointStage = 0
			
			// Ped has reached the final checkpoint.
			SET_PED_BIT(iPed, ePEDBITSET_I_HAVE_REACHED_FINAL_CHECKPOINT)
			SET_GENERIC_SERVER_BIT(eGENERICSERVERBITSET_ENTITY_REACHED_FINAL_CHECKPOINT)
		ENDIF
			
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_GO_TO_POINT_SERVER - iPed #", iPed, " has reached ", vNextCheckpoint, " (Checkpoint #", serverBD.sPed[iPed].iCheckpointOnRouteTo, "). Next checkpoint is #", serverBD.sPed[iPed].iCheckpointStage, ".")
	ENDIF
ENDPROC

PROC GENERIC_TASK_VEHICLE_GO_TO_POINT_CLIENT(INT iPed, PED_INDEX pedId)
	BOOL bOnFoot = SHOULD_DO_VEHICLE_GO_TO_POINT_ON_FOOT(iPed)
	NETWORK_INDEX vehicleNetId = GET_VEHICLE_GO_TO_POINT_VEHICLE(iPed)
	
	IF bOnFoot
	OR NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(vehicleNetId)
		VEHICLE_INDEX vehId
		
		IF bOnFoot
			SET_PED_COMBAT_ATTRIBUTES(pedId, CA_USE_VEHICLE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(pedId, CA_LEAVE_VEHICLES, TRUE)
		ELSE
			vehId = NET_TO_VEH(vehicleNetId)
		ENDIF
		
		#IF IS_DEBUG_BUILD
		INT i
		REPEAT GET_NUM_VEHICLE_GO_TO_POINT_COORDS(iPed) i
			IF sDebugVars.bDrawVehicleGoToPointCheckpoints
				//Draw a corona for the checkpoint update distance (yellow for the checkpoint the ped is heading towards, blue for the checkpoint they are waiting at, red for the others).
				IF i = serverBD.sPed[iPed].iCheckpointOnRouteTo
					IF serverBD.sPed[iPed].iCheckpointOnRouteTo != serverBD.sPed[iPed].iCheckpointStage
						DRAW_DEBUG_CORONA(GET_VEHICLE_GO_TO_POINT_COORDS(iPed, i), GET_VEHICLE_GO_TO_POINT_RADIUS_SERVER(iPed), HUD_COLOUR_BLUE)
					ELSE
						DRAW_DEBUG_CORONA(GET_VEHICLE_GO_TO_POINT_COORDS(iPed, i), GET_VEHICLE_GO_TO_POINT_RADIUS_SERVER(iPed))
					ENDIF
				ELSE
					DRAW_DEBUG_CORONA(GET_VEHICLE_GO_TO_POINT_COORDS(iPed, i), GET_VEHICLE_GO_TO_POINT_RADIUS_SERVER(iPed), HUD_COLOUR_RED, 200)
				ENDIF
				
				//Draw a corona for the straight line distance.
				DRAW_DEBUG_CORONA(GET_VEHICLE_GO_TO_POINT_COORDS(iPed, i), GET_VEHICLE_GO_TO_POINT_STRAIGHT_LINE_DISTANCE(iPed), HUD_COLOUR_WHITE)
				
				//Draw a corona for the target reached distance.
				DRAW_DEBUG_CORONA(GET_VEHICLE_GO_TO_POINT_COORDS(iPed, i), GET_VEHICLE_GO_TO_POINT_RADIUS_CLIENT(iPed), HUD_COLOUR_BLACK)
			ENDIF
			
			IF sDebugVars.bDrawVehicleGoToPointCheckpointIDs
				TEXT_LABEL_15 tlCheckpoint = "Checkpoint #"
				tlCheckpoint += i
				DRAW_DEBUG_TEXT_ABOVE_COORDS(GET_VEHICLE_GO_TO_POINT_COORDS(iPed, i), tlCheckpoint, 2.0)
				
				TEXT_LABEL_15 tlDebug = "Checkpoint #"
				tlDebug += serverBD.sPed[iPed].iCheckpointOnRouteTo
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(pedId, tlDebug, 1.0)
			ENDIF
		ENDREPEAT
		#ENDIF
		
		IF bOnFoot OR NOT IS_ENTITY_DEAD(vehId)
			IF SHOULD_REENTER_VEHICLE_DURING_VEHICLE_GO_TO_POINT_TASK()
			AND NOT bOnFoot
			AND NOT IS_PED_IN_VEHICLE(pedId, vehId)
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_ENTER_VEHICLE)
					OR GET_VEHICLE_PED_IS_ENTERING(pedID) != vehId
						CALL logic.Ped.Task.EnterVehicle.OnTask(pedId)
						
						TASK_ENTER_VEHICLE(pedID, vehID,
											CALL logic.Ped.Task.EnterVehicle.WarpTime(iPed),
											CALL logic.Ped.Task.EnterVehicle.Seat(iPed),
											CALL logic.Ped.Task.EnterVehicle.MoveBlendRatio(iPed),
											CALL logic.Ped.Task.EnterVehicle.EnterExitVehicleFlags(iPed))
						
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_GO_TO_POINT_CLIENT - iPed #", iPed, " tasked to enter vehicle during vehicle go to point.")
					ENDIF
				ENDIF
			ELIF IS_PED_IN_ANY_HELI(pedId)
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_VEHICLE_MISSION)
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
						VECTOR vCoords = GET_VEHICLE_GO_TO_POINT_COORDS(iPed)
						
						SET_PED_CONFIG_FLAG(pedId, PCF_DisableStartEngine, FALSE)	
						
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehID, FALSE) 
						SET_HELI_BLADES_SPEED(vehID, 1.0)
						SET_HELI_BLADES_FULL_SPEED(vehId)
						SET_VEHICLE_ENGINE_ON(vehId, TRUE, TRUE)
						
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_GO_TO_POINT_CLIENT SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehId, FALSE) ")
						
						IF logic.Ped.Task.VehicleGoToPoint.OnTask != NULL
							CALL logic.Ped.Task.VehicleGoToPoint.OnTask(iPed, pedID)
						ENDIF
						
						TASK_HELI_MISSION(pedId, vehID, NULL, NULL, vCoords, MISSION_GOTO,
											GET_VEHICLE_GO_TO_POINT_HELI_CRUISE_SPEED(iPed), 10.0, GET_VEHICLE_GO_TO_POINT_HELI_ORIENTATION(iPed),
											ROUND(GET_VEHICLE_GO_TO_POINT_HELI_FLIGHT_HEIGHT()),
											GET_VEHICLE_GO_TO_POINT_HELI_MIN_FLIGHT_HEIGHT_ABOVE_TERRAIN(), -1, HF_StartEngineImmediately)
						
						IF serverBD.sPed[iPed].iCheckpointOnRouteTo != serverBD.sPed[iPed].iCheckpointStage
							BROADCAST_EVENT_FREEMODE_CONTENT_UPDATE_CURRENT_CHECKPOINT(serverBD.sPed[iPed].iCheckpointStage, iPed)
						ENDIF
						
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_GO_TO_POINT_CLIENT - iPed #", iPed, " has been told to fly to ", vCoords, " (Checkpoint #", serverBD.sPed[iPed].iCheckpointStage, ") with speed ", GET_VEHICLE_GO_TO_POINT_HELI_CRUISE_SPEED(iPed)," .")
					ENDIF
				ELSE
					IF serverBD.sPed[iPed].iCheckpointOnRouteTo != serverBD.sPed[iPed].iCheckpointStage
						IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
							CLEAR_PED_TASKS(pedId)
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_GO_TO_POINT_CLIENT - iPed #", iPed, "'s current checkpoint is ", serverBD.sPed[iPed].iCheckpointStage, " but currently on route to Checkpoint #", serverBD.sPed[iPed].iCheckpointOnRouteTo, ". Clearing ped tasks.")
						ENDIF
					ENDIF
				ENDIF
			ELIF IS_PED_IN_ANY_PLANE(pedId)
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_VEHICLE_MISSION)
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
						VECTOR vCoords = GET_VEHICLE_GO_TO_POINT_COORDS(iPed)
						
						SET_PED_CONFIG_FLAG(pedId, PCF_DisableStartEngine, FALSE)	
						
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehID, FALSE) 
						SET_VEHICLE_ENGINE_ON(vehId, TRUE, TRUE)
						
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_GO_TO_POINT_CLIENT SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehId, FALSE) ")
						
						IF logic.Ped.Task.VehicleGoToPoint.OnTask != NULL
							CALL logic.Ped.Task.VehicleGoToPoint.OnTask(iPed, pedID)
						ENDIF
						
						TASK_PLANE_MISSION(pedId, vehID, NULL, NULL, vCoords, MISSION_GOTO,
											GET_VEHICLE_GO_TO_POINT_HELI_CRUISE_SPEED(iPed), 
											GET_VEHICLE_GO_TO_POINT_RADIUS_CLIENT(iPed), 
											GET_VEHICLE_GO_TO_POINT_HELI_ORIENTATION(iPed),
											ROUND(GET_VEHICLE_GO_TO_POINT_HELI_FLIGHT_HEIGHT()),
											GET_VEHICLE_GO_TO_POINT_HELI_MIN_FLIGHT_HEIGHT_ABOVE_TERRAIN())
						
						IF serverBD.sPed[iPed].iCheckpointOnRouteTo != serverBD.sPed[iPed].iCheckpointStage
							BROADCAST_EVENT_FREEMODE_CONTENT_UPDATE_CURRENT_CHECKPOINT(serverBD.sPed[iPed].iCheckpointStage, iPed)
						ENDIF
						
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_GO_TO_POINT_CLIENT - iPed #", iPed, " has been told to fly to ", vCoords, " (Checkpoint #", serverBD.sPed[iPed].iCheckpointStage, ") with speed ", GET_VEHICLE_GO_TO_POINT_HELI_CRUISE_SPEED(iPed)," .")
					ENDIF
				ELSE
					IF serverBD.sPed[iPed].iCheckpointOnRouteTo != serverBD.sPed[iPed].iCheckpointStage
						IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
							CLEAR_PED_TASKS(pedId)
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_GO_TO_POINT_CLIENT - iPed #", iPed, "'s current checkpoint is ", serverBD.sPed[iPed].iCheckpointStage, " but currently on route to Checkpoint #", serverBD.sPed[iPed].iCheckpointOnRouteTo, ". Clearing ped tasks.")
						ENDIF
					ENDIF
				ENDIF
			ELIF IS_PED_IN_ANY_BOAT(pedId)
				IF GET_ACTIVE_VEHICLE_MISSION_TYPE(NET_TO_VEH(vehicleNetId)) != MISSION_GOTO			
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
						FLOAT fSpeed = GET_VEHICLE_GO_TO_POINT_SPEED(iPed)
						VECTOR vCoords = GET_VEHICLE_GO_TO_POINT_COORDS(iPed)
						
						IF logic.Ped.Task.VehicleGoToPoint.OnTask != NULL
							CALL logic.Ped.Task.VehicleGoToPoint.OnTask(iPed, pedID)
						ENDIF
						
						SET_BOAT_ANCHOR(vehID, FALSE)
						TASK_BOAT_MISSION(pedId, vehID, NULL, NULL, vCoords, MISSION_GOTO, fSpeed, DRIVINGMODE_AVOIDCARS, -1.0, GET_VEHICLE_GO_TO_POINT_BOAT_SETTINGS(iPed))
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_GO_TO_POINT_CLIENT - iPed #", iPed, " has been told sail to vCoords =", vCoords, " (Checkpoint #", serverBD.sPed[iPed].iCheckpointStage, ") at fSpeed = ", fSpeed)
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, GET_VEHICLE_GO_TO_POINT_TASK(iPed))
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
					AND (bOnFoot OR MAINTAIN_CONTROL_OF_NETWORK_ID(vehicleNetId))
						VECTOR vCoords = GET_VEHICLE_GO_TO_POINT_COORDS(iPed)
						FLOAT fSpeed = GET_VEHICLE_GO_TO_POINT_SPEED(iPed)
						FLOAT fStraightDist = GET_VEHICLE_GO_TO_POINT_STRAIGHT_LINE_DISTANCE(iPed)
						
						IF NOT bOnFoot
							SET_VEHICLE_GO_TO_POINT_VEHICLE_ATTRIBUTES(vehId)
						ENDIF
						
						IF logic.Ped.Task.VehicleGoToPoint.OnTask != NULL
							CALL logic.Ped.Task.VehicleGoToPoint.OnTask(iPed, pedID)
						ENDIF
						
						IF SHOULD_USE_LONG_RANGE_VEHICLE_GO_TO_POINT_TASK(iPed)
							TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED(pedID, vCoords,
																						GET_VEHICLE_GO_TO_POINT_MOVE_BLEND_RATIO(iPed), vehID, TRUE,
																						GET_VEHICLE_GO_TO_POINT_DRIVING_MODE(iPed), DEFAULT, DEFAULT, fStraightDist,
																						GET_VEHICLE_GO_TO_POINT_TGCAM_FLAGS(iPed), fSpeed,
																						GET_VEHICLE_GO_TO_POINT_RADIUS_CLIENT(iPed))
						ELSE
							TASK_VEHICLE_DRIVE_TO_COORD(pedId, vehID, vCoords, fSpeed, 
														GET_VEHICLE_GO_TO_POINT_DRIVING_STYLE(iPed), GET_ENTITY_MODEL(vehID), 
														GET_VEHICLE_GO_TO_POINT_DRIVING_MODE(iPed),
														GET_VEHICLE_GO_TO_POINT_RADIUS_CLIENT(iPed), fStraightDist)
						ENDIF
							
						IF serverBD.sPed[iPed].iCheckpointOnRouteTo != serverBD.sPed[iPed].iCheckpointStage
							BROADCAST_EVENT_FREEMODE_CONTENT_UPDATE_CURRENT_CHECKPOINT(serverBD.sPed[iPed].iCheckpointStage, iPed)
						ENDIF
						
						IF GET_VEHICLE_GO_TO_POINT_VEHICLE_FORWARD_SPEED(iPed) != -1
							IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_SET_FORWARD_SPEED)
							AND NOT IS_PED_CLIENT_BIT_SET(iPed, LOCAL_PARTICIPANT_INDEX, ePEDCLIENTBITSET_I_HAVE_SET_FORWARD_SPEED)
								SET_VEHICLE_FORWARD_SPEED(vehID, GET_VEHICLE_GO_TO_POINT_VEHICLE_FORWARD_SPEED(iPed))
								SET_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_I_HAVE_SET_FORWARD_SPEED)
							ENDIF
						ENDIF
						
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_GO_TO_POINT_CLIENT - iPed #", iPed, " has been told to drive to vCoords = ", vCoords, " (Checkpoint #", serverBD.sPed[iPed].iCheckpointStage, ") at fSpeed = ", fSpeed, " with fStraightDist = ", fStraightDist)
					ENDIF
				ELSE
					IF serverBD.sPed[iPed].iCheckpointOnRouteTo != serverBD.sPed[iPed].iCheckpointStage
						IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
							CLEAR_PED_TASKS(pedId)
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_GO_TO_POINT_CLIENT - iPed #", iPed, "'s current checkpoint is ", serverBD.sPed[iPed].iCheckpointStage, " but currently on route to checkpoint ", serverBD.sPed[iPed].iCheckpointOnRouteTo, ". Clearing ped tasks.")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_CLEARED_TASKS_DURING_VEHICLE_GO_TO_POINT)
			AND NOT IS_PED_CLIENT_BIT_SET(iPed, LOCAL_PARTICIPANT_INDEX, ePEDCLIENTBITSET_I_HAVE_CLEARED_TASKS_DURING_VEHICLE_GO_TO_POINT)
			AND SHOULD_CLEAR_PED_TASKS_DURING_VEHICLE_GO_TO_POINT(iPed)
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
					CLEAR_PED_TASKS(pedID)
					SET_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_I_HAVE_CLEARED_TASKS_DURING_VEHICLE_GO_TO_POINT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC TASK_COMBAT_PED_FLAGS GET_COMBAT_PED_COMBAT_FLAGS(INT iPed)
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		RETURN COMBAT_PED_PREVENT_CHANGING_TARGET
	ENDIF
	
	IF logic.Ped.Task.CombatPed.CombatFlags != null
		RETURN CALL logic.Ped.Task.CombatPed.CombatFlags(iPed)
	ENDIF
	
	RETURN COMBAT_PED_NONE
ENDFUNC

FUNC BOOL GET_COMBAT_PED_TARGET(INT iPed, PED_INDEX& targetPed)
	#IF MAX_NUM_AMBUSH_VEHICLES
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		targetPed = GET_AMBUSH_TARGET_PED_INDEX(GET_AMBUSH_INDEX_FROM_ENTITY(eENTITYTYPE_PED, iPed))
		IF DOES_ENTITY_EXIST(targetPed)
			RETURN TRUE
		ENDIF
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF logic.Ped.Task.CombatPed.Entity != null
		targetPed = CALL logic.Ped.Task.CombatPed.Entity(iPed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TARGET_VALID_FOR_COMBAT(PED_INDEX targetId)
	IF IS_ENTITY_DEAD(targetId)
		#IF IS_DEBUG_BUILD		IF sDebugVars.bDoPedPrints			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] IS_TARGET_VALID_FOR_COMBAT - target ped ", NATIVE_TO_INT(targetId), " is not valid as they are dead.")		ENDIF		#ENDIF
		RETURN FALSE
	ENDIF
	IF IS_PED_A_PLAYER(targetId)
		PLAYER_INDEX targetPlayerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(targetId)
		IF NATIVE_TO_INT(targetPlayerId) != -1
			IF GlobalplayerBD[NATIVE_TO_INT(targetPlayerId)].bRequiresCannotBeTargetedByAI
				#IF IS_DEBUG_BUILD		IF sDebugVars.bDoPedPrints			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] IS_TARGET_VALID_FOR_COMBAT - target player ", GET_PLAYER_NAME(targetPlayerId), " is not valid as bRequiresCannotBeTargetedByAI.")		ENDIF		#ENDIF
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC GENERIC_TASK_COMBAT_PED(INT iPed, PED_INDEX pedID)
	IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedID, SCRIPT_TASK_COMBAT)
		PED_INDEX targetId
		IF GET_COMBAT_PED_TARGET(iPed, targetId)
			IF IS_TARGET_VALID_FOR_COMBAT(targetId)
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netID)
					SET_ADDITIONAL_ATTACK_ATTRIBUTES(iPed, pedID)
					TASK_COMBAT_PED_FLAGS eFlags = GET_COMBAT_PED_COMBAT_FLAGS(iPed)
					TASK_COMBAT_PED(pedId, targetId, eFlags)
					
					#IF IS_DEBUG_BUILD
					IF IS_PED_A_PLAYER(targetId)
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_COMBAT_PED - iPed #", iPed, " has been tasked with combat against PLAYER_", NETWORK_ENTITY_GET_OBJECT_ID(targetId), " with flags ", eFlags)
					ELSE
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_COMBAT_PED - iPed #", iPed, " has been tasked with combat against SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(targetId), " with flags ", eFlags)
					ENDIF
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_WALK_TO_POINT_USE_NAV_MESH(INT iPed)
	IF logic.Ped.Task.WalkToPoint.UseNavMesh != NULL
		RETURN CALL logic.Ped.Task.WalkToPoint.UseNavMesh(iPed)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_WALK_TO_POINT_COORD(INT iPed)
	IF logic.Ped.Task.WalkToPoint.Coords != NULL
		RETURN CALL logic.Ped.Task.WalkToPoint.Coords(iPed)
	ENDIF
	
	RETURN CALL logic.Ped.Coords(iPed)
ENDFUNC

FUNC FLOAT GET_WALK_TO_POINT_PED_MOVE_BLEND_RATIO(INT iPed)
	IF logic.Ped.Task.WalkToPoint.MoveBlendRatio != NULL
		RETURN CALL logic.Ped.Task.WalkToPoint.MoveBlendRatio(iPed)
	ENDIF
	
	RETURN PEDMOVE_WALK
ENDFUNC

FUNC INT GET_WALK_TO_POINT_TIME_BEFORE_WARP()
	IF logic.Ped.Task.WalkToPoint.TimeBeforeWarp != NULL
		RETURN CALL logic.Ped.Task.WalkToPoint.TimeBeforeWarp()
	ENDIF
	
	RETURN DEFAULT_TIME_BEFORE_WARP
ENDFUNC

FUNC FLOAT GET_WALK_TO_POINT_FINAL_HEADING(INT iPed)
	IF logic.Ped.Task.WalkToPoint.FinalHeading != NULL
		RETURN CALL logic.Ped.Task.WalkToPoint.FinalHeading(iPed)
	ENDIF
	
	RETURN 0.0
ENDFUNC

PROC GENERIC_TASK_WALK_TO_POINT(INT iPed, PED_INDEX pedId)
	IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
		SCRIPT_TASK_NAME task = SCRIPT_TASK_GO_TO_COORD_ANY_MEANS
		
		IF SHOULD_WALK_TO_POINT_USE_NAV_MESH(iPed)
			task = SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD
		ENDIF
		
		IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, task)
			VECTOR vCoords = GET_WALK_TO_POINT_COORD(iPed)
			
			SET_PED_COMBAT_ATTRIBUTES(pedId, CA_USE_VEHICLE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(pedId, CA_LEAVE_VEHICLES, TRUE)
			
			SWITCH task
				CASE SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD
					TASK_FOLLOW_NAV_MESH_TO_COORD(pedId, vCoords, GET_WALK_TO_POINT_PED_MOVE_BLEND_RATIO(iPed), GET_WALK_TO_POINT_TIME_BEFORE_WARP(), DEFAULT, ENAV_DONT_ADJUST_TARGET_POSITION, GET_WALK_TO_POINT_FINAL_HEADING(iPed))
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_WALK_TO_POINT - iPed #", iPed, " has been told to walk to vCoords = ", vCoords, " with final heading ", GET_WALK_TO_POINT_FINAL_HEADING(iPed))
				BREAK
				
				CASE SCRIPT_TASK_GO_TO_COORD_ANY_MEANS
					TASK_GO_TO_COORD_ANY_MEANS(pedId, vCoords, GET_WALK_TO_POINT_PED_MOVE_BLEND_RATIO(iPed), NULL)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_WALK_TO_POINT - iPed #", iPed, " has been told to walk to vCoords = ", vCoords)
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
ENDPROC

PROC GENERIC_TASK_RETURN_TO_START(INT iPed, PED_INDEX pedId)
	IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
		IF data.Ped.Peds[iPed].iVehicle != -1
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId)
				VEHICLE_INDEX vehId = NET_TO_VEH(serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId)
				
				IF IS_VEHICLE_DRIVEABLE(vehId)
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_ENTER_VEHICLE)
					OR GET_VEHICLE_PED_IS_ENTERING(pedID) != vehId
						CLEAR_PED_TASKS(pedId)
						TASK_ENTER_VEHICLE(pedID, vehId, -1, data.Ped.Peds[iPed].eSeat, GET_WALK_TO_POINT_PED_MOVE_BLEND_RATIO(iPed), ECF_RESUME_IF_INTERRUPTED)
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_RETURN_TO_START - iPed #", iPed, " has been told to enter iVehicle #", data.Ped.Peds[iPed].iVehicle)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
				VECTOR vCoords = GET_WALK_TO_POINT_COORD(iPed)
				
				SET_PED_COMBAT_ATTRIBUTES(pedID, CA_USE_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(pedID, CA_LEAVE_VEHICLES, TRUE)
				
				TASK_FOLLOW_NAV_MESH_TO_COORD(pedID, vCoords, GET_WALK_TO_POINT_PED_MOVE_BLEND_RATIO(iPed), GET_WALK_TO_POINT_TIME_BEFORE_WARP(), DEFAULT, ENAV_DONT_ADJUST_TARGET_POSITION, GET_WALK_TO_POINT_FINAL_HEADING(iPed))
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_RETURN_TO_START - iPed #", iPed, " has been told to walk to vCoords = ", vCoords, " with final heading ", GET_WALK_TO_POINT_FINAL_HEADING(iPed))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_ACHIEVE_HEADING_DESIRED_HEADING(INT iPed)
	IF logic.Ped.Task.AchieveHeading.DesiredHeading != NULL
		RETURN CALL logic.Ped.Task.AchieveHeading.DesiredHeading(iPed)
	ENDIF
	
	RETURN CALL logic.Ped.Heading(iPed)
ENDFUNC

PROC GENERIC_TASK_ACHIEVE_HEADING(INT iPed, PED_INDEX pedId)
	IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_ACHIEVE_HEADING)
	AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
		TASK_ACHIEVE_HEADING(pedId, GET_ACHIEVE_HEADING_DESIRED_HEADING(iPed))
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_ACHIEVE_HEADING - iPed #", iPed, " has been told to achieve heading ", GET_ACHIEVE_HEADING_DESIRED_HEADING(iPed))
	ENDIF
ENDPROC

#IF MAX_NUM_PATROLS
FUNC TEXT_LABEL_15 GET_PATROL_ROUTE(INT iPatrol)
	IF logic.Ped.Task.Patrol.Route != null	
		RETURN CALL logic.Ped.Task.Patrol.Route(iPatrol)
	ENDIF
	RETURN sPatrol[iPatrol].strName
ENDFUNC

FUNC PATROL_ALERT_STATE GET_PATROL_TASK_ALERT_STATE(INT iPatrol)
	IF logic.Ped.Task.Patrol.AlertState != null
		RETURN CALL logic.Ped.Task.Patrol.AlertState(iPatrol)
	ENDIF
	RETURN PAS_CASUAL
ENDFUNC

FUNC BOOL CAN_PATROL_PED_CHAT(INT iPatrol)
	IF logic.Ped.Task.Patrol.CanChat != null
		RETURN CALL logic.Ped.Task.Patrol.CanChat(iPatrol)
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PATROL_PED_FOLLOW_EXACT_ROUTE(INT iPed)
	IF logic.Ped.Task.Patrol.ShouldFollowExactRoute != NULL
		RETURN CALL logic.Ped.Task.Patrol.ShouldFollowExactRoute(iPed)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PATROL_PED_MOVE_ONTO_THE_NEXT_NODE(INT iPed, INT iNode)
	IF logic.Ped.Task.Patrol.ShouldMoveOntoTheNextNode != NULL
		RETURN CALL logic.Ped.Task.Patrol.ShouldMoveOntoTheNextNode(iPed, iNode)
	ENDIF
	
	RETURN TRUE
ENDFUNC
#ENDIF

PROC DELETE_PATROL_ROUTES()
	#IF MAX_NUM_PATROLS
		INT iPatrol
		REPEAT data.Patrol.iCount iPatrol
			IF NOT IS_STRING_NULL_OR_EMPTY(sPatrol[iPatrol].strName)
				DELETE_PATROL_ROUTE(sPatrol[iPatrol].strName)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] DELETE_PATROL_ROUTES - Deleted ", sPatrol[iPatrol].strName)
			ENDIF
		ENDREPEAT
	#ENDIF
ENDPROC

#IF MAX_NUM_PATROLS
#IF NOT DEFINED(GET_PATROL_SCENARIO)
FUNC STRING GET_PATROL_SCENARIO(INT iPatrol, INT iNode)
	INT iScenario = data.Patrol.Patrols[iPatrol].Node[iNode].iScenarioIndex
	IF iScenario != -1
		#IF MAX_NUM_SCENARIOS
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_PATROL_SCENARIO - Using generic version.")
		RETURN CONVERT_TEXT_LABEL_TO_STRING(data.Scenario[iScenario].sName)
		#ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC
#ENDIF

PROC SETUP_PED_PATROL(INT iPed)
	INT iPatrol, iNode
	REPEAT data.Patrol.iCount iPatrol
		IF data.Patrol.Patrols[iPatrol].iPed = iPed
			sPatrol[iPatrol].strName =  "miss_"
			sPatrol[iPatrol].strName += sMission.iType
			sPatrol[iPatrol].strName += "_"
			sPatrol[iPatrol].strName += NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT()
			sPatrol[iPatrol].strName += "_"
			sPatrol[iPatrol].strName += iPatrol
	
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed,"] SETUP_PED_PATROL - ", sPatrol[iPatrol].strName)
			OPEN_PATROL_ROUTE(sPatrol[iPatrol].strName)
			WHILE (iNode < MAX_NUM_PATROL_NODES AND NOT IS_VECTOR_ZERO(data.Patrol.Patrols[iPatrol].Node[iNode].vCoord))
				ADD_PATROL_ROUTE_NODE(iNode, GET_PATROL_SCENARIO(iPatrol, iNode), data.Patrol.Patrols[iPatrol].Node[iNode].vCoord, data.Patrol.Patrols[iPatrol].Node[iNode].vLookAt, data.Patrol.Patrols[iPatrol].Node[iNode].iDuration)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed,"] SETUP_PED_PATROL - Node: ", iNode, ", ", data.Patrol.Patrols[iPatrol].Node[iNode].vCoord, ", ", data.Patrol.Patrols[iPatrol].Node[iNode].vLookAt, ", ", GET_PATROL_SCENARIO(iPatrol, iNode), ", ", data.Patrol.Patrols[iPatrol].Node[iNode].iDuration)
				iNode++
			ENDWHILE
			
			INT iNextNode, iMaxNodes = iNode
			REPEAT iMaxNodes iNode
				iNextNode = iNode+1
				IF iNextNode >= iMaxNodes
					iNextNode = 0
				ENDIF
				ADD_PATROL_ROUTE_LINK(iNode, iNextNode)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed,"] SETUP_PED_PATROL - Linked ", iNode, " to ", iNextNode)
			ENDREPEAT
			
			CLOSE_PATROL_ROUTE()
			CREATE_PATROL_ROUTE()
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed,"] SETUP_PED_PATROL - Finished creating ", sPatrol[iPatrol].strName, " with ", iMaxNodes, " nodes.")
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

PROC GENERIC_TASK_PATROL(INT iPed, PED_INDEX pedID)
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(pedID)
		
	#IF MAX_NUM_PATROLS
		IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedID, SCRIPT_TASK_PATROL)
		AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
			INT iPatrol = GET_PED_PATROL(iPed)
			IF iPatrol != (-1)
				IF NOT SHOULD_PATROL_PED_FOLLOW_EXACT_ROUTE(iPed)
					TEXT_LABEL_15 strPatrolName = GET_PATROL_ROUTE(iPatrol)
					TASK_PATROL(pedID, strPatrolName, GET_PATROL_TASK_ALERT_STATE(iPatrol), CAN_PATROL_PED_CHAT(iPatrol))
				ELSE
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedID, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) // To avoid task spam
						INT iNumPatrols
						REPEAT data.Patrol.iCount iNumPatrols
							IF data.Patrol.Patrols[iNumPatrols].iPed = iPed
								IF SHOULD_PATROL_PED_MOVE_ONTO_THE_NEXT_NODE(iPed, serverBD.sPed[iPed].iCheckpointStage)
									TASK_GO_STRAIGHT_TO_COORD(pedID, data.Patrol.Patrols[iNumPatrols].Node[serverBD.sPed[iPed].iCheckpointStage].vCoord, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
								ENDIF
							ENDIF
						ENDREPEAT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
ENDPROC

FUNC INT GET_HANDS_UP_TIME(INT iPed)
	IF logic.Ped.Task.HandsUp.Time != NULL
		RETURN CALL logic.Ped.Task.HandsUp.Time(iPed)
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC PED_INDEX GET_HANDS_UP_PED_TO_FACE(INT iPed)
	IF logic.Ped.Task.HandsUp.PedToFace != NULL
		RETURN CALL logic.Ped.Task.HandsUp.PedToFace(iPed)
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC INT GET_HANDS_UP_PED_TO_FACE_TIME(INT iPed)
	IF logic.Ped.Task.HandsUp.PedToFaceTime != NULL
		RETURN CALL logic.Ped.Task.HandsUp.PedToFaceTime(iPed)
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC TASK_HANDS_UP_FLAGS GET_HANDS_UP_FLAGS(INT iPed)
	IF logic.Ped.Task.HandsUp.Flags != NULL
		RETURN CALL logic.Ped.Task.HandsUp.Flags(iPed)
	ENDIF
	
	RETURN HANDS_UP_NOTHING
ENDFUNC

PROC ON_HANDS_UP(INT iPed, PED_INDEX pedId)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] ON_HANDS_UP - Calling SET_CURRENT_PED_WEAPON(pedId, WEAPONTYPE_UNARMED, TRUE) for iPed #", iPed)
	SET_CURRENT_PED_WEAPON(pedId, WEAPONTYPE_UNARMED, TRUE)
ENDPROC

PROC GENERIC_TASK_HANDS_UP(INT iPed, PED_INDEX pedId)
	IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_HANDS_UP)
	AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
		CALL logic.Ped.Task.HandsUp.OnTask(iPed, pedId)
		
		TASK_HANDS_UP(pedId, GET_HANDS_UP_TIME(iPed), GET_HANDS_UP_PED_TO_FACE(iPed), GET_HANDS_UP_PED_TO_FACE_TIME(iPed), GET_HANDS_UP_FLAGS(iPed))
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed,"] GENERIC_TASK_HANDS_UP - iPed #", iPed, " has been told to put their hands up with Time = ", GET_HANDS_UP_TIME(iPed), ", PedToFaceIndex = ", NATIVE_TO_INT(GET_HANDS_UP_PED_TO_FACE(iPed)), ", TimeToFacePed = ", GET_HANDS_UP_PED_TO_FACE_TIME(iPed), " and Flags = ", ENUM_TO_INT(GET_HANDS_UP_FLAGS(iPed)))
	ENDIF
ENDPROC

FUNC VECTOR GET_FLEE_FROM_COORD_COORDS(INT iPed)
	IF logic.Ped.Task.FleeFromCoord.Coords != NULL
		RETURN CALL logic.Ped.Task.FleeFromCoord.Coords(iPed)
	ENDIF
	
	RETURN GET_ENTITY_COORDS(NET_TO_PED(serverBD.sPed[iPed].netId), FALSE)
ENDFUNC

FUNC FLOAT GET_FLEE_FROM_COORD_SAFE_DISTANCE(INT iPed)
	IF logic.Ped.Task.FleeFromCoord.SafeDistance != NULL
		RETURN CALL logic.Ped.Task.FleeFromCoord.SafeDistance(iPed)
	ENDIF
	
	RETURN 10000.0
ENDFUNC

FUNC INT GET_FLEE_FROM_COORD_FLEE_TIME(INT iPed)
	IF logic.Ped.Task.FleeFromCoord.FleeTime != NULL
		RETURN CALL logic.Ped.Task.FleeFromCoord.FleeTime(iPed)
	ENDIF
	
	RETURN -1
ENDFUNC

PROC ON_FLEE_FROM_COORD(INT iPed, PED_INDEX pedId)
	IF logic.Ped.Task.FleeFromCoord.OnTask != NULL
		CALL logic.Ped.Task.FleeFromCoord.OnTask(iPed, pedId)
		EXIT
	ENDIF

	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(pedId, CA_ALWAYS_FIGHT, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(pedId, CA_ALWAYS_FLEE, TRUE)
	SET_PED_FLEE_ATTRIBUTES(pedId, FA_DISABLE_COWER, TRUE)
	SET_PED_FLEE_ATTRIBUTES(pedId, FA_COWER_INSTEAD_OF_FLEE, FALSE)
ENDPROC

PROC GENERIC_TASK_FLEE_FROM_COORD(INT iPed, PED_INDEX pedId)
	IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_SMART_FLEE_POINT)
	AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
		ON_FLEE_FROM_COORD(iPed, pedId)
		
		VECTOR vFleeCoord = GET_FLEE_FROM_COORD_COORDS(iPed)
		TASK_SMART_FLEE_COORD(pedId, vFleeCoord, GET_FLEE_FROM_COORD_SAFE_DISTANCE(iPed), GET_FLEE_FROM_COORD_FLEE_TIME(iPed))
		
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed,"] GENERIC_TASK_FLEE_FROM_COORD - iPed #", iPed, " has been told to flee from vFleeCoord = ", vFleeCoord)
	ENDIF
ENDPROC

FUNC BOOL GET_DRIVE_VEHICLE_VEHICLE_ID(INT iPed, VEHICLE_INDEX& vehID)
	
	NETWORK_INDEX netID
	
	IF logic.Ped.Task.DriveVehicle.Entity != NULL
		netID = CALL logic.Ped.Task.DriveVehicle.Entity(iPed)
	ENDIF
	
	IF data.Ped.Peds[iPed].iVehicle != -1
		netID = serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netID)
	AND IS_ENTITY_A_VEHICLE(NET_TO_ENT(netID))
		vehID = NET_TO_VEH(netID)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_DRIVE_VEHICLE_CRUISE_SPEED(INT iPed)
	IF logic.Ped.Task.DriveVehicle.CruiseSpeed != NULL
		RETURN CALL logic.Ped.Task.DriveVehicle.CruiseSpeed(iPed)
	ENDIF

	RETURN 10.0
ENDFUNC

FUNC DRIVINGMODE GET_DRIVE_VEHICLE_DRIVINGMODE_FLAGS(INT iPed)
	IF logic.Ped.Task.DriveVehicle.DrivingModeFlags != NULL
		RETURN CALL logic.Ped.Task.DriveVehicle.DrivingModeFlags(iPed)
	ENDIF
	
	RETURN DF_SteerAroundStationaryCars | DF_StopForCars | DF_SteerAroundPeds | DF_SteerAroundObjects | DF_StopAtLights | DF_UseWanderFallbackInsteadOfStraightLine | DF_AvoidRestrictedAreas | DF_AdjustCruiseSpeedBasedOnRoadSpeed
ENDFUNC

PROC GENERIC_TASK_DRIVE_VEHICLE(INT iPed, PED_INDEX pedID)
	IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)	
		VEHICLE_INDEX vehID
		IF GET_DRIVE_VEHICLE_VEHICLE_ID(iPed, vehID)
			IF IS_VEHICLE_DRIVEABLE(vehID)
				IF IS_PED_IN_VEHICLE(pedId, vehID)
					IF GET_ACTIVE_VEHICLE_MISSION_TYPE(vehID) != MISSION_CRUISE
						TASK_VEHICLE_MISSION(pedId, vehID, NULL, MISSION_CRUISE, GET_DRIVE_VEHICLE_CRUISE_SPEED(iPed), GET_DRIVE_VEHICLE_DRIVINGMODE_FLAGS(iPed), -1, -1, FALSE)						
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [PED_", iPed, "] - GENERIC_TASK_DRIVE_VEHICLE - Tasked with TASK_VEHICLE_MISSION")
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
						TASK_ENTER_VEHICLE(pedId, vehID, -1, DEFAULT, DEFAULT, ECF_RESUME_IF_INTERRUPTED | ECF_JACK_ANYONE)
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed,"] GENERIC_TASK_DRIVE_VEHICLE - Tasked with TASK_ENTER_VEHICLE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_NUM_GO_TO_COORD_ANY_MEANS_COORDS(INT iPed)
	IF logic.Ped.Task.GoToCoordAnyMeans.NumCoords != NULL
		RETURN CALL logic.Ped.Task.GoToCoordAnyMeans.NumCoords(iPed)
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC VECTOR GET_GO_TO_COORD_ANY_MEANS_COORD(INT iPed, INT iCheckpointOverride = -1)
	#IF MAX_NUM_AMBUSH_VEHICLES
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		ENTITY_INDEX ambushTargetId = GET_AMBUSH_TARGET_FOR_PED(iPed)
		IF DOES_ENTITY_EXIST(ambushTargetId)
			RETURN GET_ENTITY_COORDS(ambushTargetId, FALSE)
		ENDIF
	ENDIF
	#ENDIF
	
	IF logic.Ped.Task.GoToCoordAnyMeans.Coords != NULL
		INT iCheckpointStage = serverBD.sPed[iPed].iCheckpointStage
		IF iCheckpointOverride != -1
			iCheckpointStage = iCheckpointOverride
		ENDIF
		
		RETURN CALL logic.Ped.Task.GoToCoordAnyMeans.Coords(iPed, iCheckpointStage)
	ENDIF
	
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

FUNC VECTOR GET_PATROL_SHOULD_FOLLOW_EXACT_ROUTE_COORDS(INT iPatrol, INT iNode)
	RETURN data.Patrol.Patrols[iPatrol].Node[iNode].vCoord
ENDFUNC

FUNC VEHICLE_INDEX GET_GO_TO_COORD_ANY_MEANS_VEHICLE(INT iPed)
	#IF MAX_NUM_AMBUSH_VEHICLES
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
	AND data.Ped.Peds[iPed].iVehicle != (-1)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId)
		AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId))
			RETURN NET_TO_VEH(serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId)
		ENDIF
	ENDIF
	#ENDIF
	
	IF logic.Ped.Task.GoToCoordAnyMeans.Vehicle != NULL
		RETURN CALL logic.Ped.Task.GoToCoordAnyMeans.Vehicle(iPed)
	ENDIF
	
	IF data.Ped.Peds[iPed].iVehicle != (-1)
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId)
		AND IS_VEHICLE_DRIVEABLE(NET_TO_VEH(serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId))
			RETURN NET_TO_VEH(serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] GET_GO_TO_COORD_ANY_MEANS_VEHICLE is NULL!")
	#ENDIF
	
	RETURN NULL
ENDFUNC

FUNC DRIVINGMODE GET_GO_TO_COORD_ANY_MEANS_DRIVING_FLAGS(INT iPed)
	#IF MAX_NUM_AMBUSH_VEHICLES
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		RETURN DF_SwerveAroundAllCars | DF_SteerAroundStationaryCars | DF_SteerAroundObjects | DF_SteerAroundPeds | DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_DriveIntoOncomingTraffic | DF_UseSwitchedOffNodes | DF_AdjustCruiseSpeedBasedOnRoadSpeed | DF_ForceJoinInRoadDirection
	ENDIF
	#ENDIF
	
	IF logic.Ped.Task.GoToCoordAnyMeans.DrivingFlags != NULL
		RETURN CALL logic.Ped.Task.GoToCoordAnyMeans.DrivingFlags(iPed)
	ENDIF
	
	RETURN DF_SwerveAroundAllCars | DF_SteerAroundStationaryCars | DF_SteerAroundObjects | DF_SteerAroundPeds | DF_UseShortCutLinks | DF_ChangeLanesAroundObstructions | DF_DriveIntoOncomingTraffic | DF_UseSwitchedOffNodes | DF_AdjustCruiseSpeedBasedOnRoadSpeed | DF_ForceJoinInRoadDirection
ENDFUNC

FUNC FLOAT GET_GO_TO_COORD_ANY_MEANS_EXTRA_DISTANCE_TO_PREFER_VEHICLE(INT iPed)
	IF logic.Ped.Task.GoToCoordAnyMeans.ExtraDistanceToPreferVehicle != NULL
		RETURN CALL logic.Ped.Task.GoToCoordAnyMeans.ExtraDistanceToPreferVehicle(iPed)
	ENDIF
	
	RETURN 0.0
ENDFUNC

FUNC TASK_GO_TO_COORD_ANY_MEANS_FLAGS GET_GO_TO_COORD_ANY_MEANS_FLAGS(INT iPed)
	#IF MAX_NUM_AMBUSH_VEHICLES
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		RETURN TGCAM_NEVER_ABANDON_VEHICLE | TGCAM_REMAIN_IN_VEHICLE_AT_DESTINATION | TGCAM_IGNORE_VEHICLE_HEALTH
	ENDIF
	#ENDIF
	
	IF logic.Ped.Task.GoToCoordAnyMeans.Flags != NULL
		RETURN CALL logic.Ped.Task.GoToCoordAnyMeans.Flags(iPed)
	ENDIF
	
	RETURN TGCAM_NEVER_ABANDON_VEHICLE | TGCAM_REMAIN_IN_VEHICLE_AT_DESTINATION | TGCAM_IGNORE_VEHICLE_HEALTH
ENDFUNC

FUNC FLOAT GET_GO_TO_COORD_ANY_MEANS_SPEED(INT iPed)
	#IF MAX_NUM_AMBUSH_VEHICLES
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		RETURN 100.0
	ENDIF
	#ENDIF
	
	IF logic.Ped.Task.GoToCoordAnyMeans.Speed != NULL
		RETURN CALL logic.Ped.Task.GoToCoordAnyMeans.Speed(iPed)
	ENDIF
	
	RETURN 20.0
ENDFUNC

FUNC FLOAT GET_GO_TO_COORD_ANY_MEANS_RADIUS(INT iPed)
	#IF MAX_NUM_AMBUSH_VEHICLES
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		RETURN 10.0
	ENDIF
	#ENDIF
	
	IF logic.Ped.Task.GoToCoordAnyMeans.Radius != NULL
		RETURN CALL logic.Ped.Task.GoToCoordAnyMeans.Radius(iPed)
	ENDIF
	
	RETURN 10.0
ENDFUNC

FUNC FLOAT GET_GO_TO_COORD_ANY_MEANS_PED_MOVE_SPEED(INT iPed)
	#IF MAX_NUM_AMBUSH_VEHICLES
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		RETURN PEDMOVEBLENDRATIO_RUN
	ENDIF
	#ENDIF
	
	IF logic.Ped.Task.GoToCoordAnyMeans.PedMoveBlendRatio != NULL
		RETURN CALL logic.Ped.Task.GoToCoordAnyMeans.PedMoveBlendRatio(iPed)
	ENDIF
	
	RETURN PEDMOVEBLENDRATIO_RUN
ENDFUNC

FUNC BOOL GET_GO_TO_COORD_ANY_MEANS_SHOULD_CHECK_3D_DISTANCE(INT iPed, INT iCheckpointStage)
	IF logic.Ped.Task.GoToCoordAnyMeans.ShouldCheck3dDistance != NULL
		RETURN CALL logic.Ped.Task.GoToCoordAnyMeans.ShouldCheck3dDistance(iPed,iCheckpointStage)
	ENDIF

	RETURN FALSE
ENDFUNC

PROC GENERIC_TASK_GO_TO_COORD_ANY_MEANS_CLIENT(INT iPed, PED_INDEX pedID)
	IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
		IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED)
			IF serverBD.sPed[iPed].iCheckpointOnRouteTo != serverBD.sPed[iPed].iCheckpointStage AND NOT IS_PED_RUNNING_RAGDOLL_TASK(pedID)
				CLEAR_PED_TASKS(pedId)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_GO_TO_COORD_ANY_MEANS_CLIENT - iPed #", iPed, "'s current checkpoint is ", serverBD.sPed[iPed].iCheckpointStage, " but currently on route to checkpoint ", serverBD.sPed[iPed].iCheckpointOnRouteTo, ". Clearing ped tasks.")
			ENDIF
		ELSE
			IF serverBD.sPed[iPed].iCheckpointOnRouteTo = -1
				CLEAR_PED_TASKS(pedId)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_GO_TO_COORD_ANY_MEANS_CLIENT - Clearing any previous tasks for iPed #", iPed)
			ENDIF
			
			IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedId, TRUE)
			ENDIF
			
			VECTOR vGoToCoord = GET_GO_TO_COORD_ANY_MEANS_COORD(iPed)
			TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED(pedId, vGoToCoord, 
																	  GET_GO_TO_COORD_ANY_MEANS_PED_MOVE_SPEED(iPed), GET_GO_TO_COORD_ANY_MEANS_VEHICLE(iPed), 
																	  TRUE, GET_GO_TO_COORD_ANY_MEANS_DRIVING_FLAGS(iPed), 
																	  50.0, GET_GO_TO_COORD_ANY_MEANS_EXTRA_DISTANCE_TO_PREFER_VEHICLE(iPed), 
																	  10, GET_GO_TO_COORD_ANY_MEANS_FLAGS(iPed), 
																	  GET_GO_TO_COORD_ANY_MEANS_SPEED(iPed), GET_GO_TO_COORD_ANY_MEANS_RADIUS(iPed))
			
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed,"] GENERIC_TASK_GO_TO_COORD_ANY_MEANS_CLIENT - Tasked with TASK_GO_TO_COORD_ANY_MEANS_EXTRA_PARAMS_WITH_CRUISE_SPEED to checkpoint ", serverBD.sPed[iPed].iCheckpointStage, " at ", vGoToCoord)
			
			IF serverBD.sPed[iPed].iCheckpointOnRouteTo != serverBD.sPed[iPed].iCheckpointStage
				BROADCAST_EVENT_FREEMODE_CONTENT_UPDATE_CURRENT_CHECKPOINT(serverBD.sPed[iPed].iCheckpointStage, iPed)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_GO_TO_COORD_ANY_MEANS_RADIUS_SERVER(INT iPed)
	IF logic.Ped.Task.GoToCoordAnyMeans.RadiusServer != NULL
		RETURN CALL logic.Ped.Task.GoToCoordAnyMeans.RadiusServer(iPed)
	ENDIF
	RETURN GET_GO_TO_COORD_ANY_MEANS_RADIUS(iPed) + 2.5
ENDFUNC

PROC GENERIC_TASK_GO_TO_COORD_ANY_MEANS_SERVER(INT iPed, PED_INDEX pedId)
	VECTOR vNextCheckpoint = GET_GO_TO_COORD_ANY_MEANS_COORD(iPed)
	FLOAT fDistanceToCheckpoint = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(pedID, vNextCheckpoint, GET_GO_TO_COORD_ANY_MEANS_SHOULD_CHECK_3D_DISTANCE(iPed,serverBD.sPed[iPed].iCheckpointStage))
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_GO_TO_COORD_ANY_MEANS_SERVER - iPed #", iPed, ", vNextCheckpoint = ", vNextCheckpoint, ", fDistanceToCheckpoint = ", fDistanceToCheckpoint)
	
	IF fDistanceToCheckpoint <= GET_GO_TO_COORD_ANY_MEANS_RADIUS_SERVER(iPed)
		serverBD.sPed[iPed].iCheckpointStage++
		IF serverBD.sPed[iPed].iCheckpointStage >= GET_NUM_GO_TO_COORD_ANY_MEANS_COORDS(iPed)
			serverBD.sPed[iPed].iCheckpointStage = 0
			
			SET_PED_BIT(iPed, ePEDBITSET_I_HAVE_REACHED_FINAL_CHECKPOINT)
			SET_GENERIC_SERVER_BIT(eGENERICSERVERBITSET_ENTITY_REACHED_FINAL_CHECKPOINT)
		ENDIF
		
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_GO_TO_COORD_ANY_MEANS_SERVER - iPed #", iPed, " has reached ", vNextCheckpoint, " (Checkpoint #", serverBD.sPed[iPed].iCheckpointOnRouteTo, "). Next checkpoint is #", serverBD.sPed[iPed].iCheckpointStage, ".")
	ENDIF
ENDPROC

PROC GENERIC_TASK_PATROL_SERVER(INT iPed, PED_INDEX pedId)

	IF logic.Ped.Task.Patrol.ShouldFollowExactRoute = NULL
	OR CALL logic.Ped.Task.Patrol.ShouldFollowExactRoute(iPed) = FALSE
		EXIT
	ENDIF
	
	// Note: iCheckpointOnRouteTo is used for direction (forward or backwards). iCheckpointStage is used to keep track of the current Patrol Node.
	INT iNumPatrols
	REPEAT data.Patrol.iCount iNumPatrols
		IF data.Patrol.Patrols[iNumPatrols].iPed = iPed
			VECTOR vNextCheckpoint = GET_PATROL_SHOULD_FOLLOW_EXACT_ROUTE_COORDS(iNumPatrols, serverBD.sPed[iPed].iCheckpointStage)
			FLOAT fDistanceToCheckpoint = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(pedID, vNextCheckpoint)
			
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_PATROL_SERVER - iPed #", iPed, ", vNextCheckpoint = ", vNextCheckpoint, ", fDistanceToCheckpoint = ", fDistanceToCheckpoint)
			
			IF fDistanceToCheckpoint <= 2.0 // TODO: Maybe have a Function Pointer for the radius?
				// Below controls if the Ped is moving forwards in the path or backwards (Last Node to First Node).
				IF serverBD.sPed[iPed].iCheckpointOnRouteTo != 1
					serverBD.sPed[iPed].iCheckpointStage++
				ELSE
					serverBD.sPed[iPed].iCheckpointStage--
				ENDIF
				
				IF serverBD.sPed[iPed].iCheckpointStage >= MAX_NUM_PATROL_NODES
				OR IS_VECTOR_ZERO(data.Patrol.Patrols[iNumPatrols].Node[serverBD.sPed[iPed].iCheckpointStage].vCoord) // Because we might not be using all nodes from the available slots (MAX_NUM_PATROL_NODES)
				AND serverBD.sPed[iPed].iCheckpointOnRouteTo = 0 OR serverBD.sPed[iPed].iCheckpointOnRouteTo = -1 // AND Ped was going forward.
					serverBD.sPed[iPed].iCheckpointStage = serverBD.sPed[iPed].iCheckpointStage - 1 // Use the last node (that was not a zero vector.)
					serverBD.sPed[iPed].iCheckpointOnRouteTo = 1 // Start going backwards.
					
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_PATROL_SERVER - Ped Reached final Point, moving backwards")
				ENDIF
				
				IF serverBD.sPed[iPed].iCheckpointStage <= 0 // If we are at the start of the path.
				AND serverBD.sPed[iPed].iCheckpointOnRouteTo = 1 OR serverBD.sPed[iPed].iCheckpointOnRouteTo = -1 // To avoid spamming this.
					serverBD.sPed[iPed].iCheckpointOnRouteTo = 0 // Start going forward in the path.
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_PATROL_SERVER - Ped started moving forward")
				ENDIF
				
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_PATROL_SERVER - iPed #", iPed, " has reached ", vNextCheckpoint, " (Checkpoint #", serverBD.sPed[iPed].iCheckpointOnRouteTo, "). Next checkpoint is #", serverBD.sPed[iPed].iCheckpointStage, ".")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC FLOAT GET_CRUISE_SPEED_FOR_VEHICLE_MISSION(INT iPed, VEHICLE_MISSION eVehMission)
	IF logic.Ped.Task.VehicleMission.CruiseSpeed != NULL
		RETURN CALL logic.Ped.Task.VehicleMission.CruiseSpeed(iPed, eVehMission)
	ENDIF
	
	SWITCH eVehMission
		CASE MISSION_FOLLOW			RETURN 30.0
		CASE MISSION_GOTO			RETURN 30.0
	ENDSWITCH
	RETURN 30.0
ENDFUNC

FUNC FLOAT GET_TARGET_REACHED_DISTANCE_FOR_VEHICLE_MISSION(INT iPed, VEHICLE_MISSION eVehMission)
	IF logic.Ped.Task.VehicleMission.TargetReachedDist != NULL
		RETURN CALL logic.Ped.Task.VehicleMission.TargetReachedDist(iPed, eVehMission)
	ENDIF
	
	SWITCH eVehMission
		CASE MISSION_FOLLOW			RETURN 5.0
		CASE MISSION_GOTO			RETURN 0.1
	ENDSWITCH
	RETURN 10.0
ENDFUNC

FUNC FLOAT GET_STRAIGHT_LINE_DISTANCE_FOR_VEHICLE_MISSION(INT iPed, VEHICLE_MISSION eVehMission)
	IF logic.Ped.Task.VehicleMission.StraightLineDist != NULL
		RETURN CALL logic.Ped.Task.VehicleMission.StraightLineDist(iPed, eVehMission)
	ENDIF
	
	SWITCH eVehMission
		CASE MISSION_FOLLOW			RETURN 1.0
		CASE MISSION_GOTO			RETURN 10.0
	ENDSWITCH
	RETURN 1.0
ENDFUNC

FUNC DRIVINGMODE GET_DRIVING_MODE_FOR_VEHICLE_MISSION(INT iPed, VEHICLE_MISSION eVehMission)
	IF logic.Ped.Task.VehicleMission.DrivingFlags != NULL
		RETURN CALL logic.Ped.Task.VehicleMission.DrivingFlags(iPed, eVehMission)
	ENDIF
	
	SWITCH eVehMission
		CASE MISSION_FOLLOW			RETURN DRIVINGMODE_AVOIDCARS
		CASE MISSION_GOTO			RETURN DRIVINGMODE_PLOUGHTHROUGH
	ENDSWITCH
	RETURN DRIVINGMODE_AVOIDCARS
ENDFUNC

FUNC INT GET_HEIGHT_FOR_VEHICLE_MISSION(INT iPed, VEHICLE_MISSION eVehMission)
	UNUSED_PARAMETER(eVehMission)
	IF logic.Ped.Task.VehicleMission.Height != NULL
		RETURN CALL logic.Ped.Task.VehicleMission.Height(iPed, eVehMission)
	ENDIF

	RETURN 100
ENDFUNC

PROC GENERIC_TASK_VEHICLE_MISSION(INT iPed, PED_INDEX pedId)
	IF logic.Ped.Task.VehicleMission.Mission = NULL
		ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_MISSION - No mission set in logic.Ped.Task.VehicleMission.Mission!")
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_MISSION - No mission set in logic.Ped.Task.VehicleMission.Mission!")
		EXIT
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId)
		VEHICLE_INDEX vehId = NET_TO_VEH(serverBD.sVehicle[data.Ped.Peds[iPed].iVehicle].netId)
		VEHICLE_MISSION eVehMission = CALL logic.Ped.Task.VehicleMission.Mission(iPed)
		
		IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedId, SCRIPT_TASK_VEHICLE_MISSION)
		OR GET_ACTIVE_VEHICLE_MISSION_TYPE(vehId) != eVehMission
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
				PED_INDEX targetPed = NULL
				VEHICLE_INDEX targetVeh = NULL
				VECTOR vTargetCoord
				
				IF logic.Ped.Task.VehicleMission.TargetEntity != null
					ENTITY_INDEX targetEntity = CALL logic.Ped.Task.VehicleMission.TargetEntity(iPed)
					IF IS_ENTITY_A_PED(targetEntity)
						targetPed = GET_PED_INDEX_FROM_ENTITY_INDEX(targetEntity)
						vTargetCoord = GET_ENTITY_COORDS(targetPed, FALSE)
					ELIF IS_ENTITY_A_VEHICLE(targetEntity)
						targetVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(targetEntity)
						vTargetCoord = GET_ENTITY_COORDS(targetVeh, FALSE)
					ENDIF
				ENDIF

				IF targetPed != NULL
					IF IS_PED_IN_ANY_PLANE(pedId)
						TASK_PLANE_MISSION(pedId, vehId, NULL, targetPed, vTargetCoord, eVehMission,
										   GET_CRUISE_SPEED_FOR_VEHICLE_MISSION(iPed, eVehMission),
										   GET_TARGET_REACHED_DISTANCE_FOR_VEHICLE_MISSION(iPed, eVehMission), -1,
										   GET_HEIGHT_FOR_VEHICLE_MISSION(iPed, eVehMission),
										   GET_HEIGHT_FOR_VEHICLE_MISSION(iPed, eVehMission))
					ELIF IS_PED_IN_ANY_HELI(pedId)
						TASK_HELI_MISSION(pedId, vehId, NULL, targetPed, vTargetCoord, eVehMission,
										   GET_CRUISE_SPEED_FOR_VEHICLE_MISSION(iPed, eVehMission),
										   GET_TARGET_REACHED_DISTANCE_FOR_VEHICLE_MISSION(iPed, eVehMission), -1,
										   GET_HEIGHT_FOR_VEHICLE_MISSION(iPed, eVehMission),
										   GET_HEIGHT_FOR_VEHICLE_MISSION(iPed, eVehMission), -1, HF_StartEngineImmediately)
					ELSE
						TASK_VEHICLE_MISSION_PED_TARGET(pedID, vehID, targetPed, eVehMission, 
														GET_CRUISE_SPEED_FOR_VEHICLE_MISSION(iPed, eVehMission), 
														GET_DRIVING_MODE_FOR_VEHICLE_MISSION(iPed, eVehMission), 
														GET_TARGET_REACHED_DISTANCE_FOR_VEHICLE_MISSION(iPed, eVehMission), 
														GET_STRAIGHT_LINE_DISTANCE_FOR_VEHICLE_MISSION(iPed, eVehMission), 
														TRUE)
					ENDIF
				ELIF targetVeh != NULL
					IF IS_PED_IN_ANY_PLANE(pedId)
						
						TASK_PLANE_MISSION(pedId, vehId, targetVeh, NULL, vTargetCoord, eVehMission,
										   GET_CRUISE_SPEED_FOR_VEHICLE_MISSION(iPed, eVehMission),
										   GET_TARGET_REACHED_DISTANCE_FOR_VEHICLE_MISSION(iPed, eVehMission), -1,
										   GET_HEIGHT_FOR_VEHICLE_MISSION(iPed, eVehMission),
										   GET_HEIGHT_FOR_VEHICLE_MISSION(iPed, eVehMission))		   
					ELIF IS_PED_IN_ANY_HELI(pedId)
						TASK_HELI_MISSION(pedId, vehId, targetVeh, NULL, vTargetCoord, eVehMission,
										   GET_CRUISE_SPEED_FOR_VEHICLE_MISSION(iPed, eVehMission),
										   GET_TARGET_REACHED_DISTANCE_FOR_VEHICLE_MISSION(iPed, eVehMission), -1,
										   GET_HEIGHT_FOR_VEHICLE_MISSION(iPed, eVehMission),
										   GET_HEIGHT_FOR_VEHICLE_MISSION(iPed, eVehMission), -1, HF_StartEngineImmediately)
					ELSE
						TASK_VEHICLE_MISSION(pedID, vehID, targetVeh, eVehMission, 
											GET_CRUISE_SPEED_FOR_VEHICLE_MISSION(iPed, eVehMission), 
											GET_DRIVING_MODE_FOR_VEHICLE_MISSION(iPed, eVehMission), 
											GET_TARGET_REACHED_DISTANCE_FOR_VEHICLE_MISSION(iPed, eVehMission), 
											GET_STRAIGHT_LINE_DISTANCE_FOR_VEHICLE_MISSION(iPed, eVehMission), 
											TRUE)
					ENDIF
				ELIF logic.Ped.Task.VehicleMission.TargetCoord != null
					IF IS_PED_IN_ANY_PLANE(pedId)
						TASK_PLANE_MISSION(pedId, vehId, NULL, NULL, CALL logic.Ped.Task.VehicleMission.TargetCoord(iPed), eVehMission,
										   GET_CRUISE_SPEED_FOR_VEHICLE_MISSION(iPed, eVehMission),
										   GET_TARGET_REACHED_DISTANCE_FOR_VEHICLE_MISSION(iPed, eVehMission), -1,
										   GET_HEIGHT_FOR_VEHICLE_MISSION(iPed, eVehMission),
										   GET_HEIGHT_FOR_VEHICLE_MISSION(iPed, eVehMission))
					ELIF IS_PED_IN_ANY_HELI(pedId)
						TASK_HELI_MISSION(pedId, vehId, NULL, NULL, CALL logic.Ped.Task.VehicleMission.TargetCoord(iPed), eVehMission,
										   GET_CRUISE_SPEED_FOR_VEHICLE_MISSION(iPed, eVehMission),
										   GET_TARGET_REACHED_DISTANCE_FOR_VEHICLE_MISSION(iPed, eVehMission), -1,
										   GET_HEIGHT_FOR_VEHICLE_MISSION(iPed, eVehMission),
										   GET_HEIGHT_FOR_VEHICLE_MISSION(iPed, eVehMission), -1, HF_StartEngineImmediately)					   
					ELSE
						TASK_VEHICLE_MISSION_COORS_TARGET(pedId, vehId, CALL logic.Ped.Task.VehicleMission.TargetCoord(iPed), eVehMission,
														  GET_CRUISE_SPEED_FOR_VEHICLE_MISSION(iPed, eVehMission), 
														  GET_DRIVING_MODE_FOR_VEHICLE_MISSION(iPed, eVehMission), 
														  GET_TARGET_REACHED_DISTANCE_FOR_VEHICLE_MISSION(iPed, eVehMission), 
														  GET_STRAIGHT_LINE_DISTANCE_FOR_VEHICLE_MISSION(iPed, eVehMission), 
														  TRUE)
					ENDIF
				ELSE
					ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_MISSION - No valid target for mission!")
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_MISSION - No valid target for mission!")
					EXIT
				ENDIF
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_VEHICLE_MISSION - Started mission on target.")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_STAY_IN_COVER_COORD(INT iPed, PED_INDEX pedId)
	IF logic.Ped.Task.StayInCover.Coord != NULL
		RETURN CALL logic.Ped.Task.StayInCover.Coord(iPed, pedId)
	ENDIF
	
	RETURN GET_ENTITY_COORDS(pedId, FALSE)
ENDFUNC

FUNC FLOAT GET_STAY_IN_COVER_RADIUS(INT iPed, PED_INDEX pedId)
	IF logic.Ped.Task.StayInCover.Radius != NULL
		RETURN CALL logic.Ped.Task.StayInCover.Radius(iPed, pedId)
	ENDIF
	
	RETURN 5.0
ENDFUNC

PROC GENERIC_TASK_STAY_IN_COVER(INT iPed, PED_INDEX pedID)
	IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedID, SCRIPT_TASK_STAY_IN_COVER)
	AND NOT IS_PED_IN_COVER(pedID)
		IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netID)
			IF IS_PED_IN_ANY_VEHICLE(pedID)
				TASK_LEAVE_ANY_VEHICLE(pedID)
				PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_STAY_IN_COVER - Tasked ped to leave vehicle before cover.")
			ELSE
				VECTOR vCover = GET_STAY_IN_COVER_COORD(iPed, pedID)
				FLOAT fCoverRadius = GET_STAY_IN_COVER_RADIUS(iPed, pedID)
				SET_PED_SPHERE_DEFENSIVE_AREA(pedID, vCover, fCoverRadius)
				
				TASK_STAY_IN_COVER(pedID)
				PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] GENERIC_TASK_STAY_IN_COVER - Tasked ped to stay in cover ", fCoverRadius, "m around ", vCover)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_FLEE_IN_VEHICLE_SPEED(INT iPed, BOOL bBoat = FALSE)
	IF logic.Ped.Task.FleeInVehicle.FleeSpeed != NULL
		RETURN CALL logic.Ped.Task.FleeInVehicle.FleeSpeed(iPed)
	ENDIF

	IF bBoat
		RETURN 20.00
	ENDIF

	RETURN 30.00
ENDFUNC

FUNC FLOAT GET_FLEE_IN_VEHICLE_HELI_HEIGHT(INT iPed)
	IF logic.Ped.Task.FleeInVehicle.FlightHeight != NULL
		RETURN CALL logic.Ped.Task.FleeInVehicle.FlightHeight(iPed)
	ENDIF
	RETURN 80.0
ENDFUNC

FUNC INT GET_FLEE_IN_VEHICLE_HELI_MINIMUM_FLIGHT_HEIGHT_ABOVE_TERRAIN(INT iPed)
	IF logic.Ped.Task.FleeInVehicle.MinFlightHeightAboveTerrain != NULL
		RETURN CALL logic.Ped.Task.FleeInVehicle.MinFlightHeightAboveTerrain(iPed)
	ENDIF
	RETURN 80
ENDFUNC

FUNC DRIVINGMODE GET_FLEE_IN_VEHICLE_VEHICLE_DRIVINGMODE_FLAGS(INT iPed)
	IF logic.Ped.Task.FleeInVehicle.DrivingModeFlags != NULL
		RETURN CALL logic.Ped.Task.FleeInVehicle.DrivingModeFlags(iPed)
	ENDIF
	
	RETURN DRIVINGMODE_AVOIDCARS_RECKLESS
ENDFUNC

PROC GENERIC_TASK_FLEE_IN_VEHICLE(INT iPed, PED_INDEX pedID)
	IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)	
		VEHICLE_INDEX vehID
		BOOL bSpecificVeh
		
		IF logic.Ped.Task.FleeInVehicle.VehToUse != NULL
			vehID = CALL logic.Ped.Task.FleeInVehicle.VehToUse(iPed)
			bSpecificVeh = TRUE
		ELIF IS_PED_IN_ANY_VEHICLE(pedID)
			vehID = GET_VEHICLE_PED_IS_USING(pedID)
			bSpecificVeh = FALSE
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehID)
			IF IS_VEHICLE_DRIVEABLE(vehID)
				IF IS_PED_IN_VEHICLE(pedId, vehID)
					IF GET_ACTIVE_VEHICLE_MISSION_TYPE(vehID) != MISSION_FLEE
						PED_INDEX pedToFleeFrom = LOCAL_PED_INDEX
						IF logic.Ped.Task.FleeInVehicle.PedToFleeFrom != NULL
							pedToFleeFrom = CALL logic.Ped.Task.FleeInVehicle.PedToFleeFrom(iPed)
						ENDIF
						
						IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(vehID))
							TASK_HELI_MISSION(pedId, vehID, NULL, pedToFleeFrom, <<0.0, 0.0, 0.0>>, MISSION_FLEE, GET_FLEE_IN_VEHICLE_SPEED(iPed), 10.0, -1, ROUND(GET_FLEE_IN_VEHICLE_HELI_HEIGHT(iPed)), GET_FLEE_IN_VEHICLE_HELI_MINIMUM_FLIGHT_HEIGHT_ABOVE_TERRAIN(iPed))
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] - GENERIC_TASK_FLEE_IN_VEHICLE - Tasked with TASK_HELI_MISSION")
						ELIF IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(vehID))
							SET_BOAT_ANCHOR(vehID, FALSE)
							TASK_BOAT_MISSION(pedId, vehID, NULL, pedToFleeFrom, <<0.0, 0.0, 0.0>>, MISSION_FLEE, GET_FLEE_IN_VEHICLE_SPEED(iPed, TRUE), DRIVINGMODE_AVOIDCARS, -1.0, BCF_OPENOCEANSETTINGS)
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] - GENERIC_TASK_FLEE_IN_VEHICLE - Tasked with TASK_BOAT_MISSION")
						ELSE
							TASK_VEHICLE_MISSION_PED_TARGET(pedId, vehID, pedToFleeFrom, MISSION_FLEE, GET_FLEE_IN_VEHICLE_SPEED(iPed), GET_FLEE_IN_VEHICLE_VEHICLE_DRIVINGMODE_FLAGS(iPed), 1000.0, 1000.0)
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] - GENERIC_TASK_FLEE_IN_VEHICLE - Tasked with TASK_VEHICLE_MISSION_PED_TARGET")
						ENDIF
					ENDIF
				ELSE
					// Only try to enter the vehicle if a flee vehicle is specified, otherwise mission logic should have the ped move to flee on foot/from coord if not in any vehicle
					IF bSpecificVeh
					AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(pedId, SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
						TASK_ENTER_VEHICLE(pedId, vehID, -1, DEFAULT, DEFAULT, ECF_RESUME_IF_INTERRUPTED | ECF_JACK_ANYONE)
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed,"] GENERIC_TASK_FLEE_IN_VEHICLE - Tasked with TASK_ENTER_VEHICLE")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_FOLLOW_PLAYER_JOIN_RANGE(INT iPed)
	IF logic.Ped.Task.FollowPlayer.JoinRange != NULL
		RETURN CALL logic.Ped.Task.FollowPlayer.JoinRange(iPed)
	ENDIF
	
	RETURN 7.5
ENDFUNC

FUNC FLOAT GET_FOLLOW_PLAYER_LEAVE_RANGE(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN 35.0
ENDFUNC

FUNC BOOL ENABLE_FOLLOW_PLAYER(INT iPed, PED_INDEX pedID)
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(pedID)
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PED_FOLLOW_PLAYER(INT iPed, PED_INDEX pedID)

	IF NOT IS_LOCAL_ALIVE
		RETURN FALSE
	ENDIF

	IF NOT CALL logic.Ped.Task.FollowPlayer.Enable(iPed, pedID)
		RETURN FALSE
	ENDIF

	INT iLeaders, iFollowers
	GET_GROUP_SIZE(sPlayerGroup.index, iLeaders, iFollowers)
	IF iFollowers > 0
		RETURN FALSE
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_ENTITIES(LOCAL_PED_INDEX, pedID) > GET_FOLLOW_PLAYER_JOIN_RANGE(iPed)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

PROC GENERIC_TASK_FOLLOW_PLAYER(INT iPed, PED_INDEX pedID)

	IF NOT IS_PED_IN_GROUP(pedID)
		
		IF SHOULD_PED_FOLLOW_PLAYER(iPed, pedID)		
			IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(pedID)
				NETWORK_REQUEST_CONTROL_OF_ENTITY(pedID)
			ELSE
				
				SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedID)
				CLEAR_PED_TASKS(pedID)
				
				IF logic.Ped.Task.FollowPlayer.MaxMoveBlendRatio != NULL					
					SET_PED_MAX_MOVE_BLEND_RATIO(pedID, CALL logic.Ped.Task.FollowPlayer.MaxMoveBlendRatio(iPed))
				ENDIF
				
				SET_PED_CONFIG_FLAG(pedID, PCF_TeleportToLeaderVehicle, TRUE)
				
				SET_PED_AS_GROUP_MEMBER(pedID, sPlayerGroup.index)
				
				SET_GROUP_FORMATION_SPACING(sPlayerGroup.index, 2, 0.0, 0.0)
				SET_GROUP_SEPARATION_RANGE(sPlayerGroup.index, 36.0)
				
				IF logic.Ped.Task.FollowPlayer.OnJoinGroup != NULL
					CALL logic.Ped.Task.FollowPlayer.OnJoinGroup(iPed)
				ENDIF
				
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed,"] GENERIC_TASK_FOLLOW_PLAYER - Added ped #", iPed," to group")
			ENDIF
		ELSE
			IF logic.Ped.Task.FollowPlayer.IdleBehaviour != NULL
				IF NOT IS_PED_IN_ANY_VEHICLE(pedID)
					SWITCH CALL logic.Ped.Task.FollowPlayer.IdleBehaviour(iPed)
						CASE eFOLLOWPEDIDLETYPE_SCENARIO
							GENERIC_TASK_DO_SCENARIO(iPed, pedID)
						BREAK
						CASE eFOLLOWPEDIDLETYPE_ANIM
							GENERIC_TASK_PLAY_ANIM(iPed, pedID)
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		IF IS_PED_GROUP_MEMBER(pedID, sPlayerGroup.index)
			IF GET_DISTANCE_BETWEEN_ENTITIES(LOCAL_PED_INDEX, pedID) > GET_FOLLOW_PLAYER_LEAVE_RANGE(iPed)
			OR NOT IS_LOCAL_ALIVE
			OR NOT CALL logic.Ped.Task.FollowPlayer.Enable(iPed, pedID)
				IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(pedID)
					NETWORK_REQUEST_CONTROL_OF_ENTITY(pedID)
				ELSE
					REMOVE_PED_FROM_GROUP(pedID)
					
					IF logic.Ped.Task.FollowPlayer.OnLeaveGroup != NULL
						CALL logic.Ped.Task.FollowPlayer.OnLeaveGroup(iPed)
					ENDIF
					
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed,"] GENERIC_TASK_FOLLOW_PLAYER - Removed ped #", iPed," to group")
				ENDIF

			ENDIF
		ENDIF
	ENDIF

ENDPROC

// TASK: WANDER_ON_FOOT

FUNC SCRIPT_TASK_NAME GET_TASK_WANDER_ON_FOOT_TYPE(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN SCRIPT_TASK_WANDER_STANDARD
ENDFUNC

FUNC FLOAT GET_TASK_WANDER_ON_FOOT_HEADING(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN DEFAULT_NAVMESH_FINAL_HEADING
ENDFUNC

FUNC EWDR_SCRIPT_FLAGS GET_TASK_WANDER_ON_FOOT_FLAGS(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN EWDR_DEFAULT
ENDFUNC

FUNC FLOAT GET_TASK_WANDER_ON_FOOT_AREA_MIN_WAIT_TIME(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN 3.0 // DEFAULT FROM "TASK_WANDER_IN_AREA"
ENDFUNC

FUNC FLOAT GET_TASK_WANDER_ON_FOOT_AREA_MAX_WAIT_TIME(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN 6.0 // DEFAULT FROM "TASK_WANDER_IN_AREA"
ENDFUNC

// TASK: GO_TO_ENTITY

FUNC BOOL GET_GO_TO_ENTITY_INDEX(INT iPed, ENTITY_INDEX& gotoEntityId)
	IF logic.Ped.Task.GoToEntity.Entity != NULL
		gotoEntityId = CALL logic.Ped.Task.GoToEntity.Entity(iPed)
		IF DOES_ENTITY_EXIST(gotoEntityId)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_GO_TO_ENTITY_TIME(INT iPed)
	IF logic.Ped.Task.GoToEntity.Time != NULL
		RETURN CALL logic.Ped.Task.GoToEntity.Time(iPed)
	ENDIF
	RETURN DEFAULT_TIME_NEVER_WARP
ENDFUNC

FUNC FLOAT GET_GO_TO_ENTITY_SEEK_RADIUS(INT iPed)
	IF logic.Ped.Task.GoToEntity.SeekRadius != NULL
		RETURN CALL logic.Ped.Task.GoToEntity.SeekRadius(iPed)
	ENDIF
	RETURN 0.0
ENDFUNC

FUNC FLOAT GET_GO_TO_ENTITY_MOVE_BLEND_RATIO(INT iPed)
	IF logic.Ped.Task.GoToEntity.MoveBlendRatio != NULL
		RETURN CALL logic.Ped.Task.GoToEntity.MoveBlendRatio(iPed)
	ENDIF
	RETURN PEDMOVEBLENDRATIO_RUN
ENDFUNC

FUNC FLOAT GET_GO_TO_ENTITY_SLOW_DOWN_DISTANCE(INT iPed)
	IF logic.Ped.Task.GoToEntity.SlowDownDistance != NULL
		RETURN CALL logic.Ped.Task.GoToEntity.SlowDownDistance(iPed)
	ENDIF
	RETURN 2.0
ENDFUNC

FUNC EGOTO_ENTITY_FLAGS GET_GO_TO_ENTITY_FLAGS(INT iPed)
	IF logic.Ped.Task.GoToEntity.Flags != NULL
		RETURN CALL logic.Ped.Task.GoToEntity.Flags(iPed)
	ENDIF
	RETURN EGOTO_ENTITY_DEFAULT
ENDFUNC

FUNC BOOL IS_ENTITY_VALID_FOR_GOTO(ENTITY_INDEX entityId)
	IF IS_ENTITY_DEAD(entityId)
		#IF IS_DEBUG_BUILD		IF sDebugVars.bDoPedPrints			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] IS_ENTITY_VALID_FOR_GOTO - target entity is dead.")		ENDIF		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF IS_ENTITY_A_PED(entityId)
		IF IS_PED_A_PLAYER(GET_PED_INDEX_FROM_ENTITY_INDEX(entityId))
			PLAYER_INDEX targetPlayerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_PED_INDEX_FROM_ENTITY_INDEX(entityId))
			IF NATIVE_TO_INT(targetPlayerId) != -1
				IF GlobalplayerBD[NATIVE_TO_INT(targetPlayerId)].bRequiresCannotBeTargetedByAI
					#IF IS_DEBUG_BUILD		IF sDebugVars.bDoPedPrints			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] IS_ENTITY_VALID_FOR_GOTO - target player ", GET_PLAYER_NAME(targetPlayerId), " is not valid as bRequiresCannotBeTargetedByAI.")		ENDIF		#ENDIF
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN TRUE
ENDFUNC

PROC GENERIC_TASK_GO_TO_ENTITY(INT iPed, PED_INDEX pedId)

	IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedID, SCRIPT_TASK_GO_TO_ENTITY)
		ENTITY_INDEX gotoEntityId
		IF GET_GO_TO_ENTITY_INDEX(iPed, gotoEntityId)
			IF IS_ENTITY_VALID_FOR_GOTO(gotoEntityId)
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netID)
					TASK_GO_TO_ENTITY(pedId, gotoEntityId, GET_GO_TO_ENTITY_TIME(iPed), GET_GO_TO_ENTITY_SEEK_RADIUS(iPed), GET_GO_TO_ENTITY_MOVE_BLEND_RATIO(iPed), GET_GO_TO_ENTITY_SLOW_DOWN_DISTANCE(iPed), GET_GO_TO_ENTITY_FLAGS(iPed))
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC CLEANUP_PED_DATA(BOOL bMissionEnded)
	IF bMissionEnded
	
	ELSE
		DELETE_PATROL_ROUTES()
	ENDIF
ENDPROC

PROC SET_DEFAULT_PED_TASK(INT iBehaviour, INT iState, ePED_TASK eTask)

	_T_MAINTAIN_PED_TASK runTaskClient
	_T_MAINTAIN_PED_TASK runTaskServer

	SWITCH eTask
		CASE ePEDTASK_DO_SCENARIOS								runTaskClient = &GENERIC_TASK_DO_SCENARIO																									BREAK
		CASE ePEDTASK_FLEE_ON_FOOT								runTaskClient = &GENERIC_TASK_FLEE_ON_FOOT																									BREAK
		CASE ePEDTASK_COWER										runTaskClient = &GENERIC_TASK_COWER																											BREAK
		CASE ePEDTASK_ENTER_VEHICLE								runTaskClient = &GENERIC_TASK_ENTER_VEHICLE																									BREAK
		CASE ePEDTASK_EXIT_VEHICLE								runTaskClient = &GENERIC_TASK_EXIT_VEHICLE																									BREAK
		CASE ePEDTASK_STOP_VEHICLE								runTaskClient = &GENERIC_TASK_STOP_VEHICLE																									BREAK
		CASE ePEDTASK_PLAY_ANIM									runTaskClient = &GENERIC_TASK_PLAY_ANIM																										BREAK
		CASE ePEDTASK_ATTACK_HATED_TARGETS						runTaskClient = &GENERIC_TASK_COMBAT_HATED_TARGETS_IN_AREA																					BREAK
		CASE ePEDTASK_AIM_AT_COORD								runTaskClient = &GENERIC_TASK_AIM_AT_COORD																									BREAK
		CASE ePEDTASK_AIM_AT_ENTITY								runTaskClient = &GENERIC_TASK_AIM_AT_ENTITY																									BREAK
		CASE ePEDTASK_SHOOT_AT_TARGET							runTaskClient = &GENERIC_TASK_SHOOT_AT_TARGET																								BREAK
		CASE ePEDTASK_VEHICLE_GO_TO_POINT						runTaskClient = &GENERIC_TASK_VEHICLE_GO_TO_POINT_CLIENT	runTaskServer = &GENERIC_TASK_VEHICLE_GO_TO_POINT_SERVER						BREAK
		CASE ePEDTASK_COMBAT_PED								runTaskClient = &GENERIC_TASK_COMBAT_PED																									BREAK
		CASE ePEDTASK_WALK_TO_POINT								runTaskClient = &GENERIC_TASK_WALK_TO_POINT																									BREAK
		CASE ePEDTASK_RETURN_TO_START							runTaskClient = &GENERIC_TASK_RETURN_TO_START																								BREAK
		CASE ePEDTASK_ACHIEVE_HEADING							runTaskClient = &GENERIC_TASK_ACHIEVE_HEADING																								BREAK
		CASE ePEDTASK_PATROL									runTaskClient = &GENERIC_TASK_PATROL						runTaskServer = &GENERIC_TASK_PATROL_SERVER										BREAK
		CASE ePEDTASK_HANDS_UP									runTaskClient = &GENERIC_TASK_HANDS_UP																										BREAK
		CASE ePEDTASK_FLEE_FROM_COORD							runTaskClient = &GENERIC_TASK_FLEE_FROM_COORD																								BREAK
		CASE ePEDTASK_DRIVE_VEHICLE								runTaskClient = &GENERIC_TASK_DRIVE_VEHICLE																									BREAK
		CASE ePEDTASK_FLEE_IN_VEHICLE							runTaskClient = &GENERIC_TASK_FLEE_IN_VEHICLE																								BREAK
		CASE ePEDTASK_FOLLOW_PLAYER								runTaskClient = &GENERIC_TASK_FOLLOW_PLAYER																									BREAK
		CASE ePEDTASK_GO_TO_COORD_ANY_MEANS						runTaskClient = &GENERIC_TASK_GO_TO_COORD_ANY_MEANS_CLIENT	runTaskServer = &GENERIC_TASK_GO_TO_COORD_ANY_MEANS_SERVER						BREAK
		CASE ePEDTASK_WANDER_IN_VEHICLE							runTaskClient = &GENERIC_TASK_VEHICLE_WANDER_IN_AREA																						BREAK
		CASE ePEDTASK_WANDER_ON_FOOT							runTaskClient = &GENERIC_TASK_WANDER_ON_FOOT																								BREAK
		CASE ePEDTASK_WANDER_ON_FOOT_WITH_ANIM					runTaskClient = &GENERIC_TASK_WANDER_ON_FOOT_WITH_ANIM																						BREAK
		CASE ePEDTASK_LAND_HELI									runTaskClient = &GENERIC_TASK_LAND_HELI																										BREAK
		CASE ePEDTASK_VEHICLE_MISSION							runTaskClient = &GENERIC_TASK_VEHICLE_MISSION																								BREAK
		CASE ePEDTASK_STAY_IN_COVER								runTaskClient = &GENERIC_TASK_STAY_IN_COVER																									BREAK
		CASE ePEDTASK_THROW_PROJECTILE							runTaskClient = &GENERIC_TASK_THROW_PROJECTILE																								BREAK
		CASE ePEDTASK_CRASH										runTaskClient = &GENERIC_TASK_CRASH																											BREAK
		CASE ePEDTASK_GO_TO_ENTITY								runTaskClient = &GENERIC_TASK_GO_TO_ENTITY																									BREAK
	ENDSWITCH
	
	IF runTaskClient != NULL
		ADD_PED_TASK_MAINTAIN(iBehaviour, INT_TO_ENUM(DEFAULT_MODE_STATE, iState), runTaskClient, runTaskServer)
	ENDIF

ENDPROC

#IF MAX_NUM_AMBUSH_VEHICLES

PROC SET_AMBUSH_TO_FLEE()
	PED_INDEX pedID
	INT iPed
	REPEAT data.Ped.iCount iPed
		IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iPed].netId)
				pedID = NET_TO_PED(serverBD.sPed[iPed].netId)
				IF NOT IS_ENTITY_DEAD(pedID)
					GENERIC_TASK_FLEE_FROM_COORD(iPed, pedID)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC FLOAT GET_AMBUSH_DISTANCE_FROM_TARGET(INT iAmbush)
	IF iAmbush != (-1)
		IF serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromTarget != INVALID_DISTANCE
			RETURN serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromTarget
		ENDIF
	ENDIF
	RETURN INVALID_DISTANCE
ENDFUNC

FUNC FLOAT GET_AMBUSH_ATTACK_IN_AREA_DISTANCE(INT iPed)
	IF IS_THIS_MODEL_A_HELI(data.Vehicle.Vehicles[data.Ped.Peds[iPed].iVehicle].model)
		RETURN 100.0
	ENDIF
	RETURN 30.0
ENDFUNC

FUNC BOOL PED_TASKS_AMBUSH_SHOULD_ATTACK_TARGET_IN_AREA(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	INT iAmbush = GET_AMBUSH_INDEX_FROM_ENTITY(eENTITYTYPE_PED, iPed)
	IF GET_AMBUSH_DISTANCE_FROM_TARGET(iAmbush) < GET_AMBUSH_ATTACK_IN_AREA_DISTANCE(iPed)
		IF NOT DOES_ENTITY_EXIST(GET_AMBUSH_TARGET_PED_INDEX(iAmbush))
			IF IS_THIS_MODEL_A_HELI(data.Vehicle.Vehicles[data.Ped.Peds[iPed].iVehicle].model)
				RETURN TRUE
			ELSE
				IF NOT IS_PED_IN_ANY_VEHICLE(pedId)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PED_TASKS_AMBUSH_SHOULD_EXIT_VEHICLE(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	
	IF DOES_PED_SPAWN_IN_AIR_VEHICLE(iPed)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(pedId)
		INT iAmbush = GET_AMBUSH_INDEX_FROM_ENTITY(eENTITYTYPE_PED, iPed)
		IF GET_AMBUSH_DISTANCE_FROM_TARGET(iAmbush) < 10.0
			PED_INDEX targetPedId = GET_AMBUSH_TARGET_PED_INDEX(iAmbush)
			IF NOT DOES_ENTITY_EXIST(targetPedId)
			OR NOT IS_PED_IN_ANY_VEHICLE(targetPedId, TRUE)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PED_TASKS_AMBUSH_SHOULD_FLEE(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	IF GET_END_REASON() != eENDREASON_NO_REASON_YET
		RETURN TRUE
	ENDIF
	
	IF GET_AMBUSH_DROPOFF_DISMISS_RANGE() != (-1)
		INT iAmbush = GET_AMBUSH_INDEX_FROM_ENTITY(eENTITYTYPE_PED, iPed)
		IF iAmbush != (-1)
		AND serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromDropoff != INVALID_DISTANCE
		AND serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromDropoff < GET_AMBUSH_DROPOFF_DISMISS_RANGE()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_AMBUSH_DRIVE_TO_TARGET_DISTANCE(INT iPed)
	IF IS_THIS_MODEL_A_HELI(data.Vehicle.Vehicles[data.Ped.Peds[iPed].iVehicle].model)
		RETURN 100.0
	ENDIF
	RETURN 30.0
ENDFUNC

FUNC BOOL PED_TASKS_AMBUSH_SHOULD_DRIVE_TO_TARGET(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	IF data.Ped.Peds[iPed].eSeat = VS_DRIVER
		IF IS_PED_IN_ANY_VEHICLE(pedId)
			INT iAmbush = GET_AMBUSH_INDEX_FROM_ENTITY(eENTITYTYPE_PED, iPed)
			IF GET_AMBUSH_DISTANCE_FROM_TARGET(iAmbush) > GET_AMBUSH_DRIVE_TO_TARGET_DISTANCE(iPed)
				IF NOT DOES_ENTITY_EXIST(GET_AMBUSH_TARGET_PED_INDEX(iAmbush))
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PED_TASKS_AMBUSH_SHOULD_COMBAT_PED(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	IF IS_PED_IN_ANY_VEHICLE(pedId)
		INT iAmbush = GET_AMBUSH_INDEX_FROM_ENTITY(eENTITYTYPE_PED, iPed)
		IF DOES_ENTITY_EXIST(GET_AMBUSH_TARGET_PED_INDEX(iAmbush))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL PED_TASKS_AMBUSH_SHOULD_ENTER_VEHICLE(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	
	IF DOES_PED_SPAWN_IN_AIR_VEHICLE(iPed)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(pedId)
		IF PED_TASKS_AMBUSH_SHOULD_DRIVE_TO_TARGET(iPed, pedId)
			RETURN TRUE
		ENDIF
		
		INT iAmbush = GET_AMBUSH_INDEX_FROM_ENTITY(eENTITYTYPE_PED, iPed)
		PED_INDEX targetPedId = GET_AMBUSH_TARGET_PED_INDEX(iAmbush)
		IF DOES_ENTITY_EXIST(targetPedId) AND NOT IS_PED_INJURED(targetPedId)
			IF IS_PED_GETTING_INTO_A_VEHICLE(targetPedId)
				RETURN TRUE
			ELIF IS_PED_IN_ANY_VEHICLE(targetPedId)
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedID, SCRIPT_TASK_COMBAT)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL PED_TASKS_DEFAULT_SHOULD_DO_SCENARIO(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	RETURN (data.Ped.Peds[iPed].iVehicle = (-1) AND data.Ped.Peds[iPed].iScenarioIndex != (-1))
ENDFUNC

FUNC BOOL PED_TASKS_DEFAULT_SHOULD_PATROL(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	RETURN IS_PED_BIT_SET(iPed, ePEDBITSET_PATROL_PED)
ENDFUNC

FUNC BOOL PED_TASKS_DEFAULT_SHOULD_ATTACK_HATED(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	RETURN HAS_PED_BEEN_TRIGGERED(iPed)
ENDFUNC

FUNC BOOL PED_TASKS_DEFAULT_SHOULD_FLEE(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	RETURN HAS_PED_BEEN_TRIGGERED(iPed)
ENDFUNC

FUNC BOOL PED_TASKS_DEFAULT_SHOULD_PROGRESS_VEHICLE_GO_TO(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	RETURN IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_REACHED_FINAL_CHECKPOINT)
ENDFUNC

FUNC BOOL PED_TASKS_DEFAULT_SHOULD_PROGRESS_GO_TO(INT iPed, PED_INDEX pedId)
	UNUSED_PARAMETER(pedId)
	RETURN IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_REACHED_FINAL_CHECKPOINT)
ENDFUNC

PROC SET_DEFAULT_PED_TASK_TRANSITION(INT iBehaviour, ePED_TASK eTask, ePED_TASK eNextTask, _T_SHOULD_SWITCH_PED_TASK &transitionOn)
	UNUSED_PARAMETER(iBehaviour)
	UNUSED_PARAMETER(eTask)
	UNUSED_PARAMETER(eNextTask)
	UNUSED_PARAMETER(transitionOn)
	
	#IF MAX_NUM_AMBUSH_VEHICLES
	IF iBehaviour = MAX_NUM_PED_BEHAVIOURS	// Ambush behaviour is always last
		SWITCH eTask
			CASE ePEDTASK_COMBAT_PED
				SWITCH eNextTask
					CASE ePEDTASK_ATTACK_HATED_TARGETS			transitionOn = &PED_TASKS_AMBUSH_SHOULD_ATTACK_TARGET_IN_AREA		BREAK
					CASE ePEDTASK_EXIT_VEHICLE					transitionOn = &PED_TASKS_AMBUSH_SHOULD_EXIT_VEHICLE				BREAK
					CASE ePEDTASK_GO_TO_COORD_ANY_MEANS			transitionOn = &PED_TASKS_AMBUSH_SHOULD_DRIVE_TO_TARGET				BREAK
					CASE ePEDTASK_FLEE_FROM_COORD				transitionOn = &PED_TASKS_AMBUSH_SHOULD_FLEE						BREAK
					CASE ePEDTASK_ENTER_VEHICLE					transitionOn = &PED_TASKS_AMBUSH_SHOULD_ENTER_VEHICLE				BREAK
				ENDSWITCH
			BREAK
			CASE ePEDTASK_ATTACK_HATED_TARGETS
				SWITCH eNextTask
					CASE ePEDTASK_COMBAT_PED					transitionOn = &PED_TASKS_AMBUSH_SHOULD_COMBAT_PED					BREAK
					CASE ePEDTASK_EXIT_VEHICLE					transitionOn = &PED_TASKS_AMBUSH_SHOULD_EXIT_VEHICLE				BREAK
					CASE ePEDTASK_GO_TO_COORD_ANY_MEANS			transitionOn = &PED_TASKS_AMBUSH_SHOULD_DRIVE_TO_TARGET				BREAK
					CASE ePEDTASK_FLEE_FROM_COORD				transitionOn = &PED_TASKS_AMBUSH_SHOULD_FLEE						BREAK
					CASE ePEDTASK_ENTER_VEHICLE					transitionOn = &PED_TASKS_AMBUSH_SHOULD_ENTER_VEHICLE				BREAK
				ENDSWITCH
			BREAK
			CASE ePEDTASK_EXIT_VEHICLE
				SWITCH eNextTask
					CASE ePEDTASK_COMBAT_PED					transitionOn = &PED_TASKS_AMBUSH_SHOULD_COMBAT_PED					BREAK
					CASE ePEDTASK_ATTACK_HATED_TARGETS			transitionOn = &PED_TASKS_AMBUSH_SHOULD_ATTACK_TARGET_IN_AREA		BREAK
					CASE ePEDTASK_GO_TO_COORD_ANY_MEANS			transitionOn = &PED_TASKS_AMBUSH_SHOULD_DRIVE_TO_TARGET				BREAK
					CASE ePEDTASK_FLEE_FROM_COORD				transitionOn = &PED_TASKS_AMBUSH_SHOULD_FLEE						BREAK
					CASE ePEDTASK_ENTER_VEHICLE					transitionOn = &PED_TASKS_AMBUSH_SHOULD_ENTER_VEHICLE				BREAK
				ENDSWITCH
			BREAK
			CASE ePEDTASK_GO_TO_COORD_ANY_MEANS
				SWITCH eNextTask
					CASE ePEDTASK_COMBAT_PED					transitionOn = &PED_TASKS_AMBUSH_SHOULD_COMBAT_PED					BREAK
					CASE ePEDTASK_ATTACK_HATED_TARGETS			transitionOn = &PED_TASKS_AMBUSH_SHOULD_ATTACK_TARGET_IN_AREA		BREAK
					CASE ePEDTASK_EXIT_VEHICLE					transitionOn = &PED_TASKS_AMBUSH_SHOULD_EXIT_VEHICLE				BREAK
					CASE ePEDTASK_FLEE_FROM_COORD				transitionOn = &PED_TASKS_AMBUSH_SHOULD_FLEE						BREAK
					CASE ePEDTASK_ENTER_VEHICLE					transitionOn = &PED_TASKS_AMBUSH_SHOULD_ENTER_VEHICLE				BREAK
				ENDSWITCH
			BREAK
			CASE ePEDTASK_ENTER_VEHICLE
				SWITCH eNextTask
					CASE ePEDTASK_COMBAT_PED					transitionOn = &PED_TASKS_AMBUSH_SHOULD_COMBAT_PED					BREAK
					CASE ePEDTASK_ATTACK_HATED_TARGETS			transitionOn = &PED_TASKS_AMBUSH_SHOULD_ATTACK_TARGET_IN_AREA		BREAK
					CASE ePEDTASK_GO_TO_COORD_ANY_MEANS			transitionOn = &PED_TASKS_AMBUSH_SHOULD_DRIVE_TO_TARGET				BREAK
					CASE ePEDTASK_FLEE_FROM_COORD				transitionOn = &PED_TASKS_AMBUSH_SHOULD_FLEE						BREAK
					CASE ePEDTASK_EXIT_VEHICLE					transitionOn = &PED_TASKS_AMBUSH_SHOULD_EXIT_VEHICLE				BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
		EXIT
	ENDIF
	#ENDIF
	
	// Default conditions
	SWITCH eNextTask
		CASE ePEDTASK_DO_SCENARIOS								transitionOn = &PED_TASKS_DEFAULT_SHOULD_DO_SCENARIO				BREAK
		CASE ePEDTASK_PATROL									transitionOn = &PED_TASKS_DEFAULT_SHOULD_PATROL						BREAK
		CASE ePEDTASK_ATTACK_HATED_TARGETS						transitionOn = &PED_TASKS_DEFAULT_SHOULD_ATTACK_HATED				BREAK
		CASE ePEDTASK_FLEE_FROM_COORD							transitionOn = &PED_TASKS_DEFAULT_SHOULD_FLEE						BREAK
		CASE ePEDTASK_FLEE_IN_VEHICLE							transitionOn = &PED_TASKS_DEFAULT_SHOULD_FLEE						BREAK
		CASE ePEDTASK_FLEE_ON_FOOT								transitionOn = &PED_TASKS_DEFAULT_SHOULD_FLEE						BREAK
		CASE ePEDTASK_COWER										transitionOn = &PED_TASKS_DEFAULT_SHOULD_FLEE						BREAK
	ENDSWITCH

	SWITCH eTask
		CASE ePEDTASK_VEHICLE_GO_TO_POINT						transitionOn = &PED_TASKS_DEFAULT_SHOULD_PROGRESS_VEHICLE_GO_TO		BREAK
		CASE ePEDTASK_GO_TO_COORD_ANY_MEANS						transitionOn = &PED_TASKS_DEFAULT_SHOULD_PROGRESS_GO_TO				BREAK
	ENDSWITCH
	
ENDPROC

PROC SETUP_DEFAULT_PED_TASKS(INT iBehaviour)
	INT iTask, iTransition, iNextState
		
	REPEAT MAX_NUM_PED_TASKS iTask
		ePED_TASK eTask = sPedLocal.sBehaviour[iBehaviour].Task[iTask].eType
		IF eTask != ePEDTASK_INVALID
			IF sPedLocal.sBehaviour[iBehaviour].Task[iTask].runTaskClient = NULL
				SET_DEFAULT_PED_TASK(iBehaviour, iTask, eTask)
			ENDIF
			
			REPEAT sPedLocal.sBehaviour[iBehaviour].Task[iTask].iNumTransitions iTransition
				IF sPedLocal.sBehaviour[iBehaviour].Task[iTask].transitions[iTransition].shouldMoveState = NULL
					iNextState = sPedLocal.sBehaviour[iBehaviour].Task[iTask].transitions[iTransition].iTask
					SET_DEFAULT_PED_TASK_TRANSITION(iBehaviour, eTask, sPedLocal.sBehaviour[iBehaviour].Task[iNextState].eType, sPedLocal.sBehaviour[iBehaviour].Task[iTask].transitions[iTransition].shouldMoveState)		
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT

ENDPROC

FUNC INT GET_NUM_PED_BEHAVIOURS()

	INT iMaxBehaviour

	INT iPed
	REPEAT data.Ped.iCount iPed	
		INT iBehaviour = GET_PED_BEHAVIOUR(iPed)
		
		IF iBehaviour < MAX_NUM_PED_BEHAVIOURS
		AND iBehaviour > iMaxBehaviour
			iMaxBehaviour = iBehaviour
		ENDIF
	ENDREPEAT
	
	RETURN iMaxBehaviour + 1
	
ENDFUNC

PROC SETUP_PED_LOGIC()

	IF IS_GENERIC_BIT_SET(eGENERICBITSET_SETUP_PED_LOGIC)
		PRINTLN("[",scriptName,"] - SETUP_PED_LOGIC - eGENERICBITSET_SETUP_PED_LOGIC already set, exiting.")
		EXIT
	ENDIF
	
	IF data.Ped.iCount = 0
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_TASK_SETUP] - No peds in this variation.")
		SET_GENERIC_BIT(eGENERICBITSET_SETUP_PED_LOGIC)
		EXIT
	ENDIF
	
	INT iBehaviour
	INT iNumBehaviours = GET_NUM_PED_BEHAVIOURS()
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_TASK_SETUP] - iNumBehaviours - ", iNumBehaviours)
	
	REPEAT iNumBehaviours iBehaviour
		CALL logic.Ped.Behaviour.Setup(iBehaviour)
		SETUP_DEFAULT_PED_TASKS(iBehaviour)
	ENDREPEAT
	
	#IF MAX_NUM_AMBUSH_VEHICLES
	SETUP_AMBUSH_PED_TASKS(MAX_NUM_PED_BEHAVIOURS)
	SETUP_DEFAULT_PED_TASKS(MAX_NUM_PED_BEHAVIOURS)
	#ENDIF
	
	INT iPed
	REPEAT data.Ped.iCount iPed		
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [Ped_",iPed,"] [PED_TASK_SETUP] -  Behaviour - [", GET_PED_BEHAVIOUR(iPed),"]")
	
		CALL logic.Ped.Triggers.Setup(iPed)
		
		#IF MAX_NUM_PATROLS
		SETUP_PED_PATROL(iPed)
		#ENDIF

	ENDREPEAT
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_TASK_SETUP] - Setup complete.")
	
	SET_GENERIC_BIT(eGENERICBITSET_SETUP_PED_LOGIC)

ENDPROC

FUNC BOOL SHOULD_PROCESS_PED_TASK_EVERY_FRAME(ePED_TASK eTask)

	SWITCH eTask
		CASE ePEDTASK_VEHICLE_GO_TO_POINT
		CASE ePEDTASK_WALK_TO_POINT
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

PROC PROCESS_CURRENT_PED_TASK_CLIENT(INT iPed, PED_INDEX pedId, BOOL bEveryFrame)

	INT iState = GET_PED_TASK_ID(iPed)

	IF iState = -1
		EXIT
	ENDIF
	
	INT iBehaviour = GET_PED_BEHAVIOUR(iPed)
	
	// Check if task should be processed this frame
	IF SHOULD_PROCESS_PED_TASK_EVERY_FRAME(sPedLocal.sBehaviour[iBehaviour].Task[iState].eType) != bEveryFrame
		EXIT
	ENDIF
		
	// Run client task logic
	IF sPedLocal.sBehaviour[iBehaviour].Task[iState].runTaskClient != NULL
		CALL sPedLocal.sBehaviour[iBehaviour].Task[iState].runTaskClient(iPed, PedId)
	ENDIF

ENDPROC

PROC PROCESS_CURRENT_PED_TASK_SERVER(INT iPed, PED_INDEX pedId, BOOL bEveryFrame)
			
	INT iBehaviour = GET_PED_BEHAVIOUR(iPed)
	
	// First time through
	IF GET_PED_TASK_ID(iPed) = -1
		SET_PED_TASK(iPed, 0)
		IF sPedLocal.sBehaviour[iBehaviour].Task[GET_PED_TASK_ID(iPed)].onEntry != NULL
			CALL sPedLocal.sBehaviour[iBehaviour].Task[GET_PED_TASK_ID(iPed)].onEntry(iPed, pedId)
		ENDIF
		EXIT
	ENDIF
	
	// Check if task should be processed this frame
	IF SHOULD_PROCESS_PED_TASK_EVERY_FRAME(sPedLocal.sBehaviour[iBehaviour].Task[GET_PED_TASK_ID(iPed)].eType) != bEveryFrame
		EXIT
	ENDIF
		
	// Run server task logic
	IF sPedLocal.sBehaviour[iBehaviour].Task[GET_PED_TASK_ID(iPed)].runTaskServer != NULL
		CALL sPedLocal.sBehaviour[iBehaviour].Task[GET_PED_TASK_ID(iPed)].runTaskServer(iPed, pedId)
	ENDIF
	
	// Check for transitions
	INT iTransition
	WHILE GET_PED_TASK_ID(iPed) < sPedLocal.sBehaviour[iBehaviour].iNumTasks
	AND iTransition < sPedLocal.sBehaviour[iBehaviour].Task[GET_PED_TASK_ID(iPed)].iNumTransitions
	AND sPedLocal.sBehaviour[iBehaviour].Task[GET_PED_TASK_ID(iPed)].transitions[iTransition].iTask != -1
		IF CALL sPedLocal.sBehaviour[iBehaviour].Task[GET_PED_TASK_ID(iPed)].transitions[iTransition].shouldMoveState(iPed, PedId)
			IF sPedLocal.sBehaviour[iBehaviour].Task[GET_PED_TASK_ID(iPed)].onExit != NULL
				CALL sPedLocal.sBehaviour[iBehaviour].Task[GET_PED_TASK_ID(iPed)].onExit(iPed, pedId)
			ENDIF
			SET_PED_TASK(iPed, sPedLocal.sBehaviour[iBehaviour].Task[GET_PED_TASK_ID(iPed)].transitions[iTransition].iTask)
			IF sPedLocal.sBehaviour[iBehaviour].Task[GET_PED_TASK_ID(iPed)].onEntry != NULL
				CALL sPedLocal.sBehaviour[iBehaviour].Task[GET_PED_TASK_ID(iPed)].onEntry(iPed, pedId)
			ENDIF
			EXIT
		ENDIF
		iTransition++
	ENDWHILE

ENDPROC

PROC PROCESS_WANTED_LEVEL_ON_PED_TRIGGER(INT iPed)

	IF IS_DATA_BIT_SET(eDATABITSET_DISABLE_WANTED_FROM_MISSION_COPS)
		EXIT
	ENDIF

	// Give wanted level regardless of mission for triggering cop
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iPed].netId)
		IF IS_PED_A_COP(NET_TO_PED(serverBD.sPed[iPed].netId))	
			GIVE_WANTED_LEVEL_TO_GANG(GET_COP_WANTED_LEVEL(), TRUE)
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [PED_TRIGGERS] [PED_", iPed,"] PROCESS_WANTED_LEVEL_ON_PED_TRIGGER - Given wanted level due to cop ped being triggered")
		ENDIF
	ENDIF

ENDPROC

PROC PROCESS_CURRENT_PED_TRIGGERS(INT iPed)

	INT iGroup = data.Ped.Peds[iPed].iGroup
	
	IF HAS_PED_GROUP_BEEN_TRIGGERED(iGroup)
		EXIT
	ENDIF

	INT iTrigger
	REPEAT sPedLocal.sPed[iPed].iNumTriggers iTrigger
		IF sPedLocal.sPed[iPed].Trigger[iTrigger].Activate != NULL
			IF CALL sPedLocal.sPed[iPed].Trigger[iTrigger].Activate(iPed)
				IF NOT IS_BIT_SET(serverBD.sPedGroup.iAlertBs, iGroup)
					PROCESS_WANTED_LEVEL_ON_PED_TRIGGER(iPed)
				ENDIF
				
				SET_BIT(serverBD.sPedGroup.iAlertBs, iGroup)
				IF logic.Ped.Group.OnTriggered != NULL
					CALL logic.Ped.Group.OnTriggered(iPed, iGroup, iTrigger)
				ENDIF
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_TRIGGERS] [PED_", iPed,"] Alerting ped group ", iGroup, " on trigger ", iTrigger)
			ENDIF
		ENDIF
	ENDREPEAT

ENDPROC

//----------------------
//	MAINTAINS
//----------------------

FUNC BOOL SHOULD_CHECK_FOR_PLAYER_TARGETTING_PED(INT iPed)	
	RETURN IS_PED_BIT_SET(iPed, ePEDBITSET_ENABLE_TARGETTED) 
		AND NOT IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_BEEN_TARGETTED)
ENDFUNC

FUNC FLOAT GET_TARGET_RANGE_FOR_REACTIONS() 

	IF logic.Ped.Triggers.Targetted.Range != NULL
		RETURN CALL logic.Ped.Triggers.Targetted.Range()
	ENDIF

	RETURN 20.0
ENDFUNC

FUNC BOOL IS_PLAYER_TARGETTING_PED(PED_INDEX pedId, FLOAT fRange2 = 2500.0)
	IF IS_PLAYER_TARGETTING_ENTITY(LOCAL_PLAYER_INDEX, pedId)
	OR IS_PLAYER_FREE_AIMING_AT_ENTITY(LOCAL_PLAYER_INDEX, pedId)
		VECTOR vPedCoords = GET_ENTITY_COORDS(pedId, FALSE)		
		IF VDIST2(LOCAL_PLAYER_COORD, vPedCoords) < (fRange2)
			RETURN TRUE	
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


PROC MAINTAIN_HAS_PLAYER_TARGETTED_PED(INT iPed, PED_INDEX pedId, FLOAT fRange2 = 2500.0)
	
	IF NOT IS_LOCAL_ALIVE
		EXIT
	ENDIF
	
	IF NOT SHOULD_CHECK_FOR_PLAYER_TARGETTING_PED(iPed)
		EXIT
	ENDIF	
	
	IF IS_PLAYER_TARGETTING_PED(pedId, fRange2)
		SET_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_I_HAVE_BEEN_TARGETTED)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_MAINTAIN_PED_KNOCKED_INTO_BY_PLAYER(INT iPed)	
	RETURN IS_PED_BIT_SET(iPed, ePEDBITSET_ENABLE_KNOCKED_INTO)
ENDFUNC

PROC MAINTAIN_PED_KNOCKED_INTO_BY_PLAYER(INT iPed, PED_INDEX pedID)
	
	IF NOT IS_LOCAL_ALIVE
		EXIT
	ENDIF
	
	IF NOT SHOULD_MAINTAIN_PED_KNOCKED_INTO_BY_PLAYER(iPed)
		EXIT
	ENDIF	
	
	IF IS_PED_RAGDOLL(pedID)
	AND IS_ENTITY_TOUCHING_ENTITY(pedID, LOCAL_PED_INDEX)
		SET_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_I_HAVE_BEEN_KNOCKED_INTO)
		IF logic.Ped.OnBumpedByPlayer != NULL
			CALL logic.Ped.OnBumpedByPlayer(iPed)
		ENDIF
		
	ENDIF
ENDPROC

FUNC BOOL SHOULD_MAINTAIN_IS_PLAYER_NEAR_PED(INT iPed)
	IF logic.Ped.Triggers.PlayerNearPed.Enable != NULL
		RETURN CALL logic.Ped.Triggers.PlayerNearPed.Enable(iPed)
	ENDIF
	
	RETURN IS_PED_BIT_SET(iPed, ePEDBITSET_ENABLE_PLAYER_NEAR_PED)
ENDFUNC

FUNC FLOAT GET_PLAYER_NEAR_PED_RANGE(INT iPed)
	IF logic.Ped.Triggers.PlayerNearPed.Range != NULL
		RETURN CALL logic.Ped.Triggers.PlayerNearPed.Range(iPed)
	ENDIF
	
	RETURN 50.0
ENDFUNC

FUNC BOOL GET_PLAYER_NEAR_PED_REQUIRES_LINE_OF_SIGHT(INT iPed)
	IF logic.Ped.Triggers.PlayerNearPed.LineOfSight != NULL
		RETURN CALL logic.Ped.Triggers.PlayerNearPed.LineOfSight(iPed)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_PLAYER_NEAR_PED_DELAY(INT iPed)
	IF logic.Ped.Triggers.PlayerNearPed.Delay != NULL
		RETURN CALL logic.Ped.Triggers.PlayerNearPed.Delay(iPed)
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC BOOL SHOULD_PLAYER_NEAR_PED_USE_PERSISTENT_CHECK(INT iPed)
	IF logic.Ped.Triggers.PlayerNearPed.PersistentCheck != NULL
		RETURN CALL logic.Ped.Triggers.PlayerNearPed.PersistentCheck(iPed)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_NEAR_PED(PED_INDEX pedID, FLOAT fNearRange, BOOL bCheckLoS = FALSE)
	IF VDIST2(GET_ENTITY_COORDS(pedID, FALSE), LOCAL_PLAYER_COORD) <= fNearRange * fNearRange
		IF bCheckLoS
			IF IS_LOCAL_ALIVE
				RETURN HAS_ENTITY_CLEAR_LOS_TO_ENTITY(LOCAL_PED_INDEX, pedID, SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_VEHICLE | SCRIPT_INCLUDE_PED | SCRIPT_INCLUDE_RAGDOLL | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_FOLIAGE)
			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_IS_PLAYER_NEAR_PED(INT iPed, PED_INDEX pedID)
	IF NOT SHOULD_MAINTAIN_IS_PLAYER_NEAR_PED(iPed)	
		EXIT
	ENDIF
	
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_PLAYER_HAS_BEEN_NEAR)
	AND NOT SHOULD_PLAYER_NEAR_PED_USE_PERSISTENT_CHECK(iPed)
		EXIT
	ENDIF
	
	IF NOT IS_PED_CLIENT_BIT_SET(iPed, LOCAL_PARTICIPANT_INDEX, ePEDCLIENTBITSET_NEAR_PLAYER)
		IF IS_PLAYER_NEAR_PED(pedId, GET_PLAYER_NEAR_PED_RANGE(iPed), GET_PLAYER_NEAR_PED_REQUIRES_LINE_OF_SIGHT(iPed))
			IF HAS_NET_TIMER_EXPIRED(sPedLocal.sPed[iPed].stPlayerNearPedTimer, GET_PLAYER_NEAR_PED_DELAY(iPed))
				SET_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_NEAR_PLAYER)
				
				IF logic.Ped.Triggers.PlayerNearPed.OnChanged != NULL
					CALL logic.Ped.Triggers.PlayerNearPed.OnChanged(iPed, TRUE)
				ENDIF
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(sPedLocal.sPed[iPed].stPlayerNearPedTimer)
				RESET_NET_TIMER(sPedLocal.sPed[iPed].stPlayerNearPedTimer)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_PLAYER_NEAR_PED(pedId, GET_PLAYER_NEAR_PED_RANGE(iPed), GET_PLAYER_NEAR_PED_REQUIRES_LINE_OF_SIGHT(iPed))
			CLEAR_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_NEAR_PLAYER)
			
			IF logic.Ped.Triggers.PlayerNearPed.OnChanged != NULL
				CALL logic.Ped.Triggers.PlayerNearPed.OnChanged(iPed, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC INIT_COMBAT_FLAG()
	CLEAR_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_IN_COMBAT_WITH_ANY_PED #IF IS_DEBUG_BUILD , TRUE #ENDIF )
ENDPROC

FUNC INT DEFAULT_COP_COMBAT_WANTED_DELAY()
	RETURN 0
ENDFUNC

FUNC INT DEFAULT_LEVEL_FOR_SHOOTING_COPS()
	IF NOT IS_GLOBAL_FLAG_BIT_SET(eGLOBALFLAGBITSET_ENABLING_WANTED_LEVEL_INCREASE)
		RETURN GET_COP_WANTED_LEVEL()
	ENDIF
	
	RETURN 1
ENDFUNC

PROC MAINTAIN_PED_COMBAT_TARGET(INT iPed, PED_INDEX pedID)
	UNUSED_PARAMETER(iPed)
	
	ENTITY_INDEX target = GET_PED_TARGET_FROM_COMBAT_PED(pedID, TRUE)
	
	IF NOT DOES_ENTITY_EXIST(target)
		EXIT
	ENDIF	
	
	IF NOT IS_ENTITY_A_PED(target)
		EXIT
	ENDIF
	
	IF GET_PED_INDEX_FROM_ENTITY_INDEX(target) != LOCAL_PED_INDEX
		EXIT
	ENDIF	
	
	SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_IN_COMBAT_WITH_ANY_PED #IF IS_DEBUG_BUILD , TRUE #ENDIF )
	
	// Give wanted level in combat with scripted cops
	IF NOT IS_DATA_BIT_SET(eDATABITSET_DISABLE_WANTED_FROM_MISSION_COPS)
		IF IS_PED_A_COP(pedID)
			IF GET_PLAYER_WANTED_LEVEL(LOCAL_PLAYER_INDEX) < GET_COP_WANTED_LEVEL()
			OR ARE_PLAYER_FLASHING_STARS_ABOUT_TO_DROP(LOCAL_PLAYER_INDEX)
				INT iDelay = CALL logic.Wanted.CopCombatDelay()
				GIVE_WANTED_LEVEL_TO_PLAYER(LOCAL_PLAYER_INDEX, GET_COP_WANTED_LEVEL(), TRUE, iDelay)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] MAINTAIN_PED_COMBAT_TARGET - Giving wanted level due to being target of cop ped", iPed, " after delay of ", iDelay, "ms.")
			ENDIF
		ENDIF
	ENDIF

ENDPROC

#IF IS_DEBUG_BUILD
PROC MAINTAIN_PED_DEBUG_CLIENT(INT iPed, PED_INDEX pedId, BOOL bDead)
	IF NOT bDead
		IF IS_PED_BIT_SET(iPed, ePEDBITSET_DEBUG_KILL)
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
				SET_ENTITY_HEALTH(pedId, 0)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL SHOULD_ROTATE_PED_BLIP(INT iPed)
	UNUSED_PARAMETER(iPed)
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SHOW_INTERIOR_OR_EXTERIOR_PED(INT iPed)
	UNUSED_PARAMETER(iPed)
	
	#IF MAX_NUM_MISSION_INTERIORS
	IF CALL logic.Interior.ShouldRunInteriorExteriorBlipChecks(iPed)
		// Prevent seeing interior peds if outside
		IF IS_PED_BIT_SET(iPed, ePEDBITSET_INSIDE_MISSION_INTERIOR)
		AND NOT IS_GENERIC_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX, eGENERICCLIENTBITSET_INSIDE_MISSION_INTERIOR)
			RETURN TRUE
		ENDIF
		
		// Prevent seeing outside peds if inside
		IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_INSIDE_MISSION_INTERIOR)
		AND IS_GENERIC_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX, eGENERICCLIENTBITSET_INSIDE_MISSION_INTERIOR)	
			RETURN FALSE
		ENDIF
	ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_SHOW_PED_VISION_CONE(INT iPed)

	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		RETURN FALSE
	ENDIF

	IF logic.Ped.Blip.VisionCone != null
		RETURN CALL logic.Ped.Blip.VisionCone(iPed)
	ENDIF
	
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_CHECK_VISION)
		RETURN TRUE	
	ENDIF
	
	IF IS_DATA_BIT_SET(eDATABITSET_ENABLE_STEALTH)
	AND NOT IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		RETURN TRUE
	ENDIF
			
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_BLIP_PED(INT iPed, BOOL bDead)
	IF bDead 
	AND NOT IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_ALLOW_DEAD_BLIPPING)
		RETURN FALSE
	ENDIF
	
	IF logic.Ped.Blip.Enable != null
		RETURN CALL logic.Ped.Blip.Enable(iPed)
	ENDIF
	
	IF NOT SHOULD_SHOW_INTERIOR_OR_EXTERIOR_PED(iPed)
		RETURN FALSE
	ENDIF
	
	SWITCH GET_CLIENT_MODE_STATE()
		CASE eMODESTATE_TAKE_OUT_TARGETS
		CASE eMODESTATE_PROTECT_TARGETS
			IF IS_PED_BIT_SET(iPed, ePEDBITSET_TARGET_PED)
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE eMODESTATE_COLLECT_PED_PICKUP
		CASE eMODESTATE_COLLECT_PED_SIGNAL
		CASE eMODESTATE_COLLECT_PED_WAIT
			IF GET_MISSION_ENTITY_FOR_COLLECT_PED(iPed) = GET_MISSION_ENTITY_PLAYER_IS_HOLDING_OR_IN_VEHICLE_WITH()
				RETURN TRUE
			ENDIF
		BREAK
		
		#IF MAX_NUM_ARREST_PED_SUSPECTS
		CASE eMODESTATE_ARREST_PED
			INT i
			REPEAT data.ArrestPed.iNumSuspects i
				IF data.ArrestPed.Suspects[i].iPed = iPed
					RETURN TRUE
				ENDIF
			ENDREPEAT
		BREAK
		#ENDIF
	ENDSWITCH 
		
	IF SHOULD_SHOW_PED_VISION_CONE(iPed)
	AND NOT HAS_PED_BEEN_TRIGGERED(iPed)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_USE_GENERIC_ENEMY_PED_BLIP(INT iPed, BOOL bDead)
	IF logic.Ped.DisableGenericEnemyPedBlip != NULL
	AND CALL logic.Ped.DisableGenericEnemyPedBlip(iPed, bDead)
		RETURN FALSE
	ENDIF
	
	// Prevent seeing ped blips when in non-mission interiors
	IF NOT IS_GENERIC_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX, eGENERICCLIENTBITSET_INSIDE_MISSION_INTERIOR)
	AND IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR()
		RETURN FALSE
	ENDIF
	
	// Prevent seeing ped blips in real world location e.g. under docks, etc
	IF IS_GENERIC_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX, eGENERICCLIENTBITSET_INSIDE_MISSION_INTERIOR)
	AND IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(LOCAL_PLAYER_INDEX)
		RETURN FALSE
	ENDIF
	
	IF NOT SHOULD_SHOW_INTERIOR_OR_EXTERIOR_PED(iPed)
		RETURN FALSE
	ENDIF
	
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_I_AM_A_CIVILIAN)
	OR IS_PED_CLIENT_BIT_SET(iPed, LOCAL_PARTICIPANT_INDEX, ePEDCLIENTBITSET_SET_AS_FRIENDLY)
		RETURN FALSE
	ENDIF 
	
	IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		IF NOT HAS_PED_BEEN_TRIGGERED(iPed)
			RETURN FALSE
		ENDIF
	ELSE
		#IF MAX_NUM_AMBUSH_VEHICLES
		IF logic.Ambush.ForceCleanup != NULL
		AND CALL logic.Ambush.ForceCleanup()
			RETURN FALSE
		ENDIF
		#ENDIF
	ENDIF
	
	IF SHOULD_BLIP_PED(iPed, bDead)
		RETURN FALSE
	ENDIF
	
	IF bDead
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC STRING GET_ENEMY_PED_NAME(INT iPed)
	IF logic.Ped.Blip.Name != null
		RETURN CALL logic.Ped.Blip.Name(iPed)
	ENDIF
	
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_I_AM_A_COP)
		RETURN "PB_COP"
	ENDIF
	
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_I_AM_A_CIVILIAN)
		RETURN "PB_CIV"
	ENDIF
	
	SWITCH GET_CLIENT_MODE_STATE()
		CASE eMODESTATE_TAKE_OUT_TARGETS
		CASE eMODESTATE_PROTECT_TARGETS
			IF IS_PED_BIT_SET(iPed, ePEDBITSET_TARGET_PED)
				RETURN "PB_TARGET"
			ENDIF
		BREAK
		
		#IF MAX_NUM_ARREST_PED_SUSPECTS
		CASE eMODESTATE_ARREST_PED
			RETURN "FMC_BLIP_SUSPECT"
		#ENDIF
	ENDSWITCH
	
	RETURN "PB_ENEMY"
ENDFUNC

FUNC FLOAT GET_ENEMY_PED_BLIP_NOTICEABLE_RANGE(INT iPed)
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		RETURN 300.0
	ENDIF
	RETURN -1.0
ENDFUNC

FUNC BLIP_SPRITE GET_PED_BLIP_SPRITE(INT iPed)
	IF logic.Ped.Blip.Sprite != null
		RETURN CALL logic.Ped.Blip.Sprite(iPed)
	ENDIF
	
	SWITCH GET_CLIENT_MODE_STATE()
		CASE eMODESTATE_TAKE_OUT_TARGETS
			IF IS_PED_BIT_SET(iPed, ePEDBITSET_TARGET_PED)
			AND NOT IS_DATA_BIT_SET(eDATABITSET_DISABLE_TAKEOUT_TARGET_UI)
				// url:bugstar:7467483 - Freemode Creator - Can we remove the crosshair blip and the bottom right Targets UI being set by default when the player chooses to set something as a Target in the creator.
				// New eDATABITSET for target blip not showing crosshair, but default blip
				RETURN RADAR_TRACE_TEMP_4 // Default Case
			ENDIF
		BREAK
		CASE eMODESTATE_PROTECT_TARGETS
			IF IS_PED_BIT_SET(iPed, ePEDBITSET_TARGET_PED)
				RETURN RADAR_TRACE_FRIEND
			ENDIF
		BREAK
		
		CASE eMODESTATE_COLLECT_PED_PICKUP
		CASE eMODESTATE_COLLECT_PED_SIGNAL
		CASE eMODESTATE_COLLECT_PED_WAIT
			IF GET_MISSION_ENTITY_FOR_COLLECT_PED(iPed) != -1
				RETURN RADAR_TRACE_FRIEND
			ENDIF
		BREAK
		
		#IF MAX_NUM_ARREST_PED_SUSPECTS
		CASE eMODESTATE_ARREST_PED
			RETURN RADAR_TRACE_PLAYERSTATE_ARRESTED
		#ENDIF
	ENDSWITCH
	
	RETURN RADAR_TRACE_INVALID
ENDFUNC

FUNC HUD_COLOURS GET_PED_BLIP_COLOUR(INT iPed)
	IF logic.Ped.Blip.Colour != null
		RETURN CALL logic.Ped.Blip.Colour(iPed)
	ENDIF
	
	SWITCH GET_CLIENT_MODE_STATE()
		CASE eMODESTATE_TAKE_OUT_TARGETS
			IF IS_PED_BIT_SET(iPed, ePEDBITSET_TARGET_PED)
				RETURN HUD_COLOUR_RED
			ENDIF
		BREAK
		CASE eMODESTATE_PROTECT_TARGETS
			IF IS_PED_BIT_SET(iPed, ePEDBITSET_TARGET_PED)
				RETURN HUD_COLOUR_BLUELIGHT
			ENDIF
		BREAK
		
		CASE eMODESTATE_COLLECT_PED_PICKUP
		CASE eMODESTATE_COLLECT_PED_SIGNAL
		CASE eMODESTATE_COLLECT_PED_WAIT
			IF GET_MISSION_ENTITY_FOR_COLLECT_PED(iPed) != -1
				RETURN HUD_COLOUR_BLUE
			ENDIF
		BREAK
	ENDSWITCH
	
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_I_AM_A_CIVILIAN)
		RETURN HUD_COLOUR_BLUE
	ENDIF
	
	RETURN HUD_COLOUR_RED
ENDFUNC

FUNC FLOAT GET_PED_BLIP_SCALE(INT iPed)
	IF logic.Ped.Blip.Scale != null
		RETURN CALL logic.Ped.Blip.Scale(iPed)
	ENDIF
	
	SWITCH GET_CLIENT_MODE_STATE()
		CASE eMODESTATE_TAKE_OUT_TARGETS
			IF IS_PED_BIT_SET(iPed, ePEDBITSET_TARGET_PED)
				RETURN 0.75
			ENDIF
		BREAK
	ENDSWITCH
	
	IF SHOULD_SHOW_PED_VISION_CONE(iPed)
		RETURN 0.5
	ENDIF
	
	RETURN 1.0
ENDFUNC

FUNC BOOL IS_PED_BLIP_SHORT_RANGE(INT iPed)
	IF logic.Ped.Blip.ShortRange != NULL
		RETURN CALL logic.Ped.Blip.ShortRange(iPed)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_PED_BLIP_DISTANCE_BASED_ALPHA_DETAILS(INT iPed, INT& iMinAlpha, FLOAT& fMaxBlipDist, FLOAT& fMinBlipDist)
	IF logic.Ped.Blip.DistanceAlphaData != NULL
		RETURN CALL logic.Ped.Blip.DistanceAlphaData(iPed, iMinAlpha, fMaxBlipDist, fMinBlipDist)
	ENDIF
	
	IF NOT IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_BLIP_FADE_BASED_ON_DISTANCE)
		RETURN FALSE
	ENDIF
	
	iMinAlpha = 100
	fMaxBlipDist = 5000
	fMinBlipDist = 400
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PED_BLIP_FLASH_ON_CREATION(INT iPed)

	IF logic.Ped.Blip.FlashOnCreation != null
		RETURN CALL logic.Ped.Blip.FlashOnCreation(iPed)
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_PED_BLIP_FLASH_ON_CHANGE(INT iPed)

	IF logic.Ped.Blip.FlashOnChange != null
		RETURN CALL logic.Ped.Blip.FlashOnChange(iPed)
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BLIP_PRIORITY GET_PED_BLIP_PRIORITY(INT iPed)
	IF logic.Ped.Blip.Priority != null
		RETURN CALL logic.Ped.Blip.Priority(iPed)
	ENDIF
	
	RETURN BLIP_PRIORITY_HIGHES_SPECIAL_MED
ENDFUNC

PROC MAINTAIN_PED_BLIP(INT iPed, PED_INDEX pedId, BOOL bDead)
	IF SHOULD_USE_GENERIC_ENEMY_PED_BLIP(iPed, bDead)
		IF NOT IS_BIT_SET(sPedLocal.sPed[iPed].sBlip.iStateBitset, BIT_AI_BLIP_STATE_CONFIGURED)
		AND DOES_BLIP_EXIST(sPedLocal.sPed[iPed].sBlip.BlipID)
			REMOVE_BLIP(sPedLocal.sPed[iPed].sBlip.BlipID)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] MAINTAIN_PED_BLIP - Removed unique blip for sPedLocal.sPed[", iPed, "] as generic enemy blip required - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId))
		ENDIF
		
		UPDATE_ENEMY_NET_PED_BLIP(serverBD.sPed[iPed].netId, sPedLocal.sPed[iPed].sBlip , GET_ENEMY_PED_BLIP_NOTICEABLE_RANGE(iPed), DEFAULT, DEFAULT, DEFAULT, GET_ENEMY_PED_NAME(iPed), BLIP_COLOUR_RED, DEFAULT, FALSE)
	ELSE
		IF bSafeToDisplay 
		AND NOT IS_BIT_SET(sPedLocal.sPed[iPed].sBlip.iStateBitset, BIT_AI_BLIP_STATE_CONFIGURED)
		AND SHOULD_BLIP_PED(iPed, bDead)
		
			HUD_COLOURS eBlipColour = GET_PED_BLIP_COLOUR(iPed)
			BLIP_SPRITE eBlipSprite = GET_PED_BLIP_SPRITE(iPed)
			BOOL bShortRange = IS_PED_BLIP_SHORT_RANGE(iPed)
			
			IF NOT DOES_BLIP_EXIST(sPedLocal.sPed[iPed].sBlip.BlipID)
				IF DOES_PED_HAVE_AI_BLIP(pedId)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] MAINTAIN_PED_BLIP - Ped was set to have an AI blip, which has not yet been configured or created. Cleaning it up.")
					CLEANUP_AI_PED_BLIP(sPedLocal.sPed[iPed].sBlip)
				ENDIF
				
				BOOL bShowHeight = logic.Ped.Blip.ShowHeight != null AND CALL logic.Ped.Blip.ShowHeight(iPed)
				
				ADD_ENTITY_BLIP(sPedLocal.sPed[iPed].sBlip.BlipID, pedId, eBlipSprite, eBlipColour, GET_ENEMY_PED_NAME(iPed), GET_PED_BLIP_SCALE(iPed), DEFAULT, bShortRange, SHOULD_PED_BLIP_FLASH_ON_CREATION(iPed), bShowHeight)
				SET_BLIP_PRIORITY(sPedLocal.sPed[iPed].sBlip.BlipID, GET_PED_BLIP_PRIORITY(iPed))
				
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] MAINTAIN_PED_BLIP - Added blip for sPedLocal.sPed[", iPed, "] - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId), " GET_ENEMY_PED_NAME(iPed) = ", GET_ENEMY_PED_NAME(iPed), " bShowHeight = ", bShowHeight)
			ELSE
				IF SHOULD_ROTATE_PED_BLIP(iPed)
					SET_BLIP_ROTATION(sPedLocal.sPed[iPed].sBlip.BlipID, ROUND(GET_ENTITY_HEADING_FROM_EULERS(pedId)))
				ENDIF
				
				IF SHOULD_SHOW_PED_VISION_CONE(iPed)
					SET_BLIP_SHOW_CONE(sPedLocal.sPed[iPed].sBlip.BlipID, TRUE)
				ENDIF
				
				IF eBlipSprite != RADAR_TRACE_INVALID
				AND eBlipSprite != GET_BLIP_SPRITE(sPedLocal.sPed[iPed].sBlip.BlipID)
					SET_BLIP_SPRITE(sPedLocal.sPed[iPed].sBlip.BlipID, eBlipSprite)
					SET_BLIP_SCALE(sPedLocal.sPed[iPed].sBlip.BlipID, GET_PED_BLIP_SCALE(iPed))
					SET_BLIP_NAME_FROM_TEXT_FILE(sPedLocal.sPed[iPed].sBlip.BlipID, GET_ENEMY_PED_NAME(iPed))
					
					IF SHOULD_PED_BLIP_FLASH_ON_CHANGE(iPed)
						FLASH_MISSION_BLIP(sPedLocal.sPed[iPed].sBlip.BlipID)
					ENDIF
					
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] MAINTAIN_PED_BLIP - Updated blip sprite for sPedLocal.sPed[", iPed, "] - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId))
				ENDIF
				
				IF GET_BLIP_COLOUR_FROM_HUD_COLOUR(eBlipColour) != GET_BLIP_COLOUR(sPedLocal.sPed[iPed].sBlip.BlipID)
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(sPedLocal.sPed[iPed].sBlip.BlipID, eBlipColour)
					SET_BLIP_SCALE(sPedLocal.sPed[iPed].sBlip.BlipID, GET_PED_BLIP_SCALE(iPed))
					
					IF SHOULD_PED_BLIP_FLASH_ON_CHANGE(iPed)
						FLASH_MISSION_BLIP(sPedLocal.sPed[iPed].sBlip.BlipID)
					ENDIF
					
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] MAINTAIN_PED_BLIP - Updated blip colour for sPedLocal.sPed[", iPed, "] - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId))
				ENDIF
				
				IF bShortRange != IS_BLIP_SHORT_RANGE(sPedLocal.sPed[iPed].sBlip.BlipID)
					SET_BLIP_AS_SHORT_RANGE(sPedLocal.sPed[iPed].sBlip.BlipID, bShortRange)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] MAINTAIN_PED_BLIP - Updated blip short range to ", GET_STRING_FROM_BOOL(bShortRange), " for sPedLocal.sPed[", iPed, "] - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId))
				ENDIF
				
				INT iMinAlpha
				FLOAT fMaxBlipDist
				FLOAT fMinBlipDist
				
				IF GET_PED_BLIP_DISTANCE_BASED_ALPHA_DETAILS(iPed, iMinAlpha, fMaxBlipDist, fMinBlipDist)
					SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(sPedLocal.sPed[iPed].sBlip.BlipID, iMinAlpha, fMaxBlipDist, fMinBlipDist)
				ENDIF
				
				IF logic.Ped.Blip.ConstantFlash != null
				AND CALL logic.Ped.Blip.ConstantFlash(iPed)
					IF NOT IS_BLIP_FLASHING(sPedLocal.sPed[iPed].sBlip.BlipID)
						FLASH_MISSION_BLIP(sPedLocal.sPed[iPed].sBlip.BlipID)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(sPedLocal.sPed[iPed].sBlip.iStateBitset, BIT_AI_BLIP_STATE_CONFIGURED)
				CLEANUP_AI_PED_BLIP(sPedLocal.sPed[iPed].sBlip)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] MAINTAIN_PED_BLIP - Removed generic enemy blip for sPedLocal.sPed[", iPed, "] - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId))
			ELIF DOES_PED_HAVE_AI_BLIP(pedId)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] MAINTAIN_PED_BLIP - Removed generic enemy blip, which was not yet created or configured, for sPedLocal.sPed[", iPed, "] - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId))
				CLEANUP_AI_PED_BLIP(sPedLocal.sPed[iPed].sBlip)
			ELIF DOES_BLIP_EXIST(sPedLocal.sPed[iPed].sBlip.BlipID)
				REMOVE_BLIP(sPedLocal.sPed[iPed].sBlip.BlipID)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] MAINTAIN_PED_BLIP - Removed blip for sPedLocal.sPed[", iPed, "] - SCRIPT_PED_", NETWORK_ENTITY_GET_OBJECT_ID(pedId))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_PED_GROUP(INT iPed)
	RETURN data.Ped.Peds[iPed].iGroup
ENDFUNC
FUNC INT GET_PED_GROUP_EVENT_DELAY(INT iPed)
	RETURN data.Ped.Group[data.Ped.Peds[iPed].iGroup].iEventDelay
ENDFUNC

FUNC BOOL HAS_PED_RECEIVED_ANY_EVENT(INT iPed, PED_INDEX pedId)

	UNUSED_PARAMETER(iPed)

	// too sensitive for stealth
//	IF HAS_PED_RECEIVED_EVENT(pedId, EVENT_SHOT_FIRED_BULLET_IMPACT)
//		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] [PED_EVENTS] HAS_PED_RECEIVED_ANY_EVENT - EVENT_SHOT_FIRED_BULLET_IMPACT")
//		RETURN TRUE
//	ENDIF
	
	IF HAS_PED_RECEIVED_EVENT(pedId, EVENT_SHOT_FIRED_WHIZZED_BY)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] [PED_EVENTS] HAS_PED_RECEIVED_ANY_EVENT - EVENT_SHOT_FIRED_WHIZZED_BY")
		RETURN TRUE
	ENDIF
	
	IF HAS_PED_RECEIVED_EVENT(pedId, EVENT_SHOCKING_DEAD_BODY)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] [PED_EVENTS] HAS_PED_RECEIVED_ANY_EVENT - EVENT_SHOCKING_DEAD_BODY")
		RETURN TRUE
	ENDIF
	
	IF HAS_PED_RECEIVED_EVENT(pedId, EVENT_SHOCKING_INJURED_PED)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] [PED_EVENTS] HAS_PED_RECEIVED_ANY_EVENT - EVENT_SHOCKING_INJURED_PED")
		RETURN TRUE
	ENDIF
	
	IF HAS_PED_RECEIVED_EVENT(pedId, EVENT_SHOCKING_SEEN_PED_KILLED)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] [PED_EVENTS] HAS_PED_RECEIVED_ANY_EVENT - EVENT_SHOCKING_SEEN_PED_KILLED")
		RETURN TRUE
	ENDIF
	
	IF HAS_PED_RECEIVED_EVENT(pedId, EVENT_SHOCKING_PED_KNOCKED_INTO_BY_PLAYER)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] [PED_EVENTS] HAS_PED_RECEIVED_ANY_EVENT - EVENT_SHOCKING_PED_KNOCKED_INTO_BY_PLAYER")
		RETURN TRUE
	ENDIF
	
	IF HAS_PED_RECEIVED_EVENT(pedId, EVENT_SHOT_FIRED)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] [PED_EVENTS] HAS_PED_RECEIVED_ANY_EVENT - EVENT_SHOT_FIRED")
		RETURN TRUE
	ENDIF

	IF HAS_PED_RECEIVED_EVENT(pedId, EVENT_EXPLOSION_HEARD)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] [PED_EVENTS] HAS_PED_RECEIVED_ANY_EVENT - EVENT_EXPLOSION_HEARD")
		RETURN TRUE
	ENDIF	

	RETURN FALSE

ENDFUNC

// Loop through the groups to see if any timers have expired and all peds are not dead
// Timers are only started when a ped in a group receives an event
// This is used for setting off peds in groups and allowing delays for ped events coming in, so that stealth takedowns are possible
// E.g. Take out a member of group 0, there's a 1 second delay. If after 1 second they're all dead, then no reaction will take place. If any are still alive, peds react
FUNC BOOL SHOULD_PED_GROUP_REACT_TO_EVENT(INT iPed)

	IF HAS_NET_TIMER_EXPIRED(sPedGroupLocal[data.Ped.Peds[iPed].iGroup].stEventTimer, GET_PED_GROUP_EVENT_DELAY(iPed))
		IF NOT ARE_ALL_PEDS_IN_GROUP_DEAD(GET_PED_GROUP(iPed))
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] [PED_EVENTS] SHOULD_PED_GROUP_REACT_TO_EVENT - All peds in group #", GET_PED_GROUP(iPed), " are not dead after ", GET_PED_GROUP_EVENT_DELAY(iPed), "ms receiving an event.")
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

// Arbitrary delay from script start before beginning certain processing
FUNC BOOL HAS_SCRIPT_START_DELAY_EXPIRED()
	RETURN GET_CLOUD_TIME_AS_INT() > (serverBD.iLaunchPosix + 2)
ENDFUNC

PROC PROCESS_PED_EVENTS(INT iPed, PED_INDEX pedId, BOOL bDelayExpired)

	IF NOT bDelayExpired
	OR IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_DISABLE_PED_EVENTS)
		EXIT
	ENDIF

	IF NOT HAS_NET_TIMER_STARTED(sPedGroupLocal[data.Ped.Peds[iPed].iGroup].stEventTimer)
		IF HAS_PED_RECEIVED_ANY_EVENT(iPed, pedId)
			START_NET_TIMER(sPedGroupLocal[data.Ped.Peds[iPed].iGroup].stEventTimer)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] [PED_EVENTS] PROCESS_PED_EVENTS - Ped in group ", data.Ped.Peds[iPed].iGroup,". Processing event in ", GET_PED_GROUP_EVENT_DELAY(iPed),"ms if rest of group are not dead.")
		ENDIF
	ELSE
		IF SHOULD_PED_GROUP_REACT_TO_EVENT(iPed)
			SET_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_I_HAVE_RECEIVED_PED_EVENT)
		ENDIF
	ENDIF
	
ENDPROC

FUNC INT GET_VISION_ALERT_TIME(INT iPed)
	BOOL bWeaponOut = DOES_PLAYER_HAVE_ANY_WEAPON_EQUIPPED()
	
	IF logic.Ped.VisionAlertTime != null
		RETURN CALL logic.Ped.VisionAlertTime(iPed, bWeaponOut)
	ENDIF
	
	IF IS_LOCAL_PLAYER_WEARING_DISGUISE()
	AND NOT bWeaponOut
		RETURN 8000
	ENDIF
	
	RETURN 1000
ENDFUNC

PROC MAINTAIN_PED_VISION(INT iPed, PED_INDEX pedId, BOOL& bInVisionCone)

	IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_ENABLE_VISION)
	AND NOT SHOULD_SHOW_PED_VISION_CONE(iPed)
		EXIT
	ENDIF
	
	IF HAS_PED_PERCEIVED_PLAYER(iPed, pedId)
		bInVisionCone = TRUE
		IF NOT HAS_NET_TIMER_STARTED(sPedLocal.stVisionTimer)
			START_NET_TIMER(sPedLocal.stVisionTimer)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] MAINTAIN_PED_VISION - Local player has entered ped's vision.")
			
			IF logic.Ped.DoWarningSpeech != null
				CALL logic.Ped.DoWarningSpeech(iPed, pedId)
			ENDIF
		ELIF HAS_NET_TIMER_EXPIRED(sPedLocal.stVisionTimer, GET_VISION_ALERT_TIME(iPed))
			SET_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_I_HAVE_SEEN_ENEMY_PLAYER)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] MAINTAIN_PED_VISION - Local player has been spotted by ped after ", GET_VISION_ALERT_TIME(iPed), "ms.")	
			
			
			IF logic.Ped.OnSeenPlayer != null
				CALL logic.Ped.OnSeenPlayer(iPed, pedId)
			ENDIF
		ENDIF
	ENDIF		
ENDPROC

#IF MAX_NUM_AMBIENT_SPEECH_PEDS
FUNC INT GET_AMBIENT_SPEECH_COLLECTION_DELAY_MS(AMBIENT_SPEECH_COLLECTION eCollection)
	IF logic.Ped.AmbientSpeech.CollectionDelay != NULL
		RETURN CALL logic.Ped.AmbientSpeech.CollectionDelay(eCollection)
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC BOOL SHOULD_AMBIENT_SPEECH_COLLECTION_REPEAT(AMBIENT_SPEECH_COLLECTION eCollection)
	IF logic.Ped.AmbientSpeech.ShouldCollectionRepeat != NULL
		RETURN CALL logic.Ped.AmbientSpeech.ShouldCollectionRepeat(eCollection)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC AMBIENT_SPEECH_CONTEXT GET_AMBIENT_SPEECH_CONTEXT(INT iPed, AMBIENT_SPEECH_COLLECTION eCollection)
	IF logic.Ped.AmbientSpeech.Context != NULL
		RETURN CALL logic.Ped.AmbientSpeech.Context(iPed, eCollection)
	ENDIF
	
	RETURN eAMBIENTSPEECHCONTEXT_END
ENDFUNC

//MAINTAIN_AMBIENT_SPEECH_SERVER is responsible for resetting the current collection once it has played, potentially after a delay specified by GET_eAMBIENT_SPEECH_COLLECTION_DELAY_MS.
//Further collections will not play until the current collection has been reset.
//If SHOULD_AMBIENT_SPEECH_COLLECTION_REPEAT returns FALSE for the collection, the "done" status for the collection will not be cleared automatically, preventing it repeating.
PROC MAINTAIN_AMBIENT_SPEECH_SERVER(INT iPed)
	IF NOT DOES_PED_HAVE_AMBIENT_SPEECH(iPed)
		EXIT
	ENDIF
	
	AMBIENT_SPEECH_COLLECTION eCollection = GET_AMBIENT_SPEECH_COLLECTION(iPed)
	
	SWITCH GET_AMBIENT_SPEECH_STATE(iPed)
		CASE eAMBIENTSPEECHSTATE_WAITING
			IF eCollection != eAMBIENTSPEECHCOLLECTION_END
				SET_AMBIENT_SPEECH_STATE(iPed, eAMBIENTSPEECHSTATE_SPEAKING)
			ENDIF
		BREAK
		
		CASE eAMBIENTSPEECHSTATE_SPEAKING
			IF IS_AMBIENT_SPEECH_COLLECTION_DONE_LOCAL_BIT_SET(iPed, eCollection)
				//Set the delay timer to the time in the future when this ped should next be able to speak. This enables the use of potentially varying delays, without having to keep track of the expiry time.
				SET_NET_TIMER_TO_SAME_VALUE_AS_TIMER(serverBD.sPed[iPed].stAmbientSpeechDelay, GET_TIME_OFFSET(GET_NETWORK_TIME(), GET_AMBIENT_SPEECH_COLLECTION_DELAY_MS(eCollection)))
				SET_AMBIENT_SPEECH_STATE(iPed, eAMBIENTSPEECHSTATE_DONE)
			ENDIF
		BREAK
		
		CASE eAMBIENTSPEECHSTATE_DONE
			IF GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.sPed[iPed].stAmbientSpeechDelay) > 0
				RESET_NET_TIMER(serverBD.sPed[iPed].stAmbientSpeechDelay)
				
				IF SHOULD_AMBIENT_SPEECH_COLLECTION_REPEAT(eCollection)
					CLEAR_DONE_AMBIENT_SPEECH(iPed, eCollection)
				ENDIF
				
				SET_AMBIENT_SPEECH_COLLECTION(iPed, eAMBIENTSPEECHCOLLECTION_END)
				SET_AMBIENT_SPEECH_STATE(iPed, eAMBIENTSPEECHSTATE_WAITING)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC SPEECH_PARAMS GET_AMBIENT_SPEECH_PARAMS(AMBIENT_SPEECH_COLLECTION eCollection)
	IF logic.Ped.AmbientSpeech.SpeechParams != NULL
		RETURN CALL logic.Ped.AmbientSpeech.SpeechParams(eCollection)
	ENDIF
	RETURN SPEECH_PARAMS_FORCE
ENDFUNC

//MAINTAIN_AMBIENT_SPEECH_CLIENT mainly ensures that one client actually triggers the speech using PLAY_PED_AMBIENT_SPEECH.
PROC MAINTAIN_AMBIENT_SPEECH_CLIENT(INT iPed, PED_INDEX pedID)
	IF NOT DOES_PED_HAVE_AMBIENT_SPEECH(iPed)
		EXIT
	ENDIF
	
	//Check whether this ped needs to play any speech.
	IF GET_AMBIENT_SPEECH_STATE(iPed) = eAMBIENTSPEECHSTATE_SPEAKING
		AMBIENT_SPEECH_COLLECTION eCollection = GET_AMBIENT_SPEECH_COLLECTION(iPed) //Get the current collection specified by the server.
		
		IF eCollection != eAMBIENTSPEECHCOLLECTION_END
		AND NOT IS_AMBIENT_SPEECH_COLLECTION_DONE_LOCAL_BIT_SET(iPed, eCollection)
			AMBIENT_SPEECH_CONTEXT eSpeech = GET_AMBIENT_SPEECH_CONTEXT(iPed, eCollection) //Get an appropriate context for the collection.
			
			//If a collection has been specified, but has not yet been played and has a valid speech context, play it.
			IF eSpeech != eAMBIENTSPEECHCONTEXT_END
				IF NOT IS_ANY_SPEECH_PLAYING(pedID)
					IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
						TEXT_LABEL_63 tl63Context = GET_AMBIENT_SPEECH_CONTEXT_STRING(eSpeech)
						
						STOP_PED_SPEAKING(pedID, FALSE)
						PLAY_PED_AMBIENT_SPEECH(pedID, tl63Context, GET_AMBIENT_SPEECH_PARAMS(eCollection))
						
						IF NOT IS_AMBIENT_SPEECH_DATA_BIT_SET(data.Ped.Peds[iPed].iAmbientSpeechIndex, eAMBIENTSPEECHDATABITSET_DISABLE_STOP_PED_SPEAKING)
							STOP_PED_SPEAKING(pedID, TRUE)
						ENDIF
						
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBIENT_SPEECH] [PED_", iPed, "] MAINTAIN_AMBIENT_SPEECH_CLIENT - iPed #", iPed, " played tl63Context = ", tl63Context, ", eSpeech = ", GET_AMBIENT_SPEECH_CONTEXT_NAME(eSpeech), ", eCollection = ", GET_AMBIENT_SPEECH_COLLECTION_NAME(eCollection))
						
						NOTIFY_DONE_AMBIENT_SPEECH(iPed, eCollection) //Notify all participants that this client has played a context for the collection.
						SET_AMBIENT_SPEECH_COLLECTION_DONE_LOCAL_BIT(iPed, eCollection)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL SHOULD_DRAW_PED_TARGET_ARROW(INT iPed)
	IF logic.TakeOutTarget.DrawPedArrow != null
		RETURN CALL logic.TakeOutTarget.DrawPedArrow(iPed)
	ENDIF
	
	// url:bugstar:7467483 - Freemode Creator - Can we remove the crosshair blip and the bottom right Targets UI being set by default when the player chooses to set something as a Target in the creator.
	// New eDATABITSET for target arrow to be disabled by default
	IF GET_CLIENT_MODE_STATE() = eMODESTATE_TAKE_OUT_TARGETS AND IS_DATA_BIT_SET(eDATABITSET_DISABLE_TAKEOUT_TARGET_UI)
		RETURN FALSE
	ENDIF
	
	RETURN GET_CLIENT_MODE_STATE() = eMODESTATE_TAKE_OUT_TARGETS 
		OR GET_CLIENT_MODE_STATE() = eMODESTATE_PROTECT_TARGETS
ENDFUNC

FUNC BOOL SHOULD_DRAW_PED_MARKER(INT iPed, PED_INDEX pedId)
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_I_AM_MISSION_CRITICAL_PED)
	AND NOT IS_PED_LOCAL_BIT_SET(iPed, ePEDLOCALBITSET_IN_ANY_VEHICLE)
	AND NOT IS_PED_BIT_SET(iPed, ePEDBITSET_DO_NOT_DRAW_MISSION_CRITICAL_MARKER)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_TARGET_PED)
	AND SHOULD_DRAW_PED_TARGET_ARROW(iPed)
		RETURN TRUE
	ENDIF
	
	IF logic.Ped.Blip.DrawMarker != NULL
		RETURN CALL logic.Ped.Blip.DrawMarker(iPed, pedId)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC HUD_COLOURS GET_PED_MARKER_COLOUR(INT iPed)
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_I_AM_MISSION_CRITICAL_PED)
		RETURN HUD_COLOUR_BLUE
	ENDIF
	
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_TARGET_PED)
		RETURN HUD_COLOUR_RED
	ENDIF
	
	RETURN GET_PED_BLIP_COLOUR(iPed)
ENDFUNC

FUNC FLOAT GET_PED_MARKER_SCALE(INT iPed)
	IF logic.Ped.Blip.MarkerScale != NULL
		RETURN CALL logic.Ped.Blip.MarkerScale(iPed)
	ENDIF
	
	RETURN 0.5
ENDFUNC

PROC MAINTAIN_PED_MARKER(INT iPed, PED_INDEX pedId)
	IF SHOULD_DRAW_PED_MARKER(iPed, pedId)
		DRAW_GENERIC_ENTITY_MARKER(eENTITYTYPE_PED, iPed, pedId, GET_PED_MARKER_COLOUR(iPed), GET_PED_MARKER_SCALE(iPed))
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PED_FADE_OUT(INT iPed)
	IF logic.Ped.FadeOut.ShouldFade != NULL
		RETURN CALL logic.Ped.FadeOut.ShouldFade(iPed)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_PED_FADING_OUT_CLIENT(INT iPed, PED_INDEX pedID)
	IF IS_ENTITY_VISIBLE_TO_SCRIPT(pedID)
	AND NOT NETWORK_IS_ENTITY_FADING(pedID)
	AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
		IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_CLEAR_PED_TASKS_IMMEDIATELY_ON_FADE_OUT)
			CLEAR_PED_TASKS_IMMEDIATELY(pedID)
			SET_PED_KEEP_TASK(pedID, FALSE)
		ENDIF
		
		SET_ENTITY_INVINCIBLE(pedID, TRUE)
		NETWORK_FADE_OUT_ENTITY(pedID, TRUE, TRUE)
		
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] MAINTAIN_PED_FADING_OUT_CLIENT - Fading out iPed #", iPed)
	ENDIF
ENDPROC

PROC SET_PED_FRIENDLY(INT iPed, PED_INDEX pedID)

	IF NOT IS_PED_CLIENT_BIT_SET(iPed, LOCAL_PARTICIPANT_INDEX, ePEDCLIENTBITSET_SET_AS_FRIENDLY)
		#IF MAX_NUM_RELATIONSHIP_GROUPS
		INT iGroup = data.Ped.Group[data.Ped.Peds[iPed].iGroup].Relationship.iCustomGroup
		IF iGroup != -1
			RELATIONSHIP_TYPE relType = CALL logic.Relationship.ToPlayer(iGroup, LOCAL_PLAYER_INDEX)
			IF relType = ACQUAINTANCE_TYPE_PED_LIKE
				SET_PED_TREATED_AS_FRIENDLY(pedID, TRUE, TRUE)
				SET_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_SET_AS_FRIENDLY)
				EXIT
			ENDIF
		ENDIF
		#ENDIF
		
		IF data.Ped.Group[data.Ped.Peds[iPed].iGroup].Relationship.eGroup = rgFM_AiLike
		OR data.Ped.Group[data.Ped.Peds[iPed].iGroup].Relationship.eGroup = rgFM_AiLike_HateAiHate
			SET_PED_TREATED_AS_FRIENDLY(pedID, TRUE, TRUE)
			SET_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_SET_AS_FRIENDLY)
		ENDIF
	ENDIF

ENDPROC

PROC MAINTAIN_PED_FLAGS(INT iPed, PED_INDEX pedID)

	// Set liked peds as friendly
	SET_PED_FRIENDLY(iPed, pedID)

ENDPROC

FUNC BOOL SHOULD_PED_BE_DELETED_ON_CLEANUP(INT iPed)
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_DELETE_ON_CLEANUP)
		RETURN TRUE
	ENDIF
	
	IF logic.Ped.ShouldDeleteOnCleanup != NULL
	AND CALL logic.Ped.ShouldDeleteOnCleanup(iPed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DELETE_PED_ON_END(INT iPed)
	IF SHOULD_PED_BE_DELETED_ON_CLEANUP(iPed)
		RETURN TRUE
	ENDIF
	
	#IF MAX_NUM_MISSION_INTERIORS
	IF data.Ped.Peds[iPed].iInterior != (-1)
	AND IS_FMC_INTERIOR_A_WARP_INTERIOR(data.Interior.eInteriors[data.Ped.Peds[iPed].iInterior].eType)
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_PEDS()
	
	INT i
	
	REPEAT data.Ped.iCount i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[i].netId)
			PED_INDEX pedID = NET_TO_PED(serverBD.sPed[i].netId)
			IF NOT IS_ENTITY_DEAD(pedID)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[i].netId)
					IF GET_PED_TYPE(pedID) = PEDTYPE_COP
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedID, FALSE)
						IF IS_PED_USING_ANY_SCENARIO(pedID)
							SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedID)
						ENDIF
						CLEAR_PED_TASKS(pedID)
						SET_PED_KEEP_TASK(pedID, FALSE)	
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", i, "] CLEANUP_PEDS set MissionEntity iPed #", i, " to no longer keep task and to not block temp events - they are a pedtype_cop.")
					ENDIF
				ENDIF
				REMOVE_PED_FROM_GROUP(pedID)
			ENDIF

			IF logic.Ped.OnCleanup != NULL
				CALL logic.Ped.OnCleanup(i)
			ENDIF
			
			IF SHOULD_DELETE_PED_ON_END(i)
				IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.sPed[i].netId)
					DELETE_NET_ID(serverBD.sPed[i].netId)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", i, "] CLEANUP_PEDS - Deleted ped.")
				ENDIF
			ELSE
				CLEANUP_NET_ID(serverBD.sPed[i].netId)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", i, "] CLEANUP_PEDS set MissionEntity iPed #", i, " as no longer needed.")
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

PROC GET_PED_STATUS(INT iPed, BOOL &bExists, BOOL &bDead, PED_INDEX &pedID, ePED_STATE &eState)

	eState = GET_PED_STATE(iPed)
	bExists = NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iPed].netId)
	IF bExists
		pedID = NET_TO_PED(serverBD.sPed[iPed].netId)
		bDead = IS_PED_INJURED(pedID)
	ENDIF

ENDPROC

#IF MAX_NUM_MISSION_INTERIORS
PROC PROCESS_DELETE_INTERIOR_RESPAWN_PED_CHECKS(INT iPed)
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_RESPAWN_PED)
	AND data.Ped.Peds[iPed].iInterior != (-1)
	AND IS_FMC_INTERIOR_A_WARP_INTERIOR(data.Interior.eInteriors[data.Ped.Peds[iPed].iInterior].eType)
	AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
		DELETE_NET_ID(serverBD.sPed[iPed].netId)
		PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] PROCESS_DELETE_INTERIOR_RESPAWN_PED_CHECKS - Deleted interior respawn ped before respawn.")
	ENDIF
ENDPROC
#ENDIF

PROC MAINTAIN_PED_CLIENT()

	// Indexes to use throughout function
	PED_INDEX pedID
//	VEHICLE_INDEX vehID

	ePED_STATE eState
	
	// Flags to use throughout function
	BOOL bPedExists
	BOOL bDead
	BOOL bInVisionCone
	
//	BOOL bInVehicle
//	BOOL bVehicleOk
	BOOL bDelayExpired = HAS_SCRIPT_START_DELAY_EXPIRED()
	FLOAT fRange2 = GET_TARGET_RANGE_FOR_REACTIONS()
	fRange2 *= fRange2
	
	///////////////////////////////////
	////     EVERY FRAME LOOP 	   ////
	/////////////////////////////////// 
	
	INT iPed
	REPEAT data.Ped.iCount iPed
	
		// Set up all flags
		bPedExists = FALSE
		bDead = FALSE
	//	bInVehicle = FALSE
	//	bVehicleOk = FALSE
	
		GET_PED_STATUS(iPed, bPedExists, bDead, pedID, eState)
	
		IF bPedExists
			IF NOT bDead
				IF IS_PED_IN_ANY_VEHICLE(pedID, TRUE)
					SET_PED_LOCAL_BIT(iPed, ePEDLOCALBITSET_ENTERING_ANY_VEHICLE)
				ELSE
					CLEAR_PED_LOCAL_BIT(iPed, ePEDLOCALBITSET_ENTERING_ANY_VEHICLE)
				ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(pedID)
					SET_PED_LOCAL_BIT(iPed, ePEDLOCALBITSET_IN_ANY_VEHICLE)
				ELSE
					CLEAR_PED_LOCAL_BIT(iPed, ePEDLOCALBITSET_IN_ANY_VEHICLE)
				ENDIF
			ENDIF

			#IF IS_DEBUG_BUILD
			MAINTAIN_PED_DEBUG_CLIENT(iPed, pedID, bDead)
			#ENDIF
			
			IF logic.Ped.Client != null
				CALL logic.Ped.Client(iPed, pedID, bDead)
			ENDIF
			
			MAINTAIN_PED_BLIP(iPed, pedID, bDead)
		ENDIF
		
		SWITCH eState
			
			CASE ePEDSTATE_WAITING_FOR_RESPAWN
				IF bPedExists
					#IF MAX_NUM_MISSION_INTERIORS	PROCESS_DELETE_INTERIOR_RESPAWN_PED_CHECKS(iPed)	#ENDIF
				ENDIF
			BREAK
			
			CASE ePEDSTATE_ACTIVE
				IF bPedExists	
					IF NOT bDead
						IF IS_PED_BIT_SET(iPed, ePEDBITSET_TARGET_PED)
							INCREMENT_TAKE_OUT_TARGETS_REMAINING()
						ENDIF
						
						#IF MAX_NUM_AMBIENT_SPEECH_PEDS		MAINTAIN_AMBIENT_SPEECH_CLIENT(iPed, pedID)		#ENDIF
						
						MAINTAIN_PED_MARKER(iPed, pedID)
						MAINTAIN_PED_FLAGS(iPed, pedID)
						PROCESS_CURRENT_PED_TASK_CLIENT(iPed, pedID, TRUE)
						
						IF NOT HAS_PED_BEEN_TRIGGERED(iPed)  
							PROCESS_PED_EVENTS(iPed, pedID, bDelayExpired)
							MAINTAIN_PED_VISION(iPed, pedID, bInVisionCone)
							MAINTAIN_HAS_PLAYER_TARGETTED_PED(iPed, pedID, fRange2)	// TODO: Would like to try and replace this with EVENT_GUN_AIMED_AT in PROCESS_PED_EVENTS if possible
							MAINTAIN_PED_KNOCKED_INTO_BY_PLAYER(iPed, pedID)
						ENDIF
						MAINTAIN_IS_PLAYER_NEAR_PED(iPed, pedID)
						MAINTAIN_PED_COMBAT_TARGET(iPed, pedID)
					ENDIF
				ENDIF
			BREAK
			
			CASE ePEDSTATE_FADING_OUT
				MAINTAIN_PED_FADING_OUT_CLIENT(iPed, pedID)
			BREAK
			
			CASE ePEDSTATE_DEAD
				IF bPedExists	
					IF NOT bDead
					
						#IF MAX_NUM_AMBUSH_VEHICLES
						IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
							GENERIC_TASK_FLEE_FROM_COORD(iPed, pedID)
						ENDIF
						#ENDIF
						
					ENDIF
				ENDIF
				
				IF NOT IS_DATA_BIT_SET(eDATABITSET_DROP_MISSION_ENTITIES_ON_CARRIER_DEATH)
					DESTROY_ALL_MISSION_ENTITIES_IN_CARRIER(iPed, ET_PED)
				ENDIF
			BREAK
			
			CASE ePEDSTATE_NO_LONGER_NEEDED
				IF bPedExists
					IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_DELETE_ON_NO_LONGER_NEEDED)
					AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sPed[iPed].netId)
						DELETE_NET_ID(serverBD.sPed[iPed].netId)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	
	ENDREPEAT

	IF NOT bInVisionCone
		IF HAS_NET_TIMER_STARTED(sPedLocal.stVisionTimer)
			RESET_NET_TIMER(sPedLocal.stVisionTimer)
		ENDIF
	ENDIF
	
	// Staggered loop
	iPed = sPedLocal.iStagger
	
	GET_PED_STATUS(iPed, bPedExists, bDead, pedID, eState)
	
	SWITCH eState
		CASE ePEDSTATE_ACTIVE
			IF bPedExists	
				IF NOT bDead
					PROCESS_CURRENT_PED_TASK_CLIENT(iPed, pedID, FALSE)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH	
	
ENDPROC

FUNC BOOL SHOULD_CLEANUP_PED(INT iPed, PED_INDEX pedID)
	UNUSED_PARAMETER(iPed)
	UNUSED_PARAMETER(pedID)
	
	#IF MAX_NUM_AMBUSH_VEHICLES
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		INT iAmbush = GET_AMBUSH_INDEX_FROM_ENTITY(eENTITYTYPE_PED, iPed)
		IF iAmbush != (-1)
			IF serverBD.sAmbush.sVehicle[iAmbush].iDriverIndex != (-1)
			AND GET_PED_STATE(serverBD.sAmbush.sVehicle[iAmbush].iDriverIndex) = ePEDSTATE_DEAD
			AND serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromTarget >= 299.0
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] [AMBUSH_", iAmbush,"] Cleaned up due to range from target while driver is dead. Distance: ", serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromTarget, " - Range: 299.0")
				RETURN TRUE
			ENDIF
			
			#IF MAX_NUM_AMBUSH_VEHICLES_VARIATIONS
			// url:bugstar:7467758 - Freemode Creator: Could we add the option to customise the waves to not use identical vehicle setups for every ambush?
			// ambush vehicle variations behaviour
			INT iAmbushVar
			REPEAT MAX_NUM_AMBUSH_VEHICLES_VARIATIONS iAmbushVar
				IF serverBD.sAmbush.sVehicle[iAmbush].iVariationDriverIndexList[iAmbushVar] != (-1)
				AND GET_PED_STATE(serverBD.sAmbush.sVehicle[iAmbush].iVariationDriverIndexList[iAmbushVar]) = ePEDSTATE_DEAD
				AND serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromTarget >= 299.0
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] [AMBUSH_", iAmbush,"_V", iAmbushVar,"] Cleaned up due to range from target while driver is dead. Distance: ", serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromTarget, " - Range: 299.0")
					RETURN TRUE
				ENDIF
			ENDREPEAT
			#ENDIF
		
			IF serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromTarget != INVALID_DISTANCE
			AND serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromTarget > GET_AMBUSH_CLEANUP_RANGE()
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] [AMBUSH_", iAmbush,"] Cleaned up due to range from target. Distance: ", serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromTarget, " - Range: ", GET_AMBUSH_CLEANUP_RANGE())
				RETURN TRUE
			ENDIF
			
			IF serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromDropoff != INVALID_DISTANCE
			AND serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromDropoff < GET_AMBUSH_DROPOFF_DISMISS_RANGE()
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] [AMBUSH_", iAmbush,"] Cleaned up due to distance to dropoff. Distance: ", serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromDropoff, " - Range: ", GET_AMBUSH_DROPOFF_DISMISS_RANGE())
				RETURN TRUE
			ENDIF
			
			IF SHOULD_CLEANUP_AMBUSH_FOR_INVALID_TARGET(iAmbush)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] [AMBUSH_", iAmbush,"] Cleaned up due to target no longer valid.")
				RETURN TRUE
			ENDIF
			
			IF SHOULD_FORCE_CLEANUP_AMBUSH()
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] [AMBUSH_", iAmbush,"] Cleaned up due to logic.Ambush.ForceCleanup()")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PED_RESPAWN_DELAY_PASSED(INT iPed)
	UNUSED_PARAMETER(iPed)
	
	#IF MAX_NUM_AMBUSH_VEHICLES
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		IF NOT HAS_NET_TIMER_STARTED_AND_EXPIRED(serverBD.sAmbush.stSpawnTimer, GET_AMBUSH_RESPAWN_DELAY())
			RETURN FALSE
		ENDIF
	ENDIF
	#ENDIF
	
	IF logic.Ped.Respawn.Delay != NULL
		INT iDelay = CALL logic.Ped.Respawn.Delay(iPed)
		IF iDelay != -1
			IF NOT HAS_NET_TIMER_EXPIRED(serverBD.sPed[iPed].stRespawnTimer, iDelay)
				RETURN FALSE
			ELSE
				RESET_NET_TIMER(serverBD.sPed[iPed].stRespawnTimer)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_RESPAWN_PED(INT iPed)
	IF GET_END_REASON() != eENDREASON_NO_REASON_YET
		#IF IS_DEBUG_BUILD		IF sDebugVars.bDoPedPrints		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] SHOULD_RESPAWN_PED = FALSE - GET_END_REASON() != eENDREASON_NO_REASON_YET")	ENDIF	#ENDIF
		RETURN FALSE
	ENDIF
	
	#IF MAX_NUM_AMBUSH_VEHICLES
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		RETURN SHOULD_RESPAWN_AMBUSH_VEHICLE(data.Ped.Peds[iPed].iVehicle)
	ENDIF
	#ENDIF
	
	IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_RESPAWN_PED)
		RETURN CALL logic.Ped.Respawn.Enable(iPed)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC PROCESS_PED_ON_RESPAWN(INT iPed)
	// Reset tasks for respawning so they don't stay in the last task before death
	SET_PED_TASK(iPed, 0)

	IF logic.Ped.Respawn.OnRespawn != NULL
		CALL logic.Ped.Respawn.OnRespawn(iPed)
	ENDIF
ENDPROC

FUNC BOOL MAINTAIN_PED_FORCE_CLEANUP(INT iPed, PED_INDEX pedID)
	IF logic.Ped.ForceCleanup != NULL
	AND CALL logic.Ped.ForceCleanup(iPed, pedID)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] MAINTAIN_PED_FORCE_CLEANUP - logic.Ped.ForceCleanup = TRUE.")
	
		IF logic.Ped.OnCleanup != NULL
			CALL logic.Ped.OnCleanup(iPed)
		ENDIF
	
		IF SHOULD_PED_BE_DELETED_ON_CLEANUP(iPed)
			DELETE_NET_ID(serverBD.sPed[iPed].netId)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] MAINTAIN_PED_FORCE_CLEANUP - CLEANUP_NET_ID.")
		ELSE
			CLEANUP_NET_ID(serverBD.sPed[iPed].netId)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] MAINTAIN_PED_FORCE_CLEANUP - CLEANUP_NET_ID.")
		ENDIF
		
		RETURN TRUE
		
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CLEANUP_RESPAWN_PED(INT iPed)
	UNUSED_PARAMETER(iPed)
	
	#IF MAX_NUM_MISSION_INTERIORS
	IF data.Ped.Peds[iPed].iInterior != (-1)
	AND IS_FMC_INTERIOR_A_WARP_INTERIOR(data.Interior.eInteriors[data.Ped.Peds[iPed].iInterior].eType)
		RETURN FALSE
	ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_PED_SERVER()
	PED_INDEX pedID
	BOOL bCreatedThisFrame, bPedExists, bDead
	ePED_STATE eState
	
	#IF MAX_NUM_AMBUSH_VEHICLES
	INT iAmbushPedsDeadBitset
	SETUP_AMBUSH_PED_DEAD_TEMP_BITSET(iAmbushPedsDeadBitset)
	#ENDIF
	
	INT iPed
	REPEAT data.Ped.iCount iPed
	
		// Retrieve ped information
		GET_PED_STATUS(iPed, bPedExists, bDead, pedID, eState)

		IF bPedExists
			IF logic.Ped.Server != null
				CALL logic.Ped.Server(iPed, pedID, bDead)
			ENDIF
		ENDIF
		
		// Detect if the ped is dead and set state accordingly.
		IF eState > ePEDSTATE_CREATE
			IF eState != ePEDSTATE_DEAD
			AND eState != ePEDSTATE_FADING_OUT
				IF NOT bPedExists
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] Does not exist.")
					SET_PED_STATE(iPed, ePEDSTATE_DEAD)
				ELSE
					IF bDead
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iPed, "] Dead.")
						SET_PED_STATE(iPed, ePEDSTATE_DEAD)
					ELIF SHOULD_CLEANUP_PED(iPed, pedID)
						SET_PED_STATE(iPed, ePEDSTATE_DEAD)
					ELSE
						#IF MAX_NUM_AMBUSH_VEHICLES		CLEAR_TEMP_AMBUSH_PED_DEAD_BIT(iAmbushPedsDeadBitset, iPed)		#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			#IF MAX_NUM_AMBUSH_VEHICLES		CLEAR_TEMP_AMBUSH_PED_DEAD_BIT(iAmbushPedsDeadBitset, iPed)		#ENDIF
		ENDIF	
	
		SWITCH eState
		
			CASE ePEDSTATE_INACTIVE
				IF SHOULD_ACTIVATE_PED(iPed)
					SET_PED_STATE(iPed, ePEDSTATE_CREATE)
				ENDIF
			BREAK
			
			CASE ePEDSTATE_WAITING_FOR_RESPAWN
				IF NOT bPedExists
					IF HAS_PED_RESPAWN_DELAY_PASSED(iPed)
						PROCESS_PED_ON_RESPAWN(iPed)
						SET_PED_STATE(iPed, ePEDSTATE_CREATE)
					ENDIF
				ELIF SHOULD_CLEANUP_RESPAWN_PED(iPed)
					CLEANUP_NET_ID(serverBD.sPed[iPed].netId)
				ENDIF
			BREAK
		
			CASE ePEDSTATE_CREATE
				IF NOT bCreatedThisFrame
					IF CAN_PED_BE_CREATED(iPed)						
						IF CREATE_MISSION_PED(iPed)
							SET_PED_STATE(iPed, ePEDSTATE_ACTIVE)
							IF IS_PED_BIT_SET(iPed, ePEDBITSET_DIED_ONCE)
								SET_PED_BIT(iPed, ePEDBITSET_RESPAWNED)
							ENDIF
							SET_PED_DEBUG_NAME(iPed)
							bCreatedThisFrame = TRUE
						ENDIF
					ENDIF
				ENDIF
			BREAK	
			
			CASE ePEDSTATE_ACTIVE
				IF bPedExists	
					IF MAINTAIN_PED_FORCE_CLEANUP(iPed, pedID)
						SET_PED_STATE(iPed, ePEDSTATE_DEAD)
						RELOOP
					ENDIF
					
					IF NOT bDead
						PROCESS_CURRENT_PED_TRIGGERS(iPed)
						PROCESS_CURRENT_PED_TASK_SERVER(iPed, pedID, TRUE)
						#IF MAX_NUM_AMBIENT_SPEECH_PEDS		MAINTAIN_AMBIENT_SPEECH_SERVER(iPed)		#ENDIF
						
						IF SHOULD_PED_FADE_OUT(iPed)
							SET_PED_STATE(iPed, ePEDSTATE_FADING_OUT)
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE ePEDSTATE_DEAD
				SET_PED_BIT(iPed, ePEDBITSET_DIED_ONCE)
				
				IF bPedExists
					IF MAINTAIN_PED_FORCE_CLEANUP(iPed, pedID)
						RELOOP
					ENDIF
					
					IF SHOULD_PED_FADE_OUT(iPed)
						SET_PED_STATE(iPed, ePEDSTATE_FADING_OUT)
					ENDIF
				ENDIF
				
				#IF MAX_NUM_AMBUSH_VEHICLES
				IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
				AND SHOULD_FORCE_CLEANUP_AMBUSH()
					CLEANUP_NET_ID(serverBD.sPed[iPed].netId)
				ENDIF
				#ENDIF

				IF SHOULD_RESPAWN_PED(iPed)
					IF SHOULD_CLEANUP_RESPAWN_PED(iPed)
						CLEANUP_NET_ID(serverBD.sPed[iPed].netId)
					ENDIF
					SET_PED_STATE(iPed, ePEDSTATE_WAITING_FOR_RESPAWN)
				ENDIF
				
				IF GET_END_REASON() = eENDREASON_NO_REASON_YET
					IF IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_I_AM_MISSION_CRITICAL_PED)
						SET_END_REASON(eENDREASON_PEDS_KILLED)
					ENDIF
				ENDIF
				
			BREAK
			
			CASE ePEDSTATE_FADING_OUT
				IF NOT IS_PED_DATA_BIT_SET(iPed, ePEDDATABITSET_DISABLE_MOVE_TO_NO_LONGER_NEEDED_ON_FADED_OUT)
				AND NOT IS_ENTITY_VISIBLE_TO_SCRIPT(pedID)
					SET_PED_BIT(iPed, ePEDBITSET_FADED_OUT)
					SET_PED_STATE(iPed, ePEDSTATE_NO_LONGER_NEEDED)
				ENDIF
			BREAK
			
			CASE ePEDSTATE_NO_LONGER_NEEDED
			
			BREAK
		ENDSWITCH
	ENDREPEAT
	
	// Post-loop processing
	#IF MAX_NUM_AMBUSH_VEHICLES		MAINTAIN_AMBUSH_PEDS_DATA(iAmbushPedsDeadBitset)	#ENDIF
	
	// Staggered loop
	iPed = sPedLocal.iStagger
	
	GET_PED_STATUS(iPed, bPedExists, bDead, pedID, eState)
	
	SWITCH eState
		CASE ePEDSTATE_ACTIVE
			IF bPedExists	
				IF NOT bDead
					PROCESS_CURRENT_PED_TASK_SERVER(iPed, pedID, FALSE)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH	
	
ENDPROC

PROC PROCESS_EVENT_PED_SEEN_DEAD_PED(INT iCount)
	STRUCT_DEAD_PED_SEEN sDeadPedSeen
	
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_AI, iCount, sDeadPedSeen, SIZE_OF(sDeadPedSeen))
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_EVENT_PED_SEEN_DEAD_PED - NOT GET_EVENT_DATA()")
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(sDeadPedSeen.FindingPedId)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_EVENT_PED_SEEN_DEAD_PED - NOT DOES_ENTITY_EXIST(sDeadPedSeen.FindingPedId)")
		EXIT
	ENDIF
	
	IF IS_PED_INJURED(sDeadPedSeen.FindingPedId)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_EVENT_PED_SEEN_DEAD_PED - IS_PED_INJURED(sDeadPedSeen.FindingPedId)")
		EXIT
	ENDIF
	
	INT iFindingPed = -1
	INT iDeadPed = -1
	
	INT i
	REPEAT data.Ped.iCount i
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[i].netId)
			PED_INDEX pedID = NET_TO_PED(serverBD.sPed[i].netId)
			
			IF pedID = sDeadPedSeen.FindingPedId
				IF IS_PED_BIT_SET(i, ePEDBITSET_I_HAVE_SEEN_DEAD_PED)
				OR IS_PED_CLIENT_BIT_SET(i, LOCAL_PARTICIPANT_INDEX, ePEDCLIENTBITSET_I_HAVE_SEEN_DEAD_PED)
					BREAKLOOP
				ELSE
					iFindingPed = i
				ENDIF
			ELIF pedID = sDeadPedSeen.DeadPedId
				iDeadPed = i
			ENDIF
			
			IF iFindingPed != -1
			AND iDeadPed != -1
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PED_", iFindingPed, "] PROCESS_EVENT_PED_SEEN_DEAD_PED - iPed #", iFindingPed, " has seen a dead ped (iPed #", iDeadPed, ").")
				SET_PED_CLIENT_BIT(iFindingPed, ePEDCLIENTBITSET_I_HAVE_SEEN_DEAD_PED)
				
				BREAKLOOP
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
