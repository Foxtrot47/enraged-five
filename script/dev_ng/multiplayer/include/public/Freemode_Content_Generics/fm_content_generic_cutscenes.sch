USING "fm_content_generic_support.sch"
USING "cutscene_help.sch"

//----------------------
//	CUTSCENES
//----------------------

#IF MAX_NUM_CUTSCENES
FUNC BOOL SHOULD_ENABLE_CUTSCENE()
	IF logic.Cutscene.Enable != NULL
		RETURN CALL logic.Cutscene.Enable()
	ENDIF
	
	RETURN GET_CLIENT_MODE_STATE() = eMODESTATE_CUTSCENE
ENDFUNC

FUNC BOOL SHOULD_CUTSCENE_START(INT iScene)
	IF NOT IS_LOCAL_ALIVE
		RETURN FALSE
	ENDIF
	
	IF logic.Cutscene.ShouldStart != NULL
		RETURN CALL logic.Cutscene.ShouldStart(iScene)
	ENDIF
	
	RETURN GET_CURRENT_CUTSCENE(LOCAL_PARTICIPANT_INDEX) = -1
ENDFUNC

FUNC BOOL SHOULD_CUTSCENE_LOAD_ASSETS(INT iScene)
	IF logic.Cutscene.ShouldLoadAssets != NULL
		RETURN CALL logic.Cutscene.ShouldLoadAssets(iScene)
	ENDIF
	
	RETURN SHOULD_CUTSCENE_START(iScene)
ENDFUNC

FUNC VECTOR GET_CUTSCENE_COORDS(INT iScene)
	IF logic.Cutscene.Coords != NULL
		RETURN CALL logic.Cutscene.Coords(iScene)
	ENDIF
	
	RETURN data.Cutscene.Scenes[iScene].vCoords
ENDFUNC

FUNC FLOAT GET_CUTSCENE_HEADING(INT iScene)
	IF logic.Cutscene.Heading != NULL
		RETURN CALL logic.Cutscene.Heading(iScene)
	ENDIF
	
	RETURN data.Cutscene.Scenes[iScene].fHeading
ENDFUNC

#IF MAX_NUM_CUTSCENE_CAMERAS
FUNC VECTOR GET_CUTSCENE_CAMERA_COORD(INT iScene, INT iCamera)
	IF logic.Cutscene.Camera.Coords != NULL
		RETURN CALL logic.Cutscene.Camera.Coords(iScene, iCamera)
	ENDIF
	
	RETURN data.Cutscene.Scenes[iScene].Camera[iCamera].vCoords
ENDFUNC

FUNC VECTOR GET_CUTSCENE_CAMERA_ROTATION(INT iScene, INT iCamera)
	IF logic.Cutscene.Camera.Rotation != NULL
		RETURN CALL logic.Cutscene.Camera.Rotation(iScene, iCamera)
	ENDIF
	
	RETURN data.Cutscene.Scenes[iScene].Camera[iCamera].vRotation
ENDFUNC

FUNC FLOAT GET_CUTSCENE_CAMERA_FOV(INT iScene, INT iCamera)
	IF logic.Cutscene.Camera.FOV != NULL
		RETURN CALL logic.Cutscene.Camera.FOV(iScene, iCamera)
	ENDIF
	
	RETURN data.Cutscene.Scenes[iScene].Camera[iCamera].fFOV
ENDFUNC

FUNC FLOAT GET_CUTSCENE_CAMERA_SHAKE_AMPLITUDE(INT iScene, INT iCamera)
	IF logic.Cutscene.Camera.ShakeAmplitude != NULL
		RETURN CALL logic.Cutscene.Camera.ShakeAmplitude(iScene, iCamera)
	ENDIF
	
	RETURN data.Cutscene.Scenes[iScene].Camera[iCamera].fShakeAmplitude
ENDFUNC
#ENDIF

FUNC STRING GET_CUTSCENE_ANIM_DICT(INT iScene)
	IF logic.Cutscene.AnimDict != NULL
		RETURN CALL logic.Cutscene.AnimDict(iScene)
	ENDIF
	
	RETURN data.Cutscene.Scenes[iScene].sAnimDict
ENDFUNC

FUNC STRING GET_CUTSCENE_CAMERA_ANIM_DICT(INT iScene)
	IF logic.Cutscene.CameraAnimDict != NULL
		// Optional Overwrite
		RETURN CALL logic.Cutscene.CameraAnimDict(iScene)
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(data.Cutscene.Scenes[iScene].sCameraAnimDict)
		// Fallback if field not set
		RETURN GET_CUTSCENE_ANIM_DICT(iScene)
	ELSE
		// Specific Camera Anim Dict
		RETURN data.Cutscene.Scenes[iScene].sCameraAnimDict
	ENDIF
	
ENDFUNC

FUNC INT GET_CUTSCENE_DURATION(INT iScene)
	IF logic.Cutscene.Duration != NULL
		RETURN CALL logic.Cutscene.Duration(iScene)
	ENDIF
	
	SWITCH data.Cutscene.Scenes[iScene].eType
		CASE eCUTSCENETYPE_ANIM_SYNCED
			RETURN ROUND(GET_ANIM_DURATION(GET_CUTSCENE_CAMERA_ANIM_DICT(iScene), data.Cutscene.Scenes[iScene].sCameraAnim) * 1000)
			
		CASE eCUTSCENETYPE_MOCAP
			IF HAS_CUTSCENE_LOADED()
				RETURN GET_CUTSCENE_TOTAL_DURATION()
			ELSE
				RETURN -1
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN CUTSCENE_DURATION_DEFAULT_MS
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_OK_FOR_CUTSCENE()
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(LOCAL_PLAYER_INDEX)
	OR IS_HACKER_TRUCK_PILOT_ENTERING()
	OR NOT IS_LOCAL_ALIVE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC VECTOR GET_CUTSCENE_RANGE_COORDS(INT iScene)
	IF logic.Cutscene.RangeCoords != NULL
		RETURN CALL logic.Cutscene.RangeCoords(iScene)
	ENDIF
	
	RETURN GET_CUTSCENE_COORDS(iScene)
ENDFUNC

FUNC BOOL CREATE_CUTSCENE_CAMERA(INT iScene)
	IF NOT DOES_CAM_EXIST(sCutscene.sScenes[iScene].camID)
		SWITCH data.Cutscene.Scenes[iScene].eType
			CASE eCUTSCENETYPE_SCRIPTED_CAM
				#IF MAX_NUM_CUTSCENE_CAMERAS
				INT iCamera
				REPEAT data.Cutscene.Scenes[iScene].iNumCameras iCamera
					IF iCamera > 0
						ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [CUTSCENE] CREATE_CUTSCENE_CAMERA - iNumCameras > 1. Multiple cameras not implemented yet, give Ryan a shout.")
						BREAKLOOP
					ENDIF
				
					sCutscene.sScenes[iScene].camID = CREATE_CAMERA(CAMTYPE_SCRIPTED)
					SET_CAM_PARAMS(sCutscene.sScenes[iScene].camID, GET_CUTSCENE_CAMERA_COORD(iScene, iCamera), 
																	GET_CUTSCENE_CAMERA_ROTATION(iScene, iCamera), 
																	GET_CUTSCENE_CAMERA_FOV(iScene, iCamera))
					
					SET_CAM_FAR_CLIP(sCutscene.sScenes[iScene].camID, 1000.0)
					SHAKE_CAM(sCutscene.sScenes[iScene].camID, "HAND_SHAKE", GET_CUTSCENE_CAMERA_SHAKE_AMPLITUDE(iScene, iCamera))
				ENDREPEAT
				#ENDIF
			BREAK
			
			DEFAULT
				IF IS_VECTOR_ZERO(GET_CUTSCENE_CAMERA_COORD(iScene, iCamera))
					sCutscene.sScenes[iScene].camID = CREATE_CAMERA(CAMTYPE_ANIMATED)
				ELSE
					sCutscene.sScenes[iScene].camID = CREATE_CAMERA(CAMTYPE_SCRIPTED)
					SET_CAM_PARAMS(sCutscene.sScenes[iScene].camID, GET_CUTSCENE_CAMERA_COORD(iScene, iCamera), 
																	GET_CUTSCENE_CAMERA_ROTATION(iScene, iCamera), 
																	GET_CUTSCENE_CAMERA_FOV(iScene, iCamera))
					
					SET_CAM_FAR_CLIP(sCutscene.sScenes[iScene].camID, 1000.0)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CREATE_CUTSCENE_CAMERA - iScene = ", iScene, " - Created sCutscene.sScenes[", iScene, "].camID.")
	
	RETURN TRUE
ENDFUNC

#IF MAX_NUM_CUTSCENE_PLAYERS
FUNC PLAYER_INDEX GET_CUTSCENE_PLAYER_ID(INT iScene, INT iPlayerClone)
	IF logic.Cutscene.PlayerID != NULL
		RETURN CALL logic.Cutscene.PlayerID(iScene, iPlayerClone)
	ENDIF
	
	RETURN INVALID_PLAYER_INDEX()
ENDFUNC

FUNC VECTOR GET_CUTSCENE_PLAYER_CLONE_COORDS(INT iScene, INT iPlayerClone)
	IF NOT IS_VECTOR_ZERO(data.Cutscene.Scenes[iScene].PlayerClones[iPlayerClone].vCoords)
		RETURN data.Cutscene.Scenes[iScene].PlayerClones[iPlayerClone].vCoords
	ENDIF
	
	RETURN GET_CUTSCENE_COORDS(iScene) + <<0.0, 0.0, 0.1 * iPlayerClone>>
ENDFUNC

FUNC BOOL CLONE_CUTSCENE_PLAYERS(INT iScene)
	BOOL bDone = TRUE
	
	INT iPlayerClone
	REPEAT data.Cutscene.Scenes[iScene].iNumPlayerClones iPlayerClone
		PLAYER_INDEX playerID = GET_CUTSCENE_PLAYER_ID(iScene, iPlayerClone)
		
		IF playerID = INVALID_PLAYER_INDEX()
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CLONE_CUTSCENE_PLAYERS - iScene = ", iScene, " - Invalid player index for sCutscene.sScenes[", iScene, "].pedPlayerClones[", iPlayerClone, "], relooping.")
			RELOOP
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone])
			IF CUTSCENE_HELP_CLONE_PLAYER(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone], playerID, FALSE)
				SET_ENTITY_COORDS(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone], GET_CUTSCENE_PLAYER_CLONE_COORDS(iScene, iPlayerClone))
				SET_ENTITY_HEADING(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone], data.Cutscene.Scenes[iScene].PlayerClones[iPlayerClone].fHeading)
				
				SET_ENTITY_VISIBLE(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone], FALSE)
				SET_ENTITY_COMPLETELY_DISABLE_COLLISION(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone], FALSE)
				FREEZE_ENTITY_POSITION(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone], TRUE)
				SET_ENTITY_INVINCIBLE(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone], TRUE)
				
				IF logic.Cutscene.OnPlayerCloneCreated != NULL
					CALL logic.Cutscene.OnPlayerCloneCreated(iScene, iPlayerClone)
				ENDIF
				
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CLONE_CUTSCENE_PLAYERS - iScene = ", iScene, " - Created sCutscene.sScenes[", iScene, "].pedPlayerClones[", iPlayerClone, "].")
			ELSE
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CLONE_CUTSCENE_PLAYERS - iScene = ", iScene, " - Waiting for sCutscene.sScenes[", iScene, "].pedPlayerClones[", iPlayerClone, "] to be created.")
				bDone = FALSE
			ENDIF
		ENDIF
		
		IF IS_ENTITY_ALIVE(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone])
			IF HAS_PED_HEAD_BLEND_FINISHED(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone])
			AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone])
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CLONE_CUTSCENE_PLAYERS - iScene = ", iScene, " - Streaming requests completed for sCutscene.sScenes[", iScene, "].pedPlayerClones[", iPlayerClone, "].")
			ELSE
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CLONE_CUTSCENE_PLAYERS - iScene = ", iScene, " - Waiting for sCutscene.sScenes[", iScene, "].pedPlayerClones[", iPlayerClone, "] streaming requests to complete.")
				bDone = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN bDone
ENDFUNC
#ENDIF

#IF MAX_NUM_CUTSCENE_PEDS
FUNC PED_INDEX GET_CUTSCENE_PED(INT iScene, INT iPedClone)
	PED_INDEX pedId
	
	IF data.Cutscene.Scenes[iScene].PedClones[iPedClone].iPed != -1
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[data.Cutscene.Scenes[iScene].PedClones[iPedClone].iPed].netId)
	AND NOT IS_NET_PED_INJURED(serverBD.sPed[data.Cutscene.Scenes[iScene].PedClones[iPedClone].iPed].netId)
		pedId = NET_TO_PED(serverBD.sPed[data.Cutscene.Scenes[iScene].PedClones[iPedClone].iPed].netId)
	ENDIF
	
	RETURN pedId
ENDFUNC

FUNC VECTOR GET_CUTSCENE_PED_CLONE_COORDS(INT iScene, INT iPedClone)
	IF NOT IS_VECTOR_ZERO(data.Cutscene.Scenes[iScene].PedClones[iPedClone].vCoords)
		RETURN data.Cutscene.Scenes[iScene].PedClones[iPedClone].vCoords
	ENDIF
	
	RETURN GET_CUTSCENE_COORDS(iScene) + <<0.0, 0.0, 0.1 * iPedClone>>
ENDFUNC

FUNC BOOL CLONE_CUTSCENE_PEDS(INT iScene)
	BOOL bDone = TRUE
	
	INT iPedClone
	REPEAT data.Cutscene.Scenes[iScene].iNumPedClones iPedClone
		IF NOT DOES_ENTITY_EXIST(sCutscene.sScenes[iScene].pedClones[iPedClone])
			IF CUTSCENE_HELP_CLONE_PED(sCutscene.sScenes[iScene].pedClones[iPedClone], GET_CUTSCENE_PED(iScene, iPedClone), FALSE)
				SET_ENTITY_COORDS(sCutscene.sScenes[iScene].pedClones[iPedClone], GET_CUTSCENE_PED_CLONE_COORDS(iScene, iPedClone))
				SET_ENTITY_HEADING(sCutscene.sScenes[iScene].pedClones[iPedClone], data.Cutscene.Scenes[iScene].PedClones[iPedClone].fHeading)
				
				SET_ENTITY_VISIBLE(sCutscene.sScenes[iScene].pedClones[iPedClone], FALSE)
				SET_ENTITY_COMPLETELY_DISABLE_COLLISION(sCutscene.sScenes[iScene].pedClones[iPedClone], FALSE)
				FREEZE_ENTITY_POSITION(sCutscene.sScenes[iScene].pedClones[iPedClone], TRUE)
				SET_ENTITY_INVINCIBLE(sCutscene.sScenes[iScene].pedClones[iPedClone], TRUE)
				
				IF logic.Cutscene.OnPedCloneCreated != NULL
					CALL logic.Cutscene.OnPedCloneCreated(iScene, iPedClone)
				ENDIF
				
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CLONE_CUTSCENE_PEDS - iScene = ", iScene, " - Created sCutscene.sScenes[", iScene, "].pedClones[", iPedClone, "] as a clone of iPed #", data.Cutscene.Scenes[iScene].PedClones[iPedClone].iPed)
			ELSE
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CLONE_CUTSCENE_PEDS - iScene = ", iScene, " - Waiting for sCutscene.sScenes[", iScene, "].pedClones[", iPedClone, "] to be created.")
				bDone = FALSE
			ENDIF
		ENDIF
		
		IF IS_ENTITY_ALIVE(sCutscene.sScenes[iScene].pedClones[iPedClone])
			IF HAS_PED_HEAD_BLEND_FINISHED(sCutscene.sScenes[iScene].pedClones[iPedClone])
			AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sCutscene.sScenes[iScene].pedClones[iPedClone])
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CLONE_CUTSCENE_PEDS - iScene = ", iScene, " - Streaming requests completed for sCutscene.sScenes[", iScene, "].pedClones[", iPedClone, "].")
			ELSE
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CLONE_CUTSCENE_PEDS - iScene = ", iScene, " - Waiting for sCutscene.sScenes[", iScene, "].pedClones[", iPedClone, "] streaming requests to complete.")
				bDone = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN bDone
ENDFUNC
#ENDIF

#IF MAX_NUM_CUTSCENE_VEHICLES
FUNC VEHICLE_INDEX GET_CUTSCENE_VEHICLE(INT iScene, INT iVehicleClone)
	IF data.Cutscene.Scenes[iScene].VehicleClones[iVehicleClone].iVehicle != -1
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[data.Cutscene.Scenes[iScene].VehicleClones[iVehicleClone].iVehicle].netId)
		VEHICLE_INDEX vehId = NET_TO_VEH(serverBD.sVehicle[data.Cutscene.Scenes[iScene].VehicleClones[iVehicleClone].iVehicle].netId)
		
		IF NOT IS_ENTITY_DEAD(vehId)
			RETURN vehId
		ENDIF
	ENDIF
	
	RETURN NULL
ENDFUNC

FUNC VECTOR GET_CUTSCENE_VEHICLE_CLONE_COORDS(INT iScene, INT iVehicleClone)
	IF NOT IS_VECTOR_ZERO(data.Cutscene.Scenes[iScene].VehicleClones[iVehicleClone].vCoords)
		RETURN data.Cutscene.Scenes[iScene].VehicleClones[iVehicleClone].vCoords
	ENDIF
	
	RETURN GET_CUTSCENE_COORDS(iScene) + <<0.0, 0.0, 0.1 * iVehicleClone>>
ENDFUNC

FUNC BOOL CLONE_CUTSCENE_VEHICLES_WITH_PASSENGERS(INT iScene)
	BOOL bDone = TRUE
	
	INT iVehicleClone
	REPEAT data.Cutscene.Scenes[iScene].iNumVehicleClones iVehicleClone
		IF NOT sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].bCreated
			VEHICLE_INDEX vehId = GET_CUTSCENE_VEHICLE(iScene, iVehicleClone)
			
			IF vehId != NULL
			AND CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS(vehId, sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone, data.Cutscene.Scenes[iScene].VehicleClones[iVehicleClone].eVehicleCloneFlags)
				SET_ENTITY_COORDS(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehicle, GET_CUTSCENE_VEHICLE_CLONE_COORDS(iScene, iVehicleClone))
				SET_ENTITY_HEADING(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehicle, data.Cutscene.Scenes[iScene].VehicleClones[iVehicleClone].fHeading)
				
				SET_VEHICLE_ENGINE_ON(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehicle, TRUE, TRUE)
				
				SET_ENTITY_COMPLETELY_DISABLE_COLLISION(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehicle, FALSE)
				
				IF logic.Cutscene.OnVehicleCloneCreated != NULL
					CALL logic.Cutscene.OnVehicleCloneCreated(iScene, iVehicleClone)
				ENDIF
				
				sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].bCreated = TRUE
				
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CLONE_CUTSCENE_VEHICLES_WITH_PASSENGERS - iScene = ", iScene, " - Created sCutscene.sScenes[", iScene, "].VehicleClones[", iVehicleClone, "] as a clone of iVehicle #", data.Cutscene.Scenes[iScene].VehicleClones[iVehicleClone].iVehicle)
			ELSE
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CLONE_CUTSCENE_VEHICLES_WITH_PASSENGERS - iScene = ", iScene, " - Waiting for sCutscene.sScenes[", iScene, "].VehicleClones[", iVehicleClone, "] to be created.")
				bDone = FALSE
			ENDIF
		ENDIF
		
		INT iPassengerClone
		REPEAT CI_MAX_NUMBER_OF_PASSANGERS iPassengerClone
			IF IS_ENTITY_ALIVE(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehPassengers[iPassengerClone])
				IF IS_ENTITY_VISIBLE(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehPassengers[iPassengerClone])
					SET_ENTITY_VISIBLE(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehPassengers[iPassengerClone], FALSE)
				ENDIF
				
				IF HAS_PED_HEAD_BLEND_FINISHED(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehPassengers[iPassengerClone])
				AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehPassengers[iPassengerClone])
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CLONE_CUTSCENE_VEHICLES_WITH_PASSENGERS - iScene = ", iScene, " - Streaming requests completed for sCutscene.sScenes[", iScene, "].VehicleClones[", iVehicleClone, "].sCutsceneHelpVehicleClone.vehPassengers[", iPassengerClone, "].")
				ELSE
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CLONE_CUTSCENE_VEHICLES_WITH_PASSENGERS - iScene = ", iScene, " - Waiting for sCutscene.sScenes[", iScene, "].VehicleClones[", iVehicleClone, "].sCutsceneHelpVehicleClone.vehPassengers[", iPassengerClone, "] streaming requests to complete.")
					bDone = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	RETURN bDone
ENDFUNC
#ENDIF

#IF MAX_NUM_CUTSCENE_PROPS
FUNC VECTOR GET_CUTSCENE_PROP_COORDS(INT iScene, INT iProp)
	IF NOT IS_VECTOR_ZERO(data.Cutscene.Scenes[iScene].Props[iProp].vCoords)
		RETURN data.Cutscene.Scenes[iScene].Props[iProp].vCoords
	ENDIF
	
	RETURN GET_CUTSCENE_COORDS(iScene) + <<0.0, 0.0, 0.1 * iProp>>
ENDFUNC

FUNC BOOL CREATE_CUTSCENE_PROPS(INT iScene)
	BOOL bDone = TRUE
	
	INT iProp
	REPEAT data.Cutscene.Scenes[iScene].iNumProps iProp
		IF NOT DOES_ENTITY_EXIST(sCutscene.sScenes[iScene].objProps[iProp])
			sCutscene.sScenes[iScene].objProps[iProp] = CREATE_OBJECT(data.Cutscene.Scenes[iScene].Props[iProp].eModel, GET_CUTSCENE_PROP_COORDS(iScene, iProp), FALSE, FALSE, TRUE)
			SET_ENTITY_HEADING(sCutscene.sScenes[iScene].objProps[iProp], data.Cutscene.Scenes[iScene].Props[iProp].fHeading)
			
			SET_ENTITY_INVINCIBLE(sCutscene.sScenes[iScene].objProps[iProp], TRUE)
			SET_ENTITY_COMPLETELY_DISABLE_COLLISION(sCutscene.sScenes[iScene].objProps[iProp], FALSE)
			FREEZE_ENTITY_POSITION(sCutscene.sScenes[iScene].objProps[iProp], TRUE)
			
			IF logic.Cutscene.OnPropCreated != NULL
				CALL logic.Cutscene.OnPropCreated(iScene, iProp)
			ENDIF
			
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CREATE_CUTSCENE_PROPS - iScene = ", iScene, " - Created sCutscene.sScenes[", iScene, "].objProps[", iProp, "].")
		ENDIF
		
		IF DOES_ENTITY_HAVE_DRAWABLE(sCutscene.sScenes[iScene].objProps[iProp])
			SET_ENTITY_VISIBLE(sCutscene.sScenes[iScene].objProps[iProp], FALSE)
		ELSE
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CREATE_CUTSCENE_PROPS - iScene = ", iScene, " - Waiting for sCutscene.sScenes[", iScene, "].objProps[", iProp, "] to stream drawable.")
			bDone = FALSE
		ENDIF
	ENDREPEAT
	
	RETURN bDone
ENDFUNC
#ENDIF

FUNC BOOL CREATE_CUTSCENE_ENTITIES(INT iScene)
	UNUSED_PARAMETER(iScene)
	
	BOOL bDone = CREATE_CUTSCENE_CAMERA(iScene)
	
	#IF MAX_NUM_CUTSCENE_PLAYERS
	bDone = bDone AND CLONE_CUTSCENE_PLAYERS(iScene)
	#ENDIF
	
	#IF MAX_NUM_CUTSCENE_PEDS
	bDone = bDone AND CLONE_CUTSCENE_PEDS(iScene)
	#ENDIF
	
	#IF MAX_NUM_CUTSCENE_VEHICLES
	bDone = bDone AND CLONE_CUTSCENE_VEHICLES_WITH_PASSENGERS(iScene)
	#ENDIF
	
	#IF MAX_NUM_CUTSCENE_PROPS
	bDone = bDone AND CREATE_CUTSCENE_PROPS(iScene)
	#ENDIF
	
	RETURN bDone
ENDFUNC

PROC ADD_CUTSCENE_CAMERA(INT iScene)
	IF NOT IS_STRING_NULL_OR_EMPTY(data.Cutscene.Scenes[iScene].sCameraAnim)
		PLAY_SYNCHRONIZED_CAM_ANIM(sCutscene.sScenes[iScene].camID, sCutscene.sScenes[iScene].iID, data.Cutscene.Scenes[iScene].sCameraAnim, GET_CUTSCENE_CAMERA_ANIM_DICT(iScene))
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - ADD_CUTSCENE_CAMERA - iScene = ", iScene, " - Camera using PLAY_SYNCHRONIZED_CAM_ANIM, sCameraAnim = ", data.Cutscene.Scenes[iScene].sCameraAnim, " sCameraAnimDict = ", GET_CUTSCENE_CAMERA_ANIM_DICT(iScene))
	ENDIF
	
	SET_CAM_ACTIVE(sCutscene.sScenes[iScene].camID, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
ENDPROC

#IF MAX_NUM_CUTSCENE_PLAYERS
PROC ADD_CUTSCENE_PLAYER_CLONES(INT iScene)
	INT iPlayerClone
	REPEAT data.Cutscene.Scenes[iScene].iNumPlayerClones iPlayerClone
		IF IS_PED_UNINJURED(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone])
			
			// Body Animation
			IF NOT IS_STRING_NULL_OR_EMPTY(data.Cutscene.Scenes[iScene].PlayerClones[iPlayerClone].sAnim)
				TASK_SYNCHRONIZED_SCENE(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone], sCutscene.sScenes[iScene].iID, GET_CUTSCENE_ANIM_DICT(iScene), data.Cutscene.Scenes[iScene].PlayerClones[iPlayerClone].sAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, DEFAULT, DEFAULT, DEFAULT, data.Cutscene.Scenes[iScene].PlayerClones[iPlayerClone].eIKControlFlags)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - ADD_CUTSCENE_PLAYER_CLONES - iScene = ", iScene, " - iPlayerClone = ", iPlayerClone, " using TASK_SYNCHRONIZED_SCENE, sAnim = ", data.Cutscene.Scenes[iScene].PlayerClones[iPlayerClone].sAnim)
			ENDIF
			
			// Facial Animation
			IF NOT IS_STRING_NULL_OR_EMPTY(data.Cutscene.Scenes[iScene].PlayerClones[iPlayerClone].sFacialAnim)
				PLAY_FACIAL_ANIM(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone], data.Cutscene.Scenes[iScene].PlayerClones[iPlayerClone].sAnim, GET_CUTSCENE_ANIM_DICT(iScene))
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - ADD_CUTSCENE_PLAYER_CLONES - iScene = ", iScene, " - iPlayerClone = ", iPlayerClone, " using PLAY_FACIAL_ANIM, sAnim = ", data.Cutscene.Scenes[iScene].PlayerClones[iPlayerClone].sFacialAnim)
			ENDIF
			
			SET_ENTITY_VISIBLE(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone], TRUE)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone], TRUE, TRUE)
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

#IF MAX_NUM_CUTSCENE_PEDS
PROC ADD_CUTSCENE_PED_CLONES(INT iScene)
	INT iPedClone
	REPEAT data.Cutscene.Scenes[iScene].iNumPedClones iPedClone
		IF IS_PED_UNINJURED(sCutscene.sScenes[iScene].pedClones[iPedClone])
			IF NOT IS_STRING_NULL_OR_EMPTY(data.Cutscene.Scenes[iScene].PedClones[iPedClone].sAnim)
				TASK_SYNCHRONIZED_SCENE(sCutscene.sScenes[iScene].pedClones[iPedClone], sCutscene.sScenes[iScene].iID,  GET_CUTSCENE_ANIM_DICT(iScene), data.Cutscene.Scenes[iScene].PedClones[iPedClone].sAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, DEFAULT, DEFAULT, DEFAULT, data.Cutscene.Scenes[iScene].PedClones[iPedClone].eIKControlFlags)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - ADD_CUTSCENE_PED_CLONES - iScene = ", iScene, " - iPedClone = ", iPedClone, " using TASK_SYNCHRONIZED_SCENE, sAnim = ", data.Cutscene.Scenes[iScene].PedClones[iPedClone].sAnim)
			ENDIF
			
			SET_ENTITY_VISIBLE(sCutscene.sScenes[iScene].pedClones[iPedClone], TRUE)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(sCutscene.sScenes[iScene].pedClones[iPedClone], TRUE, TRUE)
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

#IF MAX_NUM_CUTSCENE_VEHICLES
PROC ADD_CUTSCENE_VEHICLE_CLONES(INT iScene)
	INT iVehicleClone
	REPEAT data.Cutscene.Scenes[iScene].iNumVehicleClones iVehicleClone
		IF IS_ENTITY_ALIVE(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehicle)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - ADD_CUTSCENE_VEHICLE_CLONES - iScene = ", iScene, " - iVehicleClone = ", iVehicleClone)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(data.Cutscene.Scenes[iScene].VehicleClones[iVehicleClone].sAnim)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehicle, sCutscene.sScenes[iScene].iID, data.Cutscene.Scenes[iScene].VehicleClones[iVehicleClone].sAnim, GET_CUTSCENE_ANIM_DICT(iScene), INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - ADD_CUTSCENE_VEHICLE_CLONES - iScene = ", iScene, " - iVehicleClone = ", iVehicleClone, " using PLAY_SYNCHRONIZED_ENTITY_ANIM, sAnim = ", data.Cutscene.Scenes[iScene].VehicleClones[iVehicleClone].sAnim)
			ENDIF
			
			SET_ENTITY_VISIBLE(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehicle, TRUE)
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehicle)
		ENDIF
		
		#IF MAX_NUM_CUTSCENE_VEHICLE_PASSENGERS
		INT iPassenger
		REPEAT data.Cutscene.Scenes[iScene].VehicleClones[iVehicleClone].iNumPassengers iPassenger
			IF IS_PED_UNINJURED(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehPassengers[iPassenger])
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - ADD_CUTSCENE_VEHICLE_CLONES - iScene = ", iScene, " - iVehicleClone = ", iVehicleClone, ", iPassenger = ", iPassenger)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(data.Cutscene.Scenes[iScene].VehicleClones[iVehicleClone].sPassengers[iPassenger].sAnim)
					TASK_SYNCHRONIZED_SCENE(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehPassengers[iPassenger], sCutscene.sScenes[iScene].iID, GET_CUTSCENE_ANIM_DICT(iScene), data.Cutscene.Scenes[iScene].VehicleClones[iVehicleClone].sPassengers[iPassenger].sAnim, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, DEFAULT, DEFAULT, DEFAULT, data.Cutscene.Scenes[iScene].VehicleClones[iVehicleClone].sPassengers[iPassenger].eIKControlFlags)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - ADD_CUTSCENE_VEHICLE_CLONES - iScene = ", iScene, " - iVehicleClone = ", iVehicleClone, ", iPassenger = ", iPassenger, " using TASK_SYNCHRONIZED_SCENE, sAnim = ", data.Cutscene.Scenes[iScene].VehicleClones[iVehicleClone].sPassengers[iPassenger].sAnim)
				ENDIF
				
				SET_ENTITY_VISIBLE(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehPassengers[iPassenger], TRUE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone.vehPassengers[iPassenger], TRUE, TRUE)
			ENDIF
		ENDREPEAT
		#ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

#IF MAX_NUM_CUTSCENE_PROPS
PROC ADD_CUTSCENE_PROPS(INT iScene)
	INT iProp
	REPEAT data.Cutscene.Scenes[iScene].iNumProps iProp
		IF IS_ENTITY_ALIVE(sCutscene.sScenes[iScene].objProps[iProp])
			IF NOT IS_STRING_NULL_OR_EMPTY(data.Cutscene.Scenes[iScene].Props[iProp].sAnim)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(sCutscene.sScenes[iScene].objProps[iProp], sCutscene.sScenes[iScene].iID, data.Cutscene.Scenes[iScene].Props[iProp].sAnim, GET_CUTSCENE_ANIM_DICT(iScene), INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - ADD_CUTSCENE_PROPS - iScene = ", iScene, " - iProp = ", iProp, " using PLAY_SYNCHRONIZED_ENTITY_ANIM, sAnim = ", data.Cutscene.Scenes[iScene].Props[iProp].sAnim)
			ENDIF
			
			SET_ENTITY_VISIBLE(sCutscene.sScenes[iScene].objProps[iProp], TRUE, TRUE)
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sCutscene.sScenes[iScene].objProps[iProp])
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

PROC ADD_CUTSCENE_ENTITIES(INT iScene)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - ADD_CUTSCENE_ENTITIES - iScene = ", iScene)

	SWITCH data.Cutscene.Scenes[iScene].eType
		CASE eCUTSCENETYPE_ANIM_SYNCED
		CASE eCUTSCENETYPE_SCRIPTED_CAM
			ADD_CUTSCENE_CAMERA(iScene)
		BREAK
	ENDSWITCH
	
	#IF MAX_NUM_CUTSCENE_PLAYERS
	ADD_CUTSCENE_PLAYER_CLONES(iScene)
	#ENDIF
	
	#IF MAX_NUM_CUTSCENE_PEDS
	ADD_CUTSCENE_PED_CLONES(iScene)
	#ENDIF
	
	#IF MAX_NUM_CUTSCENE_VEHICLES
	ADD_CUTSCENE_VEHICLE_CLONES(iScene)
	#ENDIF
	
	#IF MAX_NUM_CUTSCENE_PROPS
	ADD_CUTSCENE_PROPS(iScene)
	#ENDIF
ENDPROC

FUNC BOOL SHOULD_CUTSCENE_FADE_IN(INT iScene)
	IF logic.Cutscene.ShouldFadeIn != NULL
		RETURN CALL logic.Cutscene.ShouldFadeIn(iScene)
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CUTSCENE_FADE_OUT(INT iScene)
	#IF IS_DEBUG_BUILD
	IF sDebugVars.Cutscene.bPause
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF logic.Cutscene.ShouldFadeOut != NULL
		RETURN CALL logic.Cutscene.ShouldFadeOut(iScene)
	ENDIF
	
	//By default, start fading out iFadeOutTime milliseconds before the end of the cutscene.
	INT iDurationMS = GET_CUTSCENE_DURATION(iScene)
	
	SWITCH data.Cutscene.Scenes[iScene].eType
		CASE eCUTSCENETYPE_ANIM_SYNCED
			RETURN GET_SYNCHRONIZED_SCENE_PHASE(sCutscene.sScenes[iScene].iID) > (iDurationMS - data.Cutscene.Scenes[iScene].iFadeOutTime) / TO_FLOAT(iDurationMS)
		
		CASE eCUTSCENETYPE_SCRIPTED_CAM
			RETURN HAS_NET_TIMER_EXPIRED(sCutscene.sScenes[iScene].stTimer, iDurationMS - data.Cutscene.Scenes[iScene].iFadeOutTime)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC DELETE_CUTSCENE_CAMERA(INT iScene)
	SWITCH data.Cutscene.Scenes[iScene].eType
		CASE eCUTSCENETYPE_ANIM_SYNCED
		CASE eCUTSCENETYPE_SCRIPTED_CAM
			IF DOES_CAM_EXIST(sCutscene.sScenes[iScene].camID)
				IF IS_CAM_RENDERING(sCutscene.sScenes[iScene].camID)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CLEANUP_CUTSCENE - iScene = ", iScene, " - This scene's camera is rendering during cleanup, returning to gameplay camera.")
					RENDER_SCRIPT_CAMS(FALSE, FALSE, DEFAULT, DEFAULT, TRUE)
				ENDIF
				
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CLEANUP_CUTSCENE - iScene = ", iScene, " - Destroying camera.")
				DESTROY_CAM(sCutscene.sScenes[iScene].camID)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

#IF MAX_NUM_CUTSCENE_PLAYERS
PROC DELETE_CUTSCENE_PLAYER_CLONES(INT iScene)
	INT iPlayerClone
	REPEAT data.Cutscene.Scenes[iScene].iNumPlayerClones iPlayerClone
		IF DOES_ENTITY_EXIST(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone])
			DELETE_PED(sCutscene.sScenes[iScene].pedPlayerClones[iPlayerClone])
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - DELETE_CUTSCENE_PLAYER_CLONES - iScene = ", iScene, " - Deleted sCutscene.sScenes[", iScene, "].pedPlayerClones[", iPlayerClone, "].")
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

#IF MAX_NUM_CUTSCENE_PEDS
PROC DELETE_CUTSCENE_PED_CLONES(INT iScene)
	INT iPedClone
	REPEAT data.Cutscene.Scenes[iScene].iNumPedClones iPedClone
		IF DOES_ENTITY_EXIST(sCutscene.sScenes[iScene].pedClones[iPedClone])
			DELETE_PED(sCutscene.sScenes[iScene].pedClones[iPedClone])
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - DELETE_CUTSCENE_PED_CLONES - iScene = ", iScene, " - Deleted sCutscene.sScenes[", iScene, "].pedClones[", iPedClone, "].")
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

#IF MAX_NUM_CUTSCENE_VEHICLES
PROC DELETE_CUTSCENE_VEHICLE_CLONES(INT iScene)
	INT iVehicleClone
	REPEAT data.Cutscene.Scenes[iScene].iNumVehicleClones iVehicleClone
		CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_CLEANUP(sCutscene.sScenes[iScene].VehicleClones[iVehicleClone].sCutsceneHelpVehicleClone)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - DELETE_CUTSCENE_VEHICLE_CLONES - iScene = ", iScene, " - Called CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_CLEANUP on sCutscene.sScenes[", iScene, "].VehicleClones[", iVehicleClone, "].sCutsceneHelpVehicleClone.")
	ENDREPEAT
ENDPROC
#ENDIF

#IF MAX_NUM_CUTSCENE_PROPS
PROC DELETE_CUTSCENE_PROPS(INT iScene)
	INT iProp
	REPEAT data.Cutscene.Scenes[iScene].iNumProps iProp
		IF DOES_ENTITY_EXIST(sCutscene.sScenes[iScene].objProps[iProp])
			DELETE_OBJECT(sCutscene.sScenes[iScene].objProps[iProp])
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - DELETE_CUTSCENE_PROPS - iScene = ", iScene, " - Deleted sCutscene.sScenes[", iScene, "].objProps[", iProp, "].")
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

PROC DELETE_CUTSCENE_ENTITIES(INT iScene)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - DELETE_CUTSCENE_ENTITIES - iScene = ", iScene)
	
	DELETE_CUTSCENE_CAMERA(iScene)
	
	#IF MAX_NUM_CUTSCENE_PLAYERS
	DELETE_CUTSCENE_PLAYER_CLONES(iScene)
	#ENDIF
	
	#IF MAX_NUM_CUTSCENE_PEDS
	DELETE_CUTSCENE_PED_CLONES(iScene)
	#ENDIF
	
	#IF MAX_NUM_CUTSCENE_VEHICLES
	DELETE_CUTSCENE_VEHICLE_CLONES(iScene)
	#ENDIF
	
	#IF MAX_NUM_CUTSCENE_PROPS
	DELETE_CUTSCENE_PROPS(iScene)
	#ENDIF
ENDPROC

FUNC VECTOR GET_CUTSCENE_LOAD_SCENE_COORDS(INT iScene)
	IF logic.Cutscene.LoadSceneCoords != NULL
		RETURN CALL logic.Cutscene.LoadSceneCoords(iScene)
	ENDIF
	
	RETURN GET_CUTSCENE_COORDS(iScene)
ENDFUNC

FUNC BOOL SHOULD_CLEANUP_MP_CUTSCENE_RETURN_PLAYER_CONTROL(INT iScene)
	IF IS_CUTSCENE_DATA_BIT_SET(iScene, eCUTSCENEDATABITSET_DISABLE_RETURN_CONTROL_ON_CLEANUP)
		RETURN FALSE
	ENDIF
	
	#IF MAX_NUM_MISSION_PORTALS
	IF IS_LOCAL_PLAYER_USING_PORTAL()
		RETURN FALSE
	ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CLEANUP_MP_CUTSCENE_PREVENT_INVINCIBILITY_CHANGES()
	#IF MAX_NUM_MISSION_PORTALS
	IF IS_LOCAL_PLAYER_USING_PORTAL()
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_CUTSCENE_MP_CUTSCENE(INT iScene)
	IF IS_CUTSCENE_LOCAL_BIT_SET(eCUTSCENELOCALBITSET_STARTED_MP_CUTSCENE)
		IF ARE_CUTSCENE_ENTITIES_NETWORKED()
			SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
		ENDIF
		
		CLEANUP_MP_CUTSCENE(FALSE, SHOULD_CLEANUP_MP_CUTSCENE_RETURN_PLAYER_CONTROL(iScene), SHOULD_CLEANUP_MP_CUTSCENE_PREVENT_INVINCIBILITY_CHANGES())
		
		IF SHOULD_CLEANUP_MP_CUTSCENE_RETURN_PLAYER_CONTROL(iScene)
			CLEAR_GENERIC_BIT(eGENERICBITSET_DISABLED_PLAYER_CONTROL)
		ENDIF
		
		CLEAR_CUTSCENE_LOCAL_BIT(eCUTSCENELOCALBITSET_STARTED_MP_CUTSCENE)
	ENDIF
ENDPROC

PROC CLEANUP_CUTSCENE(INT iScene)
	IF GET_CUTSCENE_STATE(iScene) = eCUTSCENESTATE_INIT
		EXIT
	ENDIF
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - CLEANUP_CUTSCENE - iScene = ", iScene)
	DEBUG_PRINTCALLSTACK()
	
	IF logic.Cutscene.OnCleanup != NULL
		CALL logic.Cutscene.OnCleanup(iScene)
	ENDIF
	
	sCutscene.sScenes[iScene].iID = -1
	RESET_NET_TIMER(sCutscene.sScenes[iScene].stTimer)
	
	DELETE_CUTSCENE_ENTITIES(iScene)
	
	SWITCH data.Cutscene.Scenes[iScene].eType
		CASE eCUTSCENETYPE_ANIM_SYNCED
			REMOVE_ANIM_DICT(GET_CUTSCENE_ANIM_DICT(iScene))
			REMOVE_ANIM_DICT(GET_CUTSCENE_CAMERA_ANIM_DICT(iScene))
		BREAK
		
		CASE eCUTSCENETYPE_MOCAP
			IF IS_CUTSCENE_PLAYING()
				STOP_CUTSCENE_IMMEDIATELY()
				REMOVE_CUTSCENE()
			ENDIF
		BREAK
	ENDSWITCH
	
	IF IS_CUTSCENE_DATA_BIT_SET(iScene, eCUTSCENEDATABITSET_ENABLE_LOAD_SCENE)
	AND IS_CUTSCENE_LOCAL_BIT_SET(eCUTSCENELOCALBITSET_LOAD_SCENE_ACTIVE)
	AND IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
		CLEAR_CUTSCENE_LOCAL_BIT(eCUTSCENELOCALBITSET_LOAD_SCENE_ACTIVE)
	ENDIF
	
	IF NOT IS_CUTSCENE_DATA_BIT_SET(iScene, eCUTSCENEDATABITSET_DISABLE_CLEANUP_MP_CUTSCENE)
		CLEANUP_CUTSCENE_MP_CUTSCENE(iScene)
	ENDIF
	
	SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_INIT)
	
	IF iScene = GET_CURRENT_CUTSCENE(LOCAL_PARTICIPANT_INDEX)
		CLEAR_CURRENT_CUTSCENE()
	ENDIF
ENDPROC

PROC CLEANUP_CUTSCENES()
	IF IS_CUTSCENE_LOCAL_BIT_SET(eCUTSCENELOCALBITSET_FADE_OUT_ACTIVE)
		IF NOT IS_SCREEN_FADED_IN()
		AND NOT IS_SCREEN_FADING_IN()
			INT iScene = GET_CURRENT_CUTSCENE(LOCAL_PARTICIPANT_INDEX)
			
			IF iScene = -1
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ELSE
				DO_SCREEN_FADE_IN(data.Cutscene.Scenes[iScene].iFadeInTime)
			ENDIF
		ENDIF
		
		CLEAR_CUTSCENE_LOCAL_BIT(eCUTSCENELOCALBITSET_FADE_OUT_ACTIVE)
	ENDIF
	
	INT iScene
	REPEAT data.Cutscene.iNumScenes iScene
		CLEANUP_CUTSCENE(iScene)
	ENDREPEAT
ENDPROC

PROC REQUEST_MISSION_CUTSCENE(INT iScene)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - REQUEST_MISSION_CUTSCENE - iScene = ", iScene)
	
	SWITCH data.Cutscene.Scenes[iScene].eType
		CASE eCUTSCENETYPE_ANIM_SYNCED
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - REQUEST_MISSION_CUTSCENE - iScene = ", iScene, " - Loading GET_CUTSCENE_ANIM_DICT(iScene) = ", GET_CUTSCENE_ANIM_DICT(iScene))
			REQUEST_ANIM_DICT(GET_CUTSCENE_ANIM_DICT(iScene))
			
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - REQUEST_MISSION_CUTSCENE - iScene = ", iScene, " - Loading GET_CUTSCENE_CAMERA_ANIM_DICT(iScene) = ", GET_CUTSCENE_CAMERA_ANIM_DICT(iScene))
			REQUEST_ANIM_DICT(GET_CUTSCENE_CAMERA_ANIM_DICT(iScene))
		BREAK
		
		CASE eCUTSCENETYPE_MOCAP
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - REQUEST_MISSION_CUTSCENE - iScene = ", iScene, " - Loading GET_CUTSCENE_ANIM_DICT(iScene) = ", GET_CUTSCENE_ANIM_DICT(iScene))
			REQUEST_CUTSCENE(GET_CUTSCENE_ANIM_DICT(iScene))
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL HAS_MISSION_CUTSCENE_LOADED(INT iScene)
	SWITCH data.Cutscene.Scenes[iScene].eType
		CASE eCUTSCENETYPE_ANIM_SYNCED		RETURN HAS_ANIM_DICT_LOADED(GET_CUTSCENE_ANIM_DICT(iScene))
		CASE eCUTSCENETYPE_MOCAP			RETURN HAS_CUTSCENE_LOADED()
		CASE eCUTSCENETYPE_SCRIPTED_CAM		RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC START_MISSION_CUTSCENE(INT iScene)
	SWITCH data.Cutscene.Scenes[iScene].eType
		CASE eCUTSCENETYPE_ANIM_SYNCED
			sCutscene.sScenes[iScene].iID = CREATE_SYNCHRONIZED_SCENE(GET_CUTSCENE_COORDS(iScene), <<0.0, 0.0, GET_CUTSCENE_HEADING(iScene)>>)
		BREAK
		
		CASE eCUTSCENETYPE_MOCAP
			IF logic.Cutscene.PlayMocapAtCoords != NULL
			AND CALL logic.Cutscene.PlayMocapAtCoords(iScene)
				START_CUTSCENE_AT_COORDS(GET_CUTSCENE_COORDS(iScene))
				SET_CUTSCENE_ORIGIN(GET_CUTSCENE_COORDS(iScene), GET_CUTSCENE_HEADING(iScene), 0)
			ELSE
				START_CUTSCENE()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL HAS_MISSION_CUTSCENE_FINISHED(INT iScene)
	#IF IS_DEBUG_BUILD
	IF sDebugVars.Cutscene.bPause
		RETURN FALSE
	ENDIF
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_FMContentAllowCutsceneSkip")
	AND IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED() 
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - HAS_MISSION_CUTSCENE_FINISHED - DEBUG - iScene = ", iScene, " - ***** Player pressed skip button *****")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF logic.Cutscene.ShouldFinish != NULL
		RETURN CALL logic.Cutscene.ShouldFinish(iScene)
	ENDIF
	
	SWITCH data.Cutscene.Scenes[iScene].eType
		CASE eCUTSCENETYPE_ANIM_SYNCED		RETURN GET_SYNCHRONIZED_SCENE_PHASE(sCutscene.sScenes[iScene].iID) > 0.99
		CASE eCUTSCENETYPE_MOCAP			RETURN HAS_CUTSCENE_FINISHED()
		CASE eCUTSCENETYPE_SCRIPTED_CAM		RETURN HAS_NET_TIMER_EXPIRED(sCutscene.sScenes[iScene].stTimer, GET_CUTSCENE_DURATION(iScene))
	ENDSWITCH

	RETURN FALSE
ENDFUNC

PROC FMC_MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(BOOL bLeavePedCopyBehind = TRUE, BOOL bKeepPortableObjects = FALSE, BOOL bStopDucking = TRUE)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - FMC_MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE - bLeavePedCopyBehind = ", GET_STRING_FROM_BOOL(bLeavePedCopyBehind), ", bKeepPortableObjects = ", GET_STRING_FROM_BOOL(bKeepPortableObjects), ", bStopDucking = ", GET_STRING_FROM_BOOL(bStopDucking))
	DEBUG_PRINTCALLSTACK()
	
	Disable_MP_Comms()
	DISABLE_CELLPHONE(TRUE)
	
	SET_DPADDOWN_ACTIVE(TRUE)
	
	DISABLE_SCRIPT_HUD(HUDPART_AWARDS, TRUE)
	DISABLE_SELECTOR()
	SET_USER_RADIO_CONTROL_ENABLED(FALSE)
	
	IF bLeavePedCopyBehind
		NETWORK_LEAVE_PED_BEHIND_BEFORE_CUTSCENE(LOCAL_PLAYER_INDEX, bKeepPortableObjects)
	ENDIF
	
	IF bStopDucking
	AND IS_PED_DUCKING(LOCAL_PED_INDEX)
		SET_PED_DUCKING(LOCAL_PED_INDEX, FALSE)
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_USING_PORTAL()
		DISABLE_PLAYER_CONTROL(TRUE, NSPC_CLEAR_TASKS)
	ENDIF
ENDPROC

FUNC BOOL RUN_CUTSCENE(INT iScene)
	IF logic.Cutscene.MaintainRun != NULL
		CALL logic.Cutscene.MaintainRun(iScene)
	ENDIF
	
	IF GET_CUTSCENE_STATE(iScene) > eCUTSCENESTATE_WAIT_FOR_START
		CONTACT_REQUEST_MENU_DISABLE_THIS_FRAME()
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
	ENDIF
	
	SWITCH GET_CUTSCENE_STATE(iScene)
		CASE eCUTSCENESTATE_INIT
			IF SHOULD_CUTSCENE_LOAD_ASSETS(iScene)
				SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_REQUEST_ANIM_DICT)
			ENDIF
		BREAK
		
		CASE eCUTSCENESTATE_REQUEST_ANIM_DICT
			REQUEST_MISSION_CUTSCENE(iScene)
			
			IF data.Cutscene.Scenes[iScene].eType = eCUTSCENETYPE_MOCAP
				SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_WAIT_FOR_REQUEST_MOCAP_ASSETS)
			ELSE
				SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_WAIT_FOR_LOAD)
			ENDIF
			
		BREAK
		
		CASE eCUTSCENESTATE_WAIT_FOR_REQUEST_MOCAP_ASSETS
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				IF logic.Cutscene.SetStreamingFlags != NULL
					CALL logic.Cutscene.SetStreamingFlags(iScene)
				ENDIF
				SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_WAIT_FOR_LOAD)
			ENDIF
		BREAK
		
		CASE eCUTSCENESTATE_WAIT_FOR_LOAD
			IF HAS_MISSION_CUTSCENE_LOADED(iScene)
				IF IS_CUTSCENE_DATA_BIT_SET(iScene, eCUTSCENEDATABITSET_ENABLE_LOAD_SCENE)
					SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_LOAD_SCENE_START)
				ELSE
					SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_CREATE_ENTITIES)
				ENDIF
			ENDIF
		BREAK
		
		CASE eCUTSCENESTATE_LOAD_SCENE_START
			IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			AND NEW_LOAD_SCENE_START_SPHERE(GET_CUTSCENE_LOAD_SCENE_COORDS(iScene), data.Cutscene.Scenes[iScene].fLoadSceneDistance, data.Cutscene.Scenes[iScene].eLoadSceneFlags)
				SET_CUTSCENE_LOCAL_BIT(eCUTSCENELOCALBITSET_LOAD_SCENE_ACTIVE)
				SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_LOAD_SCENE_WAIT)
			ELIF HAS_NET_TIMER_EXPIRED(sCutscene.sScenes[iScene].stTimer, CUTSCENE_LOAD_SCENE_TIMEOUT_MS)
				RESET_NET_TIMER(sCutscene.sScenes[iScene].stTimer)
				SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_CREATE_ENTITIES)
			ENDIF
		BREAK
		
		CASE eCUTSCENESTATE_LOAD_SCENE_WAIT
			IF IS_NEW_LOAD_SCENE_LOADED()
			OR HAS_NET_TIMER_EXPIRED(sCutscene.sScenes[iScene].stTimer, CUTSCENE_LOAD_SCENE_TIMEOUT_MS)
				RESET_NET_TIMER(sCutscene.sScenes[iScene].stTimer)
				
				SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_CREATE_ENTITIES)
			ENDIF
		BREAK
		
		CASE eCUTSCENESTATE_CREATE_ENTITIES
			IF CREATE_CUTSCENE_ENTITIES(iScene)
				IF logic.Cutscene.LoadCustomAssets != NULL
					SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_LOAD_CUSTOM_ASSETS)
				ELSE
					SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_WAIT_FOR_START)
				ENDIF
			ENDIF
		BREAK
		
		CASE eCUTSCENESTATE_LOAD_CUSTOM_ASSETS
			IF CALL logic.Cutscene.LoadCustomAssets(iScene)
				SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_WAIT_FOR_START)
			ENDIF
		BREAK
		
		CASE eCUTSCENESTATE_WAIT_FOR_START
			IF SHOULD_CUTSCENE_START(iScene)
				SET_CURRENT_CUTSCENE(iScene)
				
				IF logic.Cutscene.RegisterEntities != NULL
					CALL logic.Cutscene.RegisterEntities(iScene)
				ENDIF
				
				START_MISSION_CUTSCENE(iScene)
				ADD_CUTSCENE_ENTITIES(iScene)
				
				SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(IS_CUTSCENE_DATA_BIT_SET(iScene, eCUTSCENEDATABITSET_LOCAL_PLAYER_LOCALLY_VISIBLE), IS_CUTSCENE_DATA_BIT_SET(iScene, eCUTSCENEDATABITSET_LOCAL_PLAYER_REMOTELY_VISIBLE))
				
				IF NOT IS_CUTSCENE_LOCAL_BIT_SET(eCUTSCENELOCALBITSET_STARTED_MP_CUTSCENE)
					FMC_MAKE_PLAYER_SAFE_FOR_MP_CUTSCENE(FALSE)
					
					IF IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_PLAYER_IN_CORONA()					
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - RUN_CUTSCENE - iScene = ", iScene, " - SET_FRONTEND_ACTIVE(FALSE).")
			            SET_FRONTEND_ACTIVE(FALSE)
			        ENDIF
					
					START_MP_CUTSCENE(NOT IS_CUTSCENE_DATA_BIT_SET(iScene, eCUTSCENEDATABITSET_DISABLE_MP_CUTSCENE))
					
					IF NOT IS_CUTSCENE_DATA_BIT_SET(iScene, eCUTSCENEDATABITSET_DISABLE_MP_CUTSCENE)
						SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
					ENDIF
					
					SET_CUTSCENE_LOCAL_BIT(eCUTSCENELOCALBITSET_STARTED_MP_CUTSCENE)
				ENDIF
				
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - RUN_CUTSCENE - iScene = ", iScene, " - Cutscene has started.")
				
				IF logic.Cutscene.OnStart != NULL
					CALL logic.Cutscene.OnStart(iScene)
				ENDIF
				
				IF IS_CUTSCENE_DATA_BIT_SET(iScene, eCUTSCENEDATABITSET_FADE_IN)
					SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_WAIT_FOR_FADE_IN)
				ELIF IS_CUTSCENE_DATA_BIT_SET(iScene, eCUTSCENEDATABITSET_WAIT_FOR_PLAYING)
					SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_WAIT_FOR_PLAYING)
				ELIF IS_CUTSCENE_DATA_BIT_SET(iScene, eCUTSCENEDATABITSET_FADE_OUT)
					SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_WAIT_FOR_FADE_OUT)
				ELSE
					SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_WAIT_FOR_END)
				ENDIF
			ENDIF
		BREAK
		
		CASE eCUTSCENESTATE_WAIT_FOR_FADE_IN
			IF SHOULD_CUTSCENE_FADE_IN(iScene)
				IF NOT IS_SCREEN_FADED_IN()
				AND NOT IS_SCREEN_FADING_IN()
					DO_SCREEN_FADE_IN(data.Cutscene.Scenes[iScene].iFadeInTime)
				ENDIF
				
				IF IS_CUTSCENE_DATA_BIT_SET(iScene, eCUTSCENEDATABITSET_FADE_OUT)
					SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_WAIT_FOR_FADE_OUT)
				ELSE
					SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_WAIT_FOR_END)
				ENDIF
			ENDIF
		BREAK
		
		CASE eCUTSCENESTATE_WAIT_FOR_PLAYING
			IF IS_CUTSCENE_PLAYING()
				IF logic.Cutscene.OnPlaying	!= NULL
					CALL logic.Cutscene.OnPlaying(iScene)
				ENDIF
				
				IF IS_CUTSCENE_DATA_BIT_SET(iScene, eCUTSCENEDATABITSET_FADE_OUT)
					SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_WAIT_FOR_FADE_OUT)
				ELSE
					SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_WAIT_FOR_END)
				ENDIF
			ENDIF
		BREAK
		
		CASE eCUTSCENESTATE_WAIT_FOR_FADE_OUT
			IF SHOULD_CUTSCENE_FADE_OUT(iScene)
				IF NOT IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(data.Cutscene.Scenes[iScene].iFadeOutTime)
					
					SET_CUTSCENE_LOCAL_BIT(eCUTSCENELOCALBITSET_FADE_OUT_ACTIVE)
				ENDIF
				
				SET_CUTSCENE_STATE(iScene, eCUTSCENESTATE_WAIT_FOR_END)
			ENDIF
		BREAK
		
		CASE eCUTSCENESTATE_WAIT_FOR_END
			IF HAS_MISSION_CUTSCENE_FINISHED(iScene)
			AND (NOT IS_CUTSCENE_DATA_BIT_SET(iScene, eCUTSCENEDATABITSET_FADE_OUT)	OR IS_SCREEN_FADED_OUT())
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CUTSCENE] - RUN_CUTSCENE - iScene = ", iScene, " - Cutscene has finished.")
				
				IF logic.Cutscene.OnEnd != NULL
					CALL logic.Cutscene.OnEnd(iScene)
				ENDIF
				
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CUTSCENE_CLEAN_UP(INT iScene)
	IF logic.Cutscene.ShouldCleanUp != NULL
		RETURN CALL logic.Cutscene.ShouldCleanUp(iScene)
	ENDIF
	
	RETURN IS_CUTSCENE_DONE_CLIENT(iScene, LOCAL_PARTICIPANT_INDEX)
ENDFUNC

PROC MAINTAIN_CUTSCENE()
	INT iScene
	IF SHOULD_ENABLE_CUTSCENE()
		IF IS_LOCAL_PLAYER_OK_FOR_CUTSCENE()
			REPEAT data.Cutscene.iNumScenes iScene
				IF NOT IS_CUTSCENE_DONE_CLIENT(iScene, LOCAL_PARTICIPANT_INDEX)
				AND RUN_CUTSCENE(iScene)
					SET_CUTSCENE_DONE_CLIENT(iScene)
				ENDIF
				
				IF SHOULD_CUTSCENE_CLEAN_UP(iScene)
					CLEANUP_CUTSCENE(iScene)
				ENDIF
			ENDREPEAT
		ELSE
			REPEAT data.Cutscene.iNumScenes iScene
				IF data.Cutscene.Scenes[iScene].eType != eCUTSCENETYPE_MOCAP
					// This isn't a mocap - cleanup
					CLEANUP_CUTSCENE(iScene)
				ELSE
					IF !IS_LOCAL_ALIVE
						// If it's a mocap, and we're dead, don't abandon the mocap unless we've actually started playing it - not sure this can ever actually happen?
						// N.B the cutscene won't start unless the local player is alive
						IF GET_CUTSCENE_STATE(iScene) > eCUTSCENESTATE_WAIT_FOR_START
							CLEANUP_CUTSCENE(iScene)
						ENDIF
					ELSE
						// We're not ok for the cutscene for some reason other than being dead - cleanup
						CLEANUP_CUTSCENE(iScene)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
		IF IS_CUTSCENE_LOCAL_BIT_SET(eCUTSCENELOCALBITSET_FADE_OUT_ACTIVE)
			IF IS_SCREEN_FADING_IN()
			OR IS_SCREEN_FADED_IN()
				CLEAR_CUTSCENE_LOCAL_BIT(eCUTSCENELOCALBITSET_FADE_OUT_ACTIVE)
			ENDIF
		ENDIF
		
	ELSE
		CLEANUP_CUTSCENES()
	ENDIF
ENDPROC

PROC PROCESS_CUTSCENE_PLAYER_LOOP(INT iParticipant)
	//Start of loop setup.
	IF iParticipant = 0
		INT iScene
		REPEAT data.Cutscene.iNumScenes iScene
			SET_CUTSCENE_DONE(iScene #IF IS_DEBUG_BUILD , TRUE #ENDIF)
		ENDREPEAT
	ENDIF
	
	//Participant checks.
	INT iScene
	REPEAT data.Cutscene.iNumScenes iScene
		IF IS_CUTSCENE_DONE(iScene)
		AND NOT IS_CUTSCENE_DONE_CLIENT(iScene, INT_TO_PARTICIPANTINDEX(iParticipant))
		AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			CLEAR_CUTSCENE_DONE(iScene #IF IS_DEBUG_BUILD , TRUE #ENDIF)
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF
