
// DO NOT DECLARE VARIABLES IN HERE

USING "globals.sch"
USING "rage_builtins.sch"
USING "net_spawn.sch"
USING "net_mission.sch"
USING "net_include.sch"
USING "net_ambience.sch"
USING "am_common_ui.sch"
USING "net_events.sch"
USING "net_gang_boss.sch"
USING "DM_Leaderboard.sch"
USING "fmmc_camera.sch"
USING "net_office.sch"
USING "net_random_events_public.sch"
USING "FMMC_Veh_Mod_Preset_List.sch"
USING "socialclub_leaderboard.sch"

#IF FEATURE_HEIST_ISLAND
USING "heist_island_travel.sch"
#ENDIF

USING "net_simple_interior_fixer_hq.sch"

#IF IS_DEBUG_BUILD
USING "profiler.sch"
#ENDIF

PROC EMPTY()
ENDPROC

FUNC BOOL RETURN_FALSE()
	RETURN FALSE
ENDFUNC

FUNC BOOL RETURN_TRUE()
	RETURN TRUE
ENDFUNC

PROC EMPTY_INT(INT i)
	UNUSED_PARAMETER(i)
ENDPROC
