USING "fm_content_generic_support.sch"

FUNC BOOL SHOULD_PROP_ONLY_EXIST_FOR_PARTICIPANTS(INT iProp)
	
	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_EXIST_ONLY_FOR_PARTICIPANTS)
		RETURN TRUE
	ENDIF
	
	#IF MAX_NUM_MISSION_INTERIORS
	IF data.Prop.Props[iProp].iInterior != (-1)
	AND IS_FMC_INTERIOR_A_WARP_INTERIOR(data.Interior.eInteriors[data.Prop.Props[iProp].iInterior].eType)
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF IS_DATA_BIT_SET(eDATABITSET_ENABLE_HEIST_ISLAND)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC VECTOR GET_MISSION_PROP_COORDS(INT iProp)
	
	VECTOR vCoords

	IF logic.Prop.Coords != NULL
		vCoords = CALL logic.Prop.Coords(iProp)
	ENDIF

	IF IS_VECTOR_ZERO(vCoords)
		vCoords = data.Prop.Props[iProp].vCoords
	ENDIF

	RETURN vCoords

ENDFUNC

FUNC VECTOR GET_MISSION_PROP_ROTATION(INT iProp)
	
	VECTOR vRotation

	IF logic.Prop.Rotation != NULL
		vRotation = CALL logic.Prop.Rotation(iProp)
	ENDIF

	IF IS_VECTOR_ZERO(vRotation)
		vRotation = data.Prop.Props[iProp].vRotation
	ENDIF

	RETURN vRotation

ENDFUNC

PROC SET_MISSION_PROP_ATTRIBUTES(INT iProp, ENTITY_INDEX propId)
	NETWORK_SET_OBJECT_SCOPE_DISTANCE(NET_TO_OBJ(serverBD.sProp[iProp].netID), 300)
	
	SET_ENTITY_PROOFS(propId, IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_BULLET_PROOF), 
							  IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_FLAME_PROOF),
							  IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_EXPLOSION_PROOF),
							  IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_COLLISION_PROOF),
							  IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_MELEE_PROOF))
							  
	CLEAR_PROP_BIT(iProp, ePROPBITSET_DESTROYED)

	#IF MAX_NUM_MISSION_INTERIORS
	IF data.Prop.Props[iProp].iInterior != -1
	AND IS_FMC_INTERIOR_A_WARP_INTERIOR(data.Interior.eInteriors[data.Prop.Props[iProp].iInterior].eType)
		SET_PROP_BIT(iProp, ePROPBITSET_INSIDE_MISSION_INTERIOR)
	ENDIF
	#ENDIF
	
	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_SYNC_SCENE_PROP)
		SET_ENTITY_INVINCIBLE(propId, TRUE)
		SET_ENTITY_VISIBLE(propId, FALSE)
		FREEZE_ENTITY_POSITION(propId, TRUE)
		SET_ENTITY_COLLISION(propId, FALSE)
	ENDIF
	
	IF NOT IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_DISABLE_PLACE_ON_GROUND)
		PLACE_OBJECT_ON_GROUND_OR_OBJECT_PROPERLY(NET_TO_OBJ(serverBD.sProp[iProp].netID))
	ENDIF
	
	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_VISIBLE_IN_CUTSCENES)
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(serverBD.sProp[iProp].netID, TRUE, TRUE)
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE_REMAIN_HACK(serverBD.sProp[iProp].netID, TRUE)
		SET_ENTITY_VISIBLE_IN_CUTSCENE(propId, TRUE, TRUE)
	ENDIF
	
	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_DISABLE_COLLISION)
		SET_ENTITY_COLLISION(propId, FALSE)
	ENDIF
	
	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_DISABLE_PROP_LIGHTS)
		SET_ENTITY_LIGHTS(propId, TRUE)
	ENDIF
	
	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_DISABLE_CLIMBING_ON_PROP)
		SET_CAN_CLIMB_ON_ENTITY(propId, FALSE)
	ENDIF
	
	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_DYNAMIC)
		SET_ENTITY_DYNAMIC(propId, TRUE)
	ENDIF
	
	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_BUMP_PROP_ON_SPAWN)
		APPLY_FORCE_TO_ENTITY(propId, APPLY_TYPE_FORCE, << 0.0, 0.0, 1.0>>, <<0.0, 0.0, 0.0>>, 0, FALSE, FALSE, FALSE)
	ENDIF
	
	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_FORCE_LOD_ON_PROP)
		SET_ENTITY_LOD_DIST(NET_TO_OBJ(serverBD.sProp[iProp].netID), 1200)
	ENDIF
	
	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_FREEZE_PROP)
		FREEZE_ENTITY_POSITION(propId, TRUE)
	ENDIF
	
	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_DISABLE_DAMAGE_FROM_OFF_SCRIPT)
		SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(NET_TO_OBJ(serverBD.sProp[iProp].netID), TRUE)
	ENDIF
	
	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_FORCE_COORDS_NO_OFFSET)
		SET_ENTITY_COORDS_NO_OFFSET(propId, GET_MISSION_PROP_COORDS(iProp))
	ELIF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_FORCE_COORDS)
		SET_ENTITY_COORDS(propId, GET_MISSION_PROP_COORDS(iProp))
	ENDIF
	
	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_FORCE_ROTATION)
		SET_ENTITY_ROTATION(propId, GET_MISSION_PROP_ROTATION(iProp))
	ENDIF

	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_SPAWN_INVINCIBLE)
		SET_ENTITY_INVINCIBLE(propId, TRUE)
	ENDIF
	
	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_DISABLE_CARGOBOB_COLLECTION)
		SET_PICK_UP_BY_CARGOBOB_DISABLED(propId, TRUE)
	ENDIF
	
	IF SHOULD_PROP_ONLY_EXIST_FOR_PARTICIPANTS(iProp)
		NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(propId, TRUE)
	ENDIF
	
	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_SPAWN_INVISIBLE)
		SET_ENTITY_VISIBLE(propId, FALSE)
	ENDIF
	
	IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_STATIC_EMITTER)	
		TEXT_LABEL_63 strStaticEmitter, strEmitterRadio
		IF logic.Prop.StaticEmitter != null
		AND CALL logic.Prop.StaticEmitter(iProp, strStaticEmitter, strEmitterRadio)
			LINK_STATIC_EMITTER_TO_ENTITY(strStaticEmitter, propId)			
			SET_STATIC_EMITTER_ENABLED(strStaticEmitter, TRUE)
			SET_EMITTER_RADIO_STATION(strStaticEmitter, strEmitterRadio)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SET_MISSION_PROP_ATTRIBUTES - Static emitter = ", strStaticEmitter, "; Radio station = ", strEmitterRadio, " - Enabled and linked to Prop #", iProp)
		ELSE
			ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SET_MISSION_PROP_ATTRIBUTES - Prop marked as static emitter but no data found in logic.Prop.StaticEmitter. Check logs for more info.")
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] SET_MISSION_PROP_ATTRIBUTES - Prop #", iProp, " marked as static emitter but no data found in logic.Prop.StaticEmitter")
		ENDIF
	ENDIF
	
	IF logic.Prop.Attributes != null
		CALL logic.Prop.Attributes(iProp, propiD)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_ATTACH_PROP_DETACH_ON_DEATH(INT iAttachIndex)
	UNUSED_PARAMETER(iAttachIndex)
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ATTACH_PROP_HAVE_COLLISION(INT iProp)
	UNUSED_PARAMETER(iProp)
	RETURN FALSE
ENDFUNC

#IF MAX_NUM_ATTACH_PROPS
FUNC INT GET_PROP_ATTACH_INDEX(INT iProp)
	INT iAttachIndex
	REPEAT MAX_NUM_ATTACH_PROPS iAttachIndex
		IF data.Prop.AttachedProp[iAttachIndex].iPropIndex = iProp
			RETURN iAttachIndex
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

FUNC NETWORK_INDEX GET_PROP_ATTACH_PARENT_NET_ID(INT iAttachIndex)
	SWITCH data.Prop.AttachedProp[iAttachIndex].eParentType
		CASE ET_VEHICLE			RETURN serverBD.sVehicle[data.Prop.AttachedProp[iAttachIndex].iParentIndex].netId
		CASE ET_OBJECT			RETURN serverBD.sProp[data.Prop.AttachedProp[iAttachIndex].iParentIndex].netId
		CASE ET_PED				RETURN serverBD.sPed[data.Prop.AttachedProp[iAttachIndex].iParentIndex].netId
	ENDSWITCH
	RETURN INT_TO_NATIVE(NETWORK_INDEX, -1)
ENDFUNC

FUNC BOOL ATTACH_PROP_TO_ENTITY(INT iProp, ENTITY_INDEX propId, INT iAttachIndex)

	NETWORK_INDEX attachNetId = GET_PROP_ATTACH_PARENT_NET_ID(iAttachIndex)
	IF DOES_ENTITY_EXIST(propId)
	AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(attachNetId)
		IF TAKE_CONTROL_OF_ENTITY(propId)
			IF TAKE_CONTROL_OF_NET_ID(attachNetId)
				ENTITY_INDEX attachEntity = NET_TO_ENT(attachNetId)
				IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(propId, attachEntity)
					VECTOR vRot = GET_MISSION_PROP_ROTATION(iProp)
					ATTACH_ENTITY_TO_ENTITY(propId, attachEntity, GET_ENTITY_BONE_INDEX_BY_NAME(attachEntity, "chassis_dummy"), data.Prop.AttachedProp[iAttachIndex].vOffset, vRot, CALL logic.Prop.DetachOnDeath(iAttachIndex), FALSE, CALL logic.Prop.AttachCollisionsActive(iProp))
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PROP_", iProp, "] ATTACH_PROP_TO_ENTITY - Attaching ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(propId)), " to entity with vRot = ", vRot)
				ELSE
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PROP_", iProp, "] ATTACH_PROP_TO_ENTITY - ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(propId)), " attached to entity.")
					RETURN TRUE
				ENDIF
			ELSE
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PROP_", iProp, "] ATTACH_PROP_TO_ENTITY - TAKE_CONTROL_OF_NET_ID(attachNetId) = FALSE.")
			ENDIF
		ELSE
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PROP_", iProp, "] ATTACH_PROP_TO_ENTITY - TAKE_CONTROL_OF_ENTITY(propId) = FALSE.")
		ENDIF
	ELSE
		IF NOT DOES_ENTITY_EXIST(propId)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PROP_", iProp, "] ATTACH_PROP_TO_ENTITY - Prop entity doesnt exist.")
		ENDIF
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(attachNetId)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PROP_", iProp, "] ATTACH_PROP_TO_ENTITY - net id to attach to doesnt exist.")
		ENDIF
	ENDIF
		
	RETURN FALSE
	
ENDFUNC

FUNC BOOL SHOULD_ATTACH_PROP_TO_ENTITY(INT iProp)
	RETURN GET_PROP_ATTACH_INDEX(iProp) != -1
ENDFUNC
#ENDIF

FUNC BOOL SHOULD_CREATE_PROP(INT iProp)
	UNUSED_PARAMETER(iProp)
	
	IF NETWORK_IS_PLAYER_IN_MP_CUTSCENE(LOCAL_PLAYER_INDEX)
		PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [PROP_", iProp, "] SHOULD_CREATE_PROP - FALSE - NETWORK_IS_PLAYER_IN_MP_CUTSCENE")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CREATE_MISSION_PROP(INT iProp)
	ENTITY_INDEX propId
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[iProp].netID)
		VECTOR vCoords = GET_MISSION_PROP_COORDS(iProp)
		VECTOR vRotation = GET_MISSION_PROP_ROTATION(iProp)
		
		#IF MAX_NUM_SPAWN_POSITIONS
			// url:bugstar:7523499 - Freemode Creator - Investigate better ways to handle when we want an entity to spawn randomly out of several locations
			// define random spawn index for this spawn
			IF IS_PROP_BIT_SET(iProp, ePROPBITSET_NEED_SPAWN_VARIATION)
				IF serverBD.iEntitySpawnPositionIndex = (-1)
					IF logic.Prop.GetSpawnPositionVariationIndex != NULL
						serverBD.iEntitySpawnPositionIndex = CALL logic.Prop.GetSpawnPositionVariationIndex(iProp)
					ELSE
						serverBD.iEntitySpawnPositionIndex = GET_RANDOM_ENTITY_POSITION_INDEX(iProp, ET_OBJECT)
					ENDIF
				ENDIF
			
				// Use spawn position variations
				IF serverBD.iEntitySpawnPositionIndex > (-1)
					// Random Coord from Random Spawn Position List
					vCoords = data.SpawnPosition.Positions[serverBD.iEntitySpawnPositionIndex].vCoords
					vRotation = data.SpawnPosition.Positions[serverBD.iEntitySpawnPositionIndex].vRotation
				ENDIF
			ENDIF
		#ENDIF
		
		IF IS_VECTOR_ZERO(vCoords)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PROP_", iProp, "] CREATE_MISSION_PROP - GET_MISSION_PROP_COORDS returning zero.")
			RETURN FALSE
		ENDIF
		
		MODEL_NAMES eModel = data.Prop.Props[iProp].model
		IF NOT REQUEST_LOAD_MODEL(eModel)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PROP_", iProp, "] CREATE_MISSION_PROP - Model not loaded yet.")
			RETURN FALSE
		ENDIF
		IF CREATE_NET_OBJ(serverBD.sProp[iProp].netID, eModel, vCoords, DEFAULT, DEFAULT, TRUE, IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_FREEZE_WAITING_ON_COLLISION), TRUE)
			propId = NET_TO_OBJ(serverBD.sProp[iProp].netID)
			SET_ENTITY_ROTATION(propId, vRotation)
			SET_MISSION_PROP_ATTRIBUTES(iProp, propId)
			SET_MODEL_AS_NO_LONGER_NEEDED(eModel)
			#IF MAX_NUM_SPAWN_POSITIONS
			// url:bugstar:7523499 - Freemode Creator - Investigate better ways to handle when we want an entity to spawn randomly out of several locations
			// reset the entiy spawn position index on spawn
			serverBD.iEntitySpawnPositionIndex = -1
			#ENDIF
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PROP_", iProp, "] CREATE_MISSION_PROP - ", GET_MODEL_NAME_FOR_DEBUG(eModel), " created at <<", vCoords, ">> with rotation of <<", vRotation, ">>")
		ENDIF
	ENDIF
	
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[iProp].netID)
		#IF MAX_NUM_ATTACH_PROPS
		INT iAttach = GET_PROP_ATTACH_INDEX(iProp) 
		IF iAttach != -1
		AND data.Prop.AttachedProp[iAttach].iParentIndex != -1
			propId = NET_TO_OBJ(serverBD.sProp[iProp].netID)
			RETURN ATTACH_PROP_TO_ENTITY(iProp, propId, iAttach)
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_DELETE_PROP(INT iProp)
	#IF MAX_NUM_ATTACH_PROPS
	IF SHOULD_ATTACH_PROP_TO_ENTITY(iProp)
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF logic.Prop.ShouldDelete != NULL
		RETURN CALL logic.Prop.ShouldDelete(iProp)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CLEANUP_PROP(INT iProp)
	IF logic.Prop.ShouldCleanup != NULL
		RETURN CALL logic.Prop.ShouldCleanup(iProp)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_PROPS()
	INT iProp
	REPEAT data.Prop.iCount iProp
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[iProp].netID)			
			IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_FADE_ON_END)
				NETWORK_FADE_OUT_ENTITY(NET_TO_ENT(serverBD.sProp[iProp].netId), FALSE, TRUE)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PROP_", iProp, "] CLEANUP_PROPS - Faded out prop.")
			ENDIF
			
			IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_DELETE_ON_END)
				IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sProp[iProp].netID)
					DELETE_NET_ID(serverBD.sProp[iProp].netID)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PROP_", iProp, "] CLEANUP_PROPS - Deleted prop.")
				ENDIF
			ELSE
				CLEANUP_NET_ID(serverBD.sProp[iProp].netID)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL SHOULD_ACTIVATE_PROP(INT iProp)
	
	#IF MAX_NUM_MISSION_INTERIORS
	// Fix for interior props not appearing lit properly
	// Fix for interior props falling through interior objects (because of interior not fully loading)
	IF data.Prop.Props[iProp].iInterior != -1
		INT iInterior = data.Prop.Props[iProp].iInterior
		IF NOT IS_MISSION_INTERIOR_READY(data.Interior.eInteriors[iInterior].eType)
			RETURN FALSE
		ENDIF
	ENDIF
	#ENDIF
	
	IF logic.Prop.Activate != null
		RETURN CALL logic.Prop.Activate(iProp)
	ENDIF
	RETURN TRUE
ENDFUNC

///////////////////////////////////////	
///      PROP SERVER MAINTAIN		 //  				
///////////////////////////////////////

FUNC BOOL IS_PROP_IN_ANY_STORED_MISSION_INTERIOR(INT iProp, ENTITY_INDEX propId)
	IF DOES_ENTITY_EXIST(propId)
		#IF MAX_NUM_ATTACH_PROPS
		INT iAttachIndex = GET_PROP_ATTACH_INDEX(iProp)
		IF iAttachIndex != (-1)
		AND data.Prop.AttachedProp[iAttachIndex].iParentIndex != (-1) 
			SWITCH data.Prop.AttachedProp[iAttachIndex].eParentType
				CASE ET_VEHICLE			RETURN IS_VEHICLE_BIT_SET(data.Prop.AttachedProp[iAttachIndex].iParentIndex, eVEHICLEBITSET_INSIDE_MISSION_INTERIOR)
				CASE ET_PED				RETURN IS_PED_BIT_SET(data.Prop.AttachedProp[iAttachIndex].iParentIndex, ePEDBITSET_INSIDE_MISSION_INTERIOR)
			ENDSWITCH
		ENDIF
		#ENDIF
	ENDIF
	
	RETURN IS_PROP_BIT_SET(iProp, ePROPBITSET_INSIDE_MISSION_INTERIOR)
ENDFUNC

PROC MAINTAIN_PROP_INTERIOR_CHECKS_SERVER(INT iProp, ENTITY_INDEX propId)
	IF data.Prop.Props[iProp].iInterior = -1
		EXIT
	ENDIF
	
	IF GET_PROP_STATE(iProp) > ePROPSTATE_CREATE
	AND GET_PROP_STATE(iProp) < ePROPSTATE_DEAD
		IF IS_PROP_IN_ANY_STORED_MISSION_INTERIOR(iProp, propId)
			SET_PROP_BIT(iProp, ePROPBITSET_INSIDE_MISSION_INTERIOR)
		ELSE
			CLEAR_PROP_BIT(iProp, ePROPBITSET_INSIDE_MISSION_INTERIOR)
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_PROP_SERVER()
	ENTITY_INDEX propId
	BOOL bCreatedThisFrame, bDead, bExists
	
	INT iProp
	REPEAT data.Prop.iCount iProp
		bExists = NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[iProp].netID)
		IF bExists
			propId = NET_TO_ENT(serverBD.sProp[iProp].netID)
			bDead = IS_ENTITY_DEAD(propId)
			
			IF logic.Prop.Server != NULL
				CALL logic.Prop.Server(iProp, propId, bDead)
			ENDIF
		ENDIF
		
		// Prop Cleanup Checks
		IF GET_PROP_STATE(iProp) > ePROPSTATE_CREATE
		AND GET_PROP_STATE(iProp) != ePROPSTATE_DEAD
			IF bExists
				IF bDead
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PROP_", iProp, "] MAINTAIN_PROP_SERVER - SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(propId), " is dead.")
					SET_PROP_STATE(iProp, ePROPSTATE_DEAD)
				ELIF SHOULD_CLEANUP_PROP(iProp)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PROP_", iProp, "] MAINTAIN_PROP_SERVER - SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(propId), " should be cleaned up.")
					SET_PROP_STATE(iProp, ePROPSTATE_DEAD)
				ENDIF
			ELSE
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PROP_", iProp, "] MAINTAIN_PROP_SERVER - SCRIPT_OBJECT_<UNKNOWN> does not have valid net ID.")
				SET_PROP_STATE(iProp, ePROPSTATE_DEAD)
			ENDIF
		ENDIF
		
		MAINTAIN_PROP_INTERIOR_CHECKS_SERVER(iProp, propId)
		
		SWITCH GET_PROP_STATE(iProp)
			CASE ePROPSTATE_INACTIVE
				IF SHOULD_ACTIVATE_PROP(iProp)
					SET_PROP_STATE(iProp, ePROPSTATE_CREATE)
				ENDIF
			BREAK
			
			CASE ePROPSTATE_CREATE
				IF NOT bCreatedThisFrame
				AND SHOULD_CREATE_PROP(iProp)
				AND CREATE_MISSION_PROP(iProp)
					SET_PROP_STATE(iProp, ePROPSTATE_IDLE)
					bCreatedThisFrame = TRUE
				ENDIF
			BREAK
			
			CASE ePROPSTATE_IDLE
	
			BREAK
			
			CASE ePROPSTATE_DEAD
			
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC

///////////////////////////////////////	
///      PROP CLIENT MAINTAIN		 //  				
///////////////////////////////////////

FUNC BOOL SHOULD_BLIP_PROP(INT iProp, BOOL bDead)

	IF bDead
		RETURN FALSE
	ENDIF

	IF NOT bSafeForUI
		RETURN FALSE
	ENDIF

	IF logic.Prop.Blip.Enable != null
		RETURN CALL logic.Prop.Blip.Enable(iProp)
	ENDIF
	
	IF DOES_MODE_STATE_USE_TARGETS(GET_CLIENT_MODE_STATE())
	AND IS_PROP_BIT_SET(iProp, ePROPBITSET_TARGET_PROP)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BLIP_SPRITE GET_PROP_BLIP_SPRITE(INT iProp)
	IF logic.Prop.Blip.Sprite != null
		RETURN CALL logic.Prop.Blip.Sprite(iProp)
	ENDIF
	
	IF GET_CLIENT_MODE_STATE() = eMODESTATE_TAKE_OUT_TARGETS
	AND IS_PROP_BIT_SET(iProp, ePROPBITSET_TARGET_PROP)
	AND NOT IS_DATA_BIT_SET(eDATABITSET_DISABLE_TAKEOUT_TARGET_UI)
		// url:bugstar:7467483 - Freemode Creator - Can we remove the crosshair blip and the bottom right Targets UI being set by default when the player chooses to set something as a Target in the creator.
		// New eDATABITSET for target blip not showing crosshair, but default blip
		RETURN RADAR_TRACE_TEMP_4 // Default Case
	ENDIF
	
	IF GET_CLIENT_MODE_STATE() = eMODESTATE_PROTECT_TARGETS
	AND IS_PROP_BIT_SET(iProp, ePROPBITSET_TARGET_PROP)
		RETURN RADAR_TRACE_INVALID
	ENDIF
	
	RETURN RADAR_TRACE_INVALID
ENDFUNC
FUNC HUD_COLOURS GET_PROP_BLIP_COLOUR(INT iProp)
	IF logic.Prop.Blip.Colour != null
		RETURN CALL logic.Prop.Blip.Colour(iProp)
	ENDIF
	
	IF GET_CLIENT_MODE_STATE() = eMODESTATE_TAKE_OUT_TARGETS
	AND IS_PROP_BIT_SET(iProp, ePROPBITSET_TARGET_PROP)
		RETURN HUD_COLOUR_RED
	ENDIF
	
	IF GET_CLIENT_MODE_STATE() = eMODESTATE_PROTECT_TARGETS
	AND IS_PROP_BIT_SET(iProp, ePROPBITSET_TARGET_PROP)
		RETURN HUD_COLOUR_BLUELIGHT
	ENDIF
	
	RETURN HUD_COLOUR_RED
ENDFUNC
FUNC FLOAT GET_PROP_BLIP_SCALE(INT iProp)
	IF logic.Prop.Blip.Scale != null
		RETURN CALL logic.Prop.Blip.Scale(iProp)
	ENDIF
	
	IF DOES_MODE_STATE_USE_TARGETS(GET_CLIENT_MODE_STATE())
	AND IS_PROP_BIT_SET(iProp, ePROPBITSET_TARGET_PROP)
		RETURN 1.0
	ENDIF

	RETURN 1.0
ENDFUNC
FUNC STRING GET_PROP_BLIP_NAME(INT iProp)
	IF logic.Prop.Blip.Name != null
		RETURN CALL logic.Prop.Blip.Name(iProp)
	ENDIF
	
	IF DOES_MODE_STATE_USE_TARGETS(GET_CLIENT_MODE_STATE())
	AND IS_PROP_BIT_SET(iProp, ePROPBITSET_TARGET_PROP)
		RETURN "PB_TARGET"
	ENDIF
	
	RETURN "BLIP_OBJ"
ENDFUNC
FUNC BOOL IS_PROP_BLIP_SHORT_RANGE(INT iProp)
	IF logic.Prop.Blip.ShortRange != NULL
		RETURN CALL logic.Prop.Blip.ShortRange(iProp)
	ENDIF
	
	RETURN FALSE
ENDFUNC
FUNC BOOL SHOULD_PROP_BLIP_FLASH_ON_CREATION(INT iProp)
	IF logic.Prop.Blip.FlashOnCreation != null
		RETURN CALL logic.Prop.Blip.FlashOnCreation(iProp)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_PROP_BLIP(INT iProp, ENTITY_INDEX propId, BOOL bDead)
	IF bSafeToDisplay 
	AND SHOULD_BLIP_PROP(iProp, bDead)
		HUD_COLOURS eBlipColour = GET_PROP_BLIP_COLOUR(iProp)
		BLIP_SPRITE eBlipSprite = GET_PROP_BLIP_SPRITE(iProp)
		FLOAT fBlipScale = GET_PROP_BLIP_SCALE(iProp)
		BOOL bShortRange = IS_PROP_BLIP_SHORT_RANGE(iProp)
		
		IF NOT DOES_BLIP_EXIST(sPropLocal.sProp[iProp].blip)
			BOOL bShowHeight = logic.Prop.Blip.ShowHeight != null AND CALL logic.Prop.Blip.ShowHeight(iProp)
			
			ADD_ENTITY_BLIP(sPropLocal.sProp[iProp].blip, propId, eBlipSprite, eBlipColour, GET_PROP_BLIP_NAME(iProp), fBlipScale, DEFAULT, bShortRange, SHOULD_PROP_BLIP_FLASH_ON_CREATION(iProp), bShowHeight)
			SET_BLIP_PRIORITY(sPropLocal.sProp[iProp].blip, BLIP_PRIORITY_HIGHEST_SPECIAL_HIGH)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PROP_BLIP - Added blip for sProp[", iProp, "] - SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(propId), " with name ", GET_PROP_BLIP_NAME(iProp)," '", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_PROP_BLIP_NAME(iProp)), "'")
			
		ELSE
			IF eBlipSprite != GET_BLIP_SPRITE(sPropLocal.sProp[iProp].blip)
				IF eBlipSprite != RADAR_TRACE_INVALID
					SET_BLIP_SPRITE(sPropLocal.sProp[iProp].blip, eBlipSprite)
				ENDIF
				SET_BLIP_SCALE(sPropLocal.sProp[iProp].blip, fBlipScale)
				SET_BLIP_NAME_FROM_TEXT_FILE(sPropLocal.sProp[iProp].blip, GET_PROP_BLIP_NAME(iProp))
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PROP_BLIP - Updated blip sprite for sProp[", iProp, "] - SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(propId), " with name ", GET_PROP_BLIP_NAME(iProp), " '", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_PROP_BLIP_NAME(iProp)), "'.")
			ENDIF
			
			IF bShortRange != IS_BLIP_SHORT_RANGE(sPropLocal.sProp[iProp].blip)
				SET_BLIP_AS_SHORT_RANGE(sPropLocal.sProp[iProp].blip, bShortRange)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PROP_BLIP - Updated blip short range for sProp[", iProp, "] - SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(propId), " with name ", GET_PROP_BLIP_NAME(iProp), " '", GET_FILENAME_FOR_AUDIO_CONVERSATION(GET_PROP_BLIP_NAME(iProp)), "'.")
			ENDIF
			
			IF GET_BLIP_COLOUR_FROM_HUD_COLOUR(eBlipColour) != GET_BLIP_COLOUR(sPropLocal.sProp[iProp].blip)
				SET_BLIP_COLOUR_FROM_HUD_COLOUR(sPropLocal.sProp[iProp].blip, eBlipColour)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PROP_BLIP - Updated blip colour for sProp[", iProp, "] - SCRIPT_OBJECT_", NETWORK_ENTITY_GET_OBJECT_ID(propId))
			ENDIF
			
			IF IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_BLIP_FADE_BASED_ON_DISTANCE)
				SET_BLIP_ALPHA_BASED_ON_DIST_FROM_PLAYER(sPropLocal.sProp[iProp].blip, 100, 5000)
			ENDIF
			
			IF logic.Prop.Blip.ConstantFlash != null
			AND CALL logic.Prop.Blip.ConstantFlash(iProp)
				IF NOT IS_BLIP_FLASHING(sPropLocal.sProp[iProp].blip)
					FLASH_MISSION_BLIP(sPropLocal.sProp[iProp].blip)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(sPropLocal.sProp[iProp].blip)
			REMOVE_BLIP(sPropLocal.sProp[iProp].blip)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PROP_BLIP - Removed blip for sProp[", iProp, "] - SCRIPT_Prop_", NETWORK_ENTITY_GET_OBJECT_ID(propId))
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PROP_BE_DAMAGEABLE(INT iProp)
	IF IS_PROP_CLIENT_BIT_SET(iProp, LOCAL_PARTICIPANT_INDEX, ePROPCLIENTBITSET_MADE_PROP_DESTROYABLE)
		RETURN FALSE
	ENDIF

	IF logic.Prop.Damageable != null
	AND CALL logic.Prop.Damageable(iProp)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
PROC MAINTAIN_PROP_DAMAGEABLE(INT iProp, ENTITY_INDEX propId)
	IF NOT SHOULD_PROP_BE_DAMAGEABLE(iProp)
		EXIT
	ENDIF

	IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sProp[iProp].netId)
		SET_ENTITY_INVINCIBLE(propId, FALSE)							
		SET_PROP_CLIENT_BIT(iProp, ePROPCLIENTBITSET_MADE_PROP_DESTROYABLE)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_PROP_EXPLODE(INT iProp)
	IF IS_PROP_CLIENT_BIT_SET(iProp, LOCAL_PARTICIPANT_INDEX, ePROPCLIENTBITSET_PROP_EXPLODED)
	OR IS_PROP_BIT_SET(iProp, ePROPBITSET_PROP_EXPLODED)
		RETURN FALSE
	ENDIF

	IF logic.Prop.Explosion.ShouldExplode != NULL
	AND CALL logic.Prop.Explosion.ShouldExplode(iProp)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC EXPLOSION_TAG PROP_EXPLOSION_TAG(INT iProp)

	IF logic.Prop.Explosion.Tag != NULL
		RETURN CALL logic.Prop.Explosion.Tag(iProp)
	ENDIF

	RETURN EXP_TAG_HI_OCTANE
ENDFUNC

FUNC FLOAT PROP_EXPLOSION_SCALE(INT iProp)

	IF logic.Prop.Explosion.Scale != NULL
		RETURN CALL logic.Prop.Explosion.Scale(iProp)
	ENDIF

	RETURN 1.0
ENDFUNC

PROC MAINTAIN_PROP_EXPLOSION(INT iProp, ENTITY_INDEX propId)
	IF NOT SHOULD_PROP_EXPLODE(iProp)
		EXIT
	ENDIF

	IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sProp[iProp].netId)
		SET_ENTITY_PROOFS(propId, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
		SET_ENTITY_INVINCIBLE(propId, FALSE)
		SET_ENTITY_CAN_BE_DAMAGED(propId, TRUE)
		SET_ENTITY_HEALTH(propId, 0)
		
		ADD_EXPLOSION(GET_ENTITY_COORDS(propId), PROP_EXPLOSION_TAG(iProp), PROP_EXPLOSION_SCALE(iProp))			
		SET_PROP_CLIENT_BIT(iProp, ePROPCLIENTBITSET_PROP_EXPLODED)
	ENDIF
ENDPROC

PROC MAINTAIN_TARGET_PROP(INT iProp, ENTITY_INDEX propId)
	IF IS_PROP_BIT_SET(iProp, ePROPBITSET_TARGET_PROP)
		IF DOES_MODE_STATE_USE_TARGETS(GET_CLIENT_MODE_STATE())
			DRAW_TAKE_OUT_TARGET_ENTITY_MARKER(eENTITYTYPE_PROP, iProp, propId)
		ENDIF
		INCREMENT_TAKE_OUT_TARGETS_REMAINING()
	ENDIF
ENDPROC

FUNC FLOAT GET_PLAYER_NEAR_PROP_RANGE(INT iProp)
	IF logic.Prop.NearRange != NULL
		RETURN CALL logic.Prop.NearRange(iProp)
	ENDIF
	
	RETURN 10.0
ENDFUNC

FUNC BOOL SHOULD_PLAYER_NEAR_PROP_CHECK_LOS(INT iProp)
	IF logic.Prop.CheckLOSNear != NULL
		RETURN CALL logic.Prop.CheckLOSNear(iProp)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_NEAR_PROP_CHECK_ON_SCREEN(INT iProp)
	IF logic.Prop.CheckOnScreenNear != NULL
		RETURN CALL logic.Prop.CheckOnScreenNear(iProp)
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_PLAYER_NEAR_PROP(INT iProp, ENTITY_INDEX entityId)
	IF NOT IS_PROP_DATA_BIT_SET(iProp, ePROPDATABITSET_CHECK_PLAYER_NEAR)
		EXIT
	ENDIF
	
	FLOAT fRange = GET_PLAYER_NEAR_PROP_RANGE(iProp)
	
	IF fRange = -1.0
		EXIT
	ENDIF
	
	IF NOT IS_PROP_CLIENT_BIT_SET(iProp, LOCAL_PARTICIPANT_INDEX, ePROPCLIENTBITSET_PLAYER_IS_NEAR)
		IF IS_PLAYER_NEAR_ENTITY(entityId, fRange, SHOULD_PLAYER_NEAR_PROP_CHECK_LOS(iProp), SHOULD_PLAYER_NEAR_PROP_CHECK_ON_SCREEN(iProp))
			SET_PROP_CLIENT_BIT(iProp, ePROPCLIENTBITSET_PLAYER_HAS_BEEN_NEAR)
			SET_PROP_CLIENT_BIT(iProp, ePROPCLIENTBITSET_PLAYER_IS_NEAR)
			
			IF logic.Prop.OnNearChanged != NULL
				CALL logic.Prop.OnNearChanged(iProp, TRUE)
			ENDIF
			
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PLAYER_NEAR_PROP - Player is within ", fRange, "m of iProp #", iProp)
		ENDIF
	ELSE
		IF NOT IS_PLAYER_NEAR_ENTITY(entityId, fRange, SHOULD_PLAYER_NEAR_PROP_CHECK_LOS(iProp), SHOULD_PLAYER_NEAR_PROP_CHECK_ON_SCREEN(iProp))
			CLEAR_PROP_CLIENT_BIT(iProp, ePROPCLIENTBITSET_PLAYER_IS_NEAR)
			
			IF logic.Prop.OnNearChanged != NULL
				CALL logic.Prop.OnNearChanged(iProp, FALSE)
			ENDIF
			
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PLAYER_NEAR_PROP - Player is no longer within ", fRange, "m of iProp #", iProp)
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC MAINTAIN_PROP_DEBUG_CLIENT(INT iProp, ENTITY_INDEX propId, BOOL bDead)
	IF NOT bDead
		IF IS_PROP_BIT_SET(iProp, ePROPBITSET_DEBUG_KILL)
			IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sProp[iProp].netId)
				SET_ENTITY_HEALTH(propId, 0)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL SHOULD_DRAW_PROP_MARKER(INT iProp, ENTITY_INDEX propId)
	IF logic.Prop.Blip.DrawMarker != null
		RETURN CALL logic.Prop.Blip.DrawMarker(iProp, propId)
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_PROP_MARKER_SCALE(INT iProp)
	IF logic.Prop.Blip.MarkerScale != null
		RETURN CALL logic.Prop.Blip.MarkerScale(iProp)
	ENDIF
	RETURN 0.5
ENDFUNC

FUNC HUD_COLOURS GET_PROP_MARKER_COLOUR(INT iProp)
	IF logic.Prop.Blip.Colour != null
		RETURN CALL logic.Prop.Blip.Colour(iProp)
	ENDIF
	RETURN HUD_COLOUR_GREEN
ENDFUNC

PROC MAINTAIN_PROP_MARKER(INT iProp, ENTITY_INDEX propId)
	IF SHOULD_DRAW_PROP_MARKER(iProp, propId)
		DRAW_GENERIC_ENTITY_MARKER(eENTITYTYPE_PROP, iProp, propId, GET_PROP_MARKER_COLOUR(iProp), GET_PROP_MARKER_SCALE(iProp))
	ENDIF
ENDPROC

// Fixes the lighting issue on props
#IF MAX_NUM_MISSION_INTERIORS
PROC MAINTAIN_PROP_INTERIOR_FIX(INT iProp, ENTITY_INDEX propId)
	IF IS_LOCAL_ALIVE
	AND IS_PROP_BIT_SET(iProp, ePROPBITSET_INSIDE_MISSION_INTERIOR)
	AND IS_FMC_INTERIOR_A_WARP_INTERIOR(data.Interior.eInteriors[data.Prop.Props[iProp].iInterior].eType)
	AND NOT IS_PROP_IN_SAME_INTERIOR_AS_PLAYER(iProp)
		SET_ENTITY_LOCALLY_INVISIBLE(propId)
	ENDIF
ENDPROC
#ENDIF

// --- Prop animation maintain ---
// url:bugstar:7896794 - Framework - Implement animation maintain for props

#IF MAX_NUM_PROP_ANIMATIONS

FUNC INT GET_PROP_ANIMATIONS_NUM_STAGES(INT iProp)
	UNUSED_PARAMETER(iProp)
	RETURN 1
ENDFUNC

FUNC FLOAT GET_PROP_ANIMATIONS_BLEND_IN(INT iProp, INT iStage)
	UNUSED_PARAMETER(iProp)
	UNUSED_PARAMETER(iStage)
	RETURN NORMAL_BLEND_IN
ENDFUNC

FUNC FLOAT GET_PROP_ANIMATIONS_BLEND_OUT(INT iProp, INT iStage)
	UNUSED_PARAMETER(iProp)
	UNUSED_PARAMETER(iStage)
	RETURN NORMAL_BLEND_OUT
ENDFUNC

FUNC FLOAT GET_PROP_ANIMATIONS_START_DISTANCE(INT iProp)
	UNUSED_PARAMETER(iProp)
	RETURN 250.0
ENDFUNC

FUNC BOOL SHOULD_PROP_ANIMATIONS_LOOP(INT iProp, INT iStage)
	UNUSED_PARAMETER(iProp)
	UNUSED_PARAMETER(iStage)
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_PROP_ANIMATIONS_EXIT_PHASE(INT iProp, INT iStage)
	UNUSED_PARAMETER(iProp)
	UNUSED_PARAMETER(iStage)
	RETURN 0.95
ENDFUNC

FUNC BOOL SHOULD_PROP_ANIMATIONS_HOLD_LAST(INT iProp)
	UNUSED_PARAMETER(iProp)
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PROP_ANIMATION_REGISTERED(INT iProp)

	INT iIterator
	REPEAT MAX_NUM_PROP_ANIMATIONS iIterator
		IF iProp = sPropLocal.sPropAnim[iIterator].iProp
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE

ENDFUNC

PROC PROP_ANIMATIONS_PERFORM_PROP_ANIM_SYNC_SCENE(INT iPropAnim, BOOL bLoopAnim = FALSE, BOOL bHoldLastFrame = FALSE)

	INT iProp = sPropLocal.sPropAnim[iPropAnim].iProp
	ENTITY_INDEX propId = sPropLocal.sPropAnim[iPropAnim].propId
	INT iPhase = sPropLocal.sPropAnim[iPropAnim].iPhase

	TEXT_LABEL_63 sAnimDict = CALL logic.Prop.Animation.AnimDict(iProp, iPhase)
	TEXT_LABEL_63 sAnim = CALL logic.Prop.Animation.Anim(iProp, iPhase)
	
	FLOAT fBlendIn = CALL logic.Prop.Animation.BlendIn(iProp, iPhase)
	FLOAT fBlendOut = CALL logic.Prop.Animation.BlendOut(iProp, iPhase)
	
	VECTOR vSyncSceneCoords = GET_ENTITY_COORDS(propId)
	VECTOR vSyncSceneOrientation = <<0, 0, GET_ENTITY_HEADING(propId)>>
	
	sPropLocal.sPropAnim[iPropAnim].iPropAnimSyncScene = NETWORK_CREATE_SYNCHRONISED_SCENE(vSyncSceneCoords, vSyncSceneOrientation, DEFAULT, bHoldLastFrame, bLoopAnim)
	
	NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(propId, sPropLocal.sPropAnim[iPropAnim].iPropAnimSyncScene, sAnimDict, sAnim, fBlendIn, fBlendOut, SYNCED_SCENE_ON_ABORT_STOP_SCENE)
	
	PLAY_ENTITY_ANIM(propId, sAnim, sAnimDict, fBlendIn, bLoopAnim, bHoldLastFrame, FALSE, 0)
	FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(propId)
	
	NETWORK_START_SYNCHRONISED_SCENE(sPropLocal.sPropAnim[iPropAnim].iPropAnimSyncScene)
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] PROP_ANIMATIONS_PERFORM_PROP_ANIM_SYNC_SCENE, vSyncSceneCoords = ", vSyncSceneCoords, " sAnim = ", sAnim, " sAnimDict = ", sAnimDict)
	#ENDIF
	
ENDPROC

PROC STOP_PROP_ANIMATIONS_CURRENT_ANIM_SYNCHED_SCENE()

	INT iLocalSceneID
	iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iLocalSceneID)
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
		
		DETACH_SYNCHRONIZED_SCENE(iLocalSceneID)
		NETWORK_STOP_SYNCHRONISED_SCENE(iLocalSceneID)
		iLocalSceneID = -1
		
		#IF IS_DEBUG_BUILD
		PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] STOP_PROP_ANIMATIONS_CURRENT_ANIM_SYNCHED_SCENE - called")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HAS_PROP_ANIMATIONS_CLIP_FINISHED(INT iPropAnim, BOOL bStopAnim = FALSE)
	
	PED_INDEX pedToCheck = LOCAL_PED_INDEX

	IF IS_ENTITY_ALIVE(pedToCheck)
		FLOAT fExitAnimPhase = CALL logic.Prop.Animation.ExitPhase(sPropLocal.sPropAnim[iPropAnim].iProp, sPropLocal.sPropAnim[iPropAnim].iPhase)
		INT iLocalSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sPropLocal.sPropAnim[iPropAnim].iPropAnimSyncScene)
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iLocalSceneID)
		AND GET_SYNCHRONIZED_SCENE_PHASE(iLocalSceneID) >= fExitAnimPhase

			IF bStopAnim
				STOP_PROP_ANIMATIONS_CURRENT_ANIM_SYNCHED_SCENE()
			ENDIF	
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEANUP_PROP_ANIMATION(INT iPropAnim, BOOL bFinal)
		
	IF bFinal
	AND logic.Prop.Animation.AnimDict != NULL
		STRING sAnimDict = CALL logic.Prop.Animation.AnimDict(sPropLocal.sPropAnim[iPropAnim].iProp, sPropLocal.sPropAnim[iPropAnim].iPhase)
		IF NOT IS_STRING_NULL_OR_EMPTY(sAnimDict)
			REMOVE_ANIM_DICT(sAnimDict)
		ENDIF
	ENDIF
	
	IF sPropLocal.sPropAnim[iPropAnim].iPhase > 0
	OR bFinal
		STOP_PROP_ANIMATIONS_CURRENT_ANIM_SYNCHED_SCENE()
		sPropLocal.sPropAnim[iPropAnim].iPropAnimSyncScene = -1
		sPropLocal.sPropAnim[iPropAnim].iPhase = 0
	ENDIF
	
	PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [CLEANUP_PROP_ANIMATION] ", iPropAnim)
	
ENDPROC

PROC CLEANUP_PROP_ANIMATIONS()

	INT iIterator
	REPEAT MAX_NUM_PROP_ANIMATIONS iIterator
		IF sPropLocal.sPropAnim[iIterator].iProp > -1
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] [CLEANUP_PROP_ANIMATIONS] iProp = ", sPropLocal.sPropAnim[iIterator].iProp, " - removed.")
			CLEANUP_PROP_ANIMATION(sPropLocal.sPropAnim[iIterator].iProp, FALSE)
			sPropLocal.sPropAnim[iIterator].iProp = -1
			sPropLocal.sPropAnim[iIterator].propId = NULL
			sPropLocal.sPropAnim[iIterator].iPropAnimSyncScene = -1
			sPropLocal.sPropAnim[iIterator].iPhase = 0
		ENDIF
	ENDREPEAT

ENDPROC

PROC PROP_ANIMATIONS_REGISTER_PROP(INT iProp, ENTITY_INDEX propId)
	
	INT iIterator
	REPEAT MAX_NUM_PROP_ANIMATIONS iIterator
		IF sPropLocal.sPropAnim[iIterator].iProp = -1
			sPropLocal.sPropAnim[iIterator].iProp = iProp
			sPropLocal.sPropAnim[iIterator].propId = propId
			sPropLocal.sPropAnim[iIterator].iPropAnimSyncScene = -1
			sPropLocal.sPropAnim[iIterator].iPhase = 0
			EXIT
		ENDIF
	ENDREPEAT	
	
	ASSERTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] PROP_ANIMATIONS_REGISTER_PROP, iProp = ", iProp, " - array filled. MAX_NUM_PROP_ANIMATIONS = ", MAX_NUM_PROP_ANIMATIONS)	
	
ENDPROC

PROC PROP_ANIMATIONS_UNREGISTER_PROP(INT iProp)

	INT iIterator
	REPEAT MAX_NUM_PROP_ANIMATIONS iIterator
		IF iProp = sPropLocal.sPropAnim[iIterator].iProp
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] PROP_ANIMATIONS_UNREGISTER_PROP, iProp = ", iProp, " - removed.")
			CLEANUP_PROP_ANIMATION(iProp, FALSE)
			sPropLocal.sPropAnim[iIterator].iProp = -1
			sPropLocal.sPropAnim[iIterator].propId = NULL
			sPropLocal.sPropAnim[iIterator].iPropAnimSyncScene = -1
			sPropLocal.sPropAnim[iIterator].iPhase = 0
			EXIT
		ENDIF
	ENDREPEAT
	
ENDPROC

// --- Maintain ---

PROC MAINTAIN_PROP_ANIMATIONS(INT iPropAnim)
	
	// Check prop iProp
	INT iProp = sPropLocal.sPropAnim[iPropAnim].iProp
	
	IF iProp < 0 OR iProp >= MAX_NUM_PROPS
		PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PROP_ANIMATIONS iPropAnim = ", iPropAnim, " - iProp = ", iProp, " - out of range.")
		EXIT
	ENDIF
	
	// Check if prop has anim dict and animation is setuped
	STRING sAnimDict, sAnim
	
	IF logic.Prop.Animation.AnimDict != NULL
	AND logic.Prop.Animation.Anim != NULL
	
		sAnimDict = CALL logic.Prop.Animation.AnimDict(iProp, sPropLocal.sPropAnim[iPropAnim].iPhase)
		sAnim = CALL logic.Prop.Animation.Anim(iProp, sPropLocal.sPropAnim[iPropAnim].iPhase)
		
		IF IS_STRING_NULL_OR_EMPTY(sAnimDict)
		OR IS_STRING_NULL_OR_EMPTY(sAnim)
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PROP_ANIMATIONS iPropAnim = ", iPropAnim, " - iProp = ", iProp, " iPhase = ",  sPropLocal.sPropAnim[iPropAnim].iPhase, " - sAnimDict = ", sAnimDict, " or sAnim = ", sAnim, " not defined.")
			EXIT
		ELSE
			REQUEST_ANIM_DICT(sAnimDict)
			IF NOT HAS_ANIM_DICT_LOADED(sAnimDict)
				PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PROP_ANIMATIONS iPropAnim = ", iPropAnim, " - iProp = ", iProp, " - sAnimDict = ", sAnimDict, " not loaded.")
				EXIT
			ENDIF
		ENDIF
	ELSE
		EXIT
	ENDIF
	
	// Try animate prop
	IF NETWORK_DOES_NETWORK_ID_EXIST(serverBD.sProp[iProp].netID)
	AND MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sProp[iProp].netID)
		
		// Check propID
		ENTITY_INDEX propId = sPropLocal.sPropAnim[iPropAnim].propId
		
		// Check if Prop is dead
		IF IS_ENTITY_ALIVE(propId)
		AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), propId) < CALL logic.Prop.Animation.StartDistance(iProp)
		
			#IF IS_DEBUG_BUILD
			VECTOR vPos = GET_ENTITY_COORDS(propId, FALSE)
			FLOAT fReturnZ
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PROP_ANIMATIONS, vPos = ", vPos, " | iProp = ", iProp, " | iPhase = ", ENUM_TO_INT(sPropLocal.sPropAnim[iPropAnim].iPhase))
			IF NOT GET_GROUND_Z_FOR_3D_COORD(vPos, fReturnZ)
				PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PROP_ANIMATIONS, GET_GROUND_Z_FOR_3D_COORD, FALSE, fReturnZ = ", fReturnZ, " | iProp = ", iProp)
			ENDIF
			#ENDIF
			
			IF sPropLocal.sPropAnim[iPropAnim].iPhase = 0
				// Start Stage 0
				PROP_ANIMATIONS_PERFORM_PROP_ANIM_SYNC_SCENE(iPropAnim, CALL logic.Prop.Animation.Loop(iProp, sPropLocal.sPropAnim[iPropAnim].iPhase))
				++sPropLocal.sPropAnim[iPropAnim].iPhase
				PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PROP_ANIMATIONS - iProp = ", iProp, " - Start Animation - Next Stage sPropLocal.sPropAnim[iPropAnim].iPhase = ", sPropLocal.sPropAnim[iPropAnim].iPhase)
				
			ELIF HAS_PROP_ANIMATIONS_CLIP_FINISHED(iProp, FALSE)
			
				IF sPropLocal.sPropAnim[iPropAnim].iPhase + 1 < CALL logic.Prop.Animation.NumStages(iProp)
					// Continue Stages till the end
					PROP_ANIMATIONS_PERFORM_PROP_ANIM_SYNC_SCENE(iPropAnim, CALL logic.Prop.Animation.Loop(iProp, sPropLocal.sPropAnim[iPropAnim].iPhase))
					++sPropLocal.sPropAnim[iPropAnim].iPhase
					PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PROP_ANIMATIONS - iProp = ", iProp, " - Next Animation - Next Stage sPropLocal.sPropAnim[iPropAnim].iPhase = ", sPropLocal.sPropAnim[iPropAnim].iPhase)
				ELIF logic.Prop.Animation.HoldLastAnimation != NULL
				AND CALL logic.Prop.Animation.HoldLastAnimation(iProp)
					// End - Hold animation
					PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PROP_ANIMATIONS - iProp = ", iProp, " - Finish Animation Stages - Hold")
					SET_PROP_CLIENT_BIT(iProp, ePROPCLIENTBITSET_PLAYED_PROP_ANIM)
				ELSE
					// End - Clean up after last animation
					PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PROP_ANIMATIONS - iProp = ", iProp, " - Finish Animation Stages - Cleanup")
					SET_PROP_CLIENT_BIT(iProp, ePROPCLIENTBITSET_PLAYED_PROP_ANIM)
					CLEANUP_PROP_ANIMATION(iPropAnim, FALSE)
				ENDIF
				
			ENDIF
		ELSE
			PRINTLN("[",scriptName,"] [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_PROP_ANIMATIONS - iProp = ", iProp, " CLEANUP_PROP_ANIMATION - Out of Animation Range")
			CLEANUP_PROP_ANIMATION(iPropAnim, FALSE)
		ENDIF
		
	ENDIF
	
ENDPROC

PROC PROP_ANIMATIONS_MANAGE_REGISTRATION(INT iProp, ENTITY_INDEX propId, BOOL bDead)

	// Check if prop is allowed to animate
	IF logic.Prop.Animation.ShouldAnimate != NULL
		IF NOT bDead
		AND CALL logic.Prop.Animation.ShouldAnimate(iProp)
		 	IF NOT IS_PROP_ANIMATION_REGISTERED(iProp)
				PROP_ANIMATIONS_REGISTER_PROP(iProp, propId)
			ENDIF
		ELSE
			IF IS_PROP_ANIMATION_REGISTERED(iProp)
				PROP_ANIMATIONS_UNREGISTER_PROP(iProp)
			ENDIF
		ENDIF	
	ENDIF

ENDPROC

#ENDIF

PROC MAINTAIN_PROP_CLIENT()
	ENTITY_INDEX propId
	BOOL bExists, bDead
	
	INT iProp
	REPEAT data.Prop.iCount iProp
		bExists = NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[iProp].netId)
		IF bExists
			propId = NET_TO_ENT(serverBD.sProp[iProp].netId)
			bDead = IS_ENTITY_DEAD(propId)
			
			#IF IS_DEBUG_BUILD
			MAINTAIN_PROP_DEBUG_CLIENT(iProp, propId, bDead)
			#ENDIF
			
			IF logic.Prop.Client != NULL
				CALL logic.Prop.Client(iProp, propId, bDead)
			ENDIF
			
			MAINTAIN_PROP_EXPLOSION(iProp, propId)
			MAINTAIN_PROP_BLIP(iProp, propId, bDead)
			MAINTAIN_PLAYER_NEAR_PROP(iProp, propId)
			
			#IF MAX_NUM_PROP_ANIMATIONS
			PROP_ANIMATIONS_MANAGE_REGISTRATION(iProp, propId, bDead)
			#ENDIF		
			
			SWITCH GET_PROP_STATE(iProp)
				CASE ePROPSTATE_IDLE
					MAINTAIN_PROP_DAMAGEABLE(iProp, propId)
					
					IF NOT bDead
						MAINTAIN_TARGET_PROP(iProp, propId)	
						MAINTAIN_PROP_MARKER(iProp, propId)
						#IF MAX_NUM_MISSION_INTERIORS			MAINTAIN_PROP_INTERIOR_FIX(iProp, propId)			#ENDIF
					ENDIF
				BREAK
				
				CASE ePROPSTATE_DEAD
					IF SHOULD_DELETE_PROP(iProp)
						IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sProp[iProp].netId)
							DELETE_NET_ID(serverBD.sProp[iProp].netID)
						ENDIF
					ELIF SHOULD_CLEANUP_PROP(iProp)
						IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sProp[iProp].netID)
							CLEANUP_NET_ID(serverBD.sProp[iProp].netID)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDREPEAT
	
	#IF MAX_NUM_PROP_ANIMATIONS 
	// url:bugstar:7896794 - Framework - Implement animation maintain for props
	INT iPropAnim
	REPEAT MAX_NUM_PROP_ANIMATIONS iPropAnim
		MAINTAIN_PROP_ANIMATIONS(iPropAnim) 			
	ENDREPEAT
	#ENDIF
	
ENDPROC
