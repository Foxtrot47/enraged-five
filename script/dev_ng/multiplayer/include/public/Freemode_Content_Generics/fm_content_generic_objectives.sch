USING "fm_content_generic_support.sch"

//----------------------
//	Go to Point
//----------------------

#IF NOT DEFINED(SHOULD_GO_TO_PROGRESS_WHEN_IN_RANGE)
FUNC BOOL SHOULD_GO_TO_PROGRESS_WHEN_IN_RANGE()
	RETURN TRUE
ENDFUNC
#ENDIF

#IF NOT DEFINED(SHOULD_CLEAR_GO_TO_POINT)
FUNC BOOL SHOULD_CLEAR_GO_TO_POINT()
	RETURN FALSE
ENDFUNC
#ENDIF

#IF NOT DEFINED(SHOW_INTERIOR_BLIP_GO_TO_POINT)
FUNC BOOL SHOW_INTERIOR_BLIP_GO_TO_POINT(INT iGoToPoint)
	unused_parameter(iGoToPoint)
	RETURN FALSE
ENDFUNC
#ENDIF

#IF MAX_NUM_GOTO_LOCATIONS
FUNC BOOL IS_SERVER_GOTO_REACHED_SET()
	RETURN IS_GENERIC_SERVER_BIT_SET(eGENERICSERVERBITSET_REACHED_GOTO_LOCATION)
ENDFUNC

PROC SET_SERVER_GOTO_REACHED()

	IF GET_MODE_STATE() != eMODESTATE_GO_TO_POINT
		EXIT
	ENDIF

	SET_GENERIC_SERVER_BIT(eGENERICSERVERBITSET_REACHED_GOTO_LOCATION)
	
ENDPROC

FUNC FLOAT GET_GO_TO_POINT_RADIUS()
	IF logic.GoToPoint.Radius != null
	AND CALL logic.GoToPoint.Radius(GET_CURRENT_GO_TO_POINT()) != (-1.0)
		RETURN CALL logic.GoToPoint.Radius(GET_CURRENT_GO_TO_POINT())
	ENDIF	
	RETURN data.GoToPoint.Locations[GET_CURRENT_GO_TO_POINT()].fRadius
ENDFUNC

FUNC BOOL SHOULD_PROGRESS_GO_TO_POINT()
	IF logic.GoToPoint.ShouldProgress != NULL
		RETURN CALL logic.GoToPoint.ShouldProgress(GET_CURRENT_GO_TO_POINT())
	ENDIF
	
	IF IS_GENERIC_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX, eGENERICCLIENTBITSET_I_AM_AT_GOTO_LOCATION)
		RETURN TRUE
	ENDIF
	
	IF IS_GO_TO_POINT_DATA_BIT_SET(GET_CURRENT_GO_TO_POINT(),eGOTOPOINTDATABITSET_PASS_ON_ENTERING_ZANCUDO)
		IF IS_PLAYER_IN_ZANCUDO_ARMY_BASE(LOCAL_PLAYER_INDEX)
			RETURN TRUE
		ENDIF
	ENDIF
	
	// url:bugstar:7467709 - Freemode Creator - Can we add a Pass at LSIA option to the Goto Location menu please.
	IF IS_GO_TO_POINT_DATA_BIT_SET(GET_CURRENT_GO_TO_POINT(),eGOTOPOINTDATABITSET_PASS_ON_ENTERING_LSIA)
		IF IS_PLAYER_IN_LSIA(LOCAL_PED_INDEX)
			RETURN TRUE
		ENDIF
	ENDIF

	// If the variation doesn't have a go to, just pass through
	IF data.GoToPoint.Locations[GET_CURRENT_GO_TO_POINT()].fRadius = (-1.0)
		PRINTLN("[",scriptName,"][GB_MISSION_GENERICS] - SHOULD_PROGRESS_GO_TO_POINT - TRUE - GET_CURRENT_GO_TO_POINT = ", GET_CURRENT_GO_TO_POINT(), ", fRadius is -1")
		RETURN TRUE
	ENDIF
	
	// If within range of the go to, move on
	IF SHOULD_GO_TO_PROGRESS_WHEN_IN_RANGE()
		IF fMyDistanceFromGoToLocation != -1
			IF fMyDistanceFromGoToLocation <= GET_GO_TO_POINT_RADIUS()
				PRINTLN("[",scriptName,"][GB_MISSION_GENERICS] - SHOULD_PROGRESS_GO_TO_POINT - TRUE - GET_CURRENT_GO_TO_POINT = ", GET_CURRENT_GO_TO_POINT(), ", fMyDistanceFromGoToLocation = ", fMyDistanceFromGoToLocation, " fRadius = ", data.GoToPoint.Locations[GET_CURRENT_GO_TO_POINT()].fRadius)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_CHECK_2D_DISTANCE_FOR_GO_TO_POINT()
	IF logic.GoToPoint.Check2dDistance != null
		RETURN CALL logic.GoToPoint.Check2dDistance(GET_CURRENT_GO_TO_POINT())
	ENDIF	
	RETURN FALSE
ENDFUNC

#IF NOT DEFINED(MAINTAIN_GO_TO_POINT)
PROC MAINTAIN_GO_TO_POINT()
	
	IF SHOULD_CHECK_2D_DISTANCE_FOR_GO_TO_POINT()
		fMyDistanceFromGoToLocation = VDIST_2D(LOCAL_PLAYER_COORD, data.GoToPoint.Locations[GET_CURRENT_GO_TO_POINT()].vCoords)
	ELSE
		fMyDistanceFromGoToLocation = VDIST(LOCAL_PLAYER_COORD, data.GoToPoint.Locations[GET_CURRENT_GO_TO_POINT()].vCoords)
	ENDIF
	
	IF GET_CLIENT_MODE_STATE() = eMODESTATE_GO_TO_POINT
		MAINTAIN_GO_TO_POINT_UI()
		IF SHOULD_PROGRESS_GO_TO_POINT()
			SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_I_AM_AT_GOTO_LOCATION)	
		ENDIF
	ELSE
		IF SHOULD_CLEAR_GO_TO_POINT()
			CLEAR_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_I_AM_AT_GOTO_LOCATION)	
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

#IF NOT DEFINED(SHOULD_GO_TO_POINT_PROGRESS_ON_PED_REACTION)
FUNC BOOL SHOULD_GO_TO_POINT_PROGRESS_ON_PED_REACTION()

	INT iGroup
	REPEAT COUNT_OF(data.Ped.Group) iGroup
		IF data.Ped.Group[iGroup].iGoto = GET_CURRENT_GO_TO_POINT()
		AND HAS_PED_GROUP_BEEN_TRIGGERED(iGroup)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE

ENDFUNC
#ENDIF

#IF NOT DEFINED(SHOULD_GO_TO_POINT_PROGRESS_ON_COLLECTION)
FUNC BOOL SHOULD_GO_TO_POINT_PROGRESS_ON_COLLECTION()
	RETURN FALSE
ENDFUNC
#ENDIF
#ENDIF

PROC SETUP_GOTOS()
	#IF MAX_NUM_GOTO_LOCATIONS
	IF logic.GoToPoint.PointId != NULL
	AND CALL logic.GoToPoint.PointId(GET_CLIENT_MODE_STATE_ID()) != -1
		SET_CURRENT_GO_TO_POINT(CALL logic.GoToPoint.PointId(GET_CLIENT_MODE_STATE_ID()))
	ENDIF
	
	INT iGoTo
	INT iState
	REPEAT iNumClientModeStates iState
		IF iGoTo >= MAX_NUM_GOTO_LOCATIONS
			BREAKLOOP
		ENDIF
		
		IF sClientModeStates[iState].eType = eMODESTATE_GO_TO_POINT
			#IF IS_DEBUG_BUILD
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SETUP_GOTOS - GoTo ", iGoTo, " for state ", iState)
			#ENDIF
			
			sGoToLocal.sGotos[iGoTo].iModeStateId = iState
			iGoTo++
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SETUP_GOTOS - Num GoTos: ", iGoTo)
	#ENDIF
	#ENDIF
ENDPROC

//----------------------
//	Take Out Target
//----------------------

#IF MAX_NUM_TARGETS	
PROC INITIALISE_TARGETS(INT iStage)
	
	INT iTarget
	REPEAT MAX_NUM_TARGETS iTarget
	
		IF data.TakeOutTarget[iStage].TakeOutTargets[iTarget].iIndex = -1
			BREAKLOOP
		ENDIF
	
		SWITCH data.TakeOutTarget[iStage].TakeOutTargets[iTarget].eType
		
		  	CASE ET_VEHICLE
				SET_VEHICLE_BIT(data.TakeOutTarget[iStage].TakeOutTargets[iTarget].iIndex, eVEHICLEBITSET_TARGET_VEHICLE)
			BREAK
			
			CASE ET_OBJECT
				SET_PROP_BIT(data.TakeOutTarget[iStage].TakeOutTargets[iTarget].iIndex, ePROPBITSET_TARGET_PROP)
			BREAK

			CASE ET_PED
				SET_PED_BIT(data.TakeOutTarget[iStage].TakeOutTargets[iTarget].iIndex, ePEDBITSET_TARGET_PED)
			BREAK
			
		ENDSWITCH
	ENDREPEAT

ENDPROC

PROC SERVER_COUNT_TOTAL_TARGETS()

	INT i, iTotalTargets
	REPEAT MAX_NUM_TAKE_OUT_TARGET_STAGES i
		iTotalTargets += data.TakeOutTarget[i].iCount
	ENDREPEAT
	
	serverBD.iTotalNumTargets = iTotalTargets
	PRINTLN("[",scriptName,"] - SERVER_COUNT_TOTAL_TARGETS - serverBD.sMissionData.iTotalNumTargets = ", serverBD.iTotalNumTargets)

ENDPROC
#ENDIF

//----------------------
//	Search Area
//----------------------

#IF MAX_NUM_SEARCH_AREAS
FUNC BOOL SHOULD_MAINTAIN_SEARCH_AREA()
	RETURN GET_CLIENT_MODE_STATE() = eMODESTATE_SEARCH_AREA
ENDFUNC

FUNC BOOL SHOULD_ENABLE_SEARCH_AREA(INT iSearchArea)
	RETURN	NOT IS_SEARCH_AREA_DONE(iSearchArea)
			AND NOT IS_SEARCH_AREA_DONE_CLIENT(iSearchArea, LOCAL_PARTICIPANT_INDEX)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_IN_SEARCH_AREA(INT iSearchArea)
	IF sSearchArea.sAreas[iSearchArea].fCentreDistanceSquared  < data.SearchArea.Areas[iSearchArea].fEnterRadius * data.SearchArea.Areas[iSearchArea].fEnterRadius
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_SEARCH_AREA_TRIGGERED(INT iSearchArea)
	IF sSearchArea.fTriggerDistanceSquared < data.SearchArea.Areas[iSearchArea].fTriggerRadius * data.SearchArea.Areas[iSearchArea].fTriggerRadius
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [SEARCH_AREA] - HAS_SEARCH_AREA_TRIGGERED - TRUE - iSearchArea = ", iSearchArea, ", SQRT(sSearchArea.fTriggerDistanceSquared) = ", SQRT(sSearchArea.fTriggerDistanceSquared), " < data.SearchArea.Areas[iSearchArea].fTriggerRadius = ", data.SearchArea.Areas[iSearchArea].fTriggerRadius)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF NOT DEFINED(MAINTAIN_SEARCH_AREA)
PROC MAINTAIN_SEARCH_AREA()
	MAINTAIN_SEARCH_AREA_UI()
	
	IF CALL logic.SearchArea.Enable()
		VECTOR vPlayerPos = LOCAL_PLAYER_COORD
		sSearchArea.iCurrentArea = -1
		
		INT iArea
		REPEAT data.SearchArea.iNumAreas iArea
			IF CALL logic.SearchArea.EnableArea(iArea)
				sSearchArea.sAreas[iArea].fCentreDistanceSquared = VDIST2(vPlayerPos, serverBD.sSearchAreaVars.sAreas[iArea].vRadiusBlipCoord)
				
				IF IS_LOCAL_PLAYER_IN_SEARCH_AREA(iArea)
					sSearchArea.iCurrentArea = iArea
					sSearchArea.fTriggerDistanceSquared = VDIST2(vPlayerPos, GET_SEARCH_AREA_TRIGGER_COORDS(iArea))
						
					IF CALL logic.SearchArea.HasTriggered(iArea)
						SET_SEARCH_AREA_DONE_CLIENT(iArea)
						
						IF logic.SearchArea.OnTriggered != NULL
							CALL logic.SearchArea.OnTriggered(iArea)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF sSearchArea.iCurrentArea = -1
		AND sSearchArea.fTriggerDistanceSquared != -1
			sSearchArea.fTriggerDistanceSquared = -1
		ENDIF
	ENDIF
ENDPROC
#ENDIF 

PROC SETUP_SEARCH_AREA_RADIUS_BLIP_POSITION(INT iSearchArea)
	// Mission Script Overwrite
	IF logic.SearchArea.ForceBlipCoords != NULL
		serverBD.sSearchAreaVars.sAreas[iSearchArea].vRadiusBlipCoord = CALL logic.SearchArea.ForceBlipCoords(iSearchArea)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [SEARCH_AREA] SETUP_SEARCH_AREA_RADIUS_BLIP_POSITION - Blip coord overridden for search area #",iSearchArea,", forced to ", serverBD.sSearchAreaVars.sAreas[iSearchArea].vRadiusBlipCoord)
		EXIT
	ENDIF

	// url:bugstar:7062472 - Freemode Creator - Search Area - Add option to set centre position
	// Creator Overwrite - defined coordiante for center of search area
	IF data.SearchArea.Areas[iSearchArea].bCenterCoordsActive	
		serverBD.sSearchAreaVars.sAreas[iSearchArea].vRadiusBlipCoord = data.SearchArea.Areas[iSearchArea].vCenterCoords
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [SEARCH_AREA] SETUP_SEARCH_AREA_RADIUS_BLIP_POSITION - Blip coord overridden for search area #",iSearchArea,", creator center overwrite to ", serverBD.sSearchAreaVars.sAreas[iSearchArea].vRadiusBlipCoord)
		EXIT
	ENDIF
	
	// Default behaviour - random coordiante from trigger position
	FLOAT fMaxRadius = data.SearchArea.Areas[iSearchArea].fEnterRadius / 2.0
	FLOAT fMinimumRange = fMaxRadius * 0.25
	FLOAT fMaximumRange = fMaxRadius * 0.50
	
	VECTOR vRandomCoord = GET_SEARCH_AREA_TRIGGER_COORDS(iSearchArea)
	
	IF GET_RANDOM_BOOL()
		vRandomCoord.X += GET_RANDOM_FLOAT_IN_RANGE(fMinimumRange, fMaximumRange)
	ELSE
		vRandomCoord.X -= GET_RANDOM_FLOAT_IN_RANGE(fMinimumRange, fMaximumRange)
	ENDIF
	
	IF GET_RANDOM_BOOL()
		vRandomCoord.Y += GET_RANDOM_FLOAT_IN_RANGE(fMinimumRange, fMaximumRange)
	ELSE
		vRandomCoord.Y -= GET_RANDOM_FLOAT_IN_RANGE(fMinimumRange, fMaximumRange)
	ENDIF
	
	serverBD.sSearchAreaVars.sAreas[iSearchArea].vRadiusBlipCoord = vRandomCoord
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [SEARCH_AREA] SETUP_SEARCH_AREA_RADIUS_BLIP_POSITION - Randomised blip coord for search area #",iSearchArea," to ", serverBD.sSearchAreaVars.sAreas[iSearchArea].vRadiusBlipCoord)
ENDPROC
#ENDIF //#IF MAX_NUM_SEARCH_AREAS

//----------------------
//	Leave Area
//----------------------

#IF MAX_NUM_LEAVE_AREAS
FUNC BOOL SHOULD_MAINTAIN_LEAVE_AREA()
	RETURN GET_CLIENT_MODE_STATE() = eMODESTATE_LEAVE_AREA
ENDFUNC

FUNC BOOL SHOULD_ENABLE_LEAVE_AREA(INT iLeaveArea)
	UNUSED_PARAMETER(iLeaveArea)
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_LEAVE_AREA()

	IF NOT CALL logic.LeaveArea.Enable()
		EXIT
	ENDIF
	
	BOOL bInLeaveArea = FALSE
	
	INT i
	REPEAT MAX_NUM_LEAVE_AREAS i
		
		IF NOT IS_VECTOR_ZERO(GET_LEAVE_AREA_COORDS(i))
		AND CALL logic.LeaveArea.EnableArea(i)
		AND IS_PARTICIPANT_IN_LEAVE_AREA(i, LOCAL_PARTICIPANT_INDEX)
			bInLeaveArea = TRUE
			
			IF playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentLeaveArea != i
				playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentLeaveArea = i
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [LEAVE_AREA] - MAINTAIN_LEAVE_AREA - Local player has entered area = ", playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentLeaveArea)
			ENDIF
			
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	IF bInLeaveArea
		CLEAR_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_I_HAVE_LEFT_AREA)
	ELSE
		SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_I_HAVE_LEFT_AREA)
		
		IF playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentLeaveArea != -1
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [LEAVE_AREA] - MAINTAIN_LEAVE_AREA - Local player has left area = ", playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentLeaveArea)
			playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentLeaveArea = -1
		ENDIF
	ENDIF

ENDPROC
#ENDIF

//----------------------
//	Lose Cops
//----------------------

#IF ENABLE_LOSE_THE_COPS
FUNC BOOL SHOULD_ENABLE_LOSE_THE_COPS()
	IF logic.LoseTheCops.Enable != NULL
		RETURN CALL logic.LoseTheCops.Enable()
	ENDIF
	
	RETURN GET_CLIENT_MODE_STATE() = eMODESTATE_LOSE_THE_COPS
ENDFUNC

PROC SET_LOSE_THE_COPS_STATE(eLOSE_THE_COPS_STATE eState)
	IF sLoseTheCops.eState != eState
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [LOSE_THE_COPS] - SET_LOSE_THE_COPS_STATE - ", GET_LOSE_THE_COPS_STATE_NAME(sLoseTheCops.eState), " -> ", GET_LOSE_THE_COPS_STATE_NAME(eState))
		sLoseTheCops.eState = eState
	ENDIF
ENDPROC

PROC CLEANUP_LOSE_THE_COPS()
	IF GET_LOSE_THE_COPS_STATE() = eLOSETHECOPSSTATE_INIT
		EXIT
	ENDIF
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [LOSE_THE_COPS] - CLEANUP_LOSE_THE_COPS.")
	
	SET_LOSE_THE_COPS_STATE(eLOSETHECOPSSTATE_INIT)
ENDPROC

FUNC BOOL SHOULD_TRIGGER_LOSE_THE_COPS_WANTED_LEVEL()
	IF logic.LoseTheCops.ShouldTriggerWantedLevel != NULL
		RETURN CALL logic.LoseTheCops.ShouldTriggerWantedLevel() 
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_LOSE_THE_COPS_WAIT_FOR_WANTED_LEVEL()
	IF logic.LoseTheCops.ShouldWaitForWantedLevel != NULL
	AND CALL logic.LoseTheCops.ShouldWaitForWantedLevel()
		RETURN TRUE
	ENDIF
	
	IF IS_GENERIC_BIT_SET(eGENERICBITSET_RESPAWN_WITH_WANTED_LEVEL)
		RETURN TRUE
	ENDIF
	
	IF IS_GENERIC_BIT_SET(eGENERICBITSET_KEEP_WANTED_LEVEL_INSIDE_INTERIORS)
		RETURN TRUE
	ENDIF
	
	#IF MAX_NUM_MISSION_PORTALS
	IF sPortalLocal.iCachedWantedLevel > 0
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_LOSE_THE_COPS()
	IF NOT SHOULD_ENABLE_LOSE_THE_COPS()
		CLEANUP_LOSE_THE_COPS()
		EXIT
	ENDIF
	
	SWITCH GET_LOSE_THE_COPS_STATE()
		CASE eLOSETHECOPSSTATE_INIT
			IF IS_LOCAL_ALIVE // url:bugstar:6776749
				IF SHOULD_TRIGGER_LOSE_THE_COPS_WANTED_LEVEL()
					IF HAS_LOSE_THE_COPS_WANTED_LEVEL_DELAY_EXPIRED()
						RESET_NET_TIMER(sLoseTheCops.stDelayTimer)
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [LOSE_THE_COPS] - MAINTAIN_LOSE_THE_COPS - Giving a wanted level to the local player.")
						GIVE_WANTED_LEVEL_TO_PLAYER(LOCAL_PLAYER_INDEX, GET_CONTENT_WANTED_LEVEL(), FALSE)
					
						SET_LOSE_THE_COPS_STATE(eLOSETHECOPSSTATE_WAIT_FOR_WANTED_LEVEL)
					ENDIF
				ELSE
					SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_STARTED_LOSE_THE_COPS)
					
					SET_LOSE_THE_COPS_STATE(eLOSETHECOPSSTATE_LOSING_COPS)
				ENDIF
			ENDIF
		BREAK
		
		CASE eLOSETHECOPSSTATE_WAIT_FOR_WANTED_LEVEL
			IF IS_GENERIC_BIT_SET(eGENERICBITSET_RECEIVED_WANTED_EVENT)
			OR GET_PLAYER_WANTED_LEVEL(LOCAL_PLAYER_INDEX) > 0
			OR GET_PLAYER_FAKE_WANTED_LEVEL(LOCAL_PLAYER_INDEX) > 0
				SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_STARTED_LOSE_THE_COPS)
				
				SET_LOSE_THE_COPS_STATE(eLOSETHECOPSSTATE_LOSING_COPS)
			ENDIF
		BREAK
		
		CASE eLOSETHECOPSSTATE_LOSING_COPS
			IF GET_PLAYER_WANTED_LEVEL(LOCAL_PLAYER_INDEX) = 0
			AND GET_PLAYER_FAKE_WANTED_LEVEL(LOCAL_PLAYER_INDEX) = 0
				IF SHOULD_LOSE_THE_COPS_WAIT_FOR_WANTED_LEVEL()
					SET_LOSE_THE_COPS_STATE(eLOSETHECOPSSTATE_WAIT_FOR_WANTED_LEVEL)
				ELSE
					SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_COMPLETED_LOSE_THE_COPS)
					
					SET_LOSE_THE_COPS_STATE(eLOSETHECOPSSTATE_LOST_COPS)
				ENDIF
			ENDIF
		BREAK
		
		CASE eLOSETHECOPSSTATE_LOST_COPS
			IF GET_PLAYER_WANTED_LEVEL(LOCAL_PLAYER_INDEX) > 0
			OR GET_PLAYER_FAKE_WANTED_LEVEL(LOCAL_PLAYER_INDEX) > 0
				CLEAR_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_COMPLETED_LOSE_THE_COPS)
				
				SET_LOSE_THE_COPS_STATE(eLOSETHECOPSSTATE_LOSING_COPS)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF

//----------------------
//	Take Photographs
//----------------------

#IF MAX_NUM_TAKE_PHOTOGRAPHS

FUNC BOOL SHOULD_MAINTAIN_TAKE_PHOTOGRAPHS()
	RETURN GET_CLIENT_MODE_STATE() = eMODESTATE_TAKE_PHOTOGRAPHS
ENDFUNC

FUNC FLOAT GET_CENTRE_SCREEN_TOLERANCE()
	RETURN 1.0
ENDFUNC

FUNC FLOAT GET_MINIMUM_RANGE_FOR_PHOTO(INT iPoint)
	IF logic.TakePhotos.PhotoRange != null 
		RETURN CALL logic.TakePhotos.PhotoRange(iPoint)
	ENDIF
	
	RETURN 100.0
ENDFUNC

FUNC BOOL IS_COORD_ON_SCREEN_AND_WITHIN_BOUNDS( VECTOR vCoord, FLOAT fScreenMin, FLOAT fScreenMax )
	FLOAT fx
	FLOAT fy
	IF( GET_SCREEN_COORD_FROM_WORLD_COORD( vCoord, fx, fy ) )
		IF( fx >= fScreenMin AND fx <= fScreenMax )
			IF( fy >= fScreenMin AND fy <= fScreenMax )
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC VECTOR TAKE_PHOTOS_COORD(INT iPoint)
	RETURN data.TakePhotos.Photos.TrackedPoints[iPoint].vCoord 
ENDFUNC

FUNC BOOL IS_PLAYER_IN_AREA_TO_TAKE_PHOTO(INT iPoint)
	VECTOR vCoords = TAKE_PHOTOS_COORD(iPoint)
	RETURN IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vCoords, GET_MINIMUM_RANGE_FOR_PHOTO(iPoint))
ENDFUNC

FUNC BOOL CLEAN_UP_TRACKED_POINT(INT iPoint)

	IF sTakePhotos.TrackedPointId[iPoint] != 0
		DESTROY_TRACKED_POINT(sTakePhotos.TrackedPointId[iPoint])
		sTakePhotos.TrackedPointId[iPoint] = 0
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - DESTROY_TRACKED_POINT - destroyed tracked point ", iPoint, "!")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DESTROY_TRACKED_POINTS()
	INT i
	REPEAT MAX_NUM_TAKE_PHOTOGRAPHS i
		CLEAN_UP_TRACKED_POINT(i)
	ENDREPEAT
ENDPROC

PROC CLEANUP_PHOTO_SENDING(BOOL bPutPhoneAway = TRUE, BOOL bScriptCleanup = FALSE)
	ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
	BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE)
	DISABLE_AUTO_SCROLL_TO_CONTACT()
	IF bPutPhoneAway
		HANG_UP_AND_PUT_AWAY_PHONE()
	ENDIF
	CLEAR_GENERIC_BIT(eGENERICBITSET_PHOTO_TAKEN)
	IF bScriptCleanup
		MPGlobalsAmbience.bSkipContactListSendingPhotos = FALSE
		REMOVE_TAKE_PHOTOGRAPHS_BLIPS()
	ENDIF
ENDPROC

PROC CLEANUP_TAKE_PHOTOGRAPHS()
	DESTROY_TRACKED_POINTS()	
	CLEANUP_PHOTO_SENDING(FALSE, TRUE)
ENDPROC

FUNC BOOL IS_CONTACT_VALID_FOR_REPLY()
	SWITCH g_TheContactInvolvedinCall
		CASE CHAR_MP_BRUCIE
		CASE CHAR_MP_GERALD
		CASE CHAR_LAMAR
		CASE CHAR_MP_AGENT_14
		CASE CHAR_RON
		CASE CHAR_SIMEON
		CASE CHAR_TAXI
		CASE CHAR_NCLUBT
		CASE CHAR_NCLUBE
		CASE CHAR_NCLUBL
		CASE CHAR_BRYONY
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_WRONG_CONTACT_BIT()
	SWITCH g_TheContactInvolvedinCall
		CASE CHAR_MP_BRUCIE		RETURN 0
		CASE CHAR_MP_GERALD		RETURN 1
		CASE CHAR_LAMAR			RETURN 2
		CASE CHAR_MP_AGENT_14	RETURN 3
		CASE CHAR_RON			RETURN 4
		CASE CHAR_SIMEON		RETURN 5
		CASE CHAR_TAXI			RETURN 6
		CASE CHAR_NCLUBT		RETURN 7
		CASE CHAR_NCLUBE		RETURN 8
		CASE CHAR_NCLUBL		RETURN 9
		CASE CHAR_BRYONY		RETURN 10
	ENDSWITCH
	
	RETURN -1
ENDFUNC

FUNC STRING GET_VARIATION_TEXT_MESSAGE_TAG(INT iPhoto)

	IF logic.TakePhotos.TextMessageTag != NULL
		RETURN CALL logic.TakePhotos.TextMessageTag(iPhoto)
	ENDIF
	
	RETURN ""
ENDFUNC

#IF NOT DEFINED(GET_CHAR_FOR_PHOTO_TAKING_OBJECTIVE)
FUNC enumCharacterList GET_CHAR_FOR_PHOTO_TAKING_OBJECTIVE()
	RETURN CHAR_LESTER
ENDFUNC
#ENDIF

FUNC BOOL DOES_VARIATION_REQUIRE_PHOTO_CONFIRMATION()

	IF logic.TakePhotos.RequiresConfirmation != NULL
		RETURN CALL logic.TakePhotos.RequiresConfirmation()
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL HANDLE_CONFIRMATION()

	// Add variations here if you dont need to send a confirmation text after the timer runs out
	IF NOT DOES_VARIATION_REQUIRE_PHOTO_CONFIRMATION()
		RETURN TRUE
	ENDIF
	
	TEXT_LABEL_15 label
	label = "CSH_TM_"
	label += GET_VARIATION_TEXT_MESSAGE_TAG()
	label += "_"
	label += sTakePhotos.iPhotoTakenForText

	IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(GET_CHAR_FOR_PHOTO_TAKING_OBJECTIVE(), label, TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SEND_PHOTO_TEXT_MESSAGE()
	TEXT_LABEL_15 label
	
	IF sTakePhotos.iPhotoTakenForText != -1
		IF serverBD.iPhotosTaken = data.TakePhotos.Photos.iPhotoCount
			sTakePhotos.iPhotoTakenForText = -1
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - SEND_PHOTO_TEXT_MESSAGE - Last photo taken, don't send final text and let generic one send.")
			EXIT
		ENDIF
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_UP)
		
		IF HAS_NET_TIMER_EXPIRED(sTakePhotos.localPhotoTextDelayTimer, 3000)
			IF IS_GENERIC_BIT_SET(eGENERICBITSET_PHOTO_SENT_TO_WRONG_CONTACT)
				IF IS_CONTACT_VALID_FOR_REPLY()
				AND NOT IS_BIT_SET(sTakePhotos.iWrongContactBitSet, GET_WRONG_CONTACT_BIT())
					SWITCH g_TheContactInvolvedinCall
						CASE CHAR_MP_BRUCIE		label = "CSH_TM_SM_BRC"		BREAK
						CASE CHAR_MP_GERALD		label = "CSH_TM_SM_GER"		BREAK
						CASE CHAR_LAMAR			label = "CSH_TM_SM_LAM"		BREAK
						CASE CHAR_MP_AGENT_14	label = "CSH_TM_SM_A14"		BREAK
						CASE CHAR_RON			label = "CSH_TM_SM_RON"		BREAK
						CASE CHAR_SIMEON		label = "CSH_TM_SM_SIM"		BREAK
						CASE CHAR_TAXI			label = "CSH_TM_SM_TAX"		BREAK
						CASE CHAR_NCLUBT		label = "CSH_TM_SM_TNY"		BREAK
						CASE CHAR_NCLUBE		label = "CSH_TM_SM_EDV"		BREAK
						CASE CHAR_NCLUBL		label = "CSH_TM_SM_LZW"		BREAK
						CASE CHAR_BRYONY		label = "CSH_TM_SM_BRY"		BREAK
					ENDSWITCH
					
					IF DOES_TEXT_LABEL_EXIST(label)
					AND NOT IS_STRING_NULL_OR_EMPTY(TEXT_LABEL_TO_STRING(label))
						IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(g_TheContactInvolvedinCall, label, TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - SEND_PHOTO_TEXT_MESSAGE - Send reply from contact for sending them the photo - ", ENUM_TO_INT(g_TheContactInvolvedinCall))
							sTakePhotos.iPhotoTakenForText = -1
							CLEAR_GENERIC_BIT(eGENERICBITSET_PHOTO_SENT_TO_WRONG_CONTACT)
							RESET_NET_TIMER(sTakePhotos.localPhotoTextDelayTimer)
						ENDIF
					ENDIF
				
					SET_BIT(sTakePhotos.iWrongContactBitSet, GET_WRONG_CONTACT_BIT())
				ELSE
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - SEND_PHOTO_TEXT_MESSAGE - Contact is not valid to send a reply - ", ENUM_TO_INT(g_TheContactInvolvedinCall))
					sTakePhotos.iPhotoTakenForText = -1
					CLEAR_GENERIC_BIT(eGENERICBITSET_PHOTO_SENT_TO_WRONG_CONTACT)
					RESET_NET_TIMER(sTakePhotos.localPhotoTextDelayTimer)
				ENDIF
			ELSE
				IF HANDLE_CONFIRMATION()
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - SEND_PHOTO_TEXT_MESSAGE - Sent text for photo ", sTakePhotos.iPhotoTakenForText)
					sTakePhotos.iPhotoTakenForText = -1
					CLEAR_GENERIC_BIT(eGENERICBITSET_PHOTO_SENT_TO_WRONG_CONTACT)
					RESET_NET_TIMER(sTakePhotos.localPhotoTextDelayTimer)
					SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_PHOTO_SENT_TO_CONTACT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_NUM_PHOTOS_TO_TAKE()
	IF data.TakePhotos.Photos.iPhotoCount > 0
		RETURN data.TakePhotos.Photos.iPhotoCount
	ENDIF
	
	RETURN MAX_NUM_TAKE_PHOTOGRAPHS
ENDFUNC

FUNC BOOL IS_CAMERA_POINT_ON_SCREEN_AND_VALID(INT iPoint)
	VECTOR vCoords = TAKE_PHOTOS_COORD(iPoint)
	
	IF HAS_PHOTO_BEEN_TAKEN_ALREADY(iPoint)
		PRINTLN("IS_CAMERA_POINT_ON_SCREEN_AND_VALID - ", iPoint, " - IS_BIT_SET(serverBD.iPhotographyBitSetServer, iPoint)")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_COORD_ON_SCREEN_AND_WITHIN_BOUNDS(vCoords, 0.0, GET_CENTRE_SCREEN_TOLERANCE())
		PRINTLN("IS_CAMERA_POINT_ON_SCREEN_AND_VALID - ", iPoint, " - NOT IS_COORD_ON_SCREEN_AND_WITHIN_BOUNDS(vCoords, 0.0, GET_CENTRE_SCREEN_TOLERANCE()), vCoords = ", vCoords, " iPoint = ", iPoint)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_IN_AREA_TO_TAKE_PHOTO(iPoint)
		PRINTLN("IS_CAMERA_POINT_ON_SCREEN_AND_VALID - ", iPoint, " - NOT IS_PLAYER_IN_AREA_TO_TAKE_PHOTO(iPoint, 30.0)")
		RETURN FALSE
	ENDIF
	
	IF sTakePhotos.TrackedPointId[iPoint] = 0
		PRINTLN("IS_CAMERA_POINT_ON_SCREEN_AND_VALID - ", iPoint, " - sTakePhotos.sTakePhotos.TrackedPointId[iPoint] = 0")
		RETURN FALSE
	ENDIF
	
	IF NOT IS_TRACKED_POINT_VISIBLE(sTakePhotos.TrackedPointId[iPoint])
		PRINTLN("IS_CAMERA_POINT_ON_SCREEN_AND_VALID - ", iPoint, " - NOT IS_TRACKED_POINT_VISIBLE(sTakePhotos.TrackedPointId[",iPoint,"] = ", sTakePhotos.TrackedPointId[iPoint])
		RETURN FALSE
	ENDIF
	
	IF logic.TakePhotos.RequiredZoom != null
	AND GET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR() < CALL logic.TakePhotos.RequiredZoom(iPoint)
		PRINTLN("IS_CAMERA_POINT_ON_SCREEN_AND_VALID - ", iPoint, " - GET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR (", GET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(),") < logic.TakePhotos.RequiredZoom")
		SET_GENERIC_BIT(eGENERICBITSET_PHOTO_FAILED_FROM_ZOOM)
		RETURN FALSE
	ENDIF
	
	PRINTLN("IS_CAMERA_POINT_ON_SCREEN_AND_VALID - ", iPoint, " return TRUE")
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ANY_CAMERA_POINT_ON_SCREEN(INT& iPointCaptured)
	
	INT iPoint
	IF sTakePhotos.iClosestPhotoPoint != (-1)
	AND IS_CAMERA_POINT_ON_SCREEN_AND_VALID(sTakePhotos.iClosestPhotoPoint)
		iPointCaptured = sTakePhotos.iClosestPhotoPoint
		RETURN TRUE
	ENDIF

	REPEAT GET_NUM_PHOTOS_TO_TAKE() iPoint
		IF IS_CAMERA_POINT_ON_SCREEN_AND_VALID(iPoint)
			iPointCaptured = iPoint
			RETURN TRUE
		ENDIF
	ENDREPEAT

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PHOTO_ENABLED(INT iPhoto)

	IF HAS_PHOTO_BEEN_TAKEN_ALREADY(iPhoto)
		RETURN FALSE
	ENDIF

	IF logic.TakePhotos.EnablePhoto != NULL
		RETURN CALL logic.TakePhotos.EnablePhoto(iPhoto)
	ENDIF

	RETURN TRUE

ENDFUNC

PROC MAINTAIN_CLOSEST_PHOTO_POINT()
	sTakePhotos.fDistanceFromClosestPhotoPoint = 99999.0 
	
	IF sTakePhotos.iClosestPhotoPoint != (-1) 
	AND NOT IS_PHOTO_ENABLED(sTakePhotos.iClosestPhotoPoint)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - MAINTAIN_CLOSEST_PHOTO_POINT - Closest photo point: ", sTakePhotos.iClosestPhotoPoint, " not enabled. Clearing closest data.")
		sTakePhotos.iClosestPhotoPoint = -1
	ENDIF
	
	VECTOR vCoords
	FLOAT fDistance
	
	INT iPoint
	REPEAT GET_NUM_PHOTOS_TO_TAKE() iPoint
		IF IS_PHOTO_ENABLED(iPoint)
			vCoords = TAKE_PHOTOS_COORD(iPoint)
			fDistance = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(LOCAL_PED_INDEX, vCoords, TRUE)
			IF fDistance < sTakePhotos.fDistanceFromClosestPhotoPoint
				IF sTakePhotos.iClosestPhotoPoint != iPoint
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - MAINTAIN_CLOSEST_PHOTO_POINT - Previous closest point: ", sTakePhotos.iClosestPhotoPoint, ". New closest point: ", iPoint)
					sTakePhotos.iClosestPhotoPoint = iPoint
				ENDIF
				sTakePhotos.fDistanceFromClosestPhotoPoint = fDistance
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL ADD_TRACKED_POINT(INT iPoint)

	IF sTakePhotos.TrackedPointId[iPoint] = 0
		VECTOR vCoords = TAKE_PHOTOS_COORD(iPoint)
		IF NOT IS_VECTOR_ZERO(vCoords)
			sTakePhotos.TrackedPointId[iPoint] = CREATE_TRACKED_POINT()
			SET_TRACKED_POINT_INFO(sTakePhotos.TrackedPointId[iPoint], vCoords, data.TakePhotos.Photos.TrackedPoints[iPoint].fRadius)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - CREATE_TRACKED_POINT - Tracked point has been set up: iTrackedPointID[",iPoint,"] = ", sTakePhotos.TrackedPointId[iPoint], "; vCoords = ", vCoords, ", radius = ", data.TakePhotos.Photos.TrackedPoints[iPoint].fRadius)
			RETURN TRUE
		ELSE
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - CREATE_TRACKED_POINT - Coord of tracked point #", iPoint, " is at the origin.")	
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC MAINTAIN_TRACKED_POINTS(INT iPoint)
	IF IS_PHOTO_ENABLED(iPoint)	
		IF ADD_TRACKED_POINT(iPoint)
			EXIT
		ENDIF
	ELSE
		IF CLEAN_UP_TRACKED_POINT(iPoint)
			EXIT
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SHOULD_SHOW_CONTACT_LIST()
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_PHOTO_TAKING_CLIENT()
		
	MAINTAIN_CLOSEST_PHOTO_POINT()

//	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CSH_HLP_SM_0")
//		CLEAR_HELP()
//	ENDIF

	//TRIGGER_HELP(eHELPTEXT_EXPLAIN_PHOTO)
	
	IF NOT IS_GENERIC_BIT_SET(eGENERICBITSET_PHOTO_TAKEN)
		IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
			IF NOT IS_GENERIC_BIT_SET(eGENERICBITSET_TARGET_NOT_IN_PHOTO)
			AND IS_ANY_CAMERA_POINT_ON_SCREEN(sTakePhotos.iLastPhotoTaken)
			AND IS_PHOTO_ENABLED(sTakePhotos.iLastPhotoTaken)
				ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(TRUE)
				ENABLE_AUTO_SCROLL_TO_CONTACT(GET_CHAR_FOR_PHOTO_TAKING_OBJECTIVE())
				
				IF NOT CALL logic.TakePhotos.ShowContactList()
					MPGlobalsAmbience.bSkipContactListSendingPhotos = TRUE
				ENDIF

				IF MPGlobalsAmbience.bSkipContactListSendingPhotos = FALSE
					BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(TRUE)
				ENDIF
				
				CLEAR_CONTACT_PICTURE_MESSAGE(GET_CHAR_FOR_PHOTO_TAKING_OBJECTIVE())
				SET_GENERIC_BIT(eGENERICBITSET_PHOTO_TAKEN)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - MAINTAIN_PHOTO_TAKING_CLIENT - Photo of location was taken, enabling send photo feature.")
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CELL_FOC_HLP")
					CLEAR_HELP()
				ENDIF
			ELSE
				IF NOT IS_GENERIC_BIT_SET(eGENERICBITSET_TARGET_NOT_IN_PHOTO)
					SET_GENERIC_BIT(eGENERICBITSET_TARGET_NOT_IN_PHOTO)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - MAINTAIN_PHOTO_TAKING_CLIENT - Location was not in photo, waiting for another photo.")
				ENDIF
			ENDIF
		ELSE
			IF IS_GENERIC_BIT_SET(eGENERICBITSET_TARGET_NOT_IN_PHOTO)
				CLEAR_GENERIC_BIT(eGENERICBITSET_TARGET_NOT_IN_PHOTO)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - MAINTAIN_PHOTO_TAKING_CLIENT - Reset data from location not being in photo.")
			ENDIF
		ENDIF
	ELSE
		IF HAS_PICTURE_MESSAGE_BEEN_SENT_TO_ANY_CONTACT()
		OR (MPGlobalsAmbience.bSkipContactListSendingPhotos
		AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_EXTRA_OPTION))
			IF HAS_CONTACT_RECEIVED_PICTURE_MESSAGE(GET_CHAR_FOR_PHOTO_TAKING_OBJECTIVE())
			OR MPGlobalsAmbience.bSkipContactListSendingPhotos
				//IF iCurrentPhotoTaken = iLocalPhotosTaken
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - MAINTAIN_PHOTO_TAKING_CLIENT - Photo taken of point #", sTakePhotos.iLastPhotoTaken,"! Setting bit.")
					SET_CLIENT_PHOTOGRAPHY_BIT(sTakePhotos.iLastPhotoTaken)
					
//					IF IS_SERVER_BIT_SET(eSERVERBITSET_INITIAL_SCOPE_OUT)
//						BROADCAST_CASINO_FM_MISSION_SCOPE_OUT_PHOTO_TAKEN(sTakePhotos.iLastPhotoTaken, serverBD.iUniqueMissionID)
//						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - MAINTAIN_PHOTO_TAKING_CLIENT - Scope out (initial) - send a ticker event. Photo: ", sTakePhotos.iLastPhotoTaken)
//					ENDIF

					IF logic.TakePhotos.OnPhotoTaken != NULL
						CALL logic.TakePhotos.OnPhotoTaken(sTakePhotos.iLastPhotoTaken)
					ENDIF
					
					IF DOES_VARIATION_REQUIRE_PHOTO_CONFIRMATION()
						sTakePhotos.iPhotoTakenForText = sTakePhotos.iLastPhotoTaken
					ELSE
						sTakePhotos.iPhotoTakenForText = -1
						RESET_NET_TIMER(sTakePhotos.localPhotoTextDelayTimer)
						SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_PHOTO_SENT_TO_CONTACT)
					ENDIF
					
					sTakePhotos.iLastPhotoTaken = -1 
					CLEAR_GENERIC_BIT(eGENERICBITSET_PHOTO_SENT_TO_WRONG_CONTACT)
					CLEAR_CONTACT_PICTURE_MESSAGE(GET_CHAR_FOR_PHOTO_TAKING_OBJECTIVE())
					CLEANUP_PHOTO_SENDING()
					
					IF MPGlobalsAmbience.bSkipContactListSendingPhotos
						PLAY_SOUND_FRONTEND(-1, "Menu_Accept", g_Owner_Soundset)
					ENDIF
					MPGlobalsAmbience.bSkipContactListSendingPhotos = TRUE

//					//B* 6143788 - Clear the request for the help text
//					CLEAR_HELP_TRIGGER(eHELPTEXT_SEND_PHOTOGRAPH)
//					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CSH_HLP_SNDPHOT")
//						CLEAR_HELP()
//					ELSE
//						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CSH_HLP_EXPSCOP")
//							CLEAR_HELP()
//						ENDIF
//					ENDIF
					
//					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CSH_HLP_PHOWC")
//						CLEAR_HELP()
//					ENDIF
					
//				ELSE
//					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - MAINTAIN_PHOTO_TAKING_CLIENT - Another player has already sent this photo")
//				ENDIF
			ELSE
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - MAINTAIN_PHOTO_TAKING_CLIENT - Player sent photo to wrong contact, need to try again.")
				sTakePhotos.iPhotoTakenForText = sTakePhotos.iLastPhotoTaken
				sTakePhotos.iLastPhotoTaken = -1
				SET_GENERIC_BIT(eGENERICBITSET_PHOTO_SENT_TO_WRONG_CONTACT)
				
				CLEANUP_PHOTO_SENDING()
				
//				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CSH_HLP_PHOTO")
//					CLEAR_HELP()
//				ENDIF
			ENDIF

		ELSE
			IF NOT IS_PHONE_ONSCREEN()
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [TAKE_PHOTOS] - MAINTAIN_PHOTO_TAKING_CLIENT - Phone no longer on screen, cleanup")
				CLEANUP_PHOTO_SENDING()
//			ELSE
//				TRIGGER_HELP(eHELPTEXT_SEND_PHOTOGRAPH)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#IF NOT DEFINED(MAINTAIN_TAKE_PHOTOGRAPHS)
PROC MAINTAIN_TAKE_PHOTOGRAPHS()

	IF NOT CALL logic.TakePhotos.Enable()
		IF IS_GENERIC_BIT_SET(eGENERICBITSET_PHOTOGRAPHY_SET_UP)
			CLEANUP_TAKE_PHOTOGRAPHS()
			CLEAR_GENERIC_BIT(eGENERICBITSET_PHOTOGRAPHY_SET_UP)
		ENDIF
		EXIT
	ENDIF
	
	SET_GENERIC_BIT(eGENERICBITSET_PHOTOGRAPHY_SET_UP)
	
	INT i
	REPEAT data.TakePhotos.Photos.iPhotoCount i
		MAINTAIN_TAKE_PHOTOGRAPHS_BLIPS(i)
		MAINTAIN_TRACKED_POINTS(i)
	ENDREPEAT
	
	MAINTAIN_PHOTO_TAKING_CLIENT()	
ENDPROC
#ENDIF

#ENDIF //MAX_NUM_TAKE_PHOTOGRAPHS

//----------------------
//	Collect Ped
//----------------------

#IF MAX_NUM_COLLECT_PEDS
#IF NOT DEFINED(SHOULD_DO_COLLECT_PED)
FUNC BOOL SHOULD_DO_COLLECT_PED()
	IF logic.CollectPed.ShouldCollectPed != NULL
		RETURN CALL logic.CollectPed.ShouldCollectPed()
	ENDIF
	
	RETURN GET_MODE_STATE() = eMODESTATE_COLLECT_PED
ENDFUNC
#ENDIF

//Checks whether the local player currently holds the mission entity associated with the given ped.
FUNC BOOL IS_LOCAL_PLAYER_HOLDING_CORRECT_MISSION_ENTITY_FOR_PED(INT iPed)
	IF IS_PLAYER_IN_POSSESSION_OF_ANY_MISSION_ENTITY(LOCAL_PLAYER_INDEX)
	OR IS_PLAYER_IN_VEHICLE_WITH_ANY_MISSION_ENTITY(LOCAL_PLAYER_INDEX)
		RETURN GET_MISSION_ENTITY_PLAYER_IS_HOLDING_OR_IN_VEHICLE_WITH() = GET_MISSION_ENTITY_FOR_COLLECT_PED(iPed)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PED_IN_CORRECT_STATE_FOR_SIGNAL(INT iPed)
	IF logic.CollectPed.IsPedInCorrectStateForSignal != NULL
		RETURN CALL logic.CollectPed.IsPedInCorrectStateForSignal(iPed)
	ENDIF
	
	IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_BEEN_SIGNALED)
	AND NOT IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_BEEN_COLLECTED)
		//Ensure the ped is in their starting position.
		SWITCH GET_PED_TASK(iPed)
			CASE ePEDTASK_DO_SCENARIOS
			CASE ePEDTASK_PLAY_ANIM
			CASE ePEDTASK_IDLE
				RETURN TRUE
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ALL_PEDS_COLLECTED()
	INT iMissionEntity
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() iMissionEntity
		INT iPed = GET_COLLECT_PED_FOR_MISSION_ENTITY(iMissionEntity)
		
		IF iPed != -1
			IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_BEEN_COLLECTED)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_COLLECTING_PED()
	IF IS_PLAYER_IN_POSSESSION_OF_ANY_MISSION_ENTITY(LOCAL_PLAYER_INDEX)
	OR IS_PLAYER_IN_VEHICLE_WITH_ANY_MISSION_ENTITY(LOCAL_PLAYER_INDEX)
		INT iMissionEntity = GET_MISSION_ENTITY_PLAYER_IS_HOLDING_OR_IN_VEHICLE_WITH()
		INT iPedToCollect = GET_COLLECT_PED_FOR_MISSION_ENTITY(iMissionEntity)
		
		IF iPedToCollect != -1
			IF IS_PED_BIT_SET(iPedToCollect, ePEDBITSET_I_HAVE_BEEN_COLLECTED)
			OR IS_PED_CLIENT_BIT_SET(iPedToCollect, LOCAL_PARTICIPANT_INDEX, ePEDCLIENTBITSET_I_HAVE_BEEN_COLLECTED)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_COLLECT_PED_COORDS(INT iPed)
	RETURN CALL logic.Ped.Coords(iPed)
ENDFUNC

FUNC FLOAT GET_COLLECT_PED_RADIUS(INT iPed)
	#IF NOT MAX_NUM_COLLECT_PEDS
	UNUSED_PARAMETER(iPed)
	#ENDIF
	
	#IF MAX_NUM_COLLECT_PEDS
	INT i
	REPEAT MAX_NUM_COLLECT_PEDS i
		IF data.CollectPeds[i].iPed = iPed
			RETURN data.CollectPeds[i].fRadius
		ENDIF
	ENDREPEAT
	#ENDIF
	
	RETURN -1.0
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_SIGNALING_COLLECT_PED()
	IF logic.CollectPed.IsLocalPlayerSignalingCollectPed != NULL
		RETURN CALL logic.CollectPed.IsLocalPlayerSignalingCollectPed()
	ENDIF
	
	RETURN IS_PLAYER_PRESSING_HORN(LOCAL_PLAYER_INDEX)
ENDFUNC

FUNC BOOL IS_MISSION_ENTITY_IN_CORRECT_STATE_FOR_SIGNAL(INT iMissionEntity)
	IF logic.CollectPed.IsMissionEntityInCorrectStateForSignal != NULL
		CALL logic.CollectPed.IsMissionEntityInCorrectStateForSignal(iMissionEntity)
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_NEAR_COLLECT_PED(INT iPed)
	IF logic.CollectPed.IsLocalPlayerNearCollectPed != NULL
		RETURN CALL logic.CollectPed.IsLocalPlayerNearCollectPed(iPed)
	ENDIF
	
	RETURN VDIST2(LOCAL_PLAYER_COORD, GET_COLLECT_PED_COORDS(iPed)) < GET_COLLECT_PED_RADIUS(iPed) * GET_COLLECT_PED_RADIUS(iPed)
ENDFUNC

FUNC BOOL SHOULD_STOP_VEHICLE_NEAR_PEDS_FOR_COLLECT_PED()
	IF logic.CollectPed.ShouldStopVehicleNearPedsForCollectPed != NULL
		RETURN CALL logic.CollectPed.ShouldStopVehicleNearPedsForCollectPed()
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_SIGNAL_COLLECT_PED()
	IF logic.CollectPed.ShouldSignalCollectPed != NULL
		RETURN CALL logic.CollectPed.ShouldSignalCollectPed()
	ENDIF
	
	RETURN TRUE
ENDFUNC

#IF NOT DEFINED(MAINTAIN_COLLECT_PED)
PROC MAINTAIN_COLLECT_PED()
	IF NOT SHOULD_DO_COLLECT_PED()
		EXIT
	ENDIF
	
	INT i
	INT iPed
	INT iMissionEntity
	INT iVehicle
	REPEAT MAX_NUM_COLLECT_PEDS i
		IF data.CollectPeds[i].iPed = -1
		OR data.CollectPeds[i].iMissionEntity = -1
			RELOOP
		ENDIF
		
		iPed = data.CollectPeds[i].iPed
		iMissionEntity = data.CollectPeds[i].iMissionEntity
		iVehicle = GET_MISSION_ENTITY_CARRIER(iMissionEntity)
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
		AND NOT IS_ENTITY_DEAD(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iPed].netId)
		AND NOT IS_ENTITY_DEAD(NET_TO_PED(serverBD.sPed[iPed].netId))
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
				IF IS_PED_IN_VEHICLE(NET_TO_PED(serverBD.sPed[iPed].netId), NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
					SET_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_I_HAVE_BEEN_COLLECTED)
				ELSE
					CLEAR_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_I_HAVE_BEEN_COLLECTED)
				ENDIF
			ENDIF
			
			IF iMissionEntity = GET_MISSION_ENTITY_PLAYER_IS_HOLDING(LOCAL_PLAYER_INDEX)
			AND NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iMissionEntity, TRUE)
				IF NOT IS_PED_BIT_SET(iPed, ePEDBITSET_I_HAVE_BEEN_COLLECTED)
				AND NOT IS_PED_CLIENT_BIT_SET(iPed, LOCAL_PARTICIPANT_INDEX, ePEDCLIENTBITSET_I_HAVE_BEEN_COLLECTED)
					BOOL bNearCollectPed = IS_LOCAL_PLAYER_NEAR_COLLECT_PED(iPed)
					BOOL bMissionEntityCorrectState = IS_MISSION_ENTITY_IN_CORRECT_STATE_FOR_SIGNAL(iMissionEntity)
					
					IF NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(iMissionEntity, LOCAL_PARTICIPANT_INDEX, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
						IF bNearCollectPed
						AND bMissionEntityCorrectState
							IF NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(iMissionEntity, LOCAL_PARTICIPANT_INDEX, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
								SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
								
								IF logic.CollectPed.OnNearChanged != NULL
									CALL logic.CollectPed.OnNearChanged(iPed, TRUE)
								ENDIF
								
								IF MAINTAIN_CONTROL_OF_NETWORK_ID(serverBD.sVehicle[iVehicle].netId)
									BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(NET_TO_VEH(serverBD.sVehicle[iVehicle].netId))
								ENDIF
								
								PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_PED - I am close enough to iPed #", iPed, " to signal them to enter.")
							ENDIF
						ENDIF
					ELSE
						IF bNearCollectPed
						AND bMissionEntityCorrectState
							IF SHOULD_SIGNAL_COLLECT_PED()
								IF NOT IS_PED_CLIENT_BIT_SET(iPed, LOCAL_PARTICIPANT_INDEX, ePEDCLIENTBITSET_I_HAVE_BEEN_SIGNALED)
								AND IS_PED_IN_CORRECT_STATE_FOR_SIGNAL(iPed)
									IF IS_LOCAL_PLAYER_SIGNALING_COLLECT_PED()
										SET_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_I_HAVE_BEEN_SIGNALED)
										
										IF logic.CollectPed.OnSignaled != NULL
											CALL logic.CollectPed.OnSignaled(iPed)
										ENDIF
										
										PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_PED - I have signaled iPed #", iPed, " to enter.")
									ELSE
										IF logic.CollectPed.MaintainSignalAvailable != NULL
											CALL logic.CollectPed.MaintainSignalAvailable()
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_PED_CLIENT_BIT_SET(iPed, LOCAL_PARTICIPANT_INDEX, ePEDCLIENTBITSET_I_HAVE_BEEN_SIGNALED)
									SET_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_I_HAVE_BEEN_SIGNALED)
								ENDIF
							ENDIF
						ELSE
							IF IS_MISSION_ENTITY_CLIENT_BIT_SET(iMissionEntity, LOCAL_PARTICIPANT_INDEX, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
								CLEAR_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
								
								IF logic.CollectPed.OnNearChanged != NULL
									CALL logic.CollectPed.OnNearChanged(iPed, FALSE)
								ENDIF
								
								IF IS_PED_CLIENT_BIT_SET(iPed, LOCAL_PARTICIPANT_INDEX, ePEDCLIENTBITSET_I_HAVE_BEEN_SIGNALED)
									CLEAR_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_I_HAVE_BEEN_SIGNALED)
								ENDIF
								
								PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_PED - I am no longer close enough to iPed #", iPed, " to signal them to enter.")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//The player is in the carrier vehicle and the ped has been collected.
					IF logic.CollectPed.DoCollected != NULL
						CALL logic.CollectPed.DoCollected(iPed)
					ENDIF
				ENDIF
			ELSE
				IF IS_MISSION_ENTITY_CLIENT_BIT_SET(iMissionEntity, LOCAL_PARTICIPANT_INDEX, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
					CLEAR_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
					
					IF logic.CollectPed.OnNearChanged != NULL
						CALL logic.CollectPed.OnNearChanged(iPed, FALSE)
					ENDIF
					
					IF IS_PED_CLIENT_BIT_SET(iPed, LOCAL_PARTICIPANT_INDEX, ePEDCLIENTBITSET_I_HAVE_BEEN_SIGNALED)
						CLEAR_PED_CLIENT_BIT(iPed, ePEDCLIENTBITSET_I_HAVE_BEEN_SIGNALED)
					ENDIF
					
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_PED - I am no longer in iMissionEntity = ", iMissionEntity)
				ENDIF
			ENDIF
		ELSE
			IF IS_MISSION_ENTITY_CLIENT_BIT_SET(iMissionEntity, LOCAL_PARTICIPANT_INDEX, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
				CLEAR_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_NEAR_PED_TO_COLLECT)
				
				IF logic.CollectPed.OnNearChanged != NULL
					CALL logic.CollectPed.OnNearChanged(iPed, FALSE)
				ENDIF
				
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_PED - iPed #", iPed, " does not exist or is dead, cannot signal them to enter.")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF
#ENDIF

//----------------------
//	COLLECT CHECKPOINT
//----------------------

#IF MAX_NUM_CHECKPOINTS

FUNC VECTOR GET_COLLECT_CHECKPOINT_COORDS(INT iCheckpointId = -1)
	IF iCheckpointId = -1
		RETURN GET_COLLECT_CHECKPOINT_COORDS(playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId)
	ENDIF
	
	IF logic.Checkpoints.OverrideCheckpointCoords != NULL
		RETURN CALL logic.Checkpoints.OverrideCheckpointCoords(iCheckpointId)
	ENDIF
	
	IF iCheckpointId >= data.Checkpoint.iCount
		ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CHECKPOINTS] - [GET_CHECKPOINT_COORDS] - iCheckpointId ",iCheckpointId," >= data.Checkpoint.iCount ",data.Checkpoint.iCount)
	ENDIF
	RETURN data.Checkpoint.Checkpoints[iCheckpointId].vCoords
ENDFUNC

FUNC FLOAT GET_COLLECT_CHECKPOINT_VISUAL_RADIUS(INT iCheckpointId = -1)
	IF iCheckpointId = -1
		RETURN GET_COLLECT_CHECKPOINT_VISUAL_RADIUS(playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId)
	ENDIF
	IF iCheckpointId >= data.Checkpoint.iCount
		ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CHECKPOINTS] - [GET_CHECKPOINT_COORDS] - iCheckpointId ",iCheckpointId," >= data.Checkpoint.iCount ",data.Checkpoint.iCount)
	ENDIF
	RETURN data.Checkpoint.Checkpoints[iCheckpointId].fVisualRadius
ENDFUNC

FUNC FLOAT GET_COLLECT_CHECKPOINT_COLLECT_RADIUS(INT iCheckpointId = -1)
	IF iCheckpointId = -1
		RETURN GET_COLLECT_CHECKPOINT_COLLECT_RADIUS(playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId)
	ENDIF
	IF iCheckpointId >= data.Checkpoint.iCount
		ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [CHECKPOINTS] - [GET_CHECKPOINT_COORDS] - iCheckpointId ",iCheckpointId," >= data.Checkpoint.iCount ",data.Checkpoint.iCount)
	ENDIF
	RETURN data.Checkpoint.Checkpoints[iCheckpointId].fCollectRadius
ENDFUNC

#IF IS_DEBUG_BUILD
PROC DEBUG_COLLECT_CHECKPOINT_RENDER_CORONA(INT iCheckpointId,FLOAT fRadius, HUD_COLOURS eColor)
	DRAW_DEBUG_CORONA(GET_COLLECT_CHECKPOINT_COORDS(iCheckpointId),fRadius*0.5,eColor)
ENDPROC
#ENDIF

FUNC STRING GET_COLLECT_CHECKPOINT_BLIP_NAME(INT iCheckpointId)
	IF logic.Checkpoints.BlipName != NULL
		RETURN CALL logic.Checkpoints.BlipName(iCheckpointId)
	ENDIF

	RETURN "TSA_CC_CHECK"
ENDFUNC

FUNC BOOL GET_COLLECT_CHECKPOINT_SHOULD_BLIP_FLASH(INT iCheckpointId)
	IF logic.Checkpoints.ShouldBlipFlash != NULL
		RETURN CALL logic.Checkpoints.ShouldBlipFlash(iCheckpointId)
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL GET_COLLECT_CHECKPOINT_SHOULD_BLIP_DRAW_GPS(INT iCheckpointId)
	IF logic.Checkpoints.ShouldBlipDrawGPS != NULL
		RETURN CALL logic.Checkpoints.ShouldBlipDrawGPS(iCheckpointId)
	ENDIF

	RETURN FALSE
ENDFUNC

PROC CREATE_COLLECT_CHECKPOINTS_BLIP(BLIP_INDEX &biNewCheckpointBlip, INT iCheckpointId)
	ADD_COORD_BLIP(	biNewCheckpointBlip,
					GET_COLLECT_CHECKPOINT_COORDS(iCheckpointId),
					data.Checkpoint.Checkpoints[iCheckpointId].sBlip.eSprite,
					data.Checkpoint.Checkpoints[iCheckpointId].sBlip.eHudColour,
					GET_COLLECT_CHECKPOINT_BLIP_NAME(iCheckpointId),
					data.Checkpoint.Checkpoints[iCheckpointId].sBlip.fScale,
					GET_COLLECT_CHECKPOINT_SHOULD_BLIP_DRAW_GPS(iCheckpointId),
					FALSE,
					GET_COLLECT_CHECKPOINT_SHOULD_BLIP_FLASH(iCheckpointId))
ENDPROC
FUNC FLOAT GET_ANGLE_BETWEEN_3D_VECTORS(VECTOR a, VECTOR b)
	a = NORMALISE_VECTOR(a)
	b = NORMALISE_VECTOR(b)
	RETURN ACOS(((a.x * b.x + a.y * b.y + a.z * b.z) / (SQRT(a.x*a.x + a.y*a.y + a.z*a.z) * SQRT(b.x*b.x + b.y*b.y + b.z*b.z))))
ENDFUNC

FUNC FLOAT GET_COLLECT_CHECKPOINT_INSIDE_CYLINDER_HEIGHT_SCALE(INT iCheckpointId)
	IF logic.Checkpoints.InsideCylinderHeightScale != NULL
		RETURN CALL logic.Checkpoints.InsideCylinderHeightScale(iCheckpointId)
	ENDIF
	RETURN 0.25
ENDFUNC

FUNC FLOAT GET_COLLECT_CHECKPOINT_INSIDE_CYLINDER_SCALE(INT iCheckpointId)
	IF logic.Checkpoints.InsideCylinderScale != NULL
		RETURN CALL logic.Checkpoints.InsideCylinderScale(iCheckpointId)
	ENDIF
	RETURN 0.5
ENDFUNC

FUNC FLOAT GET_COLLECT_CHECKPOINT_CYLINDER_HEIGHT_MAX(INT iCheckpointId)
	IF logic.Checkpoints.CylinderHeightMax != NULL
		RETURN CALL logic.Checkpoints.CylinderHeightMax(iCheckpointId)
	ENDIF
	RETURN 5.0
ENDFUNC

FUNC FLOAT GET_COLLECT_CHECKPOINT_CYLINDER_HEIGHT_MIN(INT iCheckpointId)
	IF logic.Checkpoints.CylinderHeightMin != NULL
		RETURN CALL logic.Checkpoints.CylinderHeightMin(iCheckpointId)
	ENDIF
	RETURN 4.0
ENDFUNC

FUNC BOOL GET_COLLECT_CHECKPOINT_APPLY_STEEP_FIX(INT iCheckpointId)
	IF logic.Checkpoints.ApplySteepFix != NULL
		RETURN CALL logic.Checkpoints.ApplySteepFix(iCheckpointId)
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DRAW_COLLECT_CHECKPOINT_CYLINDER(INT iCheckpointId)
	IF sCollectCheckpointsLocal.ciCheckpoint[iCheckpointId] = NULL
		INT iR,iG,iB,iA
		VECTOR vCurrentCheckpointCoord,vNextCheckpointCoord
		CHECKPOINT_TYPE ctType

		vCurrentCheckpointCoord = GET_COLLECT_CHECKPOINT_COORDS(iCheckpointId)
		vNextCheckpointCoord 	= GET_COLLECT_CHECKPOINT_COORDS((iCheckpointId+1)%data.Checkpoint.iCount)
		ctType = data.Checkpoint.Checkpoints[iCheckpointId].eType
		HUD_COLOURS eColour = data.Checkpoint.Checkpoints[iCheckpointId].iVisualColour
		IF logic.Checkpoints.VisualColour != NULL
			eColour = CALL logic.Checkpoints.VisualColour(iCheckpointId)
		ENDIF
		FLOAT fAlpha = 0.5
		IF logic.Checkpoints.VisualAlpha != NULL
			fAlpha = CALL logic.Checkpoints.VisualAlpha(iCheckpointId)
		ENDIF
		GET_HUD_COLOUR(eColour,iR, iG, iB, iA)

		FLOAT fCylinderMaxHeight = GET_COLLECT_CHECKPOINT_CYLINDER_HEIGHT_MAX(iCheckpointId)
		FLOAT fCylinderMinHeight = GET_COLLECT_CHECKPOINT_CYLINDER_HEIGHT_MIN(iCheckpointId)
		FLOAT fInsideCylinderHeightScale = GET_COLLECT_CHECKPOINT_INSIDE_CYLINDER_HEIGHT_SCALE(iCheckpointId)
		FLOAT fInsideCylinderScale = GET_COLLECT_CHECKPOINT_INSIDE_CYLINDER_SCALE(iCheckpointId)

		BOOL bApplySteepFix = GET_COLLECT_CHECKPOINT_APPLY_STEEP_FIX(iCheckpointId)
		IF bApplySteepFix
			FLOAT fAngle = GET_ANGLE_BETWEEN_3D_VECTORS(<<0,0,1>>,sCollectCheckpointsLocal.vCoordsNormal[iCheckpointId])
			FLOAT fTanAngle = TAN(fAngle)
			FLOAT fRadius = data.Checkpoint.Checkpoints[iCheckpointId].fVisualRadius / 2.0
			FLOAT fHeight = fTanAngle * fRadius

			fCylinderMaxHeight += fHeight // we increase the cylinder height because we are going to lower it to bury it in the terrain
			fCylinderMinHeight += fHeight
			
			sCollectCheckpointsLocal.ciCheckpoint[iCheckpointId] = CREATE_CHECKPOINT(ctType, vCurrentCheckpointCoord + <<0,0, -fHeight>>, vNextCheckpointCoord, GET_COLLECT_CHECKPOINT_VISUAL_RADIUS(iCheckpointId), iR, iG, iB, ROUND(iA*fAlpha),1)
			SET_CHECKPOINT_INSIDE_CYLINDER_HEIGHT_SCALE(sCollectCheckpointsLocal.ciCheckpoint[iCheckpointId],fInsideCylinderHeightScale+fHeight/fCylinderMaxHeight)
			SET_CHECKPOINT_CLIPPLANE_WITH_POS_NORM(sCollectCheckpointsLocal.ciCheckpoint[iCheckpointId], vCurrentCheckpointCoord - <<0,0,0.05>>, sCollectCheckpointsLocal.vCoordsNormal[iCheckpointId])
		ELSE
			sCollectCheckpointsLocal.ciCheckpoint[iCheckpointId] = CREATE_CHECKPOINT(ctType, vCurrentCheckpointCoord, vNextCheckpointCoord, GET_COLLECT_CHECKPOINT_VISUAL_RADIUS(iCheckpointId), iR, iG, iB, ROUND(iA*fAlpha),1)
			SET_CHECKPOINT_INSIDE_CYLINDER_HEIGHT_SCALE(sCollectCheckpointsLocal.ciCheckpoint[iCheckpointId],fInsideCylinderHeightScale)
		ENDIF
		SET_CHECKPOINT_INSIDE_CYLINDER_SCALE(sCollectCheckpointsLocal.ciCheckpoint[iCheckpointId],fInsideCylinderScale)
		SET_CHECKPOINT_CYLINDER_HEIGHT(sCollectCheckpointsLocal.ciCheckpoint[iCheckpointId],fCylinderMinHeight,fCylinderMaxHeight,fCylinderMaxHeight)
		GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS,data.Checkpoint.Checkpoints[iCheckpointId].iChevronColour), iR, iG, iB, iA)
		IF NOT data.Checkpoint.Checkpoints[iCheckpointId].bShowChevron
			iA = 0
		ENDIF
		SET_CHECKPOINT_RGBA2(sCollectCheckpointsLocal.ciCheckpoint[iCheckpointId],iR, iG, iB, iA)
	ENDIF
ENDPROC

PROC DRAW_COLLECT_CHECKPOINT_DEFAULT(INT iCheckpointId)
	INT iR,iG,iB,iA
	FLOAT fRadius = data.Checkpoint.Checkpoints[iCheckpointId].fVisualRadius
	MARKER_TYPE mtMarkerType = GET_MARKER_TYPE_FROM_FMC_CHECKPOINTS_LIST_MARKER_TYPE(data.Checkpoint.Checkpoints[iCheckpointId].iMarkerType)
	HUD_COLOURS eColour = data.Checkpoint.Checkpoints[iCheckpointId].iVisualColour
	IF logic.Checkpoints.VisualColour != NULL
		eColour = CALL logic.Checkpoints.VisualColour(iCheckpointId)
	ENDIF
	FLOAT fAlpha = 0.5
	IF logic.Checkpoints.VisualAlpha != NULL
		fAlpha = CALL logic.Checkpoints.VisualAlpha(iCheckpointId)
	ENDIF
	GET_HUD_COLOUR(eColour,iR, iG, iB, iA)
	DRAW_MARKER(mtMarkerType, data.Checkpoint.Checkpoints[iCheckpointId].vCoords, <<0.0,0.0,0.0>>, data.Checkpoint.Checkpoints[iCheckpointId].vRotation, <<fRadius, fRadius, fRadius>>, iR, iG, iB, ROUND(iA*fAlpha))
ENDPROC

PROC DRAW_COLLECT_CHECKPOINT(INT iCheckpointId)
	SWITCH data.Checkpoint.Checkpoints[iCheckpointId].iMarkerType
		CASE FMCCL_MARKER_CYLINDER		
			DRAW_COLLECT_CHECKPOINT_CYLINDER(iCheckpointId)
		BREAK
		DEFAULT
			DRAW_COLLECT_CHECKPOINT_DEFAULT(iCheckpointId)
		BREAK
	ENDSWITCH
ENDPROC

PROC UNDRAW_COLLECT_CHECKPOINT(INT iCheckpointId)
	IF sCollectCheckpointsLocal.ciCheckpoint[iCheckpointId] != NULL
		DELETE_CHECKPOINT(sCollectCheckpointsLocal.ciCheckpoint[iCheckpointId])
		sCollectCheckpointsLocal.ciCheckpoint[iCheckpointId] = NULL
		sCollectCheckpointsLocal.bCheckpointClipChecked[iCheckpointId] = FALSE
//		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - UNDRAW_COLLECT_CHECKPOINT - DELETE_CHECKPOINT iCheckpointId ",iCheckpointId)
	ENDIF
ENDPROC

PROC BLIP_COLLECT_CHECKPOINT(INT iCheckpointId, BOOL bSmall = FALSE)
 	IF NOT DOES_BLIP_EXIST(sCollectCheckpointsLocal.biCheckpointBlip[iCheckpointId])
		CREATE_COLLECT_CHECKPOINTS_BLIP(sCollectCheckpointsLocal.biCheckpointBlip[iCheckpointId],iCheckpointId)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - BLIP_COLLECT_CHECKPOINT - CREATE_COLLECT_CHECKPOINTS_BLIP iCheckpointId ",iCheckpointId," bSmall ",bSmall)
	ELSE
		IF bSmall
			SET_BLIP_SCALE(sCollectCheckpointsLocal.biCheckpointBlip[iCheckpointId], data.Checkpoint.Checkpoints[iCheckpointId].sBlip.fScale*0.5)
		ELSE
			SET_BLIP_SCALE(sCollectCheckpointsLocal.biCheckpointBlip[iCheckpointId], data.Checkpoint.Checkpoints[iCheckpointId].sBlip.fScale)
		ENDIF
		
		IF DOES_BLIP_HAVE_GPS_ROUTE(sCollectCheckpointsLocal.biCheckpointBlip[iCheckpointId])
			IF NOT GET_COLLECT_CHECKPOINT_SHOULD_BLIP_DRAW_GPS(iCheckpointId)
				SET_BLIP_ROUTE(sCollectCheckpointsLocal.biCheckpointBlip[iCheckpointId], FALSE)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - BLIP_COLLECT_CHECKPOINT - SET_BLIP_ROUTE(sCollectCheckpointsLocal.biCheckpointBlip[", iCheckpointId, "], FALSE)")
			ENDIF
		ELSE
			IF GET_COLLECT_CHECKPOINT_SHOULD_BLIP_DRAW_GPS(iCheckpointId)
				SET_BLIP_ROUTE(sCollectCheckpointsLocal.biCheckpointBlip[iCheckpointId], TRUE)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - BLIP_COLLECT_CHECKPOINT - SET_BLIP_ROUTE(sCollectCheckpointsLocal.biCheckpointBlip[", iCheckpointId, "], TRUE)")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UNBLIP_COLLECT_CHECKPOINT(INT iCheckpointId)
 	IF DOES_BLIP_EXIST(sCollectCheckpointsLocal.biCheckpointBlip[iCheckpointId])
		REMOVE_BLIP(sCollectCheckpointsLocal.biCheckpointBlip[iCheckpointId])
		sCollectCheckpointsLocal.biCheckpointBlip[iCheckpointId] = NULL
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - UNBLIP_COLLECT_CHECKPOINT - REMOVE_BLIP iCheckpointId ",iCheckpointId)
	ENDIF
ENDPROC

PROC DRAW_COLLECT_CHECKPOINT_UI()
	INT iCheckpoint
	REPEAT data.Checkpoint.iCount iCheckpoint
#IF IS_DEBUG_BUILD
		IF sDebugVars.Checkpoints.sDebugData[iCheckpoint].bUpdateCheckpoint
			sDebugVars.Checkpoints.sDebugData[iCheckpoint].bUpdateCheckpoint = FALSE
			UNDRAW_COLLECT_CHECKPOINT(iCheckpoint)
		ENDIF
		IF sDebugVars.Checkpoints.sDebugData[iCheckpoint].bDebugRender
			DEBUG_COLLECT_CHECKPOINT_RENDER_CORONA(iCheckpoint,GET_COLLECT_CHECKPOINT_COLLECT_RADIUS(iCheckpoint),HUD_COLOUR_BLUE)
			DEBUG_COLLECT_CHECKPOINT_RENDER_CORONA(iCheckpoint,GET_COLLECT_CHECKPOINT_VISUAL_RADIUS(iCheckpoint),HUD_COLOUR_RED)
		ENDIF
#ENDIF
		IF sCollectCheckpointsLocal.bUpdateCheckpoint[iCheckpoint]
		OR NOT CALL logic.Checkpoints.ShouldDraw(iCheckpoint)
			sCollectCheckpointsLocal.bUpdateCheckpoint[iCheckpoint] = FALSE
			UNDRAW_COLLECT_CHECKPOINT(iCheckpoint)
		ENDIF
		IF CALL logic.Checkpoints.ShouldDraw(iCheckpoint)
			DRAW_COLLECT_CHECKPOINT(iCheckpoint)
		ENDIF
		IF CALL logic.Checkpoints.ShouldDrawBlip(iCheckpoint)
		OR CALL logic.Checkpoints.ShouldDrawSmallBlip(iCheckpoint)
//				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - DRAW_COLLECT_CHECKPOINT_UI - BLIP_COLLECT_CHECKPOINT iCheckpoint ",iCheckpoint)
			BLIP_COLLECT_CHECKPOINT(iCheckpoint,CALL logic.Checkpoints.ShouldDrawSmallBlip(iCheckpoint))
		ELSE
//				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - DRAW_COLLECT_CHECKPOINT_UI - UNBLIP_COLLECT_CHECKPOINT iCheckpoint ",iCheckpoint)
			UNBLIP_COLLECT_CHECKPOINT(iCheckpoint)
		ENDIF
	ENDREPEAT
ENDPROC

PROC DEFAULT_SET_NEXT_CHECKPOINT()
	IF playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId < data.Checkpoint.iCount-1
		playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId++
	ENDIF
ENDPROC

PROC DEFAULT_ON_LAP_COMPLETE()
	SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_ALL_CHECKPOINTS_COLLECTED)
ENDPROC

FUNC BOOL DEFAULT_COLLECT_CHECKPOINTS_ENABLE()
	RETURN GET_CLIENT_MODE_STATE() = eMODESTATE_COLLECT_CHECKPOINTS
ENDFUNC

FUNC BOOL DEFAULT_COLLECT_CHECKPOINTS_SHOULD_DRAW(INT iCheckpointId)
	RETURN CALL logic.Checkpoints.Enable() AND iCheckpointId = playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId
ENDFUNC

FUNC BOOL DEFAULT_COLLECT_CHECKPOINTS_SHOULD_DRAW_BLIP(INT iCheckpointId)
	RETURN CALL logic.Checkpoints.ShouldDraw(iCheckpointId)
ENDFUNC

FUNC BOOL DEFAULT_COLLECT_CHECKPOINTS_SHOULD_DRAW_NEXT_BLIP(INT iCheckpointId)
	UNUSED_PARAMETER(iCheckpointId)
	RETURN FALSE
ENDFUNC

FUNC BOOL DEFAULT_COLLECT_CHECKPOINTS_EVENTS_ENABLED()
	RETURN FALSE
ENDFUNC

FUNC BOOL DEFAULT_COLLECT_CHECKPOINTS_CAN_COLLECT(INT iCheckpointId)
	UNUSED_PARAMETER(iCheckpointId)
	RETURN TRUE
ENDFUNC

FUNC BOOL DEFAULT_COLLECT_CHECKPOINTS_CAN_MISS(INT iCheckpointId)
	UNUSED_PARAMETER(iCheckpointId)
	RETURN FALSE
ENDFUNC

FUNC BOOL DEFAULT_COLLECT_CHECKPOINTS_HAS_MISSED(INT iCheckpointId)
	UNUSED_PARAMETER(iCheckpointId)
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_COLLECTED_CHECKPOINT(INT iCheckpointId = -1)
	IF iCheckpointId = -1
		RETURN HAS_COLLECTED_CHECKPOINT(playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId)
	ENDIF
	IF NOT CALL logic.Checkpoints.CanCollectCheckpoint(iCheckpointId)
		RETURN FALSE
	ENDIF
	IF playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iLastCollectedCheckpointId = iCheckpointId
		RETURN FALSE
	ENDIF
	IF IS_PED_IN_ANY_VEHICLE(LOCAL_PED_INDEX)
		IF VDIST(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(LOCAL_PED_INDEX)),GET_COLLECT_CHECKPOINT_COORDS(iCheckpointId)) < GET_COLLECT_CHECKPOINT_COLLECT_RADIUS(iCheckpointId)*0.5
			playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iLastCollectedCheckpointId = iCheckpointId
			RETURN TRUE
		ENDIF
	ENDIF
	IF VDIST(LOCAL_PLAYER_COORD,GET_COLLECT_CHECKPOINT_COORDS(iCheckpointId)) < GET_COLLECT_CHECKPOINT_COLLECT_RADIUS(iCheckpointId)*0.5
		playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iLastCollectedCheckpointId = iCheckpointId
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_COMPLETED_LAP()
	IF CALL logic.Checkpoints.CanMissCheckpoint(playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId)
		RETURN (playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCheckpointsCollected + playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCheckpointsMissed) = data.Checkpoint.iCount
	ENDIF
	RETURN playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCheckpointsCollected = data.Checkpoint.iCount
ENDFUNC

FUNC INT RESEND_COLLECT_CHECKPOINTS_EVENT_TIME
	RETURN 500
ENDFUNC

//Broadcasts an collect checkpoint event. Used to notify the server when a client has complete a lap (can be expanded adding new event types)
PROC BROADCAST_EVENT_COLLECT_CHECKPOINTS(COLLECT_CHECKPOINTS_EVENT_TYPE eCollectCheckpointsEventType, PARTICIPANT_INDEX piParticipant)
	SCRIPT_EVENT_DATA_COLLECT_CHECKPOINTS Event
	Event.Details.Type = SCRIPT_EVENT_COLLECT_CHECKPOINTS
	Event.Details.FromPlayerIndex = LOCAL_PLAYER_INDEX
	
	Event.eCollectCheckpointsEventType = eCollectCheckpointsEventType
	Event.piParticipant = piParticipant
	Event.smiiMission.iType = sMission.iType
	Event.smiiMission.iInstance = sMission.iInstance
	Event.iCheckpointsCollected = playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCheckpointsCollected
	Event.iCheckpointsMissed = playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCheckpointsMissed
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - BROADCAST_EVENT_COLLECT_CHECKPOINTS - eEventType = ", GET_COLLECT_CHECKPOINTS_EVENT_TYPE_DEBUG_STRING(eCollectCheckpointsEventType), ", piParticipant ", NATIVE_TO_INT(piParticipant), ", iCheckpointsCollected ",Event.iCheckpointsCollected," GET_CLOUD_TIME_AS_INT() = ", GET_CLOUD_TIME_AS_INT())
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - BROADCAST_EVENT_COLLECT_CHECKPOINTS - sMission.iType ",sMission.iType," sMission.iInstance ",sMission.iInstance)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - BROADCAST_EVENT_COLLECT_CHECKPOINTS - Event.smiiMission.iType ",Event.smiiMission.iType," Event.smiiMission.iInstance ",Event.smiiMission.iInstance)
	
	INT iPlayersFlag = ALL_PLAYERS_ON_SCRIPT()
	
	IF iPlayersFlag != 0
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - BROADCAST_EVENT_COLLECT_CHECKPOINTS - called by local player.")
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayersFlag)	
	ELSE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - BROADCAST_EVENT_COLLECT_CHECKPOINTS - iPlayersFlag = 0, not broadcasting.")
	ENDIF
ENDPROC

PROC PROCESS_SCRIPT_EVENT_COLLECT_CHECKPOINTS(INT iCount)
	IF NOT CALL logic.Checkpoints.EventsEnabled()
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_SCRIPT_EVENT_COLLECT_CHECKPOINTS - NOT CALL logic.Checkpoints.EventsEnabled()")
		EXIT
	ENDIF
	SCRIPT_EVENT_DATA_COLLECT_CHECKPOINTS sCollectCheckpointDetails

	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sCollectCheckpointDetails, SIZE_OF(sCollectCheckpointDetails))
		IF sMission.iType != sCollectCheckpointDetails.smiiMission.iType
		OR sMission.iInstance != sCollectCheckpointDetails.smiiMission.iInstance
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_SCRIPT_EVENT_COLLECT_CHECKPOINTS - sMission.iType ",sMission.iType," sMission.iInstance ",sMission.iInstance)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_SCRIPT_EVENT_COLLECT_CHECKPOINTS - sCollectCheckpointDetails.smiiMission.iType ",sCollectCheckpointDetails.smiiMission.iType," sCollectCheckpointDetails.smiiMission.iInstance ",sCollectCheckpointDetails.smiiMission.iInstance)
			EXIT
		ENDIF
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - PROCESS_SCRIPT_EVENT_COLLECT_CHECKPOINTS - Received SCRIPT_EVENT_DATA_COLLECT_CHECKPOINTS from ", GET_PLAYER_NAME(sCollectCheckpointDetails.Details.FromPlayerIndex), " with eEventType = ", GET_COLLECT_CHECKPOINTS_EVENT_TYPE_DEBUG_STRING(sCollectCheckpointDetails.eCollectCheckpointsEventType), " at GET_CLOUD_TIME_AS_INT() = ", GET_CLOUD_TIME_AS_INT())
		
		SWITCH sCollectCheckpointDetails.eCollectCheckpointsEventType
			CASE eCOLLECTCHECKPOINTSEVENTTYPE_CHECKPOINT_COLLECTED
				//This is a notification (usually from a client) to inform the client collected a checkpoint, and send the time.
				IF IS_LOCAL_HOST
					IF logic.Checkpoints.ProcessCheckpointoCollectedEvent != NULL
						CALL logic.Checkpoints.ProcessCheckpointoCollectedEvent(sCollectCheckpointDetails.piParticipant,sCollectCheckpointDetails.iCheckpointsCollected)
					ENDIF
					BROADCAST_EVENT_COLLECT_CHECKPOINTS(eCOLLECTCHECKPOINTSEVENTTYPE_CHECKPOINT_COLLECTED_ACK,sCollectCheckpointDetails.piParticipant)
				ENDIF
			BREAK
			CASE eCOLLECTCHECKPOINTSEVENTTYPE_CHECKPOINT_COLLECTED_ACK
				//This is a notification (usually from the server) to inform the server has received and processed the collected checkpoint event.
				IF sCollectCheckpointDetails.piParticipant = LOCAL_PARTICIPANT_INDEX
					CLEAR_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_SENDING_EVENT_CHECKPOINT_COLLECTED)
				ENDIF
			BREAK
			CASE eCOLLECTCHECKPOINTSEVENTTYPE_CHECKPOINT_MISSED
				//This is a notification (usually from a client) to inform the client missed a checkpoint, and send the time.
				IF IS_LOCAL_HOST
					IF logic.Checkpoints.ProcessCheckpointMissedEvent != NULL
						CALL logic.Checkpoints.ProcessCheckpointMissedEvent(sCollectCheckpointDetails.piParticipant,sCollectCheckpointDetails.iCheckpointsCollected,sCollectCheckpointDetails.iCheckpointsMissed)
					ENDIF
					BROADCAST_EVENT_COLLECT_CHECKPOINTS(eCOLLECTCHECKPOINTSEVENTTYPE_CHECKPOINT_MISSED_ACK,sCollectCheckpointDetails.piParticipant)
				ENDIF
			BREAK
			CASE eCOLLECTCHECKPOINTSEVENTTYPE_CHECKPOINT_MISSED_ACK
				//This is a notification (usually from the server) to inform the server has received and processed the missed checkpoint event.
				IF sCollectCheckpointDetails.piParticipant = LOCAL_PARTICIPANT_INDEX
					CLEAR_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_SENDING_EVENT_CHECKPOINT_MISSED)
				ENDIF
			BREAK
			CASE eCOLLECTCHECKPOINTSEVENTTYPE_LAP_COMPLETE
				//This is a notification (usually from a client) to inform the client finished a lap, and send the time.
				IF IS_LOCAL_HOST
					IF logic.Checkpoints.ProcessLapCompleteEvent != NULL
						CALL logic.Checkpoints.ProcessLapCompleteEvent(sCollectCheckpointDetails.piParticipant)
					ENDIF
					BROADCAST_EVENT_COLLECT_CHECKPOINTS(eCOLLECTCHECKPOINTSEVENTTYPE_LAP_COMPLETE_ACK,sCollectCheckpointDetails.piParticipant)
				ENDIF
			BREAK
			CASE eCOLLECTCHECKPOINTSEVENTTYPE_LAP_COMPLETE_ACK
				//This is a notification (usually from the server) to inform the server has received and processed the lap complete event.
				IF sCollectCheckpointDetails.piParticipant = LOCAL_PARTICIPANT_INDEX
					CLEAR_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_SENDING_EVENT_LAP_COMPLETE)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC MAINTAIN_COLLECT_CHECKPOINTS_NORMALS()
	VECTOR vCheckPointGroundPos, vFinalNormal, vHitNormal, vHitPoint
	ENTITY_INDEX eiHitEntity
	INT iHitCount
	INT iChpt
	INT iCurrentCheckpoint = playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId
	INT iCheckpoint

	REPEAT 3 iChpt
		iCheckpoint = iCurrentCheckpoint + iChpt
		IF iCheckpoint < data.Checkpoint.iCount
			IF GET_COLLECT_CHECKPOINT_APPLY_STEEP_FIX(iCheckpoint) // we don't need to do this if the fix is not applied
				vCheckPointGroundPos = GET_COLLECT_CHECKPOINT_COORDS(iCheckpoint)
				IF NOT sCollectCheckpointsLocal.bCheckpointClipChecked[iCheckpoint]
					
					IF sCollectCheckpointsLocal.stiCheckpoint[iCheckpoint] = NULL
						IF NOT IS_VECTOR_ZERO(vCheckPointGroundPos)
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_COLLECT_CHECKPOINTS_NORMALS checkpoint = ",iCheckpoint," - START_SHAPE_TEST_LOS_PROBE(", vCheckPointGroundPos, " + <<0.0, 0.0, 1.0>>, ", vCheckPointGroundPos, " - <<0.0, 0.0, 1.0>>, SCRIPT_INCLUDE_OBJECT)")
							sCollectCheckpointsLocal.stiCheckpoint[iCheckpoint] = START_EXPENSIVE_SYNCHRONOUS_SHAPE_TEST_LOS_PROBE(vCheckPointGroundPos + <<0.0, 0.0, 1.0>>, vCheckPointGroundPos - <<0.0, 0.0, 1.0>>, 
												SCRIPT_INCLUDE_ALL, NULL, SCRIPT_SHAPETEST_OPTION_IGNORE_NO_COLLISION | SCRIPT_SHAPETEST_OPTION_IGNORE_GLASS)
						ENDIF
					ENDIF
					IF GET_SHAPE_TEST_RESULT(sCollectCheckpointsLocal.stiCheckpoint[iCheckpoint], iHitCount, vHitPoint, vHitNormal, eiHitEntity) = SHAPETEST_STATUS_RESULTS_READY
						IF iHitCount = 0
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_COLLECT_CHECKPOINTS_NORMALS checkpoint = ",iCheckpoint," - iHitCount = ", iHitCount)
							sCollectCheckpointsLocal.stiCheckpoint[iCheckpoint] = NULL
						ELSE
							vFinalNormal = vHitNormal
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_COLLECT_CHECKPOINTS_NORMALS checkpoint = ",iCheckpoint," - iHitCount = ", iHitCount)
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_COLLECT_CHECKPOINTS_NORMALS checkpoint = ",iCheckpoint," - vHitPoint = ", vHitPoint)
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_COLLECT_CHECKPOINTS_NORMALS checkpoint = ",iCheckpoint," - vFinalNormal = ", vFinalNormal)
							sCollectCheckpointsLocal.vCoordsNormal[iCheckpoint] = vFinalNormal
							sCollectCheckpointsLocal.bCheckpointClipChecked[iCheckpoint] = TRUE
							sCollectCheckpointsLocal.stiCheckpoint[iCheckpoint] = NULL
							sCollectCheckpointsLocal.bUpdateCheckpoint[iCheckpoint] = TRUE
						ENDIF
					ENDIF
				ENDIF
	//			PRINTLN("[MAINTAIN_COLLECT_CHECKPOINTS_NORMALS] - DRAW LINE ",vCheckPointGroundPos," + <<0.0, 0.0, 1.0>>, ",vCheckPointGroundPos," - <<0.0, 0.0, 1.0>>")
	//			DRAW_LINE(vCheckPointGroundPos+<<0.0, 0.0, 1.0>>,vCheckPointGroundPos-<<0.0, 0.0, 1.0>>,255,0,0)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_COLLECT_CHECKPOINTS
	IF CALL logic.Checkpoints.Enable()
		IF logic.Checkpoints.Maintain != NULL
			CALL logic.Checkpoints.Maintain()
		ENDIF

		IF HAS_COLLECTED_CHECKPOINT()
			playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCheckpointsCollected++
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_CHECKPOINTS - playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCheckpointsCollected ",playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCheckpointsCollected)
			IF logic.Checkpoints.OnCollect != NULL 
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_CHECKPOINTS - logic.Checkpoints.OnCollect(",playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId,")")
				CALL logic.Checkpoints.OnCollect(playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId) 
			ENDIF
			IF CALL logic.Checkpoints.EventsEnabled()
				SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_SENDING_EVENT_CHECKPOINT_COLLECTED)
			ENDIF
			CALL logic.Checkpoints.SetNextCheckpoint()
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_CHECKPOINTS - CALL logic.Checkpoints.SetNextCheckpoint()")
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_CHECKPOINTS - playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId ",playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId)
			IF HAS_COMPLETED_LAP()
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_CHECKPOINTS - CALL logic.Checkpoints.OnLapComplete()")
				CALL logic.Checkpoints.OnLapComplete()
				IF CALL logic.Checkpoints.EventsEnabled()
					SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_SENDING_EVENT_LAP_COMPLETE)
				ENDIF
			ENDIF
		ELSE
			IF CALL logic.Checkpoints.CanMissCheckpoint(playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId)
				IF logic.Checkpoints.HasMissedCheckpoint != NULL
					IF CALL logic.Checkpoints.HasMissedCheckpoint(playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId)
						playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCheckpointsMissed++
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_CHECKPOINTS - playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCheckpointsMissed ",playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCheckpointsMissed)
						IF logic.Checkpoints.OnMiss != NULL 
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_CHECKPOINTS - logic.Checkpoints.OnMiss(",playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId,")")
							CALL logic.Checkpoints.OnMiss(playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId) 
						ENDIF
						IF CALL logic.Checkpoints.EventsEnabled()
							SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_SENDING_EVENT_CHECKPOINT_MISSED)
						ENDIF
						CALL logic.Checkpoints.SetNextCheckpoint()
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_CHECKPOINTS - CALL logic.Checkpoints.SetNextCheckpoint()")
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_CHECKPOINTS - playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId ",playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId)
						IF HAS_COMPLETED_LAP()
							PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_CHECKPOINTS - CALL logic.Checkpoints.OnLapComplete()")
							CALL logic.Checkpoints.OnLapComplete()
							IF CALL logic.Checkpoints.EventsEnabled()
								SET_GENERIC_CLIENT_BIT(eGENERICCLIENTBITSET_SENDING_EVENT_LAP_COMPLETE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF CALL logic.Checkpoints.EventsEnabled()
			IF NOT HAS_NET_TIMER_STARTED(sCollectCheckpointsLocal.stEventTimer)
				IF IS_GENERIC_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX,eGENERICCLIENTBITSET_SENDING_EVENT_CHECKPOINT_COLLECTED)
					BROADCAST_EVENT_COLLECT_CHECKPOINTS(eCOLLECTCHECKPOINTSEVENTTYPE_CHECKPOINT_COLLECTED,LOCAL_PARTICIPANT_INDEX)
					START_NET_TIMER(sCollectCheckpointsLocal.stEventTimer)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_CHECKPOINTS - Broadcasting checkpoint collected. Timer Started ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sCollectCheckpointsLocal.stEventTimer))
				ENDIF
				IF IS_GENERIC_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX,eGENERICCLIENTBITSET_SENDING_EVENT_LAP_COMPLETE)
					BROADCAST_EVENT_COLLECT_CHECKPOINTS(eCOLLECTCHECKPOINTSEVENTTYPE_LAP_COMPLETE,LOCAL_PARTICIPANT_INDEX)
					START_NET_TIMER(sCollectCheckpointsLocal.stEventTimer)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_CHECKPOINTS - Broadcasting lap complete. Timer Started ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sCollectCheckpointsLocal.stEventTimer))
				ENDIF
				IF IS_GENERIC_CLIENT_BIT_SET(LOCAL_PARTICIPANT_INDEX,eGENERICCLIENTBITSET_SENDING_EVENT_CHECKPOINT_MISSED)
					BROADCAST_EVENT_COLLECT_CHECKPOINTS(eCOLLECTCHECKPOINTSEVENTTYPE_CHECKPOINT_MISSED,LOCAL_PARTICIPANT_INDEX)
					START_NET_TIMER(sCollectCheckpointsLocal.stEventTimer)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_CHECKPOINTS - Broadcasting checkpoint collected. Timer Started ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sCollectCheckpointsLocal.stEventTimer))
				ENDIF
			ENDIF
			IF  HAS_NET_TIMER_STARTED(sCollectCheckpointsLocal.stEventTimer)
			AND HAS_NET_TIMER_EXPIRED_READ_ONLY(sCollectCheckpointsLocal.stEventTimer,RESEND_COLLECT_CHECKPOINTS_EVENT_TIME())
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_COLLECT_CHECKPOINTS - Reseting timer. Timer Expired ",GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sCollectCheckpointsLocal.stEventTimer))
				RESET_NET_TIMER(sCollectCheckpointsLocal.stEventTimer)
			ENDIF
		ENDIF
		
	
	ENDIF
	MAINTAIN_COLLECT_CHECKPOINTS_NORMALS()
	DRAW_COLLECT_CHECKPOINT_UI()
ENDPROC
#ENDIF

