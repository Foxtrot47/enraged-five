USING "fm_content_generic_support.sch"

//----------------------
//	Custom Spawns
//----------------------

#IF NOT DEFINED(SHOULD_USE_CUSTOM_SPAWN_POINTS)
FUNC BOOL SHOULD_USE_CUSTOM_SPAWN_POINTS()
	RETURN FALSE
ENDFUNC
#ENDIF

#IF NOT DEFINED(IS_CUSTOM_SPAWN_POINT_VALID)
FUNC BOOL IS_CUSTOM_SPAWN_POINT_VALID(INT iSpawnPoint)
	UNUSED_PARAMETER(iSpawnPoint)
	RETURN TRUE
ENDFUNC
#ENDIF

#IF MAX_NUM_CUSTOM_SPAWNS
#IF NOT DEFINED(MAINTAIN_CUSTOM_SPAWN_POINTS)
PROC MAINTAIN_CUSTOM_SPAWN_POINTS()

	IF IS_LOCAL_PLAYER_PARTICIPATING()
	AND IS_SAFE_TO_DISPLAY_MISSION_UI(DEFAULT, TRUE)
	AND CALL logic.Player.Spawning.Custom.Enable()
		IF NOT IS_GENERIC_BIT_SET(eGENERICBITSET_USING_CUSTOM_SPAWN_POINTS)

			USE_CUSTOM_SPAWN_POINTS(TRUE, TRUE,FALSE,CUSTOM_SPAWN_PED_CLEARANCE_RADIUS, CUSTOM_SPAWN_VEHICLE_CLEARANCE_RADIUS, CUSTOM_SPAWN_WORLD_OBJECT_CLEARANCE_RADIUS, CUSTOM_SPAWN_SCRIPT_OBJECT_CLEARANCE_RADIUS, CUSTOM_SPAWN_MIN_DIST_WARPER, CUSTOM_SPAWN_MIN_DIST_ENEMY, CUSTOM_SPAWN_MIN_DIST_TEAMMATE, FALSE, TRUE)
			
			VECTOR vPos
			FLOAT fHeading
			
			INT iSpawnPoint
			REPEAT data.CustomSpawns.iCount iSpawnPoint
				// url:bugstar:7559897 - Framework Content - Investigate allowing the user to place different stages of custom spawns to facilitate missions taking place at multiple locations
				IF logic.Player.Spawning.Custom.SpawnOverride != NULL AND CALL logic.Player.Spawning.Custom.SpawnOverride(iSpawnPoint, vPos, fHeading)
					// using override
					PRINTLN("[",scriptName,"] - MAINTAIN_CUSTOM_SPAWN_POINTS - Spawn point ",iSpawnPoint," override ",vPos," ",fHeading)
				ELSE
					// using data set
					vPos = data.CustomSpawns.Spawns[iSpawnPoint].vCoords
					fHeading = data.CustomSpawns.Spawns[iSpawnPoint].fHeading
				ENDIF
				
				IF NOT IS_VECTOR_ZERO(vPos)
				AND CALL logic.Player.Spawning.Custom.IsSpawnPointValid(iSpawnPoint)
					ADD_CUSTOM_SPAWN_POINT(vPos, fHeading)
					PRINTLN("[",scriptName,"] - MAINTAIN_CUSTOM_SPAWN_POINTS - Spawn point ",iSpawnPoint," added at ", vPos," ",fHeading)
				ELSE
					PRINTLN("[",scriptName,"] - MAINTAIN_CUSTOM_SPAWN_POINTS - Spawn point ",iSpawnPoint," not added as position was origin.")
				ENDIF
			ENDREPEAT
			
			IF logic.Player.Spawning.Custom.SpawnVehicle != null
				SET_PLAYER_RESPAWN_IN_VEHICLE(TRUE, CALL logic.Player.Spawning.Custom.SpawnVehicle(), DEFAULT, DEFAULT, FALSE, FALSE, DEFAULT, DEFAULT, TRUE,TRUE, DEFAULT)
			ENDIF
						
			SET_GENERIC_BIT(eGENERICBITSET_USING_CUSTOM_SPAWN_POINTS)
		ENDIF
	ELSE
		IF IS_GENERIC_BIT_SET(eGENERICBITSET_USING_CUSTOM_SPAWN_POINTS)
			IF  NOT IS_PED_INJURED(LOCAL_PED_INDEX)
			AND NOT IS_PLAYER_RESPAWNING(LOCAL_PLAYER_INDEX)
				CLEAR_CUSTOM_SPAWN_POINTS()
			
				USE_CUSTOM_SPAWN_POINTS(FALSE)
				
				IF logic.Player.Spawning.Custom.SpawnVehicle != null
					SET_PLAYER_RESPAWN_IN_VEHICLE(FALSE) 
				ENDIF
								
				CLEAR_GENERIC_BIT(eGENERICBITSET_USING_CUSTOM_SPAWN_POINTS)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF
#ENDIF

//----------------------
//	Respawn
//----------------------

FUNC BOOL IS_SAFE_TO_RESPAWN_PLAYER()

	IF NOT IS_LOCAL_ALIVE
	OR NETWORK_IS_PLAYER_IN_MP_CUTSCENE(LOCAL_PLAYER_INDEX)
	OR GET_CLIENT_MODE_STATE() = eMODESTATE_CUTSCENE
		RETURN FALSE	
	ENDIF
	
	#IF MAX_NUM_CUTSCENES
	IF IS_CUTSCENE_LOCAL_BIT_SET(eCUTSCENELOCALBITSET_STARTED_MP_CUTSCENE)
		RETURN FALSE	
	ENDIF
	#ENDIF
	
	IF IS_PLAYER_RESPAWNING(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC NET_SET_PLAYER_CONTROL_FLAGS GET_PLAYER_RESPAWNING_DISABLE_PLAYER_CONTROL_FLAGS(BOOL bDisable)
	UNUSED_PARAMETER(bDisable)
	
	RETURN NSPC_NONE
ENDFUNC

FUNC PLAYER_SPAWN_LOCATION GET_PLAYER_RESPAWNING_LOCATION()
	RETURN SPAWN_LOCATION_CUSTOM_SPAWN_POINTS
ENDFUNC

FUNC BOOL SHOULD_PLAYER_RESPAWNING_WARP_INTO_SPAWN_VEHICLE()
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_RESPAWNING_KEEP_CURRENT_VEHICLE()

	IF LOCAL_VEHICLE_MODEL = DUMMY_MODEL_FOR_SCRIPT
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_MODEL_A_BOAT(LOCAL_VEHICLE_MODEL)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC

FUNC BOOL SHOULD_PLAYER_RESPAWNING_USE_CAR_NODE_OFFSET()
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PLAYER_RESPAWNING_USE_START_OF_MISSION_PV_SPAWN()
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_RESPAWNING_PLAYER()
	
	IF NOT IS_GENERIC_BIT_SET(eGENERICBITSET_RESPAWN_PLAYER)
		EXIT
	ENDIF
	
	IF NOT IS_SAFE_TO_RESPAWN_PLAYER()
		CLEAR_GENERIC_BIT(eGENERICBITSET_RESPAWN_PLAYER)
		EXIT
	ENDIF
	
	IF NOT IS_GENERIC_BIT_SET(eGENERICBITSET_DISABLED_PLAYER_CONTROL)
		IF FADE_SCREEN_OUT()
			DISABLE_PLAYER_CONTROL(TRUE, CALL logic.Player.Spawning.Respawn.DisablePlayerControlFlags(TRUE))
			
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				// url:bugstar:6819376 respawn won't work if a load scene is active.
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [MAINTAIN_RESPAWNING_PLAYER] Stopping load scene")
				NEW_LOAD_SCENE_STOP()
			ENDIF
		ENDIF
	ELSE
		IF WARP_TO_SPAWN_LOCATION(	CALL logic.Player.Spawning.Respawn.Location(),
									FALSE,
									CALL logic.Player.Spawning.Respawn.WarpIntoSpawnVehicle(),
									DEFAULT, DEFAULT, DEFAULT, DEFAULT,
									CALL logic.Player.Spawning.Respawn.KeepCurrentVehicle(),
									CALL logic.Player.Spawning.Respawn.UseCarNodeOffset(),
									DEFAULT, DEFAULT, DEFAULT,
									CALL logic.Player.Spawning.Respawn.UseStartOfMissionPVSpawn())
			FADE_SCREEN_IN()
			DISABLE_PLAYER_CONTROL(FALSE, CALL logic.Player.Spawning.Respawn.DisablePlayerControlFlags(FALSE))
			CLEAR_GENERIC_BIT(eGENERICBITSET_RESPAWN_PLAYER)
		ENDIF
	ENDIF

ENDPROC

PROC MAINTAIN_RESPAWN_WEAPON()
	IF NOT IS_DATA_BIT_SET(eDATABITSET_EQUIP_LAST_WEAPON_ON_RESPAWN)
	OR NOT IS_PLAYER_PERMANENT(LOCAL_PLAYER_INDEX)
		EXIT
	ENDIF
	
	SWITCH GET_LOCAL_PLAYER_RESPAWN_STATE()
		CASE RESPAWN_STATE_DEAD
			IF NOT IS_GENERIC_BIT_SET(eGENERICBITSET_EQUIP_LAST_WEAPON_ON_RESPAWN)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_RESPAWN_WEAPON - The local player has died. Setting that they should equip their last weapon on respawn.")
				SET_GENERIC_BIT(eGENERICBITSET_EQUIP_LAST_WEAPON_ON_RESPAWN)
			ENDIF
		BREAK
		
		CASE RESPAWN_STATE_LOAD_SCENE
			IF IS_GENERIC_BIT_SET(eGENERICBITSET_EQUIP_LAST_WEAPON_ON_RESPAWN)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_RESPAWN_WEAPON - The local player is respawning. Equipping their last weapon, sRespawnWeapon.eWeapon = ", GET_WEAPON_TYPE_NAME_FOR_DEBUG(sRespawnWeapon.eWeapon), " (", ENUM_TO_INT(sRespawnWeapon.eWeapon), ").")
				
				//If the player had a melee weapon equipped, PUT_WEAPON_IN_HAND(WEAPONINHAND_LASTWEAPON_BOTH) selects the next non-melee weapon with available ammunition, so use SET_CURRENT_PED_WEAPON in this case.
				IF (sRespawnWeapon.eWeapon = WEAPONTYPE_UNARMED
					OR GET_WEAPONTYPE_GROUP(sRespawnWeapon.eWeapon) = WEAPONGROUP_MELEE)
				AND HAS_PED_GOT_WEAPON(LOCAL_PED_INDEX, sRespawnWeapon.eWeapon)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_RESPAWN_WEAPON - Setting current weapon directly.")
					SET_CURRENT_PED_WEAPON(LOCAL_PED_INDEX, sRespawnWeapon.eWeapon, TRUE)
				ELSE
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_RESPAWN_WEAPON - Setting current weapon using PUT_WEAPON_IN_HAND(WEAPONINHAND_LASTWEAPON_BOTH).")
					PUT_WEAPON_IN_HAND(WEAPONINHAND_LASTWEAPON_BOTH)
				ENDIF
				
				CLEAR_GENERIC_BIT(eGENERICBITSET_EQUIP_LAST_WEAPON_ON_RESPAWN)
			ENDIF
		BREAK
		
		CASE RESPAWN_STATE_PLAYING
			//If the player's current weapon is WEAPONTYPE_UNARMED, GET_CURRENT_PED_WEAPON returns FALSE and the global accessed by GET_PLAYER_CURRENT_HELD_WEAPON and SET_PLAYER_CURRENT_HELD_WEAPON is not updated, so maintain the current weapon here.
			WEAPON_TYPE eCurrentWeapon
			GET_CURRENT_PED_WEAPON(LOCAL_PED_INDEX, eCurrentWeapon, FALSE)
			
			IF sRespawnWeapon.eWeapon != eCurrentWeapon
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - MAINTAIN_RESPAWN_WEAPON - Updating sRespawnWeapon.eWeapon from ", GET_WEAPON_TYPE_NAME_FOR_DEBUG(sRespawnWeapon.eWeapon), " (", ENUM_TO_INT(sRespawnWeapon.eWeapon), ") to ", GET_WEAPON_TYPE_NAME_FOR_DEBUG(eCurrentWeapon), " (", ENUM_TO_INT(eCurrentWeapon), ").")
				sRespawnWeapon.eWeapon = eCurrentWeapon
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//----------------------
//	Population
//----------------------

#IF MAX_NUM_POP_BLOCKERS
PROC SET_POPULATION_BLOCKER(BOOL bBlock, INT iIndex)

	IF NOT IS_VECTOR_ZERO(data.Population.Blockers[iIndex].vMin)
	AND NOT IS_VECTOR_ZERO(data.Population.Blockers[iIndex].vMax)
		
		IF iIndex >= 10
			ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_POPULATION_BLOCKERS - About to create >=10 population blockers for one mission. This is excessive and can result in crashes. Adding no more.")
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_POPULATION_BLOCKERS ", iIndex," - About to create >=10 population blockers for one mission. This is excessive and can result in crashes. Adding no more.")
			EXIT
		ENDIF
	
		SWITCH data.Population.Blockers[iIndex].eType
			CASE ePOPULATIONBLOCKTYPE_SCENARIO
				IF bBlock
					IF iNumPopMultipliers < MAX_NUM_POP_MULTIPLIERS
						iPopMultiplierIndex[iNumPopMultipliers] = NATIVE_TO_INT(ADD_SCENARIO_BLOCKING_AREA(data.Population.Blockers[iIndex].vMin, data.Population.Blockers[iIndex].vMax))
						iNumPopMultipliers++
					ELSE
						ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_POPULATION_BLOCKERS - Reached the maximum number of population multipliers, not adding")
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_POPULATION_BLOCKERS ", iIndex," eType = ", data.Population.Blockers[iIndex].eType, " Reached the maximum number of population multipliers, not adding any more")
					ENDIF
				ELSE
					IF iNumPopMultipliers < MAX_NUM_POP_MULTIPLIERS
						IF iPopMultiplierIndex[iNumPopMultipliers] != -1
							REMOVE_SCENARIO_BLOCKING_AREA(INT_TO_NATIVE(SCENARIO_BLOCKING_INDEX, iPopMultiplierIndex[iNumPopMultipliers]))
							iPopMultiplierIndex[iNumPopMultipliers] = -1
							iNumPopMultipliers--
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE ePOPULATIONBLOCKTYPE_VEHICLE_GEN
				SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(data.Population.Blockers[iIndex].vMin, data.Population.Blockers[iIndex].vMax, !bBlock, FALSE)
			BREAK
			CASE ePOPULATIONBLOCKTYPE_TRAFFIC
				IF bBlock
					IF iNumPopMultipliers < MAX_NUM_POP_MULTIPLIERS
						iPopMultiplierIndex[iNumPopMultipliers] = ADD_POP_MULTIPLIER_AREA(data.Population.Blockers[iIndex].vMin, data.Population.Blockers[iIndex].vMax, 1.0, 0.25, TRUE)
						iNumPopMultipliers++
					ELSE
						ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_POPULATION_BLOCKERS - Reached the maximum number of population multipliers, not adding")
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_POPULATION_BLOCKERS ", iIndex," eType = ", data.Population.Blockers[iIndex].eType, " Reached the maximum number of population multipliers, not adding any more")
					ENDIF
				ELSE
					IF iPopMultiplierIndex[iNumPopMultipliers] != -1
						IF DOES_POP_MULTIPLIER_AREA_EXIST(iPopMultiplierIndex[iNumPopMultipliers])
							REMOVE_POP_MULTIPLIER_AREA(iPopMultiplierIndex[iNumPopMultipliers], TRUE)
							iPopMultiplierIndex[iNumPopMultipliers] = -1
							iNumPopMultipliers--
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		PRINTLN("[",scriptName,"] - SET_POPULATION_BLOCKERS ", iIndex," eType = ", data.Population.Blockers[iIndex].eType, " vMin = ",data.Population.Blockers[iIndex].vMin," vMax ", data.Population.Blockers[iIndex].vMax," bBlock = ",bBlock)
	ENDIF

ENDPROC

PROC SET_POPULATION_BLOCKERS(BOOL bBlock)

	INT iIndex
	
	IF bBlock
		REPEAT data.Population.iCount iIndex
			SET_POPULATION_BLOCKER(bBlock, iIndex)
		ENDREPEAT
	ELSE
		FOR iIndex = data.Population.iCount-1 TO 0 STEP -1
			SET_POPULATION_BLOCKER(bBlock, iIndex)
		ENDFOR
	ENDIF

ENDPROC
#ENDIF
