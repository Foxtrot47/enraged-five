USING "commands_datafile.sch"

FUNC BOOL DATADICT_IS_DICT_VALID(DATAFILE_DICT dfdDict)
	RETURN dfdDict != NULL
ENDFUNC

FUNC DATAFILE_TYPE SAFE_DATADICT_GET_TYPE(DATAFILE_DICT dfdDict, STRING sName)
	IF NOT DATADICT_IS_DICT_VALID(dfdDict)
		RETURN DF_NONE
	ENDIF
	RETURN DATADICT_GET_TYPE(dfdDict, sName)
ENDFUNC

PROC SAFE_DATADICT_GET_DICT(DATAFILE_DICT &dfdDict, DATAFILE_DICT dfdParent, STRING sName)
	IF NOT DATADICT_IS_DICT_VALID(dfdParent)
		PRINTLN("SAFE_DATADICT_GET_DICT - Parent dict of ", sName, " is NULL")
		EXIT
	ENDIF
	IF (NOT DATADICT_IS_DICT_VALID(dfdDict)) AND DATADICT_GET_TYPE(dfdParent, sName) = DF_DICT
		PRINTLN("SAFE_DATADICT_GET_DICT - Getting dict with handle: ", sName)
		dfdDict = DATADICT_GET_DICT(dfdParent, sName)
		IF NOT DATADICT_IS_DICT_VALID(dfdDict)
			PRINTLN("SAFE_DATADICT_GET_DICT - Dict ", sName, " is NULL")
			ASSERTLN("SAFE_DATADICT_GET_DICT - Dict ", sName, " is NULL")
		ENDIF
	ENDIF
	IF NOT DATADICT_IS_DICT_VALID(dfdDict)
		PRINTLN("SAFE_DATADICT_GET_DICT - Could not find dict named ", sName, ", type is ", ENUM_TO_INT(DATADICT_GET_TYPE(dfdParent, sName)))
	ENDIF
ENDPROC

// dictionary create
PROC SAFE_DATADICT_CREATE_DICT(DATAFILE_DICT &dfdDict, DATAFILE_DICT dfdParent, STRING sName)
	IF DATADICT_GET_TYPE(dfdParent, sName) <> DF_DICT
		PRINTLN("SAFE_DATADICT_CREATE_DICT - Creating dict with handle: ", sName)
		dfdDict = DATADICT_CREATE_DICT(dfdParent, sName)	
	ELSE
		SAFE_DATADICT_GET_DICT(dfdDict, dfdParent, sName)
	ENDIF
ENDPROC

// arrayed getters
FUNC BOOL SAFE_ARRAY_GET_BOOL(DATAFILE_ARRAY dfArray, INT iIndex, BOOL bDefault, BOOL bUseDataValue)
	IF bUseDataValue
	AND iIndex < DATAARRAY_GET_COUNT(dfArray)
		RETURN DATAARRAY_GET_BOOL(dfArray, iIndex)
	ENDIF
	RETURN bDefault
ENDFUNC

FUNC INT SAFE_ARRAY_GET_INT(DATAFILE_ARRAY dfArray, INT iIndex, INT iDefault, BOOL bUseDataValue)
	IF bUseDataValue
	AND iIndex < DATAARRAY_GET_COUNT(dfArray)
		RETURN DATAARRAY_GET_INT(dfArray, iIndex)
	ENDIF
	RETURN iDefault
ENDFUNC

FUNC INT SAFE_ARRAY_GET_ENUM(DATAFILE_ARRAY dfArray, INT iIndex, ENUM_TO_INT iDefault, BOOL bUseDataValue)
	IF bUseDataValue
	AND iIndex < DATAARRAY_GET_COUNT(dfArray)
		RETURN DATAARRAY_GET_INT(dfArray, iIndex)
	ENDIF
	RETURN ENUM_TO_INT(iDefault)
ENDFUNC

FUNC FLOAT SAFE_ARRAY_GET_FLOAT(DATAFILE_ARRAY dfArray, INT iIndex, FLOAT fDefault, BOOL bUseDataValue)
	IF bUseDataValue
	AND iIndex < DATAARRAY_GET_COUNT(dfArray)
		RETURN DATAARRAY_GET_FLOAT(dfArray, iIndex)
	ENDIF
	RETURN fDefault
ENDFUNC

FUNC VECTOR SAFE_ARRAY_GET_VECTOR(DATAFILE_ARRAY dfArray, INT iIndex, VECTOR vDefault, BOOL bUseDataValue)
	IF bUseDataValue
	AND iIndex < DATAARRAY_GET_COUNT(dfArray)
		RETURN DATAARRAY_GET_VECTOR(dfArray, iIndex)
	ENDIF
	RETURN vDefault
ENDFUNC

FUNC STRING SAFE_ARRAY_GET_STRING(DATAFILE_ARRAY dfArray, INT iIndex, STRING sDefault, BOOL bUseDataValue)
	IF bUseDataValue
	AND iIndex < DATAARRAY_GET_COUNT(dfArray)
		RETURN DATAARRAY_GET_STRING(dfArray, iIndex)
	ENDIF
	RETURN sDefault
ENDFUNC

FUNC TEXT_LABEL_3 SAFE_ARRAY_GET_TEXT_LABEL_3(DATAFILE_ARRAY dfArray, INT iIndex, STRING sDefault, BOOL bUseDataValue)
	TEXT_LABEL_3 tl
	IF bUseDataValue
	AND iIndex < DATAARRAY_GET_COUNT(dfArray)
		tl = DATAARRAY_GET_STRING(dfArray, iIndex)
	ELSE
		tl = sDefault
	ENDIF
	RETURN tl
ENDFUNC

FUNC TEXT_LABEL_7 SAFE_ARRAY_GET_TEXT_LABEL_7(DATAFILE_ARRAY dfArray, INT iIndex, STRING sDefault, BOOL bUseDataValue)
	TEXT_LABEL_7 tl
	IF bUseDataValue
	AND iIndex < DATAARRAY_GET_COUNT(dfArray)
		tl = DATAARRAY_GET_STRING(dfArray, iIndex)
	ELSE
		tl = sDefault
	ENDIF
	RETURN tl
ENDFUNC

FUNC TEXT_LABEL_15 SAFE_ARRAY_GET_TEXT_LABEL_15(DATAFILE_ARRAY dfArray, INT iIndex, STRING sDefault, BOOL bUseDataValue)
	TEXT_LABEL_15 tl
	IF bUseDataValue
	AND iIndex < DATAARRAY_GET_COUNT(dfArray)
		tl = DATAARRAY_GET_STRING(dfArray, iIndex)
	ELSE
		tl = sDefault
	ENDIF
	RETURN tl
ENDFUNC

FUNC TEXT_LABEL_23 SAFE_ARRAY_GET_TEXT_LABEL_23(DATAFILE_ARRAY dfArray, INT iIndex, STRING sDefault, BOOL bUseDataValue)
	TEXT_LABEL_23 tl
	IF bUseDataValue
	AND iIndex < DATAARRAY_GET_COUNT(dfArray)
		tl = DATAARRAY_GET_STRING(dfArray, iIndex)
	ELSE
		tl = sDefault
	ENDIF
	RETURN tl
ENDFUNC

FUNC TEXT_LABEL_31 SAFE_ARRAY_GET_TEXT_LABEL_31(DATAFILE_ARRAY dfArray, INT iIndex, STRING sDefault, BOOL bUseDataValue)
	TEXT_LABEL_31 tl
	IF bUseDataValue
	AND iIndex < DATAARRAY_GET_COUNT(dfArray)
		tl = DATAARRAY_GET_STRING(dfArray, iIndex)
	ELSE
		tl = sDefault
	ENDIF
	RETURN tl
ENDFUNC

FUNC TEXT_LABEL_63 SAFE_ARRAY_GET_TEXT_LABEL_63(DATAFILE_ARRAY dfArray, INT iIndex, STRING sDefault, BOOL bUseDataValue)
	TEXT_LABEL_63 tl
	IF bUseDataValue
	AND iIndex < DATAARRAY_GET_COUNT(dfArray)
		tl = DATAARRAY_GET_STRING(dfArray, iIndex)
	ELSE
		tl = sDefault
	ENDIF
	RETURN tl
ENDFUNC

FUNC DATAFILE_ARRAY SAFE_GET_ARRAY(DATAFILE_ARRAY dfArray, INT iIndex)
	IF iIndex < DATAARRAY_GET_COUNT(dfArray)
		RETURN DATAARRAY_GET_ARRAY(dfArray, iIndex)
	ENDIF
	RETURN NULL
ENDFUNC

// non arrayed getters
FUNC BOOL SAFE_GET_BOOL(DATAFILE_DICT dfDict, STRING sHandle, BOOL bDefault)
	IF  DATADICT_IS_DICT_VALID(dfDict)
	AND DATADICT_GET_TYPE(dfDict, sHandle) = DF_BOOL
		RETURN DATADICT_GET_BOOL(dfDict, sHandle)
	ENDIF
	RETURN bDefault
ENDFUNC

FUNC INT SAFE_GET_INT(DATAFILE_DICT dfDict, STRING sHandle, INT iDefault)
	IF  DATADICT_IS_DICT_VALID(dfDict)
	AND DATADICT_GET_TYPE(dfDict, sHandle) = DF_INT
		RETURN DATADICT_GET_INT(dfDict, sHandle)
	ENDIF
	RETURN iDefault
ENDFUNC

FUNC INT SAFE_GET_ENUM(DATAFILE_DICT dfDict, STRING sHandle, ENUM_TO_INT iDefault)
	IF DATADICT_IS_DICT_VALID(dfDict)
	AND DATADICT_GET_TYPE(dfDict, sHandle) = DF_INT
		RETURN DATADICT_GET_INT(dfDict, sHandle)
	ENDIF
	RETURN ENUM_TO_INT(iDefault)
ENDFUNC

FUNC INT SAFE_GET_CAPPED_INT(DATAFILE_DICT dfDict, STRING sHandle, INT iDefault, INT iCap)
	INT iReturn = SAFE_GET_INT(dfDict, sHandle, iDefault)
	IF iReturn > iCap
		PRINTLN("SAFE_GET_CAPPED_INT - Capping! Handle: ", sHandle, ", value: ", iReturn, ", cap:", iCap)
		iReturn = iCap
	ENDIF
	RETURN iReturn
ENDFUNC

FUNC FLOAT SAFE_GET_FLOAT(DATAFILE_DICT dfDict, STRING sHandle, FLOAT fDefault)
	IF  DATADICT_IS_DICT_VALID(dfDict)
	AND DATADICT_GET_TYPE(dfDict, sHandle) = DF_FLOAT
		RETURN DATADICT_GET_FLOAT(dfDict, sHandle)
	ELSE
		RETURN fDefault
	ENDIF
ENDFUNC

FUNC VECTOR SAFE_GET_VECTOR(DATAFILE_DICT dfDict, STRING sHandle, VECTOR vDefault)
	IF  DATADICT_IS_DICT_VALID(dfDict)
	AND DATADICT_GET_TYPE(dfDict, sHandle) = DF_VEC3
		RETURN DATADICT_GET_VECTOR(dfDict, sHandle)
	ENDIF
	RETURN vDefault
ENDFUNC

FUNC STRING SAFE_GET_STRING(DATAFILE_DICT dfDict, STRING sHandle, STRING sDefault)
	IF  DATADICT_IS_DICT_VALID(dfDict)
	AND DATADICT_GET_TYPE(dfDict, sHandle) = DF_STRING
		RETURN DATADICT_GET_STRING(dfDict, sHandle)
	ENDIF
	RETURN sDefault
ENDFUNC

FUNC TEXT_LABEL_3 SAFE_GET_TEXT_LABEL_3(DATAFILE_DICT dfDict, STRING sHandle, STRING sDefault)
	TEXT_LABEL_3 tl3
	IF  DATADICT_IS_DICT_VALID(dfDict)
	AND DATADICT_GET_TYPE(dfDict, sHandle) = DF_STRING
		tl3 = DATADICT_GET_STRING(dfDict, sHandle)
	ELSE
		tl3 = sDefault
	ENDIF
	RETURN tl3
ENDFUNC

FUNC TEXT_LABEL_7 SAFE_GET_TEXT_LABEL_7(DATAFILE_DICT dfDict, STRING sHandle, STRING sDefault)
	TEXT_LABEL_7 tl7
	IF  DATADICT_IS_DICT_VALID(dfDict)
	AND DATADICT_GET_TYPE(dfDict, sHandle) = DF_STRING
		tl7 = DATADICT_GET_STRING(dfDict, sHandle)
	ELSE
		tl7 = sDefault
	ENDIF
	RETURN tl7
ENDFUNC

FUNC TEXT_LABEL_15 SAFE_GET_TEXT_LABEL_15(DATAFILE_DICT dfDict, STRING sHandle, STRING sDefault)
	TEXT_LABEL_15 tl15
	IF  DATADICT_IS_DICT_VALID(dfDict)
	AND DATADICT_GET_TYPE(dfDict, sHandle) = DF_STRING
		tl15 = DATADICT_GET_STRING(dfDict, sHandle)
	ELSE
		tl15 = sDefault
	ENDIF
	RETURN tl15
ENDFUNC

FUNC TEXT_LABEL_23 SAFE_GET_TEXT_LABEL_23(DATAFILE_DICT dfDict, STRING sHandle, STRING sDefault)
	TEXT_LABEL_23 tl23
	IF  DATADICT_IS_DICT_VALID(dfDict)
	AND DATADICT_GET_TYPE(dfDict, sHandle) = DF_STRING
		tl23 = DATADICT_GET_STRING(dfDict, sHandle)
	ELSE
		tl23 = sDefault
	ENDIF
	RETURN tl23
ENDFUNC

FUNC TEXT_LABEL_31 SAFE_GET_TEXT_LABEL_31(DATAFILE_DICT dfDict, STRING sHandle, STRING sDefault)
	TEXT_LABEL_31 tl31
	IF  DATADICT_IS_DICT_VALID(dfDict)
	AND DATADICT_GET_TYPE(dfDict, sHandle) = DF_STRING
		tl31 = DATADICT_GET_STRING(dfDict, sHandle)
	ELSE
		tl31 = sDefault
	ENDIF
	RETURN tl31
ENDFUNC

FUNC TEXT_LABEL_63 SAFE_GET_TEXT_LABEL_63(DATAFILE_DICT dfDict, STRING sHandle, STRING sDefault)
	TEXT_LABEL_63 tl63
	IF  DATADICT_IS_DICT_VALID(dfDict)
	AND DATADICT_GET_TYPE(dfDict, sHandle) = DF_STRING
		tl63 = DATADICT_GET_STRING(dfDict, sHandle)
	ELSE
		tl63 = sDefault
	ENDIF
	RETURN tl63
ENDFUNC

// non arrayed setters
PROC SAFE_SET_BOOL(BOOL bVar, DATAFILE_DICT dfDict, STRING sHandle, BOOL bDefault)
	IF bVar <> bDefault
		DATADICT_SET_BOOL(dfDict, sHandle, bVar)
	ENDIF
ENDPROC

PROC SAFE_SET_INT(INT iVar, DATAFILE_DICT dfDict, STRING sHandle, INT iDefault)
	IF iVar <> iDefault
		DATADICT_SET_INT(dfDict, sHandle, iVar)
	ENDIF
ENDPROC

PROC SAFE_SET_INT_IN_STRUCT(INT iValue, DATAFILE_ARRAY dfArray, INT index, INT iDefault = -1)
	IF iValue != iDefault
		DATAARRAY_SET_INT(dfArray, index, iValue)
	ENDIF
ENDPROC

PROC SAFE_SET_ARRAY_BOOL(DATAFILE_ARRAY dfArray, BOOL bVar)
	DATAARRAY_ADD_BOOL(dfArray, bVar)
ENDPROC

PROC SAFE_SET_ARRAY_INT(DATAFILE_ARRAY dfArray, INT iVar)
	DATAARRAY_ADD_INT(dfArray, iVar)
ENDPROC

PROC SAFE_SET_ARRAY_ENUM(DATAFILE_ARRAY dfArray, ENUM_TO_INT iVar)
	DATAARRAY_ADD_INT(dfArray, ENUM_TO_INT(iVar))
ENDPROC

PROC SAFE_SET_ENUM(ENUM_TO_INT iVar, DATAFILE_DICT dfDict, STRING sHandle, ENUM_TO_INT iDefault)
	IF iVar <> iDefault
		DATADICT_SET_INT(dfDict, sHandle, ENUM_TO_INT(iVar))
	ENDIF
ENDPROC

PROC SAFE_SET_FLOAT(FLOAT fVar, DATAFILE_DICT dfDict, STRING sHandle, FLOAT fDefault)
	IF fVar <> fDefault
		DATADICT_SET_FLOAT(dfDict, sHandle, fVar)
	ENDIF
ENDPROC

PROC SAFE_SET_ARRAY_FLOAT(DATAFILE_ARRAY dfArray, FLOAT fVar)
	DATAARRAY_ADD_FLOAT(dfArray, fVar)
ENDPROC

PROC SAFE_SET_ARRAY_VECTOR(DATAFILE_ARRAY dfArray, VECTOR fVar)
	DATAARRAY_ADD_VECTOR(dfArray, fVar)
ENDPROC

PROC SAFE_SET_VECTOR(VECTOR vVar, DATAFILE_DICT dfDict, STRING sHandle, VECTOR vDefault)
	IF NOT ARE_VECTORS_EQUAL(vVar, vDefault)
		DATADICT_SET_VECTOR(dfDict, sHandle, vVar)
	ENDIF
ENDPROC

PROC SAFE_SET_ARRAY_STRING(DATAFILE_ARRAY dfArray, STRING fVar)
	DATAARRAY_ADD_STRING(dfArray, fVar)
ENDPROC

PROC SAFE_SET_STRING(STRING sVar, DATAFILE_DICT dfDict, STRING sHandle, STRING sDefault)
	IF NOT ARE_STRINGS_EQUAL(sVar, sDefault)
		DATADICT_SET_STRING(dfDict, sHandle, sVar)
	ENDIF
ENDPROC

PROC SAFE_SET_ARRAY_LABEL_3(DATAFILE_ARRAY dfArray, TEXT_LABEL_3 fVar)
	SAFE_SET_ARRAY_STRING(dfArray, fVar)
ENDPROC

PROC SAFE_SET_TEXT_LABEL_3(TEXT_LABEL_3 sVar, DATAFILE_DICT dfDict, STRING sHandle, STRING sDefault)
	SAFE_SET_STRING(sVar, dfDict, sHandle, sDefault)
ENDPROC
PROC SAFE_SET_ARRAY_TEXT_LABEL_7(DATAFILE_ARRAY dfArray, TEXT_LABEL_7 fVar)
	SAFE_SET_ARRAY_STRING(dfArray, fVar)
ENDPROC
PROC SAFE_SET_TEXT_LABEL_7(TEXT_LABEL_7 sVar, DATAFILE_DICT dfDict, STRING sHandle, STRING sDefault)
	SAFE_SET_STRING(sVar, dfDict, sHandle, sDefault)
ENDPROC
PROC SAFE_SET_ARRAY_TEXT_LABEL_15(DATAFILE_ARRAY dfArray, TEXT_LABEL_15 fVar)
	SAFE_SET_ARRAY_STRING(dfArray, fVar)
ENDPROC
PROC SAFE_SET_TEXT_LABEL_15(TEXT_LABEL_15 sVar, DATAFILE_DICT dfDict, STRING sHandle, STRING sDefault)
	SAFE_SET_STRING(sVar, dfDict, sHandle, sDefault)
ENDPROC
PROC SAFE_SET_ARRAY_TEXT_LABEL_23(DATAFILE_ARRAY dfArray, TEXT_LABEL_23 fVar)
	SAFE_SET_ARRAY_STRING(dfArray, fVar)
ENDPROC
PROC SAFE_SET_TEXT_LABEL_23(TEXT_LABEL_23 sVar, DATAFILE_DICT dfDict, STRING sHandle, STRING sDefault)
	SAFE_SET_STRING(sVar, dfDict, sHandle, sDefault)
ENDPROC
PROC SAFE_SET_ARRAY_TEXT_LABEL_31(DATAFILE_ARRAY dfArray, TEXT_LABEL_31 fVar)
	SAFE_SET_ARRAY_STRING(dfArray, fVar)
ENDPROC
PROC SAFE_SET_TEXT_LABEL_31(TEXT_LABEL_31 sVar, DATAFILE_DICT dfDict, STRING sHandle, STRING sDefault)
	SAFE_SET_STRING(sVar, dfDict, sHandle, sDefault)
ENDPROC
PROC SAFE_SET_ARRAY_TEXT_LABEL_63(DATAFILE_ARRAY dfArray, TEXT_LABEL_63 fVar)
	SAFE_SET_ARRAY_STRING(dfArray, fVar)
ENDPROC
PROC SAFE_SET_TEXT_LABEL_63(TEXT_LABEL_63 sVar, DATAFILE_DICT dfDict, STRING sHandle, STRING sDefault)
	SAFE_SET_STRING(sVar, dfDict, sHandle, sDefault)
ENDPROC

//######
//######
//				DATA CRAWLER USING
//######
//######

CONST_INT ciMAX_DATA_CRAWLER_LAYERS	10
STRUCT DATA_CRAWL_LOADER_STRUCT
	
	//Current "layer" we're in
	DATAFILE_DICT dfdCurrentDirectory
	INT iCurrentLayer
	
	INT i1D_Element[ciMAX_DATA_CRAWLER_LAYERS]
	INT i2D_Element[ciMAX_DATA_CRAWLER_LAYERS]
	INT i3D_Element[ciMAX_DATA_CRAWLER_LAYERS]
	DATAFILE_DICT dfdLayerDirectoryCache[ciMAX_DATA_CRAWLER_LAYERS]
	
	BOOL bLastGrabFailed
	
ENDSTRUCT
STRUCT DATACRAWLER_DATA_RETURN
	BOOL bFailed
	
	INT iData = -1
	FLOAT fData = -1.0
	STRING sData = NULL
	VECTOR vData
	BOOL bData
ENDSTRUCT

//######
//######
//				DATA CRAWLER PRIVATE FUNCTIONS, NO TREASURE HERE
//######
//######
FUNC BOOL private_DATA_CRAWLER_SETUP_THE_GETTER(DATA_CRAWL_LOADER_STRUCT& sDataCrawler, STRING sHandle, DATAFILE_ARRAY& dfaData, INT i1D_Element = -1, INT i2D_Element = -1, INT i3D_Element = -1)
	TEXT_LABEL_15 tl15 = sHandle
	INT i
	
	//g_UGC_Mission.s1[0][0][0].s2[0][0][1].v1[0][1][1] = 26
	//g_UGC_Mission.s1[1][0][0].s2[0][0][1].v1[0][1][1] = 390
	//"dsttd00001011": [26, 390]
	//handle goes first: 									"dsttd"
	//s1: 1d is saved for the '[26, 390]', 2d and 3d added 	"dsttd00"
	//s2: 1d, 2d, 3d all go in 								"dsttd00001"
	//v1: 1d, 2d, 3d all go in								"dsttd00001011"
	
	FOR i = 0 TO sDataCrawler.iCurrentLayer-1
		
		//Element 1D for the first layer is used for the actual json array
		IF i != 0
		AND sDataCrawler.i1D_Element[i] != -1
			tl15 += sDataCrawler.i1D_Element[i]
		ENDIF
		
		//after element 1D, always add 2Ds and 3Ds
		IF sDataCrawler.i2D_Element[i] != -1
			tl15 += sDataCrawler.i2D_Element[i]
		ENDIF
		
		IF sDataCrawler.i3D_Element[i] != -1
			tl15 += sDataCrawler.i3D_Element[i]
		ENDIF
	ENDFOR
	
	//The numbers for this variable are added at the end
	
	//Only add 1D if we aren't already using it for the first layer (as the first array on the first layer is the json array)
	IF i1D_Element != -1
	AND sDataCrawler.iCurrentLayer > 0
		tl15 += i1D_Element
	ENDIF
	
	IF i2D_Element != -1
		tl15 += i2D_Element
	ENDIF
	
	IF i3D_Element != -1
		tl15 += i3D_Element
	ENDIF
	
	PRINTLN("[FMC][DataCrawler] Trying to grab: ", tl15, " - [", i1D_Element, ",", i2D_Element, ",", i3D_Element, "], Layer: ", sDataCrawler.iCurrentLayer, ", Type: ", DATADICT_GET_TYPE(sDataCrawler.dfdCurrentDirectory, tl15))
	
	dfaData = DATADICT_GET_ARRAY(sDataCrawler.dfdCurrentDirectory, tl15)
	IF DATADICT_GET_TYPE(sDataCrawler.dfdCurrentDirectory, tl15) = DF_NONE
		dfaData = NULL
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC private_DATA_CRAWLER_SET_DATA_IN_DATA(DATA_CRAWL_LOADER_STRUCT& sDataCrawler, STRING sHandle, DATACRAWLER_DATA_RETURN sNewData, INT i1D_Element = -1, INT i2D_Element = -1, INT i3D_Element = -1)
	
	DATAFILE_ARRAY dfaData

	IF private_DATA_CRAWLER_SETUP_THE_GETTER(sDataCrawler, sHandle, dfaData, i1D_Element, i2D_Element, i3D_Element)
		IF sDataCrawler.iCurrentLayer > 0
		AND sDataCrawler.i1D_Element[0] != -1
			//If arrayed on the first layer, that number needs to be used on the json array, any other arrays on top of this
			//	will be put into the actual json node name e.g. "ro", "ebs30[]" (single array inside of a 2d array)
			PRINTLN("[FMC][DataCrawler] private_DATA_CRAWLER_SET_DATA_IN_DATA (1) ", sHandle, ": ", DATAARRAY_GET_TYPE(dfaData, sDataCrawler.i1D_Element[0]))
			SWITCH DATAARRAY_GET_TYPE(dfaData, sDataCrawler.i1D_Element[0])
				CASE DF_INT 	DATAARRAY_SET_INT(dfaData, sDataCrawler.i1D_Element[0], sNewData.iData) 		BREAK
				CASE DF_FLOAT 	DATAARRAY_SET_FLOAT(dfaData, sDataCrawler.i1D_Element[0], sNewData.fData) 		BREAK
				CASE DF_STRING 	DATAARRAY_SET_STRING(dfaData, sDataCrawler.i1D_Element[0], sNewData.sData) 	BREAK
				CASE DF_BOOL 	DATAARRAY_SET_BOOL(dfaData, sDataCrawler.i1D_Element[0], sNewData.bData) 		BREAK
				CASE DF_VEC3 	DATAARRAY_SET_VECTOR(dfaData, sDataCrawler.i1D_Element[0], sNewData.vData) 	BREAK
			ENDSWITCH
		ELSE
			PRINTLN("[FMC][DataCrawler] private_DATA_CRAWLER_SET_DATA_IN_DATA - This part of the function has not been set up yet. Please see commented out script at this print.")
			ASSERTLN("[FMC][DataCrawler] private_DATA_CRAWLER_SET_DATA_IN_DATA - This part of the function has not been set up yet. Please see commented out script at this assert.")
//			//If it's a straight up variable (in the root directory)
//			PRINTLN("[FMC][DataCrawler] private_DATA_CRAWLER_SET_DATA_IN_DATA (2) ", sHandle, ": ", DATADICT_GET_TYPE(sDataCrawler.dfdCurrentDirectory, sHandle))
//			SWITCH DATADICT_GET_TYPE(sDataCrawler.dfdCurrentDirectory, sHandle)
//				CASE DF_INT 	DATADICT_SET_INT(sDataCrawler.dfdCurrentDirectory, sHandle, sNewData.iData)	BREAK
//				CASE DF_FLOAT 	DATADICT_SET_FLOAT(sDataCrawler.dfdCurrentDirectory, sHandle, sNewData.fData)	BREAK
//				CASE DF_STRING	DATADICT_SET_STRING(sDataCrawler.dfdCurrentDirectory, sHandle, sNewData.sData) BREAK
//				CASE DF_BOOL 	DATADICT_SET_BOOL(sDataCrawler.dfdCurrentDirectory, sHandle, sNewData.bData) 	BREAK
//				CASE DF_VEC3 	DATADICT_SET_VECTOR(sDataCrawler.dfdCurrentDirectory, sHandle, sNewData.vData) BREAK
//				
//				CASE DF_NONE    //If it's more than a 1d array in the root, the above will report it to be nothing
//				CASE DF_ARRAY	//If it's an arrayed variable (in the root directory)
//					IF i1D_Element > -1
//						PRINTLN("[FMC][DataCrawler] private_DATA_CRAWLER_SET_DATA_IN_DATA (3) ", sHandle, ": ", DATAARRAY_GET_TYPE(dfaData, i1D_Element))
//						SWITCH DATAARRAY_GET_TYPE(dfaData, i1D_Element)
//							CASE DF_INT 	DATAARRAY_SET_INT(dfaData, i1D_Element, sNewData.iData) 			BREAK
//							CASE DF_FLOAT 	DATAARRAY_SET_FLOAT(dfaData, i1D_Element, sNewData.fData) 			BREAK
//							CASE DF_STRING 	DATAARRAY_SET_STRING(dfaData, i1D_Element, sNewData.sData) 			BREAK
//							CASE DF_BOOL 	DATAARRAY_SET_BOOL(dfaData, i1D_Element, sNewData.bData) 			BREAK
//							CASE DF_VEC3 	DATAARRAY_SET_VECTOR(dfaData, i1D_Element, sNewData.vData) 			BREAK
//						ENDSWITCH
//					#IF IS_DEBUG_BUILD
//					ELSE
//						PRINTLN("private_DATA_CRAWLER_SET_DATA_IN_DATA Var ", sHandle, " on layer ", sDataCrawler.iCurrentLayer, " is of type DF_ARRAY but i1D_Element was passed as ", i1D_Element, 
//								"!! If grabbing from an array, a element needs to be provided! Bad!")
//						ASSERTLN("private_DATA_CRAWLER_SET_DATA_IN_DATA - No array element")
//					#ENDIF
//					ENDIF
//				BREAK
//			ENDSWITCH
		ENDIF
	ELSE
		PRINTLN("[FMC][DataCrawler] private_DATA_CRAWLER_SET_DATA_IN_DATA Failed to set: ", sHandle, " - [", i1D_Element, ",", i2D_Element, ",", i3D_Element, "].")
	ENDIF
ENDPROC

FUNC DATACRAWLER_DATA_RETURN private_DATA_CRAWLER_GET_DATA_FROM_DATA(DATA_CRAWL_LOADER_STRUCT& sDataCrawler, STRING sHandle, INT i1D_Element = -1, INT i2D_Element = -1, INT i3D_Element = -1)
	
	DATACRAWLER_DATA_RETURN eReturn
	DATAFILE_ARRAY dfaData
	
	IF private_DATA_CRAWLER_SETUP_THE_GETTER(sDataCrawler, sHandle, dfaData, i1D_Element, i2D_Element, i3D_Element)
		IF sDataCrawler.iCurrentLayer > 0
		AND sDataCrawler.i1D_Element[0] != -1
			//If arrayed on the first layer, that number needs to be used on the json array, any other arrays on top of this
			//	will be put into the actual json node name e.g. "ro", "ebs30[]" (single array inside of a 2d array)
			PRINTLN("[FMC][DataCrawler] (1) ", sHandle, ": ", DATAARRAY_GET_TYPE(dfaData, sDataCrawler.i1D_Element[0]))
			SWITCH DATAARRAY_GET_TYPE(dfaData, sDataCrawler.i1D_Element[0])
				CASE DF_INT 	eReturn.iData = DATAARRAY_GET_INT(dfaData, sDataCrawler.i1D_Element[0]) 		BREAK
				CASE DF_FLOAT 	eReturn.fData = DATAARRAY_GET_FLOAT(dfaData, sDataCrawler.i1D_Element[0]) 		BREAK
				CASE DF_STRING 	eReturn.sData = DATAARRAY_GET_STRING(dfaData, sDataCrawler.i1D_Element[0]) 	BREAK
				CASE DF_BOOL 	eReturn.bData = DATAARRAY_GET_BOOL(dfaData, sDataCrawler.i1D_Element[0]) 		BREAK
				CASE DF_VEC3 	eReturn.vData = DATAARRAY_GET_VECTOR(dfaData, sDataCrawler.i1D_Element[0]) 	BREAK
			ENDSWITCH
		ELSE
			//If it's a straight up variable (in the root directory)
			PRINTLN("[FMC][DataCrawler] (2) ", sHandle, ": ", DATADICT_GET_TYPE(sDataCrawler.dfdCurrentDirectory, sHandle))
			SWITCH DATADICT_GET_TYPE(sDataCrawler.dfdCurrentDirectory, sHandle)
				CASE DF_INT 	eReturn.iData = DATADICT_GET_INT(sDataCrawler.dfdCurrentDirectory, sHandle)	BREAK
				CASE DF_FLOAT 	eReturn.fData = DATADICT_GET_FLOAT(sDataCrawler.dfdCurrentDirectory, sHandle)	BREAK
				CASE DF_STRING	eReturn.sData = DATADICT_GET_STRING(sDataCrawler.dfdCurrentDirectory, sHandle) BREAK
				CASE DF_BOOL 	eReturn.bData = DATADICT_GET_BOOL(sDataCrawler.dfdCurrentDirectory, sHandle) 	BREAK
				CASE DF_VEC3 	eReturn.vData = DATADICT_GET_VECTOR(sDataCrawler.dfdCurrentDirectory, sHandle) BREAK
				
				CASE DF_NONE    //If it's more than a 1d array in the root, the above will report it to be nothing
				CASE DF_ARRAY	//If it's an arrayed variable (in the root directory)
					IF i1D_Element > -1
						PRINTLN("[FMC][DataCrawler] (3) ", sHandle, ": ", (DATAARRAY_GET_TYPE(dfaData, i1D_Element)))
						SWITCH DATAARRAY_GET_TYPE(dfaData, i1D_Element)
							CASE DF_INT 	eReturn.iData = DATAARRAY_GET_INT(dfaData, i1D_Element) 			BREAK
							CASE DF_FLOAT 	eReturn.fData = DATAARRAY_GET_FLOAT(dfaData, i1D_Element) 			BREAK
							CASE DF_STRING 	eReturn.sData = DATAARRAY_GET_STRING(dfaData, i1D_Element) 			BREAK
							CASE DF_BOOL 	eReturn.bData = DATAARRAY_GET_BOOL(dfaData, i1D_Element) 			BREAK
							CASE DF_VEC3 	eReturn.vData = DATAARRAY_GET_VECTOR(dfaData, i1D_Element) 			BREAK
						ENDSWITCH
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("private_DATA_CRAWLER_GET_DATA_FROM_DATA Var ", sHandle, " on layer ", sDataCrawler.iCurrentLayer, " is of type DF_ARRAY but i1D_Element was passed as ", i1D_Element, 
								"!! If grabbing from an array, a element needs to be provided! Bad!")
						ASSERTLN("private_DATA_CRAWLER_GET_DATA_FROM_DATA - No array element provided.")
					#ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ELSE
		PRINTLN("[FMC][DataCrawler] Failed to grab: ", sHandle, " - [", i1D_Element, ",", i2D_Element, ",", i3D_Element, "] Returning default value.")
		eReturn.bFailed = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT eReturn.bFailed
		PRINTLN("[FMC][DataCrawler] Grab success! (", eReturn.iData, ", ", eReturn.fData, ", ", eReturn.vData, ", ", eReturn.sData, ", ", eReturn.bData, ")")
	ENDIF
	#ENDIF
	
	RETURN eReturn
ENDFUNC

PROC CLEANUP_LOCAL_DATA_FILE()
	IF DATAFILE_GET_FILE_DICT() != NULL
		#IF IS_DEBUG_BUILD
		PRINTLN("CLEANUP_LOCAL_DATA_FILE")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		DATAFILE_DELETE()
	ENDIF
ENDPROC

PROC CLEANUP_ADDITIONAL_LOCAL_DATA_FILE()
	IF DATAFILE_GET_FILE_DICT_FOR_ADDITIONAL_DATA_FILE(0) != NULL
		#IF IS_DEBUG_BUILD
		PRINTLN("CLEANUP_ADDITIONAL_LOCAL_DATA_FILE")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		DATAFILE_DELETE_FOR_ADDITIONAL_DATA_FILE(0)
	ENDIF
ENDPROC

//######
//######
//				DATA CRAWLER PUBLIC FUNCTIONS - USE THESE!
//######
//######

PROC DATA_CRAWLER__RESET(DATA_CRAWL_LOADER_STRUCT& sDataCrawler, DATAFILE_DICT &dfd)
	sDataCrawler.dfdCurrentDirectory = dfd
	sDataCrawler.iCurrentLayer = 0
	
	INT i
	REPEAT ciMAX_DATA_CRAWLER_LAYERS i
		sDataCrawler.i1D_Element[i] = -1
		sDataCrawler.i2D_Element[i] = -1
		sDataCrawler.i3D_Element[i] = -1
		sDataCrawler.dfdLayerDirectoryCache[i] = NULL
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    After having successfully entered a struct via GO_INTO_STRUCT will exit back out to the previous layer.
/// REMARKS:
///    No need to call this if GO_INTO_STRUCT returns false. Will incorrectly be jumping back out into the previous layer if
///    	multiple layers deep already.
PROC DATA_CRAWLER__GO_BACK(DATA_CRAWL_LOADER_STRUCT& sDataCrawler)
	IF sDataCrawler.iCurrentLayer > 0
		
		PRINTLN("DATA_CRAWLER__GO_BACK Current layer = ", sDataCrawler.iCurrentLayer, ", going back up!")
		
		sDataCrawler.i1D_Element[sDataCrawler.iCurrentLayer] = -1
		sDataCrawler.i2D_Element[sDataCrawler.iCurrentLayer] = -1
		sDataCrawler.i3D_Element[sDataCrawler.iCurrentLayer] = -1
		
		sDataCrawler.iCurrentLayer--
		
		sDataCrawler.dfdCurrentDirectory = sDataCrawler.dfdLayerDirectoryCache[sDataCrawler.iCurrentLayer]
		sDataCrawler.dfdLayerDirectoryCache[sDataCrawler.iCurrentLayer] = NULL
		
	ELSE
		PRINTLN("DATA_CRAWLER__GO_BACK Current layer = ", sDataCrawler.iCurrentLayer, " couldn't go backwards!")
	ENDIF
ENDPROC

FUNC BOOL DATA_CRAWLER__GO_INTO_STRUCT(DATA_CRAWL_LOADER_STRUCT& sDataCrawler, STRING sHandle, INT i1D_Element = -1, INT i2D_Element = -1, INT i3D_Element = -1)
	
	DATAFILE_DICT dfDict
	SAFE_DATADICT_GET_DICT(dfDict, sDataCrawler.dfdCurrentDirectory, sHandle)
	
	IF dfDict = NULL
		PRINTLN("(1) ", sHandle, " not valid")
		RETURN FALSE
	ENDIF
	
	sDataCrawler.dfdLayerDirectoryCache[sDataCrawler.iCurrentLayer] = sDataCrawler.dfdCurrentDirectory
	sDataCrawler.dfdCurrentDirectory = dfDict
	
	#IF IS_DEBUG_BUILD
	IF i1D_Element = -1
		PRINTLN("DATA_CRAWLER__GO_INTO_STRUCT Entered struct: ", sHandle)
	ELSE
		IF i2D_Element = -1
			PRINTLN("DATA_CRAWLER__GO_INTO_STRUCT Entered struct: ", sHandle, "[", i1D_Element, "]")
		ELIF i3D_Element = -1
			PRINTLN("DATA_CRAWLER__GO_INTO_STRUCT Entered struct: ", sHandle, "[", i1D_Element, "][", i2D_Element, "]")
		ELSE
			PRINTLN("DATA_CRAWLER__GO_INTO_STRUCT Entered struct: ", sHandle, "[", i1D_Element, "][", i2D_Element, "][", i3D_Element, "]")
		ENDIF
	ENDIF
	#ENDIF
	
	sDataCrawler.i1D_Element[sDataCrawler.iCurrentLayer] = i1D_Element
	sDataCrawler.i2D_Element[sDataCrawler.iCurrentLayer] = i2D_Element
	sDataCrawler.i3D_Element[sDataCrawler.iCurrentLayer] = i3D_Element
	sDataCrawler.iCurrentLayer++
	
	RETURN TRUE
ENDFUNC

FUNC INT DATA_CRAWLER__GET_INT(DATA_CRAWL_LOADER_STRUCT& sDataCrawler, STRING sHandle, INT iDefault = 0, INT i1D_Element = -1, INT i2D_Element = -1, INT i3D_Element = -1)
	DATACRAWLER_DATA_RETURN data = private_DATA_CRAWLER_GET_DATA_FROM_DATA(sDataCrawler, sHandle, i1D_Element, i2D_Element, i3D_Element)
	sDataCrawler.bLastGrabFailed = data.bFailed
	RETURN PICK_INT(data.bFailed, iDefault, data.iData)
ENDFUNC
FUNC FLOAT DATA_CRAWLER__GET_FLOAT(DATA_CRAWL_LOADER_STRUCT& sDataCrawler, STRING sHandle, FLOAT fDefault = 0.0, INT i1D_Element = -1, INT i2D_Element = -1, INT i3D_Element = -1)
	DATACRAWLER_DATA_RETURN data = private_DATA_CRAWLER_GET_DATA_FROM_DATA(sDataCrawler, sHandle, i1D_Element, i2D_Element, i3D_Element)
	sDataCrawler.bLastGrabFailed = data.bFailed
	RETURN PICK_FLOAT(data.bFailed, fDefault, data.fData)
ENDFUNC
FUNC STRING DATA_CRAWLER__GET_STRING(DATA_CRAWL_LOADER_STRUCT& sDataCrawler, STRING sHandle, STRING tlDefault = NULL, INT i1D_Element = -1, INT i2D_Element = -1, INT i3D_Element = -1)
	DATACRAWLER_DATA_RETURN data = private_DATA_CRAWLER_GET_DATA_FROM_DATA(sDataCrawler, sHandle, i1D_Element, i2D_Element, i3D_Element)
	sDataCrawler.bLastGrabFailed = data.bFailed
	RETURN PICK_STRING(data.bFailed, tlDefault, data.sData)
ENDFUNC
FUNC VECTOR DATA_CRAWLER__GET_VECTOR(DATA_CRAWL_LOADER_STRUCT& sDataCrawler, STRING sHandle, VECTOR vDefault, INT i1D_Element = -1, INT i2D_Element = -1, INT i3D_Element = -1)
	DATACRAWLER_DATA_RETURN data = private_DATA_CRAWLER_GET_DATA_FROM_DATA(sDataCrawler, sHandle, i1D_Element, i2D_Element, i3D_Element)
	sDataCrawler.bLastGrabFailed = data.bFailed
	RETURN PICK_VECTOR(data.bFailed, vDefault, data.vData)
ENDFUNC
FUNC STRING DATA_CRAWLER__GET_BOOL(DATA_CRAWL_LOADER_STRUCT& sDataCrawler, STRING sHandle, BOOL bDefault = FALSE, INT i1D_Element = -1, INT i2D_Element = -1, INT i3D_Element = -1)
	DATACRAWLER_DATA_RETURN data = private_DATA_CRAWLER_GET_DATA_FROM_DATA(sDataCrawler, sHandle, i1D_Element, i2D_Element, i3D_Element)
	sDataCrawler.bLastGrabFailed = data.bFailed
	IF data.bFailed
		RETURN bDefault
	ENDIF
	RETURN data.bData
ENDFUNC

/// PURPOSE:
///    Use to test if the last GET attempt found valid data in the data file. Useful for seeing if data actually exists.
/// NOTE: 
///    Data data will not be saved in json if not changed from its default.
/// RETURNS:
///    Returns true if the last GET attempt failed to find valid data.
FUNC BOOL DATA_CRAWLER__LAST_GRAB_FAILED(DATA_CRAWL_LOADER_STRUCT& sDataCrawler)
	RETURN sDataCrawler.bLastGrabFailed
ENDFUNC
