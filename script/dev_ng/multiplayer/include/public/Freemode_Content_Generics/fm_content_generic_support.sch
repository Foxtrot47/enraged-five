
USING "fm_content_generic_structs.sch"

//----------------------
//	GENERIC OVERRIDES
//----------------------

// Override to define your data files
#IF NOT DEFINED(GET_MISSION_PLACEMENT_DATA_FILENAME)
FUNC STRING GET_MISSION_PLACEMENT_DATA_FILENAME()
	IF DOES_FREEMODE_CONTENT_LOAD_FROM_DATA_FILE(sMission.iType)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [DATA] - GET_MISSION_PLACEMENT_DATA_FILENAME is not defined, please define and populate with your data files!")
		ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [DATA] - GET_MISSION_PLACEMENT_DATA_FILENAME is not defined, please define and populate with your data files!")
	ENDIF
	RETURN ""
ENDFUNC
#ENDIF

#IF NOT DEFINED(SET_PLAYER_PERMANENT)
PROC SET_PLAYER_PERMANENT()
ENDPROC
#ENDIF

#IF NOT DEFINED(IS_PLAYER_PERMANENT)
FUNC BOOL IS_PLAYER_PERMANENT(PLAYER_INDEX playerId)
	UNUSED_PARAMETER(playerId)
	RETURN TRUE
ENDFUNC
#ENDIF

#IF NOT DEFINED(SET_PLAYER_CRITICAL)
PROC SET_PLAYER_CRITICAL(BOOL bCritical)
	UNUSED_PARAMETER(bCritical)
ENDPROC
#ENDIF

#IF NOT DEFINED(IS_PLAYER_CRITICAL)
FUNC BOOL IS_PLAYER_CRITICAL(PLAYER_INDEX playerId)
	UNUSED_PARAMETER(playerId)
	RETURN FALSE
ENDFUNC
#ENDIF

#IF NOT DEFINED(IS_LOCAL_PLAYER_PARTICIPATING)
FUNC BOOL IS_LOCAL_PLAYER_PARTICIPATING()
	RETURN TRUE
ENDFUNC
#ENDIF

#IF NOT DEFINED(IS_SAFE_TO_DISPLAY_MISSION_UI)
FUNC BOOL IS_SAFE_TO_DISPLAY_MISSION_UI(BOOL bCheckCutscene = TRUE, BOOL bOnlyCheckPermanentForUI = FALSE, BOOL bCheckYachtWarp = TRUE)
	UNUSED_PARAMETER(bCheckCutscene)
	UNUSED_PARAMETER(bOnlyCheckPermanentForUI)
	UNUSED_PARAMETER(bCheckYachtWarp)
	RETURN TRUE
ENDFUNC
#ENDIF

#IF NOT DEFINED(IS_MISSION_RESTRICTED_FOR_HIDE_OR_PASSIVE)
FUNC BOOL IS_MISSION_RESTRICTED_FOR_HIDE_OR_PASSIVE()
	RETURN FALSE
ENDFUNC
#ENDIF

#IF NOT DEFINED(GET_VARIATION_NAME_FOR_DEBUG)
FUNC STRING GET_VARIATION_NAME_FOR_DEBUG()
	RETURN "NO VARIATION NAMES DEFINED"
ENDFUNC
#ENDIF

#IF NOT DEFINED(GET_SUBVARIATION_NAME_FOR_DEBUG)
FUNC STRING GET_SUBVARIATION_NAME_FOR_DEBUG()
	RETURN "NO SUBVARIATION NAMES DEFINED"
ENDFUNC
#ENDIF

#IF DEFINED(eMISSION_TEAMS)
#IF NOT DEFINED(SETUP_PLAYER_MISSION_TEAM)
FUNC eMISSION_TEAMS SETUP_PLAYER_MISSION_TEAM()
	RETURN 0
ENDFUNC
#ENDIF

#IF NOT DEFINED(GET_PLAYER_MISSION_TEAM_NAME)
FUNC STRING GET_PLAYER_MISSION_TEAM_NAME(eMISSION_TEAMS eTeam)
	UNUSED_PARAMETER(eTeam)
	RETURN "TEAM NAMES NOT DEFINED"
ENDFUNC
#ENDIF
#ENDIF

//----------------------
//	GENERAL FUNCS
//----------------------

#IF IS_DEBUG_BUILD
PROC DRAW_DEBUG_CORONA(VECTOR vCoronaCoord, FLOAT fRadius = 0.5, HUD_COLOURS hcColour = HUD_COLOUR_YELLOW, INT iAlpha = 150)
    INT iR, iG, iB, iA
    GET_HUD_COLOUR(hcColour, iR, iG, iB, iA)
    
    DRAW_MARKER(MARKER_CYLINDER,
                <<vCoronaCoord.x, vCoronaCoord.y, vCoronaCoord.z - 0.7>>,
                <<0.0, 0.0, 0.0>>,
                <<0.0, 0.0, 0.0>>,
                <<fRadius * 2.0, fRadius * 2.0, 1.5>>,
                iR,
                iG,
                iB,
                iAlpha)
ENDPROC
#ENDIF

FUNC BOOL IS_SAFE_FOR_HELP_TEXT()
	IF NOT IS_CUSTOM_MENU_ON_SCREEN()
	AND NOT IS_BROWSER_OPEN()
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_RADAR_HIDDEN()
	AND IS_LOCAL_ALIVE
	AND NOT IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
	AND IS_GENERIC_BIT_SET(eGENERICBITSET_SETUP_JOB_START_BIG_MESSAGE)
	AND NOT IS_SCREEN_FADED_OUT()
	AND NOT IS_SCREEN_FADING_OUT()
	AND NOT IS_SCREEN_FADING_IN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_MODEL_A_CARGOBOB(MODEL_NAMES eModel)
	IF eModel = CARGOBOB
		RETURN TRUE
	ENDIF
	IF eModel = CARGOBOB2
		RETURN TRUE
	ENDIF
	IF eModel = CARGOBOB3
		RETURN TRUE
	ENDIF
	IF eModel = CARGOBOB4
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_ALL_MISSION_ENTITIES_LEFT_CARRIER_VEHICLE()
	INT iMissionEntity
	REPEAT MAX_NUM_MISSION_ENTITIES iMissionEntity
		IF NOT HAS_MISSION_ENTITY_LEFT_CARRIER_VEHICLE(iMissionEntity)
			RETURN FALSE
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

PROC DESTROY_ALL_MISSION_ENTITIES_IN_CARRIER(INT iEntity, ENTITY_TYPE entityType)

	IF entityType != GET_MISSION_ENTITY_CARRIER_TYPE()
		EXIT
	ENDIF

	INT iMissionEntity
	REPEAT data.MissionEntity.iCount iMissionEntity
		IF DOES_MISSION_ENTITY_HAVE_CARRIER(iMissionEntity)
		AND iEntity = GET_MISSION_ENTITY_CARRIER(iMissionEntity)
		AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
		AND NOT IS_MISSION_ENTITY_CLIENT_BIT_SET(iMissionEntity, LOCAL_PARTICIPANT_INDEX, eMISSIONENTITYCLIENTBITSET_DELIVERED)
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] DESTROY_ALL_MISSION_ENTITIES_IN_CARRIER - Destroyed mission entity ", iMissionEntity, " inside carrier ", iEntity)
			SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_DESTROYED)
			SET_MISSION_ENTITY_CLIENT_BIT(iMissionEntity, eMISSIONENTITYCLIENTBITSET_RECEIVED_DESTROYED_DAMAGE_EVENT)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Marks all remaining mission entities as delivered
PROC MARK_ALL_MISSION_ENTITIES_AS_DELIVERED()
	INT iMissionEntity
	REPEAT data.MissionEntity.iCount iMissionEntity
		IF NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iMissionEntity)
			SET_MISSION_ENTITY_BIT(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Warps to the first mission entity that the player is not already near
FUNC VECTOR GET_MISSION_ENTITY_WARP_COORD(Bool bAllowHeld)
	VECTOR vCoords 
	INT iMissionEntity
	REPEAT data.MissionEntity.iCount iMissionEntity
		IF NOT HAS_MISSION_ENTITY_BEEN_DELIVERED_OR_DESTROYED(iMissionEntity)
		AND (NOT IS_MISSION_ENTITY_HELD(iMissionEntity)
		OR bAllowHeld)
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntities.netId[iMissionEntity])
				vCoords = GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sMissionEntities.netId[iMissionEntity]), FALSE)
				FLOAT fDist = VDIST2(LOCAL_PLAYER_COORD, vCoords)
				IF fDist > 2*2
					RETURN vCoords
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN vCoords
ENDFUNC

FUNC BOOL WARP_INTO_MISSION_ENTITY_CARRIER_VEHICLE()
	VEHICLE_SEAT eSeat
	INT iMissionEntity
	REPEAT data.MissionEntity.iCount iMissionEntity
		IF IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_I_AM_IN_CARRIER)
		AND NOT IS_MISSION_ENTITY_BIT_SET(iMissionEntity, eMISSIONENTITYBITSET_DELIVERED)
		AND DOES_MISSION_ENTITY_HAVE_CARRIER_VEHICLE(iMissionEntity)
		AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(GET_MISSION_ENTITY_CARRIER_NET_ID(iMissionEntity))
		AND NOT IS_ENTITY_DEAD(GET_MISSION_ENTITY_CARRIER_VEHICLE_VEH_ID(iMissionEntity))
			eSeat = GET_FIRST_FREE_VEHICLE_SEAT(GET_MISSION_ENTITY_CARRIER_VEHICLE_VEH_ID(iMissionEntity))
			IF eSeat != VS_ANY_PASSENGER
				TASK_ENTER_VEHICLE(LOCAL_PED_INDEX, GET_MISSION_ENTITY_CARRIER_VEHICLE_VEH_ID(iMissionEntity), DEFAULT, eSeat, DEFAULT, ECF_WARP_PED)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_LOCAL_PLAYER_PASS_MISSION_ON_REACHING_END_STATE()
	#IF IS_DEBUG_BUILD
	IF IS_DEBUG_LOCAL_BIT_SET(eDEBUGLOCALBITSET_S_PASS)
		RETURN TRUE
	ELIF IS_DEBUG_LOCAL_BIT_SET(eDEBUGLOCALBITSET_F_FAIL)
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF logic.Mission.Pass != NULL
		RETURN CALL logic.Mission.Pass()
	ENDIF

	IF data.MissionEntity.iCount > 0
		IF DOES_MISSION_ALLOW_PARTIAL_COMPLETION()
			RETURN HAS_PLAYER_GANG_DELIVERED_ANY_MISSION_ENTITIES(LOCAL_PLAYER_INDEX)
		ENDIF
		
		RETURN HAS_PLAYER_GANG_DELIVERED_ALL_MISSION_ENTITIES(LOCAL_PLAYER_INDEX)
	ENDIF
	
	RETURN GET_END_REASON() = eENDREASON_MISSION_PASSED
	
ENDFUNC

FUNC BOOL SHOULD_USE_SAME_CARRIER_FOR_ALL_MISSION_ENTITIES()
	RETURN FALSE
ENDFUNC

FUNC BOOL RD_IS_ENTITY_POTENTIAL_MISSION_ENTITY(INT i)
	UNUSED_PARAMETER(i)
	RETURN FALSE
ENDFUNC

FUNC BOOL POPULATE_ARRAY_WITH_RANDOM_ENTITIES(INT& iMissionEntities[], INT& iSelectedBitset)
	INT iPotentialEntities[MAX_NUM_MISSION_ENTITIES]
	
	//Find the potential entities for this variation.
	INT iNumPotentialEntities
	INT i
	REPEAT MAX_NUM_MISSION_ENTITIES i
		IF RD_IS_ENTITY_POTENTIAL_MISSION_ENTITY(i)
			iPotentialEntities[iNumPotentialEntities] = i
			iNumPotentialEntities++
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] POPULATE_ARRAY_WITH_RANDOM_ENTITIES - iNumPotentialEntities = ", iNumPotentialEntities, ", iPotentialEntities[]:")
	DEBUG_PRINT_INT_ARRAY(iPotentialEntities, iNumPotentialEntities)
	#ENDIF
	
	IF iNumPotentialEntities < GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION()
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] POPULATE_ARRAY_WITH_RANDOM_ENTITIES - Not enough potential entities for selection as mission entities (iNumPotentialEntities = ", iNumPotentialEntities, ", GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() = ", GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION())
		RETURN FALSE
	ENDIF
	
	//Randomly select from the potential entities.
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() i
	
		IF SHOULD_USE_SAME_CARRIER_FOR_ALL_MISSION_ENTITIES()
			IF i > 0
				iMissionEntities[i] = iMissionEntities[i-1]
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] POPULATE_ARRAY_WITH_RANDOM_ENTITIES - Selected entity #", iMissionEntities[i], " for mission entity #", i, " due to previous entity")
				RELOOP
			ENDIF
		ENDIF
	
		INT iRandomPotentialEntity = GET_RANDOM_INT_IN_RANGE(0, iNumPotentialEntities)
		iMissionEntities[i] = iPotentialEntities[iRandomPotentialEntity]
		
		#IF IS_DEBUG_BUILD
		IF i = 0
		AND serverBD.iRandomModeInt0 != -1
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] POPULATE_ARRAY_WITH_RANDOM_ENTITIES - serverBD.iRandomModeInt0 != -1, forcing iMissionEntities[", i, "] = ", serverBD.iRandomModeInt0)
			iMissionEntities[i] = serverBD.iRandomModeInt0
		ENDIF
		#ENDIF
		
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] POPULATE_ARRAY_WITH_RANDOM_ENTITIES - Selected entity #", iMissionEntities[i], " for mission entity #", i)
		
		IF iPotentialEntities[iRandomPotentialEntity] < 32
			SET_BIT(iSelectedBitset, iPotentialEntities[iRandomPotentialEntity])
		ENDIF
		
		IF i < GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() - 1
			//Another entity needs to be selected. Move the currently selected entity to the end of the array, then decrease the range by one.
			SWAP_INT(iPotentialEntities[iRandomPotentialEntity], iPotentialEntities[iNumPotentialEntities - 1])
			iNumPotentialEntities--
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] POPULATE_ARRAY_WITH_RANDOM_ENTITIES - iMissionEntities[]:")
	DEBUG_PRINT_INT_ARRAY(iMissionEntities, GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION())
	#ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_CHOOSE_RANDOM_DROP_ENTITY()
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SERVER_CHOOSE_RANDOM_DROP_ENTITY()
	IF NOT SHOULD_CHOOSE_RANDOM_DROP_ENTITY()
		EXIT
	ENDIF
	
	IF NOT IS_GENERIC_SERVER_BIT_SET(eGENERICSERVERBITSET_CHOSEN_DROP_ENTITIES)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_SERVER_CHOOSE_RANDOM_DROP_ENTITY - GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION = ", GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION())
		
		IF POPULATE_ARRAY_WITH_RANDOM_ENTITIES(serverBD.sRDVars.iChosenDropEntity, serverBD.sRDVars.iChosenBitSet)
			SET_GENERIC_SERVER_BIT(eGENERICSERVERBITSET_CHOSEN_DROP_ENTITIES)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_NEAR_ENTITY(ENTITY_INDEX entityId, FLOAT fRange, BOOL bCheckLoS = FALSE, BOOL bCheckOnScreen = FALSE)
	IF VDIST2(GET_ENTITY_COORDS(entityId, FALSE), LOCAL_PLAYER_COORD) > fRange*fRange
		RETURN FALSE
	ENDIF
	
	IF bCheckLoS
	AND NOT HAS_ENTITY_CLEAR_LOS_TO_ENTITY(LOCAL_PED_INDEX, entityId, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_PED|SCRIPT_INCLUDE_RAGDOLL|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_FOLIAGE)
		RETURN FALSE
	ENDIF
	
	IF bCheckOnScreen
	AND NOT IS_ENTITY_ON_SCREEN(entityId)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ATTACH_MISSION_ENTITY_TO_ENTITY(ENTITY_INDEX attachEntity, ENTITY_INDEX targetEntity, VECTOR vOffset, VECTOR vRotation, BOOL bDetachWhenDead = FALSE, BOOL bKeepCollision = FALSE)
	IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(attachEntity, targetEntity)
		
		SWITCH GET_MISSION_ENTITY_CARRIER_TYPE()
	
			CASE ET_VEHICLE
				ATTACH_ENTITY_TO_ENTITY(attachEntity, targetEntity, GET_ENTITY_BONE_INDEX_BY_NAME(targetEntity, "chassis_dummy"), vOffset, vRotation, bDetachWhenDead, FALSE, bKeepCollision)		
			BREAK
			
			CASE ET_PED
				ATTACH_PORTABLE_PICKUP_TO_PED(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(attachEntity), GET_PED_INDEX_FROM_ENTITY_INDEX(targetEntity))
			BREAK
		ENDSWITCH
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_MISSION_ENTITY_ATTACH_OFFSET(INT iMissionEntity, MODEL_NAMES carrierModel)

	SWITCH GET_MISSION_ENTITY_CARRIER_TYPE()
	
		CASE ET_VEHICLE
			SWITCH carrierModel
				CASE INSURGENT3
				CASE limo2
					SWITCH(iMissionEntity)
						CASE 0 			RETURN <<1, -2.0, -0.1>>
						CASE 1 			RETURN <<-1, -2.0, -0.1>>
						CASE 2 			RETURN <<1, -2.0, -0.1>>
						CASE 3 			RETURN <<-1, -2.0, -0.1>>
					ENDSWITCH
				BREAK
				CASE ISSI3
				CASE PANTO
				CASE RAPTOR
					SWITCH(iMissionEntity)
						CASE 0 			RETURN <<0.5, 0.0, -0.1>>
						CASE 1 			RETURN <<-0.5, 0.0, -0.1>>
						CASE 2 			RETURN <<0.5, 0.0, -0.1>>
						CASE 3 			RETURN <<-0.5, 0.0, -0.1>>
					ENDSWITCH
				BREAK
				CASE FAGGIO
				CASE DEATHBIKE
				CASE DEATHBIKE2
				CASE DEATHBIKE3
				CASE MANCHEZ
				CASE MANCHEZ2
				CASE MANCHEZ3
					RETURN <<0.0, -0.65, 0.5>>
				BREAK
			ENDSWITCH
			
			SWITCH(iMissionEntity)
				CASE 0 			RETURN <<1, 0.0, -0.1>>
				CASE 1 			RETURN <<-1, 0.0, -0.1>>
				CASE 2 			RETURN <<1, 0.0, -0.1>>
				CASE 3 			RETURN <<-1, 0.0, -0.1>>
			ENDSWITCH
			
		RETURN <<0.0, -1, -0.1>>
	ENDSWITCH
		
	RETURN <<0.0, 0.0, 0.0>>
	
ENDFUNC

FUNC BOOL DOES_MODE_STATE_USE_TARGETS(eMODE_STATE eModeState)
	SWITCH eModeState 
		CASE eMODESTATE_TAKE_OUT_TARGETS 
		CASE eMODESTATE_PROTECT_TARGETS
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//----------------------
//	HUD FUNCTIONALITY
//----------------------

FUNC BOOL IS_ENTITY_SPAWN_AVAILABLE(eENTITY_TYPE eType, INT iIndex)

	// Nothing is currently spawning, spawn is available
	IF serverBD.eEntitySpawnType = eENTITYTYPE_INVALID
		RETURN TRUE
	ENDIF
	
	// Spawning this type
	IF serverBD.eEntitySpawnType = eType
		// Spawning this index
		IF serverBD.iEntitySpawnIndex = iIndex
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE: Locks the spawn system for the passed in entity
///    
/// PARAMS:
///    eType - The type of entity
///    iIndex - The index of the entity
PROC SET_ENTITY_SPAWNING(eENTITY_TYPE eType, INT iIndex)

	IF serverBD.iEntitySpawnIndex = -1
		serverBD.eEntitySpawnType = eType
		serverBD.iEntitySpawnIndex = iIndex
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - SET_ENTITY_SPAWNING - eType = ",eType," iIndex = ", iIndex)
	ENDIF

ENDPROC

/// PURPOSE: Clears the spawn system to allow other entities to be spawned
///    
PROC CLEAR_ENTITY_SPAWNING()

	IF serverBD.iEntitySpawnIndex != -1
		serverBD.vEntitySpawnCoord = <<0.0, 0.0, 0.0>>
		serverBD.fEntitySpawnHeading = 0.0
		iEntityFindSpawnCoordAttempts = 0
		
		serverBD.eEntitySpawnType = eENTITYTYPE_INVALID
		serverBD.iEntitySpawnIndex = -1
		#IF MAX_NUM_SPAWN_POSITIONS
		serverBD.iEntitySpawnPositionIndex = -1
		#ENDIF
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - CLEAR_ENTITY_SPAWNING")
	ENDIF

ENDPROC

FUNC INT GET_CURRENT_DROPOFF_INDEX()
	RETURN playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentDropoff
ENDFUNC

FUNC INT GET_CURRENT_TAKE_OUT_TARGET_STAGE()
	RETURN serverBD.iCurrentTargetStage
ENDFUNC

PROC INCREMENT_TAKE_OUT_TARGETS_REMAINING()
	sTakeOutTargetLocal.iTargetsRemaining++
ENDPROC

PROC INIT_TAKE_OUT_TARGET_DATA_THIS_FRAME()
	IF sTakeOutTargetLocal.iTargetsRemaining > sTakeOutTargetLocal.iTargetsRegistered
		sTakeOutTargetLocal.iTargetsRegistered = sTakeOutTargetLocal.iTargetsRemaining
	ENDIF
	sTakeOutTargetLocal.iTargetsRemaining = 0
ENDPROC

PROC DRAW_TAKE_OUT_TARGET_ENTITY_MARKER(eENTITY_TYPE eType, INT iEntity, ENTITY_INDEX entityId)
	SWITCH eType
		CASE eENTITYTYPE_PED
			IF IS_DATA_BIT_SET(eDATABITSET_DRAW_MARKERS_ABOVE_PED_TARGETS)
				DRAW_GENERIC_ENTITY_MARKER(eType, iEntity, entityId, HUD_COLOUR_RED)
			ENDIF
		BREAK
		CASE eENTITYTYPE_VEHICLE
			IF IS_DATA_BIT_SET(eDATABITSET_DRAW_MARKERS_ABOVE_VEHICLE_TARGETS)
				DRAW_GENERIC_ENTITY_MARKER(eType, iEntity, entityId, HUD_COLOUR_RED)
			ENDIF
		BREAK
		CASE eENTITYTYPE_PROP
			IF IS_DATA_BIT_SET(eDATABITSET_DRAW_MARKERS_ABOVE_PROP_TARGETS)
				DRAW_GENERIC_ENTITY_MARKER(eType, iEntity, entityId, HUD_COLOUR_RED)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

STRUCT SCRIPT_EVENT_DATA_FREEMODE_CONTENT_UPDATE_CURRENT_CHECKPOINT
	STRUCT_EVENT_COMMON_DETAILS Details
	INT iCheckpointOnRouteTo
	INT iPed
	INT iFrameCount
	INT iFMMC
	INT iInstance
ENDSTRUCT

PROC BROADCAST_EVENT_FREEMODE_CONTENT_UPDATE_CURRENT_CHECKPOINT(INT iCheckpointOnRouteTo, INT iPed)
	
	SCRIPT_EVENT_DATA_FREEMODE_CONTENT_UPDATE_CURRENT_CHECKPOINT Event
	Event.Details.Type = SCRIPT_EVENT_FREEMODE_CONTENT_UPDATE_CURRENT_CHECKPOINT
	Event.Details.FromPlayerIndex = LOCAL_PLAYER_INDEX
	
	Event.iCheckpointOnRouteTo = iCheckpointOnRouteTo
	Event.iPed = iPed
	Event.iFrameCount = GET_FRAME_COUNT()
	Event.iFMMC = sMission.iType
	Event.iInstance = NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT()
	
	PRINTLN("BROADCAST_EVENT_FREEMODE_CONTENT_UPDATE_CURRENT_CHECKPOINT: iFMMC = ", Event.iFMMC)
	PRINTLN("BROADCAST_EVENT_FREEMODE_CONTENT_UPDATE_CURRENT_CHECKPOINT: iInstance = ", Event.iInstance)
	PRINTLN("BROADCAST_EVENT_FREEMODE_CONTENT_UPDATE_CURRENT_CHECKPOINT: iCheckpointOnRouteTo = ", Event.iCheckpointOnRouteTo)
	PRINTLN("BROADCAST_EVENT_FREEMODE_CONTENT_UPDATE_CURRENT_CHECKPOINT: iPed = ", Event.iPed)
	PRINTLN("BROADCAST_EVENT_FREEMODE_CONTENT_UPDATE_CURRENT_CHECKPOINT: iFrameCount = ", Event.iFrameCount)
	
	INT iPlayersFlag = ALL_PLAYERS_ON_SCRIPT()
	IF iPlayersFlag != 0
		NET_PRINT("BROADCAST_EVENT_FREEMODE_CONTENT_UPDATE_CURRENT_CHECKPOINT - called by local player" ) NET_NL()	
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iPlayersFlag)	
	ELSE
		NET_PRINT("BROADCAST_EVENT_FREEMODE_CONTENT_UPDATE_CURRENT_CHECKPOINT - playerflags = 0 so not broadcasting") NET_NL()		
	ENDIF	
	
ENDPROC

FUNC VECTOR GET_ENTITY_DATA_COORDS_FOR_ENTITY_TYPE(eENTITY_TYPE eType, INT iIndex)
	SWITCH eType
		CASE eENTITYTYPE_MISSION_ENTITY		RETURN data.MissionEntity.MissionEntities[iIndex].vCoords
		CASE eENTITYTYPE_PED				RETURN data.Ped.Peds[iIndex].vCoords
		CASE eENTITYTYPE_VEHICLE			RETURN data.Vehicle.Vehicles[iIndex].vCoords
		CASE eENTITYTYPE_PROP				RETURN data.Prop.Props[iIndex].vCoords
	ENDSWITCH
	RETURN <<0.0, 0.0, 0.0>>
ENDFUNC

#IF MAX_NUM_SPAWN_POSITIONS
// url:bugstar:7523499 - Freemode Creator - Investigate better ways to handle when we want an entity to spawn randomly out of several locations
// function collection for spawn position variations

FUNC INT GET_ENTITY_SPAWN_POSITION_VARIATION_COUNT(INT iEntity, ENTITY_TYPE eType)
	// Count defined variation counts
	INT iSpawnPositionsIndex, iSpawnCount = 0
	REPEAT data.SpawnPosition.iCount iSpawnPositionsIndex
		IF eType = INT_TO_ENUM(ENTITY_TYPE, data.SpawnPosition.Positions[iSpawnPositionsIndex].iEntityType)
		AND iEntity = data.SpawnPosition.Positions[iSpawnPositionsIndex].iEntity
			// Count
			++iSpawnCount
		ENDIF
	ENDREPEAT
	
	RETURN iSpawnCount
ENDFUNC

/// PURPOSE:
///    Get a index of a ped spawn position incl. spawn position variations
/// PARAMS:
///    iPed - ped index in data array
/// RETURNS:
///    
FUNC INT GET_RANDOM_ENTITY_POSITION_INDEX(INT iEntity, ENTITY_TYPE eType)
	// Set variation count
	// Create list with potential position indexes
	INT iSpawnVariationsIndex
	INT iSpawnVariations[MAX_NUM_SPAWN_POSITIONS+1]
	INT iSpawnCount = 1
	iSpawnVariations[0] = -1 // default position entry
	
	REPEAT data.SpawnPosition.iCount iSpawnVariationsIndex
		IF eType = INT_TO_ENUM(ENTITY_TYPE, data.SpawnPosition.Positions[iSpawnVariationsIndex].iEntityType)
		AND iEntity = data.SpawnPosition.Positions[iSpawnVariationsIndex].iEntity
			// Collect numbers
			iSpawnVariations[iSpawnCount] = iSpawnVariationsIndex
			++iSpawnCount
		ENDIF
	ENDREPEAT
	
	// Select random vehicle index
	INT iRandomIndex = iSpawnVariations[GET_RANDOM_INT_IN_RANGE(0, iSpawnCount)]
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [PEDS][SPAWN] GET_RANDOM_ENTITY_POSITION_INDEX - iEntity: ", iEntity ,"; eType: ", ENUM_TO_INT(eType) ,"; iRandomIndex: ", iRandomIndex)
	RETURN iRandomIndex
ENDFUNC

#ENDIF

#IF MAX_NUM_SEARCH_AREAS
FUNC VECTOR GET_SEARCH_AREA_TRIGGER_COORDS(INT iSearchArea)
	IF logic.SearchArea.TriggerCoords != NULL
		RETURN CALL logic.SearchArea.TriggerCoords(iSearchArea)
	ENDIF
	
	IF data.SearchArea.Areas[iSearchArea].eEntityType != eENTITYTYPE_INVALID
		RETURN GET_ENTITY_DATA_COORDS_FOR_ENTITY_TYPE(data.SearchArea.Areas[iSearchArea].eEntityType, data.SearchArea.Areas[iSearchArea].iEntityIndex)
	ENDIF

	RETURN data.SearchArea.Areas[iSearchArea].vCoords
ENDFUNC
#ENDIF

FUNC BOOL IS_LOCAL_PLAYER_IN_VEHICLE_WITH_PERMANENT_PLAYER(BOOL bIncludeLocal = TRUE)
	IF IS_PED_IN_ANY_VEHICLE(LOCAL_PED_INDEX)
		VEHICLE_INDEX vehId = GET_VEHICLE_PED_IS_IN(LOCAL_PED_INDEX)
		INT iMaxSeats = (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(vehId) + 1) //Add 1 for the driver
		INT iSeat
		REPEAT iMaxSeats iSeat
			PED_INDEX pedId = GetPedUsingVehicleSeat(vehId, INT_TO_ENUM(VEHICLE_SEAT, iSeat-1), FALSE)
			IF DOES_ENTITY_EXIST(pedId)
			AND IS_PED_A_PLAYER(pedId)
				PLAYER_INDEX playerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId)
				IF IS_PLAYER_PERMANENT(playerId)
					IF NOT bIncludeLocal
						IF pedId != LOCAL_PED_INDEX
							RETURN TRUE
						ENDIF
					ELSE
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC

////////////////////////
////   DATA COUNTS
///////////////////////
    
#IF MAX_NUM_COLLECT_PEDS
FUNC INT GET_NUM_COLLECT_PEDS()
	INT iCount, iLoop
	REPEAT MAX_NUM_COLLECT_PEDS iLoop
		IF data.CollectPeds[iLoop].iPed != (-1)
		OR data.CollectPeds[iLoop].iMissionEntity != (-1)
			++iCount
		ENDIF
	ENDREPEAT
		
	RETURN iCount
ENDFUNC
#ENDIF

#IF MAX_NUM_LEAVE_AREAS
FUNC INT GET_NUM_LEAVE_AREAS()
	INT iCount, iLoop
	REPEAT MAX_NUM_LEAVE_AREAS iLoop
		IF NOT IS_VECTOR_ZERO(data.LeaveArea[iLoop].vCoords)
		AND data.LeaveArea[iLoop].fRadius != 0.0
			++iCount
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC
#ENDIF

#IF MAX_NUM_MOVING_DOORS
FUNC INT GET_NUM_MOVING_DOORS()
	INT iCount, iLoop
	REPEAT MAX_NUM_MOVING_DOORS iLoop
		IF NOT IS_VECTOR_ZERO(data.MovingDoor.MovingDoors[iLoop].vCoord)
			++iCount
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC
#ENDIF

#IF MAX_NUM_WEAPON_PICKUPS
FUNC INT GET_NUM_WEAPON_PICKUPS()
	INT iCount, iLoop
	REPEAT MAX_NUM_WEAPON_PICKUPS iLoop
		IF data.WeaponPickup[iLoop].eType != PICKUP_TYPE_INVALID
			++iCount
		ENDIF
	ENDREPEAT
		
	RETURN iCount
ENDFUNC
#ENDIF

#IF MAX_NUM_FOCUS_CAMS
FUNC INT GET_NUM_FOCUS_CAMS()
	INT iCount, iLoop
	REPEAT MAX_NUM_FOCUS_CAMS iLoop
		IF data.FocusCam.Cam[iLoop].eFocusType != ET_NONE
			++iCount
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC
#ENDIF

#IF MAX_NUM_BOTTOM_RIGHT_HUD
FUNC INT GET_NUM_BOTTOM_RIGHT_HUDS()
	INT iCount, iLoop
	REPEAT MAX_NUM_BOTTOM_RIGHT_HUD iLoop
		++iCount
	ENDREPEAT
		
	RETURN iCount
ENDFUNC
#ENDIF

#IF MAX_NUM_ANIMATIONS
FUNC INT GET_NUM_ANIMATIONS()
	INT iCount, iLoop
	REPEAT MAX_NUM_ANIMATIONS iLoop
		IF NOT IS_STRING_NULL_OR_EMPTY(data.Animation.Anim[iLoop].sName)
			++iCount
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC
#ENDIF

#IF MAX_NUM_ANIMATION_DICTIONARIES
FUNC INT GET_NUM_ANIMATION_DICTS()
	INT iCount, iLoop
	REPEAT MAX_NUM_ANIMATION_DICTIONARIES iLoop
		IF NOT IS_STRING_NULL_OR_EMPTY(data.Animation.Dict[iLoop].sName)
			++iCount
		ENDIF
	ENDREPEAT
	RETURN iCount
ENDFUNC
#ENDIF

#IF MAX_NUM_MISSION_PORTALS
FUNC INT GET_NUM_PORTALS()
	INT iCount, iLoop
	REPEAT MAX_NUM_MISSION_PORTALS iLoop
		IF NOT IS_VECTOR_ZERO(data.Portal[iLoop].vCoord)
			++iCount
		ENDIF
	ENDREPEAT
	
	RETURN iCount
ENDFUNC
#ENDIF

////////////////////////
////   FLAGS
///////////////////////

FUNC BOOL IS_IK_CONTROL_FLAG_SET(IK_CONTROL_FLAGS iControlFlagField, IK_CONTROL_FLAGS iFlagMask)
	RETURN IS_BITMASK_ENUM_AS_ENUM_SET(iControlFlagField, iFlagMask)
ENDFUNC

FUNC BOOL IS_CUTSCENE_HELP_VEHICLE_CLONE_FLAG_SET(CUTSCENE_HELP_VEHICLE_CLONE_FLAGS iControlFlagField, CUTSCENE_HELP_VEHICLE_CLONE_FLAGS iFlagMask)
	RETURN IS_BITMASK_ENUM_AS_ENUM_SET(iControlFlagField, iFlagMask)
ENDFUNC

////////////////////////
////   BITSETS
///////////////////////

#IF MAX_NUM_MOVING_DOORS
FUNC BOOL IS_MOVING_DOOR_DATA_BIT_SET(INT iMovingDoor, eMOVING_DOORS_DATA_BITSET eBitset)
	RETURN IS_ARRAYED_BIT_ENUM_SET(data.MovingDoor.MovingDoors[iMovingDoor].iBitset, eBitset)
ENDFUNC
#ENDIF
