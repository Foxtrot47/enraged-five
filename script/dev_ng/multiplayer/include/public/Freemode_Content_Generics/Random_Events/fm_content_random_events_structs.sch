
USING "globals.sch"
USING "rage_builtins.sch"
USING "fm_content_includes.sch"

#IF NOT DEFINED(PLAYER_BROADCAST_DATA_SIZE)
CONST_INT PLAYER_BROADCAST_DATA_SIZE	NUM_NETWORK_PLAYERS
#ENDIF

CONST_INT MAX_NUM_EVENT_LOCATION_BLIPS		1

BLIP_INDEX eventAreaBlip[MAX_NUM_EVENT_LOCATION_BLIPS]
BLIP_INDEX eventAreaCentreBlip[MAX_NUM_EVENT_LOCATION_BLIPS]

FUNC INT GET_VARIATION_TO_LAUNCH()
	INT iID = NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT()
	IF iID != -1
		PRINTLN("[fm_content][random_events] - GET_VARIATION_TO_LAUNCH - GlobalServerBD_RandomEvents.Events[",iID,"].iVariation = ", GlobalServerBD_RandomEvents.Events[iID].iVariation)
		RETURN GlobalServerBD_RandomEvents.Events[iID].iVariation
	ENDIF
	
	PRINTLN("[fm_content][random_events] - GET_VARIATION_TO_LAUNCH - iVariation = -1 !!!!!! ")
	RETURN -1
ENDFUNC

FUNC INT GET_SUBVARIATION_TO_LAUNCH()
	INT iID = NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT()
	IF iID != -1
		PRINTLN("[fm_content][random_events] - GET_VARIATION_TO_LAUNCH - GlobalServerBD_RandomEvents.Events[",iID,"].iSubvariation = ", GlobalServerBD_RandomEvents.Events[iID].iSubvariation)
		RETURN GlobalServerBD_RandomEvents.Events[iID].iSubvariation
	ENDIF
	
	PRINTLN("[fm_content][random_events] - GET_SUBVARIATION_TO_LAUNCH - iSubvariation = -1 !!!!!! ")
	RETURN -1
ENDFUNC
