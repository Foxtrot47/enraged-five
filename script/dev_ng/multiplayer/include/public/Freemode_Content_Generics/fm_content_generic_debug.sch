
#IF IS_DEBUG_BUILD

USING "fm_content_generic_support.sch"

FUNC PED_BONETAG GET_PROP_PLACEMENT_ATTACHMENT_PED_BONETAG(INT i)
	SWITCH i
		CASE 0	RETURN	BONETAG_ROOT
		CASE 1	RETURN	BONETAG_PELVIS
		CASE 2	RETURN	BONETAG_SPINE
		CASE 3	RETURN	BONETAG_SPINE1
		CASE 4	RETURN	BONETAG_SPINE2
		CASE 5	RETURN	BONETAG_SPINE3
		CASE 6	RETURN	BONETAG_NECK
		CASE 7	RETURN	BONETAG_HEAD
		CASE 8	RETURN	BONETAG_R_CLAVICLE
		CASE 9	RETURN	BONETAG_R_UPPERARM
		CASE 10	RETURN	BONETAG_R_FOREARM
		CASE 11	RETURN	BONETAG_R_HAND
		CASE 12	RETURN	BONETAG_R_FINGER0
		CASE 13	RETURN	BONETAG_R_FINGER01
		CASE 14	RETURN	BONETAG_R_FINGER02
		CASE 15	RETURN	BONETAG_R_FINGER1
		CASE 16	RETURN	BONETAG_R_FINGER11
		CASE 17	RETURN	BONETAG_R_FINGER12	
		CASE 18	RETURN	BONETAG_R_FINGER2
		CASE 19	RETURN	BONETAG_R_FINGER21
		CASE 20	RETURN	BONETAG_R_FINGER22
		CASE 21	RETURN	BONETAG_R_FINGER3
		CASE 22	RETURN	BONETAG_R_FINGER31
		CASE 23	RETURN	BONETAG_R_FINGER32
		CASE 24	RETURN	BONETAG_R_FINGER4
		CASE 25	RETURN	BONETAG_R_FINGER41
		CASE 26	RETURN	BONETAG_R_FINGER42
		CASE 27	RETURN	BONETAG_L_CLAVICLE
		CASE 28	RETURN	BONETAG_L_UPPERARM	
		CASE 29	RETURN	BONETAG_L_FOREARM
		CASE 30	RETURN	BONETAG_L_HAND
		CASE 31	RETURN	BONETAG_L_FINGER0
		CASE 32	RETURN	BONETAG_L_FINGER01
		CASE 33	RETURN	BONETAG_L_FINGER02
		CASE 34	RETURN	BONETAG_L_FINGER1
		CASE 35	RETURN	BONETAG_L_FINGER11
		CASE 36	RETURN	BONETAG_L_FINGER12
		CASE 37	RETURN	BONETAG_L_FINGER2
		CASE 38	RETURN	BONETAG_L_FINGER21
		CASE 39	RETURN	BONETAG_L_FINGER22
		CASE 40	RETURN	BONETAG_L_FINGER3
		CASE 41	RETURN	BONETAG_L_FINGER31
		CASE 42	RETURN	BONETAG_L_FINGER32
		CASE 43	RETURN	BONETAG_L_FINGER4
		CASE 44	RETURN	BONETAG_L_FINGER41
		CASE 45	RETURN	BONETAG_L_FINGER42
		CASE 46	RETURN	BONETAG_L_THIGH
		CASE 47	RETURN	BONETAG_L_CALF
		CASE 48	RETURN	BONETAG_L_FOOT
		CASE 49	RETURN	BONETAG_L_TOE
		CASE 50	RETURN	BONETAG_R_THIGH
		CASE 51	RETURN	BONETAG_R_CALF
		CASE 52	RETURN	BONETAG_R_FOOT
		CASE 53	RETURN	BONETAG_R_TOE
		CASE 54	RETURN	BONETAG_PH_L_HAND
		CASE 55	RETURN	BONETAG_PH_R_HAND
	ENDSWITCH
	
	RETURN BONETAG_NULL
ENDFUNC

PROC UPDATE_SERVER_GAME_STATE_WIDGET()
//	SET_CONTENTS_OF_TEXT_WIDGET(twIdServerGameState, GET_GAME_STATE_NAME(GET_GAME_STATE()))
//	SET_CONTENTS_OF_TEXT_WIDGET(twIdModeState, GET_MODE_STATE_NAME(GET_MODE_STATE()))
ENDPROC

TWEAK_FLOAT TweakX 0.0
TWEAK_FLOAT TweakY 0.0
TWEAK_FLOAT TweakZ 0.0

PROC CREATE_WIDGETS()
	IF IS_DEBUG_LOCAL_BIT_SET(eDEBUGLOCALBITSET_CREATED_WIDGETS)
		EXIT
	ENDIF
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ShowMissionEntityArrayIndexes")
		sDebugVars.bShowEntityArrayIndexes = TRUE
	ENDIF
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_AdditionalFMContentPrints")
//		bDoAmbushPrints = TRUE
//		bDoDeliveryPrints = TRUE
//		bDoDestroyablePrints = TRUE
//		bDoFlashingGpsPrints = TRUE
//		bParticipantLoopPrints = TRUE
//		bDoFlashinPlayerBlipgDebugPrints = TRUE
//		bShowUndriveablePrints = TRUE
		sDebugVars.bEnableDamageEventPrints = TRUE
//		bDoLoseCopsObjectivePrints = TRUE
//		bDoBottomRightPrints = TRUE
//		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] CREATE_WIDGETS - sc_AdditionalFreemodeContentPrints active. Turning on all additional prints.")
	ENDIF
	
	INT i
	TEXT_LABEL_63 tl63
	
	UNUSED_PARAMETER(i)
	UNUSED_PARAMETER(tl63)
	TEXT_LABEL_63 tlWidgetGroupName
	tlWidgetGroupName = "   FMContent Script Widgets - "
	tlWidgetGroupName += GET_CURRENT_VARIATION_STRING()
	tlWidgetGroupName += " "
	tlWidgetGroupName += sMission.iInstance
	
	START_WIDGET_GROUP(CONVERT_TEXT_LABEL_TO_STRING(tlWidgetGroupName))
		#IF MAX_NUM_GOTO_LOCATIONS
		INT iGoto = GET_CURRENT_GO_TO_POINT()
		ADD_WIDGET_INT_READ_ONLY("GET_CURRENT_GO_TO_POINT", iGoto)
		ADD_WIDGET_FLOAT_READ_ONLY("fMyDistanceFromGoToLocation", fMyDistanceFromGoToLocation)
		#ENDIF
		ADD_WIDGET_FLOAT_READ_ONLY("fMyDistanceFromDropOff", fMyDistanceFromDropOff)
		ADD_WIDGET_BOOL("Draw VehicleGoToPoint Checkpoints", sDebugVars.bDrawVehicleGoToPointCheckpoints)
		ADD_WIDGET_BOOL("Draw VehicleGoToPoint Checkpoint IDs", sDebugVars.bDrawVehicleGoToPointCheckpointIDs)
		ADD_WIDGET_BOOL("Flip Carrier Vehicle", sDebugVars.bFlipCarrierVehicle)
		ADD_WIDGET_BOOL("Request to be host of this script", sDebugVars.bRequestToBeHostOfThisScript)
		#IF MAX_NUM_CHECKPOINTS
		ADD_WIDGET_INT_READ_ONLY("data.Checkpoint.iCount", data.Checkpoint.iCount)
		#ENDIF
		
		#IF MAX_NUM_CUTSCENES
		START_WIDGET_GROUP("Cutscenes")
			ADD_WIDGET_INT_READ_ONLY("Current scene", sDebugVars.Cutscene.iScene)
			
			START_WIDGET_GROUP("Anim synced scene")
				ADD_WIDGET_BOOL("Pause", sDebugVars.Cutscene.bPause)
				ADD_WIDGET_FLOAT_SLIDER("Phase", sDebugVars.Cutscene.fPhase, 0.0, 1.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("Rate", sDebugVars.Cutscene.fRate, 0.0, 2.0, 0.001)
				ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
				ADD_WIDGET_VECTOR_SLIDER("Position Local Offset", sDebugVars.Cutscene.vPositionLocalOffset, -5.0, 5.0, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("Orientation Offset", sDebugVars.Cutscene.vOrientationOffset, -180.0, 180.0, 1.0)
				ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
				sDebugVars.Cutscene.twFinalPosition = ADD_TEXT_WIDGET("Final Position")
				SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.Cutscene.twFinalPosition, "")
				sDebugVars.Cutscene.twFinalOrientation = ADD_TEXT_WIDGET("Final Orientation")
				SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.Cutscene.twFinalOrientation, "")
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		#ENDIF
		
		#IF MAX_NUM_DIALOGUE
		START_WIDGET_GROUP("Dialogue")
			ADD_WIDGET_BOOL("Skip current", sDebugVars.Dialogue.bSkipCurrent)
		STOP_WIDGET_GROUP()
		#ENDIF
		
		#IF MAX_NUM_TARGETS
		START_WIDGET_GROUP("Targets")
			ADD_WIDGET_INT_READ_ONLY("iTargetsRemaining", sTakeOutTargetLocal.iTargetsRemaining)
			ADD_WIDGET_INT_READ_ONLY("iTargetsRegistered", sTakeOutTargetLocal.iTargetsRegistered)
		STOP_WIDGET_GROUP()
		#ENDIF
		
		#IF MAX_NUM_SEARCH_AREAS
		START_WIDGET_GROUP("Search Area")
			ADD_WIDGET_INT_READ_ONLY("Current area", sSearchArea.iCurrentArea)
			ADD_WIDGET_FLOAT_READ_ONLY("Current trigger distance", sDebugVars.SearchArea.fTriggerDistance)
			ADD_WIDGET_BOOL("Show triggers", sDebugVars.SearchArea.bShowTriggers)
			
			REPEAT MAX_NUM_SEARCH_AREAS i
				ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
				
				tl63 = "Area: "
				tl63 += i
				
				ADD_WIDGET_STRING(tl63)
				ADD_WIDGET_INT_READ_ONLY("Enabled", sDebugVars.SearchArea.Areas[i].iEnabled)
				ADD_WIDGET_FLOAT_READ_ONLY("Centre distance", sDebugVars.SearchArea.Areas[i].fCentreDistance)
			ENDREPEAT
		STOP_WIDGET_GROUP()
		#ENDIF
		
		#IF MAX_NUM_LEAVE_AREAS
		START_WIDGET_GROUP("Leave Area")
			ADD_WIDGET_BOOL("Enable debug prints", sDebugVars.LeaveArea.bEnableDebugPrints)
		STOP_WIDGET_GROUP()
		#ENDIF
		
		#IF MAX_NUM_TAKE_PHOTOGRAPHS
		START_WIDGET_GROUP("Tracked Point Testing")
			ADD_WIDGET_BOOL("Render Created Tracked Points", sDebugVars.TrackedPoints.bRenderTrackedPoints)
//			ADD_WIDGET_BOOL("Enable/Disable", sDebugVars.TrackedPoints.bDebugTestTrackedPoint)
//			START_WIDGET_GROUP("Vars")
//				ADD_WIDGET_FLOAT_SLIDER("Offset X", sDebugVars.TrackedPoints.fTrackedPointOffsetX, -20.0, 20.0, 0.01)
//				ADD_WIDGET_FLOAT_SLIDER("Offset Y", sDebugVars.TrackedPoints.fTrackedPointOffsetY, -20.0, 20.0, 0.01)
//				ADD_WIDGET_FLOAT_SLIDER("Offset Z", sDebugVars.TrackedPoints.fTrackedPointOffsetZ, -20.0, 20.0, 0.01)
//				ADD_WIDGET_FLOAT_SLIDER("Radius", sDebugVars.TrackedPoints.fTrackedPointRadius, -20.0, 20.0, 0.01)
//			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		#ENDIF
		
		#IF MAX_NUM_COLLISION_TESTS
		START_WIDGET_GROUP("Collision Tests")
			ADD_WIDGET_BOOL("Enable", sDebugVars.CollisionTests.bEnable)
			
			START_WIDGET_GROUP("Draw Box")
				ADD_WIDGET_BOOL("Enable", sDebugVars.CollisionTests.DrawBox.bEnable)
				ADD_WIDGET_INT_SLIDER("Colour", sDebugVars.CollisionTests.DrawBox.iColour, 0, COUNT_OF(HUD_COLOURS) - 1, 1)
				ADD_WIDGET_INT_SLIDER("Alpha", sDebugVars.CollisionTests.DrawBox.iAlpha, 0, 255, 1)
				
				START_NEW_WIDGET_COMBO()
				REPEAT COUNT_OF(eDEBUG_DRAW_BOX_TYPE) i
					ADD_TO_WIDGET_COMBO(GET_DEBUG_DRAW_BOX_TYPE_NAME(INT_TO_ENUM(eDEBUG_DRAW_BOX_TYPE, i)))
				ENDREPEAT
				STOP_WIDGET_COMBO("Type", sDebugVars.CollisionTests.DrawBox.iType)
				
				ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
				ADD_WIDGET_BOOL("Use player position/orientation", sDebugVars.CollisionTests.DrawBox.bUsePlayerPositionOrientation)
				ADD_WIDGET_VECTOR_SLIDER("Position", sDebugVars.CollisionTests.DrawBox.vPosition, -10000.0, 10000.0, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("Orientation", sDebugVars.CollisionTests.DrawBox.vOrientation, -360.0, 360.0, 1.0)
				ADD_WIDGET_VECTOR_SLIDER("Dimensions", sDebugVars.CollisionTests.DrawBox.vDimensions, 0.0, 100.0, 0.1)
				ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
				ADD_WIDGET_VECTOR_SLIDER("Position Local Offset", sDebugVars.CollisionTests.DrawBox.vPositionLocalOffset, -5.0, 5.0, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("Orientation Offset", sDebugVars.CollisionTests.DrawBox.vOrientationOffset, -360.0, 360.0, 1.0)
				ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
				sDebugVars.CollisionTests.DrawBox.twFinalPosition = ADD_TEXT_WIDGET("Final Position")
				SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.CollisionTests.DrawBox.twFinalPosition, "")
				sDebugVars.CollisionTests.DrawBox.twFinalOrientation = ADD_TEXT_WIDGET("Final Orientation")
				SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.CollisionTests.DrawBox.twFinalOrientation, "")
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		#ENDIF
		
		#IF MAX_NUM_TRIGGER_AREAS
		START_WIDGET_GROUP("Trigger Areas")
			ADD_WIDGET_INT_SLIDER("Index", sDebugVars.TriggerArea.iSelected, -1, MAX_NUM_TRIGGER_AREAS - 1, 1)
			ADD_WIDGET_VECTOR_SLIDER("Min", sDebugVars.TriggerArea.vMin, -10000.0, 10000.0, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("Max", sDebugVars.TriggerArea.vMax, -10000.0, 10000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Width", sDebugVars.TriggerArea.fWidth, 0.0, 999999.0, 0.1)
		STOP_WIDGET_GROUP()
		#ENDIF
		
		#IF ENABLE_VEHICLE_VALUE
		START_WIDGET_GROUP("Vehicle Value")
			ADD_WIDGET_INT_SLIDER("Base Value", sDebugVars.VehicleValue.iBaseValue, 0, HIGHEST_INT, 1000)
			ADD_WIDGET_INT_SLIDER("Num Hits", serverBD.sVehicleValue.iNumHits, 0, HIGHEST_INT, 1)
		STOP_WIDGET_GROUP()
		#ENDIF
		
		#IF MAX_NUM_SECURITY_CAMERAS
		START_WIDGET_GROUP("Security Camera")
			ADD_WIDGET_INT_SLIDER("iCamera", sDebugVars.SecurityCamera.iCamera, -1, data.SecurityCamera.iNumCameras - 1, 1)
			ADD_WIDGET_BOOL("Draw Debug", sDebugVars.SecurityCamera.bDrawDebug)
			ADD_WIDGET_BOOL("Disable Alert", sDebugVars.SecurityCamera.bDisableAlert)
			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
			ADD_WIDGET_FLOAT_SLIDER("Vision Range", sDebugVars.SecurityCamera.fVisionRange, 0.0, 30.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Vision Width", sDebugVars.SecurityCamera.fVisionWidth, 0.0, 30.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Vision Cone Size", sDebugVars.SecurityCamera.fVisionConeCentreOfGazeMaxAngle, 0.0, 5.0, 0.01)
			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
			ADD_WIDGET_BOOL("Use Debug Offsets", sDebugVars.SecurityCamera.bUseDebugOffsets)
			ADD_WIDGET_VECTOR_SLIDER("Co-ordinates Offset", sDebugVars.SecurityCamera.vCoordsOffset, -10.0, 10.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Forward Vector Rotation Offset z", sDebugVars.SecurityCamera.fForwardVectorRotationOffsetZ, -180.0, 180.0, 0.1)
			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
			ADD_WIDGET_BOOL("Use Debug Turning", sDebugVars.SecurityCamera.bUseDebugTurning)
			ADD_WIDGET_FLOAT_SLIDER("Target Offset Rotation z", sDebugVars.SecurityCamera.fTurningTargetOffsetRotationZ, -180.0, 180.0, 0.1)
			ADD_WIDGET_INT_SLIDER("Turn Time (ms)", sDebugVars.SecurityCamera.iTurnTimeMS, 0, 30000, 1)
			ADD_WIDGET_INT_SLIDER("Wait Time (ms)", sDebugVars.SecurityCamera.iWaitTimeMS, 0, 30000, 1)
			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
			ADD_WIDGET_BOOL("View Marker Force Enable", sDebugVars.SecurityCamera.bViewMarkerForceEnable)
			ADD_WIDGET_VECTOR_SLIDER("View Marker Local Offset", sDebugVars.SecurityCamera.vViewMarkerLocalOffset, -360.0, 360.0, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("View Marker Scale Override", sDebugVars.SecurityCamera.vViewMarkerScale, -10.0, 10.0, 0.01)
			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
			ADD_WIDGET_BOOL("Light Force Enable", sDebugVars.SecurityCamera.bLightForceEnable)
			ADD_WIDGET_FLOAT_SLIDER("Light Base Intensity", sDebugVars.SecurityCamera.fLightBaseIntensity, 0.0, 10.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Light Range", sDebugVars.SecurityCamera.fLightRange, 0.0, 5.0, 0.1)
		STOP_WIDGET_GROUP()
		#ENDIF
		
		#IF MAX_NUM_CHECKPOINTS
		START_WIDGET_GROUP("Collect Checkpoints")
			ADD_WIDGET_INT_SLIDER("Current checkpoint ID", playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId,0,data.Checkpoint.iCount,1)
			INT iDebugCheckpoint
			REPEAT data.Checkpoint.iCount iDebugCheckpoint
				tl63 = "Checkpoint "
				tl63 += iDebugCheckpoint
				START_WIDGET_GROUP(tl63)
					ADD_WIDGET_BOOL("Debug Show Checkpoint",sDebugVars.Checkpoints.sDebugData[iDebugCheckpoint].bDebugRender)
					ADD_WIDGET_VECTOR_SLIDER("Coord", sDebugVars.Checkpoints.sDebugData[iDebugCheckpoint].vDebugCoord,-5000.0,5000.0,1.0)
					ADD_WIDGET_FLOAT_SLIDER("Visual Radius",sDebugVars.Checkpoints.sDebugData[iDebugCheckpoint].fDebugVisualRadius,-1.0,50.0,1.0)
					ADD_WIDGET_FLOAT_SLIDER("Collect Radius",sDebugVars.Checkpoints.sDebugData[iDebugCheckpoint].fDebugCollectRadius,-1.0,50.0,1.0)
					sDebugVars.Checkpoints.sDebugData[iDebugCheckpoint].twCheckpointLocation = ADD_TEXT_WIDGET("Formatted Coords")
					tl63 = "Checkpoints[0].vCoords = <<>>"
					SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.Checkpoints.sDebugData[iDebugCheckpoint].twCheckpointLocation, tl63)
					sDebugVars.Checkpoints.sDebugData[iDebugCheckpoint].twCheckpointVisualRadius = ADD_TEXT_WIDGET("Formatted Visual Radius")
					tl63 = "Checkpoints[0].fVisualRadius = "
					SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.Checkpoints.sDebugData[iDebugCheckpoint].twCheckpointVisualRadius, tl63)
					sDebugVars.Checkpoints.sDebugData[iDebugCheckpoint].twCheckpointCollectRadius = ADD_TEXT_WIDGET("Formatted Collect Radius")
					tl63 = "Checkpoints[0].fCollectRadius = "
					SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.Checkpoints.sDebugData[iDebugCheckpoint].twCheckpointCollectRadius, tl63)
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
		#ENDIF
		
		IF GET_MODE_TIME_LIMIT() > -1
			START_WIDGET_GROUP("Mode Timer")
				ADD_WIDGET_INT_READ_ONLY("Elapsed Time (s)", sDebugVars.ModeTimer.iElapsedTimeS)
				ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
				ADD_WIDGET_STRING("Host Only")
				ADD_WIDGET_INT_SLIDER("Set Elapsed Time (s)", sDebugVars.ModeTimer.iSetElapsedTimeS, -1, GET_MODE_TIME_LIMIT() / 1000, 1)
				ADD_WIDGET_BOOL("Pause", sDebugVars.ModeTimer.bPause)
				ADD_WIDGET_BOOL("Fake End Of Mode", sDebugVars.ModeTimer.bFakeEndOfMode)
			STOP_WIDGET_GROUP()
		ENDIF
		
//		ADD_WIDGET_BOOL("Draw Patrol Checkpoints", sDebugVars.bDrawPatrolCheckpoints)
//		ADD_WIDGET_BOOL("Draw Patrol Checkpoint IDs", bDrawPatrolCheckpointIDs)
//		ADD_WIDGET_BOOL("Entity Spawn debug", g_SpawnData.bShowAdvancedSpew)
//		
//		ADD_WIDGET_FLOAT_READ_ONLY("fMyDistanceFromGoToLocation", fMyDistanceFromGoToLocation)
//		ADD_WIDGET_FLOAT_READ_ONLY("fMyDistanceFromMissionEntity[MISSION_ENTITY_ONE]", fMyDistanceFromMissionEntity[MISSION_ENTITY_ONE])
//		ADD_WIDGET_FLOAT_READ_ONLY("fMyDistanceFromMissionEntity[MISSION_ENTITY_TWO]", fMyDistanceFromMissionEntity[MISSION_ENTITY_TWO])
//			
//		ADD_WIDGET_INT_READ_ONLY("serverBD.iRandomModeInt0", serverBD.iRandomModeInt0)
//		ADD_WIDGET_INT_READ_ONLY("serverBD.iSelectedSpawn0", serverBD.iSelectedSpawn0)
//		
//		ADD_WIDGET_INT_READ_ONLY("serverBD.iTotalScore", serverBD.iTotalScore)	
//		ADD_WIDGET_INT_READ_ONLY("serverBD.iTargetPedsEliminated", serverBD.iTargetPedsEliminated)
//		ADD_WIDGET_INT_READ_ONLY("serverBD.iTargetVehDestroyedCount", serverBD.iTargetVehDestroyedCount)
//		ADD_WIDGET_INT_READ_ONLY("serverBD.iPropsDestroyed", serverBD.iPropsDestroyed)
//		ADD_WIDGET_INT_READ_ONLY("serverBD.iAmbushWavesCleared", serverBD.iAmbushWavesCleared)
//		
		START_WIDGET_GROUP("Additional Prints")
			ADD_WIDGET_BOOL("Ambush Prints", sDebugVars.bDoAmbushPrints)
			ADD_WIDGET_BOOL("Vehicle Prints", sDebugVars.bDoVehiclePrints)
			ADD_WIDGET_BOOL("Portal Prints", sDebugVars.bDoPortalPrints)
			ADD_WIDGET_BOOL("Vehicle Value Prints", sDebugVars.bDoVehicleValuePrints)
			ADD_WIDGET_BOOL("Heli Cam Prints", sDebugVars.bDoHeliCamPrints)
//			ADD_WIDGET_BOOL("Delivery Prints", bDoDeliveryPrints)
//			ADD_WIDGET_BOOL("Destroyable Prints", bDoDestroyablePrints)
//			ADD_WIDGET_BOOL("GPS Flash Prints", bDoFlashingGpsPrints)
//			ADD_WIDGET_BOOL("Participant Loop Prints", bParticipantLoopPrints)
//			ADD_WIDGET_BOOL("Player Blip Flash Prints", bDoFlashinPlayerBlipgDebugPrints)
//			ADD_WIDGET_BOOL("Undriveable Prints", bShowUndriveablePrints)
			ADD_WIDGET_BOOL("Damage Event Prints", sDebugVars.bEnableDamageEventPrints)
//			ADD_WIDGET_BOOL("Lose Cops Prints", bDoLoseCopsObjectivePrints)
//			ADD_WIDGET_BOOL("HUD Prints", bDoBottomRightPrints)
			ADD_WIDGET_BOOL("Ped Prints", sDebugVars.bDoPedPrints)			
			ADD_WIDGET_BOOL("Delivery Prints", sDebugVars.bDoDeliveryPrints)		
			ADD_WIDGET_BOOL("HUD Prints", sDebugVars.bDoHUDPrints)
		STOP_WIDGET_GROUP()
//				
//		START_WIDGET_GROUP("Entry Scene Debug")
//			ADD_WIDGET_BOOL("Loop Entry Scene", bLoopEntryScene)
//		STOP_WIDGET_GROUP()
//		
//		START_WIDGET_GROUP("Ped Seeing Range")
//			ADD_WIDGET_STRING("Select the ped you want to modify.")
//			ADD_WIDGET_INT_SLIDER("Ped #", iWidgetSeeingRangePed, -1, NUM_MISSION_PEDS, 1)
//			ADD_WIDGET_FLOAT_SLIDER("Seeing Range", fWidgetPedSeeingRange, 0.00, 100.00, 1.0)
//		STOP_WIDGET_GROUP()
//		
		START_WIDGET_GROUP("Entity Array Indexes")
			ADD_WIDGET_STRING("'Script/Draw Debug Lines And Spheres' also needs set to display entity indexes.")
			ADD_WIDGET_BOOL("Draw Entity Array Indexes", sDebugVars.bShowEntityArrayIndexes)
		STOP_WIDGET_GROUP()
//		
//		START_WIDGET_GROUP("Bullet Impact Area")
//			ADD_WIDGET_STRING("This lets you see the bullet impact areas in game.")
//			ADD_WIDGET_INT_SLIDER("Bullet Impact Area", iBulletImpactAreaToDraw, 0, MAX_NUM_BULLET_IMPACT_AREAS, 1)
//			ADD_WIDGET_BOOL("Draw Bullet Impact Area", bDrawBulletImpactArea)
//		STOP_WIDGET_GROUP()
//
		START_WIDGET_GROUP("Prop Placement")
			START_WIDGET_GROUP("Attachment")
				ADD_WIDGET_INT_SLIDER("iProp", sDebugVars.PropPlacement.Attachment.iProp, -1, MAX_NUM_PROPS - 1, 1)
				sDebugVars.PropPlacement.Attachment.twFocusEntity = ADD_TEXT_WIDGET("Focus entity")
				SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.Attachment.twFocusEntity, "")
				ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
				sDebugVars.PropPlacement.Attachment.twEntityBoneName = ADD_TEXT_WIDGET("Entity bone name")
				SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.Attachment.twEntityBoneName, "")
				
				START_NEW_WIDGET_COMBO()
					i = 0
					WHILE GET_PROP_PLACEMENT_ATTACHMENT_PED_BONETAG(i) != BONETAG_NULL
						ADD_TO_WIDGET_COMBO(GET_BONETAG_NAME_FOR_DEBUG(GET_PROP_PLACEMENT_ATTACHMENT_PED_BONETAG(i)))
						i++
					ENDWHILE
				STOP_WIDGET_COMBO("Ped bone tag", sDebugVars.PropPlacement.Attachment.iPedBoneTag)
				
				ADD_WIDGET_BOOL("Toggle attach", sDebugVars.PropPlacement.Attachment.bToggleAttach)
				ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
				ADD_WIDGET_VECTOR_SLIDER("Offset", sDebugVars.PropPlacement.Attachment.vOffset, -10.0, 10.0, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Rotation", sDebugVars.PropPlacement.Attachment.vRotation, -180.0, 180.0, 1.0)
				ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
				sDebugVars.PropPlacement.Attachment.twOffset = ADD_TEXT_WIDGET("Offset")
				SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.Attachment.twOffset, "")
				sDebugVars.PropPlacement.Attachment.twRotation = ADD_TEXT_WIDGET("Rotation")
				SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.Attachment.twRotation, "")
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_INT_SLIDER("iProp", sDebugVars.PropPlacement.iProp, -1, MAX_NUM_PROPS - 1, 1)
			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
			ADD_WIDGET_INT_READ_ONLY("Is attached?", sDebugVars.PropPlacement.iIsAttached)
			ADD_WIDGET_INT_READ_ONLY("Has collision?", sDebugVars.PropPlacement.iHasCollision)
			ADD_WIDGET_INT_READ_ONLY("Is visible?", sDebugVars.PropPlacement.iIsVisible)
			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
			ADD_WIDGET_BOOL("Detach", sDebugVars.PropPlacement.bDetach)
			ADD_WIDGET_BOOL("Toggle collision", sDebugVars.PropPlacement.bToggleCollision)
			ADD_WIDGET_BOOL("Toggle visibility", sDebugVars.PropPlacement.bToggleVisibility)
			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
			ADD_WIDGET_VECTOR_SLIDER("Position", sDebugVars.PropPlacement.vPosition, -10000.0, 10000.0, 0.01)
			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
			ADD_WIDGET_VECTOR_SLIDER("Rotation", sDebugVars.PropPlacement.vRotation, -180.0, 180.0, 1.0)
			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
			ADD_WIDGET_BOOL("Use focus entity offset", sDebugVars.PropPlacement.bUseFocusEntityOffset)
			ADD_WIDGET_VECTOR_SLIDER("Local Position Offset", sDebugVars.PropPlacement.vLocalPositionOffset, -10000.0, 10000.0, 0.01)
			sDebugVars.PropPlacement.twLocalPositionOffset = ADD_TEXT_WIDGET("Local Position Offset")
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.twLocalPositionOffset, "")
			ADD_WIDGET_VECTOR_SLIDER("Local Rotation Offset", sDebugVars.PropPlacement.vLocalRotationOffset, -180.0, 180.0, 1.0)
			sDebugVars.PropPlacement.twLocalRotationOffset = ADD_TEXT_WIDGET("Local Rotation Offset")
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.twLocalRotationOffset, "")
			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
			sDebugVars.PropPlacement.twFinalPosition = ADD_TEXT_WIDGET("Final Position")
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.twFinalPosition, "")
			sDebugVars.PropPlacement.twFinalRotation = ADD_TEXT_WIDGET("Final Rotation")
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.twFinalRotation, "")
		STOP_WIDGET_GROUP()
//		START_WIDGET_GROUP("Entity Mission Placement")
//			ADD_WIDGET_STRING("Use -2 for modifying the Find Item")
//			ADD_WIDGET_INT_SLIDER("Entity Mission Index", iEntityMissionPlacementIndex, -2, MAX_NUM_MISSION_ENTITIES, 1)
//			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
//			ADD_WIDGET_VECTOR_SLIDER("Entity Mission Position", vEntityMissionPlacementPosition, -10000.0, 10000.0, 0.1)
//			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
//			ADD_WIDGET_VECTOR_SLIDER("Entity Mission Rotation", vEntityMissionPlacementRotation, -10000.0, 10000.0, 0.1)
//			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
//			ADD_WIDGET_VECTOR_SLIDER("Entity Mission Local Offset", vEntityMissionPlacementLocalOffset, -10000.0, 10000.0, 0.1)
//			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
//			twEntityMissionFinalPosition = ADD_TEXT_WIDGET("Prop Final Position")
//			SET_CONTENTS_OF_TEXT_WIDGET(twEntityMissionFinalPosition, "")
//		STOP_WIDGET_GROUP()
//		START_WIDGET_GROUP("Ped Placement")
//			ADD_WIDGET_STRING("Use -2 for modifying the Find Item")
//			ADD_WIDGET_INT_SLIDER("Ped Index", iPedPlacementIndex, -2, NUM_MISSION_PEDS, 1)
//			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
//			ADD_WIDGET_VECTOR_SLIDER("Ped Position", vPedPlacementPosition, -10000.0, 10000.0, 0.1)
//			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
//			ADD_WIDGET_VECTOR_SLIDER("Ped Rotation", vPedPlacementRotation, -10000.0, 10000.0, 0.1)
//			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
//			ADD_WIDGET_VECTOR_SLIDER("Ped Local Offset", vPedPlacementLocalOffset, -10000.0, 10000.0, 0.1)
//			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
//			twPedFinalPosition = ADD_TEXT_WIDGET("Prop Final Position")
//			SET_CONTENTS_OF_TEXT_WIDGET(twPedFinalPosition, "")
//		STOP_WIDGET_GROUP()
//		
//		START_WIDGET_GROUP("Telemetry")
//			ADD_WIDGET_INT_READ_ONLY("serverBD.iTelemInnocentsKilled", serverBD.iTelemInnocentsKilled)
//			ADD_WIDGET_INT_READ_ONLY("serverBD.iTelemAttackType", serverBD.iTelemAttackType)
//		STOP_WIDGET_GROUP()
//
		START_WIDGET_GROUP("Misc Debug")		
			START_WIDGET_GROUP("Float tweaks")
				ADD_WIDGET_FLOAT_SLIDER("FloatX", TweakX, -100.00, 100.00, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("FloatY", TweakY, -100.00, 100.00, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("FloatZ", TweakZ, -100.00, 100.00, 0.001)
			STOP_WIDGET_GROUP()
//		
//			START_WIDGET_GROUP("Vars")
//				ADD_WIDGET_INT_READ_ONLY("serverBD.iNumMissionEntitiesCreated", serverBD.iNumMissionEntitiesCreated)
//				ADD_WIDGET_INT_READ_ONLY("serverBD.iActiveSuppportVehCount", serverBD.iActiveSuppportVehCount)
//				ADD_WIDGET_INT_READ_ONLY("iClosestVehicle", iClosestVehicle)
//			STOP_WIDGET_GROUP()
//			
//			START_WIDGET_GROUP("Vectors")
//				ADD_WIDGET_VECTOR_SLIDER("Bullet Box 0", vWidgetGenericVector0, -9999.0, 9999.0, 0.1)
//				ADD_WIDGET_VECTOR_SLIDER("Bullet Box 1", vWidgetGenericVector1, -9999.0, 9999.0, 0.1)
//			STOP_WIDGET_GROUP()
//
//			START_WIDGET_GROUP("Offset Tool")
//				ADD_WIDGET_BOOL("Activate Offset Tool", bActivateTool)
//				ADD_WIDGET_VECTOR_SLIDER("Coords Offset", voffsetToolCoordsOffset, -10000.0, 10000.0, 0.01)
//				ADD_WIDGET_VECTOR_SLIDER("Rotation Offset", voffsetToolRotationOffset, -10000.0, 10000.0, 0.01)
//			STOP_WIDGET_GROUP()
//
//			START_WIDGET_GROUP("Game State")
//				START_WIDGET_GROUP("Server")
//					twIdServerGameState = ADD_TEXT_WIDGET("Game State")
//					twIdModeState = ADD_TEXT_WIDGET("Mode State")
//					UPDATE_SERVER_GAME_STATE_WIDGET()
//				STOP_WIDGET_GROUP()
//			STOP_WIDGET_GROUP()
//			
//			ADD_WIDGET_BOOL("Terminate Script Locally", bTerminateScriptNow)
//			ADD_WIDGET_BOOL("Flip Mission Entity Onto Roof", bFlipMissionEntityVehicle)
//
//			START_WIDGET_GROUP("Marker Testing")
//				ADD_WIDGET_BOOL("Draw Marker above local player", bMarkerTestingDrawMarker)
//				ADD_WIDGET_FLOAT_SLIDER("Direction X", dirX, 0.0, 360.0, 1.0)
//				ADD_WIDGET_FLOAT_SLIDER("Direction Y", dirY, 0.0, 360.0, 1.0)
//				ADD_WIDGET_FLOAT_SLIDER("Direction Z", dirZ, 0.0, 360.0, 1.0)
//				ADD_WIDGET_FLOAT_SLIDER("Rotation X", rotX, 0.0, 360.0, 1.0)
//				ADD_WIDGET_FLOAT_SLIDER("Rotation Y", rotY, 0.0, 360.0, 1.0)
//				ADD_WIDGET_FLOAT_SLIDER("Rotation Z", rotZ, 0.0, 360.0, 1.0)
//			STOP_WIDGET_GROUP()
//						
//			START_WIDGET_GROUP("Vehicle Speed Testing")
//				ADD_WIDGET_BOOL("Enable", bDebugSpeed)
//				ADD_WIDGET_FLOAT_SLIDER("Speed", fDebugSpeed, 5.0, 200.0, 1.0)
//			STOP_WIDGET_GROUP()
//			
//			ADD_WIDGET_BOOL("Force Host Change",bForceHostChange)
//			
		STOP_WIDGET_GROUP()
//		
		#IF SCRIPT_PROFILER_ACTIVE
		CREATE_SCRIPT_PROFILER_WIDGET()
		#ENDIF 
//		
		IF logic.Debug.Widgets.Create != NULL
			CALL logic.Debug.Widgets.Create()
		ENDIF
	STOP_WIDGET_GROUP()
	
	PRINTLN("[",scriptName,"] - created widgets.")
	
	SET_DEBUG_LOCAL_BIT(eDEBUGLOCALBITSET_CREATED_WIDGETS)
ENDPROC		

FUNC HUD_COLOURS GET_DEBUG_TEXT_ALIVE_HUD_COLOUR()
	RETURN HUD_COLOUR_WHITE
ENDFUNC

FUNC HUD_COLOURS GET_DEBUG_TEXT_TRIGGERED_HUD_COLOUR()
	RETURN HUD_COLOUR_GREEN
ENDFUNC

FUNC HUD_COLOURS GET_DEBUG_TEXT_DEAD_HUD_COLOUR()
	RETURN HUD_COLOUR_BLACK
ENDFUNC

#IF MAX_NUM_TAGS
FUNC INT GET_TAG_FOR_ENTITY(eTAG_TYPE eType, INT iEntity)
	INT iTag
	REPEAT data.Tags.iCount iTag
		IF DEBUG_GET_TAG_TYPE(iTag) = eType
		AND DEBUG_GET_TAG_ID(iTag) = iEntity
			RETURN iTag
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC
#ENDIF

PROC UPDATE_WIDGETS()

//	UPDATE_SERVER_GAME_STATE_WIDGET()
//		
//	IF NOT IS_LOCAL_DEBUG_BIT0_SET(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
//		IF bTerminateScriptNow
//			PRINTLN("[CH_PREP] widget bTerminateScriptNow = TRUE.")
//			SET_LOCAL_DEBUG_BIT0(eLOCALDEBUGBITSET0_TERMINATE_SCRIPT_NOW)
//		ENDIF
//	ENDIF
//	
	IF IS_DEBUG_KEY_JUST_RELEASED(KEY_R, KEYBOARD_MODIFIER_CTRL, "Draw mission entity array indexes on screen")
		IF sDebugVars.bShowEntityArrayIndexes
			sDebugVars.bShowEntityArrayIndexes = FALSE
		ELSE
			sDebugVars.bShowEntityArrayIndexes = TRUE
		ENDIF
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_SPACE, KEYBOARD_MODIFIER_NONE, "Draw mission entity array indexes on screen for F9")
	AND NOT sDebugVars.bShowEntityArrayIndexes
		sDebugVars.bShowEntityArrayIndexes = TRUE
		IF g_debugDisplayState != DEBUG_DISP_CHK
			g_debugDisplayState = DEBUG_DISP_CHK
			sDebugVars.bShowCtrlShiftDForF9 = TRUE
		ENDIF
		START_NET_TIMER(sDebugVars.entityIndexDebugTimer)
	ENDIF

	IF HAS_NET_TIMER_STARTED(sDebugVars.entityIndexDebugTimer)
	AND HAS_NET_TIMER_EXPIRED(sDebugVars.entityIndexDebugTimer, 2000)
		sDebugVars.bShowEntityArrayIndexes = FALSE
		IF sDebugVars.bShowCtrlShiftDForF9
		AND g_debugDisplayState = DEBUG_DISP_CHK
			g_debugDisplayState = DEBUG_DISP_OFF
		ENDIF
		RESET_NET_TIMER(sDebugVars.entityIndexDebugTimer)
	ENDIF
	
	#IF MAX_NUM_DIALOGUE
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_EnableSkipCurrentDialogue")
	AND IS_DEBUG_KEY_JUST_RELEASED(KEY_RIGHT, KEYBOARD_MODIFIER_SHIFT, "Skip current dialogue")
		sDebugVars.Dialogue.bSkipCurrent = NOT sDebugVars.Dialogue.bSkipCurrent
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [DIALOGUE] - UPDATE_WIDGETS - sDebugVars.Dialogue.bSkipCurrent = ", GET_STRING_FROM_BOOL(sDebugVars.Dialogue.bSkipCurrent))
	ENDIF
	#ENDIF
	
	#IF MAX_NUM_TRIGGER_AREAS
	IF sDebugVars.TriggerArea.iSelected != sDebugVars.TriggerArea.iPrevious
		IF sDebugVars.TriggerArea.iSelected = -1
			sDebugVars.TriggerArea.vMin = <<0.0, 0.0, 0.0>>
			sDebugVars.TriggerArea.vMax = <<0.0, 0.0, 0.0>>
			sDebugVars.TriggerArea.fWidth = 0.0
		ELSE
			MISSION_PLACEMENT_DATA_TRIGGER_AREA triggerArea = GET_TRIGGER_AREA_DATASET(sDebugVars.TriggerArea.iSelected)
			sDebugVars.TriggerArea.vMin = triggerArea.vMin
			sDebugVars.TriggerArea.vMax = triggerArea.vMax
			sDebugVars.TriggerArea.fWidth = triggerArea.fWidth
		ENDIF
		
		sDebugVars.TriggerArea.iPrevious = sDebugVars.TriggerArea.iSelected
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] UPDATE_WIDGETS - Updated values to selected trigger area: ", sDebugVars.TriggerArea.iSelected)
	ENDIF
	#ENDIF
	
//	
//	IF iWidgetSeeingRangePed != (-1)
//		IF iWidgetSeeingRangePed != iWidgetLastSeeingRangePed
//			iWidgetLastSeeingRangePed = iWidgetSeeingRangePed
//			fWidgetPedSeeingRange = 0.0
//			fWidgetLastPedSeeingRange = 0.0
//		ENDIF
//		
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iWidgetSeeingRangePed].netId)
//			PED_INDEX pedId = NET_TO_PED(serverBD.sPed[iWidgetSeeingRangePed].netId)
//			IF NOT IS_ENTITY_DEAD(pedId)
//				IF fWidgetPedSeeingRange != fWidgetLastPedSeeingRange
//					SET_PED_SEEING_RANGE(pedId, fWidgetPedSeeingRange)
//					fWidgetLastPedSeeingRange = fWidgetPedSeeingRange
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF	
//	
//	IF bFlipMissionEntityVehicle
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntities.netId[MISSION_ENTITY_ONE])
//			VEHICLE_INDEX vehId = NET_TO_VEH(serverBD.sMissionEntities.netId[MISSION_ENTITY_ONE])
//			IF IS_VEHICLE_DRIVEABLE(vehId)
//				IF TAKE_CONTROL_OF_NET_ID(serverBD.sMissionEntities.netId[MISSION_ENTITY_ONE])
//					VECTOR vEntityRotation = GET_ENTITY_ROTATION(vehId)
//					vEntityRotation.Y = 180.0
//					SET_ENTITY_ROTATION(vehId, vEntityRotation)
//					bFlipMissionEntityVehicle = FALSE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
	IF sDebugVars.PropPlacement.Attachment.iProp != -1
	AND sDebugVars.PropPlacement.Attachment.iProp < data.Prop.iCount
		IF sDebugVars.PropPlacement.Attachment.iPropCached != sDebugVars.PropPlacement.Attachment.iProp
			sDebugVars.PropPlacement.Attachment.iPropCached = sDebugVars.PropPlacement.Attachment.iProp
			sDebugVars.PropPlacement.Attachment.entFocusEntityCached = NULL
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.Attachment.twFocusEntity, "")
			sDebugVars.PropPlacement.Attachment.vOffset = <<0.0, 0.0, 0.0>>
			sDebugVars.PropPlacement.Attachment.vRotation = <<0.0, 0.0, 0.0>>
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.Attachment.twEntityBoneName, "")
			sDebugVars.PropPlacement.Attachment.iPedBoneTag = 0
			sDebugVars.PropPlacement.Attachment.iBoneIndexCached = 0
			sDebugVars.PropPlacement.Attachment.bToggleAttach = FALSE
		ENDIF
		
		ENTITY_INDEX entParentID = GET_FOCUS_ENTITY_INDEX()
		
		IF TAKE_CONTROL_OF_ENTITY(entParentID)
		AND TAKE_CONTROL_OF_NET_ID(serverBD.sProp[sDebugVars.PropPlacement.Attachment.iProp].netID)
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.Attachment.twFocusEntity, GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(entParentID)))
			
			INT iBoneIndex
			
			IF IS_ENTITY_ALIVE(entParentID)
			ENDIF
			
			IF IS_ENTITY_A_PED(entParentID)
				iBoneIndex = GET_PED_BONE_INDEX(GET_PED_INDEX_FROM_ENTITY_INDEX(entParentID), GET_PROP_PLACEMENT_ATTACHMENT_PED_BONETAG(sDebugVars.PropPlacement.Attachment.iPedBoneTag))
			ELIF NOT IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.Attachment.twEntityBoneName))
				iBoneIndex = GET_ENTITY_BONE_INDEX_BY_NAME(entParentID, GET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.Attachment.twEntityBoneName))
			ENDIF
			
			ENTITY_INDEX entChildID = NET_TO_ENT(serverBD.sProp[sDebugVars.PropPlacement.Attachment.iProp].netID)
			
			IF NOT IS_ENTITY_ATTACHED(entChildID)
				IF sDebugVars.PropPlacement.Attachment.bToggleAttach
					sDebugVars.PropPlacement.Attachment.bToggleAttach = FALSE
					
					ATTACH_ENTITY_TO_ENTITY(entChildID, entParentID, iBoneIndex, sDebugVars.PropPlacement.Attachment.vOffset, sDebugVars.PropPlacement.Attachment.vRotation)
					
					sDebugVars.PropPlacement.Attachment.entFocusEntityCached = entParentID
					sDebugVars.PropPlacement.Attachment.iBoneIndexCached = iBoneIndex
					sDebugVars.PropPlacement.Attachment.vOffsetCached = sDebugVars.PropPlacement.Attachment.vOffset
					sDebugVars.PropPlacement.Attachment.vRotationCached = sDebugVars.PropPlacement.Attachment.vRotation
				ENDIF
			ELSE
				IF sDebugVars.PropPlacement.Attachment.bToggleAttach
					sDebugVars.PropPlacement.Attachment.bToggleAttach = FALSE
					
					DETACH_ENTITY(entChildID)
				ELSE
					IF sDebugVars.PropPlacement.Attachment.entFocusEntityCached != entParentID
					OR sDebugVars.PropPlacement.Attachment.iBoneIndexCached != iBoneIndex
					OR NOT ARE_VECTORS_EQUAL(sDebugVars.PropPlacement.Attachment.vOffsetCached, sDebugVars.PropPlacement.Attachment.vOffset)
					OR NOT ARE_VECTORS_EQUAL(sDebugVars.PropPlacement.Attachment.vRotationCached, sDebugVars.PropPlacement.Attachment.vRotation)
						DETACH_ENTITY(entChildID)
						
						ATTACH_ENTITY_TO_ENTITY(entChildID, entParentID, iBoneIndex, sDebugVars.PropPlacement.Attachment.vOffset, sDebugVars.PropPlacement.Attachment.vRotation)
						
						sDebugVars.PropPlacement.Attachment.entFocusEntityCached = entParentID
						sDebugVars.PropPlacement.Attachment.iBoneIndexCached = iBoneIndex
						sDebugVars.PropPlacement.Attachment.vOffsetCached = sDebugVars.PropPlacement.Attachment.vOffset
						sDebugVars.PropPlacement.Attachment.vRotationCached = sDebugVars.PropPlacement.Attachment.vRotation
					ENDIF
				ENDIF
			ENDIF
			
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.Attachment.twOffset, GET_STRING_FROM_VECTOR(sDebugVars.PropPlacement.Attachment.vOffset))
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.Attachment.twRotation, GET_STRING_FROM_VECTOR(sDebugVars.PropPlacement.Attachment.vRotation))
		ENDIF
	ENDIF
	
	IF sDebugVars.PropPlacement.iProp != -1
		IF sDebugVars.PropPlacement.iProp != sDebugVars.PropPlacement.iLastProp
			sDebugVars.PropPlacement.iLastProp = sDebugVars.PropPlacement.iProp
			sDebugVars.PropPlacement.iIsAttached = 0
			sDebugVars.PropPlacement.iHasCollision = 0
			sDebugVars.PropPlacement.iIsVisible = 0
			sDebugVars.PropPlacement.bDetach = FALSE
			sDebugVars.PropPlacement.bToggleCollision = FALSE
			sDebugVars.PropPlacement.bToggleVisibility = FALSE
			sDebugVars.PropPlacement.vPosition = <<0.0, 0.0, 0.0>>
			sDebugVars.PropPlacement.vRotation = <<0.0, 0.0, 0.0>>
			sDebugVars.PropPlacement.bUseFocusEntityOffset = FALSE
			sDebugVars.PropPlacement.vLocalPositionOffset = <<0.0, 0.0, 0.0>>
			sDebugVars.PropPlacement.vLocalRotationOffset = <<0.0, 0.0, 0.0>>
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.twLocalPositionOffset, "")
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.twLocalRotationOffset, "")
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.twFinalPosition, "")
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.twFinalRotation, "")
		ENDIF
		
		NETWORK_INDEX netId = serverBD.sProp[sDebugVars.PropPlacement.iProp].netID
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(netId)
		AND NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(netId)
			ENTITY_INDEX entId = NET_TO_ENT(netId)
			
			sDebugVars.PropPlacement.iIsAttached = BOOL_TO_INT(IS_ENTITY_ATTACHED(entId))
			sDebugVars.PropPlacement.iHasCollision = BOOL_TO_INT(NOT GET_ENTITY_COLLISION_DISABLED(entId))
			sDebugVars.PropPlacement.iIsVisible = BOOL_TO_INT(IS_ENTITY_VISIBLE(entId))
			
			IF sDebugVars.PropPlacement.bDetach
				sDebugVars.PropPlacement.bDetach = FALSE
				
				IF IS_ENTITY_ATTACHED(entId)
					DETACH_ENTITY(entId)
				ENDIF
			ENDIF
			
			IF sDebugVars.PropPlacement.bToggleCollision
				sDebugVars.PropPlacement.bToggleCollision = FALSE
				
				SET_ENTITY_COLLISION(entId, GET_ENTITY_COLLISION_DISABLED(entId))
			ENDIF
			
			IF sDebugVars.PropPlacement.bToggleVisibility
				sDebugVars.PropPlacement.bToggleVisibility = FALSE
				
				SET_ENTITY_VISIBLE(entId, NOT IS_ENTITY_VISIBLE(entId))
			ENDIF
			
			IF IS_VECTOR_ZERO(sDebugVars.PropPlacement.vPosition)
				sDebugVars.PropPlacement.vPosition = GET_ENTITY_COORDS(entId, FALSE)
			ELSE
				IF IS_ENTITY_ATTACHED(entId)
					sDebugVars.PropPlacement.vPosition = GET_ENTITY_COORDS(entId, FALSE)
				ELSE
					SET_ENTITY_COORDS(entId, sDebugVars.PropPlacement.vPosition, FALSE)
				ENDIF
			ENDIF
			
			IF IS_VECTOR_ZERO(sDebugVars.PropPlacement.vRotation)
				sDebugVars.PropPlacement.vRotation = GET_ENTITY_ROTATION(entId)
			ELSE
				IF IS_ENTITY_ATTACHED(entId)
					sDebugVars.PropPlacement.vRotation = GET_ENTITY_ROTATION(entId)
				ELSE
					SET_ENTITY_ROTATION(entId, sDebugVars.PropPlacement.vRotation, DEFAULT, FALSE)
				ENDIF
			ENDIF
			
			IF IS_ENTITY_ATTACHED(entId)
				IF NOT IS_VECTOR_ZERO(sDebugVars.PropPlacement.vLocalPositionOffset)
					sDebugVars.PropPlacement.vLocalPositionOffset = <<0.0, 0.0, 0.0>>
				ENDIF
				
				IF NOT IS_VECTOR_ZERO(sDebugVars.PropPlacement.vLocalRotationOffset)
					sDebugVars.PropPlacement.vLocalRotationOffset = <<0.0, 0.0, 0.0>>
				ENDIF
			ELSE
				IF sDebugVars.PropPlacement.bUseFocusEntityOffset
					ENTITY_INDEX entFocusEntityID = GET_FOCUS_ENTITY_INDEX()
					
					IF IS_ENTITY_ALIVE(entFocusEntityID)
						SET_ENTITY_COORDS(entId, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entFocusEntityID, sDebugVars.PropPlacement.vLocalPositionOffset), FALSE)
						
						VECTOR vFocusEntityRotation = GET_ENTITY_ROTATION(entFocusEntityID)
						SET_ENTITY_ROTATION(entId, vFocusEntityRotation + sDebugVars.PropPlacement.vLocalRotationOffset, DEFAULT, FALSE)
					ENDIF
				ELSE
					SET_ENTITY_COORDS(entId, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entId, sDebugVars.PropPlacement.vLocalPositionOffset), FALSE)
					
					VECTOR vEntityRotation = GET_ENTITY_ROTATION(entId)
					SET_ENTITY_ROTATION(entId, vEntityRotation + sDebugVars.PropPlacement.vLocalRotationOffset, DEFAULT, FALSE)
				ENDIF
			ENDIF
			
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.twLocalPositionOffset, GET_STRING_FROM_VECTOR(sDebugVars.PropPlacement.vLocalPositionOffset))
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.twLocalRotationOffset, GET_STRING_FROM_VECTOR(sDebugVars.PropPlacement.vLocalRotationOffset))
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.twFinalPosition, GET_STRING_FROM_VECTOR(GET_ENTITY_COORDS(entId)))
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.PropPlacement.twFinalRotation, GET_STRING_FROM_VECTOR(GET_ENTITY_ROTATION(entId)))
		ENDIF
	ENDIF
//	IF iEntityMissionPlacementIndex != (-1)
//	
//		IF iEntityMissionPlacementIndex != iLastEntityMissionPlacementIndex
//			iLastEntityMissionPlacementIndex = iEntityMissionPlacementIndex
//			vEntityMissionPlacementPosition = <<0.0, 0.0, 0.0>>
//			vEntityMissionPlacementRotation = <<0.0, 0.0, 0.0>>
//			vEntityMissionPlacementLocalOffset = <<0.0, 0.0, 0.0>>
//			SET_CONTENTS_OF_TEXT_WIDGET(twEntityMissionFinalPosition, "")
//		ENDIF
//		
//		NETWORK_INDEX entityMissionNetId 
//		IF iEntityMissionPlacementIndex = (-2)
////			entityMissionNetId = serverBD.niFindItemPortablePickup
//		ELSE
//			entityMissionNetId = serverBD.sMissionEntities.netId[iEntityMissionPlacementIndex]
//		ENDIF
//		
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(entityMissionNetId)
//			ENTITY_INDEX entityMissionId = NET_TO_ENT(entityMissionNetId)
//			IF NOT IS_ENTITY_DEAD(entityMissionId)
//				IF IS_VECTOR_ZERO(vEntityMissionPlacementPosition)
//					vEntityMissionPlacementPosition = GET_ENTITY_COORDS(entityMissionId)
//				ELSE
//					SET_ENTITY_COORDS(entityMissionId, vEntityMissionPlacementPosition)
//				ENDIF
//				IF IS_VECTOR_ZERO(vEntityMissionPlacementRotation)
//					vEntityMissionPlacementRotation = GET_ENTITY_ROTATION(entityMissionId)
//				ELSE
//					SET_ENTITY_ROTATION(entityMissionId, vEntityMissionPlacementRotation)
//				ENDIF
//				IF NOT IS_VECTOR_ZERO(vEntityMissionPlacementLocalOffset)
//					SET_ENTITY_COORDS(entityMissionId, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entityMissionId, vEntityMissionPlacementLocalOffset))
//				ENDIF
//			ENDIF
//			
//			SET_CONTENTS_OF_TEXT_WIDGET(twEntityMissionFinalPosition, GET_STRING_FROM_VECTOR(GET_ENTITY_COORDS(entityMissionId)))
//		ENDIF
//	ENDIF
//	IF iPedPlacementIndex != (-1)
//	
//		IF iPedPlacementIndex != iLastPedPlacementIndex
//			iLastPedPlacementIndex = iPedPlacementIndex
//			vPedPlacementPosition = <<0.0, 0.0, 0.0>>
//			vPedPlacementRotation = <<0.0, 0.0, 0.0>>
//			vPedPlacementLocalOffset = <<0.0, 0.0, 0.0>>
//			SET_CONTENTS_OF_TEXT_WIDGET(twPedFinalPosition, "")
//		ENDIF
//		
//		NETWORK_INDEX entityMissionNetId 
//		IF iPedPlacementIndex = (-2)
////			entityMissionNetId = serverBD.niFindItemPortablePickup
//		ELSE
//			entityMissionNetId = serverBD.sPed[iPedPlacementIndex].netId
//		ENDIF
//		
//		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(entityMissionNetId)
//			ENTITY_INDEX entityMissionId = NET_TO_ENT(entityMissionNetId)
//			IF NOT IS_ENTITY_DEAD(entityMissionId)
//				IF IS_VECTOR_ZERO(vPedPlacementPosition)
//					vPedPlacementPosition = GET_ENTITY_COORDS(entityMissionId)
//				ELSE
//					SET_ENTITY_COORDS(entityMissionId, vPedPlacementPosition)
//				ENDIF
//				IF IS_VECTOR_ZERO(vPedPlacementRotation)
//					vPedPlacementRotation = GET_ENTITY_ROTATION(entityMissionId)
//				ELSE
//					SET_ENTITY_ROTATION(entityMissionId, vPedPlacementRotation)
//				ENDIF
//				IF NOT IS_VECTOR_ZERO(vPedPlacementLocalOffset)
//					SET_ENTITY_COORDS(entityMissionId, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(entityMissionId, vPedPlacementLocalOffset))
//				ENDIF
//			ENDIF
//			
//			SET_CONTENTS_OF_TEXT_WIDGET(twPedFinalPosition, GET_STRING_FROM_VECTOR(GET_ENTITY_COORDS(entityMissionId)))
//		ENDIF
//	ENDIF
//	
//	IF bDrawDebugBlip0
//		VECTOR vCoord = GET_DEBUG_BLIP_COORD(0)
//		IF NOT IS_VECTOR_ZERO(vCoord)
//			IF NOT DOES_BLIP_EXIST(blipWidgetDebugBlip0)
//				blipWidgetDebugBlip0 = ADD_BLIP_FOR_COORD(vCoord)
//				SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipWidgetDebugBlip0, HUD_COLOUR_BLACK)
//				SET_BLIP_NAME_FROM_TEXT_FILE(blipWidgetDebugBlip0, "DM_MISC")
//			ELSE
//				IF NOT ARE_VECTORS_EQUAL(vCoord, GET_BLIP_COORDS(blipWidgetDebugBlip0))
//					SET_BLIP_COORDS(blipWidgetDebugBlip0, vCoord)
//				ENDIF
//			ENDIF
//		ELSE
//			bDrawDebugBlip0 = FALSE
//		ENDIF
//	ELSE
//		IF DOES_BLIP_EXIST(blipWidgetDebugBlip0)
//			REMOVE_BLIP(blipWidgetDebugBlip0)
//		ENDIF 
//	ENDIF
//	
//	IF bDrawDebugBlip1
//		VECTOR vCoord = GET_DEBUG_BLIP_COORD(1)
//		IF NOT IS_VECTOR_ZERO(vCoord)
//			IF NOT DOES_BLIP_EXIST(blipWidgetDebugBlip1)
//				blipWidgetDebugBlip1 = ADD_BLIP_FOR_COORD(vCoord)
//				SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipWidgetDebugBlip1, HUD_COLOUR_BLACK)
//				SET_BLIP_NAME_FROM_TEXT_FILE(blipWidgetDebugBlip1, "DM_MISC")
//			ENDIF
//		ELSE
//			bDrawDebugBlip1 = FALSE
//		ENDIF
//	ELSE
//		IF DOES_BLIP_EXIST(blipWidgetDebugBlip1)
//			REMOVE_BLIP(blipWidgetDebugBlip1)
//		ENDIF 
//	ENDIF
//	
//	IF bDrawDebugBlip2
//		VECTOR vCoord = GET_DEBUG_BLIP_COORD(2)
//		IF NOT IS_VECTOR_ZERO(vCoord)
//			IF NOT DOES_BLIP_EXIST(blipWidgetDebugBlip2)
//				blipWidgetDebugBlip2 = ADD_BLIP_FOR_COORD(vCoord)
//				SET_BLIP_COLOUR_FROM_HUD_COLOUR(blipWidgetDebugBlip2, HUD_COLOUR_BLACK)
//				SET_BLIP_NAME_FROM_TEXT_FILE(blipWidgetDebugBlip2, "DM_MISC")
//			ELSE
//				SET_BLIP_COORDS(blipWidgetDebugBlip2, vCoord)
//			ENDIF
//		ELSE
//			bDrawDebugBlip2 = FALSE
//		ENDIF
//	ELSE
//		IF DOES_BLIP_EXIST(blipWidgetDebugBlip2)
//			REMOVE_BLIP(blipWidgetDebugBlip2)
//		ENDIF 
//	ENDIF
//		
	IF sDebugVars.bShowEntityArrayIndexes
		INT iEntity
		TEXT_LABEL_63 tl63Temp
		HUD_COLOURS eEntityStateColour
		
		#IF MAX_NUM_TAGS
		INT iTag
		TEXT_LABEL_15 tl15Tag
		#ENDIF

		REPEAT data.MissionEntity.iCount iEntity
			tl63Temp = " Mission Entity #"
			tl63Temp += iEntity
			tl63Temp += " "
			
			IF IS_MISSION_ENTITY_BIT_SET(iEntity, eMISSIONENTITYBITSET_I_AM_IN_CARRIER)
				tl63Temp += "[C]"
			ENDIF
			IF sMissionEntityLocal.sMissionEntity[iEntity].holderPlayerId != INVALID_PLAYER_INDEX()
				tl63Temp += "["
				tl63Temp += GET_PLAYER_NAME(sMissionEntityLocal.sMissionEntity[iEntity].holderPlayerId)
				tl63Temp += "]"
			ENDIF
			
			#IF MAX_NUM_TAGS
			iTag = GET_TAG_FOR_ENTITY(eTAGTYPE_MISSION_ENTITY, iEntity)
			IF iTag != (-1)
				tl63Temp += " - "
				tl15Tag = DEBUG_GET_TAG_NAME(iTag)
				tl63Temp += tl15Tag
			ENDIF
			#ENDIF
			
			eEntityStateColour = GET_DEBUG_TEXT_ALIVE_HUD_COLOUR()

			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sMissionEntities.netId[iEntity])
				DRAW_DEBUG_TEXT_ABOVE_NETID_COLOUR(serverBD.sMissionEntities.netId[iEntity], tl63Temp, FALSE, 0.2, eEntityStateColour)
			ENDIF
		ENDREPEAT
		
		REPEAT data.Ped.iCount iEntity

			tl63Temp = " Ped #"
			tl63Temp += iEntity
			tl63Temp += " "
			
			tl63Temp += "[G"
			tl63Temp += data.Ped.Peds[iEntity].iGroup
			tl63Temp += "]"
			
			IF IS_PED_BIT_SET(iEntity, ePEDBITSET_INSIDE_MISSION_INTERIOR)
				tl63Temp += "[I]"
			ENDIF
			IF IS_PED_BIT_SET(iEntity, ePEDBITSET_AMBUSH_PED)
				tl63Temp += "[A]"
			ENDIF
			IF IS_PED_BIT_SET(iEntity, ePEDBITSET_PATROL_PED)
				tl63Temp += "[P]"
			ENDIF
			IF IS_PED_BIT_SET(iEntity, ePEDBITSET_TARGET_PED)
				tl63Temp += "[T]"
			ENDIF
			IF IS_PED_BIT_SET(iEntity, ePEDBITSET_RESPAWNED)
				tl63Temp += "[R]"
			ENDIF
			
			#IF MAX_NUM_TAGS
			iTag = GET_TAG_FOR_ENTITY(eTAGTYPE_PED, iEntity)
			IF iTag != (-1)
				tl63Temp += " - "
				tl15Tag = DEBUG_GET_TAG_NAME(iTag)
				tl63Temp += tl15Tag
			ENDIF
			#ENDIF
	
			tl63Temp += " - "
			tl63Temp += GET_PED_TASK_NAME(GET_PED_TASK(iEntity))
			tl63Temp += " "
			
			IF GET_PED_STATE(iEntity) > ePEDSTATE_ACTIVE
				eEntityStateColour = GET_DEBUG_TEXT_DEAD_HUD_COLOUR()
			ELIF HAS_PED_BEEN_TRIGGERED(iEntity, TRUE)
				eEntityStateColour = GET_DEBUG_TEXT_TRIGGERED_HUD_COLOUR()
			ELSE
				eEntityStateColour = GET_DEBUG_TEXT_ALIVE_HUD_COLOUR()
			ENDIF
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPed[iEntity].netId)
				DRAW_DEBUG_TEXT_ABOVE_NETID_COLOUR(serverBD.sPed[iEntity].netId, tl63Temp, TRUE, DEFAULT, eEntityStateColour)
			ENDIF
		ENDREPEAT
		
		REPEAT data.Vehicle.iCount iEntity
			tl63Temp = " Vehicle #"
			tl63Temp += iEntity
			tl63Temp += " "
			
			IF IS_VEHICLE_BIT_SET(iEntity, eVEHICLEBITSET_INSIDE_MISSION_INTERIOR)
				tl63Temp += "[I]"
			ENDIF
			IF IS_VEHICLE_BIT_SET(iEntity, eVEHICLEBITSET_AMBUSH_VEHICLE)
				tl63Temp += "[A]"
			ENDIF
			IF IS_VEHICLE_BIT_SET(iEntity, eVEHICLEBITSET_I_AM_CARRIER_VEHICLE)
				tl63Temp += "[C]"
			ENDIF
			IF IS_VEHICLE_BIT_SET(iEntity, eVEHICLEBITSET_TARGET_VEHICLE)
				tl63Temp += "[T]"
			ENDIF
			
			#IF MAX_NUM_TAGS
			iTag = GET_TAG_FOR_ENTITY(eTAGTYPE_VEHICLE, iEntity)
			IF iTag != (-1)
				tl63Temp += " - "
				tl15Tag = DEBUG_GET_TAG_NAME(iTag)
				tl63Temp += tl15Tag
			ENDIF
			#ENDIF
			
			IF GET_VEHICLE_STATE(iEntity) > eVEHICLESTATE_DRIVEABLE
				eEntityStateColour = GET_DEBUG_TEXT_DEAD_HUD_COLOUR()
			ELSE
				eEntityStateColour = GET_DEBUG_TEXT_ALIVE_HUD_COLOUR()
			ENDIF
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[iEntity].netId)
				DRAW_DEBUG_TEXT_ABOVE_NETID_COLOUR(serverBD.sVehicle[iEntity].netId, tl63Temp, FALSE, DEFAULT, eEntityStateColour)
			ENDIF
		ENDREPEAT
		
		REPEAT data.Prop.iCount iEntity
				
			tl63Temp = " Prop #"
			tl63Temp += iEntity
			tl63Temp += " "
			
			IF IS_PROP_BIT_SET(iEntity, ePROPBITSET_INSIDE_MISSION_INTERIOR)
				tl63Temp += "[I]"
			ENDIF
			IF IS_PROP_BIT_SET(iEntity, ePROPBITSET_TARGET_PROP)
				tl63Temp += "[T]"
			ENDIF
			
			#IF MAX_NUM_TAGS
			iTag = GET_TAG_FOR_ENTITY(eTAGTYPE_PROP, iEntity)
			IF iTag != (-1)
				tl63Temp += " - "
				tl15Tag = DEBUG_GET_TAG_NAME(iTag)
				tl63Temp += tl15Tag
			ENDIF
			#ENDIF
		
			IF GET_PROP_STATE(iEntity) > ePROPSTATE_IDLE
				eEntityStateColour = GET_DEBUG_TEXT_DEAD_HUD_COLOUR()
			ELSE
				eEntityStateColour = GET_DEBUG_TEXT_ALIVE_HUD_COLOUR()
			ENDIF
		
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sProp[iEntity].netID)
				DRAW_DEBUG_TEXT_ABOVE_NETID_COLOUR(serverBD.sProp[iEntity].netID, tl63Temp, FALSE, DEFAULT, eEntityStateColour)
			ENDIF
		ENDREPEAT
		
		#IF MAX_NUM_MISSION_PORTALS
		REPEAT MAX_NUM_MISSION_PORTALS iEntity
			IF IS_VECTOR_ZERO(data.Portal[iEntity].vCoord)
				RELOOP
			ENDIF
			
			tl63Temp = " Portal #"
			tl63Temp += iEntity
			tl63Temp += " "
			
			IF IS_PORTAL_DATA_BIT_SET(iEntity, ePORTALDATABITSET_INSIDE_INTERIOR)
				tl63Temp += "[I]"
			ENDIF
	
			DRAW_DEUG_TEXT_ABOVE_COORD_COLOUR(data.Portal[iEntity].vCoord, tl63Temp, 1.0)
		ENDREPEAT
		
		#IF MAX_NUM_PICKUPS
		REPEAT data.Pickup.iCount iEntity
			tl63Temp = " Pickup #"
			tl63Temp += iEntity
			tl63Temp += " "
			
			IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPickup.sPickup[iEntity].netID)
				DRAW_DEBUG_TEXT_ABOVE_NETID_COLOUR(serverBD.sPickup.sPickup[iEntity].netID, tl63Temp, FALSE, 1.0)
			ENDIF
		ENDREPEAT
		#ENDIF
		
		#IF MAX_NUM_CHECKPOINTS
		REPEAT data.Checkpoint.iCount iEntity
			tl63Temp = " Checkpoint #"
			tl63Temp += iEntity
			tl63Temp += " "
			
			DRAW_DEUG_TEXT_ABOVE_COORD_COLOUR(data.Checkpoint.Checkpoints[iEntity].vCoords, tl63Temp, 1.0)
		ENDREPEAT		
		#ENDIF
		
	ENDIF
//	
//	IF bMarkerTestingDrawMarker
//		INT iR, iG, iB, iA
//		GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iR, iG, iB, iA)
//		iA = 100
//		//DRAW_MARKER(MARKER_RING, LOCAL_PLAYER_COORD+<<0.0,0.0,10.0>>, <<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>, <<20.0, 20.0, 20.0>>, iR, iG, iB, iA, FALSE, TRUE)
//		DRAW_MARKER(MARKER_LINES, LOCAL_PLAYER_COORD+<<0.0,0.0,10.0>>, <<dirX,dirY,dirZ>>, <<rotX,rotY,rotZ>>, <<10.0, 10.0, 10.0>>, iR, iG, iB, iA, FALSE, TRUE)
//	ENDIF
//	
//	IF bDebugSpeed
//		IF IS_LOCAL_ALIVE
//		AND IS_PED_IN_ANY_VEHICLE(LOCAL_PED_INDEX)
//			VEHICLE_INDEX myVeh = GET_VEHICLE_PED_IS_IN(LOCAL_PED_INDEX)
//			
//			IF DOES_ENTITY_EXIST(myVeh)
//				SET_VEHICLE_MAX_SPEED(myVeh, fDebugSpeed)
//			ENDIF
//		ELSE
//			bDebugSpeed = FALSE
//		ENDIF
//	ENDIF
//	
//	IF bForceHostChange
//		NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT() 
//		bForceHostChange = FALSE
//		PRINTLN("[CH_PREP] NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT - Forced host change.")
//	ENDIF
//
	IF NOT IS_DEBUG_LOCAL_BIT_SET(eDEBUGLOCALBITSET_ENABLED_DEBUG_LINES_AND_SPHERES)
		IF sDebugVars.bShowEntityArrayIndexes
		OR sDebugVars.bDrawVehicleGoToPointCheckpointIDs
//		OR bDrawPatrolCheckpointIDs
//		OR bDebugShowVehicleLockDebug
		#IF MAX_NUM_COLLISION_TESTS
		OR sDebugVars.CollisionTests.bEnable
		OR sDebugVars.CollisionTests.DrawBox.bEnable
		#ENDIF
		#IF MAX_NUM_SECURITY_CAMERAS
		OR sDebugVars.SecurityCamera.bDrawDebug
		#ENDIF
		#IF MAX_NUM_TAKE_PHOTOGRAPHS
		OR sDebugVars.TrackedPoints.bRenderTrackedPoints
		#ENDIF
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			SET_DEBUG_LOCAL_BIT(eDEBUGLOCALBITSET_ENABLED_DEBUG_LINES_AND_SPHERES)
		ENDIF
	ENDIF
	
	#IF MAX_NUM_TAKE_PHOTOGRAPHS
	
	IF sDebugVars.TrackedPoints.bRenderTrackedPoints
		INT iPoint
		REPEAT data.TakePhotos.Photos.iPhotoCount iPoint
			IF sTakePhotos.TrackedPointId[iPoint] != 0 
				DRAW_DEBUG_SPHERE(data.TakePhotos.Photos.TrackedPoints[iPoint].vCoord, data.TakePhotos.Photos.TrackedPoints[iPoint].fRadius, DEFAULT, DEFAULT, DEFAULT, 100) 
			ENDIF
		ENDREPEAT
	ENDIF
	#ENDIF
	
	#IF MAX_NUM_DIALOGUE
	IF sDebugVars.Dialogue.bSkipCurrent
		TEXT_LABEL_63 tl63Temp = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [DIALOGUE] - UPDATE_WIDGETS - Debug skipping current dialogue. Dialogue root = ", tl63Temp, ", sDialogue.iCurrent = ", sDialogue.iCurrent)
		
		IF IS_MOBILE_PHONE_CALL_ONGOING()
			HANG_UP_AND_PUT_AWAY_PHONE()
		ELSE
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
		
		CLEAR_ALL_BIG_MESSAGES()
		SET_GENERIC_BIT(eGENERICBITSET_SHOWN_JOB_START_BIG_MESSAGE)
		
		sDebugVars.Dialogue.bSkipCurrent = FALSE
	ENDIF
	#ENDIF
	
	IF sDebugVars.bRequestToBeHostOfThisScript
		sDebugVars.bRequestToBeHostOfThisScript = FALSE
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [DIALOGUE] - UPDATE_WIDGETS - Calling NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT.")
		
		NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
	ENDIF
	
	#IF MAX_NUM_CHECKPOINTS
		TEXT_LABEL_63 tlFormatetOutput
		INT iCheckpoint
		REPEAT data.Checkpoint.iCount iCheckpoint
			IF sDebugVars.Checkpoints.sDebugData[iCheckpoint].vDebugCoord.x = 0.0
			OR sDebugVars.Checkpoints.sDebugData[iCheckpoint].vDebugCoord.y = 0.0
			OR sDebugVars.Checkpoints.sDebugData[iCheckpoint].vDebugCoord.z = 0.0
				sDebugVars.Checkpoints.sDebugData[iCheckpoint].vDebugCoord.x = data.Checkpoint.Checkpoints[iCheckpoint].vCoords.x
				sDebugVars.Checkpoints.sDebugData[iCheckpoint].vDebugCoord.y = data.Checkpoint.Checkpoints[iCheckpoint].vCoords.y
				sDebugVars.Checkpoints.sDebugData[iCheckpoint].vDebugCoord.z = data.Checkpoint.Checkpoints[iCheckpoint].vCoords.z
			ENDIF
			IF sDebugVars.Checkpoints.sDebugData[iCheckpoint].vDebugCoord.x != data.Checkpoint.Checkpoints[iCheckpoint].vCoords.x
			OR sDebugVars.Checkpoints.sDebugData[iCheckpoint].vDebugCoord.y != data.Checkpoint.Checkpoints[iCheckpoint].vCoords.y
			OR sDebugVars.Checkpoints.sDebugData[iCheckpoint].vDebugCoord.z != data.Checkpoint.Checkpoints[iCheckpoint].vCoords.z
				data.Checkpoint.Checkpoints[iCheckpoint].vCoords = sDebugVars.Checkpoints.sDebugData[iCheckpoint].vDebugCoord
				sDebugVars.Checkpoints.sDebugData[iCheckpoint].bUpdateCheckpoint = TRUE
				tlFormatetOutput = "Checkpoints["
				tlFormatetOutput += iCheckpoint
				tlFormatetOutput += "].vCoords = "
				tlFormatetOutput += VECTOR_TO_STRING(data.Checkpoint.Checkpoints[iCheckpoint].vCoords)
				SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.Checkpoints.sDebugData[iCheckpoint].twCheckpointLocation, tlFormatetOutput)
			ENDIF
			IF sDebugVars.Checkpoints.sDebugData[iCheckpoint].fDebugVisualRadius = -1.0
				sDebugVars.Checkpoints.sDebugData[iCheckpoint].fDebugVisualRadius = data.Checkpoint.Checkpoints[iCheckpoint].fVisualRadius			
			ENDIF
			IF sDebugVars.Checkpoints.sDebugData[iCheckpoint].fDebugVisualRadius != data.Checkpoint.Checkpoints[iCheckpoint].fVisualRadius
				data.Checkpoint.Checkpoints[iCheckpoint].fVisualRadius = sDebugVars.Checkpoints.sDebugData[iCheckpoint].fDebugVisualRadius
				sDebugVars.Checkpoints.sDebugData[iCheckpoint].bUpdateCheckpoint = TRUE
				tlFormatetOutput = "Checkpoints["
				tlFormatetOutput += iCheckpoint
				tlFormatetOutput += "].fVisualRadius = "
				tlFormatetOutput += FLOAT_TO_STRING(data.Checkpoint.Checkpoints[iCheckpoint].fVisualRadius)
				SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.Checkpoints.sDebugData[iCheckpoint].twCheckpointVisualRadius, tlFormatetOutput)
			ENDIF
			IF sDebugVars.Checkpoints.sDebugData[iCheckpoint].fDebugCollectRadius = -1.0
				sDebugVars.Checkpoints.sDebugData[iCheckpoint].fDebugCollectRadius = data.Checkpoint.Checkpoints[iCheckpoint].fCollectRadius	
			ENDIF
			IF sDebugVars.Checkpoints.sDebugData[iCheckpoint].fDebugCollectRadius != data.Checkpoint.Checkpoints[iCheckpoint].fCollectRadius
				data.Checkpoint.Checkpoints[iCheckpoint].fCollectRadius = sDebugVars.Checkpoints.sDebugData[iCheckpoint].fDebugCollectRadius
				sDebugVars.Checkpoints.sDebugData[iCheckpoint].bUpdateCheckpoint = TRUE
				tlFormatetOutput = "Checkpoints["
				tlFormatetOutput += iCheckpoint
				tlFormatetOutput += "].fCollectRadius = "
				tlFormatetOutput += FLOAT_TO_STRING(data.Checkpoint.Checkpoints[iCheckpoint].fCollectRadius)
				SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.Checkpoints.sDebugData[iCheckpoint].twCheckpointCollectRadius, tlFormatetOutput)
			ENDIF
		ENDREPEAT
	#ENDIF
	
	IF logic.Debug.Widgets.Update != NULL
		CALL logic.Debug.Widgets.Update()
	ENDIF
ENDPROC

PROC SETUP_DEBUG_DATA()
	sDebugVars.bBlockCheatTickers = GET_COMMANDLINE_PARAM_EXISTS("sc_blockJSkipTickers")
ENDPROC

PROC DISPLAY_CHEATED_DEBUG_TEXT(PLAYER_INDEX playerID, STRING strCheatType)
	IF sDebugVars.bBlockCheatTickers
		EXIT	
	ENDIF
	
	BEGIN_TEXT_COMMAND_PRINT("TICK_CHEATED")
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(GET_PLAYER_NAME(playerID))
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(strCheatType)
	END_TEXT_COMMAND_PRINT(5000, TRUE)
ENDPROC

PROC PROCESS_FM_CONTENT_MISSION_DEBUG_EVENT(INT iEventID)
	SCRIPT_EVENT_DATA_FM_CONTENT_MISSION_DEBUG EventData
	IF NOT GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, EventData, SIZE_OF(EventData))
		EXIT
	ENDIF
	
	INT iEntity
	STRING strEventName = GET_FM_CONTENT_MISSION_DEBUG_EVENT_STRING(EventData.eDebugEvent)
	
	IF logic.Debug.OverrideEvent != NULL
	AND CALL logic.Debug.OverrideEvent(EventData)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] ", strEventName, " - Debug event received from ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex), ". Default behaviour has been overridden.")
		EXIT
	ENDIF
	
	SWITCH EventData.eDebugEvent
		CASE FM_CONTENT_DEBUG_S_PASS
			SET_DEBUG_LOCAL_BIT(eDEBUGLOCALBITSET_S_PASS)
			DISPLAY_CHEATED_DEBUG_TEXT(EventData.Details.FromPlayerIndex, "TICK_CHEAT_SPAS")
			
			IF IS_LOCAL_HOST
				MARK_ALL_MISSION_ENTITIES_AS_DELIVERED()
				SET_END_REASON(eENDREASON_MISSION_PASSED)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] ", strEventName, " - ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex), " has S-passed.")
			ENDIF
		BREAK
		
		CASE FM_CONTENT_DEBUG_F_FAIL
			SET_DEBUG_LOCAL_BIT(eDEBUGLOCALBITSET_F_FAIL)
			DISPLAY_CHEATED_DEBUG_TEXT(EventData.Details.FromPlayerIndex, "TICK_CHEAT_FFAI")
			
			IF IS_LOCAL_HOST
				SET_END_REASON(eENDREASON_TIME_UP)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] ", strEventName, " - ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex), " has F-failed.")
			ENDIF
		BREAK
		
		CASE FM_CONTENT_DEBUG_T_KILL
			IF IS_LOCAL_HOST
				REPEAT data.Ped.iCount iEntity
					IF GET_PED_STATE(iEntity) = ePEDSTATE_ACTIVE
						SET_PED_BIT(iEntity, ePEDBITSET_DEBUG_KILL)
					ENDIF
				ENDREPEAT
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] ", strEventName, " - ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex), " has killed all mission peds.")
			ENDIF
		BREAK
		
		CASE FM_CONTENT_DEBUG_LOSE_WANTED_GANG
			DISPLAY_CHEATED_DEBUG_TEXT(EventData.Details.FromPlayerIndex, "TICK_CHEAT_WNTG")
		
			IF IS_LOCAL_HOST
				GIVE_WANTED_LEVEL_TO_GANG(0, FALSE)
			ENDIF
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] ", strEventName, " - ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex), " has caused gang to lose wanted level.")
		BREAK
		
		CASE FM_CONTENT_DEBUG_J_SKIP
		
			IF NOT ARE_MISSION_INSTANCE_INFO_THE_SAME(sMission, EventData.sMII)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] ", strEventName, " - EXIT due to debug conflict ")
				EXIT
			ENDIF
		
			SWITCH GET_MODE_STATE()
				CASE eMODESTATE_TAKE_OUT_TARGETS
					IF IS_LOCAL_HOST
						REPEAT data.Ped.iCount iEntity
							IF IS_PED_BIT_SET(iEntity, ePEDBITSET_TARGET_PED)
							AND GET_PED_STATE(iEntity) = ePEDSTATE_ACTIVE
								SET_PED_BIT(iEntity, ePEDBITSET_DEBUG_KILL)
							ENDIF
						ENDREPEAT	
						
						REPEAT data.Vehicle.iCount iEntity
							IF IS_VEHICLE_BIT_SET(iEntity, eVEHICLEBITSET_TARGET_VEHICLE)
							AND GET_VEHICLE_STATE(iEntity) = eVEHICLESTATE_DRIVEABLE
								SET_VEHICLE_BIT(iEntity, eVEHICLEBITSET_DEBUG_KILL)
							ENDIF
						ENDREPEAT	
						
						REPEAT data.Prop.iCount iEntity
							IF IS_PROP_BIT_SET(iEntity, ePROPBITSET_TARGET_PROP)
							AND GET_PROP_STATE(iEntity) = ePROPSTATE_IDLE
								SET_PROP_BIT(iEntity, ePROPBITSET_DEBUG_KILL)
							ENDIF
						ENDREPEAT
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] ", strEventName, " - ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex), " has killed all targets.")
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		DEFAULT
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] ", strEventName, " - No conditions set up for event sent from ", GET_PLAYER_NAME(EventData.Details.FromPlayerIndex), ".")
		BREAK
	ENDSWITCH
ENDPROC

PROC DEBUG_MAINTAIN_DEBUG_WINDOW_TOGGLE()

	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_D, KEYBOARD_MODIFIER_CTRL_SHIFT, "NO")
		IF g_debugDisplayState = DEBUG_DISP_OFF
			g_debugDisplayState = DEBUG_DISP_CHK
			PRINTLN("g_debugDisplayState = DEBUG_DISP_CHK")
		ELSE
			g_debugDisplayState = DEBUG_DISP_OFF
			PRINTLN("g_debugDisplayState = DEBUG_DISP_OFF")
		ENDIF
	ENDIF
	
	SWITCH g_debugDisplayState 
	
		CASE DEBUG_DISP_OFF
	
			IF sDebugVars.bDrawDriveToPointCheckpoints
				sDebugVars.bDrawDriveToPointCheckpoints = FALSE
			ENDIF
			IF sDebugVars.bDrawPatrolCheckpoints
				sDebugVars.bDrawPatrolCheckpoints = FALSE
			ENDIF				

		BREAK
		
		CASE DEBUG_DISP_CHK
	
			IF NOT sDebugVars.bDrawDriveToPointCheckpoints
				sDebugVars.bDrawDriveToPointCheckpoints = TRUE
			ENDIF
			IF NOT sDebugVars.bDrawPatrolCheckpoints
				sDebugVars.bDrawPatrolCheckpoints = TRUE
			ENDIF				

		BREAK
		
	ENDSWITCH

ENDPROC

FUNC INT DEBUG_PATROL_PED()
	INT iReturn = -1

	RETURN iReturn
ENDFUNC

FUNC INT DBG_STAGE(INT iPed)

	INT iReturn = serverBD.sPed[iPed].iCheckpointStage
	
	RETURN iReturn
ENDFUNC

FUNC INT DBG_MAX(INT iPed)

	UNUSED_PARAMETER(iPed)

	INT iReturn 
	
	RETURN iReturn
ENDFUNC

FUNC VECTOR DBG_POS(INT iPed)

	UNUSED_PARAMETER(iPed)

	VECTOR vReturn 
	
	RETURN vReturn
ENDFUNC

PROC DEBUG_RENDER_WINDOW()

	IF g_debugDisplayState != DEBUG_DISP_CHK
		EXIT
	ENDIF

	FLOAT fDrawX, fDrawY
	INT iRows
	INT r,g,b,a
	TEXT_LABEL_63 tl63Temp
	
	// Debug starting positions
	FLOAT fWindowX = 0.287//TweakX
	FLOAT fWindowY = 0.815//Tweaky

	// Black box values
	FLOAT fBlackW = 0.275//Tweakz
	FLOAT fBlackH = 0.182
	FLOAT fBlackX = fWindowX + (fBlackW / 2.0) - 0.001//0.383//TweakX
	FLOAT fBlackY = 0.829//Tweaky
	
	// draw black box
	DRAW_RECT(fBlackX, fBlackY, fBlackW, fBlackH, 0, 0, 0, 190)
	
	BOOL bDrawPatrolValues = (DEBUG_PATROL_PED() != -1)
	
	STRING sDisplay 
	INT iLength
	
	// COLUMN HEADINGS ///////////////////////////////////////////////////////////////////////
	
	// SUBVARIATION	
	sDisplay = GET_SUBVARIATION_NAME_FOR_DEBUG()
	iLength = GET_LENGTH_OF_LITERAL_STRING(sDisplay)
	sDisplay = GET_STRING_FROM_STRING(sDisplay, 4, iLength)
	
	fDrawX = fWindowX
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	HUD_COLOURS hcSubvariation = HUD_COLOUR_GREY
	IF HAS_ANY_PED_GROUP_BEEN_TRIGGERED()
		hcSubvariation = HUD_COLOUR_RED
	ENDIF
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", sDisplay, hcSubvariation)
	
	iRows += 1
	
	iRows += 1
	
	// SERVER
	fDrawX = fWindowX
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	GET_HUD_COLOUR(HUD_COLOUR_WHITE, r,g,b,a)
	SET_TEXT_COLOUR(r,g,b,a)
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Server:", HUD_COLOUR_GREY)
	iRows += 1
	
	// CLIENT
	fDrawX = fWindowX
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, r,g,b,a)
	SET_TEXT_COLOUR(r,g,b,a)
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Client:", HUD_COLOUR_GREY)	
	iRows += 1
	
	// iRandomModeInt0
	fDrawX = fWindowX 
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, r,g,b,a)
	SET_TEXT_COLOUR(r,g,b,a)
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Random INT:", HUD_COLOUR_GREY)	
	iRows += 1
	
	// Audio Trigger
	fDrawX = fWindowX 
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Audio Trigger:", HUD_COLOUR_GREY)	
	iRows += 1
	
	// Dialogue Root
	fDrawX = fWindowX 
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)		
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Dialogue Root:", HUD_COLOUR_GREY)	
	iRows += 1
	
	IF bDrawPatrolValues
		
		iRows += 1
	
		fDrawX = fWindowX
		fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)
		SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
		DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Patrol Stage:", HUD_COLOUR_GREY)	
		iRows += 1
		
		fDrawX = fWindowX
		fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)
		SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
		DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Patrol Max:", HUD_COLOUR_GREY)	
		iRows += 1
		
		fDrawX = fWindowX
		fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)
		SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
		DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", "Patrol Destination:", HUD_COLOUR_GREY)	
		iRows += 1
	ENDIF
	
	// POPULATE VARIABLES /////////////////////////////////////////////////////////////////////////////
	
	iRows = 0
	
	HUD_COLOURS HudColour = HUD_COLOUR_WHITE
		
	fDrawX = fWindowX * 1.25		
	
	iRows += 1
	
	iRows += 1
				
	// Server State
	
	sDisplay = GET_MODE_STATE_NAME(GET_MODE_STATE())
	iLength = GET_LENGTH_OF_LITERAL_STRING(sDisplay)
	sDisplay = GET_STRING_FROM_STRING(sDisplay, 11, iLength)
	
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)			
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", sDisplay, HudColour)
	iRows += 1
	
	GET_HUD_COLOUR(HUD_COLOUR_WHITE, r,g,b,a)	
	
	// Client state
	
	sDisplay = GET_MODE_STATE_NAME(GET_CLIENT_MODE_STATE())
	iLength = GET_LENGTH_OF_LITERAL_STRING(sDisplay)
	sDisplay = GET_STRING_FROM_STRING(sDisplay, 11, iLength)
	
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", sDisplay, HudColour)
	iRows += 1
	
	// serverBD.iRandomModeInt0
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	DISPLAY_TEXT_WITH_NUMBER(fDrawX, fDrawY, "NUMBR", serverBD.iRandomModeInt0)
	iRows += 1
	
	// Audio Trigger
	#IF MAX_NUM_MUSIC_TRIGGERS
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	GET_HUD_COLOUR(HUD_COLOUR_WHITE, r,g,b,a)
	SET_TEXT_COLOUR(r,g,b,a)
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", sDebugVars.MusicTriggers.tl63CurrentTrigger, HUD_COLOUR_WHITE)
	iRows += 1
	#ENDIF
	
	// Dialogue Root
	fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)
	SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
	GET_HUD_COLOUR(HUD_COLOUR_WHITE, r,g,b,a)
	SET_TEXT_COLOUR(r,g,b,a)
	tl63Temp = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	DISPLAY_TEXT_WITH_PLAYER_NAME(fDrawX, fDrawY, "STRING", tl63Temp, HUD_COLOUR_WHITE)
	iRows += 1
	
	IF bDrawPatrolValues
		
		iRows += 1
	
		VECTOR vDisplay = DBG_POS(DEBUG_PATROL_PED())

		fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)
		SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
		DISPLAY_TEXT_WITH_NUMBER(fDrawX, fDrawY, "NUMBR", DBG_STAGE(DEBUG_PATROL_PED()))
		iRows += 1
		
		fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)
		SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
		DISPLAY_TEXT_WITH_NUMBER(fDrawX, fDrawY, "NUMBR", DBG_MAX(DEBUG_PATROL_PED()))
		iRows += 1
		
		fDrawY = fWindowY + g_SpawnData.MissionSpawnDetails.SpawnWindowRowY + (TO_FLOAT(iRows) * g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY)
		SET_TEXT_SCALE(0.0000, g_SpawnData.MissionSpawnDetails.SpawnWindowScale)
		DISPLAY_TEXT_WITH_VECTOR(fDrawX, fDrawY, "SPN_V", vDisplay, 0)
		iRows += 1
	ENDIF
ENDPROC

FUNC FLOAT DEBUG_GET_HEADING_BETWEEN_LOCATIONS_2D(VECTOR a, VECTOR b)
	VECTOR direction = b - a
	RETURN GET_HEADING_FROM_VECTOR_2D(direction.x, direction.y)
ENDFUNC

FUNC NETWORK_INDEX DEBUG_GET_CARRIER_VEHICLE(BOOL bRandom = FALSE, BOOL bIncludeInactive = FALSE, BOOL bRequireSeat = FALSE, VEHICLE_SEAT eSeat = VS_DRIVER)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] DEBUG_GET_CARRIER_VEHICLE - bRandom = ", GET_STRING_FROM_BOOL(bRandom), ", bRequireSeat = ", GET_STRING_FROM_BOOL(bRequireSeat), " and eSeat = ", ENUM_TO_INT(eSeat))
	
	INT iCarriers[MAX_NUM_MISSION_ENTITIES]
	
	//Populate iCarriers array with carrier vehicle indices, randomising the order if required.
	POPULATE_ARRAY_WITH_INDICES(iCarriers, GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION(), bRandom)
	
	//Select a valid carrier vehicle.
	INT i
	REPEAT GET_NUM_MISSION_ENTITIES_IN_THIS_VARIATION() i
		IF GET_MISSION_ENTITY_CARRIER(iCarriers[i]) = (-1)
			RELOOP
		ENDIF
		
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sVehicle[GET_MISSION_ENTITY_CARRIER(iCarriers[i])].netId)
			VEHICLE_INDEX vehId = NET_TO_VEH(serverBD.sVehicle[GET_MISSION_ENTITY_CARRIER(iCarriers[i])].netId)
			
			IF (NOT IS_MISSION_ENTITY_BIT_SET(iCarriers[i], eMISSIONENTITYBITSET_DELIVERED)
				OR bIncludeInactive)
			AND IS_VEHICLE_DRIVEABLE(vehId)
			AND (NOT bRequireSeat OR IS_VEHICLE_SEAT_FREE(vehId, eSeat))
				RETURN serverBD.sVehicle[GET_MISSION_ENTITY_CARRIER(iCarriers[i])].netId
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] DEBUG_GET_CARRIER_VEHICLE - No carrier vehicle found.")
	RETURN NULL
ENDFUNC

FUNC ENTITY_INDEX DEBUG_GET_ENTITY_INDEX_FOR_FOLLOW_ENTITY()
	IF logic.Debug.FollowEntity != NULL
		RETURN CALL logic.Debug.FollowEntity()
	ENDIF
	
	NETWORK_INDEX netId = DEBUG_GET_CARRIER_VEHICLE(TRUE)
	
	IF netId = NULL
		ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] DEBUG_GET_ENTITY_INDEX_FOR_FOLLOW_ENTITY - Returning NULL.")
		RETURN NULL
	ENDIF
	
	RETURN NET_TO_ENT(netId)
ENDFUNC

PROC DEBUG_DO_FOLLOW_ENTITY_J_SKIP()
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] DEBUG_DO_FOLLOW_ENTITY_J_SKIP - Toggling eDEBUGLOCALBITSET_FOLLOW_ENTITY_J_SKIP.")
	
	IF g_debugDisplayState = DEBUG_DISP_CHK
	AND logic.Debug.FollowEntityEndPoint != null
		IF DOES_ENTITY_EXIST(sDebugVars.entFollowEntity)
		AND TAKE_CONTROL_OF_ENTITY(sDebugVars.entFollowEntity)
			SET_ENTITY_COORDS(sDebugVars.entFollowEntity, CALL logic.Debug.FollowEntityEndPoint())
		ENDIF
	ELSE					
		IF NOT IS_DEBUG_LOCAL_BIT_SET(eDEBUGLOCALBITSET_FOLLOW_ENTITY_J_SKIP)
			sDebugVars.eFollowEntityClientModeState = GET_CLIENT_MODE_STATE()
			sDebugVars.entFollowEntity = DEBUG_GET_ENTITY_INDEX_FOR_FOLLOW_ENTITY()
			SET_DEBUG_LOCAL_BIT(eDEBUGLOCALBITSET_FOLLOW_ENTITY_J_SKIP)
		ELSE
			CLEAR_DEBUG_LOCAL_BIT(eDEBUGLOCALBITSET_FOLLOW_ENTITY_J_SKIP)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DEBUG_SHOULD_STOP_FOLLOW_ENTITY()
	RETURN sDebugVars.eFollowEntityClientModeState != GET_CLIENT_MODE_STATE()
ENDFUNC

FUNC VECTOR DEBUG_GET_FOLLOW_ENTITY_OFFSET_VECTOR()
	IF logic.Debug.FollowEntityOffsetVector != NULL
		RETURN CALL logic.Debug.FollowEntityOffsetVector()
	ENDIF
	
	RETURN <<0.0, -5.0, 1.0>>
ENDFUNC

PROC DEBUG_MAINTAIN_FOLLOW_ENTITY()
	IF NOT IS_DEBUG_LOCAL_BIT_SET(eDEBUGLOCALBITSET_FOLLOW_ENTITY_J_SKIP)
		EXIT
	ENDIF
	
	IF DEBUG_SHOULD_STOP_FOLLOW_ENTITY()
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - DEBUG_MAINTAIN_FOLLOW_ENTITY - DEBUG_SHOULD_STOP_FOLLOW_ENTITY() = TRUE. Resetting sDebugVars.eFollowEntityClientModeState, clearing eDEBUGLOCALBITSET_FOLLOW_ENTITY_J_SKIP and exiting.")
		sDebugVars.eFollowEntityClientModeState = eMODESTATE_END
		CLEAR_DEBUG_LOCAL_BIT(eDEBUGLOCALBITSET_FOLLOW_ENTITY_J_SKIP)
		EXIT
	ENDIF
	
	IF DOES_ENTITY_EXIST(sDebugVars.entFollowEntity)
	AND NOT IS_ENTITY_DEAD(sDebugVars.entFollowEntity)
		FLOAT fHeading = GET_ENTITY_HEADING(sDebugVars.entFollowEntity)
		VECTOR vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(sDebugVars.entFollowEntity, FALSE), fHeading, DEBUG_GET_FOLLOW_ENTITY_OFFSET_VECTOR())
		
		SET_ENTITY_COORDS(LOCAL_PED_INDEX, vCoords, FALSE, TRUE, DEFAULT, FALSE)
		SET_ENTITY_ROTATION(LOCAL_PED_INDEX, <<0.0, 0.0, fHeading>>, DEFAULT, FALSE)
	ENDIF
ENDPROC

PROC DEBUG_MAINTAIN_MODE_TIMER()
	IF NOT HAS_NET_TIMER_STARTED(serverBD.stModeTimer)
		EXIT
	ENDIF
	
	IF sDebugVars.ModeTimer.bPause
	AND IS_LOCAL_HOST
		NET_TIMER_PAUSE_THIS_FRAME(serverBd.stModeTimer, sDebugVars.ModeTimer.stPauseTimer)
	ELSE
		IF HAS_NET_TIMER_STARTED(sDebugVars.ModeTimer.stPauseTimer)
			NET_TIMER_PAUSE_RESET(sDebugVars.ModeTimer.stPauseTimer)
		ENDIF
	ENDIF
	
	IF sDebugVars.ModeTimer.iSetElapsedTimeSCached != sDebugVars.ModeTimer.iSetElapsedTimeS
	AND IS_LOCAL_HOST
		sDebugVars.ModeTimer.iSetElapsedTimeSCached = sDebugVars.ModeTimer.iSetElapsedTimeS
		
		RESET_NET_TIMER(serverBd.stModeTimer)
		START_NET_TIMER_RETROACTIVELY(serverBd.stModeTimer, sDebugVars.ModeTimer.iSetElapsedTimeS * 1000)
	ENDIF
	
	sDebugVars.ModeTimer.iElapsedTimeS = GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(serverBD.stModeTimer) / 1000
ENDPROC

#IF MAX_NUM_SEARCH_AREAS
PROC DEBUG_ADD_SEARCH_AREA_TRIGGER_BLIP(INT iSearchArea)
	IF NOT DOES_BLIP_EXIST(sDebugVars.SearchArea.Areas[iSearchArea].blipTrigger)
		sDebugVars.SearchArea.Areas[iSearchArea].blipTrigger = ADD_BLIP_FOR_COORD(CALL logic.SearchArea.TriggerCoords(iSearchArea))
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(sDebugVars.SearchArea.Areas[iSearchArea].blipTrigger, HUD_COLOUR_GREEN)
	ENDIF
	
	DRAW_DEBUG_CORONA(CALL logic.SearchArea.TriggerCoords(iSearchArea), data.SearchArea.Areas[iSearchArea].fTriggerRadius, HUD_COLOUR_GREEN)
ENDPROC

PROC DEBUG_REMOVE_SEARCH_AREA_TRIGGER_BLIP(INT iSearchArea)
	IF DOES_BLIP_EXIST(sDebugVars.SearchArea.Areas[iSearchArea].blipTrigger)
		REMOVE_BLIP(sDebugVars.SearchArea.Areas[iSearchArea].blipTrigger)
	ENDIF
ENDPROC

PROC DEBUG_MAINTAIN_SEARCH_AREA()
	IF NOT CALL logic.SearchArea.Enable()
		sDebugVars.SearchArea.bShowTriggers = FALSE
	ENDIF
	
	IF sSearchArea.iCurrentArea != -1
		sDebugVars.SearchArea.fTriggerDistance = SQRT(sSearchArea.fTriggerDistanceSquared)
	ELSE
		sDebugVars.SearchArea.fTriggerDistance = -1.0
	ENDIF
	
	INT iSearchArea
	REPEAT data.SearchArea.iNumAreas iSearchArea
		IF CALL logic.SearchArea.Enable()
		AND CALL logic.SearchArea.EnableArea(iSearchArea)
			sDebugVars.SearchArea.Areas[iSearchArea].iEnabled = 1
			sDebugVars.SearchArea.Areas[iSearchArea].fCentreDistance = SQRT(sSearchArea.sAreas[iSearchArea].fCentreDistanceSquared)
			
			IF sDebugVars.SearchArea.bShowTriggers
				DEBUG_ADD_SEARCH_AREA_TRIGGER_BLIP(iSearchArea)
			ENDIF
		ELSE
			sDebugVars.SearchArea.Areas[iSearchArea].iEnabled = 0
			sDebugVars.SearchArea.Areas[iSearchArea].fCentreDistance = -1.0
			
			DEBUG_REMOVE_SEARCH_AREA_TRIGGER_BLIP(iSearchArea)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC INT DEBUG_GET_SEARCH_AREA(BOOL bRandom = FALSE)
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] DEBUG_GET_SEARCH_AREA - bRandom = ", GET_STRING_FROM_BOOL(bRandom))
	
	INT iSearchAreas[MAX_NUM_SEARCH_AREAS]
	
	//Populate iSearchAreas array with search area indices, randomising the order if required.
	POPULATE_ARRAY_WITH_INDICES(iSearchAreas, data.SearchArea.iNumAreas, bRandom)
	
	//Select a valid search area.
	INT i
	REPEAT data.SearchArea.iNumAreas i
		IF CALL logic.SearchArea.EnableArea(iSearchAreas[i])
			RETURN iSearchAreas[i]
		ENDIF
	ENDREPEAT
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] DEBUG_GET_SEARCH_AREA - No search area found.")
	RETURN -1
ENDFUNC
#ENDIF

#IF MAX_NUM_CUTSCENES
PROC DEBUG_MAINTAIN_CUTSCENE()
	//Current scene.
	sDebugVars.Cutscene.iScene = GET_CURRENT_CUTSCENE(LOCAL_PARTICIPANT_INDEX)
	
	IF sDebugVars.Cutscene.iSceneCached != sDebugVars.Cutscene.iScene
		sDebugVars.Cutscene.iSceneCached = sDebugVars.Cutscene.iScene
		
		sDebugVars.Cutscene.fPhase = 0.0
		sDebugVars.Cutscene.fRate = 1.0
		
		sDebugVars.Cutscene.vPositionLocalOffset = <<0.0, 0.0, 0.0>>
		sDebugVars.Cutscene.vOrientationOffset = <<0.0, 0.0, 0.0>>
		
		SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.Cutscene.twFinalPosition, "")
		SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.Cutscene.twFinalOrientation, "")
	ENDIF
	
	//Pause, phase and rate.
	IF sDebugVars.Cutscene.iScene != -1
	AND IS_SYNCHRONIZED_SCENE_RUNNING(sCutscene.sScenes[sDebugVars.Cutscene.iScene].iID)
		//If the widgets have been modified through RAG, update the cutscene data.
		IF sDebugVars.Cutscene.bPause
		OR sDebugVars.Cutscene.fPhase != sDebugVars.Cutscene.fPhaseCached
			SET_SYNCHRONIZED_SCENE_PHASE(sCutscene.sScenes[sDebugVars.Cutscene.iScene].iID, sDebugVars.Cutscene.fPhase)
		ENDIF
		
		IF sDebugVars.Cutscene.fRate != sDebugVars.Cutscene.fRateCached
			SET_SYNCHRONIZED_SCENE_RATE(sCutscene.sScenes[sDebugVars.Cutscene.iScene].iID, sDebugVars.Cutscene.fRate)
		ENDIF
		
		IF NOT ARE_VECTORS_EQUAL(sDebugVars.Cutscene.vPositionLocalOffsetCached, sDebugVars.Cutscene.vPositionLocalOffset)
		OR NOT ARE_VECTORS_EQUAL(sDebugVars.Cutscene.vOrientationOffsetCached, sDebugVars.Cutscene.vOrientationOffset)
		OR IS_STRING_NULL_OR_EMPTY(GET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.Cutscene.twFinalPosition))
			VECTOR vOrientation
			
			IF logic.Cutscene.Heading != NULL
				vOrientation = <<0.0, 0.0, CALL logic.Cutscene.Heading(sDebugVars.Cutscene.iScene)>>
			ELSE
				vOrientation = <<0.0, 0.0, data.Cutscene.Scenes[sDebugVars.Cutscene.iScene].fHeading>>
			ENDIF
			
			vOrientation += sDebugVars.Cutscene.vOrientationOffset
			
			VECTOR vPosition
			
			IF logic.Cutscene.Coords != NULL
				vPosition = CALL logic.Cutscene.Coords(sDebugVars.Cutscene.iScene)
			ELSE
				vPosition = data.Cutscene.Scenes[sDebugVars.Cutscene.iScene].vCoords
			ENDIF
			
			vPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPosition, vOrientation.z, sDebugVars.Cutscene.vPositionLocalOffset)
			
			SET_SYNCHRONIZED_SCENE_ORIGIN(sCutscene.sScenes[sDebugVars.Cutscene.iScene].iID, vPosition, vOrientation)
			
			sDebugVars.Cutscene.vPositionLocalOffsetCached = sDebugVars.Cutscene.vPositionLocalOffset
			sDebugVars.Cutscene.vOrientationOffsetCached = sDebugVars.Cutscene.vOrientationOffset
			
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.Cutscene.twFinalPosition, GET_STRING_FROM_VECTOR(vPosition))
			SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.Cutscene.twFinalOrientation, GET_STRING_FROM_VECTOR(vOrientation))
		ENDIF
		
		//Update the widgets and cached values with the current cutscene data.
		sDebugVars.Cutscene.fPhaseCached = GET_SYNCHRONIZED_SCENE_PHASE(sCutscene.sScenes[sDebugVars.Cutscene.iScene].iID)
		sDebugVars.Cutscene.fPhase = sDebugVars.Cutscene.fPhaseCached
		
		sDebugVars.Cutscene.fRateCached = GET_SYNCHRONIZED_SCENE_RATE(sCutscene.sScenes[sDebugVars.Cutscene.iScene].iID)
		sDebugVars.Cutscene.fRate = sDebugVars.Cutscene.fRateCached
	ENDIF
ENDPROC
#ENDIF

#IF MAX_NUM_COLLISION_TESTS
PROC DEBUG_MAINTAIN_COLLISION_TESTS()
	IF NOT sDebugVars.CollisionTests.bEnable
		EXIT
	ENDIF
	
	INT iR, iG, iB, iA
	INT i
	REPEAT MAX_NUM_COLLISION_TESTS i
		SWITCH GET_COLLISION_TEST_STATE(i)
			CASE eCOLLISIONTESTSTATE_IN_PROGRESS
			CASE eCOLLISIONTESTSTATE_COMPLETE
				SWITCH GET_COLLISION_TEST_RESULT(i)
					CASE eCOLLISIONTESTRESULT_COLLISION
						GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
					BREAK
					
					CASE eCOLLISIONTESTRESULT_NO_COLLISION
						GET_HUD_COLOUR(HUD_COLOUR_GREEN, iR, iG, iB, iA)
					BREAK
					
					DEFAULT
						GET_HUD_COLOUR(HUD_COLOUR_BLUE, iR, iG, iB, iA)
					BREAK
				ENDSWITCH
				
				SWITCH sDebugVars.CollisionTests.sData[i].eType
					CASE eDEBUGCOLLISIONTESTTYPE_BOX
						DEBUG_DRAW_BOX(sDebugVars.CollisionTests.sData[i].vVectors[0], sDebugVars.CollisionTests.sData[i].vVectors[1], sDebugVars.CollisionTests.sData[i].vVectors[2], iR, iG, iB, DEFAULT, eDEBUGDRAWBOXTYPE_WIRE)
					BREAK
					
					CASE eDEBUGCOLLISIONTESTTYPE_LINE
						DRAW_DEBUG_LINE(sDebugVars.CollisionTests.sData[i].vVectors[0], sDebugVars.CollisionTests.sData[i].vVectors[1], iR, iG, iB)
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC

PROC DEBUG_MAINTAIN_DRAW_BOX()
	IF sDebugVars.CollisionTests.DrawBox.bUsePlayerPositionOrientation
		sDebugVars.CollisionTests.DrawBox.bUsePlayerPositionOrientation = FALSE
		
		sDebugVars.CollisionTests.DrawBox.vPosition = LOCAL_PLAYER_COORD
		sDebugVars.CollisionTests.DrawBox.vOrientation = GET_ENTITY_ROTATION(LOCAL_PED_INDEX)
	ENDIF
	
	IF sDebugVars.CollisionTests.DrawBox.bEnable
		IF IS_VECTOR_ZERO(sDebugVars.CollisionTests.DrawBox.vDimensions)
			sDebugVars.CollisionTests.DrawBox.vDimensions = <<1.0, 1.0, 1.0>>
		ENDIF
		
		VECTOR vFinalOrientation = sDebugVars.CollisionTests.DrawBox.vOrientation + sDebugVars.CollisionTests.DrawBox.vOrientationOffset
		VECTOR vFinalPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sDebugVars.CollisionTests.DrawBox.vPosition, vFinalOrientation.z, sDebugVars.CollisionTests.DrawBox.vPositionLocalOffset)
		
		INT iR, iG, iB, iA
		GET_HUD_COLOUR(INT_TO_ENUM(HUD_COLOURS, sDebugVars.CollisionTests.DrawBox.iColour), iR, iG, iB, iA)
		
		DEBUG_DRAW_BOX(vFinalPosition, sDebugVars.CollisionTests.DrawBox.vDimensions, vFinalOrientation, iR, iG, iB, sDebugVars.CollisionTests.DrawBox.iAlpha, INT_TO_ENUM(eDEBUG_DRAW_BOX_TYPE, sDebugVars.CollisionTests.DrawBox.iType))
		
		SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.CollisionTests.DrawBox.twFinalPosition, GET_STRING_FROM_VECTOR(vFinalPosition))
		SET_CONTENTS_OF_TEXT_WIDGET(sDebugVars.CollisionTests.DrawBox.twFinalOrientation, GET_STRING_FROM_VECTOR(vFinalOrientation))
	ENDIF
ENDPROC
#ENDIF

#IF MAX_NUM_INTERACT_LOCATIONS
FUNC INT DEBUG_GET_INTERACT_LOCATION(BOOL bRandom = FALSE, BOOL bIncludeInactive = FALSE)
	INT iLocations[MAX_NUM_INTERACT_LOCATIONS]
	
	//Populate iLocations array with locations, randomising the order if required.
	POPULATE_ARRAY_WITH_INDICES(iLocations, data.Interact.iNumLocations, bRandom)
	
	//Select a valid location.
	INT i
	REPEAT data.Interact.iNumLocations i
		IF bIncludeInactive
		OR CALL logic.Interact.EnableLocation(i)
			RETURN i
		ENDIF
	ENDREPEAT
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [INTERACT] - DEBUG_GET_INTERACT_LOCATION - No interact location found.")
	RETURN -1
ENDFUNC
#ENDIF

#IF MAX_NUM_ARREST_PED_SUSPECTS
FUNC INT DEBUG_GET_ARREST_PED_SUSPECT(BOOL bRandom = FALSE, BOOL bIncludeInactive = FALSE)
	INT iSuspects[MAX_NUM_ARREST_PED_SUSPECTS]
	
	//Populate iSuspects array with suspects, randomising the order if required.
	POPULATE_ARRAY_WITH_INDICES(iSuspects, data.ArrestPed.iNumSuspects, bRandom)
	
	//Select a valid suspect.
	INT i
	REPEAT data.ArrestPed.iNumSuspects i
		IF bIncludeInactive
		OR CALL logic.ArrestPed.EnableSuspect(i)
			RETURN i
		ENDIF
	ENDREPEAT
	
	PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] - [ARREST_PED] - DEBUG_GET_ARREST_PED_SUSPECT - No arrest ped suspect found.")
	RETURN -1
ENDFUNC

FUNC VECTOR DEBUG_GET_ARREST_PED_SUSPECT_OFFSET_VECTOR(INT iSuspect)
	IF logic.Debug.ArrestPedOffsetVector != NULL
		RETURN CALL logic.Debug.ArrestPedOffsetVector(iSuspect)
	ENDIF
	
	RETURN <<0.0, data.ArrestPed.Suspects[iSuspect].fMaxDistance / 2, 0.0>>
ENDFUNC
#ENDIF

#IF MAX_NUM_SEARCH_AREAS
FUNC VECTOR DEBUG_GET_SEARCH_AREA_COORD_OFFSET_VECTOR(INT iSearchArea)
	IF logic.Debug.SearchAreaCoordOffsetVector != NULL
		RETURN CALL logic.Debug.SearchAreaCoordOffsetVector(iSearchArea)
	ENDIF
	
	RETURN <<0.0, -5.0, 0.0>>
ENDFUNC
#ENDIF

PROC DEBUG_MAINTAIN_SAVE_DATA()

	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_EnableFreemodeContentSave")
		EXIT
	ENDIF

	IF NOT IS_DEBUG_LOCAL_BIT_SET(eDEBUGLOCALBITSET_SAVING_DATA)
		IF IS_DEBUG_KEY_JUST_RELEASED(KEY_C, KEYBOARD_MODIFIER_SHIFT, "Save Data")
			SET_DEBUG_LOCAL_BIT(eDEBUGLOCALBITSET_SAVING_DATA)
		ENDIF
	ELSE
		IF SAVE_LOCAL_FREEMODE_DATA(GET_MISSION_PLACEMENT_DATA_FILENAME(), data, sLoad, TRUE)
			CLEAR_DEBUG_LOCAL_BIT(eDEBUGLOCALBITSET_SAVING_DATA)
		ENDIF
	ENDIF

ENDPROC

FUNC BOOL SHOULD_MAINTAIN_DEBUG()

	IF GET_FREEMODE_CONTENT_TYPE(sMission.iType) = eFMCONTENTTYPE_RANDOM_EVENT
		IF GB_IS_PLAYER_PERMANENT_PARTICIPANT(LOCAL_PLAYER_INDEX)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC

PROC MAINTAIN_DEBUG()

	IF NOT SHOULD_MAINTAIN_DEBUG()
		EXIT
	ENDIF

	// Ctrl-Shift-D debug window
	DEBUG_MAINTAIN_DEBUG_WINDOW_TOGGLE()
	DEBUG_RENDER_WINDOW()
	
	DEBUG_MAINTAIN_FOLLOW_ENTITY()
	DEBUG_MAINTAIN_MODE_TIMER()
	
	#IF MAX_NUM_SEARCH_AREAS
	DEBUG_MAINTAIN_SEARCH_AREA()
	#ENDIF
	
	#IF MAX_NUM_CUTSCENES
	DEBUG_MAINTAIN_CUTSCENE()
	#ENDIF
	
	#IF MAX_NUM_COLLISION_TESTS
	DEBUG_MAINTAIN_COLLISION_TESTS()
	DEBUG_MAINTAIN_DRAW_BOX()
	#ENDIF
	
	DEBUG_MAINTAIN_SAVE_DATA()
	
	// S-Pass
	IF IS_DEBUG_KEY_JUST_RELEASED(KEY_S, KEYBOARD_MODIFIER_NONE, "S Pass")
	OR sDebugVars.bSimulateSPass
		sDebugVars.bSimulateSPass = FALSE
		
		IF NOT sDebugVars.bBlockCheatTickers
			SCRIPT_EVENT_DATA_TICKER_MESSAGE sData
			sData.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_SPASS
			BROADCAST_TICKER_EVENT(sData, ALL_PLAYERS_ON_SCRIPT())
		ENDIF
		
		SET_GENERIC_BIT(eGENERICBITSET_I_WON)
		
		BROADCAST_EVENT_FM_CONTENT_MISSION_DEBUG(FM_CONTENT_DEBUG_S_PASS, sMission)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - KEY_S released. Sending FM_CONTENT_DEBUG_S_PASS event.")
		EXIT
	ENDIF
	
	// F-Fail
	IF IS_DEBUG_KEY_JUST_RELEASED(KEY_F, KEYBOARD_MODIFIER_NONE, "F Fail")
	OR sDebugVars.bSimulateFFail
		sDebugVars.bSimulateFFail = FALSE
	
		IF NOT sDebugVars.bBlockCheatTickers
			SCRIPT_EVENT_DATA_TICKER_MESSAGE sData
			sData.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_FFAIL
			BROADCAST_TICKER_EVENT(sData, ALL_PLAYERS_ON_SCRIPT())
		ENDIF
		
		BROADCAST_EVENT_FM_CONTENT_MISSION_DEBUG(FM_CONTENT_DEBUG_F_FAIL, sMission)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - KEY_F released. Sending FM_CONTENT_DEBUG_F_FAIL event.")
		EXIT
	ENDIF
	
	// Kill Mission Peds
	IF IS_DEBUG_KEY_JUST_RELEASED(KEY_T, KEYBOARD_MODIFIER_CTRL_SHIFT, "Kill Mission Peds")
		BROADCAST_EVENT_FM_CONTENT_MISSION_DEBUG(FM_CONTENT_DEBUG_T_KILL, sMission)	
		EXIT
	ENDIF
	
	// Lose wanted for gang
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_F3, KEYBOARD_MODIFIER_CTRL, "Lose Gang Wanted")
		BROADCAST_EVENT_FM_CONTENT_MISSION_DEBUG(FM_CONTENT_DEBUG_LOSE_WANTED_GANG, sMission)	
		EXIT
	ENDIF

	// J-Skip
	IF IS_DEBUG_KEY_JUST_RELEASED(KEY_J, KEYBOARD_MODIFIER_NONE, "J Skip")
	OR sDebugVars.bSimulateJSkip
		sDebugVars.bSimulateJSkip = FALSE
		
		IF NOT sDebugVars.bBlockCheatTickers
			SCRIPT_EVENT_DATA_TICKER_MESSAGE sData
			sData.TickerEvent = TICKER_EVENT_PLAYER_CHEATED_JSKIP
			BROADCAST_TICKER_EVENT(sData, ALL_PLAYERS_ON_SCRIPT())
		ENDIF
		
		IF logic.Debug.OverrideJSkip != NULL
		AND CALL logic.Debug.OverrideJSkip()
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - KEY_J released. Default J-Skip has been overridden.")
			EXIT
		ENDIF
		
		VECTOR vCoords
		FLOAT fHeading
		INT i
		INT iPed
		PED_INDEX pedId
		VEHICLE_INDEX vehId
		
		#IF MAX_NUM_DROPOFFS
		VECTOR vDropoffCoords = GET_DROPOFF_COORDS()
		#ENDIF
		
		UNUSED_PARAMETER(i)
		
		SWITCH GET_CLIENT_MODE_STATE()
			CASE eMODESTATE_GO_TO_POINT
				#IF MAX_NUM_GOTO_LOCATIONS
				NET_WARP_TO_COORD(data.GoToPoint.Locations[GET_CURRENT_GO_TO_POINT()].vCoords, 0.0, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - KEY_J released. Warped to ", data.GoToPoint.Locations[GET_CURRENT_GO_TO_POINT()].vCoords, " for eMODESTATE_GO_TO_POINT.")
				#ENDIF
			BREAK
			
			CASE eMODESTATE_COLLECT_MISSION_ENTITY
				IF WARP_INTO_MISSION_ENTITY_CARRIER_VEHICLE()
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - KEY_J released. Warped into carrier vehicle for eMODESTATE_COLLECT_MISSION_ENTITY.")
				ELSE
					vCoords = GET_MISSION_ENTITY_WARP_COORD(FALSE)
					NET_WARP_TO_COORD(vCoords, 0.0, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - KEY_J released. Warped to ", vCoords, " for eMODESTATE_COLLECT_MISSION_ENTITY.")
				ENDIF
			BREAK
			
			CASE eMODESTATE_HELP_DELIVER_MISSION_ENTITY
				IF WARP_INTO_MISSION_ENTITY_CARRIER_VEHICLE()
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - KEY_J released. Warped into carrier vehicle for eMODESTATE_HELP_DELIVER_MISSION_ENTITY.")
				ELSE
					vCoords = GET_MISSION_ENTITY_WARP_COORD(TRUE)
					NET_WARP_TO_COORD(vCoords, 0.0, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - KEY_J released. Warped to ", vCoords, " for eMODESTATE_HELP_DELIVER_MISSION_ENTITY.")
				ENDIF
			BREAK
			
			CASE eMODESTATE_DELIVER_MISSION_ENTITY
				#IF MAX_NUM_DROPOFFS
				NET_WARP_TO_COORD(vDropoffCoords, 0.0, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - KEY_J released. Warped to ", vDropoffCoords, " for eMODESTATE_DELIVER_MISSION_ENTITY.")
				#ENDIF
			BREAK
			
			CASE eMODESTATE_COLLECT_PED
				TASK_ENTER_VEHICLE(LOCAL_PED_INDEX, NET_TO_VEH(DEBUG_GET_CARRIER_VEHICLE(TRUE, DEFAULT, TRUE)), DEFAULT, VS_DRIVER, DEFAULT, ECF_WARP_PED)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - eMODESTATE_COLLECT_PED - KEY_J released. Warping player ped into random available carrier vehicle using TASK_ENTER_VEHICLE with ENTER_EXIT_VEHICLE_FLAGS = ECF_WARP_PED.")
			BREAK
			
			CASE eMODESTATE_COLLECT_PED_PICKUP
				iPed = GET_COLLECT_PED_FOR_MISSION_ENTITY(GET_MISSION_ENTITY_PLAYER_IS_HOLDING(LOCAL_PLAYER_INDEX))
				vCoords = CALL logic.Ped.Coords(iPed)
				fHeading = GET_ENTITY_HEADING(NET_TO_PED(serverBD.sPed[iPed].netId))
				
				vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords, fHeading, <<0.0, 5.0, 0.0>>)
				NET_WARP_TO_COORD(vCoords, fHeading - 90.0, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - eMODESTATE_COLLECT_PED_PICKUP - KEY_J released. Warping player to pickup location, vCoords = ", vCoords)
			BREAK
			
			CASE eMODESTATE_COLLECT_PED_SIGNAL
				SET_PED_CLIENT_BIT(GET_COLLECT_PED_FOR_MISSION_ENTITY(GET_MISSION_ENTITY_PLAYER_IS_HOLDING(LOCAL_PLAYER_INDEX)), ePEDCLIENTBITSET_I_HAVE_BEEN_SIGNALED)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - eMODESTATE_COLLECT_PED_PICKUP - KEY_J released. Warping player to pickup location.")
			BREAK
			
			CASE eMODESTATE_COLLECT_PED_WAIT
				iPed = GET_COLLECT_PED_FOR_MISSION_ENTITY(GET_MISSION_ENTITY_PLAYER_IS_HOLDING(LOCAL_PLAYER_INDEX))
				
				IF TAKE_CONTROL_OF_NET_ID(serverBD.sPed[iPed].netId)
					pedId = NET_TO_PED(serverBD.sPed[iPed].netId)
					
					IF NOT IS_PED_INJURED(pedId)
						vehId = NET_TO_VEH(serverBD.sVehicle[GET_MISSION_ENTITY_CARRIER(GET_MISSION_ENTITY_PLAYER_IS_HOLDING(LOCAL_PLAYER_INDEX))].netId)
						TASK_ENTER_VEHICLE(pedId, vehId, DEFAULT, CALL logic.Ped.Task.EnterVehicle.Seat(iPed), DEFAULT, ECF_WARP_PED)
						PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - eMODESTATE_COLLECT_PED_WAIT - KEY_J released. Giving collect ped TASK_ENTER_VEHICLE with ENTER_EXIT_VEHICLE_FLAGS = ECF_WARP_PED.")
					ENDIF
				ENDIF
			BREAK
			
			CASE eMODESTATE_COLLECT_PED_PROTECT
				DEBUG_DO_FOLLOW_ENTITY_J_SKIP()
			BREAK
			
			CASE eMODESTATE_COLLECT_PED_DELIVER
				#IF MAX_NUM_DROPOFFS
				NET_WARP_TO_COORD(vDropoffCoords, 0.0, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - KEY_J released. Warped to ", vDropoffCoords, " for eMODESTATE_COLLECT_PED_DELIVER.")
				#ENDIF
			BREAK
			
			CASE eMODESTATE_TAIL_ENTITY
			CASE eMODESTATE_FOLLOW_ENTITY
				DEBUG_DO_FOLLOW_ENTITY_J_SKIP()
			BREAK
			
			CASE eMODESTATE_INTERACT
				#IF MAX_NUM_INTERACT_LOCATIONS
				i = FIND_NEAREST_INTERACT_LOCATION()
				
				IF i = -1
					i = DEBUG_GET_INTERACT_LOCATION(TRUE)
					
					IF i != -1
						vCoords = CALL logic.Interact.Coords(i)
						fHeading = CALL logic.Interact.Anim.Heading(i)
						
						IF IS_INTERACT_LOCATION_DATA_BIT_SET(i, eINTERACTLOCATIONDATABITSET_CHECK_FACING)
						AND fHeading = 0.0
							fHeading = DEBUG_GET_HEADING_BETWEEN_LOCATIONS_2D(vCoords, CALL logic.Interact.FacingCoords(i))
						ENDIF
						
						NET_WARP_TO_COORD(vCoords, fHeading, DEFAULT, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
					ENDIF
				ELSE
					SET_INTERACT_LOCATION_DONE_CLIENT(i)
				ENDIF
				#ENDIF
			BREAK
			
			CASE eMODESTATE_ARREST_PED
				#IF MAX_NUM_ARREST_PED_SUSPECTS
				i = FIND_NEAREST_ARREST_PED_SUSPECT()
				
				IF i = -1
					i = DEBUG_GET_ARREST_PED_SUSPECT(TRUE)
					
					IF i != -1
						pedId = NET_TO_PED(serverBD.sPed[data.ArrestPed.Suspects[i].iPed].netId)
						
						fHeading = GET_ENTITY_HEADING(pedId)
						vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(pedId, FALSE), fHeading, DEBUG_GET_ARREST_PED_SUSPECT_OFFSET_VECTOR(i))
						
						fHeading = DEBUG_GET_HEADING_BETWEEN_LOCATIONS_2D(vCoords, GET_ENTITY_COORDS(pedId, FALSE))
						
						NET_WARP_TO_COORD(vCoords, fHeading, DEFAULT, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
					ENDIF
				ELSE
					SET_ARREST_PED_ARRESTED_CLIENT(i)
				ENDIF
				#ENDIF
			BREAK
			
			CASE eMODESTATE_CUTSCENE
				#IF MAX_NUM_CUTSCENES
				IF GET_CURRENT_CUTSCENE(LOCAL_PARTICIPANT_INDEX) != -1
					SET_CUTSCENE_DONE_CLIENT(GET_CURRENT_CUTSCENE(LOCAL_PARTICIPANT_INDEX))
				ENDIF
				#ENDIF
			BREAK
			
			CASE eMODESTATE_SEARCH_AREA
				#IF MAX_NUM_SEARCH_AREAS
				IF sSearchArea.iCurrentArea = -1
					i = DEBUG_GET_SEARCH_AREA(TRUE)
					
					IF i != -1
						NET_WARP_TO_COORD(serverBD.sSearchAreaVars.sAreas[i].vRadiusBlipCoord, GET_ENTITY_HEADING(LOCAL_PED_INDEX), DEFAULT, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
					ENDIF
				ELSE
					fHeading = GET_ENTITY_HEADING(LOCAL_PED_INDEX)
					vCoords = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_SEARCH_AREA_TRIGGER_COORDS(sSearchArea.iCurrentArea), fHeading, DEBUG_GET_SEARCH_AREA_COORD_OFFSET_VECTOR(sSearchArea.iCurrentArea))
					
					GET_SAFE_COORD_FOR_PED(vCoords, FALSE, vCoords)
					
					NET_WARP_TO_COORD(vCoords, fHeading, DEFAULT, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
				ENDIF
				#ENDIF
			BREAK
			
			CASE eMODESTATE_COLLECT_PICKUP
				#IF MAX_NUM_PICKUPS
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.sPickup.sPickup[0].netID)
					NET_WARP_TO_COORD(GET_ENTITY_COORDS(NET_TO_ENT(serverBD.sPickup.sPickup[0].netID), FALSE), 0.0, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - KEY_J released. Warped to Pickup 0 for eMODESTATE_COLLECT_PICKUP.")
				ENDIF
				#ENDIF
			BREAK
			
			CASE eMODESTATE_LOSE_THE_COPS
				#IF ENABLE_LOSE_THE_COPS
				GIVE_WANTED_LEVEL_TO_GANG(0, FALSE)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - KEY_J released. Calling GIVE_WANTED_LEVEL_TO_GANG(0, FALSE).")
				#ENDIF
			BREAK
			
			CASE eMODESTATE_WAIT
				SET_DEBUG_LOCAL_BIT(eDEBUGLOCALBITSET_SKIPPED_WAIT)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - KEY_J released. Skipping timer for eMODESTATE_WAIT.")
			BREAK
			
			DEFAULT
				BROADCAST_EVENT_FM_CONTENT_MISSION_DEBUG(FM_CONTENT_DEBUG_J_SKIP, sMission)		
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - KEY_J released. Sending FM_CONTENT_DEBUG_J_SKIP event.")
			BREAK
		ENDSWITCH
		
		SWITCH GET_CLIENT_MODE_STATE()
			CASE eMODESTATE_COLLECT_CHECKPOINTS
				#IF MAX_NUM_CHECKPOINTS
				IF logic.Checkpoints.OverrideCheckpointCoords != NULL
					NET_WARP_TO_COORD(CALL logic.Checkpoints.OverrideCheckpointCoords(playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId),0.0, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - KEY_J released. Calling NET_WARP_TO_COORD to iCurrentCheckpointId ",playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId," ",VECTOR_TO_STRING(CALL logic.Checkpoints.OverrideCheckpointCoords(playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId)))
				ELSE
					NET_WARP_TO_COORD(data.Checkpoint.Checkpoints[playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId].vCoords,0.0, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT,TRUE)
					PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] MAINTAIN_DEBUG - KEY_J released. Calling NET_WARP_TO_COORD to iCurrentCheckpointId ",playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId," ",VECTOR_TO_STRING(data.Checkpoint.Checkpoints[playerBD[LOCAL_PARTICIPANT_INDEX_AS_INT].iCurrentCheckpointId].vCoords))
				ENDIF
				#ENDIF
			BREAK
		ENDSWITCH
		
		IF logic.Debug.OnJSkip != NULL
			CALL logic.Debug.OnJSkip()
		ENDIF
		
		EXIT
	ENDIF
	
	IF logic.Debug.AdditionalDebug != NULL
		CALL logic.Debug.AdditionalDebug()
	ENDIF
ENDPROC

////////////////////////
////   ENUM NAMES
///////////////////////

FUNC STRING GET_CUTSCENE_HELP_VEHICLE_CLONE_FLAGS_NAME(CUTSCENE_HELP_VEHICLE_CLONE_FLAGS eFlag)
	SWITCH eFlag
		CASE chVEHICLE_CLONE_FREEZE RETURN "chVEHICLE_CLONE_FREEZE"
		CASE chVEHICLE_CLONE_POSTIION_ON_GROUND RETURN "chVEHICLE_CLONE_POSTIION_ON_GROUND"
		CASE chVEHICLE_CLONE_TRAILER RETURN "chVEHICLE_CLONE_TRAILER"
		CASE chVEHICLE_CLONE_FIX_TIRES RETURN "chVEHICLE_CLONE_FIX_TIRES"
		CASE chVEHICLE_CLONE_ONLY_PLAYER_PASSENGERS RETURN "chVEHICLE_CLONE_ONLY_PLAYER_PASSENGERS"
		CASE chVEHICLE_CLONE_DO_NOT_CLONE_PASSENGERS RETURN "chVEHICLE_CLONE_DO_NOT_CLONE_PASSENGERS"
		CASE chVEHICLE_CLONE_DEAD_PED_PASSENGERS RETURN "chVEHICLE_CLONE_DEAD_PED_PASSENGERS"
		CASE chVEHICLE_CLONE_WAIT_FOR_HEAD_BLEND RETURN "chVEHICLE_CLONE_WAIT_FOR_HEAD_BLEND"
		CASE chVEHICLE_CLONE_DO_NOT_LINK_BLENDS RETURN "chVEHICLE_CLONE_DO_NOT_LINK_BLENDS"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING GET_IK_CONTROL_FLAG_NAME(IK_CONTROL_FLAGS eFlag)
	SWITCH eFlag
		CASE AIK_NONE RETURN "AIK_NONE"
		CASE AIK_DISABLE_LEG_IK RETURN "AIK_DISABLE_LEG_IK"
		CASE AIK_DISABLE_ARM_IK RETURN "AIK_DISABLE_ARM_IK"
		CASE AIK_DISABLE_HEAD_IK RETURN "AIK_DISABLE_HEAD_IK"
		CASE AIK_DISABLE_TORSO_IK RETURN "AIK_DISABLE_TORSO_IK"
		CASE AIK_DISABLE_TORSO_REACT_IK RETURN "AIK_DISABLE_TORSO_REACT_IK"
		CASE AIK_USE_LEG_ALLOW_TAGS RETURN "AIK_USE_LEG_ALLOW_TAGS"
		CASE AIK_USE_LEG_BLOCK_TAGS RETURN "AIK_USE_LEG_BLOCK_TAGS"
		CASE AIK_USE_ARM_ALLOW_TAGS RETURN "AIK_USE_ARM_ALLOW_TAGS"
		CASE AIK_USE_ARM_BLOCK_TAGS RETURN "AIK_USE_ARM_BLOCK_TAGS"
		CASE AIK_PROCESS_WEAPON_HAND_GRIP RETURN "AIK_PROCESS_WEAPON_HAND_GRIP"
		CASE AIK_USE_FP_ARM_LEFT RETURN "AIK_USE_FP_ARM_LEFT"
		CASE AIK_USE_FP_ARM_RIGHT RETURN "AIK_USE_FP_ARM_RIGHT"
		CASE AIK_DISABLE_TORSO_VEHICLE_IK RETURN "AIK_DISABLE_TORSO_VEHICLE_IK"
		CASE AIK_LINKED_FACIAL RETURN "AIK_LINKED_FACIAL"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING GET_CHECKPOINT_TYPE_NAME(CHECKPOINT_TYPE eCheckpointType)
	SWITCH eCheckpointType
		// ground races - positioned at base of cylinder
		CASE CHECKPOINT_RACE_GROUND_CHEVRON_1_BASE RETURN "CHECKPOINT_RACE_GROUND_CHEVRON_1_BASE"
		CASE CHECKPOINT_RACE_GROUND_CHEVRON_2_BASE RETURN "CHECKPOINT_RACE_GROUND_CHEVRON_2_BASE"
		CASE CHECKPOINT_RACE_GROUND_CHEVRON_3_BASE RETURN "CHECKPOINT_RACE_GROUND_CHEVRON_3_BASE"
		CASE CHECKPOINT_RACE_GROUND_LAP_BASE RETURN "CHECKPOINT_RACE_GROUND_LAP_BASE"
		CASE CHECKPOINT_RACE_GROUND_FLAG_BASE RETURN "CHECKPOINT_RACE_GROUND_FLAG_BASE"
		CASE CHECKPOINT_RACE_GROUND_PIT_LANE_BASE RETURN "CHECKPOINT_RACE_GROUND_PIT_LANE_BASE"
		// ground races - positioned at centre of chevron/flag etc			
		CASE CHECKPOINT_RACE_GROUND_CHEVRON_1 RETURN "CHECKPOINT_RACE_GROUND_CHEVRON_1"								
		CASE CHECKPOINT_RACE_GROUND_CHEVRON_2 RETURN "CHECKPOINT_RACE_GROUND_CHEVRON_2"								
		CASE CHECKPOINT_RACE_GROUND_CHEVRON_3 RETURN "CHECKPOINT_RACE_GROUND_CHEVRON_3"								
		CASE CHECKPOINT_RACE_GROUND_LAP RETURN "CHECKPOINT_RACE_GROUND_LAP"									
		CASE CHECKPOINT_RACE_GROUND_FLAG RETURN "CHECKPOINT_RACE_GROUND_FLAG"
		CASE CHECKPOINT_RACE_GROUND_PIT_LANE RETURN "CHECKPOINT_RACE_GROUND_PIT_LANE"
		// air races
		CASE CHECKPOINT_RACE_AIR_CHEVRON_1 RETURN "CHECKPOINT_RACE_AIR_CHEVRON_1"
		CASE CHECKPOINT_RACE_AIR_CHEVRON_2 RETURN "CHECKPOINT_RACE_AIR_CHEVRON_2"
		CASE CHECKPOINT_RACE_AIR_CHEVRON_3 RETURN "CHECKPOINT_RACE_AIR_CHEVRON_3"
		CASE CHECKPOINT_RACE_AIR_LAP RETURN "CHECKPOINT_RACE_AIR_LAP"
		CASE CHECKPOINT_RACE_AIR_FLAG RETURN "CHECKPOINT_RACE_AIR_FLAG"
		// water races
		CASE CHECKPOINT_RACE_WATER_CHEVRON_1 RETURN "CHECKPOINT_RACE_WATER_CHEVRON_1"
		CASE CHECKPOINT_RACE_WATER_CHEVRON_2 RETURN "CHECKPOINT_RACE_WATER_CHEVRON_2"
		CASE CHECKPOINT_RACE_WATER_CHEVRON_3 RETURN "CHECKPOINT_RACE_WATER_CHEVRON_3"
		CASE CHECKPOINT_RACE_WATER_LAP RETURN "CHECKPOINT_RACE_WATER_LAP"
		CASE CHECKPOINT_RACE_WATER_FLAG RETURN "CHECKPOINT_RACE_WATER_FLAG"
		// triathlon running races
		CASE CHECKPOINT_RACE_TRI_RUN_CHEVRON_1 RETURN "CHECKPOINT_RACE_TRI_RUN_CHEVRON_1"
		CASE CHECKPOINT_RACE_TRI_RUN_CHEVRON_2 RETURN "CHECKPOINT_RACE_TRI_RUN_CHEVRON_2"
		CASE CHECKPOINT_RACE_TRI_RUN_CHEVRON_3 RETURN "CHECKPOINT_RACE_TRI_RUN_CHEVRON_3"
		CASE CHECKPOINT_RACE_TRI_RUN_LAP RETURN "CHECKPOINT_RACE_TRI_RUN_LAP"
		CASE CHECKPOINT_RACE_TRI_RUN_FLAG RETURN "CHECKPOINT_RACE_TRI_RUN_FLAG"
		// triathlon swimming races
		CASE CHECKPOINT_RACE_TRI_SWIM_CHEVRON_1 RETURN "CHECKPOINT_RACE_TRI_SWIM_CHEVRON_1"
		CASE CHECKPOINT_RACE_TRI_SWIM_CHEVRON_2 RETURN "CHECKPOINT_RACE_TRI_SWIM_CHEVRON_2"
		CASE CHECKPOINT_RACE_TRI_SWIM_CHEVRON_3 RETURN "CHECKPOINT_RACE_TRI_SWIM_CHEVRON_3"
		CASE CHECKPOINT_RACE_TRI_SWIM_LAP RETURN "CHECKPOINT_RACE_TRI_SWIM_LAP"
		CASE CHECKPOINT_RACE_TRI_SWIM_FLAG RETURN "CHECKPOINT_RACE_TRI_SWIM_FLAG"
		// triathlon cycling races
		CASE CHECKPOINT_RACE_TRI_CYCLE_CHEVRON_1 RETURN "CHECKPOINT_RACE_TRI_CYCLE_CHEVRON_1"
		CASE CHECKPOINT_RACE_TRI_CYCLE_CHEVRON_2 RETURN "CHECKPOINT_RACE_TRI_CYCLE_CHEVRON_2"
		CASE CHECKPOINT_RACE_TRI_CYCLE_CHEVRON_3 RETURN "CHECKPOINT_RACE_TRI_CYCLE_CHEVRON_3"
		CASE CHECKPOINT_RACE_TRI_CYCLE_LAP RETURN "CHECKPOINT_RACE_TRI_CYCLE_LAP"
		CASE CHECKPOINT_RACE_TRI_CYCLE_FLAG RETURN "CHECKPOINT_RACE_TRI_CYCLE_FLAG"
		// plane school
		CASE CHECKPOINT_PLANE_FLAT RETURN "CHECKPOINT_PLANE_FLAT"
		CASE CHECKPOINT_PLANE_SIDE_L RETURN "CHECKPOINT_PLANE_SIDE_L"
		CASE CHECKPOINT_PLANE_SIDE_R RETURN "CHECKPOINT_PLANE_SIDE_R"
		CASE CHECKPOINT_PLANE_INVERTED RETURN "CHECKPOINT_PLANE_INVERTED"
		// heli 
		CASE CHECKPOINT_HELI_LANDING RETURN "CHECKPOINT_HELI_LANDING"
		// parachute
		CASE CHECKPOINT_PARACHUTE_RING RETURN "CHECKPOINT_PARACHUTE_RING"
		CASE CHECKPOINT_PARACHUTE_LANDING RETURN "CHECKPOINT_PARACHUTE_LANDING"
		// gang locators
		CASE CHECKPOINT_GANG_LOCATE_LOST RETURN "CHECKPOINT_GANG_LOCATE_LOST"
		CASE CHECKPOINT_GANG_LOCATE_VAGOS RETURN "CHECKPOINT_GANG_LOCATE_VAGOS"
		CASE CHECKPOINT_GANG_LOCATE_COPS RETURN "CHECKPOINT_GANG_LOCATE_COPS"
		// gang locators
		CASE CHECKPOINT_RACE_LOCATE_GROUND RETURN "CHECKPOINT_RACE_LOCATE_GROUND"
		CASE CHECKPOINT_RACE_LOCATE_AIR RETURN "CHECKPOINT_RACE_LOCATE_AIR"
		CASE CHECKPOINT_RACE_LOCATE_WATER RETURN "CHECKPOINT_RACE_LOCATE_WATER"
		// gang locators
		CASE CHECKPOINT_MP_LOCATE_1 RETURN "CHECKPOINT_MP_LOCATE_1"
		CASE CHECKPOINT_MP_LOCATE_2 RETURN "CHECKPOINT_MP_LOCATE_2"
		CASE CHECKPOINT_MP_LOCATE_3 RETURN "CHECKPOINT_MP_LOCATE_3"
		CASE CHECKPOINT_MP_CREATOR_TRIGGER RETURN "CHECKPOINT_MP_CREATOR_TRIGGER"
		// freemode
		CASE CHECKPOINT_MONEY RETURN "CHECKPOINT_MONEY"
		// misc
		CASE CHECKPOINT_BEAST RETURN "CHECKPOINT_BEAST"
		// transforms
		CASE CHECKPOINT_TRANSFORM RETURN "CHECKPOINT_TRANSFORM"
		CASE CHECKPOINT_TRANSFORM_PLANE RETURN "CHECKPOINT_TRANSFORM_PLANE"
		CASE CHECKPOINT_TRANSFORM_HELICOPTER RETURN "CHECKPOINT_TRANSFORM_HELICOPTER"
		CASE CHECKPOINT_TRANSFORM_BOAT RETURN "CHECKPOINT_TRANSFORM_BOAT"
		CASE CHECKPOINT_TRANSFORM_CAR RETURN "CHECKPOINT_TRANSFORM_CAR"
		CASE CHECKPOINT_TRANSFORM_BIKE RETURN "CHECKPOINT_TRANSFORM_BIKE"
		CASE CHECKPOINT_TRANSFORM_PUSH_BIKE RETURN "CHECKPOINT_TRANSFORM_PUSH_BIKE"
		CASE CHECKPOINT_TRANSFORM_TRUCK RETURN "CHECKPOINT_TRANSFORM_TRUCK"
		CASE CHECKPOINT_TRANSFORM_PARACHUTE RETURN "CHECKPOINT_TRANSFORM_PARACHUTE"
		CASE CHECKPOINT_TRANSFORM_THRUSTER RETURN "CHECKPOINT_TRANSFORM_THRUSTER"
		// warps
		CASE CHECKPOINT_WARP RETURN "CHECKPOINT_TRANSFORM_THRUSTER"
	ENDSWITCH
	
	RETURN "***INVALID***" 
ENDFUNC

FUNC STRING GET_HUD_ORDER_NAME(HUDORDER eHUDOrder)
	SWITCH eHUDOrder
		CASE HUDORDER_NOTDISPLAYING 				RETURN "HUDORDER_NOTDISPLAYING"
		CASE HUDORDER_FREEROAM 						RETURN "HUDORDER_FREEROAM"
		CASE HUDORDER_DONTCARE 						RETURN "HUDORDER_DONTCARE"
		CASE HUDORDER_BOTTOM 						RETURN "HUDORDER_BOTTOM"
		CASE HUDORDER_SECONDBOTTOM 					RETURN "HUDORDER_SECONDBOTTOM"
		CASE HUDORDER_THIRDBOTTOM 					RETURN "HUDORDER_THIRDBOTTOM"
		CASE HUDORDER_FOURTHBOTTOM 					RETURN "HUDORDER_FOURTHBOTTOM"
		CASE HUDORDER_FIFTHBOTTOM 					RETURN "HUDORDER_FIFTHBOTTOM"
		CASE HUDORDER_SIXTHBOTTOM 					RETURN "HUDORDER_SIXTHBOTTOM"
		CASE HUDORDER_SEVENTHBOTTOM 				RETURN "HUDORDER_SEVENTHBOTTOM"
		CASE HUDORDER_EIGHTHBOTTOM 					RETURN "HUDORDER_EIGHTHBOTTOM"
		CASE HUDORDER_NINETHBOTTOM 					RETURN "HUDORDER_NINETHBOTTOM"
		CASE HUDORDER_TENTHBOTTOM 					RETURN "HUDORDER_TENTHBOTTOM"
		CASE HUDORDER_TOP 							RETURN "HUDORDER_TOP"
	ENDSWITCH
	
	RETURN "***INVALID***" 
ENDFUNC

FUNC STRING GET_PLACEMENT_FLAG_NAME(PLACEMENT_FLAG ePlacementFlag)
	SWITCH ePlacementFlag
		CASE PLACEMENT_FLAG_MAP 								RETURN "PLACEMENT_FLAG_MAP"
		CASE PLACEMENT_FLAG_FIXED 								RETURN "PLACEMENT_FLAG_FIXED"
		CASE PLACEMENT_FLAG_REGENERATES 						RETURN "PLACEMENT_FLAG_REGENERATES"
		CASE PLACEMENT_FLAG_SNAP_TO_GROUND 						RETURN "PLACEMENT_FLAG_SNAP_TO_GROUND"
		CASE PLACEMENT_FLAG_ORIENT_TO_GROUND 					RETURN "PLACEMENT_FLAG_ORIENT_TO_GROUND"
		CASE PLACEMENT_FLAG_LOCAL_ONLY 							RETURN "PLACEMENT_FLAG_LOCAL_ONLY"
		CASE PLACEMENT_FLAG_BLIPPED_SIMPLE 						RETURN "PLACEMENT_FLAG_BLIPPED_SIMPLE"
		CASE PLACEMENT_FLAG_BLIPPED_COMPLEX 					RETURN "PLACEMENT_FLAG_BLIPPED_COMPLEX"
		CASE PLACEMENT_FLAG_UPRIGHT 							RETURN "PLACEMENT_FLAG_UPRIGHT"
		CASE PLACEMENT_FLAG_ROTATE 								RETURN "PLACEMENT_FLAG_ROTATE"
		CASE PLACEMENT_FLAG_FACEPLAYER 							RETURN "PLACEMENT_FLAG_FACEPLAYER"
		CASE PLACEMENT_FLAG_HIDE_IN_PHOTOS 						RETURN "PLACEMENT_FLAG_HIDE_IN_PHOTOS"
		CASE PLACEMENT_FLAG_PLAYER_GIFT 						RETURN "PLACEMENT_FLAG_PLAYER_GIFT"
		CASE PLACEMENT_FLAG_ON_OBJECT 							RETURN "PLACEMENT_FLAG_ON_OBJECT"
		CASE PLACEMENT_FLAG_GLOW_IN_TEAM 						RETURN "PLACEMENT_FLAG_GLOW_IN_TEAM"
		CASE PLACEMENT_CREATION_FLAG_AUTO_EQUIP 				RETURN "PLACEMENT_CREATION_FLAG_AUTO_EQUIP"
		CASE PLACEMENT_CREATION_FLAG_COLLECTABLE_IN_VEHICLE 	RETURN "PLACEMENT_CREATION_FLAG_COLLECTABLE_IN_VEHICLE"
		CASE PLACEMENT_CREATION_FLAG_DISABLE_WEAPON_HD_MODEL 	RETURN "PLACEMENT_CREATION_FLAG_DISABLE_WEAPON_HD_MODEL"
		CASE PLACEMENT_CREATION_FLAG_FORCE_DEFERRED_MODEL 		RETURN "PLACEMENT_CREATION_FLAG_FORCE_DEFERRED_MODEL"
	ENDSWITCH
	
	RETURN "***INVALID***" 
ENDFUNC

FUNC STRING GET_COMBAT_MOVEMENT_TYPE_NAME(COMBAT_MOVEMENT eBit)
	SWITCH eBit
		CASE CM_STATIONARY RETURN "CM_STATIONARY"
		CASE CM_DEFENSIVE RETURN "CM_DEFENSIVE"
		CASE CM_WILLADVANCE RETURN "CM_WILLADVANCE"
		CASE CM_WILLRETREAT RETURN "CM_WILLRETREAT"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING GET_NEWLOADSCENE_FLAG_NAME(NEWLOADSCENE_FLAGS flag)
	SWITCH flag
		CASE NEWLOADSCENE_FLAG_REQUIRE_COLLISION 		RETURN "NEWLOADSCENE_FLAG_REQUIRE_COLLISION"
		CASE NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE 		RETURN "NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE"
		CASE NEWLOADSCENE_FLAG_INTERIOR_AND_EXTERIOR 	RETURN "NEWLOADSCENE_FLAG_INTERIOR_AND_EXTERIOR"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING GET_VEHICLE_SEAT_NAME(VEHICLE_SEAT thisSeat)
	SWITCH thisSeat
		CASE VS_ANY_PASSENGER	RETURN "VS_ANY_PASSENGER"
		CASE VS_DRIVER			RETURN "VS_DRIVER"			
		CASE VS_FRONT_RIGHT		RETURN "VS_FRONT_RIGHT"			
		CASE VS_BACK_LEFT		RETURN "VS_BACK_LEFT"				
		CASE VS_BACK_RIGHT		RETURN "VS_BACK_RIGHT"
		CASE VS_EXTRA_LEFT_1	RETURN "VS_EXTRA_LEFT_1"
		CASE VS_EXTRA_RIGHT_1	RETURN "VS_EXTRA_RIGHT_1"
		CASE VS_EXTRA_LEFT_2	RETURN "VS_EXTRA_LEFT_2"
		CASE VS_EXTRA_RIGHT_2	RETURN "VS_EXTRA_RIGHT_2"
		CASE VS_EXTRA_LEFT_3	RETURN "VS_EXTRA_LEFT_3"
		CASE VS_EXTRA_RIGHT_3	RETURN "VS_EXTRA_RIGHT_3"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING GET_VEHICLE_DOOR_NAME(SC_DOOR_LIST eDoor)
	SWITCH eDoor
		CASE SC_DOOR_INVALID					RETURN "SC_DOOR_INVALID"
	    CASE SC_DOOR_FRONT_LEFT					RETURN "SC_DOOR_FRONT_LEFT"
	    CASE SC_DOOR_FRONT_RIGHT				RETURN "SC_DOOR_FRONT_RIGHT"
	    CASE SC_DOOR_REAR_LEFT					RETURN "SC_DOOR_REAR_LEFT"
	    CASE SC_DOOR_REAR_RIGHT					RETURN "SC_DOOR_REAR_RIGHT"
	    CASE SC_DOOR_BONNET						RETURN "SC_DOOR_BONNET"
	    CASE SC_DOOR_BOOT						RETURN "SC_DOOR_BOOT"
		CASE SC_MAX_DOORS						RETURN "SC_MAX_DOORS"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

////////////////////////
//// DATA ARRAY PRINTS
///////////////////////

PROC PRINT_BLIP_DATA(MISSION_PLACEMENT_DATA_BLIP sBlip, STRING sLocation, STRING path, STRING element, INT iArrayIndex = -1)
	// Define path
	TEXT_LABEL_63 tlPath = path
	
	IF iArrayIndex > -1
		// add array index to path
		tlPath += "["
		tlPath += iArrayIndex
		tlPath += "]"
	ENDIF
	
	tlPath += "."
	tlPath += element
	
	// Print information
	PRINTLN("[",scriptName,"] - [DATA] - BLIP_OPTIONS(",sLocation,")")
	PRINTLN("[",scriptName,"] - [DATA] - ",tlPath,".eSprite = ", ENUM_TO_INT(sBlip.eSprite))
	PRINTLN("[",scriptName,"] - [DATA] - ",tlPath,".eHudColour = ", ENUM_TO_INT(sBlip.eHudColour))
	PRINTLN("[",scriptName,"] - [DATA] - ",tlPath,".fScale = ", sBlip.fScale)
	PRINTLN("[",scriptName,"] - [DATA] - ",tlPath,".bShowHeight = ", sBlip.bShowHeight)
ENDPROC

PROC PRINT_MISSION_DATA_BIT()
	INT iBit
	REPEAT eDATABITSET_END iBit
		IF IS_DATA_BIT_SET(INT_TO_ENUM(eDATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.iBitset - ", GET_DATA_BIT_NAME(INT_TO_ENUM(eDATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT
ENDPROC

#IF MAX_NUM_FLARES
PROC PRINT_FLARE_DATA_BIT(INT iFlare)
	INT iBit
	REPEAT eFLAREDATABITSET_END iBit
		IF IS_FLARE_DATA_BIT_SET(iFlare, INT_TO_ENUM(eFLARE_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.Flare[",iFlare,"].iBitset - ", GET_FLARE_DATA_BIT_NAME(INT_TO_ENUM(eFLARE_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

#IF MAX_NUM_GOTO_LOCATIONS
PROC PRINT_GO_TO_POINT_DATA_BIT(INT iGoTo)
	INT iBit
	REPEAT eGOTOPOINTDATABITSET_END iBit
		IF IS_GO_TO_POINT_DATA_BIT_SET(iGoTo, INT_TO_ENUM(eGO_TO_POINT_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.GoToPoint[",iGoTo,"].iBitset - ", GET_GO_TO_POINT_DATA_BIT_NAME(INT_TO_ENUM(eGO_TO_POINT_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

PROC PRINT_MISSION_DATA_MISSION_ENTITY_BIT(INT iMissionEntity)
	INT iBit
	REPEAT eMISSIONENTITYDATABITSET_END iBit
		IF IS_MISSION_ENTITY_DATA_BIT_SET(iMissionEntity, INT_TO_ENUM(eMISSION_ENTITY_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.MissionEntity.MissionEntities[",iMissionEntity,"].iBitset - ", GET_MISSION_ENTITY_DATA_BIT_NAME(INT_TO_ENUM(eMISSION_ENTITY_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT	
ENDPROC

PROC PRINT_PED_DATA_BIT(INT iPed)
	INT iBit
	REPEAT ePEDDATABITSET_END iBit
		IF IS_PED_DATA_BIT_SET(iPed, INT_TO_ENUM(ePED_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.Ped[",iPed,"].iBitset - ", GET_PED_DATA_BIT_NAME(INT_TO_ENUM(ePED_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT
ENDPROC

#IF MAX_NUM_ARREST_PED_SUSPECTS
PROC PRINT_ARREST_PED_SUSPECTS_DATA_BIT(INT iSuspect)
	INT iBit
	REPEAT eARRESTPEDDATABITSET_END iBit
		IF IS_ARREST_PED_DATA_BIT_SET(iSuspect, INT_TO_ENUM(eARREST_PED_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.ArrestPed.Suspects[",iSuspect,"].iBitset - ", GET_ARREST_PED_DATA_BIT_NAME(INT_TO_ENUM(eARREST_PED_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT	
ENDPROC
#ENDIF

#IF MAX_NUM_CHECKPOINTS
PROC PRINT_CHECKPOINT_DATA_BIT(INT iCheckpoint)
	INT iBit
	REPEAT eCHECKPOINTLISTDATABITSET_END iBit
		IF IS_ARRAYED_BIT_ENUM_SET(data.Checkpoint.Checkpoints[iCheckpoint].iBitset, INT_TO_ENUM(eCHECKPOINT_LIST_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.Checkpoint.Checkpoints[",iCheckpoint,"].iBitset - ", GET_CHECKPOINT_LIST_DATA_BIT_NAME(INT_TO_ENUM(eCHECKPOINT_LIST_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT	
ENDPROC
#ENDIF

#IF MAX_NUM_CUTSCENES
PROC PRINT_CUTSCENE_DATA_BIT(INT iCutscene)
	INT iBit
	REPEAT eCUTSCENEDATABITSET_END iBit
		IF IS_ARRAYED_BIT_ENUM_SET(data.Cutscene.Scenes[iCutscene].iBitset, INT_TO_ENUM(eCUTSCENE_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.Cutscene.Scenes[",iCutscene,"].iBitset - ", GET_CUTSCENE_DATA_BIT_NAME(INT_TO_ENUM(eCUTSCENE_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT	
ENDPROC

CONST_INT TOTAL_NUM_CUTSCENE_LOADSCENE_FLAGS 3

PROC PRINT_GET_CUTSCENE_LOADSCENE_FLAGS(INT iCutscene)
	INT iFlag, iInterator
	REPEAT TOTAL_NUM_CUTSCENE_LOADSCENE_FLAGS iInterator // IK_CONTROL_FLAGS num of entries
		iFlag = BIT_TO_INT(iInterator)	
		IF IS_BITMASK_ENUM_AS_ENUM_SET(data.Cutscene.Scenes[iCutscene].eLoadSceneFlags, INT_TO_ENUM(NEWLOADSCENE_FLAGS, iFlag))
			PRINTLN("[",scriptName,"] - [DATA] - data.Cutscene.Scenes[",iCutscene,"].eLoadSceneFlags = ", GET_NEWLOADSCENE_FLAG_NAME(INT_TO_ENUM(NEWLOADSCENE_FLAGS, iFlag)))
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

#IF MAX_NUM_INTERACT_LOCATIONS
PROC PRINT_INTERACT_LOCATION_DATA_BIT(INT iLocation)
	INT iBit
	REPEAT eINTERACTLOCATIONDATABITSET_END iBit
		IF IS_INTERACT_LOCATION_DATA_BIT_SET(iLocation, INT_TO_ENUM(eINTERACT_LOCATION_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.Interact.Locations[",iLocation,"].iBitset - ", GET_INTERACT_LOCATION_DATA_BIT_NAME(INT_TO_ENUM(eINTERACT_LOCATION_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT	
ENDPROC
#ENDIF

#IF MAX_NUM_MOVING_DOORS
PROC PRINT_MOVING_DOORS_DATA_BIT(INT iMovingDoor)
	INT iBit
	REPEAT eMOVINGDOORDATABITSET_END iBit
		IF IS_MOVING_DOOR_DATA_BIT_SET(iMovingDoor, INT_TO_ENUM(eMOVING_DOORS_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.MovingDoor.MovingDoors[",iMovingDoor,"].iBitset - ", GET_MOVING_DOORS_DATA_BIT_NAME(INT_TO_ENUM(eMOVING_DOORS_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT	
ENDPROC
#ENDIF

#IF MAX_NUM_AMBIENT_SPEECH_PEDS
PROC PRINT_AMBIENT_SPEECH_PEDS_DATA_BIT(INT iAmbientSpeechPed)
	INT iBit
	REPEAT eAMBIENTSPEECHDATABITSET_END iBit
		IF IS_AMBIENT_SPEECH_DATA_BIT_SET(iAmbientSpeechPed, INT_TO_ENUM(eAMBIENT_SPEECH_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.Ped.AmbientSpeech[",iAmbientSpeechPed,"].iBitset - ", GET_AMBIENT_SPEECH_DATA_BIT_NAME(INT_TO_ENUM(eAMBIENT_SPEECH_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT	
ENDPROC
#ENDIF

#IF MAX_NUM_PICKUPS
PROC PRINT_MISSION_PICKUP_DATA_BIT(INT iPickup)
	INT iBit
	REPEAT eMISSIONPICKUPDATABITSET_END iBit
		IF IS_MISSION_PICKUP_DATA_BIT_SET(iPickup, INT_TO_ENUM(eMISSION_PICKUP_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.Pickup.Pickups[",iPickup,"].iBitset - ", GET_MISSION_PICKUP_DATA_BIT_NAME(INT_TO_ENUM(eMISSION_PICKUP_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

PROC PRINT_PROP_DATA_BIT(INT iProp)
	INT iBit
	REPEAT ePROPDATABITSET_END iBit
		IF IS_PROP_DATA_BIT_SET(iProp, INT_TO_ENUM(ePROP_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.Prop[",iProp,"].iBitset - ", GET_PROP_DATA_BIT_NAME(INT_TO_ENUM(ePROP_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT
ENDPROC

PROC PRINT_PORTAL_DATA_BIT(INT iPortal)
	INT iBit
	REPEAT ePORTALDATABITSET_END iBit
		IF IS_PORTAL_DATA_BIT_SET(iPortal, INT_TO_ENUM(ePORTAL_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.Portal[",iPortal,"].iBitset - ", GET_PORTAL_DATA_BIT_NAME(INT_TO_ENUM(ePORTAL_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT
ENDPROC

#IF MAX_NUM_SEARCH_AREAS
PROC PRINT_SEARCH_AREA_DATA_BIT(INT iSearchArea)
	INT iBit
	REPEAT eSEARCHAREADATABITSET_END iBit
		IF IS_SEARCH_AREA_DATA_BIT_SET(iSearchArea, INT_TO_ENUM(eSEARCH_AREA_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.SearchArea[",iSearchArea,"].iBitset - ", GET_SEARCH_AREA_DATA_BIT_NAME(INT_TO_ENUM(eSEARCH_AREA_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

#IF MAX_NUM_SECURITY_CAMERAS
PROC PRINT_SECURITY_CAMERA_DATA_BIT(INT iSecurityCamera)
	INT iBit
	REPEAT eSECURITYCAMERADATABITSET_END iBit
		IF IS_SECURITY_CAMERA_DATA_BIT_SET(iSecurityCamera, INT_TO_ENUM(eSECURITY_CAMERA_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.SecurityCamera[",iSecurityCamera,"].iBitset - ", GET_SECURITY_CAMERA_DATA_BIT_NAME(INT_TO_ENUM(eSECURITY_CAMERA_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

PROC PRINT_TRIGGER_AREA_DATA_BIT(INT iArea)
	INT iBit
	REPEAT eTRIGGERAREADATABITSET_END iBit
		IF IS_TRIGGER_AREA_DATA_BIT_SET(iArea, INT_TO_ENUM(eTRIGGER_AREA_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.TriggerArea[",iArea,"].iBitset - ", GET_TRIGGER_AREA_DATA_BIT_NAME(INT_TO_ENUM(eTRIGGER_AREA_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT
ENDPROC

#IF MAX_NUM_CUTSCENES
CONST_INT TOTAL_NUM_IK_CONTROL_FLAGS 15
CONST_INT TOTAL_NUM_CUTSCENE_HELP_VEHICLE_CLONE_FLAGS 9

#IF MAX_NUM_CUTSCENE_PLAYERS

PROC PRINT_CUTSCENE_PLAYERCLONE_IK_CONTROL_FLAG(INT iLoop, INT iLoop2)
	INT iFlag, iInterator
	REPEAT TOTAL_NUM_IK_CONTROL_FLAGS iInterator // IK_CONTROL_FLAGS num of entries
		iFlag = BIT_TO_INT(iInterator)
		IF IS_IK_CONTROL_FLAG_SET(data.Cutscene.Scenes[iLoop].PlayerClones[iLoop2].eIKControlFlags, INT_TO_ENUM(IK_CONTROL_FLAGS, iFlag))
			PRINTLN("[",scriptName,"] - [DATA] - data.Cutscene.Scenes[",iLoop,"].PlayerClones[",iLoop2,"].eIKControlFlags = ", GET_IK_CONTROL_FLAG_NAME(INT_TO_ENUM(IK_CONTROL_FLAGS, iFlag)))
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF
#IF MAX_NUM_CUTSCENE_VEHICLES
PROC PRINT_CUTSCENE_VEHICLECLONE_FLAGS(INT iLoop, INT iLoop2)
	INT iFlag, iInterator
	REPEAT TOTAL_NUM_CUTSCENE_HELP_VEHICLE_CLONE_FLAGS iInterator // IK_CONTROL_FLAGS num of entries
		iFlag = BIT_TO_INT(iInterator)	
		IF IS_CUTSCENE_HELP_VEHICLE_CLONE_FLAG_SET(data.Cutscene.Scenes[iLoop].VehicleClones[iLoop2].eVehicleCloneFlags, INT_TO_ENUM(CUTSCENE_HELP_VEHICLE_CLONE_FLAGS, iFlag))
			PRINTLN("[",scriptName,"] - [DATA] - data.Cutscene.Scenes[",iLoop,"].VehicleClones[",iLoop2,"].eVehicleCloneFlags = ", GET_CUTSCENE_HELP_VEHICLE_CLONE_FLAGS_NAME(INT_TO_ENUM(CUTSCENE_HELP_VEHICLE_CLONE_FLAGS, iFlag)))
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF
#IF MAX_NUM_CUTSCENE_VEHICLE_PASSENGERS
PROC PRINT_CUTSCENE_VEHICLECLONE_PASENGER_IK_CONTROL_FLAG(INT iLoop, INT iLoop2, INT iLoop3)
	INT iFlag, iInterator
	REPEAT TOTAL_NUM_IK_CONTROL_FLAGS iInterator // IK_CONTROL_FLAGS num of entries
		iFlag = BIT_TO_INT(iInterator)	
		IF IS_IK_CONTROL_FLAG_SET(data.Cutscene.Scenes[iLoop].VehicleClones[iLoop2].sPassengers[iLoop3].eIKControlFlags, INT_TO_ENUM(IK_CONTROL_FLAGS, iFlag))
			PRINTLN("[",scriptName,"] - [DATA] - data.Cutscene.Scenes[",iLoop,"].VehicleClones[",iLoop2,"].sPassengers[",iLoop3,"].eIKControlFlags = ", GET_IK_CONTROL_FLAG_NAME(INT_TO_ENUM(IK_CONTROL_FLAGS, iFlag)))
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF
#IF MAX_NUM_CUTSCENE_PEDS
PROC PRINT_CUTSCENE_PEDCLONE_IK_CONTROL_FLAG(INT iLoop, INT iLoop2)
	INT iFlag, iInterator
	REPEAT TOTAL_NUM_IK_CONTROL_FLAGS iInterator // IK_CONTROL_FLAGS num of entries
		iFlag = BIT_TO_INT(iInterator)	
		IF IS_IK_CONTROL_FLAG_SET(data.Cutscene.Scenes[iLoop].PedClones[iLoop2].eIKControlFlags, INT_TO_ENUM(IK_CONTROL_FLAGS, iFlag))
			PRINTLN("[",scriptName,"] - [DATA] - data.Cutscene.Scenes[",iLoop,"].PedClones[",iLoop2,"].eIKControlFlags = ", GET_IK_CONTROL_FLAG_NAME(INT_TO_ENUM(IK_CONTROL_FLAGS, iFlag)))
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF
#ENDIF // END CUTSCENE

PROC PRINT_VEHICLE_DATA_BIT(INT iVehicle)
	INT iBit
	REPEAT eVEHICLEDATABITSET_END iBit
		IF IS_VEHICLE_DATA_BIT_SET(iVehicle, INT_TO_ENUM(eVEHICLE_DATA_BITSET, iBit))
			PRINTLN("[",scriptName,"] - [DATA] - data.Vehicle[",iVehicle,"].iBitset - ", GET_VEHICLE_DATA_BIT_NAME(INT_TO_ENUM(eVEHICLE_DATA_BITSET, iBit)))
		ENDIF
	ENDREPEAT
ENDPROC

PROC PRINT_VEHICLE_DOOR_STATE_DATA_BIT(INT iVehicle)

	// Original function from GET_VEHICLE_DOOR_STATE_FROM_PLACEMENT_DATA
	// Iterate through all doors of the vehicle
	INT iDoor, iStartBit, iBit
	REPEAT ENUM_TO_INT(SC_MAX_DOORS) iDoor
		// Pick the current door for the iteration
		SWITCH INT_TO_ENUM(SC_DOOR_LIST, iDoor)
			CASE SC_DOOR_FRONT_LEFT		iStartBit = ENUM_TO_INT(eVEHICLEDOORSTATEBITSET_FRONT_LEFT_CLOSED)		BREAK
			CASE SC_DOOR_FRONT_RIGHT	iStartBit = ENUM_TO_INT(eVEHICLEDOORSTATEBITSET_FRONT_RIGHT_CLOSED)		BREAK
			CASE SC_DOOR_REAR_LEFT		iStartBit = ENUM_TO_INT(eVEHICLEDOORSTATEBITSET_REAR_LEFT_CLOSED)		BREAK
			CASE SC_DOOR_REAR_RIGHT		iStartBit = ENUM_TO_INT(eVEHICLEDOORSTATEBITSET_REAR_RIGHT_CLOSED)		BREAK
			CASE SC_DOOR_BONNET			iStartBit = ENUM_TO_INT(eVEHICLEDOORSTATEBITSET_BONNET_CLOSED)			BREAK
			CASE SC_DOOR_BOOT			iStartBit = ENUM_TO_INT(eVEHICLEDOORSTATEBITSET_BOOT_CLOSED)			BREAK
		ENDSWITCH
		
		// Iterate through the door states
		BOOL bFoundEntry = FALSE
		REPEAT ENUM_TO_INT(eVEHICLEDOORSTATE_MAX) iBit
			IF IS_BIT_SET(data.Vehicle.Vehicles[iVehicle].iDoorStateBitSet, iStartBit+iBit)
				PRINTLN("[",scriptName,"] - [DATA] - data.Vehicle[",iVehicle,"].iDoorStateBitSet - ",GET_VEHICLE_DOOR_NAME(INT_TO_ENUM(SC_DOOR_LIST, iDoor))," : ", GET_FMC_VEHICLE_DOOR_STATE_BITSET_NAME(INT_TO_ENUM(FMC_VEHICLE_DOOR_STATE_BITSET, iBit)))
				bFoundEntry = TRUE
				BREAKLOOP
			ENDIF
		ENDREPEAT
		
		IF NOT bFoundEntry
			// Fallback entry
			PRINTLN("[",scriptName,"] - [DATA] - data.Vehicle[",iVehicle,"].iDoorStateBitSet - ",GET_VEHICLE_DOOR_NAME(INT_TO_ENUM(SC_DOOR_LIST, iDoor))," : eVEHICLEDOORSTATE_DEFAULT") 
		ENDIF
	ENDREPEAT
ENDPROC

#IF MAX_NUM_WEAPON_PICKUPS
PROC PRINT_WEAPON_PICKUP_PLACEMENT_FLAG(INT iLoop)
	INT iInterator
	REPEAT COUNT_OF(PLACEMENT_FLAG) iInterator
		IF IS_BIT_SET(data.WeaponPickup[iLoop].iPlacementFlags, iInterator)
			PRINTLN("[",scriptName,"] - [DATA] - data.WeaponPickup[",iLoop,"].iPlacementFlags - ", GET_PLACEMENT_FLAG_NAME(INT_TO_ENUM(PLACEMENT_FLAG, iInterator)))
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

/////////////////////////////////
////   LOGIC FUNCTION POINTER
/////////////////////////////////

PROC PRINT_LOGIC_FUNCTION_POINTERS()

	PRINTLN("[",scriptName,"] - [LOGIC_FP] - ****** START ******")
	
	// --- Core ---
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - **** CORE ****")
	// -- Init --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Init' ***")
	IF logic.Init.Common != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Init.Common' - function pointer defined.")
	ENDIF
	IF logic.Init.Server != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Init.Server' - function pointer defined.")
	ENDIF
	IF logic.Init.Client != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Init.Client' - function pointer defined.")
	ENDIF
	// -- Maintain --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Maintain' ***")
	IF logic.Maintain.Server != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Maintain.Server' - function pointer defined.")
	ENDIF
	IF logic.Maintain.Client != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Maintain.Client' - function pointer defined.")
	ENDIF
	// Reward
	IF logic.Maintain.Rewards.Server != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Rewards.Server' - function pointer defined.")
	ENDIF
	IF logic.Maintain.Rewards.Client != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Rewards.Client' - function pointer defined.")
	ENDIF
	// Participant
	IF logic.Maintain.Participant.PreLoop != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Participant.PreLoop' - function pointer defined.")
	ENDIF
	IF logic.Maintain.Participant.Loop != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Participant.Loop' - function pointer defined.")
	ENDIF
	IF logic.Maintain.Participant.PostLoop != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Participant.PostLoop' - function pointer defined.")
	ENDIF
	// -- Cleanup --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Cleanup' ***")
	IF logic.Cleanup.ScriptEnd != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cleanup.ScriptEnd' - function pointer defined.")
	ENDIF
	// -- States --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.States' ***")
	IF logic.StateMachine.SetupStates != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.StateMachine' - function pointer defined.")
	ENDIF
	IF logic.StateMachine.SetupClientStates != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SetupClientStates' - function pointer defined.")
	ENDIF
	// -- Data --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Data' ***")
	IF logic.Data.Setup != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Data.Setup' - function pointer defined.")
	ENDIF
	// -- Common --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Common' ***")
	IF logic.Common.OnActivate != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Common.OnActivate' - function pointer defined.")
	ENDIF
	// -- Player --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Player' ***")
	IF logic.Player.OnDamaged != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.OnDamaged' - function pointer defined.")
	ENDIF
	IF logic.Player.Rebreather != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Rebreather' - function pointer defined.")
	ENDIF
	IF logic.Player.RebreatherAmount != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.RebreatherAmount' - function pointer defined.")
	ENDIF
	IF logic.Player.ShouldEquipBag != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.ShouldEquipBag' - function pointer defined.")
	ENDIF
	IF logic.Player.DisableAttack != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.DisableAttack' - function pointer defined.")
	ENDIF
	// HeliHover
	IF logic.Player.HeliHover.Allow != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.HeliHover.Allow' - function pointer defined.")
	ENDIF
	IF logic.Player.HeliHover.ShowStopHelp != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.HeliHover.ShowStopHelp' - function pointer defined.")
	ENDIF
	// Weapon
	IF logic.Player.Weapon.ShouldEquip != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Weapon.ShouldEquip' - function pointer defined.")
	ENDIF
	IF logic.Player.Weapon.TickerLabel != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Weapon.TickerLabel' - function pointer defined.")
	ENDIF
	IF logic.Player.Weapon.Type != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Weapon.Type' - function pointer defined.")
	ENDIF
	IF logic.Player.Weapon.AmmoAmount != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Weapon.AmmoAmount' - function pointer defined.")
	ENDIF
	IF logic.Player.Weapon.ForceInHandParam != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Weapon.ForceInHandParam' - function pointer defined.")
	ENDIF
	IF logic.Player.Weapon.bEquipParam != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Weapon.bEquipParam' - function pointer defined.")
	ENDIF
	// Halt
	IF logic.Player.Halt.Enable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Halt.Enable' - function pointer defined.")
	ENDIF
	IF logic.Player.Halt.StoppingDistance != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Halt.StoppingDistance' - function pointer defined.")
	ENDIF
	IF logic.Player.Halt.ForceOutVehicle != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Halt.ForceOutVehicle' - function pointer defined.")
	ENDIF
	IF logic.Player.Halt.ForceOutDelay != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Halt.ForceOutDelay' - function pointer defined.")
	ENDIF
	IF logic.Player.Halt.LockVehicle != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Halt.LockVehicle' - function pointer defined.")
	ENDIF
	// Respawing
	IF logic.Player.Spawning.Respawn.DisablePlayerControlFlags != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Spawning.Respawn.DisablePlayerControlFlags' - function pointer defined.")
	ENDIF
	IF logic.Player.Spawning.Respawn.Location != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Spawning.Respawn.Location' - function pointer defined.")
	ENDIF
	IF logic.Player.Spawning.Respawn.WarpIntoSpawnVehicle != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Spawning.Respawn.WarpIntoSpawnVehicle' - function pointer defined.")
	ENDIF
	IF logic.Player.Spawning.Respawn.KeepCurrentVehicle != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Spawning.Respawn.KeepCurrentVehicle' - function pointer defined.")
	ENDIF
	IF logic.Player.Spawning.Respawn.UseCarNodeOffset != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Spawning.Respawn.UseCarNodeOffset' - function pointer defined.")
	ENDIF
	IF logic.Player.Spawning.Respawn.UseStartOfMissionPVSpawn != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Spawning.Respawn.UseStartOfMissionPVSpawn' - function pointer defined.")
	ENDIF
	// ShouldGhostRemote
	IF logic.Player.ShouldGhostRemote != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.ShouldGhostRemote' - function pointer defined.")
	ENDIF
	// -- HUD --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.HUD' ***")
	IF logic.Hud.ShouldDelayUI != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Hud.ShouldDelayUI' - function pointer defined.")
	ENDIF
	IF logic.Hud.StartBigMessageDelayMS != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Hud.StartBigMessageDelayMS' - function pointer defined.")
	ENDIF
	// Tickers
	IF logic.Hud.Tickers.RepeatCollected != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Hud.Tickers.RepeatCollected' - function pointer defined.")
	ENDIF
	IF logic.Hud.Tickers.CustomStringLocal != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Hud.Tickers.CustomStringLocal' - function pointer defined.")
	ENDIF
	IF logic.Hud.Tickers.CustomStringRemote != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Hud.Tickers.CustomStringRemote' - function pointer defined.")
	ENDIF
	// -
	IF logic.Hud.ShouldCheckTutorialSession != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Hud.ShouldCheckTutorialSession' - function pointer defined.")
	ENDIF
	IF logic.Hud.ShouldSkipDisplayBigMessage != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Hud.ShouldSkipDisplayBigMessage' - function pointer defined.")
	ENDIF
	// -- Reward --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Reward' ***")
	IF logic.Reward.WinnerCash != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Reward.WinnerCash' - function pointer defined.")
	ENDIF
	IF logic.Reward.WinnerRp != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Reward.WinnerRp' - function pointer defined.")
	ENDIF
	IF logic.Reward.MinPartCash != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Reward.MinPartCash' - function pointer defined.")
	ENDIF
	IF logic.Reward.MinPartRp != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Reward.MinPartRp' - function pointer defined.")
	ENDIF
	IF logic.Reward.MinPartTimeCap != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Reward.MinPartTimeCap' - function pointer defined.")
	ENDIF
	IF logic.Reward.Multiplier != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Reward.Multiplier' - function pointer defined.")
	ENDIF
	IF logic.Reward.OnDeliveryCash != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Reward.OnDeliveryCash' - function pointer defined.")
	ENDIF
	IF logic.Reward.OnDeliveryRp != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Reward.OnDeliveryRp' - function pointer defined.")
	ENDIF
	IF logic.Reward.ShouldAdjustWages != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Reward.ShouldAdjustWages' - function pointer defined.")
	ENDIF
	// -- Wanted --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Wanted' ***")
	IF logic.Wanted.Level != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Wanted.Level' - function pointer defined.")
	ENDIF
	IF logic.Wanted.FakeLevel != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Wanted.FakeLevel' - function pointer defined.")
	ENDIF
	IF logic.Wanted.LoseOnDeath != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Wanted.LoseOnDeath' - function pointer defined.")
	ENDIF
	IF logic.Wanted.ShouldApply != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Wanted.ShouldApply' - function pointer defined.")
	ENDIF
	IF logic.Wanted.ShouldBlock != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Wanted.ShouldBlock' - function pointer defined.")
	ENDIF
	IF logic.Wanted.ShouldCap != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Wanted.ShouldCap' - function pointer defined.")
	ENDIF
	IF logic.Wanted.ShouldFake != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Wanted.ShouldFake' - function pointer defined.")
	ENDIF
	IF logic.Wanted.ShouldForce != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Wanted.ShouldForce' - function pointer defined.")
	ENDIF
	IF logic.Wanted.ShouldStore != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Wanted.ShouldStore' - function pointer defined.")
	ENDIF
	IF logic.Wanted.ShouldClearAfterForce != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Wanted.ShouldClearAfterForce' - function pointer defined.")
	ENDIF
	IF logic.Wanted.ShouldLose != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Wanted.ShouldLose' - function pointer defined.")
	ENDIF
	IF logic.Wanted.ShouldLoseWhenInPossession != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Wanted.ShouldLoseWhenInPossession' - function pointer defined.")
	ENDIF
	IF logic.Wanted.ShouldCreateShockingEvent != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Wanted.ShouldCreateShockingEvent' - function pointer defined.")
	ENDIF
	IF logic.Wanted.DelayCondition != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Wanted.DelayCondition' - function pointer defined.")
	ENDIF
	IF logic.Wanted.CopCombatDelay != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Wanted.CopCombatDelay' - function pointer defined.")
	ENDIF
	IF logic.Wanted.KeepWantedLevelOnInterior != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Wanted.KeepWantedLevelOnInterior' - function pointer defined.")
	ENDIF
	// -- Participation --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Participation' ***")
	IF logic.Participation.Coords != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Participation.Coords' - function pointer defined.")
	ENDIF
	IF logic.Participation.MinNumParticipants != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Participation.MinNumParticipants' - function pointer defined.")
	ENDIF
	// -- ModeTimer --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.ModeTimer' ***")
	IF logic.ModeTimer.Enable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ModeTimer.Enable' - function pointer defined.")
	ENDIF
	IF logic.ModeTimer.Restart != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ModeTimer.Restart' - function pointer defined.")
	ENDIF
	IF logic.ModeTimer.NearEndTime != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ModeTimer.NearEndTime' - function pointer defined.")
	ENDIF
	IF logic.ModeTimer.OnExpired != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ModeTimer.OnExpired' - function pointer defined.")
	ENDIF
	// -- FocusCam --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.FocusCam' ***")
	IF logic.FocusCam.Enable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.FocusCam.Enable' - function pointer defined.")
	ENDIF
	IF logic.FocusCam.ForceActive != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.FocusCam.ForceActive' - function pointer defined.")
	ENDIF
	IF logic.FocusCam.AllowOnFoot != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.FocusCam.AllowOnFoot' - function pointer defined.")
	ENDIF
	IF logic.FocusCam.ShouldShowHint != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.FocusCam.ShouldShowHint' - function pointer defined.")
	ENDIF
	IF logic.FocusCam.Prompt != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.FocusCam.Prompt' - function pointer defined.")
	ENDIF
	// -- Phonecall --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Phonecall' ***")
	IF logic.Phonecall.PostMissionCall != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Phonecall.PostMissionCall' - function pointer defined.")
	ENDIF
	// -- Telemetry --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Telemetry' ***")
	IF logic.Telemetry.ShouldIncrementScriptPedsKilled != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Telemetry.ShouldIncrementScriptPedsKilled' - function pointer defined.")
	ENDIF
	IF logic.Telemetry.ShouldIncrementInnocentPedsKilled != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Telemetry.ShouldIncrementInnocentPedsKilled' - function pointer defined.")
	ENDIF
	// -- Audio --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Audio' ***")
	// SoundTrigger
	IF logic.Audio.SoundTrigger.Trigger != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Audio.SoundTrigger.Trigger' - function pointer defined.")
	ENDIF
	IF logic.Audio.SoundTrigger.Play != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Audio.SoundTrigger.Play' - function pointer defined.")
	ENDIF
	IF logic.Audio.SoundTrigger.ShouldLoop != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Audio.SoundTrigger.ShouldLoop' - function pointer defined.")
	ENDIF
	// MusicTrigger
	IF logic.Audio.MusicTrigger.Event != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Audio.MusicTrigger.Event' - function pointer defined.")
	ENDIF
	IF logic.Audio.MusicTrigger.EventString != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Audio.MusicTrigger.EventString' - function pointer defined.")
	ENDIF
	IF logic.Audio.MusicTrigger.StopString != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Audio.MusicTrigger.StopString' - function pointer defined.")
	ENDIF
	IF logic.Audio.MusicTrigger.FailString != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Audio.MusicTrigger.FailString' - function pointer defined.")
	ENDIF
	IF logic.Audio.MusicTrigger.DoSafetyCheck != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Audio.MusicTrigger.DoSafetyCheck' - function pointer defined.")
	ENDIF
	// -- Mission --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Mission' ***")
	IF logic.Mission.Pass != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Mission.Pass' - function pointer defined.")
	ENDIF
	IF logic.Mission.PartialPass != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Mission.PartialPass' - function pointer defined.")
	ENDIF
	IF logic.Mission.PartialPassValues != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Mission.PartialPassValues' - function pointer defined.")
	ENDIF
	IF logic.Mission.Next != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Mission.Next' - function pointer defined.")
	ENDIF
	IF logic.Mission.ShouldQuit != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Mission.ShouldQuit' - function pointer defined.")
	ENDIF
	IF logic.Mission.OnEnd != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Mission.OnEnd' - function pointer defined.")
	ENDIF
	IF logic.Mission.HoldEnd != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Mission.HoldEnd' - function pointer defined.")
	ENDIF
	IF logic.Mission.Coord != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Mission.Coord' - function pointer defined.")
	ENDIF
	IF logic.Mission.WithinRange != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Mission.WithinRange' - function pointer defined.")
	ENDIF
	// -- Waiting --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Waiting' ***")
	IF logic.Waiting.Time != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Waiting.Time' - function pointer defined.")
	ENDIF
	// -- Flare --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Flare' ***")
	IF logic.Flare.Enabled != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Flare.Enabled' - function pointer defined.")
	ENDIF
	IF logic.Flare.AttachEntity != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Flare.AttachEntity' - function pointer defined.")
	ENDIF
	
	// --- Entities ---
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - **** Entities ****")
	// -- MissionEntity --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.MissionEntity' ***")
	// Blip
	IF logic.MissionEntity.Blip.Enable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.Enable' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.EnableArea != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.EnableArea' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.EnableCentre != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.EnableCentre' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.AreaAlpha != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.AreaAlpha' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.Sprite != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.Sprite' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.Colour != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.Colour' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.Scale != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.Scale' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.Priority != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.Priority' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.PlayerScale != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.PlayerScale' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.FlashOnCreation != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.FlashOnCreation' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.FlashOnChange != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.FlashOnChange' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.ConstantFlash != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.ConstantFlash' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.VisionCone != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.VisionCone' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.Name != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.Name' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.ShowHeight != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.ShowHeight' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.DrawGPS != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.DrawGPS' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.ShortRange != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.ShortRange' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.DistanceAlphaData != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.DistanceAlphaData' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.Rotate != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.Rotate' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.DrawMarker != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.DrawMarker' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.MarkerScale != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.MarkerScale' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.MarkerOffset != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.MarkerOffset' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Blip.MarkerYOffset != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Blip.MarkerYOffset' - function pointer defined.")
	ENDIF
	// - Misc -
	IF logic.MissionEntity.ShouldSpawn != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.ShouldSpawn' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.NearRange != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.NearRange' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.CheckLOSNear != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.CheckLOSNear' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.CheckNearZRange != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.CheckNearZRange' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.GetDynamicSpawnCoords != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.GetDynamicSpawnCoords' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.GetPickupType != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.GetPickupType' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Check3dDistance != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Check3dDistance' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.OnCreation != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.OnCreation' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.OnDelivery != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.OnDelivery' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.OnCollect != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.OnCollect' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.OnDestruction != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.OnDestruction' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Client != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Client' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.OnNear != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.OnNear' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.DisableEndOnDestruction != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.DisableEndOnDestruction' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.ShouldAttachToParticipant != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.ShouldAttachToParticipant' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.ShouldBeCollectable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.ShouldBeCollectable' - function pointer defined.")
	ENDIF
	// Rival
	IF logic.MissionEntity.Rival.Visible != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Rival.Visible' - function pointer defined.")
	ENDIF
	IF logic.MissionEntity.Rival.Delay != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MissionEntity.Rival.Delay' - function pointer defined.")
	ENDIF
	// -- PED --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.PED' ***")
	// - Behaviour -
	IF logic.Ped.Behaviour.Setup != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Behaviour.Setup' - function pointer defined.")
	ENDIF
	IF logic.Ped.Behaviour.Get != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Behaviour.Get' - function pointer defined.")
	ENDIF
	// - Task -
	// DoScenario
	IF logic.Ped.Task.DoScenario.ScenarioName != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.DoScenario.ScenarioName' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.DoScenario.Position != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.DoScenario.Position' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.DoScenario.Heading != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.DoScenario.Heading' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.DoScenario.ShouldHeadTrack != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.DoScenario.ShouldHeadTrack' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.DoScenario.PlayAtPosition != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.DoScenario.PlayAtPosition' - fuction pointer defined.")
	ENDIF
	// Enter Vehicle
	IF logic.Ped.Task.EnterVehicle.Vehicle != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.EnterVehicle.Vehicle' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.EnterVehicle.WarpTime != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.EnterVehicle.WarpTime' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.EnterVehicle.Seat != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.EnterVehicle.Seat' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.EnterVehicle.MoveBlendRatio != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.EnterVehicle.MoveBlendRatio' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.EnterVehicle.EnterExitVehicleFlags != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.EnterVehicle.EnterExitVehicleFlags' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.EnterVehicle.OnTask != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.EnterVehicle.OnTask' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.EnterVehicle.CustomVehicle != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.EnterVehicle.CustomVehicle' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.EnterVehicle.LeaveCurrentVehicle != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.EnterVehicle.LeaveCurrentVehicle' - fuction pointer defined.")
	ENDIF
	// PlayAnim
	IF logic.Ped.Task.PlayAnim.AnimDict != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.PlayAnim.AnimDict' - function pointer defined.")
	ENDIF
	IF logic.Ped.Task.PlayAnim.Anim != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.PlayAnim.Anim' - function pointer defined.")
	ENDIF
	IF logic.Ped.Task.PlayAnim.AnimFlags != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.PlayAnim.AnimFlags' - function pointer defined.")
	ENDIF
	// AttackHatedTargets
	IF logic.Ped.Task.AttackHatedTargets.Coords != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.AttackHatedTargets.Coords' - function pointer defined.")
	ENDIF
	IF logic.Ped.Task.AttackHatedTargets.Range != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.AttackHatedTargets.Range' - function pointer defined.")
	ENDIF
	IF logic.Ped.Task.AttackHatedTargets.OnTask != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.AttackHatedTargets.OnTask' - function pointer defined.")
	ENDIF
	// AimAtCoord
	IF logic.Ped.Task.AimAtCoord.Coords != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.AimAtCoord.Coords' - function pointer defined.")
	ENDIF
	// AimAtEntity
	IF logic.Ped.Task.AimAtEntity.Entity != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.AimAtEntity.Entity' - function pointer defined.")
	ENDIF
	// ShootAtTarget
	IF logic.Ped.Task.ShootAtTarget.Entity != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.ShootAtTarget.Entity' - function pointer defined.")
	ENDIF
	IF logic.Ped.Task.ShootAtTarget.FiringType != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.ShootAtTarget.FiringType' - function pointer defined.")
	ENDIF
	// VehicleGoToPoint
	IF logic.Ped.Task.VehicleGoToPoint.NumCoords != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.NumCoords' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.Coords != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.Coords' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.Vehicle != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.Vehicle' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.Speed != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.Speed' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.RadiusServer != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.RadiusServer' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.RadiusClient != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.RadiusClient' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.ShouldUseLongRangeTask != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.ShouldUseLongRangeTask' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.DriveStyle != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.DriveStyle' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.DriveMode != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.DriveMode' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.StraightLineDistance != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.StraightLineDistance' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.MoveBlendRatio != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.MoveBlendRatio' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.TgcamFlags != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.TgcamFlags' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.BoatSettings != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.BoatSettings' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.ShouldDoOnFoot != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.ShouldDoOnFoot' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.ShouldReenterVehicle != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.ShouldReenterVehicle' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.ForwardSpeed != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.ForwardSpeed' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.HeliCruiseSpeed != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.HeliCruiseSpeed' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.HeliFlightHeight != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.HeliFlightHeight' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.HeliMinFlightHeightAboveTerrain != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.HeliMinFlightHeightAboveTerrain' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.HeliOrientation != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.HeliOrientation' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.ShouldClearTasks != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.ShouldClearTasks' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.ShouldUsePedBoneCoords != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.ShouldUsePedBoneCoords' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.MaintainNearCheckpoint != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.MaintainNearCheckpoint' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.OnReachedCheckpoint != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.OnReachedCheckpoint' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.OnCheckpointTimeoutExpired != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.OnCheckpointTimeoutExpired' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.ShouldCheck3dDistance != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.ShouldCheck3dDistance' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleGoToPoint.OnTask != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleGoToPoint.OnTask' - fuction pointer defined.")
	ENDIF
	// CombatPed
	IF logic.Ped.Task.CombatPed.Entity != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.CombatPed.Entity' - function pointer defined.")
	ENDIF
	IF logic.Ped.Task.CombatPed.CombatFlags != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.CombatPed.CombatFlags' - function pointer defined.")
	ENDIF
	// WalkToPoint
	IF logic.Ped.Task.WalkToPoint.UseNavMesh != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.WalkToPoint.UseNavMesh' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.WalkToPoint.Coords != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.WalkToPoint.Coords' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.WalkToPoint.MoveBlendRatio != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.WalkToPoint.MoveBlendRatio' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.WalkToPoint.TimeBeforeWarp != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.WalkToPoint.TimeBeforeWarp' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.WalkToPoint.FinalHeading != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.WalkToPoint.FinalHeading' - fuction pointer defined.")
	ENDIF
	// AchieveHeading
	IF logic.Ped.Task.AchieveHeading.DesiredHeading != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.AchieveHeading.DesiredHeading' - function pointer defined.")
	ENDIF
	// Patrol
	IF logic.Ped.Task.Patrol.Route != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.Patrol.Route' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.Patrol.AlertState != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.Patrol.AlertState' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.Patrol.CanChat != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.Patrol.CanChat' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.Patrol.ShouldFollowExactRoute != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.Patrol.ShouldFollowExactRoute' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.Patrol.ShouldMoveOntoTheNextNode != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.Patrol.ShouldMoveOntoTheNextNode' - fuction pointer defined.")
	ENDIF
	// HandsUp
	IF logic.Ped.Task.HandsUp.Time != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.HandsUp.Time' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.HandsUp.PedToFace != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.HandsUp.PedToFace' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.HandsUp.PedToFaceTime != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.HandsUp.PedToFaceTime' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.HandsUp.Flags != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.HandsUp.Flags' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.HandsUp.OnTask != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.HandsUp.OnTask' - fuction pointer defined.")
	ENDIF
	// FleeFromCoord
	IF logic.Ped.Task.FleeFromCoord.Coords != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.FleeFromCoord.Coords' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.FleeFromCoord.SafeDistance != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.FleeFromCoord.SafeDistance' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.FleeFromCoord.FleeTime != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.FleeFromCoord.FleeTime' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.FleeFromCoord.OnTask != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.FleeFromCoord.OnTask' - fuction pointer defined.")
	ENDIF
	// DriveVehicle
	IF logic.Ped.Task.DriveVehicle.Entity != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.DriveVehicle.Entity' - function pointer defined.")
	ENDIF
	IF logic.Ped.Task.DriveVehicle.CruiseSpeed != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.DriveVehicle.CruiseSpeed' - function pointer defined.")
	ENDIF
	IF logic.Ped.Task.DriveVehicle.DrivingModeFlags != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.DriveVehicle.DrivingModeFlags' - function pointer defined.")
	ENDIF
	// FleeInVehicle
	IF logic.Ped.Task.FleeInVehicle.VehToUse != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.FleeInVehicle.VehToUse' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.FleeInVehicle.PedToFleeFrom != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.FleeInVehicle.PedToFleeFrom' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.FleeInVehicle.FleeSpeed != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.FleeInVehicle.FleeSpeed' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.FleeInVehicle.FlightHeight != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.FleeInVehicle.FlightHeight' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.FleeInVehicle.MinFlightHeightAboveTerrain != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.FleeInVehicle.MinFlightHeightAboveTerrain' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.FleeInVehicle.DrivingModeFlags != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.FleeInVehicle.DrivingModeFlags' - fuction pointer defined.")
	ENDIF
	// FollowPlayer
	IF logic.Ped.Task.FollowPlayer.Enable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.FollowPlayer.Enable' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.FollowPlayer.IdleBehaviour != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.FollowPlayer.IdleBehaviour' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.FollowPlayer.JoinRange != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.FollowPlayer.JoinRange' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.FollowPlayer.MaxMoveBlendRatio != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.FollowPlayer.MaxMoveBlendRatio' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.FollowPlayer.OnJoinGroup != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.FollowPlayer.OnJoinGroup' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.FollowPlayer.OnLeaveGroup != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.FollowPlayer.OnLeaveGroup' - fuction pointer defined.")
	ENDIF
	// GoToCoordAnyMeans
	IF logic.Ped.Task.GoToCoordAnyMeans.Coords != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.GoToCoordAnyMeans.Coords' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.GoToCoordAnyMeans.NumCoords != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.GoToCoordAnyMeans.NumCoords' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.GoToCoordAnyMeans.Radius != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.GoToCoordAnyMeans.Radius' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.GoToCoordAnyMeans.RadiusServer != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.GoToCoordAnyMeans.RadiusServer' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.GoToCoordAnyMeans.PedMoveBlendRatio != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.GoToCoordAnyMeans.PedMoveBlendRatio' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.GoToCoordAnyMeans.Vehicle != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.GoToCoordAnyMeans.Vehicle' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.GoToCoordAnyMeans.DrivingFlags != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.GoToCoordAnyMeans.DrivingFlags' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.GoToCoordAnyMeans.ExtraDistanceToPreferVehicle != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.GoToCoordAnyMeans.ExtraDistanceToPreferVehicle' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.GoToCoordAnyMeans.Flags != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.GoToCoordAnyMeans.Flags' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.GoToCoordAnyMeans.Speed != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.GoToCoordAnyMeans.Speed' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.GoToCoordAnyMeans.ShouldCheck3dDistance != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.GoToCoordAnyMeans.ShouldCheck3dDistance' - fuction pointer defined.")
	ENDIF
	// LandHeli
	IF logic.Ped.Task.LandHeli.Coords != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.LandHeli.Coords' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.LandHeli.Heading != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.LandHeli.Heading' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.LandHeli.DisableSlowdown != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.LandHeli.DisableSlowdown' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.LandHeli.ReachDistance != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.LandHeli.ReachDistance' - fuction pointer defined.")
	ENDIF
	// VehicleMission
	IF logic.Ped.Task.VehicleMission.Mission != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleMission.Mission' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleMission.TargetEntity != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleMission.TargetEntity' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleMission.TargetCoord != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleMission.TargetCoord' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleMission.DrivingFlags != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleMission.DrivingFlags' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleMission.StraightLineDist != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleMission.StraightLineDist' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleMission.TargetReachedDist != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleMission.TargetReachedDist' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.VehicleMission.CruiseSpeed != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.VehicleMission.CruiseSpeed' - fuction pointer defined.")
	ENDIF
	// WanderOnFoot
	IF logic.Ped.Task.WanderOnFoot.Type != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.WanderOnFoot.Type' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.WanderOnFoot.Heading != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.WanderOnFoot.Heading' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.WanderOnFoot.Flags != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.WanderOnFoot.Flags' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.WanderOnFoot.AreaCoord != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.WanderOnFoot.AreaCoord' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.WanderOnFoot.AreaRadius != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.WanderOnFoot.AreaRadius' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.WanderOnFoot.AreaMinWaitTime != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.WanderOnFoot.AreaMinWaitTime' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.WanderOnFoot.AreaMaxWaitTime != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.WanderOnFoot.AreaMaxWaitTime' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.WanderOnFoot.AnimGroup != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.WanderOnFoot.AnimGroup' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.WanderOnFoot.Anim != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.WanderOnFoot.Anim' - fuction pointer defined.")
	ENDIF
	// StayInCover
	IF logic.Ped.Task.StayInCover.Coord != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.StayInCover.Coord' - function pointer defined.")
	ENDIF
	IF logic.Ped.Task.StayInCover.Radius != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.StayInCover.Radius' - function pointer defined.")
	ENDIF
	// ThrowProjectile
	IF logic.Ped.Task.ThrowProjectile.Coord != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.ThrowProjectile.Coord' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.ThrowProjectile.MinDistance != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.ThrowProjectile.MinDistance' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.ThrowProjectile.WeaponType != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.ThrowProjectile.WeaponType' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.ThrowProjectile.AmmoCount != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.ThrowProjectile.AmmoCount' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Task.ThrowProjectile.MarkAsDone != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Task.ThrowProjectile.MarkAsDone' - fuction pointer defined.")
	ENDIF
	// - Triggers -
	IF logic.Ped.Triggers.Setup != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Triggers.Setup' - function pointer defined.")
	ENDIF
	// PlayerNearPed
	IF logic.Ped.Triggers.PlayerNearPed.Enable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Triggers.PlayerNearPed.Enable' - function pointer defined.")
	ENDIF
	IF logic.Ped.Triggers.PlayerNearPed.Range != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Triggers.PlayerNearPed.Range' - function pointer defined.")
	ENDIF
	IF logic.Ped.Triggers.PlayerNearPed.LineOfSight != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Triggers.PlayerNearPed.LineOfSight' - function pointer defined.")
	ENDIF
	IF logic.Ped.Triggers.PlayerNearPed.Delay != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Triggers.PlayerNearPed.Delay' - function pointer defined.")
	ENDIF
	IF logic.Ped.Triggers.PlayerNearPed.PersistentCheck != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Triggers.PlayerNearPed.PersistentCheck' - function pointer defined.")
	ENDIF
	IF logic.Ped.Triggers.PlayerNearPed.OnChanged != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Triggers.PlayerNearPed.OnChanged' - function pointer defined.")
	ENDIF
	// -
	IF logic.Ped.Triggers.Targetted.Range != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Triggers.Targetted.Range' - function pointer defined.")
	ENDIF
	// - Blip -
	IF logic.Ped.Blip.Enable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.Enable' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.EnableArea != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.EnableArea' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.EnableCentre != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.EnableCentre' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.AreaAlpha != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.AreaAlpha' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.Sprite != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.Sprite' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.Colour != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.Colour' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.Scale != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.Scale' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.Priority != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.Priority' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.PlayerScale != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.PlayerScale' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.FlashOnCreation != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.FlashOnCreation' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.FlashOnChange != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.FlashOnChange' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.ConstantFlash != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.ConstantFlash' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.VisionCone != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.VisionCone' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.Name != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.Name' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.ShowHeight != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.ShowHeight' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.DrawGPS != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.DrawGPS' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.ShortRange != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.ShortRange' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.DistanceAlphaData != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.DistanceAlphaData' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.Rotate != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.Rotate' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.DrawMarker != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.DrawMarker' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.MarkerScale != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.MarkerScale' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.MarkerOffset != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.MarkerOffset' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Blip.MarkerYOffset != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Blip.MarkerYOffset' - fuction pointer defined.")
	ENDIF
	// - AmbientSpeech -
	IF logic.Ped.AmbientSpeech.Context != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.AmbientSpeech.Context' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.AmbientSpeech.CollectionDelay != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.AmbientSpeech.CollectionDelay' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.AmbientSpeech.SpeechParams != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.AmbientSpeech.SpeechParams' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.AmbientSpeech.ShouldCollectionRepeat != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.AmbientSpeech.ShouldCollectionRepeat' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.AmbientSpeech.VoiceHash != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.AmbientSpeech.VoiceHash' - fuction pointer defined.")
	ENDIF
	// - FadeOut -
	IF logic.Ped.FadeOut.ShouldFade != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.FadeOut.ShouldFade' - function pointer defined.")
	ENDIF
	// - Respawn -
	IF logic.Ped.Respawn.Enable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Respawn.Enable' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Respawn.Coords != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Respawn.Coords' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Respawn.Heading != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Respawn.Heading' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Respawn.OnRespawn != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Respawn.OnRespawn' - fuction pointer defined.")
	ENDIF
	IF logic.Ped.Respawn.Delay != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Respawn.Delay' - fuction pointer defined.")
	ENDIF
	// - Group -
	IF logic.Ped.Group.OnTriggered != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Group.OnTriggered' - fuction pointer defined.")
	ENDIF
	// - Misc -
	IF logic.Ped.Activate != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Activate' - function pointer defined.")
	ENDIF
	IF logic.Ped.Model != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Model' - function pointer defined.")
	ENDIF
	IF logic.Ped.Coords != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Coords' - function pointer defined.")
	ENDIF
	IF logic.Ped.Heading != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Heading' - function pointer defined.")
	ENDIF
	IF logic.Ped.Weapon != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Weapon' - function pointer defined.")
	ENDIF
	IF logic.Ped.Attributes != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Attributes' - function pointer defined.")
	ENDIF
	IF logic.Ped.Client != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Client' - function pointer defined.")
	ENDIF
	IF logic.Ped.Server != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.Server' - function pointer defined.")
	ENDIF
	IF logic.Ped.VisionAlertTime != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.VisionAlertTime' - function pointer defined.")
	ENDIF
	IF logic.Ped.VisionFlags != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.VisionFlags' - function pointer defined.")
	ENDIF
	IF logic.Ped.DoWarningSpeech != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.DoWarningSpeech' - function pointer defined.")
	ENDIF
	IF logic.Ped.OnSeenPlayer != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.OnSeenPlayer' - function pointer defined.")
	ENDIF
	IF logic.Ped.DisableGenericEnemyPedBlip != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.DisableGenericEnemyPedBlip' - function pointer defined.")
	ENDIF
	IF logic.Ped.OnDamaged != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.OnDamaged' - function pointer defined.")
	ENDIF
	IF logic.Ped.OnDamagedNonPed != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.OnDamagedNonPed' - function pointer defined.")
	ENDIF
	IF logic.Ped.OnBumpedByPlayer != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.OnBumpedByPlayer' - function pointer defined.")
	ENDIF
	IF logic.Ped.OnCleanup != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.OnCleanup' - function pointer defined.")
	ENDIF
	IF logic.Ped.ForceCleanup != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.ForceCleanup' - function pointer defined.")
	ENDIF
	IF logic.Ped.ShouldEquipWeaponInHand != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.ShouldEquipWeaponInHand' - function pointer defined.")
	ENDIF
	IF logic.Ped.CanBeCreated != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.CanBeCreated' - function pointer defined.")
	ENDIF
	#IF MAX_NUM_SPAWN_POSITIONS
	    IF logic.Ped.GetSpawnPositionVariationIndex != NULL
	        PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ped.GetSpawnPositionVariationIndex' - function pointer defined.")
	    ENDIF
	#ENDIF
	// -- Vehicle --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Vehicle' ***")
	// - Blip -	
	IF logic.Vehicle.Blip.Enable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.Enable' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.EnableArea != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.EnableArea' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.EnableCentre != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.EnableCentre' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.AreaAlpha != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.AreaAlpha' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.Sprite != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.Sprite' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.Colour != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.Colour' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.Scale != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.Scale' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.Priority != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.Priority' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.PlayerScale != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.PlayerScale' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.FlashOnCreation != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.FlashOnCreation' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.FlashOnChange != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.FlashOnChange' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.ConstantFlash != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.ConstantFlash' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.VisionCone != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.Name != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.Name' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.ShowHeight != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.ShowHeight' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.DrawGPS != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.DrawGPS' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.ShortRange != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.ShortRange' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.DistanceAlphaData != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.DistanceAlphaData' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.Rotate != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.Rotate' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.DrawMarker != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.DrawMarker' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.MarkerScale != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.MarkerScale' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.MarkerOffset != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.MarkerOffset' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Blip.MarkerYOffset != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Blip.MarkerYOffset' - fuction pointer defined.")
	ENDIF
	// - Misc -
	IF logic.Vehicle.Activate != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Activate' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.PersistentNearCheck != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.PersistentNearCheck' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.NearRange != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.NearRange' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.CheckLOSNear != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.CheckLOSNear' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.CheckOnScreenNear != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.CheckOnScreenNear' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.OnNearChanged != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.OnNearChanged' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.Damageable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Damageable' - fuction pointer defined.")
	ENDIF		
	IF logic.Vehicle.MaintainClosest != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.MaintainClosest' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.ShouldUnlock != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.ShouldUnlock' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.ShouldWarp != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.ShouldWarp' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.ShouldRespawn != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.ShouldRespawn' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.RespawnDelay != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.RespawnDelay' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.SendWarpHelpText != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.SendWarpHelpText' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.DamageScale != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.DamageScale' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.GetOffsetSpawnCoords != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.GetOffsetSpawnCoords' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.FindSpawnParams != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.FindSpawnParams' - fuction pointer defined.")
	ENDIF		
	IF logic.Vehicle.DynamicSpawnCoord != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.DynamicSpawnCoord' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.DynamicSpawnDirection != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.DynamicSpawnDirection' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.DynamicSpawnRange != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.DynamicSpawnRange' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.DynamicSpawnValidation != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.DynamicSpawnValidation' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.Attributes != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Attributes' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.Mods != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Mods' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.Client != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Client' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.Server != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Server' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.OnDamaged != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.OnDamaged' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.OnDamager != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.OnDamager' - fuction pointer defined.")
	ENDIF	
	IF logic.Vehicle.Doors != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Doors' - fuction pointer defined.")
	ENDIF
	// - Trailer -
	IF logic.Vehicle.Trailer.Offset != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Trailer.Offset' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.Trailer.Rotation != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.Trailer.Rotation' - fuction pointer defined.")
	ENDIF
	// - Misc -
	IF logic.Vehicle.ForceCleanup != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.ForceCleanup' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.OnCleanup != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.OnCleanup' - fuction pointer defined.")
	ENDIF
	IF logic.Vehicle.CanBeCreated != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.CanBeCreated' - fuction pointer defined.")
	ENDIF
	#IF MAX_NUM_SPAWN_POSITIONS
		IF logic.Vehicle.GetSpawnPositionVariationIndex != NULL
		    PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Vehicle.GetSpawnPositionVariationIndex' - fuction pointer defined.")
		ENDIF
	#ENDIF
	// -- Prop --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Prop' ***")
	// - Blip -
	IF logic.Prop.Blip.Enable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.Enable' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.EnableArea != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.EnableArea' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.EnableCentre != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.EnableCentre' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.AreaAlpha != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.AreaAlpha' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.Sprite != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.Sprite' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.Colour != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.Colour' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.Scale != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.Scale' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.Priority != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.Priority' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.PlayerScale != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.PlayerScale' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.FlashOnCreation != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.FlashOnCreation' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.FlashOnChange != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.FlashOnChange' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.ConstantFlash != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.ConstantFlash' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.VisionCone != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.VisionCone' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.Name != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.Name' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.ShowHeight != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.ShowHeight' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.DrawGPS != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.DrawGPS' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.ShortRange != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.ShortRange' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.DistanceAlphaData != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.DistanceAlphaData' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.Rotate != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.Rotate' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.DrawMarker != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.DrawMarker' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.MarkerScale != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.MarkerScale' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.MarkerOffset != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.MarkerOffset' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.MarkerYOffset != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.MarkerYOffset' - fuction pointer defined.")
	ENDIF
	// - Misc -
	IF logic.Prop.Activate != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Activate' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.StaticEmitter != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.StaticEmitter' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.DetachOnDeath != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.DetachOnDeath' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.AttachCollisionsActive != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.AttachCollisionsActive' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.ShouldCleanup != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.ShouldCleanup' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.ShouldDelete != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.ShouldDelete' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Damageable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Damageable' - fuction pointer defined.")
	ENDIF
	// - Explosion -
	IF logic.Prop.Explosion.ShouldExplode != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Explosion.ShouldExplode' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Explosion.Tag != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Explosion.Tag' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Explosion.Scale != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Explosion.Scale' - fuction pointer defined.")
	ENDIF
	// - Misc -
	IF logic.Prop.Attributes != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Attributes' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.OnDamaged != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.OnDamaged' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Client != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Client' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Server != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Server' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Coords != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Coords' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Rotation != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Rotation' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.NearRange != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.NearRange' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.DrawMarker != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.DrawMarker' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.MarkerScale != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.MarkerScale' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.Blip.Colour != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Blip.Colour' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.CheckLOSNear != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.CheckLOSNear' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.CheckOnScreenNear != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.CheckOnScreenNear' - fuction pointer defined.")
	ENDIF
	IF logic.Prop.OnNearChanged != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.OnNearChanged' - fuction pointer defined.")
	ENDIF
	#IF MAX_NUM_SPAWN_POSITIONS
		IF logic.Prop.GetSpawnPositionVariationIndex != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.GetSpawnPositionVariationIndex' - fuction pointer defined.")
		ENDIF
	#ENDIF
	
	#IF MAX_NUM_PROP_ANIMATIONS
		IF logic.Prop.Animation.ShouldAnimate != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Animation.ShouldAnimate' - fuction pointer defined.")
		ENDIF
		IF logic.Prop.Animation.AnimDict != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Animation.AnimDict' - fuction pointer defined.")
		ENDIF
		IF logic.Prop.Animation.Anim != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Animation.Anim' - fuction pointer defined.")
		ENDIF
		IF logic.Prop.Animation.NumStages != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Animation.NumStages' - fuction pointer defined.")
		ENDIF
		IF logic.Prop.Animation.BlendIn != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Animation.BlendIn' - fuction pointer defined.")
		ENDIF
		IF logic.Prop.Animation.BlendOut != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Animation.BlendOut' - fuction pointer defined.")
		ENDIF
		IF logic.Prop.Animation.StartDistance != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Animation.StartDistance' - fuction pointer defined.")
		ENDIF
		IF logic.Prop.Animation.Loop != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Animation.Loop' - fuction pointer defined.")
		ENDIF
		IF logic.Prop.Animation.ExitPhase != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Animation.ExitPhase' - fuction pointer defined.")
		ENDIF
		IF logic.Prop.Animation.HoldLastAnimation != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Prop.Animation.HoldLastAnimation' - fuction pointer defined.")
		ENDIF
	#ENDIF
	
	// -- Systems --
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - **** SYSTEMS ****")
	#IF MAX_NUM_GOTO_LOCATIONS
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.GoToPoint' ***")
		IF logic.GoToPoint.PointId != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.GoToPoint.PointId' - fuction pointer defined.")
		ENDIF
		IF logic.GoToPoint.ShouldClear != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.GoToPoint.ShouldClear' - fuction pointer defined.")
		ENDIF
		IF logic.GoToPoint.ShowInteriorBlip != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.GoToPoint.ShowInteriorBlip' - fuction pointer defined.")
		ENDIF
		IF logic.GoToPoint.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.GoToPoint.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.GoToPoint.SetRoute != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.GoToPoint.SetRoute' - fuction pointer defined.")
		ENDIF
		IF logic.GoToPoint.FlashBlip != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.GoToPoint.FlashBlip' - fuction pointer defined.")
		ENDIF
		IF logic.GoToPoint.Radius != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.GoToPoint.Radius' - fuction pointer defined.")
		ENDIF
		IF logic.GoToPoint.Check2dDistance != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.GoToPoint.Check2dDistance' - fuction pointer defined.")
		ENDIF
		IF logic.GoToPoint.ShouldProgress != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.GoToPoint.ShouldProgress' - fuction pointer defined.")
		ENDIF						
	#ENDIF
	
	#IF MAX_NUM_INTERACT_LOCATIONS	
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Interact' ***")
		IF logic.Interact.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.EnableLocation != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.EnableLocation' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Coords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Coords' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.FacingCoords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.FacingCoords' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.ForwardDirection != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.ForwardDirection' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.IsZCoordAcceptable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.IsZCoordAcceptable' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.HasTriggered != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.HasTriggered' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.MoveOnTime != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.MoveOnTime' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.MarkerEntity != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.MarkerEntity' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.MarkerCoords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.MarkerCoords' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.MarkerScale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.MarkerScale' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.CoronaCoords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.CoronaCoords' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.OnCheckObstructed != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.OnCheckObstructed' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.EnableMinigame != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.EnableMinigame' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.OnBlipCreated != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.OnBlipCreated' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.CheckLocationObstructed != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.CheckLocationObstructed' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.OnRegisteredContext != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.OnRegisteredContext' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.OnTriggered != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.OnTriggered' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.LoadCustomAssets != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.LoadCustomAssets' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.OnStartPlayingInteract != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.OnStartPlayingInteract' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.MaintainPlayingInteract != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.MaintainPlayingInteract' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.OnComplete != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.OnComplete' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.MinigamePassword != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.MinigamePassword' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.OnMinigameFail != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.OnMinigameFail' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.OnCleanup != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.OnCleanup' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.SoundBank != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.SoundBank' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.SearchPed != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.SearchPed' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.IgnoreFullySpawnedCheck != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.IgnoreFullySpawnedCheck' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.HideMyComputerIcon != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.HideMyComputerIcon' - fuction pointer defined.")
		ENDIF
		// Blip
		IF logic.Interact.Blip.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.EnableArea != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.EnableArea' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.EnableCentre != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.EnableCentre' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.AreaAlpha != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.AreaAlpha' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.Sprite != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.Sprite' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.Colour != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.Colour' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.Scale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.Scale' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.Priority != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.Priority' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.PlayerScale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.PlayerScale' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.FlashOnCreation != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.FlashOnCreation' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.FlashOnChange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.FlashOnChange' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.ConstantFlash != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.ConstantFlash' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.VisionCone != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.VisionCone' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.Name != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.Name' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.ShowHeight != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.ShowHeight' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.DrawGPS != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.DrawGPS' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.ShortRange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.ShortRange' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.DistanceAlphaData != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.DistanceAlphaData' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.Rotate != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.Rotate' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.DrawMarker != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.DrawMarker' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.MarkerScale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.MarkerScale' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.MarkerOffset != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.MarkerOffset' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Blip.MarkerYOffset != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Blip.MarkerYOffset' - fuction pointer defined.")
		ENDIF
		// HelpText
		IF logic.Interact.HelpText.ContextIntention != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.HelpText.ContextIntention' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.HelpText.ContextIntentionSubstring != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.HelpText.ContextIntentionSubstring' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.HelpText.Obstructed != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.HelpText.Obstructed' - fuction pointer defined.")
		ENDIF
		// Anim
		IF logic.Interact.Anim.Coords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Anim.Coords' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Anim.Heading != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Anim.Heading' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Anim.AnimDict != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Anim.AnimDict' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Anim.Anim != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Anim.Anim' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Anim.FacialAnim != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Anim.FacialAnim' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Anim.HasFinished != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Anim.HasFinished' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Anim.MoveOnPhase != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Anim.MoveOnPhase' - fuction pointer defined.")
		ENDIF
		IF logic.Interact.Anim.AnimFlags != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interact.Anim.AnimFlags' - fuction pointer defined.")
		ENDIF
	#ENDIF
	
	#IF MAX_NUM_SEARCH_AREAS
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.SearchArea' ***")
		// Misc
		IF logic.SearchArea.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.EnableArea != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.EnableArea' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.ForceBlipCoords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.ForceBlipCoords' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.TriggerCoords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.TriggerCoords' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.HasTriggered != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.HasTriggered' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.OnTriggered != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.OnTriggered' - fuction pointer defined.")
		ENDIF
		// Blip
		IF logic.SearchArea.Blip.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.EnableArea != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.EnableArea' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.EnableCentre != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.EnableCentre' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.AreaAlpha != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.AreaAlpha' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.Sprite != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.Sprite' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.Colour != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.Colour' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.Scale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.Scale' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.Priority != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.Priority' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.PlayerScale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.PlayerScale' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.FlashOnCreation != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.FlashOnCreation' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.FlashOnChange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.FlashOnChange' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.ConstantFlash != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.ConstantFlash' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.VisionCone != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.VisionCone' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.Name != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.Name' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.ShowHeight != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.ShowHeight' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.DrawGPS != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.DrawGPS' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.ShortRange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.ShortRange' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.DistanceAlphaData != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.DistanceAlphaData' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.Rotate != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.Rotate' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.DrawMarker != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.DrawMarker' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.MarkerScale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.MarkerScale' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.MarkerOffset != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.MarkerOffset' - fuction pointer defined.")
		ENDIF
		IF logic.SearchArea.Blip.MarkerYOffset != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SearchArea.Blip.MarkerYOffset' - fuction pointer defined.")
		ENDIF
	#ENDIF
	
	#IF MAX_NUM_TAKE_PHOTOGRAPHS
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.TakePhotos' ***")
		// Misc
		IF logic.TakePhotos.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.RequiresConfirmation != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.RequiresConfirmation' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Synced != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Synced' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.TextMessageTag != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.TextMessageTag' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.EnablePhoto != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.EnablePhoto' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.OnPhotoTaken != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.OnPhotoTaken' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.PhotoRange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.PhotoRange' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.ShowContactList != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.ShowContactList' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.RequiredZoom != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.RequiredZoom' - fuction pointer defined.")
		ENDIF			
		// Blip
		IF logic.TakePhotos.Blip.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.EnableArea != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.EnableArea' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.EnableCentre != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.EnableCentre' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.AreaAlpha != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.AreaAlpha' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.Sprite != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.Sprite' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.Colour != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.Colour' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.Scale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.Scale' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.Priority != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.Priority' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.PlayerScale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.PlayerScale' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.FlashOnCreation != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.FlashOnCreation' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.FlashOnChange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.FlashOnChange' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.ConstantFlash != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.ConstantFlash' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.VisionCone != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.VisionCone' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.Name != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.Name' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.ShowHeight != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.ShowHeight' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.DrawGPS != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.DrawGPS' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.ShortRange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.ShortRange' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.DistanceAlphaData != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.DistanceAlphaData' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.Rotate != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.Rotate' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.DrawMarker != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.DrawMarker' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.MarkerScale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.MarkerScale' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.MarkerOffset != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.MarkerOffset' - fuction pointer defined.")
		ENDIF
		IF logic.TakePhotos.Blip.MarkerYOffset != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakePhotos.Blip.MarkerYOffset' - fuction pointer defined.")
		ENDIF
	#ENDIF
	
	#IF MAX_NUM_LEAVE_AREAS
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.LeaveArea' ***")
		IF logic.LeaveArea.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.LeaveArea.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.LeaveArea.EnableArea != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.LeaveArea.EnableArea' - fuction pointer defined.")
		ENDIF
		IF logic.LeaveArea.Coords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.LeaveArea.Coords' - fuction pointer defined.")
		ENDIF
		IF logic.LeaveArea.Radius != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.LeaveArea.Radius' - fuction pointer defined.")
		ENDIF
	#ENDIF
	
	#IF MAX_NUM_CUSTOM_SPAWNS
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Player.Spawning.Custom' ***")
		IF logic.Player.Spawning.Custom.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Spawning.Custom.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.Player.Spawning.Custom.IsSpawnPointValid != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Spawning.Custom.IsSpawnPointValid' - fuction pointer defined.")
		ENDIF
		IF logic.Player.Spawning.Custom.SpawnVehicle != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Spawning.Custom.SpawnVehicle' - fuction pointer defined.")
		ENDIF
		IF logic.Player.Spawning.Custom.SpawnOverride != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Player.Spawning.Custom.SpawnOverride' - fuction pointer defined.")
		ENDIF
	#ENDIF
	
	#IF MAX_NUM_COLLECT_PEDS			
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.CollectPed' ***")
		IF logic.CollectPed.OnNearChanged != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CollectPed.OnNearChanged' - fuction pointer defined.")
		ENDIF
		IF logic.CollectPed.OnSignaled != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CollectPed.OnSignaled' - fuction pointer defined.")
		ENDIF
		IF logic.CollectPed.MaintainSignalAvailable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CollectPed.MaintainSignalAvailable' - fuction pointer defined.")
		ENDIF
		IF logic.CollectPed.DoCollected != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CollectPed.DoCollected' - fuction pointer defined.")
		ENDIF
		IF logic.CollectPed.ShouldCollectPed != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CollectPed.ShouldCollectPed' - fuction pointer defined.")
		ENDIF
		IF logic.CollectPed.ShouldSignalCollectPed != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CollectPed.ShouldSignalCollectPed' - fuction pointer defined.")
		ENDIF
		IF logic.CollectPed.IsMissionEntityInCorrectStateForSignal != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CollectPed.IsMissionEntityInCorrectStateForSignal' - fuction pointer defined.")
		ENDIF
		IF logic.CollectPed.IsPedInCorrectStateForSignal != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CollectPed.IsPedInCorrectStateForSignal' - fuction pointer defined.")
		ENDIF
		IF logic.CollectPed.ShouldStopVehicleNearPedsForCollectPed != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CollectPed.ShouldStopVehicleNearPedsForCollectPed' - fuction pointer defined.")
		ENDIF
		IF logic.CollectPed.IsLocalPlayerNearCollectPed != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CollectPed.IsLocalPlayerNearCollectPed' - fuction pointer defined.")
		ENDIF
		IF logic.CollectPed.IsLocalPlayerSignalingCollectPed != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CollectPed.IsLocalPlayerSignalingCollectPed' - fuction pointer defined.")
		ENDIF			
	#ENDIF
	
	#IF MAX_NUM_ARREST_PED_SUSPECTS
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.ArrestPed' ***")
		IF logic.ArrestPed.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ArrestPed.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.ArrestPed.EnableSuspect != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ArrestPed.EnableSuspect' - fuction pointer defined.")
		ENDIF
		IF logic.ArrestPed.Help != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ArrestPed.Help' - fuction pointer defined.")
		ENDIF
		IF logic.ArrestPed.HelpDelay != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ArrestPed.HelpDelay' - fuction pointer defined.")
		ENDIF
		IF logic.ArrestPed.DisplayHelp != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ArrestPed.DisplayHelp' - fuction pointer defined.")
		ENDIF
		IF logic.ArrestPed.IsZCoordAcceptable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ArrestPed.IsZCoordAcceptable' - fuction pointer defined.")
		ENDIF
		IF logic.ArrestPed.PeformArrest != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ArrestPed.PeformArrest' - fuction pointer defined.")
		ENDIF
		IF logic.ArrestPed.OnComplete != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ArrestPed.OnComplete' - fuction pointer defined.")
		ENDIF				
	#ENDIF
	
	#IF MAX_NUM_CUTSCENES				
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Cutscene' ***")
		IF logic.Cutscene.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.Coords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.Coords' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.Heading != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.Heading' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.RangeCoords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.RangeCoords' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.LoadSceneCoords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.LoadSceneCoords' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.AnimDict != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.AnimDict' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.CameraAnimDict != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.CameraAnimDict' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.Duration != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.Duration' - fuction pointer defined.")
		ENDIF
		#IF MAX_NUM_CUTSCENE_PLAYERS
			IF logic.Cutscene.PlayerID != NULL
				PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.PlayerID' - fuction pointer defined.")
			ENDIF
		#ENDIF
		IF logic.Cutscene.ShouldLoadAssets != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.ShouldLoadAssets' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.ShouldStart != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.ShouldStart' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.ShouldFadeIn != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.ShouldFadeIn' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.ShouldFadeOut != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.ShouldFadeOut' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.ShouldFinish != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.ShouldFinish' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.ShouldCleanUp != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.ShouldCleanUp' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.PlayMocapAtCoords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.PlayMocapAtCoords' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.MaintainRun != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.MaintainRun' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.LoadCustomAssets != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.LoadCustomAssets' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.OnPlayerCloneCreated != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.OnPlayerCloneCreated' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.OnPedCloneCreated != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.OnPedCloneCreated' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.OnVehicleCloneCreated != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.OnVehicleCloneCreated' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.OnPropCreated != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.OnPropCreated' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.OnStart != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.OnStart' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.OnPlaying != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.OnPlaying' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.OnEnd != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.OnEnd' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.RegisterEntities != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.RegisterEntities' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.SetStreamingFlags != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.SetStreamingFlags' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.OnCleanup != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.OnCleanup' - fuction pointer defined.")
		ENDIF
		// - Camera -
		IF logic.Cutscene.Camera.Coords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.Camera.Coords' - fuction pointer defined.")
		ENDIF				
		IF logic.Cutscene.Camera.Rotation != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.Camera.Rotation' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.Camera.FOV != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.Camera.FOV' - fuction pointer defined.")
		ENDIF
		IF logic.Cutscene.Camera.ShakeAmplitude != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Cutscene.Camera.ShakeAmplitude' - fuction pointer defined.")
		ENDIF
	#ENDIF
	
	#IF MAX_NUM_AMBUSH_VEHICLES			
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Ambush' ***")
		IF logic.Ambush.ShouldActivate != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ambush.ShouldActivate' - fuction pointer defined.")
		ENDIF
		IF logic.Ambush.IsTargetValid != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ambush.IsTargetValid' - fuction pointer defined.")
		ENDIF
		IF logic.Ambush.ShouldPrioritiseTarget != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ambush.ShouldPrioritiseTarget' - fuction pointer defined.")
		ENDIF
		IF logic.Ambush.TargetEntityType != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ambush.TargetEntityType' - fuction pointer defined.")
		ENDIF
		IF logic.Ambush.DropoffDismissRange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ambush.DropoffDismissRange' - fuction pointer defined.")
		ENDIF
		IF logic.Ambush.SpawnRange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ambush.SpawnRange' - fuction pointer defined.")
		ENDIF
		IF logic.Ambush.SpawnOffsetRange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ambush.SpawnOffsetRange' - fuction pointer defined.")
		ENDIF
		IF logic.Ambush.CleanupRange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ambush.CleanupRange' - fuction pointer defined.")
		ENDIF
		IF logic.Ambush.ForceCleanup != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ambush.ForceCleanup' - fuction pointer defined.")
		ENDIF
		IF logic.Ambush.OverrideVehicleModel != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Ambush.OverrideVehicleModel' - fuction pointer defined.")
		ENDIF
	#ENDIF
	
	#IF MAX_NUM_TRIGGER_AREAS	
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.TriggerArea' ***")
		IF logic.TriggerArea.TimeAllowed != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TriggerArea.TimeAllowed' - fuction pointer defined.")
		ENDIF
		IF logic.TriggerArea.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TriggerArea.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.TriggerArea.Dataset != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TriggerArea.Dataset' - fuction pointer defined.")
		ENDIF				
	#ENDIF
	
	#IF MAX_NUM_BOTTOM_RIGHT_HUD		
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.BottomRightHUD' ***")
		IF logic.BottomRightHUD.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.BottomRightHUD.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.BottomRightHUD.Value != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.BottomRightHUD.Value' - fuction pointer defined.")
		ENDIF
		IF logic.BottomRightHUD.DrawCustom != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.BottomRightHUD.DrawCustom' - fuction pointer defined.")
		ENDIF		
	#ENDIF
	
	#IF MAX_NUM_RELATIONSHIP_GROUPS		
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Relationship' ***")
		IF logic.Relationship.ToPlayer != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Relationship.ToPlayer' - fuction pointer defined.")
		ENDIF
		IF logic.Relationship.Setup != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Relationship.Setup' - fuction pointer defined.")
		ENDIF				
	#ENDIF
	
	#IF MAX_NUM_EVENT_LOCATION_BLIPS	
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.EventLocationBlip' ***")
		IF logic.EventLocationBlip.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.EventLocationBlip.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.EventLocationBlip.ShowAreaBlip != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.EventLocationBlip.ShowAreaBlip' - fuction pointer defined.")
		ENDIF
		IF logic.EventLocationBlip.OnCreation != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.EventLocationBlip.OnCreation' - fuction pointer defined.")
		ENDIF
		IF logic.EventLocationBlip.Name != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.EventLocationBlip.Name' - fuction pointer defined.")
		ENDIF
		IF logic.EventLocationBlip.Sprite != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.EventLocationBlip.Sprite' - fuction pointer defined.")
		ENDIF
		IF logic.EventLocationBlip.Colour != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.EventLocationBlip.Colour' - fuction pointer defined.")
		ENDIF
		IF logic.EventLocationBlip.Scale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.EventLocationBlip.Scale' - fuction pointer defined.")
		ENDIF
		IF logic.EventLocationBlip.Entity != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.EventLocationBlip.Entity' - fuction pointer defined.")
		ENDIF
	#ENDIF
	
	#IF MAX_NUM_HELP_TEXTS				
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.HelpText' ***")
		IF logic.HelpText.Text != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.HelpText.Text' - fuction pointer defined.")
		ENDIF
		IF logic.HelpText.Trigger != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.HelpText.Trigger' - fuction pointer defined.")
		ENDIF
		IF logic.HelpText.Type != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.HelpText.Type' - fuction pointer defined.")
		ENDIF
		IF logic.HelpText.OnDisplay != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.HelpText.OnDisplay' - fuction pointer defined.")
		ENDIF
		IF logic.HelpText.Custom != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.HelpText.Custom' - fuction pointer defined.")
		ENDIF
		IF logic.HelpText.Time != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.HelpText.Time' - fuction pointer defined.")
		ENDIF
		IF logic.HelpText.ShouldSetDone != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.HelpText.ShouldSetDone' - fuction pointer defined.")
		ENDIF
		IF logic.HelpText.ShouldOverwrite != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.HelpText.ShouldOverwrite' - fuction pointer defined.")
		ENDIF					
		IF logic.HelpText.ShouldFinish != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.HelpText.ShouldFinish' - fuction pointer defined.")
		ENDIF					
	#ENDIF
	
	#IF MAX_NUM_TEXT_MESSAGES 			
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.TextMessages' ***")
		IF logic.TextMessages.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TextMessages.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.TextMessages.LockedStatus != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TextMessages.LockedStatus' - fuction pointer defined.")
		ENDIF
		IF logic.TextMessages.CriticalStatus != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TextMessages.CriticalStatus' - fuction pointer defined.")
		ENDIF
		IF logic.TextMessages.ReplyRequired != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TextMessages.ReplyRequired' - fuction pointer defined.")
		ENDIF
		IF logic.TextMessages.ShouldSend != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TextMessages.ShouldSend' - fuction pointer defined.")
		ENDIF
		IF logic.TextMessages.Character != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TextMessages.Character' - fuction pointer defined.")
		ENDIF
		IF logic.TextMessages.Label != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TextMessages.Label' - fuction pointer defined.")
		ENDIF
	#ENDIF
	
	#IF MAX_NUM_DIALOGUE		
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Dialogue' ***")
		IF logic.Dialogue.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.Priority != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.Priority' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.UseHeadsetAudio != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.UseHeadsetAudio' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.InterruptImmediately != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.InterruptImmediately' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.ShouldFinish != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.ShouldFinish' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.DisableHangup != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.DisableHangup' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.Opening != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.Opening' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.SpeakerID != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.SpeakerID' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.NumSpeakers != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.NumSpeakers' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.SubtitleGroup != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.SubtitleGroup' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.Character != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.Character' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.CellphoneCharacter != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.CellphoneCharacter' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.ShouldTrigger != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.ShouldTrigger' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.Root != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.Root' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.Delay != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.Delay' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.PedID != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.PedID' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.OnComplete != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.OnComplete' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.AllowDuringCutscene != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.AllowDuringCutscene' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.OnAnswer != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.OnAnswer' - fuction pointer defined.")
		ENDIF	
		IF logic.Dialogue.DoReplyCall != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.DoReplyCall' - fuction pointer defined.")
		ENDIF
		IF logic.Dialogue.ReplyCallOnResponse != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Dialogue.ReplyCallOnResponse' - fuction pointer defined.")
		ENDIF
	#ENDIF
	
	#IF MAX_NUM_MOVING_DOORS			
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.MovingDoor' ***")
		IF logic.MovingDoor.ShouldMove != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.MovingDoor.ShouldMove' - fuction pointer defined.")
		ENDIF					
	#ENDIF
	
	#IF MAX_NUM_WEAPON_PICKUPS			
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.WeaponPickup' ***")
		IF logic.WeaponPickup.ShouldCreate != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.WeaponPickup.ShouldCreate' - fuction pointer defined.")
		ENDIF	
	#ENDIF
	
	#IF MAX_NUM_DROPOFFS
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Delivery' ***")
		IF logic.Delivery.ShowUI != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Delivery.ShowUI' - fuction pointer defined.")
		ENDIF
		IF logic.Delivery.ShowLocate != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Delivery.ShowLocate' - fuction pointer defined.")
		ENDIF
		IF logic.Delivery.EnableDropOffBlipRoute != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Delivery.EnableDropOffBlipRoute' - fuction pointer defined.")
		ENDIF
		IF logic.Delivery.ShowMultipleDropoffs != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Delivery.ShowMultipleDropoffs' - fuction pointer defined.")
		ENDIF
		// - DropOff -
		IF logic.Delivery.DropOff.Setup != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Delivery.DropOff.Setup' - fuction pointer defined.")
		ENDIF
		IF logic.Delivery.DropOff.OverrideDropoff != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Delivery.DropOff.OverrideDropoff' - fuction pointer defined.")
		ENDIF
		IF logic.Delivery.DropOff.OverrideCoords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Delivery.DropOff.OverrideCoords' - fuction pointer defined.")
		ENDIF
		IF logic.Delivery.DropOff.ShouldShowBlipForMultipleDropoffs != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Delivery.DropOff.ShouldShowBlipForMultipleDropoffs' - fuction pointer defined.")
		ENDIF				
	#ENDIF
	
	#IF DEFINED(eMISSION_TEAMS)
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Team' ***")
		IF logic.Team.Join != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Team.Join' - fuction pointer defined.")
		ENDIF							
	#ENDIF
	
	#IF MAX_NUM_SECURITY_CAMERAS		
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.SecurityCamera' ***")
		IF logic.SecurityCamera.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.CoordsOffset != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.CoordsOffset' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.ForwardDirection != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.ForwardDirection' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.RotationOffset != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.RotationOffset' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.VisionRange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.VisionRange' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.VisionWidth != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.VisionWidth' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.VisionConeCentreOfGazeMaxAngle != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.VisionConeCentreOfGazeMaxAngle' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.EnableAlert != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.EnableAlert' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.EnableTriggerArea != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.EnableTriggerArea' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.AlertDelay != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.AlertDelay' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.OnEnteredTriggerArea != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.OnEnteredTriggerArea' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.OnAlerted != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.OnAlerted' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.OnExitedTriggerArea != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.OnExitedTriggerArea' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.DisableEntityLight != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.DisableEntityLight' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.DisableTargettable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.DisableTargettable' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.AllowPersistAudio != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.AllowPersistAudio' - fuction pointer defined.")
		ENDIF	
		// - Blip -
		IF logic.SecurityCamera.Blip.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.EnableArea != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.EnableArea' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.EnableCentre != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.EnableCentre' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.AreaAlpha != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.AreaAlpha' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.Sprite != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.Sprite' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.Colour != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.Colour' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.Scale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.Scale' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.Priority != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.Priority' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.PlayerScale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.PlayerScale' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.FlashOnCreation != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.FlashOnCreation' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.FlashOnChange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.FlashOnChange' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.ConstantFlash != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.ConstantFlash' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.VisionCone != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.VisionCone' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.Name != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.Name' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.ShowHeight != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.ShowHeight' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.DrawGPS != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.DrawGPS' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.ShortRange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.ShortRange' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.DistanceAlphaData != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.DistanceAlphaData' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.Rotate != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.Rotate' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.DrawMarker != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.DrawMarker' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.MarkerScale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.MarkerScale' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.MarkerOffset != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.MarkerOffset' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Blip.MarkerYOffset != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Blip.MarkerYOffset' - fuction pointer defined.")
		ENDIF
		// - Sound -
		IF logic.SecurityCamera.Sound.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Sound.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Sound.UseFrontendAudio != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Sound.UseFrontendAudio' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Sound.SoundSetName != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Sound.SoundSetName' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Sound.SoundName != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Sound.SoundName' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Sound.Maintain != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Sound.Maintain' - fuction pointer defined.")
		ENDIF
		// - Turning -
		IF logic.SecurityCamera.Turning.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Turning.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Turning.TurnTimeMS != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Turning.TurnTimeMS' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Turning.WaitTimeMS != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Turning.WaitTimeMS' - fuction pointer defined.")
		ENDIF
		IF logic.SecurityCamera.Turning.TargetOffsetRotation != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecurityCamera.Turning.TargetOffsetRotation' - fuction pointer defined.")
		ENDIF		
	#ENDIF
	
	#IF MAX_NUM_MISSION_INTERIORS		
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Interior' ***")
		IF logic.Interior.ShouldWarpOut != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interior.ShouldWarpOut' - fuction pointer defined.")
		ENDIF
		IF logic.Interior.ShouldKeepVehicleOnWarpOut != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interior.ShouldKeepVehicleOnWarpOut' - fuction pointer defined.")
		ENDIF
		IF logic.Interior.DisplacedCoords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interior.DisplacedCoords' - fuction pointer defined.")
		ENDIF
		IF logic.Interior.ShouldIsolateMap != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interior.ShouldIsolateMap' - fuction pointer defined.")
		ENDIF
		IF logic.Instancing.PersistentTutorialSessionCheck != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Instancing.PersistentTutorialSessionCheck' - fuction pointer defined.")
		ENDIF
		IF logic.Interior.ShouldRunInteriorExteriorBlipChecks != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Interior.ShouldRunInteriorExteriorBlipChecks' - fuction pointer defined.")
		ENDIF
	#ENDIF
	
	#IF MAX_NUM_MISSION_PORTALS		
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Portal' ***")
		// - Misc -
		IF logic.Portal.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.ShouldTrigger != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.ShouldTrigger' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.OnWarp != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.OnWarp' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.KeepVehicle != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.KeepVehicle' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.AllowVehicleEntry != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.AllowVehicleEntry' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.DrawCorona != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.DrawCorona' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.DelayFadeIn != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.DelayFadeIn' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.SnapToGround != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.SnapToGround' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.PlaySound != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.PlaySound' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.ShouldSoundFinish != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.ShouldSoundFinish' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.SoundAudioBank != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.SoundAudioBank' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.MoveAfterWarpDistance != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.MoveAfterWarpDistance' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.BackupWarpCoord != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.BackupWarpCoord' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.InputString != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.InputString' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.RequireInput != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.RequireInput' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.WarpNearestActiveCoord != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.WarpNearestActiveCoord' - fuction pointer defined.")
		ENDIF
		// - Blip -
		IF logic.Portal.Blip.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.EnableArea != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.EnableArea' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.EnableCentre != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.EnableCentre' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.AreaAlpha != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.AreaAlpha' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.Sprite != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.Sprite' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.Colour != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.Colour' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.Scale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.Scale' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.Priority != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.Priority' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.PlayerScale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.PlayerScale' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.FlashOnCreation != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.FlashOnCreation' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.FlashOnChange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.FlashOnChange' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.ConstantFlash != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.ConstantFlash' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.VisionCone != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.VisionCone' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.Name != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.Name' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.ShowHeight != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.ShowHeight' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.DrawGPS != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.DrawGPS' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.ShortRange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.ShortRange' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.DistanceAlphaData != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.DistanceAlphaData' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.Rotate != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.Rotate' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.DrawMarker != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.DrawMarker' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.MarkerScale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.MarkerScale' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.MarkerOffset != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.MarkerOffset' - fuction pointer defined.")
		ENDIF
		IF logic.Portal.Blip.MarkerYOffset != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.Blip.MarkerYOffset' - fuction pointer defined.")
		ENDIF
		// - Misc -
		IF logic.Portal.AdditionalDisablePlayerFlags != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Portal.AdditionalDisablePlayerFlags' - fuction pointer defined.")
		ENDIF				
	#ENDIF
	
	#IF MAX_NUM_PICKUPS					
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Pickup' ***")
		// - Blip -
		IF logic.Pickup.Blip.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.EnableArea != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.EnableArea' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.EnableCentre != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.EnableCentre' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.AreaAlpha != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.AreaAlpha' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.Sprite != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.Sprite' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.Colour != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.Colour' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.Scale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.Scale' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.Priority != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.Priority' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.PlayerScale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.PlayerScale' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.FlashOnCreation != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.FlashOnCreation' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.FlashOnChange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.FlashOnChange' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.ConstantFlash != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.ConstantFlash' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.VisionCone != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.VisionCone' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.Name != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.Name' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.ShowHeight != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.ShowHeight' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.DrawGPS != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.DrawGPS' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.ShortRange != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.ShortRange' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.DistanceAlphaData != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.DistanceAlphaData' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.Rotate != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.Rotate' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.DrawMarker != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.DrawMarker' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.MarkerScale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.MarkerScale' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.MarkerOffset != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.MarkerOffset' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Blip.MarkerYOffset != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Blip.MarkerYOffset' - fuction pointer defined.")
		ENDIF
		// - Misc -
		IF logic.Pickup.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.ShouldCreatePickup != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.ShouldCreatePickup' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Coords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Coords' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Heading != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Heading' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Rotation != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Rotation' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.Attributes != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.Attributes' - fuction pointer defined.")
		ENDIF
		IF logic.Pickup.OnCollection != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Pickup.OnCollection' - fuction pointer defined.")
		ENDIF
	#ENDIF
	
	#IF MAX_NUM_CCTV_CAMERAS
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.CCTV' ***")
		IF logic.CCTV.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CCTV.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.CCTV.OnEnter != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CCTV.OnEnter' - fuction pointer defined.")
		ENDIF
		IF logic.CCTV.UseTimecycle != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CCTV.UseTimecycle' - fuction pointer defined.")
		ENDIF
		// - Camera -
		IF logic.CCTV.Camera.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CCTV.Camera.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.CCTV.Camera.StartLeft != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CCTV.Camera.StartLeft' - fuction pointer defined.")
		ENDIF
		IF logic.CCTV.Camera.RotateThroughFlip != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CCTV.Camera.RotateThroughFlip' - fuction pointer defined.")
		ENDIF
		IF logic.CCTV.Camera.Title != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CCTV.Camera.Title' - fuction pointer defined.")
		ENDIF
		IF logic.CCTV.Camera.Details != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CCTV.Camera.Details' - fuction pointer defined.")
		ENDIF
		// - Discoverable -
		IF logic.CCTV.Discoverable.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CCTV.Discoverable.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.CCTV.Discoverable.OnDiscover != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.CCTV.Discoverable.OnDiscover' - fuction pointer defined.")
		ENDIF
	#ENDIF
	
	#IF ENABLE_OFFICE_SIGN_TEXT
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.PiOfficeSignTextckup' ***")
	IF logic.OfficeSignText.Enable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.OfficeSignText.Enable' - fuction pointer defined.")
	ENDIF
	IF logic.OfficeSignText.Text != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.OfficeSignText.Text' - fuction pointer defined.")
	ENDIF
	IF logic.OfficeSignText.StylePalette != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.OfficeSignText.StylePalette' - fuction pointer defined.")
	ENDIF
	IF logic.OfficeSignText.FontColour != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.OfficeSignText.FontColour' - fuction pointer defined.")
	ENDIF
	IF logic.OfficeSignText.Font != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.OfficeSignText.Font' - fuction pointer defined.")
	ENDIF		
	#ENDIF
	
	#IF ENABLE_LOSE_THE_COPS	
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.LoseTheCops' ***")
	IF logic.LoseTheCops.Enable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.LoseTheCops.Enable' - fuction pointer defined.")
	ENDIF
	IF logic.LoseTheCops.ShouldTriggerWantedLevel != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.LoseTheCops.ShouldTriggerWantedLevel' - fuction pointer defined.")
	ENDIF
	IF logic.LoseTheCops.Delay != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.LoseTheCops.Delay' - fuction pointer defined.")
	ENDIF
	IF logic.LoseTheCops.ShouldWaitForWantedLevel != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.LoseTheCops.ShouldWaitForWantedLevel' - fuction pointer defined.")
	ENDIF
	#ENDIF
	
	#IF ENABLE_SECUROSERV_HACK_APP		
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.SecuroServHack' ***")
	IF logic.SecuroServHack.Coord != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecuroServHack.Coord' - fuction pointer defined.")
	ENDIF	
	IF logic.SecuroServHack.OnComplete != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.SecuroServHack.OnComplete' - fuction pointer defined.")
	ENDIF	
	#ENDIF
	
	#IF ENABLE_COUNTDOWN				
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Countdown' ***")
	IF logic.Countdown.Enable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Countdown.Enable' - fuction pointer defined.")
	ENDIF
	IF logic.Countdown.ShouldStart != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Countdown.ShouldStart' - fuction pointer defined.")
	ENDIF
	IF logic.Countdown.OnStart != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Countdown.OnStart' - fuction pointer defined.")
	ENDIF
	IF logic.Countdown.Maintain != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Countdown.Maintain' - fuction pointer defined.")
	ENDIF
	IF logic.Countdown.OnEnd != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Countdown.OnEnd' - fuction pointer defined.")
	ENDIF
	IF logic.Countdown.ExpireTime != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Countdown.ExpireTime' - fuction pointer defined.")
	ENDIF
	IF logic.Countdown.OnSecsLeftChanged != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Countdown.OnSecsLeftChanged' - fuction pointer defined.")
	ENDIF
	#ENDIF
	
	#IF MAX_NUM_CHECKPOINTS				
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Checkpoints' ***")
		IF logic.Checkpoints.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.ShouldDraw != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.ShouldDraw' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.ShouldDrawBlip != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.ShouldDrawBlip' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.ShouldDrawSmallBlip != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.ShouldDrawSmallBlip' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.Maintain != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.Maintain' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.OnCollect != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.OnCollect' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.OnMiss != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.OnMiss' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.OnLapComplete != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.OnLapComplete' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.SetNextCheckpoint != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.SetNextCheckpoint' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.EventsEnabled != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.EventsEnabled' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.ProcessCheckpointoCollectedEvent != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.ProcessCheckpointoCollectedEvent' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.ProcessLapCompleteEvent != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.ProcessLapCompleteEvent' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.OverrideCheckpointCoords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.OverrideCheckpointCoords' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.BlipName != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.BlipName' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.ShouldBlipFlash != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.ShouldBlipFlash' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.ShouldBlipDrawGPS != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.ShouldBlipDrawGPS' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.InsideCylinderHeightScale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.InsideCylinderHeightScale' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.InsideCylinderScale != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.InsideCylinderScale' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.CylinderHeightMax != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.CylinderHeightMax' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.CylinderHeightMin != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.CylinderHeightMin' - fuction pointer defined.")
		ENDIF
		IF logic.Checkpoints.ApplySteepFix != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.ApplySteepFix' - fuction pointer defined.")
		ENDIF		
		IF logic.Checkpoints.CanMissCheckpoint != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.CanMissCheckpoint' - fuction pointer defined.")
		ENDIF		
		IF logic.Checkpoints.HasMissedCheckpoint != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Checkpoints.HasMissedCheckpoint' - fuction pointer defined.")
		ENDIF		
	#ENDIF
	
	#IF ENABLE_TAIL_ENTITY		
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.TailEntity' ***")
		IF logic.TailEntity.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TailEntity.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.TailEntity.Target != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TailEntity.Target' - fuction pointer defined.")
		ENDIF
		IF logic.TailEntity.CloseDuration != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TailEntity.CloseDuration' - fuction pointer defined.")
		ENDIF
		IF logic.TailEntity.CloseDist != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TailEntity.CloseDist' - fuction pointer defined.")
		ENDIF
		IF logic.TailEntity.FarDuration != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TailEntity.FarDuration' - fuction pointer defined.")
		ENDIF
		IF logic.TailEntity.FarDist != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TailEntity.FarDist' - fuction pointer defined.")
		ENDIF
		IF logic.TailEntity.HudTitle != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TailEntity.HudTitle' - fuction pointer defined.")
		ENDIF
		IF logic.TailEntity.DrawHUD != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TailEntity.DrawHUD' - fuction pointer defined.")
		ENDIF
		IF logic.TailEntity.HudColourSpook != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TailEntity.HudColourSpook' - fuction pointer defined.")
		ENDIF			
	#ENDIF
		
	#IF ENABLE_VEHICLE_VALUE	
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.VehicleValue' ***")
		IF logic.VehicleValue.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.VehicleValue.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.VehicleValue.Vehicle != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.VehicleValue.Vehicle' - fuction pointer defined.")
		ENDIF
		IF logic.VehicleValue.OfferReductionCost != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.VehicleValue.OfferReductionCost' - fuction pointer defined.")
		ENDIF
		IF logic.VehicleValue.BaseValue != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.VehicleValue.BaseValue' - fuction pointer defined.")
		ENDIF
		IF logic.VehicleValue.NumHitsCost != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.VehicleValue.NumHitsCost' - fuction pointer defined.")
		ENDIF
		IF logic.VehicleValue.DrawHUD != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.VehicleValue.DrawHUD' - fuction pointer defined.")
		ENDIF			
	#ENDIF
	
	#IF MAX_NUM_SHOCKING_EVENTS	
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.ShockingEvents' ***")	
		IF logic.ShockingEvents.Trigger != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ShockingEvents.Trigger' - fuction pointer defined.")
		ENDIF
		IF logic.ShockingEvents.Type != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ShockingEvents.Type' - fuction pointer defined.")
		ENDIF
		IF logic.ShockingEvents.Interval != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ShockingEvents.Interval' - fuction pointer defined.")
		ENDIF
		IF logic.ShockingEvents.Coords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ShockingEvents.Coords' - fuction pointer defined.")
		ENDIF
		IF logic.ShockingEvents.Entity != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ShockingEvents.Entity' - fuction pointer defined.")
		ENDIF
		IF logic.ShockingEvents.Duration != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.ShockingEvents.Duration' - fuction pointer defined.")
		ENDIF	
	#ENDIF
	
	#IF ENABLE_INVITES			
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Invites' ***")	
		IF logic.Invites.EnableApproval != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Invites.EnableApproval' - fuction pointer defined.")
		ENDIF
		IF logic.Invites.ShouldApprove!= NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Invites.ShouldApprove' - fuction pointer defined.")
		ENDIF						
	#ENDIF
	
	#IF MAX_NUM_POCKET_ITEMS
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.PocketItem' ***")	
		IF logic.PocketItem.GetNum != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.PocketItem.GetNum' - fuction pointer defined.")
		ENDIF
		IF logic.PocketItem.ShouldCreate != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.PocketItem.ShouldCreate' - fuction pointer defined.")
		ENDIF
		IF logic.PocketItem.ShouldHold != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.PocketItem.ShouldHold' - fuction pointer defined.")
		ENDIF
		IF logic.PocketItem.GetHolderFallback != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.PocketItem.GetHolderFallback' - fuction pointer defined.")
		ENDIF
		IF logic.PocketItem.OnCollect != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.PocketItem.OnCollect' - fuction pointer defined.")
		ENDIF
		IF logic.PocketItem.OnDelivery != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.PocketItem.OnDelivery' - fuction pointer defined.")
		ENDIF				
	#ENDIF
	
	#IF MAX_NUM_STATIC_BLIPS		
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.StaticBlips' ***")
		IF logic.StaticBlips.Enable != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.StaticBlips.Enable' - fuction pointer defined.")
		ENDIF
		IF logic.StaticBlips.StaticBlip != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.StaticBlips.StaticBlip' - fuction pointer defined.")
		ENDIF
		IF logic.StaticBlips.Name != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.StaticBlips.Name' - fuction pointer defined.")
		ENDIF
		IF logic.StaticBlips.vComparisonCoords != NULL
			PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.StaticBlips.vComparisonCoords' - fuction pointer defined.")
		ENDIF				
	#ENDIF
	
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.TakeOutTarget' ***")
	IF logic.TakeOutTarget.BottomRightHUD.Enable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakeOutTarget.Enable' - fuction pointer defined.")
	ENDIF
	IF logic.TakeOutTarget.BottomRightHUD.Value != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakeOutTarget.Value' - fuction pointer defined.")
	ENDIF
	IF logic.TakeOutTarget.BottomRightHUD.DrawCustom != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakeOutTarget.DrawCustom' - fuction pointer defined.")
	ENDIF
	IF logic.TakeOutTarget.DrawPedArrow != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.TakeOutTarget.DrawPedArrow' - fuction pointer defined.")
	ENDIF
	
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.RadarZoom' ***")
	IF logic.RadarZoom.Enable != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.RadarZoom.Enable' - fuction pointer defined.")
	ENDIF
	IF logic.RadarZoom.DisableBossRadius != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.RadarZoom.DisableBossRadius' - fuction pointer defined.")
	ENDIF
	IF logic.RadarZoom.Level != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.RadarZoom.Level' - fuction pointer defined.")
	ENDIF
		
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - *** Section 'logic.Debug' ***")	
	IF logic.Debug.Widgets.Create != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Debug.Widgets.Create' - fuction pointer defined.")
	ENDIF
	IF logic.Debug.Widgets.Update != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Debug.Widgets.Update' - fuction pointer defined.")
	ENDIF
	IF logic.Debug.OnJSkip != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Debug.OnJSkip' - fuction pointer defined.")
	ENDIF
	IF logic.Debug.OverrideJSkip != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Debug.OverrideJSkip' - fuction pointer defined.")
	ENDIF
	IF logic.Debug.FollowEntity != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Debug.FollowEntity' - fuction pointer defined.")
	ENDIF
	IF logic.Debug.FollowEntityOffsetVector != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Debug.FollowEntityOffsetVector' - fuction pointer defined.")
	ENDIF
	IF logic.Debug.FollowEntityEndPoint != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Debug.FollowEntityEndPoint' - fuction pointer defined.")
	ENDIF
	IF logic.Debug.ArrestPedOffsetVector != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Debug.ArrestPedOffsetVector' - fuction pointer defined.")
	ENDIF
	IF logic.Debug.SearchAreaCoordOffsetVector != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Debug.SearchAreaCoordOffsetVector' - fuction pointer defined.")
	ENDIF
	IF logic.Debug.AdditionalDebug != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Debug.AdditionalDebug' - fuction pointer defined.")
	ENDIF
	IF logic.Debug.OverrideEvent != NULL
		PRINTLN("[",scriptName,"] - [LOGIC_FP] - 'logic.Debug.OverrideEvent' - fuction pointer defined.")
	ENDIF
	
	PRINTLN("[",scriptName,"] - [LOGIC_FP] - ****** END ******")
ENDPROC

#ENDIF //#IF IS_DEBUG_BUILD
