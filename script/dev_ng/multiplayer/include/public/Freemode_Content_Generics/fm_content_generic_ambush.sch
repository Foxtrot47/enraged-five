USING "fm_content_generic_support.sch"

///////////////////////////////////
////  		  SPAWNING 	       ////
///////////////////////////////////

#IF MAX_NUM_AMBUSH_VEHICLES

FUNC eENTITY_TYPE GET_AMBUSH_TARGET_ENTITY_TYPE()
	IF logic.Ambush.TargetEntityType != null
		RETURN CALL logic.Ambush.TargetEntityType()
	ENDIF
	RETURN eENTITYTYPE_MISSION_ENTITY
ENDFUNC

FUNC FLOAT GET_AMBUSH_SPAWN_RANGE()
	IF logic.Ambush.SpawnRange != null
		RETURN CALL logic.Ambush.SpawnRange()
	ENDIF
	RETURN data.Ambush.fSpawnDistance
ENDFUNC

FUNC FLOAT GET_AMBUSH_SPAWN_OFFSET_RANGE()
	IF logic.Ambush.SpawnOffsetRange != null
		RETURN CALL logic.Ambush.SpawnOffsetRange()
	ENDIF
	RETURN 100.0
ENDFUNC

FUNC FLOAT GET_AMBUSH_CLEANUP_RANGE()
	IF logic.Ambush.CleanupRange != null
		RETURN CALL logic.Ambush.CleanupRange()
	ENDIF
	RETURN 400.0
ENDFUNC

FUNC ENTITY_INDEX GET_AMBUSH_TARGET_ENTITY_INDEX(INT iTarget)
	RETURN GET_ENTITY_INDEX_FOR_ENTITY_TYPE(GET_AMBUSH_TARGET_ENTITY_TYPE(), iTarget)
ENDFUNC

FUNC INT GET_AMBUSH_TARGET_MAX_LOOP()
	SWITCH GET_AMBUSH_TARGET_ENTITY_TYPE()
		CASE eENTITYTYPE_MISSION_ENTITY		RETURN data.MissionEntity.iCount
		CASE eENTITYTYPE_PED				RETURN data.Ped.iCount
		CASE eENTITYTYPE_PROP				RETURN data.Prop.iCount
		CASE eENTITYTYPE_VEHICLE			RETURN data.Vehicle.iCount
		CASE eENTITYTYPE_PLAYER				RETURN NUM_NETWORK_PLAYERS
	ENDSWITCH
	RETURN 0
ENDFUNC	

FUNC BOOL IS_ENTITY_AN_AMBUSH_TARGET(INT iTarget)
	SWITCH GET_AMBUSH_TARGET_ENTITY_TYPE()
		CASE eENTITYTYPE_MISSION_ENTITY		RETURN TRUE
		CASE eENTITYTYPE_PED				RETURN IS_PED_DATA_BIT_SET(iTarget, ePEDDATABITSET_AMBUSH_TARGET)
		CASE eENTITYTYPE_PROP				RETURN IS_PROP_DATA_BIT_SET(iTarget, ePROPDATABITSET_AMBUSH_TARGET)
		CASE eENTITYTYPE_VEHICLE			RETURN IS_VEHICLE_DATA_BIT_SET(iTarget, eVEHICLEDATABITSET_AMBUSH_TARGET)
		CASE eENTITYTYPE_PLAYER				RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TARGET_VALID_FOR_AMBUSH(INT iTarget, ENTITY_INDEX targetId)
	IF logic.Ambush.IsTargetValid != NULL
		RETURN CALL logic.Ambush.IsTargetValid(iTarget, targetId)
	ENDIF
	
	IF iTarget = -1
		RETURN FALSE
	ENDIF
	
	SWITCH GET_AMBUSH_TARGET_ENTITY_TYPE()
		CASE eENTITYTYPE_MISSION_ENTITY
			IF IS_MISSION_ENTITY_BIT_SET(iTarget, eMISSIONENTITYBITSET_INSIDE_MISSION_INTERIOR)
				RETURN FALSE
			ENDIF
			
			IF GET_AMBUSH_DROPOFF_DISMISS_RANGE() = (-1.0)
			OR sMissionEntityLocal.sMissionEntity[iTarget].fDistanceFromDropoff > GET_AMBUSH_DROPOFF_DISMISS_RANGE()
				IF sMissionEntityLocal.sMissionEntity[iTarget].holderPlayerId != INVALID_PLAYER_INDEX()
					IF IS_PLAYER_IN_MOD_SHOP(sMissionEntityLocal.sMissionEntity[iTarget].holderPlayerId)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE eENTITYTYPE_PLAYER	
			PLAYER_INDEX player
			player = INT_TO_NATIVE(PLAYER_INDEX, iTarget)
			IF IS_NET_PLAYER_OK(player, FALSE)
			AND NETWORK_IS_PLAYER_A_PARTICIPANT(player)
				PARTICIPANT_INDEX partId
				partId = NETWORK_GET_PARTICIPANT_INDEX(player)
				IF IS_GENERIC_CLIENT_BIT_SET(partId, eGENERICCLIENTBITSET_INSIDE_MISSION_INTERIOR)
				OR IS_GENERIC_CLIENT_BIT_SET(partId, eGENERICCLIENTBITSET_ENTERING_MISSION_INTERIOR)
					RETURN FALSE
				ENDIF
			ELSE 
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE eENTITYTYPE_PED
			IF IS_PED_BIT_SET(iTarget, ePEDBITSET_INSIDE_MISSION_INTERIOR)
				RETURN FALSE
			ENDIF
		BREAK
		
		CASE eENTITYTYPE_VEHICLE
			IF IS_VEHICLE_BIT_SET(iTarget, eVEHICLEBITSET_INSIDE_MISSION_INTERIOR)
				RETURN FALSE
			ENDIF
		BREAK
		
//		CASE eENTITYTYPE_PROP
//			IF IS_PROP_BIT_SET(iTarget, ePROPBITSET_INSIDE_MISSION_INTERIOR)
//				RETURN FALSE
//			ENDIF
//		BREAK
	ENDSWITCH
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_PRIORITISE_AMBUSH_TARGET(INT iTarget, ENTITY_INDEX targetId)
	IF logic.Ambush.ShouldPrioritiseTarget != null
		RETURN CALL logic.Ambush.ShouldPrioritiseTarget(iTarget, targetId)
	ENDIF
	
	SWITCH GET_AMBUSH_TARGET_ENTITY_TYPE()
		CASE eENTITYTYPE_MISSION_ENTITY
			IF sMissionEntityLocal.sMissionEntity[iTarget].holderPlayerId != INVALID_PLAYER_INDEX()
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_RANDOM_AMBUSH_TARGET(INT iAmbushIndex)
	UNUSED_PARAMETER(iAmbushIndex)
	
	INT iTargetPool[NUM_NETWORK_PLAYERS]
	INT iPriorityPool[NUM_NETWORK_PLAYERS]
	INT iTarget, iFoundCount, iPriorityCount, iBackupTarget
	INT iSelectedTarget = -1
	ENTITY_INDEX targetId
	
	REPEAT GET_AMBUSH_TARGET_MAX_LOOP() iTarget
		IF IS_ENTITY_AN_AMBUSH_TARGET(iTarget)
			targetId = GET_AMBUSH_TARGET_ENTITY_INDEX(iTarget)
			IF DOES_ENTITY_EXIST(targetId)
				IF IS_TARGET_VALID_FOR_AMBUSH(iTarget, targetId)
					IF SHOULD_PRIORITISE_AMBUSH_TARGET(iTarget, targetId)
						iPriorityPool[iPriorityCount] = iTarget
						iPriorityCount++
					ENDIF
					
					iTargetPool[iFoundCount] = iTarget
					iFoundCount++
				ENDIF
				
				iBackupTarget = iTarget
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF iPriorityCount > 0 
		iSelectedTarget = iPriorityPool[GET_RANDOM_INT_IN_RANGE(0, iPriorityCount)]
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH_", iAmbushIndex,"] GET_RANDOM_AMBUSH_TARGET - Priority target found. Returning target #", iSelectedTarget, " of type ", GET_ENTITY_TYPE_NAME(GET_AMBUSH_TARGET_ENTITY_TYPE()))
	ELIF iFoundCount > 0
		iSelectedTarget = iTargetPool[GET_RANDOM_INT_IN_RANGE(0, iFoundCount)]
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH_", iAmbushIndex,"] GET_RANDOM_AMBUSH_TARGET - Target found. Returning target #", iSelectedTarget, " of type ", GET_ENTITY_TYPE_NAME(GET_AMBUSH_TARGET_ENTITY_TYPE()))
	ELSE
		iSelectedTarget = iBackupTarget
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH_", iAmbushIndex,"] GET_RANDOM_AMBUSH_TARGET - No target found. Returning backup target #", iSelectedTarget, " of type ", GET_ENTITY_TYPE_NAME(GET_AMBUSH_TARGET_ENTITY_TYPE()))
	ENDIF
	
	RETURN iSelectedTarget
ENDFUNC

FUNC INT GET_AMBUSH_INDEX_FROM_ENTITY(eENTITY_TYPE eType, INT iEntity)
	INT iAmbush
	SWITCH eType
		CASE eENTITYTYPE_VEHICLE
			REPEAT MAX_NUM_AMBUSH_VEHICLES iAmbush
				// url:bugstar:7467758 - Freemode Creator: Could we add the option to customise the waves to not use identical vehicle setups for every ambush?
				#IF MAX_NUM_AMBUSH_VEHICLES_VARIATIONS
					// ambush vehicle variations behaviour - Checking only for the generated next ambush vehicle index
					IF serverBD.sAmbush.sVehicle[iAmbush].iVariationIndex = iEntity
						RETURN iAmbush
					ENDIF
				#ENDIF
				
				#IF NOT MAX_NUM_AMBUSH_VEHICLES_VARIATIONS
					// default behaviour - checking for ambush vehicle index
					IF data.Ambush.Vehicle[iAmbush].iIndex = iEntity
						RETURN iAmbush
					ENDIF
				#ENDIF				
			ENDREPEAT
		BREAK
		CASE eENTITYTYPE_PED
			REPEAT MAX_NUM_AMBUSH_VEHICLES iAmbush
				// url:bugstar:7467758 - Freemode Creator: Could we add the option to customise the waves to not use identical vehicle setups for every ambush?
				#IF MAX_NUM_AMBUSH_VEHICLES_VARIATIONS
					// ambush vehicle variations behaviour - Checking only for the generated next ambush vehicle index
					IF serverBD.sAmbush.sVehicle[iAmbush].iVariationIndex = data.Ped.Peds[iEntity].iVehicle
						RETURN iAmbush
					ENDIF
				#ENDIF
				
				#IF NOT MAX_NUM_AMBUSH_VEHICLES_VARIATIONS
					// default behaviour - checking for ambush vehicle index
					IF data.Ambush.Vehicle[iAmbush].iIndex = data.Ped.Peds[iEntity].iVehicle
					 	RETURN iAmbush
					ENDIF
				#ENDIF
			ENDREPEAT
		BREAK
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC INT GET_AMBUSH_ENTITY_TARGET_INDEX(INT iAmbushIndex, BOOL bReadOnly = FALSE)
	IF iAmbushIndex != (-1)
		IF NOT bReadOnly
			IF NOT DOES_ENTITY_EXIST(GET_AMBUSH_TARGET_ENTITY_INDEX(serverBD.sAmbush.sVehicle[iAmbushIndex].iTarget))
				serverBD.sAmbush.sVehicle[iAmbushIndex].iTarget = GET_RANDOM_AMBUSH_TARGET(iAmbushIndex)
			ENDIF
		ENDIF
		RETURN serverBD.sAmbush.sVehicle[iAmbushIndex].iTarget
	ENDIF
	RETURN -1
ENDFUNC

FUNC ENTITY_INDEX GET_AMBUSH_TARGET(INT iAmbushIndex)
	RETURN GET_AMBUSH_TARGET_ENTITY_INDEX(GET_AMBUSH_ENTITY_TARGET_INDEX(iAmbushIndex))
ENDFUNC

FUNC ENTITY_INDEX GET_AMBUSH_TARGET_FOR_VEHICLE(INT iVehicle)
	RETURN GET_AMBUSH_TARGET_ENTITY_INDEX(GET_AMBUSH_ENTITY_TARGET_INDEX(GET_AMBUSH_INDEX_FROM_ENTITY(eENTITYTYPE_VEHICLE, iVehicle)))
ENDFUNC

FUNC ENTITY_INDEX GET_AMBUSH_TARGET_FOR_PED(INT iPed)
	RETURN GET_AMBUSH_TARGET_ENTITY_INDEX(GET_AMBUSH_ENTITY_TARGET_INDEX(GET_AMBUSH_INDEX_FROM_ENTITY(eENTITYTYPE_PED, iPed)))
ENDFUNC

FUNC INT GET_AMBUSH_SPAWN_TIMER()
	RETURN data.Ambush.iSpawnDelay
ENDFUNC

FUNC INT GET_AMBUSH_RESPAWN_DELAY()
	RETURN data.Ambush.iRespawnDelay
ENDFUNC

FUNC BOOL SHOULD_CLEANUP_AMBUSH_FOR_INVALID_TARGET(INT iAmbush)
	IF NOT IS_TARGET_VALID_FOR_AMBUSH(GET_AMBUSH_ENTITY_TARGET_INDEX(iAmbush, TRUE), GET_AMBUSH_TARGET(iAmbush))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_SEND_AMBUSH_IN_WAVES()
	IF IS_DATA_BIT_SET(eDATABITSET_ENABLE_NON_WAVE_AMBUSH)
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_FORCE_CLEANUP_AMBUSH()
	IF IS_GENERIC_BIT_SET(eGENERICBITSET_AMBUSH_ACTIVATED)
		IF logic.Ambush.ForceCleanup != NULL
		AND CALL logic.Ambush.ForceCleanup()
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_RESPAWN_AMBUSH()
	IF IS_GENERIC_BIT_SET(eGENERICBITSET_AMBUSH_ACTIVATED)
	
		IF logic.Ambush.Respawn = NULL
			RETURN TRUE
		ENDIF
		
		RETURN CALL logic.Ambush.Respawn()
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DEFAULT_SHOULD_ACTIVATE_AMBUSH()
	RETURN HAS_ANY_MISSION_ENTITY_BEEN_COLLECTED_FOR_THE_FIRST_TIME()
ENDFUNC

#IF NOT DEFINED(SHOULD_ACTIVATE_AMBUSH)
FUNC BOOL SHOULD_ACTIVATE_AMBUSH()

	IF CALL logic.Ambush.ShouldActivate()
		IF HAS_NET_TIMER_EXPIRED(serverBD.sAmbush.stSpawnTimer, GET_AMBUSH_SPAWN_TIMER())
			SET_GENERIC_BIT(eGENERICBITSET_AMBUSH_ACTIVATED)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL ARE_PEDS_IN_AMBUSH_VEHICLE_DEAD(INT iVehicle)
	INT iAmbush = GET_AMBUSH_INDEX_FROM_ENTITY(eENTITYTYPE_VEHICLE, iVehicle)
	IF iAmbush != (-1)
		RETURN IS_BIT_SET(serverBD.sAmbush.iPedsDeadBitset, iAmbush)
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_AMBUSH_PEDS_DATA(INT& iAmbushPedsDeadBitset)
	INT iAmbush
	REPEAT MAX_NUM_AMBUSH_VEHICLES iAmbush
		IF IS_BIT_SET(iAmbushPedsDeadBitset, iAmbush)
			IF NOT IS_BIT_SET(serverBD.sAmbush.iPedsDeadBitset, iAmbush)
				SET_BIT(serverBD.sAmbush.iPedsDeadBitset, iAmbush)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH_", iAmbush, "] MAINTAIN_AMBUSH_PEDS_DATA - All peds in ambush vehicle dead.")
			ENDIF
		ELSE
			IF IS_BIT_SET(serverBD.sAmbush.iPedsDeadBitset, iAmbush)
				CLEAR_BIT(serverBD.sAmbush.iPedsDeadBitset, iAmbush)
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH_", iAmbush, "] MAINTAIN_AMBUSH_PEDS_DATA - All peds in ambush vehicle no longer dead.")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_AMBUSH_VEHICLE_DATA(BOOL& bAllVehiclesDead)
	IF serverBD.sAmbush.bAllVehiclesDead != bAllVehiclesDead
		serverBD.sAmbush.bAllVehiclesDead = bAllVehiclesDead
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] MAINTAIN_AMBUSH_VEHICLE_DATA - All ambush vehicles dead: ", GET_STRING_FROM_BOOL(bAllVehiclesDead))
	ENDIF
ENDPROC

FUNC BOOL SHOULD_RESPAWN_AMBUSH_VEHICLE(INT iVehicle)
	#IF IS_DEBUG_BUILD
	INT iAmbushIndex = GET_AMBUSH_INDEX_FROM_ENTITY(eENTITYTYPE_VEHICLE, iVehicle)
	#ENDIF
	
	IF NOT SHOULD_RESPAWN_AMBUSH()
		#IF IS_DEBUG_BUILD
		IF sDebugVars.bDoAmbushPrints
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] [AMBUSH_", iAmbushIndex, "] SHOULD_RESPAWN_AMBUSH_VEHICLE = FALSE - NOT SHOULD_RESPAWN_AMBUSH")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF SHOULD_FORCE_CLEANUP_AMBUSH()
		#IF IS_DEBUG_BUILD
		IF sDebugVars.bDoAmbushPrints
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] [AMBUSH_", iAmbushIndex, "] SHOULD_RESPAWN_AMBUSH_VEHICLE = FALSE - logic.Ambush.ForceCleanup is currently true.")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF SHOULD_SEND_AMBUSH_IN_WAVES()
	AND HAVE_ALL_AMBUSH_WAVES_BEEN_SENT()
		#IF IS_DEBUG_BUILD
		IF sDebugVars.bDoAmbushPrints
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] [AMBUSH_", iAmbushIndex, "] SHOULD_RESPAWN_AMBUSH_VEHICLE = FALSE - All waves have been sent. Sent: ", serverBD.sAmbush.iWavesSent, " - Max: ", GET_MAX_AMBUSH_WAVES())
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT ARE_PEDS_IN_AMBUSH_VEHICLE_DEAD(iVehicle)	
		#IF IS_DEBUG_BUILD
		IF sDebugVars.bDoAmbushPrints
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] [AMBUSH_", iAmbushIndex, "] SHOULD_RESPAWN_AMBUSH_VEHICLE = FALSE - ARE_PEDS_IN_AMBUSH_VEHICLE_DEAD = FALSE.")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF
	
	IF SHOULD_SEND_AMBUSH_IN_WAVES()
	AND NOT IS_AMBUSH_WAVE_DEAD()
		#IF IS_DEBUG_BUILD
		IF sDebugVars.bDoAmbushPrints
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] [AMBUSH_", iAmbushIndex, "] SHOULD_RESPAWN_AMBUSH_VEHICLE = FALSE - IS_AMBUSH_WAVE_DEAD() = FALSE.")
		ENDIF
		#ENDIF	
		RETURN FALSE
	ENDIF
	
	//RESET_NET_TIMER(serverBD.sAmbush.stSpawnTimer)
	RETURN TRUE
ENDFUNC

#IF MAX_NUM_AMBUSH_VEHICLES_VARIATIONS

//url:bugstar:7467758 - Freemode Creator: Could we add the option to customise the waves to not use identical vehicle setups for every ambush?
// function collection for "ambush vehicle variations feature

/// PURPOSE:
///    Get Random ambush vehicle index using ambush vehicle variations
/// PARAMS:
///    iAmbush index of ambush dataset 
/// RETURNS:  
FUNC INT GET_RANDOM_AMBUSH_VEHICLE_INDEX(INT iAmbush)

	// Set variation count
	IF data.Ambush.Vehicle[iAmbush].iVariationCount > 0
		// ambush vehicle variations behaviour
		INT iAmbushVariationIndex = 0
		INT iAmbushVariation[MAX_NUM_AMBUSH_VEHICLES_VARIATIONS+1]
		
		// Create list with potential vehicle indexes
		iAmbushVariation[0] = data.Ambush.Vehicle[iAmbush].iIndex
		REPEAT MAX_NUM_AMBUSH_VEHICLES_VARIATIONS iAmbushVariationIndex
			IF data.Ambush.Vehicle[iAmbush].Variations[iAmbushVariationIndex].iIndex != (-1)
				iAmbushVariation[iAmbushVariationIndex+1] = data.Ambush.Vehicle[iAmbush].Variations[iAmbushVariationIndex].iIndex
			ENDIF
		ENDREPEAT
		
		// Select random vehicle index
		INT iRandomIndex = iAmbushVariation[GET_RANDOM_INT_IN_RANGE(0, data.Ambush.Vehicle[iAmbush].iVariationCount + 1)]
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] GET_RANDOM_AMBUSH_VEHICLE_INDEX - Random: ", iRandomIndex)
		RETURN iRandomIndex
	ELSE
		// default behaviour - fallback if no variations are set
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] GET_RANDOM_AMBUSH_VEHICLE_INDEX - Default: ", data.Ambush.Vehicle[iAmbush].iIndex)
		RETURN data.Ambush.Vehicle[iAmbush].iIndex
	ENDIF

ENDFUNC

/// PURPOSE:
///    Randomizes the vehicleIndex for the next wave of spawns
PROC CREATE_RANDOM_AMBUSH_WAVE()
	INT iAmbush
	REPEAT MAX_NUM_AMBUSH_VEHICLES iAmbush
		serverBD.sAmbush.sVehicle[iAmbush].iVariationIndex = GET_RANDOM_AMBUSH_VEHICLE_INDEX(iAmbush)
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] CREATE_RANDOM_AMBUSH_WAVE - iAmbush: ", iAmbush, "; iIndex: ", serverBD.sAmbush.sVehicle[iAmbush].iVariationIndex)
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    checks if the given vehicle index is equal to the current set ambush vehicle index
/// PARAMS:
///    iAmbush - ambush vehicle entry
///    iVehicle - vehicle index to compare
/// RETURNS:
///    
FUNC BOOL IS_AMBUSH_VEHICLE_AVAILABLE_FOR_WAVE(INT iAmbush, INT iVehicle)
	
	IF iAmbush < data.Ambush.iCount
		// Vehicle Index for current wave
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] IS_AMBUSH_VEHICLE_AVAILABLE_FOR_WAVE - iVehicle: ", iVehicle, "; iVariationIndex: ", serverBD.sAmbush.sVehicle[iAmbush].iVariationIndex)
		RETURN iVehicle = serverBD.sAmbush.sVehicle[iAmbush].iVariationIndex
	ELSE
		// iAmbush out of range
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] IS_AMBUSH_VEHICLE_AVAILABLE_FOR_WAVE - iVehicle: ", iVehicle, "; - Attention the index given is out of range!")
		RETURN FALSE
	ENDIF

ENDFUNC

#ENDIF

// --------------------

PROC RESET_AMBUSH_VEHICLE_DATA(INT iAmbush)
	serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromTarget = INVALID_DISTANCE
	serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromDropoff = INVALID_DISTANCE
	serverBD.sAmbush.sVehicle[iAmbush].iTarget = -1
	
	#IF MAX_NUM_AMBUSH_VEHICLES_VARIATIONS
	// url:bugstar:7467758 - Freemode Creator: Could we add the option to customise the waves to not use identical vehicle setups for every ambush?
	// On each reset reshuffle the next ambush-wave
	CREATE_RANDOM_AMBUSH_WAVE()
	#ENDIF
	
ENDPROC

PROC RESET_ALL_AMBUSH_VEHICLE_DATA()
	INT iAmbush
	REPEAT MAX_NUM_AMBUSH_VEHICLES iAmbush
		RESET_AMBUSH_VEHICLE_DATA(iAmbush)
	ENDREPEAT
ENDPROC

PROC MAINTAIN_AMBUSH_SERVER()
	IF SHOULD_SEND_AMBUSH_IN_WAVES()
		IF IS_AMBUSH_WAVE_DEAD()
			IF HAS_NET_TIMER_STARTED(serverBD.sAmbush.stSpawnTimer)
				RESET_NET_TIMER(serverBD.sAmbush.stSpawnTimer)
				RESET_ALL_AMBUSH_VEHICLE_DATA()
				serverBD.sAmbush.iWavesSent++
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] MAINTAIN_AMBUSH_SERVER - Incremented waves to ", serverBD.sAmbush.iWavesSent)
			ENDIF
		ENDIF
	ELSE
		INT iAmbush
		REPEAT MAX_NUM_AMBUSH_VEHICLES iAmbush
			IF ARE_ALL_AMBUSH_PEDS_IN_VEHICLE_DEAD(iAmbush)
			AND serverBD.sAmbush.sVehicle[iAmbush].iTarget != (-1)
				RESET_AMBUSH_VEHICLE_DATA(iAmbush)
				serverBD.sAmbush.iWavesSent++
				PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [AMBUSH] MAINTAIN_AMBUSH_SERVER - Incremented waves to ", serverBD.sAmbush.iWavesSent, " after ambush ", iAmbush, " peds died on non-wave ambush.")
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC MAINTAIN_VEHICLE_AMBUSH_CHECKS(INT iVehicle, VEHICLE_INDEX vehId)
	IF NOT IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_AMBUSH_VEHICLE)
		EXIT
	ENDIF
	
	INT iAmbush = GET_AMBUSH_INDEX_FROM_ENTITY(eENTITYTYPE_VEHICLE, iVehicle)
	IF iAmbush = (-1)
		ASSERTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] MAINTAIN_VEHICLE_AMBUSH_CHECKS - Ambush index is -1! Ambush is not set up correctly, or this vehicle is incorrectly marked as an ambush vehicle.")
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] [AMBUSH_", iAmbush, "] MAINTAIN_VEHICLE_AMBUSH_CHECKS - Ambush index is -1! Ambush is not set up correctly, or this vehicle is incorrectly marked as an ambush vehicle")
		EXIT
	ENDIF
	
	VECTOR vAmbushCoords = GET_ENTITY_COORDS(vehId, FALSE)
	FLOAT fDistance
	
	// Distance from Dropoff
	fDistance = VDIST(vAmbushCoords, GET_DROPOFF_COORDS())
	IF ABSF(fDistance - serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromDropoff) > 10.0
		#IF IS_DEBUG_BUILD
		IF sDebugVars.bDoAmbushPrints
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] [AMBUSH_", iAmbush, "] MAINTAIN_VEHICLE_AMBUSH_CHECKS - Ambush distance from dropoff: ", fDistance, " - Stored value: ", serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromDropoff, " - Difference: ", ABSF(fDistance - serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromDropoff))
		ENDIF
		#ENDIF
		
		serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromDropoff = fDistance
	ENDIF
	
	// Distance from Target
	ENTITY_INDEX targetId = GET_AMBUSH_TARGET(iAmbush)
	IF DOES_ENTITY_EXIST(targetId)
		fDistance = VDIST(vAmbushCoords, GET_ENTITY_COORDS(targetId, FALSE))
		IF ABSF(fDistance - serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromTarget) > 10.0
			#IF IS_DEBUG_BUILD
			IF sDebugVars.bDoAmbushPrints
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] [AMBUSH_", iAmbush, "] MAINTAIN_VEHICLE_AMBUSH_CHECKS - Ambush distance from target: ", fDistance, " - Stored value: ", serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromTarget, " - Difference: ", ABSF(fDistance - serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromTarget))
			ENDIF
			#ENDIF
			
			serverBD.sAmbush.sVehicle[iAmbush].fDistanceFromTarget = fDistance
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns ped index of the ambush target, whether this is the target if it's a ped type target or the driver/holder of a vehicle or mission entity. Used for combat task checks.  
FUNC PED_INDEX GET_AMBUSH_TARGET_PED_INDEX(INT iAmbush)
	INT iTarget
	SWITCH GET_AMBUSH_TARGET_ENTITY_TYPE()
		CASE eENTITYTYPE_MISSION_ENTITY
			iTarget = GET_AMBUSH_ENTITY_TARGET_INDEX(iAmbush)
			IF iTarget != (-1)
				IF sMissionEntityLocal.sMissionEntity[iTarget].holderPlayerId != INVALID_PLAYER_INDEX()
					RETURN GET_PLAYER_PED(sMissionEntityLocal.sMissionEntity[iTarget].holderPlayerId)
				ENDIF
			ENDIF
		BREAK
		CASE eENTITYTYPE_VEHICLE
			VEHICLE_INDEX vehId
			vehId = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_AMBUSH_TARGET(iAmbush))
			IF DOES_ENTITY_EXIST(vehId)
				RETURN GET_PED_IN_VEHICLE_SEAT(vehId, VS_DRIVER)
			ENDIF
		BREAK
		CASE eENTITYTYPE_PLAYER
		CASE eENTITYTYPE_PED			
			IF DOES_ENTITY_EXIST(GET_AMBUSH_TARGET(iAmbush))
				RETURN GET_PED_INDEX_FROM_ENTITY_INDEX(GET_AMBUSH_TARGET(iAmbush))
			ENDIF
		BREAK
	ENDSWITCH
	RETURN INT_TO_NATIVE(PED_INDEX, -1)
ENDFUNC

FUNC FLOAT GET_AMBUSH_TARGET_SPEED(INT iAmbush)
	ENTITY_INDEX targetId = GET_AMBUSH_TARGET_ENTITY_INDEX(GET_AMBUSH_ENTITY_TARGET_INDEX(iAmbush))
	IF DOES_ENTITY_EXIST(targetId)
		RETURN GET_ENTITY_SPEED(targetId)
	ENDIF
	RETURN 0.0
ENDFUNC

PROC SETUP_AMBUSH_PED_DEAD_TEMP_BITSET(INT& iBitset)
	INT iAmbush
	REPEAT MAX_NUM_AMBUSH_VEHICLES iAmbush
		SET_BIT(iBitset, iAmbush)
	ENDREPEAT
ENDPROC

PROC CLEAR_TEMP_AMBUSH_PED_DEAD_BIT(INT& iBitset, INT iPed)
	IF IS_PED_BIT_SET(iPed, ePEDBITSET_AMBUSH_PED)
		INT iAmbush = GET_AMBUSH_INDEX_FROM_ENTITY(eENTITYTYPE_PED, iPed)
		IF iAmbush != (-1)
			CLEAR_BIT(iBitset, iAmbush)
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_TEMP_AMBUSH_VEHICLE_DEAD_FLAG(INT iVehicle, BOOL& bAllVehiclesDead)
	IF IS_VEHICLE_BIT_SET(iVehicle, eVEHICLEBITSET_AMBUSH_VEHICLE)
	
		#IF MAX_NUM_AMBUSH_VEHICLES_VARIATIONS
		// url:bugstar:7467758 - Freemode Creator: Could we add the option to customise the waves to not use identical vehicle setups for every ambush?
		// Clear flags need to listen to the variation, since all ambush vehicles are flagged, but only the availablity for the current shuffle counts
		// ambush vehicle variations behaviour
		INT iAmbush
		REPEAT MAX_NUM_AMBUSH_VEHICLES iAmbush
			IF iVehicle = serverBD.sAmbush.sVehicle[iAmbush].iVariationIndex
//				PRINTLN("CLEAR_TEMP_AMBUSH_VEHICLE_DEAD_FLAG - using variations iVehicle: ", iVehicle, "; available; state: ", GET_VEHICLE_STATE_NAME(GET_VEHICLE_STATE(iVehicle)))
				bAllVehiclesDead = FALSE
			ENDIF
		ENDREPEAT		
		#ENDIF
	
		#IF NOT MAX_NUM_AMBUSH_VEHICLES_VARIATIONS
		// default behaviour
		bAllVehiclesDead = FALSE
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_AMBUSH_SPAWN_COORD_VALID(INT iVehicle, VECTOR vSpawnCoord, VECTOR vTargetCoord, ENTITY_INDEX targetID)
	UNUSED_PARAMETER(iVehicle)
	
	#IF IS_DEBUG_BUILD
	INT iAmbushIndex = GET_AMBUSH_INDEX_FROM_ENTITY(eENTITYTYPE_VEHICLE, iVehicle)
	#ENDIF
	
	FLOAT fDistanceFromTarget = VDIST(vSpawnCoord, vTargetCoord)
	IF fDistanceFromTarget > GET_AMBUSH_SPAWN_RANGE()
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] [AMBUSH_", iAmbushIndex, "] IS_SPAWN_COORD_IN_DIRECTION_VALID - Distance from target (", fDistanceFromTarget, ") greater than target spawn range (", GET_AMBUSH_SPAWN_RANGE(), ")")
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] [AMBUSH_", iAmbushIndex, "] IS_SPAWN_COORD_IN_DIRECTION_VALID - vSpawnCoord = ", vSpawnCoord, ", vTargetCoord = ", vTargetCoord)
		RETURN FALSE
	ENDIF
	
	FLOAT fDistanceFromDropoff = VDIST(vSpawnCoord, GET_DROPOFF_COORDS())
	IF fDistanceFromDropoff < GET_AMBUSH_DROPOFF_DISMISS_RANGE()
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] [AMBUSH_", iAmbushIndex, "] IS_SPAWN_COORD_IN_DIRECTION_VALID - Distance from dropoff (", fDistanceFromDropoff, ") within dismiss range (", GET_AMBUSH_DROPOFF_DISMISS_RANGE(), ")")
		PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] [AMBUSH_", iAmbushIndex, "] IS_SPAWN_COORD_IN_DIRECTION_VALID - vSpawnCoord = ", vSpawnCoord, ", vTargetCoord = ", vTargetCoord)
		RETURN FALSE
	ENDIF

	IF DOES_ENTITY_EXIST(targetID)
		VECTOR vOffsetFromTarget = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(targetID, vSpawnCoord)
		IF ABSF(vOffsetFromTarget.x) > GET_AMBUSH_SPAWN_OFFSET_RANGE()
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] [AMBUSH_", iAmbushIndex, "] IS_SPAWN_COORD_IN_DIRECTION_VALID - x-offset (", ABSF(vOffsetFromTarget.x), ") between spawn and target greater than range (", GET_AMBUSH_SPAWN_OFFSET_RANGE(), ")")
			PRINTLN("[",scriptName,"] - [", GET_CURRENT_VARIATION_STRING(), "] [VEH_", iVehicle, "] [AMBUSH_", iAmbushIndex, "] IS_SPAWN_COORD_IN_DIRECTION_VALID - vSpawnCoord = ", vSpawnCoord, ", vTargetCoord = ", vTargetCoord)
			RETURN FALSE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

#ENDIF
