// Name: 		net_kill_strip.sch
// Author: 		James Adwick
// Date: 		19/06/2012
// Purpose: 	Scaleform loaded when a player dies.
// 				Summarises details of the player who killed them and their progress in MP.
// 				Player must press X to respawn (handled as usual by net_spawn.sch)

USING "net_kill_strip_helper.sch"
USING "commands_network.sch"
USING "commands_camera.sch"
USING "hud_drawing.sch"
USING "net_big_message.sch"
USING "net_common_functions.sch"
USING "mp_scaleform_functions.sch"
USING "net_player_headshots.sch"
USING "FM_unlocks_header.sch"
USING "net_cash_transactions.sch"
USING "freemode_events_header_private.sch"
USING "net_grief_passive.sch"

CONST_INT WASTED_SOUND_DELAY	750

// For keyboard and mouse so we can use mouse click to respawn faster.
CONTROL_ACTION caRespawnInput

// --------------------- PUBLIC ACCESSORS -------------------

//PURPOSE: Returns TRUE if we should do the quick respawn
FUNC BOOL DO_QUICK_RESPAWN()
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
		RETURN FALSE
	ENDIF
	
	IF g_bVSMission = TRUE 
		RETURN FALSE
	ENDIF
	
	IF IS_ON_GTA_RACE_GLOBAL_SET()
	AND NOT IS_ARENA_RACE()
	
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_AN_ANIMAL()
		RETURN TRUE
	ENDIF
	
	IF g_ImpromptuSpawn // If Impromptu launching
	
		RETURN TRUE
	ENDIF
	
	IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Returns TRUE if the kill strip was shown for the previous death. (FALSE if it was skipped)
FUNC BOOL HAS_KILL_STRIP_BEEN_SHOWN()
	RETURN (MPGlobals.g_KillStrip.iPreviousState != ENUM_TO_INT(STATE_KILL_STRIP_SKIP))
ENDFUNC

/// PURPOSE: Sets global flag so killstrip will auto respawn (no need for X button press)
PROC SET_KILLSTRIP_AUTO_RESPAWN(BOOL bAutoRespawn)
	#IF IS_DEBUG_BUILD
	IF bAutoRespawn
		PRINTLN("SET_KILLSTRIP_AUTO_RESPAWN, TRUE")
	ELSE
		PRINTLN("SET_KILLSTRIP_AUTO_RESPAWN, FALSE")
	ENDIF
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	MPGlobals.g_bAutoRespawnOnKillstrip = bAutoRespawn
ENDPROC

/// PURPOSE: Sets the global time the killstrip will wait before auto respawning
PROC SET_KILLSTRIP_AUTO_RESPAWN_TIME(INT iTime)
	MPGlobals.g_iAutoRespawnOnKillstripTime = iTime
ENDPROC

/// PURPOSE: Returns TRUE if killstrip should auto respawn 
FUNC BOOL IS_KILLSTRIP_SET_TO_AUTO_RESPAWN()
	RETURN MPGlobals.g_bAutoRespawnOnKillstrip
ENDFUNC

/// PURPOSE: Returns the time killstrip should wait before respawning automatically
FUNC INT GET_KILLSTRIP_AUTO_RESPAWN_TIME()
	RETURN MPGlobals.g_iAutoRespawnOnKillstripTime
ENDFUNC
/// PURPOSE:Should the impromptu DM stuff work?
FUNC BOOL CAN_TRIGGER_IMPROMPTU_DM_KILLSTRIP()
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		RETURN FALSE
	ENDIF
	IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	IF NOT HAS_PASSIVE_MODE_CUTSCENE_BEEN_DONE()
		RETURN FALSE
	ENDIF
	IF MPGlobals.g_KillStrip.killerData.killerID = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	IF MPGlobals.g_KillStrip.killerData.killerID = PLAYER_ID()
		RETURN FALSE
	ENDIF
	IF NOT IS_NET_PLAYER_OK(MPGlobals.g_KillStrip.killerData.killerID, FALSE, TRUE)
		RETURN FALSE
	ENDIF
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.g_KillStrip.killerData.killerID)].iXPBitset, ciGLOBAL_XP_BIT_ON_IMPROMPTU_DM)
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_CompletedInitalAmbientTut)
		//-- I haven't completed tutorial / unlocked deathmatches
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(MPGlobals.g_KillStrip.killerData.killerID)].iFmTutProgBitset, biTrigTut_CompletedInitalAmbientTut)
		//-- The player who killed me hasn't completed tutorial / unlocked deathmatches
		RETURN FALSE
	ENDIF
	
	IF SHOULD_BLOCK_KILL_STRIP_ONE_ON_ONE_AND_PASSIVE_MODE_OPTIONS(PLAYER_ID())
		//-- The player is on a new freemode event
		RETURN FALSE
	ENDIF
	
	//If players are in the same gang then remove this option
	IF GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), MPGlobals.g_KillStrip.killerData.killerID)
		RETURN FALSE
	ENDIF
	
	// 2577675 - Don't allow 1v1 for any boss work
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID(), TRUE)
	OR GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(MPGlobals.g_KillStrip.killerData.killerID, TRUE)
		RETURN FALSE
	ENDIF
	
	// If either the local or remote player is considered critical, remove this option
	IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
	OR IS_PLAYER_CRITICAL_TO_ANY_EVENT(MPGlobals.g_KillStrip.killerData.killerID)
	//Or the remote player is on a time trial
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(MPGlobals.g_KillStrip.killerData.killerID, MPAM_TYPE_TIME_TRIAL)
	OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(MPGlobals.g_KillStrip.killerData.killerID, MPAM_TYPE_RC_TIME_TRIAL)
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(g_sGriefPassivePlayer[NATIVE_TO_INT(MPGlobals.g_KillStrip.killerData.killerID)].GriefDurationTimer)
		PRINTLN(" [CS_IMPROMPTU] CAN_TRIGGER_IMPROMPTU_DM_KILLSTRIP - ", GET_PLAYER_NAME(MPGlobals.g_KillStrip.killerData.killerID), " is currently ghosted for griefing.")
		RETURN FALSE
	ENDIF
	
	IF PLAYER_USING_ORBITAL(MPGlobals.g_KillStrip.killerData.killerID)
		PRINTLN(" [CS_IMPROMPTU] CAN_TRIGGER_IMPROMPTU_DM_KILLSTRIP - ", GET_PLAYER_NAME(MPGlobals.g_KillStrip.killerData.killerID), " is currently using the Orbital Cannon.")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_AN_ANIMAL(MPGlobals.g_KillStrip.killerData.killerID)
	OR IS_LOCAL_PLAYER_AN_ANIMAL()
		PRINTLN(" [CS_IMPROMPTU] CAN_TRIGGER_IMPROMPTU_DM_KILLSTRIP - ", GET_PLAYER_NAME(MPGlobals.g_KillStrip.killerData.killerID), " is an animal IS_PLAYER_AN_ANIMAL ")
		RETURN FALSE
	ENDIF
	
	PRINTLN(" [CS_IMPROMPTU] CAN_TRIGGER_IMPROMPTU_DM_KILLSTRIP killerID =  ", GET_PLAYER_NAME(MPGlobals.g_KillStrip.killerData.killerID), " i = ", NATIVE_TO_INT(MPGlobals.g_KillStrip.killerData.killerID))
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Returns TRUE if Passive Mode button is available
FUNC BOOL CAN_USE_PASSIVE_MODE_BUTTON()
	/*
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		VEHICLE_INDEX temp_player_veh  
		IF DOES_ENTITY_EXIST(temp_player_veh)
			IF GB_IS_EXPORT_ENTITY(temp_player_veh)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	*/
	
	IF g_IEDeliveryData.vehCurrentlyDelivering != NULL
		IF DOES_ENTITY_EXIST(g_IEDeliveryData.vehCurrentlyDelivering)
			RETURN FALSE
		ENDIF
	ENDIF
	IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PASSIVE_MODE_SELECTED)
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_GRIEF_PASSIVE_SELECTED)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_MP_PASSIVE_MODE_ENABLED()
		RETURN FALSE
	ENDIF
	
	IF NOT CAN_AFFORD_PASSIVE_MODE()
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_PASSIVE_MODE_CUTSCENE_BEEN_DONE()
		RETURN FALSE
	ENDIF
	
	IF IS_BOUNTY_SET_ON_PLAYER(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	//Block if the player is in a gang
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		RETURN FALSE
	ENDIF
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF SHOULD_BLOCK_KILL_STRIP_ONE_ON_ONE_AND_PASSIVE_MODE_OPTIONS(PLAYER_ID())
		//-- The player is on a new freemode event
		RETURN FALSE
	ENDIF
	// If either the local or remote player is considered critical, remove this option
	IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
	OR (MPGlobals.g_KillStrip.killerData.killerID != INVALID_PLAYER_INDEX() AND IS_PLAYER_CRITICAL_TO_ANY_EVENT(MPGlobals.g_KillStrip.killerData.killerID))
	//Or the remote player is on a time trial
	OR (MPGlobals.g_KillStrip.killerData.killerID != INVALID_PLAYER_INDEX() AND IS_PLAYER_ON_MP_AMBIENT_SCRIPT(MPGlobals.g_KillStrip.killerData.killerID, MPAM_TYPE_TIME_TRIAL))
	OR (MPGlobals.g_KillStrip.killerData.killerID != INVALID_PLAYER_INDEX() AND IS_PLAYER_ON_MP_AMBIENT_SCRIPT(MPGlobals.g_KillStrip.killerData.killerID, MPAM_TYPE_RC_TIME_TRIAL))
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.iPassiveModeCoolDownTimer)
	AND NOT HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.iPassiveModeCoolDownTimer, MPGlobalsAmbience.iPassiveModeCoolDownDelay)
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(g_stPassiveModeKillTimer)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_USE_GHOSTED_BUTTON()

	IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PASSIVE_MODE_SELECTED)
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_MP_PASSIVE_MODE_ENABLED()
		RETURN FALSE
	ENDIF
	
	IF NOT CAN_AFFORD_PASSIVE_MODE()
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_PASSIVE_MODE_CUTSCENE_BEEN_DONE()
		RETURN FALSE
	ENDIF
		
	//Block if the player is in a gang
	IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		RETURN FALSE
	ENDIF
	IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	// If either the local or remote player is considered critical, remove this option
	IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
	OR (MPGlobals.g_KillStrip.killerData.killerID != INVALID_PLAYER_INDEX() AND IS_PLAYER_CRITICAL_TO_ANY_EVENT(MPGlobals.g_KillStrip.killerData.killerID))
	//Or the remote player is on a time trial
	OR (MPGlobals.g_KillStrip.killerData.killerID != INVALID_PLAYER_INDEX() AND IS_PLAYER_ON_MP_AMBIENT_SCRIPT(MPGlobals.g_KillStrip.killerData.killerID, MPAM_TYPE_TIME_TRIAL))
	OR (MPGlobals.g_KillStrip.killerData.killerID != INVALID_PLAYER_INDEX() AND IS_PLAYER_ON_MP_AMBIENT_SCRIPT(MPGlobals.g_KillStrip.killerData.killerID, MPAM_TYPE_RC_TIME_TRIAL))
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

//PROC GET_KILL_STRIP_WASTED_STRAPLINE_AND_TIME(TEXT_LABEL_15 &strStrapline, INT &iNumber)
//	
//	iNumber = -1
//	strStrapline = ""
//		
//	IF IS_ON_DEATHMATCH_GLOBAL_SET()
//		
//		strStrapline = "KILL_PLY_RESP"
//		iNumber = -1
//	
//		IF SHOULD_HAVE_DM_RESPAWN_OPTIONS()
//			IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PERK_SELECTED)
//				IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
//					strStrapline = "KILL_LOSER_P"
//				ENDIF
//			ENDIF
//		ENDIF
//	ELSE
//		IF NOT IS_KILLSTRIP_SET_TO_AUTO_RESPAWN()
//		AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
//		AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_START_IM_DM)
//			strStrapline = "KILL_STRIP_W"
//			iNumber = ROUND(KILL_STRIP_RESPAWN_TIME * 0.001)
//		ENDIF
//	ENDIF
//	
//ENDPROC

// ------------------- KILL STRIP DEATH EFFECTS -------------------


/// PURPOSE: Allow wasted message
PROC TRIGGER_KILL_STRIP_WASTED_MESSAGE(BOOL bAllow = TRUE)
	
	IF bAllow
		PRINTLN("[JA@KILLSTRIP] TRIGGER_KILL_STRIP_WASTED - SET_BIT(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_ALLOW_WASTED)")
		SET_BIT(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_ALLOW_WASTED)
	ELSE
		PRINTLN("[JA@KILLSTRIP] TRIGGER_KILL_STRIP_WASTED - CLEAR_BIT(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_STAGE_2)")
		CLEAR_BIT(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_ALLOW_WASTED)
	ENDIF
	
ENDPROC

// Maintains our timer to trigger second effect
PROC MAINTAIN_KILL_STRIP_DEATH_EFFECT()
	IF MPGlobals.g_bSuppressKillstripDeathEffect
		PRINTLN("[JA@KILLSTRIP] MAINTAIN_KILL_STRIP_DEATH_EFFECT - suppressed by (MPGlobals.g_bSuppressKillstripDeathEffect)")
		EXIT
	ENDIF

	// If we have initialise our effects:
	IF IS_BIT_SET(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_STAGE_1)
	
		IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.g_KillStripDeathEffect.iDeathEffectTimer)) > KILL_STRIP_DEATH_EFFECT_TIMER
			
			PRINTLN("[JA@KILLSTRIP] MAINTAIN_KILL_STRIP_DEATH_EFFECT - Stage 1 time up. Trigger effect 2 with WASTED")
			
			// If timer expires, stop initial effect and trigger second
			TRIGGER_KILL_STRIP_DEATH_EFFECT_STAGE_1(FALSE)
			TRIGGER_KILL_STRIP_DEATH_EFFECT_STAGE_2()
			TRIGGER_KILL_STRIP_WASTED_MESSAGE()
					
			MPGlobals.g_KillStripDeathEffect.iDeathEffectTimer = GET_NETWORK_TIME()
		ENDIF
		
		//Play the First Sound - BUG 1610041
		IF NOT IS_BIT_SET(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_FIRST_SOUND)
			IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.g_KillStripDeathEffect.iSoundEffectTimer)) > WASTED_SOUND_DELAY
				IF HAS_WASTED_AUDIO_BANK_LOADED()
				AND NOT g_LastPlayerAliveTinyRacers
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
						PLAY_SOUND_FRONTEND(-1, "Wasted", "POWER_PLAY_General_Soundset")
					ELSE
						PLAY_SOUND_FRONTEND(-1, "MP_Flash", "WastedSounds")
					ENDIF
				ENDIF
				MPGlobals.g_KillStripDeathEffect.iSoundEffectTimer = GET_NETWORK_TIME()
				SET_BIT(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_FIRST_SOUND)
				PRINTLN("[JA@KILLSTRIP] MAINTAIN_KILL_STRIP_DEATH_EFFECT - FIRST SOUND PLAYED")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_STAGE_2)
		IF NOT IS_BIT_SET(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_ALLOW_ALL_HUD)
			IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.g_KillStripDeathEffect.iDeathEffectTimer)) > KILL_STRIP_DEATH_WASTED_TIMER
				
				PRINTLN("[JA@KILLSTRIP] MAINTAIN_KILL_STRIP_DEATH_EFFECT - Animate WASTED and show all other HUD")
				SET_BIT(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_ALLOW_ALL_HUD)
			ENDIF
		ENDIF
	ENDIF	
	
	//Play the Second Sound - BUG 1610041
	IF IS_BIT_SET(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_FIRST_SOUND)
	AND NOT IS_BIT_SET(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_SECOND_SOUND)
		IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.g_KillStripDeathEffect.iSoundEffectTimer)) > KILL_STRIP_DEATH_EFFECT_TIMER
			IF HAS_WASTED_AUDIO_BANK_LOADED()
				IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetSeven, ciENABLE_POWERPLAY_PICKUP_SOUNDS)
				AND NOT g_LastPlayerAliveTinyRacers
					PLAY_SOUND_FRONTEND(-1, "MP_Impact", "WastedSounds")	//MOVED TO 1sec After this.
				ENDIF
			ENDIF
			SET_BIT(MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset, KILL_STRIP_DEATH_EFFECT_SECOND_SOUND)
			PRINTLN("[JA@KILLSTRIP] MAINTAIN_KILL_STRIP_DEATH_EFFECT - SECOND SOUND PLAYED")
		ENDIF
	ENDIF
	
	// If stage 2 bit is set we don;t need to do anything until we clean up.
ENDPROC

/// PURPOSE: Stops our 2 effects
PROC CLEAR_KILL_STRIP_DEATH_EFFECTS()

	PRINTLN("[JA@KILLSTRIP] CLEAR_KILL_STRIP_DEATH_EFFECTS")
	DEBUG_PRINTCALLSTACK()
	
	IF ANIMPOSTFX_IS_RUNNING("MP_race_crash")
		ANIMPOSTFX_STOP("MP_race_crash")
	ENDIF
	TRIGGER_KILL_STRIP_DEATH_EFFECT_STAGE_1(FALSE)
	TRIGGER_KILL_STRIP_DEATH_EFFECT_STAGE_2(FALSE)
	TRIGGER_KILL_STRIP_WASTED_MESSAGE(FALSE)

	MPGlobals.g_KillStripDeathEffect.iDeathEffectBitset = 0	
ENDPROC


// ------------------- KILL STRIP LOGIC -------------------

PROC ADD_KILL_STRIP_BUTTONS(INT iRespawnTime = 0)	

	/*IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_RESPAWNING)
		
		REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPGlobals.g_KillStrip.instructionalButtons)
		
		CLEAR_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_SHOW_BUTTONS)
		
		// If player involved in deathmatch and has perks avaiable, offer them
		IF SHOULD_HAVE_DM_RESPAWN_OPTIONS()
			IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PERK_SELECTED)
				IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
					PRINTLN("[JA@KILLSTRIP] (KILL_STRIP_RESPAWNING) Adding BUTTONS for PERKS")
				
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X), "KILL_STRIP_P1", MPGlobals.g_KillStrip.instructionalButtons)
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y), "KILL_STRIP_P2", MPGlobals.g_KillStrip.instructionalButtons)
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "KILL_STRIP_P3", MPGlobals.g_KillStrip.instructionalButtons)
					
					//UPDATE_BIG_MESSAGE_STRAPLINE_AND_INT("KILL_LOSER_P")
					
					SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_SHOW_BUTTONS)
				ENDIF
			ENDIF
		ELSE
			IF IS_ON_DEATHMATCH_GLOBAL_SET()
				//UPDATE_BIG_MESSAGE_STRAPLINE_AND_INT("KILL_PLY_RESP")
			ENDIF
		ENDIF
		
	EL*/
	
	// So we can use the mouse button to respawn on PC.
	caRespawnInput = INPUT_RESPAWN_FASTER
	
	
	IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_CAN_RESPAWN)
	
		IF NOT IS_KILLSTRIP_SET_TO_AUTO_RESPAWN()
		AND NOT DO_QUICK_RESPAWN()	//AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
		//AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_BASE_JUMP
			REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPGlobals.g_KillStrip.instructionalButtons)
		
			// Clean up message below big message
			//UPDATE_BIG_MESSAGE_STRAPLINE_AND_INT("", -1)
		
			// Respawn option
			//IF NOT IS_ON_DEATHMATCH_GLOBAL_SET() // 1578396
			
			IF NOT PLAYER_USING_ORBITAL(MPGlobals.g_KillStrip.killerData.killerID)
				IF MPGlobals.g_KillStrip.iRespawnFastRespawn = 0
					PRINTLN("3992698 1 ")
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caRespawnInput), "HUD_INPUT27", MPGlobals.g_KillStrip.instructionalButtons)
				ELSE
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caRespawnInput), "HUD_INPUT97", MPGlobals.g_KillStrip.instructionalButtons)
				ENDIF
			ENDIF
			
			IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PASSIVE_MODE_SELECTED)
			AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_IM_DM_UNAVAILABLE)
			AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_GRIEF_PASSIVE_SELECTED)
			AND CAN_TRIGGER_IMPROMPTU_DM_KILLSTRIP()
				PRINTLN("3992698 A ")
				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X), "KILL_STRIP_DM", MPGlobals.g_KillStrip.instructionalButtons)
			ENDIF
			//Passive Mode Button
			IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_START_IM_DM)
	
				IF CAN_USE_PASSIVE_MODE_BUTTON()
					IF g_sMPTunables.iPassiveDonateCost > 0
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_INT(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y), "KILL_STRIP_PM", g_sMPTunables.iPassiveDonateCost, MPGlobals.g_KillStrip.instructionalButtons)
					ELSE
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y), "KILL_STRIP_FPM", MPGlobals.g_KillStrip.instructionalButtons)
					ENDIF
				ENDIF
				
				IF CAN_USE_GHOSTED_BUTTON()
					IF IS_KILLER_A_GRIEFER()
					AND CAN_PLAYER_USE_GRIEF_PASSIVE()
					AND NOT AM_I_ALREADY_GHOSTED_TO_KILLER()
					AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_GRIEF_PASSIVE_SELECTED)
						IF GB_IS_PLAYER_MEMBER_OF_A_GANG(MPGlobals.g_KillStrip.killerData.killerID)
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_PLAYER(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RB), "KILL_STRIP_TPM", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(MPGlobals.g_KillStrip.killerData.killerID), MPGlobals.g_KillStrip.instructionalButtons)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_PLAYER(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RB), "KILL_STRIP_TPM", GET_PLAYER_NAME(MPGlobals.g_KillStrip.killerData.killerID), MPGlobals.g_KillStrip.instructionalButtons)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_SHOW_BUTTONS)
		ENDIF
	ELSE
		//IF MPGlobals.g_KillStrip.iRespawnTime != iRespawnTime	
						
			IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_RESPAWN_TIMER_INIT)	//PUT BACK IN AND DRAW BAR ???
			
				CLEAR_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_SHOW_BUTTONS)
			
				REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPGlobals.g_KillStrip.instructionalButtons)
				MPGlobals.g_KillStrip.iRespawnTime = iRespawnTime

				//IF NOT IS_KILLSTRIP_SET_TO_AUTO_RESPAWN()
				//AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
					//IF iRespawnTime > 0
					//	UPDATE_BIG_MESSAGE_STRAPLINE_AND_INT("KILL_STRIP_W", iRespawnTime)
					//ENDIF
				//ENDIF
				
				IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_START_IM_DM)
					IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PASSIVE_MODE_SELECTED)
					AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_GRIEF_PASSIVE_SELECTED)
					AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_IM_DM_UNAVAILABLE)
					AND CAN_TRIGGER_IMPROMPTU_DM_KILLSTRIP()
						PRINTLN("3992698 B ")
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X), "KILL_STRIP_DM", MPGlobals.g_KillStrip.instructionalButtons)
						SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_SHOW_BUTTONS)
					ENDIF
				ENDIF
				
				IF SHOULD_HAVE_DM_RESPAWN_OPTIONS()
					IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PERK_SELECTED)
						IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
					
							PRINTLN("[JA@KILLSTRIP] Adding BUTTONS for PERKS")
						
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X), "KILL_STRIP_P1", MPGlobals.g_KillStrip.instructionalButtons)
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y), "KILL_STRIP_P2", MPGlobals.g_KillStrip.instructionalButtons)
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "KILL_STRIP_P3", MPGlobals.g_KillStrip.instructionalButtons)
							
							//UPDATE_BIG_MESSAGE_STRAPLINE_AND_INT("KILL_LOSER_P")	//REMOVED FOR BUG 1602527
							
							SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_SHOW_BUTTONS)
						ENDIF
					ENDIF
				ENDIF
				
				// If IMPROMPTU DM invite has been sent, show spinner
				/*IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_START_IM_DM)
				AND IS_NET_PLAYER_OK(MPGlobals.g_KillStrip.killerData.killerID, FALSE)
					PRINTLN("[JA@KILLSTRIP] Adding BUTTONS for waiting for impromptu DM accept")
				
					//ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_PLAYER(ICON_SPINNER, "KILL_STRIP_IDM", GET_PLAYER_NAME(MPGlobals.g_KillStrip.killerData.killerID), MPGlobals.g_KillStrip.instructionalButtons)
					SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_SHOW_BUTTONS)
					
					//Show Spinner
					IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_IM_DM_LOADING_SPINNER)
						BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("STRING") 
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_PLAYER_NAME(MPGlobals.g_KillStrip.killerData.killerID))
	                        //ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.TopNumber) 
	                        //ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.Denominator) 
	    				END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING)) 
						SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_IM_DM_LOADING_SPINNER)
						PRINTLN("[JA@KILLSTRIP] KILL_STRIP_IM_DM_LOADING_SPINNER set")
					ENDIF
				ENDIF*/
				
				//IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
				OR NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
					PRINTLN("[JA@KILLSTRIP] Adding BUTTONS for respawn bar")
				
				//	IF NOT IS_ON_DEATHMATCH_GLOBAL_SET() 
					IF NOT DO_QUICK_RESPAWN()	//IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_BASE_JUMP
						IF NOT PLAYER_USING_ORBITAL(MPGlobals.g_KillStrip.killerData.killerID)
							IF MPGlobals.g_KillStrip.iRespawnFastRespawn = 0
								PRINTLN("3992698 2 ")
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caRespawnInput), "HUD_INPUT27", MPGlobals.g_KillStrip.instructionalButtons)
							ELSE
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caRespawnInput), "HUD_INPUT97", MPGlobals.g_KillStrip.instructionalButtons)
							ENDIF
						ENDIF
						
						//Passive Mode Button
						IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_START_IM_DM)
							IF CAN_USE_PASSIVE_MODE_BUTTON()
								IF g_sMPTunables.iPassiveDonateCost > 0
									ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_INT(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y), "KILL_STRIP_PM", g_sMPTunables.iPassiveDonateCost, MPGlobals.g_KillStrip.instructionalButtons)
								ELSE
									ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y), "KILL_STRIP_FPM", MPGlobals.g_KillStrip.instructionalButtons)
								ENDIF
							ENDIF

							IF CAN_USE_GHOSTED_BUTTON()
								IF IS_KILLER_A_GRIEFER()
								AND CAN_PLAYER_USE_GRIEF_PASSIVE()
								AND NOT AM_I_ALREADY_GHOSTED_TO_KILLER()
								AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_GRIEF_PASSIVE_SELECTED)
									IF GB_IS_PLAYER_MEMBER_OF_A_GANG(MPGlobals.g_KillStrip.killerData.killerID)
										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_PLAYER(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RB), "KILL_STRIP_TPM", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(MPGlobals.g_KillStrip.killerData.killerID), MPGlobals.g_KillStrip.instructionalButtons)
									ELSE
										ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_PLAYER(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RB), "KILL_STRIP_TPM", GET_PLAYER_NAME(MPGlobals.g_KillStrip.killerData.killerID), MPGlobals.g_KillStrip.instructionalButtons)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_SHOW_BUTTONS)	
					ENDIF
				ENDIF

				SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_RESPAWN_TIMER_INIT)
			/*ELSE
				IF NOT IS_KILLSTRIP_SET_TO_AUTO_RESPAWN()
				AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
				AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_START_IM_DM)
				
					IF iRespawnTime > 0
						UPDATE_BIG_MESSAGE_STRAPLINE_AND_INT("KILL_STRIP_W", iRespawnTime)
						MPGlobals.g_KillStrip.iRespawnTime = iRespawnTime
					ENDIF
				
				ENDIF*/
			ENDIF
		//ENDIF
	ENDIF
ENDPROC

// Checks all crucial assets for kill strip has loaded.
FUNC BOOL HAS_KILL_STRIP_SCALEFORM_LOADED()
	
	// If we are rendering we are done loading.
	IF MPGlobals.g_KillStrip.iState = STATE_KILL_STRIP_RENDERING
		RETURN TRUE
	ENDIF
	
	RETURN TRUE
ENDFUNC

CONST_INT DEFAULT_MASH_DURATION 1000

FUNC INT GET_MASH_BUTTON_DURATION()
	//IF IS_ON_DEATHMATCH_GLOBAL_SET()
		RETURN KILL_STRIP_DM_RESPAWN_BAR_TIMER
	//ENDIF
	
	//RETURN DEFAULT_MASH_DURATION
ENDFUNC


PROC MAINTAIN_KILL_STRIP_FAST_RESPAWN()
	// Listen for the player hitting X to speed up the respawn
	IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_ALLOW_RESPAWN_BAR)
	AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_CAN_RESPAWN)
		IF ( IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, caRespawnInput) OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, caRespawnInput) )
		AND NOT PLAYER_USING_ORBITAL(MPGlobals.g_KillStrip.killerData.killerID)
			IF MPGlobals.g_KillStrip.iRespawnFastRespawn < GET_MASH_BUTTON_DURATION()
				IF MPGlobals.g_KillStrip.iRespawnFastRespawn = 0
					REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPGlobals.g_KillStrip.instructionalButtons)
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caRespawnInput), "HUD_INPUT97", MPGlobals.g_KillStrip.instructionalButtons)
					IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_START_IM_DM)
						IF CAN_USE_PASSIVE_MODE_BUTTON()
							IF g_sMPTunables.iPassiveDonateCost > 0
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_INT(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y), "KILL_STRIP_PM", g_sMPTunables.iPassiveDonateCost, MPGlobals.g_KillStrip.instructionalButtons)
							ELSE
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y), "KILL_STRIP_FPM", MPGlobals.g_KillStrip.instructionalButtons)
							ENDIF
						ENDIF

						IF CAN_USE_GHOSTED_BUTTON()
							IF IS_KILLER_A_GRIEFER()
							AND CAN_PLAYER_USE_GRIEF_PASSIVE()
							AND NOT AM_I_ALREADY_GHOSTED_TO_KILLER()
							AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_GRIEF_PASSIVE_SELECTED)
								IF GB_IS_PLAYER_MEMBER_OF_A_GANG(MPGlobals.g_KillStrip.killerData.killerID)
									ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_PLAYER(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RB), "KILL_STRIP_TPM", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(MPGlobals.g_KillStrip.killerData.killerID), MPGlobals.g_KillStrip.instructionalButtons)
								ELSE
									ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_PLAYER(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RB), "KILL_STRIP_TPM", GET_PLAYER_NAME(MPGlobals.g_KillStrip.killerData.killerID), MPGlobals.g_KillStrip.instructionalButtons)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PASSIVE_MODE_SELECTED)
					AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_GRIEF_PASSIVE_SELECTED)
					AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_IM_DM_UNAVAILABLE)
					AND CAN_TRIGGER_IMPROMPTU_DM_KILLSTRIP()
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X), "KILL_STRIP_DM", MPGlobals.g_KillStrip.instructionalButtons)
					ELIF SHOULD_HAVE_DM_RESPAWN_OPTIONS()
						IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PERK_SELECTED)
							IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X), "KILL_STRIP_P1", MPGlobals.g_KillStrip.instructionalButtons)
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_Y), "KILL_STRIP_P2", MPGlobals.g_KillStrip.instructionalButtons)
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "KILL_STRIP_P3", MPGlobals.g_KillStrip.instructionalButtons)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				//IF MPGlobals.g_KillStrip.iRespawnFastRespawn > 0
				//	//MAKE_BIG_MESSAGE_RESPAWN_FLASH()
				//ENDIF
				REINIT_NET_TIMER(MPGlobals.g_KillStrip.RespawnBarFlashTimer)
				//MPGlobals.g_KillStrip.RespawnBarFlashLength = RESPAWN_BAR_FLASH_TIME
				PLAY_SOUND_FRONTEND(-1, "Faster_Click", "RESPAWN_ONLINE_SOUNDSET")
				MAKE_INSTRUCTIONAL_BUTTON_FLASH(MPGlobals.g_KillStrip.buttonMovie, 0, 30, (TO_FLOAT(RESPAWN_BAR_FLASH_TIME)/1000))	//ENUM_TO_INT(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
				PRINTLN(" FLASH CHECK NOW 3 ")
				MPGlobals.g_KillStrip.iRespawnFastRespawn += 550	//500	//300	//240	//200
				PRINTLN("[JA@KILLSTRIP] Player speeding up respawn: ", MPGlobals.g_KillStrip.iRespawnFastRespawn)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// Listens for input from player and updates display
FUNC BOOL PROCESS_KILL_STRIP_INPUT()

	IF GET_GAME_STATE_ON_DEATH() = AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP
//		PRINTLN("[JA@KILLSTRIP] [CS_PERKS]PROCESS_KILL_STRIP_INPUT - AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP() = TRUE")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_HIDE_STRIP)
		RETURN FALSE
	ENDIF 

	IF IS_WARNING_MESSAGE_ACTIVE()
		PRINTLN("[JA@KILLSTRIP] [CS_PERKS]PROCESS_KILL_STRIP_INPUT - IS_WARNING_MESSAGE_ACTIVE() = TRUE")
		RETURN FALSE
	ENDIF
	
	IF IS_TRANSITION_ACTIVE()
		PRINTLN("[JA@KILLSTRIP] [CS_PERKS]PROCESS_KILL_STRIP_INPUT - IS_TRANSITION_ACTIVE() = TRUE")
		RETURN FALSE
	ENDIF

	IF g_SpawnData.bPlayerWantsToRespawn
	OR NOT IS_KILL_STRIP_HUD_ALLOWED_TO_DISPLAY()
//		PRINTLN("[JA@KILLSTRIP] [CS_PERKS]PROCESS_KILL_STRIP_INPUT - IS_KILL_STRIP_HUD_ALLOWED_TO_DISPLAY() = TRUE")
		RETURN FALSE	// We don't need to listen to anything else now
	ENDIF
	
	IF IS_LOCAL_PLAYER_AN_ANIMAL()
		RETURN FALSE
	ENDIF

	IF SHOULD_HAVE_DM_RESPAWN_OPTIONS()
		IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PERK_SELECTED)
			IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
			
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
											
					PRINTLN("[JA@KILLSTRIP] [CS_PERKS]Player chosen perk: g_e_Chosen_Perk = KS_PP_SQUARE_ALL_BLIP")
					
					g_e_Chosen_Perk = KS_PP_SQUARE_ALL_BLIP
					
					UPDATE_BIG_MESSAGE_WITH_PERK("KILL_LOSER_PS", "KILL_LOSER_PS1")
					
					PLAY_SOUND_FRONTEND(-1, "TOGGLE_ON", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
					REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPGlobals.g_KillStrip.instructionalButtons)
					
					IF NOT PLAYER_USING_ORBITAL(MPGlobals.g_KillStrip.killerData.killerID)
						IF MPGlobals.g_KillStrip.iRespawnFastRespawn = 0
							PRINTLN("3992698 3 ")
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caRespawnInput), "HUD_INPUT27", MPGlobals.g_KillStrip.instructionalButtons)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caRespawnInput), "HUD_INPUT97", MPGlobals.g_KillStrip.instructionalButtons)
						ENDIF
					ENDIF
					
					SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PERK_SELECTED)
				ENDIF
				
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
					
					PRINTLN("[JA@KILLSTRIP] [CS_PERKS]Player chosen perk: g_e_Chosen_Perk = KS_PP_TRIANGLE_QUAD")
					
					g_e_Chosen_Perk = KS_PP_TRIANGLE_QUAD
					
					UPDATE_BIG_MESSAGE_WITH_PERK("KILL_LOSER_PS", "KILL_LOSER_PS2")
					
					PLAY_SOUND_FRONTEND(-1, "TOGGLE_ON", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
					REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPGlobals.g_KillStrip.instructionalButtons)
					
					IF NOT PLAYER_USING_ORBITAL(MPGlobals.g_KillStrip.killerData.killerID)
						IF MPGlobals.g_KillStrip.iRespawnFastRespawn = 0
							PRINTLN("3992698 4 ")
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caRespawnInput), "HUD_INPUT27", MPGlobals.g_KillStrip.instructionalButtons)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caRespawnInput), "HUD_INPUT97", MPGlobals.g_KillStrip.instructionalButtons)
						ENDIF
					ENDIF
					
					SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PERK_SELECTED)
				ENDIF
				
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
					
					PRINTLN("[JA@KILLSTRIP] [CS_PERKS]Player chosen perk: g_e_Chosen_Perk = KS_PP_CIRCLE_HIDE_BLIP")
					
					g_e_Chosen_Perk = KS_PP_CIRCLE_HIDE_BLIP
					
					UPDATE_BIG_MESSAGE_WITH_PERK("KILL_LOSER_PS", "KILL_LOSER_PS3")
					
					PLAY_SOUND_FRONTEND(-1, "TOGGLE_ON", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
					REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPGlobals.g_KillStrip.instructionalButtons)
					
					IF NOT PLAYER_USING_ORBITAL(MPGlobals.g_KillStrip.killerData.killerID)
						IF MPGlobals.g_KillStrip.iRespawnFastRespawn = 0
							PRINTLN("3992698 5 ")
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caRespawnInput), "HUD_INPUT27", MPGlobals.g_KillStrip.instructionalButtons)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caRespawnInput), "HUD_INPUT97", MPGlobals.g_KillStrip.instructionalButtons)
						ENDIF
					ENDIF
					
					SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PERK_SELECTED)
				ENDIF
				
				IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PERK_SELECTED)
					ADD_KILL_STRIP_BUTTONS()
				ENDIF
			ELSE
				PRINTLN("[JA@KILLSTRIP] [CS_PERKS]ERROR KILL_STRIP_NO_KILLER")
			ENDIF
		ELSE
			PRINTLN("[JA@KILLSTRIP] [CS_PERKS]ERROR KILL_STRIP_PERK_SELECTED")
		ENDIF
	ELSE
		PRINTLN("[JA@KILLSTRIP] [CS_PERKS]ERROR SHOULD_HAVE_DM_RESPAWN_OPTIONS/HAS_PASSIVE_MODE_CUTSCENE_BEEN_DONE")
	ENDIF

	// Deathmatch ignores press X to respawn
	IF IS_KILLSTRIP_SET_TO_AUTO_RESPAWN()
		RETURN FALSE
	ELSE
	
		// Return true if we have pressed X and can respawn
		IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_CAN_RESPAWN)
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, caRespawnInput)
			//OR IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_START_IM_DM)
			
				PRINTLN("[JA@KILLSTRIP] Player chosen to respawn off killstrip")
			
				//SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_RESPAWNING)
				ADD_KILL_STRIP_BUTTONS()			
				RETURN TRUE
			ENDIF
		ENDIF
		
		//Player can start DM if 
		IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_START_IM_DM)
			IF CAN_TRIGGER_IMPROMPTU_DM_KILLSTRIP()
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)		
					
					PRINTLN("[JA@KILLSTRIP] Player triggered IMPROMPTU DM invite")
					
					DISABLE_MP_PASSIVE_MODE(PMER_DEATHSCREEN, FALSE)
					
					SEND_IMPROMPTU_CHALLENGE(MPGlobals.g_KillStrip.killerData.killerID, IS_BIT_SET(g_sImpromptuVars.iBitSet, ciIMPROMPTU_IN_A_HELI), GlobalServerBD.iLaunchPosix)
					SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_START_IM_DM)
					
					// Blank our WASTED
					UPDATE_BIG_MESSAGE_STRAPLINE_AND_INT("", -1)
					//CLEAR_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_SHOW_BUTTONS)
					
					CLEAR_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_RESPAWN_TIMER_INIT)
					CLEAR_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_CAN_RESPAWN)
					
					// Set ourselves to 10s wait for reply
					MPGlobals.g_KillStrip.iDeathTime = GET_NETWORK_TIME()
					
					//Show Spinner
					IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_IM_DM_LOADING_SPINNER)
						BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("KILL_STRIP_IDM") 
							ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_PLAYER_NAME(MPGlobals.g_KillStrip.killerData.killerID))
	                        //ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.TopNumber) 
	                        //ADD_TEXT_COMPONENT_INTEGER(DisplayStruct.Denominator) 
	    				END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING)) 
						SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_IM_DM_LOADING_SPINNER)
						PRINTLN("[JA@KILLSTRIP] KILL_STRIP_IM_DM_LOADING_SPINNER set")
					ENDIF
					
					ADD_KILL_STRIP_BUTTONS()
					SET_KILLSTRIP_AUTO_RESPAWN(TRUE)
					SET_KILLSTRIP_AUTO_RESPAWN_TIME(10000)
					
					PLAY_SOUND_FRONTEND(-1, "TOGGLE_ON", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
				ENDIF
			ENDIF
		ENDIF
		
		//Check for Passive Mode being enabled
		IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PASSIVE_MODE_SELECTED)
		AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
			IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
				IF CAN_USE_PASSIVE_MODE_BUTTON()
					PRINTLN("[JA@KILLSTRIP] PASSIVE MODE SELECTED")
					ENABLE_MP_PASSIVE_MODE(PMER_DEATHSCREEN, DEFAULT,NATIVE_TO_INT(MPGlobals.g_KillStrip.killerData.killerID) )
					IF g_sMPTunables.iPassiveDonateCost > 0
						IF USE_SERVER_TRANSACTIONS()
							INT iTransactionID
							TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_PASSIVE, g_sMPTunables.iPassiveDonateCost, iTransactionID)
						ELSE
							GIVE_LOCAL_PLAYER_CASH(-g_sMPTunables.iPassiveDonateCost)
							NETWORK_SPENT_BUY_PASSIVE_MODE(g_sMPTunables.iPassiveDonateCost)
						ENDIF
						
					ENDIF
					SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PASSIVE_MODE_SELECTED)
					//ADD_KILL_STRIP_BUTTONS()
					
					REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPGlobals.g_KillStrip.instructionalButtons)
					
					IF NOT PLAYER_USING_ORBITAL(MPGlobals.g_KillStrip.killerData.killerID)
						IF MPGlobals.g_KillStrip.iRespawnFastRespawn = 0
							PRINTLN("3992698 6 ")
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caRespawnInput), "HUD_INPUT27", MPGlobals.g_KillStrip.instructionalButtons)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caRespawnInput), "HUD_INPUT97", MPGlobals.g_KillStrip.instructionalButtons)
						ENDIF
					ENDIF
					
					PLAY_SOUND_FRONTEND(-1, "TOGGLE_ON", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				ENDIF
			ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
				IF CAN_USE_GHOSTED_BUTTON()
					IF IS_KILLER_A_GRIEFER()
					AND CAN_PLAYER_USE_GRIEF_PASSIVE()
					AND NOT AM_I_ALREADY_GHOSTED_TO_KILLER()
					AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_GRIEF_PASSIVE_SELECTED)
						PRINTLN("[JA@KILLSTRIP] [RE] TARGETTED PASSIVE MODE SELECTED")
						IF GB_IS_PLAYER_MEMBER_OF_A_GANG(MPGlobals.g_KillStrip.killerData.killerID, TRUE)
							SET_PLAYER_TO_PASSIVE_AGAINST_PLAYERS_GANG(MPGlobals.g_KillStrip.killerData.killerID)
						ELSE
							SET_PLAYER_TO_PASSIVE_AGAINST_PLAYER(MPGlobals.g_KillStrip.killerData.killerID)
						ENDIF
						
						REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPGlobals.g_KillStrip.instructionalButtons)
						
						IF NOT PLAYER_USING_ORBITAL(MPGlobals.g_KillStrip.killerData.killerID)
							IF MPGlobals.g_KillStrip.iRespawnFastRespawn = 0
								PRINTLN("3992698 7 ")
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caRespawnInput), "HUD_INPUT27", MPGlobals.g_KillStrip.instructionalButtons)
							ELSE
								ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caRespawnInput), "HUD_INPUT97", MPGlobals.g_KillStrip.instructionalButtons)
							ENDIF
						ENDIF
						
						SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_GRIEF_PASSIVE_SELECTED)
						
						g_bShowGriefPassiveSpawnHelp = TRUE
						g_piFirstGriefPassivePlayer = MPGlobals.g_KillStrip.killerData.killerID
						PRINTLN("[JA@KILLSTRIP] [RE] g_bShowGriefPassiveSpawnHelp = TRUE, g_piFirstGriefPassivePlayer = ", GET_PLAYER_NAME(g_piFirstGriefPassivePlayer))
						
						//PLAY_SOUND_FRONTEND(-1, "TOGGLE_ON", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						PLAY_SOUND_FRONTEND(-1, "Accept_Ghosting_Mode", "RESPAWN_ONLINE_SOUNDSET", FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC CLEANUP_FREEMODE_AMBIENT_EVENT_KILL_STRIP_FLAGS()
	// Clean up any ambient event flags
	SET_PLAYER_HAS_BEEN_KNOCKED_OUT_OF_MODE(FALSE)
ENDPROC

PROC SET_KILL_STRIP_OVER_RIDE_PLAYER_ID(PLAYER_INDEX piOverRideKillerPlayer)
	IF NETWORK_IS_PLAYER_ACTIVE(piOverRideKillerPlayer)
		PRINTLN("[JA@KILLSTRIP] SET_KILL_STRIP_OVER_RIDE_PLAYER_ID - ", GET_PLAYER_NAME(piOverRideKillerPlayer)) 
		MPGlobals.g_KillStrip.piOverRideKillerPlayer = piOverRideKillerPlayer
	ENDIF
ENDPROC


/// PURPOSE: Check to see if we have rendered killstrip. Unload all texture dictionaries and reset kill strip global
PROC CLEANUP_KILL_STRIP(BOOL bResetKillStripState = TRUE)

	#IF IS_DEBUG_BUILD
		NET_PRINT("[JA@KILLSTRIP] CLEANUP_KILL_STRIP - cleanup kill strip") NET_NL()
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	// Set that the player is ready to respawn
	g_SpawnData.bPlayerWantsToRespawn = TRUE
	//Clean up the over ride
	CLEANUP_KILL_STRIP_OVER_RIDE_PLAYER_ID()
	Unpause_Objective_Text()				// Unpause any mission objective text

	IF MPGlobals.g_KillStrip.iState != STATE_KILL_STRIP_SKIP
		NET_PRINT("[JA@KILLSTRIP] CLEANUP_KILL_STRIP - setting movies as no longer needed") NET_NL()
	
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(MPGlobals.g_KillStrip.buttonMovie)
	ENDIF
	
	// NOTE: we have to release this now for memory cleanup (2205634)
	//REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
	NET_PRINT("[JA@KILLSTRIP] CLEANUP_KILL_STRIP - Releasing TimerBars texture dict") NET_NL()
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("TimerBars")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("timerbar_lines")
					
	
	// Clean up instructional button struct
	RESET_SCALEFORM_INSTRUCTIONAL_BUTTON(MPGlobals.g_KillStrip.instructionalButtons)
	
	// Clean up any ambient event flags
	CLEANUP_FREEMODE_AMBIENT_EVENT_KILL_STRIP_FLAGS()
	
	IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_IM_DM_LOADING_SPINNER)
		SET_LOADING_ICON_INACTIVE()
		PRINTLN("[JA@KILLSTRIP] CLEANUP_KILL_STRIP - SET_LOADING_ICON_INACTIVE")
	ENDIF
	
	// Used in spawn, if kill strip was displayed no respawn delay timer.
	MPGlobals.g_KillStrip.iPreviousState = MPGlobals.g_KillStrip.iState
	
	MPGlobals.g_KillStrip.killerData.bKilledUsingMelee = FALSE
	
	// Reset hit tracking
	IF NETWORK_IS_GAME_IN_PROGRESS()
		NETWORK_RESET_BODY_TRACKER()
	ENDIF
	
	// Clean up our help, only if we've been in a state. 
	IF MPGlobals.g_KillStrip.iState != STATE_KILL_STRIP_NULL
		CLEAR_HELP(TRUE)
	ENDIF

	IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_START_IM_DM)
	AND NOT IS_ON_IMPROMPTU_DEATHMATCH_GLOBAL_SET()
		NET_PRINT("[JA@KILLSTRIP] CLEANUP_KILL_STRIP - turn off our auto respawn as used for IMPROMPTU DM") NET_NL()
		SET_KILLSTRIP_AUTO_RESPAWN(FALSE)
	ENDIF

	KILLER_DATA_STRUCT emptyKillerData
	PLAYER_CREW_DATA_STRUCT emptyCrewData
	
	IF bResetKillStripState
		MPGlobals.g_KillStrip.iState = STATE_KILL_STRIP_NULL
	ENDIF
	MPGlobals.g_KillStrip.killerData = emptyKillerData
	MPGlobals.g_KillStrip.killerData.killerID = INVALID_PLAYER_INDEX()
	MPGlobals.g_KillStrip.killerCrewInfo = emptyCrewData
	MPGlobals.g_KillStrip.iRespawnFastRespawn = 0
	
	MPGlobals.g_KillStrip.iWeaponUsed = 0

	MPGlobals.g_KillStrip.iKillStripFlags = 0
ENDPROC

// Checks the killers crew emblem has been loaded.
FUNC BOOL HAS_KILL_STRIP_CREW_EMBLEM_LOADED()
	
	IF MPGlobals.g_KillStrip.killerCrewInfo.bIsActive
		
		GAMER_HANDLE aGamerHandle = GET_GAMER_HANDLE_PLAYER(MPGlobals.g_KillStrip.killerData.killerID)
		IF NETWORK_CLAN_GET_EMBLEM_TXD_NAME(aGamerHandle, MPGlobals.g_KillStrip.killerCrewInfo.crewEmblemTxd)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Currently Pilot school hides the wasted and killstrip
FUNC BOOL IS_MP_MODE_VALID_FOR_WASTED_AND_KILLSTRIP()
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MG_PILOT_SCHOOL
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Check if the player is safe to display the kill strip
FUNC BOOL IS_SAFE_TO_SHOW_KILLSTRIP()
		
	IF MPGlobals.g_KillStrip.iState = STATE_KILL_STRIP_NULL
		IF GET_GAME_STATE_ON_DEATH() != AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP
			NET_PRINT("[JA@KILLSTRIP] IS_SAFE_TO_SHOW_KILLSTRIP - CALLED CLEANUP_KILL_STRIP (1)") NET_NL()
			RETURN FALSE
		ENDIF
	ENDIF
		
	IF NOT IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
		IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ACTIVE)
			NET_PRINT("[JA@KILLSTRIP] IS_SAFE_TO_SHOW_KILLSTRIP - CALLED CLEANUP_KILL_STRIP (2)") NET_NL()
			RETURN FALSE
		ENDIF		
	
		IF g_bMissionEnding = TRUE
		OR g_b_OnLeaderboard = TRUE
		OR g_bCelebrationScreenIsActive = TRUE
			IF MPGlobals.g_KillStrip.iState != STATE_KILL_STRIP_NULL
				CLEANUP_KILL_STRIP()
				NET_PRINT("[JA@KILLSTRIP] IS_SAFE_TO_SHOW_KILLSTRIP - CALLED CLEANUP_KILL_STRIP (3)") NET_NL()
			ENDIF
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_TRANSITION_ACTIVE()
		NET_PRINT("[JA@KILLSTRIP] IS_SAFE_TO_SHOW_KILLSTRIP - CALLED CLEANUP_KILL_STRIP (4)") NET_NL()
		RETURN FALSE
	ENDIF

	// Player can not be pressing dpad down
	IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	IF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
		#IF IS_DEBUG_BUILD
			IF GET_GAME_TIMER() % 1000 < 75
				PRINTLN("[JA@KILLSTRIP] IS_SAFE_TO_SHOW_KILLSTRIP - false. IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE()")
			ENDIF
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_KILL_STRIP_BG_COLOUR_FROM_BIT(INT i)
	
	SWITCH i
		CASE ATTRIBUTE_DAMAGE_BS_VAGOS
			NET_PRINT("[JA@KILLSTRIP] GET_KILL_STRIP_WEAPONTYPE_FROM_BIT - returning WEAPON: HUD_COLOUR_GANG2") NET_NL()
			
			RETURN ENUM_TO_INT(HUD_COLOUR_GANG2)
		CASE ATTRIBUTE_DAMAGE_BS_LOST
			NET_PRINT("[JA@KILLSTRIP] GET_KILL_STRIP_WEAPONTYPE_FROM_BIT - returning WEAPON: HUD_COLOUR_GANG3") NET_NL()
			
			RETURN ENUM_TO_INT(HUD_COLOUR_GANG3)
		CASE ATTRIBUTE_DAMAGE_BS_FAMILIES
			NET_PRINT("[JA@KILLSTRIP] GET_KILL_STRIP_WEAPONTYPE_FROM_BIT - returning WEAPON: HUD_COLOUR_FRANKLIN") NET_NL()
			
			RETURN ENUM_TO_INT(HUD_COLOUR_FRANKLIN)
		CASE ATTRIBUTE_DAMAGE_BS_THIEVES
			NET_PRINT("[JA@KILLSTRIP] GET_KILL_STRIP_WEAPONTYPE_FROM_BIT - returning WEAPON: HUD_COLOUR_NET_PLAYER20") NET_NL()
			
			RETURN ENUM_TO_INT(HUD_COLOUR_NET_PLAYER20)
		CASE ATTRIBUTE_DAMAGE_BS_BACKUP_HELI
			NET_PRINT("[JA@KILLSTRIP] GET_KILL_STRIP_WEAPONTYPE_FROM_BIT - returning WEAPON: HUD_COLOUR_RED") NET_NL()
			
			RETURN ENUM_TO_INT(HUD_COLOUR_RED)
	ENDSWITCH

	RETURN -1
ENDFUNC

FUNC INT GET_KILL_STRIP_BG_COLOUR(ENTITY_INDEX killerEntity)
	
	INT i
	FOR i = 0 TO (ATTRIBUTE_DAMAGE_BS_MAX-1) STEP 1
		IF IS_ATTRIBUTE_DAMAGE_DECORATOR_BIT_SET(killerEntity, i)
			RETURN GET_KILL_STRIP_BG_COLOUR_FROM_BIT(i)
		ENDIF
	ENDFOR
	
	RETURN -1
ENDFUNC

FUNC BOOL SHOULD_SHOW_KILL_TICKER(PLAYER_INDEX playerkiller)
	INT iVictimTeam, iKillerTeam
	IF g_bFM_ON_TEAM_MISSION
		iVictimTeam = GET_PLAYER_TEAM(PLAYER_ID())
		iKillerTeam = GET_PLAYER_TEAM(playerkiller)
		PRINTLN("SHOULD_SHOW_KILL_TICKER, iVictimTeam =  ", iVictimTeam)
		PRINTLN("SHOULD_SHOW_KILL_TICKER, iKillerTeam =  ", iKillerTeam)
		IF DOES_TEAM_LIKE_TEAM(iVictimTeam, iKillerTeam)
			PRINTLN("SHOULD_SHOW_KILL_TICKER, DOES_TEAM_LIKE_TEAM, EXIT  ")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF g_bNoDeathTickers
		PRINTLN("SHOULD_SHOW_KILL_TICKER, g_bNoDeathTickers, EXIT  ")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// ----------------------------------------------------------------------
//                          PROCESS FUNCTIONS
// ----------------------------------------------------------------------

// Called when kill strup is in NULL state to initialise a new kill strip
PROC INITIALISE_KILL_STRIP()

	/*IF NOT IS_SAFE_TO_SHOW_KILLSTRIP()
		NET_PRINT("[JA@KILLSTRIP] INITIALISE_KILL_STRIP - player not in safe state (dpad down?)...EXIT") NET_NL()
		
		MPGlobals.g_KillStrip.iState = STATE_KILL_STRIP_SKIP
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_WASTED)
		EXIT
	ENDIF*/
		
	REQUEST_STREAMED_TEXTURE_DICT("TimerBars")
	
	// Clean up other visuals on screen
	CLEAR_HELP(TRUE)
	
	// Reset our globals that are effected by killstrip
	g_e_Chosen_Perk = KS_PP_NOT_USE

	WEAPON_TYPE weaponUsed
	//INT iHudColourBG = -1
	
	ENTITY_INDEX killerEntity = NETWORK_GET_ENTITY_KILLER_OF_PLAYER(PLAYER_ID(), weaponUsed)
	PLAYER_INDEX killerID = INVALID_PLAYER_INDEX()
	PED_INDEX killerPed
	
	IF DOES_ENTITY_EXIST(killerEntity)
	AND IS_ENTITY_A_PED(killerEntity)

		killerPed = GET_PED_INDEX_FROM_ENTITY_INDEX(killerEntity)

		IF IS_PED_A_PLAYER(killerPed)
			killerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(killerPed)
			NET_PRINT("[JA@KILLSTRIP] INITIALISE_KILL_STRIP - ped is a player: ") NET_PRINT(GET_PLAYER_NAME(killerID)) NET_NL()
		/*ELSE
			IF NOT IS_ENTITY_DEAD(killerEntity)
				IF NETWORK_GET_ATTRIBUTE_DAMAGE_TO_PLAYER(killerPed, killerID)
					NET_PRINT("[JA@KILLSTRIP] INITIALISE_KILL_STRIP - ped is a player attribute: ") NET_PRINT(GET_PLAYER_NAME(killerID)) NET_NL()
					
					iHudColourBG = GET_KILL_STRIP_BG_COLOUR(killerEntity)
				ELSE
					NET_PRINT("[JA@KILLSTRIP] INITIALISE_KILL_STRIP - ped index: ") NET_PRINT_INT(NATIVE_TO_INT(killerPed)) NET_PRINT(" not attributed to a player") NET_NL()	
					
					// Reset our killerID to be picked up in invalid checks
					killerID = INVALID_PLAYER_INDEX()
				ENDIF
			ENDIF*/
		ENDIF
		
	#IF IS_DEBUG_BUILD
	ELSE
		NET_PRINT("[JA@KILLSTRIP] INITIALISE_KILL_STRIP - entity is not a player / invalid") NET_NL()	
	#ENDIF
	ENDIF
	IF MPGlobals.g_KillStrip.piOverRideKillerPlayer = INVALID_PLAYER_INDEX()
	OR NOT NETWORK_IS_PLAYER_ACTIVE(MPGlobals.g_KillStrip.piOverRideKillerPlayer)
		MPGlobals.g_KillStrip.killerData.killerID = killerID
	ELSE
		MPGlobals.g_KillStrip.killerData.killerID = MPGlobals.g_KillStrip.piOverRideKillerPlayer
		killerID = MPGlobals.g_KillStrip.piOverRideKillerPlayer
		PRINTLN("[JA@KILLSTRIP] INITIALISE_KILL_STRIP -MPGlobals.g_KillStrip.piOverRideKillerPlayer = ", GET_PLAYER_NAME(MPGlobals.g_KillStrip.piOverRideKillerPlayer))
	ENDIF
	CLEANUP_KILL_STRIP_OVER_RIDE_PLAYER_ID()
	//MPGlobals.g_KillStrip.killerData.iBGColour = iHudColourBG
	
	// Add debug functionality for code
	#IF IS_DEBUG_BUILD
	IF bDebugKillStrip
		killerID = PLAYER_ID()
		weaponUsed = WEAPONTYPE_PISTOL
	ENDIF
	#ENDIF

	// Check if the returned ID is valid - if not, exit at this point
	IF killerID = INVALID_PLAYER_INDEX()
	OR killerID = PLAYER_ID()
	OR NOT IS_NET_PLAYER_OK(killerID, FALSE, TRUE)

		NET_PRINT("[JA@KILLSTRIP] INITIALISE_KILL_STRIP - invalid player index, no killer so only show custom spawn points") NET_NL()
		NET_PRINT("Killer ID = ") NET_PRINT_INT(NATIVE_TO_INT(killerID)) NET_NL()
		
		#IF IS_DEBUG_BUILD
		IF NOT bDebugKillStrip
		#ENDIF
	
		SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)

		#IF IS_DEBUG_BUILD
		ENDIF
		#ENDIF
	ENDIF
	
	
	IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
		// Dave W:  Do passive mode cutscene first time player is killed by another player off job.
		IF SHOULD_PASSIVE_MODE_CUTSCENE_RUN()
			PRINTLN("[dsw] SHOULD_PASSIVE_MODE_CUTSCENE_RUN True ")
			SET_GAME_STATE_ON_DEATH(AFTERLIFE_MANUAL_RESPAWN) 
			SET_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_SET_PASSIVE_SPAWN)
			SET_BIT(MPGlobalsAmbience.iFmNmhBitSet4, BI_FM_NMH4_DO_PASSIVE_CUT)
		ELSE
			PRINTLN("[dsw] SHOULD_PASSIVE_MODE_CUTSCENE_RUN FALSE ")
		ENDIF
		
		// Gather all the data possible about the killer of the player (rank / team / stats)
		GET_DETAILS_FOR_KILLER_OF_PLAYER(MPGlobals.g_KillStrip.killerData, killerID)
		
		// Maybe check if players are in a crew before requesting this info
		//GET_PLAYERS_CREW_INFO(MPGlobals.g_KillStrip.killerCrewInfo, killerID)
		
		// Retrive info on kill points of victim
		GET_PLAYER_HIT_POINTS(MPGlobals.g_KillStrip.hitPoints)
		
		#IF IS_DEBUG_BUILD
		IF bDebugKillStrip
			MPGlobals.g_KillStrip.hitPoints[0] = -1
		ENDIF
		#ENDIF
		
		// Store the weapon
		MPGlobals.g_KillStrip.iWeaponUsed = ENUM_TO_INT(weaponUsed)
		
		/*SET_PEDHEADSHOT_CUSTOM_LIGHTING(TRUE)
		SET_PEDHEADSHOT_CUSTOM_LIGHT(0, <<0.0, -2.0, 0.75>>, <<1.0, 1.0, 1.0>>, 0.4, 20.0)
		SET_PEDHEADSHOT_CUSTOM_LIGHT(1, <<-1.0, 0.0, 1.25>>, <<0.662745, 0.901961, 0.921569>>, 0.5, 10.0)
		SET_PEDHEADSHOT_CUSTOM_LIGHT(2, <<1.0, 0.0, 1.25>>, <<0.662745, 0.901961, 0.921569>>, 0.5, 10.0)
		SET_PEDHEADSHOT_CUSTOM_LIGHT(3, <<0.0, -2.0, 0.75>>, <<0.980392, 0.956863, 0.635294>>, 0.5, 10.0)*/
				
		NET_PRINT("[JA@KILLSTRIP] INITIALISE_KILL_STRIP - killer: ") NET_PRINT(MPGlobals.g_KillStrip.killerData.sKillerName)
		NET_PRINT(" at rank: ") NET_PRINT_INT(MPGlobals.g_KillStrip.killerData.iKillerRank) NET_NL()
		NET_PRINT("Player killed killer: ") NET_PRINT_INT(MPGlobals.g_KillStrip.killerData.iPlayerKilledKiller) NET_NL()
		NET_PRINT("Player killed BY killer: ") NET_PRINT_INT(MPGlobals.g_KillStrip.killerData.iKilledByKiller) NET_NL()
		NET_PRINT_STRINGS("Weapon used: ", GET_WEAPON_NAME(weaponUsed)) NET_PRINT(" ENUM: ") NET_PRINT_INT(ENUM_TO_INT(weaponUsed)) NET_NL()
		
		//MPGlobals.g_KillStrip.killerPicId = Get_Transparent_HeadshotID_For_Player(MPGlobals.g_KillStrip.killerData.killerID)	
		//MPGlobals.g_KillStrip.killStripMovie = REQUEST_SCALEFORM_MOVIE("MP_KILLSTRIP_FREEMODE")

	#IF IS_DEBUG_BUILD
	ELSE
		NET_PRINT("[JA@KILLSTRIP] INITIALISE_KILL_STRIP - no killer so don't request movie etc.") NET_NL()
	#ENDIF
	ENDIF
	
	// ******** Request our WASTED big message *********
	
	//TEXT_LABEL_15 strStrapline
	//INT iNumber
	//GET_KILL_STRIP_WASTED_STRAPLINE_AND_TIME(strStrapline, iNumber)
	//SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_WASTED, iNumber, strStrapline)
	// If we shouldn't show any of this then hide it
	IF IS_MP_MODE_VALID_FOR_WASTED_AND_KILLSTRIP()
		IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
			
			IF IS_BIT_SET(MPGlobals.KillYourselfData.iBitSet, biKY_DoWastedStrapline)
				
				CLEAR_BIT(MPGlobals.KillYourselfData.iBitSet, biKY_DoWastedStrapline)
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_WASTED, DEFAULT, "DM_TK_KY1")
				PRINT_TICKER("DM_TK_KY1")
			ELIF killerID = PLAYER_ID()
			AND g_i_RacePlayerExploded != NATIVE_TO_INT(killerID)
			AND NOT (IS_BIT_SET(g_iArcadeFortuneBS, ARCADE_FORTUNE_GLOBAL_BS_AIRSTRIKE) AND weaponUsed = WEAPONTYPE_AIRSTRIKE_ROCKET)
				SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_WASTED, DEFAULT, GET_SUICIDE_TICKER(NETWORK_PLAYER_ID_TO_INT(), TRUE))
				
				IF !g_b_RC_Race
					PRINT_TICKER(GET_SUICIDE_TICKER(NETWORK_PLAYER_ID_TO_INT(), TRUE))
				ENDIF
				
			ELSE
				BOOL bRunStandardWastedShard = TRUE
				
				IF GB_IS_LOCAL_PLAYER_RUNNING_GLOBAL_DEFEND()
				AND DOES_ENTITY_EXIST(killerPed)
				AND GB_IS_MODEL_GLOBAL_DEFEND_AI(GET_ENTITY_MODEL(killerPed))
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_WASTED, killerID, -1, "", "GB_SHARD_BUST", DEFAULT, DEFAULT, DEFAULT, BIG_MESSAGE_BIT_BUSTED)
					bRunStandardWastedShard = FALSE
				ENDIF
				
				IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_SHOW_BUSTED)
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_WASTED, killerID, -1, "", "GB_SHARD_BUST", DEFAULT, DEFAULT, DEFAULT, BIG_MESSAGE_BIT_BUSTED)
					bRunStandardWastedShard = FALSE
					CLEAR_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_SHOW_BUSTED)
				ENDIF
				
				IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_FMBB_DEFEND
				AND DOES_ENTITY_EXIST(killerPed)
				AND GET_ENTITY_MODEL(killerPed) = S_M_M_CIASEC_01
					SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_WASTED, killerID, -1, "", "GB_SHARD_BUST", DEFAULT, DEFAULT, DEFAULT, BIG_MESSAGE_BIT_BUSTED)
					bRunStandardWastedShard = FALSE
				ENDIF
				
				IF SHOULD_DO_ELIMINATE_TICKER(NETWORK_PLAYER_ID_TO_INT())
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_WASTED, DEFAULT, "TCK_ELMU")
					bRunStandardWastedShard = FALSE
				ENDIF
			
				IF bRunStandardWastedShard
					PRINTLN("bShowStandardShard 1 ")
					SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_WASTED)
				ENDIF
				
				IF g_i_RacePlayerExploded = NATIVE_TO_INT(PLAYER_ID())
					IF g_sRC_SB_CoronaOptions.iDestroyLoser = 1 // 1 = ciCORONA_DESTROY_VEHICLE_LAP
						// "~HUD_COLOUR_WHITE~You were last on this lap and destroyed."
						PRINT_TICKER("RCE_EXP_1")
						PRINTLN("[CS_EVENTS] g_i_RacePlayerExploded = ", g_i_RacePlayerExploded)
					ELSE
						// "~HUD_COLOUR_WHITE~You were last on this checkpoint and destroyed."
						PRINT_TICKER("RCE_EXP_3")
						PRINTLN("[CS_EVENTS] g_i_RacePlayerExploded = ", g_i_RacePlayerExploded)
					ENDIF
				ELSE
					IF NOT SHOULD_KILL_STRIP_RUN_BEAST_MODE()
					
					AND NOT SHOULD_DO_ELIMINATE_TICKER(NETWORK_PLAYER_ID_TO_INT())
					
						PRINT_TICKER("DM_TK_YD1")
						PRINTLN("[CS_EVENTS] ")
					ENDIF
				ENDIF
			ENDIF
		ELSE
			
			TEXT_LABEL_23 tlKillString
			
//			#IF FEATURE_NEW_AMBIENT_EVENTS
//			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bInfectionParticipant
//			
//				IF GlobalplayerBD_FM[NATIVE_TO_INT(killerID)].bAmInfected
//				AND NOT GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bAmInfected
//				
//					PRINTLN("     ---------->     INFECTION - Victim Kill Ticker - Injecting new feed string.")
//					tlKillString = "INF_TICK_2"
//					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bInfectionParticipant = FALSE
//				
//				ELSE
//					PRINTLN("     ---------->     INFECTION - Victim Kill Ticker - Killer: [",
//					NATIVE_TO_INT(killerID),", ", GET_PLAYER_NAME(killerID),"] bInfectionParticipant = ", 
//					GlobalplayerBD_FM[NATIVE_TO_INT(killerID)].bInfectionParticipant, ", bAmInfected = ",
//					GlobalplayerBD_FM[NATIVE_TO_INT(killerID)].bAmInfected)
//					
//					PRINTLN("     ---------->     INFECTION - Victim Kill Ticker - Victim: [",
//					NATIVE_TO_INT(PLAYER_ID()),", ", GET_PLAYER_NAME(PLAYER_ID()),"] bInfectionParticipant = ", 
//					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bInfectionParticipant, ", bAmInfected = ",
//					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bAmInfected)
//				ENDIF
//			
//			ELSE
//			#ENDIF

			// url:bugstar:5818571, use default tickers
			IF IS_PLAYER_AN_ANIMAL(killerID)
				weaponUsed = WEAPONTYPE_INVALID
			ENDIF
			
			tlKillString = GET_WEAPON_KILL_STRING(FALSE, weaponUsed, MPGlobals.g_KillStrip.killerData.bKilledUsingMelee)
			
//			#IF FEATURE_NEW_AMBIENT_EVENTS
//			ENDIF
//			#ENDIF
			BOOL bShowStandardShard = TRUE
			
			IF PLAYER_USING_ORBITAL(killerID)
				SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_WASTED, killerID, -1, "", "ORB_BM", DEFAULT, DEFAULT, DEFAULT, BIG_MESSAGE_BIT_ORBITAL)
				bShowStandardShard = FALSE
				PRINTLN("[JA@KILLSTRIP] INITIALISE_KILL_STRIP, PLAYER_USING_ORBITAL, killerID = ", GET_PLAYER_NAME(killerID))
			ENDIF
			
			// Prevent victim from seeing the name of the player that is The Beast
			IF SHOULD_REPLACE_NAME_WITH_BEAST(killerID)
				bShowStandardShard = FALSE
				SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_WASTED, INVALID_PLAYER_INDEX(), -1, "AMHB_BIG_KILLED")
			ENDIF
			
			IF bShowStandardShard
				PRINTLN("bShowStandardShard 2 ")
				SETUP_NEW_BIG_MESSAGE_WITH_PLAYER(BIG_MESSAGE_WASTED, killerID, -1, tlKillString)
			ENDIF		
			
			// (1845194)
			IF SHOULD_SHOW_KILL_TICKER(killerID)
				IF PLAYER_USING_ORBITAL(killerID)
					PRINT_TICKER_WITH_PLAYER_NAME("ORB_VICT", killerID)
				ELSE
					IF SHOULD_REPLACE_NAME_WITH_BEAST(killerID)
						PRINT_TICKER("AMHB_KILLED")
					ELSE
					
						IF GB_IS_PLAYER_BOSS_OF_A_GANG(killerID)
						AND GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())
						
							PRINT_TICKER_WITH_TWO_PLAYER_NAMES_AND_TWO_TEXT_LABELS("GB_TICK_BSSV", killerID, PLAYER_ID(), sGANG_ROLE(killerID), sGANG_ROLE(PLAYER_ID()))
						ELSE
							PRINT_TICKER_WITH_PLAYER_NAME(tlKillString, killerID)
						ENDIF

					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Refresh our instructional buttons	
	REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPGlobals.g_KillStrip.instructionalButtons)

	MPGlobals.g_KillStrip.iDeathTime = GET_NETWORK_TIME()

	// Change state of kill strip
	MPGlobals.g_KillStrip.iState = STATE_KILL_STRIP_LOADING
ENDPROC

// url:bugstar:4000984, when you are killing another player this kill vs kill display is hit for one frame before we need to clean it up to prevent old kill strip data appearing
PROC CLEANUP_KILLS_VS_KILLS()
	PRINTLN("CLEANUP_KILLS_VS_KILLS, ")
	CLEAR_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PLAYER_HEADSHOT)
	CLEAR_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_KILLER_HEADSHOT)
	CLEAR_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_KILLS_VS_KILLS_TICKER)
ENDPROC

//PURPOSE: Displays a ticker showing the Player Kills Vs the Killer Kills
PROC DISPLAY_KILLS_VS_KILLS(PLAYER_INDEX playerKiller, PLAYER_INDEX playerDead, BOOL bNoKillstrip = FALSE)
	IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
	
		PRINTLN("DISPLAY_KILLS_VS_KILLS, A ")
		
		IF playerKiller = INVALID_PLAYER_INDEX()
		OR NOT IS_NET_PLAYER_OK(playerKiller, FALSE)
			PRINTLN("DISPLAY_KILLS_VS_KILLS, playerKiller = INVALID_PLAYER_INDEX() ")
			EXIT
		ENDIF
		
		//For Bug 1911368
		IF DOES_TEAM_LIKE_TEAM(GET_PLAYER_TEAM(playerDead), GET_PLAYER_TEAM(playerKiller))
			EXIT
		ENDIF
		
		IF MPGlobals.g_KillStrip.bIsInstigatedSuicide = TRUE
			EXIT
		ENDIF
		
		PRINTLN("DISPLAY_KILLS_VS_KILLS, B ")
		
		//Get Player Headshot
		IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PLAYER_HEADSHOT)
			PEDHEADSHOT_ID PlayerHeadshot = Get_HeadshotID_For_Player(playerDead)
			IF PlayerHeadshot != NULL
				MPGlobals.g_KillStrip.strPlayerHeadshotTxd = GET_PEDHEADSHOT_TXD_STRING(PlayerHeadshot)
				PRINTLN("KILLSTRIP - Found player: ", GET_PLAYER_NAME(playerDead), " headshot")
				SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PLAYER_HEADSHOT)
			ENDIF
		ENDIF
		
		//Get Killer Headshot
		IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_KILLER_HEADSHOT)
			MPGlobals.g_KillStrip.killerPicId = Get_HeadshotID_For_Player(playerKiller)
			IF MPGlobals.g_KillStrip.killerPicId != NULL
				IF IS_PEDHEADSHOT_READY(MPGlobals.g_KillStrip.killerPicId)
					MPGlobals.g_KillStrip.killerPicTxd = GET_PEDHEADSHOT_TXD_STRING(MPGlobals.g_KillStrip.killerPicId)
					PRINTLN("KILLSTRIP - Found killer: ", GET_PLAYER_NAME(playerKiller), " headshot")
					SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_KILLER_HEADSHOT)
				ELSE
					PRINTLN("KILLSTRIP - Found killer: but the pedheadshot is being refreshed or not ready. check for GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE. ")
				ENDIF
			ENDIF
		ENDIF
		
		//Display Kills Vs Kills
		IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_KILLS_VS_KILLS_TICKER)
		OR bNoKillstrip
			PRINTLN("DISPLAY_KILLS_VS_KILLS, C ")
			IF HAS_NET_TIMER_EXPIRED(MPGlobals.g_KillStrip.KillsVsKillsDelayTimer, KILLSTRIP_KILLS_TICKER_DELAY)
			OR bNoKillstrip
			
				PRINTLN("DISPLAY_KILLS_VS_KILLS, D ")
				IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_PLAYER_HEADSHOT)
				AND IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_KILLER_HEADSHOT)
				
					IF bNoKillstrip
						MPGlobals.g_KillStrip.killerData.iPlayerKilledKiller = MPGlobalsEvents.number_times_killed_player[NATIVE_TO_INT(playerDead)]	//MPGlobalsEvents.number_times_killed_by_player[NATIVE_TO_INT(playerDead)]	
						MPGlobals.g_KillStrip.killerData.iKilledByKiller = MPGlobalsEvents.number_times_killed_by_player[NATIVE_TO_INT(playerDead)]		//MPGlobalsEvents.number_times_killed_player[NATIVE_TO_INT(playerDead)]
					ELSE
						MPGlobals.g_KillStrip.killerData.iPlayerKilledKiller = MPGlobalsEvents.number_times_killed_by_player[NATIVE_TO_INT(playerKiller)]	// Confusing wording (player here is referring to local player)
						MPGlobals.g_KillStrip.killerData.iKilledByKiller = MPGlobalsEvents.number_times_killed_player[NATIVE_TO_INT(playerKiller)]
					ENDIF
					
					PRINTLN("DISPLAY_KILLS_VS_KILLS, iPlayerKilledKiller ", MPGlobals.g_KillStrip.killerData.iPlayerKilledKiller)
					PRINTLN("DISPLAY_KILLS_VS_KILLS, iKilledByKiller ", MPGlobals.g_KillStrip.killerData.iKilledByKiller)
					
					HUD_COLOURS eLocalTeamHUDColour = HUD_COLOUR_BLUE
					HUD_COLOURS eKillerTeamHUDColour = HUD_COLOUR_RED
					
					INT iLocalTeamID = GET_PLAYER_TEAM(playerDead)
					INT iKillerTeamID = GET_PLAYER_TEAM(playerKiller)
					
					IF iLocalTeamID != -1
						eLocalTeamHUDColour = GET_PLAYER_HUD_COLOUR(playerDead, iLocalTeamId)
					ENDIF
					
					IF iKillerTeamID != -1
						eKillerTeamHUDColour = GET_PLAYER_HUD_COLOUR(playerKiller, iKillerTeamID)
					ENDIF
					
					IF GB_IS_PLAYER_BOSS_OF_A_GANG(playerKiller)
					AND GB_IS_PLAYER_BOSS_OF_A_GANG(playerDead)

						eLocalTeamHUDColour = GB_GET_PLAYER_GANG_HUD_COLOUR(playerDead)
						
						eKillerTeamHUDColour = GB_GET_PLAYER_GANG_HUD_COLOUR(playerKiller)
						
						PRINTLN("KILLSTRIP - 3047433 colours Killer: ", GET_PLAYER_NAME(playerKiller), " Victim: ", GET_PLAYER_NAME(playerDead))
					ENDIF
					 // #IF FEATURE_BIKER

					PRINTLN("KILLSTRIP - Local Team ID: ", iLocalTeamID, " Local Team HUD Colour: ", HUD_COLOUR_AS_STRING(eLocalTeamHUDColour))
					PRINTLN("KILLSTRIP - Killer Team ID: ", iKillerTeamID, " Killer Team HUD Colour: ", HUD_COLOUR_AS_STRING(eKillerTeamHUDColour))
					
					BEGIN_TEXT_COMMAND_THEFEED_POST("")
					END_TEXT_COMMAND_THEFEED_POST_VERSUS_TU(MPGlobals.g_KillStrip.killerPicTxd, MPGlobals.g_KillStrip.killerPicTxd, MPGlobals.g_KillStrip.killerData.iKilledByKiller, MPGlobals.g_KillStrip.strPlayerHeadshotTxd, MPGlobals.g_KillStrip.strPlayerHeadshotTxd, MPGlobals.g_KillStrip.killerData.iPlayerKilledKiller, eKillerTeamHUDColour, eLocalTeamHUDColour)
					SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_KILLS_VS_KILLS_TICKER)
					PRINTLN("KILLSTRIP - KILLS VS KILLS TICKER DONE")
				ELSE
					PRINTLN("DISPLAY_KILLS_VS_KILLS, F ")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CHECK_FOR_KILLER_LEAVING()
	IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_IM_DM_UNAVAILABLE)
	AND MPGlobals.g_KillStrip.killerData.killerID != INVALID_PLAYER_INDEX()
	AND NATIVE_TO_INT(MPGlobals.g_KillStrip.killerData.killerID) > -1
		//IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_START_IM_DM)
		IF IS_NET_PLAYER_OK(MPGlobals.g_KillStrip.killerData.killerID, FALSE)
			SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_IM_DM_UNAVAILABLE)
			PRINTLN("[JA@KILLSTRIP] KILL_STRIP_IM_DM_UNAVAILABLE - SET")
			IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_IM_DM_LOADING_SPINNER)
				SET_LOADING_ICON_INACTIVE()
				PRINTLN("[JA@KILLSTRIP] KILL_STRIP_IM_DM_UNAVAILABLE - SET_LOADING_ICON_INACTIVE")
			ENDIF
			ADD_KILL_STRIP_BUTTONS()
		ENDIF
	ENDIF
ENDPROC
	
PROC RENDER_RESPAWN_BUTTONS()
	
//	IF g_SpawnData.bPlayerWantsToRespawn
//		EXIT	// We don't need to render anything now
//	ENDIF
	IF GET_GAME_STATE_ON_DEATH() = AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP
		EXIT
	ENDIF
	
	IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_HIDE_STRIP)
		EXIT
	ENDIF 
	
	IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_SHOW_BUTTONS)
	//AND NOT g_SpawnData.bPlayerWantsToRespawn)
	
	#IF IS_DEBUG_BUILD
	OR bDebugKillStrip
	#ENDIF

		// Clear any on call HUD 2280950
		SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
		SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
		
		SPRITE_PLACEMENT aSprite
		aSprite = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
		
		//SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
		
		IF HAS_SCALEFORM_MOVIE_LOADED(MPGlobals.g_KillStrip.buttonMovie)
		
				// Check text chat closed for pc
			IF IS_PC_VERSION()
				// Wait until we are allowed to display
				IF IS_KILL_STRIP_HUD_ALLOWED_TO_DISPLAY()
				AND NOT IS_MP_TEXT_CHAT_TYPING()
		
					RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPGlobals.g_KillStrip.buttonMovie, aSprite, MPGlobals.g_KillStrip.instructionalButtons, 
															SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPGlobals.g_KillStrip.instructionalButtons))
				ENDIF
			ELSE
				// Wait until we are allowed to display
				IF IS_KILL_STRIP_HUD_ALLOWED_TO_DISPLAY()
		
					RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPGlobals.g_KillStrip.buttonMovie, aSprite, MPGlobals.g_KillStrip.instructionalButtons, 
															SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPGlobals.g_KillStrip.instructionalButtons))
				ENDIF
			ENDIF
		ELSE
			MPGlobals.g_KillStrip.buttonMovie = REQUEST_SCALEFORM_MOVIE("instructional_buttons")
		ENDIF

		//SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	ENDIF
	
ENDPROC

PROC PROCESS_KILLSTRIP_RESPAWN_TIMER(INT &iTimeLeft)
	BOOL bHideRespawnBar
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
			bHideRespawnBar = TRUE
		ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
			IF g_mnOfLastVehicleWeWasIn = BOMBUSHKA
				PRINTLN("[LM][MAINTAIN_KILL_STRIP] - Last Vehicle we were in was a Bombushka. Blocking Respawn Bar. (1)")
				bHideRespawnBar = TRUE
			ENDIF
		ENDIF
		
		IF g_bMissionHideRespawnBar
		OR g_bMissionHideRespawnBarForRestart
			bHideRespawnBar = TRUE
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_CAN_RESPAWN)
		
		// 1578396
		//IF IS_ON_DEATHMATCH_GLOBAL_SET() 
			// 1370504 - Add the respawn speed up to time as we sometimes can hit X to speed it up
			//iTimeLeft = ((KILL_STRIP_DEATH_EFFECT_TIMER + KILL_STRIP_DM_RESPAWN_BAR_TIMER + KILL_STRIP_DEATH_WASTED_TIMER) - (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.g_KillStrip.iDeathTime)) + MPGlobals.g_KillStrip.iRespawnFastRespawn))
		//ELSE
		//IF (IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER) AND NOT IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID()))
		//OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_BASE_JUMP
		
		IF DO_QUICK_RESPAWN()
			iTimeLeft = ((KILL_STRIP_DEATH_EFFECT_TIMER+KILL_STRIP_RESPAWN_BAR_TIMER+KILL_STRIP_DEATH_WASTED_TIMER) - (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.g_KillStrip.iDeathTime))))
			PRINTLN("KILL STRIP - PROCESS_KILLSTRIP_RESPAWN_TIMER - SHORT - iTimeLeft = ", iTimeLeft)
		ELSE
			// 1370504 - Add the respawn speed up to time as we sometimes can hit X to speed it up
			iTimeLeft = (KILL_STRIP_RESPAWN_TIME - (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.g_KillStrip.iDeathTime)) + MPGlobals.g_KillStrip.iRespawnFastRespawn))
			PRINTLN("KILL STRIP - PROCESS_KILLSTRIP_RESPAWN_TIMER - NORMAL - iTimeLeft = ", iTimeLeft)
			
			IF PLAYER_USING_ORBITAL(MPGlobals.g_KillStrip.killerData.killerID)
				iTimeLeft = iTimeLeft + 5
				CLEAR_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_ALLOW_RESPAWN_BAR)
				PRINTLN("[JA@KILLSTRIP] 3990549 CANNON KILL ", iTimeLeft)
			ENDIF

		ENDIF
		
		PRINTLN("[JA@KILLSTRIP] iTimeLeft ", iTimeLeft)
			
		IF iTimeLeft <= 0
			iTimeLeft = 0

			NET_PRINT("[JA@KILLSTRIP] RENDER_RESPAWN_BUTTONS - timer up, allow to respawn") NET_NL()
			
			// Clear all our help
			CLEAR_HELP(TRUE)
			
			SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_CAN_RESPAWN)
			
			IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_ALLOW_RESPAWN_BAR)
				IF g_bMissionEnding = FALSE
				AND g_b_JobEnded = FALSE
				AND IS_SKYSWOOP_AT_GROUND()
					PLAY_SOUND_FRONTEND(-1, "Faster_Bar_Full", "RESPAWN_ONLINE_SOUNDSET")
					PRINTLN("[JA@KILLSTRIP] SOUND 'Faster_Bar_Full' PLAYED")
				ENDIF
			ENDIF
			
			//Keep Bar Drawing on Fade Out
			IF GET_GAME_STATE_ON_DEATH() != AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP
			AND NOT DO_QUICK_RESPAWN()	//AND (IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID()) OR NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER))
			//AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_BASE_JUMP
			AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_HIDE_STRIP)
				DRAW_GENERIC_METER(KILL_STRIP_DM_RESPAWN_BAR_TIMER, KILL_STRIP_DM_RESPAWN_BAR_TIMER, "KS_RESPAWN_B", HUD_COLOUR_RED, 0, HUDORDER_DONTCARE, -1, -1, FALSE, TRUE)
			ENDIF
			
			//IF IS_KILLSTRIP_SET_TO_AUTO_RESPAWN()
				//SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_RESPAWNING)
			//ENDIF
		ELSE
			// Draw our bar (TODO: Wrap with are we in a mission)
			//IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
			IF GET_GAME_STATE_ON_DEATH() != AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP
			AND NOT DO_QUICK_RESPAWN()	//AND (IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID()) OR NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER))
			//AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_BASE_JUMP
			AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_HIDE_STRIP)
				INT iBarTimer = KILL_STRIP_DM_RESPAWN_BAR_TIMER
				/*IF IS_ON_DEATHMATCH_GLOBAL_SET() 
					iBarTimer = KILL_STRIP_DM_RESPAWN_BAR_TIMER
				ELSE
					iBarTimer = KILL_STRIP_RESPAWN_BAR_TIMER
				ENDIF*/
				IF iTimeLeft < iBarTimer
					IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_ALLOW_RESPAWN_BAR)
						PRINTLN("[JA@KILLSTRIP] SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_ALLOW_RESPAWN_BAR")
						
						IF NOT PLAYER_USING_ORBITAL(MPGlobals.g_KillStrip.killerData.killerID)
						
							SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_ALLOW_RESPAWN_BAR)
						
						ENDIF
						
//						REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPGlobals.g_KillStrip.instructionalButtons)
//						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "HUD_INPUT27", MPGlobals.g_KillStrip.instructionalButtons)
//						SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_SHOW_BUTTONS)					
					ENDIF			
				
					//PRINTLN("[JA@KILLSTRIP] DRAW_GENERIC_METER: ", iTimeLeft)
					//REQUEST_SCRIPT("TimersHUD")
					//REQUEST_STREAMED_TEXTURE_DICT("TimerBars")

					//IF MPGlobals.g_KillStrip.iRespawnFastRespawn > 0
						
						//REMOVED BAR FLASH FOR BUG 1630812
						//IF HAS_NET_TIMER_EXPIRED(MPGlobals.g_KillStrip.RespawnBarFlashTimer, MPGlobals.g_KillStrip.RespawnBarFlashLength)						
							
							IF NOT bHideRespawnBar
								DRAW_GENERIC_METER((iBarTimer - iTimeLeft), iBarTimer, "KS_RESPAWN_B", HUD_COLOUR_RED, 0, HUDORDER_DONTCARE, -1, -1, FALSE, TRUE)
							ENDIF
						//ELSE
						//	DRAW_GENERIC_METER((iBarTimer - iTimeLeft), iBarTimer, "KS_RESPAWN_B", HUD_COLOUR_WHITE, 0, HUDORDER_DONTCARE, -1, -1, FALSE, TRUE)
						//ENDIF
						
						//INT iPercentage = ROUND((TO_FLOAT(iBarTimer - iTimeLeft)/iBarTimer)*100)
						//PRINTLN("KILLSTRIP - RESPAWN BAR PERCENTAGE = ", iPercentage)
						
						//REMOVED FOR BUG 1613380
						/*SET_BIG_MESSAGE_RESPAWN_BAR(iPercentage)*/
						//IF MPGlobals.g_KillStrip.iRespawnFastRespawn = 240
							//MAKE_BIG_MESSAGE_RESPAWN_FLASH()
							//FLASH_BUTTON_BY_ID
							//MPGlobals.g_KillStrip.iRespawnFastRespawn += 1
						//ENDIF
					//ENDIF
					
				//ELSE			
					//DRAW_GENERIC_METER(iTimeLeft, iBarTimer, "KS_RESPAWN_B")
				ENDIF
				
				//If not a suicide then udpate strapline with hit point location (only Headshot just now) - REMOVED FOR BUG 1635638
				/*IF MPGlobals.g_KillStrip.killerData.killerID != PLAYER_ID()
				AND MPGlobals.g_KillStrip.iRespawnFastRespawn = 0
					IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_UPDATE_STRAPLINE)
						IF iTimeLeft < (iBarTimer/2)
							IF MPGlobals.g_KillStrip.hitPoints[0] = ENUM_TO_INT(RAGDOLL_HEAD)
								UPDATE_BIG_MESSAGE_STRAPLINE_AND_INT("DM_HEAD_INC", -1)
							ENDIF
							SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_UPDATE_STRAPLINE)
						ENDIF
					ENDIF
				ENDIF*/
			ENDIF
			
			iTimeLeft = CEIL(iTimeLeft / 1000.0)
		ENDIF
		
		ADD_KILL_STRIP_BUTTONS(iTimeLeft)
		
	ELSE
		
		//Keep Bar Drawing on Fade Out
		IF GET_GAME_STATE_ON_DEATH() != AFTERLIFE_SET_SPECTATORCAM_AFTER_KILLSTRIP
		AND NOT DO_QUICK_RESPAWN()	//AND (IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID()) OR NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER))
		//AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_BASE_JUMP
		AND NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_HIDE_STRIP)
			IF NOT bHideRespawnBar
				DRAW_GENERIC_METER(KILL_STRIP_DM_RESPAWN_BAR_TIMER, KILL_STRIP_DM_RESPAWN_BAR_TIMER, "KS_RESPAWN_B", HUD_COLOUR_RED, 0, HUDORDER_DONTCARE, -1, -1, FALSE, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//////////// DEBUG /////////////
#IF IS_DEBUG_BUILD
PROC CREATE_KILL_STRIP_WIDGETS()

	START_WIDGET_GROUP("Kill Strip / Afterlife")
		//ADD_WIDGET_INT_SLIDER("WASTED_SOUND_DELAY", WASTED_SOUND_DELAY, 0, 5000, 25)
		ADD_WIDGET_BOOL("Hold on Kill Strip", bHoldOnKillStrip)
		ADD_WIDGET_BOOL("Hold on Kill Strip", bHoldOnKillStrip)
		ADD_WIDGET_BOOL("Debug Kill Strip", bDebugKillStrip)
		
		ADD_WIDGET_BOOL("Display Killstreak", bDisplayKillStreak)
		ADD_WIDGET_INT_SLIDER("Score 1", iKillStreakScoreOne, 0, 100, 1)
		ADD_WIDGET_INT_SLIDER("Score 2", iKillStreakScoreTwo, 0, 100, 1)
		ADD_WIDGET_INT_SLIDER("HUD Colour 1", iHUDColourOne, 0, 216, 1)
		ADD_WIDGET_INT_SLIDER("HUD Colour 2", iHUDColourTwo, 0, 216, 1)
		
		ADD_WIDGET_STRING("0 - BRINGUPHUD, 1 - SPECTATORCAM, 2 - JUST_REJOIN, 3 - BRINGUPHUD_AFTER_TIME")
		ADD_WIDGET_STRING("4 - SPECTATORCAM_AFTER_TIME, 5 - REJOIN_AFTER_TIME, 6 - REJOIN_INTO_LAST_VEHICLE")
		ADD_WIDGET_STRING("7 - JUST_AUTO_REJOIN")
		ADD_WIDGET_INT_READ_ONLY("Afterlife state", g_iAfterLifeWidget)
	STOP_WIDGET_GROUP()

ENDPROC
#ENDIF

/// PURPOSE: Returns TRUE when killstrip is being displayed
FUNC BOOL IS_KILLSTRIP_BEING_DISPLAYED()
	RETURN (MPGlobals.g_KillStrip.iState != STATE_KILL_STRIP_NULL)
ENDFUNC

PROC MAINTAIN_GRIEF_PASSIVE_HELP()
	INT iStatInt

	IF NOT IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet11, BI_FM_NMH11_DONE_GRIEF_PASSIVE_HELP)
	AND IS_KILLER_A_GRIEFER()
	AND CAN_PLAYER_USE_GRIEF_PASSIVE()
	AND NOT AM_I_ALREADY_GHOSTED_TO_KILLER()
		iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT8)
		
		IF NOT IS_BIT_SET(iStatInt, biNmh8_DoneGriefPassiveHelp1)
		OR NOT IS_BIT_SET(iStatInt, biNmh8_DoneGriefPassiveHelp2)
		OR NOT IS_BIT_SET(iStatInt, biNmh8_DoneGriefPassiveHelp3)
			IF GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(MPGlobals.g_KillStrip.killerData.killerID)
				PRINT_HELP("GRIEF_HELP2")
				CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_GRIEF_PASSIVE_HELP] PRINT_HELP(GRIEF_HELP2)")
			ELIF GB_IS_PLAYER_MEMBER_OF_A_GANG(MPGlobals.g_KillStrip.killerData.killerID)
				PRINT_HELP("GRIEF_HELP3")
				CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_GRIEF_PASSIVE_HELP] PRINT_HELP(GRIEF_HELP3)")
			ELSE
				PRINT_HELP("GRIEF_HELP")
				CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_GRIEF_PASSIVE_HELP] PRINT_HELP(GRIEF_HELP)")
			ENDIF
			
			IF IS_BIT_SET(iStatInt, biNmh8_DoneGriefPassiveHelp2)
				SET_BIT(iStatInt, biNmh8_DoneGriefPassiveHelp3)
				CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_GRIEF_PASSIVE_HELP] biNmh8_DoneGriefPassiveHelp3 SET.")
			ELIF IS_BIT_SET(iStatInt, biNmh8_DoneGriefPassiveHelp1)
				SET_BIT(iStatInt, biNmh8_DoneGriefPassiveHelp2)
				CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_GRIEF_PASSIVE_HELP] biNmh8_DoneGriefPassiveHelp2 SET.")
			ELSE
				SET_BIT(iStatInt, biNmh8_DoneGriefPassiveHelp1)
				CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_GRIEF_PASSIVE_HELP] biNmh8_DoneGriefPassiveHelp1 SET.")
			ENDIF
			
			SET_MP_INT_CHARACTER_STAT(MP_STAT_FM_NON_MS_HELP_TEXT8, iStatInt) 
			SET_BIT(MPGlobalsAmbience.iFmNmhBitSet11, BI_FM_NMH11_DONE_GRIEF_PASSIVE_HELP)
		ELSE
			SET_BIT(MPGlobalsAmbience.iFmNmhBitSet11, BI_FM_NMH11_DONE_GRIEF_PASSIVE_HELP)
			CPRINTLN(DEBUG_NET_MAGNATE, "[RE] [MAINTAIN_GRIEF_PASSIVE_HELP] IS_BIT_SET(iStatInt, biNmh8_DoneGriefPassiveHelp) = TRUE. Already done BI_FM_NMH11_DONE_GRIEF_PASSIVE_HELP help message.")
		ENDIF
	ENDIF
ENDPROC


// ----------------------------------------------------------------------
//                          PUBLIC FUNCTIONS
// ----------------------------------------------------------------------

/// PURPOSE: Called within net_spawn.sch while player is on kill strip
FUNC BOOL MAINTAIN_KILL_STRIP()
	
	BOOL bHideRespawnBar
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_OVERTIME_ANY(g_FMMC_STRUCT.iAdversaryModeType)
		OR IS_THIS_ROCKSTAR_MISSION_NEW_VS_AIRSHOOTOUT(g_FMMC_STRUCT.iAdversaryModeType)
			bHideRespawnBar = TRUE
		ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_BOMBUSHKA(g_FMMC_STRUCT.iAdversaryModeType)
			IF g_mnOfLastVehicleWeWasIn = BOMBUSHKA
				PRINTLN("[LM][MAINTAIN_KILL_STRIP] - Last Vehicle we were in was a Bombushka. Blocking Respawn Bar. (2)")
				bHideRespawnBar = TRUE
			ENDIF
		ENDIF
		
		IF g_bMissionHideRespawnBar
		OR g_bMissionHideRespawnBarForRestart
			bHideRespawnBar = TRUE
		ENDIF
	ENDIF
	
	// If player is on pause menu, don't load / draw anything (and wait).
	/*IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF*/
	
	IF NOT IS_SAFE_TO_SHOW_KILLSTRIP()
		RETURN FALSE
	ENDIF
	
	IF MPGlobals.g_KillStrip.iState > STATE_KILL_STRIP_NULL
		INT i
		INT iPlayerInt = NATIVE_TO_INT(MPGlobals.g_KillStrip.killerData.killerID)
		REPEAT NUM_NETWORK_PLAYERS i
			IF i != iPlayerInt
				DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_THISPLAYER_OVERHEADS, i)
			ENDIF
		ENDREPEAT
	ENDIF
	
	// Return if we want to debug hold on wasted screen
	#IF IS_DEBUG_BUILD
		IF bHoldOnKillStrip
			RETURN FALSE
		ENDIF
	#ENDIF
	
	// So we can display mouse button in the instructional buttons
	caRespawnInput = INPUT_RESPAWN_FASTER
	
	INT iTimeLeft
	// Load kill strip
	SWITCH MPGlobals.g_KillStrip.iState 
	
		CASE STATE_KILL_STRIP_NULL
			// Do nothing
		BREAK
		
		CASE STATE_KILL_STRIP_LOADING
			//IF HAS_KILL_STRIP_SCALEFORM_LOADED()
				
				// Pause any objective text within missions
				Pause_Objective_Text()
				
				// Don't want to hide the radar / feed or help text for Conor's bounty
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RADIO_STATIONS)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SAVING_GAME)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
				
				IF NOT MPGlobalsHud.b_ShowSubtitlesDuringKillstrip
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
				ENDIF
				
				DISPLAY_HUD_WHEN_NOT_IN_STATE_OF_PLAY_THIS_FRAME()
				DISABLE_ALL_MP_HUD_THIS_FRAME()
				//HIDE_LOADING_ON_FADE_THIS_FRAME()
				
				IF NOT IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
					
					NET_PRINT("[JA@KILLSTRIP] MAINTAIN_KILL_STRIP - we are showing killstrip so set up all scaleform info") NET_NL()

					// Get the latest values we can get before displaying
					REFRESH_KILL_V_DEATH_VALUES(MPGlobals.g_KillStrip.killerData)
					
					// Is Impromptu DM available?
					IF CAN_TRIGGER_IMPROMPTU_DM_KILLSTRIP()
					
						//...yes, do we need to display the help
						IF OK_TO_DISPLAY_IMPROMPTU_DM_HELP()
							PRINTLN("[JA@KILLSTRIP] OK_TO_DISPLAY_IMPROMPTU_DM_HELP() TRUE - print help")
							PRINT_HELP("KILL_STRIP_H")
							
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("[JA@KILLSTRIP] OK_TO_DISPLAY_IMPROMPTU_DM_HELP() is FALSE")
						#ENDIF
						ENDIF
						
					ENDIF
					
					MAINTAIN_GRIEF_PASSIVE_HELP()
				ENDIF
				
				NET_PRINT("[JA@KILLSTRIP] MAINTAIN_KILL_STRIP - everything has loaded. Move to rendering")
				
				// Make sure the killstrip buttons are refreshed
				SET_BIT(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_REFRESH_BUTTONS)
				
				MPGlobals.g_KillStrip.iState = STATE_KILL_STRIP_RENDERING
				
			/*ELSE
				IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPGlobals.g_KillStrip.iDeathTime)) >= 30000
					NET_PRINT("[JA@KILLSTRIP] STATE_KILL_STRIP_LOADING has timed out (>30s). Auto respawning player") NET_NL()
				
					RETURN TRUE
				ENDIF
			ENDIF*/
			
			IF NOT bHideRespawnBar
				MAINTAIN_KILL_STRIP_FAST_RESPAWN()
			ENDIF
			
			IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_RESURRECTION(g_FMMC_STRUCT.iAdversaryModeType)
				// Listen for input, TRUE when player presses X
				PROCESS_KILLSTRIP_RESPAWN_TIMER(iTimeLeft)
				
				IF NOT bHideRespawnBar
					RENDER_RESPAWN_BUTTONS()
				ENDIF
			ENDIF
				
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
				IF PLAYER_ID() != INVALID_PLAYER_INDEX()
					IF GET_PLAYER_TEAM(PLAYER_ID()) > -1
						IF g_bMissionInstantRespawn
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// If our time runs out and we should autorespawn - return
			//IF IS_KILLSTRIP_SET_TO_AUTO_RESPAWN()
			//OR IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
				IF iTimeLeft <= 0
					RETURN TRUE
				ENDIF
			//ENDIF
	
			IF PROCESS_KILL_STRIP_INPUT()
				NET_PRINT("[JA@KILLSTRIP] MAINTAIN_KILL_STRIP - STATE_KILL_STRIP_LOADING respawn pressed. Cleaning up script")
				RETURN TRUE
			ENDIF
			
		BREAK
		
		CASE STATE_KILL_STRIP_RENDERING
		
			// Don't want to hide the radar / feed or help text for Conor's bounty
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RADIO_STATIONS)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SAVING_GAME)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
			
			IF NOT MPGlobalsHud.b_ShowSubtitlesDuringKillstrip
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
			ENDIF
			
			DISPLAY_HUD_WHEN_NOT_IN_STATE_OF_PLAY_THIS_FRAME()
			DISABLE_ALL_MP_HUD_THIS_FRAME()
			//HIDE_LOADING_ON_FADE_THIS_FRAME()		
				
			IF NOT IS_THIS_ROCKSTAR_MISSION_NEW_VS_RESURRECTION(g_FMMC_STRUCT.iAdversaryModeType)	
				IF IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags,KILL_STRIP_REFRESH_BUTTONS)
					ADD_KILL_STRIP_BUTTONS(0)
					NET_PRINT("Adding kill strip buttons") NET_NL()
					CLEAR_BIT(MPGlobals.g_KillStrip.iKillStripFlags,KILL_STRIP_REFRESH_BUTTONS)
				ENDIF
			
				IF NOT bHideRespawnBar
					RENDER_RESPAWN_BUTTONS()
				ENDIF
			
				PROCESS_KILLSTRIP_RESPAWN_TIMER(iTimeLeft)
			ENDIF
			
			IF NOT bHideRespawnBar
				MAINTAIN_KILL_STRIP_FAST_RESPAWN()
				
				PROCESS_KILL_STRIP_INPUT()
			ENDIF	
			
			DISPLAY_KILLS_VS_KILLS(MPGlobals.g_KillStrip.killerData.killerID, PLAYER_ID())
			
			CHECK_FOR_KILLER_LEAVING()
			
			// Process if we can allow respawn
			//IF IS_KILLSTRIP_SET_TO_AUTO_RESPAWN()
			//OR IS_BIT_SET(MPGlobals.g_KillStrip.iKillStripFlags, KILL_STRIP_NO_KILLER)
				IF iTimeLeft <= 0
					NET_PRINT("[JA@KILLSTRIP] MAINTAIN_KILL_STRIP - iTimeLeft <= 0 -> RESPAWN") NET_NL()
					RETURN TRUE
				ENDIF
			//ENDIF
			
			// Listen for input, TRUE when player presses X
			/*IF PROCESS_KILL_STRIP_INPUT()
				NET_PRINT("[JA@KILLSTRIP] MAINTAIN_KILL_STRIP - STATE_KILL_STRIP_RENDERING respawn pressed. Cleaning up script") NET_NL()
				RETURN TRUE
			ENDIF*/
		BREAK
		
		CASE STATE_KILL_STRIP_SKIP
			RETURN TRUE
		BREAK
	ENDSWITCH	
	
	RETURN FALSE	
ENDFUNC

// -------------- KILL STRIP VOICE SETTINGS -------------

/// PURPOSE: Death has happened, we need to hear the killer or victim
PROC SET_PLAYER_VOICE_SETTINGS_FOR_DEATH(PLAYER_INDEX playerID)
	MPGlobals.g_KillStripVoiceData.killStripVoiceTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), KILL_STRIP_VOICE_TIMER)
	MPGlobals.g_KillStripVoiceData.bVoiceTimerActive = TRUE
	
	// Clean up any previous voice settings
	IF MPGlobals.g_KillStripVoiceData.bVoiceTimerActive
		IF IS_NET_PLAYER_OK(MPGlobals.g_KillStripVoiceData.voicePlayerID, FALSE, TRUE)
			NETWORK_OVERRIDE_CHAT_RESTRICTIONS(MPGlobals.g_KillStripVoiceData.voicePlayerID, FALSE)
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("SET_PLAYER_VOICE_SETTINGS_FOR_DEATH - allowing killer / victim to hear each other") NET_NL()
	#ENDIF
	
	MPGlobals.g_KillStripVoiceData.voicePlayerID = playerID
	NETWORK_OVERRIDE_CHAT_RESTRICTIONS(playerID, TRUE)
ENDPROC

/// PURPOSE: Monitor if we are allowing cross team comms. If timer expired, clean up
PROC MAINTAIN_KILL_STRIP_VOICE_SETTINGS()
	
	IF MPGlobals.g_KillStripVoiceData.bVoiceTimerActive
		IF IS_TIME_LESS_THAN(MPGlobals.g_KillStripVoiceData.killStripVoiceTimer, GET_NETWORK_TIME())
		
			#IF IS_DEBUG_BUILD
				NET_PRINT("MAINTAIN_KILL_STRIP_VOICE_SETTINGS - cleaning up voice setting between players") NET_NL()
			#ENDIF
			
			MPGlobals.g_KillStripVoiceData.bVoiceTimerActive = FALSE
			IF MPGlobals.g_KillStripVoiceData.voicePlayerID != INVALID_PLAYER_INDEX()
			AND IS_NET_PLAYER_OK(MPGlobals.g_KillStripVoiceData.voicePlayerID, FALSE)
				IF NOT IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(MPGlobals.g_KillStripVoiceData.voicePlayerID)
					IF DOES_PLAYER_PASS_CHAT_OPTION(MPGlobals.g_KillStripVoiceData.voicePlayerID)
						NETWORK_OVERRIDE_CHAT_RESTRICTIONS(MPGlobals.g_KillStripVoiceData.voicePlayerID, TRUE)
					ELSE
						NETWORK_OVERRIDE_CHAT_RESTRICTIONS(MPGlobals.g_KillStripVoiceData.voicePlayerID, FALSE)
					ENDIF
				ELSE
					NETWORK_OVERRIDE_CHAT_RESTRICTIONS(MPGlobals.g_KillStripVoiceData.voicePlayerID, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Process stealth kills
	IF NOT MPGlobals.g_KillStripVoiceData.bVoiceTimerActive
		
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF IS_PED_BEING_STEALTH_KILLED(PLAYER_PED_ID())
				
				// GET THE PED and see if player
				PED_INDEX pedKiller = GET_MELEE_TARGET_FOR_PED(PLAYER_PED_ID())
				
				IF DOES_ENTITY_EXIST(pedKiller)
					IF IS_PED_A_PLAYER(pedKiller)
						
						PLAYER_INDEX playerKiller = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedKiller)
						IF playerKiller != INVALID_PLAYER_INDEX()
							
							NET_PRINT("MAINTAIN_KILL_STRIP_VOICE_SETTINGS - I'm being stelath killed, open chat to: ") NET_PRINT(GET_PLAYER_NAME(playerKiller)) NET_NL()
							
							// Open up chat
							MPGlobals.g_KillStripVoiceData.killStripVoiceTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), 6000)
							MPGlobals.g_KillStripVoiceData.bVoiceTimerActive = TRUE
							MPGlobals.g_KillStripVoiceData.voicePlayerID = playerKiller
							NETWORK_OVERRIDE_CHAT_RESTRICTIONS(playerKiller, TRUE)				
						ENDIF
					ENDIF
				ENDIF
			
			ELIF IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
				
				// GET THE PED and see if player
				PED_INDEX pedVictim = GET_MELEE_TARGET_FOR_PED(PLAYER_PED_ID())
				
				IF DOES_ENTITY_EXIST(pedVictim)
					IF IS_PED_A_PLAYER(pedVictim)
						
						PLAYER_INDEX playerVictim = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedVictim)
						IF playerVictim != INVALID_PLAYER_INDEX()
							
							NET_PRINT("MAINTAIN_KILL_STRIP_VOICE_SETTINGS - I'm stealth killing, open chat to: ") NET_PRINT(GET_PLAYER_NAME(playerVictim)) NET_NL()
							
							// Open up chat
							MPGlobals.g_KillStripVoiceData.killStripVoiceTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), 6000)
							MPGlobals.g_KillStripVoiceData.bVoiceTimerActive = TRUE
							MPGlobals.g_KillStripVoiceData.voicePlayerID = playerVictim
							NETWORK_OVERRIDE_CHAT_RESTRICTIONS(playerVictim, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF	
	
ENDPROC


FUNC BOOL SHOW_KILLER_OVERHEAD(PLAYER_INDEX playerID)

	IF MPGlobals.g_KillStripVoiceData.bVoiceTimerActive
		IF MPGlobals.g_KillStripVoiceData.voicePlayerID = playerID
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Clean up all the Kill Strip
PROC CLEANUP_ALL_KILL_STRIP()
	CLEAR_KILL_STRIP_DEATH_EFFECTS() 
	IF HAS_KILL_STRIP_BEEN_SHOWN()
		CLEANUP_KILL_STRIP()
	ENDIF
	CLEAR_ALL_BIG_MESSAGES_OF_TYPE(BIG_MESSAGE_WASTED)
	PRINTLN(" CLEANUP_ALL_KILL_STRIP - DONE ")
ENDPROC
	
#IF IS_DEBUG_BUILD
PROC UPDATE_KILL_STRIP_WIDGETS()
	
	IF bDebugKillStrip
		IF MPGlobals.g_KillStrip.iState = STATE_KILL_STRIP_NULL
			INITIALISE_KILL_STRIP()	
		ENDIF
		
		IF MAINTAIN_KILL_STRIP()
			CLEANUP_KILL_STRIP()
			
			bDebugKillStrip = FALSE
		ENDIF
	ENDIF
	
	IF bDisplayKillStreak
		
		PEDHEADSHOT_ID PlayerHeadshot = Get_HeadshotID_For_Player(PLAYER_ID())
		TEXT_LABEL_63 strPlayerHeadshotTxd = GET_PEDHEADSHOT_TXD_STRING(PlayerHeadshot)
		
		PRINTLN("UPDATE_KILL_STRIP_WIDGETS - Player 1 - Score: ", iKillStreakScoreOne, " Colour: ", HUD_COLOUR_AS_STRING(INT_TO_ENUM(HUD_COLOURS, iHUDColourOne)))
		PRINTLN("UPDATE_KILL_STRIP_WIDGETS - Player 2 - Score: ", iKillStreakScoreTwo, " Colour: ", HUD_COLOUR_AS_STRING(INT_TO_ENUM(HUD_COLOURS, iHUDColourTwo)))
		
		BEGIN_TEXT_COMMAND_THEFEED_POST("")
		END_TEXT_COMMAND_THEFEED_POST_VERSUS_TU(strPlayerHeadshotTxd, strPlayerHeadshotTxd, iKillStreakScoreOne, strPlayerHeadshotTxd, strPlayerHeadshotTxd, iKillStreakScoreTwo, INT_TO_ENUM(HUD_COLOURS, iHUDColourOne), INT_TO_ENUM(HUD_COLOURS, iHUDColourTwo))
		
		bDisplayKillStreak = FALSE
	ENDIF
	
ENDPROC
#ENDIF

