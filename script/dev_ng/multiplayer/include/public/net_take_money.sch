//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_TAKE_MONEY.sch																			//
// Description: Controls the Put Money in Bag mini-game														//
// Written by:  Ryan Baker																					//
// Date: /12/2011																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "help_at_location.sch"
USING "shared_hud_displays.sch"
USING "context_control_public.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"

// CnC Headers

USING "net_hud_activating.sch"

//Constants
CONST_INT TAKE_MONEY_SPEED_MAX		1000//500
CONST_INT TAKE_MONEY_SPEED_MIN		500//400
CONST_INT CASH_TAKEN_INCREMENT		1

//Anim Types
CONST_INT TM_ANIM_SET_CROUCHED		0
CONST_INT TM_ANIM_SET_STAND			1
CONST_INT TM_ANIM_SET_OPEN_CRATE	2

//Take Money BitSet
CONST_INT TM_BS_IS_PLAYER_IN_POSITION			0
CONST_INT TM_BS_IS_PLAYER_TAKING_MONEY			1
CONST_INT TM_BS_HAS_PLAYER_PASSED_TAKE_MONEY	2
CONST_INT TM_BS_HAS_PLAYER_FAILED_TAKE_MONEY	3
CONST_INT TM_BS_HAS_PLAYER_QUIT_TAKE_MONEY		4
CONST_INT TM_BS_STICK_PRESSED_LEFT				5
CONST_INT TM_BS_STICK_PRESSED_RIGHT				6
CONST_INT TM_BS_DIAL_IN_CORRECT_POSITION		7
CONST_INT TM_BS_FLOATING_HELP_DISPLAYED			8

//Take Money Variables
STRUCT TAKE_MONEY_STRUCT
	INT iBitSet
	INT iMoneyTakenCounter = 0
	INT iMoneyTakenCounterMax = 25
	INT iMoneyToTake = 5000
	INT iTimeToTake = 10000
	SCRIPT_TIMER iTakeMoneyTimer	// = 0
	INT iTakeMoneySpeed = TAKE_MONEY_SPEED_MAX
	VECTOR vMoneyCoord
	VECTOR vPlayerTakeMoneyCoord
	VECTOR vPlayerTakeMoneyDimensions
	FLOAT fPlayerTakeMoneyHeading
	VECTOR vFloatingHelpMin
	VECTOR vFloatingHelpMax
	FLOAT fFloatingHelpWidth
	BOOL bTakeMoneyDisabled
	INT iStartContextID = NEW_CONTEXT_INTENTION
	INT iEndContextID = NEW_CONTEXT_INTENTION
	INT iAnimSetToUse = TM_ANIM_SET_STAND
ENDSTRUCT

//Take Money Server Variables
STRUCT TAKE_MONEY_SERVER_STRUCT
	INT iMoneyTakenCounter = 0
	INT iParticipantTakingMoney = -1
ENDSTRUCT

//Take Money Client Variables
STRUCT TAKE_MONEY_CLIENT_STRUCT
	INT iMoneyTakenCounter = 0
	BOOL bInTakeMoneyGame = FALSE
	BOOL bDisplayBarRedWhenNotInMiniGame = FALSE
ENDSTRUCT


//PURPOSE: Returns the Help to use
FUNC STRING GET_TM_ANIM_HELP(TAKE_MONEY_STRUCT TakeMoneyData)
	SWITCH TakeMoneyData.iAnimSetToUse
		CASE TM_ANIM_SET_CROUCHED	RETURN "TM_HELP"
		CASE TM_ANIM_SET_STAND		RETURN "TM_HELP"
		CASE TM_ANIM_SET_OPEN_CRATE	RETURN "TM_CDHLP"
	ENDSWITCH
	
	RETURN "TM_HELP"
ENDFUNC

//PURPOSE: Returns the Text to use
FUNC STRING GET_TM_ANIM_TEXT(TAKE_MONEY_STRUCT TakeMoneyData)
	SWITCH TakeMoneyData.iAnimSetToUse
		CASE TM_ANIM_SET_CROUCHED	RETURN "TM_START"
		CASE TM_ANIM_SET_STAND		RETURN "TM_START"
		CASE TM_ANIM_SET_OPEN_CRATE	RETURN "TM_CDSTA"
	ENDSWITCH
	
	RETURN "TM_START"
ENDFUNC

//PURPOSE: Returns the Bar Text to use
FUNC STRING GET_TM_ANIM_BAR_TEXT(TAKE_MONEY_STRUCT TakeMoneyData)
	SWITCH TakeMoneyData.iAnimSetToUse
		CASE TM_ANIM_SET_CROUCHED	RETURN "TM_BAR"
		CASE TM_ANIM_SET_STAND		RETURN "TM_BAR"
		CASE TM_ANIM_SET_OPEN_CRATE	RETURN "TM_CDBAR"
	ENDSWITCH
	
	RETURN "TM_BAR"
ENDFUNC

//PURPOSE: Clears the Floating Help above the Take Money location
PROC CLEAR_TAKE_MONEY_FLOATING_HELP(TAKE_MONEY_STRUCT &TakeMoneyData)
	IF IS_BIT_SET(TakeMoneyData.iBitSet, TM_BS_FLOATING_HELP_DISPLAYED)
		IF IS_THIS_FLOATING_HELP_BEING_DISPLAYED(GET_TM_ANIM_TEXT(TakeMoneyData))
			CLEAR_THIS_FLOATING_HELP(GET_TM_ANIM_TEXT(TakeMoneyData))
		ENDIF
		CLEAR_BIT(TakeMoneyData.iBitSet, TM_BS_FLOATING_HELP_DISPLAYED)
	ENDIF
ENDPROC

//PURPOSE: Resets the Safe Cracking Data
PROC RESET_TAKE_MONEY(TAKE_MONEY_STRUCT &TakeMoneyData, TAKE_MONEY_CLIENT_STRUCT &TakeMoneyClientData)
	
	RESET_NET_TIMER(TakeMoneyData.iTakeMoneyTimer) //TakeMoneyData.iTakeMoneyTimer = GET_THE_NETWORK_TIMER()
	TakeMoneyData.iTakeMoneySpeed = TAKE_MONEY_SPEED_MAX
	CLEAR_TAKE_MONEY_FLOATING_HELP(TakeMoneyData)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_TM_ANIM_HELP(TakeMoneyData))
		CLEAR_HELP()
	ENDIF
	TakeMoneyData.iBitSet = -1
	TakeMoneyData.iBitSet = 0
	TakeMoneyClientData.bInTakeMoneyGame = FALSE
	NET_PRINT("   --->>>   TAKE MONEY - RESET_TAKE_MONEY - CALLED")NET_NL()
ENDPROC

//PURPOSE: Clears Help Text and Context Controller data
PROC CLEAR_TAKE_MONEY_PERSISTING_DATA(TAKE_MONEY_STRUCT &TakeMoneyData)
	CLEAR_TAKE_MONEY_FLOATING_HELP(TakeMoneyData)
	RELEASE_CONTEXT_INTENTION(TakeMoneyData.iStartContextID)
	RELEASE_CONTEXT_INTENTION(TakeMoneyData.iEndContextID)
ENDPROC

//PURPOSE: Returns the anim Dictionary to use
FUNC STRING GET_TM_ANIM_DICTIONARY(TAKE_MONEY_STRUCT TakeMoneyData)
	SWITCH TakeMoneyData.iAnimSetToUse
		CASE TM_ANIM_SET_CROUCHED	RETURN "MP_TAKE_MONEY_MG"
		CASE TM_ANIM_SET_STAND		RETURN "MP_TAKE_MONEY_MG"
		CASE TM_ANIM_SET_OPEN_CRATE	RETURN "MP_CRATE_DROP"
	ENDSWITCH
	
	RETURN "MP_TAKE_MONEY_MG"
ENDFUNC

//PURPOSE: Returns TRUE when the assets for the Take Money MiniGame assets are loaded
FUNC BOOL HAVE_TAKE_MONEY_ASSETS_LOADED(TAKE_MONEY_STRUCT &TakeMoneyData)
	REQUEST_ANIM_DICT(GET_TM_ANIM_DICTIONARY(TakeMoneyData))
	
	IF HAS_ANIM_DICT_LOADED(GET_TM_ANIM_DICTIONARY(TakeMoneyData))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Initialises the Safe Cracking data
PROC INITIALISE_TAKE_MONEY(TAKE_MONEY_STRUCT &TakeMoneyData)
	
	//Calcualte Money to Take
	//TakeMoneyData.iMoneyTakenCounterMax = (TakeMoneyData.iTimeToTake/TAKE_MONEY_SPEED_MAX)
	//NET_PRINT("   --->>>   TAKE MONEY - iMoneyTakenCounterMax = ")NET_PRINT_INT(TakeMoneyData.iMoneyTakenCounterMax)NET_NL()
	
	//Set Timer
	RESET_NET_TIMER(TakeMoneyData.iTakeMoneyTimer)
	START_NET_TIMER(TakeMoneyData.iTakeMoneyTimer)	//TakeMoneyData.iTakeMoneyTimer = GET_THE_NETWORK_TIMER()
	TakeMoneyData.iTakeMoneySpeed = TAKE_MONEY_SPEED_MAX
	
	IF ARE_VECTORS_ALMOST_EQUAL(TakeMoneyData.vPlayerTakeMoneyDimensions, <<0, 0, 0>>)
		TakeMoneyData.vPlayerTakeMoneyDimensions = <<0.75, 0.75, 1.5>>
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_TM_ANIM_HELP(TakeMoneyData))
		CLEAR_HELP()
	ENDIF
	NET_PRINT("   --->>>   TAKE MONEY - INITIALISE_TAKE_MONEY - CALLED")NET_NL()
ENDPROC

//PURPOSE: Returns the Intro anim to use
FUNC STRING GET_TM_ANIM_INTRO(TAKE_MONEY_STRUCT TakeMoneyData)
	SWITCH TakeMoneyData.iAnimSetToUse
		CASE TM_ANIM_SET_CROUCHED	RETURN "PUT_CASH_INTO_BAG_INTRO"
		CASE TM_ANIM_SET_STAND		RETURN "STAND_CASH_IN_BAG_INTRO"
		CASE TM_ANIM_SET_OPEN_CRATE	RETURN "MP_CRATE_DROP_INTRO"
	ENDSWITCH
	
	RETURN "PUT_CASH_INTO_BAG_LOOP"
ENDFUNC

//PURPOSE: Returns the Loop anim to use
FUNC STRING GET_TM_ANIM_LOOP(TAKE_MONEY_STRUCT TakeMoneyData)
	SWITCH TakeMoneyData.iAnimSetToUse
		CASE TM_ANIM_SET_CROUCHED	RETURN "PUT_CASH_INTO_BAG_LOOP"
		CASE TM_ANIM_SET_STAND		RETURN "STAND_CASH_IN_BAG_LOOP"
		CASE TM_ANIM_SET_OPEN_CRATE	RETURN "MP_CRATE_DROP_IDLE"
	ENDSWITCH
	
	RETURN "STAND_CASH_IN_BAG_LOOP"
ENDFUNC

//PURPOSE: Returns the Outro anim to use
FUNC STRING GET_TM_ANIM_OUTRO(TAKE_MONEY_STRUCT TakeMoneyData)
	SWITCH TakeMoneyData.iAnimSetToUse
		CASE TM_ANIM_SET_CROUCHED	RETURN ""
		CASE TM_ANIM_SET_STAND		RETURN ""
		CASE TM_ANIM_SET_OPEN_CRATE	RETURN "MP_CRATE_DROP_EXIT"
	ENDSWITCH
	
	RETURN ""
ENDFUNC

//PURPOSE: Returns the amount of money taken
FUNC INT GET_MONEY_TAKEN(TAKE_MONEY_STRUCT &TakeMoneyData)
	INT iCashTaken = ((TakeMoneyData.iMoneyToTake/TakeMoneyData.iMoneyTakenCounterMax) * TakeMoneyData.iMoneyTakenCounter)
	NET_PRINT("   --->>>   TAKE MONEY - iCashTaken = ")NET_PRINT_INT(iCashTaken)NET_NL()
	RETURN iCashTaken
ENDFUNC

//PURPOSE: Returns TRUE if the player is in Safe Cracking minigame
FUNC BOOL IS_PLAYER_TAKING_MONEY(TAKE_MONEY_STRUCT &TakeMoneyData)
	IF IS_BIT_SET(TakeMoneyData.iBitSet, TM_BS_IS_PLAYER_TAKING_MONEY)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns TRUE if the player has passed the Safe Cracking minigame
FUNC BOOL HAS_PLAYER_TAKEN_MONEY(TAKE_MONEY_STRUCT &TakeMoneyData)
	IF IS_BIT_SET(TakeMoneyData.iBitSet, TM_BS_HAS_PLAYER_PASSED_TAKE_MONEY)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Checks if another player is already performing Take Money
PROC TAKE_MONEY_DISABLED_CHECK(PLAYER_INDEX thisPlayer, TAKE_MONEY_STRUCT &TakeMoneyData, TAKE_MONEY_CLIENT_STRUCT TakeMoneyClientData)
	IF thisPlayer != PLAYER_ID()
		IF TakeMoneyClientData.bInTakeMoneyGame = TRUE
			TakeMoneyData.bTakeMoneyDisabled = TRUE
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Increases the Server Counter to match the Player COunter if necessary
PROC UPDATE_SERVER_MONEY_TAKEN_COUNTER(TAKE_MONEY_SERVER_STRUCT &TakeMoneyServerData, TAKE_MONEY_CLIENT_STRUCT TakeMoneyClientData)
	IF TakeMoneyClientData.iMoneyTakenCounter > TakeMoneyServerData.iMoneyTakenCounter
		TakeMoneyServerData.iMoneyTakenCounter = TakeMoneyClientData.iMoneyTakenCounter
		NET_PRINT("   --->>>   TAKE MONEY - TakeMoneyServerData.iMoneyTakenCounter INCREASED ")NET_PRINT_INT(TakeMoneyServerData.iMoneyTakenCounter)NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Returns TRUE if the Player is able to start Take Money
FUNC BOOL CAN_PLAYER_START_TAKE_MONEY(TAKE_MONEY_STRUCT &TakeMoneyData)
	//IF IS_POSITION_OCCUPIED(TakeMoneyData.vPlayerTakeMoneyCoord, 0.75, FALSE, FALSE, TRUE, FALSE, FALSE, PLAYER_PED_ID())
	IF IS_PED_RAGDOLL(PLAYER_PED_ID())
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	OR NOT HAS_ANIM_DICT_LOADED(GET_TM_ANIM_DICTIONARY(TakeMoneyData))
	OR IS_PAUSE_MENU_ACTIVE()
	OR IS_PED_IN_COVER(PLAYER_PED_ID())
	OR TakeMoneyData.bTakeMoneyDisabled = TRUE
	OR NOT IS_PLAYER_FREE_FOR_AMBIENT_TASK(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Returns True if the player should leave the Safe Cracking
FUNC BOOL HAS_PLAYER_LEFT_TAKE_MONEY(TAKE_MONEY_STRUCT &TakeMoneyData)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SELECT_WEAPON)
	
	REGISTER_CONTEXT_INTENTION(TakeMoneyData.iEndContextID, CP_HIGH_PRIORITY, "", TRUE)
	
	//IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), TakeMoneyData.vPlayerTakeMoneyCoord, TakeMoneyData.vPlayerTakeMoneyDimensions, FALSE, TRUE, TM_ON_FOOT)
	IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), TakeMoneyData.vFloatingHelpMin, TakeMoneyData.vFloatingHelpMax, TakeMoneyData.fFloatingHelpWidth, FALSE, TRUE, TM_ON_FOOT)
	//OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)	//(PAD1, LEFTSHOULDER1)
	OR HAS_CONTEXT_BUTTON_TRIGGERED(TakeMoneyData.iEndContextID)
	OR IS_PED_RAGDOLL(PLAYER_PED_ID())
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	OR IS_PAUSE_MENU_ACTIVE()
	OR IS_PED_IN_COVER(PLAYER_PED_ID())
	OR TakeMoneyData.bTakeMoneyDisabled = TRUE
		RELEASE_CONTEXT_INTENTION(TakeMoneyData.iEndContextID)
		NET_PRINT("   --->>>   TAKE MONEY - HAS_PLAYER_LEFT_TAKE_MONEY - TRUE")NET_NL()
		RETURN TRUE	
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Controls the unlocking of the current Lock Stage (also performs the pas vibration)
PROC PROCESS_TAKE_MONEY_PUT_IN_BAG(TAKE_MONEY_STRUCT &TakeMoneyData)
	IF TakeMoneyData.iMoneyTakenCounter < TakeMoneyData.iMoneyTakenCounterMax
		IF HAS_NET_TIMER_EXPIRED(TakeMoneyData.iTakeMoneyTimer, TakeMoneyData.iTakeMoneySpeed)	//IF (TakeMoneyData.iTakeMoneyTimer + TakeMoneyData.iTakeMoneySpeed) < GET_THE_NETWORK_TIMER()
			RESET_NET_TIMER(TakeMoneyData.iTakeMoneyTimer)
			START_NET_TIMER(TakeMoneyData.iTakeMoneyTimer)	//TakeMoneyData.iTakeMoneyTimer = GET_THE_NETWORK_TIMER()
			TakeMoneyData.iTakeMoneySpeed = TAKE_MONEY_SPEED_MAX
			TakeMoneyData.iMoneyTakenCounter++
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 100, 100)
			SET_ENTITY_ANIM_SPEED(PLAYER_PED_ID(), GET_TM_ANIM_DICTIONARY(TakeMoneyData), GET_TM_ANIM_LOOP(TakeMoneyData), 1.00)
			NET_PRINT("   --->>>   TAKE MONEY - MONEY PUT IN BAG")NET_NL()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Processes the player using the Safe Cracking Controls
PROC PROCESS_TAKE_MONEY_CONTROLS(TAKE_MONEY_STRUCT &TakeMoneyData)
	IF TakeMoneyData.iMoneyTakenCounter < TakeMoneyData.iMoneyTakenCounterMax
		//Display help text
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_TM_ANIM_HELP(TakeMoneyData))
			PRINT_HELP_FOREVER(GET_TM_ANIM_HELP(TakeMoneyData))	//~p~Take Money~s~ ~n~Repeatedly tap ~PAD_A~ to speed up putting the money into the bag.~n~Press ~PAD_LB~ to stop.
		ENDIF
		
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), GET_TM_ANIM_DICTIONARY(TakeMoneyData), GET_TM_ANIM_LOOP(TakeMoneyData))
			//Player pressed X to speed up
			IF TakeMoneyData.iTakeMoneySpeed != TAKE_MONEY_SPEED_MIN
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)	//(PAD1, CROSS)
					TakeMoneyData.iTakeMoneySpeed = TAKE_MONEY_SPEED_MIN
					SET_ENTITY_ANIM_SPEED(PLAYER_PED_ID(), GET_TM_ANIM_DICTIONARY(TakeMoneyData), GET_TM_ANIM_LOOP(TakeMoneyData), 1.25)
					NET_PRINT("   --->>>   TAKE MONEY - TAKE MONEY SPED UP")NET_NL()
				ENDIF
			ENDIF
			
			//Check if player has put some money in the bag
			PROCESS_TAKE_MONEY_PUT_IN_BAG(TakeMoneyData)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Processes the Safe Cracking minigame
PROC RUN_TAKE_MONEY_MINIGAME(TAKE_MONEY_STRUCT &TakeMoneyData)
	IF IS_BIT_SET(TakeMoneyData.iBitSet, TM_BS_IS_PLAYER_TAKING_MONEY)
		IF NOT IS_BIT_SET(TakeMoneyData.iBitSet, TM_BS_IS_PLAYER_IN_POSITION)
			
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			
			//Give the player a sequence to get them into position for hacking
			SEQUENCE_INDEX thisSequence
			OPEN_SEQUENCE_TASK(thisSequence)
				TASK_TURN_PED_TO_FACE_COORD(NULL, TakeMoneyData.vMoneyCoord)
				TASK_PLAY_ANIM(NULL, GET_TM_ANIM_DICTIONARY(TakeMoneyData), GET_TM_ANIM_INTRO(TakeMoneyData), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HIDE_WEAPON)
				TASK_PLAY_ANIM(NULL, GET_TM_ANIM_DICTIONARY(TakeMoneyData), GET_TM_ANIM_LOOP(TakeMoneyData), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_HIDE_WEAPON)
			CLOSE_SEQUENCE_TASK(thisSequence)
			TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), thisSequence)
			
			INITIALISE_TAKE_MONEY(TakeMoneyData)
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_ALLOW_PLAYER_DAMAGE|SPC_LEAVE_CAMERA_CONTROL_ON|SPC_PREVENT_EVERYBODY_BACKOFF)
			
			SET_BIT(TakeMoneyData.iBitSet, TM_BS_IS_PLAYER_IN_POSITION)
			NET_PRINT("   --->>>   TAKE MONEY - PLAYER GIVEN GET IN POSITION SEQUENCE")NET_NL()
		ELSE
			
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_UseKinematicPhysics, TRUE)
			
			PROCESS_TAKE_MONEY_CONTROLS(TakeMoneyData)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Processes the Take Money minigame
PROC PROCESS_PLAYER_TAKE_MONEY(TAKE_MONEY_STRUCT &TakeMoneyData, TAKE_MONEY_SERVER_STRUCT TakeMoneyServerData, TAKE_MONEY_CLIENT_STRUCT &TakeMoneyClientData)	//INT iServerMoneyTakenCounter, INT &iPlayerMoneyTakenCounter)
	
	//Update Player Counter to Match Server if Server is Higher
	IF TakeMoneyServerData.iMoneyTakenCounter > TakeMoneyData.iMoneyTakenCounter
		TakeMoneyData.iMoneyTakenCounter = TakeMoneyServerData.iMoneyTakenCounter
		NET_PRINT("   --->>>   TAKE MONEY - TakeMoneyData.iMoneyTakenCounter INCREASED TO MATCH TakeMoneyServerData.iMoneyTakenCounter ")NET_PRINT_INT(TakeMoneyData.iMoneyTakenCounter)NET_NL()
	ENDIF
	
	//Draw Meter
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), TakeMoneyData.vPlayerTakeMoneyCoord, <<100, 100, 100>>)
		IF TakeMoneyClientData.bDisplayBarRedWhenNotInMiniGame = FALSE
			DRAW_GENERIC_METER(TakeMoneyServerData.iMoneyTakenCounter, TakeMoneyData.iMoneyTakenCounterMax, GET_TM_ANIM_BAR_TEXT(TakeMoneyData), GET_TEAM_HUD_COLOUR(GET_PLAYER_TEAM(PLAYER_ID())))	//~s~Money Taken
		ELSE
			IF TakeMoneyData.bTakeMoneyDisabled = FALSE
			OR IS_BIT_SET(TakeMoneyData.iBitSet, TM_BS_IS_PLAYER_TAKING_MONEY)
				DRAW_GENERIC_METER(TakeMoneyServerData.iMoneyTakenCounter, TakeMoneyData.iMoneyTakenCounterMax, GET_TM_ANIM_BAR_TEXT(TakeMoneyData), GET_TEAM_HUD_COLOUR(GET_PLAYER_TEAM(PLAYER_ID())))	//~s~Money Taken
			ELSE
				DRAW_GENERIC_METER(TakeMoneyServerData.iMoneyTakenCounter, TakeMoneyData.iMoneyTakenCounterMax, GET_TM_ANIM_BAR_TEXT(TakeMoneyData), HUD_COLOUR_RED)	//~s~Money Taken
			ENDIF
		ENDIF
	ENDIF
	
	//Check if Player is near Safe
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), TakeMoneyData.vFloatingHelpMin, TakeMoneyData.vFloatingHelpMax, TakeMoneyData.fFloatingHelpWidth, FALSE, TRUE, TM_ON_FOOT)
	//AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), TakeMoneyData.vPlayerTakeMoneyCoord, <<0.50, 0.50, 1.0>>, FALSE, TRUE, TM_ON_FOOT)
		REQUEST_ANIM_DICT(GET_TM_ANIM_DICTIONARY(TakeMoneyData))
		
		//Player is not yet Taking Money
		IF NOT IS_BIT_SET(TakeMoneyData.iBitSet, TM_BS_IS_PLAYER_TAKING_MONEY)
			IF CAN_PLAYER_START_TAKE_MONEY(TakeMoneyData)
				IF NOT IS_THIS_FLOATING_HELP_BEING_DISPLAYED(GET_TM_ANIM_TEXT(TakeMoneyData))
					HELP_AT_LOCATION(GET_TM_ANIM_TEXT(TakeMoneyData), TakeMoneyData.vMoneyCoord, HELP_TEXT_SOUTH, -1, FLOATING_HELP_MISSION_SLOT)	//~s~Press ~PAD_LB~ to start taking the money.
					SET_BIT(TakeMoneyData.iBitSet, TM_BS_FLOATING_HELP_DISPLAYED)
				ENDIF
				
				//Check if Player is at the Money
				//IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), TakeMoneyData.vPlayerTakeMoneyCoord, TakeMoneyData.vPlayerTakeMoneyDimensions, FALSE, TRUE, TM_ON_FOOT)
					
					REGISTER_CONTEXT_INTENTION(TakeMoneyData.iStartContextID, CP_HIGH_PRIORITY, "", TRUE)
					
					IF HAS_CONTEXT_BUTTON_TRIGGERED(TakeMoneyData.iStartContextID)
						CLEAR_TAKE_MONEY_FLOATING_HELP(TakeMoneyData)
						RELEASE_CONTEXT_INTENTION(TakeMoneyData.iStartContextID)
						TakeMoneyClientData.bInTakeMoneyGame = TRUE
						SET_BIT(TakeMoneyData.iBitSet, TM_BS_IS_PLAYER_TAKING_MONEY)
						NET_PRINT("   --->>>   TAKE MONEY - TM_BS_IS_PLAYER_TAKING_MONEY - SET")NET_NL()
					ENDIF
				//ENDIF
			ELSE
				CLEAR_TAKE_MONEY_FLOATING_HELP(TakeMoneyData)
				RELEASE_CONTEXT_INTENTION(TakeMoneyData.iStartContextID)
				RELEASE_CONTEXT_INTENTION(TakeMoneyData.iEndContextID)
			ENDIF
			
		//Player is Taking Money
		ELSE
			
			IF TakeMoneyServerData.iMoneyTakenCounter < TakeMoneyData.iMoneyTakenCounterMax	//TakeMoneyData.iMoneyTakenCounter < TakeMoneyData.iMoneyTakenCounterMax
				//Check that player hasn't left the Money Area
				IF HAS_PLAYER_LEFT_TAKE_MONEY(TakeMoneyData)
					
					IF NOT ARE_STRINGS_EQUAL(GET_TM_ANIM_LOOP(TakeMoneyData), "")
					AND NOT ARE_STRINGS_EQUAL(GET_TM_ANIM_OUTRO(TakeMoneyData), "")
						IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), GET_TM_ANIM_DICTIONARY(TakeMoneyData), GET_TM_ANIM_LOOP(TakeMoneyData))
							TASK_PLAY_ANIM(PLAYER_PED_ID(), GET_TM_ANIM_DICTIONARY(TakeMoneyData), GET_TM_ANIM_OUTRO(TakeMoneyData), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HIDE_WEAPON)
						ELSE
							CLEAR_PED_TASKS(PLAYER_PED_ID())
						ENDIF
					ELSE
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
					RESET_TAKE_MONEY(TakeMoneyData, TakeMoneyClientData)
				ENDIF
				
				RUN_TAKE_MONEY_MINIGAME(TakeMoneyData)
			ELSE
				IF NOT IS_BIT_SET(TakeMoneyData.iBitSet, TM_BS_HAS_PLAYER_PASSED_TAKE_MONEY)
					
					IF NOT ARE_STRINGS_EQUAL(GET_TM_ANIM_INTRO(TakeMoneyData), "")
						TASK_PLAY_ANIM(PLAYER_PED_ID(), GET_TM_ANIM_DICTIONARY(TakeMoneyData), GET_TM_ANIM_OUTRO(TakeMoneyData), NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HIDE_WEAPON)
					ELSE
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_TM_ANIM_HELP(TakeMoneyData))
						CLEAR_HELP()
					ENDIF
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
					SET_BIT(TakeMoneyData.iBitSet, TM_BS_HAS_PLAYER_PASSED_TAKE_MONEY)
					NET_PRINT("   --->>>   TAKE MONEY - TM_BS_HAS_PLAYER_PASSED_TAKE_MONEY - SET")NET_NL()
				ENDIF
			ENDIF
		ENDIF
		
		//Display the Money Taken Meter
		//IF TakeMoneyServerData.iMoneyTakenCounter <= TakeMoneyData.iMoneyTakenCounterMax		//TakeMoneyData.iMoneyTakenCounter 
		/*IF TakeMoneyClientData.bDisplayBarRedWhenNotInMiniGame = FALSE
			DRAW_GENERIC_METER(TakeMoneyServerData.iMoneyTakenCounter, TakeMoneyData.iMoneyTakenCounterMax, GET_TM_ANIM_BAR_TEXT(TakeMoneyData), GET_TEAM_HUD_COLOUR(GET_PLAYER_TEAM(PLAYER_ID())))	//~s~Money Taken
		ELSE
			IF TakeMoneyData.bTakeMoneyDisabled = FALSE
			OR IS_BIT_SET(TakeMoneyData.iBitSet, TM_BS_IS_PLAYER_TAKING_MONEY)
				DRAW_GENERIC_METER(TakeMoneyServerData.iMoneyTakenCounter, TakeMoneyData.iMoneyTakenCounterMax, GET_TM_ANIM_BAR_TEXT(TakeMoneyData), GET_TEAM_HUD_COLOUR(GET_PLAYER_TEAM(PLAYER_ID())))	//~s~Money Taken
			ELSE
				DRAW_GENERIC_METER(TakeMoneyServerData.iMoneyTakenCounter, TakeMoneyData.iMoneyTakenCounterMax, GET_TM_ANIM_BAR_TEXT(TakeMoneyData), HUD_COLOUR_RED)	//~s~Money Taken
			ENDIF
		ENDIF*/
	
	ELSE
		CLEAR_TAKE_MONEY_FLOATING_HELP(TakeMoneyData)
		RELEASE_CONTEXT_INTENTION(TakeMoneyData.iStartContextID)
		RELEASE_CONTEXT_INTENTION(TakeMoneyData.iEndContextID)
		IF IS_BIT_SET(TakeMoneyData.iBitSet, TM_BS_IS_PLAYER_TAKING_MONEY)
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
			RESET_TAKE_MONEY(TakeMoneyData, TakeMoneyClientData)
		ENDIF
	ENDIF
	
	//Update the Players Money Counter
	IF TakeMoneyData.iMoneyTakenCounter > TakeMoneyClientData.iMoneyTakenCounter
		TakeMoneyClientData.iMoneyTakenCounter = TakeMoneyData.iMoneyTakenCounter
		NET_PRINT("   --->>>   TAKE MONEY - TakeMoneyClientData.iMoneyTakenCounter INCREASED TO MATCH TakeMoneyData.iMoneyTakenCounter ")NET_PRINT_INT(TakeMoneyClientData.iMoneyTakenCounter)NET_NL()
	ENDIF
	
ENDPROC
