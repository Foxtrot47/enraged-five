USING "globals.sch"

TYPEDEF PROC THREAD_UPDATE_HANDLER()
PROC THREAD_UPDATE_NULL()
ENDPROC

ENUM THREAD_UPDATE_STATUS
	THREAD_UPDATE_EVERYFRAME,
	THREAD_UPDATE_STAGGERED,
	THREAD_UPDATE_LONG
ENDENUM
#IF IS_DEBUG_BUILD
FUNC STRING GET_THREAD_STATUS_PRINT(THREAD_UPDATE_STATUS eStatus)
	SWITCH eStatus
		CASE THREAD_UPDATE_EVERYFRAME	RETURN "THREAD_UPDATE_EVERYFRAME"
		CASE THREAD_UPDATE_STAGGERED	RETURN "THREAD_UPDATE_STAGGERED"
		CASE THREAD_UPDATE_LONG			RETURN "THREAD_UPDATE_LONG"
	ENDSWITCH
	RETURN ""
ENDFUNC
#ENDIF

//Details of the thread we want to update
STRUCT SCRIPT_THREAD
	THREAD_UPDATE_HANDLER 	fpThreadUpdate
	THREAD_UPDATE_STATUS 	eStatus
	BOOL 					bThreadLocked
ENDSTRUCT

CONST_INT ciSTAGGER_PROCESS_DEFAULT 1

STRUCT SCRIPT_THREAD_DATA
	//Holds all the functions we need to update
	SCRIPT_THREAD sUpdateThread[MAX_UPDATE_THREAD]
	//Array of staggered functions
	UPDATE_THREAD updateThreadStaggered[MAX_UPDATE_THREAD]
	BOOL bRemovedFromStaggered[MAX_UPDATE_THREAD]
	BOOL bNeedToRemoveStaggeredThread
	//Array of long functions
	UPDATE_THREAD updateThreadLong[MAX_UPDATE_THREAD]
	BOOL bDontUpDateLong
	//Array of everyframe functions
	UPDATE_THREAD updateThreadEveryFrame[MAX_UPDATE_THREAD]
	BOOL bRemovedFromEveryFrame[MAX_UPDATE_THREAD]
	BOOL bNeedToRemoveEveryFrameThread
	
	//Count of staggered functions and current update
	INT iUpdateThreadStaggeredCount
	INT iCurrentThreadUpdateStaggered
	INT iStaggerProcessCount
	INT iStaggerLongTimer
	//Count of long functions and current update
	INT iUpdateThreadLongCount
	INT iCurrentThreadUpdateLong
	
	//Count of the everyframe functions
	INT iUpdateThreadEveryFrameCount
	
	//Long time
	SCRIPT_TIMER stLongUpdate
	#IF IS_DEBUG_BUILD
	STRING sThreadName
	BOOL bDumpStatusToLogs
	#ENDIF
ENDSTRUCT


//Initilise thread data
PROC INT_SCRIPT_THREAD_DATA(	SCRIPT_THREAD_DATA &sScriptThread, 
								INT iStaggerProcessCount = ciSTAGGER_PROCESS_DEFAULT, 
								INT iStaggerLongTimer = ciSTAGGER_PROCESS_DEFAULT 
								#IF IS_DEBUG_BUILD , STRING sThreadName = NULL, INT iBgArray = 0 #ENDIF )
								
	sScriptThread.iStaggerProcessCount = iStaggerProcessCount
	sScriptThread.iStaggerLongTimer = iStaggerLongTimer
	#IF IS_DEBUG_BUILD
	sScriptThread.sThreadName = sThreadName
	sScriptThread.bDumpStatusToLogs = GET_COMMANDLINE_PARAM_EXISTS("sc_DumpUpdateThreadStatus")
	
	IF iBgArray != 0
	AND ENUM_TO_INT(MAX_UPDATE_THREAD)/32 >= iBgArray
		PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] INT_SCRIPT_THREAD_DATA - ENUM_TO_INT(MAX_UPDATE_THREAD)/32 >= iBgArray - iBgArray = ", iBgArray, " - Needed = ", ENUM_TO_INT(MAX_UPDATE_THREAD)/32)
		ASSERTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] INT_SCRIPT_THREAD_DATA - ENUM_TO_INT(MAX_UPDATE_THREAD)/32 >= iBgArray")
	ENDIF
	
	#ENDIF
	PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] INT_SCRIPT_THREAD_DATA - iStaggerProcessCount = ", sScriptThread.iStaggerProcessCount, " - iStaggerLongTimer = ", sScriptThread.iStaggerLongTimer)
	
	INT iLoop
	REPEAT MAX_UPDATE_THREAD iLoop
		sScriptThread.updateThreadStaggered[iLoop] = UPDATE_THREAD_INVALID
		sScriptThread.updateThreadLong[iLoop] = UPDATE_THREAD_INVALID		
		sScriptThread.updateThreadEveryFrame[iLoop] = UPDATE_THREAD_INVALID
	ENDREPEAT
	
ENDPROC
#IF IS_DEBUG_BUILD
PROC DUMP_UPDATE_THREAD_STATUS(SCRIPT_THREAD_DATA &sScriptThread)
	PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,	"] DUMP - EVERYFRAME=", sScriptThread.iUpdateThreadEveryFrameCount, 
															" STAGGERED=", sScriptThread.iUpdateThreadStaggeredCount 
															," LONG=", sScriptThread.iUpdateThreadLongCount, 
															" Total=", sScriptThread.iUpdateThreadEveryFrameCount + sScriptThread.iUpdateThreadStaggeredCount + sScriptThread.iUpdateThreadLongCount)
	INT iLoop
	REPEAT sScriptThread.iUpdateThreadEveryFrameCount iLoop
		PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] EVERYFRAME - ", GET_THREAD_NAME(sScriptThread.updateThreadEveryFrame[iLoop]))
	ENDREPEAT
	REPEAT sScriptThread.iUpdateThreadStaggeredCount iLoop
		PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] STAGGERED  - ", GET_THREAD_NAME(sScriptThread.updateThreadStaggered[iLoop]))
	ENDREPEAT
	REPEAT sScriptThread.iUpdateThreadLongCount iLoop
		PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] LONG       - ", GET_THREAD_NAME(sScriptThread.updateThreadLong[iLoop]))
	ENDREPEAT
ENDPROC
#ENDIF

//Add to staggered
PROC ADD_UPDATE_THREAD_TO_STAGGERED(SCRIPT_THREAD_DATA &sScriptThread, UPDATE_THREAD eThread)
	sScriptThread.sUpdateThread[eThread].eStatus = THREAD_UPDATE_STAGGERED
	sScriptThread.updateThreadStaggered[sScriptThread.iUpdateThreadStaggeredCount] = eThread
	sScriptThread.iUpdateThreadStaggeredCount++
	PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] ADD_UPDATE_THREAD_TO_STAGGERED - ", GET_THREAD_NAME(eThread), " - iUpdateThreadStaggeredCount = ", sScriptThread.iUpdateThreadStaggeredCount)
ENDPROC

//Add to long staggered 
PROC ADD_UPDATE_THREAD_TO_LONG(SCRIPT_THREAD_DATA &sScriptThread, UPDATE_THREAD eThread)
	sScriptThread.sUpdateThread[eThread].eStatus = THREAD_UPDATE_LONG
	sScriptThread.updateThreadLong[sScriptThread.iUpdateThreadLongCount] = eThread
	sScriptThread.iUpdateThreadLongCount++
	PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] ADD_UPDATE_THREAD_TO_LONG - ", GET_THREAD_NAME(eThread), " - iUpdateThreadLongCount = ", sScriptThread.iUpdateThreadLongCount)
ENDPROC

//Add to every frame
PROC ADD_UPDATE_THREAD_TO_EVERYFRAME(SCRIPT_THREAD_DATA &sScriptThread, UPDATE_THREAD eThread)
	sScriptThread.sUpdateThread[eThread].eStatus = THREAD_UPDATE_EVERYFRAME
	sScriptThread.updateThreadEveryFrame[sScriptThread.iUpdateThreadEveryFrameCount] = eThread
	sScriptThread.iUpdateThreadEveryFrameCount++
	PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] ADD_UPDATE_THREAD_TO_EVERYFRAME - ", GET_THREAD_NAME(eThread), " - iUpdateThreadEveryFrameCount = ", sScriptThread.iUpdateThreadEveryFrameCount)
ENDPROC

FUNC THREAD_UPDATE_STATUS GET_THREAD_UPDATE_STATUS(SCRIPT_THREAD_DATA &sScriptThread, UPDATE_THREAD eThread)
	RETURN sScriptThread.sUpdateThread[eThread].eStatus
ENDFUNC

FUNC BOOL IS_THREAD_UPDATE_STATUS_LOCKED(SCRIPT_THREAD_DATA &sScriptThread, UPDATE_THREAD eThread)
	RETURN sScriptThread.sUpdateThread[eThread].bThreadLocked
ENDFUNC

PROC SET_THREAD_UPDATE_STATUS_LOCKED(SCRIPT_THREAD_DATA &sScriptThread, UPDATE_THREAD eThread)
	sScriptThread.sUpdateThread[eThread].bThreadLocked = TRUE
	PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] SET_THREAD_UPDATE_STATUS_LOCKED - ", GET_THREAD_NAME(eThread))
ENDPROC

//Register a thread
PROC REGISTER_UPDATE_THREAD(	THREAD_UPDATE_HANDLER 	fpThreadUpdate,
								UPDATE_THREAD			eThread,
								SCRIPT_THREAD_DATA 		&sScriptThread,
								THREAD_UPDATE_STATUS	eStatus)

	PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] REGISTER_UPDATE_THREAD - ", GET_THREAD_NAME(eThread), " eStatus = ", GET_THREAD_STATUS_PRINT(eStatus))
	
	sScriptThread.sUpdateThread[eThread].fpThreadUpdate = fpThreadUpdate
	
	SWITCH eStatus
		CASE THREAD_UPDATE_EVERYFRAME
			ADD_UPDATE_THREAD_TO_EVERYFRAME(sScriptThread, eThread)
		BREAK
		CASE THREAD_UPDATE_STAGGERED
			ADD_UPDATE_THREAD_TO_STAGGERED(sScriptThread, eThread)
		BREAK
		CASE THREAD_UPDATE_LONG
			ADD_UPDATE_THREAD_TO_LONG(sScriptThread, eThread)
		BREAK
	  ENDSWITCH

ENDPROC


FUNC BOOL REMOVE_UPDATE_THREAD_FROM_STAGGERED(SCRIPT_THREAD_DATA &sScriptThread, UPDATE_THREAD eThread)	
	INT iThread
	REPEAT MAX_UPDATE_THREAD iThread
		IF sScriptThread.updateThreadStaggered[iThread] = eThread
			//Flag the thread to be removed at the end of the frame so we don't mess up the update loop
			sScriptThread.bRemovedFromSTAGGERED[eThread] = TRUE
			sScriptThread.bNeedToRemoveStaggeredThread = TRUE
			RETURN TRUE
		ENDIF
	ENDREPEAT
	PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] REMOVE_UPDATE_THREAD_FROM_STAGGERED - ", GET_THREAD_NAME(eThread), " Not Found")	
	RETURN FALSE
ENDFUNC

FUNC BOOL REMOVE_UPDATE_THREAD_FROM_LONG(SCRIPT_THREAD_DATA &sScriptThread, UPDATE_THREAD eThread)	
	INT iThread
	INT iLoop2
	REPEAT MAX_UPDATE_THREAD iThread
		IF sScriptThread.updateThreadLong[iThread] = eThread
			sScriptThread.iUpdateThreadLongCount--
			IF sScriptThread.iUpdateThreadLongCount < 0 
				sScriptThread.iUpdateThreadLongCount = 0
			ENDIF
			PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] REMOVE_UPDATE_THREAD_FROM_LONG - ", GET_THREAD_NAME(eThread), " - iUpdateThreadLongCount = ", sScriptThread.iUpdateThreadLongCount)	
			sScriptThread.bDontUpDateLong= TRUE
			FOR iLoop2 = iThread TO ENUM_TO_INT(MAX_UPDATE_THREAD)-2 
				sScriptThread.updateThreadLong[iLoop2] = sScriptThread.updateThreadLong[iLoop2+1]
			ENDFOR
			RETURN TRUE
		ENDIF
	ENDREPEAT
	PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] REMOVE_UPDATE_THREAD_FROM_LONG - ", GET_THREAD_NAME(eThread), " Not Found")	
	RETURN FALSE
ENDFUNC 

FUNC BOOL REMOVE_UPDATE_THREAD_FROM_EVERYFRAME(SCRIPT_THREAD_DATA &sScriptThread, UPDATE_THREAD eThread)	
	
	INT iLoop
	REPEAT MAX_UPDATE_THREAD iLoop
		IF sScriptThread.updateThreadEveryFrame[iLoop] = eThread
			PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] REMOVE_UPDATE_THREAD_FROM_EVERYFRAME - ", GET_THREAD_NAME(eThread))	
			//Flag the thread to be removed at the end of the frame so we don't mess up the update loop
			sScriptThread.bRemovedFromEveryFrame[eThread] = TRUE
			sScriptThread.bNeedToRemoveEveryFrameThread = TRUE
			RETURN TRUE
		ENDIF
	ENDREPEAT

	PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] REMOVE_UPDATE_THREAD_FROM_EVERYFRAME - ", GET_THREAD_NAME(eThread), " Not Found")	
	RETURN FALSE
ENDFUNC

//Move thread to staggered
PROC MOVE_UPDATE_THREAD_TO_STAGGERED(SCRIPT_THREAD_DATA &sScriptThread, UPDATE_THREAD eThread)

	IF IS_THREAD_UPDATE_STATUS_LOCKED(sScriptThread, eThread)
		EXIT
	ENDIF
	
	IF sScriptThread.iUpdateThreadStaggeredCount >= ENUM_TO_INT(MAX_UPDATE_THREAD)
		PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] MOVE_UPDATE_THREAD_TO_STAGGERED - ", GET_THREAD_NAME(eThread), " failed - sScriptThread.iUpdateThreadStaggeredCount >= ENUM_TO_INT(MAX_UPDATE_THREAD) ")			
		ASSERTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] MOVE_UPDATE_THREAD_TO_STAGGERED - ", GET_THREAD_NAME(eThread), " failed - sScriptThread.iUpdateThreadStaggeredCount >= ENUM_TO_INT(MAX_UPDATE_THREAD) ")			
		EXIT
	ENDIF
	
	SWITCH sScriptThread.sUpdateThread[eThread].eStatus
		CASE THREAD_UPDATE_EVERYFRAME
			IF REMOVE_UPDATE_THREAD_FROM_EVERYFRAME(sScriptThread, eThread)	
				PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] MOVE_UPDATE_THREAD_TO_STAGGERED - ", GET_THREAD_NAME(eThread))			
				ADD_UPDATE_THREAD_TO_STAGGERED(sScriptThread, eThread)
				EXIT
			ENDIF
		BREAK
		CASE THREAD_UPDATE_LONG
			IF REMOVE_UPDATE_THREAD_FROM_LONG(sScriptThread, eThread)	
				PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] MOVE_UPDATE_THREAD_TO_STAGGERED - ", GET_THREAD_NAME(eThread))			
				ADD_UPDATE_THREAD_TO_STAGGERED(sScriptThread, eThread)
				EXIT
			ENDIF
		BREAK
	ENDSWITCH
	
	PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] MOVE_UPDATE_THREAD_TO_STAGGERED - ", GET_THREAD_NAME(eThread), " Not found")			
ENDPROC

//Move thread to Long
PROC MOVE_UPDATE_THREAD_TO_LONG(SCRIPT_THREAD_DATA &sScriptThread, UPDATE_THREAD eThread)
	
	IF IS_THREAD_UPDATE_STATUS_LOCKED(sScriptThread, eThread)
		EXIT
	ENDIF
	
	IF sScriptThread.iUpdateThreadLongCount >= ENUM_TO_INT(MAX_UPDATE_THREAD)
		PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] MOVE_UPDATE_THREAD_TO_LONG - ", GET_THREAD_NAME(eThread), " failed - sScriptThread.iUpdateThreadLongCount >= ENUM_TO_INT(MAX_UPDATE_THREAD) ")			
		ASSERTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] MOVE_UPDATE_THREAD_TO_LONG - ", GET_THREAD_NAME(eThread), " failed - sScriptThread.iUpdateThreadLongCount >= ENUM_TO_INT(MAX_UPDATE_THREAD) ")			
		EXIT
	ENDIF
	
	SWITCH sScriptThread.sUpdateThread[eThread].eStatus
		CASE THREAD_UPDATE_STAGGERED
			IF REMOVE_UPDATE_THREAD_FROM_STAGGERED(sScriptThread, eThread)	
				PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] MOVE_UPDATE_THREAD_TO_LONG - ", GET_THREAD_NAME(eThread))			
				ADD_UPDATE_THREAD_TO_LONG(sScriptThread, eThread)
				EXIT
			ENDIF
		BREAK
		CASE THREAD_UPDATE_EVERYFRAME
			IF REMOVE_UPDATE_THREAD_FROM_EVERYFRAME(sScriptThread, eThread)	
				PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] MOVE_UPDATE_THREAD_TO_LONG - ", GET_THREAD_NAME(eThread))			
				ADD_UPDATE_THREAD_TO_LONG(sScriptThread, eThread)
				EXIT
			ENDIF
		BREAK
	ENDSWITCH
	
	PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] MOVE_UPDATE_THREAD_TO_LONG - ", GET_THREAD_NAME(eThread), " Not found")			
ENDPROC

//Move thread to every frame.
PROC MOVE_UPDATE_THREAD_TO_EVERYFRAME(SCRIPT_THREAD_DATA &sScriptThread, UPDATE_THREAD eThread)

	SWITCH sScriptThread.sUpdateThread[eThread].eStatus
		CASE THREAD_UPDATE_STAGGERED
			IF REMOVE_UPDATE_THREAD_FROM_STAGGERED(sScriptThread, eThread)	
				PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] MOVE_UPDATE_THREAD_TO_EVERYFRAME - ", GET_THREAD_NAME(eThread))			
				ADD_UPDATE_THREAD_TO_EVERYFRAME(sScriptThread, eThread)
				EXIT
			ENDIF
		BREAK
		CASE THREAD_UPDATE_LONG
			IF REMOVE_UPDATE_THREAD_FROM_LONG(sScriptThread, eThread)	
				PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] MOVE_UPDATE_THREAD_TO_EVERYFRAME - ", GET_THREAD_NAME(eThread))			
				ADD_UPDATE_THREAD_TO_EVERYFRAME(sScriptThread, eThread)
				EXIT
			ENDIF
		BREAK
	ENDSWITCH
	
	PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] MOVE_UPDATE_THREAD_TO_EVERYFRAME - ", GET_THREAD_NAME(eThread), " Not found")		
ENDPROC

PROC MOVE_UPDATE_THREAD_TO_STAGGERED_IF_NEEDED(SCRIPT_THREAD_DATA &sScriptThread, UPDATE_THREAD eThread)
	
	IF GET_THREAD_UPDATE_STATUS(sScriptThread, eThread) != THREAD_UPDATE_STAGGERED
		MOVE_UPDATE_THREAD_TO_STAGGERED(sScriptThread, eThread)		
	ENDIF
	
ENDPROC

PROC MOVE_UPDATE_THREAD_TO_LONG_IF_NEEDED(SCRIPT_THREAD_DATA &sScriptThread, UPDATE_THREAD eThread)
	
	IF GET_THREAD_UPDATE_STATUS(sScriptThread, eThread) != THREAD_UPDATE_LONG
		MOVE_UPDATE_THREAD_TO_LONG(sScriptThread, eThread)		
	ENDIF
	
ENDPROC

PROC MOVE_UPDATE_THREAD_TO_EVERYFRAME_IF_NEEDED(SCRIPT_THREAD_DATA &sScriptThread, UPDATE_THREAD eThread, BOOL bLock = FALSE)

	IF GET_THREAD_UPDATE_STATUS(sScriptThread, eThread) != THREAD_UPDATE_EVERYFRAME
		MOVE_UPDATE_THREAD_TO_EVERYFRAME(sScriptThread, eThread)		
	ENDIF
	
	IF bLock
	AND NOT IS_THREAD_UPDATE_STATUS_LOCKED(sScriptThread, eThread)
		SET_THREAD_UPDATE_STATUS_LOCKED(sScriptThread, eThread)
	ENDIF
	
ENDPROC

PROC MAINTAIN_BG_SCRIPT_MOVE_FUNCTION_TO_EVERY_FRAME(SCRIPT_THREAD_DATA &sScriptThread, BOOL &bActive, SCRIPT_THREAD_BG_DATA &sThreadData[])
	IF bActive
		INT iLoop
		INT iArrayCount = COUNT_OF(sThreadData)
		REPEAT ENUM_TO_INT(MAX_UPDATE_THREAD) iLoop
			INT iBitArray = iLoop/32
			INT iBit = iLoop%32
			IF iBitArray < iArrayCount
			AND IS_BIT_SET(sThreadData[iBitArray].iThreadBitSet, iBit)
				PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] MAINTAIN_BG_SCRIPT_MOVE_FUNCTION_TO_EVERY_FRAME - forcing thread to every frame - ", GET_THREAD_NAME(INT_TO_ENUM(UPDATE_THREAD, iLoop)))			
				MOVE_UPDATE_THREAD_TO_EVERYFRAME_IF_NEEDED(sScriptThread, INT_TO_ENUM(UPDATE_THREAD, iLoop), IS_BIT_SET(sThreadData[iBitArray].iThreadLockBitSet, iBit))
			ENDIF
		ENDREPEAT
		REPEAT iArrayCount iLoop
			sThreadData[iLoop].iThreadBitSet = 0
			sThreadData[iLoop].iThreadLockBitSet = 0
		ENDREPEAT
		bActive = FALSE
	ENDIF
ENDPROC

//Step through the staggered functions
PROC PROCESS_STAGGERED_FUNCTIONS(SCRIPT_THREAD_DATA &sScriptThread)
	
	IF sScriptThread.iUpdateThreadStaggeredCount <=0
		EXIT
	ENDIF
	
	INT iThread
	INT iLoop2
	
	REPEAT sScriptThread.iStaggerProcessCount iThread
				
		CALL sScriptThread.sUpdateThread[sScriptThread.updateThreadStaggered[sScriptThread.iCurrentThreadUpdateStaggered]].fpThreadUpdate()
		
		sScriptThread.iCurrentThreadUpdateStaggered++
		
		//Cap the count
		IF sScriptThread.iCurrentThreadUpdateStaggered >= sScriptThread.iUpdateThreadStaggeredCount
			sScriptThread.iCurrentThreadUpdateStaggered = 0
			PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] PROCESS_STAGGERED_FUNCTIONS - sScriptThread.iCurrentThreadUpdateStaggered = 0")	
			BREAKLOOP
		ENDIF
		
	ENDREPEAT
	
	//Once we've got to the end of the loop, then remove any items that have been flagged to be removed. 	
	IF sScriptThread.bNeedToRemoveStaggeredThread
	AND sScriptThread.iCurrentThreadUpdateStaggered = 0
	
		sScriptThread.bNeedToRemoveStaggeredThread = FALSE
		
		UPDATE_THREAD threadToCheck
		
		// Loop backwards so we don't need to alter iThread when we remove items
		FOR iThread = ENUM_TO_INT(MAX_UPDATE_THREAD)-1 TO 0 STEP -1
			threadToCheck = sScriptThread.updateThreadStaggered[iThread]
			
			// Verify if the thread in the Staggered loop is to be removed
			IF threadToCheck != UPDATE_THREAD_INVALID
			AND sScriptThread.bRemovedFromStaggered[threadToCheck] = TRUE
			
				sScriptThread.bRemovedFromStaggered[threadToCheck] = FALSE
				
				// Loop from our position in every frame and shift the array down one.
				FOR iLoop2 = iThread TO ENUM_TO_INT(MAX_UPDATE_THREAD)-2 
					sScriptThread.updateThreadStaggered[iLoop2] = sScriptThread.updateThreadStaggered[iLoop2+1]									
				ENDFOR
				
				//Reduce the count
				sScriptThread.iUpdateThreadStaggeredCount--
				IF sScriptThread.iUpdateThreadStaggeredCount < 0 
					sScriptThread.iUpdateThreadStaggeredCount = 0
				ENDIF
				IF sScriptThread.iStaggerProcessCount > sScriptThread.iUpdateThreadStaggeredCount
					sScriptThread.iStaggerProcessCount = sScriptThread.iUpdateThreadStaggeredCount
				ENDIF
				PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] PROCESS_STAGGERED_FUNCTIONS Removed - ", GET_THREAD_NAME(INT_TO_ENUM(UPDATE_THREAD, threadToCheck)), " - iUpdateThreadStaggeredCount = ", sScriptThread.iUpdateThreadStaggeredCount)			
			ENDIF
		ENDFOR
		
	ENDIF
ENDPROC

//Step through the staggered long functions
PROC PROCESS_LONG_FUNCTIONS(SCRIPT_THREAD_DATA &sScriptThread)
	
	IF sScriptThread.iUpdateThreadLongCount <=0
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_EXPIRED(sScriptThread.stLongUpdate, sScriptThread.iStaggerLongTimer)
		EXIT
	ENDIF
	
	RESET_NET_TIMER(sScriptThread.stLongUpdate)
	
	CALL sScriptThread.sUpdateThread[sScriptThread.updateThreadLong[sScriptThread.iCurrentThreadUpdateLong]].fpThreadUpdate()
	
	//If we've removed an item from the stagger, we don't want to updated the stagger, as we've reduced the count. 
	IF sScriptThread.bDontUpDateLong
		PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] PROCESS_LONG_FUNCTIONS - bDontUpDateLong")			
		sScriptThread.bDontUpDateLong = FALSE
		IF sScriptThread.iCurrentThreadUpdateLong >= sScriptThread.iUpdateThreadLongCount
			sScriptThread.iCurrentThreadUpdateLong = 0
			PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] PROCESS_LONG_FUNCTIONS - iCurrentThreadUpdateLong = ", sScriptThread.iCurrentThreadUpdateLong)			
		ENDIF
		EXIT
	ELSE
		sScriptThread.iCurrentThreadUpdateLong++
		IF sScriptThread.iCurrentThreadUpdateLong >= sScriptThread.iUpdateThreadLongCount
			sScriptThread.iCurrentThreadUpdateLong = 0
			#IF IS_DEBUG_BUILD
			IF sScriptThread.bDumpStatusToLogs
			DUMP_UPDATE_THREAD_STATUS(sScriptThread)
			ENDIF
			#ENDIF
		ENDIF
		PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] PROCESS_LONG_FUNCTIONS - iCurrentThreadUpdateLong = ", sScriptThread.iCurrentThreadUpdateLong)			
	ENDIF

ENDPROC

PROC PROCESS_EVERYFRAME_FUNCTIONS(SCRIPT_THREAD_DATA &sScriptThread)
	
	IF sScriptThread.iUpdateThreadEveryFrameCount <=0
		EXIT
	ENDIF
	
	INT iThread
	INT iLoop2
	
	REPEAT sScriptThread.iUpdateThreadEveryFrameCount iThread

		CALL sScriptThread.sUpdateThread[sScriptThread.updateThreadEveryFrame[iThread]].fpThreadUpdate()

	ENDREPEAT
	
	//Once we've got to the end of the loop, then remove any items that have been flagged to be removed. 	
	IF sScriptThread.bNeedToRemoveEveryFrameThread
		
		UPDATE_THREAD threadToCheck
		
		// Loop backwards so we don't need to alter iThread when we remove items
		FOR iThread = ENUM_TO_INT(MAX_UPDATE_THREAD)-1 TO 0 STEP -1
			threadToCheck = sScriptThread.updateThreadEveryFrame[iThread]
			
			// Verify if the thread in the everyframe loop is to be removed
			IF threadToCheck != UPDATE_THREAD_INVALID
			AND sScriptThread.bRemovedFromEveryFrame[threadToCheck] = TRUE
			
				sScriptThread.bRemovedFromEveryFrame[threadToCheck] = FALSE
				
				// Loop from our position in every frame and shift the array down one.
				FOR iLoop2 = iThread TO ENUM_TO_INT(MAX_UPDATE_THREAD)-2 
					sScriptThread.updateThreadEveryFrame[iLoop2] = sScriptThread.updateThreadEveryFrame[iLoop2+1]									
				ENDFOR
				
				//Reduce the count
				sScriptThread.iUpdateThreadEveryFrameCount--
				IF sScriptThread.iUpdateThreadEveryFrameCount < 0 
					sScriptThread.iUpdateThreadEveryFrameCount = 0
				ENDIF
				PRINTLN("[UPDATE_THREAD][", sScriptThread.sThreadName,"] PROCESS_EVERYFRAME_FUNCTIONS Removed - ", GET_THREAD_NAME(INT_TO_ENUM(UPDATE_THREAD, threadToCheck)), " - iUpdateThreadEveryFrameCount = ", sScriptThread.iUpdateThreadEveryFrameCount)			
			ENDIF
		ENDFOR
		sScriptThread.bNeedToRemoveEveryFrameThread = FALSE
	ENDIF
ENDPROC

