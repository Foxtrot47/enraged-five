//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_marketing_rewards.sc																		//
// Description: Script for giving players rewards through a FIFO queue without duplication. The main  			//
//				functionality is executed by a freemode.sc thread through MAINTAIN_MARKETING_REWARDS_QUEUE		//
// Written by:  Bobby (Boyan) Yotov																				//
// Date:  		14/09/2022																						//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
#IF FEATURE_DLC_2_2022
USING "net_marketing_rewards_public.sch"
USING "mp_globals_fm.sch"
USING "stats_enums.sch"
USING "reward_unlocks.sch"
USING "net_rank_rewards.sch"
USING "net_stat_tracking.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 																												//
//									MARKETING REWARDS TYPE PROCESSING FUNCTIONS									//
//																												//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PROC MLR_PROCESS_GIFT_FULL_AMMO()
	INT iWeaponSlot
	WEAPON_TYPE aWeapon
	AMMO_TYPE eAmmoType
	scrShopWeaponData sWeaponData
	INT iMaxAmmoCount = 0
	INT iCurrentAmmo = 0
	INT iAmmoToGive = 0
	// Loop over our normal weapons
	REPEAT (ENUM_TO_INT(NUM_WEAPONSLOTS)-1) iWeaponSlot
		// Get the weapon type from the slot
		IF GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot) = WEAPONSLOT_INVALID
			aWeapon = WEAPONTYPE_INVALID
		ELSE
			aWeapon = GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot))
		ENDIF
		
		IF aWeapon != WEAPONTYPE_INVALID
		AND aWeapon != WEAPONTYPE_UNARMED
		AND GET_WEAPON_AMMO_TYPE_FROM_WEAPON_TYPE(aWeapon,eAmmoType)
		AND eAmmoType != AMMOTYPE_INVALID
			GET_MAX_AMMO(PLAYER_PED_ID(), aWeapon, iMaxAmmoCount)
			iCurrentAmmo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), aWeapon)
			iAmmoToGive = iMaxAmmoCount - iCurrentAmmo
			IF iAmmoToGive > 0
				ADD_AMMO_TO_PED(PLAYER_PED_ID(), aWeapon, iAmmoToGive)
				CHECK_AND_SAVE_WEAPON(aWeapon)
			ENDIF
			PRINTLN(" MLR_PROCESS_GIFT_FULL_AMMO - Gifting ammo for weapon: ", GET_WEAPON_NAME(aWeapon), " Ammo: ", iAmmoToGive)
		ENDIF
	ENDREPEAT
	
	// Process our DLC weapons in the same way	
	REPEAT (GET_NUM_DLC_WEAPONS()-1) iWeaponSlot
		IF GET_DLC_WEAPON_DATA(iWeaponSlot, sWeaponData)
			aWeapon = INT_TO_ENUM(WEAPON_TYPE, sWeaponData.m_nameHash)
			
			IF aWeapon != WEAPONTYPE_INVALID
			AND aWeapon != WEAPONTYPE_UNARMED
			AND GET_WEAPON_AMMO_TYPE_FROM_WEAPON_TYPE(aWeapon,eAmmoType)
			AND eAmmoType != AMMOTYPE_INVALID
				GET_MAX_AMMO(PLAYER_PED_ID(), aWeapon, iMaxAmmoCount)
				iCurrentAmmo = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), aWeapon)
				iAmmoToGive = iMaxAmmoCount - iCurrentAmmo
				IF iAmmoToGive > 0
					ADD_AMMO_TO_PED(PLAYER_PED_ID(), aWeapon, iAmmoToGive)
					CHECK_AND_SAVE_WEAPON(aWeapon)
				ENDIF
				PRINTLN(" MLR_PROCESS_GIFT_FULL_AMMO - Gifting ammo for DCL weapon: ", GET_WEAPON_NAME(aWeapon), " Ammo: ", iAmmoToGive)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MLR_PROCESS_GIFT_SNACKS()

	// Currently setup with Marketing Login Rewards. Later might need to update to handle additional data in the queue process.
	INT iItemCategory = hash("SNACK")
	INT iReason = hash("GET")
	INT iShopType = hash("MARKETING_REWARDS")
	INT iLocation = hash("LOGIN")
	INT iDifference
	
	// PQ
	iDifference = INV_SNACK_1_MAX - GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS)
	IF iDifference > 0
		SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
		PRINTLN(" MLR_PROCESS_GIFT_SNACKS: correct PQ (YUM_SNACKS) to INV_SNACK_1_MAX = ", INV_SNACK_1_MAX, " ")
		SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0"), iItemCategory, iDifference, iLocation, iReason, FALSE, iShopType)
	ENDIF
	
	// EGOCHASER
	iDifference = INV_SNACK_2_MAX - GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS)
	IF iDifference > 0
		SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
		PRINTLN("MLR_PROCESS_GIFT_SNACKS: correct EGOCHASER (HEALTH_SNACKS) to INV_SNACK_2_MAX = ", INV_SNACK_2_MAX, " ")
		SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0"), iItemCategory, iDifference, iLocation, iReason, FALSE, iShopType)
	ENDIF
	
	// METEORITE
	iDifference = INV_SNACK_3_MAX - GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS)
	IF iDifference > 0
		SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
		PRINTLN(" MLR_PROCESS_GIFT_SNACKS: correct METORITE (EPIC_SNACKS) to INV_SNACK_3_MAX = ", INV_SNACK_3_MAX, " ")
		SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0"), iItemCategory, iDifference, iLocation, iReason, FALSE, iShopType)
	ENDIF

	// REDWOOD (CIGGS)
	iDifference = INV_SMOKES_MAX - GET_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT)
	IF iDifference > 0
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT, INV_SMOKES_MAX)
		PRINTLN(" MLR_PROCESS_GIFT_SNACKS: correct REDWOOD (CIGGARETTES) to INV_SMOKES_MAX = ", INV_SMOKES_MAX, " ")
		SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_CIGARETTES_BOUGHT_v0"), iItemCategory, iDifference, iLocation, iReason, FALSE, iShopType)
	ENDIF
	
	// ECOLA
	iDifference = INV_DRINK_1_MAX - GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT)
	IF iDifference > 0
		SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, INV_DRINK_1_MAX)
		PRINTLN(" MLR_PROCESS_GIFT_SNACKS: correct ECOLA (ORANGE) to INV_DRINK_1_MAX = ", INV_DRINK_1_MAX, " ")
		SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0"), iItemCategory, iDifference, iLocation, iReason, FALSE, iShopType)
	ENDIF
	
	// PISSWASSER
	iDifference = INV_DRINK_2_MAX - GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_BOURGE_BOUGHT)
	IF iDifference > 0
		SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_BOURGE_BOUGHT, INV_DRINK_2_MAX)
		PRINTLN(" MLR_PROCESS_GIFT_SNACKS: correct PISSWASSER (BOURGE) to INV_DRINK_2_MAX = ", INV_DRINK_2_MAX, " ")
		SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NUMBER_OF_BOURGE_BOUGHT_v0"), iItemCategory, iDifference, iLocation, iReason, FALSE, iShopType)
	ENDIF
	
	// CHAMPAGNE = INV_DRINK_3_MAX
	
	// SPRUNK
	iDifference = INV_DRINK_4_MAX - GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_SPRUNK_BOUGHT)
	IF iDifference > 0
		SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_SPRUNK_BOUGHT, INV_DRINK_4_MAX)
		PRINTLN(" MLR_PROCESS_GIFT_SNACKS: correct SPRUNK (SPRUNK) TO INV_DRINK_4_MAX = ", INV_DRINK_4_MAX, " ")
		SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_NUMBER_OF_SPRUNK_BOUGHT_v0"), iItemCategory, iDifference, iLocation, iReason, FALSE, iShopType)
	ENDIF
	
ENDPROC

PROC MLR_PROCESS_GIFT_FULL_ARMOUR()
	INT iItemCategory = hash("ARMOR")
	INT iReason = hash("GET")
	INT iShopType = hash("MARKETING_REWARDS")
	INT iLocation = hash("LOGIN")
	INT iDifference = 0
	
	
	INT iSlot = GET_ACTIVE_CHARACTER_SLOT()
	// Get the number of inventory slots for armour
	INT iCount = 0
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L10)
		iCount = 10
	ELIF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L9)
		iCount = 9
	ELIF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L8)
		iCount = 8
	ELIF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L7)
		iCount = 7
	ELIF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L6)
		iCount = 6
	ELIF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L5)
		iCount = 5
	ELIF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L4)
		iCount = 4
	ELIF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L3)
		iCount = 3
	ELIF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L2)
		iCount = 2
	ELIF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L1)
		iCount = 1
	ENDIF
	
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_SUPERLIGHTARMOUR)
		iDifference = iCount - GET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_1_COUNT)
		IF iDifference > 0 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_1_COUNT, iCount, iSlot)
			PRINTLN(" MLR_PROCESS_GIFT_FULL_ARMOUR: Adding ", iDifference, " to ARMOUR_1 SUPERLIGHTARMOUR")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_MP_CHAR_ARMOUR_1_COUNT_v0"), iItemCategory, iDifference, iLocation, iReason, FALSE, iShopType)
		ENDIF
	ENDIF
	
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_LIGHTARMOUR)
		iDifference = iCount - GET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_2_COUNT)
		IF iDifference > 0 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_2_COUNT, iCount, iSlot)
			PRINTLN(" MLR_PROCESS_GIFT_FULL_ARMOUR: Adding ", iDifference, " to ARMOUR_2 LIGHTARMOUR")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_MP_CHAR_ARMOUR_2_COUNT_v0"), iItemCategory, iDifference, iLocation, iReason, FALSE, iShopType)
		ENDIF
	ENDIF
	
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_STANDARDARMOUR)
		iDifference = iCount - GET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_3_COUNT)
		IF iDifference > 0 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_3_COUNT, iCount, iSlot)
			PRINTLN(" MLR_PROCESS_GIFT_FULL_ARMOUR: Adding ", iDifference, " to ARMOUR_3 STANDARDARMOUR")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_MP_CHAR_ARMOUR_3_COUNT_v0"), iItemCategory, iDifference, iLocation, iReason, FALSE, iShopType)
		ENDIF
	ENDIF
	
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_HEAVYARMOUR)
		iDifference = iCount - GET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_4_COUNT)
		IF iDifference > 0 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_4_COUNT, iCount, iSlot)
			PRINTLN(" MLR_PROCESS_GIFT_FULL_ARMOUR: Adding ", iDifference, " to ARMOUR_4 HEAVYARMOUR")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_MP_CHAR_ARMOUR_4_COUNT_v0"), iItemCategory, iDifference, iLocation, iReason, FALSE, iShopType)
		ENDIF
	ENDIF
	
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_SUPERHEAVYARMOUR)
		iDifference = iCount - GET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_5_COUNT)
		IF iDifference > 0 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_5_COUNT, iCount, iSlot)
			PRINTLN(" MLR_PROCESS_GIFT_FULL_ARMOUR: Adding ", iDifference, " to ARMOUR_5 SUPERHEAVYARMOUR")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, hash("MP_STAT_MP_CHAR_ARMOUR_5_COUNT_v0"), iItemCategory, iDifference, iLocation, iReason, FALSE, iShopType)
		ENDIF
	ENDIF
	
	NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
ENDPROC

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// 																												//
//												MARKETING REWARDS QUEUE											//
//																												//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC BOOL PROCESS_MARKETING_REWARDS(MARKETING_REWARDS_TYPE eType)
	SWITCH eType
	
	CASE MARKETING_REWARDS_REFILL_AMMO
		MLR_PROCESS_GIFT_FULL_AMMO()
		RETURN TRUE
	BREAK
	
	CASE MARKETING_REWARDS_REFILL_ARMOUR
		MLR_PROCESS_GIFT_FULL_ARMOUR()
		RETURN TRUE
	BREAK
	
	CASE MARKETING_REWARDS_REFILL_SNACKS
		MLR_PROCESS_GIFT_SNACKS()
		RETURN TRUE
	BREAK
	DEFAULT
		CASSERTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_REWARDS] - PROCESS_MARKETING_REWARDS: reward type is invalid/missing!")
		RETURN FALSE
	BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_MARKETING_REWARDS_QUEUE()
	IF g_sMarketingRewardsData.iCurrentSize > 0
			CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_REWARDS] - MAINTAIN_MARKETING_REWARDS_QUEUE_PROCESSING: Processing reward of type: ", GET_MARKETING_REWARDS_TYPE_NAME(g_sMarketingRewardsData.queue[0]))
		IF PROCESS_MARKETING_REWARDS(g_sMarketingRewardsData.queue[0])
			CLEAR_BIT(g_sMarketingRewardsData.iRewardsInQueueBitset0 , ENUM_TO_INT(g_sMarketingRewardsData.queue[0]))
			
			// Remove the head item and push forward all other items in the queue
			g_sMarketingRewardsData.iCurrentSize--
			
			INT index
			REPEAT g_sMarketingRewardsData.iCurrentSize index
				g_sMarketingRewardsData.queue[index] = g_sMarketingRewardsData.queue [index + 1]
			ENDREPEAT
			
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_REWARDS] - MAINTAIN_MARKETING_REWARDS_QUEUE: Processed successfully: Queue size = ", g_sMarketingRewardsData.iCurrentSize)
			#ENDIF
		ENDIF
	ELIF g_sMarketingRewardsData.iCurrentSize = 0
		CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_REWARDS] - MAINTAIN_MARKETING_REWARDS_QUEUE_PROCESSING: Queue emptied")
	ELSE
		// Should not reach here
		CPRINTLN(DEBUG_NET_MARKETING_REWARDS, "[NET_MARKETING_REWARDS] - MAINTAIN_MARKETING_REWARDS_QUEUE_PROCESSING: index is negative, assigning 0")
		g_sMarketingRewardsData.iCurrentSize = 0
	ENDIF
ENDPROC
#ENDIF
