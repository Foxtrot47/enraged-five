USING "net_spawn_move.sch"
USING "net_missions_at_coords_public_checks.sch"

FUNC BOOL IS_POINT_IN_NEAREST_GANG_ATTACK_BLIP_RADIUS(VECTOR &vPoint, SPAWN_SEARCH_GANG_ATTACK &NearestGangAttack[], BOOL bMovePointOutsideZone=FALSE, FLOAT fRadiusMultiplier=1.0)
	INT		tempLoop		= 0
	VECTOR vNewPoint
	REPEAT COUNT_OF(NearestGangAttack) tempLoop

		IF (VDIST(NearestGangAttack[tempLoop].vBlip, vPoint) < (ANGLED_AREA_BLIP_RADIUS_m*fRadiusMultiplier))
			
			// Found the point is inside an area-triggered mission
			NET_PRINT("[spawning] IS_POINT_IN_NEAREST_GANG_ATTACK_BLIP_RADIUS - returning TRUE -  point inside gang attack blip") NET_NL()
			
			IF (bMovePointOutsideZone)
				vNewPoint = vPoint
				MovePointOutsideSphere(vNewPoint, NearestGangAttack[tempLoop].vBlip, ANGLED_AREA_BLIP_RADIUS_m)
				NET_PRINT("[spawning] IS_POINT_IN_NEAREST_GANG_ATTACK_BLIP_RADIUS - MovePointOutsideSphere - new vNewPoint = ") NET_PRINT_VECTOR(vNewPoint) NET_NL()					
//				// if new point is in another area then move out, but this time flip
//				IF IS_POINT_IN_NEAREST_GANG_ATTACK_BLIP_RADIUS(vNewPoint)
//					vNewPoint = vPoint
//					MovePointOutsideSphere(vNewPoint, NearestGangAttack[tempLoop].vBlip, ANGLED_AREA_BLIP_RADIUS_m, DEFAULT, TRUE)
//					NET_PRINT("[spawning] IS_POINT_IN_NEAREST_GANG_ATTACK_BLIP_RADIUS - MovePointOutsideSphere (flipped) - new vNewPoint = ") NET_PRINT_VECTOR(vNewPoint) NET_NL()	
//				ENDIF
				vPoint = vNewPoint
			ENDIF
				
			RETURN TRUE			
		ENDIF

	ENDREPEAT	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_POINT_IN_NEAREST_GANG_ATTACK_TRIGGER_AREA(VECTOR &vPoint, SPAWN_SEARCH_GANG_ATTACK &NearestGangAttack[])
	INT		tempLoop		= 0
	REPEAT COUNT_OF(NearestGangAttack) tempLoop
		IF IS_POINT_IN_ANGLED_AREA(vPoint, NearestGangAttack[tempLoop].vMin, NearestGangAttack[tempLoop].vMax, NearestGangAttack[tempLoop].fWidth)
			PRINTLN("[spawning] IS_POINT_IN_NEAREST_GANG_ATTACK_TRIGGER_AREA - returning TRUE -  point inside gang attack trigger location.") 
			RETURN(TRUE)
		ENDIF
	ENDREPEAT	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_POINT_IN_NEAREST_MISSION_CORONA(VECTOR &vPoint, SPAWN_SEARCH_CORONA &NearestCoords[], BOOL bMovePointOutsideZone=FALSE)

	INT		tempLoop		= 0
	
	// For the first pass, check only missions with coronas
	REPEAT COUNT_OF(NearestCoords) tempLoop
	
		IF (VDIST(NearestCoords[tempLoop].vCoords, vPoint) < NearestCoords[tempLoop].fRadius)
			
			// Found the point is inside an area-triggered mission
			NET_PRINT("[spawning] IS_POINT_IN_NEAREST_MISSION_CORONA - returning TRUE -  point inside corona ") NET_NL()
			
			IF (bMovePointOutsideZone)
				MovePointOutsideSphere(vPoint, NearestCoords[tempLoop].vCoords, NearestCoords[tempLoop].fRadius)
			ENDIF
			
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN(FALSE)
ENDFUNC

// note - this should only be called by the local player when faded out as it will cause a spike
FUNC BOOL IS_POINT_IN_GANG_ATTACK_OR_MISSION_CORONA(VECTOR vCoords, FLOAT fGangAttackRadiusMultiplier=1.0)

	#IF IS_DEBUG_BUILD
		IF NOT IS_SCREEN_FADED_OUT()
		AND GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
			IF NOT IS_SCREEN_FADED_OUT()
				PRINTLN("[spawning] IS_POINT_IN_GANG_ATTACK_OR_MISSION_CORONA - called when screen is not faded out")
			ENDIF
			IF GET_TOGGLE_PAUSED_RENDERPHASES_STATUS()
				PRINTLN("[spawning] IS_POINT_IN_GANG_ATTACK_OR_MISSION_CORONA - called when renderphase is true.")
			ENDIF
			SCRIPT_ASSERT("IS_POINT_IN_GANG_ATTACK_OR_MISSION_CORONA - this should only be called when screen is faded out as it will cause a spike")
		ENDIF
	#ENDIF

	// is this in a gang attack area?
	Get_Nearest_Mission_Coronas_And_Gang_Attacks_To_Point(vCoords, g_SpawnData.SearchTempData.NearestCoronas, g_SpawnData.SearchTempData.NearestGangAttack)

	IF IS_POINT_IN_NEAREST_GANG_ATTACK_BLIP_RADIUS(vCoords, g_SpawnData.SearchTempData.NearestGangAttack, FALSE, fGangAttackRadiusMultiplier)
		PRINTLN("[spawning] IS_POINT_IN_GANG_ATTACK_OR_MISSION_CORONA - in gang attack radius ")
		RETURN(TRUE)
	ENDIF
	
	IF IS_POINT_IN_NEAREST_GANG_ATTACK_TRIGGER_AREA(vCoords, g_SpawnData.SearchTempData.NearestGangAttack)
		PRINTLN("[spawning] IS_POINT_IN_NEAREST_GANG_ATTACK_TRIGGER_AREA - in gang attack trigger area ")
		RETURN(TRUE)
	ENDIF	

	IF IS_POINT_IN_NEAREST_MISSION_CORONA(vCoords, g_SpawnData.SearchTempData.NearestCoronas, FALSE)
		PRINTLN("[spawning] IS_POINT_IN_GANG_ATTACK_OR_MISSION_CORONA - in mission corona ")
		RETURN(TRUE)
	ENDIF
	
	RETURN(FALSE)
	
ENDFUNC


