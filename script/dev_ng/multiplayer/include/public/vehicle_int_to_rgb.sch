
USING "globals.sch"

//Converts a vehicle colour INT to an RGB

PROC GET_VEHICLE_RGB_FROM_INT(INT iColour, INT &iTempR, INT &iTempG, INT &iTempB)
 	SWITCH iColour
	CASE 0
 		iTempR = 8
 		iTempG = 8
 		iTempB = 8
 	BREAK
 	CASE 1
 		iTempR = 37
 		iTempG = 37
 		iTempB = 39
 	BREAK
 	CASE 22
 		iTempR = 140
 		iTempG = 146
 		iTempB = 154
 	BREAK
 	CASE 23
 		iTempR = 91
 		iTempG = 93
 		iTempB = 94
 	BREAK
 	CASE 6
 		iTempR = 81
 		iTempG = 84
 		iTempB = 89
 	BREAK
 	CASE 111
 		iTempR = 240
 		iTempG = 240
 		iTempB = 240
 	BREAK
 	CASE 28
 		iTempR = 150
 		iTempG = 8
 		iTempB = 0
 	BREAK
 	CASE 34
 		iTempR = 38
 		iTempG = 3
 		iTempB = 6
 	BREAK
 	CASE 88
 		iTempR = 245
 		iTempG = 137
 		iTempB = 15
 	BREAK
 	CASE 45
 		iTempR = 74
 		iTempG = 22
 		iTempB = 7
 	BREAK
 	CASE 56
 		iTempR = 45
 		iTempG = 58
 		iTempB = 53
 	BREAK
 	CASE 58
 		iTempR = 71
 		iTempG = 120
 		iTempB = 60
 	BREAK
 	CASE 54
 		iTempR = 77
 		iTempG = 98
 		iTempB = 104
 	BREAK
 	CASE 73
 		iTempR = 14
 		iTempG = 49
 		iTempB = 109
 	BREAK
 	CASE 68
 		iTempR = 22
 		iTempG = 34
 		iTempB = 72
 	BREAK
 	CASE 140
 		iTempR = 0
 		iTempG = 174
 		iTempB = 239
 	BREAK
 	CASE 131
 		iTempR = 255
 		iTempG = 183
 		iTempB = 0
 	BREAK
 	CASE 90
 		iTempR = 142
 		iTempG = 140
 		iTempB = 70
 	BREAK
 	CASE 97
 		iTempR = 156
 		iTempG = 141
 		iTempB = 113
 	BREAK
 	CASE 89
 		iTempR = 145
 		iTempG = 115
 		iTempB = 71
 	BREAK
 	CASE 105
 		iTempR = 98
 		iTempG = 68
 		iTempB = 40
 	BREAK
 	CASE 100
 		iTempR = 124
 		iTempG = 27
 		iTempB = 68
 	BREAK
 	CASE 99
 		iTempR = 114
 		iTempG = 42
 		iTempB = 63
 	BREAK
 	CASE 136
 		iTempR = 246
 		iTempG = 151
 		iTempB = 153
 	BREAK
 	CASE 49
 		iTempR = 32
 		iTempG = 32
 		iTempB = 44
 	BREAK
 	CASE 146
 		iTempR = 26
 		iTempG = 1
 		iTempB = 23
 	BREAK
	DEFAULT
 		iTempR = 255
 		iTempG = 255
 		iTempB = 255
	BREAK
	ENDSWITCH
ENDPROC







