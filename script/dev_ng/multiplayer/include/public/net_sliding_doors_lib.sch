///////////////////////////////////////////////////////////////////////////////////
// Name:       net_sliding_doors_lib.sch	                                     //
// Description: Create sliding doors											 //
// Written By:   Aleksej Leskin                                    				 //
///////////////////////////////////////////////////////////////////////////////////
   

USING "model_enums.sch"
USING "commands_entity.sch"
USING "net_events.sch"

CONST_FLOAT CF_DOOR_OPENED_BIAS 1.00
CONST_FLOAT CF_DOOR_CLOSED_BIAS 0.00

ENUM SLIDING_DOOR_STATE_ENUM
	//static
	SDS_OPENED,
	SDS_CLOSED,
	//In motion
	SDS_OPENING,
	SDS_CLOSING
ENDENUM

STRUCT SLIDING_DOOR_DESCRIPTION_STRUCT
	//model of the door
	MODEL_NAMES		Model = DUMMY_MODEL_FOR_SCRIPT
	//radius in which player starts/stops to interact with the door
	FLOAT			fInteractRadius = 1.5
	//speed of the door opening/closing
	FLOAT 			fDoorSpeed = 2.5
	VECTOR			vOpenedPosition
	VECTOR			vClosedPosition
	//If door can be opened/closed
	BOOL 			bStatic = FALSE
	FLOAT			fDoorHeading = 0.0
	//How much time should pass before door closes , if no palyer is near.
	FLOAT			fDoorOpenedTimeOut = 3.0
	
	STRING					audioSoundSetDoorOpen
	STRING					audioSoundSetDoorClose
	STRING					audioSoundDoorOpen
	STRING					audioSoundDoorClose
ENDSTRUCT

STRUCT SLIDING_DOOR_STRUCT
	ENTITY_INDEX 			Entity	
	MODEL_NAMES				Model = DUMMY_MODEL_FOR_SCRIPT
	
	BOOL 					bStatic = FALSE
	
	SLIDING_DOOR_STATE_ENUM	eDesiredState = SDS_CLOSED
	SLIDING_DOOR_STATE_ENUM	eCurrentState = SDS_CLOSED
	
	FLOAT					fStateBias = CF_DOOR_CLOSED_BIAS
	
	VECTOR					vOpenedPosition
	VECTOR					vClosedPosition
	FLOAT 					fDoorSpeed = 2.5
	FLOAT					fInteractRadius = 1.5
	FLOAT					fDoorHeading = 0.0
	FLOAT					fDoorOpenedTimeOut = 3.0
	FLOAT					fDoorOpenedTimer = 0.0
	
	BOOL 					bDoorSoundPlayed = FALSE
	STRING					audioSoundSetDoorOpen
	STRING					audioSoundSetDoorClose
	STRING					audioSoundDoorOpen
	STRING					audioSoundDoorClose
	INT						stateChangedGameTimeMs = 0
ENDSTRUCT

FUNC STRING GET_SLIDING_DOOR_STATE_ENUM_STRING(SLIDING_DOOR_STATE_ENUM eState)
	SWITCH eState
		CASE SDS_OPENED RETURN "SDS_OPENED"
		CASE SDS_CLOSED RETURN "SDS_CLOSED"
		CASE SDS_OPENING RETURN "SDS_OPENING"
		CASE SDS_CLOSING RETURN "SDS_CLOSING"
	ENDSWITCH
	
	RETURN "UNDEFINED DOOR STATE"
ENDFUNC

STRUCT DOOR_STATE_EVENT
	SCRIPTED_EVENT_TYPES eEventType
	INT	iDoorID
	SLIDING_DOOR_STATE_ENUM newState
	INT iHitByClientID
ENDSTRUCT

///////////////////////////////////////////////////////////////////////////////////////////////////////////
///    										FUNC/PROC													//
//////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Force a specific door state Opened/Closed
/// PARAMS:
///    sDoorsArray - 
///    iActiveDoorCount - 
///    iDoorID - 
///    eNewState - 
/// RETURNS:
///    
FUNC BOOL SET_DOOR_DESIRED_STATE(SLIDING_DOOR_STRUCT& sDoorsArray[], INT & iActiveDoorCount, INT iDoorID, SLIDING_DOOR_STATE_ENUM eNewState)
	IF iDoorID > iActiveDoorCount
		CDEBUG1LN(DEBUG_SAFEHOUSE, "SS - SET_DOOR_STATE - can not set door state, iDoorID: ", iDoorID, " is > iActiveDoorCount: ", iActiveDoorCount)
		RETURN FALSE
	ENDIF
	
	IF sDoorsArray[iDoorID].eDesiredState <> eNewState
		sDoorsArray[iDoorID].eDesiredState = eNewState
		RETURN TRUE
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "SS - SET_DOOR_STATE - can not set door state, iDoorID: ", iDoorID, " TO State: ", GET_SLIDING_DOOR_STATE_ENUM_STRING(eNewState), " already in that state")
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Add single door to the stack
/// PARAMS:
///    sDoorsArray - 
///    iActiveDoorCount - 
///    newDoorDescription - 
/// RETURNS:
///    
FUNC BOOL ADD_SLIDING_DOOR(SLIDING_DOOR_STRUCT& sDoorsArray[], INT & iActiveDoorCount, SLIDING_DOOR_DESCRIPTION_STRUCT newDoorDescription)
	IF iActiveDoorCount >= COUNT_OF(sDoorsArray)
		ASSERTLN("ADD_SLIDING_DOOR - Can not add more sliding doors, sDoorsArray is full, COUNT_OF(sDoorsArray): ",COUNT_OF(sDoorsArray))
		RETURN FALSE
	ENDIF
	
	sDoorsArray[iActiveDoorCount].Model = newDoorDescription.Model
	sDoorsArray[iActiveDoorCount].vOpenedPosition = newDoorDescription.vOpenedPosition
	sDoorsArray[iActiveDoorCount].vClosedPosition = newDoorDescription.vClosedPosition
	sDoorsArray[iActiveDoorCount].fInteractRadius = newDoorDescription.fInteractRadius
	sDoorsArray[iActiveDoorCount].bStatic = newDoorDescription.bStatic
	sDoorsArray[iActiveDoorCount].fDoorSpeed = newDoorDescription.fDoorSpeed
	sDoorsArray[iActiveDoorCount].fDoorHeading = newDoorDescription.fDoorHeading
	sDoorsArray[iActiveDoorCount].fDoorOpenedTimeOut = newDoorDescription.fDoorOpenedTimeOut
	//Audio	
	sDoorsArray[iActiveDoorCount].audioSoundSetDoorOpen = newDoorDescription.audioSoundSetDoorOpen
	sDoorsArray[iActiveDoorCount].audioSoundSetDoorClose = newDoorDescription.audioSoundSetDoorClose
	sDoorsArray[iActiveDoorCount].audioSoundDoorOpen = newDoorDescription.audioSoundDoorOpen
	sDoorsArray[iActiveDoorCount].audioSoundDoorClose = newDoorDescription.audioSoundDoorClose
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "AD - ADD_SLIDING_DOOR - Sliding door Added ID: ", iActiveDoorCount, " vPosClosed: ", sDoorsArray[iActiveDoorCount].vClosedPosition)	
	iActiveDoorCount++
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Spawn all the defined doors.
/// PARAMS:
///    sDoorsArray - 
///    iActiveDoorCount - 
PROC CREATE_SLIDING_DOORS(SLIDING_DOOR_STRUCT& sDoorsArray[], INT iActiveDoorCount)
	INT iDoorID
	
	REPEAT iActiveDoorCount iDoorID
		IF NOT DOES_ENTITY_EXIST(sDoorsArray[iDoorID].Entity)
			sDoorsArray[iDoorID].Entity = GET_ENTITY_FROM_PED_OR_VEHICLE(
				CREATE_OBJECT_NO_OFFSET(sDoorsArray[iDoorID].Model, sDoorsArray[iDoorID].vClosedPosition, FALSE, TRUE, TRUE))
			
			SET_ENTITY_HEADING(sDoorsArray[iDoorID].Entity, sDoorsArray[iDoorID].fDoorHeading)
			FREEZE_ENTITY_POSITION(sDoorsArray[iDoorID].Entity, TRUE)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CSD - CRAETE_SLIDING_DOORS - Door Craeted ID: ", iDoorID)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Func definition for event callback from sliding doors, to broadcast the event.
/// PARAMS:
///    iDoorID - 
///    newState - 
PROC EVENT_DOOR_STATE_CHANGED(INT iDoorID, SLIDING_DOOR_STATE_ENUM newState)
	DOOR_STATE_EVENT sToSend
	sToSend.eEventType = SCRIPT_EVENT_SLIDING_DOOR_STATE
	sToSend.newState = newState
	sToSend.iDoorID = iDoorID
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CALLBACK! - EVENT_DOOR_STATE_CHANGED - CALL BACK DOOR SLIDING OPEN: iDoorID", iDoorID, " NewState?: ", GET_SLIDING_DOOR_STATE_ENUM_STRING(newState))
	
	INT iPlayers = ALL_PLAYERS_ON_SCRIPT(FALSE)
	IF iPlayers <> 0
		CDEBUG1LN(DEBUG_SAFEHOUSE, "CALLBACK! - EVENT_DOOR_STATE_CHANGED - was sent to players")
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sToSend, SIZE_OF(sToSend), iPlayers)
	ENDIF
ENDPROC

/// PURPOSE:
///    Update a specific door to move towards its desired state.
/// PARAMS:
///    sDoor - 
PROC UPDATE_DOOR_SLIDE(SLIDING_DOOR_STRUCT &sDoor)
	BOOL bUpdatePosition = FALSE
	
	IF (sDoor.eCurrentState <> sDoor.eDesiredState)
		FLOAT fNewBias

		IF sDoor.eDesiredState = SDS_OPENED
			//Open Sound
			IF NOT sDoor.bDoorSoundPlayed
			AND NOT IS_STRING_NULL_OR_EMPTY(sDoor.audioSoundSetDoorOpen)
			AND NOT IS_STRING_NULL_OR_EMPTY(sDoor.audioSoundDoorOpen)
				PLAY_SOUND_FROM_COORD(-1, sDoor.audioSoundDoorOpen, GET_ENTITY_COORDS(sDoor.Entity), sDoor.audioSoundSetDoorOpen)
				sDoor.bDoorSoundPlayed = TRUE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_GUNTRUCK_SLIDING_DOORS - Open Sound, Sound: ", sDoor.audioSoundDoorOpen, " Stream: ", sDoor.audioSoundSetDoorOpen)
			ENDIF
			
			//If Closed
			IF sDoor.fStateBias < CF_DOOR_OPENED_BIAS
				fNewBias = sDoor.fStateBias + sDoor.fDoorSpeed * (TO_FLOAT(GET_GAME_TIMER() - sDoor.stateChangedGameTimeMs) / 1000.0)
				CDEBUG1LN(DEBUG_SHOOTRANGE, "sDoor.fDoorSpeed: ", sDoor.fDoorSpeed)
				sDoor.fStateBias = CLAMP(fNewBias, CF_DOOR_CLOSED_BIAS, CF_DOOR_OPENED_BIAS)
				
				IF sDoor.fStateBias = CF_DOOR_OPENED_BIAS
					sDoor.eCurrentState = SDS_OPENED
				ELSE
					sDoor.eCurrentState = SDS_OPENING
				ENDIF
				bUpdatePosition = TRUE
			ENDIF
		ELSE
			//Close Sound
			IF NOT sDoor.bDoorSoundPlayed
			AND NOT IS_STRING_NULL_OR_EMPTY(sDoor.audioSoundSetDoorClose)
			AND NOT IS_STRING_NULL_OR_EMPTY(sDoor.audioSoundDoorClose)
				PLAY_SOUND_FROM_COORD(-1, sDoor.audioSoundDoorClose, GET_ENTITY_COORDS(sDoor.Entity), sDoor.audioSoundSetDoorClose)
				sDoor.bDoorSoundPlayed = TRUE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_GUNTRUCK_SLIDING_DOORS - Close Sound, Sound: ", sDoor.audioSoundDoorClose, " Stream: ", sDoor.audioSoundSetDoorClose)
			ENDIF
			
			//If Opened
			IF sDoor.fStateBias > CF_DOOR_CLOSED_BIAS
				fNewBias = sDoor.fStateBias - sDoor.fDoorSpeed * (TO_FLOAT(GET_GAME_TIMER() - sDoor.stateChangedGameTimeMs) / 1000.0)
				sDoor.fStateBias = CLAMP(fNewBias, CF_DOOR_CLOSED_BIAS, CF_DOOR_OPENED_BIAS)
				//CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_GUNTRUCK_SLIDING_DOORS - DCSD fStateBias : ", sDoorsArray[iDoorID].fStateBias, " DOOR: ", iDoorID)
				IF sDoor.fStateBias = CF_DOOR_CLOSED_BIAS
					sDoor.eCurrentState = SDS_CLOSED
				ELSE
					sDoor.eCurrentState = SDS_CLOSING
				ENDIF
				bUpdatePosition = TRUE
			ENDIF
		ENDIF
		
		IF bUpdatePosition
			SET_ENTITY_COORDS(sDoor.Entity, LERP_VECTOR(sDoor.vClosedPosition, sDoor.vOpenedPosition, sDoor.fStateBias))
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_PLAYER_DOOR_INTERACT(SLIDING_DOOR_STRUCT &sDoor, INT iDoorID, PLAYER_INDEX &piPlayer, BOOL bBroadcastEvent = FALSE)
	BOOL bOpenDoor = FALSE
	
	IF (GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(piPlayer), sDoor.vClosedPosition) <= sDoor.fInteractRadius)
		bOpenDoor = TRUE
		//Hacky for Armory Truck
		PED_INDEX pedPlayer = GET_PLAYER_PED(piPlayer)
		
		IF IS_ENTITY_PLAYING_ANIM(pedPlayer, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@", "ENTER")
		OR IS_ENTITY_PLAYING_ANIM(pedPlayer, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@", "BASE")
		OR IS_ENTITY_PLAYING_ANIM(pedPlayer, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(pedPlayer, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(pedPlayer, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(pedPlayer, "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@", "EXIT")
		
		OR IS_ENTITY_PLAYING_ANIM(pedPlayer, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@", "ENTER")
		OR IS_ENTITY_PLAYING_ANIM(pedPlayer, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@", "BASE")
		OR IS_ENTITY_PLAYING_ANIM(pedPlayer, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(pedPlayer, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(pedPlayer, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(pedPlayer, "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@", "EXIT")
			CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_GUNTRUCK_SLIDING_DOORS - Prevent Door")
			//force close door via timer.
			sDoor.fDoorOpenedTimer = sDoor.fDoorOpenedTimeOut
			bOpenDoor = FALSE
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_GUNTRUCK_SLIDING_DOORS - Allow Door")
		ENDIF
	ENDIF
	
	IF bOpenDoor
		//reset timer if palyer walks back into the locator.
		sDoor.fDoorOpenedTimer = 0
		
		IF sDoor.eDesiredState <> SDS_OPENED
			sDoor.eDesiredState = SDS_OPENED
			sDoor.stateChangedGameTimeMs = GET_GAME_TIMER()
			sDoor.bDoorSoundPlayed = FALSE
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_GUNTRUCK_SLIDING_DOORS - Closed, desire open, reset sound")
			IF NOT bBroadcastEvent
				EVENT_DOOR_STATE_CHANGED(iDoorID, sDoor.eDesiredState)
			ENDIF
			//CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_GUNTRUCK_SLIDING_DOORS - Open Door, ID: ", iDoorID)
		ENDIF
	ELSE
		IF sDoor.eCurrentState = SDS_OPENED
			IF sDoor.fDoorOpenedTimer < sDoor.fDoorOpenedTimeOut
				sDoor.fDoorOpenedTimer += GET_FRAME_TIME()
				//CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_GUNTRUCK_SLIDING_DOORS - Door Timeout: ", sDoorsArray[iDoorID].fDoorOpenedTimeOut - sDoorsArray[iDoorID].fDoorOpenedTimer)
			ELSE
				IF sDoor.eDesiredState <> SDS_CLOSED
					sDoor.eDesiredState = SDS_CLOSED
					sDoor.stateChangedGameTimeMs = GET_GAME_TIMER()
					sDoor.bDoorSoundPlayed = FALSE
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_GUNTRUCK_SLIDING_DOORS - Opened, desire close, reset sound")
					IF NOT bBroadcastEvent
						EVENT_DOOR_STATE_CHANGED(iDoorID, sDoor.eDesiredState)
					ENDIF
					//CDEBUG1LN(DEBUG_SAFEHOUSE, "HANDLE_GUNTRUCK_SLIDING_DOORS - Close Door, ID: ", iDoorID)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Update door movement when a player aproaches
/// PARAMS:
///    sDoorsArray - 
///    iActiveDoorCount - 
PROC UPDATE_SLIDING_DOORS(SLIDING_DOOR_STRUCT& sDoorsArray[], INT iActiveDoorCount, PLAYER_INDEX &piPlayer, BOOL bLocalUpdate = TRUE)
	INT iDoorID
	
	REPEAT iActiveDoorCount iDoorID
		IF NOT sDoorsArray[iDoorID].bStatic
			IF DOES_ENTITY_EXIST(sDoorsArray[iDoorID].Entity)			
				IF bLocalUpdate
					IF NOT IS_PLAYER_DEAD(piPlayer)
					AND IS_NET_PLAYER_OK(piPlayer)
						UPDATE_PLAYER_DOOR_INTERACT(sDoorsArray[iDoorID], iDoorID, piPlayer, bLocalUpdate)
					ENDIF
				ELSE
					IF NOT IS_PLAYER_DEAD(piPlayer)
					AND IS_NET_PLAYER_OK(piPlayer)
						UPDATE_PLAYER_DOOR_INTERACT(sDoorsArray[iDoorID], iDoorID, piPlayer, bLocalUpdate)		
					ENDIF
					
				ENDIF
				
				UPDATE_DOOR_SLIDE(sDoorsArray[iDoorID])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC DESTROY_SLIDING_DOORS(SLIDING_DOOR_STRUCT & sDoorsArray[], INT & iActiveDoorCount)
	INT iDoorID
	
	REPEAT COUNT_OF(sDoorsArray) iDoorID
		IF DOES_ENTITY_EXIST(sDoorsArray[iDoorID].Entity)
			DELETE_ENTITY(sDoorsArray[iDoorID].Entity)
		ENDIF
	ENDREPEAT
	
	iActiveDoorCount = 0
ENDPROC

