

//|=======================================================================================|
//|                 Author:  Brenda Carey                Date: 05/07/2011                 |
//|=======================================================================================|
//|                                                                                       |
//|                                  CNC STAT TRACKING                                    |
//|                        a) Listen to the players every move                            |
//|                                                                                       |
//|                                                                                       |
//|=======================================================================================|



USING "net_include.sch"
USING "net_stat_tracking_weapons.sch"
USING "NET_SCORING_Common.sch"

USING "net_stat_tracking_movement.sch"
USING "net_hud_displays.sch"
USING "net_rank_rewards.sch"
USING "achievement_public.sch"
USING "Cheat_handler.sch"
USING "tattoo_private.sch"
#IF IS_DEBUG_BUILD
USING "net_debug.sch"
USING "net_mission_details_overlay.sch"
USING "MP_globals_ambience_definitions.sch"
USING "NET_STAT_SYSTEM.sch"
USING "net_system_activity_feed.sch"

//USING "net_hud_debug.sch"
#ENDIF
USING "net_gang_boss_common.sch"

//CONST_INT LONG_DELAY_FOR_STAT_CHECKING 5000

//NETSTATTRACKINGSTRUCT structnetstattracking
// will have to be stored in a stuct
BOOL bdoes_player_have_wanted_level = FALSE
BOOL players_wanted_level_at_5_stars = FALSE
INT players_recorded_wanted_level_this_seasson_highest
INT players_recorded_wanted_level_this_seasson
//FLOAT iLastArrestRateAttempt
//FLOAT iLastArrestSuccess
//FLOAT iLastArrestCustody
BOOL bInitialiseValues


BOOL bsyncmk2statstomk1stat = FALSE
//INT total_cash_spent_player
//INT total_cash_earned_player

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN WORKING PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////                  MAIN PROCS                            ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



PROC TRACK_PLAYER_MISSION_LIKES()
    IF g_FMMC_HEADER_STRUCT.iMyTotalLikes > GET_MP_INT_PLAYER_STAT(MPPLY_FM_MISSION_LIKES)

		IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		AND NOT IS_TRANSITION_ACTIVE()
		
	        INT AMOUNT_TO_ADD = g_FMMC_HEADER_STRUCT.iMyTotalLikes - GET_MP_INT_PLAYER_STAT(MPPLY_FM_MISSION_LIKES) 
	        
			AMOUNT_TO_ADD = AMOUNT_TO_ADD*g_sMPTunables.iRprewardperlike
			#IF IS_DEBUG_BUILD
			   NET_PRINT("\n  TRACK_PLAYER_MISSION_LIKES: g_FMMC_HEADER_STRUCT.iMyTotalLikes =   ")
			   NET_PRINT_INT(g_FMMC_HEADER_STRUCT.iMyTotalLikes )
			   
			   NET_PRINT("\n  TRACK_PLAYER_MISSION_LIKES: MPPLY_FM_MISSION_LIKES =   ")
			   NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_FM_MISSION_LIKES))
			   
	           NET_PRINT("\n  TRACK_PLAYER_MISSION_LIKES: Given XP  ")
			   NET_PRINT_INT(AMOUNT_TO_ADD)
			   
			   NET_PRINT("\n  TRACK_PLAYER_MISSION_LIKES: Multiplier g_sMPTunables.iRprewardperlike  ")
			   NET_PRINT_INT(g_sMPTunables.iRprewardperlike)
			   
			   NET_PRINT("\n  TRACK_PLAYER_MISSION_LIKES: CAP g_sMPTunables.iRprewardtotallikecap  ")
			   NET_PRINT_INT(g_sMPTunables.iRprewardtotallikecap)
			   
			   
			   NET_PRINT("\n  TRACK_PLAYER_MISSION_LIKES: CAP  g_sMPTunables.iRprewardperlikecap   ")
			   NET_PRINT_INT( g_sMPTunables.iRprewardperlikecap )
			   
			  
	       #ENDIF
		   
		   IF g_sMPTunables.iRprewardtotallikecap < AMOUNT_TO_ADD
		   
		   		AMOUNT_TO_ADD = g_sMPTunables.iRprewardtotallikecap
			 	NET_PRINT("\n  TRACK_PLAYER_MISSION_LIKES: AMOUNT TO ADD is greater than cap per award. ROUNDING  ")
			
		   ENDIF

		//	IF GET_MP_INT_PLAYER_STAT(MPPLY_FM_MISSION_LIKES) >= 0
			IF AMOUNT_TO_ADD > 0
			AND g_sMPTunables.iRprewardperlike > 0
				IF AMOUNT_TO_ADD < g_sMPTunables.iRprewardperlikecap //temp cap 					// if amount to add is less that RP award cap
				AND g_FMMC_HEADER_STRUCT.iMyTotalLikes < g_sMPTunables.iRprewardperlikecap //temp cap // if total likes is less that RP award cap
				    GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_2", XPTYPE_SOCIALCLUB, XPCATEGORY_SOCIALCLUB_MISSION_LIKES, AMOUNT_TO_ADD ) // 1 xp for every like
					PRINT_TICKER_WITH_INT("REWLIKESXP", AMOUNT_TO_ADD)
					INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_FM_MISSION_LIKES, AMOUNT_TO_ADD)
				ELSE
					NET_PRINT("\n  TRACK_PLAYER_MISSION_LIKES: gone over cap or mission likes is not initialised\n ")
					
					// if player receives RP  and it goes over total cap, round off the RP award to cap amount
					IF g_FMMC_HEADER_STRUCT.iMyTotalLikes < g_sMPTunables.iRprewardperlikecap 
						IF AMOUNT_TO_ADD +g_FMMC_HEADER_STRUCT.iMyTotalLikes> g_sMPTunables.iRprewardperlikecap //temp cap 
					
							AMOUNT_TO_ADD = g_FMMC_HEADER_STRUCT.iMyTotalLikes - GET_MP_INT_PLAYER_STAT(MPPLY_FM_MISSION_LIKES)*g_sMPTunables.iRprewardperlike
							
							IF AMOUNT_TO_ADD >  g_sMPTunables.iRprewardperlikecap
						   		AMOUNT_TO_ADD = g_sMPTunables.iRprewardperlikecap
								NET_PRINT("\n  TRACK_PLAYER_MISSION_LIKES: Rounding Amount to give to CAp as amount to give is greater   ")
								NET_PRINT_INT(AMOUNT_TO_ADD)
						   	ENDIF
							
							GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_2", XPTYPE_SOCIALCLUB, XPCATEGORY_SOCIALCLUB_MISSION_LIKES, AMOUNT_TO_ADD ) // 1 xp for every like
							PRINT_TICKER_WITH_INT("REWLIKESXP", AMOUNT_TO_ADD)
							INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_FM_MISSION_LIKES, AMOUNT_TO_ADD)
							NET_PRINT("\n  TRACK_PLAYER_MISSION_LIKES: player receives RP  and it goes over total cap, round off the RP award to cap amount Given XP  ")
							NET_PRINT_INT(AMOUNT_TO_ADD)
							
						ELSE
						// if amount to add plus total likes > total cap, round amount to give so amount to add +total likes = cap
							IF AMOUNT_TO_ADD +g_FMMC_HEADER_STRUCT.iMyTotalLikes<= g_sMPTunables.iRprewardperlikecap //temp cap 
								AMOUNT_TO_ADD =  g_sMPTunables.iRprewardperlikecap - g_FMMC_HEADER_STRUCT.iMyTotalLikes
								GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_2", XPTYPE_SOCIALCLUB, XPCATEGORY_SOCIALCLUB_MISSION_LIKES, AMOUNT_TO_ADD ) // 1 xp for every like
								PRINT_TICKER_WITH_INT("REWLIKESXP", AMOUNT_TO_ADD)
								INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_FM_MISSION_LIKES, AMOUNT_TO_ADD)
								NET_PRINT("\n  TRACK_PLAYER_MISSION_LIKES: player receives RP  and it goes over total cap, round off the RP award to cap amount Given XP  ")
								NET_PRINT_INT(AMOUNT_TO_ADD)
								
							ENDIF
						
						ENDIF
					ENDIF
					
				ENDIF
			
			ENDIF
		   // SET_MP_INT_PLAYER_STAT(MPPLY_FM_MISSION_LIKES, g_FMMC_HEADER_STRUCT.iMyTotalLikes )
		   
			
			#IF IS_DEBUG_BUILD
	           NET_PRINT("\n  TRACK_PLAYER_MISSION_LIKES: Given XP \n ")
			   NET_PRINT_INT(AMOUNT_TO_ADD)
			   NET_PRINT("\n  TRACK_PLAYER_MISSION_LIKES: g_FMMC_HEADER_STRUCT.iMyTotalLikes = ")NET_PRINT_INT(g_FMMC_HEADER_STRUCT.iMyTotalLikes )
	      	   NET_PRINT("\n  TRACK_PLAYER_MISSION_LIKES: GET_MP_INT_PLAYER_STAT(MPPLY_FM_MISSION_LIKES)  = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_FM_MISSION_LIKES)  )

		   #ENDIF
	    
	    ENDIF
	
	ENDIF
ENDPROC
PROC INIT_STAT_VALUES()

    IF bInitialiseValues = FALSE
//
//      iLastArrestRateAttempt = TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_ARRESTATTEMPTS))
//      iLastArrestSuccess = TO_FLOAT((GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_ARRESTSUCCESS)))
//      iLastArrestCustody = TO_FLOAT(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_CUSTODY_CUFFED_CROOK))
//      
       IF ARE_ALL_INSTANCED_TUTORIALS_COMPLETE() 
           TRACK_PLAYER_MISSION_LIKES()
       ENDIF
        bInitialiseValues = TRUE
    ENDIF

ENDPROC


PROC INIT_STAT_TIMERS()
    
    INT I
//  FOR I = 0 TO MAX_NUM_STAT_TIMERS-1
//      MPGlobalsStats.StatTrackingTimer[I] = -1
//  ENDFOR
//  MPGlobalsStats.StatTrackingMain = -1

    FOR i = 0 to MAX_NUM_STAT_TIMERS-1
        MPGlobalsStats.LastStatTimer[i] = GET_NETWORK_TIME()    
    ENDFOR
    FOR i = 0 to MAX_NUM_AWARD_CHARFLOAT_TIMERS-1
        MPGlobalsStats.LastAward_CharFloat_Timer[i]= GET_NETWORK_TIME() 
    ENDFOR
    FOR i = 0 to MAX_NUM_AWARD_PLYFLOAT_TIMERS-1
        MPGlobalsStats.LastAward_PlyFloat_Timer[i] = GET_NETWORK_TIME() 
    ENDFOR
    FOR i = 0 to MAX_NUM_AWARD_CHARINT_TIMERS-1
        MPGlobalsStats.LastAward_CharInt_Timer[i] = GET_NETWORK_TIME()  
    ENDFOR
    FOR i = 0 to MAX_NUM_AWARD_TIMERS-1
        MPGlobalsStats.LastAwardTimer[i] = GET_NETWORK_TIME()   
    ENDFOR


    
    number_kills_this_session_for_revenge_tracking = 0
    
    
    //total_cash_spent_player = GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_TOTAL_SPENT)
    //total_cash_earned_player = GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_CASH_EARNED)
ENDPROC




//PROC TRACK_BOOLCHAR_AWARDS(MP_BOOL_AWARD  award, BOOL GiveGeneralAward = TRUE)
//      BOOL Value = GET_MP_BOOL_CHARACTER_AWARD(award)
//      IF MPGlobalsStats.AWARD_INIT_BOOLCHAR[award] = FALSE
//          IF Value = TRUE
//              MPGlobalsStats.AWARD_INIT_BOOLCHAR[award] = TRUE
//              IF GiveGeneralAward = TRUE
//                  SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_AWARD, AWARD_GET_NAME_BOOLCHAR(award) , GET_AWARD_TEXTURE_NAME_BOOLCHAR(award), GET_AWARD_TEXTURE_DICT_BOOLCHAR(award))                      
//              ENDIF
//          ENDIF
//      ENDIF
//ENDPROC
//
//
//PROC TRACK_FLOAT_CHAR_MISSION_PERCENTAGE_INCREMENT(MP_FLOAT_AWARD anAward, FLOAT Denominator, BOOL display_zero_percent_progress, BOOL DisplayGeneralAwardOnComplete = FALSE) //
//
//      IF MPGlobalsStats.AWARD_INIT_FLOATCHAR[anAward] = FALSE
//      
//          IF IS_FLOAT_AWARD_PASSED_VALUE(GET_MP_FLOAT_CHARACTER_AWARD(anAward), 1)
//              MPGlobalsStats.AWARD_DISPLAYED_FLOATCHAR[anAward]=0
//              
//          ENDIF
//          
//          IF IS_FLOAT_AWARD_PASSED_VALUE(GET_MP_FLOAT_CHARACTER_AWARD(anAward), (Denominator*25.0)/100.0) // 25 percent?
//              MPGlobalsStats.AWARD_DISPLAYED_FLOATCHAR[anAward]=1
//              //PRINTNL()
//              //PRINTSTRING("25%: ")
//              //PRINTNL()
//          ENDIF
//          IF IS_FLOAT_AWARD_PASSED_VALUE(GET_MP_FLOAT_CHARACTER_AWARD(anAward), (Denominator*50.0)/100.0) // 50 percent?
//              MPGlobalsStats.AWARD_DISPLAYED_FLOATCHAR[anAward]=2
//              //PRINTNL()
//          //  PRINTSTRING("50 percent: ")
//              //PRINTNL()
//          ENDIF
//          
//          IF IS_FLOAT_AWARD_PASSED_VALUE(GET_MP_FLOAT_CHARACTER_AWARD(anAward), ((Denominator*75.0)/100.0)) // 75 percent?
//              MPGlobalsStats.AWARD_DISPLAYED_FLOATCHAR[anAward]=3
//      //      PRINTNL()
//          //  PRINTSTRING("100 percent AWARD GIVEN ")
//              //PRINTNL()
//          ENDIF
//          
//          
//          
//          IF IS_FLOAT_AWARD_PASSED_VALUE(GET_MP_FLOAT_CHARACTER_AWARD(anAward), Denominator) // 100 percent
//              MPGlobalsStats.AWARD_DISPLAYED_FLOATCHAR[anAward]=4
//          ENDIF
//          MPGlobalsStats.AWARD_INIT_FLOATCHAR[anAward] = TRUE
//          
//          
//      ENDIF
//      
//      SWITCH MPGlobalsStats.AWARD_DISPLAYED_FLOATCHAR[ENUM_TO_INT(anAward)]
//      
//          CASE 0 
//                  
//
//              IF IS_FLOAT_AWARD_PASSED_VALUE(GET_MP_FLOAT_CHARACTER_AWARD(anAward), 1)
//                  IF display_zero_percent_progress = TRUE
//                      PRINT_TICKER_WITH_STRING_AND_INT(eTICKER_AWARD_PROGRESS, "AWARD_PROG_PC", AWARD_GET_NAME_FLOATCHAR(anAward) ,0, 
//                                                      GET_AWARD_TEXTURE_DICT_FLOATCHAR(anAward),  GET_AWARD_TEXTURE_NAME_FLOATCHAR(anAward))
//                  ENDIF
//                  MPGlobalsStats.AWARD_DISPLAYED_FLOATCHAR[ENUM_TO_INT(anAward)] = 1
//                  
//              ENDIF
//          BREAK
//          CASE 1
//          
//      
//              IF IS_FLOAT_AWARD_PASSED_VALUE(GET_MP_FLOAT_CHARACTER_AWARD(anAward), ((Denominator*25)/100))
//                  PRINT_TICKER_WITH_STRING_AND_INT(eTICKER_AWARD_PROGRESS, "AWARD_PROG_PC", AWARD_GET_NAME_FLOATCHAR(anAward) ,25, 
//                                                      GET_AWARD_TEXTURE_DICT_FLOATCHAR(anAward),  GET_AWARD_TEXTURE_NAME_FLOATCHAR(anAward))
//                  MPGlobalsStats.AWARD_DISPLAYED_FLOATCHAR[ENUM_TO_INT(anAward)] = 2  
//              ENDIF
//          
//          BREAK
//          CASE 2
//          
//
//              IF IS_FLOAT_AWARD_PASSED_VALUE(GET_MP_FLOAT_CHARACTER_AWARD(anAward), ((Denominator*50)/100))
//                  PRINT_TICKER_WITH_STRING_AND_INT(eTICKER_AWARD_PROGRESS, "AWARD_PROG_PC",  AWARD_GET_NAME_FLOATCHAR(anAward), 50,
//                                                      GET_AWARD_TEXTURE_DICT_FLOATCHAR(anAward),  GET_AWARD_TEXTURE_NAME_FLOATCHAR(anAward))
//                  MPGlobalsStats.AWARD_DISPLAYED_FLOATCHAR[ENUM_TO_INT(anAward)] = 3
//              ENDIF
//              
//          BREAK
//          CASE 3
//          
//
//              IF IS_FLOAT_AWARD_PASSED_VALUE(GET_MP_FLOAT_CHARACTER_AWARD(anAward), ((Denominator*75)/100))
//                  PRINT_TICKER_WITH_STRING_AND_INT(eTICKER_AWARD_PROGRESS, "AWARD_PROG_PC",  AWARD_GET_NAME_FLOATCHAR(anAward), 75,
//                                   GET_AWARD_TEXTURE_DICT_FLOATCHAR(anAward),  GET_AWARD_TEXTURE_NAME_FLOATCHAR(anAward))
//                  MPGlobalsStats.AWARD_DISPLAYED_FLOATCHAR[ENUM_TO_INT(anAward)] = 4
//              ENDIF
//      
//          BREAK
//          CASE 4
//
//              //
//              //[AWARD_PROG_PC]
//              //AWARD - ~a~ ~n~PROGRESS ~1~%
//              IF IS_FLOAT_AWARD_PASSED_VALUE(GET_MP_FLOAT_CHARACTER_AWARD(anAward), Denominator)
//                  IF DisplayGeneralAwardOnComplete
//                      SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_AWARD, AWARD_GET_NAME_FLOATCHAR(anaward) , GET_AWARD_TEXTURE_NAME_FLOATCHAR(anaward), GET_AWARD_TEXTURE_DICT_FLOATCHAR(anaward))              
//                  ENDIF
//                  MPGlobalsStats.AWARD_DISPLAYED_FLOATCHAR[ENUM_TO_INT(anAward)] = 5
//                  
//                  
//              ENDIF
//          
//          BREAK
//          
//          CASE 5
//          
//          
//          
//          
//          BREAK
//          
//      
//
//      ENDSWITCH
//      
//  
//  
//ENDPROC
//
//
//PROC TRACK_INTCHAR_AWARD_PERCENTAGE(MP_INT_AWARD anAward, INT Denominator, BOOL display_zero_percent_progress, BOOL DisplayGeneralAwardOnComplete = TRUE)
//      /*
//      INT Value = GET_MP_INT_CHARACTER_AWARD(anAward)
//              PRINTNL()
//              PRINTSTRING("start checking percentage check: ")
//              PRINTNL()
//              PRINTSTRING("VALUE: ")
//              PRINTINT(Value)
//              PRINTNL()
//              PRINTSTRING("DENOMINATOR: ")
//              PRINTINT(Denominator)
//              PRINTNL()       */
//      IF MPGlobalsStats.AWARD_INIT_INTCHAR[anAward] = FALSE
//      
//          IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), 1)
//              MPGlobalsStats.AWARD_DISPLAYED_INTCHAR[anAward]=0
//          ENDIF
//          
//          IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), (Denominator*25)/100) // 25 percent?
//              MPGlobalsStats.AWARD_DISPLAYED_INTCHAR[anAward]=1
//              //PRINTNL()
//              //PRINTSTRING("25%: ")
//              //PRINTNL()
//          ENDIF
//          IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), (Denominator*50)/100) // 50 percent?
//              MPGlobalsStats.AWARD_DISPLAYED_INTCHAR[anAward]=2
//              //PRINTNL()
//          //  PRINTSTRING("50 percent: ")
//              //PRINTNL()
//          ENDIF
//          
//          IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), ((Denominator*75)/100)) // 75 percent?
//              MPGlobalsStats.AWARD_DISPLAYED_INTCHAR[anAward]=3
//      //      PRINTNL()
//          //  PRINTSTRING("100 percent AWARD GIVEN ")
//              //PRINTNL()
//          ENDIF
//          
//          
//          
//          IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), Denominator) // 100 percent
//              MPGlobalsStats.AWARD_DISPLAYED_INTCHAR[anAward]=4
//          ENDIF
//          MPGlobalsStats.AWARD_INIT_INTCHAR[anAward] = TRUE
//          
//          
//      ENDIF
//      
//      SWITCH MPGlobalsStats.AWARD_DISPLAYED_INTCHAR[ENUM_TO_INT(anAward)]
//      
//          CASE 0 
//                  
//
//              IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), 1)
//                  IF display_zero_percent_progress = TRUE
//                      PRINT_TICKER_WITH_STRING_AND_INT(eTICKER_AWARD_PROGRESS, "AWARD_PROG_PC", AWARD_GET_NAME_INTCHAR(anAward) ,0, 
//                                                      GET_AWARD_TEXTURE_DICT_INTCHAR(anAward),  GET_AWARD_TEXTURE_NAME_INTCHAR(anAward))
//                  ENDIF
//                  MPGlobalsStats.AWARD_DISPLAYED_INTCHAR[ENUM_TO_INT(anAward)] = 1
//                  
//              ENDIF
//          BREAK
//          CASE 1
//          
//      
//              IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), ((Denominator*25)/100))
//                  PRINT_TICKER_WITH_STRING_AND_INT(eTICKER_AWARD_PROGRESS, "AWARD_PROG_PC", AWARD_GET_NAME_INTCHAR(anAward) ,25, 
//                                                      GET_AWARD_TEXTURE_DICT_INTCHAR(anAward),  GET_AWARD_TEXTURE_NAME_INTCHAR(anAward))
//                  MPGlobalsStats.AWARD_DISPLAYED_INTCHAR[ENUM_TO_INT(anAward)] = 2    
//              ENDIF
//          
//          BREAK
//          CASE 2
//          
//
//              IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), ((Denominator*50)/100))
//                  PRINT_TICKER_WITH_STRING_AND_INT(eTICKER_AWARD_PROGRESS, "AWARD_PROG_PC",  AWARD_GET_NAME_INTCHAR(anAward), 50,
//                                                      GET_AWARD_TEXTURE_DICT_INTCHAR(anAward),  GET_AWARD_TEXTURE_NAME_INTCHAR(anAward))
//                  MPGlobalsStats.AWARD_DISPLAYED_INTCHAR[ENUM_TO_INT(anAward)] = 3
//              ENDIF
//              
//          BREAK
//          CASE 3
//          
//
//              IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), ((Denominator*75)/100))
//                  PRINT_TICKER_WITH_STRING_AND_INT(eTICKER_AWARD_PROGRESS, "AWARD_PROG_PC",  AWARD_GET_NAME_INTCHAR(anAward), 75,
//                                   GET_AWARD_TEXTURE_DICT_INTCHAR(anAward),  GET_AWARD_TEXTURE_NAME_INTCHAR(anAward))
//                  MPGlobalsStats.AWARD_DISPLAYED_INTCHAR[ENUM_TO_INT(anAward)] = 4
//              ENDIF
//      
//          BREAK
//          CASE 4
//
//              //
//              //[AWARD_PROG_PC]
//              //AWARD - ~a~ ~n~PROGRESS ~1~%
//              IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), Denominator)
//                  IF DisplayGeneralAwardOnComplete
//                      SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_AWARD, AWARD_GET_NAME_INTCHAR(anaward) , GET_AWARD_TEXTURE_NAME_INTCHAR(anaward), GET_AWARD_TEXTURE_DICT_INTCHAR(anaward))                   
//                  ENDIF
//                  MPGlobalsStats.AWARD_DISPLAYED_INTCHAR[ENUM_TO_INT(anAward)] = 5
//                  
//                  
//                  
//              ENDIF
//          
//          BREAK
//          
//          CASE 5
//          
//          
//          
//          
//          BREAK
//          
//      
//
//      ENDSWITCH
//              
//ENDPROC
/*
PROC TRANSFER_VIRTUAL_CASH_TRANSFER_TO_BANK_ACCOUNT()
    #IF IS_DEBUG_BUILD
        NET_PRINT("\n TRANSFER_VIRTUAL_CASH_TRANSFER_TO_BANK_ACCOUNT BEFORE VIRTUAL CASH = ")
        NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_STORE_MONEY_BOUGHT))
        NET_PRINT("\n TRANSFER_VIRTUAL_CASH_TRANSFER_TO_BANK_ACCOUNT BEFORE BANK BALANCE = ")
        NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_BANK_ACCOUNT))
        NET_PRINT("\n TRANSFER_VIRTUAL_CASH_TRANSFER_TO_BANK_ACCOUNT BEFORE SCRIPT TRANSFER AMOUNT = ")
        NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_STORE_BOUGHT_SCRIPT))

    
    #ENDIF
    
    //GET_MP_INT_PLAYER_STAT(MPPLY_VIRTUAL_CURRENCY_BOUGHT)
    
    INT amount_to_send_to_bank = GET_MP_INT_PLAYER_STAT(MPPLY_STORE_MONEY_BOUGHT) 
    amount_to_send_to_bank -=GET_MP_INT_PLAYER_STAT(MPPLY_STORE_BOUGHT_SCRIPT)
    
    INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_BANK_ACCOUNT, amount_to_send_to_bank )
    
    INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_STORE_BOUGHT_SCRIPT,amount_to_send_to_bank )

    #IF IS_DEBUG_BUILD
        NET_PRINT("\n TRANSFER_VIRTUAL_CASH_TRANSFER_TO_BANK_ACCOUNT AFTER VIRTUAL CASH = ")
        NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_STORE_MONEY_BOUGHT))
        NET_PRINT("\n TRANSFER_VIRTUAL_CASH_TRANSFER_TO_BANK_ACCOUNT AFTER BANK BALANCE = ")
        NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_BANK_ACCOUNT))
        
        NET_PRINT("\n TRANSFER_VIRTUAL_CASH_TRANSFER_TO_BANK_ACCOUNT AFTER SCRIPT TRANSFER AMOUNT = ")
        NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_STORE_BOUGHT_SCRIPT))
    #ENDIF
            
ENDPROC*/


/*
PROC TRANSFER_VIRTUAL_CASH_TO_INGAME_CASH()
    #IF IS_DEBUG_BUILD
        NET_PRINT("\n TRANSFER_VIRTUAL_CASH_TO_INGAME_CASH BEFORE VIRTUAL CASH = ")
        NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_STORE_MONEY_BOUGHT))
        NET_PRINT("\n TRANSFER_VIRTUAL_CASH_TO_INGAME_CASH BEFORE BANK BALANCE = ")
        NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_BANK_ACCOUNT))
        NET_PRINT("\n TRANSFER_VIRTUAL_CASH_TO_INGAME_CASH BEFORE SCRIPT TRANSFER AMOUNT = ")
        NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_STORE_BOUGHT_SCRIPT))
    
    #ENDIF
    
    //GET_MP_INT_PLAYER_STAT(MPPLY_VIRTUAL_CURRENCY_BOUGHT)
    
    

    
    GIVE_LOCAL_PLAYER_CASH(GET_MP_INT_PLAYER_STAT(MPPLY_BANK_ACCOUNT))
    INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_STORE_SPENT_SCRIPT, GET_MP_INT_PLAYER_STAT(MPPLY_BANK_ACCOUNT) )

    SET_MP_INT_PLAYER_STAT(MPPLY_BANK_ACCOUNT,0) 
    STAT_SAVE()
    #IF IS_DEBUG_BUILD
        NET_PRINT("\nTRANSFER_VIRTUAL_CASH_TO_INGAME_CASH AFTER VIRTUAL CASH = ")
        NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_STORE_MONEY_BOUGHT))
        NET_PRINT("\n TRANSFER_VIRTUAL_CASH_TO_INGAME_CASH AFTER BANK BALANCE = ")
        NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_BANK_ACCOUNT))
        NET_PRINT("\n TRANSFER_VIRTUAL_CASH_TO_INGAME_CASH AFTER SCRIPT TRANSFER AMOUNT = ")
        NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_STORE_BOUGHT_SCRIPT))
    
    #ENDIF
            
ENDPROC
*/
//
//PROC TRACK_ARREST_RATE()
//
//  FLOAT ArrestRate = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_CHAR_ARRESTRATE)
//  
//  FLOAT SuccessRate = TO_FLOAT((GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_ARRESTSUCCESS)))
//  FLOAT AttemptRate = TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_ARRESTATTEMPTS))
//  FLOAT Custody = TO_FLOAT(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_CUSTODY_CUFFED_CROOK))
//
//  IF AttemptRate > 0
//      IF AttemptRate %10 = 0 //For every 10 arrest attempts check out the ratio
//          IF iLastArrestRateAttempt != AttemptRate
//
//              
//              //For Every 10 arrest attempts 
//              //Successes are divided by Attempts and timed by 10 to increase the value to a units level
//              //The Divider is the threshold when an amount is increased or decreased. 
//              //The Divider starts at 35% but then moves up when you get better and moves down back to 35% when you get a worse rating.
//              //Custody successes will increase the success count by 20%. This might be too high, depends on how easy fully completing an arrest is. 
//              //The arrest change is then the result of the Success/Arrest*10 minus the divider.
//              //This is too high so this values is halved before being added
//              
//              //Arrest Rate
//              //Arresting Rate += Arrest Successes*Custody completed/Arrest Attempts. 
//              //Worked out every 10 arrest attempts with a snapshot of those 10 only. 
//              //Max increases +3.25% (Custody = 0) +4.25% (Custody = 10), Max decrease -1.75%. 
//
//              
//              AttemptRate = 10
//              SuccessRate -= iLastArrestSuccess
//              Custody -= iLastArrestCustody
//              IF SuccessRate < 0
//                  SuccessRate = 0.0
//              ENDIF
//              
//              SuccessRate += (Custody*0.2)
//              FLOAT Result = ((SuccessRate/AttemptRate)*10.0)
//              FLOAT Divider = 3.5 //35% anything above will be positive addon, negative if under this rate. 
//
//              Divider += ((ArrestRate/10)/5) //Makes it harder to increase the higher you go. 
//              
//              FLOAT ArrestRateChange = Result-Divider
//          
//              ArrestRateChange /= 2
//          
//              ArrestRate += ArrestRateChange
//              
//              iLastArrestRateAttempt = TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_ARRESTATTEMPTS))
//              iLastArrestSuccess = TO_FLOAT((GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_ARRESTSUCCESS)))
//              iLastArrestCustody = TO_FLOAT(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_CUSTODY_CUFFED_CROOK))
//              
//              //INCREMENT_PLAYER_SKILL_FLOAT(PLAYER_SKILL_ARRESTRATE, ArrestRateChange, TRUE, "ARRSTSKLU")
//
//              
//              #IF IS_DEBUG_BUILD
//              NET_NL()NET_PRINT("<<<BC: ARREST RATE CHANGE: ")
//              NET_NL()NET_PRINT("Result = ")NET_PRINT_FLOAT(Result)
//              NET_NL()NET_PRINT("Divider = ")NET_PRINT_FLOAT(Divider)
//              NET_NL()NET_PRINT("SuccessRate = ")NET_PRINT_FLOAT(SuccessRate)
//              NET_NL()NET_PRINT("AttemptRate = ")NET_PRINT_FLOAT(AttemptRate)
//              NET_NL()NET_PRINT("Custody = ")NET_PRINT_FLOAT(Custody)
//              NET_NL()NET_PRINT("ArrestRateChange = ")NET_PRINT_FLOAT(ArrestRateChange)
//              NET_NL()NET_PRINT("NEW ArrestRate = ")NET_PRINT_FLOAT(ArrestRate)
//              NET_NL()NET_PRINT("NEW iLastArrestRateAttempt = ")NET_PRINT_FLOAT(iLastArrestRateAttempt)NET_NL()
//              #ENDIF
//          ENDIF
//      ENDIF
//
////        #IF IS_DEBUG_BUILD
////        NET_NL()NET_PRINT("<<<BC: ARREST RATE CHANGE EVERY FRAME: ")
////        NET_NL()NET_PRINT("SuccessRate = ")NET_PRINT_FLOAT(SuccessRate)
////        NET_NL()NET_PRINT("AttemptRate = ")NET_PRINT_FLOAT(AttemptRate)
////        NET_NL()NET_PRINT("Custody = ")NET_PRINT_FLOAT(Custody)
////        NET_NL()NET_PRINT("NEW ArrestRate = ")NET_PRINT_FLOAT(ArrestRate)
////        NET_NL()NET_PRINT("NEW iLastArrestRateAttempt = ")NET_PRINT_FLOAT(iLastArrestRateAttempt)NET_NL()
////
////        #ENDIF
//      
////        IF ArrestRate > 100.0
////            ArrestRate = 100.0
////        ENDIF
////        IF ArrestRate < 0
////            ArrestRate = 0
////        ENDIF
//      
//      
////        SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_CHAR_ARRESTRATE, ArrestRate)
////        IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
////            GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerBroadcastStats.fArrestRateSkill = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_CHAR_ARRESTRATE)
////        ENDIF
//  ENDIF
//
//
//ENDPROC


PROC RUN_LOBBY_AND_LOADING_TRACKERS()
	
	
		INT WhatState = 0
	
	    INT current_time_SP = (GET_GAME_TIMER() - LoadingAndLobbySPTimerTrackerLastFrame)
		LoadingAndLobbySPTimerTrackerLastFrame = GET_GAME_TIMER()

		IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		

		
			IF IS_PLAYER_IN_CORONA()
				IF IS_THIS_PLAYER_FULLY_ACTIVE_IN_CORONA(PLAYER_ID())
				OR GET_CORONA_STATUS() = CORONA_STATUS_JOIN_HOST 
					//MPPLY_TOTAL_TIME_IN_LOBBY++
//					NET_NL()NET_PRINT("RUN_LOBBY_AND_LOADING_TRACKERS: player active in corona current_time = ")NET_PRINT_INT(current_time)
//					INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TIME_IN_LOBBY,current_time )
					
					IF WhatState = 0
						WhatState = 2
					ENDIF
					
					
				ELSE
//					NET_NL()NET_PRINT("RUN_LOBBY_AND_LOADING_TRACKERS: player loading corona current_time = ")NET_PRINT_INT(current_time)
			
					//Loading parts. MPPLY_TOTAL_TIME_LOAD_SCREEN++
//					 INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TIME_LOAD_SCREEN,current_time )

					IF WhatState = 0
						WhatState = 1
					ENDIF

				ENDIF
			ENDIF
			
		
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
			AND NOT IS_TRANSITION_ACTIVE()
			AND NOT IS_PLAYER_IN_CORONA()
			AND NOT ON_NEW_JOB_VOTE_SCREEN()
			AND NOT g_b_OnLeaderboard
			AND NOT IS_COMMERCE_STORE_OPEN()
			
//				NET_NL()NET_PRINT("RUN_LOBBY_AND_LOADING_TRACKERS: player loading not in a corona current_time = ")NET_PRINT_INT(current_time)
		
				//MPPLY_TOTAL_TIME_LOAD_SCREEN++
//				INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TIME_LOAD_SCREEN,current_time )
				
				IF WhatState = 0
					WhatState = 1
				ENDIF
				
			ENDIF
		ENDIF
		
		
		IF IS_TRANSITION_ACTIVE()
		AND GET_PAUSE_MENU_WARP_FROM_GAMEMODE() = GAMEMODE_FM
		AND GET_PAUSE_MENU_WARP_TO_GAMEMODE() = GAMEMODE_FM
			
			IF WhatState = 0
				WhatState = 3
			ENDIF
		ENDIF
		
		IF ON_NEW_JOB_VOTE_SCREEN()
		OR g_b_OnLeaderboard
			IF WhatState = 0
				WhatState = 4
			ENDIF 
		ENDIF
		
		
		IF HAS_IMPORTANT_STATS_LOADED()

		
			SWITCH WhatState
			
				CASE 0
				
				BREAK
				
				CASE 1
					INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TIME_LOAD_SCREEN,current_time_SP )
				BREAK
				
				CASE 2
				
					INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TIME_IN_LOBBY,current_time_SP )
				
				BREAK
				
				CASE 3
					INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TIME_SESS_SWAP,current_time_SP )
				BREAK
				
				CASE 4
					INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TIME_ENDJOB,current_time_SP )
				BREAK
				
				
			
			ENDSWITCH
		
		ENDIF
		


ENDPROC


FUNC BOOL DID_I_CREATE_THIS_MISSION(BOOL &bSuccess)
    STRING stUserName
    IF (IS_XBOX360_VERSION() OR IS_XBOX_PLATFORM())
		stUserName = "TEMP" //temp fix until we can get a command that gets an XBOX gamer tag #943909
        //stUserName = NETWORK_GET_XUID_FROM_GAMERTAG(GET_PLAYER_NAME(PLAYER_ID()))
		IF NOT IS_STRING_NULL_OR_EMPTY(stUserName)
			IF ARE_STRINGS_EQUAL(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl23CurrentMissionOwner, stUserName)
				bSuccess = TRUE
				#IF IS_DEBUG_BUILD
					IF IS_XBOX360_VERSION()
						NET_PRINT("\n  DID_I_CREATE_THIS_MISSION(): TRUE (XBOX 360)\n ")
					ELIF IS_XBOX_PLATFORM()
						NET_PRINT("\n  DID_I_CREATE_THIS_MISSION(): TRUE (XBOX ONE)\n ")
					ENDIF
                #ENDIF
                RETURN TRUE
            ENDIF
		ENDIF
	ELIF (IS_PS3_VERSION() OR IS_PLAYSTATION_PLATFORM() OR IS_PC_VERSION())
        stUserName = GET_PLAYER_NAME(PLAYER_ID())
        IF NOT IS_STRING_NULL_OR_EMPTY(stUserName)
            IF ARE_STRINGS_EQUAL(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].tl23CurrentMissionOwner, stUserName)
                bSuccess = TRUE
                #IF IS_DEBUG_BUILD
					IF IS_PS3_VERSION()
                    	NET_PRINT("\n  DID_I_CREATE_THIS_MISSION(): TRUE (PS3) \n ")
					ELIF IS_PLAYSTATION_PLATFORM()
						NET_PRINT("\n  DID_I_CREATE_THIS_MISSION(): TRUE (PS4) \n ")
					ELIF IS_PC_VERSION()
						NET_PRINT("\n  DID_I_CREATE_THIS_MISSION(): TRUE (PC) \n ")
					ENDIF
                #ENDIF
                RETURN TRUE
            ENDIF
        ENDIF
    ENDIF
    
    bSuccess = FALSE
    RETURN FALSE
ENDFUNC

FUNC BOOL REWARD_XP_FOR_PLAYING_OWN_CONTENT_AGAINST_OTHERS(INT current_no_mission_participants)

    BOOL DID_PLAYER_MAKE_THIS_MISSION
    DID_I_CREATE_THIS_MISSION(DID_PLAYER_MAKE_THIS_MISSION)
    
    IF DID_PLAYER_MAKE_THIS_MISSION = TRUE
        IF current_no_mission_participants >= 2
            #IF IS_DEBUG_BUILD
                NET_PRINT("\n  REWARD_XP_FOR_PLAYING_OWN_CONTENT_AGAINST_OTHERS TRUE Giving 500xp reward \n ")
            #ENDIF
           // GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXPOWNMISS", XPTYPE_SOCIALCLUB, XPCATEGORY_SOCIALCLUB_PLAYED_OWN_CONTENT_AGAINST_OTHERS, 500*ROUND(g_sMPTunables.fxp_tunable_Play_your_creations_against_other_players) , 1)
            //PRINT_TICKER_WITH_INT("REWXPOWNMISS", 500)
            RETURN TRUE
            
        ENDIF
    ENDIF
    
    RETURN FALSE

ENDFUNC

FUNC BOOL IS_THIS_A_ROCKSTAR_ACTIVITY()
    INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
    IF GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idCreator = FMMC_ROCKSTAR_CREATOR_ID
    OR GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idCreator = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
    OR GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idCreator = FMMC_ROCKSTAR_HIDEOUT_CREATOR_ID
            #IF IS_DEBUG_BUILD
                NET_PRINT("\n  IS_THIS_A_ROCKSTAR_ACTIVITY(): TRUE \n ")
            #ENDIF

        RETURN TRUE


    ENDIF
        #IF IS_DEBUG_BUILD
            NET_PRINT("\n  IS_THIS_A_ROCKSTAR_ACTIVITY(): FALSE \n ")
        #ENDIF
    
    RETURN FALSE    
ENDFUNC


PROC TRACK_DRIVE_WITHOUT_CRASHING_AWARD(INT DelayTime)
    // HAS_ENTITY_COLLIDED_WITH_ANYTHING
    //MP_AWARD_FMDRIVEWITHOUTCRASH
    
    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
    AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
            VEHICLE_INDEX temp_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
            INT TEMP_VEHICLE_HEALTH = GET_ENTITY_HEALTH(temp_vehicle)
            FLOAT temp_vehicle_speed = GET_ENTITY_SPEED(temp_vehicle)
            IF IS_PLAYER_IN_VEH_SEAT(player_ID(), VS_DRIVER)
            AND NOT HAS_ENTITY_COLLIDED_WITH_ANYTHING(temp_vehicle)
            AND MPGlobalsStats.veh_damage = TEMP_VEHICLE_HEALTH

                //IF NOT IS_PED_IN_(temp_vehicle)
                IF NOT IS_PED_IN_ANY_BOAT(player_PED_ID())
                AND NOT IS_PED_IN_ANY_PLANE(player_PED_ID())
                AND NOT IS_PED_IN_ANY_HELI(player_PED_ID())
                AND NOT IS_PED_IN_ANY_TRAIN(player_PED_ID())
                AND NOT IS_PED_IN_ANY_SUB(player_PED_ID())
				AND  GET_ENTITY_MODEL(temp_vehicle) != BLAZER5
				AND  GET_ENTITY_MODEL(temp_vehicle) != STROMBERG
                    NET_PRINT("\n TRACK_DRIVE_WITHOUT_CRASHING_AWARD2 ")
                    
                    IF temp_vehicle_speed > 1.0
                        //MPGlobalsStats.time_without_crashing_ms +=DelayTime //in ms has to be converted to mins
                        //INT temp_int=  MPGlobalsStats.time_without_crashing_ms
                        INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIMEWITHOUTCRASHING, DelayTime)
                        PRINTLN("TRACK_DRIVE_WITHOUT_CRASHING_AWARD MP_STAT_TIMEWITHOUTCRASHING: ",GET_MP_INT_CHARACTER_STAT(MP_STAT_TIMEWITHOUTCRASHING))
                       	PRINTLN("TRACK_DRIVE_WITHOUT_CRASHING_AWARD MP_AWARD_FMDRIVEWITHOUTCRASH: ",GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMDRIVEWITHOUTCRASH))
                        PRINTLN("TRACK_DRIVE_WITHOUT_CRASHING_AWARD DelayTime: ",DelayTime)
                       
                    
                        IF GET_TOTAL_NUMBER_OF_MINUTES_FOR_MP_UNSIGNED_INT_STAT(MP_STAT_TIMEWITHOUTCRASHING) > GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMDRIVEWITHOUTCRASH)
                            
							SET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMDRIVEWITHOUTCRASH,  GET_TOTAL_NUMBER_OF_MINUTES_FOR_MP_UNSIGNED_INT_STAT(MP_STAT_TIMEWITHOUTCRASHING))
                            
                            PRINTLN("TRACK_DRIVE_WITHOUT_CRASHING_AWARD 2 MP_STAT_TIMEWITHOUTCRASHING: ",GET_MP_INT_CHARACTER_STAT(MP_STAT_TIMEWITHOUTCRASHING))
                       		PRINTLN("TRACK_DRIVE_WITHOUT_CRASHING_AWARD 2 MP_AWARD_FMDRIVEWITHOUTCRASH: ",GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMDRIVEWITHOUTCRASH))
                    		PRINTLN("TRACK_DRIVE_WITHOUT_CRASHING_AWARD 2 DelayTime: ",DelayTime)
                            NET_PRINT_INT( GET_MP_INT_CHARACTER_STAT(MP_STAT_TIMEWITHOUTCRASHING))
                        
                        
                        ENDIF
                    ENDIF
                ELSE
                    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_TIMEWITHOUTCRASHING)!= 0
                        SET_MP_INT_CHARACTER_STAT(MP_STAT_TIMEWITHOUTCRASHING, 0)
                    ENDIF
                    MPGlobalsStats.veh_damage = GET_ENTITY_HEALTH(temp_vehicle)
                ENDIF
            ELSE
                IF GET_MP_INT_CHARACTER_STAT(MP_STAT_TIMEWITHOUTCRASHING)!= 0
                    SET_MP_INT_CHARACTER_STAT(MP_STAT_TIMEWITHOUTCRASHING, 0)
                ENDIF
                MPGlobalsStats.veh_damage = GET_ENTITY_HEALTH(temp_vehicle)
            ENDIF
        
    ELSE
        MPGlobalsStats.veh_damage = 0
        IF GET_MP_INT_CHARACTER_STAT(MP_STAT_TIMEWITHOUTCRASHING)!= 0
            SET_MP_INT_CHARACTER_STAT(MP_STAT_TIMEWITHOUTCRASHING, 0)
        ENDIF
    ENDIF
ENDPROC


PROC DISPLAY_CARMOD_UNLOCK_MESSAGE(STRING label)

	BEGIN_TEXT_COMMAND_THEFEED_POST(label)
	END_TEXT_COMMAND_THEFEED_POST_UNLOCK_TU("VEHMOD_UNLOCK", 5,label,TRUE)

	PLAY_SOUND_FRONTEND(-1, "CHALLENGE_UNLOCKED", "HUD_AWARDS") 
		

ENDPROC


// check if player has filled a titan
PROC TRACK_FILL_A_TITAN()
    IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_FILL_TITAN)
            
        IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
        AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
            VEHICLE_INDEX temp_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
            IF GET_ENTITY_MODEL(temp_vehicle) = TITAN
            
                IF GET_VEHICLE_NUMBER_OF_PASSENGERS(temp_vehicle)= GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(temp_vehicle)
                    SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_FILL_TITAN, TRUE)
                ENDIF
            ENDIF
                
        ENDIF
    ENDIF
ENDPROC

FUNC BOOL CHECK_AND_REWARD_USER_RACES_AND_DEATHMATCHES(BOOL &DID_PLAYER_CREATE_ACTIVITY)
    
    
    //DID_I_CREATE_THIS_MISSION(DID_PLAYER_CREATE_ACTIVITY)
    
        
    IF NOT IS_THIS_A_ROCKSTAR_ACTIVITY()
    AND ARE_ALL_INSTANCED_TUTORIALS_COMPLETE() 
        IF DID_PLAYER_CREATE_ACTIVITY = FALSE
            INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_REW_PLAY_OTHER_MISS_COUNT)
            #IF IS_DEBUG_BUILD
                NET_PRINT("\n  CHECK_AND_REWARD_USER_RACES_AND_DEATHMATCHES(): Incremented \n ")
            #ENDIF  
            IF GET_MP_BOOL_CHARACTER_STAT( MP_STAT_REW_BOOL_PLAY_OTHER_MISS) = FALSE
                IF GET_MP_INT_CHARACTER_STAT( MP_STAT_REW_PLAY_OTHER_MISS_COUNT) = 10
                    SET_MP_BOOL_CHARACTER_STAT( MP_STAT_REW_BOOL_PLAY_OTHER_MISS, TRUE)
                   // GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP10USER", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_TEN_OTHER_USER_JOBS, 2000*ROUND(g_sMPTunables.fxp_tunable_Play_your_creations_against_other_players), 1)
                    //PRINT_TICKER_WITH_INT("REWXP10USER", 2000)
                    #IF IS_DEBUG_BUILD
                     //   NET_PRINT("\n  CHECK_AND_REWARD_USER_RACES_AND_DEATHMATCHES(): Given award \n ")
                    #ENDIF
                    RETURN TRUE
                    
                ENDIF
            ENDIF
            
        ELSE    // give player XP for playing their own content against more than 1 player 500 XP
        
        
            RETURN FALSE
        
        ENDIF
    ENDIF
    
    RETURN FALSE
    
ENDFUNC





FUNC BOOL CHECK_AND_REWARD_ROCKSTAR_RACES_AND_DEATHMATCHES()
    
    
    //DID_I_CREATE_THIS_MISSION(DID_PLAYER_CREATE_ACTIVITY)
    
        
    IF IS_THIS_A_ROCKSTAR_ACTIVITY()
    AND ARE_ALL_INSTANCED_TUTORIALS_COMPLETE() 
        INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_REW_RSTAR_VER_COMP_COUNT)
        SET_PACKED_STAT_BOOL( PACKED_MP_ROCKSTAR_VERIFIED, TRUE)
        #IF IS_DEBUG_BUILD
            NET_PRINT("\n  CHECK_AND_REWARD_ROCKSTAR_RACES_AND_DEATHMATCHES(): Incremented \n ")
        #ENDIF  
        IF GET_MP_BOOL_CHARACTER_STAT( MP_STAT_REW_BOOL_PLAY_RSTAR_MISS) = FALSE
            IF GET_MP_INT_CHARACTER_STAT( MP_STAT_REW_RSTAR_VER_COMP_COUNT) = 10
                SET_MP_BOOL_CHARACTER_STAT( MP_STAT_REW_BOOL_PLAY_RSTAR_MISS, TRUE)
                //GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REW10RSTAR",XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_TEN_ROCKSTAR_JOBS, 1000*ROUND(g_sMPTunables.fxp_tunable_Play_ten_Rockstar_verified_races_missions_or_deathmatches) , 1)
                //PRINT_TICKER_WITH_INT("REW10RSTAR", 1000)
                #IF IS_DEBUG_BUILD
                    NET_PRINT("\n  CHECK_AND_REWARD_ROCKSTAR_RACES_AND_DEATHMATCHES(): Given award \n ")
                #ENDIF
                RETURN TRUE
                
            ENDIF
        ENDIF
        /*
        IF IS_MP_TATTOO_UNLOCKED(TSHIRT_TRANS_MP_FM_ROCKSTAR) = FALSE
            SET_MP_TATTOO_UNLOCKED(TSHIRT_TRANS_MP_FM_ROCKSTAR, TRUE)
        ENDIF
    
        */
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

#IF USE_TU_CHANGES


PROC APPLY_ARMOUR_GIFTING(INT iSlot, INT rank)
	
	PRINTLN("APPLY_ARMOUR_GIFTING: APPLYING ARMOUR INV for slot ", iSlot, " rank = ", rank, " ")

	IF rank >= GET_FM_KIT_UNLOCK_RANK(PLAYERKIT_ARMOUR_STORE_L1)
		SET_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L1, TRUE, FALSE)
	ENDIF
	IF rank >= GET_FM_KIT_UNLOCK_RANK(PLAYERKIT_ARMOUR_STORE_L2)
		SET_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L2, TRUE, FALSE)
	ENDIF
	IF rank >= GET_FM_KIT_UNLOCK_RANK(PLAYERKIT_ARMOUR_STORE_L3)
		SET_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L3, TRUE, FALSE)
	ENDIF
	IF rank >= GET_FM_KIT_UNLOCK_RANK(PLAYERKIT_ARMOUR_STORE_L4)
		SET_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L4, TRUE, FALSE)
	ENDIF
	IF rank >= GET_FM_KIT_UNLOCK_RANK(PLAYERKIT_ARMOUR_STORE_L5)
		SET_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L5, TRUE, FALSE)
	ENDIF
	IF rank >= GET_FM_KIT_UNLOCK_RANK(PLAYERKIT_ARMOUR_STORE_L6)
		SET_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L6, TRUE, FALSE)
	ENDIF
	IF rank >= GET_FM_KIT_UNLOCK_RANK(PLAYERKIT_ARMOUR_STORE_L7)
		SET_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L7, TRUE, FALSE)
	ENDIF
	IF rank >= GET_FM_KIT_UNLOCK_RANK(PLAYERKIT_ARMOUR_STORE_L8)
		SET_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L8, TRUE, FALSE)
	ENDIF
	IF rank >= GET_FM_KIT_UNLOCK_RANK(PLAYERKIT_ARMOUR_STORE_L9)
		SET_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L9, TRUE, FALSE)
	ENDIF
	IF rank >= GET_FM_KIT_UNLOCK_RANK(PLAYERKIT_ARMOUR_STORE_L10)
		SET_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L10, TRUE, FALSE)
	ENDIF
	
	IF rank >= GET_FM_KIT_UNLOCK_RANK( PLAYERKIT_SUPERLIGHTARMOUR)
		SET_MP_KIT_UNLOCKED(PLAYERKIT_SUPERLIGHTARMOUR, TRUE, FALSE)
	ENDIF
	IF rank >= GET_FM_KIT_UNLOCK_RANK( PLAYERKIT_LIGHTARMOUR)
		SET_MP_KIT_UNLOCKED(PLAYERKIT_LIGHTARMOUR, TRUE, FALSE)
	ENDIF
	
	
	IF rank >= GET_FM_KIT_UNLOCK_RANK( PLAYERKIT_STANDARDARMOUR)
		SET_MP_KIT_UNLOCKED(PLAYERKIT_STANDARDARMOUR, TRUE, FALSE)
	ENDIF
	
	
	IF rank >= GET_FM_KIT_UNLOCK_RANK( PLAYERKIT_HEAVYARMOUR)
		SET_MP_KIT_UNLOCKED(PLAYERKIT_HEAVYARMOUR, TRUE, FALSE)
	ENDIF
	
	
	IF rank >= GET_FM_KIT_UNLOCK_RANK( PLAYERKIT_SUPERHEAVYARMOUR)
		SET_MP_KIT_UNLOCKED(PLAYERKIT_SUPERHEAVYARMOUR, TRUE, FALSE)
	ENDIF
	
	
	INT iInventoryMax = 0
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L1) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L2) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L3) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L4) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L5) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L6) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L7) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L8) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L9) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L10) iInventoryMax++ ENDIF

	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_SUPERLIGHTARMOUR)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_1_COUNT, iInventoryMax, iSlot)
		PRINTLN(" APPLY_ARMOUR_GIFTING: APPLYING ARMOUR_1 SUPERLIGHTARMOUR TO ", iInventoryMax, " ")
		
		INT iItemHash = HASH("MP_STAT_MP_CHAR_ARMOUR_1_COUNT_v0")
		INT iItemCategory = hash("ARMOR")	
		INT iShopType = hash("EVENT")
		INT iReason = hash("GET")
		SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, iInventoryMax, 0, iReason, false, iShopType )
	ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_LIGHTARMOUR)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_2_COUNT, iInventoryMax, iSlot)
		PRINTLN(" APPLY_ARMOUR_GIFTING: APPLYING ARMOUR_2 LIGHTARMOUR TO ", iInventoryMax, " ")
		
		INT iItemHash = HASH("MP_STAT_MP_CHAR_ARMOUR_2_COUNT_v0")
		INT iItemCategory = hash("ARMOR")	
		INT iShopType = hash("EVENT")
		INT iReason = hash("GET")
		SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, iInventoryMax, 0, iReason, false, iShopType )
	ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_STANDARDARMOUR)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_3_COUNT, iInventoryMax, iSlot)
		PRINTLN(" APPLY_ARMOUR_GIFTING: APPLYING ARMOUR_3 STANDARDARMOUR TO ", iInventoryMax, " ")
		
		INT iItemHash = HASH("MP_STAT_MP_CHAR_ARMOUR_3_COUNT_v0")
		INT iItemCategory = hash("ARMOR")	
		INT iShopType = hash("EVENT")
		INT iReason = hash("GET")
		SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, iInventoryMax, 0, iReason, false, iShopType )
	ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_HEAVYARMOUR)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_4_COUNT, iInventoryMax, iSlot)
		PRINTLN(" APPLY_ARMOUR_GIFTING: APPLYING ARMOUR_4 HEAVYARMOUR TO ", iInventoryMax, " ")
		
		INT iItemHash = HASH("MP_STAT_MP_CHAR_ARMOUR_4_COUNT_v0")
		INT iItemCategory = hash("ARMOR")	
		INT iShopType = hash("EVENT")
		INT iReason = hash("GET")
		SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, iInventoryMax, 0, iReason, false, iShopType )
	ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_SUPERHEAVYARMOUR)
		SET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_5_COUNT, iInventoryMax, iSlot)
		PRINTLN(" APPLY_ARMOUR_GIFTING: APPLYING ARMOUR_5 SUPERHEAVYARMOUR TO ", iInventoryMax, " ")
		
		INT iItemHash = HASH("MP_STAT_MP_CHAR_ARMOUR_5_COUNT_v0")
		INT iItemCategory = hash("ARMOR")	
		INT iShopType = hash("EVENT")
		INT iReason = hash("GET")
		SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, iInventoryMax, 0, iReason, false, iShopType )
	ENDIF
	
	


ENDPROC


FUNC string GET_XMAS_GIFT_SHIRT_TICKER(int xmastshirt)
	PRINTLN("GET_XMAS_GIFT_SHIRT_TICKER number passed in: ", xmastshirt)
	
	string xmasstring 
	BOOL bIsPlayerMale = (GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0)
	
	IF bIsPlayerMale
	
		SWITCH xmastshirt
			CASE 0		xmasstring = "UNLOCK_AWD_XMAS0"	BREAK
			CASE 1		xmasstring = "UNLOCK_AWD_XMAS1"	BREAK
			CASE 2		xmasstring = "UNLOCK_AWD_XMAS2"	BREAK
			CASE 3		xmasstring = "UNLOCK_AWD_XMAS3"	BREAK
			CASE 4		xmasstring = "UNLOCK_AWD_XMAS4"	BREAK
			CASE 5		xmasstring = "UNLOCK_AWD_XMAS5"	BREAK
			CASE 6		xmasstring = "UNLOCK_AWD_XMAS6"	BREAK
			CASE 7		xmasstring = "UNLOCK_AWD_XMAS7"	BREAK
			CASE 8		xmasstring = "UNLOCK_AWD_XMAS8"	BREAK
			CASE 9		xmasstring = "UNLOCK_AWD_XMAS9"	BREAK
			CASE 10		xmasstring = "UNLOCK_AWD_XMAS10"	BREAK
			CASE 11		xmasstring = "UNLOCK_AWD_XMAS11"	BREAK
			CASE 12		xmasstring = "UNLOCK_AWD_XMAS12"	BREAK
			CASE 13		xmasstring = "UNLOCK_AWD_XMAS13"	BREAK
			CASE 14		xmasstring = "UNLOCK_AWD_XMAS14"	BREAK
			CASE 15		xmasstring = "UNLOCK_AWD_XMAS15"	BREAK
			CASE 16		xmasstring = "UNLOCK_AWD_XMAS16"	BREAK
			CASE 17		xmasstring = "UNLOCK_AWD_XMAS17"	BREAK
			CASE 18		xmasstring = "UNLOCK_AWD_XMAS18"	BREAK
			CASE 19		xmasstring = "UNLOCK_AWD_XMAS19"	BREAK
			CASE 20		xmasstring = "UNLOCK_AWD_XMAS20"	BREAK
			CASE 21		xmasstring = "UNLOCK_AWD_XMAS21"	BREAK
			CASE 22		xmasstring = "UNLOCK_AWD_XMAS22"	BREAK
			CASE 23		xmasstring = "UNLOCK_AWD_XMAS23"	BREAK
			CASE 24		xmasstring = "UNLOCK_AWD_XMAS24"	BREAK
			CASE 25		xmasstring = "UNLOCK_AWD_XMAS25"	BREAK
			CASE 26		xmasstring = "UNLOCK_AWD_XMAS26"	BREAK
			CASE 27		xmasstring = "UNLOCK_AWD_XMAS27"	BREAK
			CASE 28		xmasstring = "UNLOCK_AWD_XMAS28"	BREAK
			CASE 29		xmasstring = "UNLOCK_AWD_XMAS29"	BREAK
			CASE 30		xmasstring = "UNLOCK_AWD_XMAS30"	BREAK
			CASE 31		xmasstring = "UNLOCK_AWD_XMAS31"	BREAK
			CASE 32		xmasstring = "UNLOCK_AWD_XMAS32"	BREAK
			CASE 33		xmasstring = "UNLOCK_AWD_XMAS33"	BREAK
			CASE 34		xmasstring = "UNLOCK_AWD_XMAS34"	BREAK
			CASE 35		xmasstring = "UNLOCK_AWD_XMAS35"	BREAK
			CASE 36		xmasstring = "UNLOCK_AWD_XMAS36"	BREAK
			CASE 37		xmasstring = "UNLOCK_AWD_XMAS37"	BREAK
			CASE 38		xmasstring = "UNLOCK_AWD_XMAS38"	BREAK
			CASE 39		xmasstring = "UNLOCK_AWD_XMAS39"	BREAK
			CASE 40		xmasstring = "UNLOCK_AWD_XMAS40"	BREAK
			CASE 41		xmasstring = "UNLOCK_AWD_XMAS41"	BREAK
			CASE 42		xmasstring = "UNLOCK_AWD_XMAS42"	BREAK
			CASE 43		xmasstring = "UNLOCK_AWD_XMAS43"	BREAK
			CASE 44		xmasstring = "UNLOCK_AWD_XMAS44"	BREAK
			CASE 45		xmasstring = "UNLOCK_AWD_XMAS45"	BREAK
			CASE 46		xmasstring = "UNLOCK_AWD_XMAS46"	BREAK
			CASE 47		xmasstring = "UNLOCK_AWD_XMAS47"	BREAK
			CASE 48		xmasstring = "UNLOCK_AWD_XMAS48"	BREAK
			CASE 49		xmasstring = "UNLOCK_AWD_XMAS49"	BREAK
			CASE 50		xmasstring = "UNLOCK_AWD_XMAS50"	BREAK
			CASE 51		xmasstring = "UNLOCK_AWD_XMAS51"	BREAK
			CASE 52		xmasstring = "UNLOCK_AWD_XMAS52"	BREAK
			CASE 53		xmasstring = "UNLOCK_AWD_XMAS53"	BREAK
			CASE 54		xmasstring = "UNLOCK_AWD_XMAS55"	BREAK
			CASE 55		xmasstring = "UNLOCK_AWD_XMAS56"	BREAK
			CASE 56		xmasstring = "UNLOCK_AWD_XMAS57"	BREAK
			CASE 57		xmasstring = "UNLOCK_AWD_XMAS58"	BREAK
		ENDSWITCH
	ELSE
		SWITCH xmastshirt
			CASE 0		xmasstring = "UNLOCK_AWD_XMAB0"	BREAK
			CASE 1		xmasstring = "UNLOCK_AWD_XMAB1"	BREAK
			CASE 2		xmasstring = "UNLOCK_AWD_XMAB2"	BREAK
			CASE 3		xmasstring = "UNLOCK_AWD_XMAB3"	BREAK
			CASE 4		xmasstring = "UNLOCK_AWD_XMAB4"	BREAK
			CASE 5		xmasstring = "UNLOCK_AWD_XMAB5"	BREAK
			CASE 6		xmasstring = "UNLOCK_AWD_XMAB6"	BREAK
			CASE 7		xmasstring = "UNLOCK_AWD_XMAB7"	BREAK
			CASE 8		xmasstring = "UNLOCK_AWD_XMAB8"	BREAK
			CASE 9		xmasstring = "UNLOCK_AWD_XMAB9"	BREAK
			CASE 10		xmasstring = "UNLOCK_AWD_XMAB10"	BREAK
			CASE 11		xmasstring = "UNLOCK_AWD_XMAB11"	BREAK
			CASE 12		xmasstring = "UNLOCK_AWD_XMAB12"	BREAK
			CASE 13		xmasstring = "UNLOCK_AWD_XMAB13"	BREAK
			CASE 14		xmasstring = "UNLOCK_AWD_XMAB14"	BREAK
			CASE 15		xmasstring = "UNLOCK_AWD_XMAB15"	BREAK
			CASE 16		xmasstring = "UNLOCK_AWD_XMAS16"	BREAK
			CASE 17		xmasstring = "UNLOCK_AWD_XMAS17"	BREAK
			CASE 18		xmasstring = "UNLOCK_AWD_XMAS18"	BREAK
			CASE 19		xmasstring = "UNLOCK_AWD_XMAS19"	BREAK
			CASE 20		xmasstring = "UNLOCK_AWD_XMAS20"	BREAK
			CASE 21		xmasstring = "UNLOCK_AWD_XMAS21"	BREAK
			CASE 22		xmasstring = "UNLOCK_AWD_XMAS22"	BREAK
			CASE 23		xmasstring = "UNLOCK_AWD_XMAS23"	BREAK
			CASE 24		xmasstring = "UNLOCK_AWD_XMAS24"	BREAK
			CASE 25		xmasstring = "UNLOCK_AWD_XMAS25"	BREAK
			CASE 26		xmasstring = "UNLOCK_AWD_XMAS26"	BREAK
			CASE 27		xmasstring = "UNLOCK_AWD_XMAS27"	BREAK
			CASE 28		xmasstring = "UNLOCK_AWD_XMAS28"	BREAK
			CASE 29		xmasstring = "UNLOCK_AWD_XMAS29"	BREAK
			CASE 30		xmasstring = "UNLOCK_AWD_XMAS30"	BREAK
			CASE 31		xmasstring = "UNLOCK_AWD_XMAS31"	BREAK
			CASE 32		xmasstring = "UNLOCK_AWD_XMAS32"	BREAK
			CASE 33		xmasstring = "UNLOCK_AWD_XMAS33"	BREAK
			CASE 34		xmasstring = "UNLOCK_AWD_XMAS34"	BREAK
			CASE 35		xmasstring = "UNLOCK_AWD_XMAS35"	BREAK
			CASE 36		xmasstring = "UNLOCK_AWD_XMAS36"	BREAK
			CASE 37		xmasstring = "UNLOCK_AWD_XMAS37"	BREAK
			CASE 38		xmasstring = "UNLOCK_AWD_XMAS38"	BREAK
			CASE 39		xmasstring = "UNLOCK_AWD_XMAS39"	BREAK
			CASE 40		xmasstring = "UNLOCK_AWD_XMAS40"	BREAK
			CASE 41		xmasstring = "UNLOCK_AWD_XMAS41"	BREAK
			CASE 42		xmasstring = "UNLOCK_AWD_XMAS42"	BREAK
			CASE 43		xmasstring = "UNLOCK_AWD_XMAS43"	BREAK
			CASE 44		xmasstring = "UNLOCK_AWD_XMAS44"	BREAK
			CASE 45		xmasstring = "UNLOCK_AWD_XMAS45"	BREAK
			CASE 46		xmasstring = "UNLOCK_AWD_XMAS46"	BREAK
			CASE 47		xmasstring = "UNLOCK_AWD_XMAS47"	BREAK
			CASE 48		xmasstring = "UNLOCK_AWD_XMAS48"	BREAK
			CASE 49		xmasstring = "UNLOCK_AWD_XMAS49"	BREAK
			CASE 50		xmasstring = "UNLOCK_AWD_XMAS50"	BREAK
			CASE 51		xmasstring = "UNLOCK_AWD_XMAS51"	BREAK
			CASE 52		xmasstring = "UNLOCK_AWD_XMAS52"	BREAK
			CASE 53		xmasstring = "UNLOCK_AWD_XMAS53"	BREAK
			CASE 54		xmasstring = "UNLOCK_AWD_XMAS55"	BREAK
			CASE 55		xmasstring = "UNLOCK_AWD_XMAS56"	BREAK
			CASE 56		xmasstring = "UNLOCK_AWD_XMAS57"	BREAK
			CASE 57		xmasstring = "UNLOCK_AWD_XMAS58"	BREAK
		ENDSWITCH
	
	
	
	ENDIF
	RETURN (xmasstring)
ENDFUNC


FUNC INT GET_XMAS16_GIFT_SHIRT_HASH(int xmastshirt)
	PRINTLN("GET_XMAS_GIFT_SHIRT_PACKED_STAT number passed in: ", xmastshirt)

	BOOL bIsPlayerMale = (GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0)
	
	IF bIsPlayerMale
		SWITCH xmastshirt
			CASE 0		RETURN 			(HASH("FM_Hip_M_Retro_000"		))   BREAK  // PACKED_STAT_BOOL_AWARD_Base5TShirt									
			CASE 1		RETURN 			(HASH("FM_Hip_M_Retro_001"		))   BREAK  // PACKED_STAT_BOOL_AWARD_BitchnDogFoodTShirt							
			CASE 2		RETURN 			(HASH("FM_Hip_M_Retro_002"		))   BREAK  // PACKED_STAT_BOOL_AWARD_BOBOTShirt									
			CASE 3		RETURN 			(HASH("FM_Hip_M_Retro_012"		))   BREAK  			// PACKED_STAT_BOOL_AWARD_BounceFMTShirt                                
			CASE 4		RETURN 			(HASH("FM_Hip_M_Retro_003"		))   BREAK  				// PACKED_STAT_BOOL_AWARD_CrocsBarTShirt                                
			CASE 5		RETURN 			(HASH("FM_Hip_M_Retro_004"		))   BREAK  				// PACKED_STAT_BOOL_AWARD_Emotion983TShirt                              
			CASE 6		RETURN 			(HASH("FM_Hip_M_Retro_005"		))   BREAK  				// PACKED_STAT_BOOL_AWARD_Fever105TShirt                                
			CASE 7		RETURN 			(HASH("FM_Hip_M_Retro_006"		))   BREAK  				// PACKED_STAT_BOOL_AWARD_FlashTShirt                                   
			CASE 8		RETURN 			(HASH("FM_Hip_M_Retro_008"		))   BREAK  				// PACKED_STAT_BOOL_AWARD_HomiesSharpTShirt                             
			CASE 9		RETURN 			(HASH("FM_Hip_M_Retro_009"		))   BREAK  				// PACKED_STAT_BOOL_AWARD_KDSTTShirt                                    
			CASE 10		RETURN 			(HASH("FM_Hip_M_Retro_011"		))   BREAK  				// PACKED_STAT_BOOL_AWARD_KJAHRadioTShirt                               
			CASE 11		RETURN 			(HASH("FM_Hip_M_Retro_013"		))   BREAK  				// PACKED_STAT_BOOL_AWARD_KROSE                                         
			CASE 12		RETURN 			(HASH("FM_Ind_M_Award_000"		))   BREAK  			// PACKED_MP_STATS_GIVE_FOR_FREE_STATUE_OF_HAPPINESS_TEE                
			CASE 13		RETURN 			(HASH("FM_LTS_M_Tshirt_000"		))   BREAK  				// PACKED_STAT_BOOL_AWARD_VictoryFistTshirt                             
			CASE 14		RETURN 			(HASH("FM_Hip_M_Retro_007"		))   BREAK  				// PACKED_STAT_BOOL_AWARD_VinylCountdownTShirt                          
			CASE 15		RETURN 			(HASH("FM_Hip_M_Retro_010"		))   BREAK  				// PACKED_STAT_BOOL_AWARD_VivisectionTShirt	                            
			CASE 16		RETURN 			(HASH("MP_FILM_003_M"			))   BREAK  		// PACKED_MP_FILM_DIE_ALREADY_4                                         
			CASE 17		RETURN 			(HASH("MP_FAKE_DS_000_M"			))   BREAK  			// PACKED_MP_FAKE_DIDIER_SACHS_TSHIRT                                   
			CASE 18		RETURN 			(HASH("MP_FAKE_DIS_001_M"		))   BREAK  			// PACKED_MP_FAKE_DIX_GOLD_TSHIRT                                       
			CASE 19		RETURN 			(HASH("MP_FAKE_DIS_000_M"		))   BREAK  			// PACKED_MP_FAKE_DIX_WHITE_TSHIRT                                      
			CASE 20		RETURN 			(HASH("MP_FAKE_ENEMA_000_M"		))   BREAK  				// PACKED_MP_FAKE_ENEMA_TSHIRT_TSHIRT                                   
			CASE 21		RETURN 			(HASH("MP_FAKE_LB_000_M"			))   BREAK  			// PACKED_MP_FAKE_LE_CHIEN_NO2_TSHIRT                                   
			CASE 22		RETURN 			(HASH("MP_FAKE_LC_000_M"			))   BREAK  			// PACKED_MP_FAKE_LE_CHIEN_CREW_TSHIRT                                  
			CASE 23		RETURN 			(HASH("MP_FAKE_Per_000_M"		))	BREAK  			// PACKED_MP_FAKE_PERSEUS_TSHIRT                                        
			CASE 24		RETURN 			(HASH("MP_FAKE_SC_000_M"			))   BREAK  			// PACKED_MP_FAKE_SANTO_CAPRA_TSHIRT                                    
			CASE 25		RETURN 			(HASH("MP_FAKE_SN_000_M"			))   BREAK  			// PACKED_MP_FAKE_SESSANTA_NOVE_TSHIRT                                  
			CASE 26		RETURN 			(HASH("MP_FAKE_Vap_000_M"		))	BREAK  			// PACKED_MP_FAKE_VAPID_TSHIRT                                          
			CASE 27		RETURN 			(HASH("MP_FILM_002_M"			))	BREAK  		// PACKED_MP_FILM_I_MARRIED_MY_DAD	                                    
			CASE 28		RETURN 			(HASH("MP_FILM_001_M"			))	BREAK  		// PACKED_MP_FILM_MELTDOWN                                              
			CASE 29		RETURN 			(HASH("MP_FILM_005_M"			))	BREAK  		// PACKED_MP_FILM_NELSON_IN_NAPLES                                      
			CASE 30		RETURN 			(HASH("MP_FILM_004_M"			))	BREAK  		// PACKED_MP_FILM_SHOULDER_OF_ORION                                     
			CASE 31		RETURN 			(HASH("MP_FILM_000_M"			))	BREAK  		// PACKED_MP_FILM_VINEWOOD_ZOMBIE                                       
			CASE 32		RETURN 			(HASH("MP_FILM_007_M"			))	BREAK  		// PACKED_MP_FILM_AMERICAN_DIVORCE                                      
			CASE 33		RETURN 			(HASH("MP_exec_prizes_001_M"		))   BREAK  				// PACKED_STAT_BOOL_SHIRT_BAHAMAMAMAS                                   
			CASE 34		RETURN 			(HASH("MP_Biker_Tee_049_M"		))   BREAK  				// PACKED_MP_BOOL_TSHIRT_WESTERN_BIG_LOGO_BLACK                         
			CASE 35		RETURN 			(HASH("MP_Biker_Tee_053_M"		))   BREAK  				// PACKED_MP_BOOL_TSHIRT_NAGASAKI_LOGO_BLACK_SHIRT_WHITE_STAR           
			CASE 36		RETURN 			(HASH("HW_Tee_010_M"				))   BREAK  		// PACKED_MP_AWARD_LOW_TSHIRT_BUTCHERY                                  
			CASE 37		RETURN 			(HASH("HW_Tee_011_M"				))   BREAK  		// PACKED_MP_AWARD_LOW_TSHIRT_BOMBSHELL_BLOODBATH_CHEERLEADER_MASSACRE_3
			CASE 38		RETURN 			(HASH("MP_exec_prizes_003_M"		))   BREAK  				// PACKED_STAT_BOOL_SHIRT_GROTTI                                        
			CASE 39		RETURN 			(HASH("HW_Tee_007_M"				))   BREAK  		// PACKED_MP_AWARD_LOW_TSHIRT_KNIFE_AFTER_DARK                          
			CASE 40		RETURN 			(HASH("HW_Tee_005_M"				))   BREAK  		// PACKED_MP_AWARD_LOW_TSHIRT_PSYCHO_SWINGERS                           
			CASE 41		RETURN 			(HASH("MP_FILM_008_M"			))   BREAK  		// PACKED_MP_FILM_LONLIEST_ROBOT                                        
			CASE 42		RETURN 			(HASH("HW_Tee_003_M"				))   BREAK  		// PACKED_MP_AWARD_LOW_TSHIRT_TWILIGHT_KNIFE                            
			CASE 43		RETURN 			(HASH("MP_Biker_Tee_050_M"		))   BREAK  				// PACKED_MP_BOOL_TSHIRT_WESTERN_BIG_LOGO_WHITE                         
			CASE 44		RETURN 			(HASH("HW_Tee_000_M"				))   BREAK  		// PACKED_MP_AWARD_LOW_TSHIRT_ZOMBIE_LIBERALS_IN_THE_MIDWEST            
		
			CASE 45  RETURN  (HASH("DLC_MP_GR_M_DECL_5_8")) 	BREAK 		//	PACKED_MP_BOOL_BLACK_AMMUNATION_HOODIE		
			CASE 46  RETURN  (HASH("DLC_MP_GR_M_DECL_5_0"))    BREAK       //	PACKED_MP_BOOL_WHITE_HAWK_AND_LITTLE_HOODIE	  
			CASE 47  RETURN  (HASH("MP_Gunrunning_Award_014_M"))    BREAK       //	PACKED_MP_BOOL_BLACK_COIL_TEE         			Do a tattoo check for this RETURN (NOT IS_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(iPreset, TATTOO_MP_FM)))        
			CASE 48  RETURN  (HASH("MP_Gunrunning_Award_000_M"))    BREAK       //	PACKED_MP_BOOL_BLACK_HAWK_AND_LITTLE_LOGO_TEE 
			CASE 49  RETURN  (HASH("MP_Gunrunning_Award_005_M"))    BREAK       //	PACKED_MP_BOOL_WHITE_SHREWSBURY_LOGO_TEE      
			CASE 50	 RETURN  (HASH("DLC_MP_GR_M_JBIB_19_0")) 	BREAK       //	PACKED_MP_BOOL_CLASS_OF_98_BLUE				
			CASE 51	 RETURN  (HASH("DLC_MP_GR_M_JBIB_19_1")) 	BREAK       //	PACKED_MP_BOOL_CLASS_OF_98_RED				
			CASE 52  RETURN  (HASH("MP_Gunrunning_Award_023_M"))    BREAK       //	PACKED_MP_BOOL_RSTAR_LOGO_INTERFERENCE        
			CASE 53  RETURN  (HASH("MP_Gunrunning_Award_024_M"))    BREAK       //	PACKED_MP_BOOL_ROCKSTAR_GAMES_INTERFERENCE    
			CASE 54  RETURN  (HASH("MP_Gunrunning_Award_021_M"))    BREAK       //	PACKED_MP_BOOL_ROCKSTAR_PINK_BLADE            
          
			CASE 55  RETURN  (HASH("MP_Gunrunning_Award_020_M"))    BREAK       //	PACKED_MP_BOOL_RSTAR_CAMO_LOGO_GREY           
			CASE 56  RETURN  (HASH("MP_Gunrunning_Award_020_M_ALT"))    BREAK       //	PACKED_MP_BOOL_RSTAR_CAMO_LOGO_WHITE          
			CASE 57  RETURN  (HASH("DLC_MP_GR_M_JBIB_20_0"))    BREAK       //	PACKED_MP_BOOL_RSTAR_LOGO_PATTERN_POCKET      
			
			
		ENDSWITCH
	ELSE
		SWITCH xmastshirt
			CASE 0		RETURN 			(   HASH(   "FM_Hip_F_Retro_000"	))	BREAK// PACKED_STAT_BOOL_AWARD_Base5TShirt									
			CASE 1		RETURN 			(   HASH(   "FM_Hip_F_Retro_001"	))	BREAK// PACKED_STAT_BOOL_AWARD_BitchnDogFoodTShirt							
			CASE 2		RETURN 			(   HASH(   "FM_Hip_F_Retro_002"	))	BREAK// PACKED_STAT_BOOL_AWARD_BOBOTShirt									
			CASE 3		RETURN 			(   HASH(   "FM_Hip_F_Retro_012"	))	BREAK			// PACKED_STAT_BOOL_AWARD_BounceFMTShirt                                
			CASE 4		RETURN 			(   HASH(   "FM_Hip_F_Retro_003"	))	BREAK				// PACKED_STAT_BOOL_AWARD_CrocsBarTShirt                                
			CASE 5		RETURN 			(   HASH(   "FM_Hip_F_Retro_004"	))	BREAK				// PACKED_STAT_BOOL_AWARD_Emotion983TShirt                              
			CASE 6		RETURN 			(   HASH(   "FM_Hip_F_Retro_005"	))	BREAK				// PACKED_STAT_BOOL_AWARD_Fever105TShirt                                
			CASE 7		RETURN 			(   HASH(   "FM_Hip_F_Retro_006"	))	BREAK				// PACKED_STAT_BOOL_AWARD_FlashTShirt                                   
			CASE 8		RETURN 			(   HASH(   "FM_Hip_F_Retro_008"	))	BREAK				// PACKED_STAT_BOOL_AWARD_HomiesSharpTShirt                             
			CASE 9		RETURN 			(   HASH(   "FM_Hip_F_Retro_009"	))	BREAK				// PACKED_STAT_BOOL_AWARD_KDSTTShirt                                    
			CASE 10		RETURN 			(   HASH(   "FM_Hip_F_Retro_011"	))	BREAK				// PACKED_STAT_BOOL_AWARD_KJAHRadioTShirt                               
			CASE 11		RETURN 			(   HASH(   "FM_Hip_F_Retro_013"	))	BREAK				// PACKED_STAT_BOOL_AWARD_KROSE                                         
			CASE 12		RETURN 			(   HASH(   "FM_Ind_F_Award_000"	))	BREAK			// PACKED_MP_STATS_GIVE_FOR_FREE_STATUE_OF_HAPPINESS_TEE                
			CASE 13		RETURN 			(   HASH(   "FM_LTS_F_Tshirt_000"	))	BREAK				// PACKED_STAT_BOOL_AWARD_VictoryFistTshirt                             
			CASE 14		RETURN 			(   HASH(   "FM_Hip_F_Retro_007"	))	BREAK				// PACKED_STAT_BOOL_AWARD_VinylCountdownTShirt                          
			CASE 15		RETURN 			(   HASH(   "FM_Hip_F_Retro_010"	))	BREAK				// PACKED_STAT_BOOL_AWARD_VivisectionTShirt	                            
			CASE 16		RETURN 			(   HASH(   "MP_FILM_003_F"			))	BREAK		// PACKED_MP_FILM_DIE_ALREADY_4                                         
			CASE 17		RETURN 			(   HASH(   "MP_FAKE_DS_000_F"		))	BREAK			// PACKED_MP_FAKE_DIDIER_SACHS_TSHIRT                                   
			CASE 18		RETURN 			(   HASH(   "MP_FAKE_DIS_001_F"		))	BREAK			// PACKED_MP_FAKE_DIX_GOLD_TSHIRT                                       
			CASE 19		RETURN 			(   HASH(   "MP_FAKE_DIS_000_F"		))	BREAK			// PACKED_MP_FAKE_DIX_WHITE_TSHIRT                                      
			CASE 20		RETURN 			(   HASH(   "MP_FAKE_ENEMA_000_F"	))	BREAK				// PACKED_MP_FAKE_ENEMA_TSHIRT_TSHIRT                                   
			CASE 21		RETURN 			(   HASH(   "MP_FAKE_LB_000_F"		))	BREAK			// PACKED_MP_FAKE_LE_CHIEN_NO2_TSHIRT                                   
			CASE 22		RETURN 			(   HASH(   "MP_FAKE_LC_000_F"		))	BREAK			// PACKED_MP_FAKE_LE_CHIEN_CREW_TSHIRT                                  
			CASE 23		RETURN 			(   HASH(   "MP_FAKE_Per_000_F"		))	BREAK			// PACKED_MP_FAKE_PERSEUS_TSHIRT                                        
			CASE 24		RETURN 			(   HASH(   "MP_FAKE_SC_000_F"		))	BREAK			// PACKED_MP_FAKE_SANTO_CAPRA_TSHIRT                                    
			CASE 25		RETURN 			(   HASH(   "MP_FAKE_SN_000_F"		))	BREAK			// PACKED_MP_FAKE_SESSANTA_NOVE_TSHIRT                                  
			CASE 26		RETURN 			(   HASH(   "MP_FAKE_Vap_000_F"		))	BREAK			// PACKED_MP_FAKE_VAPID_TSHIRT                                          
			CASE 27		RETURN 			(   HASH(   "MP_FILM_002_F"			))	BREAK		// PACKED_MP_FILM_I_MARRIED_MY_DAD	                                    
			CASE 28		RETURN 			(   HASH(   "MP_FILM_001_F"			))	BREAK		// PACKED_MP_FILM_MELTDOWN                                              
			CASE 29		RETURN 			(   HASH(   "MP_FILM_005_F"			))	BREAK		// PACKED_MP_FILM_NELSON_IN_NAPLES                                      
			CASE 30		RETURN 			(   HASH(   "MP_FILM_004_F"			))	BREAK		// PACKED_MP_FILM_SHOULDER_OF_ORION                                     
			CASE 31		RETURN 			(   HASH(   "MP_FILM_000_F"			))	BREAK		// PACKED_MP_FILM_VINEWOOD_ZOMBIE                                       
			CASE 32		RETURN 			(   HASH(   "MP_FILM_007_F"			))	BREAK		// PACKED_MP_FILM_AMERICAN_DIVORCE                                      
			CASE 33		RETURN 			(   HASH(   "MP_exec_prizes_001_F"	))	BREAK				// PACKED_STAT_BOOL_SHIRT_BAHAMAMAMAS                                   
			CASE 34		RETURN 			(   HASH(   "MP_Biker_Tee_049_F"	))	BREAK				// PACKED_MP_BOOL_TSHIRT_WESTERN_BIG_LOGO_BLACK                         
			CASE 35		RETURN 			(   HASH(   "MP_Biker_Tee_053_F"	))	BREAK				// PACKED_MP_BOOL_TSHIRT_NAGASAKI_LOGO_BLACK_SHIRT_WHITE_STAR           
			CASE 36		RETURN 			(   HASH(   "HW_Tee_010_F"			))	BREAK		// PACKED_MP_AWARD_LOW_TSHIRT_BUTCHERY                                  
			CASE 37		RETURN 			(   HASH(   "HW_Tee_011_F"			))	BREAK		// PACKED_MP_AWARD_LOW_TSHIRT_BOMBSHELL_BLOODBATH_CHEERLEADER_MASSACRE_3
			CASE 38		RETURN 			(   HASH(   "MP_exec_prizes_003_F"	))	BREAK				// PACKED_STAT_BOOL_SHIRT_GROTTI                                        
			CASE 39		RETURN 			(   HASH(   "HW_Tee_007_F"			))	BREAK		// PACKED_MP_AWARD_LOW_TSHIRT_KNIFE_AFTER_DARK                          
			CASE 40		RETURN 			(   HASH(   "HW_Tee_005_F"			))	BREAK		// PACKED_MP_AWARD_LOW_TSHIRT_PSYCHO_SWINGERS                           
			CASE 41		RETURN 			(   HASH(   "MP_FILM_008_F"			))	BREAK		// PACKED_MP_FILM_LONLIEST_ROBOT                                        
			CASE 42		RETURN 			(   HASH(   "HW_Tee_003_F"			))	BREAK		// PACKED_MP_AWARD_LOW_TSHIRT_TWILIGHT_KNIFE                            
			CASE 43		RETURN 			(   HASH(   "MP_Biker_Tee_050_F"	))	BREAK				// PACKED_MP_BOOL_TSHIRT_WESTERN_BIG_LOGO_WHITE                         
			CASE 44		RETURN 			(   HASH(   "HW_Tee_000_F"			))	BREAK		// PACKED_MP_AWARD_LOW_TSHIRT_ZOMBIE_LIBERALS_IN_THE_MIDWEST            
	
			CASE 45  RETURN  (HASH("DLC_MP_GR_F_DECL_5_8")) 		BREAK		//	PACKED_MP_BOOL_BLACK_AMMUNATION_HOODIE		
			CASE 46  RETURN  (HASH("DLC_MP_GR_F_DECL_5_0"))    		BREAK       //	PACKED_MP_BOOL_WHITE_HAWK_AND_LITTLE_HOODIE	  
			CASE 47  RETURN  (HASH("MP_Gunrunning_Award_014_F"))    	BREAK       //	PACKED_MP_BOOL_BLACK_COIL_TEE                 
			CASE 48  RETURN  (HASH("MP_Gunrunning_Award_000_F"))    BREAK       //	PACKED_MP_BOOL_BLACK_HAWK_AND_LITTLE_LOGO_TEE 
			CASE 49  RETURN  (HASH("MP_Gunrunning_Award_005_F"))  BREAK       //	PACKED_MP_BOOL_WHITE_SHREWSBURY_LOGO_TEE      
			CASE 50	 RETURN  (HASH("DLC_MP_GR_F_JBIB_25_0")) 	BREAK       //	PACKED_MP_BOOL_CLASS_OF_98_BLUE				
			CASE 51	 RETURN  (HASH("DLC_MP_GR_F_JBIB_25_1")) 	BREAK       //	PACKED_MP_BOOL_CLASS_OF_98_RED				
			CASE 52  RETURN  (HASH("MP_Gunrunning_Award_023_F"))    BREAK       //	PACKED_MP_BOOL_RSTAR_LOGO_INTERFERENCE        
			CASE 53  RETURN  (HASH("MP_Gunrunning_Award_024_F"))    BREAK       //	PACKED_MP_BOOL_ROCKSTAR_GAMES_INTERFERENCE    
			
			CASE 54  RETURN  (HASH("MP_Gunrunning_Award_021_F"))    BREAK       //	PACKED_MP_BOOL_ROCKSTAR_PINK_BLADE            
			CASE 55  RETURN  (HASH("MP_Gunrunning_Award_020_F"))    BREAK       //	PACKED_MP_BOOL_RSTAR_CAMO_LOGO_GREY           
			CASE 56  RETURN  (HASH("MP_Gunrunning_Award_020_F_ALT"))    BREAK       //	PACKED_MP_BOOL_RSTAR_CAMO_LOGO_WHITE          
			CASE 57  RETURN  (HASH("DLC_MP_GR_F_JBIB_26_0"))    BREAK       //	PACKED_MP_BOOL_RSTAR_LOGO_PATTERN_POCKET      
			
		
		
		
		
		
		ENDSWITCH
	ENDIF
	SCRIPT_ASSERT("GET_XMAS16_GIFT_SHIRT_HASH invalid int passed in. Tell Kevin Wong")
	RETURN 0
		
		
	
ENDFUNC

FUNC BOOL DOES_2017_TSHIRT_REWARD_USE_TATTOO_CHECK(int xmasshirt)
	PRINTLN("DOES_2017_TSHIRT_REWARD_USE_TATTOO_CHECK passed in xmasshirt = ", xmasshirt)
		SWITCH xmasshirt
			CASE 45  RETURN FALSE 	BREAK    ///  (HASH("DLC_MP_GR_M_DECL_5_8")) 	BREAK 		//	PACKED_MP_BOOL_BLACK_AMMUNATION_HOODIE		
			CASE 46  RETURN FALSE 	BREAK			///  (HASH("DLC_MP_GR_M_DECL_5_0"))    BREAK       //	PACKED_MP_BOOL_WHITE_HAWK_AND_LITTLE_HOODIE	  
			CASE 47  RETURN TRUE 	BREAK			///  (HASH("MP_Gunrunning_Award_014_M"))    BREAK       //	PACKED_MP_BOOL_BLACK_COIL_TEE         			Do a tattoo check for this RETURN (NOT IS_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(iPreset, TATTOO_MP_FM)))        
			CASE 48  RETURN TRUE 	BREAK			///  (HASH("MP_Gunrunning_Award_000_M"))    BREAK       //	PACKED_MP_BOOL_BLACK_HAWK_AND_LITTLE_LOGO_TEE 
			CASE 49  RETURN TRUE 	BREAK			///  (HASH("MP_Gunrunning_Award_005_FM"))    BREAK       //	PACKED_MP_BOOL_WHITE_SHREWSBURY_LOGO_TEE      
			CASE 50	 RETURN FALSE 	BREAK			///  (HASH("DLC_MP_GR_M_JBIB_19_0")) 	BREAK       //	PACKED_MP_BOOL_CLASS_OF_98_BLUE				
			CASE 51	 RETURN FALSE 	BREAK			///  (HASH("DLC_MP_GR_M_JBIB_19_1")) 	BREAK       //	PACKED_MP_BOOL_CLASS_OF_98_RED				
			CASE 52  RETURN TRUE 	BREAK			///  (HASH("MP_Gunrunning_Award_023_M"))    BREAK       //	PACKED_MP_BOOL_RSTAR_LOGO_INTERFERENCE        
			CASE 53  RETURN TRUE 	BREAK			///  (HASH("MP_Gunrunning_Award_024_M"))    BREAK       //	PACKED_MP_BOOL_ROCKSTAR_GAMES_INTERFERENCE    
			CASE 54  RETURN TRUE 	BREAK			///  (HASH("MP_Gunrunning_Award_021_M"))    BREAK       //	PACKED_MP_BOOL_ROCKSTAR_PINK_BLADE            
			CASE 55  RETURN TRUE 	BREAK			///  (HASH("MP_Gunrunning_Award_020_M"))    BREAK       //	PACKED_MP_BOOL_RSTAR_CAMO_LOGO_GREY           
			CASE 56  RETURN TRUE 	BREAK			///  (HASH("MP_Gunrunning_Award_020_M_ALT"))    BREAK       //	PACKED_MP_BOOL_RSTAR_CAMO_LOGO_WHITE          
			CASE 57  RETURN FALSE 	BREAK			///  (HASH("DLC_MP_GR_M_JBIB_20_0"))    BREAK       //	PACKED_MP_BOOL_RSTAR_LOGO_PATTERN_POCKET      
			
		ENDSWITCH

	RETURN FALSE
ENDFUNC


FUNC INT GET_XMAS_ITEM_COLLECTION(int xmastshirt)
	PRINTLN("GET_XMAS_ITEM_COLLECTION number passed in: ", xmastshirt)
		
	SWITCH xmastshirt
	    CASE 0		RETURN 				(HASH("mpHipster_overlays"		))	BREAK	//"FM_Hip_F_Retro_000"		  
		CASE 1		RETURN  			(HASH("mpHipster_overlays"		))	BREAK	//"FM_Hip_F_Retro_001"		
		CASE 2		RETURN  			(HASH("mpHipster_overlays"		))	BREAK	//"FM_Hip_F_Retro_002"		
		CASE 3		RETURN  			(HASH("mpHipster_overlays"		))	BREAK	//"FM_Hip_F_Retro_012"		
		CASE 4		RETURN  			(HASH("mpHipster_overlays"		))	BREAK	//"FM_Hip_F_Retro_003"		
		CASE 5		RETURN  			(HASH("mpHipster_overlays"		))	BREAK	//"FM_Hip_F_Retro_004"		
		CASE 6		RETURN  			(HASH("mpHipster_overlays"		))	BREAK	//"FM_Hip_F_Retro_005"		
		CASE 7		RETURN  			(HASH("mpHipster_overlays"		))	BREAK	//"FM_Hip_F_Retro_006"		
		CASE 8		RETURN  			(HASH("mpHipster_overlays"		))	BREAK	//"FM_Hip_F_Retro_008"		
		CASE 9		RETURN  			(HASH("mpHipster_overlays"		))	BREAK	//"FM_Hip_F_Retro_009"		
		CASE 10		RETURN  			(HASH("mpHipster_overlays"		))	BREAK	//"FM_Hip_F_Retro_011"		
		CASE 11		RETURN  			(HASH("mpHipster_overlays"		))	BREAK	//"FM_Hip_F_Retro_013"		
		CASE 12		RETURN  			(HASH("mpIndependence_overlays"	))	BREAK		//"FM_Ind_F_Award_000"		
		CASE 13		RETURN  			(HASH("mpLTS_overlays"			))	BREAK		//"FM_LTS_F_Tshirt_000"		
		CASE 14		RETURN  			(HASH("mpHipster_overlays"		))	BREAK			//"FM_Hip_F_Retro_007"		
		CASE 15		RETURN  			(HASH("mpHipster_overlays"		))	BREAK			//"FM_Hip_F_Retro_010"		
		CASE 16		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FILM_003_F"				
		CASE 17		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FAKE_DS_000_F"			
		CASE 18		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FAKE_DIS_001_F"			
		CASE 19		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FAKE_DIS_000_F"			
		CASE 20		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FAKE_ENEMA_000_F"		
		CASE 21		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FAKE_LB_000_F"			
		CASE 22		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FAKE_LC_000_F"			
		CASE 23		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FAKE_Per_000_F"			
		CASE 24		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FAKE_SC_000_F"			
		CASE 25		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FAKE_SN_000_F"			
		CASE 26		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FAKE_Vap_000_F"			
		CASE 27		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FILM_002_F"				
		CASE 28		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FILM_001_F"				
		CASE 29		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FILM_005_F"				
		CASE 30		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FILM_004_F"				
		CASE 31		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FILM_000_F"				
		CASE 32		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FILM_007_F"				
		CASE 33		RETURN  			(HASH("mpExecutive_overlays"		))	BREAK				//"MP_exec_prizes_001_F"		
		CASE 34		RETURN  			(HASH("mpBiker_overlays"			))	BREAK		//"MP_Biker_Tee_049_F"		
		CASE 35		RETURN  			(HASH("mpBiker_overlays"			))	BREAK		//"MP_Biker_Tee_053_F"		
		CASE 36		RETURN  			(HASH("mpHalloween_overlays"		))	BREAK			//"HW_Tee_010_F"				
		CASE 37		RETURN  			(HASH("mpHalloween_overlays"		))	BREAK			//"HW_Tee_011_F"				
		CASE 38		RETURN  			(HASH("mpExecutive_overlays"		))	BREAK		//"MP_exec_prizes_003_F"		
		CASE 39		RETURN  			(HASH("mpHalloween_overlays"		))	BREAK			//"HW_Tee_007_F"				
		CASE 40		RETURN  			(HASH("mpHalloween_overlays"		))	BREAK			//"HW_Tee_005_F"				
		CASE 41		RETURN  			(HASH("mpLuxe_overlays"			))	BREAK		//"MP_FILM_008_F"				
		CASE 42		RETURN  			(HASH("mpHalloween_overlays"		))	BREAK			//"HW_Tee_003_F"				
		CASE 43		RETURN  			(HASH("mpBiker_overlays"			))	BREAK		//"MP_Biker_Tee_050_F"		
		CASE 44		RETURN  			(HASH("mpHalloween_overlays"		))	BREAK		//"HW_Tee_000_F"			
		CASE 45  RETURN  (HASH("mpGunrunning_overlays")) 	BREAK
		CASE 46  RETURN  (HASH("mpGunrunning_overlays"))    BREAK
		CASE 47  RETURN  (HASH("mpGunrunning_overlays"))    BREAK
		CASE 48  RETURN  (HASH("mpGunrunning_overlays"))    BREAK
		CASE 49  RETURN  (HASH("mpGunrunning_overlays"))    BREAK
		CASE 50	 RETURN  (HASH("mpGunrunning_overlays")) 	BREAK
		CASE 51	 RETURN  (HASH("mpGunrunning_overlays")) 	BREAK
		CASE 52  RETURN  (HASH("mpGunrunning_overlays"))    BREAK
		CASE 53  RETURN  (HASH("mpGunrunning_overlays"))    BREAK
		CASE 54  RETURN  (HASH("mpGunrunning_overlays"))    BREAK
		CASE 55  RETURN  (HASH("mpGunrunning_overlays"))    BREAK
		CASE 56  RETURN  (HASH("mpGunrunning_overlays"))    BREAK
		CASE 57  RETURN  (HASH("mpGunrunning_overlays"))    BREAK
	
	ENDSWITCH
	SCRIPT_ASSERT("GET_XMAS_ITEM_COLLECTION invalid int passed in. Tell Kevin Wong")
	RETURN 0
ENDFUNC
FUNC STATS_PACKED GET_XMAS_GIFT_SHIRT_PACKED_STAT(int xmastshirt)
	PRINTLN("GET_XMAS_GIFT_SHIRT_PACKED_STAT number passed in: ", xmastshirt)
		
	SWITCH xmastshirt
		CASE 0		RETURN 	PACKED_STAT_BOOL_AWARD_Base5TShirt											BREAK
		CASE 1		RETURN  PACKED_STAT_BOOL_AWARD_BitchnDogFoodTShirt								BREAK
		CASE 2		RETURN  PACKED_STAT_BOOL_AWARD_BOBOTShirt										BREAK
		CASE 3		RETURN  PACKED_STAT_BOOL_AWARD_BounceFMTShirt                                       BREAK
		CASE 4		RETURN  PACKED_STAT_BOOL_AWARD_CrocsBarTShirt                                       BREAK
		CASE 5		RETURN  PACKED_STAT_BOOL_AWARD_Emotion983TShirt                                   BREAK
		CASE 6		RETURN  PACKED_STAT_BOOL_AWARD_Fever105TShirt                                       BREAK
		CASE 7		RETURN  PACKED_STAT_BOOL_AWARD_FlashTShirt                                          BREAK
		CASE 8		RETURN  PACKED_STAT_BOOL_AWARD_HomiesSharpTShirt                                    BREAK
		CASE 9		RETURN  PACKED_STAT_BOOL_AWARD_KDSTTShirt                                           BREAK
		CASE 10		RETURN  PACKED_STAT_BOOL_AWARD_KJAHRadioTShirt                                      BREAK
		CASE 11		RETURN  PACKED_STAT_BOOL_AWARD_KROSE                                              BREAK
		CASE 12		RETURN  PACKED_MP_STATS_GIVE_FOR_FREE_STATUE_OF_HAPPINESS_TEE                              BREAK
		CASE 13		RETURN  PACKED_STAT_BOOL_AWARD_VictoryFistTshirt                                    BREAK
		CASE 14		RETURN  PACKED_STAT_BOOL_AWARD_VinylCountdownTShirt                                 BREAK
		CASE 15		RETURN  PACKED_STAT_BOOL_AWARD_VivisectionTShirt	                                BREAK
		CASE 16		RETURN  PACKED_MP_FILM_DIE_ALREADY_4                                               BREAK
		CASE 17		RETURN  PACKED_MP_FAKE_DIDIER_SACHS_TSHIRT                                          BREAK
		CASE 18		RETURN  PACKED_MP_FAKE_DIX_GOLD_TSHIRT                                              BREAK
		CASE 19		RETURN  PACKED_MP_FAKE_DIX_WHITE_TSHIRT                                             BREAK
		CASE 20		RETURN  PACKED_MP_FAKE_ENEMA_TSHIRT_TSHIRT                                          BREAK
		CASE 21		RETURN  PACKED_MP_FAKE_LE_CHIEN_NO2_TSHIRT                                          BREAK
		CASE 22		RETURN  PACKED_MP_FAKE_LE_CHIEN_CREW_TSHIRT                                         BREAK
		CASE 23		RETURN  PACKED_MP_FAKE_PERSEUS_TSHIRT                                               BREAK
		CASE 24		RETURN  PACKED_MP_FAKE_SANTO_CAPRA_TSHIRT                                           BREAK
		CASE 25		RETURN  PACKED_MP_FAKE_SESSANTA_NOVE_TSHIRT                                         BREAK
		CASE 26		RETURN  PACKED_MP_FAKE_VAPID_TSHIRT                                                 BREAK
		CASE 27		RETURN  PACKED_MP_FILM_I_MARRIED_MY_DAD	                                        BREAK
		CASE 28		RETURN  PACKED_MP_FILM_MELTDOWN                                                    BREAK
		CASE 29		RETURN  PACKED_MP_FILM_NELSON_IN_NAPLES                                            BREAK
		CASE 30		RETURN  PACKED_MP_FILM_SHOULDER_OF_ORION                                           BREAK
		CASE 31		RETURN  PACKED_MP_FILM_VINEWOOD_ZOMBIE                                             BREAK
		CASE 32		RETURN  PACKED_MP_FILM_AMERICAN_DIVORCE                                            BREAK
		CASE 33		RETURN  PACKED_STAT_BOOL_SHIRT_BAHAMAMAMAS                                           BREAK
		CASE 34		RETURN  PACKED_MP_BOOL_TSHIRT_WESTERN_BIG_LOGO_BLACK                                BREAK
		CASE 35		RETURN  PACKED_MP_BOOL_TSHIRT_NAGASAKI_LOGO_BLACK_SHIRT_WHITE_STAR                  BREAK
		CASE 36		RETURN  PACKED_MP_AWARD_LOW_TSHIRT_BUTCHERY                                         BREAK
		CASE 37		RETURN  PACKED_MP_AWARD_LOW_TSHIRT_BOMBSHELL_BLOODBATH_CHEERLEADER_MASSACRE_3       BREAK
		CASE 38		RETURN  PACKED_STAT_BOOL_SHIRT_GROTTI                                               BREAK
		CASE 39		RETURN  PACKED_MP_AWARD_LOW_TSHIRT_KNIFE_AFTER_DARK                                 BREAK
		CASE 40		RETURN  PACKED_MP_AWARD_LOW_TSHIRT_PSYCHO_SWINGERS                                  BREAK
		CASE 41		RETURN  PACKED_MP_FILM_LONLIEST_ROBOT                                              BREAK
		CASE 42		RETURN  PACKED_MP_AWARD_LOW_TSHIRT_TWILIGHT_KNIFE                                   BREAK
		CASE 43		RETURN  PACKED_MP_BOOL_TSHIRT_WESTERN_BIG_LOGO_WHITE                                BREAK
		CASE 44		RETURN  PACKED_MP_AWARD_LOW_TSHIRT_ZOMBIE_LIBERALS_IN_THE_MIDWEST                   BREAK
		CASE 45  RETURN PACKED_MP_BOOL_BLACK_AMMUNATION_HOODIE			BREAK
		CASE 46  RETURN PACKED_MP_BOOL_WHITE_HAWK_AND_LITTLE_HOODIE	    BREAK
		CASE 47  RETURN PACKED_MP_BOOL_BLACK_COIL_TEE                   BREAK
		CASE 48  RETURN PACKED_MP_BOOL_BLACK_HAWK_AND_LITTLE_LOGO_TEE   BREAK
		CASE 49  RETURN PACKED_MP_BOOL_WHITE_SHREWSBURY_LOGO_TEE        BREAK
		CASE 50	 RETURN PACKED_MP_BOOL_CLASS_OF_98_BLUE					BREAK
		CASE 51	 RETURN PACKED_MP_BOOL_CLASS_OF_98_RED					BREAK
		CASE 52  RETURN PACKED_MP_BOOL_RSTAR_LOGO_INTERFERENCE          BREAK
		CASE 53  RETURN PACKED_MP_BOOL_ROCKSTAR_GAMES_INTERFERENCE      BREAK
		CASE 54  RETURN PACKED_MP_BOOL_ROCKSTAR_PINK_BLADE              BREAK
		CASE 55  RETURN PACKED_MP_BOOL_RSTAR_CAMO_LOGO_GREY             BREAK
		CASE 56  RETURN PACKED_MP_BOOL_RSTAR_CAMO_LOGO_WHITE            BREAK
		CASE 57  RETURN PACKED_MP_BOOL_RSTAR_LOGO_PATTERN_POCKET        BREAK
		
	ENDSWITCH
	SCRIPT_ASSERT("GET_XMAS_GIFT_SHIRT_PACKED_STAT invalid int passed in. Tell Kevin Wong")
	RETURN (PACKED_MP_AWARD_LOW_TSHIRT_ZOMBIE_LIBERALS_IN_THE_MIDWEST)
ENDFUNC
FUNC BOOL GET_XMAS_SHIRT_NOT_OWNED_BY_PLAYER(INT &iRetShirt)
	iRetShirt = -1
	CONST_INT numberofshirts 57
	INT iAvailableShirtsID[numberofshirts]
	INT iTotalShirts
	INT iShirt
	
		BOOL bIsPlayerMale = (GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0)
		

		REPEAT numberofshirts iShirt
		IF NOT DOES_2017_TSHIRT_REWARD_USE_TATTOO_CHECK (iShirt)
			IF NOT GET_PACKED_STAT_BOOL(GET_XMAS_GIFT_SHIRT_PACKED_STAT(iShirt))
			//AND  IS_DLC_OVERLAY_LOCKED_BY_SCRIPT( GET_XMAS_ITEM_COLLECTION(iShirt)     ,GET_XMAS16_GIFT_SHIRT_HASH(iShirt))
			AND NOT IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(GET_XMAS16_GIFT_SHIRT_HASH(iShirt), PED_COMPONENT_ACQUIRED_SLOT)	
				
				PRINTLN("GET_XMAS_SHIRT_NOT_OWNED_BY_PLAYER is not owned", iShirt)
				iAvailableShirtsID[iTotalShirts] = iShirt
				iTotalShirts++
			ELSE
				PRINTLN("GET_XMAS_SHIRT_NOT_OWNED_BY_PLAYER is already owned", iShirt)
				IF NOT IS_DLC_OVERLAY_LOCKED_BY_SCRIPT( GET_XMAS_ITEM_COLLECTION(iShirt) ,GET_XMAS16_GIFT_SHIRT_HASH(iShirt), INVALID_TATTOO)
					PRINTLN("GET_XMAS_SHIRT_NOT_OWNED_BY_PLAYER IS_DLC_OVERLAY_LOCKED_BY_SCRIPT FALSE for", iShirt)
				ENDIF
				
				IF GET_PACKED_STAT_BOOL(GET_XMAS_GIFT_SHIRT_PACKED_STAT(iShirt))
					PRINTLN("GET_XMAS_SHIRT_NOT_OWNED_BY_PLAYER GET_PACKED_STAT_BOOL true for", iShirt)
				ENDIF
			ENDIF
			
		ELSE
			// is player male or female
			TATTOO_FACTION_ENUM tamptattoofaction 

			IF 	bIsPlayerMale
				tamptattoofaction = TATTOO_MP_FM
			ELSE
				tamptattoofaction = TATTOO_MP_FM_F
			ENDIF

			IF NOT (GET_PACKED_STAT_BOOL(GET_XMAS_GIFT_SHIRT_PACKED_STAT(iShirt))
			AND  NOT IS_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_XMAS16_GIFT_SHIRT_HASH(iShirt), tamptattoofaction)))   
				PRINTLN("GET_XMAS_SHIRT_NOT_OWNED_BY_PLAYER tattoo check is not owned", iShirt)
				iAvailableShirtsID[iTotalShirts] = iShirt
				iTotalShirts++
			ELSE
				PRINTLN("GET_XMAS_SHIRT_NOT_OWNED_BY_PLAYER tattoo check is already owned", iShirt)
				IF NOT IS_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_XMAS16_GIFT_SHIRT_HASH(iShirt), tamptattoofaction))     
					PRINTLN("GET_XMAS_SHIRT_NOT_OWNED_BY_PLAYER tattoo check  IS_MP_TATTOO_UNLOCKED FALSE for", iShirt)
				ENDIF
				
				IF GET_PACKED_STAT_BOOL(GET_XMAS_GIFT_SHIRT_PACKED_STAT(iShirt))
					PRINTLN("GET_XMAS_SHIRT_NOT_OWNED_BY_PLAYER tattoo check  GET_PACKED_STAT_BOOL true for", iShirt)
				ENDIF
			ENDIF
		
		ENDIF
	ENDREPEAT
	
	
	PRINTLN("GET_XMAS_SHIRT_NOT_OWNED_BY_PLAYER iTotalShirts = ", iTotalShirts)
	IF iTotalShirts > 0
		// Shuffle list a few times.
		INT i, j, k, iTempShirt
		REPEAT 3 k
			REPEAT iTotalShirts i
				j = GET_RANDOM_INT_IN_RANGE(0, iTotalShirts)
				iTempShirt = iAvailableShirtsID[i]
				iAvailableShirtsID[i] = iAvailableShirtsID[j]
				iAvailableShirtsID[j] = iTempShirt
			ENDREPEAT
		ENDREPEAT
		iRetShirt = iAvailableShirtsID[0]
	ENDIF
	
	RETURN (iRetShirt != -1)
ENDFUNC



FUNC BOOL PROCESS_GIFTING_AMMO(WEAPON_TYPE aweapon, BOOL bApplyAmmo = TRUE)

								
	AMMO_TYPE eAmmoType
	GET_WEAPON_AMMO_TYPE_FROM_WEAPON_TYPE( aweapon, eAmmoType )
	
	

	IF bApplyAmmo
		IF aweapon != WEAPONTYPE_UNARMED
			IF IS_MP_WEAPON_PURCHASED(aweapon)
			OR HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), aweapon)
		//	OR IS_MP_WEAPON_EQUIPPED(aweapon)
				INT iAmmoCount 
				GET_MAX_AMMO(PLAYER_PED_ID(), aweapon, iAmmoCount)
				INT ammo_in_weapon_temp = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), aweapon)
				IF iAmmoCount > ammo_in_weapon_temp
					INT ammotogive = iAmmoCount - ammo_in_weapon_temp
					ADD_AMMO_TO_PED(PLAYER_PED_ID(), aweapon, ammotogive)
					CHECK_AND_SAVE_WEAPON(aweapon)
					PRINTLN(" [ammo/armour] PROCESS_GIFTING_AMMO - Purchased: Ammo: ", iAmmoCount, ", Remaining Cash: $", GET_PLAYER_CASH(PLAYER_ID()))

				ENDIF
			ELSE
				PRINTLN(" [ammo/armour] PROCESS_GIFTING_AMMO - Not awarding. This weapon is not purchased or equiped ", GET_WEAPON_NAME(aweapon))
			ENDIF
		ENDIF
	ENDIF
	


				
	RETURN FALSE
	


ENDFUNC

FUNC BOOL PROCESS_GIFTING_FULL_AMMO(BOOL bApplyAmmo = TRUE)

	INT iWeaponSlot
	WEAPON_TYPE aWeapon


	// Loop over our normal weapons
	FOR iWeaponSlot = 0 TO (ENUM_TO_INT(NUM_WEAPONSLOTS)-1) STEP 1
		
		// Get the weapon type from the slot
		IF GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot) = WEAPONSLOT_INVALID
			aWeapon = WEAPONTYPE_INVALID
		ELSE
			aWeapon = GET_PED_WEAPONTYPE_IN_SLOT(PLAYER_PED_ID(), GET_PLAYER_PED_WEAPON_SLOT_FROM_INT(iWeaponSlot))
		ENDIF
		
		IF  aWeapon != WEAPONTYPE_INVALID  
			IF PROCESS_GIFTING_AMMO(aWeapon, bApplyAmmo)
				PRINTLN(" [ammo/armour] PROCESS_GIFTING_FULL_AMMO - Purchasing ammo for: ", GET_WEAPON_NAME(aWeapon))
					
			ENDIF
		ENDIF
		// Is the weapon valid for the corona?
			

	ENDFOR
	
	// Process our DLC weapons in the same way	
	scrShopWeaponData sWeaponData
	FOR iWeaponSlot = 0 TO (GET_NUM_DLC_WEAPONS()-1) STEP 1
		
		IF GET_DLC_WEAPON_DATA(iWeaponSlot, sWeaponData)

			aWeapon = INT_TO_ENUM(WEAPON_TYPE, sWeaponData.m_nameHash)
			IF PROCESS_GIFTING_AMMO(aWeapon, bApplyAmmo)
				PRINTLN(" [ammo/armour] PROCESS_GIFTING_FULL_AMMO - Purchasing ammo for: ", GET_WEAPON_NAME(aWeapon))
				
			ENDIF


		ENDIF
	ENDFOR
	
	// give full pistol rank 1
	IF GET_RANK_FROM_XP_VALUE(GET_STAT_CHARACTER_XP()) = 1
		INT iAmmoCountMAx
		INT ammo_in_weapon_temp = GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), aweapon)
		GET_MAX_AMMO(PLAYER_PED_ID(), aweapon, iAmmoCountMAx)
		INT ammotogive = iAmmoCountMAx - ammo_in_weapon_temp
		IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)

			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, ammotogive)
			PRINTLN(" [ammo/armour] PROCESS_GIFTING_AMMO giving rank 1 player pistol and pistol ammo", GET_WEAPON_NAME(aweapon))
		ELSE
			ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, ammotogive)
			PRINTLN(" [ammo/armour] PROCESS_GIFTING_AMMO giving rank 1 player full pistol ammo", GET_WEAPON_NAME(aweapon))
		ENDIF
		CHECK_AND_SAVE_WEAPON(WEAPONTYPE_PISTOL)
	
	ENDIF

	RETURN TRUE
ENDFUNC


FUNC INT GET_XMAS_GIFT_ITEM_ID_FROM_WEAPON_TYPE(WEAPON_TYPE eWeaponType)

	SWITCH eWeaponType
	
		// other
		CASE WEAPONTYPE_PETROLCAN 					RETURN 		HASH("WP_WT_PETROL_t5_v1")
		
		// melee
		CASE WEAPONTYPE_KNIFE						RETURN		HASH("WP_WT_KNIFE_t1_v1")
		CASE WEAPONTYPE_NIGHTSTICK					RETURN 		HASH("WP_WT_NGTSTK_t1_v1")
		CASE WEAPONTYPE_HAMMER						RETURN 		HASH("WP_WT_HAMMER_t1_v1")
		CASE WEAPONTYPE_BAT							RETURN -1
		CASE WEAPONTYPE_CROWBAR						RETURN -1
		CASE WEAPONTYPE_GOLFCLUB					RETURN -1
		CASE WEAPONTYPE_DLC_BOTTLE					RETURN		HASH("WP_WT_BOTTLE_t1_v1")
		CASE WEAPONTYPE_DLC_DAGGER					RETURN		HASH("WP_WT_DAGGER_t1_v1")
		
		// proj
		CASE WEAPONTYPE_GRENADE 					RETURN		HASH("WP_WT_GNADE_t2_v2")		
		CASE WEAPONTYPE_MOLOTOV						RETURN -1
		CASE WEAPONTYPE_STICKYBOMB 					RETURN 		HASH("WP_WT_GNADE_STK_t2_v2")
		CASE WEAPONTYPE_SMOKEGRENADE				RETURN 		HASH("WP_WT_GNADE_SMK_t2_v2")
		CASE WEAPONTYPE_DLC_PROXMINE				RETURN		HASH("WP_WT_PRXMINE_t2_v2")
		
		// gun
		CASE WEAPONTYPE_COMBATPISTOL				RETURN 		HASH("WP_WT_PIST_CBT_t0_v1")
		CASE WEAPONTYPE_MICROSMG					RETURN		HASH("WP_WT_SMG_MCR_t0_v1")
		CASE WEAPONTYPE_ASSAULTRIFLE				RETURN 		HASH("WP_WT_RIFLE_ASL_t0_v1")
		CASE WEAPONTYPE_PUMPSHOTGUN					RETURN		HASH("WP_WT_SG_PMP_t0_v1")
		CASE WEAPONTYPE_SNIPERRIFLE					RETURN		HASH("WP_WT_SNIP_RIF_t0_v1")
		CASE WEAPONTYPE_CARBINERIFLE				RETURN	    HASH("WP_WT_RIFLE_CBN_t0_v1")
		CASE WEAPONTYPE_DLC_MARKSMANRIFLE			RETURN	    HASH("WP_WT_MKRIFLE_t0_v1")
		CASE WEAPONTYPE_DLC_KNUCKLE    				RETURN	    HASH("WP_WT_KNUCKLE_t1_v1")
		CASE WEAPONTYPE_MINIGUN    					RETURN	    HASH("WP_WT_MINIGUN_t0_v1")
	ENDSWITCH

	CASSERTLN(DEBUG_FMPICKUPS, "GET_XMAS_GIFT_ITEM_ID_FROM_WEAPON_TYPE - weapon type not listed ", eWeaponType)
	RETURN -1

ENDFUNC
PROC SET_TRANSACTION_FOR_WEAPON(WEAPON_TYPE eWeaponType)
	
	INT iItemID
	IF USE_SERVER_TRANSACTIONS()
		iItemID = GET_XMAS_GIFT_ITEM_ID_FROM_WEAPON_TYPE(eWeaponType)	
		IF (iItemID != -1)
		#IF IS_DEBUG_BUILD
		AND NOT (g_bSkipCashTransaction)
		#ENDIF
			IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_WEAPON, iItemID, NET_SHOP_ACTION_SPEND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, DEFAULT, CTPF_AUTO_RETRY|CTPF_FAIL_ALERT|CTPF_AUTO_PROCESS_REPLY)
				NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
			ENDIF
		ENDIF
	ENDIF
	
	SET_MP_WEAPON_PURCHASED(eWeaponType, TRUE)
	
	// melee weapons might need to have their ammo set
	

ENDPROC


PROC UNLOCK_SPECIAL_TUNABLE_ITEMS()

	BOOL bIsPlayerMale = (GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0)
	
	//2021
	//	23rd December:
	//- Festive Mask Design 1 (TBC)
	//- Festive T-shirt Design 1 (TBC)
	//- Firework Launcher
	//- 20 x Firework Rockets
	//- Full Snacks
	//- Full Armor
	//- 25 x Sticky Bombs
	//- 25 x Grenades
	//- 10 x Proximity Mines
	//- 10 x Molotovs
	
	
	IF g_sMPTunables.btoggle_2021_Christmas_gift
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2021_CHRISTMAS_GIFT_AWARDED	) 
			PRINT_TICKER("UNLOCK_21XMASDAY")
			
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2021_CHRISTMAS_GIFT_AWARDED	, TRUE) 
			PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)	
			PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS btoggle_2021_Christmas_gift TRUE")
				// 20* firework launcher
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 20, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 20)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 20)
			
			// Festive tee
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_FESTIVE_TEE_RED, TRUE)
			
			// clownfish mask
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_FISHMASK_4, TRUE) 
			// Full armour and snacks
			APPLY_ARMOUR_GIFTING(GET_ACTIVE_CHARACTER_SLOT(), GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE))
			NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
			
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS) > INV_SNACK_1_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT YUM SNACKS TO INV_SNACK_1_MAX = ", INV_SNACK_1_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS) > INV_SNACK_2_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
				PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT HEALTH SNACKS TO INV_SNACK_2_MAX = ", INV_SNACK_2_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS) > INV_SNACK_3_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT EPIC SNACKS TO INV_SNACK_3_MAX = ", INV_SNACK_3_MAX, " ")
			ENDIF
			
			INT iItemHash
			INT iItemCategory = hash("SNACK")
			INT iShopType = hash("EVENT")
			INT iReason = hash("GET")
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_EPIC_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_YUM_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_HEALTHY_SNACKS, TRUE) 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, INV_DRINK_1_MAX) 
			iItemHash = hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_DRINK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_2_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_3_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			
			
			
			
			
			
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_MOLOTOV, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_MOLOTOV, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_MOLOTOV, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_MOLOTOV, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MOLOTOV, 10, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MOLOTOV_FM_AMMO_BOUGHT, 10)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MOLOTOV_FM_AMMO_CURRENT, 10)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_GRENADE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_GRENADE, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_GRENADE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_GRENADE, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 25, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_FM_AMMO_BOUGHT, 25)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_FM_AMMO_CURRENT, 25)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_STICKYBOMB, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_STICKYBOMB, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_STICKYBOMB, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_STICKYBOMB, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, 25, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_FM_AMMO_BOUGHT, 25)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_FM_AMMO_CURRENT, 25)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_PROXMINE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_PROXMINE, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_PROXMINE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_PROXMINE, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_PROXMINE, 5, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_BOUGHT , 5)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_CURRENT, 5)

		ENDIF	
	ENDIF

	//30th December:
	//- Festive Mask Design 2 (TBC)
	//- Festive T-shirt Design 2 (TBC)
	//- Firework Launcher
	//- 20 x Firework Rockets
	//- Full Snacks
	//- Full Armor
	//- 25 x Sticky Bombs
	//- 25 x Grenades
	//- 10 x Proximity Mines
	//- 10 x Molotovs
	IF g_sMPTunables.btoggle_2021_New_Years_gift
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2021_NEWYEARS_GIFT_AWARDED	) 
			PRINT_TICKER("UNLOCK_21NEWYEARS")
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2021_NEWYEARS_GIFT_AWARDED	, TRUE) 
			PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)	
			PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS btoggle_2021_New_Years_gift TRUE")
				// 20* firework launcher
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 20, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 20)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 20)
			
			// Festive tee
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_FESTIVE_TEE_GREEN, TRUE)
			// Brown Seal
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_SEALMASK_1, TRUE) 
			// Full armour and snacks
			APPLY_ARMOUR_GIFTING(GET_ACTIVE_CHARACTER_SLOT(), GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE))
			NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
			
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS) > INV_SNACK_1_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT YUM SNACKS TO INV_SNACK_1_MAX = ", INV_SNACK_1_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS) > INV_SNACK_2_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
				PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT HEALTH SNACKS TO INV_SNACK_2_MAX = ", INV_SNACK_2_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS) > INV_SNACK_3_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT EPIC SNACKS TO INV_SNACK_3_MAX = ", INV_SNACK_3_MAX, " ")
			ENDIF
			
			INT iItemHash
			INT iItemCategory = hash("SNACK")
			INT iShopType = hash("EVENT")
			INT iReason = hash("GET")
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_EPIC_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_YUM_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_HEALTHY_SNACKS, TRUE) 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, INV_DRINK_1_MAX) 
			iItemHash = hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_DRINK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_2_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_3_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_MOLOTOV, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_MOLOTOV, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_MOLOTOV, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_MOLOTOV, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MOLOTOV, 10, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MOLOTOV_FM_AMMO_BOUGHT, 10)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MOLOTOV_FM_AMMO_CURRENT, 10)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_GRENADE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_GRENADE, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_GRENADE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_GRENADE, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 25, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_FM_AMMO_BOUGHT, 25)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_FM_AMMO_CURRENT, 25)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_STICKYBOMB, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_STICKYBOMB, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_STICKYBOMB, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_STICKYBOMB, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, 25, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_FM_AMMO_BOUGHT, 25)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_FM_AMMO_CURRENT, 25)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_PROXMINE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_PROXMINE, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_PROXMINE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_PROXMINE, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_PROXMINE, 5, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_BOUGHT , 5)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_CURRENT, 5)

		ENDIF	
	ENDIF
	
	
	
//	Christmas Eve Gift:
//	Players should receive
//	- Green Reindeer Lights Bodysuit
//	- Firework Launcher
//	- 20 x Firework Rockets
//	- Full Snacks
//	- Full Armor
	IF g_sMPTunables.btoggle_2020_Christmas_day_gift
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2020_CHRISTMAS_EVE_GIFT_AWARDED	) 
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2020_CHRISTMAS_EVE_GIFT_AWARDED	, TRUE) 
			PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)	
			PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS toggle_2020_Christmas_day_gift TRUE")
			
			
			SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_MASK_FULL_HEAD_10	, TRUE) //Festive Mask
			
			
			// 20* firework launcher
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 20, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 20)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 20)
			
			
			// Full armour and snacks
			APPLY_ARMOUR_GIFTING(GET_ACTIVE_CHARACTER_SLOT(), GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE))
			NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
			
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS) > INV_SNACK_1_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT YUM SNACKS TO INV_SNACK_1_MAX = ", INV_SNACK_1_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS) > INV_SNACK_2_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
				PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT HEALTH SNACKS TO INV_SNACK_2_MAX = ", INV_SNACK_2_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS) > INV_SNACK_3_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT EPIC SNACKS TO INV_SNACK_3_MAX = ", INV_SNACK_3_MAX, " ")
			ENDIF
			
			INT iItemHash
			INT iItemCategory = hash("SNACK")
			INT iShopType = hash("EVENT")
			INT iReason = hash("GET")
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_EPIC_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_YUM_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_HEALTHY_SNACKS, TRUE) 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, INV_DRINK_1_MAX) 
			iItemHash = hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_DRINK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_2_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_3_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_MOLOTOV, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_MOLOTOV, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_MOLOTOV, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_MOLOTOV, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MOLOTOV, 10, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MOLOTOV_FM_AMMO_BOUGHT, 10)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MOLOTOV_FM_AMMO_CURRENT, 10)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_GRENADE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_GRENADE, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_GRENADE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_GRENADE, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 25, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_FM_AMMO_BOUGHT, 25)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_FM_AMMO_CURRENT, 25)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_STICKYBOMB, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_STICKYBOMB, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_STICKYBOMB, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_STICKYBOMB, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, 25, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_FM_AMMO_BOUGHT, 25)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_FM_AMMO_CURRENT, 25)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_PROXMINE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_PROXMINE, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_PROXMINE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_PROXMINE, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_PROXMINE, 5, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_BOUGHT , 5)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_CURRENT, 5)
				
			PRINT_TICKER("UNLOCK_20XMASDAY")
			
			
		ENDIF
	
	
	
	ENDIF
	
	
	IF g_sMPTunables.btoggle_2019_christmas_eve_gift = TRUE
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2019_CHRISTMAS_EVE_GIFT_AWARDED	) 
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2019_CHRISTMAS_EVE_GIFT_AWARDED	, TRUE) 
			PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)	
			
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_XMAS2019_GREEN_REINDEER_LIGHTS_BODYSUIT	, TRUE) 
			
			// 20* firework launcher
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 20, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 20)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 20)
			
			
			// Full armour and snacks
			APPLY_ARMOUR_GIFTING(GET_ACTIVE_CHARACTER_SLOT(), GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE))
			NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
			
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS) > INV_SNACK_1_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT YUM SNACKS TO INV_SNACK_1_MAX = ", INV_SNACK_1_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS) > INV_SNACK_2_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
				PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT HEALTH SNACKS TO INV_SNACK_2_MAX = ", INV_SNACK_2_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS) > INV_SNACK_3_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT EPIC SNACKS TO INV_SNACK_3_MAX = ", INV_SNACK_3_MAX, " ")
			ENDIF
			
			INT iItemHash
			INT iItemCategory = hash("SNACK")
			INT iShopType = hash("EVENT")
			INT iReason = hash("GET")
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_EPIC_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_YUM_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_HEALTHY_SNACKS, TRUE) 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, INV_DRINK_1_MAX) 
			iItemHash = hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_DRINK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_2_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_3_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			
			PRINT_TICKER("UNLOCK_19XMASEVE")
			PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS btoggle_2019_christmas_eve_gift ")	
		ENDIF
	ENDIF
	

	
	
//Christmas Day Gift:
//Players should receive:
//- Die Hard Style Jumper (set up url:bugstar:6155276) Could we get a placeholder jumper set up for this in the meantime?
//- Red Nose Festive Lights Bodysuit	
//- Firework Launcher
//- 20 x Firework Rockets
//- Full Snacks
//- Full Armor
//- Minigun + full Ammo
//- 25 x Sticky Bombs
//- 25 x Grenades
//- 10 x Proximity Mines
//- 10 x Molotovs
	IF g_sMPTunables.btoggle_2019_Christmas_day_gift = TRUE
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2019_CHRISTMAS_DAY_GIFT_AWARDED	) 
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2019_CHRISTMAS_DAY_GIFT_AWARDED	, TRUE) 
			PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)
			
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_XMAS2019_MINIGUN_SWEATER, TRUE) 
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_XMAS2019_TRADITIONAL_FESTIVE_LIGHTS_BODYSUIT	, TRUE) 
			// 20* firework launcher
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)

			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 20, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 20)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 20)
			
			
			//  MINI GUN
			
			SET_TRANSACTION_FOR_WEAPON(WEAPONTYPE_MINIGUN)

			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_MINIGUN, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_MINIGUN, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_MINIGUN, TRUE)

			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MINIGUN, 500, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MINIGUNS_FM_AMMO_BOUGHT, 500)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MINIGUNS_FM_AMMO_CURRENT, 500)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_MOLOTOV, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_MOLOTOV, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_MOLOTOV, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_MOLOTOV, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MOLOTOV, 10, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MOLOTOV_FM_AMMO_BOUGHT, 10)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MOLOTOV_FM_AMMO_CURRENT, 10)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_GRENADE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_GRENADE, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_GRENADE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_GRENADE, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 25, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_FM_AMMO_BOUGHT, 25)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_FM_AMMO_CURRENT, 25)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_STICKYBOMB, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_STICKYBOMB, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_STICKYBOMB, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_STICKYBOMB, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, 25, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_FM_AMMO_BOUGHT, 25)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_FM_AMMO_CURRENT, 25)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_PROXMINE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_PROXMINE, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_PROXMINE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_PROXMINE, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_PROXMINE, 5, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_BOUGHT , 5)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_CURRENT, 5)
				
			
						
			// Full armour and snacks
			APPLY_ARMOUR_GIFTING(GET_ACTIVE_CHARACTER_SLOT(), GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE))
			NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
			
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS) > INV_SNACK_1_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT YUM SNACKS TO INV_SNACK_1_MAX = ", INV_SNACK_1_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS) > INV_SNACK_2_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
				PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT HEALTH SNACKS TO INV_SNACK_2_MAX = ", INV_SNACK_2_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS) > INV_SNACK_3_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT EPIC SNACKS TO INV_SNACK_3_MAX = ", INV_SNACK_3_MAX, " ")
			ENDIF
			
			INT iItemHash
			INT iItemCategory = hash("SNACK")
			INT iShopType = hash("EVENT")
			INT iReason = hash("GET")
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_EPIC_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_YUM_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_HEALTHY_SNACKS, TRUE) 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, INV_DRINK_1_MAX) 
			iItemHash = hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_DRINK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_2_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_3_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			
			PRINT_TICKER("UNLOCK_19XMASDAY")
			PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS btoggle_2019_Christmas_day_gift ")
		ENDIF
	ENDIF	
	

	


	
	
//New Year’s Eve Gift
//Players should receive:
//- Yellow Reindeer Lights Bodysuit	
//- Firework Launcher
//- 20 x Firework Rockets
//- Full Snacks
//- Full Armor	
	IF g_sMPTunables.btoggle_2019_new_years_eve_gift = TRUE
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2019_NEW_YEARS_EVE_GIFT_AWARDED	) 
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2019_NEW_YEARS_EVE_GIFT_AWARDED	, TRUE) 
			PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)
			
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_XMAS2019_YELLOW_REINDEER_LIGHTS_BODYSUIT	, TRUE) 
			// 20* firework launcher
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)

			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 20, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 20)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 20)
			
						
			// Full armour and snacks
			APPLY_ARMOUR_GIFTING(GET_ACTIVE_CHARACTER_SLOT(), GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE))
			NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
			
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS) > INV_SNACK_1_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT YUM SNACKS TO INV_SNACK_1_MAX = ", INV_SNACK_1_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS) > INV_SNACK_2_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
				PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT HEALTH SNACKS TO INV_SNACK_2_MAX = ", INV_SNACK_2_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS) > INV_SNACK_3_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT EPIC SNACKS TO INV_SNACK_3_MAX = ", INV_SNACK_3_MAX, " ")
			ENDIF
			
			INT iItemHash
			INT iItemCategory = hash("SNACK")
			INT iShopType = hash("EVENT")
			INT iReason = hash("GET")
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_EPIC_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_YUM_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_HEALTHY_SNACKS, TRUE) 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, INV_DRINK_1_MAX) 
			iItemHash = hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_DRINK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_2_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_3_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			
			PRINT_TICKER("UNLOCK_19NYEVE")
			PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS btoggle_2019_new_years_eve_gift ")
		ENDIF
	ENDIF
	


	
//New Year’s Day Gift
//Players should receive:
//- Green Nose Festive Lights Bodysuit	
//- Firework Launcher
//- 20 x Firework Rockets
//- Full Snacks
//- Full Armor
	IF g_sMPTunables.btoggle_2019_new_years_day_gift = TRUE
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2019_NEW_YEARS_DAY_GIFT_AWARDED	) 
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2019_NEW_YEARS_DAY_GIFT_AWARDED	, TRUE) 
			PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)	
			
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_XMAS2019_NEON_FESTIVE_LIGHTS_BODYSUIT	, TRUE)
			
			
			// 20* firework launcher
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)

			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 20, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 20)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 20)
			
			
						
			// Full armour and snacks
			APPLY_ARMOUR_GIFTING(GET_ACTIVE_CHARACTER_SLOT(), GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE))
			NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
			
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS) > INV_SNACK_1_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT YUM SNACKS TO INV_SNACK_1_MAX = ", INV_SNACK_1_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS) > INV_SNACK_2_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
				PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT HEALTH SNACKS TO INV_SNACK_2_MAX = ", INV_SNACK_2_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS) > INV_SNACK_3_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT EPIC SNACKS TO INV_SNACK_3_MAX = ", INV_SNACK_3_MAX, " ")
			ENDIF
			
			INT iItemHash
			INT iItemCategory = hash("SNACK")
			INT iShopType = hash("EVENT")
			INT iReason = hash("GET")
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_EPIC_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_YUM_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_HEALTHY_SNACKS, TRUE) 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, INV_DRINK_1_MAX) 
			iItemHash = hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_DRINK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_2_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_3_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			PRINT_TICKER("UNLOCK_19NYDAY")
			PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS btoggle_2019_new_years_day_gift ")
		ENDIF
	ENDIF
	
	

	

		//  1x Horror Sweater (url:bugstar:5382797) 
		//  Firework Launcher
		//  10x Firework rockets
		//  Full Snacks
		//  Full Armor
		IF g_sMPTunables.btoggle_2018_christmas_eve_gift = TRUE
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2018_CHRISTMAS_EVE_GIFT_AWARDED	) 
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2018_CHRISTMAS_EVE_GIFT_AWARDED	, TRUE) 
				PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)
		
				SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
				SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
				SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
				SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 10, FALSE)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 10)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 10)
				
				
				APPLY_ARMOUR_GIFTING(GET_ACTIVE_CHARACTER_SLOT(), GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE))
				NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
				
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS) > INV_SNACK_1_MAX
					SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
					PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT YUM SNACKS TO INV_SNACK_1_MAX = ", INV_SNACK_1_MAX, " ")
				ENDIF
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS) > INV_SNACK_2_MAX
					SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
					PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT HEALTH SNACKS TO INV_SNACK_2_MAX = ", INV_SNACK_2_MAX, " ")
				ENDIF
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS) > INV_SNACK_3_MAX
					SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
					PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT EPIC SNACKS TO INV_SNACK_3_MAX = ", INV_SNACK_3_MAX, " ")
				ENDIF
				
				INT iItemHash
				INT iItemCategory = hash("SNACK")
				INT iShopType = hash("EVENT")
				INT iReason = hash("GET")
				SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_EPIC_SNACKS, TRUE) 
				SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_YUM_SNACKS, TRUE) 
				SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_HEALTHY_SNACKS, TRUE) 
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, INV_DRINK_1_MAX) 
				iItemHash = hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0")
				SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_DRINK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
				iItemHash = hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0")
				SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
				iItemHash = hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0")
				SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_2_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
				iItemHash = hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0")
				SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_3_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
				IF  GET_PLAYER_RANK(PLAYER_ID()) >= 5
					PRINT_TICKER("UNLOCK_18_XMASEVE")
				ELSE
					PRINT_TICKER("UNLOCK_18bXMASEVE") // no armour
				ENDIF
			
			ENDIF
		ENDIF
		
		
		
		//  Christmas DayGift:
		//  Players should receive:
		//  -	Ray Gun
		//  -	1x Horror Sweaters
		//  -	Firework Launcher
		//  -	10x Firework rockets
		//  -	25x Sticky Bombs
		//  -	25x Grenades
		//  -	5x Proximity Mines
		//  -	5x Molotovs
		//  -	Full Snacks
		//  -	Full Armor
		IF g_sMPTunables.btoggle_2018_Christmas_day_gift = TRUE
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2018_CHRISTMAS_DAY_GIFT_AWARDED	) 
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2018_CHRISTMAS_DAY_GIFT_AWARDED	, TRUE) 
				SET_PACKED_STAT_BOOL( PACKED_MP_BOOL_WEAPON_RAYGUN, TRUE)
				PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)
				
				SET_MP_WEAPON_PURCHASED(WEAPONTYPE_MOLOTOV, TRUE)
				SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_MOLOTOV, TRUE, FALSE)
				SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_MOLOTOV, TRUE)
				SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_MOLOTOV, TRUE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MOLOTOV, 5, FALSE)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MOLOTOV_FM_AMMO_BOUGHT, 5)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MOLOTOV_FM_AMMO_CURRENT, 5)
				
				SET_MP_WEAPON_PURCHASED(WEAPONTYPE_GRENADE, TRUE)
				SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_GRENADE, TRUE, FALSE)
				SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_GRENADE, TRUE)
				SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_GRENADE, TRUE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 25, FALSE)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_FM_AMMO_BOUGHT, 25)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_FM_AMMO_CURRENT, 25)
				
				SET_MP_WEAPON_PURCHASED(WEAPONTYPE_STICKYBOMB, TRUE)
				SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_STICKYBOMB, TRUE, FALSE)
				SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_STICKYBOMB, TRUE)
				SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_STICKYBOMB, TRUE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, 25, FALSE)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_FM_AMMO_BOUGHT, 25)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_FM_AMMO_CURRENT, 25)
				
				SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_PROXMINE, TRUE)
				SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_PROXMINE, TRUE, FALSE)
				SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_PROXMINE, TRUE)
				SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_PROXMINE, TRUE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_PROXMINE, 5, FALSE)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_BOUGHT , 5)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_CURRENT, 5)
				
		
				SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
				SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
				SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
				SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 10, FALSE)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 10)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 10)
				
				
				APPLY_ARMOUR_GIFTING(GET_ACTIVE_CHARACTER_SLOT(), GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE))
				NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
				
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS) > INV_SNACK_1_MAX
					SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
					PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT YUM SNACKS TO INV_SNACK_1_MAX = ", INV_SNACK_1_MAX, " ")
				ENDIF
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS) > INV_SNACK_2_MAX
					SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
					PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT HEALTH SNACKS TO INV_SNACK_2_MAX = ", INV_SNACK_2_MAX, " ")
				ENDIF
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS) > INV_SNACK_3_MAX
					SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
					PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT EPIC SNACKS TO INV_SNACK_3_MAX = ", INV_SNACK_3_MAX, " ")
				ENDIF
				
				INT iItemHash
				INT iItemCategory = hash("SNACK")
				INT iShopType = hash("EVENT")
				INT iReason = hash("GET")
				SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_EPIC_SNACKS, TRUE) 
				SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_YUM_SNACKS, TRUE) 
				SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_HEALTHY_SNACKS, TRUE) 
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, INV_DRINK_1_MAX) 
				iItemHash = hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0")
				SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_DRINK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
				iItemHash = hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0")
				SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
				iItemHash = hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0")
				SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_2_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
				iItemHash = hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0")
				SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_3_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
				
				IF  GET_PLAYER_RANK(PLAYER_ID()) >= 5
					PRINT_TICKER("UNLOCK_18_XMASDAY")
				ELSE
					PRINT_TICKER("UNLOCK_18bXMASDAY") // no armour
				ENDIF
			ENDIF
		ENDIF
		
		
		
		//	1x Horror Sweater (url:bugstar:5382797) 
		//	Firework Launcher
		//	10x Firework rockets
		//	Full Snacks
		//	Full Armor
		IF g_sMPTunables.btoggle_2018_new_years_eve_gift = TRUE
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2018_NEW_YEARS_EVE_GIFT_AWARDED	) 
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2018_NEW_YEARS_EVE_GIFT_AWARDED	, TRUE) 
				PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)
		
				SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
				SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
				SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
				SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 10, FALSE)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 10)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 10)
				
				
				APPLY_ARMOUR_GIFTING(GET_ACTIVE_CHARACTER_SLOT(), GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE))
				NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
				
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS) > INV_SNACK_1_MAX
					SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
					PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT YUM SNACKS TO INV_SNACK_1_MAX = ", INV_SNACK_1_MAX, " ")
				ENDIF
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS) > INV_SNACK_2_MAX
					SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
					PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT HEALTH SNACKS TO INV_SNACK_2_MAX = ", INV_SNACK_2_MAX, " ")
				ENDIF
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS) > INV_SNACK_3_MAX
					SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
					PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT EPIC SNACKS TO INV_SNACK_3_MAX = ", INV_SNACK_3_MAX, " ")
				ENDIF
				
				INT iItemHash
				INT iItemCategory = hash("SNACK")
				INT iShopType = hash("EVENT")
				INT iReason = hash("GET")
				SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_EPIC_SNACKS, TRUE) 
				SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_YUM_SNACKS, TRUE) 
				SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_HEALTHY_SNACKS, TRUE) 
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, INV_DRINK_1_MAX) 
				iItemHash = hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0")
				SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_DRINK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
				iItemHash = hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0")
				SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
				iItemHash = hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0")
				SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_2_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
				iItemHash = hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0")
				SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_3_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
				IF  GET_PLAYER_RANK(PLAYER_ID()) >= 5
					PRINT_TICKER("UNLOCK_18_NYEVE")
				ELSE
					PRINT_TICKER("UNLOCK_18bNYEVE") // no armour
				ENDIF
			
			ENDIF
		ENDIF
		
		
		// New Year’s Day Gift
		// Players should receive:
		// -	1x Horror Sweater (url:bugstar:5382797) 
		// -	Firework Launcher
		// -	10x Firework rockets
		// -	Full Snacks
		// -	Full Armor
		IF g_sMPTunables.btoggle_2018_new_years_day_gift = TRUE
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2018_NEW_YEARS_DAY_GIFT_AWARDED	) 
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_2018_NEW_YEARS_DAY_GIFT_AWARDED	, TRUE)  
				PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)
		
				SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
				SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
				SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
				SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 10, FALSE)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 10)
				INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 10)
				
				
				APPLY_ARMOUR_GIFTING(GET_ACTIVE_CHARACTER_SLOT(), GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE))
				NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
				
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS) > INV_SNACK_1_MAX
					SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
					PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT YUM SNACKS TO INV_SNACK_1_MAX = ", INV_SNACK_1_MAX, " ")
				ENDIF
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS) > INV_SNACK_2_MAX
					SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
					PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT HEALTH SNACKS TO INV_SNACK_2_MAX = ", INV_SNACK_2_MAX, " ")
				ENDIF
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS) > INV_SNACK_3_MAX
					SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
					PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT EPIC SNACKS TO INV_SNACK_3_MAX = ", INV_SNACK_3_MAX, " ")
				ENDIF
				
				INT iItemHash
				INT iItemCategory = hash("SNACK")
				INT iShopType = hash("EVENT")
				INT iReason = hash("GET")
				SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_EPIC_SNACKS, TRUE) 
				SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_YUM_SNACKS, TRUE) 
				SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_HEALTHY_SNACKS, TRUE) 
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, INV_DRINK_1_MAX) 
				iItemHash = hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0")
				SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_DRINK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
				iItemHash = hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0")
				SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
				iItemHash = hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0")
				SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_2_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
				iItemHash = hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0")
				SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_3_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
				
				IF  GET_PLAYER_RANK(PLAYER_ID()) >= 5
					PRINT_TICKER("UNLOCK_18_NYDAY")
				ELSE
					PRINT_TICKER("UNLOCK_18bNYDAY") // no armour
				ENDIF
			
				
				
			ENDIF
		ENDIF

//	IF g_sMPTunables.bnew_AW_vehicle_text = TRUE	
//		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DISPLAY_ARENA_VEH_MESSAGE	)  
//			PRINT_HELP("AWS_HUB_VEH")
//			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DISPLAY_ARENA_VEH_MESSAGE	, TRUE)  
//		ENDIF
//	ENDIF




	IF g_sMPTunables.bnew_BH_vehicle_text = TRUE	
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DISPLAY_BUSINESS_HUB_VEH_MESSAGE	)  
			PRINT_HELP("BUS_HUB_VEH")
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DISPLAY_BUSINESS_HUB_VEH_MESSAGE	, TRUE)  
		ENDIF
	ENDIF
	
	
	IF g_sMPTunables.bEVENT_MORPHSUIT_GREEN		 = TRUE  
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_MORPHSUIT_GREEN	)  
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_MORPHSUIT_GREEN	, TRUE)  
			//PRINT_TICKER("MORPHAWDGREE")	
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"MORPHAWDGREE", "UNLOCK_NAME_SHIRT3", "FeedhitTshirt04", "MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF     
	ENDIF
	
	
    IF g_sMPTunables.bEVENT_MORPHSUIT_ORANGE	 = TRUE  
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_MORPHSUIT_ORANGE )  
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_MORPHSUIT_ORANGE , TRUE) 
			//PRINT_TICKER("MORPHAWDORAN")     
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"MORPHAWDORAN", "UNLOCK_NAME_SHIRT3", "FeedhitTshirt04","MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF   
	ENDIF
	
    IF g_sMPTunables.bEVENT_MORPHSUIT_BLUE       = TRUE  
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_MORPHSUIT_BLUE   )  
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_MORPHSUIT_BLUE   , TRUE) 
			//PRINT_TICKER("MORPHAWDBLUE")          
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"MORPHAWDBLUE", "UNLOCK_NAME_SHIRT3", "FeedhitTshirt04","MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF    
	ENDIF
    IF g_sMPTunables.bEVENT_MORPHSUIT_PINK       = TRUE  
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_MORPHSUIT_PINK   )  
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_MORPHSUIT_PINK   , TRUE) 
			//PRINT_TICKER("MORPHAWDPINK")    
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"MORPHAWDPINK", "UNLOCK_NAME_SHIRT3", "FeedhitTshirt04","MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF   
	ENDIF
    IF g_sMPTunables.bEVENT_MORPHSUIT_YELLOW     = TRUE  
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_MORPHSUIT_YELLOW )  
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_MORPHSUIT_YELLOW , TRUE) 
			//PRINT_TICKER("MORPHAWDYELL")        
			SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"MORPHAWDYELL", "UNLOCK_NAME_SHIRT3", "FeedhitTshirt04","MPTshirtAwards3" ,DEFAULT,DEFAULT,DEFAULT,DEFAULT,DEFAULT,TRUE)
		ENDIF   
	ENDIF
    IF g_sMPTunables.bENABLE_ADVENT_ARMOR        = TRUE 
		IF  GET_PLAYER_RANK(PLAYER_ID()) >= 5
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_ADVENT_ARMOR     )  
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_ADVENT_ARMOR     , TRUE) 
				PRINT_TICKER("XMAS17ARMOR")      
				APPLY_ARMOUR_GIFTING(GET_ACTIVE_CHARACTER_SLOT(), GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE))
				NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
				
			ENDIF   
		ENDIF
	ENDIF
    IF g_sMPTunables.bENABLE_ADVENT_STICKIES     = TRUE  
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_ADVENT_STICKIES  )  
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_ADVENT_STICKIES  , TRUE) 
			PRINT_TICKER("XMAS17STICK")  
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_STICKYBOMB, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_STICKYBOMB, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_STICKYBOMB, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_STICKYBOMB, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, 25, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_FM_AMMO_BOUGHT, 25)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_FM_AMMO_CURRENT, 25)
		ENDIF    
	ENDIF
    IF g_sMPTunables.bENABLE_ADVENT_SNACKS       = TRUE 
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_ADVENT_SNACKS    )  
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_ADVENT_SNACKS    , TRUE) 
			PRINT_TICKER("XMAS17SNACKS")       

			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS) > INV_SNACK_1_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT YUM SNACKS TO INV_SNACK_1_MAX = ", INV_SNACK_1_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS) > INV_SNACK_2_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
				PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT HEALTH SNACKS TO INV_SNACK_2_MAX = ", INV_SNACK_2_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS) > INV_SNACK_3_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT EPIC SNACKS TO INV_SNACK_3_MAX = ", INV_SNACK_3_MAX, " ")
			ENDIF
			
			INT iItemHash
			INT iItemCategory = hash("SNACK")
			INT iShopType = hash("EVENT")
			INT iReason = hash("GET")
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_EPIC_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_YUM_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_HEALTHY_SNACKS, TRUE) 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, INV_DRINK_1_MAX) 
			iItemHash = hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_DRINK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_2_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_3_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			
		ENDIF   
	ENDIF

	
	
	IF g_sMPTunables.btoggle_2017_Christmas_day_gift
		//o	Free Access to the Hermes - remember to pick it up today from the  Southersanandreassupersutos.com website  // alwyn
		//o	One Free Shot on the Orbital Cannon - purchase a Facility War Room at any time in order to redeem 			// alex
		//o	Krampus Mask (exact model name?)
		//o	Carbine + 200 rounds
		//o	Marksman Rifle + 200 rounds
		//o	Knuckle Dusters
		//o	Firework Launcher
		//o	10x Firework rockets
		//o	25x Sticky Bombs
		//o	25x Grenades
		//o	5x Proximity Mines
		//o	5x Molotovs
		//o	Full Snacks
		//o	Full Armor

		IF GET_PACKED_STAT_BOOL( PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_CHRISTMAS_DAY_17) = FALSE
		
			SET_PACKED_STAT_BOOL(PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_CHRISTMAS_DAY_17, TRUE)
			
			IF  GET_PLAYER_RANK(PLAYER_ID()) >= 5
				PRINT_TICKER("UNLOCK_AWD_XMASDAY")
			ELSE
				PRINT_TICKER("UNLOCK_AWD_XMASDAB") // no armour
			ENDIF
			
			
			
			OBTAIN_COUPON(COUPON_CAR_XMAS2017) // Free Access to the Hermes - remember to pick it up today from the  Southersanandreassupersutos.com website

			
			PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)
	
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 10, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 10)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 10)
			
			IF NOT IS_MP_WEAPON_ADDON_EQUIPPED(WEAPONCOMPONENT_DLC_GUNRUN_MK2_UPGRADE, WEAPONTYPE_CARBINERIFLE)
				SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_CARBINERIFLE, TRUE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_CARBINERIFLE, 200, FALSE)
			ELSE
				ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_CARBINERIFLE, 200)
			ENDIF
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_CARBINERIFLE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_CARBINERIFLE, TRUE, FALSE)

			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CRBNRIFLE_FM_AMMO_BOUGHT, 200)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CRBNRIFLE_FM_AMMO_CURRENT, 200)
			SET_TRANSACTION_FOR_WEAPON(WEAPONTYPE_CARBINERIFLE)
			IF NOT IS_MP_WEAPON_ADDON_EQUIPPED(WEAPONCOMPONENT_DLC_GUNRUN_MK2_UPGRADE, WEAPONTYPE_CARBINERIFLE)
				SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_CARBINERIFLE, TRUE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_CARBINERIFLE, 200, FALSE)
			ELSE
				ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_CARBINERIFLE, 200)
			ENDIF
			
			IF NOT IS_MP_WEAPON_ADDON_EQUIPPED(WEAPONCOMPONENT_DLC_GUNRUN_MK2_UPGRADE, WEAPONTYPE_DLC_MARKSMANRIFLE)
				SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_MARKSMANRIFLE, TRUE)
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_MARKSMANRIFLE, 200, FALSE)
			ELSE
				ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_MARKSMANRIFLE, 200)
			ENDIF
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_MARKSMANRIFLE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_MARKSMANRIFLE, TRUE, FALSE)
		
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MKRIFLE_FM_AMMO_BOUGHT, 200)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MKRIFLE_FM_AMMO_CURRENT, 200)
			SET_TRANSACTION_FOR_WEAPON(WEAPONTYPE_DLC_MARKSMANRIFLE)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_MOLOTOV, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_MOLOTOV, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_MOLOTOV, TRUE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MOLOTOV, 5, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MOLOTOV_FM_AMMO_BOUGHT, 5)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MOLOTOV_FM_AMMO_CURRENT, 5)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_GRENADE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_GRENADE, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_GRENADE, TRUE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 25, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_FM_AMMO_BOUGHT, 25)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_FM_AMMO_CURRENT, 25)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_STICKYBOMB, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_STICKYBOMB, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_STICKYBOMB, TRUE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, 25, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_FM_AMMO_BOUGHT, 25)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_FM_AMMO_CURRENT, 25)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_PROXMINE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_PROXMINE, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_PROXMINE, TRUE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_PROXMINE, 5, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_BOUGHT , 5)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_CURRENT, 5)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_KNUCKLE, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_KNUCKLE, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_KNUCKLE, TRUE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_KNUCKLE, 1, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_KNUCKLE_FM_AMMO_BOUGHT , 1)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_KNUCKLE_FM_AMMO_CURRENT, 1)
			SET_TRANSACTION_FOR_WEAPON(WEAPONTYPE_DLC_KNUCKLE)
			
			
			APPLY_ARMOUR_GIFTING(GET_ACTIVE_CHARACTER_SLOT(), GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE))
			NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
			
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS) > INV_SNACK_1_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT YUM SNACKS TO INV_SNACK_1_MAX = ", INV_SNACK_1_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS) > INV_SNACK_2_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
				PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT HEALTH SNACKS TO INV_SNACK_2_MAX = ", INV_SNACK_2_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS) > INV_SNACK_3_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT EPIC SNACKS TO INV_SNACK_3_MAX = ", INV_SNACK_3_MAX, " ")
			ENDIF
			
			INT iItemHash
			INT iItemCategory = hash("SNACK")
			INT iShopType = hash("EVENT")
			INT iReason = hash("GET")
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_EPIC_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_YUM_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_HEALTHY_SNACKS, TRUE) 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, INV_DRINK_1_MAX) 
			iItemHash = hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_DRINK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_2_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_3_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_AWARD_XMAS2017_CANNON_GIFT, TRUE)
			
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_KRAMPUS_4, TRUE)//Krampus mask
			REQUEST_SAVE(SSR_REASON_TUNEABLE_CONTENT, STAT_SAVETYPE_IMMEDIATE_FLUSH)
			PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS REQUEST_SAVE(STAT_SAVETYPE_IMMEDIATE_FLUSH) ")
			
			
			
		ENDIF
	ENDIF
	
	
	IF g_sMPTunables.bGIFT2017_ODIOUS_RED 	
		IF GET_PACKED_STAT_BOOL( PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_CHRISTMAS_EVE_17) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_CHRISTMAS_EVE_17, TRUE)
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 10, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 10)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 10)
			
			// Give random t shirts
			INT iShirt
			IF GET_XMAS_SHIRT_NOT_OWNED_BY_PLAYER(iShirt)
				IF iShirt != -1
					PRINTLN("[KW] UNLOCK_SPECIAL_EVENT_ITEMS btoggle_2016_christmas_eve_gift - GIVE ITEMS. T shirt = ", iShirt)
					SET_PACKED_STAT_BOOL(GET_XMAS_GIFT_SHIRT_PACKED_STAT(iShirt), TRUE)
					IF iShirt = 12
						SET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_STATUE_OF_HAPPINESS_TEE, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)
			IF iShirt = -1
				PRINT_TICKER_WITH_STRING("UNLOCK_AWD_XMASNO", "UNLOCK_AWD_KRAMP3")  // no shirt
			ELSE
				PRINT_TICKER_WITH_STRING(GET_XMAS_GIFT_SHIRT_TICKER(iShirt), "UNLOCK_AWD_KRAMP3")
			ENDIF
			
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_KRAMPUS_3, TRUE)//Krampus mask red
			
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bGIFT2017_HIDEOUS_WHITE 	
		IF GET_PACKED_STAT_BOOL( PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_NEW_YEARS_EVE_17) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_NEW_YEARS_EVE_17, TRUE)
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 10, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 10)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 10)
			
			// Give random t shirts
			INT iShirt
			IF GET_XMAS_SHIRT_NOT_OWNED_BY_PLAYER(iShirt)
				IF iShirt != -1
					PRINTLN("[KW] UNLOCK_SPECIAL_EVENT_ITEMS btoggle_2016_new_years_eve_gift - GIVE ITEMS. T shirt = ", iShirt)
					SET_PACKED_STAT_BOOL(GET_XMAS_GIFT_SHIRT_PACKED_STAT(iShirt), TRUE)
					IF iShirt = 12
						SET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_STATUE_OF_HAPPINESS_TEE, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)
			IF iShirt = -1
				PRINT_TICKER_WITH_STRING("UNLOCK_AWD_XMASNO", "UNLOCK_AWD_KRAMP1")  // no shirt
			ELSE
				PRINT_TICKER_WITH_STRING(GET_XMAS_GIFT_SHIRT_TICKER(iShirt), "UNLOCK_AWD_KRAMP1")
			ENDIF
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_KRAMPUS_1, TRUE)//Krampus mask white
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bGIFT2017_FEARSOME_BLUE  	
		IF GET_PACKED_STAT_BOOL( PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_NEW_YEARS_DAY_17) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_NEW_YEARS_DAY_17, TRUE)
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 10, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 10)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 10)
			
			// Give random t shirts
			INT iShirt
			IF GET_XMAS_SHIRT_NOT_OWNED_BY_PLAYER(iShirt)
				IF iShirt != -1
					PRINTLN("[KW] UNLOCK_SPECIAL_EVENT_ITEMS btoggle_2016_new_years_day_gift - GIVE ITEMS. T shirt = ", iShirt)
					SET_PACKED_STAT_BOOL(GET_XMAS_GIFT_SHIRT_PACKED_STAT(iShirt), TRUE)
					IF iShirt = 12
						SET_PACKED_STAT_BOOL(PACKED_MP_STATS_UNLOCK_STATUE_OF_HAPPINESS_TEE, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)
			IF iShirt = -1
				PRINT_TICKER_WITH_STRING("UNLOCK_AWD_XMASNO", "UNLOCK_AWD_KRAMP2")  // no shirt
			ELSE
				PRINT_TICKER_WITH_STRING(GET_XMAS_GIFT_SHIRT_TICKER(iShirt), "UNLOCK_AWD_KRAMP2")
			ENDIF
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_KRAMPUS_2, TRUE)//Krampus mask blue
		ENDIF
	ENDIF
	
	IF g_sMPTunables.btoggle_gift_to_player_when_logging_on
		IF GET_PACKED_STAT_BOOL( PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_1) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_1, TRUE)
			PRINT_TICKER("UNLOCK_AWD_LOG1")
			/*
			-	Exclusive Stocking Mask
			-             Body Armour
			-	3x Proximity Mines
			-	3x Molotov Cocktails
			-	3x Homing Missile missiles
			-	Carbine Rifle + 200 rounds

			*/
			NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MOLOTOV, 3, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_PROXMINE, 3, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 5, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_CARBINERIFLE, 200, FALSE)
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_MOLOTOV, TRUE)
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_PROXMINE, TRUE)
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_CARBINERIFLE, TRUE)

			 
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_MOLOTOV, TRUE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_PROXMINE, TRUE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_CARBINERIFLE, TRUE)
			
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MOLOTOV_FM_AMMO_BOUGHT, 5)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 5)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CRBNRIFLE_FM_AMMO_BOUGHT, 200)
			

			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCK_STOCKING_MASK, TRUE)
			
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_MOLOTOV, TRUE, FALSE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_PROXMINE, TRUE, FALSE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_CARBINERIFLE, TRUE, FALSE)
			
			
			
			

			
			
			IF bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_M_BERD_1_0"), PED_COMPONENT_ACQUIRED_SLOT)
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_F_BERD_1_0"), PED_COMPONENT_ACQUIRED_SLOT)
			ENDIF
			
			PRINTLN("[KW] UNLOCK_SPECIAL_EVENT_ITEMS  - btoggle_gift_to_player_when_logging_on - GIVE ITEMS")
		ENDIF
	ENDIF
	
	IF g_sMPTunables.btoggle_christmas_eve_gift 		
		IF GET_PACKED_STAT_BOOL( PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_CHRISTMAS_EVE) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_CHRISTMAS_EVE, TRUE)
			PRINT_TICKER("UNLOCK_AWD_LOG2")
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 5, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 5)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 5)
			PRINTLN("[KW] UNLOCK_SPECIAL_EVENT_ITEMS  - btoggle_christmas_eve_gift - GIVE ITEMS")
		ENDIF
		
	ENDIF
	
	
	IF g_sMPTunables.btoggle_new_years_eve_gift 		
		IF GET_PACKED_STAT_BOOL( PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_NEW_YEARS_EVE) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_NEW_YEARS_EVE, TRUE)
			PRINT_TICKER("UNLOCK_AWD_LOG2")
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 5, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 5)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 5)
			PRINTLN("[KW] UNLOCK_SPECIAL_EVENT_ITEMS  - btoggle_new_years_eve_gift - GIVE ITEMS")
		ENDIF
		
	ENDIF
	
	IF g_sMPTunables.btoggle_new_years_day_gift 		
		IF GET_PACKED_STAT_BOOL( PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_NEW_YEARS_DAY) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_NEW_YEARS_DAY, TRUE)
			PRINT_TICKER("UNLOCK_AWD_LOG2")
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 5, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 5)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 5)
			PRINTLN("[KW] UNLOCK_SPECIAL_EVENT_ITEMS  - btoggle_new_years_day_gift - GIVE ITEMS")
		ENDIF
		
	ENDIF
	
	IF g_sMPTunables.btoggle_2015_christmas_eve_gift 		
		IF GET_PACKED_STAT_BOOL( PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_CHRISTMAS_EVE_15) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_CHRISTMAS_EVE_15, TRUE)
			PRINT_TICKER("UNLOCK_AWD_LOG4")
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 5, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 5)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 5)
			PRINTLN("[KW] UNLOCK_SPECIAL_EVENT_ITEMS  - btoggle_2015_christmas_eve_gift - GIVE ITEMS")
			PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)
		ENDIF
	ENDIF
	
	IF g_sMPTunables.btoggle_2015_new_years_eve_gift 		
		IF GET_PACKED_STAT_BOOL( PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_NEW_YEARS_EVE_15) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_NEW_YEARS_EVE_15, TRUE)
			PRINT_TICKER("UNLOCK_AWD_LOG4")
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 5, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 5)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 5)
			PRINTLN("[KW] UNLOCK_SPECIAL_EVENT_ITEMS  - btoggle_2015_new_years_eve_gift - GIVE ITEMS")
			PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)
		ENDIF
	ENDIF
	
	IF g_sMPTunables.btoggle_2015_new_years_day_gift 		
		IF GET_PACKED_STAT_BOOL( PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_NEW_YEARS_DAY_15) = FALSE
			SET_PACKED_STAT_BOOL(PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_NEW_YEARS_DAY_15, TRUE)
			PRINT_TICKER("UNLOCK_AWD_LOG4")
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_FIREWORK, TRUE, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 5, FALSE)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 5)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 5)
			PRINTLN("[KW] UNLOCK_SPECIAL_EVENT_ITEMS  - btoggle_2015_new_years_day_gift - GIVE ITEMS")
			PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)
		ENDIF
	ENDIF
	

	
	
	IF g_sMPTunables.btoggle_2015_Christmas_day_gift
		IF GET_PACKED_STAT_BOOL( PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_CHRISTMAS_DAY_15) = FALSE
			PRINTLN("[KW] UNLOCK_SPECIAL_EVENT_ITEMS  - btoggle_2015_Christmas_day_gift - GIVE ITEMS")
			SET_PACKED_STAT_BOOL(PACKED_MP_RECIEVED_GIFT_WHEN_LOGGING_ON_CHRISTMAS_DAY_15, TRUE)
			PLAY_SOUND_FRONTEND(-1, "FestiveGift", "Feed_Message_Sounds", FALSE)
			
			/*
			 - 2015 Abominable Snowman mask
			 - Firework Launcher
			 - 5x Firework rockets
			 - 3x Proximity Mines
			 - 3x Molotovs
			 - Special Carbine + 200 Rounds
			 - Full Snacks
			 - 10x Sticky Bombs
			 - 15x Grenades 
			 - 10x Rockets for Rocket Launcher
			 - Baseball Bat
			 - Full Armour added to inventory

			*/
			APPLY_ARMOUR_GIFTING(GET_ACTIVE_CHARACTER_SLOT(), GET_RANK_FROM_XP_VALUE(GET_PLAYER_XP(PLAYER_ID()), FALSE))
			NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_FIREWORK, 5, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_DLC_PROXMINE, 3, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MOLOTOV, 3, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_CARBINERIFLE, 200, FALSE)

			
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, 10, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 15, FALSE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_BAT, 1, FALSE)
		
			IF IS_MP_WEAPON_PURCHASED(WEAPONTYPE_RPG)
			OR IS_MP_WEAPON_EQUIPPED(WEAPONTYPE_RPG) 
			OR GET_MP_BOOL_CHARACTER_STAT(GET_WEAPON_AS_GIFT_STAT(WEAPONTYPE_RPG) )
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_RPG, 10, FALSE)
			ENDIF
			
			
			
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_MOLOTOV, TRUE)
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_DLC_PROXMINE, TRUE)
			
			IF NOT NET_GAMESERVER_BASKET_IS_ACTIVE()
			AND NOT IS_PLAYER_IN_ANY_SHOP()
			      IF NOT NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_WEAPON, HASH("WP_WT_RIFLE_CBN_t0_v1"), NET_SHOP_ACTION_SPEND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, DEFAULT, CTPF_AUTO_RETRY|CTPF_FAIL_ALERT|CTPF_AUTO_PROCESS_REPLY)
			      OR NOT NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
			            DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
			      ENDIF
			ENDIF

			

			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_CARBINERIFLE, TRUE)
			SET_MP_WEAPON_PURCHASED(WEAPONTYPE_BAT, TRUE)
			 
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_MOLOTOV, TRUE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_PROXMINE, TRUE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_DLC_FIREWORK, TRUE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_CARBINERIFLE, TRUE)
			SET_MP_WEAPON_EQUIPPED(WEAPONTYPE_BAT, TRUE)
			 
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MOLOTOV_FM_AMMO_BOUGHT, 5)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CRBNRIFLE_FM_AMMO_CURRENT, 200)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CRBNRIFLE_FM_AMMO_BOUGHT, 200)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_BOUGHT, 3)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_PRXMINE_FM_AMMO_CURRENT, 3)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_BAT_FM_AMMO_BOUGHT, 1)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_RPG_FM_AMMO_BOUGHT, 10)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_BOUGHT, 5)
			INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_FIREWRK_FM_AMMO_CURRENT, 5)
			
			
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_MOLOTOV, TRUE, FALSE)
			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_PROXMINE, TRUE, FALSE)

			SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_CARBINERIFLE, TRUE, FALSE)
			
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CRBNRIFLE_IN_POSSESSION , TRUE)
			
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS) > INV_SNACK_1_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT YUM SNACKS TO INV_SNACK_1_MAX = ", INV_SNACK_1_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS) > INV_SNACK_2_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
				PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT HEALTH SNACKS TO INV_SNACK_2_MAX = ", INV_SNACK_2_MAX, " ")
			ENDIF
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS) > INV_SNACK_3_MAX
				SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
				PRINTLN(" UNLOCK_SPECIAL_TUNABLE_ITEMS: CORRECT EPIC SNACKS TO INV_SNACK_3_MAX = ", INV_SNACK_3_MAX, " ")
			ENDIF
			
			INT iItemHash
			INT iItemCategory = hash("SNACK")
			INT iShopType = hash("EVENT")
			INT iReason = hash("GET")
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_EPIC_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_YUM_SNACKS, TRUE) 
			SET_PACKED_STAT_BOOL( PACKED_MP_BOUGHT_HEALTHY_SNACKS, TRUE) 
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, INV_DRINK_1_MAX) 
			iItemHash = hash("MP_STAT_NUMBER_OF_ORANGE_BOUGHT_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_DRINK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_YUM_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_1_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_HEALTH_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_2_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
			iItemHash = hash("MP_STAT_NO_BOUGHT_EPIC_SNACKS_v0")
			SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA(1, iItemHash, iItemCategory, INV_SNACK_3_MAX-GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS), GET_LOCATION_HASH_FOR_TELEMETRY(), iReason, FALSE, iShopType)
			
			

			
			// Random pyjamas
			INT randomint = GET_RANDOM_INT_IN_RANGE(0,3)	
			PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS g_sMPTunables.btoggle_2015_Christmas_day_gift randomint = ", randomint)
			IF randomint = 0 // red 
				IF  bIsPlayerMale
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS3_M_JBIB_1_0"), PED_COMPONENT_ACQUIRED_SLOT)
				ELSE
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS3_F_JBIB_1_0"), PED_COMPONENT_ACQUIRED_SLOT)
				ENDIF
				SET_PACKED_STAT_BOOL( PACKED_MP_STAT_PYJAMA_RED_CHECK_UNLOCKED, TRUE)
				PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS g_sMPTunables.btoggle_2015_Christmas_day_gift randomint UNLOCKED RED PYJAMAS")
			ELIF randomint = 1
				IF  bIsPlayerMale
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS3_M_JBIB_1_1"), PED_COMPONENT_ACQUIRED_SLOT)
				ELSE
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS3_F_JBIB_1_1"), PED_COMPONENT_ACQUIRED_SLOT)
				ENDIF
				SET_PACKED_STAT_BOOL( PACKED_MP_STAT_PYJAMA_NAVY_CHECK_UNLOCKED, TRUE)
				PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS g_sMPTunables.btoggle_2015_Christmas_day_gift randomint UNLOCKED NAVY PYJAMAS")
			ELIF randomint = 2
				IF  bIsPlayerMale
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS3_M_JBIB_1_2"), PED_COMPONENT_ACQUIRED_SLOT)
				ELSE
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS3_F_JBIB_1_2"), PED_COMPONENT_ACQUIRED_SLOT)
				ENDIF
				SET_PACKED_STAT_BOOL( PACKED_MP_STAT_PYJAMA_BLACK_CHECK_UNLOCKED, TRUE)
				PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS g_sMPTunables.btoggle_2015_Christmas_day_gift randomint UNLOCKED BLACK PYJAMAS")
			ENDIF
			

			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCK_ABOMINABLE_SNOWMAN_MASK, TRUE)
			IF bIsPlayerMale
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS3_M_BERD_10_0"), PED_COMPONENT_ACQUIRED_SLOT)
			//	SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS3_M_BERD_10_1"), PED_COMPONENT_ACQUIRED_SLOT)
			//	SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS3_M_BERD_10_2"), PED_COMPONENT_ACQUIRED_SLOT)
			//	SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS3_M_BERD_10_3"), PED_COMPONENT_ACQUIRED_SLOT)
				PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS g_sMPTunables.btoggle_2015_Christmas_day_gift randomint UNLOCKED MALE PACKED_MP_STAT_UNLOCK_ABOMINABLE_SNOWMAN_MASK")
			ELSE
				SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS3_F_BERD_10_0"), PED_COMPONENT_ACQUIRED_SLOT)
				//SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS3_F_BERD_10_1"), PED_COMPONENT_ACQUIRED_SLOT)
				//SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS3_F_BERD_10_2"), PED_COMPONENT_ACQUIRED_SLOT)
				//SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS3_F_BERD_10_3"), PED_COMPONENT_ACQUIRED_SLOT)
				PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS g_sMPTunables.btoggle_2015_Christmas_day_gift randomint UNLOCKED FEMALE PACKED_MP_STAT_UNLOCK_ABOMINABLE_SNOWMAN_MASK")
			ENDIF
			
			INT numberofreports = GET_TOTAL_NUMBER_OF_REPORTS()
			INT badreports = GET_TOTAL_NUMBER_OF_NEGATIVE_REPORTS()
			INT GOOD_REPORTS = numberofreports - badreports
			INT PERCENTAGE_OF_BAD_REPORTS = (badreports /numberofreports) * 100
			INT PERCENTAGE_OF_GOOD_REPORTS = (GOOD_REPORTS /numberofreports) * 100
		
			
			PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS numberofreports = ", numberofreports)
			PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS badreports = ", badreports)
			PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS PERCENTAGE_OF_BAD_REPORTS = ", PERCENTAGE_OF_BAD_REPORTS)
			PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS PERCENTAGE_OF_GOOD_REPORTS = ", PERCENTAGE_OF_GOOD_REPORTS)
			PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS GOOD_REPORTS = ", GOOD_REPORTS)
	
			IF PERCENTAGE_OF_BAD_REPORTS > 5
			
			ENDIF
			
		
			IF NETWORK_PLAYER_IS_CHEATER()
			OR NETWORK_PLAYER_IS_BADSPORT() 
				PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS NAUGHTY  ")

				PRINT_TICKER("UNLOCK_AWD_LOG3")
				//naughty
				SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCK_NAUGHTY_HAT, TRUE)
				
				IF bIsPlayerMale
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_M_PHEAD_4_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_M_PHEAD_5_0"), PED_COMPONENT_ACQUIRED_SLOT)
				ELSE
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_F_PHEAD_4_0"), PED_COMPONENT_ACQUIRED_SLOT)
					SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_F_PHEAD_5_0"), PED_COMPONENT_ACQUIRED_SLOT)
				ENDIF
			ELIF PERCENTAGE_OF_GOOD_REPORTS > 5
			OR (GOOD_REPORTS > 40 AND  PERCENTAGE_OF_GOOD_REPORTS > 1 )
					PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS GOOD")
					PRINT_TICKER("UNLOCK_AWD_LOG3b")
					//NICe
					SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCK_NICE_HAT, TRUE)
					
					IF bIsPlayerMale
						SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_M_PHEAD_4_3"), PED_COMPONENT_ACQUIRED_SLOT)
						SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_M_PHEAD_5_3"), PED_COMPONENT_ACQUIRED_SLOT)
					ELSE
						SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_F_PHEAD_4_3"), PED_COMPONENT_ACQUIRED_SLOT)
						SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_F_PHEAD_5_3"), PED_COMPONENT_ACQUIRED_SLOT)
					
					ENDIF
				
				
			ELSE
				IF GET_RANDOM_INT_IN_RANGE(0, 100) > 65
					PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS RANDOM Naughty")
					PRINT_TICKER("UNLOCK_AWD_LOG3")
					//naughty
					SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCK_NAUGHTY_HAT, TRUE)
					
					IF bIsPlayerMale
						SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_M_PHEAD_4_0"), PED_COMPONENT_ACQUIRED_SLOT)
						SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_M_PHEAD_5_0"), PED_COMPONENT_ACQUIRED_SLOT)
					ELSE
						SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_F_PHEAD_4_0"), PED_COMPONENT_ACQUIRED_SLOT)
						SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_F_PHEAD_5_0"), PED_COMPONENT_ACQUIRED_SLOT)
					ENDIF
				ELSE
					PRINTLN("UNLOCK_SPECIAL_TUNABLE_ITEMS RANDOM GOOD")
					PRINT_TICKER("UNLOCK_AWD_LOG3b")
					//NICe
					SET_PACKED_STAT_BOOL(PACKED_MP_STAT_UNLOCK_NICE_HAT, TRUE)
					
					IF bIsPlayerMale
						SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_M_PHEAD_4_3"), PED_COMPONENT_ACQUIRED_SLOT)
						SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_M_PHEAD_5_3"), PED_COMPONENT_ACQUIRED_SLOT)
					ELSE
						SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_F_PHEAD_4_3"), PED_COMPONENT_ACQUIRED_SLOT)
						SET_BIT_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_XMAS2_F_PHEAD_5_3"), PED_COMPONENT_ACQUIRED_SLOT)
					
					ENDIF
					
					
				ENDIF
			ENDIF
			
			
			
			
		ENDIF
	ENDIF
	
	
	/*
	IF  g_sMPTunables.bTURN_ON_HALLOWEEN_JOBS = TRUE
		IF FM_LOW_FLOW_HAS_BEEN_COMPLETED()
		OR GET_FM_STRAND_PROGRESS(ciFLOW_STRAND_LOW_RIDER) > 1
			IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HALLOWEEN_LOG_HELP)
				PRINTLN("[KW] UNLOCK_SPECIAL_EVENT_ITEMS  - bTURN_ON_TH = TRUE - DISPLAY HELP")
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HALLOWEEN_LOG_HELP, TRUE)
				PRINT_HELP("UNLOCK_AWD_HALL")
			ENDIF
		ENDIF
		
	ELSE
		IF  GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HALLOWEEN_LOG_HELP)
			PRINTLN("[KW] UNLOCK_SPECIAL_EVENT_ITEMS  - bTURN_ON_TH = FALSE - DISPLAY HELP")
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HALLOWEEN_LOG_HELP, FALSE)
		ENDIF
		
	ENDIF
	
	
	*/
	
	IF g_sMPTunables.bENABLE_ADVENT_AMMO         = TRUE  
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_ADVENT_AMMO      ) 
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MESSSAGEING_ADVENT_AMMO      , TRUE) 
			PRINT_TICKER("XMAS17AMMO")    

			PROCESS_GIFTING_FULL_AMMO( TRUE)
			

			//SET_CORONA_FULL_AMMO_AMOUNT(coronaMenuData)
		ENDIF  
	ENDIF
	
	IF g_sMPTunables.bXMASDAYGIFT2018_CAR
		IF GET_PACKED_STAT_BOOL( PACKED_MP_RECIEVED_COUPON_CAR_WHEN_LOGGING_ON_CHRISTMAS_DAY_18) = FALSE
		
			SET_PACKED_STAT_BOOL(PACKED_MP_RECIEVED_COUPON_CAR_WHEN_LOGGING_ON_CHRISTMAS_DAY_18, TRUE)
			
			OBTAIN_COUPON(COUPON_CAR_XMAS2018) // url:bugstar:5441195 - Free Car - Christmas Gift 2018
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bXMASDAYGIFT2018_HELI
		IF GET_PACKED_STAT_BOOL( PACKED_MP_RECIEVED_COUPON_HELI_WHEN_LOGGING_ON_CHRISTMAS_DAY_18) = FALSE
		
			SET_PACKED_STAT_BOOL(PACKED_MP_RECIEVED_COUPON_HELI_WHEN_LOGGING_ON_CHRISTMAS_DAY_18, TRUE)
			
			OBTAIN_COUPON(COUPON_HELI_XMAS2018) // url:bugstar:5441238 - Free Buzzard - Christmas Gift 2018
			PRINT_TICKER("UNLOCK_BUZZHOL")
		ENDIF
	ENDIF
	
	IF g_sMPTunables.bXMASDAYGIFT2018_CAR2
		IF GET_PACKED_STAT_BOOL( PACKED_MP_RECIEVED_COUPON_CAR2_WHEN_LOGGING_ON_CHRISTMAS_DAY_18) = FALSE
		
			SET_PACKED_STAT_BOOL(PACKED_MP_RECIEVED_COUPON_CAR2_WHEN_LOGGING_ON_CHRISTMAS_DAY_18, TRUE)
			
			OBTAIN_COUPON(COUPON_CAR2_XMAS2018) // url:bugstar:5441243 - Free Insurgent - Christmas Gift 2018
			IF g_sMPTunables.btoggle_2018_new_years_eve_gift = FALSE
				PRINT_TICKER("UNLOCK_INSUGHOL")
			ENDIF
		ENDIF
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_STAT_AWARD_GEN9_MIGRATION_CAR)
		IF IS_GEN_8_PLAYER()
			OBTAIN_COUPON(COUPON_CAR_GEN9_MIGRATION) // url:bugstar:url:bugstar:7192940 - Free Toyota GT86 for migrating SP pre-fixer
			SET_PACKED_STAT_BOOL(PACKED_MP_STAT_AWARD_GEN9_MIGRATION_CAR, TRUE)
			PRINTLN("OBTAIN_COUPON(COUPON_CAR_GEN9_MIGRATION)")
		ENDIF	
	ENDIF
	#ENDIF

ENDPROC

FUNC STRING GET_DLC_SHIRT_DESC(DLC_AWARD_SHIRT_ENUM dlc_shirt)
	SWITCH dlc_shirt	
		CASE DLC_SHIRT_RETRO_BITCHN
			//SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,GET_TATTOO_DESC(aPlayerTATTOO, TATTOO_MP_FM), GET_TATTOO_DESCRIPTION(aPlayerTATTOO, TATTOO_MP_FM), GET_TATTOO_TEXTURE_DESC(aPlayerTATTOO, TATTOO_MP_FM), GET_TATTOO_TEXTURE_DICTIONARY(aPlayerTATTOO, TATTOO_MP_FM) )
			RETURN"UNLOCK_DESC_SHIRT1"
			//You can now purchase the retro 'Death Defying' T-shirt from any clothes store.
		BREAK
	
	
		CASE DLC_SHIRT_RETRO_VINYL
			//SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,GET_TATTOO_DESC(aPlayerTATTOO, TATTOO_MP_FM), GET_TATTOO_DESCRIPTION(aPlayerTATTOO, TATTOO_MP_FM), GET_TATTOO_TEXTURE_DESC(aPlayerTATTOO, TATTOO_MP_FM), GET_TATTOO_TEXTURE_DICTIONARY(aPlayerTATTOO, TATTOO_MP_FM) )
			RETURN"UNLOCK_DESC_SHIRT2"
			//You can now purchase the retro 'Death Defying' T-shirt from any clothes store.
		BREAK
		
		
		CASE DLC_SHIRT_RETRO_HOMIES
			//SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,GET_TATTOO_DESC(aPlayerTATTOO, TATTOO_MP_FM), GET_TATTOO_DESCRIPTION(aPlayerTATTOO, TATTOO_MP_FM), GET_TATTOO_TEXTURE_DESC(aPlayerTATTOO, TATTOO_MP_FM), GET_TATTOO_TEXTURE_DICTIONARY(aPlayerTATTOO, TATTOO_MP_FM) )
			RETURN"UNLOCK_DESC_SHIRT3"
			//You can now purchase the retro 'Death Defying' T-shirt from any clothes store.
		BREAK
		
		
		
		CASE DLC_SHIRT_AWARD_DEATH_DEFYING
			//SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,GET_TATTOO_DESC(aPlayerTATTOO, TATTOO_MP_FM), GET_TATTOO_DESCRIPTION(aPlayerTATTOO, TATTOO_MP_FM), GET_TATTOO_TEXTURE_DESC(aPlayerTATTOO, TATTOO_MP_FM), GET_TATTOO_TEXTURE_DICTIONARY(aPlayerTATTOO, TATTOO_MP_FM) )
			RETURN"UNLOCK_DESC_SHIRT4"
			//You can now purchase the retro 'Death Defying' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_FOR_HIRE
			RETURN"UNLOCK_DESC_SHIRT5"
			//You can now purchase the retro 'For Hire' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_SHOT_CALLER
			RETURN"UNLOCK_DESC_SHIRT6"
			//You can now purchase the retro 'Shot caller' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_LIVE_A_LITTLE
			RETURN"UNLOCK_DESC_SHIRT7" 
			//You can now purchase the retro 'Live a Little' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_ASSHOLE
			RETURN"UNLOCK_DESC_SHIRT8"
			//You can now purchase the retro 'Asshole' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_CANT_TOUCH_THIS
			RETURN"UNLOCK_DESC_SHIRT8"
			//You can now purchase the retro 'Can't Touch This' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_DECORATED
			RETURN"UNLOCK_DESC_SHIRT9"
			//You can now purchase the retro 'Decorated' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_PSYCHO_KILLER
			RETURN"UNLOCK_DESC_SHIRT9"
			//You can now purchase the retro 'Psycho Killer' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_ONE_MAN_ARMY
			RETURN"UNLOCK_DESC_SHIRT10" 
			//You can now purchase the retro 'One Man Army' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_SHOWROOM
			RETURN"UNLOCK_DESC_SHIRT11"
			//You can now purchase the retro 'Showroom' T-shirt from any clothes store.

		BREAK
	ENDSWITCH
	RETURN"UNLOCK_DESC_SHIRT11"
ENDFUNC

FUNC STRING GET_DLC_SHIRT_TEXTURE_NAME(DLC_AWARD_SHIRT_ENUM dlc_shirt)
	SWITCH dlc_shirt	
		CASE DLC_SHIRT_RETRO_BITCHN
			//SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,GET_TATTOO_NAME(aPlayerTATTOO, TATTOO_MP_FM), GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
			RETURN"FeedhitTshirt01"
			//You can now purchase the retro 'Death Defying' T-shirt from any clothes store.
		BREAK
	
	
		CASE DLC_SHIRT_RETRO_VINYL
			//SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,GET_TATTOO_NAME(aPlayerTATTOO, TATTOO_MP_FM), GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
			RETURN"FeedhitTshirt03"
			//You can now purchase the retro 'Death Defying' T-shirt from any clothes store.
		BREAK
		
		
		CASE DLC_SHIRT_RETRO_HOMIES
			//SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,GET_TATTOO_NAME(aPlayerTATTOO, TATTOO_MP_FM), GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
			RETURN"FeedhitTshirt04"
			//You can now purchase the retro 'Death Defying' T-shirt from any clothes store.
		BREAK
		
		
	ENDSWITCH
	RETURN"FeedhitTshirt04"
ENDFUNC

FUNC STRING GET_DLC_SHIRT_NAME(DLC_AWARD_SHIRT_ENUM dlc_shirt)
	SWITCH dlc_shirt	
		CASE DLC_SHIRT_RETRO_BITCHN
			//SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,GET_TATTOO_NAME(aPlayerTATTOO, TATTOO_MP_FM), GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
			RETURN"UNLOCK_NAME_SHIRT1" 
			//You can now purchase the retro 'Death Defying' T-shirt from any clothes store.
		BREAK
	
	
		CASE DLC_SHIRT_RETRO_VINYL
			//SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,GET_TATTOO_NAME(aPlayerTATTOO, TATTOO_MP_FM), GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
			RETURN"UNLOCK_NAME_SHIRT2"
			//You can now purchase the retro 'Death Defying' T-shirt from any clothes store.
		BREAK
		
		
		CASE DLC_SHIRT_RETRO_HOMIES
			//SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,GET_TATTOO_NAME(aPlayerTATTOO, TATTOO_MP_FM), GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
			RETURN"UNLOCK_NAME_SHIRT3"
			//You can now purchase the retro 'Death Defying' T-shirt from any clothes store.
		BREAK
		
		
		
		CASE DLC_SHIRT_AWARD_DEATH_DEFYING
			//SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,GET_TATTOO_NAME(aPlayerTATTOO, TATTOO_MP_FM), GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
			RETURN"UNLOCK_NAME_SHIRT4"
			//You can now purchase the retro 'Death Defying' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_FOR_HIRE
			RETURN"UNLOCK_NAME_SHIRT5"
			//You can now purchase the retro 'For Hire' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_SHOT_CALLER
			RETURN"UNLOCK_NAME_SHIRT6"
			//You can now purchase the retro 'Shot caller' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_LIVE_A_LITTLE
			RETURN"UNLOCK_NAME_SHIRT7"
			//You can now purchase the retro 'Live a Little' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_ASSHOLE
			RETURN"UNLOCK_NAME_SHIRT8"
			//You can now purchase the retro 'Asshole' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_CANT_TOUCH_THIS
			RETURN "UNLOCK_NAME_SHIRT9"
			//You can now purchase the retro 'Can't Touch This' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_DECORATED
			RETURN "UNLOCK_NAME_SHIRT10"
			//You can now purchase the retro 'Decorated' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_PSYCHO_KILLER
			RETURN "UNLOCK_NAME_SHIRT11"
			//You can now purchase the retro 'Psycho Killer' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_ONE_MAN_ARMY
			RETURN"UNLOCK_NAME_SHIRT12"
			//You can now purchase the retro 'One Man Army' T-shirt from any clothes store.
		BREAK
		
		CASE DLC_SHIRT_AWARD_SHOWROOM
			RETURN"UNLOCK_NAME_SHIRT13"
			//You can now purchase the retro 'Showroom' T-shirt from any clothes store.

		BREAK
		
		CASE DLC_SHIRT_HEIST_ELITE_1
			RETURN"UNLOCK_NAME_ELITE_1"
			//You can now purchase the retro 'Showroom' T-shirt from any clothes store.

		BREAK
		CASE DLC_SHIRT_HEIST_ELITE
			RETURN"UNLOCK_NAME_ELITE"
			//You can now purchase the retro 'Showroom' T-shirt from any clothes store.

		BREAK
		
		 
		CASE DLC_SHIRT_RSTAR_DEV_WHITE	RETURN"UNLOCK_SHIRTROCKST" BREAK
        CASE DLC_SHIRT_RSTAR_DEV_BLACK	RETURN"UNLOCK_SHIRTROCKST" BREAK
        CASE DLC_SHIRT_RSTAR_DEV_GRAY	RETURN"UNLOCK_SHIRTROCKST" BREAK
		
		
		
		
		
		
		
	ENDSWITCH
		RETURN"UNLOCK_NAME_SHIRT11"
ENDFUNC

//Added by Kevin> This unlocks a DLC TSHIRT from taking in a new dlc t shirt
PROC UNLOCK_A_DLC_AWARD_TSHIRT(DLC_AWARD_SHIRT_ENUM tshirt)
	
	BOOL bIsPlayerMale = (GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0)
	BOOL bRun_ticker = FALSE
	
	
	IF bIsPlayerMale
		IF NOT IS_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(tshirt,bIsPlayerMale), TATTOO_MP_FM))

			//SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(tshirt,bIsPlayerMale), TATTOO_MP_FM), TRUE, FALSE)
			SET_BITSET_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(tshirt,bIsPlayerMale), TATTOO_MP_FM), TRUE, -1)
			NET_PRINT("UNLOCK_A_DLC_AWARD_TSHIRT UNlock Retro  t shirt for a boy")
			PRINTLN("UNLOCK_A_DLC_AWARD_TSHIRT DLC_AWARD_SHIRT_ENUM = ", ENUM_TO_INT( GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(tshirt,bIsPlayerMale), TATTOO_MP_FM)))
			IF GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(tshirt,bIsPlayerMale), TATTOO_MP_FM) !=INVALID_TATTOO 
				bRun_ticker = TRUE
				NET_PRINT("UNLOCK_A_DLC_AWARD_TSHIRT TATTOO IS VALID male, run ticker")
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(tshirt,bIsPlayerMale), TATTOO_MP_FM_F))
			//SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(tshirt,bIsPlayerMale), TATTOO_MP_FM_F), TRUE, FALSE)
			SET_BITSET_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(tshirt,bIsPlayerMale), TATTOO_MP_FM_F), TRUE, -1)
			NET_PRINT("UNLOCK_A_DLC_AWARD_TSHIRT UNlock Retro  t shirt for a girl")
			PRINTLN("UNLOCK_A_DLC_AWARD_TSHIRT DLC_AWARD_SHIRT_ENUM = ", ENUM_TO_INT( GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(tshirt,bIsPlayerMale), TATTOO_MP_FM_F)))
			IF GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(tshirt,bIsPlayerMale), TATTOO_MP_FM_F) !=INVALID_TATTOO 
				bRun_ticker = TRUE
				NET_PRINT("UNLOCK_A_DLC_AWARD_TSHIRT TATTOO IS VALID female, run ticker")
			ENDIF
		ENDIF
	ENDIF
	
	IF bRun_ticker = TRUE
		SWITCH tshirt
			CASE DLC_SHIRT_AWARD_DEATH_DEFYING
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_DEATH_DEFYING)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_SHIRT4", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
					//PRINT_TICKER("UNLOCK_AWRD_SHIRT4") 
					//You can now purchase the retro 'Death Defying' T-shirt from any clothes store.
					NET_PRINT(" UNLOCK_A_DLC_AWARD_TSHIRT UNLOCK_AWRD_SHIRT4")
					SET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_DEATH_DEFYING, TRUE)
				ENDIF
			BREAK
			
			CASE DLC_SHIRT_AWARD_FOR_HIRE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_FOR_HIRE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_SHIRT5", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
					//PRINT_TICKER("UNLOCK_AWRD_SHIRT5") 
					//You can now purchase the retro 'For Hire' T-shirt from any clothes store.
					NET_PRINT(" UNLOCK_A_DLC_AWARD_TSHIRT UNLOCK_AWRD_SHIRT5")
					SET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_FOR_HIRE, TRUE)
				ENDIF
			BREAK
			
			CASE DLC_SHIRT_AWARD_SHOT_CALLER
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_SHOT_CALLER)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_SHIRT6", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
					//PRINT_TICKER("UNLOCK_AWRD_SHIRT6") 
					//You can now purchase the retro 'Shot caller' T-shirt from any clothes store.
					NET_PRINT(" UNLOCK_A_DLC_AWARD_TSHIRT UNLOCK_AWRD_SHIRT6")
					SET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_SHOT_CALLER, TRUE)
					
				ENDIF
			BREAK
			
			CASE DLC_SHIRT_AWARD_LIVE_A_LITTLE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_LIVE_A_LITTLE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_SHIRT7", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
					//PRINT_TICKER("UNLOCK_AWRD_SHIRT7") 
					//You can now purchase the retro 'Live a Little' T-shirt from any clothes store.
						NET_PRINT(" UNLOCK_A_DLC_AWARD_TSHIRT UNLOCK_AWRD_SHIRT7")
					SET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_LIVE_A_LITTLE, TRUE)
				ENDIF
			BREAK
			
			CASE DLC_SHIRT_AWARD_ASSHOLE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_ASSHOLE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_SHIRT8", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
					//PRINT_TICKER("UNLOCK_AWRD_SHIRT8") 
					//You can now purchase the retro 'Asshole' T-shirt from any clothes store.
						NET_PRINT(" UNLOCK_A_DLC_AWARD_TSHIRT UNLOCK_AWRD_SHIRT8")
					SET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_ASSHOLE, TRUE)
				ENDIF
			BREAK
			
			CASE DLC_SHIRT_AWARD_CANT_TOUCH_THIS
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_CANT_TOUCH_THIS)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_SHIRT9", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
					//PRINT_TICKER("UNLOCK_AWRD_SHIRT9") 
					//You can now purchase the retro 'Can't Touch This' T-shirt from any clothes store.
					NET_PRINT(" UNLOCK_A_DLC_AWARD_TSHIRT UNLOCK_AWRD_SHIRT9")
					SET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_CANT_TOUCH_THIS, TRUE)
				ENDIF
			BREAK
			
			CASE DLC_SHIRT_AWARD_DECORATED
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_DECORATED)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_SHIRT10", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
					//PRINT_TICKER("UNLOCK_AWRD_SHIRT10") 
					//You can now purchase the retro 'Decorated' T-shirt from any clothes store.
					NET_PRINT(" UNLOCK_A_DLC_AWARD_TSHIRT UNLOCK_AWRD_SHIRT10")
					SET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_DECORATED, TRUE)
				ENDIF
			BREAK
			
			CASE DLC_SHIRT_AWARD_PSYCHO_KILLER
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_PSYCHO_KILLER)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_SHIRT11", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
					//PRINT_TICKER("UNLOCK_AWRD_SHIRT11") 
					//You can now purchase the retro 'Psycho Killer' T-shirt from any clothes store.
					NET_PRINT(" UNLOCK_A_DLC_AWARD_TSHIRT UNLOCK_AWRD_SHIRT11")
					SET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_PSYCHO_KILLER, TRUE)
				ENDIF
			BREAK
			
			CASE DLC_SHIRT_AWARD_ONE_MAN_ARMY
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_ONE_MAN_ARMY)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_SHIRT12", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
					//PRINT_TICKER("UNLOCK_AWRD_SHIRT12") 
					//You can now purchase the retro 'One Man Army' T-shirt from any clothes store.
					NET_PRINT(" UNLOCK_A_DLC_AWARD_TSHIRT UNLOCK_AWRD_SHIRT12")
					SET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_ONE_MAN_ARMY, TRUE)
				ENDIF
			BREAK
			
			CASE DLC_SHIRT_AWARD_SHOWROOM
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_SHOWROOM)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_SHIRT13", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
					//PRINT_TICKER("UNLOCK_AWRD_SHIRT13") 
					//You can now purchase the retro 'Showroom' T-shirt from any clothes store.
					NET_PRINT(" UNLOCK_A_DLC_AWARD_TSHIRT UNLOCK_AWRD_SHIRT13")
					SET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_SHOWROOM,TRUE)
				ENDIF

			BREAK
			
			CASE DLC_SHIRT_HEIST_ELITE
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_ELITE)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_ELITE", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
					//PRINT_TICKER("UNLOCK_AWRD_SHIRT13") 
					//You can now purchase the retro 'Showroom' T-shirt from any clothes store.
					NET_PRINT(" UNLOCK_A_DLC_AWARD_TSHIRT DLC_SHIRT_HEIST_ELITE")
					SET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_ELITE,TRUE)
				ENDIF
			BREAK
			
			
			CASE DLC_SHIRT_HEIST_ELITE_1
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_ELITE1)
					SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_NAME_ELITE_1", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
					//PRINT_TICKER("UNLOCK_AWRD_SHIRT13") 
					//You can now purchase the retro 'Showroom' T-shirt from any clothes store.
					NET_PRINT(" UNLOCK_A_DLC_AWARD_TSHIRT DLC_SHIRT_HEIST_ELITE_1")
					SET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_ELITE1,TRUE)
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDIF
	
	/*
	DLC_SHIRT_AWARD_DEATH_DEFYING = 0,
	DLC_SHIRT_AWARD_FOR_HIRE,
	DLC_SHIRT_AWARD_LIVE_A_LITTLE,
	DLC_SHIRT_AWARD_ASSHOLE,
	DLC_SHIRT_AWARD_CANT_TOUCH_THIS,
	DLC_SHIRT_AWARD_DECORATED,
	DLC_SHIRT_AWARD_PSYCHO_KILLER,
	DLC_SHIRT_AWARD_ONE_MAN_ARMY,
	DLC_SHIRT_AWARD_SHOT_CALLER,
	DLC_SHIRT_AWARD_SHOWROOM,*/
ENDPROC

FUNC BOOL IS_DLC_AWARD_TSHIRT_UNLOCKED(DLC_AWARD_SHIRT_ENUM tshirt)

	BOOL bIsPlayerMale = (GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE) = 0)

	
	IF bIsPlayerMale
		IF IS_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(tshirt,bIsPlayerMale), TATTOO_MP_FM))
			RETURN(TRUE)
			
		ELSE
	
			RETURN(FALSE)
		ENDIF
			
	ELSE
		IF IS_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(tshirt,bIsPlayerMale), TATTOO_MP_FM_F))
			RETURN(TRUE)
			
		ELSE
	
			RETURN(FALSE)
		ENDIF
	ENDIF

ENDFUNC
#ENDIF

FUNC BOOL CHECK_AND_GIVE_MISS_RACE_DM_DAILY_XP()

    // CURRENT date
    STRUCT_STAT_DATE currentdate
    GET_POSIX_TIME(currentdate.Year, currentdate.Month, currentdate.Day, currentdate.Hour, currentdate.Minute, currentdate.Seconds)
    
    // recorded date
    STRUCT_STAT_DATE STAT_VALUE
    INT rp_award_amount = 500
    MP_DATE_STATS thestat
    IF ARE_ALL_INSTANCED_TUTORIALS_COMPLETE() 
    AND GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR
        IF g_bFM_ON_TEAM_MISSION = TRUE
            STAT_VALUE = GET_MP_DATE_CHARACTER_STAT(MP_STAT_REW_DAILY_MISS)
            thestat = MP_STAT_REW_DAILY_MISS
        ELIF g_bFM_ON_TEAM_DEATHMATCH = TRUE
            STAT_VALUE = GET_MP_DATE_CHARACTER_STAT(MP_STAT_REW_DAILY_DM)
            thestat = MP_STAT_REW_DAILY_DM
        ELIF g_b_On_Race = TRUE
            STAT_VALUE = GET_MP_DATE_CHARACTER_STAT(MP_STAT_REW_DAILY_RACE)
            
            thestat = MP_STAT_REW_DAILY_RACE
        ENDIF
        
        

        IF currentdate.Year > STAT_VALUE.Year // if a year is greater
            SET_MP_DATE_CHARACTER_STAT(thestat, currentdate)
            #IF IS_DEBUG_BUILD
                NET_PRINT("\n CHECK_AND_GIVE_MISS_RACE_DM_DAILY_XP: Give player reward 1 \n ")
            #ENDIF
            
            IF g_bFM_ON_TEAM_MISSION = TRUE
                GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_3",XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DAILY_RACEDMMISSDONE,  ROUND(rp_award_amount *g_sMPTunables.fxp_tunable_Daily_Mission) , 1)
                RETURN TRUE
                //PRINT_TICKER_WITH_INT("REWXP_3", 500)
            ELIF g_b_On_Deathmatch = TRUE
			
                GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_1", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DAILY_RACEDMMISSDONE,ROUND(rp_award_amount *g_sMPTunables.fxp_tunable_Daily_Deathmatch), 1)
                RETURN TRUE
                //PRINT_TICKER_WITH_INT("REWXP_1", 500)
            ELIF g_b_On_Race = TRUE

                GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_2", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DAILY_RACEDMMISSDONE,ROUND(rp_award_amount *g_sMPTunables.fxp_tunable_Daily_Race), 1) 
                RETURN TRUE
                //PRINT_TICKER_WITH_INT("REWXP_2", 500)
                
            ENDIF

        ELIF currentdate.Year = STAT_VALUE.Year 
            AND currentdate.Month > STAT_VALUE.Month //same year, but greater month
                SET_MP_DATE_CHARACTER_STAT(thestat, currentdate)
                #IF IS_DEBUG_BUILD
                    NET_PRINT("\n CHECK_AND_GIVE_MISS_RACE_DM_DAILY_XP: Give player reward 2 \n ")
                #ENDIF
                IF g_bFM_ON_TEAM_MISSION = TRUE
                    GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_3", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DAILY_RACEDMMISSDONE,ROUND(rp_award_amount *g_sMPTunables.fxp_tunable_Daily_Mission) , 1)
                    RETURN TRUE
                    //PRINT_TICKER_WITH_INT("REWXP_3", 500)
                ELIF g_b_On_Deathmatch = TRUE
                    GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_1", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DAILY_RACEDMMISSDONE,ROUND(rp_award_amount *g_sMPTunables.fxp_tunable_Daily_Deathmatch), 1)
                    RETURN TRUE
                    //PRINT_TICKER_WITH_INT("REWXP_1", 500)
                ELIF g_b_On_Race = TRUE
                    GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_2", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DAILY_RACEDMMISSDONE,ROUND(rp_award_amount *g_sMPTunables.fxp_tunable_Daily_Race), 1)
                    RETURN TRUE
                    //PRINT_TICKER_WITH_INT("REWXP_2", 500)
                ENDIF

                
        ELIF currentdate.Year = STAT_VALUE.Year     //check if 24 hours have gone by within the month
            AND currentdate.Month = STAT_VALUE.Month 
            AND currentdate.Day  > STAT_VALUE.Day 
            AND currentdate.Hour >= STAT_VALUE.Hour
                // give reward and set new stat value
                SET_MP_DATE_CHARACTER_STAT(thestat, currentdate)
                #IF IS_DEBUG_BUILD
                    NET_PRINT("\n CHECK_AND_GIVE_MISS_RACE_DM_DAILY_XP: Give player reward 3 \n ")
                #ENDIF 
                
                IF g_bFM_ON_TEAM_MISSION = TRUE
                    GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_3", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DAILY_RACEDMMISSDONE,ROUND(rp_award_amount *g_sMPTunables.fxp_tunable_Daily_Mission) , 1)
                    RETURN TRUE
                    //PRINT_TICKER_WITH_INT("REWXP_3", 500)
                ELIF g_b_On_Deathmatch = TRUE
                    GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_1", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DAILY_RACEDMMISSDONE,ROUND(rp_award_amount *g_sMPTunables.fxp_tunable_Daily_Deathmatch), 1)
                    RETURN TRUE
                    //PRINT_TICKER_WITH_INT("REWXP_1", 500)
                ELIF g_b_On_Race = TRUE
                    GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_2", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DAILY_RACEDMMISSDONE,ROUND(rp_award_amount *g_sMPTunables.fxp_tunable_Daily_Race), 1)
                    RETURN TRUE
                    //PRINT_TICKER_WITH_INT("REWXP_2", 500)
                ENDIF

        ELSE
            
            #IF IS_DEBUG_BUILD
                    NET_PRINT("\n Somethings gone wrong CHECK_AND_GIVE_MISS_RACE_DM_DAILY_XP \n ")
            #ENDIF
            RETURN FALSE
        ENDIF
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

/*
FUNC BOOL CHECK_AND_GIVE_MISS_RACE_DM_DAILY_XP_FUNC()

    // CURRENT date
    STRUCT_STAT_DATE currentdate
    GET_POSIX_TIME(currentdate.Year, currentdate.Month, currentdate.Day, currentdate.Hour, currentdate.Minute, currentdate.Seconds)
    
    // recorded date
    STRUCT_STAT_DATE STAT_VALUE
    
    MP_DATE_STATS thestat
    IF ARE_ALL_INSTANCED_TUTORIALS_COMPLETE() 
    AND GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR
        IF g_bFM_ON_TEAM_MISSION = TRUE
            STAT_VALUE = GET_MP_DATE_CHARACTER_STAT(MP_STAT_REW_DAILY_MISS)
            thestat = MP_STAT_REW_DAILY_MISS
        ELIF g_bFM_ON_TEAM_DEATHMATCH = TRUE
            STAT_VALUE = GET_MP_DATE_CHARACTER_STAT(MP_STAT_REW_DAILY_DM)
            thestat = MP_STAT_REW_DAILY_DM
        ELIF g_b_On_Race = TRUE
            STAT_VALUE = GET_MP_DATE_CHARACTER_STAT(MP_STAT_REW_DAILY_RACE)
            
            thestat = MP_STAT_REW_DAILY_RACE
        ENDIF
        
        

        IF currentdate.Year > STAT_VALUE.Year // if a year is greater
            SET_MP_DATE_CHARACTER_STAT(thestat, currentdate)
            
             
            IF g_bFM_ON_TEAM_MISSION = TRUE
                GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_3", 500, 1)
                //PRINT_TICKER_WITH_INT("REWXP_3", 500)
            ELIF g_bFM_ON_TEAM_DEATHMATCH = TRUE
                GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_1", 500, 1)
                //PRINT_TICKER_WITH_INT("REWXP_1", 500)
            ELIF g_b_On_Race = TRUE

                GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_2", 500, 1)
                //PRINT_TICKER_WITH_INT("REWXP_2", 500)
                
            ENDIF
            RETURN TRUE
                #IF IS_DEBUG_BUILD
                    NET_PRINT("\n CHECK_AND_GIVE_MISS_RACE_DM_DAILY_XP: Give player reward 1 \n ")
                #ENDIF
        ELIF currentdate.Year = STAT_VALUE.Year 
            AND currentdate.Month > STAT_VALUE.Month //same year, but greater month
                SET_MP_DATE_CHARACTER_STAT(thestat, currentdate)
                 
                IF g_bFM_ON_TEAM_MISSION = TRUE
                    GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_3", 500, 1)
                    //PRINT_TICKER_WITH_INT("REWXP_3", 500)
                ELIF g_bFM_ON_TEAM_DEATHMATCH = TRUE
                    GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_1", 500, 1)
                    //PRINT_TICKER_WITH_INT("REWXP_1", 500)
                ELIF g_b_On_Race = TRUE
                    GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_2", 500, 1)
                    //PRINT_TICKER_WITH_INT("REWXP_2", 500)
                ENDIF
                
                RETURN TRUE
                #IF IS_DEBUG_BUILD
                    NET_PRINT("\n CHECK_AND_GIVE_MISS_RACE_DM_DAILY_XP: Give player reward 2 \n ")
                #ENDIF
                
        ELIF currentdate.Year = STAT_VALUE.Year     //check if 24 hours have gone by within the month
            AND currentdate.Month = STAT_VALUE.Month 
            AND currentdate.Day  > STAT_VALUE.Day 
            AND currentdate.Hour >= STAT_VALUE.Hour
                // give reward and set new stat value
                SET_MP_DATE_CHARACTER_STAT(thestat, currentdate)
                 
                
                IF g_bFM_ON_TEAM_MISSION = TRUE
                    GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_3", 500, 1)
                    //PRINT_TICKER_WITH_INT("REWXP_3", 500)
                ELIF g_bFM_ON_TEAM_DEATHMATCH = TRUE
                    GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_1", 500, 1)
                    //PRINT_TICKER_WITH_INT("REWXP_1", 500)
                ELIF g_b_On_Race = TRUE
                    GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_2", 500, 1)
                    //PRINT_TICKER_WITH_INT("REWXP_2", 500)
                ENDIF
                
                
                RETURN TRUE
                #IF IS_DEBUG_BUILD
                    NET_PRINT("\n CHECK_AND_GIVE_MISS_RACE_DM_DAILY_XP: Give player reward 3 \n ")
                #ENDIF
        ELSE
        
            #IF IS_DEBUG_BUILD
                    NET_PRINT("\n Somethings gone wrong CHECK_AND_GIVE_MISS_RACE_DM_DAILY_XP \n ")
            #ENDIF
        ENDIF
    ENDIF

    RETURN FALSE
ENDFUNC
*/

/// PURPOSE:
///    Counts how many people are active in a players crew
/// PARAMS:
///    bNotCheckActivePart - if this is true we check if they are active participents
///    bIncludePlayer - include player in this check
/// RETURNS:
///    
FUNC INT GET_NUMBER_OF_ACTIVE_PLAYERS_IN_PLAYERS_CREW()
    IF NOT g_bInMultiplayer 
        RETURN 0
    ENDIF
    
    IF NOT NETWORK_IS_GAME_IN_PROGRESS()
        RETURN 0
    ENDIF
    
    IF NOT IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
        RETURN 0
    ENDIF
    
    GAMER_HANDLE hdle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
    INT iPlayerClanID = GET_PLAYER_CLAN_ID(hdle)
    
    CPRINTLN(DEBUG_ACHIEVEMENT, "Players Clan ID:", iPlayerClanID)
    
    GAMER_HANDLE ThisPlayerHdle
    PLAYER_INDEX ThisPlayerID
    INT iThisPlayerClan
    
    INT iParticipant
    INT iClanCount
    PARTICIPANT_INDEX iParticipantID
    
    REPEAT NUM_NETWORK_PLAYERS iParticipant
        iParticipantID = INT_TO_PARTICIPANTINDEX(iParticipant)
        IF NETWORK_IS_PARTICIPANT_ACTIVE(iParticipantID)
            IF IS_NET_PARTICIPANT_INT_ID_VALID(iParticipant)
                ThisPlayerID = NETWORK_GET_PLAYER_INDEX(iParticipantID)
                IF (ThisPlayerID <> INVALID_PLAYER_INDEX())
                    IF NETWORK_IS_PLAYER_ACTIVE(ThisPlayerID)
                        ThisPlayerHdle = GET_GAMER_HANDLE_PLAYER(ThisPlayerID)
                        iThisPlayerClan = GET_PLAYER_CLAN_ID(ThisPlayerHdle)
                        CPRINTLN(DEBUG_ACHIEVEMENT, " Participant:", iParticipant, " Clan ID:", iThisPlayerClan)
                        IF (iThisPlayerClan = iPlayerClanID) AND (iThisPlayerClan <> -1) AND (iPlayerClanID <> -1)
                            iClanCount ++
                        ENDIF           
                    ELSE
                        CPRINTLN(DEBUG_ACHIEVEMENT, " Participant:", iParticipant, " is not okay")
                    ENDIF
                ELSE
                    CPRINTLN(DEBUG_ACHIEVEMENT, " Participant:", iParticipant, " has invalid player index")
                ENDIF
            ELSE
                CPRINTLN(DEBUG_ACHIEVEMENT, " Participant:", iParticipant, " isn't valid")
            ENDIF
        ELSE
            CPRINTLN(DEBUG_ACHIEVEMENT, " Participant:", iParticipant, " isn't active")
        ENDIF
    ENDREPEAT
    
    CPRINTLN(DEBUG_ACHIEVEMENT, "Active in Player's Clan:", iClanCount)
    RETURN iClanCount
ENDFUNC

PROC DO_CREW_CUT_ACHIEVEMENT()
    
    INT crewcnt 
    
    IF GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR
        IF IS_LOCAL_PLAYER_IN_ACTIVE_CLAN() 
		
            crewcnt = GET_NUMBER_OF_ACTIVE_PLAYERS_IN_PLAYERS_CREW()
            IF crewcnt >= 2
                AWARD_ACHIEVEMENT(ACH46) // Crew Cut
				SET_HEIST_ACHIEVEMENT_ID(HEISTACH_CREW_CUT, FALSE)
				SET_HEIST_ACHIEVEMENT_ID(HEISTACH_CREW_CUT_SANCHECK, FALSE)
			ELSE
				PRINTLN("DO_CREW_CUT_ACHIEVEMENT fail crewcnt =  ", crewcnt)
            ENDIF  
		ELSE
			PRINTLN("DO_CREW_CUT_ACHIEVEMENT fail IS_LOCAL_PLAYER_IN_ACTIVE_CLAN = false ")
        ENDIF   
    ENDIF
ENDPROC

// this should be called when player completes a fm mission (used by RC)
/*
FUNC BOOL HAS_PLAYER_PLAYED_5_MISSIONS_TODAY_REWARD()

    STRUCT_STAT_DATE currentdate
    GET_POSIX_TIME(currentdate.Year, currentdate.Month, currentdate.Day, currentdate.Hour, currentdate.Minute, currentdate.Seconds)
    MP_DATE_STATS thestat
    STRUCT_STAT_DATE STAT_VALUE
    
    thestat = MP_STAT_REW_DAILY_5_MISS
    
    
    STAT_VALUE = GET_MP_DATE_CHARACTER_STAT(thestat)
    IF GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR
    AND  ARE_ALL_INSTANCED_TUTORIALS_COMPLETE() 
        IF currentdate.Year > STAT_VALUE.Year // if a year is greater
        // give reward and set new stat value
            
            SET_MP_DATE_CHARACTER_STAT(thestat, currentdate)
            SET_MP_INT_CHARACTER_STAT(MP_STAT_REW_DAILY_5_MISS_COUNTER, 0)
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("\n HAS_A_REAL_TIME_DAY_GONE_BY: REset day 1\n ")
            #ENDIF
            
        ELIF currentdate.Year = STAT_VALUE.Year 
            AND currentdate.Month > STAT_VALUE.Month //same year, but greater month
            // give reward and set new stat value
            SET_MP_DATE_CHARACTER_STAT(thestat, currentdate)
            SET_MP_INT_CHARACTER_STAT(MP_STAT_REW_DAILY_5_MISS_COUNTER, 0)
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("\n  HAS_A_REAL_TIME_DAY_GONE_BY: REset day 2 \n ")
            #ENDIF

        ELIF currentdate.Year = STAT_VALUE.Year     //check if 24 hours have gone by within the month
            AND currentdate.Month = STAT_VALUE.Month 
            AND currentdate.Day  > STAT_VALUE.Day 
            AND currentdate.Hour >= STAT_VALUE.Hour
                // give reward and set new stat value
                SET_MP_DATE_CHARACTER_STAT(thestat, currentdate)
                SET_MP_INT_CHARACTER_STAT(MP_STAT_REW_DAILY_5_MISS_COUNTER, 0)
                #IF IS_DEBUG_BUILD
                    NET_PRINT("\n  HAS_A_REAL_TIME_DAY_GONE_BY: REset day 3 \n ")
                #ENDIF

        ELSE
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("\n A day has NOT gone by HAS_A_REAL_TIME_DAY_GONE_BY: Same day since player started \n ")
            #ENDIF
            
            INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_REW_DAILY_5_MISS_COUNTER, 1)
            
            
            
            IF GET_MP_INT_CHARACTER_STAT(MP_STAT_REW_DAILY_5_MISS_COUNTER) >= 5
                // GIVE REWARD
                SET_MP_INT_CHARACTER_STAT(MP_STAT_REW_DAILY_5_MISS_COUNTER, 0)
                //GIVE_LOCAL_PLAYER_XP("REWXP5MISS", 1000, 1)
                GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP5MISS",XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_PLAYED_5_MISSIONS_TODAY, 1000, 1)
                //PRINT_TICKER_WITH_INT("REWXP5MISS", 1000)
                NET_PRINT("\n HAS_PLAYER_PLAYED_5_MISSIONS_TODAY_REWARD: 1000\n ")
                RETURN TRUE
            ENDIF

            

        ENDIF
    ENDIF
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("\n Somethings gone wrong HAS_A_REAL_TIME_DAY_GONE_BY \n ")
    #ENDIF
        
    RETURN FALSE
    
ENDFUNC

*/







FUNC BOOL ARE_ALL_TATTOOS_PATCHES_UNLOCKED(INT iTeam)

    SWITCH iTeam
    
        CASE TEAM_FREEMODE

            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_UP_SHOPS_1) = FALSE
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_UP_SHOPS_2) = FALSE
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_UP_SHOPS_3) = FALSE
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_UP_SHOPS_4) = FALSE
                RETURN FALSE
            ENDIF
            
                
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_GANGHIDEOUT_CLEAR) = FALSE
            
                RETURN FALSE
            ENDIF
            /*
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_VAG_RACES_WON_x1) = FALSE
                RETURN FALSE
            ENDIF

            
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_VAG_KILL_RIVAL_x10) = FALSE
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_VAG_WIN_PARLEY) = FALSE
            
                RETURN FALSE
            ENDIF
            
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_GANGHIDEOUT_CLEAR) = FALSE
            
                RETURN FALSE
            ENDIF*/
            
            

            IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_HEAD_BANGER) = FALSE
            
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_SLAYER) = FALSE
            
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_GANGHIDEOUT_CLEAR) = FALSE
            
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_HUSTLER) = FALSE
            
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_ARMOURED_VAN_TAKEDOWN) = FALSE
            
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_WIN_EVER_MODE_ONCE) = FALSE
            
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_BOUNTY_KILLER) = FALSE
            
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_HOLD_WORLD_RECORD) = FALSE
            
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED(   TATTOO_MP_FM_FULL_MODDED) = FALSE
            
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_REVENGE_KILL) = FALSE
            
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_KILL_3_RACERS) = FALSE
            
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_REACH_RANK_1) = FALSE
            
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_REACH_RANK_2) = FALSE
            
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_REACH_RANK_3) = FALSE
            
                RETURN FALSE
            ENDIF

            IF IS_MP_TATTOO_UNLOCKED(   TATTOO_MP_FM_CREW_A) = FALSE
            
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_CREW_B) = FALSE
            
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_CREW_C) = FALSE
            
                RETURN FALSE
            ENDIF
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_CREW_D) = FALSE
            
                RETURN FALSE
            ENDIF
            
            #IF USE_TU_CHANGES
                IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_CREW_E) = FALSE
                
                    RETURN FALSE
                ENDIF
                IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_CREW_F) = FALSE
                
                    RETURN FALSE
                ENDIF
                
            #ENDIF
            
            IF IS_MP_TATTOO_UNLOCKED(TSHIRT_TRANS_MP_FM_REDSKULL) = FALSE
            
                RETURN FALSE
            ENDIF
            
//          IF IS_MP_TATTOO_UNLOCKED(TSHIRT_TRANS_MP_FM_LSBELLE) = FALSE
//          
//              RETURN FALSE
//          ENDIF
            
            IF IS_MP_TATTOO_UNLOCKED(TSHIRT_TRANS_MP_FM_ROCKSTAR) = FALSE
                RETURN FALSE
            ENDIF
            
            
            
        BREAK
    ENDSWITCH

    


RETURN TRUE

ENDFUNC



    

/*
FUNC INT GET_FM_TATTOO_UNLOCK_STAT_VALUE()

    SWITCH playerhealth

    ENDSWITCH
    
    #IF IS_DEBUG_BUILD
        DEBUG_PRINTCALLSTACK()
        NET_PRINT(" - GET_FM_CLOTHES_UNLOCK_RANK - CLOTHES NOT FOUND RETURNING RANK 0")NET_NL()
    #ENDIF
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_VAG_HOLD_UP_SHOPS_x1) = FALSE

            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_VAG_HOLD_UP_SHOPS_x5) = FALSE

            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_VAG_HOLD_UP_SHOPS_x10) = FALSE

            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_VAG_HOLD_UP_SHOPS_x20) = FALSE

            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_VAG_RACES_WON_x1) = FALSE

            
            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_VAG_KILL_RIVAL_x10) = FALSE

            IF IS_MP_TATTOO_UNLOCKED(TATTOO_MP_VAG_WIN_PARLEY) = FALSE
        
    RETURN 0
ENDFUNC*/

PROC TRACK_MP_AWARD_NEVERABADCOP()
//MP_STAT_CHAR_XP
//MP_STAT_CHAR_BADCOP



ENDPROC





PROC TRACK_TIME_IN_HELI(int current_time)
    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
        IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
            VEHICLE_INDEX temp_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
            IF IS_ENTITY_IN_AIR(temp_vehicle)
                IF GET_TOTAL_NUMBER_OF_HOURS_FOR_MP_UNSIGNED_INT_STAT(MP_STAT_TIME_DRIVING_HELI) >= 1
                    INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIMEINHELI,current_time )
                    
                    SET_MP_INT_CHARACTER_AWARD(MP_AWARD_TIME_IN_HELICOPTER, GET_TOTAL_NUMBER_OF_HOURS_FOR_MP_UNSIGNED_INT_STAT(MP_STAT_TIME_DRIVING_HELI))
                //INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_TIME_IN_HELICOPTER,CONVERT_INT_MILLISECONDS_TO_HOURS(GET_MP_INT_CHARACTER_STAT(MP_STAT_TIMEINHELI)) )
                ENDIF
            ENDIF
        ENDIF
    ENDIF

ENDPROC



PROC CHECK_AND_GIVE_BOOL_AWARD( MP_BOOL_AWARD anaward, int iteam)
	IF NOT IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED (anAward) 
        IF GET_MP_BOOL_CHARACTER_AWARD(anAward)
            IF CAN_DRAW_UNLOCKS_SCALEFORM()
				SET_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(anAward, TRUE, iTeam)
			ENDIF
        ENDIF
    ELSE
        IF GET_MP_BOOL_CHARACTER_AWARD(anAward) = FALSE
            IF CAN_DRAW_UNLOCKS_SCALEFORM()
				SET_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(anAward, FALSE, iTeam)
			ENDIF
        ENDIF
	ENDIF
	
	
	// FIX 5902954
	IF IS_CASINO_BOOL_AWARD(anaward) 
		CHECK_AND_SET_CASINO_BOOL_AWARD(anaward) 
	ENDIF
	
	IF IS_TUNER_BOOL_AWARD(anaward) 
		CHECK_AND_SET_TUNER_BOOL_AWARD(anaward) 
	ENDIF
	
	
ENDPROC

PROC CHECK_AND_GIVE_PLAYER_BOOL_AWARD(MPPLY_BOOL_AWARD_INDEX anaward, int iteam)
	IF NOT IS_MP_AWARD_PLATINUM_BOOLPLY_UNLOCKED(anAward) 
        IF GET_MP_BOOL_PLAYER_AWARD(anAward)
            IF CAN_DRAW_UNLOCKS_SCALEFORM()
				SET_MP_AWARD_PLATINUM_BOOLPLY_UNLOCKED(anAward, TRUE, iTeam)
			ENDIF
        ENDIF
    ELSE
        IF GET_MP_BOOL_PLAYER_AWARD(anAward) = FALSE
            IF CAN_DRAW_UNLOCKS_SCALEFORM()
				SET_MP_AWARD_PLATINUM_BOOLPLY_UNLOCKED(anAward, FALSE, iTeam)
			ENDIF
        ENDIF
    ENDIF
ENDPROC

PROC GIVE_EXTRA_REWARD_FOR_INT_AWARDS(MP_INT_AWARD anaward, AWARDPOSITIONS awardposition)
	
	
	SWITCH anaward
		CASE MP_AWARD_ODD_JOBS 
			SWITCH awardposition
				CASE AWARDPOSITIONS_BRONZE
					//GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "AWR_845", XPTYPE_AWARDS, XPCATEGORY_BRONZE_AWARD, g_sMPtunables.iVC_AWARD_RP_HIGH_ROLLER_BRONZE )
					
					IF USE_SERVER_TRANSACTIONS()
						INT iScriptTransactionIndex
						PRINTLN("[CASH_TRANSACTION] - GIVE_EXTRA_REWARD_FOR_INT_AWARDS - Casino High Roller Bronze Award - TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION, SERVICE_EARN_CASINO_AWARD_HIGH_ROLLER_BRONZE - ", g_sMPtunables.iVC_AWARD_CASH_HIGH_ROLLER_BRONZE)
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_CASINO_AWARD_HIGH_ROLLER_BRONZE, g_sMPtunables.iVC_AWARD_CASH_HIGH_ROLLER_BRONZE, iScriptTransactionIndex, TRUE)
						g_cashTransactionData[iScriptTransactionIndex].cashInfo.iItemHash = HASH("HIGH_ROLLER_BRONZE")
					ELSE
						PRINTLN("[CASH_TRANSACTION] - GIVE_EXTRA_REWARD_FOR_INT_AWARDS - Casino High Roller Bronze Award - NETWORK_EARN_CASINO_AWARD - ", g_sMPtunables.iVC_AWARD_CASH_HIGH_ROLLER_BRONZE)
						NETWORK_EARN_CASINO_AWARD(g_sMPtunables.iVC_AWARD_CASH_HIGH_ROLLER_BRONZE, HASH("HIGH_ROLLER_BRONZE"))
					ENDIF
				BREAK
				
				
				CASE AWARDPOSITIONS_SILVER
				//	GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "AWR_845", XPTYPE_AWARDS, XPCATEGORY_SILVER_AWARD, g_sMPtunables.iVC_AWARD_RP_HIGH_ROLLER_SILVER )
					IF USE_SERVER_TRANSACTIONS()
						INT iScriptTransactionIndex
						PRINTLN("[CASH_TRANSACTION] - GIVE_EXTRA_REWARD_FOR_INT_AWARDS - Casino High Roller Silver Award - TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION, SERVICE_EARN_CASINO_AWARD_HIGH_ROLLER_SILVER - ", g_sMPtunables.iVC_AWARD_CASH_HIGH_ROLLER_SILVER)
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_CASINO_AWARD_HIGH_ROLLER_SILVER, g_sMPtunables.iVC_AWARD_CASH_HIGH_ROLLER_SILVER, iScriptTransactionIndex, TRUE)
						g_cashTransactionData[iScriptTransactionIndex].cashInfo.iItemHash = HASH("HIGH_ROLLER_SILVER")
					ELSE
						PRINTLN("[CASH_TRANSACTION] - GIVE_EXTRA_REWARD_FOR_INT_AWARDS - Casino High Roller Award - NETWORK_EARN_CASINO_AWARD - ", g_sMPtunables.iVC_AWARD_CASH_HIGH_ROLLER_SILVER)
						NETWORK_EARN_CASINO_AWARD(g_sMPtunables.iVC_AWARD_CASH_HIGH_ROLLER_SILVER, HASH("HIGH_ROLLER_SILVER"))
					ENDIF
				BREAK
				
				CASE AWARDPOSITIONS_GOLD
					//GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "AWR_845", XPTYPE_AWARDS, XPCATEGORY_GOLD_AWARD, g_sMPtunables.iVC_AWARD_RP_HIGH_ROLLER_GOLD )
					IF USE_SERVER_TRANSACTIONS()
						INT iScriptTransactionIndex
						PRINTLN("[CASH_TRANSACTION] - GIVE_EXTRA_REWARD_FOR_INT_AWARDS - Casino High Roller Gold Award - TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION, SERVICE_EARN_CASINO_AWARD_HIGH_ROLLER_GOLD - ", g_sMPtunables.iVC_AWARD_CASH_HIGH_ROLLER_GOLD)
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_CASINO_AWARD_HIGH_ROLLER_GOLD, g_sMPtunables.iVC_AWARD_CASH_HIGH_ROLLER_GOLD, iScriptTransactionIndex, TRUE)
						g_cashTransactionData[iScriptTransactionIndex].cashInfo.iItemHash = HASH("HIGH_ROLLER_GOLD")
					ELSE
						PRINTLN("[CASH_TRANSACTION] - GIVE_EXTRA_REWARD_FOR_INT_AWARDS - Casino High Roller Gold Award - NETWORK_EARN_CASINO_AWARD - ", g_sMPtunables.iVC_AWARD_CASH_HIGH_ROLLER_GOLD)
						NETWORK_EARN_CASINO_AWARD(g_sMPtunables.iVC_AWARD_CASH_HIGH_ROLLER_GOLD, HASH("HIGH_ROLLER_GOLD"))
					ENDIF
				
				BREAK
				
				CASE AWARDPOSITIONS_PLATINUM
					//GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "AWR_845", XPTYPE_AWARDS, XPCATEGORY_PLATINUM_AWARD, g_sMPtunables.iVC_AWARD_RP_HIGH_ROLLER_PLATINUM )
					IF USE_SERVER_TRANSACTIONS()
						INT iScriptTransactionIndex
						PRINTLN("[CASH_TRANSACTION] - GIVE_EXTRA_REWARD_FOR_INT_AWARDS - Casino High Roller Platinum Award - TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION, SERVICE_EARN_CASINO_AWARD_HIGH_ROLLER_PLATINUM - ", g_sMPtunables.iVC_AWARD_CASH_HIGH_ROLLER_PLATINUM)
						TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_CASINO_AWARD_HIGH_ROLLER_PLATINUM, g_sMPtunables.iVC_AWARD_CASH_HIGH_ROLLER_PLATINUM, iScriptTransactionIndex, TRUE)
						g_cashTransactionData[iScriptTransactionIndex].cashInfo.iItemHash = HASH("HIGH_ROLLER_PLATINUM")
					ELSE
						PRINTLN("[CASH_TRANSACTION] - GIVE_EXTRA_REWARD_FOR_INT_AWARDS - Casino High Roller Platinum Award - NETWORK_EARN_CASINO_AWARD - ", g_sMPtunables.iVC_AWARD_CASH_HIGH_ROLLER_PLATINUM)
						NETWORK_EARN_CASINO_AWARD(g_sMPtunables.iVC_AWARD_CASH_HIGH_ROLLER_PLATINUM, HASH("HIGH_ROLLER_PLATINUM"))
					ENDIF
				BREAK
				
			ENDSWITCH
		BREAK
		
		
	ENDSWITCH
ENDPROC

PROC CHECK_AND_GIVE_INT_AWARD(MP_INT_AWARD anaward, int iteam)
	IF CAN_DRAW_UNLOCKS_SCALEFORM()
	    IF NOT IS_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(anAward) 
	        IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), GET_AWARD_INTCHAR_LEVEL_NUMBER(anAward, AWARDPOSITIONS_BRONZE, iTeam)) //10 Seconds
	            SET_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(anAward, TRUE, iTeam)
	            PRINTLN("CHECK_AND_GIVE_INT_AWARD  anAward = ", enum_to_int(anAward))
				
				IF anaward = MP_AWARD_ODD_JOBS
					GIVE_EXTRA_REWARD_FOR_INT_AWARDS(anaward, AWARDPOSITIONS_BRONZE)
				ENDIF
	        ENDIF
	    ELSE
	        IF NOT IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), GET_AWARD_INTCHAR_LEVEL_NUMBER(anAward, AWARDPOSITIONS_BRONZE, iTeam)) //10 Seconds
	            SET_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(anAward, FALSE, iTeam)
			    PRINTLN("CHECK_AND_GIVE_INT_AWARD  anAward = ", enum_to_int(anAward))

	        ENDIF
	    ENDIF
	    
	    IF NOT IS_MP_AWARD_SILVER_INTCHAR_UNLOCKED(anAward) 
	        IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), GET_AWARD_INTCHAR_LEVEL_NUMBER(anAward, AWARDPOSITIONS_SILVER, iTeam)) //10 Minutes
	            SET_MP_AWARD_SILVER_INTCHAR_UNLOCKED(anAward, TRUE, iTeam)
	       		PRINTLN("CHECK_AND_GIVE_INT_AWARD  anAward = ", enum_to_int(anAward))
				IF anaward = MP_AWARD_ODD_JOBS
					GIVE_EXTRA_REWARD_FOR_INT_AWARDS(anaward, AWARDPOSITIONS_SILVER)
				ENDIF
			ENDIF
	    ELSE
	        IF NOT IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), GET_AWARD_INTCHAR_LEVEL_NUMBER(anAward, AWARDPOSITIONS_SILVER, iTeam)) //10 Minutes
	            SET_MP_AWARD_SILVER_INTCHAR_UNLOCKED(anAward, FALSE, iTeam)
				 PRINTLN("CHECK_AND_GIVE_INT_AWARD  anAward = ", enum_to_int(anAward))
	        ENDIF
	    ENDIF
	    
	    IF NOT IS_MP_AWARD_GOLD_INTCHAR_UNLOCKED(anAward) 
	        IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), GET_AWARD_INTCHAR_LEVEL_NUMBER(anAward, AWARDPOSITIONS_GOLD, iTeam)) //30 Minutes
	            SET_MP_AWARD_GOLD_INTCHAR_UNLOCKED(anAward, TRUE, iTeam)
	        	PRINTLN("CHECK_AND_GIVE_INT_AWARD  anAward = ", enum_to_int(anAward))
				IF anaward = MP_AWARD_ODD_JOBS
					GIVE_EXTRA_REWARD_FOR_INT_AWARDS(anaward, AWARDPOSITIONS_GOLD)
				ENDIF
			ENDIF
	    ELSE
	        IF NOT IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), GET_AWARD_INTCHAR_LEVEL_NUMBER(anAward, AWARDPOSITIONS_GOLD, iTeam)) //30 Minutes
	            SET_MP_AWARD_GOLD_INTCHAR_UNLOCKED(anAward, FALSE, iTeam)
				 PRINTLN("CHECK_AND_GIVE_INT_AWARD  anAward = ", enum_to_int(anAward))
	        ENDIF
	    ENDIF
	    
	    IF NOT IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(anAward) 
	        IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), GET_AWARD_INTCHAR_LEVEL_NUMBER(anAward, AWARDPOSITIONS_PLATINUM, iTeam)) //1 Hour
	            SET_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(anAward, TRUE, iTeam)
	        	PRINTLN("CHECK_AND_GIVE_INT_AWARD  anAward = ", enum_to_int(anAward))
				IF anaward = MP_AWARD_ODD_JOBS
					GIVE_EXTRA_REWARD_FOR_INT_AWARDS(anaward, AWARDPOSITIONS_PLATINUM)
				ENDIF
			ENDIF
	    ELSE
	        IF NOT IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(anAward), GET_AWARD_INTCHAR_LEVEL_NUMBER(anAward, AWARDPOSITIONS_PLATINUM, iTeam)) //1 Hour
	             SET_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(anAward, FALSE, iTeam)
		     	 PRINTLN("CHECK_AND_GIVE_INT_AWARD  anAward = ", enum_to_int(anAward))
	        ENDIF
	    ENDIF
	ENDIF
ENDPROC


PROC CHECK_AND_GIVE_INTPLY_AWARD(MPPLY_INT_AWARD_INDEX anaward, int iteam)
	IF CAN_DRAW_UNLOCKS_SCALEFORM()
    IF NOT IS_MP_AWARD_BRONZE_PLYINT_UNLOCKED (anAward) 
        IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_PLAYER_AWARD(anAward), GET_AWARD_INTPLY_LEVEL_NUMBER (anAward, AWARDPOSITIONS_BRONZE, iTeam)) //10 Seconds
            SET_MP_AWARD_BRONZE_PLYINT_UNLOCKED(anAward, TRUE, iTeam)
        
        ENDIF
    ELSE
        IF NOT IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_PLAYER_AWARD(anAward), GET_AWARD_INTPLY_LEVEL_NUMBER(anAward, AWARDPOSITIONS_BRONZE, iTeam)) //10 Seconds
            SET_MP_AWARD_BRONZE_PLYINT_UNLOCKED(anAward, FALSE, iTeam)
        ENDIF
    ENDIF
    
    IF NOT IS_MP_AWARD_SILVER_PLYINT_UNLOCKED(anAward) 
        IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_PLAYER_AWARD(anAward), GET_AWARD_INTPLY_LEVEL_NUMBER(anAward, AWARDPOSITIONS_SILVER, iTeam)) //10 Minutes
            SET_MP_AWARD_SILVER_PLYINT_UNLOCKED(anAward, TRUE, iTeam)
        ENDIF
    ELSE
        IF NOT IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_PLAYER_AWARD(anAward), GET_AWARD_INTPLY_LEVEL_NUMBER(anAward, AWARDPOSITIONS_SILVER, iTeam)) //10 Minutes
            SET_MP_AWARD_SILVER_PLYINT_UNLOCKED(anAward, FALSE, iTeam)
        ENDIF
    ENDIF
    
    IF NOT IS_MP_AWARD_GOLD_PLYINT_UNLOCKED(anAward) 
        IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_PLAYER_AWARD(anAward), GET_AWARD_INTPLY_LEVEL_NUMBER(anAward, AWARDPOSITIONS_GOLD, iTeam)) //30 Minutes
            SET_MP_AWARD_GOLD_PLYINT_UNLOCKED(anAward, TRUE, iTeam)
        ENDIF
    ELSE
        IF NOT IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_PLAYER_AWARD(anAward), GET_AWARD_INTPLY_LEVEL_NUMBER(anAward, AWARDPOSITIONS_GOLD, iTeam)) //30 Minutes
            SET_MP_AWARD_GOLD_PLYINT_UNLOCKED(anAward, FALSE, iTeam)
        ENDIF
    ENDIF
    
    IF NOT IS_MP_AWARD_PLATINUM_PLYINT_UNLOCKED(anAward) 
        IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_PLAYER_AWARD(anAward), GET_AWARD_INTPLY_LEVEL_NUMBER(anAward, AWARDPOSITIONS_PLATINUM, iTeam)) //1 Hour
            SET_MP_AWARD_PLATINUM_PLYINT_UNLOCKED(anAward, TRUE, iTeam)
        ENDIF
    ELSE
        IF NOT IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_PLAYER_AWARD(anAward), GET_AWARD_INTPLY_LEVEL_NUMBER(anAward, AWARDPOSITIONS_PLATINUM, iTeam)) //1 Hour
            SET_MP_AWARD_PLATINUM_PLYINT_UNLOCKED(anAward, FALSE, iTeam)
        ENDIF
    ENDIF
	ENDIF
ENDPROC

PROC TRACK_HAS_PLAYER_BEEN_IN_ALL_POLICE_VEHICLES()

    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
        IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
            VEHICLE_INDEX players_car = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
            
            IF IS_VEHICLE_MODEL(players_car, POLICEb)
                MPGlobalsStats.award_been_in_all_cop_vehicles_POLICEb = TRUE
            ELIF IS_VEHICLE_MODEL(players_car, POLICE3)
                MPGlobalsStats.award_been_in_all_cop_vehicles_POLICE3 = TRUE
            ELIF IS_VEHICLE_MODEL(players_car, POLICEt)
                MPGlobalsStats.award_been_in_all_cop_vehicles_POLICEt = TRUE
            ELIF IS_VEHICLE_MODEL(players_car, POLMAV)
                MPGlobalsStats.award_been_in_all_cop_vehicles_POLMAV = TRUE
            ENDIF
        ENDIF


        IF  MPGlobalsStats.award_been_in_all_cop_vehicles_POLICE3 = TRUE
        AND MPGlobalsStats.award_been_in_all_cop_vehicles_POLICEt = TRUE
        AND MPGlobalsStats.award_been_in_all_cop_vehicles_POLICEb = TRUE
        AND MPGlobalsStats.award_been_in_all_cop_vehicles_POLMAV = TRUE
        
            
        ENDIF
    ENDIF
ENDPROC




// Tracking the players wanted level stat. Will need to track average wanted level time, longest wanted level time, highest wanted level escape, average wanted level escape
PROC TRACK_WANTED_LEVEL_STATS(MP_INT_STATS total_time_with_wanted_level, MP_INT_STATS no_times_wanted_by_police_stat, MP_INT_STATS average_wanted_time_stat,  INT TimeDelay )
    //INT TimeDelay_int = CONVERT_INT_MILLISECONDS_TO_MINUTES(TimeDelay)
    
    INT players_total_time_with_wanted_level_stat = GET_MP_INT_CHARACTER_STAT(total_time_with_wanted_level)
    INT players_no_times_wanted_by_police_stat = GET_MP_INT_CHARACTER_STAT(no_times_wanted_by_police_stat)
    // tracking the players total time with a wanted level
    INT players_current_wanted_level = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())

    
        IF players_current_wanted_level > 0
        AND NOT IS_PLAYER_DEAD(PLAYER_ID())
            IF bdoes_player_have_wanted_level = FALSE
                // START OF A WANTED LEVEL SESSION
                bdoes_player_have_wanted_level = TRUE
            
                //MPGlobalsStats.has_respond_time_been_passed_ARRESTWITHIN2MINS[PARTICIPANT_ID_TO_INT()] = TRUE
            //  players_no_times_wanted_by_police_stat ++ //adding number of times player getss a wanted level
                SET_MP_INT_CHARACTER_STAT(MP_STAT_TIME_LAST_WANTED_LEVEL, 0) 
            ENDIF
            

            INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_WANTED_LEVEL_TIME, TimeDelay)
            
                

                
            // increment total wanted level time stat
            //INCREMENT_BY_MP_INT_CHARACTER_STAT(total_time_with_wanted_level,TimeDelay ) // tracked in net_crime_cnc
    
            IF players_current_wanted_level = 5
                
                INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_WANTED_LEVEL_TIME5STAR, TimeDelay) 

            ENDIF 
            INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIME_LAST_WANTED_LEVEL, TimeDelay) 

            
            // storing current wanted level session
            MPGlobalsStats.i_time_of_current_wanted_level_session+=TimeDelay
            
            

            
            
            
            
            IF players_current_wanted_level > GET_MP_INT_CHARACTER_STAT(MP_STAT_HIGHEST_WANTD_LEVEL_ESCAPE)
                players_recorded_wanted_level_this_seasson_highest = players_current_wanted_level
            ENDIF
            
            IF players_current_wanted_level > players_recorded_wanted_level_this_seasson
                players_recorded_wanted_level_this_seasson = players_current_wanted_level
            ENDIF
            
            IF players_current_wanted_level = 5
                players_wanted_level_at_5_stars = TRUE
            ENDIF
        ELSE
            IF bdoes_player_have_wanted_level = TRUE
                // store highest wanted level here.
                
                IF players_recorded_wanted_level_this_seasson_highest > GET_MP_INT_CHARACTER_STAT(MP_STAT_HIGHEST_WANTD_LEVEL_ESCAPE)
                    //players_recorded_wanted_level_this_seasson_highest = players_current_wanted_level
                    SET_MP_INT_CHARACTER_STAT(MP_STAT_HIGHEST_WANTD_LEVEL_ESCAPE,players_recorded_wanted_level_this_seasson_highest )
                ENDIF
                
                // update and store total accumulated wanted level starts
                
    
                // store average wanted level time per escape
                
                MPGlobalsStats.total_accumulated_wanted_level_stars += players_recorded_wanted_level_this_seasson       
                INT average_wanted_level_escape = MPGlobalsStats.total_accumulated_wanted_level_stars / players_no_times_wanted_by_police_stat

                    SET_MP_INT_CHARACTER_STAT(MP_STAT_AVERAGE_WANTD_LEVEL_ESCAPE,average_wanted_level_escape)
        
                
                
                
                    IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
    
                        IF players_wanted_level_at_5_stars = TRUE
                            IF MPGlobalsStats.bwantedlevellostphonecall = FALSE
								INT iSlot = GET_ACTIVE_CHARACTER_SLOT()

	                            IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) // added for bug 1200442
                                    VEHICLE_INDEX temp_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
                                    IF NOT GET_IS_VEHICLE_SPRAYED(temp_vehicle)
										IF 	 IS_TRANSITION_SESSIONS_SETTING_UP_QUICKMATCH()
										OR   MPGlobalsAmbience.bBlockWantedLevelRP
										OR  g_sJoblistWarpMP.jlwIsActive
										OR  IS_TRANSITION_ACTIVE()
										OR  g_bMissionRemovedWanted = TRUE
										OR IS_BIT_SET(GlobalplayerBD_FM[iSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)
										//OR IS_HUD_DOING_A_TRANSITION()
											PRINTLN("1 TRACK_WANTED_LEVEL_STATS: MP_AWARD_5STAR_WANTED_AVOIDANCE not incremented because player is using Quick job" )
										
											IF   MPGlobalsAmbience.bBlockWantedLevelRP
												PRINTLN("1 TRACK_WANTED_LEVEL_STATS: MP_AWARD_5STAR_WANTED_AVOIDANCE not incremented because player is using Quick job 1" )
											ENDIF
											IF  g_sJoblistWarpMP.jlwIsActive
												PRINTLN("1 TRACK_WANTED_LEVEL_STATS: MP_AWARD_5STAR_WANTED_AVOIDANCE not incremented because player is using Quick job 2" )
											ENDIF
											IF  IS_TRANSITION_ACTIVE()
												PRINTLN("1 TRACK_WANTED_LEVEL_STATS: MP_AWARD_5STAR_WANTED_AVOIDANCE not incremented because player is using Quick job 3" )
											ENDIF
											IF  g_bMissionRemovedWanted = TRUE
												PRINTLN("1 TRACK_WANTED_LEVEL_STATS: MP_AWARD_5STAR_WANTED_AVOIDANCE not incremented because player is using Quick job 4" )
											ENDIF
											IF IS_BIT_SET(GlobalplayerBD_FM[iSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)
												PRINTLN("1 TRACK_WANTED_LEVEL_STATS: MP_AWARD_5STAR_WANTED_AVOIDANCE not incremented because player is using Quick job 5" )
											ENDIF
										//	IF IS_HUD_DOING_A_TRANSITION()
											//	PRINTLN("1 TRACK_WANTED_LEVEL_STATS: MP_AWARD_5STAR_WANTED_AVOIDANCE not incremented because player is using Quick job 6" )
											//ENDIF
										
										
										ELSE
                                       		 INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_5STAR_WANTED_AVOIDANCE )
										
											
										ENDIF
                                    ENDIF
                                ELSE
                                   		IF 	 IS_TRANSITION_SESSIONS_SETTING_UP_QUICKMATCH()
										OR   MPGlobalsAmbience.bBlockWantedLevelRP
										OR  g_sJoblistWarpMP.jlwIsActive
										OR  IS_TRANSITION_ACTIVE()
										OR  g_bMissionRemovedWanted = TRUE
										OR IS_BIT_SET(GlobalplayerBD_FM[iSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)
										//OR IS_HUD_DOING_A_TRANSITION()
											PRINTLN("TRACK_WANTED_LEVEL_STATS: MP_AWARD_5STAR_WANTED_AVOIDANCE not incremented because player is using Quick job" )
											IF   MPGlobalsAmbience.bBlockWantedLevelRP
												PRINTLN("2 TRACK_WANTED_LEVEL_STATS: MP_AWARD_5STAR_WANTED_AVOIDANCE not incremented because player is using Quick job 1" )
											ENDIF
											IF  g_sJoblistWarpMP.jlwIsActive
												PRINTLN("2 TRACK_WANTED_LEVEL_STATS: MP_AWARD_5STAR_WANTED_AVOIDANCE not incremented because player is using Quick job 2" )
											ENDIF
											IF  IS_TRANSITION_ACTIVE()
												PRINTLN("2 TRACK_WANTED_LEVEL_STATS: MP_AWARD_5STAR_WANTED_AVOIDANCE not incremented because player is using Quick job 3" )
											ENDIF
											IF  g_bMissionRemovedWanted = TRUE
												PRINTLN("2 TRACK_WANTED_LEVEL_STATS: MP_AWARD_5STAR_WANTED_AVOIDANCE not incremented because player is using Quick job 4" )
											ENDIF
											IF IS_BIT_SET(GlobalplayerBD_FM[iSlot].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bQuitJob)
												PRINTLN("2 TRACK_WANTED_LEVEL_STATS: MP_AWARD_5STAR_WANTED_AVOIDANCE not incremented because player is using Quick job 5" )
											ENDIF
					
										
										ELSE
                                       		 INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_5STAR_WANTED_AVOIDANCE )
										
											
										ENDIF
                                
                                ENDIF
                            ELSE
                                #IF IS_DEBUG_BUILD
                                    NET_PRINT("TRACK_WANTED_LEVEL_STATS MP_AWARD_5STAR_WANTED_AVOIDANCE not incremented because player called help to lose wanted")
                                #ENDIF
                                 
                            ENDIF
                        ENDIF
                    ENDIF
                
                IF IS_PLAYER_DEAD(PLAYER_ID())
                OR IS_HUD_DOING_A_TRANSITION()
                    MPGlobalsStats.i_time_of_current_wanted_level_session = 0
                    bdoes_player_have_wanted_level = FALSE
                    players_wanted_level_at_5_stars = FALSE
                ENDIF
                
                
            ENDIF
            MPGlobalsStats.i_time_of_current_wanted_level_session = 0
            bdoes_player_have_wanted_level = FALSE
            players_wanted_level_at_5_stars = FALSE
            g_bIwasDeliveredWhileInCustodyNoLoseWantedAward = FALSE
            MPGlobalsStats.bwantedlevellostphonecall = FALSE
        ENDIF
        
        
        // calculate average wanted time
        INT players_average_wanted_time_value = GET_MP_INT_CHARACTER_STAT(average_wanted_time_stat)
        players_average_wanted_time_value = (players_total_time_with_wanted_level_stat/players_no_times_wanted_by_police_stat)
        
        
        // store stats
        
    //  SET_MP_INT_CHARACTER_STAT(no_times_wanted_by_police_stat, players_no_times_wanted_by_police_stat)   //store no times wanted by cops
        IF players_no_times_wanted_by_police_stat >0.0
            SET_MP_INT_CHARACTER_STAT(average_wanted_time_stat, players_average_wanted_time_value)  // store average wanted level time
        ENDIF
        
        
        // store number of crooks arrested 
        
        
ENDPROC

// Tracking which team player is in (in seconds)
PROC MP_FLOAT_AWARDS_CONVERT_TO_HOURS(MP_FLOAT_AWARD a_float_award)
    
    FLOAT award_value =GET_MP_FLOAT_CHARACTER_AWARD(a_float_award)
    
    /*  
    FUNC FLOAT CONVERT_MILLISECONDS_TO_HOURS(INT Milliseconds)
        
        FLOAT x = TO_FLOAT(Milliseconds)/1000.0
        x /= 60
        x /= 60
        FLOAT hours = x % 24
        
        RETURN Hours

    ENDFUNC
    
    */
    
    award_value /= 1000.0 //seconds
    award_value /= 60.0     // minutes
    award_value /= 60.0     //hours
//  FLOAT hours = award_value % 24.0
    
    SET_MP_FLOAT_CHARACTER_AWARD(a_float_award,  award_value)
ENDPROC

// Tracking which team player is in (in seconds)
PROC MP_INT_AWARDS_CONVERT_TO_HOURS(MP_INT_AWARD an_int_award)
    
    int HoursAddition = GET_MP_INT_CHARACTER_AWARD(an_int_award) 
    
    CONVERT_MILLISECONDS_TO_HOURS(HoursAddition)
    SET_MP_INT_CHARACTER_AWARD(an_int_award,  HoursAddition)
ENDPROC







PROC RUN_PERFERRED_CREW_UPDATE()

    INT I, J
    INT Current_Longest_Time_in_Crew 
    INT WinningCrewSlot
    FOR I = 0 TO MAX_NUM_CHARACTER_SLOTS-1
        Current_Longest_Time_in_Crew = -1
        WinningCrewSlot = -1
        FOR J = 0 TO MAX_NUM_CREWS_SAVED-1
            IF GET_CREW_ID_FROM_SLOT(J) > 0
                IF Current_Longest_Time_in_Crew < GET_CHARACTER_CREW_TIME_STAT_VALUE(J , I)
                    Current_Longest_Time_in_Crew = GET_CHARACTER_CREW_TIME_STAT_VALUE(J , I)
                    WinningCrewSlot = J
                ENDIF
            ENDIF
        ENDFOR
        IF WinningCrewSlot > -1
            IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_PREFERRED_CREW) != GET_CREW_ID_FROM_SLOT(WinningCrewSlot)
                SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_PREFERRED_CREW, GET_CREW_ID_FROM_SLOT(WinningCrewSlot), I)
            ENDIF
        ENDIF
    ENDFOR  
    
ENDPROC


PROC TRACK_PLAYERS_TIME_IN_GAME_MODES(MPPLY_INT_STATS time_mission_creator_gamemode_stat, MPPLY_INT_STATS time_deathmatch_gamemode_stat,MPPLY_INT_STATS time_freemode_gamemode_stat, MPPLY_INT_STATS time_races_gamemode_stat, int delay_for_checking)
    
    
    //INT minutesAddition = CONVERT_INT_MILLISECONDS_TO_MINUTES(delay_for_checking)
    GAMER_HANDLE player_handle 
    player_handle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())


            SWITCH GET_CURRENT_GAMEMODE()
                CASE GAMEMODE_CREATOR
                    INCREMENT_BY_MP_INT_PLAYER_STAT(time_mission_creator_gamemode_stat, delay_for_checking)
                BREAK
                CASE GAMEMODE_FM
                    INCREMENT_BY_MP_INT_PLAYER_STAT(time_freemode_gamemode_stat, delay_for_checking)
                BREAK
                
            ENDSWITCH
            
            IF IS_ON_DEATHMATCH_GLOBAL_SET()
                INCREMENT_BY_MP_INT_PLAYER_STAT(time_deathmatch_gamemode_stat, delay_for_checking)
            ENDIF
            
            IF IS_ON_RACE_GLOBAL_SET()
            OR IS_ON_BASEJUMP_GLOBAL_SET()
                INCREMENT_BY_MP_INT_PLAYER_STAT(time_races_gamemode_stat, delay_for_checking)
            ENDIF

            
            // track if player is in a crew
            IF  NETWORK_CLAN_SERVICE_IS_VALID()
                IF NETWORK_CLAN_PLAYER_IS_ACTIVE(player_handle)

                    INCREMENT_BY_MP_INT_PLAYER_STAT(GET_PLAYER_CREW_TIME_STAT_FROM_PLAYER_ID(PLAYER_ID()),delay_for_checking)
                    INCREMENT_BY_MP_INT_CHARACTER_STAT(GET_CHARACTER_CREW_TIME_STAT_FROM_PLAYER_ID(PLAYER_ID()),delay_for_checking)
                ENDIF
            ENDIF
                
                
            
            INT mission_creation_totals
            mission_creation_totals += GET_MP_INT_PLAYER_AWARD(MPPLY_AWD_FM_CR_RACES_MADE_INDEX)
       		mission_creation_totals += GET_MP_INT_PLAYER_STAT(MPPLY_NUM_CAPTURES_CREATED )
            mission_creation_totals += GET_MP_INT_PLAYER_AWARD(MPPLY_AWD_FM_CR_DM_MADE_INDEX)
			mission_creation_totals += GET_MP_INT_PLAYER_STAT(MPPLY_LTS_CREATED)

            IF GET_MP_INT_PLAYER_STAT(MPPLY_MISSIONS_CREATED) != mission_creation_totals
                SET_MP_INT_PLAYER_STAT(MPPLY_MISSIONS_CREATED, mission_creation_totals)
            ENDIF
        
    
ENDPROC




PROC TRACK_RACE_WIN_PERCENTAGE(MP_INT_STATS races_won_stat, MP_INT_STATS races_started_stat,MP_FLOAT_STATS percentage_of_wins_stat )
    INT races_won_value = GET_MP_INT_CHARACTER_STAT(races_won_stat) 
    
//  INT races_lost_value = GET_MP_INT_CHARACTER_STAT(races_lost_stat)
    INT races_total_value = GET_MP_INT_CHARACTER_STAT(races_started_stat) 
    
    /*
    IF races_total_value >= 1
        races_total_value -= 1 //have to deduct 1 for the tutorial
    ENDIF
    */
    
    FLOAT race_percentage = TO_FLOAT(races_won_value) / TO_FLOAT(races_total_value) 
    race_percentage = race_percentage *100.0
    IF GET_MP_FLOAT_CHARACTER_STAT(percentage_of_wins_stat) != race_percentage
        SET_MP_FLOAT_CHARACTER_STAT(percentage_of_wins_stat, race_percentage)
    ENDIF
    //SET_MP_INT_CHARACTER_AWARD(MP_AWARD_RACES_WON,races_won_value )
    //MP_STAT_RACES_WON,
    //MP_STAT_RACES_LOST,
    //MP_STAT_RACES_STARTED
    
ENDPROC



PROC STORE_STAT_VALUE_INTO_AWARD_VALUE(MP_INT_STATS stat, MP_INT_AWARD award )
    INT stat_value = GET_MP_INT_CHARACTER_STAT(stat)
    INT award_value = stat_value
    
    IF GET_MP_INT_CHARACTER_AWARD(award)!= award_value
        SET_MP_INT_CHARACTER_AWARD(award, award_value)
    ENDIF
ENDPROC


PROC CHECK_IF_PLAYER_HAS_PERFORMED_UNIQUE_STUNT_JUMP()
    IF MPGlobalsStats.b_able_to_check_number_of_unique_stunt_jumps_performed = TRUE
        IF GET_NUM_SUCCESSFUL_STUNT_JUMPS() > MPGlobalsStats.number_of_unique_stunt_jumps_performed

            MPGlobalsStats.number_of_unique_stunt_jumps_performed = GET_NUM_SUCCESSFUL_STUNT_JUMPS()
            
            //GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD,PLAYER_PED_ID() )
            GIVE_LOCAL_PLAYER_XP_WITH_ANIMATION(eXPTYPE_STANDARD, PLAYER_PED_ID(), "REWXPUSJ_USJ", XPTYPE_ACTION, XPCATEGORY_COMPLETED_UNIQUE_STUNT_JUMP,  ROUND(100.0*g_sMPTunables.fxp_tunable_Stunt_Jumps), 1)
            //GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXPUSJ_USJ", XPTYPE_COMPLETE, XPCATEGORY_COMPLETED_UNIQUE_STUNT_JUMP, 100, 1)
           // GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP2ND_PARA",XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_RACE_TAKING_PART, irewardxp, 1) // XP reward 
            NET_PRINT("\n CHECK_IF_PLAYER_HAS_PERFORMED_UNIQUE_STUNT_JUMP: Player has just performed a stunt jump. VALUE = ")
            NET_PRINT_INT(MPGlobalsStats.number_of_unique_stunt_jumps_performed)
			
			INT PercentageProgress = MPGlobalsStats.number_of_unique_stunt_jumps_performed / 50
			PercentageProgress = PercentageProgress*100
            REQUEST_SYSTEM_ACTIVITY_TYPE_DONE_STUNT_JUMP( PercentageProgress)
        ENDIF
    ELSE
        IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
            IF IS_PLAYER_CONTROL_ON(Player_id())
                MPGlobalsStats.number_of_unique_stunt_jumps_performed  = GET_NUM_SUCCESSFUL_STUNT_JUMPS()
                NET_PRINT("\n CHECK_IF_PLAYER_HAS_PERFORMED_UNIQUE_STUNT_JUMP: INITIAL VALUE = ")
                NET_PRINT_INT(MPGlobalsStats.number_of_unique_stunt_jumps_performed)
                MPGlobalsStats.b_able_to_check_number_of_unique_stunt_jumps_performed = TRUE
            ENDIF

        ENDIF
        
    ENDIF
ENDPROC


//PROC TRACK_STAT_TIME_AS_BAD_COP(INT DelayTime)

//  IF Is_Player_A_Bad_Cop(PLAYER_ID())
//      //INT minutesAddition = CONVERT_MILLISECONDS_TO_MINUTES(DelayTime)
//  
//      //SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_TIME_AS_A_BADCOP,minutesAddition) 
//      INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIME_AS_A_BADCOP,DelayTime) 
//  ENDIF

//ENDPROC

PROC TRACK_AVERAGE_XP_ON_MISSION()

    //SET_MP_INT_CHARACTER_STAT(MP_STAT_AVERAGE_XP_ON_MISSION, (g_MP_STAT_CHAR_XP_FM[GET_SLOT_NUMBER(-1)] / GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MISSION_OVER)) )
    SET_MP_INT_CHARACTER_STAT(MP_STAT_AVERAGE_XP_ON_MISSION, (g_MP_STAT_CHAR_XP_FM[GET_SLOT_NUMBER(-1)] / GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MISSION_OVER)) )

ENDPROC



PROC TRACK_AVERAGE_XP_GAINED_PER_MINUTE()
    INT time_min = GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_PLAYING_TIME)
    FLOAT time_min_float = CONVERT_MILLISECONDS_TO_MINUTES (time_min)
    int average_xp
    IF time_min_float >1.0 // if time is greater than a minute
         average_xp =   GET_MP_INT_CHARACTER_STAT(MP_STAT_MISS_XP_EARNED)/round(time_min_float)
    ELSE
         average_xp = 0
    ENDIF
    
    SET_MP_INT_CHARACTER_STAT(MP_STAT_AVERAGE_EX_GAIN_PER_MINUTE,average_xp )
ENDPROC

PROC TRACK_RANK_GAIN_PER_HOUR()
    INT Rank = GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_XP_EARNED)  //GET_PLAYER_CnC_RANK(Player_ID()) //GET_CNC_RANK_FROM_XP_VALUE(GET_STAT_CHARACTER_XP())
    INT time_hours 
    FLOAT average_rank_gain 


        
    time_hours = GET_TOTAL_NUMBER_OF_MINUTES_FOR_MP_UNSIGNED_INT_STAT(MP_STAT_TOTAL_PLAYING_TIME)
    
    

    
    

    IF time_hours>1
        FLOAT average_gain
        average_gain= TO_FLOAT(rank) / average_rank_gain
        
        IF GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_AVERAGE_RANK_GAIN_PER_HOUR) !=average_gain
            SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_AVERAGE_RANK_GAIN_PER_HOUR,average_gain )
        ENDIF
    ENDIF
    /*
    #IF IS_DEBUG_BUILD
        PRINTLN("\n RANK: ", Rank)
        PRINTLN("\n TIME IN HOURS:  ", time_hours)
        PRINTLN("\n AVERAGE RANK PER HOURS:  ", average_rank_gain)

    #ENDIF
    
    */
     
    
    IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_REACH_RANK_1) = FALSE
        IF GET_MP_INT_CHARACTER_STAT(MP_STAT_KILLS_PLAYERS) >= 250
            SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_REACH_RANK_1, TRUE)
        ENDIF
    ENDIF
    
    IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_REACH_RANK_2) = FALSE
        IF GET_MP_INT_CHARACTER_STAT(MP_STAT_KILLS_PLAYERS) >= 500
            SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_REACH_RANK_2, TRUE)
        ENDIF
    ENDIF
    
    IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_REACH_RANK_3) = FALSE
        IF GET_MP_INT_CHARACTER_STAT(MP_STAT_KILLS_PLAYERS) >= 1000
            SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_REACH_RANK_3, TRUE)
        ENDIF
    ENDIF

    
    
ENDPROC


FUNC INT GET_NUMBER_OF_DAYS_LAST_PLAYED()

    FLOAT number_of_days_last_played = (GET_CLOUD_TIME_AS_INT() - g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.ilasttimeplayed) / TO_FLOAT(86400)
                        
    INT number_of_days_last_played_int = ROUND(number_of_days_last_played)
    RETURN number_of_days_last_played_int           

ENDFUNC


FUNC INT GET_ABILITY_STAT_DIMINISH_AMOUNT(MP_INT_STATS stat, INT number_of_days_last_played)
    
    FLOAT AMOUNT_TO_REMOVE = TO_FLOAT(GET_MP_INT_CHARACTER_STAT(stat)) 
    AMOUNT_TO_REMOVE = AMOUNT_TO_REMOVE / 100.0 
    AMOUNT_TO_REMOVE = AMOUNT_TO_REMOVE * 2.0 // 2 percent to be removed
    AMOUNT_TO_REMOVE = AMOUNT_TO_REMOVE * number_of_days_last_played
    
    INT RETURN_AMOUNT = ROUND(AMOUNT_TO_REMOVE)     

    IF (GET_MP_INT_CHARACTER_STAT(stat) -AMOUNT_TO_REMOVE)  <=0
        RETURN_AMOUNT = 0
        SET_MP_INT_CHARACTER_STAT(stat, 0) 
        #IF IS_DEBUG_BUILD
            NET_PRINT("[KW@DIMINISHING ABILITY] GET_ABILITY_STAT_DIMINISH_AMOUNT Stat is zero.")
            NET_PRINT_INT(GET_NUMBER_OF_DAYS_LAST_PLAYED())
        #ENDIF
                            
    ENDIF
    
    
    RETURN RETURN_AMOUNT            

ENDFUNC

	PROC GOON_BENEFITS_DRIVING_WITH_BOSS()
	    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	        IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	            VEHICLE_INDEX temp_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			// 2490828 Goon Benefits for the apartments pack
				IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID(), TRUE) 
					IF NOT GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID())	
						PLAYER_INDEX leaderofgang = GB_GET_LOCAL_PLAYER_GANG_BOSS()
						PED_INDEX leaderofgangped =  GET_PLAYER_PED(leaderofgang) 
						IF IS_ENTITY_A_PED(leaderofgangped) // 1415208
							PRINTLN("GOON_BENEFITS_DRIVING_WITH_BOSS giving RP")
							IF IS_PED_IN_VEHICLE(leaderofgangped, temp_vehicle)
								IF GET_PED_IN_VEHICLE_SEAT(temp_vehicle, VS_DRIVER) = PLAYER_PED_ID()	
								// reward player for driving the boss.
								//g_sMPTunables.irprewardfordrivingbossaround
								  // GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "REWXP_1", XPTYPE_COMPLETE, XPCATEGORY_COMPLETE_DAILY_RACEDMMISSDONE,ROUND(rp_award_amount *g_sMPTunables.fxp_tunable_Daily_Deathmatch), 1)
								ELSE
								// reward player for riding with boss.
								//g_sMPTunables.irprewardfordrivingwithboss
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
 	 ENDPROC

PROC TRACK_PLAYER_IN_CAR_STATS(MP_INT_STATS time_spent_as_driver_Stat, MP_INT_STATS  time_spent_as_passenger_stat, MP_INT_STATS  total_time_spent_in_car_stat, int delay_time_check)

    //INT time_spent_as_driver_value = GET_MP_INT_CHARACTER_STAT(time_spent_as_driver_Stat)
    //INT time_spent_as_passenger_value = GET_MP_INT_CHARACTER_STAT(time_spent_as_passenger_stat)
    //INT total_time_spent_in_car_value = GET_MP_INT_CHARACTER_STAT(total_time_spent_in_car_stat)

    //delay_time_check = delay_time_check / 1000 //convert into seconds
//  INT delay_time_float = CONVERT_INT_MILLISECONDS_TO_MINUTES(delay_time_check)
    // check if player is a driver
    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
        IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
        

            VEHICLE_INDEX temp_vehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
            
            IF GET_PED_IN_VEHICLE_SEAT(temp_vehicle, VS_DRIVER) = PLAYER_PED_ID()
    
                INCREMENT_BY_MP_INT_CHARACTER_STAT(time_spent_as_driver_Stat, delay_time_check)
            ELSE
                IF NOT IS_VEHICLE_SEAT_FREE(temp_vehicle, VS_DRIVER)
                AND  ARE_ALL_INSTANCED_TUTORIALS_COMPLETE() 
                AND IS_PED_A_PLAYER(GET_PED_IN_VEHICLE_SEAT(temp_vehicle, VS_DRIVER))
                // block here if in a taxi maybe?
                //AND NOT IS_VEHICLE_STOPPED(temp_vehicle)
                // store player as passenger
                //(MPGlobalsAmbience.iTaxiBitSet, biG_PlayerUsingTaxi)
                    INCREMENT_BY_MP_INT_CHARACTER_STAT(time_spent_as_passenger_stat, delay_time_check)
                ENDIF
            ENDIF
			
				GOON_BENEFITS_DRIVING_WITH_BOSS()
            
            // check if player is in car
            IF NOT IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
            AND NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
            AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
            AND NOT IS_PED_IN_ANY_TRAIN(PLAYER_PED_ID())
            AND NOT IS_PED_IN_ANY_SUB(PLAYER_PED_ID())
            AND NOT IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
            AND NOT IS_THIS_MODEL_A_QUADBIKE (GET_ENTITY_MODEL(temp_vehicle))
        //  get_model

                INCREMENT_BY_MP_INT_CHARACTER_STAT(total_time_spent_in_car_stat, delay_time_check)
    

            ENDIF
        
        
        ENDIF
        
         
        INT time_spent_as_passenger_value = GET_TOTAL_NUMBER_OF_HOURS_FOR_MP_UNSIGNED_INT_STAT(time_spent_as_passenger_Stat)
        
        IF MPGlobalsStats.temp_integer_for_maintain_stat_PASSENGERTIME != time_spent_as_passenger_value
            MPGlobalsStats.temp_integer_for_maintain_stat_PASSENGERTIME = GET_MP_INT_CHARACTER_AWARD(MP_AWARD_PASSENGERTIME)
            SET_MP_INT_CHARACTER_AWARD(MP_AWARD_PASSENGERTIME,time_spent_as_passenger_value )  
        ENDIF
    ENDIF
ENDPROC


PROC TRACK_PLAYER_CUFFED_TIME(MP_INT_STATS time_spent_cuffed_stat, int delay_time_check)
    //int time_spent_cuffed_value = CONVERT_INT_MILLISECONDS_TO_MINUTES(delay_time_check)

    IF IS_NET_PLAYER_OK(PLAYER_ID())
        INCREMENT_BY_MP_INT_CHARACTER_STAT(time_spent_cuffed_stat,delay_time_check )
    ENDIF

    

ENDPROC


/*

PROC TRACKING_TOTAL_KILLS(MP_INT_STATS total_KILLS_weapons_fired_stat )

    INT temp_int
    INT total_KILLS_weapons =  GET_MP_INT_CHARACTER_STAT(total_KILLS_weapons_fired_stat)

    IF ENUM_TO_INT(MP_STAT_CMBTPISTOL_KILLS)>0
        temp_int=GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTPISTOL_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_APPISTOL_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_APPISTOL_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_MICROSMG_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_MICROSMG_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_SMG_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF

    IF ENUM_TO_INT(MP_STAT_ASLTRIFLE_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTRIFLE_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_CRBNRIFLE_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_CRBNRIFLE_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF

    IF ENUM_TO_INT(MP_STAT_ADVRIFLE_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_ADVRIFLE_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_MG_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_MG_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_CMBTMG_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTMG_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF

    IF ENUM_TO_INT(MP_STAT_PUMP_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_PUMP_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_SAWNOFF_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_SAWNOFF_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_BULLPUP_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_BULLPUP_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_ASLTSHTGN_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTSHTGN_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_RUBBERGUN_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_RUBBERGUN_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_STUNGUN_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_STUNGUN_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_SNIPERRFL_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_SNIPERRFL_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF

    IF ENUM_TO_INT(MP_STAT_HVYSNIPER_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_HVYSNIPER_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_GRNLAUNCH_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_GRNLAUNCH_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_RPG_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_RPG_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_MINIGUN_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_MINIGUN_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_PRGRMMBLAR_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_PRGRMMBLAR_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_GRENADE_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    IF ENUM_TO_INT(MP_STAT_SMKGRENADE_KILLS)>0
    
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_SMKGRENADE_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF
    
    IF ENUM_TO_INT(MP_STAT_STKYBMB_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF

    
    IF ENUM_TO_INT(MP_STAT_NO_MELEE_KILLS)>0
        temp_int = GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_MELEE_KILLS)
        total_KILLS_weapons+=temp_int

    ENDIF


    
    SET_MP_INT_CHARACTER_STAT(total_KILLS_weapons_fired_stat, total_KILLS_weapons)
ENDPROC

*/

PROC TRACK_TRACK_WHEN_PLAYER_IS_ON_PHONE(MP_INT_STATS total_time_spent_on_phone_STAT, int delay_time_check)
    INT total_time_spent_on_phone_value = GET_MP_INT_CHARACTER_STAT(total_time_spent_on_phone_STAT)
    //IF IS_MOBILE_PHONE_CALL_ONGOING()
    IF IS_PHONE_ONSCREEN()
        //INT delay_time_check_float_minutes= CONVERT_INT_MILLISECONDS_TO_MINUTES(delay_time_check)
        total_time_spent_on_phone_value+=delay_time_check
        SET_MP_INT_CHARACTER_STAT(total_time_spent_on_phone_STAT, total_time_spent_on_phone_value)
    ENDIF
ENDPROC








PROC TRACK_AVERAGE_NUMBER_OF_PASSENGERS(int delaytime)
    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
        IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
        AND IS_PLAYER_DRIVING_ANY_VEHICLE()
        VEHICLE_INDEX temp_current_car = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
        
        int current_no_passengers = GET_VEHICLE_NUMBER_OF_PASSENGERS(temp_current_car)
    
            IF current_no_passengers >0
                //int time_in_sec = CONVERT_INT_MILLISECONDS_TO_SECONDS(delaytime)
                current_no_passengers = current_no_passengers * delaytime
                IF NOT IS_VEHICLE_STOPPED(temp_current_car)
                    INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIME_DRIVING_PASSENGER, delaytime)
                
                    INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_ACCUM_PASSENGER, current_no_passengers)
                ENDIF
                
                int total_time_in_car_as_driver = GET_MP_INT_CHARACTER_STAT(MP_STAT_TIME_DRIVING_PASSENGER)
                INT total_number_of_passengers_units = GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_ACCUM_PASSENGER)
                

                int average_number_passengers = total_number_of_passengers_units/total_time_in_car_as_driver 
                
                //PRINTLN("\n Total passenger:",total_number_of_passengers_units)
                //PRINTLN("\n Total time in car:",total_time_in_car_as_driver)
                //PRINTLN("\n Total passenger units :",total_number_of_passengers_units)
                
                SET_MP_INT_CHARACTER_STAT(MP_STAT_AVERAGE_NO_PASSENGERS, average_number_passengers)
                
        
            ENDIF
        
        ENDIF

    ENDIF

ENDPROC


FUNC BOOL HAS_PLAYER_DRIVEN_A_RALLY_RACE() 

    //Play a Land Race.
    INT land_races 
    land_races = GET_MP_INT_CHARACTER_AWARD(MP_AWARD_RACES_WON)
    land_races -= GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMWINAIRRACE)
    land_races -= GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMWINSEARACE)
    IF land_races > 0 
        RETURN TRUE
    ELSE
    
        RETURN FALSE
    ENDIF
    


ENDFUNC



PROC TRACK_NUM_WEAPONS_USED() // MP_STAT_ACCURACY need number of shots for all weapons, accuracy for all weapons
// if shots for weapon  is above 0, add that into the total 

    INT weapon_counter = 0
    
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_PISTOL_SHOTS)>0
        weapon_counter++
    ENDIF


    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTPISTOL_SHOTS)>0
        weapon_counter++
    ENDIF
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_APPISTOL_SHOTS)>0
        weapon_counter++
    ENDIF
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_MICROSMG_SHOTS)>0
        weapon_counter++
    ENDIF
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_SHOTS)>0
        weapon_counter++
    ENDIF

    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTRIFLE_SHOTS)>0
        weapon_counter++
    ENDIF
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CRBNRIFLE_SHOTS)>0
        weapon_counter++
    ENDIF

    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_ADVRIFLE_SHOTS)>0
        weapon_counter++
    ENDIF
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_MG_SHOTS)>0
        weapon_counter++
    ENDIF
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTMG_SHOTS)>0
        weapon_counter++
    ENDIF

    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_PUMP_SHOTS)>0
        weapon_counter++
    ENDIF
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_SAWNOFF_SHOTS)>0
        weapon_counter++
    ENDIF

    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTSHTGN_SHOTS)>0
        weapon_counter++
    ENDIF
    /*IF GET_MP_INT_CHARACTER_STAT(MP_STAT_RUBBERGUN_SHOTS)>0
        weapon_counter++
    ENDIF*/
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_STUNGUN_SHOTS)>0
        weapon_counter++
    ENDIF
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_SNIPERRFL_SHOTS)>0
        weapon_counter++
    ENDIF

    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_HVYSNIPER_SHOTS)>0
        weapon_counter++
    ENDIF

    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_MINIGUNS_SHOTS)>0
        weapon_counter++
    ENDIF

    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_SMKGRENADE_SHOTS)>0
        weapon_counter++
    ENDIF
//DLC
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_SNSPISTOL_SHOTS )>0
        weapon_counter++
    ENDIF
        
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_PISTOL50_SHOTS )>0
        weapon_counter++
    ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTMG_SHOTS )>0
        weapon_counter++
    ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_HVYPISTOL_SHOTS )>0
        weapon_counter++
    ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_VPISTOL_SHOTS )>0
        weapon_counter++
    ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_BULLPUP_SHOTS )>0
        weapon_counter++
    ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_BULLRIFLE_SHOTS )>0
        weapon_counter++
    ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_GUSNBRG_SHOTS )>0
        weapon_counter++
    ENDIF
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_SPCARBINE_SHOTS )>0
        weapon_counter++
    ENDIF
	

	
    // total accuracy = total_shots / total_hits
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_NO_WEAPONS_USED) !=weapon_counter
        SET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_NO_WEAPONS_USED, weapon_counter)
    ENDIF
    // will have to include mellees and shit
    
ENDPROC


FUNC FLOAT GET_WEAPON_ACCURACY_FROM_STATS(WEAPON_TYPE aweapontype)

    MP_INT_STATS weapon_shots_stat
    MP_INT_STATS weapon_hit_stat
    
	SWITCH aweapontype
	
	    CASE  WEAPONTYPE_PISTOL 
			weapon_shots_stat = MP_STAT_PISTOL_SHOTS
	        weapon_hit_stat = MP_STAT_PISTOL_HITS
		BREAK
		CASE  WEAPONTYPE_DLC_PISTOL_MK2
	        weapon_shots_stat = MP_STAT_PISTOL_SHOTS
	        weapon_hit_stat = MP_STAT_PISTOL_HITS
		BREAK
	    CASE   WEAPONTYPE_COMBATPISTOL
	        weapon_shots_stat = MP_STAT_CMBTPISTOL_SHOTS
	        weapon_hit_stat = MP_STAT_CMBTPISTOL_HITS
		BREAK
	    CASE   WEAPONTYPE_APPISTOL
	        weapon_shots_stat = MP_STAT_APPISTOL_SHOTS
	        weapon_hit_stat = MP_STAT_APPISTOL_HITS
		BREAK
	    CASE   WEAPONTYPE_MICROSMG
	        weapon_shots_stat = MP_STAT_MICROSMG_SHOTS
	        weapon_hit_stat = MP_STAT_MICROSMG_HITS
		BREAK
	    CASE   WEAPONTYPE_SMG 
			weapon_shots_stat = MP_STAT_SMG_SHOTS
	        weapon_hit_stat = MP_STAT_SMG_HITS
		BREAK
		CASE WEAPONTYPE_DLC_SMG_MK2
	        weapon_shots_stat = MP_STAT_SMG_SHOTS
	        weapon_hit_stat = MP_STAT_SMG_HITS
		BREAK
	    CASE WEAPONTYPE_ASSAULTRIFLE 
		CASE WEAPONTYPE_DLC_ASSAULTRIFLE_MK2
	        weapon_shots_stat = MP_STAT_ASLTRIFLE_SHOTS
	        weapon_hit_stat = MP_STAT_ASLTRIFLE_HITS
		BREAK
	    CASE   WEAPONTYPE_CARBINERIFLE 
		CASE WEAPONTYPE_DLC_CARBINERIFLE_MK2
	        weapon_shots_stat = MP_STAT_CRBNRIFLE_SHOTS
	        weapon_hit_stat = MP_STAT_CRBNRIFLE_HITS
		BREAK
	    CASE   WEAPONTYPE_DLC_SPECIALCARBINE
	        weapon_shots_stat = MP_STAT_SPCARBINE_SHOTS
	        weapon_hit_stat = MP_STAT_SPCARBINE_HITS
		BREAK
		CASE   WEAPONTYPE_DLC_SNSPISTOL
		
	        weapon_shots_stat = MP_STAT_SNSPISTOL_SHOTS
	        weapon_hit_stat = MP_STAT_SNSPISTOL_HITS
		BREAK
	    CASE   WEAPONTYPE_ADVANCEDRIFLE
	        weapon_shots_stat = MP_STAT_ADVRIFLE_SHOTS
	        weapon_hit_stat = MP_STAT_ADVRIFLE_HITS
		BREAK
	    CASE   WEAPONTYPE_MG
	        weapon_shots_stat = MP_STAT_MG_SHOTS
	        weapon_hit_stat = MP_STAT_MG_HITS 
		BREAK
	    CASE   WEAPONTYPE_COMBATMG 
		CASE WEAPONTYPE_DLC_COMBATMG_MK2
	        weapon_shots_stat = MP_STAT_CMBTMG_SHOTS
	        weapon_hit_stat = MP_STAT_CMBTMG_HITS
		BREAK
	    CASE   WEAPONTYPE_SAWNOFFSHOTGUN
	        weapon_shots_stat = MP_STAT_SAWNOFF_SHOTS
	        weapon_hit_stat = MP_STAT_SAWNOFF_HITS
		BREAK
	    CASE   WEAPONTYPE_PUMPSHOTGUN
	        weapon_shots_stat = MP_STAT_PUMP_SHOTS
	        weapon_hit_stat = MP_STAT_PUMP_HITS 
		BREAK
	    CASE   WEAPONTYPE_ASSAULTSHOTGUN
	        weapon_shots_stat = MP_STAT_ASLTSHTGN_SHOTS
	        weapon_hit_stat = MP_STAT_ASLTSHTGN_HITS
		BREAK
	    CASE   WEAPONTYPE_SNIPERRIFLE
	        weapon_shots_stat = MP_STAT_SNIPERRFL_SHOTS
	        weapon_hit_stat = MP_STAT_SNIPERRFL_HITS
		BREAK
	    CASE   WEAPONTYPE_HEAVYSNIPER 
		CASE  WEAPONTYPE_DLC_HEAVYSNIPER_MK2
	        weapon_shots_stat = MP_STAT_HVYSNIPER_SHOTS 
	        weapon_hit_stat = MP_STAT_HVYSNIPER_HITS
		BREAK
	    CASE   WEAPONTYPE_GRENADELAUNCHER
	        weapon_shots_stat = MP_STAT_GRNLAUNCH_SHOTS
	        weapon_hit_stat = MP_STAT_GRNLAUNCH_KILLS
		BREAK
	    CASE   WEAPONTYPE_MINIGUN
	        weapon_shots_stat = MP_STAT_MINIGUNS_SHOTS
	        weapon_hit_stat = MP_STAT_MINIGUNS_HITS
		BREAK
	    CASE   WEAPONTYPE_RPG
	        weapon_shots_stat = MP_STAT_RPG_SHOTS
	        weapon_hit_stat = MP_STAT_RPG_KILLS
		BREAK
		CASE   WEAPONTYPE_DLC_BULLPUPSHOTGUN
			weapon_shots_stat = MP_STAT_BULLPUP_SHOTS
			weapon_hit_stat = MP_STAT_BULLPUP_HITS
		BREAK
		CASE   WEAPONTYPE_DLC_ASSAULTSMG
			weapon_shots_stat = MP_STAT_ASLTSMG_SHOTS
			weapon_hit_stat = MP_STAT_ASLTSMG_HITS
		BREAK
		CASE   WEAPONTYPE_DLC_PISTOL50
			weapon_shots_stat = MP_STAT_PISTOL50_SHOTS
			weapon_hit_stat = MP_STAT_PISTOL50_HITS
		BREAK	
		CASE   WEAPONTYPE_DLC_HEAVYPISTOL
			weapon_shots_stat = MP_STAT_HVYPISTOL_SHOTS
			weapon_hit_stat = MP_STAT_HVYPISTOL_HITS
		BREAK	
		CASE   WEAPONTYPE_DLC_BULLPUPRIFLE

			weapon_shots_stat = MP_STAT_BULLRIFLE_SHOTS
			weapon_hit_stat = MP_STAT_BULLRIFLE_HITS
		BREAK	
					
		CASE   WEAPONTYPE_DLC_GUSENBERG
			weapon_shots_stat = MP_STAT_GUSNBRG_SHOTS
			weapon_hit_stat = MP_STAT_GUSNBRG_HITS
		BREAK	
		CASE   WEAPONTYPE_DLC_VINTAGEPISTOL
			weapon_shots_stat = MP_STAT_VPISTOL_SHOTS
			weapon_hit_stat = MP_STAT_VPISTOL_HITS
		BREAK
		CASE   WEAPONTYPE_DLC_MUSKET
			weapon_shots_stat = MP_STAT_MUSKET_SHOTS
			weapon_hit_stat = MP_STAT_MUSKET_HITS
		BREAK
		CASE   WEAPONTYPE_DLC_FIREWORK
			weapon_shots_stat = MP_STAT_FIREWRK_SHOTS
			weapon_hit_stat = MP_STAT_FIREWRK_HITS
		BREAK
		CASE   WEAPONTYPE_DLC_FLAREGUN
			weapon_shots_stat = MP_STAT_FLAREGUN_SHOTS
			weapon_hit_stat = MP_STAT_FLAREGUN_HITS
		BREAK
		CASE   WEAPONTYPE_DLC_HEAVYSHOTGUN
			weapon_shots_stat = MP_STAT_HVYSHGN_SHOTS
			weapon_hit_stat = MP_STAT_HVYSHGN_HITS
		BREAK
		CASE   WEAPONTYPE_DLC_MARKSMANRIFLE
 
			weapon_shots_stat = MP_STAT_MKRIFLE_SHOTS
			weapon_hit_stat = MP_STAT_MKRIFLE_HITS
		BREAK
		CASE   WEAPONTYPE_DLC_HOMINGLAUNCHER
			weapon_shots_stat = MP_STAT_HOMLNCH_SHOTS
			weapon_hit_stat = MP_STAT_HOMLNCH_KILLS
		BREAK
		CASE   WEAPONTYPE_DLC_PROXMINE
			weapon_shots_stat = MP_STAT_PRXMINE_SHOTS
			weapon_hit_stat = MP_STAT_PRXMINE_KILLS
		BREAK
			
		CASE   WEAPONTYPE_DLC_COMBATPDW
			weapon_shots_stat = MP_STAT_COMBATPDW_SHOTS
			weapon_hit_stat = MP_STAT_COMBATPDW_HITS
		
		BREAK
		CASE   WEAPONTYPE_DLC_MARKSMANPISTOL
			weapon_shots_stat = MP_STAT_MKPISTOL_SHOTS
			weapon_hit_stat = MP_STAT_MKPISTOL_HITS
		BREAK
		
		CASE   WEAPONTYPE_DLC_MACHINEPISTOL
			weapon_shots_stat = MP_STAT_MCHPIST_SHOTS
		 	weapon_hit_stat = MP_STAT_MCHPIST_HITS
		BREAK
		CASE   WEAPONTYPE_DLC_COMPACTRIFLE
			weapon_shots_stat = MP_STAT_CMPRIFLE_SHOTS
			weapon_hit_stat = MP_STAT_CMPRIFLE_HITS
		BREAK
		CASE   WEAPONTYPE_DLC_DBSHOTGUN 
			weapon_shots_stat = MP_STAT_DBSHGN_SHOTS
			weapon_hit_stat = MP_STAT_DBSHGN_HITS
		BREAK
		
		CASE   WEAPONTYPE_DLC_REVOLVER
			weapon_shots_stat = MP_STAT_REVOLVER_SHOTS
			weapon_hit_stat = MP_STAT_REVOLVER_HITS
		BREAK
		
		CASE   WEAPONTYPE_DLC_AUTOSHOTGUN
			weapon_shots_stat = MP_STAT_AUTOSHGN_SHOTS
			weapon_hit_stat = MP_STAT_AUTOSHGN_HITS
		BREAK
		CASE   WEAPONTYPE_DLC_MINISMG
			weapon_shots_stat = MP_STAT_MINISMG_SHOTS
			weapon_hit_stat = MP_STAT_MINISMG_HITS
		BREAK
		CASE   WEAPONTYPE_DLC_COMPACTLAUNCHER
			weapon_shots_stat = MP_STAT_CMPGL_SHOTS
			weapon_hit_stat = MP_STAT_CMPGL_KILLS
			
		BREAK

		CASE WEAPONTYPE_DLC_DOUBLEACTION
			weapon_shots_stat = MP_STAT_REV_DA_SHOTS
			weapon_hit_stat = MP_STAT_REV_DA_HITS	
    	BREAK
		
		CASE WEAPONTYPE_DLC_BULLPUPRIFLE_MK2	weapon_hit_stat = MP_STAT_BLRIFLE_MK2_KILLS  weapon_shots_stat = MP_STAT_BLRIFLE_MK2_SHOTS   BREAK									
		CASE WEAPONTYPE_DLC_MARKSMANRIFLE_MK2	weapon_hit_stat = MP_STAT_MKRIFLE_MK2_KILLS  weapon_shots_stat = MP_STAT_MKRIFLE_MK2_SHOTS   BREAK
		CASE WEAPONTYPE_DLC_PUMPSHOTGUN_MK2 	weapon_hit_stat = MP_STAT_PUMP_MK2_KILLS  	 weapon_shots_stat = MP_STAT_PUMP_MK2_SHOTS   	BREAK
		CASE WEAPONTYPE_DLC_REVOLVER_MK2 		weapon_hit_stat = MP_STAT_REV_MK2_KILLS  	 weapon_shots_stat = MP_STAT_REV_MK2_SHOTS   	BREAK
		CASE WEAPONTYPE_DLC_SNSPISTOL_MK2 		weapon_hit_stat = MP_STAT_SNSPIST_MK2_KILLS  weapon_shots_stat = MP_STAT_SNSPIST_MK2_SHOTS   BREAK
		CASE WEAPONTYPE_DLC_SPECIALCARBINE_Mk2  weapon_hit_stat = MP_STAT_SPCARB_MK2_KILLS   weapon_shots_stat = MP_STAT_SPCARB_MK2_SHOTS   BREAK
	
		CASE WEAPONTYPE_DLC_RAYCARBINE
			weapon_shots_stat = MP_STAT_RAYCARBINE_SHOTS
			weapon_hit_stat = MP_STAT_RAYCARBINE_HITS	
    	BREAK
		CASE WEAPONTYPE_DLC_RAYMINIGUN
			weapon_shots_stat = MP_STAT_RAYMINIGUN_SHOTS
			weapon_hit_stat = MP_STAT_RAYMINIGUN_HITS	
    	BREAK
		CASE WEAPONTYPE_DLC_RAYPISTOL
			weapon_shots_stat = MP_STAT_RAYPISTOL_SHOTS
			weapon_hit_stat = MP_STAT_RAYPISTOL_HITS	
    	BREAK
		
		CASE WEAPONTYPE_DLC_NAVYREVOLVER
			weapon_shots_stat = MP_STAT_REV_NV_SHOTS
			weapon_hit_stat = MP_STAT_REV_NV_HITS	
    	BREAK
		CASE WEAPONTYPE_DLC_CERAMICPISTOL
			weapon_shots_stat = MP_STAT_CERPST_SHOTS
			weapon_hit_stat = MP_STAT_CERPST_HITS	
    	BREAK
		
		#IF FEATURE_COPS_N_CROOKS
		CASE WEAPONTYPE_DLC_POLICERIFLE
			weapon_shots_stat = MP_STAT_PLCRIFLE_SHOTS
			weapon_hit_stat = MP_STAT_PLCRIFLE_HITS	
    	BREAK
		#ENDIF
		
		CASE WEAPONTYPE_DLC_COMBATSHOTGUN
			weapon_shots_stat = MP_STAT_CMBSHGN_SHOTS
			weapon_hit_stat = MP_STAT_CMBSHGN_HITS	
    	BREAK
		
		
		CASE WEAPONTYPE_DLC_GADGETPISTOL
			weapon_shots_stat = MP_STAT_GDGTPST_SHOTS
			weapon_hit_stat = MP_STAT_GDGTPST_HITS	
    	BREAK
		
		CASE WEAPONTYPE_DLC_MILITARYRIFLE
			weapon_shots_stat = MP_STAT_MLTRYRFL_SHOTS
			weapon_hit_stat = MP_STAT_MLTRYRFL_HITS	
    	BREAK
		
		CASE WEAPONTYPE_DLC_HEAVYRIFLE
			weapon_shots_stat = MP_STAT_HVYRIFLE_SHOTS
			weapon_hit_stat = MP_STAT_HVYRIFLE_HITS	
    	BREAK
		
		CASE WEAPONTYPE_DLC_EMPLAUNCHER
			weapon_shots_stat = MP_STAT_EMPGL_SHOTS
			weapon_hit_stat = MP_STAT_EMPGL_HITS	
    	BREAK
		
		CASE WEAPONTYPE_DLC_STUNGUNG_MP
			weapon_shots_stat = MP_STAT_STNGNMP_SHOTS
			weapon_hit_stat = MP_STAT_STNGNMP_HITS	
    	BREAK
		
		
		CASE WEAPONTYPE_DLC_TACTICALRIFLE
			weapon_shots_stat = MP_STAT_TACRIFLE_SHOTS
			weapon_hit_stat = MP_STAT_TACRIFLE_HITS	
    	BREAK
		
		CASE WEAPONTYPE_DLC_PRECISIONRIFLE
			weapon_shots_stat = MP_STAT_PRCSRIFLE_SHOTS
			weapon_hit_stat = MP_STAT_PRCSRIFLE_HITS	
    	BREAK
    ENDSWITCH
	
	
	IF IS_WEAPON_A_MELEE(aweapontype)
	OR aweapontype = WEAPONTYPE_DLC_FLASHLIGHT
	OR IS_WEAPON_A_THROWING( aweapontype)
		// do nothing
	ELSE
	    FLOAT fWeaponShots = TO_FLOAT(GET_MP_INT_CHARACTER_STAT(weapon_shots_stat))
	    FLOAT fWeaponHit = TO_FLOAT(GET_MP_INT_CHARACTER_STAT(weapon_hit_stat))
	    FLOAT fPercent
	    IF fWeaponShots > 0.0       
	        fPercent = (fWeaponHit / fWeaponShots)
	        IF fPercent  > 1.0
	            fPercent  = 1.0
	        ENDIF
	        fPercent *= 100.0
			
			///fix bug 1674045
			INT Rounded_value = ROUND(fPercent)
			fPercent = TO_FLOAT(Rounded_value)
			
	        RETURN(fPercent)
	    ENDIF

		//SCRIPT_ASSERT("GET_WEAPON_ACCURACY_FROM_STATS invalid weapon passed in", GET_WEAPON_NAME(aweapontype))
    ENDIF
    RETURN(0.0)
ENDFUNC

FUNC FLOAT GET_TOTAL_WEAPON_ACCURACY(INT iSlot = -1) 
    

    FLOAT total_accuracy
    total_accuracy = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HITS_PEDS_VEHICLES, iSlot) / TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_SHOTS, iSlot))) * 100.00
    //SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_ACCURACY, total_accuracy)
    
    RETURN total_accuracy
ENDFUNC
/*
PROC TRACK_Accuracy_weapons2() 
    FLOAT temp_gun_accuracy
    

    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTSHTGN_SHOTS)>0
        temp_gun_accuracy = (TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTSHTGN_HITS)) / (TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTSHTGN_SHOTS)))) * 100.0
        SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_ASLTSHTGN_ACCURACY, temp_gun_accuracy)

    ENDIF
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_RUBBERGUN_SHOTS)>0
        temp_gun_accuracy = (TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_RUBBERGUN_HITS)) / (TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_RUBBERGUN_SHOTS)))) * 100.0
        SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_RUBBERGUN_ACCURACY, temp_gun_accuracy)

    ENDIF
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_STUNGUN_SHOTS)>0
        temp_gun_accuracy = (TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_STUNGUN_HITS)) / (TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_STUNGUN_SHOTS)))) * 100.0
        SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_STUNGUN_ACCURACY, temp_gun_accuracy)

    ENDIF
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_SNIPERRFL_SHOTS)>0
        temp_gun_accuracy = (TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_SNIPERRFL_HITS)) / (TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_SNIPERRFL_SHOTS)))) * 100.0
        SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_SNIPERRFL_ACCURACY, temp_gun_accuracy)

    ENDIF

    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_HVYSNIPER_SHOTS)>0
        temp_gun_accuracy = (TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_HVYSNIPER_HITS)) / (TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_HVYSNIPER_SHOTS)))) * 100.0
        SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HVYSNIPER_ACCURACY, temp_gun_accuracy)

    ENDIF
    

    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_MINIGUN_SHOTS)>0
        temp_gun_accuracy = (TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_MINIGUN_HITS)) / (TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_MINIGUN_SHOTS)))) * 100.0
        SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_MINIGUN_ACCURACY, temp_gun_accuracy)

    ENDIF


    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_SMKGRENADE_SHOTS)>0
        temp_gun_accuracy = (TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_SMKGRENADE_HITS)) / (TO_FLOAT(GET_MP_INT_CHARACTER_STAT(MP_STAT_SMKGRENADE_SHOTS)))) * 100.0
        SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_SMKGRENADE_ACCURACY, temp_gun_accuracy)

    ENDIF
ENDPROC*/

/*
PROC TRACK_DISTANCE_TRAVELLED()

INT total_distance_travelled_value
// need to total up 

ENDPROC*/

// stores the rank value from the XP to an award
PROC TRACK_STORE_PLAYERS_XP_STAT_IN_AWARD(MP_INT_AWARD award)
    INT Rank = GET_RANK_FROM_XP_VALUE(GET_STAT_CHARACTER_XP())
    SET_MP_INT_CHARACTER_AWARD(award, Rank)
    
    // have to split labels depending on which gang player is in
    
ENDPROC



//PROC GENERAL_INT_AWARD_TRACK(MP_INT_AWARD award, MP_INT_STATS stat)
//
//  SET_MP_INT_CHARACTER_AWARD(award, GET_MP_INT_CHARACTER_STAT(stat))
//
//ENDPROC

//PROC TRACK_BOOL_WEAPON_AWARDS_KILLS(MP_BOOL_AWARD award, MP_INT_STATS stat)
//  //INT number_of_times_cuffed = GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_TIMES_ARRESTED)
//  INT value_of_stat = GET_MP_INT_CHARACTER_STAT(stat)
//  
//  IF value_of_stat >= 100
//      SET_MP_BOOL_CHARACTER_AWARD(AWARD, TRUE)
//      TRACK_BOOLCHAR_AWARDS(AWARD)
//  ENDIF
//                      
//ENDPROC


//PROC TRACK_STORE_LAST_SESSION_TIME()
//
//  IF NOT IS_TRANSITION_ACTIVE()
//      STRUCT_STAT_DATE CurrentTime = GET_CURRENT_TIMEOFDAY_AS_STAT()
//  
//      SET_MP_DATE_CHARACTER_STAT(MP_STAT_CHAR_LAST_PLAY_TIME, CurrentTime)
//  ENDIF
//ENDPROC



PROC CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(BOOL bDisplayMessage, PLAYERKIT Kit, MP_INT_STATS stat_of_weapon_to_check, INT number_of_kills_to_unlock)
    
    IF NOT IS_MP_KIT_UNLOCKED(Kit)
        bDisplayMessage = TRUE
        IF GET_MP_INT_CHARACTER_STAT(stat_of_weapon_to_check) >= number_of_kills_to_unlock
            SET_MP_KIT_UNLOCKED(Kit, TRUE, bDisplayMessage)
            INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NO_WEAPON_CLR_MOD_UNLOCK,1)
            INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_NO_WEAPON_MODS_UNLOCK, 1)
            
        ENDIF
    ENDIF

ENDPROC

PROC CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(BOOL bDisplayMessage, PLAYERKIT Kit,  INT rank_to_unlock)
    
    IF NOT IS_MP_KIT_UNLOCKED(Kit)
        bDisplayMessage = TRUE
        INT rank = GET_PLAYER_RANK(PLAYER_ID())
        IF rank >= rank_to_unlock
            SET_MP_KIT_UNLOCKED(Kit, TRUE, bDisplayMessage)
        ENDIF
    ENDIF

ENDPROC

PROC CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(BOOL bDisplayMessage, PLAYERKIT Kit,  INT rank_to_unlock)

    IF NOT IS_MP_KIT_UNLOCKED(Kit)
        bDisplayMessage = TRUE
        //INT CREWXPSTAT= GET_MP_INT_PLAYER_STAT(GET_CURRENT_CREW_LOCAL_STAT_FROM_SLOT(GET_CURRENT_CREW_SLOT_FROM_ID(GET_MP_PLAYER_CLAN_ID(PLAYER_ID())))) // need to get the correct slot
        //INT rank = GET_PLAYER_RANK(PLAYER_ID())
        GAMER_HANDLE player_handle 
        player_handle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
        IF  NETWORK_CLAN_SERVICE_IS_VALID()
            IF NETWORK_CLAN_PLAYER_IS_ACTIVE(player_handle)   
                INT CREWXPSTAT = GET_PLAYER_LOCAL_CREW_XP_VALUE_FROM_PLAYER_ID(PLAYER_ID())
                IF CREWXPSTAT >= rank_to_unlock
                    SET_MP_KIT_UNLOCKED(Kit, TRUE, bDisplayMessage)
                        #IF IS_DEBUG_BUILD
                            NET_PRINT("Gold guns unlocked in net_stats_tracking CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED")
                        #ENDIF
                ENDIF

            ENDIF
        ENDIF
    ENDIF
    
    
    IF Kit =  PLAYERKIT_GOLD_PISTOL
    AND NOT IS_MP_CREWREWUNLOCKED(CREWUNLOCKS_GOLD_GUN_TINT)
        SET_MP_CREWUNLOCKSED(CREWUNLOCKS_GOLD_GUN_TINT, TRUE, FALSE)
    ENDIF
ENDPROC

PROC ADD_COLLECTORS_EDITION_VEHICLE_TO_MP_SAVE_GAME(INT iSlotToUse)
    REQUEST_MODEL(KHAMELION)
    IF HAS_MODEL_LOADED(KHAMELION) 
        VEHICLE_INDEX veh = CREATE_VEHICLE(KHAMELION, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, 0.0, -300.0>>), 0.0, FALSE, FALSE)
        INT iRand = GET_RANDOM_INT_IN_RANGE(0, GET_NUMBER_OF_VEHICLE_MODEL_COLOURS(KHAMELION))
        SET_VEHICLE_COLOUR_COMBINATION(veh, iRand)
        PRINTLN("CDM MP.. - Set colour combination #", iRand)
        
        MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(veh, iSlotToUse, TRUE, TRUE, FALSE,TRUE)
        
        DELETE_VEHICLE(veh)
        SET_MODEL_AS_NO_LONGER_NEEDED(KHAMELION)
        SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CHAR_JOIN_SOCIAL_CLUB_MSG, TRUE)
        REQUEST_SAVE(SSR_REASON_SPECIAL_EDITION_CONTENT, STAT_SAVETYPE_IMMEDIATE_FLUSH)
        PRINTLN("CDM MP.. - Added collectors edition car to garage in slot #", MPGlobalsAmbience.iSlotToUseForVeh)
    ENDIF

ENDPROC

PROC GIVE_SOCIAL_CLUB_WELCOME_MESSAGE()

//  IF NOT  GET_MP_BOOL_CHARACTER_STAT( MP_STAT_CHAR_JOIN_SOCIAL_CLUB_MSG)
//      IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_CUSTOM_CAR_GARAGE)
//          IF NOT MPGlobalsAmbience.bInitedValuesForSC_Collector
//              IF IS_COLLECTORS_EDITION_GAME()
//                  IF g_MP_STAT_PROPERTY_HOUSE[GET_SLOT_NUMBER(-1)] <= 0
//                      BEGIN_TEXT_COMMAND_THEFEED_POST("MP_PROP_COLLECT")
//                          ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("KHAMEL") 
//                      END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, "")
//                      INT iRandArray[MAX_MP_PROPERTIES]
//                      INT i
//                      INT iCounter
//                      REPEAT MAX_MP_PROPERTIES i
//                          IF IS_PROPERTY_OF_SIZE_TYPE(i+1,PROP_SIZE_TYPE_2_GAR)
//                              iRandArray[iCounter] = i+1
//                              iCounter++
//                          ENDIF
//                      ENDREPEAT
//                      INT iRand = GET_RANDOM_INT_IN_RANGE(0,iCounter)
//                      SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE,iRandArray[iRand])
//                      PRINTLN("CDM MP.. - Player given property #",iRandArray[iRand], " for Collector's Edition!" )
//                      MPGlobalsAmbience.iSlotToUseForVeh  = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,TRUE,FALSE,)
//                      #IF IS_DEBUG_BUILD
//                      IF MPGlobalsAmbience.iSlotToUseForVeh = -1
//                          PRINTLN("Player has collectors edition, and no property but also no space for a vehicle!!! See Conor.")
//                          SCRIPT_ASSERT("Player has collectors edition, and no property but also no space for a vehicle!!! See Conor.")
//                      ENDIF
//                      #ENDIF
//                  ELSE
//                      BEGIN_TEXT_COMMAND_THEFEED_POST("MP_PROP_COLLECT2")
//                          ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("KHAMEL") 
//                      END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, "")
//                      PRINTLN("CDM MP.. - Player would be given a Collectors Edition unlock property but they already have a property")
//                      MPGlobalsAmbience.iSlotToUseForVeh = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST,TRUE,FALSE,)
//                      #IF IS_DEBUG_BUILD
//                      IF MPGlobalsAmbience.iSlotToUseForVeh = -1
//                          PRINTLN("Player has collectors edition, and no property but also no space for a vehicle!!! See Conor.")
//                          SCRIPT_ASSERT("Player has collectors edition, and no property but also no space for a vehicle!!! See Conor.")
//                      ENDIF
//                      #ENDIF
//                  ENDIF
//                  MPGlobalsAmbience.bInitedValuesForSC_Collector = TRUE
//                  
//                  
//                  //Display message here
////                ELSE
////                    IF IS_GAME_LINKED_TO_SOCIAL_CLUB()
////                        IF g_MP_STAT_PROPERTY_HOUSE[GET_SLOT_NUMBER(-1)] <= 0
////                            BEGIN_TEXT_COMMAND_THEFEED_POST("MP_PROP_SC_T")
////                                ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("MP_PROP_SC") 
////                            END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, "")
////                            INT iRandArray[MAX_MP_PROPERTIES]
////                            INT i
////                            INT iCounter
////                            REPEAT MAX_MP_PROPERTIES i
////                                IF IS_PROPERTY_OF_SIZE_TYPE(i+1,PROP_SIZE_TYPE_2_GAR)
////                                    IF IS_PROPERTY_IN_CITY_BOUNDS(i+1)
////                                        iRandArray[iCounter] = i+1
////                                        iCounter++
////                                    ENDIF
////                                ENDIF
////                            ENDREPEAT
////                            INT iRand = GET_RANDOM_INT_IN_RANGE(0,iCounter)
////        
////                            //IF iRandArray[iRand]= 0
////                            //ENDIF
////                            
////                            SET_HEAVILY_ACCESSED_MP_INT_CHARACTER_STAT(MP_STAT_PROPERTY_HOUSE,iRandArray[iRand])
////                        ELSE
////                            PRINTLN("CDM MP.. - Player would be given a SC unlock property but they already have a property")
////                        ENDIF
////                        SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CHAR_JOIN_SOCIAL_CLUB_MSG, TRUE)
////                        
////                        REQUEST_SAVE()
////                        //Display message here
////                        //SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_UNLOCKS, rank_unlock_items, "", "", "", -1,WEAPONTYPE_INVALID, WEAPONCOMPONENT_INVALID,"", 0 )
////                    ENDIF
//              ENDIF
//          ELSE
//              IF MPGlobalsAmbience.iSlotToUseForVeh != -1
//                  ADD_COLLECTORS_EDITION_VEHICLE_TO_MP_SAVE_GAME(MPGlobalsAmbience.iSlotToUseForVeh)
//              ELSE
//                  SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CHAR_JOIN_SOCIAL_CLUB_MSG, TRUE)
//                  REQUEST_SAVE()
//              ENDIF
//          ENDIF
//      ENDIF
//  ENDIF
ENDPROC



PROC TRACK_CREW_TATTOO_UNLOCKS()
    GAMER_HANDLE player_handle
        player_handle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
        IF  NETWORK_CLAN_SERVICE_IS_VALID()
            IF NETWORK_CLAN_PLAYER_IS_ACTIVE(player_handle)   
                BOOL displaymessage = FALSE
                
            // If crew unlocks have been displayed already, hide message during taxi ride
                /*IF IS_LOCAL_PLAYER_IN_TAXI_FOR_INTRO() = TRUE
                OR IS_LOCAL_PLAYER_RUNNING_FM_INTRO_CUT()
                    displaymessage = FALSE
                ENDIF
                */
                
                IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_CREW_A   ) = FALSE
                    IF IS_MP_CREWREWUNLOCKED(CREWUNLOCKS_TATTOO_LOGO)
                        SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_CREW_A  , TRUE, displaymessage)
                        
                        #IF IS_DEBUG_BUILD
                            NET_PRINT("TRACK_CREW_TATTOO_UNLOCKS TATTOO_MP_FM_CREW_A UNLOCKED ")
                        #ENDIF
                    ENDIF
                ENDIF
                
                IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_CREW_B   ) = FALSE
                    IF IS_MP_CREWREWUNLOCKED(CREWUNLOCKS_TATTOO_LOGO)
                        SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_CREW_B  , TRUE, displaymessage)
                        #IF IS_DEBUG_BUILD
                            NET_PRINT("TRACK_CREW_TATTOO_UNLOCKS TATTOO_MP_FM_CREW_B UNLOCKED")
                        #ENDIF
                    ENDIF
                ENDIF


                IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_CREW_C   ) = FALSE
                    IF IS_MP_CREWREWUNLOCKED(CREWUNLOCKS_TATTOO_LOGO)
                        SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_CREW_C  , TRUE, displaymessage)
                        #IF IS_DEBUG_BUILD
                            NET_PRINT("TRACK_CREW_TATTOO_UNLOCKS TATTOO_MP_FM_CREW_C UNLOCKED")
                        #ENDIF
                    ENDIF
                ENDIF

                IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_CREW_D   ) = FALSE
                    IF IS_MP_CREWREWUNLOCKED(CREWUNLOCKS_TATTOO_LOGO)
                        SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_CREW_D  , TRUE, displaymessage)
                        #IF IS_DEBUG_BUILD
                            NET_PRINT("TRACK_CREW_TATTOO_UNLOCKS TATTOO_MP_FM_CREW_D UNLOCKED")
                        #ENDIF
                    ENDIF
                ENDIF
                #IF USE_TU_CHANGES
                    IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_CREW_E   ) = FALSE
                        IF IS_MP_CREWREWUNLOCKED(CREWUNLOCKS_TATTOO_LOGO)
                            SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_CREW_E  , TRUE, displaymessage)
                            #IF IS_DEBUG_BUILD
                                NET_PRINT("TRACK_CREW_TATTOO_UNLOCKS TATTOO_MP_FM_CREW_D UNLOCKED")
                            #ENDIF
                        ENDIF
                    ENDIF
                    
                    IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_CREW_F   ) = FALSE
                        IF IS_MP_CREWREWUNLOCKED(CREWUNLOCKS_TATTOO_LOGO)
                            SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_CREW_F  , TRUE, displaymessage)
                            #IF IS_DEBUG_BUILD
                                NET_PRINT("TRACK_CREW_TATTOO_UNLOCKS TATTOO_MP_FM_CREW_D UNLOCKED")
                            #ENDIF
                        ENDIF
                    ENDIF
                #ENDIF
            ENDIF
        ENDIF
ENDPROC


PROC CHECK_IF_ALL_FM_PLATINUM_AWARDS_HAVE_BEEN_UNLOCKED()       // check if all platinum awards have been unlocked

    IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_100_HEADSHOTS             )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_100_KILLS_PISTOL         ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_100_KILLS_SHOTGUN        ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_100_KILLS_SMG            ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_100_KILLS_SNIPER         ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_20_KILLS_MELEE           ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_25_KILLS_STICKYBOMBS     ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_50_KILLS_GRENADES        ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_50_KILLS_ROCKETLAUNCH    ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_50_VEHICLES_BLOWNUP      ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_5STAR_WANTED_AVOIDANCE   ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_CAR_BOMBS_ENEMY_KILLS    ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_CARS_EXPORTED            ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_ENEMYDRIVEBYKILLS        ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FM_DM_WINS               )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FM_GOLF_WON              )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FM_GTA_RACES_WON         )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FM_SHOOTRANG_CT_WON      )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FM_SHOOTRANG_RT_WON      )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FM_SHOOTRANG_TG_WON      )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FM_TDM_WINS              )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FM_TENNIS_WON            )
    AND IS_MP_AWARD_PLATINUM_PLYINT_UNLOCKED (MPPLY_AWD_FM_CR_DM_MADE_INDEX           )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMBASEJMP                )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMBBETWIN                )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMHORDWAVESSURVIVE       )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_HOLD_UP_SHOPS            )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_KILLS_ASSAULT_RIFLE      )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_KILLS_MACHINEGUN         )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_LAPDANCES                )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_NO_ARMWRESTLING_WINS     )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_NO_HAIRCUTS              )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_RACES_WON                )
   // AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED( MP_AWARD_SECURITY_CARS_ROBBED    ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_VEHICLES_JACKEDR         ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_WIN_AT_DARTS             )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_PASSENGERTIME            ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_TIME_IN_HELICOPTER       ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FM_DM_3KILLSAMEGUY       ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FM_DM_KILLSTREAK         ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FM_DM_STOLENKILL         ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FM_DM_TOTALKILLS         ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FM_GOLF_BIRDIES          ) 
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FM_GOLF_HOLE_IN_1       ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FM_RACE_LAST_FIRST      ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FM_RACES_FASTEST_LAP     ) 
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FM_SHOOTRANG_GRAN_WON   ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FM_TDM_MVP               )
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FM_TENNIS_5_SET_WINS    ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED( MP_AWARD_FM_TENNIS_ACE           )
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FM_TENNIS_STASETWIN     )
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FM6DARTCHKOUT           ) 
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FMATTGANGHQ             )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_PARACHUTE_JUMPS_20M      ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_PARACHUTE_JUMPS_50M      ) 
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_VEHICLE_JUMP_OVER_40M    ) 
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_BUY_EVERY_GUN           ) 
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FMWINEVERYGAMEMODE      )
    AND IS_MP_AWARD_PLATINUM_PLYINT_UNLOCKED (MPPLY_AWD_FM_CR_MISSION_SCORE_INDEX     ) 
    AND IS_MP_AWARD_PLATINUM_PLYINT_UNLOCKED (MPPLY_AWD_FM_CR_PLAYED_BY_PEEP_INDEX    ) 
    AND IS_MP_AWARD_PLATINUM_PLYINT_UNLOCKED (MPPLY_AWD_FM_CR_RACES_MADE_INDEX        ) 
    //AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED( MP_AWARD_FMCRATEDROPS            )
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FM25DIFFERENTDM         )
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FM25DIFFERENTRACES      )
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FM25DIFITEMSCLOTHES     )       
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FMFULLYMODDEDCAR        )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMKILLBOUNTY             )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMOVERALLKILLS           )
  //  AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FMPICKUPDLCCRATE1ST     )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMSHOOTDOWNCOPHELI       )
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FM25DIFFERENTDM         )
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FMKILL3ANDWINGTARACE    )
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FMKILLSTREAKSDM         )
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FMMOSTKILLSGANGHIDE     )       
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FMMOSTKILLSSURVIVE      )   
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FMRACEWORLDRECHOLDER    )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMRALLYWONDRIVE          )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMRALLYWONNAV            )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMREVENGEKILLSDM         )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMWINAIRRACE             )
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FMWINCUSTOMRACE         )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMWINRACETOPOINTS        )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMWINSEARACE             )
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FMWINALLRACEMODES       )
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMTIME5STARWANTED)
    AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FMFURTHESTWHEELIE)
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMDRIVEWITHOUTCRASH)
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTFLIPSINONEVEHICLE)
    AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTSPINSINONEVEHICLE)
	AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_DANCE_TO_SOLOMUN )
	AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_DANCE_TO_TALEOFUS)
	AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_DANCE_TO_DIXON   )
	AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_DANCE_TO_BLKMAD  )
	AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_CLUB_DRUNK       )
	AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_CLUB_COORD      )
	AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_CLUB_HOTSPOT    )
	AND IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_CLUB_CLUBBER    )
        #IF IS_DEBUG_BUILD
            NET_PRINT("CHECK_IF_ALL_FM_PLATINUM_AWARDS_HAVE_BEEN_UNLOCKED Unlock platinum guns")
        #ENDIF
    /*  SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_PISTOL    ,TRUE      )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_CMBTPISTOL,FALSE )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_APPISTOL ,FALSE   )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_MICROSMG ,FALSE   )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_SMG      ,FALSE  )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_ASLTRIFLE,FALSE  )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_CRBNRIFLE,FALSE  )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_ADVRRIFLE,FALSE  )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_MG       ,FALSE  )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_CMBTMG   ,FALSE  )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_PUMP     ,FALSE   )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_SAWOFF   ,FALSE  )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_ASLTSHTGN,FALSE  )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_RUBBERGUN,FALSE  )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_STUNGUN  ,FALSE  )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_SNIPERRFL,FALSE  )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_HVYSNIPER,FALSE  )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_GRNLAUNCH,FALSE  )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_RPG      ,FALSE  )
        SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_MINIGUN  ,FALSE  )*/
        gcompletedallplatnumawards = TRUE
    ENDIF

    
    #IF IS_DEBUG_BUILD
        IF  g_bAward_UnlockAllPLATINUM =TRUE
            #IF IS_DEBUG_BUILD
                NET_PRINT("CHECK_IF_ALL_FM_PLATINUM_AWARDS_HAVE_BEEN_UNLOCKED g_bAward_UnlockAllPLATINUM Unlock = true platinum guns")
            #ENDIF
        
        /*  SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_PISTOL    ,TRUE      )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_CMBTPISTOL,FALSE )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_APPISTOL ,FALSE   )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_MICROSMG ,FALSE   )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_SMG      ,FALSE  )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_ASLTRIFLE,FALSE  )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_CRBNRIFLE,FALSE  )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_ADVRRIFLE,FALSE  )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_MG       ,FALSE  )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_CMBTMG   ,FALSE  )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_PUMP     ,FALSE   )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_SAWOFF   ,FALSE  )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_ASLTSHTGN,FALSE  )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_RUBBERGUN,FALSE  )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_STUNGUN  ,FALSE  )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_SNIPERRFL,FALSE  )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_HVYSNIPER,FALSE  )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_GRNLAUNCH,FALSE  )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_RPG      ,FALSE  )
            SET_MP_KIT_UNLOCKED(PLAYERKIT_PLATINUM_MINIGUN  ,FALSE  )*/
        ENDIF
    
    #ENDIF
            
ENDPROC






PLAYERKIT iColourWeaponToCheck
PROC TRACK_UNLOCK_OF_COLOURED_WEAPONS()
    
        int number_tan_kills = 100
        int number_green_kills = 200
        int number_red_kills = 400
        int number_blue_kills = 600
        //int rank_to_unlock_pink = 69
        
        //INT crewxp_to_unlock_GOLD = 255100    // respect.  unlock at level 50 of Crew rank Will have to tell kenneth if this changes
        
//	Graeme - do we need to deal with DLC weapons (e.g. PLAYERKIT_GREEN_DLC_SPECIALCARBINE) here?
        
    SWITCH iColourWeaponToCheck
    
        //tan guns
        CASE  PLAYERKIT_TAN_PISTOL      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,  PLAYERKIT_TAN_PISTOL,     MP_STAT_PISTOL_ENEMY_KILLS, number_tan_kills)       BREAK
        CASE  PLAYERKIT_TAN_CMBTPISTOL  CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_CMBTPISTOL,  MP_STAT_CMBTPISTOL_ENEMY_KILLS, number_tan_kills)   BREAK
        CASE  PLAYERKIT_TAN_APPISTOL    CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_APPISTOL ,   MP_STAT_APPISTOL_ENEMY_KILLS, number_tan_kills)     BREAK
        CASE  PLAYERKIT_TAN_MICROSMG    CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_MICROSMG ,   MP_STAT_MICROSMG_ENEMY_KILLS, number_tan_kills)     BREAK
        CASE  PLAYERKIT_TAN_SMG         CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_SMG,         MP_STAT_SMG_ENEMY_KILLS, number_tan_kills)          BREAK
        CASE  PLAYERKIT_TAN_ASLTRIFLE   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_ASLTRIFLE,   MP_STAT_ASLTRIFLE_ENEMY_KILLS, number_tan_kills)    BREAK
        CASE  PLAYERKIT_TAN_CRBNRIFLE   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_CRBNRIFLE,   MP_STAT_CRBNRIFLE_ENEMY_KILLS, number_tan_kills)    BREAK
        CASE  PLAYERKIT_TAN_ADVRRIFLE   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_ADVRRIFLE,   MP_STAT_ADVRIFLE_ENEMY_KILLS, number_tan_kills)     BREAK
        CASE  PLAYERKIT_TAN_MG          CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_MG,          MP_STAT_MG_ENEMY_KILLS, number_tan_kills)           BREAK
        CASE  PLAYERKIT_TAN_CMBTMG      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_CMBTMG,      MP_STAT_CMBTMG_ENEMY_KILLS, number_tan_kills)       BREAK
        CASE  PLAYERKIT_TAN_PUMP        CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_PUMP,        MP_STAT_PUMP_ENEMY_KILLS, number_tan_kills)         BREAK
        CASE  PLAYERKIT_TAN_SAWOFF      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_SAWOFF,      MP_STAT_SAWNOFF_ENEMY_KILLS, number_tan_kills)      BREAK
        CASE  PLAYERKIT_TAN_ASLTSHTGN   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_ASLTSHTGN,   MP_STAT_ASLTSHTGN_ENEMY_KILLS, number_tan_kills)    BREAK
        //CASE  PLAYERKIT_TAN_RUBBERGUN     CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_TAN_RUBBERGUN,  MP_STAT_RUBBERGUN_ENEMY_KILLS, number_tan_kills)    BREAK
        CASE  PLAYERKIT_TAN_STUNGUN     CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_STUNGUN,         MP_STAT_STUNGUN_ENEMY_KILLS, number_tan_kills)      BREAK
        CASE  PLAYERKIT_TAN_SNIPERRFL   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_SNIPERRFL,   MP_STAT_SNIPERRFL_ENEMY_KILLS, number_tan_kills)    BREAK
        CASE  PLAYERKIT_TAN_HVYSNIPER   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_HVYSNIPER,   MP_STAT_HVYSNIPER_ENEMY_KILLS, number_tan_kills)    BREAK
        CASE  PLAYERKIT_TAN_GRNLAUNCH   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_GRNLAUNCH,   MP_STAT_GRNLAUNCH_ENEMY_KILLS, number_tan_kills)    BREAK
        CASE  PLAYERKIT_TAN_RPG         CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_RPG,             MP_STAT_RPG_ENEMY_KILLS, number_tan_kills)          BREAK
        CASE  PLAYERKIT_TAN_MINIGUN     CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_TAN_MINIGUN,         MP_STAT_MINIGUNS_ENEMY_KILLS, number_tan_kills)     BREAK
                                                                                                                                                                                    
        // GREEN weapons                                                                                                                                                            
                                                                                                                                                                                    
        CASE PLAYERKIT_GREEN_PISTOL         CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,  PLAYERKIT_GREEN_PISTOL,       MP_STAT_PISTOL_ENEMY_KILLS, number_green_kills)         BREAK               
        CASE PLAYERKIT_GREEN_CMBTPISTOL     CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_CMBTPISTOL,    MP_STAT_CMBTPISTOL_ENEMY_KILLS, number_green_kills)     BREAK
        CASE PLAYERKIT_GREEN_APPISTOL       CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_APPISTOL   ,   MP_STAT_APPISTOL_ENEMY_KILLS, number_green_kills)       BREAK
        CASE PLAYERKIT_GREEN_MICROSMG       CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_MICROSMG   ,   MP_STAT_MICROSMG_ENEMY_KILLS, number_green_kills)       BREAK
        CASE PLAYERKIT_GREEN_SMG            CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_SMG,           MP_STAT_SMG_ENEMY_KILLS, number_green_kills)            BREAK
        CASE PLAYERKIT_GREEN_ASLTRIFLE      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_ASLTRIFLE,         MP_STAT_ASLTRIFLE_ENEMY_KILLS, number_green_kills)      BREAK
        CASE PLAYERKIT_GREEN_CRBNRIFLE      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_CRBNRIFLE,         MP_STAT_CRBNRIFLE_ENEMY_KILLS, number_green_kills)      BREAK
        CASE PLAYERKIT_GREEN_ADVRRIFLE      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_ADVRRIFLE,         MP_STAT_ADVRIFLE_ENEMY_KILLS, number_green_kills)       BREAK
        CASE PLAYERKIT_GREEN_MG             CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_MG,            MP_STAT_MG_ENEMY_KILLS, number_green_kills)             BREAK
        CASE PLAYERKIT_GREEN_CMBTMG         CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_CMBTMG,        MP_STAT_CMBTMG_ENEMY_KILLS, number_green_kills)         BREAK
        CASE PLAYERKIT_GREEN_PUMP           CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_PUMP,          MP_STAT_PUMP_ENEMY_KILLS, number_green_kills)           BREAK
        CASE PLAYERKIT_GREEN_SAWOFF         CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_SAWOFF,        MP_STAT_SAWNOFF_ENEMY_KILLS, number_green_kills)        BREAK
        CASE PLAYERKIT_GREEN_ASLTSHTGN      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_ASLTSHTGN,         MP_STAT_ASLTSHTGN_ENEMY_KILLS, number_green_kills)      BREAK
        //CASE PLAYERKIT_GREEN_RUBBERGUN        CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GREEN_RUBBERGUN,        MP_STAT_RUBBERGUN_ENEMY_KILLS, number_green_kills)      BREAK
        CASE PLAYERKIT_GREEN_STUNGUN        CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_STUNGUN,       MP_STAT_STUNGUN_ENEMY_KILLS, number_green_kills)        BREAK
        CASE PLAYERKIT_GREEN_SNIPERRFL      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_SNIPERRFL,         MP_STAT_SNIPERRFL_ENEMY_KILLS, number_green_kills)      BREAK
        CASE PLAYERKIT_GREEN_HVYSNIPER      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_HVYSNIPER,         MP_STAT_HVYSNIPER_ENEMY_KILLS, number_green_kills)      BREAK
        CASE PLAYERKIT_GREEN_GRNLAUNCH      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_GRNLAUNCH,         MP_STAT_GRNLAUNCH_ENEMY_KILLS, number_green_kills)      BREAK
        CASE PLAYERKIT_GREEN_RPG            CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_RPG,           MP_STAT_RPG_ENEMY_KILLS, number_green_kills)            BREAK
        CASE PLAYERKIT_GREEN_MINIGUN        CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_GREEN_MINIGUN,       MP_STAT_MINIGUNS_ENEMY_KILLS, number_green_kills)       BREAK
    
        // red weapons
        
        CASE  PLAYERKIT_RED_PISTOL      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,  PLAYERKIT_RED_PISTOL,         MP_STAT_PISTOL_ENEMY_KILLS, number_red_kills)           BREAK
        CASE  PLAYERKIT_RED_CMBTPISTOL  CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_CMBTPISTOL,  MP_STAT_CMBTPISTOL_ENEMY_KILLS, number_red_kills)       BREAK
        CASE  PLAYERKIT_RED_APPISTOL    CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_APPISTOL ,   MP_STAT_APPISTOL_ENEMY_KILLS, number_red_kills)         BREAK
        CASE  PLAYERKIT_RED_MICROSMG    CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_MICROSMG ,   MP_STAT_MICROSMG_ENEMY_KILLS, number_red_kills)         BREAK
        CASE  PLAYERKIT_RED_SMG         CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_SMG,             MP_STAT_SMG_ENEMY_KILLS, number_red_kills)              BREAK
        CASE  PLAYERKIT_RED_ASLTRIFLE   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_ASLTRIFLE,   MP_STAT_ASLTRIFLE_ENEMY_KILLS, number_red_kills)        BREAK
        CASE  PLAYERKIT_RED_CRBNRIFLE   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_CRBNRIFLE,   MP_STAT_CRBNRIFLE_ENEMY_KILLS, number_red_kills)        BREAK
        CASE  PLAYERKIT_RED_ADVRRIFLE   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_ADVRRIFLE,   MP_STAT_ADVRIFLE_ENEMY_KILLS, number_red_kills)         BREAK
        CASE  PLAYERKIT_RED_MG          CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_MG,          MP_STAT_MG_ENEMY_KILLS, number_red_kills)               BREAK
        CASE  PLAYERKIT_RED_CMBTMG      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_CMBTMG,      MP_STAT_CMBTMG_ENEMY_KILLS, number_red_kills)           BREAK
        CASE  PLAYERKIT_RED_PUMP        CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_PUMP,        MP_STAT_PUMP_ENEMY_KILLS, number_red_kills)             BREAK
        CASE  PLAYERKIT_RED_SAWOFF      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_SAWOFF,      MP_STAT_SAWNOFF_ENEMY_KILLS, number_red_kills)          BREAK
        CASE  PLAYERKIT_RED_ASLTSHTGN   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_ASLTSHTGN,   MP_STAT_ASLTSHTGN_ENEMY_KILLS, number_red_kills)        BREAK
        //CASE  PLAYERKIT_RED_RUBBERGUN CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_RED_RUBBERGUN,  MP_STAT_RUBBERGUN_ENEMY_KILLS, number_red_kills)        BREAK
        CASE  PLAYERKIT_RED_STUNGUN     CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_STUNGUN,         MP_STAT_STUNGUN_ENEMY_KILLS, number_red_kills)          BREAK
        CASE  PLAYERKIT_RED_SNIPERRFL   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_SNIPERRFL,   MP_STAT_SNIPERRFL_ENEMY_KILLS, number_red_kills)        BREAK
        CASE  PLAYERKIT_RED_HVYSNIPER   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_HVYSNIPER,   MP_STAT_HVYSNIPER_ENEMY_KILLS, number_red_kills)        BREAK
        CASE  PLAYERKIT_RED_GRNLAUNCH   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_GRNLAUNCH,   MP_STAT_GRNLAUNCH_ENEMY_KILLS, number_red_kills)        BREAK
        CASE  PLAYERKIT_RED_RPG         CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_RPG,             MP_STAT_RPG_ENEMY_KILLS, number_red_kills)              BREAK
        CASE  PLAYERKIT_RED_MINIGUN     CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_RED_MINIGUN,         MP_STAT_MINIGUNS_ENEMY_KILLS, number_red_kills)         BREAK
    
        //blue weapon checks
        CASE PLAYERKIT_BLUE_PISTOL      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,  PLAYERKIT_BLUE_PISTOL,        MP_STAT_PISTOL_ENEMY_KILLS, number_blue_kills)          BREAK
        CASE PLAYERKIT_BLUE_CMBTPISTOL  CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_CMBTPISTOL,     MP_STAT_CMBTPISTOL_ENEMY_KILLS, number_blue_kills)      BREAK
        CASE PLAYERKIT_BLUE_APPISTOL    CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_APPISTOL    ,   MP_STAT_APPISTOL_ENEMY_KILLS, number_blue_kills)        BREAK
        CASE PLAYERKIT_BLUE_MICROSMG    CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_MICROSMG    ,   MP_STAT_MICROSMG_ENEMY_KILLS, number_blue_kills)        BREAK
        CASE PLAYERKIT_BLUE_SMG         CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_SMG,        MP_STAT_SMG_ENEMY_KILLS, number_blue_kills)             BREAK
        CASE PLAYERKIT_BLUE_ASLTRIFLE   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_ASLTRIFLE,  MP_STAT_ASLTRIFLE_ENEMY_KILLS, number_blue_kills)       BREAK
        CASE PLAYERKIT_BLUE_CRBNRIFLE   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_CRBNRIFLE,  MP_STAT_CRBNRIFLE_ENEMY_KILLS, number_blue_kills)       BREAK
        CASE PLAYERKIT_BLUE_ADVRRIFLE   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_ADVRRIFLE,  MP_STAT_ADVRIFLE_ENEMY_KILLS, number_blue_kills)        BREAK
        CASE PLAYERKIT_BLUE_MG          CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_MG,             MP_STAT_MG_ENEMY_KILLS, number_blue_kills)              BREAK
        CASE PLAYERKIT_BLUE_CMBTMG      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_CMBTMG,         MP_STAT_CMBTMG_ENEMY_KILLS, number_blue_kills)          BREAK
        CASE PLAYERKIT_BLUE_PUMP        CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_PUMP,       MP_STAT_PUMP_ENEMY_KILLS, number_blue_kills)            BREAK
        CASE PLAYERKIT_BLUE_SAWOFF      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_SAWOFF,         MP_STAT_SAWNOFF_ENEMY_KILLS, number_blue_kills)         BREAK
        CASE PLAYERKIT_BLUE_ASLTSHTGN   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_ASLTSHTGN,  MP_STAT_ASLTSHTGN_ENEMY_KILLS, number_blue_kills)       BREAK
        //CASE PLAYERKIT_BLUE_RUBBERGUN     CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_BLUE_RUBBERGUN,     MP_STAT_RUBBERGUN_ENEMY_KILLS, number_blue_kills)       BREAK
        CASE PLAYERKIT_BLUE_STUNGUN     CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_STUNGUN,    MP_STAT_STUNGUN_ENEMY_KILLS, number_blue_kills)         BREAK
        CASE PLAYERKIT_BLUE_SNIPERRFL   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_SNIPERRFL,  MP_STAT_SNIPERRFL_ENEMY_KILLS, number_blue_kills)       BREAK
        CASE PLAYERKIT_BLUE_HVYSNIPER   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_HVYSNIPER,  MP_STAT_HVYSNIPER_ENEMY_KILLS, number_blue_kills)       BREAK
        CASE PLAYERKIT_BLUE_GRNLAUNCH   CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_GRNLAUNCH,  MP_STAT_GRNLAUNCH_ENEMY_KILLS, number_blue_kills)       BREAK
        CASE PLAYERKIT_BLUE_RPG         CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_RPG,        MP_STAT_RPG_ENEMY_KILLS, number_blue_kills)             BREAK
        CASE PLAYERKIT_BLUE_MINIGUN     CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE, PLAYERKIT_BLUE_MINIGUN,    MP_STAT_MINIGUNS_ENEMY_KILLS, number_blue_kills)            BREAK
    
	
	
	
	
	
		  			
		CASE PLAYERKIT_TAN_DLC_ASSAULTSMG  	 	CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_TAN_DLC_ASSAULTSMG  	, MP_STAT_ASLTSMG_ENEMY_KILLS, number_tan_kills) BREAK				
		CASE PLAYERKIT_BLUE_DLC_ASSAULTSMG 	 	CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_BLUE_DLC_ASSAULTSMG 	, MP_STAT_ASLTSMG_ENEMY_KILLS, number_blue_kills) BREAK			
		CASE PLAYERKIT_RED_DLC_ASSAULTSMG  		CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_RED_DLC_ASSAULTSMG  	, MP_STAT_ASLTSMG_ENEMY_KILLS, number_red_kills) BREAK		
		CASE PLAYERKIT_GREEN_DLC_ASSAULTSMG     CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_GREEN_DLC_ASSAULTSMG  , MP_STAT_ASLTSMG_ENEMY_KILLS, number_green_kills) BREAK 		
		CASE PLAYERKIT_TAN_DLC_PISTOL50  		CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_TAN_DLC_PISTOL50  	, MP_STAT_PISTOL50_ENEMY_KILLS, number_tan_kills) BREAK			
		CASE PLAYERKIT_BLUE_DLC_PISTOL50 	 	CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_BLUE_DLC_PISTOL50 	,MP_STAT_PISTOL50_ENEMY_KILLS, number_blue_kills) BREAK		
		CASE PLAYERKIT_RED_DLC_PISTOL50  		CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_RED_DLC_PISTOL50  	,	MP_STAT_PISTOL50_ENEMY_KILLS, number_red_kills) BREAK	
		CASE PLAYERKIT_GREEN_DLC_PISTOL50       CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_GREEN_DLC_PISTOL50    ,  MP_STAT_PISTOL50_ENEMY_KILLS, number_green_kills) BREAK
		CASE PLAYERKIT_TAN_DLC_ASSAULTMG 	    CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_TAN_DLC_ASSAULTMG 	,  MP_STAT_ASLTMG_ENEMY_KILLS,  number_tan_kills) BREAK
		CASE PLAYERKIT_BLUE_DLC_ASSAULTMG 	 	CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_BLUE_DLC_ASSAULTMG 	,	MP_STAT_ASLTMG_ENEMY_KILLS,  number_BLUE_kills) BREAK	
		CASE PLAYERKIT_RED_DLC_ASSAULTMG  		CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_RED_DLC_ASSAULTMG  	,	MP_STAT_ASLTMG_ENEMY_KILLS,  number_red_kills) BREAK		
		CASE PLAYERKIT_GREEN_DLC_ASSAULTMG      CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_GREEN_DLC_ASSAULTMG   ,   MP_STAT_ASLTMG_ENEMY_KILLS,  number_green_kills) BREAK
		
		//CASE PLAYERKIT_TAN_DLC_BULLPUPSHOTGUN 	    CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_TAN_DLC_BULLPUPSHOTGUN 	,  MP_STAT_BULLPUP_ENEMY_KILLS,  number_tan_kills) BREAK
		//CASE PLAYERKIT_BLUE_DLC_BULLPUPSHOTGUN 	 	CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_BLUE_DLC_BULLPUPSHOTGUN 	,	MP_STAT_BULLPUP_ENEMY_KILLS,  number_BLUE_kills) BREAK	
		//CASE PLAYERKIT_RED_DLC_BULLPUPSHOTGUN  		CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_RED_DLC_BULLPUPSHOTGUN  	,	MP_STAT_BULLPUP_ENEMY_KILLS,  number_red_kills) BREAK		
		//CASE PLAYERKIT_GREEN_DLC_BULLPUPSHOTGUN     CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_GREEN_DLC_BULLPUPSHOTGUN   ,   MP_STAT_BULLPUP_ENEMY_KILLS,  number_green_kills) BREAK
		
		//CASE PLAYERKIT_TAN_DLC_HEAVYRIFLE  		CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_TAN_DLC_HEAVYRIFLE  	,	MP_STAT_HVYSNIPER_ENEMY_KILLS,  number_tan_kills) BREAK	
		//CASE PLAYERKIT_BLUE_DLC_HEAVYRIFLE 	 	CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_BLUE_DLC_HEAVYRIFLE 	,	MP_STAT_HVYSNIPER_ENEMY_KILLS,  number_blue_kills) BREAK 	
		//CASE PLAYERKIT_RED_DLC_HEAVYRIFLE  		CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_RED_DLC_HEAVYRIFLE  	,	 MP_STAT_HVYSNIPER_ENEMY_KILLS,  number_red_kills) BREAK	
		//CASE PLAYERKIT_GREEN_DLC_HEAVYRIFLE		CHECK_KILLED_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,PLAYERKIT_GREEN_DLC_HEAVYRIFLE	,	 MP_STAT_HV,  number_green_kills) BREAK	

		
			
	
	
	

	
	
	
	
	
	
	
	
	
	
	
	
	
   
		// DLC
	
	/*  // check pink gun (dont in net_rank_unlocks)
        CASE PLAYERKIT_PINK_PISTOL      CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE,  PLAYERKIT_PINK_PISTOL,     rank_to_unlock_pink)    BREAK       
        CASE PLAYERKIT_PINK_CMBTPISTOL  CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_CMBTPISTOL,  rank_to_unlock_pink)    BREAK
        CASE PLAYERKIT_PINK_APPISTOL    CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_APPISTOL ,   rank_to_unlock_pink)    BREAK
        CASE PLAYERKIT_PINK_MICROSMG    CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_MICROSMG ,   rank_to_unlock_pink)    BREAK
        CASE PLAYERKIT_PINK_SMG         CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_SMG,         rank_to_unlock_pink)    BREAK   
        CASE PLAYERKIT_PINK_ASLTRIFLE   CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_ASLTRIFLE,       rank_to_unlock_pink)    BREAK
        CASE PLAYERKIT_PINK_CRBNRIFLE   CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_CRBNRIFLE,       rank_to_unlock_pink)    BREAK
        CASE PLAYERKIT_PINK_ADVRRIFLE   CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_ADVRRIFLE,       rank_to_unlock_pink)    BREAK
        CASE PLAYERKIT_PINK_MG          CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_MG,          rank_to_unlock_pink)    BREAK       
        CASE PLAYERKIT_PINK_CMBTMG      CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_CMBTMG,      rank_to_unlock_pink)    BREAK   
        CASE PLAYERKIT_PINK_PUMP        CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_PUMP,            rank_to_unlock_pink)    BREAK   
        CASE PLAYERKIT_PINK_SAWOFF      CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_SAWOFF,      rank_to_unlock_pink)    BREAK   
        CASE PLAYERKIT_PINK_ASLTSHTGN   CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_ASLTSHTGN,       rank_to_unlock_pink)    BREAK
        //CASE PLAYERKIT_PINK_RUBBERGUN CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_RUBBERGUN,       rank_to_unlock_pink)    BREAK
        CASE PLAYERKIT_PINK_STUNGUN     CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_STUNGUN,     rank_to_unlock_pink)    BREAK
        CASE PLAYERKIT_PINK_SNIPERRFL   CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_SNIPERRFL,       rank_to_unlock_pink)    BREAK
        CASE PLAYERKIT_PINK_HVYSNIPER   CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_HVYSNIPER,       rank_to_unlock_pink)    BREAK
        CASE PLAYERKIT_PINK_GRNLAUNCH   CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_GRNLAUNCH,       rank_to_unlock_pink)    BREAK
        CASE PLAYERKIT_PINK_RPG         CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_RPG,         rank_to_unlock_pink)    BREAK   
        CASE PLAYERKIT_PINK_MINIGUN     CHECK_RANK_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_MINIGUN,     rank_to_unlock_pink)    BREAK
    */
        // check GOLD gun CREW XP
        
        //NEED TO SET crewxp_to_unlock_GOLD
        /*
        if   total crew xp /number of guys in crew > amount to unlock gold 
            crewxp_to_unlock_GOLD = true
        endif
        */
    
    /*  CASE PLAYERKIT_GOLD_PISTOL      CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(TRUE,  PLAYERKIT_GOLD_PISTOL,        crewxp_to_unlock_GOLD)  BREAK       
        CASE PLAYERKIT_GOLD_CMBTPISTOL  CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_CMBTPISTOL,    crewxp_to_unlock_GOLD)  BREAK
        CASE PLAYERKIT_GOLD_APPISTOL    CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_APPISTOL   ,   crewxp_to_unlock_GOLD)  BREAK
        CASE PLAYERKIT_GOLD_MICROSMG    CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_MICROSMG   ,   crewxp_to_unlock_GOLD)  BREAK
        CASE PLAYERKIT_GOLD_SMG         CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_SMG,           crewxp_to_unlock_GOLD)  BREAK   
        CASE PLAYERKIT_GOLD_ASLTRIFLE   CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_ASLTRIFLE,     crewxp_to_unlock_GOLD)  BREAK
        CASE PLAYERKIT_GOLD_CRBNRIFLE   CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_CRBNRIFLE,     crewxp_to_unlock_GOLD)  BREAK
        CASE PLAYERKIT_GOLD_ADVRRIFLE   CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_ADVRRIFLE,     crewxp_to_unlock_GOLD)  BREAK
        CASE PLAYERKIT_GOLD_MG          CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_MG,            crewxp_to_unlock_GOLD)  BREAK       
        CASE PLAYERKIT_GOLD_CMBTMG      CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_CMBTMG,        crewxp_to_unlock_GOLD)  BREAK   
        CASE PLAYERKIT_GOLD_PUMP        CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_PUMP,          crewxp_to_unlock_GOLD)  BREAK   
        CASE PLAYERKIT_GOLD_SAWOFF      CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_SAWOFF,        crewxp_to_unlock_GOLD)  BREAK   
        CASE PLAYERKIT_GOLD_ASLTSHTGN   CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_ASLTSHTGN,     crewxp_to_unlock_GOLD)  BREAK
        //CASE PLAYERKIT_GOLD_RUBBERGUN CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_PINK_RUBBERGUN,     crewxp_to_unlock_GOLD)  BREAK
        CASE PLAYERKIT_GOLD_STUNGUN     CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_STUNGUN,       crewxp_to_unlock_GOLD)  BREAK
        CASE PLAYERKIT_GOLD_SNIPERRFL   CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_SNIPERRFL,     crewxp_to_unlock_GOLD)  BREAK
        CASE PLAYERKIT_GOLD_HVYSNIPER   CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_HVYSNIPER,     crewxp_to_unlock_GOLD)  BREAK
        CASE PLAYERKIT_GOLD_GRNLAUNCH   CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_GRNLAUNCH,     crewxp_to_unlock_GOLD)  BREAK
        CASE PLAYERKIT_GOLD_RPG         CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_RPG,           crewxp_to_unlock_GOLD)  BREAK   
        CASE PLAYERKIT_GOLD_MINIGUN     CHECK_CREWXP_COLOURED_WEAPON_BEEN_UNLOCKED(FALSE, PLAYERKIT_GOLD_MINIGUN,       crewxp_to_unlock_GOLD)  BREAK
    */
        
        // PLATINUM CHECK IF ALL PLATINUM AWARDS HAVE BEEN UNLOCKED
        
    /*  CASE PLAYERKIT_PLATINUM_PISTOL      
        CASE PLAYERKIT_PLATINUM_CMBTPISTOL  
        CASE PLAYERKIT_PLATINUM_APPISTOL    
        CASE PLAYERKIT_PLATINUM_MICROSMG    
        CASE PLAYERKIT_PLATINUM_SMG         
        CASE PLAYERKIT_PLATINUM_ASLTRIFLE   
        CASE PLAYERKIT_PLATINUM_CRBNRIFLE   
        CASE PLAYERKIT_PLATINUM_ADVRRIFLE   
        CASE PLAYERKIT_PLATINUM_MG          
        CASE PLAYERKIT_PLATINUM_CMBTMG      
        CASE PLAYERKIT_PLATINUM_PUMP        
        CASE PLAYERKIT_PLATINUM_SAWOFF      
        CASE PLAYERKIT_PLATINUM_ASLTSHTGN   
        CASE PLAYERKIT_PLATINUM_STUNGUN     
        CASE PLAYERKIT_PLATINUM_SNIPERRFL   
        CASE PLAYERKIT_PLATINUM_HVYSNIPER   
        CASE PLAYERKIT_PLATINUM_GRNLAUNCH   
        CASE PLAYERKIT_PLATINUM_RPG         
        CASE PLAYERKIT_PLATINUM_MINIGUN     
            CHECK_IF_ALL_FM_PLATINUM_AWARDS_HAVE_BEEN_UNLOCKED()
        BREAK
        */
        
        DEFAULT
            // do sweet f.a.
        BREAK
    
    ENDSWITCH


//  IF (INT_TO_ENUM(PLAYERKIT, iColourWeaponToCheck) = PLAYERKIT_PINK_MINIGUN)
    IF iColourWeaponToCheck = PLAYERKIT_GREEN_DLC_PISTOL50
        iColourWeaponToCheck = PLAYERKIT_GOLD_PISTOL
    ELSE
        iColourWeaponToCheck =  INT_TO_ENUM(PLAYERKIT, ENUM_TO_INT(iColourWeaponToCheck) + 1)
    ENDIF
        
        #IF IS_DEBUG_BUILD
            //NET_PRINT("iColourWeaponToCheck= ") 
            //NET_PRINT_INT(ENUM_TO_INT(iColourWeaponToCheck))
        #ENDIF
        
ENDPROC




FUNC INT GET_NUMBER_OF_MELEE_KILLS()
    
    
    INT total_melee_kills = 0
    total_melee_kills = GET_MP_INT_CHARACTER_STAT(MP_STAT_KNIFE_ENEMY_KILLS)
    total_melee_kills += GET_MP_INT_CHARACTER_STAT(MP_STAT_NIGHTSTICK_ENEMY_KILLS)
    total_melee_kills += GET_MP_INT_CHARACTER_STAT(MP_STAT_CROWBAR_ENEMY_KILLS)
    total_melee_kills += GET_MP_INT_CHARACTER_STAT(MP_STAT_SHOVEL_ENEMY_KILLS)
    total_melee_kills += GET_MP_INT_CHARACTER_STAT(MP_STAT_WRENCH_ENEMY_KILLS)
    total_melee_kills += GET_MP_INT_CHARACTER_STAT(MP_STAT_HAMMER_ENEMY_KILLS)
    total_melee_kills += GET_MP_INT_CHARACTER_STAT(MP_STAT_BAT_ENEMY_KILLS)
    total_melee_kills += GET_MP_INT_CHARACTER_STAT(MP_STAT_UNARMED_ENEMY_KILLS)
    total_melee_kills += GET_MP_INT_CHARACTER_STAT(MP_STAT_GCLUB_ENEMY_KILLS)
	 total_melee_kills += GET_MP_INT_CHARACTER_STAT(MP_STAT_BOTTLE_ENEMY_KILLS)
	 total_melee_kills += GET_MP_INT_CHARACTER_STAT(MP_STAT_DAGGER_ENEMY_KILLS)
	 	total_melee_kills += GET_MP_INT_CHARACTER_STAT(MP_STAT_KNUCKLE_ENEMY_KILLS)
	
	 	total_melee_kills += GET_MP_INT_CHARACTER_STAT(MP_STAT_MACHETE_ENEMY_KILLS)
	
		total_melee_kills += GET_MP_INT_CHARACTER_STAT(MP_STAT_FLASHLIGHT_ENEMY_KILLS)
		total_melee_kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_SWBLADE_ENEMY_KILLS)
		total_melee_kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_BATTLEAXE_ENEMY_KILLS)
		total_melee_kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_POOLCUE_ENEMY_KILLS)


    RETURN total_melee_kills
    
ENDFUNC

FUNC INT GET_NUMBER_OF_SNIPER_KILLS()
    
    
    INT total_sniper_kills = 0
    total_sniper_kills = GET_MP_INT_CHARACTER_STAT(MP_STAT_SNIPERRFL_ENEMY_KILLS)
    total_sniper_kills += GET_MP_INT_CHARACTER_STAT(MP_STAT_HVYSNIPER_ENEMY_KILLS)
   	 total_sniper_kills += GET_MP_INT_CHARACTER_STAT(MP_STAT_MKRIFLE_ENEMY_KILLS)
    RETURN total_sniper_kills
ENDFUNC


FUNC INT GET_NUMBER_OF_VEHICLES_BLOWN_UP()
    //INT number_of_times_cuffed = GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_TIMES_ARRESTED)
/*
"MP0_CARS_EXPLODED"             
MP0_CARS_COPS_EXPLODED"   
MP0_BIKES_EXPLODED
MP0_BOATS_EXPLODED             
MP0_HELIS_EXPLODED              
MP0_PLANES_EXPLODED           
MP0_TRAILER_EXPLODED           
MP0_QUADBIKE_EXPLODED          
MP0_AUTOGYRO_EXPLODED           
MP0_BICYCLE_EXPLODED             
MP0_SUBMARINE_EXPLODED           
MP0_TRAIN_EXPLODED*/
    
    
    INT value_of_stat   =  GET_MP_INT_CHARACTER_STAT(MP_STAT_BIKES_EXPLODED) 
    value_of_stat       += GET_MP_INT_CHARACTER_STAT(MP_STAT_BOATS_EXPLODED) 
    value_of_stat       += GET_MP_INT_CHARACTER_STAT(MP_STAT_HELIS_EXPLODED) 
    value_of_stat       += GET_MP_INT_CHARACTER_STAT(MP_STAT_PLANES_EXPLODED) 
    value_of_stat       += GET_MP_INT_CHARACTER_STAT(MP_STAT_QUADBIKE_EXPLODED) 
    //value_of_stat         += GET_MP_INT_CHARACTER_STAT(MP_STAT_AUTOGYRO_EXPLODED)
    value_of_stat       += GET_MP_INT_CHARACTER_STAT(MP_STAT_BICYCLE_EXPLODED) 
    value_of_stat       += GET_MP_INT_CHARACTER_STAT(MP_STAT_SUBMARINE_EXPLODED)
    value_of_stat       += GET_MP_INT_CHARACTER_STAT(MP_STAT_TRAIN_EXPLODED)
    value_of_stat       += GET_MP_INT_CHARACTER_STAT(MP_STAT_CARS_EXPLODED)
    //value_of_stat         += GET_MP_INT_CHARACTER_STAT(MP_STAT_CARS_COPS_EXPLODED)


    RETURN value_of_stat
ENDFUNC



FUNC INT GET_NUMBER_OF_VEHICLES_JACKED()
    //INT number_of_times_cuffed = GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_TIMES_ARRESTED)
/*
MP_STAT_NUMBER_STOLEN_CARS            

MP_STAT_NUMBER_STOLEN_BIKES
MP_STAT_NUMBER_STOLEN_BOATS            
MP_STAT_NUMBER_STOLEN_HELIS              
MP_STAT_NUMBER_STOLEN_PLANES          
MP_STAT_NUMBER_STOLEN_TRAILER           
MP_STAT_NUMBER_STOLEN_QUADBIKE          
MP_STAT_NUMBER_STOLEN_AUTOGYRO           
MP_STAT_NUMBER_STOLEN_BICYCLE            
MP_STAT_NUMBER_STOLEN_SUBMARINE           
MP_STAT_NUMBER_STOLEN_TRAIN


*/


    INT value_of_stat = GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_STOLEN_CARS)
        value_of_stat +=GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_STOLEN_BIKES) 
        value_of_stat +=GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_STOLEN_BOATS) 
        value_of_stat +=GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_STOLEN_HELIS) 
        value_of_stat +=GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_STOLEN_PLANES)
        value_of_stat +=GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_STOLEN_QUADBIKES) 
        //value_of_stat +=GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_STOLEN_AUTOGYROS)
        value_of_stat +=GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_STOLEN_BICYCLES)
//      value_of_stat +=GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_STOLEN_SUBMARINES)
        value_of_stat +=GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_STOLEN_VEH_SCRIPT)
        

    RETURN  value_of_stat
ENDFUNC



FUNC WEAPON_TYPE TRACK_FAVORITE_DRIVEBY_WEAPON()

    WEAPON_TYPE current_fave_weapon
    INT current_longest_db_held_time

    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_PISTOL_DB_HELDTIME) > current_longest_db_held_time
        current_fave_weapon = WEAPONTYPE_PISTOL
        current_longest_db_held_time = GET_MP_INT_CHARACTER_STAT(MP_STAT_PISTOL_DB_HELDTIME)
    ENDIF
    
        
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_APPISTOL_DB_HELDTIME) > current_longest_db_held_time
        current_fave_weapon = WEAPONTYPE_APPISTOL
        current_longest_db_held_time = GET_MP_INT_CHARACTER_STAT(MP_STAT_APPISTOL_DB_HELDTIME)
    ENDIF
    

    
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTPISTOL_DB_HELDTIME) > current_longest_db_held_time
        current_fave_weapon = WEAPONTYPE_COMBATPISTOL
        current_longest_db_held_time = GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTPISTOL_DB_HELDTIME)
    ENDIF
    
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_MICROSMG_DB_HELDTIME) > current_longest_db_held_time
        current_fave_weapon = WEAPONTYPE_MICROSMG
        current_longest_db_held_time = GET_MP_INT_CHARACTER_STAT(MP_STAT_MICROSMG_DB_HELDTIME)
    ENDIF
    
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_DB_HELDTIME) > current_longest_db_held_time
        current_fave_weapon = WEAPONTYPE_SMG
        current_longest_db_held_time = GET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_DB_HELDTIME)
    ENDIF
    

    
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_SAWNOFF_DB_HELDTIME) > current_longest_db_held_time
        current_fave_weapon = WEAPONTYPE_SAWNOFFSHOTGUN
        current_longest_db_held_time = GET_MP_INT_CHARACTER_STAT(MP_STAT_SAWNOFF_DB_HELDTIME)
    ENDIF
    
/*  IF GET_MP_INT_CHARACTER_STAT(MP_STAT_RUBBERGUN_DB_HELDTIME) > current_longest_db_held_time
        current_fave_weapon = WEAPONTYPE_DLC_RUBBERGUN
        current_longest_db_held_time = GET_MP_INT_CHARACTER_STAT(MP_STAT_RUBBERGUN_DB_HELDTIME)
    ENDIF
    */
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_STUNGUN_DB_HELDTIME) > current_longest_db_held_time
        current_fave_weapon = WEAPONTYPE_STUNGUN
        current_longest_db_held_time = GET_MP_INT_CHARACTER_STAT(MP_STAT_STUNGUN_DB_HELDTIME)
    ENDIF
    
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_HELDTIME) > current_longest_db_held_time
        current_fave_weapon = WEAPONTYPE_GRENADE
        current_longest_db_held_time = GET_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_HELDTIME)
    ENDIF
    
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_SMKGRENADE_DB_HELDTIME) > current_longest_db_held_time
        current_fave_weapon = WEAPONTYPE_SMOKEGRENADE
        current_longest_db_held_time = GET_MP_INT_CHARACTER_STAT(MP_STAT_SMKGRENADE_DB_HELDTIME)
    ENDIF
    
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_DB_HELDTIME) > current_longest_db_held_time
        current_fave_weapon = WEAPONTYPE_STICKYBOMB
        current_longest_db_held_time = GET_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_DB_HELDTIME)
    ENDIF
    
    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_MOLOTOV_DB_HELDTIME) > current_longest_db_held_time
        current_fave_weapon = WEAPONTYPE_MOLOTOV
        current_longest_db_held_time = GET_MP_INT_CHARACTER_STAT(MP_STAT_MOLOTOV_DB_HELDTIME)
    ENDIF
    
    
    /*
    MP_STAT_SAWNOFF_DB_HELDTIME
MP_STAT_STUNGUN_DB_HELDTIME
MP_STAT_GRENADE_HELDTIME
MP_STAT_SMKGRENADE_DB_HELDTIME
MP_STAT_STKYBMB_DB_HELDTIME
MP_STAT_MOLOTOV_DB_HELDTIME
*/  
    
    RETURN current_fave_weapon


/*
MP_STAT_PISTOL_DB_HELDTIME

MP_STAT_APPISTOL_DB_HELDTIME
MP_STAT_CMBTPISTOL_DB_HELDTIME
MP_STAT_MICROSMG_DB_HELDTIME
MP_STAT_SMG_DB_HELDTIME

*/
    


ENDFUNC




// award for shotgun kills  // add assult, bullpup, sawnoff and pump
FUNC INT GET_NUMBER_OF_SHOTGUN_KILLS()
    INT Kills 

    Kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_PUMP_ENEMY_KILLS)
    Kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_SAWNOFF_ENEMY_KILLS)
    Kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTSHTGN_ENEMY_KILLS)
     Kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_BULLPUP_ENEMY_KILLS)
	   Kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_HVYSHGN_ENEMY_KILLS)
	Kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_MUSKET_ENEMY_KILLS)
	Kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_DBSHGN_ENEMY_KILLS)
	 	Kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_AUTOSHGN_ENEMY_KILLS) 
    RETURN Kills
    
ENDFUNC

FUNC INT GET_NUMBER_OF_PISTOL_KILLS()

    INT Kills 

    Kills       = GET_MP_INT_CHARACTER_STAT(MP_STAT_PISTOL_ENEMY_KILLS)
    Kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTPISTOL_ENEMY_KILLS)
    Kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_APPISTOL_ENEMY_KILLS)
    Kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_SNSPISTOL_ENEMY_KILLS )
	Kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_HVYPISTOL_ENEMY_KILLS) 
	Kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_PISTOL50_ENEMY_KILLS )
	Kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_VPISTOL_ENEMY_KILLS )
		Kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_MKPISTOL_ENEMY_KILLS )
		Kills       += GET_MP_INT_CHARACTER_STAT(MP_STAT_REVOLVER_ENEMY_KILLS)
    RETURN Kills
    
ENDFUNC

// totalling up code stats distance on bike, car, walking, boat etc
/*
PROC TRACK_TOTAL_DISTANCE()

    FLOAT total_distance_value
    total_distance_value =GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_DIST_CAR) 
    total_distance_value +=GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_DIST_PLANE)
    total_distance_value +=GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_DIST_TRAILER)
    total_distance_value +=GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_DIST_QUADBIKE)
    total_distance_value +=GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_DIST_HELI)
    total_distance_value +=GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_DIST_AUTOGYRO)
    total_distance_value +=GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_DIST_BIKE)
    total_distance_value +=GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_DIST_BICYCLE)
    total_distance_value +=GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_DIST_BOAT)
    total_distance_value +=GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_DIST_SUBMARINE)
    total_distance_value +=GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_DIST_SWIMMING)
    total_distance_value +=GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_DIST_WALKING)
    total_distance_value +=GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_DIST_AS_RUNNING)
    //total_distance_value +=GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_DIST_AS_PASSENGER_TAXI)
    //total_distance_value +=GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_DIST_AS_PASSENGER_TRAIN)


    SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_CHAR_DIST_TRAVELLED, total_distance_value)
    

ENDPROC

*/


PROC TRACKING_INCIDENT_REPONSE_TIMES(int delay_time_check)
    
    INT number_of_incidents = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MISSION_STARTED)

    INT average_response_time_value
    
    //MPGlobalsStats.iTotal_number_of_incidents // needs to be incremented by scriptor?
    
    IF MPGlobalsStats.bIsMPCharRespondingToIncident[PARTICIPANT_ID_TO_INT()] = TRUE
        // storing total time when player is responding to an incident
        //INT delay_time_check_float = CONVERT_INT_MILLISECONDS_TO_MINUTES(delay_time_check)
        MPGlobalsStats.iTotal_response_time_to_incidents +=delay_time_check
        
        average_response_time_value = (MPGlobalsStats.iTotal_response_time_to_incidents / number_of_incidents) // MP_STAT_CHAR_MISSION_STARTED)
        SET_MP_INT_CHARACTER_STAT(MP_STAT_AVERAGE_RESPONSE_TIME, average_response_time_value)

    ENDIF
    
    //storing number of incidents. // store in a case
    
ENDPROC




PROC TRACK_NUMBER_OF_DISABLES(MP_INT_STATS total_no_disables_stat,MP_INT_STATS total_no_stun_gun_hits_award, MP_INT_STATS total_no_rubberbullet_hits_award, MP_INT_STATS total_no_successful_arrests_award )


    INT total_no_disables_value = GET_MP_INT_CHARACTER_STAT (total_no_disables_stat)
    INT total_no_stun_gun_hits_value = GET_MP_INT_CHARACTER_STAT(total_no_stun_gun_hits_award)
    INT total_no_rubberbullet_hits_value = GET_MP_INT_CHARACTER_STAT(total_no_rubberbullet_hits_award)
    INT total_no_successful_arrests_value = GET_MP_INT_CHARACTER_STAT(total_no_successful_arrests_award)
    
    total_no_disables_value = (total_no_stun_gun_hits_value + total_no_rubberbullet_hits_value + total_no_successful_arrests_value)

    SET_MP_INT_CHARACTER_STAT(total_no_disables_stat, total_no_disables_value)
    
    
    //ARREST
ENDPROC


PROC SYNC_MK2_WEAPON_STAT_TO_MK1()
	
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CMBMG_MK2_KILLS		, GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTMG_KILLS))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CMBMG_MK2_DEATHS      , GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTMG_DEATHS     ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CMBMG_MK2_HEADSHOTS   , GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTMG_HEADSHOTS  ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CBNR_MK2_KILLS        , GET_MP_INT_CHARACTER_STAT(MP_STAT_CRBNRIFLE_KILLS       ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CBNR_MK2_DEATHS       , GET_MP_INT_CHARACTER_STAT(MP_STAT_CRBNRIFLE_DEATHS      ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CBNR_MK2_HEADSHOTS    , GET_MP_INT_CHARACTER_STAT(MP_STAT_CRBNRIFLE_HEADSHOTS   ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTR_MK2_KILLS       , GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTRIFLE_KILLS      ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTR_MK2_DEATHS      , GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTRIFLE_DEATHS     ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTR_MK2_HEADSHOTS   , GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTRIFLE_HEADSHOTS  ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_HVYS_MK2_KILLS        , GET_MP_INT_CHARACTER_STAT(MP_STAT_HVYSNIPER_KILLS       ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_HVYS_MK2_DEATHS       , GET_MP_INT_CHARACTER_STAT(MP_STAT_HVYSNIPER_DEATHS      ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_HVYS_MK2_HEADSHOTS    , GET_MP_INT_CHARACTER_STAT(MP_STAT_HVYSNIPER_HEADSHOTS   ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_PIST_MK2_KILLS        , GET_MP_INT_CHARACTER_STAT(MP_STAT_PISTOL_KILLS       ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_PIST_MK2_DEATHS       , GET_MP_INT_CHARACTER_STAT(MP_STAT_PISTOL_DEATHS      ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_PIST_MK2_HEADSHOTS    , GET_MP_INT_CHARACTER_STAT(MP_STAT_PISTOL_HEADSHOTS   ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_MK2_KILLS         , GET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_KILLS        ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_MK2_DEATHS        , GET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_DEATHS       ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_MK2_HEADSHOTS     , GET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_HEADSHOTS    ))

		
		SET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_MK2_SHOTS         , GET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_SHOTS        ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CMBMG_MK2_SHOTS       , GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTMG_SHOTS         ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CBNR_MK2_SHOTS        , GET_MP_INT_CHARACTER_STAT(MP_STAT_CRBNRIFLE_SHOTS         ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTR_MK2_SHOTS       , GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTRIFLE_SHOTS         ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_PIST_MK2_SHOTS    	, GET_MP_INT_CHARACTER_STAT(MP_STAT_PISTOL_SHOTS   ))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_HVYS_MK2_SHOTS    , GET_MP_INT_CHARACTER_STAT(MP_STAT_HVYSNIPER_SHOTS    ))
		
ENDPROC
    
PROC TRACK_MULTIPLE_DEATHS_STAT()

        //MPGlobalsStats.high_multiple_kills_timer1 ++
    

            IF MPGlobalsStats.high_multiple_kills_timer1 < 150 //15 seconds
                MPGlobalsStats.high_multiple_kills_timer1++
                // tracking player peds
                //PRINTSTRING("\n MULTIPLE KILL TIMER")
                //PRINTINT(MPGlobalsStats.high_multiple_kills_timer1)
                    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_KILLS) > MPGlobalsStats.total_kills_recorded1 
                        //PRINTSTRING("\n KILL STAT greater than deaths recorded \n ")
                        IF MPGlobalsStats.high_multiple_kills_timer1 < 130
                            //INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_HIGHEST_NO_MULTIPLE_KILLS)
                            MPGlobalsStats.high_multiple_kills_timer1 = 0
                            MPGlobalsStats.local_multiple_kill++
                            IF MPGlobalsStats.local_multiple_kill > GET_MP_INT_CHARACTER_STAT(MP_STAT_HIGHEST_NO_MULTIPLE_KILLS)
                                SET_MP_INT_CHARACTER_STAT(MP_STAT_HIGHEST_NO_MULTIPLE_KILLS,MPGlobalsStats.local_multiple_kill )
                            ENDIF
                            
                            MPGlobalsStats.total_kills_recorded1 = GET_MP_INT_CHARACTER_STAT(MP_STAT_KILLS) 
                            
                    
                        ENDIF
                    
                    ENDIF
                
                
                // need to track ai ped deaths.
                
                
            ELSE    
                // reset counter
                IF MPGlobalsStats.local_multiple_kill > 1
                    // check if new value is greater than record
                    IF MPGlobalsStats.local_multiple_kill > GET_MP_INT_CHARACTER_STAT(MP_STAT_HIGHEST_NO_MULTIPLE_KILLS)
                        SET_MP_INT_CHARACTER_STAT(MP_STAT_HIGHEST_NO_MULTIPLE_KILLS,MPGlobalsStats.local_multiple_kill )
                    ENDIF
                    MPGlobalsStats.local_multiple_kill = 0
                ENDIF
                //PRINTSTRING("\n reset multiple kills counter \n ")
                MPGlobalsStats.high_multiple_kills_timer1 = 0
                // equalise current kill counter
                MPGlobalsStats.total_kills_recorded1 = GET_MP_INT_CHARACTER_STAT(MP_STAT_KILLS)
            ENDIF
            


ENDPROC
    



PROC TRACK_TIME_SPENT_FLYING(int delay_time)
    IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
        IF IS_PED_IN_FLYING_VEHICLE(PLAYER_PED_ID())
            VEHICLE_INDEX veh_temp_flying_player_is_using
            veh_temp_flying_player_is_using = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
            INT stat_time_spent_flying_value = GET_MP_INT_CHARACTER_STAT(MP_STAT_TIME_SPENT_FLYING)
            
            IF NOT IS_VEHICLE_ON_ALL_WHEELS(veh_temp_flying_player_is_using)
            AND IS_ENTITY_IN_AIR(veh_temp_flying_player_is_using)
                //delay_time_int = CONVERT_INT_MILLISECONDS_TO_MINUTES(delay_time)
                stat_time_spent_flying_value +=delay_time
                SET_MP_INT_CHARACTER_STAT(MP_STAT_TIME_SPENT_FLYING, stat_time_spent_flying_value)
        
            ENDIF
            
        ENDIF
    ENDIF
ENDPROC
/*
FUNC BOOL HAS_WEAPON_BEEN_AQUIRED(WEAPON_TYPE aWeapon, INT iSlot = -1)

    SWITCH aWeapon
        CASE WEAPONTYPE_KNIFE           RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_KNIFE_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_PISTOL          RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_PISTOL_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_COMBATPISTOL    RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_COMBATPISTOL_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_APPISTOL        RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_APPISTOL_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_MICROSMG        RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_MICROSMG_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_SMG             RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_SMG_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_ASSAULTRIFLE    RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_ASSAULTRIFLE_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_CARBINERIFLE    RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_CARBINERIFLE_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_DLC_SPECIALCARBINE RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_DLC_SPECIALCARBINE_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_ADVANCEDRIFLE   RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_ADVANCEDRIFLE_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_MG              RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_MG_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_COMBATMG        RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_COMBATMG_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_PUMPSHOTGUN     RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_PUMPSHOTGUN_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_SAWNOFFSHOTGUN  RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_SAWNOFFSHOTGUN_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_ASSAULTSHOTGUN  RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_ASSAULTSHOTGUN_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_SNIPERRIFLE     RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_SNIPERRIFLE_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_HEAVYSNIPER     RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_HEAVYSNIPER_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_GRENADELAUNCHER RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_GRENADELAUNCHER_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_RPG             RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_RPG_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_MINIGUN         RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_MINIGUN_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_GRENADE         RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_GRENADE_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_SMOKEGRENADE    RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_SMOKEGRENADE_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_STICKYBOMB      RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_STICKYBOMB_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_MOLOTOV         RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_MOLOTOV_BIT_FIELD), iSlot)
        CASE WEAPONTYPE_STUNGUN         RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_STUNGUN_BIT_FIELD), iSlot)
        //CASE WEAPONTYPE_DLC_RUBBERGUN       RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_RUBBERGUN_BIT_FIELD), iSlot)
        //CASE WEAPONTYPE_DLC_LOUDHAILER      RETURN GET_MP_PACKED_BOOL_CHARACTER_STAT(MP_STAT_WEAPON_ACQUIRED_0, ENUM_TO_INT(WEAPON_LOUDHAILER_BIT_FIELD), iSlot)
ENDSWITCH
    
    RETURN FALSE


ENDFUNC
*/
PROC TRACK_PLAYER_HAS_HELD_EVERY_GUN(INT players_team)

    
    /*Stun Gun / Rubber Gun
    Pistol (Unlocked in flow during Armory Tutorial)
    SMG  
    Pump Shotgun
    Sniper Rifle
    Carbine Rifle 
    Sticky Grenade
    Assault SMG 
    Pistol .50
    Grenades 

    RPG
    Combat MG
    Heavy Sniper
    Advanced Rifle*/
    
    
    
    
    
    IF players_team = TEAM_FREEMODE
        IF GET_MP_INT_CHARACTER_STAT(MP_STAT_ADVRIFLE_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_PISTOL_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTPISTOL_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_APPISTOL_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_MICROSMG_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTSHTGN_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_PUMP_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_SAWNOFF_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTRIFLE_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_CRBNRIFLE_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_ADVRIFLE_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_SNIPERRFL_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_MG_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTMG_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_MINIGUNS_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_GRNLAUNCH_FM_AMMO_BOUGHT) > 0
        AND GET_MP_INT_CHARACTER_STAT(MP_STAT_RPG_FM_AMMO_BOUGHT) > 0
            SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_BUY_EVERY_GUN, TRUE)
            //SCRIPT_ASSERT("VAgos got all the guns")
        ENDIF
    ENDIF
        
    /*
    Pistol
    Micro SMG 
    Sticky Grenade
    Pump Shotgun
    Sniper Rifle (level)
    Assault Rifle
    Assault SMG
    Sawn Off Shotgun
    MG
    Assault Sniper
    AP Pistol
    Heavy Rifle
    Grenade Launcher
    Assault MG
    Heavy Sniper
    Minigun
    
    */
    
    
    
    /*
    Combat Pistol
Sawn off shotgun
Sticky Grenade (level)
Micro SMG
Assault Rifle
BullPup Shotgun
MG
Assault Sniper
AP Pistol
RPG
Assault Shotgun
Combat MG
Heavy Sniper
Minigun
    */
    


ENDPROC
/*
PROC TRACK_AVERAGE_TIME_ALIVE()

    FLOAT value_average_time_alive
    FLOAT char_play_time_value = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_CHAR_PLAY_TIME)
    INT char_no_deaths_value = GET_MP_INT_CHARACTER_STAT(MP_STAT_DEATHS)


    value_average_time_alive = char_play_time_value/TO_FLOAT(char_no_deaths_value)
    //IF value_average_time_alive > 1.0
    //value_average_time_alive =  CONVERT_FLOAT_MILLISECONDS_TO_MINUTES(value_average_time_alive)// already converted
    //ENDIF
    SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_AVERAGE_TIME_ALIVE,value_average_time_alive )

ENDPROC*/

PROC TRACK_AVERAGE_TIME_ON_MISSION( INT delay_time)


//Brenda's Attempt
    
    
    
    IF IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
        //PRINTLN(" \n Delay time in milliseconds = ", delay_time, "\n")
    //  INT delay_time_float_in_minutes= CONVERT_INT_MILLISECONDS_TO_MINUTES(delay_time)
        //PRINTLN(" \n Delay time in minutes = ", delay_time_float_in_minutes, "\n")
        INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_TIME_SPENT_ON_MISS, delay_time)
        INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_TOTAL_TIME_MISSION, delay_time)
        
        INT average_time_on_mission_value = GET_MP_INT_CHARACTER_STAT (MP_STAT_CHAR_TOTAL_TIME_MISSION) / GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MISSION_OVER)
        

        //SET_MP_INT_PLAYER_STAT(MPPLY_AVER_TIME_TAKEN_MISSION, average_time_on_mission_value)
        SET_MP_INT_CHARACTER_STAT(MP_STAT_AV_TIME_COMPLETE_MISSIONS,average_time_on_mission_value )
        
    
    ENDIF

ENDPROC






FUNC INT GET_MP_TOTAL_CASH_SPENT()

    INT total_cash_spent = 0
    /*
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_HAIRDOS)
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_BETAMOUNT)
    total_cash_spent += ROUND(GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_IN_CLOTHES))
    total_cash_spent += ROUND(GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_IN_STRIP_CLUBS))
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_REPAIRS)
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_CAR_MODS)
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_DRUGS)
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_HEALTHCARE)
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_TATTOOS)
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_PROSTITUTES)
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_AIRSTRIKES)
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_BACKUP_GANG)
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_HELI_STRIKE)
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_CRATE_DROP)
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_BOUNTY)
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_TAXIS)
    total_cash_spent += ROUND(GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_IN_BUYING_GUNS))
    total_cash_spent += ROUND(GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_BUYING_AMMO))
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_PROPERTY)
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_DROPPED)
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SENT_TO_PLY)
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_WEAPON_MODS)
    
    total_cash_spent += GET_MP_INT_CHARACTER_STAT (MP_STAT_MONEY_SPENT_YUM_SNACKS)  
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_HEALTH_SNACKS)
    total_cash_spent += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_EPIC_SNACKS)

    */
    
    
    
    
    RETURN(total_cash_spent)
ENDFUNC

FUNC INT GET_MP_TOTAL_CASH_EARNED()
    INT total_cash_earned
/*
    total_cash_earned += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_GAINED_BETTING)
    total_cash_earned += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_MADE_FROM_MISSIONS)
    total_cash_earned += ROUND(GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_MONEY_MADE_FROM_RANDOM_PEDS))
    
    total_cash_earned += ROUND(GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_MONEY_PICKED_UP_ON_STREET))
    total_cash_earned += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_EARNED_ON_DRUGS)
    total_cash_earned += GET_MP_INT_CHARACTER_STAT(MP_STAT_BOUNTMONEY)
    total_cash_earned += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_PICKED_FROM_CRATES)
    total_cash_earned += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_MADE_IMPORT_EXPORT)
    total_cash_earned += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_PICKED_FROM_HOLDUPS)
    total_cash_earned += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_EARNED_PROPERTY)
    total_cash_earned += GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_RECIEVED_FRM_PLY)
*/
    
    RETURN(total_cash_earned)


ENDFUNC

PROC TRACK_TOTAL_CASH_SPENT_AND_EARNED_CHARACTER()

    
    //SET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_CASH_EARNED, GET_MP_TOTAL_CASH_EARNED())
ENDPROC

PROC TRACK_TOTAL_CASH_SPENT_AND_SPENT_CHARACTER()

    
    //SET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_TOTAL_SPENT, GET_MP_TOTAL_CASH_SPENT())
ENDPROC
    /*
PROC INCREMENT_TOTAL_CASH_SPENT_AND_EARNED_PLAYER()

    IF total_cash_spent_player < GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_TOTAL_SPENT)
        INT AMOUNT_TO_ADD = GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_TOTAL_SPENT) - total_cash_spent_player
        INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_SPENT, AMOUNT_TO_ADD)
        total_cash_spent_player = GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_TOTAL_SPENT)
        #IF IS_DEBUG_BUILD
            NET_NL()NET_PRINT("INCREMENT_TOTAL_CASH_SPENT_AND_EARNED_PLAYER total_cash_spent_player  ")NET_PRINT_INT(AMOUNT_TO_ADD)
        #ENDIF
    ENDIF
    
    IF total_cash_EARNED_player < GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_CASH_EARNED)
        INT AMOUNT_TO_ADD = GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_CASH_EARNED) - total_cash_EARNED_player
        INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTAL_EARNED, AMOUNT_TO_ADD)
        total_cash_earned_player = GET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_CASH_EARNED)
        #IF IS_DEBUG_BUILD
            NET_NL()NET_PRINT("INCREMENT_TOTAL_CASH_SPENT_AND_EARNED_PLAYER total_cash_EARNED_player  ")NET_PRINT_INT(AMOUNT_TO_ADD)
        #ENDIF
    ENDIF
ENDPROC
*/
PROC INITIALISE_TOTAL_NUMBER_OF_MEDALS(INT iTeam)
    INT Amount
    INT I
    
    FOR I = 0 TO ENUM_TO_INT(MAX_NUM_MP_BOOL_AWARD)-1
        IF GET_AWARD_BOOLCHAR_TYPE(INT_TO_ENUM(MP_BOOL_AWARD, I),   iTeam) = UNLOCKTYPEENUM_MEDAL
            Amount++
        ENDIF
    ENDFOR
    FOR I = 0 TO ENUM_TO_INT(MAX_NUM_MP_INT_AWARD)-1
        IF GET_AWARD_INTCHAR_TYPE(INT_TO_ENUM(MP_INT_AWARD, I),   iTeam) = UNLOCKTYPEENUM_MEDAL
            Amount++
        ENDIF
    ENDFOR
    /*
    FOR I = 0 TO ENUM_TO_INT(MAX_NUM_MP_FLOAT_AWARD)-1
        IF GET_AWARD_FLOATCHAR_TYPE(INT_TO_ENUM(MP_FLOAT_AWARD, I),   iTeam) = UNLOCKTYPEENUM_MEDAL
            Amount++
        ENDIF
    ENDFOR
    */
    
    FOR I = 0 TO MAX_NUM_MPPLY_BOOL_AWARD-1
        IF GET_AWARD_BOOLPLY_TYPE(INT_TO_ENUM(MPPLY_BOOL_AWARD_INDEX, I),   iTeam) = UNLOCKTYPEENUM_MEDAL
            Amount++
        ENDIF
    ENDFOR
    FOR I = 0 TO MAX_NUM_MPPLY_INT_AWARD-1
        IF GET_AWARD_INTPLY_TYPE(INT_TO_ENUM(MPPLY_INT_AWARD_INDEX, I),   iTeam) = UNLOCKTYPEENUM_MEDAL
            Amount++
        ENDIF
    ENDFOR
    /*
    FOR I = 0 TO MAX_NUM_MPPLY_FLOAT_AWARD-1
        IF GET_AWARD_FLOATPLY_TYPE(INT_TO_ENUM(MPPLY_FLOAT_AWARD, I),   iTeam) = UNLOCKTYPEENUM_MEDAL
            Amount++
        ENDIF
    ENDFOR
    */
    FOR I = 0 TO MAX_NUM_MPGEN_BOOL_AWARD-1
        IF GET_AWARD_BOOLGEN_TYPE(INT_TO_ENUM(MPGEN_BOOL_AWARD, I),   iTeam) = UNLOCKTYPEENUM_MEDAL
            Amount++
        ENDIF
    ENDFOR
    FOR I = 0 TO MAX_NUM_MPPLY_INT_AWARD-1
        IF GET_AWARD_INTGEN_TYPE(INT_TO_ENUM(MPGEN_INT_AWARD, I),   iTeam) = UNLOCKTYPEENUM_MEDAL
            Amount++
        ENDIF
    ENDFOR
    FOR I = 0 TO MAX_NUM_MPPLY_FLOAT_AWARD-1
        IF GET_AWARD_FLOATGEN_TYPE(INT_TO_ENUM(MPGEN_FLOAT_AWARD, I),   iTeam) = UNLOCKTYPEENUM_MEDAL
            Amount++
        ENDIF
    ENDFOR

    
    SET_TOTAL_NUMBER_OF_MEDALS(Amount)
ENDPROC

    
    
PROC UPDATE_TOTAL_NUMBER_OF_MEDALS_AWARDED(INT& iStaggeredCheck)
    INT Amount
    INT I
    SWITCH iStaggeredCheck 
        CASE 0
            
            FOR I = 0 TO MAX_NUM_MEDALS_OF_EACH_TYPE-1
                IF GET_MP_BOOL_CHARACTER_AWARD(MPGlobalsStats.bMedals[I])
                    Amount++
                ENDIF
            ENDFOR
            SET_STAGGERED_NUMBER_OF_MEDALS_GIVEN(GET_STAGGERED_NUMBER_OF_MEDALS()+Amount)
            iStaggeredCheck++
        BREAK
        CASE 1
            FOR I = 0 TO MAX_NUM_MEDALS_OF_EACH_TYPE-1
                IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MPGlobalsStats.iMedals[I])
                    Amount++
                ENDIF
            ENDFOR
            SET_STAGGERED_NUMBER_OF_MEDALS_GIVEN(GET_STAGGERED_NUMBER_OF_MEDALS()+Amount)
            iStaggeredCheck++
        BREAK
        /*
        CASE 2
            FOR I = 0 TO MAX_NUM_MEDALS_OF_EACH_TYPE-1
                IF IS_MP_AWARD_PLATINUM_FLOATCHAR_UNLOCKED(MPGlobalsStats.fMedals[I])
                    Amount++
                ENDIF
            ENDFOR
            SET_STAGGERED_NUMBER_OF_MEDALS_GIVEN(GET_STAGGERED_NUMBER_OF_MEDALS()+Amount)
            iStaggeredCheck++
        BREAK
        CASE 3
            FOR I = 0 TO MAX_NUM_MEDALS_OF_EACH_TYPE-1
                IF IS_MP_AWARD_PLATINUM_FLOATPLY_UNLOCKED(MPGlobalsStats.fPlyMedals[I]) 
                    Amount++
                ENDIF
            ENDFOR
            SET_STAGGERED_NUMBER_OF_MEDALS_GIVEN(GET_STAGGERED_NUMBER_OF_MEDALS()+Amount)
            iStaggeredCheck++
        BREAK
        CASE 4
            FOR I = 0 TO MAX_NUM_MEDALS_OF_EACH_TYPE-1
                IF IS_MP_AWARD_PLATINUM_FLOATGEN_UNLOCKED(MPGlobalsStats.fGenMedals[I])  
                    Amount++
                ENDIF
            ENDFOR
            SET_STAGGERED_NUMBER_OF_MEDALS_GIVEN(GET_STAGGERED_NUMBER_OF_MEDALS()+Amount)
            
            SET_TOTAL_NUMBER_OF_MEDALS_GIVEN(GET_STAGGERED_NUMBER_OF_MEDALS())
            SET_STAGGERED_NUMBER_OF_MEDALS_GIVEN(0)
            iStaggeredCheck = 0
            
        BREAK   
        
        */
    ENDSWITCH


ENDPROC

    

    
PROC RUN_UNLOCKED_ALL_MEDALS_CHECK(MP_BOOL_AWARD aBool, INT iTeam, INT& iStaggeredCheck)
    
    UPDATE_TOTAL_NUMBER_OF_MEDALS_AWARDED(iStaggeredCheck)
    IF GET_TOTAL_NUMBER_OF_MEDALS_GIVEN() > 0
        IF GET_TOTAL_NUMBER_OF_MEDALS() = GET_TOTAL_NUMBER_OF_MEDALS_GIVEN()
            SET_MP_BOOL_CHARACTER_AWARD(aBool, TRUE)
        ELSE
            SET_MP_BOOL_CHARACTER_AWARD(aBool, FALSE)
        ENDIF
        CHECK_AND_GIVE_BOOL_AWARD(aBool, iteam)
    ENDIF


ENDPROC


//FUNC BOOL RUN_THIS_STAT_CHECK(INT WhichTimer, INT DelayTime, INT currentTime)
    
//  IF WhichTimer = 0
//  ENDIF
//  IF DelayTime = 0
//  ENDIF
    
//  IF MPGlobalsStats.StatTrackingTimer[WhichTimer] = -1
//      NET_NL()NET_PRINT("INITI RUN_THIS_STAT_CHECK ")NET_NL()
//      MPGlobalsStats.StatTrackingTimer[WhichTimer] = currentTime + (DelayTime-WhichTimer)
//  ELSE
        
//      IF currentTime < MPGlobalsStats.StatTrackingTimer[WhichTimer]
//          NET_NL()NET_PRINT("OH NO RADGE! I'M STUCK IN HERE!! GAAAAAAAAAAAAAAAAAHHHHHHHHHH!!!!!!!!")NET_NL()
//      ELSE
        
//          IF PlayerCounter >= GET_NUM_ACTIVE_PLAYERS()-1
//              MPGlobalsStats.StatTrackingTimer[WhichTimer] = -1
//              PlayerCounter = 0
//          ELSE
//              PlayerCounter++
//          ENDIF
        
//          NET_NL()NET_PRINT("YAY! RUN THE CHECK")NET_NL()
//          MPGlobalsStats.StatTrackingTimer[WhichTimer] = -1
        
//          RETURN TRUE
            
//      ENDIF
        
//  ENDIF
//  RETURN TRUE
            
    //RETURN FALSE
//ENDFUNC



PROC TRACK_HEIST_ACHIEVEMENTS()

	//CPRINTLN(DEBUG_ACHIEVEMENT, "[ACHIEVEMENT] - TRACK_HEIST_ACHIEVEMENTS()")
	
	// these achievements aren't supposed to pop up during cutscenes
	IF IS_CUTSCENE_PLAYING()
		EXIT
	ENDIF
	
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		EXIT
	ENDIF
	
	// B*2228556
	// In order to stop 'retroactive unlocking' for the heist trophies
	// We clear the achievement tracker flag when the achievement has been given out
	// If the player does then reconnect with a different psn account they'd have to make some progess towards this achievement
	// Before we give it out. Even though the save data says that they have it already.

	// Note: Moved the clear flag functions to inside the achievement passed checks so they don't get cleared until the achievement has been given out. (B*2246969)
	
	// check for achievement ach_52 - Be Prepared
	IF IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_H1_DONE)
		IF NOT HAS_ACHIEVEMENT_BEEN_PASSED(ENUM_TO_INT(ACHH1))
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE DONT'T HAVE APPARENTLY ACHH1 YET")
			AWARD_ACHIEVEMENT(ACHH1)
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H1_DONE, FALSE)
		ELSE
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE APPARENTLY HAVE ACHH1")
		ENDIF
	ENDIF
	
	// check for achievement ach_52 - In the name of science - Human Labs Clear and 
	IF IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_H2_DONE) 
		IF NOT HAS_ACHIEVEMENT_BEEN_PASSED(ENUM_TO_INT(ACHH2))
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE DONT'T HAVE APPARENTLY ACHH2 YET")
			AWARD_ACHIEVEMENT(ACHH2)
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H2_DONE, FALSE)
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_HUMANELAB_DONE, FALSE)
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_SERIESA_DONE, FALSE)
		ELSE
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE APPARENTLY HAVE ACHH2")
		ENDIF
	ENDIF

	// check for achievement ach_53 - Dead Presidents - Pacific Standard Clear + Fleeca Job Clear
	IF IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_H3_DONE) 
		IF NOT HAS_ACHIEVEMENT_BEEN_PASSED(ENUM_TO_INT(ACHH3))
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE DONT'T HAVE APPARENTLY ACHH3 YET")
			AWARD_ACHIEVEMENT(ACHH3)
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H3_DONE, FALSE)
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_FLECCAJOB_DONE, FALSE)
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_PACIFIC_DONE, FALSE)
		ELSE
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE APPARENTLY HAVE ACHH3")
		ENDIF
	ENDIF

	// check for achievement ach_54 - Parole Day - Prison Break Clear
	IF IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_H4_DONE) 
		IF NOT HAS_ACHIEVEMENT_BEEN_PASSED(ENUM_TO_INT(ACHH4))
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE DONT'T HAVE APPARENTLY ACHH4 YET")
			AWARD_ACHIEVEMENT(ACHH4) 
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H4_DONE, FALSE)
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_PRISON_DONE, FALSE)
		ELSE
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE APPARENTLY HAVE ACHH4")
		ENDIF
	ENDIF
	
	IF IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_H5_DONE) 
		IF NOT HAS_ACHIEVEMENT_BEEN_PASSED(ENUM_TO_INT(ACHH5))
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE DONT'T HAVE APPARENTLY ACHH5 YET")
			AWARD_ACHIEVEMENT(ACHH5) 
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H5_DONE, FALSE)
		ELSE
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE APPARENTLY HAVE ACHH5")
		ENDIF		
	ENDIF
	
	IF IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_H6_DONE) 
		IF NOT HAS_ACHIEVEMENT_BEEN_PASSED(ENUM_TO_INT(ACHH6))
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE DONT'T HAVE APPARENTLY ACHH6 YET")
			AWARD_ACHIEVEMENT(ACHH6) 
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H6_DONE, FALSE)
		ELSE
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE APPARENTLY HAVE ACHH6")
		ENDIF	
	ENDIF
	
	IF IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_H7_DONE) 
		IF NOT HAS_ACHIEVEMENT_BEEN_PASSED(ENUM_TO_INT(ACHH7))
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE DONT'T HAVE APPARENTLY ACHH7 YET")
			AWARD_ACHIEVEMENT(ACHH7) 
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H7_DONE, FALSE)
		ELSE
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE APPARENTLY HAVE ACHH7")
		ENDIF	
	ENDIF

	IF IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_H8_DONE) 
		IF NOT HAS_ACHIEVEMENT_BEEN_PASSED(ENUM_TO_INT(ACHH8))
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE DONT'T HAVE APPARENTLY ACHH8 YET")
			AWARD_ACHIEVEMENT(ACHH8) 
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H8_DONE, FALSE)
		ELSE
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE APPARENTLY HAVE ACHH8")
		ENDIF	
	ENDIF
	
	/* B*2221178 - REMOVE VALUABLE ASSET ACHIEVEMENT
	IF IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_H9_DONE) AND NOT HAS_ACHIEVEMENT_BEEN_PASSED(ENUM_TO_INT(ACHH9))
		AWARD_ACHIEVEMENT(ACHH9) 
	ENDIF
	*/
	
	IF IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_H10_DONE) 
		IF NOT HAS_ACHIEVEMENT_BEEN_PASSED(ENUM_TO_INT(ACHH10))
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE DONT'T HAVE APPARENTLY ACHH10 YET")
			AWARD_ACHIEVEMENT(ACHH10) 
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_H10_DONE, FALSE)
		ELSE
			CPRINTLN(DEBUG_ACHIEVEMENT, "WE APPARENTLY HAVE ACHH10")
		ENDIF
	ENDIF
	
	IF IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_CREW_CUT) 
		IF IS_HEIST_ACHIEVEMENT_ID_SET(HEISTACH_CREW_CUT_SANCHECK)
			IF NOT HAS_ACHIEVEMENT_BEEN_PASSED(ENUM_TO_INT(ACH46))
				CPRINTLN(DEBUG_ACHIEVEMENT, "WE DONT'T HAVE APPARENTLY ACH46 YET")
				DO_CREW_CUT_ACHIEVEMENT()
				SET_HEIST_ACHIEVEMENT_ID(HEISTACH_CREW_CUT, FALSE)
				SET_HEIST_ACHIEVEMENT_ID(HEISTACH_CREW_CUT_SANCHECK, FALSE)
			ELSE
				CPRINTLN(DEBUG_ACHIEVEMENT, "WE APPARENTLY HAVE ACH46")
			ENDIF
		ELSE
		
			// the sanity check hasn't been set both of these are supposed to be set at the same time
			// awesome that means we're carrying across from an old profile
			SCRIPT_ASSERT("OLD PROFILE CHECK!!! - HEISTACH_CREW_CUT IS SET BUT HEISTACH_CREW_CUT_SANCHECK ISN'T - THIS ASSERT IS INTENTIONAL - IF YOU SEE IT THEN THAT MEANS MY OLD PROFILE CHECK IS WORKING")
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_CREW_CUT, FALSE)
			SET_HEIST_ACHIEVEMENT_ID(HEISTACH_CREW_CUT_SANCHECK, FALSE)			
		ENDIF
	ENDIF

ENDPROC



PROC TRACK_GANGOPS_ACHIEVEMENTS()
	// These achievements aren't supposed to pop up during cutscenes or when the player doesn't have control
	IF IS_CUTSCENE_PLAYING() OR NOT IS_PLAYER_PLAYING(PLAYER_ID())
		EXIT
	ENDIF
	
	eGangOpsAchCheck i = GOPS_ACH_1_DONE
	REPEAT MAX_GOPS_ACH i
		ACHIEVEMENT_ENUM ach = GET_GANGOPS_ACHIEVEMENT_FROM_CHECKER_ENUM(i)

		IF IS_GANGOPS_ACHIEVEMENT_BIT_SET(i)
			IF NOT HAS_ACHIEVEMENT_BEEN_AWARDED(ach)
				CPRINTLN(DEBUG_ACHIEVEMENT, "[GOPS ACHIEVEMENT] - Awarding this achievement: ",DEBUG_GET_ACHIEVEMENT_TITLE(ach))
				AWARD_ACHIEVEMENT(ach)
			ELSE
				CPRINTLN(DEBUG_ACHIEVEMENT, "[GOPS ACHIEVEMENT] - Attempted to award this but we've already got it: ",DEBUG_GET_ACHIEVEMENT_TITLE(ach))
			ENDIF
			CLEAR_GANGOPS_ACHIEVEMENT_BIT(i)
		ENDIF				
	ENDREPEAT
ENDPROC
	




PROC MAINTAIN_STAT_TRACKING(INT& TimerCounter  #IF IS_DEBUG_BUILD, WIDGET_GROUP_ID& ParentWidgetGroup #ENDIF)

    
    #IF IS_DEBUG_BUILD
    #IF SCRIPT_PROFILER_ACTIVE 
    TEXT_LABEL_63 strString
    OPEN_SCRIPT_PROFILE_MARKER_GROUP("MAINTAIN_STAT_TRACKING") // MUST BE AT START OF FUNCTION
    #ENDIF
    #ENDIF
    
    
    TIME_DATATYPE  CurrentTime
    INT iTeam
    INT incrementTo
    
    iTeam = TEAM_FREEMODE //GET_STAT_CHARACTER_TEAM()

    #IF IS_DEBUG_BUILD
        IF DOES_WIDGET_GROUP_EXIST(ParentWidgetGroup)
        ENDIF
    #ENDIF

    IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED("MPAWD", MP_STATS_TEXT_SLOT)
        REQUEST_ADDITIONAL_TEXT("MPAWD", MP_STATS_TEXT_SLOT)
		//REQUEST_ADDITIONAL_TEXT_FOR_DLC("MPAWD", MP_STATS_ )
    ENDIF
    
   // IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED("MPAWD", DLC_TEXT_SLOT0) 
   		//REQUEST_ADDITIONAL_TEXT_FOR_DLC("MPAWD", DLC_TEXT_SLOT0)
   // ENDIF
    
    IF HAS_THIS_ADDITIONAL_TEXT_LOADED("MPAWD", MP_STATS_TEXT_SLOT)

        //MPPLY_FLOAT_AWARD FLOATPlayerAward
        MP_INT_AWARD INTAward
        //MP_FLOAT_AWARD FLOATAward
        MP_BOOL_AWARD BOOLAward
        MPPLY_INT_AWARD_INDEX INTPlayerAward
		
		MPPLY_BOOL_AWARD_INDEX BOOLPlayerAward
        //UNLOCKTYPEENUM WhichType
        
        INIT_STAT_VALUES()
		
		
		IF MPGlobalsHud_TitleUpdate.iAwardStaggerStage = 0
		
		SWITCH MPGlobalsHud_TitleUpdate.WhichBoolPLYAward
		
			CASE MPPLY_AWD_FLEECA_FIN_INDEX			 BOOLPlayerAward =  MPPLY_AWD_FLEECA_FIN_INDEX  CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam) BREAK
			CASE MPPLY_AWD_PRISON_FIN_INDEX             BOOLPlayerAward =  MPPLY_AWD_PRISON_FIN_INDEX  CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam) BREAK
			CASE MPPLY_AWD_HUMANE_FIN_INDEX             BOOLPlayerAward =  MPPLY_AWD_HUMANE_FIN_INDEX  CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam) BREAK
			CASE MPPLY_AWD_SERIESA_FIN_INDEX            BOOLPlayerAward =  MPPLY_AWD_SERIESA_FIN_INDEX  CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam) BREAK
			CASE MPPLY_AWD_PACIFIC_FIN_INDEX            BOOLPlayerAward =  MPPLY_AWD_PACIFIC_FIN_INDEX  CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam) BREAK
			CASE MPPLY_AWD_HST_ORDER_INDEX              BOOLPlayerAward =  MPPLY_AWD_HST_ORDER_INDEX  CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam) BREAK
			CASE MPPLY_AWD_HST_SAME_TEAM_INDEX          BOOLPlayerAward =  MPPLY_AWD_HST_SAME_TEAM_INDEX  CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam) BREAK
			CASE MPPLY_AWD_HST_ULT_CHAL_INDEX           BOOLPlayerAward =  MPPLY_AWD_HST_ULT_CHAL_INDEX  CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam) BREAK
        
			CASE MPPLY_AWD_COMPLET_HEIST_1STPER_INDEX	
			     BOOLPlayerAward =  MPPLY_AWD_COMPLET_HEIST_1STPER_INDEX
		
	           	 CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam)
	
	        BREAK   
			CASE  MPPLY_AWD_COMPLET_HEIST_MEM_INDEX
				 BOOLPlayerAward =  MPPLY_AWD_COMPLET_HEIST_MEM_INDEX
			    IF g_sMPTunables.bdisable_heist_bonus = FALSE
	           	 CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam)
			    ENDIF
	        BREAK   
			
			CASE MPPLY_AWD_GANGOPS_IAA_INDEX		 BOOLPlayerAward = MPPLY_AWD_GANGOPS_IAA_INDEX				 CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam)  break
			CASE MPPLY_AWD_GANGOPS_SUBMARINE_INDEX	 BOOLPlayerAward = MPPLY_AWD_GANGOPS_SUBMARINE_INDEX	     CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam)  break
			CASE MPPLY_AWD_GANGOPS_MISSILE_INDEX	 BOOLPlayerAward = MPPLY_AWD_GANGOPS_MISSILE_INDEX	         CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam)  break
			CASE MPPLY_AWD_GANGOPS_ALLINORDER_INDEX	 BOOLPlayerAward = MPPLY_AWD_GANGOPS_ALLINORDER_INDEX	     CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam)  break
			CASE MPPLY_AWD_GANGOPS_LOYALTY_INDEX	 BOOLPlayerAward = MPPLY_AWD_GANGOPS_LOYALTY_INDEX	         CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam)  break
			CASE MPPLY_AWD_GANGOPS_CRIMMASMD_INDEX	 BOOLPlayerAward = MPPLY_AWD_GANGOPS_CRIMMASMD_INDEX	     CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam)  break
			CASE MPPLY_AWD_GANGOPS_LOYALTY2_INDEX	 BOOLPlayerAward = MPPLY_AWD_GANGOPS_LOYALTY2_INDEX	         CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam)  break
			CASE MPPLY_AWD_GANGOPS_LOYALTY3_INDEX    BOOLPlayerAward = MPPLY_AWD_GANGOPS_LOYALTY3_INDEX          CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam)  break
			CASE MPPLY_AWD_GANGOPS_CRIMMASMD2_INDEX  BOOLPlayerAward = MPPLY_AWD_GANGOPS_CRIMMASMD2_INDEX        CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam)  break
			CASE MPPLY_AWD_GANGOPS_CRIMMASMD3_INDEX  BOOLPlayerAward = MPPLY_AWD_GANGOPS_CRIMMASMD3_INDEX        CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam)  break
			CASE MPPLY_AWD_GANGOPS_SUPPORT_INDEX	 BOOLPlayerAward = MPPLY_AWD_GANGOPS_SUPPORT_INDEX	         CHECK_AND_GIVE_PLAYER_BOOL_AWARD(BOOLPlayerAward, iteam)  break
						
		
        ENDSWITCH
		
		
		incrementTo = ENUM_TO_INT(MPGlobalsHud_TitleUpdate.WhichBoolPLYAward)
        incrementTo++
        MPGlobalsHud_TitleUpdate.WhichBoolPLYAward = INT_TO_ENUM(MPPLY_BOOL_AWARD_INDEX, incrementTo)
        IF ENUM_TO_INT(MPGlobalsHud_TitleUpdate.WhichBoolPLYAward) = MAX_NUM_MPPLY_BOOL_AWARD

            MPGlobalsHud_TitleUpdate.WhichBoolPLYAward = INT_TO_ENUM(MPPLY_BOOL_AWARD_INDEX, 0)
        ENDIF
		
        
		ELIF MPGlobalsHud_TitleUpdate.iAwardStaggerStage = 1
		
        
        SWITCH MPGlobalsHud.WhichBoolAward

            CASE MP_AWARD_BUY_EVERY_GUN
            
                IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_BUY_EVERY_GUN) =  FALSE
                    TRACK_PLAYER_HAS_HELD_EVERY_GUN(iteam)
                ENDIF
                
                BOOLAward = MP_AWARD_BUY_EVERY_GUN
                IF NOT IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(BOOLAward) 
                    IF GET_MP_BOOL_CHARACTER_AWARD(BOOLAward)
                        SET_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(BOOLAward, TRUE, iTeam)
                    ENDIF
                ELSE
                    IF NOT GET_MP_BOOL_CHARACTER_AWARD(BOOLAward)
                        SET_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(BOOLAward, FALSE, iTeam)
                    ENDIF
                ENDIF   
            
            BREAK

            
            
        
            
            //FREE MODE BOOL AWARDS
            CASE MP_AWARD_FM_GOLF_HOLE_IN_1
                BOOLAward = MP_AWARD_FM_GOLF_HOLE_IN_1
                CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
				
					
            BREAK
            
            CASE MP_AWARD_FM_SHOOTRANG_GRAN_WON
                BOOLAward = MP_AWARD_FM_SHOOTRANG_GRAN_WON
                CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
            BREAK
            
                    
            CASE MP_AWARD_FM_TENNIS_5_SET_WINS
                BOOLAward = MP_AWARD_FM_TENNIS_5_SET_WINS
                CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
            BREAK
            
            
            CASE MP_AWARD_FM_TENNIS_STASETWIN
                BOOLAward = MP_AWARD_FM_TENNIS_STASETWIN
                CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
            BREAK
            

            
            CASE MP_AWARD_FMATTGANGHQ     
                BOOLAward = MP_AWARD_FMATTGANGHQ     
                CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
                IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_GANGHIDEOUT_CLEAR) = FALSE
                    IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMATTGANGHQ)
                        SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_GANGHIDEOUT_CLEAR, TRUE)
                    ENDIF
                ENDIF
            BREAK
            

            
            CASE MP_AWARD_FM6DARTCHKOUT
                BOOLAward = MP_AWARD_FM6DARTCHKOUT   
                CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
            BREAK
            


            
	        CASE MP_AWARD_FMWINEVERYGAMEMODE 
			
	            INT land_races 
	            land_races = GET_MP_INT_CHARACTER_AWARD(MP_AWARD_RACES_WON)
	            land_races -= GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMWINAIRRACE)

	            IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMWINEVERYGAMEMODE) =  FALSE
	                IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_NO_ARMWRESTLING_WINS) > 0
	                AND GET_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_AT_DARTS) > 0
	                AND GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_TENNIS_WON) > 0
	                AND GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_DM_WINS) > 0
	                AND GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_TDM_WINS) > 0

	                AND GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_GOLF_WON) > 0
	                AND GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMWINAIRRACE) > 0
	                AND GET_MP_INT_CHARACTER_STAT(MP_STAT_HORDEWINS) > 0
	                AND GET_MP_INT_CHARACTER_STAT(MP_STAT_MCMWINS) > 0
	                AND GET_MP_INT_CHARACTER_STAT(MP_STAT_GANGHIDWINS) > 0
	                AND land_races > 0
	                AND GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_GTA_RACES_WON) > 0
	                    IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMRALLYWONDRIVE) > 0
	                    OR GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMRALLYWONNAV) > 0
	                        IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_SHOOTRANG_TG_WON) > 0
	                        OR GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_SHOOTRANG_RT_WON) > 0
	                        OR GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_SHOOTRANG_CT_WON) > 0
	                        OR GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FM_SHOOTRANG_GRAN_WON) = TRUE
	                            SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMWINEVERYGAMEMODE, TRUE)
	                        ENDIF
	                    ENDIF
	                ENDIF
	            ELSE
	                IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_WIN_EVER_MODE_ONCE) = FALSE
	                    IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMWINEVERYGAMEMODE) 
	                        SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_WIN_EVER_MODE_ONCE, TRUE)
	                    ENDIF
	                ENDIF
	            ENDIF
	        
	            
	            BOOLAward = MP_AWARD_FMWINEVERYGAMEMODE 
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
	            CHECK_NUMERO_UNO_ACHIEVEMENT()
	        BREAK       
//	        CASE MP_AWARD_FMPICKUPDLCCRATE1ST   
//				IF g_sMPTunables.bBLOCK_AWARD_SALVAGED               = FALSE
//		            BOOLAward = MP_AWARD_FMPICKUPDLCCRATE1ST 
//		            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
//		        ENDIF   
//		                            /// tshirt transfer unlock
//	            IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMPICKUPDLCCRATE1ST) = TRUE
//					IF IS_MP_TATTOO_UNLOCKED(TSHIRT_TRANS_MP_FM_LSBELLE) = FALSE
//	                 OR GET_PACKED_STAT_BOOL(PACKED_MP_TSHIRT_TRANS_MP_FM_LSBELLE) = FALSE
//						IF NOT IS_PLAYER_FEMALE()
//							UNLOCK_REWARD_TSHIRT( MP_M_FREEMODE_01  ,TSHIRT_TRANS_MP_FM_LSBELLE)
//						ELSE
//						
//							UNLOCK_REWARD_TSHIRT( MP_F_FREEMODE_01  ,TSHIRT_TRANS_MP_FM_LSBELLE)
//						ENDIF
//						SET_PACKED_STAT_BOOL(PACKED_MP_TSHIRT_TRANS_MP_FM_LSBELLE, TRUE)
//	                    SET_MP_TATTOO_UNLOCKED(TSHIRT_TRANS_MP_FM_LSBELLE, TRUE)
//	                ENDIF
//	            ENDIF
//				
//				// added for bug 2382586 KW 22/6/15
//				IF CAN_DRAW_UNLOCKS_SCALEFORM()
//					IF g_award_event_tshirt
//						//SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"UNLOCK_AWD_SHIRT", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
//						g_award_event_tshirt = FALSE
//						
//						PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS Unlocked event t shirt KW g_award_event_tshirt_TYPE =  ", ENUM_TO_INT(g_award_event_tshirt_TYPE))
//						STRING event_shirt_unlock_type
//						
//						IF g_award_event_tshirt_TYPE = COMP_TYPE_BERD
//							event_shirt_unlock_type = "UNLOCK_AWD_MASK"
//							PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS g_award_event_tshirt_TYPE = UNLOCK_AWD_MASK ")
//						ELIF g_award_event_tshirt_TYPE = COMP_TYPE_OUTFIT
//							event_shirt_unlock_type = "UNLOCK_AWD_OUTFIT"
//							PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS g_award_event_tshirt_TYPE = UNLOCK_AWD_OUTFIT ")
//						ELIF g_award_event_tshirt_TYPE = COMP_TYPE_PROPS
//							event_shirt_unlock_type = "UNLOCK_AWD_ACCES"
//							PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS g_award_event_tshirt_TYPE = COMP_TYPE_PROPS ")
//						ELSE
//							event_shirt_unlock_type = "UNLOCK_AWD_SHIRT"
//							PRINTLN(" UNLOCK_SPECIAL_EVENT_ITEMS g_award_event_tshirt_TYPE = UNLOCK_AWD_SHIRT ")
//						ENDIF
//						SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,event_shirt_unlock_type, "UNLOCK_DESC_SHIRT8", "FeedhitTshirt03","MPTshirtAwards3" )
//						g_award_event_tshirt_TYPE = COMP_TYPE_HEAD // reset type
//						
//					ENDIF
//					
//
//				 ENDIF       
//	            
//	            
//	        BREAK       
	        CASE MP_AWARD_FMWINALLRACEMODES  
	            IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMWINALLRACEMODES) = FALSE
	                IF GET_MP_INT_CHARACTER_AWARD( MP_AWARD_RACES_WON) > 0
	                AND GET_MP_INT_CHARACTER_AWARD( MP_AWARD_FM_GTA_RACES_WON) > 0  
	            //  AND GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMWINAIRRACE)
	                //AND GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMWINSEARACE)
	                    IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMRALLYWONNAV) > 0
	                    OR GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMRALLYWONDRIVE) > 0
	                        SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMWINALLRACEMODES, TRUE)
	                        
	                    ENDIF
	                ENDIF
	            ENDIF
	            
	            BOOLAward = MP_AWARD_FMWINALLRACEMODES 
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
	        BREAK       
	        CASE MP_AWARD_FMRACEWORLDRECHOLDER 
	            BOOLAward = MP_AWARD_FMRACEWORLDRECHOLDER 
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
	            IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_HOLD_WORLD_RECORD) = FALSE
	                IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMRACEWORLDRECHOLDER)
	                    SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_WORLD_RECORD, TRUE)
	                ENDIF
	            ENDIF
				
				SET_PACKED_STAT_BOOL(PACKED_MP_STAT_BOOL_PROP_FLARE_01, TRUE)
	        BREAK       






	        CASE MP_AWARD_FM25DIFITEMSCLOTHES  
	            IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FM25DIFITEMSCLOTHES) = FALSE
	                IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NO_OF_OUTFITS) >=25
	                    SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FM25DIFITEMSCLOTHES, TRUE)
	                ENDIF
	            ENDIF
	            BOOLAward = MP_AWARD_FM25DIFITEMSCLOTHES 
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
	        BREAK       
	        CASE MP_AWARD_FMFULLYMODDEDCAR    
	            BOOLAward = MP_AWARD_FMFULLYMODDEDCAR 
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
	            IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_FULL_MODDED) = FALSE
	                IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMFULLYMODDEDCAR)
	                    SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_FULL_MODDED, TRUE)
	                ENDIF
	            ENDIF
	        BREAK       
	        CASE MP_AWARD_FMWINCUSTOMRACE   
	            BOOLAward = MP_AWARD_FMWINCUSTOMRACE 
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
	        BREAK       


	        CASE MP_AWARD_FM25DIFFERENTDM   
	            BOOLAward = MP_AWARD_FM25DIFFERENTDM 
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
	        BREAK       
	        CASE MP_AWARD_FM25DIFFERENTRACES   
	            BOOLAward = MP_AWARD_FM25DIFFERENTRACES 
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
	        BREAK       
	        CASE MP_AWARD_FMMOSTKILLSGANGHIDE  
	            BOOLAward = MP_AWARD_FMMOSTKILLSGANGHIDE 
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)

	            
	        BREAK       
	        CASE MP_AWARD_FMMOSTKILLSSURVIVE  
	            BOOLAward = MP_AWARD_FMMOSTKILLSSURVIVE 
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
	            
	            IF GET_MP_BOOL_CHARACTER_AWARD( MP_AWARD_FMMOSTKILLSSURVIVE)
	                IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_17) = FALSE
	                    SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_17, TRUE)
	                ENDIF
	            ENDIF
	            
	        BREAK       

	        
	        CASE MP_AWARD_FMKILL3ANDWINGTARACE  
	            BOOLAward = MP_AWARD_FMKILL3ANDWINGTARACE 
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
	            IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_KILL_3_RACERS    ) = FALSE
	                IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMKILL3ANDWINGTARACE)
	                    SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_KILL_3_RACERS   , TRUE)
	                ENDIF
	            ENDIF
	            
	        BREAK       
	        CASE MP_AWARD_FMTATTOOALLBODYPARTS  
	            BOOLAward = MP_AWARD_FMTATTOOALLBODYPARTS 
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
	            
	            CHECK_IF_PLAYER_HAS_PERFORMED_UNIQUE_STUNT_JUMP()
	        BREAK       

	            
	        CASE MP_AWARD_FMKILLSTREAKSDM   
	            BOOLAward = MP_AWARD_FMKILLSTREAKSDM
	            
	            //IF GET_MP_BOOL_CHARACTER_AWARD(BOOLAward) = FALSE
	                CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
	            //ENDIF
	            
	            IF NOT IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_41)
	                IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMKILLSTREAKSDM)
	                    SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_41, TRUE, TRUE)
	                ENDIF
	            ENDIF
	        BREAK   
	        
	        CASE MP_AWARD_FMFURTHESTWHEELIE
	            BOOLAward    = MP_AWARD_FMFURTHESTWHEELIE
	            IF GET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMFURTHESTWHEELIE) = FALSE
	                IF GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_LONGEST_WHEELIE_DIST) >609.0 // 1000 feet
	                    SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMFURTHESTWHEELIE,  TRUE)
	                    CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam) 
	                 ENDIF
	            ENDIF
	        BREAK
			
			CASE MP_AWARD_DAILYOBJWEEKBONUS			
	            BOOLAward    = MP_AWARD_DAILYOBJWEEKBONUS
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam) 
	             
	        BREAK
			
			CASE MP_AWARD_DAILYOBJMONTHBONUS		
	            BOOLAward    = MP_AWARD_DAILYOBJMONTHBONUS
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam) 
	             
	        BREAK

			CASE MP_AWARD_DRIVELESTERCAR5MINS		
				IF g_sMPTunables.bBLOCK_AWARD_JOYRIDER               = FALSE
		            BOOLAward    = MP_AWARD_DRIVELESTERCAR5MINS

		            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam) 
		         ENDIF    
	        BREAK
	
			CASE MP_AWARD_CLUB_COORD		
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_DANCEPERFECTOWNCLUB) >= 5
		            BOOLAward    = MP_AWARD_CLUB_COORD
					IF NOT GET_MP_BOOL_CHARACTER_AWARD(BOOLAward)
						SET_MP_BOOL_CHARACTER_AWARD(BOOLAward,  TRUE)
					ENDIF
		            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam) 
		        ENDIF     
	        BREAK
	
			
			CASE MP_AWARD_CLUB_HOTSPOT		
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_NIGHTCLUB_PLAYER_APPEAR) >= 100
		            BOOLAward    = MP_AWARD_CLUB_HOTSPOT
					IF NOT GET_MP_BOOL_CHARACTER_AWARD(BOOLAward)
						SET_MP_BOOL_CHARACTER_AWARD(BOOLAward,  TRUE)
					ENDIF
		            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam) 
		       	ENDIF      
	        BREAK
	
			
			CASE MP_AWARD_CLUB_CLUBBER	
				IF GET_MP_INT_CHARACTER_STAT(MP_STAT_DANCETODIFFDJS) >= 4
		           BOOLAward    = MP_AWARD_CLUB_CLUBBER
				   IF NOT GET_MP_BOOL_CHARACTER_AWARD(BOOLAward)
					   SET_MP_BOOL_CHARACTER_AWARD(BOOLAward,  TRUE)
			           
				   ENDIF
				   CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam) 
		        ENDIF
	        BREAK
	
			
			
			
			
		#IF USE_TU_CHANGES   
        
			CASE   MP_AWARD_STORE_20_CAR_IN_GARAGES
	            BOOLAward =  MP_AWARD_STORE_20_CAR_IN_GARAGES
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
				IF IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(BOOLAward)
				AND NOT IS_DLC_AWARD_TSHIRT_UNLOCKED(DLC_SHIRT_AWARD_SHOWROOM)
					UNLOCK_A_DLC_AWARD_TSHIRT(DLC_SHIRT_AWARD_SHOWROOM)
				ENDIF	
	        BREAK     
			
			CASE  MP_AWARD_ACTIVATE_2_PERSON_KEY    			
				BOOLAward = MP_AWARD_ACTIVATE_2_PERSON_KEY
				CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
			BREAK   
			


			CASE   MP_AWARD_SPLIT_HEIST_TAKE_EVENLY
	            BOOLAward =  MP_AWARD_SPLIT_HEIST_TAKE_EVENLY
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
	        BREAK   
			
			CASE   MP_AWARD_ALL_ROLES_HEIST
	            BOOLAward =  MP_AWARD_ALL_ROLES_HEIST
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
	        BREAK   
			

			
			
			CASE   MP_AWARD_FINISH_HEIST_NO_DAMAGE
	            BOOLAward =  MP_AWARD_FINISH_HEIST_NO_DAMAGE
	            CHECK_AND_GIVE_BOOL_AWARD(BOOLAward, iteam)
								
				IF IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(BOOLAward)
				AND NOT IS_DLC_AWARD_TSHIRT_UNLOCKED(DLC_SHIRT_AWARD_CANT_TOUCH_THIS)
					UNLOCK_A_DLC_AWARD_TSHIRT(DLC_SHIRT_AWARD_CANT_TOUCH_THIS)
				ENDIF
	        BREAK  
			
		#ENDIF       
		
		
		
		
			CASE MP_AWARD_BEGINNER			
			CASE MP_AWARD_FIELD_FILLER      
			CASE MP_AWARD_ARMCHAIR_RACER    
			CASE MP_AWARD_LEARNER           
			CASE MP_AWARD_SUNDAY_DRIVER     
			CASE MP_AWARD_THE_ROOKIE        
			CASE MP_AWARD_BUMP_AND_RUN      
			CASE MP_AWARD_GEAR_HEAD         
			CASE MP_AWARD_DOOR_SLAMMER      
			CASE MP_AWARD_HOT_LAP           
			CASE MP_AWARD_ARENA_AMATEUR     
			CASE MP_AWARD_PAINT_TRADER      
			CASE MP_AWARD_SHUNTER           
			CASE MP_AWARD_JOCK              
			CASE MP_AWARD_WARRIOR           
			CASE MP_AWARD_T_BONE            
			CASE MP_AWARD_MAYHEM            
			CASE MP_AWARD_WRECKER           
			CASE MP_AWARD_CRASH_COURSE      
			CASE MP_AWARD_ARENA_LEGEND      
			CASE MP_AWARD_PEGASUS					
			CASE MP_AWARD_CONTACT_SPORT     
			CASE MP_AWARD_UNSTOPPABLE    
			CASE  MP_AWARD_FIRST_TIME1  
			CASE  MP_AWARD_FIRST_TIME2  
			CASE  MP_AWARD_FIRST_TIME3  
			CASE  MP_AWARD_FIRST_TIME4  
			CASE  MP_AWARD_FIRST_TIME5  
			CASE  MP_AWARD_FIRST_TIME6  
			CASE  MP_AWARD_ALL_IN_ORDER 
			CASE  MP_AWARD_SURVIVALIST 		
			CASE MP_AWARD_SUPPORTING_ROLE
			CASE MP_AWARD_LEADER
			CASE MP_AWARD_SCOPEOUT 	   
			CASE MP_AWARD_CREWEDUP     
			CASE MP_AWARD_MOVINGON     
			CASE MP_AWARD_PROMOCAMP    
			CASE MP_AWARD_GUNMAN 	   
			CASE MP_AWARD_SMASHNGRAB   
			CASE MP_AWARD_INPLAINSI    
			CASE MP_AWARD_UNDETECTED   
			CASE MP_AWARD_ALLROUND     
			CASE MP_AWARD_ELITETHEIF   
			CASE MP_AWARD_PRO     	   
			CASE MP_AWARD_SUPPORTACT   
			CASE MP_AWARD_SHAFTED      
			CASE MP_AWARD_COLLECTOR    
			CASE MP_AWARD_DEADEYE      
			CASE MP_AWARD_PISTOLSATDAWN
			CASE MP_AWARD_TRAFFICAVOI  
			CASE MP_AWARD_CANTCATCHBRA 
			CASE MP_AWARD_WIZHARD      
			CASE MP_AWARD_APEESCAPE    
			CASE MP_AWARD_MONKEYKIND   
			CASE MP_AWARD_AQUAAPE      
			CASE MP_AWARD_KEEPFAITH    
			CASE MP_AWARD_TRUELOVE     
			CASE MP_AWARD_NEMESIS      
			CASE MP_AWARD_FRIENDZONED    	   
			CASE MP_AWARD_KINGOFQUB3D      
			CASE MP_AWARD_QUBISM     	   
			CASE MP_AWARD_GOFOR11TH        
			CASE MP_AWARD_GODOFQUB3D    
			CASE MP_AWARD_QUIBITS 
			CASE MP_AWARD_ELEVENELEVEN    
			#IF FEATURE_COPS_N_CROOKS
			CASE MP_AWARD_GOODCOP     	
			CASE MP_AWARD_PRIZECATCH     
			CASE MP_AWARD_PROCOP     		
			CASE MP_AWARD_PROCROOK     	
			CASE MP_AWARD_CRIMEDOESNOTPAY
			CASE MP_AWARD_STARTINGOUT		
			CASE MP_AWARD_ONTHEJOB       
			CASE MP_AWARD_BESTOFBOTH     
			CASE MP_AWARD_PRESTIGE1      
			CASE MP_AWARD_PRESTIGE2      
			CASE MP_AWARD_PRESTIGE3      
			CASE MP_AWARD_PRESTIGE4      
			CASE MP_AWARD_PRESTIGE5    
			#ENDIF
			CASE MP_AWARD_INTELGATHER  		
			CASE MP_AWARD_COMPOUNDINFILT  	
			CASE MP_AWARD_LOOT_FINDER  		
			CASE MP_AWARD_MAX_DISRUPT   		
			CASE MP_AWARD_THE_ISLAND_HEIST    
			CASE MP_AWARD_GOING_ALONE         
			CASE MP_AWARD_TEAM_WORK         	
			CASE MP_AWARD_MIXING_UP     		
			CASE MP_AWARD_PRO_THIEF     		
			CASE MP_AWARD_CAT_BURGLAR  		
			CASE MP_AWARD_ONE_OF_THEM        	
			CASE MP_AWARD_GOLDEN_GUN      	
			CASE MP_AWARD_ELITE_THIEF     	
			CASE MP_AWARD_PROFESSIONAL    	
			CASE MP_AWARD_HELPING_OUT  		
			CASE MP_AWARD_COURIER  			
			CASE MP_AWARD_PARTY_VIBES 
			CASE MP_AWARD_HELPING_HAND
   			CASE MP_AWARD_CAR_CLUB			
		
			CASE MP_AWARD_PRO_CAR_EXPORT	
			CASE MP_AWARD_UNION_DEPOSITORY	
			CASE MP_AWARD_MILITARY_CONVOY	
			CASE MP_AWARD_FLEECA_BANK		
			CASE MP_AWARD_FREIGHT_TRAIN		
			CASE MP_AWARD_BOLINGBROKE_ASS	
			CASE MP_AWARD_IAA_RAID			
			CASE MP_AWARD_METH_JOB			
			CASE MP_AWARD_BUNKER_RAID		
			CASE MP_AWARD_STRAIGHT_TO_VIDEO 
			CASE MP_AWARD_MONKEY_C_MONKEY_DO
			CASE MP_AWARD_TRAINED_TO_KILL  	
			CASE MP_AWARD_DIRECTOR  		
			#IF FEATURE_FIXER
			CASE MP_AWARD_TEEING_OFF    	
			CASE MP_AWARD_PARTY_NIGHT 		
			CASE MP_AWARD_BILLIONAIRE_GAMES 
			CASE MP_AWARD_HOOD_PASS   		
			CASE MP_AWARD_STUDIO_TOUR 		
			CASE MP_AWARD_DONT_MESS_DRE   	
			CASE MP_AWARD_BACKUP  			
			CASE MP_AWARD_SHORTFRANK_1  	
			CASE MP_AWARD_SHORTFRANK_2  	
			CASE MP_AWARD_SHORTFRANK_3  	
			CASE MP_AWARD_CONTR_KILLER		
			CASE MP_AWARD_DOGS_BEST_FRIEND	
			CASE MP_AWARD_MUSIC_STUDIO   	
			CASE MP_AWARD_SHORTLAMAR_1  	
			CASE MP_AWARD_SHORTLAMAR_3 		
			CASE MP_AWARD_SHORTLAMAR_2   	
			#ENDIF 

		 		CHECK_AND_GIVE_BOOL_AWARD(MPGlobalsHud.WhichBoolAward, iteam)
			BREAK
			
			
	ENDSWITCH
        #IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE 
            strString = "WhichBoolAward = "
            strString += ENUM_TO_INT(MPGlobalsHud.WhichBoolAward)
            ADD_SCRIPT_PROFILE_MARKER(strString)
        #ENDIF
        #ENDIF
        
        incrementTo = ENUM_TO_INT(MPGlobalsHud.WhichBoolAward)
        incrementTo++
        MPGlobalsHud.WhichBoolAward = INT_TO_ENUM(MP_BOOL_AWARD, incrementTo)
        IF MPGlobalsHud.WhichBoolAward = MAX_NUM_MP_BOOL_AWARD
            MPGlobalsHud.WhichBoolAward = INT_TO_ENUM(MP_BOOL_AWARD, 0)
        ENDIF
        
        
    	ELIF MPGlobalsHud_TitleUpdate.iAwardStaggerStage = 2  
		
        SWITCH MPGlobalsHud.WhichIntAward
        
            CASE MP_AWARD_FMSHOOTDOWNCOPHELI    
                INTAward = MP_AWARD_FMSHOOTDOWNCOPHELI 
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK   
            
            
            CASE MP_AWARD_FMTIME5STARWANTED
                //CHAR_WANTED_LEVEL_TIME
                IF GET_TOTAL_NUMBER_OF_MINUTES_FOR_MP_UNSIGNED_INT_STAT(MP_STAT_CHAR_WANTED_LEVEL_TIME5STAR) != GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMTIME5STARWANTED)
                    SET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMTIME5STARWANTED, GET_TOTAL_NUMBER_OF_MINUTES_FOR_MP_UNSIGNED_INT_STAT(MP_STAT_CHAR_WANTED_LEVEL_TIME5STAR))                   
                	//SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_TOTAL_TIME_MAX_STARS, TO_FLOAT(GET_TOTAL_NUMBER_OF_MINUTES_FOR_MP_UNSIGNED_INT_STAT(MP_STAT_CHAR_WANTED_LEVEL_TIME5STAR)))
				ENDIF
                INTAward     = MP_AWARD_FMTIME5STARWANTED
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                    
            BREAK
            

            
    
            
            CASE MP_AWARD_FMMOSTFLIPSINONEVEHICLE
                IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMMOSTFLIPSINONEVEHICLE) !=GET_MP_INT_CHARACTER_STAT(MP_STAT_MOST_FLIPS_IN_ONE_JUMP)
                    SET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMMOSTFLIPSINONEVEHICLE, GET_MP_INT_CHARACTER_STAT(MP_STAT_MOST_FLIPS_IN_ONE_JUMP))
                ENDIF
                INTAward     = MP_AWARD_FMMOSTFLIPSINONEVEHICLE
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                    
            BREAK
            
            
            CASE MP_AWARD_FMMOSTSPINSINONEVEHICLE
                IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMMOSTSPINSINONEVEHICLE) != GET_MP_INT_CHARACTER_STAT(MP_STAT_MOST_SPINS_IN_ONE_JUMP)
                    SET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMMOSTSPINSINONEVEHICLE, GET_MP_INT_CHARACTER_STAT(MP_STAT_MOST_SPINS_IN_ONE_JUMP))
                ENDIF
                INTAward     = MP_AWARD_FMMOSTSPINSINONEVEHICLE
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                    
            BREAK
            
                
            
            
        
        
            CASE MP_AWARD_PASSENGERTIME
                INTAward     = MP_AWARD_PASSENGERTIME
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                    
            BREAK
            
            
            CASE MP_AWARD_FMDRIVEWITHOUTCRASH
                //SET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMDRIVEWITHOUTCRASH, CONVERT_FLOAT_MILLISECONDS_TO_INT_MINUTES( GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_LONGEST_DRIVE_NOCRASH)))
                CurrentTime = GET_NETWORK_TIME()  
                TRACK_DRIVE_WITHOUT_CRASHING_AWARD(GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastAward_CharFloat_Timer[7]))
                MPGlobalsStats.LastAward_CharFloat_Timer[7] = GET_NETWORK_TIME() 
                
                INTAward     = MP_AWARD_FMDRIVEWITHOUTCRASH
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                    
            BREAK
            
            CASE MP_AWARD_TIME_IN_HELICOPTER
                CurrentTime = GET_NETWORK_TIME() 

                TRACK_TIME_IN_HELI(GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastAward_CharFloat_Timer[3]))

                MPGlobalsStats.LastAward_CharFloat_Timer[3] = GET_NETWORK_TIME() 
                
        
                INTAward     = MP_AWARD_TIME_IN_HELICOPTER
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                    
            BREAK
        
        
        

        
            CASE MP_AWARD_RACES_WON
                
                TRACK_RACE_WIN_PERCENTAGE(MP_STAT_RACES_WON, MP_STAT_RACE_START, MP_STAT_RACES_WON_PC  )
                    
                IF NOT g_b_OnLeaderboard
                    INTAward = MP_AWARD_RACES_WON 
                    
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                
                
                    
                    
                    SWITCH iTeam
                    
                        CASE TEAM_FREEMODE
                    
                            IF NOT IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_RACES_WON) //As there's only one Tattoo it's give at the highest. 
                                IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_RACES_WON), 50)
                                    SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_RACES_WON, TRUE)
                                ENDIF
                            ELSE
                                IF NOT IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_RACES_WON), 50)
                                    SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_RACES_WON, FALSE)
                                ENDIF
                            ENDIF
                        BREAK
                    
    
                        
                    ENDSWITCH
                
                ENDIF
            BREAK
            
        

        
            CASE MP_AWARD_VEHICLE_JUMP_OVER_40M
                INTAward     = MP_AWARD_VEHICLE_JUMP_OVER_40M
                    
                IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_VEHICLE_JUMP_OVER_40M) != GET_MP_INT_CHARACTER_STAT(MP_STAT_AIR_LAUNCHES_OVER_40M)
                    SET_MP_INT_CHARACTER_AWARD(MP_AWARD_VEHICLE_JUMP_OVER_40M, GET_MP_INT_CHARACTER_STAT(MP_STAT_AIR_LAUNCHES_OVER_40M))
                ENDIF
                
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                
            BREAK
            
            
        
            

            
        
        
            CASE MP_AWARD_5STAR_WANTED_AVOIDANCE
            
                INTAward     = MP_AWARD_5STAR_WANTED_AVOIDANCE
                    
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                
            
                IF ITEAM =  TEAM_FREEMODE
                    /*IF NOT IS_MP_TATTOO_UNLOCKED(TATTOO_MP_VAG_UNCUFF_GANG_MEMBERS_x50)
                        IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_5STAR_WANTED_AVOIDANCE), 1)
                            SET_MP_TATTOO_UNLOCKED(TATTOO_MP_VAG_UNCUFF_GANG_MEMBERS_x50, TRUE)
                        ENDIF
                    ELSE
                        IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_5STAR_WANTED_AVOIDANCE), 1) = FALSE
                            SET_MP_TATTOO_UNLOCKED(TATTOO_MP_VAG_UNCUFF_GANG_MEMBERS_x50, FALSE)
                        ENDIF
                    ENDIF*/
                ENDIF
				
				
            	
				IF NOT 	GET_PACKED_STAT_BOOL(PACKED_MP_RELOAD_VEHICLES_AFTER_MIGRATION)
					IF IS_LAST_GEN_GTAO_CHARACTERS() // fix for 2103238
						SET_PACKED_STAT_BOOL(PACKED_MP_RELOAD_VEHICLES_AFTER_MIGRATION, TRUE)
						g_RUN_FM_INITIAL_CARMOD_UNLOCK_FM_CHECK = TRUE
					ENDIF
				ENDIF
            
            BREAK
        
            
            CASE MP_AWARD_CARS_EXPORTED
            
            
                INTAward     = MP_AWARD_CARS_EXPORTED
                    
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            
            BREAK
        
            CASE MP_AWARD_100_HEADSHOTS
            
            
                
            
                    INTAward     = MP_AWARD_100_HEADSHOTS
                    IF MPGlobalsStats.temp_integer_for_maintain_stat_HEADSHOTS!=GET_MP_INT_CHARACTER_STAT(MP_STAT_PLAYER_HEADSHOTS)
                        SET_MP_INT_CHARACTER_AWARD(INTAward, GET_MP_INT_CHARACTER_STAT(MP_STAT_PLAYER_HEADSHOTS))
                        MPGlobalsStats.temp_integer_for_maintain_stat_HEADSHOTS =GET_MP_INT_CHARACTER_AWARD(MP_AWARD_100_HEADSHOTS)
                    ENDIF
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                    
                    IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_HEAD_BANGER) = FALSE
                        IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_100_HEADSHOTS) >= 500
                            SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HEAD_BANGER, TRUE)
                        ENDIF
                    ENDIF
                
                    
                
            
            
            BREAK
        
        
            CASE MP_AWARD_20_KILLS_MELEE
                
            
                    INTAward    = MP_AWARD_20_KILLS_MELEE
                    
                    
                    IF MPGlobalsStats.temp_integer_for_maintain_stat_MELEE !=GET_NUMBER_OF_MELEE_KILLS() 
                        MPGlobalsStats.temp_integer_for_maintain_stat_MELEE =GET_NUMBER_OF_MELEE_KILLS() 
                        SET_MP_INT_CHARACTER_AWARD(INTAward, GET_NUMBER_OF_MELEE_KILLS())
                    ENDIF
                    
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                    
                
                    
                
            BREAK
            
            CASE MP_AWARD_25_KILLS_STICKYBOMBS
            
        
                    INTAward     = MP_AWARD_25_KILLS_STICKYBOMBS
                    IF MPGlobalsStats.temp_integer_for_maintain_stat_STICKYBOMB !=  GET_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_ENEMY_KILLS)
                        SET_MP_INT_CHARACTER_AWARD(INTAward, GET_MP_INT_CHARACTER_STAT(MP_STAT_STKYBMB_ENEMY_KILLS))
                        MPGlobalsStats.temp_integer_for_maintain_stat_STICKYBOMB = GET_MP_INT_CHARACTER_AWARD(INTAward)
                    ENDIF
                    
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                
                
            
            BREAK
            
            CASE MP_AWARD_50_KILLS_ROCKETLAUNCH
                    INTAward     = MP_AWARD_50_KILLS_ROCKETLAUNCH
                    IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_50_KILLS_ROCKETLAUNCH) != GET_MP_INT_CHARACTER_STAT(MP_STAT_RPG_ENEMY_KILLS) 
                        
                        SET_MP_INT_CHARACTER_AWARD(MP_AWARD_50_KILLS_ROCKETLAUNCH, GET_MP_INT_CHARACTER_STAT(MP_STAT_RPG_ENEMY_KILLS))
                    ENDIF
                        
                    
                    
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            
            
            
            
            BREAK
        
            CASE MP_AWARD_100_KILLS_SMG
        
                    INTAward     = MP_AWARD_100_KILLS_SMG
                    
                
                    // tracking SMG awards. (TOTAL ASSAULT, MICRO and SMG )
                    
                    IF MPGlobalsStats.temp_integer_for_maintain_stat_SMG !=GET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_ENEMY_KILLS) +  GET_MP_INT_CHARACTER_STAT(MP_STAT_MICROSMG_ENEMY_KILLS) +  GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTSMG_ENEMY_KILLS) +  GET_MP_INT_CHARACTER_STAT(MP_STAT_COMBATPDW_ENEMY_KILLS) + GET_MP_INT_CHARACTER_STAT(MP_STAT_MCHPIST_ENEMY_KILLS) + GET_MP_INT_CHARACTER_STAT(MP_STAT_MINISMG_ENEMY_KILLS)
                        MPGlobalsStats.temp_integer_for_maintain_stat_SMG = GET_MP_INT_CHARACTER_STAT(MP_STAT_SMG_ENEMY_KILLS) +  GET_MP_INT_CHARACTER_STAT(MP_STAT_MICROSMG_ENEMY_KILLS) +  GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTSMG_ENEMY_KILLS) +  GET_MP_INT_CHARACTER_STAT(MP_STAT_COMBATPDW_ENEMY_KILLS) + GET_MP_INT_CHARACTER_STAT(MP_STAT_MCHPIST_ENEMY_KILLS)  + GET_MP_INT_CHARACTER_STAT(MP_STAT_MINISMG_ENEMY_KILLS)
						
			        	SET_MP_INT_CHARACTER_AWARD(INTAward, MPGlobalsStats.temp_integer_for_maintain_stat_SMG)
                    ENDIF
                    /////
                    
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                    
                

            BREAK
            
            CASE MP_AWARD_50_KILLS_GRENADES
            
            
            		
                    INTAward     = MP_AWARD_50_KILLS_GRENADES
                    IF GET_MP_INT_CHARACTER_AWARD(INTAward) != GET_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_ENEMY_KILLS)
                        SET_MP_INT_CHARACTER_AWARD(INTAward, GET_MP_INT_CHARACTER_STAT(MP_STAT_GRENADE_ENEMY_KILLS))
                    ENDIF   
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
        
            BREAK
        
    
            

        
        

        
        
            CASE MP_AWARD_LAPDANCES

                    IF MPGlobalsStats.temp_integer_for_maintain_stat_LAPDANCES !=GET_MP_INT_CHARACTER_STAT(MP_STAT_LAP_DANCED_BOUGHT)
                        SET_MP_INT_CHARACTER_AWARD(MP_AWARD_LAPDANCES, GET_MP_INT_CHARACTER_STAT(MP_STAT_LAP_DANCED_BOUGHT))
                        MPGlobalsStats.temp_integer_for_maintain_stat_LAPDANCES =GET_MP_INT_CHARACTER_STAT(MP_STAT_LAP_DANCED_BOUGHT)
                    ENDIF
                    INTAward     = MP_AWARD_LAPDANCES
                        
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                    

                    IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_24) = FALSE
                        IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_LAPDANCES), GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_LAPDANCES, AWARDPOSITIONS_PLATINUM, iTeam)) //30 Minutes
                            SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_24, TRUE)
                        ENDIF
                    ENDIF
                    
                    
            BREAK
            
            
            
            
            CASE MP_AWARD_100_KILLS_SNIPER
            
    
                    INTAward     = MP_AWARD_100_KILLS_SNIPER
                    IF  GET_MP_INT_CHARACTER_AWARD(INTAward) != GET_NUMBER_OF_SNIPER_KILLS()
                        SET_MP_INT_CHARACTER_AWARD(INTAward, GET_NUMBER_OF_SNIPER_KILLS())
                    ENDIF
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                    
                    IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_40) = FALSE
                        IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_100_KILLS_SNIPER), GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_100_KILLS_SNIPER, AWARDPOSITIONS_PLATINUM, iTeam)) //30 Minutes
                            SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_40, TRUE)
                        ENDIF
                    ENDIF
                    
            
            BREAK
            
            
            CASE MP_AWARD_100_KILLS_SHOTGUN
        
                    INTAward     = MP_AWARD_100_KILLS_SHOTGUN
                    IF GET_MP_INT_CHARACTER_AWARD(INTAward) != GET_NUMBER_OF_SHOTGUN_KILLS()
                        SET_MP_INT_CHARACTER_AWARD(INTAward, GET_NUMBER_OF_SHOTGUN_KILLS())
                    ENDIF
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                

            BREAK
            
            
            CASE MP_AWARD_50_VEHICLES_BLOWNUP
                    INTAward     = MP_AWARD_50_VEHICLES_BLOWNUP
                    IF  MPGlobalsStats.temp_integer_for_maintain_stat_50_VEHICLES_BLOWNUP!=GET_NUMBER_OF_VEHICLES_BLOWN_UP()
                        SET_MP_INT_CHARACTER_AWARD(INTAward, GET_NUMBER_OF_VEHICLES_BLOWN_UP())
                        MPGlobalsStats.temp_integer_for_maintain_stat_50_VEHICLES_BLOWNUP =GET_NUMBER_OF_VEHICLES_BLOWN_UP()
                    ENDIF
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
            
            CASE MP_AWARD_VEHICLES_JACKEDR
            
                    INTAward     = MP_AWARD_VEHICLES_JACKEDR
                    
                    IF MPGlobalsStats.temp_integer_for_maintain_stat_VEHICLES_JACKEDR !=GET_NUMBER_OF_VEHICLES_JACKED()
                        SET_MP_INT_CHARACTER_AWARD(INTAward, GET_NUMBER_OF_VEHICLES_JACKED())
                        MPGlobalsStats.temp_integer_for_maintain_stat_VEHICLES_JACKEDR =GET_NUMBER_OF_VEHICLES_JACKED()
                    ENDIF
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                
            
            BREAK
            
            
            
            CASE MP_AWARD_ENEMYDRIVEBYKILLS
                    IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_ENEMYDRIVEBYKILLS) != GET_MP_INT_CHARACTER_STAT(MP_STAT_DB_PLAYER_KILLS)+GET_MP_INT_CHARACTER_STAT(MP_STAT_PASS_DB_PLAYER_KILLS)

                        SET_MP_INT_CHARACTER_AWARD(MP_AWARD_ENEMYDRIVEBYKILLS, (GET_MP_INT_CHARACTER_STAT(MP_STAT_DB_PLAYER_KILLS)+GET_MP_INT_CHARACTER_STAT(MP_STAT_PASS_DB_PLAYER_KILLS)))
                    ENDIF
                    INTAward     = MP_AWARD_ENEMYDRIVEBYKILLS
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                
            
            BREAK
            
            
            
            
        
//            CASE MP_AWARD_SECURITY_CARS_ROBBED
//            
//                // security vans robbed being tracked in securityvan.sc ambient script
//                
//					IF g_sMPTunables.bBLOCK_AWARD_FULLMETALJACKET		= FALSE
//				        INTAward = MP_AWARD_SECURITY_CARS_ROBBED
//				        CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
//				            
//				        IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_ARMOURED_VAN_TAKEDOWN) = FALSE
//				            IF GET_MP_INT_CHARACTER_AWARD(INTAward) >= GET_AWARD_INTCHAR_LEVEL_NUMBER(INTAward, AWARDPOSITIONS_PLATINUM, iTeam)
//								SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_ARMOURED_VAN_TAKEDOWN, TRUE)
//				            ENDIF
//				        ENDIF
//					ENDIF
//            
//            BREAK
            
            CASE MP_AWARD_100_KILLS_PISTOL
            
                INTAward = MP_AWARD_100_KILLS_PISTOL
                    IF GET_MP_INT_CHARACTER_AWARD(INTAward) != GET_NUMBER_OF_PISTOL_KILLS()
                        SET_MP_INT_CHARACTER_AWARD(INTAward, GET_NUMBER_OF_PISTOL_KILLS())
                    ENDIF
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                
                IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_37) = FALSE
                    IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_100_KILLS_PISTOL), GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_100_KILLS_PISTOL, AWARDPOSITIONS_PLATINUM, iTeam)) //30 Minutes
                        SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_37, TRUE)
                    ENDIF
                ENDIF
            BREAK
            
            
            
            
            CASE MP_AWARD_NO_ARMWRESTLING_WINS
            
                STORE_STAT_VALUE_INTO_AWARD_VALUE(MP_STAT_MOST_ARM_WRESTLING_WINS, MP_AWARD_NO_ARMWRESTLING_WINS)
                
                INTAward     = MP_AWARD_NO_ARMWRESTLING_WINS    
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            
            BREAK
            

            
            
            CASE MP_AWARD_HOLD_UP_SHOPS
            
                SWITCH iTeam
                    CASE TEAM_FREEMODE
                        INTAward = MP_AWARD_HOLD_UP_SHOPS 
                        CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                        
                        IF NOT IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_UP_SHOPS_1)
                            IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS), 5)
                                SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_UP_SHOPS_1, TRUE)
                            ENDIF
                        ELSE
                            IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS), 5) = FALSE
                                SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_UP_SHOPS_1, FALSE)
                            ENDIF
                        ENDIF
                        
                        IF NOT IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_UP_SHOPS_2)
                            IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS), 10)
                                SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_UP_SHOPS_2, TRUE)
                                NET_NL()NET_PRINT(" IN NET_STAT_TRACKING SET_MP_TATTOO_UNLOCKED CALLED1  ")  NET_NL()
                            ENDIF
                        ELSE
                            IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS), 10) = FALSE
                                SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_UP_SHOPS_2, FALSE)
                                NET_NL()NET_PRINT(" IN NET_STAT_TRACKING SET_MP_TATTOO_UNLOCKED CALLED2  ")  NET_NL()
                            ENDIF
                        ENDIF
                        IF NOT IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_UP_SHOPS_3) 
                            IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS), 15)
                                SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_UP_SHOPS_3, TRUE)
                            ENDIF
                        ELSE
                            IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS), 15) = FALSE
                                SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_UP_SHOPS_3, FALSE)
                            ENDIF
                        ENDIF
                        IF NOT IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_UP_SHOPS_4) //Typo, should be old value 19. 
                            IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS), 20)
                                SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_UP_SHOPS_4, TRUE)
                            ENDIF
                        ELSE
                            IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS), 20) = FALSE
                                SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HOLD_UP_SHOPS_4, FALSE)
                            ENDIF
                        ENDIF
                        
                    /*
                        IF NOT IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_0)
                            IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS), 1)
                                SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_0, TRUE)
                            ENDIF
                        ELSE
                            IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS), 1) = FALSE
                                SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_0, FALSE)
                            ENDIF
                        ENDIF
                        
                        IF NOT IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_1)
                            IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS), 5)
                                SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_1, TRUE)
                                NET_NL()NET_PRINT(" IN NET_STAT_TRACKING SET_MP_TATTOO_UNLOCKED CALLED1  ")  NET_NL()
                            ENDIF
                        ELSE
                            IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS), 5) = FALSE
                                SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_1, FALSE)
                                NET_NL()NET_PRINT(" IN NET_STAT_TRACKING SET_MP_TATTOO_UNLOCKED CALLED2  ")  NET_NL()
                            ENDIF
                        ENDIF
                        IF NOT IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_2) 
                            IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS), 10)
                                SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_2, TRUE)
                            ENDIF
                        ELSE
                            IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS), 10) = FALSE
                                SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_2, FALSE)
                            ENDIF
                        ENDIF
                        IF NOT IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_3) //Typo, should be old value 19. 
                            IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS), 20)
                                SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_3, TRUE)
                            ENDIF
                        ELSE
                            IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_HOLD_UP_SHOPS), 20) = FALSE
                                SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_3, FALSE)
                            ENDIF
                        ENDIF*/
                        //STORE_STAT_VALUE_INTO_AWARD_VALUE(MP_STAT_CHEAT_MISSION_PASSED, INTAward)
                        

                        
                    
                    BREAK
                    
                    
                    
                ENDSWITCH
                    
            
            BREAK
            

        
            
            
            CASE MP_AWARD_WIN_AT_DARTS
                INTAward = MP_AWARD_WIN_AT_DARTS
                //SET_MP_INT_CHARACTER_AWARD(MP_AWARD_WIN_AT_DARTS, GET_MP_INT_CHARACTER_STAT(MP_STAT_DARTWINS))
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
            
            CASE MP_AWARD_CAR_BOMBS_ENEMY_KILLS
                INTAward = MP_AWARD_CAR_BOMBS_ENEMY_KILLS

                IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_FMKILLCHEATER) = FALSE
                    IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_CAR_BOMBS_ENEMY_KILLS) >= GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_CAR_BOMBS_ENEMY_KILLS, AWARDPOSITIONS_PLATINUM, iTeam)     
                        SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_FMKILLCHEATER, TRUE)
                    ENDIF
                ENDIF
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
            

                
            CASE MP_AWARD_KILLS_ASSAULT_RIFLE
                IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_KILLS_ASSAULT_RIFLE) != GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTRIFLE_ENEMY_KILLS) + GET_MP_INT_CHARACTER_STAT(MP_STAT_CRBNRIFLE_ENEMY_KILLS) + GET_MP_INT_CHARACTER_STAT(MP_STAT_ADVRIFLE_ENEMY_KILLS) + GET_MP_INT_CHARACTER_STAT(MP_STAT_SPCARBINE_ENEMY_KILLS) + GET_MP_INT_CHARACTER_STAT(MP_STAT_BULLRIFLE_ENEMY_KILLS) + GET_MP_INT_CHARACTER_STAT(MP_STAT_CMPRIFLE_ENEMY_KILLS)
                    SET_MP_INT_CHARACTER_AWARD(MP_AWARD_KILLS_ASSAULT_RIFLE,(GET_MP_INT_CHARACTER_STAT(MP_STAT_ASLTRIFLE_ENEMY_KILLS)+ GET_MP_INT_CHARACTER_STAT(MP_STAT_CRBNRIFLE_ENEMY_KILLS) + GET_MP_INT_CHARACTER_STAT(MP_STAT_ADVRIFLE_ENEMY_KILLS) + GET_MP_INT_CHARACTER_STAT(MP_STAT_SPCARBINE_ENEMY_KILLS)+ GET_MP_INT_CHARACTER_STAT(MP_STAT_BULLRIFLE_ENEMY_KILLS)+ GET_MP_INT_CHARACTER_STAT(MP_STAT_CMPRIFLE_ENEMY_KILLS)) )
                ENDIF
                INTAward = MP_AWARD_KILLS_ASSAULT_RIFLE
                
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
                        
            CASE MP_AWARD_KILLS_MACHINEGUN
            
                IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_KILLS_MACHINEGUN)!= GET_MP_INT_CHARACTER_STAT(MP_STAT_MG_ENEMY_KILLS) + GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTMG_ENEMY_KILLS) + GET_MP_INT_CHARACTER_STAT(MP_STAT_GUSNBRG_ENEMY_KILLS)
                    SET_MP_INT_CHARACTER_AWARD(MP_AWARD_KILLS_MACHINEGUN,(GET_MP_INT_CHARACTER_STAT(MP_STAT_MG_ENEMY_KILLS) + GET_MP_INT_CHARACTER_STAT(MP_STAT_CMBTMG_ENEMY_KILLS)+ GET_MP_INT_CHARACTER_STAT(MP_STAT_GUSNBRG_ENEMY_KILLS)) )
               		//SET_MP_TATTOO_UNLOCKED(TSHIRT_TRANS_MP_FM_ROCKSTAR, TRUE)
					//NET_PRINT("\n SET_MP_TATTOO_UNLOCKED(TSHIRT_TRANS_MP_FM_ROCKSTAR, TRUE) ")
				ENDIF
                
                INTAward = MP_AWARD_KILLS_MACHINEGUN

                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
            
            /*CASE MP_AWARD_REPLENISH_AMMO_SAFEHS
                INTAward = MP_AWARD_REPLENISH_AMMO_SAFEHS

                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
                    */
            
            CASE MP_AWARD_NO_HAIRCUTS
                INTAward = MP_AWARD_NO_HAIRCUTS
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
            
            
            // SUPRESS DURING FM BASEJUMP
            CASE MP_AWARD_PARACHUTE_JUMPS_50M
                INTAward = MP_AWARD_PARACHUTE_JUMPS_50M
                // SUPRESS DURING FM BASEJUMP
                IF g_b_On_BASEJUMP_RACE = FALSE
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                ENDIF
            BREAK
            
            CASE MP_AWARD_PARACHUTE_JUMPS_20M
                INTAward = MP_AWARD_PARACHUTE_JUMPS_20M
                // SUPRESS DURING FM BASEJUMP
                IF g_b_On_BASEJUMP_RACE = FALSE
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                ENDIF
            BREAK
            
            
            
            // Freemode awards
            CASE MP_AWARD_FM_GOLF_BIRDIES
                INTAward = MP_AWARD_FM_GOLF_BIRDIES
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
            CASE MP_AWARD_FM_GOLF_WON 
                INTAward = MP_AWARD_FM_GOLF_WON 
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
            CASE MP_AWARD_FM_SHOOTRANG_TG_WON  
                INTAward = MP_AWARD_FM_SHOOTRANG_TG_WON  
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
            CASE MP_AWARD_FM_SHOOTRANG_RT_WON  
                INTAward = MP_AWARD_FM_SHOOTRANG_RT_WON  
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
            CASE MP_AWARD_FM_SHOOTRANG_CT_WON  
                INTAward = MP_AWARD_FM_SHOOTRANG_CT_WON  
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK

            CASE  MP_AWARD_FM_TENNIS_WON   
                INTAward = MP_AWARD_FM_TENNIS_WON  
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
            CASE  MP_AWARD_FM_TENNIS_ACE   
                INTAward = MP_AWARD_FM_TENNIS_ACE  
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
            
            CASE MP_AWARD_FM_RACES_FASTEST_LAP        
                INTAward =  MP_AWARD_FM_RACES_FASTEST_LAP       
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
            
            CASE MP_AWARD_FM_DM_WINS     
                IF g_b_On_Deathmatch = FALSE
                    INTAward =  MP_AWARD_FM_DM_WINS     
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                    IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_SLAYER) = FALSE
                        IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_DM_WINS) >= GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_FM_DM_WINS, AWARDPOSITIONS_PLATINUM, iTeam)
                            SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_SLAYER, TRUE)
                        ENDIF
                    ENDIF   
                    
                ENDIF
            BREAK
			
            CASE MP_AWARD_FM_TDM_WINS      
                IF g_b_On_Deathmatch = FALSE
                    INTAward = MP_AWARD_FM_TDM_WINS       
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                ENDIF
            BREAK
			
            CASE MP_AWARD_FM_TDM_MVP      
                IF g_b_On_Deathmatch = FALSE
                    INTAward = MP_AWARD_FM_TDM_MVP        
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                    IF NOT IS_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_38)
                        IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_TDM_MVP) >= GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_FM_TDM_MVP, AWARDPOSITIONS_PLATINUM, iTeam)
                            SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_38, TRUE, TRUE)
                        ENDIF
                    ENDIF
                ENDIF
            BREAK
			
            CASE   MP_AWARD_FM_DM_KILLSTREAK      
                INTAward =  MP_AWARD_FM_DM_KILLSTREAK      
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            CASE MP_AWARD_FM_DM_TOTALKILLS        
                INTAward = MP_AWARD_FM_DM_TOTALKILLS  
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                
                IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_09) = FALSE
                    IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FM_DM_TOTALKILLS) >= GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_FM_DM_TOTALKILLS, AWARDPOSITIONS_PLATINUM, iTeam)
                        SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_09, TRUE)
                    ENDIF
                ENDIF   
            BREAK
            CASE MP_AWARD_FM_DM_3KILLSAMEGUY       
                INTAward = MP_AWARD_FM_DM_3KILLSAMEGUY       
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
            CASE MP_AWARD_FM_DM_STOLENKILL       
                INTAward =  MP_AWARD_FM_DM_STOLENKILL      
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
            
            
            
            CASE MP_AWARD_FM_GTA_RACES_WON       
                INTAward =  MP_AWARD_FM_GTA_RACES_WON      
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
            
                        
            CASE MP_AWARD_FMBASEJMP   
                //IF g_b_On_BASEJUMP_RACE = FALSE
                    INTAward = MP_AWARD_FMBASEJMP       
                    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                //ENDIFFho
            BREAK
            
            
            CASE MP_AWARD_FMBBETWIN     
                INTAward = MP_AWARD_FMBBETWIN    
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                
                IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_HUSTLER) = FALSE
                    IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMBBETWIN) >= GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_FMBBETWIN, AWARDPOSITIONS_PLATINUM, iTeam)
                        SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_HUSTLER, TRUE)
                    ENDIF
                ENDIF
            BREAK
            
			
//	            CASE MP_AWARD_FMCRATEDROPS   
//					IF g_sMPTunables.bBLOCK_AWARD_CRATE   = FALSE
//	                	INTAward = MP_AWARD_FMCRATEDROPS
//	                	CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
//					ENDIF
//	            BREAK
			
            
            CASE MP_AWARD_FMHORDWAVESSURVIVE
                INTAward = MP_AWARD_FMHORDWAVESSURVIVE
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                
                // achievement
				IF NOT HAS_ACHIEVEMENT_BEEN_PASSED(ENUM_TO_INT(ACH36))
	                IF GET_MP_INT_CHARACTER_AWARD (MP_AWARD_FMHORDWAVESSURVIVE) >= ACH36_WAVESSURVIVED              
	                    AWARD_ACHIEVEMENT(ACH36) // Unnatural Selection
					ELSE
						SET_ACHIEVEMENT_PROGRESS_SAFE(ENUM_TO_INT(ACH36),GET_MP_INT_CHARACTER_AWARD (MP_AWARD_FMHORDWAVESSURVIVE))
	                ENDIF
                ENDIF
				
                /// tshirt transfer unlock
                IF IS_MP_TATTOO_UNLOCKED(TSHIRT_TRANS_MP_FM_REDSKULL) = FALSE
                    IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE), GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_FMHORDWAVESSURVIVE, AWARDPOSITIONS_PLATINUM, iTeam))  
	                    IF NOT IS_PLAYER_FEMALE()
							UNLOCK_REWARD_TSHIRT( MP_M_FREEMODE_01  ,TSHIRT_TRANS_MP_FM_REDSKULL)
						ELSE
						
							UNLOCK_REWARD_TSHIRT( MP_f_FREEMODE_01  ,TSHIRT_TRANS_MP_FM_REDSKULL)
						ENDIF
						
						SET_MP_TATTOO_UNLOCKED(TSHIRT_TRANS_MP_FM_REDSKULL, TRUE)
                    ENDIF
                ENDIF
                
				IF GET_PACKED_STAT_BOOL(PACKED_MP_TSHIRT_TRANS_MP_FM_REDSKULL) = FALSE
				 	IF IS_INT_AWARD_PASSED_VALUE(GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMHORDWAVESSURVIVE), GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_FMHORDWAVESSURVIVE, AWARDPOSITIONS_PLATINUM, iTeam))  
						IF NOT IS_PLAYER_FEMALE()
							UNLOCK_REWARD_TSHIRT( MP_M_FREEMODE_01  ,TSHIRT_TRANS_MP_FM_REDSKULL)
						ELSE
							UNLOCK_REWARD_TSHIRT( MP_f_FREEMODE_01  ,TSHIRT_TRANS_MP_FM_REDSKULL)
						ENDIF
						SET_PACKED_STAT_BOOL(PACKED_MP_TSHIRT_TRANS_MP_FM_REDSKULL, TRUE)
					ENDIF
				ENDIF
				
                IF IS_MP_WEAPON_UNLOCKED(WEAPONTYPE_SAWNOFFSHOTGUN)= FALSE
                    IF NETWORK_HAS_SOCIAL_CLUB_ACCOUNT()
                        //  SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_DLC_ASSAULTSMG, TRUE, ShouldDisplayMessage)
                        SET_MP_WEAPON_UNLOCKED(WEAPONTYPE_SAWNOFFSHOTGUN, TRUE, FALSE)
                        INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_NO_WEAPONS_UNLOCK)
                    ENDIF
                ENDIF       
                
            BREAK
            
            
            

            CASE MP_AWARD_FMWINRACETOPOINTS 
                INTAward = MP_AWARD_FMWINRACETOPOINTS
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK   
            
            CASE MP_AWARD_FMKILLBOUNTY     
                INTAward = MP_AWARD_FMKILLBOUNTY
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                
                IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_BOUNTY_KILLER) = FALSE
                    IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMKILLBOUNTY) >= GET_AWARD_INTCHAR_LEVEL_NUMBER(MP_AWARD_FMKILLBOUNTY, AWARDPOSITIONS_PLATINUM, iTeam)
                        SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_BOUNTY_KILLER, TRUE)
                    ENDIF
                ENDIF
                
                /* THIS ACHIEVEMENT HAS BEEN REMOVED
                IF (GET_MP_INT_CHARACTER_AWARD (MP_AWARD_FMKILLBOUNTY) >= ACH44_BOUNTIESKILLED)
                    AWARD_ACHIEVEMENT(ACH44) // Got Away With Murder
                ENDIF
                */
            BREAK       
            
            CASE MP_AWARD_FMOVERALLKILLS    
                IF GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMOVERALLKILLS)!= GET_MP_INT_CHARACTER_STAT(MP_STAT_KILLS_PLAYERS)
                    SET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMOVERALLKILLS, GET_MP_INT_CHARACTER_STAT(MP_STAT_KILLS_PLAYERS))
                ENDIF
                INTAward = MP_AWARD_FMOVERALLKILLS
            
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK       
            CASE MP_AWARD_FMWINSEARACE      
                INTAward = MP_AWARD_FMWINSEARACE
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK       
            CASE MP_AWARD_FMWINAIRRACE      
                INTAward = MP_AWARD_FMWINAIRRACE
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK           
            CASE MP_AWARD_FMREVENGEKILLSDM 
                INTAward = MP_AWARD_FMREVENGEKILLSDM
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
                IF IS_MP_TATTOO_UNLOCKED( TATTOO_MP_FM_REVENGE_KILL ) = FALSE
                    IF GET_MP_INT_CHARACTER_AWARD(INTAward) >= GET_AWARD_INTCHAR_LEVEL_NUMBER(INTAward, AWARDPOSITIONS_PLATINUM, iTeam)
                        SET_MP_TATTOO_UNLOCKED(TATTOO_MP_FM_REVENGE_KILL    , TRUE)
                    ENDIF
                ENDIF
            BREAK   

            CASE MP_AWARD_FMRALLYWONDRIVE   
                INTAward = MP_AWARD_FMRALLYWONDRIVE 
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK       
            CASE MP_AWARD_FMRALLYWONNAV    
                INTAward = MP_AWARD_FMRALLYWONNAV 
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK       

                    
    
            CASE MP_AWARD_FM_RACE_LAST_FIRST
                INTAward = MP_AWARD_FM_RACE_LAST_FIRST
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
//			CASE  MP_AWARD_TAKEDOWNSMUGPLANE    
//				IF g_sMPTunables.bBLOCK_AWARD_FLYBYE                 = FALSE
//					INTAward = MP_AWARD_TAKEDOWNSMUGPLANE 
//					CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
//				ENDIF
//			BREAK   
			
		



//			CASE MP_AWARD_ODISTRACTCOPSNOEATH		
//				IF g_sMPTunables.bBLOCK_AWARD_OVERHERE               = FALSE
//					INTAward = MP_AWARD_ODISTRACTCOPSNOEATH 
//					CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
//				ENDIF
//			BREAK   
//			CASE MP_AWARD_LESTERDELIVERVEHICLES		
//				IF g_sMPTunables.bBLOCK_AWARD_AUTOGO                 = FALSE
//					INTAward = MP_AWARD_LESTERDELIVERVEHICLES 
//					CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
//				ENDIF
//			BREAK   
			CASE MP_AWARD_DAILYOBJCOMPLETED			
				INTAward = MP_AWARD_DAILYOBJCOMPLETED 
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
			BREAK   
						
						
            
  			
			#IF USE_TU_CHANGES	
			CASE  MP_AWARD_WIN_CAPTURES     						
				INTAward = MP_AWARD_WIN_CAPTURES 
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
				// Sport
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_CHROME_RIM_1_MESSAGE)
				AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(INTAward)
					DISPLAY_CARMOD_UNLOCK_MESSAGE("UNLOCK_CHRO_RIM1")
					SET_PACKED_STAT_BOOL(PACKED_MP_CHROME_RIM_1_MESSAGE, TRUE)
				ENDIF
			BREAK   
			
			
			CASE  MP_AWARD_DROPOFF_CAP_PACKAGES     		  
				INTAward = MP_AWARD_DROPOFF_CAP_PACKAGES 
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
				// Lowrider
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_CHROME_RIM_2_MESSAGE)
				AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(INTAward)
					DISPLAY_CARMOD_UNLOCK_MESSAGE("UNLOCK_CHRO_RIM2")
					SET_PACKED_STAT_BOOL(PACKED_MP_CHROME_RIM_2_MESSAGE, TRUE)
				ENDIF
				
			BREAK   		
			CASE  MP_AWARD_PICKUP_CAP_PACKAGES     			
				INTAward = MP_AWARD_PICKUP_CAP_PACKAGES 
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
				IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(INTAward)
				AND NOT IS_DLC_AWARD_TSHIRT_UNLOCKED(DLC_SHIRT_AWARD_LIVE_A_LITTLE)
					UNLOCK_A_DLC_AWARD_TSHIRT(DLC_SHIRT_AWARD_LIVE_A_LITTLE)
				ENDIF

				
			BREAK   
			CASE  MP_AWARD_KILL_CARRIER_CAPTURE     			
				INTAward = MP_AWARD_KILL_CARRIER_CAPTURE 
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
				// Off road chrome rims
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_CHROME_RIM_3_MESSAGE)
				AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(INTAward)
					DISPLAY_CARMOD_UNLOCK_MESSAGE("UNLOCK_CHRO_RIM3")
					SET_PACKED_STAT_BOOL(PACKED_MP_CHROME_RIM_3_MESSAGE, TRUE)
				ENDIF
				
			BREAK   
			CASE  MP_AWARD_WIN_CAPTURE_DONT_DYING     			
				INTAward = MP_AWARD_WIN_CAPTURE_DONT_DYING 
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
				
				IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(INTAward)
				AND NOT IS_DLC_AWARD_TSHIRT_UNLOCKED(DLC_SHIRT_AWARD_DEATH_DEFYING)
					UNLOCK_A_DLC_AWARD_TSHIRT(DLC_SHIRT_AWARD_DEATH_DEFYING)
				ENDIF
				
				
				IF NOT IS_DLC_AWARD_TSHIRT_UNLOCKED(DLC_SHIRT_HEIST_ELITE)
					IF SHOULD_UNLOCK_ELITE_CHALLENGE_TSHIRT()
						UNLOCK_A_DLC_AWARD_TSHIRT(DLC_SHIRT_HEIST_ELITE)
						/*IF bIsPlayerMale
							SET_BITSET_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(DLC_SHIRT_HEIST_ELITE,bIsPlayerMale), TATTOO_MP_FM), TRUE, -1)
							SET_BITSET_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(DLC_SHIRT_HEIST_ELITE_1,bIsPlayerMale), TATTOO_MP_FM), TRUE, -1)
							PRINTLN("[RCC MISSION] MARK_ELITE_CHALLENGE_AS_COMPLETE UNlocked shirt male")
						ELSE
							SET_BITSET_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(DLC_SHIRT_HEIST_ELITE ,bIsPlayerMale), TATTOO_MP_FM_F), TRUE, -1)
							SET_BITSET_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(DLC_SHIRT_HEIST_ELITE_1,bIsPlayerMale), TATTOO_MP_FM_F), TRUE, -1)
							PRINTLN("[RCC MISSION] MARK_ELITE_CHALLENGE_AS_COMPLETE UNlocked shirt female")
						ENDIF*/
					ENDIF
				ELSE
					IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_ELITE) 
						SET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_ELITE, TRUE) 
					ENDIF		
				ENDIF
				
				IF NOT IS_DLC_AWARD_TSHIRT_UNLOCKED(DLC_SHIRT_HEIST_ELITE_1)
					IF SHOULD_UNLOCK_ELITE_CHALLENGE_TSHIRT()
						UNLOCK_A_DLC_AWARD_TSHIRT(DLC_SHIRT_HEIST_ELITE_1)
						/*IF bIsPlayerMale
							SET_BITSET_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(DLC_SHIRT_HEIST_ELITE,bIsPlayerMale), TATTOO_MP_FM), TRUE, -1)
							SET_BITSET_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(DLC_SHIRT_HEIST_ELITE_1,bIsPlayerMale), TATTOO_MP_FM), TRUE, -1)
							PRINTLN("[RCC MISSION] MARK_ELITE_CHALLENGE_AS_COMPLETE UNlocked shirt male")
						ELSE
							SET_BITSET_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(DLC_SHIRT_HEIST_ELITE ,bIsPlayerMale), TATTOO_MP_FM_F), TRUE, -1)
							SET_BITSET_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(DLC_SHIRT_HEIST_ELITE_1,bIsPlayerMale), TATTOO_MP_FM_F), TRUE, -1)
							PRINTLN("[RCC MISSION] MARK_ELITE_CHALLENGE_AS_COMPLETE UNlocked shirt female")
						ENDIF*/
					ENDIF
				ELSE
					IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_ELITE1) 
						SET_PACKED_STAT_BOOL(PACKED_MP_MSG_SHIRT_ELITE1, TRUE) 
					ENDIF
				ENDIF
			
			BREAK   
			

			CASE  MP_AWARD_FINISH_HEISTS     					
				INTAward = MP_AWARD_FINISH_HEISTS 
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
				// chrome rims
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_CHROME_RIM_4_MESSAGE)
				AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(INTAward)
					DISPLAY_CARMOD_UNLOCK_MESSAGE("UNLOCK_CHRO_RIM4")
					SET_PACKED_STAT_BOOL(PACKED_MP_CHROME_RIM_4_MESSAGE, TRUE)
				ENDIF
				
				
			BREAK   
			CASE  MP_AWARD_FINISH_HEIST_SETUP_JOB     			
				INTAward = MP_AWARD_FINISH_HEIST_SETUP_JOB 
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
				// chrome rims
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_CHROME_RIM_5_MESSAGE)
				AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(INTAward)
					DISPLAY_CARMOD_UNLOCK_MESSAGE("UNLOCK_CHRO_RIM5")
					SET_PACKED_STAT_BOOL(PACKED_MP_CHROME_RIM_5_MESSAGE, TRUE)
				ENDIF
				
			
			BREAK   
			CASE  MP_AWARD_DO_HEIST_AS_MEMBER     				
				INTAward = MP_AWARD_DO_HEIST_AS_MEMBER 
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
				IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(INTAward)
				AND NOT IS_DLC_AWARD_TSHIRT_UNLOCKED(DLC_SHIRT_AWARD_FOR_HIRE)
					UNLOCK_A_DLC_AWARD_TSHIRT(DLC_SHIRT_AWARD_FOR_HIRE)
				ENDIF
				
				
				
			BREAK   		
			CASE  MP_AWARD_DO_HEIST_AS_THE_LEADER   			
				INTAward = MP_AWARD_DO_HEIST_AS_THE_LEADER 
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
				IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(INTAward)
				AND NOT IS_DLC_AWARD_TSHIRT_UNLOCKED(DLC_SHIRT_AWARD_SHOT_CALLER)
					UNLOCK_A_DLC_AWARD_TSHIRT(DLC_SHIRT_AWARD_SHOT_CALLER)
				ENDIF
			BREAK   
			CASE  MP_AWARD_COMPLETE_HEIST_NOT_DIE     			
				INTAward = MP_AWARD_COMPLETE_HEIST_NOT_DIE
				//CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
				//IF NOT IS_DLC_AWARD_TSHIRT_UNLOCKED(DLC_SHIRT_AWARD_LIVE_A_LITTLE)
				//AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(INTAward)
				//	UNLOCK_A_DLC_AWARD_TSHIRT(DLC_SHIRT_AWARD_LIVE_A_LITTLE)
				//ENDIF
			BREAK   
			CASE  MP_AWARD_WIN_GOLD_MEDAL_HEISTS     			
				INTAward = MP_AWARD_WIN_GOLD_MEDAL_HEISTS
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
				IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(INTAward)
				AND NOT IS_DLC_AWARD_TSHIRT_UNLOCKED(DLC_SHIRT_AWARD_DECORATED)
					UNLOCK_A_DLC_AWARD_TSHIRT(DLC_SHIRT_AWARD_DECORATED)
				ENDIF		
				
			BREAK   
			CASE  MP_AWARD_NIGHTVISION_KILLS   				
				INTAward = MP_AWARD_NIGHTVISION_KILLS
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
				// chrome rims
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_CHROME_RIM_6_MESSAGE)
				AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(INTAward)
					DISPLAY_CARMOD_UNLOCK_MESSAGE("UNLOCK_CHRO_RIM6")
					SET_PACKED_STAT_BOOL(PACKED_MP_CHROME_RIM_6_MESSAGE, TRUE)
				ENDIF
			BREAK   

			CASE  MP_AWARD_CONTROL_CROWDS     					
				INTAward = MP_AWARD_CONTROL_CROWDS
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
			BREAK   
			

			
			CASE  MP_AWARD_KILL_PSYCHOPATHS     				
				INTAward = MP_AWARD_KILL_PSYCHOPATHS
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
				IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(INTAward)
				AND NOT IS_DLC_AWARD_TSHIRT_UNLOCKED(DLC_SHIRT_AWARD_PSYCHO_KILLER)
					UNLOCK_A_DLC_AWARD_TSHIRT(DLC_SHIRT_AWARD_PSYCHO_KILLER)
				ENDIF		
				
			BREAK   
			CASE  MP_AWARD_MENTALSTATE_TO_NORMAL     			
				INTAward = MP_AWARD_MENTALSTATE_TO_NORMAL
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
			BREAK   
			CASE  MP_AWARD_WIN_LAST_TEAM_STANDINGS     		
				INTAward = MP_AWARD_WIN_LAST_TEAM_STANDINGS
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
				
				// chrome rims
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_CHROME_RIM_7_MESSAGE)
				AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(INTAward)
					DISPLAY_CARMOD_UNLOCK_MESSAGE("UNLOCK_CHRO_RIM7")
					SET_PACKED_STAT_BOOL(PACKED_MP_CHROME_RIM_7_MESSAGE, TRUE)
				ENDIF
				
			BREAK   
			CASE  MP_AWARD_ONLY_PLAYER_ALIVE_LTS     			
				INTAward = MP_AWARD_ONLY_PLAYER_ALIVE_LTS
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
				// chrome rims
				IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_CHROME_RIM_8_MESSAGE)
				AND IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(INTAward)
					DISPLAY_CARMOD_UNLOCK_MESSAGE("UNLOCK_CHRO_RIM8")
					SET_PACKED_STAT_BOOL(PACKED_MP_CHROME_RIM_8_MESSAGE, TRUE)
				ENDIF		
				
			BREAK   
			CASE  MP_AWARD_KILL_TEAM_YOURSELF_LTS   			
				INTAward = MP_AWARD_KILL_TEAM_YOURSELF_LTS
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
				
				IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(INTAward)
				AND NOT IS_DLC_AWARD_TSHIRT_UNLOCKED(DLC_SHIRT_AWARD_ONE_MAN_ARMY)
					UNLOCK_A_DLC_AWARD_TSHIRT(DLC_SHIRT_AWARD_ONE_MAN_ARMY)
				ENDIF	
			BREAK   
			CASE  MP_AWARD_TRADE_IN_YOUR_PROPERTY     			
				INTAward = MP_AWARD_TRADE_IN_YOUR_PROPERTY 
				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
			BREAK   
		#ENDIF
		
		
	        CASE MP_AWARD_DANCE_TO_SOLOMUN
                INTAward     = MP_AWARD_DANCE_TO_SOLOMUN
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
		    CASE MP_AWARD_DANCE_TO_TALEOFUS
                INTAward     = MP_AWARD_DANCE_TO_TALEOFUS
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
		    CASE MP_AWARD_DANCE_TO_DIXON
                INTAward     = MP_AWARD_DANCE_TO_DIXON
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
		    CASE MP_AWARD_DANCE_TO_BLKMAD  
                INTAward     = MP_AWARD_DANCE_TO_BLKMAD
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
		    CASE MP_AWARD_CLUB_DRUNK
                INTAward     = MP_AWARD_CLUB_DRUNK
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
		
		
		
		
		    CASE MP_AWARD_WATCH_YOUR_STEP
                INTAward     = MP_AWARD_WATCH_YOUR_STEP
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
			CASE MP_AWARD_TOWER_OFFENSE
                INTAward     = MP_AWARD_TOWER_OFFENSE
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
			CASE MP_AWARD_READY_FOR_WAR
                INTAward     = MP_AWARD_READY_FOR_WAR
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
			CASE MP_AWARD_THROUGH_A_LENS
                INTAward     = MP_AWARD_THROUGH_A_LENS
				IF g_bRequestingLiveStream = FALSE
        	        CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
           		ENDIF
			BREAK
			
			
			CASE MP_AWARD_SPINNER
                INTAward     = MP_AWARD_SPINNER
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
			
			CASE MP_AWARD_YOUMEANBOOBYTRAPS
                INTAward     = MP_AWARD_YOUMEANBOOBYTRAPS
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
			
			CASE MP_AWARD_MASTER_BANDITO
                INTAward     = MP_AWARD_MASTER_BANDITO
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
			
			CASE MP_AWARD_SITTING_DUCK
                INTAward     = MP_AWARD_SITTING_DUCK
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
			
			CASE MP_AWARD_CROWDPARTICIPATION
                INTAward     = MP_AWARD_CROWDPARTICIPATION
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
			
			CASE MP_AWARD_KILL_OR_BE_KILLED
                INTAward     = MP_AWARD_KILL_OR_BE_KILLED
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
			
			CASE MP_AWARD_MASSIVE_SHUNT
                INTAward     = MP_AWARD_MASSIVE_SHUNT
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
			
			CASE MP_AWARD_YOURE_OUTTA_HERE
                INTAward     = MP_AWARD_YOURE_OUTTA_HERE
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
			CASE MP_AWARD_WEVE_GOT_ONE
                INTAward     = MP_AWARD_WEVE_GOT_ONE
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
			
			CASE MP_AWARD_ARENA_WAGEWORKER
                INTAward     = MP_AWARD_ARENA_WAGEWORKER
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
			CASE MP_AWARD_TIME_SERVED
                INTAward     = MP_AWARD_TIME_SERVED
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
			CASE MP_AWARD_TOP_SCORE
                INTAward     = MP_AWARD_TOP_SCORE
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
			CASE MP_AWARD_CAREER_WINNER
                INTAward     = MP_AWARD_CAREER_WINNER
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			

			
			CASE MP_AWARD_ODD_JOBS
                INTAward     = MP_AWARD_ODD_JOBS
                CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)
            BREAK
			
			
			CASE MP_AWARD_PREPARATION 	 INTAward     =  MP_AWARD_ODD_JOBS			CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
            CASE MP_AWARD_ASLEEPONJOB 	 INTAward     =  MP_AWARD_ASLEEPONJOB 		CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
            CASE MP_AWARD_DAICASHCRAB    INTAward     =  MP_AWARD_DAICASHCRAB   	CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
            CASE MP_AWARD_BIGBRO         INTAward     =  MP_AWARD_BIGBRO        	CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
            CASE MP_AWARD_SHARPSHOOTER   INTAward     =  MP_AWARD_SHARPSHOOTER  	CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
            CASE MP_AWARD_RACECHAMP      INTAward     =  MP_AWARD_RACECHAMP     	CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
            CASE MP_AWARD_BATSWORD       INTAward     =  MP_AWARD_BATSWORD      	CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
            CASE MP_AWARD_COINPURSE      INTAward     =  MP_AWARD_COINPURSE     	CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
            CASE MP_AWARD_ASTROCHIMP     INTAward     =  MP_AWARD_ASTROCHIMP    	CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
            CASE MP_AWARD_MASTERFUL      INTAward     =  MP_AWARD_MASTERFUL     	CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			#IF FEATURE_COPS_N_CROOKS
		 	 CASE MP_AWARD_COPSNCROOKS     IntAward =   MP_AWARD_COPSNCROOKS     			 CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			 CASE MP_AWARD_UPHOLDLAW     	IntAward =   MP_AWARD_UPHOLDLAW     	         CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			 CASE MP_AWARD_CRACKDOWN     	IntAward =   MP_AWARD_CRACKDOWN     	         CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			 CASE MP_AWARD_JUSTSERVE     	IntAward =   MP_AWARD_JUSTSERVE     	         CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			 CASE MP_AWARD_CASHSEIZE     	IntAward =   MP_AWARD_CASHSEIZE     	         CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			 CASE MP_AWARD_POLICECHIEF     IntAward =   MP_AWARD_POLICECHIEF     	         CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			 CASE MP_AWARD_CRIMINALREC     IntAward =   MP_AWARD_CRIMINALREC     	         CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			 CASE MP_AWARD_COVERTCRIM     	IntAward =   MP_AWARD_COVERTCRIM     	         CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			 CASE MP_AWARD_STASHCASH     	IntAward =   MP_AWARD_STASHCASH     	         CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			 CASE MP_AWARD_STRAIGHTUPRUN   IntAward =   MP_AWARD_STRAIGHTUPRUN               CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			 CASE MP_AWARD_KINGPIN     		IntAward =   MP_AWARD_KINGPIN     	             CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			#ENDIF
			

			 CASE MP_AWARD_SUNSET  		        INTAWARD = MP_AWARD_SUNSET  		CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK 
			 CASE MP_AWARD_TREASURE_HUNTER      INTAWARD = MP_AWARD_TREASURE_HUNTER CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			 CASE MP_AWARD_WRECK_DIVING  	    INTAWARD = MP_AWARD_WRECK_DIVING    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			 CASE MP_AWARD_KEINEMUSIK  	        INTAWARD = MP_AWARD_KEINEMUSIK  	CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK 
			 CASE MP_AWARD_PALMS_TRAX  	        INTAWARD = MP_AWARD_PALMS_TRAX  	CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK 
			 CASE MP_AWARD_MOODYMANN  	        INTAWARD = MP_AWARD_MOODYMANN  	    CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			 CASE MP_AWARD_FILL_YOUR_BAGS       INTAWARD = MP_AWARD_FILL_YOUR_BAGS  CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			 CASE MP_AWARD_WELL_PREPARED        INTAWARD = MP_AWARD_WELL_PREPARED   CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK
			
			 CASE MP_AWARD_CAR_CLUB_MEM		          INTAWARD = MP_AWARD_CAR_CLUB_MEM			CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK 
			 CASE MP_AWARD_SPRINTRACER 		          INTAWARD = MP_AWARD_SPRINTRACER 			CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK 
			 CASE MP_AWARD_STREETRACER			      INTAWARD = MP_AWARD_STREETRACER			CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK 
			 CASE MP_AWARD_PURSUITRACER			      INTAWARD = MP_AWARD_PURSUITRACER			CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK 
			 CASE MP_AWARD_AUTO_SHOP				  INTAWARD = MP_AWARD_AUTO_SHOP				CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK 
			 CASE MP_AWARD_GROUNDWORK				  INTAWARD = MP_AWARD_GROUNDWORK			CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK 
			 CASE MP_AWARD_ROBBERY_CONTRACT  		  INTAWARD = MP_AWARD_ROBBERY_CONTRACT  	CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK 		
			CASE MP_AWARD_TEST_CAR      			 INTAWARD = MP_AWARD_TEST_CAR  		CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK 		
			CASE MP_AWARD_CAR_EXPORT				 INTAWARD = MP_AWARD_CAR_EXPORT  	CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK 		
			#IF FEATURE_FIXER
			CASE MP_AWARD_CONTRACTOR				 INTAWARD = MP_AWARD_CONTRACTOR  	CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK 		
			CASE MP_AWARD_COLD_CALLER				 INTAWARD = MP_AWARD_COLD_CALLER  	CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK 		
			CASE MP_AWARD_PRODUCER				 INTAWARD = MP_AWARD_PRODUCER  	CHECK_AND_GIVE_INT_AWARD(INTAward, iteam)   BREAK 	
			#ENDIF						
					
        ENDSWITCH
		
		
		
        
        #IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE 
            strString = "WhichIntAward = "
            strString += ENUM_TO_INT(MPGlobalsHud.WhichIntAward)
            ADD_SCRIPT_PROFILE_MARKER(strString)
        #ENDIF
        #ENDIF
        
        incrementTo = ENUM_TO_INT(MPGlobalsHud.WhichIntAward)
        incrementTo++
        MPGlobalsHud.WhichIntAward = INT_TO_ENUM(MP_INT_AWARD, incrementTo)
        IF MPGlobalsHud.WhichIntAward = MAX_NUM_MP_INT_AWARD
            MPGlobalsHud.WhichIntAward = INT_TO_ENUM(MP_INT_AWARD, 0)
        ENDIF
          
		ELIF MPGlobalsHud_TitleUpdate.iAwardStaggerStage = 3
		
        SWITCH MPGlobalsHud.WhichPlyIntAward
        	
            CASE MPPLY_AWD_FM_CR_RACES_MADE_INDEX       
                INTPlayerAward =  MPPLY_AWD_FM_CR_RACES_MADE_INDEX     
				IF CAN_DRAW_UNLOCKS_SCALEFORM()
				
               	 CHECK_AND_GIVE_INTPLY_AWARD(INTPlayerAward, iteam)
				ENDIF
            BREAK
            

            
            CASE  MPPLY_AWD_FM_CR_DM_MADE_INDEX     
                INTPlayerAward =   MPPLY_AWD_FM_CR_DM_MADE_INDEX    
				IF CAN_DRAW_UNLOCKS_SCALEFORM()
                	CHECK_AND_GIVE_INTPLY_AWARD(INTPlayerAward, iteam)
				ENDIF
            BREAK
            
            
            CASE MPPLY_AWD_FM_CR_PLAYED_BY_PEEP_INDEX        
                INTPlayerAward =  MPPLY_AWD_FM_CR_PLAYED_BY_PEEP_INDEX   
				IF CAN_DRAW_UNLOCKS_SCALEFORM()
             	   CHECK_AND_GIVE_INTPLY_AWARD(INTPlayerAward, iteam)
				 ENDIF
            BREAK
            
            CASE MPPLY_AWD_FM_CR_MISSION_SCORE_INDEX 
            
                IF g_FMMC_HEADER_STRUCT.iMyHighestLikes != GET_MP_INT_PLAYER_AWARD(MPPLY_AWD_FM_CR_MISSION_SCORE_INDEX)
                    SET_MP_INT_PLAYER_AWARDS(MPPLY_AWD_FM_CR_MISSION_SCORE_INDEX, g_FMMC_HEADER_STRUCT.iMyHighestLikes)
                ENDIF
                INTPlayerAward = MPPLY_AWD_FM_CR_MISSION_SCORE_INDEX    
				IF CAN_DRAW_UNLOCKS_SCALEFORM()
              	  CHECK_AND_GIVE_INTPLY_AWARD(INTPlayerAward, iteam)
				 ENDIF
            BREAK
            

        ENDSWITCH
        
        #IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE 
            strString = "WhichPlyIntAward = "
            strString += ENUM_TO_INT(MPGlobalsHud.WhichPlyIntAward)
            ADD_SCRIPT_PROFILE_MARKER(strString)
        #ENDIF
        #ENDIF
        
        
        incrementTo = ENUM_TO_INT(MPGlobalsHud.WhichPlyIntAward)
        incrementTo++
        MPGlobalsHud.WhichPlyIntAward = INT_TO_ENUM(MPPLY_INT_AWARD_INDEX, incrementTo)
        IF ENUM_TO_INT(MPGlobalsHud.WhichPlyIntAward) = MAX_NUM_MPPLY_INT_AWARD
            MPGlobalsHud.WhichPlyIntAward = INT_TO_ENUM(MPPLY_INT_AWARD_INDEX, 0)
        ENDIF
        
        ENDIF
		
		MPGlobalsHud_TitleUpdate.iAwardStaggerStage++
		IF MPGlobalsHud_TitleUpdate.iAwardStaggerStage >= 4
			MPGlobalsHud_TitleUpdate.iAwardStaggerStage = 0
		ENDIF
        

        


        
        

        //STAT CHECKING
        SWITCH TimerCounter // neilf: I changed this to a switch statement for optimising. 
    
            
            
            CASE 1
                    
                CurrentTime = GET_NETWORK_TIME()  
                
                TRACK_PLAYER_IN_CAR_STATS(MP_STAT_TIME_AS_A_DRIVER ,MP_STAT_TIME_AS_A_PASSENGER, MP_STAT_TIME_IN_CAR, GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[0]))
                MPGlobalsStats.LastStatTimer[0] = GET_NETWORK_TIME()
                    
            BREAK

            
            CASE 2
                
                
//              IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_PLAYING
//                  NET_NL()NET_PRINT("UPDATE_HEALTH = ")NET_PRINT_INT(GET_ENTITY_HEALTH(PLAYER_PED_ID()))
                    
                    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_HEALTH) != GET_ENTITY_HEALTH(PLAYER_PED_ID()) 
                        SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_HEALTH, GET_ENTITY_HEALTH(PLAYER_PED_ID()) )
                    ENDIF
                    					
					IF gbFM_start_tracking_armour = TRUE
	                    IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_ARMOUR) != GET_PED_ARMOUR(PLAYER_PED_ID()) 
	                        SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_ARMOUR, GET_PED_ARMOUR(PLAYER_PED_ID()) )
	                    ENDIF
					ENDIF
//              ELSE
//                  
//                  NET_NL()NET_PRINT("UPDATE_HEALTH = ")NET_PRINT_INT(999)
//                  SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_HEALTH, 999)
//              ENDIF

                
                MPGlobalsStats.LastStatTimer[13] = GET_NETWORK_TIME()   

            BREAK
            
            CASE 3
            
                CurrentTime = GET_NETWORK_TIME()  
            
                TRACK_WANTED_LEVEL_STATS(MP_STAT_CHAR_WANTED_LEVEL_TIME, MP_STAT_NO_TIMES_WANTED_LEVEL, MP_STAT_AVERAGE_WANTED_TIME, GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[1]) )
                
                 MPGlobalsStats.LastStatTimer[1] = GET_NETWORK_TIME()
                
            BREAK
            

            
            CASE 4
                
                //CurrentTime = GET_NETWORK_TIME()  

                //MPGlobalsStats.LastStatTimer[2] = GET_NETWORK_TIME( )
                TRACK_FILL_A_TITAN()
            BREAK
            
            
            CASE 5
                
                CurrentTime = GET_NETWORK_TIME()  
                
                TRACK_TRACK_WHEN_PLAYER_IS_ON_PHONE(MP_STAT_TOTAL_TIME_SPENT_ON_PHONE, GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[3]))
                MPGlobalsStats.LastStatTimer[3] = GET_NETWORK_TIME( )
        
            BREAK
            
            
            CASE 6
                CurrentTime = GET_NETWORK_TIME()  
                
                TRACK_PLAYERS_TIME_IN_GAME_MODES(MPPLY_TOTAL_TIME_MISSION_CREATO, MPPLY_TOTAL_TIME_SPENT_DEATHMAT , MPPLY_TOTAL_TIME_SPENT_FREEMODE, MPPLY_TOTAL_TIME_SPENT_RACES, GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[4]))
                MPGlobalsStats.LastStatTimer[4]= GET_NETWORK_TIME( )
            BREAK
            

            
    
            
            CASE 8
                // for bug 1594598 Save the photo stats every 5 mins - UPDATE Miguel requested via email that we remove this routine and only save when a picture has actually been taken
                //I've moved REQUEST_SAVE(STAT_SAVETYPE_PHOTOS) to appCAMERA.sc
                /*CurrentTime = GET_NETWORK_TIME() 
                
               
                IF GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[5]) > 300000 
                    REQUEST_SAVE(STAT_SAVETYPE_PHOTOS)
                    MPGlobalsStats.LastStatTimer[5] = GET_NETWORK_TIME()
                    NET_PRINT("\n SAVE  STAT_SAVETYPE_PHOTOS ")
                
                
                
                
                ELSE
                
                //NET_PRINT("\n CurrentTime = ")
                //NET_PRINT_INT(GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[5]))
                ENDIF
                */
				
					// remove xmas clothes
			
							
				


            BREAK
            

            CASE 9
                TRACK_AMMO_GROUPING()
            BREAK

        
            
            CASE 10
                // TRACK CASH
                //SET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_CASH_EARNED, GET_MP_TOTAL_CASH_EARNED())
                //SET_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_CASH_SPENT, GET_MP_TOTAL_CASH_SPENT())
                TRACK_CREW_TATTOO_UNLOCKS()
                //TRANSFER_VIRTUAL_CASH_TRANSFER_TO_BANK_ACCOUNT()

				IF bsyncmk2statstomk1stat = FALSE
					SYNC_MK2_WEAPON_STAT_TO_MK1()
					bsyncmk2statstomk1stat = TRUE
				ENDIF
			BREAK
            
                            

            CASE 11
                CurrentTime = GET_NETWORK_TIME()  
                
                TRACKING_INCIDENT_REPONSE_TIMES(GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[6]))
                MPGlobalsStats.LastStatTimer[6] = GET_NETWORK_TIME()
            BREAK
            
            
        
        
            
        
            
            CASE 12
                GIVE_SOCIAL_CLUB_WELCOME_MESSAGE()
            BREAK
            
            
            
            CASE 13
                //TRACK_STAT_KILL_DEATH_RATIO() <= moved to process events now

            BREAK
            
            CASE 14
			
			     // AG - Had to move this into here so we can be sure the achievements only fire after the outro scenes - B*2090918
					TRACK_HEIST_ACHIEVEMENTS()

            BREAK
            
            
            CASE 15
            // KW Todo 1409641 Can we add degrading player stats to MP characters
                IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.ilasttimeplayed != 0
                    IF (GET_CLOUD_TIME_AS_INT() - g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.ilasttimeplayed) > 86400 // 172800seconds = 2 days. 86400 = 1 day
                        SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_STAMINA,  (GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_STAMINA) - GET_ABILITY_STAT_DIMINISH_AMOUNT(MP_STAT_CHAR_CREATOR_STAMINA, GET_NUMBER_OF_DAYS_LAST_PLAYED()) ) )
                        SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_SHOOTING,  (GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_SHOOTING) - GET_ABILITY_STAT_DIMINISH_AMOUNT(MP_STAT_CHAR_CREATOR_SHOOTING, GET_NUMBER_OF_DAYS_LAST_PLAYED()) ) )
                        SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_STRENGTH,  (GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_STRENGTH) - GET_ABILITY_STAT_DIMINISH_AMOUNT(MP_STAT_CHAR_CREATOR_STRENGTH, GET_NUMBER_OF_DAYS_LAST_PLAYED()) ) )
                        SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_STEALTH,  (GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_STEALTH) - GET_ABILITY_STAT_DIMINISH_AMOUNT(MP_STAT_CHAR_CREATOR_STEALTH, GET_NUMBER_OF_DAYS_LAST_PLAYED()) ) )
                        SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_FLYING,  (GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_FLYING) - GET_ABILITY_STAT_DIMINISH_AMOUNT(MP_STAT_CHAR_CREATOR_FLYING, GET_NUMBER_OF_DAYS_LAST_PLAYED()) ) )
                        SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_BIKE,  (GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_BIKE) - GET_ABILITY_STAT_DIMINISH_AMOUNT(MP_STAT_CHAR_CREATOR_BIKE, GET_NUMBER_OF_DAYS_LAST_PLAYED()) ) )
                        SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_LUNG,  (GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_LUNG) - GET_ABILITY_STAT_DIMINISH_AMOUNT(MP_STAT_CHAR_CREATOR_LUNG, GET_NUMBER_OF_DAYS_LAST_PLAYED()) ) )
                        SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_MECHANIC,  (GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CREATOR_MECHANIC) - GET_ABILITY_STAT_DIMINISH_AMOUNT(MP_STAT_CHAR_CREATOR_MECHANIC, GET_NUMBER_OF_DAYS_LAST_PLAYED()) ) )
                        
                        
                        #IF IS_DEBUG_BUILD
                            NET_PRINT("[KW@DIMINISHING ABILITY] PLAYER ABILITY lowering because player has not played for  ")
                            NET_PRINT_INT(GET_NUMBER_OF_DAYS_LAST_PLAYED())
                            NET_PRINT(" Days")
                            NET_PRINT("\n g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.ilasttimeplayed() = ")
                            NET_PRINT_INT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.ilasttimeplayed)
                            
                            NET_PRINT("\n removing MP_STAT_CHAR_CREATOR_STAMINA amount ")
                            NET_PRINT_INT(GET_ABILITY_STAT_DIMINISH_AMOUNT(MP_STAT_CHAR_CREATOR_STAMINA, GET_NUMBER_OF_DAYS_LAST_PLAYED()))
                            NET_PRINT("\n removing MP_STAT_CHAR_CREATOR_SHOOTING amount ")
                            NET_PRINT_INT(GET_ABILITY_STAT_DIMINISH_AMOUNT(MP_STAT_CHAR_CREATOR_SHOOTING, GET_NUMBER_OF_DAYS_LAST_PLAYED()))
                            NET_PRINT("\n removing MP_STAT_CHAR_CREATOR_STEALTH amount ")
                            NET_PRINT_INT(GET_ABILITY_STAT_DIMINISH_AMOUNT(MP_STAT_CHAR_CREATOR_STEALTH, GET_NUMBER_OF_DAYS_LAST_PLAYED()))
                            NET_PRINT("\n removing MP_STAT_CHAR_CREATOR_FLYING amount ")
                            NET_PRINT_INT(GET_ABILITY_STAT_DIMINISH_AMOUNT(MP_STAT_CHAR_CREATOR_FLYING, GET_NUMBER_OF_DAYS_LAST_PLAYED()))
                            NET_PRINT("\n removing MP_STAT_CHAR_CREATOR_BIKE amount ")
                            NET_PRINT_INT(GET_ABILITY_STAT_DIMINISH_AMOUNT(MP_STAT_CHAR_CREATOR_BIKE, GET_NUMBER_OF_DAYS_LAST_PLAYED()))
                            NET_PRINT("\n removing MP_STAT_CHAR_CREATOR_LUNG amount ")
                            NET_PRINT_INT(GET_ABILITY_STAT_DIMINISH_AMOUNT(MP_STAT_CHAR_CREATOR_LUNG, GET_NUMBER_OF_DAYS_LAST_PLAYED()))
                            NET_PRINT("\n removing MP_STAT_CHAR_CREATOR_MECHANIC amount ")
                            NET_PRINT_INT(GET_ABILITY_STAT_DIMINISH_AMOUNT(MP_STAT_CHAR_CREATOR_MECHANIC, GET_NUMBER_OF_DAYS_LAST_PLAYED()))
            
                        #ENDIF
        
                        
                        // INCREMENT_PLAYER_PED_STAT(enumCharacterList ePed, PLAYER_STATS_ENUM ePlayerStat, INT iAmount)
                        // lets drop 2% = 1 day. per stat per day you havent played
                        
                    
                    ENDIF
                ENDIF
                
                g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.ilasttimeplayed = GET_CLOUD_TIME_AS_INT()
            BREAK
            
            CASE 16
            
            
            
                
                
                


                CurrentTime = GET_NETWORK_TIME()  
                MPGlobalsStats.LastStatTimer[8] = GET_NETWORK_TIME() 
            
                
            BREAK
            
            CASE 17
				IF NOT IS_TRANSITION_ACTIVE()
                	TRACK_AVERAGE_XP_GAINED_PER_MINUTE()
				ENDIF
            BREAK
            
            CASE 18
                TRACK_AVERAGE_XP_ON_MISSION()
            BREAK
        
        
        

        
            CASE 19
                CurrentTime = GET_NETWORK_TIME()  
                
                TRACK_TIME_SPENT_FLYING(GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[9]))
                MPGlobalsStats.LastStatTimer[9] = GET_NETWORK_TIME() 
                
                
            BREAK
            
                    
            CASE 20
                // work out average time alive
                
                // players game times divided by number of deaths
                //TRACK_AVERAGE_TIME_ALIVE()
                TRACK_AMMO_USED()
                //TRACK_TOTAL_CASH_SPENT_AND_EARNED_CHARACTER()
                //TRACK_TOTAL_CASH_SPENT_AND_SPENT_CHARACTER()
                //INCREMENT_TOTAL_CASH_SPENT_AND_EARNED_PLAYER()
            BREAK
            
            CASE 21
                //average time on mission
                CurrentTime = GET_NETWORK_TIME()  
                
                TRACK_AVERAGE_TIME_ON_MISSION(GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[10]))
                MPGlobalsStats.LastStatTimer[10] =GET_NETWORK_TIME() 

                
            BREAK
            

            CASE 22
                
            //  TRACK_TOTAL_DISTANCE()      ///removed . now being tracked in code
				TRACK_GANGOPS_ACHIEVEMENTS()
            BREAK
            
            CASE 23
                TRACK_RANK_GAIN_PER_HOUR()

            BREAK
            

            
            CASE 24
                CurrentTime = GET_NETWORK_TIME()  
                    
         
                MPGlobalsStats.LastStatTimer[11]=GET_NETWORK_TIME() 
        
            BREAK
            CASE 25
                
                    CurrentTime = GET_NETWORK_TIME()  
                    
                    TRACK_AVERAGE_NUMBER_OF_PASSENGERS(GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[12]))
                    MPGlobalsStats.LastStatTimer[12]=GET_NETWORK_TIME() 
                
            BREAK
            
            
            CASE 26

                TRACK_NUM_WEAPONS_USED()
            BREAK

                
            CASE 27
//              TRACK_ARREST_RATE()
                //TRACK_TOTAL_WEAPON_Accuracy()
				
				IF GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_DCTL_MSG) = FALSE
					IF NOT g_bDontCrossRunning 
						IF GET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_DCTL) = TRUE 
							SET_REWARD_UNLOCKED(UNLOCKTYPEENUM_TATTOO,"CLO_EXF_DECL_17", GET_DLC_SHIRT_DESC(DLC_SHIRT_RETRO_HOMIES), GET_DLC_SHIRT_TEXTURE_NAME(DLC_SHIRT_RETRO_HOMIES),"MPTshirtAwards3" )
							SET_PACKED_STAT_BOOL(PACKED_STAT_BOOL_SHIRT_DCTL_MSG, TRUE)
						ENDIF
					ENDIF
				ENDIF
            BREAK
            

            
            CASE 29
                TRACK_AMMO_USED2()
                //TRACK_Accuracy_weapons2()
            BREAK
            
            CASE 30
                TRACK_AMMO_GROUPING2()
            BREAK
            
            CASE 31
                RUN_PERFERRED_CREW_UPDATE()
            BREAK
            
            
            CASE 32
//              TRACK_STORE_LAST_SESSION_TIME()
            BREAK
			
			 CASE 33
			 	IF NOT NETWORK_PLAYER_IS_BADSPORT()
				AND NOT NETWORK_PLAYER_IS_CHEATER()
				AND NOT IS_PLAYER_AUTOMUTED_BADSPORT(PLAYER_ID())
				
	                CurrentTime = GET_NETWORK_TIME()  
	                
					INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_TOTALPLAYTIME_GOODBOY,  GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[5]))
					
	               
				ELSE
					SET_MP_INT_PLAYER_STAT(MPPLY_TOTALPLAYTIME_GOODBOY, 0)
				ENDIF
				 MPGlobalsStats.LastStatTimer[5]= GET_NETWORK_TIME()
            BREAK
            
			//Track SCTV Stats
            CASE 34
                IF IS_PLAYER_SCTV(PLAYER_ID())
					CurrentTime = GET_NETWORK_TIME()  
	                IF MPGlobalsStats.bResetSctvStatTimer = FALSE
					
						//Track SCTV in Freemode or Job
						IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
							INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIME_SPECT_JOBS,  GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[19]))
						ELSE
							INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIME_SPECT_FREEROAM,  GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[19]))
			            ENDIF
						
						//Track SCTV Friend/Crew/Stranger
						IF DOES_ENTITY_EXIST(MPSpecGlobals.pedCurrentFocus)
							IF IS_PED_A_PLAYER(MPSpecGlobals.pedCurrentFocus)
								PLAYER_INDEX playerId
								playerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(MPSpecGlobals.pedCurrentFocus)
								GAMER_HANDLE eGHandle
								eGHandle = GET_GAMER_HANDLE_PLAYER(playerId)
								IF ARE_PLAYERS_ON_SAME_ACTIVE_CLAN(PLAYER_ID(), playerId)
									IF NETWORK_IS_FRIEND(eGHandle)
										INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIME_SPECT_FRIENDS,  GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[19]))
										INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIME_SPECT_CREW_MEMBERS,  GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[19]))
									ELSE
										INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIME_SPECT_CREW_MEMBERS,  GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[19]))
										INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIME_SPECT_CREW_NOT_FRIEND,  GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[19]))
									ENDIF
								ELIF NETWORK_IS_FRIEND(eGHandle)
									INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIME_SPECT_FRIENDS,  GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[19]))
									INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIME_SPECT_FRIEND_NOT_CREW,  GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[19]))
								ELSE
									INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TIME_SPECT_STRANGERS,  GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[19]))
								ENDIF
							ENDIF
						ENDIF
					ELSE
						MPGlobalsStats.bResetSctvStatTimer = FALSE
						PRINTLN(" TRACK SCTV - bResetSctvStatTimer set FALSE")
					ENDIF
					
		            MPGlobalsStats.LastStatTimer[19] = GET_NETWORK_TIME()
					
					//Reset Spectator Timer
					IF MPGlobalsStats.bResetJobSpecStatTimer = FALSE
						MPGlobalsStats.bResetJobSpecStatTimer = TRUE
						PRINTLN(" TRACK SPEC - bResetJobSpecStatTimer set TRUE")
					ENDIF
				
				ELSE
					//Reset SCTV Timer
					IF MPGlobalsStats.bResetSctvStatTimer = FALSE
						MPGlobalsStats.bResetSctvStatTimer = TRUE
						PRINTLN(" TRACK SCTV - bResetSctvStatTimer set TRUE")
					ENDIF
					
					IF IS_A_SPECTATOR_CAM_ACTIVE()
					AND IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID())
						//Track Job Spectator Time
						CurrentTime = GET_NETWORK_TIME()  
		                IF MPGlobalsStats.bResetJobSpecStatTimer = FALSE
							IF Is_Player_Currently_On_MP_CTF_Mission(PLAYER_ID())
								INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_TIME_SPECCAM_CTF,  GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[18]))
								IF MPGlobalsStats.bResetJobSpecStatStarted[0] = FALSE
									INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_IS_CHAR_IN_SPECCAM_CTF,  1)
									PRINTLN(" TRACK SPEC - MP_STAT_IS_CHAR_IN_SPECCAM_CTF + 1")
									MPGlobalsStats.bResetJobSpecStatStarted[0] = TRUE
								ENDIF
							ELIF Is_Player_Currently_On_MP_LTS_Mission(PLAYER_ID())
								INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_TIME_SPECCAM_LTS,  GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[18]))
								IF MPGlobalsStats.bResetJobSpecStatStarted[1] = FALSE
									INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_IS_CHAR_IN_SPECCAM_LTS,  1)
									PRINTLN(" TRACK SPEC - MP_STAT_IS_CHAR_IN_SPECCAM_LTS + 1")
									MPGlobalsStats.bResetJobSpecStatStarted[1] = TRUE
								ENDIF
							ELIF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_MISSION)
								INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_TIME_SPECCAM_MISS,  GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[18]))
								IF MPGlobalsStats.bResetJobSpecStatStarted[2] = FALSE
									INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_IS_CHAR_IN_SPECCAM_MISS,  1)
									PRINTLN(" TRACK SPEC - MP_STAT_IS_CHAR_IN_SPECCAM_MISS + 1")
									MPGlobalsStats.bResetJobSpecStatStarted[2] = TRUE
								ENDIF
							ELIF IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_ID(), FMMC_TYPE_SURVIVAL)
								INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_TOTAL_TIME_SPECCAM_SUR,  GET_TIME_DIFFERENCE(CurrentTime, MPGlobalsStats.LastStatTimer[18]))
								IF MPGlobalsStats.bResetJobSpecStatStarted[3] = FALSE
									INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_IS_CHAR_IN_SPECCAM_SUR,  1)
									PRINTLN(" TRACK SPEC - MP_STAT_IS_CHAR_IN_SPECCAM_SUR + 1")
									MPGlobalsStats.bResetJobSpecStatStarted[3] = TRUE
								ENDIF
				            ENDIF
						ELSE
							MPGlobalsStats.bResetJobSpecStatTimer = FALSE
							MPGlobalsStats.bResetJobSpecStatStarted[0] = FALSE
							MPGlobalsStats.bResetJobSpecStatStarted[1] = FALSE
							MPGlobalsStats.bResetJobSpecStatStarted[2] = FALSE
							MPGlobalsStats.bResetJobSpecStatStarted[3] = FALSE
							PRINTLN(" TRACK SPEC - bResetJobSpecStatTimer set FALSE")
						ENDIF
						
						MPGlobalsStats.LastStatTimer[18] = GET_NETWORK_TIME()
					ELSE
						//Reset Spectator Timer
						IF MPGlobalsStats.bResetJobSpecStatTimer = FALSE
							MPGlobalsStats.bResetJobSpecStatTimer = TRUE
							PRINTLN(" TRACK SPEC - bResetJobSpecStatTimer set TRUE")
						ENDIF
					ENDIF
				ENDIF
            BREAK
            
        ENDSWITCH
        


        TimerCounter++
        IF (TimerCounter > 35)

            TimerCounter = 0
        ENDIF
        
        // must be tracked every frame
        TRACK_MULTIPLE_DEATHS_STAT()
        
        // do this every frame, it has its own stagger
        TRACK_UNLOCK_OF_COLOURED_WEAPONS()
        
        
//      IF PlayerCounter >= GET_NUM_ACTIVE_PLAYERS()-1
//          MPGlobalsStats.StatTrackingMain = -1
//      ENDIF

        
        #IF IS_DEBUG_BUILD
        #IF SCRIPT_PROFILER_ACTIVE 
            strString = "TimerCounter = "
            strString += TimerCounter-1
            ADD_SCRIPT_PROFILE_MARKER(strString)
        #ENDIF
        #ENDIF      
            
        
    ENDIF
    
    
    #IF IS_DEBUG_BUILD
    #IF SCRIPT_PROFILER_ACTIVE 
    CLOSE_SCRIPT_PROFILE_MARKER_GROUP()
    #ENDIF
    #ENDIF
    

    

ENDPROC

















