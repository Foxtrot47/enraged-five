USING "mp_globals_new_features_tu.sch"

// This header shouldn't be in the release project anyway, but wrap it up just in case


USING "rage_builtins.sch"
USING "globals.sch"
USING "Commands_Cutscene.sch"
USING "script_network.sch"
USING "net_team_info.sch"
USING "net_realty_details.sch"
USING "net_heists_public.sch"
USING "net_cutscene.sch"
USING "context_control_public.sch"
USING "net_realty_new.sch"

USING "net_events.sch"					// KGM 30/4/14: Possibly TEMP - used to tell other players to play the preplanning cutscene
USING "commands_audio.sch"

#IF IS_DEBUG_BUILD
USING "net_debug.sch"
#ENDIF




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Heists_Cutscenes.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains the Heist Cutscene functionality.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      DEBUG HEIST MOCAP CUTSCENE FUNCTIONS
// ===========================================================================================================

// PURPOSE:	Re-display a help message indicating if the cutscene is skippable or not
//
// INPUT PARAMS:		paramIsSkippable		TRUE if skippable, FALSE if not
#IF IS_DEBUG_BUILD
PROC DEBUG_Maintain_Cutscene_Skip_Help(BOOL paramIsSkippable)

	// Only display if the commandline parameter is set
	#IF IS_DEBUG_BUILD
		IF NOT (GET_COMMANDLINE_PARAM_EXISTS("sc_HeistCutSkippable"))
			EXIT
		ENDIF
	#ENDIF

	IF (IS_HELP_MESSAGE_BEING_DISPLAYED())
		EXIT
	ENDIF
	
	// The Heist leader will be in their own property
	BOOL isHeistLeader = IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE)
	
	IF (isHeistLeader)
		IF (paramIsSkippable)
			PRINT_HELP("HDBG_LSKIP", 60000)
			PRINTLN(".KGM [Heist][Cutscene] Display Cutscene Skip Help: HDBG_LSKIP [", GET_STRING_FROM_TEXT_FILE("HDBG_LSKIP"), "]")
		ELSE
			PRINT_HELP("HDBG_LNOSKIP", 60000)
			PRINTLN(".KGM [Heist][Cutscene] Display Cutscene Skip Help: HDBG_LNOSKIP [", GET_STRING_FROM_TEXT_FILE("HDBG_LNOSKIP"), "]")
		ENDIF
	ELSE
		PRINT_HELP("HDBG_CNOSKIP", 60000)
		PRINTLN(".KGM [Heist][Cutscene] Display Cutscene Skip Help: HDBG_CNOSKIP [", GET_STRING_FROM_TEXT_FILE("HDBG_CNOSKIP"), "]")
	ENDIF

ENDPROC
#ENDIF	// IS_DEBUG_BUILD


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear one of the debug help messages if on display
#IF IS_DEBUG_BUILD
PROC DEBUG_Clear_Cutscene_Skip_Help()

	IF NOT (IS_HELP_MESSAGE_BEING_DISPLAYED())
		EXIT
	ENDIF
	
	IF (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HDBG_LSKIP"))
	OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HDBG_LNOSKIP"))
	OR (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HDBG_CNOSKIP"))
		CLEAR_HELP()
	ENDIF

ENDPROC
#ENDIF	// IS_DEBUG_BUILD




// ===========================================================================================================
//      HEIST MOCAP CUTSCENE SKIP - BUTTON CHECK AND EVENT CONTROL
// ===========================================================================================================

// PURPOSE:	Display a 'Cutscene Skippable' Help Message to Leader the first time they are allowed to skip a cutscene
//
// INPUT PARAMS:		paramIsSkippable		TRUE if skippable, FALSE if not
PROC Maintain_Leader_Skip_Cutscene_Help(BOOL paramIsSkippable)

	IF NOT (paramIsSkippable)
		EXIT
	ENDIF

	IF (IS_BIT_SET(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_CUTSCENE_SKIP_HELP_DISPLAYED))
		EXIT
	ENDIF

	IF (IS_HELP_MESSAGE_BEING_DISPLAYED())
		EXIT
	ENDIF
	
	// Don't display if the 'allow skip' commandline parameter is active as it will mess up the flag setting and help display
	#IF IS_DEBUG_BUILD
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_HeistCutSkippable"))
			EXIT
		ENDIF
	#ENDIF
	
	// The Heist leader will be in their own property
	IF NOT (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE))
		EXIT
	ENDIF
	
	// Display the help: "You can skip this scene by pressing ~INPUT_SKIP_CUTSCENE~ as you are the Heist leader and have watched it before."
	PRINT_HELP("HEIST_CUTSKIP") //, 60000) - Changed to default time to fix - url:bugstar:2234023 - Prison Intro: New Help Text re cutscene skippable stayed on screen throughout the scene. We would normally expect this to fade
	PRINTLN(".KGM [Heist][Cutscene] Display One-Off Leader Cutscene Skip Help: HEIST_CUTSKIP")
	
	// Ensure it never gets seen again
	SET_BIT(g_sLocalMPHeistUnlocks.lhuBitsetStatCopyHeistUnlocks, HEIST_UNLOCK_BITFLAG_CUTSCENE_SKIP_HELP_DISPLAYED)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the 'skip' help text if on display
PROC Clear_Leader_Skip_Cutscene_Help()

	IF NOT (IS_HELP_MESSAGE_BEING_DISPLAYED())
		EXIT
	ENDIF
	
	IF (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_CUTSKIP"))
		CLEAR_HELP()
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Broadcast Skip Cutscene to all players
PROC Broadcast_Skip_Heist_Cutscene()
	PRINTLN(".KGM [Heist][Cutscene] Broadcasting 'Skip Cutscene' to all players")
	BROADCAST_GENERAL_EVENT(GENERAL_EVENT_SKIP_HEIST_CUTSCENE, ALL_PLAYERS())
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process the Skip Cutscene event
PROC Process_Event_Skip_Heist_Cutscene()
	PRINTLN(".KGM [Heist][Cutscene] Processing 'Skip Cutscene'")
	g_heistMocap.skipCutscene = TRUE
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Broadcast Check to all players to see if they are playing the heist cutscene
//
// INPUT PARAMS:			paramLeader		Replies get sent only to this player
PROC Broadcast_Reply_Playing_Heist_Cutscene(PLAYER_INDEX paramLeader)
	PRINTLN(".KGM [Heist][Cutscene] Broadcasting 'Reply Playing Heist Cutscene' to Leader: ", GET_PLAYER_NAME(paramLeader))
	BROADCAST_GENERAL_EVENT(GENERAL_EVENT_REPLY_PLAYING_HEIST_CUTSCENE, SPECIFIC_PLAYER(paramLeader))
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process and count the Replies 'Playing Heist Cutscene'
//
// INPUT PARAMS:			paramMember		The player that sent the reply (Crew AND Leader)
//
// NOTES:	This should only get received by the Leader
PROC Process_Event_Reply_Playing_Heist_Cutscene(PLAYER_INDEX paramMember)
	g_heistMocap.numPlayingCutscene++
	PRINTLN(".KGM [Heist][Cutscene] Processing 'Reply Playing Cutscene'. From Member: ", GET_PLAYER_NAME(paramMember), ". Total Replies: ", g_heistMocap.numPlayingCutscene)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Broadcast Check to all players to see if they are playing the heist cutscene
// NOTES:	Leader only. Clear the counter ready for player replies.
PROC Broadcast_Check_If_Playing_Heist_Cutscene()
	PRINTLN(".KGM [Heist][Cutscene] Broadcasting 'Check If Playing Heist Cutscene' to all players")
	g_heistMocap.numPlayingCutscene = 0
	BROADCAST_GENERAL_EVENT(GENERAL_EVENT_CHECK_IF_PLAYING_HEIST_CUTSCENE, ALL_PLAYERS())
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process the Check If Playing Heist Cutscene event
//
// INPUT PARAMS:		paramLeader		
PROC Process_Event_Check_If_Playing_Heist_Cutscene(PLAYER_INDEX paramLeader)

	IF NOT (IS_NET_PLAYER_OK(paramLeader, FALSE))
		PRINTLN(".KGM [Heist][Cutscene] Processing 'Check If Playing Cutscene' but Leader is not ok. Ignoring.")
		EXIT
	ENDIF

	PRINTLN(".KGM [Heist][Cutscene] Processing 'Check If Playing Cutscene' from Leader: ", GET_PLAYER_NAME(paramLeader))
	// Only reply if playing Heist Cutscene
	IF (g_heistMocap.stage = HMS_STAGE_PLAYING_CUTSCENE)
		PRINTLN(".KGM [Heist][Cutscene] ...replying YES")
		Broadcast_Reply_Playing_Heist_Cutscene(paramLeader)
	ELSE
		PRINTLN(".KGM [Heist][Cutscene] ...not in stage HMS_STAGE_PLAYING_CUTSCENE. Ignoring.")
	ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks to see if player has just pressed the cutscene skip button
//
// RETURN VALUE:		BOOL		TRUE if the heist cutscene button has just been skipped, otherwise FALSE
//
// NOTES:	A copy of the standard IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED() function from script_buttons, but without the 'pause menu' check since the pause menu is active behind-the-scenes
FUNC BOOL Is_Heist_Cutscene_Skip_Button_Just_Pressed()

	IF IS_PC_VERSION()
		IF NOT NETWORK_TEXT_CHAT_IS_TYPING()
			IF (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SKIP_CUTSCENE))
			OR (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE))
				PRINTLN(".KGM [Heist][Cutscene] Is_Heist_Cutscene_Skip_Button_Just_Pressed - NETWORK_TEXT_CHAT_IS_TYPING = FALSE, return true.")
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SKIP_CUTSCENE))
		OR (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE))
			PRINTLN(".KGM [Heist][Cutscene] Is_Heist_Cutscene_Skip_Button_Just_Pressed - Control pressed, skipping cutscene.")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC





// ===========================================================================================================
//      HEIST MOCAP CUTSCENES
// ===========================================================================================================

FUNC BOOL IS_CURRENT_APARTMENT_DLC()

	INT PropertyToCheck = globalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty			

	IF Propertytocheck = PROPERTY_BUS_HIGH_APT_1
	OR Propertytocheck = PROPERTY_BUS_HIGH_APT_2
	OR Propertytocheck = PROPERTY_BUS_HIGH_APT_3
	OR Propertytocheck = PROPERTY_BUS_HIGH_APT_4
	OR Propertytocheck= PROPERTY_BUS_HIGH_APT_5
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF

ENDFUNC


// PURPOSE:	Prepare a new Heist cutscene and choose the players that will appear in it
//
// INPUT PARAM:			paramCutsceneName		The cutscene to trigger
PROC Setup_Heist_Cutscene(TEXT_LABEL_23 paramCutsceneName)

	// Store the Player_Indexes of the players that should be in the Mocap
	INT tempLoop = 0
	INT numPlayers = 0
	PLAYER_INDEX thisPlayer = INVALID_PLAYER_INDEX()
	
	IF g_HeistSharedClient.iCurrentPropertyOwnerIndex = INVALID_HEIST_DATA
		PRINTLN(".KGM [Heist][Cutscene] Setup_Heist_Cutscene - ERROR! Could not locate heist leader for registration in slot 0!")
		SCRIPT_ASSERT(".KGM [Heist][Cutscene] Setup_Heist_Cutscene - ERROR! Could not locate heist leader for registration in slot 0! Please enter a bug for Alastair C.")
	ENDIF
	
	// First, find me
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		thisPlayer = INT_TO_PLAYERINDEX(tempLoop)
		IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
			IF (thisPlayer = INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex))
				IF NOT (IS_THIS_PLAYER_SCTV_OR_JOINING_AS_SCTV(thisPlayer))
					g_heistMocap.players[numPlayers] = thisPlayer
					PRINTLN(".KGM [Heist][Cutscene] Setup_Heist_Cutscene - Saving leader: ", GET_PLAYER_NAME(thisPlayer), ", in mocap slot: ", numPlayers)
					numPlayers++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Then, find up to three other players
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (numPlayers < MAX_HEIST_MOCAP_PLAYERS)
			thisPlayer = INT_TO_PLAYERINDEX(tempLoop)
			IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
				IF (thisPlayer != INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex))
					IF NOT (IS_THIS_PLAYER_SCTV_OR_JOINING_AS_SCTV(thisPlayer))
						g_heistMocap.players[numPlayers] = thisPlayer
						PRINTLN(".KGM [Heist][Cutscene] Setup_Heist_Cutscene - Saving member: ", GET_PLAYER_NAME(thisPlayer), ", in mocap slot: ", numPlayers)
						numPlayers++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	WHILE (numPlayers < MAX_HEIST_MOCAP_PLAYERS)
		g_heistMocap.players[numPlayers] = INVALID_PLAYER_INDEX()
		numPlayers++
	ENDWHILE
	
	// Clear out the Mocap PedIndexes
	REPEAT MAX_HEIST_MOCAP_PLAYERS tempLoop
		g_heistMocap.mocapPeds[tempLoop] = NULL
	ENDREPEAT
	
	// Store the mocap cutscene name
	g_heistMocap.cutsceneName = paramCutsceneName
	
	// Clear out the 'skip cutscene' controls
	g_heistMocap.skipCutscene		= FALSE
	g_heistMocap.numPlayingCutscene	= 0
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Make clones of the players that are to appear in the cutscene
//
// RETURN VALUE:			BOOL			TRUE when all the clones are created, otherwise FALSE
FUNC BOOL Create_Heist_Cutscene_Peds()

	INT tempLoop = 0
	BOOL atLeastOnePlayerRemainsInGame = FALSE

	PED_INDEX tempPed
	
		// SETTING UP PLAYERS HAIR FOR CELEBRATION SCREEN
	IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_USING_HAIR_SA_STAT)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA) 
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHairTintPrime 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHairTintSecondary 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
	ELSE
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO) 
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHairTintPrime 		= GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHairTintSecondary 	= GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
	ENDIF
	
	PED_COMP_NAME_ENUM eMyCurrentHair = INT_TO_ENUM(PED_COMP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair)
	IF (eMyCurrentHair != DUMMY_PED_COMP)
		PED_COMP_NAME_ENUM eGRHairItem = DUMMY_PED_COMP
		IF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01)
			eGRHairItem = GET_MALE_HAIR(eMyCurrentHair)
		ELIF (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01)
			eGRHairItem = GET_FEMALE_HAIR(eMyCurrentHair)
		ENDIF
		
		IF (eGRHairItem != DUMMY_PED_COMP)
		AND (eMyCurrentHair != eGRHairItem)
			CPRINTLN(DEBUG_MP_HEISTS_AMEC,"[Heist][Cutscene] gr_hair: replacing hair enum ", eMyCurrentHair, " with gunrunning hair enum ", eGRHairItem)
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair = ENUM_TO_INT(eGRHairItem)
		ENDIF
	ENDIF
	
	REPEAT MAX_HEIST_MOCAP_PLAYERS tempLoop
	
		IF (g_heistMocap.players[tempLoop] != INVALID_PLAYER_INDEX())
			IF (IS_NET_PLAYER_OK(g_heistMocap.players[tempLoop], FALSE))
				atLeastOnePlayerRemainsInGame = TRUE
				IF (g_heistMocap.mocapPeds[tempLoop] = NULL)
					IF NOT (DOES_ENTITY_EXIST(g_heistMocap.mocapPeds[tempLoop]))
					
						PRINTLN(".KGM [Heist][Cutscene] Requesting Ped for Player in array: ", tempLoop)
						tempPed = GET_PLAYER_PED(g_heistMocap.players[tempLoop])

						g_heistMocap.mocapPeds[tempLoop] = CLONE_PED(tempPed, FALSE, FALSE, FALSE)					
						
						CLEAR_PED_TASKS_IMMEDIATELY(g_heistMocap.mocapPeds[tempLoop])
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(g_heistMocap.mocapPeds[tempLoop], TRUE)
						// ------- Fix for Bug: 206347 Rowan J
						SET_PED_RESET_FLAG(g_heistMocap.mocapPeds[tempLoop], PRF_BlockWeaponReactionsUnlessDead, TRUE) 
						SET_PED_RESET_FLAG(g_heistMocap.mocapPeds[tempLoop], PRF_DisableSecondaryAnimationTasks, TRUE) 
						SET_PED_CONFIG_FLAG(g_heistMocap.mocapPeds[tempLoop], PCF_PedIgnoresAnimInterruptEvents, TRUE)
						SET_PED_CONFIG_FLAG(g_heistMocap.mocapPeds[tempLoop], PCF_DisableExplosionReactions, TRUE)
						SET_PED_CONFIG_FLAG(g_heistMocap.mocapPeds[tempLoop], PCF_RunFromFiresAndExplosions, FALSE)
						// -------
						SET_PED_RELATIONSHIP_GROUP_HASH(g_heistMocap.mocapPeds[tempLoop], rgFM_AiLike)
						
						SET_ENTITY_INVINCIBLE(g_heistMocap.mocapPeds[tempLoop], TRUE)
						FREEZE_ENTITY_POSITION(g_heistMocap.mocapPeds[tempLoop], TRUE)
						SET_ENTITY_VISIBLE(g_heistMocap.mocapPeds[tempLoop], FALSE)
						SET_ENTITY_COLLISION(g_heistMocap.mocapPeds[tempLoop], FALSE)
					
						FINALIZE_HEAD_BLEND(g_heistMocap.mocapPeds[tempLoop])
						SET_PED_HELMET(g_heistMocap.mocapPeds[tempLoop], FALSE)
						REMOVE_PED_HELMET(g_heistMocap.mocapPeds[tempLoop], TRUE)
						REMOVE_PED_MASK(g_heistMocap.mocapPeds[tempLoop])
						
						// Checks if ped is wearing scarf before setting teeth comp to 0 - url:bugstar:2235506
						IF GET_PED_DRAWABLE_VARIATION(g_heistMocap.mocapPeds[tempLoop], PED_COMP_TEETH) = 30
						OR GET_PED_DRAWABLE_VARIATION(g_heistMocap.mocapPeds[tempLoop], PED_COMP_TEETH) = 31
						OR GET_PED_DRAWABLE_VARIATION(g_heistMocap.mocapPeds[tempLoop], PED_COMP_TEETH) = 34
						OR GET_PED_DRAWABLE_VARIATION(g_heistMocap.mocapPeds[tempLoop], PED_COMP_TEETH) = 35
							PRINTLN(".KGM [Heist][Cutscene] Player is wearing a scarf - removing the scarf for cloned mocap ped")
							SET_PED_COMPONENT_VARIATION(g_heistMocap.mocapPeds[tempLoop], PED_COMP_TEETH, 0, 0)		// Remove scarf url:bugstar:2220157
						ENDIF
						
						PED_COMP_ITEM_DATA_STRUCT sTempCompData = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(g_heistMocap.mocapPeds[tempLoop]), COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(g_heistMocap.players[tempLoop])].iMyCurrentHair))
						SET_PED_COMPONENT_VARIATION(g_heistMocap.mocapPeds[tempLoop], PED_COMP_HAIR, sTempCompData.iDrawable, sTempCompData.iTexture)
						PRINTLN(".KGM [Heist][Cutscene] Cloning ped with hair: ", GlobalplayerBD[NATIVE_TO_INT(g_heistMocap.players[tempLoop])].iMyCurrentHair)		
	
						SET_FACIAL_IDLE_ANIM_OVERRIDE(g_heistMocap.mocapPeds[tempLoop], GET_PLAYER_MOOD_ANIM_FROM_INDEX(GlobalplayerBD_FM[NATIVE_TO_INT(g_heistMocap.players[tempLoop])].iPlayerMood))
						PRINTLN(".KGM [Heist][Cutscene] Cloning ped with mood: ", GlobalplayerBD_FM[NATIVE_TO_INT(g_heistMocap.players[tempLoop])].iPlayerMood)		
							
						CLEAR_PED_PROP(g_heistMocap.mocapPeds[tempLoop], ANCHOR_HEAD)
										
						IF IS_PED_WEARING_HAZ_HOOD_UP(g_heistMocap.mocapPeds[tempLoop])
							SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(g_heistMocap.mocapPeds[tempLoop]) 
						ENDIF
						
						SET_HOODED_JACKET_STATE(g_heistMocap.mocapPeds[tempLoop], JACKET_HOOD_DOWN)
						
						REMOVE_MP_PED_SCUBA_TANK(g_heistMocap.mocapPeds[tempLoop])
						
						// Ensure female peds do not wear heels
						//SET_FEMALE_PED_FEET_TO_FLATS(g_heistMocap.mocapPeds[tempLoop])
									
						// Only adding return false here because the mission COntroller template does this - it may be that only one ped can be cloned in a frame
						RETURN FALSE
						
					ENDIF
					
				ELSE
					
					IF DOES_ENTITY_EXIST(g_heistMocap.mocapPeds[tempLoop])
					AND NOT IS_ENTITY_DEAD(g_heistMocap.mocapPeds[tempLoop])
						//force normal head
						IF g_heistMocap.players[tempLoop] = PLAYER_ID()
							RESET_PLAYER_HEAD_BLEND_TO_NORM(g_heistMocap.mocapPeds[tempLoop])
						ELSE
							IF NETWORK_HAS_CACHED_PLAYER_HEAD_BLEND_DATA(g_heistMocap.players[tempLoop])
								IF NOT NETWORK_APPLY_CACHED_PLAYER_HEAD_BLEND_DATA(g_heistMocap.mocapPeds[tempLoop], g_heistMocap.players[tempLoop])
									PRINTLN(".KGM [Heist][Cutscene] Waiting to apply head-blend from cache. Index: ", tempLoop)
									RETURN FALSE
								ELSE
									PRINTLN(".KGM [Heist][Cutscene] Cached head blend applied successfully")
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_ENTITY_DEAD(g_heistMocap.mocapPeds[tempLoop])
							PRINTLN(".KGM [Heist][Cutscene] Failed head-blend, entity: ",NATIVE_TO_INT(g_heistMocap.mocapPeds[tempLoop])," is DEAD.")
						ELSE
							PRINTLN(".KGM [Heist][Cutscene] Failed head-blend, entity: ",NATIVE_TO_INT(g_heistMocap.mocapPeds[tempLoop])," DOES NOT EXIST.")
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	IF NOT (atLeastOnePlayerRemainsInGame)
		PRINTLN(".KGM [Heist][Cutscene] QUITTING: NO VALID PLAYER PEDS REMAIN")
		g_heistMocap.stage = HMS_STAGE_INACTIVE
		RETURN TRUE
	ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Delete any cloned peds used inteh cutscene to represent the players
PROC Destroy_Heist_Cutscene_Peds()

	INT tempLoop = 0
	MODEL_NAMES tempModel
	
	REPEAT MAX_HEIST_MOCAP_PLAYERS tempLoop
		IF NOT (g_heistMocap.mocapPeds[tempLoop] = NULL)
			IF (DOES_ENTITY_EXIST(g_heistMocap.mocapPeds[tempLoop]))
				PRINTLN(".KGM [Heist][Cutscene] Mark Cloned Player Model As No Longer Needed in array: ", tempLoop)
				tempModel = GET_ENTITY_MODEL(g_heistMocap.mocapPeds[tempLoop])
				SET_MODEL_AS_NO_LONGER_NEEDED(tempModel)
				DELETE_PED(g_heistMocap.mocapPeds[tempLoop])
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if all players in the mocap are playing the cutscene
// 
// INPUT PARAMS:			paramCutsceneParticipants		The number of participants that should be taking part in this cutscene
// RETURN VALUE:			BOOL							TRUE if all are playign cutscene, otherwise FALSE
//
// NOTES:	KGM 7/11/14 [BUG 2110075]: Part of the new skip cutscene routines
FUNC BOOL Are_All_Players_Playing_Cutscene(INT paramCutsceneParticipants)

	#IF IS_DEBUG_BUILD
		// For Debug Builds, base it on the number of mocap peds that have been created
		INT numClonedPeds = 0
		INT tempLoop = 0
		
		REPEAT MAX_HEIST_MOCAP_PLAYERS tempLoop
			IF NOT (g_heistMocap.mocapPeds[tempLoop] = NULL)
				IF (DOES_ENTITY_EXIST(g_heistMocap.mocapPeds[tempLoop]))
					numClonedPeds++
				ENDIF
			ENDIF
		ENDREPEAT
		
		RETURN (g_heistMocap.numPlayingCutscene = numClonedPeds)
	#ENDIF

	// The 'real' check
	RETURN (g_heistMocap.numPlayingCutscene = paramCutsceneParticipants)
			
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Make clones of the doors that are to appear in the cutscene
//
// RETURN VALUE:			BOOL			TRUE when all the clones are created, otherwise FALSE
FUNC BOOL Create_Heist_Cutscene_Doors(BOOL bAllowStartingApartment = FALSE, BOOL bModelHide = TRUE)

	//VECTOR vdoor_coordinates
	//TEXT_LABEL_15 cutsceneDoorID = "bedroom_door"
	MODEL_NAMES doorModel
	
	INT iBaseProperty = -1 
	
	INT iProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
	INT iPropertyVariation = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideVariation
	
	IF iProperty = -1
	AND bAllowStartingApartment
		PRINTLN("Create_Heist_Cutscene_Doors: Using GET_HEIST_MISSION_STARTING_APARTMENT as iProperty")
		iProperty = GET_HEIST_MISSION_STARTING_APARTMENT()
	ELSE
		PRINTLN("Create_Heist_Cutscene_Doors: Using propertyID from playerBD, value: ", iProperty, ", Variant: ", iPropertyVariation)
	ENDIF
	
	//IF DOES_CUTSCENE_ENTITY_EXIST(cutsceneDoorID, V_ILEV_FH_BEDRMDOOR)
	IF  iProperty = PROPERTY_BUS_HIGH_APT_1
	OR  iProperty = PROPERTY_BUS_HIGH_APT_2
	OR  iProperty = PROPERTY_BUS_HIGH_APT_3
	OR  iProperty = PROPERTY_BUS_HIGH_APT_4
	OR  iProperty = PROPERTY_BUS_HIGH_APT_5
		doorModel = HEI_HEIST_APART2_DOOR
		iBaseProperty = PROPERTY_BUS_HIGH_APT_1
	ELIF iProperty = PROPERTY_HIGH_APT_1	
	OR iProperty = PROPERTY_HIGH_APT_2					
	OR iProperty = PROPERTY_HIGH_APT_3					
	OR iProperty = PROPERTY_HIGH_APT_4					
	OR iProperty = PROPERTY_HIGH_APT_5					
	OR iProperty = PROPERTY_HIGH_APT_6					
	OR iProperty = PROPERTY_HIGH_APT_7					
	OR iProperty = PROPERTY_HIGH_APT_8					
	OR iProperty = PROPERTY_HIGH_APT_9					
	OR iProperty = PROPERTY_HIGH_APT_10					
	OR iProperty = PROPERTY_HIGH_APT_11					
	OR iProperty = PROPERTY_HIGH_APT_12					
	OR iProperty = PROPERTY_HIGH_APT_13					
	OR iProperty = PROPERTY_HIGH_APT_14					
	OR iProperty = PROPERTY_HIGH_APT_15					
	OR iProperty = PROPERTY_HIGH_APT_16					
	OR iProperty = PROPERTY_HIGH_APT_17
		doorModel = HEI_V_ILEV_FH_HEISTDOOR1
		
	ELIF IS_PROPERTY_CUSTOM_APARTMENT(iProperty)
		
		IF iPropertyVariation = PROPERTY_VARIATION_1
			doorModel = APA_P_MP_DOOR_APART_DOOR
		ELIF iPropertyVariation = PROPERTY_VARIATION_2
		OR iPropertyVariation = PROPERTY_VARIATION_4
		OR iPropertyVariation = PROPERTY_VARIATION_6
			doorModel = APA_P_MP_DOOR_MPA
		ELIF iPropertyVariation = PROPERTY_VARIATION_5
			doorModel = APA_P_MP_DOOR_APART_DOOR_BLACK
		ELSE
			doorModel = APA_P_MP_DOOR_STILT_DOOR
		ENDIF
		
		iBaseProperty = PROPERTY_CUSTOM_APT_1_BASE
	ELIF IS_PROPERTY_STILT_APARTMENT(iProperty, PROPERTY_STILT_APT_5_BASE_A)
		doorModel = APA_P_MP_DOOR_APART_DOOR
		iBaseProperty = PROPERTY_STILT_APT_5_BASE_A
	ELIF IS_PROPERTY_STILT_APARTMENT(iProperty, PROPERTY_STILT_APT_1_BASE_B)
		doorModel = APA_P_MP_DOOR_APART_DOOR
		iBaseProperty = PROPERTY_STILT_APT_1_BASE_B
	
	ELSE
		PRINTLN("Create_Heist_Cutscene_Doors: exiting early - iProperty is invalid ", iProperty)
		RETURN TRUE
	ENDIF 	
  	
	REQUEST_MODEL(doorModel)
	IF HAS_MODEL_LOADED(doorModel)
	
		#IF IS_DEBUG_BUILD
		PRINTLN(".KW [Heist][Cutscene]  ... Requested door model: ", GET_MODEL_NAME_FOR_DEBUG(doorModel)) 
		#ENDIF
	
		MP_PROP_OFFSET_STRUCT offsetDetails
		
		IF g_heistMocap.appartment_doors != NULL
		
			PRINTLN(".KW [Heist][Cutscene]  ...Did NOT create Door for Cutscene: ")
			g_heistMocap.appartment_doors = NULL
			DELETE_OBJECT(g_heistMocap.appartment_doors)
		ENDIF
		
		GET_POSITION_AS_OFFSET_FOR_PROPERTY(iProperty, MP_PROP_ELEMENT_HEIST_APPARTMENT_DOORS, offsetDetails, iBaseProperty)

		IF bModelHide
			CREATE_MODEL_HIDE(offsetDetails.vLoc,10.0, doorModel, TRUE)
			CREATE_MODEL_HIDE(offsetDetails.vLoc, 10.0, HEI_HEIST_APART2_DOOR, TRUE)
			CREATE_MODEL_HIDE(offsetDetails.vLoc,  10.0, V_ILEV_FH_BEDRMDOOR, TRUE)
			CREATE_MODEL_HIDE(offsetDetails.vLoc, 10.0, HEI_V_ILEV_FH_HEISTDOOR1, TRUE)
			CREATE_MODEL_HIDE(offsetDetails.vLoc, 10.0, APA_P_MP_DOOR_APART_DOOR, TRUE)
			CREATE_MODEL_HIDE(offsetDetails.vLoc, 10.0, APA_P_MP_DOOR_STILT_DOOR, TRUE)
			CREATE_MODEL_HIDE(offsetDetails.vLoc, 10.0, APA_P_MP_DOOR_APART_DOOR_BLACK, TRUE)
			CREATE_MODEL_HIDE(offsetDetails.vLoc, 10.0, APA_P_MP_DOOR_MPA, TRUE)
		ENDIF
		
		g_heistMocap.appartment_doors = CREATE_OBJECT_NO_OFFSET(doorModel, offsetDetails.vLoc, FALSE, FALSE, TRUE)
		SET_ENTITY_INVINCIBLE(g_heistMocap.appartment_doors, TRUE)
		SET_ENTITY_VISIBLE(g_heistMocap.appartment_doors, FALSE)
	
		PRINTLN(".KW [Heist][Cutscene]  ...Created Door for Cutscene: ")
		PRINTLN("\n.KW [Heist][Cutscene]  ...offsetDetails X: ", offsetDetails.vLoc.x)
		PRINTLN("\n.KW [Heist][Cutscene]  ...offsetDetails Y: ", offsetDetails.vLoc.y)
		PRINTLN("\n.KW [Heist][Cutscene]  ...offsetDetails Z: ", offsetDetails.vLoc.z)
		PRINTLN("\n.KW [Heist][Cutscene]  CURRENT PROPERTY = ", iProperty)
		RETURN TRUE	
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("\n.KW   [Heist][Cutscene] HAS_MODEL_LOADED(doorModel) = FALSE")
		#ENDIF
	ENDIF
	RETURN FALSE	
	
	
	//ELSE
	//PRINTLN(".KW [Heist][Cutscene]  ...Did NOT create Door for Cutscene. Door does not exist in cutscene")
		//RETURN TRUE
	
	//ENDIF
ENDFUNC

// PURPOSE:	Delete any cloned doors used in the cutscene 
PROC Destroy_Heist_Cutscene_Doors()
	MODEL_NAMES tempModel 
	INT Propertytocheck, iPropertyVariant
	
	IF globalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty = -1
		Propertytocheck = GET_HEIST_MISSION_STARTING_APARTMENT()
	ELSE
		Propertytocheck = globalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty 
		iPropertyVariant = globalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideVariation 
	ENDIF
	
	PRINTLN("Destroy_Heist_Cutscene_Doors: Propertytocheck: ", Propertytocheck, ", iPropertyVariant: ", iPropertyVariant)

	IF Propertytocheck = PROPERTY_BUS_HIGH_APT_1
	OR Propertytocheck = PROPERTY_BUS_HIGH_APT_2
	OR Propertytocheck = PROPERTY_BUS_HIGH_APT_3
	OR Propertytocheck = PROPERTY_BUS_HIGH_APT_4
	OR Propertytocheck= PROPERTY_BUS_HIGH_APT_5
		tempModel = HEI_HEIST_APART2_DOOR
	ELIF IS_PROPERTY_CUSTOM_APARTMENT(Propertytocheck)	
	OR IS_PROPERTY_STILT_APARTMENT(Propertytocheck, PROPERTY_STILT_APT_5_BASE_A)
	OR IS_PROPERTY_STILT_APARTMENT(Propertytocheck, PROPERTY_STILT_APT_1_BASE_B)
		IF IS_PROPERTY_CUSTOM_APARTMENT(Propertytocheck)	
			IF iPropertyVariant = PROPERTY_VARIATION_1
				tempModel = APA_P_MP_DOOR_APART_DOOR
			ELIF iPropertyVariant = PROPERTY_VARIATION_2
			OR iPropertyVariant = PROPERTY_VARIATION_4
			OR iPropertyVariant = PROPERTY_VARIATION_6
				tempModel = APA_P_MP_DOOR_MPA
			ELIF iPropertyVariant = PROPERTY_VARIATION_5
				tempModel = APA_P_MP_DOOR_APART_DOOR_BLACK
			ELSE
				tempModel = APA_P_MP_DOOR_STILT_DOOR
			ENDIF
		ELSE
			tempModel = APA_P_MP_DOOR_APART_DOOR
		ENDIF
	ELSE
		tempModel = HEI_V_ILEV_FH_HEISTDOOR1
	ENDIF
	
	VECTOR vdoor_coordinates
	PRINTLN("Destroy_Heist_Cutscene_Doors: tempModel = ", ENUM_TO_INT(tempModel))
	GET_PLAYER_PROPERTY_HEIST_LOCATION(vdoor_coordinates, MP_PROP_ELEMENT_HEIST_APPARTMENT_DOORS, Propertytocheck)
	
	REMOVE_MODEL_HIDE(vdoor_coordinates,10.0, tempModel)
	REMOVE_MODEL_HIDE(vdoor_coordinates,10.0, HEI_HEIST_APART2_DOOR)
	REMOVE_MODEL_HIDE(vdoor_coordinates,10.0, V_ILEV_FH_BEDRMDOOR)
	REMOVE_MODEL_HIDE(vdoor_coordinates,10.0, HEI_V_ILEV_FH_HEISTDOOR1)
	REMOVE_MODEL_HIDE(vdoor_coordinates,10.0, APA_P_MP_DOOR_APART_DOOR)
	REMOVE_MODEL_HIDE(vdoor_coordinates,10.0, APA_P_MP_DOOR_STILT_DOOR)
	REMOVE_MODEL_HIDE(vdoor_coordinates,10.0, APA_P_MP_DOOR_APART_DOOR_BLACK)
	REMOVE_MODEL_HIDE(vdoor_coordinates,10.0, APA_P_MP_DOOR_MPA)
	
	// Removing front door of apartment model hide
	GET_PLAYER_PROPERTY_HEIST_LOCATION(vdoor_coordinates, MP_PROP_ELEMENT_APART_ENTRANCE_DOOR, Propertytocheck)
	REMOVE_MODEL_HIDE(vdoor_coordinates,1.5, V_ILEV_MP_HIGH_FRONTDOOR)
	REMOVE_MODEL_HIDE(vdoor_coordinates,1.5, HEI_HEIST_APART2_DOOR)
	REMOVE_MODEL_HIDE(vdoor_coordinates,1.5, HEI_V_ILEV_FH_HEISTDOOR2)
	REMOVE_MODEL_HIDE(vdoor_coordinates,1.5, APA_P_MP_DOOR_APARTFRT_DOOR)
	REMOVE_MODEL_HIDE(vdoor_coordinates,1.5, APA_P_MP_DOOR_MPA2_FRNT)
	REMOVE_MODEL_HIDE(vdoor_coordinates,1.5, APA_P_MP_DOOR_STILT_DOORFRNT)
	REMOVE_MODEL_HIDE(vdoor_coordinates,1.5, APA_P_MP_DOOR_APARTFRT_DOOR_BLACK)
	REMOVE_MODEL_HIDE(vdoor_coordinates,1.5, APA_P_MP_DOOR_MPA2_FRNT)
	
	PRINTLN("\n.KW [Heist][Cutscene]   Removing front door of apartment model hide ")
		
	IF g_heistMocap.appartment_doors != NULL
		// GlobalplayerBD_FM_HeistPlanning[PARTICIPANT_ID_TO_INT()].iPreviousPropertyIndex = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
		//GET_PLAYER_PROPERTY_HEIST_LOCATION(vdoor_coordinates, MP_PROP_ELEMENT_HEIST_APPARTMENT_DOORS, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	
		PRINTLN(".KW [Heist][Cutscene] Mark Cloned Door Model As No Longer Needed in  vdoor_coordinates: ",vdoor_coordinates)
		
		DELETE_OBJECT(g_heistMocap.appartment_doors)
		SET_MODEL_AS_NO_LONGER_NEEDED(tempModel)
	ENDIF
ENDPROC

// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Determine the apartment type, room name, etc, to calculate the correct interior room for
///    the door to be registered with.
PROC Register_Appartment_Door_Cutscene()

	TEXT_LABEL_15 cutsceneDoorID = "bedroom_door"
	MP_PROP_OFFSET_STRUCT tempOffset 

	IF DOES_ENTITY_EXIST(g_heistMocap.appartment_doors)
		
		REGISTER_ENTITY_FOR_CUTSCENE(g_heistMocap.appartment_doors, cutsceneDoorID, CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME)
		PRINTLN(".KW [Heist][Cutscene]  ...Registered Door for Cutscene: ", cutsceneDoorID)
		SET_ENTITY_VISIBLE(g_heistMocap.appartment_doors, TRUE)
		SET_ENTITY_INVINCIBLE(g_heistMocap.appartment_doors, FALSE)
		
		IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE,FALSE) 
			
			INT iPropertyIndex = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
			
			IF iPropertyIndex > 0
				tempOffset = GET_BASE_INTERIOR_LOCATION(iPropertyIndex)
			ENDIF
			
			IF  iPropertyIndex = PROPERTY_BUS_HIGH_APT_1
			OR  iPropertyIndex = PROPERTY_BUS_HIGH_APT_2
			OR  iPropertyIndex = PROPERTY_BUS_HIGH_APT_3
			OR  iPropertyIndex = PROPERTY_BUS_HIGH_APT_4
			OR  iPropertyIndex = PROPERTY_BUS_HIGH_APT_5
			
				IF IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc, "hei_dlc_apart_high2_new"))
					PRINTLN(".KW [Heist][Cutscene]  ...forcing heist room for door using GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty: ", iPropertyIndex)
					PRINTLN(".KW [Heist][Cutscene]  ...forcing heist room for door using ci_HEIST_PLANNING_BUS_ROOM_KEY: ", GET_HASH_KEY("mp_apt_h_01_study"))
					FORCE_ROOM_FOR_ENTITY(g_heistMocap.appartment_doors, GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc, "hei_dlc_apart_high2_new"), ci_HEIST_PLANNING_BUS_ROOM_KEY)	
					
				ELSE
					PRINTLN(".KW [Heist][Cutscene]  ...Failed to force heist room as interior isn't ready")
				ENDIF
			
			ELIF iPropertyIndex = PROPERTY_HIGH_APT_1	
			OR iPropertyIndex = PROPERTY_HIGH_APT_2					
			OR iPropertyIndex = PROPERTY_HIGH_APT_3					
			OR iPropertyIndex = PROPERTY_HIGH_APT_4					
			OR iPropertyIndex = PROPERTY_HIGH_APT_5					
			OR iPropertyIndex = PROPERTY_HIGH_APT_6					
			OR iPropertyIndex = PROPERTY_HIGH_APT_7					
			OR iPropertyIndex = PROPERTY_HIGH_APT_8					
			OR iPropertyIndex = PROPERTY_HIGH_APT_9					
			OR iPropertyIndex = PROPERTY_HIGH_APT_10					
			OR iPropertyIndex = PROPERTY_HIGH_APT_11					
			OR iPropertyIndex = PROPERTY_HIGH_APT_12					
			OR iPropertyIndex = PROPERTY_HIGH_APT_13					
			OR iPropertyIndex = PROPERTY_HIGH_APT_14					
			OR iPropertyIndex = PROPERTY_HIGH_APT_15					
			OR iPropertyIndex = PROPERTY_HIGH_APT_16					
			OR iPropertyIndex = PROPERTY_HIGH_APT_17
			
				IF IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc, "hei_dlc_apart_high_new"))
					PRINTLN(".KW [Heist][Cutscene]  ...forcing heist room for door using GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty: ", iPropertyIndex)
					PRINTLN(".KW [Heist][Cutscene]  ...forcing heist room for door using ci_HEIST_PLANNING_ROOM_KEY: ", ci_HEIST_PLANNING_ROOM_KEY)
					FORCE_ROOM_FOR_ENTITY(g_heistMocap.appartment_doors, GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc, "hei_dlc_apart_high_new"), ci_HEIST_PLANNING_ROOM_KEY)	
				ELSE
					PRINTLN(".KW [Heist][Cutscene]  ...Failed to force heist room as interior isn't ready")
				ENDIF
				
			ELIF IS_PROPERTY_CUSTOM_APARTMENT(iPropertyIndex)
				
				INT iRoomHash = GET_CUSTOM_PLANNING_ROOM_HASH_VARIATION(g_iCurrentPropertyVariation)
				STRING sInteriorName = GET_CUSTOM_APARTMENT_NAME_VARIATION(g_iCurrentPropertyVariation)

				IF iRoomHash != INVALID_HEIST_DATA
					IF IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc, sInteriorName))
						PRINTLN(".KW [Heist][Cutscene]  ...forcing heist room for door using iCurrentlyInsideProperty: ", iPropertyIndex)
						PRINTLN(".KW [Heist][Cutscene]  ...forcing heist room for door using hash: ", iRoomHash, ", sInteriorName: ", sInteriorName)
						FORCE_ROOM_FOR_ENTITY(g_heistMocap.appartment_doors, GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc, sInteriorName), iRoomHash)	
					ELSE
						PRINTLN(".KW [Heist][Cutscene]  ...Failed to force heist room as interior isn't ready")
					ENDIF
					
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN(".KW [Heist][Cutscene]  ERROR! Cannot force room for door because room hash is INVALID. Is iPropertyVariant available at this point?")
					SCRIPT_ASSERT(".KW [Heist][Cutscene]  ERROR! Cannot force room for door because room hash is INVALID. Enter a bug for AlastairC.")
				#ENDIF
				
				ENDIF
				
			ELIF IS_PROPERTY_STILT_APARTMENT(iPropertyIndex, PROPERTY_STILT_APT_5_BASE_A)
				
				IF IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc, "apa_v_mp_stilts_a"))
					PRINTLN(".KW [Heist][Cutscene]  ...forcing heist room for door using iCurrentlyInsideProperty: ", iPropertyIndex)
					PRINTLN(".KW [Heist][Cutscene]  ...forcing heist room for door using ci_HEIST_PLANNING_STILT_A_ROOM_KEY: ", ci_HEIST_PLANNING_STILT_A_ROOM_KEY)
					FORCE_ROOM_FOR_ENTITY(g_heistMocap.appartment_doors, GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc, "apa_v_mp_stilts_a"), ci_HEIST_PLANNING_STILT_A_ROOM_KEY)	
				ELSE
					PRINTLN(".KW [Heist][Cutscene]  ...Failed to force heist room as interior isn't ready")
				ENDIF
				
			ELIF IS_PROPERTY_STILT_APARTMENT(iPropertyIndex, PROPERTY_STILT_APT_1_BASE_B)
				
				IF IS_INTERIOR_READY(GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc, "apa_v_mp_stilts_b"))
					PRINTLN(".KW [Heist][Cutscene]  ...forcing heist room for door using iCurrentlyInsideProperty: ", iPropertyIndex)
					PRINTLN(".KW [Heist][Cutscene]  ...forcing heist room for door using ci_HEIST_PLANNING_STILT_B_ROOM_KEY: ", ci_HEIST_PLANNING_STILT_B_ROOM_KEY)
					FORCE_ROOM_FOR_ENTITY(g_heistMocap.appartment_doors, GET_INTERIOR_AT_COORDS_WITH_TYPE(tempOffset.vLoc, "apa_v_mp_stilts_b"), ci_HEIST_PLANNING_STILT_B_ROOM_KEY)	
				ELSE
					PRINTLN(".KW [Heist][Cutscene]  ...Failed to force heist room as interior isn't ready")
				ENDIF
			
			ELSE

			ENDIF
		ENDIF
	ELSE
		PRINTLN(".KW [Heist][Cutscene]  Register_Appartment_Door_Cutscene: Can't register doors as they don't exist")
	ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	There are usually 4 slots in the cutscene for players - register each player, or NULL if that player slot won't be used
PROC Register_Players_For_Heist_Cutscene(INT paramCutsceneParticipants)

	INT tempLoop = 0
	BOOL registerThisPlayer = FALSE
	TEXT_LABEL_7 cutscenePlayerID = ""
	
	REPEAT paramCutsceneParticipants tempLoop
	
		registerThisPlayer = FALSE
		
		SWITCH (tempLoop)
			CASE 0		cutscenePlayerID = "MP_1"		BREAK
			CASE 1		cutscenePlayerID = "MP_2"		BREAK
			CASE 2		cutscenePlayerID = "MP_3"		BREAK
			CASE 3		cutscenePlayerID = "MP_4"		BREAK
			
			DEFAULT
				SCRIPT_ASSERT("Register_Players_For_Heist_Cutscene(): ERROR: Illegal MoCap Player ID. Tell Keith.")
				BREAK
		ENDSWITCH
		
		IF NOT (g_heistMocap.mocapPeds[tempLoop] = NULL)
			IF (DOES_ENTITY_EXIST(g_heistMocap.mocapPeds[tempLoop]))
				// Register this player for the cutscene
				IF NOT (IS_PED_INJURED(g_heistMocap.mocapPeds[tempLoop]))
					registerThisPlayer = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF (registerThisPlayer)
			// Register this player
			REGISTER_ENTITY_FOR_CUTSCENE(g_heistMocap.mocapPeds[tempLoop], cutscenePlayerID, CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME | CEO_PRESERVE_BODY_BLOOD_DAMAGE | CEO_PRESERVE_FACE_BLOOD_DAMAGE)
			PRINTLN(".KGM [Heist][Cutscene]  ...Registered Player for Cutscene: ", cutscenePlayerID, "in slot: ", tempLoop)
			
			IF (g_heistMocap.players[tempLoop] = PLAYER_ID())
				NETWORK_SET_IN_MP_CUTSCENE(TRUE)
				PRINTLN("[TS] [MSRAND] - NETWORK_SET_IN_MP_CUTSCENE(TRUE)")
				PRINTLN(".KGM [Heist][Cutscene]  ......and setting local player as IN MP Cutscene: ", cutscenePlayerID)
			ENDIF
		ELSE
			// No player to register
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, cutscenePlayerID, CU_DONT_ANIMATE_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IGNORE_MODEL_NAME | CEO_PRESERVE_BODY_BLOOD_DAMAGE | CEO_PRESERVE_FACE_BLOOD_DAMAGE)
			PRINTLN(".KGM [Heist][Cutscene]  ...Player not required for cutscene: ", cutscenePlayerID)
		ENDIF
	ENDREPEAT
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Make the Clones of all players that will be in the cutscene visible
PROC Make_All_Clone_Peds_Visible_For_Heist_Cutscene()

	INT tempLoop = 0
	
	REPEAT MAX_HEIST_MOCAP_PLAYERS tempLoop
		IF NOT (g_heistMocap.mocapPeds[tempLoop] = NULL)
			IF (DOES_ENTITY_EXIST(g_heistMocap.mocapPeds[tempLoop]))
				PRINTLN(".KGM [Heist][Cutscene] - Make_All_Clone_Peds_Visible_For_Heist_Cutscene - Setting ped index: ",tempLoop," to VISIBLE.")
				SET_ENTITY_VISIBLE(g_heistMocap.mocapPeds[tempLoop], TRUE)
				
				INT iCurrProperty, iCurrVariant, iRoomHash
				
				iCurrProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
				iCurrVariant = g_iCurrentPropertyVariation
				
				IF IS_PROPERTY_BUSINESS_APARTMENT(iCurrProperty)
				
					IF IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
						FORCE_ROOM_FOR_ENTITY(g_heistMocap.mocapPeds[tempLoop], GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()), ci_HEIST_PLANNING_BUS_ROOM_KEY)	
						PRINTLN(".KGM [Heist][Cutscene] - Make_All_Clone_Peds_Visible_For_Heist_Cutscene - Forced g_heistMocap.mocapPeds[",tempLoop,"] into business interior with room key: ", ci_HEIST_PLANNING_BUS_ROOM_KEY, ", string: mp_apt_h_01_study")
					ELSE
						PRINTLN(".KGM [Heist][Cutscene] - Make_All_Clone_Peds_Visible_For_Heist_Cutscene - ERROR! player's interior is NOT READY! Cannot force ped for room.")
					ENDIF
				
				ELIF IS_PROPERTY_CUSTOM_APARTMENT(iCurrProperty)
				
					iRoomHash = GET_CUSTOM_PLANNING_ROOM_HASH_VARIATION(iCurrVariant)
				
					IF iRoomHash != INVALID_HEIST_DATA
				
						IF IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
							FORCE_ROOM_FOR_ENTITY(g_heistMocap.mocapPeds[tempLoop], GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()), iRoomHash)	
							PRINTLN(".KGM [Heist][Cutscene] - Make_All_Clone_Peds_Visible_For_Heist_Cutscene - Forced g_heistMocap.mocapPeds[",tempLoop,"] into custom interior with room key: ", iRoomHash)
						ELSE
							PRINTLN(".KGM [Heist][Cutscene] - Make_All_Clone_Peds_Visible_For_Heist_Cutscene - ERROR! player's interior is NOT READY! Cannot force ped for room.")
						ENDIF
					
					ELSE
						PRINTLN(".KGM [Heist][Cutscene] - Make_All_Clone_Peds_Visible_For_Heist_Cutscene - ERROR! iRoomHash is INVALID, verify that apartment variant is valid: ", iCurrVariant)
						SCRIPT_ASSERT(".KGM [Heist][Cutscene] - Make_All_Clone_Peds_Visible_For_Heist_Cutscene - ERROR! iRoomHash is INVALID, verify that apartment variant is valid. Enter a bug for AlastairC.")
					ENDIF
					
				ELIF IS_PROPERTY_STILT_APARTMENT(iCurrProperty, PROPERTY_STILT_APT_5_BASE_A)
				
					IF IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
						FORCE_ROOM_FOR_ENTITY(g_heistMocap.mocapPeds[tempLoop], GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()), ci_HEIST_PLANNING_STILT_A_ROOM_KEY)	
						PRINTLN(".KGM [Heist][Cutscene] - Make_All_Clone_Peds_Visible_For_Heist_Cutscene - Forced g_heistMocap.mocapPeds[",tempLoop,"] into custom interior with room key: ", ci_HEIST_PLANNING_STILT_A_ROOM_KEY, ", string: mr_mp_stilts_a_plan")
					ELSE
						PRINTLN(".KGM [Heist][Cutscene] - Make_All_Clone_Peds_Visible_For_Heist_Cutscene - ERROR! player's interior is NOT READY! Cannot force ped for room.")
					ENDIF
				
				ELIF IS_PROPERTY_STILT_APARTMENT(iCurrProperty, PROPERTY_STILT_APT_1_BASE_B)
				
					IF IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
						FORCE_ROOM_FOR_ENTITY(g_heistMocap.mocapPeds[tempLoop], GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()), ci_HEIST_PLANNING_STILT_B_ROOM_KEY)	
						PRINTLN(".KGM [Heist][Cutscene] - Make_All_Clone_Peds_Visible_For_Heist_Cutscene - Forced g_heistMocap.mocapPeds[",tempLoop,"] into custom interior with room key: ", ci_HEIST_PLANNING_STILT_B_ROOM_KEY, ", string: mr_mp_stilts_b_plan")
					ELSE
						PRINTLN(".KGM [Heist][Cutscene] - Make_All_Clone_Peds_Visible_For_Heist_Cutscene - ERROR! player's interior is NOT READY! Cannot force ped for room.")
					ENDIF
				
				ELSE
				
					IF IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
						FORCE_ROOM_FOR_ENTITY(g_heistMocap.mocapPeds[tempLoop], GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()), ci_HEIST_PLANNING_ROOM_KEY)
						PRINTLN(".KGM [Heist][Cutscene] - Make_All_Clone_Peds_Visible_For_Heist_Cutscene - Forced g_heistMocap.mocapPeds[",tempLoop,"] into old interior with room key: ", ci_HEIST_PLANNING_ROOM_KEY, ", string: rm_high_planning")
					ELSE
						PRINTLN(".KGM [Heist][Cutscene] - Make_All_Clone_Peds_Visible_For_Heist_Cutscene - ERROR! player's interior is NOT READY! Cannot force ped for room.")
					ENDIF
					
				ENDIF
				
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC


FUNC BOOL IS_THIS_CUTSCENE_TUTORIAL_MIDPOINT(TEXT_LABEL_23 paramCutsceneName)

	IF ARE_STRINGS_EQUAL(paramCutsceneName, "MPH_TUT_MID") // Bit hacky, maybe we should look into a better way of doing this?
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_CUTSCENE_TUTORIAL_START(TEXT_LABEL_23 paramCutsceneName)

	IF ARE_STRINGS_EQUAL(paramCutsceneName, "MPH_TUT_INT") // Bit hacky, maybe we should look into a better way of doing this?
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_CUTSCENE_PRISON_BESPOKE(TEXT_LABEL_23 paramCutsceneName)

	IF ARE_STRINGS_EQUAL(paramCutsceneName, "APA_PRI_INT") // Bit hacky, maybe we should look into a better way of doing this?
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_CUTSCENE_NARCOTICS_BESPOKE(TEXT_LABEL_23 paramCutsceneName)

	IF ARE_STRINGS_EQUAL(paramCutsceneName, "APA_NAR_INT") // Bit hacky, maybe we should look into a better way of doing this?
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Download the number of concat parts in the provided cutscene. May take several frames to load, only returns true for 1 frame before cleaning up.
FUNC BOOL Downloaded_Heist_Cutscene_Data_File(STRING paramCutsceneName, INT& iTotalConcatParts)

	PRINTLN(".KGM [Heist][Cutscene] - Downloaded_Heist_Cutscene_Data_File - Checking if cutscene cut file is ready...")
	
	REQUEST_CUT_FILE(paramCutsceneName)
	
	IF HAS_CUT_FILE_LOADED(paramCutsceneName)
		iTotalConcatParts = GET_CUT_FILE_CONCAT_COUNT(paramCutsceneName)
		REMOVE_CUT_FILE(paramCutsceneName)
		PRINTLN(".KGM [Heist][Cutscene] - Downloaded_Heist_Cutscene_Data_File - HAS_CUT_FILE_LOADED() = TRUE, value: ", iTotalConcatParts)
		RETURN TRUE
	ELSE
		PRINTLN(".KGM [Heist][Cutscene] - Downloaded_Heist_Cutscene_Data_File - HAS_CUT_FILE_LOADED() = FALSE for cutscene: ", paramCutsceneName)
		RETURN FALSE
	ENDIF

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	The cutscene is exported at the MILO of a specific apartment, we need to move it and orientate it to the apartment the heist is being launched in
PROC Move_Heist_Cutscene_To_Heist_Apartment(INT paramConcatParts, INT iProperty = -1, BOOL bUseBasecoords = FALSE, INT iPositionID = MP_PROP_ELEMENT_HEIST_PLANNING_CUTSCENE)

	//UNUSED_PARAMETER(bUseBasecoords)

	IF ARE_STRINGS_EQUAL(g_heistMocap.cutsceneName, "MPH_HUM_INT")
	OR ARE_STRINGS_EQUAL(g_heistMocap.cutsceneName, "APA_PRI_INT")
	OR ARE_STRINGS_EQUAL(g_heistMocap.cutsceneName, "MPH_PRI_FIN_INT")
	OR ARE_STRINGS_EQUAL(g_heistMocap.cutsceneName, "MPH_PAC_FIN_INT")
	OR ARE_STRINGS_EQUAL(g_heistMocap.cutsceneName, "MPH_TUT_INT")
	OR ARE_STRINGS_EQUAL(g_heistMocap.cutsceneName, "MPH_NAR_FIN_INT")
	OR ARE_STRINGS_EQUAL(g_heistMocap.cutsceneName, "MPH_PRI_UNF_EXT")  // Prison Break Unfinished business document drop off
	OR ARE_STRINGS_EQUAL(g_heistMocap.cutsceneName, "MPH_HUM_KEY_EXT")
	OR ARE_STRINGS_EQUAL(g_heistMocap.cutsceneName, "mph_pri_sta_ext") 
	OR ARE_STRINGS_EQUAL(g_heistMocap.cutsceneName, "mph_pri_sta_mcs2")

		PRINTLN(".KGM [Heist][Cutscene] Prop hiding required for active cutscene: ", g_heistMocap.cutsceneName)
			
		VECTOR vHeistCoronaLocation
		GET_PLAYER_PROPERTY_HEIST_LOCATION(vHeistCoronaLocation, MP_PROP_ELEMENT_HEIST_PLAN_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		
		g_heistMocap.objOfficeChair = GET_CLOSEST_OBJECT_OF_TYPE(vHeistCoronaLocation, 4.0, PROP_OFF_CHAIR_01, FALSE)
		
		IF g_heistMocap.objOfficeChair != NULL
			PRINTLN(".KGM [Heist][Cutscene] Setting prop: PROP_OFF_CHAIR_01 to INVISIBLE.")
			SET_ENTITY_VISIBLE(g_heistMocap.objOfficeChair, FALSE)
		ELSE
			PRINTLN(".KGM [Heist][Cutscene] Could not locate prop: PROP_OFF_CHAIR_01 in area, not hiding...")
		ENDIF
	ELSE
		PRINTLN(".KGM [Heist][Cutscene] Prop hiding NOT required for active cutscene")
	ENDIF

	START_CUTSCENE()
	
	IF iProperty = INVALID_HEIST_DATA
		iProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
	ENDIF

	VECTOR vCutsceneLoc, vCutsceneRot, vConcatSceneLoc, vConcatSceneRot

	// Add all apartment variations here. 
	IF bUseBasecoords = TRUE
		GET_PLAYER_PROPERTY_HEIST_LOCATION(vCutsceneLoc, MP_PROP_ELEMENT_HEIST_CELEBRATION_DLC_MPH_FIN_CEL_APT1, iProperty)
		GET_PLAYER_PROPERTY_HEIST_ROTATION(vCutsceneRot, MP_PROP_ELEMENT_HEIST_CELEBRATION_DLC_MPH_FIN_CEL_APT1, iProperty)
	ELSE
		GET_PLAYER_PROPERTY_HEIST_LOCATION(vCutsceneLoc, iPositionID, iProperty)
		GET_PLAYER_PROPERTY_HEIST_ROTATION(vCutsceneRot, iPositionID, iProperty)
	ENDIF
	PRINTLN(".KGM [Heist][Cutscene] Apartment: ", iProperty, " - Adjusted - Coords: ", vCutsceneLoc, ", Heading: ", vCutsceneRot.Z, ", in property: ", iProperty, ", iPositionID: ", iPositionID)
	
	IF paramConcatParts > 0
		PRINTLN(".KGM [Heist][Cutscene] Loaded cutscene has ", paramConcatParts, " total concat shots. Moving to scene pre-orientation...")
		GET_PLAYER_PROPERTY_HEIST_LOCATION(vConcatSceneLoc, MP_PROP_ELEMENT_HEIST_CUTSCENE_CONCAT_BESPOKE, iProperty)
		GET_PLAYER_PROPERTY_HEIST_ROTATION(vConcatSceneRot, MP_PROP_ELEMENT_HEIST_CUTSCENE_CONCAT_BESPOKE, iProperty)
	ELSE
		PRINTLN(".KGM [Heist][Cutscene] Loaded cutscene has invalid total number of concat shots. Actual: ",paramConcatParts,", Assumed: 1")
		paramConcatParts = 1
	ENDIF
	
	INT index
	FOR index = 0 TO (paramConcatParts-1)
		
		IF IS_THIS_CUTSCENE_PRISON_BESPOKE(g_heistMocap.cutsceneName)
			IF index = 2 // Stilt A
			OR index = 3 // Stilt B
			OR index = 4 // Custom 1
				PRINTLN(".KGM [Heist][Cutscene] Custom PRI concat position STILT. Loc: ",vConcatSceneLoc,", Rot: ",vConcatSceneRot)
				SET_CUTSCENE_ORIGIN(vConcatSceneLoc, vConcatSceneRot.Z, index)
			ELSE
				PRINTLN(".KGM [Heist][Cutscene] Orientating shot: ",index," at Loc/Rot: ", vCutsceneLoc, ", ", vCutsceneRot, ", heading: ", vCutsceneRot.Z)
				SET_CUTSCENE_ORIGIN(vCutsceneLoc, vCutsceneRot.Z, index)
			ENDIF
		ELIF IS_THIS_CUTSCENE_NARCOTICS_BESPOKE(g_heistMocap.cutsceneName)
			IF index = 3 // Stilt A
			OR index = 4 // Stilt B
			OR index = 5 // Custom 1
				PRINTLN(".KGM [Heist][Cutscene] Custom NAR concat position STILT. Loc: ",vConcatSceneLoc,", Rot: ",vConcatSceneRot)
				SET_CUTSCENE_ORIGIN(vConcatSceneLoc, vConcatSceneRot.Z, index)
			ELSE
				PRINTLN(".KGM [Heist][Cutscene] Orientating shot: ",index," at Loc/Rot: ", vCutsceneLoc, ", ", vCutsceneRot, ", heading: ", vCutsceneRot.Z)
				SET_CUTSCENE_ORIGIN(vCutsceneLoc, vCutsceneRot.Z, index)
			ENDIF
		ELSE
			PRINTLN(".KGM [Heist][Cutscene] Orientating shot: ",index," at Loc/Rot: ", vCutsceneLoc, ", ", vCutsceneRot, ", heading: ", vCutsceneRot.Z)
			SET_CUTSCENE_ORIGIN(vCutsceneLoc, vCutsceneRot.Z, index)
		ENDIF
		
	ENDFOR
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Start a safety timeout timer in case cutscene triggering gets stuck
PROC Start_Mocap_Triggering_Safety_Timeout()

	// Check if a quick heist timeout is required
	#IF IS_DEBUG_BUILD
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_HeistCutQuickTimeout"))
			INT quickTimeout = 30000
			PRINTLN(".KGM [Heist][Cutscene] Setting Quick Safety Timeout to ", quickTimeout, "msec (because -sc_HeistCutQuickTimeout is active)")
			g_heistMocap.safetyTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), quickTimeout)
			
			EXIT
		ENDIF
	#ENDIF

	PRINTLN(".KGM [Heist][Cutscene] Setting Safety Timeout to ", HEIST_MOCAP_SAFETY_TIMEOUT_msec, "msec")
	g_heistMocap.safetyTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), HEIST_MOCAP_SAFETY_TIMEOUT_msec)
	
ENDPROC

/// PURPOSE:
///    Start a timer for the e4stablishing shot. This is generally 3 seconds, but if loading has not finished, it may take longer.
PROC START_MOCAP_ESTABLISHING_SHOT_TIMER()
	PRINTLN(".KGM [Heist][Cutscene] Setting Establishing Shot Timer to ", HEIST_MOCAP_ESTABLISHING_SHOT_TIME, "msec")
	g_heistMocap.establishingTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), HEIST_MOCAP_ESTABLISHING_SHOT_TIME)
ENDPROC


/// PURPOSE:
///    Start a timer for the corona wait timer. This is a 1 second delay for the cleanup of the previous anim.
///    HEIST_CORONA_RELEASE_TIME can be raised if the cleanup is still occuring too early- but highly doubtful.
///    Fix for B* 2150554 RowanJ
PROC START_CORONA_RELEASE_TIMER()
	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - Setting Corona release timer to ", HEIST_CORONA_RELEASE_TIME, " msec.")
//	#IF IS_DEBUG_BUILD
//	IF GET_COMMANDLINE_PARAM_EXISTS("heist_spew_network_time")
//		PRINTLN("[RBJ][HEIST_ANIMS] - START_CORONA_RELEASSE_TIMER - HEIST_CORONA_RELEASE_TIME : ", HEIST_CORONA_RELEASE_TIME)
//		PRINTLN("[RBJ][HEIST_ANIMS] - START_CORONA_RELEASSE_TIMER - GET_NETWORK_TIME()        : ", NATIVE_TO_INT(GET_NETWORK_TIME()))
//		PRINTLN("[RBJ][HEIST_ANIMS] - START_CORONA_RELEASSE_TIMER - GET_TIME_OFFSET(GET_NETWORK_TIME(), HEIST_CORONA_RELEASE_TIME): ", NATIVE_TO_INT(GET_TIME_OFFSET(GET_NETWORK_TIME(), HEIST_CORONA_RELEASE_TIME)))
//	ENDIF
//	#ENDIF
	g_heistMocap.timerCoronaRelease = GET_TIME_OFFSET(GET_NETWORK_TIME(), HEIST_CORONA_RELEASE_TIME)
	g_heistMocap.bCoronaReleaseTimerActive = TRUE
ENDPROC



// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the safety timeout time has elapsed
FUNC BOOL Has_Mocap_Triggering_Safety_Timeout_Expired()
	RETURN (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_heistMocap.safetyTimeout))
ENDFUNC


// PURPOSE:	Check if the safety timeout time has elapsed
FUNC BOOL HAS_MOCAP_ESTABLISHING_SHOT_TIMER_EXPIRED()
	RETURN (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_heistMocap.establishingTimer))
ENDFUNC


// PURPOSE:	Check if the front end timer has elapsed.
//    Fix for B* 2150554 RowanJ
FUNC BOOL HAS_CORONA_RELEASE_TIMER_EXPIRED()
//	#IF IS_DEBUG_BUILD
//	IF GET_COMMANDLINE_PARAM_EXISTS("heist_spew_network_time")
//		PRINTLN("[RBJ][HEIST_ANIMS] - HAS_CORONA_RELEASE_TIMER_EXPIRED - Timer left on timer            : ", GET_TIME_DIFFERENCE(g_heistMocap.timerCoronaRelease, GET_NETWORK_TIME()), " msec.")	//	Spams each frame for duration of the timer.
//		PRINTLN("[RBJ][HEIST_ANIMS] - HAS_CORONA_RELEASE_TIMER_EXPIRED - GET_NETWORK_TIME()             : ", NATIVE_TO_INT(GET_NETWORK_TIME()))
//		PRINTLN("[RBJ][HEIST_ANIMS] - HAS_CORONA_RELEASE_TIMER_EXPIRED - IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_heistMocap.timerCoronaRelease): ", PICK_STRING(IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_heistMocap.timerCoronaRelease), "TRUE", "FALSE"))
//	ENDIF
//	#ENDIF
	RETURN IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_heistMocap.timerCoronaRelease)
ENDFUNC


///	  Reset the flag to allow for multiple passes of the timer.
//    Fix for B* 2150554 RowanJ
PROC CLEAR_CORONA_RELEASE_TIMER()
	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - CLEAR_CORONA_RELEASE_TIMER - Resetting corona release timer to ", HEIST_CORONA_RELEASE_TIME, " msec.")
	g_heistMocap.bCoronaReleaseTimerActive = FALSE
ENDPROC



// -----------------------------------------------------------------------------------------------------------

// ===========================================================================================================
//      INTRO / OUTRO HEIST ANIMS
// ===========================================================================================================
/// PURPOSE:
///    Start a timer for animation timeout.
PROC START_HEIST_ANIM_TIMER(INT iDuration, g_structHeistAnims& sAnimStruct)
	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - Setting Phone Anim Timer to ", iDuration, "msec")
	sAnimStruct.timerAnimTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), iDuration)
ENDPROC


/// PURPOSE:
///    Check if the animation has timed out.
FUNC BOOL HAS_HEIST_ANIM_TIMER_EXPIRED(g_structHeistAnims& sAnimStruct)
	RETURN (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), sAnimStruct.timerAnimTimeout))
ENDFUNC


/// PURPOSE:
///    Hide all active players in the same session (quite expensive, but required).
PROC HIDE_ALL_OTHER_PLAYERS_FOR_HEIST_INTRO()
	DISABLE_SCRIPT_HUD_THIS_FRAME(HUDPART_ALL_OVERHEADS)
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)		
			IF INT_TO_PLAYERINDEX(i) != PLAYER_ID()
				SET_PLAYER_INVISIBLE_LOCALLY(INT_TO_PLAYERINDEX(i))
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Lookup the pre-defined data when making a new heist animation module.
PROC SET_HEIST_ANIM_DETAILS_FROM_ACTIVE(g_structHeistAnims& sAnimStruct)

	INT index
	
	FOR index = 0 TO (MAX_HEIST_ANIM_PROPS-1)
		sAnimStruct.sHeistAnimCams[index] = ""
		sAnimStruct.sHeistAnimModels[index] = DUMMY_MODEL_FOR_SCRIPT
	ENDFOR

	SWITCH sAnimStruct.sActiveAnim
	
		CASE HEIST_SELECTED_ANIM_NONE
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: NONE")
		
			sAnimStruct.sHeistAnimDictionary = ""
			sAnimStruct.sHeistAnimName = ""
			
			BREAK
			
		CASE HEIST_SELECTED_ANIM_PHONE

			
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_PHONE")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@PHONE"
			sAnimStruct.sHeistAnimName = "Phone_intro"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_PHONE, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_PHONE, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "phone_intro_cam1" 
			sAnimStruct.sHeistAnimCams[1] = "phone_intro_cam2"

			sAnimStruct.sHeistAnimModels[0] = Prop_NPC_Phone
			sAnimStruct.sHeistAnimPropName[0] = "PHONE_INTRO_PHONE"
			
			IF NOT IS_PS3_VERSION()
			AND NOT IS_XBOX360_VERSION()	
				sAnimStruct.mnLightRig = HEI_MPH_SELECTCLOTHSLRIG_02
			ELSE
				sAnimStruct.mnLightRig = DUMMY_MODEL_FOR_SCRIPT
			ENDIF
			 
			BREAK
			
		CASE HEIST_SELECTED_ANIM_PICKUP
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_PICKUP")
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_WHISKY_1
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_WHISKY_1")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@WHISKY@WINDOW"
			sAnimStruct.sHeistAnimName = "whisky_window_part_one"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_WHISKY_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_WHISKY_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "whisky_window_part_one_cam"
			
			sAnimStruct.sHeistAnimModels[0] = P_TUMBLER_01_S // Tumbler
			sAnimStruct.sHeistAnimPropName[0] = "WHISKY_WINDOW_PART_ONE_GLASS"
			
			sAnimStruct.bFreezeOnLastFrame = TRUE
			sAnimStruct.bPreLoadScene = TRUE
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_WHISKY_2
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_WHISKY_2")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@WHISKY@WINDOW"
			sAnimStruct.sHeistAnimName = "whisky_window_part_two"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_WHISKY_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_WHISKY_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			
			sAnimStruct.sHeistAnimCams[0] = "whisky_window_part_two_cam"
			
			sAnimStruct.sHeistAnimAudioDict = "MP_PLAYER_APARTMENT"
			sAnimStruct.sHeistAudio[0].tl31AudioName = "DOOR_BUZZ"
			sAnimStruct.sHeistAudio[0].fAudioCue = 0.275
			sAnimStruct.sHeistAudio[0].vAudioLoc = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].house.vBuzzerLoc
			sAnimStruct.sHeistAudio[0].iErrorMarginPercent = 2
			
			sAnimStruct.sHeistAnimModels[0] = P_TUMBLER_01_S // Tumbler
			sAnimStruct.sHeistAnimPropName[0] = "WHISKY_WINDOW_PART_TWO_GLASS"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			sAnimStruct.bPlayStartPostFX = TRUE
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_WHISKY_IDLE
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_WHISKY_IDLE")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@WHISKY@WINDOW"
			sAnimStruct.sHeistAnimName = "WHISKY_WINDOW_IDLE"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_WHISKY_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_WHISKY_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			
			sAnimStruct.sHeistAnimCams[0] = "WHISKY_WINDOW_IDLE_cam"
			
			sAnimStruct.sHeistAnimModels[0] = P_TUMBLER_01_S // Tumbler
			sAnimStruct.sHeistAnimPropName[0] = "WHISKY_WINDOW_IDLE_glass"
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_WINE_1
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_WINE_1")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@WINE@WINDOW"
			sAnimStruct.sHeistAnimName = "wine_window_part_one"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_WINE_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_WINE_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			
			sAnimStruct.sHeistAnimCams[0] = "wine_window_part_one_cam"
			
			sAnimStruct.sHeistAnimModels[0] = P_WINE_GLASS_S // Wine glass
			sAnimStruct.sHeistAnimPropName[0] = "WINE_WINDOW_PART_ONE_GLASS"
			
			sAnimStruct.bFreezeOnLastFrame = TRUE
			sAnimStruct.bPreLoadScene = TRUE
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_WINE_2
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_WINE_2")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@WINE@WINDOW"
			sAnimStruct.sHeistAnimName = "wine_window_part_two"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_WINE_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_WINE_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			
			sAnimStruct.sHeistAnimCams[0] = "wine_window_part_two_cam"
			
			sAnimStruct.sHeistAnimAudioDict = "MP_PLAYER_APARTMENT"
			sAnimStruct.sHeistAudio[0].tl31AudioName = "DOOR_BUZZ"
			sAnimStruct.sHeistAudio[0].fAudioCue = 0.230
			sAnimStruct.sHeistAudio[0].vAudioLoc = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].house.vBuzzerLoc
			sAnimStruct.sHeistAudio[0].iErrorMarginPercent = 2
			
			sAnimStruct.sHeistAnimModels[0] = P_WINE_GLASS_S // Wine glass
			sAnimStruct.sHeistAnimPropName[0] = "WINE_WINDOW_PART_TWO_GLASS"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			sAnimStruct.bPlayStartPostFX = TRUE
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_WINE_IDLE
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_WINE_IDLE")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@WINE@WINDOW"
			sAnimStruct.sHeistAnimName = "WINE_WINDOW_IDLE"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_WINE_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_WINE_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			
			sAnimStruct.sHeistAnimCams[0] = "WINE_WINDOW_IDLE_cam"
			
			sAnimStruct.sHeistAnimModels[0] = P_WINE_GLASS_S // Wine glass
			sAnimStruct.sHeistAnimPropName[0] = "WINE_WINDOW_IDLE_glass"
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_COUCH_M_1
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_COUCH_M_1")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@PHONE_COUCH@MALE"
			sAnimStruct.sHeistAnimName = "Phone_couch_male_part_one"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_COUCH_M_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_COUCH_M_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			
			sAnimStruct.sHeistAnimCams[0] = "Phone_couch_male_part_one_cam"
			
			sAnimStruct.sHeistAnimModels[0] = Prop_NPC_Phone
			sAnimStruct.sHeistAnimPropName[0] = "PHONE_COUCH_MALE_PART_ONE_PHONE"
			
			sAnimStruct.bFreezeOnLastFrame = TRUE
			sAnimStruct.bPreLoadScene = TRUE
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_COUCH_M_2
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_COUCH_M_2")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@PHONE_COUCH@MALE"
			sAnimStruct.sHeistAnimName = "Phone_couch_male_part_two"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_COUCH_M_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_COUCH_M_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			
			sAnimStruct.sHeistAnimCams[0] = "Phone_couch_male_part_two_cam"
			
			sAnimStruct.sHeistAnimAudioDict = "MP_PLAYER_APARTMENT"
			sAnimStruct.sHeistAudio[0].tl31AudioName = "DOOR_BUZZ"
			sAnimStruct.sHeistAudio[0].fAudioCue = 0.150
			sAnimStruct.sHeistAudio[0].vAudioLoc = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].house.vBuzzerLoc
			sAnimStruct.sHeistAudio[0].iErrorMarginPercent = 2
			
			sAnimStruct.sHeistAnimModels[0] = Prop_NPC_Phone
			sAnimStruct.sHeistAnimPropName[0] = "PHONE_COUCH_MALE_PART_TWO_PHONE"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			sAnimStruct.bPlayStartPostFX = TRUE
			
			BREAK
			
		CASE HEIST_SELECTED_ANIM_COUCH_M_IDLE
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_COUCH_M_IDLE")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@PHONE_COUCH@MALE"
			sAnimStruct.sHeistAnimName = "PHONE_COUCH_MALE_IDLE"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_COUCH_M_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_COUCH_M_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			
			sAnimStruct.sHeistAnimCams[0] = "PHONE_COUCH_MALE_IDLE_cam"
			
			sAnimStruct.sHeistAnimModels[0] = Prop_NPC_Phone
			sAnimStruct.sHeistAnimPropName[0] = "PHONE_COUCH_MALE_IDLE_phone"
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_COUCH_F_1
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_COUCH_F_1")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@PHONE_COUCH@FEMALE"
			sAnimStruct.sHeistAnimName = "Phone_couch_female_part_one"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_COUCH_F_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_COUCH_F_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			
			sAnimStruct.sHeistAnimCams[0] = "Phone_couch_female_part_one_cam"
			
			sAnimStruct.sHeistAnimModels[0] = Prop_NPC_Phone
			sAnimStruct.sHeistAnimPropName[0] = "PHONE_COUCH_FEMALE_PART_ONE_PHONE"
			
			sAnimStruct.bFreezeOnLastFrame = TRUE
			sAnimStruct.bPreLoadScene = TRUE
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_COUCH_F_2
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_COUCH_F_2")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@PHONE_COUCH@FEMALE"
			sAnimStruct.sHeistAnimName = "Phone_couch_female_part_two"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_COUCH_F_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_COUCH_F_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "Phone_couch_female_part_two_cam"
			
			sAnimStruct.sHeistAnimAudioDict = "MP_PLAYER_APARTMENT"
			sAnimStruct.sHeistAudio[0].tl31AudioName = "DOOR_BUZZ"
			sAnimStruct.sHeistAudio[0].fAudioCue = 0.180
			sAnimStruct.sHeistAudio[0].vAudioLoc = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].house.vBuzzerLoc
			sAnimStruct.sHeistAudio[0].iErrorMarginPercent = 2
			
			sAnimStruct.sHeistAnimModels[0] = Prop_NPC_Phone
			sAnimStruct.sHeistAnimPropName[0] = "PHONE_COUCH_FEMALE_PART_TWO_PHONE"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			sAnimStruct.bPlayStartPostFX = TRUE

			BREAK
		
		CASE HEIST_SELECTED_ANIM_COUCH_F_IDLE
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_COUCH_F_IDLE")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@PHONE_COUCH@FEMALE"
			sAnimStruct.sHeistAnimName = "PHONE_COUCH_FEMALE_IDLE"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_COUCH_F_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_COUCH_F_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "PHONE_COUCH_FEMALE_IDLE_cam"
			
			sAnimStruct.sHeistAnimModels[0] = Prop_NPC_Phone
			sAnimStruct.sHeistAnimPropName[0] = "PHONE_COUCH_FEMALE_IDLE_phone"

			BREAK
			
		CASE HEIST_SELECTED_ANIM_TV_M_1
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_TV_M_1")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@MALE@TV"
			sAnimStruct.sHeistAnimName = "tv_part_one"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_TV_M_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_TV_M_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "tv_part_one_cam"
			
			sAnimStruct.sHeistAnimModels[0] = PROP_CS_REMOTE_01
			sAnimStruct.sHeistAnimPropName[0] = "TV_PART_ONE_REMOTE"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			sAnimStruct.bFreezeOnLastFrame = TRUE
			sAnimStruct.bPreLoadScene = TRUE
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_TV_M_2
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_TV_M_2")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@MALE@TV"
			sAnimStruct.sHeistAnimName = "tv_part_two"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_TV_M_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_TV_M_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "tv_part_two_cam"
			
			sAnimStruct.sHeistAnimAudioDict = "MP_PLAYER_APARTMENT"
			sAnimStruct.sHeistAudio[0].tl31AudioName = "DOOR_BUZZ"
			sAnimStruct.sHeistAudio[0].fAudioCue = 0.230
			sAnimStruct.sHeistAudio[0].vAudioLoc = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].house.vBuzzerLoc
			sAnimStruct.sHeistAudio[0].iErrorMarginPercent = 2
			
			sAnimStruct.sHeistAnimModels[0] = PROP_CS_REMOTE_01
			sAnimStruct.sHeistAnimPropName[0] = "TV_PART_TWO_REMOTE"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			sAnimStruct.bPlayStartPostFX = TRUE
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_TV_M_IDLE
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_TV_M_IDLE")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@MALE@TV"
			sAnimStruct.sHeistAnimName = "tv_part_one_loop"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_TV_M_IDLE, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_TV_M_IDLE, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "tv_part_one_loop_cam"
			
			sAnimStruct.sHeistAnimModels[0] = PROP_CS_REMOTE_01
			sAnimStruct.sHeistAnimPropName[0] = "TV_PART_ONE_LOOP_REMOTE"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_TV_F_1
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_TV_F_1")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@FEMALE@TV"
			sAnimStruct.sHeistAnimName = "tv_part_one"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_TV_F_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_TV_F_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "tv_part_one_cam"
			
			sAnimStruct.sHeistAnimModels[0] = PROP_CS_REMOTE_01
			sAnimStruct.sHeistAnimPropName[0] = "TV_PART_ONE_REMOTE"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			sAnimStruct.bFreezeOnLastFrame = TRUE
			sAnimStruct.bPreLoadScene = TRUE
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_TV_F_2
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_TV_F_2")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@FEMALE@TV"
			sAnimStruct.sHeistAnimName = "tv_part_two"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_TV_F_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_TV_F_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "tv_part_two_cam"
			
			sAnimStruct.sHeistAnimAudioDict = "MP_PLAYER_APARTMENT"
			sAnimStruct.sHeistAudio[0].tl31AudioName = "DOOR_BUZZ"
			sAnimStruct.sHeistAudio[0].fAudioCue = 0.230
			sAnimStruct.sHeistAudio[0].vAudioLoc = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].house.vBuzzerLoc
			sAnimStruct.sHeistAudio[0].iErrorMarginPercent = 2
			
			sAnimStruct.sHeistAnimModels[0] = PROP_CS_REMOTE_01
			sAnimStruct.sHeistAnimPropName[0] = "TV_PART_TWO_REMOTE"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			sAnimStruct.bPlayStartPostFX = TRUE
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_TV_F_IDLE

			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_TV_F_IDLE")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@FEMALE@TV"
			sAnimStruct.sHeistAnimName = "tv_part_one_loop"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_TV_F_IDLE, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_TV_F_IDLE, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "tv_part_one_loop_cam"
			
			sAnimStruct.sHeistAnimModels[0] = PROP_CS_REMOTE_01
			sAnimStruct.sHeistAnimPropName[0] = "TV_PART_ONE_LOOP_REMOTE"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"

			BREAK
			
		CASE HEIST_SELECTED_ANIM_WINDOW_M_1
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_WINDOW_M_1")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@MALE@WINDOW"
			sAnimStruct.sHeistAnimName = "window_part_one"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_M_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_M_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "window_part_one_cam"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			sAnimStruct.bFreezeOnLastFrame = TRUE
			sAnimStruct.bPreLoadScene = TRUE
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_WINDOW_M_2
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_WINDOW_M_2")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@MALE@WINDOW"
			sAnimStruct.sHeistAnimName = "window_part_two"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_M_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_M_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "window_part_two_cam"
			
			sAnimStruct.sHeistAnimAudioDict = "MP_PLAYER_APARTMENT"
			sAnimStruct.sHeistAudio[0].tl31AudioName = "DOOR_BUZZ"
			sAnimStruct.sHeistAudio[0].fAudioCue = 0.230
			sAnimStruct.sHeistAudio[0].vAudioLoc = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].house.vBuzzerLoc
			sAnimStruct.sHeistAudio[0].iErrorMarginPercent = 2
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			sAnimStruct.bPlayStartPostFX = TRUE
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_WINDOW_M_IDLE
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_WINDOW_M_IDLE")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@MALE@WINDOW"
			sAnimStruct.sHeistAnimName = "window_part_one_loop"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_M_IDLE, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_M_IDLE, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "window_part_one_loop_cam"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_WINDOW_F_1
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_WINDOW_F_1")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@FEMALE@WINDOW"
			sAnimStruct.sHeistAnimName = "window_part_one"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_F_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_F_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "window_part_one_cam"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			sAnimStruct.bFreezeOnLastFrame = TRUE
			sAnimStruct.bPreLoadScene = TRUE
			
			BREAK
			
		CASE HEIST_SELECTED_ANIM_WINDOW_F_2
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_WINDOW_F_2")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@FEMALE@WINDOW"
			sAnimStruct.sHeistAnimName = "window_part_two"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_F_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_F_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "window_part_two_cam"
			
			sAnimStruct.sHeistAnimAudioDict = "MP_PLAYER_APARTMENT"
			sAnimStruct.sHeistAudio[0].tl31AudioName = "DOOR_BUZZ"
			sAnimStruct.sHeistAudio[0].fAudioCue = 0.230
			sAnimStruct.sHeistAudio[0].vAudioLoc = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].house.vBuzzerLoc
			sAnimStruct.sHeistAudio[0].iErrorMarginPercent = 2
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			sAnimStruct.bPlayStartPostFX = TRUE
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_WINDOW_F_IDLE
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_WINDOW_F_IDLE")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@FEMALE@WINDOW"
			sAnimStruct.sHeistAnimName = "window_part_one_loop"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_F_IDLE, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_F_IDLE, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "window_part_one_loop_cam"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
		
			BREAK
			
		CASE HEIST_SELECTED_ANIM_KITCHEN_M_1
			
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_KITCHEN_M_1")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@MALE@KITCHEN"
			sAnimStruct.sHeistAnimName = "kitchen_part_one"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_M_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_M_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "kitchen_part_one_cam"
			
			sAnimStruct.sHeistAnimModels[0] = PROP_NPC_PHONE
			sAnimStruct.sHeistAnimPropName[0] = "KITCHEN_PART_ONE_PHONE"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			sAnimStruct.bFreezeOnLastFrame = TRUE
			sAnimStruct.bPreLoadScene = TRUE
			
			BREAK
			
		CASE HEIST_SELECTED_ANIM_KITCHEN_M_2
			
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_KITCHEN_M_2")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@MALE@KITCHEN"
			sAnimStruct.sHeistAnimName = "kitchen_part_two"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_M_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_M_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "kitchen_part_two_cam"
			
			sAnimStruct.sHeistAnimAudioDict = "MP_PLAYER_APARTMENT"
			sAnimStruct.sHeistAudio[0].tl31AudioName = "DOOR_BUZZ"
			sAnimStruct.sHeistAudio[0].fAudioCue = 0.230
			sAnimStruct.sHeistAudio[0].vAudioLoc = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].house.vBuzzerLoc
			sAnimStruct.sHeistAudio[0].iErrorMarginPercent = 2
			
			sAnimStruct.sHeistAnimModels[0] = PROP_NPC_PHONE
			sAnimStruct.sHeistAnimPropName[0] = "KITCHEN_PART_TWO_PHONE"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			sAnimStruct.bPlayStartPostFX = TRUE
			
			BREAK
			
		CASE HEIST_SELECTED_ANIM_KITCHEN_M_IDLE
			
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_KITCHEN_M_IDLE")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@MALE@KITCHEN"
			sAnimStruct.sHeistAnimName = "kitchen_part_one_loop"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_M_IDLE, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_M_IDLE, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "kitchen_part_one_loop_cam"
			
			sAnimStruct.sHeistAnimModels[0] = PROP_NPC_PHONE
			sAnimStruct.sHeistAnimPropName[0] = "KITCHEN_PART_ONE_LOOP_PHONE"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			BREAK
			
		CASE HEIST_SELECTED_ANIM_KITCHEN_F_1
			
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_KITCHEN_F_1")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@FEMALE@KITCHEN"
			sAnimStruct.sHeistAnimName = "kitchen_part_one"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_F_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_F_1, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "kitchen_part_one_cam"
			
			sAnimStruct.sHeistAnimModels[0] = PROP_NPC_PHONE
			sAnimStruct.sHeistAnimPropName[0] = "KITCHEN_PART_ONE_PHONE"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			sAnimStruct.bFreezeOnLastFrame = TRUE
			sAnimStruct.bPreLoadScene = TRUE
			
			BREAK
			
		CASE HEIST_SELECTED_ANIM_KITCHEN_F_2
			
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_KITCHEN_F_2")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@FEMALE@KITCHEN"
			sAnimStruct.sHeistAnimName = "kitchen_part_two"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_F_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_F_2, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "kitchen_part_two_cam"
			
			sAnimStruct.sHeistAnimAudioDict = "MP_PLAYER_APARTMENT"
			sAnimStruct.sHeistAudio[0].tl31AudioName = "DOOR_BUZZ"
			sAnimStruct.sHeistAudio[0].fAudioCue = 0.230
			sAnimStruct.sHeistAudio[0].vAudioLoc = mpProperties[GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty].house.vBuzzerLoc
			sAnimStruct.sHeistAudio[0].iErrorMarginPercent = 2
			
			sAnimStruct.sHeistAnimModels[0] = PROP_NPC_PHONE
			sAnimStruct.sHeistAnimPropName[0] = "KITCHEN_PART_TWO_PHONE"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			sAnimStruct.bPlayStartPostFX = TRUE
			
			BREAK
			
		CASE HEIST_SELECTED_ANIM_KITCHEN_F_IDLE
			
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_KITCHEN_F_IDLE")
		
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@VARIATIONS@FEMALE@KITCHEN"
			sAnimStruct.sHeistAnimName = "kitchen_part_one_loop"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_F_IDLE, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_F_IDLE, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			sAnimStruct.sHeistAnimCams[0] = "kitchen_part_one_loop_cam"
			
			sAnimStruct.sHeistAnimModels[0] = PROP_NPC_PHONE
			sAnimStruct.sHeistAnimPropName[0] = "KITCHEN_PART_ONE_LOOP_PHONE"
			
			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			BREAK
			
		CASE HEIST_SELECTED_ANIM_COUCH_F_2_GUEST
		CASE HEIST_SELECTED_ANIM_COUCH_M_2_GUEST
		CASE HEIST_SELECTED_ANIM_WHISKY_2_GUEST
		CASE HEIST_SELECTED_ANIM_WINE_2_GUEST
		
			SWITCH sAnimStruct.sActiveAnim
				CASE HEIST_SELECTED_ANIM_COUCH_F_2_GUEST 	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_COUCH_F_2_GUEST") 	BREAK
				CASE HEIST_SELECTED_ANIM_COUCH_M_2_GUEST 	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_COUCH_M_2_GUEST") 	BREAK
				CASE HEIST_SELECTED_ANIM_WHISKY_2_GUEST 	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_WHISKY_2_GUEST") 		BREAK
				CASE HEIST_SELECTED_ANIM_WINE_2_GUEST 		CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - Setting details for: HEIST_SELECTED_ANIM_WINE_2_GUEST") 		BREAK
			ENDSWITCH
			
			sAnimStruct.sHeistAnimDictionary = "ANIM@HEISTS@HEIST_SAFEHOUSE_INTRO@OPEN_DOOR@"
			sAnimStruct.sHeistAnimName = "Open_Door_HOST" // owner answering the door.
			sAnimStruct.sHeistAnimAdditionalName = "Open_Door_GUEST" // ped walking in.
			sAnimStruct.sHeistAnimDoor = "Open_Door_DOOR"
			
			sAnimStruct.sHeistAnimCams[0] = "Open_Door_CAM"

			sAnimStruct.sHeistAnimRoomName = "rm_high_lounge"
			sAnimStruct.sHeistAnimRoomNameDLC = "mp_apt_h_01_hall"
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vHeistAnimScenePos, MP_PROP_ELEMENT_HEIST_INTRO_ANIM, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vHeistAnimSceneRot, MP_PROP_ELEMENT_HEIST_INTRO_ANIM, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vBespokePos[0], MP_PROP_ELEMENT_HEIST_BESPOKE_ANIM_INTRO_LEADER, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vBespokeRot[0], MP_PROP_ELEMENT_HEIST_BESPOKE_ANIM_INTRO_LEADER, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			
			GET_PLAYER_PROPERTY_HEIST_LOCATION(sAnimStruct.vBespokePos[1], MP_PROP_ELEMENT_HEIST_BESPOKE_ANIM_INTRO_GUEST, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			GET_PLAYER_PROPERTY_HEIST_ROTATION(sAnimStruct.vBespokeRot[1], MP_PROP_ELEMENT_HEIST_BESPOKE_ANIM_INTRO_GUEST, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			
			sAnimStruct.bAdditionalClone = TRUE
			sAnimStruct.bBespokePositions = TRUE
			sAnimStruct.bDontApplyFlatFeet = TRUE
	
			BREAK
	
	ENDSWITCH
	
ENDPROC


/// PURPOSE:
///    Request and create a new ped model to clone the player ped onto. Set to invisible by default.
/// RETURNS:
///    TRUE when finished.
FUNC BOOL HEIST_CREATE_PED_CLONE_FOR_ANIMS(g_structHeistAnims& sAnimStruct, BOOL bAdditionalClone)
	
	IF g_HeistSharedClient.iCurrentPropertyOwnerIndex = INVALID_HEIST_DATA
		CDEBUG2LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - iCurrentPropertyOwnerIndex = -1, wait for valid data.")
		RETURN FALSE
	ENDIF
	
	PLAYER_INDEX piHeistLeader = INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex)
	
	IF NOT bAdditionalClone
	
		IF sAnimStruct.bHaveClonedLeader
		AND sAnimStruct.bHaveBlendedHead
			RETURN TRUE
		ENDIF
	
		IF NOT DOES_ENTITY_EXIST(sAnimStruct.pedHeistLeaderClone)
			
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - pedHeistLeaderClone does NOT yet exist, continue to creation.")

			PED_INDEX tempPed = GET_PLAYER_PED(piHeistLeader)
				
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - HAS_MODEL_LOADED() = TRUE, cloning player.")
			
			sAnimStruct.pedHeistLeaderClone = CLONE_PED(tempPed, FALSE, FALSE, FALSE)
			
			CLEAR_PED_TASKS_IMMEDIATELY(sAnimStruct.pedHeistLeaderClone)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sAnimStruct.pedHeistLeaderClone, TRUE)
			// ------- Fix for Bug: 206347 Rowan J
			SET_PED_RESET_FLAG(sAnimStruct.pedHeistLeaderClone, PRF_BlockWeaponReactionsUnlessDead, TRUE) 
			SET_PED_RESET_FLAG(sAnimStruct.pedHeistLeaderClone, PRF_DisableSecondaryAnimationTasks, TRUE) 
			SET_PED_CONFIG_FLAG(sAnimStruct.pedHeistLeaderClone, PCF_PedIgnoresAnimInterruptEvents, TRUE)
			SET_PED_CONFIG_FLAG(sAnimStruct.pedHeistLeaderClone, PCF_DisableExplosionReactions, TRUE)
			SET_PED_CONFIG_FLAG(sAnimStruct.pedHeistLeaderClone, PCF_RunFromFiresAndExplosions, FALSE)
			// -------			
			SET_PED_RELATIONSHIP_GROUP_HASH(sAnimStruct.pedHeistLeaderClone, rgFM_AiLike)
			
			SET_ENTITY_INVINCIBLE(sAnimStruct.pedHeistLeaderClone, TRUE)
			FREEZE_ENTITY_POSITION(sAnimStruct.pedHeistLeaderClone, TRUE)
			SET_ENTITY_VISIBLE(sAnimStruct.pedHeistLeaderClone, FALSE)
			SET_ENTITY_COLLISION(sAnimStruct.pedHeistLeaderClone, FALSE)
			
			IF sAnimStruct.bBespokePositions
				// Add a small offset to the scene pos to make sure that peds are created above floor.
				sAnimStruct.vBespokePos[0].Z = sAnimStruct.vBespokePos[0].Z + 0.3
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - Using bespoke position: ", sAnimStruct.vBespokePos[0], ", rotation: ", sAnimStruct.vBespokePos[0])
				SET_ENTITY_COORDS(sAnimStruct.pedHeistLeaderClone, sAnimStruct.vBespokePos[0])
				SET_ENTITY_ROTATION(sAnimStruct.pedHeistLeaderClone, sAnimStruct.vBespokeRot[0])
			ELSE
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - Using scene position: ", sAnimStruct.vHeistAnimScenePos, ", rotation: ", sAnimStruct.vHeistAnimSceneRot)
				SET_ENTITY_COORDS(sAnimStruct.pedHeistLeaderClone, sAnimStruct.vHeistAnimScenePos)
				SET_ENTITY_ROTATION(sAnimStruct.pedHeistLeaderClone, sAnimStruct.vHeistAnimSceneRot)
			ENDIF
			
			FINALIZE_HEAD_BLEND(sAnimStruct.pedHeistLeaderClone)
			SET_PED_HELMET(sAnimStruct.pedHeistLeaderClone, FALSE)
			REMOVE_PED_HELMET(sAnimStruct.pedHeistLeaderClone, TRUE)
			SET_PED_COMPONENT_VARIATION(sAnimStruct.pedHeistLeaderClone, PED_COMP_BERD, 0, 0)		// Remove the mask
			
			// Checks if ped is wearing scarf before setting teeth comp to 0 - url:bugstar:2235506
			IF GET_PED_DRAWABLE_VARIATION(sAnimStruct.pedHeistLeaderClone, PED_COMP_TEETH) = 30
			OR GET_PED_DRAWABLE_VARIATION(sAnimStruct.pedHeistLeaderClone, PED_COMP_TEETH) = 31
			OR GET_PED_DRAWABLE_VARIATION(sAnimStruct.pedHeistLeaderClone, PED_COMP_TEETH) = 34
			OR GET_PED_DRAWABLE_VARIATION(sAnimStruct.pedHeistLeaderClone, PED_COMP_TEETH) = 35
				PRINTLN("[HEIST_ANIMS] Player is wearing a scarf - removing the scarf for cloned heist leader ped")
				SET_PED_COMPONENT_VARIATION(sAnimStruct.pedHeistLeaderClone, PED_COMP_TEETH, 0, 0)		// Remove scarf url:bugstar:2220157
			ENDIF
			
			PED_COMP_ITEM_DATA_STRUCT sTempCompData = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(sAnimStruct.pedHeistLeaderClone), COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(piHeistLeader)].iMyCurrentHair))
			SET_PED_COMPONENT_VARIATION(sAnimStruct.pedHeistLeaderClone, PED_COMP_HAIR, sTempCompData.iDrawable, sTempCompData.iTexture)
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - Setting cloned ped's hair to: ", GlobalplayerBD[NATIVE_TO_INT(piHeistLeader)].iMyCurrentHair)		
			
			SET_FACIAL_IDLE_ANIM_OVERRIDE(sAnimStruct.pedHeistLeaderClone, GET_PLAYER_MOOD_ANIM_FROM_INDEX(GlobalplayerBD_FM[NATIVE_TO_INT(piHeistLeader)].iPlayerMood))
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - Setting cloned ped's mood to: ", GlobalplayerBD_FM[NATIVE_TO_INT(piHeistLeader)].iPlayerMood)		
			
			CLEAR_PED_PROP(sAnimStruct.pedHeistLeaderClone, ANCHOR_HEAD)
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - Cleared head prop.")
			
			IF NOT sAnimStruct.bDontApplyFlatFeet
				SET_FEMALE_PED_FEET_TO_FLATS(sAnimStruct.pedHeistLeaderClone)
			ENDIF
			
			IF IS_PED_WEARING_HAZ_HOOD_UP(sAnimStruct.pedHeistLeaderClone)
				SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(sAnimStruct.pedHeistLeaderClone)
			ENDIF
			
			SET_HOODED_JACKET_STATE(sAnimStruct.pedHeistLeaderClone, JACKET_HOOD_DOWN)
			sAnimStruct.bHaveClonedLeader = TRUE
			
			REMOVE_MP_PED_SCUBA_TANK(sAnimStruct.pedHeistLeaderClone)
		ELSE
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - DOES_ENTITY_EXIST() = TRUE, don't re-create clone.")
		ENDIF
		
		
		IF NOT sAnimStruct.bHaveBlendedHead
			IF NOT IS_ENTITY_DEAD(sAnimStruct.pedHeistLeaderClone)
			AND DOES_ENTITY_EXIST(sAnimStruct.pedHeistLeaderClone)
				//force normal head
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS]: Force leader head to normal - Berd item is doesn't need a shrunken head")
				IF piHeistLeader = PLAYER_ID()
					RESET_PLAYER_HEAD_BLEND_TO_NORM(sAnimStruct.pedHeistLeaderClone)
					sAnimStruct.bHaveBlendedHead = TRUE
				ELSE
					IF NETWORK_HAS_CACHED_PLAYER_HEAD_BLEND_DATA(piHeistLeader)
						IF NETWORK_APPLY_CACHED_PLAYER_HEAD_BLEND_DATA(sAnimStruct.pedHeistLeaderClone, piHeistLeader)
							CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - Applied cached head blend data.")
							sAnimStruct.bHaveBlendedHead = TRUE
						ELSE
							CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - Waiting for cached head blend data to apply.")
						ENDIF
					ELSE
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS]: Force head to normal - Leader doesn't have cached head blend data.")
					ENDIF
				ENDIF
			ELSE	
				IF IS_ENTITY_DEAD(sAnimStruct.pedHeistLeaderClone)
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS]: Failed head-blend, leader clone: ",NATIVE_TO_INT(sAnimStruct.pedHeistLeaderClone)," is DEAD.")
				ELSE
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS]: Failed head-blend, leader clone: ",NATIVE_TO_INT(sAnimStruct.pedHeistLeaderClone)," DEOS NOT EXIST.")
				ENDIF
			ENDIF
			
		ENDIF
		
	ELSE
	
		IF sAnimStruct.bHaveClonedAdditional
			RETURN TRUE
		ENDIF
	
		IF NOT DOES_ENTITY_EXIST(sAnimStruct.pedAdditionalClone)
			
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - pedAdditionalClone does NOT yet exist, continue to creation.")
		
			PED_INDEX tempPed = PLAYER_PED_ID()
				
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - HAS_MODEL_LOADED() = TRUE, cloning additional player.")
			
			sAnimStruct.pedAdditionalClone = CLONE_PED(tempPed, FALSE, FALSE, FALSE)
			
			CLEAR_PED_TASKS_IMMEDIATELY(sAnimStruct.pedAdditionalClone)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sAnimStruct.pedAdditionalClone, TRUE)
			// ------- Fix for Bug: 206347 Rowan J
			SET_PED_RESET_FLAG(sAnimStruct.pedAdditionalClone, PRF_BlockWeaponReactionsUnlessDead, TRUE) 
			SET_PED_RESET_FLAG(sAnimStruct.pedAdditionalClone, PRF_DisableSecondaryAnimationTasks, TRUE) 
			SET_PED_CONFIG_FLAG(sAnimStruct.pedAdditionalClone, PCF_PedIgnoresAnimInterruptEvents, TRUE)
			SET_PED_CONFIG_FLAG(sAnimStruct.pedAdditionalClone, PCF_DisableExplosionReactions, TRUE)
			SET_PED_CONFIG_FLAG(sAnimStruct.pedAdditionalClone, PCF_RunFromFiresAndExplosions, FALSE)
			// -------	
			SET_PED_RELATIONSHIP_GROUP_HASH(sAnimStruct.pedAdditionalClone, rgFM_AiLike)
			
			SET_ENTITY_INVINCIBLE(sAnimStruct.pedAdditionalClone, TRUE)
			FREEZE_ENTITY_POSITION(sAnimStruct.pedAdditionalClone, TRUE)
			SET_ENTITY_VISIBLE(sAnimStruct.pedAdditionalClone, FALSE)
			SET_ENTITY_COLLISION(sAnimStruct.pedAdditionalClone, FALSE)
			
			IF sAnimStruct.bBespokePositions
				// Add a small offset to the scene pos to make sure that peds are created above floor.
				sAnimStruct.vBespokePos[1].Z = sAnimStruct.vBespokePos[1].Z + 0.3
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - Using bespoke position: ", sAnimStruct.vBespokePos[1], ", rotation: ", sAnimStruct.vBespokePos[1])
				SET_ENTITY_COORDS(sAnimStruct.pedHeistLeaderClone, sAnimStruct.vBespokePos[1])
				SET_ENTITY_ROTATION(sAnimStruct.pedHeistLeaderClone, sAnimStruct.vBespokeRot[1])
			ELSE
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - Using scene position: ", sAnimStruct.vHeistAnimScenePos, ", rotation: ", sAnimStruct.vHeistAnimSceneRot)
				SET_ENTITY_COORDS(sAnimStruct.pedAdditionalClone, sAnimStruct.vHeistAnimScenePos)
				SET_ENTITY_ROTATION(sAnimStruct.pedAdditionalClone, sAnimStruct.vHeistAnimSceneRot)
			ENDIF
			
			FINALIZE_HEAD_BLEND(sAnimStruct.pedAdditionalClone)
			SET_PED_HELMET(sAnimStruct.pedAdditionalClone, FALSE)
			REMOVE_PED_HELMET(sAnimStruct.pedAdditionalClone, TRUE)
			SET_PED_COMPONENT_VARIATION(sAnimStruct.pedAdditionalClone, PED_COMP_BERD, 0, 0)		// Remove the mask
			
			// Checks if ped is wearing scarf before setting teeth comp to 0 - url:bugstar:2235506
			IF GET_PED_DRAWABLE_VARIATION(sAnimStruct.pedAdditionalClone, PED_COMP_TEETH) = 30
			OR GET_PED_DRAWABLE_VARIATION(sAnimStruct.pedAdditionalClone, PED_COMP_TEETH) = 31
			OR GET_PED_DRAWABLE_VARIATION(sAnimStruct.pedAdditionalClone, PED_COMP_TEETH) = 34
			OR GET_PED_DRAWABLE_VARIATION(sAnimStruct.pedAdditionalClone, PED_COMP_TEETH) = 35
				PRINTLN("[HEIST_ANIMS] Player is wearing a scarf - removing the scarf for cloned additional ped")
				SET_PED_COMPONENT_VARIATION(sAnimStruct.pedAdditionalClone, PED_COMP_TEETH, 0, 0)		// Remove scarf url:bugstar:2220157
			ENDIF
			
			PED_COMP_ITEM_DATA_STRUCT sTempCompData = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(sAnimStruct.pedAdditionalClone), COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair))
			SET_PED_COMPONENT_VARIATION(sAnimStruct.pedAdditionalClone, PED_COMP_HAIR, sTempCompData.iDrawable, sTempCompData.iTexture)
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - Setting cloned ped's hair to: ", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iMyCurrentHair)		
			
			SET_FACIAL_IDLE_ANIM_OVERRIDE(sAnimStruct.pedAdditionalClone, GET_PLAYER_MOOD_ANIM_FROM_INDEX(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iPlayerMood))
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - Setting cloned ped's mood to: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iPlayerMood)		
			
			CLEAR_PED_PROP(sAnimStruct.pedAdditionalClone, ANCHOR_HEAD)
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - Cleared head prop.")
			
			IF NOT sAnimStruct.bDontApplyFlatFeet
				SET_FEMALE_PED_FEET_TO_FLATS(sAnimStruct.pedAdditionalClone)
			ENDIF
			
			IF IS_PED_WEARING_HAZ_HOOD_UP(sAnimStruct.pedAdditionalClone)
				SWAP_CHEM_SUIT_HOOD_FOR_ALTERNATE(sAnimStruct.pedAdditionalClone) 
			ENDIF
			
			SET_HOODED_JACKET_STATE(sAnimStruct.pedAdditionalClone, JACKET_HOOD_DOWN)
			
			REMOVE_MP_PED_SCUBA_TANK(sAnimStruct.pedAdditionalClone)
			
			IF NOT IS_ENTITY_DEAD(sAnimStruct.pedAdditionalClone)
			AND DOES_ENTITY_EXIST(sAnimStruct.pedAdditionalClone)
				//force normal head
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS]: Force additional ped head to normal - Berd item is doesn't need a shrunken head")
				RESET_PLAYER_HEAD_BLEND_TO_NORM(sAnimStruct.pedAdditionalClone) // This is the player so safe to call this command
			ELSE
				IF IS_ENTITY_DEAD(sAnimStruct.pedHeistLeaderClone)
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS]: Failed head-blend, additional clone: ",NATIVE_TO_INT(sAnimStruct.pedHeistLeaderClone)," is DEAD.")
				ELSE
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS]: Failed head-blend, additional clone: ",NATIVE_TO_INT(sAnimStruct.pedHeistLeaderClone)," DEOS NOT EXIST.")
				ENDIF
			ENDIF
								
			sAnimStruct.bHaveClonedAdditional = TRUE
			
			RETURN TRUE
			
		ELSE
			
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - HEIST_CREATE_PED_CLONE_FOR_ANIMS - DOES_ENTITY_EXIST() = TRUE, don't re-create additional clone.")
			RETURN TRUE
			
		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Run every frame to check if the scene is at an audio cue, if so, play the audio as detailed in the anim controller module.
PROC UPDATE_ANIM_SOUND_STATE(g_structHeistAnims& sAnimStruct)

	IF IS_SYNCHRONIZED_SCENE_RUNNING(sAnimStruct.iHeistAnimSceneID)

		FLOAT fCurrentPhase = GET_SYNCHRONIZED_SCENE_PHASE(sAnimStruct.iHeistAnimSceneID)
		INT index
		
	//	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - UPDATE_ANIM_SOUND_STATE - Scene phase: ",fCurrentPhase,"; updating audio details for: ", GET_HEIST_ANIM_NAME(sAnimStruct.sActiveAnim))

		FOR index = 0 TO (MAX_HEIST_ANIM_PROPS-1)
		
			IF NOT IS_STRING_NULL_OR_EMPTY(sAnimStruct.sHeistAudio[index].tl31AudioName)
			
				IF NOT IS_BIT_SET(sAnimStruct.bsAudioQueue, index)

					FLOAT fErrorMargin = sAnimStruct.sHeistAudio[index].iErrorMarginPercent / 100.0

					IF fCurrentPhase <= sAnimStruct.sHeistAudio[index].fAudioCue + fErrorMargin
					AND fCurrentPhase > sAnimStruct.sHeistAudio[index].fAudioCue - fErrorMargin
					
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - UPDATE_ANIM_SOUND_STATE - Audio cue hit (within error margin), activating sound: ")
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - UPDATE_ANIM_SOUND_STATE ...  CueIndex:	", index)
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - UPDATE_ANIM_SOUND_STATE ...  DictName:	", sAnimStruct.sHeistAnimAudioDict)
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - UPDATE_ANIM_SOUND_STATE ...  CueName:	", sAnimStruct.sHeistAudio[index].tl31AudioName)	
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - UPDATE_ANIM_SOUND_STATE ...  CurrPhase:	", fCurrentPhase)
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - UPDATE_ANIM_SOUND_STATE ...  vLoc:		", sAnimStruct.sHeistAudio[index].vAudioLoc)
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - UPDATE_ANIM_SOUND_STATE ...  Error %:	", sAnimStruct.sHeistAudio[index].iErrorMarginPercent, ", Value: ", fErrorMargin)
						
						PLAY_SOUND_FROM_COORD(GET_SOUND_ID(),  sAnimStruct.sHeistAudio[index].tl31AudioName, sAnimStruct.sHeistAudio[index].vAudioLoc, sAnimStruct.sHeistAnimAudioDict)
					
						SET_BIT(sAnimStruct.bsAudioQueue, index)
					
					ENDIF
					
				ENDIF
				
			ENDIF
			
		ENDFOR
		
	ENDIF

ENDPROC


/// PURPOSE:
///    Find and create a new door to use in the intro animation.
/// RETURNS:
///    TRUE if successful.
FUNC BOOL CREATE_HEIST_ANIM_DOORS(g_structHeistAnims& sAnimStruct)

	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CREATE_HEIST_ANIM_DOORS - Locate, find and hide existing apartment door.")

	MODEL_NAMES mnDoorModel[8]
	MODEL_NAMES mnTempModel

	mnDoorModel[0] = V_ILEV_MP_HIGH_FRONTDOOR	
	mnDoorModel[1] = HEI_HEIST_APART2_DOOR
	mnDoorModel[2] = HEI_V_ILEV_FH_HEISTDOOR2	
	mnDoorModel[3] = APA_P_MP_DOOR_APARTFRT_DOOR
	mnDoorModel[4] = APA_P_MP_DOOR_MPA2_FRNT
	mnDoorModel[5] = APA_P_MP_DOOR_STILT_DOORFRNT
	mnDoorModel[6] = APA_P_MP_DOOR_APARTFRT_DOOR_BLACK
	mnDoorModel[7] = APA_P_MP_DOOR_MPA2_FRNT

	INT PropertyToCheck = globalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
	INT iPropertyVariant = globalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideVariation 
	VECTOR vDoorLocation
	
	GET_PLAYER_PROPERTY_HEIST_LOCATION(vDoorLocation, MP_PROP_ELEMENT_APART_ENTRANCE_DOOR, PropertyToCheck)
		 
	INT index
	FOR index = 0 TO (COUNT_OF(mnDoorModel)-1)
		CREATE_MODEL_HIDE(vDoorLocation, 1.5, mnDoorModel[index], TRUE)
	ENDFOR
	
	// TODO: Add new apartment door models to this check.
	IF Propertytocheck = PROPERTY_BUS_HIGH_APT_1
	OR Propertytocheck = PROPERTY_BUS_HIGH_APT_2
	OR Propertytocheck = PROPERTY_BUS_HIGH_APT_3
	OR Propertytocheck = PROPERTY_BUS_HIGH_APT_4
	OR Propertytocheck = PROPERTY_BUS_HIGH_APT_5
		mnTempModel = HEI_HEIST_APART2_DOOR
	ELIF IS_PROPERTY_CUSTOM_APARTMENT(Propertytocheck)	
		
		IF iPropertyVariant = -1 
		OR iPropertyVariant = PROPERTY_VARIATION_1
			mnTempModel = APA_P_MP_DOOR_APARTFRT_DOOR
		ELIF iPropertyVariant = PROPERTY_VARIATION_2
		OR iPropertyVariant = PROPERTY_VARIATION_4
		OR iPropertyVariant = PROPERTY_VARIATION_6
		OR iPropertyVariant = PROPERTY_VARIATION_8
			mnTempModel = APA_P_MP_DOOR_MPA2_FRNT
		ELIF iPropertyVariant = PROPERTY_VARIATION_3
		OR iPropertyVariant = PROPERTY_VARIATION_7
			mnTempModel = APA_P_MP_DOOR_STILT_DOORFRNT
		ELIF iPropertyVariant = PROPERTY_VARIATION_5
			mnTempModel = APA_P_MP_DOOR_APARTFRT_DOOR_BLACK
		ENDIF
	
	ELIF (IS_PROPERTY_STILT_APARTMENT(Propertytocheck, PROPERTY_STILT_APT_5_BASE_A) OR (IS_PROPERTY_STILT_APARTMENT(Propertytocheck, PROPERTY_STILT_APT_1_BASE_B)))
		mnTempModel = APA_P_MP_DOOR_APARTFRT_DOOR
	ELSE
		mnTempModel = HEI_V_ILEV_FH_HEISTDOOR2
	ENDIF
	
	sAnimStruct.oiApartmentDoor = CREATE_OBJECT_NO_OFFSET(mnTempModel, vDoorLocation, FALSE, FALSE, TRUE)
	
	SET_ENTITY_INVINCIBLE(sAnimStruct.oiApartmentDoor, FALSE)
	SET_ENTITY_VISIBLE(sAnimStruct.oiApartmentDoor, TRUE)
	
	IF sAnimStruct.iRoomKey != 0
		IF IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
			FORCE_ROOM_FOR_ENTITY(sAnimStruct.oiApartmentDoor, GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()), sAnimStruct.iRoomKey)
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CREATE_HEIST_ANIM_DOORS - Forced created apartment door for anim: ",sAnimStruct.sHeistAnimDoor," into room key: ", sAnimStruct.iRoomKey)
		ELSE
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CREATE_HEIST_ANIM_DOORS - ERROR! player's interior is NOT READY!")
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CREATE_HEIST_ANIM_DOORS - Finished creating new scene door, total hidden: ", index)
	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CREATE_HEIST_ANIM_DOORS ... vLoc:	", vDoorLocation)
	
	RETURN TRUE	
	
ENDFUNC


//Flattens the contents of the STRUC g_structHeistAnims
PROC CLEAN_g_structHeistAnims_STRUCT(g_structHeistAnims& sAnimStruct)

	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - CLEAN_g_structHeistAnims_STRUCT")
	DEBUG_PRINTCALLSTACK()

	INT iLoop
	STRING stNull
	TEXT_LABEL_63 tl63Null = stNull
	sAnimStruct.ID = 0
	sAnimStruct.iHeistAnimSceneID 		= -1
	sAnimStruct.iRoomKey 				= 0
	sAnimStruct.bsAudioQueue = 0

	sAnimStruct.stage = HMS_ANIM_STAGE_INACTIVE
	sAnimStruct.sActiveAnim = HEIST_SELECTED_ANIM_NONE
	sAnimStruct.timerAnimTimeout = NULL
	sAnimStruct.oiTempAnimProp = NULL	
	sAnimStruct.camAnimCam = NULL
	sAnimStruct.oiApartmentDoor = NULL

	sAnimStruct.sHeistAnimDictionary = tl63Null
	sAnimStruct.sHeistAnimName = tl63Null
	sAnimStruct.sHeistAnimAudioDict = tl63Null
	sAnimStruct.sHeistAnimDoor = tl63Null
	sAnimStruct.sHeistAnimAdditionalName = tl63Null
	sAnimStruct.sHeistAnimRoomName = tl63Null
	sAnimStruct.sHeistAnimRoomNameDLC = tl63Null

	REPEAT MAX_HEIST_ANIM_PROPS iLoop
		sAnimStruct.sHeistAudio[iLoop].tl31AudioName = tl63Null
		sAnimStruct.sHeistAudio[iLoop].fAudioCue = 0.0	
		sAnimStruct.sHeistAudio[iLoop].vAudioLoc = <<0.0, 0.0, 0.0>>	
		sAnimStruct.sHeistAudio[iLoop].iErrorMarginPercent	= 0	
		sAnimStruct.oiPropIndices[iLoop] = NULL
		sAnimStruct.sHeistAnimCams[iLoop] = tl63Null
		sAnimStruct.sHeistAnimPropName[iLoop] = tl63Null
		sAnimStruct.sHeistAnimModels[iLoop] = DUMMY_MODEL_FOR_SCRIPT
		sAnimStruct.vBespokePos[iLoop] = <<0.0, 0.0, 0.0>>
		sAnimStruct.vBespokeRot[iLoop] = <<0.0, 0.0, 0.0>>
	ENDREPEAT
	
	sAnimStruct.oiLightRig = NULL
	sAnimStruct.mnLightRig = DUMMY_MODEL_FOR_SCRIPT
	
	sAnimStruct.vHeistAnimScenePos = <<0.0, 0.0, 0.0>>
	sAnimStruct.vHeistAnimSceneRot = <<0.0, 0.0, 0.0>>
	sAnimStruct.pedHeistLeaderClone = NULL
	sAnimStruct.pedAdditionalClone = NULL

	sAnimStruct.bHaveClonedLeader 			= FALSE
	sAnimStruct.bHaveFinishedPlaying 		= FALSE
	sAnimStruct.bAtLastFrame 				= FALSE
	sAnimStruct.bHaveFrozenScreen 			= FALSE
	sAnimStruct.bSceneCamRendering 			= FALSE
	sAnimStruct.bAssetsLoaded 				= FALSE
	sAnimStruct.bXThreadCleanup 			= FALSE
	sAnimStruct.bAdditionalClone 			= FALSE
	sAnimStruct.bHaveClonedAdditional		= FALSE
	sAnimStruct.bBespokePositions			= FALSE
	sAnimStruct.bPlayStartPostFX			= FALSE
	sAnimStruct.bPreLoadScene				= FALSE
	sAnimStruct.bSceneLoaded				= FALSE
	sAnimStruct.bHaveBlendedHead			= FALSE
	sAnimStruct.bHaveCorrectedTOD			= FALSE
		
ENDPROC


/// PURPOSE:
///    Delete all intro anim doors and model hides.
PROC DESTROY_HEIST_ANIM_DOORS(g_structHeistAnims& sAnimStruct)
	
	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - DESTROY_HEIST_ANIM_DOORS - Starting door cleanup.")	
	
	MODEL_NAMES mnDoorModel[8]
	
	mnDoorModel[0] = V_ILEV_MP_HIGH_FRONTDOOR	
	mnDoorModel[1] = HEI_HEIST_APART2_DOOR
	mnDoorModel[2] = HEI_V_ILEV_FH_HEISTDOOR2	
	mnDoorModel[3] = APA_P_MP_DOOR_APARTFRT_DOOR
	mnDoorModel[4] = APA_P_MP_DOOR_MPA2_FRNT
	mnDoorModel[5] = APA_P_MP_DOOR_STILT_DOORFRNT
	mnDoorModel[6] = APA_P_MP_DOOR_APARTFRT_DOOR_BLACK
	mnDoorModel[7] = APA_P_MP_DOOR_MPA2_FRNT
	
	INT PropertyToCheck = globalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
	VECTOR vDoorLocation
	INT index
	
	GET_PLAYER_PROPERTY_HEIST_LOCATION(vDoorLocation, MP_PROP_ELEMENT_APART_ENTRANCE_DOOR, PropertyToCheck)
	IF NOT IS_THIS_CUTSCENE_NARCOTICS_BESPOKE(g_heistmocap.cutsceneName) // added this exception by Kevin to fix  2248069
		FOR index = 0 TO (COUNT_OF(mnDoorModel)-1)
			REMOVE_MODEL_HIDE(vDoorLocation, 2.0, mnDoorModel[index])
		ENDFOR
	ENDIF	
	IF sAnimStruct.oiApartmentDoor != NULL
		CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - DESTROY_HEIST_ANIM_DOORS - Setting model in oiApartmentDoor to no longer needed.")
		DELETE_OBJECT(sAnimStruct.oiApartmentDoor)
		FOR index = 0 TO (COUNT_OF(mnDoorModel)-1)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnDoorModel[index])
		ENDFOR
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Get the heist intro animation for the part before the corona screen.
/// PARAMS:
///    iStrandContID - Strand root content ID
///    bMale - Male / Female
///    bIntro - Init heist
///    bMidStrand - Mid-strand cutscene
///    bFinale - Finale intro
/// RETURNS:
///    g_enumActiveHeistAnim - Anim ID
FUNC g_enumActiveHeistAnim GET_HEIST_INTO_ANIM_PRE(INT iStrandContID, BOOL bMale, BOOL bIntro, BOOL bMidStrand, BOOL bFinale)
	
	g_enumActiveHeistAnim eReturnValue	

	IF iStrandContID = g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job
	
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_1
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_1
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WHISKY_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINE_1
			ENDIF
		ENDIF
			
	ELIF iStrandContID = g_sMPTUNABLES.iroot_id_HASH_The_Prison_Break
		
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_M_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_F_1
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_TV_M_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_TV_F_1
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WINDOW_M_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINDOW_F_1
			ENDIF
		ENDIF
			
	ELIF iStrandContID = g_sMPTUNABLES.iroot_id_HASH_The_Humane_Labs_Raid
		
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_1
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_M_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_F_1
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WHISKY_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINE_1
			ENDIF
		ENDIF
			
	ELIF iStrandContID = g_sMPTUNABLES.iroot_id_HASH_Series_A_Funding
			
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_TV_M_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_TV_F_1
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_1
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WINDOW_M_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINDOW_F_1
			ENDIF
		ENDIF
			
	ELIF iStrandContID = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job
		
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_M_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_F_1
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_TV_M_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_TV_F_1
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WHISKY_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINE_1
			ENDIF
		ENDIF
	
	ELSE
	
		PRINTLN("[AMEC][HEIST_ANIMS] - GET_HEIST_INTO_ANIM_PRE - ERROR! StrandID did not match any tunables. Value: ", iStrandContID)

		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_1
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_1
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WHISKY_1
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINE_1
			ENDIF
		ENDIF
	ENDIF
	
	IF eReturnValue = HEIST_SELECTED_ANIM_NONE
		SCRIPT_ASSERT("[AMEC][HEIST_ANIMS] - GET_HEIST_INTO_ANIM_PRE - CRITICAL ERROR! No animation selected! This will break the game!")
		PRINTLN("[AMEC][HEIST_ANIMS] - GET_HEIST_INTO_ANIM_PRE - CRITICAL ERROR! No animation selected! This will break the game!")
		PRINTLN("[AMEC][HEIST_ANIMS]  ... iStrandContID: 	", iStrandContID)
		PRINTLN("[AMEC][HEIST_ANIMS]  ... bMale: 			", bMale)
		PRINTLN("[AMEC][HEIST_ANIMS]  ... bIntro: 			", bIntro)
		PRINTLN("[AMEC][HEIST_ANIMS]  ... bMidStrand: 		", bMidStrand)
		PRINTLN("[AMEC][HEIST_ANIMS]  ... bFinale: 			", bFinale)
	ENDIF
	
	RETURN eReturnValue

ENDFUNC

/// PURPOSE:
///    Get the heist intro animation for the part during the corona screen.
/// PARAMS:
///    iStrandContID - Strand root content ID
///    bMale - Male / Female
///    bIntro - Init heist
///    bMidStrand - Mid-strand cutscene
///    bFinale - Finale intro
/// RETURNS:
///    g_enumActiveHeistAnim - Anim ID
FUNC g_enumActiveHeistAnim GET_HEIST_INTO_ANIM_DURING(INT iStrandContID, BOOL bMale, BOOL bIntro, BOOL bMidStrand, BOOL bFinale)
	
	g_enumActiveHeistAnim eReturnValue

	IF iStrandContID = g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job
	
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_IDLE
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_IDLE
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WHISKY_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINE_IDLE
			ENDIF
		ENDIF
			
	ELIF iStrandContID = g_sMPTUNABLES.iroot_id_HASH_The_Prison_Break
		
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_M_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_F_IDLE
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_TV_M_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_TV_F_IDLE
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WINDOW_M_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINDOW_F_IDLE
			ENDIF
		ENDIF
		
	ELIF iStrandContID = g_sMPTUNABLES.iroot_id_HASH_The_Humane_Labs_Raid
		
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_IDLE
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_M_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_F_IDLE
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WHISKY_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINE_IDLE
			ENDIF
		ENDIF
		
	ELIF iStrandContID = g_sMPTUNABLES.iroot_id_HASH_Series_A_Funding
			
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_TV_M_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_TV_F_IDLE
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_IDLE
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WINDOW_M_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINDOW_F_IDLE
			ENDIF
		ENDIF
			
	ELIF iStrandContID = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job
		
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_M_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_F_IDLE
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_TV_M_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_TV_F_IDLE
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WHISKY_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINE_IDLE
			ENDIF
		ENDIF
	
	ELSE
	
		PRINTLN("[AMEC][HEIST_ANIMS] - GET_HEIST_INTO_ANIM_DURING - ERROR! StrandID did not match any tunables, using default animations. Value: ", iStrandContID)
	
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_IDLE
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_IDLE
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WHISKY_IDLE
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINE_IDLE
			ENDIF
		ENDIF

	ENDIF
	
	IF eReturnValue = HEIST_SELECTED_ANIM_NONE
		SCRIPT_ASSERT("[AMEC][HEIST_ANIMS] - GET_HEIST_INTO_ANIM_DURING - CRITICAL ERROR! No animation selected! This will break the game!")
		PRINTLN("[AMEC][HEIST_ANIMS] - GET_HEIST_INTO_ANIM_DURING - CRITICAL ERROR! No animation selected! This will break the game!")
		PRINTLN("[AMEC][HEIST_ANIMS]  ... iStrandContID: 	", iStrandContID)
		PRINTLN("[AMEC][HEIST_ANIMS]  ... bMale: 			", bMale)
		PRINTLN("[AMEC][HEIST_ANIMS]  ... bIntro: 			", bIntro)
		PRINTLN("[AMEC][HEIST_ANIMS]  ... bMidStrand: 		", bMidStrand)
		PRINTLN("[AMEC][HEIST_ANIMS]  ... bFinale: 			", bFinale)
	ENDIF
	
	RETURN eReturnValue

ENDFUNC


/// PURPOSE:
///    Get the heist intro animation for the part after the corona screen.
/// PARAMS:
///    iStrandContID - Strand root content ID
///    bMale - Male / Female
///    bIntro - Init heist
///    bMidStrand - Mid-strand cutscene
///    bFinale - Finale intro
/// RETURNS:
///    g_enumActiveHeistAnim - Anim ID
FUNC g_enumActiveHeistAnim GET_HEIST_INTO_ANIM_POST(INT iStrandContID, BOOL bMale, BOOL bIntro, BOOL bMidStrand, BOOL bFinale)
	
	g_enumActiveHeistAnim eReturnValue

	IF iStrandContID = g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job
	
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_2
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_2
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WHISKY_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINE_2
			ENDIF
		ENDIF
			
	ELIF iStrandContID = g_sMPTUNABLES.iroot_id_HASH_The_Prison_Break
		
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_M_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_F_2
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_TV_M_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_TV_F_2
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WINDOW_M_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINDOW_F_2
			ENDIF
		ENDIF
			
	ELIF iStrandContID = g_sMPTUNABLES.iroot_id_HASH_The_Humane_Labs_Raid
		
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_2
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_M_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_F_2
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WHISKY_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINE_2
			ENDIF
		ENDIF
			
	ELIF iStrandContID = g_sMPTUNABLES.iroot_id_HASH_Series_A_Funding
			
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_TV_M_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_TV_F_2
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_2
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WINDOW_M_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINDOW_F_2
			ENDIF
		ENDIF
			
	ELIF iStrandContID = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job
		
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_M_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_KITCHEN_F_2
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_TV_M_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_TV_F_2
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WHISKY_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINE_2
			ENDIF
		ENDIF
	
	ELSE
	
		PRINTLN("[AMEC][HEIST_ANIMS] - GET_HEIST_INTO_ANIM_POST - ERROR! StrandID did not match any tunables, using default animations. Value: ", iStrandContID)
	
		IF bIntro
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_2
			ENDIF
		ELIF bMidStrand
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_M_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_COUCH_F_2
			ENDIF
		ELIF bFinale
			IF bMale
				eReturnValue = HEIST_SELECTED_ANIM_WHISKY_2
			ELSE
				eReturnValue = HEIST_SELECTED_ANIM_WINE_2
			ENDIF
		ENDIF
	
	ENDIF
	
	IF eReturnValue = HEIST_SELECTED_ANIM_NONE
		SCRIPT_ASSERT("[AMEC][HEIST_ANIMS] - GET_HEIST_INTO_ANIM_POST - CRITICAL ERROR! No animation selected! This will break the game!")
		PRINTLN("[AMEC][HEIST_ANIMS] - GET_HEIST_INTO_ANIM_POST - CRITICAL ERROR! No animation selected! This will break the game!")
		PRINTLN("[AMEC][HEIST_ANIMS]  ... iStrandContID: 	", iStrandContID)
		PRINTLN("[AMEC][HEIST_ANIMS]  ... bMale: 			", bMale)
		PRINTLN("[AMEC][HEIST_ANIMS]  ... bIntro: 			", bIntro)
		PRINTLN("[AMEC][HEIST_ANIMS]  ... bMidStrand: 		", bMidStrand)
		PRINTLN("[AMEC][HEIST_ANIMS]  ... bFinale: 			", bFinale)
	ENDIF
	
	RETURN eReturnValue

ENDFUNC




/// PURPOSE:
///    Main animation maintenance function. Used to pre-load, play and cleanup all heist intro/outro animations.
/// PARAMS:
///    paramLoadOnly - BOOL - Do all loading and hold before playing, useful for back-to-back animation playing.
///    paramFreezeOnLastFrame - BOOL - Stop the animation on the last frame.
///    paramIsVariableCam - BOOL - Use the relative game camera heading to calculate an offset camera position (as defined in the anim struct).
PROC MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(	g_structHeistAnims& sAnimStruct, 
												BOOL paramLoadOnly = FALSE, 
												BOOL paramFreezeOnLastFrame = TRUE, 
												BOOL paramIsVariableCam = FALSE, 
												BOOL paramLoopAnimation = FALSE, 
												BOOL paramEnableVoice = TRUE,
												BOOL paramFreezeOnFirstFrame = FALSE)

	CDEBUG2LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," ****************************************** ")
	DEBUG_PRINTCALLSTACK()
	
	INT index

	SWITCH sAnimStruct.stage
	
		DEFAULT
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - DEFAULT - ERROR! Entered default heist animation state!")
			
			BREAK
	
		CASE HMS_ANIM_STAGE_INACTIVE
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_INACTIVE - In inactive anim state.")
		
			IF sAnimStruct.sActiveAnim != HEIST_SELECTED_ANIM_NONE
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_INACTIVE - ACtive animation data, moving to init...")
				SET_HEIST_ANIM_STATE(sAnimStruct, HMS_ANIM_STAGE_INIT)
			ENDIF

			BREAK
			
		CASE HMS_ANIM_STAGE_INIT
		
			IF ARE_HEIST_ANIM_ASSETS_LOADED(sAnimStruct)
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_INIT - ARE_HEIST_ANIM_ASSETS_LOADED() = TRUE, move to playing.")
				SET_HEIST_ANIM_STATE(sAnimStruct, HMS_ANIM_STAGE_PLAY)
				EXIT
			ENDIF
			
			IF sAnimStruct.bXThreadCleanup
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_INIT - bXThreadCleanup = TRUE in INIT state, move to cleanup.")
				SET_HEIST_ANIM_STATE(sAnimStruct, HMS_ANIM_STAGE_DONE)
				EXIT
			ENDIF
		
			SET_HEIST_ANIM_DETAILS_FROM_ACTIVE(sAnimStruct)
			
			START_HEIST_ANIM_TIMER(60000, sAnimStruct) // ci_HEIST_PROP_LOADING_TIME
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_INIT - One frame init for anim: ", GET_HEIST_ANIM_NAME(sAnimStruct.sActiveAnim))
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS ... Anim Name:		", sAnimStruct.sHeistAnimName)
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS ... Anim Dict:		", sAnimStruct.sHeistAnimDictionary)
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS ... Anim Loc:		", sAnimStruct.vHeistAnimScenePos)
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS ... Anim Rot:		", sAnimStruct.vHeistAnimSceneRot)
			FOR index = 0 TO (MAX_HEIST_ANIM_PROPS-1)
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS ... Anim Cam[",index,"]: 	", sAnimStruct.sHeistAnimCams[index])
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS ... Anim Prop[",index,"]:	", sAnimStruct.sHeistAnimModels[index])
			ENDFOR
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS ... Additional Ped:	", sAnimStruct.bAdditionalClone)
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS ... Additional Anim:	", sAnimStruct.sHeistAnimAdditionalName)
			
			SET_HEIST_ANIM_STATE(sAnimStruct, HMS_ANIM_STAGE_LOAD)
		
			BREAK
			
		CASE HMS_ANIM_STAGE_LOAD
		
			IF sAnimStruct.bBasicCleanupFinished
				sAnimStruct.bBasicCleanupFinished = FALSE
			ENDIF
		
			IF HAS_HEIST_ANIM_TIMER_EXPIRED(sAnimStruct)
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_LOAD - ERROR! Anim assert loader timed out!")
				SCRIPT_ASSERT("[AMEC][HEIST_ANIMS] - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_LOAD - ERROR! Anim assert loader timed out! Please enter a bug for Alastiar.")
				SET_HEIST_ANIM_STATE(sAnimStruct, HMS_ANIM_STAGE_DONE)
				EXIT
			ENDIF
			
			IF sAnimStruct.bXThreadCleanup
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_LOAD - bXThreadCleanup = TRUE in LOAD state, move to cleanup.")
				SET_HEIST_ANIM_STATE(sAnimStruct, HMS_ANIM_STAGE_DONE)
				EXIT
			ENDIF
		
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_LOAD - Loading props for anim: ", GET_HEIST_ANIM_NAME(sAnimStruct.sActiveAnim))
		
			// Request all cutscene assets.
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			REQUEST_ANIM_DICT(sAnimStruct.sHeistAnimDictionary)
			
			FOR index = 0 TO (MAX_HEIST_ANIM_PROPS-1)
				IF sAnimStruct.sHeistAnimModels[index] != DUMMY_MODEL_FOR_SCRIPT
					REQUEST_MODEL(sAnimStruct.sHeistAnimModels[index])
				ENDIF
			ENDFOR
			
			IF sAnimStruct.mnLightRig != DUMMY_MODEL_FOR_SCRIPT
				REQUEST_MODEL(sAnimStruct.mnLightRig)
			ENDIF
			
			BOOL bAssetsLoaded
			bAssetsLoaded = TRUE
			
			// Check if requested assets have been loaded.
			IF NOT HEIST_CREATE_PED_CLONE_FOR_ANIMS(sAnimStruct, FALSE)
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_LOAD - Still waiting on leader clone ped.")
				bAssetsLoaded = FALSE
			ENDIF
			
			IF sAnimStruct.bAdditionalClone
				IF NOT HEIST_CREATE_PED_CLONE_FOR_ANIMS(sAnimStruct, TRUE)
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_LOAD - Still waiting on additional clone ped.")
					bAssetsLoaded = FALSE
				ENDIF
			ENDIF

			IF NOT HAS_ANIM_DICT_LOADED(sAnimStruct.sHeistAnimDictionary)
				bAssetsLoaded = FALSE
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_LOAD - Still waiting on HAS_ANIM_DICT_LOADED(",sAnimStruct.sHeistAnimDictionary,")")
			ENDIF
			
			FOR index = 0 TO (MAX_HEIST_ANIM_PROPS-1)
				IF sAnimStruct.sHeistAnimModels[index] != DUMMY_MODEL_FOR_SCRIPT
					IF NOT HAS_MODEL_LOADED(sAnimStruct.sHeistAnimModels[index])
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_LOAD - Still waiting on prop[",index,"].")
						bAssetsLoaded = FALSE
					ENDIF
				ENDIF
			ENDFOR
			
			IF NOT DOES_CAM_EXIST(sAnimStruct.camAnimCam)
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_LOAD - Still waiting on scene cam.")
				sAnimStruct.camAnimCam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
				bAssetsLoaded = FALSE
			ENDIF
			
			IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_APARTMENT_FOLEY")
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - Still loading heist scene audio bank: HEIST_APARTMENT_FOLEY")
				bAssetsLoaded = FALSE
			ENDIF

			// Only next gen
			IF sAnimStruct.mnLightRig != DUMMY_MODEL_FOR_SCRIPT
				IF NOT HAS_MODEL_LOADED(sAnimStruct.mnLightRig)
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_LOAD - Waiting for light rig: LIGHT_CAR_RIG")
					bAssetsLoaded = FALSE
				ENDIF
			ENDIF
				
			// Once assets have been loaded, move onto the playing state.
			IF bAssetsLoaded
				sAnimStruct.bHaveFinishedPlaying = FALSE // Hacky fix for 2248445, not sure why these flags might not be reset here.
				sAnimStruct.bAtLastFrame = FALSE
				sAnimStruct.bHaveFrozenScreen = FALSE
				SET_HEIST_ANIM_STATE(sAnimStruct, HMS_ANIM_STAGE_PLAY)
			ENDIF
				
			BREAK
			
		CASE HMS_ANIM_STAGE_PLAY
		
			DISABLE_FRONTEND_THIS_FRAME()
			CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS DISABLE_FRONTEND_THIS_FRAME()")
			
			IF paramLoadOnly
				CDEBUG2LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - All assets loaded and ready, waiting for signal to play")//. Phase: ", GET_SYNCHRONIZED_SCENE_PHASE(sAnimStruct.iHeistAnimSceneID))
				
				IF NOT ARE_HEIST_ANIM_ASSETS_LOADED(sAnimStruct)
					SET_HEIST_ANIM_ASSETS_LOADED(sAnimStruct, TRUE)
				ENDIF
				
				IF NOT g_HeistPrePlanningClient.bLaunchFromPhone
				AND IS_SKYSWOOP_AT_GROUND()
				AND sAnimStruct.bPreLoadScene
				 	IF IS_SYNCHRONIZED_SCENE_RUNNING(g_HeistSharedClient.sHeistAnimFirst.iHeistAnimSceneID) // This is a total hack; it will break in the future, i know it.
						IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
							CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - s Starting new load scene 35m at: ", sAnimStruct.vHeistAnimScenePos)
							NEW_LOAD_SCENE_START_SPHERE(sAnimStruct.vHeistAnimScenePos, 35)
							sAnimStruct.bSceneLoaded = TRUE
						ENDIF
					ELSE
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - s IS_SYNCHRONIZED_SCENE_RUNNING = FALSE for FIRST anims.")
					ENDIF
				ELSE
					IF NOT IS_SKYSWOOP_AT_GROUND()
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - s SkySwoop not at ground, not douing load scene.")
					ELIF NOT sAnimStruct.bPreLoadScene
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - s bPreLoadScene = FALSE.")
					ELSE
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - s Phone launch, not doing load scene.")
					ENDIF
				ENDIF
				
				EXIT
			ENDIF
			
			IF sAnimStruct.bXThreadCleanup
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - bXThreadCleanup = TRUE in PLAY state, move to cleanup.")
				SET_HEIST_ANIM_STATE(sAnimStruct, HMS_ANIM_STAGE_DONE)
				EXIT
			ENDIF
			
			
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sAnimStruct.iHeistAnimSceneID)
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Playing anim (phase: ",GET_SYNCHRONIZED_SCENE_PHASE(sAnimStruct.iHeistAnimSceneID),"): ", GET_HEIST_ANIM_NAME(sAnimStruct.sActiveAnim))
			ELSE
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Playing anim (phase: N/A): ", GET_HEIST_ANIM_NAME(sAnimStruct.sActiveAnim))
			ENDIF
			
			THEFEED_HIDE_THIS_FRAME()
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			HIDE_ALL_OTHER_PLAYERS_FOR_HEIST_INTRO()
			SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
			
//			PRINTLN("[RBJ] - net_heist_cutscenes - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - ID: ",sAnimStruct.ID," - paramEnableVoice: ", paramEnableVoice, " - HEIST_IS_NETWORK_VOICE_ACTIVE: ", HEIST_IS_NETWORK_VOICE_ACTIVE())
//			DEBUG_PRINTCALLSTACK()
			
			IF paramEnableVoice
				IF NOT HEIST_IS_NETWORK_VOICE_ACTIVE()
					HEIST_SET_NETWORK_VOICE_ACTIVE(TRUE)
				ENDIF
			ELSE
				IF HEIST_IS_NETWORK_VOICE_ACTIVE()
					HEIST_SET_NETWORK_VOICE_ACTIVE(FALSE)
				ENDIF
			ENDIF
			
			CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Check if scene is running on ID: ", sAnimStruct.iHeistAnimSceneID)
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sAnimStruct.iHeistAnimSceneID)
				
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_FREEZE_POSITION )
				DISABLE_ALL_MP_HUD()
				DISABLE_SCRIPT_HUD(HUDPART_AWARDS, TRUE)
				FORCE_CONTEXT_SYSTEM_RESET()
				CLEAR_ALL_BIG_MESSAGES()
				
				IF GET_USINGNIGHTVISION()
				OR GET_REQUESTINGNIGHTVISION()
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Disabling NVGs.")
					ENABLE_NIGHTVISION(VISUALAID_OFF)
				ENDIF
				
				IF sAnimStruct.iRoomKey = 0
					IF DOES_ENTITY_EXIST(sAnimStruct.pedHeistLeaderClone)
						IF NOT IS_PED_DEAD_OR_DYING(sAnimStruct.pedHeistLeaderClone)
							sAnimStruct.iRoomKey = GET_ROOM_KEY_FROM_ENTITY(sAnimStruct.pedHeistLeaderClone)
							CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Saving room key: ",sAnimStruct.iRoomKey," from leader clone for animation props.")
						ENDIF
					ENDIF
				ENDIF
				
				FOR index = 0 TO (MAX_HEIST_ANIM_PROPS-1)
					IF sAnimStruct.sHeistAnimModels[index] != DUMMY_MODEL_FOR_SCRIPT
						REQUEST_MODEL(sAnimStruct.sHeistAnimModels[index])
						IF HAS_MODEL_LOADED(sAnimStruct.sHeistAnimModels[index])
							
							VECTOR vTempPos	
							IF NOT IS_PED_INJURED(sAnimStruct.pedHeistLeaderClone)
								vTempPos = GET_ENTITY_COORDS(sAnimStruct.pedHeistLeaderClone)
								CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Got coords from leader clone, value: ", vTempPos)
							ELSE
								vTempPos = sAnimStruct.vHeistAnimScenePos
								CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - ERROR! IS_ENTITY_DEAD = TRUE for leader clone. Use fallback position: ", vTempPos)
							ENDIF
							
							vTempPos.Z = vTempPos.Z + 1.0 // Make sure we don't make the object outside the room.
						
							sAnimStruct.oiPropIndices[index] = CREATE_OBJECT(sAnimStruct.sHeistAnimModels[index], vTempPos, FALSE)
							CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Created model: ",sAnimStruct.sHeistAnimModels[index]," for prop: ", sAnimStruct.sHeistAnimPropName[index], " at index: ", index, " at pos: ", vTempPos)
							
							// See comment below, though this is still required.
							SET_ENTITY_VISIBLE(sAnimStruct.oiPropIndices[index], TRUE)
							CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Set entity as VISIBLE.")
							
							// This system will force the clone peds room for all animation props now regardless of whether they need it or not.
							IF sAnimStruct.iRoomKey != 0
								IF IS_INTERIOR_READY(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
									FORCE_ROOM_FOR_ENTITY(sAnimStruct.oiPropIndices[index], GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()), sAnimStruct.iRoomKey)
									CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Forced created entity: ",sAnimStruct.sHeistAnimModels[index]," for anim: ",sAnimStruct.sHeistAnimPropName[index]," into room key: ", sAnimStruct.iRoomKey)
								ELSE
									CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - ERROR! player's interior is NOT READY!")
								ENDIF
							ENDIF
							
						ELSE
							CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - ERROR! HAS_MODEL_LOADED() = FALSE for model: ", sAnimStruct.sHeistAnimModels[index], " at index: ", index)
						ENDIF
					ENDIF
				ENDFOR
			
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Starting sync-scene and creating sync-cam.")
			
				IF NOT IS_PED_INJURED(sAnimStruct.pedHeistLeaderClone)
					IF NOT IS_ENTITY_VISIBLE(sAnimStruct.pedHeistLeaderClone)
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Setting leader clone to VISIBLE..")
						SET_ENTITY_VISIBLE(sAnimStruct.pedHeistLeaderClone, TRUE)
					ENDIF
				ENDIF
				
				IF sAnimStruct.bHaveClonedAdditional
					IF NOT IS_PED_INJURED(sAnimStruct.pedAdditionalClone)
						IF NOT IS_ENTITY_VISIBLE(sAnimStruct.pedAdditionalClone)
							CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Setting additional clone to VISIBLE..")
							SET_ENTITY_VISIBLE(sAnimStruct.pedAdditionalClone, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				sAnimStruct.iHeistAnimSceneID = CREATE_SYNCHRONIZED_SCENE(sAnimStruct.vHeistAnimScenePos, sAnimStruct.vHeistAnimSceneRot)
			
				IF paramFreezeOnLastFrame
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sAnimStruct.iHeistAnimSceneID, TRUE)
				ENDIF
				
				SET_SYNCHRONIZED_SCENE_LOOPED(sAnimStruct.iHeistAnimSceneID, paramLoopAnimation)
			
				TEXT_LABEL_63 tlSelectedCam
			
				IF paramIsVariableCam
				
					IF sAnimStruct.sActiveAnim = HEIST_SELECTED_ANIM_COUCH_F_2_GUEST
					OR sAnimStruct.sActiveAnim = HEIST_SELECTED_ANIM_COUCH_M_2_GUEST
					OR sAnimStruct.sActiveAnim = HEIST_SELECTED_ANIM_WHISKY_2_GUEST
					OR sAnimStruct.sActiveAnim = HEIST_SELECTED_ANIM_WINE_2_GUEST
					
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - paramIsVariableCam = TRUE for gues intro anim, use standard camera.")
						tlSelectedCam = sAnimStruct.sHeistAnimCams[0]
						
					ELIF sAnimStruct.sActiveAnim = HEIST_SELECTED_ANIM_PHONE
					
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - paramIsVariableCam = TRUE, using variable cam based on relative heading.")
						IF ABSF(GET_GAMEPLAY_CAM_RELATIVE_HEADING()) > 90.0
							CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Gameplay cam headeing(abs): ",ABSF(GET_GAMEPLAY_CAM_RELATIVE_HEADING())," is greater than 90, use front cam.")
							tlSelectedCam = sAnimStruct.sHeistAnimCams[1]
						ELSE
							CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - SET_HEIST_ANIM_DETAILS_FROM_ACTIVE - HMS_ANIM_STAGE_PLAY - Gameplay cam headeing(abs): ",ABSF(GET_GAMEPLAY_CAM_RELATIVE_HEADING())," is less than 90, use rear cam.")
							tlSelectedCam = sAnimStruct.sHeistAnimCams[0]
						ENDIF
						
					ELSE
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - paramIsVariableCam = TRUE, but animation does not have a camera profile, using default.")
						tlSelectedCam = sAnimStruct.sHeistAnimCams[0]
					ENDIF
					
				ELSE
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - paramIsVariableCam = FALSE, using cam 0: ",sAnimStruct.sHeistAnimCams[0])
					tlSelectedCam = sAnimStruct.sHeistAnimCams[0]
				ENDIF
			
				PLAY_SYNCHRONIZED_CAM_ANIM(	sAnimStruct.camAnimCam, 
											sAnimStruct.iHeistAnimSceneID,
											tlSelectedCam,
											sAnimStruct.sHeistAnimDictionary)
				
				FOR index = 0 TO (MAX_HEIST_ANIM_PROPS-1)
					IF sAnimStruct.sHeistAnimModels[index] != DUMMY_MODEL_FOR_SCRIPT
						IF NOT IS_STRING_NULL_OR_EMPTY(sAnimStruct.sHeistAnimPropName[index])
							IF DOES_ENTITY_EXIST(sAnimStruct.oiPropIndices[index])
								PLAY_SYNCHRONIZED_ENTITY_ANIM(	sAnimStruct.oiPropIndices[index],
																sAnimStruct.iHeistAnimSceneID,
																sAnimStruct.sHeistAnimPropName[index],
																sAnimStruct.sHeistAnimDictionary,
																INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
								CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Adding synced prop animation (",sAnimStruct.sHeistAnimPropName[index],") to scene, index: ", index)
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				
				SET_CAM_ACTIVE(sAnimStruct.camAnimCam, TRUE)
				
				IF sAnimStruct.bAdditionalClone
					IF CREATE_HEIST_ANIM_DOORS(sAnimStruct)
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Adding door to animation.")
						PLAY_SYNCHRONIZED_ENTITY_ANIM(	sAnimStruct.oiApartmentDoor,
														sAnimStruct.iHeistAnimSceneID,
														sAnimStruct.sHeistAnimDoor,
														sAnimStruct.sHeistAnimDictionary,
														INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					ENDIF
					
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Adding additional ped to animation.")
					TASK_SYNCHRONIZED_SCENE(	sAnimStruct.pedAdditionalClone, 
												sAnimStruct.iHeistAnimSceneID, 
												sAnimStruct.sHeistAnimDictionary, 
												sAnimStruct.sHeistAnimAdditionalName, 
												INSTANT_BLEND_IN, INSTANT_BLEND_OUT,
												SYNCED_SCENE_USE_PHYSICS, 
												RBF_PLAYER_IMPACT)	
					
				ENDIF
				
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Adding player ped to animation.")
				TASK_SYNCHRONIZED_SCENE(	sAnimStruct.pedHeistLeaderClone, 
											sAnimStruct.iHeistAnimSceneID, 
											sAnimStruct.sHeistAnimDictionary, 
											sAnimStruct.sHeistAnimName, 
											INSTANT_BLEND_IN, INSTANT_BLEND_OUT,
											SYNCED_SCENE_USE_PHYSICS, 
											RBF_PLAYER_IMPACT)	
											
				IF IS_CAM_ACTIVE(sAnimStruct.camAnimCam)
					RENDER_SCRIPT_CAMS(TRUE, FALSE, 0)
				ENDIF
				
				IF sAnimStruct.mnLightRig != DUMMY_MODEL_FOR_SCRIPT
					IF NOT DOES_ENTITY_EXIST(sAnimStruct.oiLightRig)
						
						VECTOR vRigPos, vRigRot
						GET_PLAYER_PROPERTY_HEIST_LOCATION(vRigPos, MP_PROP_ELEMENT_BESPOKE_CORONA_LIGHT_RIG_TEAM_0, globalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
						GET_PLAYER_PROPERTY_HEIST_ROTATION(vRigRot, MP_PROP_ELEMENT_BESPOKE_CORONA_LIGHT_RIG_TEAM_0, globalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

						sAnimStruct.oiLightRig = CREATE_OBJECT_NO_OFFSET(sAnimStruct.mnLightRig, vRigPos, FALSE)
						
						GET_PLAYER_PROPERTY_HEIST_LOCATION(vRigPos, MP_PROP_ELEMENT_BESPOKE_CORONA_LIGHT_RIG_TEAM_3, globalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
						GET_PLAYER_PROPERTY_HEIST_ROTATION(vRigRot, MP_PROP_ELEMENT_BESPOKE_CORONA_LIGHT_RIG_TEAM_3, globalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		
						SET_ENTITY_COORDS_NO_OFFSET(sAnimStruct.oiLightRig, vRigPos)
						SET_ENTITY_ROTATION(sAnimStruct.oiLightRig, vRigRot)
						SET_MODEL_AS_NO_LONGER_NEEDED(sAnimStruct.mnLightRig)
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Created light rig object at Pos: ",vRigPos,", Rot: ", vRigRot)
					ENDIF
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sAnimStruct.iHeistAnimSceneID)
					sAnimStruct.fScenePlaybackRate = GET_SYNCHRONIZED_SCENE_RATE(sAnimStruct.iHeistAnimSceneID)
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - saved scene playback rate: ", sAnimStruct.fScenePlaybackRate)
				ENDIF
				
				IF sAnimStruct.bPlayStartPostFX
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Playing postFX at anim start: MenuMGHeistOut")
					ANIMPOSTFX_PLAY("MenuMGHeistOut", 0, TRUE)
				ENDIF
				
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Stopping requested load scene.")
					NEW_LOAD_SCENE_STOP()
				ENDIF
				
				sAnimStruct.bAtLastFrame = FALSE
				sAnimStruct.bHaveFinishedPlaying = FALSE
				
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Created sync-scene with ID: ", sAnimStruct.iHeistAnimSceneID)
					
			ELSE								
			
				IF NOT paramLoopAnimation
					IF BUSYSPINNER_IS_ON()
						CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Disabling busyspinner as it was left on!")
						BUSYSPINNER_OFF()
					ENDIF
				ENDIF
			
				IF NOT paramFreezeOnFirstFrame
					IF DOES_CAM_EXIST(sAnimStruct.camAnimCam)
						IF NOT IS_CAM_ACTIVE(sAnimStruct.camAnimCam)
							SET_CAM_ACTIVE(sAnimStruct.camAnimCam, TRUE)
							CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - ERROR! Scene cam was NOT active during playback! Re-setting to active.")
						ELSE
							IF NOT IS_CAM_RENDERING(sAnimStruct.camAnimCam)
								CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Starting render for sync-scene cam.")
								RENDER_SCRIPT_CAMS(TRUE, FALSE, 0)
							ELSE
								IF NOT sAnimStruct.bSceneCamRendering
									CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Render fully started.")
									SET_SYNCHRONIZED_SCENE_PHASE(sAnimStruct.iHeistAnimSceneID, 0.0)
									sAnimStruct.bSceneCamRendering = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
			ENDIF
			
			IF sAnimStruct.sActiveAnim = HEIST_SELECTED_ANIM_COUCH_F_2_GUEST
			OR sAnimStruct.sActiveAnim = HEIST_SELECTED_ANIM_COUCH_M_2_GUEST
			OR sAnimStruct.sActiveAnim = HEIST_SELECTED_ANIM_WHISKY_2_GUEST
			OR sAnimStruct.sActiveAnim = HEIST_SELECTED_ANIM_WINE_2_GUEST
			
				IF NOT sAnimStruct.bHaveCorrectedTOD
					IF NOT IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciFORCE_SPECIFIC_TOD)
						SET_TIME_OF_DAY(g_FMMC_STRUCT.iTimeOfDay)
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Set TOD value: ", g_FMMC_STRUCT.iTimeOfDay)
					ELSE
						NETWORK_OVERRIDE_CLOCK_TIME(g_FMMC_STRUCT.iTODOverrideHours,  g_FMMC_STRUCT.iTODOverrideMinutes, 0)
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Set TOD override, Hr: ", g_FMMC_STRUCT.iTODOverrideHours, ", Min: ", g_FMMC_STRUCT.iTODOverrideMinutes)
					ENDIF
					
					sAnimStruct.bHaveCorrectedTOD = TRUE
				ENDIF
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sAnimStruct.iHeistAnimSceneID)
					IF (GET_SYNCHRONIZED_SCENE_PHASE(sAnimStruct.iHeistAnimSceneID) >= 0.358)
						IF (GET_SYNCHRONIZED_SCENE_PHASE(sAnimStruct.iHeistAnimSceneID) >= 0.858)
							IF sAnimStruct.bDoorPortalOpen
								CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Anim phase = 0.858, set door audio portal to CLOSED.")
								REMOVE_PORTAL_SETTINGS_OVERRIDE("V_DLC_HEIST_APARTMENT_DOOR_CLOSED")
								sAnimStruct.bDoorPortalOpen = FALSE
							ENDIF
						ELSE
							IF NOT sAnimStruct.bDoorPortalOpen
								CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Anim phase = 0.358, set door audio portal to OPEN.")
								SET_PORTAL_SETTINGS_OVERRIDE("V_DLC_HEIST_APARTMENT_DOOR_CLOSED", "V_DLC_HEIST_APARTMENT_DOOR_OPEN")
								sAnimStruct.bDoorPortalOpen = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			IF paramFreezeOnFirstFrame
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sAnimStruct.iHeistAnimSceneID)
					IF GET_SYNCHRONIZED_SCENE_RATE(sAnimStruct.iHeistAnimSceneID) > 0.0
						SET_SYNCHRONIZED_SCENE_RATE(sAnimStruct.iHeistAnimSceneID, 0.0)
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - paramFreezeOnFirstFrame = TRUE, setting playback rate to 0.0")
					ENDIF
				ENDIF
			ELSE
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sAnimStruct.iHeistAnimSceneID)
					IF GET_SYNCHRONIZED_SCENE_RATE(sAnimStruct.iHeistAnimSceneID) != sAnimStruct.fScenePlaybackRate
						SET_SYNCHRONIZED_SCENE_RATE(sAnimStruct.iHeistAnimSceneID, sAnimStruct.fScenePlaybackRate)
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - paramFreezeOnFirstFrame = FALSE, setting playback rate to stored value: ", sAnimStruct.fScenePlaybackRate)
					ENDIF
				ENDIF
			ENDIF
			
			UPDATE_ANIM_SOUND_STATE(sAnimStruct)
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sAnimStruct.iHeistAnimSceneID)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sAnimStruct.iHeistAnimSceneID) = 1.0
				AND sAnimStruct.iHeistAnimSceneID != INVALID_HEIST_DATA
				AND NOT paramLoopAnimation
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Sync-scene has reached phase 1.0, move to post scene cleanup.")
					SET_HEIST_ANIM_STATE(sAnimStruct, HMS_ANIM_STAGE_DONE)
					sAnimStruct.bHaveFinishedPlaying = TRUE
				ENDIF
			ENDIF
			
			IF paramFreezeOnLastFrame
			OR sAnimStruct.bFreezeOnLastFrame
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sAnimStruct.iHeistAnimSceneID)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sAnimStruct.iHeistAnimSceneID) < 1.0
					AND GET_SYNCHRONIZED_SCENE_PHASE(sAnimStruct.iHeistAnimSceneID) >= 0.99
					AND sAnimStruct.iHeistAnimSceneID != INVALID_HEIST_DATA
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - At last frame, exiting for finish next frame.")
						
						IF IS_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sAnimStruct.iHeistAnimSceneID)
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sAnimStruct.iHeistAnimSceneID, FALSE)
							CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_PLAY - Unholding scene as last frame.")
						ENDIF
						
						sAnimStruct.bAtLastFrame = TRUE
						EXIT
					ENDIF
				ENDIF
			ENDIF
		
			BREAK
			
		CASE HMS_ANIM_STAGE_DONE
			
			CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_DONE - In post-anim cleanup for: ", GET_HEIST_ANIM_NAME(sAnimStruct.sActiveAnim))
		
			REMOVE_ANIM_DICT(sAnimStruct.sHeistAnimDictionary)
			
			IF NOT sAnimStruct.bXThreadCleanup
				THEFEED_HIDE_THIS_FRAME()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				HIDE_ALL_OTHER_PLAYERS_FOR_HEIST_INTRO()
				SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
			ENDIF
			
			IF DOES_CAM_EXIST(sAnimStruct.camAnimCam)

				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_DONE - Removing sync-cam.")

				IF IS_CAM_ACTIVE(sAnimStruct.camAnimCam)
					SET_CAM_ACTIVE(sAnimStruct.camAnimCam, FALSE)
				ENDIF

				DESTROY_CAM(sAnimStruct.camAnimCam, TRUE)

			ENDIF
			
			MODEL_NAMES tempModel
			
			FOR index = 0 TO (MAX_HEIST_ANIM_PROPS-1)
				IF sAnimStruct.sHeistAnimModels[index] != DUMMY_MODEL_FOR_SCRIPT
					IF HAS_MODEL_LOADED(sAnimStruct.sHeistAnimModels[index])
						IF DOES_ENTITY_EXIST(sAnimStruct.oiPropIndices[index])
							DELETE_OBJECT(sAnimStruct.oiPropIndices[index])
						ENDIF
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_DONE - deleting and unloading model from index: ", index)
					ENDIF
				ENDIF
				
				sAnimStruct.vBespokePos[index] = <<0.0, 0.0, 0.0>>
				sAnimStruct.vBespokeRot[index] = <<0.0, 0.0, 0.0>>
			ENDFOR
			
			IF DOES_ENTITY_EXIST(sAnimStruct.oiLightRig)
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_DONE - Deleting light rig.")
				DELETE_OBJECT(sAnimStruct.oiLightRig)
			ENDIF

			IF NOT (sAnimStruct.pedHeistLeaderClone = NULL)
				IF (DOES_ENTITY_EXIST(sAnimStruct.pedHeistLeaderClone))
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_DONE - Removing leader cloned ped.")
					tempModel = GET_ENTITY_MODEL(sAnimStruct.pedHeistLeaderClone)
					SET_MODEL_AS_NO_LONGER_NEEDED(tempModel)
					DELETE_PED(sAnimStruct.pedHeistLeaderClone)
				ENDIF
			ENDIF
			
			IF NOT (sAnimStruct.pedAdditionalClone = NULL)
				IF (DOES_ENTITY_EXIST(sAnimStruct.pedAdditionalClone))
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_DONE - Removing additional cloned ped.")
					tempModel = GET_ENTITY_MODEL(sAnimStruct.pedAdditionalClone)
					SET_MODEL_AS_NO_LONGER_NEEDED(tempModel)
					DELETE_PED(sAnimStruct.pedAdditionalClone)
				ENDIF
			ENDIF
			
			IF sAnimStruct.bPreLoadScene
			AND sAnimStruct.bSceneLoaded
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_DONE - Stopping requested load scene.")
					NEW_LOAD_SCENE_STOP()
				ENDIF
				sAnimStruct.bSceneLoaded = FALSE
			ENDIF
			
			IF ANIMPOSTFX_IS_RUNNING("MenuMGHeistIntro")
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_DONE - Stopping postFX: MenuMGHeistIntro")
				ANIMPOSTFX_STOP("MenuMGHeistIntro")
			ENDIF
			
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - Removing audio-bank.")
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_APARTMENT_FOLEY")
			
			IF ARE_HEIST_ANIM_ASSETS_LOADED(sAnimStruct)
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_DONE - Re-enable player control and voice.")
				
//				IF NOT IS_PLAYER_IN_CORONA()
//					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//				ENDIF
				
				HEIST_SET_NETWORK_VOICE_ACTIVE(TRUE)

				ENABLE_ALL_MP_HUD()
				DISABLE_SCRIPT_HUD(HUDPART_AWARDS, FALSE)
				SET_DPADDOWN_ACTIVE(TRUE)
				
				SET_HEIST_ANIM_ASSETS_LOADED(sAnimStruct, FALSE)
			ENDIF
			
			sAnimStruct.bAtLastFrame 			= FALSE
			sAnimStruct.bBespokePositions 		= FALSE
			sAnimStruct.bPlayStartPostFX 		= FALSE
			sAnimStruct.bPreLoadScene			= FALSE
			sAnimStruct.sActiveAnim 			= HEIST_SELECTED_ANIM_NONE
			
			IF sAnimStruct.bAdditionalClone
				DESTROY_HEIST_ANIM_DOORS(sAnimStruct)
				sAnimStruct.bAdditionalClone = FALSE
				sAnimStruct.sHeistAnimDoor = ""
				sAnimStruct.sHeistAnimAdditionalName = ""
			ENDIF
			
			sAnimStruct.sHeistAnimRoomName = ""
			sAnimStruct.sHeistAnimRoomNameDLC = ""
			
			IF sAnimStruct.bXThreadCleanup
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_DONE - XThreadCleanup = TRUE, obliterate assets and exit state.")
				CLEAN_g_structHeistAnims_STRUCT(sAnimStruct)
			
				IF ARE_HEIST_ANIM_ASSETS_LOADED(sAnimStruct)
					SET_HEIST_ANIM_ASSETS_LOADED(sAnimStruct, FALSE)
				ENDIF
			ENDIF
			
			IF paramLoadOnly
			
				IF NOT HAS_HEIST_ANIM_TIMER_EXPIRED(sAnimStruct)
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_DONE - ERROR! paramLoadOnly = TRUE and timer has NOT expired! Moving to init state to recover.")
					SET_HEIST_ANIM_STATE(sAnimStruct, HMS_ANIM_STAGE_INIT)
				ELSE
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS - HMS_ANIM_STAGE_DONE - ERROR! paramLoadOnly = TRUE and timer HAS expired! Moving to idle state.")
					SET_HEIST_ANIM_STATE(sAnimStruct, HMS_ANIM_STAGE_INACTIVE)
				ENDIF
			
			ENDIF
			
			sAnimStruct.bBasicCleanupFinished = TRUE
		
			BREAK
			
	ENDSWITCH

ENDPROC


/// PURPOSE:
///    For when last minute emergency anim cleanups are called when they don't own the assets, we
///    need to make sure that we reset the flags at least.
PROC CLEAR_ANIM_CONTROL_FLAGS(g_structHeistAnims& sAnimStruct)

	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - CLEAR_ANIM_CONTROL_FLAGS - Starting to clean control flags ONLY.")
	DEBUG_PRINTCALLSTACK()

	sAnimStruct.bHaveClonedLeader 			= FALSE
	sAnimStruct.bHaveFinishedPlaying 		= FALSE
	sAnimStruct.bAtLastFrame 				= FALSE
	sAnimStruct.bHaveFrozenScreen 			= FALSE
	sAnimStruct.bSceneCamRendering 			= FALSE
	sAnimStruct.bAssetsLoaded 				= FALSE
	sAnimStruct.bXThreadCleanup 			= FALSE
	sAnimStruct.bAdditionalClone 			= FALSE
	sAnimStruct.bHaveClonedAdditional		= FALSE
	sAnimStruct.bBespokePositions			= FALSE
	sAnimStruct.bPlayStartPostFX			= FALSE
	sAnimStruct.bPreLoadScene				= FALSE
	sAnimStruct.bSceneLoaded				= FALSE
	sAnimStruct.bHaveBlendedHead			= FALSE
	sAnimStruct.bHaveCorrectedTOD			= FALSE

ENDPROC



/// PURPOSE:
///    External clean all heist anims, also does a total struct wipe (all ID and state data).
///    Handles x-thread asset cleanup.
PROC CLEAN_ALL_HEIST_ANIM_DETAILS(g_structHeistAnims& sAnimStruct, BOOL bCleanCamera = TRUE, BOOL bRemoveAssets = TRUE, BOOL bReturnControl = FALSE, BOOL bIgnoreXThread = FALSE)

	CDEBUG2LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - Cleaning all heist anim module details.")
	DEBUG_PRINTCALLSTACK()
	
	BOOL bXThreadCleanup = FALSE

	INT index
	
	FOR index = 0 TO (MAX_HEIST_ANIM_PROPS-1)
		IF sAnimStruct.sHeistAnimModels[index] != DUMMY_MODEL_FOR_SCRIPT
			IF DOES_ENTITY_EXIST(sAnimStruct.oiPropIndices[index])
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(sAnimStruct.oiPropIndices[index], FALSE)
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - deleting and unloading model from index: ", index)
					DELETE_OBJECT(sAnimStruct.oiPropIndices[index])
					
					IF bRemoveAssets
						SET_MODEL_AS_NO_LONGER_NEEDED(sAnimStruct.sHeistAnimModels[index])
					ENDIF
					
				ELSE
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - Prop index: ", index, " does not belong to calling script. Force xthread cleanup.")
					bXThreadCleanup = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	IF bCleanCamera

		CDEBUG2LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - bCleanCamera = TRUE, doing xthread camera cleanup.")

		IF DOES_CAM_EXIST(sAnimStruct.camAnimCam)
			RENDER_SCRIPT_CAMS(FALSE, FALSE, DEFAULT, DEFAULT, TRUE)
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - Removing sync-cam.")
			
			IF IS_CAM_ACTIVE(sAnimStruct.camAnimCam)
				SET_CAM_ACTIVE(sAnimStruct.camAnimCam, FALSE)
			ENDIF
			
			DESTROY_CAM(sAnimStruct.camAnimCam, TRUE)
		ENDIF	
		
	ENDIF
	
	IF sAnimStruct.bAdditionalClone
		DESTROY_HEIST_ANIM_DOORS(sAnimStruct)
		sAnimStruct.bAdditionalClone = FALSE
		sAnimStruct.sHeistAnimDoor = ""
		sAnimStruct.sHeistAnimAdditionalName = ""
	ENDIF
	
	sAnimStruct.sHeistAnimRoomName = ""
	sAnimStruct.sHeistAnimRoomNameDLC = ""
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sAnimStruct.sHeistAnimDictionary)
		REMOVE_ANIM_DICT(sAnimStruct.sHeistAnimDictionary)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sAnimStruct.oiLightRig)
		CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - Deleting light rig.")
		DELETE_OBJECT(sAnimStruct.oiLightRig)
	ENDIF

	MODEL_NAMES tempModel
	IF NOT (sAnimStruct.pedHeistLeaderClone = NULL)
		IF (DOES_ENTITY_EXIST(sAnimStruct.pedHeistLeaderClone))
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - Removing leader cloned ped.")
			tempModel = GET_ENTITY_MODEL(sAnimStruct.pedHeistLeaderClone)
//			STOP_SYNCHRONIZED_ENTITY_ANIM(sAnimStruct.pedHeistLeaderClone, INSTANT_BLEND_OUT, FALSE)
			
			IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(sAnimStruct.pedHeistLeaderClone, FALSE)
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - Successfully deleted leader clone ped.")
				DELETE_PED(sAnimStruct.pedHeistLeaderClone)
				
				IF bRemoveAssets
					SET_MODEL_AS_NO_LONGER_NEEDED(tempModel)
				ENDIF
				
			ELSE
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - Leader clone ped does not belong to calling script. Force xthread cleanup.")
				bXThreadCleanup = TRUE
			ENDIF
			
		ENDIF
	ENDIF
	
	IF NOT (sAnimStruct.pedAdditionalClone = NULL)
		IF (DOES_ENTITY_EXIST(sAnimStruct.pedAdditionalClone))
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - Removing additional cloned ped.")
			tempModel = GET_ENTITY_MODEL(sAnimStruct.pedAdditionalClone)
			
			IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(sAnimStruct.pedAdditionalClone, FALSE)
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - Successfully deleted additional clone ped.")
				DELETE_PED(sAnimStruct.pedAdditionalClone)
				
				IF bRemoveAssets
					SET_MODEL_AS_NO_LONGER_NEEDED(tempModel)
				ENDIF
				
			ELSE
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - Additional clone ped does not belong to calling script. Force xthread cleanup.")
				bXThreadCleanup = TRUE
			ENDIF
			
		ENDIF
	ENDIF
	
	IF sAnimStruct.bPreLoadScene
	AND sAnimStruct.bSceneLoaded
		IF IS_NEW_LOAD_SCENE_ACTIVE()
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - Stopping requested load scene.")
			NEW_LOAD_SCENE_STOP()
		ENDIF
		sAnimStruct.bSceneLoaded = FALSE
	ENDIF
		
	IF ANIMPOSTFX_IS_RUNNING("MenuMGHeistIntro")
		CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - Stopping postFX: MenuMGHeistIntro")
		ANIMPOSTFX_STOP("MenuMGHeistIntro")
		sAnimStruct.bPlayStartPostFX = FALSE
	ENDIF
	
	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - Releasing audio bank.")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_APARTMENT_FOLEY")
	
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		IF NOT HEIST_IS_NETWORK_VOICE_ACTIVE()
			HEIST_SET_NETWORK_VOICE_ACTIVE(TRUE)
		ENDIF
	ENDIF
	
	ENABLE_ALL_MP_HUD()
	DISABLE_SCRIPT_HUD(HUDPART_AWARDS, FALSE)
	sAnimStruct.bHaveFinishedPlaying = FALSE
	
	IF NOT IS_DPADDOWN_SET_AS_ACTIVE()
		SET_DPADDOWN_ACTIVE(TRUE)
	ENDIF
	
//	NETWORK_SET_VOICE_ACTIVE(TRUE)
	
	IF NOT bXThreadCleanup
		CDEBUG2LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - bXThreadCleanup = FALSE, obliterating heist anim details.")
		CLEAN_g_structHeistAnims_STRUCT(sAnimStruct)
	
		IF ARE_HEIST_ANIM_ASSETS_LOADED(sAnimStruct)
			SET_HEIST_ANIM_ASSETS_LOADED(sAnimStruct, FALSE)
		ENDIF
	ELSE
		IF NOT bIgnoreXThread
			CDEBUG2LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - bXThreadCleanup = TRUE, set anim state to cleanup for external script to pickup.")
			sAnimStruct.bXThreadCleanup = bXThreadCleanup
			SET_HEIST_ANIM_STATE(sAnimStruct, HMS_ANIM_STAGE_DONE)
			CLEAR_ANIM_CONTROL_FLAGS(sAnimStruct)
		ELSE
			CDEBUG2LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - bIgnoreXThread = TRUE, don't finish cleaning.")
		ENDIF
	ENDIF
	
	IF bReturnControl
		CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",sAnimStruct.ID," - CLEAN_ALL_HEIST_ANIM_DETAILS - bReturnControl = TRUE, setting player control back on.")
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	sAnimStruct.bBasicCleanupFinished = FALSE

ENDPROC


/// PURPOSE:
///    Check if the passed in animation module is registered as an active controller, and is in the process of playing.
/// RETURNS:
///    TRUE if module anim is playing.
FUNC BOOL IS_HEIST_ANIM_PLAYING(g_structHeistAnims& sAnimStruct)

	BOOL bIsPlaying = TRUE

	IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sAnimStruct.iHeistAnimSceneID)
	AND NOT sAnimStruct.bAtLastFrame
		bIsPlaying = FALSE
	ENDIF
			
	RETURN bIsPlaying

ENDFUNC


/// PURPOSE:
///    Check if the passed in animation module has finished playing its' animation.
/// RETURNS:
///    TRUE if module anim is finished playing.
FUNC BOOL HAS_HEIST_STRAND_ANIM_FINISHED(g_structHeistAnims& sAnimStruct)

	IF sAnimStruct.bHaveFinishedPlaying
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Check if the passed in animation module has almost finished playing it's animation
/// RETURNS:
///    TRUE if module anim is finished playing.
FUNC BOOL HAS_HEIST_STRAND_ANIM_NEARLY_FINISHED(g_structHeistAnims& sAnimStruct)
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(sAnimStruct.iHeistAnimSceneID)
		IF GET_SYNCHRONIZED_SCENE_PHASE(sAnimStruct.iHeistAnimSceneID) >= 0.98
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Contsruct a new heist anim controller module. This module can be used to form an animation chain, to add a follow on animation simply create another anim controller
///    with the selected anim details, and set the holdOnLoad flag to true until ready to play.
///    WARNING: If an instantiated controller module is placed in the ByRef param, it will DESTROY the existing module and replace it with a new one. 
/// PARAMS:
///    sAnimStruct - Struct to place the controller in.
///    ID - Controller ID (int)
///    eHeistAnim - Controller ANIM type.
PROC CONSTRUCT_NEW_HEIST_ANIM_CONTROLLER(g_structHeistAnims& sAnimStruct, INT ID, g_enumActiveHeistAnim eHeistAnim)

	CLEAN_ALL_HEIST_ANIM_DETAILS(sAnimStruct)

	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",ID," - CONSTRUCT_NEW_HEIST_ANIM_CONTROLLER - Attempting to build a new heist animation module. Details: ")
	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",ID," - CONSTRUCT_NEW_HEIST_ANIM_CONTROLLER ... ID:		", ID)
	CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - ID: ",ID," - CONSTRUCT_NEW_HEIST_ANIM_CONTROLLER ... Anim:	", GET_HEIST_ANIM_NAME(eHeistAnim))
	
	sAnimStruct.sActiveAnim = eHeistAnim
	sAnimStruct.ID = ID

ENDPROC


/// PURPOSE:
///    Check if the passed in module has an assigned animation type. 
FUNC BOOL IS_ANIM_CONTROLLER_ALREADY_ACTIVE(g_structHeistAnims& sAnimStruct)

	IF sAnimStruct.sActiveAnim != HEIST_SELECTED_ANIM_NONE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC



// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintains a Heist Cutscene Mocap sequence
//
// INPUT PARAMS:			paramCutsceneName		The name of the cutscene to trigger
//							paramIsSkippable		TRUE if the cutscene is allowed to be skipped by this player, FALSE if not
//							paramIsFinaleCutscene	TRUE if this is the Fianle cutscene, FALSE if Pre-planning
// RETURN VALUE:			BOOL					TRUE when the cutscene is over, FALSE while still ongoing
FUNC BOOL Maintain_Net_Heist_Cutscenes(TEXT_LABEL_23 paramCutsceneName, BOOL paramIsSkippable, BOOL paramIsFinaleCutscene, BOOL paramIsTutorialCutscene = FALSE, INT paramCutsceneParticipants = MAX_HEIST_CUTSCENE_PARTICIPANTS)
	
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	THEFEED_HIDE_THIS_FRAME()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
	
	// ! UNCOMMENT TO SKIP CUTSCENES !
//	g_heistMocap.stage = HMS_STAGE_PLAYING_CUTSCENE
//	RETURN TRUE
	
	SWITCH (g_heistMocap.stage)
		CASE HMS_STAGE_INACTIVE
			#IF IS_DEBUG_BUILD
				g_DEBUG_F9_Heist_Cutscene_Stage = "Heist Cut: Inactive"
			#ENDIF
			PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_INACTIVE - Activating Heist Cutscene: ", paramCutsceneName, ". IsSkippable? ", paramIsSkippable, ". Finale? ", paramIsFinaleCutscene)
			g_heistMocap.hasSafetyTimedOut = FALSE
			g_heistMocap.bCutsceneAtEnd = FALSE
			g_heistMocap.bHaveCleanedScreen = FALSE
			g_heistMocap.bForceScreenCleanup = FALSE
			ENABLE_NIGHTVISION(VISUALAID_OFF)
			//HEIST_SET_NETWORK_VOICE_ACTIVE(FALSE)
			
			FORCE_CONTEXT_SYSTEM_RESET()

			SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_ESTABLISHING_SHOT)
			RETURN FALSE
			
		CASE HMS_STAGE_ESTABLISHING_SHOT
			
			SWITCH (g_heistMocap.subStage)

				DEFAULT
				
					#IF IS_DEBUG_BUILD
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - DEFAULT - ERROR! g_heistMocap.subStage was INVALID, value: ", ENUM_TO_INT(g_heistMocap.subStage))
						g_DEBUG_F9_Heist_Cutscene_Stage = "Cuts-Sub: INVALID!"
					#ENDIF
					
					BREAK
					
				CASE HMS_SUB_STAGE_INACTIVE
				
					#IF IS_DEBUG_BUILD
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_INACTIVE - Currently in IDLE/INACIVE state, setting up cutscene: ", paramCutsceneName)
						g_DEBUG_F9_Heist_Cutscene_Stage = "Cuts-Sub: Inactive"
					#ENDIF
					
					IF NOT Downloaded_Heist_Cutscene_Data_File(paramCutsceneName, g_heistMocap.iConcatParts)
						RETURN FALSE
					ENDIF
					
					Setup_Heist_Cutscene(paramCutsceneName)
					
					Start_Mocap_Triggering_Safety_Timeout()
					
					SET_HEIST_ENTER_CUTS_SUB_STATE(HMS_SUB_STAGE_CREATE_CAMERA)
					
					BREAK
					
				CASE HMS_SUB_STAGE_CREATE_CAMERA
				
					#IF IS_DEBUG_BUILD
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_CREATE_CAMERA - Creating establishing shot camera.")
						g_DEBUG_F9_Heist_Cutscene_Stage = "Cuts-Sub: Create Cam"
					#ENDIF
					
					IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_PLAYER_USING_PROPERTY)
						#IF IS_DEBUG_BUILD
							g_DEBUG_F9_Heist_Cutscene_Stage = "Cuts-Sub: Wait for apt."
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_CREATE_CAMERA - Waiting for apartment script to fully load before grabbing location data.")
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							g_DEBUG_F9_Heist_Cutscene_Stage = "Cuts-Sub: Create Cam"
						#ENDIF
					
						SET_HEIST_ENTER_CUTS_SUB_STATE(HMS_SUB_STAGE_CREATE_PEDS)
							
					ENDIF
				
					BREAK
				
				CASE HMS_SUB_STAGE_CREATE_PEDS
					
					#IF IS_DEBUG_BUILD
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_CREATE_PEDS - Creating mo-cap peds and doors.")
						g_DEBUG_F9_Heist_Cutscene_Stage = "Cuts-Sub: Create Peds"
					#ENDIF
					
					IF (Create_Heist_Cutscene_Peds())
					AND Create_Heist_Cutscene_Doors()
						SET_HEIST_ENTER_CUTS_SUB_STATE(HMS_SUB_STAGE_REQUEST_CUTSCENE)
					ENDIF
				
					BREAK
				
				CASE HMS_SUB_STAGE_REQUEST_CUTSCENE
				
					#IF IS_DEBUG_BUILD
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_REQUEST_CUTSCENE - Requsting cutscene: ", paramCutsceneName)
						g_DEBUG_F9_Heist_Cutscene_Stage = "Cuts-Sub: Get Cutscene"
					#ENDIF
					
					INT iProperty
					iProperty = globalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
						
					IF IS_THIS_CUTSCENE_PRISON_BESPOKE(paramCutsceneName)
						// Custom High-end
						IF IS_PROPERTY_CUSTOM_APARTMENT(iProperty)
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_REQUEST_CUTSCENE - CUST - Requesting sections 5 + 6-9")
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(g_heistMocap.cutsceneName, CS_SECTION_5 |  CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 | CS_SECTION_9)
						// High-end Stilt A
						ELIF IS_PROPERTY_STILT_APARTMENT(iProperty, PROPERTY_STILT_APT_5_BASE_A)
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_REQUEST_CUTSCENE - STILT A - Requesting sections 3 + 6-9")
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(g_heistMocap.cutsceneName, CS_SECTION_3 |  CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 | CS_SECTION_9)
						// High-end Stilt B
						ELIF IS_PROPERTY_STILT_APARTMENT(iProperty, PROPERTY_STILT_APT_1_BASE_B)
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_REQUEST_CUTSCENE - STILT B - Requesting sections 4 + 6-9")
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(g_heistMocap.cutsceneName, CS_SECTION_4 |  CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 | CS_SECTION_9)
						// Business High-end
						ELIF IS_CURRENT_APARTMENT_DLC()
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_REQUEST_CUTSCENE - DLC - Requesting sections 2 + 6-9")
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(g_heistMocap.cutsceneName, CS_SECTION_2 |  CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 | CS_SECTION_9)
						// Classic High-end
						ELSE
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_REQUEST_CUTSCENE - OLD - Requesting sections 1 + 6-9")
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(g_heistMocap.cutsceneName, CS_SECTION_1 | CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 | CS_SECTION_9)
						ENDIF
					ELIF IS_THIS_CUTSCENE_NARCOTICS_BESPOKE(paramCutsceneName)
						// Custom High-end
						IF IS_PROPERTY_CUSTOM_APARTMENT(iProperty)
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_REQUEST_CUTSCENE - CUST - Requesting sections 1 + 6 + 7")
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(g_heistMocap.cutsceneName, CS_SECTION_1 | CS_SECTION_6 |  CS_SECTION_7)
						// High-end Stilt A
						ELIF IS_PROPERTY_STILT_APARTMENT(iProperty, PROPERTY_STILT_APT_5_BASE_A)
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_REQUEST_CUTSCENE - STILT A - Requesting sections 1 + 4 + 7")
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(g_heistMocap.cutsceneName, CS_SECTION_1 | CS_SECTION_4 |  CS_SECTION_7)
						// High-end Stilt B
						ELIF IS_PROPERTY_STILT_APARTMENT(iProperty, PROPERTY_STILT_APT_1_BASE_B)
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_REQUEST_CUTSCENE - STILT B - Requesting sections 1 + 5 + 7")
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(g_heistMocap.cutsceneName, CS_SECTION_1 | CS_SECTION_5 |  CS_SECTION_7)
						// Business High-end
						ELIF IS_CURRENT_APARTMENT_DLC()
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_REQUEST_CUTSCENE - DLC - Requesting sections 1 + 3 + 7")
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(g_heistMocap.cutsceneName, CS_SECTION_1 | CS_SECTION_3 |  CS_SECTION_7)
						// Classic High-end
						ELSE
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_REQUEST_CUTSCENE - OLD - Requesting sections 1 + 2 + 7")
							REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(g_heistMocap.cutsceneName, CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_7)
						ENDIF
					ELSE
						REQUEST_CUTSCENE(g_heistMocap.cutsceneName)
					ENDIF
					
					SET_HEIST_UPDATE_AVAILABLE(TRUE)
					
					// Need to wait a frame before the 'can_request_assets...' check
					SET_HEIST_ENTER_CUTS_SUB_STATE(HMS_SUB_STAGE_REQUEST_CUTSCENE_ASSETS)
				
					BREAK
				
				CASE HMS_SUB_STAGE_REQUEST_CUTSCENE_ASSETS
				
					#IF IS_DEBUG_BUILD
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_REQUEST_CUTSCENE_ASSETS - Requesting assets for cutscene: ", paramCutsceneName)
						g_DEBUG_F9_Heist_Cutscene_Stage = "Cuts-Sub: Get Assets"
					#ENDIF

					IF (CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY())
					
						SWITCH paramCutsceneParticipants
							CASE MAX_HEIST_CUTSCENE_PARTICIPANTS
								PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_REQUEST_CUTSCENE_ASSETS - Set streaming flags for ",MAX_HEIST_CUTSCENE_PARTICIPANTS," participants.")
								SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
								SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_2", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
								SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_3", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
								SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_4", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
							BREAK
							
							CASE MAX_HEIST_TUTORIAL_PARTICIPANTS
								PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_REQUEST_CUTSCENE_ASSETS - Set streaming flags for ",MAX_HEIST_TUTORIAL_PARTICIPANTS," participants.")
								SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_1", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
								SET_CUTSCENE_ENTITY_STREAMING_FLAGS("MP_2", DUMMY_MODEL_FOR_SCRIPT, CES_DONT_STREAM_AND_APPLY_VARIATIONS)
							BREAK
						ENDSWITCH
						
						SET_HEIST_ENTER_CUTS_SUB_STATE(HMS_SUB_STAGE_CHECK_CUTSCENE_LOADED)
					ELSE
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_REQUEST_CUTSCENE_ASSETS - Waiting for CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY to return TRUE.")
					ENDIF
					
					IF (Has_Mocap_Triggering_Safety_Timeout_Expired())
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_REQUEST_CUTSCENE_ASSETS - Safety Timeout. Cleaning up cutscene: ", paramCutsceneName)
						REMOVE_CUTSCENE()
						g_heistMocap.hasSafetyTimedOut = TRUE
						// ...need to allow certain heist stages to get hit to ensure external stuff moves on
						SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_CUTSCENE_JUST_STARTED)
					ENDIF
				
					BREAK
				
				CASE HMS_SUB_STAGE_CHECK_CUTSCENE_LOADED
				
					#IF IS_DEBUG_BUILD
						g_DEBUG_F9_Heist_Cutscene_Stage = "Cuts-Sub: Cuts Loaded?"
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_CHECK_CUTSCENE_LOADED - Checking if cutscene has loaded: ", paramCutsceneName)
					#ENDIF
				
					IF (HAS_CUTSCENE_LOADED())
						CLEAR_PRINTS()
						CLEAR_HELP()
						
						IF NOT GET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD()
						AND NOT IS_THIS_CUTSCENE_PRISON_BESPOKE(paramCutsceneName)
						AND NOT g_replayHeistIsFromBoard
						
							CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimFirst)
							CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimSecond)
							CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimThird)
							
							VECTOR vHeistCoronaLocation
							GET_PLAYER_PROPERTY_HEIST_LOCATION(vHeistCoronaLocation, MP_PROP_ELEMENT_HEIST_PLAN_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
							SET_ENTITY_COORDS(PLAYER_PED_ID(), vHeistCoronaLocation)
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_CHECK_CUTSCENE_LOADED - Moving player to: ", vHeistCoronaLocation)
							
							START_MOCAP_ESTABLISHING_SHOT_TIMER()
						ENDIF
						
						SET_HEIST_ENTER_CUTS_SUB_STATE(HMS_SUB_STAGE_CHECK_PLANNING_LOADED)
						SET_HEIST_UPDATE_AVAILABLE(TRUE)
					ELSE
						IF (Has_Mocap_Triggering_Safety_Timeout_Expired())
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_CHECK_CUTSCENE_LOADED - Safety Timeout. Cleaning up cutscene: ", paramCutsceneName)
							REMOVE_CUTSCENE()
							g_heistMocap.hasSafetyTimedOut = TRUE
							// ...need to allow certain heist stages to get hit to ensure external stuff moves on
							SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_CUTSCENE_JUST_STARTED)
						ENDIF
					ENDIF
				
					BREAK
					
				CASE HMS_SUB_STAGE_CHECK_PLANNING_LOADED
				
					#IF IS_DEBUG_BUILD
						g_DEBUG_F9_Heist_Cutscene_Stage = "Cuts-Sub: Board Loaded?"
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_CHECK_PLANNING_LOADED - Checking if planning board has loaded")
					#ENDIF
				
					IF (Has_Mocap_Triggering_Safety_Timeout_Expired())
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_CHECK_PLANNING_LOADED - Safety Timeout. Cleaning up cutscene: ", paramCutsceneName)
						REMOVE_CUTSCENE()
						g_heistMocap.hasSafetyTimedOut = TRUE
						// ...need to allow certain heist stages to get hit to ensure external stuff moves on
						SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_CUTSCENE_JUST_STARTED)
					ENDIF
					
					IF NOT paramIsTutorialCutscene
					AND GET_HEIST_UPDATE_AVAILABLE()
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_CHECK_PLANNING_LOADED - Waiting for heist board to update before loading intro anims. Flag: ", GET_HEIST_UPDATE_AVAILABLE())
						RETURN FALSE
					ENDIF

					IF NOT GET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD()
					AND NOT IS_THIS_CUTSCENE_PRISON_BESPOKE(paramCutsceneName)
					AND NOT g_replayHeistIsFromBoard

						IF NOT IS_ANIM_CONTROLLER_ALREADY_ACTIVE(g_HeistSharedClient.sHeistAnimFirst)

							// Setup the establishing anim.
							g_enumActiveHeistAnim eHeistAnim	
							
							IF paramIsFinaleCutscene
								IF (IS_PLAYER_PED_FEMALE(INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex)))
									IF IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
										eHeistAnim = HEIST_SELECTED_ANIM_WINE_2_GUEST
									ELSE
										eHeistAnim = GET_HEIST_INTO_ANIM_POST(	g_FMMC_STRUCT.iRootContentIDHash, 
																				FALSE, // FEMALE
																				FALSE,
																				FALSE,
																				TRUE) // FINALE
									ENDIF
								ELSE
									IF IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
										eHeistAnim = HEIST_SELECTED_ANIM_WHISKY_2_GUEST
									ELSE
										eHeistAnim = GET_HEIST_INTO_ANIM_POST(	g_FMMC_STRUCT.iRootContentIDHash, 
																				TRUE, // MALE
																				FALSE,
																				FALSE,
																				TRUE) // FINALE
									ENDIF
								ENDIF
								CONSTRUCT_NEW_HEIST_ANIM_CONTROLLER(g_HeistSharedClient.sHeistAnimFirst, 0, eHeistAnim)
							ELSE
								IF DO_HEIST_TRIGGER_MID_STRAND_CUTSCENE()
									IF (IS_PLAYER_PED_FEMALE(INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex)))
										IF IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
											eHeistAnim = HEIST_SELECTED_ANIM_COUCH_F_2_GUEST
										ELSE
											eHeistAnim = GET_HEIST_INTO_ANIM_POST(	g_FMMC_STRUCT.iRootContentIDHash, 
																					FALSE, // FEMALE
																					FALSE,
																					TRUE, // MIDSTRAND
																					FALSE)
										ENDIF
									ELSE
										IF IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
											eHeistAnim = HEIST_SELECTED_ANIM_COUCH_M_2_GUEST
										ELSE
											eHeistAnim = GET_HEIST_INTO_ANIM_POST(	g_FMMC_STRUCT.iRootContentIDHash, 
																					TRUE, // MALE
																					FALSE,
																					TRUE, // MIDSTRAND
																					FALSE)
										ENDIF
									ENDIF
									CONSTRUCT_NEW_HEIST_ANIM_CONTROLLER(g_HeistSharedClient.sHeistAnimFirst, 0, eHeistAnim)
								ELSE
									IF (IS_PLAYER_PED_FEMALE(INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex)))
										IF IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
											eHeistAnim = HEIST_SELECTED_ANIM_COUCH_F_2_GUEST
										ELSE
											eHeistAnim = GET_HEIST_INTO_ANIM_POST(	g_FMMC_STRUCT.iRootContentIDHash, 
																					FALSE, // FEMALE
																					TRUE, // INTRO
																					FALSE,
																					FALSE)
										ENDIF
									ELSE
										IF IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
											eHeistAnim = HEIST_SELECTED_ANIM_COUCH_M_2_GUEST
										ELSE
											eHeistAnim = GET_HEIST_INTO_ANIM_POST(	g_FMMC_STRUCT.iRootContentIDHash, 
																					TRUE, // MALE
																					TRUE, // INTRO
																					FALSE,
																					FALSE)
										ENDIF
									ENDIF
									CONSTRUCT_NEW_HEIST_ANIM_CONTROLLER(g_HeistSharedClient.sHeistAnimFirst, 0, eHeistAnim)
								ENDIF
							ENDIF
							
						ENDIF
						
						IF NOT ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimFirst)
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_CHECK_PLANNING_LOADED - Waiting for intro anim to load...")
							MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimFirst, TRUE, DEFAULT, DEFAULT, DEFAULT, FALSE)
							RETURN FALSE
						ELSE
							//    Fix for B* 2150554 & 2171063 RowanJ
							//	  Hold the cleanup for one second to ensure that the loading of the next scene is complete.
							//	  Time can be upped in HAS_CORONA_RELEASE_TIMER_EXPIRED if needed.
							IF NOT g_heistMocap.bCoronaReleaseTimerActive
								START_CORONA_RELEASE_TIMER()
//								#IF IS_DEBUG_BUILD
//								IF GET_COMMANDLINE_PARAM_EXISTS("heist_spew_network_time")
//									PRINTLN("[RBJ][HEIST_ANIMS] - MAINTAIN_NET_HEIST_CUTSCENES - HMS_SUB_STAGE_CHECK_PLANNING_LOADED - Corona timer started, and returning false.")
//								ENDIF
//								#ENDIF
								RETURN FALSE
							ELSE
								IF HAS_CORONA_RELEASE_TIMER_EXPIRED()
//									#IF IS_DEBUG_BUILD
//									IF GET_COMMANDLINE_PARAM_EXISTS("heist_spew_network_time")
//										PRINTLN("[RBJ][HEIST_ANIMS] - MAINTAIN_NET_HEIST_CUTSCENES - HMS_SUB_STAGE_CHECK_PLANNING_LOADED - HAS_CORONA_RELEASE_TIMER_EXPIRED() = TRUE so clearing corona timer.")
//									ENDIF
//									#ENDIF
									CLEAR_CORONA_RELEASE_TIMER()
									// Move on after timer expired.
								ELSE
//									#IF IS_DEBUG_BUILD
//									IF GET_COMMANDLINE_PARAM_EXISTS("heist_spew_network_time")
//										PRINTLN("[RBJ][HEIST_ANIMS] - MAINTAIN_NET_HEIST_CUTSCENES - HMS_SUB_STAGE_CHECK_PLANNING_LOADED - HAS_CORONA_RELEASE_TIMER_EXPIRED() = FALSE so returning FALSE.")
//									ENDIF
//									#ENDIF
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
					
					ELSE
						IF IS_THIS_CUTSCENE_PRISON_BESPOKE(paramCutsceneName)
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_CHECK_PLANNING_LOADED - Skipping establishing shot because IS_THIS_CUTSCENE_PRISON_BESPOKE() = TRUE.")
						ELIF GET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD()
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_CHECK_PLANNING_LOADED - Skipping establishing shot because GET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD() = TRUE.")
						ELIF g_replayHeistIsFromBoard
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_CHECK_PLANNING_LOADED - Skipping establishing shot because g_replayHeistIsFromBoard = TRUE.")
						ENDIF
					ENDIF
					
					SET_HEIST_UPDATE_AVAILABLE(TRUE)
					
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_ANIMS] - *** Moving onto HMS_SUB_STAGE_WAIT ***")
					SET_HEIST_ENTER_CUTS_SUB_STATE(HMS_SUB_STAGE_WAIT)
				
					BREAK
					
				CASE HMS_SUB_STAGE_WAIT
				
					#IF IS_DEBUG_BUILD
						g_DEBUG_F9_Heist_Cutscene_Stage = "Cuts-Sub: Waiting..."
//						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_WAIT - Waiting for establishing shot to expire. Is finale: ", paramIsFinaleCutscene)
					#ENDIF
					
					IF NOT paramIsFinaleCutscene
					AND NOT paramIsTutorialCutscene
						IF NOT IS_MP_HEIST_PRE_PLANNING_ACTIVE()
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_WAIT - Waiting for IS_MP_HEIST_PRE_PLANNING_ACTIVE() = TRUE")
							RETURN FALSE
						ENDIF
					ELIF paramIsFinaleCutscene
						IF NOT IS_MP_HEIST_PLANNING_ACTIVE()
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_WAIT - Waiting for IS_MP_HEIST_PLANNING_ACTIVE() = TRUE")
							RETURN FALSE
						ENDIF
					ENDIF
				
					IF NOT GET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD()
					AND NOT IS_THIS_CUTSCENE_PRISON_BESPOKE(paramCutsceneName)
					AND NOT g_replayHeistIsFromBoard
					
						IF (Has_Mocap_Triggering_Safety_Timeout_Expired())
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_WAIT - Safety Timeout. Cleaning up cutscene: ", paramCutsceneName)
							g_heistMocap.hasSafetyTimedOut = TRUE
							REMOVE_CUTSCENE()
							// ...need to allow certain heist stages to get hit to ensure external stuff moves on
							SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_CUTSCENE_JUST_STARTED)
							RETURN FALSE
						ENDIF
						
						IF NOT g_heistMocap.bHaveCleanedScreen
							IF NOT g_heistMocap.bForceScreenCleanup
								PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_WAIT - Setting bForceScreenCleanup as screen is NOT yet clear.")
								g_heistMocap.bForceScreenCleanup = TRUE
							ENDIF
						ELSE
							IF g_heistMocap.bForceScreenCleanup
								PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_WAIT - Setting bForceScreenCleanup to FALSE, screen has been manually cleared.")
								g_heistMocap.bForceScreenCleanup = FALSE
							ENDIF
						ENDIF
						
						IF NOT HAS_HEIST_STRAND_ANIM_NEARLY_FINISHED(g_HeistSharedClient.sHeistAnimFirst)
						
							MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimFirst, FALSE, FALSE, DEFAULT, DEFAULT, FALSE)
							
							IF HAS_MOCAP_ESTABLISHING_SHOT_TIMER_EXPIRED()
								PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_WAIT - ERROR! HAS_MOCAP_ESTABLISHING_SHOT_TIMER_EXPIRED = TRUE, move the cutscene.")
								Register_Appartment_Door_Cutscene()
								Register_Players_For_Heist_Cutscene(paramCutsceneParticipants)
								Make_All_Clone_Peds_Visible_For_Heist_Cutscene()
								Move_Heist_Cutscene_To_Heist_Apartment(g_heistMocap.iConcatParts)
								g_heistMocap.hasEstTimedOut = TRUE
								SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_CHECK_CUTSCENE_PLAYING)
							ENDIF
							
						ELSE
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_WAIT - Moving to cutscene from intro anim.")
							Register_Appartment_Door_Cutscene()
							Register_Players_For_Heist_Cutscene(paramCutsceneParticipants)
							Make_All_Clone_Peds_Visible_For_Heist_Cutscene()
							Move_Heist_Cutscene_To_Heist_Apartment(g_heistMocap.iConcatParts)
							SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_CHECK_CUTSCENE_PLAYING)
						ENDIF
						
					ELSE
					
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_WAIT - Waiting for g_bCelebrationScreenIsActive to be FALSE.")
						
						IF NOT g_bCelebrationScreenIsActive
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_ESTABLISHING_SHOT - HMS_SUB_STAGE_WAIT - g_bCelebrationScreenIsActive = FALSE, move to player cutscene.")
							Register_Appartment_Door_Cutscene()
							Register_Players_For_Heist_Cutscene(paramCutsceneParticipants)
							Make_All_Clone_Peds_Visible_For_Heist_Cutscene()
							Move_Heist_Cutscene_To_Heist_Apartment(g_heistMocap.iConcatParts)
							SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_CHECK_CUTSCENE_PLAYING)
							SET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD(FALSE)
						ENDIF
						
					ENDIF
				
					BREAK
			
			ENDSWITCH
			
			RETURN FALSE
			
		CASE HMS_STAGE_CHECK_CUTSCENE_PLAYING
			#IF IS_DEBUG_BUILD
				g_DEBUG_F9_Heist_Cutscene_Stage = "Heist Cut: Is Playing?"
			#ENDIF
			PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_CHECK_CUTSCENE_PLAYING - Check if cutscene is playing: ", paramCutsceneName)
			// Safety Timed out so skipping through the stages?
			IF (g_heistMocap.hasSafetyTimedOut)
				PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_CHECK_CUTSCENE_PLAYING - Safety Timeout had been hit. Skipping through cutscene stages")
				SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_CUTSCENE_JUST_STARTED)
				RETURN FALSE
			ENDIF
			// Normal triggering
			IF (IS_CUTSCENE_PLAYING())
				SET_SKYFREEZE_CLEAR()
				
				IF HEIST_IS_NETWORK_VOICE_ACTIVE()
					PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_CHECK_CUTSCENE_PLAYING - Disabling VOIP for cutscene.")
					HEIST_SET_NETWORK_VOICE_ACTIVE(FALSE)
				ENDIF
				
				IF GET_USINGNIGHTVISION()
				OR GET_REQUESTINGNIGHTVISION()
					PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_CHECK_CUTSCENE_PLAYING - Disabling NVGs.")
					ENABLE_NIGHTVISION(VISUALAID_OFF)
				ENDIF
					
				SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_CUTSCENE_JUST_STARTED)
			ELSE
				// ...check if the game has been stuck here for too long
				IF (Has_Mocap_Triggering_Safety_Timeout_Expired())
					PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_CHECK_CUTSCENE_PLAYING - Safety Timeout. Cleaning up cutscene triggering: ", paramCutsceneName)
					g_heistMocap.hasSafetyTimedOut = TRUE
					REMOVE_CUTSCENE()
					// ...need to allow certain heist stages to get hit to ensure external stuff moves on
					SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_CUTSCENE_JUST_STARTED)
				ENDIF
			ENDIF
			RETURN FALSE
			
		CASE HMS_STAGE_CUTSCENE_JUST_STARTED
			#IF IS_DEBUG_BUILD
				g_DEBUG_F9_Heist_Cutscene_Stage = "Heist Cut: Started"
				PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_CUTSCENE_JUST_STARTED - Check if cutscene just started: ", paramCutsceneName)
			#ENDIF
			
			// Safety Timed out so skipping through the stages?
			IF (g_heistMocap.hasSafetyTimedOut)
				PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_CUTSCENE_JUST_STARTED - Safety Timeout had been hit. Skipping through cutscene stages")
				SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_PLAYING_CUTSCENE)
				RETURN FALSE
			ENDIF
			
			IF IS_THIS_CUTSCENE_TUTORIAL_MIDPOINT(paramCutsceneName)
				SET_HEIST_TUT_CUTS_MIDPOINT_BOARD_STATE(HEIST_TUT_CUTS_EMPTY)
			ENDIF
			
			BROADCAST_GENERAL_EVENT(GENERAL_EVENT_HEIST_PREPLAN_UPDATE, ALL_PLAYERS())
			
			CLEAR_BIT_ENUM( g_heistMocap.iBitSet, H_MOCAP_STRUCT_TrevorTattooApplied )
			
			// Normal triggering
			SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_PLAYING_CUTSCENE)
			RETURN FALSE
			
		CASE HMS_STAGE_PLAYING_CUTSCENE
			#IF IS_DEBUG_BUILD
				g_DEBUG_F9_Heist_Cutscene_Stage = "Heist Cut: Playing"
			#ENDIF
			#IF IS_DEBUG_BUILD
				DEBUG_Maintain_Cutscene_Skip_Help(paramIsSkippable)
			#ENDIF
			Maintain_Leader_Skip_Cutscene_Help(paramIsSkippable)
			
			IF g_replayHeistIsFromBoard
				g_replayHeistIsFromBoard = FALSE
			ENDIF
			
			// Safety Timed out so skipping through the stages?
			IF (g_heistMocap.hasSafetyTimedOut)
				PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - Safety Timeout had been hit. Skipping through cutscene stages")
				SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_DELETE_PEDS)
				g_heistMocap.bCutsceneAtEnd = TRUE
				RETURN FALSE
			ENDIF
			
			IF BUSYSPINNER_IS_ON()
				PRINTLN(".KGM [Heist][Cutscene] - Disabling busyspinner as it was left on!")
				BUSYSPINNER_OFF()
			ENDIF
			
			IF ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimFirst)
			OR IS_ANIM_CONTROLLER_ALREADY_ACTIVE(g_HeistSharedClient.sHeistAnimFirst)
				PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - Cleaning up intro animation details.")
				CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimFirst, FALSE)
			ENDIF
			
			IF IS_THIS_CUTSCENE_TUTORIAL_MIDPOINT(paramCutsceneName)
				IF NOT (HAS_CUTSCENE_FINISHED())
				
					PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - Monitoring cutscene events. Board cuts state: ", GET_HEIST_TUT_CUTS_BOARD_STATE_NAME(GET_HEIST_TUT_CUTS_MIDPOINT_BOARD_STATE()))
					
					ENTITY_INDEX tempEntity 
					PED_INDEX tempPed
					tempEntity = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Lester") 
					
					IF DOES_ENTITY_EXIST(tempEntity) 
						tempPed = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity) 
						
						IF NOT IS_PED_DEAD_OR_DYING(tempPed)

							SWITCH GET_HEIST_TUT_CUTS_MIDPOINT_BOARD_STATE()
								
								CASE HEIST_TUT_CUTS_NONE
									PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - Tut cuts state is DEFAULT, move to EMPTY.")
									SET_HEIST_TUT_CUTS_MIDPOINT_BOARD_STATE(HEIST_TUT_CUTS_EMPTY)
									BREAK
								
								CASE HEIST_TUT_CUTS_EMPTY
									IF HAS_ANIM_EVENT_FIRED(tempPed, GET_HASH_KEY("MARKER_SCALEFORM"))
										PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - HAS_ANIM_EVENT_FIRED = TRUE for event MARKER_SCALEFORM, setting board cuts state to TEXT.")
										SET_HEIST_TUT_CUTS_MIDPOINT_BOARD_STATE(HEIST_TUT_CUTS_TEXT)
									ELSE
										PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - HAS_ANIM_EVENT_FIRED = FALSE for event MARKER_SCALEFORM, waiting for event...")
									ENDIF
									BREAK
									
								CASE HEIST_TUT_CUTS_TEXT
									IF HAS_ANIM_EVENT_FIRED(tempPed, GET_HASH_KEY("FOTOS_SCALEFORM"))
										PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - HAS_ANIM_EVENT_FIRED = TRUE for event FOTOS_SCALEFORM, setting board cuts state to PICTURES.")
										SET_HEIST_TUT_CUTS_MIDPOINT_BOARD_STATE(HEIST_TUT_CUTS_PICTURES)
									ELSE
										PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - HAS_ANIM_EVENT_FIRED = FALSE for event FOTOS_SCALEFORM, waiting for event...")
									ENDIF
									BREAK
									
								CASE HEIST_TUT_CUTS_PICTURES
									IF HAS_ANIM_EVENT_FIRED(tempPed, GET_HASH_KEY("MAP_SCALEFORM"))
										PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - HAS_ANIM_EVENT_FIRED = TRUE for event MAP_SCALEFORM, setting board cuts state to MAP.")
										SET_HEIST_TUT_CUTS_MIDPOINT_BOARD_STATE(HEIST_TUT_CUTS_MAP)
									ELSE
										PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - HAS_ANIM_EVENT_FIRED = FALSE for event MAP_SCALEFORM, waiting for event...")
									ENDIF
									BREAK
							
							ENDSWITCH
							
						ENDIF
					ELSE
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - 'Lester' entity does not exist; cannot check for events!")
					ENDIF

				ENDIF
			ENDIF
			
			IF paramIsTutorialCutscene
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - Cutscene is about to finish, pause renderphases to prevent lighting pops.")
					SET_SKYFREEZE_FROZEN(TRUE)
				ENDIF
			ENDIF
			
			// 2221384 - set trevor's RIP michael tattoo
			IF IS_THIS_CUTSCENE_NARCOTICS_BESPOKE(paramCutsceneName)
				IF NOT IS_BIT_SET_ENUM( g_heistMocap.iBitSet, H_MOCAP_STRUCT_TrevorTattooApplied )
					IF DOES_CUTSCENE_ENTITY_EXIST( "TREVOR" )
						PED_INDEX pedTemp
						pedTemp = GET_PED_INDEX_FROM_ENTITY_INDEX( GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY( "TREVOR", PLAYER_TWO ) )
						IF NOT IS_PED_INJURED( pedTemp )
							SETUP_PLAYER_PED_DEFAULTS_FOR_MP( pedTemp )
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - Set Trevor's tattoo on ped ", NATIVE_TO_INT( pedTemp ) )
							SET_BIT_ENUM( g_heistMocap.iBitSet, H_MOCAP_STRUCT_TrevorTattooApplied )
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// Normal triggering
			IF (HAS_CUTSCENE_FINISHED())
			
				PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - Cutscene thinks it has played to the end: ", paramCutsceneName)
				
				IF paramIsTutorialCutscene
				
					SET_SKYFREEZE_FROZEN()
				
					PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - paramIsTutorialCutscene = TRUE, do fade out for immediate exit.")
					IF g_heistMocap.bFadedToBlack
						IF NOT IS_SCREEN_FADED_OUT()
						AND NOT IS_SCREEN_FADING_OUT()
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - HAS_CUTSCENE_FINISHED = TRUE and bFadedToBlack = TRUE but scene is not faded!")
							DO_SCREEN_FADE_OUT(500)
						ENDIF
					ENDIF
					
					PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - Playing postFX: MenuMGHeistIn")
					ANIMPOSTFX_PLAY("MenuMGHeistIn", 0, TRUE)
					SET_CORONA_FREEZE_AND_START_FX()
					
					IF NOT BUSYSPINNER_IS_ON() 
						BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("FMMC_PLYLOAD") 
						END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - Activate busy spinner using FMMC_PLYLOAD.")
					ENDIF

				ENDIF
				
				SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_DELETE_PEDS)
				g_heistMocap.bCutsceneAtEnd = TRUE
				RETURN FALSE
				
			ELIF (g_heistMocap.skipCutscene)
				// A 'skip cutscene' event has been received, so set the flag that allows the cutscene to cleanup normally
				NETWORK_SET_MOCAP_CUTSCENE_CAN_BE_SKIPPED(TRUE)
				PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - Cutscene skip requested: ", paramCutsceneName)
				
				IF NOT IS_SCREEN_FADED_OUT()
				AND NOT IS_SCREEN_FADING_OUT()
					IF NOT g_heistMocap.bFadedToBlack
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - Doing screen fade out (500).")
						DO_SCREEN_FADE_OUT(500)
						g_heistMocap.bFadedToBlack = TRUE
					ELSE
						PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - ERROR! bFadedToBlack = TRUE but scene is not faded!")
						DO_SCREEN_FADE_OUT(0)
					ENDIF
				ENDIF
				
			// KGM 7/11/14: This acts on the 'skip cutscene' button being pressed by finding out if all players are playing the cutscene
			ELIF (paramIsSkippable)
				// If the cutscene is allowed to be skipped, process the skip checks
				
				// KGM 7/11/14 [BUG 2110075]: Part of the new skip cutscene functionality - when all particpants reply with 'playing cutscene' it'll get skipped.
				// NOTE: Only the Leader will update this counter. On 'skip button' being pressed the counter is cleared and an 'are you playing cutscene' event will get sent
				//			to all players which they will only reply to if they are 'playing' when the event is received, otherwise the event is ignored, so the cutscene
				//			will only get skipped if all players are 'playing' it when the event is sent.
				IF (Are_All_Players_Playing_Cutscene(paramCutsceneParticipants))
					// All players are playing the cutscene, so the skip request can be processed
					#IF IS_DEBUG_BUILD
						IF (GET_COMMANDLINE_PARAM_EXISTS("sc_HeistCutSkippable"))
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - All Players have replied to Leader to say 'playing cutscene'. Allowing skip. (", g_heistMocap.numPlayingCutscene, " players)")
						ENDIF
					#ENDIF
			
					Broadcast_Skip_Heist_Cutscene()
				ENDIF

				// KGM 7/10/14 [BUG 2054748]: Uses the Heist Specific button check. The standard one returns FALSE if pause menu active - heists have pause menu active behind-the-scenes
				// KGM 7/11/14 [BUG 2110075]: The way cutscenes skip will change. When Leader presses skip I send an event to find out how many players are on the 'playing' stage. If
				//					all replay 'playing' then I'll broadcast the 'skip cutscene' event, otherwise it'll get ignored allowing the Leader to try again later.
				IF (Is_Heist_Cutscene_Skip_Button_Just_Pressed())
					#IF IS_DEBUG_BUILD
						IF (GET_COMMANDLINE_PARAM_EXISTS("sc_HeistCutSkippable"))
							PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_PLAYING_CUTSCENE - Cutscene Skip may have been allowed because of -sc_HeistCutSkippable CommandLine. Are all players 'playing' cutscene?")
						ENDIF
					#ENDIF
			
					Broadcast_Check_If_Playing_Heist_Cutscene()
				ENDIF
			ENDIF
			
			RETURN FALSE
			
		CASE HMS_STAGE_DELETE_PEDS
			#IF IS_DEBUG_BUILD
				g_DEBUG_F9_Heist_Cutscene_Stage = "Heist Cut: Delete Peds"
				PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_DELETE_PEDS - Deleting peds for Cutscene: ", paramCutsceneName)
			#ENDIF
			
			Destroy_Heist_Cutscene_Peds()
			Destroy_Heist_Cutscene_Doors()
			Set_New_Heist_Strand_Cooldown_Period()
			
			IF g_heistMocap.objOfficeChair != NULL
				IF DOES_ENTITY_EXIST(g_heistMocap.objOfficeChair)
					PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_DELETE_PEDS - Setting prop: PROP_OFF_CHAIR_01 to VISIBLE.")
					SET_ENTITY_VISIBLE(g_heistMocap.objOfficeChair, TRUE)
				ENDIF
			ELSE
				PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_DELETE_PEDS - Could not locate prop: PROP_OFF_CHAIR_01 in area, not setting to visible...")
			ENDIF
			
			IF NOT paramIsTutorialCutscene
				PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_DELETE_PEDS - paramIsTutorialCutscene = FALSE, don't defer cutscene cleanup to apartment exit.")
				PRINTLN("[TS] [MSRAND] - NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)")
				NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
				CLEANUP_MP_CUTSCENE(FALSE, FALSE)
			ENDIF
			
			// Fix for B*: 2086334. Setting the players to invis AFTER cleanup MP cutscene.
			HIDE_ALL_OTHER_PLAYERS_FOR_HEIST_INTRO()
			SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
			
			#IF IS_DEBUG_BUILD
				DEBUG_Clear_Cutscene_Skip_Help()
			#ENDIF
			Clear_Leader_Skip_Cutscene_Help()
			
			IF NOT HEIST_IS_NETWORK_VOICE_ACTIVE()
				PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_DELETE_PEDS - re-enabling VOIP.")
				HEIST_SET_NETWORK_VOICE_ACTIVE(TRUE)
			ENDIF
			
			SET_HEIST_TUT_CUTS_MIDPOINT_BOARD_STATE(HEIST_TUT_CUTS_NONE)
			
			CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimFirst, FALSE)
			CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimSecond, FALSE)
			CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimThird, FALSE)
			
			SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_WAIT_FOR_FADE_IN)
			
			RETURN FALSE
			
		CASE HMS_STAGE_WAIT_FOR_FADE_IN
			#IF IS_DEBUG_BUILD
				g_DEBUG_F9_Heist_Cutscene_Stage = "Heist Cut: Fading In"
			#ENDIF
			
			HIDE_ALL_OTHER_PLAYERS_FOR_HEIST_INTRO()
			SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())

			IF NOT IS_THIS_CUTSCENE_TUTORIAL_START(paramCutsceneName)
				PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_WAIT_FOR_FADE_IN - NOT tutorial intro cutscene, checking for unfade.")
				IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
				AND NOT IS_SCREEN_FADED_IN()
				AND NOT IS_SCREEN_FADING_IN()
					PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_WAIT_FOR_FADE_IN - Doing screen fade in (500).")
					DO_SCREEN_FADE_IN(500)
				ENDIF	
			ELSE
				PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_WAIT_FOR_FADE_IN - IS_THIS_CUTSCENE_TUTORIAL_START() = TRUE, Skip fade in.")
			ENDIF
			
			SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_INACTIVE)
			SET_HEIST_ENTER_CUTS_SUB_STATE(HMS_SUB_STAGE_INACTIVE)
			PRINTLN(".KGM [Heist][Cutscene] - HMS_STAGE_WAIT_FOR_FADE_IN - Heist Cutscene Sequence Ended: ", paramCutsceneName)
			#IF IS_DEBUG_BUILD
				g_DEBUG_F9_Heist_Cutscene_Stage = ""
			#ENDIF
			
			g_heistMocap.bFadedToBlack = FALSE
			g_heistMocap.hasSafetyTimedOut = FALSE
			g_heistMocap.hasEstTimedOut = FALSE
			
			IF NOT HEIST_IS_NETWORK_VOICE_ACTIVE()
				HEIST_SET_NETWORK_VOICE_ACTIVE(TRUE)
			ENDIF
			
			RETURN TRUE
			
	ENDSWITCH
	
	PRINTLN(".KGM [Heist][Cutscene] Maintain_Net_Heist_Cutscenes() - ERROR: Unknown Stage")
	SCRIPT_ASSERT("Maintain_Net_Heist_Cutscenes() - Unknown Heist Cutscene Control stage. Add to SWITCH statement. Tell Alastair (or Keith)")
	RETURN FALSE
	
ENDFUNC


/// PURPOSE:
///    For bail-out, force a cutscene cleanup in 1 frame.
/// PARAMS:
///    paramCutsceneName - TL23 name of cutscene.
PROC CLEAN_HEIST_NET_CUTSCENE_IMMEDIATELY()

	IF GET_HEIST_ENTER_CUTS_STATE() > HMS_STAGE_INACTIVE

		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [Heist][Cutscene] - CLEAN_HEIST_NET_CUTSCENE_IMMEDIATELY - Running independent cleanup for any active heist planning cutscene")
		#ENDIF
		
		Destroy_Heist_Cutscene_Peds()
		Destroy_Heist_Cutscene_Doors()
		Set_New_Heist_Strand_Cooldown_Period()
		
		IF g_heistMocap.objOfficeChair != NULL
		AND DOES_ENTITY_EXIST(g_heistMocap.objOfficeChair)
			PRINTLN(".KGM [Heist][Cutscene] - CLEAN_HEIST_NET_CUTSCENE_IMMEDIATELY - Setting prop: PROP_OFF_CHAIR_01 to VISIBLE.")
			SET_ENTITY_VISIBLE(g_heistMocap.objOfficeChair, TRUE)
		ELSE
			PRINTLN(".KGM [Heist][Cutscene] - CLEAN_HEIST_NET_CUTSCENE_IMMEDIATELY - Could not locate prop: PROP_OFF_CHAIR_01 in cutscene data, not setting to visible...")
		ENDIF

		PRINTLN("[TS] [MSRAND] - NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)")
		NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
		CLEANUP_MP_CUTSCENE(FALSE, FALSE)

		SET_HEIST_ENTER_CUTS_STATE(HMS_STAGE_INACTIVE)
		SET_HEIST_ENTER_CUTS_SUB_STATE(HMS_SUB_STAGE_INACTIVE)
				
		CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimFirst, FALSE)
		CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimSecond, FALSE)
		CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimThird, FALSE)
		
		IF NOT HEIST_IS_NETWORK_VOICE_ACTIVE()
			PRINTLN(".KGM [Heist][Cutscene] - CLEAN_HEIST_NET_CUTSCENE_IMMEDIATELY - re-enable VOIP.")
			HEIST_SET_NETWORK_VOICE_ACTIVE(TRUE)
		ENDIF
		
		IF g_heistMocap.bFadedToBlack
			IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
			AND NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				PRINTLN(".KGM [Heist][Cutscene] - CLEAN_HEIST_NET_CUTSCENE_IMMEDIATELY - Fading screen back into game from black.")
				DO_SCREEN_FADE_IN(500)
			ENDIF	
			g_heistMocap.bFadedToBlack = FALSE
		ENDIF
				
		g_heistMocap.hasSafetyTimedOut = FALSE
		g_heistMocap.hasEstTimedOut = FALSE
		
		// Mocap Finished, so clear out the variables
		g_heistPrePlanningMocap.mocapRequested		= FALSE
		g_heistPrePlanningMocap.playMocap			= FALSE
		g_heistPrePlanningMocap.bCutscenePropsInit	= FALSE
		g_heistPrePlanningMocap.finaleRCID			= ""
		
		#IF IS_DEBUG_BUILD
			g_DEBUG_F9_Heist_Cutscene_Stage = ""
			DEBUG_Clear_Cutscene_Skip_Help()
		#ENDIF
		Clear_Leader_Skip_Cutscene_Help()
		
	ENDIF

ENDPROC






		// FEATURE_HEIST_PLANNING


