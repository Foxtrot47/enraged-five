///NET_BOUNTY
/// CONOR - 10/9/2012
/// Bounty functionality

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
USING "script_player.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
//USING "net_blips.sch"  
USING "net_cutscene.sch"
USING "fm_unlocks_header_public.sch"
USING "net_comms_txtmsg.sch"
USING "commands_socialclub.sch"
USING "net_mission.sch"
USING "net_transition_sessions.sch"
USING "achievement_public.sch"
USING "net_comms_stored_public.sch"
USING "net_big_message.sch"

//USING "net_mission_trigger_public.sch"

//CONST_INT BB_BS_REGISTED_CONTEXT				0
//CONST_INT BB_BS_STARTED_BOARD					1
//CONST_INT BB_BS_SETUP_MENU						2
//CONST_INT BB_BS_AT_BOARD						3
//CONST_INT BB_BS_CONFIRM_SET_PROMPT				4
//CONST_INT BB_BS_SET_BOUNTY						5
//CONST_INT BB_BS_TOOK_AWAY_CONTROL				11 
//CONST_INT BB_BS_GIVEN_NOT_UNLOCKED_HELP			12
//CONST_INT BB_BS_ADDED_PED_TO_CONVERSATION		13
//CONST_INT BB_BS_CALLED_CONTACT					14
//CONST_INT BB_BS_START_WAIT_FOR_PHONECALL		15
//CONST_INT BB_BS_PHONECALL_STARTED				16
//CONST_INT BB_BS_CALL_CONNECTED					18
//CONST_INT BB_BS_STARTED_DRAWING_MENU			19

//CONST_INT BB_BUTTON_BS_PRESSED_UP		0
//CONST_INT BB_BUTTON_BS_PRESSED_DOWN		1
//CONST_INT BB_BUTTON_BS_PRESSED_LEFT		2
//CONST_INT BB_BUTTON_BS_PRESSED_RIGHT	3
//CONST_INT BB_BUTTON_BS_PRESSED_ACCEPT	4
//CONST_INT BB_BUTTON_BS_PRESSED_CANCEL	5

//CONST_INT BB_STAGE_WAIT				0
//CONST_INT BB_STAGE_FIRST_MENU		1
//CONST_INT BB_STAGE_VIEWING_BOARD	2
//CONST_INT BB_STAGE_EXIT				3
//
//CONST_INT BB_NUM_DIF_BOARDS	9

CONST_INT MP_BOUNTY_NUM_AMOUNTS	5

//CONST_INT BOUNTY_ADMIN_PRICE				1000

CONST_INT BOUNTY_MAX_VALUE		10000

//PROC SET_BB_STAGE(BOUNTY_STRUCT &Bounty,INT iStage)
//	#IF IS_DEBUG_BUILD
//	SWITCH istage
//		CASE BB_STAGE_WAIT
//			NET_PRINT("SET_BB_STAGE = BB_STAGE_WAIT") NET_NL()
//		BREAK
//		
//		CASE BB_STAGE_FIRST_MENU
//			NET_PRINT("SET_BB_STAGE = BB_STAGE_FIRST_MENU") NET_NL()
//		BREAK
//		
//		CASE BB_STAGE_VIEWING_BOARD
//			NET_PRINT("SET_BB_STAGE = BB_STAGE_VIEWING_BOARD") NET_NL()
//		BREAK
//		
//		CASE BB_STAGE_EXIT
//			NET_PRINT("SET_BB_STAGE = BB_STAGE_EXIT") NET_NL()
//		BREAK
//	ENDSWITCH
//	#ENDIF
//	Bounty.iStage = iStage
//ENDPROC

FUNC BOOL IS_BOUNTY_UNLOCKED_FOR_LOCAL_PLAYER()
	IF IS_FM_TYPE_UNLOCKED(FMMC_TYPE_BOUNTIES)
	#IF IS_DEBUG_BUILD
	OR MPGlobalsAmbience.global_Bounty.bDebugUnlocked
	#ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_BOUNTY_UNLOCKED_FOR_PLAYER(PLAYER_INDEX thePlayer)
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(thePlayer)].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bUnlockedBounty)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

CONST_INT SPECIAL_BOUNTY_MC		1234

FUNC INT BB_GET_MONEY_FOR_POS(INT iPos)
	SWITCH iPos
		CASE 0 RETURN g_sMPTunables.ibountyawardlev1	BREAK
		CASE 1 RETURN g_sMPTunables.ibountyawardlev2 	BREAK
		CASE 2 RETURN g_sMPTunables.ibountyawardlev3 	BREAK
		CASE 3 RETURN g_sMPTunables.ibountyawardlev4 	BREAK
		CASE 4 RETURN g_sMPTunables.ibountyawardlev5	BREAK
		//CASE 5 RETURN 15500 BREAK
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC INT GET_SPECIAL_BOUNTY_REWARD_VALUE(INT iBountyValue, BOOL bXp)
	IF iBountyValue = SPECIAL_BOUNTY_MC
		IF bXP
			RETURN 2000
		ELSE
			RETURN 1000
		ENDIF
	ENDIF
	RETURN -1
ENDFUNC

FUNC BOOL IS_BOUNTY_SET_ON_PLAYER(PLAYER_INDEX playerID)
	IF GlobalServerBD_FM.currentBounties[NATIVE_TO_INT(playerID)].bTargeted = TRUE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC INT GET_BOUNTY_AMOUNT(PLAYER_INDEX playerID)
	IF MPGlobalsAmbience.playerBounty.iValue[NATIVE_TO_INT(playerID)] != 0
		RETURN MPGlobalsAmbience.playerBounty.iValue[NATIVE_TO_INT(playerID)]
	ENDIF
	RETURN GlobalServerBD_FM.currentBounties[NATIVE_TO_INT(playerID)].iAmount
ENDFUNC

PROC CLEANUP_BOUNTY_TEXT(INT iIndex)
	MPGlobalsAmbience.BountyTexts.details[iIndex].iValue = -1
	MPGlobalsAmbience.BountyTexts.details[iIndex].player1Name = ""
	MPGlobalsAmbience.BountyTexts.details[iIndex].player2Name = ""
	MPGlobalsAmbience.BountyTexts.details[iIndex].textLabel = ""
	MPGlobalsAmbience.BountyTexts.iBS = 0
	MPGlobalsAmbience.BountyTexts.details[iIndex].charEnum = NO_CHARACTER
	MPGlobalsAmbience.BountyTexts.details[iIndex].iType = 0
	RESET_NET_TIMER(MPGlobalsAmbience.BountyTexts.details[iIndex].timeDelay)
	MPGlobalsAmbience.BountyTexts.iSlotBeingProcessed = 0
	PRINTLN("CLEANUP_BOUNTY_TEXT: called ")
ENDPROC

//PURPOSE: Sets players with a Bounty to allow missiles to lock onto them
PROC ENABLE_MISSILE_LOCK_IF_PLAYER_HAS_BOUNTY(PLAYER_INDEX thisPlayer)
	IF IS_BOUNTY_SET_ON_PLAYER(thisPlayer)
		SET_PED_RESET_FLAG(GET_PLAYER_PED(thisPlayer), PRF_AllowHomingMissileLockOnInVehicle, TRUE)
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC PRINT_BOUNTY_TEXT_DETAILS(INT iSlot)
	PRINTLN("PRINT_BOUNTY_TEXT_DETAILS: SLOT: ",iSlot)
	PRINTLN("MPGlobalsAmbience.BountyTexts.details[",iSlot,"].charEnum: ",MPGlobalsAmbience.BountyTexts.details[iSlot].charEnum)
	PRINTLN("MPGlobalsAmbience.BountyTexts.details[",iSlot,"].textLabel: ",MPGlobalsAmbience.BountyTexts.details[iSlot].textLabel)
	PRINTLN("MPGlobalsAmbience.BountyTexts.details[",iSlot,"].iValue: ",MPGlobalsAmbience.BountyTexts.details[iSlot].iValue)
	PRINTLN("MPGlobalsAmbience.BountyTexts.details[",iSlot,"].player1Name: ",MPGlobalsAmbience.BountyTexts.details[iSlot].player1Name)
	PRINTLN("MPGlobalsAmbience.BountyTexts.details[",iSlot,"].player2Name: ",MPGlobalsAmbience.BountyTexts.details[iSlot].player2Name)
	PRINTLN("MPGlobalsAmbience.BountyTexts.details[",iSlot,"].iDelay: ",MPGlobalsAmbience.BountyTexts.details[iSlot].iDelay)
	PRINTLN("MPGlobalsAmbience.BountyTexts.details[",iSlot,"].iType: ",MPGlobalsAmbience.BountyTexts.details[iSlot].iType)
ENDPROC
#ENDIF

PROC ADD_ITEM_TO_BOUNTY_TEXTS(enumCharacterList charEnum,STRING textLabel,INT iValue,STRING playerName1, STRING playerName2,INT iDelay, INT iType)
	INT i
	INT iIndexToReplace = -1
	INT iFreeIndex = -1
	INT iFillIndex = -1
	BOOL bExit
	REPEAT MAX_BOUNTY_TEXTS i
		//IF MPGlobalsAmbience.BountyTexts.iSlotBeingProcessed != i
			IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.BountyTexts.details[i].timeDelay)
				IF NOT ARE_STRINGS_EQUAL(MPGlobalsAmbience.BountyTexts.details[i].textLabel,"FM_TXT_BNTY0")
					IF ARE_STRINGS_EQUAL(playerName1,MPGlobalsAmbience.BountyTexts.details[i].player1Name)
						iIndexToReplace = i
						bExit = TRUE
					ENDIF
				ENDIF
			ENDIF
			IF NOT HAS_NET_TIMER_STARTED(MPGlobalsAmbience.BountyTexts.details[i].timeDelay)
				IF iFreeIndex = -1
					iFreeIndex = i
				ENDIF
			ENDIF
			IF bExit
				i = MAX_BOUNTY_TEXTS
			ENDIF
		//ENDIF
	ENDREPEAT
	
	iFillIndex = iIndexToReplace
	IF iFillIndex = -1
		iFillIndex = iFreeIndex
	ENDIF
	
	IF iFillIndex >= 0
		CLEANUP_BOUNTY_TEXT(iFillIndex)
		MPGlobalsAmbience.BountyTexts.details[iFillIndex].charEnum = charEnum
		MPGlobalsAmbience.BountyTexts.details[iFillIndex].textLabel = textLabel
		MPGlobalsAmbience.BountyTexts.details[iFillIndex].iValue = iValue
		MPGlobalsAmbience.BountyTexts.details[iFillIndex].player1Name = playerName1
		MPGlobalsAmbience.BountyTexts.details[iFillIndex].player2Name = playerName2
		START_NET_TIMER(MPGlobalsAmbience.BountyTexts.details[iFillIndex].timeDelay,TRUE)
		MPGlobalsAmbience.BountyTexts.details[iFillIndex].iDelay = iDelay
		MPGlobalsAmbience.BountyTexts.details[iFillIndex].iType = iType
		#IF IS_DEBUG_BUILD
			PRINT_BOUNTY_TEXT_DETAILS(iFillIndex)
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		PRINTLN("ADD_ITEM_TO_BOUNTY_TEXTS: no available slot!! See conor")
		SCRIPT_ASSERT("ADD_ITEM_TO_BOUNTY_TEXTS: no available slot!! See conor")
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_PLAYER_VIEW_BOUNTY_REMINDER_NOW()
	IF (g_sTriggerMP.mtState = NO_CURRENT_MP_TRIGGER_REQUEST)
		RETURN FALSE
	ENDIF
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType != FMMC_TYPE_INVALID
		RETURN FALSE	
	ENDIF
	
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bLeaderboardCamRenderingOrInterping
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ACTIVE_IN_CORONA()
		RETURN FALSE
	ENDIF
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADED_IN()
		RETURN FALSE
	ENDIF
	
	IF IS_CELLPHONE_DISABLED()
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_FM_MISSION_LAUNCH_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	IF MPGlobals.g_missionSummary.bSummaryScreenDisplayed
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	IF g_bBrowserVisible 	//(Checks if the internet active)
		RETURN FALSE
	ENDIF
	
	IF SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
		RETURN FALSE
	ENDIF
	
	IF IS_TRANSITION_SESSION_LAUNCHING()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC



PROC PAUSE_BOUNTY_TIMER()
	IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.playerBounty.bountyTimer)
		MPGlobalsAmbience.playerBounty.iTimeSoFar += GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MPGlobalsAmbience.playerBounty.bountyTimer,TRUE)
		PRINTLN("PAUSE_BOUNTY_TIMER: pausing local player bounty timer at: ",MPGlobalsAmbience.playerBounty.iTimeSoFar , " ms" )
		#IF IS_DEBUG_BUILD
		IF NETWORK_IS_IN_TUTORIAL_SESSION()
			PRINTLN("PAUSE_BOUNTY_TIMER: paused NETWORK_IS_IN_TUTORIAL_SESSION")
		ENDIF
		IF MPGlobalsAmbience.playerBounty.bPausePlayerBountyThisFrame
			PRINTLN("PAUSE_BOUNTY_TIMER: paused MPGlobalsAmbience.playerBounty.bPausePlayerBountyThisFrame")
		ENDIF 
		IF IS_MP_PASSIVE_MODE_ENABLED()
			PRINTLN("PAUSE_BOUNTY_TIMER: paused IS_MP_PASSIVE_MODE_ENABLED")
		ENDIF
		IF IS_PLAYER_ON_ANY_FM_MISSION2(PLAYER_ID())
			PRINTLN("PAUSE_BOUNTY_TIMER: paused IS_PLAYER_ON_ANY_FM_MISSION2")
		ENDIF
		IF IS_PLAYER_IN_CORONA()
			PRINTLN("PAUSE_BOUNTY_TIMER: paused IS_PLAYER_IN_CORONA")
		ENDIF
		IF IS_TRANSITION_ACTIVE()
			PRINTLN("PAUSE_BOUNTY_TIMER: paused IS_TRANSITION_ACTIVE")
		ENDIF
		IF IS_TRANSITION_SESSION_RESTARTING()
			PRINTLN("PAUSE_BOUNTY_TIMER: paused IS_TRANSITION_SESSION_RESTARTING")
		ENDIF
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty > 0
			PRINTLN("PAUSE_BOUNTY_TIMER: paused player is in a property")
		ENDIF
		IF IS_BIT_SET(MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset[GET_WEAPON_BITSET(ENUM_TO_INT(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE))][NATIVE_TO_INT(PLAYER_ID())], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE)))
			PRINTLN("PAUSE_BOUNTY_TIMER: paused bounty passive flag set")
		ENDIF
		#ENDIF
		RESET_NET_TIMER(MPGlobalsAmbience.playerBounty.bountyTimer)
		SET_BIT(MPGlobalsAmbience.playerBounty.iControlBS,BB_CONTROL_BS_PAUSED_TIMER)
		CLEAR_BIT(MPGlobalsAmbience.playerBounty.iControlBS,BB_CONTROL_BS_STARTED_TIMER)
	ENDIF
ENDPROC

PROC RESET_BOUNTY_TIMER()
	CLEAR_BIT(MPGlobalsAmbience.playerBounty.iControlBS,BB_CONTROL_BS_PAUSED_TIMER)
	CLEAR_BIT(MPGlobalsAmbience.playerBounty.iControlBS,BB_CONTROL_BS_STARTED_TIMER)
	RESET_NET_TIMER(MPGlobalsAmbience.playerBounty.bountyTimer)
	//PRINTLN("RESET_BOUNTY_TIMER- Called this frame")
ENDPROC

PROC RESUME_BOUNTY_TIMER(BOOL bReinit)
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(MPGlobalsAmbience.playerBounty.iControlBS,BB_CONTROL_BS_PAUSED_TIMER)
	OR NOT IS_BIT_SET(MPGlobalsAmbience.playerBounty.iControlBS,BB_CONTROL_BS_STARTED_TIMER)
		PRINTLN("RESUME_BOUNTY_TIMER: un-pausing local player bounty timer")
	ENDIF
	#ENDIF
	CLEAR_BIT(MPGlobalsAmbience.playerBounty.iControlBS,BB_CONTROL_BS_PAUSED_TIMER)
	SET_BIT(MPGlobalsAmbience.playerBounty.iControlBS,BB_CONTROL_BS_STARTED_TIMER)
	IF bReinit
		REINIT_NET_TIMER(MPGlobalsAmbience.playerBounty.bountyTimer,TRUE)
	ELSE
		START_NET_TIMER(MPGlobalsAmbience.playerBounty.bountyTimer,TRUE)
	ENDIF
ENDPROC

PROC PAUSE_BOUNTY_TIMER_THIS_FRAME()
	MPGlobalsAmbience.playerBounty.bPausePlayerBountyNextFrame = TRUE
ENDPROC

PROC HIDE_BOUNTY_BLIP_THIS_FRAME()
	MPGlobalsAmbience.playerBounty.bHidePlayerBountyBlipNextFrame = TRUE
ENDPROC

//PROC SEND_BOUNTY_INBOX_MESSAGE_TO_PLACER()
//	SC_INBOX_SEND_BOUNTY_TO_RECIP_LIST 
//	SC_INBOX_GET_BOUNTY_DATA_AT_INDEX
//ENDPROC

FUNC BOOL HAS_PLAYER_REACHED_LIMIT_OF_BOUNTIES_TODAY(INT &iMinTimeLeft)
	INT i
	iMinTimeLeft = BB_BOUNTY_TIME_LIMIT
	INT iTemp
	REPEAT MAX_BOUNTIES_PER_DAY i
		IF IS_GAMER_HANDLE_VALID(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.gh_BountySet[i])
			iTemp = GET_CLOUD_TIME_AS_INT() - g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iPosXTimeSet[i] 
			iTemp = iTemp*1000
			iTemp = BB_BOUNTY_TIME_LIMIT - iTemp
			IF iTemp < iMinTimeLeft
				iMinTimeLeft = iTemp
			ENDIF
		ELSE
			RETURN FALSE	
		ENDIF
	ENDREPEAT
	PRINTLN("HAS_PLAYER_REACHED_LIMIT_OF_BOUNTIES_TODAY: True!")
	RETURN TRUE
ENDFUNC

PROC CLEAR_STORED_BOUNTY_ARRAY_SLOT(INT iSlot)
	#IF IS_DEBUG_BUILD
	PRINTLN("CLEAR_STORED_BOUNTY_ARRAY_SLOT -- Clearing slot #", iSlot)
	DEBUG_PRINT_GAMER_HANDLE(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.gh_BountySet[iSlot])
	#ENDIF
	CLEAR_GAMER_HANDLE_STRUCT(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.gh_BountySet[iSlot])
	g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iPosXTimeSet[iSlot] = 0 
ENDPROC

#IF IS_DEBUG_BUILD
PROC CLEAR_DAILY_BOUNTY_DATA()
	INT i
	REPEAT MAX_BOUNTIES_PER_DAY i
		CLEAR_STORED_BOUNTY_ARRAY_SLOT(i)
	ENDREPEAT
ENDPROC
#ENDIF

FUNC BOOL STORE_BOUNTY_ARRAY_SLOT(GAMER_HANDLE &gamerHandle)
	
	#IF IS_DEBUG_BUILD
	PRINTLN("STORE_BOUNTY_ARRAY_SLOT -- attempting to store gamer")
	DEBUG_PRINT_GAMER_HANDLE(gamerHandle)
	PLAYER_INDEX thePlayer
	IF IS_GAMER_HANDLE_VALID(gamerHandle)
		thePlayer = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(gamerHandle)
		IF thePlayer != INVALID_PLAYER_INDEX()
			PRINTLN("STORE_BOUNTY_ARRAY_SLOT -- attempting to store gamer NAME = ",NETWORK_PLAYER_GET_NAME(thePlayer))
		ENDIF
	ENDIF
	#ENDIF
	INT i
	REPEAT MAX_BOUNTIES_PER_DAY i
		IF IS_GAMER_HANDLE_VALID(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.gh_BountySet[i])
			IF NETWORK_ARE_HANDLES_THE_SAME(gamerHandle,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.gh_BountySet[i])
				PRINTLN("STORE_BOUNTY_ARRAY_SLOT -- (GAMER ALREADY STORED???) gamer stored in slot #", i)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.gh_BountySet[i] = gamerHandle
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iPosXTimeSet[i] = GET_CLOUD_TIME_AS_INT()
				PRINTLN("STORE_BOUNTY_ARRAY_SLOT -- at time: ",g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iPosXTimeSet[i])
				RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("STORE_BOUNTY_ARRAY_SLOT -- gamer stored in slot #", i)
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.gh_BountySet[i] = gamerHandle
			g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iPosXTimeSet[i] = GET_CLOUD_TIME_AS_INT()	
			PRINTLN("STORE_BOUNTY_ARRAY_SLOT -- at time: ",g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iPosXTimeSet[i])
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_PUT_BOUNTY_ON_THIS_PLAYER_ALREADY(GAMER_HANDLE &gamerHandle, INT &iTimeLeft)
	INT i
	REPEAT MAX_BOUNTIES_PER_DAY i
		IF IS_GAMER_HANDLE_VALID(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.gh_BountySet[i])
			IF NETWORK_ARE_HANDLES_THE_SAME(gamerHandle,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.gh_BountySet[i])
				PRINTLN("HAS_PLAYER_PUT_BOUNTY_ON_THIS_PLAYER_ALREADY-- (GAMER ALREADY STORED) gamer stored in slot #", i)
				iTimeLeft = GET_CLOUD_TIME_AS_INT() - g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iPosXTimeSet[i] 
				iTimeLeft = iTimeLeft*1000
				iTimeLeft = BB_BOUNTY_TIME_LIMIT - iTimeLeft
				PRINTLN("HAS_PLAYER_PUT_BOUNTY_ON_THIS_PLAYER_ALREADY-- time left = ", iTimeLeft)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_BOUNTY_OVER_SAVE_CHECKS()
	IF IS_BIT_SET(MPGlobalsAmbience.BountySaveCheck.iBS,BOUNTY_SAVE_CHECK_BS_TRIGGERED)
		IF NOT HAS_ANY_SAVE_BEEN_REQUESTED()
		AND NOT IS_SAVE_IN_PROGRESS()
			IF NOT IS_SAVING_HAVING_TROUBLE()
				PRINTLN("MAINTAIN_BOUNTY_OVER_SAVE_CHECKS: save successfull triggering over")
				BROADCAST_BOUNTY_OVER(ALL_PLAYERS(),
									MPGlobalsAmbience.BountySaveCheck.killer,
									MPGlobalsAmbience.BountySaveCheck.bountyPlacer,
									MPGlobalsAmbience.BountySaveCheck.iResultBS,
									MPGlobalsAmbience.BountySaveCheck.iTimeSinceBountyStart,
									MPGlobalsAmbience.BountySaveCheck.iValue,FALSE)
				IF IS_BIT_SET(MPGlobalsAmbience.BountySaveCheck.iBS,BOUNTY_SAVE_CHECK_BS_SURVIVED)
					PRINTLN("Awarding bounty survival")
					AWARD_ACHIEVEMENT(ACH39)
				ENDIF
				CLEAR_BIT(MPGlobalsAmbience.BountySaveCheck.iBS,BOUNTY_SAVE_CHECK_BS_SURVIVED)
				CLEAR_BIT(MPGlobalsAmbience.BountySaveCheck.iBS,BOUNTY_SAVE_CHECK_BS_TRIGGERED)
			ELSE
				PRINTLN("MAINTAIN_BOUNTY_OVER_SAVE_CHECKS: save is failing not completing bounty")
				CLEAR_BIT(MPGlobalsAmbience.BountySaveCheck.iBS,BOUNTY_SAVE_CHECK_BS_TRIGGERED)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_BOUNTY_TEXTS_CALLS()
	IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.BountyCalls.Timer)
		IF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.BountyCalls.Timer,MPGlobalsAmbience.BountyCalls.iDelay,TRUE)
			Store_MP_General_Communication("LESTER","3",CHAR_LESTER,"CT_AUD",MPGlobalsAmbience.BountyCalls.textLabel)
			PRINTLN("MAINTAIN_BOUNTY_TEXTS_CALLS: triggering lester phone call for bounty")
			MPGlobalsAmbience.BountyCalls.textLabel = ""
			
			RESET_NET_TIMER(MPGlobalsAmbience.BountyCalls.Timer)
		ENDIF
	ENDIF
	IF NOT IS_BIT_SET(MPGlobalsAmbience.BountyTexts.iBS,BT_BS_PROCESSING_TEXT_MES)
		//IF MPGlobalsAmbience.BountyTexts.details[MPGlobalsAmbience.BountyTexts.iSlowCounter].iValue != -1
		IF NOT Is_Any_MP_Comms_Active_Or_Pending()
			IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.BountyTexts.details[MPGlobalsAmbience.BountyTexts.iSlowCounter].timeDelay)
				IF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.BountyTexts.details[MPGlobalsAmbience.BountyTexts.iSlowCounter].timeDelay,
											MPGlobalsAmbience.BountyTexts.details[MPGlobalsAmbience.BountyTexts.iSlowCounter].iDelay,TRUE)
					
					MPGlobalsAmbience.BountyTexts.iSlotBeingProcessed = MPGlobalsAmbience.BountyTexts.iSlowCounter
					SET_BIT(MPGlobalsAmbience.BountyTexts.iBS,BT_BS_PROCESSING_TEXT_MES)
				ENDIF
			ELSE
		
				MPGlobalsAmbience.BountyTexts.iSlowCounter++
				IF MPGlobalsAmbience.BountyTexts.iSlowCounter >= MAX_BOUNTY_TEXTS
					MPGlobalsAmbience.BountyTexts.iSlowCounter = 0
				ENDIF
			ENDIF
		ENDIF
	ELSE
		INT iIndex = MPGlobalsAmbience.BountyTexts.iSlotBeingProcessed
		IF NOT Is_Any_MP_Comms_Device_Actively_Communicating()
			IF MPGlobalsAmbience.BountyTexts.details[iIndex].iType = 0
				IF ARE_STRINGS_EQUAL(MPGlobalsAmbience.BountyTexts.details[iIndex].textLabel,"FM_TXT_BNTY0")
					IF Request_MP_Comms_Txtmsg_With_Components(MPGlobalsAmbience.BountyTexts.details[iIndex].charEnum,
																		MPGlobalsAmbience.BountyTexts.details[iIndex].textLabel,
																		MPGlobalsAmbience.BountyTexts.details[iIndex].player1Name,
																		TRUE,
																		MPGlobalsAmbience.BountyTexts.details[iIndex].iValue)
						
						SETUP_NEW_BIG_MESSAGE_WITH_LITERAL_STRING_AND_INT(BIG_MESSAGE_BOUNTY_PLACED,
																	MPGlobalsAmbience.BountyTexts.details[iIndex].iValue,
																	"BNTY_BIG_0", MPGlobalsAmbience.BountyTexts.details[iIndex].player1Name,
																	"BB_KILLSTRIP",HUD_COLOUR_RED)												
						CLEANUP_BOUNTY_TEXT(iIndex)	
					ENDIF
				ELSE
					PRINT_TICKER_WITH_TWO_PLAYER_NAME_STRING_AND_INT(MPGlobalsAmbience.BountyTexts.details[iIndex].textLabel,
																		MPGlobalsAmbience.BountyTexts.details[iIndex].player1Name,
																		MPGlobalsAmbience.BountyTexts.details[iIndex].player2Name,
																		MPGlobalsAmbience.BountyTexts.details[iIndex].iValue)
					CLEANUP_BOUNTY_TEXT(iIndex)	
				ENDIF
			ELSE
				IF ARE_STRINGS_EQUAL(MPGlobalsAmbience.BountyTexts.details[iIndex].textLabel,"TXT_BNTY_NPCa")
					IF Request_MP_Comms_Txtmsg_With_Components(MPGlobalsAmbience.BountyTexts.details[iIndex].charEnum,
																		MPGlobalsAmbience.BountyTexts.details[iIndex].textLabel,
																		MPGlobalsAmbience.BountyTexts.details[iIndex].player1Name,
																		TRUE,
																		MPGlobalsAmbience.BountyTexts.details[iIndex].iValue)
						
						SETUP_NEW_BIG_MESSAGE_WITH_INT_AND_STRING(BIG_MESSAGE_BOUNTY_PLACED, MPGlobalsAmbience.BountyTexts.details[iIndex].iValue, "BNTY_BIG_1", "BB_KILLSTRIP",HUD_COLOUR_RED)									
						CLEANUP_BOUNTY_TEXT(MPGlobalsAmbience.BountyTexts.iSlotBeingProcessed)	
					ENDIF
				ELSE
					PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_INT(MPGlobalsAmbience.BountyTexts.details[iIndex].textLabel,MPGlobalsAmbience.BountyTexts.details[iIndex].player1Name,MPGlobalsAmbience.BountyTexts.details[iIndex].iValue)
					CLEANUP_BOUNTY_TEXT(MPGlobalsAmbience.BountyTexts.iSlotBeingProcessed)	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(MPGlobalsAmbience.BountyTexts.iBS,BT_BS_SENT_REMINDER_TEXT)
		IF IS_BIT_SET(MPGlobalsAmbience.BountyTexts.iBS,BT_BS_SEND_REMINDER_TEXT)
			IF IS_BOUNTY_UNLOCKED_FOR_LOCAL_PLAYER()
				IF NOT HAS_NET_TIMER_STARTED(MPGlobalsAmbience.BountyTexts.bountyRankupReminder)
					START_NET_TIMER(MPGlobalsAmbience.BountyTexts.bountyRankupReminder,TRUE)
				ELSE
					IF CAN_PLAYER_VIEW_BOUNTY_REMINDER_NOW()
						IF GET_MP_INT_CHARACTER_STAT(MP_STAT_BOUNTPLACED) <1
							Send_MP_Txtmsg_From_CharSheetID(CHAR_LESTER,"BB_REMIND_TXT")
							SET_BIT(MPGlobalsAmbience.BountyTexts.iBS,BT_BS_SENT_REMINDER_TEXT)
						ENDIF
						CLEAR_BIT(MPGlobalsAmbience.BountyTexts.iBS,BT_BS_SEND_REMINDER_TEXT)
					ELSE
						RESET_NET_TIMER(MPGlobalsAmbience.BountyTexts.bountyRankupReminder)
					ENDIF
				ENDIF
			ELSE
				CLEAR_BIT(MPGlobalsAmbience.BountyTexts.iBS,BT_BS_SEND_REMINDER_TEXT)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MAINTAIN_BOUNTY_TIMER()
	//INT iCurrentMin
	IF NETWORK_IS_IN_TUTORIAL_SESSION()
	OR MPGlobalsAmbience.playerBounty.bPausePlayerBountyThisFrame 
	OR IS_MP_PASSIVE_MODE_ENABLED() 
	OR IS_BIT_SET(MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset[GET_WEAPON_BITSET(ENUM_TO_INT(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE))][NATIVE_TO_INT(PLAYER_ID())], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE)))
	OR IS_PLAYER_ON_ANY_FM_MISSION2(PLAYER_ID())
	OR IS_PLAYER_IN_CORONA()
	OR IS_TRANSITION_ACTIVE()
	OR IS_TRANSITION_SESSION_RESTARTING()
	OR IS_TRANSITION_SESSION_LAUNCHING()
	OR GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty > 0
	OR IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
	OR NETWORK_IS_IN_TRANSITION()
	OR (GET_STAT_CHARACTER_TEAM() = TEAM_SCTV OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iSpecInfoBitset, SPEC_INFO_BS_SPECTATOR_CAM_IS_RUNNING)
	OR IS_A_SPECTATOR_CAM_ACTIVE())
	OR IS_CORONA_INITIALISING_A_QUICK_RESTART()
	OR IS_A_STRAND_MISSION_BEING_INITIALISED()
	
		PAUSE_BOUNTY_TIMER()
		#IF IS_DEBUG_BUILD
		IF g_db_OutputBountyDebug
			IF NETWORK_IS_IN_TUTORIAL_SESSION()
				PRINTLN("Pause bounty in tutorial")
			ENDIF
			IF MPGlobalsAmbience.playerBounty.bPausePlayerBountyThisFrame 
				PRINTLN("Pause bounty bPausePlayerBountyThisFrame")
			ENDIF
			IF IS_MP_PASSIVE_MODE_ENABLED()
				PRINTLN("Pause bounty IS_MP_PASSIVE_MODE_ENABLED")
			ENDIF
			IF IS_PLAYER_ON_ANY_FM_MISSION2(PLAYER_ID())
				PRINTLN("Pause bounty IS_PLAYER_ON_ANY_FM_MISSION2")
			ENDIF
			IF IS_PLAYER_IN_CORONA()
				PRINTLN("Pause bounty IS_PLAYER_IN_CORONA")
			ENDIF
			IF IS_TRANSITION_ACTIVE()
				PRINTLN("Pause bounty IS_TRANSITION_ACTIVE")
			ENDIF
			IF IS_TRANSITION_SESSION_RESTARTING()
				PRINTLN("Pause bounty transition session restart")
			ENDIF
			IF IS_TRANSITION_SESSION_LAUNCHING()
				PRINTLN("Pause bounty transition session launch")
			ENDIF
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty > 0
				PRINTLN("Pause bounty in property")
			ENDIF
			IF NETWORK_IS_IN_TRANSITION()
				PRINTLN("Pause bounty in transition")
			ENDIF
			IF IS_BIT_SET(MPGlobalsHud.DisplayInfo.iOverheadEVENTBitset[GET_WEAPON_BITSET(ENUM_TO_INT(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE))][NATIVE_TO_INT(PLAYER_ID())], GET_WEAPON_INDEX_BITSET(ENUM_TO_INT(DISPLAYOVERHEADS_EVENTS_IS_PLAYER_PASSIVE)))
				PRINTLN("Pause bounty passive flag set")
			ENDIF
			IF (GET_STAT_CHARACTER_TEAM() = TEAM_SCTV OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iSpecInfoBitset, SPEC_INFO_BS_SPECTATOR_CAM_IS_RUNNING)
			OR IS_A_SPECTATOR_CAM_ACTIVE())
				PRINTLN("Pause bounty spectator")
			ENDIF
		ENDIF
		#ENDIF
		//Fix for Bug 1790498 - Looks messy, could probably be done much neater.
		IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty > 0
			IF GlobalServerBD_FM.currentBounties[NATIVE_TO_INT(PLAYER_ID())].bTargeted = FALSE
				IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iBountyValue > 0
				AND MPGlobalsAmbience.playerBounty.iBountyOnLocalPlayer = 0	
					IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
					AND NOT MPGlobalsAmbience.playerBounty.bPausePlayerBountyThisFrame 
					AND NOT IS_MP_PASSIVE_MODE_ENABLED()
					AND NOT IS_PLAYER_ON_ANY_FM_MISSION2(PLAYER_ID())
					AND NOT IS_PLAYER_IN_CORONA()
					AND NOT IS_TRANSITION_ACTIVE()
					AND NOT IS_TRANSITION_SESSION_RESTARTING()
					AND NOT IS_TRANSITION_SESSION_LAUNCHING()
					AND NOT NETWORK_IS_IN_TRANSITION()
					AND NOT (GET_STAT_CHARACTER_TEAM() = TEAM_SCTV OR IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iSpecInfoBitset, SPEC_INFO_BS_SPECTATOR_CAM_IS_RUNNING)
					OR IS_A_SPECTATOR_CAM_ACTIVE())
					AND NOT IS_CORONA_INITIALISING_A_QUICK_RESTART()
					AND NOT IS_A_STRAND_MISSION_BEING_INITIALISED()
					
						BROADCAST_SET_BOUNTY_ON_PLAYER(ALL_PLAYERS(),PLAYER_ID(),g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iBountyValue,1,FALSE)
						RESET_BOUNTY_TIMER()
						MPGlobalsAmbience.playerBounty.bBountyFromPreviousSession = TRUE
						PRINTLN("Player has bounty from previous session re-applying - STARTED NEW SESSION IN PROPERTY")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		IF GlobalServerBD_FM.currentBounties[NATIVE_TO_INT(PLAYER_ID())].bTargeted
			IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.playerBounty.bountyTimer)
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iTimePassedSoFar += GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MPGlobalsAmbience.playerBounty.bountyTimer,TRUE)
				//PRINTLN("MAINTAIN_BOUNTY_TIMER: Time passed so far = ", g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iTimePassedSoFar)
				RESUME_BOUNTY_TIMER(TRUE)
//				iCurrentMin = FLOOR(CONVERT_MILLISECONDS_TO_MINUTES(MPGlobalsAmbience.playerBounty.iTimeSoFar + GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(MPGlobalsAmbience.playerBounty.bountyTimer)))
//				IF iCurrentMin > MPGlobalsAmbience.playerBounty.iRewardMinute
//					MPGlobalsAmbience.playerBounty.iRewardMinute = iCurrentMin
//					//SET_NEXT_XP_DESCRIPTION("XPT_BOUNTYSURT1",50)
//					//GIVE_LOCAL_PLAYER_XP("XPT_BOUNTYSURT1",50)
//					//NET_PRINT("Giving player 50xp for surviving bounty for minute") NET_NL()
//				ENDIF
				IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iTimePassedSoFar >= BB_BOUNTY_TIME_LIMIT
					INT iBS
					SET_BIT(iBS,BOUNTY_RESULT_TIME_UP)
					INT iBountyValue = GlobalServerBD_FM.currentBounties[NATIVE_TO_INT(PLAYER_ID())].iAmount
					IF iBountyValue = 0
						iBountyValue = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iBountyValue
					ENDIF
					BROADCAST_BOUNTY_OVER(ALL_PLAYERS(),INVALID_PLAYER_INDEX(),g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.gh_BountyPlacer,iBS,g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iTimePassedSoFar,iBountyValue,TRUE)
					SET_BIT(MPGlobalsAmbience.BountySaveCheck.iBS,BOUNTY_SAVE_CHECK_BS_SURVIVED)
					
					//AWARD_ACHIEVEMENT(ACH39) // run like the wind
					
					//IF NOT MPGlobalsAmbience.playerBounty.bBountyFromPreviousSession
						///player did not have bounty reapplied from another session award run like the wind. 1419514
					//	AWARD_ACHIEVEMENT(ACH39) // run like the wind
					//ENDIF
					
					RESET_BOUNTY_TIMER()
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(MPGlobalsAmbience.playerBounty.iControlBS,BB_CONTROL_BS_STARTED_TIMER)
					RESUME_BOUNTY_TIMER(FALSE)
				ELSE
					IF IS_BIT_SET(MPGlobalsAmbience.playerBounty.iControlBS,BB_CONTROL_BS_PAUSED_TIMER)
						RESUME_BOUNTY_TIMER(FALSE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iBountyValue > 0
			AND MPGlobalsAmbience.playerBounty.iBountyOnLocalPlayer = 0
				IF NOT IS_BIT_SET(MPGlobalsAmbience.playerBounty.iControlBS,BB_CONTROL_BS_TRIGGER_IN_NEW_SES)
					IF IS_NET_PLAYER_OK(PLAYER_ID())
						RESUME_BOUNTY_TIMER(TRUE)
						SET_BIT(MPGlobalsAmbience.playerBounty.iControlBS,BB_CONTROL_BS_TRIGGER_IN_NEW_SES)
						PRINTLN("Starting to trigger bounty from previous session")
					ENDIF
				ELSE
					IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.playerBounty.bountyTimer)
						IF HAS_NET_TIMER_EXPIRED_READ_ONLY(MPGlobalsAmbience.playerBounty.bountyTimer,1000,TRUE)
							BROADCAST_SET_BOUNTY_ON_PLAYER(ALL_PLAYERS(),PLAYER_ID(),g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iBountyValue,1,FALSE)
							RESET_BOUNTY_TIMER()
							MPGlobalsAmbience.playerBounty.bBountyFromPreviousSession = TRUE
							PRINTLN("Player has bounty from previous session re-applying")
						ENDIF
					ELSE
							RESUME_BOUNTY_TIMER(FALSE)
					ENDIF
				ENDIF
			ELSE
				MPGlobalsAmbience.playerBounty.iControlBS = 0
				RESET_BOUNTY_TIMER()
			ENDIF
		ENDIF
		
		MAINTAIN_BOUNTY_TEXTS_CALLS()
	ENDIF
	
	IF IS_GAMER_HANDLE_VALID(g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.gh_BountySet[MPGlobalsAmbience.playerBounty.iSlowLoopMaintainDailyLimit])
		INT iTimeLeft = GET_CLOUD_TIME_AS_INT() - g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedBounty.iPosXTimeSet[MPGlobalsAmbience.playerBounty.iSlowLoopMaintainDailyLimit]
		iTimeLeft = iTimeLeft*1000
		IF iTimeLeft > BB_BOUNTY_TIME_LIMIT
			PRINTLN("MAINTAIN_BOUNTY_TIMER(): time left is great than total time limit reseting ")
			CLEAR_STORED_BOUNTY_ARRAY_SLOT(MPGlobalsAmbience.playerBounty.iSlowLoopMaintainDailyLimit)
		ELSE
			iTimeLeft = BB_BOUNTY_TIME_LIMIT - iTimeLeft
		ENDIF
		IF iTimeLeft <= 0
			CLEAR_STORED_BOUNTY_ARRAY_SLOT(MPGlobalsAmbience.playerBounty.iSlowLoopMaintainDailyLimit)
		ENDIF
	ENDIF
	MPGlobalsAmbience.playerBounty.iSlowLoopMaintainDailyLimit++
	IF MPGlobalsAmbience.playerBounty.iSlowLoopMaintainDailyLimit >= MAX_BOUNTIES_PER_DAY
		MPGlobalsAmbience.playerBounty.iSlowLoopMaintainDailyLimit= 0
	ENDIF
ENDPROC
//
//PROC MAINTAIN_BOUNTY_BLIPPING(BOUNTY_STRUCT &Bounty)
//	PLAYER_INDEX playerID
//	IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
//		playerID = INT_TO_PLAYERINDEX(Bounty.iSlowBlipPlayer)
//		IF IS_NET_PLAYER_OK(playerID)
//			IF IS_BOUNTY_SET_ON_PLAYER(playerID)
//				SET_BIT(Bounty.iBlippedPlayerBS, Bounty.iSlowBlipPlayer)
//				IF NOT IS_BIT_SET(g_PlayerBlipsData.bs_FlashingAlways, Bounty.iSlowBlipPlayer)
//					//FLASH_BLIP_FOR_PLAYER(playerID, TRUE, -1)
//				ENDIF
//			ELSE
//				IF IS_BIT_SET(Bounty.iBlippedPlayerBS, Bounty.iSlowBlipPlayer)
//					//FLASH_BLIP_FOR_PLAYER(playerID, FALSE, -1)
//					CLEAR_BIT(Bounty.iBlippedPlayerBS, Bounty.iSlowBlipPlayer)
//				ENDIF
//			ENDIF
//		ENDIF
//		Bounty.iSlowBlipPlayer++
//		IF Bounty.iSlowBlipPlayer >= NUM_NETWORK_PLAYERS
//			Bounty.iSlowBlipPlayer = 0
//		ENDIF
//	ENDIF
//ENDPROC

PROC MAINTAIN_BOUNTY_FUNCTIONALITY()
	MAINTAIN_BOUNTY_TIMER()
	//MAINTAIN_BOUNTY_BLIPPING(Bounty)
	MAINTAIN_BOUNTY_OVER_SAVE_CHECKS()
ENDPROC

PROC TRIGGER_NPC_BOUNTY(PLAYER_INDEX targetPlayer,STRING paramText, enumCharacterList charEnum = NO_CHARACTER,BOOL bInitTxt = FALSE)
	IF NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_BOUNTIES)
		PRINTLN("TRIGGER_NPC_BOUNTY: NOT IS_FM_TYPE_UNLOCKED(FMMC_TYPE_BOUNTIES) aborting")
		EXIT
	ENDIF
	
	IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PLayerCompletedTriggerTut)
		PRINTLN("TRIGGER_NPC_BOUNTY: HAS_PLAYER_COMPLETED_RACE_AND_DM_TUTORIAL(PLAYER_ID()) aborting")
		EXIT
	ENDIF
	IF IS_BOUNTY_SET_ON_PLAYER(targetPlayer)
		PRINTLN("TRIGGER_NPC_BOUNTY: Bounty already set on player aborting")
		EXIT
	ENDIF
	
	MPGlobalsAmbience.npcBountyRequests.targetPlayer = targetPlayer
	BROADCAST_ASK_SERVER_FOR_CONTACT_REQUEST(ALL_PLAYERS(),REQUEST_BOUNTY,targetPlayer,REQUEST_VARIATION_NPC)
	
	IF bInitTxt
		PRINTLN("TRIGGER_NPC_BOUNTY: Sending a pre-bounty text message for NPC")
		Send_MP_Txtmsg_From_CharSheetID(charEnum,paramText)
	ENDIF
ENDPROC

FUNC BOOL IS_VEHICLE_MODEL_VALID_FOR_STOLEN_CAR_BOUNTY(MODEL_NAMES theModel)
	IF IS_MODEL_POLICE_VEHICLE(theModel)
		PRINTLN("IS_VEHICLE_MODEL_VALID_FOR_STOLEN_CAR_BOUNTY: False police vehicle")
		RETURN FALSE
	ENDIF
	
	IF theModel = RHINO
		PRINTLN("IS_VEHICLE_MODEL_VALID_FOR_STOLEN_CAR_BOUNTY: False RHINO")
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_MODEL_A_CAR(theModel)
	OR IS_THIS_MODEL_A_BIKE(theModel)
		SWITCH theModel
			CASE Bulldozer
			CASE Cutter
			CASE Dump
			CASE Handler
			CASE mixer
			CASE Airtug
			CASE Caddy
			CASE tractor2
			CASE Forklift
			CASE Mower
			CASE Ripley
			CASE AMBULANCE
			CASE FIRETRUK
			CASE FBI
			CASE FBI2
			CASE Pranger
			CASE dilettante2
			CASE lguard
			CASE pbus
			CASE riot
			CASE stockade
			CASE stockade3
			CASE BARRACKS
			CASE BARRACKS2
			CASE CRUSADER
				PRINTLN("IS_VEHICLE_MODEL_VALID_FOR_STOLEN_CAR_BOUNTY: False this vehicle does not trigger bounty")
				RETURN FALSE
			BREAK
		ENDSWITCH
		
	ELSE
		PRINTLN("IS_VEHICLE_MODEL_VALID_FOR_STOLEN_CAR_BOUNTY: False not a bike or car")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_PLAYER_STILL_VALID_FOR_STOLEN_CAR_BOUNTY()
	PED_INDEX playerPed
	VEHICLE_INDEX tempVeh
	MODEL_NAMES theModel
	IF IS_NET_PLAYER_OK(PLAYER_ID())
		IF NOT NETWORK_IS_IN_TUTORIAL_SESSION()
		AND IS_FM_TYPE_UNLOCKED(FMMC_TYPE_BOUNTIES)
		AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmTutProgBitset, biTrigTut_PLayerCompletedTriggerTut) //Finish race/DM tutorial
			IF NOT IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				playerPed = PLAYER_PED_ID()
				IF IS_PED_IN_ANY_VEHICLE(playerPed)
					tempVeh = GET_VEHICLE_PED_IS_IN(playerPed)
					
					IF DECOR_IS_REGISTERED_AS_TYPE("Player_Vehicle", DECOR_TYPE_INT)
						IF DECOR_EXIST_ON(tempVeh,"Player_Vehicle")
							#IF IS_DEBUG_BUILD
								PRINTLN("IS_PLAYER_STILL_VALID_FOR_STOLEN_CAR_BOUNTY: NO this is a player vehicle ")
							#ENDIF
							RETURN FALSE
						ENDIF
					ENDIF
					theModel = GET_ENTITY_MODEL(tempVeh)
					IF IS_VEHICLE_MODEL_VALID_FOR_STOLEN_CAR_BOUNTY(theModel)
						RETURN TRUE
					#IF IS_DEBUG_BUILD
					ELSE
						PRINTLN("IS_PLAYER_STILL_VALID_FOR_STOLEN_CAR_BOUNTY: NO vehicle mode is not valid for bounty")
					#ENDIF
					ENDIF
				#IF IS_DEBUG_BUILD
				ELSE
					PRINTLN("IS_PLAYER_STILL_VALID_FOR_STOLEN_CAR_BOUNTY: NO player not in a vehicle ")
				#ENDIF
				ENDIF
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("IS_PLAYER_STILL_VALID_FOR_STOLEN_CAR_BOUNTY: NO player on FM mission ")
			#ENDIF
			ENDIF
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("IS_PLAYER_STILL_VALID_FOR_STOLEN_CAR_BOUNTY: NO not unlocked it yet ")
		#ENDIF
		ENDIF
	#IF IS_DEBUG_BUILD
	ELSE
		PRINTLN("IS_PLAYER_STILL_VALID_FOR_STOLEN_CAR_BOUNTY: NO not OK ")
	#ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//enumCharacterList charEnum = NO_CHARACTER,
//, INT iBountyType = 0, BOOL bInitTxt = FALSE)
FUNC BOOL MAINTAIN_TRIGGER_NPC_BOUNTY()
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		RETURN FALSE
	ENDIF
	//MAINTAIN STOLEN VEHICLE BOUNTY
	INT iRandInt
	IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.npcBountyRequests.stStoleCarDelay)
		IF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.npcBountyRequests.stStoleCarDelay,MPGlobalsAmbience.npcBountyRequests.iDelayTime,TRUE)
			IF IS_PLAYER_STILL_VALID_FOR_STOLEN_CAR_BOUNTY()
				iRandInt = GET_RANDOM_INT_IN_RANGE(0,1000)
				IF iRandInt <= 333
					TRIGGER_NPC_BOUNTY(PLAYER_ID(),"TXT_BNTY_NPC1",CHAR_BLOCKED,TRUE)
				ELIF iRandInt <= 666
					TRIGGER_NPC_BOUNTY(PLAYER_ID(),"TXT_BNTY_NPC2",CHAR_BLOCKED,TRUE)
				ELSE
					TRIGGER_NPC_BOUNTY(PLAYER_ID(),"TXT_BNTY_NPC3",CHAR_BLOCKED,TRUE)
				ENDIF
				RESET_NET_TIMER(MPGlobalsAmbience.npcBountyRequests.stStoleCarDelay)
			ELSE
				PRINTLN("MAINTAIN_TRIGGER_NPC_BOUNTY: player not in appropriate vehicle aborting")
				RESET_NET_TIMER(MPGlobalsAmbience.npcBountyRequests.stStoleCarDelay)
			ENDIF
		ENDIF
	ENDIF

	IF HAS_NET_TIMER_STARTED(MPGlobalsAmbience.npcBountyRequests.stOutstandingTimer)
		IF IS_BOUNTY_SET_ON_PLAYER(MPGlobalsAmbience.npcBountyRequests.targetPlayer)
			PRINTLN("MAINTAIN_TRIGGER_NPC_BOUNTY: Bounty already set on player aborting")
			MPGlobalsAmbience.npcBountyRequests.iResultBS = 0
			BROADCAST_ASK_SERVER_FOR_CLEAR_PENDING_CONTACT_REQUEST(ALL_PLAYERS(),REQUEST_BOUNTY,MPGlobalsAmbience.npcBountyRequests.targetPlayer,REQUEST_VARIATION_NPC)
			RETURN TRUE
		ENDIF
		IF NOT IS_NET_PLAYER_OK(MPGlobalsAmbience.npcBountyRequests.targetPlayer,FALSE)
			MPGlobalsAmbience.npcBountyRequests.iResultBS = 0
			BROADCAST_ASK_SERVER_FOR_CLEAR_PENDING_CONTACT_REQUEST(ALL_PLAYERS(),REQUEST_BOUNTY,MPGlobalsAmbience.npcBountyRequests.targetPlayer,REQUEST_VARIATION_NPC)
			RETURN TRUE
		ENDIF	
		
		IF MPGlobalsAmbience.npcBountyRequests.iResultBS != 0
			INT iRand = GET_RANDOM_INT_IN_RANGE(0,50000)
			INT iBountyValue 
			IF iRand <= 15000
				iBountyValue = BB_GET_MONEY_FOR_POS(0) - g_sMPTunables.iLESTER_BOUNTY_CUT
			ELIF iRand <= 25000
		 		iBountyValue = BB_GET_MONEY_FOR_POS(1) - g_sMPTunables.iLESTER_BOUNTY_CUT
			ELIF iRand <= 35000 
				iBountyValue = BB_GET_MONEY_FOR_POS(2) - g_sMPTunables.iLESTER_BOUNTY_CUT
			ELIF iRand <= 45000 
				iBountyValue = BB_GET_MONEY_FOR_POS(3) - g_sMPTunables.iLESTER_BOUNTY_CUT
			ELSE
				iBountyValue = BB_GET_MONEY_FOR_POS(4) - g_sMPTunables.iLESTER_BOUNTY_CUT
			ENDIF
			BROADCAST_SET_BOUNTY_ON_PLAYER(ALL_PLAYERS(),MPGlobalsAmbience.npcBountyRequests.targetPlayer,iBountyValue,1)
			CLEAR_BIT(MPGlobalsAmbience.npcBountyRequests.iBS,NPC_BOUNTY_BS_RESENT_REQUEST)
			PRINTLN(" MAINTAIN_TRIGGER_NPC_BOUNTY: MPGlobalsAmbience.npcBountyRequests.iResultBS != 0")
			MPGlobalsAmbience.npcBountyRequests.iResultBS = 0
			
			BROADCAST_ASK_SERVER_FOR_CLEAR_PENDING_CONTACT_REQUEST(ALL_PLAYERS(),REQUEST_BOUNTY,MPGlobalsAmbience.npcBountyRequests.targetPlayer,REQUEST_VARIATION_NPC)	
		ELSE
			PRINTLN(" MAINTAIN_TRIGGER_NPC_BOUNTY: WAIT_FOR_SERVER_RESPONSE: response is 0 still")
			IF g_TransitionSessionNonResetVars.contactRequests.piHostWhenRequested = INVALID_PLAYER_INDEX()
			OR g_TransitionSessionNonResetVars.contactRequests.piHostWhenRequested != NETWORK_GET_HOST_OF_SCRIPT(FREEMODE_SCRIPT())
				BROADCAST_ASK_SERVER_FOR_CONTACT_REQUEST(ALL_PLAYERS(),REQUEST_BOUNTY,MPGlobalsAmbience.npcBountyRequests.targetPlayer,REQUEST_VARIATION_NPC)
				REINIT_NET_TIMER(MPGlobalsAmbience.npcBountyRequests.stOutstandingTimer,TRUE)
				PRINTLN("MAINTAIN_TRIGGER_NPC_BOUNTY: Player re-requesting bounty from server- Host no longer valid")
			ENDIF
			IF HAS_NET_TIMER_EXPIRED(MPGlobalsAmbience.npcBountyRequests.stOutstandingTimer,CONTACT_REQUEST_TIMEOUT,TRUE)
			
				IF NOT IS_BIT_SET(MPGlobalsAmbience.npcBountyRequests.iBS,NPC_BOUNTY_BS_RESENT_REQUEST)
					BROADCAST_ASK_SERVER_FOR_CONTACT_REQUEST(ALL_PLAYERS(),REQUEST_BOUNTY,MPGlobalsAmbience.npcBountyRequests.targetPlayer,REQUEST_VARIATION_NPC)
					REINIT_NET_TIMER(MPGlobalsAmbience.npcBountyRequests.stOutstandingTimer,TRUE)
					SET_BIT(MPGlobalsAmbience.npcBountyRequests.iBS,NPC_BOUNTY_BS_RESENT_REQUEST)
					PRINTLN("MAINTAIN_TRIGGER_NPC_BOUNTY: Player re-requesting bounty from server")
				ELSE
					#IF IS_DEBUG_BUILD
					PRINTLN("MAINTAIN_TRIGGER_NPC_BOUNTY: Player hasn't recieved confirmation from server for contact request in 4 seconds!")
					SCRIPT_ASSERT(" MAINTAIN_TRIGGER_NPC_BOUNTY: Player hasn't recieved confirmation from server for contact request in 4 seconds! See Conor")
					#ENDIF
					CLEAR_BIT(MPGlobalsAmbience.npcBountyRequests.iBS,NPC_BOUNTY_BS_RESENT_REQUEST)
					MPGlobalsAmbience.npcBountyRequests.iResultBS = 0
					BROADCAST_ASK_SERVER_FOR_CLEAR_PENDING_CONTACT_REQUEST(ALL_PLAYERS(),REQUEST_BOUNTY,MPGlobalsAmbience.npcBountyRequests.targetPlayer,REQUEST_VARIATION_NPC)
				ENDIF
			ENDIF
		ENDIF
	//ELSE
	//	PRINTLN("WAIT_FOR_SERVER_RESPONSE: timer hasn't started")
	ENDIF
	RETURN FALSE
ENDFUNC

PROC ADD_BOUNTY_DATA_DELAYED_REWARDS_ARRAY(PLAYER_INDEX thePlayer,GAMER_HANDLE theHandle)
	INT i
	REPEAT MAX_BOUNTY_CLAIMS_IN_TEN_SECONDS i
		IF g_PlayerToCheckForBountyReward[i] = INVALID_PLAYER_INDEX()
			IF g_PlayerToCheckForBountyReward[i] != thePlayer
				START_NET_TIMER(g_stCheCkForBountyReward[i],TRUE)
				g_PlayerToCheckForBountyReward[i] = thePlayer
				g_ghBountyPlacer[i] = theHandle
				#IF IS_DEBUG_BUILD
				IF IS_NET_PLAYER_OK(thePlayer,FALSE)
					PRINTLN("ADD_BOUNTY_DATA_DELAYED_REWARDS_ARRAY: added date to slot #",i," player = ",GET_PLAYER_NAME(thePlayer))
				ELSE
					PRINTLN("ADD_BOUNTY_DATA_DELAYED_REWARDS_ARRAY: added date to slot #",i," player = ",NATIVE_TO_INT(thePlayer))
				ENDIF
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF IS_NET_PLAYER_OK(thePlayer,FALSE)
					PRINTLN("ADD_BOUNTY_DATA_DELAYED_REWARDS_ARRAY: data for player already exists in slot #",i," player = ",GET_PLAYER_NAME(thePlayer))
				ELSE
					PRINTLN("ADD_BOUNTY_DATA_DELAYED_REWARDS_ARRAY: data for player already exists in slot #",i," player = ",NATIVE_TO_INT(thePlayer))
				ENDIF
				#ENDIF
			ENDIF
			EXIT
		ENDIF
	ENDREPEAT
	#IF IS_DEBUG_BUILD
	IF IS_NET_PLAYER_OK(thePlayer,FALSE)
		PRINTLN("ADD_BOUNTY_DATA_DELAYED_REWARDS_ARRAY: FAILED TO ADD PLAYER! player = ",GET_PLAYER_NAME(thePlayer))
	ELSE
		PRINTLN("ADD_BOUNTY_DATA_DELAYED_REWARDS_ARRAY: FAILED TO ADD PLAYER! player = ",NATIVE_TO_INT(thePlayer))
	ENDIF
	#ENDIF
ENDPROC

PROC REMOVE_BOUNTY_DATA_DELAYED_REWARDS_ARRAY(PLAYER_INDEX thePlayer,GAMER_HANDLE theHandle)
	INT i
	GAMER_HANDLE tempHandle
	REPEAT MAX_BOUNTY_CLAIMS_IN_TEN_SECONDS i
		IF g_PlayerToCheckForBountyReward[i] = thePlayer
			IF IS_NET_PLAYER_OK(thePlayer,FALSE)
				tempHandle = GET_GAMER_HANDLE_PLAYER(thePlayer)
				IF IS_GAMER_HANDLE_VALID(tempHandle )
				AND IS_GAMER_HANDLE_VALID(theHandle)
				AND NETWORK_ARE_HANDLES_THE_SAME(tempHandle,theHandle)
					#IF IS_DEBUG_BUILD
					IF IS_NET_PLAYER_OK(thePlayer,FALSE)
						PRINTLN("REMOVE_BOUNTY_DATA_DELAYED_REWARDS_ARRAY: removing date from slot #",i," player = ",GET_PLAYER_NAME(thePlayer))
					ELSE
						PRINTLN("REMOVE_BOUNTY_DATA_DELAYED_REWARDS_ARRAY: removing date from slot #",i," player = ",NATIVE_TO_INT(thePlayer))
					ENDIF
					#ENDIF
					RESET_NET_TIMER(g_stCheCkForBountyReward[i])
					g_PlayerToCheckForBountyReward[i] = INVALID_PLAYER_INDEX()
					CLEAR_GAMER_HANDLE_STRUCT(g_ghBountyPlacer[i])
				ELSE
					#IF IS_DEBUG_BUILD
					IF IS_NET_PLAYER_OK(thePlayer,FALSE)
						PRINTLN("REMOVE_BOUNTY_DATA_DELAYED_REWARDS_ARRAY: INVALID HANDLE! removing date from slot #",i," player = ",GET_PLAYER_NAME(thePlayer))
					ELSE
						PRINTLN("REMOVE_BOUNTY_DATA_DELAYED_REWARDS_ARRAY:  INVALID HANDLE! removing date from slot #",i," player = ",NATIVE_TO_INT(thePlayer))
					ENDIF
					#ENDIF
					RESET_NET_TIMER(g_stCheCkForBountyReward[i])
					g_PlayerToCheckForBountyReward[i] = INVALID_PLAYER_INDEX()
					CLEAR_GAMER_HANDLE_STRUCT(g_ghBountyPlacer[i])
				ENDIF
					
			ELSE
				#IF IS_DEBUG_BUILD
				PRINTLN("REMOVE_BOUNTY_DATA_DELAYED_REWARDS_ARRAY: PLAYER NOT ACTIVE removing date from slot #",i," player = ",NATIVE_TO_INT(thePlayer))
				#ENDIF
				RESET_NET_TIMER(g_stCheCkForBountyReward[i])
				g_PlayerToCheckForBountyReward[i] = INVALID_PLAYER_INDEX()
				CLEAR_GAMER_HANDLE_STRUCT(g_ghBountyPlacer[i])
			ENDIF
			EXIT
		ENDIF
	ENDREPEAT
ENDPROC

//EOF NL
