//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_arena_career_wall.sch																	//
// Description: Funcionality for maintaining the career wall in the arena workshop. 						//
// 				https://hub.rockstargames.com/display/RSGGTAV/Arena+Career+Wall								//
// Written by:  Online Technical Team: Orlando C-H                                                          //
// Date:  		17/08/2018																					//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_clubhouse_private.sch"
USING "context_control_public.sch"
USING "commands_graphics.sch"
USING "net_corona_V2.sch"
USING "net_arena_wars_career.sch"
USING "svm_mission_flow.sch"
USING "socialclub_leaderboard.sch"

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
///                                              CONSTS                                                     //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// Fade in duration when going from mode title -> map flyover binks (sqr fade)
CONST_FLOAT cf_ACW_BINK_FADE_IN_DURATION_MS 2000.0

// Time (ms) with no input on BOARD section before getting booted back to intro view
TWEAK_INT ci_ACW_RETURN_TO_INTRO_VIEW_TIME 7000 

// Duration of each tutorial segment before moving onto next
CONST_INT ci_WALL_TUT_DURATION 12000 

// Wall scaleform data
TWEAK_FLOAT cf_WALL_CENTRE_X 0.25
TWEAK_FLOAT cf_WALL_CENTRE_Y 0.5
TWEAK_FLOAT cf_WALL_WIDTH 0.5
TWEAK_FLOAT cf_WALL_HEIGHT 1.0

// TV bink data
TWEAK_FLOAT cf_TV_CENTRE_X 0.5
TWEAK_FLOAT cf_TV_CENTRE_Y 0.5
TWEAK_FLOAT cf_TV_WIDTH 1.0
TWEAK_FLOAT cf_TV_HEIGHT 1.0

// TV settings
CONST_FLOAT cf_TV_VOLUME -100.0

// Trigger area locate
CONST_FLOAT cf_WALL_TRIGGER_AREA_V0_X 207.450790
CONST_FLOAT cf_WALL_TRIGGER_AREA_V0_Y 5162.068848
CONST_FLOAT cf_WALL_TRIGGER_AREA_V0_Z -86.59737
CONST_FLOAT cf_WALL_TRIGGER_AREA_V1_X 202.558456
CONST_FLOAT cf_WALL_TRIGGER_AREA_V1_Y 5162.038086
CONST_FLOAT cf_WALL_TRIGGER_AREA_V1_Z -83.586205
CONST_FLOAT cf_WALL_TRIGGER_WIDTH     1.25

// Tigger area width for owners is larger
CONST_FLOAT cf_WALL_OWNER_TRIGGER_WIDTH  2.0

CONST_INT ci_WALL_GRID_SIZE_X 2
CONST_INT ci_WALL_GRID_SIZE_Y 2

// These should always be 0 and are just used with widgets to help find ideal cam positions
TWEAK_FLOAT cf_WALL_CAM_OFFSET_X 0.0
TWEAK_FLOAT cf_WALL_CAM_OFFSET_Y 0.0
TWEAK_FLOAT cf_WALL_CAM_OFFSET_Z 0.0
TWEAK_FLOAT cf_WALL_CAM_LOOKAT_X 0.0
TWEAK_FLOAT cf_WALL_CAM_LOOKAT_Y 0.0
TWEAK_FLOAT cf_WALL_CAM_LOOKAT_Z 0.0

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
///                                          STRUCTS & ENUMS                                                //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

FUNC BOOL IS_ARENA_WARS_SERIES_HIDDEN()
	RETURN IS_BIT_SET(g_sV2CoronaVars.iPimHideJobStat, ciPI_HIDE_MENU_ITEM_JOBS_SERIES_ARENA_WAR) OR NET_CORONA_SHOULD_HIDE_ALL_SERIES_MODE_BLIPS()
ENDFUNC
 
FUNC INT GET_CURRENT_ARENA_JOB_INDEX()
	INT iPlaylist = CV2_GET_ARENA_SERIES_ARRAY()
	IF NETWORK_SESSION_IS_SOLO()
	OR IS_ARENA_WARS_SERIES_HIDDEN()
		RETURN CV2_PROFESSIONAL_GET_CURRENTLY_ACTIVE_V2_CORONA_POS(iPlaylist)
	ELSE
		RETURN g_sV2CoronaVars.iCurrentPlaylistPosProfessional[iPlayList]
	ENDIF
ENDFUNC

ENUM ARENA_CAREER_WALL_STATE
	ACWS_LOAD,
	ACWS_MOCAP_SCENE,
	ACWS_WAIT_FOR_TRIGGER,
	ACWS_LOOK_AT_WALL,
	ACWS_DO_TUTORIAL,
	ACWS_CLEANUP_COMPLETE,
	ACWS_LEADERBOARD,
	ACWS_COUNT
ENDENUM

#IF IS_DEBUG_BUILD
STRUCT ARENA_CAREER_WALL_DEBUG
	BOOL bWidgetsReady
	INT iState
	BOOL bDebugDraw
	BOOL bResetTutorialStat
	BOOL bClearSeenBonusHelp
	FLOAT fTimeFactor
	BOOL bApplyAltOffsets
	BOOL bApplyDefOffsets
	BOOL bResetTrinketTickerStat
	INT iOverrideJobIndex =-1
ENDSTRUCT
#ENDIF

STRUCT VEC2_INT
	INT x
	INT y
ENDSTRUCT

ENUM ARENA_CAREER_WALL_VIEW
	ACWV_PANEL_BOARD_INTRO,		
	ACWV_PANEL_OVERVIEW, 
	ACWV_PANEL_BOARD_TL,	
	ACWV_PANEL_BOARD_TR,	
	ACWV_PANEL_BOARD_BL,
	ACWV_PANEL_BOARD_BR,		
	ACWV_PANEL_BOARD_C,	
	ACWV_PANEL_TV,			
	ACWV_PANEL_TROPHY,
	ACWV_NONE
ENDENUM

STRUCT ARENA_CAREER_WALL_CAMERA
	CAMERA_INDEX ciCam
	FLOAT fZoomAlpha = 0.0

	ARENA_CAREER_WALL_VIEW eView
	ARENA_CAREER_WALL_VIEW ePrevBoardView 	// Last board panel looked at
	ARENA_CAREER_WALL_VIEW ePrevView 		// Last panel looked at

	VEC2_INT v2GridPos
	VEC2_INT v2StoredGridPos

	structPointCam sStoredOffset
	structPointCam sCurrentOffset
	structPointCam sTargetOffset
ENDSTRUCT

STRUCT SCALEFORM_RENDER_TO_RT_DATA
	INT iArenaJobIndex = -1
	INT iRenderTargetId = -1
	STRING sRenderTargetName
	SCALEFORM_INDEX sScaleform
	MODEL_NAMES eWallProp
ENDSTRUCT

STRUCT BINK_RENDER_TO_RT_DATA
	INT iArenaJobIndex = -1
	INT iRenderTargetId = -1
	STRING sRenderTargetName
	SCRIPT_TIMER mapClipFadeInStart
	INT iModeMapClipHash
	INT iPlaylistHash
	MODEL_NAMES eTvProp
ENDSTRUCT

ENUM NAVIGATION_INPUT
	NAVIGATION_INPUT_NONE,
	NAVIGATION_INPUT_N,
	NAVIGATION_INPUT_NE,
	NAVIGATION_INPUT_E,
	NAVIGATION_INPUT_SE,
	NAVIGATION_INPUT_S,
	NAVIGATION_INPUT_SW,
	NAVIGATION_INPUT_W,
	NAVIGATION_INPUT_NW,
	NAVIGATION_INPUT_F,	
	NAVIGATION_INPUT_B,
	NAVIGATION_INPUT_OVERVIEW,
	NAVIGATION_INPUT_DETAILS
ENDENUM

STRUCT ARENA_CAREER_WALL_INPUT
	CLUBHOUSE_MENU_INPUT base 
	BOOL bGoOnCall
	BOOL bCanScroll
	NAVIGATION_INPUT eNavInput
	SCRIPT_TIMER scrollTimer
	SCRIPT_TIMER idleTimer
ENDSTRUCT

STRUCT ARENA_CAREER_WALL
	PLAYER_INDEX pOwnerId
	ARENA_CAREER_WALL_STATE            eState
	 
	SCALEFORM_RENDER_TO_RT_DATA        drawDataWall
	BINK_RENDER_TO_RT_DATA             drawDataTv
	ARENA_CAREER_WALL_CAMERA		   cam
	
	ARENA_CAREER_WALL_INPUT input 	
	
	INT iTriggerContext = NEW_CONTEXT_INTENTION
	
	BOOL bLoadedButtonHelp = FALSE
	BOOL bDoneFirstLoadFrame = FALSE
	BOOL bGotStats = FALSE
	BOOL bDoneOneTimeHelp = FALSE
	BOOL bLoadedPcControls = FALSE
	INT iBonusHelpCount
	
	TIME_DATATYPE tStatsRequestTime

	BLIP_INDEX blip
	
	INT iArenaJobIndex = -1 		// e.g. ciARENA_BOMB_BALL_II
	
	INT iTutorialStage
	SCRIPT_TIMER tutorialTimer
	
	INT iMocapStage
	
	INT iBsTrinkets // TrinketBs for the owner at time of entry
	
	SC_LEADERBOARD_CONTROL_STRUCT scLB_control
	SCALEFORM_INDEX scLB_ScaleformID[2]
	BOOL bSetupSCLeaderboardData
	INT iCurrentLeaderboardID = 0
				
	#IF IS_DEBUG_BUILD
	ARENA_CAREER_WALL_DEBUG debug
	#ENDIF
ENDSTRUCT

//////////////////////////////////////////////////////////////////////////////////////////////////////////////
///                                                FUNCTIONS                                                //
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
#IF IS_DEBUG_BUILD
FUNC STRING _ARENA_CAREER_WALL_GET_STATE_STRING(ARENA_CAREER_WALL_STATE& eState)
	SWITCH eState
		CASE ACWS_LOAD             	RETURN "LOAD"	
		CASE ACWS_MOCAP_SCENE		RETURN "MOCAP_SCENE"
		CASE ACWS_WAIT_FOR_TRIGGER 	RETURN "WAIT_FOR_TRIGGER"	
		CASE ACWS_LOOK_AT_WALL     	RETURN "LOOK_AT_WALL"	
		CASE ACWS_DO_TUTORIAL	   	RETURN "DO_TUTORIAL"
		CASE ACWS_LEADERBOARD	   	RETURN "LEADERBOARD"
		CASE ACWS_CLEANUP_COMPLETE 	RETURN "CLEANUP_COMPLETE"
		DEFAULT PRINTLN("[ARENA_CAREER_WALL] _ARENA_CAREER_WALL_GET_STATE_STRING ", ENUM_TO_INT(eState)) RETURN "Unknown state"
	ENDSWITCH
ENDFUNC
#ENDIF

CONST_INT ci_ACW_VIEW_TV 		0
CONST_INT ci_ACW_VIEW_BOARD 	1
CONST_INT ci_ACW_VIEW_TROPHY	2
CONST_INT ci_ACW_VIEW_OVERVIEW	3

FUNC STRING GET_VIEW_LABEL(ARENA_CAREER_WALL_VIEW eView)
	SWITCH eView
		CASE ACWV_PANEL_BOARD_BL
		CASE ACWV_PANEL_BOARD_BR
		CASE ACWV_PANEL_BOARD_TL
		CASE ACWV_PANEL_BOARD_TR
		CASE ACWV_PANEL_BOARD_C
		CASE ACWV_PANEL_BOARD_INTRO RETURN "ACW_BOARD"
		CASE ACWV_PANEL_TV			RETURN "ACW_TV"
		CASE ACWV_PANEL_TROPHY		RETURN "ACW_TROPHY"
		CASE ACWV_PANEL_OVERVIEW	RETURN "ACW_OVERVIEW"
		DEFAULT 					RETURN "UNDEFINED"
	ENDSWITCH
ENDFUNC

PROC _FORCE_UI_UPDATE(ARENA_CAREER_WALL& ref_data)
	ref_data.bLoadedButtonHelp = FALSE
ENDPROC

FUNC BOOL _ARENA_CAREER_WALL_VIEW_IS_BOARD(ARENA_CAREER_WALL_VIEW eView)
	RETURN eView = ACWV_PANEL_BOARD_TL
		OR eView = ACWV_PANEL_BOARD_TR
		OR eView = ACWV_PANEL_BOARD_BL
		OR eView = ACWV_PANEL_BOARD_BR
		OR eView = ACWV_PANEL_BOARD_C
		OR eView = ACWV_PANEL_BOARD_INTRO
ENDFUNC

/// PURPOSE:
///    Helper for _ARENA_CAREER_WALL_GET_PANEL_FROM_INPUT
FUNC ARENA_CAREER_WALL_VIEW _GET_TROHPY_VIEW(PLAYER_INDEX player)
	IF DOES_PLAYER_HAVE_ANY_CAREER_WALL_TRINKETS(player)
		RETURN ACWV_PANEL_TROPHY
	ELSE
		RETURN ACWV_NONE
	ENDIF
ENDFUNC

/// PURPOSE:
///    Returns ACWV_NONE if the input shouldn't do anything on the current view.
FUNC ARENA_CAREER_WALL_VIEW _ARENA_CAREER_WALL_GET_PANEL_FROM_INPUT(ARENA_CAREER_WALL& ref_local, NAVIGATION_INPUT eNavInput)	
	SWITCH ref_local.cam.eView
		CASE ACWV_PANEL_BOARD_TL
			SWITCH eNavInput
				CASE NAVIGATION_INPUT_E			RETURN ACWV_PANEL_BOARD_TR
				CASE NAVIGATION_INPUT_S			RETURN ACWV_PANEL_BOARD_BL

				CASE NAVIGATION_INPUT_F			RETURN _GET_TROHPY_VIEW(ref_local.pOwnerId)
				CASE NAVIGATION_INPUT_B			RETURN ACWV_PANEL_TV
				CASE NAVIGATION_INPUT_OVERVIEW	RETURN ACWV_PANEL_OVERVIEW
			ENDSWITCH
			BREAK
		CASE ACWV_PANEL_BOARD_TR
			SWITCH eNavInput
				CASE NAVIGATION_INPUT_S			RETURN ACWV_PANEL_BOARD_BR
				CASE NAVIGATION_INPUT_W			RETURN ACWV_PANEL_BOARD_TL

				CASE NAVIGATION_INPUT_F			RETURN _GET_TROHPY_VIEW(ref_local.pOwnerId)
				CASE NAVIGATION_INPUT_B			RETURN ACWV_PANEL_TV
				CASE NAVIGATION_INPUT_OVERVIEW	RETURN ACWV_PANEL_OVERVIEW
			ENDSWITCH		
			BREAK
		CASE ACWV_PANEL_BOARD_BL
			SWITCH eNavInput
				CASE NAVIGATION_INPUT_N			RETURN ACWV_PANEL_BOARD_TL
				CASE NAVIGATION_INPUT_E			RETURN ACWV_PANEL_BOARD_BR
				
				CASE NAVIGATION_INPUT_F			RETURN _GET_TROHPY_VIEW(ref_local.pOwnerId)
				CASE NAVIGATION_INPUT_B			RETURN ACWV_PANEL_TV
				CASE NAVIGATION_INPUT_OVERVIEW	RETURN ACWV_PANEL_OVERVIEW				
			ENDSWITCH
			BREAK
		CASE ACWV_PANEL_BOARD_BR
			SWITCH eNavInput
				CASE NAVIGATION_INPUT_N			RETURN ACWV_PANEL_BOARD_TR
				CASE NAVIGATION_INPUT_W			RETURN ACWV_PANEL_BOARD_BL
				
				CASE NAVIGATION_INPUT_F			RETURN _GET_TROHPY_VIEW(ref_local.pOwnerId)
				CASE NAVIGATION_INPUT_B			RETURN ACWV_PANEL_TV
				CASE NAVIGATION_INPUT_OVERVIEW	RETURN ACWV_PANEL_OVERVIEW				
			ENDSWITCH		
			BREAK
		CASE ACWV_PANEL_BOARD_C
			SWITCH eNavInput
				CASE NAVIGATION_INPUT_NE		RETURN ACWV_PANEL_BOARD_TR
				CASE NAVIGATION_INPUT_SE		RETURN ACWV_PANEL_BOARD_BR
				CASE NAVIGATION_INPUT_SW		RETURN ACWV_PANEL_BOARD_BL
				CASE NAVIGATION_INPUT_NW		RETURN ACWV_PANEL_BOARD_TL
				
				CASE NAVIGATION_INPUT_F			RETURN _GET_TROHPY_VIEW(ref_local.pOwnerId)
				CASE NAVIGATION_INPUT_B			RETURN ACWV_PANEL_TV
				CASE NAVIGATION_INPUT_OVERVIEW	RETURN ACWV_PANEL_OVERVIEW				
			ENDSWITCH		
			BREAK
		CASE ACWV_PANEL_BOARD_INTRO
			SWITCH eNavInput
				CASE NAVIGATION_INPUT_N		
				CASE NAVIGATION_INPUT_NE		RETURN ACWV_PANEL_BOARD_TR
				CASE NAVIGATION_INPUT_E		
				CASE NAVIGATION_INPUT_SE		RETURN ACWV_PANEL_BOARD_BR
				CASE NAVIGATION_INPUT_S		
				CASE NAVIGATION_INPUT_SW		RETURN ACWV_PANEL_BOARD_BL
				
				CASE NAVIGATION_INPUT_W	
				CASE NAVIGATION_INPUT_NW		RETURN ACWV_PANEL_BOARD_TL
				
				CASE NAVIGATION_INPUT_F			RETURN _GET_TROHPY_VIEW(ref_local.pOwnerId)
				CASE NAVIGATION_INPUT_B			RETURN ACWV_PANEL_TV
				CASE NAVIGATION_INPUT_OVERVIEW	RETURN ACWV_PANEL_OVERVIEW				
			ENDSWITCH	
			BREAK
		CASE ACWV_PANEL_TV
			SWITCH eNavInput
				CASE NAVIGATION_INPUT_F			RETURN ref_local.cam.ePrevBoardView
				CASE NAVIGATION_INPUT_OVERVIEW	RETURN ACWV_PANEL_OVERVIEW		
			ENDSWITCH	
			BREAK
		CASE ACWV_PANEL_TROPHY
			SWITCH eNavInput
				CASE NAVIGATION_INPUT_B			RETURN ref_local.cam.ePrevBoardView
				CASE NAVIGATION_INPUT_OVERVIEW	RETURN ACWV_PANEL_OVERVIEW		
			ENDSWITCH	
			BREAK	
		CASE ACWV_PANEL_OVERVIEW
			SWITCH eNavInput
				CASE NAVIGATION_INPUT_DETAILS	RETURN ref_local.cam.ePrevView
			ENDSWITCH	
			BREAK			
	ENDSWITCH
	
	RETURN ACWV_NONE
ENDFUNC

PROC _ARENA_CAREER_WALL_GET_CAM_TRANSFORM(ARENA_CAREER_WALL_VIEW eView, VECTOR& ref_vPos, VECTOR& ref_vLookAt)
	SWITCH eView
		CASE ACWV_PANEL_BOARD_INTRO	
			ref_vPos = <<204.6760, 5163.8755, -85.1433>>
			ref_vLookAt = <<204.68802, 5161.07324, -85.24987>>
			BREAK
		CASE ACWV_PANEL_OVERVIEW
			ref_vPos = <<204.4178, 5165.1016, -84.9881>>
			ref_vLookAt = <<205.49130, 5161.07422, -85.19710>>
			BREAK
		CASE ACWV_PANEL_BOARD_TL
			ref_vPos = <<205.25+0.200, 5162.8193, -84.695>>
			ref_vLookAt = <<205.395, 5161.07324, -84.68>>
			BREAK		
		CASE ACWV_PANEL_BOARD_TR	
			ref_vPos = <<204.5966+0.040-0.280, 5162.7593, -84.7251-0.030>>
			ref_vLookAt = <<204.30009-0.010, 5161.07324, -84.82117-0.070>>
			BREAK
		CASE ACWV_PANEL_BOARD_BL
			ref_vPos = <<205.0364+0.200, 5162.7949, -85.1134>>
			ref_vLookAt = <<205.27893, 5161.07324, -85.56166>> 
			BREAK
		CASE ACWV_PANEL_BOARD_BR		
			ref_vPos = <<204.62-0.280, 5162.7915, -85.125>>
			ref_vLookAt = <<204.36952, 5161.07324, -85.60269>> 
			BREAK
		CASE ACWV_PANEL_BOARD_C	
			ref_vPos = <<204.7345, 5162.9795, -85.0443>>
			ref_vLookAt = <<204.71086, 5161.07324, -85.32809>>
			BREAK
		CASE ACWV_PANEL_TV		
			ref_vPos = <<205.4797, 5163.2100, -85.1254>>  
			ref_vLookAt = <<206.74510, 5161.69434, -85.09945>> 
			BREAK
		CASE ACWV_PANEL_TROPHY	
			ref_vPos = <<204.8491-0.100, 5163.3003, -85.1523-0.430-0.190>>
			ref_vLookAt = <<204.85750-0.100, 5161.07324, -85.86658+0.090>>
			BREAK
	ENDSWITCH
ENDPROC

PROC _ARENA_CAREER_WALL_SET_STATE(ARENA_CAREER_WALL& ref_local, ARENA_CAREER_WALL_STATE eState)
	PRINTLN("[ARENA_CAREER_WALL] SET STATE from ",_ARENA_CAREER_WALL_GET_STATE_STRING(ref_local.eState), " to ", _ARENA_CAREER_WALL_GET_STATE_STRING(eState))
	ref_local.eState = eState
ENDPROC

PROC BUILD_CAREER_WALL_TEXTURE_NAME(INT iArenaJobIndex, TEXT_LABEL_63& ref_name)
	INT iType = GET_ARENA_JOB_TYPE(iArenaJobIndex)
	INT iTheme = GET_ARENA_JOB_THEME(iArenaJobIndex)
	
	ref_name = GET_ARENA_THEME_STRING(iTheme)
	ref_name += "_"
	ref_name += GET_ARENA_JOB_TYPE_SCALEFORM_STRING(iType)
	
	PRINTLN("[ARENA_CAREER_WALL] BUILD_CAREER_WALL_TEXTURE_NAME iArenaJobIndex = ", iArenaJobIndex, " -> iTheme = ", iTheme," iType = ", iType, " name = ", ref_name)
ENDPROC

PROC BUILD_CAREER_WALL_TV_PLAYLIST_NAME(INT iArenaJobIndex, TEXT_LABEL_15& ref_name)
	INT iType = GET_ARENA_JOB_TYPE(iArenaJobIndex)
	INT iTheme = GET_ARENA_JOB_THEME(iArenaJobIndex)
	
	ref_name = "AWTV_"
	ref_name += GET_ARENA_THEME_STRING(iTheme)
	ref_name += "_"
	ref_name += GET_ARENA_JOB_TYPE_TV_PLAYLIST_STRING(iType)

	PRINTLN("[AWTV] BUILD_CAREER_WALL_TV_PLAYLIST_NAME iArenaJobIndex = ", iArenaJobIndex, " -> iTheme = ", iTheme," iType = ", iType, " name = ", ref_name)
ENDPROC

PROC BUILD_ARENA_WARS_TV_LOBBY_INTRO_PLAYLIST_NAME(INT iArenaJobIndex, TEXT_LABEL_15& ref_name)
	INT iType = GET_ARENA_JOB_TYPE(iArenaJobIndex)
	INT iTheme = GET_ARENA_JOB_THEME(iArenaJobIndex)
	
	ref_name = "INTR_"
	ref_name += GET_ARENA_THEME_STRING(iTheme)
	ref_name += "_"
	ref_name += GET_ARENA_JOB_TYPE_TV_PLAYLIST_STRING(iType)

	PRINTLN("[AWTV] BUILD_ARENA_WARS_TV_LOBBY_INTRO_PLAYLIST_NAME iArenaJobIndex = ", iArenaJobIndex, " -> iTheme = ", iTheme," iType = ", iType, " name = ", ref_name)
ENDPROC

PROC BUILD_ARENA_WARS_TV_LOBBY_LOOP_PLAYLIST_NAME(INT iArenaJobIndex, TEXT_LABEL_15& ref_name)
	INT iType = GET_ARENA_JOB_TYPE(iArenaJobIndex)
	INT iTheme = GET_ARENA_JOB_THEME(iArenaJobIndex)
	
	ref_name = "LOOP_"
	ref_name += GET_ARENA_THEME_STRING(iTheme)
	ref_name += "_"
	ref_name += GET_ARENA_JOB_TYPE_TV_PLAYLIST_STRING(iType)

	PRINTLN("[AWTV] BUILD_ARENA_WARS_TV_LOBBY_LOOP_PLAYLIST_NAME iArenaJobIndex = ", iArenaJobIndex, " -> iTheme = ", iTheme," iType = ", iType, " name = ", ref_name)
ENDPROC

PROC BUILD_ARENA_WARS_TV_MAP_CLIP_NAME(INT iArenaJobIndex, TEXT_LABEL_15& ref_name)
	INT iType = GET_ARENA_JOB_TYPE(iArenaJobIndex)
	INT iTheme = GET_ARENA_JOB_THEME(iArenaJobIndex)
	
	ref_name = "CLIP_"
	ref_name += GET_ARENA_THEME_STRING(iTheme)
	ref_name += "_"
	ref_name += GET_ARENA_JOB_TYPE_TV_PLAYLIST_STRING(iType)
ENDPROC

PROC RESET_ARENA_WARS_TV_LOBBY_FADE_IN()
	g_tAwTvLobbyFadeInStart = GET_NETWORK_TIME()
	PRINTLN("[AWTV] RESET_ARENA_WARS_TV_LOBBY_FADE_IN SET g_tAwTvLobbyFadeInStart to ", NATIVE_TO_INT(g_tAwTvLobbyFadeInStart))
ENDPROC

FUNC INT GET_ARENA_WARS_TV_COLOUR_COMPONENT_VALUE_FROM_START_TIME(TIME_DATATYPE tNetworkStartTime, FLOAT fFadeInDuration = cf_ACW_BINK_FADE_IN_DURATION_MS)
	INT iElapsedTime = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tNetworkStartTime))
	FLOAT mul = iElapsedTime / fFadeInDuration
	RETURN CLAMP_INT(ROUND(255.0 * mul * mul), 0, 255)
ENDFUNC

FUNC INT GET_ARENA_WARS_TV_LOBBY_FADE_IN_COLOUR_COMPONENT_VALUE(FLOAT fFadeInDuration = cf_ACW_BINK_FADE_IN_DURATION_MS)
	RETURN GET_ARENA_WARS_TV_COLOUR_COMPONENT_VALUE_FROM_START_TIME(g_tAwTvLobbyFadeInStart, fFadeInDuration)
ENDFUNC

FUNC BOOL IS_MOST_RECENT_SIMPLE_INTERIOR_CHILD_SCRIPT_RUNNING()
	RETURN g_SimpleInteriorData.iHashOfMostRecentChildScript <> 0
		AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(g_SimpleInteriorData.iHashOfMostRecentChildScript) > 0
ENDFUNC

FUNC INT GET_MOST_RECENT_SIMPLE_INTERIOR_CHILD_SCRIPT_HASH()
	RETURN g_SimpleInteriorData.iHashOfMostRecentChildScript
ENDFUNC

/// PURPOSE:
///    Init the AWTV lobby playlist. See DRAW_ARENA_WARS_TV_LOBBY_PLAYLIST.
PROC CREATE_ARENA_WARS_TV_LOBBY_PLAYLIST(INT iArenaJobIndex)
	SET_ARENA_WARS_TV_LOBBY_JOB_INDEX(iArenaJobIndex)
	SET_ARENA_WARS_TV_LOBBY_STATE(AWTVS_INIT)
	
	TEXT_LABEL_15 txt
	BUILD_ARENA_WARS_TV_MAP_CLIP_NAME(iArenaJobIndex, txt)
	g_iAwTvLobbyMapHash = GET_HASH_KEY(txt)
	PRINTLN("[AWTV] CREATE_ARENA_WARS_TV_LOBBY_PLAYLIST SET g_iAwTvLobbyMapHash to hash of ",txt," = ", g_iAwTvLobbyMapHash)
	
	INT iType = GET_ARENA_JOB_TYPE(iArenaJobIndex)
	txt = "CLIP_ACTN_"
	txt += GET_ARENA_JOB_TYPE_TV_PLAYLIST_STRING(iType)
	g_iAwTvLobbyActionHash = GET_HASH_KEY(txt)
	PRINTLN("[AWTV] CREATE_ARENA_WARS_TV_LOBBY_PLAYLIST SET g_iAwTvLobbyActionHash to hash of ",txt," = ", g_iAwTvLobbyActionHash)

	g_iAwTvLobbyLoopCurrentHash = g_iAwTvLobbyMapHash
	PRINTLN("[AWTV] CREATE_ARENA_WARS_TV_LOBBY_PLAYLIST SET g_iAwTvLobbyLoopCurrentHash = g_iAwTvLobbyMapHash = ", g_iAwTvLobbyMapHash)
	
	g_fAwTvLobbyLoopCurrentFadeDuration = cf_ACW_BINK_FADE_IN_DURATION_MS
ENDPROC

/// PURPOSE:
///    Cleanup the AWTV lobby playlist. See DRAW_ARENA_WARS_TV_LOBBY_PLAYLIST.
PROC STOP_ARENA_WARS_TV_LOBBY_PLAYLIST()
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[AWTV] STOP_ARENA_WARS_TV_LOBBY_PLAYLIST")
	SET_ARENA_WARS_TV_LOBBY_JOB_INDEX(-1)
	SET_ARENA_WARS_TV_LOBBY_STATE(AWTVS_NONE)
	
	CLEAR_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_2)
	SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
ENDPROC

/// PURPOSE:
///    Process and draw the Arena Wars Tv (full screen) for the current Arena Wars Job in the series.
///    | CREATE_ARENA_WARS_TV_LOBBY_PLAYLIST 	- once beforehand to init.
///    | STOP_ARENA_WARS_TV_LOBBY_PLAYLIST 		- to clean up.
PROC DRAW_ARENA_WARS_TV_LOBBY_PLAYLIST()
	
	// NOTE:
	// Switching playlists seems to have a few frames of lag where whatever was playing previously
	// will continue to play. So, if you're near the AW office TV (as you would be if you're using
	// the career wall), you'll see whatever was playing on the TV fullscreen when the AWTV lobby 
	// playlist begins.
	//
	// To cover this switch over, the hack (in stage AWTVS_INIT/AWTVS_CREATE) is to play a short black
	// clip before the new playlist. 
	//
	// While waiting for the black clip to actually start rendering we draw a black rect fullscreen
	// to hide whatever was playing on the office TV.
	//
	// When we detect that the black clip has actually started playing we start the AWTV lobby playlist. 
	// 
	// During the switch over to the AWTV playlist there will be a few frames of black from the black
	// clip since that was the last clip playing. This looks fine because we were rending a black rect 
	// before, and are switching to a clip (the Arena War logo) which starts off black!
		
	// Prevent this processing happening more than once per frame.
	INT iFrame = GET_FRAME_COUNT()
	IF g_iAwTvLobbyLastDrawFrame = iFrame
		cdebug1ln(debug_net_dancing, "DRAW_ARENA_WARS_TV_LOBBY_PLAYLIST EXIT from ", GET_THIS_SCRIPT_NAME())
		EXIT
	ENDIF
	g_iAwTvLobbyLastDrawFrame = iFrame	
	
	// Most interiors use the TV CHANNEL natives in some way and clear them up on cleanup.
	// For the purpose of maintainability, it's easiest to assume that running an interior child
	// script and processing the lobby TV is a bad idea. Other special cases can get added as they
	// show up, e.g. base_lounge_seating (which runs the TV in the Facility).
	IF (IS_MOST_RECENT_SIMPLE_INTERIOR_CHILD_SCRIPT_RUNNING()
	AND GET_MOST_RECENT_SIMPLE_INTERIOR_CHILD_SCRIPT_HASH() <> HASH("am_mp_arena_garage"))
	OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("base_lounge_seats")) > 0
		PRINTLN("[AWTV] Launching into Arena War series from a bad place...")
		EXIT
	ENDIF
	
	// There are a few blank frames where the channel switches over.
	BOOL bDrawTvChannel = TRUE
	TEXT_LABEL_15 txt15_playlistName

	// The binks are faded in after intro to soften the cut.
	INT iColourComponents = 255
	INT a = 255
		
	SWITCH GET_ARENA_WARS_TV_LOBBY_STATE()	
		CASE AWTVS_INIT
			SET_ARENA_WARS_TV_LOBBY_STATE(AWTVS_CREATE)
			
			// Set TV playlist to a black screen loop ready for drawing the INTR_ bink playlist.
			// See "NOTE" at top of proc.	
			SET_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_2, "AWTV_START", TRUE)
			SET_TV_AUDIO_FRONTEND(FALSE)
			SET_TV_VOLUME(cf_TV_VOLUME)
			SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_2)

			PRINTLN("[AWTV] CREATE_ARENA_WARS_TV_LOBBY_PLAYLIST starting playlist AWTV_START on TVCHANNELTYPE_CHANNEL_2")
			FALLTHRU
			
		CASE AWTVS_CREATE
			IF GET_TV_CHANNEL() <> TVCHANNELTYPE_CHANNEL_2
				PRINTLN("[AWTV] DRAW_ARENA_WARS_TV_LOBBY_PLAYLIST ! SET_TV_CHANNEL")
				SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_2)
			ENDIF
			
			// Check if the AWTV_START buffer movie has started. See "NOTE" at top of proc.
			IF IS_TVSHOW_CURRENTLY_PLAYING(HASH("END_OF_MOVIE_MARKER2"))
				BUILD_ARENA_WARS_TV_LOBBY_INTRO_PLAYLIST_NAME(GET_ARENA_WARS_TV_LOBBY_JOB_INDEX(), txt15_playlistName)	
				SET_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_2, txt15_playlistName, TRUE)
				PRINTLN("[AWTV] DRAW_ARENA_WARS_TV_LOBBY_PLAYLIST starting playlist 0 ", txt15_playlistName, " on TVCHANNELTYPE_CHANNEL_2")
				SET_ARENA_WARS_TV_LOBBY_STATE(AWTVS_INTRO)
			ELSE
				// Draw a black screen to cover up the playlist switch. See "NOTE" at top of proc.
				bDrawTvChannel = FALSE
				PRINTLN("[AWTV] DRAW_ARENA_WARS_TV_LOBBY_PLAYLIST DRAW_RECT")
				DRAW_RECT(0.5, 0.5, 1, 1, 0, 0, 0, 255)
			ENDIF	
			BREAK
		
		CASE AWTVS_INTRO	
			// Check the end-of-playlist marker.
			IF IS_TVSHOW_CURRENTLY_PLAYING(HASH("END_OF_MOVIE_MARKER"))
				// Set TV playlist to the LOOP_ for this job.
				BUILD_ARENA_WARS_TV_LOBBY_LOOP_PLAYLIST_NAME(GET_ARENA_WARS_TV_LOBBY_JOB_INDEX(), txt15_playlistName)		
				SET_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_2, txt15_playlistName, TRUE)
				PRINTLN("[AWTV] DRAW_ARENA_WARS_TV_LOBBY_PLAYLIST starting playlist 1 ", txt15_playlistName, " on TVCHANNELTYPE_CHANNEL_2")
				RESET_ARENA_WARS_TV_LOBBY_FADE_IN()
				SET_ARENA_WARS_TV_LOBBY_STATE(AWTVS_LOOP_FADE_IN)
			ENDIF
			BREAK
			
		CASE AWTVS_LOOP_FADE_IN
			iColourComponents = GET_ARENA_WARS_TV_LOBBY_FADE_IN_COLOUR_COMPONENT_VALUE()
			// Go into next stage before fade in is complete so that the lobby menus display.
			IF iColourComponents > 100
				SET_ARENA_WARS_TV_LOBBY_STATE(AWTVS_LOOP)
			ENDIF
			BREAK		
			
		CASE AWTVS_LOOP
			// Fade in every time a new clip starts playing.
			// This is a hack to hide the few black frames we see between clips.
			IF g_iAwTvLobbyLoopCurrentHash = g_iAwTvLobbyMapHash
				IF IS_TVSHOW_CURRENTLY_PLAYING(g_iAwTvLobbyActionHash)
					PRINTLN("[AWTV] FLIPFLOP g_iAwTvLobbyLoopCurrentHash from ",g_iAwTvLobbyLoopCurrentHash," to ", g_iAwTvLobbyActionHash)
					g_iAwTvLobbyLoopCurrentHash = g_iAwTvLobbyActionHash
					RESET_ARENA_WARS_TV_LOBBY_FADE_IN()
				ENDIF
			ELSE
				IF IS_TVSHOW_CURRENTLY_PLAYING(g_iAwTvLobbyMapHash)
					PRINTLN("[AWTV] FLIPFLOP g_iAwTvLobbyLoopCurrentHash from ",g_iAwTvLobbyLoopCurrentHash," to ", g_iAwTvLobbyMapHash)
					g_iAwTvLobbyLoopCurrentHash = g_iAwTvLobbyMapHash
					RESET_ARENA_WARS_TV_LOBBY_FADE_IN()
				ENDIF
			ENDIF
			
			iColourComponents = GET_ARENA_WARS_TV_LOBBY_FADE_IN_COLOUR_COMPONENT_VALUE(g_fAwTvLobbyLoopCurrentFadeDuration)
			
			// If fade in is complete?
			IF iColourComponents = 255
				// The inital duration assigned in CREATE_ARENA_WARS_TV_LOBBY_PLAYLIST
				//  is longer than the one we want to happen on subsequent fades.
				g_fAwTvLobbyLoopCurrentFadeDuration = g_fAwTvLobbyLoopFadeDuration
			ENDIF
			BREAK
			
	ENDSWITCH	
	
	IF bDrawTvChannel
		SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
		SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)	
		DRAW_TV_CHANNEL(0.5, 0.5, 1.0, 1.0, 0.0, iColourComponents, iColourComponents, iColourComponents, a)
	ENDIF
	IF g_Private_BailWarningScreenEventParam > 0 
	AND IS_WARNING_MESSAGE_ACTIVE()
		STOP_ARENA_WARS_TV_LOBBY_PLAYLIST()
	ENDIF
ENDPROC



/// PURPOSE:
///    Force TVCHANNELTYPE_CHANNEL_2 to start an Arena Wars TV playlist for the job iArenaJobIndex.
///    iArenaJobIndex must be valid.
PROC CREATE_ARENA_WARS_TV_OFFICE_PLAYLIST(INT iArenaJobIndex, INT& out_iModeMapClipHash, INT& out_iPlaylistHash)
	PRINTLN("[AWTV] CREATE_ARENA_WARS_TV_OFFICE_PLAYLIST Setting g_iArenaWarTvPlaylistJobIndex from ",g_iArenaWarTvPlaylistJobIndex," to ", iArenaJobIndex)
	g_iArenaWarTvPlaylistJobIndex = iArenaJobIndex
	
	TEXT_LABEL_15 txt15_modeMapClip
	BUILD_ARENA_WARS_TV_MAP_CLIP_NAME(iArenaJobIndex, txt15_modeMapClip)
	out_iModeMapClipHash = GET_HASH_KEY(txt15_modeMapClip)
	
	TEXT_LABEL_15 txt15_playlistName
	BUILD_CAREER_WALL_TV_PLAYLIST_NAME(iArenaJobIndex, txt15_playlistName)
	out_iPlaylistHash = GET_HASH_KEY(txt15_playlistName)
	
	SET_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_2, txt15_playlistName, TRUE)
	SET_TV_AUDIO_FRONTEND(FALSE)
	SET_TV_VOLUME(cf_TV_VOLUME)
	SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_2)

	SET_ARENA_OFFICE_TV_ACTIVE_RENDER_STATE(TRUE)
ENDPROC

FUNC BOOL IS_PLAYLIST_PLAYING_ON_CHANNEL(TVCHANNELTYPE eChannelType, INT iPlaylistHash)
	RETURN GET_TV_CHANNEL() = eChannelType
		AND IS_PLAYLIST_ON_CHANNEL(TVCHANNELTYPE_CHANNEL_2, iPlaylistHash)
ENDFUNC

FUNC BOOL IS_UNSAFE_TO_RENDER_TO_RT()
	IF NOT IS_SKYSWOOP_AT_GROUND()
		PRINTLN("IS_UNSAFE_TO_RENDER_TO_RT Skyswoop not at ground")
		RETURN TRUE
	ENDIF		
	
	// The following 2 checks cause us to stop drawing too early - before fade out.
	// Leaving them here commented out in case B* 5520039 comes back as a last resort.
//	IF IS_PLAYER_IN_CORONA()
//		PRINTLN("IS_UNSAFE_TO_RENDER_TO_RT In corona")
//		RETURN TRUE
//	ENDIF
//	
//	IF IS_PLAYER_ACTIONING_ANY_CORONA_INVITE()
//		PRINTLN("IS_UNSAFE_TO_RENDER_TO_RT Actioning any corona invite")
//		RETURN TRUE
//	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Draw TVCHANNELTYPE_CHANNEL_2 to a render target. 
///    Pass in iRenderTargetId = GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID() to render to the screen.
PROC DRAW_ARENA_WARS_TV_OFFICE_PLAYLIST(ARENA_CAREER_WALL& ref_local)
	IF IS_UNSAFE_TO_RENDER_TO_RT()
		PRINTLN("[AWTV] DRAW_ARENA_WARS_TV_OFFICE_PLAYLIST FAILED - see above.")
		EXIT
	ENDIF	
	
	IF g_iArenaWarTvPlaylistJobIndex = -1
		PRINTLN("[AWTV] DRAW_ARENA_WARS_TV_OFFICE_PLAYLIST g_iArenaWarTvPlaylistJobIndex = -1, abort")
		EXIT
	ENDIF
	
	SET_ARENA_OFFICE_TV_ACTIVE_RENDER_STATE(TRUE)
	IF NOT IS_ARENA_BIG_SCREEN_RENDERING()
		IF GET_TV_CHANNEL() <> TVCHANNELTYPE_CHANNEL_2
			PRINTLN("[AWTV] DRAW_ARENA_WARS_TV_OFFICE_PLAYLIST GET_TV_CHANNEL() = ", GET_TV_CHANNEL())
			SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_2)
		ENDIF 
		
		IF IS_TVSHOW_CURRENTLY_PLAYING(ref_local.drawDataTv.iModeMapClipHash)
			IF NOT HAS_NET_TIMER_STARTED(ref_local.drawDataTv.mapClipFadeInStart)
				REINIT_NET_TIMER(ref_local.drawDataTv.mapClipFadeInStart)
			ENDIF
		ELSE
			RESET_NET_TIMER(ref_local.drawDataTv.mapClipFadeInStart)
		ENDIF
		
		INT iComponentValue = 255
		IF HAS_NET_TIMER_STARTED(ref_local.drawDataTv.mapClipFadeInStart)
			TIME_DATATYPE tStart = ref_local.drawDataTv.mapClipFadeInStart.Timer
			iComponentValue = GET_ARENA_WARS_TV_COLOUR_COMPONENT_VALUE_FROM_START_TIME(tStart)
		ENDIF
		
		IF IS_NAMED_RENDERTARGET_LINKED(ref_local.drawDataTv.eTvProp)
			SET_TEXT_RENDER_ID(ref_local.drawDataTv.iRenderTargetId)
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			DRAW_TV_CHANNEL(cf_TV_CENTRE_X, cf_TV_CENTRE_Y, cf_TV_WIDTH, cf_TV_HEIGHT, 0.0, iComponentValue, iComponentValue, iComponentValue, 255)
			SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
		ELSE
			PRINTLN("[AWTV] DRAW_ARENA_WARS_TV_OFFICE_PLAYLIST TV has become unlinked - interior streaming out?")
		ENDIF
	ELSE
		PRINTLN("[AWTV] DRAW_ARENA_WARS_TV_OFFICE_PLAYLIST Waiting for IS_ARENA_BIG_SCREEN_RENDERING to return false...")
	ENDIF	
ENDPROC

/// PURPOSE:
///    Returns the job current job TV playlist. 
///    Returns -1 if a playlist hasn't been created (CREATE_ARENA_WARS_TV_OFFICE_PLAYLIST).
FUNC INT GET_ARENA_WARS_TV_OFFICE_PLAYLIST_JOB()
	RETURN g_iArenaWarTvPlaylistJobIndex
ENDFUNC

/// PURPOSE:
///   Reset Arena Wars TV global, remove the current playlist.
PROC CLEAR_ARENA_WARS_TV_OFFICE_PLAYLIST()
	IF g_iArenaWarTvPlaylistJobIndex <> -1
		PRINTLN("[AWTV] CLEAR_ARENA_WARS_TV_OFFICE_PLAYLIST Setting g_iArenaWarTvPlaylistJobIndex from ",g_iArenaWarTvPlaylistJobIndex," to -1")
		g_iArenaWarTvPlaylistJobIndex = -1
		SET_ARENA_OFFICE_TV_ACTIVE_RENDER_STATE(FALSE)
	ENDIF
ENDPROC

/// PURPOSE:
///   Reset Arena Wars TV global, remove the current playlist, and set TV channel to NONE.
PROC STOP_ARENA_WARS_TV_OFFICE_PLAYLIST()
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[AWTV] STOP_ARENA_WARS_TV_PLAYLIST Setting g_iArenaWarTvPlaylistJobIndex from ",g_iArenaWarTvPlaylistJobIndex," to -1")
	CLEAR_ARENA_WARS_TV_OFFICE_PLAYLIST()
	CLEAR_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_2)
	SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
ENDPROC

PROC _SCALEFORM_UPDATE_WALL_TEXTURES(INT iArenaJobIndex, SCALEFORM_RENDER_TO_RT_DATA& ref_renderInfo)
	ref_renderInfo.iArenaJobIndex = iArenaJobIndex
	TEXT_LABEL_63 txt63_texDict
	BUILD_CAREER_WALL_TEXTURE_NAME(iArenaJobIndex, txt63_texDict)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(ref_renderInfo.sScaleform, "SET_TEXTURES")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(txt63_texDict)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ACW_NOTE_1")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ACW_NOTE_2")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("ACW_NOTE_3")
	END_SCALEFORM_MOVIE_METHOD()

	PRINTLN("[ARENA_CAREER_WALL][STATS] *texture dictionary* ", txt63_texDict)	
ENDPROC

PROC _SCALEFORM_APPLY_WALL_CHANGES(SCALEFORM_RENDER_TO_RT_DATA& ref_renderInfo)
	BEGIN_SCALEFORM_MOVIE_METHOD(ref_renderInfo.sScaleform, "SHOW_SCREEN")
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC _SCALEFORM_METHOD_HIDE_UNLOCK(SCALEFORM_INDEX scaleform, INT iSticker)
	PRINTLN("[ARENA_CAREER_WALL][STATS] Removing sticker ", iSticker)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleform, "HIDE_UNLOCK")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSticker)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC _SCALEFORM_METHOD_SHOW_UNLOCK(SCALEFORM_INDEX scaleform, INT iSticker)
	PRINTLN("[ARENA_CAREER_WALL][STATS] Adding sticker ", iSticker)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleform, "SHOW_UNLOCK")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSticker)
	END_SCALEFORM_MOVIE_METHOD()
ENDPROC

PROC _REMOVE_ALL_STICKERS(SCALEFORM_INDEX scaleform)
	INT i
	FOR i = ci_ACW_STICKER_FIRST to ci_ACW_STICKER_LAST
		_SCALEFORM_METHOD_HIDE_UNLOCK(scaleform, i)
	ENDFOR	
ENDPROC

PROC _ADD_WALL_STICKERS(SCALEFORM_INDEX scaleform, INT iBsTrinkets)
	// Stickers B* 5491918
	INT i
	FOR i = ci_ACW_STICKER_FIRST to ci_ACW_STICKER_LAST
		IF IS_BIT_SET(iBsTrinkets, i)
			_SCALEFORM_METHOD_SHOW_UNLOCK(scaleform, i)
		ENDIF
	ENDFOR
ENDPROC

PROC _SCALEFORM_METHOD_SET_STATS(SCALEFORM_INDEX scaleform, STRING sName, 
								INT iSkillLevel, INT iTotalArenaPoints,
								INT iArenaPoints, INT iTier, INT iNumGames, 
								INT iNumWins, INT iKills, INT iDeaths,
								INT iSpectatorKills, INT iBestJobType, INT iWorstJobType,
								MODEL_NAMES eFavArenaVehicle)

	// Some stats are just derived
	FLOAT fTotalPointsForPrevTier 	= TO_FLOAT(GET_TOTAL_ARENA_CAREER_POINTS_REQUIRERD_TO_REACH_TIER(iTier))
	FLOAT fTotalPointsForNextTier	= TO_FLOAT(GET_TOTAL_ARENA_CAREER_POINTS_REQUIRERD_TO_REACH_TIER(iTier + 1))
	INT iTotalPointsForThisTier 	= ROUND(fTotalPointsForNextTier - fTotalPointsForPrevTier)
	INT iArenaPointsLeft			= iTotalPointsForThisTier - iArenaPoints		 
	FLOAT fTierProgress		 		= (100.0 * iArenaPoints) / iTotalPointsForThisTier
	INT iNumLosses					= iNumGames - iNumWins
	
	TEXT_LABEL_15 txt15_rawRank = GET_ARENA_SKILL_LEVEL_TITLE(iSkillLevel)

	// String look ups
	STRING sMostWins = GET_ARENA_JOB_TYPE_LABEL(iBestJobType)
	STRING sMostLosses = GET_ARENA_JOB_TYPE_LABEL(iWorstJobType)

	// Fav vehicle
	TEXT_LABEL_15 txt15_favVeh
	IF eFavArenaVehicle = DUMMY_MODEL_FOR_SCRIPT
		txt15_favVeh = "ARN_VEH_NONE" // "-"
	ELSE
		txt15_favVeh = GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(eFavArenaVehicle)
	ENDIF
	
	//https://hub.rockstargames.com/display/RSGGTAV/Arena+Career+Wall
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleform, "SET_STATS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sName)
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING("ACW_RANK_CONCAT")
			ADD_TEXT_COMPONENT_INTEGER(iSkillLevel)
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(txt15_rawRank)		
		END_TEXT_COMMAND_SCALEFORM_STRING()		
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTotalArenaPoints)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) 						// Unused: num items unlocked
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iArenaPointsLeft)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTier)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTierProgress)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNumGames)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNumWins)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iNumLosses)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iKills)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDeaths)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSpectatorKills)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(txt15_favVeh)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING("") 			// Unused: fav maode
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sMostWins)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sMostLosses)
	END_SCALEFORM_MOVIE_METHOD()
	
	#IF IS_DEBUG_BUILD
	PRINTLN("[ARENA_CAREER_WALL][STATS] CAREER_WALL.SET_STATS params: ")
	PRINTLN(" gamername          ", sName)
	PRINTLN(" rank               ", iTier)
	PRINTLN(" totalArenaPoints   ", iTotalArenaPoints)
	PRINTLN(" noLongerUsed1      DEPRECATED")
	PRINTLN(" arenaPoints        ", iArenaPoints)
	PRINTLN(" currTier           ", iTier)
	PRINTLN(" currTierProgress   ", fTierProgress)
	PRINTLN(" gamesPlayed        ", iNumGames)
	PRINTLN(" wins               ", iNumWins)
	PRINTLN(" losses             ", iNumLosses)
	PRINTLN(" kills              ", iKills)
	PRINTLN(" deaths             ", iDeaths)
	PRINTLN(" spectatorKills     ", iSpectatorKills)
	PRINTLN(" favouriteVehicle   ", txt15_favVeh)
	PRINTLN(" noLongerUsed2      DEPRECATED")
	PRINTLN(" bestMode           [", sMostWins, "] ", GET_STRING_FROM_TEXT_FILE(sMostWins))
	PRINTLN(" worstMode          [", sMostLosses, "] ", GET_STRING_FROM_TEXT_FILE(sMostLosses))	
	#ENDIF // IS_DEBUG_BUILD
ENDPROC

PROC _ARENA_CAREER_WALL_UPDATE_DATA(INT iArenaJobIndex, SCALEFORM_RENDER_TO_RT_DATA& ref_renderInfo, PLAYER_INDEX pOwner, INT iBsTrinkets)
	INT iOwner = NATIVE_TO_INT(pOwner)

	// Stats we're going to display
	STRING sGamerName 			= GET_PLAYER_NAME(pOwner)
	INT iSkillLevel 			= GET_PLAYERS_CURRENT_ARENA_WARS_SKILL_LEVEL(pOwner)
	INT iTotalArenaPoints 		= g_sArenaWarsStats[iOwner].iArenaPointsTotal
	INT iArenaPoints			= g_sArenaWarsStats[iOwner].iArenaPoints		 
	INT iTier 					= GET_PLAYERS_ARENA_WARS_CURRENT_TIER(pOwner) 
	INT iNumGames				= g_sArenaWarsStats[iOwner].iNumMatchesPlayedSolo + g_sArenaWarsStats[iOwner].iNumMatchesPlayedTeam
	INT iNumWins				= g_sArenaWarsStats[iOwner].iLifetimeWins + g_sArenaWarsStats[iOwner].iLifetimeWinsTeam
	INT iKills					= g_sArenaWarsStats[iOwner].iLifetimeKills
	INT iDeaths					= g_sArenaWarsStats[iOwner].iLifetimeDeaths
	INT iSpectatorKills			= g_sArenaWarsStats[iOwner].iSpectatorKills
	INT iBestJobType 			= GET_ARENA_JOB_TYPE_MOST_WINS(pOwner)
	INT iWorstJobtype 			= GET_ARENA_JOB_TYPE_MOST_LOSSES(pOwner)
	MODEL_NAMES eFavVeh 		= INT_TO_ENUM(MODEL_NAMES, GET_ARENA_FAV_VEH(pOwner))
	
	_SCALEFORM_METHOD_SET_STATS(ref_renderInfo.sScaleform, sGamerName, iSkillLevel, 
								iTotalArenaPoints, iArenaPoints, iTier, iNumGames, iNumWins,
								iKills, iDeaths, iSpectatorKills, iBestJobType, iWorstJobtype, eFavVeh)
	
	IF iArenaJobIndex >= 0
		_SCALEFORM_UPDATE_WALL_TEXTURES(iArenaJobIndex, ref_renderInfo)
	ELSE	
		PRINTLN("[ARENA_CAREER_WALL][STATS] iArenaJobIndex is invalid! ", iArenaJobIndex)
	ENDIF
	
	_ADD_WALL_STICKERS(ref_renderInfo.sScaleform, iBsTrinkets)
	
	_SCALEFORM_APPLY_WALL_CHANGES(ref_renderInfo)
ENDPROC

PROC DRAW_SCALEFORM_TO_RT(ARENA_CAREER_WALL& ref_local)
	IF IS_UNSAFE_TO_RENDER_TO_RT()
		PRINTLN("[ARENA_CAREER_WALL] IS_UNSAFE_TO_RENDER_TO_RT FAILED - see above.")
		EXIT
	ENDIF	
		
	IF ref_local.iArenaJobIndex = -1
		PRINTLN("[ARENA_CAREER_WALL] DRAW_SCALEFORM_TO_RT iArenaJobIndex is invalid! ", ref_local.iArenaJobIndex)
		EXIT
	ENDIF
	
	IF ref_local.drawDataWall.iArenaJobIndex <> ref_local.iArenaJobIndex 
	AND ref_local.eState <> ACWS_MOCAP_SCENE
		_SCALEFORM_UPDATE_WALL_TEXTURES(ref_local.iArenaJobIndex, ref_local.drawDataWall)
		_SCALEFORM_APPLY_WALL_CHANGES(ref_local.drawDataWall)
	ENDIF
	
	IF IS_NAMED_RENDERTARGET_LINKED(ref_local.drawDataWall.eWallProp)
		SET_TEXT_RENDER_ID(ref_local.drawDataWall.iRenderTargetId)
		SET_SCALEFORM_MOVIE_TO_USE_SUPER_LARGE_RT(ref_local.drawDataWall.sScaleform, TRUE)
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
		SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
		DRAW_SCALEFORM_MOVIE(ref_local.drawDataWall.sScaleform,
							cf_WALL_CENTRE_X, cf_WALL_CENTRE_Y,
							cf_WALL_WIDTH, cf_WALL_HEIGHT,
							255, 255, 255, 255)
		SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
	ELSE
		PRINTLN("[ARENA_CAREER_WALL] DRAW_SCALEFORM_TO_RT Wall has become unlinked - interior streaming out?")
	ENDIF
ENDPROC

PROC _ARENA_CAREER_WALL_CLEAR_BLIP(ARENA_CAREER_WALL& ref_local)
	IF DOES_BLIP_EXIST(ref_local.blip)
		PRINTLN("[ARENA_CAREER_WALL] Removing blip")
		REMOVE_BLIP(ref_local.blip)
	ENDIF
ENDPROC

PROC _ARENA_CAREER_WALL_MAINTIAN_BLIP(ARENA_CAREER_WALL& ref_local)
	IF IS_PLAYER_IN_ZOOMED_OUT_RADAR_OR_MAP(PLAYER_ID())
		IF DOES_BLIP_EXIST(ref_local.blip)
			REMOVE_BLIP(ref_local.blip)
		ENDIF
	ELSE
		IF NOT DOES_BLIP_EXIST(ref_local.blip)
			PRINTLN("[ARENA_CAREER_WALL] Creating blip")
			ref_local.blip = CREATE_BLIP_FOR_COORD(<<204.8143, 5161.3589, -86.2309>>)
			SET_BLIP_SPRITE(ref_local.blip, RADAR_TRACE_ARENA_SERIES)
			SET_BLIP_NAME_FROM_TEXT_FILE(ref_local.blip, "AW_WALL_BLIP")
			SET_BLIP_SCALE(ref_local.blip, 1.0)	
			SET_BLIP_DISPLAY(ref_local.blip, DISPLAY_BOTH)
		ENDIF
	ENDIF
ENDPROC

PROC _ARENA_CAREER_WALL_INIT(ARENA_CAREER_WALL& ref_local)
	PRINTLN("[ARENA_CAREER_WALL] _ARENA_CAREER_WALL_INIT")
	
	// Store owner's trinket bitset on entry
	ref_local.iBsTrinkets = GET_PLAYERS_ARENA_CAREER_WALL_TRINKET_BITSET(ref_local.pOwnerId)
	_ARENA_CAREER_WALL_UPDATE_DATA(ref_local.iArenaJobIndex, ref_local.drawDataWall, ref_local.pOwnerId, ref_local.iBsTrinkets)
		
	ref_local.bDoneOneTimeHelp = GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARENA_DONE_WALL_HELP)
	ref_local.iBonusHelpCount = GET_PACKED_STAT_INT(PACKED_MP_INT_ARENA_BONUS_HELP_COUNT)
	
	// Re-init TV playlist
	CLEAR_ARENA_WARS_TV_OFFICE_PLAYLIST()

	PRINTLN("[ARENA_CAREER_WALL] MUSIC : PREPARE_MUSIC_EVENT('AW_LOBBY_MUSIC_START')")
	PREPARE_MUSIC_EVENT("AW_LOBBY_MUSIC_START")
	
	// Tickers for new unlocks
	IF ref_local.pOwnerId = PLAYER_ID()
		INT iBsTrinketTickers = GET_MP_INT_CHARACTER_STAT(MP_STAT_ARN_BS_TRINKET_TICKERS)
		PRINTLN("[ARENA_CAREER_WALL] TICKERS : Tickers bitset ", iBsTrinketTickers)
		PRINTLN("[ARENA_CAREER_WALL] TICKERS : Local bitset ", ref_local.iBsTrinkets)
		
		INT i
		FOR i = 0 to ci_ACW_TRINKET_LAST
			IF IS_BIT_SET(ref_local.iBsTrinkets, i)
			AND NOT IS_BIT_SET(iBsTrinketTickers, i)
				TEXT_LABEL_15 txt15_ticker = "ACW_TRINKET_"
				txt15_ticker += i
				PRINT_TICKER(txt15_ticker)
				PRINTLN("[ARENA_CAREER_WALL] TICKERS : Showing ", txt15_ticker)
			ENDIF
		ENDFOR
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ARN_BS_TRINKET_TICKERS, ref_local.iBsTrinkets)
	ENDIF
	
	IF IS_PLAYER_WATCHING_ARENA_INTRO_2_CUTSCENE(PLAYER_ID())
		_ARENA_CAREER_WALL_SET_STATE(ref_local, ACWS_MOCAP_SCENE)
	ELSE 
		_ARENA_CAREER_WALL_SET_STATE(ref_local, ACWS_WAIT_FOR_TRIGGER)
	ENDIF
ENDPROC

/// PURPOSE:
/// Checks to see if an entity is in the trigger area for the arena career wall and is facing the wall. 
FUNC BOOL ARENA_CAREER_WALL_IS_ENTITY_IN_TRIGGER_AREA(ENTITY_INDEX entity)
	// Check heading first
	FLOAT fHeading = GET_ENTITY_HEADING(entity)
	IF fHeading < 110
	OR fHeading > 250
		RETURN FALSE
	ENDIF
	
	RETURN IS_ENTITY_AT_COORD(entity, <<205.027893,5162.085938,-85.600708>>,<<2.000000,1.000000,1.000000>>)
ENDFUNC

FUNC BOOL ARENA_CAREER_WALL_IS_ENTITY_IN_OFFICE_AREA(ENTITY_INDEX entity)
	IF DOES_ENTITY_EXIST(entity)
		RETURN IS_ENTITY_IN_ANGLED_AREA(entity, <<212.147110,5164.661621,-90.198135>>, <<193.873795,5164.686035,-82.097267>>, 8.4)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL _ARENA_CAREER_WALL_IS_SAFE_TO_USE_WALL()
	RETURN //IS_SAFE_TO_START_CLUBHOUSE_MISSION_MENU() // <-- this requires you are also biker gang boss so no longer using
		NOT IS_PLAYER_IN_CORONA()
		AND IS_SKYSWOOP_AT_GROUND()
		AND NOT AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
		AND NOT IS_PHONE_ONSCREEN()
		AND NOT Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		AND NOT Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
		AND (MPGlobals.PlayerInteractionData.iTargetPlayerInt = -1) // interaction menu open
		AND NOT IS_BROWSER_OPEN() //browser open
		AND NOT IS_SELECTOR_ONSCREEN()
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		AND NOT IS_CUTSCENE_ACTIVE()
		AND (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) <> FMMC_TYPE_GB_CONTRABAND_SELL)
		AND NOT IS_ANY_GB_BIG_MESSAGE_BEING_DISPLAYED()
		AND NOT SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER()
		AND GET_FM_JOB_ENTERY_TYPE() <> ciMISSION_ENTERY_TYPE_ARENA_SERIES_CORONA_WALL
		AND GET_FM_JOB_ENTERY_TYPE() <> ciMISSION_ENTERY_TYPE_ARENA_SERIES_CORONA_WALL_ON_CALL
		AND NOT IS_PAUSE_MENU_ACTIVE_EX()
ENDFUNC


PROC _ARENA_CAREER_WALL_SET_GLOBAL(BOOL bUsingWall)
	#IF IS_DEBUG_BUILD
	IF IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_ARENA_USING_CAREER_WALL)	
		IF NOT bUsingWall
			PRINTLN("[ARENA_CAREER_WALL] Setting global flag FALSE")
		ENDIF
	ELSE
		IF bUsingWall
			PRINTLN("[ARENA_CAREER_WALL] Setting global flag TRUE")
		ENDIF
	ENDIF	
	#ENDIF //IS_DEBUG_BUILD
		
	ENABLE_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iBSTwo, BSTWO_ARENA_USING_CAREER_WALL, bUsingWall)
ENDPROC

PROC HIDE_ALL_PLAYERS_THIS_FRAME()
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i), FALSE)		
			SET_PLAYER_INVISIBLE_LOCALLY(INT_TO_PLAYERINDEX(i))
		ENDIF
	ENDREPEAT
ENDPROC	

PROC ARENA_CAREER_WALL_INIT_PC_SCRIPTED_CONTROLS(ARENA_CAREER_WALL& ref_local)
	IF ref_local.bLoadedPcControls
		PRINTLN("[ARENA_CAREER_WALL] CONTROLS ! : Already loaded")
	ELSE	
		PRINTLN("[ARENA_CAREER_WALL] CONTROLS : Loading 'Career_Wall'")
		INIT_PC_SCRIPTED_CONTROLS("Career_Wall")
		ref_local.bLoadedPcControls = TRUE
	ENDIF
ENDPROC

PROC ARENA_CAREER_WALL_SHUTDOWN_PC_SCRIPTED_CONTROLS(ARENA_CAREER_WALL& ref_local)
	IF ref_local.bLoadedPcControls
		PRINTLN("[ARENA_CAREER_WALL] CONTROLS : Shutdown controls")
		SHUTDOWN_PC_SCRIPTED_CONTROLS()
		ref_local.bLoadedPcControls = FALSE
	ELSE
		PRINTLN("[ARENA_CAREER_WALL] CONTROLS ! : Already shutdown")
	ENDIF
ENDPROC
	
PROC _ARENA_CAREER_WALL_STATE_WAIT_FOR_TRIGGER(ARENA_CAREER_WALL& ref_local)
	// Need to do this because we're using the clubhouse wall 
	// header which defines some tweak_ values we  don't use here
	#IF IS_DEBUG_BUILD
	unused_parameter(fCONST_CLUBHOUSE_MISSION_TRIGGER_RADIUS)
	#ENDIF

	PLAYER_INDEX piLocal = PLAYER_ID()
	IF piLocal <> ref_local.pOwnerId
		EXIT // B*5486200 Now only the owner can use the board.
	ENDIF
	
	_ARENA_CAREER_WALL_MAINTIAN_BLIP(ref_local)
	
	PED_INDEX pedLocal = GET_PLAYER_PED(piLocal)
	IF _ARENA_CAREER_WALL_IS_SAFE_TO_USE_WALL()
	AND ARENA_CAREER_WALL_IS_ENTITY_IN_TRIGGER_AREA(pedLocal)	

		// Players can't launch jobs in solo
		IF NETWORK_SESSION_IS_SOLO()
			PRINTLN("[ARENA_CAREER_WALL] NETWORK_SESSION_IS_SOLO")
			PRINT_HELP("ACW_SOLO")
			EXIT
		ENDIF
	
		IF IS_ARENA_WARS_SERIES_HIDDEN()
			PRINTLN("[ARENA_CAREER_WALL] IS_ARENA_WARS_SERIES_HIDDEN")
			
			IF FM_EVENT_IS_PLAYER_CRITICAL_TO_FM_EVENT(PLAYER_ID())
			AND FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_BUSINESS_BATTLE(PLAYER_ID()) 
				PRINT_HELP("ACW_BATTLE")
			ELSE
				PRINT_HELP("ACW_HIDDEN")
			ENDIF

			EXIT
		ENDIF
	
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ACW_START")
			// B* url:bugstar:5501983 - Career Wall - Not getting the prompt as there is a message staying on screen
			PRINTLN("[ARENA_CAREER_WALL] Clearing help for arena career wall")
			CLEAR_HELP()
		ENDIF
		
		IF ref_local.iTriggerContext = NEW_CONTEXT_INTENTION
			REGISTER_CONTEXT_INTENTION(ref_local.iTriggerContext, CP_MAXIMUM_PRIORITY, "ACW_START")
		ELIF HAS_CONTEXT_BUTTON_TRIGGERED(ref_local.iTriggerContext)
			PRINTLN("[ARENA_CAREER_WALL] Context accepted")
			RELEASE_CONTEXT_INTENTION(ref_local.iTriggerContext)
			CLEAR_HELP(TRUE)
			
			_ARENA_CAREER_WALL_CLEAR_BLIP(ref_local)
			
			// Start cam looking at... 
			ref_locaL.cam.eView = ACWV_PANEL_BOARD_INTRO
			ref_local.cam.v2StoredGridPos.x = 0
			ref_local.cam.v2StoredGridPos.y = 0
			ref_local.cam.v2GridPos = ref_local.cam.v2StoredGridPos
			
			_ARENA_CAREER_WALL_GET_CAM_TRANSFORM(ref_local.cam.eView, ref_local.cam.sStoredOffset.vCoord, ref_local.cam.sStoredOffset.vPoint)
			
			ref_local.cam.sCurrentOffset = ref_local.cam.sStoredOffset
			ref_local.cam.sTargetOffset = ref_local.cam.sStoredOffset
						
			CAMERA_INDEX ciCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)		
			ref_local.cam.ciCam = ciCam
			SET_CAM_COORD(ciCam, ref_local.cam.sCurrentOffset.vCoord)
			POINT_CAM_AT_COORD(ciCam, ref_local.cam.sCurrentOffset.vPoint)
			SET_CAM_FOV(ref_local.cam.ciCam, F_CAM_FOV)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			SHAKE_CAM(ciCam, "HAND_SHAKE", CONST_BROWSE_CAM_SHAKE)
			
			HIDE_ALL_PLAYERS_THIS_FRAME()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
			ARENA_CAREER_WALL_INIT_PC_SCRIPTED_CONTROLS(ref_local)
						
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARENA_DONE_WALL_TUT)
				_ARENA_CAREER_WALL_SET_STATE(ref_local, ACWS_LOOK_AT_WALL)
			ELSE
				ref_local.iTutorialStage = 0
				START_NET_TIMER(ref_local.tutorialTimer)
				_ARENA_CAREER_WALL_SET_STATE(ref_local, ACWS_DO_TUTORIAL)						
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF ref_local.iTriggerContext <> NEW_CONTEXT_INTENTION
			PRINTLN("[ARENA_CAREER_WALL] No longer in trigger area, clearing context")
		ENDIF	
		#ENDIF
		RELEASE_CONTEXT_INTENTION(ref_local.iTriggerContext)
	ENDIF		
ENDPROC

FUNC CONTROL_ACTION _GET_CONTROL_ACTION_BACK()
	#IF IS_JAPANESE_BUILD
		RETURN INPUT_SCRIPT_RDOWN
	#ENDIF
	
	#IF NOT IS_JAPANESE_BUILD
		RETURN INPUT_SCRIPT_RRIGHT
	#ENDIF
ENDFUNC

FUNC CONTROL_ACTION _GET_CONTROL_ACTION_ACCEPT()
	#IF IS_JAPANESE_BUILD
		RETURN INPUT_SCRIPT_RRIGHT
	#ENDIF
	
	#IF NOT IS_JAPANESE_BUILD
		RETURN INPUT_SCRIPT_RDOWN
	#ENDIF
ENDFUNC

// @@ CHANGE TO SCRIPT CONTROLS FROM FRONTEND?
PROC _ARENA_CAREER_WALL_DRAW_INSTRUCTIONAL_BUTTONS(ARENA_CAREER_WALL& ref_local, BOOL bKeyPressSafe, BOOL bDoingTutorial = FALSE)
	// Don't draw instructional buttons while on call / in lobby 
	// as this messes with their scaleforms
	IF IS_PLAYER_IN_CORONA()
		EXIT
	ENDIF
	
	BOOL bMenuAssetsLoaded = LOAD_MENU_ASSETS()
	IF NOT bMenuAssetsLoaded
		ref_local.bLoadedButtonHelp = FALSE
	ENDIF
	
	IF NOT ref_local.bLoadedButtonHelp
		IF bKeyPressSafe
		AND bMenuAssetsLoaded
			PRINTLN("[ARENA_CAREER_WALL] _ARENA_CAREER_WALL_DRAW_INSTRUCTIONAL_BUTTONS updating UI")
			
			REMOVE_MENU_HELP_KEYS()
			IF ref_local.eState = ACWS_LEADERBOARD
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_SCRIPT_RB, "ACW_NXT_LB")
				ADD_MENU_HELP_KEY_CLICKABLE(_GET_CONTROL_ACTION_BACK(), "ITEM_BACK")
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_SELECT,"SCLB_PROFILE")
			ELIF bDoingTutorial
				ADD_MENU_HELP_KEY_CLICKABLE(_GET_CONTROL_ACTION_ACCEPT(), "ACW_SKIP_TUT")
			ELSE
				IF ref_local.pOwnerId = PLAYER_ID()
					ADD_MENU_HELP_KEY_CLICKABLE(_GET_CONTROL_ACTION_ACCEPT(), "ACW_JOIN_SRS")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_SCRIPT_RLEFT, "ACW_ON_CALL")
				ENDIF
				
				ADD_MENU_HELP_KEY_CLICKABLE(_GET_CONTROL_ACTION_BACK(), "ITEM_BACK")
				ADD_MENU_HELP_KEY_GROUP(INPUTGROUP_LOOK, "ACW_LOOK")
				
				SWITCH ref_local.cam.eView
					CASE ACWV_PANEL_BOARD_BL
					CASE ACWV_PANEL_BOARD_BR
					CASE ACWV_PANEL_BOARD_TL
					CASE ACWV_PANEL_BOARD_TR
					CASE ACWV_PANEL_BOARD_C
					CASE ACWV_PANEL_BOARD_INTRO
						ADD_MENU_HELP_KEY_CLICKABLE(INPUT_SCRIPT_RT, GET_VIEW_LABEL(_ARENA_CAREER_WALL_GET_PANEL_FROM_INPUT(ref_local, NAVIGATION_INPUT_OVERVIEW)))
						ADD_MENU_HELP_KEY_CLICKABLE(GET_CH_MISSION_ZOOM_INPUT(), "ITEM_ZOOM")
						ADD_MENU_HELP_KEY_GROUP(INPUTGROUP_MOVE, "IB_NAVIGATE")
						BREAK
					CASE ACWV_PANEL_TROPHY
						ADD_MENU_HELP_KEY_CLICKABLE(INPUT_SCRIPT_RT, GET_VIEW_LABEL(_ARENA_CAREER_WALL_GET_PANEL_FROM_INPUT(ref_local, NAVIGATION_INPUT_OVERVIEW)))
						ADD_MENU_HELP_KEY_CLICKABLE(GET_CH_MISSION_ZOOM_INPUT(), "ITEM_ZOOM")
						BREAK
					CASE ACWV_PANEL_TV
						ADD_MENU_HELP_KEY_CLICKABLE(INPUT_SCRIPT_RT, GET_VIEW_LABEL(_ARENA_CAREER_WALL_GET_PANEL_FROM_INPUT(ref_local, NAVIGATION_INPUT_OVERVIEW)))
						ADD_MENU_HELP_KEY_CLICKABLE(INPUT_SCRIPT_RUP, "ACW_LEADERBOARD")
						ADD_MENU_HELP_KEY_CLICKABLE(GET_CH_MISSION_ZOOM_INPUT(), "ITEM_ZOOM")
						BREAK
					CASE ACWV_PANEL_OVERVIEW
						ADD_MENU_HELP_KEY_CLICKABLE(GET_CH_MISSION_ZOOM_INPUT(), "ITEM_ZOOM")
						BREAK
				ENDSWITCH
				
				ARENA_CAREER_WALL_VIEW eNavView = _ARENA_CAREER_WALL_GET_PANEL_FROM_INPUT(ref_local, NAVIGATION_INPUT_F)
				IF eNavView <> ACWV_NONE
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_SCRIPT_RB, GET_VIEW_LABEL(eNavView))
				ENDIF
				eNavView = _ARENA_CAREER_WALL_GET_PANEL_FROM_INPUT(ref_local, NAVIGATION_INPUT_B)
				IF eNavView <> ACWV_NONE
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_SCRIPT_LB, GET_VIEW_LABEL(eNavView))
				ENDIF		

			ENDIF
			
			ref_local.bLoadedButtonHelp = TRUE
		ENDIF
	ENDIF
	
	IF bKeyPressSafe
	AND bMenuAssetsLoaded
		SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
		SET_MENU_HELP_CLEAR_SPACE(0.0)
		DRAW_MENU_HELP_SCALEFORM(0)
	ENDIF
ENDPROC

FUNC BOOL _ARENA_CAREER_WALL_CAN_ACCEPT_INPUTS()
	IF IS_PAUSE_MENU_ACTIVE_EX()
	OR SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER()
	OR IS_SYSTEM_UI_BEING_DISPLAYED()
	OR IS_WARNING_MESSAGE_ACTIVE()
	OR NETWORK_TEXT_CHAT_IS_TYPING()
	OR g_sShopSettings.bProcessStoreAlert
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

	// Taken from net_clubhouse_private.sch...
TWEAK_INT ci_ACW_MOVE_DELAY	0
TWEAK_INT ci_ACW_MOVE_TIME	550

FUNC FLOAT _ARENA_CAREER_WALL_GET_SCROLL_TIME_FACTOR(SCRIPT_TIMER timer)
	RETURN CLAMP(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), GET_TIME_OFFSET(timer.Timer, ci_ACW_MOVE_DELAY)) / TO_FLOAT(ci_ACW_MOVE_TIME - ci_ACW_MOVE_DELAY), 0, 1)
ENDFUNC

/// PURPOSE:
///    Get cardinal direction from 2D input vector. (x, y) should be a normalized vector.
FUNC NAVIGATION_INPUT GET_NAVIGATION_INPUT_FROM_INPUT_VECTOR(FLOAT x, FLOAT y)
	IF VMAG2(<<x, y, 0.0>>) < 0.01
		RETURN NAVIGATION_INPUT_NONE
	ENDIF
	
	FLOAT fAngle = ATAN2(y, x)
	IF fAngle > -22.5
		IF fAngle <= 22.5
			RETURN NAVIGATION_INPUT_E
		ELIF fAngle <= 67.5 
			RETURN NAVIGATION_INPUT_SE
		ELIF fAngle <= 112.55
			RETURN NAVIGATION_INPUT_S
		ELIF fAngle <= 157.5
			RETURN NAVIGATION_INPUT_SW
		ELSE
			RETURN NAVIGATION_INPUT_W
		ENDIF
	ELSE		
		IF fAngle < -157.5
			RETURN NAVIGATION_INPUT_W
		ELIF fAngle < -112.5
			RETURN NAVIGATION_INPUT_NW
		ELIF fAngle < -67.5
			RETURN NAVIGATION_INPUT_N
		ELSE
			RETURN NAVIGATION_INPUT_NE
		ENDIF
	ENDIF
ENDFUNC
//*/ NEW CONTROL SCHEME
PROC SETUP_ARENA_MENU_CONTROLS(ARENA_CAREER_WALL_INPUT& ref_input, BOOL bIAmOwner, ARENA_CAREER_WALL_CAMERA& ref_cam)
		
	ref_input.base.iLeftX = FLOOR(GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) * 127)
	ref_input.base.iLeftY = FLOOR(GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD) * 127)
	ref_input.base.iRightX = FLOOR(GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X) * 127)
	ref_input.base.iRightY = FLOOR(GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y) * 127)
	
	// Apply PC movements cumulatively (clamped to normal min/max of 127)
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
	 	FLOAT fMouseSensitivity = 0.2
		FLOAT fMouseX
		FLOAT fMouseY
		
		fMouseX = GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_CURSOR_X) 
		fMouseY = GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_CURSOR_Y)
		
		ref_input.base.fPrevMouseX = fMouseX
		ref_input.base.fPrevMouseY = fMouseY

		// Get look input direct from LR/UD inputs, so they take account of mouse sensitivity and all inversions
		ref_input.base.iRightX = ROUND((GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_LR) * fMouseSensitivity) * 127.0)
		ref_input.base.iRightY = ROUND((GET_DISABLED_CONTROL_UNBOUND_NORMAL(PLAYER_CONTROL, INPUT_SCALED_LOOK_UD) * fMouseSensitivity) * 127.0)
		
		ref_input.base.iRightX = CLAMP_INT(ref_input.base.iRightX + ref_input.base.iPrevRightX[0], -127, 127)
		ref_input.base.iRightY = CLAMP_INT(ref_input.base.iRightY + ref_input.base.iPrevRightY[0], -127, 127)
	ENDIF
	
	IF ref_input.bCanScroll
		IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
			ref_input.eNavInput = NAVIGATION_INPUT_E
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_DOWN)
			ref_input.eNavInput = NAVIGATION_INPUT_S
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
			ref_input.eNavInput = NAVIGATION_INPUT_W
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_UP)
			ref_input.eNavInput = NAVIGATION_INPUT_N
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_LB)
			ref_input.eNavInput = NAVIGATION_INPUT_B
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RB)
			ref_input.eNavInput = NAVIGATION_INPUT_F
		ELIF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RT) 
			ref_input.eNavInput = NAVIGATION_INPUT_OVERVIEW
		ELIF ref_cam.eView = ACWV_PANEL_OVERVIEW
		AND NOT IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RT) 
			ref_input.eNavInput = NAVIGATION_INPUT_DETAILS
		ELSE				
			VECTOR vLeftNorm = NORMALISE_VECTOR(<<ref_input.base.iLeftX, ref_input.base.iLeftY, 0>>)
			ref_input.eNavInput = GET_NAVIGATION_INPUT_FROM_INPUT_VECTOR(vLeftNorm.x, vLeftNorm.y)	
		ENDIF	
	
		IF ref_input.eNavInput <> NAVIGATION_INPUT_NONE
			REINIT_NET_TIMER(ref_input.scrollTimer)
		ENDIF
	ELSE
		ref_input.eNavInput = NAVIGATION_INPUT_NONE
	ENDIF
	
	// Set the input flag states
	ref_input.base.bBack = IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, _GET_CONTROL_ACTION_BACK())
	IF bIAmOwner
		ref_input.bGoOnCall = IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RLEFT)
		ref_input.base.bAccept	= IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, _GET_CONTROL_ACTION_ACCEPT())
	ELSE
		ref_input.bGoOnCall = FALSE
		ref_input.base.bAccept = FALSE
	ENDIF
	
	// * Nudge section
	INT i
	CONST_FLOAT fCONST_RIGHT_STICK_MULT	0.0025
	
	ref_input.base.vCamNudge.x  = TO_FLOAT(ref_input.base.iRightX)
	REPEAT COUNT_OF(ref_input.base.iPrevRightX) i
		ref_input.base.vCamNudge.x += TO_FLOAT(ref_input.base.iPrevRightX[i])
	ENDREPEAT
	
	ref_input.base.vCamNudge.x /= COUNT_OF(ref_input.base.iPrevRightX) + 1
	ref_input.base.vCamNudge.x *= fCONST_RIGHT_STICK_MULT
	
	ref_input.base.vCamNudge.y = 0.0
	
	ref_input.base.vCamNudge.z  = TO_FLOAT(ref_input.base.iRightX)
	REPEAT COUNT_OF(ref_input.base.iPrevRightY) i
		ref_input.base.vCamNudge.z += TO_FLOAT(ref_input.base.iPrevRightY[i])
	ENDREPEAT
	ref_input.base.vCamNudge.z /= COUNT_OF(ref_input.base.iPrevRightY) + 1
	ref_input.base.vCamNudge.z *= fCONST_RIGHT_STICK_MULT
	
	IF IS_LOOK_INVERTED()
		ref_input.base.vCamNudge.z *= -1.0
	ENDIF
	
	IF LOAD_MENU_ASSETS()
	AND GET_PAUSE_MENU_STATE() = PM_INACTIVE
	AND NOT IS_SYSTEM_UI_BEING_DISPLAYED()
	AND NOT IS_WARNING_MESSAGE_ACTIVE()
	AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
	AND NOT g_sShopSettings.bProcessStoreAlert
	
		// Mouse control support  
		IF IS_PC_VERSION()	
		AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
			IF IS_USING_CURSOR(PLAYER_CONTROL)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)	
				ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
				ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
				ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
			ENDIF
		ENDIF
	ENDIF
	
	// R-stick history
	FOR i = COUNT_OF(ref_input.base.iPrevRightX)-1 TO 1 STEP -1
		ref_input.base.iPrevRightX[i] = ref_input.base.iPrevRightX[i-1]
		ref_input.base.iPrevRightY[i] = ref_input.base.iPrevRightY[i-1]
	ENDFOR
	ref_input.base.iPrevRightX[0] = ref_input.base.iRightX
	ref_input.base.iPrevRightY[0] = ref_input.base.iRightY
	
	// Handle zoom
	// 
	BOOL bZoomInput = FALSE
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		IF IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, GET_CH_MISSION_ZOOM_INPUT())
			bZoomInput = TRUE
		ENDIF
	ENDIF
	IF (IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, GET_CH_MISSION_ZOOM_INPUT()) OR bZoomInput) //input or PC zoom toggle...
		bZoomInput = TRUE
		IF ref_cam.fZoomAlpha < 1
			SET_CAM_SHAKE_AMPLITUDE(ref_cam.ciCam, (1-ref_cam.fZoomAlpha)*CONST_BROWSE_CAM_SHAKE)
			ref_cam.fZoomAlpha = ref_cam.fZoomAlpha + 0.05
		ENDIF
		IF ref_cam.fZoomAlpha > 1
			ref_cam.fZoomAlpha = 1
		ENDIF
	ELSE
		IF ref_cam.fZoomAlpha > 0
			SET_CAM_SHAKE_AMPLITUDE(ref_cam.ciCam, (1-ref_cam.fZoomAlpha)*CONST_BROWSE_CAM_SHAKE)
			ref_cam.fZoomAlpha = ref_cam.fZoomAlpha - 0.05
		ENDIF
		IF ref_cam.fZoomAlpha < 0
			ref_cam.fZoomAlpha = 0
		ENDIF
	ENDIF
	
	IF ref_input.eNavInput <> NAVIGATION_INPUT_NONE
	OR ref_input.base.iRightX <> 0
	OR ref_input.base.iRightY <> 0
	OR bZoomInput
		REINIT_NET_TIMER(ref_input.idleTimer)
	ENDIF		
ENDPROC	

PROC _ARENA_CAREER_WALL_CLEANUP_FOR_LOBBY(ARENA_CAREER_WALL& ref_local)
	PRINTLN("[ARENA_CAREER_WALL] _ARENA_CAREER_WALL_CLEANUP_FOR_LOBBY")
	PLAYER_INDEX piLocal = PLAYER_ID()
	
	_ARENA_CAREER_WALL_SET_STATE(ref_local, ACWS_WAIT_FOR_TRIGGER)

	ARENA_CAREER_WALL_SHUTDOWN_PC_SCRIPTED_CONTROLS(ref_local)

	SET_PLAYER_VISIBLE_LOCALLY(piLocal)
	CLEANUP_MENU_ASSETS()
ENDPROC

PROC _ARENA_CAREER_WALL_STOP_USING_WALL(ARENA_CAREER_WALL& ref_local)
	PRINTLN("[ARENA_CAREER_WALL] _ARENA_CAREER_WALL_STOP_USING_WALL")
	PLAYER_INDEX piLocal = PLAYER_ID()
	
	IF NOT IS_PLAYER_TELEPORT_ACTIVE()
		NET_SET_PLAYER_CONTROL(piLocal, TRUE)
	ENDIF
	
	_ARENA_CAREER_WALL_SET_STATE(ref_local, ACWS_WAIT_FOR_TRIGGER)
	
	IF DOES_CAM_EXIST(ref_local.cam.ciCam)
		IF IS_CAM_ACTIVE(ref_local.cam.ciCam)
			SET_PLAYER_VISIBLE_LOCALLY(piLocal)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		ENDIF
		DESTROY_CAM(ref_local.cam.ciCam)
	ENDIF
	
	scLB_DisplayedData.bResetTriggered = TRUE
	CLEANUP_SOCIAL_CLUB_LEADERBOARD(ref_local.scLB_control,TRUE)
	ref_local.bSetupSCLeaderboardData = FALSE
	
	ARENA_CAREER_WALL_SHUTDOWN_PC_SCRIPTED_CONTROLS(ref_local)
	g_bForceStopUsingCareerWall = FALSE
	CLEANUP_MENU_ASSETS()
ENDPROC

PROC _ARENA_CAREER_WALL_SET_VIEW(ARENA_CAREER_WALL& ref_local, ARENA_CAREER_WALL_VIEW eNewView)
	PRINTLN("[ARENA_CAREER_WALL] _ARENA_CAREER_WALL_SET_VIEW ", ref_local.cam.eView, " --> ", eNewView)
	// Save prev view details
	ref_local.cam.ePrevView = ref_local.cam.eView
	IF _ARENA_CAREER_WALL_VIEW_IS_BOARD(ref_local.cam.eView)
		ref_local.cam.ePrevBoardView = ref_local.cam.eView
	ENDIF		
	ref_local.cam.sStoredOffset = ref_local.cam.sCurrentOffset
	ref_local.cam.v2StoredGridPos = ref_local.cam.v2GridPos
	// Set new view
	ref_local.cam.eView = eNewView
	_ARENA_CAREER_WALL_GET_CAM_TRANSFORM(eNewView, ref_local.cam.sTargetOffset.vCoord, ref_local.cam.sTargetOffset.vPoint)
	_FORCE_UI_UPDATE(ref_local)
	PLAY_SOUND_FRONTEND(-1, "Highlight_Move_Up_Down", "DLC_AW_Arena_Office_Planning_Wall_Sounds")
ENDPROC

PROC _ARENA_CAREER_WALL_MAINTAIN_CORE(ARENA_CAREER_WALL& ref_local, BOOL bKeyPressSafe = FALSE)

	VECTOR vNudge = <<0, 0, 0>>
	IF bKeyPressSafe
		vNudge = ref_local.input.base.vCamNudge
	ENDIF
	VECTOR vCurrentNudgedPointOffset = ref_local.cam.sCurrentOffset.vPoint - vNudge // Clubhouse uses -, so we do too

	SET_CAM_COORD(ref_local.cam.ciCam, ref_local.cam.sCurrentOffset.vCoord)
	POINT_CAM_AT_COORD(ref_local.cam.ciCam, vCurrentNudgedPointOffset)

	FLOAT fTimeFactor = _ARENA_CAREER_WALL_GET_SCROLL_TIME_FACTOR(ref_local.input.scrollTimer)
	IF fTimeFactor = 0.0
		ref_local.cam.sStoredOffset.vCoord = ref_local.cam.sCurrentOffset.vCoord
		ref_local.cam.sStoredOffset.vPoint = ref_local.cam.sCurrentOffset.vPoint
	ENDIF

	FLOAT fInterp = GET_GRAPH_TYPE_SIN_ACCEL_DECEL(fTimeFactor)
	
	VECTOR vPosOffset  = <<cf_WALL_CAM_OFFSET_X, cf_WALL_CAM_OFFSET_Y, cf_WALL_CAM_OFFSET_Z>>
	VECTOR vCoordDiff = ref_local.cam.sTargetOffset.vCoord + vPosOffset - ref_local.cam.sStoredOffset.vCoord
	ref_local.cam.sCurrentOffset.vCoord = ref_local.cam.sStoredOffset.vCoord + (vCoordDiff * fInterp)

	VECTOR vLookAtOffset = <<cf_WALL_CAM_LOOKAT_X, cf_WALL_CAM_LOOKAT_Y, cf_WALL_CAM_LOOKAT_Z>>
	VECTOR vPointDiff = ref_local.cam.sTargetOffset.vPoint + vLookAtOffset - ref_local.cam.sStoredOffset.vPoint
	ref_local.cam.sCurrentOffset.vPoint = ref_local.cam.sStoredOffset.vPoint + (vPointDiff * fInterp)

	#IF IS_DEBUG_BUILD
	IF ref_local.debug.bDebugDraw
		IF NOT IS_CAM_RENDERING(GET_DEBUG_CAM())
			DRAW_DEBUG_SPHERE(vCurrentNudgedPointOffset, 0.1,
								128, 128, 000, 128)
			DRAW_DEBUG_TEXT_WITH_OFFSET("nudge", vCurrentNudgedPointOffset,	0,00,
										128, 128, 255)
			
			TEXT_LABEL_31 str31 = "iRight "
			str31 += ref_local.input.base.iRightX
			str31 += ", "
			str31 += ref_local.input.base.iRightY
			DRAW_DEBUG_TEXT_WITH_OFFSET(str31, vCurrentNudgedPointOffset,	0,10,
										128, 128, 255)
			
			INT iPrev
			REPEAT COUNT_OF(ref_local.input.base.iPrevRightY) iPrev
				str31  = "iRight["
				str31 += iPrev
				str31 += "] "
				str31 += ref_local.input.base.iPrevRightX[iPrev]
				str31 += ", "
				str31 += ref_local.input.base.iPrevRightY[iPrev]
				DRAW_DEBUG_TEXT_WITH_OFFSET(str31, vCurrentNudgedPointOffset, 0, (2+iPrev)*10,
											128, 128, 255)
			ENDREPEAT
		ENDIF
	ENDIF
	#ENDIF
	
	// Only allow scroll inputs when we've reached our current target
	ref_local.input.bCanScroll = ARE_VECTORS_ALMOST_EQUAL(ref_local.cam.sTargetOffset.vCoord + vPosOffset, ref_local.cam.sCurrentOffset.vCoord, 0.001)
	
	// Update zoom
	SET_CAM_FOV(ref_local.cam.ciCam, COSINE_INTERP_FLOAT(F_CAM_FOV,F_CAM_ZOOMED_FOV, ref_local.cam.fZoomAlpha))
ENDPROC

PROC _ARENA_CAREER_WALL_STATE_LOOK_AT_WALL(ARENA_CAREER_WALL& ref_local, CAMERA_INDEX &ciCam)
	
	BOOL bKeyPressSafe = _ARENA_CAREER_WALL_CAN_ACCEPT_INPUTS()

	_ARENA_CAREER_WALL_DRAW_INSTRUCTIONAL_BUTTONS(ref_local, bKeyPressSafe)

	IF ref_local.iBonusHelpCount < 3
	AND NOT IS_BIT_SET(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_ARENA_DONE_BONUS_HELP)
		PRINTLN("[ARENA_CAREER_WALL] Print 'ACW_TUT_BONUS' help")
		PRINT_HELP("ACW_TUT_BONUS", DEFAULT_HELP_TEXT_TIME)
		SET_BIT(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_ARENA_DONE_BONUS_HELP)
		ref_local.iBonusHelpCount++
		SET_PACKED_STAT_INT(PACKED_MP_INT_ARENA_BONUS_HELP_COUNT, ref_local.iBonusHelpCount)
	ENDIF
	

	// Control the camera
	//
	ref_local.cam.v2StoredGridPos = ref_local.cam.v2GridPos
	IF bKeyPressSafe
		SETUP_ARENA_MENU_CONTROLS(ref_local.input, ref_local.pOwnerId = PLAYER_ID(), ref_local.cam)
		
		IF ref_local.input.base.bBack
			PLAY_SOUND_FRONTEND(-1, "Highlight_Back", "DLC_AW_Arena_Office_Planning_Wall_Sounds")
			_ARENA_CAREER_WALL_STOP_USING_WALL(ref_local)
			EXIT
		ELIF ref_local.input.base.bAccept
			// Only process the input if we can launch
			INT iRootID = GET_ROCKSTAR_MISSION_FROM_ROOT_CONTENT_ID_HASH(g_sMPTunables.iFmCoronaPlaylistProffesionalRootContentId[CV2_GET_ARENA_SERIES_ARRAY()][ref_local.iArenaJobIndex])
			IF LAUNCH_NEXT_ARENA_SERIES_RACE(FALSE, TRUE, iRootID)
				PRINTLN("[ARENA_CAREER_WALL] Launching series...")
				PLAY_SOUND_FRONTEND(-1, "Highlight_Accept", "DLC_AW_Arena_Office_Planning_Wall_Sounds")	
				_ARENA_CAREER_WALL_CLEANUP_FOR_LOBBY(ref_local)
				IF GB_GET_NUM_GOONS_IN_LOCAL_GANG() <= 3
					SMV_FLOW_SET_LAUNCHING_SMV_IN_OFFICE()
				ENDIF
				SVM_FLOW_SET_TRANSITION_SESSIONS_LAUNCHING_SMV_FROM_LAPTOP()
				BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("FMMC_PLYLOAD")
				END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_LOADING))
				//Store_Heist_Data_Ready_For_Cross_Session_Transmission(Get_Players_Current_Apartment(), FALSE, FALSE, FALSE) 
				VECTOR vOnCallCamera
				BOOL bTemp
				INT iSetupBS
				SET_BIT(iSetupBS, ciCORONA_CAMERA_SETUP_BS_ARENA)
				SETUP_CORONA_CAMERA(ciCam, FMMC_TYPE_MISSION, 0, << 0.0, 0.0, 0.0 >>, vOnCallCamera, << 0.0, 0.0, 0.0 >>, TRUE, FALSE, vOnCallCamera, bTemp, bTemp, FALSE, iSetupBS)
				DISPLAY_RADAR(FALSE)
				
				// Create the Tv lobby playlist.
				CREATE_ARENA_WARS_TV_LOBBY_PLAYLIST(ref_local.iArenaJobIndex)
				
				// Stop the auido for the video to playing twice.
				SET_CORONA_BIT(CORONA_BINK_VIDEO_MUSIC_EVENT_TRIGGERED)
				
				// Play Tv audio
				TRIGGER_MUSIC_EVENT("AW_LOBBY_MUSIC_START")
				
				// Get rid of any lingering help
				CLEAR_HELP()
				EXIT
			ELSE
				ASSERTLN("[ARENA_CAREER_WALL] LAUNCH_NEXT_ARENA_SERIES_RACE returned false")
			ENDIF
		ELIF ref_local.input.bGoOnCall
			PRINTLN("[ARENA_CAREER_WALL] Going on call for series...")
			PLAY_SOUND_FRONTEND(-1, "Highlight_Accept", "DLC_AW_Arena_Office_Planning_Wall_Sounds")
			_ARENA_CAREER_WALL_STOP_USING_WALL(ref_local)
			LAUNCH_NEXT_ARENA_SERIES_RACE(TRUE)
			EXIT
		// If looking at TV & press Y (XBOX)	
		ELIF ref_local.cam.eView = ACWV_PANEL_TV
		AND IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RUP)
			// Go to leaderboard
			_ARENA_CAREER_WALL_SET_STATE(ref_local, ACWS_LEADERBOARD)
			_FORCE_UI_UPDATE(ref_local)
		ELSE	
			ARENA_CAREER_WALL_VIEW eNewView = _ARENA_CAREER_WALL_GET_PANEL_FROM_INPUT(ref_local, ref_local.input.eNavInput)
			IF eNewView <> ref_local.cam.eView
			AND eNewView <> ACWV_NONE			
				_ARENA_CAREER_WALL_SET_VIEW(ref_local, eNewView)
			ENDIF
		ENDIF
	ENDIF
	
	// Float back out to the overview if there's been no input on the board section for a while
	IF HAS_NET_TIMER_EXPIRED(ref_local.input.idleTimer, ci_ACW_RETURN_TO_INTRO_VIEW_TIME) 
	AND ref_local.cam.eView <> ACWV_PANEL_BOARD_INTRO
	AND _ARENA_CAREER_WALL_VIEW_IS_BOARD(ref_local.cam.eView)
		REINIT_NET_TIMER(ref_local.input.scrollTimer)
		_ARENA_CAREER_WALL_SET_VIEW(ref_local, ACWV_PANEL_BOARD_INTRO)
	ENDIF	
	
	HIDE_ALL_PLAYERS_THIS_FRAME()
	_ARENA_CAREER_WALL_MAINTAIN_CORE(ref_local, bKeyPressSafe)
ENDPROC

PROC _ARENA_CAREER_WALL_STATE_LEADERBOARD(ARENA_CAREER_WALL& ref_local)
	// Do any leaderboard per-frame processing
	HIDE_ALL_PLAYERS_THIS_FRAME()
	BOOL bKeyPressSafe = _ARENA_CAREER_WALL_CAN_ACCEPT_INPUTS()
	_ARENA_CAREER_WALL_DRAW_INSTRUCTIONAL_BUTTONS(ref_local, bKeyPressSafe)
	IF NOT ref_local.bSetupSCLeaderboardData
//		CONST_INT SCLB_TYPE_ARENA_MODE_BUZZER_BEATER 	1
//		CONST_INT SCLB_TYPE_ARENA_MODE_CARNAGE 			2
//		CONST_INT SCLB_TYPE_ARENA_MODE_FLAG_WAR 		3
//		CONST_INT SCLB_TYPE_ARENA_MODE_WRECK_IT 		4
//		CONST_INT SCLB_TYPE_ARENA_MODE_BOMB_BALL 		5
//		CONST_INT SCLB_TYPE_ARENA_MODE_GAMES_MASTERS 	6
//		CONST_INT SCLB_TYPE_ARENA_MODE_MONSTERS 		7
//		CONST_INT SCLB_TYPE_ARENA_MODE_HOT_BOMB 		8
//		CONST_INT SCLB_TYPE_ARENA_MODE_TAG_TEAM 		9
		IF ref_local.iCurrentLeaderboardID = 0
			SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(ref_local.scLB_control, SCLB_TYPE_ARENA_WARS_CAREER,"","")
		ELSE
			TEXT_LABEL_23 theMode = GET_ARENA_WARS_MODE_NAME(ref_local.iCurrentLeaderboardID)
			SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(ref_local.scLB_control, SCLB_TYPE_ARENA_WARS_MODES,"ArenaMode",theMode,ref_local.iCurrentLeaderboardID)
		ENDIF
		ref_local.bSetupSCLeaderboardData = TRUE
	ELSE
		DRAW_SOCIAL_CLUB_LEADERBOARD(ref_local.scLB_control,ref_local.scLB_ScaleformID) 
	ENDIF
//	IF ref_local.scLB_control.ReadDataStruct.m_LeaderboardId = 0
//		SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(ref_local.scLB_control, SCLB_TYPE_ARENA_WARS_MODES,"ArenaMode","Carnage",SCLB_TYPE_ARENA_MODE_CARNAGE)
//	ELSE
//		DRAW_SOCIAL_CLUB_LEADERBOARD(ref_local.scLB_control,g_DB_scLC_ScaleformID)
//	ENDIF
	IF _ARENA_CAREER_WALL_CAN_ACCEPT_INPUTS()
		IF IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_SCRIPT_RB)
			ref_local.iCurrentLeaderboardID++
			IF ref_local.iCurrentLeaderboardID > SCLB_TYPE_ARENA_MODE_TAG_TEAM
				ref_local.iCurrentLeaderboardID = 0
			ENDIF
			scLB_DisplayedData.bResetTriggered = TRUE
			CLEANUP_SOCIAL_CLUB_LEADERBOARD(ref_local.scLB_control,TRUE)
			ref_local.bSetupSCLeaderboardData = FALSE
		ENDIF
		// XBOX B to go back to wall control
		IF IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, _GET_CONTROL_ACTION_BACK())
			scLB_DisplayedData.bResetTriggered = TRUE
			CLEANUP_SOCIAL_CLUB_LEADERBOARD(ref_local.scLB_control,TRUE)
			ref_local.bSetupSCLeaderboardData = FALSE
			_ARENA_CAREER_WALL_SET_STATE(ref_local, ACWS_LOOK_AT_WALL)
			_FORCE_UI_UPDATE(ref_local)
		ENDIF 
	ELSE
		scLB_DisplayedData.bResetTriggered = TRUE
		CLEANUP_SOCIAL_CLUB_LEADERBOARD(ref_local.scLB_control,TRUE)
		ref_local.bSetupSCLeaderboardData = FALSE
		_ARENA_CAREER_WALL_SET_STATE(ref_local, ACWS_LOOK_AT_WALL)
		_FORCE_UI_UPDATE(ref_local)
	ENDIF
ENDPROC

PROC _ARENA_CAREER_WALL_STATE_DO_TUTORIAL(ARENA_CAREER_WALL& ref_local)

	HIDE_ALL_PLAYERS_THIS_FRAME()
	IF NOT _ARENA_CAREER_WALL_CAN_ACCEPT_INPUTS()
		EXIT
	ENDIF
	
	BOOL bTutorialButtons = TRUE
	
	SWITCH ref_local.iTutorialStage
		CASE 0
			PRINT_HELP("ACW_TUT_1")
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, _GET_CONTROL_ACTION_ACCEPT())
			OR HAS_NET_TIMER_EXPIRED(ref_local.tutorialTimer, ci_WALL_TUT_DURATION)
				_ARENA_CAREER_WALL_SET_VIEW(ref_local, ACWV_PANEL_BOARD_TL)
				REINIT_NET_TIMER(ref_local.input.scrollTimer)
				REINIT_NET_TIMER(ref_local.tutorialTimer)
				ref_local.iTutorialStage++
			ENDIF
			BREAK			
			
		CASE 1
			PRINT_HELP("ACW_TUT_2")
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, _GET_CONTROL_ACTION_ACCEPT())
			OR HAS_NET_TIMER_EXPIRED(ref_local.tutorialTimer, ci_WALL_TUT_DURATION)
				_ARENA_CAREER_WALL_SET_VIEW(ref_local, ACWV_PANEL_TV)
				REINIT_NET_TIMER(ref_local.input.scrollTimer)
				REINIT_NET_TIMER(ref_local.tutorialTimer)
				ref_local.iTutorialStage++
			ENDIF
			BREAK		
			
		CASE 2
			PRINT_HELP("ACW_TUT_3")
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, _GET_CONTROL_ACTION_ACCEPT())
			OR HAS_NET_TIMER_EXPIRED(ref_local.tutorialTimer, ci_WALL_TUT_DURATION)
				_ARENA_CAREER_WALL_SET_VIEW(ref_local, ACWV_PANEL_BOARD_TR)
				REINIT_NET_TIMER(ref_local.input.scrollTimer)
				REINIT_NET_TIMER(ref_local.tutorialTimer)
				ref_local.iTutorialStage++
			ENDIF
			BREAK	
			
		CASE 3
			PRINT_HELP("ACW_TUT_4")
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, _GET_CONTROL_ACTION_ACCEPT())
			OR HAS_NET_TIMER_EXPIRED(ref_local.tutorialTimer, ci_WALL_TUT_DURATION)
				_ARENA_CAREER_WALL_SET_VIEW(ref_local, ACWV_PANEL_BOARD_BR)
				REINIT_NET_TIMER(ref_local.input.scrollTimer)
				REINIT_NET_TIMER(ref_local.tutorialTimer)
				ref_local.iTutorialStage++
			ENDIF
			BREAK	
			
		CASE 4
			PRINT_HELP("ACW_TUT_5")
			IF IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, _GET_CONTROL_ACTION_ACCEPT())
			OR HAS_NET_TIMER_EXPIRED(ref_local.tutorialTimer, ci_WALL_TUT_DURATION)
				ref_local.iTutorialStage++
			ENDIF
			BREAK
			
		DEFAULT
			bTutorialButtons = FALSE
			_ARENA_CAREER_WALL_SET_VIEW(ref_local, ACWV_PANEL_BOARD_INTRO)
			REINIT_NET_TIMER(ref_local.input.scrollTimer)
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARENA_DONE_WALL_TUT, TRUE)
			_ARENA_CAREER_WALL_SET_STATE(ref_local, ACWS_LOOK_AT_WALL)
			BREAK
	ENDSWITCH
	
	_ARENA_CAREER_WALL_MAINTAIN_CORE(ref_local)
	_ARENA_CAREER_WALL_DRAW_INSTRUCTIONAL_BUTTONS(ref_local, _ARENA_CAREER_WALL_CAN_ACCEPT_INPUTS(), bTutorialButtons)
ENDPROC

#IF IS_DEBUG_BUILD

PROC ADD_WIDGET_VECTOR_SLIDER_GROUP(STRING sVectorName, VECTOR& ref_vec, FLOAT min = -10000.0, FLOAT max = 10000.0, FLOAT sliderStep = 1.0)
	START_WIDGET_GROUP(sVectorName)
		ADD_WIDGET_FLOAT_SLIDER("x", ref_vec.x, min, max, sliderStep)
		ADD_WIDGET_FLOAT_SLIDER("y", ref_vec.y, min, max, sliderStep)
		ADD_WIDGET_FLOAT_SLIDER("z", ref_vec.z, min, max, sliderStep)
	STOP_WIDGET_GROUP()
ENDPROC

PROC ADD_WIDGET_VECTOR_COMP_SLIDER_GROUP(STRING sVectorName, FLOAT& x, FLOAT& y, FLOAT& z, FLOAT min = -10000.0, FLOAT max = 10000.0, FLOAT sliderStep = 1.0)
	START_WIDGET_GROUP(sVectorName)
		ADD_WIDGET_FLOAT_SLIDER("x", x, min, max, sliderStep)
		ADD_WIDGET_FLOAT_SLIDER("y", y, min, max, sliderStep)
		ADD_WIDGET_FLOAT_SLIDER("z", z, min, max, sliderStep)
	STOP_WIDGET_GROUP()
ENDPROC

PROC ARENA_CAREER_WALL_CREATE_WIDGETS(ARENA_CAREER_WALL& ref_local)
	ref_local.debug.bWidgetsReady = TRUE
	
	INT i
	START_WIDGET_GROUP("Arena Career Wall")
		START_NEW_WIDGET_COMBO()
		REPEAT ACWS_COUNT i
			ARENA_CAREER_WALL_STATE eState = INT_TO_ENUM(ARENA_CAREER_WALL_STATE, i)
			STRING sStateString = _ARENA_CAREER_WALL_GET_STATE_STRING(eState)
			ADD_TO_WIDGET_COMBO(sStateString)
		ENDREPEAT
		STOP_WIDGET_COMBO("State", ref_local.debug.iState)

		ADD_WIDGET_INT_SLIDER("pan time", ci_ACW_MOVE_TIME, 0, 1000, 100)
		ADD_WIDGET_INT_SLIDER("pan delay", ci_ACW_MOVE_DELAY, 0, 1000, 100)
		ADD_WIDGET_BOOL("Reset tutorial stat", ref_local.debug.bResetTutorialStat)
		ADD_WIDGET_BOOL("Reset trinket ticker stat", ref_local.debug.bResetTrinketTickerStat)
		ADD_WIDGET_BOOL("Do debug draw", ref_local.debug.bDebugDraw)
		ADD_WIDGET_FLOAT_READ_ONLY("Time factor", ref_local.debug.fTimeFactor)
		ADD_WIDGET_INT_SLIDER("Bonus help seen times", ref_local.iBonusHelpCount, 0, 3, 1)
		ADD_WIDGET_BOOL("Clear 'have seen' bonus help", ref_local.debug.bClearSeenBonusHelp)
		ADD_WIDGET_INT_SLIDER("Job index override ", ref_local.debug.iOverrideJobIndex, -1, 26, 1)
		
		START_WIDGET_GROUP("Tv render target")
			ADD_WIDGET_FLOAT_SLIDER("Centre x", cf_TV_CENTRE_X, -100, 100, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("Centre y", cf_TV_CENTRE_Y, -100, 100, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("w", cf_TV_WIDTH, -100, 100, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("h", cf_TV_HEIGHT, -100, 100, 0.5)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Wall render target")
			ADD_WIDGET_FLOAT_SLIDER("Centre x", cf_WALL_CENTRE_X, -100, 100, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Centre y", cf_WALL_CENTRE_Y, -100, 100, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("w", cf_WALL_WIDTH, -100, 100, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("h", cf_WALL_HEIGHT, -100, 100, 0.01)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Cam")
			ADD_WIDGET_INT_SLIDER("Idle timeout (ms)", ci_ACW_RETURN_TO_INTRO_VIEW_TIME, 0, 100000, 1000)
			ADD_WIDGET_VECTOR_COMP_SLIDER_GROUP("Pos offset", cf_WALL_CAM_OFFSET_X, cf_WALL_CAM_OFFSET_Y, cf_WALL_CAM_OFFSET_Z, -10.0, 10.0, 0.01)
			ADD_WIDGET_VECTOR_COMP_SLIDER_GROUP("Look-at offset", cf_WALL_CAM_LOOKAT_X, cf_WALL_CAM_LOOKAT_Y, cf_WALL_CAM_LOOKAT_Z, -10.0, 10.0, 0.01)
			ADD_WIDGET_BOOL("Apply player-look offsets", ref_local.debug.bApplyAltOffsets)
			ADD_WIDGET_BOOL("Apply default (plan) offsets", ref_local.debug.bApplyDefOffsets)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF

FUNC BOOL REGISTER_AND_LINK_RT(STRING sRenderTarget, MODEL_NAMES eProp, INT& out_id)
	IF NOT IS_NAMED_RENDERTARGET_REGISTERED(sRenderTarget)
		PRINTLN("[ARENA_CAREER_WALL] NOT IS_NAMED_RENDERTARGET_REGISTERED ", sRenderTarget)
		IF NOT REGISTER_NAMED_RENDERTARGET(sRenderTarget)
			PRINTLN("[ARENA_CAREER_WALL] NOT REGISTER_NAMED_RENDERTARGET ", sRenderTarget)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_NAMED_RENDERTARGET_LINKED(eProp)
		LINK_NAMED_RENDERTARGET(eProp)
		PRINTLN("[ARENA_CAREER_WALL] NOT IS_NAMED_RENDERTARGET_LINKED ", sRenderTarget)
		RETURN FALSE		
	ENDIF		

	out_id = GET_NAMED_RENDERTARGET_RENDER_ID(sRenderTarget)
	RETURN TRUE	
ENDFUNC

FUNC BOOL ARENA_CAREER_WALL_LOAD(ARENA_CAREER_WALL& ref_local)
	// Already loaded?
	IF ref_local.eState > ACWS_LOAD
		RETURN TRUE 
	ENDIF
		
	BOOL bLoaded = TRUE
	
	// Put one-frame-only init script here that is required for this function 
	// (Rest can go in init)
	IF NOT ref_local.bDoneFirstLoadFrame 
		ref_local.drawDataWall.sScaleform = REQUEST_SCALEFORM_MOVIE("ARENA_CAREER_WALL")
		ref_local.bDoneFirstLoadFrame  = TRUE
	ENDIF
	
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(ref_local.drawDataWall.sScaleform)
		PRINTLN("[ARENA_CAREER_WALL][LOAD] Loaing scaleform 'ARENA_CAREER_WALL', id = ", NATIVE_TO_INT(ref_local.drawDataWall.sScaleform))
		bLoaded = FALSE
	ENDIF
	
	// Set up Tv
	ref_local.drawDataTv.eTvProp = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Screen_TV_01"))
	ref_local.drawDataTv.sRenderTargetName = "Screen_Tv_01"
	IF NOT REGISTER_AND_LINK_RT(ref_local.drawDataTv.sRenderTargetName, ref_local.drawDataTv.eTvProp, ref_local.drawDataTv.iRenderTargetId)
		PRINTLN("[ARENA_CAREER_WALL][LOAD] ... still trying to register and link RT ", ref_local.drawDataTv.sRenderTargetName)
		bLoaded = FALSE
	ENDIF
	
	ref_local.drawDataWall.eWallProp = INT_TO_ENUM(MODEL_NAMES, HASH("xs_Prop_Arena_Planning_RT_01"))
	ref_local.drawDataWall.sRenderTargetName = "planning_board_01" 
	IF NOT REGISTER_AND_LINK_RT(ref_local.drawDataWall.sRenderTargetName, ref_local.drawDataWall.eWallProp, ref_local.drawDataWall.iRenderTargetId)
		PRINTLN("[ARENA_CAREER_WALL][LOAD] Trying to register and link RT ", ref_local.drawDataWall.sRenderTargetName)
		bLoaded = FALSE
	ENDIF
	
	// Grab owner's data
	IF ref_local.pOwnerId <> PLAYER_ID()
		IF NATIVE_TO_INT(ref_local.tStatsRequestTime) = 0
			IF IS_NET_PLAYER_OK(ref_local.pOwnerId, FALSE)
				bLoaded = FALSE
				ref_local.tStatsRequestTime = REQUEST_PLAYER_ARENA_CAREER_STATS(ref_local.pOwnerId)
				PRINTLN("[ARENA_CAREER_WALL][LOAD] Sending stat request... ", NATIVE_TO_INT(ref_local.tStatsRequestTime))
			ELSE
				PRINTLN("[ARENA_CAREER_WALL][LOAD] Garage owner is invalid (", NATIVE_TO_INT(ref_local.pOwnerId),")")
			ENDIF
		ELSE
			IF NOT IS_PLAYER_ARENA_CAREER_STATS_REQUEST_COMPLETE(ref_local.pOwnerId, ref_local.tStatsRequestTime)
				// IF we've timeed out then just continue
				IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), ref_local.tStatsRequestTime)) >= 4000)
					PRINTLN("[ARENA_CAREER_WALL][LOAD] Request timed out... Stats may be out of date.")
				ELSE
					PRINTLN("[ARENA_CAREER_WALL][LOAD] Waiting for stats... ")
					bLoaded = FALSE
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
	IF bLoaded
		_ARENA_CAREER_WALL_INIT(ref_local)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

#IF IS_DEBUG_BUILD
PROC _ARENA_CAREER_WALL_UPDATE_WIDGETS(ARENA_CAREER_WALL& ref_local)
	IF NOT ref_local.debug.bWidgetsReady
		EXIT
	ENDIF
	
	IF ref_local.debug.bResetTutorialStat
		ref_local.debug.bResetTutorialStat = FALSE
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARENA_DONE_WALL_TUT, FALSE)
	ENDIF
	
	IF ref_local.debug.bResetTrinketTickerStat
		PRINTLN("[ARENA_CAREER_WALL] Resetting MP_STAT_ARN_BS_TRINKET_TICKERS via widget")
		ref_local.debug.bResetTrinketTickerStat = FALSE
		SET_MP_INT_CHARACTER_STAT(MP_STAT_ARN_BS_TRINKET_TICKERS, 0)
	ENDIF
	
	IF ref_local.debug.bClearSeenBonusHelp
		ref_local.debug.bClearSeenBonusHelp = FALSE
		CLEAR_BIT(g_SimpleInteriorData.iFifthBS, BS5_SIMPLE_INTERIOR_ARENA_DONE_BONUS_HELP)
	ENDIF
	
	IF ref_local.debug.iOverrideJobIndex <> -1
		ref_local.iArenaJobIndex = ref_local.debug.iOverrideJobIndex		
	ENDIF
	
	IF ref_local.debug.bApplyAltOffsets
		SWITCH ref_local.cam.eView
			CASE ACWV_PANEL_BOARD_TL	
				cf_WALL_CAM_OFFSET_X = -0.200
				cf_WALL_CAM_OFFSET_Y = 0.0
				cf_WALL_CAM_OFFSET_Z = 0.0
				cf_WALL_CAM_LOOKAT_X = 0.0
				cf_WALL_CAM_LOOKAT_Y = 0.0
				cf_WALL_CAM_LOOKAT_Z = 0.0
				BREAK
			CASE ACWV_PANEL_BOARD_TR
				cf_WALL_CAM_OFFSET_X = 0.280
				cf_WALL_CAM_OFFSET_Y = 0.0
				cf_WALL_CAM_OFFSET_Z = 0.0
				cf_WALL_CAM_LOOKAT_X = 0.0
				cf_WALL_CAM_LOOKAT_Y = 0.0
				cf_WALL_CAM_LOOKAT_Z = 0.0
				BREAK
			CASE ACWV_PANEL_BOARD_BL	
				cf_WALL_CAM_OFFSET_X = -0.200
				cf_WALL_CAM_OFFSET_Y = 0.0
				cf_WALL_CAM_OFFSET_Z = 0.0
				cf_WALL_CAM_LOOKAT_X = 0.0
				cf_WALL_CAM_LOOKAT_Y = 0.0
				cf_WALL_CAM_LOOKAT_Z = 0.0
				BREAK			
			CASE ACWV_PANEL_BOARD_BR	
				cf_WALL_CAM_OFFSET_X = 0.280
				cf_WALL_CAM_OFFSET_Y = 0.0
				cf_WALL_CAM_OFFSET_Z = 0.0
				cf_WALL_CAM_LOOKAT_X = 0.0
				cf_WALL_CAM_LOOKAT_Y = 0.0
				cf_WALL_CAM_LOOKAT_Z = 0.0
				BREAK	
		ENDSWITCH
	ENDIF
	
	IF ref_local.debug.bApplyDefOffsets
		ref_local.debug.bApplyAltOffsets = FALSE
		ref_local.debug.bApplyDefOffsets = FALSE
				cf_WALL_CAM_OFFSET_X = 0.0
				cf_WALL_CAM_OFFSET_Y = 0.0
				cf_WALL_CAM_OFFSET_Z = 0.0
				cf_WALL_CAM_LOOKAT_X = 0.0
				cf_WALL_CAM_LOOKAT_Y = 0.0
				cf_WALL_CAM_LOOKAT_Z = 0.0
	ENDIF
	
	ref_local.debug.iState = ENUM_TO_INT(ref_local.eState)
 	ref_local.debug.fTimeFactor = _ARENA_CAREER_WALL_GET_SCROLL_TIME_FACTOR(ref_local.input.scrollTimer)
ENDPROC
#ENDIF

/// PURPOSE:
///    Cleans up the arena_career_wall and prevents any further processing.
/// PARAMS:
///    ref_local - 
PROC ARENA_CAREER_WALL_CLEANUP(ARENA_CAREER_WALL& ref_local)
	PRINTLN("[ARENA_CAREER_WALL] ARENA_CAREER_WALL_CLEANUP")
	IF ref_local.eState = ACWS_LOOK_AT_WALL
	OR ref_local.eState = ACWS_LEADERBOARD
		_ARENA_CAREER_WALL_STOP_USING_WALL(ref_local)
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(ref_local.drawDataTv.sRenderTargetName)
	AND IS_NAMED_RENDERTARGET_REGISTERED(ref_local.drawDataTv.sRenderTargetName)
		PRINTLN("[ARENA_CAREER_WALL] Releasing ", ref_local.drawDataTv.sRenderTargetName)
		RELEASE_NAMED_RENDERTARGET(ref_local.drawDataTv.sRenderTargetName)
		ref_local.drawDataTv.iRenderTargetId = -1
	ENDIF

	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(ref_local.drawDataWall.sScaleform)
	IF NOT IS_STRING_NULL_OR_EMPTY(ref_local.drawDataWall.sRenderTargetName)
	AND IS_NAMED_RENDERTARGET_REGISTERED(ref_local.drawDataWall.sRenderTargetName)
		PRINTLN("[ARENA_CAREER_WALL] Releasing ", ref_local.drawDataWall.sRenderTargetName)
		RELEASE_NAMED_RENDERTARGET(ref_local.drawDataWall.sRenderTargetName)
		ref_local.drawDataWall.iRenderTargetId = -1
	ENDIF
	
	SET_ARENA_OFFICE_TV_ACTIVE_RENDER_STATE(FALSE)
	RELEASE_CONTEXT_INTENTION(ref_local.iTriggerContext)
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ACW_START")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ACW_MOCAP_HELP")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ACW_TUT_BONUS")
		CLEAR_HELP()
	ENDIF
	
	_ARENA_CAREER_WALL_CLEAR_BLIP(ref_local)
	
	ref_local.bDoneFirstLoadFrame = FALSE
	
	_ARENA_CAREER_WALL_SET_GLOBAL(FALSE)
	
	scLB_DisplayedData.bResetTriggered = TRUE
	CLEANUP_SOCIAL_CLUB_LEADERBOARD(ref_local.scLB_control)
	
	IF NOT HAS_ARENA_WARS_TV_LOBBY_PLAYLIST_STARTED()
		STOP_ARENA_WARS_TV_OFFICE_PLAYLIST()
	ENDIF
	
	CANCEL_MUSIC_EVENT("AW_LOBBY_MUSIC_START")
	PRINTLN("[ARENA_CAREER_WALL] MUSIC : CANCEL_MUSIC_EVENT('AW_LOBBY_MUSIC_START')")
	g_bForceStopUsingCareerWall = FALSE
	g_bForceCleanupCareerWall = FALSE
	
	_ARENA_CAREER_WALL_SET_STATE(ref_local, ACWS_CLEANUP_COMPLETE)
ENDPROC

FUNC BOOL ARENA_CAREER_WALL_BLOCK_BINK_RENDERING()
	IF IS_BROWSER_OPEN()
	OR NOT ARENA_CAREER_WALL_IS_ENTITY_IN_OFFICE_AREA(PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC _ARENA_CAREER_WALL_STATE_MOCAP_SCENE(ARENA_CAREER_WALL& ref_local, ENTITY_INDEX entMocapPed)
	SWITCH ref_local.iMocapStage
		CASE 0
			PRINTLN("[ARENA_CAREER_WALL] MOCAP STAGE 0 : Set up wall with Peter's data")
			INT iBsTrinkets, iNumDeaths, iSkillLevel
			iSkillLevel = 3
			iNumDeaths = 1
			iBsTrinkets = CALC_ARENA_TRINKET_BITSET_STICKERS_ONLY(iSkillLevel, iNumDeaths, FALSE, FALSE)
			
			_SCALEFORM_METHOD_SET_STATS(ref_local.drawDataWall.sScaleform, "Peter", 
											iSkillLevel, 1565, 65, 3, 10, 0, 2, iNumDeaths, 2, -1, 
											ciARENA_JOB_TYPE_HERE_COME_THE_MONSTERS, MONSTER3)
			_ADD_WALL_STICKERS(ref_local.drawDataWall.sScaleform, iBsTrinkets)						
			_SCALEFORM_APPLY_WALL_CHANGES(ref_local.drawDataWall)
			ref_local.iMocapStage++
			BREAK
			
		CASE 1
			IF NOT IS_ENTITY_ALIVE(entMocapPed)
			OR HAS_ANIM_EVENT_FIRED(entMocapPed, HASH("BOARD_SWAP_1"))
				PRINTLN("[ARENA_CAREER_WALL] MOCAP STAGE 1 : Set up wall with Garage owner's data. mocap ped alive?", IS_ENTITY_ALIVE(entMocapPed))
				// Reset wall textures
				_REMOVE_ALL_STICKERS(ref_local.drawDataWall.sScaleform)
				// Pass in currently drawn job index so it cannot be changed
				_ARENA_CAREER_WALL_UPDATE_DATA(ref_local.drawDataWall.iArenaJobIndex, ref_local.drawDataWall, ref_local.pOwnerId, ref_local.iBsTrinkets)
				_SCALEFORM_APPLY_WALL_CHANGES(ref_local.drawDataWall)
				ref_local.iMocapStage++
			ENDIF	
			BREAK
			
		CASE 2
			IF NOT IS_ENTITY_ALIVE(entMocapPed)
			OR HAS_ANIM_EVENT_FIRED(entMocapPed, HASH("BOARD_SWAP_2"))
				PRINTLN("[ARENA_CAREER_WALL] MOCAP STAGE 2 : ... doing nothing... mocap ped alive?", IS_ENTITY_ALIVE(entMocapPed))
				ref_local.iMocapStage++
			ENDIF			
			BREAK
			
		CASE 3
			IF NOT IS_PLAYER_WATCHING_ARENA_INTRO_2_CUTSCENE(PLAYER_ID())
				PRINTLN("[ARENA_CAREER_WALL] MOCAP STAGE 3: done")
				ref_local.iMocapStage++
				_ARENA_CAREER_WALL_SET_STATE(ref_local, ACWS_WAIT_FOR_TRIGGER)
			ENDIF
			BREAK
	ENDSWITCH
ENDPROC

PROC ARENA_CAREER_WALL_MAINTAIN(ARENA_CAREER_WALL& ref_local, PLAYER_INDEX pOwner, ENTITY_INDEX entMocapPed = NULL)
	ref_local.pOwnerId = pOwner

	ref_local.iArenaJobIndex = GET_CURRENT_ARENA_JOB_INDEX()
	
	#IF IS_DEBUG_BUILD
	_ARENA_CAREER_WALL_UPDATE_WIDGETS(ref_local)
	#ENDIF


	IF ((ref_local.eState = ACWS_LOOK_AT_WALL
	OR ref_local.eState = ACWS_DO_TUTORIAL
	OR ref_local.eState = ACWS_LEADERBOARD)
	AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<205.3092, 5162.6089, -86.5974>>) > 7.0)
	OR g_bForceStopUsingCareerWall
		_ARENA_CAREER_WALL_STOP_USING_WALL(ref_local) 
	ENDIF
	
	IF (ref_local.eState = ACWS_LOOK_AT_WALL
	OR ref_local.eState = ACWS_DO_TUTORIAL
	OR ref_local.eState = ACWS_LEADERBOARD)
		DISABLE_FRONTEND_THIS_FRAME()
	ENDIF
	
	IF g_bForceCleanupCareerWall
		ARENA_CAREER_WALL_CLEANUP(ref_local)
	ENDIF
	
	SWITCH ref_local.eState
		CASE ACWS_LOAD             
			ARENA_CAREER_WALL_LOAD(ref_local)                    
			BREAK
		CASE ACWS_WAIT_FOR_TRIGGER
			IF IS_PLAYER_WATCHING_ARENA_INTRO_2_CUTSCENE(PLAYER_ID())
			AND ref_local.iMocapStage = 0 // Don't trigger if we've already done it
				_ARENA_CAREER_WALL_SET_STATE(ref_local, ACWS_MOCAP_SCENE)
			ELSE 
				_ARENA_CAREER_WALL_STATE_WAIT_FOR_TRIGGER(ref_local) 
			ENDIF
			BREAK 
		CASE ACWS_LOOK_AT_WALL     
			_ARENA_CAREER_WALL_STATE_LOOK_AT_WALL(ref_local, g_sTransitionSessionData.ciCam)     
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MULTIPLAYER_INFO)
			BREAK
		CASE ACWS_DO_TUTORIAL
			_ARENA_CAREER_WALL_STATE_DO_TUTORIAL(ref_local)     
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MULTIPLAYER_INFO)
			BREAK		
		CASE ACWS_LEADERBOARD
			_ARENA_CAREER_WALL_STATE_LEADERBOARD(ref_local)     
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MULTIPLAYER_INFO)
			BREAK
		CASE ACWS_MOCAP_SCENE
			_ARENA_CAREER_WALL_STATE_MOCAP_SCENE(ref_local, entMocapPed)
			BREAK	
	ENDSWITCH
	
	_ARENA_CAREER_WALL_SET_GLOBAL(ref_local.eState = ACWS_LOOK_AT_WALL OR ref_local.eState = ACWS_DO_TUTORIAL OR ref_local.eState = ACWS_LEADERBOARD)
	
	IF ref_local.eState > ACWS_LOAD
		IF HAVE_CONTROLS_CHANGED(PLAYER_CONTROL)
		OR HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
			ref_local.bLoadedButtonHelp = FALSE
		ENDIF
		
		IF IS_SCREEN_FADED_IN()
		AND HAS_LOCAL_PLAYER_VIEWED_ARENA_MOCAP_SCENE_2()
			// One-time help "Access your Career wall ~BLIP_ARENA_SERIES~ to enter the Arena War Series."
			IF NOT ref_local.bDoneOneTimeHelp
			AND (NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ACW_START"))
				PRINTLN("[ARENA_CAREER_WALL] Printing one-time help text")
				ref_local.bDoneOneTimeHelp = TRUE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_ARENA_DONE_WALL_HELP, TRUE)
				PRINT_HELP("ACW_MOCAP_HELP", DEFAULT_HELP_TEXT_TIME)
			ENDIF
		ENDIF
		
		// Draw Wall
		DRAW_SCALEFORM_TO_RT(ref_local)
		
		// Draw TV
		IF ARENA_CAREER_WALL_BLOCK_BINK_RENDERING()
			SET_ARENA_OFFICE_TV_ACTIVE_RENDER_STATE(FALSE)
			IF NOT IS_PLAYER_IN_CORONA()
				CLEAR_ARENA_WARS_TV_OFFICE_PLAYLIST()
			ENDIF
		ELSE
			IF HAS_ARENA_WARS_TV_LOBBY_PLAYLIST_STARTED()
				SET_ARENA_OFFICE_TV_ACTIVE_RENDER_STATE(TRUE)
				DRAW_ARENA_WARS_TV_LOBBY_PLAYLIST()
				CLEAR_ARENA_WARS_TV_OFFICE_PLAYLIST()
			ELIF ref_local.iArenaJobIndex <> -1
				IF GET_ARENA_WARS_TV_OFFICE_PLAYLIST_JOB() <> ref_local.iArenaJobIndex
				OR NOT IS_PLAYLIST_PLAYING_ON_CHANNEL(TVCHANNELTYPE_CHANNEL_2, ref_local.drawDataTv.iPlaylistHash)
					CREATE_ARENA_WARS_TV_OFFICE_PLAYLIST(ref_local.iArenaJobIndex, ref_local.drawDataTv.iModeMapClipHash, ref_local.drawDataTv.iPlaylistHash)
				ENDIF				
				DRAW_ARENA_WARS_TV_OFFICE_PLAYLIST(ref_local)
			ENDIF
		ENDIF
	ENDIF
ENDPROC
