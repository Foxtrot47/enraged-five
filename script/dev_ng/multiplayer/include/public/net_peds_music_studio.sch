//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_MUSIC_STUDIO.sch																					//
// Description: Functions to implement from net_peds_base.sch and look up tables to return MUSIC_STUDIO ped data.			//
// Written by:  Jan Mencfel																								//
// Date:  		04/08/21																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_MUSIC_STUDIO
USING "net_peds_base.sch"
USING "website_public.sch"
USING "net_peds_data_common.sch"
USING "net_simple_cutscene_common.sch"
USING "net_simple_interior.sch"

FUNC VECTOR _GET_MUSIC_STUDIO_PED_LOCAL_COORDS_BASE_POSITION(PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	UNUSED_PARAMETER(ePedLocation)
	RETURN <<-1009.9955, -70.0099, -100.4031>>	
ENDFUNC

FUNC FLOAT _GET_MUSIC_STUDIO_PED_LOCAL_HEADING_BASE_HEADING(PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	UNUSED_PARAMETER(ePedLocation)
	RETURN 0.0
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ PED DATA ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛
PROC GET_RANDOM_DJPOOH_DATA(PEDS_DATA &Data, SERVER_PED_DATA &ServerBD, CLUB_DJS eCurrentDJ)
	//UNUSED_PARAMETER(ServerBD)
	INT i = ServerBD.iLayout
	IF eCurrentDJ != CLUB_DJ_DR_DRE_ABSENT
		SWITCH i
			CASE 0
				Data.vPosition = <<7.7905, 6.9408, 0.4069>>
				Data.vRotation.z  = 240.0000
				Data.iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_POOH_COUCH)
				SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEAD_TRACKING_ONLY_WITH_BASE_ANIM)
				SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
			BREAK
			CASE 1  // UPDATED TO DOC VERSION 6
				Data.vPosition = <<5.4195, 6.5849, 0.4031>>
				Data.vRotation.z = -131.760
				Data.iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_POOH_STAND)
				SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEAD_TRACKING_ONLY_WITH_BASE_ANIM)
			BREAK
			CASE 2
				Data.vPosition = <<5.5525, 6.7033, 0.4069>>
				Data.vRotation.z = 240.0000
				Data.iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_POOH_DRINK_BAR)
			BREAK
		ENDSWITCH
	ELSE
		SWITCH i 
			CASE 0
				Data.vPosition = <<6.6235, 2.6349, 0.4031>>
				Data.vRotation.z = -123.120
				Data.iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_POOH_DRINK_SIT)
			BREAK
//			CASE 1 										url:bugstar:7436851 - DJ Pooh seems a bit exposed here, could we remove this scenario spot for Pooh?
//				Data.vPosition = <<0.3310, 7.5283, -0.0056>>
//				Data.vRotation.z = 98.280
//				Data.iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_POOH_TEXT_STAND)
//			BREAK
			CASE 1
				Data.vPosition = <<-8.9235, -18.9151, 0.5040>>
				Data.vRotation.z = 180.0
				Data.iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_POOH_TEXT_SIT)
			BREAK
			CASE 2
				Data.vPosition = <<-8.9235, -18.9151, 0.5040>>
				Data.vRotation.z = 180.0
				Data.iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_POOH_TEXT_SIT)
			BREAK
		ENDSWITCH
	ENDIF

ENDPROC


PROC GET_RANDOM_ENTOURAGE1_POSITION_AND_HEADING(VECTOR &vPosition, FLOAT &fHeading, INT &iActivity, SERVER_PED_DATA &ServerBD, CLUB_DJS eCurrentDJ)
	//UNUSED_PARAMETER(ServerBD)
	INT i = ServerBD.iLayout
	IF eCurrentDJ != CLUB_DJ_DR_DRE_ABSENT
		SWITCH i  
			CASE 0			// UPDATED TO DOC VERSION 6 
				vPosition = << 8.3755, 7.8224, 0.8906>>
				fHeading = -149.010
				iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_E_SIT_DRINK)
			BREAK
			CASE 1			// UPDATED TO DOC VERSION 6
				vPosition = << 6.3755, 2.8724, 0.9281>>
				fHeading = -108.760
				iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_E_SIT_DRINK)
			BREAK
			CASE 2			// UPDATED TO DOC VERSION 6 	
				vPosition =	<< 6.3755, 2.8724, 0.9281>>
				fHeading = -108.760
				iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_E_SIT_DRINK)
			BREAK
		ENDSWITCH
	ELSE
		SWITCH i
			CASE 0 	
				vPosition = <<-2.9995, -12.75123, 0.99377>>
				fHeading = 180.000
				iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_E_TEXTING)
			BREAK
			CASE 1	// UPDATED TO DOC VERSION 6
				vPosition = <<11.5735,8.8329,1.4001>>
				fHeading = 76.860
				iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_E_STAND_ARMS_CROSSED)
			BREAK
			CASE 2
				vPosition = <<6.3815, 2.7974, 0.904>>
				fHeading = -102.570
				iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_E_SIT_LEGS_CROSSED)
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC GET_RANDOM_ENTOURAGE2_POSITION_AND_HEADING(VECTOR &vPosition, FLOAT &fHeading, INT &iActivity, SERVER_PED_DATA &ServerBD, CLUB_DJS eCurrentDJ)
	//UNUSED_PARAMETER(ServerBD)
	INT i = ServerBD.iLayout
	IF eCurrentDJ != CLUB_DJ_DR_DRE_ABSENT
		SWITCH i
			CASE 0 	// UPDATED TO DOC VERSION 6	
				vPosition = <<8.6265, 8.2099, 1.3906>>
				fHeading = -172.250
				iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_E_STAND_DRINK_CUP)
			BREAK
			CASE 1		// UPDATED TO DOC VERSION 6
				vPosition = << 5.8865, 2.3849, 1.3906>>
				fHeading = -117.260
				iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_E_STAND_DRINK_CUP)
			BREAK
			CASE 2		// UPDATED TO DOC VERSION 6
				vPosition = << 5.8865, 2.3849, 1.3906>>
				fHeading = -117.260
				iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_E_STAND_DRINK_CUP)
			BREAK
		ENDSWITCH
	ELSE
		SWITCH i 
			CASE 0
				vPosition = <<-3.4910, -18.7105, 0.9981>>
				fHeading = 170.5569//180.0
				iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_E_WINDOW_SHOP)
			BREAK
			CASE 1		// UPDATED TO DOC VERSION 6 //<<-1009.9955, -70.0099, -100.4031>>	
				vPosition = <<10.6855,9.2579,1.4001>>
				fHeading = -110.090
				iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_E_STAND_DRINK)
			BREAK
			CASE 2
				vPosition = <<7.6095, 2.6349, 1.404>>
				fHeading = 77.040
				iActivity	= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_E_STAND_ARM_HOLD)
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC GET_STAFF_MEMBER_POSITION_HEADING_AND_ACTIVITY(INT iStaffMemberIndex, VECTOR &vPosition, FLOAT &fHeading, INT &iActivity, SERVER_PED_DATA &ServerBD, CLUB_DJS eCurrentDJ)
	//UNUSED_PARAMETER(ServerBD)
	INT i = ServerBD.iLayout
	SWITCH i
		CASE 0
			SWITCH iStaffMemberIndex
				CASE 0
					vPosition   =  <<20.1395, 10.1931, 1.4231>>
					fHeading 	=  80.970	
					iActivity =  ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_CLIPBOARD)
				BREAK				
				CASE 1
					vPosition   =  <<18.9270, 10.8181, 1.4231>>
					fHeading 	=  147.500
					iActivity =  ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_KNEEL_INSPECT)
				BREAK
				CASE 2
					vPosition   =  <<16.7792, -5.5146, 1.3974>>
					fHeading 	=  108.250	
					iActivity =  ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_CROUCH_INSPECT)
				BREAK
				CASE 3
					vPosition   =  <<-7.1085, -21.1569, 0.5107>>
					fHeading 	=  90
					iActivity =  ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_SIT_TABLET)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iStaffMemberIndex
				CASE 0
					vPosition   =  <<24.5542, -5.5771, 1.3974>>
					fHeading 	=  152.250
					iActivity =  ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_CLIPBOARD)
				BREAK
				CASE 1
					vPosition   =  <<19.4205, 2.3224, 1.4031>>
					fHeading 	= 161.460
					iActivity =  ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_MOVE_STUDIO_LIGHT)
				BREAK
				CASE 2//<<-1009.9955, -70.0099, -100.4031>>	
					vPosition   =   <<-2.4395, -18.51511, 0.9944>>//<<-2.5745, -16.7444, 0.9981>>
					fHeading 	=  161.500//142.840
					iActivity =  ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DRINK_COFFEE)
				BREAK
				CASE 3
					//IF MUSICIAN IS NOT IN THE STUDIO
					IF eCurrentDJ != CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
						vPosition   =  <<9.9145, -8.5896, 1.3974>>
						fHeading 	=  98.500
						iActivity =  ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_LEAN_INSPECT)
					ELSE 
						vPosition   =  <<-8.1445, -15.0694, 1.0106>>
						fHeading 	=  180.000
						iActivity =  ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_LEAN_TEXT)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			SWITCH iStaffMemberIndex
				CASE 0
					vPosition   =  <<23.7770, -2.2694, 1.4107>>
					fHeading 	=  264.5800
					iActivity =  ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_STAND_MEDIC_TOD)
				BREAK
				CASE 1
					vPosition   = <<-2.5330, -1.5794, 1.0107>>
					fHeading 	= 270.6400
					iActivity =  ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_E_TEXTING)			
				BREAK
				CASE 2
					vPosition   =  <<24.6820, -10.6444, 1.3981>>
					fHeading 	=  10.250
					iActivity =  ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_CLIPBOARD)
				BREAK
				CASE 3
					vPosition   =  <<-10.8695, -13.6319, 0.5831>>
					fHeading 	=  14.500
					iActivity =  ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_SITTING_LAPTOP)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC



PROC _SET_MUSIC_STUDIO_PED_DATA_LAYOUT(PEDS_DATA &Data, SERVER_PED_DATA &ServerBD, INT iPed, BOOL bSetPedArea = TRUE)
	CLUB_DJS eCurrentDJ = g_clubMusicData.eActiveDJ
	IF g_clubMusicData.eNextDJ != CLUB_DJ_NULL
		eCurrentDJ = g_clubMusicData.eNextDJ
	ENDIF

	SWITCH iPed
	// Local Peds
		CASE 0   ///////////////////////////DR DRE///////////////////////
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_DR_DRE)				
					SWITCH  eCurrentDJ
						CASE CLUB_DJ_DR_DRE_MIX
						CASE CLUB_DJ_DR_DRE_MIX_VOCALS
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_MIXING)
							Data.vPosition 										= <<-0.0045, 0.0099, 0.4031>>//<<13.5615, 1.2173, 1.2971>>//
						BREAK
						CASE CLUB_DJ_DR_DRE_MIX_2	
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_MIXING_2)
							Data.vPosition 										= <<-0.0045, 0.0099, 0.4031>>//<<13.5615, 1.2173, 1.2971>>//
						BREAK	
						CASE CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_DRUMS)
							Data.vPosition 										= <<-0.0045, 0.0099, 0.4031>>//<<13.5615, 1.2173, 1.2971>>//
						BREAK	
						CASE CLUB_DJ_DR_DRE_MUSICIAN_VOCALS
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_VOCALS)
							Data.vPosition 										= <<-0.0045, 0.0099, 0.4031>>//<<13.5615, 1.2173, 1.2971>>//
						BREAK
						CASE CLUB_DJ_DR_DRE_ABSENT								
							Data.vPosition 										= <<5.0555, -10.501, 0.4356>>//<<13.5615, 1.2173, 1.2971>>//
						BREAK
					ENDSWITCH			
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33554432
					Data.iPackedDrawable[1]								= 131073
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0				
					
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)				
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)			
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SYNC_DJ_ANIM_OFTEN)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)
					IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
					OR !g_sMPTunables.bENABLE_FIXER_STUDIO_APPEARANCE 
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
					
					IF eCurrentDJ = CLUB_DJ_DR_DRE_ABSENT
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
					#IF IS_DEBUG_BUILD
					g_DebugAnimSeqDetail.pedActivity[0] = Data.iActivity 
					#ENDIF
				BREAK
			ENDSWITCH
		BREAK
		/////////////////////////////// DRE'S ENGINEER	/////////////////////////
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_ENGINEER)
					SWITCH  eCurrentDJ
						CASE CLUB_DJ_DR_DRE_MIX
						CASE CLUB_DJ_DR_DRE_MIX_VOCALS
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_MIXING)
						BREAK
						CASE CLUB_DJ_DR_DRE_MIX_2	
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_MIXING_2)
						BREAK	
						CASE CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_DRUMS)
						BREAK	
						CASE CLUB_DJ_DR_DRE_MUSICIAN_VOCALS
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_VOCALS)
						BREAK
						CASE CLUB_DJ_DR_DRE_ABSENT
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_DRE_ABSENT)
						BREAK
					ENDSWITCH		
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0					
					Data.vPosition 										= <<-0.0045, 0.0099, 0.4031>>//<<11.4025, 0.5648, 1.2971>>//
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)									
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SYNC_DJ_ANIM_OFTEN)					
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)				
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
					#IF IS_DEBUG_BUILD
					g_DebugAnimSeqDetail.pedActivity[1] = Data.iActivity 
					#ENDIF
				BREAK
			ENDSWITCH
		BREAK
		///////////////////////////// MUSICIAN /////////////////////////////
		CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_MUSICIAN_1)
					SWITCH  eCurrentDJ
						CASE CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_MUSICIAN_DRUMS)
							Data.vPosition 										= <<-0.0045, 0.0099, 0.4031>>
						BREAK	
						CASE CLUB_DJ_DR_DRE_MUSICIAN_VOCALS
							Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_DR_DRE_MUSICIAN_VOCALS)
							Data.vPosition 										= <<-0.0045, 0.0099, 0.4031>>
						BREAK
						DEFAULT																				
							Data.vPosition 										= <<5.0108, -8.1127, 0.4356>>
						BREAK
					ENDSWITCH		
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0				
					
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)				
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIGH_PRIORITY_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SYNC_DJ_ANIM_OFTEN)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_PED)					
					IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
					OR !g_sMPTunables.bENABLE_FIXER_STUDIO_APPEARANCE 
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
					
					IF eCurrentDJ != CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
					AND eCurrentDJ != CLUB_DJ_DR_DRE_MUSICIAN_VOCALS
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
						
					#IF IS_DEBUG_BUILD
					g_DebugAnimSeqDetail.pedActivity[2] = Data.iActivity 
					#ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_STUDIO_ASSISTANT_1)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_RECEPTION)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16908290
					Data.iPackedDrawable[1]								= 65793
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554434
					Data.iPackedTexture[1] 								= 513
					Data.iPackedTexture[2] 								= 1
					Data.vPosition 										= <<-12.5410, -13.2280, 0.6940>>
					Data.vRotation 										= <<0.0000, 0.0000, 18.6748>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 4
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_DJPOOH)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0										
					CLEAR_PEDS_BITSET(Data.iBS)
					GET_RANDOM_DJPOOH_DATA(Data, ServerBD, eCurrentDJ)	
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_RESETTABLE_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 5
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_ENTOURAGE_1)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					
					GET_RANDOM_ENTOURAGE1_POSITION_AND_HEADING(Data.vPosition, Data.vRotation.z, Data.iActivity , ServerBD, eCurrentDJ)
					
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_RESETTABLE_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 6
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_ENTOURAGE_2)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0				
					GET_RANDOM_ENTOURAGE2_POSITION_AND_HEADING(Data.vPosition, Data.vRotation.z,Data.iActivity , ServerBD, eCurrentDJ)
					
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_RESETTABLE_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 7
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_STUDIO_SOUND_ENG)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0				
					GET_STAFF_MEMBER_POSITION_HEADING_AND_ACTIVITY(0, Data.vPosition, Data.vRotation.z, Data.iActivity, ServerBD, eCurrentDJ)
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 8
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_STUDIO_ASSISTANT_1)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0				
					GET_STAFF_MEMBER_POSITION_HEADING_AND_ACTIVITY(1, Data.vPosition, Data.vRotation.z, Data.iActivity, ServerBD, eCurrentDJ)
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 9
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_STUDIO_ASSISTANT_2)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0				
					GET_STAFF_MEMBER_POSITION_HEADING_AND_ACTIVITY(2, Data.vPosition, Data.vRotation.z, Data.iActivity, ServerBD, eCurrentDJ)
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 10
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_STUDIO_PRODUCER)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33554432
					Data.iPackedDrawable[1]								= 256
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0				
					GET_STAFF_MEMBER_POSITION_HEADING_AND_ACTIVITY(3, Data.vPosition, Data.vRotation.z, Data.iActivity, ServerBD, eCurrentDJ)
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE NETWORK_PED_ID_0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_FRANKLIN)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_FRANKLIN)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33620224
					Data.iPackedDrawable[1]								= 65538
					Data.iPackedDrawable[2]								= 16777473
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0					
					Data.vPosition 										= <<-0.0045, 0.0099, 0.4031>>//<<11.4025, 0.5648, 1.2971>>//
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>				
					CLEAR_PEDS_BITSET(Data.iBS)					
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
					SET_PEDS_BIT(Data.iBS,BS_PED_DATA_USE_NETWORK_ANIMS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_PED)
					IF NOT IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ELSE
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_REMOTELY_VISIBLE)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE NETWORK_PED_ID_1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_LAMAR)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_LAMAR)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777216
					Data.iPackedDrawable[1]								= 65793
					Data.iPackedDrawable[2]								= 16843009
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0					
					Data.vPosition 										= <<-0.0045, 0.0099, 0.4031>>//<<11.4025, 0.5648, 1.2971>>//
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)					
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
					SET_PEDS_BIT(Data.iBS,BS_PED_DATA_USE_NETWORK_ANIMS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					IF NOT IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ELSE
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_REMOTELY_VISIBLE)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE NETWORK_PED_ID_2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_STUDIO_ASSISTANT_1)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_PATROL)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0					
					Data.vPosition 										= <<-1004.9483, -53.6589, -100.0031>> -_GET_MUSIC_STUDIO_PED_LOCAL_COORDS_BASE_POSITION()
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)										
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_PATROL)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_ALTERNATIVE_WALK_ANIMATION)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)				
					IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE NETWORK_PED_ID_3
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_STUDIO_ASSISTANT_2)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_PATROL)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0					
					Data.vPosition 										= <<-1009.9588, -83.3802, -100.4031>> - _GET_MUSIC_STUDIO_PED_LOCAL_COORDS_BASE_POSITION()
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)										
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_PATROL)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_ALTERNATIVE_WALK_ANIMATION)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					IF IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE NETWORK_PED_ID_4
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_MUSIC_STUDIO_LAMAR_VFX_PASS)
					Data.iLevel 										= 0			
					Data.vPosition 										= <<-0.0045, 0.0099, 0.4031>>//<<11.4025, 0.5648, 1.2971>>//
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)					
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
					SET_PEDS_BIT(Data.iBS,BS_PED_DATA_USE_NETWORK_ANIMS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHILD_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PROPS_ONLY_NO_PED)
					
					IF NOT IS_LOCAL_PLAYER_IN_MUSIC_STUDIO_SMOKING_ROOM()
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK

		
		
	ENDSWITCH	
	
	// Transform the local coords into world coords.
	IF NOT IS_VECTOR_ZERO(Data.vPosition)
		Data.vPosition = TRANSFORM_LOCAL_COORDS_TO_WORLD_COORDS(_GET_MUSIC_STUDIO_PED_LOCAL_COORDS_BASE_POSITION(), _GET_MUSIC_STUDIO_PED_LOCAL_HEADING_BASE_HEADING(), Data.vPosition)
	ENDIF
	
	// Transform the local heading into world heading.
	IF (Data.vRotation.z != -1.0)
		Data.vRotation.z = TRANSFORM_LOCAL_HEADING_TO_WORLD_HEADING(_GET_MUSIC_STUDIO_PED_LOCAL_HEADING_BASE_HEADING(), Data.vRotation.z)
	ENDIF

ENDPROC

PROC _SET_MUSIC_STUDIO_PED_PARENT_DATA_LAYOUT(PARENT_PED_DATA &ParentPedData, INT iArrayID, INT &iParentPedID[], BOOL bNetworkPed = FALSE)
	
	IF (NOT bNetworkPed)
	ELSE
		SWITCH iArrayID
			CASE 0
				ParentPedData.iParentID							= 0
				ParentPedData.iChildID[0]						= 1
				ParentPedData.iChildID[1]						= 4  //only lighter prop
				ParentPedData.iChildID[2]						= -1
				iParentPedID[iArrayID]							= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC _SET_MUSIC_STUDIO_PED_PARENT_DATA(PARENT_PED_DATA &ParentPedData, INT iLayout, INT iArrayID, INT &iParentPedID[], BOOL bNetworkPed = FALSE)
	UNUSED_PARAMETER(iLayout)	
	_SET_MUSIC_STUDIO_PED_PARENT_DATA_LAYOUT(ParentPedData, iArrayID, iParentPedID, bNetworkPed)		
ENDPROC

PROC _SET_MUSIC_STUDIO_PED_PROP_INDEXES(PED_INDEX &PedID, INT iPed, INT iLayout, PED_MODELS ePedModel)
	UNUSED_PARAMETER(ePedModel)
	UNUSED_PARAMETER(iLayout)
	UNUSED_PARAMETER(PedID)
	UNUSED_PARAMETER(iPed)
	SWITCH iPed	
		CASE 1
			SET_PED_PROP_INDEX(PedID, ANCHOR_HEAD, 0, 0)
		BREAK
		CASE 2
			SET_PED_PROP_INDEX(PedID, ANCHOR_HEAD, 0, 0)
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0, 0)
		BREAK
		CASE 4
			SET_PED_PROP_INDEX(PedID, ANCHOR_HEAD, 0, 0)
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0, 0)
		BREAK
		CASE 7
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0, 0)
		BREAK
		CASE 9
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0, 0)
		BREAK
		CASE NETWORK_PED_ID_0
			SET_PED_PROP_INDEX(PedID, ANCHOR_HEAD, 0, 0)
		BREAK
		CASE NETWORK_PED_ID_1
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0, 0)
		BREAK
	ENDSWITCH 
ENDPROC
PROC _SET_MUSIC_STUDIO_PED_HEAD_TRACKING_DATA_LAYOUT(HEAD_TRACKING_DATA &HeadtrackingData, INT iArrayID, INT &iHeadTrackingPedID[], BOOL bNetworkPed = FALSE)
	
	IF (bNetworkPed)
//		SWITCH iArrayID
//		ENDSWITCH
	ELSE
		SWITCH iArrayID
			CASE 0
				HeadtrackingData.iPedID							= 3
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 7.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
			CASE 1
				HeadtrackingData.iPedID							= 4
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 7.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC _SET_MUSIC_STUDIO_PED_HEAD_TRACKING_DATA(HEAD_TRACKING_DATA &HeadtrackingData, INT iLayout, INT iArrayID, INT &iHeadTrackingPedID[], BOOL bNetworkPed = FALSE)
	UNUSED_PARAMETER(iLayout)
	
	_SET_MUSIC_STUDIO_PED_HEAD_TRACKING_DATA_LAYOUT(HeadtrackingData, iArrayID, iHeadTrackingPedID, bNetworkPed)
	
ENDPROC
//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ FUNCTIONS TO IMPLEMENT ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _SHOULD_MUSIC_STUDIO_PED_SCRIPT_LAUNCH()
	RETURN IS_PLAYER_IN_MUSIC_STUDIO(PLAYER_ID())
ENDFUNC

FUNC BOOL _IS_MUSIC_STUDIO_PARENT_A_SIMPLE_INTERIOR
	RETURN TRUE
ENDFUNC

PROC _SET_MUSIC_STUDIO_PED_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, INT iPed, INT iLayout, BOOL bSetPedArea = TRUE)
	UNUSED_PARAMETER(ServerBD)
	UNUSED_PARAMETER(ePedLocation)
	
	PRINTLN("_SET_MUSIC_STUDIO_PED_DATA PED ", iPed)
	PRINTLN("_SET_MUSIC_STUDIO_PED_DATA USING LAYOUT ", iLayout)
	
	_SET_MUSIC_STUDIO_PED_DATA_LAYOUT(Data, ServerBD, iPed, bSetPedArea)
	
ENDPROC

FUNC INT _GET_MUSIC_STUDIO_NETWORK_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	UNUSED_PARAMETER(ePedLocation)
	UNUSED_PARAMETER(ServerBD)
	RETURN 5
ENDFUNC

FUNC INT _GET_MUSIC_STUDIO_LOCAL_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	
	INT iPed
	INT iActivePeds = 0
	PEDS_DATA tempData
	
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iPed
		tempData.vPosition = NULL_VECTOR()
		_SET_MUSIC_STUDIO_PED_DATA(ePedLocation, ServerBD, tempData, iPed, ServerBD.iLayout)
		IF NOT IS_VECTOR_ZERO(tempData.vPosition)
			iActivePeds++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iActivePeds
ENDFUNC

FUNC INT _GET_MUSIC_STUDIO_SERVER_PED_LAYOUT_TOTAL()
	RETURN 3
ENDFUNC

FUNC INT _GET_MUSIC_STUDIO_SERVER_PED_LAYOUT()	
	RETURN GET_RANDOM_INT_IN_RANGE(0, 3)
ENDFUNC

FUNC INT _GET_MUSIC_STUDIO_SERVER_PED_LEVEL()
	RETURN 0 // Ped levels 0, 1 & 2 spawn by default
ENDFUNC

PROC _SET_MUSIC_STUDIO_PED_SERVER_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
	UNUSED_PARAMETER(ePedLocation)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ServerBD.iLayout 			= _GET_MUSIC_STUDIO_SERVER_PED_LAYOUT()
		ServerBD.iLevel 			= _GET_MUSIC_STUDIO_SERVER_PED_LEVEL()
		ServerBD.iMaxLocalPeds 		= _GET_MUSIC_STUDIO_LOCAL_PED_TOTAL(ServerBD)
		ServerBD.iMaxNetworkPeds	= _GET_MUSIC_STUDIO_NETWORK_PED_TOTAL(ServerBD)
	ENDIF
	g_iPedLayout = ServerBD.iLayout 
	PRINTLN("[AM_MP_PEDS] _SET_MUSIC_STUDIO_PED_SERVER_DATA - Layout: ", ServerBD.iLayout)
	PRINTLN("[AM_MP_PEDS] _SET_MUSIC_STUDIO_PED_SERVER_DATA - Level: ", ServerBD.iLevel)
	PRINTLN("[AM_MP_PEDS] _SET_MUSIC_STUDIO_PED_SERVER_DATA - Max Local Peds: ", ServerBD.iMaxLocalPeds)
	PRINTLN("[AM_MP_PEDS] _SET_MUSIC_STUDIO_PED_SERVER_DATA - Max Network Peds: ", ServerBD.iMaxNetworkPeds)
ENDPROC

FUNC BOOL _IS_PLAYER_IN_MUSIC_STUDIO_PARENT_PROPERTY(PLAYER_INDEX playerID)
	RETURN IS_PLAYER_IN_MUSIC_STUDIO(playerID)
ENDFUNC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED MODELS ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_MUSIC_STUDIO_LOCAL_PED_PROPERTIES(PED_INDEX &PedID, INT iPed)
	UNUSED_PARAMETER(iPed)
	
	SET_ENTITY_CAN_BE_DAMAGED(PedID, FALSE)
	SET_PED_AS_ENEMY(PedID, FALSE)
	SET_CURRENT_PED_WEAPON(PedID, WEAPONTYPE_UNARMED, TRUE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedID, TRUE)
	SET_PED_RESET_FLAG(PedID, PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromExplosions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DisableExplosionReactions, TRUE)
	
	SET_PED_CAN_EVASIVE_DIVE(PedID, FALSE)
	SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(PedID, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PedID, FALSE)
	SET_PED_CAN_RAGDOLL(PedID, FALSE)
	
	CLEAR_PED_TASKS(PedID)

ENDPROC

PROC _SET_MUSIC_STUDIO_NETWORK_PED_PROPERTIES(NETWORK_INDEX &NetworkPedID, INT &iPedDataBS[PEDS_DATA_BITSET_ARRAY_SIZE])
	NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NETWORK_GET_ENTITY_FROM_NETWORK_ID(NetworkPedID), TRUE)
	SET_NETWORK_ID_CAN_MIGRATE(NetworkPedID, FALSE)
		
	IF IS_PEDS_BIT_SET(iPedDataBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, FALSE)
	ELSE
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, TRUE)
	ENDIF
	
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED SPEECH ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_MUSIC_STUDIO_PED_SPEECH_DATA(SPEECH_DATA &SpeechData, INT iLayout, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	UNUSED_PARAMETER(iLayout)
	
	IF (NOT bNetworkPed)
		SWITCH iArrayID
			CASE 0
				SpeechData.iPedID							= 1
				SpeechData.fGreetSpeechDistance				= 3.5
				SpeechData.fByeSpeechDistance				= 5.0
				SpeechData.fListenDistance					= 7.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
				SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_TIMED_GREETING)
				
			BREAK
			CASE 1
				SpeechData.iPedID							= 4
				SpeechData.fGreetSpeechDistance				= 3.100
				SpeechData.fByeSpeechDistance				= 3.940
				SpeechData.fListenDistance					= 5.080
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
				SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_DONT_BROADCAST_LOITER)
				SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_TIMED_GREETING)
			BREAK
			CASE 2
				SpeechData.iPedID							= 5
				SpeechData.fGreetSpeechDistance				= 3.100
				SpeechData.fByeSpeechDistance				= 3.940
				SpeechData.fListenDistance					= 5.080
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
				SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_DONT_BROADCAST_LOITER)
				SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_TIMED_GREETING)
			BREAK
			CASE 3
				SpeechData.iPedID							= 6
				SpeechData.fGreetSpeechDistance				= 3.100
				SpeechData.fByeSpeechDistance				= 3.940
				SpeechData.fListenDistance					= 5.080
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
				SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_DONT_BROADCAST_LOITER)
				SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_TIMED_GREETING)
			BREAK
		ENDSWITCH
	ELSE
		SWITCH iArrayID
			CASE 0
				SpeechData.iPedID							= iArrayID
				SpeechData.fGreetSpeechDistance				= 5.5
				SpeechData.fByeSpeechDistance				= 8.0
				SpeechData.fListenDistance					= 10.0
				iSpeechPedID[iArrayID]						= iArrayID
				SET_PEDS_BIT(speechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_MUTE_DURING_CUTSCENE)
			BREAK
			CASE 1
				SpeechData.iPedID							= iArrayID
				SpeechData.fGreetSpeechDistance				= 5.5
				SpeechData.fByeSpeechDistance				= 8.0
				SpeechData.fListenDistance					= 10.0
				iSpeechPedID[iArrayID]						= iArrayID
				SET_PEDS_BIT(speechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_MUTE_DURING_CUTSCENE)
			BREAK		
		ENDSWITCH
	ENDIF
ENDPROC

FUNC BOOL _CAN_MUSIC_STUDIO_PED_PLAY_SPEECH(PED_INDEX PedID, INT iPed, INT iLayout, PED_SPEECH ePedSpeech)
	UNUSED_PARAMETER(PedID)
	UNUSED_PARAMETER(iLayout)
	
	// Generic conditions
	IF NOT IS_ENTITY_ALIVE(PedID)
		PRINTLN("[AM_MP_PEDS] _CAN_MUSIC_STUDIO_PED_PLAY_SPEECH - Bail Reason: Ped is not alive")
		RETURN FALSE
	ENDIF
	
	IF NOT g_bInitPedsCreated
		PRINTLN("[AM_MP_PEDS] _CAN_MUSIC_STUDIO_PED_PLAY_SPEECH - Bail Reason: Waiting for all peds to be created first")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_MUSIC_STUDIO_PED_PLAY_SPEECH - Bail Reason: Player is walking in or out of interior")
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT()
		PRINTLN("[AM_MP_PEDS] _CAN_MUSIC_STUDIO_PED_PLAY_SPEECH - Bail Reason: Screen is fading out")
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		PRINTLN("[AM_MP_PEDS] _CAN_MUSIC_STUDIO_PED_PLAY_SPEECH - Bail Reason: Browser is open")
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE() OR IS_PLAYER_IN_SIMPLE_CUTSCENE(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_MUSIC_STUDIO_PED_PLAY_SPEECH - Bail Reason: Cutscene is active")
		RETURN FALSE
	ENDIF
	
	IF (IS_PLAYER_IN_CORONA() OR IS_TRANSITION_SESSION_LAUNCHING() OR IS_TRANSITION_SESSION_RESTARTING())
		PRINTLN("[AM_MP_PEDS] _CAN_MUSIC_STUDIO_PED_PLAY_SPEECH - Bail Reason: Player in corona")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_INTERACTING_WITH_PLANNING_BOARD()
		PRINTLN("[AM_MP_PEDS] _CAN_MUSIC_STUDIO_PED_PLAY_SPEECH - Bail Reason: Player is interacting with the planning board")
		RETURN FALSE
	ENDIF
	
	//url:bugstar:7438040
	//Blocking speech during vocals when Dre and Anderson are on mics
	IF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MUSICIAN_VOCALS
	AND g_clubMusicData.iCurrentTrackTimeMS>=  50000 	//50s        -After Lola says Mic is on, before Anderson starts rapping
	AND g_clubMusicData.iCurrentTrackTimeMS<= 100000	//1min 40 sec	- After Dre finishes his last line
		RETURN FALSE
	ENDIF
	
	// Specific conditions
	SWITCH iPed
		CASE 1 // ENGINEER (DONT PLAY SPEECH DURING DRE PERFORMANCE)
			IF g_clubMusicData.eActiveDJ!= CLUB_DJ_DR_DRE_ABSENT
				RETURN FALSE
			ENDIF 
		BREAK
		
		CASE 4
		CASE 5
			IF ePedSpeech = PED_SPH_PT_BYE
			AND IS_PED_IN_MIXING_ROOM_AREA(PLAYER_PED_ID())
				RETURN FALSE
			ENDIF	
		BREAK
		CASE 6
			IF ePedSpeech = PED_SPH_PT_LOITER
			AND g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_ABSENT
				RETURN FALSE
			ENDIF
			
			IF ePedSpeech = PED_SPH_PT_BYE
			AND IS_PED_IN_MIXING_ROOM_AREA(PLAYER_PED_ID())
				RETURN FALSE
			ENDIF	
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
FUNC PED_SPEECH _GET_MUSIC_STUDIO_PED_SPEECH_TYPE(INT iPed, PED_ACTIVITIES ePedActivity, INT iSpeech)
	UNUSED_PARAMETER(ePedActivity)
	PED_SPEECH eSpeech = PED_SPH_INVALID	
	SWITCH iPed
		CASE 1			
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
				CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK								
			ENDSWITCH
		BREAK
		CASE 4			
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
				CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
				CASE 3	eSpeech = PED_SPH_PT_LOITER											BREAK
			ENDSWITCH
		BREAK
		CASE 5			
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
				CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
				CASE 3	eSpeech = PED_SPH_PT_LOITER											BREAK
			ENDSWITCH
		BREAK
		CASE 6			
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
				CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
				CASE 3	eSpeech = PED_SPH_PT_LOITER											BREAK
			ENDSWITCH
		BREAK
		CASE NETWORK_PED_ID_0		
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
				CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
				CASE 3	eSpeech = PED_SPH_PT_LOITER											BREAK
				CASE 4	eSpeech = PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_ACTOR					BREAK
				CASE 5	eSpeech = PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_WEAK					BREAK
				CASE 6	eSpeech = PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_SIMEON				BREAK
				CASE 7	eSpeech = PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_AUNTIE				BREAK
				CASE 8	eSpeech = PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_PHONE					BREAK
				CASE 9	eSpeech = PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_CHOP					BREAK
				CASE 10	eSpeech = PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_FAKE					BREAK
				CASE 11	eSpeech = PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_MOON					BREAK
				CASE 12	eSpeech = PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_FOOD					BREAK
				CASE 13	eSpeech = PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_TONYA					BREAK
				CASE 14	eSpeech = PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_SUPERSTONED			BREAK
				CASE 15	eSpeech = PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_EYEDROPS				BREAK
			ENDSWITCH
		BREAK
		CASE NETWORK_PED_ID_1	
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
				CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
				CASE 3	eSpeech = PED_SPH_PT_LOITER											BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN eSpeech
ENDFUNC

PROC _GET_MUSIC_STUDIO_PED_CONVO_DATA(PED_CONVO_DATA &convoData, INT iPed, PED_ACTIVITIES ePedActivity, PED_SPEECH ePedSpeech)
	UNUSED_PARAMETER(ePedActivity)
	
	RESET_PED_CONVO_DATA(convoData)
	
	INT iRandSpeech
	
	SWITCH iPed
		CASE 1
			convoData.sCharacterVoice = "FIX_ENGINEER"  
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
					IF iRandSpeech = 0
						convoData.sRootName = "GREET"
					ELSE
						convoData.sRootName = "HOWSITGOING"
					ENDIF
				BREAK
				CASE PED_SPH_PT_BYE
					convoData.sRootName = "BYE"
				BREAK
				CASE PED_SPH_PT_BUMP
					convoData.sRootName = "BUMP"
				BREAK	
			ENDSWITCH
		BREAK
		CASE 4 //DJ POOH
			convoData.sCharacterVoice = "HS4_POOH"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
					IF iRandSpeech = 0
						IF HAS_LOCAL_PLAYER_COMPLETED_FIXER_STORY_AS_LEADER()
							convoData.sRootName = "GREET_FINALE_COMPLETE"
						ELIF HAS_LOCAL_PLAYER_COMPLETED_FIXER_INTRO_MISSION()
							convoData.sRootName = "GREET_FINALE_NOT_COMPLETE"
						ELSE
							convoData.sRootName = "GREET_OTHER_PLAYER"
						ENDIF
					ELSE
						IF HAS_LOCAL_PLAYER_COMPLETED_FIXER_STORY_AS_LEADER()
							convoData.sRootName = "HOWSITGOING_FINALE_COMPLETE"
						ELIF HAS_LOCAL_PLAYER_COMPLETED_FIXER_INTRO_MISSION()
							convoData.sRootName = "HOWSITGOING_FINALE_NOT_COMPLETE"
						ELSE
							convoData.sRootName = "HOWSITGOING_OTHER_PLAYER"
						ENDIF
					ENDIF
				BREAK
				CASE PED_SPH_PT_BYE
					IF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_ABSENT
						convoData.sRootName = "BYE"
					ENDIF
				BREAK
				CASE PED_SPH_PT_BUMP
					convoData.sRootName = "BUMP"
				BREAK
				CASE PED_SPH_PT_LOITER
					IF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_ABSENT
						iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
						IF iRandSpeech = 0
							convoData.sRootName = "IDLE_DRE_NOT_PRESENT"
						ELSE
							IF HAS_LOCAL_PLAYER_COMPLETED_FIXER_STORY_AS_LEADER()
								convoData.sRootName = "IDLE_FINALE_COMPLETE"
							ELIF HAS_LOCAL_PLAYER_COMPLETED_FIXER_INTRO_MISSION()
								convoData.sRootName = "IDLE_FINALE_NOT_COMPLETE"
							ELSE
								convoData.sRootName = "IDLE_OTHER_PLAYER"
							ENDIF
						ENDIF
					ELIF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
					OR g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MUSICIAN_VOCALS
						iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
						IF iRandSpeech = 0
							convoData.sRootName = "COMMENT_MUSIC_ARTIST"
						ELSE
							convoData.sRootName = "COMMENT_MUSIC_ARTIST2"
						ENDIF
						
					ELIF (g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MIX
					OR  g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MIX_2
					OR  g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MIX_VOCALS)
					AND g_clubMusicData.iCurrentTrackTimeMS >= 60000
						convoData.sRootName = "COMMENT_MUSIC"
					ELSE
						iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
						IF iRandSpeech = 0
							convoData.sRootName = "COMMENT_RADIO"
						ELSE
							IF HAS_LOCAL_PLAYER_COMPLETED_FIXER_STORY_AS_LEADER()
								convoData.sRootName = "IDLE_FINALE_COMPLETE"
							ELIF HAS_LOCAL_PLAYER_COMPLETED_FIXER_INTRO_MISSION()
								convoData.sRootName = "IDLE_FINALE_NOT_COMPLETE"
							ELSE
								convoData.sRootName = "IDLE_OTHER_PLAYER"
							ENDIF
						ENDIF		
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 5
			convoData.sCharacterVoice = "HS4_PARTYGIRL1"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					IF NOT g_bBlockEntourageGirlGreeting
						g_bBlockEntourageGirlGreeting = TRUE
						iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
						IF iRandSpeech = 0
							convoData.sRootName = "HOWSITGOING"
						ELSE
							convoData.sRootName = "GREET"
						ENDIF
					ELSE
						g_bBlockEntourageGirlGreeting = FALSE
					ENDIF
				BREAK
				CASE PED_SPH_PT_BYE
					IF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_ABSENT
						convoData.sRootName = "BYE"
					ENDIF
				BREAK
				CASE PED_SPH_PT_BUMP
					convoData.sRootName = "BUMP"
				BREAK
				CASE PED_SPH_PT_LOITER
					//DRE ABSENT
					IF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_ABSENT
						convoData.sRootName = "IDLE_DRE_NOT_PRESENT"
					//DRE PRESENT, ARTIST ABSENT
					ELIF (g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MIX
					OR  g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MIX_2
					OR  g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MIX_VOCALS)
						//IF MUSIC IS ALREADY PLAYING
						IF g_clubMusicData.iCurrentTrackTimeMS >= 60000
							convoData.sRootName = "COMMENT_MUSIC"
						ELSE
							iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
							IF iRandSpeech = 0
								convoData.sRootName = "COMMENT_DRE"
							ELSE
								convoData.sRootName = "IDLE"
							ENDIF
						ENDIF
					// ARTIST PRESENT
					ELIF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
					OR g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MUSICIAN_VOCALS
						convoData.sRootName = "COMMENT_MUSIC_ARTIST"	
					ENDIF
				BREAK			
			ENDSWITCH
		BREAK
		CASE 6
			convoData.sCharacterVoice = "HS4_PARTYGIRL2"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					IF NOT g_bBlockEntourageGirlGreeting
						g_bBlockEntourageGirlGreeting = TRUE
						iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
						IF iRandSpeech = 0
							convoData.sRootName = "HOWSITGOING"
						ELSE
							convoData.sRootName = "GREET"
						ENDIF
					ELSE
						g_bBlockEntourageGirlGreeting = FALSE
					ENDIF
				BREAK
				CASE PED_SPH_PT_BYE
					IF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_ABSENT
						convoData.sRootName = "BYE"
					ENDIF
				BREAK
				CASE PED_SPH_PT_BUMP
					convoData.sRootName = "BUMP"
				BREAK
				CASE PED_SPH_PT_LOITER
					//url:bugstar:7442070 - Music Studio: Peds triggered lines while there was no music playing and sounds weird
//					//DRE ABSENT
//					IF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_ABSENT
//						convoData.sRootName = "IDLE_DRE_NOT_PRESENT"
					//DRE PRESENT, ARTIST ABSENT
					IF (g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MIX
					OR  g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MIX_2
					OR  g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MIX_VOCALS)
						//IF MUSIC IS ALREADY PLAYING
						IF g_clubMusicData.iCurrentTrackTimeMS >= 60000
							convoData.sRootName = "COMMENT_MUSIC"
						ELSE
							iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
							IF iRandSpeech = 0
								convoData.sRootName = "COMMENT_DRE"
							ELSE
								convoData.sRootName = "IDLE"
							ENDIF
						ENDIF
					// ARTIST PRESENT
					ELIF g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MUSICIAN_DRUMS
					OR g_clubMusicData.eActiveDJ = CLUB_DJ_DR_DRE_MUSICIAN_VOCALS
						convoData.sRootName = "COMMENT_MUSIC_ARTIST"	
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE NETWORK_PED_ID_0
			convoData.iSpeakerID                = 2
			convoData.sCharacterVoice 			= "FIX_FRANKLIN"			
			convoData.sSubtitleTextBlock		= "FXIGAUD"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					convoData.sRootName = "WEED_ROOM_WELCOME"					
				BREAK
				CASE PED_SPH_PT_BYE
					IF DOES_PLAYER_OWN_A_FIXER_HQ(PLAYER_ID())
						convoData.sRootName = "BYE_OWNER"
					ELSE
						convoData.sRootName = "BYE_OTHER_PLAYER"
					ENDIF
				BREAK
				CASE PED_SPH_PT_BUMP
					convoData.sRootName = "BUMP"
				BREAK
				CASE PED_SPH_PT_LOITER
					SWITCH GET_RANDOM_INT_IN_RANGE(0,4)
						CASE 0	convoData.sRootName = "WEED_ROOM_BANTER" 	BREAK
						CASE 1	convoData.sRootName = "WEED_ROOM_MUSIC" 	BREAK
						CASE 2	convoData.sRootName = "WEED_ROOM_SMOKE"		BREAK
						CASE 3	convoData.sRootName = "WEED_ROOM_HIGH" 		BREAK
					ENDSWITCH
				BREAK
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_ACTOR			
					convoData.bAddChildPedsToConvo		= TRUE		
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR1_1A"
				BREAK
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_WEAK
					convoData.bAddChildPedsToConvo		= TRUE		
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR2_1A"
				BREAK
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_SIMEON
					convoData.bAddChildPedsToConvo		= TRUE		
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR3_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_AUNTIE
					convoData.bAddChildPedsToConvo		= TRUE		
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR4_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_PHONE
					convoData.bAddChildPedsToConvo		= TRUE		
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR5_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_CHOP
					convoData.bAddChildPedsToConvo		= TRUE		
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR6_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_FAKE
					convoData.bAddChildPedsToConvo		= TRUE		
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR7_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_MOON
					convoData.bAddChildPedsToConvo		= TRUE		
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR8_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_FOOD
					convoData.bAddChildPedsToConvo		= TRUE		
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR9_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_TONYA
					convoData.bAddChildPedsToConvo		= TRUE		
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR10_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_SUPERSTONED
					convoData.bAddChildPedsToConvo		= TRUE		
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR11_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_EYEDROPS
					convoData.bAddChildPedsToConvo		= TRUE		
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR12_1A"
				BREAK						
			ENDSWITCH
		BREAK
		CASE NETWORK_PED_ID_1
			convoData.iSpeakerID                = 6
			convoData.sCharacterVoice			= "FIX_LAMAR"		
			convoData.sSubtitleTextBlock		= "FXIGAUD"		
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					convoData.sRootName = "WEED_ROOM_WELCOME"					
				BREAK
				CASE PED_SPH_PT_BYE
					IF DOES_PLAYER_OWN_A_FIXER_HQ(PLAYER_ID())
						convoData.sRootName = "BYE_OWNER"
					ELSE
						convoData.sRootName = "BYE_OTHER_PLAYER"
					ENDIF
				BREAK
				CASE PED_SPH_PT_BUMP
					convoData.sRootName = "BUMP"
				BREAK
				CASE PED_SPH_PT_LOITER
					SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)
						CASE 0	convoData.sRootName = "WEED_ROOM_BANTER"	BREAK
						CASE 1	convoData.sRootName = "WEED_ROOM_MUSIC"		BREAK
						CASE 2	convoData.sRootName = "WEED_ROOM_SMOKE"		BREAK
						CASE 3	convoData.sRootName = "WEED_ROOM_HIGH"		BREAK
					ENDSWITCH
				BREAK
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_ACTOR				
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR1_1A"
				BREAK
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_WEAK	
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR2_1A"
				BREAK
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_SIMEON	
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR3_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_AUNTIE
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR4_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_PHONE	
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR5_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_CHOP		
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR6_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_FAKE	
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR7_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_MOON	
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR8_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_FOOD	
					g_bWeedRoomStartingFoodDialogue = TRUE
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR9_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_TONYA		
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR10_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_SUPERSTONED		
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR11_1A"
				BREAK	
				CASE PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_EYEDROPS
					convoData.bConvoRequiresAnimation	= TRUE
					convoData.bPlayAsConvo				= TRUE
					convoData.sRootName					= "FXIG_WR12_1A"
				BREAK						
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC PED_SPEECH _GET_MUSIC_STUDIO_PED_CONTROLLER_SPEECH(PED_SPEECH &eCurrentSpeech, INT iPed, PED_ACTIVITIES ePedActivity)
	
	INT iSpeech
	INT iAttempts = 0
	INT iMaxSpeech
	INT iRandSpeech
	PED_SPEECH eSpeech
	
	INT iMaxControllerSpeechTypes = 0
	INT iControllerSpeechTypes[PED_SPH_TOTAL]
	
	SWITCH iPed
		CASE 1
		CASE 4
		CASE 5
		CASE 6
		CASE NETWORK_PED_ID_0
		//CASE 12		
			// Description: Default simply selects a new speech to play that isn't the same as the previous speech.
			PED_CONVO_DATA convoData
			
			// Populate the iControllerSpeechTypes array with all the controller speech type IDs from _GET_GENREIC_PED_SPEECH_TYPE
			REPEAT PED_SPH_TOTAL iSpeech
				eSpeech = _GET_MUSIC_STUDIO_PED_SPEECH_TYPE(iPed, ePedActivity, iSpeech)
				IF (eSpeech > PED_SPH_PT_TOTAL AND eSpeech < PED_SPH_CT_TOTAL)
					RESET_PED_CONVO_DATA(convoData)
					_GET_MUSIC_STUDIO_PED_CONVO_DATA(convoData, iPed, ePedActivity, eSpeech)
					IF IS_CONVO_DATA_VALID(convoData)
						iControllerSpeechTypes[iMaxControllerSpeechTypes] = iSpeech
						iMaxControllerSpeechTypes++
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF (iMaxControllerSpeechTypes > 1)
				iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
				eSpeech = _GET_MUSIC_STUDIO_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
				
				// Ensure speech type is different from previous
				WHILE (eSpeech = eCurrentSpeech AND iAttempts < 10)
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
					eSpeech = _GET_MUSIC_STUDIO_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
					iAttempts++
				ENDWHILE
				
				// Randomising failed to find new speech type. Manually set it.
				IF (iAttempts >= 10)
					REPEAT iMaxSpeech iSpeech
						eSpeech = _GET_MUSIC_STUDIO_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iSpeech])
						IF (eSpeech != eCurrentSpeech)
							BREAKLOOP
						ENDIF
					ENDREPEAT
				ENDIF
				
			ELSE
				eSpeech = _GET_MUSIC_STUDIO_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[0])
			ENDIF
			
		BREAK
	ENDSWITCH
	
	eCurrentSpeech = eSpeech
	RETURN eSpeech
ENDFUNC

PROC _GET_MUSIC_STUDIO_PED_PROP_ANIM_DATA(PED_ACTIVITIES eActivity, PED_PROP_ANIM_DATA &pedPropAnimData, INT iProp = 0, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bHeeledPed = FALSE)
	UNUSED_PARAMETER(eMusicIntensity)
	UNUSED_PARAMETER(ePedMusicIntensity)
	UNUSED_PARAMETER(bDancingTransition)
	UNUSED_PARAMETER(bHeeledPed)
	RESET_PED_PROP_ANIM_DATA(pedPropAnimData)
	
	pedPropAnimData.fBlendInDelta						= SLOW_BLEND_IN
	pedPropAnimData.fBlendOutDelta						= SLOW_BLEND_OUT
	pedPropAnimData.fMoverBlendInDelta					= SLOW_BLEND_IN
	
	SWITCH eActivity
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MIXING
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MIXING_2
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_DRUMS
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_VOCALS
			pedPropAnimData.sPropAnimDict 		= "anim@scripted@freemode_npc@fix_astu_prod_mix@"
			IF eActivity = PED_ACT_MUSIC_STUDIO_DR_DRE_DRUMS
				pedPropAnimData.sPropAnimDict 		= "anim@scripted@freemode_npc@FIX_ASTU_PROD_DRUMS@"
			ELIF  eActivity = PED_ACT_MUSIC_STUDIO_DR_DRE_VOCALS
				pedPropAnimData.sPropAnimDict 		= "anim@scripted@freemode_npc@FIX_ASTU_PROD_VOCALS@"	
			ENDIF
			pedPropAnimData.iFlags = ENUM_TO_INT(SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS )
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_DJ_HEADPHONE_ANIM_CLIP_BASE(eActivity, iClip), "", "")
				BREAK
				CASE 1
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_chair")
				BREAK						
				CASE 2
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_mixer_01a")
				BREAK
				CASE 3
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_mixer_01b")
				BREAK				
				CASE 4
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_mixer_02a")
				BREAK
				CASE 5
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_mixer_02a^1")
				BREAK
				CASE 6
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_mixer_02b")
				BREAK
				CASE 7
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_mixer_02b^1")
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_DRE_ABSENT	
			pedPropAnimData .sPropAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig12_idle@"
			SWITCH iClip
				CASE 0		
					pedPropAnimData.sPropAnimClip 		= "eng_ig12_idle_a_chair"
				BREAK
				CASE 1
					pedPropAnimData.sPropAnimClip 		= "eng_ig12_idle_b_chair"
				BREAK
				CASE 2
					pedPropAnimData.sPropAnimClip 		= "eng_ig12_idle_c_chair"
				BREAK
				CASE 3
					pedPropAnimData.sPropAnimClip 		= "eng_ig12_idle_d_chair"
				BREAK
				CASE 4
					pedPropAnimData.sPropAnimClip 		= "eng_ig12_idle_e_chair"
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_MIXING
			pedPropAnimData.iFlags =  ENUM_TO_INT(SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS )
			pedPropAnimData.sPropAnimDict 		= "anim@scripted@freemode_npc@fix_astu_prod_mix@"
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_chair")
				BREAK
			ENDSWITCH		
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_MIXING_2
			pedPropAnimData.iFlags =  ENUM_TO_INT(SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS )
			pedPropAnimData.sPropAnimDict 		= "anim@scripted@freemode_npc@fix_astu_prod_mix@"
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_chair")
				BREAK
			ENDSWITCH		
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_VOCALS
			pedPropAnimData.iFlags =  ENUM_TO_INT(SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS )
			pedPropAnimData.sPropAnimDict 		= "anim@scripted@freemode_npc@FIX_ASTU_PROD_VOCALS@"	
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_chair")
				BREAK
			ENDSWITCH		
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_DRUMS
			pedPropAnimData.iFlags =  ENUM_TO_INT(SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS )
			pedPropAnimData.sPropAnimDict 		= "anim@scripted@freemode_npc@FIX_ASTU_PROD_DRUMS@"
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_chair")
				BREAK
			ENDSWITCH		
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MUSICIAN_DRUMS
			pedPropAnimData.sPropAnimDict 		= "anim@scripted@freemode_npc@FIX_ASTU_PROD_DRUMS@"
			pedPropAnimData.iFlags = ENUM_TO_INT(SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS )
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_DJ_HEADPHONE_ANIM_CLIP_BASE(eActivity, iClip), "", "")
				BREAK
				CASE 1
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_lstick")
				BREAK
				CASE 2
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_rstick")
				BREAK
				CASE 3
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_drums")
				BREAK
				CASE 4
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_stool")
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MUSICIAN_VOCALS
			pedPropAnimData.sPropAnimDict 		= "anim@scripted@freemode_npc@FIX_ASTU_PROD_VOCALS@"	
			pedPropAnimData.iFlags = ENUM_TO_INT(SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS )
			SWITCH iProp
				CASE 0
					pedPropAnimData.sPropAnimClip = BUILD_DJ_PED_ANIM_CLIP(GET_DJ_HEADPHONE_ANIM_CLIP_BASE(eActivity, iClip), "", "")
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_COUCH
			pedPropAnimData.sPropAnimDict = "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			SWITCH iClip
				CASE 0			pedPropAnimData.sPropAnimClip = "sit_base_joint"				BREAK
				CASE 1			pedPropAnimData.sPropAnimClip = "sit_idle_01_joint"				BREAK
				CASE 2			pedPropAnimData.sPropAnimClip = "sit_idle_02_joint"				BREAK
				CASE 3			pedPropAnimData.sPropAnimClip = "sit_idle_03_joint"				BREAK
				CASE 4			pedPropAnimData.sPropAnimClip = "sit_idle_04_joint"				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_STAND
			pedPropAnimData.sPropAnimDict = "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			SWITCH iClip
				CASE 0			pedPropAnimData.sPropAnimClip = "stand_base_joint"					BREAK
				CASE 1			pedPropAnimData.sPropAnimClip = "stand_idle_01_joint"				BREAK
				CASE 2			pedPropAnimData.sPropAnimClip = "stand_idle_02_joint"				BREAK
				CASE 3			pedPropAnimData.sPropAnimClip = "stand_idle_03_joint"				BREAK
				CASE 4			pedPropAnimData.sPropAnimClip = "stand_idle_04_joint"				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_DRINK_BAR
			pedPropAnimData.sPropAnimDict = "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			SWITCH iClip
				CASE 0			pedPropAnimData.sPropAnimClip = "drink_base_glass1"					BREAK
				CASE 1			pedPropAnimData.sPropAnimClip = "drink_idle_01_glass1"				BREAK
				CASE 2			pedPropAnimData.sPropAnimClip = "drink_idle_02_glass1"				BREAK
				CASE 3			pedPropAnimData.sPropAnimClip = "drink_idle_03_glass1"				BREAK
				CASE 4			pedPropAnimData.sPropAnimClip = "drink_idle_04_glass1"				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_TEXT_STAND
			pedPropAnimData.sPropAnimDict = "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			SWITCH iClip
				CASE 0			pedPropAnimData.sPropAnimClip = "text_base_phone1"					BREAK
				CASE 1			pedPropAnimData.sPropAnimClip = "text_idle_01_phone1"				BREAK
				CASE 2			pedPropAnimData.sPropAnimClip = "text_idle_02_phone1"				BREAK
				CASE 3			pedPropAnimData.sPropAnimClip = "text_idle_03_phone1"				BREAK
				CASE 4			pedPropAnimData.sPropAnimClip = "text_idle_04_phone1"				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_DRINK_SIT
			pedPropAnimData.sPropAnimDict = "anim@scripted@freemode_npc@fix_dre_studio_entourage@"
			SWITCH iClip
				CASE 0			pedPropAnimData.sPropAnimClip = "base_glass"					BREAK
				CASE 1			pedPropAnimData.sPropAnimClip = "idle_a_glass"			 		BREAK
				CASE 2			pedPropAnimData.sPropAnimClip = "idle_b_glass"					BREAK
				CASE 3			pedPropAnimData.sPropAnimClip = "idle_c_glass"					BREAK
				CASE 4			pedPropAnimData.sPropAnimClip = "lonely_glass"					BREAK
			ENDSWITCH
		BREAK
			CASE PED_ACT_MUSIC_STUDIO_FRANKLIN
			pedPropAnimData.sPropAnimDict = "anim@scripted@freemode_npc@fix_astu_ig5_wrm_fra_lam@"
			SWITCH iProp
				CASE 0 //JOINT
					SWITCH iClip
						CASE 0			pedPropAnimData.sPropAnimClip = "base_franklinweed"					BREAK
						CASE 1			pedPropAnimData.sPropAnimClip = "idle_01_franklinweed"				BREAK
						CASE 2			pedPropAnimData.sPropAnimClip = "idle_02_franklinweed"				BREAK
						CASE 3			pedPropAnimData.sPropAnimClip = "idle_03_franklinweed"				BREAK
						CASE 4			pedPropAnimData.sPropAnimClip = "idle_04_franklinweed"				BREAK
						CASE 5			pedPropAnimData.sPropAnimClip = "idle_05_franklinweed"				BREAK
						CASE 6			pedPropAnimData.sPropAnimClip = "idle_06_franklinweed"				BREAK
						CASE 7			pedPropAnimData.sPropAnimClip = "idle_07_franklinweed"				BREAK
						CASE 8			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig1_actor@"			
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig1_actor_franklinweed"				
						BREAK
						CASE 9			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig2_weak@"				
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig2_weak_action_franklinweed"		
						BREAK
						CASE 10			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig3_simeon@"			
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig3_simeon_action_franklinweed"		
						BREAK
						CASE 11			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig4_auntie@"			
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig4_auntie_franklinweed"			
						BREAK
						CASE 12			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig5_phone@"			
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig5_phone_franklinweed"				
						BREAK
						CASE 13			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig6_chop@"				
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig6_chop_franklinweed"				
						BREAK
						CASE 14			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig7_fake@"				
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig7_fake_franklinweed"				
						BREAK
						CASE 15			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig8_moon@"				
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig8_moon_franklinweed"				
						BREAK
						CASE 16			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig9_food@"				
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig9_food_franklinweed"				
						BREAK
						CASE 17			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig10_tonya@"			
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig10_tonya_franklinweed"			
						BREAK
						CASE 18			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig11_superstoned@"		
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig11_superstoned_franklinweed"		
						BREAK
						CASE 19			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig12_eyedrops@"		
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig12_eyedrops_franklinweed"			
						BREAK
					ENDSWITCH
				BREAK
				CASE 1		//PHONE
					SWITCH iClip				
						CASE 12			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig5_phone@"			
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig5_phone_phone"				
						BREAK
						CASE 16			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig9_food@"				
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig9_food_phone"				
						BREAK
						CASE 19			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig12_eyedrops@"				
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig12_eyedrops_phone"				
						BREAK
					ENDSWITCH
				BREAK
				
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_LAMAR_VFX_PASS
			pedPropAnimData.sPropAnimDict = "anim@scripted@freemode_npc@fix_astu_ig5_wrm_fra_lam@"		
			SWITCH iClip
				CASE 0			pedPropAnimData.sPropAnimClip = "base_lamarweed"				BREAK
				CASE 1			pedPropAnimData.sPropAnimClip = "idle_01_lamarweed"				BREAK
				CASE 2			pedPropAnimData.sPropAnimClip = "idle_02_lamarweed"				BREAK
				CASE 3			pedPropAnimData.sPropAnimClip = "idle_03_lamarweed"				BREAK
				CASE 4			pedPropAnimData.sPropAnimClip = "idle_04_lamarweed"				BREAK
				CASE 5			pedPropAnimData.sPropAnimClip = "idle_05_lamarweed"				BREAK
				CASE 6			pedPropAnimData.sPropAnimClip = "idle_06_lamarweed"				BREAK
				CASE 7			pedPropAnimData.sPropAnimClip = "idle_07_lamarweed"				BREAK
				CASE 8			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig1_actor@"			
								pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig1_actor_lamarweed"				
				BREAK
				CASE 9			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig2_weak@"				
								pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig2_weak_action_lamarweed"		
				BREAK
				CASE 10			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig3_simeon@"			
								pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig3_simeon_action_lamarweed"		
				BREAK
				CASE 11			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig4_auntie@"			
								pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig4_auntie_lamarweed"			
				BREAK
				CASE 12			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig5_phone@"			
								pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig5_phone_lamarweed"				
				BREAK
				CASE 13			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig6_chop@"				
								pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig6_chop_lamarweed"				
				BREAK
				CASE 14			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig7_fake@"				
								pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig7_fake_lamarweed"				
				BREAK
				CASE 15			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig8_moon@"				
								pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig8_moon_lamarweed"				
				BREAK
				CASE 16			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig9_food@"				
								pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig9_food_lamarweed"				
				BREAK
				CASE 17			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig10_tonya@"			
								pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig10_tonya_lamarweed"			
				BREAK
				CASE 18			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig11_superstoned@"		
								pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig11_superstoned_lamarweed"		
				BREAK
				CASE 19			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig12_eyedrops@"		
								pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig12_eyedrops_lamarweed"			
				BREAK				
			ENDSWITCH
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_LAMAR
			pedPropAnimData.sPropAnimDict = "anim@scripted@freemode_npc@fix_astu_ig5_wrm_fra_lam@"
			SWITCH iProp
				CASE 0
					SWITCH iClip
						CASE 0			pedPropAnimData.sPropAnimClip = "base_lighter"					BREAK
						CASE 1			pedPropAnimData.sPropAnimClip = "idle_01_lighter"				BREAK
						CASE 2			pedPropAnimData.sPropAnimClip = "idle_02_lighter"				BREAK
						CASE 3			pedPropAnimData.sPropAnimClip = "idle_03_lighter"				BREAK
						CASE 4			pedPropAnimData.sPropAnimClip = "idle_04_lighter"				BREAK
						CASE 5			pedPropAnimData.sPropAnimClip = "idle_05_lighter"				BREAK
						CASE 6			pedPropAnimData.sPropAnimClip = "idle_06_lighter"				BREAK
						CASE 7			pedPropAnimData.sPropAnimClip = "idle_07_lighter"				BREAK
					ENDSWITCH	
				BREAK
				CASE 1
					SWITCH iClip
						CASE 0			pedPropAnimData.sPropAnimClip = "base_beer"					BREAK
						CASE 1			pedPropAnimData.sPropAnimClip = "idle_01_beer"				BREAK
						CASE 2			pedPropAnimData.sPropAnimClip = "idle_02_beer"				BREAK
						CASE 3			pedPropAnimData.sPropAnimClip = "idle_03_beer"				BREAK
						CASE 4			pedPropAnimData.sPropAnimClip = "idle_04_beer"				BREAK
						CASE 5			pedPropAnimData.sPropAnimClip = "idle_05_beer"				BREAK
						CASE 6			pedPropAnimData.sPropAnimClip = "idle_06_beer"				BREAK
						CASE 7			pedPropAnimData.sPropAnimClip = "idle_07_beer"				BREAK
						CASE 8			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig1_actor@"			
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig1_actor_beer"				
						BREAK
						CASE 9			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig2_weak@"				
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig2_weak_action_beer"		
						BREAK
						CASE 10			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig3_simeon@"			
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig3_simeon_action_beerbottle"		
						BREAK
						CASE 11			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig4_auntie@"			
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig4_auntie_beer"			
						BREAK
						CASE 12			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig5_phone@"			
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig5_phone_beer"				
						BREAK
						CASE 13			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig6_chop@"				
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig6_chop_beer"				
						BREAK
						CASE 14			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig7_fake@"				
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig7_fake_beer"				
						BREAK
						CASE 15			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig8_moon@"				
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig8_moon_beer"				
						BREAK
						CASE 16			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig9_food@"				
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig9_food_beer"				
						BREAK
						CASE 17			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig10_tonya@"			
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig10_tonya_beer"			
						BREAK
						CASE 18			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig11_superstoned@"		
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig11_superstoned_beer"		
						BREAK
						CASE 19			pedPropAnimData.sPropAnimDict = "anim@scripted@weed_room_conv@fix_wrms_ig12_eyedrops@"		
										pedPropAnimData.sPropAnimClip 	= "fix_wrms_ig12_eyedrops_beer"			
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC
//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PATROL DATA ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_MUSIC_STUDIO_PED_PATROL_DATA(SEQUENCE_INDEX &SequenceIndex, INT iLayout, INT iArrayID, INT &iPatrolPedID[], BOOL &bShowProp[], INT iTask, INT &iMaxSequence, BOOL bNetworkPed = FALSE)
	UNUSED_PARAMETER(iLayout)
	// Max tasks in a sequence is 10 (code limit).
	SequenceIndex = NULL
	iMaxSequence = 0
	
	FLOAT fRadius = 0.05
	IF (bNetworkPed)
		SWITCH iArrayID
			CASE 2
				iPatrolPedID[iArrayID] = iArrayID
				iMaxSequence = 3
				bShowProp[iTask] = TRUE		
				SWITCH iTask
					CASE 0
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1004.9483, -53.6589, -100.0031>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK
					CASE 1
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(15000, 35000))
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK
					CASE 2
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1019.4627, -50.2930, -100.0031>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK
					CASE 3
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(15000, 35000))
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK
				ENDSWITCH
			BREAK
			CASE 3
				iPatrolPedID[iArrayID] = iArrayID
				iMaxSequence = 3
				
				bShowProp[iTask] = TRUE							
				SWITCH iTask
					CASE 0
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1014.0554, -91.5497, -100.4031>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK
					CASE 1
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(15000, 35000))
						CLOSE_SEQUENCE_TASK(SequenceIndex)	
					BREAK
					CASE 2
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1019.4627, -50.2930, -100.0031>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_ACHIEVE_HEADING(NULL, 234.7078)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK
					CASE 3
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(15000, 35000))
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK					
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PED ANIM DATA ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// Note: Activities with multiple animations have their anim data populated in their own functions below.
//		 If an activity only has one animation associated with it, it is populated within _GET_MUSIC_STUDIO_PED_ANIM_DATA itself.
//
// Note: Each activity either has a _M_ or _F_ in its title to specify which ped gender should be using that activity.
//		 If an activity does not has either an _M_ or _F_ in its title; it is unisex, and can be used by either ped gender.
//
// Note: Some animations have been excluded from activities. 
//		 The excluded anims have Z axis starting positions that dont line up with the other anims in the same dictionary.
//		 This causes a snap that a blend cannot fix. Use the widget 'RAG/Script/AM_MP_PEDS/Animation/Output Initial Activity Anim Data' to see which Z axis anims are broken.

//╒═══════════════════════════════╕
//╞══════╡ LEANING SMOKING ╞══════╡
//╘═══════════════════════════════╛

PROC GET_MUSIC_STUDIO_LEANING_SMOKING_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_C"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_D"
		BREAK
		CASE 5
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_E"
		BREAK
	ENDSWITCH
ENDPROC

//╒═══════════════════════════════╕
//╞═════╡ STANDING SMOKING ╞══════╡
//╘═══════════════════════════════╛

PROC GET_MUSIC_STUDIO_STANDING_SMOKING_M_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_C"
		BREAK
	ENDSWITCH
ENDPROC

//╒═══════════════════════════════╕
//╞═════╡ 	RECEPTIONIST   ╞══════╡
//╘═══════════════════════════════╛

PROC GET_MUSIC_STUDIO_RECEPTIONIST_ANIM(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "anim@amb@office@pa@female@"
			pedAnimData.sAnimClip 		= "pa_base"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "anim@amb@office@pa@female@"
			pedAnimData.sAnimClip 		= "pa_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "anim@amb@office@pa@female@"
			pedAnimData.sAnimClip 		= "pa_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "anim@amb@office@pa@female@"
			pedAnimData.sAnimClip 		= "pa_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "anim@amb@office@pa@female@"
			pedAnimData.sAnimClip 		= "pa_idle_d"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_LISTEN_MUSIC_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@listen_music@"
	SWITCH iClip
		CASE 0		
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_LISTEN_MUSIC_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@listen_music@"
	SWITCH iClip
		CASE 0	
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_DRE_WORK_STATE_ANIM_DICT(PED_ACTIVITIES eActivity, PED_ANIM_DATA &pedAnimData)
	SWITCH eActivity
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_DRUMS
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MUSICIAN_DRUMS
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_DRUMS
			pedAnimData.sAnimDict 				= "anim@scripted@freemode_npc@FIX_ASTU_PROD_DRUMS@"
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_VOCALS
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MUSICIAN_VOCALS
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_VOCALS
			pedAnimData.sAnimDict 				= "anim@scripted@freemode_npc@FIX_ASTU_PROD_VOCALS@"	
		BREAK
		DEFAULT			
			pedAnimData.sAnimDict 				= "anim@scripted@freemode_npc@fix_astu_prod_mix@"
		BREAK
	ENDSWITCH
ENDPROC


PROC GET_MUSIC_STUDIO_POOH_COUCH_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "sit_base_pooh"
			pedAnimData.sFacialAnimClip = "sit_base_pooh_facial"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "sit_idle_01_pooh"
			pedAnimData.sFacialAnimClip = "sit_idle_01_pooh_facial"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "sit_idle_02_pooh"
			pedAnimData.sFacialAnimClip = "sit_idle_02_pooh_facial"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "sit_idle_03_pooh"
			pedAnimData.sFacialAnimClip = "sit_idle_03_pooh_facial"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "sit_idle_04_pooh"
			pedAnimData.sFacialAnimClip = "sit_idle_04_pooh_facial"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_POOH_STAND_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "stand_base_pooh"
			pedAnimData.sFacialAnimClip = "stand_base_pooh_facial"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "stand_idle_01_pooh"
			pedAnimData.sFacialAnimClip = "stand_idle_01_pooh_facial"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "stand_idle_02_pooh"
			pedAnimData.sFacialAnimClip = "stand_idle_02_pooh_facial"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "stand_idle_03_pooh"
			pedAnimData.sFacialAnimClip = "stand_idle_03_pooh_facial"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "stand_idle_04_pooh"
			pedAnimData.sFacialAnimClip = "stand_idle_04_pooh_facial"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_POOH_DRINK_BAR_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "drink_base_pooh"
			pedAnimData.sFacialAnimClip = "drink_base_pooh_facial"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "drink_idle_01_pooh"
			pedAnimData.sFacialAnimClip = "drink_idle_01_pooh_facial"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "drink_idle_02_pooh"
			pedAnimData.sFacialAnimClip = "drink_idle_02_pooh_facial"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "drink_idle_03_pooh"
			pedAnimData.sFacialAnimClip = "drink_idle_03_pooh_facial"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "drink_idle_04_pooh"
			pedAnimData.sFacialAnimClip = "drink_idle_04_pooh_facial"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_POOH_DRINK_SIT_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_dre_studio_entourage@"
			pedAnimData.sAnimClip 		= "base_ballas"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_dre_studio_entourage@"
			pedAnimData.sAnimClip 		= "idle_a_ballas"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_dre_studio_entourage@"
			pedAnimData.sAnimClip 		= "idle_b_ballas"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_dre_studio_entourage@"
			pedAnimData.sAnimClip 		= "idle_c_ballas"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_dre_studio_entourage@"
			pedAnimData.sAnimClip 		= "lonely_ballas"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_POOH_TEXT_STAND_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "text_base_pooh"
			pedAnimData.sFacialAnimClip = "text_base_pooh_facial"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "text_idle_01_pooh"
			pedAnimData.sFacialAnimClip = "text_idle_01_pooh_facial"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "text_idle_02_pooh"
			pedAnimData.sFacialAnimClip = "text_idle_02_pooh_facial"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "text_idle_03_pooh"
			pedAnimData.sFacialAnimClip = "text_idle_03_pooh_facial"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig3_pooh_jimmy@pooh@"
			pedAnimData.sAnimClip 		= "text_idle_04_pooh"
			pedAnimData.sFacialAnimClip = "text_idle_04_pooh_facial"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_E_STAND_DRINK_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "anim@amb@casino@hangout@ped_female@stand_withdrink@01b@base"
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "anim@amb@casino@hangout@ped_female@stand_withdrink@01b@idles"
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "anim@amb@casino@hangout@ped_female@stand_withdrink@01b@idles"
			pedAnimData.sAnimClip 		= "idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "anim@amb@casino@hangout@ped_female@stand_withdrink@01b@idles"
			pedAnimData.sAnimClip 		= "idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "anim@amb@casino@hangout@ped_female@stand_withdrink@01b@idles"
			pedAnimData.sAnimClip 		= "idle_d"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_E_STAND_STAND_ARMS_CROSSED_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "amb@world_human_hang_out_street@female_arms_crossed@base"
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "amb@world_human_hang_out_street@female_arms_crossed@idle_a"
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "amb@world_human_hang_out_street@female_arms_crossed@idle_a"
			pedAnimData.sAnimClip 		= "idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "amb@world_human_hang_out_street@female_arms_crossed@idle_a"
			pedAnimData.sAnimClip 		= "idle_c"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_E_STAND_STAND_ARM_HOLD_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "amb@world_human_hang_out_street@female_hold_arm@base"
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "amb@world_human_hang_out_street@female_hold_arm@idle_a"
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "amb@world_human_hang_out_street@female_hold_arm@idle_a"
			pedAnimData.sAnimClip 		= "idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "amb@world_human_hang_out_street@female_hold_arm@idle_a"
			pedAnimData.sAnimClip 		= "idle_c"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_E_STAND_STAND_DRINK_CUP_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_dre_studio_entourage_b@"
			pedAnimData.sAnimClip 		= "stand_drink_cup_female_a_base"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_dre_studio_entourage_b@"
			pedAnimData.sAnimClip 		= "stand_drink_cup_female_a_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_dre_studio_entourage_b@"
			pedAnimData.sAnimClip 		= "stand_drink_cup_female_a_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_dre_studio_entourage_b@"
			pedAnimData.sAnimClip 		= "stand_drink_cup_female_a_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_dre_studio_entourage_b@"
			pedAnimData.sAnimClip 		= "stand_drink_cup_female_a_idle_d"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_E_STAND_SIT_DRINK_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink@female@generic@base"
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink@female@generic@idle_a"
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink@female@generic@idle_a"
			pedAnimData.sAnimClip 		= "idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair_drink@female@generic@idle_a"
			pedAnimData.sAnimClip 		= "idle_c"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_E_LEAN_TEXT_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "amb@world_human_leaning@female@wall@back@texting@base"
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "amb@world_human_leaning@female@wall@back@texting@idle_a"
			pedAnimData.sAnimClip 		= "idle_a"	
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "amb@world_human_leaning@female@wall@back@texting@idle_a"
			pedAnimData.sAnimClip 		= "idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "amb@world_human_leaning@female@wall@back@texting@idle_a"
			pedAnimData.sAnimClip 		= "idle_c"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_E_TEXTING_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@FEMALE@TEXT@BASE"
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@FEMALE@TEXT@IDLE_A"
			pedAnimData.sAnimClip 		= "idle_a"	
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@FEMALE@TEXT@IDLE_A"
			pedAnimData.sAnimClip 		= "idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@FEMALE@TEXT@IDLE_A"
			pedAnimData.sAnimClip 		= "idle_c"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_E_SIT_LEGS_CROSSED_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair@female@legs_crossed@base"
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair@female@legs_crossed@idle_a"
			pedAnimData.sAnimClip 		= "idle_a"	
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair@female@legs_crossed@idle_a"
			pedAnimData.sAnimClip 		= "idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair@female@legs_crossed@idle_a"
			pedAnimData.sAnimClip 		= "idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair@female@legs_crossed@idle_b"
			pedAnimData.sAnimClip 		= "idle_d"	
		BREAK
		CASE 5
			pedAnimData.sAnimDict 		= "amb@prop_human_seat_chair@female@legs_crossed@idle_b"
			pedAnimData.sAnimClip 		= "idle_e"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_E_WINDOW_SHOP_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "amb@world_human_window_shop@female@base"
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "amb@world_human_window_shop@female@idle_a"
			pedAnimData.sAnimClip 		= "browse_a"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "amb@world_human_window_shop@female@idle_a"
			pedAnimData.sAnimClip 		= "browse_b"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "amb@world_human_window_shop@female@idle_a"
			pedAnimData.sAnimClip 		= "browse_c"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_FRANKLIN_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	pedAnimData.bVFXAnimClip			= TRUE
	pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig5_wrm_fra_lam@"	
	SWITCH iClip
		CASE 0				
			pedAnimData.sAnimClip 		= "base_franklin"		
			pedAnimData.bLoopingAnim = TRUE	
			pedAnimData.sFacialAnimClip = "base_franklin_facial"
		BREAK
		CASE 1		pedAnimData.sAnimClip 		= "idle_01_franklin"	pedAnimData.sFacialAnimClip = "idle_01_franklin_facial"		pedAnimData.bLoopingAnim = TRUE	 	BREAK
		CASE 2		pedAnimData.sAnimClip 		= "idle_02_franklin"	pedAnimData.sFacialAnimClip = "idle_02_franklin_facial"		pedAnimData.bLoopingAnim = TRUE		BREAK
		CASE 3		pedAnimData.sAnimClip 		= "idle_03_franklin"	pedAnimData.sFacialAnimClip = "idle_03_franklin_facial"		pedAnimData.bLoopingAnim = TRUE		BREAK
		CASE 4		pedAnimData.sAnimClip 		= "idle_04_franklin"	pedAnimData.sFacialAnimClip = "idle_04_franklin_facial"		pedAnimData.bLoopingAnim = TRUE		BREAK
		CASE 5		pedAnimData.sAnimClip 		= "idle_05_franklin"	pedAnimData.sFacialAnimClip = "idle_05_franklin_facial"		pedAnimData.bLoopingAnim = TRUE		BREAK
		CASE 6		pedAnimData.sAnimClip 		= "idle_06_franklin"	pedAnimData.sFacialAnimClip = "idle_06_franklin_facial"		pedAnimData.bLoopingAnim = TRUE		BREAK
		CASE 7		pedAnimData.sAnimClip 		= "idle_07_franklin"	pedAnimData.sFacialAnimClip = "idle_07_franklin_facial"		pedAnimData.bLoopingAnim = TRUE		BREAK
		CASE 8		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig1_actor@"			pedAnimData.sAnimClip 		= "fix_wrms_ig1_actor_franklin"				pedAnimData.ePedSpeech		= PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_ACTOR				pedAnimData.sFacialAnimClip = "fix_wrms_ig1_actor_franklin_facial"				BREAK
		CASE 9		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig2_weak@"				pedAnimData.sAnimClip 		= "fix_wrms_ig2_weak_action_franklin"		pedAnimData.ePedSpeech		= PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_WEAK				pedAnimData.sFacialAnimClip = "fix_wrms_ig2_weak_action_franklin_facial"		BREAK
		CASE 10		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig3_simeon@"			pedAnimData.sAnimClip 		= "fix_wrms_ig3_simeon_action_franklin"	 	pedAnimData.ePedSpeech		= PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_SIMEON			pedAnimData.sFacialAnimClip = "fix_wrms_ig3_simeon_action_franklin_facial"		BREAK
		CASE 11		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig4_auntie@"			pedAnimData.sAnimClip 		= "fix_wrms_ig4_auntie_franklin"			pedAnimData.ePedSpeech		= PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_AUNTIE			pedAnimData.sFacialAnimClip = "fix_wrms_ig4_auntie_franklin_facial"				BREAK
		CASE 12		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig5_phone@"			pedAnimData.sAnimClip 		= "fix_wrms_ig5_phone_franklin"				pedAnimData.ePedSpeech		= PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_PHONE				pedAnimData.sFacialAnimClip = "fix_wrms_ig5_phone_franklin_facial"				BREAK
		CASE 13		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig6_chop@"				pedAnimData.sAnimClip 		= "fix_wrms_ig6_chop_franklin"				pedAnimData.ePedSpeech		= PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_CHOP				pedAnimData.sFacialAnimClip = "fix_wrms_ig6_chop_franklin_facial"				BREAK
		CASE 14		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig7_fake@"				pedAnimData.sAnimClip 		= "fix_wrms_ig7_fake_franklin"				pedAnimData.ePedSpeech		= PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_FAKE				pedAnimData.sFacialAnimClip = "fix_wrms_ig7_fake_franklin_facial"				BREAK
		CASE 15		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig8_moon@"				pedAnimData.sAnimClip 		= "fix_wrms_ig8_moon_franklin"				pedAnimData.ePedSpeech		= PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_MOON				pedAnimData.sFacialAnimClip = "fix_wrms_ig8_moon_franklin_facial"				BREAK
		CASE 16		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig9_food@"				pedAnimData.sAnimClip 		= "fix_wrms_ig9_food_franklin"				pedAnimData.ePedSpeech		= PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_FOOD				pedAnimData.sFacialAnimClip = "fix_wrms_ig9_food_franklin_facial"				BREAK
		CASE 17		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig10_tonya@"			pedAnimData.sAnimClip 		= "fix_wrms_ig10_tonya_franklin"			pedAnimData.ePedSpeech		= PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_TONYA				pedAnimData.sFacialAnimClip = "fix_wrms_ig10_tonya_franklin_facial"				BREAK
		CASE 18		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig11_superstoned@"		pedAnimData.sAnimClip 		= "fix_wrms_ig11_superstoned_franklin"		pedAnimData.ePedSpeech		= PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_SUPERSTONED		pedAnimData.sFacialAnimClip = "fix_wrms_ig11_superstoned_franklin_facial"		BREAK
		CASE 19		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig12_eyedrops@"		pedAnimData.sAnimClip 		= "fix_wrms_ig12_eyedrops_franklin"			pedAnimData.ePedSpeech		= PED_SPH_CT_FRANKLIN_LAMAR_WEED_ROOM_EYEDROPS			pedAnimData.sFacialAnimClip = "fix_wrms_ig12_eyedrops_franklin_facial"			BREAK
	ENDSWITCH
	
	IF NOT g_bWeedRoomStartingFoodDialogue
	AND iClip = 16
		g_bWeedRoomStartingFoodDialogue = TRUE
	ELIF g_bWeedRoomStartingFoodDialogue
	AND iClip != 16
		g_bWeedRoomStartingFoodDialogue = FALSE
	ENDIF
ENDPROC

PROC GET_MUSIC_STUDIO_LAMAR_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT

	pedAnimData.bVFXAnimClip			= TRUE
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig5_wrm_fra_lam@"
			pedAnimData.sAnimClip 		= "base_lamar"
			pedAnimData.sFacialAnimClip = "base_lamar_facial"
			pedAnimData.bLoopingAnim = TRUE	
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig5_wrm_fra_lam@"
			pedAnimData.sAnimClip 		= "idle_01_lamar"	
			pedAnimData.sFacialAnimClip = "idle_01_lamar_facial"
			pedAnimData.bLoopingAnim = TRUE	
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig5_wrm_fra_lam@"
			pedAnimData.sAnimClip 		= "idle_02_lamar"
			pedAnimData.sFacialAnimClip = "idle_02_lamar_facial"
			pedAnimData.bLoopingAnim = TRUE	
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig5_wrm_fra_lam@"
			pedAnimData.sAnimClip 		= "idle_03_lamar"
			pedAnimData.sFacialAnimClip = "idle_03_lamar_facial"
			pedAnimData.bLoopingAnim = TRUE	
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig5_wrm_fra_lam@"
			pedAnimData.sAnimClip 		= "idle_04_lamar"	
			pedAnimData.sFacialAnimClip = "idle_04_lamar_facial"
			pedAnimData.bLoopingAnim = TRUE	
		BREAK
		CASE 5
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig5_wrm_fra_lam@"
			pedAnimData.sAnimClip 		= "idle_05_lamar"
			pedAnimData.sFacialAnimClip = "idle_05_lamar_facial"
			pedAnimData.bLoopingAnim = TRUE	
		BREAK
		CASE 6
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig5_wrm_fra_lam@"
			pedAnimData.sAnimClip 		= "idle_06_lamar"
			pedAnimData.sFacialAnimClip = "idle_06_lamar_facial"
			pedAnimData.bLoopingAnim = TRUE	
		BREAK
		CASE 7
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig5_wrm_fra_lam@"
			pedAnimData.sAnimClip 		= "idle_07_lamar"
			pedAnimData.sFacialAnimClip = "idle_07_lamar_facial"
			pedAnimData.bLoopingAnim = TRUE	
		BREAK
		CASE 8		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig1_actor@"			pedAnimData.sAnimClip 		= "fix_wrms_ig1_actor_lamar"			pedAnimData.sFacialAnimClip = "fix_wrms_ig1_actor_lamar_facial"				BREAK
		CASE 9		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig2_weak@"				pedAnimData.sAnimClip 		= "fix_wrms_ig2_weak_action_lamar"		pedAnimData.sFacialAnimClip = "fix_wrms_ig2_weak_action_lamar_facial"		BREAK
		CASE 10		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig3_simeon@"			pedAnimData.sAnimClip 		= "fix_wrms_ig3_simeon_action_lamar"	pedAnimData.sFacialAnimClip = "fix_wrms_ig3_simeon_action_lamar_facial"		BREAK
		CASE 11		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig4_auntie@"			pedAnimData.sAnimClip 		= "fix_wrms_ig4_auntie_lamar"			pedAnimData.sFacialAnimClip = "fix_wrms_ig4_auntie_lamar_facial"			BREAK
		CASE 12		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig5_phone@"			pedAnimData.sAnimClip 		= "fix_wrms_ig5_phone_lamar"			pedAnimData.sFacialAnimClip = "fix_wrms_ig5_phone_lamar_facial"				BREAK
		CASE 13		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig6_chop@"				pedAnimData.sAnimClip 		= "fix_wrms_ig6_chop_lamar"				pedAnimData.sFacialAnimClip = "fix_wrms_ig6_chop_lamar_facial"				BREAK
		CASE 14		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig7_fake@"				pedAnimData.sAnimClip 		= "fix_wrms_ig7_fake_lamar"				pedAnimData.sFacialAnimClip = "fix_wrms_ig7_fake_lamar_facial"				BREAK
		CASE 15		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig8_moon@"				pedAnimData.sAnimClip 		= "fix_wrms_ig8_moon_lamar"				pedAnimData.sFacialAnimClip = "fix_wrms_ig8_moon_lamar_facial"				BREAK
		CASE 16		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig9_food@"				pedAnimData.sAnimClip 		= "fix_wrms_ig9_food_lamar"				pedAnimData.sFacialAnimClip = "fix_wrms_ig9_food_lamar_facial"				BREAK
		CASE 17		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig10_tonya@"			pedAnimData.sAnimClip 		= "fix_wrms_ig10_tonya_lamar"			pedAnimData.sFacialAnimClip = "fix_wrms_ig10_tonya_lamar_facial"			BREAK
		CASE 18		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig11_superstoned@"		pedAnimData.sAnimClip 		= "fix_wrms_ig11_superstoned_lamar"		pedAnimData.sFacialAnimClip = "fix_wrms_ig11_superstoned_lamar_facial"		BREAK
		CASE 19		pedAnimData.sAnimDict 		= "anim@scripted@weed_room_conv@fix_wrms_ig12_eyedrops@"		pedAnimData.sAnimClip 		= "fix_wrms_ig12_eyedrops_lamar"		pedAnimData.sFacialAnimClip = "fix_wrms_ig12_eyedrops_lamar_facial"			BREAK
	ENDSWITCH
ENDPROC

PROC GET_MUSIC_STUDIO_DR_DRE_IDLE_ENGINEER_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0		
			pedAnimData.sAnimClip 		= "eng_ig12_idle_a"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "eng_ig12_idle_b"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "eng_ig12_idle_c"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "eng_ig12_idle_d"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "eng_ig12_idle_e"
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL SHOULD_PLAY_FACIAL_ANIM(PED_ACTIVITIES eActivity, INT iClip)

	IF (eActivity = PED_ACT_MUSIC_STUDIO_DR_DRE_MIXING
	OR eActivity = PED_ACT_MUSIC_STUDIO_DR_DRE_MIXING_2)
	AND iClip != 3
		RETURN FALSE	
	ENDIF	
	
	RETURN TRUE	
	
ENDFUNC
//╒═══════════════════════════════╕
//╞════════╡ ANIM LOOKUP ╞════════╡
//╘═══════════════════════════════╛
PROC _GET_MUSIC_STUDIO_PED_ANIM_DATA(PED_ACTIVITIES eActivity, PED_ANIM_DATA &pedAnimData, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, INT iPedID = 0, PED_TRANSITION_STATES eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE, BOOL bPedTransition = FALSE)
	UNUSED_PARAMETER(iPedID)
	UNUSED_PARAMETER(eMusicIntensity)
	UNUSED_PARAMETER(ePedMusicIntensity)
	UNUSED_PARAMETER(bDancingTransition)
	UNUSED_PARAMETER(eTransitionState)
	UNUSED_PARAMETER(bPedTransition)
	RESET_PED_ANIM_DATA(pedAnimData)
	SWITCH eActivity
		CASE PED_ACT_MUSIC_STUDIO_PATROL
			pedAnimData.sAnimDict = "amb@code_human_wander_clipboard@male@base"
			pedAnimData.sAnimClip = "base"
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_FRANKLIN  //PROPS NOT ATTACHED
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_sec_weed_smoke_exhale_start"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_mouth", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)			
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "vfx_sec_weed_smoke_start", "vfx_sec_weed_smoke_stop", "scr_sec", "scr_sec_weed_smoke", "", "", <<-0.086, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 2.0, 0,DEFAULT, TRUE)		
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DIALOGUE_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HIDE_PROPS_WITHOUT_ANIMATION)
				
			GET_MUSIC_STUDIO_FRANKLIN_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_LAMAR //PROPS NOT ATTACHED
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_sec_weed_smoke_exhale_start"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_mouth", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)
			
			//only set data for looping for clip with the lighter
			IF iClip = 5
				SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "vfx_sec_weed_lighter_start", "vfx_sec_weed_lighter_stop", "scr_sec","scr_sec_weed_lighter_flame", "", "", <<0.0, 0.0, 0.05>>, <<0.0, 0.0, 0.0>>, 1.0, 0 ,DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, 0.42, 0.65)
			ENDIF	
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			pedAnimData.bAddChildPedProps = TRUE
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)		
			GET_MUSIC_STUDIO_LAMAR_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_LAMAR_VFX_PASS //PROPS NOT ATTACHED
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "vfx_sec_weed_smoke_start", "vfx_sec_weed_smoke_stop", "scr_sec", "scr_sec_weed_smoke", "", "", <<-0.086, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 2.0, 0,DEFAULT,FALSE)		
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			pedAnimData.bAddChildPedProps = TRUE	
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)	
			GET_MUSIC_STUDIO_LAMAR_ANIM_CLIP(pedAnimData, iClip)
		BREAK
			
		CASE PED_ACT_MUSIC_STUDIO_E_SIT_LEGS_CROSSED /////PROPS AND FIXED
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_MUSIC_STUDIO_E_SIT_LEGS_CROSSED_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_WINDOW_SHOP
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)	
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			GET_MUSIC_STUDIO_E_WINDOW_SHOP_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_AUTO_SHOP_SESSANTA_TEXTING		//PROPS 	
			pedAnimData.animFlags 		= AF_LOOPING		
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			GET_MUSIC_STUDIO_E_LEAN_TEXT_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_TEXTING
			pedAnimData.animFlags 		= AF_LOOPING		
				
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			GET_MUSIC_STUDIO_E_TEXTING_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_SIT_DRINK /////PROPS AND FIXED
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_MUSIC_STUDIO_E_STAND_SIT_DRINK_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_STAND_DRINK_CUP //PROPS 
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_MUSIC_STUDIO_E_STAND_STAND_DRINK_CUP_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_STAND_ARM_HOLD
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_MUSIC_STUDIO_E_STAND_STAND_ARM_HOLD_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_STAND_ARMS_CROSSED
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_MUSIC_STUDIO_E_STAND_STAND_ARMS_CROSSED_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_E_STAND_DRINK //PROPS 
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_MUSIC_STUDIO_E_STAND_DRINK_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_COUCH //PROPS NOT ATTACHED
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_sec_weed_smoke_exhale_start"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_mouth", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 2.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)	
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "vfx_sec_weed_smoke_start", "vfx_sec_weed_smoke_stop", "scr_sec", "scr_sec_weed_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0,DEFAULT,TRUE)				
			pedAnimData.bVFXAnimClip	= TRUE
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_MUSIC_STUDIO_POOH_COUCH_ANIM_CLIP(pedAnimData, iClip)
		BREAK 
		CASE PED_ACT_MUSIC_STUDIO_POOH_STAND //PROPS NOT ATTACHED
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_sec_weed_smoke_exhale_start"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_mouth", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 2.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)	
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "vfx_sec_weed_smoke_start", "vfx_sec_weed_smoke_stop", "scr_sec", "scr_sec_weed_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0,DEFAULT,TRUE)				
			pedAnimData.bVFXAnimClip	= TRUE
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_MUSIC_STUDIO_POOH_STAND_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_DRINK_BAR //PROPS NOT ATTACHED
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_MUSIC_STUDIO_POOH_DRINK_BAR_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_DRINK_SIT
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_MUSIC_STUDIO_POOH_DRINK_SIT_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_TEXT_STAND //PROPS NOT ATTACHED
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_MUSIC_STUDIO_POOH_TEXT_STAND_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_POOH_TEXT_SIT
			pedAnimData.sAnimDict 		= "anim@amb@beach_party@seated@cell_phone@male_a@base"
			pedAnimData.sAnimClip 		= "base"
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_CLIPBOARD
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_CLIPBOARD@MALE@BASE"
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MIXING
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MIXING_2
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_VOCALS
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_DRUMS
			GET_DRE_WORK_STATE_ANIM_DICT(eActivity, pedAnimData)
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_USE_MOVER_EXTRACTION
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DJ_START_PHASE)
				
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "")
			IF SHOULD_PLAY_FACIAL_ANIM(eActivity, iClip)
				pedAnimData.sFacialAnimClip	= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_facial") 
			ENDIF
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_MIXING
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_MIXING_2
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_DRUMS
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_VOCALS
			GET_DRE_WORK_STATE_ANIM_DICT(eActivity, pedAnimData)
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS )
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DJ_START_PHASE)
					
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "")		 
			IF SHOULD_PLAY_FACIAL_ANIM(eActivity, iClip)
				pedAnimData.sFacialAnimClip	= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_facial") 
			ENDIF		
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_ENGINEER_DRE_ABSENT
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_astu_ig12_idle@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS )
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DJ_START_PHASE)	
			
			GET_MUSIC_STUDIO_DR_DRE_IDLE_ENGINEER_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MUSICIAN_DRUMS
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@FIX_ASTU_PROD_DRUMS@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK 
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DJ_START_PHASE)
				
			pedAnimData.sFacialAnimClip	= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_facial") 
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "")
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DR_DRE_MUSICIAN_VOCALS
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@FIX_ASTU_PROD_VOCALS@"	
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK 
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_DJ_START_PHASE)
			
			//TEMP CHECK WHILE NOT ALL OF FACE ANIMS ARE READY		
			pedAnimData.sFacialAnimClip	= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "_facial") 
			pedAnimData.sAnimClip 		= BUILD_DJ_PED_ANIM_CLIP(GET_PED_DJ_ANIM_CLIP_BASE(eActivity, iClip), "", "")
		BREAK
		CASE PED_ACT_LEANING_TEXTING_M_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@TEXTING@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_LEANING_TEXTING_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_LEANING@FEMALE@WALL@BACK@TEXTING@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_ON_PHONE_M_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@MOBILE@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_ON_PHONE_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_LEANING@FEMALE@WALL@BACK@MOBILE@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_LEANING_SMOKING_M_01
		CASE PED_ACT_LEANING_SMOKING_F_01
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_MUSIC_STUDIO_LEANING_SMOKING_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_STANDING_SMOKING_M_01
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_MUSIC_STUDIO_STANDING_SMOKING_M_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_RECEPTION
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_MUSIC_STUDIO_RECEPTIONIST_ANIM(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_KNEEL_INSPECT
			pedAnimData.sAnimDict 		= "amb@medic@standing@kneel@idle_a"
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_CROUCH_INSPECT
			pedAnimData.sAnimDict 		= "ANIM@AMB@INSPECT@CROUCH@MALE_A@IDLES"
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_SIT_TABLET  /////PROPS AND FIXED
			pedAnimData.sAnimDict 		= "anim@arena@amb@seat_drone_tablet@male@"
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
		
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			pedAnimData.sAnimClip = "base"			
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_MOVE_STUDIO_LIGHT
			pedAnimData.sAnimDict 		= "AMB@PROP_HUMAN_MOVIE_STUDIO_LIGHT@BASE"
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_DRINK_COFFEE
			pedAnimData.sAnimDict 		= "amb@world_human_drinking@coffee@male@idle_a"
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_LEAN_INSPECT
			pedAnimData.sAnimDict 		= "ANIM@AMB@INSPECT@STAND@MALE_A@IDLES"
			pedAnimData.animFlags 		= AF_LOOPING | AF_EXPAND_PED_CAPSULE_FROM_SKELETON
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_LEAN_TEXT
			pedAnimData.sAnimDict 		= "amb@world_human_leaning@male@wall@back@texting@base"
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_STAND_MEDIC_TOD
			pedAnimData.sAnimDict 		= "amb@medic@standing@timeofdeath@base"
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_STANDING_CALL
			pedAnimData.sAnimDict 		= "amb@world_human_stand_mobile@male@standing@call@base"
			pedAnimData.animFlags 		= AF_LOOPING
			pedAnimData.ikFlags 		= (AIK_DISABLE_LEG_IK | AIK_DISABLE_ARM_IK)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_MUSIC_STUDIO_SITTING_LAPTOP
			pedAnimData.sAnimDict 		= "anim@amb@office@laptops_v1@male@var_a@base@"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			pedAnimData.sAnimClip 		= "base"
		BREAK
		
	ENDSWITCH
ENDPROC

FUNC INT _GET_MUSIC_STUDIO_ACTIVE_PED_LEVEL_THRESHOLD(INT iLevel)
	INT iThreshold = -1
	SWITCH iLevel
		// Peds have level 0, 1 & 2. 
		// Level 2 peds are culled if the player count > 10
		// Level 1 peds are culled if the player count > 20
		// Level 0 peds are base and always remain
		CASE 0	iThreshold = 20	BREAK
		CASE 1	iThreshold = 10	BREAK
		CASE 2	iThreshold = 0	BREAK
	ENDSWITCH
	RETURN iThreshold
ENDFUNC

FUNC BOOL _DOES_MUSIC_STUDIO_ACTIVE_PED_TOTAL_CHANGE()
	RETURN TRUE
ENDFUNC

FUNC BOOL _HAS_MUSIC_STUDIO_PED_BEEN_CREATED(PEDS_DATA &Data, INT iLevel)
	
	IF (IS_ENTITY_ALIVE(Data.PedID)
		AND (GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK 
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK)
		OR Data.iActivity = 0)
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CHILD_PED)
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SKIP_PED)
	OR (Data.iLevel > iLevel)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ LOOK-UP TABLE ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC BUILD_PED_MUSIC_STUDIO_LOOK_UP_TABLE(PED_INTERFACE &interface, PED_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		// Script Launching
		CASE E_SHOULD_PED_SCRIPT_LAUNCH
			interface.returnShouldPedScriptLaunch = &_SHOULD_MUSIC_STUDIO_PED_SCRIPT_LAUNCH
		BREAK
		CASE E_IS_PARENT_A_SIMPLE_INTERIOR
			interface.returnIsParentASimpleInterior = &_IS_MUSIC_STUDIO_PARENT_A_SIMPLE_INTERIOR
		BREAK
		
		// Ped Data
		CASE E_GET_LOCAL_PED_TOTAL
			interface.returnGetLocalPedTotal = &_GET_MUSIC_STUDIO_LOCAL_PED_TOTAL
		BREAK
		CASE E_GET_NETWORK_PED_TOTAL
			interface.returnGetNetworkPedTotal = &_GET_MUSIC_STUDIO_NETWORK_PED_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT_TOTAL
			interface.returnGetServerPedLayoutTotal= &_GET_MUSIC_STUDIO_SERVER_PED_LAYOUT_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT
			interface.returnGetServerPedLayout = &_GET_MUSIC_STUDIO_SERVER_PED_LAYOUT
		BREAK
		CASE E_GET_SERVER_PED_LEVEL
			interface.returnGetServerPedLevel = &_GET_MUSIC_STUDIO_SERVER_PED_LEVEL
		BREAK
		CASE E_SET_PED_DATA
			interface.setPedData = &_SET_MUSIC_STUDIO_PED_DATA
		BREAK
		CASE E_SET_PED_SERVER_DATA
			interface.setPedServerData = &_SET_MUSIC_STUDIO_PED_SERVER_DATA
		BREAK
		CASE E_GET_PED_ANIM_DATA
			interface.getPedAnimData = &_GET_MUSIC_STUDIO_PED_ANIM_DATA
		BREAK
		CASE E_IS_PLAYER_IN_PARENT_PROPERTY
			interface.returnIsPlayerInParentProperty = &_IS_PLAYER_IN_MUSIC_STUDIO_PARENT_PROPERTY
		BREAK
		CASE E_GET_PED_LOCAL_COORDS_BASE_POSITION
			interface.returnGetPedLocalCoordsBasePosition = &_GET_MUSIC_STUDIO_PED_LOCAL_COORDS_BASE_POSITION
		BREAK
		CASE E_GET_PED_LOCAL_HEADING_BASE_HEADING
			interface.returnGetPedLocalHeadingBaseHeading = &_GET_MUSIC_STUDIO_PED_LOCAL_HEADING_BASE_HEADING
		BREAK
		CASE E_GET_ACTIVE_PED_LEVEL_THRESHOLD
			interface.getActivePedLevelThreshold = &_GET_MUSIC_STUDIO_ACTIVE_PED_LEVEL_THRESHOLD
		BREAK
		
		// Ped Creation
		CASE E_SET_LOCAL_PED_PROPERTIES
			interface.setLocalPedProperties = &_SET_MUSIC_STUDIO_LOCAL_PED_PROPERTIES
		BREAK
		CASE E_SET_NETWORK_PED_PROPERTIES
			interface.setNetworkPedProperties = &_SET_MUSIC_STUDIO_NETWORK_PED_PROPERTIES
		BREAK
		CASE E_SET_PED_PROP_INDEXES
			interface.setPedPropIndexes = &_SET_MUSIC_STUDIO_PED_PROP_INDEXES
		BREAK
		CASE E_HAS_PED_BEEN_CREATED
			interface.returnHasPedBeenCreated = &_HAS_MUSIC_STUDIO_PED_BEEN_CREATED
		BREAK
		CASE E_DOES_ACTIVE_PED_TOTAL_CHANGE
			interface.returnDoesActivePedTotalChange = &_DOES_MUSIC_STUDIO_ACTIVE_PED_TOTAL_CHANGE
		BREAK
		
		//Head tracking
		CASE E_SET_PED_HEAD_TRACKING_DATA
			interface.setPedHeadTrackingData = &_SET_MUSIC_STUDIO_PED_HEAD_TRACKING_DATA
		BREAK
		// Ped Speech
		CASE E_SET_PED_SPEECH_DATA
			interface.setPedSpeechData = &_SET_MUSIC_STUDIO_PED_SPEECH_DATA
		BREAK
		CASE E_CAN_PED_PLAY_SPEECH
			interface.returnCanPedPlaySpeech = &_CAN_MUSIC_STUDIO_PED_PLAY_SPEECH
		BREAK
		CASE E_GET_PED_SPEECH_TYPE
			interface.returnGetPedSpeechType = &_GET_MUSIC_STUDIO_PED_SPEECH_TYPE
		BREAK
		CASE E_GET_PED_CONTROLLER_SPEECH
			interface.returnGetPedControllerSpeech = &_GET_MUSIC_STUDIO_PED_CONTROLLER_SPEECH
		BREAK
		CASE E_GET_PED_CONVO_DATA
			interface.getPedConvoData = &_GET_MUSIC_STUDIO_PED_CONVO_DATA
		BREAK
		CASE E_GET_PED_PROP_ANIM_DATA
			interface.getPedPropAnimData = &_GET_MUSIC_STUDIO_PED_PROP_ANIM_DATA
		BREAK		
		// Parent Ped Data
		CASE E_SET_PED_PARENT_DATA
			interface.setPedParentData = &_SET_MUSIC_STUDIO_PED_PARENT_DATA
		BREAK
		// Patrol Data
		CASE E_SET_PED_PATROL_DATA
			interface.setPedPatrolData = &_SET_MUSIC_STUDIO_PED_PATROL_DATA
		BREAK

	ENDSWITCH
ENDPROC
#ENDIF	// FEATURE_TUNER
