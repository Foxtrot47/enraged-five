USING "net_common_hacking_base.sch"

CONST_INT ciOU_NODE_REQUIRED			0
CONST_INT ciOU_NODE_SELECTED			1
CONST_INT ciOU_NODE_RECCENTLY_SELECTED	2

CONST_INT ciOU_PATTTERNS_SELECTED 0
CONST_INT ciOU_CHECKING_MATCH 1
CONST_INT ciOU_SHOWING_LOADING 2
CONST_INT ciOU_CORRECT_NODE 3
CONST_INT ciOU_FAILED_NODE 4
CONST_INT ciOU_SCRAMBLING 5
CONST_INT ciOU_SCRAMBLED 6
CONST_INT ciOU_SHOWING_NEXT_PATTERN 7
CONST_INT ciOU_SHOWING_RANDOM_PATTERN 8
CONST_INT ciOU_TIMER_STARTED 9
CONST_INT ciOU_PATTERN_FINISHED 10
CONST_INT ciOU_SHOWING_LAST_SELECTED 11
CONST_INT ciOU_SHOWING_NEXT_ROW 12
CONST_INT ciOU_SHOWING_STARTING_HELP 13

CONST_INT ciOU_PATTERN_LENGTH 6
CONST_INT ciOU_NUMBER_OF_OPTIONS 5
CONST_INT ciOU_NUMBER_OF_PATTERNS 4

CONST_INT ciOU_MAX_LIVES  6
CONST_INT ciOU_STARTUP_TIMER 4000
CONST_INT ciOU_HACK_TIME  300000
CONST_INT ciOU_CHECKING_TIME 5000
CONST_INT ciOU_PRE_CHECK_TIME 2500
CONST_INT ciOU_SCRAMBLE_TIME 30000
CONST_INT ciOU_END_DELAY 4000
CONST_INT ciOU_PATTERN_ANIMATION_TIME 2000
TWEAK_INT ciOU_SELECT_TIME 825
CONST_INT ciOU_RANDOM_PATTERN_CHANGE_TIME 250
CONST_INT ciOU_INTRO_HELP_TIME 5000

TWEAK_FLOAT cfOU_GRID_START_POSITION_X 0.073
TWEAK_FLOAT cfOU_GRID_START_POSITION_Y 0.317
TWEAK_FLOAT cfOU_GRID_COLUMN_DIFFERENCE_X 0.1
TWEAK_FLOAT cfOU_GRID_ROW_DIFFERENCE_Y 0.1

CONST_FLOAT cfOU_CORRECT_CIRCLE_WIDTH 100.0
CONST_FLOAT cfOU_CORRECT_CIRCLE_HEIGHT 100.0
CONST_FLOAT cfOU_INCORRECT_CIRCLE_WIDTH 32.0
CONST_FLOAT cfOU_INCORRECT_CIRCLE_HEIGHT 32.0
CONST_FLOAT cfOU_SELECTOR_CIRCLE_WIDTH 128.0
CONST_FLOAT cfOU_SELECTOR_CIRCLE_HEIGHT 128.0

TWEAK_FLOAT cfOU_SCRAMBLE_POSITIONX 0.313
TWEAK_FLOAT cfOU_SCRAMBLE_POSITIONY 0.846
TWEAK_FLOAT cfOU_SCRAMBLE_LEFTPOINT 0.178

TWEAK_FLOAT cfOU_TIMER_POSITIONX 0.122
TWEAK_FLOAT cfOU_TIMER_POSITIONY 0.144
CONST_FLOAT cfOU_TIMER_DIGIT_DIFFERENCE 0.031

TWEAK_FLOAT cfOU_LIVES_POSITIONX 0.535
TWEAK_FLOAT cfOU_LIVES_POSITIONY 0.145
CONST_FLOAT	cfOU_LIVES_OFFSET 0.055

TWEAK_FLOAT cfOU_NUMPAD_POSITIONX 0.878
TWEAK_FLOAT cfOU_NUMPAD_POSITIONY 0.433
TWEAK_FLOAT cfOU_NUMPAD_OFFSETX 0.0944
TWEAK_FLOAT cfOU_NUMPAD_OFFSETY 0.0944
CONST_FLOAT cfOU_NUMPAD_ELEMENT_WIDTH 100.0
CONST_FLOAT cfOU_NUMPAD_ELEMENT_HEIGHT 100.0

TWEAK_FLOAT cfOU_PROGRESS_TRACKER_POSITIONX 0.777
TWEAK_FLOAT cfOU_PROGRESS_TRACKER_POSITIONY 0.773
TWEAK_FLOAT cfOU_PROGRESS_TRACKER_OFFSETX 0.067
CONST_FLOAT cfOU_PROGRESS_TRACKER_ELEMENT_WIDTH 64.0
CONST_FLOAT cfOU_PROGRESS_TRACKER_ELEMENT_HEIGHT 64.0
CONST_FLOAT cfOU_PROGRESS_TRACKER_SELECTOR_WIDTH 80.0
CONST_FLOAT cfOU_PROGRESS_TRACKER_SELECTOR_HEIGHT 80.0

TWEAK_INT ciOU_CLEAR_FRAME 0

STRUCT ORDER_UNLOCK_GAMEPLAY_DATA
	HACKING_GAME_STRUCT sBaseStruct
	
	//Gameplay
	INT iOU_Bs
	INT iSelectedNode
	
	INT iNumberOfLastMatchedElements
	
	INT iCurrentPatternIndex
	INT iCurrentColumnIndex
	
	INT iNodeStructure[ciOU_NUMBER_OF_PATTERNS][ciOU_PATTERN_LENGTH][ciOU_NUMBER_OF_OPTIONS]
	INT iKeypadNumbers[ciOU_NUMBER_OF_PATTERNS]
	
	INT iRandomValue[ciOU_PATTERN_LENGTH]
	INT iUIAnimationFrame
	INT iUIRegularAnimationFrame
	
	SCRIPT_TIMER tdPauseTimer
	SCRIPT_TIMER tdRandomiseTimer
	SCRIPT_TIMER tdStartHelpTimer
	
	HG_RGBA_COLOUR_STRUCT rgbaTintColour

ENDSTRUCT

PROC PLAN_PATTERNS_FOR_ORDER_UNLOCK(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData)
	INT i
	INT j
	INT k
	INT integer
	INT newPositions[ciOU_PATTERN_LENGTH]
	
	
	FOR i = 0 TO ciOU_NUMBER_OF_PATTERNS-1
		FOR integer = 0 TO ciOU_PATTERN_LENGTH-1
			newPositions[integer] = -1
		ENDFOR

		FOR integer = 0 TO ciOU_PATTERN_LENGTH-1
			newPositions[integer] = GET_RANDOM_INT_IN_RANGE(0, ciOU_NUMBER_OF_OPTIONS)
		ENDFOR
		
		FOR integer=1 TO ciOU_PATTERN_LENGTH-2
			IF newPositions[integer] = newPositions[integer-1]
			OR newPositions[integer] = newPositions[integer+1]
				newPositions[integer] = GET_RANDOM_INT_IN_RANGE(0, ciOU_NUMBER_OF_OPTIONS)
			ENDIF
		ENDFOR
	
	
		PRINTLN("[MC][PLAN_PATTERNS_FOR_ORDER_UNLOCK] Pattern: ", i)
		
		#IF IS_DEBUG_BUILD
		FOR integer = 0 TO  ciOU_PATTERN_LENGTH-1
			PRINTLN("[MC][PLAN_PATTERNS_FOR_ORDER_UNLOCK] Target location for column ", integer, " is node ", newPositions[integer])
		ENDFOR
		#ENDIF
		
		
		FOR j = 0 TO ciOU_PATTERN_LENGTH-1
			PRINTLN("[MC][PLAN_PATTERNS_FOR_ORDER_UNLOCK] Column: ", j)
			
			FOR k = 0 TO ciOU_NUMBER_OF_OPTIONS - 1
				PRINTLN("[MC][PLAN_PATTERNS_FOR_ORDER_UNLOCK] Element: ", k )
				IF k = newPositions[j]
					SET_BIT(sGameplayData.iNodeStructure[i][j][k], ciOU_NODE_REQUIRED)
					PRINTLN("[MC][PLAN_PATTERNS_FOR_ORDER_UNLOCK] Required Element: ", k )
				ENDIF
			ENDFOR
		ENDFOR
	ENDFOR

ENDPROC

FUNC BOOL OU_IS_NODE_CORRECTLY_SELECTED(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, INT iColumn, INT iNode )
	RETURN IS_BIT_SET(sGameplayData.iNodeStructure[sGameplayData.iCurrentPatternIndex][iColumn][iNode], ciOU_NODE_SELECTED) 
	AND IS_BIT_SET(sGameplayData.iNodeStructure[sGameplayData.iCurrentPatternIndex][iColumn][iNode], ciOU_NODE_REQUIRED)
ENDFUNC

FUNC BOOL OU_IS_NODE_INCORRECTLY_SELECTED(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, INT iColumn, INT iNode )
	RETURN IS_BIT_SET(sGameplayData.iNodeStructure[sGameplayData.iCurrentPatternIndex][iColumn][iNode], ciOU_NODE_SELECTED) 
	AND NOT IS_BIT_SET(sGameplayData.iNodeStructure[sGameplayData.iCurrentPatternIndex][iColumn][iNode], ciOU_NODE_REQUIRED)
ENDFUNC

FUNC BOOL OU_IS_NODE_REQUIRED(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, INT iColumn, INT iNode )
	RETURN IS_BIT_SET(sGameplayData.iNodeStructure[sGameplayData.iCurrentPatternIndex][iColumn][iNode], ciOU_NODE_REQUIRED) 
ENDFUNC

FUNC BOOL OU_IS_NODE_SELECTED(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, INT iColumn, INT iNode )
	RETURN IS_BIT_SET(sGameplayData.iNodeStructure[sGameplayData.iCurrentPatternIndex][iColumn][iNode], ciOU_NODE_SELECTED) 
ENDFUNC

FUNC BOOL OU_IS_NODE_RECCENTLY_SELECTED(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, INT iColumn, INT iNode )
	RETURN IS_BIT_SET(sGameplayData.iNodeStructure[sGameplayData.iCurrentPatternIndex][iColumn][iNode], ciOU_NODE_RECCENTLY_SELECTED) 
ENDFUNC

PROC OU_CLEAR_NODE_RECCENTLY_SELECTED(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, INT iColumn, INT iNode )
	CLEAR_BIT(sGameplayData.iNodeStructure[sGameplayData.iCurrentPatternIndex][iColumn][iNode], ciOU_NODE_RECCENTLY_SELECTED) 
ENDPROC

PROC OU_CLEAR_ALL_NODES_RECCENTLY_SELECTED(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData)
	INT i
	INT j
	INT k
	
	FOR i = 0 TO ciOU_NUMBER_OF_PATTERNS-1
		FOR j = 0 TO ciOU_PATTERN_LENGTH-1
			FOR k = 0 TO ciOU_NUMBER_OF_OPTIONS - 1
				CLEAR_BIT(sGameplayData.iNodeStructure[i][j][k], ciOU_NODE_RECCENTLY_SELECTED)
			ENDFOR
		ENDFOR
	ENDFOR
ENDPROC

FUNC BOOL OU_IS_PLAYER_ABLE_TO_INTERACT(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData)
	IF sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_PLAY
	AND sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_VISUAL_TEST
		//PRINTLN("[MC][OrderUnlock] No interact - Wrong state")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
		PRINTLN("[MC][OrderUnlock] No interact - checking match")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_CORRECT_NODE)
		PRINTLN("[MC][OrderUnlock] No interact - passing fingerprint")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_FAILED_NODE)
		PRINTLN("[MC][OrderUnlock] No interact - failed fingerprint")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SCRAMBLING)
		PRINTLN("[MC][OrderUnlock] No interact - scrambling")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_RANDOM_PATTERN)
		PRINTLN("[MC][OrderUnlock] No interact - showing random pattern")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_NEXT_PATTERN)
		PRINTLN("[MC][OrderUnlock] No interact - showing next pattern")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_NEXT_ROW)
		PRINTLN("[MC][OrderUnlock] No interact - showing next row")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MOVE_SELECTOR(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, CONTROL_ACTION eControlAction)
	//INT iTempElement = sGameplayData.iSelectedElement
	PRINTLN("[MC][OrderUnlock] Old Selector Position ", sGameplayData.iSelectedNode)
	//PRINTLN("[MC][OrderUnlock] Action ", eControlAction)
	SWITCH eControlAction
		CASE INPUT_FRONTEND_UP
		PRINTLN("[MC][OrderUnlock] Action Up")
			sGameplayData.iSelectedNode -= 1
			
			IF sGameplayData.iSelectedNode < 0
				sGameplayData.iSelectedNode = ciOU_NUMBER_OF_OPTIONS -1

			ENDIF
		BREAK
		CASE INPUT_FRONTEND_DOWN
		PRINTLN("[MC][OrderUnlock] Action Down")
			sGameplayData.iSelectedNode += 1
			
			IF sGameplayData.iSelectedNode >= ciOU_NUMBER_OF_OPTIONS
				sGameplayData.iSelectedNode = 0
			ENDIF
		BREAK
	
	ENDSWITCH
	
	PLAY_SOUND_FRONTEND(-1, "Cursor_Move", sGameplayData.sBaseStruct.sAudioSet)
	PRINTLN("[MC][OrderUnlock] New Selector Position ", sGameplayData.iSelectedNode)
ENDPROC

PROC CHECK_NODE(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData)
	IF NOT OU_IS_NODE_SELECTED(sGameplayData, sGameplayData.iCurrentColumnIndex, sGameplayData.iSelectedNode)
		SET_BIT(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
		
		OU_CLEAR_ALL_NODES_RECCENTLY_SELECTED(sGameplayData)
	ENDIF
ENDPROC

PROC PROCESS_ORDER_UNLOCK_INPUT(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData)
	CONTROL_ACTION eLeftStick = HG_GET_LEFT_STICK_ACTION(sGameplayData.sBaseStruct)
	
	// Move around the selection
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	OR eLeftStick = INPUT_FRONTEND_UP
		MOVE_SELECTOR(sGameplayData, INPUT_FRONTEND_UP)
	ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	OR eLeftStick = INPUT_FRONTEND_DOWN
		MOVE_SELECTOR(sGameplayData, INPUT_FRONTEND_DOWN)
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		IF NOT IS_BIT_SET(sGameplayData.iOU_Bs, ciHG_ABORT_WARNING)
			RESET_NET_TIMER(sGameplayData.sBaseStruct.tdCancelTimer)
			SET_BIT(sGameplayData.iOU_Bs, ciHG_ABORT_WARNING)
		ENDIF
	ENDIF
	
	
	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	OR (IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT))
		 CHECK_NODE(sGameplayData)
	ENDIF
//	IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
//		START_ELEMENT_CHECK(sGameplayData)
//	ENDIF
ENDPROC

PROC MAINTAIN_ORDER_UNLOCK_HELP(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, BOOL bShowFinalQuitWarning, HG_CONTROL_STRUCT sControllerStruct)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HG_FC_HELP_04a")
	AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		EXIT
	ENDIF
	
	// Quit
	IF IS_BIT_SET(sGameplayData.iOU_Bs, ciHG_ABORT_WARNING)
		IF NOT IS_BIT_SET(sControllerStruct.iBS, ciHGBS_TEAM_HAS_AGGRO)
			IF NOT bShowFinalQuitWarning
				PRINT_HELP_NO_SOUND("HG_FC_HELP_04a", ciHG_FIRST_ABORT_TIME)
			ELSE
				PRINT_HELP_NO_SOUND("HG_FC_HELP_04")
			ENDIF
		ENDIF
			
		IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdCancelTimer, ciHG_ABORT_WARNING_TIME)
			CLEAR_BIT(sGameplayData.iOU_Bs, ciHG_ABORT_WARNING)
		ENDIF
		
		EXIT
	ENDIF
	
	// scramble
	IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_STARTING_HELP)
		PRINT_HELP_NO_SOUND("HG_OU_HELP_00")
		
		IF HAS_NET_TIMER_EXPIRED(sGameplayData.tdStartHelpTimer, ciOU_INTRO_HELP_TIME)
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_STARTING_HELP)
		ENDIF
		
		EXIT
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SCRAMBLING)
		PRINT_HELP_NO_SOUND("HG_FC_HELP_03")
		EXIT
	ENDIF
	
	// Select
	IF OU_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
		PRINT_HELP_NO_SOUND("HG_OU_HELP_01")
	ENDIF
ENDPROC

PROC MAINTAIN_ORDER_UNLOCK_CONTEXT(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData)
	IF sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_PLAY
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
			PRINTLN("[MC][OrderUnlock] Drawing cancel prompt")
		ENDIF
		
		SPRITE_PLACEMENT thisSpritePlacement = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
		
		IF sGameplayData.sBaseStruct.bUpdatePrompts
			REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sGameplayData.sBaseStruct.scaleformInstructionalButtons)
			
			// Exit
			//ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "HG_INT_04", sGameplayData.sBaseStruct.scaleformInstructionalButtons)
			IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
				DRAW_GENERIC_METER(GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sGameplayData.sBaseStruct.tdCancelTimer), ci_HG_QUIT_TIME, "HG_INT_04a")
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HG_INT_04",sGameplayData.sBaseStruct.scaleformInstructionalButtons )
			ELSE
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HG_INT_04",sGameplayData.sBaseStruct.scaleformInstructionalButtons )
			ENDIF
			
			// Select
			ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "HG_INT_01", sGameplayData.sBaseStruct.scaleformInstructionalButtons)
			
			// Change Selector
			ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_ALL), "HG_INT_03", sGameplayData.sBaseStruct.scaleformInstructionalButtons)			
			
			sGameplayData.sBaseStruct.bUpdatePrompts = FALSE
		ENDIF
		
		//SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sGameplayData.sBaseStruct.scaleformInstructionalButtonsIndex, thisSpritePlacement, sGameplayData.sBaseStruct.scaleformInstructionalButtons)
	ELSE
		RESET_SCALEFORM_INSTRUCTIONAL_BUTTON(sGameplayData.sBaseStruct.scaleformInstructionalButtons)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sGameplayData.sBaseStruct.scaleformInstructionalButtonsIndex)
	ENDIF
ENDPROC

PROC SCRAMBLE_CURRENT_PATTERN(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData)
	INT i
	INT j
	INT iRandomValue
	
	iRandomValue = GET_RANDOM_INT_IN_RANGE(0, 100)
	
	IF iRandomValue < sGameplayData.sBaseStruct.iScrambleWipeChance 
		sGameplayData.iCurrentColumnIndex = 0
	ENDIF
	
	FOR i = 0 TO ciOU_PATTERN_LENGTH - 1
		IF i >=  sGameplayData.iCurrentColumnIndex
			iRandomValue = GET_RANDOM_INT_IN_RANGE(0, ciOU_NUMBER_OF_OPTIONS-1)
		
			FOR j = 0 TO ciOU_NUMBER_OF_OPTIONS -1
				CLEAR_BIT(sGameplayData.iNodeStructure[sGameplayData.iCurrentPatternIndex][i][j], ciOU_NODE_REQUIRED)
				CLEAR_BIT(sGameplayData.iNodeStructure[sGameplayData.iCurrentPatternIndex][i][j], ciOU_NODE_SELECTED)
				CLEAR_BIT(sGameplayData.iNodeStructure[sGameplayData.iCurrentPatternIndex][i][j], ciOU_NODE_RECCENTLY_SELECTED)
				
			ENDFOR
			
			SET_BIT(sGameplayData.iNodeStructure[sGameplayData.iCurrentPatternIndex][i][iRandomValue], ciOU_NODE_REQUIRED)
			
			PRINTLN("[MC][OrderUnlock] Scrambling - Column: ", i, " Node: ", iRandomValue)
			IF OU_IS_NODE_REQUIRED(sGameplayData, i, iRandomValue)
				PRINTLN("[MC][OrderUnlock] Scrambling - BIT SET FOR Column: ", i, " Node: ", iRandomValue)
			ENDIF
		ENDIF
	ENDFOR

ENDPROC

PROC PROCESS_ORDER_UNLOCK_GAME(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct, BOOL bShowFinalQuitWarning)
	IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
	AND NOT IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_CORRECT_NODE)
	AND NOT IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_FAILED_NODE)
		SET_BIT(sGameplayData.iNodeStructure[sGameplayData.iCurrentPatternIndex][sGameplayData.iCurrentColumnIndex][sGameplayData.iSelectedNode], ciOU_NODE_SELECTED)
		SET_BIT(sGameplayData.iNodeStructure[sGameplayData.iCurrentPatternIndex][sGameplayData.iCurrentColumnIndex][sGameplayData.iSelectedNode], ciOU_NODE_RECCENTLY_SELECTED)
		PRINTLN("[MC][OrderUnlock] Setting Last Selected on node ", sGameplayData.iSelectedNode)
		SET_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_LAST_SELECTED)
		PRINTLN("[MC][OrderUnlock] Setting Last Selected")
		RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
		sGameplayData.iUIRegularAnimationFrame = 0
		IF OU_IS_NODE_CORRECTLY_SELECTED(sGameplayData, sGameplayData.iCurrentColumnIndex, sGameplayData.iSelectedNode)
			SET_BIT(sGameplayData.iOU_Bs, ciOU_CORRECT_NODE)
			SET_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_NEXT_ROW)
			PLAY_SOUND_FRONTEND(-1, "Cursor_Choose_Good", sGameplayData.sBaseStruct.sAudioSet)
		ELSE
			SET_BIT(sGameplayData.iOU_Bs, ciOU_FAILED_NODE)
			PLAY_SOUND_FRONTEND(-1, "Cursor_Choose_Bad", sGameplayData.sBaseStruct.sAudioSet)
		ENDIF
	ENDIF
	
	
	// Help Text
	MAINTAIN_ORDER_UNLOCK_HELP(sGameplayData, bShowFinalQuitWarning, sControllerStruct)
	
	IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdScrambleTimer, sGameplayData.sBaseStruct.iScrambleTime)
	AND OU_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
		SET_BIT(sGameplayData.iOU_Bs, ciOU_SCRAMBLING)
		CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_SCRAMBLED)
		PRINTLN("[MC][OrderUnlock] Scrambling")
		RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
	ELIF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SCRAMBLING)
		IF NOT IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SCRAMBLED)
			SCRAMBLE_CURRENT_PATTERN(sGameplayData)
			SET_BIT(sGameplayData.iOU_Bs, ciOU_SCRAMBLED)
		ENDIF
		
		SET_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_NEXT_PATTERN)
		SET_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_RANDOM_PATTERN)
		RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
		CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_SCRAMBLING)
		CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_SCRAMBLED)
		
//		IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdAnimationTimer, ciOU_CHECKING_TIME)
//			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_SCRAMBLING)
//			RESET_NET_TIMER(sGameplayData.sBaseStruct.tdScrambleTimer)
//			PRINTLN("[MC][OrderUnlock] Finishing Scrambling")
//			PRINTLN("[MC][OrderUnlock] Setting Showing Next Pattern")
//			SET_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_NEXT_PATTERN)
//			RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
//		ENDIF
	ENDIF
	
	// Audio
	IF sGameplayData.iUIAnimationFrame = 0
		IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_NEXT_ROW)
		AND NOT IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_LAST_SELECTED)
			PLAY_SOUND_FRONTEND(-1, "New_Line_Flash", sGameplayData.sBaseStruct.sAudioSet)
		ELIF (IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_RANDOM_PATTERN)
		OR IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SCRAMBLING))
			PLAY_SOUND_FRONTEND(-1, "Dot_Sequence_Change", sGameplayData.sBaseStruct.sAudioSet)
		ELIF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_NEXT_PATTERN)
			PLAY_SOUND_FRONTEND(-1, "Dot_Sequence_Choose_Flash", sGameplayData.sBaseStruct.sAudioSet)
		ENDIF
	ENDIF
	
	IF (IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
	AND IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_CORRECT_NODE)
	AND IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_PATTERN_FINISHED)
	AND NOT IS_BIT_SET(sGameplayData.sBaseStruct.iHGSoundBitset, ciHG_BS_SOUND_SECTIONCLEAR))
		PLAY_SOUND_FRONTEND(-1, "Section_Success_Window_Appears", sGameplayData.sBaseStruct.sAudioSet)
		SET_BIT(sGameplayData.sBaseStruct.iHGSoundBitset, ciHG_BS_SOUND_SECTIONCLEAR)
	ELIF (NOT IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
	AND NOT IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_CORRECT_NODE)
	AND NOT IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_PATTERN_FINISHED)
	AND IS_BIT_SET(sGameplayData.sBaseStruct.iHGSoundBitset, ciHG_BS_SOUND_SECTIONCLEAR))
		PLAY_SOUND_FRONTEND(-1, "Section_Success_Window_Cleared", sGameplayData.sBaseStruct.sAudioSet)
		CLEAR_BIT(sGameplayData.sBaseStruct.iHGSoundBitset, ciHG_BS_SOUND_SECTIONCLEAR)
	ENDIF
	
	
	
	// Random Patterns
	IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_RANDOM_PATTERN)
		IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdAnimationTimer, ciOU_PATTERN_ANIMATION_TIME)
		AND sGameplayData.iUIAnimationFrame != 0
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_RANDOM_PATTERN)
			RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
		ENDIF
	ELIF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_NEXT_PATTERN)
		PRINTLN("[MC][OrderUnlock] Showing Next Pattern is set")
		IF NOT IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_TIMER_STARTED)
		AND sGameplayData.iUIAnimationFrame != 0
			REINIT_NET_TIMER(sGameplayData.sBaseStruct.tdTimer)
			REINIT_NET_TIMER(sGameplayData.sBaseStruct.tdScrambleTimer)
		ENDIF
		
		IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdAnimationTimer, ciOU_PATTERN_ANIMATION_TIME)
		AND sGameplayData.iUIAnimationFrame != 0
			PRINTLN("[MC][OrderUnlock] Clearing Next Pattern")
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_NEXT_PATTERN)
			
			IF NOT IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_TIMER_STARTED)
				REINIT_NET_TIMER(sGameplayData.sBaseStruct.tdTimer)
				REINIT_NET_TIMER(sGameplayData.sBaseStruct.tdScrambleTimer)
				SET_BIT(sGameplayData.iOU_Bs, ciOU_TIMER_STARTED)
			ELSE
				RESET_NET_TIMER(sGameplayData.sBaseStruct.tdScrambleTimer)	
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_LAST_SELECTED)
		IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdAnimationTimer, ciOU_SELECT_TIME)
		AND sGameplayData.iUIRegularAnimationFrame = ciOU_CLEAR_FRAME
			PRINTLN("[MC][OrderUnlock] Clearing Last Selected")
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_LAST_SELECTED)
			sGameplayData.iUIRegularAnimationFrame = 0
			IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_NEXT_ROW)
				RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
			ENDIF
		ENDIF
	ELIF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_NEXT_ROW)
		IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdAnimationTimer, ciOU_SELECT_TIME)
		AND sGameplayData.iUIRegularAnimationFrame = ciOU_CLEAR_FRAME
			PRINTLN("[MC][OrderUnlock] Clearing Showing Next Row")
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_NEXT_ROW)
		ENDIF
	ENDIF
	
	// Do animation
	IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
		IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_CORRECT_NODE)
			
			IF NOT IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_PATTERN_FINISHED)
				sGameplayData.iCurrentColumnIndex++
			ENDIF
			//sGameplayData.iSelectedNode = 0
			
			IF sGameplayData.iCurrentColumnIndex >= ciOU_PATTERN_LENGTH
				IF NOT IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_LAST_SELECTED)
					IF NOT IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_PATTERN_FINISHED)
						RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
						SET_BIT(sGameplayData.iOU_Bs, ciOU_PATTERN_FINISHED)
					ELSE
						IF HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdAnimationTimer, ciOU_PRE_CHECK_TIME)
							sGameplayData.iCurrentPatternIndex++
							
							sGameplayData.iCurrentColumnIndex = 0
							
							SET_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_NEXT_PATTERN)
							SET_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_RANDOM_PATTERN)
							CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_PATTERN_FINISHED)
							RESET_NET_TIMER(sGameplayData.tdRandomiseTimer)
							RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
							RESET_NET_TIMER(sGameplayData.sBaseStruct.tdScrambleTimer)
							PRINTLN("[MC][OrderUnlock] New Pattern is ", sGameplayData.iCurrentPatternIndex)	
				
							IF sGameplayData.iCurrentPatternIndex >= sGameplayData.sBaseStruct.iNumberOfStages
								sGameplayData.iCurrentPatternIndex = sGameplayData.sBaseStruct.iNumberOfStages - 1
								PRINTLN("[MC][OrderUnlock] Passing Game")
								UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_PASS)
							ELSE
								IF NOT IS_BIT_SET(sControllerStruct.iBS, ciHGBS_PASSED_SECTION)
									SET_BIT(sControllerStruct.iBS, ciHGBS_PASSED_SECTION)
								ENDIF
							ENDIF
							
							CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
							CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_CORRECT_NODE)
							CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_FAILED_NODE)
							
						ENDIF
					ENDIF
				ENDIF
			ELSE
				CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
				CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_CORRECT_NODE)
				CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_FAILED_NODE)
			ENDIF
		ELIF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_FAILED_NODE)
			sGameplayData.sBaseStruct.iCurrentLives--
			//sGameplayData.iSelectedElement = 0
			IF sGameplayData.sBaseStruct.iCurrentLives <= 0
				PRINTLN("[MC][OrderUnlock] Out of Lives")
				UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_FAIL)
			ELSE
				IF NOT IS_BIT_SET(sControllerStruct.iBS, ciHGBS_LOST_LIFE)
					SET_BIT(sControllerStruct.iBS, ciHGBS_LOST_LIFE)
				ENDIF
			ENDIF
			
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_CORRECT_NODE)
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_FAILED_NODE)
		ENDIF
		
	ENDIF

ENDPROC

PROC DRAW_ORDER_UNLOCK_GRID(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData)
	INT i
	INT j
	
	FLOAT fColumnPositionX
	FLOAT fRowPositionY
	
	// do frame counter change
	sGameplayData.iUIAnimationFrame += sGameplayData.sBaseStruct.iSlowUpdateFrames
	sGameplayData.iUIRegularAnimationFrame += sGameplayData.sBaseStruct.iDefaultUpdateFrames
	
	IF sGameplayData.iUIAnimationFrame >= 2
		sGameplayData.iUIAnimationFrame = 0
	ENDIF
	
	IF sGameplayData.iUIRegularAnimationFrame >= 2
		sGameplayData.iUIRegularAnimationFrame = 0
	ENDIF
	
	FOR i = 0 TO ciOU_PATTERN_LENGTH-1
		fColumnPositionX = cfOU_GRID_START_POSITION_X + (i * cfOU_GRID_COLUMN_DIFFERENCE_X)
		
		IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_RANDOM_PATTERN)
		OR IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SCRAMBLING)
			IF sGameplayData.iUIAnimationFrame != 0
				sGameplayData.iRandomValue[i] = GET_RANDOM_INT_IN_RANGE(0, ciOU_NUMBER_OF_OPTIONS-1)
				RESET_NET_TIMER(sGameplayData.tdRandomiseTimer)
				PRINTLN("[MC][OrderUnlock] Randomising required to ", sGameplayData.iRandomValue[i])
			ENDIF
		ENDIF
	
		FOR j = 0 TO ciOU_NUMBER_OF_OPTIONS-1
			fRowPositionY = cfOU_GRID_START_POSITION_Y + (j * cfOU_GRID_ROW_DIFFERENCE_Y)
			
			IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_RANDOM_PATTERN)
			OR IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SCRAMBLING)
				IF sGameplayData.iUIAnimationFrame = 0
					IF i < sGameplayData.iCurrentColumnIndex
						IF OU_IS_NODE_CORRECTLY_SELECTED(sGameplayData, i, j)
							DRAW_HACKING_SPRITE("mporderunlock", "correct_circles", fColumnPositionX, fRowPositionY, cfOU_CORRECT_CIRCLE_WIDTH, cfOU_CORRECT_CIRCLE_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
						ENDIF
					ELSE
						IF j = sGameplayData.iRandomValue[i]
							DRAW_HACKING_SPRITE("mporderunlock", "correct_circles", fColumnPositionX, fRowPositionY, cfOU_CORRECT_CIRCLE_WIDTH, cfOU_CORRECT_CIRCLE_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
							PRINTLN("[MC][OrderUnlock] Randomising - Drawing at ", j, " for value ", sGameplayData.iRandomValue[i])
						ENDIF
					ENDIF
				ENDIF
			ELIF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_NEXT_PATTERN)
				IF sGameplayData.iUIAnimationFrame = 0
					IF OU_IS_NODE_REQUIRED(sGameplayData, i, j)
						DRAW_HACKING_SPRITE("mporderunlock", "correct_circles", fColumnPositionX, fRowPositionY, cfOU_CORRECT_CIRCLE_WIDTH, cfOU_CORRECT_CIRCLE_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
						PRINTLN("[MC][OrderUnlock] Next Pattern - Drawing at ", j)
					ENDIF
				ENDIF
			ELSE
				// DRAW THE GRID
				IF OU_IS_NODE_CORRECTLY_SELECTED(sGameplayData, i, j)
					IF OU_IS_NODE_RECCENTLY_SELECTED(sGameplayData, i, j)
						IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_LAST_SELECTED)
							IF sGameplayData.iUIRegularAnimationFrame = 0
								DRAW_HACKING_SPRITE("mporderunlock", "correct_circles", fColumnPositionX, fRowPositionY, cfOU_CORRECT_CIRCLE_WIDTH, cfOU_CORRECT_CIRCLE_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
							ENDIF
						ELSE
							OU_CLEAR_NODE_RECCENTLY_SELECTED(sGameplayData, i, j)
							PRINTLN("[MC][OrderUnlock] Clearing Last Selected on node ", j)
						ENDIF
					ELSE
						DRAW_HACKING_SPRITE("mporderunlock", "correct_circles", fColumnPositionX, fRowPositionY, cfOU_CORRECT_CIRCLE_WIDTH, cfOU_CORRECT_CIRCLE_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
					ENDIF
				
				ELIF OU_IS_NODE_INCORRECTLY_SELECTED(sGameplayData, i, j)
					IF OU_IS_NODE_RECCENTLY_SELECTED(sGameplayData, i, j)
						IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_LAST_SELECTED)
							IF sGameplayData.iUIRegularAnimationFrame = 0
								DRAW_HACKING_SPRITE("mporderunlock", "incorrect_circles", fColumnPositionX, fRowPositionY, cfOU_INCORRECT_CIRCLE_WIDTH, cfOU_INCORRECT_CIRCLE_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
							ENDIF
						ELSE
							OU_CLEAR_NODE_RECCENTLY_SELECTED(sGameplayData, i, j)
							PRINTLN("[MC][OrderUnlock] Clearing Last Selected on node ", j)
						ENDIF
					ELSE
						DRAW_HACKING_SPRITE("mporderunlock", "incorrect_circles", fColumnPositionX, fRowPositionY, cfOU_INCORRECT_CIRCLE_WIDTH, cfOU_INCORRECT_CIRCLE_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
					ENDIF
				ENDIF
			ENDIF
			
			IF i <= sGameplayData.iCurrentColumnIndex
				IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_NEXT_ROW)
				AND i = sGameplayData.iCurrentColumnIndex
					IF sGameplayData.iUIRegularAnimationFrame = 0
					AND NOT IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SHOWING_LAST_SELECTED)
						DRAW_HACKING_SPRITE("mporderunlock", "Inner_Circles", fColumnPositionX, fRowPositionY, cfOU_SELECTOR_CIRCLE_WIDTH, cfOU_SELECTOR_CIRCLE_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
					ENDIF
				ELSE
					DRAW_HACKING_SPRITE("mporderunlock", "Inner_Circles", fColumnPositionX, fRowPositionY, cfOU_SELECTOR_CIRCLE_WIDTH, cfOU_SELECTOR_CIRCLE_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
				ENDIF
			ENDIF
			
			IF sGameplayData.iCurrentColumnIndex = i
			AND sGameplayData.iSelectedNode = j
			AND OU_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
				DRAW_HACKING_SPRITE("mporderunlock", "selector", fColumnPositionX, fRowPositionY, cfOU_SELECTOR_CIRCLE_WIDTH, cfOU_SELECTOR_CIRCLE_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF sGameplayData.sBaseStruct.bDebugShowGuide
			AND OU_IS_NODE_REQUIRED(sGameplayData, i, j)
				DRAW_HACKING_SPRITE("mporderunlock", "correct_circles", fColumnPositionX, fRowPositionY, cfOU_INCORRECT_CIRCLE_WIDTH, cfOU_INCORRECT_CIRCLE_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
			ENDIF
			#ENDIF
		ENDFOR
	ENDFOR
ENDPROC

FUNC FLOAT OU_GET_NUMPAD_POS(INT iValue, BOOL bIsX)
	IF bIsX
		SWITCH iValue
			CASE 1
			CASE 4
			CASE 7
				RETURN cfOU_NUMPAD_POSITIONX - cfOU_NUMPAD_OFFSETX
			BREAK
			
			CASE 0
			CASE 2
			CASE 5
			CASE 8
				RETURN cfOU_NUMPAD_POSITIONX
			BREAK
			
			CASE 3
			CASE 6
			CASE 9
				RETURN cfOU_NUMPAD_POSITIONX + cfOU_NUMPAD_OFFSETX
			BREAK
		ENDSWITCH
	ELSE
		SWITCH iValue
			CASE 0
				RETURN cfOU_NUMPAD_POSITIONY + (cfOU_NUMPAD_OFFSETY * 2)
			BREAK
			CASE 1
			CASE 2
			CASE 3
				RETURN cfOU_NUMPAD_POSITIONY - cfOU_NUMPAD_OFFSETY
			BREAK
			CASE 4
			CASE 5
			CASE 6
				RETURN cfOU_NUMPAD_POSITIONY
			BREAK
			CASE 7
			CASE 8
			CASE 9
				RETURN cfOU_NUMPAD_POSITIONY + cfOU_NUMPAD_OFFSETY
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN cfOU_NUMPAD_POSITIONX
ENDFUNC

PROC DRAW_ORDER_UNLOCK_NUMPAD(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData)
		INT i
		
		TEXT_LABEL_23 tlElementSprite
		
		FOR i = 0 TO ciOU_NUMBER_OF_PATTERNS - 1
			IF i < sGameplayData.iCurrentPatternIndex
				// main numpad
				tlElementSprite = "Keypad_"
				tlElementSprite += sGameplayData.iKeypadNumbers[i]
			
				DRAW_HACKING_SPRITE("mporderunlock", tlElementSprite, OU_GET_NUMPAD_POS(sGameplayData.iKeypadNumbers[i], TRUE), OU_GET_NUMPAD_POS(sGameplayData.iKeypadNumbers[i], FALSE), cfOU_NUMPAD_ELEMENT_WIDTH, cfOU_NUMPAD_ELEMENT_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
			
				// tracker
				tlElementSprite = "Keypad_Feedback_"
				tlElementSprite += sGameplayData.iKeypadNumbers[i]
				
				DRAW_HACKING_SPRITE("mporderunlock", tlElementSprite, cfOU_PROGRESS_TRACKER_POSITIONX + (cfOU_PROGRESS_TRACKER_OFFSETX * i), cfOU_PROGRESS_TRACKER_POSITIONY, cfOU_PROGRESS_TRACKER_ELEMENT_WIDTH, cfOU_PROGRESS_TRACKER_ELEMENT_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
			ELIF (IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
			AND IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_CORRECT_NODE)
			AND IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_PATTERN_FINISHED)
			AND i = sGameplayData.iCurrentPatternIndex)
				IF sGameplayData.sBaseStruct.iResultCounter = 0
					tlElementSprite = "Keypad_"
					tlElementSprite += sGameplayData.iKeypadNumbers[i]
				
					DRAW_HACKING_SPRITE("mporderunlock", tlElementSprite, OU_GET_NUMPAD_POS(sGameplayData.iKeypadNumbers[i], TRUE), OU_GET_NUMPAD_POS(sGameplayData.iKeypadNumbers[i], FALSE), cfOU_NUMPAD_ELEMENT_WIDTH, cfOU_NUMPAD_ELEMENT_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
				
					// tracker
					tlElementSprite = "Keypad_Feedback_"
					tlElementSprite += sGameplayData.iKeypadNumbers[i]
					
					DRAW_HACKING_SPRITE("mporderunlock", tlElementSprite, cfOU_PROGRESS_TRACKER_POSITIONX + (cfOU_PROGRESS_TRACKER_OFFSETX * i), cfOU_PROGRESS_TRACKER_POSITIONY, cfOU_PROGRESS_TRACKER_ELEMENT_WIDTH, cfOU_PROGRESS_TRACKER_ELEMENT_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
				ENDIF
			ELIF i >= sGameplayData.sBaseStruct.iNumberOfStages
				DRAW_HACKING_SPRITE("mporderunlock", "Disabled_Digit", cfOU_PROGRESS_TRACKER_POSITIONX + (cfOU_PROGRESS_TRACKER_OFFSETX * i), cfOU_PROGRESS_TRACKER_POSITIONY, cfOU_PROGRESS_TRACKER_ELEMENT_WIDTH, cfOU_PROGRESS_TRACKER_ELEMENT_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
			ENDIF
			
			IF i = sGameplayData.iCurrentPatternIndex
				DRAW_HACKING_SPRITE("mporderunlock", "Keypad_Feedback_Box", cfOU_PROGRESS_TRACKER_POSITIONX + (cfOU_PROGRESS_TRACKER_OFFSETX * i), cfOU_PROGRESS_TRACKER_POSITIONY, cfOU_PROGRESS_TRACKER_SELECTOR_WIDTH, cfOU_PROGRESS_TRACKER_SELECTOR_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
			ENDIF
		
		ENDFOR
		
		IF sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_VISUAL_TEST
			FOR i = 0 TO 9
				// main numpad
				tlElementSprite = "Keypad_"
				tlElementSprite += i
			
				DRAW_HACKING_SPRITE("mporderunlock", tlElementSprite, OU_GET_NUMPAD_POS(i, TRUE), OU_GET_NUMPAD_POS(i, FALSE), cfOU_NUMPAD_ELEMENT_WIDTH, cfOU_NUMPAD_ELEMENT_HEIGHT, 0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA )
			ENDFOR
		ENDIF
ENDPROC

PROC DRAW_ORDER_UNLOCK_GAME(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData)

	// show normal game
	
	DRAW_ORDER_UNLOCK_GRID(sGameplayData)
	
	DRAW_ORDER_UNLOCK_NUMPAD(sGameplayData)
	
	// Warning Messages
	IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
		IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_CORRECT_NODE)
			IF IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_PATTERN_FINISHED)
				DRAW_HACKING_ANIMATED_SPRITE(sGameplayData.sBaseStruct, sGameplayData.sBaseStruct.iResultCounter, ciHG_RESULT_FRAME_COUNT, HG_ANIMATION_SLOW, "mphackinggame", "Correct_", cfHG_STATUS_MESSAGE_POSITIONX, cfHG_STATUS_MESSAGE_POSITIONY, cfHG_STATUS_MESSAGE_WIDTH, cfHG_STATUS_MESSAGE_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
			ENDIF
		ENDIF
	ENDIF
	
	
ENDPROC
// END FUNCTION

PROC OU_RESET_GAMEPLAY_DATA(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData)
	sGameplayData.iOU_Bs = 0
	sGameplayData.sBaseStruct.iCurrentLives = ciOU_MAX_LIVES
	
	sGameplayData.rgbaTintColour.iR = 255
	sGameplayData.rgbaTintColour.iG = 255
	sGameplayData.rgbaTintColour.iB = 255
	sGameplayData.rgbaTintColour.iA = 250
	
	sGameplayData.sBaseStruct.iMaxTime = ciOU_HACK_TIME
	sGameplayData.sBaseStruct.iScrambleTime = ciOU_SCRAMBLE_TIME
	
	sGameplayData.sBaseStruct.bUpdatePrompts = TRUE
	
	#IF IS_DEBUG_BUILD
		sGameplayData.sBaseStruct.fDebugTimeToSet = TO_FLOAT(ciOU_HACK_TIME/60000)
		sGameplayData.sBaseStruct.iDebugLivesToSet = ciOU_MAX_LIVES
		sGameplayData.sBaseStruct.fDebugScrambleTimeToSet = TO_FLOAT(ciOU_SCRAMBLE_TIME/1000)
		sGameplayData.sBaseStruct.iScrambleWipeChanceToSet = 0
	#ENDIF
	
	INT i
	
	FOR i = 0 TO ciOU_PATTERN_LENGTH-1
		sGameplayData.iRandomValue[i] = GET_RANDOM_INT_IN_RANGE(0, ciOU_NUMBER_OF_OPTIONS-1)
	ENDFOR
ENDPROC

PROC DRAW_ORDER_UNLOCK_PLAY_AREA(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData)
	DRAW_PLAY_AREA_COMMON(sGameplayData.sBaseStruct)
	
	// Draw Mode Specific play area here
	// Background frames
	DRAW_HACKING_SPRITE("MPOrderUnlock", "background_layout", cfHG_BG_XPOS, cfHG_BG_YPOS, cfHG_PLAYAREA_WIDTH, cfHG_PLAYAREA_HEIGHT, 0.0, sGameplayData.rgbaTintColour.iR, sGameplayData.rgbaTintColour.iG, sGameplayData.rgbaTintColour.iB, sGameplayData.rgbaTintColour.iA)
	// Draw scramble timer
	DRAW_HACKING_GAME_SCRAMBLER(sGameplayData.sBaseStruct, sGameplayData.sBaseStruct.iScrambleTime, cfOU_SCRAMBLE_POSITIONX, cfOU_SCRAMBLE_POSITIONY, cfHG_SCRAMBLE_WIDTH, cfHG_SCRAMBLE_HEIGHT, sGameplayData.rgbaTintColour, cfOU_SCRAMBLE_LEFTPOINT, cfHG_SCRAMBLE_DIFFBETWEENSEGMENT, cfHG_SCRAMBLE_ELEMENT_WIDTH, cfHG_SCRAMBLE_ELEMENT_HEIGHT, TRUE, cfHG_SCRAMBLE_Y_OFFSET, IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_SCRAMBLING))
	
	// Timer
	DRAW_HACKING_GAME_TIMER(sGameplayData.sBaseStruct, sGameplayData.sBaseStruct.iMaxTime, cfOU_TIMER_POSITIONX, cfOU_TIMER_POSITIONY, cfHG_TIMER_WIDTH, cfHG_TIMER_HEIGHT, sGameplayData.rgbaTintColour, cfOU_TIMER_DIGIT_DIFFERENCE, IS_BIT_SET(sGameplayData.iOU_Bs, ciOU_TIMER_STARTED))
	
	// Draw Lives
	DRAW_HACKING_GAME_LIVES(sGameplayData.sBaseStruct, FALSE, cfOU_LIVES_POSITIONX, cfOU_LIVES_POSITIONY, cfHG_LIVES_WIDTH, cfHG_LIVES_HEIGHT, sGameplayData.rgbaTintColour, cfOU_LIVES_OFFSET)
	
	// Draw Techoration
	DRAW_HACKING_GAME_DECOR(sGameplayData.sBaseStruct, "MPOrderUnlock_Decor", cfHG_BG_XPOS, cfHG_BG_YPOS, cfHG_TECHARATION_WIDTH, cfHG_TECHARATION_HEIGHT, sGameplayData.rgbaTintColour )
ENDPROC

PROC CLEAN_UP_ORDER_UNLOCK(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct, BOOL bFinalCleanup = FALSE, INT iType = HASH("practice"), INT iPlaythroughId = 0)
	IF NOT bFinalCleanup
		HACKING_GAME_UPDATE_TELEMETRY(sControllerStruct, sGameplayData.sBaseStruct, iType, iPlaythroughId, HASH("order unlock"), sGameplayData.iCurrentPatternIndex+1)
	ENDIF
	
	HACKING_GAME_CLOSE_LOOPING_SOUNDS(sGameplayData.sBaseStruct, "DLC_H3_Door_Hack_Scene")
	
	HACKING_COMMON_CLEANUP(sControllerStruct, sGameplayData.sBaseStruct, bFinalCleanup)
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPOrderUnlock")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPOrderUnlock_Decor")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPOrderUnlock_Decor1")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_HEIST3/Door_Hacking")
ENDPROC

PROC PROCESS_ORDER_UNLOCK_EVERY_FRAME_MISC(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	PROCESS_COMMON_HACKING_EVERY_FRAME_MISC(sGameplayData.sBaseStruct, sControllerStruct)
	
	HACKING_GAME_HANDLE_LOOPING_SOUNDS(sGameplayData.sBaseStruct, "DLC_H3_Door_Hack_Scene")
ENDPROC

// PROCESS STATE FUNCTIONS
PROC PROCESS_ORDER_UNLOCK_INIT(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	IF IS_BIT_SET(sControllerStruct.iBS, ciHGBS_FAILED) // Don't re-enter Init when the game is cleaning up
		EXIT
	ENDIF
	
	ORDER_UNLOCK_GAMEPLAY_DATA sEmpty
	 
	COPY_SCRIPT_STRUCT(sGameplayData, sEmpty, SIZE_OF(ORDER_UNLOCK_GAMEPLAY_DATA))  
	
	// setup common data
	IF NOT PROCESS_HACKING_GAME_INIT(sControllerStruct, sGameplayData.sBaseStruct)
		EXIT
	ENDIF
	
	OU_RESET_GAMEPLAY_DATA(sGameplayData)
	// Load Common textures for fingerprint
	TEXT_LABEL_23 tl23 = "MPOrderUnlock"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][OrderUnlock] Not Loaded ", tl23)
		EXIT
	ENDIF

	
	tl23 = "MPOrderUnlock_Decor"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][OrderUnlock] Not Loaded ", tl23)
		EXIT
	ENDIF
	
	tl23 = "MPOrderUnlock_Decor1"
	REQUEST_STREAMED_TEXTURE_DICT(tl23)
	IF NOT HAS_STREAMED_TEXTURE_DICT_LOADED(tl23)
		PRINTLN("[MC][OrderUnlock] Not Loaded ", tl23)
		EXIT
	ENDIF
	
	// Audio
	sGameplayData.sBaseStruct.sAudioSet = "DLC_H3_Cas_Door_Minigame_Sounds"
	
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_HEIST3/Door_Hacking")
		PRINTLN("[MC][OrderUnlock] Not Loaded DLC_HEIST3/Door_Hacking")
		EXIT
	ENDIF
	
	UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_LOAD_LEVEL)
ENDPROC

PROC ORDER_UNLOCK_SETUP_HACKER_DETAILS(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData)
	CASINO_HEIST_HACKERS eHacker = GET_PLAYER_CASINO_HEIST_HACKER(GB_GET_LOCAL_PLAYER_GANG_BOSS())
	
	IF NOT NETWORK_IS_ACTIVITY_SESSION()
		eHacker = CASINO_HEIST_HACKER__AVI_SCHWARTZMAN
		PRINTLN("[MC][OrderUnlock] Hacker for practise session = Seting to Avi  ")
	ENDIF
	
	SWITCH eHacker
		CASE CASINO_HEIST_HACKER__NONE
		CASE CASINO_HEIST_HACKER__MAX
			sGameplayData.sBaseStruct.iMaxTime = ciOU_HACK_TIME
			sGameplayData.sBaseStruct.iScrambleTime = ciOU_SCRAMBLE_TIME
			
			sGameplayData.sBaseStruct.iMaxLives = ciOU_MAX_LIVES
			sGameplayData.sBaseStruct.iCurrentLives = ciOU_MAX_LIVES
			
			sGameplayData.sBaseStruct.iScrambleWipeChance = 0
			PRINTLN("[MC][OrderUnlock] Setting Hacker to DEFAULT")
		BREAK
		CASE CASINO_HEIST_HACKER__RICKIE_LUKENS
			sGameplayData.sBaseStruct.iMaxTime = 120000
			sGameplayData.sBaseStruct.iScrambleTime = 40000

			sGameplayData.sBaseStruct.iMaxLives = 3
			sGameplayData.sBaseStruct.iCurrentLives = 3
			
			sGameplayData.sBaseStruct.iScrambleWipeChance = 40
			PRINTLN("[MC][OrderUnlock] Setting Hacker to Rickie")
		BREAK
		CASE CASINO_HEIST_HACKER__CHRISTIAN_FELTZ
			sGameplayData.sBaseStruct.iMaxTime = 180000
			sGameplayData.sBaseStruct.iScrambleTime = 70000
			
			sGameplayData.sBaseStruct.iMaxLives = 4
			sGameplayData.sBaseStruct.iCurrentLives = 4
			
			sGameplayData.sBaseStruct.iScrambleWipeChance = 25
			PRINTLN("[MC][OrderUnlock] Setting Hacker to Christian")
		BREAK
		CASE CASINO_HEIST_HACKER__YOHAN
			sGameplayData.sBaseStruct.iMaxTime = 180000
			sGameplayData.sBaseStruct.iScrambleTime = 60000
			
			sGameplayData.sBaseStruct.iMaxLives = 5
			sGameplayData.sBaseStruct.iCurrentLives = 5
			
			sGameplayData.sBaseStruct.iScrambleWipeChance = 20
			PRINTLN("[MC][OrderUnlock] Setting Hacker to Yohan")
		BREAK
		CASE CASINO_HEIST_HACKER__AVI_SCHWARTZMAN
			sGameplayData.sBaseStruct.iMaxTime = 240000
			sGameplayData.sBaseStruct.iScrambleTime = 110000
			
			sGameplayData.sBaseStruct.iMaxLives = 6
			sGameplayData.sBaseStruct.iCurrentLives = 6
			
			sGameplayData.sBaseStruct.iScrambleWipeChance = 5
			PRINTLN("[MC][OrderUnlock] Setting Hacker to Avi")
		BREAK
		CASE CASINO_HEIST_HACKER__PAIGE_HARRIS
			sGameplayData.sBaseStruct.iMaxTime = 240000
			sGameplayData.sBaseStruct.iScrambleTime = 100000
			
			sGameplayData.sBaseStruct.iMaxLives = 6
			sGameplayData.sBaseStruct.iCurrentLives = 6
			
			sGameplayData.sBaseStruct.iScrambleWipeChance = 5
			PRINTLN("[MC][OrderUnlock] Setting Hacker to Paige")
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_ORDER_UNLOCK_LOAD_LEVEL(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct, INT iNumberOfStages)
	HACKING_GAME_SET_NUMBER_OF_PATTERNS(sGameplayData.sBaseStruct, iNumberOfStages)

	PROCESS_HACKING_GAME_LOAD_LEVEL(sControllerStruct)
	
	PLAN_PATTERNS_FOR_ORDER_UNLOCK(sGameplayData)
	
	INT i
	FOR i = 0 TO ciOU_NUMBER_OF_PATTERNS -1
		sGameplayData.iKeypadNumbers[i] = GET_RANDOM_INT_IN_RANGE(0, 9)
	ENDFOR
	
	ORDER_UNLOCK_SETUP_HACKER_DETAILS(sGameplayData)
	
	// Start animation time
	REINIT_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
	REINIT_NET_TIMER(sGameplayData.sBaseStruct.tdTimer)
	
	LOAD_BINK_MOVIE(sGameplayData.sBaseStruct, "Intro_OU")
	
	UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_STARTUP)
ENDPROC

PROC PROCESS_ORDER_UNLOCK_STARTUP(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	// Run the startup process
	PROCESS_HACKING_GAME_STARTUP(sControllerStruct, sGameplayData.sBaseStruct)
	
	IF MAINTAIN_BINK_MOVIE(sGameplayData.sBaseStruct)
	OR HAS_NET_TIMER_EXPIRED(sGameplayData.sBaseStruct.tdAnimationTimer, ciOU_STARTUP_TIMER)
		RELEASE_BINK_MOVIE(sGameplayData.sBaseStruct.movieId)
		UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_PLAY)
		SET_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_NEXT_PATTERN)
		SET_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_RANDOM_PATTERN)
		RESET_NET_TIMER(sGameplayData.tdRandomiseTimer)
		RESET_NET_TIMER(sGameplayData.sBaseStruct.tdAnimationTimer)
		REINIT_NET_TIMER(sGameplayData.sBaseStruct.tdTimer)
		RESET_NET_TIMER(sGameplayData.tdStartHelpTimer)
		SET_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_STARTING_HELP)
		//UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_VISUAL_TEST)
	ENDIF
ENDPROC

PROC PROCESS_ORDER_UNLOCK_PLAY(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct, INT iNumberOfPlays, BOOL bShowFinalQuitWarning)
	BOOL bCanInteract = OU_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
	
	IF NOT IS_BIT_SET(sGameplayData.sBaseStruct.iHGSoundBitset, ciHG_BS_SOUND_PLAYSTART)
		PLAY_SOUND_FRONTEND(-1, "Play_Start", sGameplayData.sBaseStruct.sAudioSet)
		SET_BIT(sGameplayData.sBaseStruct.iHGSoundBitset, ciHG_BS_SOUND_PLAYSTART)
	ENDIF
	
	PROCESS_HACKING_GAME_PLAY(sControllerStruct, sGameplayData.sBaseStruct, iNumberOfPlays, bCanInteract)
	
	IF bCanInteract
		PROCESS_ORDER_UNLOCK_INPUT(sGameplayData)
	ENDIF
	
	PROCESS_ORDER_UNLOCK_GAME(sGameplayData, sControllerStruct, bShowFinalQuitWarning)
	
	DRAW_ORDER_UNLOCK_GAME(sGameplayData)
	
	MAINTAIN_ORDER_UNLOCK_CONTEXT(sGameplayData)
ENDPROC

PROC PROCESS_ORDER_UNLOCK_PASS(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	PROCESS_HACKING_GAME_PASS(sControllerStruct, sGameplayData.sBaseStruct, ciOU_END_DELAY, "Success_OU")

ENDPROC

PROC PROCESS_ORDER_UNLOCK_FAIL(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	PROCESS_HACKING_GAME_FAIL(sControllerStruct, sGameplayData.sBaseStruct, ciOU_END_DELAY, "Fail_OU")

ENDPROC

PROC PROCESS_ORDER_UNLOCK_VISUAL_TEST(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct)
	PROCESS_HACKING_GAME_VISUAL_TEST(sControllerStruct, sGameplayData.sBaseStruct)
	
	IF OU_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
		PROCESS_ORDER_UNLOCK_INPUT(sGameplayData)
	ENDIF
	
	DRAW_ORDER_UNLOCK_GAME(sGameplayData)
ENDPROC

#IF IS_DEBUG_BUILD
PROC PROCESS_ORDER_UNLOCK_WIDGETS(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData)
	IF NOT sGameplayData.sBaseStruct.bDebugCreatedWidgets
		PRINTLN("[MC][OrderUnlockDebug] - Widgets not made ")
		
		
		sGameplayData.sBaseStruct.widgetFingerprintClone = GET_ID_OF_TOP_LEVEL_WIDGET_GROUP()
		SET_CURRENT_WIDGET_GROUP(sGameplayData.sBaseStruct.widgetFingerprintClone)
		START_WIDGET_GROUP("OrderUnlock")
			
			// Game Values
			ADD_WIDGET_STRING("Game Values")
			
			ADD_WIDGET_INT_SLIDER("Lives:", sGameplayData.sBaseStruct.iDebugLivesToSet, 1, 6, 1 )
			
			ADD_WIDGET_FLOAT_SLIDER("Time:", sGameplayData.sBaseStruct.fDebugTimeToSet, 1.0, 10.0, 0.5 )
			ADD_WIDGET_FLOAT_SLIDER("Scramble Time:", sGameplayData.sBaseStruct.fDebugScrambleTimeToSet, 10.0, 500.0, 1.0 )
			ADD_WIDGET_INT_SLIDER("Scramble Wipe Chance:", sGameplayData.sBaseStruct.iScrambleWipeChanceToSet, 0, 100, 1 )
			
			ADD_WIDGET_BOOL("RESET GAME", sGameplayData.sBaseStruct.bDebugReset)
			// Positioning Values
//			ADD_WIDGET_STRING("Debug Values")
//			ADD_WIDGET_FLOAT_SLIDER("Background X", cfHG_BG_XPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Background Y", cfHG_BG_YPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Background Width", cfHG_BG_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Background Height", cfHG_BG_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Play Area Width", cfHG_PLAYAREA_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Play Area Height", cfHG_PLAYAREA_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Techaration Width", cfHG_TECHARATION_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Techaration Height", cfHG_TECHARATION_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Tech 1 X", cfHG_tech1_XPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 1 Y", cfHG_tech1_YPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 1 Width", cfHG_tech1_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 1 Height", cfHG_tech1_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Tech 2 X", cfHG_tech2_XPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 2 Y", cfHG_tech2_YPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 2 Width", cfHG_tech2_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 2 Height", cfHG_tech2_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Tech 3 X", cfHG_tech3_XPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 3 Y", cfHG_tech3_YPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 3 Width", cfHG_tech3_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 3 Height", cfHG_tech3_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Tech 4 X", cfHG_tech4_XPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 4 Y", cfHG_tech4_YPOS, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 4 Width", cfHG_tech4_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Tech 4 Height", cfHG_tech4_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintColumn 1", cfOU_FINGERPRINT_ELEMENT_COLUMN1, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintColumn 2", cfOU_FINGERPRINT_ELEMENT_COLUMN2, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintRow 1", cfOU_FINGERPRINT_ELEMENT_ROW1, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintRow 2", cfOU_FINGERPRINT_ELEMENT_ROW2, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintRow 3", cfOU_FINGERPRINT_ELEMENT_ROW3, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintRow 4", cfOU_FINGERPRINT_ELEMENT_ROW4, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintElement Width", cfOU_FINGERPRINT_ELEMENT_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintElement Height", cfOU_FINGERPRINT_ELEMENT_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintLarge X", cfOU_FINGERPRINT_LARGE_POSITIONX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintLarge Y", cfOU_FINGERPRINT_LARGE_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintLarge Width", cfOU_FINGERPRINT_LARGE_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("FingerPrintLarge Height", cfOU_FINGERPRINT_LARGE_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Grid Details Left X", cfOU_GRID_DETAILS_LEFTX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Details Right X", cfOU_GRID_DETAILS_RIGHTX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Details Width", cfOU_GRID_DETAILS_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Details Height", cfOU_GRID_DETAILS_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Grid Noise X", cfOU_GRID_NOISE_POSITIONX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Noise Y", cfOU_GRID_NOISE_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Noise Width", cfOU_GRID_NOISE_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Grid Noise Height", cfOU_GRID_NOISE_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("StatusMessage X", cfHG_STATUS_MESSAGE_POSITIONX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("StatusMessage Y", cfHG_STATUS_MESSAGE_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("StatusMessage Width", cfHG_STATUS_MESSAGE_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("StatusMessage Height", cfHG_STATUS_MESSAGE_HEIGHT, 0.0, 2000.0, 10.0 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Results Loading Initial X", cfHG_RESULTS_LOADING_INITIALX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Results Loading Diff X", cfHG_RESULTS_LOADING_XDIFF, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Results Loading Y", cfHG_RESULTS_LOADING_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Results Loading Height", cfHG_RESULTS_LOADING_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Results Loading Width", cfHG_RESULTS_LOADING_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_INT_SLIDER("Results Loading Max Segments", ciHG_RESULTS_LOADING_MAX_SEGMENTS, 0, 100, 1)
//			
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker Width", cfOU_FINGERPRINT_TRACKER_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker Height", cfOU_FINGERPRINT_TRACKER_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker Width", cfOU_FINGERPRINT_TRACKER_SELECTOR_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker Height", cfOU_FINGERPRINT_TRACKER_SELECTOR_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker Y", cfOU_FINGERPRINT_TRACKER_POSITIONY, 0.0, 2000.0, 0.01 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker1 X", cfOU_FINGERPRINT_TRACKER1_POSITIONX, 0.0, 2000.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker2 X", cfOU_FINGERPRINT_TRACKER2_POSITIONX, 0.0, 2000.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker3 X", cfOU_FINGERPRINT_TRACKER3_POSITIONX, 0.0, 2000.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Fingerprint Tracker4 X", cfOU_FINGERPRINT_TRACKER4_POSITIONX, 0.0, 2000.0, 0.01 )
			
			ADD_WIDGET_FLOAT_SLIDER("Timer X", cfOU_TIMER_POSITIONX, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("Timer Y", cfOU_TIMER_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Timer Width", cfHG_TIMER_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Timer Height", cfHG_TIMER_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Timer Digit Difference", cfOU_TIMER_DIGIT_DIFFERENCE, 0.0, 2000.0, 0.01 )
//			
			ADD_WIDGET_FLOAT_SLIDER("Lives X", cfOU_LIVES_POSITIONX, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("Lives Y", cfOU_LIVES_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Lives Width", cfHG_LIVES_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Lives Height", cfHG_LIVES_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Lives Offset", cfOU_LIVES_OFFSET, 0.0, 2000.0, 0.01 )
//			
//			ADD_WIDGET_FLOAT_SLIDER("Circular Decoration X", cfOU_CIRCLEDECORATION_POSITIONX, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Circular Decoration Y", cfOU_CIRCLEDECORATION_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Circular Decoration Width", cfOU_CIRCLEDECORATION_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Circular Decoration Height", cfOU_CIRCLEDECORATION_HEIGHT, 0.0, 2000.0, 10.0 )
//			
			ADD_WIDGET_FLOAT_SLIDER("Scramble X", cfOU_SCRAMBLE_POSITIONX, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("Scramble Y", cfOU_SCRAMBLE_POSITIONY, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Scramble BG Width", cfHG_SCRAMBLE_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Scramble BG Height", cfHG_SCRAMBLE_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Scramble Element Width", cfHG_SCRAMBLE_ELEMENT_WIDTH, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Scramble Element Height", cfHG_SCRAMBLE_ELEMENT_HEIGHT, 0.0, 2000.0, 10.0 )
//			ADD_WIDGET_FLOAT_SLIDER("Scramble Left Most", cfOU_SCRAMBLE_LEFTPOINT, 0.0, 1.0, 0.01 )
//			ADD_WIDGET_FLOAT_SLIDER("Scramble Difference", cfHG_SCRAMBLE_DIFFBETWEENSEGMENT, 0.0, 2000.0, 0.01 )
			
//			ADD_WIDGET_FLOAT_SLIDER("Default Anim Speed", cfHG_DEFAULT_ANIM_FRAME_TIME, 0.0, 500.0, 10.00 )
//			ADD_WIDGET_FLOAT_SLIDER("Slow Anim Speed", cfHG_SLOW_ANIM_FRAME_TIME, 0.0, 500.0, 10.00 )
//			ADD_WIDGET_FLOAT_SLIDER("Fast Anim Speed", cfHG_FAST_ANIM_FRAME_TIME, 0.0, 500.0, 10.00 )
			
//			ADD_WIDGET_BOOL("sBeamHackDebug.bDebugDrawDebugLines", sBeamHackDebug.bDebugDrawDebugLines)
//			ADD_WIDGET_BOOL("sBeamHackDebug.bDebugDrawGrid", sBeamHackDebug.bDebugDrawGrid)

			ADD_WIDGET_FLOAT_SLIDER("Numpad Position X", cfOU_NUMPAD_POSITIONX, 0.0, 1.0, 0.001 )
			ADD_WIDGET_FLOAT_SLIDER("Numpad Position Y", cfOU_NUMPAD_POSITIONY, 0.0, 1.0, 0.001 )
			ADD_WIDGET_FLOAT_SLIDER("Numpad Position Offset X", cfOU_NUMPAD_OFFSETX, 0.0, 1.0, 0.001 )
			ADD_WIDGET_FLOAT_SLIDER("Numpad Position Offset Y", cfOU_NUMPAD_OFFSETY, 0.0, 1.0, 0.001 )
			
			ADD_WIDGET_FLOAT_SLIDER("Tracker Position X", cfOU_PROGRESS_TRACKER_POSITIONX, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("Tracker Position Y", cfOU_PROGRESS_TRACKER_POSITIONY, 0.0, 1.0, 0.01 )
			ADD_WIDGET_FLOAT_SLIDER("Tracker Offset", cfOU_PROGRESS_TRACKER_OFFSETX, 0.0, 1.0, 0.01 )
			
			ADD_WIDGET_INT_SLIDER("Last Selected Time", ciOU_SELECT_TIME, 0, 2000, 100)
			ADD_WIDGET_INT_SLIDER("Clear Frame", ciOU_CLEAR_FRAME, 0, 1, 1)
			
		STOP_WIDGET_GROUP()
		CLEAR_CURRENT_WIDGET_GROUP(sGameplayData.sBaseStruct.widgetFingerprintClone)
		sGameplayData.sBaseStruct.bDebugCreatedWidgets = TRUE
	ENDIF
ENDPROC

/// DEBUG
PROC PROCESS_ORDER_UNLOCK_DEBUG(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData)
	
	IF NOT sGameplayData.sBaseStruct.bDebugCreatedWidgets
		PROCESS_ORDER_UNLOCK_WIDGETS(sGameplayData)
	ENDIF
	
	IF NOT OU_IS_PLAYER_ABLE_TO_INTERACT(sGameplayData)
		DRAW_DEBUG_TEXT_2D("INTERACTION LOCKED", NULL_VECTOR(), 255, 0, 0)
	ENDIF
	
	IF sGameplayData.sBaseStruct.bDebugReset
		sGameplayData.sBaseStruct.iMaxTime = FLOOR(sGameplayData.sBaseStruct.fDebugTimeToSet * 60000)
		sGameplayData.sBaseStruct.iScrambleTime = FLOOR(sGameplayData.sBaseStruct.fDebugScrambleTimeToSet * 1000)
		sGameplayData.sBaseStruct.iCurrentLives = sGameplayData.sBaseStruct.iDebugLivesToSet
		RESET_NET_TIMER(sGameplayData.sBaseStruct.tdTimer)
		sGameplayData.sBaseStruct.iScrambleWipeChance = sGameplayData.sBaseStruct.iScrambleWipeChanceToSet
		sGameplayData.sBaseStruct.bDebugReset = FALSE
	ENDIF
	
	IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_SHIFT, "")
		PRINTLN("[MC][OrderUnlock] - PROCESS_ORDER_UNLOCK_DEBUG - J SKIP - Going to HACKING_GAME_PASS")
		UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_PASS)
	ENDIF
	
	IF sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_PLAY
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_CTRL_SHIFT, "")
			PRINTLN("[MC][OrderUnlock] - PROCESS_ORDER_UNLOCK_DEBUG - J SKIP - Going to HACKING_GAME_VISUAL_TEST")
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_FAILED_NODE)
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_CORRECT_NODE)
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_SCRAMBLING)
			UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_VISUAL_TEST)
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_CTRL_SHIFT, "")
			sGameplayData.sBaseStruct.bDebugShowGuide = !sGameplayData.sBaseStruct.bDebugShowGuide
		ENDIF
	
	ELIF sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_VISUAL_TEST
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_CTRL_SHIFT, "")
			PRINTLN("[MC][OrderUnlock] - PROCESS_ORDER_UNLOCK_DEBUG - J SKIP - Going to HACKING_GAME_PLAY")
			//sGameplayData.iCurrentFingerprintIndex = 0
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_FAILED_NODE)
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_CORRECT_NODE)
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_SCRAMBLING)
			UPDATE_HACKING_GAME_STATE(sGameplayData.sBaseStruct, HACKING_GAME_PLAY)
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_CTRL, "")
			SET_BIT(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
			SET_BIT(sGameplayData.iOU_Bs, ciOU_CORRECT_NODE)
		ENDIF

		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_SHIFT, "")
			SET_BIT(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
			SET_BIT(sGameplayData.iOU_Bs, ciOU_FAILED_NODE)
		ENDIF
		
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_ALT, "")
			SET_BIT(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
			SET_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_LOADING)
		ENDIF

		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_V, KEYBOARD_MODIFIER_CTRL_SHIFT, "")
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_CHECKING_MATCH)
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_FAILED_NODE)
			CLEAR_BIT(sGameplayData.iOU_Bs, ciOU_CORRECT_NODE)
			SET_BIT(sGameplayData.iOU_Bs, ciOU_SHOWING_LOADING)
		ENDIF
	ENDIF
ENDPROC
#ENDIF
///

PROC PROCESS_ORDER_UNLOCK_MINIGAME(ORDER_UNLOCK_GAMEPLAY_DATA &sGameplayData, HG_CONTROL_STRUCT &sControllerStruct, INT iNumberOfStages = 3, INT iNumberOfPlays = -1, BOOL bShowFinalQuitWarning = FALSE)
	IF sGameplayData.sBaseStruct.eHackingGameState > HACKING_GAME_SLEEP
	AND sGameplayData.sBaseStruct.eHackingGameState < HACKING_GAME_CLEANUP
	OR sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_VISUAL_TEST
		#IF IS_DEBUG_BUILD
		PRINTLN("[MC][OrderUnlock] Running Process Order Unlock - ", sGameplayData.sBaseStruct.eHackingGameState)
		IF sGameplayData.sBaseStruct.eHackingGameState > HACKING_GAME_LOAD_LEVEL
			PROCESS_ORDER_UNLOCK_DEBUG(sGameplayData)
		ENDIF
		#ENDIF
		IF sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_CLEANUP
			PROCESS_ORDER_UNLOCK_EVERY_FRAME_MISC(sGameplayData, sControllerStruct)
		ENDIF
		IF NOT g_bHideHackingVisuals
		AND sGameplayData.sBaseStruct.eHackingGameState > HACKING_GAME_LOAD_LEVEL
		AND sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_CLEANUP
			DRAW_ORDER_UNLOCK_PLAY_AREA(sGameplayData)
		ENDIF
	ENDIF
	
	SWITCH sGameplayData.sBaseStruct.eHackingGameState
		CASE HACKING_GAME_SLEEP
			PROCESS_HACKING_GAME_SLEEP(sGameplayData.sBaseStruct, sControllerStruct)
		BREAK
		CASE HACKING_GAME_INIT
			PROCESS_ORDER_UNLOCK_INIT(sGameplayData, sControllerStruct)
		BREAK
		CASE HACKING_GAME_LOAD_LEVEL
			PROCESS_ORDER_UNLOCK_LOAD_LEVEL(sGameplayData, sControllerStruct, iNumberOfStages)
		BREAK
		CASE HACKING_GAME_STARTUP
			PROCESS_ORDER_UNLOCK_STARTUP(sGameplayData, sControllerStruct)
		BREAK
		CASE HACKING_GAME_PLAY
			PROCESS_ORDER_UNLOCK_PLAY(sGameplayData, sControllerStruct, iNumberOfPlays, bShowFinalQuitWarning)
		BREAK
		CASE HACKING_GAME_PASS
			PROCESS_ORDER_UNLOCK_PASS(sGameplayData, sControllerStruct)
		BREAK
		CASE HACKING_GAME_FAIL
			PROCESS_ORDER_UNLOCK_FAIL(sGameplayData, sControllerStruct)
		BREAK
		CASE HACKING_GAME_CLEANUP
			CLEAN_UP_ORDER_UNLOCK(sGameplayData, sControllerStruct)
		BREAK
		#IF IS_DEBUG_BUILD
		CASE HACKING_GAME_VISUAL_TEST
			PROCESS_ORDER_UNLOCK_VISUAL_TEST(sGameplayData, sControllerStruct)
		BREAK
		#ENDIF
		
	ENDSWITCH
	
	IF NOT g_bHideHackingVisuals
	AND sGameplayData.sBaseStruct.eHackingGameState > HACKING_GAME_LOAD_LEVEL
	AND sGameplayData.sBaseStruct.eHackingGameState != HACKING_GAME_CLEANUP
	OR sGameplayData.sBaseStruct.eHackingGameState = HACKING_GAME_VISUAL_TEST
		DRAW_PLAY_AREA_OVERLAY(sGameplayData.sBaseStruct)
	ENDIF
ENDPROC
