
//_________________________________________________________________________ \\
//_________________________________________________________________________ \\
// 																			\\
// 	Script: 																\\
//	-------																	\\
//	net_save_create_eom_vehicle.sch											\\
// 																			\\
// 	Purpose: 																\\
//	--------																\\
//	Runs system for saving an ambient vehicle and 							\\
// 	then recreating with you and your buddies inside it.					\\
// 																			\\
// 	Author: 																\\
// 	-------																	\\
//	William.Kennedy@RockstarNorth.com										\\
// 																			\\
//_________________________________________________________________________ \\
//_________________________________________________________________________ \\


// ------------
// USING FILES
// ------------

USING "globals.sch"
USING "net_include.sch"
USING "commands_zone.sch"
USING "commands_network.sch"
USING "net_spawn_vehicle.sch"
USING "net_spawn_private.sch"
USING "net_ambience.sch"
USING "net_spawn.sch"

// -------
// CONSTS
// -------



// ----------
// FUNCTIONS
// ----------

#IF IS_DEBUG_BUILD
PROC PRINT_VEHICLE_SEAT(VEHICLE_SEAT eSeat)
	SWITCH eSeat
		CASE VS_ANY_PASSENGER	PRINTLN("VS_ANY_PASSENGER")	BREAK
		CASE VS_DRIVER			PRINTLN("VS_DRIVER")			BREAK
		CASE VS_FRONT_RIGHT		PRINTLN("VS_FRONT_RIGHT") 	BREAK
		CASE VS_BACK_LEFT		PRINTLN("VS_BACK_LEFT")		BREAK
		CASE VS_BACK_RIGHT		PRINTLN("VS_BACK_RIGHT")		BREAK
		CASE VS_EXTRA_LEFT_1	PRINTLN("VS_EXTRA_LEFT_1") 	BREAK
		CASE VS_EXTRA_RIGHT_1	PRINTLN("VS_EXTRA_RIGHT_1") 	BREAK
		CASE VS_EXTRA_LEFT_2	PRINTLN("VS_EXTRA_LEFT_2")	BREAK
		CASE VS_EXTRA_RIGHT_2	PRINTLN("VS_EXTRA_RIGHT_2") 	BREAK
		CASE VS_EXTRA_LEFT_3	PRINTLN("VS_EXTRA_LEFT_3")	BREAK
		CASE VS_EXTRA_RIGHT_3	PRINTLN("VS_EXTRA_RIGHT_3")	BREAK
	ENDSWITCH
ENDPROC
#ENDIF

PROC SET_MY_SAVED_EOM_VEHICLE_AS_NO_LONGER_NEEDED()
	IF NOT (PERSONAL_VEHICLE_NET_ID() = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sEomAmbientVehicleData.niVeh)
		CLEANUP_NET_ID(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sEomAmbientVehicleData.niVeh)
	ELSE
		PRINTLN("SET_MY_SAVED_EOM_VEHICLE_AS_NO_LONGER_NEEDED - saved eom is same as personal vehicle, so just setting to NULL instead.")
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sEomAmbientVehicleData.niVeh = NULL
	ENDIF
ENDPROC

PROC RESET_CREATE_AND_ENTER_EOM_AMBIENT_VEHICLE_DATA(BOOL bSetNoLongerNeeded = FALSE)
	
	PRINTLN("[Save_Create_Vehicle] - RESET_CREATE_AND_ENTER_EOM_AMBIENT_VEHICLE_DATA() been called:")
	
	GAMER_HANDLE tempGh
	VEHICLE_SETUP_STRUCT_MP sVehSetupDataTemp
	
	IF bSetNoLongerNeeded
		SET_MY_SAVED_EOM_VEHICLE_AS_NO_LONGER_NEEDED()
	ENDIF
	
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iMainStage									= 0
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.s_vehicleSetupStruct							= sVehSetupDataTemp
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos										= << 0.0, 0.0, 0.0 >>
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.fVehRot										= 0.0
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bFinishedInPersonalVehicle					= FALSE
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bFinishedInSomeoneElsesPersonalVehicle		= FALSE
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bSavedValidVehicle							= FALSE
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bsavedVehIsOverride							= FALSE
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eVsMySeat									= VS_ANY_PASSENGER
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver									= tempGh
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhFrontRight								= tempGh
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackRight									= tempGh
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackLeft									= tempGh
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft1								= tempGh
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight1								= tempGh
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft2								= tempGh
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight2								= tempGh
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft3								= tempGh
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight3								= tempGh
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iCreateStage									= 0
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh										= NULL
	RESET_NET_TIMER(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eTimer)
	RESET_NET_TIMER(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eFailSafeTimer)
	RESET_NET_TIMER(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eFindSeatTimer)
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eResult										= eCREATEANDENTERAMBIENTVEHICLESTATUS_NO_RESULT
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sEomAmbientVehicleData.niVeh							= NULL
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bIAmDriverForContactMission 					= FALSE
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iSecondaryR									= 0
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iSecondaryG									= 0
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iSecondaryB									= 0
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sEomAmbientVehicleData.bIAmCoronaHosThatHasAborted 	= FALSE
	
ENDPROC



FUNC BOOL IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED(VEHICLE_INDEX vehId)
	
	MODEL_NAMES eModel = GET_ENTITY_MODEL(vehId)
	
	IF IS_THIS_MODEL_A_BOAT(eModel)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a boat.")
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_MODEL_A_HELI(eModel)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a heli.")
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_MODEL_A_PLANE(eModel)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a plane.")
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_MODEL_A_TRAIN(eModel)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a train.")
		RETURN FALSE
	ENDIF
	
	IF IS_MP_PREMIUM_VEHICLE(eModel)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a premium model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = SUBMERSIBLE)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a submersible model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = APC)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is an APC model.")
		RETURN FALSE
	ENDIF
	
	IF eModel = INT_TO_ENUM(MODEL_NAMES, HASH("Insurgent")) 
	OR eModel = INT_TO_ENUM(MODEL_NAMES, HASH("Insurgent2"))
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is an Insurgent model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = TECHNICAL)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a techinical truck model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = BENSON)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a benson model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = MESA)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a mesa model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = MESA2)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a mesa2 model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = MESA3)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a mesa3 model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = MAVERICK)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a maverick model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = ROMERO)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a Romero model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = CADDY2)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a CADDY2 model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = CADDY3)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a CADDY3 model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = OPPRESSOR)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a OPPRESSOR model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = GRANGER)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a GRANGER model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = PATRIOT)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a PATRIOT model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = TAMPA)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a TAMPA truck model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = TAMPA2)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a TAMPA2 truck model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = TAMPA3)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a TAMPA3 truck model.")
		RETURN FALSE
	ENDIF
	
	IF (eModel = fbi)
		PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a fbi truck model.")
		RETURN FALSE
	ENDIF
	
		IF eModel = INT_TO_ENUM(MODEL_NAMES, HASH("dune4")) 
			PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a dune4 model.")
			RETURN FALSE
		ENDIF
		
		IF eModel = INT_TO_ENUM(MODEL_NAMES, HASH("phantom2")) 
			PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a phantom2 model.")
			RETURN FALSE
		ENDIF
		
		IF eModel = INT_TO_ENUM(MODEL_NAMES, HASH("technical2")) 
			PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a technical2 model.")
			RETURN FALSE
		ENDIF
		
		IF eModel = INT_TO_ENUM(MODEL_NAMES, HASH("blazer5")) 
			PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a blazer5 model.")
			RETURN FALSE
		ENDIF
		
		IF eModel = INT_TO_ENUM(MODEL_NAMES, HASH("boxville5")) 
			PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a boxville5 model.")
			RETURN FALSE
		ENDIF
		
		IF eModel = INT_TO_ENUM(MODEL_NAMES, HASH("wastelander")) 
			PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a wastelander model.")
			RETURN FALSE
		ENDIF
		
		IF eModel = INT_TO_ENUM(MODEL_NAMES, HASH("ruiner2")) 
			PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a ruiner2 model.")
			RETURN FALSE
		ENDIF
		
		IF eModel = INT_TO_ENUM(MODEL_NAMES, HASH("voltic2")) 
			PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a voltic2 model.")
			RETURN FALSE
		ENDIF
	
		IF eModel = INT_TO_ENUM(MODEL_NAMES, HASH("deluxo"))
			PRINTLN("[Save_Create_Vehicle] - IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED - vehicle model is a deluxo model.")
			RETURN FALSE
		ENDIF
	
//	IF DECOR_IS_REGISTERED_AS_TYPE("CreatedByPegasus", DECOR_TYPE_INT)
//		IF DECOR_EXIST_ON(vehId, "CreatedByPegasus")
//			IF DECOR_SET_BOOL(vehId, "CreatedByPegasus", TRUE)
//				RETURN FALSE
//			ENDIF
//		ENDIF
//	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC SAVE_MY_EOM_VEHICLE_DATA(VEHICLE_INDEX veh = NULL, BOOL bOverrideVeh = FALSE, BOOL bDoDataClean = TRUE, INT iCall = -1)
	
	BOOL bSaveVehicleModelData = TRUE
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)
	AND GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) != GET_HASH_KEY("carmod_shop")
	AND GET_HASH_KEY(GET_THIS_SCRIPT_NAME()) != GET_HASH_KEY("personal_carmod_shop")
		PRINTLN("[Save_Create_Vehicle] - player is browsing in mod shop.")
		EXIT
	ENDIF
	
	INT i
	PED_INDEX pedId
	PLAYER_INDEX playerId	
	VEHICLE_INDEX vehId
	
	// Stop compiler moaning. 
	IF iCall = iCall
		iCall = iCall
	ENDIF
	
	PRINTLN("[Save_Create_Vehicle] - SAVE_MY_EOM_VEHICLE_DATA iCall = ", iCall)
	
	// Reset the data in preperation.
	IF bDoDataClean
		RESET_CREATE_AND_ENTER_EOM_AMBIENT_VEHICLE_DATA()
	ENDIF
	
	// If I exist and I'm alive I can be in a vehicle.
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			
			// Print debug info used to display data used in checks below.
			#IF IS_DEBUG_BUILD
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					PRINTLN("[Save_Create_Vehicle] - local player is in a vehicle.")
				ELSE
					PRINTLN("[Save_Create_Vehicle] - local player is not in a vehicle.")
				ENDIF
			#ENDIF
		
			// Get the vehicle I am in.
			IF bOverrideVeh
				IF DOES_ENTITY_EXIST(veh)
					IF IS_VEHICLE_DRIVEABLE(veh)
						vehId = veh
						PRINTLN("[Save_Create_Vehicle] - bOverrideVeh = TRUE, vehicle id being used = ", NATIVE_TO_INT(vehId))
					ENDIF
				ENDIF
			ELSE
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehId = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					PRINTLN("[Save_Create_Vehicle] - got vehicle, id = ", NATIVE_TO_INT(vehId))
				ENDIF
			ENDIF
				
			IF DOES_ENTITY_EXIST(vehId)
				IF IS_VEHICLE_DRIVEABLE(vehId)
					
					IF NOT IS_VEHICLE_MODEL_ALLOWED_TO_BE_RECREATED(vehId)
						PRINTLN("[Save_Create_Vehicle] - vehicle model is on block list, cannot save.")
						EXIT
					ENDIF
					
					IF IS_BIG_VEHICLE(vehId)
						PRINTLN("[Save_Create_Vehicle] - vehicle is a big vehicle, cannot save.")
						EXIT
					ENDIF
					
					IF DECOR_EXIST_ON(vehId, "Veh_Modded_By_Player") 
						IF DECOR_GET_INT(vehId, "Veh_Modded_By_Player") != NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID())
							PRINTLN("[Save_Create_Vehicle] - vehicle has been modded by another player, cannot save.")
							EXIT
						ENDIF
					ENDIF
					
					// Get if it's my personal vehicle. If it is bail, we don't want ot save it.
					IF IS_VEHICLE_MY_PERSONAL_VEHICLE(vehId) // IS_VEHICLE_A_PERSONAL_VEHICLE(vehId)
					OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(), FALSE)
						g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bFinishedInPersonalVehicle = TRUE
						PRINTLN("[Save_Create_Vehicle] - vehicle is a personal vehicle.")
						EXIT
					ELSE
						IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehId)
						OR IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
							g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bFinishedInSomeoneElsesPersonalVehicle = TRUE
							bSaveVehicleModelData = FALSE
							PRINTLN("[Save_Create_Vehicle] - vehicle is someone else's personal vehicle, setting bSaveVehicleModelData = FALSE.")
						ELSE
							PRINTLN("[Save_Create_Vehicle] - vehicle is not a personal vehicle.")
						ENDIF
					ENDIF
					
					// If we've made it here the vehicle isn't a personal vehicle, save it.
					IF bSaveVehicleModelData
						GET_VEHICLE_SETUP_MP(vehId, g_TransitionSessionNonResetVars.sEomAbmientVehicleData.s_vehicleSetupStruct)
						g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos = GET_ENTITY_COORDS(vehId)
						g_TransitionSessionNonResetVars.sEomAbmientVehicleData.fVehRot = GET_ENTITY_HEADING(vehId)
						IF DECOR_EXIST_ON(vehId,"Not_Allow_As_Saved_Veh")
							IF NOT (DECOR_GET_INT(vehId, "Not_Allow_As_Saved_Veh") = NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
								g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bHasNotAllowSavedVehDecorator = TRUE
							ENDIF
						ENDIF
						GET_VEHICLE_CUSTOM_SECONDARY_COLOUR(	vehId, 
																g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iSecondaryR, 
																g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iSecondaryG,
																g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iSecondaryB	)
						
						PRINTLN("[Save_Create_Vehicle] - got vehicle secondary colours: ")
						PRINTLN("[Save_Create_Vehicle] - R: ", g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iSecondaryR)
						PRINTLN("[Save_Create_Vehicle] - G: ", g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iSecondaryG)
						PRINTLN("[Save_Create_Vehicle] - B: ", g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iSecondaryB)
						
					ENDIF
					
					// If we have not overridden with a specific vehicle of our own.
					IF NOT bOverrideVeh
						
						// Save driver gamer handle and my vehicle seat.
						FOR i = VS_DRIVER TO COUNT_OF(VEHICLE_SEAT)-1
							
							// Get ped in the seat.
							pedId = GET_PED_IN_VEHICLE_SEAT(vehId, INT_TO_ENUM(VEHICLE_SEAT, i))
							
							// Get players in vehicle info.
							IF (INT_TO_ENUM(VEHICLE_SEAT, i) = VS_DRIVER)
								
								PRINTLN("[Save_Create_Vehicle] - checking driver seat.")
								
								IF DOES_ENTITY_EXIST(pedId)
								AND IS_ENTITY_A_PED(pedId)
								AND IS_PED_A_PLAYER(pedId)
								
									playerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId)
									
									#IF IS_DEBUG_BUILD
									IF IS_NET_PLAYER_OK(playerId, FALSE)
										PRINTLN("[Save_Create_Vehicle] - driver gamer handle is ", GET_PLAYER_NAME(playerId))
									ENDIF
									#ENDIF
									
									IF NOT IS_ENTITY_DEAD(pedId)
										g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver = GET_GAMER_HANDLE_PLAYER(playerId)
										PRINTLN("[Save_Create_Vehicle] - driver is not dead, saved driver.")
									ELSE
										PRINTLN("[Save_Create_Vehicle] - driver is dead.")
									ENDIF
									
								ELSE
									PRINTLN("[Save_Create_Vehicle] - no ped in driver seat.")
								ENDIF
								
							ELIF (INT_TO_ENUM(VEHICLE_SEAT, i) = VS_FRONT_RIGHT)
								
								PRINTLN("[Save_Create_Vehicle] - checking front right seat.")
								
								IF DOES_ENTITY_EXIST(pedId)
								AND IS_ENTITY_A_PED(pedId)
								AND IS_PED_A_PLAYER(pedId) 
								
									playerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId)
									
									#IF IS_DEBUG_BUILD
									IF IS_NET_PLAYER_OK(playerId, FALSE)
										PRINTLN("[Save_Create_Vehicle] - front right gamer handle is ", GET_PLAYER_NAME(playerId))
									ENDIF
									#ENDIF
									
									IF NOT IS_ENTITY_DEAD(pedId)
										g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhFrontRight = GET_GAMER_HANDLE_PLAYER(playerId)
										PRINTLN("[Save_Create_Vehicle] - front right is not dead, saved front right.")
									ELSE
										PRINTLN("[Save_Create_Vehicle] - front right is dead.")
									ENDIF
								
								ELSE
									PRINTLN("[Save_Create_Vehicle] - no ped in front right seat.")
								ENDIF
								
							ELIF (INT_TO_ENUM(VEHICLE_SEAT, i) = VS_BACK_RIGHT)
								
								PRINTLN("[Save_Create_Vehicle] - checking back right seat.")
								
								IF DOES_ENTITY_EXIST(pedId)
								AND IS_ENTITY_A_PED(pedId)
								AND IS_PED_A_PLAYER(pedId)
								
									playerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId)
									
									#IF IS_DEBUG_BUILD
									IF IS_NET_PLAYER_OK(playerId, FALSE)
										PRINTLN("[Save_Create_Vehicle] - back right gamer handle is ", GET_PLAYER_NAME(playerId))
									ENDIF
									#ENDIF
									
									IF NOT IS_ENTITY_DEAD(pedId)
										g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackRight = GET_GAMER_HANDLE_PLAYER(playerId)
										PRINTLN("[Save_Create_Vehicle] - back right is not dead, saved back right.")
									ELSE
										PRINTLN("[Save_Create_Vehicle] - back right is dead.")
									ENDIF
									
								ELSE
									PRINTLN("[Save_Create_Vehicle] - no ped in back right seat.")
								ENDIF
								
							ELIF (INT_TO_ENUM(VEHICLE_SEAT, i) = VS_BACK_LEFT)
								
								PRINTLN("[Save_Create_Vehicle] - checking back left seat.")
								
								IF DOES_ENTITY_EXIST(pedId)
								AND IS_ENTITY_A_PED(pedId)
								AND IS_PED_A_PLAYER(pedId)
								
									playerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId)
									
									#IF IS_DEBUG_BUILD
									IF IS_NET_PLAYER_OK(playerId, FALSE)
										PRINTLN("[Save_Create_Vehicle] - back left gamer handle is ", GET_PLAYER_NAME(playerId))
									ENDIF
									#ENDIF
									
									IF NOT IS_ENTITY_DEAD(pedId)
										g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackLeft = GET_GAMER_HANDLE_PLAYER(playerId)
										PRINTLN("[Save_Create_Vehicle] - back left is not dead, saved back left.")
									ELSE
										PRINTLN("[Save_Create_Vehicle] - back left is dead.")
									ENDIF
									
								ELSE
									PRINTLN("[Save_Create_Vehicle] - no ped in back left seat.")
								ENDIF
								
							ELIF (INT_TO_ENUM(VEHICLE_SEAT, i) = VS_EXTRA_LEFT_1)
								
								PRINTLN("[Save_Create_Vehicle] - checking extra left 1 seat.")
								
								IF DOES_ENTITY_EXIST(pedId)
								AND IS_ENTITY_A_PED(pedId)
								AND IS_PED_A_PLAYER(pedId)
								
									playerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId)
									
									#IF IS_DEBUG_BUILD
									IF IS_NET_PLAYER_OK(playerId, FALSE)
										PRINTLN("[Save_Create_Vehicle] - extra left 1 gamer handle is ", GET_PLAYER_NAME(playerId))
									ENDIF
									#ENDIF
									
									IF NOT IS_ENTITY_DEAD(pedId)
										g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft1 = GET_GAMER_HANDLE_PLAYER(playerId)
										PRINTLN("[Save_Create_Vehicle] - extra left 1 is not dead, saved extra left 1.")
									ELSE
										PRINTLN("[Save_Create_Vehicle] - extra left 1 is dead.")
									ENDIF
									
								ELSE
									PRINTLN("[Save_Create_Vehicle] - no ped in extra left 1 seat.")
								ENDIF
								
							ELIF (INT_TO_ENUM(VEHICLE_SEAT, i) = VS_EXTRA_RIGHT_1)
								
								PRINTLN("[Save_Create_Vehicle] - checking extra right 1 seat.")
								
								IF DOES_ENTITY_EXIST(pedId)
								AND IS_ENTITY_A_PED(pedId)
								AND IS_PED_A_PLAYER(pedId)
								
									playerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId)
									
									#IF IS_DEBUG_BUILD
									IF IS_NET_PLAYER_OK(playerId, FALSE)
										PRINTLN("[Save_Create_Vehicle] - extra right 1 gamer handle is ", GET_PLAYER_NAME(playerId))
									ENDIF
									#ENDIF
									
									IF NOT IS_ENTITY_DEAD(pedId)
										g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight1 = GET_GAMER_HANDLE_PLAYER(playerId)
										PRINTLN("[Save_Create_Vehicle] - extra right 1 is not dead, saved extra right 1.")
									ELSE
										PRINTLN("[Save_Create_Vehicle] - extra right 1 is dead.")
									ENDIF
								
								ELSE
									PRINTLN("[Save_Create_Vehicle] - no ped in extra right 1 seat.")
								ENDIF
								
							ELIF (INT_TO_ENUM(VEHICLE_SEAT, i) = VS_EXTRA_LEFT_2)
								
								PRINTLN("[Save_Create_Vehicle] - checking extra left 2 seat.")
								
								IF DOES_ENTITY_EXIST(pedId)
								AND IS_ENTITY_A_PED(pedId)
								AND IS_PED_A_PLAYER(pedId)
								
									playerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId)
									
									#IF IS_DEBUG_BUILD
									IF IS_NET_PLAYER_OK(playerId, FALSE)
										PRINTLN("[Save_Create_Vehicle] - extra left 2 gamer handle is ", GET_PLAYER_NAME(playerId))
									ENDIF
									#ENDIF
									
									IF NOT IS_ENTITY_DEAD(pedId)
										g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft2 = GET_GAMER_HANDLE_PLAYER(playerId)
										PRINTLN("[Save_Create_Vehicle] - extra left 2 is not dead, saved extra left 2.")
									ELSE
										PRINTLN("[Save_Create_Vehicle] - extra left 2 is dead.")
									ENDIF
								
								ELSE
									PRINTLN("[Save_Create_Vehicle] - no ped in extra left 2 seat.")
								ENDIF
								
							ELIF (INT_TO_ENUM(VEHICLE_SEAT, i) = VS_EXTRA_RIGHT_2)
								
								PRINTLN("[Save_Create_Vehicle] - checking extra right 2 seat.")
								
								IF DOES_ENTITY_EXIST(pedId)
								AND IS_ENTITY_A_PED(pedId)
								AND IS_PED_A_PLAYER(pedId)
								
									playerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId) 
									
									#IF IS_DEBUG_BUILD
									IF IS_NET_PLAYER_OK(playerId, FALSE)
										PRINTLN("[Save_Create_Vehicle] - extra right 2 gamer handle is ", GET_PLAYER_NAME(playerId))
									ENDIF
									#ENDIF
									
									IF NOT IS_ENTITY_DEAD(pedId)
										g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight2 = GET_GAMER_HANDLE_PLAYER(playerId)
										PRINTLN("[Save_Create_Vehicle] - extra right 2 is not dead, saved extra right 2.")
									ELSE
										PRINTLN("[Save_Create_Vehicle] - extra right 2 is dead.")
									ENDIF
								
								ELSE
									PRINTLN("[Save_Create_Vehicle] - no ped in extra right 2 seat.")
								ENDIF
								
							ELIF (INT_TO_ENUM(VEHICLE_SEAT, i) = VS_EXTRA_LEFT_3)
								
								PRINTLN("[Save_Create_Vehicle] - checking extra left 3 seat.")
								
								IF DOES_ENTITY_EXIST(pedId)
								AND IS_ENTITY_A_PED(pedId)
								AND IS_PED_A_PLAYER(pedId)
								
									playerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId)
									
									#IF IS_DEBUG_BUILD
									IF IS_NET_PLAYER_OK(playerId, FALSE)
										PRINTLN("[Save_Create_Vehicle] - extra left 3 gamer handle is ", GET_PLAYER_NAME(playerId))
									ENDIF
									#ENDIF
									
									IF NOT IS_ENTITY_DEAD(pedId)
										g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft3 = GET_GAMER_HANDLE_PLAYER(playerId)
										PRINTLN("[Save_Create_Vehicle] - extra left 3 is not dead, saved extra left 3.")
									ELSE
										PRINTLN("[Save_Create_Vehicle] - extra left 3 is dead.")
									ENDIF
								
								ELSE
									PRINTLN("[Save_Create_Vehicle] - no ped in extra left 3 seat.")
								ENDIF
								
							ELIF (INT_TO_ENUM(VEHICLE_SEAT, i) = VS_EXTRA_RIGHT_3)
								
								PRINTLN("[Save_Create_Vehicle] - checking extra right 3 seat.")
								
								IF DOES_ENTITY_EXIST(pedId)
								AND IS_ENTITY_A_PED(pedId)
								AND IS_PED_A_PLAYER(pedId)
								
									playerId = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedId)
									
									#IF IS_DEBUG_BUILD
									IF IS_NET_PLAYER_OK(playerId, FALSE)
										PRINTLN("[Save_Create_Vehicle] - extra right 3 gamer handle is ", GET_PLAYER_NAME(playerId))
									ENDIF
									#ENDIF
									
									IF NOT IS_ENTITY_DEAD(pedId)
										g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight3 = GET_GAMER_HANDLE_PLAYER(playerId)
										PRINTLN("[Save_Create_Vehicle] - extra right 3 is not dead, saved extra right 3.")
									ELSE
										PRINTLN("[Save_Create_Vehicle] - extra right 3 is dead.")
									ENDIF
								
								ELSE
									PRINTLN("[Save_Create_Vehicle] - no ped in extra right 3 seat.")
								ENDIF
								
							ENDIF
							
							// If the ped is me, save my vehicle seat.
							IF (pedId = PLAYER_PED_ID())
								
								g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eVsMySeat = INT_TO_ENUM(VEHICLE_SEAT, i)
								
								#IF IS_DEBUG_BUILD
									PRINTLN("[Save_Create_Vehicle] - my vehicle seat is ", GET_PLAYER_NAME(playerId))
									PRINT_VEHICLE_SEAT(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eVsMySeat)
								#ENDIF
								
							ENDIF
							
						ENDFOR
					
					ELSE
					
						g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
						PRINTLN("[Save_Create_Vehicle] - using override vehicle, saving local player as driver.")
					
					ENDIF
					
					// Done!
					IF bOverrideVeh
						g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bsavedVehIsOverride = TRUE
						PRINTLN("[Save_Create_Vehicle] - vehicle is an override veh, bsavedVehIsOverride = TRUE.")
					ENDIF
					
					g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bSavedValidVehicle = TRUE
					PRINTLN("[Save_Create_Vehicle] - vehicle is driveable, saved physical info with GET_VEHICLE_SETUP().")
					
				ELSE
				
					PRINTLN("[Save_Create_Vehicle] - vehicle is not driveable.")
					
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

PROC SAVE_CORONA_HOST_AS_DRIVER(BOOL bIAmCoronaHost)
	
	IF bIAmCoronaHost
		PRINTLN("[Save_Create_Vehicle] - bIAmCoronaHost = TRUE.")
	ELSE
		PRINTLN("[Save_Create_Vehicle] - bIAmCoronaHost = FALSE.")
	ENDIF
	
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bIAmDriverForContactMission = FALSE
	
	IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhCoronaHost, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhCoronaHost))
		
		// if we are making the corona host the driver, but they were originally a passenger, make the car non-saveable (to avoid duping, see 1778262)
		IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver))
			IF NOT NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver, g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhCoronaHost)
				g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bHasNotAllowSavedVehDecorator = TRUE
			ENDIF
		ENDIF
		
		g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver = g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhCoronaHost
		
		IF bIAmCoronaHost
			g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eVsMySeat = VS_DRIVER
			g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bIAmDriverForContactMission = TRUE
		ELSE
			g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eVsMySeat = VS_FRONT_RIGHT
		ENDIF
		
		PRINTLN("[Save_Create_Vehicle] - setting driver of my vehicle to be the corona host.")
		
	ELSE
	
		PRINTLN("[Save_Create_Vehicle] - corona host gamer handle not valid.")
		
	ENDIF
	
ENDPROC

FUNC PLAYER_INDEX GET_DRIVER_PLAYER_INDEX()
	
	INT i
	GAMER_HANDLE eTempHandle
	PLAYER_INDEX playerId
	
	
	IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver))
		// Repeat throughplayer comparing their gamer handles to the saved one. If we find the driver return it.
		REPEAT NUM_NETWORK_PLAYERS i
			playerId = INT_TO_NATIVE(PLAYER_INDEX, i)
			IF NETWORK_IS_PLAYER_ACTIVE(playerId)
				PRINTLN("[Save_Create_Vehicle] - GET_DRIVER_PLAYER_INDEX - player being checked = ", GET_PLAYER_NAME(playerId))
				eTempHandle = GET_GAMER_HANDLE_PLAYER(playerId)
				IF NETWORK_IS_HANDLE_VALID(eTempHandle, SIZE_OF(eTempHandle))
					IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver, eTempHandle)
						PRINTLN("[Save_Create_Vehicle] - GET_DRIVER_PLAYER_INDEX - driver = ", GET_PLAYER_NAME(playerId))
						RETURN INT_TO_NATIVE(PLAYER_INDEX, i)
					ENDIF
				ELSE
					PRINTLN("[Save_Create_Vehicle] - GET_DRIVER_PLAYER_INDEX - player doesnt have a valid handle")
				ENDIF
			ENDIF
		ENDREPEAT
	ELSE
		PRINTLN("[Save_Create_Vehicle] - GET_DRIVER_PLAYER_INDEX - eGhDriver is not a valid handle")
	ENDIF
	
	// Return an invalid player if we can't find the driver.
	RETURN INVALID_PLAYER_INDEX()
	
ENDFUNC

FUNC BOOL GET_IS_PLAYER_SITTING_IN_VEHICLE(PLAYER_INDEX playerId)
	
	PED_INDEX pedID
	pedId = GET_PLAYER_PED(playerId)
	
	IF DOES_ENTITY_EXIST(pedId)
		IF NOT IS_ENTITY_DEAD(pedId)
			IF NOT IS_PED_SITTING_IN_VEHICLE(GET_PLAYER_PED(playerId), g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL ARE_ALL_PASSENGERS_IN_VEHICLE()
	
	INT i
	GAMER_HANDLE eTempHandle
	PLAYER_INDEX playerId
	
	IF g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bIAmDriverForContactMission
		RETURN TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh)
		IF IS_VEHICLE_DRIVEABLE(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh)
			REPEAT NUM_NETWORK_PLAYERS i
				playerId = INT_TO_NATIVE(PLAYER_INDEX, i)
				IF NETWORK_IS_PLAYER_ACTIVE(playerId)
					PRINTLN("[Save_Create_Vehicle] - ARE_ALL_PASSENGERS_IN_VEHICLE - player being checked = ", GET_PLAYER_NAME(playerId))
					eTempHandle = GET_GAMER_HANDLE_PLAYER(playerId)
					IF NETWORK_IS_HANDLE_VALID(eTempHandle, SIZE_OF(eTempHandle))
						IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver))	
							IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver, eTempHandle)
								IF NOT GET_IS_PLAYER_SITTING_IN_VEHICLE(playerId)
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
						IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhFrontRight, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhFrontRight))
							IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhFrontRight, eTempHandle)
								IF NOT GET_IS_PLAYER_SITTING_IN_VEHICLE(playerId)
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
						IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackRight, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackRight))
							IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackRight, eTempHandle)
								IF NOT GET_IS_PLAYER_SITTING_IN_VEHICLE(playerId)
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
						IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackLeft, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackLeft))
							IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackLeft, eTempHandle)
								IF NOT GET_IS_PLAYER_SITTING_IN_VEHICLE(playerId)
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
						IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhFrontRight, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhFrontRight))
							IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhFrontRight, eTempHandle)
								IF NOT GET_IS_PLAYER_SITTING_IN_VEHICLE(playerId)
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
						IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackRight, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackRight))
							IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackRight, eTempHandle)
								IF NOT GET_IS_PLAYER_SITTING_IN_VEHICLE(playerId)
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
						IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackLeft, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackLeft))
							IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackLeft, eTempHandle)
								IF NOT GET_IS_PLAYER_SITTING_IN_VEHICLE(playerId)
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
						IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft1, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft1))
							IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft1, eTempHandle)
								IF NOT GET_IS_PLAYER_SITTING_IN_VEHICLE(playerId)
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
						IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight1, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight1))
							IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight1, eTempHandle)
								IF NOT GET_IS_PLAYER_SITTING_IN_VEHICLE(playerId)
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
						IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft2, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft2))
							IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft2, eTempHandle)
								IF NOT GET_IS_PLAYER_SITTING_IN_VEHICLE(playerId)
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
						IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight2, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight2))
							IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight2, eTempHandle)
								IF NOT GET_IS_PLAYER_SITTING_IN_VEHICLE(playerId)
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
						IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft3, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft3))
							IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft3, eTempHandle)
								IF NOT GET_IS_PLAYER_SITTING_IN_VEHICLE(playerId)
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
						IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight3, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight3))
							IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight3, eTempHandle)
								IF NOT GET_IS_PLAYER_SITTING_IN_VEHICLE(playerId)
									RETURN FALSE
								ENDIF		
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	RETURN TRUE
							
ENDFUNC


///   Please pass in << 0.0, 0.0, 0.0 >> for  vStartPosCoords. This is a special argument to sort a specific bug for Rowan. 
FUNC BOOL CREATE_MY_EOM_SAVED_VEHICLE(VECTOR vStartPosCoords, BOOL bDoingMissionTutorial = FALSE, BOOL bIAmCreatingVehicle = FALSE, PLAYER_INDEX piMissionTutorialCreatorPlayerID = NULL, BOOL bUseOptionsMenuStart = FALSE, BOOL bFreezeVehicleForPostMissionCleanup = FALSE)
	
	#IF IS_DEBUG_BUILD 
	VECTOR vDebugVector 
	#ENDIF
	
	// Get who was the driver of the vehicle.
	PLAYER_INDEX driverPlayer = GET_DRIVER_PLAYER_INDEX()
	
	// Run failsafe timer, we don't want to be stuck forever.
	//#IF IS_DEBUG_BUILD
		IF NOt HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eFailSafeTimer)
			START_NET_TIMER(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eFailSafeTimer)
		ELSE
			IF HAS_NET_TIMER_EXPIRED(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eFailSafeTimer, 30000)
				PRINTLN("[Save_Create_Vehicle] - eFailSafeTimer has expired, something bad has happened, don't bother creating vehicle.")
				g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eResult = eCREATEANDENTERAMBIENTVEHICLESTATUS_VEHICLE_CREATION_ABORTED
				RETURN TRUE
			ENDIF
		ENDIF
	//#ENDIF
	
	// If the driver isn't in the session with us, just return true and finish up.
	IF NOT bDoingMissionTutorial
		IF driverPlayer = INVALID_PLAYER_INDEX()
			PRINTLN("[Save_Create_Vehicle] - driverPlayer = invalid player index, not in session with us, don't bother creating vehicle.")
			g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eResult = eCREATEANDENTERAMBIENTVEHICLESTATUS_VEHICLE_CREATION_ABORTED
			RETURN TRUE
		ENDIF
	ELSE
		IF GET_DISTANCE_BETWEEN_COORDS(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos, <<-55.9261, -1502.5599, 30.3742>>) > 50.0
			g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos = <<-55.9261, -1502.5599, 30.3742>>
		ENDIF
	ENDIF
	
	// If we have a driver the vehicle is going to be created. Wait for it to be created then return true.
	IF (driverPlayer = PLAYER_ID())
	OR (bDoingMissionTutorial AND bIAmCreatingVehicle)
		IF NOT g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bSavedValidVehicle
			PRINTLN("[Save_Create_Vehicle] - bSavedValidVehicle = FALSE, aborting creation since there's no vehicle to create.")
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sEomAmbientVehicleData.bIAmCoronaHosThatHasAborted = TRUE
			g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eResult = eCREATEANDENTERAMBIENTVEHICLESTATUS_VEHICLE_CREATION_ABORTED
			RETURN TRUE
		ENDIF
		SWITCH g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iCreateStage
			// Get if the original position is still ok to place the vehicle.
			CASE 0
				IF bDoingMissionTutorial
					IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-47.1300, -1498.7919, 34.1751>>, FALSE)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 285.5052)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					ENDIF
				ELSE
					IF g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bFinishedInPersonalVehicle
						PRINTLN("[Save_Create_Vehicle] - supposed to go into personal vehicle, just return TRUE so we skip clone creation.")
						RETURN TRUE
					ENDIF
				ENDIF
				IF bUseOptionsMenuStart
					IF NOT ARE_VECTORS_EQUAL(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos, vStartPosCoords)
						g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos = vStartPosCoords
						PRINTLN("[Save_Create_Vehicle] - options menu start being used, setting veh pos to vStartPos. Now equals ", g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos)
					ENDIF
				ENDIF
				IF ( IS_POINT_OK_FOR_NET_ENTITY_CREATION(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos, 3, 1, 1, 4, FALSE, TRUE, TRUE, 60, TRUE)) // AND (NOT bDoingMissionTutorial) )
				AND NOT IS_POINT_IN_GLOBAL_EXCLUSION_ZONE(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos)
				AND NOT IS_POINT_IN_PROBLEM_NODE_AREA(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos)
					PRINTLN("[Save_Create_Vehicle] - original loc ", g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos, " is ok for net entity creation.")
					g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iCreateStage = 2
				ELSE
					PRINTLN("[Save_Create_Vehicle] - original loc ", g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos, " is not ok for net entity creation, need to find new coords.")
					IF bDoingMissionTutorial
						SET_MISSION_SPAWN_OCCLUSION_SPHERE(<< -72.993, -1520.132, 33.434 >>, 18.0)
					ENDIF
					g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iCreateStage = 1
				ENDIF
			BREAK
			// If can't place in the original position, find new coords nearby.
			CASE 1
				VECTOR vTemp
				FLOAT fTemp
				
				VEHICLE_SPAWN_LOCATION_PARAMS Params
				Params.fMaxDistance = 27.5
				
				IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos,<<0.0, 0.0, 0.0>>, g_TransitionSessionNonResetVars.sEomAbmientVehicleData.s_vehicleSetupStruct.VehicleSetup.eModel, TRUE, vTemp, fTemp, Params)
					g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos = vTemp
					g_TransitionSessionNonResetVars.sEomAbmientVehicleData.fVehRot = fTemp
					CLEAR_SPAWN_AREA()
					g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iCreateStage = 2
					PRINTLN("[Save_Create_Vehicle] - found new coords to place vehicle: ", g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos)
				ENDIF
			BREAK
			// Create the vehicle.
			CASE 2
				IF IS_MODEL_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.s_vehicleSetupStruct.VehicleSetup.eModel)
					IF REQUEST_LOAD_MODEL(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.s_vehicleSetupStruct.VehicleSetup.eModel)
						IF CREATE_NET_VEHICLE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sEomAmbientVehicleData.niVeh, g_TransitionSessionNonResetVars.sEomAbmientVehicleData.s_vehicleSetupStruct.VehicleSetup.eModel, g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos, g_TransitionSessionNonResetVars.sEomAbmientVehicleData.fVehRot, FALSE, TRUE, TRUE, FALSE, TRUE)
							SET_VEHICLE_SETUP_MP(NET_TO_VEH(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sEomAmbientVehicleData.niVeh), g_TransitionSessionNonResetVars.sEomAbmientVehicleData.s_vehicleSetupStruct)
							SET_VEHICLE_CUSTOM_SECONDARY_COLOUR(NET_TO_VEH(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sEomAmbientVehicleData.niVeh), 
																g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iSecondaryR, 
																g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iSecondaryG,
																g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iSecondaryB	)
							PRINTLN("[Save_Create_Vehicle] - set vehicle secondary colours: ")
							PRINTLN("[Save_Create_Vehicle] - R: ", g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iSecondaryR)
							PRINTLN("[Save_Create_Vehicle] - G: ", g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iSecondaryG)
							PRINTLN("[Save_Create_Vehicle] - B: ", g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iSecondaryB)
							g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh = NET_TO_VEH(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sEomAmbientVehicleData.niVeh) // Make local copy of net id.
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh, TRUE)
							SET_VEHICLE_ON_GROUND_PROPERLY(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh)
							IF bFreezeVehicleForPostMissionCleanup
								FREEZE_ENTITY_POSITION(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh, TRUE)
								PRINTLN("[Save_Create_Vehicle] - bFreezeVehicleForPostMissionCleanup = TRUE, freezeing car. This should get unfrozen in the post mission cleanup.")
							ENDIF
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.viAfterMissionWarpVehToUnfreeze = g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh
							IF g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bHasNotAllowSavedVehDecorator
								IF HAS_FM_CAR_MOD_TUT_BEEN_DONE() // 1813284
									IF DECOR_IS_REGISTERED_AS_TYPE("Not_Allow_As_Saved_Veh", DECOR_TYPE_INT)
										IF NOT DECOR_EXIST_ON(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh, "Not_Allow_As_Saved_Veh")
											DECOR_SET_INT(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh, "Not_Allow_As_Saved_Veh", 1)
											PRINTLN("[Save_Create_Vehicle] - set decor Not_Allow_As_Saved_Veh on vehicle.")
										ENDIF
									ENDIF
								ELSE
									PRINTLN("[Save_Create_Vehicle] - NOT setting decor Not_Allow_As_Saved_Veh on vehicle as car mod tutorial not done.")
								ENDIF
							ENDIF
							#IF IS_DEBUG_BUILD
							vDebugVector = GET_ENTITY_COORDS(NET_TO_VEH(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sEomAmbientVehicleData.niVeh))
							PRINTLN("[Save_Create_Vehicle] - created vehicle at coords: ", vDebugVector)
							#ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				ELSE
					PRINTLN("[Save_Create_Vehicle] - IS_MODEL_VALID returning FALSE?")
				ENDIF
			BREAK
		ENDSWITCH
	ELIF (driverPlayer != INVALID_PLAYER_INDEX()) OR (bDoingMissionTutorial AND (NOT bIAmCreatingVehicle))
		IF (bDoingMissionTutorial AND (NOT bIAmCreatingVehicle) ) 
			driverPlayer = piMissionTutorialCreatorPlayerID
		ENDIF
		IF IS_PED_IN_ANY_VEHICLE(GET_PLAYER_PED(driverPlayer))
			IF g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh != GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(driverPlayer))
				g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh = GET_VEHICLE_PED_IS_IN(GET_PLAYER_PED(driverPlayer)) // Make local copy of net id in case driver drops out ofer creation and we lose the handle in his player Bd.
				PRINTLN("[Save_Create_Vehicle] - driver has entered a vehicle during the create stage - personal vehicle? ")
				RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("[Save_Create_Vehicle] - driver is not in a vehicle yet.")	
		ENDIF
		IF IS_NET_VEHICLE_DRIVEABLE(GlobalplayerBD_FM[NATIVE_TO_INT(driverPlayer)].sEomAmbientVehicleData.niVeh)
			g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh = NET_TO_VEH(GlobalplayerBD_FM[NATIVE_TO_INT(driverPlayer)].sEomAmbientVehicleData.niVeh) // Make local copy of net id in case driver drops out ofer creation and we lose the handle in his player Bd.
			#IF IS_DEBUG_BUILD
			vDebugVector = GET_ENTITY_COORDS(NET_TO_VEH(GlobalplayerBD_FM[NATIVE_TO_INT(driverPlayer)].sEomAmbientVehicleData.niVeh))
			PRINTLN("[Save_Create_Vehicle] - driver has created vehicle at coords: ", vDebugVector)
			#ENDIF
			RETURN TRUE
		ELSE
			PRINTLN("[Save_Create_Vehicle] - driver has not created saved vehicle yet.")
			IF GlobalplayerBD_FM[NATIVE_TO_INT(driverPlayer)].sEomAmbientVehicleData.bIAmCoronaHosThatHasAborted
				PRINTLN("[Save_Create_Vehicle] - bIAmCoronaHosThatHasAborted, setting to aborted and kicking out.")
				g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eResult = eCREATEANDENTERAMBIENTVEHICLESTATUS_VEHICLE_CREATION_ABORTED
				RETURN TRUE
			ELSE
				PRINTLN("[Save_Create_Vehicle] - bIAmCoronaHosThatHasAborted = FALSE, continuing to wait fo vehicle creation.")
			ENDIF
		ENDIF
	ELSE
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC VEHICLE_SEAT GET_MY_CORONA_HOST_VEHICLE_PASSENGER_SEAT()
	
	// Variables.
	INT i
	INT iParticipants[NUM_NETWORK_PLAYERS]
	INT iIndexCount
	
	// Store a list of the participants on the mission.
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_NATIVE(PARTICIPANT_INDEX, i))
			iParticipants[iIndexCount] = i
			iIndexCount++
		ENDIF
	ENDREPEAT
	
	// Loop through the list and get my assigned seat. 
	REPEAT iIndexCount i
		IF iParticipants[i] = NATIVE_TO_INT(PARTICIPANT_ID())
			SWITCH i
				CASE 0	RETURN VS_DRIVER
				CASE 1	RETURN VS_FRONT_RIGHT
				CASE 2	RETURN VS_BACK_LEFT
				CASE 3	RETURN VS_BACK_RIGHT
				CASE 4	RETURN VS_EXTRA_LEFT_1
				CASE 5	RETURN VS_EXTRA_RIGHT_1
				CASE 6	RETURN VS_EXTRA_LEFT_2
				CASE 7	RETURN VS_EXTRA_RIGHT_2
				CASE 8	RETURN VS_EXTRA_LEFT_3
				CASE 9	RETURN VS_EXTRA_RIGHT_3
			ENDSWITCH
		ENDIF
	ENDREPEAT
	
	// Default invalid value.
	RETURN VS_ANY_PASSENGER
	
ENDFUNC

FUNc BOOL WARP_PLAYER_INTO_EOM_VEHICLE()
	
	IF g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bSavedVehIsOverride
		PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - bSavedVehIsOverride = TRUE, returning TRUE straight away, we don;t want to warp into the car.")
		g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eResult = eCREATEANDENTERAMBIENTVEHICLESTATUS_CREATED_AND_ENTERED_VEHICLE
		RETURN TRUE
	ENDIF
	
	// start failsafe timer soi if warping messes up we can keep going with the game.
	IF NOT HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eTimer)
		
		START_NET_TIMER(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eTimer)
		PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - started failsafe timer.")
		
	ELSE
		
		// If the failsafe timer kicks in, assert.
		IF HAS_NET_TIMER_EXPIRED(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eTimer, 30000)
			SCRIPT_ASSERT("[WJK] - SAVE_CREATE_EOM_VEHICLE - WARP_PLAYER_INTO_EOM_VEHICLE - failsafe timer has timed out, took longer than 30 seconds to warp into the vehicle.")
			PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - failsafe timer has timed out, took longer than 30 seconds to warp into the vehicle.")
			g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eResult = eCREATEANDENTERAMBIENTVEHICLESTATUS_VEHICLE_CREATION_ABORTED
			RETURN TRUE
		ENDIF
		
		// If the player isn't in the vehicle, warp the player into it.
		IF DOES_ENTITY_EXIST(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh)
			PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - saved vehicle exists.")
			IF IS_VEHICLE_DRIVEABLE(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh)
				PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - saved vehicle is driveable.")
				IF NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh)
					PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - not sitting in saved vehicle.")
					IF IS_VEHICLE_SEAT_FREE(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh, g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eVsMySeat)
					AND g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eVsMySeat != VS_ANY_PASSENGER
						PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - my saved seat is free.")
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
								FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
							ENDIF
							TASK_ENTER_VEHICLE(PLAYER_PED_ID(), g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh, 1, g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eVsMySeat, PEDMOVEBLENDRATIO_RUN, ECF_WARP_PED)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING()
							SET_GAMEPLAY_CAM_RELATIVE_PITCH()
							g_TransitionSessionNonResetVars.sPostMissionCleanupData.viAfterMissionWarpVehToUnfreeze = g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh
							PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - called TASK_ENTER_VEHICLE (ECF_WARP_PED).")
						ENDIF
					ELSE
						IF NOT HAS_NET_TIMER_STARTED(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eFindSeatTimer)
							START_NET_TIMER(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eFindSeatTimer)
						ELSE
							IF HAS_NET_TIMER_EXPIRED(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eFindSeatTimer, 10000)
								PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - took more than 10 seconds to find a free seat, aborting. Is the vehicle too small for everyone?")
								PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - veh id = ", NATIVE_TO_INT(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh))
								g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eResult = eCREATEANDENTERAMBIENTVEHICLESTATUS_VEHICLE_CREATION_ABORTED
								RETURN TRUE
							ENDIF
							g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eVsMySeat = GET_MY_CORONA_HOST_VEHICLE_PASSENGER_SEAT()// GET_FIRST_FREE_VEHICLE_PASSENGER_SEAT(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh)
							PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - saved vehicle seat not free for some reason, getting a new free one.")
						ENDIF
					ENDIF
				ELSE
					IF ARE_ALL_PASSENGERS_IN_VEHICLE()
						PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - sitting in vehicle.")
						g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eResult = eCREATEANDENTERAMBIENTVEHICLESTATUS_CREATED_AND_ENTERED_VEHICLE
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - vehicle not driveable, returning true and kicking out.")
				PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - veh id = ", NATIVE_TO_INT(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh))
				g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eResult = eCREATEANDENTERAMBIENTVEHICLESTATUS_VEHICLE_CREATION_ABORTED
				RETURN TRUE
			ENDIF
		ELSE
			PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - vehicle does not exist, returning true and kicking out.")
			PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - veh id = ", NATIVE_TO_INT(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh))
			g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eResult = eCREATEANDENTERAMBIENTVEHICLESTATUS_VEHICLE_CREATION_ABORTED
			RETURN TRUE
		ENDIF
		
	ENDIF
		
	IF (g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eResult = eCREATEANDENTERAMBIENTVEHICLESTATUS_VEHICLE_CREATION_ABORTED)
		PRINTLN("[Save_Create_Vehicle] - WARP_PLAYER_INTO_EOM_VEHICLE - already set to abort, returning true and kicking out.")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


// ------
// LOGIC
// ------

///   Please pass in << 0.0, 0.0, 0.0 >> for  vStartPosCoords. This is a special argument to sort a specific bug for Rowan. 
FUNC eCREATE_AND_ENTER_AMBIENT_VEHICLE_RESULT CREATE_AND_ENTER_EOM_AMBIENT_VEHICLE(VECTOR vStartPosCoords, BOOL bUseOptionsMenuStart = FALSE, BOOL bFreezeVehicleForPostMissionCleanup = FALSE)
	
	SWITCH g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iMainStage
		
		CASE 0
			
			IF CREATE_MY_EOM_SAVED_VEHICLE(vStartPosCoords, FALSE, FALSE, NULL, bUseOptionsMenuStart, bFreezeVehicleForPostMissionCleanup)
				g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iMainStage++
				IF (g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eResult = eCREATEANDENTERAMBIENTVEHICLESTATUS_VEHICLE_CREATION_ABORTED)
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sEomAmbientVehicleData.bIAmCoronaHosThatHasAborted = TRUE
				ENDIF
				PRINTLN("[Save_Create_Vehicle] - CREATE_AND_ENTER_EOM_AMBIENT_VEHICLE - created vehicle, going to main stage ", g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iMainStage)
			ENDIF
			
		BREAK
		
		CASE 1
			
			IF WARP_PLAYER_INTO_EOM_VEHICLE()
				IF NOT g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bsavedVehIsOverride
					IF DOES_ENTITY_EXIST(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh)
						IF NOT IS_ENTITY_DEAD(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh)
							IF NOT IS_VEHICLE_A_PERSONAL_VEHICLE(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viVeh)	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF (g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eResult = eCREATEANDENTERAMBIENTVEHICLESTATUS_VEHICLE_CREATION_ABORTED)
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sEomAmbientVehicleData.bIAmCoronaHosThatHasAborted = TRUE
				ENDIF
				g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iMainStage++
				PRINTLN("[Save_Create_Vehicle] - CREATE_AND_ENTER_EOM_AMBIENT_VEHICLE - warped into vehicle, going to main stage ", g_TransitionSessionNonResetVars.sEomAbmientVehicleData.iMainStage)
			ENDIF
			
		BREAK
		
		CASE 2
			
			PRINTLN("[Save_Create_Vehicle] - CREATE_AND_ENTER_EOM_AMBIENT_VEHICLE - done!")
			
		BREAK
		
	ENDSWITCH
	
	RETURN g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eResult
	
ENDFUNC


// ------------------------------------- \\
// ------------------------------------- \\
//										 \\
// Tutorial mission special case stuff.	 \\
//										 \\
// ------------------------------------- \\
// ------------------------------------- \\

PROC SAVE_MY_TRACKED_VEHICLE_DATA(VEHICLE_INDEX myVeh)
	
	GET_VEHICLE_SETUP_MP(myVeh, g_TransitionSessionNonResetVars.sEomAbmientVehicleData.s_vehicleSetupStruct)
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eVsMySeat 				= GET_SEAT_PED_IS_IN(PLAYER_PED_ID())
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.vVehPos					= GET_ENTITY_COORDS(myVeh)
	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.fVehRot					= GET_ENTITY_HEADING(myVeh)
	
ENDPROC

PROC TRACK_MY_LAST_VEHICLE_DATA(PLAYER_INDEX playerID, PED_INDEX pedID, BOOL bPartActive)
	
	VEHICLE_INDEX theirVeh
	VEHICLE_INDEX myVeh
	BOOL bClearVehicle
	
	// If playing.
	IF bPartActive
		
		// Save my vehicle info.
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					myVeh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(myVeh)
						IF myVeh != g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viTrackVehicle
							g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viTrackVehicle = myVeh
							SAVE_MY_TRACKED_VEHICLE_DATA(myVeh)
							g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bSavedValidVehicle = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// If ped is in a vehicle.
		IF IS_PED_IN_ANY_VEHICLE(pedID)
		
			// Get their vehicle.
			IF DOES_ENTITY_EXIST(pedID)
				IF NOT IS_ENTITY_DEAD(pedID)
					theirVeh = GET_VEHICLE_PED_IS_USING(pedID)
				ENDIF
			ENDIF
			
			// If we're sharing a vehicle.
			IF (theirVeh = g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viTrackVehicle)
				
				// If their vehicle is driveable.
				IF IS_VEHICLE_DRIVEABLE(theirVeh)
					
					// Save the seat info of everyone in the vehicle.
					SWITCH GET_SEAT_PED_IS_IN(pedID)
						CASE VS_DRIVER			g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver 		= GET_GAMER_HANDLE_PLAYER(playerID)	BREAK
						CASE VS_FRONT_RIGHT		g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhFrontRight 	= GET_GAMER_HANDLE_PLAYER(playerID)	BREAK
						CASE VS_BACK_RIGHT		g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackRight 	= GET_GAMER_HANDLE_PLAYER(playerID)	BREAK
						CASE VS_BACK_LEFT 		g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackLeft 		= GET_GAMER_HANDLE_PLAYER(playerID)	BREAK
						CASE VS_EXTRA_LEFT_1 	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft1 	= GET_GAMER_HANDLE_PLAYER(playerID)	BREAK
						CASE VS_EXTRA_RIGHT_1 	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight1 	= GET_GAMER_HANDLE_PLAYER(playerID)	BREAK
						CASE VS_EXTRA_LEFT_2 	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft2 	= GET_GAMER_HANDLE_PLAYER(playerID)	BREAK
						CASE VS_EXTRA_RIGHT_2 	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight2 	= GET_GAMER_HANDLE_PLAYER(playerID)	BREAK
						CASE VS_EXTRA_LEFT_3 	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraLeft3 	= GET_GAMER_HANDLE_PLAYER(playerID)	BREAK
						CASE VS_EXTRA_RIGHT_3 	g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhExtraRight3 	= GET_GAMER_HANDLE_PLAYER(playerID)	BREAK
					ENDSWITCH
					
				ENDIF
				
			ENDIF
			
		ENDIF
		
	ENDIF
	
	// If my vehicle is no longer driveable, clear flags and id ready to save again when I get in a new one.
	IF NOT DOES_ENTITY_EXIST(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viTrackVehicle)
		bClearVehicle = TRUE
	ELSE
		IF NOT IS_VEHICLE_DRIVEABLE(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viTrackVehicle)
			bClearVehicle = TRUE
		ENDIF
	ENDIF
	
	IF bClearVehicle
		VEHICLE_INDEX tempVehicle
		g_TransitionSessionNonResetVars.sEomAbmientVehicleData.viTrackVehicle = tempVehicle
		g_TransitionSessionNonResetVars.sEomAbmientVehicleData.bSavedValidVehicle = FALSE
	ENDIF
	
ENDPROC

FUNC PLAYER_INDEX GET_CREATE_VEHICLE_PLAYER()
	
	INT i
	GAMER_HANDLE eTempHandle
	PLAYER_INDEX playerId
	PLAYER_INDEX driverPlayerId, frontRightPlayerId, backLeftPlayerId, backRightPlayerId
	
	// Loop through all the players and save the player indexes of each player I was sharing the car with.
	REPEAT NUM_NETWORK_PLAYERS i
	
		playerId = INT_TO_NATIVE(PLAYER_INDEX, i)
		
		IF NETWORK_IS_PLAYER_ACTIVE(playerId)
		
			PRINTLN("[Save_Create_Vehicle] - GET_CREATE_VEHICLE_PLAYER - player being checked = ", GET_PLAYER_NAME(playerId))
			eTempHandle = GET_GAMER_HANDLE_PLAYER(playerId)
			
			IF NETWORK_IS_HANDLE_VALID(eTempHandle, SIZE_OF(eTempHandle))
					
				IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver))
					IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhDriver, eTempHandle)
						driverPlayerId = INT_TO_NATIVE(PLAYER_INDEX, i)
						PRINTLN("[Save_Create_Vehicle] - GET_CREATE_VEHICLE_PLAYER - driverPlayerId = ", GET_PLAYER_NAME(playerId))
					ENDIF
				ENDIF
				
				IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhFrontRight, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhFrontRight))
					IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhFrontRight, eTempHandle)
						frontRightPlayerId = INT_TO_NATIVE(PLAYER_INDEX, i)
						PRINTLN("[Save_Create_Vehicle] - GET_CREATE_VEHICLE_PLAYER - frontRightPlayerId = ", GET_PLAYER_NAME(playerId))
					ENDIF
				ENDIF
				
				IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackLeft, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackLeft))	
					IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackLeft, eTempHandle)
						backLeftPlayerId = INT_TO_NATIVE(PLAYER_INDEX, i)
						PRINTLN("[Save_Create_Vehicle] - GET_CREATE_VEHICLE_PLAYER - backLeftPlayerId = ", GET_PLAYER_NAME(playerId))
					ENDIF
				ENDIF
				
				IF NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackRight, SIZE_OF(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackRight))	
					IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sEomAbmientVehicleData.eGhBackRight, eTempHandle)
						backRightPlayerId = INT_TO_NATIVE(PLAYER_INDEX, i)
						PRINTLN("[Save_Create_Vehicle] - GET_CREATE_VEHICLE_PLAYER - backRightPlayerId = ", GET_PLAYER_NAME(playerId))
					ENDIF
				ENDIF
					
			ENDIF
			
		ENDIF
		
	ENDREPEAT
	
	// Check each player in order of priority and return the first valid one found.
	IF IS_NET_PLAYER_OK(driverPlayerId, FALSE)
		PRINTLN("[Save_Create_Vehicle] - GET_CREATE_VEHICLE_PLAYER - driver is ok, returning player: ", GET_PLAYER_NAME(driverPlayerId))
		RETURN driverPlayerId
	ELIF IS_NET_PLAYER_OK(frontRightPlayerId, FALSE)
		PRINTLN("[Save_Create_Vehicle] - GET_CREATE_VEHICLE_PLAYER - front right player is ok, returning player: ", GET_PLAYER_NAME(frontRightPlayerId))
		RETURN frontRightPlayerId
	ELIF IS_NET_PLAYER_OK(backLeftPlayerId, FALSE)
		PRINTLN("[Save_Create_Vehicle] - GET_CREATE_VEHICLE_PLAYER - back left player is ok, returning player: ", GET_PLAYER_NAME(backLeftPlayerId))
		RETURN backLeftPlayerId
	ELIF IS_NET_PLAYER_OK(backRightPlayerId, FALSE)
		PRINTLN("[Save_Create_Vehicle] - GET_CREATE_VEHICLE_PLAYER - back right player is ok, returning player: ", GET_PLAYER_NAME(backRightPlayerId))
		RETURN backRightPlayerId
	ENDIF
	
	PRINTLN("[Save_Create_Vehicle] - GET_CREATE_VEHICLE_PLAYER - nobody ok, returning invalid player.")
	NET_SCRIPT_ASSERT("[Save_Create_Vehicle] - SAVE_CREATE_EOM_VEHICLE - GET_CREATE_VEHICLE_PLAYER - nobody ok, returning invalid player.")
	RETURN INVALID_PLAYER_INDEX()
	
ENDFUNC

FUNC BOOL CREATE_MY_LAST_VEHICLE()
	
	PLAYER_INDEX createVehiclePlayer = GET_CREATE_VEHICLE_PLAYER()
	
	// If should be creating the vehicle, make it.
	IF (createVehiclePlayer = PLAYER_ID())
		
		PRINTLN("[Save_Create_Vehicle] - CREATE_MY_LAST_VEHICLE - I am create vehicle player.")
		
		IF CREATE_MY_EOM_SAVED_VEHICLE(<<0.0,0.0,0.0>>, TRUE, TRUE, createVehiclePlayer)
			PRINTLN("[Save_Create_Vehicle] - CREATE_MY_LAST_VEHICLE - complete!")
			RETURN TRUE
		ENDIF
		
	// If another player should be creating the vehicle, wait for him to make it.
	ELSE
		
		PRINTLN("[Save_Create_Vehicle] - CREATE_MY_LAST_VEHICLE -  I am not create vehicle player.")
		
		IF CREATE_MY_EOM_SAVED_VEHICLE(<<0.0,0.0,0.0>>, TRUE, FALSE, createVehiclePlayer)
			PRINTLN("[Save_Create_Vehicle] - CREATE_MY_LAST_VEHICLE - complete!")
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC




































