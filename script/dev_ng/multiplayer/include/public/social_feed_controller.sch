//////////////////////////////////////////////////////////////////////////////////////////
//                                                                                      //
//      SCRIPT NAME     :   social_feed_controller.sch                                  //
//      AUTHOR          :   William.Kennedy@RockstarNorth.com                           //
//      DESCRIPTION     :   Displays social feed messages.                              //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////
 
USING "rage_builtins.sch"
USING "globals.sch"
USING "net_events.sch"
USING "net_ticker_logic.sch"
USING "flow_mission_data_public.sch"
USING "commands_socialclub.sch"
USING "fmmc_cloud_loader.sch"
USING "socialclub_leaderboard.sch"
USING "net_comms_stored_public.sch"
USING "transition_controller.sch"
USING "net_cash_transactions.sch"

CONST_INT PRESENCE_EVENT_UNLOCKED_VEHICLES_BLOWN_UP_BRONZE          0
CONST_INT PRESENCE_EVENT_UNLOCKED_VEHICLES_BLOWN_UP_SILVER          1
CONST_INT PRESENCE_EVENT_UNLOCKED_VEHICLES_BLOWN_UP_GOLD            2
CONST_INT PRESENCE_EVENT_UNLOCKED_VEHICLES_BLOWN_UP_PLATINUM        3

CONST_INT PRESENCE_EVENT_UNLOCKED_VEHICLES_STOLEN_BRONZE            4
CONST_INT PRESENCE_EVENT_UNLOCKED_VEHICLES_STOLEN_SILVER            5
CONST_INT PRESENCE_EVENT_UNLOCKED_VEHICLES_STOLEN_GOLD              6
CONST_INT PRESENCE_EVENT_UNLOCKED_VEHICLES_STOLEN_PLATINUM          7

CONST_INT PRESENCE_EVENT_UNLOCKED_5_STAR_WANTED_BRONZE              8
CONST_INT PRESENCE_EVENT_UNLOCKED_5_STAR_WANTED_VEHICLES_SILVER     9
CONST_INT PRESENCE_EVENT_UNLOCKED_5_STAR_WANTED_VEHICLES_GOLD       10
CONST_INT PRESENCE_EVENT_UNLOCKED_5_STAR_WANTED_VEHICLES_PLATINUM   11

CONST_INT PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_BRONZE       12
CONST_INT PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_SILVER       13
CONST_INT PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_GOLD         14
CONST_INT PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_PLATINUM     15

CONST_INT PRESENCE_EVENT_UNLOCKED_NO_CRASH_DRIVE_BRONZE             16
CONST_INT PRESENCE_EVENT_UNLOCKED_NO_CRASH_DRIVE_SILVER             17
CONST_INT PRESENCE_EVENT_UNLOCKED_NO_CRASH_DRIVE_GOLD               18
CONST_INT PRESENCE_EVENT_UNLOCKED_NO_CRASH_DRIVE_PLATINUM           19

CONST_INT PRESENCE_EVENT_UNLOCKED_FLIPS_ONE_JUMP_BRONZE             20
CONST_INT PRESENCE_EVENT_UNLOCKED_FLIPS_ONE_JUMP_SILVER             21
CONST_INT PRESENCE_EVENT_UNLOCKED_FLIPS_ONE_JUMP_GOLD               22
CONST_INT PRESENCE_EVENT_UNLOCKED_FLIPS_ONE_JUMP_PLATINUM           23

CONST_INT PRESENCE_EVENT_UNLOCKED_SPINS_ONE_JUMP_BRONZE             24
CONST_INT PRESENCE_EVENT_UNLOCKED_SPINS_ONE_JUMP_SILVER             25
CONST_INT PRESENCE_EVENT_UNLOCKED_SPINS_ONE_JUMP_GOLD               26
CONST_INT PRESENCE_EVENT_UNLOCKED_SPINS_ONE_JUMP_PLATINUM           27

STRUCT STRUCT_MP_PRESENCE_EVENT_VARS
    
    INT iStage
    INT iCurrentProfile = -1
    
    INT iCurrent5StarWantedTime
    INT iCurrentMostFlipsOneJump
    INT iCurrentMostSpinsOneJump
    INT iCurrentNearMisses 
    INT iCurrentJackedVehicles
    
    FLOAT fCurrentLongestWheelieDist 
    FLOAT fCurrentLongestNoCrashDriveDist
    
    INT iUnlockedAwardsBitset
    
    SCRIPT_TIMER stProcessMpMessagesTimer
    
    INT iInboxStage
    UGCStateUpdate_Data sUgcStateUpdateData
    GET_UGC_CONTENT_STRUCT sGetUgcContentStruct
    
    BOOL bDisplayedAMessage
    
    INT iInboxProcessStage
    
ENDSTRUCT


// ---------------------------------------
//
// Steven Taylor Start
//
// Processes single player social events.
//
// ---------------------------------------


PROC AddEventToSP_Queue(STRUCT_GAME_PRESENCE_EVENT_STAT_UPDATE  nc_StatEventStruct, SP_MISSIONS Passed_eMission)

    INT TempIndex = 0

    
    WHILE TempIndex < MAX_ITEMS_IN_SP_EVENT_QUEUE

        IF SP_Queue_IsFull[TempIndex] = FALSE

            SP_Queue_UpdateEventStructs[TempIndex] = nc_StatEventStruct
            SP_Queue_MissionTypes[TempIndex] = Passed_eMission
            SP_Queue_IsFull[TempIndex] = TRUE

            
            i_NumberOfEventsInSP_Queue ++

            IF i_NumberOfEventsInSP_Queue > MAX_ITEMS_IN_SP_EVENT_QUEUE
                i_NumberOfEventsInSP_Queue  = MAX_ITEMS_IN_SP_EVENT_QUEUE
            ENDIF



            #if IS_DEBUG_BUILD

                PRINTSTRING ("SP_FEED - Not creating feed at present for this KEEP Presence Event entry as SP window is currently shut. Adding to queue at position ")
                PRINTINT (TempIndex)
                PRINTSTRING (" SP_FEED - Number of events in queue is ")
                PRINTINT (i_NumberOfEventsInSP_Queue)
                PRINTNL()

            #endif


            TempIndex  = MAX_ITEMS_IN_SP_EVENT_QUEUE //Breakout of this loop as soon as entry has found an open space.

        ENDIF

        TempIndex ++    

    ENDWHILE

ENDPROC










#if USE_CLF_DLC
PROC CreateFeedForMissionStatEnumCLF(STRUCT_GAME_PRESENCE_EVENT_STAT_UPDATE  nc_StatEventStruct, SP_MISSIONS Passed_eMission)
    
    IF IS_ACCOUNT_OVER_17()

        INT iLocalPlayerScore
        TEXT_LABEL_63 txtMissionName
        TEXT_LABEL_23 txtBlurbDecision
        //SP_MISSIONS eMission

        txtMissionName = GET_SP_MISSION_NAME_LABEL(Passed_eMission)
        txtBlurbDecision = "NO_BLURB_REQ"
//
//        SWITCH Passed_eMission
//
//            CASE SP_MISSION_PROLOGUE
//                txtBlurbDecision = "CELL_F_PROLOGUE"  //doesn't appear on self machine?
//            BREAK
//
//        ENDSWITCH


        i_SP_Queue_Window_CurrentTime = GET_GAME_TIMER() 

        IF (i_SP_Queue_Window_CurrentTime - i_SP_Queue_Window_StartTime) > i_SP_QueueWindow_ShutTime  //The Start Time is initialised in cellphone_controller as it is used in MP and SP.

    

            IF NOT ARE_STRINGS_EQUAL (txtBlurbDecision, "NO_BLURB_REQ")
                
                BEGIN_TEXT_COMMAND_THEFEED_POST (txtBlurbDecision)

                    TEXT_LABEL_63 s_MPGamerTagTempHolder

                    //Grab player name and apply prefix and suffix for displaying the sender name with the condensed font.
                
                    s_MPGamerTagTempHolder = "<C>"
                    s_MPGamerTagTempHolder += "~HUD_COLOUR_SOCIAL_CLUB~"
                    s_MPGamerTagTempHolder += nc_StatEventStruct.fromGamer
                    s_MPGamerTagTempHolder += "..."
                    s_MPGamerTagTempHolder += "</C>"

                END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, s_MPGamerTagTempHolder)
                                

            ELSE  //Only force beaten score notificiations for missions without blurb.
      
                //If the player has completed this mission and his local score has been beaten fire off a feed entry notification. See bug 1010990

                IF GET_MISSION_COMPLETE_STATE (Passed_eMission) //Local player must have completed this mission. Now compare score of friend and local player.

                    iLocalPlayerScore = GET_MISSION_OVERALL_SCORE(Passed_eMission)

                    IF nc_StatEventStruct.iValue  > iLocalPlayerScore
                    //Comment in an ENDIF here for easy testing.
                      
                        BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_FEED_BEAT_MIS_SCORE")

                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL (txtMissionName) //Need to extract this from the stat enum somehow.
                                         
                     
                            TEXT_LABEL_63 s_MPGamerTagTempHolder
                                        //Grab player name and apply prefix and suffix for displaying the sender name with the condensed font.
                            s_MPGamerTagTempHolder = "<C>"
                            s_MPGamerTagTempHolder += "~HUD_COLOUR_SOCIAL_CLUB~"
                            s_MPGamerTagTempHolder += nc_StatEventStruct.fromGamer
                            s_MPGamerTagTempHolder += "..."
                            s_MPGamerTagTempHolder += "</C>"

                        END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, s_MPGamerTagTempHolder)

                    ENDIF //Comment out this ENDIF and put one above for easy testing.
                ENDIF
            ENDIF


            i_SP_Queue_Window_StartTime = GET_GAME_TIMER() //Vital that we reset this so as to shut the Presence Event window again.

            #if IS_DEBUG_BUILD
                PRINTSTRING ("SP_FEED - Feed window was open for this KEEP Presence Event, now shutting.")
                PRINTNL()
            #endif

        ELSE
            AddEventToSP_Queue(nc_StatEventStruct, Passed_eMission)
        ENDIF
    ENDIF

ENDPROC
#endif
#if USE_NRM_DLC
PROC CreateFeedForMissionStatEnumNRM(STRUCT_GAME_PRESENCE_EVENT_STAT_UPDATE  nc_StatEventStruct, SP_MISSIONS Passed_eMission)
    

    IF IS_ACCOUNT_OVER_17()

        INT iLocalPlayerScore
        TEXT_LABEL_63 txtMissionName
        TEXT_LABEL_23 txtBlurbDecision

        txtMissionName = GET_SP_MISSION_NAME_LABEL(Passed_eMission)
        txtBlurbDecision = "NO_BLURB_REQ"
//
//        SWITCH Passed_eMission
//
//            CASE SP_MISSION_PROLOGUE
//                txtBlurbDecision = "CELL_F_PROLOGUE"  //doesn't appear on self machine?
//            BREAK
//
//        ENDSWITCH


        i_SP_Queue_Window_CurrentTime = GET_GAME_TIMER() 

        IF (i_SP_Queue_Window_CurrentTime - i_SP_Queue_Window_StartTime) > i_SP_QueueWindow_ShutTime  //The Start Time is initialised in cellphone_controller as it is used in MP and SP.

    

            IF NOT ARE_STRINGS_EQUAL (txtBlurbDecision, "NO_BLURB_REQ")
                
                BEGIN_TEXT_COMMAND_THEFEED_POST (txtBlurbDecision)

                    TEXT_LABEL_63 s_MPGamerTagTempHolder

                    //Grab player name and apply prefix and suffix for displaying the sender name with the condensed font.
                
                    s_MPGamerTagTempHolder = "<C>"
                    s_MPGamerTagTempHolder += "~HUD_COLOUR_SOCIAL_CLUB~"
                    s_MPGamerTagTempHolder += nc_StatEventStruct.fromGamer
                    s_MPGamerTagTempHolder += "..."
                    s_MPGamerTagTempHolder += "</C>"

                END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, s_MPGamerTagTempHolder)
                                

            ELSE  //Only force beaten score notificiations for missions without blurb.
      
                //If the player has completed this mission and his local score has been beaten fire off a feed entry notification. See bug 1010990

                IF GET_MISSION_COMPLETE_STATE (Passed_eMission) //Local player must have completed this mission. Now compare score of friend and local player.

                    iLocalPlayerScore = GET_MISSION_OVERALL_SCORE(Passed_eMission)

                    IF nc_StatEventStruct.iValue  > iLocalPlayerScore
                    //Comment in an ENDIF here for easy testing.
                      
                        BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_FEED_BEAT_MIS_SCORE")

                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL (txtMissionName) //Need to extract this from the stat enum somehow.
                                         
                     
                            TEXT_LABEL_63 s_MPGamerTagTempHolder
                                        //Grab player name and apply prefix and suffix for displaying the sender name with the condensed font.
                            s_MPGamerTagTempHolder = "<C>"
                            s_MPGamerTagTempHolder += "~HUD_COLOUR_SOCIAL_CLUB~"
                            s_MPGamerTagTempHolder += nc_StatEventStruct.fromGamer
                            s_MPGamerTagTempHolder += "..."
                            s_MPGamerTagTempHolder += "</C>"

                        END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, s_MPGamerTagTempHolder)

                    ENDIF //Comment out this ENDIF and put one above for easy testing.
                ENDIF
            ENDIF


            i_SP_Queue_Window_StartTime = GET_GAME_TIMER() //Vital that we reset this so as to shut the Presence Event window again.

            #if IS_DEBUG_BUILD
                PRINTSTRING ("SP_FEED - Feed window was open for this KEEP Presence Event, now shutting.")
                PRINTNL()
            #endif

        ELSE
            AddEventToSP_Queue(nc_StatEventStruct, Passed_eMission)
        ENDIF
    ENDIF

ENDPROC
#endif
PROC CreateFeedForMissionStatEnum(STRUCT_GAME_PRESENCE_EVENT_STAT_UPDATE  nc_StatEventStruct, SP_MISSIONS Passed_eMission)
#if USE_CLF_DLC
    CreateFeedForMissionStatEnumCLF(nc_StatEventStruct,Passed_eMission)
#endif
#if USE_NRM_DLC
    CreateFeedForMissionStatEnumNRM(nc_StatEventStruct,Passed_eMission)
#endif
#if not USE_CLF_DLC
#if not USE_NRM_DLC

    IF IS_ACCOUNT_OVER_17()

        INT iLocalPlayerScore
        TEXT_LABEL_63 txtMissionName
        TEXT_LABEL_23 txtBlurbDecision
        //SP_MISSIONS eMission

        txtMissionName = GET_SP_MISSION_NAME_LABEL(Passed_eMission)
        txtBlurbDecision = "NO_BLURB_REQ"

        SWITCH Passed_eMission

            CASE SP_MISSION_PROLOGUE
                txtBlurbDecision = "CELL_F_PROLOGUE"  //doesn't appear on self machine?
            BREAK

            CASE SP_MISSION_LESTER_1
                txtBlurbDecision = "CELL_F_LESTER1"   //checked.
            BREAK

            CASE SP_MISSION_FAMILY_4
                txtBlurbDecision = "CELL_F_FAMILY4"   //checked.   
            BREAK

            CASE SP_HEIST_DOCKS_2B
                txtBlurbDecision = "CELL_F_DOCKH2B"   //checked.
            BREAK

            CASE SP_MISSION_FBI_4
                txtBlurbDecision = "CELL_F_FIB4"      //checked.
            BREAK
            
            CASE SP_HEIST_RURAL_2
                txtBlurbDecision = "CELL_F_RURALH2"   //checked.
            BREAK

            CASE SP_HEIST_AGENCY_3A
                txtBlurbDecision = "CELL_F_AGENCYH3A"
            BREAK

            CASE SP_HEIST_AGENCY_3B
                txtBlurbDecision = "CELL_F_AGENCYH3B"  //checked.
            BREAK

            CASE SP_MISSION_MICHAEL_2
                txtBlurbDecision = "CELL_F_MICHAEL2"   //checked.
            BREAK

            CASE SP_MISSION_FRANKLIN_2
                txtBlurbDecision = "CELL_F_FRANKLIN2"  //mission wouldn't load!
            BREAK

            CASE SP_HEIST_FINALE_2A
                txtBlurbDecision = "CELL_F_FINALEH2A"
            BREAK

            CASE SP_HEIST_FINALE_2B
                txtBlurbDecision = "CELL_F_FINALEH2B"
            BREAK

            CASE SP_MISSION_FINALE_A
                txtBlurbDecision = "CELL_F_ENDCHOICE"
            BREAK

            CASE SP_MISSION_FINALE_B
                txtBlurbDecision = "CELL_F_ENDCHOICE"
            BREAK

            CASE SP_MISSION_FINALE_C2
                txtBlurbDecision = "CELL_F_ENDCHOICE"
            BREAK

        ENDSWITCH


        i_SP_Queue_Window_CurrentTime = GET_GAME_TIMER() 

        IF (i_SP_Queue_Window_CurrentTime - i_SP_Queue_Window_StartTime) > i_SP_QueueWindow_ShutTime  //The Start Time is initialised in cellphone_controller as it is used in MP and SP.

    

            IF NOT ARE_STRINGS_EQUAL (txtBlurbDecision, "NO_BLURB_REQ")
                
                BEGIN_TEXT_COMMAND_THEFEED_POST (txtBlurbDecision)

                    TEXT_LABEL_63 s_MPGamerTagTempHolder

                    //Grab player name and apply prefix and suffix for displaying the sender name with the condensed font.
                
                    s_MPGamerTagTempHolder = "<C>"
                    s_MPGamerTagTempHolder += "~HUD_COLOUR_SOCIAL_CLUB~"
                    s_MPGamerTagTempHolder += nc_StatEventStruct.fromGamer
                    s_MPGamerTagTempHolder += "..."
                    s_MPGamerTagTempHolder += "</C>"

                END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, s_MPGamerTagTempHolder)
                                

            ELSE  //Only force beaten score notificiations for missions without blurb.
      
                //If the player has completed this mission and his local score has been beaten fire off a feed entry notification. See bug 1010990

                IF GET_MISSION_COMPLETE_STATE (Passed_eMission) //Local player must have completed this mission. Now compare score of friend and local player.

                    iLocalPlayerScore = GET_MISSION_OVERALL_SCORE(Passed_eMission)

                    IF nc_StatEventStruct.iValue  > iLocalPlayerScore
                    //Comment in an ENDIF here for easy testing.
                      
                        BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_FEED_BEAT_MIS_SCORE")

                            ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL (txtMissionName) //Need to extract this from the stat enum somehow.
                                         
                     
                            TEXT_LABEL_63 s_MPGamerTagTempHolder
                                        //Grab player name and apply prefix and suffix for displaying the sender name with the condensed font.
                            s_MPGamerTagTempHolder = "<C>"
                            s_MPGamerTagTempHolder += "~HUD_COLOUR_SOCIAL_CLUB~"
                            s_MPGamerTagTempHolder += nc_StatEventStruct.fromGamer
                            s_MPGamerTagTempHolder += "..."
                            s_MPGamerTagTempHolder += "</C>"

                        END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, s_MPGamerTagTempHolder)

                    ENDIF //Comment out this ENDIF and put one above for easy testing.
                ENDIF
            ENDIF


            i_SP_Queue_Window_StartTime = GET_GAME_TIMER() //Vital that we reset this so as to shut the Presence Event window again.

            #if IS_DEBUG_BUILD
                PRINTSTRING ("SP_FEED - Feed window was open for this KEEP Presence Event, now shutting.")
                PRINTNL()
            #endif

        ELSE
            AddEventToSP_Queue(nc_StatEventStruct, Passed_eMission)
        ENDIF
    ENDIF
#endif
#endif
ENDPROC



PROC CreateFeedForGameEventUpdate(STRUCT_GAME_PRESENCE_EVENT_STAT_UPDATE  nc_StatEventStruct, STRING PassedTextLabel)

    IF (GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) = 1) //Pause Menu profile allows friend updates. Create Feed entry in that case.

        IF IS_ACCOUNT_OVER_17()


         

            i_SP_Queue_Window_CurrentTime = GET_GAME_TIMER() 

            IF (i_SP_Queue_Window_CurrentTime - i_SP_Queue_Window_StartTime) > i_SP_QueueWindow_ShutTime  //The Start Time is initialised in cellphone_controller as it is used in MP and SP.
            //IF (i_SP_Queue_Window_CurrentTime - i_SP_Queue_Window_StartTime) > 0 //DEBUG ONLY!

                #if IS_DEBUG_BUILD

                    PRINTSTRING ("Creating feed entry for PRESENCE EVENT with text key ")
                    PRINTSTRING (PassedTextLabel)
                    PRINTSTRING (" which is: ")
                    PRINTSTRING(GET_FILENAME_FOR_AUDIO_CONVERSATION(PassedTextLabel))
                    PRINTNL()

                #endif



                BEGIN_TEXT_COMMAND_THEFEED_POST (PassedTextLabel)

                    TEXT_LABEL_63 s_MPGamerTagTempHolder

                    //Grab player name and apply prefix and suffix for displaying the sender name with the condensed font.
                
                    s_MPGamerTagTempHolder = "<C>"
                    s_MPGamerTagTempHolder += "~HUD_COLOUR_SOCIAL_CLUB~"
                    s_MPGamerTagTempHolder += nc_StatEventStruct.fromGamer
                    s_MPGamerTagTempHolder += "..."
                    s_MPGamerTagTempHolder += "</C>"

                END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, s_MPGamerTagTempHolder)

            
                i_SP_Queue_Window_StartTime = GET_GAME_TIMER() //Vital that we reset this so as to shut the Presence Event window again.


                #if IS_DEBUG_BUILD

                    PRINTSTRING ("SP_FEED - Feed window was open for this ONESHOT Presence Event, now shutting.")
                    PRINTNL()

                #endif


            ELSE


                #if IS_DEBUG_BUILD

                    PRINTSTRING ("SP_FEED - Not creating feed for this ONESHOT Presence Event entry as SP window is currently shut. Discarding unimportant entry.")
                    PRINTNL()

                #endif


            ENDIF
            
        ENDIF

    ENDIF

ENDPROC






PROC Common_SP_Stat_Processor(STRUCT_GAME_PRESENCE_EVENT_STAT_UPDATE  nc_StatEventStruct, SP_MISSIONS eMission )

       
        
        eMission = GET_SP_MISSION_FROM_COMPLETION_ORDER_STAT_HASH(nc_StatEventStruct.statHash)
        
        
        IF eMission != SP_MISSION_NONE
            
            IF (GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) = 1) //Pause Menu profile allows friend updates. Create Feed entry in that case.
                
                IF IS_ACCOUNT_OVER_17()
   
                    CreateFeedForMissionStatEnum(nc_StatEventStruct, eMission)  //If the statHash doesn't relate to a mission then we know this must be an event related to another stat.
                
                ENDIF

            ENDIF

        ELSE

            SWITCH (INT_TO_ENUM(STATSENUM, nc_StatEventStruct.statHash))

                                                                    
                CASE TOTAL_PROGRESS_MADE  //Keep


                    #if IS_DEBUG_BUILD 

                        PRINTSTRING("SOCIAL_FEED_CONTROLLER - Profile Feed Friends setting returns ")
                        PRINTINT(GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS))
                        PRINTNL()

                    #endif

                    IF (GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) = 1) //Pause Menu profile allows friend updates. Create Feed entry in that case.

                        IF IS_ACCOUNT_OVER_17()


                            

                            //Need to tie this one into the queue!
                            BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_FEED_F100PC_COMP")

                           
                                TEXT_LABEL_63 s_MPGamerTagTempHolder

                                //Grab player name and apply prefix and suffix for displaying the sender name with the condensed font.
                            
                                s_MPGamerTagTempHolder = "<C>"
                                s_MPGamerTagTempHolder += "~HUD_COLOUR_SOCIAL_CLUB~"
                                s_MPGamerTagTempHolder += nc_StatEventStruct.fromGamer
                                s_MPGamerTagTempHolder += "..."
                                s_MPGamerTagTempHolder += "</C>"

                            END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, s_MPGamerTagTempHolder)

                         
                            i_SP_Queue_Window_StartTime = GET_GAME_TIMER() //Vital that we reset this so as to shut the Presence Event window again.

                           
                        ENDIF

                    ENDIF   
                    
                BREAK






                CASE SP_VEHICLE_MODELS_DRIVEN  //Keep

                    IF nc_StatEventStruct.iValue = 200

                        i_SP_Queue_Window_CurrentTime = GET_GAME_TIMER() 

                        IF (i_SP_Queue_Window_CurrentTime - i_SP_Queue_Window_StartTime) > i_SP_QueueWindow_ShutTime

                            #if IS_DEBUG_BUILD 

                                PRINTSTRING("SOCIAL_FEED_CONTROLLER - Profile Feed Friends setting returns ")
                                PRINTINT(GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS))
                                PRINTNL()

                            #endif

                            IF (GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) = 1) //Pause Menu profile allows friend updates. Create Feed entry in that case.

                                IF IS_ACCOUNT_OVER_17()


                                    

                                    //Need to tie this one into the queue!
                                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_FEED_DRIVEN_ALL_VEH")

                                   
                                        TEXT_LABEL_63 s_MPGamerTagTempHolder

                                        //Grab player name and apply prefix and suffix for displaying the sender name with the condensed font.
                                    
                                        s_MPGamerTagTempHolder = "<C>"
                                        s_MPGamerTagTempHolder += "~HUD_COLOUR_SOCIAL_CLUB~"
                                        s_MPGamerTagTempHolder += nc_StatEventStruct.fromGamer
                                        s_MPGamerTagTempHolder += "..."
                                        s_MPGamerTagTempHolder += "</C>"

                                    END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, s_MPGamerTagTempHolder)

                                    i_SP_Queue_Window_StartTime = GET_GAME_TIMER() //Vital that we reset this so as to shut the Presence Event window again.

                                ENDIF

                            ENDIF   
                    
     
                        ELSE

                            AddEventToSP_Queue(nc_StatEventStruct, SP_MISSION_NONE)

                        ENDIF


                    ENDIF

                BREAK







              



                CASE NUM_RNDEVENTS_COMPLETED //Needs added to queue.




                    #if IS_DEBUG_BUILD 

                        PRINTSTRING("SOCIAL_FEED_CONTROLLER - Profile Feed Friends setting returns ")
                        PRINTINT(GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS))
                        PRINTNL()

                    #endif


                        i_SP_Queue_Window_CurrentTime = GET_GAME_TIMER() 

                        IF (i_SP_Queue_Window_CurrentTime - i_SP_Queue_Window_StartTime) > i_SP_QueueWindow_ShutTime


                            IF (GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) = 1) //Pause Menu profile allows friend updates. Create Feed entry in that case.

                                IF IS_ACCOUNT_OVER_17()


                                    

                                    //Need to tie this one into the queue!
                                    BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_FEED_RNDEV_COMP")

                                   
                                        TEXT_LABEL_63 s_MPGamerTagTempHolder

                                        //Grab player name and apply prefix and suffix for displaying the sender name with the condensed font.
                                    
                                        s_MPGamerTagTempHolder = "<C>"
                                        s_MPGamerTagTempHolder += "~HUD_COLOUR_SOCIAL_CLUB~"
                                        s_MPGamerTagTempHolder += nc_StatEventStruct.fromGamer
                                        s_MPGamerTagTempHolder += "..."
                                        s_MPGamerTagTempHolder += "</C>"

                                    END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT ("CHAR_SOCIAL_CLUB", "CHAR_SOCIAL_CLUB", FALSE, TEXT_ICON_BLANK, s_MPGamerTagTempHolder)

                                    i_SP_Queue_Window_StartTime = GET_GAME_TIMER() //Vital that we reset this so as to shut the Presence Event window again.


                                ENDIF

                            ENDIF  
                            
                        ELSE

                            AddEventToSP_Queue(nc_StatEventStruct, SP_MISSION_NONE)

                        ENDIF
                         
                    
                BREAK

















                CASE NUM_HIDDEN_PACKAGES_0


                      #if IS_DEBUG_BUILD

                        PRINTSTRING ("PRESENCE EVENT RECEIVED - NUM_HIDDEN_PACKAGES_0")
                        PRINTNL()

                    #endif
                   

                    //Make sure to use either iValue or fValue according to the stat type getting set.

                    IF nc_StatEventStruct.iValue = 50
                       CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_LETTER_SCRAPS")
                    ENDIF


                BREAK



                CASE NUM_HIDDEN_PACKAGES_1


                      #if IS_DEBUG_BUILD

                        PRINTSTRING ("PRESENCE EVENT RECEIVED - NUM_HIDDEN_PACKAGES_1")
                        PRINTNL()

                    #endif
                   

                    //Make sure to use either iValue or fValue according to the stat type getting set.

                    IF nc_StatEventStruct.iValue = 50
                       CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_SPACESHIP_PARTS")
                    ENDIF


                BREAK




                CASE NUM_HIDDEN_PACKAGES_3


                      #if IS_DEBUG_BUILD

                        PRINTSTRING ("PRESENCE EVENT RECEIVED - NUM_HIDDEN_PACKAGES_3")
                        PRINTNL()

                    #endif
                   

                    //Make sure to use either iValue or fValue according to the stat type getting set.

                    IF nc_StatEventStruct.iValue = 50
                       CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_SONAR_COLL")
                    ENDIF


                BREAK







                CASE SP0_DIST_DRIVING_CAR //Actually counts for driving in any road vehicle by any player combined.                              

                    #if IS_DEBUG_BUILD

                        PRINTSTRING ("PRESENCE EVENT RECEIVED - SP0_DIST_DRIVING_CAR")
                        PRINTNL()

                    #endif
                   

                    //Make sure to use either iValue or fValue according to the stat type getting set.

                    IF nc_StatEventStruct.fValue = 500 
                       CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_DRIVEN_500")
                    ENDIF
                    
                    IF nc_StatEventStruct.fValue = 5000 
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_DRIVEN_5000")
                    ENDIF

                    IF nc_StatEventStruct.fValue = 50000 
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_DRIVEN_50000")
                    ENDIF


                BREAK




                CASE SP0_DIST_DRIVING_PLANE //Actually counts for flying in any air vehicle by any player combined.                              

                    #if IS_DEBUG_BUILD

                        PRINTSTRING ("PRESENCE EVENT RECEIVED - SP0_DIST_DRIVING_PLANE")
                        PRINTNL()

                    #endif
                   

                    //Make sure to use either iValue or fValue according to the stat type getting set.

                    IF nc_StatEventStruct.fValue = 500 
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_FLOWN_500")
                    ENDIF
                    
                    IF nc_StatEventStruct.fValue = 5000 
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_FLOWN_5000")
                    ENDIF

                    IF nc_StatEventStruct.fValue = 50000 
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_FLOWN_50000")
                    ENDIF


                BREAK



                CASE SP0_DIST_RUNNING //Running only, not walking.                              

                    #if IS_DEBUG_BUILD

                        PRINTSTRING ("PRESENCE EVENT RECEIVED - SP0_DIST_RUNNING")
                        PRINTNL()

                    #endif
                   

                    //Make sure to use either iValue or fValue according to the stat type getting set.

                    IF nc_StatEventStruct.fValue = 50 
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_RAN_50")
                    ENDIF
                    
                    IF nc_StatEventStruct.fValue = 100 
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_RAN_100")
                    ENDIF

                    IF nc_StatEventStruct.fValue = 1000 
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_RAN_1000")
                    ENDIF


                BREAK
                         


                CASE SP0_DIST_SWIMMING //Swimming                              

                    #if IS_DEBUG_BUILD

                        PRINTSTRING ("PRESENCE EVENT RECEIVED - SP0_DIST_SWIMMING")
                        PRINTNL()

                    #endif
                   

                    //Make sure to use either iValue or fValue according to the stat type getting set.

                    IF nc_StatEventStruct.fValue = 50 
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_SWAM_50")
                    ENDIF
                    
                    IF nc_StatEventStruct.fValue = 100 
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_SWAM_100")
                    ENDIF

                    IF nc_StatEventStruct.fValue = 1000 
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_SWAM_1000")
                    ENDIF


                BREAK
                       





                CASE SP0_BUSTED

                    #if IS_DEBUG_BUILD

                        PRINTSTRING ("PRESENCE EVENT RECEIVED - SP0_BUSTED")
                        PRINTNL()

                    #endif
                   

                    //Make sure to use either iValue or fValue according to the stat type getting set.

                    IF nc_StatEventStruct.iValue = 10
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_BUSTED_10")
                    ENDIF

                    IF nc_StatEventStruct.iValue = 25
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_BUSTED_25")
                    ENDIF

                    IF nc_StatEventStruct.iValue = 50
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_BUSTED_50")
                    ENDIF

                    IF nc_StatEventStruct.iValue = 100
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_BUSTED_100")
                    ENDIF

                    IF nc_StatEventStruct.iValue = 250
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_BUSTED_250")
                    ENDIF


                BREAK





                CASE SP0_DEATHS

                    #if IS_DEBUG_BUILD

                        PRINTSTRING ("PRESENCE EVENT RECEIVED - SP0_DEATHS")
                        PRINTNL()

                    #endif
                   

                    //Make sure to use either iValue or fValue according to the stat type getting set.

                    IF nc_StatEventStruct.iValue = 10
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_WASTED_10")
                    ENDIF

                    IF nc_StatEventStruct.iValue = 25
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_WASTED_25")
                    ENDIF

                    IF nc_StatEventStruct.iValue = 50
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_WASTED_50")
                    ENDIF

                    IF nc_StatEventStruct.iValue = 100
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_WASTED_100")
                    ENDIF

                    IF nc_StatEventStruct.iValue = 250
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_WASTED_250")
                    ENDIF


                BREAK




                CASE SP0_SHOTS

                    #if IS_DEBUG_BUILD

                        PRINTSTRING ("PRESENCE EVENT RECEIVED - SP0_SHOTS")
                        PRINTNL()

                    #endif
                   

                    //Make sure to use either iValue or fValue according to the stat type getting set.

                    IF nc_StatEventStruct.iValue = 1000000
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_SHOTS_1ML")
                    ENDIF

                    IF nc_StatEventStruct.iValue = 2000000
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_SHOTS_2ML")
                    ENDIF

                    IF nc_StatEventStruct.iValue = 3000000
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_SHOTS_3ML")
                    ENDIF

                    IF nc_StatEventStruct.iValue = 4000000
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_SHOTS_4ML")
                    ENDIF

                    IF nc_StatEventStruct.iValue = 5000000
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_SHOTS_5ML")
                    ENDIF


                BREAK



                CASE SP0_STARS_EVADED

                    #if IS_DEBUG_BUILD

                        PRINTSTRING ("PRESENCE EVENT RECEIVED - SP0_STARS_EVADED")
                        PRINTNL()

                    #endif


                    IF nc_StatEventStruct.iValue= 5
                        CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_5STARS_WL")
                    ENDIF


                BREAK




                

                CASE SP0_CROUCHED   //Using this unused stat as the unique id for web purchases, to avoid adding another stat.

                    
                     #if IS_DEBUG_BUILD

                        PRINTSTRING ("PRESENCE EVENT RECEIVED - WEB PURCHASE")
                        PRINTNL()

                    #endif


                    
                    IF nc_StatEventStruct.iValue= 10
                        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_WEB_BOUGHT_CAR)
                            CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_WEB_CAR")
                            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_WEB_BOUGHT_CAR)
                        ENDIF
                    ENDIF


                    IF nc_StatEventStruct.iValue= 20
                        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_WEB_BOUGHT_RHINO)
                            CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_WEB_RHINO")
                            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_WEB_BOUGHT_RHINO)
                        ENDIF
                    ENDIF


                    IF nc_StatEventStruct.iValue= 30
                        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_WEB_BOUGHT_BUZZARD)
                            CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_WEB_BUZZARD")
                            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_WEB_BOUGHT_BUZZARD)
                        ENDIF
                    ENDIF


                BREAK





                CASE SP0_CROUCHED_AND_SHOT   //Using this unused stat as the unique id for stock market activity, to avoid adding another stat.

                    
                     #if IS_DEBUG_BUILD

                        PRINTSTRING ("PRESENCE EVENT RECEIVED - STOCK MARKET ACTIVITY")
                        PRINTNL()

                    #endif


                    
                    IF nc_StatEventStruct.iValue= 10
                        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_STOCK_BIG_INVESTMENT)
                            CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_STOCK_INV")
                            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_STOCK_BIG_INVESTMENT)
                        ENDIF
                    ENDIF


                    IF nc_StatEventStruct.iValue= 20
                        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_STOCK_BIG_WIN)
                            CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_STOCK_WIN")
                            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_STOCK_BIG_WIN)
                        ENDIF
                    ENDIF


                    IF nc_StatEventStruct.iValue= 30
                        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_STOCK_BIG_LOSS)
                            CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_STOCK_LOSS")
                            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_STOCK_BIG_LOSS)
                        ENDIF
                    ENDIF


                 BREAK





                 CASE SP0_WATER_CANNON_KILLS   //Using this unused stat as the unique id for Stunt Jumps, to avoid adding another stat.

                    
                     #if IS_DEBUG_BUILD

                        PRINTSTRING ("PRESENCE EVENT RECEIVED - Stunt Jumps")
                        PRINTNL()

                    #endif


                    
                    IF nc_StatEventStruct.iValue = 25
                        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_STUNTJUMPS_25PC)
                            CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_STNJ_25")
                            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_STUNTJUMPS_25PC)
                        ENDIF
                    ENDIF


                    IF nc_StatEventStruct.iValue = 50
                        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_STUNTJUMPS_50PC)
                            CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_STNJ_50")
                            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_STUNTJUMPS_50PC)
                        ENDIF
                    ENDIF

                    
                    IF nc_StatEventStruct.iValue = 75
                        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_STUNTJUMPS_75PC)
                            CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_STNJ_75")
                            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_STUNTJUMPS_75PC)
                        ENDIF
                    ENDIF


                    IF nc_StatEventStruct.iValue = 100
                        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_STUNTJUMPS_100PC)
                            CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_STNJ_100")
                            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_STUNTJUMPS_100PC)
                        ENDIF
                    ENDIF



                 BREAK



                    
                 CASE SP0_WATER_CANNON_DEATHS   //Using this unused stat as the unique id for Under The Bridges, to avoid adding another stat.

                    
                     #if IS_DEBUG_BUILD

                        PRINTSTRING ("PRESENCE EVENT RECEIVED - Under The Bridges")
                        PRINTNL()

                    #endif


                    
                    IF nc_StatEventStruct.iValue = 25
                        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_UNDERTHEBRIDGE_25PC)
                            CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_UTB_25")
                            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_UNDERTHEBRIDGE_25PC)
                        ENDIF
                    ENDIF


                    IF nc_StatEventStruct.iValue = 50
                        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_UNDERTHEBRIDGE_50PC)
                            CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_UTB_50")
                            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_UNDERTHEBRIDGE_50PC)
                        ENDIF
                    ENDIF

                    
                    IF nc_StatEventStruct.iValue = 75
                        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_UNDERTHEBRIDGE_75PC)
                            CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_UTB_75")
                            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_UNDERTHEBRIDGE_75PC)
                        ENDIF
                    ENDIF


                    IF nc_StatEventStruct.iValue = 100
                        IF NOT IS_BIT_SET (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_UNDERTHEBRIDGE_100PC)
                            CreateFeedForGameEventUpdate(nc_StatEventStruct, "CELL_FEED_UTB_100")
                            SET_BIT (g_savedGlobals.sCompletionPercentageData.g_BitSet_SP_EventFeedEntryTracker_2, g_BS2_SP_Event_UNDERTHEBRIDGE_100PC)
                        ENDIF
                    ENDIF



                 BREAK





            ENDSWITCH

        ENDIF

ENDPROC




PROC Process_SP_Queued_Stat_Update()


    i_SP_Queue_Window_CurrentTime = GET_GAME_TIMER() 

    IF (i_SP_Queue_Window_CurrentTime - i_SP_Queue_Window_StartTime) > i_SP_QueueWindow_ShutTime
    
        //Check for any queued updates now that window is open.
    
        #if IS_DEBUG_BUILD

            PRINTSTRING("SocialFeed_Controller - Performing runthrough of queued SP events. ")
            PRINTNL()

        #endif



        INT TempIndex = 0

        WHILE TempIndex < MAX_ITEMS_IN_SP_EVENT_QUEUE

            IF SP_Queue_IsFull[TempIndex] = TRUE 

                i_NumberOfEventsInSP_Queue --

                IF i_NumberOfEventsInSP_Queue < 0
                    i_NumberOfEventsInSP_Queue = 0 
                ENDIF


                #if IS_DEBUG_BUILD

                    PRINTSTRING("SocialFeed_Controller - Process_SP_Queued_StatUpdate found an unprocessed entry at ")
                    PRINTINT(TempIndex)
                    PRINTNL()
                    PRINTSTRING ("SP_FEED - Number of events in queue is ")
                    PRINTINT (i_NumberOfEventsInSP_Queue)
                    PRINTNL()

                #endif
            
                SP_Queue_IsFull[TempIndex] = FALSE

                Common_SP_Stat_Processor(SP_Queue_UpdateEventStructs[TempIndex], SP_Queue_MissionTypes[Tempindex])

                TempIndex = MAX_ITEMS_IN_SP_EVENT_QUEUE //Break out of while after firing off an event feed entry from queue.

                i_SP_Queue_Window_StartTime = GET_GAME_TIMER() //Vital that we reset this so as to shut the Presence Event window again so we only process one event in the queue per window opening.
 
         
            ENDIF

            TempIndex ++
        
        ENDWHILE

          
    ENDIF


ENDPROC




PROC Process_SP_Stat_Update(INT Passed_iCount)
    
    

    STRUCT_GAME_PRESENCE_EVENT_STAT_UPDATE  nc_StatEventStructToPass
    SP_MISSIONS                             eMissionToPass
    


    
    IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, Passed_iCount, nc_StatEventStructToPass, SIZE_OF(nc_StatEventStructToPass))
    
 
        #if IS_DEBUG_BUILD

            PRINTSTRING("SocialFeed_Controller - Process_SP_Stat_Update has grabbed stat update event data at index: ")
            PRINTINT(Passed_iCount)
            PRINTNL()

        #endif
        
        Common_SP_Stat_Processor(nc_StatEventStructToPass, eMissionToPass)
                
    ELSE

        SCRIPT_ASSERT("Social_feed_controller header - Process_SP_Stat_Update could not receive event data.")

    ENDIF
    




ENDPROC

// -------------------------
// Steven Taylor End
// -------------------------












FUNC INT GET_FEED_DELAY_VALUE()
    
    SWITCH GET_PROFILE_SETTING(PROFILE_FEED_DELAY)
        CASE 0 RETURN 0
        
        CASE 1  RETURN 60000
        
        CASE 2  RETURN 120000
        
        CASE 3  RETURN 180000
        
        CASE 4  RETURN 240000
        
        CASE 5  RETURN 300000
        
        CASE 6  RETURN 600000
        
        CASE 7  RETURN 900000
        
        CASE 8  RETURN 1800000
        
        CASE 9  RETURN 3600000 
    
    ENDSWITCH
    
    RETURN 300000
    
ENDFUNC


















PROC MAINTAIN_SEND_MP_PRESENCE_EVENTS(STRUCT_MP_PRESENCE_EVENT_VARS &socialFeedVars)
    
    IF NOT NETWORK_IS_GAME_IN_PROGRESS()
        socialFeedVars.iStage = 0
        EXIT
    ENDIF
    
    IF IS_PAUSE_MENU_ACTIVE()
        EXIT
    ENDIF
    
    IF NOT IS_SCREEN_FADED_IN()
        EXIT
    ENDIF
    
    IF socialFeedVars.iCurrentProfile != GET_ACTIVE_CHARACTER_SLOT()
        socialFeedVars.iStage = 0
        PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - changed mp character slot. Resetting maintain send presence events stage to 0.")
    ENDIF
    
    SWITCH socialFeedVars.iStage
        
        CASE 0
            
            IF socialFeedVars.iCurrentProfile != GET_ACTIVE_CHARACTER_SLOT()
                
                // Do initial setup of stored stats.
                socialFeedVars.iCurrent5StarWantedTime = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_WANTED_LEVEL_TIME5STAR)
                socialFeedVars.iCurrentMostFlipsOneJump = GET_MP_INT_CHARACTER_STAT(MP_STAT_MOST_FLIPS_IN_ONE_JUMP)
                socialFeedVars.iCurrentMostSpinsOneJump = GET_MP_INT_CHARACTER_STAT(MP_STAT_MOST_SPINS_IN_ONE_JUMP)
                socialFeedVars.iCurrentNearMisses  = GET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_NEAR_MISS)
                socialFeedVars.iCurrentJackedVehicles = GET_MP_INT_CHARACTER_AWARD(MP_AWARD_VEHICLES_JACKEDR)
                
                socialFeedVars.fCurrentLongestWheelieDist  = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_LONGEST_WHEELIE_DIST)
                socialFeedVars.fCurrentLongestNoCrashDriveDist = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_LONGEST_DRIVE_NOCRASH)
                
                IF IS_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(MP_AWARD_50_VEHICLES_BLOWNUP)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_BLOWN_UP_BRONZE)
                ENDIF
                IF IS_MP_AWARD_SILVER_INTCHAR_UNLOCKED(MP_AWARD_50_VEHICLES_BLOWNUP)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_BLOWN_UP_SILVER)
                ENDIF
                IF IS_MP_AWARD_GOLD_INTCHAR_UNLOCKED(MP_AWARD_50_VEHICLES_BLOWNUP)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_BLOWN_UP_GOLD)
                ENDIF
                IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_50_VEHICLES_BLOWNUP)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_BLOWN_UP_PLATINUM)
                ENDIF
                
                IF IS_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(MP_AWARD_VEHICLES_JACKEDR)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_STOLEN_BRONZE)
                ENDIF
                IF IS_MP_AWARD_SILVER_INTCHAR_UNLOCKED(MP_AWARD_VEHICLES_JACKEDR)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_STOLEN_SILVER)
                ENDIF
                IF IS_MP_AWARD_GOLD_INTCHAR_UNLOCKED(MP_AWARD_VEHICLES_JACKEDR)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_STOLEN_GOLD)
                ENDIF
                IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_VEHICLES_JACKEDR)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_STOLEN_PLATINUM)
                ENDIF
                
                IF IS_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(MP_AWARD_FMTIME5STARWANTED)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_5_STAR_WANTED_BRONZE)
                ENDIF
                IF IS_MP_AWARD_SILVER_INTCHAR_UNLOCKED(MP_AWARD_FMTIME5STARWANTED)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_5_STAR_WANTED_VEHICLES_SILVER)
                ENDIF
                IF IS_MP_AWARD_GOLD_INTCHAR_UNLOCKED(MP_AWARD_FMTIME5STARWANTED)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_5_STAR_WANTED_VEHICLES_GOLD)
                ENDIF
                IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMTIME5STARWANTED)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_5_STAR_WANTED_VEHICLES_PLATINUM)
                ENDIF
                /*
                IF IS_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(MP_AWARD_FMFURTHESTWHEELIE)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_BRONZE)
                ENDIF
                IF IS_MP_AWARD_SILVER_INTCHAR_UNLOCKED(MP_AWARD_FMFURTHESTWHEELIE)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_SILVER)
                ENDIF
                IF IS_MP_AWARD_GOLD_INTCHAR_UNLOCKED(MP_AWARD_FMFURTHESTWHEELIE)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_GOLD)
                ENDIF
                IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMFURTHESTWHEELIE)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_PLATINUM)
                ENDIF
                */
                  IF IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FMFURTHESTWHEELIE)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_PLATINUM)
                ENDIF
                
                IF IS_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(MP_AWARD_FMDRIVEWITHOUTCRASH)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_NO_CRASH_DRIVE_BRONZE)
                ENDIF
                IF IS_MP_AWARD_SILVER_INTCHAR_UNLOCKED(MP_AWARD_FMDRIVEWITHOUTCRASH)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_NO_CRASH_DRIVE_SILVER)
                ENDIF
                IF IS_MP_AWARD_GOLD_INTCHAR_UNLOCKED(MP_AWARD_FMDRIVEWITHOUTCRASH)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_NO_CRASH_DRIVE_GOLD)
                ENDIF
                IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMDRIVEWITHOUTCRASH)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_NO_CRASH_DRIVE_PLATINUM)
                ENDIF
                
                IF IS_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTFLIPSINONEVEHICLE)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_FLIPS_ONE_JUMP_BRONZE)
                ENDIF
                IF IS_MP_AWARD_SILVER_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTFLIPSINONEVEHICLE)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_FLIPS_ONE_JUMP_SILVER)
                ENDIF
                IF IS_MP_AWARD_GOLD_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTFLIPSINONEVEHICLE)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_FLIPS_ONE_JUMP_GOLD)
                ENDIF
                IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTFLIPSINONEVEHICLE)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_FLIPS_ONE_JUMP_PLATINUM)
                ENDIF
                
                IF IS_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTSPINSINONEVEHICLE)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_SPINS_ONE_JUMP_BRONZE)
                ENDIF
                IF IS_MP_AWARD_SILVER_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTSPINSINONEVEHICLE)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_SPINS_ONE_JUMP_SILVER)
                ENDIF
                IF IS_MP_AWARD_GOLD_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTSPINSINONEVEHICLE)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_SPINS_ONE_JUMP_GOLD)
                ENDIF
                IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTSPINSINONEVEHICLE)
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_SPINS_ONE_JUMP_PLATINUM)
                ENDIF
                
                socialFeedVars.iCurrentProfile = GET_ACTIVE_CHARACTER_SLOT()
                
                PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - calculated already unlocked awards and updated social feed flags. Going to stage 1.")
                
            ENDIF
			
            socialFeedVars.iStage++
            
        BREAK
        
        CASE 1
    
            // Send social message if I unlock an award for blowing up vehicles.
            IF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_BLOWN_UP_BRONZE)
                IF IS_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(MP_AWARD_50_VEHICLES_BLOWNUP)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_50_VEHICLES_BLOWNUP, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_50_VEHICLES_BLOWNUP))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_BLOWN_UP_BRONZE)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked bronze award for vehicles blown up, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_BLOWN_UP_SILVER)
                IF IS_MP_AWARD_SILVER_INTCHAR_UNLOCKED(MP_AWARD_50_VEHICLES_BLOWNUP)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_50_VEHICLES_BLOWNUP, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_50_VEHICLES_BLOWNUP))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_BLOWN_UP_SILVER)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked silver award for vehicles blown up, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_BLOWN_UP_GOLD)
                IF IS_MP_AWARD_GOLD_INTCHAR_UNLOCKED(MP_AWARD_50_VEHICLES_BLOWNUP)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_50_VEHICLES_BLOWNUP, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_50_VEHICLES_BLOWNUP))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_BLOWN_UP_GOLD)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked gold award for vehicles blown up, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_BLOWN_UP_PLATINUM)
                IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_50_VEHICLES_BLOWNUP)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_50_VEHICLES_BLOWNUP, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_50_VEHICLES_BLOWNUP))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_BLOWN_UP_PLATINUM)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked platinum award for vehicles blown up, sending social feed presence message to friends.")
                ENDIF
            ENDIF
			
			socialFeedVars.iStage++
            
        BREAK
        
        CASE 2
            
            // Send social message if I unlock an award for jacking vehicles.
            IF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_STOLEN_BRONZE)
                IF IS_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(MP_AWARD_VEHICLES_JACKEDR)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_VEHICLES_JACKEDR, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_VEHICLES_JACKEDR))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_STOLEN_BRONZE)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked bronze award for vehicles jacked, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_STOLEN_SILVER)
                IF IS_MP_AWARD_SILVER_INTCHAR_UNLOCKED(MP_AWARD_VEHICLES_JACKEDR)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_VEHICLES_JACKEDR, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_VEHICLES_JACKEDR))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_STOLEN_SILVER)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked silver award for vehicles jacked, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_STOLEN_GOLD)
                IF IS_MP_AWARD_GOLD_INTCHAR_UNLOCKED(MP_AWARD_VEHICLES_JACKEDR)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_VEHICLES_JACKEDR, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_VEHICLES_JACKEDR))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_STOLEN_GOLD)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked gold award for vehicles jacked, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_STOLEN_PLATINUM)
                IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_VEHICLES_JACKEDR)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_VEHICLES_JACKEDR, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_VEHICLES_JACKEDR))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_VEHICLES_STOLEN_PLATINUM)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked platinum award for vehicles jacked, sending social feed presence message to friends.")
                ENDIF
            ENDIF
			
			socialFeedVars.iStage++
            
        BREAK
        
        CASE 3
            
            IF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_5_STAR_WANTED_BRONZE)
                IF IS_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(MP_AWARD_FMTIME5STARWANTED)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMTIME5STARWANTED, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMTIME5STARWANTED))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_5_STAR_WANTED_BRONZE)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked bronze award for time spent with a 5 star wanted level, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_5_STAR_WANTED_VEHICLES_SILVER)
                IF IS_MP_AWARD_SILVER_INTCHAR_UNLOCKED(MP_AWARD_FMTIME5STARWANTED)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMTIME5STARWANTED, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMTIME5STARWANTED))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_5_STAR_WANTED_VEHICLES_SILVER)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked silver award for time spent with a 5 star wanted level, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_5_STAR_WANTED_VEHICLES_GOLD)
                IF IS_MP_AWARD_GOLD_INTCHAR_UNLOCKED(MP_AWARD_FMTIME5STARWANTED)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMTIME5STARWANTED, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMTIME5STARWANTED))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_5_STAR_WANTED_VEHICLES_GOLD)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked gold award for time spent with a 5 star wanted level, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_5_STAR_WANTED_VEHICLES_PLATINUM)
                IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMTIME5STARWANTED)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMTIME5STARWANTED, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMTIME5STARWANTED))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_5_STAR_WANTED_VEHICLES_PLATINUM)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked platinum award for time spent with a 5 star wanted level, sending social feed presence message to friends.")
                ENDIF
            ENDIF
			
			socialFeedVars.iStage++
            
        BREAK
        
        CASE 4
            /*
            IF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_BRONZE)
                IF IS_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(MP_AWARD_FMFURTHESTWHEELIE)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMFURTHESTWHEELIE, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMFURTHESTWHEELIE))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_BRONZE)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked bronze award for wheelie distance, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_SILVER)
                IF IS_MP_AWARD_SILVER_INTCHAR_UNLOCKED(MP_AWARD_FMFURTHESTWHEELIE)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMFURTHESTWHEELIE, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMFURTHESTWHEELIE))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_SILVER)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked silver award for wheelie distance, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_GOLD)
                IF IS_MP_AWARD_GOLD_INTCHAR_UNLOCKED(MP_AWARD_FMFURTHESTWHEELIE)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMFURTHESTWHEELIE, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMFURTHESTWHEELIE))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_GOLD)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked gold award for wheelie distance, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_PLATINUM)
                IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMFURTHESTWHEELIE)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMFURTHESTWHEELIE, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMFURTHESTWHEELIE))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_PLATINUM)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked platinum award for wheelie distance, sending social feed presence message to friends.")
                ENDIF
            ENDIF
            */
            
            IF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_PLATINUM)
                IF IS_MP_AWARD_PLATINUM_BOOLCHAR_UNLOCKED(MP_AWARD_FMFURTHESTWHEELIE)
                    PRESENCE_EVENT_UPDATESTAT_FLOAT (MP0_AWD_FMFURTHESTWHEELIE, GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_LONGEST_WHEELIE_DIST))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_LONGEST_WHEELIE_DIST_PLATINUM)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked platinum award for wheelie distance, sending social feed presence message to friends.")
                ENDIF
            ENDIF
            
            IF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_NO_CRASH_DRIVE_BRONZE)
                IF IS_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(MP_AWARD_FMDRIVEWITHOUTCRASH)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMDRIVEWITHOUTCRASH, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMDRIVEWITHOUTCRASH))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_NO_CRASH_DRIVE_BRONZE)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked bronze award for driving without crashing, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_NO_CRASH_DRIVE_SILVER)
                IF IS_MP_AWARD_SILVER_INTCHAR_UNLOCKED(MP_AWARD_FMDRIVEWITHOUTCRASH)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMDRIVEWITHOUTCRASH, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMDRIVEWITHOUTCRASH))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_NO_CRASH_DRIVE_SILVER)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked silver award for driving without crashing, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_NO_CRASH_DRIVE_GOLD)
                IF IS_MP_AWARD_GOLD_INTCHAR_UNLOCKED(MP_AWARD_FMDRIVEWITHOUTCRASH)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMDRIVEWITHOUTCRASH, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMDRIVEWITHOUTCRASH))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_NO_CRASH_DRIVE_GOLD)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked gold award for driving without crashing, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_NO_CRASH_DRIVE_PLATINUM)
                IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMDRIVEWITHOUTCRASH)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMDRIVEWITHOUTCRASH, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMDRIVEWITHOUTCRASH))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_NO_CRASH_DRIVE_PLATINUM)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked platinum award for driving without crashing, sending social feed presence message to friends.")
                ENDIF
            ENDIF
			
			socialFeedVars.iStage++
            
        BREAK
        
        CASE 5
            
            IF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_FLIPS_ONE_JUMP_BRONZE)
                IF IS_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTFLIPSINONEVEHICLE)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMMOSTFLIPSINONEVEHICLE, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMMOSTFLIPSINONEVEHICLE))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_FLIPS_ONE_JUMP_BRONZE)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked bronze award for vehicle flips, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_FLIPS_ONE_JUMP_SILVER)
                IF IS_MP_AWARD_SILVER_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTFLIPSINONEVEHICLE)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMMOSTFLIPSINONEVEHICLE, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMMOSTFLIPSINONEVEHICLE))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_FLIPS_ONE_JUMP_SILVER)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked silver award for vehicle flips, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_FLIPS_ONE_JUMP_GOLD)
                IF IS_MP_AWARD_GOLD_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTFLIPSINONEVEHICLE)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMMOSTFLIPSINONEVEHICLE, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMMOSTFLIPSINONEVEHICLE))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_FLIPS_ONE_JUMP_GOLD)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked gold award for vehicle flips, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_FLIPS_ONE_JUMP_PLATINUM)
                IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTFLIPSINONEVEHICLE)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMMOSTFLIPSINONEVEHICLE, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMMOSTFLIPSINONEVEHICLE))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_FLIPS_ONE_JUMP_PLATINUM)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked platinum award for vehicle flips, sending social feed presence message to friends.")
                ENDIF
            ENDIF
			
			socialFeedVars.iStage++
            
        BREAK
        
        CASE 6
            
            IF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_SPINS_ONE_JUMP_BRONZE)
                IF IS_MP_AWARD_BRONZE_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTSPINSINONEVEHICLE)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMMOSTSPINSINONEVEHICLE, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMMOSTSPINSINONEVEHICLE))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_SPINS_ONE_JUMP_BRONZE)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked bronze award for vehicle spins, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_SPINS_ONE_JUMP_SILVER)
                IF IS_MP_AWARD_SILVER_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTSPINSINONEVEHICLE)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMMOSTSPINSINONEVEHICLE, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMMOSTSPINSINONEVEHICLE))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_SPINS_ONE_JUMP_SILVER)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked silver award for vehicle spins, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_SPINS_ONE_JUMP_GOLD)
                IF IS_MP_AWARD_GOLD_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTSPINSINONEVEHICLE)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMMOSTSPINSINONEVEHICLE, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMMOSTSPINSINONEVEHICLE))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_SPINS_ONE_JUMP_GOLD)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked gold award for vehicle spins, sending social feed presence message to friends.")
                ENDIF
            ELIF NOT IS_BIT_SET(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_SPINS_ONE_JUMP_PLATINUM)
                IF IS_MP_AWARD_PLATINUM_INTCHAR_UNLOCKED(MP_AWARD_FMMOSTSPINSINONEVEHICLE)
                    PRESENCE_EVENT_UPDATESTAT_INT(MP0_AWD_FMMOSTSPINSINONEVEHICLE, GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMMOSTSPINSINONEVEHICLE))
                    SET_BIT(socialFeedVars.iUnlockedAwardsBitset, PRESENCE_EVENT_UNLOCKED_SPINS_ONE_JUMP_PLATINUM)
                    PRINTLN("[Feed] - [Social Feed] - MAINTAIN_SEND_MP_PRESENCE_EVENTS - unlocked platinum award for vehicle spins, sending social feed presence message to friends.")
                ENDIF
            ENDIF
			
			// Jump back to start and prcess again.
			socialFeedVars.iStage = 0
			
        BREAK
        
    ENDSWITCH
    
ENDPROC

FUNC BOOL DID_MESSAGE_COME_FROM_PLAYER_IN_SAME_MP_SESSION(STRING playerName)
    
    INT i
    PLAYER_INDEX playerId
    
    REPEAT NUM_NETWORK_PLAYERS i
        
        playerId = INT_TO_NATIVE(PLAYER_INDEX, i)
        
        IF IS_NET_PLAYER_OK(playerId, FALSE)
            
            IF ARE_STRINGS_EQUAL(GET_PLAYER_NAME(playerId), playerName)
                
                RETURN TRUE
            
            ENDIF
            
        ENDIF
        
    ENDREPEAT
    
    RETURN FALSE
    
ENDFUNC

PROC PROCESS_STATUPDATE_EVENT(INT iCount, STRUCT_MP_PRESENCE_EVENT_VARS &socialFeedVars, BOOL bProcessMessages)
    
    IF NOT IS_ACCOUNT_OVER_17()
        EXIT
    ENDIF
    
	IF NOT HAS_IMPORTANT_STATS_LOADED()
		EXIT
	ENDIF
	
    INT iMyStatValue
    
    STRUCT_GAME_PRESENCE_EVENT_STAT_UPDATE sei

    PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - Received event of type EVENT_NETWORK_PRESENCE_STAT_UPDATE")

    IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
        
        PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - Successfully got event data")
        
        IF NOT bProcessMessages
            PRINTLN("[Feed] - [Social Feed] - SOCIAL_FEED_CONTROLLER header - bProcessMessages returned false - feed Delay value in frontend is set to... ", GET_PROFILE_SETTING(PROFILE_FEED_DELAY))
            PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - ignoring because timer window is not open.")
            EXIT
        ENDIF
        
        IF (GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) = 1)
        AND socialFeedVars.bDisplayedAMessage = FALSE //Finally sussed out that the problem in 1611931 was caused by this not getting reset quickly enough, so there was a frame or two window to display another feed message. 

            
            PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - pause menu profile setting says allow friend feed messages.")
            
            SWITCH INT_TO_ENUM(STATSENUM, sei.statHash)
                /*
                CASE MP0_AWD_REACH_RANK_R
                CASE MP1_AWD_REACH_RANK_R
                CASE MP2_AWD_REACH_RANK_R
                CASE MP3_AWD_REACH_RANK_R
                CASE MP4_AWD_REACH_RANK_R
                    PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_INT("SCFEED_2", sei.fromGamer, sei.iValue)
                    PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for stat MP#_AWD_REACH_RANK_R.")
                BREAK*/
                
                CASE MP0_AWD_50_VEHICLES_BLOWNUP
                CASE MP1_AWD_50_VEHICLES_BLOWNUP
//                CASE MP2_AWD_50_VEHICLES_BLOWNUP
//                CASE MP3_AWD_50_VEHICLES_BLOWNUP
//                CASE MP4_AWD_50_VEHICLES_BLOWNUP
                    iMyStatValue = GET_MP_INT_CHARACTER_AWARD(MP_AWARD_50_VEHICLES_BLOWNUP)
                    IF sei.iValue > iMyStatValue
                        IF sei.iValue > 1
                            PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_TWO_INTS("SCFEED_13", sei.fromGamer, sei.iValue, iMyStatValue, HUD_COLOUR_PURE_WHITE, TRUE)
                        ELSE
                            PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_TWO_INTS("SCFEED_13b", sei.fromGamer, sei.iValue, iMyStatValue, HUD_COLOUR_PURE_WHITE, TRUE)
                        ENDIF
                        socialFeedVars.bDisplayedAMessage = TRUE
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for stat MP_AWARD_50_VEHICLES_BLOWNUP.")
                    ELSE
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for stat MP_AWARD_50_VEHICLES_BLOWNUP, not better than my val, no ticker.")
                    ENDIF
                BREAK
                
                CASE MP0_AWD_VEHICLES_JACKEDR
                CASE MP1_AWD_VEHICLES_JACKEDR
//                CASE MP2_AWD_VEHICLES_JACKEDR
//                CASE MP3_AWD_VEHICLES_JACKEDR
//                CASE MP4_AWD_VEHICLES_JACKEDR
                    iMyStatValue = GET_MP_INT_CHARACTER_AWARD(MP_AWARD_VEHICLES_JACKEDR)
                    IF sei.iValue > iMyStatValue
                        IF sei.iValue > 1
                            PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_TWO_INTS("SCFEED_10", sei.fromGamer, sei.iValue, iMyStatValue, HUD_COLOUR_PURE_WHITE, TRUE)
                        ELSE
                            PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_TWO_INTS("SCFEED_10b", sei.fromGamer, sei.iValue, iMyStatValue, HUD_COLOUR_PURE_WHITE, TRUE)
                        ENDIF
                        socialFeedVars.bDisplayedAMessage = TRUE
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for stat MP_AWARD_VEHICLES_JACKEDR.")
                    ELSE
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for stat MP_AWARD_VEHICLES_JACKEDR, not better than my val, no ticker.")
                    ENDIF
                BREAK
                
                CASE MP0_AWD_FMTIME5STARWANTED
                CASE MP1_AWD_FMTIME5STARWANTED
//                CASE MP2_AWD_FMTIME5STARWANTED
//                CASE MP3_AWD_FMTIME5STARWANTED
//                CASE MP4_AWD_FMTIME5STARWANTED
                    iMyStatValue = GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMTIME5STARWANTED)
                    IF sei.iValue > iMyStatValue
                        PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_TWO_INTS("SCFEED_4", sei.fromGamer, sei.iValue, iMyStatValue, HUD_COLOUR_PURE_WHITE, TRUE)
                        socialFeedVars.bDisplayedAMessage = TRUE
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for stat MP_AWARD_FMTIME5STARWANTED.")
                    ELSE
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for stat MP_AWARD_FMTIME5STARWANTED, not better than my val, no ticker.")
                    ENDIF
                BREAK
                
                CASE MP0_AWD_FMFURTHESTWHEELIE
                CASE MP1_AWD_FMFURTHESTWHEELIE
//                CASE MP2_AWD_FMFURTHESTWHEELIE
//                CASE MP3_AWD_FMFURTHESTWHEELIE

                    iMyStatValue = ROUND(GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_LONGEST_WHEELIE_DIST))
                    IF sei.iValue > iMyStatValue
						//If we're on the wheelie challenge then block the message
						IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_CHALLENGES
						AND GET_PLAYERS_ACTIVE_FREEMODE_CHALLENGE(PLAYER_ID()) = AM_CHALLENGE_LONGEST_WHEELIE
	                        PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for stat MP_AWARD_FMFURTHESTWHEELIE - ignore - FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_CHALLENGES")
						ELSE
							IF SHOULD_USE_METRIC_MEASUREMENTS()
	                        	PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_TWO_INTS("SCFEED_5", sei.fromGamer, sei.iValue, iMyStatValue, HUD_COLOUR_PURE_WHITE, TRUE)
	                        ELSE
	                        	PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_TWO_INTS("SCFEED_5b", sei.fromGamer, sei.iValue, iMyStatValue, HUD_COLOUR_PURE_WHITE, TRUE)
							ENDIF
							socialFeedVars.bDisplayedAMessage = TRUE
	                        PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for stat MP_AWARD_FMFURTHESTWHEELIE.")
						ENDIF
                    ELSE
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for stat MP_AWARD_FMFURTHESTWHEELIE, not better than my val, no ticker.")
                    ENDIF
                BREAK
                
                CASE MP0_AWD_FMDRIVEWITHOUTCRASH
                CASE MP1_AWD_FMDRIVEWITHOUTCRASH
//                CASE MP2_AWD_FMDRIVEWITHOUTCRASH
//                CASE MP3_AWD_FMDRIVEWITHOUTCRASH
//                CASE MP4_AWD_FMDRIVEWITHOUTCRASH
                    iMyStatValue = GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMDRIVEWITHOUTCRASH)
                    IF sei.iValue > iMyStatValue
                        PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_TWO_INTS("SCFEED_6", sei.fromGamer, sei.iValue, iMyStatValue, HUD_COLOUR_PURE_WHITE, TRUE)
                        socialFeedVars.bDisplayedAMessage = TRUE
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for stat MP_AWARD_FMDRIVEWITHOUTCRASH.")
                    ELSE
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for stat MP_AWARD_FMDRIVEWITHOUTCRASH, not better than my val, no ticker.")
                    ENDIF
                BREAK
                
                CASE MP0_AWD_FMMOSTFLIPSINONEVEHICLE
                CASE MP1_AWD_FMMOSTFLIPSINONEVEHICLE
//                CASE MP2_AWD_FMMOSTFLIPSINONEVEHICLE
//                CASE MP3_AWD_FMMOSTFLIPSINONEVEHICLE
//                CASE MP4_AWD_FMMOSTFLIPSINONEVEHICLE
                    iMyStatValue = GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMMOSTFLIPSINONEVEHICLE)
                    IF sei.iValue > iMyStatValue
                        IF sei.iValue > 1
                            PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_TWO_INTS("SCFEED_7", sei.fromGamer, sei.iValue, iMyStatValue, HUD_COLOUR_PURE_WHITE, TRUE)
                        ELSE
                            PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_TWO_INTS("SCFEED_7b", sei.fromGamer, sei.iValue, iMyStatValue, HUD_COLOUR_PURE_WHITE, TRUE)
                        ENDIF
                        socialFeedVars.bDisplayedAMessage = TRUE
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for stat MP_AWARD_FMMOSTFLIPSINONEVEHICLE.")
                    ELSE
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for stat MP_AWARD_FMMOSTFLIPSINONEVEHICLE, not better than my val, no ticker.")
                    ENDIF
                BREAK
                
                CASE MP0_AWD_FMMOSTSPINSINONEVEHICLE
                CASE MP1_AWD_FMMOSTSPINSINONEVEHICLE
//                CASE MP2_AWD_FMMOSTSPINSINONEVEHICLE
//                CASE MP3_AWD_FMMOSTSPINSINONEVEHICLE
//                CASE MP4_AWD_FMMOSTSPINSINONEVEHICLE
                    iMyStatValue = GET_MP_INT_CHARACTER_AWARD(MP_AWARD_FMMOSTSPINSINONEVEHICLE)
                    IF sei.iValue > iMyStatValue
                        IF sei.iValue > 1
                            PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_TWO_INTS("SCFEED_8", sei.fromGamer, sei.iValue, iMyStatValue, HUD_COLOUR_PURE_WHITE, TRUE)
                        ELSE
                            PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_TWO_INTS("SCFEED_8b", sei.fromGamer, sei.iValue, iMyStatValue, HUD_COLOUR_PURE_WHITE, TRUE)
                        ENDIF
                        socialFeedVars.bDisplayedAMessage = TRUE
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for stat MP_AWARD_FMMOSTSPINSINONEVEHICLE.")
                    ELSE
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for stat MP_AWARD_FMMOSTSPINSINONEVEHICLE, not better than my val, no ticker.")
                    ENDIF
                BREAK
                
                CASE MP0_CHAR_FM_RACE_RECORD_TIMES
                CASE MP1_CHAR_FM_RACE_RECORD_TIMES
//                CASE MP2_CHAR_FM_RACE_RECORD_TIMES
//                CASE MP3_CHAR_FM_RACE_RECORD_TIMES
//                CASE MP4_CHAR_FM_RACE_RECORD_TIMES
					// Added all these under instruction of Bobby for B* 2286272.
					IF GET_CORONA_STATUS() = CORONA_STATUS_IDLE
					AND NOT NETWORK_IS_ACTIVITY_SESSION() //And not in Jobs
					AND NOT IS_TRANSITION_SESSION_LAUNCHING() //And a transition session is not launching
					AND NOT IS_PAUSE_MENU_IS_USING_UGC() //And not has the mause menu got a hold of the UGC
					AND NOT TRANSITION_SESSIONS_CLEANUP_AFTER_MISSION() //And not is the game restarting
					AND NOT SHOULD_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH() //And we are not restarting freemode
					AND NOT SHOULD_TRANSITION_SESSION_LAUNCH_MAIN_FM_SCRIPT() //And not is the main freemode script about to relaunch
					AND NOT DOES_CURRENT_FM_MISSION_NEED_CLEANED_UP() //And not if we are in the process of cleaning up a mission
					AND NOT Is_Cloud_Loaded_Mission_Data_Being_Refreshed() // And not if the missions are still in the process of being added/refreshed from a previous refresh
					AND NOT Is_Cloud_Loaded_Mission_Data_Download_In_Progress() // And Not if a mission download request in in progress (generally Gang Attack, or Mission Triggerer)
					AND ( HAS_IMPORTANT_STATS_LOADED() AND ARE_ALL_INSTANCED_TUTORIALS_COMPLETE() )//in the tutoriasl
					AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState != FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION //And a transition session is not being set up
					AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState != FMMC_LAUNCHER_STATE_INITILISE_MISSION //And I'm not initilising a mission
					AND NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()) //And not on a playlist
					AND NOT IS_SCRIPT_UCG_REQUEST_IN_PROGRESS() //and it;s not already getting
					AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE) //And not in propertys
						GET_UGC_CONTENT_STRUCT sTempContentStruct
						CLOUD_LOADED_MISSION_HEADER_DETAILS sTempHeaderDetails
						INT iRootId
						IF GET_UGC_HEADER_BY_CONTENT_ID(sTempContentStruct, sei.szStringValue, sTempHeaderDetails, iRootId)
			            	IF sTempContentStruct.bSuccess
			                    PRINT_TICKER_WITH_PLAYER_NAME_STRING_TIME_AND_STRING_AND_DURATION_MODIFIER ("SCFEED_11", sei.fromGamer, sei.iValue, sTempHeaderDetails.tlMissionName, TRUE, 2.0) //1568870
			                    PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - received presence event from social club for fake stat MP0_CHAR_FM_RACE_RECORD_TIMES.")
			                    PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - from: ", sei.fromGamer)
			                    PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - time: ", sei.iValue)
			                    PRINTLN("[Feed] - [Social Feed] - PROCESS_STATUPDATE_EVENT - mission name: ", sei.szStringValue)

			                    IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
			                        PLAY_SOUND_FRONTEND (-1, "OTHER_TEXT", "HUD_AWARDS")
			                    ENDIF

			                    socialFeedVars.bDisplayedAMessage = TRUE
							ENDIF
						ENDIF	
					ENDIF
                BREAK
                
            ENDSWITCH
            
        ENDIF
        
    ELSE
    
        NET_SCRIPT_ASSERT("[WJK] - PROCESS_STATUPDATE_EVENT - could not get event data!")
    
    ENDIF
    
ENDPROC




PROC PROCESS_CREW_PROMOTION_DEMOTION_EVENT(INT iCount, STRUCT_MP_PRESENCE_EVENT_VARS &socialFeedVars, BOOL bProcessMessages)
    
    IF NOT IS_ACCOUNT_OVER_17()
	OR NOT IS_ACCOUNT_OVER_17_FOR_UGC()
        EXIT
    ENDIF

    //Struct types for easy reference.

    //STRUCT STRUCT_CREW_RANK_CHANGED
    //  INT nClanId
    //  INT nRankOrder
    //  BOOL bPromotion //true if a promotion, othewise, a demotion
    //  TEXT_LABEL_31 szRankName
    //ENDSTRUCT
    //
    //
    // STRUCT NETWORK_CLAN_DESC
    //  INT            Id
    //    TEXT_LABEL_63  ClanName
    //    TEXT_LABEL_7   ClanTag
    //  INT            MemberCount
    //  INT            IsSystemClan
    //  INT            IsOpenClan
    //    TEXT_LABEL_31  RankName
    //  INT            RankOrder
    //  INT            CreatedTimePosix
    //  INT            ClanColor_Red
    //  INT            ClanColor_Green
    //  INT            ClanColor_Blue
    //ENDSTRUCT



    
    NETWORK_CLAN_DESC sDesc
    STRUCT_CREW_RANK_CHANGED sei

    PRINTLN("[Feed] - [Social Feed] - PROCESS_CREW_PROMOTION_DEMOTION_EVENT - Received event of type STRUCT_CREW_RANK_CHANGED")

    IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
        
        PRINTLN("[Feed] - [Social Feed] - PROCESS_CREW_PROMOTION_DEMOTION_EVENT - Successfully got event data")
        

        //Error: [net_clan] Verify(index < m_LocalPlayerMembership.m_Count) FAILED (from Updating script social_controller)
        //Error: [script_network] Assertf(membership && membership->IsValid()) FAILED: SCRIPT: Script Name = social_controller : Program Counter = 30974:NETWORK_CLAN_GET_MEMBERSHIP_DESC - Invalid membership index 22415, count is 1. (from Updating script social_controller)


        
        IF NOT bProcessMessages
            PRINTLN("[Feed] - [Social Feed] - PROCESS_CREW_PROMOTION_DEMOTION_EVENT - ignoring because timer window is not open.")
            EXIT
        ENDIF
                


        IF (GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) = 1)
        AND socialFeedVars.bDisplayedAMessage = FALSE //Make sure this can't be spammed.

            

            INT iLoop = NETWORK_CLAN_GET_LOCAL_MEMBERSHIPS_COUNT()
            INT i


            REPEAT iLoop i
            
                NETWORK_CLAN_GET_MEMBERSHIP_DESC (sDesc, i) 
            

                IF SDesc.Id = sei.nClanId

   
                //NETWORK_CLAN_GET_MEMBERSHIP_DESC(sDesc, sei.nClanId)


                    TEXT_LABEL_31 tlRankName

                    IF sei.bPromotion   //Failsafes in case RankN label comparisons below all fail...

                        tlRankName = GET_FILENAME_FOR_AUDIO_CONVERSATION("HIGHER_RANK")

                    ELSE
                    
                        tlRankName = GET_FILENAME_FOR_AUDIO_CONVERSATION("LOWER_RANK")
                    
                    ENDIF


                        

                    IF ARE_STRINGS_EQUAL (sei.szRankName, "Rank4")

                        tlRankName = GET_FILENAME_FOR_AUDIO_CONVERSATION("CREW_RANK4")

                    ENDIF

                    IF ARE_STRINGS_EQUAL (sei.szRankName, "Rank3")

                        tlRankName = GET_FILENAME_FOR_AUDIO_CONVERSATION("CREW_RANK3")

                    ENDIF

                    IF ARE_STRINGS_EQUAL (sei.szRankName, "Rank2")

                        tlRankName = GET_FILENAME_FOR_AUDIO_CONVERSATION("CREW_RANK2")

                    ENDIF
                    

                    IF ARE_STRINGS_EQUAL (sei.szRankName, "Rank1")

                        tlRankName = GET_FILENAME_FOR_AUDIO_CONVERSATION("CREW_RANK1")

                    ENDIF


                                
                    IF sei.bPromotion

                        PRINT_TICKER_WITH_TWO_CUSTOM_STRINGS("CREWPROM", tlRankName, sDesc.ClanName)
                        //PRINT_TICKER_WITH_TWO_CUSTOM_STRINGS("CREWPROM", sei.szRankName, sDesc.ClanName)
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_CREW_PROMOTION_PROMOTION_EVENT - been promoted.")
                        PRINTLN("[Feed] - [Social Feed] - SOCIAL_FEED - Hierarchy ticker event sent - Promotion to ", tlRankName)
                        
                    ELSE

                        PRINT_TICKER_WITH_TWO_CUSTOM_STRINGS("CREWDEMO", tlRankName, sDesc.ClanName)
                        //PRINT_TICKER_WITH_TWO_CUSTOM_STRINGS("CREWDEMO", sei.szRankName, sDesc.ClanName)
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_CREW_PROMOTION_DEMOTION_EVENT - been demoted.")
                        PRINTLN("[Feed] - [Social Feed] - SOCIAL_FEED - Hierarchy ticker event sent - Demotion to ", tlRankName)
                        
                    ENDIF

                    socialFeedVars.bDisplayedAMessage = TRUE


                ENDIF
            
            ENDREPEAT


        ENDIF
        
    ELSE
        
        NET_SCRIPT_ASSERT("[WJK] - PROCESS_CREW_PROMOTION_DEMOTION_EVENT - could not get event data!")
        
    ENDIF
    
ENDPROC





PROC PROCESS_CREW_LEFT_EVENT(INT iCount, STRUCT_MP_PRESENCE_EVENT_VARS &socialFeedVars, BOOL bProcessMessages)
    
    IF NOT IS_ACCOUNT_OVER_17()
	OR NOT IS_ACCOUNT_OVER_17_FOR_UGC()
        EXIT
    ENDIF

    //Struct types for easy reference.
    /*
        STRUCT STRUCT_CLAN_INFO
            INT clanId
            BOOL primary
        ENDSTRUCT
    */

    //
    //
    // STRUCT NETWORK_CLAN_DESC
    //  INT            Id
    //    TEXT_LABEL_63  ClanName
    //    TEXT_LABEL_7   ClanTag
    //  INT            MemberCount
    //  INT            IsSystemClan
    //  INT            IsOpenClan
    //    TEXT_LABEL_31  RankName
    //  INT            RankOrder
    //  INT            CreatedTimePosix
    //  INT            ClanColor_Red
    //  INT            ClanColor_Green
    //  INT            ClanColor_Blue
    //ENDSTRUCT



    
    //NETWORK_CLAN_DESC sDesc
    STRUCT_CLAN_INFO sei

    PRINTLN("[Feed] - [Social Feed] - PROCESS_CREW_LEFT_EVENT - Received event of type EVENT_NETWORK_CLAN_LEFT")

    IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
        
        PRINTLN("[Feed] - [Social Feed] - PROCESS_CREW_LEFT_EVENT - Successfully got event data")
        

        
        IF NOT bProcessMessages
            PRINTLN("[Feed] - [Social Feed] - PROCESS_CREW_LEFT_EVENT - ignoring because timer window is not open.")
            EXIT
        ENDIF
                


        IF (GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) = 1)
        AND socialFeedVars.bDisplayedAMessage = FALSE //Make sure this can't be spammed.

            
            //Can't get crew data of a crew the player is no longer a member of. #1613920

            //INT iLoop = NETWORK_CLAN_GET_LOCAL_MEMBERSHIPS_COUNT()
            //INT i


            //REPEAT iLoop i
            
                //NETWORK_CLAN_GET_MEMBERSHIP_DESC (sDesc, i) 
            

                //IF SDesc.Id = sei.ClanId

                    //PRINT_TICKER_WITH_PLAYER_NAME_STRING("CREWLEFT", sDesc.ClanName)            

                    //PRINTLN("[Feed] - [Social Feed] - PROCESS_CREW_LEFT_EVENT")

         
                    //socialFeedVars.bDisplayedAMessage = TRUE


                //ENDIF
            
            //ENDREPEAT

            
            IF sei.primary

                BEGIN_TEXT_COMMAND_THEFEED_POST ("CREWLEFT_TA")   //Left Primary crew
                END_TEXT_COMMAND_THEFEED_POST_TICKER (FALSE, TRUE)
 
            ELSE

                BEGIN_TEXT_COMMAND_THEFEED_POST ("CREWLEFT_TA")
                END_TEXT_COMMAND_THEFEED_POST_TICKER (FALSE, TRUE)

            ENDIF
            
            //[CREWLEFT_TA]
            //You have left your Crew.

            //[CREWLEFT_TI]
            //You have left your inactive Crew.

        ENDIF
        
    ELSE
        
        NET_SCRIPT_ASSERT("[WJK] - PROCESS_CREW_LEFT_EVENT - could not get event data!")
        
    ENDIF
    
ENDPROC

//PROC PROCESS_INVITED_TO_CREW_MESSAGE_EVENT(INT iCount, STRUCT_MP_PRESENCE_EVENT_VARS &socialFeedVars, BOOL bProcessMessages)
//  
//  IF NOT IS_ACCOUNT_OVER_17()
//        EXIT
//    ENDIF
//  
//    STRUCT_CREW_INVITE_EVENT sei
//  
//    /*    INT nClanId
//        TEXT_LABEL_31 szCrewName
//        TEXT_LABEL_7  szCrewTag
//        TEXT_LABEL_31 szRankName
//        BOOL          bHasMessage
//    ENDSTRUCT
//    */
//  IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
//      
//      IF NOT bProcessMessages
//            PRINTLN("[Feed] - [Social Feed] - PROCESS_INVITED_TO_CREW_MESSAGE_EVENT - ignoring because timer window is not open.")
//            EXIT
//        ENDIF
//      
//      // If the feed settings say crew invites can be shown.
//      IF GET_PROFILE_SETTING(PROFILE_FEED_CREW) = 1
//      OR GET_PROFILE_SETTING(PROFILE_FEED_SOCIAL) = 1
//      AND socialFeedVars.bDisplayedAMessage = FALSE
//          
//          // If it is safe to show a ticker.
//          IF NOT IS_TRANSITION_ACTIVE()
//            IF NOT NETWORK_IS_ACTIVITY_SESSION()            
//                  IF HAS_PLAYER_BEEN_IN_SESSION_THIS_LONG(DEFAULT_DISPLAY_DELAY*2) 
//                      IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
//                          IF NOT IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
//                              
//                              // Display ticker.
//                              BEGIN_TEXT_COMMAND_THEFEED_POST("CREWINVITE")
//                                  ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sei.szCrewName)
//                              END_TEXT_COMMAND_THEFEED_POST_MPTICKER (FALSE, TRUE)
//                              
//                          ENDIF
//                      ENDIF
//                  ENDIF
//              ENDIF
//          ENDIF
//      
//      ENDIF
//      
//  ENDIF
//  
//ENDPROC

PROC PROCESS_CREW_KICKED_EVENT(INT iCount, STRUCT_MP_PRESENCE_EVENT_VARS &socialFeedVars, BOOL bProcessMessages)
    
    IF NOT IS_ACCOUNT_OVER_17()
	OR NOT IS_ACCOUNT_OVER_17_FOR_UGC()
        EXIT
    ENDIF

    //Struct types for easy reference.
    /*
        STRUCT STRUCT_CLAN_INFO
            INT clanId
            BOOL primary
        ENDSTRUCT
    */

    //
    //
    // STRUCT NETWORK_CLAN_DESC
    //  INT            Id
    //    TEXT_LABEL_63  ClanName
    //    TEXT_LABEL_7   ClanTag
    //  INT            MemberCount
    //  INT            IsSystemClan
    //  INT            IsOpenClan
    //    TEXT_LABEL_31  RankName
    //  INT            RankOrder
    //  INT            CreatedTimePosix
    //  INT            ClanColor_Red
    //  INT            ClanColor_Green
    //  INT            ClanColor_Blue
    //ENDSTRUCT



    
    //NETWORK_CLAN_DESC sDesc
    STRUCT_CLAN_INFO sei

    PRINTLN("[Feed] - [Social Feed] - PROCESS_CREW_KICKED_EVENT - Received event of type EVENT_NETWORK_CLAN_KICKED")

    IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sei, SIZE_OF(sei))
        
        PRINTLN("[Feed] - [Social Feed] - PROCESS_CREW_LEFT_EVENT - Successfully got event data")
        

        
        IF NOT bProcessMessages
            PRINTLN("[Feed] - [Social Feed] - PROCESS_CREW_KICKED_EVENT - ignoring because timer window is not open.")
            EXIT
        ENDIF
                


        IF (GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) = 1)
        AND socialFeedVars.bDisplayedAMessage = FALSE //Make sure this can't be spammed.

            
            //Can't get crew data of a crew the player is no longer a member of. #1613920

            //INT iLoop = NETWORK_CLAN_GET_LOCAL_MEMBERSHIPS_COUNT()
            //INT i


            //REPEAT iLoop i
            
                //NETWORK_CLAN_GET_MEMBERSHIP_DESC (sDesc, i) 
            

                //IF SDesc.Id = sei.ClanId

                    //PRINT_TICKER_WITH_PLAYER_NAME_STRING("CREWLEFT", sDesc.ClanName)            

                    //PRINTLN("[Feed] - [Social Feed] - PROCESS_CREW_LEFT_EVENT")

         
                    //socialFeedVars.bDisplayedAMessage = TRUE


                //ENDIF
            
            //ENDREPEAT

            
            IF sei.primary

                BEGIN_TEXT_COMMAND_THEFEED_POST ("CREWKICK_TA")   //Kicked from Primary crew
                END_TEXT_COMMAND_THEFEED_POST_MPTICKER (FALSE, TRUE)

            ELSE

                BEGIN_TEXT_COMMAND_THEFEED_POST ("CREWKICK_TI")
                END_TEXT_COMMAND_THEFEED_POST_MPTICKER (FALSE, TRUE)

            ENDIF
//
//[CREWKICKED]
//You have been kicked from Crew ~a~.
//
//
//[CREWKICK_TA]
//You have been kicked from your active Crew.
//
//[CREWKICK_TI]
//You have been kicked from an inactive Crew.


        ENDIF
        
    ELSE
        
        NET_SCRIPT_ASSERT("[WJK] - PROCESS_CREW_KICKED_EVENT - could not get event data!")
        
    ENDIF
    
ENDPROC





PROC PROCESS_NETWORK_EMAIL_RECEIVED_EVENT(STRUCT_MP_PRESENCE_EVENT_VARS &socialFeedVars, BOOL bProcessMessages)

    //We need to support Eyefind email notifications regardless of Social Club account linking.  See #2195214
    //The actual feed creation has been moved to PROCESS_MAINPER_NETWORK_EMAIL_RECEIVED_EVENT() within main_persistent.sc as social controller requires linked account to run.
    //Leaving legacy routine commented out in case we need to switch back. - Steve Taylor. 16/01/15     
    

    
    IF NOT IS_ACCOUNT_OVER_17()

        #if IS_DEBUG_BUILD
            cdPrintString("Social_Feed_Controller - Received Eyefind Email Event but Account is not over 17. No feed creation.")
            cdPrintnl()
        #endif
        
        EXIT
    ENDIF

    IF NOT bProcessMessages
        PRINTLN("[Feed] - [Social Feed] - PROCESS_NETWORK_EMAIL_RECEIVED_EVENT - ignoring because timer window is not open.")

        #if IS_DEBUG_BUILD
            cdPrintString("Social_Feed_Controller - Received Eyefind Email Event but bProcessMessages Timer Window is closed. No feed creation.")
            cdPrintnl()
        #endif


        EXIT
    ENDIF
                

    IF (GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) = 1)
    AND socialFeedVars.bDisplayedAMessage = FALSE //Make sure this can't be spammed.

        socialFeedVars.bDisplayedAMessage = TRUE

        //The actual feed creation has been moved to PROCESS_MAINPER_NETWORK_EMAIL_RECEIVED_EVENT(). That also listens for the NETWORK_EMAIL_RECEIVED_EVENT outwith social club.
        /*

        PRINTLN("[Feed] - [Social Feed] - PROCESS_NETWORK_EMAIL_RECEIVED_EVENT - Displaying feed notification of Eyefind message received.")

        BEGIN_TEXT_COMMAND_THEFEED_POST ("CELL_EMAIL_EVENT")   
        END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)

        */

    ELSE
        
        IF GET_PROFILE_SETTING(PROFILE_FEED_FRIENDS) <> 1

            #if IS_DEBUG_BUILD
                cdPrintString("Social_Feed_Controller - Received Eyefind Email Event but PROFILE_FEED_FRIENDS did not equal 1. No feed creation.")
                cdPrintnl()
            #endif

        ENDIF


        IF socialFeedVars.bDisplayedAMessage = TRUE

            #if IS_DEBUG_BUILD
                cdPrintString("Social_Feed_Controller - Received Eyefind Email Event but bDisplayedAMessage was still TRUE. No feed creation.")
                cdPrintnl()
            #endif

        ENDIF


    ENDIF
    


ENDPROC
















PROC PRINT_MY_SCORE_BEAT_TICKER(STRING sender, INT iScore, INT iMissionType, INT iMissionSubType, STRING tl31MissionName)
    
    SWITCH iMissionType
        

        //1603096 - for this bug regarding missing times, I have forced the last parameter of the print ticker calls to false, so that we use the standard post-ticker feed command.
        //Using the version that displays the R* logo causes auto truncation.
        //Test examples:
        //PRINT_MY_SCORE_BEAT_TICKER("SteveT", 78973, FMMC_TYPE_RACE, ciRACE_SUB_TYPE_STANDARD, "A_Very_Long_Race_Name_To_Test_Feed")
        //PRINT_MY_SCORE_BEAT_TICKER("Nakamura", 634233, FMMC_TYPE_SURVIVAL, ciRACE_SUB_TYPE_STANDARD, "A_Very_Long_Event_Name_To_Test_Feed")



        CASE FMMC_TYPE_SURVIVAL
            PRINTLN("[Feed] - [Social Feed] - PRINT_MY_SCORE_BEAT_TICKER - printing survival score beaten ticker.")
            PRINT_TICKER_WITH_PLAYER_NAME_STRING_INT_AND_STRING("SC_BTRSCR_SUR", sender, tl31MissionName, iScore, HUD_COLOUR_PURE_WHITE)
        BREAK
        
        CASE FMMC_TYPE_DEATHMATCH
            
            SWITCH iMissionSubType
                
                CASE FMMC_DM_TYPE_NORMAL
				CASE FMMC_DM_TYPE_KOTH
                    PRINTLN("[Feed] - [Social Feed] - PRINT_MY_SCORE_BEAT_TICKER - printing standard dm score beaten ticker.")
                    PRINT_TICKER_WITH_PLAYER_NAME_STRING_INT_AND_STRING("SC_BTRSCR_DMN", sender, tl31MissionName, iScore, HUD_COLOUR_PURE_WHITE)
                BREAK
                
                CASE FMMC_DM_TYPE_TEAM
				CASE FMMC_DM_TYPE_TEAM_KOTH
                    PRINTLN("[Feed] - [Social Feed] - PRINT_MY_SCORE_BEAT_TICKER - printing team dm score beaten ticker.")
                    PRINT_TICKER_WITH_PLAYER_NAME_STRING_INT_AND_STRING("SC_BTRSCR_DMT", sender, tl31MissionName, iScore, HUD_COLOUR_PURE_WHITE)
                BREAK
                
                CASE FMMC_DM_TYPE_VEHICLE
                    PRINTLN("[Feed] - [Social Feed] - PRINT_MY_SCORE_BEAT_TICKER - printing vehicle dm score beaten ticker.")
                    PRINT_TICKER_WITH_PLAYER_NAME_STRING_INT_AND_STRING("SC_BTRSCR_DMV", sender, tl31MissionName, iScore, HUD_COLOUR_PURE_WHITE)
                BREAK
                
            ENDSWITCH
            
        BREAK
        
        CASE FMMC_TYPE_RACE
            
            SWITCH iMissionSubType
                
                CASE ciRACE_SUB_TYPE_STANDARD   
                    PRINTLN("[Feed] - [Social Feed] - PRINT_MY_SCORE_BEAT_TICKER - printing standard race score beaten ticker.")
                    PRINT_TICKER_WITH_PLAYER_NAME_STRING_TIME_AND_STRING("SC_BTRSCR_RACN", sender, iScore, tl31MissionName, FALSE)
                BREAK
                
                CASE ciRACE_SUB_TYPE_GTA    
                    PRINTLN("[Feed] - [Social Feed] - PRINT_MY_SCORE_BEAT_TICKER - printing gta race score beaten ticker.")
                    PRINT_TICKER_WITH_PLAYER_NAME_STRING_TIME_AND_STRING("SC_BTRSCR_RACG", sender, iScore, tl31MissionName, FALSE)
                BREAK
                    
                CASE ciRACE_SUB_TYPE_RALLY      
                    PRINTLN("[Feed] - [Social Feed] - PRINT_MY_SCORE_BEAT_TICKER - printing rally race score beaten ticker.")
                    PRINT_TICKER_WITH_PLAYER_NAME_STRING_TIME_AND_STRING("SC_BTRSCR_RACR", sender, iScore, tl31MissionName, FALSE)
                BREAK
                
                CASE ciRACE_SUB_TYPE_NON_CONTACT
                    PRINTLN("[Feed] - [Social Feed] - PRINT_MY_SCORE_BEAT_TICKER - printing rally race score beaten ticker.")
                    PRINT_TICKER_WITH_PLAYER_NAME_STRING_TIME_AND_STRING("SC_BTRSCR_RACN", sender, iScore, tl31MissionName, FALSE)
                BREAK
                
            ENDSWITCH
            
        BREAK
        
        CASE FMMC_TYPE_BASE_JUMP
            PRINTLN("[Feed] - [Social Feed] - PRINT_MY_SCORE_BEAT_TICKER - printing base jump score beaten ticker.")
            PRINT_TICKER_WITH_PLAYER_NAME_STRING_TIME_AND_STRING("SC_BTRSCR_BJP", sender, iScore, tl31MissionName, FALSE)
        BREAK
        
        CASE FMMC_TYPE_MISSION
            PRINTLN("[Feed] - [Social Feed] - PRINT_MY_SCORE_BEAT_TICKER - printing mission score beaten ticker.")
            PRINT_TICKER_WITH_PLAYER_NAME_STRING_INT_AND_STRING("SC_BTRSCR_SUR", sender, tl31MissionName, iScore, HUD_COLOUR_PURE_WHITE)
        BREAK
        
    ENDSWITCH
    
ENDPROC




FUNC BOOL GET_IS_INBOX_MESSAGE_IMPORTANT(INBOX_MSG_TYPE eType)
    
    IF (eType = crew_message)
        RETURN TRUE
    ENDIF
	
	 IF (eType = gs_award)
        RETURN TRUE
    ENDIF
    
    RETURN FALSE
    
ENDFUNC

#IF IS_DEBUG_BUILD
PROC PRINT_ALL_CURRENT_INBOX_MESSAGES() 
    
    //Get the number of new messages that have come in.
    TEXT_LABEL_63 msg, tl63_type, fromGamerTag, tl63_missionName, t_result, t_ChallengeName, t_CrewName
    INT numNewMessages, winningCrewID, challengingCrewID, iJobsInPlayList, iTimesPlayed, iter
    FLOAT awardTypeAmnt
    INBOX_MSG_TYPE msgType
    
    numNewMessages = SC_INBOX_GET_TOTAL_NUM_MESSAGES()
    UGCStateUpdate_Data structUGCUpdateData
    scrBountyInboxMsg_Data structBountyData
    
    REPEAT (numNewMessages) iter
        IF NOT SC_INBOX_GET_MESSAGE_IS_READ_AT_INDEX(iter)
        
            PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - message at inbox index ", iter, " found.")
            
            msgType = SC_INBOX_GET_MESSAGE_TYPE_AT_INDEX(iter)
            
            SWITCH msgType
                
                CASE INVALID
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - message of type INVALID")
                BREAK
                
                CASE StatUpdate
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - message of type StatUpdate")
                BREAK
                
                CASE FriendCrewJoined
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - message of type FriendCrewJoined")
                BREAK
                
                CASE FriendCreatedCrew
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - message of type FriendCreatedCrew")
                BREAK
                
                CASE mission_verified
                    SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "msg", msg)
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - message of type mission_verified")
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - msg = '", msg, "'.")
                BREAK
                
                CASE rockstar_message
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - message of type rockstar_message")
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - msg = '", msg, "'.")
                BREAK
                
                CASE crew_message
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - message of type crew_message")
                BREAK
                
                CASE UGCStatUpdate
                    SC_INBOX_MESSAGE_GET_UGCDATA(iter, structUGCUpdateData)
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - message of type crew_message")
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - ugcdata data:")
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - tl31MissionContentId:", structUGCUpdateData.tl31MissionContentId) 
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - Score:", structUGCUpdateData.Score) 
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - tl31MissionName:", structUGCUpdateData.tl31MissionName) 
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - tl31CoPlayerName:", structUGCUpdateData.tl31CoPlayerName) 
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - MissionType:", structUGCUpdateData.MissionType) 
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - MissionSubType:", structUGCUpdateData.MissionSubType) 
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - Laps:", structUGCUpdateData.Laps) 
                    IF structUGCUpdateData.bSwapSenderWithCoPlayer
                        PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - bSwapSenderWithCoPlayer: TRUE")
                    ELSE
                        PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - bSwapSenderWithCoPlayer: FALSE")
                    ENDIF
                BREAK
                
                CASE game_award
                    SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "type", tl63_type) 
                    SC_INBOX_MESSAGE_GET_DATA_FLOAT(iter, "amt", awardTypeAmnt)
                    SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "from", fromGamerTag)
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - message of type game_award")
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - tl63_type:", tl63_type)
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - awardTypeAmnt:", awardTypeAmnt)
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - fromGamerTag:", fromGamerTag)
                BREAK
                
                CASE crew_challenge_ended
                    SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "r", t_result)
                    SC_INBOX_MESSAGE_GET_DATA_INT(iter, "wcrew.id", winningCrewID)
                    SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "chname", t_ChallengeName)
                    SC_INBOX_MESSAGE_GET_DATA_STRING (iter, "wcrew.name", t_CrewName)
                    SC_INBOX_MESSAGE_GET_DATA_INT(iter, "ccrew.id", challengingCrewID)
                    SC_INBOX_MESSAGE_GET_DATA_INT(iter, "jobct", iJobsInPlayList)
                    SC_INBOX_MESSAGE_GET_DATA_INT(iter, "playct", iTimesPlayed)
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - message of type crew_challenge_ended")
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - t_result:", t_result)
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - winningCrewID:", winningCrewID)
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - t_ChallengeName:", t_ChallengeName)
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - t_CrewName:", t_CrewName)
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - challengingCrewID:", challengingCrewID)
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - iJobsInPlayList:", iJobsInPlayList)
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - iTimesPlayed:", iTimesPlayed)
                    
                BREAK
                
                CASE mission_published
                    SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "mn", tl63_missionName)
                    SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "gtag", fromGamerTag)
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - message of type mission_published")
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - tl63_missionName:", tl63_missionName)
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - fromGamerTag:", fromGamerTag)
                BREAK
                
                CASE bounty
                    SC_INBOX_GET_BOUNTY_DATA_AT_INDEX(iter, structBountyData)      
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - message of type bounty")
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - Target: ", structBountyData.tl31FromGamerTag )
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - Killer: ", structBountyData.tl31TargetGamerTag )
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - Outcome: ", structBountyData.iOutcome )
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - Cash: ", structBountyData.iCash )
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - Rank: ", structBountyData.iRank )
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - Time: ", structBountyData.iTime )
                BREAK
                
                CASE tournament_winner_message
                    PRINTLN("[Feed] - [Social Feed] - PRINT_ALL_CURRENT_INBOX_MESSAGES - message of type tournament_winner_message")
                BREAK
                
            ENDSWITCH
            
        ENDIF
    ENDREPEAT
    
ENDPROC
#ENDIF


PROC PROCESS_INBOX_MESSAGES(STRUCT_MP_PRESENCE_EVENT_VARS &socialFeedVars, BOOL bProcessMessages)

    IF IS_PAUSE_MENU_ACTIVE()
        EXIT
    ENDIF
    
    IF NOT IS_SCREEN_FADED_IN()
        EXIT
    ENDIF
    
    IF IS_TRANSITION_ACTIVE()
        EXIT
    ENDIF
    
    //Get the number of new messages that have come in.
    INT numNewMessages = SC_INBOX_GET_TOTAL_NUM_MESSAGES() 


    // Challenge vars
//    INT iXpToGive
//    INT iJobsInPlayList
//    INT iTimesPlayed
//    FLOAT fTimesPlayedFactor
//    FLOAT fTemp
                                                
     
    INT iter = 0
    INBOX_MSG_TYPE msgType
    TEXT_LABEL_63 msg
    
//    TEXT_LABEL_63 tl63_type
//    FLOAT awardTypeAmnt
    TEXT_LABEL_63 fromGamerTag
    TEXT_LABEL_63 tl63_missionName
    
    scrBountyInboxMsg_Data bountyMessage

	GET_UGC_CONTENT_STRUCT sTempContentStruct
	CLOUD_LOADED_MISSION_HEADER_DETAILS sTempHeaderDetails


//    TEXT_LABEL_63 t_result = ""
//    TEXT_LABEL_63 t_ChallengeName = "" 
//    TEXT_LABEL_63 t_CrewName = "" 
//    INT winningCrewID = 0
//    INT challengingCrewID = 0

//    NETWORK_CLAN_DESC PlayerCurrentCrewDetails



//    FLOAT AbsFineAmnt                                                            
//    INT i_FineAmnt
//    INT i_BankBalance 
//    BOOL b_NotEnoughBankFunds = FALSE

	INT iRootId = 0
	 

    
    SWITCH socialFeedVars.iInboxProcessStage
        
        // Check for and print all important messages.
        CASE 0

            IF bProcessMessages
            
                REPEAT (numNewMessages) iter
                    
                    IF NOT SC_INBOX_GET_MESSAGE_IS_READ_AT_INDEX(iter)
                    
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - message at inbox index ", iter, " has not been read.")
                        
                        IF IS_ACCOUNT_OVER_17()
						AND IS_ACCOUNT_OVER_17_FOR_UGC()
                            
                            msgType = SC_INBOX_GET_MESSAGE_TYPE_AT_INDEX(iter)
                            
                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - Before switch - event type ai index = ", SC_INBOX_MESSAGE_GET_RAW_TYPE_AT_INDEX(iter))
                            
                            IF GET_IS_INBOX_MESSAGE_IMPORTANT(msgType)
                                
                                // Just now the only important messages are crew_messages.
                                PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - received an inbox message of type crew_message.")
                                
								SWITCH msgType
									CASE crew_message
									
										IF (GET_PROFILE_SETTING(PROFILE_FEED_CREW) = 1)
		                                    PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - PROFILE_FEED_CREW = 1.")
		                                    SC_INBOX_MESSAGE_DO_APPLY(iter)
		                                    IF SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "msg", msg)
		                                        PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - Received message of type crew_message - Message says ", msg)
		                                    ENDIF
		                                    SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
		                                ENDIF
									
									BREAK
									
									CASE gs_award
									
										PROCESS_GS_AWARD_INBOX_MESSAGE(iter)
									
									BREAK
								
								ENDSWITCH
                               
								
								
                                
                            ENDIF
                            
                        ELSE
                            
                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - account is not over 17, marking as read without showing message.")
                            SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                            
                        ENDIF
                        
                    ENDIF
                
                ENDREPEAT
            
            ENDIF
            
            socialFeedVars.iInboxProcessStage++
            
        BREAK
        
        // Check for and print all other messages.
        CASE 1
            
            REPEAT (numNewMessages) iter
                
                IF NOT SC_INBOX_GET_MESSAGE_IS_READ_AT_INDEX(iter)
                    
                    #IF IS_DEBUG_BUILD
                    IF bProcessMessages
                    OR (msgType = rockstar_message)
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - message at inbox index ", iter, " has not been read.")
                    ENDIF
                    #ENDIF
                    
                    msgType = SC_INBOX_GET_MESSAGE_TYPE_AT_INDEX(iter)
                    
                    #IF IS_DEBUG_BUILD
                    IF bProcessMessages
                    OR (msgType = rockstar_message)
                        PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - Before switch - event type ai index = ", SC_INBOX_MESSAGE_GET_RAW_TYPE_AT_INDEX(iter))
                    ENDIF
                    #ENDIF
                    
                    SWITCH(msgType)
                        
                        // Do crew messages too, since they are important and we want to show them. don;t set flag for shown messages though, we only want the first oneshot message to do this.
                        CASE crew_message
                            
                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - received an inbox message of type crew_message.")
                                
                            IF bProcessMessages
                                IF IS_ACCOUNT_OVER_17()
								AND IS_ACCOUNT_OVER_17_FOR_UGC()
                                
                                    IF (GET_PROFILE_SETTING(PROFILE_FEED_CREW) = 1)
                                        PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - PROFILE_FEED_CREW = 1.")
                                        SC_INBOX_MESSAGE_DO_APPLY(iter)
                                        IF SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "msg", msg)
                                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - Received message of type crew_message - Message says ", msg)
                                        ENDIF
                                        SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                                    ENDIF
                                
                                ELSE
                                
                                    PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - account is not over 17, marking message as read without showing feed message.")
                                    SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                                    
                                ENDIF
                            ENDIF
                            
                        BREAK
                        
                        CASE StatUpdate
                        BREAK
                        
                        CASE mission_verified
                            
                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - received an inbox message of type mission_verified.")
                            
                            IF bProcessMessages
                                IF NOT socialFeedVars.bDisplayedAMessage
                                    
                                    IF HAS_IMPORTANT_STATS_LOADED()
                                        IF NETWORK_IS_GAME_IN_PROGRESS()
                                            IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
                                                IF IS_ACCOUNT_OVER_17()
												AND IS_ACCOUNT_OVER_17_FOR_UGC()
                                                    SC_INBOX_MESSAGE_DO_APPLY(iter)
                                                    GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPT_MISSVER", XPTYPE_SOCIALCLUB, XPCATEGORY_SOCIALCLUB_MISSION_VERIFIED, 5000)
                                                    IF SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "msg", msg)
                                                    PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - Received message of type mission_verified - Message says ", msg)
                                                    PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - given 5000 xp for getting a mission verified. ", msg)
                                                    ENDIF
                                                    SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                                                ELSE
                                                    PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - account is not over 17, marking message as read without showing feed message.")
                                                    SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                                                ENDIF
                                                socialFeedVars.bDisplayedAMessage = TRUE
                                            ENDIF
                                        ENDIF
                                    ENDIF
                                    
                                ELSE
                                    
//                                    PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - socialFeedVars.bDisplayedAMessage = TRUE, marking message as read without actually showing it.")
//                                    SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                                    
                                ENDIF
                            ENDIF
                            
                        BREAK
                        
                        // Note this message type does not check for the bProcessMessages flag or the socialFeedVars.bDisplayedAMessage flag. We always want these to show no matter what.
                        CASE rockstar_message
                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - received an inbox message of type rockstar_message.")
                            IF IS_ACCOUNT_OVER_17()
							AND IS_ACCOUNT_OVER_17_FOR_UGC()
							AND g_sMPTunables.bENABLE_ROCKSTARMESSAGE
                                SC_INBOX_MESSAGE_DO_APPLY(iter)
                                IF SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "msg", msg)
                                PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - message says ", msg)
                                ENDIF
                                SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                            ELSE
                                PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - account is not over 17, marking message as read without showing feed message.")
                                SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                            ENDIF
                        BREAK
                        
                        CASE UGCStatUpdate
                            
                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - received an inbox message of type UGCStatUpdate.")
                            
                            IF bProcessMessages
                                IF NOT socialFeedVars.bDisplayedAMessage
                                    
                                    IF IS_ACCOUNT_OVER_17()
									AND IS_ACCOUNT_OVER_17_FOR_UGC()
                                        
										// Added all these under instruction of Bobby for B* 2286272.
										IF GET_CORONA_STATUS() = CORONA_STATUS_IDLE
										AND NOT IS_TRANSITION_SESSION_LAUNCHING() //And a transition session is not launching
										AND NOT IS_PAUSE_MENU_IS_USING_UGC() //And not has the mause menu got a hold of the UGC
										AND NOT TRANSITION_SESSIONS_CLEANUP_AFTER_MISSION() //And not is the game restarting
										AND NOT SHOULD_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH() //And we are not restarting freemode
										AND NOT SHOULD_TRANSITION_SESSION_LAUNCH_MAIN_FM_SCRIPT() //And not is the main freemode script about to relaunch
										AND NOT DOES_CURRENT_FM_MISSION_NEED_CLEANED_UP() //And not if we are in the process of cleaning up a mission
										AND NOT Is_Cloud_Loaded_Mission_Data_Being_Refreshed() // And not if the missions are still in the process of being added/refreshed from a previous refresh
										AND NOT Is_Cloud_Loaded_Mission_Data_Download_In_Progress() // And Not if a mission download request in in progress (generally Gang Attack, or Mission Triggerer)
										AND ( HAS_IMPORTANT_STATS_LOADED() AND ARE_ALL_INSTANCED_TUTORIALS_COMPLETE() )//in the tutoriasl
										AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState != FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION //And a transition session is not being set up
										AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState != FMMC_LAUNCHER_STATE_INITILISE_MISSION //And I'm not initilising a mission
										AND NOT IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()) //And not on a playlist
										AND NOT IS_SCRIPT_UCG_REQUEST_IN_PROGRESS() //and it;s not already getting
										AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE) //And not in propertys
										AND NOT NETWORK_IS_ACTIVITY_SESSION() //And not in Jobs
										AND NOT AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
										AND NOT g_bMissionCreatorActive
	                                        IF SC_INBOX_MESSAGE_GET_UGCDATA(iter, socialFeedVars.sUgcStateUpdateData)
												IF GET_UGC_HEADER_BY_CONTENT_ID(sTempContentStruct, socialFeedVars.sUgcStateUpdateData.tl31MissionContentId, sTempHeaderDetails, iRootId)
		                                            IF sTempContentStruct.bSuccess
														PRINTLN("[Feed] - [Social Feed] - ugcdata data:")
			                                            PRINTLN("[Feed] - [Social Feed] - tl31MissionContentId:", socialFeedVars.sUgcStateUpdateData.tl31MissionContentId) 
			                                            PRINTLN("[Feed] - [Social Feed] - Score:", socialFeedVars.sUgcStateUpdateData.Score) 
			                                            PRINTLN("[Feed] - [Social Feed] - tl31MissionName:", socialFeedVars.sUgcStateUpdateData.tl31MissionName) 
			                                            PRINTLN("[Feed] - [Social Feed] - tl31CoPlayerName:", socialFeedVars.sUgcStateUpdateData.tl31CoPlayerName) 
			                                            PRINTLN("[Feed] - [Social Feed] - MissionType:", socialFeedVars.sUgcStateUpdateData.MissionType) 
			                                            PRINTLN("[Feed] - [Social Feed] - MissionSubType:", socialFeedVars.sUgcStateUpdateData.MissionSubType) 
			                                            PRINTLN("[Feed] - [Social Feed] - Laps:", socialFeedVars.sUgcStateUpdateData.Laps) 
			                                            IF socialFeedVars.sUgcStateUpdateData.bSwapSenderWithCoPlayer
			                                                PRINTLN("[Feed] - [Social Feed] - bSwapSenderWithCoPlayer: TRUE")
			                                            ELSE
			                                                PRINTLN("[Feed] - [Social Feed] - bSwapSenderWithCoPlayer: FALSE")
			                                            ENDIF
			                                            
			                                            PRINT_MY_SCORE_BEAT_TICKER( socialFeedVars.sUgcStateUpdateData.tl31SenderGamerTag, socialFeedVars.sUgcStateUpdateData.Score, 
			                                                                        socialFeedVars.sUgcStateUpdateData.MissionType, socialFeedVars.sUgcStateUpdateData.MissionSubType,
			                                                                        sTempHeaderDetails.tlMissionName  )
			                                            
			                                            SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
			                                            
			                                            socialFeedVars.bDisplayedAMessage = TRUE
													ELSE
														PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - sTempContentStruct.bSuccess is FALSE, marking message as read without showing feed message.")
		                                        		SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
													ENDIF
	                                            ENDIF
	                                        ENDIF
											
										ELSE
//											
//											#IF IS_DEBUG_BUILD
//											
//											IF GET_CORONA_STATUS() != CORONA_STATUS_IDLE
//												PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - GET_CORONA_STATUS() != CORONA_STATUS_IDLE")
//											ENDIF
//											IF IS_TRANSITION_SESSION_LAUNCHING() //And a transition session is not launching
//												PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - IS_TRANSITION_SESSION_LAUNCHING")
//											ENDIF
//											IF IS_PAUSE_MENU_IS_USING_UGC() //IF has the mause menu got a hold of the UGC
//												PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - IS_PAUSE_MENU_IS_USING_UGC")
//											ENDIF
//											IF TRANSITION_SESSIONS_CLEANUP_AFTER_MISSION() //IF is the game restarting
//												PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - TRANSITION_SESSIONS_CLEANUP_AFTER_MISSION")
//											ENDIF
//											IF SHOULD_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH() //And we are not restarting freemode
//												PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - SHOULD_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH")
//											ENDIF
//											IF SHOULD_TRANSITION_SESSION_LAUNCH_MAIN_FM_SCRIPT() //IF is the main freemode script about to relaunch
//												PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - SHOULD_TRANSITION_SESSION_LAUNCH_MAIN_FM_SCRIPT")
//											ENDIF
//											IF DOES_CURRENT_FM_MISSION_NEED_CLEANED_UP() //IF if we are in the process of cleaning up a mission
//												PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - DOES_CURRENT_FM_MISSION_NEED_CLEANED_UP")
//											ENDIF
//											IF Is_Cloud_Loaded_Mission_Data_Being_Refreshed() // IF if the missions are still in the process of being added/refreshed from a previous refresh
//												PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - Is_Cloud_Loaded_Mission_Data_Being_Refreshed")
//											ENDIF
//											IF Is_Cloud_Loaded_Mission_Data_Download_In_Progress() // IF if a mission download request in in progress (generally Gang Attack, or Mission Triggerer)
//												PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - Is_Cloud_Loaded_Mission_Data_Download_In_Progress")
//											ENDIF
//											IF NOT HAS_IMPORTANT_STATS_LOADED()
//												PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - NOt HAS_IMPORTANT_STATS_LOADED")
//											ENDIF
//											IF NOT ARE_ALL_INSTANCED_TUTORIALS_COMPLETE()
//												PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - NOt HAS_FM_TRIGGER_TUT_BEEN_DONE")
//											ENDIF
//											IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION //And a transition session is not being set up
//												PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - FMMC_LAUNCHER_STATE_LOAD_MISSION_FOR_TRANSITION_SESSION")
//											ENDIF
//											IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iFmLauncherGameState = FMMC_LAUNCHER_STATE_INITILISE_MISSION //And I'm not initilising a mission
//												PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - FMMC_LAUNCHER_STATE_INITILISE_MISSION")
//											ENDIF
//											IF IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()) //IF on a playlist
//												PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - IS_PLAYER_ON_A_PLAYLIST")
//											ENDIF
//											IF IS_SCRIPT_UCG_REQUEST_IN_PROGRESS() //and it;s not already getting
//												PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - IS_SCRIPT_UCG_REQUEST_IN_PROGRESS")
//											ENDIF
//											IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE) //IF in propertys
//												PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - IS_PLAYER_IN_PROPERTY")
//											ENDIF
//											
//											#ENDIF
//											
//											PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - marking message as read without showing feed message.")
//                                       		SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
											
										ENDIF
                                        
                                    ELSE
                                    
                                        PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - account is not over 17, marking message as read without showing feed message.")
                                        SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                                    
                                    ENDIF
                                    
                                ELSE
                                    
                                    PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - socialFeedVars.bDisplayedAMessage = TRUE, marking message as read without actually showing it.")
                                    SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                                
                                ENDIF
                            ENDIF
                            
                        BREAK
                        




//                        CASE game_award    //Rewritten this section for TODO 1616548
//                            
//                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - received an inbox message of type game_award.")
//                            
//                            IF bProcessMessages
//                                IF NOT socialFeedVars.bDisplayedAMessage
//                                    
//                                    IF NETWORK_IS_GAME_IN_PROGRESS()
//                                        
//                                        IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE)
//                                            
//                                            IF SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "type", tl63_type) 
//                                                
//                                                PRINTLN("[Feed] - [Social Feed] - [RLB] - PROCESS_INBOX_MESSAGES - game_award - GOT tl63_type = ", tl63_type)
//                                                PRINTLN("[Feed] - [Social Feed] - tl63_type is ", tl63_type)
//                                                
//                                                IF SC_INBOX_MESSAGE_GET_DATA_FLOAT(iter, "amt", awardTypeAmnt)
//                                                    
//                                                    PRINTLN("[Feed] - [Social Feed] - [RLB] - PROCESS_INBOX_MESSAGES - game_award - GOT awardTypeAmnt = ", awardTypeAmnt)
//                                                    
//                                                    IF SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "from", fromGamerTag)
//                                                    PRINTLN("[Feed] - [Social Feed] - [RLB] - PROCESS_INBOX_MESSAGES - game_award - GOT fromGamerTag = ", fromGamerTag)
//
//                                                        IF ARE_STRINGS_EQUAL(tl63_type, "cash")
//                                                            //IF ARE_STRINGS_EQUAL(tl63_type, "1776733566") //"cash")
//                                                                                               
//                                                            IF awardTypeAmnt > 0.0
//                                                              
//                                                                IF ARE_STRINGS_EQUAL (fromGamerTag, "Rockstar")   //1522977 Could reverse last TRUE parameter to remove Large Icon. 
//
//                                                                    PRINTLN("[Feed] - [Social Feed] - [RLB] - PROCESS_INBOX_MESSAGES - Received $", ROUND(awardTypeAmnt), " from SC ", fromGamerTag)
//																	
//																	IF USE_SERVER_TRANSACTIONS()
//																		INT iTransactionID
//																		iTransactionID = 0
//																		TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_EARN_ROCKSTAR, ROUND(awardTypeAmnt), iTransactionID)
//																	ELSE
//                                                                    	NETWORK_EARN_FROM_ROCKSTAR(ROUND(awardTypeAmnt))
//																	ENDIF
//                                                                    
//                                                                    TEXT_LABEL_63 ColourAdjustedTag
//                                                                    ColourAdjustedTag = "~HUD_COLOUR_SOCIAL_CLUB~"
//                                                                    ColourAdjustedTag += fromGamerTag
//                                                                            
//                                                                    IF IS_ACCOUNT_OVER_17()
//																	AND IS_ACCOUNT_OVER_17_FOR_UGC()
//                                                                                                                                           
//                                                                        PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_INT("PIM_TIGC0", ColourAdjustedTag, ROUND(awardTypeAmnt), HUD_COLOUR_PURE_WHITE, TRUE)   // ~a~ ~s~gave you ~g~$~1~
//                                                                            
//                                                                    ENDIF
//
//                                                                ELSE
//                                                                            
//                                                                    PRINTLN("[Feed] - [Social Feed] - [RLB] - PROCESS_INBOX_MESSAGES - Received $", ROUND(awardTypeAmnt), " from Player ", fromGamerTag)
//
//                                                                    GIVE_LOCAL_PLAYER_CASH(ROUND(awardTypeAmnt), TRUE)
//                                                                    SET_PACKED_STAT_BOOL(PACKED_MP_RECIEVED_MONEY_FROM_PLAYER, TRUE)
//
//                                                                    IF IS_ACCOUNT_OVER_17()
//                                                                        PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_INT("PIM_TIGC0", fromGamerTag, ROUND(awardTypeAmnt), HUD_COLOUR_PURE_WHITE, FALSE)   // ~a~ ~s~gave you ~g~$~1~
//                                                                    ENDIF
//                                                                        
//                                                                ENDIF
//
//                                                            ELSE  //Negative award... As a general rule this will be a cash punishment from Rockstar... #1616548
//
//                                                                AbsFineAmnt = ABSF(awardTypeAmnt)
//                                                                i_FineAmnt = ROUND(AbsFineAmnt)
//                                                                i_BankBalance = NETWORK_GET_VC_BANK_BALANCE()
//                                                                b_NotEnoughBankFunds = FALSE
//
//                                                                IF i_FineAmnt > i_BankBalance  //Is the fine greater than the player's bank balance?
//
//                                                                    b_NotEnoughBankFunds = TRUE
//
//                                                                            
//                                                                    IF i_BankBalance > 0
//                                                                        NETWORK_SPENT_FROM_ROCKSTAR(i_BankBalance, TRUE) //Use Bank Account - not even deposited to cover entire fine, so wipe out balance...
//																		// TODO: Call TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION
//                                                                    ENDIF
//
//                                                                //Possible to wipe out wallet here? Not as realistic though!
//                                                                ELSE
//                                                                    
//                                                                    NETWORK_SPENT_FROM_ROCKSTAR(i_FineAmnt, TRUE) //Use Bank Account and withdraw fine.
//																	// TODO: Call TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION
//                                                                ENDIF
//
//                                                                PRINTLN("[Feed] - [Social Feed] - [RLB] - PROCESS_INBOX_MESSAGES - penalised $", i_FineAmnt, " from Player ", fromGamerTag)
//
//                                                                IF IS_ACCOUNT_OVER_17()
//																AND IS_ACCOUNT_OVER_17_FOR_UGC()
//
//                                                                    IF ARE_STRINGS_EQUAL (fromGamerTag, "Rockstar")   //1522977 Could reverse last TRUE parameter to remove Large Icon. 
//
//                                                                        TEXT_LABEL_63 ColourAdjustedTag
//
//                                                                        ColourAdjustedTag = "~HUD_COLOUR_SOCIAL_CLUB~"
//                                                                        ColourAdjustedTag += fromGamerTag
//                                                                                  
//                                                                        IF b_NotEnoughBankFunds = TRUE //Display amount withdrawn from bank...
//                                                                            PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_INT("SC_CASH_PUN", ColourAdjustedTag, i_BankBalance, HUD_COLOUR_PURE_WHITE, TRUE)   // ~a~ ~s~gave you ~g~$~1~      
//                                                                        ELSE
//                                                                            PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_INT("SC_CASH_PUN", ColourAdjustedTag, i_FineAmnt, HUD_COLOUR_PURE_WHITE, TRUE)   // ~a~ ~s~gave you ~g~$~1~
//                                                                        ENDIF
//
//                                                                    ELSE
//                                                                        
//                                                                        PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_INT("SC_CASH_PUN", fromGamerTag, i_FineAmnt, HUD_COLOUR_PURE_WHITE, FALSE)   // ~a~ ~s~gave you ~g~$~1~
//                                                                            
//                                                                    ENDIF                                                                      
//                                               
//                                                                ELSE
//                                                                            
//                                                                    PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - account is not over 17, not showing Penalised feed message.")
//
//                                                                ENDIF
//                                                                               
//                                                            ENDIF
//
//                                                        ELSE
//
//                                                            PRINTLN("[Feed] - [Social Feed] - Game award received but this was not a cash award  tl63_type = ", tl63_type)
//            
//                                                        ENDIF  //End of ARE_STRINGS_EQUAL(tl63_type, "cash") condition check.
//
//
//
//                                                        
//                                                        //IF tl63_type = GET_HASH_KEY("xp")
//                                                        
//                                                        IF ARE_STRINGS_EQUAL(tl63_type, "xp")
//                                                        //IF ARE_STRINGS_EQUAL(tl63_type, "378184122")//"xp")
//                                                            
//                                                            IF HAS_IMPORTANT_STATS_LOADED()
//                                                                
//                                                                PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - XP_AWARDED", ROUND(awardTypeAmnt))
//
//                                                                GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "XPT_XPINBOX", XPTYPE_SOCIALCLUB, XPCATEGORY_SOCIALCLUB_XP_INBOX, ROUND(awardTypeAmnt))
//                                                                
//                                                                IF IS_ACCOUNT_OVER_17()
//																AND IS_ACCOUNT_OVER_17_FOR_UGC()
//                                                                    PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_INT("PIM_TIGC12", fromGamerTag, ROUND(awardTypeAmnt), HUD_COLOUR_PURE_WHITE, TRUE)  // ~a~ ~s~gave you ~1~ XP.
//                                                                ELSE
//                                                                    PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - account is not over 17, not showing feed message.")
//                                                                ENDIF
//                                                                
//                                                                SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
//                                                                socialFeedVars.bDisplayedAMessage = TRUE
//                                                            
//                                                            ENDIF
//                                                            
//                                                        ELSE
//
//                                                            PRINTLN("[Feed] - [Social Feed] - Game award received but this was not a XP award  tl63_type = ", tl63_type)
//                                                            
//                                                            SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
//                                                            socialFeedVars.bDisplayedAMessage = TRUE
//                                                            
//                                                        ENDIF
//                                                        
//                                                        
//                                                        
//                                                    ENDIF
//                                                ENDIF
//                                            ENDIF
//                                        ENDIF
//                                    ENDIF
//                                    
//                                ELSE
//                                    
//                                    PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - socialFeedVars.bDisplayedAMessage = TRUE, marking message as read without actually showing it.")
//                                    SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
//                                    
//                                ENDIF
//                            ENDIF
//                            
//                        BREAK
                        




                        CASE mission_published
                            
                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - received an inbox message of type mission_published.")
                            
                            IF bProcessMessages
                                IF NOT socialFeedVars.bDisplayedAMessage
                                    
                                    IF IS_ACCOUNT_OVER_17()
									AND IS_ACCOUNT_OVER_17_FOR_UGC()
                                        
                                        IF SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "mn", tl63_missionName)
                                        PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - mission published - GOT tl63_missionName = ", tl63_missionName)
                                        
                                            IF SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "gtag", fromGamerTag)
                                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - mission published - GOT fromGamerTag = ", fromGamerTag)
                                                
                                                PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_MISSION_NAME("SC_PUBLISH", fromGamerTag, tl63_missionName, HUD_COLOUR_PURE_WHITE, FALSE)
                                                SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                                                
                                                socialFeedVars.bDisplayedAMessage = TRUE
                                                
                                            ENDIF
                                        
                                        ENDIF
                                    
                                    ELSE
                                        
                                        PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - account is not over 17, marking message as read without showing feed message.")
                                        SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                                        
                                    ENDIF
                                    
                                ELSE
                                    
                                    PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - socialFeedVars.bDisplayedAMessage = TRUE, marking message as read without actually showing it.")
                                    SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                                    
                                ENDIF
                                
                            ENDIF
                            
                        BREAK
                     

                        
                        CASE crew_challenge_ended
                            
                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - received an inbox message of type crew_challenge_ended.")
                            
							SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
							
//                            IF bProcessMessages
//                                IF NOT socialFeedVars.bDisplayedAMessage
//                                    
//                                    //NETWORK_EARN_FROM_CHALLENGE_WIN in FMMC_PlayList_Controller seems to deal with the cash side for these.
//                                                  
//                                    PlayerCurrentCrewDetails = GET_PLAYER_CREW(PLAYER_ID())
//                                    
//                                    IF IS_ACCOUNT_OVER_17()
//									AND IS_ACCOUNT_OVER_17_FOR_UGC()
//                                        
//                                        IF SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "r", t_result)
//                                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - game_award - GOT t_result = ", t_result)
//                                        
//                                            IF ARE_STRINGS_EQUAL (t_result, "won") //The event was won rather than expired...
//
//                                                
//                                                IF SC_INBOX_MESSAGE_GET_DATA_INT(iter, "wcrew.id", winningCrewID)
//
//                                                    IF winningCrewID = PlayerCurrentCrewDetails.Id  //Winning crew is the same as the player's crew
//                                                
//                                                        IF SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "chname", t_ChallengeName)
//                                                            IF SC_INBOX_MESSAGE_GET_DATA_STRING (iter, "wcrew.name", t_CrewName)
//                                                                
//                                                                PRINT_TICKER_WITH_TWO_CUSTOM_STRINGS("SC_C_CHL_WON", t_CrewName, t_ChallengeName) //Congratulations, Crew *name* was victorious in *name*.
//                                                            
//                                                                
//                                                            ENDIF
//                                                        ENDIF
//
//                                                    ELSE  //Player can't be in winning crew any longer...
//
//                                                                                                 
//                                                        IF SC_INBOX_MESSAGE_GET_DATA_INT(iter, "ccrew.id", challengingCrewID)  //Next, check if he's still in the challenging crew
//
//                                                            IF ChallengingCrewID = PlayerCurrentCrewDetails.Id //Therefore his crew must have lost the challenge, as he wasn't in winning crew above but still in the challenging crew.
//
//                                                                IF SC_INBOX_MESSAGE_GET_DATA_STRING (iter, "wcrew.name", t_CrewName)
//                                                                    IF SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "chname", t_ChallengeName)
//                                                                
//                                                                        PRINT_TICKER_WITH_TWO_CUSTOM_STRINGS("SC_C_CHL_LOST", t_CrewName, t_ChallengeName) //Crew *name* has defeated your crew's challenge *name*.
//                                                                        
//                                                                    ENDIF
//                                                                ENDIF
//                                                           
//                                                            ENDIF
//
//                                                        ENDIF
//
//                                                    ENDIF
//
//                                                ENDIF
//                                                
//                                                SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
//                                                socialFeedVars.bDisplayedAMessage = TRUE
//                                            
//                                            ELSE
//                                                
//                                                PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - Crew Challenge Expired! .")
//                                                
//                                                IF HAS_IMPORTANT_STATS_LOADED()
//            
//                                                
//                                                    IF SC_INBOX_MESSAGE_GET_DATA_STRING(iter, "chname", t_ChallengeName)
//                                                        
//                                                        IF SC_INBOX_MESSAGE_GET_DATA_INT(iter, "jobct", iJobsInPlayList)
//                                                            
//                                                            IF SC_INBOX_MESSAGE_GET_DATA_INT(iter, "playct", iTimesPlayed)
//                                                                
//                                                                PRINTLN("[Feed] - [Social Feed] - challenge name = ", t_ChallengeName)
//                                                                PRINTLN("[Feed] - [Social Feed] - Num jobs in challenge = ", iJobsInPlayList)
//                                                                PRINTLN("[Feed] - [Social Feed] - Number of times challenge was played = ", iTimesPlayed)
//                                                                PRINTLN("[Feed] - [Social Feed] - Expired challenge basic RP reward (tuneable) = ", g_sMPTunables.iExpiredChallengeBasicXpReward)
//                                                                PRINTLN("[Feed] - [Social Feed] - Expired challenge Turn Off RP reward (tuneable) = ", g_sMPTunables.iExpiredChallengeBasicTurnOFFXP)
//                                                                
//                                                                
//                                                                fTimesPlayedFactor = TO_FLOAT(iTimesPlayed/10)
//                                                                PRINTLN("[Feed] - [Social Feed] - fTimesPlayedFactor = TO_FLOAT(iTimesPlayed/10) = ", fTimesPlayedFactor)
//                                                                
//                                                                IF fTimesPlayedFactor < 0.5
//                                                                    fTimesPlayedFactor = 0.5
//                                                                    PRINTLN("[Feed] - [Social Feed] - fTimesPlayedFactor < 0.5. Capping to ", fTimesPlayedFactor)
//                                                                ENDIF
//                                                                
//                                                                IF fTimesPlayedFactor > 5.0
//                                                                    fTimesPlayedFactor = 5.0
//                                                                    PRINTLN("[Feed] - [Social Feed] - fTimesPlayedFactor > 5.0. Capping to ", fTimesPlayedFactor)
//                                                                ENDIF
//                                                                
//                                                                fTemp = TO_FLOAT(g_sMPTunables.iExpiredChallengeBasicXpReward * iJobsInPlayList)
//                                                                PRINTLN("[Feed] - [Social Feed] - fTemp = TO_FLOAT(g_sMPTunables.iExpiredChallengeBasicXpReward * iJobsInPlayList) = ", fTemp)
//                                                                
//                                                                fTemp = (fTemp * fTimesPlayedFactor)
//                                                                PRINTLN("[Feed] - [Social Feed] - fTemp = (fTemp * fTimesPlayedFactor) = ", fTemp)
//                                                                
//                                                                iXpToGive = CEIL(fTemp)
//                                                                PRINTLN("[Feed] - [Social Feed] - iXpToGive = CEIL(fTemp) = ", iXpToGive)
//                                                                IF g_sMPTunables.iExpiredChallengeBasicTurnOFFXP = FALSE
//                                                                    iXpToGive = GIVE_LOCAL_PLAYER_XP(eXPTYPE_STANDARD, "", XPTYPE_SOCIALCLUB, XPCATEGORY_SOCIALCLUB_XP_CHALLENGE_EXPIRED, iXpToGive)
//                                                                    PRINTLN("[Feed] - [Social Feed] - iXpToGive after GIVE_LOCAL_PLAYER_XP call = ", iXpToGive)
//                                                                ELSE
//                                                                    PRINTLN("[Feed] - [Social Feed] - iXpToGive skipped as iExpiredChallengeBasicTurnOFFXP = TRUE ")
//                                                                ENDIF
//
//                                                                IF iXpToGive > 0
//                                                                    IF g_sMPTunables.iExpiredChallengeBasicTurnOFFXP 
//                                                                        PRINT_TICKER_WITH_ONE_CUSTOM_STRING("SC_CH_EXP_NO", t_ChallengeName)
//                                                                    ELSE
//                                                                        PRINT_TICKER_WITH_CUSTOM_STRING_AND_INT("SC_CH_EXP", t_ChallengeName, iXpToGive)
//                                                                    ENDIF
//                                                                    
//                                                                #IF IS_DEBUG_BUILD
//                                                                ELSE
//                                                                    PRINTLN("[Feed] - [Social Feed] - iXpToGive is <= 0. Not printing ticker about challenge. ")
//                                                                #ENDIF
//                                                                ENDIF
//                                                                
//                                                                SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
//                                                                socialFeedVars.bDisplayedAMessage = TRUE
//                                                
//                                                            ENDIF
//                                                            
//                                                        ENDIF
//                                                        
//                                                    ENDIF
//                                                
//                                                ENDIF
//                                                
//                                            ENDIF
//                                                                 
//                                        ENDIF
//                                    
//                                    ELSE
//                                        
//                                        PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - account is not over 17, marking message as read without showing feed message.")
//                                        SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
//                                        
//                                    ENDIF
//                                
//                                ELSE
//                                    
//                                    PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - socialFeedVars.bDisplayedAMessage = TRUE, marking message as read without actually showing it.")
//                                    SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
//                                    
//                                ENDIF
//                                
//                            ENDIF

	
                        BREAK
                          
                         CASE bounty
                            
                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - received an inbox message of type bounty.")
                            
                            IF bProcessMessages
                                IF NOT socialFeedVars.bDisplayedAMessage
                                
                                    SC_INBOX_GET_BOUNTY_DATA_AT_INDEX(iter,bountyMessage)
                                    
                                    PRINTLN("[Feed] - [Social Feed] - --- Received bounty message---")
                                    PRINTLN("[Feed] - [Social Feed] - Target: ",bountyMessage.tl31FromGamerTag )
                                    PRINTLN("[Feed] - [Social Feed] - Killer: ",bountyMessage.tl31TargetGamerTag )
                                    PRINTLN("[Feed] - [Social Feed] - Outcome: ",bountyMessage.iOutcome )
                                    PRINTLN("[Feed] - [Social Feed] - Cash: ",bountyMessage.iCash )
                                    PRINTLN("[Feed] - [Social Feed] - Rank: ",bountyMessage.iRank )
                                    PRINTLN("[Feed] - [Social Feed] - Time: ",bountyMessage.iTime )
                                    
                                    IF IS_BIT_SET(bountyMessage.iOutcome,BOUNTY_RESULT_TIME_UP)
                                        IF IS_ACCOUNT_OVER_17()
										AND IS_ACCOUNT_OVER_17_FOR_UGC()
                                            PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_INT("FM_TXT_BNTY7",bountyMessage.tl31FromGamerTag,bountyMessage.iCash)
                                        ELSE
                                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - account is not over 17, not feed message.")
                                        ENDIF
                                        Store_MP_General_Communication("LESTER","3",CHAR_LESTER,"CT_AUD","MPCT_BNTFL")
                                        PRINTLN("[Feed] - [Social Feed] - BOUNTY_OVER: Displaying the failed message (player was one who placed it) - Cross session")
                                    ELIF IS_BIT_SET(bountyMessage.iOutcome,BOUNTY_RESULT_SUCCESS)
                                        Store_MP_General_Communication("LESTER","3",CHAR_LESTER,"CT_AUD","MPCT_BNTSuc")
                                        PRINTLN("[Feed] - [Social Feed] - BOUNTY_OVER: Displaying the success message (player was one who placed it) - Cross session")
                                        IF IS_ACCOUNT_OVER_17()
										AND IS_ACCOUNT_OVER_17_FOR_UGC()
                                            PRINT_TICKER_WITH_TWO_PLAYER_NAME_STRING_AND_INT("FM_TXT_BNTY5",bountyMessage.tl31FromGamerTag,bountyMessage.tl31TargetGamerTag,bountyMessage.iCash)
                                        ELSE
                                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - account is not over 17, not showing feed message.")
                                        ENDIF
                                    ENDIF
                                    SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                                            
                                    socialFeedVars.bDisplayedAMessage = TRUE
                                    
                                ELSE
                                    
                                    PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - socialFeedVars.bDisplayedAMessage = TRUE, marking message as read without actually showing it.")
                                    SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                                    
                                ENDIF
                                
                            ENDIF
                            
                        BREAK   
                        
                        CASE tournament_winner_message
                            
                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - received message of type tournament_winner_message.")
                            
                            IF IS_ACCOUNT_OVER_17()
							AND IS_ACCOUNT_OVER_17_FOR_UGC()
                                IF bProcessMessages
                                    IF NOT socialFeedVars.bDisplayedAMessage
                                        PRINT_TICKER("SC_WON_TOURN")
                                    ELSE
                                        PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - socialFeedVars.bDisplayedAMessage = TRUE, not displaying message.")
                                    ENDIF
                                ELSE
                                    PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - bProcessMessages = FALSE, not displaying message.")
                                ENDIF
                            ELSE
                                PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - IS_ACCOUNT_OVER_17 = FALSE, not displaying message.")
                            ENDIF
                            
                            SET_MP_BOOL_CHARACTER_STAT(MP_STAT_TOURNAMENT_1_WINNER, TRUE)
                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - called SET_MP_BOOL_CHARACTER_STAT(MP_STAT_TOURNAMENT_1_WINNER, TRUE)." )
                            PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - marking message as read.")
                            SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                                    
                        BREAK
						
						CASE gs_award
                            
                            PROCESS_GS_AWARD_INBOX_MESSAGE(iter)
                                    
                        BREAK
                        
                        DEFAULT
                        
                            IF bProcessMessages
                                IF GET_HASH_KEY(SC_INBOX_MESSAGE_GET_RAW_TYPE_AT_INDEX(iter)) != HASH("coupon") // to fix 1498206
                                    PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - Switch Default case - SKIPPING EVENT TYPE ", SC_INBOX_MESSAGE_GET_RAW_TYPE_AT_INDEX(iter))
                                    SC_INBOX_SET_MESSAGE_AS_READ_AT_INDEX(iter)
                                ELSE
                                    PRINTLN("[Feed] - [Social Feed] - PROCESS_INBOX_MESSAGES - Switch Default case - detected a coupon, skipping and not marking as read", SC_INBOX_MESSAGE_GET_RAW_TYPE_AT_INDEX(iter))
                                ENDIF
                            ENDIF
                        
                        BREAK
                    
                    ENDSWITCH
                    
                ENDIF
                
            ENDREPEAT
            
            IF socialFeedVars.bDisplayedAMessage    
                socialFeedVars.iInboxProcessStage = 0
            ENDIF
            
        BREAK
        
    ENDSWITCH
    
ENDPROC

// PURPOSE : Process all the events received in the events queue
PROC MAINTAIN_SOCIAL_FEED_MESSAGES(STRUCT_MP_PRESENCE_EVENT_VARS &socialFeedVars, BOOL bProcessMessages)
    
    INT iCount
    EVENT_NAMES ThisScriptEvent
    
    #IF IS_DEBUG_BUILD
        STRUCT_EVENT_COMMON_DETAILS Details
    #ENDIF
    
    //process the events
    REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
    
        ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
        
        #IF IS_DEBUG_BUILD
            
            IF ThisScriptEvent = EVENT_NETWORK_SCRIPT_EVENT
                GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
            ENDIF
            
        #ENDIF
        
        SWITCH ThisScriptEvent
            
            CASE EVENT_NETWORK_PRESENCE_STAT_UPDATE

                Process_SP_Stat_Update(iCount) // Steven Taylor SP events processing.

                PROCESS_STATUPDATE_EVENT(iCount, socialFeedVars, bProcessMessages) // Mp presence events processing, William.kennedy@rockstarnorth.com
            BREAK
            
            CASE EVENT_NETWORK_CLAN_RANK_CHANGE
                PROCESS_CREW_PROMOTION_DEMOTION_EVENT(iCount, socialFeedVars, bProcessMessages)
            BREAK
            
            CASE EVENT_NETWORK_INBOX_MSGS_RCVD
                // If we want to do any processing only the inbox received event, do it here.
                #IF IS_DEBUG_BUILD
                    PRINTLN("[Feed] - [Social Feed] - EVENT_NETWORK_INBOX_MSGS_RCVD.")
                    PRINT_ALL_CURRENT_INBOX_MESSAGES()
                #ENDIF
				MPGlobalsAmbience.bRecievedEventEmail = TRUE
            BREAK
            

            CASE EVENT_NETWORK_CLAN_JOINED


                //Structure for getting info about crew/clan events such as:
                //  -EVENT_NETWORK_CLAN_JOINED
                /*
                STRUCT STRUCT_CLAN_INFO
                    INT clanId
                    BOOL primary
                ENDSTRUCT
                */
                
                PRINTLN("[Feed] - [Social Feed] - You have JOINED a clan.")
                
            BREAK
            
            
            CASE EVENT_NETWORK_CLAN_LEFT

                PROCESS_CREW_LEFT_EVENT(iCount, socialFeedVars, bProcessMessages)
    
            BREAK


            CASE EVENT_NETWORK_CLAN_INVITE_RECEIVED
                
//              PROCESS_INVITED_TO_CREW_MESSAGE_EVENT(iCount, socialFeedVars, bProcessMessages)
                
            BREAK

            
            CASE EVENT_NETWORK_CLAN_KICKED

                /*
                STRUCT STRUCT_CLAN_INFO
                    INT clanId
                    BOOL primary
                ENDSTRUCT
                */
                PROCESS_CREW_KICKED_EVENT(iCount, socialFeedVars, bProcessMessages)

             BREAK



            CASE EVENT_NETWORK_EMAIL_RECEIVED  //Added by Steve T for Laptop Heist Email. John G. says no associated event info is sent other than the event itself. Simple.

    
                #if IS_DEBUG_BUILD
                
                    cdPrintString("Social_Feed_Controller - EVENT_NETWORK_EMAIL_RECEIVED from MAINTAIN_SOCIAL_FEED_MESSAGES.")
                    cdPrintnl()
                    PRINTLN("[Feed] - [Social Feed] - EVENT_NETWORK_EMAIL_RECEIVED from MAINTAIN_SOCIAL_FEED_MESSAGES.")

                #endif

                                
                PROCESS_NETWORK_EMAIL_RECEIVED_EVENT(socialFeedVars, bProcessMessages)

            BREAK




        ENDSWITCH
        
    ENDREPEAT


    //Steven Taylor.
    IF NOT g_bInMultiplayer //don't allow queued SP message processing in actual multiplayer sessions. Mission creator okay.
        IF i_NumberOfEventsInSP_Queue > 0
        
            Process_SP_Queued_Stat_Update()
        
        ENDIF
    ENDIF


    
    // Process MP inbox messages.
    PROCESS_INBOX_MESSAGES(socialFeedVars, bProcessMessages)
    
    
ENDPROC


/*
FUNC INT GET_FEED_DELAY_VALUE()

    PRINTLN("[Feed] - [Social Feed] - SOCIAL_FEED_CONTROLLER header - Feed Delay value in frontend is set to... ", GET_PROFILE_SETTING(PROFILE_FEED_DELAY))
    
    SWITCH GET_PROFILE_SETTING(PROFILE_FEED_DELAY)
        CASE 0 RETURN 0
        
        CASE 1  RETURN 60000
        
        CASE 2  RETURN 120000
        
        CASE 3  RETURN 180000
        
        CASE 4  RETURN 240000
        
        CASE 5  RETURN 300000
        
        CASE 6  RETURN 600000
        
        CASE 7  RETURN 900000
        
        CASE 8  RETURN 1800000
        
        CASE 9  RETURN 3600000 
    
    ENDSWITCH
    
    RETURN 300000
    
ENDFUNC

*/


PROC MAINTAIN_SOCIAL_FEED(STRUCT_MP_PRESENCE_EVENT_VARS &socialFeedVars)
    
    BOOL bProcessMessages = FALSE //Keeps getting reset to false here.
    
    #IF IS_DEBUG_BUILD
    g_b5MinWindowOpen = FALSE
    #ENDIF
    
    // Only want to process MP messages when feed delay interval has passed. Set flag to ok to process if been delay timer has since last pass of messages.
    IF NOT HAS_NET_TIMER_STARTED(socialFeedVars.stProcessMpMessagesTimer)
        PRINTLN("[Feed] - [Social Feed] - stProcessMpMessagesTimer not started yet, starting.")
        START_NET_TIMER(socialFeedVars.stProcessMpMessagesTimer)
    ELIF HAS_NET_TIMER_EXPIRED(socialFeedVars.stProcessMpMessagesTimer, GET_FEED_DELAY_VALUE())
        IF NOT socialFeedVars.bDisplayedAMessage
            bProcessMessages = TRUE
        ENDIF
    ENDIF
    
    // Process messages.
    MAINTAIN_SOCIAL_FEED_MESSAGES(socialFeedVars, bProcessMessages)
    
    // Process mp presence events.
    IF IS_ACCOUNT_OVER_17()
	AND IS_ACCOUNT_OVER_17_FOR_UGC()
        MAINTAIN_SEND_MP_PRESENCE_EVENTS(socialFeedVars) // Sending of mp presence messages.
    ENDIF
    
    IF bProcessMessages
    
        #IF IS_DEBUG_BUILD
        g_b5MinWindowOpen = TRUE
        #ENDIF
        
        IF socialFeedVars.bDisplayedAMessage
            // Did all processing, reset timer so we wait another 5 minutes.
            RESET_NET_TIMER(socialFeedVars.stProcessMpMessagesTimer)
            socialFeedVars.bDisplayedAMessage = FALSE
            PRINTLN("[Feed] - [Social Feed] - SOCIAL_FEED_CONTROLLER header - Checking reset of this. GET_PROFILE_SETTING(PROFILE_FEED_DELAY) = ", GET_PROFILE_SETTING(PROFILE_FEED_DELAY))
        ENDIF
         
    ENDIF
    
ENDPROC

FUNC BOOL DO_FRIEND_SCORE_COMPARISON(SC_LEADERBOARD_CONTROL_STRUCT& structLBControl, UGCStateUpdate_Data& ugcdata, INT iMyPreviousScore, INT iMyCurrentScore, BOOL &bHaveFriendsToSendTo, INT iMissionType, STRING stUgcUniqueMissionIdentifier, STRING stMissionName, INT iMissionSubType, INT iLaps, BOOL bIAmCoDriver, GAMER_HANDLE &excludeGamer[])
    IF NOT IS_THIS_A_RSTAR_ACTIVITY()
    AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.mdID.idCreator != FMMC_MINI_GAME_CREATOR_ID
        PRINTLN("[Feed] - [Social Feed] - DO_FRIEND_SCORE_COMPARISON - bypassed NOT IS_THIS_A_RSTAR_ACTIVITY().")
        RETURN TRUE
    ENDIF
    SETUP_SOCIAL_CLUB_LEADERBOARD_READ_DATA(structLBControl, iMissionType, stUgcUniqueMissionIdentifier, stMissionName, iMissionSubType, iLaps, bIAmCoDriver)
    
    IF SOCIAL_CLUB_COMPARE_FRIENDS_SEND_MESSAGE(structLBControl, ugcdata, bHaveFriendsToSendTo, iMyPreviousScore, iMyCurrentScore, iMissionType, iMissionSubType, iLaps, stMissionName, excludeGamer)
        PRINTLN("[Feed] - [Social Feed] - DO_FRIEND_SCORE_COMPARISON - completed friend score comparison.")
        RETURN TRUE
    ENDIF
    
    RETURN FALSE
    
ENDFUNC






