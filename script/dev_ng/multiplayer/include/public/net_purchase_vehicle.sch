///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        net_purchase_vehicle.sch																			///																			
///																													///				
/// Description:  Wrapper to purchase the vehicle player is in. Sets the vehicle as personal vehicle.				///
///               To trigger, player needs to be inside a vehicle and initialized player control pressed. 			///
///    			  If player has more than 1 garage, property menu will appear to give option to select.				///
///	USAGE:																											///
///    Purchase vehicle ped is in:																					///
///    	1. Initialize trigger control VEHICLE_PURCHASE_INIT 														///
///   	2. Call PURCHASE_VEHICLE_PED_IS_IN every frame until results are ready IS_VEHICLE_PURCHASE_RESULT_READY		///
///    	3. Once results are ready (VPS_SUCCESS or VPS_FAILED) clean up data CLEANUP_PURCHASE_VEHICLE_DATA			///
///    																												///
///    Purchase this vehicle																						///
///		1. Initialize trigger control VEHICLE_PURCHASE_INIT 														///
///    	2. Set vehicle index SET_SELECTED_PURCHASE_VEHICLE_INDEX													///
///     3. Call PURCHASE_SELECTED_VEHICLE every frame until results are ready IS_VEHICLE_PURCHASE_RESULT_READY		///
///    	4. Once results are ready (VPS_SUCCESS or VPS_FAILED) clean up data CLEANUP_PURCHASE_VEHICLE_DATA			///
///    																												///
///    Optional data:																								///
///     1. To add vehicle mod price to vehicle price SET_PURCHASE_VEHICLE_ADD_MOD_PRICE_TO_VEHICLE_PRICE			///
///     2. To add discount to total amount SET_PURCHASE_VEHICLE_DISCOUNT_AMOUNT. Discount should be value between   ///
///			0 - 100	e.g. if you want to discount 10% set the value to 10											///													
///    																												///
///    																												///
/// Written by:  Ata																								///		
/// Date:  		19/05/2021																							///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "net_include.sch"
USING "net_vehicle_setup.sch"
USING "menu_cursor_public.sch"
USING "website_public.sch"
USING "net_realty_new.sch"
USING "net_mod_value.sch"

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ ENUMS ╞══════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
ENUM VEHICLE_PURCHASE_STATE
	VPS_DEFAULT = 0,
	VPS_PURCHASE_WARNING,
	VPS_CREATE_DUMMY_VEHICLE,
	VPS_PROPERTY_MENU,
	VPS_TAKE_MOENY,
	VPS_FAILED,
	VPS_SUCCESS
ENDENUM

ENUM VEHICLE_PURCHASE_FAIL_REASON
	VPFR_DEFAULT,
	VPFR_INSUFFICIENT_FUNDS
ENDENUM

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ CONST ╞══════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

CONST_INT ciPURCHASE_VEHICLE_DELAY_TIME_CANCELLED_MENU		1500

#IF FEATURE_GEN9_EXCLUSIVE
CONST_INT VEH_PURCHASE_LOCAL_BS_THIS_IS_REMOTE_PLAYER_VEHICLE						0
#ENDIF
CONST_INT VEH_PURCHASE_LOCAL_BS_PURCHASE_IN_PROGRESS								1
CONST_INT VEH_PURCHASE_LOCAL_BS_PROCEED_WITH_PURCHASE								2
CONST_INT VEH_PURCHASE_LOCAL_BS_SKIP_WARNING_SCREEN									3

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ FUNCTION POINTER ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
/// PURPOSE:
///    Get vehicle purchase trigger control 
TYPEDEF PROC T_VEHICLE_PURCHASE_TRIGGER_CONTROLS(CONTROL_TYPE &control, CONTROL_ACTION &action)

///    Get selected vehicle which we want to purchase
TYPEDEF FUNC VEHICLE_INDEX T_VEHICLE_PURCHASE_SELECTED_VEHICLE()

///    Get discount amount to apply for vehicle purchase
TYPEDEF FUNC FLOAT T_VEHICLE_PURCHASE_DISCOUNT()

///    Check if we should add mod price to vehicle price
TYPEDEF FUNC BOOL T_VEHICLE_PURCHASE_ADD_MOD_PRICE()

///    Check if we should add custom delivery message
TYPEDEF FUNC BOOL T_VEHICLE_PURCHASE_ADD_CUSTOM_MESSAGE()

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ DATA STRUCTURES ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
STRUCT VEHICLE_PURCHASE_STRUCT
	INT iLocalBS
	INT iResultSlot = -1
	INT iDisplaySlot = -1
	INT iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
	
	VEHICLE_INDEX dummyVehicle
	VEHICLE_INDEX vehicleToPurchase
	PLAYER_INDEX remotePlayer	
	
	// Function pointers
	T_VEHICLE_PURCHASE_TRIGGER_CONTROLS fpVehPurchaseControls
	T_VEHICLE_PURCHASE_SELECTED_VEHICLE fpSelectedVehicle
	T_VEHICLE_PURCHASE_DISCOUNT fpDiscountAmount
	T_VEHICLE_PURCHASE_ADD_MOD_PRICE fpAddModPrice
	T_VEHICLE_PURCHASE_ADD_CUSTOM_MESSAGE fpAddCustomMessage
	
	REPLACE_MP_VEH_OR_PROP_MENU replaceVehMenu
	VEHICLE_SETUP_STRUCT_MP vehicleSetup
	VEHICLE_PURCHASE_STATE vehiclePurchaseState
	VEHICLE_PURCHASE_FAIL_REASON eFailReason = VPFR_DEFAULT
	SCRIPT_TIMER timerPurchaseDelay
	enumCharacterList eCharacter = CHAR_LS_CAR_MEET_MPEMAIL
ENDSTRUCT

DEBUGONLY FUNC STRING GET_VEHICLE_PURCHASE_STATE_NAME(VEHICLE_PURCHASE_STATE eState)
	SWITCH	eState
		CASE VPS_DEFAULT 				RETURN "VPS_DEFAULT"
		CASE VPS_FAILED					RETURN "VPS_FAILED"
		CASE VPS_SUCCESS				RETURN "VPS_SUCCESS"	
		CASE VPS_PURCHASE_WARNING		RETURN "VPS_PURCHASE_WARNING"
		CASE VPS_CREATE_DUMMY_VEHICLE	RETURN "VPS_CREATE_DUMMY_VEHICLE"
		CASE VPS_TAKE_MOENY				RETURN "VPS_TAKE_MOENY"
		CASE VPS_PROPERTY_MENU			RETURN "VPS_PROPERTY_MENU"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC

DEBUGONLY FUNC STRING GET_VEHICLE_PURCHASE_FAIL_REASON_NAME(VEHICLE_PURCHASE_FAIL_REASON eReason)
	SWITCH	eReason
		CASE VPFR_DEFAULT 				RETURN "VPFR_DEFAULT"
		CASE VPFR_INSUFFICIENT_FUNDS	RETURN "VPFR_INSUFFICIENT_FUNDS"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC

DEBUGONLY FUNC STRING GET_VEHICLE_PURCHASE_LOCAL_BIT_SET_NAME(INT iBit)
	SWITCH	iBit
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE VEH_PURCHASE_LOCAL_BS_THIS_IS_REMOTE_PLAYER_VEHICLE 				RETURN "VEH_PURCHASE_LOCAL_BS_THIS_IS_REMOTE_PLAYER_VEHICLE"
		#ENDIF
		CASE VEH_PURCHASE_LOCAL_BS_PURCHASE_IN_PROGRESS							RETURN "VEH_PURCHASE_LOCAL_BS_PURCHASE_IN_PROGRESS"
		CASE VEH_PURCHASE_LOCAL_BS_PROCEED_WITH_PURCHASE						RETURN "VEH_PURCHASE_LOCAL_BS_PROCEED_WITH_PURCHASE"
		CASE VEH_PURCHASE_LOCAL_BS_SKIP_WARNING_SCREEN							RETURN "VEH_PURCHASE_LOCAL_BS_SKIP_WARNING_SCREEN"
	ENDSWITCH
	
	RETURN "INVALID"
ENDFUNC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ SETTER/GETTER ╞══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
/// PURPOSE:
///    Set purchase vehicle state
/// PARAMS: 
///    eState - new state
PROC SET_VEHICLE_PURCHASE_STATE(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, VEHICLE_PURCHASE_STATE eState)
	IF vehPurchaseStruct.vehiclePurchaseState != eState
		PRINTLN("[PURCHASE_VEH_PED_IS_IN] SET_VEHICLE_PURCHASE_STATE From: ", GET_VEHICLE_PURCHASE_STATE_NAME(vehPurchaseStruct.vehiclePurchaseState), " To: ", GET_VEHICLE_PURCHASE_STATE_NAME(eState))
		vehPurchaseStruct.vehiclePurchaseState = eState
	ENDIF
ENDPROC

PROC SET_VEHICLE_PURCHASE_FAIL_REASON(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, VEHICLE_PURCHASE_FAIL_REASON eReason)
	IF vehPurchaseStruct.eFailReason != eReason
		PRINTLN("[PURCHASE_VEH_PED_IS_IN] SET_VEHICLE_PURCHASE_FAIL_REASON From: ", GET_VEHICLE_PURCHASE_FAIL_REASON_NAME(vehPurchaseStruct.eFailReason), " To: ", GET_VEHICLE_PURCHASE_FAIL_REASON_NAME(eReason))
		vehPurchaseStruct.eFailReason = eReason
	ENDIF
ENDPROC

FUNC VEHICLE_PURCHASE_FAIL_REASON GET_VEHICLE_PURCHASE_FAILED_REASON(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	RETURN vehPurchaseStruct.eFailReason
ENDFUNC

/// PURPOSE:
///    Get purchase vehicle state
FUNC VEHICLE_PURCHASE_STATE GET_VEHICLE_PURCHASE_STATE(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	RETURN vehPurchaseStruct.vehiclePurchaseState
ENDFUNC

/// PURPOSE:
///    Check if vehicle purchase local bit is set
FUNC BOOL IS_VEH_PURCHASE_LOCAL_BIT_SET(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, INT iBit)
	RETURN IS_BIT_SET(vehPurchaseStruct.iLocalBS, iBit)
ENDFUNC

/// PURPOSE:
///    Set vehicle purchase local bit set
PROC SET_VEH_PURCHASE_LOCAL_BS(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, INT iBit)
	IF NOT IS_VEH_PURCHASE_LOCAL_BIT_SET(vehPurchaseStruct, iBit)
		SET_BIT(vehPurchaseStruct.iLocalBS, iBit)
		PRINTLN("[PURCHASE_VEH_PED_IS_IN] SET_VEH_PURCHASE_LOCAL_BS: ", GET_VEHICLE_PURCHASE_LOCAL_BIT_SET_NAME(iBit))
	ENDIF
ENDPROC

/// PURPOSE:
///    Clear vehicle purchase local bit set
PROC CLEAR_VEH_PURCHASE_LOCAL_BS(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, INT iBit)
	IF IS_VEH_PURCHASE_LOCAL_BIT_SET(vehPurchaseStruct, iBit)
		CLEAR_BIT(vehPurchaseStruct.iLocalBS, iBit)
		PRINTLN("[PURCHASE_VEH_PED_IS_IN] CLEAR_VEH_PURCHASE_LOCAL_BS: ", GET_VEHICLE_PURCHASE_LOCAL_BIT_SET_NAME(iBit))
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if vehicle purchase started and in progress
FUNC BOOL IS_VEHICLE_PURCHASE_IN_PROGRESS(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	RETURN IS_VEH_PURCHASE_LOCAL_BIT_SET(vehPurchaseStruct, VEH_PURCHASE_LOCAL_BS_PURCHASE_IN_PROGRESS)
ENDFUNC

/// PURPOSE:
///    Sets the vehicle purchase trigger controls
PROC SET_VEHICLE_PURCHASE_TRIGGER_CONTROLS(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, T_VEHICLE_PURCHASE_TRIGGER_CONTROLS fpVehPurchaseControls)
	vehPurchaseStruct.fpVehPurchaseControls = fpVehPurchaseControls
ENDPROC

/// PURPOSE:
///    Gets vehicle purchase trigger controls
FUNC T_VEHICLE_PURCHASE_TRIGGER_CONTROLS GET_VEHICLE_PURCHASE_TRIGGER_CONTROLS_FP(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	RETURN vehPurchaseStruct.fpVehPurchaseControls
ENDFUNC

/// PURPOSE:
///    Sets the vehicle purchase trigger controls
PROC SET_SELECTED_PURCHASE_VEHICLE_INDEX(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, T_VEHICLE_PURCHASE_SELECTED_VEHICLE fpSelectedVehicle)
	vehPurchaseStruct.fpSelectedVehicle = fpSelectedVehicle
ENDPROC

/// PURPOSE:
///    Gets selected vehicle to purchase 
FUNC T_VEHICLE_PURCHASE_SELECTED_VEHICLE GET_SELECTED_VEHICLE_PURCHASE_INDEX_FP(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	RETURN vehPurchaseStruct.fpSelectedVehicle
ENDFUNC

/// PURPOSE:
///    Get selected vehicle function pointer
FUNC VEHICLE_INDEX GET_SELECTED_VEHICLE(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	T_VEHICLE_PURCHASE_SELECTED_VEHICLE fpSelectedVehicle = GET_SELECTED_VEHICLE_PURCHASE_INDEX_FP(vehPurchaseStruct)
	
	IF fpSelectedVehicle = NULL
		PRINTLN("[PURCHASE_VEH_PED_IS_IN] GET_SELECTED_VEHICLE is null")
		RETURN NULL
	ENDIF

	RETURN CALL fpSelectedVehicle()
ENDFUNC

/// PURPOSE:
///    Get vehicle purchase trigger controls function pointer
PROC GET_VEHICLE_PURCHASE_TRIGGER_CONTROLS(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, CONTROL_TYPE &control, CONTROL_ACTION &action)
	T_VEHICLE_PURCHASE_TRIGGER_CONTROLS fpVehPurchaseControls = GET_VEHICLE_PURCHASE_TRIGGER_CONTROLS_FP(vehPurchaseStruct)
	
	IF fpVehPurchaseControls = NULL
		PRINTLN("[PURCHASE_VEH_PED_IS_IN] GET_VEHICLE_PURCHASE_TRIGGER_CONTROLS - helm anim dic null")
		EXIT
	ENDIF

	CALL fpVehPurchaseControls(control, action)
ENDPROC


/// PURPOSE:
///    Sets the vehicle purchase discount amount
PROC SET_PURCHASE_VEHICLE_DISCOUNT_AMOUNT(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, T_VEHICLE_PURCHASE_DISCOUNT fpDiscountAmount)
	vehPurchaseStruct.fpDiscountAmount = fpDiscountAmount
ENDPROC

/// PURPOSE:
///    Get vehicle purchase discount amount 
FUNC T_VEHICLE_PURCHASE_DISCOUNT GET_PURCHASE_VEHICLE_DISCOUNT_AMOUNT_FP(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	RETURN vehPurchaseStruct.fpDiscountAmount
ENDFUNC

/// PURPOSE:
///    Get discount amount function pointer
FUNC FLOAT GET_PURCHASE_VEHICLE_DISCOUNT_AMOUNT(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	T_VEHICLE_PURCHASE_DISCOUNT fpDiscountAmount = GET_PURCHASE_VEHICLE_DISCOUNT_AMOUNT_FP(vehPurchaseStruct)
	
	IF fpDiscountAmount = NULL
		PRINTLN("[PURCHASE_VEH_PED_IS_IN] GET_PURCHASE_VEHICLE_DISCOUNT_AMOUNT_FP is null")
		RETURN 0.0
	ENDIF

	RETURN CALL fpDiscountAmount()
ENDFUNC

/// PURPOSE:
///    Sets add mod price to vehicle price
PROC SET_PURCHASE_VEHICLE_ADD_MOD_PRICE_TO_VEHICLE_PRICE(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, T_VEHICLE_PURCHASE_ADD_MOD_PRICE fpAddModPrice)
	vehPurchaseStruct.fpAddModPrice = fpAddModPrice
ENDPROC

/// PURPOSE:
///    Should add mod price to vehicle price
FUNC T_VEHICLE_PURCHASE_ADD_MOD_PRICE GET_PURCHASE_VEHICLE_ADD_MOD_PRICE_FP(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	RETURN vehPurchaseStruct.fpAddModPrice
ENDFUNC

/// PURPOSE:
///    Get should add mod price function pointer
FUNC BOOL GET_PURCHASE_VEHICLE_SHOULD_ADD_MOD_PRICE(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	T_VEHICLE_PURCHASE_ADD_MOD_PRICE fpAddModPrice = GET_PURCHASE_VEHICLE_ADD_MOD_PRICE_FP(vehPurchaseStruct)
	
	IF fpAddModPrice = NULL
		PRINTLN("[PURCHASE_VEH_PED_IS_IN] GET_PURCHASE_VEHICLE_ADD_MOD_PRICE_FP is null")
		RETURN FALSE
	ENDIF

	RETURN CALL fpAddModPrice()
ENDFUNC

/// PURPOSE:
///    Sets add custom delivery message
PROC SET_PURCHASE_VEHICLE_ADD_CUSTOM_DELIVERY_MESSAGE(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, T_VEHICLE_PURCHASE_ADD_CUSTOM_MESSAGE fpAddCustomMessage)
	vehPurchaseStruct.fpAddCustomMessage = fpAddCustomMessage
ENDPROC

/// PURPOSE:
///    Should add custom delivery message
FUNC T_VEHICLE_PURCHASE_ADD_CUSTOM_MESSAGE GET_PURCHASE_VEHICLE_CUSTOM_DELIVERY_MESSAGE_FP(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	RETURN vehPurchaseStruct.fpAddCustomMessage
ENDFUNC

/// PURPOSE:
///    Get should add custom delivery message function pointer
FUNC BOOL GET_PURCHASE_VEHICLE_SHOULD_ADD_CUSTOM_DELIVERY_MESSAGE(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	T_VEHICLE_PURCHASE_ADD_CUSTOM_MESSAGE fpAddCustomMessage = GET_PURCHASE_VEHICLE_CUSTOM_DELIVERY_MESSAGE_FP(vehPurchaseStruct)
	
	IF fpAddCustomMessage = NULL
		PRINTLN("[PURCHASE_VEH_PED_IS_IN] GET_PURCHASE_VEHICLE_SHOULD_ADD_MOD_PRICE is null")
		RETURN FALSE
	ENDIF

	RETURN CALL fpAddCustomMessage()
ENDFUNC

PROC SET_PURCHASE_VEHICLE_DELIVERY_CHARACTER(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, enumCharacterList eCharacter)
	IF vehPurchaseStruct.eCharacter != eCharacter
		PRINTLN("[NET_PURCHASE_VEHICLE] SET_PURCHASE_VEHICLE_DELIVERY_CHARACTER is: ", ENUM_TO_INT(eCharacter))
		vehPurchaseStruct.eCharacter = eCharacter
	ENDIF
ENDPROC

FUNC enumCharacterList GET_PURCHASE_VEHICLE_DELIVERY_CHARACTER(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	RETURN vehPurchaseStruct.eCharacter
ENDFUNC

#IF FEATURE_GEN9_EXCLUSIVE
/// PURPOSE:
///    Check if we should give player free HSW upgrade voucher
FUNC BOOL SHOULD_REWARD_FREE_HSW_UPGRADE(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	IF NOT IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT IS_VEH_PURCHASE_LOCAL_BIT_SET(vehPurchaseStruct, VEH_PURCHASE_LOCAL_BS_THIS_IS_REMOTE_PLAYER_VEHICLE)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC
#ENDIF

/// PURPOSE:
///    Check if vehicle purchase result ready
/// RETURNS: return true if state is VPS_SUCCESS OR VPS_FAILED
///    
FUNC BOOL IS_VEHICLE_PURCHASE_RESULT_READY(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	IF vehPurchaseStruct.vehiclePurchaseState = VPS_SUCCESS
		RETURN TRUE
	ENDIF
	
	IF vehPurchaseStruct.vehiclePurchaseState = VPS_FAILED
	AND GET_VEHICLE_PURCHASE_FAILED_REASON(vehPurchaseStruct) != VPFR_INSUFFICIENT_FUNDS	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ADD_PURCHASE_VEHICLE_EXTRAS(MODEL_NAMES eVehModel, VEHICLE_SETUP_STRUCT &vehSetup)
	IF (eVehModel = FORMULA)
	OR (eVehModel = FORMULA2)
	OR (eVehModel = OPENWHEEL1)
	OR (eVehModel = OPENWHEEL2)
	OR (eVehModel = MAMBA)
	OR (eVehModel = CASCO)
		SET_BIT(vehSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
		SET_BIT(vehSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
		SET_BIT(vehSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
		SET_BIT(vehSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_3)
		SET_BIT(vehSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_4)
		SET_BIT(vehSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_5)
		SET_BIT(vehSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_6)
		SET_BIT(vehSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_7)
	ELIF (eVehModel = ISSI7)
		SET_BIT(vehSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
	ENDIF
ENDPROC

FUNC BOOL SHOULD_START_VEHICLE_PURCHASE(BOOL bIsSelectedVehicle)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
		RETURN FALSE
	ENDIF
	
	IF IS_WARNING_MESSAGE_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SCREEN_FADED_IN()
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		RETURN FALSE
	ENDIF	
	
	IF IS_BROWSER_OPEN()
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		RETURN FALSE
	ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		RETURN FALSE
	ENDIF	
	
	IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		RETURN FALSE
	ENDIF	
	
	IF NOT bIsSelectedVehicle
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			RETURN FALSE
		ENDIF	
	ENDIF	
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_USE_ALT_PRICES(MODEL_NAMES eModel)
	SITE_BUYABLE_VEHICLE sbv = GET_WEBSITE_BUYABLE_VEHICLE_FROM_MODEL(eModel)
	
	IF IS_SBV_AN_ALTERNATE_VEHICLE(sbv, INVALID_WEBSITE_INDEX, BCV_NO_COLOUR)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_PRICE_OF_VEHICLE(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, VEHICLE_INDEX veh, BOOL bGetBasePrice = FALSE)
	IF IS_ENTITY_ALIVE(veh)
		BOOL bUseAltPrices = SHOULD_USE_ALT_PRICES(GET_ENTITY_MODEL(veh))
		INT iPrice
		
		SITE_BUYABLE_VEHICLE sbv = GET_WEBSITE_BUYABLE_VEHICLE_FROM_MODEL(GET_ENTITY_MODEL(veh))
		iPrice = GET_BUYABLE_VEHICLE_PRICE_MP(INVALID_WEBSITE_INDEX, sbv, bUseAltPrices)
		
		IF iPrice <= -1
			PRINTLN("GET_PRICE_OF_VEHICLE - Using GET_VEHICLE_VALUE")
			VEHICLE_VALUE_DATA vehicleValue
			GET_VEHICLE_VALUE(vehicleValue, GET_ENTITY_MODEL(veh), bUseAltPrices)
			iPrice = vehicleValue.iStandardPrice
		ENDIF
		
		PRINTLN("GET_PRICE_OF_VEHICLE - iPrice =  ", iPrice)
		
		IF NOT bGetBasePrice
			IF GET_PURCHASE_VEHICLE_SHOULD_ADD_MOD_PRICE(vehPurchaseStruct)
				g_ModVehicle = veh
				g_eModPriceVariation =  INT_TO_ENUM(MOD_PRICE_VARIATION_ENUM, GET_VEHICLE_MOD_PRICE_VARIATION_FOR_CATALOGUE(GET_ENTITY_MODEL(veh)))
				
				IF IS_VEHICLE_A_HSW_VEHICLE(veh)
					iPrice += GET_CARMOD_MENU_OPTION_COST(CMM_SUPERMOD_ICE, 0)
					CPRINTLN(DEBUG_SHOPS, "GET_PRICE_OF_VEHICLE - HSW Upgrade = $", GET_CARMOD_MENU_OPTION_COST(CMM_SUPERMOD_ICE, 0))
				ENDIF
				
				iPrice = iPrice + GET_CARMOD_MOD_SELL_VALUE(veh, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
				PRINTLN("GET_PRICE_OF_VEHICLE - Added on mod values. New price =  ", iPrice)
			ENDIF
			
			IF GET_PURCHASE_VEHICLE_DISCOUNT_AMOUNT(vehPurchaseStruct) != 0.0
				PRINTLN("GET_PRICE_OF_VEHICLE - Applying discount: ", GET_STRING_FROM_FLOAT(GET_PURCHASE_VEHICLE_DISCOUNT_AMOUNT(vehPurchaseStruct)))
				iPrice = CEIL(iPrice * ((100 - ABSF(GET_PURCHASE_VEHICLE_DISCOUNT_AMOUNT(vehPurchaseStruct))) / 100 ))
			ENDIF
		ENDIF
		
		RETURN iPrice
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC STRING GET_NAME_OF_VEHICLE(MODEL_NAMES eModel)
	IF IS_MODEL_A_VEHICLE(eModel)
		STRING strVehNameTag = GET_LABEL_BUYABLE_VEHICLE(GET_WEBSITE_BUYABLE_VEHICLE_FROM_MODEL(eModel))
			
		IF IS_STRING_NULL_OR_EMPTY(strVehNameTag)
			strVehNameTag = GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(eModel)
		ENDIF
		
		RETURN strVehNameTag
	ENDIF
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Checks if vehicle is safe to purchase
FUNC BOOL IS_THIS_VEHICLE_SAFE_TO_PURCHASE(VEHICLE_INDEX vehicleToCheck)
	IF IS_ENTITY_DEAD(vehicleToCheck)
		RETURN FALSE
	ENDIF
	
	MODEL_NAMES vehicleModel = GET_ENTITY_MODEL(vehicleToCheck)
	VEHICLE_VALUE_DATA vehicleValue
	
	IF NOT IS_VEHICLE_AVAILABLE_PAST_CURRENT_TIME(vehicleModel)
		RETURN FALSE
	ENDIF
	
	IF NOT GET_VEHICLE_VALUE(vehicleValue, vehicleModel, FALSE, 1)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡ INITIALISE ╞═══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛
/// PURPOSE:
///    Initilize vehicle purchase data. This must be called in init state of script in order vehicle purchase to work
/// PARAMS:
///    VEHICLE_PURCHASE_STRUCT - vehicle purchase data
///    fpVehPurchaseControls - controls used to trigger the vehicel puchase (when player is in vehicle)
PROC VEHICLE_PURCHASE_INIT(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct
						, T_VEHICLE_PURCHASE_TRIGGER_CONTROLS fpVehPurchaseControls)						
	
	SET_VEHICLE_PURCHASE_TRIGGER_CONTROLS(vehPurchaseStruct, fpVehPurchaseControls)
	PRINTLN("[PURCHASE_VEH_PED_IS_IN] INIT - Finished")
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ CLEANUP ╞════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Cleanup dummy vehicle data and delete vehicle
/// PARAMS:
///    vehPurchaseStruct - 
PROC CLEANUP_DUMMY_VEHICLE_DATA(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	IF DOES_ENTITY_EXIST(vehPurchaseStruct.dummyVehicle)
		DELETE_VEHICLE(vehPurchaseStruct.dummyVehicle)
		PRINTLN("[PURCHASE_VEH_PED_IS_IN] CLEANUP_DUMMY_VEHICLE_DATA delete vehicle")
	ENDIF
	
	IF vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel)
	ENDIF	
	
	vehPurchaseStruct.iResultSlot = -1
	vehPurchaseStruct.iDisplaySlot = -1
	
	vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
	RESET_VEHICLE_SETUP_STRUCT_MP(vehPurchaseStruct.vehicleSetup)
ENDPROC

/// PURPOSE:
///    Cleanup purchase vehicle data
PROC CLEANUP_PURCHASE_VEHICLE_DATA(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, BOOL bEnablePlayerControl = TRUE)
	vehPurchaseStruct.iLocalBS = 0

	CLEANUP_DUMMY_VEHICLE_DATA(vehPurchaseStruct)
	
	vehPurchaseStruct.replaceVehMenu.iBS = 0
	vehPurchaseStruct.replaceVehMenu.iButtonBS = 0
	
	IF bEnablePlayerControl
		IF IS_NET_PLAYER_OK(PLAYER_ID())
		AND IS_SKYSWOOP_AT_GROUND()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
	
	vehPurchaseStruct.vehicleToPurchase = NULL
	vehPurchaseStruct.remotePlayer = NULL
	
	SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_DEFAULT)
	SET_VEHICLE_PURCHASE_FAIL_REASON(vehPurchaseStruct, VPFR_DEFAULT)
	PRINTLN("[PURCHASE_VEH_PED_IS_IN] CLEANUP_PURCHASE_VEHICLE_DATA")
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════════╡ PROC/FUNC ╞═══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Maintain purchase default state. Checks if we are safe to start the purchase and player has press the trigger control.
PROC MAINTAIN_PURCHASE_VEHICLE_DEFAULT_STATE(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, BOOL bIsSelected)
	IF NOT SHOULD_START_VEHICLE_PURCHASE(bIsSelected)
		EXIT
	ENDIF

	IF NOT bIsSelected
		vehPurchaseStruct.vehicleToPurchase = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	ELSE
		vehPurchaseStruct.vehicleToPurchase = GET_SELECTED_VEHICLE(vehPurchaseStruct)
	ENDIF
	
	IF NOT IS_THIS_VEHICLE_SAFE_TO_PURCHASE(vehPurchaseStruct.vehicleToPurchase)
		EXIT
	ENDIF

	CONTROL_TYPE control
	CONTROL_ACTION action
	GET_VEHICLE_PURCHASE_TRIGGER_CONTROLS(vehPurchaseStruct, control, action)
	
	IF NOT IS_CONTROL_PRESSED(control, action)
	AND NOT IS_VEH_PURCHASE_LOCAL_BIT_SET(vehPurchaseStruct, VEH_PURCHASE_LOCAL_BS_PROCEED_WITH_PURCHASE)
		EXIT
	ENDIF

	SET_VEH_PURCHASE_LOCAL_BS(vehPurchaseStruct, VEH_PURCHASE_LOCAL_BS_PURCHASE_IN_PROGRESS)
	CLEAR_VEH_PURCHASE_LOCAL_BS(vehPurchaseStruct, VEH_PURCHASE_LOCAL_BS_PROCEED_WITH_PURCHASE)
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_VEHICLE_A_PERSONAL_VEHICLE(vehPurchaseStruct.vehicleToPurchase)
	AND NOT IS_VEHICLE_MY_PERSONAL_VEHICLE(vehPurchaseStruct.vehicleToPurchase)
		vehPurchaseStruct.remotePlayer = GET_OWNER_OF_PERSONAL_VEHICLE(vehPurchaseStruct.vehicleToPurchase)
		PRINTLN("[PURCHASE_VEH_PED_IS_IN] MAINTAIN_PURCHASE_VEHICLE_DEFAULT_STATE cloning remote player's: ", GET_PLAYER_NAME(vehPurchaseStruct.remotePlayer), " vehicle")
		SET_VEH_PURCHASE_LOCAL_BS(vehPurchaseStruct, VEH_PURCHASE_LOCAL_BS_THIS_IS_REMOTE_PLAYER_VEHICLE)
	ENDIF
	#ENDIF
	
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	GET_VEHICLE_SETUP_MP(vehPurchaseStruct.vehicleToPurchase, vehPurchaseStruct.vehicleSetup)
	
	// Add random plate to bypass dupe detector
	TEXT_LABEL_15 sRandPlate = GENERATE_RANDOM_NUMBER_PLATE()
	vehPurchaseStruct.vehicleSetup.VehicleSetup.tlPlateText = sRandPlate
	
	SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_CREATE_DUMMY_VEHICLE)
ENDPROC

/// PURPOSE:
///    Maintain initial purchase warning screen
PROC MAINTAIN_PURCHASE_VEHICLE_WARNING_SCREEN_STATE(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	INT iPrice = GET_PRICE_OF_VEHICLE(vehPurchaseStruct, vehPurchaseStruct.dummyVehicle)
	
	STRING strWarningMsgBodyTxt = "PURCHASE_VEH_IN"
	
	PRINTLN("[PURCHASE_VEH_PED_IN] MAINTAIN_PURCHASE_VEHICLE_WARNING_SCREEN_STATE: iBasePrice: ", iPrice)
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_VEH_PURCHASE_LOCAL_BIT_SET(vehPurchaseStruct, VEH_PURCHASE_LOCAL_BS_THIS_IS_REMOTE_PLAYER_VEHICLE)
		strWarningMsgBodyTxt = "BUY_PLAYER_VEH"
	ENDIF
	#ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(strWarningMsgBodyTxt)
	OR iPrice < 0
		PRINTLN("[PURCHASE_VEH_PED_IN] MAINTAIN_PURCHASE_VEHICLE_WARNING_SCREEN_STATE: Not safe to display warning message.")
		EXIT
	ENDIF
	
	SET_WARNING_MESSAGE_WITH_HEADER("WHOUSE_SHI", strWarningMsgBodyTxt, FE_WARNING_YESNO, DEFAULT, TRUE, iPrice)
	
	IF NOT IS_WARNING_MESSAGE_ACTIVE()
		EXIT
	ENDIF

	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	OR IS_MENU_CURSOR_CANCEL_PRESSED()
		PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		REINIT_NET_TIMER(vehPurchaseStruct.timerPurchaseDelay)
		SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_FAILED)
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_PROPERTY_MENU)
	ENDIF
ENDPROC

FUNC BOOL DO_CHECK_TO_TERMINATE_WHILE_LOOPS()
	IF g_bInMultiplayer
	AND MP_FORCE_TERMINATE_INTERNET_ACTIVE()
		CPRINTLN(DEBUG_INTERNET, "DO_CHECK_TO_TERMINATE_WHILE_LOOPS - MP force terminate internet active")
		RETURN FALSE
	ENDIF
	IF NOT g_bInMultiplayer
	AND SP_FORCE_TERMINATE_INTERNET_ACTIVE()
		CPRINTLN(DEBUG_INTERNET, "DO_CHECK_TO_TERMINATE_WHILE_LOOPS - SP force terminate internet active")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		CPRINTLN(DEBUG_INTERNET, "DO_CHECK_TO_TERMINATE_WHILE_LOOPS - player switch is in progress")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Creates a dummy vehicle to add to basket
PROC MAINTAIN_CREATE_DUMMY_VEHICLE_STATE(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	BOOL bAltVersion
	SITE_BUYABLE_VEHICLE siteBuyableVehicle = GET_WEBSITE_BUYABLE_VEHICLE_FROM_MODEL(vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel)
	
	IF vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel = BANSHEE
		IF IS_BIT_SET(vehPurchaseStruct.VehicleSetup.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			siteBuyableVehicle = BV_C_BANSHEE_TOPLESS
			PRINTLN("MAINTAIN_CREATE_DUMMY_VEHICLE_STATE spawn BV_C_BANSHEE_TOPLESS")
		ENDIF
	ENDIF
	
	WHILE NOT CREATE_AND_POPULATE_VEHICLE_DETAILS_FOR_MP_WEBSITE_VEHICLE_BUY(siteBuyableVehicle, WWW_NOT_EXIST, vehPurchaseStruct.vehicleSetup, vehPurchaseStruct.dummyVehicle, bAltVersion, FALSE)
	AND DO_CHECK_TO_TERMINATE_WHILE_LOOPS()
		EXIT
	ENDWHILE
	
	IF DOES_ENTITY_EXIST(vehPurchaseStruct.dummyVehicle)
		FREEZE_ENTITY_POSITION(vehPurchaseStruct.dummyVehicle, TRUE)
		SET_ENTITY_VISIBLE(vehPurchaseStruct.dummyVehicle, FALSE)
		SET_ENTITY_COLLISION(vehPurchaseStruct.dummyVehicle, FALSE)
			
		IF DECOR_EXIST_ON(vehPurchaseStruct.dummyVehicle, "Player_Vehicle")
			DECOR_REMOVE(vehPurchaseStruct.dummyVehicle, "Player_Vehicle")
		ENDIF
	ELSE
		EXIT
	ENDIF
	
	IF NOT IS_VEH_PURCHASE_LOCAL_BIT_SET(vehPurchaseStruct, VEH_PURCHASE_LOCAL_BS_SKIP_WARNING_SCREEN)
		SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_PURCHASE_WARNING)
	ELSE
		CLEAR_VEH_PURCHASE_LOCAL_BS(vehPurchaseStruct, VEH_PURCHASE_LOCAL_BS_SKIP_WARNING_SCREEN)
		SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_PROPERTY_MENU)
	ENDIF
ENDPROC

/// PURPOSE:
///    If player has move than 1 garage available, propery menu will appear to give player option to choose the garage slot
PROC MAINTAIN_VEHICLE_PURCHASE_PROEPRTY_MENU(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	INT iOnlySuitableProperty = -1
	INT iSlotToUse
	BOOL bGiveImpoundWarning = FALSE
	FE_WARNING_FLAGS WarningBS
	
	IF DOES_PLAYER_OWN_VEHICLE_STORAGE_FOR_VEHICLE_MODEL(vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel)
		CHECK_ONLY_ONE_SUITABLE_PROPERTY(vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel, iOnlySuitableProperty)
		PRINTLN("[PURCHASE_VEH_PED_IS_IN] MAINTAIN_VEHICLE_PURCHASE_PROEPRTY_MENU: iOnlySuitableProperty: ", iOnlySuitableProperty)
		IF iOnlySuitableProperty != -1
			vehPurchaseStruct.iResultSlot = MP_SAVE_VEHICLE_GET_EMPTY_SAVE_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST, FALSE, vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel, iOnlySuitableProperty)
			vehPurchaseStruct.iDisplaySlot = MP_SAVE_VEHICLE_GET_EMPTY_DISPLAY_SLOT(MP_SAVE_VEH_SEARCH_EMPTY_FIRST, FALSE, vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel, iOnlySuitableProperty)
			IF vehPurchaseStruct.iResultSlot > -1
			AND vehPurchaseStruct.iDisplaySlot > -1
				SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_TAKE_MOENY)
				PRINTLN("[PURCHASE_VEH_PED_IS_IN] MAINTAIN_VEHICLE_PURCHASE_PROEPRTY_MENU vehPurchaseStruct.iResultSlot: ", vehPurchaseStruct.iResultSlot , " vehPurchaseStruct.iDisplaySlot: ", vehPurchaseStruct.iDisplaySlot)
				EXIT
			ENDIF	
		ELSE
			iSlotToUse = -1
		ENDIF
		
		IF iSlotToUse = -1
		OR vehPurchaseStruct.iDisplaySlot = -1
			IF !IS_BIT_SET(vehPurchaseStruct.replaceVehMenu.iBS, REP_VEH_PROP_BS_EXIT_MENU)
				IF HANDLE_REPLACE_MP_SV_MENU(vehPurchaseStruct.replaceVehMenu, vehPurchaseStruct.iResultSlot, vehPurchaseStruct.iDisplaySlot, vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel, DEFAULT, DEFAULT, TRUE)
					IF IS_BIT_SET(vehPurchaseStruct.replaceVehMenu.iBS, REP_VEH_PROP_BS_EXIT_MENU)
						SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_FAILED)
						PRINTLN("[PURCHASE_VEH_PED_IS_IN] MAINTAIN_VEHICLE_PURCHASE_PROEPRTY_MENU player wants to leave the menu")
						EXIT
					ELSE
						IF vehPurchaseStruct.iResultSlot > -1
						AND vehPurchaseStruct.iDisplaySlot > -1
							SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_TAKE_MOENY)
							EXIT
						ELSE
							PRINTLN("[PURCHASE_VEH_PED_IS_IN] MAINTAIN_VEHICLE_PURCHASE_PROEPRTY_MENU HANDLE_REPLACE_MP_SV_MENU vehPurchaseStruct.iResultSlot: ", vehPurchaseStruct.iResultSlot , " vehPurchaseStruct.iDisplaySlot: ", vehPurchaseStruct.iDisplaySlot)
						ENDIF
					ENDIF	
				ENDIF
			ELSE
				CLEAR_BIT(vehPurchaseStruct.replaceVehMenu.iBS, REP_VEH_PROP_BS_EXIT_MENU)
				PRINTLN("[PURCHASE_VEH_PED_IS_IN] MAINTAIN_VEHICLE_PURCHASE_PROEPRTY_MENU player wants to leave the vehicle")
				SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_FAILED)
				EXIT
			ENDIF
		ENDIF
	ELSE
		IF !IS_SVM_VEHICLE_MODEL(vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel)
		AND !IS_THIS_MODEL_A_BICYCLE(vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel)
			IF IS_PERSONAL_VEHICLE_DRIVEABLE_IN_FREEMODE()
			
				INT iVehSlot
				
				REPEAT MAX_MP_SAVED_VEHICLES iVehSlot
					IF MP_SAVE_VEHICLE_IS_CAR_IMPOUNDED(iVehSlot ) 	
						bGiveImpoundWarning = TRUE
						iVehSlot = MAX_MP_SAVED_VEHICLES + 1 // bail
					ENDIF
				ENDREPEAT

				WarningBS = FE_WARNING_OKCANCEL
				IF bGiveImpoundWarning
					SET_WARNING_MESSAGE_WITH_HEADER("BRSCRWTEX", "PURCHASE_VEH_V1", WarningBS)
				ELSE	
					SET_WARNING_MESSAGE_WITH_HEADER("BRSCRWTEX", "PURCHASE_VEH_V2", WarningBS)
				ENDIF
				
				IF IS_WARNING_MESSAGE_ACTIVE()
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
					OR IS_MENU_CURSOR_CANCEL_PRESSED()
						SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_FAILED)
						PRINTLN("[PURCHASE_VEH_PED_IS_IN] MAINTAIN_VEHICLE_PURCHASE_PROEPRTY_MENU - player doesn't want to replace the impounded/active personal vehicle so they will lose the prize")
						EXIT
					ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
						vehPurchaseStruct.iResultSlot   	= 0
						vehPurchaseStruct.iDisplaySlot	= 0
						SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_TAKE_MOENY)
						PRINTLN("[PURCHASE_VEH_PED_IS_IN] MAINTAIN_VEHICLE_PURCHASE_PROEPRTY_MENU - player wants to replace the impounded/active personal vehicle with prize vehicle")
						EXIT
					ENDIF
				ENDIF	
				
			ELSE
				IF IS_BIT_SET(g_MpSavedVehicles[0].iVehicleBS, MP_SAVED_VEHICLE_INSURED)
					WarningBS = FE_WARNING_OKCANCEL
					SET_WARNING_MESSAGE_WITH_HEADER("BRSCRWTEX", "CASINO_PRIZE_V2", WarningBS)
					
					IF IS_WARNING_MESSAGE_ACTIVE()
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
						OR IS_MENU_CURSOR_CANCEL_PRESSED()
							SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_FAILED)
							PRINTLN("[PURCHASE_VEH_PED_IS_IN] MAINTAIN_VEHICLE_PURCHASE_PROEPRTY_MENU - player doesn't want to replace the insured vehicle so they will lose the prize")
							EXIT
						ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
							vehPurchaseStruct.iResultSlot   	= 0
							vehPurchaseStruct.iDisplaySlot	= 0
							SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_TAKE_MOENY)
							PRINTLN("[PURCHASE_VEH_PED_IS_IN] MAINTAIN_VEHICLE_PURCHASE_PROEPRTY_MENU - player wants to replace the insured vehicle with prize vehicle")
							EXIT
						ENDIF
					ENDIF	
				ELSE
					vehPurchaseStruct.iResultSlot   	= 0
					vehPurchaseStruct.iDisplaySlot	= 0
					SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_TAKE_MOENY)
					PRINTLN("[PURCHASE_VEH_PED_IS_IN] MAINTAIN_VEHICLE_PURCHASE_PROEPRTY_MENU - player don't have garage and don't have any PV so use slot 0")
					EXIT
				ENDIF
			ENDIF	
		ELSE
			SCRIPT_ASSERT("[PURCHASE_VEH_PED_IS_IN] MAINTAIN_VEHICLE_PURCHASE_PROEPRTY_MENU you must have suitable property for this vehicle model")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Process vehicle purchase basket   
FUNC BOOL PROCESSING_VEHICLE_PURCHASE_BASKET(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct,
		INT &iProcessSuccess, 
		INT iPrice, 
		INT iStatValue,
		INT iItemId, 
		INT iInventoryKey, 
		VEHICLE_INDEX tempVehID)	
	
	
	IF USE_SERVER_TRANSACTIONS()
		IF NOT DO_CHECK_TO_TERMINATE_WHILE_LOOPS()
			CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - Basket transaction failed - terminate loop!")
			vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
		ENDIF
		
		IF NOT NET_GAMESERVER_IS_SESSION_VALID(GET_ACTIVE_CHARACTER_SLOT())
		OR NET_GAMESERVER_IS_SESSION_REFRESH_PENDING()
			CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - Basket transaction failed - session pending!")
			vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
		ENDIF
		
		SWITCH vehPurchaseStruct.iProcessingBasketStage
			// Add item to basket
			CASE SHOP_BASKET_STAGE_ADD
				INT iCouponPrice
				iCouponPrice = iPrice
				BOOL bCouponPriceZero

				// Override the players cash.
				INT iWalletAmount, iBankAmount
				
				INT iTempPrice
				iWalletAmount = 0
				iBankAmount = 0
				iTempPrice = iCouponPrice
				// Bank first
				IF (NETWORK_GET_VC_BANK_BALANCE() > 0)
					IF NETWORK_GET_VC_BANK_BALANCE() >= iTempPrice
						iBankAmount = iTempPrice
					ELSE
						iBankAmount = iTempPrice-(iTempPrice - NETWORK_GET_VC_BANK_BALANCE())
					ENDIF
					iTempPrice -= iBankAmount
				ENDIF
				
				IF iTempPrice > 0
					IF (NETWORK_GET_VC_WALLET_BALANCE() > 0)
						IF NETWORK_GET_VC_WALLET_BALANCE() >= iTempPrice
							iWalletAmount = iTempPrice
						ELSE
							iWalletAmount = iTempPrice-(iTempPrice - NETWORK_GET_VC_WALLET_BALANCE())
						ENDIF
						iTempPrice -= iWalletAmount
					ENDIF
				ENDIF
				
				IF iTempPrice > 0
					vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
					SET_VEHICLE_PURCHASE_FAIL_REASON(vehPurchaseStruct, VPFR_INSUFFICIENT_FUNDS)
					CDEBUG1LN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] PROCESSING_VEHICLE_PURCHASE_BASKET - Player can't afford this item! iTempPrice=", iTempPrice, ", iWalletAmount=", iWalletAmount, ", iBankAmount=", iBankAmount)
					RETURN FALSE
				ELSE
					CDEBUG1LN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] PROCESSING_VEHICLE_PURCHASE_BASKET - Player can afford this item... iTempPrice=", iTempPrice,
							", iWalletAmount=", iWalletAmount, ", iBankAmount=", iBankAmount,
							" (iCouponPrice=", iCouponPrice, ")")
				ENDIF

				IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_INVENTORY_VEHICLE, iItemId, NET_SHOP_ACTION_BUY_VEHICLE, 1, iPrice, iStatValue, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, iInventoryKey)
					
					IF GET_PURCHASE_VEHICLE_SHOULD_ADD_MOD_PRICE(vehPurchaseStruct)
						IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_INVENTORY_VEHICLE, iInventoryKey, NET_SHOP_ACTION_BUY_VEHICLE, 1, iCouponPrice, iStatValue
																, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, HASH("PM_COUPON_ADD_VEH_MOD_P"))
							CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - Successfully added vehicle mod coupon hash ", HASH("PM_COUPON_ADD_VEH_MOD_P"), " to basket (iItemId: ", iItemId, ").")
						ELSE
							CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - Faied to add vehicle mod coupon hash ", HASH("PM_COUPON_ADD_VEH_MOD_P"), " to basket (iItemId: ", iItemId, ").")
							vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
							RETURN TRUE
						ENDIF
					ELSE
						CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - Ignore vehicle coupon hash ", HASH("PM_COUPON_ADD_VEH_MOD_P"))
					ENDIF
					
					// WE NOT USING THIS YET MIGHT ADD IT IN FUTURE FOR PC
					/*
					IF GET_PURCHASE_VEHICLE_DISCOUNT_AMOUNT(vehPurchaseStruct) != 0.0
						IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_INVENTORY_VEHICLE, iInventoryKey, NET_SHOP_ACTION_BUY_VEHICLE, 1, iCouponPrice, iStatValue
																, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, HASH("PM_COUPON_CAR_MEET_VEH_P"))
							CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - Successfully added vehicle discount coupon hash ", HASH("PM_COUPON_CAR_MEET_VEH_P"), " to basket (iItemId: ", iItemId, ").")
						ELSE
							CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - Faied to add vehicle discount coupon hash ", HASH("PM_COUPON_CAR_MEET_VEH_P"), " to basket (iItemId: ", iItemId, ").")
							vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
							RETURN TRUE
						ENDIF
					ELSE
						CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - Ignore vehicle coupon hash ", HASH("PM_COUPON_CAR_MEET_VEH_P"))
					ENDIF
					*/ 
					
					IF ADD_CURRENT_VEHICLE_MODS_TO_BASKET(vehPurchaseStruct.iResultSlot, tempVehID, TRUE)
						IF NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
							CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - Basket checkout started")
					
							IF iWalletAmount != 0
							OR iBankAmount != 0
							OR bCouponPriceZero
								CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - CHANGE_FAKE_MP_CASH(wallet=$-", iWalletAmount, ", bank=$-", iBankAmount, "): iPrice=$", iPrice, ", bankBalance=$", NETWORK_GET_VC_BANK_BALANCE(), ", walletBalance=$", NETWORK_GET_VC_WALLET_BALANCE())
								USE_FAKE_MP_CASH(TRUE)
								CHANGE_FAKE_MP_CASH(-iWalletAmount, -iBankAmount)
							ENDIF
							
							vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_PENDING
						ELSE
							CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - failed to start vehicle basket checkout")
							vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
						ENDIF
					ELSE
						CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - failed to add default mods to basket")
						vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
					ENDIF
				ELSE
					CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - failed to add vehicle to basket")
					vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
				ENDIF
			
			BREAK
			// Pending
			CASE SHOP_BASKET_STAGE_PENDING
				INT iScriptTransactionIndex
				iScriptTransactionIndex = GET_BASKET_TRANSACTION_SCRIPT_INDEX()
				
				IF iScriptTransactionIndex >= 0
				AND iScriptTransactionIndex < SHOPPING_TRANSACTION_MAX_NUMBER
					IF IS_CASH_TRANSACTION_COMPLETE(iScriptTransactionIndex)
						IF GET_CASH_TRANSACTION_STATUS(iScriptTransactionIndex) = CASH_TRANSACTION_STATUS_SUCCESS
							CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - Basket transaction finished, success!")
							vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_SUCCESS
						ELSE
							CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - Basket transaction finished, failed!")
							vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - Basket transaction invalid, failed!")
					vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
				ENDIF
			BREAK
			
			// Success
			CASE SHOP_BASKET_STAGE_SUCCESS

				vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
				
				USE_FAKE_MP_CASH(FALSE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
				
				iProcessSuccess = GENERIC_TRANSACTION_STATE_SUCCESS
				RETURN FALSE
			BREAK
			
			//Failed
			CASE SHOP_BASKET_STAGE_FAILED
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				
				vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
				
				USE_FAKE_MP_CASH(FALSE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
				
				iProcessSuccess = GENERIC_TRANSACTION_STATE_FAILED
				RETURN FALSE
			BREAK
		ENDSWITCH
		
		IF vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_FAILED
			CPRINTLN(DEBUG_INTERNET, "[PURCHASE_VEH_PED_IN] - was FORCE_CLOSE_BROWSER called to termiate the transaction? Set success to 0")
			
			DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
			
			vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_ADD

			USE_FAKE_MP_CASH(FALSE)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)

			iProcessSuccess = GENERIC_TRANSACTION_STATE_FAILED
			RETURN FALSE
		ENDIF
		
		RETURN TRUE
	ELSE
		BOOL fromBank = (NETWORK_GET_VC_BANK_BALANCE() >= 0)
		BOOL fromBankAndWallet = (NETWORK_GET_VC_BANK_BALANCE() < iPrice)
		IF (fromBank AND NETWORK_CAN_SPEND_MONEY(iPrice, TRUE, FALSE, FALSE))
		OR (fromBankAndWallet AND NETWORK_CAN_SPEND_MONEY(iPrice, FALSE, TRUE, FALSE))
			iProcessSuccess = GENERIC_TRANSACTION_STATE_SUCCESS
			RETURN FALSE
		ELSE
			CPRINTLN(DEBUG_INTERNET, "DO_MP_VEHICLE_BUY() - vehicle site fail reason \"cannot spend money\"")
			iProcessSuccess = GENERIC_TRANSACTION_STATE_FAILED
			SET_VEHICLE_PURCHASE_FAIL_REASON(vehPurchaseStruct, VPFR_INSUFFICIENT_FUNDS)
			RETURN FALSE
		ENDIF

	ENDIF

	vehPurchaseStruct.iProcessingBasketStage = SHOP_BASKET_STAGE_ADD
	iProcessSuccess = GENERIC_TRANSACTION_STATE_DEFAULT
	RETURN FALSE
ENDFUNC

FUNC INT GET_VEIHCLE_CLONE_SHOP_HASH(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	#IF NOT FEATURE_GEN9_EXCLUSIVE
	UNUSED_PARAMETER(vehPurchaseStruct)	
	#ENDIF

	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_VEH_PURCHASE_LOCAL_BIT_SET(vehPurchaseStruct, VEH_PURCHASE_LOCAL_BS_THIS_IS_REMOTE_PLAYER_VEHICLE)
		RETURN HASH("CAR_MEET_VEHICLE_CLONE_HSW")
	ENDIF
	#ENDIF
	
	IF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		RETURN HASH("AUTO_SHOP_VEHICLE_CLONE")
	#IF FEATURE_DLC_1_2022
	ELIF IS_PLAYER_IN_SIMEON_SHOWROOM(PLAYER_ID())
		RETURN HASH("SIMEON_SHOWROOM_VEHICLE_CLONE")
	ELIF IS_LUXURY_SHOWROOM_SCRIPT_RUNNING()
		RETURN HASH("LUXURY_SHOWROOM_VEHICLE_CLONE")
	ELIF IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PLAYER_ID())
		RETURN HASH("CLUBHOUSE_VEHICLE_CLONE")
	#ENDIF
	ENDIF
	
	RETURN HASH("CAR_MEET_VEHICLE_CLONE")
ENDFUNC

/// PURPOSE:
///    Maintain vehicle purchase transactions
PROC MAINTAIN_VEHICLE_PURCHASE_TRANSACTION(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	BOOL bUseAltPrice = SHOULD_USE_ALT_PRICES(vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel)
	
	INT iProcessSuccess = GENERIC_TRANSACTION_STATE_DEFAULT
	INT iItemId = GET_VEHICLE_KEY_FOR_CATALOGUE(vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel, bUseAltPrice, DEFAULT, FALSE)
	INT iInventoryKey = GET_VEHICLE_INVENTORY_KEY_FOR_CATALOGUE(vehPurchaseStruct.iResultSlot)
	INT iBasePrice
	
	BOOL bCarNowInGarage = TRUE
	BOOL bNowMostRecentCarUsed = FALSE
	BOOL bIsFreeVehicle = FALSE
	
	INT iPrice = GET_PRICE_OF_VEHICLE(vehPurchaseStruct, vehPurchaseStruct.dummyVehicle)
	
	iBasePrice = GET_PRICE_OF_VEHICLE(vehPurchaseStruct, vehPurchaseStruct.dummyVehicle, TRUE)
	
	IF iBasePrice = 0
		bIsFreeVehicle = TRUE
	ENDIF
	
	WHILE PROCESSING_VEHICLE_PURCHASE_BASKET(vehPurchaseStruct, iProcessSuccess, iPrice, ENUM_TO_INT(g_sConfVssMP.vehicleSetup.eModel), iItemId, iInventoryKey, vehPurchaseStruct.dummyVehicle)
	AND DO_CHECK_TO_TERMINATE_WHILE_LOOPS()
		EXIT
	ENDWHILE
	
	IF iProcessSuccess = GENERIC_TRANSACTION_STATE_SUCCESS
		IF USE_SERVER_TRANSACTIONS()
			CPRINTLN(DEBUG_SHOPS, "[CASH] NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED - basket")
			NET_GAMESERVER_SET_TELEMETRY_NONCE_SEED(GET_CASH_TRANSACTION_ID_FROM_INDEX(GET_BASKET_TRANSACTION_SCRIPT_INDEX()))
		ENDIF
		
		INT iItemHash = ENUM_TO_INT(vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel)
		
		IF vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel = LANDSTALKER2
			iItemHash = GET_HASH_KEY(GET_LABEL_BUYABLE_VEHICLE(GET_WEBSITE_BUYABLE_VEHICLE_FROM_MODEL(vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel)))
		ENDIF
		
		BOOL fromBank = (NETWORK_GET_VC_BANK_BALANCE() >= 0)
		BOOL fromBankAndWallet = (NETWORK_GET_VC_BANK_BALANCE() < iPrice)
		
		NETWORK_BUY_ITEM(iPrice, iItemHash, PURCHASE_VEHICLES, 1, fromBank
						, GET_LABEL_BUYABLE_VEHICLE(GET_WEBSITE_BUYABLE_VEHICLE_FROM_MODEL(vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel)), GET_VEIHCLE_CLONE_SHOP_HASH(vehPurchaseStruct), 0, 0, fromBankAndWallet)
		
		IF USE_SERVER_TRANSACTIONS()
			DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
		ENDIF
	
		SET_MP_VEHICLE_PURCHASED_PACKED_STAT_BOOL(vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel)

		INT iVehicleSpend = GET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_VEHICLES)
		
		CONST_INT iCONST_THRESHOLD  1000000 //$1,000,000 
		
		INT iSpendTarget = ((iVehicleSpend/iCONST_THRESHOLD) + 1) * iCONST_THRESHOLD
		CPRINTLN(DEBUG_SHOPS, "DO_MP_VEHICLE_BUY: request high vehicle spend ($", iVehicleSpend ," + $", iPrice, ") >= $", iSpendTarget)
		
		IF (iVehicleSpend + iPrice) >= iSpendTarget
			REQUEST_SYSTEM_ACTIVITY_TYPE_HIGH_VEHICLE_SPEND((iSpendTarget/iCONST_THRESHOLD) + 1)
		ENDIF
		
		IF !DOES_PLAYER_OWN_VEHICLE_STORAGE_FOR_VEHICLE_MODEL(vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel)
			CLEANUP_MP_SAVED_VEHICLE(TRUE)
			bCarNowInGarage = FALSE
			bNowMostRecentCarUsed = TRUE
			PRINTLN("[PURCHASE_VEH_PED_IN] MAINTAIN_VEHICLE_PURCHASE_TRANSACTION: player don't own a garage set this as active personal vehicle")
		ENDIF
	
		#IF FEATURE_GEN9_EXCLUSIVE
		BOOL bIsCloned = IS_VEH_PURCHASE_LOCAL_BIT_SET(vehPurchaseStruct, VEH_PURCHASE_LOCAL_BS_THIS_IS_REMOTE_PLAYER_VEHICLE)
		#ENDIF
		
		MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(vehPurchaseStruct.dummyVehicle, vehPurchaseStruct.iResultSlot, TRUE, bCarNowInGarage, bNowMostRecentCarUsed, TRUE, TRUE, FALSE, bIsFreeVehicle, DEFAULT, DEFAULT
												#IF FEATURE_GEN9_EXCLUSIVE , bIsCloned #ENDIF )
												
		MPSV_SET_DISPLAY_SLOT(vehPurchaseStruct.iDisplaySlot, vehPurchaseStruct.iResultSlot)
		
		IF vehPurchaseStruct.iResultSlot != -1
			g_MpSavedVehicles[vehPurchaseStruct.iResultSlot].iPricePaid = iBasePrice
			PRINTLN("[PURCHASE_VEH_PED_IN] MAINTAIN_VEHICLE_PURCHASE_TRANSACTION: iBasePrice: ", iBasePrice)
		ENDIF	

		SET_VEHICLE_AS_JUST_PURCHASED(vehPurchaseStruct.iResultSlot)
		
		IF vehPurchaseStruct.iDisplaySlot >= DISPLAY_SLOT_START_ARENAWARS_GARAGE_LVL1
		AND vehPurchaseStruct.iDisplaySlot <= DISPLAY_SLOT_END_ARENAWARS_GARAGE_LVL3
			CPRINTLN(DEBUG_INTERNET, "MAINTAIN_VEHICLE_PURCHASE_TRANSACTION - SET_ARENA_VEHICLE_NAME to NULL for ",vehPurchaseStruct.iDisplaySlot - DISPLAY_SLOT_START_ARENAWARS_GARAGE_LVL1)
			CLEAR_ARENA_VEHICLE_NAME(vehPurchaseStruct.iDisplaySlot - DISPLAY_SLOT_START_ARENAWARS_GARAGE_LVL1)
		ENDIF

		INSURE_MP_SAVE_VEHICLE_AND_SET_FLAG(vehPurchaseStruct.dummyVehicle, vehPurchaseStruct.iResultSlot)
		
		STRING strVehNameTag = GET_NAME_OF_VEHICLE(vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel)
		
		enumCharacterList eCharacter = GET_PURCHASE_VEHICLE_DELIVERY_CHARACTER(vehPurchaseStruct)
		BOOL bUseCustomMessage = FALSE
		
		#IF FEATURE_GEN9_EXCLUSIVE
		IF IS_VEH_PURCHASE_LOCAL_BIT_SET(vehPurchaseStruct, VEH_PURCHASE_LOCAL_BS_THIS_IS_REMOTE_PLAYER_VEHICLE)
			REINIT_NET_TIMER(MPGlobalsAmbience.sMPVehPurchase.sRemoteCloneTimer)

			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HAS_USED_CLONE_SHOP)
				SET_BIT(MPGlobalsAmbience.sMPVehPurchase.iBS, ciMP_PURCHASE_VEH_BS_FIRST_CLONED_VEHICLE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HAS_USED_CLONE_SHOP, TRUE)
			ENDIF
			
			eCharacter = CHAR_HAO
			bUseCustomMessage = TRUE
		ENDIF
		#ENDIF
		
		IF GET_PURCHASE_VEHICLE_SHOULD_ADD_CUSTOM_DELIVERY_MESSAGE(vehPurchaseStruct)
			bUseCustomMessage = TRUE
		ENDIF
		
		STRING strTextMsg
		STRING strTextMsgConfirmed
	
		IF bUseCustomMessage
			strTextMsg 			= GET_MP_PURCHASE_VEHICLE_TEXT_MESSAGE_CUSTOM(eCharacter, FALSE)
			strTextMsgConfirmed = GET_MP_PURCHASE_VEHICLE_TEXT_MESSAGE_CUSTOM(eCharacter, TRUE)
		ELSE
			strTextMsg 			= GET_MP_PURCHASE_VEHICLE_TEXT_MESSAGE(vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel, strVehNameTag, vehPurchaseStruct.iDisplaySlot, FALSE)
			strTextMsgConfirmed = GET_MP_PURCHASE_VEHICLE_TEXT_MESSAGE(vehPurchaseStruct.vehicleSetup.VehicleSetup.eModel, strVehNameTag, vehPurchaseStruct.iDisplaySlot, TRUE)
		ENDIF
		
		SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS(eCharacter, strTextMsg, TXTMSG_UNLOCKED
		, GET_FILENAME_FOR_AUDIO_CONVERSATION(strVehNameTag), -99, "", STRING_COMPONENT, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
		
		ADD_ITEM_TO_MP_VEHICLE_TEXT(eCharacter, strTextMsgConfirmed, strVehNameTag, "", MPGlobalsAmbience.sMPVehPurchase.iDelayTime, DEFAULT, vehPurchaseStruct.iResultSlot)
		
		SET_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_VEHICLES, iVehicleSpend + iPrice)
		
		#IF FEATURE_GEN9_EXCLUSIVE
		IF SHOULD_REWARD_FREE_HSW_UPGRADE(vehPurchaseStruct)
		AND NOT bIsFreeVehicle
			BROADCAST_PURCHASED_REMOTE_PLAYER_VEHICLE(vehPurchaseStruct.remotePlayer)
			IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIRST_HSW_VOUCHER_FOR_CLONE_VEH)
				OBTAIN_COUPON(COUPON_HSW_UPGRADE)
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIRST_HSW_VOUCHER_FOR_CLONE_VEH, TRUE)
				PRINTLN("[PURCHASE_VEH_PED_IN] MAINTAIN_VEHICLE_PURCHASE_TRANSACTION GIVE_FREE_HSW_UPGRADE_VOUCHER")
			ELSE
				OBTAIN_HSW_MOD_COUPON()
				PRINTLN("[PURCHASE_VEH_PED_IN] MAINTAIN_VEHICLE_PURCHASE_TRANSACTION already given free HSW voucher, give mod voucher")
			ENDIF
		ENDIF
		#ENDIF
		
		REQUEST_SAVE_PURCHASE_VEHICLE()
		SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_SUCCESS)
	ELIF iProcessSuccess = GENERIC_TRANSACTION_STATE_FAILED
		SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_FAILED)
	ELSE iProcessSuccess = GENERIC_TRANSACTION_STATE_PENDING
		PRINTLN("[PURCHASE_VEH_PED_IN] MAINTAIN_VEHICLE_PURCHASE_TRANSACTION iProcessSuccess is pending")
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if we should bail vehicle purchase
FUNC BOOL SHOULD_BAIL_VEHICLE_PURCHASE(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, BOOL bIsSelectedVehicle)
	
	BOOL bShouldCheckForBailConditions
	SWITCH vehPurchaseStruct.vehiclePurchaseState
		CASE VPS_PURCHASE_WARNING
		CASE VPS_CREATE_DUMMY_VEHICLE
		CASE VPS_PROPERTY_MENU
		CASE VPS_TAKE_MOENY
			bShouldCheckForBailConditions = TRUE
		BREAK
	ENDSWITCH
	
	IF bShouldCheckForBailConditions
		IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
			RETURN TRUE
		ENDIF
		
		IF NOT bIsSelectedVehicle
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
		ENDIF	
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Display insufficient funds warning screen if player doesn't have enought money to purchase
/// PARAMS:
///    vehPurchaseStruct - 
PROC MAINTAIN_INSUFFICIENT_FUNDS_WARNING_SCREEN(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
	SET_WARNING_MESSAGE_WITH_HEADER("BRSCRWTEX", "BRDISTEX", FE_WARNING_OKCANCEL, "BRSHETEX")
	
	IF NOT IS_WARNING_MESSAGE_ACTIVE()
		EXIT
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	OR IS_MENU_CURSOR_CANCEL_PRESSED()
		SET_VEHICLE_PURCHASE_FAIL_REASON(vehPurchaseStruct, VPFR_DEFAULT)
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OPEN_COMMERCE_STORE("", "", SPL_STORE)
		DISABLE_CELLPHONE(FALSE)
		SET_VEHICLE_PURCHASE_FAIL_REASON(vehPurchaseStruct, VPFR_DEFAULT)
	ENDIF
ENDPROC

//// PURPOSE:
 ///    Maintain main vehicle purchase logic
 /// PARAMS:
 ///    vehPurchaseStruc - 
 ///    bIsSelectedVehicle - 
PROC MAINTAIN_VEHICLE_PURCHASE_MAIN_LOGIC(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct, BOOL bIsSelectedVehicle)
	IF SHOULD_BAIL_VEHICLE_PURCHASE(vehPurchaseStruct, bIsSelectedVehicle)
		SET_VEHICLE_PURCHASE_STATE(vehPurchaseStruct, VPS_FAILED)
	ENDIF
	
	SWITCH vehPurchaseStruct.vehiclePurchaseState
		CASE VPS_DEFAULT
			MAINTAIN_PURCHASE_VEHICLE_DEFAULT_STATE(vehPurchaseStruct, bIsSelectedVehicle)
		BREAK
		CASE VPS_PURCHASE_WARNING
			MAINTAIN_PURCHASE_VEHICLE_WARNING_SCREEN_STATE(vehPurchaseStruct)
		BREAK
		CASE VPS_CREATE_DUMMY_VEHICLE
			MAINTAIN_CREATE_DUMMY_VEHICLE_STATE(vehPurchaseStruct)
		BREAK
		CASE VPS_PROPERTY_MENU
			MAINTAIN_VEHICLE_PURCHASE_PROEPRTY_MENU(vehPurchaseStruct)
		BREAK
		CASE VPS_TAKE_MOENY
			MAINTAIN_VEHICLE_PURCHASE_TRANSACTION(vehPurchaseStruct)
		BREAK
		CASE VPS_FAILED
			IF GET_VEHICLE_PURCHASE_FAILED_REASON(vehPurchaseStruct) = VPFR_INSUFFICIENT_FUNDS
				MAINTAIN_INSUFFICIENT_FUNDS_WARNING_SCREEN(vehPurchaseStruct)
				EXIT
			ENDIF
			
			CLEANUP_DUMMY_VEHICLE_DATA(vehPurchaseStruct)
			CLEAR_VEH_PURCHASE_LOCAL_BS(vehPurchaseStruct, VEH_PURCHASE_LOCAL_BS_PURCHASE_IN_PROGRESS)
		BREAK
		CASE VPS_SUCCESS
			CLEANUP_DUMMY_VEHICLE_DATA(vehPurchaseStruct)
			CLEAR_VEH_PURCHASE_LOCAL_BS(vehPurchaseStruct, VEH_PURCHASE_LOCAL_BS_PURCHASE_IN_PROGRESS)	
		BREAK	
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Purchase vehicle ped is in 
PROC PURCHASE_VEHICLE_PED_IS_IN(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
 	MAINTAIN_VEHICLE_PURCHASE_MAIN_LOGIC(vehPurchaseStruct, FALSE)
ENDPROC

/// PURPOSE:
///    Purchase selected vehicle
PROC PURCHASE_SELECTED_VEHICLE(VEHICLE_PURCHASE_STRUCT &vehPurchaseStruct)
 	MAINTAIN_VEHICLE_PURCHASE_MAIN_LOGIC(vehPurchaseStruct, TRUE)
ENDPROC



