USING "net_include.sch"
USING "net_spawn.sch"
USING "Net_Script_Timers.sch"


CONST_INT GARAGE_CAM_STATE_WAIT				0
CONST_INT GARAGE_CAM_STATE_INIT				1
CONST_INT GARAGE_CAM_STATE_STATIC			2
CONST_INT GARAGE_CAM_STATE_MOVING			3
CONST_INT GARAGE_CAM_STATE_END				4

STRUCT GARAGE_CAM_STRUCT
	CAMERA_INDEX theCam
	VECTOR vCam1Loc
	VECTOR vCam1Rot
	FLOAT fCam1FOV
	VECTOR vCam2Loc
	VECTOR vCam2Rot
	FLOAT fCam2FOV
	BOOL bStartedMoving
	BOOL bDisableCameraPan
	INT iGarageID
ENDSTRUCT

STRUCT GARAGE_CLEAR_AREA_STRUCT
	VECTOR vLoc1, vLoc2
	FLOAT fWidth
ENDSTRUCT

STRUCT GARAGE_DATA_STRUCT
	INT iGameMode
	INT iGarageIDHash
	INT iIndex
	VECTOR vGarageLocation
	INT iDoor1ID
	INT iDoor2ID
	STRING sGarageName
	GARAGE_CAM_STRUCT cameraData
	GARAGE_CLEAR_AREA_STRUCT clearAreaData
ENDSTRUCT

/////////////////////////////////////////
//-- 		Garage functions		 --//
///////////////////////////////////////// 
/// PURPOSE:
///    Gets the details about garage
PROC GET_GARAGE_DETAILS(GARAGE_DATA_STRUCT& garageData, INT iID)
	SWITCH iID
		CASE 0	
		CASE MP_GAR_SIMEON	
			garageData.iGameMode = FAKE_GAME_MODE_ALL
			garageData.iGarageIDHash = MP_GAR_SIMEON
			garageData.iIndex = 0
			garageData.vGarageLocation = << 1204.4286, -3110.8469, 4.3988 >>
			garageData.iDoor1ID = MP_DOOR_SIMEON_GAR_DOOR	
			garageData.iDoor2ID = 0
			garageData.sGarageName = "MP_GAR_SIMEON"
//			garageData.cameraData.vCam1Loc = <<1207.022583,-3107.567139,5.694052>> 
//			garageData.cameraData.vCam1Rot = <<-0.541996,-0.000000,152.422958>>
//			garageData.cameraData.fCam1FOV = 55.0
//			garageData.cameraData.vCam2Loc = <<1208.550537,-3104.230713,6.830945>>
//			garageData.cameraData.vCam2Rot = <<-2.409292,-0.000000,138.020309>>
//			garageData.cameraData.fCam2FOV = 55.0
			
			garageData.cameraData.vCam1Loc = <<1210.8840, -3122.4019, 5.2118>> 
			garageData.cameraData.vCam1Rot = <<4.0534, -0.0000, 32.6363>>
			garageData.cameraData.fCam1FOV = 32.4980
			garageData.cameraData.vCam2Loc = <<1210.8840, -3122.4019, 5.2118>>
			garageData.cameraData.vCam2Rot = <<4.0534, -0.0000, 32.6363>>
			garageData.cameraData.fCam2FOV = 32.4980
			
			garageData.clearAreaData.vLoc1 = <<1204.157349,-3122.599121,3.795331>>
			garageData.clearAreaData.vLoc2 = <<1204.239990,-3099.771973,8.400777>>
			garageData.clearAreaData.fWidth = 7.000000
			EXIT
		BREAK
		CASE 1	
		CASE MP_GAR_PNS_2	
			garageData.iGameMode = FAKE_GAME_MODE_ALL
			garageData.iGarageIDHash = MP_GAR_PNS_2
			garageData.iIndex = 1
			garageData.vGarageLocation = << 725.1831, -1089.3490, 21.1692 >>
			garageData.iDoor2ID = 0
			garageData.sGarageName = "MP_GAR_PNS_2"
			garageData.cameraData.vCam1Loc = <<734.3793, -1078.7905, 23.4305>>
			garageData.cameraData.vCam1Rot =  <<-16.4320, 0.0000, -19.7978>>
			garageData.cameraData.fCam1FOV = 60.0199
			garageData.cameraData.vCam2Loc = <<734.3027, -1079.0035, 23.4973>>
			garageData.cameraData.vCam2Rot = <<-16.4320, 0.0000, -19.7978>>
			garageData.cameraData.fCam2FOV = 60.0199
			garageData.clearAreaData.vLoc1 = <<738.885742,-1088.515625,20.559574>>
			garageData.clearAreaData.vLoc2 = <<718.613037,-1088.779907,24.832630>> 
			garageData.clearAreaData.fWidth = 7.000000
			EXIT
		BREAK
		
		CASE 2	
		CASE MP_GAR_PNS_3	
			garageData.iGameMode = FAKE_GAME_MODE_ALL
			garageData.iGarageIDHash = MP_GAR_PNS_3
			garageData.iIndex = 2
			garageData.vGarageLocation = <<-1164.886719,-2011.105225,12.253714 >>
			garageData.iDoor2ID = 0
			garageData.sGarageName = "MP_GAR_NEUT_PNS_3"
			garageData.cameraData.vCam1Loc = <<-1161.7738, -2010.2699, 14.2468>>
			garageData.cameraData.vCam1Rot = <<-17.3415, -0.0000, 113.6889>>
			garageData.cameraData.fCam1FOV = 64.5334
			garageData.cameraData.vCam2Loc = <<-1161.6388, -2010.2107, 14.2928>>
			garageData.cameraData.vCam2Rot = <<-17.3415, -0.0000, 113.6889>>
			garageData.cameraData.fCam2FOV = 64.5334
			garageData.clearAreaData.vLoc1 = <<-1169.722778,-2015.922607,11.504413>>
			garageData.clearAreaData.vLoc2 = <<-1160.558350,-2007.004639,15.680271>> 
			garageData.clearAreaData.fWidth = 5.500000
			EXIT
		BREAK
		
		CASE 3
		CASE MP_GAR_PNS_4
			garageData.iGameMode = FAKE_GAME_MODE_ALL
			garageData.iGarageIDHash = MP_GAR_PNS_4
			garageData.iIndex = 3
			garageData.vGarageLocation = <<-330.44,-143.39,39.33>>
			garageData.iDoor2ID = 0
			garageData.sGarageName = "MP_GAR_PNS_4"
			garageData.cameraData.vCam1Loc = <<-332.1567, -141.0546, 40.2864>>
			garageData.cameraData.vCam1Rot = <<-20.6629, 0.0000, -134.7887>>
			garageData.cameraData.fCam1FOV = 60.0241
			garageData.cameraData.vCam2Loc = <<-332.3621, -140.8507, 40.3956>>
			garageData.cameraData.vCam2Rot = <<-20.6629, 0.0000, -134.7887>>
			garageData.cameraData.fCam2FOV = 60.0241
			garageData.clearAreaData.vLoc1 = <<-323.799835,-146.253906,37.814919>>
			garageData.clearAreaData.vLoc2 = <<-334.343231,-141.726105,40.759644>> 
			garageData.clearAreaData.fWidth = 5.500000
			EXIT
		BREAK
		
		CASE 4
		CASE MP_GAR_PNS_5	
			garageData.iGameMode = FAKE_GAME_MODE_ALL
			garageData.iGarageIDHash = MP_GAR_PNS_5
			garageData.iIndex = 4
			garageData.vGarageLocation = <<106.28,6620.01,32.12>>
			garageData.iDoor2ID = 0
			garageData.sGarageName = "MP_GAR_PNS_5"
			garageData.cameraData.vCam1Loc = <<106.6880, 6617.3223, 32.5026>>
			garageData.cameraData.vCam1Rot = <<-10.7437, 0.0000, 21.7154>>
			garageData.cameraData.fCam1FOV = 67.5620
			garageData.cameraData.vCam2Loc = <<106.7971, 6617.0483, 32.5586>>
			garageData.cameraData.vCam2Rot = <<-10.7437, 0.0000, 21.7154>>
			garageData.cameraData.fCam2FOV = 67.5620
			garageData.clearAreaData.vLoc1 = <<100.975906,6625.046387,30.603012>>
			garageData.clearAreaData.vLoc2 = <<111.252235,6615.656738,33.629288>>
			garageData.clearAreaData.fWidth = 5.500000
			EXIT
		BREAK
		
		CASE 5	
		CASE MP_GAR_PNS_6	
			garageData.iGameMode = FAKE_GAME_MODE_ALL
			garageData.iGarageIDHash = MP_GAR_PNS_6
			garageData.iIndex = 5
			garageData.vGarageLocation = <<1182.65,2641.90,38.05>>
			garageData.iDoor2ID = 0
			garageData.cameraData.vCam1Loc = <<1184.2061, 2644.0039, 38.7458>>
			garageData.cameraData.vCam1Rot = <<-15.4014, 0.0000, 161.4493>>
			garageData.cameraData.fCam1FOV = 67.3374
			garageData.cameraData.vCam2Loc = <<1184.3448, 2644.4177, 38.8660>>
			garageData.cameraData.vCam2Rot = <<-15.4014, 0.0000, 161.4493>>
			garageData.cameraData.fCam2FOV = 67.3374
			garageData.clearAreaData.vLoc1 = <<1182.834839,2634.775391,36.550064>>
			garageData.clearAreaData.vLoc2 = <<1182.577637,2647.954590,39.586018>>
			garageData.clearAreaData.fWidth = 5.500000
			EXIT
		BREAK
	ENDSWITCH
	NET_PRINT_STRING_INT("GET_GARAGE_DETAILS: Trying to get id of invalid garage. ID = ",iID) NET_NL()
	SCRIPT_ASSERT("GET_GARAGE_DETAILS: Trying to get id of invalid garage see Conor")
ENDPROC

/// PURPOSE:
///    Function to get the rough position of designated garage
FUNC VECTOR GET_GARAGE_LOCATION(INT iGarageIDHash)
	SWITCH iGarageIDHash
		CASE MP_GAR_SIMEON		RETURN << 1204.4286, -3110.8469, 4.3988 >> 		BREAK //--
		CASE MP_GAR_PNS_2		RETURN << 725.1831, -1089.3490, 21.1692 >> 		BREAK //--
		CASE MP_GAR_PNS_3		RETURN <<-1164.886719,-2011.105225,12.253714 >> BREAK //--
		CASE MP_GAR_PNS_4		RETURN <<-330.44,-143.39,39.33>> 				BREAK //--
		CASE MP_GAR_PNS_5		RETURN <<106.28,6620.01,32.12>> 				BREAK //--
		CASE MP_GAR_PNS_6		RETURN <<1182.65,2641.90,38.05>> 				BREAK //--
	ENDSWITCH
	NET_PRINT("Trying to get location of invalid garage") NET_NL()
	SCRIPT_ASSERT("GET_GARAGE_LOCATION: Trying to get location of invalid garage see Conor")
	RETURN <<0,0,0>>
ENDFUNC

FUNC INT GET_GARAGE_LOOP_INDEX(INT iGarageID)
	SWITCH iGarageID
		CASE MP_GAR_SIMEON	
			RETURN 0
		BREAK	
		CASE MP_GAR_PNS_2	
			RETURN 1
		BREAK
		CASE MP_GAR_PNS_3	
			RETURN 2
		BREAK
		CASE MP_GAR_PNS_4
			RETURN 3
		BREAK
		CASE MP_GAR_PNS_5	
			RETURN 4
		BREAK
		CASE MP_GAR_PNS_6	
			RETURN 5
		BREAK
	ENDSWITCH
	NET_PRINT_STRING_INT("GET_GARAGE_DETAILS: Trying to get loop id of invalid garage. ID = ",iGarageID) NET_NL()
	SCRIPT_ASSERT("GET_GARAGE_DETAILS: Trying to get loop id of invalid garage see Conor")
	RETURN 0
ENDFUNC

FUNC BOOL IS_GARAGE_A_PNS(INT iGarageID)
	SWITCH iGarageID
		CASE MP_GAR_PNS_2	RETURN TRUE	BREAK
		CASE MP_GAR_PNS_3	RETURN TRUE	BREAK
		CASE MP_GAR_PNS_4	RETURN TRUE	BREAK
		CASE MP_GAR_PNS_5	RETURN TRUE	BREAK
		CASE MP_GAR_PNS_6	RETURN TRUE	BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///  	Check if local player is in the opening range of the garage
FUNC BOOL IS_PLAYER_IN_OPENING_RANGE_OF_GARAGE(INT iGarageID, INT iOverrideRange = 18)
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_GARAGE_LOCATION(iGarageID)) <= iOverrideRange
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_COORD_NEAR_GARAGE(VECTOR vLoc,INT iGarageID = -1, INT iRange = 18)
	IF GET_DISTANCE_BETWEEN_COORDS(vLoc,GET_GARAGE_LOCATION(iGarageID)) <= iRange
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


#IF IS_DEBUG_BUILD
FUNC STRING GET_DOOR_NAME(INT iDoor)
	SWITCH iDoor 
		CASE MP_DOOR_SIMEON_GAR_DOOR 	RETURN "MP_DOOR_SIMEON_GAR_DOOR" 				BREAK
	ENDSWITCH
	RETURN "INVALID"
ENDFUNC

FUNC STRING GET_GARAGE_NAME_FROM_ID(INT iGarageID)
	SWITCH iGarageID
		CASE MP_GAR_SIMEON 	RETURN "MP_GAR_SIMEON" 	BREAK
		CASE MP_GAR_PNS_2	RETURN "MP_GAR_PNS_2"	BREAK
		CASE MP_GAR_PNS_3	RETURN "MP_GAR_PNS_3"	BREAK
		CASE MP_GAR_PNS_4	RETURN "MP_GAR_PNS_4"	BREAK
		CASE MP_GAR_PNS_5	RETURN "MP_GAR_PNS_5"	BREAK
		CASE MP_GAR_PNS_6	RETURN "MP_GAR_PNS_6"	BREAK
	ENDSWITCH
	NET_PRINT_STRING_INT("GET_GARAGE_NAME_FROM_ID: Trying to get name of invalid garage #",iGarageID) NET_NL()
	SCRIPT_ASSERT("GET_GARAGE_NAME_FROM_ID: Trying to get name of invalid garage see Conor")
	RETURN ""
ENDFUNC
#ENDIF

/// PURPOSE:
///  	Handles players requesting a door open or closed
PROC REQUEST_GARAGE_OPEN(INT iGarageID, BOOL bOpen, BOOL bForcedOpen =FALSE)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		NET_PRINT_STRINGS(GET_THIS_SCRIPT_NAME()," REQUEST_GARAGE_OPEN called but network game no longer running") NET_NL()		
	ELSE
		IF bOpen
			IF NOT IS_BIT_SET(MPGlobalsAmbience.garageRequests.iOpenBS,GET_GARAGE_LOOP_INDEX(iGarageID))
				NET_NL()
				NET_PRINT_TIME() NET_NL()
				NET_PRINT_STRINGS(GET_THIS_SCRIPT_NAME()," REQUEST_GARAGE_OPEN true") NET_NL()
				NET_PRINT_STRINGS("GARAGE ID: ",GET_GARAGE_NAME_FROM_ID(iGarageID)) NET_NL()
				IF bForcedOpen
					SET_BIT(MPGlobalsAmbience.garageRequests.iForceOpenBS,GET_GARAGE_LOOP_INDEX(iGarageID))
					NET_PRINT("DOOR FORCED OPEN") NET_NL()
				ENDIF
				NET_NL()
				SET_BIT(MPGlobalsAmbience.garageRequests.iOpenBS,GET_GARAGE_LOOP_INDEX(iGarageID))
			ENDIF
		ELSE
			IF IS_BIT_SET(MPGlobalsAmbience.garageRequests.iOpenBS,GET_GARAGE_LOOP_INDEX(iGarageID))
				NET_NL()
				NET_PRINT_TIME() NET_NL()
				NET_PRINT_STRINGS(GET_THIS_SCRIPT_NAME()," REQUEST_GARAGE_OPEN false") NET_NL()
				NET_PRINT_STRINGS("GARAGE ID: ",GET_GARAGE_NAME_FROM_ID(iGarageID)) NET_NL()
				IF bForcedOpen
					CLEAR_BIT(MPGlobalsAmbience.garageRequests.iForceOpenBS,GET_GARAGE_LOOP_INDEX(iGarageID))
					NET_PRINT("CLEARING FORCED OPEN") NET_NL()
				ENDIF
				NET_NL()
				CLEAR_BIT(MPGlobalsAmbience.garageRequests.iOpenBS,GET_GARAGE_LOOP_INDEX(iGarageID))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///  	Checks if a given door is open or closed
FUNC BOOL NEW_IS_GARAGE_SET_OPEN(INT iGarageArrayIndex)
	RETURN NOT MPGlobalsAmbience.globalCopyOfGarageData[iGarageArrayIndex].bDoorLocked
ENDFUNC

/// PURPOSE:
///    Checks to see if a given garage door has closed fully (times out if not close after 20000 ms)
FUNC BOOL NEW_IS_GARAGE_DOOR_PROPERLY_CLOSED(SCRIPT_TIMER& failSafeTimer,GARAGE_DATA_STRUCT garageData)
	INT iFailSafeTime = 10000
	
	IF NOT NEW_IS_GARAGE_SET_OPEN(garageData.iIndex)
		IF NOT HAS_NET_TIMER_STARTED(failSafeTimer)
			NET_PRINT_STRINGS("NEW_IS_GARAGE_DOOR_PROPERLY_CLOSED : starting timer for garage: ",garageData.sGarageName) NET_NL()	
			START_NET_TIMER(failSafeTimer)
		ENDIF
		IF IS_DOOR_CLOSED(garageData.iDoor1ID)
		AND (garageData.iDoor2ID != 0 AND IS_DOOR_CLOSED(garageData.iDoor2ID))
		OR (HAS_NET_TIMER_STARTED(failSafeTimer) AND HAS_NET_TIMER_EXPIRED(failSafeTimer,iFailSafeTime))
			#IF IS_DEBUG_BUILD
				IF g_DB_AllowGarageOutput
					NET_PRINT_STRINGS("IS_GARAGE_DOOR_PROPERLY_CLOSED : door is closed for garage :",garageData.sGarageName) NET_NL()	
				ENDIF
			#ENDIF
			RETURN TRUE		
		ELSE
			#IF IS_DEBUG_BUILD
				IF g_DB_AllowGarageOutput
					NET_PRINT_STRINGS("IS_GARAGE_DOOR_PROPERLY_CLOSED : IS_DOOR_CLOSED(iDoor) is false for garage ",garageData.sGarageName) NET_NL()	
				ENDIF
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF g_DB_AllowGarageOutput
				NET_PRINT_STRINGS("IS_GARAGE_DOOR_PROPERLY_CLOSED : IS_GARAGE_SET_OPEN is true for garage ",garageData.sGarageName) NET_NL()	
			ENDIF
		#ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ANYONE_ELSE_IN_VEHICLE(VEHICLE_INDEX theVeh)
	PED_INDEX thePed
	INT i
	FOR i= -1 TO ENUM_TO_INT(VS_EXTRA_RIGHT_3)
		thePed = GET_PED_IN_VEHICLE_SEAT(theVeh,INT_TO_ENUM(VEHICLE_SEAT,i))
		IF DOES_ENTITY_EXIST(thePed)
			IF thePed != GET_PLAYER_PED(PLAYER_ID())
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC DISABLE_WARP_OUT_OF_LOCKED_GARAGE(BOOL bOnOff = FALSE)
	IF MPGlobalsAmbience.g_bDisableWarpOutOfLockedGarage != bOnOff
		IF bOnOff
			NET_PRINT_STRINGS("Disable warp out of locked garage called by :", GET_THIS_SCRIPT_NAME()) NET_NL()
		ELSE
			NET_PRINT_STRINGS("Disable warp out of locked garage cleared by :", GET_THIS_SCRIPT_NAME()) NET_NL()
		ENDIF
		MPGlobalsAmbience.g_bDisableWarpOutOfLockedGarage = bOnOff
	ENDIF
ENDPROC

PROC DISABLE_WARP_OUT_OF_LOCKED_GARAGE_FADE(BOOL bOnOff = FALSE)
	IF MPGlobalsAmbience.g_bDisableWarpOutOfLockedGarageFade != bOnOff
		MPGlobalsAmbience.g_bDisableWarpOutOfLockedGarageFade = bOnOff
	ENDIF
ENDPROC

PROC DISABLE_GARAGE_CAMERA(BOOL bOnOff = FALSE)
	IF MPGlobalsAmbience.g_bDisableGarageCam != bOnOff
		MPGlobalsAmbience.g_bDisableGarageCam = bOnOff
	ENDIF
ENDPROC

FUNC BOOL IS_GARAGE_CAM_ACTIVE()
	RETURN NOT MPGlobalsAmbience.g_bDisableGarageCam
ENDFUNC

FUNC BOOL IS_ENTITY_IN_RANGE_OF_GARAGE(ENTITY_INDEX thisEntity,INT iGarageIDHash, INT iRange = 18)
	///  	Check if a given Entity is in the opening range of the Garage
	IF iGarageIDHash != -1
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(thisEntity), GET_GARAGE_LOCATION(iGarageIDHash)) <= iRange
			RETURN TRUE	
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL NEW_IS_GARAGE_DOOR_CLEAR(GARAGE_DATA_STRUCT garageData)
	//two door garages
	IF garageData.iDoor2ID != 0
		IF NOT IS_GARAGE_EMPTY(garageData.iGarageIDHash,TRUE,2)
		OR NOT IS_GARAGE_EMPTY(garageData.iGarageIDHash,TRUE,3)
			RETURN FALSE
		ENDIF
	ELSE
		IF NOT IS_GARAGE_EMPTY(garageData.iGarageIDHash,TRUE,2)
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_ENTITY_IN_GARAGE(ENTITY_INDEX thisEntity,INT iGarageIDHash,BOOL bFullyInside, SCRIPT_TIMER& SyncDelayTimerGarage, FLOAT fMargin = 0.0, INT iSyncDelayTime = 500, BOOL bSkipDelay =TRUE, BOOL bIgnoreDoorCheck = FALSE)
	IF NOT HAS_NET_TIMER_STARTED(SyncDelayTimerGarage)
		START_NET_TIMER(SyncDelayTimerGarage)
	ENDIF
	
	PED_INDEX thePed
	VEHICLE_INDEX theVeh
	PLAYER_INDEX playerIndex
	GARAGE_DATA_STRUCT tempGarageData
	GET_GARAGE_DETAILS(tempGarageData,iGarageIDHash)
	IF DOES_ENTITY_EXIST(thisEntity)
		IF NOT IS_ENTITY_DEAD(thisEntity)
			IF IS_ENTITY_IN_RANGE_OF_GARAGE(thisEntity,iGarageIDHash,30)
				IF IS_ENTITY_A_PED(thisEntity)
					thePed = GET_PED_INDEX_FROM_ENTITY_INDEX(thisEntity) 
					IF NOT IS_PED_INJURED(thePed)
						IF IS_PED_A_PLAYER(thePed)
							playerIndex = NETWORK_GET_PLAYER_INDEX_FROM_PED(thePed)
							IF bFullyInside
								IF IS_PLAYER_ENTIRELY_INSIDE_GARAGE(iGarageIDHash,playerIndex,fMargin)
									IF bIgnoreDoorCheck 
									OR (NOT IS_PLAYER_PARTIALLY_INSIDE_GARAGE(iGarageIDHash,playerIndex,2)
									AND NOT ((tempGarageData.iDoor2ID != 0)
									AND IS_PLAYER_PARTIALLY_INSIDE_GARAGE(iGarageIDHash,playerIndex,3)))
										#IF IS_DEBUG_BUILD
											IF g_DB_AllowGarageOutput
												NET_PRINT("IS_ENTITY_IN_GARAGE: object is fully in garage") NET_NL()
											ENDIF
										#ENDIF
										IF bSkipDelay
											RETURN TRUE
										ELIF iSyncDelayTime <= 0
										OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),SyncDelayTimerGarage.timer)) >= iSyncDelayTime
											RETURN TRUE
										ELSE
											RETURN FALSE
										ENDIF
									ENDIF
									#IF IS_DEBUG_BUILD
										IF g_DB_AllowGarageOutput
											NET_PRINT("IS_ENTITY_IN_GARAGE: object is not fully in garage") NET_NL()
										ENDIF
									#ENDIF
								ENDIF
							ELSE
								IF IS_PLAYER_ENTIRELY_INSIDE_GARAGE(iGarageIDHash,playerIndex,fMargin)
									#IF IS_DEBUG_BUILD
										IF g_DB_AllowGarageOutput
											NET_PRINT("IS_ENTITY_IN_GARAGE: object is fully in garage") NET_NL()
										ENDIF
									#ENDIF
									IF bSkipDelay
										RETURN TRUE
									ELIF iSyncDelayTime <= 0
									OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),SyncDelayTimerGarage.timer)) >= iSyncDelayTime
										RETURN TRUE
									ELSE
										RETURN FALSE
									ENDIF
								ELIF IS_PLAYER_PARTIALLY_INSIDE_GARAGE(iGarageIDHash,playerIndex,2)
									#IF IS_DEBUG_BUILD
										IF g_DB_AllowGarageOutput
											NET_PRINT("IS_ENTITY_IN_GARAGE: object is partially in garage") NET_NL()
										ENDIF
									#ENDIF
									IF bSkipDelay
										RETURN TRUE
									ELIF iSyncDelayTime <= 0
									OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),SyncDelayTimerGarage.timer)) >= iSyncDelayTime
										RETURN TRUE
									ELSE
										RETURN FALSE
									ENDIF
								ELIF (tempGarageData.iDoor2ID != 0)
								AND IS_PLAYER_PARTIALLY_INSIDE_GARAGE(iGarageIDHash,playerIndex,3)
									#IF IS_DEBUG_BUILD
										IF g_DB_AllowGarageOutput
											NET_PRINT("IS_ENTITY_IN_GARAGE: object is partially in garage") NET_NL()
										ENDIF
									#ENDIF
									IF bSkipDelay
										RETURN TRUE
									ELIF iSyncDelayTime <= 0
									OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),SyncDelayTimerGarage.timer)) >= iSyncDelayTime
										RETURN TRUE
									ELSE
										RETURN FALSE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF IS_PED_IN_ANY_VEHICLE(thePed)
								theVeh = GET_VEHICLE_PED_IS_IN(thePed)
								IF DOES_ENTITY_EXIST(theVeh)
									
									IF bFullyInside
										IF IS_OBJECT_ENTIRELY_INSIDE_GARAGE(iGarageIDHash,theVeh,fMargin)
											IF bIgnoreDoorCheck 
											OR (NOT IS_OBJECT_PARTIALLY_INSIDE_GARAGE(iGarageIDHash,theVeh,2)
											AND NOT ((tempGarageData.iDoor2ID != 0)
											AND IS_OBJECT_PARTIALLY_INSIDE_GARAGE(iGarageIDHash,theVeh,3)))
												#IF IS_DEBUG_BUILD
													IF g_DB_AllowGarageOutput
														NET_PRINT("IS_ENTITY_IN_GARAGE: object is fully in garage") NET_NL()
													ENDIF
												#ENDIF
												IF bSkipDelay
													RETURN TRUE
												ELIF iSyncDelayTime <= 0
												OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),SyncDelayTimerGarage.timer)) >= iSyncDelayTime
													RETURN TRUE
												ELSE
													RETURN FALSE
												ENDIF
											ENDIF
											#IF IS_DEBUG_BUILD
												IF g_DB_AllowGarageOutput
													NET_PRINT("IS_ENTITY_IN_GARAGE: object is not fully in garage") NET_NL()
												ENDIF
											#ENDIF
										ENDIF
									ELSE
										IF IS_OBJECT_ENTIRELY_INSIDE_GARAGE(iGarageIDHash,theVeh,fMargin)
											#IF IS_DEBUG_BUILD
												IF g_DB_AllowGarageOutput
													NET_PRINT("IS_ENTITY_IN_GARAGE: object is fully in garage") NET_NL()
												ENDIF
											#ENDIF
											IF bSkipDelay
												RETURN TRUE
											ELIF iSyncDelayTime <= 0
											OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),SyncDelayTimerGarage.timer)) >= iSyncDelayTime
												RETURN TRUE
											ELSE
												RETURN FALSE
											ENDIF
									
										ELIF IS_OBJECT_PARTIALLY_INSIDE_GARAGE(iGarageIDHash,theVeh,2)
											#IF IS_DEBUG_BUILD
												IF g_DB_AllowGarageOutput
													NET_PRINT("IS_ENTITY_IN_GARAGE: object is partially in garage") NET_NL()
												ENDIF
											#ENDIF
											IF bSkipDelay
												RETURN TRUE
											ELIF iSyncDelayTime <= 0
											OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),SyncDelayTimerGarage.timer)) >= iSyncDelayTime
												RETURN TRUE
											ELSE
												RETURN FALSE
											ENDIF
										ELIF (tempGarageData.iDoor2ID != 0)
										AND IS_OBJECT_PARTIALLY_INSIDE_GARAGE(iGarageIDHash,theVeh,3)
											#IF IS_DEBUG_BUILD
												IF g_DB_AllowGarageOutput
													NET_PRINT("IS_ENTITY_IN_GARAGE: object is partially in garage") NET_NL()
												ENDIF
											#ENDIF
											IF bSkipDelay
												RETURN TRUE
											ELIF iSyncDelayTime <= 0
											OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),SyncDelayTimerGarage.timer)) >= iSyncDelayTime
												RETURN TRUE
											ELSE
												RETURN FALSE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF bFullyInside
									IF IS_OBJECT_ENTIRELY_INSIDE_GARAGE(iGarageIDHash,thisEntity,fMargin)
										IF bIgnoreDoorCheck 
										OR (NOT IS_OBJECT_PARTIALLY_INSIDE_GARAGE(iGarageIDHash,thisEntity,2)
										AND NOT ((tempGarageData.iDoor2ID != 0)
										AND IS_OBJECT_PARTIALLY_INSIDE_GARAGE(iGarageIDHash,thisEntity,3)))
											#IF IS_DEBUG_BUILD
												IF g_DB_AllowGarageOutput
													NET_PRINT("IS_ENTITY_IN_GARAGE: object is fully in garage") NET_NL()
												ENDIF
											#ENDIF
											IF bSkipDelay
												RETURN TRUE
											ELIF iSyncDelayTime <= 0
											OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),SyncDelayTimerGarage.timer)) >= iSyncDelayTime
												RETURN TRUE
											ELSE
												RETURN FALSE
											ENDIF
										ENDIF
										#IF IS_DEBUG_BUILD
											IF g_DB_AllowGarageOutput
												NET_PRINT("IS_ENTITY_IN_GARAGE: object is not fully in garage") NET_NL()
											ENDIF
										#ENDIF
									ENDIF
								ELSE
									IF IS_OBJECT_ENTIRELY_INSIDE_GARAGE(iGarageIDHash,thisEntity,fMargin)
										#IF IS_DEBUG_BUILD
											IF g_DB_AllowGarageOutput
												NET_PRINT("IS_ENTITY_IN_GARAGE: object is fully in garage") NET_NL()
											ENDIF
										#ENDIF
										IF bSkipDelay
											RETURN TRUE
										ELIF iSyncDelayTime <= 0
										OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),SyncDelayTimerGarage.timer)) >= iSyncDelayTime
											RETURN TRUE
										ELSE
											RETURN FALSE
										ENDIF
									ELIF IS_OBJECT_PARTIALLY_INSIDE_GARAGE(iGarageIDHash,thisEntity,2)
										#IF IS_DEBUG_BUILD
											IF g_DB_AllowGarageOutput
												NET_PRINT("IS_ENTITY_IN_GARAGE: object is partially in garage") NET_NL()
											ENDIF
										#ENDIF
										IF bSkipDelay
											RETURN TRUE
										ELIF iSyncDelayTime <= 0
										OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),SyncDelayTimerGarage.timer)) >= iSyncDelayTime
											RETURN TRUE
										ELSE
											RETURN FALSE
										ENDIF
									ELIF (tempGarageData.iDoor2ID != 0)
									AND IS_OBJECT_PARTIALLY_INSIDE_GARAGE(iGarageIDHash,thisEntity,3)
										#IF IS_DEBUG_BUILD
											IF g_DB_AllowGarageOutput
												NET_PRINT("IS_ENTITY_IN_GARAGE: object is partially in garage") NET_NL()
											ENDIF
										#ENDIF
										IF bSkipDelay
											RETURN TRUE
										ELIF iSyncDelayTime <= 0
										OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),SyncDelayTimerGarage.timer)) >= iSyncDelayTime
											RETURN TRUE
										ELSE
											RETURN FALSE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_ENTITY_A_VEHICLE(thisEntity)
					OR IS_ENTITY_AN_OBJECT(thisEntity)
						IF bFullyInside
							IF IS_OBJECT_ENTIRELY_INSIDE_GARAGE(iGarageIDHash, thisEntity,fMargin)
								IF bIgnoreDoorCheck OR
								(NOT IS_OBJECT_PARTIALLY_INSIDE_GARAGE(iGarageIDHash,thisEntity,2)
								AND NOT ((tempGarageData.iDoor2ID != 0)
								AND IS_OBJECT_PARTIALLY_INSIDE_GARAGE(iGarageIDHash,thisEntity,3)))
									#IF IS_DEBUG_BUILD
										IF g_DB_AllowGarageOutput
											NET_PRINT("IS_ENTITY_IN_GARAGE: object is fully in garage") NET_NL()
										ENDIF
									#ENDIF
									IF bSkipDelay
										RETURN TRUE
									ELIF iSyncDelayTime <= 0
									OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),SyncDelayTimerGarage.timer)) >= iSyncDelayTime
										RETURN TRUE
									ELSE
										RETURN FALSE
									ENDIF
								ENDIF
								#IF IS_DEBUG_BUILD
									IF g_DB_AllowGarageOutput
										NET_PRINT("IS_ENTITY_IN_GARAGE: object is not fully in garage") NET_NL()
									ENDIF
								#ENDIF
							ELSE
								#IF IS_DEBUG_BUILD
									IF g_DB_AllowGarageOutput
										NET_PRINT("IS_ENTITY_IN_GARAGE: object is not fully in garage") NET_NL()
									ENDIF
								#ENDIF
							ENDIF
						ELSE
							IF IS_OBJECT_ENTIRELY_INSIDE_GARAGE(iGarageIDHash,thisEntity,fMargin)
								#IF IS_DEBUG_BUILD
									IF g_DB_AllowGarageOutput
										NET_PRINT("IS_ENTITY_IN_GARAGE: object is fully in garage") NET_NL()
									ENDIF
								#ENDIF
								IF bSkipDelay
									RETURN TRUE
								ELIF iSyncDelayTime <= 0
								OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),SyncDelayTimerGarage.timer)) >= iSyncDelayTime
									RETURN TRUE
								ELSE
									RETURN FALSE
								ENDIF
							ELIF IS_OBJECT_PARTIALLY_INSIDE_GARAGE(iGarageIDHash,thisEntity,2)
								#IF IS_DEBUG_BUILD
									IF g_DB_AllowGarageOutput
										NET_PRINT("IS_ENTITY_IN_GARAGE: object is partially in garage") NET_NL()
									ENDIF
								#ENDIF
								IF bSkipDelay
									RETURN TRUE
								ELIF iSyncDelayTime <= 0
								OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),SyncDelayTimerGarage.timer)) >= iSyncDelayTime
									RETURN TRUE
								ELSE
									RETURN FALSE
								ENDIF
							ELIF (tempGarageData.iDoor2ID != 0)
							AND IS_OBJECT_PARTIALLY_INSIDE_GARAGE(iGarageIDHash,thisEntity,3)
								#IF IS_DEBUG_BUILD
									IF g_DB_AllowGarageOutput
										NET_PRINT("IS_ENTITY_IN_GARAGE: object is partially in garage") NET_NL()
									ENDIF
								#ENDIF
								IF bSkipDelay
									RETURN TRUE
								ELIF iSyncDelayTime <= 0
								OR ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),SyncDelayTimerGarage.timer)) >= iSyncDelayTime
									RETURN TRUE
								ELSE
									RETURN FALSE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						SCRIPT_ASSERT("IS_ENTITY_IN_GARAGE: trying to check an entity that isnt an object, ped or vehicle!")
						NET_PRINT_STRINGS("IS_ENTITY_IN_GARAGE: trying to check an entity that isnt an object, ped or vehicle!! in script",GET_THIS_SCRIPT_NAME()) NET_NL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RESET_NET_TIMER(SyncDelayTimerGarage)
	RETURN FALSE
ENDFUNC

///// PURPOSE:
/////  	Check if local player is in a give garage
FUNC BOOL IS_PLAYER_IN_THIS_GARAGE(INT iGarageID)
	SCRIPT_TIMER theTimer
	RETURN IS_ENTITY_IN_GARAGE(PLAYER_PED_ID(),iGarageID,TRUE,theTimer)
ENDFUNC

FUNC BOOL IS_WARP_OUT_OF_GARAGE_IN_PROGRESS()
	IF MPGlobalsAmbience.g_iWarpOutOfGarageStage != 0 
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_WARP_OUT_OF_GARAGE_COMPLETE()
	IF MPGlobalsAmbience.g_iWarpOutOfGarageStage = 0 
		IF IS_SCREEN_FADED_IN()
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_GARAGE_CAM_PANNING()
	IF MPGlobalsAmbience.g_iGarageCamStage = GARAGE_CAM_STATE_MOVING
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL NEW_IS_GARAGE_OCCUPIED(NEW_GARAGE_DOOR_DATA_SERVER& serverGarageBD,GARAGE_DATA_STRUCT garageData)
	
	IF serverGarageBD.bOccupied
	OR serverGarageBD.iPlayerRequestedBS != 0
		//only need to check if door isnt forced open
		IF serverGarageBD.iPlayerRequestedForcedBS = 0
			IF IS_GARAGE_EMPTY(garageData.iGarageIDHash,TRUE)
				IF serverGarageBD.bOccupied
					NET_PRINT_STRINGS("NEW_IS_GARAGE_OCCUPIED: Reseting occupied timer because garage is empty : ",garageData.sGarageName ) NET_NL()
					serverGarageBD.bOccupied = FALSE
				ENDIF
				IF serverGarageBD.bStartClearArea
					serverGarageBD.bStartClearArea = FALSE
				ENDIF
				RETURN FALSE
			ELSE
				#IF IS_DEBUG_BUILD
					IF g_DB_AllowGarageOutput
						NET_PRINT_STRINGS("Garage is at least partially occupied: ", garageData.sGarageName) NET_NL()
					ENDIF
				#ENDIF
				serverGarageBD.bOccupied = TRUE
				IF IS_ANY_ENTITY_ENTIRELY_INSIDE_GARAGE(garageData.iGarageIDHash,TRUE,TRUE,FALSE)
				AND ARE_ENTITIES_ENTIRELY_INSIDE_GARAGE(garageData.iGarageIDHash,FALSE,TRUE,FALSE)
					serverGarageBD.bStartClearArea = TRUE
					#IF IS_DEBUG_BUILD
					IF g_DB_AllowGarageOutput
						NET_PRINT_STRINGS("IS_GARAGE_OCCUPIED_TIMER_UP is up for garage ", garageData.sGarageName) NET_NL()
					ENDIF
					#ENDIF
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF serverGarageBD.bOccupied
		NET_PRINT_STRINGS("Reseting occupied timer because gone through checks and not returned a value : ",garageData.sGarageName ) NET_NL()
		serverGarageBD.bOccupied = FALSE
	ENDIF
	IF serverGarageBD.bStartClearArea
		serverGarageBD.bStartClearArea = FALSE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SCRIPT_CHECK_IS_ENTITY_FULLY_IN_GARAGE(ENTITY_INDEX thisEntity, INT iGarageID, SCRIPT_TIMER& SyncDelayTimerGarage, FLOAT fMargin = 0.0, INT iSyncDelayTime = 500, BOOL bSkipDelay = TRUE, BOOL bIgnoreDoorBox = FALSE)

	IF NOT HAS_NET_TIMER_STARTED(SyncDelayTimerGarage)
		START_NET_TIMER(SyncDelayTimerGarage)
	ENDIF

	//IF GlobalServerBD_CNC.bUseNewGarageStuff
	fMargin = 0
	IF IS_ENTITY_IN_GARAGE(thisEntity,iGarageID,TRUE,SyncDelayTimerGarage,fMargin,iSyncDelayTime,bSkipDelay,bIgnoreDoorBox)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CLEAN_UP_GARAGE_INTERIOR(INT iGarageID)	
	NET_PRINT_STRINGS("Clearing garage  : ",GET_GARAGE_NAME_FROM_ID(iGarageID)) NET_NL()
	CLEAR_OBJECTS_INSIDE_GARAGE(igarageID,TRUE,TRUE,FALSE,TRUE)
ENDPROC

PROC ENABLE_LEAVE_VEHICLE_WHEN_IN_GARAGE()
	IF NOT MPGlobalsAmbience.g_bAllowGetOutOfVehicle
		MPGlobalsAmbience.g_bAllowGetOutOfVehicle = TRUE
		NET_NL()
		NET_PRINT_STRINGS(GET_THIS_SCRIPT_NAME()," ENABLE_LEAVE_VEHICLE_WHEN_IN_GARAGE") NET_NL()
		NET_NL()
	ENDIF
	REINIT_NET_TIMER(MPGlobalsAmbience.g_AllowGetOutOfVehicleTimer)
ENDPROC

PROC DISABLE_LEAVE_VEHICLE_WHEN_IN_GARAGE()
	IF MPGlobalsAmbience.g_bAllowGetOutOfVehicle
		MPGlobalsAmbience.g_bAllowGetOutOfVehicle = FALSE
		NET_NL()
		NET_PRINT_STRINGS(GET_THIS_SCRIPT_NAME()," DISABLE_LEAVE_VEHICLE_WHEN_IN_GARAGE") NET_NL()
		NET_NL()
		RESET_NET_TIMER(MPGlobalsAmbience.g_AllowGetOutOfVehicleTimer)
	ENDIF
ENDPROC
//eof 
