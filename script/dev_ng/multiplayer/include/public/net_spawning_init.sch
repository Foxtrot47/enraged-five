USING "globals.sch"
USING "rage_builtins.sch"
USING "types.sch"
USING "script_maths.sch"
USING "net_spawn_areas.sch"
USING "net_simple_interior_bunker.sch"

PROC INIT_MP_SPAWN_POINTS()
	
	// spawning sector lines
	g_fSpawningSectorXLine[0] = -1200.0
	g_fSpawningSectorXLine[1] = -1200.0
	g_fSpawningSectorXLine[2] = -200.0
	g_fSpawningSectorXLine[3] = 400.0

	g_fSpawningSectorYLine[0] = 3000.0
	g_fSpawningSectorYLine[1] = 0
	g_fSpawningSectorYLine[2] = -1950.0

	
	INT iSector
	REPEAT (NUMBER_OF_SPAWNING_SECTORS) iSector
		g_iTotalForbiddenProblemAreasForSector[iSector] = 0
		g_iTotalNodesProblemAreasForSector[iSector] = 0
	ENDREPEAT

	// vagos outside the contact garage
	SpawnPoints_SimonGarage[0].Pos = <<1199.8704, -3107.2495, 5.0280 >>
	SpawnPoints_SimonGarage[0].heading = 348.8225
	
	SpawnPoints_SimonGarage[1].Pos = <<1198.3704, -3107.2495, 5.0280  >>
	SpawnPoints_SimonGarage[1].heading = 348.8225
	
	SpawnPoints_SimonGarage[2].Pos = <<1196.8704, -3107.2495, 5.0280  >>
	SpawnPoints_SimonGarage[2].heading = 348.8225
	
	SpawnPoints_SimonGarage[3].Pos = <<1195.3704, -3107.2495, 5.0280  >>
	SpawnPoints_SimonGarage[3].heading = 348.8225
	
	SpawnPoints_SimonGarage[4].Pos = <<1199.8704, -3104.5, 5.0280  >>
	SpawnPoints_SimonGarage[4].heading = 348.8225
	
	SpawnPoints_SimonGarage[5].Pos = <<1198.3704, -3104.5, 5. >>
	SpawnPoints_SimonGarage[5].heading = 348.8225
	
	SpawnPoints_SimonGarage[6].Pos = <<1196.8704, -3104.5, 5.0280  >>
	SpawnPoints_SimonGarage[6].heading = 348.8225
	
	SpawnPoints_SimonGarage[7].Pos = <<1195.3704, -3104.5, 5.0280  >>
	SpawnPoints_SimonGarage[7].heading = 348.8225


	// **********************************
	// 			AIRPORT ARRIVALS
	// **********************************	
	SpawnPoints_AirportArrivals[0].Pos = << -1049.7976, -2721.1033, 12.7567 >>
	SpawnPoints_AirportArrivals[0].heading = 313.6886
	
	SpawnPoints_AirportArrivals[1].Pos = << -1056.5377, -2717.5303, 12.7567 >>
	SpawnPoints_AirportArrivals[1].heading = 301.7651
	
	SpawnPoints_AirportArrivals[2].Pos = << -1034.2781, -2727.3909, 12.7565 >>
	SpawnPoints_AirportArrivals[2].heading = 259.8425
	
	SpawnPoints_AirportArrivals[3].Pos = << -1034.2489, -2738.5898, 12.7658 >>
	SpawnPoints_AirportArrivals[3].heading = 330.9371 
	
	SpawnPoints_AirportArrivals[4].Pos = << -1014.1395, -2739.1099, 12.7565 >>
	SpawnPoints_AirportArrivals[4].heading = 343.5448 
	
	SpawnPoints_AirportArrivals[5].Pos = << -1038.4586, -2738.6113, 19.1693 >>
	SpawnPoints_AirportArrivals[5].heading = 328.8999 
	
	SpawnPoints_AirportArrivals[6].Pos = << -1033.6947, -2733.9695, 19.1693 >>
	SpawnPoints_AirportArrivals[6].heading = 334.6436 
	
	SpawnPoints_AirportArrivals[7].Pos = << -1026.7617, -2737.7117, 19.1693 >>
	SpawnPoints_AirportArrivals[7].heading = 331.5706 
	
	SpawnPoints_AirportArrivals[8].Pos = << -1042.5803, -2728.8960, 19.1693 >>
	SpawnPoints_AirportArrivals[8].heading = 318.7747 
	
	SpawnPoints_AirportArrivals[9].Pos = << -1024.6107, -2485.5676, 19.1693 >>
	SpawnPoints_AirportArrivals[9].heading = 256.9353 
	
	SpawnPoints_AirportArrivals[10].Pos = << -1029.5087, -2493.7449, 19.1693 >>
	SpawnPoints_AirportArrivals[10].heading = 228.7013 
	
	SpawnPoints_AirportArrivals[11].Pos = << -1037.4441, -2507.0737, 19.1693 >>
	SpawnPoints_AirportArrivals[11].heading = 220.0311 
	
	SpawnPoints_AirportArrivals[12].Pos = << -1046.0573, -2522.8992, 19.1693 >>
	SpawnPoints_AirportArrivals[12].heading = 280.9855
	
	SpawnPoints_AirportArrivals[13].Pos = << -1056.6256, -2540.5815, 19.1693 >>
	SpawnPoints_AirportArrivals[13].heading = 271.9905
	
	SpawnPoints_AirportArrivals[14].Pos = << -1063.4434, -2553.3279, 19.1693 >>
	SpawnPoints_AirportArrivals[14].heading = 263.3782
	
	SpawnPoints_AirportArrivals[15].Pos = << -1069.1960, -2563.1204, 19.1693 >>
	SpawnPoints_AirportArrivals[15].heading = 290.6831
	
	SpawnPoints_AirportArrivals[16].Pos = << -1074.8744, -2571.3569, 19.1693 >>
	SpawnPoints_AirportArrivals[16].heading = 290.4732
	
	SpawnPoints_AirportArrivals[17].Pos = << -1079.1490, -2580.3411, 19.2128 >>
	SpawnPoints_AirportArrivals[17].heading = 258.5498
	
	SpawnPoints_AirportArrivals[18].Pos = << -1040.7863, -2743.9641, 12.9440 >>
	SpawnPoints_AirportArrivals[18].heading = 339.8474
	
	SpawnPoints_AirportArrivals[19].Pos = << -1049.3521, -2728.2271, 12.7567 >>
	SpawnPoints_AirportArrivals[19].heading = 302.1164
	
	SpawnPoints_AirportArrivals[20].Pos = << -1034.3351, -2742.5488, 12.8548 >>
	SpawnPoints_AirportArrivals[20].heading = 349.3089
	
	SpawnPoints_AirportArrivals[21].Pos = << -1022.0107, -2743.2319, 12.7565 >>
	SpawnPoints_AirportArrivals[21].heading = 345.8310
	
	SpawnPoints_AirportArrivals[22].Pos = << -1044.7772, -2742.9385, 20.3632 >>
	SpawnPoints_AirportArrivals[22].heading = 329.2591
	
	SpawnPoints_AirportArrivals[23].Pos = << -1039.6914, -2746.7136, 20.3638 >>
	SpawnPoints_AirportArrivals[23].heading = 335.5986
	
	SpawnPoints_AirportArrivals[24].Pos = << -1036.4148, -2736.3542, 19.1693 >>
	SpawnPoints_AirportArrivals[24].heading = 331.6779
	
	SpawnPoints_AirportArrivals[25].Pos = << -1031.3978, -2738.7937, 19.1693 >>
	SpawnPoints_AirportArrivals[25].heading = 4.5205
	

	
	// **********************************
	// 		safe coastal spawn points
	// **********************************		
	SpawnPoints_SafeCoastalPoints[0].Pos = <<1016.9484, 6585.8579, 3.4115>>
	SpawnPoints_SafeCoastalPoints[0].heading = 98.7576
	
	SpawnPoints_SafeCoastalPoints[1].Pos = <<3328.9707, 5152.6040, 17.3044>>
	SpawnPoints_SafeCoastalPoints[1].heading = 105.1404 
	
	SpawnPoints_SafeCoastalPoints[2].Pos = <<2824.9377, 1637.4709, 23.6324>>
	SpawnPoints_SafeCoastalPoints[2].heading = 42.6349 
	
	SpawnPoints_SafeCoastalPoints[3].Pos = <<2781.8447, -711.8494, 4.1963>>
	SpawnPoints_SafeCoastalPoints[3].heading = 104.0899 
	
	SpawnPoints_SafeCoastalPoints[4].Pos = <<178.9389, -3301.4080, 4.6066>>
	SpawnPoints_SafeCoastalPoints[4].heading = 359.9984 
	
	SpawnPoints_SafeCoastalPoints[5].Pos = <<-2843.4014, -29.5661, 3.5779>>
	SpawnPoints_SafeCoastalPoints[5].heading = 333.9116
	
	SpawnPoints_SafeCoastalPoints[6].Pos = <<-2890.7849, 3107.8564, 1.5838>>
	SpawnPoints_SafeCoastalPoints[6].heading = 328.5426
	
	SpawnPoints_SafeCoastalPoints[7].Pos = <<-1283.8376, 5355.3047, 2.2187>>
	SpawnPoints_SafeCoastalPoints[7].heading = 164.6986	
	
	
//	// safe air points
//	INT xStep
//	INT yStep
//	INT iCount
//	REPEAT 9 xStep 
//		REPEAT 12 yStep			
//			SafeSpawn_Air[iCount].x = -4000.0 + (xStep*1000.0)	
//			SafeSpawn_Air[iCount].y = -4000.0 + (yStep*1000.0)	
//			SafeSpawn_Air[iCount].z = 200.0
//			iCount++
//		ENDREPEAT
//	ENDREPEAT
	
	// safe sea points
	SafeSpawn_Sea[0] = <<1084.7950, 6780.1226, 0.6334>>
	SafeSpawn_Sea[1] = <<1703.9890, 6780.3198, 0.4536>>
	SafeSpawn_Sea[2] = <<2269.7429, 6824.0894, 0.0862>>
	SafeSpawn_Sea[3] = <<2829.1245, 6604.9995, 1.3157>>
	SafeSpawn_Sea[4] = <<3343.8538, 6295.3657, 0.2465>>
	SafeSpawn_Sea[5] = <<3516.4421, 5720.6211, 0.7792>>
	SafeSpawn_Sea[6] = <<3662.3813, 5138.5898, 0.1480>>
	SafeSpawn_Sea[7] = <<4068.8958, 4696.5806, 0.8180>>
	SafeSpawn_Sea[8] = <<4138.3364, 4100.3286, 0.4029>>
	SafeSpawn_Sea[9] = <<4022.8774, 3510.3081, 1.0664>>
	SafeSpawn_Sea[10] = <<3824.1414, 2943.9851, 0.4457>>
	SafeSpawn_Sea[11] = <<3569.5142, 2399.3591, -0.1426>>
	SafeSpawn_Sea[12] = <<3317.5916, 1854.5706, 0.5859>>
	SafeSpawn_Sea[13] = <<3231.5251, 1260.7386, 0.4143>>
	SafeSpawn_Sea[14] = <<3258.8389, 660.5713, 0.0319>>
	SafeSpawn_Sea[15] = <<3256.3940, 59.8630, 0.0246>>
	SafeSpawn_Sea[16] = <<3040.2815, -500.4430, 0.5449>>
	SafeSpawn_Sea[17] = <<2850.0952, -1069.5317, 0.4638>>
	SafeSpawn_Sea[18] = <<2755.6140, -1690.6591, 0.2178>>
	SafeSpawn_Sea[19] = <<2537.7212, -2228.3706, 0.0944>>
	SafeSpawn_Sea[20] = <<2063.3379, -2595.9492, 0.1377>>
	SafeSpawn_Sea[21] = <<1556.0791, -2917.6953, 0.5398>>
	SafeSpawn_Sea[22] = <<1219.0659, -3414.1360, -0.0934>>
	SafeSpawn_Sea[23] = <<619.3845, -3457.3271, 0.7178>>
	SafeSpawn_Sea[24] = <<33.8648, -3323.8645, 0.1324>>
	SafeSpawn_Sea[25] = <<-565.7127, -3360.1587, 0.1439>>
	SafeSpawn_Sea[26] = <<-1099.4989, -3634.8330, 0.0691>>
	SafeSpawn_Sea[27] = <<-1672.0020, -3451.4546, 0.2625>>
	SafeSpawn_Sea[28] = <<-2046.0405, -2981.6440, 0.1047>>
	SafeSpawn_Sea[29] = <<-1807.4917, -2430.7720, 0.4074>>
	SafeSpawn_Sea[30] = <<-1466.0544, -1937.1863, 0.6426>>
	SafeSpawn_Sea[31] = <<-1768.5295, -1418.5605, 0.0580>>
	SafeSpawn_Sea[32] = <<-2072.3132, -900.5018, 1.9352>>
	SafeSpawn_Sea[33] = <<-2450.2795, -433.4315, 0.1145>>
	SafeSpawn_Sea[34] = <<-2966.7515, -127.1012, 0.2979>>
	SafeSpawn_Sea[35] = <<-3226.1433, 414.2141, 0.8243>>
	SafeSpawn_Sea[36] = <<-3460.6350, 966.7424, 0.4288>>
	SafeSpawn_Sea[37] = <<-3256.4937, 1531.5151, 1.8150>>
	SafeSpawn_Sea[38] = <<-3117.6655, 2115.7649, 0.5670>>
	SafeSpawn_Sea[39] = <<-2794.2366, 2621.7136, 0.2295>>
	SafeSpawn_Sea[40] = <<-3128.4104, 3120.3203, 0.5338>>
	SafeSpawn_Sea[41] = <<-2868.1560, 3661.3792, 0.5250>>
	SafeSpawn_Sea[42] = <<-2561.9917, 4178.2451, 0.1569>>
	SafeSpawn_Sea[43] = <<-2227.0786, 4676.5181, 0.3610>>
	SafeSpawn_Sea[44] = <<-1887.1825, 5171.1313, 0.2300>>
	SafeSpawn_Sea[45] = <<-1402.7784, 5525.5967, 0.9588>>
	SafeSpawn_Sea[46] = <<-1121.7239, 6056.0161, 1.5945>>
	SafeSpawn_Sea[47] = <<-665.5502, 6447.0269, 0.6195>>
	SafeSpawn_Sea[48] = <<-216.1800, 6844.7954, 1.3456>>
	SafeSpawn_Sea[49] = <<45.4981, 7386.1118, 1.3029>>
	SafeSpawn_Sea[50] = <<474.3181, 6965.3843, 0.2172>>
	SafeSpawn_Sea[51] = <<2263.2588, 4362.5601, 30.0000>>
	SafeSpawn_Sea[52] = <<1692.4845, 4174.8960, 29.9974>>
	SafeSpawn_Sea[53] = <<1123.3571, 3982.5520, 30.1039>>
	SafeSpawn_Sea[54] = <<529.5110, 3894.9949, 29.9483>>
	SafeSpawn_Sea[55] = <<-28.5304, 4115.5186, 30.0457>>
	SafeSpawn_Sea[56] = <<-524.6341, 4420.9160, 30.0000>>
	SafeSpawn_Sea[57] = <<-1128.6970, 4403.8569, 10.4817>>
	SafeSpawn_Sea[58] = <<-1734.7711, 4500.3584, 0.0858>>
	SafeSpawn_Sea[59] = <<213.6361, 3680.3970, 30.0000>>
	SafeSpawn_Sea[60] = <<-1.2548, 3120.0703, 25.8060>>
	SafeSpawn_Sea[61] = <<-574.3451, 2942.1306, 13.2769>>
	SafeSpawn_Sea[62] = <<-1151.2861, 2775.7949, 0.0709>>
	SafeSpawn_Sea[63] = <<-1724.6147, 2598.7471, 0.4245>>
	SafeSpawn_Sea[64] = <<-2324.8623, 2598.8186, 0.0813>>
	SafeSpawn_Sea[65] = <<-1445.8080, 2263.2290, 16.8734>>
	SafeSpawn_Sea[66] = <<1966.8610, 180.0000, 160.4577>>
	SafeSpawn_Sea[67] = <<683.6877, -1634.3142, 9.3088>>
	SafeSpawn_Sea[68] = <<624.1580, -2231.2754, 0.1886>>
	SafeSpawn_Sea[69] = <<618.7031, -2533.4478, 0.1114>>
	SafeSpawn_Sea[70] = <<976.0086, -2746.6235, -0.4330>>
	SafeSpawn_Sea[71] = <<-62.1295, -2327.7605, 0.0000>>
	SafeSpawn_Sea[72] = <<-830.0434, -1450.0911, 0.0000>>
	SafeSpawn_Sea[73] = <<-64.4245, -1922.6855, 0.0000>>
	
//	SafeSpawn_Air[0] = <<-4000.0000, -4000.0000, 200.0000>>
//	SafeSpawn_Air[1] = <<-4000.0000, -3000.0000, 200.0000>>
//	SafeSpawn_Air[2] = <<-4000.0000, -2000.0000, 200.0000>>
//	SafeSpawn_Air[3] = <<-4000.0000, -1000.0000, 200.0000>>
//	SafeSpawn_Air[4] = <<-4000.0000, 0.0000, 200.0000>>
//	SafeSpawn_Air[5] = <<-4000.0000, 1000.0000, 200.0000>>
//	SafeSpawn_Air[6] = <<-4000.0000, 2000.0000, 200.0000>>
//	SafeSpawn_Air[7] = <<-4000.0000, 3000.0000, 200.0000>>
//	SafeSpawn_Air[8] = <<-4000.0000, 4000.0000, 200.0000>>
//	SafeSpawn_Air[9] = <<-4000.0000, 5000.0000, 200.0000>>
//	SafeSpawn_Air[10] = <<-4000.0000, 6000.0000, 200.0000>>
//	SafeSpawn_Air[11] = <<-4000.0000, 7000.0000, 200.0000>>
//	SafeSpawn_Air[12] = <<-3000.0000, -4000.0000, 200.0000>>
//	SafeSpawn_Air[13] = <<-3000.0000, -3000.0000, 200.0000>>
//	SafeSpawn_Air[14] = <<-3000.0000, -2000.0000, 200.0000>>
//	SafeSpawn_Air[15] = <<-3000.0000, -1000.0000, 200.0000>>
//	SafeSpawn_Air[16] = <<-3000.0000, 0.0000, 200.0000>>
//	SafeSpawn_Air[17] = <<-3000.0000, 1000.0000, 299.4446>>
//	SafeSpawn_Air[18] = <<-3000.0000, 2000.0000, 200.0000>>
//	SafeSpawn_Air[19] = <<-3000.0000, 3000.0000, 200.0000>>
//	SafeSpawn_Air[20] = <<-3000.0000, 4000.0000, 200.0000>>
//	SafeSpawn_Air[21] = <<-3000.0000, 5000.0000, 200.0000>>
//	SafeSpawn_Air[22] = <<-3000.0000, 6000.0000, 200.0000>>
//	SafeSpawn_Air[23] = <<-3000.0000, 7000.0000, 200.0000>>
//	SafeSpawn_Air[24] = <<-2000.0000, -4000.0000, 200.0000>>
//	SafeSpawn_Air[25] = <<-2000.0000, -3000.0000, 200.0000>>
//	SafeSpawn_Air[26] = <<-2000.0000, -2000.0000, 200.0000>>
//	SafeSpawn_Air[27] = <<-2000.0000, -1000.0000, 200.0000>>
//	SafeSpawn_Air[28] = <<-2000.0000, 0.0000, 313.9447>>
//	SafeSpawn_Air[29] = <<-2000.0000, 1000.0000, 390.8466>>
//	SafeSpawn_Air[30] = <<-2000.0000, 2000.0000, 375.2229>>
//	SafeSpawn_Air[31] = <<-2000.0000, 3000.0000, 231.8103>>
//	SafeSpawn_Air[32] = <<-2000.0000, 4000.0000, 387.9286>>
//	SafeSpawn_Air[33] = <<-2000.0000, 5000.0000, 200.0000>>
//	SafeSpawn_Air[34] = <<-2000.0000, 6000.0000, 200.0000>>
//	SafeSpawn_Air[35] = <<-2000.0000, 7000.0000, 200.0000>>
//	SafeSpawn_Air[36] = <<-1000.0000, -4000.0000, 200.0000>>
//	SafeSpawn_Air[37] = <<-1000.0000, -3000.0000, 200.0000>>
//	SafeSpawn_Air[38] = <<-1000.0000, -2000.0000, 200.0000>>
//	SafeSpawn_Air[39] = <<-1000.0000, -1000.0000, 209.7936>>
//	SafeSpawn_Air[40] = <<-1000.0000, 0.0000, 246.6623>>
//	SafeSpawn_Air[41] = <<-1000.0000, 1000.0000, 366.9084>>
//	SafeSpawn_Air[42] = <<-1000.0000, 2000.0000, 300.0817>>
//	SafeSpawn_Air[43] = <<-1000.0000, 3000.0000, 232.5099>>
//	SafeSpawn_Air[44] = <<-1000.0000, 4000.0000, 469.5583>>
//	SafeSpawn_Air[45] = <<-1000.0000, 5000.0000, 384.3914>>
//	SafeSpawn_Air[46] = <<-1000.0000, 6000.0000, 197.6230>>
//	SafeSpawn_Air[47] = <<-1000.0000, 7000.0000, 200.0000>>
//	SafeSpawn_Air[48] = <<0.0000, -4000.0000, 200.0000>>
//	SafeSpawn_Air[49] = <<0.0000, -3000.0000, 200.0000>>
//	SafeSpawn_Air[50] = <<0.0000, -2000.0000, 200.0000>>
//	SafeSpawn_Air[51] = <<0.0000, -1000.0000, 298.6662>>
//	SafeSpawn_Air[52] = <<0.0000, 0.0000, 270.1366>>
//	SafeSpawn_Air[53] = <<0.0000, 1000.0000, 413.5376>>
//	SafeSpawn_Air[54] = <<0.0000, 2000.0000, 380.4768>>
//	SafeSpawn_Air[55] = <<0.0000, 3000.0000, 244.4835>>
//	SafeSpawn_Air[56] = <<0.0000, 4000.0000, 200.0000>>
//	SafeSpawn_Air[57] = <<0.0000, 5000.0000, 644.1526>>
//	SafeSpawn_Air[58] = <<0.0000, 6000.0000, 357.7188>>
//	SafeSpawn_Air[59] = <<0.0000, 7000.0000, 200.0000>>
//	SafeSpawn_Air[60] = <<1000.0000, -4000.0000, 200.0000>>
//	SafeSpawn_Air[61] = <<1000.0000, -3000.0000, 204.9018>>
//	SafeSpawn_Air[62] = <<1000.0000, -2000.0000, 200.0000>>
//	SafeSpawn_Air[63] = <<1000.0000, -1000.0000, 200.0000>>
//	SafeSpawn_Air[64] = <<1000.0000, 0.0000, 280.9127>>
//	SafeSpawn_Air[65] = <<1000.0000, 1000.0000, 436.3140>>
//	SafeSpawn_Air[66] = <<1000.0000, 2000.0000, 261.7036>>
//	SafeSpawn_Air[67] = <<1000.0000, 3000.0000, 200.0000>>
//	SafeSpawn_Air[68] = <<1000.0000, 4000.0000, 200.0000>>
//	SafeSpawn_Air[69] = <<1000.0000, 5000.0000, 506.7005>>
//	SafeSpawn_Air[70] = <<1000.0000, 6000.0000, 533.3429>>
//	SafeSpawn_Air[71] = <<1000.0000, 7000.0000, 200.0000>>
//	SafeSpawn_Air[72] = <<2000.0000, -4000.0000, 200.0000>>
//	SafeSpawn_Air[73] = <<2000.0000, -3000.0000, 200.0000>>
//	SafeSpawn_Air[74] = <<2000.0000, -2000.0000, 309.4255>>
//	SafeSpawn_Air[75] = <<2000.0000, -1000.0000, 283.5056>>
//	SafeSpawn_Air[76] = <<2000.0000, 0.0000, 396.6758>>
//	SafeSpawn_Air[77] = <<2000.0000, 1000.0000, 411.0481>>
//	SafeSpawn_Air[78] = <<2000.0000, 2000.0000, 267.6360>>
//	SafeSpawn_Air[79] = <<2000.0000, 3000.0000, 246.3242>>
//	SafeSpawn_Air[80] = <<2000.0000, 4000.0000, 200.0000>>
//	SafeSpawn_Air[81] = <<2000.0000, 5000.0000, 240.3083>>
//	SafeSpawn_Air[82] = <<2000.0000, 6000.0000, 285.8446>>
//	SafeSpawn_Air[83] = <<2000.0000, 7000.0000, 200.0000>>
//	SafeSpawn_Air[84] = <<3000.0000, -4000.0000, 200.0000>>
//	SafeSpawn_Air[85] = <<3000.0000, -3000.0000, 200.0000>>
//	SafeSpawn_Air[86] = <<3000.0000, -2000.0000, 200.0000>>
//	SafeSpawn_Air[87] = <<3000.0000, -1000.0000, 200.0000>>
//	SafeSpawn_Air[88] = <<3000.0000, 0.0000, 200.0000>>
//	SafeSpawn_Air[89] = <<3000.0000, 1000.0000, 200.0000>>
//	SafeSpawn_Air[90] = <<3000.0000, 2000.0000, 239.9523>>
//	SafeSpawn_Air[91] = <<3000.0000, 3000.0000, 286.8789>>
//	SafeSpawn_Air[92] = <<3000.0000, 4000.0000, 254.5939>>
//	SafeSpawn_Air[93] = <<3000.0000, 5000.0000, 246.4268>>
//	SafeSpawn_Air[94] = <<3000.0000, 6000.0000, 406.7502>>
//	SafeSpawn_Air[95] = <<3000.0000, 7000.0000, 200.0000>>
//	SafeSpawn_Air[96] = <<4000.0000, -4000.0000, 200.0000>>
//	SafeSpawn_Air[97] = <<4000.0000, -3000.0000, 200.0000>>
//	SafeSpawn_Air[98] = <<4000.0000, -2000.0000, 200.0000>>
//	SafeSpawn_Air[99] = <<4000.0000, -1000.0000, 200.0000>>
//	SafeSpawn_Air[100] = <<4000.0000, 0.0000, 200.0000>>
//	SafeSpawn_Air[101] = <<4000.0000, 1000.0000, 200.0000>>
//	SafeSpawn_Air[102] = <<4000.0000, 2000.0000, 200.0000>>
//	SafeSpawn_Air[103] = <<4000.0000, 3000.0000, 200.0000>>
//	SafeSpawn_Air[104] = <<4000.0000, 4000.0000, 200.0000>>
//	SafeSpawn_Air[105] = <<4000.0000, 5000.0000, 200.0000>>
//	SafeSpawn_Air[106] = <<4000.0000, 6000.0000, 200.0000>>
//	SafeSpawn_Air[107] = <<4000.0000, 7000.0000, 200.0000>>
	
	
	// **********************************
	// 		Hospital spawn points
	// **********************************
	SpawnAreas_Hospitals[0].vCoords1 = <<-474.367, -323.441, 34.365>>
	SpawnAreas_Hospitals[0].fFloat = 61.8
	SpawnAreas_Hospitals[0].iShape = SPAWN_AREA_SHAPE_CIRCLE
	SpawnAreas_Hospitals[0].bIsActive = TRUE
	
	SpawnAreas_Hospitals[1].vCoords1 = <<290.618, -1474.3, 12.614  >>
	SpawnAreas_Hospitals[1].vCoords2 = <<392.818, -1366.158, 52.6  >>
	SpawnAreas_Hospitals[1].fFloat = 120.6
	SpawnAreas_Hospitals[1].iShape = SPAWN_AREA_SHAPE_ANGLED
	SpawnAreas_Hospitals[1].bIsActive = TRUE
	
	SpawnAreas_Hospitals[2].vCoords1 = <<353.208, -613.028, 25.2  >>
	SpawnAreas_Hospitals[2].vCoords2 = <<386.908, -569.353, 31.7  >>
	SpawnAreas_Hospitals[2].fFloat = 28.675
	SpawnAreas_Hospitals[2].iShape = SPAWN_AREA_SHAPE_ANGLED
	SpawnAreas_Hospitals[2].bIsActive = TRUE
	
	SpawnAreas_Hospitals[3].vCoords1 = <<1809.1, 3667.662, 14.281  >>
	SpawnAreas_Hospitals[3].vCoords2 = <<1867.8, 3700.362, 54.281  >>
	SpawnAreas_Hospitals[3].fFloat = 32.95
	SpawnAreas_Hospitals[3].iShape = SPAWN_AREA_SHAPE_ANGLED
	SpawnAreas_Hospitals[3].bIsActive = TRUE
	
	SpawnAreas_Hospitals[4].vCoords1 = <<-256.248, 6307.925, 28.726  >>
	SpawnAreas_Hospitals[4].vCoords2 = <<-239.02, 6325.53, 52.42  >>
	SpawnAreas_Hospitals[4].fFloat = 25.67
	SpawnAreas_Hospitals[4].iShape = SPAWN_AREA_SHAPE_ANGLED
	SpawnAreas_Hospitals[4].bIsActive = TRUE
	
	// **********************************
	// 		Police station spawn points
	// **********************************
	SpawnAreas_PoliceStation[0].vCoords1 = <<479.6391, -976.6794, 26.9839>>
	SpawnAreas_PoliceStation[0].fFloat = 20.0
	SpawnAreas_PoliceStation[0].fHeading = 331.7253
	SpawnAreas_PoliceStation[0].iShape = SPAWN_AREA_SHAPE_CIRCLE
	SpawnAreas_PoliceStation[0].bIsActive = TRUE
	SpawnAreas_PoliceStation[0].bConsiderCentrePointAsValid = TRUE
	
	SpawnAreas_PoliceStation[1].vCoords1 = <<639.1819, 1.7650, 81.7865 >>
	SpawnAreas_PoliceStation[1].fFloat = 20.0
	SpawnAreas_PoliceStation[1].fHeading = 238.0365
	SpawnAreas_PoliceStation[1].iShape = SPAWN_AREA_SHAPE_CIRCLE
	SpawnAreas_PoliceStation[1].bIsActive = TRUE
	SpawnAreas_PoliceStation[1].bConsiderCentrePointAsValid = TRUE
	
	SpawnAreas_PoliceStation[2].vCoords1 = << -440.7429, 6019.8892, 30.4903 >>
	SpawnAreas_PoliceStation[2].fFloat = 20.0
	SpawnAreas_PoliceStation[2].fHeading = 314.9315
	SpawnAreas_PoliceStation[2].iShape = SPAWN_AREA_SHAPE_CIRCLE
	SpawnAreas_PoliceStation[2].bIsActive = TRUE
	SpawnAreas_PoliceStation[2].bConsiderCentrePointAsValid = TRUE
	
	SpawnAreas_PoliceStation[3].vCoords1 = << 1856.3516, 3682.0608, 33.2672  >>
	SpawnAreas_PoliceStation[3].fFloat = 20.0
	SpawnAreas_PoliceStation[3].fHeading = 210.2006
	SpawnAreas_PoliceStation[3].iShape = SPAWN_AREA_SHAPE_CIRCLE
	SpawnAreas_PoliceStation[3].bIsActive = TRUE
	SpawnAreas_PoliceStation[3].bConsiderCentrePointAsValid = TRUE
	
	SpawnAreas_PoliceStation[4].vCoords1 = << -560.7550, -133.9789, 37.0586  >>
	SpawnAreas_PoliceStation[4].fFloat = 20.0
	SpawnAreas_PoliceStation[4].fHeading = 210.6082
	SpawnAreas_PoliceStation[4].iShape = SPAWN_AREA_SHAPE_CIRCLE
	SpawnAreas_PoliceStation[4].bIsActive = TRUE
	SpawnAreas_PoliceStation[4].bConsiderCentrePointAsValid = TRUE
	
	SpawnAreas_PoliceStation[5].vCoords1 = << 360.8818, -1581.6075, 28.2931  >>
	SpawnAreas_PoliceStation[5].fFloat = 20.0
	SpawnAreas_PoliceStation[5].fHeading = 23.6148
	SpawnAreas_PoliceStation[5].iShape = SPAWN_AREA_SHAPE_CIRCLE
	SpawnAreas_PoliceStation[5].bIsActive = TRUE
	SpawnAreas_PoliceStation[5].bConsiderCentrePointAsValid = TRUE
	
	SpawnAreas_PoliceStation[6].vCoords1 = << -1093.8900, -807.0834, 18.2612   >>
	SpawnAreas_PoliceStation[6].fFloat = 20.0
	SpawnAreas_PoliceStation[6].fHeading = 42.2400
	SpawnAreas_PoliceStation[6].iShape = SPAWN_AREA_SHAPE_CIRCLE
	SpawnAreas_PoliceStation[6].bIsActive = TRUE
	SpawnAreas_PoliceStation[6].bConsiderCentrePointAsValid = TRUE
	
	// **********************************
	// 		Homeless hotels
	// **********************************
	SpawnAreas_Hotels[0].vCoords1 = <<270.437, -1125.987, 27.419>>
	SpawnAreas_Hotels[0].vCoords2 = <<297.887, -1063.312, 35.319>>
	SpawnAreas_Hotels[0].iShape = SPAWN_AREA_SHAPE_BOX
	SpawnAreas_Hotels[0].bIsActive = TRUE
	
	SpawnAreas_Hotels[1].vCoords1 = <<316.413, -238.528, 52.236>>
	SpawnAreas_Hotels[1].vCoords2 = <<331.688, -199.528, 60.061>>
	SpawnAreas_Hotels[1].fFloat = 36.5
	SpawnAreas_Hotels[1].iShape = SPAWN_AREA_SHAPE_ANGLED
	SpawnAreas_Hotels[1].bIsActive = TRUE

	SpawnAreas_Hotels[2].vCoords1 = <<-1503.659, -680.612, 26.482>>
	SpawnAreas_Hotels[2].vCoords2 = <<-1454.434, -644.362, 35.307>>
	SpawnAreas_Hotels[2].fFloat = 31.925
	SpawnAreas_Hotels[2].iShape = SPAWN_AREA_SHAPE_ANGLED
	SpawnAreas_Hotels[2].bIsActive = TRUE
	
	SpawnAreas_Hotels[3].vCoords1 = <<403.585, 141.906, 98.166>>
	SpawnAreas_Hotels[3].vCoords2 = <<434.835, 228.306, 108.141>>
	SpawnAreas_Hotels[3].fFloat = 58.6
	SpawnAreas_Hotels[3].iShape = SPAWN_AREA_SHAPE_ANGLED
	SpawnAreas_Hotels[3].bIsActive = TRUE
	
	// **********************************
	// global exclusion areas
	// **********************************
	// prison
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_PRISON].ExclusionArea.vCoords1 = <<1695.538, 2581.561, 45.844>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_PRISON].ExclusionArea.fFloat = 201.8
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_PRISON].ExclusionArea.iShape = SPAWN_AREA_SHAPE_CIRCLE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_PRISON].ExclusionArea.bIsActive = TRUE
	
	// army base
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE].ExclusionArea.vCoords1 = <<-2796.22, 3481.990, 132.149>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE].ExclusionArea.vCoords2 = <<-1703.458, 2858.221, 21.448>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE].ExclusionArea.fFloat = 693.4
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE].ExclusionArea.iShape = SPAWN_AREA_SHAPE_ANGLED
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE].ExclusionArea.bIsActive = TRUE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE].bUseSpecificRespawnArea = TRUE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE].vSpecificRespawnArea = <<-2591.0969, 3447.2192, 13.8465>>
	
	// army base - exit
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE_EXIT].ExclusionArea.vCoords1 = <<-1707.024, 2875.322, 9.22>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE_EXIT].ExclusionArea.vCoords2 = <<-1577.674, 2778.872, 49.22>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE_EXIT].ExclusionArea.fFloat = 131.875
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE_EXIT].ExclusionArea.iShape = SPAWN_AREA_SHAPE_ANGLED
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE_EXIT].ExclusionArea.bIsActive = TRUE	
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE_EXIT].bUseSpecificRespawnArea = TRUE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE_EXIT].vSpecificRespawnArea = <<-1270.5826, 2540.5195, 17.5553>>
	
	// airport - part 1
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_AIRPORT_1].ExclusionArea.vCoords1 = <<-1159.988, -2740.949, 0.0>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_AIRPORT_1].ExclusionArea.vCoords2 = <<-1601.0, -3508.0, 100.0>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_AIRPORT_1].ExclusionArea.fFloat = 1370.0
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_AIRPORT_1].ExclusionArea.iShape = SPAWN_AREA_SHAPE_ANGLED
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_AIRPORT_1].ExclusionArea.bIsActive = TRUE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_AIRPORT_1].bUseSpecificRespawnArea = TRUE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_AIRPORT_1].vSpecificRespawnArea = <<-1019.5, -2699.1, 14.1>> // in front of terminal building
	
	// airport - part 2
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_AIRPORT_2].ExclusionArea.vCoords1 = <<-1399.175, -2615.2, 0.0>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_AIRPORT_2].ExclusionArea.vCoords2 = <<-1139.989, -2169.3, 100.0>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_AIRPORT_2].ExclusionArea.fFloat = 468.0
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_AIRPORT_2].ExclusionArea.iShape = SPAWN_AREA_SHAPE_ANGLED
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_AIRPORT_2].ExclusionArea.bIsActive = TRUE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_AIRPORT_2].bUseSpecificRespawnArea = TRUE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_AIRPORT_2].vSpecificRespawnArea = <<-1061.1, -2552.8, 13.8>>	
	
	// prologue map area
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_PROLOGUE].ExclusionArea.vCoords1 = <<1552.523, -5461.145, -10.0>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_PROLOGUE].ExclusionArea.vCoords2 = <<10200.00, -6599.993, 200.0>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_PROLOGUE].ExclusionArea.fFloat = 3499.965
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_PROLOGUE].ExclusionArea.iShape = SPAWN_AREA_SHAPE_ANGLED
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_PROLOGUE].ExclusionArea.bIsActive = TRUE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_PROLOGUE].bUseSpecificRespawnArea = TRUE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_PROLOGUE].vSpecificRespawnArea = <<1271.2, -3091.5, 5.9>>	
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_PROLOGUE].bAlwaysConsider = TRUE
	
	// underneath the Rockford Plaza
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ROCKFORD_PLAZA].ExclusionArea.vCoords1 = <<-174.6, -93.15, 109.1>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ROCKFORD_PLAZA].ExclusionArea.vCoords2 = <<-212.5, -258.5, 42.0>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ROCKFORD_PLAZA].ExclusionArea.fFloat = 144.3
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ROCKFORD_PLAZA].ExclusionArea.iShape = SPAWN_AREA_SHAPE_ANGLED
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ROCKFORD_PLAZA].ExclusionArea.bIsActive = TRUE
	
	// tunnel under army base 
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE_3].ExclusionArea.vCoords1 = <<-2567.3, 3392.1, 11.475>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE_3].ExclusionArea.vCoords2 = <<-2604.5, 2958.5, 24.975>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE_3].ExclusionArea.fFloat = 35.0
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE_3].ExclusionArea.iShape = SPAWN_AREA_SHAPE_ANGLED
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE_3].ExclusionArea.bIsActive = TRUE	
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE_3].bUseSpecificRespawnArea = TRUE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE_3].vSpecificRespawnArea = <<-2550.8196, 2853.9685, 2.3142>>	
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARMY_BASE_3].bAlwaysConsider = TRUE
	
	// arena
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA].ExclusionArea.vCoords1 = <<2797.0, -4112.0, -200.0>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA].ExclusionArea.vCoords2 = <<2797.0, -3568.0, 300.0>> 
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA].ExclusionArea.fFloat = 400.0
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA].ExclusionArea.iShape = SPAWN_AREA_SHAPE_ANGLED	
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA].bUseSpecificRespawnArea = TRUE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA].vSpecificRespawnArea = <<ARENA_X, ARENA_Y, ARENA_Z>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA].bAlwaysConsider = FALSE	
	
	
	// arena - under chilliad
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA_UNDER_CHILLIAD].ExclusionArea.vCoords1 = <<731.0, 5189.0, 90.0>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA_UNDER_CHILLIAD].ExclusionArea.vCoords2 = <<26.0, 5177.0, -195.0>> 
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA_UNDER_CHILLIAD].ExclusionArea.fFloat = 400.0
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA_UNDER_CHILLIAD].ExclusionArea.iShape = SPAWN_AREA_SHAPE_ANGLED	
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA_UNDER_CHILLIAD].bUseSpecificRespawnArea = TRUE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA_UNDER_CHILLIAD].vSpecificRespawnArea = <<ARENA_X, ARENA_Y, ARENA_Z>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ARENA_UNDER_CHILLIAD].bAlwaysConsider = FALSE
	
	
	// altruist camp
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ALTURIST_CAMP].ExclusionArea.vCoords1 = <<-1110.899, 4920.258, 218.332>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ALTURIST_CAMP].ExclusionArea.fFloat = 90.0
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ALTURIST_CAMP].ExclusionArea.iShape = SPAWN_AREA_SHAPE_CIRCLE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_ALTURIST_CAMP].ExclusionArea.bIsActive = TRUE	
	
	// casino
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_CASINO].ExclusionArea.vCoords1 = <<928.475, -4.250, 72.207>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_CASINO].ExclusionArea.vCoords2 = <<983.675, 82.425, 92.132>> 
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_CASINO].ExclusionArea.fFloat = 65.525
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_CASINO].ExclusionArea.iShape = SPAWN_AREA_SHAPE_ANGLED	
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_CASINO].bUseSpecificRespawnArea = TRUE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_CASINO].vSpecificRespawnArea = <<921.7635, 78.7917, 77.9975>>
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_CASINO].bAlwaysConsider = TRUE
	GlobalExclusionArea[GLOBAL_EXCLUSION_AREA_CASINO].ExclusionArea.bIsActive = TRUE	
	
	CPRINTLN(DEBUG_BLIP, "[player_blips] INIT_MP_SPAWN_POINTS - init global exclusion area.")
	
	
	// ******************************************************************************
	//			Forbidden Areas	
	// ******************************************************************************
	PROBLEM_AREA TempProblemArea
	TempProblemArea.vCoords1 = <<-31.997, -1085.819, 24.965>>
	TempProblemArea.vCoords2 = <<-26.447, -1088.017, 27.565>>
	TempProblemArea.fFloat = 1.725
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<-60.256, -1094.808, 25.071>>
	TempProblemArea.vCoords2 = <<-59.531, -1092.533, 27.421>>
	TempProblemArea.fFloat = 1.725	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// simeons garage
	TempProblemArea.vCoords1 = <<1197.155, -3117.327, 3.62>>
	TempProblemArea.vCoords2 = <<1214.405, -3117.377, 8.045>>
	TempProblemArea.fFloat = 14.125	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// highway
	TempProblemArea.vCoords1 = <<-290.0, -1192.0, 62.35>>
	TempProblemArea.vCoords2 = <<1181.9, -1169.0, 35.525>>
	TempProblemArea.fFloat = 115.525	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<1173.15, -1199.525, 43.6>>
	TempProblemArea.vCoords2 = <<1554.0, -993.0, 70.475>>
	TempProblemArea.fFloat = 80.225	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// car park aboammunation
	TempProblemArea.vCoords1 = <<25.3, -1042.5, 29.375>>
	TempProblemArea.vCoords2 = <<1.9, -1110.5, 42.872>>
	TempProblemArea.fFloat = 65.7	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// stairs at meal centre
	TempProblemArea.vCoords1 = <<1854.997, 3679.577, 32.2>>
	TempProblemArea.vCoords2 = <<1858.322, 3681.552, 34.751>>
	TempProblemArea.fFloat = 1.6	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// dodgy bridge
	TempProblemArea.vCoords1 = <<-2121.28, 2321.252, 27.171>>
	TempProblemArea.vCoords2 = <<-2120.18, 2296.45, 40.09>>
	TempProblemArea.fFloat = 5.975
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// cliff
	TempProblemArea.vCoords1 = <<3383.542, 5041.074, -9.093>>
	TempProblemArea.vCoords2 = <<3619.964, 4719.953, 22.232>>
	TempProblemArea.fFloat = 99.625
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// under road
	TempProblemArea.vCoords1 = <<634.507, -289.376, 31.881>>
	TempProblemArea.vCoords2 = <<652.632, -298.526, 45.181>>
	TempProblemArea.fFloat = 24.25	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// garage
	TempProblemArea.vCoords1 = <<133.917, -1054.88, 27.0>>
	TempProblemArea.vCoords2 = <<137.092, -1046.455, 31.601>>
	TempProblemArea.fFloat = 7.2		
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// gated area
	TempProblemArea.vCoords1 = <<426.626, -1176.703, 27.978>>
	TempProblemArea.vCoords2 = <<447.376, -1177.928, 32.303>>
	TempProblemArea.fFloat = 15.75
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// police statiarage
	TempProblemArea.vCoords1 = <<427.496, -997.416, 24.36>>
	TempProblemArea.vCoords2 = <<439.946, -997.941, 26.735>>
	TempProblemArea.fFloat = 9.275
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// franklin gar
	TempProblemArea.vCoords1 = <<18.292, 546.515, 174.902>>
	TempProblemArea.vCoords2 = <<26.842, 541.790, 177.777>>
	TempProblemArea.fFloat = 8.1
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// caravan
	TempProblemArea.vCoords1 = <<88.643, 3741.656, 38.297>>
	TempProblemArea.vCoords2 = <<94.768, 3759.156, 42.947>>
	TempProblemArea.fFloat = 5.35	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// caravan
	TempProblemArea.vCoords1 = <<33.829, 3675.851, 38.017>>
	TempProblemArea.vCoords2 = <<25.179, 3659.276, 43.042>>
	TempProblemArea.fFloat = 5.8	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// underground park
	TempProblemArea.vCoords1 = <<-1441.251, -532.584, 29.551>>
	TempProblemArea.vCoords2 = <<-1459.251, -505.384, 33.276>>
	TempProblemArea.fFloat = 21.025	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// car park
	TempProblemArea.vCoords1 = <<-1579.51, -459.658, 35.711>>
	TempProblemArea.vCoords2 = <<-1604.735, -436.083, 39.536>>
	TempProblemArea.fFloat = 16.95
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// garage
	TempProblemArea.vCoords1 = <<-1363.262, -760.064, 21.053>>
	TempProblemArea.vCoords2 = <<-1354.987, -753.689, 24.128>>
	TempProblemArea.fFloat = 7.57
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// bin area
	TempProblemArea.vCoords1 = <<-1170.162, -1229.539, 5.342>>
	TempProblemArea.vCoords2 = <<-1162.462, -1226.814, 8.292>>
	TempProblemArea.fFloat = 6.3
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// doorway
	TempProblemArea.vCoords1 = <<-811.85, -979.962, 13.032>>
	TempProblemArea.vCoords2 = <<-808.950, -977.962, 16.282>>
	TempProblemArea.fFloat = 4.175
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// collision leall
	TempProblemArea.vCoords1 = <<-970.391, -1011.892, 0.604>>
	TempProblemArea.vCoords2 = <<-984.616, -987.467, 3.154>>
	TempProblemArea.fFloat = 7.650	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// garage
	TempProblemArea.vCoords1 = <<-96.26, 362.021, 110.571>>
	TempProblemArea.vCoords2 = <<-64.635, 347.146, 116.946>>
	TempProblemArea.fFloat = 36.075
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// tunnel
	TempProblemArea.vCoords1 = <<784.0, -1968.675, 9.437>>
	TempProblemArea.vCoords2 = <<743.0, -2436.01, 28.062>>
	TempProblemArea.fFloat = 22.7	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<828.87, -1766.0, 18.05>>
	TempProblemArea.vCoords2 = <<787.068, -1972.914, 27.898>>
	TempProblemArea.fFloat = 22.8	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// high end gar1
	TempProblemArea.vCoords1 = <<-791.475, 317.162, 84.249>>
	TempProblemArea.vCoords2 = <<-800.85, 317.088, 88.124>>
	TempProblemArea.fFloat = 6.610	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// around appart 1st floor
	TempProblemArea.vCoords1 = <<-635.176, 50.102, 52.141>>
	TempProblemArea.vCoords2 = <<-564.376, 50.027, 58.716>>
	TempProblemArea.fFloat = 44.986	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// bank in grannara
	TempProblemArea.vCoords1 = <<1172.091, 2703.759, 36.544>>
	TempProblemArea.vCoords2 = <<1178.516, 2703.884, 39.169>>
	TempProblemArea.fFloat = 1.875	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// locked garag
	TempProblemArea.vCoords1 = <<779.229, -1867.332, 27.764>>
	TempProblemArea.vCoords2 = <<754.604, -1865.409, 33.889>>
	TempProblemArea.fFloat = 5.725	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<2509.819, -321.995, 89.892>>
	TempProblemArea.vCoords2 = <<2508.169, -343.095, 96.442>>
	TempProblemArea.fFloat = 3.525	
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// foundry
	TempProblemArea.vCoords1 = <<1067.175, -1986.425, 14.050>>
	TempProblemArea.vCoords2 = <<1119.100, -2022.875, 48.033>>
	TempProblemArea.fFloat = 36.775
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// floyds appart
	TempProblemArea.vCoords1 = <<-1160.4, -1524.0, 12.7>>
	TempProblemArea.vCoords2 = <<-1145.3, -1513.0, 9.0>>
	TempProblemArea.fFloat = 9.5
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// michaels hou
	// floyds appart
	TempProblemArea.vCoords1 = <<-814.647, 174.719, 74.719>>
	TempProblemArea.vCoords2 = <<-816.872, 179.819, 77.741>>
	TempProblemArea.fFloat = 2.125
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// fence outsidrip club
	TempProblemArea.vCoords1 = <<202.9, -1234.6, 29.0>>
	TempProblemArea.vCoords2 = <<201.7, -1221.1, 30.0>>
	TempProblemArea.fFloat = 3.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)

	// underground car park
	TempProblemArea.vCoords1 = <<25.157, -700.75, 28.787>>
	TempProblemArea.vCoords2 = <<-77.568, -664.275, 33.337>>
	TempProblemArea.fFloat = 69.55
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// xmas tree in perishing square
	TempProblemArea.vCoords1 = <<230.949, -886.483, 27.492>>
	TempProblemArea.vCoords2 = <<244.274, -844.808, 31.492>>
	TempProblemArea.fFloat = 17.30
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)		
	
	// yacht interior upper
	TempProblemArea.vCoords1 = <<-2055.725, -1027.525, 5.925>>
	TempProblemArea.vCoords2 = <<-2076.425, -1020.875, 9.0>>
	TempProblemArea.fFloat = 11.575
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)	
	
	// yacht interior lower
	TempProblemArea.vCoords1 = <<-2037.3, -1033.6, 4.0>>
	TempProblemArea.vCoords2 = <<-2076.4, -1020.7, 6.3>>
	TempProblemArea.fFloat = 11.575
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)		
	
	// FIB interior
	TempProblemArea.vCoords1 = <<118.0, -765.0, 43.8>>
	TempProblemArea.vCoords2 = <<106.6, -740.0, 47.9>>
	TempProblemArea.fFloat = 14.175
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)	
	
	// trevor torture room
	TempProblemArea.vCoords1 = <<134.1, -2204.4, 5.75>>
	TempProblemArea.vCoords2 = <<135.0, -2200.4, 8.3>>
	TempProblemArea.fFloat = 1.55
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)	
	
	// fleeca bank
	TempProblemArea.vCoords1 = <<1178.5, 2704.0, 36.5>>
	TempProblemArea.vCoords2 = <<1170.8, 2703.8, 39.4>>
	TempProblemArea.fFloat = 2.175
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)		
	
	// Lesters Factory/Warehous
	TempProblemArea.vCoords1 = <<714.3, -978.1, 23.5>>
	TempProblemArea.vCoords2 = <<714.3, -956.1, 37.0>>
	TempProblemArea.fFloat = 23.9
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Tequi-La-La:
	TempProblemArea.vCoords1 = <<-584.9, 288.0, 93.6>>
	TempProblemArea.vCoords2 = <<-549.3, 284.8, 68.3>>
	TempProblemArea.fFloat = 22.1
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Space Docker Garage:
	TempProblemArea.vCoords1 = <<2328.2, 2568.9, 47.0>>
	TempProblemArea.vCoords2 = <<2331.7, 2576.4, 45.4>>
	TempProblemArea.fFloat = 6.65
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// meth lab
	TempProblemArea.vCoords1 = <<1395.9, 3599.4, 43.3>>
	TempProblemArea.vCoords2 = <<1388.4, 3620.4, 31.0>>
	TempProblemArea.fFloat = 16.3
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// tunnels under road that havn't been marked as interior 2433387
	TempProblemArea.vCoords1 = <<524.0, -901.0, 23.1>>
	TempProblemArea.vCoords2 = <<537.0, -666.0, 12.0>>
	TempProblemArea.fFloat = 25.75
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// bit of nav mesh under collision. 2466232
	TempProblemArea.vCoords1 = <<3359.5, 5166.7, 12.7>>
	TempProblemArea.vCoords2 = <<3342.1, 5169.2, 22.0>>
	TempProblemArea.fFloat = 12.625
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// nav mesh on wrong side of car park door 2521594
	TempProblemArea.vCoords1 = <<-817.1, -443.4, 35.0>>
	TempProblemArea.vCoords2 = <<-823.5, -431.1, 40.0>>
	TempProblemArea.fFloat = 4.95
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// nav mesh on wrong side of bank door. 2985639
	TempProblemArea.vCoords1 = <<150.185, -1038.197, 26.791>>
	TempProblemArea.vCoords2 = <<150.810, -1036.472, 30.340>>
	TempProblemArea.fFloat = 5.7
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// inside a locked room. 2999681
	TempProblemArea.vCoords1 = <<-927.361, -1415.731, 6.750>>
	TempProblemArea.vCoords2 = <<-922.536, -1414.181, 8.750>>
	TempProblemArea.fFloat = 8.825
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// dockside 3009684
	TempProblemArea.vCoords1 = <<-512.825, -2339.0, -2.838>>
	TempProblemArea.vCoords2 = <<-768.0, -2776.0, 10.175>>
	TempProblemArea.fFloat = 126.975
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// BUNKERS
	INT iBunker
	FOR iBunker = ENUM_TO_INT(SIMPLE_INTERIOR_BUNKER_1) TO ENUM_TO_INT(SIMPLE_INTERIOR_BUNKER_12)
		PRINTLN("[spawning] Adding forbidden spawning for BUNKER ", iBunker)
		GET_BUNKER_SPAWN_EXCLUSION_AREA(INT_TO_ENUM(SIMPLE_INTERIORS, iBunker), TempProblemArea.vCoords1, TempProblemArea.vCoords2, TempProblemArea.fFloat)
		ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	ENDFOR	
	
	// LSIA Sea Wall
	TempProblemArea.vCoords1 = <<-807.364258, -2822.437500, 34.944481>>
	TempProblemArea.vCoords2 =  <<-725.195313, -2684.619873, -2.398621>> 
	TempProblemArea.fFloat = 133.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Grand Senora Desert Facility
	TempProblemArea.vCoords1 = <<1274.500977, 2863.358643, 0.0>>
	TempProblemArea.vCoords2 = <<1301.332642, 2829.500488, 55.913303>> 
	TempProblemArea.fFloat = 50.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Route 68 Facility
	TempProblemArea.vCoords1 = <<28.640234, 2600.122559, 0.0>>
	TempProblemArea.vCoords2 = <<12.257702, 2624.341797, 93.917679>> 
	TempProblemArea.fFloat = 50.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Sandy Shores Facility
	TempProblemArea.vCoords1 = <<2758.898193, 3929.112305, 0.0>>
	TempProblemArea.vCoords2 = <<2778.342041, 3909.173096, 53.780231>> 
	TempProblemArea.fFloat = 50.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Mount Gordo Facility
	TempProblemArea.vCoords1 = <<3401.340820, 5473.823730, 8.426765>>
	TempProblemArea.vCoords2 = <<3420.529297, 5542.263184, 34.872040>> 
	TempProblemArea.fFloat = 70.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Paleto Bay Facility
	TempProblemArea.vCoords1 = <<-0.830860, 6818.450195, 0.0>>
	TempProblemArea.vCoords2 = <<8.693195, 6844.146484, 23.461246>> 
	TempProblemArea.fFloat = 50.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Lago Zancudo Facility
	TempProblemArea.vCoords1 = <<-2245.172852, 2415.077393, 0.0>>
	TempProblemArea.vCoords2 = <<-2217.466309, 2417.885742, 20.253265>> 
	TempProblemArea.fFloat = 50.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Zancudo River Facility
	TempProblemArea.vCoords1 = <<15.239210, 3321.969727, 13.093029>>
	TempProblemArea.vCoords2 = <<-29.732878, 3331.739258, 46.038654>> 
	TempProblemArea.fFloat = 75.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Ron Alternates Wind Farm Facility
	TempProblemArea.vCoords1 = <<2081.715088, 1738.366821, 0.0>>
	TempProblemArea.vCoords2 = <<2063.131104, 1756.570557, 112.162476>> 
	TempProblemArea.fFloat = 50.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Land Act Resevoir Facility
	TempProblemArea.vCoords1 = <<1863.880981, 292.702820, 0.0>>
	TempProblemArea.vCoords2 = <<1887.091919, 277.987274, 172.331543>> 
	TempProblemArea.fFloat = 50.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Land Act Reservoir Bridge
	TempProblemArea.vCoords1 = <<1845.747925, 253.874710, 159.435043>>
	TempProblemArea.vCoords2 = <<1859.767822, 245.087509, 165.945496>> 
	TempProblemArea.fFloat = 32.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Server Farm Interior
	TempProblemArea.vCoords1 = <<1967.044922, 2920.494629, -90.478401>>
	TempProblemArea.vCoords2 = <<2370.096680, 2922.292725, -60.800095>> 
	TempProblemArea.fFloat = 70.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Server Farm Exterior
	TempProblemArea.vCoords1 = <<2499.270264, -312.768005, 89.492859>>
	TempProblemArea.vCoords2 = <<2499.086914, -461.355865, 116.992897>> 
	TempProblemArea.fFloat = 90.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Soyler Textile Roof
	TempProblemArea.vCoords1 = <<753.857178, -1334.377441, 25.234772>>
	TempProblemArea.vCoords2 = <<755.214783, -1261.515991, 53.831936>> 
	TempProblemArea.fFloat = 55.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Doppler Cinema
	TempProblemArea.vCoords1 = <<334.350677, 172.451431, 102.301331>>
	TempProblemArea.vCoords2 = <<365.606812, 256.943665, 142.500641>> 
	TempProblemArea.fFloat = 44.75
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// Silo Exterior
	TempProblemArea.vCoords1 = <<589.948853, 5529.015137, 675.896606>>
	TempProblemArea.vCoords2 = <<607.050720, 5581.314941, 752.567993>> 
	TempProblemArea.fFloat = 70.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// under a skip
	TempProblemArea.vCoords1 = <<2356.483, 3142.057, 46.334>>
	TempProblemArea.vCoords2 = <<2361.358, 3141.032, 49.209>> 
	TempProblemArea.fFloat = 4.9
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	// url:bugstar:4632381 - After being killed outside the LSIA Warehouse, several players spawned on the roof.
	TempProblemArea.vCoords1 = <<137.6, -3325.8, 13.0>>
	TempProblemArea.vCoords2 = <<137.2, -3001.0, 22.35>> 
	TempProblemArea.fFloat = 46.575
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)	
	
	// url:bugstar:5568269 - RC - Paleto Bae - Player spawned inside a building (cs1_02_050_dec) and fell through the world after transitioning from an RC race to freemode.
	TempProblemArea.vCoords1 = <<-176.432, 6164.366, 28.9>>
	TempProblemArea.vCoords2 = <<-174.432, 6166.366, 33.35>> 
	TempProblemArea.fFloat = 7.0
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)	
	
	// url:bugstar:5726980 - Spawned into the entrance section of the old casino when booting into game with "last location" set as my spawn point
	TempProblemArea.vCoords1 = <<928.475, -4.250, 73.207>>
	TempProblemArea.vCoords2 = <<983.675, 82.425, 84.132>> 
	TempProblemArea.fFloat = 65.525
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)	
	
	// url:bugstar:5783201 - Casino Property - Player spawned in a bush near the casino after swapping session
	TempProblemArea.vCoords1 = <<974.625, -13.37, 79.52>>
	TempProblemArea.vCoords2 = <<973.5, -28.475, 89.0>> 
	TempProblemArea.fFloat = 9.5
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)		
	
	//url:bugstar:6519322 - After completing the Chicken Factory Business Battle then rebooting the game, player spawned clipping through the factory doors which caused them to fall through the world/map. 
	TempProblemArea.vCoords1 = <<-67.976158,6267.854492,28.858084>>
	TempProblemArea.vCoords2 = <<-73.761810,6263.857422,33.352608>> 
	TempProblemArea.fFloat = 1.250000
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)	
	
	//url:bugstar:6712470 - Freemode - Player spawned in the ground outside the Casino Nightclub at h4_yacht_casino_newpavement (in 'h4_yacht.rpf') at 951.980,80.934,78.954  
	TempProblemArea.vCoords1 = <<966.949585,78.732254,77.589447>>
	TempProblemArea.vCoords2 = <<940.487183,98.274742,82.829239>> 
	TempProblemArea.fFloat = 16.000000
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)	
	//same bug as above
	TempProblemArea.vCoords1 = <<947.693115,84.044594,76.768738>>
	TempProblemArea.vCoords2 = <<933.148010,59.356716,83.776649>> 
	TempProblemArea.fFloat = 8.000000
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)	
	
	//url:bugstar:6991722 - Prep - Signal Jammers - Local player spawned inside of an LS Customs interior after dying next to a Signal Jammer box at Grand Senora Desert Fleeca.
	TempProblemArea.vCoords1 = <<1171.042236,2640.392090,36.744259>>
	TempProblemArea.vCoords2 = <<1190.059326,2640.129150,43.089348>>
	TempProblemArea.fFloat = 10.500000
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)	
	
	//url:bugstar:7055759 - Spawning into Freemode - Player spawns inside a building and is able to fall through world / map after exiting GTA Online next to building and rebooting
	TempProblemArea.vCoords1 = <<-598.370361,-1632.846313,18.997440>>
	TempProblemArea.vCoords2 = <<-597.946167,-1628.230591,21.993917>> 
	TempProblemArea.fFloat = 11.250000
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)	
	
	//url:bugstar:7434172 - Gen9 - HSW - The local player spawned inside the garage after entering and exiting a lobby for a race while near the HSW Intro mission Garage.
	TempProblemArea.vCoords1 = <<509.699,172.176,97.010>>
	TempProblemArea.vCoords2 = <<525.099,167.251,10.475>> 
	TempProblemArea.fFloat = 10.475
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)		
	
	// url:bugstar:7490080 - Gen 9 - When leaving a job lobby, hosted by the local player from within Del Perro Heights Apartment building, player spawns inside the apartment foyer and is unable to leave. After 40 seconds the player is placed outside the foyer.
	TempProblemArea.vCoords1 =<<-1448.116699,-537.283752,33.494778>>
	TempProblemArea.vCoords2 =<<-1443.247559,-544.406006,38.492596>>
	TempProblemArea.fFloat = 12.750000
	ADD_TO_PROBLEM_AREAS_FORBIDDEN(TempProblemArea)
	
	
	// ******************************************************************************
	//			Problem road nodes (to ignore when using GetNearestCarNode) 
	// ******************************************************************************	
	TempProblemArea.vCoords1 = <<-183.7, 6556.825, 8.969>>
	TempProblemArea.vCoords2 = <<-200.675, 6537.7, 12.269>>
	TempProblemArea.fFloat = 3.7
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<-1109.162, -1548.997, 0.451>>
	TempProblemArea.vCoords2 = <<-1118.662, -1535.897, 6.101>>
	TempProblemArea.fFloat = 2.0 
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<-2297.870, 2804.494, -1.059>>
	TempProblemArea.vCoords2 = <<-2239.420, 2739.394, 3.591>>
	TempProblemArea.fFloat = 50.5
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<-1994.811, 2691.042, -4.788>>
	TempProblemArea.vCoords2 = <<-1866.836, 2630.817, 5.437>>
	TempProblemArea.fFloat = 40.775
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<-1755.23, 2679.410, -15.7>>
	TempProblemArea.vCoords2 = <<-1591.180, 2682.41, 4.307>>
	TempProblemArea.fFloat = 118.725
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<-2876.135, 2260.27, -10.505>>
	TempProblemArea.vCoords2 = <<-2788.196, 2360.95, 8.545>>
	TempProblemArea.fFloat = 21.25
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<-2512.695, 2431.329, -5.9>>
	TempProblemArea.vCoords2 = <<-2426.77, 2815.304, 11.5>>
	TempProblemArea.fFloat = 54.175
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<-57.737, -1071.5, 25.272>>
	TempProblemArea.vCoords2 = <<-6.087, -1089.027, 27.647>>
	TempProblemArea.fFloat = 21.0
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<-560.631, -1700.714, 13.267>>
	TempProblemArea.vCoords2 = <<-608.206, -1655.264, 27.292>>
	TempProblemArea.fFloat = 47.475	
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<-1031.521, 710.345, 148.998>>
	TempProblemArea.vCoords2 = <<-905.471, 693.670, 166.173>>
	TempProblemArea.fFloat = 43.5		
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<2461.541, 2842.711, 44.0>>
	TempProblemArea.vCoords2 = <<2353.741, 2977.411, 59.236>>
	TempProblemArea.fFloat = 58.075
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	// storm drains
	TempProblemArea.vCoords1 = <<809.616, -421.324, 20.169>>
	TempProblemArea.vCoords2 = <<877.991, -409.099, 35.544>>
	TempProblemArea.fFloat = 15.475	
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<585.236, -687.805, 7.682>>
	TempProblemArea.vCoords2 = <<701.661, -465.530, 23.132>>
	TempProblemArea.fFloat = 40.325	
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<592.894, -1300.412, 3.992>>
	TempProblemArea.vCoords2 = <<592.675, -1069.46, 16.192>>
	TempProblemArea.fFloat = 14.525		
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<634.418, -1423.885, 0.79>>
	TempProblemArea.vCoords2 = <<596.589, -1284.453, 15.94>>
	TempProblemArea.fFloat = 14.525		
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<656.688, -1919.981, 7.336>>
	TempProblemArea.vCoords2 = <<688.363, -1579.836, 16.311>>
	TempProblemArea.fFloat = 14.525		
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<-197.163, 4218.569, 39.728>>
	TempProblemArea.vCoords2 = <<-133.213, 4278.144, 54.078>>
	TempProblemArea.fFloat = 19.385
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<2679.666, 1641.739, 22.067>>
	TempProblemArea.vCoords2 = <<2651.966, 1642.189, 31.317>>
	TempProblemArea.fFloat = 5.850
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<-113.397, 858.127, 231.982>>
	TempProblemArea.vCoords2 = <<-64.197, 870.427, 243.757>>
	TempProblemArea.fFloat = 32.2
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<-2019.025, -409.184, 7.271>>
	TempProblemArea.vCoords2 = <<-1926.65, -486.284, 22.921>>
	TempProblemArea.fFloat = 8.075
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<2375.252686,3101.816406,46.187519>>
	TempProblemArea.vCoords2 = <<2387.111084,3124.555420,51.841202>>
	TempProblemArea.fFloat = 15.625000
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<1003.902710,-2200.691406,28.570148>>
	TempProblemArea.vCoords2 = <<999.093201,-2259.495605,33.634914>>
	TempProblemArea.fFloat = 9.937500
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	TempProblemArea.vCoords1 = <<-2145.822, 2689.984, -1.127>>
	TempProblemArea.vCoords2 = <<-2102.872, 2666.184, 8.523>>
	TempProblemArea.fFloat = 10.5
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	// nodes in and ad lake behind damn
	TempProblemArea.vCoords1 = <<1947.3, 151.8, 162.068>>
	TempProblemArea.vCoords2 = <<1985.0, 665.0, 148.0>>
	TempProblemArea.fFloat = 274.9
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	// awkward alleyw
	TempProblemArea.vCoords1 = <<-745.552, -276.506, 34.222>>
	TempProblemArea.vCoords2 = <<-675.477, -305.756, 37.948>>
	TempProblemArea.fFloat = 23.275
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	// mod shop exit
	TempProblemArea.vCoords1 = <<713.8, -1088.5, 27.25>>
	TempProblemArea.vCoords2 = <<738.7, -1088.6, 21.00>>
	TempProblemArea.fFloat = 3.7
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)

	// buildin site
	TempProblemArea.vCoords1 = <<-143.598, 6250.493, 29.639>>
	TempProblemArea.vCoords2 = <<-168.723, 6275.918, 37.864>>
	TempProblemArea.fFloat = 5.725
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)

	// tanker parked on road in military base
	TempProblemArea.vCoords1 = <<-2133.03, 3411.74, 29.475>>
	TempProblemArea.vCoords2 = <<-2131.98, 3395.965, 38.875>>
	TempProblemArea.fFloat = 4.575
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	// bridge by military base
	TempProblemArea.vCoords1 = <<-1547.2, 2752.5, 12.0>>
	TempProblemArea.vCoords2 = <<-1323.1, 2555.0, 20.0>>
	TempProblemArea.fFloat = 3.7
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	// dodgy road nodes - 2154258
	TempProblemArea.vCoords1 = <<-1407.572, -517.701, 23.503>>
	TempProblemArea.vCoords2 = <<-1384.797, -549.476, 37.153>>
	TempProblemArea.fFloat = 2.775
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	// inside building = 2319495
	TempProblemArea.vCoords1 = <<-52.675, 348.7, 115.4>>
	TempProblemArea.vCoords2 = <<-74.3, 358.4, 109.3>>
	TempProblemArea.fFloat = 9.6
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	// underneath docks - 2372383
	TempProblemArea.vCoords1 = <<476.65, -3050.97, 25.0>>
	TempProblemArea.vCoords2 = <<500.0, -3234.0, 0.0>>
	TempProblemArea.fFloat = 16.3
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	// beside container - 2458323
	TempProblemArea.vCoords1 = <<856.0, -2232.0, 25.3>>
	TempProblemArea.vCoords2 = <<857.7, -2209.0, 35.0>>
	TempProblemArea.fFloat = 16.3
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)	
	
	// wall along beach road
	TempProblemArea.vCoords1 = <<-1152.0, -1748.525, 0.0>>
	TempProblemArea.vCoords2 = <<-1295.0, -1544.3, 10.0>>
	TempProblemArea.fFloat = 125.3
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)	
	
	// bridge for 2642382
	TempProblemArea.vCoords1 = <<-53.790, 4562.256, 116.737>>
	TempProblemArea.vCoords2 = <<-115.965, 4601.031, 130.362>>
	TempProblemArea.fFloat = 13.475
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)		
	
	// alleyway for 2643202 (extremes are inside wall)
	TempProblemArea.vCoords1 = <<1061.4, -1954.6, 27.9>>
	TempProblemArea.vCoords2 = <<1061.8, -1890.0, 41.4>>
	TempProblemArea.fFloat = 19.3
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)		
	
	// road under pier - 2704770
	TempProblemArea.vCoords1 = <<-1673.050, -1003.050, 0.0>>
	TempProblemArea.vCoords2 = <<-1582.525, -1079.050, 10.0>>
	TempProblemArea.fFloat = 105.985
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)		
	
	// tunnel to storm drains - 2784557
	TempProblemArea.vCoords1 = <<623.0, -854.0, 10.0>>
	TempProblemArea.vCoords2 = <<769.0, -855.0, 32.0>>
	TempProblemArea.fFloat = 5.0
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)		
	
	// nodes going inside building 2804849
	TempProblemArea.vCoords1 = <<-159.455, -1305.585, 25.228>>
	TempProblemArea.vCoords2 = <<-111.9, -1304.2, 45.0>>
	TempProblemArea.fFloat = 10.925
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)			
	
	// down by beach. 2846193
	TempProblemArea.vCoords1 = <<877.8, -2628.02, 37.01>>
	TempProblemArea.vCoords2 = <<1283.0, -2698.7, -19.5>>
	TempProblemArea.fFloat = 100.0
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)	
	
	// path down to beach.(part2 of 2846193)
	TempProblemArea.vCoords1 = <<1174.0, -2575.0, 38.0>>
	TempProblemArea.vCoords2 = <<1185.75, -2667.3, -1.9>>
	TempProblemArea.fFloat = 26.55
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)	
	
	// empty garage 3291838
	TempProblemArea.vCoords1 = <<253.098, -1340.372, 28.995>>
	TempProblemArea.vCoords2 = <<258.723, -1333.947, 38.995>>
	TempProblemArea.fFloat = 4.675
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	// Block off gate near South Shambles St Garage 3648751
	TempProblemArea.vCoords1 = <<1055.315, -2373.923, 29.638>>
	TempProblemArea.vCoords2 = <<1054.469, -2384.526, 32.716>>
	TempProblemArea.fFloat = 7.75
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	// url:bugstar:3649547 - After exiting the MOC, APC spawned clipping into wall instead on the road
	TempProblemArea.vCoords1 = <<-37.7, -68.1, 52.9>>
	TempProblemArea.vCoords2 = <<-13.05, -78.0, 65.5>>
	TempProblemArea.fFloat = 7.6
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	// url:bugstar:4114313 - Block road nodes near Zancudo River Facility.
	TempProblemArea.vCoords1 = <<16.983559,3347.712891,36.112457>>
	TempProblemArea.vCoords2 = <<-3.551355,3323.385254,46.587780>> 
	TempProblemArea.fFloat = 50.0
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	// url:bugstar:4004409 - Hostile Takeover I -  Insurgent Spawns erratically on the second vehicle re spawn 
	TempProblemArea.vCoords1 = <<365.303, -1602.133, 18.692>>
	TempProblemArea.vCoords2 = <<427.503, -1651.908, 38.242>> 
	TempProblemArea.fFloat = 56.0
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)	
	
	// url:bugstar:4229588 - Stromberg - When player returned to freemode PV is wedged underneath a bridge 
	TempProblemArea.vCoords1 = <<-502.951, 5902.415, 30.870>>
	TempProblemArea.vCoords2 = <<-517.826, 5884.490, 37.920>> 
	TempProblemArea.fFloat = 7.4
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)		

	// url:bugstar:4117181 - Riot Van - After failing the mission and continuing back to Freemode, local players personal vehicle spawned on its side in an incorrect location
	TempProblemArea.vCoords1 = <<511.894, -614.599, 22.926>>
	TempProblemArea.vCoords2 = <<505.244, -598.924, 27.426>> 
	TempProblemArea.fFloat = 14.2
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)	
	
	// url:bugstar:4987605 - Avenger - After exiting the Avenger with their Barrage, Local player is spawned stuck in a tunnel and is unable to exit the vehicle
	TempProblemArea.vCoords1 = <<642.941406,-854.129944,8.314407>>
	TempProblemArea.vCoords2 = <<720.598999,-856.593140,23.884167>> 
	TempProblemArea.fFloat = 21.25
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)	
	
	// url:bugstar:4997977 - Menacer - Player is placed inside of a tunnel/ under the map, after exiting their Mobile Operations Center mod shop, near Del Perro Pier.
	TempProblemArea.vCoords1 = <<-1429.724487,-762.226135,10.154169>>
	TempProblemArea.vCoords2 = <<-1604.737061,-741.047546,19.188263>> 
	TempProblemArea.fFloat = 73.0
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)	
	
	// url:bugstar:4969274 - Hacker Truck - Player was placed inside a warehouse when exiting the Terrorbyte that was parked outside.
	TempProblemArea.vCoords1 =  <<30.628021,-2754.172363,1.136961>>
	TempProblemArea.vCoords2 = <<24.776140,-2652.691406,20.755089>>
	TempProblemArea.fFloat = 54.0
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)	
	
	// url:bugstar:4241951 - Buzzard is spawning on road node rather than one of the boss vehicle spots when ordering it from SecuroServ outside the Sandy Shores Facility.
	TempProblemArea.vCoords1 = <<2731.184, 3908.505, 41.770>>
	TempProblemArea.vCoords2 = <<2708.950, 3896.483, 47.319>> 
	TempProblemArea.fFloat = 70.0
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)
	
	// url:bugstar:5818688 - Casino Property - After requesting my PV along with several remote players just outside the casino, my vehicle spawned in a bush (clipping with prop_plant_paradise (in 'v_ext_veg.rpf') at 936.818,71.251,78.070)
	TempProblemArea.vCoords1 = <<926.556, 70.443, 57.077>>
	TempProblemArea.vCoords2 = <<946.606, 95.593, 98.852>> 
	TempProblemArea.fFloat = 21.5
	ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)	
	

	
	IF g_EnableBlockingNodesClubhouse7524212
		// url:bugstar:7524212 - Gen9 - Adversary - Extraction 3 - Local Player after getting killed by the Target player is left infinitely falling under the map after respawning.
		TempProblemArea.vCoords1 = <<968.812317,-1860.570557,29.479916>>
		TempProblemArea.vCoords2 = <<971.388489,-1830.701416,32.278702>> 
		TempProblemArea.fFloat = 21.000000
		ADD_TO_PROBLEM_AREAS_NODES(TempProblemArea)	
	ENDIF
	
	
	// output totals
	PRINTLN("[spawning] Problem area totals:")
	REPEAT (NUMBER_OF_SPAWNING_SECTORS) iSector
		PRINTLN("[spawning] g_iTotalForbiddenProblemAreasForSector[", iSector, "] = ", g_iTotalForbiddenProblemAreasForSector[iSector])
		PRINTLN("[spawning] g_iTotalNodesProblemAreasForSector[", iSector, "] = ", g_iTotalNodesProblemAreasForSector[iSector])
	ENDREPEAT	
	
	// ******************************************************************************
	//			Problem nodes search areas, doing a car node search in this area inevitably gives shit results, move out.
	// ******************************************************************************		
	
	ProblemArea_NodeSearch[0].vCoords1 = <<-1317.810, 3940.036, 25.00>>
	ProblemArea_NodeSearch[0].vCoords2 = <<-2014.503, 2694.947, 400.0>>
	ProblemArea_NodeSearch[0].fFloat = 1178.0
	
	ProblemArea_NodeSearch[1].vCoords1 = <<1645.100586,25.514793,162.508286>>
	ProblemArea_NodeSearch[1].vCoords2 = <<1644.721436,16.450609,182.774399>>
	ProblemArea_NodeSearch[1].fFloat = 10.0
	
	// ******************************************************************************
	//			Area to create custom vehicle nodes
	// ******************************************************************************		
	
	ProblemArea_CustomVehicleNodes[0].vCoords1 = <<-1162.0, -2722.0, 0.0>>
	ProblemArea_CustomVehicleNodes[0].vCoords2 = <<-1113.0, -2672.0, 38.0>>
	ProblemArea_CustomVehicleNodes[0].fFloat = 52.0	
	
	// Del Perro Apartment
	ProblemArea_CustomVehicleNodes[1].vCoords1 = <<-1447.8, -537.0, 37.0>>
	ProblemArea_CustomVehicleNodes[1].vCoords2 = <<-1425.8, -571.3, 29.0>>
	ProblemArea_CustomVehicleNodes[1].fFloat = 18.125	
	
	// Eclipse Towers Apartment
	ProblemArea_CustomVehicleNodes[2].vCoords1 = <<-773.9, 298.6, 84.4>>
	ProblemArea_CustomVehicleNodes[2].vCoords2 = <<-774.0, 322.1, 89.0>>
	ProblemArea_CustomVehicleNodes[2].fFloat = 15.325
	
	// Tinsel Towers Apartment
	ProblemArea_CustomVehicleNodes[3].vCoords1 = <<-618.1, 46.0, 47.2>>
	ProblemArea_CustomVehicleNodes[3].vCoords2 = <<-617.6, 20.9, 40.2>>
	ProblemArea_CustomVehicleNodes[3].fFloat = 15.325
	
	// Weazel Plaza Apartment
	ProblemArea_CustomVehicleNodes[4].vCoords1 = <<-929.4, -459.7, 36.0>>
	ProblemArea_CustomVehicleNodes[4].vCoords2 = <<-908.8, -449.5, 41.4>>
	ProblemArea_CustomVehicleNodes[4].fFloat = 18.125
	
	// Richard's Majestic Apartement
	ProblemArea_CustomVehicleNodes[5].vCoords1 = <<-954.8, -390.8, 37.9>>
	ProblemArea_CustomVehicleNodes[5].vCoords2 = <<-928.1, -378.2, 40.7>>
	ProblemArea_CustomVehicleNodes[5].fFloat = 18.12
	
	// Integrity Way Apartment
	ProblemArea_CustomVehicleNodes[6].vCoords1 = <<-56.3, -583.1, 34.5>>
	ProblemArea_CustomVehicleNodes[6].vCoords2 = <<-44.0, -587.1, 41.3>>
	ProblemArea_CustomVehicleNodes[6].fFloat = 12.0
	
	// Alta Street Apartment
	ProblemArea_CustomVehicleNodes[7].vCoords1 = <<-254.4, -987.6, 30.0>>
	ProblemArea_CustomVehicleNodes[7].vCoords2 = <<-264.8, -964.3, 35.2>>
	ProblemArea_CustomVehicleNodes[7].fFloat = 21.0
	
	// vanilla unicorn strip club
	ProblemArea_CustomVehicleNodes[8].vCoords1 = <<94.475, -1300.975, 37.425>>
	ProblemArea_CustomVehicleNodes[8].vCoords2 = <<142.475, -1279.8, 18.3>>
	ProblemArea_CustomVehicleNodes[8].fFloat = 39.625
	
	// the art museum
	ProblemArea_CustomVehicleNodes[9].vCoords1 = <<-2220.725, 176.250, 160.0>>
	ProblemArea_CustomVehicleNodes[9].vCoords2 = <<-2304.575, 361.375, 211.417>>
	ProblemArea_CustomVehicleNodes[9].fFloat = 173.3
	
	// ------ stilt houses ----------------
	
	//2117 Milton Road
	ProblemArea_CustomVehicleNodes[10].vCoords1 = <<-554.3, 652.1, 140.2>>
	ProblemArea_CustomVehicleNodes[10].vCoords2 = <<-576.7, 657.8, 160.3>>
	ProblemArea_CustomVehicleNodes[10].fFloat = 40.7
	
	//2866 Hillcrest Avenue
	ProblemArea_CustomVehicleNodes[11].vCoords1 = <<-756.4, 593.5, 138.0>>
	ProblemArea_CustomVehicleNodes[11].vCoords2 = <<-735.0, 581.1, 154.2>>
	ProblemArea_CustomVehicleNodes[11].fFloat = 34.9
	
	//2045 North Conker Avenue
	ProblemArea_CustomVehicleNodes[12].vCoords1 = <<359.5, 419.3, 136.7>>
	ProblemArea_CustomVehicleNodes[12].vCoords2 = <<381.8, 414.2, 155.2>>
	ProblemArea_CustomVehicleNodes[12].fFloat = 39.2
	
	//2113 Mad Wayne Thunder Drive
	ProblemArea_CustomVehicleNodes[13].vCoords1 = <<-1302.21, 440.0, 94.3>>
	ProblemArea_CustomVehicleNodes[13].vCoords2 = <<-1279.9, 440.2, 106.4>>
	ProblemArea_CustomVehicleNodes[13].fFloat = 41.0
	
	//3655 Wild Oats Drive
	ProblemArea_CustomVehicleNodes[14].vCoords1 = <<-161.5, 493.5, 130.5>>
	ProblemArea_CustomVehicleNodes[14].vCoords2 = <<-192.5, 487.9, 145.9>>
	ProblemArea_CustomVehicleNodes[14].fFloat = 38.0
	
	//2044 North Conker Avenue
	ProblemArea_CustomVehicleNodes[15].vCoords1 = <<330.1, 451.5, 137.6>>
	ProblemArea_CustomVehicleNodes[15].vCoords2 = <<343.7, 423.9, 158.2>>
	ProblemArea_CustomVehicleNodes[15].fFloat = 42.1
	
	//2868 Hillcrest Avenue
	ProblemArea_CustomVehicleNodes[16].vCoords1 = <<-762.0, 606.3, 136.6>>
	ProblemArea_CustomVehicleNodes[16].vCoords2 = <<-769.9, 628.3, 157.0>>
	ProblemArea_CustomVehicleNodes[16].fFloat = 36.5
	
	//2862 Hillcrest Avenue
	ProblemArea_CustomVehicleNodes[17].vCoords1 = <<-667.8, 594.7, 135.6>>
	ProblemArea_CustomVehicleNodes[17].vCoords2 = <<-686.5, 579.2, 156.6>>
	ProblemArea_CustomVehicleNodes[17].fFloat = 36.8
	
	//3677 Whispymound Drive
	ProblemArea_CustomVehicleNodes[18].vCoords1 = <<112.1, 551.3, 170.8>>
	ProblemArea_CustomVehicleNodes[18].vCoords2 = <<137.8, 554.0, 199.8>>
	ProblemArea_CustomVehicleNodes[18].fFloat = 41.3
	
	//2878 Hillcrest Avenue
	ProblemArea_CustomVehicleNodes[19].vCoords1 = <<-921.8, 685.3, 144.5>>
	ProblemArea_CustomVehicleNodes[19].vCoords2 = <<-896.7, 685.0, 196.5>>
	ProblemArea_CustomVehicleNodes[19].fFloat = 35.35
	
	//2874 Hillcrest Avenue
	ProblemArea_CustomVehicleNodes[20].vCoords1 = <<-846.4, 685.4, 143.7>>
	ProblemArea_CustomVehicleNodes[20].vCoords2 = <<-874.2, 683.7, 164.4>>
	ProblemArea_CustomVehicleNodes[20].fFloat = 45.8

	// office 1
	ProblemArea_CustomVehicleNodes[21].vCoords1 = <<-1556.589, -594.254, 82.373>>
	ProblemArea_CustomVehicleNodes[21].vCoords2 = <<-1584.314, -555.679, 111.673>>
	ProblemArea_CustomVehicleNodes[21].fFloat = 50.0
	
	// office 2
	ProblemArea_CustomVehicleNodes[22].vCoords1 = <<-1352.341, -473.819, 59.217>>
	ProblemArea_CustomVehicleNodes[22].vCoords2 = <<-1405.716, -481.069, 82.892>>
	ProblemArea_CustomVehicleNodes[22].fFloat = 50.0
	
	// office 3
	ProblemArea_CustomVehicleNodes[23].vCoords1 = <<-136.426, -663.413, 173.846>>
	ProblemArea_CustomVehicleNodes[23].vCoords2 = <<-141.729, -608.488, 160.546>>
	ProblemArea_CustomVehicleNodes[23].fFloat = 50.0
	
	// office 4
	ProblemArea_CustomVehicleNodes[24].vCoords1 = <<-61.368, -787.618, 235.936>>
	ProblemArea_CustomVehicleNodes[24].vCoords2 = <<-80.668, -838.218, 251.011>>
	ProblemArea_CustomVehicleNodes[24].fFloat = 41.225
	
	// office 1 - exit
	ProblemArea_CustomVehicleNodes[25].vCoords1 = <<-1573.221, -569.635, 33.178>>
	ProblemArea_CustomVehicleNodes[25].vCoords2 = <<-1586.796, -550.985, 43.728>>
	ProblemArea_CustomVehicleNodes[25].fFloat = 28.5
	
	// office 2 - exit
	ProblemArea_CustomVehicleNodes[26].vCoords1 = <<-1388.815, -504.708, 31.232>>
	ProblemArea_CustomVehicleNodes[26].vCoords2 = <<-1366.065, -501.258, 39.032>>
	ProblemArea_CustomVehicleNodes[26].fFloat = 13.050
	
	// office 3 - exit
	ProblemArea_CustomVehicleNodes[27].vCoords1 = <<-120.831, -600.078, 34.309>>
	ProblemArea_CustomVehicleNodes[27].vCoords2 = <<-107.106, -604.728, 41.784>>
	ProblemArea_CustomVehicleNodes[27].fFloat = 24.975
	
	// office 4 - exit
	ProblemArea_CustomVehicleNodes[28].vCoords1 = <<-82.474, -799.111, 41.802>>
	ProblemArea_CustomVehicleNodes[28].vCoords2 = <<-82.799, -785.336, 49.902>>
	ProblemArea_CustomVehicleNodes[28].fFloat = 25.4	
	
	// biker clubhouse variation 1
	ProblemArea_CustomVehicleNodes[29].vCoords1 = <<1109.0, -3140.0, -30.57>>
	ProblemArea_CustomVehicleNodes[29].vCoords2 = <<1109.0, -3174.0, -39.0>>
	ProblemArea_CustomVehicleNodes[29].fFloat = 35.725
	
	// biker clubhouse variation 2
	ProblemArea_CustomVehicleNodes[30].vCoords1 = <<1008.7, -3176.3, -32.3>>
	ProblemArea_CustomVehicleNodes[30].vCoords2 = <<1008.7, -3144.075, -40.8>>
	ProblemArea_CustomVehicleNodes[30].fFloat = 37.15
	
	// arena
	ProblemArea_CustomVehicleNodes[31].vCoords1 = <<ARENA_X-20.0, ARENA_Y, ARENA_Z-20.0>>
	ProblemArea_CustomVehicleNodes[31].vCoords2 = <<ARENA_X+20.0, ARENA_Y, ARENA_Z+20.0>>
	ProblemArea_CustomVehicleNodes[31].fFloat = 20.0
	
	// ******************************************************************************
	//			Problem area - when spawning airplanes, need to be at least min height in these areas.
	// ******************************************************************************		
	
	// downtown sky scrapers
	ProblemArea_MinAirplaneHeight[0].vCoords1 = <<307.749, -860.074, 0.0>>
	ProblemArea_MinAirplaneHeight[0].vCoords2 = <<-414.288, -718.096, 366.0>>
	ProblemArea_MinAirplaneHeight[0].fFloat = 794.15
	
	// downtown sky scrapers
	ProblemArea_MinAirplaneHeight[1].vCoords1 = <<-434.8, -870.7, 0.0>>
	ProblemArea_MinAirplaneHeight[1].vCoords2 = <<-1106.0, -310.0, 205.7>>
	ProblemArea_MinAirplaneHeight[1].fFloat = 377.2	
	
	// eclipse tower
	ProblemArea_MinAirplaneHeight[2].vCoords1 = <<-673.5, 156.9, 0.0>>
	ProblemArea_MinAirplaneHeight[2].vCoords2 = <<-859.0, 484.5, 262.2>>
	ProblemArea_MinAirplaneHeight[2].fFloat = 376.4		
	
	// bridge by airport
	ProblemArea_MinAirplaneHeight[3].vCoords1 = <<-137.675, -2511.925, 0.0>>
	ProblemArea_MinAirplaneHeight[3].vCoords2 = <<-998.875, -2014.8, 125.0>>
	ProblemArea_MinAirplaneHeight[3].fFloat = 408.7
	
ENDPROC


// eof
