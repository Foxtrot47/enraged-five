USING "rage_builtins.sch"
USING "globals.sch"
USING "title_update_globals.sch"

USING "net_events.sch"
USING "net_prints.sch"

USING "net_mission_control_broadcasts.sch"
USING "net_missions_at_coords_public.sch"
USING "net_missions_shared_cloud_data_public.sch"
USING "net_activity_selector_public.sch"

#IF IS_DEBUG_BUILD
	USING "net_missions_shared_debug.sch"
#ENDIF


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   net_missions_shared_cloud_data.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Stores the cloud-loaded header details of an active mission (usually one where the player has entered the
//								corona) so that, should the mission be removed from the cloud-loaded header details (because, for
//								example, the owner of a UGC mission leaves the game), or should the cloud arrays be out-of-sync on
//								different player's machines, or should there be a cloud-loading issue with a mission on a player's
//								machine, then the cloud-loaded header details are recorded here so that all player can access them
//								keeping the game in-sync and problem free.
//
//		NOTES			:	UNIQUELY IDENTIFYING MISSIONS SHARED REQUESTS - KGM 13/3/13
//							The Cloud Filename/Mission Request UniqueID pair is used as a unique identifier for a shared cloud mission.
//								It is possible that the same cloud mission is being triggered using multiple instances, so the uniqueID
//								should differentiate between them which will also be passed in as part of the registration process.
//
//							CLOUD-LOADED SHARED MISSIONS REGISTRATION - KGM 20/3/13
//							The local player sets up a corona when another player shares a mission, but the local player will only
//								register themselves for the shared mission when they enter the corona themselves. Registration
//								ensures that the mission data remains while any player still requires it so the owner of the mission
//								can leave the game without affecting those players that are in the corona through shared mission data.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      SERVER CONTROL BITSET ACCESS FUNCTIONS
// ===========================================================================================================

// PURPOSE:	Check if the Cloud Description data for a shared mission has been stored
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
// RETURN VALUE:			BOOL					TRUE if the cloud data is stored, FALSE if not stored
FUNC BOOL Is_Shared_Cloud_Mission_Description_Data_Stored(INT paramSlot)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscBitflags, MSCLOUD_BITFLAG_GOT_DESCRIPTION_DATA))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the Cloud description data for a shared mission has been stored
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
PROC Set_Shared_Cloud_Mission_Description_Data_Stored(INT paramSlot)

	SET_BIT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscBitflags, MSCLOUD_BITFLAG_GOT_DESCRIPTION_DATA)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Setting MSCLOUD_BITFLAG_GOT_DESCRIPTION_DATA") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Cloud names data for a shared mission has been stored
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
// RETURN VALUE:			BOOL					TRUE if the cloud data is stored, FALSE if not stored
FUNC BOOL Is_Shared_Cloud_Mission_Names_Data_Stored(INT paramSlot)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscBitflags, MSCLOUD_BITFLAG_GOT_NAMES_DATA))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the Cloud names data for a shared mission has been stored
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
PROC Set_Shared_Cloud_Mission_Names_Data_Stored(INT paramSlot)

	SET_BIT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscBitflags, MSCLOUD_BITFLAG_GOT_NAMES_DATA)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Setting MSCLOUD_BITFLAG_GOT_NAMES_DATA") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Cloud details data for a shared mission has been stored
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
// RETURN VALUE:			BOOL					TRUE if the cloud data is stored, FALSE if not stored
FUNC BOOL Is_Shared_Cloud_Mission_Details_Data_Stored(INT paramSlot)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscBitflags, MSCLOUD_BITFLAG_GOT_DETAILS_DATA))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the Cloud details data for a shared mission has been stored
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
PROC Set_Shared_Cloud_Mission_Details_Data_Stored(INT paramSlot)

	SET_BIT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscBitflags, MSCLOUD_BITFLAG_GOT_DETAILS_DATA)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Setting MSCLOUD_BITFLAG_GOT_DETAILS_DATA") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if all the Cloud data for a shared mission has been stored
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
// RETURN VALUE:			BOOL					TRUE if the cloud data is stored, FALSE if not stored
FUNC BOOL Is_All_Shared_Cloud_Mission_Data_Stored(INT paramSlot)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscBitflags, MSCLOUD_BITFLAG_ALL_DATA_RECEIVED))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that all the Cloud data for a shared mission has been stored
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
PROC Set_All_Shared_Cloud_Mission_Data_Stored_If_All_Data_Stored(INT paramSlot)

	IF NOT (Is_Shared_Cloud_Mission_Description_Data_Stored(paramSlot))
	OR NOT (Is_Shared_Cloud_Mission_Names_Data_Stored(paramSlot))
	OR NOT (Is_Shared_Cloud_Mission_Details_Data_Stored(paramSlot))
		EXIT
	ENDIF

	SET_BIT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscBitflags, MSCLOUD_BITFLAG_ALL_DATA_RECEIVED)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Setting MSCLOUD_BITFLAG_ALL_DATA_RECEIVED: All Shared Cloud Mission Data has been stored") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if players are restricted from joining this cloud-loaded shared mission (ie: because the mission has started or is not joinable)
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
// RETURN VALUE:			BOOL					TRUE if players are restricted, FALSE if not retricted
FUNC BOOL Is_Shared_Cloud_Mission_Restricting_Players(INT paramSlot)
	RETURN (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscBitflags, MSCLOUD_BITFLAG_RESTRICT_PLAYERS))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that players are now restricted from joining this cloud-loaded shared mission
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
PROC Set_Shared_Cloud_Mission_Is_Restricting_Players(INT paramSlot)

	SET_BIT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscBitflags, MSCLOUD_BITFLAG_RESTRICT_PLAYERS)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Setting MSCLOUD_BITFLAG_RESTRICT_PLAYERS: This Shared Cloud Mission is now restricting players (mission started or not joinable)") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that players used to be restricted but are no longer restricted from joining this cloud-loaded shared mission
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
PROC Set_Shared_Cloud_Mission_Is_No_Longer_Restricting_Players(INT paramSlot)

	CLEAR_BIT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscBitflags, MSCLOUD_BITFLAG_RESTRICT_PLAYERS)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Clearing MSCLOUD_BITFLAG_RESTRICT_PLAYERS: This Shared Cloud Mission was restricted but now isn't again (perhaps not joinable has become joinable)") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if there are any registered players for this shared cloud-loaded mission
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
// RETURN VALUE:			BOOL					TRUE if there are registered players, FALSE if no registered players
FUNC BOOL Are_There_Any_Registered_Players_For_Shared_Cloud_Mission(INT paramSlot)

	IF (GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscPlayersReg = ALL_PLAYER_BITS_CLEAR)
		// ...there are no registered players
		RETURN FALSE
	ENDIF
	
	// There are still registered players
	RETURN TRUE
	
ENDFUNC




// ===========================================================================================================
//      CLIENT MISSION CONTROL BITSET ACCESS FUNCTIONS
// ===========================================================================================================

// PURPOSE:	Check if the Client has already processed the specified shared mission
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Client Missions Shared Cloud data array
// RETURN VALUE:			BOOL					TRUE if the mission has already been processed, FALSE if not
FUNC BOOL Has_Client_Processed_This_Shared_Cloud_Loaded_Mission(INT paramSlot)
	RETURN (IS_BIT_SET(g_MSCloudClientControl.mscccdMissions[paramSlot].msccdBitfield, MSCCLIENT_BITFLAG_MISSION_PROCESSED))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the Client has processed a specified shared mission
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
PROC Set_Client_Has_Processed_Shared_Cloud_Mission(INT paramSlot)

	SET_BIT(g_MSCloudClientControl.mscccdMissions[paramSlot].msccdBitfield, MSCCLIENT_BITFLAG_MISSION_PROCESSED)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" Setting Client Is Processing Shared Cloud-Loaded Mission In Slot: ")
		NET_PRINT_INT(paramSlot)
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the client is restricted from seeing this content
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Client Missions Shared Cloud data array
// RETURN VALUE:			BOOL					TRUE if the content is restricted, FALSE if not restricted
FUNC BOOL Is_Client_Restricted_From_Seeing_This_Shared_Cloud_Loaded_Mission(INT paramSlot)
	RETURN (IS_BIT_SET(g_MSCloudClientControl.mscccdMissions[paramSlot].msccdBitfield, MSCCLIENT_BITFLAG_RESTRICTED_CONTENT))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the Client is restricted from seeing this shared mission
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
//
// NOTES: This is due to a permissions restriction, like underage, or content set to FRIENDS ONLY
PROC Set_Client_Is_Restricted_From_Seeing_This_Shared_Cloud_Mission(INT paramSlot)

	SET_BIT(g_MSCloudClientControl.mscccdMissions[paramSlot].msccdBitfield, MSCCLIENT_BITFLAG_RESTRICTED_CONTENT)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" Setting Client is privileges restricted from seeing the Shared Mission content in Slot: ")
		NET_PRINT_INT(paramSlot)
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Client has added this shared mission
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Client Missions Shared Cloud data array
// RETURN VALUE:			BOOL					TRUE if the mission was added using the shared data, FALSE if not added (or already added using local data)
//
// NOTES:	This will return TRUE if the client didn't have the mission registered using local data, but had to wait for the shared data to register it
FUNC BOOL Has_Client_Added_This_Shared_Cloud_Loaded_Mission(INT paramSlot)
	RETURN (IS_BIT_SET(g_MSCloudClientControl.mscccdMissions[paramSlot].msccdBitfield, MSCCLIENT_BITFLAG_ADDED_SHARED_MISSION))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the Client has added a specified shared mission
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
PROC Set_Client_Has_Added_Shared_Cloud_Mission(INT paramSlot)

	SET_BIT(g_MSCloudClientControl.mscccdMissions[paramSlot].msccdBitfield, MSCCLIENT_BITFLAG_ADDED_SHARED_MISSION)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" Setting Client has Registered the Mission using the shared data in Slot: ")
		NET_PRINT_INT(paramSlot)
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the Client has removed this shared mission
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Client Missions Shared Cloud data array
// RETURN VALUE:			BOOL					TRUE if the mission was added and has been removed, FALSE if not removed (or never added)
FUNC BOOL Has_Client_Removed_This_Shared_Cloud_Loaded_Mission(INT paramSlot)
	RETURN (IS_BIT_SET(g_MSCloudClientControl.mscccdMissions[paramSlot].msccdBitfield, MSCCLIENT_BITFLAG_MISSION_REMOVED))
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set that the Client has removed a specified shared mission
//
// INPUT PARAMS:			paramSlot				The mission details slot on the Missions Shared Cloud data array
PROC Set_Client_Has_Removed_Shared_Cloud_Mission(INT paramSlot)

	SET_BIT(g_MSCloudClientControl.mscccdMissions[paramSlot].msccdBitfield, MSCCLIENT_BITFLAG_MISSION_REMOVED)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" Setting Client has removed the Shared Mission in Slot: ")
		NET_PRINT_INT(paramSlot)
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Allow the Shared Mission to be setup again on the client
//
// INPUT PARAMS:		paramSlot				The slot containing the cloud-loaded header details for the mission
PROC Allow_Client_To_Setup_This_Shared_Mission_Again(INT paramSlot)

	// Clearing the 'processed', 'added', and 'removed' flags should do it

	CLEAR_BIT(g_MSCloudClientControl.mscccdMissions[paramSlot].msccdBitfield, MSCCLIENT_BITFLAG_MISSION_PROCESSED)
	CLEAR_BIT(g_MSCloudClientControl.mscccdMissions[paramSlot].msccdBitfield, MSCCLIENT_BITFLAG_ADDED_SHARED_MISSION)
	CLEAR_BIT(g_MSCloudClientControl.mscccdMissions[paramSlot].msccdBitfield, MSCCLIENT_BITFLAG_MISSION_REMOVED)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" Allowing Client to setup the Shared Mission again in Slot: ")
		NET_PRINT_INT(paramSlot)
		NET_NL()
	#ENDIF

ENDPROC




// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the Start Coords for this cloud-loaded mission slot
//
// INPUT PARAMS:		paramSlot				The slot containing the cloud-loaded header details for the mission
// RETURN VALUE:		VECTOR					The Start Coords
FUNC VECTOR Get_Coords_For_Shared_Cloud_Loaded_Mission(INT paramSlot)

	VECTOR theCoords = << 0.0, 0.0, 0.0 >>
	
	IF (Is_Shared_Cloud_Mission_Slot_In_Use(paramSlot))
		theCoords = GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.vStartPos
	ENDIF
	
	RETURN theCoords

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the 'for heist tutorial cutscene only' flag is set for this cloud-loaded mission slot
//
// INPUT PARAMS:		paramSlot				The slot containing the cloud-loaded header details for the mission
// RETURN VALUE:		BOOL					The 'for tutorial cutscene only' flag state
FUNC BOOL Is_Shared_Cloud_Loaded_Mission_For_Heist_Tutorial_Cutscene_Only(INT paramSlot)
	RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscForCutsceneID = FOR_CUT_HEIST_TUTORIAL_INTRO)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the 'for heist intro cutscene only' flag is set for this cloud-loaded mission slot
//
// INPUT PARAMS:		paramSlot				The slot containing the cloud-loaded header details for the mission
// RETURN VALUE:		BOOL					The 'for intro cutscene only' flag state
//
// NOTES:	KGM 28/9/14: This should also return TRUE for the Heist Tutorial Intro cutscene
FUNC BOOL Is_Shared_Cloud_Loaded_Mission_For_Heist_Intro_Cutscene_Only(INT paramSlot)

	// Return TRUE if this is a Heist Intro Tutorial Cutscene
	IF (Is_Shared_Cloud_Loaded_Mission_For_Heist_Tutorial_Cutscene_Only(paramSlot))
		RETURN TRUE
	ENDIF
	
	// Otherwise, return TRUE for any INTRO cutscene
	RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscForCutsceneID = FOR_CUT_HEIST_INTRO)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the 'for heist mid-strand cutscene only' flag is set for this cloud-loaded mission slot
//
// INPUT PARAMS:		paramSlot				The slot containing the cloud-loaded header details for the mission
// RETURN VALUE:		BOOL					The 'for intro cutscene only' flag state
FUNC BOOL Is_Shared_Cloud_Loaded_Mission_For_Heist_MidStrand_Cutscene_Only(INT paramSlot)
	RETURN (GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscForCutsceneID = FOR_CUT_HEIST_MID_STRAND)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the Mission Controller UnqiueID for this cloud-loaded mission slot
//
// INPUT PARAMS:		paramSlot				The slot containing the cloud-loaded header details for the mission
// RETURN VALUE:		INT						The Mission Controller UniqueID
FUNC INT Get_UniqueID_For_Shared_Cloud_Loaded_Mission(INT paramSlot)

	INT theUniqueID = NO_UNIQUE_ID
	
	IF (Is_Shared_Cloud_Mission_Slot_In_Use(paramSlot))
		theUniqueID = GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscUniqueID
	ENDIF
	
	RETURN theUniqueID

ENDFUNC




// ===========================================================================================================
//      PRIVATE FUNCTIONS
// ===========================================================================================================

// PURPOSE:	Get the Shared Mission dta slot the player is registered for
//
// INPUT PARAMS:		paramPlayerID		PlayerID of player being checked
//						INT					Shared Cloud Mission slot, or ILLEGAL_ARRAY_POSITION
FUNC INT Get_Players_Registered_Shared_Cloud_Mission_Slot(PLAYER_INDEX paramPlayerID)

	IF NOT (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
		RETURN ILLEGAL_ARRAY_POSITION
	ENDIF
	
	// Search the registered missions for this player
	INT	tempLoop	= 0
	INT	playerBit	= NATIVE_TO_INT(paramPlayerID)
	
	REPEAT MAX_MISSIONS_SHARED_CLOUD_DATA tempLoop
		IF (Is_Shared_Cloud_Mission_Slot_In_Use(tempLoop))
			// Check if the player is registered for this mission
			IF (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[tempLoop].mscPlayersReg, playerBit))
				// Player is registered for this mission
				RETURN tempLoop
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Player isn't registered for any mission
	RETURN ILLEGAL_ARRAY_POSITION
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the player is registered for any mission
//
// INPUT PARAMS:		paramPlayerID			The Player Index
// RETURN VALUE:		BOOL					TRUE if the player is registered for any mission, FALSE if not registered for any mission
FUNC BOOL Is_Player_Registered_For_Any_Mission(PLAYER_INDEX paramPlayerID)
	RETURN (Get_Players_Registered_Shared_Cloud_Mission_Slot(paramPlayerID) != ILLEGAL_ARRAY_POSITION)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Add a player to the registered players bitfield of a mission
//
// INPUT PARAMS:		paramSlot			Shared Cloud Mission slot
//						paramPlayerID		PlayerID of player registering the mission
PROC Add_Player_As_Registered_With_Shared_Cloud_Mission(INT paramSlot, PLAYER_INDEX paramPlayerID)

	// Set the player bit for this player
	INT	playerBit	= NATIVE_TO_INT(paramPlayerID)

	// DEBUG ONLY: Check if the player is already registered
	#IF IS_DEBUG_BUILD
		IF (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscPlayersReg, playerBit))
			NET_PRINT("...KGM MP [MShared][CloudData]: Add_Player_As_Registered_With_Shared_Cloud_Mission() - INFO: Player is already registered for the mission") NET_NL()
		ENDIF
	#ENDIF
	
	// KGM 30/6/14: [BUG 1907641] For heist-related missions, need to store the bit of the first player to be registered - this will be used to identify the Leader when passing
	//				the mission to the heist activity selector routines to act as in intermediary between the Mission Shared data and the Matc routines
	BOOL firstRegPlayer = FALSE
	IF (GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscPlayersReg = ALL_PLAYER_BITS_CLEAR)
		firstRegPlayer = TRUE
	ENDIF
	
	SET_BIT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscPlayersReg, playerBit)
	
	IF (firstRegPlayer)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscBitFirstPlayerReg = ALL_PLAYER_BITS_CLEAR
		SET_BIT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscBitFirstPlayerReg, playerBit)
	ENDIF
		
	#IF IS_DEBUG_BUILD
		IF (firstRegPlayer)
			NET_PRINT("...KGM MP [MShared][CloudData][Heist]: (SERVER) Registering FIRST Player for mission: ")
			NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
			NET_PRINT(" - This is use for Heist registration")
			NET_NL()
			Debug_Output_Player_Names_From_Bitfield(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscBitFirstPlayerReg)
		ELSE
			NET_PRINT("...KGM MP [MShared][CloudData][Heist]: (SERVER) For Info - first player registered for mission was: ")
			NET_NL()
			Debug_Output_Player_Names_From_Bitfield(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscBitFirstPlayerReg)
		ENDIF
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: (SERVER) Add Player as Registered for mission: ")
		NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
		NET_PRINT(" (Bit: ")
		NET_PRINT_INT(playerBit)
		NET_PRINT(") - REGISTERED PLAYERS (Bitfield value: ")
		NET_PRINT_INT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscPlayersReg)
		NET_PRINT(")")
		NET_NL()
		Debug_Output_Player_Names_From_Bitfield(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscPlayersReg)
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Remove a player as a registered player for this shared cloud-loaded mission
//
// INPUT PARAMS:		paramSlot			Shared Cloud Mission slot
//						paramPlayerID		PlayerID of player registering the mission
PROC Remove_Player_As_Registered_With_Shared_Cloud_Mission(INT paramSlot, PLAYER_INDEX paramPlayerID)

	// Set the player bit for this player
	INT	playerBit	= NATIVE_TO_INT(paramPlayerID)

	// Nothing to do if the player is not registered
	IF NOT (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscPlayersReg, playerBit))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared][CloudData]: (SERVER) Remove_Player_As_Registered_With_Shared_Cloud_Mission() - INFO: Player is not registered for the mission") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	CLEAR_BIT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscPlayersReg, playerBit)
	
	// If there are no player bits left, then clear the FirstRegPlayer bitfield
	IF (GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscPlayersReg = ALL_PLAYER_BITS_CLEAR)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscBitFirstPlayerReg = ALL_PLAYER_BITS_CLEAR
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared][CloudData][Heist]: (Server) There are no player bits left, so also clearing the First Registered Player bit")
			NET_NL()
		#ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: (Server) Unregister Player for mission: ")
		NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
		NET_PRINT(" (Bit: ")
		NET_PRINT_INT(playerBit)
		NET_PRINT(") - REMAINING REGISTERED PLAYERS (Bitfield value: ")
		NET_PRINT_INT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscPlayersReg)
		NET_PRINT(")")
		NET_NL()
		Debug_Output_Player_Names_From_Bitfield(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscPlayersReg)
	#ENDIF
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if cloud data is still required for the specified mission
//
// INPUT PARAMS:		paramFilename			The cloud filename
//						paramUniqueID			The Mission Controller UniqueID for a launch instance of a mission
// RETURN VALUE:		INT						The slot containing these mission details, or ILLEGAL_ARRAY_POSITION if not stored
FUNC INT Get_Slot_For_Existing_Shared_Cloud_Mission_With_These_Details(TEXT_LABEL_23 paramFilename, INT paramUniqueID)

	// Search the registered missions for these mission details
	INT		tempLoop		= 0
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFilename))
		RETURN ILLEGAL_ARRAY_POSITION
	ENDIF
	
	IF (paramUniqueID = NO_UNIQUE_ID)
		RETURN ILLEGAL_ARRAY_POSITION
	ENDIF
	
	REPEAT MAX_MISSIONS_SHARED_CLOUD_DATA tempLoop
		IF (Is_Shared_Cloud_Mission_Slot_In_Use(tempLoop))
			IF (paramUniqueID = GlobalServerBD_MissionsShared.cloudDetails[tempLoop].mscUniqueID)
			AND (ARE_STRINGS_EQUAL(paramFilename, GlobalServerBD_MissionsShared.cloudDetails[tempLoop].mscHeaderData.tlName))
				// ...found the mission
				RETURN (tempLoop)
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Mission Not Found
	RETURN (ILLEGAL_ARRAY_POSITION)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Find an unused shared cloud mission slot.
//
// RETURN VALUE:		INT						The free slot, or ILLEGAL_ARRAY_POSITION if none free
//
// NOTES:	A free slot is one marked not 'in use' and not 'been used' - the 'been used' is to prevent quick re-use of a slot to try to prevent edge cases.
//			After finding a free slot, the search will continue until it finds another free slot. If it does it'll quit, if it doesn't it will
//			remove all the 'been used' markers on slots opening up all slots to the next search again. It re-opens the slots when the last one is chosen
//			rather than when it doesn't find a free slot so that the next freed slot is already marked as 'been used' on the fresh list rather. If the slots
//			got re-opened when the search failed then the most recently freed slot would instantly have its 'been used' marker removed opening up the possibility
//			of instant re-use of the slot. Catering for edge case issues through immediate slot re-use may be unnecessary, but it can't hurt.
FUNC INT Find_Unused_Shared_Cloud_Mission_Slot()

	INT		tempLoop			= 0
	INT		emptySlot			= ILLEGAL_ARRAY_POSITION
	BOOL	foundEmptySlot		= FALSE
	INT		firstBeenUsedSlot	= ILLEGAL_ARRAY_POSITION
	BOOL	foundBeenUsedSlot	= FALSE
	
	REPEAT MAX_MISSIONS_SHARED_CLOUD_DATA tempLoop
		IF NOT (Is_Shared_Cloud_Mission_Slot_In_Use(tempLoop))
			IF NOT (GlobalServerBD_MissionsShared.cloudDetails[tempLoop].mscBeenUsed)
				IF (foundEmptySlot)
					// ...this is the second free slot, so abandon the search and don't bother clearing all the 'been used' flags
					RETURN (emptySlot)
				ENDIF
				
				// This is the first empty slot, so store the details and keep searching
				emptySlot		= tempLoop
				foundEmptySlot	= TRUE
			ELSE
				// ...just in case all slots have 'been used', record the first 'been used' slot so we don't have to process the loop again
				IF NOT (foundBeenUsedSlot)
					foundBeenUsedSlot	= TRUE
					firstBeenUsedSlot	= tempLoop
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// At this stage there's either no free slots, or only one
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: Running out of unused Free Slots: Current Array Contents:") NET_NL()
		Debug_Output_Missions_Shared_Cloud_Data_Array(TRUE)
	#ENDIF
	
	// Deal with there being no free slots
	IF NOT (foundEmptySlot)
		// ...there was no empty slot found
		// If there was a 'been used' slot that wasn't 'in use', then we can free up all the 'been used' slots and use this 'been used' slot, otherwise the array is full
		IF NOT (foundBeenUsedSlot)
			// Array is full
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MShared][CloudData]: Find_Unused_Shared_Cloud_Mission_Slot() - ERROR: Failed to find a free slot. Current array contents:")
				Debug_Output_Missions_Shared_Cloud_Data_Array(TRUE)
				SCRIPT_ASSERT("Find_Unused_Shared_Cloud_Mission_Slot(): ERROR - Failed to find a free slot. May need to increase MAX_MISSIONS_SHARED_CLOUD_DATA. See Console Log. Tell Keith.")
			#ENDIF
			
			RETURN ILLEGAL_ARRAY_POSITION
		ENDIF
		
		// There was a 'been used' slot, so put this into the 'empty slot' variables to re-use that slot but let processing continue into the routine below that frees up all 'been used' slots
		foundEmptySlot	= TRUE
		emptySlot		= firstBeenUsedSlot
	ENDIF
	
	// There must have been only one free slot, or we are using a 'been used' slot
	// Clear the 'been used' flags from slots that are no longer 'in use'
	REPEAT MAX_MISSIONS_SHARED_CLOUD_DATA tempLoop
		IF NOT (Is_Shared_Cloud_Mission_Slot_In_Use(tempLoop))
			IF (GlobalServerBD_MissionsShared.cloudDetails[tempLoop].mscBeenUsed)
				GlobalServerBD_MissionsShared.cloudDetails[tempLoop].mscBeenUsed = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Return the empty slot found during the original search
	RETURN (emptySlot)

ENDFUNC




// ===========================================================================================================
//      MISSION REGISTRATION FUNCTIONS
// ===========================================================================================================

// PURPOSE: Gather the blip details for a Shared Mission
//
// RETURN PARAMS:		paramMissionIdData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:		g_structMatCBlipMP		The data generated about the blip
FUNC g_structMatCBlipMP Gather_Blip_Details_For_Shared_Mission(MP_MISSION_ID_DATA &paramMissionIdData)

	g_structMatCBlipMP theBlipDetails
	Clear_All_MissionsAtCoords_BlipData(theBlipDetails)
	
	// Details from external functions
	// NOTE: These details need to become Mission Data along with the CnC blips
	BLIP_SPRITE		thisBlipSprite		= RADAR_TRACE_NUMBERED_1
	BLIP_PRIORITY	thisBlipPriority	= BLIPPRIORITY_LOWEST
	TEXT_LABEL_23	thisBlipName		= ""
	TEXT_LABEL_63	thisCreatorName		= ""
	INT				thisBlipColour		= BLIP_COLOUR_WHITE
	BOOL			fromMissionCreator	= FALSE
	BOOL			isShortRange		= TRUE
	BOOL			paramForceLongRange = FALSE
	
	TEMP_Get_FM_Activity_Blip_Details(paramMissionIdData, thisBlipSprite, thisBlipPriority, thisBlipName, thisCreatorName, thisBlipColour, fromMissionCreator, paramForceLongRange)
	
	// Interiors Only blips should be long-range
	IF paramForceLongRange
	OR (Is_MP_Mission_Variation_Blip_An_Interior_Blip_Only(paramMissionIdData.idMission, paramMissionIdData.idVariation))
		isShortRange = FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF paramForceLongRange
		PRINTLN("[LOWFLOW] paramForceLongRange = TRUE, thisBlipName = ", thisBlipName, " theBlipDetails.isShortRange = ", isShortRange)
	ENDIF
	#ENDIF
	// Setup the Blip Info
	theBlipDetails.isShortRange			= isShortRange				// Decided above
	theBlipDetails.theBlipSprite		= thisBlipSprite			// Gathered using above routine
	theBlipDetails.theBlipName			= thisBlipName				// Gathered using above routine
	theBlipDetails.thePlayerNameForBlip	= thisCreatorName			// Gathered using above routine
	theBlipDetails.theBlipPriority		= thisBlipPriority			// Gathered using above routine
	theBlipDetails.theBlipColour		= thisBlipColour			// Gathered using above routine
	theBlipDetails.fromMissionCreator	= fromMissionCreator		// Gathered using above routine
	
	// NOTE: Optional player blip name parameters currently not required
	
	RETURN theBlipDetails

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Gather the corona details for a Shared Mission
//
// RETURN PARAMS:		paramMissionIdData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:		g_structMatCCoronaMP	The data generated about the corona
FUNC g_structMatCCoronaMP Gather_Corona_Details_For_Shared_Mission(MP_MISSION_ID_DATA &paramMissionIdData)

	g_structMatCCoronaMP theCoronaDetails
	Clear_All_MissionsAtCoords_CoronaData(theCoronaDetails)

	HUD_COLOURS		coronaColour	= HUD_COLOUR_GREY
	HUD_COLOURS		textColour		= HUD_COLOUR_GREY
	eMP_TAG_SCRIPT	theIcon			= MP_TAG_SCRIPT_NONE
	FLOAT			theRadius		= 0.0
	INT				theOnHours		= MATC_ALL_DAY
	
	Fill_Corona_Details_For_FM_Activity(paramMissionIdData, coronaColour, textColour, theIcon, theRadius, theOnHours)

	// Setup the Corona Info
	theCoronaDetails.theCoronaColour	= coronaColour
	theCoronaDetails.theTextColour		= textColour
	theCoronaDetails.triggerRadius		= theRadius
	theCoronaDetails.theIcon			= theIcon
	theCoronaDetails.theOnHours			= theOnHours
	
	RETURN theCoronaDetails

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Gather the optional flags for a Shared Mission
//
// REFERENCE PARAMS:	refMissionIdData		The MissionID Data - this can update if the array is found to be out-of-sync
// RETURN VALUE:		g_structMatCOptionsMP	The struct containing the optional flags
FUNC g_structMatCOptionsMP Gather_Optional_Flags_For_Shared_Mission(MP_MISSION_ID_DATA &refMissionIdData)

	g_structMatCOptionsMP theOptionalFlags
	Clear_All_MissionsAtCoords_Options(theOptionalFlags)
	
	// Gather all the standard options that should be gathered for all missions
	Gather_Standard_Optional_Flags_For_Missions(refMissionIdData, theOptionalFlags)
	
	// Shared Missions will be deleted on play
	theOptionalFlags.matcoDeleteOnPlay			= TRUE
	
	// Shared Missions should ignore a cloud refresh (because the mission data is shared rather than on the global cloud arrays)
	theOptionalFlags.matcoIgnoreRefresh			= TRUE
			
	// These optional flags are ignored for missions that aren't registered with Matc during initialisation or during transition
	//	matcoHandleVoting		- This is going to be obsolete. Was setup for CnC but we'll instead use a different external routine to handle the voting.
	//	matcoDeleteAfterFocus	- Special case for missions where even walking out of the corona without playing should delete the mission.
	//	matcoHasCutscene		- This is required for CnC.
	//	matcoInteriorBlipOnly	- Currently only used by a minigame.
	//	matcoHasBeenPlayed		- Currently only setup during initialisation. It may be required here too at some point.

	RETURN theOptionalFlags

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Tries to gather the details for a mission and set it up
//
// INPUT PARAMS:			paramArrayPos			Shared Cloud-Loaded Missions array position
FUNC BOOL Register_This_Shared_Mission_For_Local_Player(INT paramArrayPos)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: Attempting MissionsAtCoords registration of the Shared Cloud-Loaded Mission in slot: ")
		NET_PRINT_INT(paramArrayPos)
		NET_PRINT(" [")
		NET_PRINT(GlobalServerBD_MissionsShared.cloudDetails[paramArrayPos].mscHeaderData.tlMissionName)
		NET_PRINT("]")
		NET_NL()
	#ENDIF


	MP_MISSION_ID_DATA		theMissionIdData		= Get_MissionID_Data_For_Shared_Cloud_Loaded_Mission(paramArrayPos)
	BOOL					forHeistIntroCut		= Is_Shared_Cloud_Loaded_Mission_For_Heist_Intro_Cutscene_Only(paramArrayPos)
	BOOL					forHeistMidStrandCut	= Is_Shared_Cloud_Loaded_Mission_For_Heist_MidStrand_Cutscene_Only(paramArrayPos)
	BOOL					forHeistTutorialCut		= Is_Shared_Cloud_Loaded_Mission_For_Heist_Tutorial_Cutscene_Only(paramArrayPos)
	VECTOR					theCoords				= Get_Coords_For_Shared_Cloud_Loaded_Mission(paramArrayPos)
	
	IF (Is_FM_Cloud_Loaded_Activity_A_Flow_Mission(theMissionIdData))
	AND NETWORK_IS_ACTIVITY_SESSION()
		#IF IS_DEBUG_BUILD
			NET_PRINT("         FAILED TO REGISTER Shared Cloud-Loaded - Is_FM_Cloud_Loaded_Activity_A_Flow_Mission") NET_NL() NET_NL()
		#ENDIF
		RETURN FALSE
	ENDIF
	
	// Heist or Heist Planning should now be sent to the Heist routines which will act as an intermediary between The Shared Routines and the Mission At Coords routines
	IF (Is_FM_Cloud_Loaded_Activity_A_Heist(theMissionIdData))
	OR (Is_FM_Cloud_Loaded_Activity_Heist_Planning(theMissionIdData))
		// Heist-Related Corona, so send the details to the Heist Routines to handle
		PRINTLN("...KGM MP [MShared][CloudData][Heist]: Mission is heist-related, so passing to the Heist routines")
		
		// Forward the required data to the intermediate Heist Control routines
		// NOTE: For Heists, the only registered player should be the Heist Leader
		INT bitFirstRegPlayer	= GlobalServerBD_MissionsShared.cloudDetails[paramArrayPos].mscBitFirstPlayerReg
		INT heistRegID			= Register_Other_Player_Active_Heist_Corona(theMissionIdData, theCoords, bitFirstRegPlayer, forHeistIntroCut, forHeistMidStrandCut, forHeistTutorialCut)
		
		IF (heistRegID = ILLEGAL_HEIST_CORONA_REG_ID)
			#IF IS_DEBUG_BUILD
				NET_PRINT("         FAILED TO REGISTER Shared Cloud-Loaded Heist-Related Mission with Heist Control system") NET_NL() NET_NL()
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		// If successfully registered with the Heist routines then add the returned heistRegID to the (large negative for easy ID) baseID and store in the AtCoords RegID field
		g_MSCloudClientControl.mscccdMissions[paramArrayPos].msccdMatcRegID	= MISSIONS_SHARED_AT_COORDS_BASE_ID_FOR_HEISTS + heistRegID

		PRINTLN("...KGM MP [MShared][CloudData][Heist]: Heist-related Mission registered with Heist Routines. RegID returned: ", heistRegID, "  MatcID Stored: ", g_MSCloudClientControl.mscccdMissions[paramArrayPos].msccdMatcRegID)
		
		RETURN TRUE
	ENDIF
		// FEATURE_HEIST_PLANNING
	
	// Not Heist-Related, so setup as normal
	g_eMPMissionSource		theSource			= MP_MISSION_SOURCE_MISSION_FLOW_BLIP
	g_structMatCBlipMP		theBlipDetails		= Gather_Blip_Details_For_Shared_Mission(theMissionIdData)
	g_structMatCCoronaMP	theCorona			= Gather_Corona_Details_For_Shared_Mission(theMissionIdData)
	g_structMatCOptionsMP	theOptions			= Gather_Optional_Flags_For_Shared_Mission(theMissionIdData)
	
	IF (Is_FM_Cloud_Loaded_Activity_A_Flow_Mission(theMissionIdData))
		PRINTLN("[LOWFLOW] ...KGM MP [MShared][CloudData][Heist]: Is_FM_Cloud_Loaded_Activity_A_Flow_Mission - theOptions.matcoDisplayNoBlip = TRUE, theOptions.matcoDisplayNoCorona = TRUE")
		theOptions.matcoDisplayNoBlip = TRUE
		theOptions.matcoDisplayNoCorona	= TRUE
		theOptions.matcoInviteOnly = TRUE
	ENDIF
	
	IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_PROFESSIONAL_JOB(Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(theMissionIdData))
		PRINTLN("[TS] ...KGM MP [MShared][CloudData][Heist]: Is_FM_Cloud_Loaded_Activity_A_Flow_Mission - theOptions.matcoDisplayNoBlip = TRUE, theOptions.matcoDisplayNoCorona = TRUE")
		theOptions.matcoDisplayNoBlip = TRUE
		theOptions.matcoDisplayNoCorona	= TRUE
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_HSW_RACE_SERIES(Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(theMissionIdData))
		PRINTLN("[TS] ...KGM MP [MShared][CloudData][HSW_RACE]: Is_FM_Cloud_Loaded_Activity_A_Flow_Mission - theOptions.matcoDisplayNoBlip = TRUE, theOptions.matcoDisplayNoCorona = TRUE")
		theOptions.matcoDisplayNoBlip = TRUE
		theOptions.matcoDisplayNoCorona	= TRUE
	ENDIF
	#ENDIF
	
	// Set the options flag that indicates this is being setup as a Shared Mission
	theOptions.matcoSharedMission = TRUE
	
	// If this Shared Mission data is being used only to trigger a mocap cutscene, then this needs stored
	// KGM 17/8/14: Don't think this is required, so changing it to the new variables
	IF (forHeistIntroCut)
		theOptions.matcoForCutsceneOnly = TRUE
	ENDIF
	
	IF (forHeistMidStrandCut)
		theOptions.matcoForCutsceneOnly		= TRUE
		theOptions.matcoHeistMidStrandCut	= TRUE
	ENDIF
	
	IF (forHeistTutorialCut)
		theOptions.matcoHeistTutorialCuts	= TRUE
	ENDIF

	INT atCoordsID = Register_MP_Mission_At_Coords(theSource, theCoords, theBlipDetails, theCorona, theOptions, theMissionIdData)
	
	IF (atCoordsID = ILLEGAL_AT_COORDS_ID)
		#IF IS_DEBUG_BUILD
			NET_PRINT("         FAILED TO REGISTER Shared Cloud-Loaded Mission with Missions At Coords system") NET_NL() NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Successfully registered
	g_MSCloudClientControl.mscccdMissions[paramArrayPos].msccdMatcRegID	= atCoordsID
	
	// If this mission is being 'played again' then temporarily unlock it
	IF (g_eUseOfCopyRegSharedMissionData = COPY_MISSION_USE_IS_PLAY_AGAIN)
		IF (ARE_STRINGS_EQUAL(g_sCopyRegSharedMissionData.crsmdHeaderData.tlName, theMissionIdData.idCloudFilename))
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MShared][CloudData]: This mission is being 'played again' so temporarily unlock it to avoid rank restrictions") NET_NL()
			#ENDIF
			
			Temporarily_Unlock_MissionsAtCoords_Mission_By_Filename(theMissionIdData.idCloudFilename)
		ENDIF
	ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	KGM 24/8/14 [BUG 1992201]: About to delete Shared Mission details on Matc, but they should be retained if a duplicate Shared Mission still requires the Matc registration
//
// INPUT PARAMS:			paramArrayPos			Shared Cloud-Loaded Missions array position
// RETURN VALUE:			BOOL					TRUE if the data needs to be retained
//
// NOTES:	KGM 18/11/14: Just realised this would never have worked! The missionID data is gone by the time it reaches here, only the MatcRegID remains.
FUNC BOOL Check_If_Shared_Mission_To_Delete_Is_Still_Needed_By_Another_Slot(INT paramArrayPos)

	INT		tempLoop			= 0
	INT		passedInMatcRegID	= g_MSCloudClientControl.mscccdMissions[paramArrayPos].msccdMatcRegID
	INT		checkMatcRegID		= ILLEGAL_AT_COORDS_ID

	IF (passedInMatcRegID = ILLEGAL_AT_COORDS_ID)
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [MShared][CloudData]: Check_If_Shared_Mission_To_Delete_Is_Still_Needed_By_Another_Slot() - ERROR: passed-in MatcRegID is ILLEGAL_AT_COORDS_ID")
			SCRIPT_ASSERT("Check_If_Shared_Mission_To_Delete_Is_Still_Needed_By_Another_Slot() - ERROR: passed-in MatcRegID is ILLEGAL_AT_COORDS_ID. Tell Keith")
		#ENDIF
		
		// The data is illegal, so need to return FALSE as not-needed
		RETURN FALSE
	ENDIF

	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [MShared][CloudData]: Delete Mision on Matc? Data Slot: ", paramArrayPos, ". MatcRegID: ", passedInMatcRegID)
	#ENDIF
	
	REPEAT MAX_MISSIONS_SHARED_CLOUD_DATA tempLoop
		IF (tempLoop != paramArrayPos)
			IF (Is_Shared_Cloud_Mission_Slot_In_Use(tempLoop))
				
				IF (Has_Client_Processed_This_Shared_Cloud_Loaded_Mission(tempLoop))
					IF (Has_Client_Added_This_Shared_Cloud_Loaded_Mission(tempLoop))
					AND NOT (Has_Client_Removed_This_Shared_Cloud_Loaded_Mission(tempLoop))
						// ...found another slot with registered data - check for duplicate data
						checkMatcRegID	= g_MSCloudClientControl.mscccdMissions[tempLoop].msccdMatcRegID
						
						// I think the regID at this stage should always contain valid data, so flag it if not
						#IF IS_DEBUG_BUILD
							IF (checkMatcRegID = ILLEGAL_AT_COORDS_ID)
								PRINTLN(".KGM [MShared][CloudData]: Check_If_Shared_Mission_To_Delete_Is_Still_Needed_By_Another_Slot() - ERROR: MatcRegID is ILLEGAL_AT_COORDS_ID. Slot: ", tempLoop)
								SCRIPT_ASSERT("Check_If_Shared_Mission_To_Delete_Is_Still_Needed_By_Another_Slot() - ERROR: MatcRegID is ILLEGAL_AT_COORDS_ID. Tell Keith")
							ENDIF
						#ENDIF
						
						#IF IS_DEBUG_BUILD
							PRINTLN(".KGM [MShared][CloudData][Debug]: Found Active data in Slot: ", tempLoop, ". MatcRegID: ", passedInMatcRegID)
						#ENDIF
						
						IF (checkMatcRegID != ILLEGAL_AT_COORDS_ID)
						AND (passedInMatcRegID = checkMatcRegID)
							// ...details match, so retain the Matc data
							#IF IS_DEBUG_BUILD		
								PRINTLN("...KGM MP [MShared][CloudData]: Shared Data is slot ", paramArrayPos, " is still required on MissionsAtCoords for Shared slot: ", tempLoop)
							#ENDIF
							
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Not found a duplicate mission, so data is not needed by another slot
PRINTLN(".KGM [MShared][CloudData][Debug]: Slot: ", tempLoop, " - delete")
	PRINTLN("...KGM MP [MShared][CloudData]: Shared Data is slot ", paramArrayPos, " is not required to remain on MissionsAtCoords for another slot - allow cleanup")
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// Delete this mission from the Missions At Coords system unless the data has become available locally since it was setup using Shared Mission data
//
// INPUT PARAMS:			paramArrayPos			Shared Cloud-Loaded Missions array position
PROC Delete_This_Shared_Mission_For_Local_Player_If_Not_Locally_Available(INT paramArrayPos)

	INT		missionsAtCoordsRegID	= g_MSCloudClientControl.mscccdMissions[paramArrayPos].msccdMatcRegID
	BOOL	fromSharedMission		= TRUE
	
	// KGM 3/6/14: Check if this is should be passed to the Heist intermediate routines rather than directly to MissionsAtCoords
	IF (missionsAtCoordsRegID < 0)
		PRINTLN("...KGM MP [MShared][CloudData][Heist]: Heist-related Mission. Passing to the Heist Routines to request delete.")
		
		INT heistRegID = missionsAtCoordsRegID - MISSIONS_SHARED_AT_COORDS_BASE_ID_FOR_HEISTS
		Delete_Other_Player_Active_Heist_Corona(heistRegID)
		
		EXIT
	ENDIF
		// FEATURE_HEIST_PLANNING
	
	// KGM 24/8/14 [BUG 1992201]: Don't attempt to delete the mission on MissionsAtCoords if another shared slot contains a duplicate mission - the other shared slot still needs the data
	IF (Check_If_Shared_Mission_To_Delete_Is_Still_Needed_By_Another_Slot(paramArrayPos))
		// There are two identical sets of mission data in the Shared Missions array, so the Matc registration is still required for the other Shared Mission
		// ...Don;t delete on Matc, but continue to clean up this Shared Mission array details
		EXIT
	ENDIF

	Mark_MP_Mission_At_Coords_For_Delete_If_Temporarily_Setup(missionsAtCoordsRegID, fromSharedMission)

ENDPROC




// ===========================================================================================================
//      EVENT BROADCAST AND PROCESS FUNCTIONS
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Event: Register MS Cloud Description Data
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventRegisterMSCloudDescriptionData
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	TEXT_LABEL_63				Description				// The Cloud Mission Description
	INT							SharedRegID				// The sharedRegID of the mission with additional data being registered
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Broadcast the Description cloud registration details for a Missions Shared Cloud-loaded mission
//
// RETURN PARAMS:			paramMissionDetails		The mission details that identify which cloud-loaded header data requires to be stored
PROC Broadcast_Register_MS_Cloud_Description_Data(MP_MISSION_ID_DATA &paramMissionDetails)
	
	// Ignore if the server already has these details
	IF (Is_Shared_Cloud_Mission_Description_Data_Stored(paramMissionDetails.idSharedRegID))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared][CloudData]: Ignoring Request to send MS Cloud Description Data - Server already has this data") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	// Store the Broadcast data
	m_structEventRegisterMSCloudDescriptionData Event
	
	Event.Details.Type				= SCRIPT_EVENT_REGISTER_MS_CLOUD_DESCRIPTION_DATA
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.Description				= Get_Mission_Description_For_FM_Cloud_Loaded_Activity(paramMissionDetails)
	Event.SharedRegID				= paramMissionDetails.idSharedRegID
	
	// Broadcast the event - TODO: THIS SHOULD GO TO THE HOST ONLY
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: ")	NET_PRINT_TIME() NET_PRINT(" Broadcast_Register_MS_Cloud_Description_Data")			NET_NL()
		NET_PRINT("      SharedRegID   : ") 			NET_PRINT_INT(paramMissionDetails.idSharedRegID)									NET_NL()
		NET_PRINT("      Description   : ") 			NET_PRINT(Event.Description)														NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process the registration of the Description cloud data for a shared mission
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Register_MS_Cloud_Description_Data(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MShared][CloudData]: Process_Event_Register_MS_Cloud_Description_Data - IGNORED (not the host)") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Register_MS_Cloud_Description_Data") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventRegisterMSCloudDescriptionData theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Register_MS_Cloud_Description_Data(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Output details
	#IF IS_DEBUG_BUILD
		NET_PRINT("      From Player   : ") 			NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))			NET_NL()
		NET_PRINT("      SharedRegID   : ") 			NET_PRINT_INT(theEventData.SharedRegID)										NET_NL()
		NET_PRINT("      Description   : ") 			NET_PRINT(theEventData.Description)											NET_NL()
	#ENDIF
	
	// Fill the data if the slot is still in use
	INT theSlot = theEventData.SharedRegID
	
	IF NOT (Is_Shared_Cloud_Mission_Slot_In_Use(theSlot))
		#IF IS_DEBUG_BUILD
			NET_PRINT("         BUT: The slot is not in use. Ignoring data.") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.tlMissionDec		= theEventData.Description
	
	// Set the data stored control bits
	Set_Shared_Cloud_Mission_Description_Data_Stored(theSlot)
	Set_All_Shared_Cloud_Mission_Data_Stored_If_All_Data_Stored(theSlot)

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Register MS Cloud Names Data
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventRegisterMSCloudNamesData
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	TEXT_LABEL_63				MissionName				// The Cloud Mission Name
	TEXT_LABEL_23				UserName				// The Cloud User Name
	INT							SharedRegID				// The sharedRegID of the mission with additional data being registered
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Broadcast the Names cloud registration details for a Missions Shared Cloud-loaded mission
//
// RETURN PARAMS:			paramMissionDetails		The mission details that identify which cloud-loaded header data requires to be stored
PROC Broadcast_Register_MS_Cloud_Names_Data(MP_MISSION_ID_DATA &paramMissionDetails)
	
	// Ignore if the server already has these details
	IF (Is_Shared_Cloud_Mission_Names_Data_Stored(paramMissionDetails.idSharedRegID))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared][CloudData]: Ignoring Request to send MS Cloud Names Data - Server already has this data") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	// Store the Broadcast data
	m_structEventRegisterMSCloudNamesData Event
	
	Event.Details.Type				= SCRIPT_EVENT_REGISTER_MS_CLOUD_NAMES_DATA
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.MissionName				= Get_Mission_Name_For_FM_Cloud_Loaded_Activity(paramMissionDetails)
	Event.UserName					= Get_User_Name_For_FM_Cloud_Loaded_Activity(paramMissionDetails)
	Event.SharedRegID				= paramMissionDetails.idSharedRegID
	
	// Broadcast the event - TODO: THIS SHOULD GO TO THE HOST ONLY
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: ")	NET_PRINT_TIME() NET_PRINT(" Broadcast_Register_MS_Cloud_Names_Data")			NET_NL()
		NET_PRINT("      SharedRegID   : ") 			NET_PRINT_INT(paramMissionDetails.idSharedRegID)								NET_NL()
		NET_PRINT("      Mission Name  : ") 			NET_PRINT(Event.MissionName)													NET_NL()
		NET_PRINT("      User Name     : ") 			NET_PRINT(Event.UserName)														NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process the registration of the Names cloud data for a shared mission
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Register_MS_Cloud_Names_Data(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MShared][CloudData]: Process_Event_Register_MS_Cloud_Names_Data - IGNORED (not the host)") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Register_MS_Cloud_Names_Data") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventRegisterMSCloudNamesData theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Register_MS_Cloud_Names_Data(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Output details
	#IF IS_DEBUG_BUILD
		NET_PRINT("      From Player   : ") 			NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))				NET_NL()
		NET_PRINT("      SharedRegID   : ") 			NET_PRINT_INT(theEventData.SharedRegID)											NET_NL()
		NET_PRINT("      Mission Name  : ") 			NET_PRINT(theEventData.MissionName)												NET_NL()
		NET_PRINT("      User Name     : ") 			NET_PRINT(theEventData.UserName)												NET_NL()
	#ENDIF
	
	// Fill the data if the slot is still in use
	INT theSlot = theEventData.SharedRegID
	
	IF NOT (Is_Shared_Cloud_Mission_Slot_In_Use(theSlot))
		#IF IS_DEBUG_BUILD
			NET_PRINT("         BUT: The slot is not in use. Ignoring data.") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.tlMissionName		= theEventData.MissionName
	GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.tlOwner			= theEventData.UserName
	
	// Set the data stored control bits
	Set_Shared_Cloud_Mission_Names_Data_Stored(theSlot)
	Set_All_Shared_Cloud_Mission_Data_Stored_If_All_Data_Stored(theSlot)

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Register MS Cloud Details Data
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventRegisterMSCloudDetailsData
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	INT							MissionType				// The Cloud Mission Type
	INT							MissionSubtype			// The Cloud Mission Subtype
	INT							MinPlayers				// The Cloud Mission Minimum Players
	INT							MaxPlayers				// The Cloud Mission Maximum Players
	INT							MissionRank				// The Cloud Mission Rank
	VECTOR						CameraCoords			// The Cloud Mission Camera Position
	VECTOR						CameraHeading			// The Cloud Mission Camera Heading
	INT							UserRating				// The Cloud Mission User Rating
	INT							OptionsBitfield			// The Cloud Mission Options Bitfield
	INT							ContactOnCloud			// The Cloud Mission Contact (as read from the cloud - not processed)
	INT							OnHours					// The Cloud Mission On Hours (as a bitfield)
	INT							MissionDescHash			// The Cloud Mission Hash value for Description retrieval
	INT							SharedRegID				// The sharedRegID of the mission with additional data being registered
	
	#IF IS_DEBUG_BUILD
		INT						LesRating				// The Cloud Mission Les Rating
	#ENDIF
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Broadcast the Details cloud registration details for a Missions Shared Cloud-loaded mission
//
// REFERENCE PARAMS:		refMissionIdData		The mission details that identify which cloud-loaded header data requires to be stored
PROC Broadcast_Register_MS_Cloud_Details_Data(MP_MISSION_ID_DATA &refMissionIdData)
	
	// Ignore if the server already has these details
	IF (Is_Shared_Cloud_Mission_Details_Data_Stored(refMissionIdData.idSharedRegID))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared][CloudData]: Ignoring Request to send MS Cloud Details Data - Server already has this data") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	// Store the Broadcast data
	m_structEventRegisterMSCloudDetailsData Event
	
	Event.Details.Type				= SCRIPT_EVENT_REGISTER_MS_CLOUD_DETAILS_DATA
	Event.Details.FromPlayerIndex	= PLAYER_ID()
	
	Event.MissionType				= Get_FM_Mission_Type_For_FM_Cloud_Loaded_Activity(refMissionIdData.idCreator, refMissionIdData.idVariation)
	Event.MissionSubtype			= Get_SubType_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	Event.MinPlayers				= Get_Minimum_Players_Required_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	Event.MaxPlayers				= Get_Maximum_Players_Required_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	Event.MissionRank				= Get_Rank_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	Event.CameraCoords				= Get_Cam_Vector_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	Event.CameraHeading				= Get_Cam_Heading_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	Event.UserRating				= Get_User_Rating_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	Event.OptionsBitfield			= Get_Options_Bitfield_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	Event.ContactOnCloud			= Get_Contact_Value_As_Read_From_Cloud_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	Event.OnHours					= Get_On_Hours_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	Event.MissionDescHash			= Get_Mission_Dec_Hash_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	Event.SharedRegID				= refMissionIdData.idSharedRegID
	
	#IF IS_DEBUG_BUILD
		Event.LesRating				= Get_Les_Rating_For_FM_Cloud_Loaded_Activity(refMissionIdData)
	#ENDIF
	
	// Broadcast the event - TODO: THIS SHOULD GO TO THE HOST ONLY
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())

	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63	theHoursDescription	= Convert_Hours_From_Bitfield_To_TL(Event.OnHours)
		INT				theCharacterID		= Get_enumCharacterListAsInt_From_ContactAsInt(Event.ContactOnCloud)
	
		NET_PRINT("...KGM MP [MShared][CloudData]: ")	NET_PRINT_TIME() NET_PRINT(" Broadcast_Register_MS_Cloud_Details_Data")																							NET_NL()
		NET_PRINT("      SharedRegID     : ") 			NET_PRINT_INT(refMissionIdData.idSharedRegID)																													NET_NL()
		NET_PRINT("      FM MissionType  : ") 			NET_PRINT(GET_MP_MISSION_NAME(Convert_FM_Mission_Type_To_MissionID(Event.MissionType)))																			NET_NL()
		NET_PRINT("      Subtype         : ") 			NET_PRINT_INT(Event.MissionSubtype)																																NET_NL()
		NET_PRINT("      Min Players     : ") 			NET_PRINT_INT(Event.MinPlayers)																																	NET_NL()
		NET_PRINT("      Max Players     : ") 			NET_PRINT_INT(Event.MaxPlayers)																																	NET_NL()
		NET_PRINT("      Rank            : ") 			NET_PRINT_INT(Event.MissionRank)																																NET_NL()
		NET_PRINT("      Camera Coords   : ") 			NET_PRINT_VECTOR(Event.CameraCoords) NET_PRINT(" [Heading: ") NET_PRINT_VECTOR(Event.CameraHeading) NET_PRINT("]")												NET_NL()
		NET_PRINT("      User Rating     : ") 			NET_PRINT_INT(Event.UserRating)																																	NET_NL()
		NET_PRINT("      Options Bitset  : ") 			NET_PRINT_INT(Event.OptionsBitfield)																															NET_NL()
		NET_PRINT("      Contact         : ") 			NET_PRINT_INT(Event.ContactOnCloud) NET_PRINT(": ") NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sCharacterSheetAll[theCharacterID].label))	NET_NL()
		NET_PRINT("      On Hours        : ")			NET_PRINT(theHoursDescription)																																	NET_NL()
		NET_PRINT("      Description Hash: ") 			NET_PRINT_INT(Event.MissionDescHash)																															NET_NL()
		NET_PRINT("      Les Rating      : ") 			NET_PRINT_INT(Event.LesRating)																																	NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process the registration of the Details cloud data for a shared mission
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Register_MS_Cloud_Details_Data(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MShared][CloudData]: Process_Event_Register_MS_Cloud_Details_Data - IGNORED (not the host)") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Register_MS_Cloud_Details_Data") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventRegisterMSCloudDetailsData theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Register_MS_Cloud_Details_Data(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Output details
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63	theHoursDescription = Convert_Hours_From_Bitfield_To_TL(theEventData.OnHours)
		INT				theCharacterID		= Get_enumCharacterListAsInt_From_ContactAsInt(theEventData.ContactOnCloud)
	
		NET_PRINT("      From Player     : ") 			NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))																											NET_NL()
		NET_PRINT("      SharedRegID     : ") 			NET_PRINT_INT(theEventData.SharedRegID)																																		NET_NL()
		NET_PRINT("      FM MissionType  : ") 			NET_PRINT(GET_MP_MISSION_NAME(Convert_FM_Mission_Type_To_MissionID(theEventData.MissionType)))																				NET_NL()
		NET_PRINT("      Subtype         : ") 			NET_PRINT_INT(theEventData.MissionSubtype)																																	NET_NL()
		NET_PRINT("      Min Players     : ") 			NET_PRINT_INT(theEventData.MinPlayers)																																		NET_NL()
		NET_PRINT("      Max Players     : ") 			NET_PRINT_INT(theEventData.MaxPlayers)																																		NET_NL()
		NET_PRINT("      Rank            : ") 			NET_PRINT_INT(theEventData.MissionRank)																																		NET_NL()
		NET_PRINT("      Camera Coords   : ") 			NET_PRINT_VECTOR(theEventData.CameraCoords) NET_PRINT(" [Heading: ") NET_PRINT_VECTOR(theEventData.CameraHeading) NET_PRINT("]")											NET_NL()
		NET_PRINT("      User Rating     : ") 			NET_PRINT_INT(theEventData.UserRating)																																		NET_NL()
		NET_PRINT("      Options Bitset  : ") 			NET_PRINT_INT(theEventData.OptionsBitfield)																																	NET_NL()
		NET_PRINT("      Contact         : ") 			NET_PRINT_INT(theEventData.ContactOnCloud) NET_PRINT(": ") NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sCharacterSheetAll[theCharacterID].label))							NET_NL()
		NET_PRINT("      On Hours        : ")			NET_PRINT(theHoursDescription)																																				NET_NL()
		NET_PRINT("      Description Hash: ") 			NET_PRINT_INT(theEventData.MissionDescHash)																																	NET_NL()
		NET_PRINT("      Les Rating      : ") 			NET_PRINT_INT(theEventData.LesRating)																																		NET_NL()
	#ENDIF
	
	// Fill the data if the slot is still in use
	INT theSlot = theEventData.SharedRegID
	
	IF NOT (Is_Shared_Cloud_Mission_Slot_In_Use(theSlot))
		#IF IS_DEBUG_BUILD
			NET_PRINT("         BUT: The slot is not in use. Ignoring data.") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.iType				= theEventData.MissionType
	GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.iSubType			= theEventData.MissionSubtype
	GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.iMinPlayers		= theEventData.MinPlayers
	GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.iMaxPlayers		= theEventData.MaxPlayers
	GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.iRank				= theEventData.MissionRank
	GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.vCamPos			= theEventData.CameraCoords
	GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.vCamHead			= theEventData.CameraHeading
	GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.iRating			= theEventData.UserRating
	GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.iBitSet			= theEventData.OptionsBitfield
	GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.iContactCharEnum	= theEventData.ContactOnCloud
	GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.iLaunchTimesBit	= theEventData.OnHours
	GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.iMissionDecHash	= theEventData.MissionDescHash
	GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.iLesRating		= -1
	
	#IF IS_DEBUG_BUILD
		GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.iLesRating	= theEventData.LesRating
	#ENDIF
	
	// Set the data stored control bits
	Set_Shared_Cloud_Mission_Details_Data_Stored(theSlot)
	Set_All_Shared_Cloud_Mission_Data_Stored_If_All_Data_Stored(theSlot)

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Request Full MS Cloud Data
// -----------------------------------------------------------------------------------------------------------

STRUCT m_structEventRequestFullMSCloudData
	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
	MP_MISSION_ID_DATA			MissionIdData			// Mission Identification data
	INT							UniqueID				// Mission Controller UniqueID for this launch instance of the mission
ENDSTRUCT


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Broadcast the request for full Missions Shared Cloud data
//
// INPUT PARAMS:			paramUniqueID			Mission Controller UnqiueID for this launch instance of the mission
//							paramAllPlayers			TRUE if the request goes to all players, FALSE if to a specific player
//							paramPlayerID			The player ID for a request to a specific player
// RETURN PARAMS:			paramMissionIdData		The Mission Identification Data
PROC Broadcast_Request_Full_MS_Cloud_Data(MP_MISSION_ID_DATA &paramMissionIdData, INT paramUniqueID, BOOL paramAllPlayers, PLAYER_INDEX paramPlayerID)
	
	// Store the Broadcast data
	m_structEventRequestFullMSCloudData Event
	
	Event.Details.Type					= SCRIPT_EVENT_REQUEST_FULL_MS_CLOUD_DATA
	Event.Details.FromPlayerIndex		= PLAYER_ID()
	
	Event.MissionIdData					= paramMissionIdData
	Event.UniqueID						= paramUniqueID
	
	INT playerBits = ALL_PLAYER_BITS_CLEAR
	IF NOT (paramAllPlayers)
		// ...sending request to a specific player, but if player is not OK send to all
		IF (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
			SET_BIT(playerBits, NATIVE_TO_INT(paramPlayerID))
		ELSE
			paramAllPlayers = TRUE
		ENDIF
	ENDIF
	
	IF (paramAllPlayers)
		playerBits = ALL_PLAYERS()
	ENDIF
	
	// Broadcast the event - TODO: THIS SHOULD GO TO THE HOST ONLY
	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), playerBits)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: ")
		NET_PRINT_TIME()
		NET_PRINT(" (SERVER) Broadcast_Request_Full_MS_Cloud_Data() for slot: ")
		NET_PRINT_INT(paramMissionIdData.idSharedRegID)
		IF (paramAllPlayers)
			NET_PRINT(" [TO ALL PLAYERS]")
		ELSE
			NET_PRINT(" [TO ")
			NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
			NET_PRINT("]")
		ENDIF
		NET_PRINT(" - Player Bits: ")
		NET_PRINT_INT(playerBits)
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process a request for full mission details
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Request_Full_MS_Cloud_Data(INT paramEventID)
	
	#IF IS_DEBUG_BUILD
		NET_NL()
		NET_PRINT("...KGM MP [MShared][CloudData]: ")
		NET_PRINT_TIME()
		NET_PRINT(" Process_Event_Request_Full_MS_Cloud_Data() received by: ")
		NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
		NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventRequestFullMSCloudData theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Request_Full_MS_Cloud_Data(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Output details
	#IF IS_DEBUG_BUILD
		NET_PRINT("      MissionID      : ") 			NET_PRINT(GET_MP_MISSION_NAME(theEventData.MissionIdData.idMission))												NET_NL()
		NET_PRINT("      Variation      : ")			NET_PRINT_INT(theEventData.MissionIdData.idVariation)																NET_NL()
		NET_PRINT("      CreatorID      : ")			NET_PRINT(Convert_CreatorID_To_Creator_Name_For_FM_Cloud_Loaded_Activity(theEventData.MissionIdData))				NET_NL()
		NET_PRINT("      Cloud Filename : ")			NET_PRINT(theEventData.MissionIdData.idCloudFilename)																NET_NL()
		NET_PRINT("      Shared RegID   : ")			NET_PRINT_INT(theEventData.MissionIdData.idSharedRegID)																NET_NL()
		NET_PRINT("      UniqueID       : ")			NET_PRINT_INT(theEventData.UniqueID)																				NET_NL()
	#ENDIF
	
	// Check if this mission still requires cloud data
	INT existingSlot = Get_Slot_For_Existing_Shared_Cloud_Mission_With_These_Details(theEventData.MissionIdData.idCloudFilename, theEventData.UniqueID)
	IF (existingSlot != ILLEGAL_ARRAY_POSITION)
		// ...found the specified mission, so check if it already has all the data it needs
		IF (Is_All_Shared_Cloud_Mission_Data_Stored(existingSlot))
			// ...this cloud mission has all the data it needs
			#IF IS_DEBUG_BUILD
				NET_PRINT("         IGNORING REQUEST: server already has full details for this mission: ") NET_PRINT(theEventData.MissionIdData.idCloudFilename) NET_NL()
			#ENDIF
			
			EXIT
		ENDIF
	ENDIF
	
	// Check If this machine has these cloud-loaded header details available
	// NOTE: This may update the variation parameter if the details are in a different arraypos
	IF NOT (Are_These_FM_Cloud_Loaded_Activity_Details_Available_Locally(theEventData.MissionIdData))
		// ...the details are not locally available, so nothing else to broadcast here
		#IF IS_DEBUG_BUILD
			NET_PRINT("         IGNORING REQUEST: the full details for this mission are not available locally: ") NET_PRINT(theEventData.MissionIdData.idCloudFilename) NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Send the data - this needs to be split over three broadcasts because of the size of the content
	#IF IS_DEBUG_BUILD
		NET_PRINT("         DATA REQUIRED FROM THIS PLAYER: because server does not have full details for this mission: ") NET_PRINT(theEventData.MissionIdData.idCloudFilename) NET_NL()
	#ENDIF

	Broadcast_Register_MS_Cloud_Description_Data(theEventData.MissionIdData)
	Broadcast_Register_MS_Cloud_Names_Data(theEventData.MissionIdData)
	Broadcast_Register_MS_Cloud_Details_Data(theEventData.MissionIdData)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To gather full cloud mission details for a Cloud Shared Mission if needed
//
// INPUT PARAMS:		paramSlot				Cloud Shared Mission slot
//						paramSenderHasData		TRUE if the sender has the data locally, FALSE if not
//						paramSenderID			PlayerID of the registering player (the sender)
// RETURN PARAMS:		paramMissionIdData		The MissionID Data (including correct sharedRegID)
//
// NOTES:	If full data is still required, first check if the data is on the server's machine, if not, check if the
//				registering player had the data locally, if not, send the request to all players
PROC Gather_Full_Shared_Cloud_Mission_Details_For_Slot(INT paramSlot, BOOL paramSenderHasData, PLAYER_INDEX paramSenderID, MP_MISSION_ID_DATA &paramMissionIdData)
	
	// If these cloud details are available locally on this machine then fill the Cloud Shared Array Slot
	IF (Are_These_FM_Cloud_Loaded_Activity_Details_Available_Locally(paramMissionIdData))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared][CloudData]: Gather_Full_Shared_Cloud_Mission_Details_For_Slot(): the details are available locally on the server's machine. Slot: ") NET_PRINT_INT(paramSlot) NET_NL()
		#ENDIF
			
		// Fill the data still required (cloudFilename and startPos are now classed as basic data and pre-filled)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iType				= Get_FM_Mission_Type_For_FM_Cloud_Loaded_Activity(paramMissionIdData.idCreator, paramMissionIdData.idVariation)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlOwner				= Get_User_Name_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlMissionName		= Get_Mission_Name_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlMissionDec		= Get_Mission_Description_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.vCamPos				= Get_Cam_Vector_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.vCamHead			= Get_Cam_Heading_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iSubType			= Get_SubType_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iMinPlayers			= Get_Minimum_Players_Required_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iRank				= Get_Rank_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iMaxPlayers			= Get_Maximum_Players_Required_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iRating				= Get_User_Rating_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iContactCharEnum	= Get_Contact_Value_As_Read_From_Cloud_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iBitSet				= Get_Options_Bitfield_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iLaunchTimesBit		= Get_On_Hours_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
		GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iMissionDecHash		= Get_Mission_Dec_Hash_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
		
		#IF IS_DEBUG_BUILD
			GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iLesRating		= Get_Les_Rating_For_FM_Cloud_Loaded_Activity(paramMissionIdData)
		#ENDIF
	
		// Output details
		#IF IS_DEBUG_BUILD
			TEXT_LABEL_63	theHoursDescription = Convert_Hours_From_Bitfield_To_TL(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iLaunchTimesBit)
			INT				theContactOnCloud	= GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iContactCharEnum
			INT				theCharacterID		= Get_enumCharacterListAsInt_From_ContactAsInt(theContactOnCloud)
	
			NET_PRINT("      Mission Name    : ") 			NET_PRINT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlMissionName)																		NET_NL()
			NET_PRINT("      User Name       : ") 			NET_PRINT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlOwner)																				NET_NL()
			NET_PRINT("      Description     : ") 			NET_PRINT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlMissionDec)																			NET_NL()
			NET_PRINT("      FM MissionType  : ") 			NET_PRINT(GET_MP_MISSION_NAME(Convert_FM_Mission_Type_To_MissionID(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iType)))						NET_NL()
			NET_PRINT("      Subtype         : ") 			NET_PRINT_INT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iSubType)																			NET_NL()
			NET_PRINT("      Min Players     : ") 			NET_PRINT_INT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iMinPlayers)																		NET_NL()
			NET_PRINT("      Max Players     : ") 			NET_PRINT_INT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iMaxPlayers)																		NET_NL()
			NET_PRINT("      Rank            : ") 			NET_PRINT_INT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iRank)																			NET_NL()
			NET_PRINT("      Camera Coords   : ") 			NET_PRINT_VECTOR(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.vCamPos)
			NET_PRINT(" [Heading: ")						NET_PRINT_VECTOR(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.vCamHead) NET_PRINT("]")														NET_NL()
			NET_PRINT("      User Rating     : ") 			NET_PRINT_INT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iRating)																			NET_NL()
			NET_PRINT("      Options Bitset  : ") 			NET_PRINT_INT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iBitSet)																			NET_NL()
			NET_PRINT("      Contact         : ") 			NET_PRINT_INT(theContactOnCloud) NET_PRINT(": ") NET_PRINT(GET_STRING_FROM_TEXT_FILE(g_sCharacterSheetAll[theCharacterID].label))	NET_NL()
			NET_PRINT("      On Hours        : ")			NET_PRINT(theHoursDescription)																																		NET_NL()
			NET_PRINT("      Description Hash: ")			NET_PRINT_INT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iMissionDecHash)																	NET_NL()
			NET_PRINT("      Les Rating      : ") 			NET_PRINT_INT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iLesRating)																		NET_NL()
		#ENDIF
		
		// Set the data stored control bits
		Set_Shared_Cloud_Mission_Description_Data_Stored(paramSlot)
		Set_Shared_Cloud_Mission_Names_Data_Stored(paramSlot)
		Set_Shared_Cloud_Mission_Details_Data_Stored(paramSlot)
		Set_All_Shared_Cloud_Mission_Data_Stored_If_All_Data_Stored(paramSlot)
		
		EXIT
	ENDIF
	
	// These details are not stored locally
	BOOL	requestFromAllPlayers		= FALSE
	INT		missionControllerUniqueID	= GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscUniqueID
	
	// Does the registering player have the details on their local machine?
	IF (paramSenderHasData)
		// ...yes, so request the details specifically from the registering player
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared][CloudData]: Gather_Full_Shared_Cloud_Mission_Details_For_Slot(): the details are available on the sender's machine, so request them.") NET_NL()
		#ENDIF
			
		requestFromAllPlayers = FALSE
		Broadcast_Request_Full_MS_Cloud_Data(paramMissionIdData, missionControllerUniqueID, requestFromAllPlayers, paramSenderID)
		
		EXIT
	ENDIF
	
	// The registering player doesn't have them, so send the request to everyone
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: Gather_Full_Shared_Cloud_Mission_Details_For_Slot(): send a general request to retrieve the details.") NET_NL()
	#ENDIF
			
	requestFromAllPlayers = TRUE
	Broadcast_Request_Full_MS_Cloud_Data(paramMissionIdData, missionControllerUniqueID, requestFromAllPlayers, INVALID_PLAYER_INDEX())

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Register MS Cloud Basic Data
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process the registration of the basic cloud data for a shared mission
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Register_MS_Cloud_Basic_Data(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MShared][CloudData]: Process_Event_Register_MS_Cloud_Basic_Data - IGNORED (not the host)") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Register_MS_Cloud_Basic_Data") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventRegisterMSCloudBasicData theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Register_MS_Cloud_Basic_Data(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Ensure the player is still ok
	IF NOT (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex))
		#IF IS_DEBUG_BUILD
			NET_PRINT("         BUT: Registering Player Is Not OK. Quitting Registration.") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Output details
	#IF IS_DEBUG_BUILD
		NET_PRINT("      From Player          : ") 			NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))													NET_NL()
		NET_PRINT("      UniqueID             : ") 			NET_PRINT_INT(theEventData.UniqueID)																				NET_NL()
		NET_PRINT("      MissionID            : ") 			NET_PRINT(GET_MP_MISSION_NAME(theEventData.MissionIdData.idMission))												NET_NL()
		NET_PRINT("      Variation            : ")			NET_PRINT_INT(theEventData.MissionIdData.idVariation)																NET_NL()
		NET_PRINT("      CreatorID            : ")			NET_PRINT(Convert_CreatorID_To_Creator_Name_For_FM_Cloud_Loaded_Activity(theEventData.MissionIdData))				NET_NL()
		NET_PRINT("      Cloud Filename       : ")			NET_PRINT(theEventData.MissionIdData.idCloudFilename)																NET_NL()
		NET_PRINT("      Shared RegID         : ")			NET_PRINT_INT(theEventData.MissionIdData.idSharedRegID)																NET_NL()
		NET_PRINT("      Location             : ")			NET_PRINT_VECTOR(theEventData.Location)																				NET_NL()
		NET_PRINT("      Sender Has Data      : ")
		IF (theEventData.SenderHasData)
			NET_PRINT("[TRUE] - Mission Data is available on sender's machine") NET_NL()
		ELSE
			NET_PRINT("[FALSE] - Mission Data is not available on sender's machine") NET_NL()
		ENDIF
		NET_PRINT("      For Cutscene         : ")
		IF (theEventData.ForCutsceneOnly)
			NET_PRINT("[TRUE] - Corona is setup to trigger a Mocap cutscene only, not a mission (ie: Heist Pre-Planning)") NET_NL()
		ELSE
			NET_PRINT("[FALSE]") NET_NL()
		ENDIF
		NET_PRINT("      Heist Mid-Strand Cut : ")
		IF (theEventData.HeistMidStrandCut)
			NET_PRINT("[TRUE] - Cutscene Corona is for Heist Mid-Strand Cutscene") NET_NL()
		ELSE
			NET_PRINT("[FALSE]") NET_NL()
		ENDIF
		NET_PRINT("      Heist Tutorial Cut   : ")
		IF (theEventData.HeistTutorialCut)
			NET_PRINT("[TRUE] - Cutscene Corona is for Heist Tutorial Cutscene") NET_NL()
		ELSE
			NET_PRINT("[FALSE]") NET_NL()
		ENDIF
	#ENDIF
	
	// Player should be registered with one mission only, so check if already registered for a mission
	INT existingPlayerSlot = Get_Players_Registered_Shared_Cloud_Mission_Slot(theEventData.Details.FromPlayerIndex)
	
	IF (existingPlayerSlot != ILLEGAL_ARRAY_POSITION)
		IF (Is_Player_Registered_For_Shared_Cloud_Mission_With_These_Details(theEventData.Details.FromPlayerIndex, theEventData.MissionIdData.idCloudFilename, theEventData.UniqueID))
			// Player is trying to re-register for the same mission
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MShared][CloudData]: Process_Event_Register_MS_Cloud_Basic_Data() - IGNORING: Player is already registered for this mission") NET_NL()
			#ENDIF
			
			EXIT
		ENDIF
		
		// Player is trying to register for another mission - this can only be because the 'unregister' event was missed (perhaps during a period of no-host)
		// So allow this request to replace the old request
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared][CloudData]: Process_Event_Register_MS_Cloud_Basic_Data() - ERROR: Player is already registered for a different mission. So unregister and continue.") NET_NL()
		#ENDIF
	
		// Unregister from existing mission
		Remove_Player_As_Registered_With_Shared_Cloud_Mission(existingPlayerSlot, theEventData.Details.FromPlayerIndex)
			
		#IF IS_DEBUG_BUILD
			NET_PRINT("      Updated Array Details") NET_NL()
			Debug_Output_Missions_Shared_Cloud_Data_Array()
		#ENDIF
	ENDIF
	
	// Find an existing mission with these details
	INT theSlot = Get_Slot_For_Existing_Shared_Cloud_Mission_With_These_Details(theEventData.MissionIdData.idCloudFilename, theEventData.UniqueID)
	
	// DEBUG Quick Check to display a console log warning, just in case it highlights a problem
	#IF IS_DEBUG_BUILD
		IF (theEventData.MissionIdData.idSharedRegID != ILLEGAL_ARRAY_POSITION)
			IF NOT (theSlot = theEventData.MissionIdData.idSharedRegID)
				NET_PRINT("...KGM MP [MShared][CloudData]: Process_Event_Register_MS_Cloud_Basic_Data() - INFO: the passed-in RegID differs from the found slot") NET_NL()
				NET_PRINT("      Passed-In RegID: ") NET_PRINT_INT(theEventData.MissionIdData.idSharedRegID) NET_NL()
				NET_PRINT("      Details In Slot: ") NET_PRINT_INT(theSlot) NET_PRINT(" - this should most likely be the same, although ignorable if not") NET_NL()
				NET_PRINT("      FOR INFO: CURRENT ARRAY CONTENTS:") NET_NL()
				Debug_Output_Missions_Shared_Cloud_Data_Array()
			ENDIF
		ENDIF
	#ENDIF
	
	IF (theSlot = ILLEGAL_ARRAY_POSITION)
		// Store the Mission Details in a new slot
		theSlot = Find_Unused_Shared_Cloud_Mission_Slot()
		
		// If there is no free slot then the array must be full
		IF (theSlot = ILLEGAL_ARRAY_POSITION)
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MShared][CloudData]: Process_Event_Register_MS_Cloud_Basic_Data() - ERROR: Failed to find a free slot.") NET_NL()
				SCRIPT_ASSERT("Process_Event_Register_MS_Cloud_Basic_Data(): ERROR - Failed to find a free slot. Quitting Request. Tell Keith.")
			#ENDIF
			
			EXIT
		ENDIF
		
		// Store the Basic mission details - this includes the mission's setup location (which could differ from the downloaded cloud location)
		GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscInUse				= TRUE
		GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscBeenUsed				= TRUE
		GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscUniqueID				= theEventData.UniqueID
		GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscMissionID			= theEventData.MissionIdData.idMission
		GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscVariation			= theEventData.MissionIdData.idVariation
		GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscCreatorID			= theEventData.MissionIdData.idCreator
		GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.tlName	= theEventData.MissionIdData.idCloudFilename
		GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscHeaderData.vStartPos	= theEventData.Location
		
		// ...determine the ForCutsceneID
		GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscForCutsceneID	= FOR_CUT_NONE
		IF (theEventData.ForCutsceneOnly)
			GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscForCutsceneID	= FOR_CUT_HEIST_INTRO
			
			IF (theEventData.HeistMidStrandCut)
				GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscForCutsceneID	= FOR_CUT_HEIST_MID_STRAND
			ENDIF
			
			IF (theEventData.HeistTutorialCut)
				GlobalServerBD_MissionsShared.cloudDetails[theSlot].mscForCutsceneID	= FOR_CUT_HEIST_TUTORIAL_INTRO
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared][CloudData]: Basic Shared Cloud Mission Details are being stored in slot: ") NET_PRINT_INT(theSlot) NET_NL()
		#ENDIF
	ENDIF
	
	// Add this player to the mission
	Add_Player_As_Registered_With_Shared_Cloud_Mission(theSlot, theEventData.Details.FromPlayerIndex)
		
	#IF IS_DEBUG_BUILD
		NET_PRINT("   Updated Array Details") NET_NL()
		Debug_Output_Missions_Shared_Cloud_Data_Array()
	#ENDIF
	
	// Gather the rest of the mission details if the details aren't already stored
	IF (Is_All_Shared_Cloud_Mission_Data_Stored(theSlot))
		EXIT
	ENDIF

	MP_MISSION_ID_DATA	theUpdatedMissionIdData
	Clear_MP_MISSION_ID_DATA_Struct(theUpdatedMissionIdData)
	
	theUpdatedMissionIdData					= theEventData.MissionIdData
	theUpdatedMissionIdData.idSharedRegID	= theSlot
	
	Gather_Full_Shared_Cloud_Mission_Details_For_Slot(theSlot, theEventData.SenderHasData, theEventData.Details.FromPlayerIndex, theUpdatedMissionIdData)
		
	// Update the Mission Controller details with the cloud header data
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: Broadcasting the SharedRegID to the Mission Controller.") NET_NL()
	#ENDIF
		
	BOOL actingAsScriptHost = TRUE
	Broadcast_Change_Reserved_Mission_Details(theEventData.UniqueID, theUpdatedMissionIdData, actingAsScriptHost)

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Event: Unregister MS Cloud Mission
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Process the request to unregister the player from a Missions Shared Cloud-loaded mission
//
// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Unregister_MS_Cloud_Mission(INT paramEventID)

	IF NOT (NETWORK_IS_HOST_OF_THIS_SCRIPT())
		#IF IS_DEBUG_BUILD
			NET_NL()
			NET_PRINT("...KGM MP [MShared][CloudData]: Process_Event_Unregister_MS_Cloud_Mission - IGNORED (not the host)") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_NL() NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Process_Event_Unregister_MS_Cloud_Mission") NET_NL()
	#ENDIF
	
	// Retrieve the data associated with the event
	m_structEventUnregisterMSCloudMission theEventData
	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
		SCRIPT_ASSERT("Process_Event_Unregister_MS_Cloud_Mission(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
		EXIT
	ENDIF
	
	// Output details
	#IF IS_DEBUG_BUILD
		NET_PRINT("      From Player    : ") 			NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex))					NET_NL()
		NET_PRINT("      UniqueID       : ") 			NET_PRINT_INT(theEventData.UniqueID)												NET_NL()
		NET_PRINT("      Cloud Filename : ")			NET_PRINT(theEventData.CloudFilename)												NET_NL()
		NET_PRINT("      Shared RegID   : ")			NET_PRINT_INT(theEventData.SharedRegID)												NET_NL()
	#ENDIF
	
	// If player is no longer in the game then the unregistration is automatic, so ditch this request
	IF NOT (IS_NET_PLAYER_OK(theEventData.Details.FromPlayerIndex, FALSE))
		#IF IS_DEBUG_BUILD
			NET_PRINT("         - IGNORING: Player is not OK and will be automatically unregistered for shared missions.") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Player should be registered with one mission only, so ensure player registered for a mission
	IF NOT (Is_Player_Registered_For_Any_Mission(theEventData.Details.FromPlayerIndex))
		#IF IS_DEBUG_BUILD
			NET_PRINT("         - IGNORING: Player is not registered for any shared mission.") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	// Check if the Shared RegID is still valid
	INT theSlot = ILLEGAL_ARRAY_POSITION
	
	IF (theEventData.SharedRegID != ILLEGAL_SHARED_REG_ID)
		// Ensure the data in the Shared RegID slot is still valid
		IF NOT (Is_Shared_Cloud_Mission_Slot_In_Use(theEventData.SharedRegID))
			#IF IS_DEBUG_BUILD
				NET_PRINT("         - IGNORING: The SharedRegID is no longer valid.") NET_NL()
			#ENDIF
		
			EXIT
		ENDIF
		
		// Use the other ID details to verify the contents of this slot are as expected
		IF NOT (IS_STRING_NULL_OR_EMPTY(theEventData.CloudFilename))
			// ...test the string
			IF NOT (ARE_STRINGS_EQUAL(theEventData.CloudFilename, GlobalServerBD_MissionsShared.cloudDetails[theEventData.SharedRegID].mscHeaderData.tlName))
				#IF IS_DEBUG_BUILD
					NET_PRINT("         - IGNORING: The Cloud Filename doesn't match so the data must have changed.")													NET_NL()
					NET_PRINT("             Passed-In Filename: ") NET_PRINT(theEventData.CloudFilename)																NET_NL()
					NET_PRINT("             Stored Filename   : ") NET_PRINT(GlobalServerBD_MissionsShared.cloudDetails[theEventData.SharedRegID].mscHeaderData.tlName)	NET_NL()
				#ENDIF
			
				EXIT
			ENDIF
		ELIF (theEventData.UniqueID != NO_UNIQUE_ID)
			// ...test the uniqueID
			IF (theEventData.UniqueID != GlobalServerBD_MissionsShared.cloudDetails[theEventData.SharedRegID].mscUniqueID)
				#IF IS_DEBUG_BUILD
					NET_PRINT("         - IGNORING: The Mission Controller UniqueIDs doesn't match so the data must have changed.")										NET_NL()
					NET_PRINT("             Passed-In UniqueID: ") NET_PRINT_INT(theEventData.UniqueID)																	NET_NL()
					NET_PRINT("             Stored UniqueID   : ") NET_PRINT_INT(GlobalServerBD_MissionsShared.cloudDetails[theEventData.SharedRegID].mscUniqueID)		NET_NL()
				#ENDIF
			
				EXIT
			ENDIF
		ELSE
			// ...none of the supporting data is valid - this should have been checked before sending the broadcast
			#IF IS_DEBUG_BUILD
				NET_PRINT("         - IGNORING: Neither the Cloud Filename or the UniqueID is valid.") NET_NL()
				SCRIPT_ASSERT("Process_Event_Unregister_MS_Cloud_Mission(): ERROR - At least two pieces of valid ID data should have been broadcast but only one piece received. Tell Keith.")
			#ENDIF
		
			EXIT
		ENDIF
		
		theSlot = theEventData.SharedRegID
	ENDIF
	
	// If the Shared RegID wasn't stored, get the slot using the Filename/UniqueID combo
	IF (theSlot = ILLEGAL_ARRAY_POSITION)
		theSlot = Get_Slot_For_Existing_Shared_Cloud_Mission_With_These_Details(theEventData.CloudFilename, theEventData.UniqueID)
	ENDIF
	
	IF (theSlot = ILLEGAL_ARRAY_POSITION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared][CloudData]: Process_Event_Unregister_MS_Cloud_Mission() - ERROR: Failed to find this data in an existing slot. Quitting.") NET_NL()
			Debug_Output_Missions_Shared_Cloud_Data_Array()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Remove the player's registration from this shared mission
	Remove_Player_As_Registered_With_Shared_Cloud_Mission(theSlot, theEventData.Details.FromPlayerIndex)
		
	#IF IS_DEBUG_BUILD
		NET_PRINT("      Updated Array Details") NET_NL()
		Debug_Output_Missions_Shared_Cloud_Data_Array()
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Data Clearing Functions
// ===========================================================================================================

// PURPOSE:	Clear one element of the Missions Shared Cloud Data array
//
// INPUT PARAMS:		paramSlot			The slot on the missions shared cloud data array
//						paramClearingAll	[DEFAULT = FALSE] TRUE if this is part of the whole array is being cleared, FALSE if just one element
PROC Clear_One_Mission_Shared_Cloud_Data_Slot(INT paramSlot, BOOL paramClearingAll = FALSE)

	// Clear the overall struct
	g_structMissionSharedCloudData	emptyMissionsSharedCloudData
	
	GlobalServerBD_MissionsShared.cloudDetails[paramSlot] = emptyMissionsSharedCloudData
	
	// Clear any bits of the cloud data copy that need reset to defaults
	GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlName				= ""
	GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlOwner				= ""
	GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlMissionName		= ""
	GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlMissionDec		= ""
	GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.vStartPos			= << 0.0, 0.0, 0.0 >>
	GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.vCamPos				= << 0.0, 0.0, 0.0 >>
	GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iBitSet				= ALL_MISSIONS_SHARED_BITS_CLEAR
	
	IF (paramClearingAll)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: Clear_One_Mission_Shared_Cloud_Data_Slot() - Slot: ")
		NET_PRINT_INT(paramSlot)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the whole Missions Shared Cloud Data array
PROC Clear_All_Missions_Shared_Cloud_Data()

	INT		tempLoop		= 0
	BOOL	isClearingAll	= TRUE
	
	REPEAT MAX_MISSIONS_SHARED_CLOUD_DATA tempLoop
		Clear_One_Mission_Shared_Cloud_Data_Slot(tempLoop, isClearingAll)
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: Clear_All_Missions_Shared_Cloud_Data() [Max Slots: ")
		NET_PRINT_INT(MAX_MISSIONS_SHARED_CLOUD_DATA)
		NET_PRINT("]")
		NET_NL()
		NET_PRINT("          Missions Shared Cloud Data Array Size: ")
		NET_PRINT_INT((SIZE_OF(g_structMissionSharedCloudData) * MAX_MISSIONS_SHARED_CLOUD_DATA))
		NET_PRINT("   [for Info: size of an INT is: ")
		NET_PRINT_INT(SIZE_OF(tempLoop))
		NET_PRINT("]")
		NET_NL()
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Server Initialisation and Maintenance Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//     Server Initialisation
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Initialise the Missions Shared Cloud Data array into a default starting state
PROC Initialise_MP_Missions_Shared_Cloud_Data_Server()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: Initialise_MP_Missions_Shared_Cloud_Data_Server") NET_NL()
	#ENDIF
	
	// Clear the whole shared mission cloud data array
	Clear_All_Missions_Shared_Cloud_Data()
	
	// Clear the control variables
	g_structMSCloudServerControlData	emptyCloudServerControlStruct
	g_sMSCloudControlServer = emptyCloudServerControlStruct
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//     Server Maintenance
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the next scheduled cloud mission on server
PROC Maintain_Missions_Shared_Cloud_Missions_Server()

	// Get the next mission to maintain
	IF (g_sMSCloudControlServer.msccNextInSchedule >= MAX_MISSIONS_SHARED_CLOUD_DATA)
		g_sMSCloudControlServer.msccNextInSchedule = 0
	ENDIF
	
	INT arrayPosToMaintain	= g_sMSCloudControlServer.msccNextInSchedule
	
	// Always, increment to the next mission for the next frame's maintenance
	g_sMSCloudControlServer.msccNextInSchedule++
	
	// Does this array position have data to maintain?
	IF NOT (Is_Shared_Cloud_Mission_Slot_In_Use(arrayPosToMaintain))
		// Nothing to do
		EXIT
	ENDIF	
	
	// This array position contains data
	// Has all the data been received?
	// FOR NOW: Assume all data has been requested and is en-route. In practice I'll have to do more maintenance of this to ensure all the data gets received.
	
	// All the data has been received, so check if any registered players for the mission are no longer in the game
	INT				tempLoop			= 0
	INT				regPlayersBitset	= GlobalServerBD_MissionsShared.cloudDetails[arrayPosToMaintain].mscPlayersReg
	PLAYER_INDEX	thisPlayerID		= INVALID_PLAYER_INDEX()
	
	REPEAT NUM_NETWORK_PLAYERS tempLoop
		IF (IS_BIT_SET(regPlayersBitset, tempLoop))
			// ...found a registered player
			thisPlayerID = INT_TO_PLAYERINDEX(tempLoop)
			
			IF NOT (IS_NET_PLAYER_OK(thisPlayerID, FALSE))
				// Player is no longer in the game, so unregister the player
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MShared][CloudData]: Player registered for Shared Cloud_Loaded Mission slot has left the game. Slot: ")
					NET_PRINT_INT(arrayPosToMaintain)
					NET_NL()
				#ENDIF
				
				Remove_Player_As_Registered_With_Shared_Cloud_Mission(arrayPosToMaintain, thisPlayerID)
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Are there any players still registered with the mission
	IF NOT (Are_There_Any_Registered_Players_For_Shared_Cloud_Mission(arrayPosToMaintain))
		// ...there are no registered players remaining, so delete the entry
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared][CloudData]: (SERVER) Shared Cloud_Loaded Mission slot now has no registered players, so clearing the slot. Slot: ")
			NET_PRINT_INT(arrayPosToMaintain)
			NET_NL()
		#ENDIF
		
		BOOL beenUsed = GlobalServerBD_MissionsShared.cloudDetails[arrayPosToMaintain].mscBeenUsed
		Clear_One_Mission_Shared_Cloud_Data_Slot(arrayPosToMaintain)
		GlobalServerBD_MissionsShared.cloudDetails[arrayPosToMaintain].mscBeenUsed	= beenUsed
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("   Updated Array Details") NET_NL()
			Debug_Output_Missions_Shared_Cloud_Data_Array()
		#ENDIF
		
		EXIT
	ENDIF
	
	// There are still registered players
	INT uniqueID = Get_UniqueID_For_Shared_Cloud_Loaded_Mission(arrayPosToMaintain)
	
	// Check if players should now be restricted if the mission has started or beome not joinable
	IF (Is_Shared_Cloud_Mission_Restricting_Players(arrayPosToMaintain))
		// ...already restricting players, so check if the restrictions have been lifted
		IF NOT (Has_This_Mission_Request_Mission_Started(uniqueID))
		AND (Is_This_Mission_Request_Mission_Still_Joinable(uniqueID))
			// Shared Mission is no longer restricting players
			Set_Shared_Cloud_Mission_Is_No_Longer_Restricting_Players(arrayPosToMaintain)
		ENDIF
		
		EXIT
	ENDIF
	
	IF (Has_This_Mission_Request_Mission_Started(uniqueID))
	OR NOT (Is_This_Mission_Request_Mission_Still_Joinable(uniqueID))
		Set_Shared_Cloud_Mission_Is_Restricting_Players(arrayPosToMaintain)
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Maintain the Mission Shared Cloud Data array
PROC Maintain_MP_Missions_Shared_Cloud_Data_Server()

	// Perform scheduled maintenance on missions
	Maintain_Missions_Shared_Cloud_Missions_Server()
	
ENDPROC




// ===========================================================================================================
//      Client Initialisation and Maintenance Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//     Client Initialisation
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Initialise any Missions Shared Cloud client data
PROC Initialise_MP_Missions_Shared_Cloud_Data_Client()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: Initialise_MP_Missions_Shared_Cloud_Data_Client") NET_NL()
	#ENDIF
	// Initialise the client control data
	INT tempLoop = 0
	
	g_structMSCloudClientControlData	emptyCloudClientControlStruct
	g_MSCloudClientControl = emptyCloudClientControlStruct
	
	g_structMSCloudClientData 			emptyCloudClientStruct
	
	REPEAT MAX_MISSIONS_SHARED_CLOUD_DATA tempLoop
		g_MSCloudClientControl.mscccdMissions[tempLoop] = emptyCloudClientStruct
	ENDREPEAT
	
	// This is local global server data that doesn't need to be preserved on host migration - initialise it to a decent starting default in case this machine becomes host
	g_structMSCloudServerControlData	emptyCloudServerControlStruct
	g_sMSCloudControlServer = emptyCloudServerControlStruct
	
	// Local Language versions of the Mission Name strings distributed as Shared Missions (to ensure local player uses local language rather than a potentially foreign translation)
	g_structLocalLanguageDisplayText	emptyLocalLanguageText
	g_MSCLocalLanguageText = emptyLocalLanguageText
	
	REPEAT MAX_MISSIONS_SHARED_CLOUD_DATA tempLoop
		g_MSCLocalLanguageText.msclldtMissionName[tempLoop]	= ""
		g_MSCLocalLanguageText.msclldtDescHash[tempLoop]	= 0
	ENDREPEAT

ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a UGC Privileges restriction exists for this content for this client
//
// INPUT PARAMS:		paramSlot			The mission slot currently being updated to check for the player being registered
// RETURN VALUE:		BOOL				TRUE if a restriction exists, FALSE if not
//
// NOTES:	Restrictions include under-17's seeing UGC content and content not created by a friend when the restriction is friends only
FUNC BOOL Check_If_A_UGC_Privileges_Restriction_Exists(INT paramSlot)

	INT theCreatorID = GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscCreatorID
	IF (Is_CreatorID_Rockstar(theCreatorID))
		// ...no restrictions on Rockstar content
		RETURN FALSE
	ENDIF

	PLAYER_INDEX ugcPlayer = INT_TO_PLAYERINDEX(theCreatorID)
	GAMER_HANDLE	creatorGamerHandle	= GET_GAMER_HANDLE_PLAYER(ugcPlayer)
	
	IF (IS_XBOX_PLATFORM())
	#IF FEATURE_GEN9_RELEASE
	OR IS_PROSPERO_VERSION()
	#ENDIF
		IF NETWORK_CAN_VIEW_GAMER_USER_CONTENT(creatorGamerHandle)
			PRINTLN("[TS] * .KGM [MShared][CloudData]: Check_If_A_UGC_Privileges_Restriction_Exists ", GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlName, " Slot: = ", paramSlot)
			RETURN FALSE
		ENDIF
	ENDIF
	
	// UGC Content
	// Ensure the account is for over 17s
	IF NOT (IS_ACCOUNT_OVER_17_FOR_UGC())
		PRINTLN("[TS] * .KGM [MShared][CloudData]:  Privilege Restriction blocks content: Account is not over 17 for UGC. ", GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlName, " Slot: = ", paramSlot)
		RETURN TRUE
	ENDIF
	
	// Ensure the player's are friends if the privileges are set to Friends Only
	BOOL performRefinedGamerToGamerCheck = FALSE
	
	// KGM 5/2/14 [BUG 2167358]: Rockstar Created missions have been dealt with above, so this must be the equivalent of UGC, so passing in the UGC flag
	IF NOT (ALLOWED_TO_GET_EVERYONE_CONTENT(TRUE))
		// ...a restriction exists, so check if allowed to get friend content
		IF NOT (ALLOWED_TO_GET_FRIEND_CONTENT(TRUE))
			// ...not allowed to get friend content, so a restriction exists
			#IF IS_DEBUG_BUILD
				NET_PRINT(".KGM [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" Privilege Restriction blocks content: Not allowed to get Friend Content. Slot: ")
				NET_PRINT_INT(paramSlot)
				NET_NL()
			#ENDIF
			
			// KGM 30/9/14 [BUG 2059787]: For XboxOne only, do a refined Gamer-To-Gamer check to see if we should bypass the restriction
			IF NOT (IS_XBOX_PLATFORM())
			#IF FEATURE_GEN9_RELEASE
			AND NOT IS_PROSPERO_VERSION()
			#ENDIF
				// ...not XboxOne, so the result remains: content blocked
				PRINTLN("[TS] * .KGM [MShared][CloudData]:  Privilege Restriction blocks content: Account is not over 17 for UGC. ", GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlName, " Slot: = ", paramSlot)
				RETURN TRUE
			ENDIF
	
			// XboxOne, if the commandline to ignore this check exists then the result remains: content blocked
			#IF IS_DEBUG_BUILD
				IF (GET_COMMANDLINE_PARAM_EXISTS("sc_tempKeithNGGamerCheckOff"))
					PRINTLN(".KGM [JobList]: Durango refined Gamer-To-Gamer Content Restriction check for Shared UGC switched off by commandline: sc_tempKeithNGGamerCheckOff. Block exists.")
					RETURN TRUE
				ENDIF
			#ENDIF
			
			// Need to do the refined Gamer to Gamer check to be sure
			performRefinedGamerToGamerCheck = TRUE
		ENDIF
		
		// Allowed to get Friend Content, so check if the CreatorID is still valid
		IF NOT (IS_NET_PLAYER_OK(ugcPlayer, FALSE))
			// ...the creatorID is no longer in the game, so block the content (can't check if they are friends)
			#IF IS_DEBUG_BUILD
				NET_PRINT(".KGM [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" Privilege Restriction blocks content: Friend Content allowed but creator has left game. Slot: ")
				NET_PRINT_INT(paramSlot)
				NET_NL()
			#ENDIF
	
			PRINTLN("[TS] * .KGM [MShared][CloudData]:  Privilege Restriction blocks content: Account is not over 17 for UGC. ", GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlName, " Slot: = ", paramSlot)
			RETURN TRUE
		ENDIF
		
		// CreatorID is still valid, so compare gamer handles to check for friends
		GAMER_HANDLE	myGamerHandle		= GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
		
		IF NOT (NETWORK_ARE_HANDLES_THE_SAME(myGamerHandle, creatorGamerHandle))
			IF NOT (NETWORK_IS_FRIEND(creatorGamerHandle))
				// KGM 12/3/15 [BUG 2274550]: The creatorID is not a friend, but ignore this if the mission is a Heist
				MP_MISSION_ID_DATA	theMissionIdData = Get_MissionID_Data_For_Shared_Cloud_Loaded_Mission(paramSlot)
				
				IF NOT (Is_FM_Cloud_Loaded_Activity_A_Heist(theMissionIdData))
				AND NOT (Is_FM_Cloud_Loaded_Activity_Heist_Planning(theMissionIdData))
					// ...mission is not heist-related, so privilege restriction exists
					#IF IS_DEBUG_BUILD
						NET_PRINT(".KGM [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" Privilege Restriction blocks content: Creator is not a friend. Slot: ")
						NET_PRINT_INT(paramSlot)
						NET_PRINT(" [")
						NET_PRINT(GET_PLAYER_NAME(ugcPlayer))
						NET_PRINT("]")
						NET_NL()
					#ENDIF
			
					PRINTLN("[TS] * .KGM [MShared][CloudData]:  Privilege Restriction blocks content: Account is not over 17 for UGC. ", GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlName, " Slot: = ", paramSlot)
					RETURN TRUE
				ELSE
					// KGM 12/3/15 [BUG 2274550]: Ignore the 'not a friend' privilege restriction - this is probably a Quick Restart of a Heist Part Two mission
					#IF IS_DEBUG_BUILD
						NET_PRINT(".KGM [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" IGNORE Privilege Restriction blocks content: Creator is not a friend but mission is heist-related. Slot: ")
						NET_PRINT_INT(paramSlot)
						NET_PRINT(" [")
						NET_PRINT(GET_PLAYER_NAME(ugcPlayer))
						NET_PRINT("]")
						NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// They are friends, so do a refined Gamer-To-Gamer check on Durango if required
		IF (performRefinedGamerToGamerCheck)
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" ...DURANGO ONLY. Performing refined gamer-to-gamer content restriction check. Slot: ")
				NET_PRINT_INT(paramSlot)
				NET_NL()
			#ENDIF
			
			// When a player enters a session, there are a few frames before the gamer-to-gamer check is available to be tested.
			// I'm taking the easy route here and blocking the content if the result is not available.
			IF NOT (NETWORK_HAS_VIEW_GAMER_USER_CONTENT_RESULT(creatorGamerHandle))
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" ...DURANGO ONLY. Refined gamer-to-gamer content restriction check isn't ready. Treating as Blocked. Slot: ")
					NET_PRINT_INT(paramSlot)
					NET_NL()
				#ENDIF
				
				PRINTLN("[TS] * .KGM [MShared][CloudData]:  Privilege Restriction blocks content: Account is not over 17 for UGC. ", GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlName, " Slot: = ", paramSlot)
				RETURN TRUE
			ENDIF
			
			// The content check result is available, so test it
			IF NOT (NETWORK_CAN_VIEW_GAMER_USER_CONTENT(creatorGamerHandle))
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" ...DURANGO ONLY. Refined gamer-to-gamer content restriction still exists. Content is blocked. Slot: ")
					NET_PRINT_INT(paramSlot)
					NET_NL()
				#ENDIF
				
				PRINTLN("[TS] * .KGM [MShared][CloudData]:  Privilege Restriction blocks content: Account is not over 17 for UGC. ", GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlName, " Slot: = ", paramSlot)
				RETURN TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MShared][CloudData]: ") NET_PRINT_TIME() NET_PRINT(" ...DURANGO ONLY. Refined gamer-to-gamer content check has passed. No restriction. Allow UGC. Slot: ")
				NET_PRINT_INT(paramSlot)
				NET_NL()
			#ENDIF
		ENDIF
	ENDIF
	
	// No restriction
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the local language version of the Mission Name text (just in case the shared version is in a foreign language)
//
// INPUT PARAMS:		paramSlot			The mission slot currently being updated to check for the player being registered
//
// NOTES:	KGM 11/3/14: Also now stores the local version of the Description Hash and ignores the shared version.
//						 Code uses the hash as a pointer into a local data array, so the shared version is only guaranteed to be correct on the local machine.
PROC Store_Local_Language_Display_Text(INT paramSlot)

	MP_MISSION_ID_DATA	theMissionIdData	= Get_MissionID_Data_For_Shared_Cloud_Loaded_Mission(paramSlot)
	TEXT_LABEL_63		theMissionName		= Get_Mission_Name_For_FM_Cloud_Loaded_Activity(theMissionIdData, TRUE)
	
	// If the mission name is available locally, then store it in the local display text array, otherwise use the Shared Mission Name
	IF (IS_STRING_NULL_OR_EMPTY(theMissionName))
		// ...data isn't available locally, so store the Shared Mission name
		g_MSCLocalLanguageText.msclldtMissionName[paramSlot] = GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlMissionName
		
		// ...also set the DescHash to 0
		g_MSCLocalLanguageText.msclldtDescHash[paramSlot] = 0
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared][CloudData]: Using Shared version of Mission Name for display: ")
			NET_PRINT(g_MSCLocalLanguageText.msclldtMissionName[paramSlot])
			NET_PRINT(". Description hash = ")
			NET_PRINT_INT(g_MSCLocalLanguageText.msclldtDescHash[paramSlot])
			NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// The Mission Name is available locally, so storing the local version as the display string (to prevent displaying a foreign language translation)
	g_MSCLocalLanguageText.msclldtMissionName[paramSlot] = theMissionName
		
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: Using Local version of Mission Name for display: ")
		NET_PRINT(g_MSCLocalLanguageText.msclldtMissionName[paramSlot])
		NET_PRINT(" instead of Shared Version: ")
		NET_PRINT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.tlMissionName)
		NET_NL()
	#ENDIF
	
	// Also retrieving the local version of the mission description
	g_MSCLocalLanguageText.msclldtDescHash[paramSlot] = Get_Mission_Dec_Hash_For_FM_Cloud_Loaded_Activity(theMissionIdData, TRUE)
		
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: Using Local version of DescriptionHash for display: ")
		NET_PRINT_INT(g_MSCLocalLanguageText.msclldtDescHash[paramSlot])
		NET_PRINT(" instead of Shared Version: ")
		NET_PRINT_INT(GlobalServerBD_MissionsShared.cloudDetails[paramSlot].mscHeaderData.iMissionDecHash)
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the next scheduled cloud mission on client
PROC Maintain_Missions_Shared_Cloud_Missions_Client()

	// Get the next mission to maintain
	IF (g_MSCloudClientControl.mscccdNextInMissionSchedule >= MAX_MISSIONS_SHARED_CLOUD_DATA)
		g_MSCloudClientControl.mscccdNextInMissionSchedule = 0
	ENDIF
	
	INT arrayPosToMaintain	= g_MSCloudClientControl.mscccdNextInMissionSchedule
	
	// Always, increment to the next mission for the next frame's maintenance
	g_MSCloudClientControl.mscccdNextInMissionSchedule++
	
	// Check if this shared mission slot is in use and the data is available
	IF	(Is_Shared_Cloud_Mission_Slot_In_Use(arrayPosToMaintain))
	AND (Is_All_Shared_Cloud_Mission_Data_Stored(arrayPosToMaintain))
		// ...slot is in use and data is available, so maintain it
		// Has this client processed this shared mission previously, or is it new?
		IF (Has_Client_Processed_This_Shared_Cloud_Loaded_Mission(arrayPosToMaintain))
			// ...client has previously processed this shared mission slot, but do nothing if a privileges restriction exists
			IF NOT (Is_Client_Restricted_From_Seeing_This_Shared_Cloud_Loaded_Mission(arrayPosToMaintain))
				// Maintain the local copy of the Shared Mission the player is registered on
				Maintain_Shared_Mission_Data_Local_Copy(arrayPosToMaintain)
			
				// If the player added this mission based on the shared data and hasn't yet removed it, then check if the client should remove the mission
				IF (Has_Client_Added_This_Shared_Cloud_Loaded_Mission(arrayPosToMaintain))
				AND NOT (Has_Client_Removed_This_Shared_Cloud_Loaded_Mission(arrayPosToMaintain))
					// ...the mission was added based on the shared data and hasn't yet been removed, so check if a player restriction for the mission now exists
					IF (Is_Shared_Cloud_Mission_Restricting_Players(arrayPosToMaintain))
						// ...players are now restricted, so the mission must be either not joinable or started
						// Remove the mission for this player if this player is not part of the mission and the player is not registered for the mission (meaning he player is not in the mission corona)
						IF NOT (Is_Player_Registered_For_Shared_Cloud_Mission_In_Slot(arrayPosToMaintain, PLAYER_ID()))
						AND (Get_UniqueID_For_This_Players_Mission(PLAYER_ID()) != Get_UniqueID_For_Shared_Cloud_Loaded_Mission(arrayPosToMaintain))
							// ...player is not part of this mission and is not registered for the mission, so remove the corona and mark the mission as removed
							// NOTE: If the player is registered for the mission then the corona should get cleaned up elsewhere
							#IF IS_DEBUG_BUILD
								INT missionsAtCoordsRegID = g_MSCloudClientControl.mscccdMissions[arrayPosToMaintain].msccdMatcRegID
								NET_PRINT("...KGM MP [MShared][CloudData]: Removing a shared mission that has started or is no longer joinable: ")
								NET_PRINT_INT(arrayPosToMaintain)
								NET_PRINT(" [MatC Reg ID: ")
								NET_PRINT_INT(missionsAtCoordsRegID)
								NET_PRINT("]")
								NET_NL()
							#ENDIF
							
							Delete_This_Shared_Mission_For_Local_Player_If_Not_Locally_Available(arrayPosToMaintain)
							
							g_MSCloudClientControl.mscccdMissions[arrayPosToMaintain].msccdMatcRegID = ILLEGAL_AT_COORDS_ID

							Set_Client_Has_Removed_Shared_Cloud_Mission(arrayPosToMaintain)
						ENDIF
					ENDIF
				ELSE
					// The Mission has been added and removed - check if it should get re-added
					IF (Has_Client_Added_This_Shared_Cloud_Loaded_Mission(arrayPosToMaintain))
					AND (Has_Client_Removed_This_Shared_Cloud_Loaded_Mission(arrayPosToMaintain))
						// KGM 6/7/13: It can get re-added if the mission had become Not Joinable but is no Joinable
						IF NOT (Is_Shared_Cloud_Mission_Restricting_Players(arrayPosToMaintain))
							// ...the Shared Mission now has no restrictions again, so clear the 'processed', 'added' and 'removed' flags so that the mission can get setup again
							#IF IS_DEBUG_BUILD
								NET_PRINT("...KGM MP [MShared][CloudData]: Shared Mission used to be restricted but now has no restrictions. Slot: ") NET_PRINT_INT(arrayPosToMaintain) NET_NL()
							#ENDIF
				
							Allow_Client_To_Setup_This_Shared_Mission_Again(arrayPosToMaintain)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			EXIT
		ENDIF
		
		// This is a new shared mission, so register with the MissionsAtCoords system unless it has already been registered using local data
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MShared][CloudData]: Found a new shared mission in slot: ") NET_PRINT_INT(arrayPosToMaintain) NET_NL()
		#ENDIF
		
		// KGM 21/8/13: When this kicked in early it could cause the host/join screen to display when it wasn't required, so delaying to give the mission a chance to get setup by something else first
		IF NOT (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
			#IF IS_DEBUG_BUILD
				NET_PRINT("      - DELAYING CLIENT PROCESSING: Waiting for initial cloud-loaded data to be available.") NET_NL()
			#ENDIF
		
			EXIT
		ENDIF
	
		// ...always, mark the mission as processed
		Set_Client_Has_Processed_Shared_Cloud_Mission(arrayPosToMaintain)
		
		// If the mission is stored locally, grab the local language version of the mission name
		Store_Local_Language_Display_Text(arrayPosToMaintain)
		
		// Check if a privileges restriction exists for the mission
		IF (Check_If_A_UGC_Privileges_Restriction_Exists(arrayPosToMaintain))
			// ...a privileges restriction exists, so set the flag and nothing more to do
			Set_Client_Is_Restricted_From_Seeing_This_Shared_Cloud_Mission(arrayPosToMaintain)
			
			EXIT
		ENDIF
		
		// If players are restricted for the mission then it must already be not joinable or started, so nothing more to do
		IF (Is_Shared_Cloud_Mission_Restricting_Players(arrayPosToMaintain))
			// Player restriction exists, so nothing more to do
			EXIT
		ENDIF
		
		// Check if the mission has already been registered
		MP_MISSION_ID_DATA	theMissionIdData	= Get_MissionID_Data_For_Shared_Cloud_Loaded_Mission(arrayPosToMaintain)
		VECTOR				theMissionCoords	= Get_Coords_For_Shared_Cloud_Loaded_Mission(arrayPosToMaintain)
		INT					theMatcRegID		= ILLEGAL_AT_COORDS_ID
		
		theMatcRegID = Expensive_Check_If_This_Mission_Already_Registered_With_MissionsAtCoords(theMissionIdData, theMissionCoords)
		IF (theMatcRegID != ILLEGAL_AT_COORDS_ID)
			// Mission already registered, so nothing more to do
			// KGM 18/11/14 [BUG 2135150]: Still mark the mission as added and store the MatcID if already registered so that it goes through the full delete checks when removed
			//	(EXCEPT IF THIS IS A HEIST-RELATED MISSION: This may need to change, but for now treat it the same as before)
			// KGM 28/1/15 [BUG 2089366]: UPDATE: For Heist-Related missions allow the registration to still take place unless the Local Player is the Leader (First Registered Player).
			//		This is to ensure the Heist Controller knows about all instances of the same mission available in the same apartment but for different Leaders.
			// KGM 29/1/15 [BUG 2217663]: UPDATE to yesterday's change - also don't re-register if in an activity session.
			IF NOT (Is_FM_Cloud_Loaded_Activity_A_Heist(theMissionIdData))
			AND NOT (Is_FM_Cloud_Loaded_Activity_Heist_Planning(theMissionIdData))
			AND NOT (Is_FM_Cloud_Loaded_Activity_A_Flow_Mission(theMissionIdData))
				// Not Heist-Related
				PRINTLN(".KGM [MShared][CloudData]: Already Registered. Not a heist-related mission, so mark data as added. Storing Matc RegID: ", theMatcRegID)
				Set_Client_Has_Added_Shared_Cloud_Mission(arrayPosToMaintain)
				g_MSCloudClientControl.mscccdMissions[arrayPosToMaintain].msccdMatcRegID = theMatcRegID
				
				EXIT
			ELSE
				// Heist-Related, but if the game is in an Activity Session then it shouldn't be re-registered (there should only be one Heist ongoing in an activity session)
				// KGM 29/1/15 NOTE: Before yesterday's change re-registration's in an activity sesssion would have been blocked.
				IF (NETWORK_IS_ACTIVITY_SESSION())
					PRINTLN(".KGM [MShared][CloudData]: Already Registered. This is a Heist-Related mission but the game is in an Activity Session. Don't mark as 'added' or store the MatcRegID.")
					EXIT
				ENDIF
				
				// If the local player is not the first registered player (ie: not the Leader) then allow it to be setup again
				INT localPlayerBit		= NATIVE_TO_INT(PLAYER_ID())
				
				IF (IS_BIT_SET(GlobalServerBD_MissionsShared.cloudDetails[arrayPosToMaintain].mscBitFirstPlayerReg, localPlayerBit))
					PRINTLN(".KGM [MShared][CloudData]: Already Registered. This is a Heist-Related mission and the Local Player is the Leader. Don't mark as 'added' or store the MatcRegID.")
					EXIT
				ENDIF
				
				PRINTLN(".KGM [MShared][CloudData]: Already Registered, but this is a Heist-Related mission. Local Player is NOT the Leader so pass to Heist Controller.")
			ENDIF
		ENDIF
		
		// Setup the Shared Mission with Missions At Coords if not already registered using local data
		IF (Register_This_Shared_Mission_For_Local_Player(arrayPosToMaintain))
			Set_Client_Has_Added_Shared_Cloud_Mission(arrayPosToMaintain)
		ENDIF
		
		// Store the local copy of the Shared Mission if the player is already registered on it
		Maintain_Shared_Mission_Data_Local_Copy(arrayPosToMaintain)
		
		EXIT
	ENDIF
	
	// Slot is not is use or data is not available, so maintain it
	// Nothing to do unless the client previously processed data in this slot
	IF NOT (Has_Client_Processed_This_Shared_Cloud_Loaded_Mission(arrayPosToMaintain))
		EXIT
	ENDIF
	
	// Client has previously processed data in this slot
	// If the client added a mission based on the shared data, then the mission needs to be deleted
	IF (Has_Client_Added_This_Shared_Cloud_Loaded_Mission(arrayPosToMaintain))
		// ...client added the mission based on the shared data, so it needs to be removed if it is still setup
		// NOTE: It may have been removed because the mission started or become not joinable and the player wasn't part of it
		IF NOT (Has_Client_Removed_This_Shared_Cloud_Loaded_Mission(arrayPosToMaintain))
			// ...mission hasn't already been removed
			#IF IS_DEBUG_BUILD
				INT missionsAtCoordsRegID = g_MSCloudClientControl.mscccdMissions[arrayPosToMaintain].msccdMatcRegID
				NET_PRINT("...KGM MP [MShared][CloudData]: Removing a shared mission that used to be in slot: ")
				NET_PRINT_INT(arrayPosToMaintain)
				NET_PRINT(" [MatC Reg ID: ")
				NET_PRINT_INT(missionsAtCoordsRegID)
				NET_PRINT("]")
				NET_NL()
			#ENDIF
			
			Delete_This_Shared_Mission_For_Local_Player_If_Not_Locally_Available(arrayPosToMaintain)
		ENDIF
	ENDIF
	
	// Clear the data for this slot
	g_structMSCloudClientData  emptyCloudClientStruct
	g_MSCloudClientControl.mscccdMissions[arrayPosToMaintain] = emptyCloudClientStruct
		
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MShared][CloudData]: Clearing Client Data For Shared Mission That used to be in slot: ")
		NET_PRINT_INT(arrayPosToMaintain)
		NET_NL()
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Maintain the Mission Shared Cloud client functionality
PROC Maintain_MP_Missions_Shared_Cloud_Data_Client()

	// Perform scheduled maintenance on missions
	Maintain_Missions_Shared_Cloud_Missions_Client()
	
ENDPROC







