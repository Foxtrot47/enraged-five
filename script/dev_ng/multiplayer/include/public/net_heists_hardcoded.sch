/* =================================================================================================== *\
		MISSION NAME	:	net_heists_hardcoded.sch
		AUTHOR			:	Alastair Costley - 2014/04/24
		DESCRIPTION		:	Contains hard-coded data (and functions that use it) for the heist board. 
\* =================================================================================================== */


USING "globals.sch"
USING "commands_network.sch"
USING "net_include.sch"



ENUM MP_HEIST_STRAND
	HEIST_STRAND_NONE = INVALID_HEIST_DATA,
	HEIST_STRAND_ORNATE,
	HEIST_STRAND_PRISON,
	HEIST_STRAND_BIOLAB,
	HEIST_STRAND_NARCOTICS,
	HEIST_STRAND_TUTORIAL,
	
	MAX_HEISTS_SETUP
ENDENUM


// -----------------------------------------------------------------------------------------------------------

// SPECIFIC HARDCODED CHECKS
FUNC BOOL Is_This_Ornate_Heist_RCID_Hash(INT paramHashRCID)
	RETURN (paramHashRCID = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job)
ENDFUNC

FUNC BOOL Is_This_Biolab_Heist_RCID_Hash(INT paramHashRCID)
	RETURN (paramHashRCID = g_sMPTUNABLES.iroot_id_HASH_The_Humane_Labs_Raid)
ENDFUNC

FUNC BOOL Is_This_Prison_Heist_RCID_Hash(INT paramHashRCID)
	RETURN (paramHashRCID = g_sMPTUNABLES.iroot_id_HASH_The_Prison_Break)
ENDFUNC

FUNC BOOL Is_This_Chicken_Heist_RCID_Hash(INT paramHashRCID)
	RETURN (paramHashRCID = g_sMPTUNABLES.iroot_id_HASH_Series_A_Funding)
ENDFUNC

FUNC BOOL Is_This_Tutorial_Heist_RCID_Hash(INT paramHashRCID)
	RETURN (paramHashRCID = g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job)
ENDFUNC



FUNC INT Get_Ornate_Heist_RCID_Hash()
	RETURN (g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job)
ENDFUNC

FUNC INT Get_Biolab_Heist_RCID_Hash()
	RETURN (g_sMPTUNABLES.iroot_id_HASH_The_Humane_Labs_Raid)
ENDFUNC

FUNC INT Get_Prison_Heist_RCID_Hash()
	RETURN (g_sMPTUNABLES.iroot_id_HASH_The_Prison_Break)
ENDFUNC

FUNC INT Get_Chicken_Heist_RCID_Hash()
	RETURN (g_sMPTUNABLES.iroot_id_HASH_Series_A_Funding)
ENDFUNC

FUNC INT Get_Tutorial_Heist_RCID_Hash()
	RETURN (g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Convert a Heist Finale RCID Hash into a Heist Flow Progression value (ie: 0 = Tutorial, 1 = Prison, etc)
//
// INPUT PARAMS:			paramHashRCID			The Finale Heist rootContentID
// RETURN VALUE:			INT						The Heist Flow Progression value
FUNC INT Get_Heist_Strand_Flow_Value_From_Finale_Hash_RCID(INT paramHashRCID)

	IF (Is_This_Tutorial_Heist_RCID_Hash(paramHashRCID))
		RETURN (HEIST_FLOW_ORDER_FLEECA_TUTORIAL)
	ENDIF

	IF (Is_This_Prison_Heist_RCID_Hash(paramHashRCID))
		RETURN (HEIST_FLOW_ORDER_PRISON_BREAK)
	ENDIF

	IF (Is_This_Biolab_Heist_RCID_Hash(paramHashRCID))
		RETURN (HEIST_FLOW_ORDER_HUMANE_LABS)
	ENDIF

	IF (Is_This_Chicken_Heist_RCID_Hash(paramHashRCID))
		RETURN (HEIST_FLOW_ORDER_NARCOTICS)
	ENDIF

	IF (Is_This_Ornate_Heist_RCID_Hash(paramHashRCID))
		RETURN (HEIST_FLOW_ORDER_PACIFIC_STANDARD)
	ENDIF
	
	PRINTLN(".KGM [ActSelect][Heist]: Get_Heist_Strand_Flow_Value_From_Finale_Hash_RCID(): ERROR - Unknown Finale RCID: ", paramHashRCID)
	SCRIPT_ASSERT("Get_Heist_Strand_Flow_Value_From_Finale_Hash_RCID(): ERROR - Unknown Finale RCID. Tell Keith")
	
	RETURN (UNKNOWN_HEIST_FLOW_ORDER)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Convert a Heist Flow Progression value (ie: 0 = Tutorial, 1 = Prison, etc) into a Finale RCID Hash
//
// INPUT PARAMS:			paramHeistFlow			The Heist Flow Progression value
// RETURN VALUE:			INT						The Finale Heist rootContentID
FUNC INT Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(INT paramHeistFlow)

	SWITCH (paramHeistFlow)
		CASE HEIST_FLOW_ORDER_FLEECA_TUTORIAL		RETURN (Get_Tutorial_Heist_RCID_Hash())
		CASE HEIST_FLOW_ORDER_PRISON_BREAK			RETURN (Get_Prison_Heist_RCID_Hash())
		CASE HEIST_FLOW_ORDER_HUMANE_LABS			RETURN (Get_Biolab_Heist_RCID_Hash())
		CASE HEIST_FLOW_ORDER_NARCOTICS				RETURN (Get_Chicken_Heist_RCID_Hash())
		CASE HEIST_FLOW_ORDER_PACIFIC_STANDARD		RETURN (Get_Ornate_Heist_RCID_Hash())
	ENDSWITCH
	
	PRINTLN(".KGM [ActSelect][Heist]: Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(): ERROR - Unknown Strand Flow Value: ", paramHeistFlow)
	SCRIPT_ASSERT("Get_Heist_Finale_Hash_RCID_From_Strand_Flow_Value(): ERROR - Unknown Strand Flow Value. Tell Keith")
	
	RETURN (0)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the next Heist Flow Progression value (ie: 0 = Tutorial, 1 = Prison, etc)
//
// INPUT PARAMS:			paramHeistFlow			The Heist Flow Progression value
// RETURN VALUE:			INT						The updated Heist Flow Progression value
FUNC INT Get_Next_Heist_Flow_Progression_Value(INT paramHeistFlow)

	IF (paramHeistFlow = UNKNOWN_HEIST_FLOW_ORDER)
		RETURN (0)
	ENDIF
	
	INT nextFlowValue = paramHeistFlow + 1
	
	IF (nextFlowValue >= MAX_HEIST_FLOW_ORDER)
		nextFlowValue = 0
	ENDIF
	
	RETURN (nextFlowValue)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve a hardcoded Heist Title text label based on teh RCID hash
//
// INPUT PARAMS:			paramHashRCID			The Finale Heist rootContentID
// RETURN VALUE:			STRING					The Heist Name
FUNC STRING Get_Heist_Hardcoded_Title_TextLabel(INT paramHashRCID)

	STRING sHeistDesc = "HTITLE_ERROR"
	
	IF (paramHashRCID = 0)
		RETURN (sHeistDesc)
	ENDIF
	
	IF (Is_This_Tutorial_Heist_RCID_Hash(paramHashRCID))
		sHeistDesc = "HTITLE_TUT"
	ENDIF
	
	IF (Is_This_Prison_Heist_RCID_Hash(paramHashRCID))
		sHeistDesc = "HTITLE_PRISON"
	ENDIF	

	IF (Is_This_Biolab_Heist_RCID_Hash(paramHashRCID))
		sHeistDesc = "HTITLE_HUMANE"
	ENDIF
	
	IF (Is_This_Chicken_Heist_RCID_Hash(paramHashRCID))
		sHeistDesc = "HTITLE_NARC"
	ENDIF
	
	IF (Is_This_Ornate_Heist_RCID_Hash(paramHashRCID))
		sHeistDesc = "HTITLE_ORNATE"
	ENDIF
	
	RETURN (sHeistDesc)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Help Text string to display afte a Heist Phonecall - the first call will display a different help text
//
// INPUT PARAMS:			paramHashRCID				The RootContentID Hash of the current mission
// RETURN VALUE:			TEXT_LABEL_23				The text label for the help text
FUNC TEXT_LABEL_23 Get_Heist_Help_Text_After_Call(INT paramHashRCID)

	TEXT_LABEL_23 theReturn = "HEIST_READY_HLP"

	// An uncompleted Tutorial means this is the first Heist, so display different help text
	IF (Is_This_Tutorial_Heist_RCID_Hash(paramHashRCID))
		PRINTLN("...KGM MP [ActSelect][Heist]: DISPLAYING HEIST FIRST HELP - this must be Tutorial Heist")
		theReturn = "FIRST_HEIST_HLP"
		RETURN(theReturn)
	ENDIF
	
	// Display normal text
	PRINTLN("...KGM MP [ActSelect][Heist]: DISPLAYING HEIST HELP - This must be for a Heist other than Tutorial, or for Tutorial that has previously been completed as Leader")
	RETURN (theReturn)

ENDFUNC


// PURPOSE:	Check if this rootContentIdHash is for a known Heist Finale Mission
//
// INPUT PARAMS:		paramRcidHash			The RootContentIdHash
// RETURN VALUE:		BOOL					TRUE if a Heist Finale mission, otherwise False
//
// NOTES:	KGM 17/9/14: We're going to need this as hardcoded data, but we'll refer to it as infrequently as possible
FUNC BOOL Is_This_A_Known_Heist_Finale(INT paramRcidHash)

//	SWITCH (paramRcidHash)
//		CASE ci_HEIST_STRAND_HASH_ORNATE					// Ornate Bank - Finale
//		CASE ci_HEIST_STRAND_HASH_BIOLAB					// Biolab Score - Finale
//		CASE ci_HEIST_STRAND_HASH_PRISON					// Prison Break - Finale
//		CASE ci_HEIST_STRAND_HASH_NARCOTICS					// Narcotics (chicken) - Finale
//		CASE ci_HEIST_STRAND_HASH_TUTORIAL					// Tutorial - Finale
//			RETURN TRUE
//	ENDSWITCH
	
	IF (Is_This_Tutorial_Heist_RCID_Hash(paramRcidHash))
	OR (Is_This_Prison_Heist_RCID_Hash(paramRcidHash))
	OR (Is_This_Biolab_Heist_RCID_Hash(paramRcidHash))
	OR (Is_This_Chicken_Heist_RCID_Hash(paramRcidHash))
	OR (Is_This_Ornate_Heist_RCID_Hash(paramRcidHash))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


// Switched across to INT since we don't store / pass content id across (1829768)
FUNC MP_HEIST_STRAND GET_CURRENT_HEIST_STRAND(INT iRootContID)

//	SWITCH iRootContID
//		CASE ci_HEIST_STRAND_HASH_ORNATE					RETURN HEIST_STRAND_ORNATE
//		CASE ci_HEIST_STRAND_HASH_PRISON					RETURN HEIST_STRAND_PRISON
//		CASE ci_HEIST_STRAND_HASH_BIOLAB					RETURN HEIST_STRAND_BIOLAB
//		CASE ci_HEIST_STRAND_HASH_NARCOTICS					RETURN HEIST_STRAND_NARCOTICS
//		CASE g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job		RETURN HEIST_STRAND_TUTORIAL
//		DEFAULT
//			RETURN HEIST_STRAND_NONE
//	ENDSWITCH
	
	IF (Is_This_Tutorial_Heist_RCID_Hash(iRootContID))
		RETURN HEIST_STRAND_TUTORIAL
	ENDIF
	
	IF (Is_This_Prison_Heist_RCID_Hash(iRootContID))
		RETURN HEIST_STRAND_PRISON
	ENDIF
	
	IF (Is_This_Biolab_Heist_RCID_Hash(iRootContID))
		RETURN HEIST_STRAND_BIOLAB
	ENDIF
	
	IF (Is_This_Chicken_Heist_RCID_Hash(iRootContID))
		RETURN HEIST_STRAND_NARCOTICS
	ENDIF
	
	IF (Is_This_Ornate_Heist_RCID_Hash(iRootContID))
		RETURN HEIST_STRAND_ORNATE
	ENDIF	
	
	// Not found
	RETURN HEIST_STRAND_NONE
	
ENDFUNC


FUNC INT TEMP_GET_CURRENT_HEIST_STRAND_SETUP_MSSION_COUNT(INT iRootContID)

//	SWITCH iRootContID
//		CASE ci_HEIST_STRAND_HASH_ORNATE					RETURN 5	// Ornate Bank - Finale		1) -1905677235	2) -1904218009	3) -1596291374	4) 911181645	5) 1756125549
//		CASE ci_HEIST_STRAND_HASH_PRISON					RETURN 4	// Prison Break - Finale	1) 1993250643	2) 1637355529	3) 1089168362	4) -12340551
//		CASE ci_HEIST_STRAND_HASH_BIOLAB					RETURN 5	// Biolab Score - Finale	1) -2079336134	2) 2039847454	3) 2136235844	4) -365482269	5) 496643418
//		CASE ci_HEIST_STRAND_HASH_NARCOTICS					RETURN 5	// Chicken - Finale			1) -590337633	2) -291064527	3) -630015171	4) -19483040	5) 1585746186
//		CASE g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job		RETURN 2	// Tutorial Heist - Finale	1) -240596566	2) -2049205985
//		DEFAULT
//			RETURN INVALID_HEIST_DATA
//	ENDSWITCH
	
	IF (Is_This_Tutorial_Heist_RCID_Hash(iRootContID))
		RETURN 2	// Tutorial Heist - Finale	1) -240596566	2) -2049205985
	ENDIF
	
	IF (Is_This_Prison_Heist_RCID_Hash(iRootContID))
		RETURN 4	// Prison Break - Finale	1) 1993250643	2) 1637355529	3) 1089168362	4) -12340551
	ENDIF
	
	IF (Is_This_Biolab_Heist_RCID_Hash(iRootContID))
		RETURN 5	// Biolab Score - Finale	1) -2079336134	2) 2039847454	3) 2136235844	4) -365482269	5) 496643418
	ENDIF
	
	IF (Is_This_Chicken_Heist_RCID_Hash(iRootContID))
		RETURN 5	// Chicken - Finale			1) -590337633	2) -291064527	3) -630015171	4) -19483040	5) 1585746186
	ENDIF
	
	IF (Is_This_Ornate_Heist_RCID_Hash(iRootContID))
		RETURN 5	// Ornate Bank - Finale		1) -1905677235	2) -1904218009	3) -1596291374	4) 911181645	5) 1756125549
	ENDIF	
	
	// Not found
	RETURN INVALID_HEIST_DATA

ENDFUNC

/// PURPOSE:
///    Using the passed in rContID, check if theplayer has already completed that finale cutscene.
/// PARAMS:
///    iRootContID - Heist strand to check.
FUNC BOOL HAS_LOCAL_PLAYER_COMPLETED_FINALE_CUTSCENE(INT iRootContID)

//	SWITCH iRootContID
//		CASE ci_HEIST_STRAND_HASH_ORNATE		
//			RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_ORNATE)
//		CASE ci_HEIST_STRAND_HASH_PRISON		
//			RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_BIOLAB)
//		CASE ci_HEIST_STRAND_HASH_BIOLAB		
//			RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_PRISON)
//		CASE ci_HEIST_STRAND_HASH_NARCOTICS 	
//			RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_NARCOTIC)
//		CASE g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job 	
//			RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_TUTORIAL) 
//	ENDSWITCH
	
	IF (Is_This_Tutorial_Heist_RCID_Hash(iRootContID))
		RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_TUTORIAL)
	ENDIF	

	IF (Is_This_Prison_Heist_RCID_Hash(iRootContID))
		RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_PRISON)
	ENDIF
	
	IF (Is_This_Biolab_Heist_RCID_Hash(iRootContID))
		RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_BIOLAB)
	ENDIF
	
	IF (Is_This_Chicken_Heist_RCID_Hash(iRootContID))
		RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_NARCOTIC)
	ENDIF

	IF (Is_This_Ornate_Heist_RCID_Hash(iRootContID))
		RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_ORNATE)
	ENDIF
	
	PRINTLN("[AMEC][HEIST_LAUNCH] - HAS_LOCAL_PLAYER_COMPLETED_FINALE_CUTSCENE - ERROR! iRootContID not reconised, value: ", iRootContID)
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Using the passed in rContID, Set that the local player has done this finale cutscene
/// PARAMS:
///    iRootContID - Heist strand to set.
PROC SET_LOCAL_PLAYER_COMPLETED_FINALE_CUTSCENE(INT iRootContID)

//	SWITCH iRootContID
//		CASE ci_HEIST_STRAND_HASH_ORNATE		
//			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_ORNATE, TRUE)
//		BREAK
//		CASE ci_HEIST_STRAND_HASH_PRISON		
//			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_BIOLAB, TRUE)
//		BREAK
//		CASE ci_HEIST_STRAND_HASH_BIOLAB		
//			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_PRISON, TRUE)
//		BREAK
//		CASE ci_HEIST_STRAND_HASH_NARCOTICS 	
//			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_NARCOTIC, TRUE)	
//		BREAK
//		CASE g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job	
//			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_TUTORIAL, TRUE)	
//		BREAK
//	ENDSWITCH
	
	IF (Is_This_Tutorial_Heist_RCID_Hash(iRootContID))
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_TUTORIAL, TRUE)
	ENDIF
	
	IF (Is_This_Prison_Heist_RCID_Hash(iRootContID))
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_PRISON, TRUE)
	ENDIF
	
	IF (Is_This_Biolab_Heist_RCID_Hash(iRootContID))
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_BIOLAB, TRUE)
	ENDIF
	
	IF (Is_This_Chicken_Heist_RCID_Hash(iRootContID))
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_NARCOTIC, TRUE)
	ENDIF
	
	IF (Is_This_Ornate_Heist_RCID_Hash(iRootContID))
		SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_ORNATE, TRUE)
	ENDIF	
	
ENDPROC

/// PURPOSE:
///    Using the passed in rContID, Set that the local player has done this mid strand cutscene
/// PARAMS:
///    iRootContID - Heist strand to set.
PROC SET_LOCAL_PLAYER_COMPLETED_MID_STRAND_CUTSCENE(INT iRootContID)

//	SWITCH iRootContID
//		CASE ci_HEIST_STRAND_HASH_ORNATE		
//			SET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_ORNATE, 1)		// Note: these are set to 1 for now, if we have multiple cuts mid strand we can increment it instead or use it as a bitset
//		BREAK
//		CASE ci_HEIST_STRAND_HASH_PRISON		
//			SET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_PRISON, 1)	
//		BREAK
//		CASE ci_HEIST_STRAND_HASH_BIOLAB		
//			SET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_HUMANE, 1)	
//		BREAK
//		CASE ci_HEIST_STRAND_HASH_NARCOTICS 	
//			SET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_NARC, 1)
//		BREAK
//		CASE g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job 	
//			SET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_TUT, 1)
//		BREAK
//	ENDSWITCH
	
	IF (Is_This_Tutorial_Heist_RCID_Hash(iRootContID))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_TUT, 1)		// Note: these are set to 1 for now, if we have multiple cuts mid strand we can increment it instead or use it as a bitset
	ENDIF
	
	IF (Is_This_Prison_Heist_RCID_Hash(iRootContID))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_PRISON, 1)	
	ENDIF
	
	IF (Is_This_Biolab_Heist_RCID_Hash(iRootContID))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_HUMANE, 1)
	ENDIF
	
	IF (Is_This_Chicken_Heist_RCID_Hash(iRootContID))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_NARC, 1)
	ENDIF
	
	IF (Is_This_Ornate_Heist_RCID_Hash(iRootContID))
		SET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_ORNATE, 1)
	ENDIF	
	
ENDPROC


/// PURPOSE: Checks if a heist strand requires a cutscene to be launched
/// PARAMS:
///    iRootContID - Root Content ID of the heist strand
///    iBoardLevel - Board level that we are currently at
FUNC BOOL DOES_HEIST_REQUIRE_MID_STRAND_CUTSCENE(INT iRootContID, INT iBoardLevel)

	#IF IS_DEBUG_BUILD
		// Run with this commandline to enable the mid strand behaviour
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_HeistDisableMidStrandCuts")
			RETURN FALSE
		ENDIF
	#ENDIF

	// To prevent a situation where players are given a cutscene corona immediately after already seeing one.
	IF NOT (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].eCoronaStatus != CORONA_STATUS_IDLE)
	AND NETWORK_IS_ACTIVITY_SESSION()
		RETURN FALSE
	ENDIF

	INT iBoardLevelForCut = -1
	INT iPlayedMidStrandCutscene

	BOOL foundHeist = FALSE
	IF (Is_This_Tutorial_Heist_RCID_Hash(iRootContID))
		foundHeist = TRUE
		iBoardLevelForCut = 0
		iPlayedMidStrandCutscene = GET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_TUT)
	ENDIF
	
	IF (Is_This_Prison_Heist_RCID_Hash(iRootContID))
		foundHeist = TRUE
		iBoardLevelForCut = 0
		iPlayedMidStrandCutscene = GET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_PRISON)
	ENDIF
	
	IF (Is_This_Biolab_Heist_RCID_Hash(iRootContID))
		foundHeist = TRUE
		iBoardLevelForCut = 0
		iPlayedMidStrandCutscene = GET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_HUMANE)
	ENDIF
	
	IF (Is_This_Chicken_Heist_RCID_Hash(iRootContID))
		foundHeist = TRUE
		iBoardLevelForCut = 0
		iPlayedMidStrandCutscene = GET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_NARC)
	ENDIF
	
	IF (Is_This_Ornate_Heist_RCID_Hash(iRootContID))
		foundHeist = TRUE
		iBoardLevelForCut = 1
		iPlayedMidStrandCutscene = GET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_ORNATE)
	ENDIF	
	
	IF NOT (foundHeist)
		PRINTLN("[AMEC][CUTSCENE] DOES_HEIST_REQUIRE_MID_STRAND_CUTSCENE - Unknown heist strand: ", iRootContID)
		RETURN FALSE
	ENDIF
	
	PRINTLN("[AMEC][CUTSCENE] DOES_HEIST_REQUIRE_MID_STRAND_CUTSCENE - iPlayedMidStrandCutscene: ", iPlayedMidStrandCutscene)
	PRINTLN("[AMEC][CUTSCENE] DOES_HEIST_REQUIRE_MID_STRAND_CUTSCENE - iBoardLevelForCut: ", iBoardLevelForCut)
	PRINTLN("[AMEC][CUTSCENE] DOES_HEIST_REQUIRE_MID_STRAND_CUTSCENE - iBoardLevel: ", iBoardLevel)

	// Check we have a boardlevel to check against
	IF iBoardLevelForCut != -1
		//...yes, are we currently at that board level
		IF iBoardLevel = iBoardLevelForCut
			
			//...yes, if we have not shown a mid strand cutscene already, then return TRUE
			IF iPlayedMidStrandCutscene = 0
				RETURN TRUE
			ENDIF
		ENDIF		
	ENDIF

	// Default return FALSE
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Returns TRUE if the model of our heist member is male (used in place of IS_PED_MALE which occasionally does not work)  
FUNC BOOL GET_HEIST_MEMBER_IS_MALE(PLAYER_INDEX playerID)
	
	IF playerID != INVALID_PLAYER_INDEX()
		IF IS_NET_PLAYER_OK(playerID, FALSE)
			
			// Only if our ped is correct, check if the model matches female model
			RETURN (NOT IS_PLAYER_PED_FEMALE(playerID))
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:	
///    Retrieve a literal heist decription string based on a Finale rootContentID hash
#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_HEIST_NAME_FROM_RCONTID_HASH(INT iRootContID)

	STRING sHeistDesc = "HEIST_F9_UNKNO"
	
	IF iRootContID = 0
		RETURN (sHeistDesc)
	ENDIF
	
//	SWITCH iRootContID
//
//		// Ornate Bank - Finale
//		CASE ci_HEIST_STRAND_HASH_ORNATE 					sHeistDesc = 	"HEIST_NUM_1" BREAK
		
//		// Biolab Score - Finale
//		CASE ci_HEIST_STRAND_HASH_BIOLAB					sHeistDesc = 	"HEIST_NUM_2" BREAK	
		
//		// Prison Break - Finale
//		CASE ci_HEIST_STRAND_HASH_PRISON					sHeistDesc = 	"HEIST_NUM_3" BREAK	
		
//		// Narcotics - Finale
//		CASE ci_HEIST_STRAND_HASH_NARCOTICS					sHeistDesc = 	"HEIST_NUM_4" BREAK	
		
//		// Tutorial - Finale
//		CASE g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job		sHeistDesc = 	"HEIST_NUM_5" BREAK	
	
//	ENDSWITCH
	
	IF (Is_This_Tutorial_Heist_RCID_Hash(iRootContID))
		sHeistDesc = 	"HEIST_NUM_5"
	ENDIF
	
	IF (Is_This_Prison_Heist_RCID_Hash(iRootContID))
		sHeistDesc = 	"HEIST_NUM_3"
	ENDIF	

	IF (Is_This_Biolab_Heist_RCID_Hash(iRootContID))
		sHeistDesc = 	"HEIST_NUM_2"
	ENDIF
	
	IF (Is_This_Chicken_Heist_RCID_Hash(iRootContID))
		sHeistDesc = 	"HEIST_NUM_4"
	ENDIF
	
	IF (Is_This_Ornate_Heist_RCID_Hash(iRootContID))
		sHeistDesc = 	"HEIST_NUM_1"
	ENDIF
	
	RETURN (sHeistDesc)

ENDFUNC

FUNC STRING DEBUG_GET_HEIST_NAME_FROM_RCONTID_HASH_FOR_F9(INT iRootContID)

	STRING sHeistDesc = "HEIST_F9_UNKNO"
	
	IF iRootContID = 0
		RETURN (sHeistDesc)
	ENDIF
	
//	SWITCH iRootContID
//
//		// Ornate Bank - Finale
//		CASE ci_HEIST_STRAND_HASH_ORNATE 					sHeistDesc = 	"HEIST_F9_1" BREAK
		
//		// Biolab Score - Finale
//		CASE ci_HEIST_STRAND_HASH_BIOLAB					sHeistDesc = 	"HEIST_F9_2" BREAK	
		
//		// Prison Break - Finale
//		CASE ci_HEIST_STRAND_HASH_PRISON					sHeistDesc = 	"HEIST_F9_3" BREAK	
		
//		// Narcotics - Finale
//		CASE ci_HEIST_STRAND_HASH_NARCOTICS					sHeistDesc = 	"HEIST_F9_5" BREAK	
		
//		// Tutorial - Finale
//		CASE g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job		sHeistDesc = 	"HEIST_F9_6" BREAK	
	
//	ENDSWITCH
	
	IF (Is_This_Tutorial_Heist_RCID_Hash(iRootContID))
		sHeistDesc = 	"HEIST_F9_6"
	ENDIF

	IF (Is_This_Prison_Heist_RCID_Hash(iRootContID))
		sHeistDesc = 	"HEIST_F9_3"
	ENDIF
	
	IF (Is_This_Biolab_Heist_RCID_Hash(iRootContID))
		sHeistDesc = 	"HEIST_F9_2"
	ENDIF
	
	IF (Is_This_Chicken_Heist_RCID_Hash(iRootContID))
		sHeistDesc = 	"HEIST_F9_5"
	ENDIF
	
	IF (Is_This_Ornate_Heist_RCID_Hash(iRootContID))
		sHeistDesc = 	"HEIST_F9_1"
	ENDIF
	
	RETURN (sHeistDesc)

ENDFUNC
#ENDIF


/// PURPOSE:
///    Using the heist interior set enum, get the name of the entity sets as a string.
/// PARAMS:
///    eEntitySet - Set type.
/// RETURNS:
///    String name of entity set.
FUNC STRING GET_HEIST_ENTITY_SET_STRING(HEIST_INTERIOR_ENTITY_SETS eEntitySet)

	STRING sReturnString

	SWITCH eEntitySet
	
		DEFAULT
			PRINTLN("[AMEC][HEIST_HARDCODE] - GET_HEIST_ENTITY_SET_STRING - ERROR! eEntitySet is not valid, returning study set! Value: ", ENUM_TO_INT(eEntitySet))
			sReturnString = "Apart_Hi_StudyStuff"
			BREAK
	
		CASE HEIST_INTERIOR_NONE
			sReturnString = "Apart_Hi_StudyStuff"
			BREAK
			
		CASE HEIST_INTERIOR_ORNATE
			sReturnString = "Apart_Hi_HeistStuff"
			BREAK
			
		CASE HEIST_INTERIOR_BIOLAB
			sReturnString = "Apart_Hi_HeistBio"
			BREAK
			
		CASE HEIST_INTERIOR_PRISON
			sReturnString = "Apart_Hi_HeistPrison"
			BREAK
	
	ENDSWITCH
	
	RETURN sReturnString

ENDFUNC



/// PURPOSE:
///    Get the number of times the local player's character has completed the provided heist strand.
/// PARAMS:
///    iRootContID - Heist finale root content ID.
///    bAsLeader - TRUE for times as leader, FALSE for member.
/// RETURNS:
///    INT number of times. Error return = -1.
FUNC INT GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(INT iRootContID, BOOL bAsLeader)

	INT iReturnValue

	// First we need to find the rContID in the stats, if it doesn't exist then we can add it anew.
	IF (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_0) = iRootContID)
		
		IF bAsLeader
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_0_L))
		ELSE
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_0_M))
		ENDIF

	ELIF (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_1) = iRootContID)
	
		IF bAsLeader
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_1_L))
		ELSE
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_1_M))
		ENDIF
	
	ELIF (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_2) = iRootContID)
	
		IF bAsLeader
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_2_L))
		ELSE
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_2_M))
		ENDIF
	
	ELIF (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_3) = iRootContID)
	
		IF bAsLeader
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_3_L))
		ELSE
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_3_M))
		ENDIF
	
	ELIF (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_4) = iRootContID)
	
		IF bAsLeader
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_4_L))
		ELSE
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_4_M))
		ENDIF
	
	ELIF (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_5) = iRootContID)
	
		IF bAsLeader
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_5_L))
		ELSE
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_5_M))
		ENDIF
	
	ELIF (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_6) = iRootContID)
	
		IF bAsLeader
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_6_L))
		ELSE
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_6_M))
		ENDIF
	
	ELIF (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_7) = iRootContID)
	
		IF bAsLeader
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_7_L))
		ELSE
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_7_M))
		ENDIF
	
	ELIF (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_8) = iRootContID)
		
		IF bAsLeader
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_8_L))
		ELSE
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_8_M))
		ENDIF
	
	ELIF (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_9) = iRootContID)
	
		IF bAsLeader
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_9_L))
		ELSE
			iReturnValue = (GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_SAVED_STRAND_9_M))
		ENDIF
	
	ELSE
	
		iReturnValue = INVALID_HEIST_DATA
	
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//		IF bAsLeader
//			PRINTLN("[AMEC][HEIST_COMMON] - GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED - Heist rContID: ", iRootContID, " has been completed by local player char ", iReturnValue, " times as LEADER.")
//		ELSE
//			PRINTLN("[AMEC][HEIST_COMMON] - GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED - Heist rContID: ", iRootContID, " has been completed by local player char ", iReturnValue, " times as MEMBER.")
//		ENDIF
//	#ENDIF
	
	RETURN iReturnValue

ENDFUNC


FUNC INT GET_HEIST_TOTAL_REWARD(INT iRootContID)

	INT iReturnValue
	
	IF (Is_This_Tutorial_Heist_RCID_Hash(iRootContID))
		iReturnValue = ROUND(g_sMPTunables.fEarnings_Heists_Finale_replay_cash_reward * g_sMPTunables.fheist_strand_multiplier_tutorial *ci_HEIST_TUTORIAL_REWARD_TOTAL)
		PRINTLN("[AMEC][HEIST_COMMON] - GET_HEIST_TOTAL_REWARD - Setting total reward at: ",iReturnValue)
	ENDIF
	
	IF (Is_This_Prison_Heist_RCID_Hash(iRootContID))
		iReturnValue = ROUND(g_sMPTunables.fEarnings_Heists_Finale_replay_cash_reward *  g_sMPTunables.fheist_strand_multiplier_prison *ci_HEIST_PRISON_REWARD_TOTAL)
		PRINTLN("[AMEC][HEIST_COMMON] - GET_HEIST_TOTAL_REWARD - Setting total reward at: ",iReturnValue)
	ENDIF
	
	IF (Is_This_Biolab_Heist_RCID_Hash(iRootContID))
		iReturnValue = ROUND(g_sMPTunables.fEarnings_Heists_Finale_replay_cash_reward * g_sMPTunables.fheist_strand_multiplier_biolab * ci_HEIST_BIOLAB_REWARD_TOTAL)
		PRINTLN("[AMEC][HEIST_COMMON] - GET_HEIST_TOTAL_REWARD - Setting total reward at: ",iReturnValue)
	ENDIF
	
	IF (Is_This_Chicken_Heist_RCID_Hash(iRootContID))
		iReturnValue = ROUND(g_sMPTunables.fEarnings_Heists_Finale_replay_cash_reward * g_sMPTunables.fheist_strand_multiplier_narcotics *ci_HEIST_CHICKEN_REWARD_TOTAL)
		PRINTLN("[AMEC][HEIST_COMMON] - GET_HEIST_TOTAL_REWARD - Setting total reward at: ",iReturnValue)
	ENDIF
	
	IF (Is_This_Ornate_Heist_RCID_Hash(iRootContID))
		iReturnValue = ROUND(g_sMPTunables.fEarnings_Heists_Finale_replay_cash_reward * g_sMPTunables.fheist_strand_multiplier_ornate * ci_HEIST_ORNATE_REWARD_TOTAL)
		PRINTLN("[AMEC][HEIST_COMMON] - GET_HEIST_TOTAL_REWARD - Setting total reward at: ",iReturnValue)
	ENDIF	
	
	RETURN iReturnValue

ENDFUNC

/// PURPOSE:
///    Returns FALSE if the leader should not pay the entrance fee for the upcoming Heist.
FUNC BOOL SHOULD_LEADER_PAY_FOR_THIS_HEIST(INT iRootContentID)
	
	// Currently only concerned about the tutorial heist
	IF iRootContentID = g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job 
	
		// If we have yet to play this strand as leader, do not charge us.
		IF GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(iRootContentID, TRUE) <= 0
			PRINTLN("[AMEC][HEIST_COMMON] - SHOULD_LEADER_PAY_FOR_THIS_HEIST - Player has NOT completed TUTORIAL heist as leader before, Do not charge.")
			RETURN FALSE
			
		#IF IS_DEBUG_BUILD	
		ELSE
			PRINTLN("[AMEC][HEIST_COMMON] - SHOULD_LEADER_PAY_FOR_THIS_HEIST - Player has completed TUTORIAL heist as leader before, Charge the player. # of plays = ", GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(iRootContentID, TRUE))
		#ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

//FUNC FLOAT GET_EXPENSES_HEIST_COST_PERCENTAGE_VALUE(INT iRootContentID)
//	IF iRootContentID = g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job 
//		RETURN g_sMPTunables.fExpenses_Heist_Cost_Percentage_Fleeca
//		
//	ELIF iRootContentID = g_sMPTUNABLES.iroot_id_HASH_The_Humane_Labs_Raid
//		RETURN g_sMPTunables.fExpenses_Heist_Cost_Percentage_Humane_Labs
//		
//	ELIF iRootContentID = g_sMPTUNABLES.iroot_id_HASH_The_Prison_Break
//		RETURN g_sMPTunables.fExpenses_Heist_Cost_Percentage_Prison_Break
//		
//	ELIF iRootContentID = g_sMPTUNABLES.iroot_id_HASH_Series_A_Funding
//		RETURN g_sMPTunables.fExpenses_Heist_Cost_Percentage_Series_A
//		
//	ELIF iRootContentID = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job
//	OR iRootContentID = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job2
//		RETURN g_sMPTunables.fExpenses_Heist_Cost_Percentage_Pacific_Standard
//	ENDIF
//	
//	RETURN 10.0
//ENDFUNC

FUNC INT GET_HEIST_ENTRY_FEE_FROM_ROOT_CONTENT_ID(INT iRootContentID)
	IF iRootContentID = g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job 
		RETURN g_sMPTunables.iHeist_Setup_Cost_Fleeca
		
	ELIF iRootContentID = g_sMPTUNABLES.iroot_id_HASH_The_Humane_Labs_Raid
		RETURN g_sMPTunables.iHeist_Setup_Cost_Humane_Labs
		
	ELIF iRootContentID = g_sMPTUNABLES.iroot_id_HASH_The_Prison_Break
		RETURN g_sMPTunables.iHeist_Setup_Cost_Prison_Break
		
	ELIF iRootContentID = g_sMPTUNABLES.iroot_id_HASH_Series_A_Funding
		RETURN g_sMPTunables.iHeist_Setup_Cost_Series_A
		
	ELIF iRootContentID = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job
	OR iRootContentID = g_sMPTUNABLES.iroot_id_HASH_The_Pacific_Standard_Job2
		RETURN g_sMPTunables.iHeist_Setup_Cost_Pacific_Standard
	ENDIF
	
	PRINTLN("[AMEC][HEIST_COMMON] GET_HEIST_ENTRY_FEE_FROM_ROOT_CONTENT_ID - Unrecognised Root Content ID - Return ci_HEIST_COST_DEFAULT (", ci_HEIST_COST_DEFAULT,")")
	RETURN ci_HEIST_COST_DEFAULT
ENDFUNC

FUNC INT CALCULATE_HEIST_STRAND_ENTRY_FEE(INT iPaymentBand, INT iRootContentID, BOOL bSkipLeaderCheck = FALSE)
	UNUSED_PARAMETER(iPaymentBand)

	#IF IS_DEBUG_BUILD
	PRINTLN("[AMEC][HEIST_COMMON] - DEBUG_PRINTCALLSTACK - CALCULATE_HEIST_STRAND_ENTRY_FEE, bSkipLeaderCheck = ", PICK_STRING(bSkipLeaderCheck, "TRUE", "FALSE"))
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	// Intially check if we should pay for the upcming heist
	IF NOT bSkipLeaderCheck
		IF NOT SHOULD_LEADER_PAY_FOR_THIS_HEIST(iRootContentID)
			PRINTLN("[AMEC][HEIST_COMMON] - CALCULATE_HEIST_STRAND_ENTRY_FEE - SHOULD_LEADER_PAY_FOR_THIS_HEIST = FALSE. Value: $0")
			RETURN 0
		ENDIF
	ENDIF
		
	INT iReturnValue = INVALID_HEIST_DATA
	
//	iReturnValue = ROUND(TO_FLOAT(GET_CASH_VALUE_FROM_CREATOR(iPaymentBand)) / (TO_FLOAT(HEIST_CUT_PERCENT_TOTAL) / GET_EXPENSES_HEIST_COST_PERCENTAGE_VALUE(iRootContentID)))
	
	iReturnValue = GET_HEIST_ENTRY_FEE_FROM_ROOT_CONTENT_ID(iRootContentID)
	
	IF iReturnValue < 0
		PRINTLN("[AMEC][HEIST_COMMON] - CALCULATE_HEIST_STRAND_ENTRY_FEE - Entry fee is INVALID, clamping to default value. Value: ", iReturnValue)
		iReturnValue = ci_HEIST_COST_DEFAULT
	ENDIF
	
	PRINTLN("[AMEC][HEIST_COMMON] - CALCULATE_HEIST_STRAND_ENTRY_FEE - Calculated cost of: ",iReturnValue)
	
	RETURN iReturnValue
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check retrieve the Heist Intro PhonecallID based on the rootContentID of the Heist Finale
//
// INPUT PARAMS:		paramFinaleRcidHash				The Finale rootContentID Hash
// RETURN VALUE:		TEXT_LABEL_15					The intro to this Heist phonecall rootID
FUNC TEXT_LABEL_15 Get_Phonecall_RootID_For_Triggering_Heist(INT paramFinaleRcidHash)

	TEXT_LABEL_15 phonecallRootID = ""

	// Ornate Bank Heist?
	IF (Is_This_Ornate_Heist_RCID_Hash(paramFinaleRcidHash))
		phonecallRootID = "LC19"
		RETURN (phonecallRootID)
	ENDIF
	
	// Biolab Heist?
	IF (Is_This_Biolab_Heist_RCID_Hash(paramFinaleRcidHash))
		phonecallRootID = "LC4"
		RETURN (phonecallRootID)
	ENDIF
	
	// Prison Heist?
	IF (Is_This_Prison_Heist_RCID_Hash(paramFinaleRcidHash))
		phonecallRootID = "LC6"
		RETURN (phonecallRootID)
	ENDIF
	
	// Narcotics Heist?
	IF (Is_This_Chicken_Heist_RCID_Hash(paramFinaleRcidHash))
		phonecallRootID = "LC17"
		RETURN (phonecallRootID)
	ENDIF
	
	// Tutorial Heist?
	IF (Is_This_Tutorial_Heist_RCID_Hash(paramFinaleRcidHash))
		phonecallRootID = "LC20"
		RETURN (phonecallRootID)
	ENDIF
	
	RETURN (phonecallRootID)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check retrieve the Heist Completion PhonecallID based on the rootContentID of the Heist Finale
//
// INPUT PARAMS:		paramFinaleRcidHash				The Finale rootContentID Hash
// RETURN VALUE:		TEXT_LABEL_15					The Heist Completion phonecall rootID
FUNC TEXT_LABEL_15 Get_Phonecall_RootID_For_Heist_Completion_Phonecall(INT paramFinaleRcidHash)

	TEXT_LABEL_15 phonecallRootID = ""
	
	// Tutorial Heist?
	IF (Is_This_Tutorial_Heist_RCID_Hash(paramFinaleRcidHash))
		phonecallRootID = "LC11"
		RETURN (phonecallRootID)
	ENDIF
	
	// Prison Heist?
	IF (Is_This_Prison_Heist_RCID_Hash(paramFinaleRcidHash))
		phonecallRootID = "LC7"
		RETURN (phonecallRootID)
	ENDIF
	
	// Biolab Heist?
	IF (Is_This_Biolab_Heist_RCID_Hash(paramFinaleRcidHash))
		phonecallRootID = "LC5"
		RETURN (phonecallRootID)
	ENDIF
	
	// Narcotics Heist?
	IF (Is_This_Chicken_Heist_RCID_Hash(paramFinaleRcidHash))
		phonecallRootID = "LC18"
		RETURN (phonecallRootID)
	ENDIF

	// Ornate Bank Heist?
	IF (Is_This_Ornate_Heist_RCID_Hash(paramFinaleRcidHash))
		phonecallRootID = "LC3"
		RETURN (phonecallRootID)
	ENDIF
	
	RETURN (phonecallRootID)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Heist Finale rootContentID from the hash value
//
// INPUT PARAMS:		paramHashFinaleRcid				The Hash Finale rootContentID
// RETURN VALUE:		TEXT_LABEL_23					The Finale rootContentID
FUNC TEXT_LABEL_23 Get_Finale_RootContentID_From_Hash(INT paramHashFinaleRcid)
	
	TEXT_LABEL_23 rootContentID = ""

//	SWITCH (paramHashFinaleRcid)
//		CASE ci_HEIST_STRAND_HASH_ORNATE					rootContentID = "hKSf9RCT8UiaZlykyGrMwg"	BREAK			// Ornate Bank Heist
//		CASE ci_HEIST_STRAND_HASH_BIOLAB					rootContentID = "a_hWnpMUz0-7Yd_Rc5pJ4w"	BREAK			// Biolab Heist
//		CASE ci_HEIST_STRAND_HASH_PRISON					rootContentID = "A6UBSyF61kiveglc58lm2Q"	BREAK			// Prison Heist
//		CASE ci_HEIST_STRAND_HASH_NARCOTICS					rootContentID = "7r5AKL5aB0qe9HiDy3nW8w"	BREAK			// Narcotics Heist
//		CASE g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job		rootContentID = "33TxqLipLUintwlU_YDzMg"	BREAK			// Tutorial Heist
//	ENDSWITCH
	
	IF (Is_This_Tutorial_Heist_RCID_Hash(paramHashFinaleRcid))
		rootContentID = "33TxqLipLUintwlU_YDzMg"
	ENDIF
	
	IF (Is_This_Prison_Heist_RCID_Hash(paramHashFinaleRcid))
		rootContentID = "A6UBSyF61kiveglc58lm2Q"
	ENDIF
	
	IF (Is_This_Biolab_Heist_RCID_Hash(paramHashFinaleRcid))
		rootContentID = "a_hWnpMUz0-7Yd_Rc5pJ4w"
	ENDIF
	
	IF (Is_This_Chicken_Heist_RCID_Hash(paramHashFinaleRcid))
		rootContentID = "7r5AKL5aB0qe9HiDy3nW8w"
	ENDIF
	
	IF (Is_This_Ornate_Heist_RCID_Hash(paramHashFinaleRcid))
		rootContentID = "hKSf9RCT8UiaZlykyGrMwg"
	ENDIF
	
	RETURN (rootContentID)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Hardcoded function to Retrieve the Mocap Cutscene name
//
// INPUT PARAMS:			paramHashFinaleRcid			RootContentID Hash of the Fianle mission
//							paramIsPrePlanning			TRUE if the PrePlanning cutscene is required
//							paramIsFinale				TRUE if the Finale cutscene is required
//							paramIsPostFinale			TRUE if post-Finale cutscene is required
//							paramIsMidStrand			TRUE if mid-strand cutscene is required
// RETURN VALUE:			TEXT_LABEL_23				The Mocap Cutscene name, or "" if none
//
// NOTES:	This should be replaced with data retrieved from the cloud as part of the Finale Mission data
FUNC TEXT_LABEL_23 TEMP_Get_Mocap_Name_From_FinaleRCID_Hash(INT paramHashFinaleRcid, BOOL paramIsPrePlanning, BOOL paramIsFinale, BOOL paramIsPostFinale, BOOL paramIsMidStrand, BOOL paramIsTutorial)

	TEXT_LABEL_23 theReturnString = ""
	
	// Until we add a Post-Finale cutscene for Prison Heist
	UNUSED_PARAMETER(paramIsPostFinale)
	
//	SWITCH (paramHashFinaleRcid)
//		// Ornate Bank Heist
//		CASE ci_HEIST_STRAND_HASH_ORNATE
//			IF (paramIsPrePlanning)
//				theReturnString = "MPH_PAC_INT"
//			ELIF (paramIsMidStrand)
//				theReturnString = "MPH_PAC_MID"
//			ELIF (paramIsFinale)
//				theReturnString = "MPH_PAC_FIN_INT"
//			ENDIF
//			BREAK

//		// Biolab Score Heist
//		CASE ci_HEIST_STRAND_HASH_BIOLAB
//			IF (paramIsPrePlanning)
//				theReturnString = "MPH_HUM_INT" // Also used for removing the office chair.
//			ELIF (paramIsMidStrand)
//				theReturnString = "MPH_HUM_MID"
//			ELIF (paramIsFinale)
//				theReturnString = "MPH_HUM_FIN_INT"
//			ENDIF
//			BREAK
			
		// Prison Break Heist
//		CASE ci_HEIST_STRAND_HASH_PRISON
//			IF (paramIsPrePlanning)
//				theReturnString = "MPH_PRI_INT" // Also used for removing the office chair.
//			ELIF (paramIsMidStrand)
//				theReturnString = "MPH_PRI_MID"
//			ELIF (paramIsFinale)
//				theReturnString = "MPH_PRI_FIN_INT"
//			ENDIF
//			BREAK
			
//		// Narcotics Heist
//		CASE ci_HEIST_STRAND_HASH_NARCOTICS
//			IF (paramIsPrePlanning)
//				theReturnString = "MPH_NAR_INT"
//			ELIF (paramIsMidStrand)
//				theReturnString = "MPH_NAR_MID"
//			ELIF (paramIsFinale)
//				theReturnString = "MPH_NAR_FIN_INT"
//			ENDIF
//			BREAK
			
//		// Tutorial Heist
//		CASE g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job
//			IF (paramIsPrePlanning) OR (paramIsTutorial)
//				theReturnString = "MPH_TUT_INT"
//			ELIF (paramIsMidStrand)
//				theReturnString = "MPH_TUT_MID"
//			ELIF (paramIsFinale)
//				theReturnString = "MPH_TUT_FIN_INT"
//			ENDIF
//			BREAK
			
//		// UNKNOWN FINALE ROOT CONTENT ID HASH
//		DEFAULT
//			PRINTLN(".KGM [Heist][TEMP HARDCODED]: Unknown Heist Finale RootContentID Hash when looking for cutscene: ", paramHashFinaleRcid)
//			SCRIPT_ASSERT("Unknown Heist Finale RootContentID Hash when looking for cutscene. Tell Keith.")
//			BREAK
		
//	ENDSWITCH
	
	BOOL foundHeist = FALSE
	IF (Is_This_Tutorial_Heist_RCID_Hash(paramHashFinaleRcid))
		foundHeist = TRUE
		IF (paramIsPrePlanning) OR (paramIsTutorial)
			theReturnString = "MPH_TUT_INT"
		ELIF (paramIsMidStrand)
			theReturnString = "MPH_TUT_MID"
		ELIF (paramIsFinale)
			theReturnString = "MPH_TUT_FIN_INT"
		ENDIF
	ENDIF
	
	IF (Is_This_Prison_Heist_RCID_Hash(paramHashFinaleRcid))
		foundHeist = TRUE
		IF (paramIsPrePlanning)
			theReturnString = "APA_PRI_INT" // Also used for removing the office chair.
		ELIF (paramIsMidStrand)
			theReturnString = "MPH_PRI_MID"
		ELIF (paramIsFinale)
			theReturnString = "MPH_PRI_FIN_INT"
		ENDIF
	ENDIF
	
	IF (Is_This_Biolab_Heist_RCID_Hash(paramHashFinaleRcid))
		foundHeist = TRUE
		IF (paramIsPrePlanning)
			theReturnString = "MPH_HUM_INT" // Also used for removing the office chair.
		ELIF (paramIsMidStrand)
			theReturnString = "MPH_HUM_MID"
		ELIF (paramIsFinale)
			theReturnString = "APA_HUM_FIN_INT"
		ENDIF
	ENDIF
	
	IF (Is_This_Chicken_Heist_RCID_Hash(paramHashFinaleRcid))
		foundHeist = TRUE
		IF (paramIsPrePlanning)
			theReturnString = "APA_NAR_INT"
		ELIF (paramIsMidStrand)
			theReturnString = "APA_NAR_MID" // Updated for GB/Apartments - url:bugstar:2599502
		ELIF (paramIsFinale)
			theReturnString = "MPH_NAR_FIN_INT"
		ENDIF
	ENDIF
	
	IF (Is_This_Ornate_Heist_RCID_Hash(paramHashFinaleRcid))
		foundHeist = TRUE
		IF (paramIsPrePlanning)
			theReturnString = "MPH_PAC_INT"
		ELIF (paramIsMidStrand)
			theReturnString = "MPH_PAC_MID"
		ELIF (paramIsFinale)
			theReturnString = "MPH_PAC_FIN_INT"
		ENDIF
	ENDIF
	
	IF NOT (foundHeist)
		PRINTLN(".KGM [Heist][TEMP HARDCODED]: Unknown Heist Finale RootContentID Hash when looking for cutscene: ", paramHashFinaleRcid)
		SCRIPT_ASSERT("Unknown Heist Finale RootContentID Hash when looking for cutscene. Tell Keith.")
	ENDIF
	
	RETURN (theReturnString)

ENDFUNC


// PURPOSE:	Hardcoded function to Retrieve the Mocap Number of Participants name
FUNC INT TEMP_Get_Mocap_Participant_Number_From_FinaleRCID_Hash(INT paramHashFinaleRcid)
	
	// Tutorial Heist
	IF (Is_This_Tutorial_Heist_RCID_Hash(paramHashFinaleRcid))
		RETURN (MAX_HEIST_TUTORIAL_PARTICIPANTS)
	ENDIF
	
	// All other heists
	RETURN (MAX_HEIST_CUTSCENE_PARTICIPANTS)

ENDFUNC

FUNC INT GET_HEISTS_COMPLETED_BITSET()
	INT iBitSet
	
	// Original Heists
	IF GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(Get_Tutorial_Heist_RCID_Hash(), TRUE) > 0
	OR GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(Get_Tutorial_Heist_RCID_Hash(), FALSE) > 0
		SET_BIT(iBitSet, ciHEISTS_COMPLETED_BS_HEISTS_FLEECA_JOB)
	ENDIF
	IF GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(Get_Prison_Heist_RCID_Hash(), TRUE) > 0
	OR GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(Get_Prison_Heist_RCID_Hash(), FALSE) > 0
		SET_BIT(iBitSet, ciHEISTS_COMPLETED_BS_HEISTS_PRISON_BREAK)
	ENDIF
	IF GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(Get_Biolab_Heist_RCID_Hash(), TRUE) > 0
	OR GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(Get_Biolab_Heist_RCID_Hash(), FALSE) > 0
		SET_BIT(iBitSet, ciHEISTS_COMPLETED_BS_HEISTS_HUMANE_LAB_RAID)
	ENDIF
	IF GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(Get_Chicken_Heist_RCID_Hash(), TRUE) > 0
	OR GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(Get_Chicken_Heist_RCID_Hash(), FALSE) > 0
		SET_BIT(iBitSet, ciHEISTS_COMPLETED_BS_HEISTS_SERIES_A_FUNDING)
	ENDIF
	IF GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(Get_Ornate_Heist_RCID_Hash(), TRUE) > 0
	OR GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(Get_Ornate_Heist_RCID_Hash(), FALSE) > 0
		SET_BIT(iBitSet, ciHEISTS_COMPLETED_BS_HEISTS_PACIFIC_STANDARD)
	ENDIF
	
	// The Doomsday Heist
	IF HAS_PLAYER_COMPLETED_GANG_OPS_MISSION(PLAYER_ID(), GANG_OPS_MISSION_INSTANCED_IAABASE_FINALE, TRUE)
		SET_BIT(iBitSet, ciHEISTS_COMPLETED_BS_DOOMSDAY_THE_DATA_BREACHES)
	ENDIF
	IF HAS_PLAYER_COMPLETED_GANG_OPS_MISSION(PLAYER_ID(), GANG_OPS_MISSION_INSTANCED_SUBMARINE_FINALE, TRUE)
		SET_BIT(iBitSet, ciHEISTS_COMPLETED_BS_DOOMSDAY_THE_BOGDAN_PROBLEM)
	ENDIF
	IF HAS_PLAYER_COMPLETED_GANG_OPS_MISSION(PLAYER_ID(), GANG_OPS_MISSION_INSTANCED_MISSILE_SILO_FINALE_P2, TRUE)
		SET_BIT(iBitSet, ciHEISTS_COMPLETED_BS_DOOMSDAY_THE_DOOMSDAY_SCENARIO)
	ENDIF
	
	// The Diamond Casino Heist
	IF HAS_LOCAL_PLAYER_COMPLETED_CASINO_HEIST(CASINO_HEIST_APPROACH_TYPE__STEALTH, TRUE)
		SET_BIT(iBitSet, ciHEISTS_COMPLETED_BS_CASINO_SILENT_AND_SNEAKY)
	ENDIF
	IF HAS_LOCAL_PLAYER_COMPLETED_CASINO_HEIST(CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE, TRUE)
		SET_BIT(iBitSet, ciHEISTS_COMPLETED_BS_CASINO_THE_BIG_CON)
	ENDIF
	IF HAS_LOCAL_PLAYER_COMPLETED_CASINO_HEIST(CASINO_HEIST_APPROACH_TYPE__DIRECT, TRUE)
		SET_BIT(iBitSet, ciHEISTS_COMPLETED_BS_CASINO_AGGRESSIVE)
	ENDIF
	
	// The Cayo Perico Heist
	IF HAS_LOCAL_PLAYER_COMPLETED_ISLAND_HEIST_AS_LEADER(TRUE)
	OR HAS_LOCAL_PLAYER_COMPLETED_ISLAND_HEIST_AS_CREW(TRUE)
		SET_BIT(iBitSet, ciHEISTS_COMPLETED_BS_ISLAND)
	ENDIF
	
	RETURN iBitSet
ENDFUNC



// End of file.
