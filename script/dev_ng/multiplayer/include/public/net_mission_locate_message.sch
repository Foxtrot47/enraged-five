// Name: net_global_cnc_message.sch
// Author: Robert Wright
// Date: 31/05/2012
// Purpose: Monitor state of player and display large message on screen ('WASTED', 'BUSTED' etc)
USING "globals.sch"
USING "commands_network.sch"
USING "net_mission_info.sch"
USING "net_team_info.sch"




// ===========================================================================================================
//     EXISTING ROUTINES STILL REQUIRED BY THE NEW LOCATE MESSAGE SYSTEM
// ===========================================================================================================

// PURPOSE:	Get the Locate Text Vector Rotation
//
// INPUT PARAMS:		paramLocatePos			Coords for the locate
//						paramPointAtPos			Coords of the position the message should point at
// RETURN VALUE:		VECTOR					The Rotation to use
FUNC VECTOR GET_MISSION_LOCATE_ROTATION_VECTOR(VECTOR paramLocatePos, VECTOR paramPointAtPos)

	//If the vector is ZERO then Return Zero
	IF IS_VECTOR_ZERO(paramPointAtPos)		
		RETURN <<0.0, 0.0, 0.0>>
	ENDIF
	
	//Get the heading and flip it by 180
	VECTOR returnRotation
	returnRotation.z = 180 - GET_HEADING_FROM_VECTOR_2D(paramPointAtPos.x - paramLocatePos.x, paramPointAtPos.y - paramLocatePos.y)
	//remove the x and y
	returnRotation.x = 0.0
	returnRotation.y = 0.0
	
	//return the correct rotational vector
	RETURN returnRotation
	
ENDFUNC





// ===========================================================================================================
//     EXISTING ROUTINES THAT WILL BECOME OBSOLETE WHEN EVERYTHING USES THE NEW LOCATE MESSAGE SYSTEM
// ===========================================================================================================

// KGM 18/9/12: This will probably become obsolete when Freemode missions are launched using the new Mission Controller with Mission Data
FUNC STRING GET_MISSION_LOCATION_STRING_FROM_INT(INT paramMissionType)

	// KGM 12/11/12: FM Missions use mission data
	SWITCH paramMissionType
		CASE FMMC_TYPE_MG_ARM_WRESTLING			// eFM_ARM_WRESTLING
		CASE FMMC_TYPE_MG_DARTS					// eFM_DARTS
		CASE FMMC_TYPE_MG_GOLF					// eFM_GOLF
		CASE FMMC_TYPE_MG_PILOT_SCHOOL			// eFM_PILOT_SCHOOL
		CASE FMMC_TYPE_MG_SHOOTING_RANGE		// eFM_SHOOTING_RANGE
		CASE FMMC_TYPE_MG_TENNIS				// eFM_TENNIS
		CASE FMMC_TYPE_BASE_JUMP				// eFM_BASEJUMP_CLOUD
		CASE FMMC_TYPE_DEATHMATCH				// eFM_DEATHMATCH_CLOUD
		CASE FMMC_TYPE_IMPROMPTU_DM				// eFM_DEATHMATCH_CLOUD
		CASE FMMC_TYPE_GANGHIDEOUT				// eFM_GANG_ATTACK_CLOUD
		CASE FMMC_TYPE_SURVIVAL					// eFM_SURVIVAL_CLOUD
		CASE FMMC_TYPE_MISSION					// eFM_MISSION_CLOUD
		CASE FMMC_TYPE_RACE						// eFM_RACE_CLOUD
			RETURN (GET_MP_MISSION_NAME_TEXT_LABEL(Convert_FM_Mission_Type_To_MissionID(paramMissionType), TEAM_INVALID))
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP...BWW...GET_MISSION_LOCATION_STRING_FROM_INT() - iType passed unknown: ") NET_PRINT_INT(paramMissionType) NET_NL()
		SCRIPT_ASSERT("GET_MISSION_LOCATION_STRING_FROM_INT() - Unknown Activity ID. See Console Log.")
	#ENDIF
	
	RETURN "FM_LOC_99_GEN"	
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

//Loads the scale form and sets it up for FREEMODE
// KGM 18/9/12: This extracts the message setup section of INIT_MISSION_LOCATE_MESSAGE_FM - this may be replaced by a generic CnC/FM function at some point
// KGM 2/10/12: THIS SHOULD ONLY USE THE SCALEFORM 'FREEMODE' MISSION NAME MOVIES - THEY EXPECT ADDITIONAL DETAILS
// KGM 23/10/12: Need to make sure all parameters are filled even if there is nothing visible to display
// NOTES:	This will become obsolete but is currently still required until all activities are launched through the MissionAtCoords system
PROC STORE_MISSION_LOCATE_MESSAGE_DETAILS_FM(SCALEFORM_INDEX movieID, INT iType, STRING paramMissionName = NULL, INT paramPercentage = -1, STRING sCreatorName = NULL, STRING sCreatorTL = NULL, INT iLesRating = 0)

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP...BWW...STORE_MISSION_LOCATE_MESSAGE_DETAILS_FM():") NET_NL()
		NET_PRINT("         TYPE: ")
		NET_PRINT(GET_MISSION_LOCATION_STRING_FROM_INT(iType))
		NET_NL()
		NET_PRINT("         MISSION: ")
		IF (IS_STRING_NULL_OR_EMPTY(paramMissionName))
			NET_PRINT("none")
		ELSE
			NET_PRINT(paramMissionName)
		ENDIF
		NET_NL()
		NET_PRINT("         CREATOR: ")
		IF (IS_STRING_NULL_OR_EMPTY(sCreatorName))
		AND (IS_STRING_NULL_OR_EMPTY(sCreatorTL))
			NET_PRINT("none")
		ELSE
			IF NOT (IS_STRING_NULL_OR_EMPTY(sCreatorName))
				NET_PRINT(sCreatorName)
			ELSE
				NET_PRINT(GET_STRING_FROM_TEXT_FILE(sCreatorTL))
			ENDIF
		ENDIF
		NET_NL()
		NET_PRINT("         PERCENT: ")
		IF (paramPercentage < 0)
			NET_PRINT("none")
		ELSE
			NET_PRINT_INT(paramPercentage)
			NET_PRINT("%")
		ENDIF
		NET_NL()
	#ENDIF

	// If there is no Mission Name then display the Mission Type in the Mission Name slot - this does mean I'll need to pass in a blank string to pad out any unused slots

	BOOL missionNameDisplayed = FALSE
	BOOL creatorNameDisplayed = FALSE

	// Mission Info
	BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_MISSION_INFO")
		// Mission Name - this may be from the mission creator
		IF NOT (IS_STRING_NULL_OR_EMPTY(paramMissionName))
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(paramMissionName)
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			missionNameDisplayed = TRUE
		ENDIF
		
		// Mission Type - this will use the big display text if there is no Mission Name
		BEGIN_TEXT_COMMAND_SCALEFORM_STRING(GET_MISSION_LOCATION_STRING_FROM_INT(iType))
		END_TEXT_COMMAND_SCALEFORM_STRING()
		
		IF NOT (IS_STRING_NULL_OR_EMPTY(sCreatorName))
		OR NOT (IS_STRING_NULL_OR_EMPTY(sCreatorTL))
			// Player Info (Creator)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				IF NOT (IS_STRING_NULL_OR_EMPTY(sCreatorName))
					// Used the passed in Creator Name string
					ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(sCreatorName)
				ELSE
					// Used the passed in Creator TextLabel
					IF NOT (IS_STRING_NULL_OR_EMPTY(sCreatorTL))
						ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(sCreatorTL)
					ENDIF
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
			
			creatorNameDisplayed = TRUE
		ENDIF
		
		// Need to pad out any unused strings so that the percentage appears in the correct place
		IF NOT (missionNameDisplayed)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(" ")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF

		IF NOT (creatorNameDisplayed)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(" ")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		// Percentage as a string so we append the % symbol - only display it if it is positive, otherwise pad out the parameter
		IF (paramPercentage >= 0)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PERCENTAGE")
				ADD_TEXT_COMPONENT_INTEGER(paramPercentage)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(" ")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		IF iLesRating > 0
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("LES_RATING")
				ADD_TEXT_COMPONENT_INTEGER(iLesRating)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
	END_SCALEFORM_MOVIE_METHOD()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// Called every frame while message is active. Draws it to screen and checks cleanup timer
// NOTES:	This will become obsolete when all activities are launched through the MissionsAtCoords system
PROC SHOW_MISSION_LOCATE_MESSAGE(SCALEFORM_INDEX &movieID, VECTOR vLocate)
	IF HAS_SCALEFORM_MOVIE_LOADED(movieID)
		//Draw that marker
		DRAW_SCALEFORM_MOVIE_3D_SOLID(movieID, <<vLocate.x, vLocate.y, vLocate.z + 0.5>>, GET_MISSION_LOCATE_ROTATION_VECTOR(vLocate, GET_FINAL_RENDERED_CAM_COORD()), <<1.0,1.0,1.0>>, <<3.0, 3.0, 1.0>>)
	ENDIF
ENDPROC


//Loads the scale form and sets it up for FREEMODE
// NOTES:	This will become obsolete when all activities are launched through the MissionsAtCoords system
FUNC BOOL INIT_MISSION_LOCATE_MESSAGE_FM(SCALEFORM_INDEX &movieID, INT iType, STRING paramMissionName = NULL, INT paramPercentage = -1, STRING sCreatorName = NULL, STRING sCreatorTL = NULL, INT iLesRating = 0) 

	IF HAS_SCALEFORM_MOVIE_LOADED(movieID)
		// KGM 18/9/12: This calls a new function that is also called by the new Locate Message system
		STORE_MISSION_LOCATE_MESSAGE_DETAILS_FM(movieID, iType, paramMissionName, paramPercentage, sCreatorName, sCreatorTL, iLesRating)
		RETURN TRUE
	ELSE
		movieID = REQUEST_SCALEFORM_MOVIE("MP_MISSION_NAME_FREEMODE")
	ENDIF

	RETURN FALSE
ENDFUNC 

// Mark the movie as no longer needed and reset globals
// NOTES:	This will become obsolete when all activities are launched through the MissionsAtCoords system
PROC CLEANUP_MISSION_LOCATE_MESSAGE(SCALEFORM_INDEX &movieID)
	// Speirs commented out 28/06/2012, was spamming FM DM
//	#IF IS_DEBUG_BUILD
//		NET_PRINT("[BW@MESSAGE] CLEANUP_MISSION_LOCATE_MESSAGE - cleaning up movie") NET_NL()
//	#ENDIF
	//Kill it
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(movieID)
ENDPROC




// ===========================================================================================================
//      Player Broadcast Details Data Storage for SCTV (spectating) players
// ===========================================================================================================
// -----------------------------------------------------------------------------------------------------------
//      The Spectated Player - Storing the details data for SCTV Players to access
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear on set of 'Details' on the player broadcast storage array for access by SCTV players
//
// INPUT PARAMS:		paramArrayPos		The array position for the data within the SCTV details storage array - matches this player's movie array position
PROC Clear_One_Details_Data_For_Storage_For_SCTV(INT paramArrayPos)

	INT playerAsInt = NATIVE_TO_INT(PLAYER_ID())

	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramArrayPos].cdsctv_onDisplay	= FALSE
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramArrayPos].cdsctv_position		= << 0.0, 0.0, 0.0 >>
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramArrayPos].cdsctv_missionID	= eNULL_MISSION
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramArrayPos].cdsctv_missionTitle	= ""
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramArrayPos].cdsctv_secondLine	= ""
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramArrayPos].cdsctv_restriction	= FALSE
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramArrayPos].cdsctv_verified		= FALSE
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramArrayPos].cdsctv_textColour	= HUD_COLOUR_PURE_WHITE
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramArrayPos].cdsctv_tod			= -1
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramArrayPos].cdsctv_minPlayers	= -1
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramArrayPos].cdsctv_maxPlayers	= -1
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramArrayPos].cdsctv_userRating	= -1
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store any details that will need to be accessible to SCTV players
//
// INPUT PARAMS:		paramMessageID			The ID of the message being updated (this is just the array position)
//						paramMissionID			The Mission ID
//						paramMissionTitle		The Mission Title (if it can't be generated from the MissionID)
//						paramSecondLine			The Mission Name, if required, or the restriction text
//						paramRestriction		TRUE if the mission can't launch due to a restriction, otherwise FALSE for no restrictions
//						paramVerified			TRUE if this is a Rockstar Verified mission
//						paramTextColour			The colour to display the Mission Title in
//						paramTOD				The Time Of Day value if a TOD restriction exists
//
// NOTES:	This won't store the 'onDisplay' flag or the position vector - these get updated by the display routine
//			This won't update the data if it is 'onDisplay' already - it's player broadcast data so should only change if necessary
PROC Store_Details_For_SCTV_Players(INT				paramMessageID,
									MP_MISSION		paramMissionID,
									TEXT_LABEL_23	paramMissionTitle,
									TEXT_LABEL_63	paramSecondLine,
									BOOL			paramRestriction,
									BOOL			paramVerified,
									HUD_COLOURS		paramTextColour,
									INT				paramTOD,
									INT				paramMinPlayers,
									INT				paramMaxPlayers,
									INT				paramUserRating)

	INT playerAsInt = NATIVE_TO_INT(PLAYER_ID())

	// Don't update if already 'onDisplay'
	IF (GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramMessageID].cdsctv_onDisplay)
		EXIT
	ENDIF
	
	// Store the details
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramMessageID].cdsctv_missionID		= paramMissionID
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramMessageID].cdsctv_missionTitle	= paramMissionTitle
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramMessageID].cdsctv_restriction		= paramRestriction
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramMessageID].cdsctv_secondLine		= paramSecondLine
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramMessageID].cdsctv_verified		= paramVerified
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramMessageID].cdsctv_textColour		= paramTextColour
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramMessageID].cdsctv_tod				= paramTOD
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramMessageID].cdsctv_minPlayers		= paramMinPlayers
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramMessageID].cdsctv_maxPlayers		= paramMaxPlayers
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramMessageID].cdsctv_userRating		= paramUserRating

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store that the details are now onDisplay, so the SCTV player can now set them up too
//
// INPUT PARAMS:		paramMessageID			The ID of the message being updated (this is just the array position)
//						paramPosition			The Position for the details
//
// NOTES:	This won't update the data if it is 'onDisplay' already - it's player broadcast data so should only change if necessary
PROC Store_Details_On_Display_For_SCTV_Players(INT paramMessageID, VECTOR paramPosition)

	INT playerAsInt = NATIVE_TO_INT(PLAYER_ID())

	// Don't update if already 'onDisplay'
	IF (GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramMessageID].cdsctv_onDisplay)
		EXIT
	ENDIF
	
	// Store the details
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramMessageID].cdsctv_onDisplay	= TRUE
	GlobalPlayerBD_SCTV[playerAsInt].detailsSCTV[paramMessageID].cdsctv_position	= paramPosition

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      The SCTV (Spectator) Player - Reading the Details data from the spectated player
// -----------------------------------------------------------------------------------------------------------





// ===========================================================================================================
//     Game-Specific Initialisation Routines
// ===========================================================================================================

// PURPOSE:	Fill one locate message array position with details
//
// INPUT PARAMS:		paramArrayPos			Storage Position for these details
//						paramType				The type of details required
//						paramMovieName			The Scaleform Movie Name string
PROC Fill_One_Locate_Message(INT paramArrayPos, g_eLocateMessageType paramType, STRING paramMovieName)

	// Error checking
	#IF IS_DEBUG_BUILD
		IF NOT (IS_STRING_NULL_OR_EMPTY(g_sLocateMessagesMP[paramArrayPos].lmMovieName))
			// ...movie name has already been filled
			NET_PRINT("...KGM MP [LocateMessage]: Fill_One_Locate_Message() - ERROR. Array Position has already been filled: [")
			NET_PRINT_INT(paramArrayPos)
			NET_PRINT(": ")
			NET_PRINT(g_sLocateMessagesMP[paramArrayPos].lmMovieName)
			NET_PRINT("] - Allowing new details to overwrite old details")
			NET_NL()
			SCRIPT_ASSERT("Fill_One_Locate_Message(): ERROR - Locate Message Details have already been filled. See Console Log. Allowing new details to overwrite old details. Tell Keith.")
		ENDIF
	#ENDIF

	g_sLocateMessagesMP[paramArrayPos].lmType		= paramType
	g_sLocateMessagesMP[paramArrayPos].lmMovieName	= paramMovieName

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Fill the Scaleform Movie Names associated with the Locate Messages for this player's team, and the amount of details the movie can display
// NOTES:	This is game-specific and can contain Locate Messages that require different numbers of parameters
PROC Fill_MP_Locate_Message_Details()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [LocateMessage]: Initialise_MP_Locate_Messages_Scaleform_Movie_Names")
		NET_NL()
	#ENDIF

	// Clear out the team names
	INT tempLoop = 0
	REPEAT MAX_LOCATE_MESSAGE_MOVIES tempLoop
		g_sLocateMessagesMP[tempLoop].lmMovieName = ""
	ENDREPEAT
	
	// Fill the appropriate team names
	// NOTE: It's okay for teams to have different numbers of valid movie names - the code will adapt
	// KGM 7/3/13: This will need to become a switch to set up different styles of locate message if needed (as per the now obsolete version of this code) - for now we only need Freemode
	// *** IMPORTANT: Freemode Mission Name Movies accept SET_MISSION_INFO scaleform method which accepts MissionName, MissionType, CreatorName, Percentage strings as parameters ***
	Fill_One_Locate_Message(0,	LMT_FULL_DETAILS,	"MP_MISSION_NAME_FREEMODE")
	Fill_One_Locate_Message(1,	LMT_FULL_DETAILS,	"MP_MISSION_NAME_FREEMODE_1")
	Fill_One_Locate_Message(2,	LMT_FULL_DETAILS,	"MP_MISSION_NAME_FREEMODE_2")

	// Do a final loop setting the Stage to DO NOT USE if there is not a movie available for an array position
	REPEAT MAX_LOCATE_MESSAGE_MOVIES tempLoop
		IF (IS_STRING_NULL_OR_EMPTY(g_sLocateMessagesMP[tempLoop].lmMovieName))
			g_sLocateMessagesMP[tempLoop].lmStage	= LMS_NO_MOVIE_DO_NOT_USE
			g_sLocateMessagesMP[tempLoop].lmType	= LMT_NO_DETAILS
		ENDIF
	ENDREPEAT
	
	// Console Log Output
	#IF IS_DEBUG_BUILD
		REPEAT MAX_LOCATE_MESSAGE_MOVIES tempLoop
			NET_PRINT("          Scaleform Movie for ArrayPos ")
			NET_PRINT_INT(tempLoop)
			NET_PRINT(": ")
			IF (IS_STRING_NULL_OR_EMPTY(g_sLocateMessagesMP[tempLoop].lmMovieName))
				NET_PRINT("UNUSED")
			ELSE
				NET_PRINT(g_sLocateMessagesMP[tempLoop].lmMovieName)
			ENDIF
			IF (g_sLocateMessagesMP[tempLoop].lmStage = LMS_NO_MOVIE_DO_NOT_USE)
				NET_PRINT("  [Array Position Stage set to LMS_NO_MOVIE_DO_NOT_USE]")
			ENDIF
			NET_NL()
		ENDREPEAT
	#ENDIF

ENDPROC




// ===========================================================================================================
//      Locate Message Stage Routines
// ===========================================================================================================

// PURPOSE:	Set a Locate Message Stage as Available For Use
//
// INPUT PARAMS:		paramArrayPos			the Locate Message array position
PROC Set_This_Locate_Message_Stage_As_Available_For_Use(INT paramArrayPos)

	g_sLocateMessagesMP[paramArrayPos].lmStage = LMS_AVAILABLE_FOR_USE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [LocateMessage]: Array Position ")
		NET_PRINT_INT(paramArrayPos)
		NET_PRINT(" set to LMS_AVAILABLE_FOR_USE")
		NET_PRINT(": movie = ")
		NET_PRINT(g_sLocateMessagesMP[paramArrayPos].lmMovieName)
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set a Locate Message Stage as Requested This Frame
//
// INPUT PARAMS:		paramArrayPos			The Locate Message array position
PROC Set_This_Locate_Message_Stage_As_Requested_This_Frame(INT paramArrayPos)
	g_sLocateMessagesMP[paramArrayPos].lmStage = LMS_REQUESTED_THIS_FRAME
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set a Locate Message Stage as Previously Requested
//
// INPUT PARAMS:		paramArrayPos			the Locate Message array position
PROC Set_This_Locate_Message_Stage_As_Previously_Requested(INT paramArrayPos)
	g_sLocateMessagesMP[paramArrayPos].lmStage = LMS_PREVIOUSLY_REQUESTED
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set a Locate Message Stage as Loaded
//
// INPUT PARAMS:		paramArrayPos			The Locate Message array position
PROC Set_This_Locate_Message_Stage_As_Loaded(INT paramArrayPos)

	g_sLocateMessagesMP[paramArrayPos].lmStage = LMS_LOADED
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [LocateMessage]: Array Position ")
		NET_PRINT_INT(paramArrayPos)
		NET_PRINT(" set to LMS_LOADED")
		NET_PRINT(": movie = ")
		NET_PRINT(g_sLocateMessagesMP[paramArrayPos].lmMovieName)
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set a Locate Message Stage as Previously In Use
//
// INPUT PARAMS:		paramArrayPos			The Locate Message array position
PROC Set_This_Locate_Message_Stage_As_Previously_In_Use(INT paramArrayPos)
	g_sLocateMessagesMP[paramArrayPos].lmStage = LMS_PREVIOUSLY_IN_USE
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set a Locate Message Stage as In Use This Frame
//
// INPUT PARAMS:		paramArrayPos			The Locate Message array position
PROC Set_This_Locate_Message_Stage_As_In_Use_This_Frame(INT paramArrayPos)
	g_sLocateMessagesMP[paramArrayPos].lmStage = LMS_IN_USE_THIS_FRAME
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the Locate Message Stage
//
// INPUT PARAMS:		paramArrayPos			The Locate Message array position
FUNC g_eLocateMessageStage Get_This_Locate_Message_Stage(INT paramArrayPos)
	RETURN (g_sLocateMessagesMP[paramArrayPos].lmStage)
ENDFUNC




// ===========================================================================================================
//      Locate Message Movie Name Routines
// ===========================================================================================================

// PURPOSE:	Need to do a bit of underhand text label to string conversion
//
// INPUT PARAMS:		paramMovieNameAsString		A movie name as a string (converted from its stored text label)
// RETURN VALUE:		STRING						The movie name as a string
FUNC STRING Convert_Locate_Movie_Name_TextLabel_To_String(STRING paramMovieNameAsString)
	RETURN (paramMovieNameAsString)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve the Scaleform Movie Name associated with a Locate Message
//
// INPUT PARAMS:		paramArrayPos			The Locate Message array position
// RETURN VALUE:		TEXT_LABEL_31			The Scaleform Movie Name
FUNC STRING Get_Locate_Message_Movie_Name(INT paramArrayPos)

	#IF IS_DEBUG_BUILD
		IF (IS_STRING_NULL_OR_EMPTY(g_sLocateMessagesMP[paramArrayPos].lmMovieName))
			NET_PRINT("...KGM MP [LocateMessage]: Get_Locate_Message_Movie_Name() does not contain a scaleform movie name. Array Pos: ")
			NET_PRINT_INT(paramArrayPos)
			NET_NL()
			
			SCRIPT_ASSERT("Get_Locate_Message_Movie_Name(): ERROR - Array Position doesn't contain a movie name. See console log. Tell Keith.")
			
			RETURN ("")
		ENDIF
	#ENDIF
	
	RETURN (Convert_Locate_Movie_Name_TextLabel_To_String(g_sLocateMessagesMP[paramArrayPos].lmMovieName))

ENDFUNC




// ===========================================================================================================
//      Locate Message Scaleform Movie Control Routines
// ===========================================================================================================

// PURPOSE:	Checks if the Locate Message is ready to use (effectively, jsut checks if the scaleform movie has loaded)
//
// INPUT PARAMS:		paramArrayPos			The Locate Message array position
// RETURN VALUE:		BOOL					TRUE if the Locate Message is ready to use, FALSE if not (ie: scaleform movie is still loading)
FUNC BOOL Is_This_Locate_Message_Ready_To_Use(INT paramArrayPos)

	SCALEFORM_INDEX thisMovieID = g_sLocateMessagesMP[paramArrayPos].lmMovieID
	
	// Return FALSE if there isn't a stored Movie ID
	IF (thisMovieID = NULL)
		RETURN FALSE
	ENDIF
	
	// There is a stored MovieID, so check if the MovieID has loaded or not
	RETURN (HAS_SCALEFORM_MOVIE_LOADED(thisMovieID))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Terminate the use of a Scaleform Movie for a Locate Message
//
// INPUT PARAMS:		paramArrayPos			The Locate Message array position
//
// NOTES:	Code will ensure that if the script terminates after a scaleform movie request has been made but before the movie has loaded, the movie will still cleanup properly.
PROC Terminate_This_Locate_Message(INT paramArrayPos)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [LocateMessage]: Terminating Locate Message in Array Position ")
		NET_PRINT_INT(paramArrayPos)
		NET_PRINT(": movie = ")
		NET_PRINT(g_sLocateMessagesMP[paramArrayPos].lmMovieName)
		NET_NL()
	#ENDIF

	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_sLocateMessagesMP[paramArrayPos].lmMovieID)
	g_sLocateMessagesMP[paramArrayPos].lmMovieID = NULL
	
	Clear_One_Details_Data_For_Storage_For_SCTV(paramArrayPos)
	Set_This_Locate_Message_Stage_As_Available_For_Use(paramArrayPos)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the passed in array position is still available to be used
//
// INPUT PARAMS:		paramArrayPos			The Locate Message array position
// RETURN VALUE:		BOOL					TRUE if the Locate Message Stage is still available, otherwise FALSE
//
// NOTES:	It will still be available if the stage is either LMS_AVAILABLE_FOR_USE (unused), or LMS_PREVIOUSLY_REQUESTED (nothing else has requested it this frame).
//			This is on a first-come-first-served basis, if something else also tries to use this array position it will be denied.
FUNC BOOL Is_This_Locate_Message_Still_Available(INT paramArrayPos)

	IF (paramArrayPos < 0)
	OR (paramArrayPos >= MAX_LOCATE_MESSAGE_MOVIES)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [LocateMessage]: Is_This_Locate_Message_Still_Available() passed in an illegal array position. Returning FALSE. ArrayPos: ") NET_PRINT_INT(paramArrayPos) NET_NL()
			SCRIPT_ASSERT("Is_This_Locate_Message_Still_Available(): ERROR - Illegal Array Position received. Tell Keith.")
		#ENDIF
		
		RETURN FALSE
	ENDIF

	// Check this array position
	g_eLocateMessageStage thisStage = Get_This_Locate_Message_Stage(paramArrayPos)
	
	SWITCH (thisStage)
		// These mean the array position is still available
		CASE LMS_AVAILABLE_FOR_USE
		CASE LMS_PREVIOUSLY_REQUESTED
			RETURN TRUE
	ENDSWITCH
	
	// Anything else means it is not available
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Retrieve an available array position to use
//
// RETURN VALUE:		INT					An available array position, or NO_LOCATE_MESSAGE_ID if none
FUNC INT Get_Unused_Locate_Message()

	INT tempLoop = 0
	
	REPEAT MAX_LOCATE_MESSAGE_MOVIES tempLoop
		IF (Is_This_Locate_Message_Still_Available(tempLoop))
			// ...found an available Locate Message array position
			RETURN tempLoop
		ENDIF
	ENDREPEAT
	
	// There are no available locate messages
	RETURN NO_LOCATE_MESSAGE_ID

ENDFUNC




// ===========================================================================================================
//      Set and Clear Locate Message Variables Routines
// ===========================================================================================================

// PURPOSE:	Clear one Locate Message array entry
//
// INPUT PARAMS:		paramArrayPos			The Locate Message array position
PROC Clear_One_Locate_Message(INT paramArrayPos)
	
	IF (g_sLocateMessagesMP[paramArrayPos].lmMovieID != NULL)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [LocateMessage]: Clear_One_Locate_Message() - ERROR - MovieID is not NULL. Marking Movie as no longer needed to allow processing to continue: ArrayPos = ")
			NET_PRINT_INT(paramArrayPos)
			NET_NL()
			
			SCRIPT_ASSERT("Clear_One_Locate_Message(): ERROR - Scaleform MovieID is not NULL. See Console Log. Freeing Movie Request. Tell Keith.")
		#ENDIF
		
		Terminate_This_Locate_Message(paramArrayPos)
	ENDIF
	
	// Clear the array position data
	g_structLocateMessageMP emptyLocateMessageStruct
	g_sLocateMessagesMP[paramArrayPos] = emptyLocateMessageStruct
	
ENDPROC




// ===========================================================================================================
//      Locate Message Public Access Routines
// ===========================================================================================================

// PURPOSE:	Request to use a Locate Message
//
// RETURN PARAMS:		paramMessageID			The ID of the message being requested (this is just the array position) - this will be updated within this function if necessary
// RETURN VALUE:		BOOL					TRUE if the locate message is ready to use, otherwise FALSE
FUNC BOOL Request_Locate_Message(INT &paramMessageID)

	// Check if the passed in MessageID is valid for this team
	IF (paramMessageID != NO_LOCATE_MESSAGE_ID)
		// ...a MessageID has been passed in, so check if it is still available to be used (it won't be available if something else is using it)
		IF NOT (Is_This_Locate_Message_Still_Available(paramMessageID))
			// ...messageID isn't available, so set it to be NoMessageID so that a new one gets picked if available
			paramMessageID = NO_LOCATE_MESSAGE_ID
		ENDIF
	ENDIF
	
	// MessageID is either unfilled, or already filled with a valid value
	// If unfilled, get a new messageID
	IF (paramMessageID = NO_LOCATE_MESSAGE_ID)
		paramMessageID = Get_Unused_Locate_Message()
		
		// If there isn't one available then return FALSE
		IF (paramMessageID = NO_LOCATE_MESSAGE_ID)
			RETURN FALSE
		ENDIF
	ENDIF
	
	// We now have a valid MessageID, so request the scaleform movie and update the Locate Message Stage
	g_sLocateMessagesMP[paramMessageID].lmMovieID = REQUEST_SCALEFORM_MOVIE(Get_Locate_Message_Movie_Name(paramMessageID))
	Set_This_Locate_Message_Stage_As_Requested_This_Frame(paramMessageID)
	
	// Check if the movie has loaded and so is ready to use
	IF (Is_This_Locate_Message_Ready_To_Use(paramMessageID))
		// ...the message is ready to use, so update the Locate Message Stage to 'Loaded'
		Set_This_Locate_Message_Stage_As_Loaded(paramMessageID)
		RETURN TRUE
	ENDIF
	
	// Locate Message isn't yet ready to use
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	A cheap version of Request_Locate_Message() to allow a request to be maintained every frame without doing the full processing.
//
// INPUT PARAMS:		paramMessageID			The ID of the message request being maintained (this is just the array position)
//
// NOTES:	This is intended for use by requesters that are part of a staggered processing loop - if they call this every frame it will maintain
//				the request until it can be properly checked with the full routine above
PROC Request_Locate_Message_Cheap_Update_To_Keep_Request_Active(INT paramMessageID)

	// Ideally I wouldn't do this and assume the messageID is valid, but I don't think I can risk it in case we miss something during development that leads to an array overrun in release
	IF (paramMessageID = NO_LOCATE_MESSAGE_ID)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (paramMessageID < 0)
		OR (paramMessageID >= MAX_LOCATE_MESSAGE_MOVIES)
			NET_PRINT("...KGM MP [LocateMessage]: Request_Locate_Message_Cheap_Update_To_Keep_Request_Active() passed in an illegal array position. MessageID (ArrayPos): ") NET_PRINT_INT(paramMessageID) NET_NL()
			SCRIPT_ASSERT("Request_Locate_Message_Cheap_Update_To_Keep_Request_Active(): ERROR - Illegal Array Position received. Tell Keith.")
		
			EXIT
		ENDIF
	#ENDIF
	
	// Only keep the request active if it was also requested on the previous frame
	IF (Get_This_Locate_Message_Stage(paramMessageID) = LMS_PREVIOUSLY_REQUESTED)
		Set_This_Locate_Message_Stage_As_Requested_This_Frame(paramMessageID)
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Brief Locate Message Details
//
// INPUT PARAMS:		paramMessageID			The ID of the message being updated (this is just the array position)
//						paramMissionID			The Mission ID
//						paramVariation			The Mission Variation
//						paramColour				The Text Colour
//
// NOTES:	THIS FUNCTION SHOULD ONLY BE CALLED BY Store_Locate_Message_Details()
PROC Store_Locate_Message_Brief_Details(INT				paramMessageID,
										MP_MISSION		paramMissionID,
										INT				paramVariation,
										HUD_COLOURS		paramColour)


	// Compiler fix.
	IF paramVariation = 0
	ENDIF
	
	// Locate Message Requires Brief Details
	SCALEFORM_INDEX thisMovieID = g_sLocateMessagesMP[paramMessageID].lmMovieID
	//INT				playerTeam	= GET_PLAYER_TEAM(PLAYER_ID())
	
	// ...mission name
	BEGIN_SCALEFORM_MOVIE_METHOD(thisMovieID, "SET_MISSION_NAME")
		// KGM MP: TEMP CHECK FOR Cutscene Mission - Cutscenes should just be setup as multiple variations of the cutscene 'mission'
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(GET_MP_MISSION_NAME_TEXT_LABEL(paramMissionID))
	END_SCALEFORM_MOVIE_METHOD()
	
	// ...max players
	// KGM 18/2/13: THIS WILL NEED CHANGED TO USE THE MAX PLAYERS DATA PASSED IN AS PARAMETER
	BEGIN_SCALEFORM_MOVIE_METHOD(thisMovieID, "SET_PLAYERS_NEEDED")
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(paramColour)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	END_SCALEFORM_MOVIE_METHOD()
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [LocateMessage]: Storing Brief Locate Message Details for MessageID ")
		NET_PRINT_INT(paramMessageID)
		NET_PRINT(": ")
		NET_PRINT(GET_MP_MISSION_NAME(paramMissionID))
		NET_PRINT(" (Var: ")
		NET_PRINT_INT(paramVariation)
		NET_PRINT(")  [Using Movie: ")
		NET_PRINT(g_sLocateMessagesMP[paramMessageID].lmMovieName)
		NET_PRINT("]")
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Full Locate Message Details
//
// INPUT PARAMS:		paramMessageID			The ID of the message being updated (this is just the array position)
//						paramMissionID			The Mission ID to get mission type from mission data (second scaleform param - top display line)
//						paramSecondLine			The Mission Name string, or restriction text label (first scaleform param - second display line)
//						paramSecondLineIsString	TRUE if the second line is a mission name (string), FALSE for restriction text (Text label)
//						paramCreatorName		The Creator Name (third scaleform param - third display line)
//						paramUserRating			The User Rating (within thumbsup)
//						paramLesRating			Les's Rating (possibly TEMP)
//						paramMinPlayers			The minimum players for the mission
//						paramMaxPlayers			The maximum players for the mission
//						paramContact			The Contact giving the mission
//						paramRockstarVerified	TRUE if this mission is from the Rockstar Verified list
//						paramColour				The text colour
//						paramHasRestriction		TRUE if there is a restriction that prevents the player playing the mission, FALSE if no restriction
//						paramReserved			TRUE if the 'players waiting' text should be added, FALSE if not
//						paramTimeString			KGM 21/7/15 [BUG 2425819]: A strign used to display the Record Time for some FM Events
//						paramRPMultiplier		[DEFAULT = 0.0] Current RP Multiplier for this mission
//						paramCashMultiplier		[DEFAULT = 0.0] Current Cash Multiplier for this mission
//
// NOTES:	THIS FUNCTION SHOULD ONLY BE CALLED BY Store_Locate_Message_Details()
//			The scaleform parameter order is different from the display order on-screen
//			The Mission ID (that accesses the mission data) is used to retrieve the Mission Type
PROC Store_Locate_Message_Full_Details(	INT					paramMessageID,
										MP_MISSION			paramMissionID,
										TEXT_LABEL_63		paramSecondLine,
										BOOL				paramSecondLineIsString,
										STRING				paramCreatorName,
										INT					paramUserRating,
										INT					paramLesRating,
										INT					paramMinPlayers,
										INT					paramMaxPlayers,
										TEXT_LABEL_23		paramMissionTitle,
										INT					paramTODActive,
										BOOL				paramRockstarVerified,
										HUD_COLOURS			paramColour,
										BOOL				paramHasRestriction,
										BOOL				paramReserved,
										STRING				paramTimeString,
										INT					paramTimeAsMsec,
										FLOAT				paramRPMultiplier		= 0.0,
										FLOAT 				paramCashMultiplier		= 0.0)

	// NOTES: This is based on STORE_MISSION_LOCATE_MESSAGE_DETAILS_FM()
	
	// KGM 22/7/13: The Contact Name is no longer displayed instead of 'Mission' - but for now leave the data in place
//	UNUSED_PARAMETER(paramContact)
	
	// Error Checking
	BOOL missionIDIsValid = TRUE
	#IF IS_DEBUG_BUILD
		IF (paramMissionID = eNULL_MISSION)
			SCRIPT_ASSERT("Store_Locate_Message_Full_Details(): Mission ID is eNULL_MISSION. Tell Keith.")
			missionIDIsValid = FALSE
		ENDIF
	#ENDIF

	BOOL secondLineIsValid = TRUE
	#IF IS_DEBUG_BUILD
		IF (IS_STRING_NULL_OR_EMPTY(paramSecondLine))
			secondLineIsValid = FALSE
		ENDIF
	#ENDIF

	BOOL CreatorNameIsValid = TRUE
	IF (IS_STRING_NULL_OR_EMPTY(paramCreatorName))
		CreatorNameIsValid = FALSE
	ENDIF

	BOOL timeStringIsValid = FALSE
	IF NOT (IS_STRING_NULL_OR_EMPTY(paramTimeString))
		timeStringIsValid = TRUE
	ENDIF
	
	// Ensure the minimum an maximum values are valid
	#IF IS_DEBUG_BUILD
		BOOL playerRangeModifiedDueToError = FALSE
	#ENDIF
	
	IF (paramMinPlayers < 0)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [LocateMessage]: Store_Locate_Message_Full_Details() - ERROR: Min Players < 0. Setting to 0.") NET_NL()
			SCRIPT_ASSERT("Store_Locate_Message_Full_Details(): ERROR - Minimum Players < 0. Setting to 0 to allow game to continue. Tell Keith.")
			
			playerRangeModifiedDueToError = TRUE
		#ENDIF
		
		paramMinPlayers = 0
	ENDIF

	IF (paramMaxPlayers < 0)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [LocateMessage]: Store_Locate_Message_Full_Details() - ERROR: Max Players < 0. Setting to 0.") NET_NL()
			SCRIPT_ASSERT("Store_Locate_Message_Full_Details(): ERROR - Maximum Players < 0. Setting to 0 to allow game to continue. Tell Keith.")
			
			playerRangeModifiedDueToError = TRUE
		#ENDIF
		
		paramMaxPlayers = 0
	ENDIF
	
	IF (paramMinPlayers > paramMaxPlayers)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [LocateMessage]: Store_Locate_Message_Full_Details() - ERROR: Min Players > Max Players. Changing both to 0.") NET_NL()
			SCRIPT_ASSERT("Store_Locate_Message_Full_Details(): ERROR - Min Players > Max Players. Setting both to 0 to allow game to continue with no visual bug. Tell Keith.")
			
			playerRangeModifiedDueToError = TRUE
		#ENDIF
		
		paramMinPlayers = 0
		paramMaxPlayers = 0
	ENDIF

	// Locate Message Requires Full Details
	SCALEFORM_INDEX thisMovieID = g_sLocateMessagesMP[paramMessageID].lmMovieID
	//INT				playerTeam	= GET_PLAYER_TEAM(PLAYER_ID())
	
	BEGIN_SCALEFORM_MOVIE_METHOD(thisMovieID, "SET_MISSION_INFO")
		// ...mission name (displayed on second line) - if valid, or pad it out
		IF (secondLineIsValid)
			// ... if there is a restriction on the player entering the corona we need to display the locked symbol (pass in a 'Ω')
			STRING mainStringToUse = "STRING"
			IF (paramHasRestriction)
				if paramTODActive = -1
					mainStringToUse = "TWOSTRINGS"					
				else
					// retrict is because of TOD
					mainStringToUse = "FM_RESTRICT_TOD"
				endif
			ENDIF
			
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(mainStringToUse)
				IF (paramSecondLineIsString)
					ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(paramSecondLine)
				ELSE			
					// second line display restriction 
					ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(paramSecondLine)	
					
					// is retriction because of TOD
					if paramTODActive != -1
						// set AM or PM display
						INT displayHour = paramTODActive
						if displayHour = 0	
							displayHour = 12
							ADD_TEXT_COMPONENT_INTEGER(displayHour)
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("FM_RESTRICT_AM")
						elif displayHour < 12					
							ADD_TEXT_COMPONENT_INTEGER(displayHour)
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("FM_RESTRICT_AM")
						elif displayHour = 12	
							ADD_TEXT_COMPONENT_INTEGER(displayHour)
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("FM_RESTRICT_PM")
						else
							displayHour -= 12
							ADD_TEXT_COMPONENT_INTEGER(displayHour)
							ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL("FM_RESTRICT_PM")
						endif
					endif
				ENDIF
				
				IF (paramHasRestriction)
					STRING lockSymbol = "Ω"
					ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(lockSymbol)
				ENDIF
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			// If there is no other text to display and there are player's reserved (minigames only), then display a message
			IF (paramReserved)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PLAYERS_WAIT")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("")
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		ENDIF
		
		// ...mission ID to display generic type,HEIST, CTF, LTS

		IF not ARE_STRINGS_EQUAL(paramMissionTitle, "")
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				SET_COLOUR_OF_NEXT_TEXT_COMPONENT(paramColour)
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(paramMissionTitle)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELIF (missionIDIsValid)	
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				SET_COLOUR_OF_NEXT_TEXT_COMPONENT(paramColour)
				ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(GET_MP_MISSION_NAME_TEXT_LABEL(paramMissionID))
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		// ...creator name (displayed on third line) - if valid, or pad it out
		IF (CreatorNameIsValid)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(paramCreatorName)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		// Percentage as a string so we append the % symbol - only display it if it is positive, otherwise pad out the parameter
		IF (paramUserRating >= 0)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("PERCENTAGE")
				ADD_TEXT_COMPONENT_INTEGER(paramUserRating)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		// Les Rating as a string so we can add some text to the value - only display it if it is positive, otherwise pad out the parameter
		IF (paramLesRating > 0)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("LES_RATING")
				ADD_TEXT_COMPONENT_INTEGER(paramLesRating)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		// Rockstar Verified flag
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(paramRockstarVerified)
		
		// Min-Max Players as a string - only display it if both values are positive and max is above 0
		IF	(paramMinPlayers >= 0)
		AND	(paramMaxPlayers > 0)
			IF (paramMinPlayers = paramMaxPlayers)
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER")
					ADD_TEXT_COMPONENT_INTEGER(paramMinPlayers)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING("NUMBER_RANGE")
					ADD_TEXT_COMPONENT_INTEGER(paramMinPlayers)
					ADD_TEXT_COMPONENT_INTEGER(paramMaxPlayers)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ENDIF
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
		
		// Added for bug 1905690
		IF paramRPMultiplier <= 1
			paramRPMultiplier = 0.0
		ENDIF
		
		IF paramCashMultiplier <= 1
			paramCashMultiplier = 0.0
		ENDIF
		
		PRINTLN("[MMM] RP Multiplier being sent to scaleform : ", paramRPMultiplier)
		PRINTLN("[MMM] Cash Multiplier being sent to scaleform: ", paramCashMultiplier)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(paramRPMultiplier)		// First Float displays the RP
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(paramCashMultiplier)		// Second displays the Cash
		
		// KGM 21/7/15 [BUG 2425819]: A new string parameter has been added to display time
		IF (timeStringIsValid)
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(paramTimeString)
				ADD_TEXT_COMPONENT_SUBSTRING_TIME(paramTimeAsMsec, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS|TEXT_FORMAT_USE_DOT_FOR_MILLISECOND_DIVIDER)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING("")
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	
	// KGM 17/2/14: Store the details so that SCTV players can access them
	// NOTE: This doesn't mark these details as 'onDisplay', that's done by the display routine
	Store_Details_For_SCTV_Players(paramMessageID, paramMissionID, paramMissionTitle, paramSecondLine, paramHasRestriction, paramRockstarVerified,
									paramColour, paramTODActive, paramMinPlayers, paramMaxPlayers, paramUserRating)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [LocateMessage]: Store_Locate_Message_Full_Details - Storing Full Locate Message Details for MessageID ")
		NET_PRINT_INT(paramMessageID)
		NET_NL()
		NET_PRINT("          Mission Type: ")
		NET_PRINT(GET_MP_MISSION_NAME(paramMissionID))
		NET_NL()
		IF (paramHasRestriction)
			NET_PRINT("          Restriction : ")
			NET_PRINT(GET_STRING_FROM_TEXT_FILE(paramSecondLine))
			NET_NL()
			NET_PRINT("          paramTODActive : ")
			NET_PRINT_INT(paramTODActive)
		ELSE
			NET_PRINT("          Mission Name: ")
			NET_PRINT(paramSecondLine)
			IF (paramRockstarVerified)
				NET_PRINT(" [R* VERIFIED MISSION]")
			ENDIF
		ENDIF
		NET_NL()
		NET_PRINT("          Creator Name: ")
		NET_PRINT(paramCreatorName)
		NET_NL()
		NET_PRINT("          Percentage  : ")
		NET_PRINT_INT(paramUserRating)
		NET_NL()
		NET_PRINT("          Les Rating  : ")
		NET_PRINT_INT(paramLesRating)
		NET_NL()
		NET_PRINT("          Time String : ")
		NET_PRINT(paramTimeString)
		NET_PRINT(". Time Value as msec: ")
		NET_PRINT_INT(paramTimeAsMsec)
		NET_NL()
		NET_PRINT("          Players     : ")
		NET_PRINT_INT(paramMinPlayers) NET_PRINT(" - ") NET_PRINT_INT(paramMaxPlayers)
		IF (playerRangeModifiedDueToError)
			NET_PRINT(" - MODIFIED FROM ORIGINAL PARAMETER VALUES DUE TO DATA INCONSISTENCY")
		ENDIF
		NET_NL()
		NET_PRINT("          Using Movie : ")
		NET_PRINT(g_sLocateMessagesMP[paramMessageID].lmMovieName)
		NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Locate Message Details for an SCTV player that received the info from the spectated player
//
// INPUT PARAMS:		paramMessageID			The ID of the message being updated (this is just the array position)
//						paramMissionID			The Mission ID
//						paramMissionTitle		The Mission Title					(if it cannot be generated from MissionID)
//						paramColour				The text colour
//						paramSecondLine			The cloud-loaded Mission Name data, or a restriction message
//						paramRockstarVerified	[DEFAULT = FALSE]					TRUE If this is a rockstar verified mission, otherwise FALSE
//						paramHasRestriction		[DEFAULT = FALSE]					TRUE if there is a restriction that blocks the player from entering the corona, FALSE if no restriction
//						paramTOD				[DEFAULT = -1]						Contains a value if a Time Of Day restriction is in place for launching the mission
//						paramMinPlayers			[DEFAULT = -1]						The minimum required players
//						paramMaxPlayers			[DEFAULT = -1]						The maximum required players
//						paramUserRating			[DEFAULT = -1]						The user rating
PROC Store_Locate_Message_Details_For_SCTV(	INT					paramMessageID,
											MP_MISSION			paramMissionID,
											TEXT_LABEL_23		paramMissionTitle,
											HUD_COLOURS			paramColour,
											TEXT_LABEL_63		paramSecondLine,
											BOOL				paramRockstarVerified	= FALSE,
											BOOL				paramHasRestriction		= FALSE,
											INT					paramTOD				= -1,
											INT					paramMinPlayers			= -1,
											INT					paramMaxPlayers			= -1,
											INT					paramUserRating			= -1)
	// If this player is NOT SCTV, assert
	IF NOT (IS_PLAYER_SCTV(PLAYER_ID()))
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [LocateMessage]: This player is not a spectator.")
			SCRIPT_ASSERT("ERROR - Not a spectator. Use Store_Locate_Message_Details() Function. IGNORING. Tell Keith.")
		#ENDIF

		
		EXIT
	ENDIF

	// The details can only be stored if the movie has loaded
	IF (paramMessageID < 0)
	OR (paramMessageID >= MAX_LOCATE_MESSAGE_MOVIES)
		#IF IS_DEBUG_BUILD
			NET_PRINT(".KGM [LocateMessage][SCTV]: Illegal array position. MessageID (ArrayPos): ") NET_PRINT_INT(paramMessageID) NET_NL()
			SCRIPT_ASSERT("ERROR - Illegal Array Position received for SCTV. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Ensure the stage is valid for receiving data - this can only occur after the movie has loaded
	IF NOT (Is_This_Locate_Message_Ready_To_Use(paramMessageID))
		#IF IS_DEBUG_BUILD
			NET_PRINT(".KGM [LocateMessage][SCTV]: Locate Message is not Ready To Use. MessageID (arraypos): ") NET_PRINT_INT(paramMessageID) NET_NL()
			SCRIPT_ASSERT("ERROR - Scaleform Movie isn't loaded. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Pass to the standard function
	BOOL secondLineIsString = TRUE
	IF (paramHasRestriction)
		// ...restriction text uses a text label
		secondLineIsString = FALSE
	ENDIF
	
	// Pass to the standard function if mission corona is a freemode event/time trial
	SWITCH (paramMissionID)
		CASE eAM_TIME_TRIAL
		// ...freemode event/time trial coronas require a text label
			secondLineIsString = FALSE
		BREAK
		// ...additional missions requiring handling
	ENDSWITCH
	
	STRING	ignoreCreatorName	= ""
	INT		ignoreRating		= -1
	BOOL	ignoreReserved		= FALSE
	STRING	ignoreTimeString	= ""
	INT		ignoreTimeValue		= 0
	Store_Locate_Message_Full_Details(paramMessageID, paramMissionID, paramSecondLine, secondLineIsString, ignoreCreatorName, paramUserRating, ignoreRating,
					paramMinPlayers, paramMaxPlayers, paramMissionTitle, paramTOD, paramRockstarVerified, paramColour, paramHasRestriction, ignoreReserved,
					ignoreTimeString, ignoreTimeValue)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the Locate Message Details
//
// INPUT PARAMS:		paramMessageID			The ID of the message being updated (this is just the array position)
//						paramMissionID			The Mission ID
//						paramVariation			The Mission Variation
//						paramMissionTitle		The Mission Title					(if it cannot be generated from MissionID)
//						paramColour				[DEFAULT = HUD_COLOUR_PURE_WHITE]	If required, the text colour
//						paramSecondLine			[DEFAULT = ""]						If relevant, the cloud-loaded Mission Name data, or a restriction message
//						paramSecondLineIsString	[DEFAULT = TRUE]					If relevant, TRUE if the second line is a string (mission name), FALSE for text label (restriction message)
//						paramCreatorName		[DEFAULT = ""]						If relevant, the cloud-loaded Creator Name data
//						paramUserRating			[DEFAULT = 0]						If relevant, contains the cloud-loaded user rating for the activity
//						paramLesRating			[DEFAULT = 0]   					If relevant, contains a Les Rating for the mission
//						paramMinPlayers			[DEFAULT = 0]   					If relevant, contains the minimum players for the mission
//						paramMaxPlayers			[DEFAULT = 0]						If relevant, contains the maximum players for the mission
//						paramContact			[DEFAULT = NO_CHARACTER]			If relevant, contains the contact issuing the mission
//						paramRockstarVerified	[DEFAULT = FALSE]					TRUE If this is a rockstar verified mission, otherwise FALSE
//						paramHasRestriction		[DEFAULT = FALSE]					TRUE if there is a restriction that blocks the player from entering the corona, FALSE if no restriction
//						paramReserved			[DEFAULT = FALSE]					TRUE if the 'players waiting' text should be added, FALSE if not
//						paramTimeString			[DEFAULT = ""]						KGM 21/7/15 BUG 2425819: If required, contains a string originally intedned to show Time Trial time information
//						paramRPMultiplier		[DEFAULT = 0.0]						If required, shows the current RP multiplier
//						paramCashMultiplier		[DEFAULT = 0.0]						If required, shows the current cash multipler
PROC Store_Locate_Message_Details(	INT					paramMessageID,
									MP_MISSION			paramMissionID,
									INT					paramVariation,
									TEXT_LABEL_23		paramMissionTitle,
									HUD_COLOURS			paramColour,
									TEXT_LABEL_63		paramSecondLine,
									BOOL				paramSecondLineIsString	= TRUE,
									STRING				paramCreatorName		= NULL,
									INT					paramUserRating			= 0,
									INT					paramLesRating			= 0,
									INT					paramMinPlayers			= 0,
									INT					paramMaxPlayers			= 0,
									INT					paramTODActive			= -1,
									BOOL				paramRockstarVerified	= FALSE,
									BOOL				paramHasRestriction		= FALSE,
									BOOL				paramReserved			= FALSE,
									STRING				paramTimeString			= NULL,
									INT					paramTimeAsMsec			= 0,
									FLOAT				paramRPMultiplier		= 0.0,
									FLOAT				paramCashMultiplier		= 0.0)

	// If this player is SCTV, assert, but pass the data on to the correct function
	IF (IS_PLAYER_SCTV(PLAYER_ID()))
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM MP [LocateMessage]: Store_Locate_Message_Details() called for SCTV player. Passing to SCTV function.")
			SCRIPT_ASSERT("Store_Locate_Message_Details(): ERROR - SCTV Player. Use SCTV Function. (For Now: Passing details on). Tell Keith.")
		#ENDIF
		
		Store_Locate_Message_Details_For_SCTV(paramMessageID, paramMissionID, paramMissionTitle, paramColour, paramSecondLine, paramRockstarVerified, paramHasRestriction)
		
		EXIT
	ENDIF

	// The details can only be stored if the movie has loaded
	IF (paramMessageID < 0)
	OR (paramMessageID >= MAX_LOCATE_MESSAGE_MOVIES)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [LocateMessage]: Store_Locate_Message_Details() passed in an illegal array position. MessageID (ArrayPos): ") NET_PRINT_INT(paramMessageID) NET_NL()
			SCRIPT_ASSERT("Store_Locate_Message_Details(): ERROR - Illegal Array Position received. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Ensure the stage is valid for receiving data - this can only occur after the movie has loaded
	IF NOT (Is_This_Locate_Message_Ready_To_Use(paramMessageID))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [LocateMessage]: Store_Locate_Message_Details() - Locate Message is not Ready To Use. MessageID (arraypos): ") NET_PRINT_INT(paramMessageID) NET_NL()
			SCRIPT_ASSERT("Store_Locate_Message_Details(): ERROR - Attempting to store Scaleform Movie details when movie isn't loaded. See Console Log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Call the method as required by the type of the scaleform movie, passing in the necessary details
	SWITCH (g_sLocateMessagesMP[paramMessageID].lmType)
		// Brief Details required (ie: CnC Crook Locate Messages)
		CASE LMT_BRIEF_DETAILS
			Store_Locate_Message_Brief_Details(paramMessageID, paramMissionID, paramVariation, paramColour)
			EXIT
			
		// Full Details required (ie: Freemode Locate Messages)
		CASE LMT_FULL_DETAILS
			Store_Locate_Message_Full_Details(paramMessageID, paramMissionID, paramSecondLine, 
											  paramSecondLineIsString, paramCreatorName, paramUserRating, 
											  paramLesRating, paramMinPlayers, paramMaxPlayers,
											  paramMissionTitle, paramTODActive, paramRockstarVerified, 
											  paramColour, paramHasRestriction, paramReserved,
											  paramTimeString, paramTimeAsMsec, paramRPMultiplier,
											  paramCashMultiplier)
			EXIT
			
		// No Details required
		CASE LMT_NO_DETAILS
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [LocateMessage]: Storing Locate Message Details for MessageID ")
				NET_PRINT_INT(paramMessageID)
				NET_PRINT(": NO DETAILS REQUIRED")
				NET_PRINT("  [Using Movie: ")
				NET_PRINT(g_sLocateMessagesMP[paramMessageID].lmMovieName)
				NET_PRINT("]")
				NET_NL()
			#ENDIF
			
			EXIT
	ENDSWITCH
	
	// Failed to find a valid Locate Message Type
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [LocateMessage]: Store_Locate_Message_Details() - ERROR: Unknown Locate Message Type ID. Add to SWITCH statement.")
		NET_PRINT("  [Using Movie: ")
		NET_PRINT(g_sLocateMessagesMP[paramMessageID].lmMovieName)
		NET_PRINT("]")
		NET_NL()
		SCRIPT_ASSERT("Store_Locate_Message_Details() - ERROR: Unknown Locate Message Type ID. Add to SWITCH statement. Tell Keith.")
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display the Locate Message
//
// INPUT PARAMS:		paramMessageID			The ID of the message being updated (this is just the array position)
//						paramCoords				The Display Position
//
// NOTES:	Re-directs this call to the existing display function
//			This needs called every frame to keep the movie active
PROC Display_Locate_Message(INT paramMessageID, VECTOR paramCoords)

	// The details can only be stored if the movie has loaded
	IF (paramMessageID < 0)
	OR (paramMessageID >= MAX_LOCATE_MESSAGE_MOVIES)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [LocateMessage]: Display_Locate_Message() passed in an illegal array position. MessageID (ArrayPos): ") NET_PRINT_INT(paramMessageID) NET_NL()
			SCRIPT_ASSERT("Display_Locate_Message(): ERROR - Illegal Array Position received. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Don't re-display if the message has already been displayed this frame
	IF (Get_This_Locate_Message_Stage(paramMessageID) = LMS_IN_USE_THIS_FRAME)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [LocateMessage]: Display_Locate_Message() Requested to display more than once in a frame. MessageID (ArrayPos): ") NET_PRINT_INT(paramMessageID) NET_NL()
			SCRIPT_ASSERT("Display_Locate_Message(): ERROR - Requested to be displayed more than once in a frame. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Ensure the stage is valid for receiving data - this can only occur after the movie has loaded
	IF NOT (Is_This_Locate_Message_Ready_To_Use(paramMessageID))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [LocateMessage]: Display_Locate_Message() - Locate Message is not Ready To Use. MessageID (arraypos): ") NET_PRINT_INT(paramMessageID) NET_NL()
			SCRIPT_ASSERT("Display_Locate_Message(): ERROR - Attempting to display Scaleform Movie when movie isn't loaded. See Console Log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF

	// Display the scaleform movie
	// ...display coords
	paramCoords.z += 0.5
	
	// ...rotation
	VECTOR theRotation = GET_MISSION_LOCATE_ROTATION_VECTOR(paramCoords, GET_FINAL_RENDERED_CAM_COORD())
	
	// ...scale
	VECTOR theScale = << 1.0, 1.0, 1.0>>
	
	// ...world size
	VECTOR theWorldSize = << 3.0, 3.0, 1.0 >>
	
	// KGM 12/7/13: Nasty camera clipping fix - if the camera is about to clip through the message then stick it below the ground
	//				(The alternative is cleaning it up then re-requesting it, but that brings its own problems)
	CONST_FLOAT		HIDE_MESSAGE_RADIUS_m		3.0
	CONST_FLOAT		DISTANCE_BELOW_GROUND_m		500.0
	
	VECTOR cameraRenderPos = GET_FINAL_RENDERED_CAM_COORD()
	IF (GET_DISTANCE_BETWEEN_COORDS(cameraRenderPos, paramCoords) < HIDE_MESSAGE_RADIUS_m)
		paramCoords.x -= DISTANCE_BELOW_GROUND_m
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [LocateMessage]: Camera is about to clip the message - sticking the message below ground (to prevent cleanup and re-activation)") NET_NL()
		#ENDIF
	ENDIF
	
	SCALEFORM_INDEX thisMovieID = g_sLocateMessagesMP[paramMessageID].lmMovieID
	DRAW_SCALEFORM_MOVIE_3D_SOLID(thisMovieID, paramCoords, theRotation, theScale, theWorldSize)
	
	// Set the Stage as In Use to prevent this movie from being cleaned up
	Set_This_Locate_Message_Stage_As_In_Use_This_Frame(paramMessageID)
	
	// Ensure the SCTV player knows these details are now onDisplay
	Store_Details_On_Display_For_SCTV_Players(paramMessageID, paramCoords)
	
ENDPROC




// ===========================================================================================================
//      System Initialisation, Maintenance, and Termination Routines
// ===========================================================================================================

// PURPOSE:	Terminate the Locate Message system
// NOTES:	Cleanup any movie requests that are no longer required.
PROC Terminate_MP_Locate_Messages()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [LocateMessage]: Terminate_MP_Locate_Messages") NET_NL()
	#ENDIF

	// Clean up any Locate Messages in use
	g_eLocateMessageStage	thisStage	= LMS_AVAILABLE_FOR_USE
	INT						tempLoop	= 0
	
	REPEAT MAX_LOCATE_MESSAGE_MOVIES tempLoop
		// Kill the scaleform
		thisStage = Get_This_Locate_Message_Stage(tempLoop)
		
		IF (thisStage != LMS_AVAILABLE_FOR_USE)
		AND (thisStage != LMS_NO_MOVIE_DO_NOT_USE)
			Terminate_This_Locate_Message(tempLoop)
		ENDIF
		
		// Cleanup the variables
		Clear_One_Locate_Message(tempLoop)
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Initialise the Locate Message system - for the scaleform movies that display the mission name in the middle of a corona
PROC Initialise_MP_Locate_Messages()

	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [LocateMessage]: Initialise_MP_Locate_Messages") NET_NL()
	#ENDIF

	// Get the control variables set to a decent initial value
	INT tempLoop = 0
	
	REPEAT MAX_LOCATE_MESSAGE_MOVIES tempLoop
		// On initialisation, must ensure the scaleform movie index is NULL
		g_sLocateMessagesMP[tempLoop].lmMovieID	= NULL
		
		// Clear out the rest of the struct
		Clear_One_Locate_Message(tempLoop)
	ENDREPEAT
	
	// The scaleform movie names are specific to individual teams (and the number per team may vary), so fill these details
	Fill_MP_Locate_Message_Details()
	
	// Clear the Global Player Broadcast data that stores this player's corona details on display for any SCTV players to refer to
	REPEAT MAX_LOCATE_MESSAGE_MOVIES tempLoop
		Clear_One_Details_Data_For_Storage_For_SCTV(tempLoop)
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the Locate Message system
// NOTES:	This is to ensure the system cleans up if a request is made for a movie that doesn't then get used.
PROC Maintain_MP_Locate_Messages()

	// This function is intended to stop the Locate Message system getting clogged up by requests that don't then follow through and use the requested movie.
	// The rules are as follows:
	//	LMS_REQUESTED_THIS_FRAME	- A script has requested this movie on this frame so it is still current - change to LMS_PREVIOUSLY_REQUESTED
	//	LMS_PREVIOUSLY_REQUESTED	- A script on a previous frame requested this movie, but the request wasn't maintained this frame - mark this movie as no longer needed once it loads
	//	LMS_LOADED					- A script was told the movie had loaded but didn't then use the movie - change to LMS_PREVIOUSLY_REQUESTED (this gives the script a one-frame leeway to use the movie before it gets cleaned up)
	//	LMS_PREVIOUSLY_IN_USE		- A script displayed this movie on the previous frame, but it wasn't displayed this frame, so terminate the movie
	//	LMS_IN_USE_THIS_FRAME		- A script displayed this movie on this frame so it is still current - change to LMS_PREVIOUSLY_IN_USE
	//	LMS_AVAILABLE_FOR_USE		- NOTHING TO DO: This movie is currently available but not required
	//	LMS_NO_MOVIE_DO_NOT_USE		= DO NOTHING: This array position is not available for this team
	
	g_eLocateMessageStage	thisStage	= LMS_AVAILABLE_FOR_USE
	INT						tempLoop	= 0
	
	REPEAT MAX_LOCATE_MESSAGE_MOVIES tempLoop
		thisStage = Get_This_Locate_Message_Stage(tempLoop)
		
		SWITCH (thisStage)
			// Do Nothing with these
			CASE LMS_AVAILABLE_FOR_USE
			CASE LMS_NO_MOVIE_DO_NOT_USE
				BREAK
				
			// These stages need to be set as previously requested allowing them to be cleaned up if they remain unused
			CASE LMS_REQUESTED_THIS_FRAME
			CASE LMS_LOADED
				Set_This_Locate_Message_Stage_As_Previously_Requested(tempLoop)
				BREAK
				
			// This stage needs to free up the movie because it has been requested but not used
			// NOTE: I'm going to wait until it is loaded before freeing up the movie because it allows another request to 'adopt' this movie request while it is still loading
			CASE LMS_PREVIOUSLY_REQUESTED
				IF (Is_This_Locate_Message_Ready_To_Use(tempLoop))
					Terminate_This_Locate_Message(tempLoop)
				ENDIF
				BREAK
				
			// This stage needs to set the the Locate Message as previously in use so that we can tell if it is still required on the next frame or not
			CASE LMS_IN_USE_THIS_FRAME
				Set_This_Locate_Message_Stage_As_Previously_In_Use(tempLoop)
				BREAK
				
			// This stage needs to free up this movie now, it's been used and is no longer in use
			CASE LMS_PREVIOUSLY_IN_USE
				Terminate_This_Locate_Message(tempLoop)
				BREAK
				
			// Unknown ID
			DEFAULT
				NET_PRINT("...KGM MP [LocateMessage]: Maintain_MP_Locate_Messages(): Unknown Locate Message Stage. Add to SWITCH statement.") NET_NL()
				SCRIPT_ASSERT("Maintain_MP_Locate_Messages(): ERROR - Unknown Locate Message Stage. See Console Log. Tell Keith.")
				BREAK
		ENDSWITCH
	ENDREPEAT

ENDPROC


