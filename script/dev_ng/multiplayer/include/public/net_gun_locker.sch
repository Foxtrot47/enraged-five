//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	net_gun_locker	.sch										//
//		AUTHOR			:	Ata Tabrizi													//
//		DESCRIPTION		:	Header file that combines all the common functionality 		//
//							for gun locker.												//
//							This deals with multi-player data.							//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////
//																						//
// How to instantiate Gun Locker menu: 													//
// Create state machine using VAULT_WEAPON_CUSTOMIZATION_STAGE  enum 					//
//	e.g. MAINTAIN_OFFICE_VAULT_WEAPON_LOADOUT in ne_office.sch 							// 
// make sure that to include VMC_STAGE_CLEANUP which will handle the clean-up and reset //
// the gun locker state to VMC_STAGE_INIT												//
//																						//			
//////////////////////////////////////////////////////////////////////////////////////////


USING "globals.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "commands_network.sch"
USING "context_control_public.sch"
USING "net_ambience.sch"
USING "net_nodes.sch"
USING "net_freemode_cut.sch"
USING "net_doors.sch"
USING "rc_helper_functions.sch"
USING "gunclub_shop_private.sch"

#IF IS_DEBUG_BUILD
USING "shared_debug.sch"
#ENDIF


//╒══════════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════════╡ ENUMS & STRUCTS ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE: vault weapon menus
///    If you want to add more submenus add it before VM_MAIN_MENU
ENUM VAULT_WEAPON_MENU_ENUM
	VM_MELEE = 0,
	VM_PISTOL,
	VM_SMG,
	VM_RIFLE,
	VM_SHOTGUN,
	VM_SNIPER,
	VM_HEAVY,
	VM_THROWN,
	VM_MAIN_MENU,
	VM_TOTAL_MENU
ENDENUM

/// PURPOSE: vault weapon menu stages
ENUM VAULT_WEAPON_CUSTOMIZATION_STAGE
	VMC_STAGE_INIT = 0,
	VMC_STAGE_LOAD_ASSETS = 1,
	VWC_STAGE_WAITING_TO_TRIGGER,
	VMC_STAGE_DOOR_OPEN_ANIM,
	VMC_STAGE_DOOR_CLOSE_ANIM,
	VMC_STAGE_CUSTOMIZING,
	VMC_STAGE_CLEANUP
ENDENUM

/// PURPOSE: Gun locker menu inputs
STRUCT GUN_LOCKER_MENU_INPUT
	INT iLeftX, iLeftY, iRightX, iRightY, iMoveUp, iCursorValue
	BOOL bAccept
	BOOL bBack
	BOOL bPrevious
	BOOL bNext
ENDSTRUCT

CONST_INT VAULT_PISTOL_CREATED							0
CONST_INT VAULT_RIFLE_CREATED							1
CONST_INT VAULT_SHOTGUN_CREATED							2
CONST_INT VAULT_SMG_CREATED								3
CONST_INT VAULT_BOMB_CREATED							4
CONST_INT VAULT_GRENADE_CREATED							5
CONST_INT VAULT_MG_CREATED								6
CONST_INT VAULT_SNIPER_CREATED							7
CONST_INT VAULT_ASSRIFLE_CREATED						8

CONST_INT VAULT_GUN_DOOR_ANIM_INITIALISED 				0
CONST_INT VAULT_GUN_DOOR_NET_RESERVED 					1
CONST_INT VAULT_GUN_DOOR_ANIM_RUNNING					2
CONST_INT VAULT_DOOR_ANIM_STAGE_FINISHED				3
CONST_INT OFFICE_GUN_VAULT_DOOR_CREATED					4
CONST_INT VAULT_SAFE_DOOR_ANIM_INITIALISED 				5
CONST_INT VAULT_SAFE_DOOR_NET_RESERVED 					6
CONST_INT VAULT_SAFE_DOOR_ANIM_RUNNING					7
CONST_INT OFFICE_SAFE_VAULT_DOOR_CREATED				8
CONST_INT BS_GET_NUM_AVAILABLE_WEAPON_GROUP				9
CONST_INT VAULT_DOOR_OPENED								10

CONST_INT WEAPON_VAULT_TOTAL					8
CONST_INT VAULT_WEAPON_MENU_OPTION 		 		1
CONST_INT VAULT_WEAPON_MAX_SUB_MENU_ITEM 		COUNT_OF(WEAPON_BITSET)
CONST_INT MAX_NUMBER_WEAPON_MODELS				9
CONST_INT MAX_NUMBER_WEAPON_COMP_MODELS			3

STRUCT OFFICE_VAULT_DOOR_SYNC_STRUCT
	MODEL_NAMES modelName
	STRING sAnimDicName
	STRING sAnimName
	VECTOR vDoorPosition
	VECTOR vDoorRotation
	VECTOR vScenePosition
	VECTOR vSceneHeading
	VECTOR vCameraPos
	VECTOR vCameraRot
	VECTOR vPlayerPos
	FLOAT fPlayerHeading
	INT iNetSceneid
ENDSTRUCT

STRUCT VAULT_WEAPON_PROPS_STRUCT
	INT iCreatedPropBit			= 0
	INT iOwnWeaponBit			= 0
	BOOL bUseBitsetTwo			= FALSE
	BOOL bCreateAsWeaponObject	= FALSE
	VECTOR vCoords
	VECTOR vRotation
	WEAPON_TYPE eWeaponType		= WEAPONTYPE_INVALID
	MODEL_NAMES eWeaponModel 	= DUMMY_MODEL_FOR_SCRIPT
	MODEL_NAMES eWeaponCompModel[MAX_NUMBER_WEAPON_COMP_MODELS]
ENDSTRUCT

STRUCT VAULT_WEAPON_LOCATE_STRUCT
	FLOAT fWidth				= 0.0
	FLOAT fHeading				= 0.0
	FLOAT fLeeway				= 0.0
	VECTOR vAreaOne
	VECTOR vAreaTwo
ENDSTRUCT

/// PURPOSE: vault weapon loadout customization struct
STRUCT VAULT_WEAPON_LOADOUT_CUSTOMIZATION
	INT iVaultWeaponContext = NEW_CONTEXT_INTENTION
	INT iWeaponMenuCurrentItem, iVaultWeaponBS, iWeaponMenuTopItem, iAvailableWeaponBS , iNumAvailableWeaponGroup, iHideAllWeaponBS, iLocalBS, iWeaponModelBS
	INT iWeaponModelBS2
	INT iCurrentSubMenuType[WEAPON_VAULT_TOTAL]
	VAULT_WEAPON_MENU_ENUM eWeaponCurrentMenu
	VAULT_WEAPON_CUSTOMIZATION_STAGE eCustomizationStage = VMC_STAGE_INIT
	GUN_LOCKER_MENU_INPUT sWeaponMenuInput
	TIME_DATATYPE scrollDelay
	BOOL bMenuInitialised
	BOOL bReBuildMenu
	BOOL bIsMenuItemAvailable
	INT iWeaponMenuTypeBS[WEAPON_VAULT_TOTAL]
	SCRIPT_TIMER sMenuTimer
	OFFICE_VAULT_DOOR_SYNC_STRUCT sGunVaultDoor
	OBJECT_INDEX weapons[MAX_NUMBER_WEAPON_MODELS]
	OBJECT_INDEX weapons2[MAX_NUMBER_WEAPON_MODELS]
	VAULT_WEAPON_PROPS_STRUCT PropDetails[MAX_NUMBER_WEAPON_MODELS]
	VAULT_WEAPON_LOCATE_STRUCT LocateDetails
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT VAULT_WEAPON_DEBUG_DATA
	VECTOR vWeaponPosition[MAX_NUMBER_WEAPON_MODELS]
	VECTOR vWeaponRotation[MAX_NUMBER_WEAPON_MODELS]
ENDSTRUCT
#ENDIF

/// PURPOSE:
///    sets up controls for gun locker menus
/// PARAMS:
///    sGunLockerMenuInput - inputs
///    iMenuCurrentItem - current menu item 
PROC SETUP_GUN_LOCKER_MENU_CONTROLS(GUN_LOCKER_MENU_INPUT& sGunLockerMenuInput, INT& iMenuCurrentItem)

	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(sGunLockerMenuInput.iLeftX,sGunLockerMenuInput.iLeftY,sGunLockerMenuInput.iRightX,sGunLockerMenuInput.iRightY )
	
	// Set the input flag states
	sGunLockerMenuInput.bAccept			= (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
	sGunLockerMenuInput.bBack		 	= (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
	sGunLockerMenuInput.bPrevious		= ((sGunLockerMenuInput.iLeftY < -64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)) 
	sGunLockerMenuInput.bNext			= ((sGunLockerMenuInput.iLeftY > 64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN))	
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	IF LOAD_MENU_ASSETS()
	AND GET_PAUSE_MENU_STATE() = PM_INACTIVE
	AND NOT IS_SYSTEM_UI_BEING_DISPLAYED()
	AND NOT IS_WARNING_MESSAGE_ACTIVE()
	AND NOT g_sShopSettings.bProcessStoreAlert

		// Mouse control support  
		IF IS_PC_VERSION()	
		AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
			IF IS_USING_CURSOR(FRONTEND_CONTROL)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)	
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
				ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
				IF ((g_iMenuCursorItem = MENU_CURSOR_NO_ITEM) OR (g_iMenuCursorItem = MENU_CURSOR_NO_CAMERA_MOVE) OR (g_iMenuCursorItem = MENU_CURSOR_DRAG_CAM))
					ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
					ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
				ELSE
					DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
					DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
				ENDIF	
				HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
				HANDLE_MENU_CURSOR(FALSE)
			ENDIF
			
			// Mouse select
			IF IS_MENU_CURSOR_ACCEPT_PRESSED()
				IF g_iMenuCursorItem = iMenuCurrentItem
					sGunLockerMenuInput.bAccept = TRUE
				ELSE
					iMenuCurrentItem = g_iMenuCursorItem
					SET_CURRENT_MENU_ITEM(iMenuCurrentItem)		
				ENDIF		
			ENDIF
		
			// Menu cursor back
			IF IS_MENU_CURSOR_CANCEL_PRESSED()
				sGunLockerMenuInput.bBack = TRUE
			ENDIF
			
			// Mouse scroll up
			IF IS_MENU_CURSOR_SCROLL_UP_PRESSED()
				sGunLockerMenuInput.bPrevious = TRUE
			ENDIF
			
			// Mouse scroll down
			IF IS_MENU_CURSOR_SCROLL_DOWN_PRESSED()
				sGunLockerMenuInput.bNext = TRUE
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Vault weapon menu clean up 
///    bForceCleanUp = TRUE if we are exiting property
PROC PROCESS_WEAPON_VAULT_MENU_CLEANUP(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization,NETWORK_INDEX &VaultDoor,INT iCurrentProperty,BOOL bForceCleanUp = FALSE,BOOL bInteriorSwitchCleanUp = FALSE)
	
	PRINTLN("[GUN LOCKER] Vault weapon loadout - VMC_STAGE_CLEANUP")
	
	RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
	sWLoadoutCustomization.iVaultWeaponContext = NEW_CONTEXT_INTENTION
	sWLoadoutCustomization.iWeaponMenuCurrentItem = 0
	sWLoadoutCustomization.iWeaponMenuTopItem = 0
	sWLoadoutCustomization.iNumAvailableWeaponGroup = 0
	sWLoadoutCustomization.eWeaponCurrentMenu = VM_MAIN_MENU
	sWLoadoutCustomization.bMenuInitialised = FALSE
	sWLoadoutCustomization.bReBuildMenu = FALSE
	
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_DOOR_ANIM_STAGE_FINISHED)
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_GUN_DOOR_ANIM_RUNNING)
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS, BS_GET_NUM_AVAILABLE_WEAPON_GROUP)
	
	INT iNumSubMenu
	FOR iNumSubMenu = 0 TO WEAPON_VAULT_TOTAL - 1
		CLEAR_BIT(sWLoadoutCustomization.iVaultWeaponBS,iNumSubMenu)
		CLEAR_BIT(sWLoadoutCustomization.iAvailableWeaponBS,iNumSubMenu)
		CLEAR_BIT(sWLoadoutCustomization.iHideAllWeaponBS,iNumSubMenu)
	ENDFOR

	IF bForceCleanUp
		INT iNumWeaponModels
		FOR iNumWeaponModels = 0 TO MAX_NUMBER_WEAPON_MODELS -1
			IF DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[iNumWeaponModels])
				DELETE_OBJECT(sWLoadoutCustomization.weapons[iNumWeaponModels])
			ENDIF
			
			IF DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons2[iNumWeaponModels])
				DELETE_OBJECT(sWLoadoutCustomization.weapons2[iNumWeaponModels])
			ENDIF
		ENDFOR
		CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_PISTOL_CREATED)
		CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_RIFLE_CREATED)  
		CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_SMG_CREATED)  
		CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_BOMB_CREATED)  
		CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_SHOTGUN_CREATED)  
		CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_GRENADE_CREATED)
		CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_SNIPER_CREATED)
		CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS,VAULT_MG_CREATED)
		CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS2,VAULT_PISTOL_CREATED)
		CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS2,VAULT_RIFLE_CREATED)  
		CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS2,VAULT_SMG_CREATED)  
		CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS2,VAULT_BOMB_CREATED)  
		CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS2,VAULT_SHOTGUN_CREATED)  
		CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS2,VAULT_GRENADE_CREATED)
		CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS2,VAULT_SNIPER_CREATED)
		CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS2,VAULT_MG_CREATED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_GUN_DOOR_ANIM_INITIALISED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_GUN_DOOR_NET_RESERVED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_DOOR_ANIM_STAGE_FINISHED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS,OFFICE_GUN_VAULT_DOOR_CREATED)
		
		IF IS_PROPERTY_OFFICE(iCurrentProperty)
			NETWORK_STOP_SYNCHRONISED_SCENE(sWLoadoutCustomization.sGunVaultDoor.iNetSceneid)
		ENDIF
		sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_INIT	
	ELIF bInteriorSwitchCleanUp
		IF IS_PROPERTY_OFFICE(iCurrentProperty)
			IF NETWORK_DOES_NETWORK_ID_EXIST(VaultDoor)
				IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_OBJ(VaultDoor))
					NETWORK_REQUEST_CONTROL_OF_ENTITY(NET_TO_OBJ(VaultDoor))
				ELSE
					DELETE_NET_ID(VaultDoor)
					IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(DEFAULT, RESERVATION_LOCAL_ONLY)-1, FALSE, TRUE)
						IF IS_BIT_SET(sWLoadoutCustomization.iLocalBS,VAULT_GUN_DOOR_NET_RESERVED)
							RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(DEFAULT, RESERVATION_LOCAL_ONLY)-1)
							CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_GUN_DOOR_NET_RESERVED)
						ENDIF
					ENDIF
					CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_GUN_DOOR_ANIM_INITIALISED)
					CLEAR_BIT(sWLoadoutCustomization.iLocalBS,VAULT_DOOR_ANIM_STAGE_FINISHED)
					CLEAR_BIT(sWLoadoutCustomization.iLocalBS,OFFICE_GUN_VAULT_DOOR_CREATED)
					sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_INIT	
				ENDIF
			ENDIF
		ENDIF	
	ELSE
		sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_INIT
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets weapon group depending on which menu we currently are 
/// PARAMS:
///    eCurrentMenu - vault current sub menu 
/// RETURNS:
///    WEAPON_GROUP depending on current sub menu
FUNC WEAPON_GROUP GET_WEAPON_GROUP_FOR_VAULT_MENU(VAULT_WEAPON_MENU_ENUM eCurrentMenu)
	
	SWITCH eCurrentMenu
		CASE VM_MELEE
			RETURN WEAPONGROUP_MELEE
		BREAK
		CASE VM_PISTOL
			RETURN WEAPONGROUP_PISTOL
		BREAK
		CASE VM_SMG
			RETURN WEAPONGROUP_SMG
		BREAK
		CASE VM_RIFLE
			RETURN WEAPONGROUP_RIFLE
		BREAK
		CASE VM_SHOTGUN
			RETURN WEAPONGROUP_SHOTGUN
		BREAK
		CASE VM_SNIPER
			RETURN WEAPONGROUP_SNIPER
		BREAK
		CASE VM_HEAVY
			RETURN WEAPONGROUP_HEAVY
		BREAK
		CASE VM_THROWN
			RETURN WEAPONGROUP_THROWN
		BREAK
	ENDSWITCH

	RETURN WEAPONGROUP_INVALID
	
ENDFUNC


#IF IS_DEBUG_BUILD
FUNC STRING GET_DEBUG_MENU_NAME(VAULT_WEAPON_MENU_ENUM eCurrentMenu)
	
	SWITCH eCurrentMenu
		CASE VM_MELEE
			RETURN "WEAPONGROUP_MELEE"
		BREAK
		CASE VM_PISTOL
			RETURN "WEAPONGROUP_PISTOL"
		BREAK
		CASE VM_SMG
			RETURN "WEAPONGROUP_SMG"
		BREAK
		CASE VM_RIFLE
			RETURN "WEAPONGROUP_RIFLE"
		BREAK
		CASE VM_SHOTGUN
			RETURN "WEAPONGROUP_SHOTGUN"
		BREAK
		CASE VM_SNIPER
			RETURN "WEAPONGROUP_SNIPER"
		BREAK
		CASE VM_HEAVY
			RETURN "WEAPONGROUP_HEAVY"
		BREAK
		CASE VM_THROWN
			RETURN "WEAPONGROUP_THROWN"
		BREAK
	ENDSWITCH

	RETURN "GET_DEBUG_MENU_NAME - INVALID MENU"
ENDFUNC
#ENDIF

/// PURPOSE:
///   Gets weapon menu names
/// PARAMS:
///    eCurrentMenu - vault current sub menu 
///    bTitle - is it menu title?
/// RETURNS:
///    string for menu itmes or titles
FUNC STRING GET_VAULT_WEAPON_CLASS_NAME_FOR_MENU(VAULT_WEAPON_MENU_ENUM eCurrentMenu,BOOL bTitle = FALSE)
	
	SWITCH eCurrentMenu		
		CASE VM_MELEE 		IF	!bTitle	RETURN	"VAULT_WMENUI_8"  ELSE	RETURN	"VAULT_WMENUT_9"	ENDIF	BREAK 
		CASE VM_PISTOL		IF	!bTitle	RETURN	"VAULT_WMENUI_9"  ELSE	RETURN	"VAULT_WMENUT_2"	ENDIF	BREAK 
		CASE VM_SMG			IF	!bTitle	RETURN	"VAULT_WMENUI_3"  ELSE	RETURN	"VAULT_WMENUT_4"	ENDIF	BREAK 
		CASE VM_RIFLE		IF	!bTitle	RETURN	"VAULT_WMENUI_4"  ELSE	RETURN	"VAULT_WMENUT_5"	ENDIF	BREAK  
		CASE VM_SHOTGUN 	IF	!bTitle	RETURN	"VAULT_WMENUI_2"  ELSE	RETURN	"VAULT_WMENUT_3"	ENDIF	BREAK 
		CASE VM_SNIPER 		IF	!bTitle	RETURN	"VAULT_WMENUI_5"  ELSE	RETURN	"VAULT_WMENUT_6"	ENDIF	BREAK  
		CASE VM_HEAVY 		IF	!bTitle	RETURN	"VAULT_WMENUI_6"  ELSE	RETURN	"VAULT_WMENUT_7"	ENDIF	BREAK 
		CASE VM_THROWN 		IF	!bTitle	RETURN	"VAULT_WMENUI_7"  ELSE	RETURN	"VAULT_WMENUT_8"	ENDIF	BREAK 		
	ENDSWITCH
	RETURN ""
	
ENDFUNC

/// PURPOSE:
///   Gets weapon menu description
/// PARAMS:
///    eCurrentMenu - vault current sub menu 
/// RETURNS:
///    string for menu itmes or titles
FUNC STRING GET_VAULT_WEAPON_MAIN_MENU_DESCRIPTION(VAULT_WEAPON_MENU_ENUM eCurrentMenu)
	
	SWITCH eCurrentMenu		
		CASE VM_MELEE 			RETURN	"VAULT_MENU_D_8" 	BREAK 
		CASE VM_PISTOL			RETURN	"VAULT_MENU_D_1" 	BREAK 
		CASE VM_SMG				RETURN	"VAULT_MENU_D_3" 	BREAK 
		CASE VM_RIFLE			RETURN	"VAULT_MENU_D_4" 	BREAK  
		CASE VM_SHOTGUN 		RETURN	"VAULT_MENU_D_2" 	BREAK 
		CASE VM_SNIPER 			RETURN	"VAULT_MENU_D_5" 	BREAK  
		CASE VM_HEAVY 			RETURN	"VAULT_MENU_D_6" 	BREAK 
		CASE VM_THROWN 			RETURN	"VAULT_MENU_D_7" 	BREAK 		
	ENDSWITCH
	RETURN ""
	
ENDFUNC


/// PURPOSE: 
///    Gets "ALL - weapon type" name
/// PARAMS:
///    eCurrentMenu - vault current sub menu 
/// RETURNS:
///    string for first submenu item e.g. All Pistols 
FUNC STRING GET_VAULT_ALL_WEAPON_NAME_FOR_MENU(VAULT_WEAPON_MENU_ENUM eCurrentMenu)
	
	SWITCH eCurrentMenu		
			CASE VM_MELEE 			RETURN	"VAULT_WMENUFIRST_5"  	BREAK 
			CASE VM_PISTOL			RETURN	"VAULT_WMENUFIRST_1"  	BREAK 
			CASE VM_SMG				RETURN	"VAULT_WMENUFIRST_7"  	BREAK 
			CASE VM_RIFLE			RETURN	"VAULT_WMENUFIRST_2"  	BREAK  
			CASE VM_SHOTGUN 		RETURN	"VAULT_WMENUFIRST_4"  	BREAK 
			CASE VM_SNIPER 			RETURN	"VAULT_WMENUFIRST_3"  	BREAK  
			CASE VM_HEAVY 			RETURN	"VAULT_WMENUFIRST_6"  	BREAK 
			CASE VM_THROWN 			RETURN	"VAULT_WMENUFIRST_8"  	BREAK 		
	ENDSWITCH
	
	RETURN ""
	
ENDFUNC

/// PURPOSE:
///    This is to check if weapon group is MG or SMG as we only have one menu for both of them
/// PARAMS:
///    eWeaponCurrentMenu - vault current sub menu 
///    eWeaponGroup - WEAPON_GROUP of weapon type we are querying 
/// RETURNS:
///    True if WEAPON_GROUP is SMG or MG 
FUNC BOOL IS_MENU_WEAPON_MACHINE_GUN(VAULT_WEAPON_MENU_ENUM eWeaponCurrentMenu, WEAPON_GROUP eWeaponGroup)
	
	IF eWeaponCurrentMenu = VM_SMG
		IF eWeaponGroup = WEAPONGROUP_SMG
		OR eWeaponGroup = WEAPONGROUP_MG
			RETURN TRUE 
		ENDIF
	ENDIF
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Check if custom weapon loadout is enabled 
/// RETURNS:
///    TRUE if enabled - means player has set one or more owned weapon to be hidden 
FUNC BOOL IS_CUSTOM_WEAPON_LOADOUT_ENABLED()
	IF DOES_PLAYER_OWN_ANY_GUN_LOCKER()
		INT iWeapon
		FOR iWeapon = 0 TO VAULT_WEAPON_MAX_SUB_MENU_ITEM
			WEAPON_TYPE eWeaponType = GET_WEAPONTYPE_FROM_WEAPON_BITSET(INT_TO_ENUM(WEAPON_BITSET, iWeapon))
			IF IS_MP_WEAPONTYPE_VALID(eWeaponType)
				IF NOT IS_WEAPON_AN_MK2_VARIANT(eWeaponType)
					IF IS_WEAPONTYPE_MK2_UPGRADABLE(eWeaponType)
					AND IS_MP_WEAPON_ADDON_EQUIPPED(WEAPONCOMPONENT_DLC_GUNRUN_MK2_UPGRADE, eWeaponType)
						eWeaponType = GET_MK2_WEAPON_TYPE_FROM_MK1_WEAPON(eWeaponType)
					ENDIF
					IF IS_MP_WEAPON_PURCHASED(eWeaponType)
					OR ALLOW_SPECIAL_PICKUPS_IN_GUN_LOCKER(eWeaponType)
						IF GET_PACKED_STAT_BOOL(GET_WEAPON_HIDE_PACKED_STAT(eWeaponType))
							// Hidden weapon
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR	
			
		IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_MELEE)
		OR GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_PISTOL)
		OR GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_RIFLE)
		OR GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_SMG)
		OR GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_SNIPER)
		OR GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_SHOTGUN)
		OR GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_THROWN)
		OR GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_HEAVY)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Disbale custom weapon laout
/// PARAMS:
///    bEnable - True to enable custom weapon loadout
///    bEnable - False to disable custom weapon loadout - show all weapons
PROC SET_CUSTOM_WEAPON_LOADOUT(BOOL bEnable)
	INT iWeapon
	FOR iWeapon = 0 TO VAULT_WEAPON_MAX_SUB_MENU_ITEM
		WEAPON_TYPE eWeaponType = GET_WEAPONTYPE_FROM_WEAPON_BITSET(INT_TO_ENUM(WEAPON_BITSET, iWeapon))
		IF IS_MP_WEAPONTYPE_VALID(eWeaponType)
			IF NOT IS_WEAPON_AN_MK2_VARIANT(eWeaponType)
				IF IS_WEAPONTYPE_MK2_UPGRADABLE(eWeaponType)
				AND IS_MP_WEAPON_ADDON_EQUIPPED(WEAPONCOMPONENT_DLC_GUNRUN_MK2_UPGRADE, eWeaponType)
					eWeaponType = GET_MK2_WEAPON_TYPE_FROM_MK1_WEAPON(eWeaponType)
				ENDIF
				IF IS_MP_WEAPON_PURCHASED(eWeaponType)
				OR ALLOW_SPECIAL_PICKUPS_IN_GUN_LOCKER(eWeaponType)
					IF !bEnable
						// Show all weapons
						IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),eWeaponType,0,FALSE,FALSE)
							GIVE_PLAYER_ALL_EQUIPPED_WEAPON_ADDONS_FOR_WEAPON(eWeaponType)
							GIVE_WEAPON_EQUIPPED_TINT(eWeaponType)
						ENDIF	
					ELSE
						IF GET_PACKED_STAT_BOOL(GET_WEAPON_HIDE_PACKED_STAT(eWeaponType))
						OR IS_WEAPON_TYPE_HIDE_ALL_ACTIVE(GET_WEAPONTYPE_GROUP(eWeaponType),eWeaponType)
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(),eWeaponType)
							ENDIF
						ELSE
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),eWeaponType,0,FALSE,FALSE)
								GIVE_PLAYER_ALL_EQUIPPED_WEAPON_ADDONS_FOR_WEAPON(eWeaponType)
								GIVE_WEAPON_EQUIPPED_TINT(eWeaponType)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR	
ENDPROC

/// PURPOSE:
///    Gets number of available/bought weapon group
PROC GET_NUM_AVAILABLE_WEAPON_GROUP(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)
	
	INT iWeapon
	IF NOT IS_BIT_SET(sWLoadoutCustomization.iLocalBS, BS_GET_NUM_AVAILABLE_WEAPON_GROUP)
	
		FOR iWeapon = 0 TO VAULT_WEAPON_MAX_SUB_MENU_ITEM
			WEAPON_TYPE eWeaponType = GET_WEAPONTYPE_FROM_WEAPON_BITSET(INT_TO_ENUM(WEAPON_BITSET, iWeapon))
			IF IS_MP_WEAPONTYPE_VALID(eWeaponType)
				IF NOT IS_WEAPON_AN_MK2_VARIANT(eWeaponType)
					IF IS_WEAPONTYPE_MK2_UPGRADABLE(eWeaponType)
					AND IS_MP_WEAPON_ADDON_EQUIPPED(WEAPONCOMPONENT_DLC_GUNRUN_MK2_UPGRADE, eWeaponType)
						eWeaponType = GET_MK2_WEAPON_TYPE_FROM_MK1_WEAPON(eWeaponType)
					ENDIF
					IF IS_MP_WEAPON_PURCHASED(eWeaponType)
					OR ALLOW_SPECIAL_PICKUPS_IN_GUN_LOCKER(eWeaponType)
						IF GET_WEAPONTYPE_GROUP(eWeaponType) != WEAPONGROUP_INVALID
							IF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_MELEE
							OR eWeaponType = WEAPONTYPE_DLC_KNUCKLE
								IF NOT IS_BIT_SET(sWLoadoutCustomization.iAvailableWeaponBS,ENUM_TO_INT(VM_MELEE))
									SET_BIT(sWLoadoutCustomization.iAvailableWeaponBS,ENUM_TO_INT(VM_MELEE))
									sWLoadoutCustomization.iNumAvailableWeaponGroup++
								ENDIF
							ELIF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_PISTOL
								IF NOT IS_BIT_SET(sWLoadoutCustomization.iAvailableWeaponBS,ENUM_TO_INT(VM_PISTOL))
									SET_BIT(sWLoadoutCustomization.iAvailableWeaponBS,ENUM_TO_INT(VM_PISTOL))
									sWLoadoutCustomization.iNumAvailableWeaponGroup++
									IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OFFICE_VAULT_OWN_PISTOL)
										SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OFFICE_VAULT_OWN_PISTOL)
										PRINTLN("[GUN LOCKER] GET_NUM_AVAILABLE_WEAPON_GROUP - PROPERTY_BROADCAST_BS_OFFICE_VAULT_OWN_PISTOL TRUE")
									ENDIF
								ENDIF
							ELIF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_RIFLE
								IF NOT IS_BIT_SET(sWLoadoutCustomization.iAvailableWeaponBS,ENUM_TO_INT(VM_RIFLE))
									SET_BIT(sWLoadoutCustomization.iAvailableWeaponBS,ENUM_TO_INT(VM_RIFLE))
									sWLoadoutCustomization.iNumAvailableWeaponGroup++
									IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OFFICE_VAULT_OWN_RIFLE)
										SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OFFICE_VAULT_OWN_RIFLE)
										PRINTLN("[GUN LOCKER] GET_NUM_AVAILABLE_WEAPON_GROUP - PROPERTY_BROADCAST_BS_OFFICE_VAULT_OWN_RIFLE TRUE")
									ENDIF
								ENDIF
							ELIF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_SHOTGUN
							OR eWeaponType = WEAPONTYPE_DLC_MUSKET
								IF NOT IS_BIT_SET(sWLoadoutCustomization.iAvailableWeaponBS,ENUM_TO_INT(VM_SHOTGUN))
									SET_BIT(sWLoadoutCustomization.iAvailableWeaponBS,ENUM_TO_INT(VM_SHOTGUN))
									sWLoadoutCustomization.iNumAvailableWeaponGroup++
									IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_SHOTGUN)
										SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_SHOTGUN)
										PRINTLN("[GUN LOCKER] GET_NUM_AVAILABLE_WEAPON_GROUP - PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_SHOTGUN TRUE")
									ENDIF
								ENDIF
							ELIF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_SNIPER
							AND eWeaponType != WEAPONTYPE_DLC_MUSKET
								IF NOT IS_BIT_SET(sWLoadoutCustomization.iAvailableWeaponBS,ENUM_TO_INT(VM_SNIPER))
									SET_BIT(sWLoadoutCustomization.iAvailableWeaponBS,ENUM_TO_INT(VM_SNIPER))
									sWLoadoutCustomization.iNumAvailableWeaponGroup++
								ENDIF
								IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_SNIPER)
									SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_SNIPER)
									PRINTLN("[GUN LOCKER] GET_NUM_AVAILABLE_WEAPON_GROUP - PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_SNIPER TRUE")
								ENDIF
							ELIF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_HEAVY
								IF NOT IS_BIT_SET(sWLoadoutCustomization.iAvailableWeaponBS,ENUM_TO_INT(VM_HEAVY))
									SET_BIT(sWLoadoutCustomization.iAvailableWeaponBS,ENUM_TO_INT(VM_HEAVY))
									sWLoadoutCustomization.iNumAvailableWeaponGroup++
								ENDIF
							ELIF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_THROWN
							OR eWeaponType = WEAPONTYPE_PETROLCAN
								IF NOT IS_BIT_SET(sWLoadoutCustomization.iAvailableWeaponBS,ENUM_TO_INT(VM_THROWN))
									SET_BIT(sWLoadoutCustomization.iAvailableWeaponBS,ENUM_TO_INT(VM_THROWN))
									sWLoadoutCustomization.iNumAvailableWeaponGroup++
								ENDIF
							IF eWeaponType = WEAPONTYPE_STICKYBOMB
							OR eWeaponType = WEAPONTYPE_DLC_PROXMINE
								IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_BOMB)
									SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_BOMB)
									PRINTLN("[GUN LOCKER] GET_NUM_AVAILABLE_WEAPON_GROUP - PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_BOMB TRUE")
								ENDIF
							ELIF eWeaponType = WEAPONTYPE_GRENADE
							OR eWeaponType = WEAPONTYPE_SMOKEGRENADE
								IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_GRENADE)
									SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_GRENADE)
									PRINTLN("[GUN LOCKER] GET_NUM_AVAILABLE_WEAPON_GROUP - PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_GRENADE TRUE")
								ENDIF
							ENDIF
							ELIF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_SMG
							OR GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_MG
								IF NOT IS_BIT_SET(sWLoadoutCustomization.iAvailableWeaponBS,ENUM_TO_INT(VM_SMG))
									SET_BIT(sWLoadoutCustomization.iAvailableWeaponBS,ENUM_TO_INT(VM_SMG))
									sWLoadoutCustomization.iNumAvailableWeaponGroup++
								ENDIF
								IF eWeaponType = WEAPONTYPE_MICROSMG
									IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_MICROSMG)
										SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_MICROSMG)
										PRINTLN("[GUN LOCKER] GET_NUM_AVAILABLE_WEAPON_GROUP - PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_MICROSMG TRUE")
									ENDIF
								ENDIF
								IF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_MG
									IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_MG)
										SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_MG)
										PRINTLN("[GUN LOCKER] GET_NUM_AVAILABLE_WEAPON_GROUP - PROPERTY_BROADCAST_BS2_OFFICE_VAULT_OWN_MG TRUE")
									ENDIF
								ENDIF	
							ENDIF	
						ELSE
							IF GET_FRAME_COUNT() % 60 = 0
								PRINTLN("[GUN LOCKER] GET_NUM_AVAILABLE_WEAPON_GROUP - Weapon group invalid")
							ENDIF
							
						ENDIF
						
					ELSE
						IF GET_FRAME_COUNT() % 60 = 0
							PRINTLN("[GUN LOCKER] GET_NUM_AVAILABLE_WEAPON_GROUP - Weapon not purchased:",GET_WEAPON_NAME(eWeaponType))
						ENDIF
					ENDIF	
				ENDIF
			ENDIF	
		ENDFOR
		SET_BIT(sWLoadoutCustomization.iLocalBS, BS_GET_NUM_AVAILABLE_WEAPON_GROUP)
		PRINTLN("[GUN LOCKER] GET_NUM_AVAILABLE_WEAPON_GROUP - BS_GET_NUM_AVAILABLE_WEAPON_GROUP - TRUE")
	ENDIF	
	//PRINTLN("GET_NUM_AVAILABLE_WEAPON_GROUP - Owned weapon group:",sWLoadoutCustomization.iNumAvailableWeaponGroup)
	
ENDPROC

/// PURPOSE:
///    This is special case for Knuckle, Jerry Can and Molotov as GET_WEAPONTYPE_GROUP doesn't return the right weapon group
/// PARAMS:
///    eWeaponCurrentMenu - current menu vault weapon loadout menu
///    eWeaponType - purchased wwapon type
/// RETURNS:
///    Return true if we are in melee menu and weapon type is WEAPONTYPE_DLC_KNUCKLE
FUNC BOOL IS_SPECIAL_WEAPON_TYPE(VAULT_WEAPON_MENU_ENUM eWeaponCurrentMenu,WEAPON_TYPE eWeaponType)
	IF eWeaponCurrentMenu = VM_MELEE
		IF eWeaponType = WEAPONTYPE_DLC_KNUCKLE
			RETURN TRUE
		ENDIF
	ELIF eWeaponCurrentMenu = VM_PISTOL
		IF eWeaponType = WEAPONTYPE_DLC_STUNGUNG_MP
			RETURN TRUE
		ENDIF
	ELIF eWeaponCurrentMenu = VM_THROWN
		IF eWeaponType = WEAPONTYPE_PETROLCAN
			RETURN TRUE
		ENDIF
	ELIF eWeaponCurrentMenu = VM_SHOTGUN
		IF eWeaponType = WEAPONTYPE_DLC_MUSKET
			RETURN TRUE
		ENDIF	
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets number of owned weapon type for the current sub menu
/// PARAMS:
///    eWeaponCurrentMenu - vault current sub menu 
FUNC INT GET_NUM_OWNED_WEAPON_TYPE(VAULT_WEAPON_MENU_ENUM eWeaponCurrentMenu)
	
	INT iNumWeapons, iWeapon
	WEAPON_GROUP eWeaponGroup
	eWeaponGroup = GET_WEAPON_GROUP_FOR_VAULT_MENU(eWeaponCurrentMenu)
	FOR iWeapon = 0 TO VAULT_WEAPON_MAX_SUB_MENU_ITEM
		WEAPON_TYPE eWeaponType = GET_WEAPONTYPE_FROM_WEAPON_BITSET(INT_TO_ENUM(WEAPON_BITSET, iWeapon))
		IF IS_MP_WEAPONTYPE_VALID(eWeaponType)
			IF NOT IS_WEAPON_AN_MK2_VARIANT(eWeaponType)
				IF IS_WEAPONTYPE_MK2_UPGRADABLE(eWeaponType)
				AND IS_MP_WEAPON_ADDON_EQUIPPED(WEAPONCOMPONENT_DLC_GUNRUN_MK2_UPGRADE, eWeaponType)
					eWeaponType = GET_MK2_WEAPON_TYPE_FROM_MK1_WEAPON(eWeaponType)
				ENDIF
				IF IS_MP_WEAPON_PURCHASED(eWeaponType)
				OR ALLOW_SPECIAL_PICKUPS_IN_GUN_LOCKER(eWeaponType)
					IF eWeaponGroup != WEAPONGROUP_INVALID
						IF eWeaponGroup = GET_WEAPONTYPE_GROUP(eWeaponType)
						OR IS_MENU_WEAPON_MACHINE_GUN(eWeaponCurrentMenu,GET_WEAPONTYPE_GROUP(eWeaponType))
						OR IS_SPECIAL_WEAPON_TYPE(eWeaponCurrentMenu, eWeaponType)
							IF eWeaponGroup = WEAPONGROUP_SNIPER
							AND eWeaponType = WEAPONTYPE_DLC_MUSKET
								//skip
							ELSE
								iNumWeapons++
							ENDIF
						ENDIF	
					ENDIF
				ELSE
					PRINTLN("[GUN LOCKER] GET_NUM_OWNED_WEAPON_TYPE - Weapon not purchased : ", GET_WEAPON_NAME(eWeaponType))
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	PRINTLN("[GUN LOCKER] GET_NUM_OWNED_WEAPON_TYPE:", GET_VAULT_WEAPON_CLASS_NAME_FOR_MENU(eWeaponCurrentMenu),"Owned number:",iNumWeapons)
	RETURN iNumWeapons
	
ENDFUNC
 
 /// PURPOSE:
 ///    Check if vault weapon loadout menu's current menu hide all option is active 
 /// PARAMS:
 FUNC BOOL IS_CURRENT_MENU_HIDE_ALL_ACTIVE(VAULT_WEAPON_MENU_ENUM eCurrentMenu)
	SWITCH eCurrentMenu
		CASE VM_MELEE
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_MELEE)
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
		CASE VM_PISTOL
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_PISTOL)
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		BREAK	
		CASE VM_SMG
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_SMG)
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		BREAK	
		CASE VM_RIFLE
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_RIFLE)
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		BREAK	
		CASE VM_SHOTGUN
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_SHOTGUN)
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		BREAK	
		CASE VM_SNIPER
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_SNIPER)
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		BREAK	
		CASE VM_HEAVY
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_HEAVY)
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		BREAK	
		CASE VM_THROWN
			IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_THROWN)
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets vault weapon sub menu item names
PROC SET_VAULT_WEAPON_LOADOUT_SUB_MENU_NAMES(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)
	
	WEAPON_GROUP eWeaponGroup
	INT iWeapon, iMenuIndex ,iNumOwnedWeaponType
	iNumOwnedWeaponType = GET_NUM_OWNED_WEAPON_TYPE(sWLoadoutCustomization.eWeaponCurrentMenu)
	eWeaponGroup = GET_WEAPON_GROUP_FOR_VAULT_MENU(sWLoadoutCustomization.eWeaponCurrentMenu)
	
	FOR iWeapon = 0 TO VAULT_WEAPON_MAX_SUB_MENU_ITEM
		WEAPON_TYPE eWeaponType = GET_WEAPONTYPE_FROM_WEAPON_BITSET(INT_TO_ENUM(WEAPON_BITSET, iWeapon))
		IF IS_MP_WEAPONTYPE_VALID(eWeaponType)
			IF NOT IS_WEAPON_AN_MK2_VARIANT(eWeaponType)
				IF IS_WEAPONTYPE_MK2_UPGRADABLE(eWeaponType)
				AND IS_MP_WEAPON_ADDON_EQUIPPED(WEAPONCOMPONENT_DLC_GUNRUN_MK2_UPGRADE, eWeaponType)
					eWeaponType = GET_MK2_WEAPON_TYPE_FROM_MK1_WEAPON(eWeaponType)
				ENDIF
				IF (IS_MP_WEAPON_PURCHASED(eWeaponType)
				OR ALLOW_SPECIAL_PICKUPS_IN_GUN_LOCKER(eWeaponType))
					IF eWeaponGroup = GET_WEAPONTYPE_GROUP(eWeaponType)
					OR IS_MENU_WEAPON_MACHINE_GUN(sWLoadoutCustomization.eWeaponCurrentMenu,GET_WEAPONTYPE_GROUP(eWeaponType))
					OR IS_SPECIAL_WEAPON_TYPE(sWLoadoutCustomization.eWeaponCurrentMenu,eWeaponType)
						IF eWeaponGroup = WEAPONGROUP_SNIPER
						AND eWeaponType = WEAPONTYPE_DLC_MUSKET
							//skip
						ELSE
							IF eWeaponGroup != WEAPONGROUP_INVALID
								iMenuIndex++
								IF (0 < iMenuIndex) AND (iMenuIndex <= iNumOwnedWeaponType)
									IF eWeaponType = WEAPONTYPE_HEAVYSNIPER
									AND GET_CURRENT_LANGUAGE() = LANGUAGE_GERMAN
										ADD_MENU_ITEM_TEXT(iMenuIndex, "WEP_HVSNIPER")
									ELSE
										ADD_MENU_ITEM_TEXT(iMenuIndex, GET_WEAPON_NAME(eWeaponType))
									ENDIF
									
									#IF IS_DEBUG_BUILD
									PRINTLN("[GUN LOCKER] SET_VAULT_WEAPON_LOADOUT_SUB_MENU_NAMES - Adding: ",GET_WEAPON_NAME(eWeaponType), " TO ", GET_DEBUG_MENU_NAME(sWLoadoutCustomization.eWeaponCurrentMenu), " MENU" )
									#ENDIF
									
									// This runs only for the first time each sub menu is opened to update menu options with packed stats
									IF NOT IS_BIT_SET(sWLoadoutCustomization.iVaultWeaponBS,ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu))	
										IF GET_PACKED_STAT_BOOL(GET_WEAPON_HIDE_PACKED_STAT(eWeaponType))
											SET_BIT(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],iMenuIndex) // hide
										ELSE
											CLEAR_BIT(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],iMenuIndex) // show
										ENDIF	
										PRINTLN("[GUN LOCKER] SET_VAULT_WEAPON_LOADOUT_SUB_MENU_NAMES GET_PACKED_STAT_BOOL: stat name: ",GET_WEAPON_HIDE_PACKED_STAT(eWeaponType)," VALUE: ", GET_PACKED_STAT_BOOL(GET_WEAPON_HIDE_PACKED_STAT(eWeaponType)))
										PRINTLN("[GUN LOCKER] SET_VAULT_WEAPON_LOADOUT_SUB_MENU_NAMES - iMenuIndex ",iMenuIndex)
									ENDIF
									IF NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],iMenuIndex)	// show
										ADD_MENU_ITEM_TEXT(iMenuIndex,"VAULT_SHOW",0,sWLoadoutCustomization.bIsMenuItemAvailable)
										IF NOT IS_CURRENT_MENU_HIDE_ALL_ACTIVE(sWLoadoutCustomization.eWeaponCurrentMenu)
											SET_PACKED_STAT_BOOL(GET_WEAPON_HIDE_PACKED_STAT(eWeaponType), FALSE)
											PRINTLN("[GUN LOCKER] SET_PACKED_STAT_BOOL: stat name",GET_WEAPON_HIDE_PACKED_STAT(eWeaponType),"- FALSE - show weapon")
											IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
												GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),eWeaponType,0,FALSE,FALSE)
												PRINTLN("[GUN LOCKER] SET_VAULT_WEAPON_LOADOUT_SUB_MENU_NAMES:",GET_WEAPON_NAME(eWeaponType),"stat name",GET_WEAPON_HIDE_PACKED_STAT(eWeaponType),"- FALSE - show weapon")
												GIVE_PLAYER_ALL_EQUIPPED_WEAPON_ADDONS_FOR_WEAPON(eWeaponType)
												GIVE_WEAPON_EQUIPPED_TINT(eWeaponType)
											ENDIF
										ELSE
											REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(),eWeaponType)
											PRINTLN("[GUN LOCKER] SET_VAULT_WEAPON_LOADOUT_SUB_MENU_NAMES: Hide all option active",GET_WEAPON_NAME(eWeaponType))
										ENDIF	
										
									ELSE // hide
										
										// This global is used in PI menu for enable/disable custom weapon loadout
										g_bEnableCustomWeaponLoadout = TRUE
										
										ADD_MENU_ITEM_TEXT(iMenuIndex,"VAULT_HIDE",0,sWLoadoutCustomization.bIsMenuItemAvailable)	
										IF NOT IS_CURRENT_MENU_HIDE_ALL_ACTIVE(sWLoadoutCustomization.eWeaponCurrentMenu)
											SET_PACKED_STAT_BOOL(GET_WEAPON_HIDE_PACKED_STAT(eWeaponType), TRUE)
											PRINTLN("[GUN LOCKER]SET_PACKED_STAT_BOOL: stat name",GET_WEAPON_HIDE_PACKED_STAT(eWeaponType),"- TRUE - hide weapon")
										ENDIF	
										REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(),eWeaponType)
										PRINTLN("[GUN LOCKER]SET_VAULT_WEAPON_LOADOUT_SUB_MENU_NAMES:",GET_WEAPON_NAME(eWeaponType),"stat name",GET_WEAPON_HIDE_PACKED_STAT(eWeaponType),"- TRUE - hide weapon")
									ENDIF	
								ENDIF
								//PRINTLN("SET_VAULT_WEAPON_LOADOUT_SUB_MENU_NAMES: IS_MP_WEAPONTYPE_VALID - INVALID ")
							ENDIF
						ENDIF
					ELSE
					
						PRINTLN("[GUN LOCKER] SET_VAULT_WEAPON_LOADOUT_SUB_MENU_NAMES - eWeaponGroup != GET_WEAPONTYPE_GROUP  ",GET_WEAPON_NAME(eWeaponType))
					ENDIF
				ELSE
					PRINTLN("[GUN LOCKER] SET_VAULT_WEAPON_LOADOUT_SUB_MENU_NAMES - Weapon not purchased: ",GET_WEAPON_NAME(eWeaponType))
				ENDIF
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[GUN LOCKER] SET_VAULT_WEAPON_LOADOUT_SUB_MENU_NAMES - IS_MP_WEAPONTYPE_VALID = FALSE - Weapon not valid ", GET_WEAPON_NAME(eWeaponType))
		ENDIF
	ENDFOR
	iMenuIndex = 0
	IF NOT IS_BIT_SET(sWLoadoutCustomization.iVaultWeaponBS,ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu))	
		SET_BIT(sWLoadoutCustomization.iVaultWeaponBS,ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu))
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Gets the current menu's index to be used for main menu 
FUNC INT GET_MENU_INDEX(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)
	INT iWeaponClass
	REPEAT WEAPON_VAULT_TOTAL iWeaponClass
		IF 	sWLoadoutCustomization.iCurrentSubMenuType[iWeaponClass] = ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)
			RETURN iWeaponClass
		ENDIF
	ENDREPEAT
	PRINTLN("[GUN LOCKER] GET_MENU_INDEX - Invalid menu index")
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Manage hide all option for each sub menu
/// PARAMS:
///    eCurrentMenu - custom weapon loadout current menu 
///    bHideAll - hide all weapons in current sub menu
PROC MANAGE_WEAPON_LOADOUT_MENU_HIDE_ALL_OPTION(VAULT_WEAPON_MENU_ENUM eCurrentMenu,BOOL bHideAll = FALSE)
	SWITCH eCurrentMenu
		
		CASE VM_MELEE
			IF bHideAll
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_MELEE,TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_MELEE,FALSE)
			ENDIF
		BREAK
		CASE VM_PISTOL
			IF bHideAll
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_PISTOL,TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_PISTOL,FALSE)
			ENDIF
		BREAK
		CASE VM_SMG
			IF bHideAll
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_SMG,TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_SMG,FALSE)
			ENDIF
		BREAK
		CASE VM_RIFLE
			IF bHideAll
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_RIFLE,TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_RIFLE,FALSE)
			ENDIF
		BREAK
		CASE VM_SHOTGUN
			IF bHideAll
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_SHOTGUN,TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_SHOTGUN,FALSE)
			ENDIF
		BREAK
		CASE VM_SNIPER
			IF bHideAll
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_SNIPER,TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_SNIPER,FALSE)
			ENDIF
		BREAK
		CASE VM_HEAVY
			IF bHideAll
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_HEAVY,TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_HEAVY,FALSE)
			ENDIF
		BREAK
		CASE VM_THROWN
			IF bHideAll
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_THROWN,TRUE)
			ELSE
				SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_THROWN,FALSE)
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

// Check if all the purchased weapons are hidden from office weapon gun vault
FUNC BOOL IS_HIDE_ALL_WEAPON_OPTION_ACTIVE(WEAPON_TYPE eWeaponType)
	IF IS_MP_WEAPONTYPE_VALID(eWeaponType)
			IF GET_WEAPONTYPE_GROUP(eWeaponType) != WEAPONGROUP_INVALID
				IF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_MELEE
				OR eWeaponType = WEAPONTYPE_DLC_KNUCKLE
					IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_MELEE)
						RETURN TRUE
					ELSE 
						RETURN FALSE
					ENDIF
				ELIF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_PISTOL
					IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_PISTOL)
						RETURN TRUE
					ELSE 
						RETURN FALSE
					ENDIF	
				ELIF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_RIFLE
					IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_RIFLE) 
						RETURN TRUE
					ELSE 
						RETURN FALSE
					ENDIF	
					
				ELIF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_SHOTGUN
				OR eWeaponType = WEAPONTYPE_DLC_MUSKET
					IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_SHOTGUN)  
						RETURN TRUE
					ELSE 
						RETURN FALSE	
					ENDIF	
				ELIF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_SNIPER
				AND eWeaponType != WEAPONTYPE_DLC_MUSKET
					IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_SNIPER) 
						RETURN TRUE
					ELSE 
						RETURN FALSE	
					ENDIF	
				ELIF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_HEAVY
					IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_HEAVY) 
						RETURN TRUE
					ELSE 
						RETURN FALSE	
					ENDIF			
				ELIF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_THROWN
				OR eWeaponType = WEAPONTYPE_PETROLCAN
				OR eWeaponType = WEAPONTYPE_DLC_PROXMINE
					IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_THROWN)
						RETURN TRUE
					ELSE 
						RETURN FALSE	
					ENDIF	
				ELIF GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_SMG
				OR GET_WEAPONTYPE_GROUP(eWeaponType) = WEAPONGROUP_MG
					IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CUSTOM_HIDE_ALL_SMG)
						RETURN TRUE
					ELSE 
						RETURN FALSE	
					ENDIF	
				ENDIF
			ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    This will only work for this script 
///    Returns true if player owns any weapon
FUNC BOOL HAS_PLAYER_OWN_ANY_WEAPON(INT iBS)
	
	INT iWeaponClass
	REPEAT WEAPON_VAULT_TOTAL iWeaponClass
		IF IS_BIT_SET(iBS, iWeaponClass)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC 

FUNC STRING GET_MENU_GUN_LOCKER_BANNER_TEXTURE()
	IF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
		RETURN "SHOP_BANNER_CLINTON_PARTNER"
	ENDIF
	RETURN ""
ENDFUNC

PROC ADD_MENU_BANNER()
	STRING sBanner =  GET_MENU_GUN_LOCKER_BANNER_TEXTURE()
	IF NOT IS_STRING_NULL_OR_EMPTY(sBanner)
		SET_MENU_USES_HEADER_GRAPHIC(TRUE, sBanner, sBanner)
	ENDIF
ENDPROC

FUNC BOOL REQUEST_AND_LOAD_MENU_BANNER_TEXTURE()
	STRING sBanner =  GET_MENU_GUN_LOCKER_BANNER_TEXTURE()
	IF IS_STRING_NULL_OR_EMPTY(sBanner)
		PRINTLN("REQUEST_AND_LOAD_MENU_BANNER_TEXTURE sBanner empty")
		RETURN TRUE
	ENDIF
	
	REQUEST_STREAMED_TEXTURE_DICT(sBanner)
	RETURN HAS_STREAMED_TEXTURE_DICT_LOADED(sBanner)
ENDFUNC

/// PURPOSE:
///    Build vault weapon main menu
PROC BUILD_VAULT_WEAPON_MAIN_MENU(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)
	
	CLEAR_MENU_DATA()
	ADD_MENU_BANNER()
	SET_MENU_TITLE("VAULT_WMENUT_1")
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	
	IF HAS_PLAYER_OWN_ANY_WEAPON(sWLoadoutCustomization.iAvailableWeaponBS)
		INT iWeaponClass, iMenuIndex
		REPEAT WEAPON_VAULT_TOTAL iWeaponClass
			IF IS_BIT_SET(sWLoadoutCustomization.iAvailableWeaponBS,iWeaponClass)
				ADD_MENU_ITEM_TEXT(iMenuIndex, GET_VAULT_WEAPON_CLASS_NAME_FOR_MENU(INT_TO_ENUM(VAULT_WEAPON_MENU_ENUM,iWeaponClass )),1)
				ADD_MENU_ITEM_TEXT_COMPONENT_STRING(GET_VAULT_WEAPON_CLASS_NAME_FOR_MENU(INT_TO_ENUM(VAULT_WEAPON_MENU_ENUM,iWeaponClass )))
				sWLoadoutCustomization.iCurrentSubMenuType[iMenuIndex] = iWeaponClass
				iMenuIndex ++
			ENDIF
		ENDREPEAT
	ELSE
		ADD_MENU_ITEM_TEXT(0, "DFLT_MNU_OPT")
	ENDIF
	
	SET_CURRENT_MENU_ITEM(sWLoadoutCustomization.iWeaponMenuCurrentItem)
	
	SET_TOP_MENU_ITEM(sWLoadoutCustomization.iWeaponMenuTopItem)
	
	sWLoadoutCustomization.bMenuInitialised = TRUE
	
	IF HAS_PLAYER_OWN_ANY_WEAPON(sWLoadoutCustomization.iAvailableWeaponBS)
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
	ELSE
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
	ENDIF
ENDPROC

/// PURPOSE:
///    Update the menu item togglable option (are we showing arrows for menu item?)
PROC UPDATE_SUB_MENU_ITEM_TOGGLE(VAULT_WEAPON_LOADOUT_CUSTOMIZATION sWLoadoutCustomization)
	IF 	sWLoadoutCustomization.iWeaponMenuCurrentItem = 0
		SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
	ELSE
		IF sWLoadoutCustomization.bIsMenuItemAvailable
		 	SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
		ELSE
		  	SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)
		ENDIF
	ENDIF
ENDPROC

//// PURPOSE:
 ///    Build vault weapon sub menu
PROC BUILD_VAULT_WEAPON_SUB_MENU(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)

	CLEAR_MENU_DATA()
	ADD_MENU_BANNER()
	
	STRING sTitle
	sTitle = GET_VAULT_WEAPON_CLASS_NAME_FOR_MENU(sWLoadoutCustomization.eWeaponCurrentMenu, TRUE)
	STRING sFirstItem
	sFirstItem = GET_VAULT_ALL_WEAPON_NAME_FOR_MENU(sWLoadoutCustomization.eWeaponCurrentMenu)
	
	SET_MENU_TITLE(sTitle)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	ADD_MENU_ITEM_TEXT(0,sFirstItem)	
	
	// Set Hide all option when we open the menu
	IF NOT IS_BIT_SET(sWLoadoutCustomization.iHideAllWeaponBS,ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu))
		IF IS_CURRENT_MENU_HIDE_ALL_ACTIVE(sWLoadoutCustomization.eWeaponCurrentMenu)
			SET_BIT(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],0) 	// Hide
		ELSE
			CLEAR_BIT(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],0)	// Show
		ENDIF
		SET_BIT(sWLoadoutCustomization.iHideAllWeaponBS,ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu))
	ENDIF	
	
	// Show/Hide ALL
	IF IS_BIT_SET(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],0)
		ADD_MENU_ITEM_TEXT(0,"VAULT_HIDE_ALL")
		MANAGE_WEAPON_LOADOUT_MENU_HIDE_ALL_OPTION(sWLoadoutCustomization.eWeaponCurrentMenu,TRUE)
		g_bEnableCustomWeaponLoadout = TRUE
		sWLoadoutCustomization.bIsMenuItemAvailable = FALSE
	ELSE
		ADD_MENU_ITEM_TEXT(0,"VAULT_SHOW_ALL")
		MANAGE_WEAPON_LOADOUT_MENU_HIDE_ALL_OPTION(sWLoadoutCustomization.eWeaponCurrentMenu,FALSE)
		sWLoadoutCustomization.bIsMenuItemAvailable = TRUE 
	ENDIF
	
	SET_VAULT_WEAPON_LOADOUT_SUB_MENU_NAMES(sWLoadoutCustomization)
	
	SET_TOP_MENU_ITEM(sWLoadoutCustomization.iWeaponMenuTopItem)
	SET_CURRENT_MENU_ITEM(sWLoadoutCustomization.iWeaponMenuCurrentItem)
	sWLoadoutCustomization.bMenuInitialised = TRUE

	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
	
ENDPROC

/// PURPOSE:
///    Update vault main menu 
PROC UPDATE_VW_MAIN_MENU(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)

	// Navigate menu
	IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP) AND ALLOW_ANALOGUE_MOVEMENT(sWLoadoutCustomization.scrollDelay,sWLoadoutCustomization.sWeaponMenuInput.iMoveUp) 
	OR sWLoadoutCustomization.sWeaponMenuInput.bPrevious AND ALLOW_ANALOGUE_MOVEMENT(sWLoadoutCustomization.scrollDelay,sWLoadoutCustomization.sWeaponMenuInput.iMoveUp))
	AND (HAS_PLAYER_OWN_ANY_WEAPON(sWLoadoutCustomization.iAvailableWeaponBS))
		IF sWLoadoutCustomization.sWeaponMenuInput.iMoveUp > 0
			sWLoadoutCustomization.iWeaponMenuCurrentItem--
			IF sWLoadoutCustomization.iWeaponMenuCurrentItem < 0
				sWLoadoutCustomization.iWeaponMenuCurrentItem = sWLoadoutCustomization.iNumAvailableWeaponGroup - 1
			ENDIF
			SET_CURRENT_MENU_ITEM(sWLoadoutCustomization.iWeaponMenuCurrentItem)	
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		ENDIF	
	ELIF (IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN) AND ALLOW_ANALOGUE_MOVEMENT(sWLoadoutCustomization.scrollDelay,sWLoadoutCustomization.sWeaponMenuInput.iMoveUp)
	OR sWLoadoutCustomization.sWeaponMenuInput.bNext AND ALLOW_ANALOGUE_MOVEMENT(sWLoadoutCustomization.scrollDelay,sWLoadoutCustomization.sWeaponMenuInput.iMoveUp))
	AND (HAS_PLAYER_OWN_ANY_WEAPON(sWLoadoutCustomization.iAvailableWeaponBS))
		IF sWLoadoutCustomization.sWeaponMenuInput.iMoveUp < 0
			sWLoadoutCustomization.iWeaponMenuCurrentItem++
			IF sWLoadoutCustomization.iWeaponMenuCurrentItem > sWLoadoutCustomization.iNumAvailableWeaponGroup - 1
				sWLoadoutCustomization.iWeaponMenuCurrentItem = 0
			ENDIF
			SET_CURRENT_MENU_ITEM(sWLoadoutCustomization.iWeaponMenuCurrentItem)
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		ENDIF
	// Exit menu
	ELIF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL))
	OR sWLoadoutCustomization.sWeaponMenuInput.bBack 
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_CLEANUP
		PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		sWLoadoutCustomization.bMenuInitialised = FALSE
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, FALSE)
		SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromExplosions, FALSE)
		CLEAR_PED_TASKS(PLAYER_PED_ID())
	// Select
	ELIF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
	OR sWLoadoutCustomization.sWeaponMenuInput.bAccept)
	AND (HAS_PLAYER_OWN_ANY_WEAPON(sWLoadoutCustomization.iAvailableWeaponBS))
		PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
		IF sWLoadoutCustomization.iWeaponMenuCurrentItem >= 0 
		AND sWLoadoutCustomization.iWeaponMenuCurrentItem < WEAPON_VAULT_TOTAL 
			sWLoadoutCustomization.eWeaponCurrentMenu = INT_TO_ENUM(VAULT_WEAPON_MENU_ENUM,sWLoadoutCustomization.iCurrentSubMenuType[sWLoadoutCustomization.iWeaponMenuCurrentItem])
		ENDIF
		sWLoadoutCustomization.bReBuildMenu = TRUE
		sWLoadoutCustomization.iWeaponMenuTopItem = GET_TOP_MENU_ITEM()
		sWLoadoutCustomization.iWeaponMenuCurrentItem = 0
		sWLoadoutCustomization.bMenuInitialised = FALSE
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Update vault sub menu 
PROC UPDATE_VW_SUB_MENU(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)
	UPDATE_SUB_MENU_ITEM_TOGGLE(sWLoadoutCustomization)
	
	// Back to main menu
	IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL))
	OR sWLoadoutCustomization.sWeaponMenuInput.bBack
		sWLoadoutCustomization.iWeaponMenuCurrentItem = GET_MENU_INDEX(sWLoadoutCustomization)
		sWLoadoutCustomization.eWeaponCurrentMenu = VM_MAIN_MENU
		sWLoadoutCustomization.bReBuildMenu = TRUE
		PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		sWLoadoutCustomization.bMenuInitialised = FALSE
		
		sWLoadoutCustomization.iWeaponMenuTopItem = GET_TOP_MENU_ITEM()
	// Navigate menu
	ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP) AND ALLOW_ANALOGUE_MOVEMENT(sWLoadoutCustomization.scrollDelay,sWLoadoutCustomization.sWeaponMenuInput.iMoveUp) 
	OR sWLoadoutCustomization.sWeaponMenuInput.bPrevious AND ALLOW_ANALOGUE_MOVEMENT(sWLoadoutCustomization.scrollDelay,sWLoadoutCustomization.sWeaponMenuInput.iMoveUp)
		IF sWLoadoutCustomization.sWeaponMenuInput.iMoveUp > 0
			sWLoadoutCustomization.iWeaponMenuCurrentItem--
			IF sWLoadoutCustomization.iWeaponMenuCurrentItem < 0
				sWLoadoutCustomization.iWeaponMenuCurrentItem = GET_NUM_OWNED_WEAPON_TYPE(sWLoadoutCustomization.eWeaponCurrentMenu)
			ENDIF
			SET_CURRENT_MENU_ITEM(sWLoadoutCustomization.iWeaponMenuCurrentItem)
			UPDATE_SUB_MENU_ITEM_TOGGLE(sWLoadoutCustomization)
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		ENDIF	
	ELIF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN) AND ALLOW_ANALOGUE_MOVEMENT(sWLoadoutCustomization.scrollDelay,sWLoadoutCustomization.sWeaponMenuInput.iMoveUp)
	OR sWLoadoutCustomization.sWeaponMenuInput.bNext AND ALLOW_ANALOGUE_MOVEMENT(sWLoadoutCustomization.scrollDelay,sWLoadoutCustomization.sWeaponMenuInput.iMoveUp)
		IF sWLoadoutCustomization.sWeaponMenuInput.iMoveUp < 0
			sWLoadoutCustomization.iWeaponMenuCurrentItem++
			IF sWLoadoutCustomization.iWeaponMenuCurrentItem > GET_NUM_OWNED_WEAPON_TYPE(sWLoadoutCustomization.eWeaponCurrentMenu)
				sWLoadoutCustomization.iWeaponMenuCurrentItem = 0
			ENDIF
			SET_CURRENT_MENU_ITEM(sWLoadoutCustomization.iWeaponMenuCurrentItem)
			UPDATE_SUB_MENU_ITEM_TOGGLE(sWLoadoutCustomization)
			PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		ENDIF
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT) AND ALLOW_ANALOGUE_MOVEMENT(sWLoadoutCustomization.scrollDelay,sWLoadoutCustomization.sWeaponMenuInput.iMoveUp,FALSE)
	OR sWLoadoutCustomization.sWeaponMenuInput.iCursorValue = -1 
		IF !IS_CURRENT_MENU_HIDE_ALL_ACTIVE(sWLoadoutCustomization.eWeaponCurrentMenu)
		OR sWLoadoutCustomization.iWeaponMenuCurrentItem = 0
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT) AND ALLOW_ANALOGUE_MOVEMENT(sWLoadoutCustomization.scrollDelay,sWLoadoutCustomization.sWeaponMenuInput.iMoveUp,FALSE)
				IF sWLoadoutCustomization.sWeaponMenuInput.iMoveUp > 0
					IF IS_BIT_SET(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],sWLoadoutCustomization.iWeaponMenuCurrentItem)
						CLEAR_BIT(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],sWLoadoutCustomization.iWeaponMenuCurrentItem)
					ELSE
						SET_BIT(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],sWLoadoutCustomization.iWeaponMenuCurrentItem)
					ENDIF
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					SET_CURRENT_MENU_ITEM(sWLoadoutCustomization.iWeaponMenuCurrentItem)
					UPDATE_SUB_MENU_ITEM_TOGGLE(sWLoadoutCustomization)
					IF sWLoadoutCustomization.iWeaponMenuCurrentItem != 0 // set all weapons to custom loadout
						CLEAR_BIT(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],0)
					ENDIF	
					sWLoadoutCustomization.bReBuildMenu = TRUE
					sWLoadoutCustomization.bMenuInitialised = FALSE
				ENDIF
			ELSE
				IF IS_BIT_SET(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],sWLoadoutCustomization.iWeaponMenuCurrentItem)
					CLEAR_BIT(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],sWLoadoutCustomization.iWeaponMenuCurrentItem)
				ELSE
					SET_BIT(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],sWLoadoutCustomization.iWeaponMenuCurrentItem)
				ENDIF
				// mouse arrow click
				PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				SET_CURRENT_MENU_ITEM(sWLoadoutCustomization.iWeaponMenuCurrentItem)
				UPDATE_SUB_MENU_ITEM_TOGGLE(sWLoadoutCustomization)
				IF sWLoadoutCustomization.iWeaponMenuCurrentItem != 0 // set all weapons to custom loadout
					CLEAR_BIT(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],0)
				ENDIF	
				sWLoadoutCustomization.bReBuildMenu = TRUE
				sWLoadoutCustomization.bMenuInitialised = FALSE
			ENDIF
			sWLoadoutCustomization.iWeaponMenuTopItem = GET_TOP_MENU_ITEM()
		ENDIF	
	ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT) AND ALLOW_ANALOGUE_MOVEMENT(sWLoadoutCustomization.scrollDelay,sWLoadoutCustomization.sWeaponMenuInput.iMoveUp,FALSE)
	OR sWLoadoutCustomization.sWeaponMenuInput.iCursorValue = 1
		IF !IS_CURRENT_MENU_HIDE_ALL_ACTIVE(sWLoadoutCustomization.eWeaponCurrentMenu)
		OR sWLoadoutCustomization.iWeaponMenuCurrentItem = 0
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT) AND ALLOW_ANALOGUE_MOVEMENT(sWLoadoutCustomization.scrollDelay,sWLoadoutCustomization.sWeaponMenuInput.iMoveUp,FALSE)
				IF sWLoadoutCustomization.sWeaponMenuInput.iMoveUp < 0
					IF IS_BIT_SET(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],sWLoadoutCustomization.iWeaponMenuCurrentItem)
						CLEAR_BIT(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],sWLoadoutCustomization.iWeaponMenuCurrentItem)
					ELSE
						SET_BIT(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],sWLoadoutCustomization.iWeaponMenuCurrentItem)
					ENDIF
					PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					IF sWLoadoutCustomization.iWeaponMenuCurrentItem != 0	// set all weapons to custom loadout
						CLEAR_BIT(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],0)
					ENDIF	
					SET_CURRENT_MENU_ITEM(sWLoadoutCustomization.iWeaponMenuCurrentItem)
					UPDATE_SUB_MENU_ITEM_TOGGLE(sWLoadoutCustomization)
					sWLoadoutCustomization.bReBuildMenu = TRUE
					sWLoadoutCustomization.bMenuInitialised = FALSE
				ENDIF
			ELSE
				IF IS_BIT_SET(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],sWLoadoutCustomization.iWeaponMenuCurrentItem)
					CLEAR_BIT(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],sWLoadoutCustomization.iWeaponMenuCurrentItem)
				ELSE
					SET_BIT(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],sWLoadoutCustomization.iWeaponMenuCurrentItem)
				ENDIF
				// Mouse arrow click
				PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
				IF sWLoadoutCustomization.iWeaponMenuCurrentItem != 0	// set all weapons to custom loadout
					CLEAR_BIT(sWLoadoutCustomization.iWeaponMenuTypeBS[ENUM_TO_INT(sWLoadoutCustomization.eWeaponCurrentMenu)],0)
				ENDIF	
				SET_CURRENT_MENU_ITEM(sWLoadoutCustomization.iWeaponMenuCurrentItem)
				UPDATE_SUB_MENU_ITEM_TOGGLE(sWLoadoutCustomization)
				sWLoadoutCustomization.bReBuildMenu = TRUE
				sWLoadoutCustomization.bMenuInitialised = FALSE
			ENDIF
			sWLoadoutCustomization.iWeaponMenuTopItem = GET_TOP_MENU_ITEM()
		ENDIF	
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Build vault weapon menu depending on which menu/submenu we are currently using
PROC BUILD_VAULT_WEAPON_CUSTOMIZATION_MENU(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)
	
	IF NOT sWLoadoutCustomization.bMenuInitialised
		SWITCH sWLoadoutCustomization.eWeaponCurrentMenu
			CASE VM_MAIN_MENU
				BUILD_VAULT_WEAPON_MAIN_MENU(sWLoadoutCustomization)
			BREAK
			CASE VM_MELEE
			CASE VM_PISTOL
			CASE VM_SMG
			CASE VM_RIFLE
			CASE VM_SHOTGUN
			CASE VM_SNIPER
			CASE VM_HEAVY
			CASE VM_THROWN
				BUILD_VAULT_WEAPON_SUB_MENU(sWLoadoutCustomization)
			BREAK
		ENDSWITCH
		sWLoadoutCustomization.bMenuInitialised = TRUE
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Update vault weapon menu depending on which menu/submenu we are using
PROC UPDATE_VAULT_WEAPON_CUSTOMIZATION_MENU(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)
	
	IF !IS_PAUSE_MENU_ACTIVE()
		SWITCH sWLoadoutCustomization.eWeaponCurrentMenu
			CASE VM_MAIN_MENU
				UPDATE_VW_MAIN_MENU(sWLoadoutCustomization)
			BREAK
			CASE VM_MELEE
			CASE VM_PISTOL
			CASE VM_SMG
			CASE VM_RIFLE
			CASE VM_SHOTGUN
			CASE VM_SNIPER
			CASE VM_HEAVY
			CASE VM_THROWN
				UPDATE_VW_SUB_MENU(sWLoadoutCustomization)
			BREAK
		ENDSWITCH
	ENDIF	
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
	
ENDPROC

/// PURPOSE:
///    Display menu description for each menu item in vault weapon loadout custimzation menu
PROC HANDLE_WEAPON_LOADOUT_MENU_DESCRIPTION(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)
	SWITCH sWLoadoutCustomization.eWeaponCurrentMenu
		CASE VM_MAIN_MENU
			IF sWLoadoutCustomization.iWeaponMenuCurrentItem >= 0 
			AND sWLoadoutCustomization.iWeaponMenuCurrentItem < WEAPON_VAULT_TOTAL
			AND HAS_PLAYER_OWN_ANY_WEAPON(sWLoadoutCustomization.iAvailableWeaponBS)
				SWITCH INT_TO_ENUM(VAULT_WEAPON_MENU_ENUM,sWLoadoutCustomization.iCurrentSubMenuType[sWLoadoutCustomization.iWeaponMenuCurrentItem])
					CASE VM_MELEE
						SET_CURRENT_MENU_ITEM_DESCRIPTION(GET_VAULT_WEAPON_MAIN_MENU_DESCRIPTION(VM_MELEE))
					BREAK
					CASE VM_PISTOL
						SET_CURRENT_MENU_ITEM_DESCRIPTION(GET_VAULT_WEAPON_MAIN_MENU_DESCRIPTION(VM_PISTOL))
					BREAK
					CASE VM_SMG
						SET_CURRENT_MENU_ITEM_DESCRIPTION(GET_VAULT_WEAPON_MAIN_MENU_DESCRIPTION(VM_SMG))
					BREAK
					CASE VM_RIFLE
						SET_CURRENT_MENU_ITEM_DESCRIPTION(GET_VAULT_WEAPON_MAIN_MENU_DESCRIPTION(VM_RIFLE))
					BREAK
					CASE VM_SHOTGUN
						SET_CURRENT_MENU_ITEM_DESCRIPTION(GET_VAULT_WEAPON_MAIN_MENU_DESCRIPTION(VM_SHOTGUN))
					BREAK
					CASE VM_SNIPER
						SET_CURRENT_MENU_ITEM_DESCRIPTION(GET_VAULT_WEAPON_MAIN_MENU_DESCRIPTION(VM_SNIPER))
					BREAK
					CASE VM_HEAVY
						SET_CURRENT_MENU_ITEM_DESCRIPTION(GET_VAULT_WEAPON_MAIN_MENU_DESCRIPTION(VM_HEAVY))
					BREAK
					CASE VM_THROWN
						SET_CURRENT_MENU_ITEM_DESCRIPTION(GET_VAULT_WEAPON_MAIN_MENU_DESCRIPTION(VM_THROWN))
					BREAK
				ENDSWITCH
			ELIF NOT HAS_PLAYER_OWN_ANY_WEAPON(sWLoadoutCustomization.iAvailableWeaponBS)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("VAULT_MENU_D_9")
			ENDIF	
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Manage arrow click for vault weapon menu
PROC MANAGE_VAULT_WEAPON_MENU_ARROW_CLICK(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)
	
	sWLoadoutCustomization.sWeaponMenuInput.iCursorValue = 0
	
	IF sWLoadoutCustomization.eWeaponCurrentMenu != VM_MAIN_MENU
		// Cursor is inside the menu
		IF g_iMenuCursorItem > MENU_CURSOR_NO_ITEM	
			// Clicking on an item
			IF sWLoadoutCustomization.sWeaponMenuInput.bAccept 
				// Clicking on currently selected item
				IF g_iMenuCursorItem = sWLoadoutCustomization.iWeaponMenuCurrentItem
					// Item is a < item > style 
					IF g_iMenuCursorItem = sWLoadoutCustomization.iWeaponMenuCurrentItem
						sWLoadoutCustomization.sWeaponMenuInput.iCursorValue = GET_CURSOR_MENU_ITEM_VALUE_CHANGE()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
ENDPROC


/// PURPOSE:
///    Main PROC to process vault weapon menu
/// PARAMS:
///    sWLoadoutCustomization - 
PROC PROCESS_VAULT_WEAPON_LOADOUT_MENU(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization)
	
	IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON|NSPC_FREEZE_POSITION)
		ENDIF
	ENDIF		

	SETUP_GUN_LOCKER_MENU_CONTROLS(sWLoadoutCustomization.sWeaponMenuInput,sWLoadoutCustomization.iWeaponMenuCurrentItem)
	MANAGE_VAULT_WEAPON_MENU_ARROW_CLICK(sWLoadoutCustomization)
	
	IF LOAD_MENU_ASSETS()
		IF sWLoadoutCustomization.bReBuildMenu
			BUILD_VAULT_WEAPON_CUSTOMIZATION_MENU(sWLoadoutCustomization)
			sWLoadoutCustomization.bReBuildMenu = FALSE
		ELSE
			SET_CURRENT_MENU_ITEM(sWLoadoutCustomization.iWeaponMenuCurrentItem)
		ENDIF
		
		UPDATE_VAULT_WEAPON_CUSTOMIZATION_MENU(sWLoadoutCustomization)
		HANDLE_WEAPON_LOADOUT_MENU_DESCRIPTION(sWLoadoutCustomization)
		
		DRAW_MENU()

		IF sWLoadoutCustomization.eCustomizationStage != VMC_STAGE_CUSTOMIZING
			CLEANUP_MENU_ASSETS()
		ENDIF
	ENDIF	
	
ENDPROC

FUNC BOOL IS_PLAYER_IN_BIKER_GUN_LOCKER_TRIGGER_AREA(INT iCurrentProperty)
	VECTOR vCoor1, vCoor2
	SWITCH iCurrentProperty
		CASE PROPERTY_CLUBHOUSE_1_BASE_A
		CASE PROPERTY_CLUBHOUSE_2_BASE_A
		CASE PROPERTY_CLUBHOUSE_3_BASE_A
		CASE PROPERTY_CLUBHOUSE_4_BASE_A
		CASE PROPERTY_CLUBHOUSE_5_BASE_A
		CASE PROPERTY_CLUBHOUSE_6_BASE_A
			vCoor1 = <<1122.707764,-3162.243652,-37.870518>>
			vCoor2 = <<1122.698120,-3161.280518,-35.620518>> 
		BREAK
		CASE PROPERTY_CLUBHOUSE_7_BASE_B
		CASE PROPERTY_CLUBHOUSE_8_BASE_B
		CASE PROPERTY_CLUBHOUSE_9_BASE_B
		CASE PROPERTY_CLUBHOUSE_10_BASE_B
		CASE PROPERTY_CLUBHOUSE_11_BASE_B
		CASE PROPERTY_CLUBHOUSE_12_BASE_B
			vCoor1 = <<1008.781982,-3171.890381,-39.907139>>
			vCoor2 = <<1007.858093,-3171.916992,-37.657139>> 
		BREAK
	ENDSWITCH 
	RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),vCoor1,vCoor2 ,1.0)
ENDFUNC

PROC CLEANUP_PROPERTY_GUN_LOCKER(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization, BOOL bForceCleanUp = FALSE)
	PRINTLN("NET_GUN_LOCKER - CLEANUP_GUN_LOCKER")
	
	RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
	
	sWLoadoutCustomization.iVaultWeaponContext 			= NEW_CONTEXT_INTENTION
	sWLoadoutCustomization.iWeaponMenuCurrentItem 		= 0
	sWLoadoutCustomization.iWeaponMenuTopItem 			= 0
	sWLoadoutCustomization.iNumAvailableWeaponGroup 	= 0
	sWLoadoutCustomization.eWeaponCurrentMenu 			= VM_MAIN_MENU
	sWLoadoutCustomization.bMenuInitialised 			= FALSE
	sWLoadoutCustomization.bReBuildMenu 				= FALSE
	
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS, VAULT_DOOR_ANIM_STAGE_FINISHED)
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS, VAULT_GUN_DOOR_ANIM_RUNNING)
	CLEAR_BIT(sWLoadoutCustomization.iLocalBS, BS_GET_NUM_AVAILABLE_WEAPON_GROUP)
	
	INT iNumSubMenu
	FOR iNumSubMenu = 0 TO WEAPON_VAULT_TOTAL - 1
		CLEAR_BIT(sWLoadoutCustomization.iVaultWeaponBS, iNumSubMenu)
		CLEAR_BIT(sWLoadoutCustomization.iAvailableWeaponBS, iNumSubMenu)
		CLEAR_BIT(sWLoadoutCustomization.iHideAllWeaponBS, iNumSubMenu)
	ENDFOR
	
	STRING sBanner =  GET_MENU_GUN_LOCKER_BANNER_TEXTURE()
	IF NOT IS_STRING_NULL_OR_EMPTY(sBanner)
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sBanner)
	ENDIF
	
	IF bForceCleanUp
		INT iNumWeaponModels
		FOR iNumWeaponModels = 0 TO MAX_NUMBER_WEAPON_MODELS -1
			IF DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[iNumWeaponModels])
				DELETE_OBJECT(sWLoadoutCustomization.weapons[iNumWeaponModels])
			ENDIF
			
			IF IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, iNumWeaponModels)
				CLEAR_BIT(sWLoadoutCustomization.iWeaponModelBS, iNumWeaponModels)
			ENDIF
		ENDFOR
		
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS, VAULT_GUN_DOOR_ANIM_INITIALISED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS, VAULT_GUN_DOOR_NET_RESERVED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS, VAULT_DOOR_ANIM_STAGE_FINISHED)
		CLEAR_BIT(sWLoadoutCustomization.iLocalBS, OFFICE_GUN_VAULT_DOOR_CREATED)
		
		sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_INIT
	ELSE
		sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_INIT
	ENDIF
ENDPROC

PROC RESET_GUN_LOCKER_PROP_WEAPON_COMPONENT_DATA(VAULT_WEAPON_PROPS_STRUCT& PropDetails)
	
	INT iWeaponComp
	REPEAT MAX_NUMBER_WEAPON_COMP_MODELS iWeaponComp
		PropDetails.eWeaponCompModel[iWeaponComp] = DUMMY_MODEL_FOR_SCRIPT
	ENDREPEAT
	
ENDPROC

PROC SET_GUN_LOCKER_LOCATE_DATA(VAULT_WEAPON_LOCATE_STRUCT& LocateDetails, VECTOR vLocateAreaOne, VECTOR vLocateAreaTwo, FLOAT fLocateWidth, FLOAT fLocateHeading, FLOAT fLocateLeeway)
	LocateDetails.vAreaOne							= vLocateAreaOne
	LocateDetails.vAreaTwo							= vLocateAreaTwo
	LocateDetails.fWidth							= fLocateWidth
	LocateDetails.fHeading							= fLocateHeading
	LocateDetails.fLeeway							= fLocateLeeway
ENDPROC

PROC SET_GUN_LOCKER_PROP_DATA(VAULT_WEAPON_PROPS_STRUCT& PropDetails, MODEL_NAMES eWeaponModel, VECTOR vCoords, VECTOR vRotation, INT iOwnWeaponBit, INT iCreatedPropBit, BOOL bCreateAsWeaponObject = FALSE, WEAPON_TYPE eWeaponType = WEAPONTYPE_INVALID, BOOL bUseBitsetTwo = FALSE)
	PropDetails.eWeaponModel						= eWeaponModel
	PropDetails.vCoords								= vCoords
	PropDetails.vRotation							= vRotation
	PropDetails.iOwnWeaponBit						= iOwnWeaponBit
	PropDetails.iCreatedPropBit						= iCreatedPropBit
	PropDetails.bCreateAsWeaponObject				= bCreateAsWeaponObject
	PropDetails.eWeaponType							= eWeaponType
	PropDetails.bUseBitsetTwo						= bUseBitsetTwo
ENDPROC

FUNC BOOL IS_GUN_LOCKER_WEAPON_MODEL_VALID(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization, INT iWeaponProp)
	
	BOOL bValidModel = FALSE
	IF IS_MODEL_VALID(sWLoadoutCustomization.PropDetails[iWeaponProp].eWeaponModel)
	AND sWLoadoutCustomization.PropDetails[iWeaponProp].eWeaponModel != DUMMY_MODEL_FOR_SCRIPT
		bValidModel = TRUE
	ENDIF
	
	// Return true - No need to validate weapon type as not creating as a weapon object
	IF (bValidModel AND NOT sWLoadoutCustomization.PropDetails[iWeaponProp].bCreateAsWeaponObject)
		RETURN TRUE
	ENDIF
	
	BOOL bValidWeaponType = FALSE
	IF IS_WEAPON_VALID(sWLoadoutCustomization.PropDetails[iWeaponProp].eWeaponType)
	AND sWLoadoutCustomization.PropDetails[iWeaponProp].eWeaponType != WEAPONTYPE_INVALID
		bValidWeaponType = TRUE
	ENDIF
	
	RETURN (bValidModel AND bValidWeaponType) 
ENDFUNC

FUNC BOOL REQUEST_AND_LOAD_GUN_LOCKER_WEAPON_PROPS(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization, INT iWeaponProp)
	
	BOOL bWeaponPropLoaded = FALSE
	IF REQUEST_LOAD_MODEL(sWLoadoutCustomization.PropDetails[iWeaponProp].eWeaponModel)
		bWeaponPropLoaded = TRUE
	ENDIF
	
	// Return true - Only need base weapon here, don't need to request and load weapon components
	IF (bWeaponPropLoaded AND NOT sWLoadoutCustomization.PropDetails[iWeaponProp].bCreateAsWeaponObject)
		RETURN TRUE
	ENDIF
	
	INT iWeaponComps			= 0
	INT iWeaponCompsLoaded		= 0
	INT iMaxValidWeaponComps	= 0
	
	REPEAT MAX_NUMBER_WEAPON_COMP_MODELS iWeaponComps
		IF IS_MODEL_VALID(sWLoadoutCustomization.PropDetails[iWeaponProp].eWeaponCompModel[iWeaponComps])
			IF REQUEST_LOAD_MODEL(sWLoadoutCustomization.PropDetails[iWeaponProp].eWeaponCompModel[iWeaponComps])
				iWeaponCompsLoaded++
			ENDIF
			iMaxValidWeaponComps++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN (bWeaponPropLoaded AND iWeaponCompsLoaded = iMaxValidWeaponComps)
ENDFUNC

PROC SET_GUN_LOCKER_WEAPON_MODELS_AS_NO_LONGER_NEEDED(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization, INT iWeaponProp)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(sWLoadoutCustomization.PropDetails[iWeaponProp].eWeaponModel)
	IF NOT sWLoadoutCustomization.PropDetails[iWeaponProp].bCreateAsWeaponObject
		EXIT
	ENDIF
	
	INT iWeaponComps
	REPEAT MAX_NUMBER_WEAPON_COMP_MODELS iWeaponComps
		IF IS_MODEL_VALID(sWLoadoutCustomization.PropDetails[iWeaponProp].eWeaponCompModel[iWeaponComps])
			SET_MODEL_AS_NO_LONGER_NEEDED(sWLoadoutCustomization.PropDetails[iWeaponProp].eWeaponCompModel[iWeaponComps])
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL SHOULD_CREATE_PROPERTY_GUN_LOCKER_WEAPON_PROPS(SIMPLE_INTERIORS eSimpleInteriorID)
	BOOL bCreateWeaponProps = FALSE
	
	SWITCH eSimpleInteriorID
		#IF FEATURE_TUNER
		CASE SIMPLE_INTERIOR_AUTO_SHOP_LA_MESA
		CASE SIMPLE_INTERIOR_AUTO_SHOP_STRAWBERRY
		CASE SIMPLE_INTERIOR_AUTO_SHOP_BURTON
		CASE SIMPLE_INTERIOR_AUTO_SHOP_RANCHO
		CASE SIMPLE_INTERIOR_AUTO_SHOP_MISSION_ROW
			// Add personal quarters check when set up
			bCreateWeaponProps = TRUE
		BREAK
		#ENDIF
		DEFAULT
			bCreateWeaponProps = FALSE
		BREAK
	ENDSWITCH
	
	RETURN bCreateWeaponProps
ENDFUNC

FUNC BOOL DOES_PLAYER_OWN_GUN_LOCKER_WEAPON_TYPE(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization, PLAYER_INDEX ownerID, INT iWeaponProp, BOOL bUseBitsetTwo)
	IF (bUseBitsetTwo)
		RETURN IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(ownerID)].propertyDetails.iBSTwo, sWLoadoutCustomization.PropDetails[iWeaponProp].iOwnWeaponBit)
	ELSE
		RETURN IS_BIT_SET(GlobalPlayerBD_FM[NATIVE_TO_INT(ownerID)].propertyDetails.iBS, sWLoadoutCustomization.PropDetails[iWeaponProp].iOwnWeaponBit)
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MANAGE_PROPERTY_GUN_LOCKER_WEAPONS(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization, SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX ownerID)
	
	IF (ownerID = INVALID_PLAYER_INDEX())
	OR NOT SHOULD_CREATE_PROPERTY_GUN_LOCKER_WEAPON_PROPS(eSimpleInteriorID)
		EXIT
	ENDIF
	
	INT iWeaponProp
	REPEAT MAX_NUMBER_WEAPON_MODELS iWeaponProp
		IF DOES_PLAYER_OWN_GUN_LOCKER_WEAPON_TYPE(sWLoadoutCustomization, ownerID, iWeaponProp, sWLoadoutCustomization.PropDetails[iWeaponProp].bUseBitsetTwo)
			IF NOT IS_BIT_SET(sWLoadoutCustomization.iWeaponModelBS, sWLoadoutCustomization.PropDetails[iWeaponProp].iCreatedPropBit)
				
				IF IS_GUN_LOCKER_WEAPON_MODEL_VALID(sWLoadoutCustomization, iWeaponProp)
					IF NOT DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[iWeaponProp])
						IF REQUEST_AND_LOAD_GUN_LOCKER_WEAPON_PROPS(sWLoadoutCustomization, iWeaponProp)
							
							IF (sWLoadoutCustomization.PropDetails[iWeaponProp].bCreateAsWeaponObject)
								sWLoadoutCustomization.weapons[iWeaponProp] = CREATE_WEAPON_OBJECT(sWLoadoutCustomization.PropDetails[iWeaponProp].eWeaponType, 0, sWLoadoutCustomization.PropDetails[iWeaponProp].vCoords, TRUE)
							ELSE
								sWLoadoutCustomization.weapons[iWeaponProp] = CREATE_OBJECT_NO_OFFSET(sWLoadoutCustomization.PropDetails[iWeaponProp].eWeaponModel, sWLoadoutCustomization.PropDetails[iWeaponProp].vCoords, FALSE, FALSE)
							ENDIF
							
							SET_ENTITY_COORDS_NO_OFFSET(sWLoadoutCustomization.weapons[iWeaponProp], sWLoadoutCustomization.PropDetails[iWeaponProp].vCoords)
							SET_ENTITY_ROTATION(sWLoadoutCustomization.weapons[iWeaponProp], sWLoadoutCustomization.PropDetails[iWeaponProp].vRotation)
							FREEZE_ENTITY_POSITION(sWLoadoutCustomization.weapons[iWeaponProp], TRUE)
							SET_ENTITY_COLLISION(sWLoadoutCustomization.weapons[iWeaponProp], TRUE)
							SET_ENTITY_INVINCIBLE(sWLoadoutCustomization.weapons[iWeaponProp], TRUE)
							SET_ENTITY_VISIBLE(sWLoadoutCustomization.weapons[iWeaponProp], TRUE)
							SET_GUN_LOCKER_WEAPON_MODELS_AS_NO_LONGER_NEEDED(sWLoadoutCustomization, iWeaponProp)
							SET_BIT(sWLoadoutCustomization.iWeaponModelBS, sWLoadoutCustomization.PropDetails[iWeaponProp].iCreatedPropBit)
							
						ENDIF
					ENDIF
				ELSE
					SET_BIT(sWLoadoutCustomization.iWeaponModelBS, sWLoadoutCustomization.PropDetails[iWeaponProp].iCreatedPropBit)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

FUNC BOOL CAN_PLAYER_USE_PROPERTY_GUN_LOCKER(SIMPLE_INTERIORS eSimpleInteriorID, BOOL bOwner = FALSE)
	BOOL bUseGunLocker = FALSE
	
	SWITCH eSimpleInteriorID
		#IF FEATURE_TUNER
		CASE SIMPLE_INTERIOR_AUTO_SHOP_LA_MESA
		CASE SIMPLE_INTERIOR_AUTO_SHOP_STRAWBERRY
		CASE SIMPLE_INTERIOR_AUTO_SHOP_BURTON
		CASE SIMPLE_INTERIOR_AUTO_SHOP_RANCHO
		CASE SIMPLE_INTERIOR_AUTO_SHOP_MISSION_ROW
			// Add personal quarters check when set up
			IF (bOwner)
				IF (g_iPlayerUsingOfficeSeat = -1)
					bUseGunLocker = TRUE
				ENDIF
			ELSE
				IF DOES_PLAYER_OWN_ANY_GUN_LOCKER()
				AND NOT NETWORK_IS_IN_MP_CUTSCENE()
				AND (g_iPlayerUsingOfficeSeat = -1)
					bUseGunLocker = TRUE
				ENDIF
			ENDIF
		BREAK
		#ENDIF
		DEFAULT
			bUseGunLocker = FALSE
		BREAK
	ENDSWITCH
	
	RETURN bUseGunLocker
ENDFUNC

FUNC BOOL CAN_PLAYER_START_PROPERTY_GUN_LOCKER(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX ownerID)
	
	IF (ownerID = INVALID_PLAYER_INDEX())
		RETURN FALSE
	ENDIF
	
	IF (ownerID = PLAYER_ID())
		RETURN CAN_PLAYER_USE_PROPERTY_GUN_LOCKER(eSimpleInteriorID, TRUE)
	ELSE
		RETURN CAN_PLAYER_USE_PROPERTY_GUN_LOCKER(eSimpleInteriorID)
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL IS_SAFE_TO_START_PROPERTY_GUN_LOCKER_MENUS(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX ownerID, BOOL bAllowHeist = FALSE)
	IF NOT CAN_PLAYER_START_PROPERTY_GUN_LOCKER(eSimpleInteriorID, ownerID)
		RETURN FALSE
	ENDIF
	
	IF NOT bAllowHeist
		IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
	OR IS_INTERACTION_MENU_OPEN()
	OR IS_BROWSER_OPEN()
	OR SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER()
	OR IS_SELECTOR_ONSCREEN()
	OR IS_CUSTOM_MENU_ON_SCREEN()
	OR IS_PAUSE_MENU_ACTIVE()
	OR MPGlobalsAmbience.bTriggerPropertyExitOnFoot
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
		RETURN FALSE
	ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_PROPERTY_GUN_LOCKER(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization, INT& iPropertyOptionsMenuInputCooldown, SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX ownerID, BOOL bPropertyRenovationActive = FALSE, BOOL bGunLockerPurchased = TRUE)
	
	IF (ownerID = INVALID_PLAYER_INDEX())
		EXIT
	ENDIF
	
	IF NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
	AND NOT bPropertyRenovationActive
	AND NOT IS_BROWSER_OPEN()
	AND NOT IS_PAUSE_MENU_ACTIVE_EX()
	AND bGunLockerPurchased
	
		SWITCH sWLoadoutCustomization.eCustomizationStage
			CASE VMC_STAGE_INIT
				CLEANUP_PROPERTY_GUN_LOCKER(sWLoadoutCustomization)
				sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_LOAD_ASSETS	
			BREAK
			CASE VMC_STAGE_LOAD_ASSETS
				IF REQUEST_AND_LOAD_MENU_BANNER_TEXTURE()
					sWLoadoutCustomization.eCustomizationStage = VWC_STAGE_WAITING_TO_TRIGGER
					PRINTLN("MAINTAIN_PROPERTY_GUN_LOCKER banner loaded move to VWC_STAGE_WAITING_TO_TRIGGER")
				ENDIF
			BREAK
			CASE VWC_STAGE_WAITING_TO_TRIGGER
				GET_NUM_AVAILABLE_WEAPON_GROUP(sWLoadoutCustomization)
				MANAGE_PROPERTY_GUN_LOCKER_WEAPONS(sWLoadoutCustomization, eSimpleInteriorID, ownerID)
				
				IF IS_SAFE_TO_START_PROPERTY_GUN_LOCKER_MENUS(eSimpleInteriorID, ownerID, TRUE)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sWLoadoutCustomization.LocateDetails.vAreaOne, sWLoadoutCustomization.LocateDetails.vAreaTwo, sWLoadoutCustomization.LocateDetails.fWidth)
					AND IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(PLAYER_PED_ID()), sWLoadoutCustomization.LocateDetails.fHeading, sWLoadoutCustomization.LocateDetails.fLeeway)
					AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
					AND DOES_ENTITY_EXIST(PLAYER_PED_ID())
					AND IS_PED_STILL(PLAYER_PED_ID())
						IF sWLoadoutCustomization.iVaultWeaponContext = NEW_CONTEXT_INTENTION
							IF NOT IS_HELP_MESSAGE_ON_SCREEN()
								REGISTER_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext, CP_HIGH_PRIORITY, "OF_VAULT_MENU")
							ENDIF
						ENDIF
						
						IF NOT IS_INTERACTION_MENU_OPEN()
							IF HAS_CONTEXT_BUTTON_TRIGGERED(sWLoadoutCustomization.iVaultWeaponContext)
								LOAD_MENU_ASSETS()
								
								BUILD_VAULT_WEAPON_MAIN_MENU(sWLoadoutCustomization)
								TASK_ACHIEVE_HEADING(PLAYER_PED_ID(), sWLoadoutCustomization.LocateDetails.fHeading, 0)
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, TRUE)
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromExplosions, TRUE)
								RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
								
								sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_CUSTOMIZING
							ENDIF
						ENDIF
					ELSE
						RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
					ENDIF
				ELSE
					IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(sWLoadoutCustomization.iVaultWeaponContext)
						RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
					ENDIF
				ENDIF
			BREAK
			CASE VMC_STAGE_CUSTOMIZING
				iPropertyOptionsMenuInputCooldown = GET_GAME_TIMER()
				PROCESS_VAULT_WEAPON_LOADOUT_MENU(sWLoadoutCustomization)
			BREAK
			CASE VMC_STAGE_CLEANUP
				CLEANUP_PROPERTY_GUN_LOCKER(sWLoadoutCustomization)
				START_NET_TIMER(sWLoadoutCustomization.sMenuTimer)
			BREAK
		ENDSWITCH
	ELSE
		IF (sWLoadoutCustomization.eCustomizationStage > VMC_STAGE_INIT)
			IF sWLoadoutCustomization.eCustomizationStage >= VMC_STAGE_CUSTOMIZING
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, FALSE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromExplosions, FALSE)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			CLEANUP_PROPERTY_GUN_LOCKER(sWLoadoutCustomization)
		
		ELIF NOT bGunLockerPurchased
			CLEANUP_PROPERTY_GUN_LOCKER(sWLoadoutCustomization, TRUE)
		ENDIF
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_GET_GUN_LOCKER_WEAPON_NAME(INT iWeapon)
	STRING sWeaponName = ""
	
	SWITCH iWeapon
		CASE 0	sWeaponName = "Pistol"				BREAK
		CASE 1	sWeaponName = "Special Carbine"		BREAK
		CASE 2	sWeaponName = "Assault Rifle"		BREAK
		CASE 3	sWeaponName = "Pump Shotgun"		BREAK
		CASE 4	sWeaponName = "Micro SMG"			BREAK
		CASE 5	sWeaponName = "Prox Mine"			BREAK
		CASE 6	sWeaponName = "Frag Genade"			BREAK
		CASE 7	sWeaponName = "Combat MG"			BREAK
		CASE 8	sWeaponName = "Marksman Rifle"		BREAK
	ENDSWITCH
	
	RETURN sWeaponName
ENDFUNC

PROC INITIALISE_GUN_LOCKER_WIDGETS(VAULT_WEAPON_DEBUG_DATA &DebugData, VAULT_WEAPON_LOADOUT_CUSTOMIZATION &sGunLocker)
	
	INT iWeapon
	REPEAT MAX_NUMBER_WEAPON_MODELS iWeapon
		DebugData.vWeaponPosition[iWeapon] = sGunLocker.PropDetails[iWeapon].vCoords
		DebugData.vWeaponRotation[iWeapon] = sGunLocker.PropDetails[iWeapon].vRotation
	ENDREPEAT
	
ENDPROC

PROC CREATE_GUN_LOCKER_WIDGETS(VAULT_WEAPON_DEBUG_DATA &DebugData)
	
	START_WIDGET_GROUP("Gun Locker")
		
		INT iWeapon
		REPEAT MAX_NUMBER_WEAPON_MODELS iWeapon
			START_WIDGET_GROUP(DEBUG_GET_GUN_LOCKER_WEAPON_NAME(iWeapon))
				ADD_WIDGET_VECTOR_SLIDER("Coords", DebugData.vWeaponPosition[iWeapon], -999999.9, 999999.9, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Rotation", DebugData.vWeaponRotation[iWeapon], -360.9, 360.9, 0.01)
			STOP_WIDGET_GROUP()
		ENDREPEAT
		
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_GUN_LOCKER_WIDGETS(VAULT_WEAPON_DEBUG_DATA &DebugData, VAULT_WEAPON_LOADOUT_CUSTOMIZATION &GunLockerData)
	
	INT iWeapon
	REPEAT MAX_NUMBER_WEAPON_MODELS iWeapon
		IF IS_ENTITY_ALIVE(GunLockerData.weapons[iWeapon])
			SET_ENTITY_COORDS_NO_OFFSET(GunLockerData.weapons[iWeapon], DebugData.vWeaponPosition[iWeapon])
			SET_ENTITY_ROTATION(GunLockerData.weapons[iWeapon], DebugData.vWeaponRotation[iWeapon])
		ENDIF
	ENDREPEAT
	
ENDPROC
#ENDIF
