
// ====================================================================================
// ====================================================================================
//
// Name:        net_spectator_cam_common.sch
// Description: Common spectator cam functions for use in .sc files.
// Written By:  William Kennedy
//
// ====================================================================================
// ====================================================================================

USING "globals.sch"
//USING "net_hud_activating.sch"
USING "commands_decorator.sch"
USING "net_hud_activating.sch"
USING "net_team_info.sch"


	
	
PROC RESET_structForceSpectatorCam_SPEC()
	structForceSpectatorCam sForceSpectatorCamTemp
	g_BossSpecData.specCamData.sForceSpectatorCam = sForceSpectatorCamTemp
ENDPROC
PROC RESET_SPECTATOR_CAM_DATA_STRUCT()

	//g_BossSpecData.specCamData
	RESET_structForceSpectatorCam_SPEC()
	g_BossSpecData.specCamData.iBitSet = 0
	g_BossSpecData.specCamData.iActivateBitset = 0
	g_BossSpecData.specCamData.iDeactivateBitset = 0

	g_BossSpecData.specCamData.fadeSwitchStage = eSPECFADESWITCHSTAGE_FADE_OUT
	g_BossSpecData.specCamData.bFirstTransitionComplete = FALSE
	g_BossSpecData.specCamData.iSwitchFadeTime = 0
	g_BossSpecData.specCamData.bSkipFade = FALSE
	
	INT iLoop
	REPEAT NUMBER_OF_SPEC_HUD_TARGET_LISTS iLoop
		g_BossSpecData.specCamData.camViewMode[iLoop] = CAM_VIEW_MODE_THIRD_PERSON_NEAR
	ENDREPEAT
	
	g_BossSpecData.specCamData.vHideReturn = <<0.0, 0.0, 0.0>>
	
	TIME_DATATYPE timeSwitchSafetyTemp
	g_BossSpecData.specCamData.timeSwitchSafety = timeSwitchSafetyTemp
	g_BossSpecData.specCamData.iTimer = timeSwitchSafetyTemp
	g_BossSpecData.specCamData.iLoadSceneFailSafeTimer = timeSwitchSafetyTemp
	g_BossSpecData.specCamData.iWaitForNoStreamVolFailSafeTimer = timeSwitchSafetyTemp
	g_BossSpecData.specCamData.timeSkyswoopCleanupSafety = timeSwitchSafetyTemp
	g_BossSpecData.specCamData.iLoopCheckFrameCount = 0				//used to stagger list filling loop
	
	g_BossSpecData.specCamData.vLoadScene = <<0.0, 0.0, 0.0>>
	
	g_BossSpecData.specCamData.bStartedUpdateLoadScene = FALSE
	g_BossSpecData.specCamData.bDoTransition = FALSE
	
	g_BossSpecData.specCamData.eMode = SPEC_MODE_PLAYERS_RESPAWN_EVENT
	g_BossSpecData.specCamData.eState = eSPECCAMSTATE_OFF
	
	PED_INDEX pedIDTemp
	g_BossSpecData.specCamData.pedIDFrom = pedIDTemp
	g_BossSpecData.specCamData.pedIDTo = pedIDTemp
	
	CAMERA_INDEX camTemp
	
	g_BossSpecData.specCamData.camCinematic = camTemp
	g_BossSpecData.specCamData.iSpecialCameraClearDelayTimer = 0
	g_BossSpecData.specCamData.iSpecialCamNumber = 0
	
	g_BossSpecData.specCamData.iSpectateTargetPlayerID = -1
	
	g_BossSpecData.specCamData.iChatPlayer = 0
	
	g_BossSpecData.specCamData.iFrameDelay = 0
	
	g_BossSpecData.specCamData.bPCControlsSetup = FALSE
	
	#IF IS_DEBUG_BUILD
		g_BossSpecData.specCamData.iSpectatorTextSpamCounter = 0
		g_BossSpecData.specCamData.bSpecCamActive = FALSE
		g_BossSpecData.specCamData.bSwitchSpecCamState = FALSE
		g_BossSpecData.specCamData.iCurrentSpecCamTarget = 0
		g_BossSpecData.specCamData.bResetSpecCamUponDeath = FALSE
		g_BossSpecData.specCamData.bSpecCamWidgetsActive = FALSE
		g_BossSpecData.specCamData.bLocalPlayerViewable = FALSE
		g_BossSpecData.specCamData.bSafeToTurnOnCam = FALSE
		g_BossSpecData.specCamData.iSelectedInHud = 0
	#ENDIF
	
ENDPROC


PROC RESET_SPEC_specHUDTargetList(specHUDTargetList & specHUDTargetListPassed)
	specHUDTargetList GenericEnemyTargetTemp
	specHUDTargetListPassed = GenericEnemyTargetTemp
ENDPROC
PROC RESET_SPRITE_PLACEMENT_SPEC()
	SPRITE_PLACEMENT_SPEC spGTAOLogoTemp
	g_BossSpecData.specHUDData.spGTAOLogo = spGTAOLogoTemp
ENDPROC
PROC RESET_SCALEFORM_INSTRUCTIONAL_BUTTONS_SPEC()

	g_BossSpecData.specHUDData.scaleformInstructionalButtons.bInitialised = FALSE
	INT iLoop
	TEXT_LABEL_63	ButtonSlot
	TEXT_LABEL_15	sButtonSlotString
	REPEAT MAX_SCALEFORM_INSTRC_BUTTONS_INCREASED iLoop
		g_BossSpecData.specHUDData.scaleformInstructionalButtons.Buttons[iLoop].ButtonSlot = ButtonSlot
		g_BossSpecData.specHUDData.scaleformInstructionalButtons.Buttons[iLoop].SecondaryButtonSlot = ButtonSlot
		g_BossSpecData.specHUDData.scaleformInstructionalButtons.Buttons[iLoop].sButtonSlotString = sButtonSlotString
		g_BossSpecData.specHUDData.scaleformInstructionalButtons.Buttons[iLoop].tlGamerTag = ButtonSlot
		
		g_BossSpecData.specHUDData.scaleformInstructionalButtons.Buttons[iLoop].eButtonSlotControl = PLAYER_CONTROL
		g_BossSpecData.specHUDData.scaleformInstructionalButtons.Buttons[iLoop].iButtonSlotInt = 0
		g_BossSpecData.specHUDData.scaleformInstructionalButtons.Buttons[iLoop].iButtonSlotInputA = 0
		g_BossSpecData.specHUDData.scaleformInstructionalButtons.Buttons[iLoop].iButtonSlotInputB = 0
	ENDREPEAT
	
	g_BossSpecData.specHUDData.scaleformInstructionalButtons.iBS_ButtonSlotHasInt = 0
	g_BossSpecData.specHUDData.scaleformInstructionalButtons.iBS_ButtonSlotHasPlayer = 0

	g_BossSpecData.specHUDData.scaleformInstructionalButtons.iBS_ButtonSlotIsInputValid = 0
	g_BossSpecData.specHUDData.scaleformInstructionalButtons.iBS_ButtonSlotIsInputGroup = 0
	g_BossSpecData.specHUDData.scaleformInstructionalButtons.iBS_ButtonSlotIsInputClickable = 0
	
	g_BossSpecData.specHUDData.scaleformInstructionalButtons.bRefreshInstructionalButtons = FALSE
	g_BossSpecData.specHUDData.scaleformInstructionalButtons.ButtonCount = 0
	g_BossSpecData.specHUDData.scaleformInstructionalButtons.iIntAsTime = 0
	
	g_BossSpecData.specHUDData.scaleformInstructionalButtons.AlignX = 0
	g_BossSpecData.specHUDData.scaleformInstructionalButtons.AlignY = 0
	g_BossSpecData.specHUDData.scaleformInstructionalButtons.SizeX = 0
	g_BossSpecData.specHUDData.scaleformInstructionalButtons.SizeY = 0
	
	g_BossSpecData.specHUDData.scaleformInstructionalButtons.fButtonWrap = 1.0
ENDPROC

PROC RESET_MAINTAIN_SCTV_TICKER_QUEUE_DATA_SPEC()
	MAINTAIN_SCTV_TICKER_QUEUE_DATA sSctvQueueLocalDataTemp
	g_BossSpecData.specHUDData.sSctvQueueLocalData = sSctvQueueLocalDataTemp
ENDPROC
PROC RESET_specHUDTargetListEntry_SPEC()
	specHUDTargetListEntry specHUDCurrentFocusTargetTemp
	g_BossSpecData.specHUDData.specHUDCurrentFocusTarget = specHUDCurrentFocusTargetTemp
ENDPROC

PROC RESET_SPECTATOR_HUD_DATA_STRUCT()
	INT iLoop
	
	g_BossSpecData.specHUDData.iBitSet = 0

	RESET_MAINTAIN_SCTV_TICKER_QUEUE_DATA_SPEC()
	RESET_SCALEFORM_INSTRUCTIONAL_BUTTONS_SPEC()
	RESET_SPRITE_PLACEMENT_SPEC()
	RESET_specHUDTargetListEntry_SPEC()
	
	g_BossSpecData.specHUDData.eSpecHUDStage = SPEC_HUD_STAGE_INIT
	REPEAT NUMBER_OF_SPEC_HUD_TARGET_LISTS iLoop
		RESET_SPEC_specHUDTargetList(g_BossSpecData.specHUDData.specHUDList[iLoop])
		RESET_SPEC_specHUDTargetList(g_BossSpecData.specHUDData.specHUDNextUpdateList[iLoop])
		g_BossSpecData.specHUDData.iNextUpdateListEntry[iLoop] = 0
		g_BossSpecData.specHUDData.iCurrentNumberOfTargetsInList[iLoop] = 0
	ENDREPEAT
	
	REPEAT NUM_NETWORK_PLAYERS iLoop
		g_BossSpecData.specHUDData.iProcessSpectatorJobPlayerPositions[iLoop] = 0
	ENDREPEAT

	REPEAT MAX_NUM_SPEC_TEAMS iLoop
		g_BossSpecData.specHUDData.iProcessSpectatorJobTeamPositions[iLoop] = 0
	ENDREPEAT
	g_BossSpecData.specHUDData.iProcessNextPlayerID = 0
	g_BossSpecData.specHUDData.iCurrentNumberOfTargetsInAllLists = 0
	
	g_BossSpecData.specHUDData.iProcessNextTeam = 0
	RESET_SPEC_specHUDTargetList(g_BossSpecData.specHUDData.GenericEnemyTarget)
	RESET_SPEC_specHUDTargetList(g_BossSpecData.specHUDData.SpecialEnemyTarget)
	RESET_SPEC_specHUDTargetList(g_BossSpecData.specHUDData.GenericAllyTarget)
	RESET_SPEC_specHUDTargetList(g_BossSpecData.specHUDData.SpecialAllyTarget)
	
	TIME_DATATYPE TempTime
	g_BossSpecData.specHUDData.timeSwitchProtect = TempTime
	g_BossSpecData.specHUDData.timeInput= TempTime
	g_BossSpecData.specHUDData.timeNewsFilterNextUpdate = TempTime
	
	g_BossSpecData.specHUDData.fNewsFilterScrollEntry = 0.0
	g_BossSpecData.specHUDData.iSoundChangeFilter = -1
	g_BossSpecData.specHUDData.eSpecHUDStageVisitedBeforeLeaderboard = SPEC_HUD_STAGE_FULL_LIST
	g_BossSpecData.specHUDData.eSpecHUDStageVisitedBeforeQuitYesNo = SPEC_HUD_STAGE_FULL_LIST

	#IF IS_DEBUG_BUILD
		g_BossSpecData.specHUDData.bDebugInitSpecHUDWidget = FALSE
		g_BossSpecData.specHUDData.bDebugForceAllowLeaderboard = FALSE
	#ENDIF

	g_BossSpecData.specHUDData.bSelectionButtonHeldThisFrame = FALSE
	
	g_BossSpecData.specHUDData.iCurrentPage = 0
	g_BossSpecData.specHUDData.iMaximumPage = 0
	g_BossSpecData.specHUDData.iNumberOfTargetsOnAPage = LARGEST_PAGE_SIZE
	g_BossSpecData.specHUDData.iNumberOfTargetsOnCurrentPage = 0
	g_BossSpecData.specHUDData.iCurrentlyHighlightedRow = 0

	g_BossSpecData.specHUDData.specHUDMode = SPEC_HUD_MODE_FULL
	
	g_BossSpecData.specHUDData.iCurrentFakeWantedLevel = 0
	g_BossSpecData.specHUDData.bFakeWantedLevelFlashing = FALSE
	
	//Filter variables
	g_BossSpecData.specHUDData.eCurrentFilter = SPEC_HUD_FILTER_SPEC_1	//SPEC_HUD_FILTER_NONE
	g_BossSpecData.specHUDData.eDesiredFilter = SPEC_HUD_FILTER_SPEC_1	//SPEC_HUD_FILTER_NONE
	g_BossSpecData.specHUDData.iFilterStage = 0
	g_BossSpecData.specHUDData.iFilterChangeTime = 0
	
	SCALEFORM_INDEX sfTemp
	g_BossSpecData.specHUDData.sfFilter = sfTemp
	g_BossSpecData.specHUDData.screenRT = 0
	
	g_BossSpecData.specHUDData.sfSummaryCard = sfTemp
	
	g_BossSpecData.specHUDData.sfButton = sfTemp
	
	g_BossSpecData.specHUDData.iTickerStage = 0
	g_BossSpecData.specHUDData.TickerUpdateDelay = 0
	g_BossSpecData.specHUDData.bRefresh = FALSE
	g_BossSpecData.specHUDData.bSCTVText = FALSE
	g_BossSpecData.specHUDData.bToldPlayerCantSpectateChosenPlayer = FALSE
	
	SCRIPT_TIMER TempTimer
	g_BossSpecData.specHUDData.TickerUpdateTimer = TempTimer
	g_BossSpecData.specHUDData.SCHeadshotUpdateTimer = TempTimer
	g_BossSpecData.specHUDData.FadeInTimer = TempTimer
	
	g_BossSpecData.specHUDData.iPlayerListScrollDir = 1
	
	
	g_BossSpecData.specHUDData.iSpecKillStripStage = 0
ENDPROC

PROC RESET_GLOBAL_SPECTATOR_STRUCT()
	g_BossSpecData.eStartMode = SPEC_MODE_PLAYERS_RESPAWN_EVENT
	RESET_SPECTATOR_CAM_DATA_STRUCT()
	RESET_SPECTATOR_HUD_DATA_STRUCT()
	SCRIPT_TIMER	stTemp
	g_BossSpecData.stBailTimOut = stTemp
	g_BossSpecData.stBailNoOneTimOut = stTemp
	g_BossSpecData.stRaceNoOneTimOut = stTemp
	g_BossSpecData.stBailDelay = stTemp
	g_BossSpecData.stBailStrandDelay = stTemp
	g_BossSpecData.stSwitchTargetDelay = stTemp
	PED_INDEX pedTemp
	g_BossSpecData.pedDesiredFocus = pedTemp
	g_BossSpecData.pedLastCurrentFocus = pedTemp
	g_BossSpecData.pedLastDesiredFocus = pedTemp
	
	
ENDPROC








// =================
// 	Procs & funcs
// =================

//PURPOSE: Stores the players Camera Modes 
PROC STORE_SPEC_CAM_MODES()
	INT i
	IF NOT NETWORK_IS_IN_SPECTATOR_MODE()
		#IF IS_DEBUG_BUILD DEBUG_PRINTCALLSTACK() #ENDIF
		IF NOT IS_BIT_SET(MPSpecGlobals.iStoredCamBitSet, SCTV_SC_BIT_STORED_CAM_MODES)
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === STORE_SPEC_CAM_MODES - STORE CONTEXT CAMS - NUM_CAM_VIEW_MODE_CONTEXTS = ", ENUM_TO_INT(NUM_CAM_VIEW_MODE_CONTEXTS))
			REPEAT ENUM_TO_INT(NUM_CAM_VIEW_MODE_CONTEXTS) i
				//STORE
				MPSpecGlobals.StoredContextCam[i] = GET_CAM_VIEW_MODE_FOR_CONTEXT(INT_TO_ENUM(CAM_VIEW_MODE_CONTEXT, i))
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === STORE_SPEC_CAM_MODES - GET_CAM_VIEW_MODE_FOR_CONTEXT - StoredContextCam ", i, " = ", ENUM_TO_INT(MPSpecGlobals.StoredContextCam[i]))
			ENDREPEAT
			SET_BIT(MPSpecGlobals.iStoredCamBitSet, SCTV_SC_BIT_STORED_CAM_MODES)
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === STORE_SPEC_CAM_MODES - SCTV_SC_BIT_STORED_CAM_MODES SET")
			
			SET_BIT(MPSpecGlobals.iStoredCamBitSet, SCTV_SC_BIT_SET_CINEMATIC_CAM_MODE)
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === STORE_SPEC_CAM_MODES - SCTV_SC_BIT_SET_CINEMATIC_CAM_MODE - SET ")
		ELSE
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === STORE_SPEC_CAM_MODES - SCTV_SC_BIT_STORED_CAM_MODES IS SET - SKIP")
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Sets back to the players Camera Modes 
PROC SET_SPEC_CAM_MODES(BOOL bStart = TRUE)
	INT i
	#IF IS_DEBUG_BUILD DEBUG_PRINTCALLSTACK() #ENDIF
	//Check we have stored some values
	IF IS_BIT_SET(MPSpecGlobals.iStoredCamBitSet, SCTV_SC_BIT_STORED_CAM_MODES)
		IF bStart = TRUE
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_SPEC_CAM_MODES - GET_CAM_VIEW_MODE_FOR_CONTEXT - SET CONTEXT CAMS TO CINEMATIC - NUM_CAM_VIEW_MODE_CONTEXTS = ", ENUM_TO_INT(NUM_CAM_VIEW_MODE_CONTEXTS))
			REPEAT ENUM_TO_INT(NUM_CAM_VIEW_MODE_CONTEXTS) i
				//SET
				SET_CAM_VIEW_MODE_FOR_CONTEXT(INT_TO_ENUM(CAM_VIEW_MODE_CONTEXT, i), CAM_VIEW_MODE_CINEMATIC)
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_SPEC_CAM_MODES - SET_CAM_VIEW_MODE_FOR_CONTEXT - MADE CURRENT CAME VIEW MODE CINEMATIC ", i)
			ENDREPEAT
		ELSE
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_SPEC_CAM_MODES - SET_CAM_VIEW_MODE_FOR_CONTEXT - SET CONTEXT CAMS TO STORED - NUM_CAM_VIEW_MODE_CONTEXTS = ", ENUM_TO_INT(NUM_CAM_VIEW_MODE_CONTEXTS))
			REPEAT ENUM_TO_INT(NUM_CAM_VIEW_MODE_CONTEXTS) i
				 SET_CAM_VIEW_MODE_FOR_CONTEXT(INT_TO_ENUM(CAM_VIEW_MODE_CONTEXT, i), MPSpecGlobals.StoredContextCam[i])
				 CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_SPEC_CAM_MODES - SET_CAM_VIEW_MODE_FOR_CONTEXT - StoredContextCam ", i, " = ", ENUM_TO_INT(MPSpecGlobals.StoredContextCam[i]))
			ENDREPEAT
			CLEAR_BIT(MPSpecGlobals.iStoredCamBitSet, SCTV_SC_BIT_STORED_CAM_MODES)
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_SPEC_CAM_MODES - SCTV_SC_BIT_STORED_CAM_MODES CLEARED")
		ENDIF
	ELSE
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_SPEC_CAM_MODES - SCTV_SC_BIT_STORED_CAM_MODES IS NOT SET - SKIP")
	ENDIF
ENDPROC

/// PURPOSE:
///    Wrapper for the natives to change the machine's local focus and minimap focus
PROC SET_IN_SPECTATOR_MODE(BOOL bInSpectatorMode, PED_INDEX pedTarget = NULL, BOOL bStoreCams = TRUE)
	#IF IS_DEBUG_BUILD
	IF bInSpectatorMode
		IF DOES_ENTITY_EXIST(pedTarget)
			IF IS_PED_A_PLAYER(pedTarget)
				IF IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTarget), FALSE)
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_IN_SPECTATOR_MODE(TRUE, ", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTarget)), ")")
				ELSE
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_IN_SPECTATOR_MODE(TRUE, Player ped but not OK)")
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_IN_SPECTATOR_MODE(TRUE, Non-player ped ", NATIVE_TO_INT(pedTarget), ")")
			ENDIF
		ELSE
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_IN_SPECTATOR_MODE(TRUE, Unknown Ped)")
			EXIT
		ENDIF
	ELSE
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_IN_SPECTATOR_MODE(FALSE)")
	ENDIF
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	IF IS_ENTITY_DEAD(pedTarget)
	ENDIF
	
	BOOL bSkip
	IF bInSpectatorMode = TRUE
		IF DOES_ENTITY_EXIST(pedTarget)
			IF IS_PED_A_PLAYER(pedTarget)
				IF NOT NETWORK_ARE_PLAYERS_IN_SAME_TUTORIAL_SESSION(PLAYER_ID(), NETWORK_GET_PLAYER_INDEX_FROM_PED(pedTarget))
					bSkip = TRUE
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === PLAYER AND TARGET NOT IN SAME TUTORIAL SESSION")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bSkip = FALSE
		IF bStoreCams = TRUE
			IF bInSpectatorMode = TRUE
				STORE_SPEC_CAM_MODES()
			ELSE
				IF NOT IS_BIT_SET(MPSpecGlobals.iStoredCamBitSet, SCTV_SC_BIT_SET_STORED_CAM_MODE)
					IF IS_BIT_SET(MPSpecGlobals.iStoredCamBitSet, SCTV_SC_BIT_STORED_CAM_MODES)
						INT i
						REPEAT ENUM_TO_INT(NUM_CAM_VIEW_MODE_CONTEXTS) i
							 SET_CAM_VIEW_MODE_FOR_CONTEXT(INT_TO_ENUM(CAM_VIEW_MODE_CONTEXT, i), MPSpecGlobals.StoredContextCam[i])
							 CPRINTLN(DEBUG_SPECTATOR, "=== CAM === MAINTAIN_FREEMODE_SPECTATOR_SET_CAM_MODES - FIRST ATTEMPT - SET_CAM_VIEW_MODE_FOR_CONTEXT - StoredContextCam ", i, " = ", ENUM_TO_INT(MPSpecGlobals.StoredContextCam[i]))
						ENDREPEAT
					ENDIF
				
					SET_BIT(MPSpecGlobals.iStoredCamBitSet, SCTV_SC_BIT_SET_STORED_CAM_MODE)
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === _SPEC_CAM_MODES - SCTV_SC_BIT_SET_STORED_CAM_MODE - SET ")
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_PLAYER_SCTV(PLAYER_ID())
			NETWORK_SET_IN_SPECTATOR_MODE_EXTENDED(bInSpectatorMode, pedTarget, TRUE)
		ELSE
			NETWORK_SET_IN_SPECTATOR_MODE(bInSpectatorMode, pedTarget)
		ENDIF
		
		SET_MINIMAP_IN_SPECTATOR_MODE(bInSpectatorMode, pedTarget)
				
		SET_MP_BOOL_PLAYER_STAT(MPPLY_IS_CHAR_SPECTATING, bInSpectatorMode)
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_IN_SPECTATOR_MODE - MP_STAT_IS_CHAR_SPECTATING = ", bInSpectatorMode)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Gets if any spectator cam is active.
/// RETURNS:
///    true/false
FUNC BOOL IS_A_SPECTATOR_CAM_ACTIVE()
//	#IF USE_TU_CHANGES
		RETURN IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_ACTIVE)
//	#ENDIF
//	
//	//Must not be changed. -BenR
//	#IF NOT USE_TU_CHANGES
//		RETURN IS_BIT_SET(MPSpecGlobals_OLD.iBitSet, GLOBAL_SPEC_BS_ACTIVE)
//	#ENDIF
ENDFUNC

/// PURPOSE:
///    Gets if any spectator cam has watched a ped, or is switching after having watched a ped.
/// RETURNS:
///    true/false
FUNC BOOL IS_A_SPECTATOR_CAM_RUNNING()
	RETURN IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_RUNNING)
ENDFUNC

/// PURPOSE:
///    Gets if the user has used the yes/no quit screen of a spectator cam to flag he desires to leave the current job
/// RETURNS:
///    true/false
FUNC BOOL DOES_A_SPECTATOR_CAM_WANT_TO_LEAVE()
	RETURN IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WANT_TO_LEAVE)
ENDFUNC

/// PURPOSE:
///    Gets if a spectator cam is requesting to see the overhead names
/// RETURNS:
///    true/false
FUNC BOOL DOES_A_SPECTATOR_CAM_WANT_OVERHEAD()
	RETURN IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_OVERHEAD_REQUESTED)
ENDFUNC

/// PURPOSE:
///    Gets if the spectator cam requries the player to be hidden (for example, if doing a late-player spectator)
/// RETURNS:
///    true/false
FUNC BOOL SHOULD_PLAYER_BE_HIDDEN_DUE_TO_SPECTATING()
	IF IS_A_SPECTATOR_CAM_ACTIVE()
		IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDE_LOCAL_PED_IF_ALIVE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets if the spectator cam is safe to turn on. Should be checked before ACTIVATE_SPECTATOR_CAM() is called.
/// RETURNS:
///    true/false
FUNC BOOL IS_SAFE_TO_TURN_ON_SPECTATOR_CAM()
	RETURN TRUE
	IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState = RESPAWN_STATE_PLAYING
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Gets the ped index of the current focus of the spectator cam.
/// RETURNS:
///    Current focus PED_INDEX.
FUNC PED_INDEX GET_SPECTATOR_CURRENT_FOCUS_PED()

//#IF USE_TU_CHANGES
	RETURN MPSpecGlobals.pedCurrentFocus
//#ENDIF

//#IF NOT USE_TU_CHANGES
// 	RETURN MPSpecGlobals_OLD.pedCurrentFocus
//#ENDIF

ENDFUNC

/// PURPOSE:
///    Gets the ped index of the desired focus of the spectator cam.
/// RETURNS:
///    Desired focus PED_INDEX.
FUNC PED_INDEX GET_SPECTATOR_DESIRED_FOCUS_PED()
	RETURN MPSpecGlobals.pedDesiredFocus
ENDFUNC

/// PURPOSE:
///    Gets the ped the camera is moving to if it's moving, otherwise returns the current focus ped
/// RETURNS:
///    Desired focus PED_INDEX if camera moving, otherwise current focus PED_INDEX.
FUNC PED_INDEX GET_SPECTATOR_SELECTED_PED()
	IF DOES_ENTITY_EXIST(GET_SPECTATOR_DESIRED_FOCUS_PED())
		RETURN	GET_SPECTATOR_DESIRED_FOCUS_PED()
	ENDIF
	RETURN GET_SPECTATOR_CURRENT_FOCUS_PED()
ENDFUNC

/// PURPOSE:
///    Returns true depending on if the ped exists and if it's a player or not. If it's a player, the player also needs to be active to be true
/// PARAMS:
///    thisPed - ped_index to test
/// RETURNS:
///    true/false
FUNC BOOL SPEC_IS_PED_ACTIVE(PED_INDEX thisPed)
	IF DOES_ENTITY_EXIST(thisPed)
		IF IS_PED_A_PLAYER(thisPed)
			IF NETWORK_IS_PLAYER_ACTIVE(NETWORK_GET_PLAYER_INDEX_FROM_PED(thisPed))
				RETURN TRUE
			ENDIF
		//ELSE
		//	RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if the spectator hud has been set to hidden
/// RETURNS:
///    true/false
FUNC BOOL IS_SPECTATOR_HUD_HIDDEN()
	RETURN IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_HIDDEN)
ENDFUNC

/// PURPOSE:
///    Returns true if the news filter for the spectator hud is currently being displayed
/// RETURNS:
///    true/false
FUNC BOOL IS_SPECTATOR_SHOWING_NEWS_HUD()
	RETURN IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_NEWS_FILTER)
ENDFUNC

/// PURPOSE:
///    Sets all camera view modes to cinematic. Stores the player's actual cam view choices.
PROC SETUP_CAM_VIEW_MODES_CINEMATIC(CAM_VIEW_MODE &camViewModesArray[], BOOL bDoCams = TRUE)
	IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WANT_TO_LEAVE)
	
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SETUP_CAM_VIEW_MODES_CINEMATIC")
		#ENDIF
		
		IF bDoCams = TRUE
			CAM_VIEW_MODE desiredCamViewMode
		
			INT i
			REPEAT NUM_CAM_VIEW_MODES i
				
				/*IF INT_TO_ENUM(CAM_VIEW_MODE_CONTEXT, i) = CAM_VIEW_MODE_CONTEXT_ON_FOOT
					desiredCamViewMode = CAM_VIEW_MODE_THIRD_PERSON_MEDIUM
				ELSE*/
					desiredCamViewMode = CAM_VIEW_MODE_CINEMATIC
				//ENDIF
				
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(INT_TO_ENUM(CAM_VIEW_MODE_CONTEXT, i)) <> desiredCamViewMode	//If camera is not cinematic
					camViewModesArray[i] = GET_CAM_VIEW_MODE_FOR_CONTEXT(INT_TO_ENUM(CAM_VIEW_MODE_CONTEXT, i))
					SET_CAM_VIEW_MODE_FOR_CONTEXT(INT_TO_ENUM(CAM_VIEW_MODE_CONTEXT, i), desiredCamViewMode)
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SETUP_CAM_VIEW_MODES_CINEMATIC - SET_CAM_VIEW_MODE_FOR_CONTEXT - SET CAM AS CINEMATIC - STORE CAM VIEW MODE CONTEXT = ", i)
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SETUP_CAM_VIEW_MODES_CINEMATIC - SET_CAM_VIEW_MODE_FOR_CONTEXT - SET CAM AS CINEMATIC - STORED CAM VIEW MODE = ", ENUM_TO_INT(camViewModesArray[i]))
					#ENDIF
				ENDIF
				
			ENDREPEAT
		ENDIF
		
		SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WANT_TO_LEAVE)
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets all camera view modes back to player's choice. Wipes the player choice cache.
PROC CLEANUP_CAM_VIEW_MODES_CINEMATIC(CAM_VIEW_MODE &camViewModesArray[], BOOL bDoCams = TRUE)
	IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WANT_TO_LEAVE)
	
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEANUP_CAM_VIEW_MODES_CINEMATIC")
		#ENDIF
		
		IF bDoCams = TRUE
			INT i
			REPEAT NUM_CAM_VIEW_MODES i
				IF camViewModesArray[i] <> NUM_CAM_VIEW_MODES								//If something has been cached
					SET_CAM_VIEW_MODE_FOR_CONTEXT(INT_TO_ENUM(CAM_VIEW_MODE_CONTEXT, i), camViewModesArray[i])
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEANUP_CAM_VIEW_MODES_CINEMATIC - SET_CAM_VIEW_MODE_FOR_CONTEXT - CAM VIEW MODE CONTEXT = ", i)
					CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEANUP_CAM_VIEW_MODES_CINEMATIC - SET_CAM_VIEW_MODE_FOR_CONTEXT - CAM VIEW MODE = ", ENUM_TO_INT(camViewModesArray[i]))
					camViewModesArray[i] = NUM_CAM_VIEW_MODES
				ENDIF
			ENDREPEAT
		ENDIF
		
		CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WANT_TO_LEAVE)
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Flags that the spectator target list should fully sort itself due to event
PROC SET_SPECTATOR_REDO_LIST_DUE_TO_EVENT()
	IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_REDO_LIST_DUE_TO_EVENT)
		SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_REDO_LIST_DUE_TO_EVENT)
		CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_REDO_LIST_DUE_TO_EVENT_DELAYED)
		RESET_NET_TIMER(MPSpecGlobals.RedoListDelayTimer)
		#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_SPECTATOR_REDO_LIST_DUE_TO_EVENT")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets a flag that will call SET_SPECTATOR_REDO_LIST_DUE_TO_EVENT after a short delay
PROC SET_SPECTATOR_REDO_LIST_DUE_TO_EVENT_DELAYED()
	IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_REDO_LIST_DUE_TO_EVENT_DELAYED)
		SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_REDO_LIST_DUE_TO_EVENT_DELAYED)
		RESET_NET_TIMER(MPSpecGlobals.RedoListDelayTimer)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_SPECTATOR_REDO_LIST_DUE_TO_EVENT_DELAYED")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears flag that the spectator target list should fully sort itself
PROC CLEAR_SPECTATOR_REDO_LIST_DUE_TO_EVENT()
	IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_REDO_LIST_DUE_TO_EVENT)
		CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_REDO_LIST_DUE_TO_EVENT)
		CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_REDO_LIST_DUE_TO_EVENT_DELAYED)
		RESET_NET_TIMER(MPSpecGlobals.RedoListDelayTimer)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEAR_SPECTATOR_REDO_LIST_DUE_TO_EVENT")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns whether or not the flag is set to redo the spectator target list
/// RETURNS:
///    true/false
FUNC BOOL SHOULD_SPECTATOR_REDO_LIST_DUE_TO_EVENT()
	RETURN IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_REDO_LIST_DUE_TO_EVENT)
ENDFUNC

/// PURPOSE:
///    Checks for a delayed call of SET_SPECTATOR_REDO_LIST_DUE_TO_EVENT
PROC CHECK_SPECTATOR_REDO_LIST_DUE_TO_EVENT_DELAYED()
	IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_REDO_LIST_DUE_TO_EVENT_DELAYED)
	AND NOT IS_SCREEN_FADED_OUT()
		IF HAS_NET_TIMER_EXPIRED(MPSpecGlobals.RedoListDelayTimer, REDO_LIST_DELAY)
			SET_SPECTATOR_REDO_LIST_DUE_TO_EVENT()
			RESET_NET_TIMER(MPSpecGlobals.RedoListDelayTimer)
			CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_REDO_LIST_DUE_TO_EVENT_DELAYED)
			CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SHOULD_SPECTATOR_REDO_LIST_DUE_TO_EVENT_DELAYED")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets flag to re-setup the data for the news hud (such as player name or crew name)
PROC SET_REDO_NEWS_HUD_DUE_TO_EVENT()
	IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_REDO_NEWS_HUD)
		SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_REDO_NEWS_HUD)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_REDO_NEWS_HUD_DUE_TO_EVENT")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears the flag to re-setup the news hud data (such as player name or crew name)
PROC CLEAR_REDO_NEWS_HUD_DUE_TO_EVENT()
	IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_REDO_NEWS_HUD)
		CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_REDO_NEWS_HUD)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEAR_REDO_NEWS_HUD_DUE_TO_EVENT")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns whether or not the flag has been set to re-setup the news hud data (such as player name or crew name)
/// RETURNS:
///    true/false
FUNC BOOL SHOULD_REDO_NEWS_HUD_DUE_TO_EVENT()
	RETURN IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_REDO_NEWS_HUD)
ENDFUNC

/// PURPOSE:
///    Flag that the spectator cam has been cleaned up due to a session transition
PROC SET_SPEC_BAILED_FOR_TRANSITION()
	IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BAILED_FOR_TRANSITION)
		SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BAILED_FOR_TRANSITION)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_SPEC_BAILED_FOR_TRANSITION")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears the flag that the spectator cam has been cleaned up due to a session transition
PROC CLEAR_SPEC_BAILED_FOR_TRANSITION()
	IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BAILED_FOR_TRANSITION)
		CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BAILED_FOR_TRANSITION)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEAR_SPEC_BAILED_FOR_TRANSITION")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns if the spectator cam has been cleaned up due to a session transition
/// RETURNS:
///    true/false
FUNC BOOL DID_SPEC_BAILED_FOR_TRANSITION()
	RETURN IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BAILED_FOR_TRANSITION)
ENDFUNC

/// PURPOSE:
///    Disables the spectator cam finding a new spectator target if the current target becomes invalid
PROC DISABLE_SPEC_CAN_FIND_NEW_FOCUS()
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === DISABLE_SPEC_CAN_FIND_NEW_FOCUS()")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_FIND_NEW_FOCUS)
ENDPROC

/// PURPOSE:
///    Enables the spectator cam finding a new spectator target if the current target becomes invalid
PROC ENABLE_SPEC_CAN_FIND_NEW_FOCUS()
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_SPECTATOR, "=== CAM === ENABLE_SPEC_CAN_FIND_NEW_FOCUS()")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_BLOCK_FIND_NEW_FOCUS)
ENDPROC

/// PURPOSE:
///    Sets a broadcast flag that the player is playing a cutscene for entering/leaving/moving between properties
/// PARAMS:
///    iPropertyID - 
PROC SET_PLAYER_BROADCAST_PLAYING_PROPERTY_TRANSITION_CUTSCENE(INT iPropertyID)
	#IF IS_DEBUG_BUILD
	IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayingPropertyTransitionCutscene)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === SET_PLAYER_BROADCAST_PLAYING_PROPERTY_TRANSITION_CUTSCENE(", iPropertyID, ")")
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bPlayingPropertyTransitionCutscene = TRUE
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayingPropertyTransitionCutscene)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iTransitionCutsceneProperty = iPropertyID
ENDPROC

/// PURPOSE:
///    Clears a broadcast flag that the player is playing a cutscene for entering/leaving/moving between properties
PROC CLEAR_PLAYER_BROADCAST_PLAYING_PROPERTY_TRANSITION_CUTSCENE()
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayingPropertyTransitionCutscene)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== CUTSCENE === CLEAR_PLAYER_BROADCAST_PLAYING_PROPERTY_TRANSITION_CUTSCENE()")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
		//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].bPlayingPropertyTransitionCutscene = FALSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_bPlayingPropertyTransitionCutscene)
		GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iTransitionCutsceneProperty = -1
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns if the player is being hidden under the ground or not
/// PARAMS:
///    playerID - PLAYER_INDEX of the player to test
/// RETURNS:
///    true/false
FUNC BOOL IS_SAFE_TO_USE_PLAYER_COORDS_FOR_CAMERA(PLAYER_INDEX playerID)
	IF IS_NET_PLAYER_OK(playerID, FALSE)
		PED_INDEX pedID = GET_PLAYER_PED(playerID)
		IF DOES_ENTITY_EXIST(pedID)
			IF IS_ENTITY_DEAD(pedID)
			ENDIF
			VECTOR vThisPlayerCoords = GET_ENTITY_COORDS(pedID)
			IF vThisPlayerCoords.z > -145.0 //Players are put at -150 if unsafe
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the local player is actually doing a skyswoop up, as opposed to doing a skyswoop up to mimic a spectate target
PROC SET_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP()
	IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_DOING_LEGITIMATE_SKYSWOOP_UP)
		SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_DOING_LEGITIMATE_SKYSWOOP_UP)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears the local player is actually doing a skyswoop up, as opposed to doing a skyswoop up to mimic a spectate target
PROC CLEAR_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP()
	IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_DOING_LEGITIMATE_SKYSWOOP_UP)
		CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_DOING_LEGITIMATE_SKYSWOOP_UP)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEAR_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP")
		DEBUG_PRINTCALLSTACK()
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns if the local player is actually doing a skyswoop up, as opposed to doing a skyswoop up to mimic a spectate target
/// RETURNS:
///    true/false
FUNC BOOL IS_SPECTATOR_DOING_LEGITIMATE_SKYSWOOP_UP()
	RETURN IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_DOING_LEGITIMATE_SKYSWOOP_UP)
ENDFUNC

/// PURPOSE:
///    Disables the spectator fading to match the spectated target until leaving the event
/// PARAMS:
///    bLockUntilEnableCalled - Doesn't reset even if an event ends until specifically the ENABLE_SPECTATOR_FADES is called
PROC DISABLE_SPECTATOR_FADES_DURING_THIS_EVENT(BOOL bLockUntilEnableCalled = FALSE)
	IF NOT IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FADES_DISABLED)
		
		IF bLockUntilEnableCalled
			SET_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FADE_DISABLE_LOCKED)
		ENDIF
		
		SET_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FADES_DISABLED)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === SET_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FADES_DISABLED)")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Enables the spectator cam fading to match the spectated player
PROC ENABLE_SPECTATOR_FADES()
	IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FADES_DISABLED)
	
		IF IS_BIT_SET(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FADE_DISABLE_LOCKED)
			CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FADE_DISABLE_LOCKED)
		ENDIF
	
		CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FADES_DISABLED)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== CAM === CLEAR_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_FADES_DISABLED)")
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL ARE_SPECTATOR_COPY_TARGET_FADES_BLOCKED()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_SPECTATOR_COPY_TARGET_FADES)
ENDFUNC

PROC BLOCK_SPECTATOR_COPYING_TARGET_FADES(BOOL bBlock)
	IF bBlock = TRUE
		SET_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_SPECTATOR_COPY_TARGET_FADES)
	ELSE
		CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_BLOCK_SPECTATOR_COPY_TARGET_FADES)
	ENDIF
ENDPROC

//PURPOSE: Sets a flag to say if the Spectator is in the middle of a Cutscene.
PROC SET_SPECTATOR_RUNNING_CUTSCENE(BOOL bYes)
	IF bYes = TRUE
		SET_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RUNNING_CUTSCENE)
		CPRINTLN(DEBUG_SPECTATOR, " SET_SPECTATOR_RUNNING_CUTSCENE - TRUE - GLOBAL_SPEC_BAIL_BS_RUNNING_CUTSCENE SET")
		DISABLE_SPEC_CAN_FIND_NEW_FOCUS()
	ELSE
		CLEAR_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RUNNING_CUTSCENE)
		CPRINTLN(DEBUG_SPECTATOR, " SET_SPECTATOR_RUNNING_CUTSCENE - FALSE - GLOBAL_SPEC_BAIL_BS_RUNNING_CUTSCENE CLEARED")
		ENABLE_SPEC_CAN_FIND_NEW_FOCUS()
	ENDIF
ENDPROC

FUNC BOOL IS_SPECTATOR_RUNNING_CUTSCENE()
	RETURN IS_BIT_SET(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_RUNNING_CUTSCENE)
ENDFUNC

//PURPOSE: Allows remote scripts to position the Spectator coords and focus somewhere other than the Camera. 
PROC SET_SPECTATOR_OVERRIDE_COORDS(VECTOR vCoords)
	//IF IS_PLAYER_SPECTATING(PLAYER_ID())
	IF IS_A_SPECTATOR_CAM_ACTIVE()
		MPSpecGlobals.vOverrideCoords = vCoords
		SET_FOCUS_POS_AND_VEL(vCoords, <<0,0,0>>)
		CPRINTLN(DEBUG_SPECTATOR, " SET_OVERRIDE_SPECTATOR_COORDS - SET_FOCUS_POS_AND_VEL = ", MPSpecGlobals.vOverrideCoords)
		g_bUpdateSpectatorPosition = TRUE
		CPRINTLN(DEBUG_SPECTATOR, " SET_OVERRIDE_SPECTATOR_COORDS - vOverrideCoords = ", MPSpecGlobals.vOverrideCoords)
		#IF IS_DEBUG_BUILD DEBUG_PRINTCALLSTACK() #ENDIF
	ENDIF
ENDPROC

//PURPOSE: Clears the Overridden Spectator Coords if they ahve been set. 
PROC CLEAR_SPECTATOR_OVERRIDE_COORDS()
	IF NOT IS_VECTOR_ZERO(MPSpecGlobals.vOverrideCoords)
		CLEAR_FOCUS()
		CPRINTLN(DEBUG_SPECTATOR, " CLEAR_OVERRIDE_SPECTATOR_COORDS - CLEAR_FOCUS")
		MPSpecGlobals.vOverrideCoords = <<0, 0, 0>>
		g_bUpdateSpectatorPosition = TRUE
		CPRINTLN(DEBUG_SPECTATOR, " CLEAR_OVERRIDE_SPECTATOR_COORDS - DONE")
		#IF IS_DEBUG_BUILD DEBUG_PRINTCALLSTACK() #ENDIF
	ENDIF
ENDPROC

//PURPOSE: Returns TRUE if a script has set the Spectators coords to be somewhere other than the camera
FUNC BOOL HAVE_SPECTATOR_COORDS_BEEN_OVERRIDDEN()
	RETURN NOT IS_VECTOR_ZERO(MPSpecGlobals.vOverrideCoords)
ENDFUNC

//PURPOSE: Returns the COORDS the SCTV Spectator has been Overridden to.
FUNC VECTOR GET_SPECTATOR_OVERRIDE_COORDS()
	RETURN MPSpecGlobals.vOverrideCoords
ENDFUNC

//PURPOSE: Returns TRUE if the local Player is using Heist Spectate
FUNC BOOL USING_HEIST_SPECTATE()
	RETURN IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_DO_HEIST_SPECTATE)
ENDFUNC

/*//PURPOSE: If the aytregt is in a vehicle then laod the vehicles dials. //REMOVED DUE TO SPECTATOR NO LONGER BEING ABLE TO USE 1ST PERSON
PROC MAINTAIN_VEHICLE_DIALS()
	IF SPEC_IS_PED_ACTIVE(MPSpecGlobals.pedCurrentFocus)
		IF IS_PED_IN_ANY_VEHICLE(MPSpecGlobals.pedCurrentFocus)
			REQUEST_VEHICLE_DIAL(GET_VEHICLE_PED_IS_IN(MPSpecGlobals.pedCurrentFocus))
		ENDIF
	ENDIF
ENDPROC*/

FUNC CONTROL_ACTION GET_SPECTATOR_ACCEPT_CANCEL_INPUT(BOOL bGetAccept)
	
	//ACCEPT
	IF bGetAccept = TRUE
		
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			RETURN INPUT_CELLPHONE_SELECT
		ENDIF
		
		RETURN INPUT_FRONTEND_ACCEPT
		
	ENDIF
	
	//CANCEL
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN INPUT_CELLPHONE_CANCEL
	ENDIF
	
	RETURN INPUT_FRONTEND_CANCEL

	
ENDFUNC

/// PURPOSE:
///    Translates the inputs passed in to the pc equivalents
///    Needed because the spectator HUD controls were getting unloaded on occasion - B* 
/// PARAMS:
///    caInput - The input to translate
/// RETURNS:
///    
FUNC CONTROL_ACTION GET_SPECTATOR_CAM_INPUT(CONTROL_ACTION caInput )

	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN caInput
	ENDIF

	SWITCH caInput
	
		// Leaderboard
		CASE INPUT_SCRIPT_RLEFT
		
			RETURN INPUT_SELECT_WEAPON
			
		// Show profile
		CASE INPUT_SCRIPT_LB
		
			RETURN INPUT_SPECIAL_ABILITY_PC
			
		// Toggle radio mode
		CASE INPUT_SCRIPT_RB
		
			RETURN INPUT_DUCK
			
		// Previous player
		CASE INPUT_SCRIPT_LT
				
			RETURN INPUT_CELLPHONE_UP
			
		// Next player
		CASE INPUT_SCRIPT_RT
		
			RETURN INPUT_CELLPHONE_DOWN
		
		// Show feed
		CASE INPUT_SCRIPT_LS
		
			RETURN INPUT_ENTER
			
		// Show heli
		CASE INPUT_SCRIPT_RS
			
			RETURN INPUT_COVER
			
		// Radio prev
		CASE INPUT_SCRIPT_PAD_LEFT
		
			RETURN INPUT_CELLPHONE_LEFT
			
		// Radio next
		CASE INPUT_SCRIPT_PAD_RIGHT
		
			RETURN INPUT_CELLPHONE_RIGHT
			
		// TV on/off
		CASE INPUT_SCRIPT_RUP
		
			RETURN INPUT_CONTEXT
	
	ENDSWITCH
	
	// return the original input by default.
	RETURN caInput

ENDFUNC

FUNC CONTROL_ACTION_GROUP GET_SPECTATOR_CAM_INPUT_GROUP( CONTROL_ACTION_GROUP caGroup)

	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN caGroup
	ENDIF

	IF caGroup = INPUTGROUP_SCRIPT_DPAD_LR
		RETURN INPUTGROUP_CELLPHONE_NAVIGATE_LR
	ENDIF

	RETURN caGroup
ENDFUNC
