///// net_casino_cpm.sch
///// Online Technical: Conor McGuire
///// Casino apartment customisation header

USING "net_include.sch"
USING "MP_globals_casino_consts.sch"
USING "cellphone_public.sch"
USING "context_control_public.sch"
USING "net_vehicle_setup.sch"
USING "website_public.sch"
USING "net_cutscene.sch"
USING "casino_decoration_common.sch"

#IF FEATURE_CASINO
CONST_INT CASINO_CPM_LEVEL_ROOMS			0
CONST_INT CASINO_CPM_LEVEL_SUBPOSITIONS		1
CONST_INT CASINO_CPM_LEVEL_ITEM_TYPES		2
CONST_INT CASINO_CPM_LEVEL_ITEMS			3
CONST_INT CASINO_CPM_LEVEL_MAX				4

CONST_INT CASINO_CPM_BS_SETUP_MENU			0
CONST_INT CASINO_CPM_BS_CONFIRMATION		1
CONST_INT CASINO_CPM_BS_PRESSED_ACCEPT		2
CONST_INT CASINO_CPM_BS_PRESSED_CANCEL		3
CONST_INT CASINO_CPM_BS_PRESSED_UP			4
CONST_INT CASINO_CPM_BS_PRESSED_DOWN		5
CONST_INT CASINO_CPM_BS_PRESSED_LEFT		6
CONST_INT CASINO_CPM_BS_PRESSED_RIGHT		7
CONST_INT CASINO_CPM_BS_NO_MONEY			8
CONST_INT CASINO_CPM_BS_REFRESH_ITEM		9
CONST_INT CASINO_CPM_BS_REMOVED_CONTROL		10
CONST_INT CASINO_CPM_BS_SET_DEFAULT_OPTION	11
CONST_INT CASINO_CPM_BS_SET_DEFAULT_ROOM	12
CONST_INT CASINO_CPM_BS_SET_IN_CUTSCENE		13
CONST_INT CASINO_CPM_BS_LOADED_MENU_HEADER	14
CONST_INT CASINO_CPM_BS_RESTORE_LAST_ITEM 	15
CONST_INT CASINO_CPM_BS_CHIP_TRANS_PEND 	16
CONST_INT CASINO_CPM_BS_CASH_TRANS_PEND 	17
CONST_INT CASINO_CPM_BS_TRANSACTION_FAILED	18
CONST_INT CASINO_CPM_BS_CLEAR_TRANS_STATUS	19
CONST_INT CASINO_CPM_BS_HAS_SEC_MENU_PAGE	20
CONST_INT CASINO_CPM_BS_USING_SEC_MENU_PAGE	21
CONST_INT CASINO_CPM_BS_PRESSED_X			22
CONST_INT CASINO_CPM_BS_PRESSED_Y			23
CONST_INT CASINO_CPM_BS_PRESSED_RS			24
CONST_INT CASINO_CPM_BS_SET_CUR_POSITIONS	25
#IF IS_DEBUG_BUILD
CONST_INT CASINO_CPM_BS_DRAW_DEBUG_MENU_INFO 26
#ENDIF




CONST_INT CASINO_CPM_AREA_ENTRANCE_HALL		0
CONST_INT CASINO_CPM_AREA_MASTER_BED		1
CONST_INT CASINO_CPM_AREA_SPARE_BED			2
CONST_INT CASINO_CPM_AREA_LOUNGE			3
CONST_INT CASINO_CPM_AREA_DEALER_ROOM		4
CONST_INT CASINO_CPM_AREA_OFFICE			5
CONST_INT CASINO_CPM_AREA_MEDIA_ROOM		6
CONST_INT CASINO_CPM_AREA_BAR				7
CONST_INT CASINO_CPM_AREA_SPA				8

CONST_INT CASINO_CPM_AREA_MAX				9


CONST_INT CASINO_CPM_LOC_MASTER_BED			0
CONST_INT CASINO_CPM_LOC_MASTER_BED_LOUNGE	1
CONST_INT CASINO_CPM_LOC_MASTER_BED_SHELF	2
CONST_INT CASINO_CPM_LOC_ENTRANCE_HALL		3
CONST_INT CASINO_CPM_LOC_SPARE_BED			4
CONST_INT CASINO_CPM_LOC_LOUNGE_SHELF1		5
CONST_INT CASINO_CPM_LOC_LOUNGE1			6
CONST_INT CASINO_CPM_LOC_LOUNGE_SHELF2		7
CONST_INT CASINO_CPM_LOC_LOUNGE2			8
CONST_INT CASINO_CPM_LOC_LOUNGE_BOARDROOM	9
CONST_INT CASINO_CPM_LOC_LOUNGE3			10
CONST_INT CASINO_CPM_LOC_DEALER_ROOM_SHELF	11
CONST_INT CASINO_CPM_LOC_DEALER_ROOM		12
CONST_INT CASINO_CPM_LOC_MEDIA_ROOM			13
CONST_INT CASINO_CPM_LOC_OFFICE1			14
CONST_INT CASINO_CPM_LOC_OFFICE_SHELF1		15
CONST_INT CASINO_CPM_LOC_OFFICE2			16
CONST_INT CASINO_CPM_LOC_OFFICE_SHELF2		17
CONST_INT CASINO_CPM_LOC_SPA				18
CONST_INT CASINO_CPM_LOC_BAR				19

CONST_INT CASINO_CPM_ROOM_MAX				20


//Customisation ITEMS
CONST_INT CASINO_CPM_NOTHING		0

CONST_INT MAX_CPM_ITEM_OPTIONS		125

STRUCT CASINO_CPM_AREA_DATA
	TEXT_LABEL_15 tlName
	TEXT_LABEL_15 tlTitle
	INT iAreaID
ENDSTRUCT

STRUCT CASINO_CPM_ROOM_DATA
	TEXT_LABEL_15 tlName
	INT iMaxPostions
	INT iRoomID
	INT iAreaID
	INT iRoomKey
	INT iPositions[CASINO_CPM_MAX_ROOM_POINTS]
	BOOL bOwned
	BOOL bGroupedItems
ENDSTRUCT

STRUCT CASINO_CPM_POSITION_DATA
	INT iType
	VECTOR vObjLoc
	VECTOR vObjRot
	VECTOR vCamLoc
	VECTOR vCamRot
	FLOAT fCamFOV
	FLOAT fMaxX
	FLOAT fMaxY
	FLOAT fMinX
	FLOAT fMinY
	INT iRoomKey
ENDSTRUCT

STRUCT CASINO_CPM 
	INT iBS
	
	INT iLevel
	
	INT iCurrentSelection[CASINO_CPM_LEVEL_MAX]
	INT iCurrentSelection_Top[CASINO_CPM_LEVEL_MAX]
	INT iMaxSelection
	
	INT iSelectionIDs[CASINO_CPM_LEVEL_MAX][MAX_CPM_ITEM_OPTIONS]
	INT iSelectionToRestore
	SCRIPT_TIMER inputDelay
	
	INT iCPSelection
	INT iRoomPosSelection
	
	INT iItemSecondPageStart

	OBJECT_INDEX currentObj
	
	CAMERA_INDEX theCam
	INT iCurrentCamPos = -1
	
	INTERIOR_INSTANCE_INDEX interiorID
	CASINO_CPM_ROOM_DATA roomData
	CASINO_CPM_AREA_DATA areaData
	CASINO_CPM_POSITION_DATA posData

	GENERIC_TRANSACTION_STATE removeChipTransactionState
	INT iRemoveCashTransactionState
	
	INT iOwnedDecorations[MAX_CASINO_APT_DECOR_POS]
ENDSTRUCT

PROC CLEAR_CASINO_CPM_TRANSACTION(CASINO_CPM &menu)
	CLEAR_BIT(menu.iBS,CASINO_CPM_BS_CHIP_TRANS_PEND)
	CLEAR_BIT(menu.iBS,CASINO_CPM_BS_CASH_TRANS_PEND)
	CLEAR_BIT(menu.iBS,CASINO_CPM_BS_CONFIRMATION)
	CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
	menu.removeChipTransactionState = TRANSACTION_STATE_DEFAULT
	menu.iRemoveCashTransactionState = GENERIC_TRANSACTION_STATE_DEFAULT
ENDPROC

PROC CLEANUP_CASINO_CPM(CASINO_CPM &menu)
	INT i, j
	IF IS_BIT_SET(menu.iBS,CASINO_CPM_BS_SET_IN_CUTSCENE)
		CLEANUP_MP_CUTSCENE()
		CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SET_IN_CUTSCENE)
	ENDIF
	IF IS_BIT_SET(menu.iBS,CASINO_CPM_BS_REMOVED_CONTROL)
		NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		DISPLAY_RADAR(TRUE)
		CLEAR_BIT(menu.iBS,CASINO_CPM_BS_REMOVED_CONTROL)
	ENDIF	
	IF DOES_CAM_EXIST(menu.theCam)
		RENDER_SCRIPT_CAMS(FALSE,FALSE)
		DESTROY_CAM(menu.theCam)
		menu.iCurrentCamPos  = -1
	ENDIF
	IF DOES_ENTITY_EXIST(menu.currentObj)
		DELETE_OBJECT(menu.currentObj)
	ENDIF
	REPEAT CASINO_CPM_LEVEL_MAX i
		menu.iCurrentSelection[i] = 0
		menu.iCurrentSelection_Top[i] = 0
		REPEAT MAX_CPM_ITEM_OPTIONS j
			menu.iSelectionIDs[i][j] = 0
		ENDREPEAT
	ENDREPEAT
	
	IF USE_SERVER_TRANSACTIONS()
		IF menu.removeChipTransactionState != TRANSACTION_STATE_DEFAULT
		OR menu.iRemoveCashTransactionState != GENERIC_TRANSACTION_STATE_DEFAULT
			DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
			menu.removeChipTransactionState = TRANSACTION_STATE_DEFAULT
			menu.iRemoveCashTransactionState = GENERIC_TRANSACTION_STATE_DEFAULT
		ENDIF
	ENDIF 
	
	menu.iLevel = 0
	menu.iItemSecondPageStart = 0
	menu.iSelectionToRestore = 0
	menu.iCPSelection = 0
	menu.iMaxSelection =  0
	CLEAR_CASINO_CPM_TRANSACTION(menu)
	menu.iBS = 0

	IF IS_BIT_SET(menu.iBS,CASINO_CPM_BS_LOADED_MENU_HEADER)
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("ShopUI_Title_Casino")
		CLEAR_BIT(menu.iBS,CASINO_CPM_BS_LOADED_MENU_HEADER)
	ENDIF


	CLEANUP_MENU_ASSETS()
	PRINTLN("CLEANUP_CASINO_CPM: called this frame")
ENDPROC

PROC GET_CPM_POSITION_DETAILS(CASINO_CPM_POSITION_DATA &posData,INT iPosition)
	posData.vObjLoc = <<0,0,0>>
	posData.vObjRot = <<0,0,0>>
	posData.vCamLoc = <<0,0,0>>
	posData.vCamRot = <<0,0,0>>
	posData.fCamFOV = 0
	posData.iType = 0
	posData.fMaxX = 0
	posData.fMaxY = 0

	SWITCH iPosition
		CASE CASINO_CPM_POS_1
			posData.vObjLoc = <<972.89, 75.422, 115.669>>
			posData.vObjRot = <<0.0, 0.0, 162.983>>
			posData.vCamLoc = <<974.019, 76.6581, 116.497>>
			posData.vCamRot = <<-22.8255, 1.7267, 126.708>>
			posData.fCamFOV = 59.1652
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_MAIN_BEDROOM)
		BREAK
		CASE CASINO_CPM_POS_2
			posData.vObjLoc = <<970.923, 76.145, 117.18>>
			posData.vObjRot = <<0.0, 0.0, 147.995>>
			posData.vCamLoc = <<973.656, 78.7901, 117.559>>
			posData.vCamRot = <<-5.7666, 0.0, 137.449>>
			posData.fCamFOV = 57.7689
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 3.0
			posData.fMaxY = 1.45
			posData.fMinX = 0.8
			posData.fMinY = 0.8
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_MAIN_BEDROOM)
		BREAK
		CASE CASINO_CPM_POS_3
			posData.vObjLoc = <<969.485, 77.573, 115.669>>
			posData.vObjRot = <<0.0, 0.0, 142.983>>
			posData.vCamLoc = <<969.565, 79.2408, 116.033>>
			posData.vCamRot = <<-4.3199, -0.0701, 179.63>>
			posData.fCamFOV = 44.6674
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_MAIN_BEDROOM)
		BREAK
		CASE CASINO_CPM_POS_4
			posData.vObjLoc = <<977.617, 80.677, 117.091>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<975.069, 79.0679, 117.35>>
			posData.vCamRot = <<-11.9538, 0.0009, -57.4871>>
			posData.fCamFOV = 55.6312
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 2.5
			posData.fMaxY = 1.3
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_MAIN_BEDROOM)
		BREAK
		CASE CASINO_CPM_POS_5
			posData.vObjLoc = <<977.666, 79.989, 116.11>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<977.205, 77.8024, 117.017>>
			posData.vCamRot = <<-22.9321, 0.0354, -11.7942>>
			posData.fCamFOV = 58.3449
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_MAIN_BEDROOM)
		BREAK
		CASE CASINO_CPM_POS_6
			posData.vObjLoc = <<978.122, 79.66, 116.11>>
			posData.vObjRot = <<0.0, 0.0, -42.0283>>
			posData.vCamLoc = <<976.12, 78.3051, 117.314>>
			posData.vCamRot = <<-21.0317, 0.0354, -58.6701>>
			posData.fCamFOV = 58.3449
			posData.iType = CPM_TYPE_MED_DESK_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_MAIN_BEDROOM)
		BREAK
		CASE CASINO_CPM_POS_7
			posData.vObjLoc = <<977.807, 70.619, 116.112>>
			posData.vObjRot = <<0.0, 0.0, -17.0169>>
			posData.vCamLoc = <<975.715, 69.2903, 117.223>>
			posData.vCamRot = <<-14.9288, 0.003, -63.8279>>
			posData.fCamFOV = 55.6312
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_PRIV_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_8
			posData.vObjLoc = <<978.638, 70.519, 116.955>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<977.306, 68.5211, 117.039>>
			posData.vCamRot = <<-1.7774, 0.003, -32.5045>>
			posData.fCamFOV = 62.9546
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.82
			posData.fMaxY = 1.5
			posData.fMinX = 0.7
			posData.fMinY = 0.7
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_PRIV_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_9
			posData.vObjLoc = <<979.1, 69.788, 116.112>>
			posData.vObjRot = <<0.0, 0.0, -41.975>>
			posData.vCamLoc = <<978.716, 67.2124, 116.904>>
			posData.vCamRot = <<-10.2274, 0.003, -8.7777>>
			posData.fCamFOV = 45.3752
			posData.iType = CPM_TYPE_MED_DESK_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_PRIV_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_10
			posData.vObjLoc = <<971.612, 71.143, 116.214>>
			posData.vObjRot = <<0.0, 0.0, 172.983>>
			posData.vCamLoc = <<968.509, 75.0142, 118.0>>
			posData.vCamRot = <<-15.7955, 0.003, -137.227>>
			posData.fCamFOV = 45.3752
			posData.iType = CPM_TYPE_MED_DESK_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_PRIV_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_11
			posData.vObjLoc = <<972.319, 68.564, 117.312>>
			posData.vObjRot = <<0.0, 0.0, -127.025>>
			posData.vCamLoc = <<969.887, 69.6879, 117.382>>
			posData.vCamRot = <<-9.6158, 0.0584, -123.398>>
			posData.fCamFOV = 76.3761
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_PRIV_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_12
			posData.vObjLoc = <<972.08, 68.182, 117.312>>
			posData.vObjRot = <<0.0, 0.0, -116.998>>
			posData.vCamLoc = <<969.887, 69.6879, 117.382>>
			posData.vCamRot = <<-9.6158, 0.0584, -123.398>>
			posData.fCamFOV = 76.3761
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_PRIV_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_13
			posData.vObjLoc = <<971.861, 67.832, 117.312>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<969.887, 69.6879, 117.382>>
			posData.vCamRot = <<-9.6158, 0.0584, -123.398>>
			posData.fCamFOV = 76.3761
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_PRIV_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_14
			posData.vObjLoc = <<972.308, 68.548, 116.648>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<969.887, 69.6879, 117.382>>
			posData.vCamRot = <<-9.6158, 0.0584, -123.398>>
			posData.fCamFOV = 76.3761
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_PRIV_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_15
			posData.vObjLoc = <<972.09, 68.198, 116.648>>
			posData.vObjRot = <<0.0, 0.0, -116.998>>
			posData.vCamLoc = <<969.887, 69.6879, 117.382>>
			posData.vCamRot = <<-9.6158, 0.0584, -123.398>>
			posData.fCamFOV = 76.3761
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_PRIV_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_16
			posData.vObjLoc = <<971.871, 67.848, 116.648>>
			posData.vObjRot = <<0.0, 0.0, -132.01>>
			posData.vCamLoc = <<969.887, 69.6879, 117.382>>
			posData.vCamRot = <<-9.6158, 0.0584, -123.398>>
			posData.fCamFOV = 76.3761
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_PRIV_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_17
			posData.vObjLoc = <<972.297, 68.563, 115.984>>
			posData.vObjRot = <<0.0, 0.0, -116.998>>
			posData.vCamLoc = <<969.887, 69.6879, 117.382>>
			posData.vCamRot = <<-9.6158, 0.0584, -123.398>>
			posData.fCamFOV = 76.3761
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_PRIV_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_18
			posData.vObjLoc = <<972.069, 68.208, 115.984>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<969.887, 69.6879, 117.382>>
			posData.vCamRot = <<-9.6158, 0.0584, -123.398>>
			posData.fCamFOV = 76.3761
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_PRIV_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_19
			posData.vObjLoc = <<971.834, 67.832, 115.984>>
			posData.vObjRot = <<0.0, 0.0, -127.025>>
			posData.vCamLoc = <<969.887, 69.6879, 117.382>>
			posData.vCamRot = <<-9.6158, 0.0584, -123.398>>
			posData.fCamFOV = 76.3761
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_PRIV_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_20
			posData.vObjLoc = <<979.919, 64.472, 115.164>>
			posData.vObjRot = <<0.0, 0.0, 132.995>>
			posData.vCamLoc = <<984.75, 65.2924, 116.588>>
			posData.vCamRot = <<-6.6779, -0.0565, 95.7379>>
			posData.fCamFOV = 53.409
			posData.iType = CPM_TYPE_LARGE_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_21
			posData.vObjLoc = <<982.055, 67.446, 115.164>>
			posData.vObjRot = <<0.0, 0.0, 13.0061>>
			posData.vCamLoc = <<983.19, 64.4831, 116.655>>
			posData.vCamRot = <<-15.625, -0.0565, 16.7912>>
			posData.fCamFOV = 53.409
			posData.iType = CPM_TYPE_MEDIUM_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_22
			posData.vObjLoc = <<983.944, 67.477, 116.794>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<982.42, 65.1233, 116.969>>
			posData.vCamRot = <<-6.76, -0.0565, -32.705>>
			posData.fCamFOV = 53.409
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 3.0
			posData.fMaxY = 1.4
			posData.fMinX = 0.7
			posData.fMinY = 0.7
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_23
			posData.vObjLoc = <<983.812, 67.266, 116.064>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<982.686, 65.5252, 116.617>>
			posData.vCamRot = <<-12.6135, -0.0565, -32.8895>>
			posData.fCamFOV = 53.409
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_24
			posData.vObjLoc = <<984.327, 66.941, 116.064>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<983.697, 64.9609, 116.642>>
			posData.vCamRot = <<-9.2369, -0.0565, -23.4763>>
			posData.fCamFOV = 53.409
			posData.iType = CPM_TYPE_MED_DESK_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_25
			posData.vObjLoc = <<985.123, 65.857, 115.164>>
			posData.vObjRot = <<0.0, 0.0, -72.0208>>
			posData.vCamLoc = <<981.634, 64.6042, 116.488>>
			posData.vCamRot = <<-9.3033, -0.0565, -70.9653>>
			posData.fCamFOV = 53.409
			posData.iType = CPM_TYPE_MEDIUM_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_26
			posData.vObjLoc = <<983.023, 62.532, 115.164>>
			posData.vObjRot = <<0.0, 0.0, 162.995>>
			posData.vCamLoc = <<982.988, 67.4012, 117.13>>
			posData.vCamRot = <<-14.7343, -0.0565, -179.238>>
			posData.fCamFOV = 53.409
			posData.iType = CPM_TYPE_LARGE_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_27
			posData.vObjLoc = <<978.289, 58.079, 115.164>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<974.183, 54.6514, 118.026>>
			posData.vCamRot = <<-22.418, -0.0565, -50.925>>
			posData.fCamFOV = 53.409
			posData.iType = CPM_TYPE_LARGE_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_28
			posData.vObjLoc = <<977.364, 53.649, 116.794>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<975.079, 54.8328, 117.352>>
			posData.vCamRot = <<-12.7041, -0.0565, -117.261>>
			posData.fCamFOV = 53.409
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 2.0
			posData.fMaxY = 2.0
			posData.fMinX = 0.7
			posData.fMinY = 0.7
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_29
			posData.vObjLoc = <<974.378, 55.468, 116.794>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<976.9505, 53.6830, 117.0355>>
			posData.vCamRot = <<-3.6572, -0.1167, 49.3032>>
			posData.fCamFOV = 48.7030
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 2.0
			posData.fMaxY = 2.0
			posData.fMinX = 0.7
			posData.fMinY = 0.7
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_30
			posData.vObjLoc = <<974.77, 58.94, 116.794>>
			posData.vObjRot = <<0.0, 0.0, 147.995>>
			posData.vCamLoc = <<976.415, 60.3119, 117.035>>
			posData.vCamRot = <<-8.1083, -0.0565, 128.367>>
			posData.fCamFOV = 54.0709
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.0
			posData.fMaxY = 1.5
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_31
			posData.vObjLoc = <<973.801, 59.546, 116.054>>
			posData.vObjRot = <<0.0, 0.0, 147.995>>
			posData.vCamLoc = <<975.256, 61.151, 116.418>>
			posData.vCamRot = <<-9.9256, -0.0565, 136.018>>
			posData.fCamFOV = 54.0709
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.0
			posData.fMaxY = 1.5
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_32
			posData.vObjLoc = <<972.833, 60.151, 115.325>>
			posData.vObjRot = <<0.0, 0.0, 147.995>>
			posData.vCamLoc = <<974.04, 61.8887, 115.651>>
			posData.vCamRot = <<-9.9875, -0.0565, 146.813>>
			posData.fCamFOV = 54.0709
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.0
			posData.fMaxY = 1.5
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_33
			posData.vObjLoc = <<971.864, 60.756, 114.597>>
			posData.vObjRot = <<0.0, 0.0, 147.995>>
			posData.vCamLoc = <<972.76, 62.4265, 114.791>>
			posData.vCamRot = <<-7.8395, -0.0565, 151.931>>
			posData.fCamFOV = 54.0709
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.0
			posData.fMaxY = 1.5
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_34
			posData.vObjLoc = <<970.896, 61.361, 113.868>>
			posData.vObjRot = <<0.0, 0.0, 147.995>>
			posData.vCamLoc = <<972.033, 63.167, 113.772>>
			posData.vCamRot = <<-0.0567, -0.0565, 148.382>>
			posData.fCamFOV = 54.0709
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.0
			posData.fMaxY = 1.5
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_35
			posData.vObjLoc = <<972.249, 63.526, 113.868>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<971.11, 61.6505, 113.974>>
			posData.vCamRot = <<-1.991, -0.0565, -32.1654>>
			posData.fCamFOV = 54.0709
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.0
			posData.fMaxY = 1.5
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_36
			posData.vObjLoc = <<973.218, 62.921, 114.597>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<972.159, 61.0846, 114.524>>
			posData.vCamRot = <<2.1129, -0.0565, -30.27>>
			posData.fCamFOV = 54.0709
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.0
			posData.fMaxY = 1.5
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_37
			posData.vObjLoc = <<974.186, 62.316, 115.325>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<973.145, 60.465, 115.45>>
			posData.vCamRot = <<-3.2824, -0.0565, -28.1869>>
			posData.fCamFOV = 54.0709
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.0
			posData.fMaxY = 1.5
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_38
			posData.vObjLoc = <<975.155, 61.711, 116.054>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<974.046, 59.9679, 115.944>>
			posData.vCamRot = <<2.014, -0.0565, -31.5712>>
			posData.fCamFOV = 54.0709
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.0
			posData.fMaxY = 1.5
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_39
			posData.vObjLoc = <<976.134, 61.123, 116.794>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<974.814, 59.1076, 116.998>>
			posData.vCamRot = <<-3.9211, -0.0565, -32.3032>>
			posData.fCamFOV = 54.0709
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.0
			posData.fMaxY = 1.5
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
		BREAK
		CASE CASINO_CPM_POS_40
			posData.vObjLoc = <<986.286, 65.593, 116.794>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<988.566, 64.1055, 116.882>>
			posData.vCamRot = <<-1.1441, -0.0565, 55.6714>>
			posData.fCamFOV = 45.0
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 0.8
			posData.fMaxY = 2.0
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_SPARE_BEDROOM)
		BREAK
		CASE CASINO_CPM_POS_41
			posData.vObjLoc = <<985.821, 67.31, 116.794>>
			posData.vObjRot = <<0.0, 0.0, 147.995>>
			posData.vCamLoc = <<987.189, 69.1879, 117.032>>
			posData.vCamRot = <<-6.3044, -0.109, 149.585>>
			posData.fCamFOV = 45.0
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.6
			posData.fMaxY = 2.2
			posData.fMinX = 0.7
			posData.fMinY = 0.7
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_SPARE_BEDROOM)
		BREAK
		CASE CASINO_CPM_POS_42
			posData.vObjLoc = <<984.552, 68.212, 116.11>>
			posData.vObjRot = <<0.0, 0.0, 107.972>>
			posData.vCamLoc = <<985.416, 70.4581, 116.881>>
			posData.vCamRot = <<-16.5095, -0.097, 160.864>>
			posData.fCamFOV = 45.0
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_SPARE_BEDROOM)
		BREAK
		CASE CASINO_CPM_POS_43
			posData.vObjLoc = <<983.527, 68.859, 116.11>>
			posData.vObjRot = <<0.0, 0.0, 147.995>>
			posData.vCamLoc = <<985.879, 70.5096, 116.885>>
			posData.vCamRot = <<-13.0318, -0.0828, 123.807>>
			posData.fCamFOV = 45.0
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_SPARE_BEDROOM)
		BREAK
		CASE CASINO_CPM_POS_44
			posData.vObjLoc = <<983.091, 70.014, 116.86>>
			posData.vObjRot = <<0.0, 0.0, 58.0>>
			posData.vCamLoc = <<985.42, 69.4621, 117.009>>
			posData.vCamRot = <<-3.9235, -0.042, 79.1511>>
			posData.fCamFOV = 55.3152
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 0.9
			posData.fMaxY = 1.3
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_SPARE_BEDROOM)
		BREAK
		CASE CASINO_CPM_POS_45
			posData.vObjLoc = <<988.328, 73.87, 116.998>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<986.599, 71.2304, 116.982>>
			posData.vCamRot = <<-1.2008, -0.0828, -33.2929>>
			posData.fCamFOV = 55.3152
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 2.3
			posData.fMaxY = 1.15
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_SPARE_BEDROOM)
		BREAK
		CASE CASINO_CPM_POS_46
			posData.vObjLoc = <<988.966, 72.889, 115.669>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<987.863, 71.1841, 116.308>>
			posData.vCamRot = <<-11.6119, -0.0828, -33.7351>>
			posData.fCamFOV = 45.3152
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_SPARE_BEDROOM)
		BREAK
		CASE CASINO_CPM_POS_47
			posData.vObjLoc = <<989.659, 72.457, 115.669>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<988.324, 71.0739, 116.195>>
			posData.vCamRot = <<-7.7511, -0.0828, -44.9283>>
			posData.fCamFOV = 50.0803
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_SPARE_BEDROOM)
		BREAK
		CASE CASINO_CPM_POS_48
			posData.vObjLoc = <<990.814, 72.317, 116.998>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<989.314, 69.8132, 116.951>>
			posData.vCamRot = <<-0.485, -0.0828, -32.3578>>
			posData.fCamFOV = 55.2838
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 2.3
			posData.fMaxY = 1.15
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_SPARE_BEDROOM)
		BREAK
		CASE CASINO_CPM_POS_49
			posData.vObjLoc = <<989.597, 66.768, 116.11>>
			posData.vObjRot = <<0.0, 0.0, -116.998>>
			posData.vCamLoc = <<988.469, 68.5803, 116.564>>
			posData.vCamRot = <<-6.263, -0.0828, -148.521>>
			posData.fCamFOV = 55.2838
			posData.iType = CPM_TYPE_MED_DESK_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_SPARE_BEDROOM)
		BREAK
		CASE CASINO_CPM_POS_50
			posData.vObjLoc = <<975.687, 50.802, 117.62>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_51
			posData.vObjLoc = <<975.465, 50.446, 117.62>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_52
			posData.vObjLoc = <<975.244, 50.091, 117.62>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_54
			posData.vObjLoc = <<975.021, 49.736, 117.62>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_55
			posData.vObjLoc = <<974.799, 49.381, 117.62>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_56
			posData.vObjLoc = <<974.577, 49.025, 117.62>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_57
			posData.vObjLoc = <<974.355, 48.67, 117.62>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_58
			posData.vObjLoc = <<974.134, 48.315, 117.62>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_59
			posData.vObjLoc = <<973.911, 47.959, 117.62>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_60
			posData.vObjLoc = <<975.687, 50.802, 116.852>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_61
			posData.vObjLoc = <<975.465, 50.446, 116.852>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_62
			posData.vObjLoc = <<975.244, 50.091, 116.852>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_63
			posData.vObjLoc = <<975.021, 49.736, 116.852>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_64
			posData.vObjLoc = <<974.799, 49.381, 116.852>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_65
			posData.vObjLoc = <<974.577, 49.025, 116.852>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_66
			posData.vObjLoc = <<974.355, 48.67, 116.852>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_67
			posData.vObjLoc = <<974.134, 48.315, 116.852>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_68
			posData.vObjLoc = <<973.911, 47.959, 116.852>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_69
			posData.vObjLoc = <<975.687, 50.802, 116.11>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_70
			posData.vObjLoc = <<975.465, 50.446, 116.11>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_71
			posData.vObjLoc = <<975.244, 50.091, 116.11>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_72
			posData.vObjLoc = <<975.021, 49.736, 116.11>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_73
			posData.vObjLoc = <<974.799, 49.381, 116.11>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_74
			posData.vObjLoc = <<974.577, 49.025, 116.11>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_75
			posData.vObjLoc = <<974.355, 48.67, 116.11>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_76
			posData.vObjLoc = <<974.134, 48.315, 116.11>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_77
			posData.vObjLoc = <<973.911, 47.959, 116.11>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<971.6, 51.5539, 117.445>>
			posData.vCamRot = <<-5.2658, -0.0726, -122.858>>
			posData.fCamFOV = 61.2159
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_78
			posData.vObjLoc = <<971.084, 47.4, 116.794>>
			posData.vObjRot = <<0.0, 0.0, 147.995>>
			posData.vCamLoc = <<972.879, 50.4198, 117.203>>
			posData.vCamRot = <<-6.1704, -0.126, 148.051>>
			posData.fCamFOV = 51.6063
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 2.4
			posData.fMaxY = 2.4
			posData.fMinX = 0.8
			posData.fMinY = 0.8
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_79
			posData.vObjLoc = <<971.84, 48.189, 115.164>>
			posData.vObjRot = <<0.0, 0.0, -167.017>>
			posData.vCamLoc = <<969.825, 49.9774, 116.318>>
			posData.vCamRot = <<-12.4479, -0.126, -143.604>>
			posData.fCamFOV = 55.2838
			posData.iType = CPM_TYPE_MEDIUM_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_80
			posData.vObjLoc = <<968.751, 44.969, 116.844>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<965.009, 41.9558, 116.885>>
			posData.vCamRot = <<-1.3515, -0.0315, -50.9533>>
			posData.fCamFOV = 55.2838
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.21
			posData.fMaxY = 2.8
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_81
			posData.vObjLoc = <<970.873, 43.642, 116.945>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<968.835, 41.6347, 117.077>>
			posData.vCamRot = <<-3.9782, -0.0315, -43.5658>>
			posData.fCamFOV = 55.2838
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.86
			posData.fMaxY = 1.3
			posData.fMinX = 1.0
			posData.fMinY = 1.0
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_82
			posData.vObjLoc = <<967.683, 38.698, 117.189>>
			posData.vObjRot = <<0.0, 0.0, 147.995>>
			posData.vCamLoc = <<968.502, 40.765, 117.111>>
			posData.vCamRot = <<-0.893, -0.0097, 149.745>>
			posData.fCamFOV = 55.2838
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.5
			posData.fMaxY = 2.2
			posData.fMinX = 1.0
			posData.fMinY = 1.0
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_83
			posData.vObjLoc = <<964.849, 39.558, 116.11>>
			posData.vObjRot = <<0.0, 0.0, -86.975>>
			posData.vCamLoc = <<962.355, 39.5667, 117.518>>
			posData.vCamRot = <<-20.7427, -0.0153, -92.5206>>
			posData.fCamFOV = 55.2838
			posData.iType = CPM_TYPE_MED_DESK_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_84
			posData.vObjLoc = <<965.774, 37.294, 115.164>>
			posData.vObjRot = <<0.0, 0.0, -92.017>>
			posData.vCamLoc = <<963.231, 36.11, 116.667>>
			posData.vCamRot = <<-19.4189, -0.0153, -68.0959>>
			posData.fCamFOV = 55.2838
			posData.iType = CPM_TYPE_MEDIUM_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_85
			posData.vObjLoc = <<969.016, 36.263, 117.478>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<967.525, 33.5248, 117.094>>
			posData.vCamRot = <<-0.5924, -0.0233, -46.6057>>
			posData.fCamFOV = 72.8781
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_86
			posData.vObjLoc = <<969.44, 35.997, 117.478>>
			posData.vObjRot = <<0.0, 0.0, -37.0283>>
			posData.vCamLoc = <<967.525, 33.5248, 117.094>>
			posData.vCamRot = <<-0.5924, -0.0233, -46.6057>>
			posData.fCamFOV = 72.8781
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_87
			posData.vObjLoc = <<969.903, 35.709, 117.478>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<967.525, 33.5248, 117.094>>
			posData.vCamRot = <<-0.5924, -0.0233, -46.6057>>
			posData.fCamFOV = 72.8781
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_88
			posData.vObjLoc = <<970.35, 35.43, 117.478>>
			posData.vObjRot = <<0.0, 0.0, -33.0283>>
			posData.vCamLoc = <<967.525, 33.5248, 117.094>>
			posData.vCamRot = <<-0.5924, -0.0233, -46.6057>>
			posData.fCamFOV = 72.8781
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_89
			posData.vObjLoc = <<970.803, 35.147, 117.478>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<967.525, 33.5248, 117.094>>
			posData.vCamRot = <<-0.5924, -0.0233, -46.6057>>
			posData.fCamFOV = 72.8781
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_90
			posData.vObjLoc = <<968.985, 36.283, 116.789>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<967.525, 33.5248, 117.094>>
			posData.vCamRot = <<-0.5924, -0.0233, -46.6057>>
			posData.fCamFOV = 72.8781
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_91
			posData.vObjLoc = <<969.446, 35.993, 116.789>>
			posData.vObjRot = <<0.0, 0.0, -27.0283>>
			posData.vCamLoc = <<967.525, 33.5248, 117.094>>
			posData.vCamRot = <<-0.5924, -0.0233, -46.6057>>
			posData.fCamFOV = 72.8781
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_92
			posData.vObjLoc = <<969.907, 35.706, 116.789>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<967.525, 33.5248, 117.094>>
			posData.vCamRot = <<-0.5924, -0.0233, -46.6057>>
			posData.fCamFOV = 72.8781
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_93
			posData.vObjLoc = <<970.336, 35.438, 116.789>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<967.525, 33.5248, 117.094>>
			posData.vCamRot = <<-0.5924, -0.0233, -46.6057>>
			posData.fCamFOV = 72.8781
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_94
			posData.vObjLoc = <<970.803, 35.147, 116.789>>
			posData.vObjRot = <<0.0, 0.0, -34.0283>>
			posData.vCamLoc = <<967.525, 33.5248, 117.094>>
			posData.vCamRot = <<-0.5924, -0.0233, -46.6057>>
			posData.fCamFOV = 72.8781
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_95
			posData.vObjLoc = <<969.016, 36.263, 116.074>>
			posData.vObjRot = <<0.0, 0.0, -37.0283>>
			posData.vCamLoc = <<967.525, 33.5248, 117.094>>
			posData.vCamRot = <<-0.5924, -0.0233, -46.6057>>
			posData.fCamFOV = 72.8781
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_96
			posData.vObjLoc = <<969.44, 35.997, 116.074>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<967.525, 33.5248, 117.094>>
			posData.vCamRot = <<-0.5924, -0.0233, -46.6057>>
			posData.fCamFOV = 72.8781
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_97
			posData.vObjLoc = <<969.897, 35.712, 116.074>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<967.525, 33.5248, 117.094>>
			posData.vCamRot = <<-0.5924, -0.0233, -46.6057>>
			posData.fCamFOV = 72.8781
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_98
			posData.vObjLoc = <<970.35, 35.43, 116.074>>
			posData.vObjRot = <<0.0, 0.0, -37.0283>>
			posData.vCamLoc = <<967.525, 33.5248, 117.094>>
			posData.vCamRot = <<-0.5924, -0.0233, -46.6057>>
			posData.fCamFOV = 72.8781
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_99
			posData.vObjLoc = <<970.803, 35.147, 116.074>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<967.525, 33.5248, 117.094>>
			posData.vCamRot = <<-0.5924, -0.0233, -46.6057>>
			posData.fCamFOV = 72.8781
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_100
			posData.vObjLoc = <<965.231, 28.217, 116.943>>
			posData.vObjRot = <<0.0, 0.0, 147.995>>
			posData.vCamLoc = <<966.536, 32.3847, 117.179>>
			posData.vCamRot = <<-2.8336, 0.4086, 163.043>>
			posData.fCamFOV = 48.1467
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 2.4
			posData.fMaxY = 1.5
			posData.fMinX = 0.7
			posData.fMinY = 0.7
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_101
			posData.vObjLoc = <<961.094, 27.632, 116.844>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<959.265, 28.9091, 117.282>>
			posData.vCamRot = <<-12.5129, -0.1541, -124.346>>
			posData.fCamFOV = 55.2838
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.79
			posData.fMaxY = 2.8
			posData.fMinX = 0.7
			posData.fMinY = 0.7
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_102
			posData.vObjLoc = <<956.916, 27.872, 116.844>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<960.6612, 27.5615, 116.9766>>
			posData.vCamRot = <<-1.4225, -0.0732, 89.5100>>
			posData.fCamFOV = 55.2838
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 2.2
			posData.fMaxY = 2.8
			posData.fMinX = 0.8
			posData.fMinY = 0.8
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_103
			posData.vObjLoc = <<958.387, 30.227, 116.844>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<960.8117, 27.6069, 117.2826>>
			posData.vCamRot = <<-6.5523, 0.0569, 39.8603>>
			posData.fCamFOV = 55.2838
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 2.2
			posData.fMaxY = 2.8
			posData.fMinX = 0.8
			posData.fMinY = 0.8
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_104
			posData.vObjLoc = <<959.324, 32.322, 117.798>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_105
			posData.vObjLoc = <<959.649, 32.868, 117.798>>
			posData.vObjRot = <<0.0, 0.0, 62.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_106
			posData.vObjLoc = <<959.993, 33.394, 117.798>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_107
			posData.vObjLoc = <<960.321, 33.918, 117.798>>
			posData.vObjRot = <<0.0, 0.0, 52.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_108
			posData.vObjLoc = <<960.654, 34.453, 117.798>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_109
			posData.vObjLoc = <<961.0, 34.968, 117.798>>
			posData.vObjRot = <<0.0, 0.0, 52.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_110
			posData.vObjLoc = <<961.323, 35.523, 117.798>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_111
			posData.vObjLoc = <<961.655, 36.054, 117.798>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_112
			posData.vObjLoc = <<959.324, 32.322, 117.228>>
			posData.vObjRot = <<0.0, 0.0, 62.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_113
			posData.vObjLoc = <<959.661, 32.861, 117.228>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_114
			posData.vObjLoc = <<959.993, 33.394, 117.228>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_115
			posData.vObjLoc = <<960.321, 33.918, 117.228>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_118
			posData.vObjLoc = <<961.323, 35.523, 117.228>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_119
			posData.vObjLoc = <<961.655, 36.054, 117.228>>
			posData.vObjRot = <<0.0, 0.0, 52.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_120
			posData.vObjLoc = <<959.324, 32.322, 116.657>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_121
			posData.vObjLoc = <<959.661, 32.861, 116.657>>
			posData.vObjRot = <<0.0, 0.0, 52.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_123
			posData.vObjLoc = <<960.321, 33.918, 116.657>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_126
			posData.vObjLoc = <<961.323, 35.523, 116.657>>
			posData.vObjRot = <<0.0, 0.0, 52.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_127
			posData.vObjLoc = <<961.655, 36.054, 116.657>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_128
			posData.vObjLoc = <<959.324, 32.322, 116.074>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_129
			posData.vObjLoc = <<959.661, 32.861, 116.074>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_130
			posData.vObjLoc = <<959.997, 33.399, 116.074>>
			posData.vObjRot = <<0.0, 0.0, 62.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_131
			posData.vObjLoc = <<960.321, 33.918, 116.074>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_132
			posData.vObjLoc = <<960.654, 34.453, 116.074>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_133
			posData.vObjLoc = <<960.983, 34.978, 116.074>>
			posData.vObjRot = <<0.0, 0.0, 62.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_134
			posData.vObjLoc = <<961.323, 35.523, 116.074>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_135
			posData.vObjLoc = <<961.655, 36.054, 116.074>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_136
			posData.vObjLoc = <<961.615, 37.612, 116.844>>
			posData.vObjRot = <<0.0, 0.0, 147.995>>
			posData.vCamLoc = <<963.014, 39.6959, 116.938>>
			posData.vCamRot = <<-5.4804, -0.0118, 147.143>>
			posData.fCamFOV = 55.2838
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.72
			posData.fMaxY = 2.5
			posData.fMinX = 0.7
			posData.fMinY = 0.7
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_137
			posData.vObjLoc = <<956.209, 40.993, 116.844>>
			posData.vObjRot = <<0.0, 0.0, 147.995>>
			posData.vCamLoc = <<957.891, 43.3248, 117.254>>
			posData.vCamRot = <<-3.047, -0.0118, 147.135>>
			posData.fCamFOV = 55.2838
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.3
			posData.fMaxY = 2.4
			posData.fMinX = 0.7
			posData.fMinY = 0.7
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_138
			posData.vObjLoc = <<956.93, 41.67, 115.164>>
			posData.vObjRot = <<0.0, 0.0, 97.9758>>
			posData.vCamLoc = <<959.67, 41.1557, 116.878>>
			posData.vCamRot = <<-22.6397, -0.0118, 71.8316>>
			posData.fCamFOV = 46.9383
			posData.iType = CPM_TYPE_MEDIUM_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_140
			posData.vObjLoc = <<958.233, 43.033, 116.074>>
			posData.vObjRot = <<0.0, 0.0, -17.0167>>
			posData.vCamLoc = <<958.277, 41.1462, 116.519>>
			posData.vCamRot = <<-6.8159, -0.0118, -6.3209>>
			posData.fCamFOV = 50.6335
			posData.iType = CPM_TYPE_MED_DESK_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_142
			posData.vObjLoc = <<959.814, 46.474, 116.94>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<961.961, 45.1183, 117.266>>
			posData.vCamRot = <<-7.941, -0.0118, 59.2902>>
			posData.fCamFOV = 50.6335
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.1
			posData.fMaxY = 2.4
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_143
			posData.vObjLoc = <<963.892, 53.295, 115.164>>
			posData.vObjRot = <<0.0, 0.0, 88.0063>>
			posData.vCamLoc = <<966.191, 50.9193, 116.597>>
			posData.vCamRot = <<-14.5992, -0.0118, 37.5638>>
			posData.fCamFOV = 54.8839
			posData.iType = CPM_TYPE_MEDIUM_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_144
			posData.vObjLoc = <<965.007, 55.146, 116.844>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<963.482, 52.7062, 117.412>>
			posData.vCamRot = <<-11.9709, -0.0118, -32.9249>>
			posData.fCamFOV = 54.8839
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.3
			posData.fMaxY = 2.4
			posData.fMinX = 0.7
			posData.fMinY = 0.7
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_145
			posData.vObjLoc = <<967.113, 49.26, 115.164>>
			posData.vObjRot = <<0.0, 0.0, 102.983>>
			posData.vCamLoc = <<972.642, 50.0844, 117.407>>
			posData.vCamRot = <<-12.9411, -0.0118, 97.1605>>
			posData.fCamFOV = 54.8839
			posData.iType = CPM_TYPE_LARGE_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_146
			posData.vObjLoc = <<970.447, 51.746, 116.844>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<969.104, 49.8271, 116.974>>
			posData.vCamRot = <<-1.0488, -0.0118, -35.0956>>
			posData.fCamFOV = 54.8839
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.7
			posData.fMaxY = 2.4
			posData.fMinX = 0.7
			posData.fMinY = 0.7
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_147
			posData.vObjLoc = <<972.395, 52.928, 116.794>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<974.903, 50.352, 117.731>>
			posData.vCamRot = <<-14.4891, -0.0833, 46.2666>>
			posData.fCamFOV = 45.0
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.9
			posData.fMaxY = 2.3
			posData.fMinX = 0.8
			posData.fMinY = 0.8
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_148
			posData.vObjLoc = <<969.967, 61.455, 116.82>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<967.962, 58.0659, 116.721>>
			posData.vCamRot = <<1.0753, -0.0118, -31.3691>>
			posData.fCamFOV = 48.2433
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.82
			posData.fMaxY = 2.4
			posData.fMinX = 0.8
			posData.fMinY = 0.8
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_MEETING_ROOM)
		BREAK
		CASE CASINO_CPM_POS_149
			posData.vObjLoc = <<971.859, 60.273, 116.82>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<969.7042, 56.4426, 117.0854>>
			posData.vCamRot = <<-3.7999, -0.0529, -31.3263>>
			posData.fCamFOV = 40.8961
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.82
			posData.fMaxY = 2.4
			posData.fMinX = 0.8
			posData.fMinY = 0.8
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_MEETING_ROOM)
		BREAK
		CASE CASINO_CPM_POS_150
			posData.vObjLoc = <<973.752, 59.09, 116.82>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<971.2895, 55.0593, 117.0341>>
			posData.vCamRot = <<-3.1963, 0.0491, -29.9048>>
			posData.fCamFOV = 40.8961
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.82
			posData.fMaxY = 2.4
			posData.fMinX = 0.8
			posData.fMinY = 0.8
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_MEETING_ROOM)
		BREAK
		CASE CASINO_CPM_POS_151
			posData.vObjLoc = <<973.994, 57.844, 115.164>>
			posData.vObjRot = <<0.0, 0.0, -72.0208>>
			posData.vCamLoc = <<971.244, 55.4884, 116.658>>
			posData.vCamRot = <<-15.5548, -0.0118, -48.9534>>
			posData.fCamFOV = 45.0
			posData.iType = CPM_TYPE_MEDIUM_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_MEETING_ROOM)
		BREAK
		CASE CASINO_CPM_POS_152
			posData.vObjLoc = <<974.29, 56.815, 116.82>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<970.523, 59.3674, 116.889>>
			posData.vCamRot = <<-2.4973, -0.0118, -123.045>>
			posData.fCamFOV = 45.0
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.82
			posData.fMaxY = 2.4
			posData.fMinX = 0.8
			posData.fMinY = 0.8
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_MEETING_ROOM)
		BREAK
		CASE CASINO_CPM_POS_153
			posData.vObjLoc = <<973.107, 54.922, 116.82>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<970.3136, 56.5149, 116.8257>>
			posData.vCamRot = <<-3.8152, 0.0197, -122.3685>>
			posData.fCamFOV = 55.2838
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.82
			posData.fMaxY = 2.4
			posData.fMinX = 0.8
			posData.fMinY = 0.8
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_MEETING_ROOM)
		BREAK
		CASE CASINO_CPM_POS_154
			posData.vObjLoc = <<971.925, 53.03, 116.82>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<969.0476, 54.7765, 116.7807>>
			posData.vCamRot = <<-0.2626, 0.0391, -120.6257>>
			posData.fCamFOV = 55.2838
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.82
			posData.fMaxY = 2.4
			posData.fMinX = 0.8
			posData.fMinY = 0.8
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_MEETING_ROOM)
		BREAK
		CASE CASINO_CPM_POS_155
			posData.vObjLoc = <<960.026, 36.427, 115.164>>
			posData.vObjRot = <<0.0, 0.0, -77.0055>>
			posData.vCamLoc = <<956.813, 37.4262, 117.19>>
			posData.vCamRot = <<-24.8733, -0.0325, -112.345>>
			posData.fCamFOV = 55.2838
			posData.iType = CPM_TYPE_MEDIUM_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_CINEMA_ROOM)
		BREAK
		CASE CASINO_CPM_POS_156
			posData.vObjLoc = <<959.969, 34.413, 115.964>>
			posData.vObjRot = <<0.0, 0.0, -122.017>>
			posData.vCamLoc = <<957.595, 36.0907, 116.869>>
			posData.vCamRot = <<-7.6637, -0.0325, -128.339>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_MED_DESK_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_CINEMA_ROOM)
		BREAK
		CASE CASINO_CPM_POS_157
			posData.vObjLoc = <<959.6, 33.948, 115.964>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<958.014, 34.8489, 116.543>>
			posData.vCamRot = <<-10.2496, -0.0325, -120.337>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_CINEMA_ROOM)
		BREAK
		CASE CASINO_CPM_POS_159
			posData.vObjLoc = <<959.289, 33.45, 115.964>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<957.661, 34.2568, 116.472>>
			posData.vCamRot = <<-11.2113, -0.0325, -117.053>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_CINEMA_ROOM)
		BREAK
		CASE CASINO_CPM_POS_160
			posData.vObjLoc = <<957.406, 30.438, 115.964>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<955.619, 32.1574, 116.525>>
			posData.vCamRot = <<-7.0373, -0.0325, -134.319>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_CINEMA_ROOM)
		BREAK
		CASE CASINO_CPM_POS_162
			posData.vObjLoc = <<957.095, 29.941, 115.964>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<955.752, 31.6452, 116.39>>
			posData.vCamRot = <<-5.6545, -0.0325, -138.425>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_CINEMA_ROOM)
		BREAK
		CASE CASINO_CPM_POS_163
			posData.vObjLoc = <<956.84, 29.424, 115.964>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<955.3127, 31.6139, 117.0129>>
			posData.vCamRot = <<-14.9429, -0.0836, -145.8980>>
			posData.fCamFOV = 48.9162
			posData.iType = CPM_TYPE_MED_DESK_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_CINEMA_ROOM)
		BREAK
		CASE CASINO_CPM_POS_164
			posData.vObjLoc = <<954.85, 28.218, 115.164>>
			posData.vObjRot = <<0.0, 0.0, -167.017>>
			posData.vCamLoc = <<952.669, 30.9471, 116.343>>
			posData.vCamRot = <<-11.6396, -0.0325, -142.184>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_MEDIUM_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_CINEMA_ROOM)
		BREAK
		CASE CASINO_CPM_POS_165
			posData.vObjLoc = <<952.47, 35.624, 117.212>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<955.889, 33.2264, 117.723>>
			posData.vCamRot = <<-8.3529, -0.0325, 56.1837>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 2.2
			posData.fMaxY = 2.2
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_CINEMA_ROOM)
		BREAK
		CASE CASINO_CPM_POS_166
			posData.vObjLoc = <<972.508, 46.329, 116.751>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<972.917, 41.8399, 117.616>>
			posData.vCamRot = <<-10.8447, -0.0325, 0.1752>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.2
			posData.fMaxY = 2.0
			posData.fMinX = 0.7
			posData.fMinY = 0.7
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_167
			posData.vObjLoc = <<977.721, 51.87, 117.583>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<976.59, 47.8073, 117.318>>
			posData.vCamRot = <<-3.5903, -0.0325, -29.075>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_168
			posData.vObjLoc = <<978.456, 51.411, 117.583>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<976.59, 47.8073, 117.318>>
			posData.vCamRot = <<-3.5903, -0.0325, -29.075>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_169
			posData.vObjLoc = <<978.958, 51.097, 117.583>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<976.59, 47.8073, 117.318>>
			posData.vCamRot = <<-3.5903, -0.0325, -29.075>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_170
			posData.vObjLoc = <<979.474, 50.775, 117.583>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<976.59, 47.8073, 117.318>>
			posData.vCamRot = <<-3.5903, -0.0325, -29.075>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_171
			posData.vObjLoc = <<980.206, 50.317, 117.583>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<976.59, 47.8073, 117.318>>
			posData.vCamRot = <<-3.5903, -0.0325, -29.075>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_172
			posData.vObjLoc = <<977.721, 51.87, 116.829>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<976.59, 47.8073, 117.318>>
			posData.vCamRot = <<-3.5903, -0.0325, -29.075>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_173
			posData.vObjLoc = <<980.206, 50.317, 116.829>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<976.59, 47.8073, 117.318>>
			posData.vCamRot = <<-3.5903, -0.0325, -29.075>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_174
			posData.vObjLoc = <<977.721, 51.87, 116.074>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<976.59, 47.8073, 117.318>>
			posData.vCamRot = <<-3.5903, -0.0325, -29.075>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_175
			posData.vObjLoc = <<980.206, 50.317, 116.074>>
			posData.vObjRot = <<0.0, 0.0, -32.0283>>
			posData.vCamLoc = <<976.59, 47.8073, 117.318>>
			posData.vCamRot = <<-3.5903, -0.0325, -29.075>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_176
			posData.vObjLoc = <<979.787, 48.608, 116.844>>
			posData.vObjRot = <<0.0, 0.0, -121.983>>
			posData.vCamLoc = <<976.852, 49.7481, 117.124>>
			posData.vCamRot = <<-6.4524, -0.0325, -110.663>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.9
			posData.fMaxY = 1.25
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_178
			posData.vObjLoc = <<961.686, 20.896, 115.167>>
			posData.vObjRot = <<0.0, 0.0, -77.005>>
			posData.vCamLoc = <<957.657, 18.2375, 117.742>>
			posData.vCamRot = <<-17.4842, -0.0325, -63.265>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_MEDIUM_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_SPA_ROOM)
		BREAK
		CASE CASINO_CPM_POS_179
			posData.vObjLoc = <<955.476, 16.473, 116.364>>
			posData.vObjRot = <<0.0, 0.0, -66.9788>>
			posData.vCamLoc = <<954.979, 12.8894, 118.056>>
			posData.vCamRot = <<-18.649, -0.0325, -7.6795>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_MEDIUM_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_SPA_ROOM)
		BREAK
		CASE CASINO_CPM_POS_181
			posData.vObjLoc = <<939.938, 14.244, 115.164>>
			posData.vObjRot = <<0.0, 0.0, 57.9833>>
			posData.vCamLoc = <<941.9, 10.0037, 118.388>>
			posData.vCamRot = <<-25.5687, -0.0325, 24.4365>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_LARGE_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_BAR_ROOM)
		BREAK
		CASE CASINO_CPM_POS_182
			posData.vObjLoc = <<954.355, 25.861, 115.164>>
			posData.vObjRot = <<0.0, 0.0, -32.0091>>
			posData.vCamLoc = <<951.717, 21.3115, 117.385>>
			posData.vCamRot = <<-11.1838, -0.0325, -31.2095>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_DOUBLE_SIDED
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_BAR_ROOM)
		BREAK
		CASE CASINO_CPM_POS_183
			posData.vObjLoc = <<958.498, 22.542, 115.164>>
			posData.vObjRot = <<0.0, 0.0, -92.017>>
			posData.vCamLoc = <<955.012, 20.6386, 116.928>>
			posData.vCamRot = <<-17.9621, -0.0325, -58.5254>>
			posData.fCamFOV = 49.4977
			posData.iType = CPM_TYPE_MEDIUM_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_BAR_ROOM)
		BREAK
		CASE CASINO_CPM_POS_184
			posData.vObjLoc = <<960.721, 34.792, 117.218>>
			posData.vObjRot = <<0.0, 0.0, 58.0>>
			posData.vCamLoc = <<964.624, 31.5939, 117.274>>
			posData.vCamRot = <<0.0, 0.0, 58.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.0
			posData.fMaxY = 1.0
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_185
			posData.vObjLoc = <<971.255, 43.755, 117.154>>
			posData.vObjRot = <<0.0, 0.0, 148.0>>
			posData.vCamLoc = <<973.701, 45.2919, 117.306>>
			posData.vCamRot = <<-10.0, 0.0, 115.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_186
			posData.vObjLoc = <<970.929, 43.958, 117.154>>
			posData.vObjRot = <<0.0, 0.0, 148.0>>
			posData.vCamLoc = <<973.701, 45.2919, 117.306>>
			posData.vCamRot = <<-10.0, 0.0, 115.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_187
			posData.vObjLoc = <<970.596, 44.167, 117.154>>
			posData.vObjRot = <<0.0, 0.0, 148.0>>
			posData.vCamLoc = <<973.701, 45.2919, 117.306>>
			posData.vCamRot = <<-10.0, 0.0, 115.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_188
			posData.vObjLoc = <<971.255, 43.755, 116.608>>
			posData.vObjRot = <<0.0, 0.0, 148.0>>
			posData.vCamLoc = <<973.701, 45.2919, 117.306>>
			posData.vCamRot = <<-10.0, 0.0, 115.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_189
			posData.vObjLoc = <<970.929, 43.958, 116.608>>
			posData.vObjRot = <<0.0, 0.0, 148.0>>
			posData.vCamLoc = <<973.701, 45.2919, 117.306>>
			posData.vCamRot = <<-10.0, 0.0, 115.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_190
			posData.vObjLoc = <<970.596, 44.167, 116.608>>
			posData.vObjRot = <<0.0, 0.0, 148.0>>
			posData.vCamLoc = <<973.701, 45.2919, 117.306>>
			posData.vCamRot = <<-10.0, 0.0, 115.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_191
			posData.vObjLoc = <<971.265, 43.749, 116.064>>
			posData.vObjRot = <<0.0, 0.0, 148.0>>
			posData.vCamLoc = <<973.701, 45.2919, 117.306>>
			posData.vCamRot = <<-10.0, 0.0, 115.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_192
			posData.vObjLoc = <<970.944, 43.949, 116.064>>
			posData.vObjRot = <<0.0, 0.0, 148.0>>
			posData.vCamLoc = <<973.701, 45.2919, 117.306>>
			posData.vCamRot = <<-10.0, 0.0, 115.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_193
			posData.vObjLoc = <<970.624, 44.149, 116.064>>
			posData.vObjRot = <<0.0, 0.0, 148.0>>
			posData.vCamLoc = <<973.701, 45.2919, 117.306>>
			posData.vCamRot = <<-10.0, 0.0, 115.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_194
			posData.vObjLoc = <<935.052, 6.632, 115.164>>
			posData.vObjRot = <<0.0, 0.0, 88.0>>
			posData.vCamLoc = <<940.486, 7.9568, 117.002>>
			posData.vCamRot = <<-4.8293, 0.2961, 103.03>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_DOUBLE_SIDED
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_BAR_ROOM)
		BREAK
		CASE CASINO_CPM_POS_195
			posData.vObjLoc = <<955.341, 9.91, 115.164>>
			posData.vObjRot = <<0.0, 0.0, -122.0>>
			posData.vCamLoc = <<949.585, 13.5631, 117.476>>
			posData.vCamRot = <<-10.0, 0.0, -122.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_DOUBLE_SIDED
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_SPA_ROOM)
		BREAK
		CASE CASINO_CPM_POS_196
			posData.vObjLoc = <<964.935, 45.677, 115.164>>
			posData.vObjRot = <<0.0, 0.0, 58.0>>
			posData.vCamLoc = <<968.856, 42.0254, 117.372>>
			posData.vCamRot = <<-9.8474, -0.7427, 47.9779>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_DOUBLE_SIDED
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_197
			posData.vObjLoc = <<974.877, 41.662, 115.657>>
			posData.vObjRot = <<0.0, 0.0, -147.0>>
			posData.vCamLoc = <<972.596, 44.0247, 116.879>>
			posData.vCamRot = <<-15.0, 0.0, -132.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_MED_DESK_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_198
			posData.vObjLoc = <<947.3, 5.307, 115.164>>
			posData.vObjRot = <<0.0, 0.0, -37.0>>
			posData.vCamLoc = <<941.92, 2.2926, 117.87>>
			posData.vCamRot = <<-15.0, 0.0, -62.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_LARGE_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_BAR_ROOM)
		BREAK
		CASE CASINO_CPM_POS_199
			posData.vObjLoc = <<959.811, 42.058, 116.074>>
			posData.vObjRot = <<0.0, 0.0, -47.0>>
			posData.vCamLoc = <<957.045, 40.9238, 117.228>>
			posData.vCamRot = <<-20.0, 0.0, -72.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_200
			posData.vObjLoc = <<976.762, 46.439, 115.964>>
			posData.vObjRot = <<0.0, 0.0, -57.0>>
			posData.vCamLoc = <<974.094, 44.524, 117.286>>
			posData.vCamRot = <<-15.0, 0.0, -57.0>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_MED_DESK_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
		CASE CASINO_CPM_POS_201
			posData.vObjLoc = <<965.9, 38.927, 117.047>>
			posData.vObjRot = <<0.0, 0.0, -122.0>>
			posData.vCamLoc = <<963.9164, 40.9181, 117.0740>>
			posData.vCamRot = <<-0.0183, -0.0203, -135.2716>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_WALL_ART
			posData.fMaxX = 1.0
			posData.fMaxY = 1.0
			posData.fMinX = 0.1
			posData.fMinY = 0.1
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
		BREAK
		CASE CASINO_CPM_POS_202
			posData.vObjLoc = <<972.274, 45.963, 115.564>>
			posData.vObjRot = <<0.0, 0.0, 13.0>>
			posData.vCamLoc =  <<973.0249, 43.5604, 116.4089>>
			posData.vCamRot = <<-8.3455, -0.0509, 16.0810>>
			posData.fCamFOV = 58.2611
			posData.iType = CPM_TYPE_SMALL_SCULPTURE
			posData.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
		BREAK
	ENDSWITCH																								
ENDPROC																			
																											
PROC GET_CPM_AREA_DETAILS(CASINO_CPM_AREA_DATA &data, INT iAreaID)
	data.tlName = ""
	data.iAreaID = iAreaID
	SWITCH iAreaID
		CASE CASINO_CPM_AREA_ENTRANCE_HALL
			data.tlName = "CPM_AREA0"
			data.tlTitle = "CPM_T_AREA0"
		BREAK
		CASE CASINO_CPM_AREA_MASTER_BED
			data.tlName = "CPM_AREA1"
			data.tlTitle = "CPM_T_AREA1"
		BREAK
		CASE CASINO_CPM_AREA_SPARE_BED
			data.tlName = "CPM_AREA2"
			data.tlTitle = "CPM_T_AREA2"
		BREAK
		CASE CASINO_CPM_AREA_LOUNGE
			data.tlName = "CPM_AREA3"
			data.tlTitle = "CPM_T_AREA3"
		BREAK
		CASE CASINO_CPM_AREA_DEALER_ROOM
			data.tlName = "CPM_AREA4"
			data.tlTitle = "CPM_T_AREA4"
		BREAK
		CASE CASINO_CPM_AREA_OFFICE
			data.tlName = "CPM_AREA5"
			data.tlTitle = "CPM_T_AREA5"
		BREAK
		CASE CASINO_CPM_AREA_MEDIA_ROOM
			data.tlName = "CPM_AREA6"
			data.tlTitle = "CPM_T_AREA6"
		BREAK
		CASE CASINO_CPM_AREA_BAR
			data.tlName = "CPM_AREA7"
			data.tlTitle = "CPM_T_AREA7"
		BREAK
		CASE CASINO_CPM_AREA_SPA
			data.tlName = "CPM_AREA8"
			data.tlTitle = "CPM_T_AREA8"
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT GET_CPM_ROOM_AREA(INT iRoomID)
	SWITCH iRoomID	
		CASE CASINO_CPM_LOC_ENTRANCE_HALL
			RETURN CASINO_CPM_AREA_ENTRANCE_HALL
		BREAK
		CASE CASINO_CPM_LOC_MASTER_BED
			RETURN CASINO_CPM_AREA_MASTER_BED
		BREAK	
		CASE CASINO_CPM_LOC_MASTER_BED_LOUNGE
			RETURN CASINO_CPM_AREA_MASTER_BED
		BREAK
		CASE CASINO_CPM_LOC_MASTER_BED_SHELF
			RETURN CASINO_CPM_AREA_MASTER_BED
		BREAK
		CASE CASINO_CPM_LOC_SPARE_BED
			RETURN CASINO_CPM_AREA_SPARE_BED
		BREAK
		CASE CASINO_CPM_LOC_LOUNGE_SHELF1
			RETURN CASINO_CPM_AREA_LOUNGE
		BREAK
		CASE CASINO_CPM_LOC_LOUNGE1
			RETURN CASINO_CPM_AREA_LOUNGE
		BREAK	
		CASE CASINO_CPM_LOC_LOUNGE_SHELF2
			RETURN CASINO_CPM_AREA_LOUNGE
		BREAK
		CASE CASINO_CPM_LOC_LOUNGE2
			RETURN CASINO_CPM_AREA_LOUNGE
		BREAK	
		CASE CASINO_CPM_LOC_LOUNGE_BOARDROOM
			RETURN CASINO_CPM_AREA_LOUNGE
		BREAK
		CASE CASINO_CPM_LOC_LOUNGE3
			RETURN CASINO_CPM_AREA_LOUNGE
		BREAK	
		CASE CASINO_CPM_LOC_DEALER_ROOM
			RETURN CASINO_CPM_AREA_DEALER_ROOM
		BREAK
		CASE CASINO_CPM_LOC_DEALER_ROOM_SHELF
			RETURN CASINO_CPM_AREA_DEALER_ROOM
		BREAK
		CASE CASINO_CPM_LOC_OFFICE1
			RETURN CASINO_CPM_AREA_OFFICE
		BREAK
		CASE CASINO_CPM_LOC_OFFICE_SHELF1
			RETURN CASINO_CPM_AREA_OFFICE
		BREAK
		CASE CASINO_CPM_LOC_OFFICE2
			RETURN CASINO_CPM_AREA_OFFICE
		BREAK
		CASE CASINO_CPM_LOC_OFFICE_SHELF2
			RETURN CASINO_CPM_AREA_OFFICE
		BREAK
		CASE CASINO_CPM_LOC_MEDIA_ROOM
			RETURN CASINO_CPM_AREA_MEDIA_ROOM
		BREAK
		CASE CASINO_CPM_LOC_BAR
			RETURN CASINO_CPM_AREA_BAR
		BREAK
		CASE CASINO_CPM_LOC_SPA
			RETURN CASINO_CPM_AREA_SPA
		BREAK
	ENDSWITCH
	RETURN -1
ENDFUNC

PROC GET_CPM_ROOM_DETAILS(CASINO_CPM_ROOM_DATA &data, INT iRoomID)											
	INT i																									
	data.tlName = ""																						
	data.bOwned = FALSE																						
	data.bGroupedItems = FALSE
	data.iMaxPostions = 0																					
	REPEAT CASINO_CPM_MAX_ROOM_POINTS i																		
		data.iPositions[i] = 0																				
	ENDREPEAT																								
	data.iRoomID = iRoomID																					
	data.iAreaID = -1
	SWITCH iRoomID	
		CASE CASINO_CPM_LOC_ENTRANCE_HALL
			data.tlName = "CPM_ROOM0"
			data.iAreaID = CASINO_CPM_AREA_ENTRANCE_HALL
			data.iPositions[0] = CASINO_CPM_POS_27
			data.iPositions[1] = CASINO_CPM_POS_28
			data.iPositions[2] = CASINO_CPM_POS_29
			data.iPositions[3] = CASINO_CPM_POS_30
			data.iPositions[4] = CASINO_CPM_POS_31
			data.iPositions[5] = CASINO_CPM_POS_32
			data.iPositions[6] = CASINO_CPM_POS_33
			data.iPositions[7] = CASINO_CPM_POS_34
			data.iPositions[8] = CASINO_CPM_POS_35
			data.iPositions[9] = CASINO_CPM_POS_36
			data.iPositions[10] = CASINO_CPM_POS_37
			data.iPositions[11] = CASINO_CPM_POS_38
			data.iPositions[12] = CASINO_CPM_POS_39
			data.iPositions[13] = CASINO_CPM_POS_20
			data.iPositions[14] = CASINO_CPM_POS_21
			data.iPositions[15] = CASINO_CPM_POS_22
			data.iPositions[16] = CASINO_CPM_POS_23
			data.iPositions[17] = CASINO_CPM_POS_24
			data.iPositions[18] = CASINO_CPM_POS_25
			data.iPositions[19] = CASINO_CPM_POS_26
			data.iMaxPostions = 20
			data.iRoomKey = ENUM_TO_INT(CAS_APT_ENTRY_ROOM)
			data.bOwned = TRUE
		BREAK
		CASE CASINO_CPM_LOC_MASTER_BED
			data.tlName = "CPM_ROOM1"
			data.iAreaID = CASINO_CPM_AREA_MASTER_BED
			data.iPositions[0] = CASINO_CPM_POS_1											
			data.iPositions[1] = CASINO_CPM_POS_2											
			data.iPositions[2] = CASINO_CPM_POS_3
			data.iPositions[3] = CASINO_CPM_POS_4
			data.iPositions[4] = CASINO_CPM_POS_5
			data.iPositions[5] = CASINO_CPM_POS_6
			data.iMaxPostions = 6
			data.iRoomKey = ENUM_TO_INT(CAS_APT_MAIN_BEDROOM)
			data.bOwned = TRUE
		BREAK	
		CASE CASINO_CPM_LOC_MASTER_BED_LOUNGE
			data.tlName = "CPM_ROOM2"
			data.iAreaID = CASINO_CPM_AREA_MASTER_BED
			data.iPositions[0] = CASINO_CPM_POS_7											
			data.iPositions[1] = CASINO_CPM_POS_8											
			data.iPositions[2] = CASINO_CPM_POS_9
			data.iPositions[3] = CASINO_CPM_POS_10
			data.iMaxPostions = 4
			data.iRoomKey = ENUM_TO_INT(CAS_APT_PRIV_LOUNGE_ROOM)
			data.bOwned = TRUE
		BREAK
		CASE CASINO_CPM_LOC_MASTER_BED_SHELF
			data.tlName = "CPM_ROOM3"
			data.iAreaID = CASINO_CPM_AREA_MASTER_BED
			data.iPositions[0] = CASINO_CPM_POS_11
			data.iPositions[1] = CASINO_CPM_POS_12
			data.iPositions[2] = CASINO_CPM_POS_13
			data.iPositions[3] = CASINO_CPM_POS_14
			data.iPositions[4] = CASINO_CPM_POS_15
			data.iPositions[5] = CASINO_CPM_POS_16
			data.iPositions[6] = CASINO_CPM_POS_17
			data.iPositions[7] = CASINO_CPM_POS_18
			data.iPositions[8] = CASINO_CPM_POS_19
			data.iMaxPostions = 9
			data.bGroupedItems = TRUE
			data.iRoomKey = ENUM_TO_INT(CAS_APT_PRIV_LOUNGE_ROOM)
			data.bOwned = TRUE
		BREAK
		CASE CASINO_CPM_LOC_SPARE_BED
			data.tlName = "CPM_ROOM4"
			data.iAreaID = CASINO_CPM_AREA_SPARE_BED
			data.iPositions[0] = CASINO_CPM_POS_40
			data.iPositions[1] = CASINO_CPM_POS_41
			data.iPositions[2] = CASINO_CPM_POS_42
			data.iPositions[3] = CASINO_CPM_POS_43
			data.iPositions[4] = CASINO_CPM_POS_44
			data.iPositions[5] = CASINO_CPM_POS_45
			data.iPositions[6] = CASINO_CPM_POS_46
			data.iPositions[7] = CASINO_CPM_POS_47
			data.iPositions[8] = CASINO_CPM_POS_48
			data.iPositions[9] = CASINO_CPM_POS_49
			data.iMaxPostions = 10
			data.iRoomKey = ENUM_TO_INT(CAS_APT_SPARE_BEDROOM)
			data.bOwned = TRUE
		BREAK
		CASE CASINO_CPM_LOC_LOUNGE_SHELF1
			data.tlName = "CPM_ROOM7"
			data.iAreaID = CASINO_CPM_AREA_LOUNGE
			data.iPositions[0] = CASINO_CPM_POS_50
			data.iPositions[1] = CASINO_CPM_POS_51
			data.iPositions[2] = CASINO_CPM_POS_52
			data.iPositions[3] = CASINO_CPM_POS_54
			data.iPositions[4] = CASINO_CPM_POS_55
			data.iPositions[5] = CASINO_CPM_POS_56
			data.iPositions[6] = CASINO_CPM_POS_57
			data.iPositions[7] = CASINO_CPM_POS_58
			data.iPositions[8] = CASINO_CPM_POS_59
			data.iPositions[9] = CASINO_CPM_POS_60
			data.iPositions[10] = CASINO_CPM_POS_61
			data.iPositions[11] = CASINO_CPM_POS_62
			data.iPositions[12] = CASINO_CPM_POS_63
			data.iPositions[13] = CASINO_CPM_POS_64
			data.iPositions[14] = CASINO_CPM_POS_65
			data.iPositions[15] = CASINO_CPM_POS_66
			data.iPositions[16] = CASINO_CPM_POS_67
			data.iPositions[17] = CASINO_CPM_POS_68
			data.iPositions[18] = CASINO_CPM_POS_69
			data.iPositions[19] = CASINO_CPM_POS_70
			data.iPositions[20] = CASINO_CPM_POS_71
			data.iPositions[21] = CASINO_CPM_POS_72
			data.iPositions[22] = CASINO_CPM_POS_73
			data.iPositions[23] = CASINO_CPM_POS_74
			data.iPositions[24] = CASINO_CPM_POS_75
			data.iPositions[25] = CASINO_CPM_POS_76
			data.iPositions[26] = CASINO_CPM_POS_77
			data.iMaxPostions = 27
			data.bGroupedItems = TRUE
			data.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
			data.bOwned = TRUE
		BREAK
		CASE CASINO_CPM_LOC_LOUNGE1
			data.tlName = "CPM_ROOM5"
			data.iAreaID = CASINO_CPM_AREA_LOUNGE
			data.iPositions[0] = CASINO_CPM_POS_79
			data.iPositions[1] = CASINO_CPM_POS_78
			data.iPositions[2] = CASINO_CPM_POS_80
			data.iPositions[3] = CASINO_CPM_POS_81
			data.iPositions[4] = CASINO_CPM_POS_82
			data.iPositions[5] = CASINO_CPM_POS_201
			data.iPositions[6] = CASINO_CPM_POS_83
			data.iPositions[7] = CASINO_CPM_POS_84
			data.iPositions[8] = CASINO_CPM_POS_101
			data.iPositions[9] = CASINO_CPM_POS_102
			data.iPositions[10] = CASINO_CPM_POS_103
			data.iMaxPostions = 11
			data.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
			data.bOwned = TRUE
		BREAK	
		CASE CASINO_CPM_LOC_LOUNGE_SHELF2
			data.tlName = "CPM_ROOM8"
			data.iAreaID = CASINO_CPM_AREA_LOUNGE
			data.iPositions[0] = CASINO_CPM_POS_104
			data.iPositions[1] = CASINO_CPM_POS_105
			data.iPositions[2] = CASINO_CPM_POS_106
			data.iPositions[3] = CASINO_CPM_POS_107
			data.iPositions[4] = CASINO_CPM_POS_108
			data.iPositions[5] = CASINO_CPM_POS_109
			data.iPositions[6] = CASINO_CPM_POS_110
			data.iPositions[7] = CASINO_CPM_POS_111
			data.iPositions[8] = CASINO_CPM_POS_112
			data.iPositions[9] = CASINO_CPM_POS_113
			data.iPositions[10] = CASINO_CPM_POS_114
			data.iPositions[11] = CASINO_CPM_POS_115
			data.iPositions[12] = CASINO_CPM_POS_118
			data.iPositions[13] = CASINO_CPM_POS_119
			data.iPositions[14] = CASINO_CPM_POS_120
			data.iPositions[15] = CASINO_CPM_POS_121
			data.iPositions[16] = CASINO_CPM_POS_123
			data.iPositions[17] = CASINO_CPM_POS_126
			data.iPositions[18] = CASINO_CPM_POS_127
			data.iPositions[19] = CASINO_CPM_POS_128
			data.iPositions[20] = CASINO_CPM_POS_129
			data.iPositions[21] = CASINO_CPM_POS_130
			data.iPositions[22] = CASINO_CPM_POS_131
			data.iPositions[23] = CASINO_CPM_POS_132
			data.iPositions[24] = CASINO_CPM_POS_133
			data.iPositions[25] = CASINO_CPM_POS_134
			data.iPositions[26] = CASINO_CPM_POS_135
			data.iPositions[27] = CASINO_CPM_POS_184
			data.iMaxPostions = 28
			data.bGroupedItems = TRUE
			data.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
			data.bOwned = TRUE
		BREAK
		CASE CASINO_CPM_LOC_LOUNGE2
			data.tlName = "CPM_ROOM5"
			data.iAreaID = CASINO_CPM_AREA_LOUNGE
			data.iPositions[0] = CASINO_CPM_POS_136
			data.iPositions[1] = CASINO_CPM_POS_137
			data.iPositions[2] = CASINO_CPM_POS_138
			data.iPositions[3] = CASINO_CPM_POS_140
			data.iPositions[4] = CASINO_CPM_POS_199
			data.iPositions[5] = CASINO_CPM_POS_142
			data.iPositions[6] = CASINO_CPM_POS_145
			data.iPositions[7] = CASINO_CPM_POS_196
			data.iPositions[8] = CASINO_CPM_POS_143
			data.iPositions[9] = CASINO_CPM_POS_144
			data.iMaxPostions = 10
			data.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
			data.bOwned = TRUE
		BREAK	
		CASE CASINO_CPM_LOC_LOUNGE_BOARDROOM
			data.tlName = "CPM_ROOM6"
			data.iAreaID = CASINO_CPM_AREA_LOUNGE
			data.iPositions[0] = CASINO_CPM_POS_148
			data.iPositions[1] = CASINO_CPM_POS_149
			data.iPositions[2] = CASINO_CPM_POS_150
			data.iPositions[3] = CASINO_CPM_POS_151
			data.iPositions[4] = CASINO_CPM_POS_152
			data.iPositions[5] = CASINO_CPM_POS_153
			data.iPositions[6] = CASINO_CPM_POS_154
			data.iMaxPostions = 7
			data.iRoomKey = ENUM_TO_INT(CAS_APT_MEETING_ROOM)
			data.bOwned = TRUE
		BREAK
		CASE CASINO_CPM_LOC_LOUNGE3
			data.tlName = "CPM_ROOM5"
			data.iAreaID = CASINO_CPM_AREA_LOUNGE
			data.iPositions[0] = CASINO_CPM_POS_146
			data.iPositions[1] = CASINO_CPM_POS_147
			data.iMaxPostions = 2
			data.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
			data.bOwned = TRUE
		BREAK	
		CASE CASINO_CPM_LOC_DEALER_ROOM
			data.tlName = "CPM_ROOM9"
			data.iAreaID = CASINO_CPM_AREA_DEALER_ROOM
			data.iPositions[0] = CASINO_CPM_POS_100
			data.iMaxPostions = 1
			data.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
			data.bOwned = TRUE
		BREAK
		CASE CASINO_CPM_LOC_DEALER_ROOM_SHELF
			data.tlName = "CPM_ROOM10"
			data.iAreaID = CASINO_CPM_AREA_DEALER_ROOM
			data.iPositions[0] = CASINO_CPM_POS_85
			data.iPositions[1] = CASINO_CPM_POS_86
			data.iPositions[2] = CASINO_CPM_POS_87
			data.iPositions[3] = CASINO_CPM_POS_88
			data.iPositions[4] = CASINO_CPM_POS_89	
			data.iPositions[5] = CASINO_CPM_POS_90
			data.iPositions[6] = CASINO_CPM_POS_91
			data.iPositions[7] = CASINO_CPM_POS_92
			data.iPositions[8] = CASINO_CPM_POS_93
			data.iPositions[9] = CASINO_CPM_POS_94
			data.iPositions[10] = CASINO_CPM_POS_95
			data.iPositions[11] = CASINO_CPM_POS_96
			data.iPositions[12] = CASINO_CPM_POS_97
			data.iPositions[13] = CASINO_CPM_POS_98
			data.iPositions[14] = CASINO_CPM_POS_99
			data.bGroupedItems = TRUE
			data.iMaxPostions = 15
			data.iRoomKey = ENUM_TO_INT(CAS_APT_LOUNGE_ROOM)
			data.bOwned = TRUE
		BREAK
		CASE CASINO_CPM_LOC_OFFICE1
			data.tlName = "CPM_ROOM11"
			data.iAreaID = CASINO_CPM_AREA_OFFICE
			data.iPositions[0] =CASINO_CPM_POS_202
			data.iPositions[1] = CASINO_CPM_POS_166
			data.iMaxPostions = 2
			data.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
			data.bOwned = TRUE
		BREAK
		CASE CASINO_CPM_LOC_OFFICE_SHELF1
			data.tlName = "CPM_ROOM12"
			data.iAreaID = CASINO_CPM_AREA_OFFICE
			data.iPositions[0] = CASINO_CPM_POS_167
			data.iPositions[1] = CASINO_CPM_POS_168
			data.iPositions[2] = CASINO_CPM_POS_169
			data.iPositions[3] = CASINO_CPM_POS_170
			data.iPositions[4] = CASINO_CPM_POS_171
			data.iPositions[5] = CASINO_CPM_POS_172
			data.iPositions[6] = CASINO_CPM_POS_173
			data.iPositions[7] = CASINO_CPM_POS_174
			data.iPositions[8] = CASINO_CPM_POS_175
			data.iMaxPostions = 9
			data.bGroupedItems = TRUE
			data.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
			data.bOwned = TRUE
		BREAK
		CASE CASINO_CPM_LOC_OFFICE2
			data.tlName = "CPM_ROOM11"
			data.iAreaID = CASINO_CPM_AREA_OFFICE
			data.iPositions[0] = CASINO_CPM_POS_176
			data.iPositions[1] = CASINO_CPM_POS_200
			data.iPositions[2] = CASINO_CPM_POS_197
			data.iMaxPostions = 3
			data.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
			data.bOwned = TRUE
		BREAK
		CASE CASINO_CPM_LOC_OFFICE_SHELF2
			data.tlName = "CPM_ROOM13"
			data.iAreaID = CASINO_CPM_AREA_OFFICE
			data.iPositions[0] = CASINO_CPM_POS_185
			data.iPositions[1] = CASINO_CPM_POS_186
			data.iPositions[2] = CASINO_CPM_POS_187
			data.iPositions[3] = CASINO_CPM_POS_188
			data.iPositions[4] = CASINO_CPM_POS_189
			data.iPositions[5] = CASINO_CPM_POS_190
			data.iPositions[6] = CASINO_CPM_POS_191
			data.iPositions[7] = CASINO_CPM_POS_192
			data.iPositions[8] = CASINO_CPM_POS_193
			data.iMaxPostions = 9
			data.bGroupedItems = TRUE
			data.iRoomKey = ENUM_TO_INT(CAS_APT_OFFICE_ROOM)	
			data.bOwned = TRUE
		BREAK
		CASE CASINO_CPM_LOC_MEDIA_ROOM
			data.tlName = "CPM_ROOM14"
			data.iAreaID = CASINO_CPM_AREA_MEDIA_ROOM
			data.iPositions[0] = CASINO_CPM_POS_155
			data.iPositions[1] = CASINO_CPM_POS_156
			data.iPositions[2] = CASINO_CPM_POS_157
			data.iPositions[3] = CASINO_CPM_POS_159
			data.iPositions[4] = CASINO_CPM_POS_160
			data.iPositions[5] = CASINO_CPM_POS_162
			data.iPositions[6] = CASINO_CPM_POS_163
			data.iPositions[7] = CASINO_CPM_POS_164
			data.iPositions[8] = CASINO_CPM_POS_165
			data.iMaxPostions = 9
			data.iRoomKey = ENUM_TO_INT(CAS_APT_CINEMA_ROOM)
			data.bOwned = TRUE
		BREAK
		CASE CASINO_CPM_LOC_BAR
			data.tlName = "CPM_ROOM15"
			data.iAreaID = CASINO_CPM_AREA_BAR
			data.iPositions[0] = CASINO_CPM_POS_183	
			data.iPositions[1] = CASINO_CPM_POS_198
			data.iPositions[2] = CASINO_CPM_POS_194
			data.iPositions[3] = CASINO_CPM_POS_181
			data.iPositions[4] = CASINO_CPM_POS_182
			data.iMaxPostions = 5
			data.iRoomKey = ENUM_TO_INT(CAS_APT_BAR_ROOM)
			data.bOwned = TRUE
		BREAK
		CASE CASINO_CPM_LOC_SPA
			data.tlName = "CPM_ROOM16"
			data.iAreaID = CASINO_CPM_AREA_SPA
			data.iPositions[0] = CASINO_CPM_POS_178
			data.iPositions[1] = CASINO_CPM_POS_195
			data.iPositions[2] = CASINO_CPM_POS_179
			data.iMaxPostions = 3
			data.iRoomKey = ENUM_TO_INT(CAS_APT_SPA_ROOM)
			data.bOwned = TRUE
		BREAK
		DEFAULT
			data.iMaxPostions = 0
		BREAK
	ENDSWITCH
ENDPROC

FUNC STATS_PACKED GET_DECORATION_POSITION_STAT(INT iPosition, INT iStatNum)
 	IF iStatNum = 1 
	 	RETURN INT_TO_ENUM(STATS_PACKED,ENUM_TO_INT(START_DECORATION_LOC_P1)+iPosition)
	ELIF iStatNum = 2
	 	RETURN INT_TO_ENUM(STATS_PACKED,ENUM_TO_INT(START_DECORATION_LOC_P2)+iPosition)	
	ENDIF
	RETURN INT_TO_ENUM(STATS_PACKED,ENUM_TO_INT(START_DECORATION_LOC_P1)+iPosition)
ENDFUNC


FUNC BOOL IS_OWNED_DECORATION_NO_LONGER_AVAILABLE(INT iOwnedDecoration)
	SWITCH INT_TO_ENUM(CASINO_CPM_ITEM,iOwnedDecoration)
		CASE CPM_CH_PROP_TREE_01a
			RETURN g_sMPTunables.iCH_PROP_TREE_01a = -1
		BREAK
		CASE CPM_CH_PROP_TREE_02a
			RETURN g_sMPTunables.iCH_PROP_TREE_02a = -1	
		BREAK
		CASE CPM_CH_PROP_TREE_03a
			RETURN g_sMPTunables.iCH_PROP_TREE_03a = -1	
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_PLAYER_OWNED_DECORATION_IN_POS(INT iPosition)
	INT iOwnedDecoration = GET_PACKED_STAT_INT(GET_DECORATION_POSITION_STAT(iPosition,1))
	IF iOwnedDecoration >= 255
		iOwnedDecoration += GET_PACKED_STAT_INT(GET_DECORATION_POSITION_STAT(iPosition,2))
	ENDIF
	IF IS_OWNED_DECORATION_NO_LONGER_AVAILABLE(iOwnedDecoration)
		iOwnedDecoration = 0
	ENDIF
	RETURN iOwnedDecoration
ENDFUNC

FUNC INT CPM_GET_PLAYER_OWNED_DECORATION_IN_POS(CASINO_CPM &menu,INT iPosition)
	RETURN menu.iOwnedDecorations[iPosition]
ENDFUNC


PROC CPM_SET_PLAYER_OWNED_DECORATION_IN_POS(CASINO_CPM &menu,INT iPosition, INT iItem)
	
	// Remove from current position.
	IF iItem != 0
		INT iCP
		REPEAT CASINO_CPM_MAX_POS iCP
			IF CPM_GET_PLAYER_OWNED_DECORATION_IN_POS(menu,iCP) = iItem
				CPM_SET_PLAYER_OWNED_DECORATION_IN_POS(menu,iCP, 0)
			ENDIF
		ENDREPEAT
	ENDIF
	
	// Add to new position
	IF iItem <= 255
		SET_PACKED_STAT_INT(GET_DECORATION_POSITION_STAT(iPosition,1),iItem) 
		SET_PACKED_STAT_INT(GET_DECORATION_POSITION_STAT(iPosition,2),0) 
	ELSE
		SET_PACKED_STAT_INT(GET_DECORATION_POSITION_STAT(iPosition,1),255)
		SET_PACKED_STAT_INT(GET_DECORATION_POSITION_STAT(iPosition,2),iItem-255) 
	ENDIF
	menu.iOwnedDecorations[iPosition] = iItem
	PRINTLN("SET_PLAYER_OWNED_DECORATION_IN_POS: setting decoration in position #",iPosition," to be item #",iItem)
ENDPROC

PROC SET_CASINO_CPM_CAMERA(CASINO_CPM &menu, INT iPosition)
	//VECTOR vPos//, vRot
	CASINO_CPM_POSITION_DATA tempPosdata
	//PRINTLN("SET_CASINO_CPM_CAMERA:1 cameras are :",tempPosdata.vCamLoc,menu.posData.vCamLoc)
	
	GET_CPM_POSITION_DETAILS(tempPosdata,iPosition)
	//PRINTLN("SET_CASINO_CPM_CAMERA:2 cameras are :",tempPosdata.vCamLoc,menu.posData.vCamLoc)
	IF NOT DOES_CAM_EXIST(menu.theCam)
	OR (menu.iCurrentCamPos != iPosition AND NOT ARE_VECTORS_EQUAL(tempPosdata.vCamLoc,menu.posData.vCamLoc))
		
		IF NOT DOES_CAM_EXIST(menu.theCam)
			menu.theCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE) 
			SET_CAM_ACTIVE(menu.theCam, TRUE)
			RENDER_SCRIPT_CAMS(TRUE,FALSE)
			PRINTLN("SET_CASINO_CPM_CAMERA: setting camera active")
		ENDIF
		menu.posData.vCamLoc = tempPosdata.vCamLoc
		menu.posData.vCamRot = tempPosdata.vCamRot
		menu.posData.fCamFOV = tempPosdata.fCamFOV
		SET_CAM_PARAMS(menu.theCam,menu.posData.vCamLoc,menu.posData.vCamRot,menu.posData.fCamFOV)
		SHAKE_CAM(menu.theCam, "HAND_SHAKE", 0.25)
		PRINTLN("SET_CASINO_CPM_CAMERA: setting camera for position = ",iPosition)
		PRINTLN("SET_CASINO_CPM_CAMERA: at pos :",menu.posData.vCamLoc)
		PRINTLN("SET_CASINO_CPM_CAMERA:3 cameras are :",tempPosdata.vCamLoc,menu.posData.vCamLoc)
		menu.iCurrentCamPos = iPosition
//	ELSE
//		#IF IS_DEBUG_BUILD
//		PRINTLN("SET_CASINO_CPM_CAMERA:4 cameras are :",tempPosdata.vCamLoc,menu.posData.vCamLoc)
//		PRINTLN("SET_CASINO_CPM_CAMERA: cam position = ",iPosition)
//		#ENDIF
	ENDIF
ENDPROC

PROC MOVE_TO_ITEM_SELECTION_MENU(CASINO_CPM &menu,INT iRoom)
	GET_CPM_ROOM_DETAILS(menu.roomData, iRoom)
	menu.iCPSelection =  menu.roomData.iPositions[0]
	menu.iLevel = CASINO_CPM_LEVEL_ITEMS
	menu.iCurrentSelection[CASINO_CPM_LEVEL_ITEMS] = 0
	menu.iCurrentSelection_Top[CASINO_CPM_LEVEL_ITEMS] = 0
	menu.iItemSecondPageStart = 0
	CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SET_DEFAULT_OPTION)
	CLEAR_BIT(menu.iBS, CASINO_CPM_BS_HAS_SEC_MENU_PAGE)
	CLEAR_BIT(menu.iBS, CASINO_CPM_BS_USING_SEC_MENU_PAGE)
	PRINTLN("MOVE_TO_ITEM_SELECTION: moving to item selection in room ",iRoom)
ENDPROC

PROC MOVE_TO_ROOM_SELECTION(CASINO_CPM &menu)
	//GET_CPM_ROOM_DETAILS(menu.roomData, menu.iCurrentSelection[CASINO_CPM_LEVEL_ROOMS])
	IF DOES_ENTITY_EXIST(menu.currentObj)
		DELETE_OBJECT(menu.currentObj)
		PRINTLN("HANDLE_CASINO_CPM_ITEMS: deleting previous object ")
	ENDIF
	IF DOES_CAM_EXIST(menu.theCam)
		RENDER_SCRIPT_CAMS(FALSE,FALSE)
		DESTROY_CAM(menu.theCam)
	ENDIF
	CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SET_DEFAULT_OPTION)
	CLEAR_BIT(menu.iBS, CASINO_CPM_BS_HAS_SEC_MENU_PAGE)
	CLEAR_BIT(menu.iBS, CASINO_CPM_BS_USING_SEC_MENU_PAGE)
	menu.iItemSecondPageStart = 0
	menu.iCurrentSelection[menu.ilevel] = 0	
	menu.iLevel = CASINO_CPM_LEVEL_ROOMS
	menu.iRoomPosSelection = 0
ENDPROC

PROC INCREASE_CASINO_CPM_ROOM_POS(CASINO_CPM &menu, BOOL bUseGroupBehaviour)
	IF menu.iCurrentSelection[CASINO_CPM_LEVEL_ROOMS] >= 0
		GET_CPM_ROOM_DETAILS(menu.roomData, menu.iSelectionIDs[CASINO_CPM_LEVEL_ROOMS][menu.iCurrentSelection[CASINO_CPM_LEVEL_ROOMS]])
		
		IF NOT menu.roomData.bGroupedItems
		OR bUseGroupBehaviour
			menu.iRoomPosSelection++
		ELSE
			menu.iRoomPosSelection = menu.roomData.iMaxPostions
		ENDIF
		
		IF menu.iRoomPosSelection >= menu.roomData.iMaxPostions
			
			IF bUseGroupBehaviour
				menu.iRoomPosSelection = 0
			ELSE
				// Since we have reached the end, see if we can switch to the next room in the area
				IF menu.roomData.iAreaID != -1
					INT iNextRoom = menu.roomData.iRoomID
					INT i
					CASINO_CPM_ROOM_DATA tempRoomData
					BOOL bRoomFound = FALSE
					
					FOR i = iNextRoom+1 TO CASINO_CPM_ROOM_MAX-1
						GET_CPM_ROOM_DETAILS(tempRoomData, i)
						
						IF tempRoomData.iMaxPostions > 0
						AND tempRoomData.iAreaID = menu.roomData.iAreaID
							MOVE_TO_ITEM_SELECTION_MENU(menu, i)
							menu.iSelectionIDs[CASINO_CPM_LEVEL_ROOMS][menu.iCurrentSelection[CASINO_CPM_LEVEL_ROOMS]] = i
							menu.iRoomPosSelection = 0
							bRoomFound = TRUE
							BREAKLOOP
						ENDIF
					ENDFOR
					
					IF NOT bRoomFound
						FOR i = 0 TO iNextRoom
							GET_CPM_ROOM_DETAILS(tempRoomData, i)
							IF tempRoomData.iMaxPostions > 0
							AND tempRoomData.iAreaID = menu.roomData.iAreaID
								MOVE_TO_ITEM_SELECTION_MENU(menu, i)
								menu.iSelectionIDs[CASINO_CPM_LEVEL_ROOMS][menu.iCurrentSelection[CASINO_CPM_LEVEL_ROOMS]] = i
								menu.iRoomPosSelection = 0
								bRoomFound = TRUE
								BREAKLOOP
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF menu.iRoomPosSelection >= menu.roomData.iMaxPostions
			menu.iRoomPosSelection = 0	
		ENDIF
		
		menu.iCPSelection =  menu.roomData.iPositions[menu.iRoomPosSelection]
		SET_BIT(menu.iBS,CASINO_CPM_BS_REFRESH_ITEM)
		SET_BIT(menu.iBS, CASINO_CPM_BS_RESTORE_LAST_ITEM)
		IF menu.iCurrentSelection[CASINO_CPM_LEVEL_ITEMS] >= 0
			menu.iSelectionToRestore = menu.iSelectionIDs[CASINO_CPM_LEVEL_ITEMS][menu.iCurrentSelection[CASINO_CPM_LEVEL_ITEMS]]
		ENDIF
		menu.iCurrentSelection[CASINO_CPM_LEVEL_ITEMS] = 0
		menu.iItemSecondPageStart = 0
		menu.iCurrentSelection_Top[CASINO_CPM_LEVEL_ITEMS] = 0
		CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SET_DEFAULT_OPTION)
		CLEAR_BIT(menu.iBS, CASINO_CPM_BS_HAS_SEC_MENU_PAGE)
		CLEAR_BIT(menu.iBS, CASINO_CPM_BS_USING_SEC_MENU_PAGE)
		PRINTLN("INCREASE_CASINO_CPM_ROOM_POS: menu.iCPSelection = ",menu.iCPSelection)
	ELSE
		PRINTLN("INCREASE_CASINO_CPM_ROOM_POS: room is < 0?? = ",menu.iCurrentSelection[CASINO_CPM_LEVEL_ROOMS] )
	ENDIF
ENDPROC

PROC DECREASE_CASINO_CPM_ROOM_POS(CASINO_CPM &menu, BOOL bUseGroupBehaviour)
	IF menu.iCurrentSelection[CASINO_CPM_LEVEL_ROOMS] >= 0
		GET_CPM_ROOM_DETAILS(menu.roomData, menu.iSelectionIDs[CASINO_CPM_LEVEL_ROOMS][menu.iCurrentSelection[CASINO_CPM_LEVEL_ROOMS]])
		
		IF NOT menu.roomData.bGroupedItems
		OR bUseGroupBehaviour
			menu.iRoomPosSelection--
		ELSE
			menu.iRoomPosSelection = -1
		ENDIF
		
		IF menu.iRoomPosSelection < 0
		
			IF bUseGroupBehaviour
				menu.iRoomPosSelection = menu.roomData.iMaxPostions-1
			ELSE
				// Since we have reached the beginning, see if we can switch to the previous room in the area
				IF menu.roomData.iAreaID != -1
					INT iNextRoom = menu.roomData.iRoomID
					INT i
					CASINO_CPM_ROOM_DATA tempRoomData
					BOOL bRoomFound = FALSE
					
					FOR i = iNextRoom-1 TO 0 STEP -1
						GET_CPM_ROOM_DETAILS(tempRoomData, i)
						IF tempRoomData.iMaxPostions > 0
						AND tempRoomData.iAreaID = menu.roomData.iAreaID
							MOVE_TO_ITEM_SELECTION_MENU(menu, i)
							menu.iSelectionIDs[CASINO_CPM_LEVEL_ROOMS][menu.iCurrentSelection[CASINO_CPM_LEVEL_ROOMS]] = i
							
							IF menu.roomData.bGroupedItems
								menu.iRoomPosSelection = 0
							ELSE
								menu.iRoomPosSelection = tempRoomData.iMaxPostions-1
							ENDIF
							
							bRoomFound = TRUE
							BREAKLOOP
						ENDIF
					ENDFOR
					
					IF NOT bRoomFound
						FOR i = CASINO_CPM_ROOM_MAX-1 TO iNextRoom STEP -1
							GET_CPM_ROOM_DETAILS(tempRoomData, i)
							IF tempRoomData.iMaxPostions > 0
							AND tempRoomData.iAreaID = menu.roomData.iAreaID
								MOVE_TO_ITEM_SELECTION_MENU(menu, i)
								menu.iSelectionIDs[CASINO_CPM_LEVEL_ROOMS][menu.iCurrentSelection[CASINO_CPM_LEVEL_ROOMS]] = i
								
								IF menu.roomData.bGroupedItems
									menu.iRoomPosSelection = 0
								ELSE
									menu.iRoomPosSelection = tempRoomData.iMaxPostions-1
								ENDIF
								
								bRoomFound = TRUE
								BREAKLOOP
							ENDIF
						ENDFOR
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF menu.iRoomPosSelection < 0
			menu.iRoomPosSelection = 0
		ENDIF
		
		menu.iCPSelection =  menu.roomData.iPositions[menu.iRoomPosSelection]
		SET_BIT(menu.iBS,CASINO_CPM_BS_REFRESH_ITEM)
		SET_BIT(menu.iBS, CASINO_CPM_BS_RESTORE_LAST_ITEM)
		IF menu.iCurrentSelection[CASINO_CPM_LEVEL_ITEMS] >= 0
			menu.iSelectionToRestore = menu.iSelectionIDs[CASINO_CPM_LEVEL_ITEMS][menu.iCurrentSelection[CASINO_CPM_LEVEL_ITEMS]]
		ENDIF
		menu.iCurrentSelection[CASINO_CPM_LEVEL_ITEMS] = 0
		menu.iItemSecondPageStart = 0
		menu.iCurrentSelection_Top[CASINO_CPM_LEVEL_ITEMS] = 0
		CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SET_DEFAULT_OPTION)
		CLEAR_BIT(menu.iBS, CASINO_CPM_BS_HAS_SEC_MENU_PAGE)
		CLEAR_BIT(menu.iBS, CASINO_CPM_BS_USING_SEC_MENU_PAGE)
		PRINTLN("DECREASE_CASINO_CPM_ROOM_POS: menu.iCPSelection = ",menu.iCPSelection)
	ELSE
		PRINTLN("DECREASE_CASINO_CPM_ROOM_POS: room is < 0?? = ",menu.iCurrentSelection[CASINO_CPM_LEVEL_ROOMS] )
	ENDIF
ENDPROC

FUNC BOOL HANDLE_CASINO_CPM_INPUT_DELAY(CASINO_CPM &menu, CONTROL_ACTION theInput)
	IF (NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,theInput)
	AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,theInput))
	OR HAS_NET_TIMER_EXPIRED(menu.inputDelay,250,TRUE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC CASINO_CPM_ITEM CPM_GET_CURRENT_SELECTED_ITEM(CASINO_CPM &menu)
	RETURN INT_TO_ENUM(CASINO_CPM_ITEM,menu.iSelectionIDs[CASINO_CPM_LEVEL_ITEMS][menu.iCurrentSelection[CASINO_CPM_LEVEL_ITEMS]])
ENDFUNC

PROC HANDLE_CASINO_CPM_DESCRIPTION(CASINO_CPM &menu)
	//item description
	IF menu.iLevel = CASINO_CPM_LEVEL_ITEMS
		CASINO_CPM_ITEM_DATA itemData
		GET_CASINO_CPM_ITEM_DATA_MASTER(itemData,CPM_GET_CURRENT_SELECTED_ITEM(menu))
		IF IS_BIT_SET(menu.iBS,CASINO_CPM_BS_NO_MONEY)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("CPM_NOCHIPS",100)
		ELIF IS_BIT_SET(menu.iBS,CASINO_CPM_BS_TRANSACTION_FAILED)
			SET_CURRENT_MENU_ITEM_DESCRIPTION("CPM_TRANSF",100)
		ELIF IS_BIT_SET(menu.iBS,CASINO_CPM_BS_CONFIRMATION)
		AND CPM_GET_PLAYER_OWNED_DECORATION_IN_POS(menu,menu.iCPSelection) != ENUM_TO_INT(CPM_GET_CURRENT_SELECTED_ITEM(menu))
			IF itemData.bOwned
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CPM_CONFOWN",100)
			ELSE
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CPM_BUY",100)
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(itemData.tlName)
				ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(itemData.iCost)
			ENDIF
		ELSE
			SET_CURRENT_MENU_ITEM_DESCRIPTION(itemData.tlDescription,100)
		ENDIF
	ELIF menu.iLevel = CASINO_CPM_LEVEL_ROOMS
		IF IS_MENU_ROW_SELECTABLE(menu.iCurrentSelection[menu.ilevel])
			SET_CURRENT_MENU_ITEM_DESCRIPTION("CPM_ROOM",100)
		ELSE
			SET_CURRENT_MENU_ITEM_DESCRIPTION("APT_DOOR_BLK_G",100)
		ENDIF
	ELIF menu.iLevel = CASINO_CPM_LEVEL_SUBPOSITIONS
		SET_CURRENT_MENU_ITEM_DESCRIPTION("CPM_ROOM",100)
	ELIF menu.iLevel = CASINO_CPM_LEVEL_ITEM_TYPES
		SET_CURRENT_MENU_ITEM_DESCRIPTION("CPM_SUBTYPE",100)
	ENDIF
ENDPROC

FUNC BOOL IS_SIZE_TYPE_SUITABLE_FOR_POS(CASINO_CPM_ITEM_DATA &itemData,CASINO_CPM_POSITION_DATA &posData)
	IF itemData.iSizeType = CPM_TYPE_WALL_ART
	AND posData.iType = CPM_TYPE_WALL_ART
		IF itemData.fMaxX <= posData.fMaxX
		AND itemData.fMaxY <= posData.fMaxY
		AND itemData.fMaxX >= posData.fMinX
		AND itemData.fMaxY >= posData.fMinY
			RETURN TRUE
		ENDIF
	ELSE
		IF itemData.iSizeType = posData.iType
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PLAYER_OWN_ROOM_FOR_DECORATIONS(CASINO_CPM_ROOM_DATA &tempRoomData)
	IF tempRoomData.iRoomID = CASINO_CPM_LOC_SPARE_BED
		RETURN HAS_PLAYER_PURCHASED_CASINO_APARTMENT_BEDROOM(PLAYER_ID())
	ELIF tempRoomData.iRoomID = CASINO_CPM_LOC_BAR
		RETURN HAS_PLAYER_PURCHASED_CASINO_APARTMENT_BAR(PLAYER_ID())
	ELIF tempRoomData.iAreaID = CASINO_CPM_AREA_DEALER_ROOM
		RETURN HAS_PLAYER_PURCHASED_CASINO_APARTMENT_DEALER(PLAYER_ID())
	ELIF tempRoomData.iRoomID = CASINO_CPM_LOC_MEDIA_ROOM
		RETURN HAS_PLAYER_PURCHASED_CASINO_APARTMENT_MEDIAROOM(PLAYER_ID())
	ELIF tempRoomData.iAreaID = CASINO_CPM_AREA_OFFICE
		RETURN HAS_PLAYER_PURCHASED_CASINO_APARTMENT_OFFICE(PLAYER_ID())
	ELIF tempRoomData.iRoomID = CASINO_CPM_LOC_SPA
		RETURN HAS_PLAYER_PURCHASED_CASINO_APARTMENT_SPA(PLAYER_ID())
	ELIF tempRoomData.iAreaID = CASINO_CPM_AREA_LOUNGE
		RETURN HAS_PLAYER_PURCHASED_CASINO_APARTMENT_LOUNGE(PLAYER_ID())
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC INT GET_AREA_FROM_ROOM(INT iRoomKey)
	SWITCH INT_TO_ENUM(CASINO_PENTHOUSE_ROOM_KEY,iRoomKey)
		CASE CAS_APT_ENTRY_ROOM				RETURN CASINO_CPM_AREA_ENTRANCE_HALL
		CASE CAS_APT_LOUNGE_ROOM 			RETURN CASINO_CPM_AREA_LOUNGE
		CASE CAS_APT_CINEMA_ROOM			RETURN CASINO_CPM_AREA_MEDIA_ROOM
		CASE CAS_APT_MEETING_ROOM			RETURN CASINO_CPM_AREA_LOUNGE
		CASE CAS_APT_SPARE_BEDROOM			RETURN CASINO_CPM_AREA_SPARE_BED
		CASE CAS_APT_SPARE_BATHROOM			RETURN CASINO_CPM_AREA_SPARE_BED
		CASE CAS_APT_PRIV_LOUNGE_ROOM		RETURN CASINO_CPM_AREA_MASTER_BED
		CASE CAS_APT_WARDROBE_ROOM			RETURN CASINO_CPM_AREA_MASTER_BED
		CASE CAS_APT_MAIN_BEDROOM			RETURN CASINO_CPM_AREA_MASTER_BED
		CASE CAS_APT_MAIN_BATHROOM			RETURN CASINO_CPM_AREA_MASTER_BED
		CASE CAS_APT_OFFICE_ROOM			RETURN CASINO_CPM_AREA_OFFICE
		CASE CAS_APT_SPA_CHANGE_ROOM		RETURN CASINO_CPM_AREA_SPA
		CASE CAS_APT_SPA_ROOM				RETURN CASINO_CPM_AREA_SPA
		CASE CAS_APT_BAR_ROOM				RETURN CASINO_CPM_AREA_BAR
	ENDSWITCH
	RETURN CASINO_CPM_AREA_ENTRANCE_HALL
ENDFUNC

FUNC INT GET_CASINO_DECORATION_LOCATION(CASINO_CPM &menu,INT iItem)
	INT iCP
	REPEAT CASINO_CPM_MAX_POS iCP
		IF CPM_GET_PLAYER_OWNED_DECORATION_IN_POS(menu,iCP) = iItem
			RETURN iCP
		ENDIF
	ENDREPEAT
	RETURN -1
ENDFUNC

PROC SETUP_CASINO_CPM(CASINO_CPM &menu)
	//CASINO_CPM menu
	INT iAreasBitset
	INT iMenuCounter
	INT iPageStartItem
	INT i
	INT iLocCount, iLocCurrent
	INt iCurrentRoom
	INT iRoomKey
	BOOL UseAsDefaultRoom
	
	INT iOwnedDecoration
	CASINO_CPM_ITEM_DATA itemData
	CASINO_CPM_ROOM_DATA tempRoomData
	CLEAR_MENU_DATA()
	
	SET_MENU_USES_HEADER_GRAPHIC(TRUE, "ShopUI_Title_Casino", "ShopUI_Title_Casino")
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT,MENU_ITEM_TEXT)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)
	IF menu.iLevel = CASINO_CPM_LEVEL_ITEMS
		iCurrentRoom = menu.iSelectionIDs[CASINO_CPM_LEVEL_ROOMS][menu.iCurrentSelection[CASINO_CPM_LEVEL_ROOMS]]
		iOwnedDecoration = CPM_GET_PLAYER_OWNED_DECORATION_IN_POS(menu,menu.iCPSelection)
		GET_CPM_ROOM_DETAILS(menu.roomData, iCurrentRoom)
		GET_CPM_AREA_DETAILS(menu.areaData, menu.roomData.iAreaID)
		
	ENDIF
	
	IF menu.iLevel = CASINO_CPM_LEVEL_ROOMS
		SET_MENU_TITLE("CPM_TITLE")
	ELIF menu.iLevel = CASINO_CPM_LEVEL_SUBPOSITIONS
		SET_MENU_TITLE("CPM_TITLE")
	ELIF menu.iLevel = CASINO_CPM_LEVEL_ITEM_TYPES
		SET_MENU_TITLE("CPM_TITLE")
	ELIF menu.iLevel = CASINO_CPM_LEVEL_ITEMS
		REPEAT CASINO_CPM_ROOM_MAX i
			IF GET_CPM_ROOM_AREA(i) = menu.roomData.iAreaID
				GET_CPM_ROOM_DETAILS(tempRoomData, i)
				IF tempRoomData.iMaxPostions > 0
					IF tempRoomData.bGroupedItems
						IF i = menu.iSelectionIDs[CASINO_CPM_LEVEL_ROOMS][menu.iCurrentSelection[CASINO_CPM_LEVEL_ROOMS]]
							iLocCurrent = 1+iLocCount
						ENDIF
						iLocCount += 1
					ELSE
						IF i = menu.iSelectionIDs[CASINO_CPM_LEVEL_ROOMS][menu.iCurrentSelection[CASINO_CPM_LEVEL_ROOMS]]
							iLocCurrent = 1+iLocCount+menu.iRoomPosSelection
						ENDIF
						iLocCount += tempRoomData.iMaxPostions
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF iLocCount > 1
			SET_MENU_TITLE("CPM_TITLE2b")
			ADD_MENU_TITLE_INT(iLocCurrent)
			ADD_MENU_TITLE_INT(iLocCount)
			ADD_MENU_TITLE_STRING(menu.areaData.tlTitle)
		ELSE
			SET_MENU_TITLE("CPM_TITLE2a")
			ADD_MENU_TITLE_STRING(menu.areaData.tlTitle)
		ENDIF
	ENDIF	
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("SETUP_CASINO_CPM: INIT")
	#ENDIF	
	#ENDIF
	IF menu.iLevel = CASINO_CPM_LEVEL_ROOMS

		REPEAT CASINO_CPM_ROOM_MAX i
			GET_CPM_ROOM_DETAILS(menu.roomData, i)
			IF menu.roomData.iMaxPostions > 0
			AND menu.roomData.iAreaID != -1
			AND NOT IS_BIT_SET(iAreasBitset, menu.roomData.iAreaID)
			
				IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_SET_DEFAULT_ROOM)
					iRoomKey = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())

					IF iRoomKey = menu.roomData.iRoomKey
						UseAsDefaultRoom = TRUE
					ELSE
						IF GET_AREA_FROM_ROOM(iRoomKey) = menu.roomData.iAreaID
							UseAsDefaultRoom = TRUE
						ENDIF
					ENDIF
					IF UseAsDefaultRoom
						menu.iCurrentSelection[menu.ilevel] = iMenuCounter
						PRINTLN("SETUP_CASINO_CPM: defaulted to room player is in",menu.iCurrentSelection[menu.ilevel])
						SET_BIT(menu.iBS,CASINO_CPM_BS_SET_DEFAULT_ROOM)
					ENDIF
				ENDIF
				
				GET_CPM_AREA_DETAILS(menu.areaData, menu.roomData.iAreaID)
				IF DOES_PLAYER_OWN_ROOM_FOR_DECORATIONS(menu.roomData)
					ADD_MENU_ITEM_TEXT(iMenuCounter,menu.areaData.tlName,DEFAULT,TRUE)
				ELSE
					ADD_MENU_ITEM_TEXT(iMenuCounter,menu.areaData.tlName,DEFAULT,FALSE)
				ENDIF
				
				SET_BIT(iAreasBitset, menu.roomData.iAreaID)
				menu.iSelectionIDs[menu.iLevel][iMenuCounter] = i
				iMenuCounter++
			ENDIF
		ENDREPEAT
		
	ELIF menu.iLevel = CASINO_CPM_LEVEL_SUBPOSITIONS
		
	ELIF menu.iLevel = CASINO_CPM_LEVEL_ITEM_TYPES
		
	ELIF menu.iLevel = CASINO_CPM_LEVEL_ITEMS
		IF menu.iCPSelection >= 1
			SET_CASINO_CPM_CAMERA(menu,menu.iCPSelection)
			GET_CPM_POSITION_DETAILS(menu.posData,menu.iCPSelection)	
			REPEAT MAX_CPM_ITEM_OPTIONS i
				menu.iSelectionIDs[CASINO_CPM_LEVEL_ITEMS][i] = 0
			ENDREPEAT
			IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_SET_DEFAULT_OPTION)
				REPEAT ENUM_TO_INT(CPM_MAX_ITEMS) i
					GET_CASINO_CPM_ITEM_SIZE_TYPE_MASTER(itemData,INT_TO_ENUM(CASINO_CPM_ITEM,i))
					//GET_CASINO_CPM_ITEM_DATA_MASTER(itemData,INT_TO_ENUM(CASINO_CPM_ITEM,i))
					IF i = 0
					OR (IS_SIZE_TYPE_SUITABLE_FOR_POS(itemData,menu.posData))
						GET_CASINO_CPM_ITEM_DATA_MASTER(itemData,INT_TO_ENUM(CASINO_CPM_ITEM,i))
						// Equipped
						IF (iOwnedDecoration > 0 AND iOwnedDecoration = i)
						// Available
						OR itemData.bUnlocked
						// Owned
						OR itemData.bOwned
						
							//PRINTLN("SETUP_CASINO_CPM: OWNED_DECORATION_IN_POS = ",GET_PLAYER_OWNED_DECORATION_IN_POS(menu.iCPSelection))
							IF iOwnedDecoration > 0
							AND iOwnedDecoration = i
								IF iMenuCounter >= MAX_CPM_ITEM_OPTIONS
									SET_BIT(menu.iBS, CASINO_CPM_BS_USING_SEC_MENU_PAGE)
									PRINTLN("SETUP_CASINO_CPM: default option on second page")
								ELSE
									PRINTLN("SETUP_CASINO_CPM: default option on first page")
									//i = ENUM_TO_INT(CPM_MAX_ITEMS)
								ENDIF
							ENDIF
							IF iMenuCounter >= MAX_CPM_ITEM_OPTIONS
							AND menu.iItemSecondPageStart = 0
								SET_BIT(menu.iBS, CASINO_CPM_BS_HAS_SEC_MENU_PAGE)
								menu.iItemSecondPageStart = i
								PRINTLN("SETUP_CASINO_CPM: this position has a second page of items starting at: ",menu.iItemSecondPageStart)
							ENDIF
							
							//PRINTLN("SETUP_CASINO_CPM: available menu item  counter# ",imenuCounter," itemID = ",i)
							iMenuCounter++
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("SETUP_CASINO_CPM: loop 1")
			#ENDIF	
			#ENDIF
			iMenuCounter = 0
			
			IF IS_BIT_SET(menu.iBS, CASINO_CPM_BS_HAS_SEC_MENU_PAGE)
			AND IS_BIT_SET(menu.iBS, CASINO_CPM_BS_USING_SEC_MENU_PAGE)
				iPageStartItem = menu.iItemSecondPageStart
				PRINTLN("SETUP_CASINO_CPM: this position has a second page of items starting loop at: ",iPageStartItem)
			ENDIF
			FOR i = iPageStartItem TO ENUM_TO_INT(CPM_MAX_ITEMS)
				GET_CASINO_CPM_ITEM_SIZE_TYPE_MASTER(itemData,INT_TO_ENUM(CASINO_CPM_ITEM,i))
				//GET_CASINO_CPM_ITEM_DATA_MASTER(itemData,INT_TO_ENUM(CASINO_CPM_ITEM,i))
				IF i = 0
				OR (IS_SIZE_TYPE_SUITABLE_FOR_POS(itemData,menu.posData))
					GET_CASINO_CPM_ITEM_DATA_MASTER(itemData,INT_TO_ENUM(CASINO_CPM_ITEM,i))
					
					IF iMenuCounter < MAX_CPM_ITEM_OPTIONS
						IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_SET_DEFAULT_OPTION)
							IF iOwnedDecoration > 0
							AND iOwnedDecoration = i
								menu.iCurrentSelection[menu.ilevel] = iMenuCounter
								PRINTLN("SETUP_CASINO_CPM: defaulted to currently owned selection #",menu.iCurrentSelection[menu.ilevel])
								SET_BIT(menu.iBS,CASINO_CPM_BS_SET_DEFAULT_OPTION)
							ELIF menu.iCurrentSelection[menu.ilevel] = 0
								menu.iCurrentSelection[menu.ilevel] = iMenuCounter
							ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
						IF i = 0
						AND IS_BIT_SET(menu.iBS,CASINO_CPM_BS_DRAW_DEBUG_MENU_INFO)
							TEXT_LABEL_23 tempText = "DB-CP#"
							tempText += menu.iCPSelection
							ADD_MENU_ITEM_TEXT(iMenuCounter,"MIS_CUST_TXT1",1,itemData.bUnlocked)
							ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(tempText)
						ELSE
						#ENDIF
							
							// Equipped
							IF iOwnedDecoration > 0 AND iOwnedDecoration = i
								ADD_MENU_ITEM_TEXT(iMenuCounter, itemData.tlName)
								ADD_MENU_ITEM_TEXT(iMenuCounter, "", 1)
								ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DECORATION)
								menu.iSelectionIDs[menu.iLevel][iMenuCounter] = i
								//PRINTLN("SETUP_CASINO_CPM: adding menu counter# ",imenuCounter," itemID = ",i)
								iMenuCounter++
								
							// Owned
							ELIF itemData.bOwned//gifted or purchased
								ADD_MENU_ITEM_TEXT(iMenuCounter, itemData.tlName)
								IF INT_TO_ENUM(CASINO_CPM_ITEM, i) != CPM_NONE
									INT iCasinoDecorationPos = GET_CASINO_DECORATION_LOCATION(menu,i)
									IF iCasinoDecorationPos != -1
									AND iCasinoDecorationPos != menu.iCPSelection
										ADD_MENU_ITEM_TEXT(iMenuCounter, "", 1)
										ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DECORATION_FADED)
									ELSE
										ADD_MENU_ITEM_TEXT(iMenuCounter, "", 1)
										ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_TICK)
									ENDIF
								ENDIF
								menu.iSelectionIDs[menu.iLevel][iMenuCounter] = i
								//PRINTLN("SETUP_CASINO_CPM: adding menu counter# ",imenuCounter," itemID = ",i)
								iMenuCounter++
							// Available
							ELIF itemData.bUnlocked
								IF itemData.iCost = 0
									ADD_MENU_ITEM_TEXT(iMenuCounter, itemData.tlName)
									ADD_MENU_ITEM_TEXT(iMenuCounter, "CPM_FREE")
								ELSE
									ADD_MENU_ITEM_TEXT(iMenuCounter, itemData.tlName)
									ADD_MENU_ITEM_TEXT(iMenuCounter, "CPM_COST", 2)
									ADD_MENU_ITEM_TEXT_COMPONENT_INT(itemData.iCost)
									ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_CHIPS)
								ENDIF
								menu.iSelectionIDs[menu.iLevel][iMenuCounter] = i
								//PRINTLN("SETUP_CASINO_CPM: adding menu counter# ",imenuCounter," itemID = ",i)
								iMenuCounter++
								
//							// Unavailable
//							ELSE
//								// TODO: Remove this line once we have the tunables set up.
//								ADD_MENU_ITEM_TEXT(iMenuCounter, itemData.tlName, DEFAULT, FALSE)
//								menu.iSelectionIDs[menu.iLevel][iMenuCounter] = i
//								iMenuCounter++
							ENDIF
						#IF IS_DEBUG_BUILD
						ENDIF
						#ENDIF
					ENDIF
				ENDIF
			ENDFOR
			#IF IS_DEBUG_BUILD
			#IF SCRIPT_PROFILER_ACTIVE
			ADD_SCRIPT_PROFILE_MARKER("SETUP_CASINO_CPM: loop 2")
			#ENDIF	
			#ENDIF
			IF IS_BIT_SET(menu.iBS, CASINO_CPM_BS_RESTORE_LAST_ITEM)
				IF menu.iSelectionToRestore != 0
					IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_SET_DEFAULT_OPTION)
						REPEAT MAX_CPM_ITEM_OPTIONS i
							IF menu.iSelectionIDs[CASINO_CPM_LEVEL_ITEMS][i] = menu.iSelectionToRestore
								menu.iCurrentSelection[menu.ilevel] = i
								PRINTLN("SETUP_CASINO_CPM: restoring selection #",menu.iCurrentSelection[menu.ilevel])
								SET_BIT(menu.iBS,CASINO_CPM_BS_SET_DEFAULT_OPTION)
							ENDIF
						ENDREPEAT
					ENDIF
				ENDIF
				CLEAR_BIT(menu.iBS, CASINO_CPM_BS_RESTORE_LAST_ITEM)
			ENDIF
		ELSE
			SCRIPT_ASSERT("SETUP_CASINO_CPM: in items menu with no item selected for cameras etc??")
			PRINTLN("SETUP_CASINO_CPM: in items menu with no item selected for cameras etc??")
		ENDIF
	ENDIF

	menu.iMaxSelection 	= iMenuCounter
	SET_BIT(menu.iBS,CASINO_CPM_BS_SET_DEFAULT_OPTION)
	
	SET_TOP_MENU_ITEM(menu.iCurrentSelection_Top[menu.ilevel])
	SET_CURRENT_MENU_ITEM(menu.iCurrentSelection[menu.ilevel])
	
	//Once
	REMOVE_MENU_HELP_KEYS()
	IF menu.iCurrentSelection[menu.ilevel] >= 0
	AND !IS_CASINO_DECORATION_PURCHASED(INT_TO_ENUM(CASINO_CPM_ITEM,menu.iSelectionIDs[CASINO_CPM_LEVEL_ITEMS][menu.iCurrentSelection[menu.ilevel]]))
	AND !IS_CASINO_DECORATION_GIFTED(INT_TO_ENUM(CASINO_CPM_ITEM,menu.iSelectionIDs[CASINO_CPM_LEVEL_ITEMS][menu.iCurrentSelection[menu.ilevel]]))
		ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_SELECT, "CPM_BUYB")
	ELSE
		IF IS_MENU_ROW_SELECTABLE(menu.iCurrentSelection[menu.ilevel])
			ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_SELECT, "BB_SELECT")
		ENDIF
	ENDIF
	ADD_MENU_HELP_KEY_INPUT( INPUT_CELLPHONE_CANCEL, "BB_BACK")
	IF menu.iLevel = CASINO_CPM_LEVEL_ITEMS
	AND NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_CONFIRMATION)
		IF iOwnedDecoration > 0 
		AND iOwnedDecoration = menu.iSelectionIDs[CASINO_CPM_LEVEL_ITEMS][menu.iCurrentSelection[menu.ilevel]]
			ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_RS, "CPM_REMOVE")
		ENDIF
		ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_RB, "CPM_POSN")
		ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_LB, "CPM_POSP")
		IF menu.roomData.bGroupedItems
			ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_RIGHT, "CPM_SPACEN")
			ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_LEFT, "CPM_SPACEP")
		ENDIF
		
		IF IS_BIT_SET(menu.iBS, CASINO_CPM_BS_HAS_SEC_MENU_PAGE)
		OR IS_BIT_SET(menu.iBS, CASINO_CPM_BS_USING_SEC_MENU_PAGE)
			ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_X, "CPM_PAGEN")
			ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_Y, "CPM_PAGEP")				
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(menu.iBS,CASINO_CPM_BS_CLEAR_TRANS_STATUS)
		CLEAR_BIT(menu.iBS,CASINO_CPM_BS_CLEAR_TRANS_STATUS)
		CLEAR_BIT(menu.iBS,CASINO_CPM_BS_TRANSACTION_FAILED)
		PRINTLN("SETUP_CASINO_CPM: clearing CASINO_CPM_BS_TRANSACTION_FAILED")
	ENDIF
	IF IS_BIT_SET(menu.iBS,CASINO_CPM_BS_TRANSACTION_FAILED)
		SET_BIT(menu.iBS,CASINO_CPM_BS_CLEAR_TRANS_STATUS)
		PRINTLN("SETUP_CASINO_CPM: setting CASINO_CPM_BS_CLEAR_TRANS_STATUS for next menu setup call")
	ENDIF
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("SETUP_CASINO_CPM: END")
	#ENDIF	
	#ENDIF
	PRINTLN("SETUP_CASINO_CPM: setup menu")
ENDPROC

PROC HANDLE_CASINO_CPM_ITEMS(CASINO_CPM &menu)
	IF NOT DOES_ENTITY_EXIST(menu.currentObj)
	OR IS_BIT_SET(menu.iBS,CASINO_CPM_BS_REFRESH_ITEM)
		CASINO_CPM_ITEM_DATA itemData
		CASINO_CPM_POSITION_DATA posData
		GET_CASINO_CPM_ITEM_DATA_MASTER(itemData,INT_TO_ENUM(CASINO_CPM_ITEM,menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[menu.ilevel]]))
		PRINTLN("HANDLE_CASINO_CPM_ITEMS: menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[",menu.ilevel,"] = ",menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[menu.ilevel]])
		IF itemData.theModel != DUMMY_MODEL_FOR_SCRIPT
			
			IF IS_BIT_SET(menu.iBS,CASINO_CPM_BS_REFRESH_ITEM)
				IF DOES_ENTITY_EXIST(menu.currentObj)
					DELETE_OBJECT(menu.currentObj)
					PRINTLN("HANDLE_CASINO_CPM_ITEMS: deleting previous object ")
				ENDIF
				CLEAR_BIT(menu.iBS,CASINO_CPM_BS_REFRESH_ITEM)
			ENDIF
			
			IF REQUEST_LOAD_MODEL(itemData.theModel)
				IF NOT DOES_ENTITY_EXIST(menu.currentObj)
					GET_CPM_POSITION_DETAILS(posData,menu.iCPSelection)
					menu.currentObj = CREATE_OBJECT_NO_OFFSET(itemData.theModel,posData.vObjLoc,FALSE,FALSE,TRUE)
					IF posData.iRoomKey != 0
						FORCE_ROOM_FOR_ENTITY(menu.currentObj,menu.interiorID,posData.iRoomKey)
					ENDIF
					IF itemData.iTintIndex != -1
						SET_OBJECT_TINT_INDEX(menu.currentObj,itemData.iTintIndex)
					ENDIF
					FREEZE_ENTITY_POSITION(menu.currentObj ,true)
					SET_ENTITY_ROTATION(menu.currentObj,posData.vObjRot)
					SET_MODEL_AS_NO_LONGER_NEEDED(itemData.theModel)
					PRINTLN("HANDLE_CASINO_CPM_ITEMS: creating ",ENUM_TO_INT(itemData.theModel)," at: ",posData.vObjLoc)
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(menu.currentObj)
				DELETE_OBJECT(menu.currentObj)
				PRINTLN("HANDLE_CASINO_CPM_ITEMS: deleting previous object 2")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CAN_ACCEPT_CPM_INPUT()
	IF IS_PAUSE_MENU_ACTIVE()
	OR IS_SCRIPT_HUD_DISPLAYING(HUDPART_TRANSITIONHUD)
	OR g_sTransitionSessionData.sEndReserve.bTranCleanUpEndReservingSlot
	OR (IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_IN() OR IS_SCREEN_FADING_OUT())
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC 


FUNC BOOL RUN_CASINO_CPM(CASINO_CPM &menu)
	CASINO_CPM_ITEM_DATA itemData
	INT iCP
	BOOL bPlacedElsewhere
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR IS_PLAYER_IN_CORONA()
	OR NOT IS_NET_PLAYER_OK(PLAYER_ID())
	OR g_bMissionEnding
	OR g_bAbortPropertyMenus
	OR g_TransitionSessionNonResetVars.sGlobalCelebrationData.bDoingHeistCelebration
	OR IS_BROWSER_OPEN()
	OR IS_COMMERCE_STORE_OPEN()
	OR IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()
	OR IS_PLAYER_SWITCH_IN_PROGRESS()
	OR NOT (IS_PLAYER_IN_CASINO_APARTMENT_THEY_OWN(PLAYER_ID()) AND GET_INTERIOR_FLOOR_INDEX() = ciCASINO_APT_FLOOR_APARTMENT)
		IF g_bAbortPropertyMenus
			PRINTLN("RUN_CASINO_CPM: aborting menu. g_bAbortPropertyMenus = TRUE")
		ENDIF
		PRINTLN("RUN_CASINO_CPM: - NETWORK_IS_GAME_IN_PROGRESS = FALSE ") NET_NL()
		CLEANUP_CASINO_CPM(menu)
		RETURN TRUE
	ENDIF
	
	IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_SET_CUR_POSITIONS)
		REPEAT CASINO_CPM_MAX_POS iCP
			menu.iOwnedDecorations[iCP] = GET_PLAYER_OWNED_DECORATION_IN_POS(iCP)
		ENDREPEAT
		SET_BIT(menu.iBS,CASINO_CPM_BS_SET_CUR_POSITIONS)
	ENDIF
	
	IF Get_Peds_Drunk_Alcohol_Hit_Count(PLAYER_PED_ID()) >= 9
	OR g_SpawnData.bPassedOutDrunk
	OR IS_PED_RAGDOLL(PLAYER_PED_ID())
		PRINTLN("RUN_CASINO_CPM: - player drunk aborting ") NET_NL()
		CLEANUP_CASINO_CPM(menu)
		RETURN TRUE
	ENDIF
	
	HIDE_HELP_TEXT_THIS_FRAME()
	DISABLE_DPADDOWN_THIS_FRAME()
	DISABLE_SELECTOR_THIS_FRAME()

	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CELLPHONE_LEFT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CELLPHONE_RIGHT)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CELLPHONE_SCROLL_FORWARD)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_CELLPHONE_SCROLL_BACKWARD)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_X)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_Y)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_RS)
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_ATTACK)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_ATTACK2)
	
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
		ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
	
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_LOOK_UD)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_LOOK_LR)
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		
		IF NOT IS_PAUSE_MENU_ACTIVE() AND NOT IS_WARNING_MESSAGE_ACTIVE()
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
			SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
		ENDIF 

		HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
		SET_MOUSE_CURSOR_THIS_FRAME()		
	ENDIF
	
	IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_REMOVED_CONTROL)
		NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
		DISPLAY_RADAR(FALSE)
		SET_BIT(menu.iBS,CASINO_CPM_BS_REMOVED_CONTROL)
	ENDIF	
	IF menu.iLevel = CASINO_CPM_LEVEL_ITEMS
		IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_SET_IN_CUTSCENE)
			SET_LOCAL_PLAYER_VISIBLE_IN_CUTSCENE(FALSE,TRUE)
			START_MP_CUTSCENE(TRUE)
			SET_BIT(menu.iBS,CASINO_CPM_BS_SET_IN_CUTSCENE)
			PRINTLN("RUN_CASINO_CPM: triggering cutscene for viewing items")
		ENDIF	
	ELSE
		IF IS_BIT_SET(menu.iBS,CASINO_CPM_BS_SET_IN_CUTSCENE)
			CLEANUP_MP_CUTSCENE(FALSE,FALSE)
			PRINTLN("RUN_CASINO_CPM: clearing cutscene not viewing items")
			CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SET_IN_CUTSCENE)
		ENDIF
	ENDIF
	REQUEST_STREAMED_TEXTURE_DICT("ShopUI_Title_Casino")
	SET_BIT(menu.iBS,CASINO_CPM_BS_LOADED_MENU_HEADER)
	
	IF LOAD_MENU_ASSETS()
	AND HAS_STREAMED_TEXTURE_DICT_LOADED("ShopUI_Title_Casino")
		IF IS_NET_PLAYER_OK(PLAYER_ID())
			IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
				SETUP_CASINO_CPM(menu)
				SET_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
				HANDLE_CASINO_CPM_DESCRIPTION(menu)
				DRAW_MENU()
				SHOW_CHIPS_HUD_THIS_FRAME()
			ELSE
			
				IF IS_BIT_SET(menu.iBS,CASINO_CPM_BS_CHIP_TRANS_PEND)
				OR IS_BIT_SET(menu.iBS,CASINO_CPM_BS_CASH_TRANS_PEND)
					GET_CASINO_CPM_ITEM_DATA_MASTER(itemData,CPM_GET_CURRENT_SELECTED_ITEM(menu))
					IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_CASH_TRANS_PEND)
						PRINTLN("RUN_CASINO_CPM:2 continuing chip transaction for ",itemData.theModel)
						IF REMOVE_CASINO_CHIPS_FROM_LOCAL_PLAYER_FOR_SHOP_ITEM(itemData.iCost,menu.removeChipTransactionState, 1, GET_HASH_KEY(itemData.tlName))
							IF menu.removeChipTransactionState = TRANSACTION_STATE_SUCCESS
								SET_BIT(menu.iBS,CASINO_CPM_BS_CASH_TRANS_PEND)
								PRINTLN("RUN_CASINO_CPM:2 starting cash transaction for ",itemData.theModel)
								IF PROCESS_CASINO_DECORATION_ITEM_TRANSACTION(CPM_GET_CURRENT_SELECTED_ITEM(menu),menu.iRemoveCashTransactionState)
									IF menu.iRemoveCashTransactionState = GENERIC_TRANSACTION_STATE_SUCCESS
										SET_CASINO_DECORATION_ITEM_STAT(INT_TO_ENUM(CASINO_CPM_ITEM, menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[menu.ilevel]]))
										CPM_SET_PLAYER_OWNED_DECORATION_IN_POS(menu,menu.iCPSelection,menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[menu.ilevel]])
										CLEAR_CASINO_CPM_TRANSACTION(menu)
										PRINTLN("RUN_CASINO_CPM:2 finished purchase of ",itemData.theModel)
									ELIF menu.iRemoveCashTransactionState = GENERIC_TRANSACTION_STATE_FAILED
										CLEAR_CASINO_CPM_TRANSACTION(menu)
										SET_BIT(menu.iBS,CASINO_CPM_BS_TRANSACTION_FAILED)
										PRINTLN("RUN_CASINO_CPM:2 CASH TRANSACTION FAILED FOR ",itemData.theModel)
									ENDIF
								ENDIF
							ELIF menu.iRemoveCashTransactionState = GENERIC_TRANSACTION_STATE_FAILED
								CLEAR_CASINO_CPM_TRANSACTION(menu)
								SET_BIT(menu.iBS,CASINO_CPM_BS_TRANSACTION_FAILED)
								PRINTLN("RUN_CASINO_CPM:2 CHIP TRANSACTION FAILED FOR ",itemData.theModel)	
							ENDIF
						ENDIF
					ELSE
						PRINTLN("RUN_CASINO_CPM: continuing cash transaction for ",itemData.theModel)
						IF PROCESS_CASINO_DECORATION_ITEM_TRANSACTION(CPM_GET_CURRENT_SELECTED_ITEM(menu),menu.iRemoveCashTransactionState)
							IF menu.iRemoveCashTransactionState = GENERIC_TRANSACTION_STATE_SUCCESS
								SET_CASINO_DECORATION_ITEM_STAT(INT_TO_ENUM(CASINO_CPM_ITEM, menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[menu.ilevel]]))
								CPM_SET_PLAYER_OWNED_DECORATION_IN_POS(menu,menu.iCPSelection,menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[menu.ilevel]])
								CLEAR_CASINO_CPM_TRANSACTION(menu)
								PRINTLN("RUN_CASINO_CPM: 1 finished purchase of ",itemData.theModel)
							ELIF menu.iRemoveCashTransactionState = GENERIC_TRANSACTION_STATE_FAILED
								CLEAR_CASINO_CPM_TRANSACTION(menu)
								SET_BIT(menu.iBS,CASINO_CPM_BS_TRANSACTION_FAILED)
								PRINTLN("RUN_CASINO_CPM: 1 CASH TRANSACTION FAILED FOR ",itemData.theModel)
							ENDIF
						ENDIF
					ENDIF
					HANDLE_CASINO_CPM_DESCRIPTION(menu)

					DRAW_MENU()
					SHOW_CHIPS_HUD_THIS_FRAME()
				ELIF CAN_ACCEPT_CPM_INPUT()
					
					IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_PRESSED_ACCEPT)
						IF IS_CELLPHONE_ACCEPT_JUST_PRESSED_EXCLUDING_MOUSE()
						OR (IS_MENU_CURSOR_ACCEPT_PRESSED() AND (g_iMenuCursorItem = menu.iCurrentSelection[menu.ilevel]))
						OR (IS_WARNING_MESSAGE_ACTIVE() AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
							
							PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
							//PRINTLN("CDM:menu.iCurrentSelection ", menu.iCurrentSelection[menu.ilevel])
							IF menu.iLevel = CASINO_CPM_LEVEL_ROOMS
							AND IS_MENU_ROW_SELECTABLE(menu.iCurrentSelection[menu.ilevel])
								MOVE_TO_ITEM_SELECTION_MENU(menu,menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[menu.ilevel]])
								SET_BIT(menu.iBS,CASINO_CPM_BS_REFRESH_ITEM)
								CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
							ELIF menu.iLevel = CASINO_CPM_LEVEL_ITEMS
								IF CPM_GET_PLAYER_OWNED_DECORATION_IN_POS(menu,menu.iCPSelection) != menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[menu.ilevel]]
									GET_CASINO_CPM_ITEM_DATA_MASTER(itemData,CPM_GET_CURRENT_SELECTED_ITEM(menu))
									
									IF itemData.bOwned
										
										REPEAT CASINO_CPM_MAX_POS iCP
											IF CPM_GET_PLAYER_OWNED_DECORATION_IN_POS(menu,iCP) = menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[menu.ilevel]]
												bPlacedElsewhere = TRUE
											ENDIF
										ENDREPEAT
										IF bPlacedElsewhere
										AND NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_CONFIRMATION)
											SET_BIT(menu.iBS,CASINO_CPM_BS_CONFIRMATION)
											CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
										ELSE
											CPM_SET_PLAYER_OWNED_DECORATION_IN_POS(menu,menu.iCPSelection,menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[menu.ilevel]])
											CLEAR_BIT(menu.iBS,CASINO_CPM_BS_CONFIRMATION)
											CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
										ENDIF
									ELSE
										IF GET_LOCAL_PLAYERS_CASINO_CHIPS_TOTAL() >= itemData.iCost
											CLEAR_BIT(menu.iBS,CASINO_CPM_BS_NO_MONEY)
											IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_CONFIRMATION)
											AND itemData.iCost > 0
												SET_BIT(menu.iBS,CASINO_CPM_BS_CONFIRMATION)
												CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
											ELSE
												IF itemData.iCost <= 0
													SET_BIT(menu.iBS,CASINO_CPM_BS_CASH_TRANS_PEND)
													PRINTLN("RUN_CASINO_CPM:3 starting cash transaction for ",itemData.theModel)
													IF PROCESS_CASINO_DECORATION_ITEM_TRANSACTION(CPM_GET_CURRENT_SELECTED_ITEM(menu),menu.iRemoveCashTransactionState)
														IF menu.iRemoveCashTransactionState = GENERIC_TRANSACTION_STATE_SUCCESS
															SET_CASINO_DECORATION_ITEM_STAT(INT_TO_ENUM(CASINO_CPM_ITEM, menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[menu.ilevel]]))
															CPM_SET_PLAYER_OWNED_DECORATION_IN_POS(menu,menu.iCPSelection,menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[menu.ilevel]])
															CLEAR_CASINO_CPM_TRANSACTION(menu)
															PRINTLN("RUN_CASINO_CPM:3 finished purchase of ",itemData.theModel)
														ELIF menu.iRemoveCashTransactionState = GENERIC_TRANSACTION_STATE_FAILED
															CLEAR_CASINO_CPM_TRANSACTION(menu)
															SET_BIT(menu.iBS,CASINO_CPM_BS_TRANSACTION_FAILED)
															PRINTLN("RUN_CASINO_CPM:3CASH TRANSACTION FAILED FOR ",itemData.theModel)
														ENDIF
													ENDIF
												ELSE
													SET_BIT(menu.iBS,CASINO_CPM_BS_CHIP_TRANS_PEND)
													PRINTLN("RUN_CASINO_CPM:1 starting chip transaction for ",itemData.theModel)
													IF REMOVE_CASINO_CHIPS_FROM_LOCAL_PLAYER_FOR_SHOP_ITEM(itemData.iCost, menu.removeChipTransactionState, 1, GET_HASH_KEY(itemData.tlName))
														IF menu.removeChipTransactionState = TRANSACTION_STATE_SUCCESS
															SET_BIT(menu.iBS,CASINO_CPM_BS_CASH_TRANS_PEND)
															PRINTLN("RUN_CASINO_CPM:1 starting cash transaction for ",itemData.theModel)
															IF PROCESS_CASINO_DECORATION_ITEM_TRANSACTION(CPM_GET_CURRENT_SELECTED_ITEM(menu),menu.iRemoveCashTransactionState)
																IF menu.iRemoveCashTransactionState = GENERIC_TRANSACTION_STATE_SUCCESS
																	SET_CASINO_DECORATION_ITEM_STAT(INT_TO_ENUM(CASINO_CPM_ITEM, menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[menu.ilevel]]))
																	CPM_SET_PLAYER_OWNED_DECORATION_IN_POS(menu,menu.iCPSelection,menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[menu.ilevel]])
																	CLEAR_CASINO_CPM_TRANSACTION(menu)
																	PRINTLN("RUN_CASINO_CPM:1 finished purchase of ",itemData.theModel)
																ELIF menu.iRemoveCashTransactionState = GENERIC_TRANSACTION_STATE_FAILED
																	CLEAR_CASINO_CPM_TRANSACTION(menu)
																	SET_BIT(menu.iBS,CASINO_CPM_BS_TRANSACTION_FAILED)
																	PRINTLN("RUN_CASINO_CPM:1 CASH TRANSACTION FAILED FOR ",itemData.theModel)
																ENDIF
															ENDIF
														ELIF menu.iRemoveCashTransactionState = GENERIC_TRANSACTION_STATE_FAILED
															CLEAR_CASINO_CPM_TRANSACTION(menu)
															SET_BIT(menu.iBS,CASINO_CPM_BS_TRANSACTION_FAILED)
															PRINTLN("RUN_CASINO_CPM:1 CHIP TRANSACTION FAILED FOR ",itemData.theModel)	
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ELSE
											SET_BIT(menu.iBS,CASINO_CPM_BS_NO_MONEY)
										ENDIF
//									ELSE
//										CLEAR_BIT(menu.iBS,CASINO_CPM_BS_NO_MONEY)
//										IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_CONFIRMATION)
//											SET_BIT(menu.iBS,CASINO_CPM_BS_CONFIRMATION)
//											CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
//										ELSE
//											SET_CASINO_DECORATION_ITEM_STAT(INT_TO_ENUM(CASINO_CPM_ITEM, menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[menu.ilevel]]))
//											CPM_SET_PLAYER_OWNED_DECORATION_IN_POS(menu,menu.iCPSelection,menu.iSelectionIDs[menu.iLevel][menu.iCurrentSelection[menu.ilevel]])
//											CLEAR_BIT(menu.iBS,CASINO_CPM_BS_CONFIRMATION)
//											CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
//										ENDIF
									ENDIF
								ENDIF
							ENDIF
							SET_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_ACCEPT)
						ENDIF
					ELSE 
						IF NOT IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_CELLPHONE_SELECT)
						AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_SELECT)
							CLEAR_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_ACCEPT)
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_PRESSED_CANCEL)
						IF IS_CELLPHONE_CANCEL_JUST_PRESSED_EXCLUDING_MOUSE()
						OR (IS_MENU_CURSOR_CANCEL_PRESSED() AND (g_iMenuCursorItem > MENU_CURSOR_NO_ITEM))
						OR (IS_WARNING_MESSAGE_ACTIVE() AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
							PLAY_SOUND_FRONTEND(-1, "CANCEL","HUD_FREEMODE_SOUNDSET")
							IF menu.iLevel = CASINO_CPM_LEVEL_ROOMS
								NET_PRINT("RUN_CASINO_CPM player exited menu") NET_NL()
								CLEANUP_CASINO_CPM(menu)
								RETURN TRUE
							ELIF menu.iLevel = CASINO_CPM_LEVEL_ITEMS
								IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_CONFIRMATION)
									MOVE_TO_ROOM_SELECTION(menu)
									CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
								ELSE
									CLEAR_BIT(menu.iBS,CASINO_CPM_BS_CONFIRMATION)
									CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
								ENDIF
							ENDIF
							SET_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_CANCEL)
						ENDIF
					ELSE
						IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
							CLEAR_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_CANCEL)
						ENDIF
					ENDIF
					IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_CONFIRMATION)
						IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_PRESSED_UP)
							IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_UP)
							OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_UP)
							OR IS_MENU_CURSOR_SCROLL_UP_PRESSED()
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
								menu.iCurrentSelection[menu.ilevel]--
								menu.iCurrentSelection_Top[menu.ilevel] = GET_TOP_MENU_ITEM()
								SET_BIT(menu.iBS,CASINO_CPM_BS_REFRESH_ITEM)
								SET_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_UP)
								CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
								RESET_NET_TIMER(menu.inputDelay)
							ENDIF
						ELSE
							IF HANDLE_CASINO_CPM_INPUT_DELAY(menu,INPUT_CELLPHONE_UP)
								CLEAR_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_UP)	
							ENDIF
						ENDIF
						IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_PRESSED_DOWN)
							IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_DOWN)
							OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_DOWN)
							OR IS_MENU_CURSOR_SCROLL_DOWN_PRESSED()
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
								menu.iCurrentSelection[menu.ilevel]++
								menu.iCurrentSelection_Top[menu.ilevel] = GET_TOP_MENU_ITEM()
								SET_BIT(menu.iBS,CASINO_CPM_BS_REFRESH_ITEM)
								SET_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_DOWN)
								CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
								RESET_NET_TIMER(menu.inputDelay)
							ENDIF
						ELSE
							IF HANDLE_CASINO_CPM_INPUT_DELAY(menu,INPUT_CELLPHONE_DOWN)
								CLEAR_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_DOWN)	
							ENDIF
						ENDIF
						IF menu.iLevel = CASINO_CPM_LEVEL_ITEMS
							IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_PRESSED_RS)
								IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RS)
								OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RS)
									IF CPM_GET_PLAYER_OWNED_DECORATION_IN_POS(menu,menu.iCPSelection) > 0
									AND CPM_GET_PLAYER_OWNED_DECORATION_IN_POS(menu,menu.iCPSelection) = menu.iSelectionIDs[CASINO_CPM_LEVEL_ITEMS][menu.iCurrentSelection[menu.ilevel]]
										PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
										CPM_SET_PLAYER_OWNED_DECORATION_IN_POS(menu,menu.iCPSelection,0)
										CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
										SET_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_RS)
										RESET_NET_TIMER(menu.inputDelay)
										PRINTLN("RUN_CASINO_CPM: removed currently equipped item")
									ENDIF
								ENDIF
							ELSE
								IF HANDLE_CASINO_CPM_INPUT_DELAY(menu,INPUT_FRONTEND_RS)
									CLEAR_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_RS)	
								ENDIF
							ENDIF
						
							IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_LB)
							OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_lB)
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
								DECREASE_CASINO_CPM_ROOM_POS(menu, FALSE)
								SET_BIT(menu.iBS,CASINO_CPM_BS_REFRESH_ITEM)
								CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
								RESET_NET_TIMER(menu.inputDelay)
							ENDIF
							IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_RB)
							OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_FRONTEND_RB)
								PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
								INCREASE_CASINO_CPM_ROOM_POS(menu, FALSE)
								SET_BIT(menu.iBS,CASINO_CPM_BS_REFRESH_ITEM)
								CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
								RESET_NET_TIMER(menu.inputDelay)
							ENDIF
							IF menu.roomData.bGroupedItems
								IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_PRESSED_LEFT)
									IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_LEFT)
									OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_LEFT)
										PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
										DECREASE_CASINO_CPM_ROOM_POS(menu, TRUE)
										SET_BIT(menu.iBS,CASINO_CPM_BS_REFRESH_ITEM)
										CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
										SET_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_LEFT)
										RESET_NET_TIMER(menu.inputDelay)
									ENDIF
								ELSE
									IF HANDLE_CASINO_CPM_INPUT_DELAY(menu,INPUT_CELLPHONE_LEFT)
										CLEAR_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_LEFT)	
									ENDIF
								ENDIF
								IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_PRESSED_RIGHT)
									IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_RIGHT)
									OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CELLPHONE_RIGHT)
										PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
										INCREASE_CASINO_CPM_ROOM_POS(menu, TRUE)
										SET_BIT(menu.iBS,CASINO_CPM_BS_REFRESH_ITEM)
										CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
										SET_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_RIGHT)
										RESET_NET_TIMER(menu.inputDelay)
									ENDIF
								ELSE
									IF HANDLE_CASINO_CPM_INPUT_DELAY(menu,INPUT_CELLPHONE_RIGHT)
										CLEAR_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_RIGHT)	
									ENDIF
								ENDIF
							ENDIF
							IF IS_BIT_SET(menu.iBS, CASINO_CPM_BS_HAS_SEC_MENU_PAGE)
								IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_PRESSED_X)
									IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_X)
									OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_X)
										
										PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
										IF IS_BIT_SET(menu.iBS,CASINO_CPM_BS_USING_SEC_MENU_PAGE)
											CLEAR_BIT(menu.iBS,CASINO_CPM_BS_USING_SEC_MENU_PAGE)
										ELSE
											SET_BIT(menu.iBS,CASINO_CPM_BS_USING_SEC_MENU_PAGE)
										ENDIF
										menu.iCurrentSelection[CASINO_CPM_LEVEL_ITEMS] = 0
										SET_BIT(menu.iBS,CASINO_CPM_BS_REFRESH_ITEM)
										CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
										SET_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_X)
										RESET_NET_TIMER(menu.inputDelay)
									ENDIF
								ELSE
									IF HANDLE_CASINO_CPM_INPUT_DELAY(menu,INPUT_FRONTEND_X)
										CLEAR_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_X)
									ENDIF
								ENDIF
								IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_PRESSED_X)
									IF NOT IS_BIT_SET(menu.iBS,CASINO_CPM_BS_PRESSED_Y)
										IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_Y)
										OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_Y)
											
											PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN","HUD_FREEMODE_SOUNDSET")
											IF IS_BIT_SET(menu.iBS,CASINO_CPM_BS_USING_SEC_MENU_PAGE)
												CLEAR_BIT(menu.iBS,CASINO_CPM_BS_USING_SEC_MENU_PAGE)
											ELSE
												SET_BIT(menu.iBS,CASINO_CPM_BS_USING_SEC_MENU_PAGE)
											ENDIF
											menu.iCurrentSelection[CASINO_CPM_LEVEL_ITEMS] = 0
											SET_BIT(menu.iBS,CASINO_CPM_BS_REFRESH_ITEM)
											CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
											SET_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_Y)
											RESET_NET_TIMER(menu.inputDelay)
										ENDIF
									ELSE
										IF HANDLE_CASINO_CPM_INPUT_DELAY(menu,INPUT_FRONTEND_Y)
											CLEAR_BIT(menu.iBS,CASINO_CPM_BS_PRESSED_Y)	
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					// handle clicking on a new highlighted option
					IF IS_MENU_CURSOR_ACCEPT_PRESSED()
						IF (g_iMenuCursorItem != menu.iCurrentSelection[menu.ilevel])
							menu.iCurrentSelection[menu.ilevel] = g_iMenuCursorItem
							menu.iCurrentSelection_Top[menu.ilevel] = GET_TOP_MENU_ITEM()
							SET_BIT(menu.iBS,CASINO_CPM_BS_REFRESH_ITEM)
							CLEAR_BIT(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
							//SET_CURRENT_MENU_ITEM(menu.iCurrentSelection[menu.ilevel])
							PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FREEMODE_SOUNDSET")
						ENDIF
					ENDIF
					
					IF menu.iCurrentSelection[menu.ilevel] >= menu.iMaxSelection
						menu.iCurrentSelection[menu.ilevel] = 0
						menu.iCurrentSelection_Top[menu.ilevel] = 0
					ENDIF
					IF menu.iCurrentSelection[menu.ilevel] < 0
					
						menu.iCurrentSelection[menu.ilevel] = menu.iMaxSelection-1
						IF menu.iCurrentSelection[menu.ilevel] < 0
							menu.iCurrentSelection[menu.ilevel] = 0
						ENDIF
					ENDIF

					HANDLE_CASINO_CPM_DESCRIPTION(menu)

					DRAW_MENU()
					SHOW_CHIPS_HUD_THIS_FRAME()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF menu.iLevel = CASINO_CPM_LEVEL_ITEMS
	AND IS_BIT_SET(menu.iBS,CASINO_CPM_BS_SETUP_MENU)
		HANDLE_CASINO_CPM_ITEMS(menu)
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF
