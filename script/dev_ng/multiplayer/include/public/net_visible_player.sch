USING "globals.sch"
USING "commands_network.sch"
USING "script_network.sch"
	
#IF IS_DEBUG_BUILD
//BOOL bVisiblePlayersDebug[NUM_NETWORK_PLAYERS]	// Holds which players I can see.


PROC Create_Visible_Player_Widget()

	INT iParticipant
	TEXT_LABEL_63 tl63
	
	START_WIDGET_GROUP("Visible Players") 
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
//			tl63 = "Player "
//			tl63 += iParticipant
//			tl63 += " iVisiblePlayers"
//			ADD_BIT_FIELD_WIDGET(tl63, GlobalplayerBD[iParticipant].iVisiblePlayers)
//			
//			tl63 = "Player "
//			tl63 += iParticipant
//			tl63 += " iVisibleToScriptPlayers"
//			ADD_BIT_FIELD_WIDGET(tl63, GlobalplayerBD[iParticipant].iVisibleToScriptPlayers)
			
			tl63 = "Player "
			tl63 += iParticipant
			tl63 += " bPlayerIsHiddenForWarp"
			ADD_WIDGET_BOOL(tl63, GlobalplayerBD[iParticipant].bPlayerIsHiddenForWarp)
			
			tl63 = "Player "
			tl63 += iParticipant
			tl63 += " bConsiderPlayerForVisibleChecks"
			ADD_WIDGET_BOOL(tl63, GlobalplayerBD[iParticipant].bConsiderPlayerForVisibleChecks)
			
			tl63 = "Player "
			tl63 += iParticipant
			tl63 += " g_iTrackedPedPixelHeight"
			ADD_WIDGET_INT_SLIDER(tl63, MPGlobals.g_iTrackedPedPixelHeight[iParticipant], -1, HIGHEST_INT, 1)
			
			//ADD_WIDGET_BOOL(tl63, bVisiblePlayersDebug[iParticipant])
		ENDREPEAT	
	STOP_WIDGET_GROUP()

ENDPROC

#ENDIF

//PROC REGISTER_PLAYER_FOR_VISIBLITY_CHECK(PLAYER_INDEX PlayerId)
//	REQUEST_PED_VISIBILITY_TRACKING(GET_PLAYER_PED(PlayerID))
//	MPGlobals.g_bHasRegisteredPedVisibleCheck[NATIVE_TO_INT(PlayerID)] = TRUE
//	MPGlobals.g_iPedIDRegisteredForVisibleCheck[NATIVE_TO_INT(PlayerID)] = NATIVE_TO_INT(GET_PLAYER_PED(PlayerID))	
//ENDPROC

FUNC BOOL IS_PLAYER_VISIBLE_TO_SCRIPT(PLAYER_INDEX PlayerID)
	IF IS_ENTITY_VISIBLE_TO_SCRIPT(GET_PLAYER_PED(PlayerID))
	OR (GlobalplayerBD[NATIVE_TO_INT(PlayerID)].bConsiderPlayerForVisibleChecks)
		
		#IF IS_DEBUG_BUILD
			IF (GlobalplayerBD[NATIVE_TO_INT(PlayerID)].bConsiderPlayerForVisibleChecks)
				NET_PRINT("IS_PLAYER_VISIBLE_TO_SCRIPT - player ")
				NET_PRINT_INT(NATIVE_TO_INT(PlayerID))
				NET_PRINT(" bConsiderPlayerForVisibleChecks = TRUE")
				NET_NL()
			ENDIF
		#ENDIF
	
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC



FUNC BOOL IsAnotherVisiblePlayerInVehicle(VEHICLE_INDEX vehicle, INT iThisPlayer)

	INT i
	INT iMaxSeats
	
	iMaxSeats = (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(vehicle) + 1) //Add 1 for the driver
	
	PED_INDEX VehPedID
	PLAYER_INDEX PlayerID
	
	REPEAT iMaxSeats i

		IF NOT IS_VEHICLE_SEAT_FREE(vehicle, INT_TO_ENUM(VEHICLE_SEAT, i-1))
			VehPedID = GET_PED_IN_VEHICLE_SEAT(vehicle, INT_TO_ENUM(VEHICLE_SEAT, i-1))
		ENDIF		
		
		IF DOES_ENTITY_EXIST(VehPedID)
			IF IS_PED_A_PLAYER(VehPedID)
				PlayerID = NETWORK_GET_PLAYER_INDEX_FROM_PED(VehPedID)
				IF (NATIVE_TO_INT(PlayerID) != iThisPlayer)
				AND (NATIVE_TO_INT(PlayerID) > -1)
					IF NOT (GlobalplayerBD[NATIVE_TO_INT(PlayerID)].bPlayerIsHiddenForWarp)
						PRINTLN("IsAnotherVisiblePlayerInVehicle - player ", iThisPlayer, " is in same vehicle as player ", NATIVE_TO_INT(PlayerID), " who is not hidden, returning TRUE")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC



//PURPOSE: Process the visible player checks and see who can see each other
PROC PROCESS_VISIBLE_PLAYER_CHECKS(INT iPlayer)
	
	PED_INDEX pedId
	PLAYER_INDEX PlayerID = INT_TO_NATIVE(PLAYER_INDEX, iPlayer)
	BOOL bIncludeVehicle
	VEHICLE_INDEX CarID
	
	// if this player is supposed to be invisible then don't draw him locally
	IF (GlobalplayerBD[iPlayer].bPlayerIsHiddenForWarp) 
		IF IS_NET_PLAYER_OK(PlayerID, FALSE, FALSE)
			bIncludeVehicle = TRUE
			IF PlayerID <> GET_PLAYER_INDEX()											
				pedId = GET_PLAYER_PED(PlayerId)
				IF IS_PED_IN_ANY_VEHICLE(pedId)
					CarID = GET_VEHICLE_PED_IS_IN(pedId)	
					IF NOT IS_ENTITY_DEAD(CarID)	
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), CarID)
							bIncludeVehicle = FALSE
						ELSE
							IF IsAnotherVisiblePlayerInVehicle(CarID, iPlayer)
								bIncludeVehicle = FALSE	
							ENDIF
						ENDIF						
					ENDIF
				ENDIF
			ENDIF	
			SET_PLAYER_INVISIBLE_LOCALLY(PlayerID, bIncludeVehicle)	
		ENDIF
	ENDIF
	
	IF (iPlayer > -1)
		MPGlobals.g_iTrackedPedPixelHeight[iPlayer] = 0
	ENDIF
	
	////////////////////////////////////////////////////////////////////////////	
	// Find out who I can see and broadcast it /////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	IF PlayerID <> GET_PLAYER_INDEX()	
		IF IS_NET_PLAYER_OK(PlayerID)

			pedId = GET_PLAYER_PED(PlayerId)
//			IF IS_PED_IN_FLYING_VEHICLE(pedId)
				REQUEST_PED_VEHICLE_VISIBILITY_TRACKING(pedId, TRUE) //Added for the overheads so when the player is in a plane these are still seen. 
//			ELSE
//				REQUEST_PED_VEHICLE_VISIBILITY_TRACKING(pedId, FALSE)
//			ENDIF
			REQUEST_PED_USE_SMALL_BBOX_VISIBILITY_TRACKING(pedId,TRUE)
			REQUEST_PED_RESTRICTED_VEHICLE_VISIBILITY_TRACKING(pedID, TRUE)
			
			IF GlobalplayerBD[iPlayer].bRequiresCannotBeTargetedByAI	//Added so player can flag themselves as needing protection from AI
				SET_PED_RESET_FLAG(pedId, PRF_CannotBeTargetedByAI, TRUE)
			ENDIF
			
//			IF MPGlobals.g_bHasRegisteredPedVisibleCheck[iPlayer]
//				MPGlobals.g_iTrackedPedPixelHeight[iPlayer] = GET_TRACKED_PED_PIXELCOUNT(pedId)
//			ENDIF
			
			IF IS_PED_TRACKED(pedId)			
//			IF (MPGlobals.g_iPedIDRegisteredForVisibleCheck[iPlayer] = NATIVE_TO_INT(pedId))
//			AND (MPGlobals.g_bHasRegisteredPedVisibleCheck[iPlayer] = TRUE)		
				
				MPGlobals.g_iTrackedPedPixelHeight[iPlayer] = GET_TRACKED_PED_PIXELCOUNT(pedId)
				
				IF IS_PLAYER_VISIBLE_TO_SCRIPT(PlayerID)
				AND NOT (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bPlayerIsHiddenForWarp)
					SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVisibleToScriptPlayers, iPlayer)
					IF IS_TRACKED_PED_VISIBLE(pedId)							
						SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVisiblePlayers, iPlayer)
					ELSE
						CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVisiblePlayers, iPlayer)
					ENDIF	
				ELSE
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVisiblePlayers, iPlayer)
					CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVisibleToScriptPlayers, iPlayer)
				ENDIF
				
			ELSE
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVisiblePlayers, iPlayer)	
				CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVisibleToScriptPlayers, iPlayer)
//				REGISTER_PLAYER_FOR_VISIBLITY_CHECK(PlayerID)
				REQUEST_PED_VISIBILITY_TRACKING(GET_PLAYER_PED(PlayerID))
			ENDIF

		ELSE
			//MPGlobals.g_bHasRegisteredPedVisibleCheck[iPlayer] = FALSE
			//MPGlobals.g_iPedIDRegisteredForVisibleCheck[iPlayer] = -1
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVisiblePlayers, iPlayer)	
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVisibleToScriptPlayers, iPlayer)
		ENDIF
	ELSE
		// I can see myself.
		SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVisiblePlayers, iPlayer)
		IF NOT IS_ENTITY_DEAD(GET_PLAYER_PED(PlayerID))
		AND IS_PLAYER_VISIBLE_TO_SCRIPT(PlayerID)
			SET_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVisibleToScriptPlayers, iPlayer)
		ELSE
			CLEAR_BIT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iVisibleToScriptPlayers, iPlayer)	
		ENDIF
	ENDIF
	

	
	
ENDPROC

PROC DO_ALL_PROCESS_VISIBLE_PLAYER_CHECKS()

	INT iPlayer
	//IF IS_NET_PLAYER_OK(PLAYER_ID())
		REPEAT NUM_NETWORK_PLAYERS iPlayer
			PROCESS_VISIBLE_PLAYER_CHECKS(iPlayer)	
		ENDREPEAT
	//ENDIF

ENDPROC




