USING "rage_builtins.sch"
USING "globals.sch"

USING "net_missions_at_coords_public.sch"
USING "net_mission_joblist.sch"
USING "net_activity_selector_groups_contact_missions.sch"
USING "net_activity_selector_groups_gang_attacks.sch"
USING "net_activity_selector_groups_heists.sch"

USING "net_heists_common.sch"

USING "net_prints.sch"


// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   net_activity_selector_groups.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Group-Specific functionality used by the Activity Selector system.
//
//		NOTES			:	GROUP MAINTENANCE - KGM 29/3/13
//							How a group is maintained will be determined by group-specific functions so that each group can update
//							in a different way if necessary.
//
//							FREEMODE CONTACT HEIST GROUPS - KGM 29/3/13
//							All Heists will be stored in the same group using a special vector that will be passed in with all Heist
//							missions ensuring they all match and are grouped together. Freemode Heists are each pre-allocated with
//							an NPC contact and one mission from each NPC will be the next chosen Heist for that NPC and shared by
//							all players, although only one player will receive the phonecall from the contact to trigger the mission.
//							Heists will appear at the Safehouse of the player contacted by the NPC, so only players with a safehouse
//							will receive these phonecalls. If one player has multiple heists from different NPCs available at the same
//							time then the corona in the player's safehouse will offer a choice of available heists allowing the player
//							to choose one.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************



// ===========================================================================================================
//      ACTIVITY SELECTOR GROUP WIDGETS
// ===========================================================================================================

// PURPOSE:	Set up any Activity Selector Widgets
#IF IS_DEBUG_BUILD
PROC Create_Activity_Selector_Group_Widgets()
	
	// Widgets
	Create_Contact_Mission_Widgets()
	Create_Heist_Widgets()
ENDPROC
#ENDIF



// ===========================================================================================================
//      Server Initialisation, Reset, and Maintenance Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//     Server Clear and Reset
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Reset any Server variables that should be reset after a host migration
// NOTES:	These are variables that frequently change and don't need the accuracy of being Broadcast Data
//			These should get reset following a host migration
PROC Reset_Activity_Selector_Local_Server_Control_Scheduling_Variables()

	#IF IS_DEBUG_BUILD
		NET_PRINT("      Reset_Activity_Selector_Group_Control_Scheduling_Variables") NET_NL()
	#ENDIF
	
	// Call any local Heist Group Control variables that need reset when the host migrates
	g_structASGroupControlServerMP	emptyGroupControl
	g_sASGroupControlServerMP = emptyGroupControl
	
	// Call any local Heist Control variables that need reset when the host migrates
	g_structASHeistControlServerMP	emptyHeistControl
	g_sASHeistControlServerMP = emptyHeistControl
	
	// Call any local Contact Mission Control variables that need reset when the host migrates
	g_structASCMControlServerMP	emptyContactMissionControl
	g_sASCMControlServerMP = emptyContactMissionControl
	
	
	// Call any local Contact Mission Control variables that need reset when the host migrates
	g_structASGAControlServerMP	emptyGangAttackControl
	g_sASGAControlServerMP = emptyGangAttackControl
	
	g_sASGAControlServerMP.asgacsNextHistoryCheckTimeout	= GET_NETWORK_TIME()

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//     Server Initialisation
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Initialise the Activity Selector Group Server into a default starting state
PROC Initialise_MP_Activity_Selector_Group_Server()

	#IF IS_DEBUG_BUILD
		NET_PRINT("      Initialise_MP_Activity_Selector_Group_Server") NET_NL()
	#ENDIF
	
	INT tempLoop = 0
	
	// Reset any Server Local Control scheduling variables that aren't BD
	Reset_Activity_Selector_Local_Server_Control_Scheduling_Variables()
	
	// Clearout ServerBD Variables
	g_structActivitySelectorServerBD emptyServerBD
	GlobalServerBD_ActivitySelector.activitySelector	= emptyServerBD
	
	// Clearout Server Heist Control variables contained within the ServerBD
	g_structHeistMissionServer emptyHeistServerStruct
	
	REPEAT MAX_FM_HEIST_CONTACTS tempLoop
		GlobalServerBD_ActivitySelector.activitySelector.assbdHeists[tempLoop]					= emptyHeistServerStruct
		GlobalServerBD_ActivitySelector.activitySelector.assbdHeists[tempLoop].hmsPlayer			= INVALID_PLAYER_INDEX()
		GlobalServerBD_ActivitySelector.activitySelector.assbdHeists[tempLoop].hmsContactsDelay	= Get_Random_Time_Offset(MP_HEIST_CONTACT_DELAY_msec)
	ENDREPEAT
	
	// Indicate that checking for a heist setup in another session to be played in this session should still take place
	GlobalServerBD_ActivitySelector.activitySelector.assbdCheckForTransitionHeist				= TRUE
	
	// Clearout Server Contact Mission Control variables contained within the ServerBD
	g_structContactMissionServer emptyContactServerStruct
	
	REPEAT MAX_FM_HEIST_CONTACTS tempLoop
		GlobalServerBD_ActivitySelector.activitySelector.assbdContactMissions[tempLoop]				= emptyContactServerStruct
		GlobalServerBD_ActivitySelector.activitySelector.assbdContactMissions[tempLoop].cmsTimeout	= Get_CM_Randomised_Server_Delay_msec()
		GlobalServerBD_ActivitySelector.activitySelector.assbdContactMissions[tempLoop].cmsPlayer	= INVALID_PLAYER_INDEX()
	ENDREPEAT
	
	// Clearout Server Gang Attack Control variables contained within the ServerBD
	g_structGangAttackServer 			emptyGangAttackStruct
	
	GlobalServerBD_ActivitySelector.activitySelector.assbdGangAttacks = emptyGangAttackStruct
	
	REPEAT MAX_ACTIVE_GANG_ATTACKS tempLoop
		Server_Clear_One_Active_Gang_Attack(tempLoop)
	ENDREPEAT
	
	REPEAT MAX_GANG_ATTACK_RECENT_HISTORY tempLoop
		Server_Clear_One_Gang_Attack_History(tempLoop)
	ENDREPEAT

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//     Server Maintenance - Groups
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain a specific Activity Selector Group Server
//
// INPUT PARAMS:		paramGroupArrayPos				An INT representing a position on the Group Array to be maintained
//
// NOTES:	Groups are now generated on-the-fly based on vector, but some specific groups (ie: Heists) are given specific vectors so they can be identified and dealt with differently
PROC Maintain_This_MP_Activity_Selector_Group_Server(INT paramGroupArrayPos)

	// Nothing to do if the Group is not is use
	IF NOT (Does_This_Pending_Activities_Group_Slot_Contain_Any_Missions(paramGroupArrayPos))
		EXIT
	ENDIF
	
	// Need to call a special update function if this is the Heist group
	IF (Is_This_The_Heist_Group(paramGroupArrayPos))
		// KGM 12/5/14: Heists will no longer need any server control
		EXIT
	ENDIF


//	// ALL QUICK AND TEMP
//	IF (Does_This_Pending_Activities_Group_Slot_Contain_Any_Missions(paramGroupArrayPos))
//		// Yes, so check if a mission has been setup 
//		IF NOT (GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhSetup)
//			// Choose a random Heist to setup
//			BOOL tryAgain = TRUE
//			INT randomHeist = 0
//			WHILE (tryAgain)
//				tryAgain = FALSE
//				randomHeist = GET_RANDOM_INT_IN_RANGE(0, MAX_PENDING_ACTIVITIES)
//				IF (GlobalServerBD_MissionsShared.activities[randomHeist].paInUse)
//					GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhPendingActivity	= randomHeist
//					GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhPlayer				= Choose_Player_To_Setup_FM_Heist()
//					GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhSetup				= TRUE
//					GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhSharedRegID		= ILLEGAL_SHARED_REG_ID
//					GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhStarted			= FALSE
//					
//					#IF IS_DEBUG_BUILD
//						NET_PRINT("...KGM MP [ActSelect]: Server chose this Heist mission: ")
//						NET_PRINT_INT(randomHeist)
//						NET_PRINT(" [")
//						MP_MISSION_ID_DATA	missionID_Data	= Get_Pending_Activities_MissionID_Data_For_Slot(randomHeist)
//						TEXT_LABEL_31		missionName		= Get_Mission_Name_For_FM_Cloud_Loaded_Activity(missionID_Data)
//						NET_PRINT(missionName)
//						NET_PRINT("] - Given to player: ")
//						NET_PRINT(GET_PLAYER_NAME(GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhPlayer))
//						NET_NL()
//					#ENDIF
//				ELSE
//					tryAgain = TRUE
//				ENDIF
//			ENDWHILE
//		ELSE
//			// Has this Heist now become shared?
//			IF (GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhSharedRegID = ILLEGAL_SHARED_REG_ID)
//				// ...not known to be shared, so check if the player given the heist now has a sharedRegID for this mission
//				PLAYER_INDEX controllingPlayer = GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhPlayer
//				IF (GlobalplayerBD[NATIVE_TO_INT(controllingPlayer)].playerActivitySelector.tempHeistClient.thmcSharedRegID	!= ILLEGAL_SHARED_REG_ID)
//					// ...mission now shared, so get the SharedRegID so that it can be monitored by the server
//					GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhSharedRegID = GlobalplayerBD[NATIVE_TO_INT(controllingPlayer)].playerActivitySelector.tempHeistClient.thmcSharedRegID
//					
//					#IF IS_DEBUG_BUILD
//						NET_PRINT("...KGM MP [ActSelect]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Heist is now a Shared Mission with SharedRegID: ")
//						NET_PRINT_INT(GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhSharedRegID)
//						NET_NL()
//					#ENDIF
//				ENDIF
//			ELSE
//				// ...mission was shared, so check if the mission is now being played by using the Shared Mission data to check the Mission Controller status
//				INT uniqueID = Get_UniqueID_For_Shared_Cloud_Loaded_Mission(GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhSharedRegID)
//				
//				IF (uniqueID = NO_UNIQUE_ID)
//					// ...the mission is no longer shared, so allow it to be setup again if it wasn't started, otherwise clearout all the variables to allow a new one to be setup
//					IF (GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhStarted)
//						// ...mission was started, so allow a new Heist to be setup
//						GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhPendingActivity	= ILLEGAL_PENDING_ACTIVITIES_SLOT
//						GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhPlayer				= INVALID_PLAYER_INDEX()
//						GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhSetup				= FALSE
//						GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhSharedRegID		= ILLEGAL_SHARED_REG_ID
//						GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhStarted			= FALSE
//					
//						#IF IS_DEBUG_BUILD
//							NET_PRINT("...KGM MP [ActSelect]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Heist is now finished so clearing out all server data to allow another to be chosen")
//							NET_NL()
//						#ENDIF
//					ELSE
//						// ...mission wasn't started, so wait for this one to become shared again
//						GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhSharedRegID		= ILLEGAL_SHARED_REG_ID
//						GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhStarted			= FALSE
//					
//						#IF IS_DEBUG_BUILD
//							NET_PRINT("...KGM MP [ActSelect]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Mission has been cancelled without being triggered - so allow it to become shared again")
//							NET_NL()
//						#ENDIF
//					ENDIF
//				ENDIF
//				
//				IF (Has_This_Mission_Request_Mission_Started(uniqueID))
//					GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhStarted = TRUE
//					
//					#IF IS_DEBUG_BUILD
//						NET_PRINT("...KGM MP [ActSelect]: ") NET_PRINT_TIME() NET_PRINT(" (SERVER) Heist is now Started")
//						NET_NL()
//					#ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Maintain the Activity Selector Groups Server
PROC Maintain_MP_Activity_Selector_Group_Server()

	// Maintain Gang Attacks every frame
	Maintain_MP_Activity_Selector_Gang_Attack_Group_Server()
	
	// Maintain next scheduled group
	IF (g_sASGroupControlServerMP.asgcsNextScheduledGroupArrayPos >= MAX_PENDING_ACTIVITIES_GROUPS)
		g_sASGroupControlServerMP.asgcsNextScheduledGroupArrayPos = 0
	ENDIF
	
	// Grab the group before the global is updated
	INT	scheduledUpdateGroup = g_sASGroupControlServerMP.asgcsNextScheduledGroupArrayPos
	
	// Always, update the group counter for next maintenance check
	g_sASGroupControlServerMP.asgcsNextScheduledGroupArrayPos++
	
	// Maintain a specific group
	Maintain_This_MP_Activity_Selector_Group_Server(scheduledUpdateGroup)

ENDPROC




// ===========================================================================================================
//      Client Initialisation and Maintenance Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//     Client Initialisation
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Initialise the Activity Selector Group Client into a default starting state
PROC Initialise_MP_Activity_Selector_Group_Client()

	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [Heist]: Initialise_MP_Activity_Selector_Group_Client")
	#ENDIF
	
	INT tempLoop = 0
	
	// Initialise any local server control variables when the client is initialised so that they contain decent startup values should the host migrate to this client
	Reset_Activity_Selector_Local_Server_Control_Scheduling_Variables()
	
	// Initialise any local client control variables
	// ...groups
	g_structASGroupControlClientMP emptyLocalPlayerGroupControl
	g_sASGroupControlClientMP = emptyLocalPlayerGroupControl
	
	// ...heists (a subgroup within the groups)
	// ......KGM 11/5/14 - old server heist triggering - potentially obsolete
	g_structASHeistControlClientMP emptyLocalPlayerHeistControl
	g_sASHeistControlClientMP = emptyLocalPlayerHeistControl
	
	// ......KGM 11/5/14 - new local data heist triggering
	Clear_Local_Heist_Strands()
	Clear_Other_Active_Heist_Coronas()
	Initialise_Local_Heist_Unlocks()
	
	IF (g_resetHeistsOnJoiningMP)
		// Only reset these on joining MP under specific circumstances
		// NOTE: Don't reset the 'phone for heist' replay timeouts - let those persist.
		PRINTLN(".KGM [Heist]: Reset semi-persistent Heist variables on MP rejoin because g_resetHeistsOnJoiningMP = TRUE")
		Clear_Local_Heist_Control()
		Clear_Heist_Data_Stored_For_Cross_Session_Transmission()
		Clear_New_Heist_Strand_Cooldown_Period()
		Clear_Heist_Reward_Active_Invite()
// KGM 16/11/14: Don't clear the flag here - it'll now get cleared within the 'NO HEISTS' processing stage after some work gets done
//		// All reset
//		g_resetHeistsOnJoiningMP = FALSE
		
		// Perform the check for opening up the replay board based on the player having all awards for all Finales
		// NOTE: Call function 'not as leader' - we're checking if a player has completed all Finales now as Leader OR crew and here we just need to run a general check
		PRINTLN(".KGM [Heist]: Initialise_MP_Activity_Selector_Group_Client(): Reset Heists On Joining MP - Checking if the Replay Board should be opened based on Finale awards achieved.")
		HANDLE_UNLOCKING_REPLAY_BOARD_AS_MEMBER(FALSE)
	ELSE
		PRINTLN(".KGM [Heist]: DON'T reset semi-persistent Heist variables on MP rejoin because g_resetHeistsOnJoiningMP = FALSE")
	
		// KGM 23/2/15 [BUG 2236383]: If the heist controller state is 'setup heist cutscene corona' then put it back to 'player input' - the player must have changed session just after pressing DPADRIGHT (suspected XMB invite)
		IF (g_sLocalMPHeistControl.lhcStage = HEIST_CLIENT_STAGE_SETUP_HEIST_CUTSCENE_CORONA)
			PRINTLN(".KGM [Heist]: ...BUT: Heist Controller stage is HEIST_CLIENT_STAGE_SETUP_HEIST_CUTSCENE_CORONA, switching to HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT to prevent invisibility, etc")
			g_sLocalMPHeistControl.lhcStage = HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT
		ENDIF
	
		// KGM 23/2/15 [BUG 2301751]: If the heist controller state is 'wait for heist strand active' then put it back to 'player input' - a problem must have occurred as the player as Leader started to watch the initial cutscene
		// NOTE: If the player is in an activity session, then being in the 'wait for active' stage is valid as the cutscene is being launched, so only do this check if not in an activity session
		IF (g_sLocalMPHeistControl.lhcStage = HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_ACTIVE)
			IF NOT (NETWORK_IS_ACTIVITY_SESSION())
				PRINTLN(".KGM [Heist]: ...BUT: Heist Controller stage is HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_ACTIVE when not in Activity Session, switching to HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT to ensure heist can still start at planning board")
				g_sLocalMPHeistControl.lhcStage = HEIST_CLIENT_STAGE_WAIT_FOR_PLAYER_START_INPUT
			ELSE
				PRINTLN(".KGM [Heist]: NOTE: Heist Controller stage is HEIST_CLIENT_STAGE_WAIT_FOR_HEIST_STRAND_ACTIVE, but player is in an Activity Session so leave stage as-is")
			ENDIF
		ENDIF
	ENDIF
	
	// KGM 18/2/15 [BUG 2239042]: Store whether the Replay Board has been unlocked - an optimisation to save calling this function every frame by Conor
	Update_Optimisation_Global_Heist_Replay_Board_Unlocked()
		// FEATURE_HEIST_PLANNING
	
	// ...contact missions (a subgroup within the groups)
	Clear_Local_Contact_Missions()
	
	// ...number of contact missions played per contact
	g_structCMsPlayedPerContact emptyCMsPlayedPerContact
	g_cmsPlayedPerContact = emptyCMsPlayedPerContact
	
	REPEAT MAX_FM_HEIST_CONTACTS tempLoop
		g_cmsPlayedPerContact.cmppcPlayed[tempLoop] = 0
	ENDREPEAT
	
	// ...the 'listen for contact missions played' control variables
	g_structClientPlayingCM emptyPlayingCM
	g_cpcmPlaying = emptyPlayingCM
	
	// ...current gang attack (a subgroup within the groups)
	Client_Clear_Current_Gang_Attack()
	
	// ...Gang Attack history
	REPEAT MAX_GANG_ATTACK_RECENT_HISTORY tempLoop
		g_sHistoryClientGA.gahcCloudFilename[tempLoop] = ""
	ENDREPEAT
	g_sHistoryClientGA.gahcNextScheduledHistorySlot = 0
	
	
	// Clearout PlayerBD Variables
	g_structActivitySelectorPlayerBD emptyPlayerBD
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector	= emptyPlayerBD
	
	// Clearout Player Heist Control variables contained within the PlayerBD
	// ...heist group
	g_structHeistGroupClient emptyHeistGroupClient
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbHeistGroup = emptyHeistGroupClient
	
	// ...individual heist missions per contact
//	g_structHeistMissionClient emptyHeistClientStruct
	
//	REPEAT MAX_FM_HEIST_CONTACTS tempLoop
//		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbdHeists[tempLoop]	= emptyHeistClientStruct
//	ENDREPEAT
	
	// ...general heist variables
	g_structHeistGeneralClient emptyGeneralHeistStruct
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbHeistGeneral = emptyGeneralHeistStruct
	
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbHeistGeneral.hgcSafehouseID		= GET_OWNED_PROPERTY(0)
	//CDM 2014-1-15 just a heads up the player will now be able to own 2 properties so this functionality may need to be updated.
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbHeistGeneral.hgcNewPropertyDelay	= GET_NETWORK_TIME()
	
	// ...after-transition variables: KGM HEIST TRANSITION: Set these up based on the stored transition-safe globals
	g_structASFMHeistTransitionMP emptyHeistTransitionStruct
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbAfterTransition = emptyHeistTransitionStruct

	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbAfterTransition.asfmhtContactID		= g_sLocalTransitionSafeHeistData.asfmhtContactID
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbAfterTransition.asfmhtCloudFilename	= g_sLocalTransitionSafeHeistData.asfmhtCloudFilename
	
	// ...contact mission variables
	Clear_MP_MISSION_ID_DATA_Struct(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcMissionIdData)
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcTimeout				= Get_CM_Randomised_Initial_Delay_msec()

	REPEAT MAX_CONTACT_MISSION_INVITES_AT_ONCE tempLoop
		GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.aspbContactMission.cmcActiveContentIdHashes[tempLoop] = 0
	ENDREPEAT

	
	// Contact Mission Stats
	Clear_Contact_Mission_Rank_Stats_Local_Storage()
	Get_Contact_Mission_Rank_Stats_Into_Local_Storage()
	Set_Next_Rank_Update_Quick_Retest_Timeout()

	// Debug Output
	#IF IS_DEBUG_BUILD
		REPEAT MAX_OWNED_PROPERTIES tempLoop
			PRINTLN(".KGM [Heist]: Player's Apartment ",tempLoop,": ", GET_OWNED_PROPERTY(tempLoop ))
		ENDREPEAT
		IF (Does_Player_Own_An_Apartment_Suitable_For_Heists())
			PRINTSTRING(".KGM [Heist]: PLAYER OWNS AN APARTMENT SUITABLE FOR HEISTS")
		ELSE
			PRINTSTRING(".KGM [Heist]: Player does NOT own an apartment suitabel for Heists")
		ENDIF
		PRINTNL()
		
		// Clear out the 'use numpad7' debug variable - but leave the actual variable alone
		g_DEBUG_UseNumpad7Heist = FALSE
		// FEATURE_HEIST_PLANNING
	#ENDIF	// IS_DEBUG_BUILD
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("      - Tunable: Contact Mission Initial Delay (seconds): ") NET_PRINT_INT(g_sMPTunables.ContactMissionInitialDelay_secs) NET_NL()
		NET_PRINT("      - Tunable: Contact Mission Regular Delay (seconds): ") NET_PRINT_INT(g_sMPTunables.ContactMissionRegularDelay_secs) NET_NL()
		NET_PRINT("      - Tunable: Contact Mission Server Delay (seconds) : ") NET_PRINT_INT(g_sMPTunables.ContactMissionServerDelay_secs) NET_NL()
	#ENDIF
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//     Client Maintenance - Groups
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Maintain a specific Activity Selector Groups Client
//
// INPUT PARAMS:		paramGroupArrayPos				An INT represent a position on the Group Array to be maintained
//
// NOTES:	Groups are now generated on-the-fly based on vector, but some specific groups (ie: Heists) are given specific vectors so they can be identified and dealt with differently
PROC Maintain_This_MP_Activity_Selector_Group_Client(INT paramGroupArrayPos)

	// First of all, check for any Local Groups - these have schedulerIDs greater than the number of server-based groups
	IF (paramGroupArrayPos >= MAX_PENDING_ACTIVITIES_GROUPS)
		// ...this is a local group
		// Is this the Contact Mission group?
		IF (paramGroupArrayPos = LOCAL_GROUP_CONTACT_MISSIONS_UPDATE_SCHEDULER)
			Maintain_MP_Activity_Selector_Contact_Mission_Group_Client()
			EXIT
		ENDIF
		
		EXIT
	ENDIF

	// Nothing to do if this is a Server-Based Group that is not is use
	IF NOT (Does_This_Pending_Activities_Group_Slot_Contain_Any_Missions(paramGroupArrayPos))
		EXIT
	ENDIF
	
	// Call a special update function if this is the Heist group
	IF (Is_This_The_Heist_Group(paramGroupArrayPos))
// KGM 12/5/14: I'm not calling this from here anymore, the maintenance is simplified
//		#IF IS_DEBUG_BUILD
//		Maintain_MP_Activity_Selector_Heist_Group_Client()
//		#ENDIF

		EXIT
	ENDIF

//	// ALL QUICK AND TEMP
//	IF (GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhSetup)
//		IF NOT (GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhStarted)
//			IF (GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhPlayer = PLAYER_ID())
//				// Tell Player?
//				IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcHeistTellPlayer)
//					#IF IS_DEBUG_BUILD
//						NET_PRINT("...KGM MP [ActSelect][Heist]: Attempting to tell about Heist") NET_NL()
//					#ENDIF
//			
//					INT					theSlot			= GlobalServerBD_ActivitySelector.activitySelector.tempHeist.tmhPendingActivity
//					enumCharacterList	theCharacter	= Get_Pending_Activities_ContactID_For_Slot(theSlot)
//					
//					IF Request_MP_Comms_Txtmsg(theCharacter, "HEIST_AVAILABLE", DEFAULT, DEFAULT)
//						// ...communication made
//						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcHeistTellPlayer	= FALSE
//						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcHeistDoSetup		= TRUE
//						
//						#IF IS_DEBUG_BUILD
//							NET_PRINT("      SUCCESS: Player has been told about Heist") NET_NL()
//						#ENDIF
//					ENDIF
//					
//					EXIT
//				ENDIF
//				
//				IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcHeistDoSetup)
//				AND NOT (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcHeistSetup)
//					// Setup the heist
//					Quickie_Setup_Mission()
//				ELSE
//					// Heist setup, so check if in the corona of the Heist
//					IF NOT (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcInCorona)
//						// Now in corona?
//						IF (Is_This_Missions_At_Coords_RegID_The_Focus_Mission(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcMatcRegID))
//							// ...yes, now in corona
//							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcInCorona = TRUE
//							
//							#IF IS_DEBUG_BUILD
//								NET_PRINT("...KGM MP [ActSelect]: ") NET_PRINT_TIME() NET_PRINT(" Heist Controller is in the corona") NET_NL()
//							#ENDIF
//						ELSE
//							// ...not in corona, so clear the variables if player was in the corona
//							IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcSharedRegID != ILLEGAL_SHARED_REG_ID)
//								GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcInCorona		= FALSE
//								GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcSharedRegID	= ILLEGAL_SHARED_REG_ID
//								
//								#IF IS_DEBUG_BUILD
//									NET_PRINT("...KGM MP [ActSelect]: ") NET_PRINT_TIME() NET_PRINT(" Heist Controller is no longer in the corona") NET_NL()
//								#ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						// In Corona, so check if there is a SharedRegID
//						IF (GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcSharedRegID = ILLEGAL_SHARED_REG_ID)
//							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcSharedRegID = Get_Missions_At_Coords_SharedRegID_For_Focus_Mission()
//							
//							#IF IS_DEBUG_BUILD
//								NET_PRINT("...KGM MP [ActSelect]: ")
//								NET_PRINT_TIME()
//								NET_PRINT(" Heist Controller has found a SharedRegID for the Heist mission: ")
//								NET_PRINT_INT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcSharedRegID)
//								NET_NL()
//							#ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ELSE
//			// If setup and started, clear out the client data - this is a bit nasty and needs a better method because this will trigger all the time the mission is ongoing
//			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcHeistTellPlayer	= FALSE
//			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcHeistDoSetup		= FALSE
//			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].playerActivitySelector.tempHeistClient.thmcHeistSetup		= FALSE
//		ENDIF
//	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To Maintain the Activity Selector Groups Client
PROC Maintain_MP_Activity_Selector_Group_Client()
	
	// Maintain Gang Attacks every frame
	Maintain_MP_Activity_Selector_Gang_Attack_Group_Client()
	
	// Maintain Heists every frame
	Maintain_MP_Activity_Selector_Heist_Group_Client()
		// FEATURE_HEIST_PLANNING
	
	// Maintain next scheduled group - after all the dynamic server-based groups update, also update any local groups
	IF (g_sASGroupControlClientMP.asgccNextScheduledGroupArrayPos >= MAX_LOCAL_GROUP_UPDATE_SCHEDULER)
		g_sASGroupControlClientMP.asgccNextScheduledGroupArrayPos = 0
	ENDIF
	
	// Grab the group before the global is updated
	INT	scheduledUpdateGroup = g_sASGroupControlClientMP.asgccNextScheduledGroupArrayPos
	
	// Always, update the group counter for next maintenance check
	g_sASGroupControlClientMP.asgccNextScheduledGroupArrayPos++
	
	// Maintain a specific group
	Maintain_This_MP_Activity_Selector_Group_Client(scheduledUpdateGroup)

ENDPROC



//// ===========================================================================================================
////      EVENT BROADCAST AND PROCESS FUNCTIONS
//// ===========================================================================================================
//
//// -----------------------------------------------------------------------------------------------------------
////      Event: Setup Player Choice Group Mission
//// -----------------------------------------------------------------------------------------------------------
//
//STRUCT m_structEventSetupPlayerChoiceGroupMission
//	STRUCT_EVENT_COMMON_DETAILS	Details					// Common Event details
//	g_eMissionsSharedGroupID	GroupID					// The GroupID for this mission (if required)
//	INT							MissionsSharedSlot		// The Missions Shared Slot containing the mission Identification data
//	VECTOR						MissionCoords			// The coords where the mission will be setup
//ENDSTRUCT
//
//
//// -----------------------------------------------------------------------------------------------------------
//
//// PURPOSE:	Broadcast details of a mission chosen by the player to be setup by all players
////
//// INPUT PARAMS:			paramGroupID				Missions Shared GroupID
////							paramMissionsSharedSlot		The Missions Shared Slot containing the mission Identification dat
//PROC Broadcast_Setup_Player_Choice_Group_Mission(g_eMissionsSharedGroupID paramGroupID, INT paramMissionsSharedSlot)
//	
//	// Store the Broadcast data
//	m_structEventSetupPlayerChoiceGroupMission Event
//	
//	Event.Details.Type				= SCRIPT_EVENT_SETUP_PLAYER_CHOICE_GROUP_MISSION
//	Event.Details.FromPlayerIndex	= PLAYER_ID()
//	
//	Event.GroupID					= paramGroupID
//	Event.MissionsSharedSlot		= paramMissionsSharedSlot
//	Event.MissionCoords				= Get_Contact_Point_Coords_In_Players_Savehouse()
//	
//	// Broadcast the event - to ALL PLAYERS
//	SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), ALL_PLAYERS())
//
//	#IF IS_DEBUG_BUILD
//		NET_PRINT("...KGM MP [MShared]: Broadcast_Setup_Player_Choice_Group_Mission for Group: ")
//		NET_PRINT(Convert_Missions_Shared_GroupID_To_String(paramGroupID))
//		NET_NL()
//		NET_PRINT("      ")
//		Debug_Output_Pending_Activities_Slot_Details_On_One_Line(paramMissionsSharedSlot)
//		NET_NL()
//	#ENDIF
//
//ENDPROC
//
//
//// -----------------------------------------------------------------------------------------------------------
//
//// PURPOSE:	Process a request to setup a Group Mission chosen by another player
////
//// INPUT PARAMS:		paramEventID			The Event array position for this event's data
PROC Process_Event_Setup_Player_Choice_Group_Mission(INT paramEventID)
	paramEventID = paramEventID
//	
//	#IF IS_DEBUG_BUILD
//		NET_NL() NET_PRINT("...KGM MP [ActSelect]: ") NET_PRINT_TIME() NET_PRINT(" Process_Event_Setup_Player_Choice_Group_Mission")
//	#ENDIF
//	
//	// Retrieve the data associated with the event
//	m_structEventSetupPlayerChoiceGroupMission theEventData
//	IF NOT (GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, paramEventID, theEventData, SIZE_OF(theEventData)))
//		#IF IS_DEBUG_BUILD
//			NET_NL()
//			SCRIPT_ASSERT("Process_Event_Setup_Player_Choice_Group_Mission(): FAILED TO RETRIEVE EVENT DATA. Tell Keith.")
//		#ENDIF
//		
//		EXIT
//	ENDIF
//	
//	#IF IS_DEBUG_BUILD
//		NET_PRINT(" (from: ") NET_PRINT(GET_PLAYER_NAME(theEventData.Details.FromPlayerIndex)) NET_PRINT(")") NET_NL()
//	#ENDIF
//	
//	// Output details
//	#IF IS_DEBUG_BUILD
//		NET_PRINT("      ")
//		Debug_Output_Pending_Activities_Slot_Details_On_One_Line(theEventData.missionsSharedSlot)
//		NET_NL()
//	#ENDIF
//	
//	// Only dealing with the Heist Group missions
//	IF (theEventData.GroupID != MSGID_FM_HEIST)
//		#IF IS_DEBUG_BUILD
//			NET_PRINT("      IGNORING: Only HEIST missions are being handled at the moment") NET_NL()
//		#ENDIF
//		
//		EXIT
//	ENDIF
//	
//	// Is there already a Heist mission setup for the player choosing the mission?
//	INT sendingPlayerAsInt	= NATIVE_TO_INT(theEventData.Details.FromPlayerIndex)
//	INT	atCoordsRegID		= Get_AS_Heist_Group_Missions_At_Coords_RegID(sendingPlayerAsInt)
//	
//	IF (atCoordsRegID = ILLEGAL_AT_COORDS_ID)
//		// There isn't a heist already setup from the sending player, so set one up
//		Setup_Heist_From_Sending_Player(theEventData.GroupID, theEventData.missionsSharedSlot, sendingPlayerAsInt, theEventData.MissionCoords)
//		
//		EXIT
//	ENDIF
//	
//	// There is already a Heist set up from the sending player, so nothing to do - the heist owner will update the Mission Controller and the MissionsAtCoords system will then retrieve the new data
//
ENDPROC










