//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	net_office.sch												//
//		AUTHOR			:	Online Technical Team: A.Tabrizi, Tymon						//
//		DESCRIPTION		:	Header file that contains all public functions for office	//
//							related stuff.												//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "mp_globals_ambience_tu.sch"
USING "net_include.sch"
USING "mp_globals_fm.sch"
USING "net_realty_new.sch"
USING "net_realty_mess_include.sch"

USING "net_office_private.sch"
USING "net_simple_interior.sch"

#IF IS_DEBUG_BUILD
BOOL bBlankOfficeName
#ENDIF

/// PURPOSE:
///    Initialise heli docking
/// PARAMS:
///    iBuildingID - 
///    dockData - 
PROC INITIALISE_HELI_DOCK_DATA_FOR_BUILDING(INT iBuildingID, PROPERTY_HELI_DOCK_DATA &dockData, BOOL bFromInterior = FALSE)
	UNUSED_PARAMETER(bFromInterior)
	
	IF NOT DOES_BUILDING_HAVE_A_HELIPAD(iBuildingID)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] INITIALISE_HELI_DOCK_DATA_FOR_BUILDING - Building (", iBuildingID ,") doesn't have a helipad.")
		#ENDIF
	
		SET_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_INACTIVE)
		EXIT
	ENDIF
	
	SET_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_IDLE)
	
	dockData.iBS = 0
	dockData.iInvolvedPlayersCount = 0
	dockData.iBuildingID = iBuildingID
	dockData.vDockPosition = GET_BUILDING_HELIPAD_POSITION(iBuildingID)
	
	IF NOT SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
		g_OfficeHeliDockData.bUsedSVMHeliStart = FALSE
		PRINTLN("[HELI_DOCK] INITIALISE_HELI_DOCK_DATA_FOR_BUILDING - Resetting g_OfficeHeliDockData.bUsedSVMHeliStart")
	ENDIF
	
	g_OfficeHeliDockData.bFirstSVMHeliCutsceneDone = FALSE
	PRINTLN("[HELI_DOCK] INITIALISE_HELI_DOCK_DATA_FOR_BUILDING - Resetting g_OfficeHeliDockData.bFirstSVMHeliCutsceneDone")
	
	
	
	
	/*
	IF bFromInterior
		CLEAR_BIT(globalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_CUTSCENE_DONE)
		CLEAR_BIT(globalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_CUTSCENE_DONE)
	ENDIF
	*/
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] INITIALISE_HELI_DOCK_DATA_FOR_BUILDING - Initialised for building (", iBuildingID ,").")
	#ENDIF
ENDPROC

/// PURPOSE:
///    Initialise heli docking
/// PARAMS:
///    eSimpleInteriorID - 
///    dockData - 
PROC INITIALISE_HELI_DOCK_DATA_FOR_SIMPLE_INTERIOR(SIMPLE_INTERIORS eSimpleInteriorID, PROPERTY_HELI_DOCK_DATA &dockData)
	
	IF NOT DOES_THIS_SIMPLE_INTERIOR_ALLOW_ENTRY_VIA_HELIPAD(eSimpleInteriorID)
	AND NOT DOES_THIS_SIMPLE_INTERIOR_ALLOW_EXIT_VIA_HELIPAD(eSimpleInteriorID)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] INITIALISE_HELI_DOCK_DATA_FOR_SIMPLE_INTERIOR - Simple interior (", eSimpleInteriorID ,") doesn't have a helipad.")
		#ENDIF
	
		SET_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_INACTIVE)
		EXIT
	ENDIF
	
	SET_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_IDLE)
	
	dockData.iBS = 0
	dockData.iInvolvedPlayersCount = 0
	dockData.iBuildingID = ENUM_TO_INT(eSimpleInteriorID)
	dockData.vDockPosition = GET_SIMPLE_INTERIOR_HELIPAD_POSITION(eSimpleInteriorID)
	
	IF NOT SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
		g_OfficeHeliDockData.bUsedSVMHeliStart = FALSE
		PRINTLN("[HELI_DOCK] INITIALISE_HELI_DOCK_DATA_FOR_SIMPLE_INTERIOR - Resetting g_OfficeHeliDockData.bUsedSVMHeliStart")
	ENDIF
	
	g_OfficeHeliDockData.bFirstSVMHeliCutsceneDone = FALSE
	PRINTLN("[HELI_DOCK] INITIALISE_HELI_DOCK_DATA_FOR_SIMPLE_INTERIOR - Resetting g_OfficeHeliDockData.bFirstSVMHeliCutsceneDone")
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] INITIALISE_HELI_DOCK_DATA_FOR_SIMPLE_INTERIOR - Initialised for Simple interior (", eSimpleInteriorID ,"). dockData.vDockPosition = ", dockData.vDockPosition)
	#ENDIF
ENDPROC

FUNC VECTOR _PRIVATE_GET_BUILDING_HELIPAD_POSITION(INT iStagger, INT iBuildingID)
	
	IF IS_HELI_DOCK_A_SIMPLE_INTERIOR_DOCK(iStagger)
		INT iArrayIndex = (iStagger - CI_MAX_NUM_OFFICES)
		RETURN GET_SIMPLE_INTERIOR_HELIPAD_POSITION(g_SimpleInteriorData.eInteriorsWithHelipad[iArrayIndex])
	ENDIF
	
	RETURN GET_BUILDING_HELIPAD_POSITION(iBuildingID)
ENDFUNC

PROC CLEANUP_HELI_DOCK(PROPERTY_HELI_DOCK_DATA &dockData)
	IF NOT IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_INACTIVE)
	AND NOT IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_INITIALISE)
	
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] CLEANUP_HELI_DOCK - Cleanup for building (", dockData.iBuildingID ,").")
		#ENDIF
		
		IF IS_LOCAL_PLAYER_DOING_HELI_DOCK_CUTSCENE()
		//AND IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY)
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] CLEANUP_HELI_DOCK - Player was doing a cutscene, script terminates because (probably) they got invited so no longer doing the cutscene.")
			#ENDIF
			CLEANUP_HELI_DOCK_CUTSCENE(dockData)
			SET_LOCAL_PLAYER_IS_DOING_HELI_DOCK_CUTSCENE(FALSE)
		ENDIF
	
		IF IS_BIT_SET(dockData.iBS, BS_HELI_DOCK_DOORS_LOCKED_FOR_LANDING)
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] CLEANUP_HELI_DOCK - BS_HELI_DOCK_DOORS_LOCKED_FOR_LANDING is set so will try to unlock veh.")
			#ENDIF
		
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND DOES_ENTITY_EXIST(dockData.vehPlayerHeli) AND NOT IS_ENTITY_DEAD(dockData.vehPlayerHeli)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), dockData.vehPlayerHeli)
					
					IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(dockData.vehPlayerHeli)
						NETWORK_REQUEST_CONTROL_OF_ENTITY(dockData.vehPlayerHeli)
					ENDIF
				
					UNLOCK_HELI_DOCK_VEHICLE(dockData)
				ENDIF
			ENDIF
		ENDIF

		RESET_HELI_DOCK_DATA(dockData)
		STOP_HELI_DOCK_AUDIO_SCENES()
	ELSE
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] CLEANUP_HELI_DOCK - Cleanup called but heli dock was never initialised - probably calling this for something other than office.")
		#ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_HELI_DOCK_FINISHED(PROPERTY_HELI_DOCK_DATA &dockData)
	BOOL bHasFinished = IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_WAIT_FOR_ENTRANCE)
	RETURN bHasFinished
ENDFUNC

FUNC INT GET_HELI_DOCK_PROPERTY_TO_ENTER(PROPERTY_HELI_DOCK_DATA &dockData)
	UNUSED_PARAMETER(dockData)
	RETURN -1
ENDFUNC

PROC GET_OWNER_AND_PROPERTY_TO_ENTER_AFTER_HELI_DOCK_LANDING(PROPERTY_HELI_DOCK_DATA &dockData, PLAYER_INDEX &playerOwnerOut, INT &iPropertyOut)
	
	PLAYER_INDEX playerOwner = dockData.pOwnerPlayerIndex
	
	IF IS_HELI_DOCK_OWNER_OK(dockData)
		playerOwnerOut = playerOwner
		iPropertyOut = GlobalplayerBD_FM[NATIVE_TO_INT(playerOwner)].propertyDetails.iOwnedProperty[PROPERTY_OWNED_SLOT_OFFICE_0]
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] GET_OWNER_AND_PROPERTY_TO_ENTER_AFTER_HELI_DOCK_LANDING - Heli owner is invalid, they must have quit during landing.")
	#ENDIF
	
	playerOwnerOut = INVALID_PLAYER_INDEX()
	
ENDPROC

/// PURPOSE:
///    Get all players involved in heli dock as an int flag for use with BROADCAST_PROPERTY_NEARBY_PLAYERS_INVITE_EVENT
/// PARAMS:
///    dockData - 
/// RETURNS:
///    
FUNC INT PLAYERS_INVOLVED_IN_HELI_DOCK(PROPERTY_HELI_DOCK_DATA &dockData, BOOL bIncludeOwner = FALSE)
	INT iPlayersBS = 0
	INT iPlayer
	PLAYER_INDEX playerOwnerID = dockData.pOwnerPlayerIndex
	
	REPEAT dockData.iInvolvedPlayersCount iPlayer
		PLAYER_INDEX playerID = dockData.pInvolvedPlayers[iPlayer]
		IF IS_NET_PLAYER_OK(playerID)
			IF playerID != playerOwnerID OR bIncludeOwner
				SET_BIT(iPlayersBS, NATIVE_TO_INT(dockData.pInvolvedPlayers[iPlayer]))
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] PLAYERS_INVOLVED_IN_HELI_DOCK - Including player ", GET_PLAYER_NAME(dockData.pInvolvedPlayers[iPlayer]), " (", NATIVE_TO_INT(dockData.pInvolvedPlayers[iPlayer]), ")")
				#ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] PLAYERS_INVOLVED_IN_HELI_DOCK - Got a bitset: ", iPlayersBS)
	#ENDIF
	
	RETURN iPlayersBS
ENDFUNC

PROC RUN_BUILDING_HELI_DOCK_CONTROL(PROPERTY_HELI_DOCK_DATA &dockData)
	
	IF IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_INACTIVE)
		EXIT
	ENDIF
	
	// STATE: INITIALISE
	IF IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_INITIALISE)
		RUN_HELI_DOCK_STATE_INITIALISE(dockData)
	
	// STATE: IDLE
	ELIF IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_IDLE)
		RUN_HELI_DOCK_STATE_IDLE(dockData)
	
	// STATE: STARTING LANDING CUTSCENE
	ELIF IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_START_LANDING_CUTSCENE)
		RUN_HELI_DOCK_STATE_START_LANDING_CUTSCENE(dockData)
	
	// STATE: LOADING CUTSCENE
	ELIF IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_LOADING_LANDING_CUTSCENE)
		RUN_HELI_DOCK_STATE_LOADING_LANDING_CUTSCENE(dockData)
	
	// STATE: PLAYING CUTSCENE
	ELIF IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_PLAYING_LANDING_CUTSCENE)
		RUN_HELI_DOCK_STATE_PLAYING_LANDING_CUTSCENE(dockData)
		
	// STATE: CANCELING CUTSCENE
	ELIF IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_CANCELING_LANDING)
		RUN_HELI_DOCK_STATE_CANCELING_LANDING(dockData) 
		
	// STATE: WAITING FOR ENTRY
	ELIF IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_WAIT_FOR_ENTRANCE)
		RUN_HELI_DOCK_STATE_WAIT_FOR_ENTRANCE(dockData)
		
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD
PROC CREATE_OFFICE_DEBUG_WIDGETS()
	START_WIDGET_GROUP("Office Signage")
		ADD_WIDGET_BOOL("Blank Office Sign", bBlankOfficeName)
	STOP_WIDGET_GROUP()
ENDPROC

PROC CREATE_HELI_DOCK_DEBUG_WIDGETS(PROPERTY_HELI_DOCK_DATA &dockData)
	UNUSED_PARAMETER(dockData)
	
	dockData.d_fApproachAngle = GET_HELI_DOCK_APPROACH_ANGLE(dockData)
	dockData.d_fOldApproachAngle = dockData.d_fApproachAngle
	
	START_WIDGET_GROUP("Heli docking")
		ADD_WIDGET_VECTOR_SLIDER("Helipad position", dockData.vDockPosition, -1000000.0, 1000000.0, 0.01)
		ADD_WIDGET_BOOL("Delayed landing start", dockData.d_bDelayedLandingStart)
		ADD_WIDGET_BOOL("Delay before accepting broadcast", dockData.d_bDelayBeforeAcceptingBroadcast)
		ADD_WIDGET_BOOL("Dont send invite broadcast", dockData.d_bDontSendBroadcastInvite)
		ADD_WIDGET_BOOL("Dont lock door on landing", dockData.d_bDontLockHeliDoor)
		ADD_WIDGET_BOOL("Toggle spawn points drawing", dockData.d_bToggleDrawSpawnPoints)
		ADD_WIDGET_BOOL("Force recreate heli", dockData.d_bForceRecreateHeli)
		ADD_WIDGET_BOOL("Force fail landing", dockData.d_bFailLanding)
		ADD_WIDGET_BOOL("Force fail lanidng after heli cleanup", dockData.d_bFailLandingAfterHeliCleanup)
		ADD_WIDGET_BOOL("Simulate owner dead on heli creation", dockData.d_bSimulateOwnerDeadOnHeliCreation)
		
		START_WIDGET_GROUP("Cutscene")
			ADD_WIDGET_FLOAT_SLIDER("Cam FOV", CF_HELI_DOCK_CAM_FOV, 5.0, 90.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Cam Height", CF_HELI_DOCK_CAM_HEIGHT, 0.0, 1000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Cam Height Offset", CF_HELI_DOCK_CAM_HEIGHT_OFFSET, 0.0, 1000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Cam Radius", CF_HELI_DOCK_CAM_RADIUS, 0.0, 1000.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("Cam Rot Start", CF_HELI_DOCK_CAM_ROT_START, 0.0, 360.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Cam Rot Speed", CF_HELI_DOCK_CAM_ROT_SPEED, 0.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Approach angle", dockData.d_fApproachAngle, 0.0, 360.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Spawn height", CF_HELI_DOCK_SPAWN_HEIGHT, 0.0, 100.0, 0.1)
			ADD_WIDGET_INT_SLIDER("Landing duration", CI_HELI_DOCK_LANDING_CUTSCENE_LENGTH, 1000, 6000000, 100)
			ADD_WIDGET_INT_SLIDER("Takeoff duration", CI_HELI_DOCK_TAKEOFF_CUTSCENE_LENGTH, 1000, 6000000, 100)
			ADD_WIDGET_INT_SLIDER("Takeoff PA announcement time", CI_HELI_DOCK_TAKEOFF_PILOT_ANNOUNCEMENT_TIME, 0, 600000, 100)
			ADD_WIDGET_INT_SLIDER("Fade out duration", CI_HELI_DOCK_FADE_OUT_TIME, 0, 6000, 10)
			ADD_WIDGET_INT_SLIDER("Quick travel dialogue time", CI_HELI_DOCK_QUICK_TRAVEL_PILOT_DIALOGUE_TIME, 0, 600000, 100)
			
			ADD_WIDGET_FLOAT_SLIDER("CF_HELI_DOCK_LOD_OVERRIDE", CF_HELI_DOCK_LOD_OVERRIDE, 0.0, 10.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("CF_HELI_DOCK_CAM_DOF_STRENGTH", CF_HELI_DOCK_CAM_DOF_STRENGTH, 0.0, 1.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("CF_HELI_DOCK_CAM_DOF_NEAR_OUT", CF_HELI_DOCK_CAM_DOF_NEAR_OUT, 0.0, 1000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("CF_HELI_DOCK_CAM_DOF_FAR_OUT", CF_HELI_DOCK_CAM_DOF_FAR_OUT, 0.0, 1000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("CF_HELI_DOCK_CAM_DOF_NEAR_IN", CF_HELI_DOCK_CAM_DOF_NEAR_IN, 0.0, 1000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("CF_HELI_DOCK_CAM_DOF_FAR_IN", CF_HELI_DOCK_CAM_DOF_FAR_IN, 0.0, 1000.0, 0.1)

		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("globalPlayerBD BitSet")
			ADD_WIDGET_INT_READ_ONLY("BS_HELI_DOCK_GLOBAL_PLAYER_BD_READY_FOR_CUTSCENE", 		dockData.d_iGlobalPlayerBD_ReadyForCutscene)
			ADD_WIDGET_INT_READ_ONLY("BS_HELI_DOCK_GLOBAL_PLAYER_BD_CUTSCENE_DONE",				dockData.d_iGlobalPlayerBD_CutsceneDone)
			ADD_WIDGET_INT_READ_ONLY("BS_HELI_DOCK_GLOBAL_PLAYER_BD_WARP_DONE",					dockData.d_iGlobalPlayerBD_WarpDone)
			ADD_WIDGET_INT_READ_ONLY("BS_HELI_DOCK_GLOBAL_PLAYER_BD_DOING_CUTSCENE", 			dockData.d_iGlobalPlayerBD_DoingCutscene)
			ADD_WIDGET_INT_READ_ONLY("BS_HELI_DOCK_GLOBAL_PLAYER_BD_READY_FOR_QUICK_TRAVEL", 	dockData.d_iGlovalPlayerBD_ReadyForQuickTravel)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("g_OfficeHeliDockData")
			ADD_WIDGET_BOOL("bDoingCutscene", g_OfficeHeliDockData.bDoingCutscene)
			ADD_WIDGET_BOOL("bEnteredPropertyViaLanding", g_OfficeHeliDockData.bEnteredPropertyViaLanding)
			ADD_WIDGET_BOOL("bRecreatePegasusAfterLeavingOffice", g_OfficeHeliDockData.bRecreatePegasusAfterLeavingOffice)
			ADD_WIDGET_BOOL("bRecreateBossLimoAfterLeavingOffice", g_OfficeHeliDockData.bRecreateBossLimoAfterLeavingOffice)
			ADD_WIDGET_BOOL("bOwnerWantsToQuickWarpToOffice", g_OfficeHeliDockData.bOwnerWantsToQuickWarpToOffice)
			ADD_WIDGET_BOOL("bBossWantsToTakeoff", g_OfficeHeliDockData.bBossWantsToTakeoff)
			ADD_WIDGET_BOOL("bOwnerWantsToLand", g_OfficeHeliDockData.bOwnerWantsToLand)
			ADD_WIDGET_BOOL("bCreateHeliForTakeoff", g_OfficeHeliDockData.bCreateHeliForTakeoff)
			ADD_WIDGET_BOOL("bHeliForTakeoffCreated", g_OfficeHeliDockData.bHeliForTakeoffCreated)
			ADD_WIDGET_BOOL("bCalledFromHeliTaxi", g_OfficeHeliDockData.bCalledFromHeliTaxi)
			ADD_WIDGET_BOOL("bHeliTaxiShouldCleanUpThePilot", g_OfficeHeliDockData.bHeliTaxiShouldCleanUpThePilot)
			ADD_WIDGET_INT_SLIDER("iQuickWarpState", g_HeliTaxiQuickTravelData.iQuickWarpState, 0, 99, 1)
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

PROC MAINTAIN_HELI_DOCK_DEBUG_WIDGETS(PROPERTY_HELI_DOCK_DATA &dockData)
	//UNUSED_PARAMETER(dockData)
	
	INT iBS = globalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.iBS
	
	dockData.d_iGlobalPlayerBD_ReadyForCutscene		= PICK_INT(IS_BIT_SET(iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_READY_FOR_CUTSCENE), 1, 0)
	dockData.d_iGlobalPlayerBD_CutsceneDone			= PICK_INT(IS_BIT_SET(iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_CUTSCENE_DONE), 1, 0)
	dockData.d_iGlobalPlayerBD_WarpDone				= PICK_INT(IS_BIT_SET(iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_WARP_DONE), 1, 0)
	dockData.d_iGlobalPlayerBD_DoingCutscene		= PICK_INT(IS_BIT_SET(iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_DOING_CUTSCENE), 1, 0)
	dockData.d_iGlovalPlayerBD_ReadyForQuickTravel	= PICK_INT(IS_BIT_SET(iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_READY_FOR_QUICK_TRAVEL), 1, 0)
	
	IF dockData.d_bToggleDrawSpawnPoints
		IF NOT (dockData.d_bDrawSpawnPoints)
			COMPUTE_AND_SET_HELI_DOCK_TAKEOFF_DIRECTION(dockData)
			CREATE_HELI_DOCK_TAKEOFF_SPAWN_POINTS(dockData)
			dockData.d_bDrawSpawnPoints = TRUE			
		ELSE
			dockData.d_bDrawSpawnPoints = FALSE
			CLEAR_CUSTOM_SPAWN_POINTS()
			USE_CUSTOM_SPAWN_POINTS(FALSE)
		ENDIF
		
		dockData.d_bToggleDrawSpawnPoints = FALSE
	ENDIF
	
	IF dockData.d_bDrawSpawnPoints
		INT i
		REPEAT g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints i
			DRAW_DEBUG_SPHERE(g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos, 9.0, 0, 255, 0, 128)
		ENDREPEAT
	ENDIF
	
	IF dockData.d_bForceRecreateHeli
		g_OfficeHeliDockData.bDoRecreateHeli = TRUE
		g_OfficeHeliDockData.bRecreateBossLimoAfterLeavingOffice = TRUE
		g_OfficeHeliDockData.heliSetup.VehicleSetup.eModel = BUZZARD
		ADD_CUSTOM_VEHICLE_NODE(dockData.vDockPosition, 0.0)
		g_OfficeHeliDockData.vRecreateSpawnPoint = dockData.vDockPosition
		dockData.d_bForceRecreateHeli = FALSE
	ENDIF
	
	IF dockData.d_fApproachAngle != dockData.d_fOldApproachAngle
		COMPUTE_AND_SET_HELI_DOCK_TAKEOFF_DIRECTION(dockData, dockData.d_fApproachAngle)
		dockData.d_fOldApproachAngle = dockData.d_fApproachAngle
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL IS_PED_IN_PERSONAL_ASSISTANT_TRIGGER_ZONE(PED_INDEX pPed, OFFICE_PERSONAL_ASSISTANT& sOfficePA, INT iCurrentProperty, INT RandomPAClothes)
	
	#IF NOT IS_DEBUG_BUILD
	UNUSED_PARAMETER(sOfficePA)
	#ENDIF
	
	
	VECTOR vOffset = <<-1.0000, 0.0000, 0.0000>>
	VECTOR vBounds = <<2.5000, 1.5000, 2.0000>>
	FLOAT fRotation = 0.0000
	
	MP_PROP_OFFSET_STRUCT sPAOffset
	IF IS_PROPERTY_OFFICE(iCurrentProperty)
		IF GET_DISTANCE_BETWEEN_ENTITIES(sOfficePA.PAPedIndex, pPed) < 2
			RETURN TRUE
		ENDIF
		#IF IS_DEBUG_BUILD
		IF sOfficePA.bDrawDebug
			DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(sOfficePA.PAPedIndex), 2, 0, 0, 255, 31)
		ENDIF
		#ENDIF
		
		GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_PERSONAL_ASSISTANT, sPAOffset,PROPERTY_OFFICE_2_BASE)
	ELIF (iCurrentProperty = PROPERTY_DEFUNC_BASE)
		IF RandomPAClothes = 0
			sPAOffset.vLoc = <<405.4020, 4827.1328, -59.5444>>
			sPAOffset.vRot = <<0.0000, 0.0000, 167.7600>>
			
			vOffset = <<-1.0000, 0.0000, 0.0000>>
			vBounds = <<1.5000, 1.0000, 2.0000>>
			fRotation = 0.0000
		ELSE
			sPAOffset.vLoc = <<412.4140, 4828.9058, -59.5407>>
			sPAOffset.vRot = <<0,0,77.760>>
				
			vOffset = <<-1.3000, 0.0000, 0.0000>>
			vBounds = <<1.5000, 1.0000, 2.0000>>
			fRotation = 0.0000
		ENDIF
	ELIF (iCurrentProperty = PROPERTY_CASINO_GARAGE)
//		url:bugstar:4046963 - Please reduce the size of the receptionist triggers
//		IF GET_DISTANCE_BETWEEN_ENTITIES(sOfficePA.PAPedIndex, pPed) < 2
//			RETURN TRUE
//		ENDIF
//		#IF IS_DEBUG_BUILD
//		IF sOfficePA.bDrawDebug
//			DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(sOfficePA.PAPedIndex), 2, 0, 0, 255, 31)
//		ENDIF
//		#ENDIF
		
		sPAOffset.vLoc = <<1114.7970, 253.6330, -46.2420>>
		sPAOffset.vRot = <<0.0000, 0.0000, 95.0000>>
		
		vOffset = <<1.6000, 0.0000, 0.5000>>
		vBounds = <<2.0000, 1.0000, 1.0000>>
		fRotation = -5.0000
		
		RETURN FALSE
//		IF IS_KEYBOARD_KEY_JUST_PRESSED(key_3)
//			BOOL bkey_3_released = FALSE
//			
//			WIDGET_GROUP_ID wg_key_3 = START_WIDGET_GROUP("bkey_3_released")
//				ADD_WIDGET_BOOL("bkey_3_released", bkey_3_released)
//				ADD_WIDGET_VECTOR_SLIDER("sPAOffset.vLoc", sPAOffset.vLoc, -5000, 5000, 0.1)
//				ADD_WIDGET_VECTOR_SLIDER("sPAOffset.vRot", sPAOffset.vRot, -360, 360, 0.1)
//				ADD_WIDGET_STRING("bounds")
//				ADD_WIDGET_VECTOR_SLIDER("vOffset", vOffset, -10, 10, 0.1)
//				ADD_WIDGET_VECTOR_SLIDER("vBounds", vBounds, 0, 10, 0.1)
//				ADD_WIDGET_FLOAT_SLIDER("fRotation", fRotation, -360, 360, 0.1)
//			STOP_WIDGET_GROUP()
//			
//			WHILE NOT bkey_3_released
//			AND NOT IS_PED_INJURED(pPed)
//				IS_PED_AT_ANGLED_COORD(pPed, sPAOffset.vLoc+vOffset, vBounds, sPAOffset.vRot.z+fRotation)
//				WAIT(0)
//			ENDWHILE
//			
//			SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("key_3 sPAOffset.vLoc = ")SAVE_VECTOR_TO_DEBUG_FILE(sPAOffset.vLoc)SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("key_3 sPAOffset.vRot = ")SAVE_VECTOR_TO_DEBUG_FILE(sPAOffset.vRot)SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("key_3 vOffset = ")SAVE_VECTOR_TO_DEBUG_FILE(vOffset)SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("key_3 vBounds = ")SAVE_VECTOR_TO_DEBUG_FILE(vBounds)SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_STRING_TO_DEBUG_FILE("key_3 fRotation = ")SAVE_FLOAT_TO_DEBUG_FILE(fRotation)SAVE_NEWLINE_TO_DEBUG_FILE()
//			SAVE_NEWLINE_TO_DEBUG_FILE()
//			DELETE_WIDGET_GROUP(wg_key_3)
//		ENDIF
	ENDIF
	
	IF IS_PED_AT_ANGLED_COORD(pPed, sPAOffset.vLoc+vOffset, vBounds, sPAOffset.vRot.z+fRotation)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Maintain personal assistant service's menu 
PROC MAINTAIN_PERSONAL_ASSISTANT_MENU(OFFICE_PERSONAL_ASSISTANT& sOfficePA,INT iCurrentProperty,PLAYER_INDEX officeOwner,INT RandomPAClothes = -1, BOOL bCutsceneRunning = FALSE)
	
	SWITCH sOfficePA.ePAInteractionStage
		CASE PA_STAGE_WAITING_TO_TRIGGER
			IF DOES_ENTITY_EXIST(sOfficePA.PAPedIndex)
			AND IS_ENTITY_ALIVE(sOfficePA.PAPedIndex)
				IF (IS_PROPERTY_OFFICE(iCurrentProperty) AND GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("Reception_RM"))
				OR (iCurrentProperty = PROPERTY_DEFUNC_BASE)
				OR (iCurrentProperty = PROPERTY_CASINO_GARAGE)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					AND IS_PED_IN_PERSONAL_ASSISTANT_TRIGGER_ZONE(PLAYER_PED_ID(), sOfficePA, iCurrentProperty, RandomPAClothes)
					AND IS_SAFE_TO_START_OFFICE_MENUS(TRUE)
						IF sOfficePA.iPAServiceContext = NEW_CONTEXT_INTENTION
							IF NOT IS_HELP_MESSAGE_ON_SCREEN()
							AND NOT bCutsceneRunning
								IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
								AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 0.2
									IF IS_PROPERTY_OFFICE(iCurrentProperty)
										IF IS_OFFICE_PA_MALE(FALSE)
											REGISTER_CONTEXT_INTENTION(sOfficePA.iPAServiceContext, CP_HIGH_PRIORITY, "OF_PAssitant")
										ELSE
											REGISTER_CONTEXT_INTENTION(sOfficePA.iPAServiceContext, CP_HIGH_PRIORITY, "OF_PAssitantF")
										ENDIF
									ELIF (iCurrentProperty = PROPERTY_DEFUNC_BASE)
										REGISTER_CONTEXT_INTENTION(sOfficePA.iPAServiceContext, CP_HIGH_PRIORITY, "GO_PAssitant")
									ELIF (iCurrentProperty = PROPERTY_CASINO_GARAGE)
										REGISTER_CONTEXT_INTENTION(sOfficePA.iPAServiceContext, CP_HIGH_PRIORITY, "CA_PAssitant")
									ENDIF
									
									SET_BIT(sOfficePA.iPASpeechBs, OFF_PA_SPEECH_HI)
								ENDIF	
							ENDIF	
						ENDIF
						IF NOT IS_INTERACTION_MENU_OPEN()
						AND NOT bCutsceneRunning
							IF HAS_CONTEXT_BUTTON_TRIGGERED(sOfficePA.iPAServiceContext)
								LOAD_MENU_ASSETS("SNK_MNU")
								BUILD_PA_SERVICE_MAIN_MENU(sOfficePA,iCurrentProperty,officeOwner)
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
								RELEASE_CONTEXT_INTENTION(sOfficePA.iPAServiceContext)
								IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
									CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
									FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
								ENDIF
								
								SET_BIT(sOfficePA.sSnacksShop.iBoolsBitSet2, bi2OfficePASpeech_OFFER)
								sOfficePA.ePAInteractionStage = PA_STAGE_INTERACTING
							ENDIF
						ENDIF	
					ELIF sOfficePA.iPAServiceContext != NEW_CONTEXT_INTENTION
						RELEASE_CONTEXT_INTENTION(sOfficePA.iPAServiceContext)
						SET_BIT(sOfficePA.iPASpeechBs, OFF_PA_SPEECH_BYE)
					ENDIF
				ENDIF
			ENDIF	
		BREAK
		CASE PA_STAGE_INTERACTING
			IF bCutsceneRunning
			OR IS_PAUSE_MENU_ACTIVE_EX()
			OR g_bBlockOfficeMenus
			OR SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER()
				PRINTLN("MAINTAIN_PERSONAL_ASSISTANT_MENU - Cutscene running - Sending to PA_STAGE_CLEANUP")
				PA_MENU_CLEANUP(sOfficePA, TRUE)
			ELSE
				PROCESS_PA_SERVICES(sOfficePA,iCurrentProperty,officeOwner)
			ENDIF
		BREAK
		CASE PA_STAGE_CASH_TRANSACTION
			PROCESS_PA_CASH_TRANSACTION(sOfficePA)
		BREAK
		CASE PA_STAGE_POST_ACCEPT
			
			PROCESS_PA_POST_ACCEPT(sOfficePA)
		BREAK
		CASE PA_STAGE_CLEANUP
			PROCESS_PA_MENU_CLEANUP(sOfficePA)
			START_NET_TIMER(sOfficePA.sMenuTimer)
		BREAK
	ENDSWITCH
	
	IF HAS_NET_TIMER_STARTED(sOfficePA.sMenuTimer)
		IF NOT HAS_NET_TIMER_EXPIRED(sOfficePA.sMenuTimer, 1000)
			DISABLE_FRONTEND_THIS_FRAME()
		ELSE
			RESET_NET_TIMER(sOfficePA.sMenuTimer)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_LOCAL_PA(OFFICE_PERSONAL_ASSISTANT& sOfficePA,BOOL bIsPreview = FALSE)
	REMOVE_ANIM_DICT("ANIM@AMB@OFFICE@PA@MALE@")
	REMOVE_ANIM_DICT("ANIM@AMB@OFFICE@PA@FEMALE@")
	
	IF DOES_ENTITY_EXIST(sOfficePA.PAChairIndex)
		DELETE_OBJECT(sOfficePA.PAChairIndex)
		CLEAR_BIT(sOfficePA.iPALocalBS,OFF_PA_CHAIR_INIT)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sOfficePA.PAPedIndex)
		DELETE_PED(sOfficePA.PAPedIndex)
		CLEAR_BIT(sOfficePA.iPALocalBS,OFF_PA_PED_INIT)
	ENDIF
	
	IF bIsPreview
		sOfficePA.PAanimStage = 2
	ELSE
		sOfficePA.PAanimStage = 0
		g_iOfficePersonalAssistantSpeechBitset = 0
	ENDIF
	
	CLEAR_BIT(sOfficePA.iPAAnimBs,PA_INTRO_ANIM_RUNNING)
	CLEAR_BIT(sOfficePA.iPAAnimBs,PA_GREETING_ANIM_RUNNING)
	CLEAR_BIT(sOfficePA.iPAAnimBs,PA_BASE_ANIM_RUNNING)
	CLEAR_BIT(sOfficePA.iPAAnimBs,PA_IDLE_ANIM_RUNNING)
	CLEAR_BIT(sOfficePA.iPAAnimBs,PA_IDLE_TO_INTRO_ANIM_RUNNING)
ENDPROC


/// PURPOSE:
///    Maintain office personal assistant 
///    Return true of the personal assistant is configured and created
FUNC BOOL MAINTAIN_OFFICE_PERSONAL_ASSISTANT(OFFICE_PERSONAL_ASSISTANT& sOfficePA, INT iCurrentProperty,PLAYER_INDEX OfficeOwner,INT &RandomPAClothes,INT iInteriorStage)
	
	CONST_INT iBS_CREATED_OFFICE_PA_PED		0
	CONST_INT iBS_CREATED_OFFICE_PA_CHAIR	1
	
	IF IS_PROPERTY_OFFICE(iCurrentProperty)
	OR (iCurrentProperty = PROPERTY_DEFUNC_BASE)
	OR (iCurrentProperty = PROPERTY_CASINO_GARAGE)
		// get position offset for office to place personal assistant 
		MP_PROP_OFFSET_STRUCT sPAOffset,sPAChairOffset
		MODEL_NAMES chairHideModelName, chairCreateModelName = INT_TO_ENUM(MODEL_NAMES, hash("EX_PROP_OFFCHAIR_EXEC_03"))
		IF IS_PROPERTY_OFFICE(iCurrentProperty)
			GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_PERSONAL_ASSISTANT, sPAOffset,PROPERTY_OFFICE_2_BASE)	
			GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_PERSONAL_ASSISTANT_CHAIR, sPAChairOffset,PROPERTY_OFFICE_2_BASE)
			chairHideModelName = chairCreateModelName
		ELIF (iCurrentProperty = PROPERTY_DEFUNC_BASE)
			IF RandomPAClothes = 0
				sPAOffset.vLoc = <<405.2788, 4829.6050, -59.3940>>
				sPAOffset.vRot = <<0.0000, 0.0000, 5.0000>>
				sPAChairOffset.vLoc = <<405.1318,4829.5342,-60>>
				sPAChairOffset.vRot = <<0,0,0>>
			ELSE
				sPAOffset.vLoc = <<409.0461, 4829.9731, -59.3940>>
				sPAOffset.vRot = <<0.0000, 0.0000, 91.0000>>
				sPAChairOffset.vLoc = <<409.0791,4829.8696,-60>>
				sPAChairOffset.vRot = <<0,0,60>>
			ENDIF
			chairHideModelName = INT_TO_ENUM(MODEL_NAMES, hash("v_corp_offchair"))
		ELIF (iCurrentProperty = PROPERTY_CASINO_GARAGE)
			sPAOffset.vLoc = <<1114.9139, 253.6600, -46.2020>>
			sPAOffset.vRot = <<0.0000, 0.0000, 95.0000>>
			sPAChairOffset.vLoc = <<1114.9139, 253.6600, -46.2020>>
			sPAChairOffset.vRot = <<0.0000, 0.0000, 95.0000>>
			chairHideModelName = INT_TO_ENUM(MODEL_NAMES, hash("v_corp_offchair"))
		ENDIF
		
		IF DOES_ENTITY_EXIST(sOfficePA.PAChairIndex)
		AND NOT IS_BIT_SET(sOfficePA.iPALocalBS,OFF_PA_CHAIR_HIDDEN)
			CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(sPAOffset.vLoc, 2.0, chairCreateModelName, FALSE)
			SET_BIT(sOfficePA.iPALocalBS,OFF_PA_CHAIR_HIDDEN)
		ENDIF
		IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		AND OfficeOwner != INVALID_PLAYER_INDEX()
			IF PLAYER_ID() = OfficeOwner
				IF IS_PROPERTY_OFFICE(iCurrentProperty)
					IF IS_OFFICE_PA_MALE(FALSE)
						IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OFFICE_MALE_PA)
							SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OFFICE_MALE_PA)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_PERSONAL_ASSISTANT: PROPERTY_BROADCAST_BS_OFFICE_MALE_PA - TRUE")
						ENDIF	
						IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_PA_GENDER_SET)
							SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_PA_GENDER_SET)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_PERSONAL_ASSISTANT: PROPERTY_BROADCAST_BS_OFFICE_MALE_PA - TRUE - setting PROPERTY_BROADCAST_BS2_OFFICE_PA_GENDER_SET TRUE")
						ENDIF
					ELSE
						IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OFFICE_MALE_PA)
							CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_OFFICE_MALE_PA)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_PERSONAL_ASSISTANT: PROPERTY_BROADCAST_BS_OFFICE_MALE_PA - FALSE")
						ENDIF
						IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_PA_GENDER_SET)
							SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_PA_GENDER_SET)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_PERSONAL_ASSISTANT: PROPERTY_BROADCAST_BS_OFFICE_MALE_PA - FALSE - setting PROPERTY_BROADCAST_BS2_OFFICE_PA_GENDER_SET TRUE")
						ENDIF
					ENDIF
				ELIF (iCurrentProperty = PROPERTY_DEFUNC_BASE)
					//
				ELIF (iCurrentProperty = PROPERTY_CASINO_GARAGE)
					//
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(OfficeOwner)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OFFICE_PA_GENDER_SET)
			OR (iCurrentProperty = PROPERTY_DEFUNC_BASE)
			OR (iCurrentProperty = PROPERTY_CASINO_GARAGE)

				IF NOT IS_BIT_SET(sOfficePA.iPALocalBS,OFF_PA_PED_INIT)
					IF (NATIVE_TO_INT(OfficeOwner) != -1)
						MODEL_NAMES PAModelName
						PED_TYPE PAPedType
						STRING sAnimDic
						IF IS_PROPERTY_OFFICE(iCurrentProperty)
							IF IS_PLAYERS_OFFICE_PA_MALE(OfficeOwner)
								PAModelName = GET_OFFICE_PERSONAL_ASSISTANT_MALE_MODEL()
								sAnimDic = "ANIM@AMB@OFFICE@PA@MALE@"
								PAPedType = PEDTYPE_CIVMALE 
							ELSE
								PAModelName = GET_OFFICE_PERSONAL_ASSISTANT_FEMALE_MODEL()
								sAnimDic = "ANIM@AMB@OFFICE@PA@FEMALE@"
								PAPedType = PEDTYPE_CIVFEMALE
							ENDIF
						ELIF (iCurrentProperty = PROPERTY_DEFUNC_BASE)
							IF RandomPAClothes = 0
								PAModelName = GET_DEFUNCT_BASE_PERSONAL_ASSISTANT_FEMALE_MODEL()
								sAnimDic = "ANIM@AMB@OFFICE@PA@FEMALE@"
								PAPedType = PEDTYPE_CIVFEMALE
							ELSE
								PAModelName = GET_DEFUNCT_BASE_PERSONAL_ASSISTANT_MALE_MODEL()
								sAnimDic = "ANIM@AMB@OFFICE@PA@MALE@"
								PAPedType = PEDTYPE_CIVMALE
							ENDIF
						ELIF (iCurrentProperty = PROPERTY_CASINO_GARAGE)
							PAModelName = GET_CASINO_PERSONAL_ASSISTANT_FEMALE_MODEL()
							sAnimDic = "ANIM@AMB@OFFICE@PA@FEMALE@"
							PAPedType = PEDTYPE_CIVFEMALE
						ENDIF
						REQUEST_MODEL(PAModelName)
						REQUEST_ANIM_DICT(sAnimDic)
						IF NOT HAS_MODEL_LOADED(PAModelName)
						AND NOT HAS_ANIM_DICT_LOADED(sAnimDic)
							RETURN FALSE
						ELSE
							IF NOT DOES_ENTITY_EXIST(sOfficePA.PAPedIndex)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_PERSONAL_ASSISTANT: creating PA")
								IF REQUEST_LOAD_MODEL(PAModelName)
									sOfficePA.PAPedIndex = CREATE_PED(PAPedType,PAModelName ,sPAOffset.vLoc, sPAOffset.vRot.z, FALSE, FALSE)
									IF IS_PROPERTY_OFFICE(iCurrentProperty)
										IF IS_PLAYERS_OFFICE_PA_MALE(OfficeOwner)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_PERSONAL_ASSISTANT - Male PA created ")
										ELSE
											CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_PERSONAL_ASSISTANT - Female PA created ")
										ENDIF
										CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_PERSONAL_ASSISTANT - personal assistant create at location: ", sPAOffset.vLoc)
										CONFIGURE_OFFICE_PERSONAL_ASSISTANT(sOfficePA.PAPedIndex, iCurrentProperty, (g_iCurrentPropertyVariation-1),RandomPAClothes,OfficeOwner)
									ELIF (iCurrentProperty = PROPERTY_DEFUNC_BASE)
										SET_ENTITY_CAN_BE_DAMAGED(sOfficePA.PAPedIndex, FALSE)
										SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sOfficePA.PAPedIndex, TRUE)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sOfficePA.PAPedIndex, TRUE)
										SET_PED_AS_ENEMY(sOfficePA.PAPedIndex, FALSE)
										
										// Set all drawable to zero
										SET_PED_DEFAULT_COMPONENT_VARIATION(sOfficePA.PAPedIndex)
										
										SET_PED_CONFIG_FLAG(sOfficePA.PAPedIndex, PCF_UseKinematicModeWhenStationary,TRUE)
										SET_PED_CONFIG_FLAG(sOfficePA.PAPedIndex, PCF_DisableExplosionReactions,TRUE)
										SET_PED_CONFIG_FLAG(sOfficePA.PAPedIndex, PCF_DontActivateRagdollFromExplosions,TRUE)
										SET_PED_CAN_EVASIVE_DIVE(sOfficePA.PAPedIndex, FALSE)
										SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(sOfficePA.PAPedIndex, TRUE)
										SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sOfficePA.PAPedIndex, FALSE)
										SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(sOfficePA.PAPedIndex, TRUE)
										SET_PED_CAN_BE_TARGETTED(sOfficePA.PAPedIndex, FALSE)
										
										IF RandomPAClothes = 0
											CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_PERSONAL_ASSISTANT - Configure base receptionist 1 (seated)")
											
											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_HEAD, 0,0)
											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_HAIR, 1,2)
											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_TORSO, 1,1)
											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_LEG, 0,1)
											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_FEET, 0,0)
											SET_PED_PROP_INDEX(sOfficePA.PAPedIndex, ANCHOR_EYES, 0, 0)
										ELSE
											CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_PERSONAL_ASSISTANT - Configure base receptionist 2 (seated)")
											
											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_HEAD, 1,0)
											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_HAIR, 2,0)
											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_TORSO, 0,0)
											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_LEG, 0,2)
											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_FEET, 0,0)
											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_SPECIAL, 0,3) //(accs)
											CLEAR_PED_PROP(sOfficePA.PAPedIndex, ANCHOR_EYES)
										ENDIF
									ELIF (iCurrentProperty = PROPERTY_CASINO_GARAGE)
										SET_ENTITY_CAN_BE_DAMAGED(sOfficePA.PAPedIndex, FALSE)
										SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sOfficePA.PAPedIndex, TRUE)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sOfficePA.PAPedIndex, TRUE)
										SET_PED_AS_ENEMY(sOfficePA.PAPedIndex, FALSE)
										
										// Set all drawable to zero
										SET_PED_DEFAULT_COMPONENT_VARIATION(sOfficePA.PAPedIndex)
										
										SET_PED_CONFIG_FLAG(sOfficePA.PAPedIndex, PCF_UseKinematicModeWhenStationary,TRUE)
										SET_PED_CONFIG_FLAG(sOfficePA.PAPedIndex, PCF_DisableExplosionReactions,TRUE)
										SET_PED_CONFIG_FLAG(sOfficePA.PAPedIndex, PCF_DontActivateRagdollFromExplosions,TRUE)
										SET_PED_CAN_EVASIVE_DIVE(sOfficePA.PAPedIndex, FALSE)
										SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(sOfficePA.PAPedIndex, TRUE)
										SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sOfficePA.PAPedIndex, FALSE)
										SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(sOfficePA.PAPedIndex, TRUE)
										SET_PED_CAN_BE_TARGETTED(sOfficePA.PAPedIndex, FALSE)
										
										IF RandomPAClothes = 0
											CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_PERSONAL_ASSISTANT - Configure base receptionist 1 (seated)")
											
//											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_HEAD, 0,0)
//											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_HAIR, 1,2)
//											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_TORSO, 1,1)
//											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_LEG, 0,1)
//											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_FEET, 0,0)
//											SET_PED_PROP_INDEX(sOfficePA.PAPedIndex, ANCHOR_EYES, 0, 0)
										ELSE
											CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_PERSONAL_ASSISTANT - Configure base receptionist 2 (seated)")
											
//											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_HEAD, 1,0)
//											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_HAIR, 2,0)
//											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_TORSO, 0,0)
//											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_LEG, 0,2)
//											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_FEET, 0,0)
//											SET_PED_COMPONENT_VARIATION(sOfficePA.PAPedIndex, PED_COMP_SPECIAL, 0,3) //(accs)
//											CLEAR_PED_PROP(sOfficePA.PAPedIndex, ANCHOR_EYES)
										ENDIF
									ENDIF
									
									REQUEST_ANIM_DICT(GET_OFFICE_PERSONAL_ASSISTANT_ANIM_DICT(sOfficePA.PAPedIndex))
									SET_MODEL_AS_NO_LONGER_NEEDED(PAModelName)
									
									SET_BIT(sOfficePA.iPALocalBS,OFF_PA_PED_INIT)
								ELSE
									CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_PERSONAL_ASSISTANT: PA Model NOT loaded Model name:",GET_MODEL_NAME_FOR_DEBUG(PAModelName))
									RETURN FALSE
								ENDIF
								RETURN FALSE
							ENDIF	
						ENDIF
					ENDIF
				ENDIF	
				IF NOT IS_BIT_SET(sOfficePA.iPALocalBS,OFF_PA_CHAIR_INIT)
					REQUEST_MODEL(chairCreateModelName)
					
					IF NOT HAS_MODEL_LOADED(chairCreateModelName)
						RETURN FALSE
					ELSE
						IF NOT DOES_ENTITY_EXIST(sOfficePA.PAChairIndex)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_PERSONAL_ASSISTANT: creating PA")
							IF REQUEST_LOAD_MODEL(chairCreateModelName)
								CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(sPAOffset.vLoc, 2.0, chairHideModelName, FALSE)
								CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(sPAOffset.vLoc, 2.0, chairCreateModelName, FALSE)
								SET_BIT(sOfficePA.iPALocalBS,OFF_PA_CHAIR_HIDDEN)
								sOfficePA.PAChairIndex = CREATE_OBJECT_NO_OFFSET(chairCreateModelName,sPAChairOffset.vLoc,FALSE,FALSE)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_PERSONAL_ASSISTANT - personal assistant chair create at location: ", sPAOffset.vLoc)
								SET_ENTITY_ROTATION(sOfficePA.PAChairIndex,sPAChairOffset.vRot)
												
												
								IF IS_PROPERTY_OFFICE(iCurrentProperty)
									//
								ELIF (iCurrentProperty = PROPERTY_DEFUNC_BASE)
									//
									SET_ENTITY_LOD_DIST(sOfficePA.PAChairIndex, 150)
								ELIF (iCurrentProperty = PROPERTY_CASINO_GARAGE)
									//
								ENDIF
								
								SET_MODEL_AS_NO_LONGER_NEEDED(chairCreateModelName)

								SET_BIT(sOfficePA.iPALocalBS,OFF_PA_CHAIR_INIT)
							ELSE
								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_PERSONAL_ASSISTANT:Chair Model NOT loaded")
								RETURN FALSE
							ENDIF
							RETURN FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
			IF DOES_ENTITY_EXIST(sOfficePA.PAPedIndex)
			AND DOES_ENTITY_EXIST(sOfficePA.PAChairIndex)
				IF IS_ENTITY_ALIVE(sOfficePA.PAPedIndex)
						SET_PED_RESET_FLAG(sOfficePA.PAPedIndex, PRF_DisablePotentialBlastReactions, TRUE)
						
						#IF IS_DEBUG_BUILD
						IF GET_DRAW_DEBUG_COMMANDLINE_PARAM_EXISTS("office_pa")
							IF !sOfficePA.bDrawDebug
								SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
								sOfficePA.bDrawDebug = TRUE
							ENDIF	
						ENDIF
						#ENDIF
						
						// Perform tasks here (PA animation)
						CONTROL_OFFICE_PERSONAL_ASSISTANT(sOfficePA,sPAOffset,OfficeOwner,iInteriorStage,iCurrentProperty)
						RETURN TRUE
					CREATE_MODEL_HIDE_EXCLUDING_SCRIPT_OBJECTS(sPAOffset.vLoc, 2.0, GET_ENTITY_MODEL(sOfficePA.PAChairIndex), TRUE)
					IF NOT IS_BIT_SET(sOfficePA.iPALocalBS,OFF_PA_CHAIR_HIDDEN)
						SET_BIT(sOfficePA.iPALocalBS,OFF_PA_CHAIR_HIDDEN)
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Maintain vault weapon loadout  
PROC MAINTAIN_OFFICE_VAULT_WEAPON_LOADOUT(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization, INT iCurrentProperty,NETWORK_INDEX &VaultDoor,PLAYER_INDEX OfficeOwnerPlayerIndex)
	IF IS_PROPERTY_OFFICE(iCurrentProperty)
	
		MP_PROP_OFFSET_STRUCT sWeaponCrateOffset[2]
		
		SWITCH sWLoadoutCustomization.eCustomizationStage
			CASE VMC_STAGE_INIT
				PROCESS_WEAPON_VAULT_MENU_CLEANUP(sWLoadoutCustomization,VaultDoor,iCurrentProperty)
				
				INIT_OFFICE_VAULT_WEAPON_CRATE(sWLoadoutCustomization,iCurrentProperty,VaultDoor,OfficeOwnerPlayerIndex)
			BREAK
			
			CASE VWC_STAGE_WAITING_TO_TRIGGER 	
				// get position offset for weapon crate inside offices
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_WEAPON_CRATE_TRIGGER_POS_1, sWeaponCrateOffset[0],PROPERTY_OFFICE_2_BASE)	
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_WEAPON_CRATE_TRIGGER_POS_2, sWeaponCrateOffset[1],PROPERTY_OFFICE_2_BASE)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),sWeaponCrateOffset[0].vLoc,sWeaponCrateOffset[1].vLoc,sWeaponCrateOffset[0].vRot.x)
				AND IS_SAFE_TO_START_OFFICE_MENUS(TRUE)
				AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
				AND DOES_ENTITY_EXIST(PLAYER_PED_ID())
				//AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 0.2
					GET_NUM_AVAILABLE_WEAPON_GROUP(sWLoadoutCustomization)
					MANAGE_GUN_VAULT_WEAPONS(sWLoadoutCustomization,OfficeOwnerPlayerIndex,iCurrentProperty)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPJAC_SIT")
						CLEAR_HELP()
					ENDIF
					IF sWLoadoutCustomization.iVaultWeaponContext = NEW_CONTEXT_INTENTION
						IF NOT IS_HELP_MESSAGE_ON_SCREEN()
							IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_GUN_VAULT_DOOR_OPENED)
								REGISTER_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext, CP_HIGH_PRIORITY, "OF_VAULT_MENU")
							ELSE 
								REGISTER_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext, CP_HIGH_PRIORITY, "OF_VAULT_MENU_C")
							ENDIF
						ENDIF	
					ENDIF
					IF NOT IS_INTERACTION_MENU_OPEN()
						IF HAS_CONTEXT_BUTTON_TRIGGERED(sWLoadoutCustomization.iVaultWeaponContext)
							LOAD_MENU_ASSETS()
							BUILD_VAULT_WEAPON_MAIN_MENU(sWLoadoutCustomization)
							PRINT_WEAPON_LOADOUT_PACKED_STAT_INFO()
							IF IS_BIT_SET(sWLoadoutCustomization.iLocalBS,VAULT_DOOR_ANIM_STAGE_FINISHED)
							OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_GUN_VAULT_DOOR_OPENED)
								RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),sWLoadoutCustomization.sGunVaultDoor.vPlayerPos ,1,DEFAULT_TIME_BEFORE_WARP,sWLoadoutCustomization.sGunVaultDoor.fPlayerHeading)
								sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_CUSTOMIZING 
							ELSE
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sWLoadoutCustomization.sGunVaultDoor.sAnimDicName, "OPEN",sWLoadoutCustomization.sGunVaultDoor.vScenePosition, sWLoadoutCustomization.sGunVaultDoor.vSceneHeading),1,DEFAULT_TIME_BEFORE_WARP,sWLoadoutCustomization.sGunVaultDoor.fPlayerHeading)
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseKinematicModeWhenStationary, TRUE)
								SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontActivateRagdollFromExplosions, TRUE)
								sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_DOOR_OPEN_ANIM
							ENDIF
						ELIF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_GUN_VAULT_DOOR_OPENED) 
						AND IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_CANCEL)
						AND DOES_ENTITY_EXIST(PLAYER_PED_ID())
						AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
						//AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 0.2
							CLEAR_PED_TASKS(PLAYER_PED_ID())
							TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(),GET_ANIM_INITIAL_OFFSET_POSITION(sWLoadoutCustomization.sGunVaultDoor.sAnimDicName, "CLOSE",sWLoadoutCustomization.sGunVaultDoor.vScenePosition, sWLoadoutCustomization.sGunVaultDoor.vSceneHeading),1,DEFAULT_TIME_BEFORE_WARP,sWLoadoutCustomization.sGunVaultDoor.fPlayerHeading)
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
							sWLoadoutCustomization.eCustomizationStage = VMC_STAGE_DOOR_CLOSE_ANIM
						ENDIF	
					ENDIF
				ELSE// sWLoadoutCustomization.iVaultWeaponContext != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(sWLoadoutCustomization.iVaultWeaponContext)
				ENDIF
			BREAK
			CASE VMC_STAGE_DOOR_OPEN_ANIM
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
					IF RUN_VAULT_DOOR_ANIM(sWLoadoutCustomization.sGunVaultDoor,sWLoadoutCustomization.iLocalBS, VaultDoor)
						PROCESS_WEAPON_VAULT_DOOR_ANIM(sWLoadoutCustomization,VaultDoor,iCurrentProperty)
					ENDIF	
				ENDIF	
			BREAK
			CASE VMC_STAGE_DOOR_CLOSE_ANIM
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
					IF RUN_VAULT_DOOR_ANIM(sWLoadoutCustomization.sGunVaultDoor,sWLoadoutCustomization.iLocalBS, VaultDoor,TRUE)
						PROCESS_WEAPON_VAULT_DOOR_ANIM(sWLoadoutCustomization,VaultDoor,iCurrentProperty,TRUE)
					ENDIF	
				ENDIF	
			BREAK
			CASE VMC_STAGE_CUSTOMIZING
				PROCESS_VAULT_WEAPON_LOADOUT_MENU(sWLoadoutCustomization)
			BREAK
			CASE VMC_STAGE_CLEANUP
				PROCESS_WEAPON_VAULT_MENU_CLEANUP(sWLoadoutCustomization,VaultDoor,iCurrentProperty)
				START_NET_TIMER(sWLoadoutCustomization.sMenuTimer)
			BREAK
		ENDSWITCH
	
		IF HAS_NET_TIMER_STARTED(sWLoadoutCustomization.sMenuTimer)
			IF NOT HAS_NET_TIMER_EXPIRED(sWLoadoutCustomization.sMenuTimer, 1000)
				DISABLE_FRONTEND_THIS_FRAME()
			ELSE
				RESET_NET_TIMER(sWLoadoutCustomization.sMenuTimer)
			ENDIF
		ENDIF
	ENDIF	
ENDPROC

/// PURPOSE:
///    Clean up office personal assistant and service menu
PROC CLEANUP_OFFICE_PERSONAL_ASSISTANT(OFFICE_PERSONAL_ASSISTANT& sOfficePA,VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization,NETWORK_INDEX &gunVaultDoor,OFFICE_VAULT_SAFE_STRUCT& sOfficeVaultSafe,NETWORK_INDEX &SafeDoor,INT iCurrentProperty, BOOL bInteriorSwitchCleanUp = FALSE)
	
	PROCESS_PA_MENU_CLEANUP(sOfficePA)
	CLEANUP_VAULT_SAFE(sOfficeVaultSafe,SafeDoor,bInteriorSwitchCleanUp)
	PROCESS_WEAPON_VAULT_MENU_CLEANUP(sWLoadoutCustomization,gunVaultDoor,iCurrentProperty,TRUE)
	
	CLEANUP_MENU_ASSETS()
	
	IF IS_BIT_SET(sOfficePA.iPALocalBS,OFF_PA_FONT_CHANGE_HELP)
		CLEAR_BIT(sOfficePA.iPALocalBS,OFF_PA_FONT_CHANGE_HELP)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Maintain Office safe vault
PROC MAINTAIN_OFFICE_VAULT_SAFE(OFFICE_VAULT_SAFE_STRUCT& sOfficeVaultSafe, INT iCurrentProperty,NETWORK_INDEX &VaultDoor,PLAYER_INDEX OfficeOwnerPlayerIndex)
	IF IS_PROPERTY_OFFICE(iCurrentProperty)
		
		MP_PROP_OFFSET_STRUCT sVaultSafeOffset[2]
		
		SWITCH sOfficeVaultSafe.eVaultSafeStage
			CASE VS_STAGE_INIT
				INIT_VAULT_SAFE_DOOR(sOfficeVaultSafe,iCurrentProperty,VaultDoor,OfficeOwnerPlayerIndex)
			BREAK 
			CASE VS_STAGE_WAITING_TO_TRIGGER
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_VAULT_SAFE_TRIGGER_POS_1, sVaultSafeOffset[0],PROPERTY_OFFICE_2_BASE)
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_VAULT_SAFE_TRIGGER_POS_2, sVaultSafeOffset[1],PROPERTY_OFFICE_2_BASE)
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),sVaultSafeOffset[0].vLoc,sVaultSafeOffset[1].vLoc,sVaultSafeOffset[0].vRot.x)
				AND IS_SAFE_TO_START_OFFICE_MENUS(TRUE)
				AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
				AND DOES_ENTITY_EXIST(PLAYER_PED_ID())
				//AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 0.2
					IF sOfficeVaultSafe.iVaultSafeContext = NEW_CONTEXT_INTENTION
						IF NOT IS_HELP_MESSAGE_ON_SCREEN()
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SAFE_VAULT_DOOR_OPENED) 
								REGISTER_CONTEXT_INTENTION(sOfficeVaultSafe.iVaultSafeContext, CP_HIGH_PRIORITY, "OF_VAULT_SAFE_2")
							ELSE
								REGISTER_CONTEXT_INTENTION(sOfficeVaultSafe.iVaultSafeContext, CP_HIGH_PRIORITY, "OF_VAULT_SAFE_1")
							ENDIF	
						ENDIF
					ENDIF	
					IF NOT IS_INTERACTION_MENU_OPEN()
						IF HAS_CONTEXT_BUTTON_TRIGGERED(sOfficeVaultSafe.iVaultSafeContext)
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SAFE_VAULT_DOOR_OPENED)
								RELEASE_CONTEXT_INTENTION(sOfficeVaultSafe.iVaultSafeContext)
								TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sOfficeVaultSafe.sVaultSafeDoor.sAnimDicName, "CLOSE",sOfficeVaultSafe.sVaultSafeDoor.vScenePosition, sOfficeVaultSafe.sVaultSafeDoor.vSceneHeading),1,DEFAULT_TIME_BEFORE_WARP,sOfficeVaultSafe.sVaultSafeDoor.fPlayerHeading)
								sOfficeVaultSafe.eVaultSafeStage = VS_STAGE_DOOR_CLOSE_ANIM
							ELSE		 
								RELEASE_CONTEXT_INTENTION(sOfficeVaultSafe.iVaultSafeContext)
								TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), GET_ANIM_INITIAL_OFFSET_POSITION(sOfficeVaultSafe.sVaultSafeDoor.sAnimDicName, "OPEN",sOfficeVaultSafe.sVaultSafeDoor.vScenePosition, sOfficeVaultSafe.sVaultSafeDoor.vSceneHeading),1,DEFAULT_TIME_BEFORE_WARP,sOfficeVaultSafe.sVaultSafeDoor.fPlayerHeading)
								sOfficeVaultSafe.eVaultSafeStage = VS_STAGE_DOOR_OPEN_ANIM 
							ENDIF
							NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,NSPC_LEAVE_CAMERA_CONTROL_ON)
						ENDIF
					ENDIF	
				PRINTLN("Vault safe - VS_STAGE_WAITING_TO_TRIGGER")
				ELSE //sOfficeVaultSafe.iVaultSafeContext != NEW_CONTEXT_INTENTION
					RELEASE_CONTEXT_INTENTION(sOfficeVaultSafe.iVaultSafeContext)
				ENDIF
			BREAK
			CASE VS_STAGE_DOOR_OPEN_ANIM
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
					IF RUN_VAULT_DOOR_ANIM(sOfficeVaultSafe.sVaultSafeDoor,sOfficeVaultSafe.iDoorAnimBS, VaultDoor,FALSE)
						PROCESS_VAULT_SAFE_DOOR_ANIM(sOfficeVaultSafe,VaultDoor)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_VAULT_SAFE: VS_STAGE_DOOR_OPEN_ANIM" )
					ENDIF
				ENDIF	
			BREAK
			CASE VS_STAGE_DOOR_CLOSE_ANIM
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
					IF RUN_VAULT_DOOR_ANIM(sOfficeVaultSafe.sVaultSafeDoor,sOfficeVaultSafe.iDoorAnimBS, VaultDoor,TRUE)
						PROCESS_VAULT_SAFE_DOOR_ANIM(sOfficeVaultSafe,VaultDoor,TRUE)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_VAULT_SAFE: VS_STAGE_DOOR_CLOSE_ANIM" )
					ENDIF
				ENDIF	
			BREAK
			CASE VS_STAGE_CLEANUP
				CLEANUP_VAULT_SAFE(sOfficeVaultSafe,VaultDoor)
			BREAK	
		ENDSWITCH
	ENDIF
ENDPROC

// Force cash pile to room (inside safe vault)
PROC FORCE_SAFE_CASH_PILE(OFFICE_VAULT_SAFE_STRUCT& sOfficeVaultSafe,INTERIOR_INSTANCE_INDEX apartmentInterior)
	INT iNumCashPile
	FOR iNumCashPile = 0 TO MAX_NUMBER_SAFE_PROP_PILE - 1
		IF DOES_ENTITY_EXIST(sOfficeVaultSafe.cashPile[iNumCashPile])
			FORCE_ROOM_FOR_ENTITY(sOfficeVaultSafe.cashPile[iNumCashPile],apartmentInterior,HASH("Office_FRONT_RM"))
		ENDIF
	ENDFOR	
ENDPROC
 
// Force gun vault's weapon models to room 
PROC FORCE_GUN_LOCKER_WEAPONS(VAULT_WEAPON_LOADOUT_CUSTOMIZATION& sWLoadoutCustomization,INTERIOR_INSTANCE_INDEX apartmentInterior)
	INT iNumWeapons
	FOR iNumWeapons = 0 TO MAX_NUMBER_WEAPON_MODELS - 1
		IF DOES_ENTITY_EXIST(sWLoadoutCustomization.weapons[iNumWeapons])
			FORCE_ROOM_FOR_ENTITY(sWLoadoutCustomization.weapons[iNumWeapons],apartmentInterior,HASH("Office_FRONT_RM"))
		ENDIF
	ENDFOR	
ENDPROC

FUNC VECTOR GET_OFFICE_NAME_PROP_COORDS_FROM_OFFICE_TYPE(INT iCurrentProperty)
	
	SWITCH iCurrentProperty
		CASE PROPERTY_OFFICE_1 		RETURN <<-1569.9714, -576.3257, 109.9195>>
		CASE PROPERTY_OFFICE_2_BASE RETURN <<-1378.0634, -477.3345, 73.4387>>
		CASE PROPERTY_OFFICE_3 		RETURN <<-138.8315, -635.6047, 170.2170>>
		CASE PROPERTY_OFFICE_4 		RETURN <<-70.8763, -811.0118, 244.1320>>
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

FUNC INT GET_FONT_COLOUR_PALETTE_FROM_INTERIOR_DECOR(INT iInteriorDecor)
	INT iPalette
	SWITCH iInteriorDecor
		CASE 1 iPalette = 4 BREAK
		CASE 2 iPalette = 5 BREAK
		CASE 3 iPalette = 6 BREAK
		CASE 4 iPalette = 3 BREAK
		CASE 5 iPalette = 1 BREAK
		CASE 6 iPalette = 2 BREAK
		CASE 7 iPalette = 7 BREAK
		CASE 8 iPalette = 8 BREAK
		CASE 9 iPalette = 9 BREAK
	ENDSWITCH
	RETURN iPalette
ENDFUNC

/// PURPOSE: Display organization name on office wall
PROC MAINTAIN_OFFICE_ORGANIZATION_NAME(OFFICE_ORGANIZATION_NAME& sOfficeOrganizationName, OFFICE_PERSONAL_ASSISTANT& sOfficePA, INT iCurrentProperty)

	IF IS_PROPERTY_OFFICE(iCurrentProperty)
		SWITCH sOfficeOrganizationName.eNameStates
			CASE OFFICE_NAME_STATE_LINK_RT
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_LINK_RT")
				REQUEST_SCALEFORM_MOVIE("ORGANISATION_NAME")
				
				GAMER_HANDLE sPropertyOwnerGH
				GET_GAMER_HANDLE_OF_PROPERTY_OWNER(PLAYER_ID(), sPropertyOwnerGH)
				IF IS_GAMER_HANDLE_VALID(sPropertyOwnerGH)
					IF NETWORK_IS_GAMER_IN_MY_SESSION(sPropertyOwnerGH)
						sOfficeOrganizationName.pOwnerID = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(sPropertyOwnerGH)
						
						IF sOfficeOrganizationName.pOwnerID = PLAYER_ID()
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - I am the Office Property Owner - My ID: ", NATIVE_TO_INT(sOfficeOrganizationName.pOwnerID))
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - I am Not the Office Property Owner - Owner ID: ", NATIVE_TO_INT(sOfficeOrganizationName.pOwnerID))
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_NAMED_RENDERTARGET_REGISTERED("prop_ex_office_text")
					REGISTER_NAMED_RENDERTARGET("prop_ex_office_text")
					IF NOT IS_NAMED_RENDERTARGET_LINKED(EX_PROP_EX_OFFICE_TEXT)
						LINK_NAMED_RENDERTARGET(EX_PROP_EX_OFFICE_TEXT)
						IF sOfficeOrganizationName.iRenderTargetID = -1
							sOfficeOrganizationName.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID("prop_ex_office_text")
						ELSE
							// .. Already have render target ID
						ENDIF
					ENDIF
				ENDIF
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_LINK_RT - Render Target ID: ", sOfficeOrganizationName.iRenderTargetID)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_LINK_RT - Changing State to: OFFICE_NAME_STATE_UPDATE_SCALEFORM")
				sOfficeOrganizationName.eNameStates = OFFICE_NAME_STATE_UPDATE_SCALEFORM
			BREAK
			CASE OFFICE_NAME_STATE_UPDATE_SCALEFORM
				IF sOfficeOrganizationName.bUpdateScaleform
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM")
					
					// Colour palette relies on decor but are ordered differently
					sOfficeOrganizationName.iCurrentOfficeDecor = (GET_FONT_COLOUR_PALETTE_FROM_INTERIOR_DECOR(g_iCurrentPropertyVariation)-1)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Current Office Decor: ", g_iCurrentPropertyVariation, " Palette: ", sOfficeOrganizationName.iCurrentOfficeDecor)
					
					IF sOfficeOrganizationName.pOwnerID = PLAYER_ID()
						GlobalPlayerBD_FM_3[NATIVE_TO_INT(sOfficeOrganizationName.pOwnerID)].iOfficeNameFont = GET_MP_INT_CHARACTER_STAT(MP_STAT_FONT_PLAYER_OFFICE)
						GlobalPlayerBD_FM_3[NATIVE_TO_INT(sOfficeOrganizationName.pOwnerID)].iOfficeNameFontColour = GET_MP_INT_CHARACTER_STAT(MP_STAT_COLOUR_PLAYER_OFFICE)
					ENDIF
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Current Owner Office Font: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(sOfficeOrganizationName.pOwnerID)].iOfficeNameFont, " & Font Colour: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(sOfficeOrganizationName.pOwnerID)].iOfficeNameFontColour)
					
					IF HAS_SCALEFORM_MOVIE_LOADED(sOfficeOrganizationName.sNameScaleform)
						BEGIN_SCALEFORM_MOVIE_METHOD(sOfficeOrganizationName.sNameScaleform, "SET_ORGANISATION_NAME")
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Scaleform movie 'SET_ORGANISATION_NAME' has loaded")
							
							// Dont show name for restricted accounts
							IF sOfficeOrganizationName.pOwnerID != INVALID_PLAYER_INDEX()
							AND DOES_ENTITY_EXIST(GET_PLAYER_PED(sOfficeOrganizationName.pOwnerID))
							
								IF sOfficeOrganizationName.pOwnerID = PLAYER_ID()
									CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Local player is Office Property Owner")
									
									IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
									OR GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
										CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Local player is not member of a gang")
										sOfficeOrganizationName.sName = GB_TO_STRING(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlVIPOfficeName)
										IF IS_STRING_NULL_OR_EMPTY(sOfficeOrganizationName.sName)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Office name is null, setting to Org Name")
											sOfficeOrganizationName.sName = GB_TO_STRING(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlGangName)
										ENDIF
									ELSE
										CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Local player is member of a gang")
										sOfficeOrganizationName.sName = GB_TO_STRING(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlGangName)
										IF IS_STRING_NULL_OR_EMPTY(sOfficeOrganizationName.sName)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Org name is null, setting to Office Name")
											sOfficeOrganizationName.sName = GB_TO_STRING(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlVIPOfficeName)
										ENDIF
									ENDIF
								ELSE
									CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Local player is Not Office Property Owner")
									
									IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(sOfficeOrganizationName.pOwnerID)
									OR GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(sOfficeOrganizationName.pOwnerID)
										sOfficeOrganizationName.sName = GB_GET_PLAYER_OFFICE_NAME_AS_A_STRING(sOfficeOrganizationName.pOwnerID, TRUE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Office owner is not member of a gang ", sOfficeOrganizationName.sName)
										IF IS_STRING_NULL_OR_EMPTY(sOfficeOrganizationName.sName)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Office owner name is null, setting to Org Name")
											sOfficeOrganizationName.sName = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(sOfficeOrganizationName.pOwnerID #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ENDIF
									ELSE
										CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Office owner is member of a gang")
										sOfficeOrganizationName.sName = GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(sOfficeOrganizationName.pOwnerID #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										IF IS_STRING_NULL_OR_EMPTY(sOfficeOrganizationName.sName)
											CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Owner Org name is null, setting to Office Name")
											sOfficeOrganizationName.sName = GB_GET_PLAYER_OFFICE_NAME_AS_A_STRING(sOfficeOrganizationName.pOwnerID, TRUE #IF IS_DEBUG_BUILD , TRUE #ENDIF)
										ENDIF
									ENDIF
								ENDIF
								
								#IF IS_DEBUG_BUILD
								IF bBlankOfficeName
									IF NOT IS_STRING_NULL_OR_EMPTY(sOfficeOrganizationName.sName)
										GB_SET_ORGANIZATION_NAME("",GB_ORGANISATION_NAME_TYPE_OFFICE, FALSE)
										GB_SET_OFFICE_NAME("", FALSE)
										sOfficeOrganizationName.iGangNameHash = GET_HASH_KEY(GET_FILENAME_FOR_AUDIO_CONVERSATION("GB_REST_ACC"))
									ENDIF
								ENDIF
								#ENDIF
								
								// Name
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(sOfficeOrganizationName.sName)
								sOfficeOrganizationName.iGangNameHash = GET_HASH_KEY(sOfficeOrganizationName.sName)
								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Setting Displaying Name: ", sOfficeOrganizationName.sName)
							
								// Style palette
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(sOfficeOrganizationName.iCurrentOfficeDecor)
								
								// Colour
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(sOfficeOrganizationName.pOwnerID)].iOfficeNameFontColour)
								sOfficeOrganizationName.iCurrentFontColour = GlobalPlayerBD_FM_3[NATIVE_TO_INT(sOfficeOrganizationName.pOwnerID)].iOfficeNameFontColour
								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Setting Displaying Name Font Colour: ", sOfficeOrganizationName.iCurrentFontColour)	
								
								// Font
								IF IS_LANGUAGE_NON_ROMANIC()
								OR GET_CURRENT_LANGUAGE() = LANGUAGE_POLISH
								OR GET_CURRENT_LANGUAGE() = LANGUAGE_RUSSIAN
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(-1)
									sOfficeOrganizationName.iCurrentFont = -1
									CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Language does not support custom fonts - Setting font to -1 (B* 2816403)")
								ELSE
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(sOfficeOrganizationName.pOwnerID)].iOfficeNameFont)
									sOfficeOrganizationName.iCurrentFont = GlobalPlayerBD_FM_3[NATIVE_TO_INT(sOfficeOrganizationName.pOwnerID)].iOfficeNameFont
									CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Setting Displaying Name Font: ", sOfficeOrganizationName.iCurrentFont)
								ENDIF
							ENDIF
							
						END_SCALEFORM_MOVIE_METHOD()
						SET_SCALEFORM_MOVIE_TO_USE_LARGE_RT(sOfficeOrganizationName.sNameScaleform, TRUE)
						sOfficeOrganizationName.eNameStates = OFFICE_NAME_STATE_DRAW_NAME
						sOfficeOrganizationName.bUpdateScaleform = FALSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Changing State to: OFFICE_NAME_STATE_DRAW_NAME")
					ELSE
						sOfficeOrganizationName.sNameScaleform = REQUEST_SCALEFORM_MOVIE("ORGANISATION_NAME")
					ENDIF
				ENDIF
			BREAK
			CASE OFFICE_NAME_STATE_DRAW_NAME
				SET_TEXT_RENDER_ID(sOfficeOrganizationName.iRenderTargetID)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
				SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
				DRAW_SCALEFORM_MOVIE(sOfficeOrganizationName.sNameScaleform, 0.196, 0.245, 0.46, 0.66, 255, 255, 255, 255)
				SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
				RESET_SCRIPT_GFX_ALIGN()
				
				#IF IS_DEBUG_BUILD
				IF NOT bBlankOfficeName
				#ENDIF
					
					IF sOfficeOrganizationName.pOwnerID != INVALID_PLAYER_INDEX()
					AND DOES_ENTITY_EXIST(GET_PLAYER_PED(sOfficeOrganizationName.pOwnerID))
						
						IF sOfficeOrganizationName.pOwnerID = PLAYER_ID()
							IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID())
							OR GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(PLAYER_ID())
								IF NOT IS_STRING_NULL_OR_EMPTY(GB_TO_STRING(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlVIPOfficeName))
									sOfficeOrganizationName.iGangTempHash = GET_HASH_KEY(GB_TO_STRING(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlVIPOfficeName))
								ELSE
									sOfficeOrganizationName.iGangTempHash = GET_HASH_KEY(GB_TO_STRING(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlGangName))
								ENDIF
							ELSE
								IF NOT IS_STRING_NULL_OR_EMPTY(GB_TO_STRING(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlGangName))
									sOfficeOrganizationName.iGangTempHash = GET_HASH_KEY(GB_TO_STRING(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlGangName))
								ELSE
									sOfficeOrganizationName.iGangTempHash = GET_HASH_KEY(GB_TO_STRING(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlVIPOfficeName))
								ENDIF
							ENDIF
						ELSE
							IF NOT GB_IS_PLAYER_MEMBER_OF_A_GANG(sOfficeOrganizationName.pOwnerID)
							OR GB_IS_PLAYER_MEMBER_OF_A_BIKER_GANG(sOfficeOrganizationName.pOwnerID)
								IF NOT IS_STRING_NULL_OR_EMPTY(GB_GET_PLAYER_OFFICE_NAME_AS_A_STRING(sOfficeOrganizationName.pOwnerID #IF IS_DEBUG_BUILD , TRUE #ENDIF))
									sOfficeOrganizationName.iGangTempHash = GET_HASH_KEY(GB_GET_PLAYER_OFFICE_NAME_AS_A_STRING(sOfficeOrganizationName.pOwnerID #IF IS_DEBUG_BUILD , TRUE #ENDIF))
								ELSE
									sOfficeOrganizationName.iGangTempHash = GET_HASH_KEY(GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(sOfficeOrganizationName.pOwnerID #IF IS_DEBUG_BUILD , TRUE #ENDIF))
								ENDIF
							ELSE
								IF NOT IS_STRING_NULL_OR_EMPTY(GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(sOfficeOrganizationName.pOwnerID #IF IS_DEBUG_BUILD , TRUE #ENDIF))
									sOfficeOrganizationName.iGangTempHash = GET_HASH_KEY(GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(sOfficeOrganizationName.pOwnerID #IF IS_DEBUG_BUILD , TRUE #ENDIF))
								ELSE
									sOfficeOrganizationName.iGangTempHash = GET_HASH_KEY(GB_GET_PLAYER_OFFICE_NAME_AS_A_STRING(sOfficeOrganizationName.pOwnerID #IF IS_DEBUG_BUILD , TRUE #ENDIF))
								ENDIF
							ENDIF
						ENDIF
						// Name Updates
						IF (sOfficeOrganizationName.iGangTempHash != sOfficeOrganizationName.iGangNameHash)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - iGangTempHash: ", sOfficeOrganizationName.iGangTempHash)
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - iGangNameHash: ", sOfficeOrganizationName.iGangNameHash)
							
							sOfficeOrganizationName.eNameStates = OFFICE_NAME_STATE_UPDATE_SCALEFORM
							sOfficeOrganizationName.bUpdateScaleform = TRUE
							SET_BIT(sOfficePA.iPASpeechBs, OFF_PA_SPEECH_ADDGENERAL)
							IF sOfficeOrganizationName.pOwnerID = PLAYER_ID()
								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - My display name has changed (I am office owner)")
								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Owner - (New) Gang Name: ", GB_TO_STRING(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlGangName))
								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Owner - (New) Office Name: ", GB_TO_STRING(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].sMagnateGangBossData.tlVIPOfficeName))
							ELSE
								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Office owners display name has changed")
								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Remote - (New) Gang Name: ", GB_GET_PLAYER_ORGANIZATION_NAME_AS_A_STRING(sOfficeOrganizationName.pOwnerID), TRUE )
								CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Remote - (New) Office Name: ", GB_GET_PLAYER_OFFICE_NAME_AS_A_STRING(sOfficeOrganizationName.pOwnerID , TRUE ))
							ENDIF
							CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Changing State to: OFFICE_NAME_STATE_UPDATE_SCALEFORM")
						ENDIF
					ENDIF
					
				#IF IS_DEBUG_BUILD
				ELSE
					IF NOT IS_STRING_NULL_OR_EMPTY(sOfficeOrganizationName.sName)
						sOfficeOrganizationName.eNameStates = OFFICE_NAME_STATE_UPDATE_SCALEFORM
						sOfficeOrganizationName.bUpdateScaleform = TRUE
					ENDIF
				ENDIF
				#ENDIF
				
				// Decor Updates
				IF (sOfficeOrganizationName.iCurrentOfficeDecor != (GET_FONT_COLOUR_PALETTE_FROM_INTERIOR_DECOR(g_iCurrentPropertyVariation)-1))
					sOfficeOrganizationName.eNameStates = OFFICE_NAME_STATE_UPDATE_SCALEFORM
					sOfficeOrganizationName.bUpdateScaleform = TRUE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Current Office Decor: ", sOfficeOrganizationName.iCurrentOfficeDecor, " New Office Decor: ", g_iCurrentPropertyVariation)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Office Owners Decor has Changed - Changing State to: OFFICE_NAME_STATE_UPDATE_SCALEFORM")
					IF PLAYER_ID() = sOfficeOrganizationName.pOwnerID
						IF NOT IS_BIT_SET(sOfficePA.iPALocalBS,OFF_PA_FONT_CHANGE_HELP)
							SET_BIT(sOfficePA.iPALocalBS,OFF_PA_FONT_CHANGE_HELP)
						ENDIF	
					ENDIF
				ENDIF
				// Font Colour Updates
				IF (sOfficeOrganizationName.iCurrentFontColour != GlobalPlayerBD_FM_3[NATIVE_TO_INT(sOfficeOrganizationName.pOwnerID)].iOfficeNameFontColour)
					sOfficeOrganizationName.eNameStates = OFFICE_NAME_STATE_UPDATE_SCALEFORM
					sOfficeOrganizationName.bUpdateScaleform = TRUE
					SET_BIT(sOfficePA.iPASpeechBs, OFF_PA_SPEECH_ADDGENERAL)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Current Font Colour: ", sOfficeOrganizationName.iCurrentFontColour, " New Font Colour: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(sOfficeOrganizationName.pOwnerID)].iOfficeNameFontColour)
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Office Owners Font Colour has Changed - Changing State to: OFFICE_NAME_STATE_UPDATE_SCALEFORM")
				ENDIF
				// Font Updates
				IF (sOfficeOrganizationName.iCurrentFont != GlobalPlayerBD_FM_3[NATIVE_TO_INT(sOfficeOrganizationName.pOwnerID)].iOfficeNameFont)
					IF NOT IS_LANGUAGE_NON_ROMANIC()
					AND GET_CURRENT_LANGUAGE() != LANGUAGE_POLISH
					AND GET_CURRENT_LANGUAGE() != LANGUAGE_RUSSIAN
						sOfficeOrganizationName.eNameStates = OFFICE_NAME_STATE_UPDATE_SCALEFORM
						sOfficeOrganizationName.bUpdateScaleform = TRUE
						SET_BIT(sOfficePA.iPASpeechBs, OFF_PA_SPEECH_ADDGENERAL)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Current Font: ", sOfficeOrganizationName.iCurrentFont, " New Font: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(sOfficeOrganizationName.pOwnerID)].iOfficeNameFont)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_OFFICE_ORGANIZATION_NAME - OFFICE_NAME_STATE_UPDATE_SCALEFORM - Office Owners Font has Changed - Changing State to: OFFICE_NAME_STATE_UPDATE_SCALEFORM")
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC CLEANUP_OFFICE_ORGANIZATION_NAME(OFFICE_ORGANIZATION_NAME& sOfficeOrganizationName)
	IF HAS_SCALEFORM_MOVIE_LOADED(sOfficeOrganizationName.sNameScaleform)
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sOfficeOrganizationName.sNameScaleform)
	ENDIF
	IF IS_NAMED_RENDERTARGET_REGISTERED("prop_ex_office_text")   
		RELEASE_NAMED_RENDERTARGET("prop_ex_office_text")   
	ENDIF
	sOfficeOrganizationName.iRenderTargetID = -1
	sOfficeOrganizationName.eNameStates = OFFICE_NAME_STATE_LINK_RT
ENDPROC

FUNC VECTOR GET_OFFICE_VIP_DESKTOP_COORDS_FROM_OFFICE_TYPE(INT iCurrentProperty)
	
	SWITCH iCurrentProperty
		CASE PROPERTY_OFFICE_1 		RETURN <<-1556.5187, -575.7021, 108.3919>>
		CASE PROPERTY_OFFICE_2_BASE RETURN <<-1372.2982, -465.1636, 71.9110>>
		CASE PROPERTY_OFFICE_3 		RETURN <<-126.8692, -641.7910, 168.6894>>
		CASE PROPERTY_OFFICE_4 		RETURN <<-79.6132, -801.9753, 243.2548>>
	ENDSWITCH
	
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE: Display desktop textures to office VIP monitor.  Login texture by default, Warehouse map texture if VIP using desktop 
PROC MAINTAIN_OFFICE_DESKTOP_MONITOR(OFFICE_DESKTOP_MONITOR& sOfficeDesktop, INT iCurrentProperty)
	
	IF IS_PROPERTY_OFFICE(iCurrentProperty)
		SWITCH sOfficeDesktop.eDesktopStates
			CASE OFFICE_DESKTOP_STATE_DETECT_PROP
				
				GAMER_HANDLE sPropertyOwnerGH
				GET_GAMER_HANDLE_OF_PROPERTY_OWNER(PLAYER_ID(), sPropertyOwnerGH)
				IF IS_GAMER_HANDLE_VALID(sPropertyOwnerGH)
					IF NETWORK_IS_GAMER_IN_MY_SESSION(sPropertyOwnerGH)
						sOfficeDesktop.pOwnerID = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(sPropertyOwnerGH)
					ENDIF
				ENDIF
				
				REQUEST_STREAMED_TEXTURE_DICT("MPDesktop")
				IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPDesktop")
					sOfficeDesktop.eDesktopStates = OFFICE_DESKTOP_STATE_LINK_RT
				ENDIF
			BREAK
			CASE OFFICE_DESKTOP_STATE_LINK_RT
				IF NOT IS_NAMED_RENDERTARGET_REGISTERED("prop_ex_computer_screen")
					REGISTER_NAMED_RENDERTARGET("prop_ex_computer_screen")
					IF NOT IS_NAMED_RENDERTARGET_LINKED(EX_PROP_MONITOR_01_EX)
						LINK_NAMED_RENDERTARGET(EX_PROP_MONITOR_01_EX)
						IF sOfficeDesktop.iRenderTargetID = -1
							sOfficeDesktop.iRenderTargetID = GET_NAMED_RENDERTARGET_RENDER_ID("prop_ex_computer_screen")
						ENDIF
					ENDIF
				ENDIF
				sOfficeDesktop.eDesktopStates = OFFICE_DESKTOP_STATE_UPDATE_SCALEFORM
			BREAK
			CASE OFFICE_DESKTOP_STATE_UPDATE_SCALEFORM
				SET_TEXT_RENDER_ID(sOfficeDesktop.iRenderTargetID)
				SET_SCRIPT_GFX_ALIGN(UI_ALIGN_IGNORE, UI_ALIGN_IGNORE)
				//SET_SCRIPT_GFX_ALIGN_PARAMS(0.0, 0.0, 0.0, 0.0)
				
				IF sOfficeDesktop.pOwnerID = PLAYER_ID()
					IF (GET_LOCAL_PLAYER_USING_OFFICE_SEATID() = 6) AND (g_bSecuroDelayOfficeChairExit OR IS_BROWSER_OPEN())
						IF NOT IS_INTERACTION_MENU_OPEN() AND g_bBrowserVisible
							IF IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iOfficeOwnerBrowsing, CI_OFFICE_OWNER_NOT_BROWSING)
								CLEAR_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iOfficeOwnerBrowsing, CI_OFFICE_OWNER_NOT_BROWSING)
							ENDIF
							
							IF NOT IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iOfficeOwnerBrowsing, CI_OFFICE_OWNER_BROWSING)
								SET_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iOfficeOwnerBrowsing, CI_OFFICE_OWNER_BROWSING)
							ENDIF
						ENDIF
					ELSE
						IF IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iOfficeOwnerBrowsing, CI_OFFICE_OWNER_BROWSING)
							CLEAR_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iOfficeOwnerBrowsing, CI_OFFICE_OWNER_BROWSING)
						ENDIF
						
						IF NOT IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iOfficeOwnerBrowsing, CI_OFFICE_OWNER_NOT_BROWSING)
							SET_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iOfficeOwnerBrowsing, CI_OFFICE_OWNER_NOT_BROWSING)
						ENDIF
					ENDIF
				ENDIF
				
				IF sOfficeDesktop.pOwnerID = PLAYER_ID()
					DRAW_SPRITE_NAMED_RENDERTARGET("MPDesktop", "DesktopUI_Login", 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
				ELSE
					IF IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(sOfficeDesktop.pOwnerID)].iOfficeOwnerBrowsing, CI_OFFICE_OWNER_BROWSING)
						DRAW_SPRITE_NAMED_RENDERTARGET("MPDesktop", "DesktopUI_Map", 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
						
					ELIF IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(sOfficeDesktop.pOwnerID)].iOfficeOwnerBrowsing, CI_OFFICE_OWNER_NOT_BROWSING)
						DRAW_SPRITE_NAMED_RENDERTARGET("MPDesktop", "DesktopUI_Login", 0.5, 0.5, 1.0, 1.0, 0.0, 255, 255, 255, 255)
					ENDIF
				ENDIF
				
				RESET_SCRIPT_GFX_ALIGN()
				SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC CLEANUP_OFFICE_DESKTOP_MONITOR(OFFICE_DESKTOP_MONITOR& sOfficeDesktop)
	IF IS_NAMED_RENDERTARGET_REGISTERED("prop_ex_computer_screen")   
		RELEASE_NAMED_RENDERTARGET("prop_ex_computer_screen")
	ENDIF
	IF IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(sOfficeDesktop.pOwnerID)].iOfficeOwnerBrowsing, CI_OFFICE_OWNER_NOT_BROWSING)
		CLEAR_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iOfficeOwnerBrowsing, CI_OFFICE_OWNER_NOT_BROWSING)
	ENDIF
	IF IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(sOfficeDesktop.pOwnerID)].iOfficeOwnerBrowsing, CI_OFFICE_OWNER_BROWSING)
		CLEAR_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iOfficeOwnerBrowsing, CI_OFFICE_OWNER_BROWSING)
	ENDIF
	sOfficeDesktop.iRenderTargetID = -1
	sOfficeDesktop.eDesktopStates = OFFICE_DESKTOP_STATE_DETECT_PROP
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPDesktop")
ENDPROC

// HELI TAKEOFF /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Sets who is gonna be the driver of the helicopter
/// PARAMS:
///    dockData - 
///    playerDriver - 
PROC SET_HELI_DOCK_DRIVER_PLAYER_ID(PROPERTY_HELI_DOCK_DATA &dockData, PLAYER_INDEX playerDriver)
	IF playerDriver != INVALID_PLAYER_INDEX()
		dockData.pDriverPlayerIndex = playerDriver
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] SET_HELI_DOCK_DRIVER_PLAYER_ID - pDriverPlayerIndex set to ", GET_PLAYER_NAME(dockData.pDriverPlayerIndex))
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] SET_HELI_DOCK_DRIVER_PLAYER_ID - Player ID is invalid!")
			CASSERTLN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] SET_HELI_DOCK_DRIVER_PLAYER_ID - Player ID is invalid!")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets who is the owner of the office. Other players monitor his BD to see if they initiate the takeoff
/// PARAMS:
///    dockData - 
///    playerOwner - 
PROC SET_HELI_DOCK_OWNER_PLAYER_ID(PROPERTY_HELI_DOCK_DATA &dockData, PLAYER_INDEX playerOwner)
	IF playerOwner != INVALID_PLAYER_INDEX()
		dockData.pOwnerPlayerIndex = playerOwner
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] SET_HELI_DOCK_OWNER_PLAYER_ID - pOwnerPlayerIndex set to ", GET_PLAYER_NAME(dockData.pOwnerPlayerIndex))
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] SET_HELI_DOCK_OWNER_PLAYER_ID - Player ID is invalid!")
			CASSERTLN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] SET_HELI_DOCK_OWNER_PLAYER_ID - Player ID is invalid!")
		#ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    This checks if boss sent a broadcast telling goons to take off with him.
///    net_process_event already does a check for gang memberships and doesn't set g_OfficeHeliDockData.bBossWantsToTakeoff
///    if players are not a member of boss' gang.
/// PARAMS:
///    dockData - 
/// RETURNS:
///    
FUNC BOOL DOES_BOSS_WANT_TO_USE_HELI_DOCK_TAKEOFF(PROPERTY_HELI_DOCK_DATA &dockData)
	
	IF g_OfficeHeliDockData.bBossWantsToTakeoff
		IF GB_IS_PLAYER_MEMBER_OF_A_GANG(PLAYER_ID()) 
		OR g_OfficeHeliDockData.playerOfficeOwner = PLAYER_ID()
		OR g_OfficeHeliDockData.playerDesignatedDriver = PLAYER_ID()
		OR SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
			SET_HELI_DOCK_OWNER_PLAYER_ID(dockData, g_OfficeHeliDockData.playerOfficeOwner)
			
			IF g_OfficeHeliDockData.playerOfficeOwner != INVALID_PLAYER_INDEX()
				SET_HELI_DOCK_DRIVER_PLAYER_ID(dockData, g_OfficeHeliDockData.playerDesignatedDriver)
				
				GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.playerOwner = g_OfficeHeliDockData.playerOfficeOwner
				
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] DOES_OWNER_WANT_TO_USE_HELI_DOCK_TAKEOFF - Yes! ", GET_PLAYER_NAME(g_OfficeHeliDockData.playerOfficeOwner)," selected the driver to be ", GET_PLAYER_NAME(g_OfficeHeliDockData.playerDesignatedDriver))
				#ENDIF
				
				g_OfficeHeliDockData.bBossWantsToTakeoff = FALSE
				g_OfficeHeliDockData.playerDesignatedDriver = INVALID_PLAYER_INDEX()
				
				RETURN TRUE
			ELSE
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] DOES_OWNER_WANT_TO_USE_HELI_DOCK_TAKEOFF - Yes but owner player index is invalid! Did something clean it?")
				#ENDIF
				
				g_OfficeHeliDockData.bBossWantsToTakeoff = FALSE
				g_OfficeHeliDockData.playerDesignatedDriver = INVALID_PLAYER_INDEX()
				
				RETURN FALSE
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 120 = 0
				CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] DOES_OWNER_WANT_TO_USE_HELI_DOCK_TAKEOFF - Waiting for signal to takeoff.")
			ENDIF
		#ENDIF
	ENDIF
	
	IF g_OfficeHeliDockData.bBossWantsToTakeoff
		g_OfficeHeliDockData.bBossWantsToTakeoff = FALSE
		g_OfficeHeliDockData.playerDesignatedDriver = INVALID_PLAYER_INDEX()
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_SPECIAL_VEHICLE_MISSION_TRIGGER_EXIT_VIA_HELICOPTER()
	IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
		IF SVM_GET_LOBBY_EXIT_TYPE() = 0
		AND IS_PROPERTY_OFFICE(GlobalPlayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		AND SVM_FLOW_IS_MISSION_SVM_FLOW_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		
			#IF IS_DEBUG_BUILD
				PRINTLN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] DOES_SPECIAL_VEHICLE_MISSION_TRIGGER_EXIT_VIA_HELICOPTER - Mission is on and selected exit type is Helicopter.")
			#ENDIF
		
			IF IS_BIT_SET(g_CoronaJobLaunchData.iBS_CoronaJobFlags, ciCORONA_JOB_IN_QUICK_RESTART)
			AND (FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0) OR FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1))
			AND NOT (Is_Player_Currently_On_MP_Heist(PLAYER_ID()) OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()))
				// restart is triggering at a checkpoint so heli is not needed	
			ELSE
				RETURN TRUE
				/*
			    IF START_HELICOPTER_TAKEOFF_FROM_PA_MENU(PLAYER_ID(), BV_AP_SUPERVOLITO1)
				 // SET_VEHICLE_COLOURS(tempveh,40,12)
				 //			SET_VEHICLE_EXTRA_COLOURS(tempveh,12,12)
				ENDIF
				*/
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK_TAKEOFF] DOES_SPECIAL_VEHICLE_MISSION_TRIGGER_EXIT_VIA_HELICOPTER - SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW() returns FALSE.")
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// TODO: extend this to always return TRUE if heli stars is gonna happen
FUNC BOOL ARE_WE_STARTING_SVM_IN_A_HELICOPTER()

	IF IS_THIS_A_QUICK_RESTART_JOB()
		IF NOT FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(0)
		AND NOT FMMC_CHECKPOINTS_IS_RETRY_START_POINT_SET(1)
			IF g_OfficeHeliDockData.bUsedSVMHeliStart
				PRINTLN("[HELI_DOCK_TAKEOFF][SVM] ARE_WE_STARTING_SVM_IN_A_HELICOPTER - Returning TRUE because it's a quick restart job and g_OfficeHeliDockData.bUsedSVMHeliStart is TRUE")
				RETURN TRUE
			ENDIF
		ELSE
			// Heli start should never kick in when doing checkpoint
			PRINTLN("[HELI_DOCK_TAKEOFF][SVM] ARE_WE_STARTING_SVM_IN_A_HELICOPTER - Returning FALSE as we are starting checkpoints.")
			RETURN FALSE
		ENDIF
	ENDIF

	IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
		IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
			PLAYER_INDEX pBoss = GB_GET_LOCAL_PLAYER_GANG_BOSS()
			IF pBoss != INVALID_PLAYER_INDEX()
				BOOL bWillDoHeliStart = IS_BIT_SET(globalPlayerBD_FM_3[NATIVE_TO_INT(pBoss)].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_WILL_DO_SVM_HELI_EXIT)
				PRINTLN("[HELI_DOCK_TAKEOFF][SVM] ARE_WE_STARTING_SVM_IN_A_HELICOPTER - IS BS_HELI_DOCK_GLOBAL_PLAYER_BD_WILL_DO_SVM_HELI_EXIT on boss set? ", GET_STRING_FROM_BOOL(bWillDoHeliStart))
				RETURN bWillDoHeliStart
			ELSE
				PRINTLN("[HELI_DOCK_TAKEOFF][SVM] ARE_WE_STARTING_SVM_IN_A_HELICOPTER - Boss is invalid.")
			ENDIF
		ELSE
			PRINTLN("[HELI_DOCK_TAKEOFF][SVM] ARE_WE_STARTING_SVM_IN_A_HELICOPTER - Not a gang member.")
			
			// We are gonna be a member of a gang in this activity session, if we're not it means it hasn't synced yet so we have to check all players for that flag
			INT iPlayer
			REPEAT NUM_NETWORK_REAL_PLAYERS() iPlayer
				IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayer))
					IF IS_BIT_SET(globalPlayerBD_FM_3[iPlayer].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_WILL_DO_SVM_HELI_EXIT)
						PRINTLN("[HELI_DOCK_TAKEOFF][SVM] ARE_WE_STARTING_SVM_IN_A_HELICOPTER - About to return TRUE because player ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(iPlayer)), " has flag BS_HELI_DOCK_GLOBAL_PLAYER_BD_WILL_DO_SVM_HELI_EXIT set")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDREPEAT
			
			PRINTLN("[HELI_DOCK_TAKEOFF][SVM] ARE_WE_STARTING_SVM_IN_A_HELICOPTER - Nobody has BS_HELI_DOCK_GLOBAL_PLAYER_BD_WILL_DO_SVM_HELI_EXIT flag set.")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_SVM_HELI_START_CLEANUP()
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		RETURN FALSE
	ENDIF
	
	IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
		IF g_bMissionOver
		AND NOT g_OfficeHeliDockData.bCleanupSVMHeli
		AND DOES_ENTITY_EXIST(g_OfficeHeliDockData.vehSVMHeli)
			g_OfficeHeliDockData.bCleanupSVMHeli = TRUE
			#IF IS_DEBUG_BUILD
				PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_SVM_HELI_START_CLEANUP - Failed the mission, gonna cleanup that heli as soon as it's empty.")
			#ENDIF
		ENDIF
		
		IF g_OfficeHeliDockData.bCleanupSVMHeli
			IF DOES_ENTITY_EXIST(g_OfficeHeliDockData.vehSVMHeli)
				IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(g_OfficeHeliDockData.vehSVMHeli)
					NETWORK_REQUEST_CONTROL_OF_ENTITY(g_OfficeHeliDockData.vehSVMHeli)
				ENDIF
				
				IF NETWORK_HAS_CONTROL_OF_ENTITY(g_OfficeHeliDockData.vehSVMHeli)
					IF IS_VEHICLE_EMPTY(g_OfficeHeliDockData.vehSVMHeli, TRUE, TRUE)
						SET_ENTITY_AS_MISSION_ENTITY(g_OfficeHeliDockData.vehSVMHeli, TRUE, TRUE)
						DELETE_VEHICLE(g_OfficeHeliDockData.vehSVMHeli)
						g_OfficeHeliDockData.bCleanupSVMHeli = FALSE
						#IF IS_DEBUG_BUILD
							PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_SVM_HELI_START_CLEANUP - Cleanup done.")
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_SVM_HELI_START_CLEANUP - Will cleanup but waiting for it to be empty.")
						#ENDIF
					ENDIF
				ENDIF
			ELSE
				g_OfficeHeliDockData.bCleanupSVMHeli = FALSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_SVM_HELI_START_CLEANUP - We were gonna clean up but it was already cleaned up by someone else.")
				#ENDIF
			ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL MAINTAIN_WARPING_IN_SVM_START_HELI()
	IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
		IF g_bMissionOver
			IF IS_BIT_SET(globalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_ABLE_TO_DO_SVM_START)
				CLEAR_BIT(globalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_ABLE_TO_DO_SVM_START)
				#IF IS_DEBUG_BUILD
					PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_WARPING_IN_SVM_START_HELI - Clearing bit BS_HELI_DOCK_GLOBAL_PLAYER_BD_ABLE_TO_DO_SVM_START")
				#ENDIF
				
				#IF IS_DEBUG_BUILD
				IF g_OfficeHeliDockData.d_bForceDebugSVMSeatsNoDriver
					IF globalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.vsPlayerSVM = VS_DRIVER
						globalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.vsPlayerSVM = VS_ANY_PASSENGER
						PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_WARPING_IN_SVM_START_HELI - Setting new seat to any passanger")
					ENDIF
				ENDIF
				#ENDIF
			ENDIF
		ENDIF

		IF g_OfficeHeliDockData.bGetInSVMHeli
		AND IS_THIS_A_QUICK_RESTART_JOB()
		AND ARE_WE_STARTING_SVM_IN_A_HELICOPTER()
			// We need to check this flag to make sure that they have been warped into the office and now can be warped into the helicopter
			IF IS_BIT_SET(globalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_ABLE_TO_DO_SVM_START)
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				AND NOT IS_ENTITY_DEAD(g_OfficeHeliDockData.vehSVMHeli)
				
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_PreventAutoShuffleToDriversSeat, TRUE)
				
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_OfficeHeliDockData.vehSVMHeli)
						IF NOT HAS_NET_TIMER_STARTED(g_OfficeHeliDockData.timerWarpToSVMHeliOnQuickRestart)
						OR HAS_NET_TIMER_EXPIRED(g_OfficeHeliDockData.timerWarpToSVMHeliOnQuickRestart, 3000)
						
							VEHICLE_SEAT vsToUse = GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.vsPlayerSVM
							IF NOT IS_VEHICLE_SEAT_FREE(g_OfficeHeliDockData.vehSVMHeli, vsToUse)
								vsToUse = VS_ANY_PASSENGER
								#IF IS_DEBUG_BUILD
									PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_WARPING_IN_SVM_START_HELI - Someone taken our seat! using vs_any_passenger")
								#ENDIF
							ENDIF
						
							WarpPlayerIntoCar(PLAYER_PED_ID(), g_OfficeHeliDockData.vehSVMHeli, GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.vsPlayerSVM)
							#IF IS_DEBUG_BUILD
								PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_WARPING_IN_SVM_START_HELI - We're not in vehicle yet, trying again! Seat we're trying: ", _GET_VEHICLE_SEAT_FOR_DEBUG(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.vsPlayerSVM))
							#ENDIF
							REINIT_NET_TIMER(g_OfficeHeliDockData.timerWarpToSVMHeliOnQuickRestart)
						ENDIF
					ELSE
						// OK send the broadcast to everyone so anyone who still hasnt got it or hasnt warped can do so
						BROADCAST_GET_IN_SVM_START_HELI()
						RESET_NET_TIMER(g_OfficeHeliDockData.timerWarpToSVMHeliOnQuickRestart)
						
						g_OfficeHeliDockData.bGetInSVMHeli = FALSE
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_WARPING_IN_SVM_START_HELI - We're inside, sending the broadcast.")
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						IF IS_ENTITY_DEAD(PLAYER_PED_ID())
							PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_SVM_HELI_START_VEHICLE - Player is dead!")
						ENDIF
						
						IF IS_ENTITY_DEAD(g_OfficeHeliDockData.vehSVMHeli)
							PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_SVM_HELI_START_VEHICLE - Heli is dead!")
						ENDIF
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_WARPING_IN_SVM_START_HELI - BS_HELI_DOCK_GLOBAL_PLAYER_BD_ABLE_TO_DO_SVM_START is not set!")
				#ENDIF
			ENDIF
		ENDIF
		RETURN TRUE
	ELSE
		IF IS_BIT_SET(globalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_ABLE_TO_DO_SVM_START)
			CLEAR_BIT(globalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_ABLE_TO_DO_SVM_START)
			#IF IS_DEBUG_BUILD
				PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_WARPING_IN_SVM_START_HELI - Clearing bit BS_HELI_DOCK_GLOBAL_PLAYER_BD_ABLE_TO_DO_SVM_START")
			#ENDIF
		ENDIF
		
		IF IS_BIT_SET(globalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_WE_WERE_THE_BOSS_ON_SVM_START)
			CLEAR_BIT(globalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_WE_WERE_THE_BOSS_ON_SVM_START)
			#IF IS_DEBUG_BUILD
				PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_WARPING_IN_SVM_START_HELI - Clearing bit BS_HELI_DOCK_GLOBAL_PLAYER_BD_WE_WERE_THE_BOSS_ON_SVM_START")
			#ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SVM_HELI_START_VEHICLE(IE_SVM_START_HELI_SERVER_BD &sStartHeli)
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF g_OfficeHeliDockData.iSVMHeliCreationStage != 0
			g_OfficeHeliDockData.iSVMHeliCreationStage = 0
		ENDIF
		EXIT
	ENDIF
	
	IF SVM_FLOW_IS_CURRENT_MISSION_SVM_FLOW()
	
		BOOL bCanCreateHeli = TRUE
		
		IF IS_THIS_A_QUICK_RESTART_JOB()
			// On quick restart everyone is thrown into the apartment and launches their own instance so everyone is host. Only Boss should be able to create the heli in this case.
			bCanCreateHeli = GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
		ENDIF
		
		IF NOT bCanCreateHeli
			EXIT
		ENDIF
		
		IF sStartHeli.bCreateHeli
		
			MODEL_NAMES modelHeli = SUPERVOLITO
			
			// Requesting model
			IF g_OfficeHeliDockData.iSVMHeliCreationStage = 0
				IF DOES_ENTITY_EXIST(g_OfficeHeliDockData.vehSVMHeli)
					#IF IS_DEBUG_BUILD
						PRINTLN("[HELI_DOCK_TAKEOFF][SVM][SERVER] MAINTAIN_SVM_HELI_START_VEHICLE - Waiting for old helicopter to be cleaned up...")
					#ENDIF
					EXIT
				ENDIF
				
				REQUEST_MODEL(modelHeli)
				g_OfficeHeliDockData.iSVMHeliCreationStage = 1
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[HELI_DOCK_TAKEOFF][SVM][SERVER] MAINTAIN_SVM_HELI_START_VEHICLE - Requested model.")
				#ENDIF
			
			// Creating heli
			ELIF g_OfficeHeliDockData.iSVMHeliCreationStage = 1
				IF HAS_MODEL_LOADED(modelHeli)
					VECTOR vSpawnPoint = <<0.0, 0.0, 80.0>>
					
					IF DOES_BUILDING_HAVE_A_HELIPAD(sStartHeli.iBuildingID)
						vSpawnPoint = GET_BUILDING_HELIPAD_POSITION(sStartHeli.iBuildingID)
						#IF IS_DEBUG_BUILD
							PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_SVM_HELI_START_VEHICLE - Spawning heli on the helipad, building id ", sStartHeli.iBuildingID)
						#ENDIF
					ENDIF
					
					IF IS_THIS_A_QUICK_RESTART_JOB()
						// There's no cutscene on quick restart so just create it above the helipad
						vSpawnPoint = vSpawnPoint + <<0.0, 0.0, 10>>
					ENDIF
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_SVM_HELI_START_VEHICLE - Gonna spawn the heli at ", vSpawnPoint)
					#ENDIF
					
					IF CREATE_NET_VEHICLE(sStartHeli.netID, modelHeli, vSpawnPoint, 0.0, FALSE)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(sStartHeli.netID)
							VEHICLE_INDEX vehHeli = NET_TO_VEH(sStartHeli.netID)
							FREEZE_ENTITY_POSITION(vehHeli, TRUE)
							SET_VEH_RADIO_STATION(vehHeli, "OFF")
							SET_VEHICLE_COLOURS(vehHeli, 40, 12)
				 			SET_VEHICLE_EXTRA_COLOURS(vehHeli, 12, 12)
							SET_MODEL_AS_NO_LONGER_NEEDED(modelHeli)
							
							g_OfficeHeliDockData.vehSVMHeli = vehHeli
							
							IF IS_THIS_A_QUICK_RESTART_JOB()
								g_OfficeHeliDockData.bGetInSVMHeli = TRUE
								RESET_NET_TIMER(g_OfficeHeliDockData.timerWarpToSVMHeliOnQuickRestart)
							ENDIF
							
							g_OfficeHeliDockData.iSVMHeliCreationStage = 0
							sStartHeli.bCreateHeli = FALSE
							
							#IF IS_DEBUG_BUILD
								PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_SVM_HELI_START_VEHICLE - Heli was created and player warped.")
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_SVM_HELI_START_VEHICLE - Couldn't create the heli.")
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("[HELI_DOCK_TAKEOFF][SVM] MAINTAIN_SVM_HELI_START_VEHICLE - Waiting for model to load.")
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL DO_WE_TRIGGER_SVM_HELI_START_ON_QUICK_RESTART()
	INT i
	PLAYER_INDEX pIndex
	PLAYER_INDEX pWhoTriggers = INVALID_PLAYER_INDEX()
	PLAYER_INDEX pFirstOKPlayer = INVALID_PLAYER_INDEX()
	
	REPEAT NUM_NETWORK_REAL_PLAYERS() i
		pIndex = INT_TO_PLAYERINDEX(i)
		IF IS_NET_PLAYER_OK(pIndex, FALSE)
		AND NOT IS_PLAYER_SCTV(pIndex)
			IF pFirstOKPlayer = INVALID_PLAYER_INDEX()
				pFirstOKPlayer = pIndex
				#IF IS_DEBUG_BUILD
					PRINTLN("[HELI_DOCK_TAKEOFF][SVM] DO_WE_TRIGGER_SVM_HELI_START_ON_QUICK_RESTART - First OK player is ", GET_PLAYER_NAME(pFirstOKPlayer))
				#ENDIF
			ENDIF
		
			IF IS_BIT_SET(GlobalPlayerBD_FM_3[i].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_WE_WERE_THE_BOSS_ON_SVM_START)
				IF pWhoTriggers = INVALID_PLAYER_INDEX()
					pWhoTriggers = pIndex
					#IF IS_DEBUG_BUILD
						PRINTLN("[HELI_DOCK_TAKEOFF][SVM] DO_WE_TRIGGER_SVM_HELI_START_ON_QUICK_RESTART - Player who triggers ", GET_PLAYER_NAME(pWhoTriggers))
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
			
	IF pWhoTriggers != INVALID_PLAYER_INDEX()
		#IF IS_DEBUG_BUILD
			IF pWhoTriggers = PLAYER_ID()
				PRINTLN("[HELI_DOCK_TAKEOFF][SVM] DO_WE_TRIGGER_SVM_HELI_START_ON_QUICK_RESTART - We got player who triggers, it's us.")
			ELSE
				PRINTLN("[HELI_DOCK_TAKEOFF][SVM] DO_WE_TRIGGER_SVM_HELI_START_ON_QUICK_RESTART - We got player who triggers, it's not us.")
			ENDIF
		#ENDIF
		
		RETURN pWhoTriggers = PLAYER_ID()
	ELSE
		#IF IS_DEBUG_BUILD
			IF pFirstOKPlayer = PLAYER_ID()
				PRINTLN("[HELI_DOCK_TAKEOFF][SVM] DO_WE_TRIGGER_SVM_HELI_START_ON_QUICK_RESTART - Original player is gone, we are first ok so we trigger.")
			ELSE
				PRINTLN("[HELI_DOCK_TAKEOFF][SVM] DO_WE_TRIGGER_SVM_HELI_START_ON_QUICK_RESTART - Original player is gone, first ok player triggers.")
			ENDIF
		#ENDIF
		
		RETURN pFirstOKPlayer = PLAYER_ID()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[HELI_DOCK_TAKEOFF][SVM] DO_WE_TRIGGER_SVM_HELI_START_ON_QUICK_RESTART - It's impossible to get here!")
	#ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DO_HELI_DOCK_TAKEOFF_CUTSCENE_AND_WARP(PROPERTY_HELI_DOCK_DATA &dockData)
	
	IF IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_IDLE)
	OR IS_HELI_DOCK_STATE(dockData, HELI_DOCK_OWNER_IS_CREATING_HELI)
		START_HELI_DOCK_TAKEOFF(dockData)
		
	ELIF IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_WAIT_FOR_HELI_CREATION)
		RUN_HELI_DOCK_STATE_WAIT_FOR_HELI_CREATION(dockData)
		
		IF IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_IDLE)
		AND g_OfficeHeliDockData.bTakeoffWasAborted
			RETURN TRUE
		ENDIF
		
	ELIF IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_LOADING_TAKEOFF_CUTSCENE)
		RUN_HELI_DOCK_STATE_LOADING_TAKEOFF_CUTSCENE(dockData)
		
	ELIF IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_WAITING_FOR_PEDS_TO_SETTLE_IN_TAKEOFF_CUTSCENE)
		RUN_HELI_DOCK_STATE_WAITING_FOR_PEDS_TO_SETTLE_IN_TAKEOFF_CUTSCENE(dockData)
		
	ELIF IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_PLAYING_TAKEOFF_CUTSCENE)
		RUN_HELI_DOCK_STATE_PLAYING_TAKEOFF_CUTSCENE(dockData)
		
	ELIF IS_HELI_DOCK_STATE(dockData, HELI_DOCK_STATE_FINISHING_TAKEOFF_CUTSCENE)
		FINISH_HELI_DOCK_TAKEOFF(dockData)
		RETURN TRUE
	
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// HELI DOCK ENTRY IN FREEMODE
PROC MAINTAIN_HELI_DOCK_CLOSEST_BUILDING()
	FLOAT fBuildingDist
	
	IF g_ExteriorHeliDockData.iBuildingID > -1
	AND g_ExteriorHeliDockData.eState != HELI_DOCK_STATE_LOADING_LANDING_CUTSCENE
		BOOL bMoveToInactive = FALSE
		IF IS_ENTITY_DEAD(PLAYER_PED_ID())
			bMoveToInactive = TRUE
		ELSE
			VECTOR vHelipadPos = _PRIVATE_GET_BUILDING_HELIPAD_POSITION(g_OfficeHeliDockData.iOfficeBuildingSearchStagger, g_ExteriorHeliDockData.iBuildingID)
			VECTOR vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
			
			// Ignore Z for this check
			vHelipadPos.Z = 0.0
			vPlayerPos.Z = 0.0
			fBuildingDist = VDIST(vHelipadPos, vPlayerPos)
			
			IF fBuildingDist >= 100.0
				bMoveToInactive = TRUE
			ENDIF
		ENDIF
	
		IF bMoveToInactive
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] MAINTAIN_HELI_DOCK_CLOSEST_BUILDING - We moved away from the building, going back to inactive", " (dist: ", fBuildingDist, ")")
			#ENDIF
			
			IF IS_LOCAL_PLAYER_DOING_HELI_DOCK_CUTSCENE()
				SET_LOCAL_PLAYER_IS_DOING_HELI_DOCK_CUTSCENE(FALSE)
				CLEANUP_HELI_DOCK_CUTSCENE(g_ExteriorHeliDockData)
			ENDIF
			
			g_ExteriorHeliDockData.iBS = 0
			g_ExteriorHeliDockData.iInvolvedPlayersCount = 0
			g_ExteriorHeliDockData.iBuildingID = -1
			g_ExteriorHeliDockData.eState = HELI_DOCK_STATE_IDLE
			
		ENDIF
	ELSE
		INT iBuildingToCheck
		VECTOR vHelipadPos
		VECTOR vPlayerPos
		
		IF IS_HELI_DOCK_A_SIMPLE_INTERIOR_DOCK(g_OfficeHeliDockData.iOfficeBuildingSearchStagger)
			INT iArrayIndex 	= (g_OfficeHeliDockData.iOfficeBuildingSearchStagger - CI_MAX_NUM_OFFICES)
			iBuildingToCheck 	= ENUM_TO_INT(g_SimpleInteriorData.eInteriorsWithHelipad[iArrayIndex])
			vHelipadPos 		= _PRIVATE_GET_BUILDING_HELIPAD_POSITION(g_OfficeHeliDockData.iOfficeBuildingSearchStagger, iBuildingToCheck)
			vPlayerPos 			= GET_PLAYER_COORDS(PLAYER_ID())
		ELSE
			iBuildingToCheck 	= MP_PROPERTY_OFFICE_BUILDING_1 + g_OfficeHeliDockData.iOfficeBuildingSearchStagger
			vHelipadPos 		= _PRIVATE_GET_BUILDING_HELIPAD_POSITION(g_OfficeHeliDockData.iOfficeBuildingSearchStagger, iBuildingToCheck)
			vPlayerPos 			= GET_PLAYER_COORDS(PLAYER_ID())
		ENDIF
		
		// Ignore Z for this check
		vHelipadPos.Z = 0.0
		vPlayerPos.Z = 0.0
		fBuildingDist = VDIST2(vHelipadPos, vPlayerPos)
		
		IF fBuildingDist < 10000.0
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] MAINTAIN_HELI_DOCK_CLOSEST_BUILDING - We're close to building: ", iBuildingToCheck, " (dist: ", fBuildingDist, ")")
			#ENDIF
			
			g_ExteriorHeliDockData.iBS = 0
			g_ExteriorHeliDockData.iInvolvedPlayersCount = 0
			g_ExteriorHeliDockData.iBuildingID = iBuildingToCheck
			g_ExteriorHeliDockData.vDockPosition = _PRIVATE_GET_BUILDING_HELIPAD_POSITION(g_OfficeHeliDockData.iOfficeBuildingSearchStagger, iBuildingToCheck)
			
			EXIT
		ENDIF
		
		g_OfficeHeliDockData.iOfficeBuildingSearchStagger = (g_OfficeHeliDockData.iOfficeBuildingSearchStagger + 1) % (CI_MAX_NUM_OFFICES + NUM_SIMPLE_INTERIORS_WITH_HELIPAD)
	ENDIF

ENDPROC

/// PURPOSE:
///    Recreate helicopter that was cleaned up after the end of heli->office transition
/// PARAMS:
///    dockData - 
PROC MAINTAIN_HELI_DOCK_HELI_RECREATION()

	BOOL bCanContinue = TRUE
	
	// Because ambient heli will be created right here we need to take care of requesting model,
	// Let's wait before we ask for spawn point until the model is loaded
	IF g_OfficeHeliDockData.bDoRecreateHeli AND g_OfficeHeliDockData.bRecreateAmbientHeliAfterLeavingOffice
		REQUEST_MODEL(g_OfficeHeliDockData.heliSetup.VehicleSetup.eModel)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] MAINTAIN_HELI_DOCK_HELI_RECREATION - Requesting ambient heli model...")
		#ENDIF
		IF NOT HAS_MODEL_LOADED(g_OfficeHeliDockData.heliSetup.VehicleSetup.eModel)
			bCanContinue = FALSE
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] MAINTAIN_HELI_DOCK_HELI_RECREATION - Waiting for ambient heli model to load...")
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] MAINTAIN_HELI_DOCK_HELI_RECREATION - Ambient model loaded!")
			#ENDIF
		ENDIF
	ENDIF

	IF g_OfficeHeliDockData.bDoRecreateHeli
	AND bCanContinue
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] MAINTAIN_HELI_DOCK_HELI_RECREATION - g_OfficeHeliDockData.bDoRecreateHeli = TRUE")
		#ENDIF
		
		VEHICLE_SPAWN_LOCATION_PARAMS Params
		Params.fMaxDistance = 99999.9
		Params.bCheckEntityArea = TRUE
		Params.bCheckOwnVisibility = FALSE
		Params.bUseExactCoordsIfPossible = TRUE
		
		BOOL bCanSpawn = FALSE
		VECTOR vSpawnCoords
		FLOAT fSpawnHeading
		
		IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(g_OfficeHeliDockData.vRecreateSpawnPoint, <<0.0, 0.0, 0.0>>,  g_OfficeHeliDockData.heliSetup.VehicleSetup.eModel, FALSE, vSpawnCoords, fSpawnHeading, Params)
			CLEAR_CUSTOM_VEHICLE_NODES()
			
			IF VDIST(vSpawnCoords, g_OfficeHeliDockData.vRecreateSpawnPoint) > 3.0
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] MAINTAIN_HELI_DOCK_HELI_RECREATION - Got some other point which means the helipad is taken, no spawning.")
				#ENDIF
				
				IF g_OfficeHeliDockData.bRecreateAmbientHeliAfterLeavingOffice = FALSE
					SET_MODEL_AS_NO_LONGER_NEEDED(g_OfficeHeliDockData.heliSetup.VehicleSetup.eModel)
				ENDIF
				
				g_OfficeHeliDockData.bRecreateBossLimoAfterLeavingOffice = FALSE
				g_OfficeHeliDockData.bRecreatePegasusAfterLeavingOffice = FALSE
				g_OfficeHeliDockData.bRecreateAmbientHeliAfterLeavingOffice = FALSE
				g_OfficeHeliDockData.bDoRecreateHeli = FALSE
				EXIT
			ELSE
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] MAINTAIN_HELI_DOCK_HELI_RECREATION - OK, helipad seems to be empty, let's spawn!")
				#ENDIF
				
				bCanSpawn = TRUE
			ENDIF
		ENDIF
		
		IF bCanSpawn
			// PEGASUS
			IF g_OfficeHeliDockData.bRecreatePegasusAfterLeavingOffice
				// Use am_vehicle_spawn mechanism for restoring destroyed pegasus vehicles
				SET_BIT(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_NEED_PEGASUS_SPAWN)
				CLEAR_BIT(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_PEGASUS_SPAWN_SUCCESS)
				CLEAR_BIT(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_PEGASUS_SPAWN_FAIL)
				SET_BIT(g_TransitionSessionNonResetVars.iRestoreLastVehicleBitset, TRAN_SESS_NON_RESET_LAST_VEHICLE_CLEAN_WHEN_DONE)
				
				g_TransitionSessionNonResetVars.sLastVehAfterJobData = g_OfficeHeliDockData.heliSetup
				g_TransitionSessionNonResetVars.vLastVehicleBeforeCoronaSavePosition = vSpawnCoords
				g_TransitionSessionNonResetVars.fLastVehicleBeforeCoronaSaveHeading = 0.0
				
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] MAINTAIN_HELI_DOCK_HELI_RECREATION - OK LET'S RECREATE PEGASUS!")
				#ENDIF
				
				g_OfficeHeliDockData.bRecreatePegasusAfterLeavingOffice = FALSE
				g_OfficeHeliDockData.bDoRecreateHeli = FALSE
			
			// BOSS LIMO
			ELIF g_OfficeHeliDockData.bRecreateBossLimoAfterLeavingOffice
				IF GB_GET_LOCAL_PLAYER_GANG_BOSS() != INVALID_PLAYER_INDEX()
					GB_REQUEST_CREATE_GANG_BOSS_LIMO(g_OfficeHeliDockData.heliSetup.VehicleSetup.eModel)
					
					// Create boss buzzard on top of helipad only if we leave on foot and the space is empty
					MPGlobalsAmbience.sMagnateGangBossData.bSpawnBossLimoAtExactCoords = TRUE
					MPGlobalsAmbience.sMagnateGangBossData.vBossLimoExactSpawnPoint = vSpawnCoords
					MPGlobalsAmbience.sMagnateGangBossData.fBossLimoExactSpawnHeading = 0.0
					
					#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] MAINTAIN_HELI_DOCK_HELI_RECREATION - OK LET'S RECREATE BOSS LIMO")
					#ENDIF
				ENDIF
				
				g_OfficeHeliDockData.bRecreateBossLimoAfterLeavingOffice = FALSE
				g_OfficeHeliDockData.bDoRecreateHeli = FALSE
			// AMBIENT HELI
			ELIF g_OfficeHeliDockData.bRecreateAmbientHeliAfterLeavingOffice
				
				CLEANUP_NET_ID(g_OfficeHeliDockData.netRecreatedAmbientHeli)
			
				IF HAS_MODEL_LOADED(g_OfficeHeliDockData.heliSetup.VehicleSetup.eModel)
					IF CREATE_NET_VEHICLE(g_OfficeHeliDockData.netRecreatedAmbientHeli, g_OfficeHeliDockData.heliSetup.VehicleSetup.eModel, vSpawnCoords, 0.0, FALSE, TRUE, FALSE)
						#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK] MAINTAIN_HELI_DOCK_HELI_RECREATION - OK, ambient heli recreated!")
						#ENDIF
						SET_VEHICLE_SETUP_MP(NET_TO_VEH(g_OfficeHeliDockData.netRecreatedAmbientHeli), g_OfficeHeliDockData.heliSetup)
						
						IF NETWORK_HAS_CONTROL_OF_ENTITY(NET_TO_VEH(g_OfficeHeliDockData.netRecreatedAmbientHeli))
							SET_VEHICLE_DOORS_LOCKED(NET_TO_VEH(g_OfficeHeliDockData.netRecreatedAmbientHeli), VEHICLELOCK_UNLOCKED)
						ENDIF
					ENDIF
					SET_MODEL_AS_NO_LONGER_NEEDED(g_OfficeHeliDockData.heliSetup.VehicleSetup.eModel)
				ENDIF
				
				g_OfficeHeliDockData.bRecreateAmbientHeliAfterLeavingOffice = FALSE
				g_OfficeHeliDockData.bDoRecreateHeli = FALSE
			ENDIF
		ENDIF
	ENDIF
	

	
ENDPROC

/// PURPOSE:
///    Looks out for owner wanting to quick warp to office from PA delivery service
PROC MAINTAIN_HELI_TAXI_QUICK_TRAVEL()
	BOOL bAbort = FALSE
	
	IF g_OfficeHeliDockData.bOwnerWantsToQuickWarpToOffice
	OR g_SimpleInteriorData.bOwnerWantsToQuickTravel
	
		CLEAR_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_READY_FOR_QUICK_TRAVEL)
	
		// These abort conditions are valid only for offices
		IF g_OfficeHeliDockData.bOwnerWantsToQuickWarpToOffice
			IF NOT IS_NET_PLAYER_OK(g_OfficeHeliDockData.playerOfficeOwner)
				bAbort = TRUE
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK][WARP_TO_OFFICE] MAINTAIN_HELI_DOCK_QUICK_TRAVEL - bAbort = TRUE - Owner is not OK")
				#ENDIF
			ENDIF
			
			PED_INDEX pedOwner = GET_PLAYER_PED(g_OfficeHeliDockData.playerOfficeOwner)
			
			IF IS_ENTITY_DEAD(pedOwner) OR NOT IS_PED_IN_ANY_HELI(pedOwner)
				bAbort = TRUE
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK][WARP_TO_OFFICE] MAINTAIN_HELI_DOCK_QUICK_TRAVEL - bAbort = TRUE - Owner is not in heli")
				#ENDIF
			ENDIF
			
			IF NOT ARE_PEDS_IN_THE_SAME_VEHICLE(pedOwner, PLAYER_PED_ID())
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK][WARP_TO_OFFICE] MAINTAIN_HELI_DOCK_QUICK_TRAVEL - bAbort = TRUE - We're not in the same heli as owner")
				#ENDIF
				bAbort = TRUE
			ENDIF
		ENDIF
		
		IF NOT bAbort
			// switch player control off
			IF (g_HeliTaxiQuickTravelData.iQuickWarpState = 0)		
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				g_HeliTaxiQuickTravelData.tQuickWarpTime = GET_NETWORK_TIME()
				g_HeliTaxiQuickTravelData.iQuickWarpState++			
			ENDIF
			
			// wait a couple of seconds before starting fade out
			IF (g_HeliTaxiQuickTravelData.iQuickWarpState = 1)	
				IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_HeliTaxiQuickTravelData.tQuickWarpTime)) > 6000
					DO_SCREEN_FADE_OUT(CI_HELI_DOCK_FADE_OUT_TIME)	
					g_HeliTaxiQuickTravelData.iQuickWarpState++
				ENDIF
			ENDIF
			
			// wait for screen to be fully faded out
			IF (g_HeliTaxiQuickTravelData.iQuickWarpState = 2)	
				IF IS_SCREEN_FADED_OUT()
					SET_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_READY_FOR_QUICK_TRAVEL)
					g_HeliTaxiQuickTravelData.bDoingQuickTravelWarp = FALSE
					
					g_OfficeHeliDockData.bOwnerWantsToQuickWarpToOffice = FALSE
					
					IF g_SimpleInteriorData.bOwnerWantsToQuickTravel
						#IF IS_DEBUG_BUILD
							CDEBUG1LN(DEBUG_TAXI_SERVICE, "[QUICK_TRAVEL] MAINTAIN_HELI_TAXI_QUICK_TRAVEL - We are gonna autowarp to the warehouse ID: ", g_SimpleInteriorData.iWarehouseToWarpToOnQuickTravel)
						#ENDIF
						
						SIMPLE_INTERIORS eSimpleInteriorID = INT_TO_ENUM(SIMPLE_INTERIORS, g_SimpleInteriorData.iWarehouseToWarpToOnQuickTravel)
						
						IF GET_SIMPLE_INTERIOR_TYPE(eSimpleInteriorID) = SIMPLE_INTERIOR_TYPE_IE_GARAGE
						OR GET_SIMPLE_INTERIOR_TYPE(eSimpleInteriorID) = SIMPLE_INTERIOR_TYPE_WAREHOUSE
							AUTOWARP_LOCAL_PLAYER_INSIDE_SIMPLE_INTERIOR(eSimpleInteriorID, FALSE, FALSE, <<0.0, 0.0, 0.0>>, DEFAULT, TRUE)
						ELSE
							bAbort = TRUE
						ENDIF
							
					ENDIF
					
					g_SimpleInteriorData.bOwnerWantsToQuickTravel = FALSE
				ENDIF
			ENDIF
			
			// make sure cinematic camera is running this whole time
			IF (g_HeliTaxiQuickTravelData.iQuickWarpState > 0)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				FORCE_CINEMATIC_RENDERING_THIS_UPDATE(TRUE)
			ENDIF
		ENDIF
		
	ENDIF
	
	// This test should only run for when warping to office
	IF IS_BIT_SET(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_READY_FOR_QUICK_TRAVEL)
	AND g_OfficeHeliDockData.bOwnerWantsToLand
		IF NOT IS_NET_PLAYER_OK(g_OfficeHeliDockData.playerOfficeOwner)
			bAbort = TRUE
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK][WARP_TO_OFFICE] MAINTAIN_HELI_DOCK_QUICK_TRAVEL - bAbort = TRUE - Owner is not OK (2)")
			#ENDIF
			
			CLEAR_BIT(GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].officeHeliDockBD.iBS, BS_HELI_DOCK_GLOBAL_PLAYER_BD_READY_FOR_QUICK_TRAVEL)
		ENDIF
	ENDIF
	
	IF bAbort
		// Heli taxi script was interruped
		g_OfficeHeliDockData.bOwnerWantsToQuickWarpToOffice = FALSE
		g_HeliTaxiQuickTravelData.iQuickWarpState = 0
		g_HeliTaxiQuickTravelData.bDoingQuickTravelWarp = FALSE
		g_SimpleInteriorData.bOwnerWantsToQuickTravel = FALSE
		
		// Only do full abort if landing script hasnt yet taken over
		IF NOT IS_LOCAL_PLAYER_DOING_HELI_DOCK_CUTSCENE()
			IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(CI_HELI_DOCK_FADE_OUT_TIME)
			ENDIF
			
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
			// Restore some options am heli taxi might have set before
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF NOT IS_ENTITY_DEAD(playerVeh)
				AND NETWORK_HAS_CONTROL_OF_ENTITY(playerVeh)
					FREEZE_ENTITY_POSITION(playerVeh, FALSE)
					SET_ENTITY_COLLISION(playerVeh, TRUE)
					SET_VEHICLE_DOORS_LOCKED(playerVeh, VEHICLELOCK_UNLOCKED)
					//NETWORK_FADE_IN_ENTITY(playerVeh, TRUE, FALSE)
					APPLY_FORCE_TO_ENTITY(playerVeh, APPLY_TYPE_IMPULSE, <<0.0, 0.0, -0.1>>, <<0.0, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
					
					#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK][WARP_TO_OFFICE] MAINTAIN_HELI_DOCK_QUICK_TRAVEL - we have heli control, can unlock it etc.")
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK][WARP_TO_OFFICE] MAINTAIN_HELI_DOCK_QUICK_TRAVEL - we don't have control, will try to unlock later.")
					#ENDIF
					
					g_OfficeHeliDockData.bTryToUnlockHeliWhenGotControl = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK][WARP_TO_OFFICE] MAINTAIN_HELI_DOCK_QUICK_TRAVEL - am_heli_taxi was interruped, canceling warp")
		#ENDIF
		
		EXIT
	ENDIF
	
	IF g_OfficeHeliDockData.bTryToUnlockHeliWhenGotControl
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX playerVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF NOT IS_ENTITY_DEAD(playerVeh)
			AND ENUM_TO_INT(GET_VEHICLE_DOOR_LOCK_STATUS(playerVeh)) > ENUM_TO_INT(VEHICLELOCK_UNLOCKED)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(playerVeh)
					
					SET_VEHICLE_DOORS_LOCKED(playerVeh, VEHICLELOCK_UNLOCKED)
					FREEZE_ENTITY_POSITION(playerVeh, FALSE)
					SET_ENTITY_COLLISION(playerVeh, TRUE)
					SET_ENTITY_VISIBLE(playerVeh, TRUE)
					
					#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK][WARP_TO_OFFICE] MAINTAIN_HELI_DOCK_QUICK_TRAVEL - Unlocked heli taxi.")
					#ENDIF
					g_OfficeHeliDockData.bTryToUnlockHeliWhenGotControl = FALSE
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK][WARP_TO_OFFICE] MAINTAIN_HELI_DOCK_QUICK_TRAVEL - Veh is dead or someone else unlocked it.")
				#ENDIF
				g_OfficeHeliDockData.bTryToUnlockHeliWhenGotControl = FALSE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK][WARP_TO_OFFICE] MAINTAIN_HELI_DOCK_QUICK_TRAVEL - We are dead or not in veh.")
			#ENDIF
			g_OfficeHeliDockData.bTryToUnlockHeliWhenGotControl = FALSE
		ENDIF
	ENDIF
	
ENDPROC

/*
/// PURPOSE:
///    Looks for coords to warp heli in case of abort/canceling. Has to be exported to freemode
///    as running these procs in exterior script causes stack overflow
PROC MAINTAIN_HELI_DOCK_CANCEL_WARP_COORDS()

	IF g_OfficeHeliDockData.iLandingCancelingStage = CI_HELI_DOCK_LANDING_CANCELING_STAGE_FIND_COORD_FOR_WARP_PED
	
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK][WARP_TO_OFFICE] MAINTAIN_HELI_DOCK_CANCEL_WARP_COORDS - doing ped warp")
		#ENDIF
	
		IF WARP_TO_SPAWN_LOCATION(SPAWN_LOCATION_AT_SPECIFIC_COORDS, FALSE, FALSE)
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK][WARP_TO_OFFICE] MAINTAIN_HELI_DOCK_CANCEL_WARP_COORDS - ped warp done!")
			#ENDIF
			g_OfficeHeliDockData.iLandingCancelingStage = CI_HELI_DOCK_LANDING_CANCELING_STAGE_PLAYER_WARP_DONE
			CLEAR_SPECIFIC_SPAWN_LOCATION()
		ENDIF
	
	ELIF g_OfficeHeliDockData.iLandingCancelingStage = CI_HELI_DOCK_LANDING_CANCELING_STAGE_FIND_COORD_FOR_WARP_HELI
	
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK][WARP_TO_OFFICE] MAINTAIN_HELI_DOCK_CANCEL_WARP_COORDS - looking for point to spawn heli")
		#ENDIF
	
		VEHICLE_SPAWN_LOCATION_PARAMS Params
		Params.fMaxDistance = 99999.9
		Params.bCheckEntityArea = TRUE
		Params.bCheckOwnVisibility = FALSE
		Params.bUseExactCoordsIfPossible = TRUE
		
		VECTOR vSpawnCoords
		FLOAT fSpawnHeading
		
		IF HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS(g_OfficeHeliDockData.vCancelWarpCoord, <<0.0, 0.0, 0.0>>,  g_OfficeHeliDockData.heliSetup.VehicleSetup.eModel, FALSE, vSpawnCoords, fSpawnHeading, Params)
			CLEAR_CUSTOM_VEHICLE_NODES()
			
			g_OfficeHeliDockData.iLandingCancelingStage = CI_HELI_DOCK_LANDING_CANCELING_STAGE_DO_HELI_WARP
			g_OfficeHeliDockData.vCancelWarpCoord = vSpawnCoords
			g_OfficeHeliDockData.fCancelWarpHeading = fSpawnHeading
			
			#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_SPAWN_ACTIVITIES, "[HELI_DOCK][WARP_TO_OFFICE] MAINTAIN_HELI_DOCK_CANCEL_WARP_COORDS - got heli spawn point")
			#ENDIF
		ENDIF
	ENDIF

ENDPROC
*/

PROC MAINTAIN_HELI_DOCK()
	MAINTAIN_PLAYER_ALLOWED_TO_DO_HELI_DOCK_BROADCAST()
	MAINTAIN_HELI_DOCK_CLOSEST_BUILDING()
	MAINTAIN_HELI_TAXI_QUICK_TRAVEL()
	MAINTAIN_HELI_DOCK_HELI_RECREATION()
	
	//MAINTAIN_CREATION_OF_SVM_HELICOPTER()
	//MAINTAIN_HELI_DOCK_CANCEL_WARP_COORDS()
ENDPROC

PROC RUN_HELP_TEXT_FOR_DISABLED_LIVING_QUATERS(INT iCurrentProperty)
	MP_PROP_OFFSET_STRUCT OfficeDoorOne_v1
	MP_PROP_OFFSET_STRUCT OfficeDoorTwo_v1
	MP_PROP_OFFSET_STRUCT OfficeDoorOne_v2
	MP_PROP_OFFSET_STRUCT OfficeDoorTwo_v2
	
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_PERSONAL_QUATERS_DOOR_1_L1, OfficeDoorOne_v1, PROPERTY_OFFICE_2_BASE)
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_PERSONAL_QUATERS_DOOR_2_L1, OfficeDoorTwo_v1, PROPERTY_OFFICE_2_BASE)
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_PERSONAL_QUATERS_DOOR_1_L2, OfficeDoorOne_v2, PROPERTY_OFFICE_2_BASE)
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_PERSONAL_QUATERS_DOOR_2_L2, OfficeDoorTwo_v2, PROPERTY_OFFICE_2_BASE)
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 0.2
		AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), OfficeDoorOne_v1.vLoc, OfficeDoorOne_v2.vLoc, 1.6)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), OfficeDoorTwo_v1.vLoc, OfficeDoorTwo_v2.vLoc, 1.6))
			PRINT_HELP_FOREVER("OFF_LIV_Q_DIS")
		ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OFF_LIV_Q_DIS")
			CLEAR_HELP(TRUE)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL OFFICE_SHOULD_SECURO_AWARD_BE_ADDED_TO_DESK(PLAYER_INDEX piOwner)
	IF NOT g_sMPTunables.bEXEC_OFFICE_DESK_TROPHY_DISABLE
		IF GET_REMOTE_PLAYER_LIFETIME_BUY_MISSIONS_COMPLETED(piOwner) >= g_sMPTunables.iEXEC_OFFICE_DESK_TROPHY_THRESHOLD_PLASTIC
		#IF IS_DEBUG_BUILD
		OR (g_iDebugBuyMissionsComplete >= g_sMPTunables.iEXEC_OFFICE_DESK_TROPHY_THRESHOLD_PLASTIC
		AND piOwner = PLAYER_ID())
		#ENDIF
			RETURN TRUE	
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC MODEL_NAMES GET_SECURO_AWARD_MODEL(PLAYER_INDEX piOwner)
	INT iCount = GET_REMOTE_PLAYER_LIFETIME_BUY_MISSIONS_COMPLETED(piOwner)
	
	#IF IS_DEBUG_BUILD
		IF g_iDebugBuyMissionsComplete > 0
		AND piOwner = PLAYER_ID()
			iCount = g_iDebugBuyMissionsComplete
		ENDIF
	#ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_SECURO_AWARD_MODEL Returning trophy for count: ", iCount, " for player: ", GET_PLAYER_NAME(piOwner))
	
	IF iCount >= g_sMPTunables.iEXEC_OFFICE_DESK_TROPHY_THRESHOLD_GOLD			RETURN EX_PROP_EXEC_AWARD_GOLD
	ELIF iCount >= g_sMPTunables.iEXEC_OFFICE_DESK_TROPHY_THRESHOLD_SILVER		RETURN EX_PROP_EXEC_AWARD_SILVER
	ELIF iCount >= g_sMPTunables.iEXEC_OFFICE_DESK_TROPHY_THRESHOLD_BRONZE		RETURN EX_PROP_EXEC_AWARD_BRONZE
	ELIF iCount >= g_sMPTunables.iEXEC_OFFICE_DESK_TROPHY_THRESHOLD_GLASS		RETURN EX_PROP_EXEC_AWARD_DIAMOND
	ELIF iCount >= g_sMPTunables.iEXEC_OFFICE_DESK_TROPHY_THRESHOLD_PLASTIC		RETURN EX_PROP_EXEC_AWARD_PLASTIC
	ENDIF
	
	RETURN EX_PROP_EXEC_AWARD_PLASTIC
ENDFUNC

FUNC BOOL CREATE_SECURO_AWARD_MODEL(OBJECT_INDEX &ob_SECURO_AWARD, INT iCurrentProperty, PLAYER_INDEX piOwner)
	
	IF NOT DOES_ENTITY_EXIST(ob_SECURO_AWARD)
	AND IS_PROPERTY_OFFICE(iCurrentProperty)
	AND OFFICE_SHOULD_SECURO_AWARD_BE_ADDED_TO_DESK(piOwner)	
		MODEL_NAMES model = GET_SECURO_AWARD_MODEL(piOwner)
		
		IF REQUEST_LOAD_MODEL(model)
			RESERVE_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS() + 1)
			MP_PROP_OFFSET_STRUCT officeModelLoc
			GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_AWARD, officeModelLoc, PROPERTY_OFFICE_2_BASE)
			ob_SECURO_AWARD = CREATE_OBJECT_NO_OFFSET(model, officeModelLoc.vLoc, FALSE, FALSE)
			
			SET_ENTITY_ROTATION(ob_SECURO_AWARD, officeModelLoc.vRot)				
			SET_MODEL_AS_NO_LONGER_NEEDED(model)
			
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_SECURO_AWARD_MODEL CREATED by player: ", GET_PLAYER_NAME(PLAYER_ID()), " Owner is: ", GET_PLAYER_NAME(piOwner))
			#ENDIF
			RETURN TRUE
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_SECURO_AWARD_MODEL waiting until model loads")
			RETURN FALSE
		ENDIF
	ELSE
		//RETURN TRUE here as we don't want to create the trophy
		RETURN TRUE
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_SECURO_AWARD_MODEL Returning false. Should not get here!")
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Display help text when player don't own gun vault or safe
PROC RUN_HELP_TEXT_FOR_DISABLED_VAULTS(INT iCurrentProperty, BOOL bDisabledGunV, BOOL bDisabledVault)

	IF NOT bDisabledGunV
	AND NOT bDisabledVault
		EXIT
	ENDIF
	
	MP_PROP_OFFSET_STRUCT OfficeVault[2]
	MP_PROP_OFFSET_STRUCT OfficeGunVault[2]
	
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_WEAPON_CRATE_TRIGGER_POS_1, OfficeGunVault[0], PROPERTY_OFFICE_2_BASE)
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_WEAPON_CRATE_TRIGGER_POS_2, OfficeGunVault[1], PROPERTY_OFFICE_2_BASE)
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_VAULT_SAFE_TRIGGER_POS_1, OfficeVault[0], PROPERTY_OFFICE_2_BASE)
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(iCurrentProperty, MP_PROP_ELEMENT_OFFICE_VAULT_SAFE_TRIGGER_POS_2, OfficeVault[1], PROPERTY_OFFICE_2_BASE)
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	//AND IS_PED_STILL(PLAYER_PED_ID())
		IF bDisabledVault
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),OfficeVault[0].vLoc,OfficeVault[1].vLoc,OfficeVault[0].vRot.x)
			PRINT_HELP_FOREVER("OFF_VAULT_DIS")
		ELIF bDisabledGunV
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),OfficeGunVault[0].vLoc,OfficeGunVault[1].vLoc,OfficeGunVault[0].vRot.x)
			PRINT_HELP_FOREVER("OFF_GUN_V_DIS")
		ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OFF_VAULT_DIS")
		OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OFF_GUN_V_DIS")
			CLEAR_HELP(TRUE)
		ENDIF
	ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OFF_VAULT_DIS")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("OFF_GUN_V_DIS")
		CLEAR_HELP(TRUE)
	ENDIF
	
ENDPROC

//FUNC BOOL RUN_STRIPPER_CLEANUP_CHECKS(OFFICE_STRIPPER_SERVER_DATA_STRUCT &office_stripper_server_data, OFFICE_STRIPPER_CLIENT_DATA_STRUCT &stripper_client_data, OFFICE_STRIPPER_LOCAL_DATA_STRUCT &sLocalOfficeStripData, 
//										BOOL bServer, INT& iInteriorStripperServerBits)
//	
//	IF IS_BIT_SET(office_Stripper_Server_Data.bits, ci_ServerCleanup)
//		IF NOT IS_BIT_SET(stripper_client_data.bits, ci_ClientCleanupDone)
//			CLEANUP_OFFICE_STRIPPERS(office_stripper_server_data, sLocalOfficeStripData, TRUE, TRUE, DEFAULT, TRUE, TRUE)
//			stripper_client_data.bits = 0
//			SET_BIT(stripper_client_data.bits, ci_ClientCleanupDone)
//			
//			IF bServer
//				CPRINTLN(debug_dan,"Clearing forced radio bits")
//				CLEAR_BIT(iInteriorStripperServerBits, 3)	//SERVER_STRIPPER_BS_FlagForceMusic
//				CLEAR_BIT(iInteriorStripperServerBits, 4)	//SERVER_STRIPPER_BS_FlagForceVolume
//			ENDIF
//		
//			
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "[OFF_STRIP] ci_ClientCleanupDone")
//		ELSE
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "[OFF_STRIP] ci_ClientCleanupDone already set")
//		ENDIF
//		
//		RETURN TRUE
//	ENDIF
//	
//	IF g_bCleanupOfficeStrippers
//		IF bServer
//			IF NOT IS_BIT_SET(office_Stripper_Server_Data.bits, ci_ServerCleanup)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "[OFF_STRIP] ci_ServerCleanup")
//				SET_BIT(office_Stripper_Server_Data.bits, ci_ServerCleanup)
//			ENDIF
//		ELSE
//			IF NOT IS_BIT_SET(stripper_client_data.bits, ci_ClientRequestCleanup)
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "[OFF_STRIP] ci_ClientRequestCleanup")
//				SET_BIT(stripper_client_data.bits, ci_ClientRequestCleanup)
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	IF IS_LOCAL_PLAYER_DOING_HELI_DOCK_CUTSCENE()
//		IF office_Stripper_Server_Data.piChairOneUser = NATIVE_TO_INT(PLAYER_ID())
//		OR office_Stripper_Server_Data.piChairTwoUser = NATIVE_TO_INT(PLAYER_ID())
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "[OFF_STRIP] Local player is running the heli dock cut scene. Releasing control of the strippers")
//			OFFICE_SET_ALL_STRIPPERS_CAN_MIGRATE(TRUE, office_stripper_server_data)
//			OFFICE_LAP_DANCE_CLEAR_REQUESTS(stripper_client_data, sLocalOfficeStripData)
//		ENDIF
//		
//		RETURN TRUE
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC
//
////Checks to ensure we don't revert to the starting state if the owner reenters the property and takes back ownership of hte script
//PROC RUN_CHECKS_FOR_SERVER_RESTART(OFFICE_STRIPPER_SERVER_DATA_STRUCT &office_stripper_server_data, BOOL bServer)
//	IF bServer
//	AND g_iOfficeStripperSelected < 0
//	AND ENUM_TO_INT(office_stripper_server_data.SAStage) < ENUM_TO_INT(SA_STAGE_EXIT)
//		IF IS_BIT_SET( office_stripper_server_data.bits, ci_ServerSetEntrySceneDone)
//			g_iOfficeStripperSelected 	= office_stripper_server_data.iStripperCount
//			g_iStripperFriendID 		= office_stripper_server_data.iSecondStripperID
//			
//			IF g_iStripperFriendID > -1
//				g_bStripperBroughtAFriend = TRUE
//			ENDIF
//		ENDIF	
//	ENDIF
//ENDPROC
//
///// PURPOSE:
/////    Checks if the local player should be ignored in the fade screen checks
//FUNC BOOL SHOULD_SERVER_IGNORE_ME_FOR_OFFICE_STRIPPER_ENTRY_EXIT(OFFICE_STRIPPER_CLIENT_DATA_STRUCT &stripper_client_data)
//	RETURN IS_BIT_SET(stripper_client_data.bits, ci_IgnoreMeForFadeChecks)
//ENDFUNC
//
///// PURPOSE:
/////    Checks if the local player should be ignored in the fade screen checks
//PROC SET_SERVER_IGNORE_ME_FOR_OFFICE_STRIPPER_ENTRY(OFFICE_STRIPPER_CLIENT_DATA_STRUCT &stripper_client_data, BOOL bIgnoreMe)
//	IF bIgnoreMe
//		IF NOT IS_BIT_SET(stripper_client_data.bits, ci_IgnoreMeForFadeChecks)
//			SET_BIT(stripper_client_data.bits, ci_IgnoreMeForFadeChecks)
//		ENDIF
//	ELIF IS_BIT_SET(stripper_client_data.bits, ci_IgnoreMeForFadeChecks)
//		CLEAR_BIT(stripper_client_data.bits, ci_IgnoreMeForFadeChecks)
//	ENDIF
//ENDPROC
//
////Stripper functionality:
//PROC RUN_OFFICE_STRIPPERS(OFFICE_STRIPPER_SERVER_DATA_STRUCT &office_stripper_server_data, OFFICE_STRIPPER_CLIENT_DATA_STRUCT &stripper_client_data, 
//							OFFICE_STRIPPER_LOCAL_DATA_STRUCT &sLocalOfficeStripData, BOOL bServer, INT& iInteriorStripperServerBits)
//	
//	//Checks to ensure we don't revert to the starting state if the owner re-enters the property and takes back ownership of the script
//	RUN_CHECKS_FOR_SERVER_RESTART(office_stripper_server_data, bServer)
//	
//	MAINTAIN_ACTIVE_STRIPPER_IDS(office_stripper_server_data, bServer)
//	
//	STRIPPER_ACTIVITY_STAGE saStage = office_stripper_server_data.SAStage
//	
//	//Once we've done the intro the stages should be controlled by players
//	//so that multiple players to request a lap dance from the server
//	IF ENUM_TO_INT(saStage) >= ENUM_TO_INT(SA_STAGE_SETUP_PLAYERS)
//	AND ENUM_TO_INT(saStage) < ENUM_TO_INT(SA_STAGE_EXIT)
//		IF sLocalOfficeStripData.SAStage = SA_STAGE_REQUEST_MODELS
//			sLocalOfficeStripData.SAStage = office_stripper_server_data.SAStage
//		ELSE
//			saStage = sLocalOfficeStripData.SAStage
//		ENDIF
//	ENDIF
//	
//	IF office_stripper_server_data.iStripperCount >= 0
//	OR IS_BIT_SET(office_Stripper_Server_Data.bits, ci_ServerCleanup)
//		
//		//Has a cleanup been requested
//		IF RUN_STRIPPER_CLEANUP_CHECKS(office_stripper_server_data, stripper_client_data, sLocalOfficeStripData, bServer, iInteriorStripperServerBits)
//			EXIT
//		ENDIF
//		
//		//Setup the fade sceen requests to ensure the exit scene can begin
//		IF g_bRequestStrippersLeave
//		AND bServer
//			SETUP_FOR_OFFICE_STRIPPER_EXIT_SCENE(office_stripper_server_data, saStage)
//		ENDIF
//		
//		//Hide HUD when requested
//		IF g_bStripperHideHUD
//			HIDE_HUD_AND_RADAR_THIS_FRAME()
//			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
//		ENDIF
//		
//		IF g_bStripperHideHUDCutScene
//			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
//		ENDIF
//		
//		//Maintains the cut-scene cams to account for players that join the scene late
//		MAINTAIN_OFFICE_SCENE_CAMS(sLocalOfficeStripData)
//		
//		//Setup the flags for which of the 4 strippers are active
//		BOOL bStripperActive[ci_MAX_OFFICE_STRIPPERS]
//		SETUP_ACTIVE_STRIPPER_FLAGS(bStripperActive, office_stripper_server_data)		
//		
//		//Maintain checks for an interior swap
//		OFFICE_STRIPPERS_CHECK_FOR_INTERIOR_SWAP_FLAG(stripper_client_data, office_stripper_server_data, sLocalOfficeStripData, bStripperActive)
//		
//		//Check on any players having a lap dance & check if idle strippers are out of position
//		IF bServer
//			IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS, PROPERTY_BROADCAST_BS_PLAYER_APT_VARIANT_CONCEALED)
//				MAINTAIN_CHECKS_ON_IDLE_STRIPPERS(office_stripper_server_data, sLocalOfficeStripData)
//				MAINTAIN_CHECKS_ON_PLAYERS_RECEIVIENG_OFFICE_LAP_DANCE(office_stripper_server_data)
//			ENDIF
//		ELSE
//			//Give control and cleanup anything from the dance activity we were using
//			IF RESPOND_TO_SERVER_REQUESTS_FOR_STRIPPER_CONTROL(office_stripper_server_data, stripper_client_data, sLocalOfficeStripData)
//				EXIT
//			ENDIF
//		ENDIF
//		
//		//Disable recordings
//		MAINTAIN_DISABLE_RECORDING_OF_STRIPPER_IN_OFFICE()
//		
//		SWITCH saStage
//			CASE SA_STAGE_REQUEST_MODELS
//				IF bServer
//					MAINTAIN_OFFICE_STRIPPER_CREATION(office_stripper_server_data, office_stripper_server_data.SAStage)
//				ENDIF
//				
//				//This tells anyone joining that strippers are present. Used to prevent issues with accepting property invites
//				SET_BIT(office_stripper_server_data.bits, ci_StrippersPresent)
//			BREAK
//			CASE SA_STAGE_SETUP_PLAYERS
//				
//				OFFICE_STRIPPERS_REQUEST_IDLE_ANIM_DICTIONARIES()
//				
//				IF SHOULD_SERVER_IGNORE_ME_FOR_OFFICE_STRIPPER_ENTRY_EXIT(stripper_client_data)
//					g_bStripperHideHUD 				= FALSE
//					g_bStripperHideHUDCutScene 		= FALSE
//					sLocalOfficeStripData.SAStage 	= SA_STAGE_IDLE
//					CLEAR_BIT(stripper_client_data.bits, ci_SafeForCutscene)
//				ELSE
//				
//					IF NOT IS_BIT_SET(stripper_client_data.bits, ci_SafeForCutscene)
//						g_bStripperHideHUD 			= TRUE
//						g_bStripperHideHUDCutScene 	= TRUE
//						SET_BIT(stripper_client_data.bits, ci_SafeForCutscene)
//					ENDIF
//					
//					IF OFFICE_STRIPPER_RUN_LOCAL_ENTRY_SCENE(sLocalOfficeStripData, office_stripper_server_data, bServer, bStripperActive,iInteriorStripperServerBits)
//						
//						sLocalOfficeStripData.SAStage = SA_STAGE_IDLE
//						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//						
//						IF bServer
//							office_stripper_server_data.SAStage = SA_STAGE_IDLE
//							OFFICE_SET_ALL_STRIPPERS_CAN_MIGRATE(TRUE, office_stripper_server_data)
//							OFFICE_STRIPPERS_CHARGE_PLAYER()
//							
//							INCREMENT_MP_APT_STRIPPER_COUNTER()
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE SA_STAGE_IDLE				
//				//Make sure the stage is at it's start point
//				sLocalOfficeStripData.eDancePositionPhase = PS_TAKE_POSITION_ENTRY
//				SET_SERVER_IGNORE_ME_FOR_OFFICE_STRIPPER_ENTRY(stripper_client_data, FALSE)
//				
//				//Reset control and HUD variables
//				IF g_bStripperHideHUD
//					NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//					RESET_NET_TIMER(sLocalOfficeStripData.stActivityTimer)
//					g_bStripperHideHUD 			= FALSE
//					g_bStripperHideHUDCutScene 	= FALSE
//					CLEAR_STRIPPER_HIDE_HUD_CUTSCENE()
//				ENDIF
//				
//				//Check if I (Local Player) am in position to request a dance
//				IF RUN_CHECKS_FOR_PLAYERS_IN_POSITION_TO_REQUEST_DANCE(sLocalOfficeStripData, office_stripper_server_data)
//					sLocalOfficeStripData.SAStage = SA_STAGE_REQUEST_DANCE
//				ENDIF
//			BREAK
//			CASE SA_STAGE_REQUEST_DANCE
//				//Set the bit to request a dance from the server
//				IF sLocalOfficeStripData.iActiveChair = 1
//					SET_BIT(stripper_client_data.bits, ci_ClientRequestDanceC1)
//				ELSE
//					SET_BIT(stripper_client_data.bits, ci_ClientRequestDanceC2)
//				ENDIF
//				
//				sLocalOfficeStripData.SAStage = SA_STAGE_AWAIT_DANCE_APPROVAL
//			BREAK
//			CASE SA_STAGE_AWAIT_DANCE_APPROVAL
//				//Check for approval, grab control of the 1 or 2 strippers and start the dance
//				IF HAS_SERVER_APPROVED_DANCE_REQUEST(sLocalOfficeStripData, office_stripper_server_data)
//				AND HAS_PLAYER_BEEN_GIVEN_CONTROL_OF_STRIPPERS(sLocalOfficeStripData.iActiveChair, office_stripper_server_data)
//				AND RUN_LAP_DANCE_SCENES(sLocalOfficeStripData, office_stripper_server_data)
//					
//					//Once the sit down anim is complete we move to the dance active stage and cut to the scripted cams
//					THEFEED_FLUSH_QUEUE()
//					sLocalOfficeStripData.SAStage = SA_STAGE_DANCE_ACTIVE
//					OFFICE_STRIPPERDANCE_CREATE_CAMS(sLocalOfficeStripData, GET_OFFICE_STRIPPER_SCENE_DANCE_POS(sLocalOfficeStripData.iActiveChair), GET_OFFICE_STRIPPER_SCENE_DANCE_ROT(sLocalOfficeStripData.iActiveChair))
//				ENDIF
//			BREAK
//			CASE SA_STAGE_DANCE_ACTIVE
//				//Run the dance until its conclusion and await control inuts from the player
//				IF RUN_LAP_DANCE_SCENES(sLocalOfficeStripData, office_stripper_server_data)
//					sLocalOfficeStripData.SAStage = SA_STAGE_END_DANCE
//				ENDIF
//				
//				BOOL bAllowFPCam
//				IF sLocalOfficeStripData.eDancePositionPhase != PS_WAIT
//				AND sLocalOfficeStripData.eDancePositionPhase != PS_END_DANCE
//					bAllowFPCam = TRUE
//				ENDIF
//				
//				OFFICE_STRIPPERDANCE_UPDATECAMS(sLocalOfficeStripData, bAllowFPCam)
//			BREAK
//			CASE SA_STAGE_END_DANCE
//				IF NOT IS_BIT_SET(stripper_client_data.bits, ci_ChairStandUpDone)
//					
//					//Player chose to end the dance. Run the stand up animation
//					IF RUN_LAP_DANCE_SCENES(sLocalOfficeStripData, office_stripper_server_data)
//						SET_BIT(stripper_client_data.bits, ci_ChairStandUpDone)
//						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						g_bStripperHideHUD = FALSE
//					ENDIF
//				//End the dance and return the strippers to  the idle anim
//				ELIF END_LAP_DANCE(office_stripper_server_data, sLocalOfficeStripData, bStripperActive)
//					
//					//Reset dance variables
//					g_bApartmentDanceIsActive = FALSE
//					sLocalOfficeStripData.SAStage = SA_STAGE_IDLE
//					CLEAR_BIT(stripper_client_data.bits, ci_ClientRequestDanceC1)
//					CLEAR_BIT(stripper_client_data.bits, ci_ClientRequestDanceC2)
//					CLEAR_BIT(stripper_client_data.bits, ci_ChairStandUpDone)
//					OFFICE_STRIPPERDANCE_CLEANUPCAMS(sLocalOfficeStripData)
//					OFFICE_SET_ALL_STRIPPERS_CAN_MIGRATE(TRUE, office_stripper_server_data)
//					
//				ENDIF
//				
//				OFFICE_STRIPPERDANCE_UPDATECAMS(sLocalOfficeStripData, FALSE)
//			BREAK
//			CASE SA_STAGE_EXIT
//			
//				IF NOT IS_BIT_SET(stripper_client_data.bits, ci_FinishedExitScene)
//					IF OFFICE_STRIPPER_RUN_LOCAL_EXIT_SCENE(sLocalOfficeStripData, office_stripper_server_data)
//						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						SET_BIT(stripper_client_data.bits, ci_FinishedExitScene)
//						CLEAR_BIT(stripper_client_data.bits, ci_ClientCleanupDone)
//						CHANGE_ALL_OFFICE_STRIPPERS_VISIBILITY(office_stripper_server_data,  FALSE)
//						
//						IF IS_BIT_SET(office_stripper_server_data.bits, ci_StrippersPresent)
//							RESERVE_NETWORK_MISSION_PEDS(GET_NUM_RESERVED_MISSION_PEDS()- GET_COUNT_OF_ACTIVE_STRIPPERS())
//							CLEAR_BIT(office_stripper_server_data.bits, ci_StrippersPresent)
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE SA_STAGE_CLEANUP
//				CLEANUP_OFFICE_STRIPPERS(office_stripper_server_data, sLocalOfficeStripData)
//			BREAK
//			DEFAULT
//				CASSERTLN(DEBUG_SAFEHOUSE, "RUN_OFFICE_STRIPPERS: Invalid stage! ", office_stripper_server_data.SAStage)
//				office_stripper_server_data.SAStage = SA_STAGE_CLEANUP
//		ENDSWITCH
//	ENDIF
//ENDPROC

FUNC BOOL RUN_CARMOD_SCRIPT_FOR_OFFICE(OFFICE_PERSONAL_ASSISTANT& sOfficePA, INT iCurrentProperty, PLAYER_INDEX officeOwner)

	IF IS_PROPERTY_OFFICE_GARAGE(iCurrentProperty)
	#IF IS_DEBUG_BUILD
	OR g_bForceStartCarModScriptForOffice 
	#ENDIF
	
		IF DOES_PLAYER_OWNS_OFFICE_MOD_GARAGE(officeOwner)
		//AND !g_sMPTunables.bBIKER_CLUBHOUSE_DISABLE_MOD_SHOP
			//Make sure the car mod script cleanup flag is false
			IF g_bCleanUpCarmodShop = TRUE
				g_bCleanUpCarmodShop = FALSE
				PRINTLN("RUN_CARMOD_SCRIPT_FOR_OFFICE - Setting g_bCleanUpCarmodShop To FALSE")
			ENDIF
		
			IF NOT IS_BIT_SET(sOfficePA.iPALocalBS ,OFF_IS_CAR_MOD_SCRIPT_READY)
				REQUEST_SCRIPT("carmod_shop")
				IF HAS_SCRIPT_LOADED("carmod_shop")
				AND NOT IS_THREAD_ACTIVE(sOfficePA.CarModThread)
				AND !g_bCleanUpCarmodShop
					IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY("carmod_shop")) < 1
						IF NOT NETWORK_IS_SCRIPT_ACTIVE("carmod_shop",g_iCarModInstance,TRUE)
							SHOP_LAUNCHER_STRUCT sShopLauncherData
							sShopLauncherData.bLinkedShop = FALSE
							sShopLauncherData.eShop = CARMOD_SHOP_PERSONALMOD
							sShopLauncherData.iNetInstanceID = g_iCarModInstance
							
							SWITCH GET_OFFICE_TYPE_FROM_OFFICE_GARAGE(iCurrentProperty)
								CASE PROPERTY_OFFICE_1
									sShopLauncherData.ePersonalCarModVariation = PERSONAL_CAR_MOD_VARIATION_OFFICE_ONE
								BREAK
								CASE PROPERTY_OFFICE_2_BASE
									sShopLauncherData.ePersonalCarModVariation = PERSONAL_CAR_MOD_VARIATION_OFFICE_TWO
								BREAK
								CASE PROPERTY_OFFICE_3
									sShopLauncherData.ePersonalCarModVariation = PERSONAL_CAR_MOD_VARIATION_OFFICE_THREE
								BREAK
								CASE PROPERTY_OFFICE_4
									sShopLauncherData.ePersonalCarModVariation = PERSONAL_CAR_MOD_VARIATION_OFFICE_FOUR
								BREAK
							
							ENDSWITCH

							g_iPersonalCarModVariation = ENUM_TO_INT(sShopLauncherData.ePersonalCarModVariation) 
							sOfficePA.CarModThread = START_NEW_SCRIPT_WITH_ARGS("carmod_shop", sShopLauncherData, SIZE_OF(sShopLauncherData), CAR_MOD_SHOP_STACK_SIZE)
							SET_SCRIPT_AS_NO_LONGER_NEEDED("carmod_shop")
							SET_BIT(sOfficePA.iPALocalBS,OFF_IS_CAR_MOD_SCRIPT_READY)
							g_sShopSettings.bShopScriptLaunched[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = TRUE
							g_sShopSettings.bShopScriptLaunchedInMP[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = NETWORK_IS_GAME_IN_PROGRESS()
							
							PRINTLN("RUN_CARMOD_SCRIPT_FOR_OFFICE - TRUE - PersonalCarModVariation:", sShopLauncherData.ePersonalCarModVariation)
							RETURN TRUE
						ENDIF
					ELSE
						PRINTLN("RUN_CARMOD_SCRIPT_FOR_OFFICE - carmod is already running")
						RETURN TRUE 
					ENDIF
				ENDIF
			ELSE
				PRINTLN("RUN_CARMOD_SCRIPT_FOR_OFFICE - return true IS_BIT_SET(sOfficePA.iPALocalBS,OFF_IS_CAR_MOD_SCRIPT_READY) is TRUE")
				RETURN TRUE
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			PRINTLN("RUN_CARMOD_SCRIPT_FOR_OFFICE - return true - DOES_PLAYER_OWNS_OFFICE_MOD_GARAGE is FALSE")
			#ENDIF
			RETURN TRUE
		ENDIF	
	ELSE
		#IF IS_DEBUG_BUILD
		PRINTLN("RUN_CARMOD_SCRIPT_FOR_OFFICE - return true - this property is not OFFICE_GARAGE")
		#ENDIF
		RETURN TRUE
	ENDIF
	#IF IS_DEBUG_BUILD
		PRINTLN("RUN_CARMOD_SCRIPT_FOR_OFFICE - return false")
	#ENDIF
	RETURN FALSE
	
ENDFUNC

PROC CLEAN_UP_OFFICE_PERSONAL_CAR_MOD(OFFICE_PERSONAL_ASSISTANT& sOfficePA)
	PRINTLN("CLEAN_UP_PERSONAL_CAR_MOD - called")
	g_bCleanUpCarmodShop = TRUE
	g_bPersonalCarModTriggered = FALSE
	#IF IS_DEBUG_BUILD
	g_bForceStartCarModScriptForOffice = FALSE
	#ENDIF
	g_sShopSettings.bShopScriptLaunched[ENUM_TO_INT(CARMOD_SHOP_PERSONALMOD)] = FALSE
	SET_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(FALSE)
	SET_PLAYER_IN_OFFICE_MOD_INTERIOR(FALSE)
	CLEAR_BIT(sOfficePA.iPALocalBS,OFF_IS_CAR_MOD_SCRIPT_READY)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX pedVeh
		pedVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		NETWORK_REQUEST_CONTROL_OF_ENTITY(pedVeh)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(pedVeh)
			SET_VEHICLE_RADIO_ENABLED(pedVeh, TRUE)
			#IF IS_DEBUG_BUILD
			PRINTLN("CLEAN_UP_PERSONAL_CAR_MOD - enable radio")
			#ENDIF
		ENDIF	
	ENDIF
ENDPROC

/// PURPOSE:
///   Gets the vector used to place a blip in the clubhouse interior that indicates the position of the laptop
FUNC VECTOR GET_BLIP_COORDS_FOR_SECURO_COMPUTER(INT iCurrentProperty)
	SWITCH iCurrentProperty 
		CASE PROPERTY_OFFICE_1		RETURN <<-1556.5187, -575.7021, 108.3919>>	BREAK
		CASE PROPERTY_OFFICE_2_BASE	RETURN <<-1372.2982, -465.1636, 71.9110>>	BREAK
		CASE PROPERTY_OFFICE_3		RETURN <<-126.8692, -641.7910, 168.6894>>	BREAK
		CASE PROPERTY_OFFICE_4		RETURN <<-79.6132, -801.9753, 243.2548>>	BREAK
	ENDSWITCH
	
	RETURN <<-1372.2982, -465.1636, 71.9110>>
ENDFUNC

FUNC VECTOR GET_BLIP_COORDS_FOR_OFFICE_ASSISTANT(INT iCurrentProperty)
	SWITCH iCurrentProperty 
		CASE PROPERTY_OFFICE_1		RETURN <<-1571.4681, -574.3857, 108.5551>>	BREAK
		CASE PROPERTY_OFFICE_2_BASE	RETURN <<-1380.1173, -477.7654, 72.1366>>	BREAK
		CASE PROPERTY_OFFICE_3		RETURN <<-139.1908, -633.1746, 168.8882>>	BREAK
		CASE PROPERTY_OFFICE_4		RETURN <<-72.2164, -814.9074, 243.6987>>	BREAK
	ENDSWITCH
	
	RETURN <<-1571.4681, -574.3857, 108.5551>>
ENDFUNC
