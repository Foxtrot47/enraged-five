///// net_realty_businesses.sch
///// Online Technical: Scott Ranken,
///// Description: Top level header to include core Business functions
/////              from all businessess, e.g Biker Factories, Hangars etc.

USING "net_include.sch"

#IF FEATURE_CASINO_HEIST

FUNC BOOL WAS_BUSINESS_APP_SRIPT_LAUNCHED_FROM_HUB_APP()
	RETURN g_sBusAppManagement.iPropertyID != -1
ENDFUNC

#ENDIF

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════╡ FINANCE & FELONY WAREHOUSES ╞═══════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE: Checks if the specified player owns any warehouses
FUNC BOOL DOES_PLAYER_OWN_A_WAREHOUSE(PLAYER_INDEX playerID)
	INT iIter
	
	FOR iIter = 0 TO (ciMaxOwnedWarehouses - 1)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iIter].iWarehouse != ciW_Invalid
			CDEBUG3LN(DEBUG_SAFEHOUSE, "[WHOUSE] remote player (", GET_PLAYER_NAME(playerID), ") owns warehouse ", GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[iIter].iWarehouse, " in slot ", iIter, " [any]")
			RETURN TRUE
		ENDIF
	ENDFOR
	
	CDEBUG3LN(DEBUG_SAFEHOUSE, "[WHOUSE] DOES_PLAYER_OWN_A_WAREHOUSE. remote player (", GET_PLAYER_NAME(playerID), ") does not own a warehouse")
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns the size of the specified warehouse
///    Validates the warehouse ID before returning its size
FUNC EWarehouseSize GET_WAREHOUSE_SIZE(INT iWarehouse)
	IF CHECK_WAREHOUSE_ID(iWarehouse)
		SWITCH iWarehouse
			CASE ciW_Elysian_Island_2  
			CASE ciW_La_Puerta_1 
			CASE ciW_La_Mesa_1 
			CASE ciW_Rancho_1	 
			CASE ciW_West_Vinewood_1	
			CASE ciW_Elysian_Island_1 
				RETURN eWarehouseSmall
				
			CASE ciW_El_Burro_Heights_1 
			CASE ciW_Elysian_Island_3 	
			CASE ciW_La_Mesa_2 			
			CASE ciW_Maze_Bank_Arena_1 	
			CASE ciW_Strawberry_1		
			CASE ciW_Downtown_Vinewood_1
			CASE ciW_Rancho_2
			CASE ciW_Del_Perro_1
				RETURN eWarehouseMedium
				
			CASE ciW_La_Mesa_3 			
			CASE ciW_La_Mesa_4 			
			CASE ciW_Cypress_Flats_2 	
			CASE ciW_Cypress_Flats_3 	
			CASE ciW_Vinewood_1 				
			CASE ciW_Banning_1
			CASE ciW_LSIA_1	
			CASE ciW_LSIA_2
				RETURN eWarehouseLarge
		ENDSWITCH
	ENDIF
	
	CASSERTLN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_SIZE: ", iWarehouse, " Invalid warehouse returning: EWarehouseSizeInvalid")
	RETURN EWarehouseSizeInvalid
ENDFUNC

/// PURPOSE: Checks if the specified player owns any warehouses
FUNC BOOL DOES_PLAYER_OWN_A_WAREHOUSE_OF_SIZE(PLAYER_INDEX playerID, EWarehouseSize eSize)
	IF playerID = INVALID_PLAYER_INDEX()
		SCRIPT_ASSERT("DOES_PLAYER_OWN_A_WAREHOUSE_OF_SIZE - invalid player")
		RETURN FALSE
	ENDIF
	
	INT i
	
	REPEAT ciMaxOwnedWarehouses i
		IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[i].iWarehouse != ciW_Invalid
			IF GET_WAREHOUSE_SIZE(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[i].iWarehouse) = eSize
				CDEBUG3LN(DEBUG_SAFEHOUSE, "[WHOUSE] DOES_PLAYER_OWN_A_WAREHOUSE_OF_SIZE - remote player (", GET_PLAYER_NAME(playerID), ") owns warehouse ", GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdWarehouseData[i].iWarehouse, " in slot ", i, " [any]")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	CDEBUG3LN(DEBUG_SAFEHOUSE, "[WHOUSE] DOES_PLAYER_OWN_A_WAREHOUSE_OF_SIZE. remote player (", GET_PLAYER_NAME(playerID), ") does not own a warehouse of the given size: ", eSize)
	RETURN FALSE
ENDFUNC

/// PURPOSE: Returns the maximum capacity of the passed warehouse
///    Validates the warehouse ID before checking its capacity
FUNC INT GET_WAREHOUSE_MAX_CAPACITY(INT iWarehouse)
	IF CHECK_WAREHOUSE_ID(iWarehouse)
		EWarehouseSize WHSize = GET_WAREHOUSE_SIZE(iWarehouse)
		
		IF WHSize = eWarehouseSmall
			RETURN	ciMaxWarehouseStorageSmall
		ELIF WHSize = eWarehouseMedium
			RETURN	ciMaxWarehouseStorageMedium
		ELSE
			RETURN	ciMaxWarehouseStorageLarge
		ENDIF
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[WHOUSE] GET_WAREHOUSE_MAX_CAPACITY FAILED. Valid warehouse ID not returning a valid size! ", iWarehouse)
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC BOOL DOES_LP_OWN_WAREHOUSE_SUITABLE_FOR_DAILY_OBJ()
	
	INT i
	
	REPEAT ciMaxOwnedWarehouses i
		INT iWarehouse = g_MP_STAT_WAREHOUSE[i][GET_SLOT_NUMBER(-1)]
		
		//Do we own one in this slot?
		IF iWarehouse != ciW_Invalid
			INT iMaxCapacity = GET_WAREHOUSE_MAX_CAPACITY(iWarehouse)
			
			//Does this warehouse have any storage space
			IF g_MP_STAT_CONTRABAND_TOT_FOR_WAREHOUSE[i][GET_SLOT_NUMBER(-1)] < iMaxCapacity
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ BIKER FACTORIES ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC MP_INT_STATS GET_STAT_ENUM_FOR_FACTORY_SLOT(INT iFactorySlot)
	SWITCH iFactorySlot
		CASE 0	RETURN MP_STAT_FACTORYSLOT0
		CASE 1	RETURN MP_STAT_FACTORYSLOT1
		CASE 2	RETURN MP_STAT_FACTORYSLOT2
		CASE 3	RETURN MP_STAT_FACTORYSLOT3
		CASE 4	RETURN MP_STAT_FACTORYSLOT4
		CASE 5	RETURN MP_STAT_FACTORYSLOT5
	ENDSWITCH
	
	CASSERTLN(DEBUG_SAFEHOUSE, "[FACTORY] MPPLY_CAS_TIME_PLYING passed invalid slot: ", iFactorySlot)
	RETURN MP_STAT_FACTORYSLOT0
ENDFUNC



FUNC BOOL DOES_PLAYER_OWN_FACTORY(PLAYER_INDEX playerID, FACTORY_ID eFactoryID)
	
	IF CHECK_FACTORY_ID(eFactoryID)
	AND playerID != INVALID_PLAYER_INDEX()
		INT iIter	
		FOR iIter = 0 TO (ciMAX_OWNED_FACTORIES - 1)
			IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdFactoryData[iIter].eFactoryID = eFactoryID
				RETURN TRUE
			ENDIF
		ENDFOR
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PLAYER_OWN_A_BIKER_FACTORY(PLAYER_INDEX playerID)
	IF playerID != INVALID_PLAYER_INDEX()
		INT i	
		REPEAT MAX_OWNED_BIKER_FACTORIES i
			IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdFactoryData[i].eFactoryID != FACTORY_ID_INVALID
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if the player owns a factory of the given type in any slot
FUNC BOOL DOES_PLAYER_OWN_FACTORY_OF_TYPE(PLAYER_INDEX piPlayer, FACTORY_TYPE eType)

	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN TRUE
	ENDIF
	IF piPlayer = INVALID_PLAYER_INDEX()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[FACTORY] DOES_PLAYER_OWN_FACTORY_OF_TYPE: INVALID_PLAYER_INDEX passed returning 0")
		RETURN FALSE
	ENDIF
	
	INT iIter
	FOR iIter = 0 TO (ciMAX_OWNED_FACTORIES - 1)
		IF GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].propertyDetails.bdFactoryData[iIter].eFactoryID != FACTORY_ID_INVALID
		
			FACTORY_ID eFectoryID = GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].propertyDetails.bdFactoryData[iIter].eFactoryID
			
			IF GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFectoryID) = eType
				RETURN TRUE
			ENDIF
			
		ENDIF
		
	ENDFOR
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns true if the local player owns a factory of the given type in any slot
FUNC BOOL STAT_DOES_LOCAL_PLAYER_OWN_FACTORY_OF_TYPE(FACTORY_TYPE eType)
	
	INT iIter
	FOR iIter = 0 TO (ciMAX_OWNED_FACTORIES - 1)
		FACTORY_ID eFactory = INT_TO_ENUM(FACTORY_ID, GET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_FACTORY_SLOT(iIter)))
		IF eFactory != FACTORY_ID_INVALID
			
			IF GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFactory) = eType
				RETURN TRUE
			ENDIF
			
		ENDIF
		
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC STATS_PACKED GET_STAT_ENUM_FOR_FACTORY_PRODUCTION_ACTIVE_SLOT(INT iFactorySlot)
	SWITCH iFactorySlot
		CASE 0	RETURN PACKED_MP_BOOL_FACTSTARTPROD_SLOT0
		CASE 1	RETURN PACKED_MP_BOOL_FACTSTARTPROD_SLOT1
		CASE 2	RETURN PACKED_MP_BOOL_FACTSTARTPROD_SLOT2
		CASE 3	RETURN PACKED_MP_BOOL_FACTSTARTPROD_SLOT3
		CASE 4	RETURN PACKED_MP_BOOL_FACTSTARTPROD_SLOT4
		CASE BUNKER_SAVE_SLOT RETURN PACKED_MP_BOOL_FACTSTARTPROD_SLOT5
	ENDSWITCH
	
	CASSERTLN(DEBUG_SAFEHOUSE, "[FACTORY] GET_STAT_ENUM_FOR_FACTORY_PRODUCTION_ACTIVE_SLOT passed invalid slot: ", iFactorySlot)
	RETURN PACKED_MP_BOOL_FACTSTARTPROD_SLOT0
ENDFUNC

FUNC MP_INT_STATS GET_STAT_ENUM_FOR_FACTORY_MATERIALS_TOTAL_SLOT(INT iFactorySlot)
	SWITCH iFactorySlot
		CASE 0	RETURN MP_STAT_MATTOTALFORFACTORY0
		CASE 1	RETURN MP_STAT_MATTOTALFORFACTORY1
		CASE 2	RETURN MP_STAT_MATTOTALFORFACTORY2
		CASE 3	RETURN MP_STAT_MATTOTALFORFACTORY3
		CASE 4	RETURN MP_STAT_MATTOTALFORFACTORY4
		CASE BUNKER_SAVE_SLOT RETURN MP_STAT_MATTOTALFORFACTORY5
	ENDSWITCH
	
	CASSERTLN(DEBUG_SAFEHOUSE, "[FACTORY] GET_STAT_ENUM_FOR_FACTORY_MATERIALS_TOTAL_SLOT passed invalid slot: ", iFactorySlot)
	RETURN MP_STAT_MATTOTALFORFACTORY0
ENDFUNC

/// PURPOSE:
///    Returns true if the local player owns a factory of the given type in any slot
FUNC BOOL DOES_LP_OWN_FACTORY_OF_TYPE_SUITABLE_FOR_DAILY_OBJ(FACTORY_TYPE eType)
	INT iSlot
	FACTORY_ID eFactory
	
	//Do we own one fo this type?
	FOR iSlot = 0 TO (ciMAX_OWNED_FACTORIES - 1)
		eFactory = INT_TO_ENUM(FACTORY_ID, GET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_FACTORY_SLOT(iSlot)))
		IF eFactory != FACTORY_ID_INVALID
			
			IF GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFactory) = eType
				BREAKLOOP
			ELIF iSlot = (ciMAX_OWNED_FACTORIES - 1)
				RETURN FALSE
			ENDIF
			
		ELIF iSlot = (ciMAX_OWNED_FACTORIES - 1)
			RETURN FALSE
		ENDIF
		
	ENDFOR
	
	//Is production active:
	IF NOT GET_PACKED_STAT_BOOL(GET_STAT_ENUM_FOR_FACTORY_PRODUCTION_ACTIVE_SLOT(iSlot))
		RETURN FALSE
	ENDIF
	
	//Did the player halt production:
	INT iProductionStateBS = GET_MP_INT_CHARACTER_STAT(MP_STAT_BKR_FACTORY_PROD_STOPPED)
	
	IF IS_BIT_SET(iProductionStateBS, iSlot)
		RETURN FALSE
	ENDIF
	
	//Do we need supplies?
	IF GET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_FACTORY_MATERIALS_TOTAL_SLOT(iSlot)) = 100
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC FACTORY_ID GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_INDEX playerID, INT iFactorySlot)
	
	FACTORY_ID eFactoryID
	
	IF playerID = INVALID_PLAYER_INDEX()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[FACTORY] GET_FACTORY_ID_FROM_FACTORY_SLOT: INVALID_PLAYER_INDEX passed returning FACTORY_ID_INVALID")
		RETURN FACTORY_ID_INVALID
	ENDIF
	
	IF iFactorySlot < 0
	OR iFactorySlot >= ciMAX_OWNED_FACTORIES
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[FACTORY] GET_FACTORY_ID_FROM_FACTORY_SLOT: invalid factory slot passed: ", iFactorySlot)
		RETURN FACTORY_ID_INVALID
	ENDIF
	
	IF CHECK_FACTORY_ID(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdFactoryData[iFactorySlot].eFactoryID)	
		eFactoryID = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdFactoryData[iFactorySlot].eFactoryID
	ENDIF
	
	//CDEBUG1LN(DEBUG_SAFEHOUSE, "[FACTORY] GET_FACTORY_ID_FROM_FACTORY_SLOT: Returning Factory ID: ", ENUM_TO_INT(eFactoryID), "From Slot: ", iFactorySlot)
	RETURN eFactoryID
ENDFUNC

FUNC BOOL IS_PLAYERS_FACTORY_PRODUCTION_ACTIVATED(PLAYER_INDEX pOwner, INT iSlot)
	IF pOwner != INVALID_PLAYER_INDEX()
	AND DOES_PLAYER_OWN_FACTORY(pOwner, GET_FACTORY_ID_FROM_FACTORY_SLOT(pOwner, iSlot))
		RETURN GlobalplayerBD_FM[NATIVE_TO_INT(pOwner)].propertyDetails.bdFactoryData[iSlot].bProductionActive
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_FACTORY_OF_TYPE_WEAPONS(FACTORY_ID eFactoryID)
	RETURN (GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFactoryID) = FACTORY_TYPE_WEAPONS)
ENDFUNC

/// PURPOSE:
///    Returns the percentage of research the player has done towards unlocking a weapon upgrade in their bunker business
FUNC INT GET_RESEARCH_TOTAL_FOR_FACTORY(PLAYER_INDEX playerID, FACTORY_ID eFactoryID)
	IF playerID = INVALID_PLAYER_INDEX()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[FACTORY] GET_RESEARCH_TOTAL_FOR_FACTORY: INVALID_PLAYER_INDEX passed returning 0")
		RETURN 0
	ENDIF

	IF CHECK_FACTORY_ID(eFactoryID)
	AND IS_FACTORY_OF_TYPE_WEAPONS(eFactoryID)
		INT iIter
		FOR iIter = 0 TO (ciMAX_OWNED_FACTORIES - 1)
			IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdFactoryData[iIter].eFactoryID = eFactoryID
				RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBunkerResearchTotal
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN 0
ENDFUNC

FUNC INT GET_FACTORY_RESEARCH_CAPACITY(FACTORY_ID eFactoryID)
	SWITCH eFactoryID
		CASE FACTORY_ID_BUNKER_1
		CASE FACTORY_ID_BUNKER_2
		CASE FACTORY_ID_BUNKER_3
		CASE FACTORY_ID_BUNKER_4
		CASE FACTORY_ID_BUNKER_5
		CASE FACTORY_ID_BUNKER_6
		CASE FACTORY_ID_BUNKER_7
//		CASE FACTORY_ID_BUNKER_8
		CASE FACTORY_ID_BUNKER_9
		CASE FACTORY_ID_BUNKER_10
		CASE FACTORY_ID_BUNKER_11
		CASE FACTORY_ID_BUNKER_12
			RETURN g_sMPTunables.IGR_RESEARCH_CAPACITY
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC BOOL IS_FACTORY_RESEARCH_FULL(PLAYER_INDEX playerID, FACTORY_ID eFactoryID)
	RETURN (GET_RESEARCH_TOTAL_FOR_FACTORY(playerID, eFactoryID) >= GET_FACTORY_RESEARCH_CAPACITY(eFactoryID))
ENDFUNC


FUNC INT GET_FACTORY_PRODUCT_CAPACITY(FACTORY_ID eFactoryID)
	
	SWITCH eFactoryID
		CASE FACTORY_ID_WEED_1		RETURN g_sMPTunables.iBIKER_WEED_CAPACITY
		CASE FACTORY_ID_METH_1		RETURN g_sMPTunables.iBIKER_METH_CAPACITY  
		CASE FACTORY_ID_CRACK_1		RETURN g_sMPTunables.iBIKER_COCAINE_CAPACITY 
		CASE FACTORY_ID_FAKE_ID_1	RETURN g_sMPTunables.iBIKER_FAKEIDS_CAPACITY
		CASE FACTORY_ID_FAKE_CASH_1	RETURN g_sMPTunables.iBIKER_COUNTERCASH_CAPACITY
		
		CASE FACTORY_ID_WEED_2		RETURN g_sMPTunables.iBIKER_WEED_CAPACITY
		CASE FACTORY_ID_METH_2		RETURN g_sMPTunables.iBIKER_METH_CAPACITY  
		CASE FACTORY_ID_CRACK_2		RETURN g_sMPTunables.iBIKER_COCAINE_CAPACITY 
		CASE FACTORY_ID_FAKE_ID_2	RETURN g_sMPTunables.iBIKER_FAKEIDS_CAPACITY 
		CASE FACTORY_ID_FAKE_CASH_2	RETURN g_sMPTunables.iBIKER_COUNTERCASH_CAPACITY
		
		CASE FACTORY_ID_WEED_3		RETURN g_sMPTunables.iBIKER_WEED_CAPACITY
		CASE FACTORY_ID_METH_3		RETURN g_sMPTunables.iBIKER_METH_CAPACITY  
		CASE FACTORY_ID_CRACK_3		RETURN g_sMPTunables.iBIKER_COCAINE_CAPACITY 
		CASE FACTORY_ID_FAKE_ID_3	RETURN g_sMPTunables.iBIKER_FAKEIDS_CAPACITY 
		CASE FACTORY_ID_FAKE_CASH_3	RETURN g_sMPTunables.iBIKER_COUNTERCASH_CAPACITY
		
		CASE FACTORY_ID_WEED_4		RETURN g_sMPTunables.iBIKER_WEED_CAPACITY
		CASE FACTORY_ID_METH_4		RETURN g_sMPTunables.iBIKER_METH_CAPACITY  
		CASE FACTORY_ID_CRACK_4		RETURN g_sMPTunables.iBIKER_COCAINE_CAPACITY 
		CASE FACTORY_ID_FAKE_ID_4	RETURN g_sMPTunables.iBIKER_FAKEIDS_CAPACITY
		CASE FACTORY_ID_FAKE_CASH_4	RETURN g_sMPTunables.iBIKER_COUNTERCASH_CAPACITY
		
		CASE FACTORY_ID_BUNKER_1
		CASE FACTORY_ID_BUNKER_2
		CASE FACTORY_ID_BUNKER_3
		CASE FACTORY_ID_BUNKER_4
		CASE FACTORY_ID_BUNKER_5
		CASE FACTORY_ID_BUNKER_6
		CASE FACTORY_ID_BUNKER_7
//		CASE FACTORY_ID_BUNKER_8
		CASE FACTORY_ID_BUNKER_9
		CASE FACTORY_ID_BUNKER_10
		CASE FACTORY_ID_BUNKER_11
		CASE FACTORY_ID_BUNKER_12
			RETURN g_sMPTunables.IGR_MANU_CAPACITY
	ENDSWITCH
		
	//CASSERTLN(DEBUG_SAFEHOUSE, "[FACTORY] GET_FACTORY_PRODUCT_CAPACITY invalid goods type specified returning 0")
	RETURN 0
ENDFUNC

FUNC BOOL IS_FACTORY_SLOT_VALID(INT iSlot)
	RETURN iSlot >= 0 AND iSlot < ciMAX_OWNED_FACTORIES
ENDFUNC

FUNC INT GET_PLAYER_SAVE_SLOT_FOR_FACTORY(PLAYER_INDEX playerID, FACTORY_ID eFactoryID)
	
	IF CHECK_FACTORY_ID(eFactoryID)
	AND playerID != INVALID_PLAYER_INDEX()
		INT iIter
		FOR iIter = 0 TO (ciMAX_OWNED_FACTORIES - 1)
			IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdFactoryData[iIter].eFactoryID = eFactoryID
				RETURN iIter
			ENDIF
		ENDFOR
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[FACTORY] GET_PLAYER_SAVE_SLOT_FOR_FACTORY: Player: ", NATIVE_TO_INT(playerID), " does not own Factory: ", eFactoryID, " Returning -1")
	RETURN -1
ENDFUNC

FUNC BOOL IS_PLAYERS_FACTORY_FULL(PLAYER_INDEX pOwner, FACTORY_ID eFactory)
	IF pOwner != INVALID_PLAYER_INDEX()
	AND DOES_PLAYER_OWN_FACTORY(pOwner, eFactory)
		INT iSlot = GET_PLAYER_SAVE_SLOT_FOR_FACTORY(pOwner, eFactory)
		IF IS_FACTORY_SLOT_VALID(iSlot)
			RETURN GlobalplayerBD_FM[NATIVE_TO_INT(pOwner)].propertyDetails.bdFactoryData[iSlot].iProductTotal >= GET_FACTORY_PRODUCT_CAPACITY(eFactory)
		ENDIF
	ENDIF
	
	CDEBUG3LN(DEBUG_SAFEHOUSE, "[FACTORY] IS_FACTORY_FULL: returning false as player ", GET_PLAYER_NAME(pOwner), " does not own factory: ", eFactory)
	RETURN FALSE
ENDFUNC

FUNC INT GET_MATERIALS_TOTAL_FOR_FACTORY(PLAYER_INDEX playerID, FACTORY_ID eFactoryID) 
	
	IF playerID = INVALID_PLAYER_INDEX()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[FACTORY] GET_MATERIALS_TOTAL_FOR_FACTORY: INVALID_PLAYER_INDEX passed returning 0")
		RETURN 0
	ENDIF

	IF CHECK_FACTORY_ID(eFactoryID)
		INT iIter
		FOR iIter = 0 TO (ciMAX_OWNED_FACTORIES - 1)
			IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdFactoryData[iIter].eFactoryID = eFactoryID
				RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdFactoryData[iIter].iMaterialsTotal
			ENDIF
		ENDFOR
	ENDIF
	
//	CDEBUG1LN(DEBUG_SAFEHOUSE, "[FACTORY] GET_CONTRABAND_TOTAL_FOR_FACTORY: Player does not own this Factory!")
	RETURN 0
ENDFUNC

FUNC INT GET_PRODUCTION_STATE_FOR_FACTORY(PLAYER_INDEX playerID, FACTORY_ID eFactoryID) 
	
	IF playerID = INVALID_PLAYER_INDEX()
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[FACTORY] GET_PRODUCTION_STATE_FOR_FACTORY: INVALID_PLAYER_INDEX passed returning 0")
		RETURN 0
	ENDIF
	
	IF CHECK_FACTORY_ID(eFactoryID)
		INT iIter
		FOR iIter = 0 TO (ciMAX_OWNED_FACTORIES - 1)
			IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdFactoryData[iIter].eFactoryID = eFactoryID
				RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdFactoryData[iIter].iProductionStage
			ENDIF
		ENDFOR
	ENDIF
	
//	CDEBUG1LN(DEBUG_SAFEHOUSE, "[FACTORY] GET_CONTRABAND_TOTAL_FOR_FACTORY: Player does not own this Factory!")
	RETURN 0
ENDFUNC

FUNC BOOL IS_FACTORY_IN_AN_ACTIVE_PRODUCTION_STAGE(PLAYER_INDEX pOwner, INT iFactorySlot)
	
	BOOL bActive = FALSE
	
	FACTORY_ID eFactoryID = GET_FACTORY_ID_FROM_FACTORY_SLOT(pOwner, iFactorySlot)
	FACTORY_TYPE eFactoryType = GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFactoryID)
	
	SWITCH eFactoryType
		CASE FACTORY_TYPE_WEED
			IF GET_PRODUCTION_STATE_FOR_FACTORY(pOwner, eFactoryID) > 8
				bActive = TRUE
			ENDIF
		BREAK
		CASE FACTORY_TYPE_METH
			IF GET_PRODUCTION_STATE_FOR_FACTORY(pOwner, eFactoryID) > 2
				bActive = TRUE
			ENDIF
		BREAK
		CASE FACTORY_TYPE_CRACK
			IF GET_PRODUCTION_STATE_FOR_FACTORY(pOwner, eFactoryID) > 1
				bActive = TRUE
			ENDIF
		BREAK
		CASE FACTORY_TYPE_FAKE_IDS
			IF GET_PRODUCTION_STATE_FOR_FACTORY(pOwner, eFactoryID) > 1
				bActive = TRUE
			ENDIF
		BREAK
		CASE FACTORY_TYPE_FAKE_MONEY
			IF GET_PRODUCTION_STATE_FOR_FACTORY(pOwner, eFactoryID) > 1
				bActive = TRUE
			ENDIF
		BREAK
		CASE FACTORY_TYPE_WEAPONS
			IF GET_PRODUCTION_STATE_FOR_FACTORY(pOwner, eFactoryID) > 1
				bActive = TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN bActive
ENDFUNC

FUNC BOOL IS_PLAYER_ON_A_RESUPPLY_MISSION_FOR_FACTORY(FACTORY_ID eFactory, PLAYER_INDEX pOwner)
	
		IF IS_FACTORY_OF_TYPE_WEAPONS(eFactory)
			RETURN (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(pOwner) = FMMC_TYPE_GUNRUNNING_BUY)
		ENDIF
	
	IF GB_IS_PLAYER_ON_ILLICIT_GOODS_RESUPPLY_MISSION(pOwner)
		ILLICIT_GOODS_MISSION_DATA missionData = GB_GET_REMOTE_PLAYER_ILLICIT_GOOD_MISSION_DATA(pOwner)
		IF missionData.eMissionFactory = eFactory
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Was factory production halted due to some external factors
/// PARAMS:
///    iFactorySlot - 
/// RETURNS:
///    
FUNC BOOL IS_FACTORY_PRODUCTION_HALTED(PLAYER_INDEX pOwner, INT iFactorySlot, BOOL bStageDependant = TRUE  , BOOL bResearchMode = FALSE )
	
	FACTORY_ID eFactory = GET_FACTORY_ID_FROM_FACTORY_SLOT(pOwner, iFactorySlot)
	
	#IF IS_DEBUG_BUILD
	IF g_bFactoryWidgetEnabled
		RETURN NOT g_bFactoryForceActiveProduction
	ENDIF
	#ENDIF
	
	IF pOwner != INVALID_PLAYER_INDEX()
	AND IS_FACTORY_SLOT_VALID(iFactorySlot)
	AND IS_PLAYERS_FACTORY_PRODUCTION_ACTIVATED(pOwner, iFactorySlot)
		BOOL bActiveProduction = TRUE
		
		// FACTORY PRODUCT IS FULL
		IF bResearchMode
			IF IS_FACTORY_RESEARCH_FULL(pOwner, eFactory)
				bActiveProduction = FALSE
			ENDIF
		ELSE
			IF IS_PLAYERS_FACTORY_FULL(pOwner, eFactory)
				bActiveProduction = FALSE
			ENDIF
		ENDIF
		
		// MATERIALS HAVE RUN OUT
		IF GET_MATERIALS_TOTAL_FOR_FACTORY(pOwner, eFactory) <= 0
		OR g_bProductionShouldBeHalted[iFactorySlot]
			bActiveProduction = FALSE
		ENDIF
		
		// SETUP TIMERS NOT COMPLETED
		IF bStageDependant
			IF NOT IS_FACTORY_IN_AN_ACTIVE_PRODUCTION_STAGE(pOwner, iFactorySlot)
				bActiveProduction = FALSE
			ENDIF
		ELSE
			IF GET_PRODUCTION_STATE_FOR_FACTORY(pOwner, eFactory) < 1
				bActiveProduction = FALSE
			ENDIF
		ENDIF
		
		// STOP PRODUCTION WHILE ON A MISSION
		IF IS_PLAYER_ON_A_RESUPPLY_MISSION_FOR_FACTORY(eFactory, pOwner)
			bActiveProduction = FALSE
		ENDIF
		
		IF iFactorySlot = BUNKER_SAVE_SLOT
			IF g_sMPTunables.BGR_STOP_PRODUCTION_WHEN_NOT_GANG_BOSS 
				IF NOT GB_IS_PLAYER_BOSS_OF_A_GANG(pOwner)
					RETURN NOT bActiveProduction						
				ENDIF
			ENDIF
		ELSE
			IF g_sMPTunables.bBIKER_STOP_PRODUCTION_WHEN_NOT_MC_PRESIDENT
				IF NOT GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(pOwner)
					bActiveProduction = FALSE
				ENDIF
			ENDIF
		ENDIF
		
		RETURN NOT bActiveProduction
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_FACTORY_PRODUCTION_ACTIVE(PLAYER_INDEX pOwner, INT iFactorySlot  , BOOL bResearchMode = FALSE )
	
	BOOL bActive = FALSE
	
	IF IS_PLAYERS_FACTORY_PRODUCTION_ACTIVATED(pOwner, iFactorySlot) AND NOT IS_FACTORY_PRODUCTION_HALTED(pOwner, iFactorySlot, DEFAULT  , bResearchMode )
		bActive = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF g_bFactoryWidgetEnabled
		IF g_bFactoryForceActiveProduction
			bActive = TRUE
		ENDIF
	ENDIF
	#ENDIF
	
	RETURN bActive
ENDFUNC

FUNC BOOL HAS_PLAYER_COMPLETED_FACTORY_SETUP_MISSION(PLAYER_INDEX playerID, FACTORY_ID eFactoryID)
	IF DOES_PLAYER_OWN_FACTORY(playerID, eFactoryID)
		INT iSaveSlot = GET_PLAYER_SAVE_SLOT_FOR_FACTORY(playerID, eFactoryID)
		
		IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdFactoryData[iSaveSlot].iProductionStage > 0
		AND GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdFactoryData[iSaveSlot].bSetupComplete
			RETURN TRUE
		ENDIF
	ELSE
		//CDEBUG3LN(DEBUG_SAFEHOUSE, "[FACTORY] HAS_PLAYER_COMPLETED_FACTORY_SETUP_MISSION: Player ", NATIVE_TO_INT(playerID), " does not own factory ", eFactoryID)
	ENDIF
	
	//CDEBUG3LN(DEBUG_SAFEHOUSE, "[FACTORY] HAS_PLAYER_COMPLETED_FACTORY_SETUP_MISSION: Player ", GET_PLAYER_NAME(playerID), " has not completed the setup for factory ", eFactoryID)
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DELAY_PRODUCTION_TIMER_RUNNING(INT iFactorySlot)
	RETURN g_iFactorySetupDelayTimers[iFactorySlot] != 0
ENDFUNC

FUNC BOOL HAS_PLAYER_SUSPENDED_BUNKER_PRODUCTION()
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[BUNKER_SAVE_SLOT].iProductionStage = 1	
	AND NOT IS_DELAY_PRODUCTION_TIMER_RUNNING(BUNKER_SAVE_SLOT)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_SUSPENDED_BIKER_BUSINESS_PRODUCTION(PLAYER_INDEX playerID, FACTORY_ID eFactoryID)
	IF NOT DOES_PLAYER_OWN_FACTORY(playerID, eFactoryID)
		RETURN FALSE
	ENDIF
	
	INT iSaveSlot = GET_PLAYER_SAVE_SLOT_FOR_FACTORY(playerID, eFactoryID)
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdFactoryData[iSaveSlot].iProductionStage > 0
	AND NOT IS_PLAYERS_FACTORY_PRODUCTION_ACTIVATED(playerID, iSaveSlot)
	AND NOT IS_DELAY_PRODUCTION_TIMER_RUNNING(iSaveSlot)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BUSINESS_TYPE GET_BUSINESS_TYPE_FROM_FACTORY_TYPE(FACTORY_TYPE eType)
	SWITCH eType
		CASE FACTORY_TYPE_WEAPONS 		RETURN BUSINESS_TYPE_WEAPONS
		CASE FACTORY_TYPE_WEED			RETURN BUSINESS_TYPE_WEED
		CASE FACTORY_TYPE_CRACK			RETURN BUSINESS_TYPE_COKE
		CASE FACTORY_TYPE_METH			RETURN BUSINESS_TYPE_METH
		CASE FACTORY_TYPE_FAKE_IDS		RETURN BUSINESS_TYPE_FORGED_DOCS
		CASE FACTORY_TYPE_FAKE_MONEY	RETURN BUSINESS_TYPE_COUNTERFEIT_CASH
	ENDSWITCH
	
	RETURN BUSINESS_TYPE_INVALID
ENDFUNC

FUNC FACTORY_TYPE GET_FACTORY_TYPE_FROM_BUSINESS_TYPE(BUSINESS_TYPE eType)
	SWITCH eType
		CASE BUSINESS_TYPE_WEAPONS			RETURN FACTORY_TYPE_WEAPONS 	BREAK
		CASE BUSINESS_TYPE_WEED				RETURN FACTORY_TYPE_WEED 		BREAK			
		CASE BUSINESS_TYPE_COKE				RETURN FACTORY_TYPE_CRACK 		BREAK				
		CASE BUSINESS_TYPE_METH				RETURN FACTORY_TYPE_METH		BREAK		
		CASE BUSINESS_TYPE_FORGED_DOCS		RETURN FACTORY_TYPE_FAKE_IDS 	BREAK		
		CASE BUSINESS_TYPE_COUNTERFEIT_CASH	RETURN FACTORY_TYPE_FAKE_MONEY 	BREAK
	ENDSWITCH
	
	RETURN FACTORY_TYPE_INVALID
ENDFUNC

FUNC FACTORY_TYPE GET_FACTORY_TYPE_FROM_FACTORY_ID(FACTORY_ID eFactoryID)
	SWITCH eFactoryID
		CASE FACTORY_ID_CRACK_1
		CASE FACTORY_ID_CRACK_2
		CASE FACTORY_ID_CRACK_3
		CASE FACTORY_ID_CRACK_4
			RETURN FACTORY_TYPE_CRACK
		BREAK
		CASE FACTORY_ID_FAKE_CASH_1
		CASE FACTORY_ID_FAKE_CASH_2
		CASE FACTORY_ID_FAKE_CASH_3
		CASE FACTORY_ID_FAKE_CASH_4
			RETURN FACTORY_TYPE_FAKE_MONEY
		BREAK
		CASE FACTORY_ID_FAKE_ID_1
		CASE FACTORY_ID_FAKE_ID_2
		CASE FACTORY_ID_FAKE_ID_3
		CASE FACTORY_ID_FAKE_ID_4
			RETURN FACTORY_TYPE_FAKE_IDS
		BREAK
		CASE FACTORY_ID_METH_1
		CASE FACTORY_ID_METH_2
		CASE FACTORY_ID_METH_3
		CASE FACTORY_ID_METH_4
			RETURN FACTORY_TYPE_METH
		BREAK
		CASE FACTORY_ID_WEED_1
		CASE FACTORY_ID_WEED_2
		CASE FACTORY_ID_WEED_3
		CASE FACTORY_ID_WEED_4
			RETURN FACTORY_TYPE_WEED
		BREAK		
		CASE FACTORY_ID_BUNKER_1
		CASE FACTORY_ID_BUNKER_2
		CASE FACTORY_ID_BUNKER_3
		CASE FACTORY_ID_BUNKER_4
		CASE FACTORY_ID_BUNKER_5
		CASE FACTORY_ID_BUNKER_6
		CASE FACTORY_ID_BUNKER_7
//		CASE FACTORY_ID_BUNKER_8
		CASE FACTORY_ID_BUNKER_9
		CASE FACTORY_ID_BUNKER_10
		CASE FACTORY_ID_BUNKER_11
		CASE FACTORY_ID_BUNKER_12
			RETURN FACTORY_TYPE_WEAPONS
		BREAK
	ENDSWITCH
	
	RETURN FACTORY_TYPE_INVALID
ENDFUNC

FUNC BOOL DOES_PLAYER_OWN_FACTORY_UPGRADE(PLAYER_INDEX playerID, FACTORY_ID eFactoryID, FACTORY_UPGRADE_ID eUpgrade)
	IF DOES_PLAYER_OWN_FACTORY(playerID, eFactoryID)
		INT iSaveSlot = GET_PLAYER_SAVE_SLOT_FOR_FACTORY(playerID, eFactoryID)
		
		RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdFactoryData[iSaveSlot].iFactoryUpgrade, ENUM_TO_INT(eUpgrade))
	ENDIF
	
	CDEBUG3LN(DEBUG_SAFEHOUSE, "[FACTORY] DOES_PLAYER_OWN_FACTORY_UPGRADE: Player ", NATIVE_TO_INT(playerID), " does not own factory ", eFactoryID)
	RETURN FALSE
ENDFUNC

FUNC VECTOR GET_FACTORY_COORDS(FACTORY_ID eFactoryID)
	
	FACTORY_TYPE eGoodsCategory = GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFactoryID)
	
	VECTOR vBikerWHCoords
	SWITCH eGoodsCategory
		CASE FACTORY_TYPE_WEED				vBikerWHCoords = <<1049.6, -3196.6, -38.5>>	BREAK
		CASE FACTORY_TYPE_METH				vBikerWHCoords = <<1009.5, -3196.6, -38.5>>	BREAK
		CASE FACTORY_TYPE_CRACK				vBikerWHCoords = <<1093.6, -3196.6, -38.5>>	BREAK
		CASE FACTORY_TYPE_FAKE_MONEY		vBikerWHCoords = <<1124.6, -3196.6, -38.5>>	BREAK
		CASE FACTORY_TYPE_FAKE_IDS			vBikerWHCoords = <<1165.0, -3196.6, -38.2>>	BREAK
		CASE FACTORY_TYPE_WEAPONS			vBikerWHCoords = <<938.307678, -3196.111572, -100.000000>>	BREAK
	ENDSWITCH
	
	RETURN vBikerWHCoords
ENDFUNC

FUNC STRING GET_FACTORY_INTERIOR_NAME(FACTORY_ID eFactoryID)
	
	FACTORY_TYPE eGoodsCategory = GET_GOODS_CATEGORY_FROM_FACTORY_ID(eFactoryID)
	
	STRING sBikerWHInterior
	SWITCH eGoodsCategory
		CASE FACTORY_TYPE_METH				sBikerWHInterior = "bkr_biker_dlc_int_ware01"	BREAK
		CASE FACTORY_TYPE_WEED				sBikerWHInterior = "bkr_biker_dlc_int_ware02"	BREAK
		CASE FACTORY_TYPE_CRACK				sBikerWHInterior = "bkr_biker_dlc_int_ware03"	BREAK
		CASE FACTORY_TYPE_FAKE_MONEY		sBikerWHInterior = "bkr_biker_dlc_int_ware04"	BREAK
		CASE FACTORY_TYPE_FAKE_IDS			sBikerWHInterior = "bkr_biker_dlc_int_ware05"	BREAK
		CASE FACTORY_TYPE_WEAPONS			sBikerWHInterior = "gr_grdlc_int_02"	BREAK
		
	ENDSWITCH
	
	RETURN sBikerWHInterior
ENDFUNC


PROC ADD_ENTITY_SET(FACTORY_ID factoryID, STRING sEntitySet, BOOL bRefreshInterior = FALSE)
	INTERIOR_INSTANCE_INDEX FactoryInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_FACTORY_COORDS(factoryID), GET_FACTORY_INTERIOR_NAME(factoryID))
	
	IF IS_VALID_INTERIOR(FactoryInteriorID)
		IF NOT IS_STRING_NULL_OR_EMPTY(sEntitySet)
			IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(FactoryInteriorID, sEntitySet)
				ACTIVATE_INTERIOR_ENTITY_SET(FactoryInteriorID, sEntitySet)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "ADD_ENTITY_SET - Activating: ", sEntitySet)
			ENDIF
		ENDIF
	ENDIF
	
	IF bRefreshInterior
		REFRESH_INTERIOR(FactoryInteriorID)
	ENDIF
	
ENDPROC

PROC REMOVE_ENTITY_SET(FACTORY_ID factoryID, STRING sEntitySet, BOOL bRefreshInterior = FALSE)
	
	INTERIOR_INSTANCE_INDEX FactoryInteriorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_FACTORY_COORDS(factoryID), GET_FACTORY_INTERIOR_NAME(factoryID))
	
	IF IS_VALID_INTERIOR(FactoryInteriorID)
		IF NOT IS_STRING_NULL_OR_EMPTY(sEntitySet)
			IF IS_INTERIOR_ENTITY_SET_ACTIVE(FactoryInteriorID, sEntitySet)
				DEACTIVATE_INTERIOR_ENTITY_SET(FactoryInteriorID, sEntitySet)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "REMOVE_ENTITY_SET - Deactivating: ", sEntitySet)
			ENDIF
		ENDIF
	ENDIF
	
	IF bRefreshInterior
		REFRESH_INTERIOR(FactoryInteriorID)
	ENDIF
	
ENDPROC

PROC _BIKER_APPLY_METH_LAB_PRE_SETUP_ENTITY_SETS(FACTORY_ID factoryID, PLAYER_INDEX pFactoryOwner, BOOL bRefreshInterior = FALSE, BOOL bForMission = FALSE)
	TEXT_LABEL_63 tlMethSecurity
	IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pFactoryOwner, factoryID, UPGRADE_ID_SECURITY)
	AND NOT bForMission
		tlMethSecurity = "meth_lab_security_high"
	ENDIF
	
	ADD_ENTITY_SET(factoryID, tlMethSecurity)
	ADD_ENTITY_SET(factoryID, "meth_lab_empty", bRefreshInterior)
ENDPROC

PROC _BIKER_APPLY_FAKE_ID_PRE_SETUP_ENTITY_SETS(FACTORY_ID factoryID, PLAYER_INDEX pFactoryOwner, BOOL bRefreshInterior = FALSE)
	TEXT_LABEL_63 tlFakeIDStaff
	tlFakeIDStaff = "interior_basic"
	IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pFactoryOwner, factoryID, UPGRADE_ID_EQUIPMENT)
		tlFakeIDStaff = "interior_upgrade"
	ENDIF
	
	TEXT_LABEL_63 tlFakeIDSecurity
	tlFakeIDSecurity = "security_low"
	IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pFactoryOwner, factoryID, UPGRADE_ID_SECURITY)
		tlFakeIDSecurity = "security_high"
	ENDIF
	
	ADD_ENTITY_SET(factoryID, tlFakeIDStaff)
	ADD_ENTITY_SET(factoryID, tlFakeIDSecurity)
	ADD_ENTITY_SET(factoryID, "Chair01")
	ADD_ENTITY_SET(factoryID, "Chair02")
	ADD_ENTITY_SET(factoryID, "Chair03")
	ADD_ENTITY_SET(factoryID, "Chair04")
	ADD_ENTITY_SET(factoryID, "Chair05")
	ADD_ENTITY_SET(factoryID, "Chair06")
	ADD_ENTITY_SET(factoryID, "Chair07", bRefreshInterior)
ENDPROC

PROC _BIKER_APPLY_WEED_PRE_SETUP_ENTITY_SETS(FACTORY_ID factoryID, PLAYER_INDEX pFactoryOwner, BOOL bRefreshInterior = FALSE, BOOL bForMission = FALSE)
	//Weed
	TEXT_LABEL_63 tlWeedEquip 
	tlWeedEquip	= "weed_standard_equip"
	IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pFactoryOwner, factoryID, UPGRADE_ID_EQUIPMENT)
	AND NOT bForMission
		tlWeedEquip = "weed_upgrade_equip"
	ENDIF
	
	TEXT_LABEL_63 tlWeedSecurity
	tlWeedSecurity = "weed_low_security"
	IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pFactoryOwner, factoryID, UPGRADE_ID_SECURITY)
	AND NOT bForMission
		tlWeedSecurity = "weed_security_upgrade"
	ELSE
		REMOVE_ENTITY_SET(factoryID, "weed_security_upgrade")
	ENDIF
	
	ADD_ENTITY_SET(factoryID, "weed_chairs")
	ADD_ENTITY_SET(factoryID, tlWeedEquip)
	ADD_ENTITY_SET(factoryID, tlWeedSecurity, bRefreshInterior)
ENDPROC

PROC _BIKER_APPLY_COKE_LOCKUP_PRE_SETUP_ENTITY_SETS(FACTORY_ID factoryID, PLAYER_INDEX pFactoryOwner, BOOL bRefreshInterior = FALSE, BOOL bForMission = FALSE)
	TEXT_LABEL_63 tlCrackEquip
	tlCrackEquip = "equipment_basic"
	IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pFactoryOwner, factoryID, UPGRADE_ID_EQUIPMENT)
	AND NOT bForMission
		tlCrackEquip = "equipment_upgrade"
	ENDIF
	
	TEXT_LABEL_63 tlCrackSecurity
	tlCrackSecurity = "security_low"
	IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pFactoryOwner, factoryID, UPGRADE_ID_SECURITY)
	AND NOT bForMission
		tlCrackSecurity = "security_high"
	ENDIF
	
	ADD_ENTITY_SET(factoryID, tlCrackEquip)
	ADD_ENTITY_SET(factoryID, tlCrackSecurity, bRefreshInterior)
ENDPROC

PROC _BIKER_APPLY_FAKE_CASH_PRE_SETUP_ENTITY_SETS(FACTORY_ID factoryID, PLAYER_INDEX pFactoryOwner, BOOL bRefreshInterior = FALSE, BOOL bActiveState = FALSE)
	IF NOT bActiveState
		TEXT_LABEL_63 tlCashEquipNoProd
		tlCashEquipNoProd = "counterfeit_standard_equip_no_prod"
		IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pFactoryOwner, factoryID, UPGRADE_ID_EQUIPMENT)
			tlCashEquipNoProd = "counterfeit_upgrade_equip_no_prod"
		ENDIF
		ADD_ENTITY_SET(factoryID, tlCashEquipNoProd)
	ELSE
		TEXT_LABEL_63 tlCashEquip
		tlCashEquip = "counterfeit_standard_equip"
		IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pFactoryOwner, factoryID, UPGRADE_ID_EQUIPMENT)
			tlCashEquip = "counterfeit_upgrade_equip"
		ENDIF
		ADD_ENTITY_SET(factoryID, tlCashEquip)
	ENDIF
	
	TEXT_LABEL_63 tlCashSecurity
	tlCashSecurity = "counterfeit_low_security"
	IF DOES_PLAYER_OWN_FACTORY_UPGRADE(pFactoryOwner, factoryID, UPGRADE_ID_SECURITY)
		tlCashSecurity = "counterfeit_security"
	ENDIF
	ADD_ENTITY_SET(factoryID, tlCashSecurity)
	ADD_ENTITY_SET(factoryID, "money_cutter")
	ADD_ENTITY_SET(factoryID, "special_chairs", bRefreshInterior)
ENDPROC

PROC APPLY_BIKER_FACTORY_NON_SETUP_ENTITY_SETS(FACTORY_ID factoryID, PLAYER_INDEX pFactoryOwner)
	
	SWITCH factoryID
		CASE FACTORY_ID_METH_1
		CASE FACTORY_ID_METH_2
		CASE FACTORY_ID_METH_3
		CASE FACTORY_ID_METH_4
			_BIKER_APPLY_METH_LAB_PRE_SETUP_ENTITY_SETS(factoryID, pFactoryOwner, TRUE)
		BREAK
		CASE FACTORY_ID_FAKE_ID_1
		CASE FACTORY_ID_FAKE_ID_2
		CASE FACTORY_ID_FAKE_ID_3
		CASE FACTORY_ID_FAKE_ID_4
			_BIKER_APPLY_FAKE_ID_PRE_SETUP_ENTITY_SETS(factoryID, pFactoryOwner, TRUE)
		BREAK
		CASE FACTORY_ID_WEED_1
		CASE FACTORY_ID_WEED_2
		CASE FACTORY_ID_WEED_3
		CASE FACTORY_ID_WEED_4
			_BIKER_APPLY_WEED_PRE_SETUP_ENTITY_SETS(factoryID, pFactoryOwner, TRUE)
		BREAK
		CASE FACTORY_ID_CRACK_1
		CASE FACTORY_ID_CRACK_2
		CASE FACTORY_ID_CRACK_3
		CASE FACTORY_ID_CRACK_4
			_BIKER_APPLY_COKE_LOCKUP_PRE_SETUP_ENTITY_SETS(factoryID, pFactoryOwner, TRUE)
		BREAK
		CASE FACTORY_ID_FAKE_CASH_1
		CASE FACTORY_ID_FAKE_CASH_2
		CASE FACTORY_ID_FAKE_CASH_3
		CASE FACTORY_ID_FAKE_CASH_4
			_BIKER_APPLY_FAKE_CASH_PRE_SETUP_ENTITY_SETS(factoryID, pFactoryOwner, TRUE)
		BREAK
	ENDSWITCH
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ HANGARS ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL HAS_PLAYER_COMPLETED_HANGAR_SETUP_MISSION(PLAYER_INDEX playerID)
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.bdHangarData.bSetupComplete
ENDFUNC

FUNC BOOL DOES_LP_OWN_HANGAR_SUITABLE_FOR_DAILY_OBJ()
	IF (INT_TO_ENUM(HANGAR_ID, GET_MP_INT_CHARACTER_STAT(MP_STAT_HANGAR_OWNED)) != HANGAR_ID_INVALID)
		IF GET_MP_INT_CHARACTER_STAT(MP_STAT_HANGAR_CONTRABAND_TOTAL) != ciMAX_HANGAR_PRODUCT_UNITS
			RETURN TRUE		
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════════╡ DEBUG ╞══════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
PROC DEBUG_DRAW_PRODUCT_BAR(INT iBarValue, FLOAT fBarPercent, FLOAT fBarYPos, FLOAT fBarMaxMidPoint, FLOAT fMaxWidth, INT iBGBarRGB, FLOAT fBarHeight, INT R, INT G, INT B)
	//Calculate the mid point and width of the bar
	FLOAT fWidth 	= (fMaxWidth * fBarPercent)
	FLOAT fBarXMid 	= (fBarMaxMidPoint - ((fMaxWidth - fWidth) / 2))
	
	//Product background
	DRAW_RECT(fBarMaxMidPoint, fBarYPos, fMaxWidth, fBarHeight, iBGBarRGB, iBGBarRGB, iBGBarRGB, 100)
	//Product total rectangle
	IF iBarValue > 0
		DRAW_RECT(fBarXMid, fBarYPos, fWidth, 0.005, R, G, B, 255)
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns time in the format 00:00:00 for the given time passed in ms
FUNC TEXT_LABEL_15 GET_TIME_AS_TEXT_LABEL(INT iTimeInMS)
	
	TEXT_LABEL_15 tlTime
	
	IF iTimeInMS < 0
		tlTime = "00:00:00"
		RETURN tlTime
	ENDIF
	
	FLOAT 	fSeconds 	= (TO_FLOAT(iTimeInMS)/1000)
	INT 	iSeconds 	= FLOOR(fSeconds % 60)
	INT 	iMinutes 	= FLOOR(fSeconds/60)
	INT 	iHours		= FLOOR(TO_FLOAT(iMinutes)/60)
	INT 	iDays		= FLOOR(TO_FLOAT(iHours)/24)
	
	WHILE (iMinutes >= 60)
		iMinutes -= 60
	ENDWHILE
	
	IF iDays > 0
		IF iDays < 10
			tlTime += "0"
		ENDIF
		
		tlTime += iDays
		tlTime += ":"
		
		WHILE (iHours >= 24)
			iHours -= 24
		ENDWHILE
	ENDIF
	
	IF iHours < 10
		tlTime += "0"
	ENDIF
	
	tlTime += iHours
	tlTime += ":"
	
	IF iMinutes < 10
		tlTime += "0"
	ENDIF
	
	tlTime += iMinutes
	tlTime += ":"
	
	IF iSeconds < 10
		tlTime += "0"
	ENDIF
	
	tlTime += iSeconds
	
	RETURN tlTime
ENDFUNC
#ENDIF
