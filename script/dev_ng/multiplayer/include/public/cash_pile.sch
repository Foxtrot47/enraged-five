USING "globals.sch"

/// PURPOSE: How many cash objects can be spawned
CONST_INT _CASH_PILE_MAX_CASH_OBJECTS 48

// PURPOSE: How much each cash object is worth
CONST_INT _CASH_PILE_AMOUNT_PER_OBJECT 1000

// PURPOSE:
CONST_INT _CASH_PILE_FADE_TIMER 500

/// PURPOSE: 
///    States for running the cash pile state machine
ENUM _CASH_PILE_STATE_ENUM
	_CPS_IDLE,
	_CPS_CREATE_OBJECTS,
	_CPS_FADE_IN_OBJECTS,
	_CPS_FADE_OUT_OBJECTS,
	_CPS_CLEANUP_OBJECTS
ENDENUM

/// PURPOSE:
///    Gets the cash pile state enum as a string for debug
/// RETURNS:
///    The cash pile state enum as a string for debug
DEBUGONLY FUNC STRING _GET_CASH_PILE_STATE_ENUM_AS_STRING(_CASH_PILE_STATE_ENUM eEnum)
	SWITCH eEnum
		CASE _CPS_IDLE				RETURN	"_CPS_IDLE"
		CASE _CPS_CREATE_OBJECTS	RETURN	"_CPS_CREATE_OBJECTS"
		CASE _CPS_FADE_IN_OBJECTS	RETURN	"_CPS_FADE_IN_OBJECTS"
		CASE _CPS_FADE_OUT_OBJECTS	RETURN	"_CPS_FADE_OUT_OBJECTS"
		CASE _CPS_CLEANUP_OBJECTS	RETURN	"_CPS_CLEANUP_OBJECTS"
	ENDSWITCH

	ASSERTLN("GET__CASH_PILE_STATE_ENUM_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC

TYPEDEF FUNC VECTOR _GET_CASH_VECTOR_FUNC(INT iCashindex)
TYPEDEF FUNC MODEL_NAMES _GET_CASH_MODEL_FUNC(INT iCashindex)

/// PURPOSE: 
///    Data needed for running a cash pile instance
STRUCT CASH_PILE_STRUCT
	BOOL bInitialized = FALSE
	INT iShownCashObjects = 0
	
	TIME_DATATYPE tdCashFadeTimer
	OBJECT_INDEX objSafeCash[_CASH_PILE_MAX_CASH_OBJECTS]
	
	_GET_CASH_VECTOR_FUNC funcCashPositionLookUp
	_GET_CASH_VECTOR_FUNC funcCashRotationLookUp
	_GET_CASH_MODEL_FUNC funcCashModelLookup
	
	_CASH_PILE_STATE_ENUM eState
ENDSTRUCT

/// PURPOSE:
///    Sets the fade timer ready for a new countdown
PROC _CASH_PILE__SET_FADE_TIMER(CASH_PILE_STRUCT &sInst)
	sInst.tdCashFadeTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), NIGHTCLUB_SAFE_CASH_FADE_TIMER)
ENDPROC

/// PURPOSE:
///    Determines how many cash objects need to be placed according to how much each cash stack is worth
/// PARAMS:
///    iCashAmount - the amount of cash to visualise
/// RETURNS:
///    How many objects to display to represent the amount, clamped to a max of _CASH_PILE_MAX_CASH_OBJECTS
FUNC INT _CASH_PILE_GET_REQUIRED_OBJECTS_FOR_CASH_AMOUNT(INT iCashAmount)
	INT iAmount = IMIN(iCashAmount / _CASH_PILE_AMOUNT_PER_OBJECT, _CASH_PILE_MAX_CASH_OBJECTS)
	
	// If there is any money at least show one cash stack
	IF iCashAmount > 0
		RETURN IMAX(1, iAmount)
	ENDIF
	
	RETURN iAmount
ENDFUNC

/// PURPOSE:
///    Updates the idle state for the cash pile,
///    if the cash amount passed in changes it will handle adding or removing the cash objects
/// PARAMS:
///    iCashAmount - how much cash should be shown in the pile
PROC _CASH_PILE__UPDATE_IDLE(CASH_PILE_STRUCT &sInst, INT iCashAmount)
	INT iCurrentCashObjectsShown = sInst.iShownCashObjects
	INT iRequiredCashObjects = _CASH_PILE_GET_REQUIRED_OBJECTS_FOR_CASH_AMOUNT(iCashAmount)
	
	IF iCurrentCashObjectsShown < iRequiredCashObjects
		PRINTLN("[CASH_PILE] Transition IDLE -> CREATE")
		sInst.eState = _CPS_CREATE_OBJECTS
		EXIT
	ENDIF
	
	IF iCurrentCashObjectsShown > iRequiredCashObjects
		PRINTLN("[CASH_PILE] Transition IDLE -> FADE_OUT")
		_CASH_PILE__SET_FADE_TIMER(sInst)
		sInst.eState = _CPS_FADE_OUT_OBJECTS
		EXIT
	ENDIF	
ENDPROC

/// PURPOSE:
///    Creates a cash object for the given cash index using the defined lookup tables
///    cash is spawned invisible so it can be faded in later
/// PARAMS:
///    iCashIndex - the index of the cash object to create
/// RETURNS:
///    TRUE if the cash object has been created
FUNC BOOL _CASH_PILE__CREATE_CASH_OBJECT(CASH_PILE_STRUCT &sInst, INT iCashIndex)
	IF DOES_ENTITY_EXIST(sInst.objSafeCash[iCashIndex])
		RETURN TRUE
	ENDIF
	
	MODEL_NAMES eCashModel = CALL sInst.funcCashModelLookup(iCashIndex)
	
	REQUEST_MODEL(eCashModel)
	IF NOT HAS_MODEL_LOADED(eCashModel)
		PRINTLN("[CASH_PILE] CREATE_CASH_OBJECT - model not loaded: ", eCashModel)
		RETURN FALSE
	ENDIF
	
	VECTOR vPosition = CALL sInst.funcCashPositionLookUp(iCashIndex)
	VECTOR vRotation = CALL  sInst.funcCashRotationLookUp(iCashIndex)
	
	sInst.objSafeCash[iCashIndex] = CREATE_OBJECT(eCashModel, vPosition, FALSE, FALSE)
	FREEZE_ENTITY_POSITION(sInst.objSafeCash[iCashIndex], TRUE)
	SET_ENTITY_COORDS(sInst.objSafeCash[iCashIndex], vPosition)
	SET_ENTITY_ROTATION(sInst.objSafeCash[iCashIndex], vRotation)
	SET_ENTITY_ALPHA(sInst.objSafeCash[iCashIndex], 0, FALSE)
	SET_MODEL_AS_NO_LONGER_NEEDED(eCashModel)
	PRINTLN("[CASH_PILE] CREATE_CASH_OBJECT - Created cash object ", iCashIndex)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Updates creating the required amount of cash objects for the cash amount
///    cash is spawned invisible so it can be faded in later
/// PARAMS:
///    iCashAmount - how much cash to show in the pile
PROC _CASH_PILE__UPDATE_CREATE_CASH_OBJECTS(CASH_PILE_STRUCT &sInst, INT iCashAmount)	
	INT i = 0
	INT iCreatedObjects = sInst.iShownCashObjects
	INT iRequiredCashObjects = _CASH_PILE_GET_REQUIRED_OBJECTS_FOR_CASH_AMOUNT(iCashAmount)
	INT iCashObjectsLeftToCreate = iRequiredCashObjects - sInst.iShownCashObjects
	
	PRINTLN("[CASH_PILE] UPDATE_CREATE_OBJECTS - START LOOP ",
	"already shown objects:  ", sInst.iShownCashObjects, 
	" iCreatedObjects: ", iCreatedObjects,
	" iRequiredCashObjects: ", iRequiredCashObjects,
	" iCashObjectsLeftToCreate: ", iCashObjectsLeftToCreate)
	
	REPEAT iCashObjectsLeftToCreate i 
		// Append new object on to end of currently shown
		IF _CASH_PILE__CREATE_CASH_OBJECT(sInst, sInst.iShownCashObjects + i)
			iCreatedObjects++
		ENDIF
		
		PRINTLN("[CASH_PILE] UPDATE_CREATE_OBJECTS - i: ", i,
		" iCreatedObjects: ", iCreatedObjects)
	ENDREPEAT	

	IF iCreatedObjects >= iRequiredCashObjects
		PRINTLN("[CASH_PILE] Transition CREATE -> FADE_IN")
		_CASH_PILE__SET_FADE_TIMER(sInst)
		sInst.eState = _CPS_FADE_IN_OBJECTS
		EXIT
	ENDIF	
ENDPROC

/// PURPOSE:
///    Checks if the cash index is valid
/// RETURNS:
///    TRUE if the cash index is valid
FUNC BOOL _CASH_PILE__IS_CASH_INDEX_VALID(INT iCashIndex)
	RETURN iCashIndex < _CASH_PILE_MAX_CASH_OBJECTS AND iCashIndex >= 0
ENDFUNC

/// PURPOSE:
///    Updates fading in the required amount of cash
/// PARAMS:
///    iCashIndex - the index of the cash object to fade in
///    iAlpha - the alpha to set the cash to
/// RETURNS:
///    TRUE if the object has finished fade in
FUNC BOOL _CASH_PILE__UPDATE_CASH_OBJECT_FADE_IN(CASH_PILE_STRUCT &sInst, INT iCashIndex, INT iAlpha)
	IF NOT _CASH_PILE__IS_CASH_INDEX_VALID(iCashIndex)
		RETURN TRUE// Skip over
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(sInst.objSafeCash[iCashIndex])
		RETURN TRUE
	ENDIF
	
	IF GET_ENTITY_ALPHA(sInst.objSafeCash[iCashIndex]) >= 255
		RETURN TRUE
	ENDIF
	
	SET_ENTITY_ALPHA(sInst.objSafeCash[iCashIndex],  iAlpha, FALSE)
	PRINTLN("[CASH_PILE] UPDATE_CASH_OBJECT_FADE_IN - Fading in cash object ", iCashIndex, " alpha: ", iAlpha)
	
	IF iAlpha >= 255
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Calculates what the current fade alpha should be based on the current fade time
/// PARAMS:
///    bFadeIn - if we are fading in
/// RETURNS:
///    The alpha for the current fade time
FUNC INT _CASH_PILE__CALCULATE_ALPHA(CASH_PILE_STRUCT &sInst, BOOL bFadeIn)
	INT iTimeDifference = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sInst.tdCashFadeTimer))
	INT iAlpha = ROUND((TO_FLOAT(iTimeDifference) / _CASH_PILE_FADE_TIMER) * 255)
	
	IF bFadeIn
		iAlpha = (255 - (ROUND((TO_FLOAT(iTimeDifference) / _CASH_PILE_FADE_TIMER) * 255)))
	ENDIF
	RETURN CLAMP_INT(iAlpha, 0, 255)
ENDFUNC

/// PURPOSE:
///    Updates the state for fading in all the required cash objects
/// PARAMS: 
///    iCashAmount - how much cash to show in the pile
PROC _CASH_PILE__UPDATE_FADE_IN_CASH_OBJECTS(CASH_PILE_STRUCT &sInst, INT iCashAmount)
	INT iAlpha = _CASH_PILE__CALCULATE_ALPHA(sInst, TRUE)
	INT iRequiredCashObjects = _CASH_PILE_GET_REQUIRED_OBJECTS_FOR_CASH_AMOUNT(iCashAmount)
	
	// If time is up they should all be faded in but to be sure force alpha to 255
	IF NOT IS_TIME_LESS_THAN(GET_NETWORK_TIME(), sInst.tdCashFadeTimer)
		PRINTLN("[CASH_PILE] Transition FADE_IN -> IDLE")
		sInst.eState = _CPS_IDLE
		iAlpha = 255
		sInst.iShownCashObjects = iRequiredCashObjects
	ENDIF
	
	INT i
	REPEAT iRequiredCashObjects i 
		_CASH_PILE__UPDATE_CASH_OBJECT_FADE_IN(sInst, i, iAlpha)
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Handles fading out a cash object, optionally deleting it when finishing
/// PARAMS:
///    iCashIndex - index of the cash object to fade
///    iAlpha - the alpha to se the object to
///    bDeleteOnFade - if the object should be deleted on fade
/// RETURNS:
///    TRUE if the object has faded out completely
FUNC BOOL _CASH_PILE__UPDATE_CASH_OBJECT_FADE_OUT(CASH_PILE_STRUCT &sInst, INT iCashIndex, INT iAlpha, BOOL bDeleteOnFade)
	IF NOT _CASH_PILE__IS_CASH_INDEX_VALID(iCashIndex)
		RETURN TRUE// Skip over
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(sInst.objSafeCash[iCashIndex])
		RETURN TRUE
	ENDIF
	
	SET_ENTITY_ALPHA(sInst.objSafeCash[iCashIndex], iAlpha, FALSE)
	PRINTLN("[CASH_PILE] UPDATE_CASH_OBJECT_FADE_OUT - Fading out cash object ", iCashIndex, " alpha: ", iAlpha)
	
	IF iAlpha <= 0
		IF bDeleteOnFade
			DELETE_OBJECT(sInst.objSafeCash[iCashIndex])
			sInst.iShownCashObjects--
			PRINTLN("[CASH_PILE] UPDATE_CASH_OBJECT_FADE_OUT - Deleted cash object ", iCashIndex)
		ENDIF
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles fading out all cash objects that aren't need to show the cash value
/// PARAMS:
///    bDeleteOnFade - if the cash objects should delete when finished fading
///    iCashAmount - the cash amount to show
/// RETURNS:
///    TRUE if the cash has finished fading
FUNC BOOL _CASH_PILE__FADE_OUT_ALL_CASH_OBJECTS(CASH_PILE_STRUCT &sInst, BOOL bDeleteOnFade, INT iCashAmount)
	INT iAlpha = _CASH_PILE__CALCULATE_ALPHA(sInst, FALSE)
	
	IF NOT IS_TIME_LESS_THAN(GET_NETWORK_TIME(), sInst.tdCashFadeTimer)
		PRINTLN("[CASH_PILE] FADE_OUT_ALL_CASH_OBJECTS - fade time finished")
		iAlpha = 0
	ENDIF
	
	INT iRequiredCashObjects = _CASH_PILE_GET_REQUIRED_OBJECTS_FOR_CASH_AMOUNT(iCashAmount)
	INT iMaxPossibleCash = COUNT_OF(sInst.objSafeCash)
	// Only loop added amount but never exceeed max possible
	INT iCurrentCashObjectsShown = IMIN(iMaxPossibleCash, sInst.iShownCashObjects - 1)
	INT i = 0
	
	PRINTLN("[CASH_PILE] FADE_OUT_ALL_CASH_OBJECTS - current objects shown: ", iCurrentCashObjectsShown)
	
	// Decrement so we don't waste time on objects that are already faded
	FOR i = iCurrentCashObjectsShown TO 0 STEP -1
		PRINTLN("[CASH_PILE] FADE_OUT_ALL_CASH_OBJECTS - fading out: ", i)
		IF _CASH_PILE__UPDATE_CASH_OBJECT_FADE_OUT(sInst, i, iAlpha,bDeleteOnFade)
			
			// Don't fade the rest as they are still needed
			IF sInst.iShownCashObjects = iRequiredCashObjects
				PRINTLN("[CASH_PILE] FADE_OUT_ALL_CASH_OBJECTS - required amount shown")
 				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	IF iAlpha <= 0
		sInst.iShownCashObjects = 0
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC 

/// PURPOSE:
///    Updates the state for fading out cash objects 
/// PARAMS:
///    iCashAmount - the amount of cash to show in the pile
PROC _CASH_PILE__UPDATE_FADE_OUT_CASH_OBJECTS(CASH_PILE_STRUCT &sInst, INT iCashAmount)
	IF _CASH_PILE__FADE_OUT_ALL_CASH_OBJECTS(sInst, FALSE, iCashAmount)
		sInst.eState = _CPS_IDLE
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the state for cleaning up cash objects 
/// PARAMS:
///    iCashAmount - the amount of cash to show in the pile
PROC _CASH_PILE__UPDATE_CLEANUP_CASH_OBJECTS(CASH_PILE_STRUCT &sInst, INT iCashAmount)	
	IF _CASH_PILE__FADE_OUT_ALL_CASH_OBJECTS(sInst, TRUE, iCashAmount)
		sInst.eState = _CPS_IDLE
	ENDIF
ENDPROC

/// PURPOSE:
///    Initializes the cash pile with the position, rotation, and model lookups
///    must be called before you can update the cash pile
/// PARAMS:
///    positionLookup - lookup of cash positions
///    rotationLookups - lookup of cash rotations
///    modelLookup - lookup of models to use
PROC INIT_CASH_PILE(CASH_PILE_STRUCT &sInst, _GET_CASH_VECTOR_FUNC positionLookup,
_GET_CASH_VECTOR_FUNC rotationLookups, _GET_CASH_MODEL_FUNC modelLookup)
	sInst.funcCashPositionLookUp = positionLookup
	sInst.funcCashRotationLookUp = rotationLookups
	sInst.funcCashModelLookup = modelLookup
	sInst.bInitialized = TRUE
ENDPROC

/// PURPOSE:
///    Cleans up the cash pile
PROC CLEANUP_CASH_PILE(CASH_PILE_STRUCT &sInst)	
	sInst.iShownCashObjects = 0
	sInst.eState = _CPS_IDLE
	
	INT i
	INT iMaxIndex = COUNT_OF(sInst.objSafeCash) - 1
	REPEAT iMaxIndex i
		IF DOES_ENTITY_EXIST(sInst.objSafeCash[i])
			DELETE_OBJECT(sInst.objSafeCash[i])
		ENDIF
	ENDREPEAT	
ENDPROC

/// PURPOSE:
///    Draws debug info for the cash pile if sc_ArcadeCashDebug exists in the commandline
DEBUGONLY PROC _CASH_PILE_DEBUG_DRAW_INFO(CASH_PILE_STRUCT &sInst, INT iCashAmount)
	#IF IS_DEBUG_BUILD
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_ArcadeCashDebug")
		EXIT
	ENDIF
	#ENDIF
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	DRAW_DEBUG_TEXT_2D(_GET_CASH_PILE_STATE_ENUM_AS_STRING(sInst.eState), <<0.1, 0.1, 0>>, 255, 255, 255)
	
	TEXT_LABEL_63 tlShown = "Shown: "
	tlShown += sInst.iShownCashObjects
	DRAW_DEBUG_TEXT_2D(tlShown, <<0.1, 0.15, 0>>, 255, 255, 255)
	
	TEXT_LABEL_63 tlRequired = "Required objects: "
	tlRequired += _CASH_PILE_GET_REQUIRED_OBJECTS_FOR_CASH_AMOUNT(iCashAmount)
	DRAW_DEBUG_TEXT_2D(tlRequired, <<0.1, 0.20, 0>>, 255, 255, 255)
	
	TEXT_LABEL_63 tlCashAmount = "Cash Amount: "
	tlCashAmount += iCashAmount
	DRAW_DEBUG_TEXT_2D(tlCashAmount, <<0.1, 0.25, 0>>, 255, 255, 255)
ENDPROC

/// PURPOSE:
///    Updates the cash pile so that it shows the given amount
///    will not update if INIT_CASH_PILE has not been called
/// PARAMS:
///    sInst - the instance
///    iCashAmount - the amount of cash to show
PROC MAINTAIN_CASH_PILE(CASH_PILE_STRUCT &sInst, INT iCashAmount)	
	IF NOT sInst.bInitialized
		PRINTLN("[CASH_PILE] MAINTAIN_CASH_PILE - Not initialized")
		EXIT
	ENDIF
	
	_CASH_PILE_DEBUG_DRAW_INFO(sInst, iCashAmount)
	
	SWITCH sInst.eState
		CASE _CPS_IDLE				_CASH_PILE__UPDATE_IDLE(sInst, iCashAmount)				BREAK
		CASE _CPS_CREATE_OBJECTS	_CASH_PILE__UPDATE_CREATE_CASH_OBJECTS(sInst, iCashAmount)	BREAK
		CASE _CPS_FADE_IN_OBJECTS	_CASH_PILE__UPDATE_FADE_IN_CASH_OBJECTS(sInst, iCashAmount)	BREAK
		CASE _CPS_FADE_OUT_OBJECTS	_CASH_PILE__UPDATE_FADE_OUT_CASH_OBJECTS(sInst, iCashAmount)	BREAK
		CASE _CPS_CLEANUP_OBJECTS	_CASH_PILE__UPDATE_CLEANUP_CASH_OBJECTS(sInst, iCashAmount)	BREAK
	ENDSWITCH		
ENDPROC
