USING "rage_builtins.sch"
USING "globals.sch"
USING "title_update_globals.sch"

USING "script_player.sch"
USING "net_mission.sch"
USING "net_mission_control_public.sch"
USING "net_player_headshots.sch"
USING "net_mission_joblist_public.sch"
USING "net_missions_at_coords_public.sch"
USING "net_realty_new.sch"
USING "net_contact_requests.sch"
USING "transition_common.sch"
USING "heist_island_loading.sch"
USING "net_fmc_invites.sch"

// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Mission_JobList.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all functions to create and maintain a player-specific JobList to be used by the MP JobList app.
//
//      NOTES           :   INVITES WITH ON/OFF TIMES - KGM 26/8/13
//                          These will remain available until the Off time after which they will be displayed as 'closed' for a
//                          Game Hour before being deleted - unless they were never displayed in which case they'll just get deleted.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// Joblist Update CONSTS
CONST_INT   NEXT_JOBLIST_UPDATE_TIME_msec       1000        // The regular timer interval to check if the joblist needs updated for this player or not


// ----------------------------------------------------------------------------

// External Access CONST - hopefully temp while we're dealing with FM joblists with largely separate routines from CnC
CONST_INT   NO_FM_JOINABLE_JOBS                 -1          // Used when returning info to DaveW from the Get_A_Joinable_FM_Mission_Type() routine 


// ----------------------------------------------------------------------------

// STRUCT used to check if the JobList array has been changed during an update
STRUCT m_sCopyJobList
    g_structMissionJobListMP    sJobList[MAX_MP_JOBLIST_ENTRIES]
    INT                         numEntries  = 0
ENDSTRUCT


// ===========================================================================================================
//      Broadcast Events
// ===========================================================================================================

/// PURPOSE: Broadcast to all other players that are in the same vehicle as me that I've accepted an invite and have been warped
PROC BROADCAST_EVENT_PLAYER_WARPED_BECAUSE_OF_INVITE(INT iMissionType = -1, STRING stMissionName = NULL, INT iSubMissionType = 0)    

	IF NOT (NETWORK_IS_GAME_IN_PROGRESS())
        #IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [JobList]: BROADCAST_EVENT_PLAYER_WARPED_BECAUSE_OF_INVITE - IGNORED - Player is in SP.")
		#ENDIF
		
		EXIT
	ENDIF

    EVENT_STRUCT_PLAYER_WARPED_BECAUSE_OF_INVITE    Event   
    INT                         iSendTo
    VEHICLE_INDEX               vehID
    
    //If the player is ok
    IF IS_NET_PLAYER_OK(PLAYER_ID())
        //And they are in a vehicle
        IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
            //And it's drivable
            vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
            IF IS_VEHICLE_DRIVEABLE(vehID)
                //Then send to all players in my vehicle
                iSendTo = ALL_PLAYERS_IN_VEHICLE(vehID, FALSE)
                
                //set up the event
                Event.Details.Type              = SCRIPT_EVENT_PLAYER_WARPED_BECAUSE_OF_INVITE
                Event.Details.FromPlayerIndex   = PLAYER_ID()
                Event.tl63MissionName           = stMissionName 
                Event.iMissionType              = iMissionType
                Event.iSubMissionType           = iSubMissionType
                
                IF (iSendTo != 0)   
                    #IF IS_DEBUG_BUILD
                        PRINTLN("KGM MP... [TS] BROADCAST_EVENT_PLAYER_WARPED_BECAUSE_OF_INVITE - called...")
                        PRINTLN("                   From script: ", GET_THIS_SCRIPT_NAME())
                        PRINTLN("                 stMissionName: ", stMissionName)
                        PRINTLN("                  iMissionType: ", iMissionType)
                        PRINTLN("                       iSendTo: ", iSendTo)
                        Debug_Output_Player_Names_From_Bitfield(iSendTo)
                    #ENDIF
                    SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
                #IF IS_DEBUG_BUILD
                ELSE
                    NET_PRINT("BROADCAST_EVENT_PLAYER_WARPED_BECAUSE_OF_INVITE - playerflags = 0 so not broadcasting") NET_NL()
                #ENDIF
                ENDIF
                
                EXIT
            ENDIF   
        ENDIF   
    ENDIF   
ENDPROC

//get the mini game type
FUNC STRING GET_FMMC_MINI_GAME_STRING(INT iMissionType)
    SWITCH iMissionType
        CASE FMMC_TYPE_MG_TENNIS            RETURN "FMMC_MPM_TY9" //Tennis
        CASE FMMC_TYPE_MG_ARM_WRESTLING     RETURN "FMMC_MPM_TY10" //Arm Wrestling
        CASE FMMC_TYPE_MG_DARTS             RETURN "FMMC_MPM_TY11" //Darts
        CASE FMMC_TYPE_MG_SHOOTING_RANGE    RETURN "FMMC_MPM_TY12" //Shooting Range
        CASE FMMC_TYPE_MG_GOLF              RETURN "FMMC_MPM_TY13" //Golf
        CASE FMMC_TYPE_MG_PILOT_SCHOOL      RETURN "FMMC_MPM_TY122" //Pilot School
    ENDSWITCH
    RETURN ""
ENDFUNC
//Process the event
PROC PROCESS_EVENT_PLAYER_WARPED_BECAUSE_OF_INVITE(INT iEventID)    
    PRINTLN("KGM MP... [TS] PROCESS_EVENT_PLAYER_WARPED_BECAUSE_OF_INVITE - called...")
    EVENT_STRUCT_PLAYER_WARPED_BECAUSE_OF_INVITE    Event           
    IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
        PRINTLN("KGM MP... [TS] PROCESS_EVENT_PLAYER_WARPED_BECAUSE_OF_INVITE - Event.iMissionType = ", Event.iMissionType)
        //If it's a Job
        IF Event.iMissionType < FMMC_TYPE_MG
       		IF Event.iSubMissionType != FMMC_MISSION_TYPE_FLOW_MISSION
            	PRINT_TICKER_WITH_PLAYER_NAME_STRING_AND_MISSION_NAME("TICK_PH_INV", GET_PLAYER_NAME(Event.Details.FromPlayerIndex), Event.tl63MissionName)
			ENDIF
        //no it's a mini game
        ELSE
            STRING stMission = GET_FMMC_MINI_GAME_STRING(Event.iMissionType)
            IF NOT IS_STRING_NULL_OR_EMPTY(stMission)
                PRINT_TICKER_WITH_PLAYER_NAME_AND_STRING("TICK_PH_INVA", Event.Details.FromPlayerIndex, stMission)
            #IF IS_DEBUG_BUILD
            ELSE
                PRINTLN("KGM MP... [TS] PROCESS_EVENT_PLAYER_WARPED_BECAUSE_OF_INVITE - IS_STRING_NULL_OR_EMPTY(stMission)")
                #ENDIF              
            ENDIF
        ENDIF
    #IF IS_DEBUG_BUILD
    ELSE
        PRINTLN("KGM MP... [TS] PROCESS_EVENT_PLAYER_WARPED_BECAUSE_OF_INVITE - could not retrieve data")
        #ENDIF
    ENDIF   
ENDPROC




// ===========================================================================================================
//      JobList Debug
// ===========================================================================================================

// PURPOSE: Display the contents of one element in the joblist.
//
// INPUT PARAMS:        paramArrayPos           The array position for which to output details
#IF IS_DEBUG_BUILD
PROC Debug_Display_JobList_Array_Position(INT paramArrayPos)

    NET_PRINT(" Position ") NET_PRINT_INT(paramArrayPos)
    NET_PRINT(": ")
    
    SWITCH (g_sJobListMP[paramArrayPos].jlState)
        CASE NO_MP_JOBLIST_ENTRY
            NET_PRINT("[EMPTY]   ")
            BREAK
        CASE MP_JOBLIST_CURRENT_MISSION
            NET_PRINT("[CURRENT] ")
            BREAK
        CASE MP_JOBLIST_JOINABLE_MISSION
            NET_PRINT("[JOINABLE]")
            BREAK
        CASE MP_JOBLIST_JOINABLE_PLAYLIST
            NET_PRINT("[JOINABLE PLAYLIST]")
            BREAK
        CASE MP_JOBLIST_INVITATION
            NET_PRINT("[INVITATION]")
            BREAK
        CASE MP_JOBLIST_CS_INVITE
            NET_PRINT("[CROSS-SESSION INVITE]")
            BREAK
        CASE MP_JOBLIST_RV_INVITE
            NET_PRINT("[BASIC INVITE]")
            BREAK
        DEFAULT
            NET_PRINT("[UNKNOWN] ")
            BREAK
    ENDSWITCH
    
    IF NOT (g_sJobListMP[paramArrayPos].jlState = NO_MP_JOBLIST_ENTRY)
        MP_MISSION theMissionID = INT_TO_ENUM(MP_MISSION, g_sJobListMP[paramArrayPos].jlMissionAsInt)
        
        NET_PRINT(GET_ACTUAL_MP_MISSION_TITLE(theMissionID))
        NET_PRINT("  [UniqueID = ") NET_PRINT_INT(g_sJobListMP[paramArrayPos].jlUniqueID) NET_PRINT("]")
    ENDIF
    
    NET_NL()
        
ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display the contents of the JobList
#IF IS_DEBUG_BUILD
PROC Debug_Display_JobList()

    NET_NL()
    NET_PRINT("-----------------------------------------------------------------") NET_NL()
    NET_PRINT("KGM MP: Contents of JobList array") NET_NL()
    
    INT tempLoop = 0
    REPEAT MAX_MP_JOBLIST_ENTRIES tempLoop
        Debug_Display_JobList_Array_Position(tempLoop)
    ENDREPEAT

ENDPROC
#ENDIF





// ===========================================================================================================
//      PRIVILEGE CHECKING FUNCTIONS
// ===========================================================================================================

// PURPOSE: Check if underage privilege restriction should block this invite from being shown to the player
//
// INPUT PARAMS:        paramCreatorID      The CreatorID
// RETURN VALUE:        BOOL                TRUE if a privilege restriction exists, otherwise FALSE
//
// NOTES:   Privilege restrictions include under 17's being blocked from UGC
FUNC BOOL Check_If_Underage_Privilege_Restrictions_Exist(INT paramCreatorID, GAMER_HANDLE paramInvitorGH)

    // If the creator isn't Rockstar then under 17's should be blocked from receiving this invite
    IF NOT (Is_CreatorID_Rockstar(paramCreatorID))
		IF IS_XBOX_PLATFORM()
		#IF FEATURE_GEN9_RELEASE
		OR IS_PROSPERO_VERSION()
		#ENDIF
			IF NETWORK_CAN_VIEW_GAMER_USER_CONTENT(paramInvitorGH)
				RETURN FALSE
			ENDIF
		ENDIF
        IF NOT (IS_ACCOUNT_OVER_17_FOR_UGC())
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("- FAILED: Privilege restriction exists - account isn't over 17 for UGC. CreatorID: ")
                NET_PRINT_INT(paramCreatorID)
                NET_NL()
            #ENDIF
        
            // Restriction exists
            RETURN TRUE
        ENDIF
    ENDIF
    
    // No restriction
    RETURN FALSE

ENDFUNC

// Gets called whilst m_ICTOFFSET_MSEC is set to a time in the future my Maintain Joblist Update.
// This gets set in the joblist and when invites are accepted
PROC Ensure_Exclusive_Joblist_Controls()
	
	// 1992061 - Add your controls to disable in here!
	// This has been disabled but submitted as part of 1992061, after accepting certain invites there may be a 
	// button conflict in certain modes after using the phone. By adding in disable functions like below:
	// KGM 11/9/14: Instead, going down the route of keeping the cellphone controls exclusive
	
	SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, INPUT_CELLPHONE_SELECT)
	SET_INPUT_EXCLUSIVE (PLAYER_CONTROL, INPUT_CELLPHONE_CANCEL)

	//PRINTLN("...KGM MP [JobList]: Phone Positive and Phone Negative Inputs are temporarily exclusive (Now: ", GET_GAME_TIMER(), ") - TIMEOUT: ", g_inviteControlTimeout)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a network privilege restriction should block this invite from being shown to the player
//
// INPUT PARAMS:        paramCreatorID      The CreatorID
// RETURN VALUE:        BOOL                TRUE if a network privilege restriction exists, otherwise FALSE
//
// NOTES:   This was originally required for PS4 only since chat and content restrictiosn got separated, but the network command should be generic and give no restriction on the other platforms
FUNC BOOL Check_If_Content_Privilege_Restrictions_Exist(INT paramCreatorID, GAMER_HANDLE paramInvitorGH)

    // If the creator isn't Rockstar then under 17's should be blocked from receiving this invite
    IF NOT (Is_CreatorID_Rockstar(paramCreatorID))
		IF IS_XBOX_PLATFORM()
		#IF FEATURE_GEN9_RELEASE
		OR IS_PROSPERO_VERSION()
		#ENDIF
			IF NETWORK_CAN_VIEW_GAMER_USER_CONTENT(paramInvitorGH)
				RETURN FALSE
			ENDIF
		ENDIF
		IF NOT (NETWORK_HAVE_USER_CONTENT_PRIVILEGES())
			// KGM 6/2/15 [BUG 2224865]: A general restriction exists, but 'friends' may be allowed
			IF NOT (NETWORK_HAVE_USER_CONTENT_PRIVILEGES(PC_FRIENDS))
	            #IF IS_DEBUG_BUILD
	                NET_PRINT("...KGM MP [JobList]: ")
	                NET_PRINT_TIME()
	                NET_PRINT("- FAILED: Privilege restriction exists - player doesn't have user content privileges for non-Rockstar content. CreatorID: ")
	                NET_PRINT_INT(paramCreatorID)
	                NET_NL()
	            #ENDIF
	        
	            // Restriction exists
	            RETURN TRUE
			ENDIF
        ENDIF
		
    ENDIF
    
    // No restriction
    RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the game is still waiting for the Gamer to Gamer Content Restriction check to be performed
//
// INPUT PARAMS:			paramPlayerID		The Inviting PlayerID
// RETURN VALUE:			BOOL				TRUE if the game still needs to wait for the Gamer To Gamer Content Restriction result to be ready, FALSE if it's ready
//
// NOTES:	XBOX ONE only. Refines the Content Restriction result by doing a Gamer To Gamer check. This will be ready within a few frames of a new player joining a
//				session, so this check is just in case it is still in the 'few frames' before the result is ready
FUNC BOOL Wait_For_Gamer_To_Gamer_Content_Restriction_Result(PLAYER_INDEX paramPlayerID)

	IF NOT (IS_XBOX_PLATFORM())
		// ...not Xbox One version, so no need to wait
		RETURN FALSE
	ENDIF
	
	// Commandline -sc_tempKeithNGGamerCheckOff switches this check off
	#IF IS_DEBUG_BUILD
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_tempKeithNGGamerCheckOff"))
			PRINTLN(".KGM [JobList]: Durango refined Gamer-To-Gamer Content Restriction Wait For Result check switched off by commandline: sc_tempKeithNGGamerCheckOff")
			RETURN FALSE
		ENDIF
	#ENDIF
	
	// Using the gamer handle of the inviting player, check if the gamer-to-gamer result is available
	GAMER_HANDLE inviterGamerHandle = GET_GAMER_HANDLE_PLAYER(paramPlayerID)
	
	IF (NETWORK_HAS_VIEW_GAMER_USER_CONTENT_RESULT(inviterGamerHandle))
		// ...the data is already available, so no need to wait
		RETURN FALSE
	ENDIF
	
	// ...still need to wait
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT(" DURANGO needs to do a refined Gamer-To-Gamer content restriction check. Still waiting for result to be available.")
        NET_NL()
    #ENDIF
			
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a Gamer to Gamer Content Restriction exists
//
// INPUT PARAMS:			paramPlayerID		The Inviting PlayerID
// RETURN VALUE:			BOOL				TRUE if a Gamer To Gamer Content Restriction exists, FALSE if not
//
// NOTES:	XBOX ONE only. Refines the Content Restriction result by doing a Gamer To Gamer check. Must ensure the check is ready to be tested using above function.
FUNC BOOL Check_If_Gamer_To_Gamer_Content_Restriction_Exists(PLAYER_INDEX paramPlayerID)

	IF NOT IS_XBOX_PLATFORM()
	#IF FEATURE_GEN9_RELEASE
	AND NOT IS_PROSPERO_VERSION()
	#ENDIF
		// ...not Xbox One version, so no Gamer-To-Gamer restriction exists
		RETURN FALSE
	ENDIF
	
	// Commandline -sc_tempKeithNGGamerCheckOff switches this check off
	#IF IS_DEBUG_BUILD
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_tempKeithNGGamerCheckOff"))
			PRINTLN(".KGM [JobList]:  ...Durango refined Gamer-To-Gamer Content Restriction checks switched off by commandline: sc_tempKeithNGGamerCheckOff")
			RETURN FALSE
		ENDIF
	#ENDIF
	
	// Using the gamer handle of the inviting player, check if the gamer-to-gamer result is available
	GAMER_HANDLE inviterGamerHandle = GET_GAMER_HANDLE_PLAYER(paramPlayerID)
	
	IF (NETWORK_CAN_VIEW_GAMER_USER_CONTENT(inviterGamerHandle))
		// ...there is no restriction
	    #IF IS_DEBUG_BUILD
	        NET_PRINT("...KGM MP [JobList]: ")
	        NET_PRINT_TIME()
	        NET_PRINT("  ...DURANGO refined Gamer-To-Gamer content restriction DOES NOT exist. Allow Invite.")
	        NET_NL()
	    #ENDIF
			
		RETURN FALSE
	ENDIF
	
	// ...content is restricted
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("  ...DURANGO refined Gamer-To-Gamer content restriction exists. Block Invite.")
        NET_NL()
    #ENDIF
			
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the player has Xbox LIVE privileges
//
// RETURN VALUE:		BOOL				TRUE if the player has Xbox Live privileges, FALSE if not
FUNC BOOL XBOX_ONLY_Check_If_Player_Has_LIVE_Privileges()

	IF NOT (IS_XBOX360_VERSION())
		RETURN TRUE
	ENDIF
	
	RETURN (NETWORK_HAVE_ONLINE_PRIVILEGES())

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if an Invite should be blocked because of Friends Only restrictions
//
// INPUT PARAMS:        paramCreatorID      The CreatorID
// RETURN PARAMS:		refXboxLiveHelp		TRUE if the calling function should display Xbox Live restriction help text, otherwise FALSE for normal help text
// RETURN VALUE:        BOOL                TRUE if a privilege restriction exists, otherwise FALSE
FUNC BOOL Check_If_Invite_Blocked_By_Friends_Only_Restriction(INT paramCreatorID, BOOL &refXboxLiveHelp)

	// In general, we don't want any restriction help text to be Xbox Live related
	refXboxLiveHelp = FALSE

    // If the creator is Rockstar then there is no restriction
    IF (Is_CreatorID_Rockstar(paramCreatorID))
        RETURN FALSE
    ENDIF
    
    // If allowed to get everyone's content then there is no restriction
    IF (ALLOWED_TO_GET_EVERYONE_CONTENT())
        RETURN FALSE
    ENDIF
	
	// BUG 1942627: For XboxOne, do the 'privileges for MP' check first (contained as the last part of ALLOWED_TO_GET_FRIEND_CONTENT()) so that the correct message gets displayed.
	//				If I don't do this then the 'friend content' message gets displayed instead, which is incorrect.
	IF (IS_XBOX_PLATFORM())
		IF NOT (PRIVALEGES_OKAY_FOR_MP())
	        // ...privileges are not OK for MP, so a restriction exists
	        #IF IS_DEBUG_BUILD
	            NET_PRINT("...KGM MP [JobList]: ")
	            NET_PRINT_TIME()
	            NET_PRINT("- FAILED: Privilege restriction exists - privileges not OK for MP within Friend Content check.")
	            NET_NL()
	        #ENDIF
			
			refXboxLiveHelp = TRUE
	    
			RETURN TRUE
		ENDIF
	ENDIF
    
    // If not allowed to get Friend's content then a RESTRICTION EXISTS
	// KGM 5/10/14 [BUG 2068126]: For DURANGO, a refined Gamer-To-Gamer check exists, so use it 'allowed to get Friends Content' check fails.
    IF NOT (ALLOWED_TO_GET_FRIEND_CONTENT(TRUE))
        // ...not allowed to get friend content
		
		// For DURANGO version, there is a refined Gamer-To-Gamer check that will already have been performed. At this point either the result of the check is still
		//			not known (in which case we shouldn't block the invite - the check will take place later when the result is known) OR the result is known and
		//			there is NO RESTRICTION (because a restriction would have blocked the invite at an earlier stge). Either way, the DURANGO invite shouldn't be blocked here.
		IF (IS_XBOX_PLATFORM())
	        #IF IS_DEBUG_BUILD
	            NET_PRINT("...KGM MP [JobList]: ")
	            NET_PRINT_TIME()
	            NET_PRINT("  ...BUT, obey DURANGO refined GamerToGamer check. Allow invite.")
	            NET_NL()
	            NET_PRINT("...KGM MP [JobList]: ")
	            NET_PRINT_TIME()
	            NET_PRINT("     (Either the DURANGO result is not available yet so it will be checked later, OR the check passed with NO RESTRICTION)")
	            NET_NL()
	        #ENDIF
			
			// ...not blocked
			RETURN FALSE
		ENDIF
		
		// Not the DURANGO version, so obey the restriction
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("- FAILED: Privilege restriction exists - not allowed to get Friend Content. CreatorID: ")
			NET_PRINT_INT(paramCreatorID)
            NET_NL()
        #ENDIF
    
        RETURN TRUE
    ENDIF
    
    // Players are allowed to get friend's content, so check if the CreatorID is still valid
    PLAYER_INDEX ugcPlayer = INT_TO_PLAYERINDEX(paramCreatorID)
    IF NOT (IS_NET_PLAYER_OK(ugcPlayer, FALSE))
        // ...the creatorID is no longer in the game, so block the content (can't check if they are friends)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ") NET_PRINT_TIME() NET_PRINT(" Privilege Restriction blocks content: Friend Content allowed but creator has left game. CreatorID: ")
            NET_PRINT_INT(paramCreatorID)
            NET_NL()
        #ENDIF

        RETURN TRUE
    ENDIF
    
    // CreatorID is still valid, so compare gamer handles to check for friends
    GAMER_HANDLE    myGamerHandle       = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
    GAMER_HANDLE    creatorGamerHandle  = GET_GAMER_HANDLE_PLAYER(ugcPlayer)
    
    IF NOT (NETWORK_ARE_HANDLES_THE_SAME(myGamerHandle, creatorGamerHandle))
        IF NOT (NETWORK_IS_FRIEND(creatorGamerHandle))
            // ...the creatorID is not a friend, so block the content
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ") NET_PRINT_TIME() NET_PRINT(" Privilege Restriction blocks content: Creator is not a friend. CreatorID: ")
                NET_PRINT_INT(paramCreatorID)
                NET_PRINT(" [")
                NET_PRINT(GET_PLAYER_NAME(ugcPlayer))
                NET_PRINT("]")
                NET_NL()
            #ENDIF
    
            RETURN TRUE
        ENDIF
    ENDIF
    
    // No friend restriction
    RETURN FALSE
    
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the Invitor's Gamer Handle from a cross-session invite is restricted
//
// INPUT PARAMS:        paramInvitorGH      The Invitor's Gamer Handle
// RETURN PARAMS:		refXboxLiveHelp		TRUE if the calling function should display Xbox Live restriction help text, otherwise FALSE for normal help text
// RETURN VALUE:        BOOL                TRUE if the Invitor is restricted, FALSE if not
//
// NOTES:   Assumes this is not Rockstar Created Content - that check should already have been done
FUNC BOOL Check_If_CSInvite_GH_Blocked_By_Friends_Only_Restriction(GAMER_HANDLE paramInvitorGH, BOOL &refXboxLiveHelp, BOOL bCrossSession = FALSE)

	// In general, we don't want any restriction help text to be Xbox Live related
	refXboxLiveHelp = FALSE
    
    // If allowed to get everyone's content then there is no restriction
    IF (ALLOWED_TO_GET_EVERYONE_CONTENT())
        RETURN FALSE
    ENDIF
	
	// BUG 1942627: For XboxOne, do the 'privileges for MP' check first (contained as the last part of ALLOWED_TO_GET_FRIEND_CONTENT()) so that the correct message gets displayed.
	//				If I don't do this then the 'friend content' message gets displayed instead, which is incorrect.
	IF (IS_XBOX_PLATFORM())
		IF NOT (PRIVALEGES_OKAY_FOR_MP())
	        // ...privileges are not OK for MP, so a restriction exists
	        #IF IS_DEBUG_BUILD
	            NET_PRINT("...KGM MP [JobList]: ")
	            NET_PRINT_TIME()
	            NET_PRINT("- FAILED: Privilege restriction exists - privileges not OK for MP within CS Friend Content GH check.")
	            NET_NL()
	        #ENDIF
			
			refXboxLiveHelp = TRUE
	    
			RETURN TRUE
		ENDIF
	ENDIF
    
    // If not allowed to get Friend's content then a RESTRICTION EXISTS
    IF NOT (ALLOWED_TO_GET_FRIEND_CONTENT(TRUE))
	AND NOT bCrossSession
        // ...not allowed to get friend content, so a restriction exists
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("- FAILED: CS Privilege restriction exists - not allowed to get Friend Content.")
            NET_NL()
        #ENDIF
    
        RETURN TRUE
    ENDIF
    
    // Players are allowed to get friend's content, so compare gamer handles to check for friends
    GAMER_HANDLE    myGamerHandle       = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
    
    IF NOT bCrossSession
	AND NOT (NETWORK_ARE_HANDLES_THE_SAME(myGamerHandle, paramInvitorGH))
        IF NOT (NETWORK_IS_FRIEND(paramInvitorGH))
            // ...the invitor is not a friend, so block the content
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ") NET_PRINT_TIME() NET_PRINT(" CS Privilege Restriction blocks content: Invitor is not a friend.")
                NET_NL()
            #ENDIF
    
            RETURN TRUE
        ENDIF
    ENDIF
    
    // No friend restriction
    RETURN FALSE
    
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display a restriction help message if a restriction has arrived and it has been a while since the last help message
//
// INPUT PARAMS:		paramXboxLive			[DEFAULT = FALSE] TRUE if this is an XBOX LIVE restriction so that a different help message can be displayed, otherwise FALSE
PROC Display_Restriction_Help_Message(BOOL paramXboxLive = FALSE)

	// Ignore if this is an XBOX LIVE error message and this isn't an XBOX version
	IF (paramXboxLive)
		IF NOT (IS_XBOX360_VERSION())
			EXIT
		ENDIF
	ENDIF
	
	//Adding this beucase:
	//url:bugstar:3799844 - When the remote player invites the local player, who has the "You can see and share content" 
	//		privilege set to "Block", to a UGC job lobby, the remote player is shown the system privilege conflict message without being shown the invite.
	//Would it be possible to change this so that the person with the privilege restriction who is sent the invite doesn't get anything at all? 
	//So no help text or system UI message is displayed. 
	//This would be compliant as the person sending the invite is given the "BLOCKED" message indicating the invite can't be sent.
	IF IS_XBOX_PLATFORM()
        PRINTLN("...KGM MP [JobList]:Invite Restriction Help Message still blocked by being recently displayed - IS_XBOX_PLATFORM EXIT")
		EXIT
	ENDIF

	INT gameTimer = GET_GAME_TIMER()
	
	// Is the help message allowed again?
	IF (gameTimer < g_inviteRestrictedTimeout)
		#IF IS_DEBUG_BUILD
               NET_PRINT("...KGM MP [JobList]: ") NET_PRINT_TIME() NET_PRINT("Invite Restriction Help Message still blocked by being recently displayed") NET_NL()
		#ENDIF
	
		EXIT
	ENDIF
	
	// It is allowed, so display it if there is no other help text on display
	IF (IS_HELP_MESSAGE_BEING_DISPLAYED())
		EXIT
	ENDIF
	
	// Ok to display
	IF (paramXboxLive)
		// Display XBOX LIVE privilege restriction
		PRINT_HELP("INV_NO_XBOXLIVE")
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [JobList]: ") NET_PRINT_TIME() NET_PRINT("Xbox LIVE Privileges Invite Restriction Help Message being displayed") NET_NL()
		#ENDIF
	ELSE
		// Display normal restriction (the text file itself has been setup to distinguish between Xbox and PS3 text)
		PRINT_HELP("INV_RESTRICT")
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [JobList]: ") NET_PRINT_TIME() NET_PRINT("Invite Restriction Help Message being displayed") NET_NL()
		#ENDIF
	ENDIF
	
	// Set up the timer to prevent the message again for a while
	g_inviteRestrictedTimeout = gameTimer + RESTRICTED_INVITE_HELP_MESSAGE_TIMEOUT_msec

ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display a slightly different restriction help message if an underage player receives an invite from a friend while in SP - we can't tell if the content is UGC or R*
//PROC Display_LastGen_SP_Restriction_Help_Message()
//
//	// Ignore if not LastGen
//	IF NOT (IS_XBOX_PLATFORM())
//		PRINTLN(".KGM [JobList]: Display_LastGen_SP_Restriction_Help_Message() - IGNORE: Not LastGen")
//		EXIT
//	ENDIF
//	
//	// Ignore if not SP
//	IF (NETWORK_IS_GAME_IN_PROGRESS())
//		PRINTLN(".KGM [JobList]: Display_LastGen_SP_Restriction_Help_Message() - IGNORE: Not SP")
//		EXIT
//	ENDIF
//
//	INT gameTimer = GET_GAME_TIMER()
//	
//	// Is the help message allowed again?
//	IF (gameTimer < g_inviteRestrictedTimeout)
//		PRINTLN(".KGM [JobList]: Display_LastGen_SP_Restriction_Help_Message() - IGNORE: still blocked by being restriction message being recently displayed")
//		EXIT
//	ENDIF
//
//	// Ok to display
//	NETWORK_CHECK_PRIVILEGES(0, PRIVILEGE_TYPE_DURANGO_USER_CREATED_CONTENT, TRUE)
//	NETWORK_SET_PRIVILEGE_CHECK_RESULT_NOT_NEEDED() // Forget the result once were done.
//	
//	PRINTLN(".KGM [JobList]: Display_LastGen_SP_Restriction_Help_Message() - DISPLAYED")
//	
//	// Set up the timer to prevent the message again for a while
//	g_inviteRestrictedTimeout = gameTimer + RESTRICTED_INVITE_HELP_MESSAGE_TIMEOUT_msec

//ENDPROC




// ===========================================================================================================
//      CROSS-SESSION INVITE RECENTLY ACCEPTED FUNCTIONS
// ===========================================================================================================

// PURPOSE:	Set a timeout to cover the gap between accepting a cross-session invite and IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED() being TRUE
PROC Set_Cross_Session_Invite_Just_Accepted_Timeout()
	g_joblistCSInviteJustAcceptedTimeout = GET_GAME_TIMER() + CROSS_SESSION_INVITE_ACCEPTED_INITIAL_DELAY_msec
	PRINTLN(".KGM [JobList]: Setting Recent Cross-session Invite Accepted Timeout: ", CROSS_SESSION_INVITE_ACCEPTED_INITIAL_DELAY_msec, " msec")
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a cross-session invite has just been accepted
//
// RETURN VALUE:			BOOL		TRUE if a CSInvite has been recently accepted (ie: within timeout), otherwise FALSE
FUNC BOOL Has_Cross_Session_Invite_Been_Recently_Accepted()

	IF (GET_GAME_TIMER() > g_joblistCSInviteJustAcceptedTimeout)
		RETURN FALSE
	ENDIF
	
	// Recently accepted
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the cross-session invite recent acceptance timer
// NOTES:	If the timer is active and a transition invite has recently been actioned, then extend the timer - this should allow this flag to remain so only this function needs checked
PROC Maintain_Cross_Session_Invite_Recent_Acceptance_Timer()

	IF (g_joblistCSInviteJustAcceptedTimeout = 0)
		EXIT
	ENDIF
	
	IF (GET_GAME_TIMER() > g_joblistCSInviteJustAcceptedTimeout)
		PRINTLN(".KGM [JobList]: Recent Cross-session Invite Accepted Timer has timed out.")
		g_joblistCSInviteJustAcceptedTimeout = 0
		EXIT
	ENDIF
	
	// Hasn't timed out, so extend the timer if a transition invite is being actioned
	// NOTES: This should occur about a second after the invite is accepted - the timer is really to cover the gap
	IF NOT (IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED())
		EXIT
	ENDIF
	
	// Extend the timer
	g_joblistCSInviteJustAcceptedTimeout = GET_GAME_TIMER() + CROSS_SESSION_INVITE_ACCEPTED_EXTEND_DELAY_msec

ENDPROC



// ===========================================================================================================
//      REMOVE ALL INVITE FEED MESSAGES
// ===========================================================================================================

// PURPOSE: Remove all invite feed messages on invite acceptance
PROC Joblist_Remove_All_Invite_Feed_Messages()

	INT tempLoop = 0
	
	PRINTLN(".KGM MP [JobList]: Joblist_Remove_All_Invite_Feed_Messages() called")
	
	// Same-session Invites
	REPEAT g_numMPJobListInvitations tempLoop
		IF (g_sJLInvitesMP[tempLoop].jliFeedID != NO_JOBLIST_INVITE_FEED_ID)
			THEFEED_REMOVE_ITEM(g_sJLInvitesMP[tempLoop].jliFeedID)
			g_sJLInvitesMP[tempLoop].jliFeedID = NO_JOBLIST_INVITE_FEED_ID
		ENDIF
	ENDREPEAT
	
	// Cross-session Invites
	REPEAT g_numMPCSInvites tempLoop
		IF (g_sJLCSInvitesMP[tempLoop].jcsiFeedID != NO_JOBLIST_INVITE_FEED_ID)
			THEFEED_REMOVE_ITEM(g_sJLCSInvitesMP[tempLoop].jcsiFeedID)
			g_sJLCSInvitesMP[tempLoop].jcsiFeedID = NO_JOBLIST_INVITE_FEED_ID
		ENDIF
	ENDREPEAT
	
	// Basic Invites
	REPEAT g_numMPRVInvites tempLoop
		IF (g_sJLRVInvitesMP[tempLoop].jrviFeedID != NO_JOBLIST_INVITE_FEED_ID)
			THEFEED_REMOVE_ITEM(g_sJLRVInvitesMP[tempLoop].jrviFeedID)
			g_sJLRVInvitesMP[tempLoop].jrviFeedID = NO_JOBLIST_INVITE_FEED_ID
		ENDIF
	ENDREPEAT
	
ENDPROC




// ===========================================================================================================
//      Asynchronous Display Name Retrieval Functions (XboxOne version only)
// ===========================================================================================================

// PURPOSE:	If required, start an asynchronous retrieval of a DisplayName from a GamerHandle
//
// INPUT PARAMS:			paramInvitorGH			The Inviting Player's GamerHandle
// RETURN VALUE:			INT						The DisplayName requestID - as returned from the code function for future reference to the search
FUNC INT Start_Asynchronous_DisplayName_Retrieval_If_Required(GAMER_HANDLE paramInvitorGH)

	// XboxOne only
	IF NOT (IS_XBOX_PLATFORM())
		RETURN (NO_DISPLAY_NAME_RETRIEVAL)
	ENDIF

	// Setup an array of GamerHandles where DisplayNames are required
	// NOTE: only 1 required
	GAMER_HANDLE displayGamer[1]
	displayGamer[0] = paramInvitorGH
	
	// Start the asynchronous request
	INT requestID = NETWORK_DISPLAYNAMES_FROM_HANDLES_START(displayGamer, 1)
	
	IF (requestID  < 0)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [JobList]: ") NET_PRINT_TIME() NET_PRINT("Start_Asynchronous_DisplayName_Retrieval_If_Required() - FAILED TO START, DISPLAY NAME WILL NOT GET RETRIEVED") NET_NL()
		#ENDIF
		
		RETURN (NO_DISPLAY_NAME_RETRIEVAL)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [JobList]: ") NET_PRINT_TIME() NET_PRINT("Start_Asynchronous_DisplayName_Retrieval_If_Required() - RequestID: ") NET_PRINT_INT(requestID) NET_NL()
	#ENDIF
	
	RETURN (requestID)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the asynchronous retrieval of DisplayNames for Cross-session invites
// NOTES:	This is required for XboxOne only
PROC Maintain_Asynchronous_CSInvites_DisplayName_Retrieval()

	// XboxOne only
	IF NOT (IS_XBOX_PLATFORM())
		EXIT
	ENDIF

	IF (g_numMPCSInvites = 0)
		EXIT
	ENDIF
	
	// Set up the return array of displayNames to be returned from the function
	// NOTE: Only 1 is required
	TEXT_LABEL_63 displayNames[1]
	displayNames[0] = ""
	
	INT tempLoop = 0
	INT requestResult = NO_DISPLAY_NAME_RETRIEVAL
	
	REPEAT g_numMPCSInvites tempLoop
		IF (g_sJLCSInvitesMP[tempLoop].jcsiDisplayNameID != NO_DISPLAY_NAME_RETRIEVAL)
			// ...a display name retrieval is in progress, so check if it has finished
			requestResult = NETWORK_GET_DISPLAYNAMES_FROM_HANDLES(g_sJLCSInvitesMP[tempLoop].jcsiDisplayNameID, displayNames, 1)
			
			// Check the result
			// ...-1 returned means FAILED
			IF (requestResult = -1)
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [JobList]: ")
					NET_PRINT_TIME()
					NET_PRINT("Retrieve Asynchronous DisplayName for RequestID: ")
					NET_PRINT_INT(g_sJLCSInvitesMP[tempLoop].jcsiDisplayNameID)
					NET_PRINT(" [")
					NET_PRINT(g_sJLCSInvitesMP[tempLoop].jcsiInvitorGamerTag)
					NET_PRINT("] - FAILED")
					NET_NL()
				#ENDIF
				
				g_sJLCSInvitesMP[tempLoop].jcsiDisplayNameID		= NO_DISPLAY_NAME_RETRIEVAL
				g_sJLCSInvitesMP[tempLoop].jcsiInvitorDisplayName	= ""
				g_sJLCSInvitesMP[tempLoop].jcsiDisplayNameTimeout	= GET_GAME_TIMER()
			ENDIF
			
			// ...0 returned means succeeded
			IF (requestResult = 0)
				g_sJLCSInvitesMP[tempLoop].jcsiInvitorDisplayName = displayNames[0]

				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [JobList]: ")
					NET_PRINT_TIME()
					NET_PRINT("Retrieve Asynchronous DisplayName for RequestID: ")
					NET_PRINT_INT(g_sJLCSInvitesMP[tempLoop].jcsiDisplayNameID)
					NET_PRINT(" [GamerTag: ")
					NET_PRINT(g_sJLCSInvitesMP[tempLoop].jcsiInvitorGamerTag)
					NET_PRINT("] - SUCCEEDED")
					NET_NL()
					NET_PRINT("...KGM MP [JobList]: ...Returned Display Name: ")
					NET_PRINT(g_sJLCSInvitesMP[tempLoop].jcsiInvitorDisplayName)
					NET_NL()
				#ENDIF
				
				g_sJLCSInvitesMP[tempLoop].jcsiDisplayNameID 		= NO_DISPLAY_NAME_RETRIEVAL
				g_sJLCSInvitesMP[tempLoop].jcsiDisplayNameTimeout	= GET_GAME_TIMER()
			ENDIF
			
			// ...1 returned means 'ongoing'
			IF (requestResult = 1)
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [JobList]: ")
					NET_PRINT_TIME()
					NET_PRINT("Retrieve Asynchronous DisplayName for RequestID: ")
					NET_PRINT_INT(g_sJLCSInvitesMP[tempLoop].jcsiDisplayNameID)
					NET_PRINT(" [GamerTag: ")
					NET_PRINT(g_sJLCSInvitesMP[tempLoop].jcsiInvitorGamerTag)
					NET_PRINT("] - ONGOING")
					NET_NL()
				#ENDIF
			ENDIF
			
			// Clear the local variables in case there are more ongoing requests
			displayNames[0]	= ""
			requestResult	= NO_DISPLAY_NAME_RETRIEVAL
		ENDIF
	ENDREPEAT

ENDPROC




// ===========================================================================================================
//      SOLO SESSION FUNCTIONS
// ===========================================================================================================

// PURPOSE: Check if this is a solo session
//
// RETURN VALUE:        BOOL            TRUE if the player is playing a solo session (so player invites should be suppressed), otherwise FALSE
FUNC BOOL Joblist_Is_This_A_Solo_Session()
    RETURN (g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY = FM_SESSION_MENU_CHOICE_JOIN_CLOSED_SOLO_PLAYABLE_SESSION)
ENDFUNC




// ===========================================================================================================
//      500-Char Description Functions
// ===========================================================================================================

// PURPOSE: Get an already loaded 500-char description and split it into TL63's
//
// INPUT PARAMS:		paramFullDescription		The Full Description
// REFERENCE PARAMS:	refSplitDescription			The Description split over TL63's
PROC Split_Full_Description_String_Into_TL63s(STRING paramFullDescription, UGC_DESCRIPTION &refSplitDescription)
	
	// NULLify the TLs
	INT thisSegment	= 0
	
	REPEAT NUM_TEXTLABELS_IN_UGC_DESCRIPTION thisSegment
		refSplitDescription.TextLabel[thisSegment] = ""
	ENDREPEAT
	
	IF (IS_STRING_NULL_OR_EMPTY(paramFullDescription))
		PRINTLN(".KGM [JobList]: Full Description is NULL so passing back empty TLs")
		EXIT
	ENDIF
		
	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [JobList]: Full Description: ", paramFullDescription)
	#ENDIF
	
	// Need to split the description into smaller chunks
	INT stringLength		= GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramFullDescription)
	INT startSegmentPos		= 0
	INT endSegmentPos		= INVITE_DESCRIPTION_SEGMENT_LENGTH
	
	thisSegment	 = 0
	
	IF (stringLength > 0)
		// ...while the stringLength is greater than or the same as the end segment position, copy across the whole string segment
		// KGM 24/3/15 [BUG 2288825]: Add a safety bail, based on the number of segments
		WHILE ((stringLength >= endSegmentPos) AND (thisSegment < NUM_TEXTLABELS_IN_UGC_DESCRIPTION))
			refSplitDescription.TextLabel[thisSegment] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME_BYTES(paramFullDescription, startSegmentPos, endSegmentPos)
			thisSegment++
			startSegmentPos = endSegmentPos
			endSegmentPos += INVITE_DESCRIPTION_SEGMENT_LENGTH
		ENDWHILE
		
		// KGM 24/3/15 [BUG 2288825]: Add a safety bail, based on the number of segments
		IF (thisSegment < NUM_TEXTLABELS_IN_UGC_DESCRIPTION)
			// ...if there is a small piece of the string left over, copy the remaining fragment
			IF ((stringLength > startSegmentPos) AND (stringLength < endSegmentPos))
				refSplitDescription.TextLabel[thisSegment] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME_BYTES(paramFullDescription, startSegmentPos, stringLength)
				thisSegment++
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF ((thisSegment = NUM_TEXTLABELS_IN_UGC_DESCRIPTION) AND (stringLength > startSegmentPos))
			NET_PRINT("...KGM MP [JobList]: Full Description split into segments for display - DESCRIPTION TRUNCATED FOR BEING TOO LONG") NET_NL()
		ENDIF
		
		NET_PRINT("...KGM MP [JobList]: Full Description split into segments for display:") NET_NL()
		REPEAT NUM_TEXTLABELS_IN_UGC_DESCRIPTION thisSegment
			NET_PRINT("      ")
			NET_PRINT_INT(thisSegment)
			NET_PRINT(":  ")
			NET_PRINT(refSplitDescription.TextLabel[thisSegment])
			NET_NL()
		ENDREPEAT
	#ENDIF
			
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Get an already loaded 500-char description and split it into TL63's
//
// INPUT PARAMS:		paramDescHash			The description Hash for the 500-char description
// REFERENCE PARAMS:	refSplitDescription		The Description split over TL63's
PROC Split_Cached_Description_Into_TL63s(INT paramDescHash, UGC_DESCRIPTION &refSplitDescription)

	// Retrieve the description
	STRING fullDescriptionAsString	= UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(paramDescHash, INVITE_MAX_DESCRIPTION_LENGTH)
		
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [JobList]: Retrieved Full Cached Description: ")
		NET_PRINT(fullDescriptionAsString)
		NET_NL()
	#ENDIF
	
	// Need to split the description into smaller chunks
	Split_Full_Description_String_Into_TL63s(fullDescriptionAsString, refSplitDescription)
			
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	KGM 16/3/15 [BUG 2279487]: Check if this mission contentID is stored in the Cached Mission Details array (currently used to hold Heist Prep Missions)
//
// INPUT PARAMS:		paramContentID		ContentID of the mission being checked for on the cache
// RETURN VALUE:		INT					ArrayPos in the cache, or ILLEGAL_ARRAY_POSITION if not found
FUNC INT Find_Cached_Mission_Header_Data(TEXT_LABEL_23 paramContentID)
	
	// Ignore If cache is disabled
	IF (g_sMPTunables.bDisableUgcPlanningCache)
		PRINTLN(".KGM [Joblist]: Find_Cached_Mission_Header_Data() - IGNORE, cache is disabled by tunable: g_sMPTunables.bDisableUgcPlanningCache")
		RETURN (ILLEGAL_ARRAY_POSITION)
	ENDIF

	// Ignore if not in network game (cache held in UGC global block)
	IF NOT (NETWORK_IS_GAME_IN_PROGRESS())
		PRINTLN(".KGM [Joblist]: Find_Cached_Mission_Header_Data() - IGNORE, the data is not cached when in SP")
		RETURN (ILLEGAL_ARRAY_POSITION)
	ENDIF
	
	// Ignore if there are no cached missions
	IF (g_FMMC_PLANNING_MISSION.iTotalNumMissions = 0)
		PRINTLN(".KGM [Joblist]: Find_Cached_Mission_Header_Data() - IGNORE, there are no cached missions (g_FMMC_PLANNING_MISSION.iTotalNumMissions = 0)")
		RETURN (ILLEGAL_ARRAY_POSITION)
	ENDIF

	IF (IS_STRING_NULL_OR_EMPTY(paramContentID))
		PRINTLN(".KGM [Joblist]: Find_Cached_Mission_Header_Data() - IGNORE, the contentID is NULL or EMPTY")
		RETURN (ILLEGAL_ARRAY_POSITION)
	ENDIF

	INT hashContentID = GET_HASH_KEY(paramContentID)
	
	PRINTLN(".KGM [Joblist]: Find_Cached_Mission_Header_Data() - Searching for contentID: ", paramContentID, ". Hash: ", hashContentID)
	
	INT tempLoop = 0
	
	REPEAT g_FMMC_PLANNING_MISSION.iTotalNumMissions tempLoop
		IF (g_FMMC_PLANNING_MISSION.sDefaultCoronaOptions[tempLoop].iHash = hashContentID)
			PRINTLN(".KGM [Joblist]: Find_Cached_Mission_Header_Data() - FOUND - cached array position: ", tempLoop)
			RETURN (tempLoop)
		ENDIF
	ENDREPEAT

	PRINTLN(".KGM [Joblist]: Find_Cached_Mission_Header_Data() - NOT FOUND IN CACHE")
	RETURN (ILLEGAL_ARRAY_POSITION)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	KGM 16/3/15 [BUG 2279487]: Extract the Heist Prep descriptions from the Heist Prep Cache
//
// INPUT PARAMS:			paramContentID			The contentID for the Heist Prep mission
// REFERENCE PARAMS:		refSplitDescription		The description split into TL63s
PROC Copy_Description_From_Heist_Prep_Cache_Into_TL63s(TEXT_LABEL_23 paramContentID, UGC_DESCRIPTION &refSplitDescription)
	
	// NULLify the TLs
	INT thisSegment	= 0
	
	REPEAT NUM_TEXTLABELS_IN_UGC_DESCRIPTION thisSegment
		refSplitDescription.TextLabel[thisSegment] = ""
	ENDREPEAT
		
	PRINTLN(".KGM [JobList]: Copy_Description_From_Heist_Prep_Cache_Into_TL63s() - Finding Heist Prep Description from Heist Prep Cache. ContentID: ", paramContentID)
	
	INT cachedArrayPos = Find_Cached_Mission_Header_Data(paramContentID)
	IF (cachedArrayPos = ILLEGAL_ARRAY_POSITION)
		PRINTLN(".KGM [JobList]: Copy_Description_From_Heist_Prep_Cache_Into_TL63s() - IGNORE, Heist Prep ContentID not found in Heist Prep Cache")
		EXIT
	ENDIF
	
	// Copy the Heist Prep TL63's cached into the Joblist TL63's struct
	PRINTLN(".KGM [JobList]: Copy_Description_From_Heist_Prep_Cache_Into_TL63s() - Found Heist Prep contentID at array position: ", cachedArrayPos)
	PRINTLN(".KGM [JobList]: Copy_Description_From_Heist_Prep_Cache_Into_TL63s() - Copying the Description as TL63s")
	
	REPEAT NUM_TEXTLABELS_IN_UGC_DESCRIPTION thisSegment
		refSplitDescription.TextLabel[thisSegment] = g_FMMC_PLANNING_MISSION.sDesc[cachedArrayPos].tl63MissionDecription[thisSegment]
		PRINTLN(".KGM [JobList]: Copy_Description_From_Heist_Prep_Cache_Into_TL63s(). ", thisSegment, " - ", refSplitDescription.TextLabel[thisSegment])
	ENDREPEAT
	
ENDPROC




// ===========================================================================================================
//      NPC Invite Functions
// ===========================================================================================================

// PURPOSE: Gets an in-game character's headshot.
//
// INPUT PARAMS:        paramNPC            NPC CharacterSheet ID
// RETURN VALUE:        STRING              Headshot TXD string
FUNC STRING Get_NPC_Headshot(enumCharacterList paramNPC)
	RETURN  GET_STRING_FROM_TL(g_sCharacterSheetAll[paramNPC].picture)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Gets an in-game character's name.
//
// INPUT PARAMS:        paramNPC            NPC CharacterSheet ID
// RETURN VALUE:        TEXT_LABEL          Text Label containing character name in text file
FUNC TEXT_LABEL Get_NPC_Name(enumCharacterList paramNPC)
    RETURN GLOBAL_CHARACTER_SHEET_GET_LABEL(paramNPC)
ENDFUNC




// ===========================================================================================================
//      JobList Invite Sound Effect Functions
// ===========================================================================================================

// PURPOSE: Play a sound effect when an invite is received and sent to the feed.
// NOTES:   The soundset may need to become a variable if we allow players to choose their phone - currently uses default phone soundset (hardcoded).
PROC Play_Invite_Received_Sound_Effect()
	
	//url:bugstar:1796220 requested that this be tied to an option within the Settings menu of the cellphone. We may have to link this to an MP stat in the future.
	IF  This_Cellphone_Owner_Settings_ListContents[CHAR_MULTIPLAYER].g_SettingList[ENUM_TO_INT(SETTING_MISC_A)].Setting_Currently_Selected_Option = 1 //On
    	PLAY_SOUND_FRONTEND(-1,  "Menu_Accept", "Phone_SoundSet_Default")
	ELSE
		#if IS_DEBUG_BUILD
			NET_PRINT("ST - No sound effect for Invite. Turned off in cellphone settings menu.")
		#endif
	ENDIF
	
ENDPROC




// ===========================================================================================================
//      JobList Invite Flash Effect Functions
// ===========================================================================================================

// PURPOSE: Display the Invite Flash style for Important Feed messages
PROC Set_Feed_Flash_Style_For_Invites()

    INT r, g, b, a
    GET_HUD_COLOUR(HUD_COLOUR_WHITE, r, g, b, a)
    THEFEED_SET_RGBA_PARAMETER_FOR_NEXT_MESSAGE(r, g, b, a)
    
    INT numFlashes = 1
    THEFEED_SET_FLASH_DURATION_PARAMETER_FOR_NEXT_MESSAGE(numFlashes)
    
    BOOL shouldVibrate = FALSE
    IF (g_savedGlobals.sCellphoneSettingsData.This_Cellphone_Owner_Settings[g_Cellphone.PhoneOwner].VibrateForThisPlayer = 1)
        shouldVibrate = TRUE
    ENDIF
    
    THEFEED_SET_VIBRATE_PARAMETER_FOR_NEXT_MESSAGE(shouldVibrate)
	
	#IF IS_DEBUG_BUILD
		IF (GET_COMMANDLINE_PARAM_EXISTS("sc_testFeedBackground"))
			PRINTLN(".KGM [JobList]: Set_Feed_Flash_Style_For_Invites: -sc_testFeedBackground exists, so all feed messages will use background colour: HUD_COLOUR_HEIST_BACKGROUND")
			THEFEED_SET_BACKGROUND_COLOR_FOR_NEXT_POST(HUD_COLOUR_HEIST_BACKGROUND)
		ENDIF
	#ENDIF
    
ENDPROC

FUNC BOOL SHOULD_THIS_BG_BE_RED(INT iHash = 0, INT iAdversaryModeType = 0)
	IF iHash != 0
	OR iAdversaryModeType != 0
		IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(iHash)
			RETURN FALSE
		ENDIF
		
		PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - iHash = ", iHash, " - iAdversaryModeType = ", iAdversaryModeType)
		IF IS_THIS_ROCKSTAR_MISSION_NEW_VS_ComeOuttoPlay(iHash)
			PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - IS_THIS_ROCKSTAR_MISSION_NEW_VS_ComeOuttoPlay")
			IF g_sMPTunables.bDisableRedInvite_COME_OUT_TO_PLAY
				PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - bDisableRedInvite_COME_OUT_TO_PLAY - RETURN FALSE")
				RETURN FALSE
			ENDIF
			RETURN TRUE
		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SiegeMentality(iHash)
			PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - IS_THIS_ROCKSTAR_MISSION_NEW_VS_SiegeMentality")
			IF g_sMPTunables.bDisableRedInvite_SIEGE_MENTALITY 
				PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - bDisableRedInvite_SIEGE_MENTALITY - RETURN FALSE")
				RETURN FALSE
			ENDIF
			RETURN TRUE
		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HastaLaVista(iHash)
			PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - IS_THIS_ROCKSTAR_MISSION_NEW_VS_HastaLaVista")
			IF g_sMPTunables.bDisableRedInvite_HASTA_LA_VISTA  
				PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - bDisableRedInvite_HASTA_LA_VISTA - RETURN FALSE")
				RETURN FALSE
			ENDIF
			RETURN TRUE
		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HuntingPack(iHash)
			PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - IS_THIS_ROCKSTAR_MISSION_NEW_VS_HuntingPack")
			IF g_sMPTunables.bDisableRedInvite_HUNTING_PACK    
				PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - bDisableRedInvite_HUNTING_PACK - RETURN FALSE")
				RETURN FALSE
			ENDIF
			RETURN TRUE
		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_CrossTheLine(iHash)
			PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - IS_THIS_ROCKSTAR_MISSION_NEW_VS_CrossTheLine")
			IF g_sMPTunables.bDisableRedInvite_CROSS_THE_LINE  
				PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - bDisableRedInvite_CROSS_THE_LINE - RETURN FALSE")
				RETURN FALSE
			ENDIF
			RETURN TRUE
		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_Shepherd(iHash)
			PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - IS_THIS_ROCKSTAR_MISSION_NEW_VS_Shepherd")
			IF g_sMPTunables.bDisableRedInvite_SHEPHERD        
				PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - bDisableRedInvite_SHEPHERD - RETURN FALSE")
				RETURN FALSE
			ENDIF
			RETURN TRUE
		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_Relay(iHash)
			PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - IS_THIS_ROCKSTAR_MISSION_NEW_VS_Relay")
			IF g_sMPTunables.bDisableRedInvite_RELAY           
				PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - bDisableRedInvite_RELAY - RETURN FALSE")
				RETURN FALSE
			ENDIF
			RETURN TRUE
		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_SpeedRace(iHash)
			PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - IS_THIS_ROCKSTAR_MISSION_NEW_VS_SpeedRace")
			IF g_sMPTunables.bDisableRedInvite_SPEED_RACE     
				PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - bDisableRedInvite_SPEED_RACE - RETURN FALSE")
				RETURN FALSE
			ENDIF
			RETURN TRUE
		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_Ebc(iHash)
			PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - IS_THIS_ROCKSTAR_MISSION_NEW_VS_Ebc")
			IF g_sMPTunables.bDisableRedInvite_EBC       
				PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - bDisableRedInvite_EBC - RETURN FALSE")
				RETURN FALSE
			ENDIF
			RETURN TRUE
		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_HuntDark(iHash)
			PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - IS_THIS_ROCKSTAR_MISSION_NEW_VS_HuntDark")
			IF g_sMPTunables.bDisableRedInvite_HUNT_DARK       
				PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - bDisableRedInvite_HUNT_DARK - RETURN FALSE")
				RETURN FALSE
			ENDIF
			RETURN TRUE
		ELIF IS_THIS_ROCKSTAR_MISSION_NEW_VS_RunningBack(iHash)
			PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - IS_THIS_ROCKSTAR_MISSION_NEW_VS_RunningBack")
			IF g_sMPTunables.bDisableRedInvite_RUNNING_BACK    
				PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - bDisableRedInvite_RUNNING_BACK - RETURN FALSE")
				RETURN FALSE
			ENDIF
			RETURN TRUE
		ENDIF
		
		// We are now happy for all future adversary modes to be red
		IF IS_THIS_AN_ADVERSARY_MODE_MISSION(iHash, iAdversaryModeType)
			PRINTLN(".KGM [JobList]: SHOULD_THIS_BG_BE_RED - IS_THIS_AN_ADVERSARY_MODE_MISSION() = TRUE")
			RETURN TRUE
		ENDIF		
	ENDIF
	RETURN FALSE

ENDFUNC

// -----------------------------------------------------------------------------------------------------------
// PURPOSE:	Set the background colour for this invite
//
// INPUT PARAMS:		paramType		The FMMC Type of the Job
//						paramSubtype	The Subtype
PROC Set_Feed_Background_Colour_For_Invite(INT paramType, INT paramSubtype, INT iHash = 0, INT iAdversaryModeType = 0)
	PRINTLN(".KGM [JobList][Heist]: Set_Feed_Background_Colour_For_Invite:paramType = ", paramType, " paramSubtype = ", paramSubtype, " iHash = ", iHash, " iAdversaryModeType = ", iAdversaryModeType)
	IF GET_IS_LOADING_SCREEN_ACTIVE()
		EXIT
	ENDIF
	IF (paramType = FMMC_TYPE_LOW_FLOW_PHONE_INVITE)
		THEFEED_SET_BACKGROUND_COLOR_FOR_NEXT_POST(HUD_COLOUR_LOW_FLOW)
		PRINTLN(".KGM [JobList][Heist]: Set_Feed_Background_Colour_For_Invite: LowFlow-Related Invite getting background colour: HUD_COLOUR_LOW_FLOW")
		EXIT	
	ELIF CASINO_HEIST_FLOW_IS_A_CASINO_HEIST_FINALE_FROM_ROOT_ID(iHash)	
	#IF FEATURE_HEIST_ISLAND
	OR HEIST_ISLAND_FLOW_IS_A_HEIST_ISLAND_FINALE_FROM_ROOT_ID(iHash)
	#ENDIF
		PRINTLN(".KGM [JobList][Heist]: Set_Feed_Background_Colour_For_Invite: CASINO_HEIST_FLOW_IS_A_CASINO_HEIST_FINALE_FROM_ROOT_ID colour: HUD_COLOUR_HEIST_BACKGROUND")
		THEFEED_SET_BACKGROUND_COLOR_FOR_NEXT_POST(HUD_COLOUR_HEIST_BACKGROUND)
		EXIT	
	ELIF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(iHash)	
		PRINTLN(".KGM [JobList][Heist]: Set_Feed_Background_Colour_For_Invite: GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID colour: HUD_COLOUR_HDARK")
		THEFEED_SET_BACKGROUND_COLOR_FOR_NEXT_POST(HUD_COLOUR_HDARK)
		EXIT	
	ELIF ((paramSubtype = FMMC_MISSION_TYPE_HEIST) OR (paramSubtype = FMMC_MISSION_TYPE_PLANNING))
	AND ((paramType = FMMC_TYPE_MISSION) OR (paramType = FMMC_TYPE_HEIST_PHONE_INVITE) OR (paramType = FMMC_TYPE_HEIST_PHONE_INVITE_2))	
		PRINTLN(".KGM [JobList][Heist]: Set_Feed_Background_Colour_For_Invite: Heist-Related Invite getting background colour: HUD_COLOUR_HEIST_BACKGROUND")
		THEFEED_SET_BACKGROUND_COLOR_FOR_NEXT_POST(HUD_COLOUR_HEIST_BACKGROUND)
		EXIT	
	ELIF ((paramSubtype = FMMC_MISSION_TYPE_FLOW_MISSION AND paramType = FMMC_TYPE_MISSION)
	OR LOW_FLOW_IS_THIS_MISSION_LOW_FLOW_FROM_ROOT_HASH(iHash))
		PRINTLN(".KGM [JobList][Heist]: Set_Feed_Background_Colour_For_Invite: New Flow-Related Invite getting background colour: HUD_COLOUR_LOW_FLOW")
		THEFEED_SET_BACKGROUND_COLOR_FOR_NEXT_POST(HUD_COLOUR_LOW_FLOW)
		EXIT	
	ELIF g_sMPTunables.bRED_INVITE
		IF SHOULD_THIS_BG_BE_RED(iHash, iAdversaryModeType)
			THEFEED_SET_BACKGROUND_COLOR_FOR_NEXT_POST(HUD_COLOUR_ADVERSARY)
			PRINTLN(".KGM [JobList][Heist]: Set_Feed_Background_Colour_For_Invite: NewVs-Related Invite getting background colour: HUD_COLOUR_RED")
			EXIT	
		ENDIF
	ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Clear the Feed Flash style for Important Feed message
PROC Clear_Feed_Flash_Style_For_Invites()
    THEFEED_RESET_ALL_PARAMETERS()
ENDPROC




// ===========================================================================================================
//      JobList Invite Flash Effect Functions
// ===========================================================================================================

// PURPOSE: Let the cellphone system know what the most recent script feed message being displayed should be - allows autoLaunch when dpadUp held
//
// INPUT PARAMS:        paramFeedID         The FeedID
PROC Store_Feed_Message_As_Most_Recent_For_AutoLaunch(INT paramFeedID)

    g_Last_App_ActivatableType      = ACT_APP_INVITE
    g_Last_App_ActivatableFeedID    = paramFeedID

ENDPROC




// ===========================================================================================================
//      JobList Invite Received Flag Access Functions
//      NOTE: Added for DaveW so that the help text about Invites can be repeated more than once
// ===========================================================================================================

// PURPOSE: Set that a new Invite has been accepted and displayed.
PROC Set_New_Invite_Received()

    g_joblistInviteReceived = TRUE
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Set_New_Invite_Received().")
        NET_NL()
    #ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Clear the flag that indicates a new Invite has been accepted.
PROC Clear_New_Invite_Received()

    g_joblistInviteReceived = FALSE
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Clear_New_Invite_Received() requested by: ")
        NET_PRINT(GET_THIS_SCRIPT_NAME())
        NET_PRINT(" - this may be an auto-clear from the 'check' routine.")
        NET_NL()
    #ENDIF
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a new Invite has been accepted and clear the flag.
//
// RETURN VALUE:            BOOL            TRUE if an invite has been received since the last check, otherwise FALSE
FUNC BOOL Check_If_New_Invite_Received_And_Clear_Flag()

    BOOL inviteReceived = g_joblistInviteReceived
    
    IF NOT (inviteReceived)
        RETURN FALSE
    ENDIF
    
    // Clear the flag
    Clear_New_Invite_Received()
    
    RETURN (inviteReceived)
    
ENDFUNC




// ===========================================================================================================
//      New Jobs Indicator
// ===========================================================================================================

// PURPOSE: Dynamically update the indicator if the cellphone homepage is visible
PROC Dynamically_Update_New_Invites_Indicator()

    //Steve T. If the phone is on the homescreen when an invite comes in, this routine will dynamically update the red indicator overlay. 
    //It uses g_numNewJoblistInvites specified above to decide the number that should be displayed. Scaleform_Place... done in cellphone_private.
    //TODO 1567234 

    IF NOT g_Homescreen_In_Secondary_Mode
        IF g_Cellphone.PhoneDS = PDS_MAXIMUM //Only dynamically update number of texts notifications when user is on first homescreen page.
        
            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex,"SET_DATA_SLOT_EMPTY", 1)
   
            Scaleform_Place_Items_on_Homescreen(1)

            LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_MovieIndex, "DISPLAY_VIEW", 1, TO_FLOAT(AppCursorIndex))

        ENDIF
    ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Increment New Invites Indicator
// NOTES:	Now increments the g_numNewJoblistInvites by 1 everytime it is called
PROC Increment_New_Invites_Indicator()
	
	g_numNewJoblistInvites++	// MMM 1695633 - Changed this so that it will increment the counter again
	Dynamically_Update_New_Invites_Indicator()
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [NewJobInv][JobList]: ")
		NET_PRINT_TIME()
		NET_PRINT("Increasing New Invite Indicator to ")
		NET_PRINT_INT(g_numNewJoblistInvites)
        NET_NL()
    #ENDIF

ENDPROC

PROC Reset_New_Invites_Indicator(STRING debugText)
	IF g_numNewJoblistInvites != 0
		PRINTLN("[MMM] [NewJobInv][JobList] Resetting New Invites Indicator to 0, previous value was : ", g_numNewJoblistInvites)
		
		
		IF NOT IS_STRING_NULL_OR_EMPTY(debugText)
			PRINTLN(debugText)
		ENDIF
		g_numNewJoblistInvites = 0
	ENDIF
ENDPROC

// PURPOSE: Decrements the new invites indicator, compliments the Increment function
// NOTES: Does error checking and ensures that the value does not go below zero. Added for bug 1695633
PROC Decrement_New_Invites_Indicator()

	PRINTLN("[MMM] [NewJobInv][JobList] Decrementing. The counter was ", g_numNewJoblistInvites)
	g_numNewJoblistInvites--
	PRINTLN("[MMM][NewJobInv][JobList] and is now ", g_numNewJoblistInvites)
	
	IF g_numNewJoblistInvites < 0
		PRINTLN("[MMM][NewJobInv][JobList] Decrement_New_Invites_Indicator:ERROR(g_numNewJoblistInvites < 0), setting it to 0")
		//SCRIPT_ASSERT("Decrement_New_Invites_Indicator: ERROR (g_numNewJoblistInvites < 0), setting to 0. Tell Mark Miller.")
		Reset_New_Invites_Indicator("[MMM][NewJobInv][JobList] Calling Reset_New_Invites_Indicator from Decrement_New_Invites_Indicator")
	ENDIF
	
	Dynamically_Update_New_Invites_Indicator()	// Update the indicator after we remove the invite 
												// (since we may be looking at the screen also)
ENDPROC




// ===========================================================================================================
//      Recent JobList Invite Functions
// ===========================================================================================================

// PURPOSE: Set the Recent Invite timeout value again - this is used to block phonecalls within a period of time after an invite is received
PROC Update_Recent_Invite_Received_Timer()

    g_inviteRecentlyReceivedTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), RECENT_INVITE_TIME_msec)
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: Updating 'Recent Invite' Timeout time. Current Time: ")
        NET_PRINT(GET_TIME_AS_STRING(GET_NETWORK_TIME()))
        NET_PRINT("  New Timeout: ")
        NET_PRINT(GET_TIME_AS_STRING(g_inviteRecentlyReceivedTimeout))
        NET_NL()
    #ENDIF

ENDPROC




// ===========================================================================================================
//      Display/Closed JobList Invite and Cross-Session Invite Functions
// ===========================================================================================================

// PURPOSE: Check if the invitation in the invitation slot is allowed to be displayed
//
// INPUT PARAMS:        paramInvitationSlot         The array position within the Invitations list
// RETURN VALUE:        BOOL                        TRUE if the invitation is still allowed to be on display
FUNC BOOL Is_Invitation_In_Slot_Allowed_To_Be_Displayed(INT paramInvitationSlot)
    RETURN (g_sJLInvitesMP[paramInvitationSlot].jliAllowDisplay)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the invitation in the Cross-Session Invite slot is allowed to be displayed
//
// INPUT PARAMS:        paramInvitationSlot         The array position within the CSInvite list
// RETURN VALUE:        BOOL                        TRUE if the invitation is still allowed to be on display
FUNC BOOL Is_CSInvite_In_Slot_Allowed_To_Be_Displayed(INT paramInvitationSlot)
    RETURN (g_sJLCSInvitesMP[paramInvitationSlot].jcsiAllowDisplay)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the invitation in the Basic Invite slot is allowed to be displayed
//
// INPUT PARAMS:        paramInvitationSlot         The array position within the RVInvite list
// RETURN VALUE:        BOOL                        TRUE if the invitation is still allowed to be on display
FUNC BOOL Is_RVInvite_In_Slot_Allowed_To_Be_Displayed(INT paramInvitationSlot)
    RETURN (g_sJLRVInvitesMP[paramInvitationSlot].jrviAllowDisplay)
ENDFUNC




// ===========================================================================================================
//      Duplicate JobList Invite Functions
// ===========================================================================================================

// PURPOSE: Check if the invitation in the invitation slot is a duplicate of an existing entry from a different player
//
// INPUT PARAMS:        paramInvitationSlot         The array position within the Invitations list
// RETURN VALUE:        BOOL                        TRUE if the invitation is a duplicate
FUNC BOOL Is_Invitation_In_Slot_A_Duplicate(INT paramInvitationSlot)
    RETURN (g_sJLInvitesMP[paramInvitationSlot].jliIsDuplicate)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if there is an invite with the same details that isn't already marked as a duplicate
//
// INPUT PARAMS:            paramInviteSlot         The slot being checked to see if it is a duplicate of any of the other slots 
// RETURN VALUE:            BOOL                    TRUE if any of the other slots contain the same details but are not classed as a DUPLICATE
//
// NOTES:   The first invite for a mission will not be a duplicate. Further invites will be classed as duplicates because they match the
//              same details as the first invite and the first invite has the DUPLICATE flag set to FALSE. If the first invite gets removed
//              then all remaining duplicate invites will still have the DUPLICATE flag set to TRUE, but the first duplicate to call this
//              function will not find an entry with matching details that has the DUPLICATE flag set to FALSE and so will set its own DUPLICATE
//              flag to FALSE.
FUNC BOOL Check_If_Duplicate_Invite(INT paramInviteSlot)

    // If there are no Invites then this can't be a duplicate
    IF (g_numMPJobListInvitations = 0)
        RETURN FALSE
    ENDIF

    INT     tempLoop                    = 0
    BOOL    checkingEntryTakesPriority  = FALSE
    
    REPEAT g_numMPJobListInvitations tempLoop
        // Ignore entries already marked as a duplicate - we're only interested in non-duplicate entries
        IF NOT (Is_Invitation_In_Slot_A_Duplicate(tempLoop))
            // Not a duplicate, so ensure it isn't this entry
            IF (tempLoop != paramInviteSlot)
                // Not this entry, so check for matching details
                IF  (g_sJLInvitesMP[tempLoop].jliType       = g_sJLInvitesMP[paramInviteSlot].jliType)
                AND (g_sJLInvitesMP[tempLoop].jliCreatorID  = g_sJLInvitesMP[paramInviteSlot].jliCreatorID)
                AND (ARE_VECTORS_ALMOST_EQUAL(g_sJLInvitesMP[tempLoop].jliCoords, g_sJLInvitesMP[paramInviteSlot].jliCoords))
				AND (ARE_STRINGS_EQUAL(g_sJLInvitesMP[tempLoop].jliContentID, g_sJLInvitesMP[paramInviteSlot].jliContentID))
                    // Found a matching entry, so this entry is a duplicate, but check for any conditions that mean this entry should take priority
                    checkingEntryTakesPriority = FALSE
                    
                    // If the existing non-duplicate invite is from an in-game character then it should become the duplicate allowing this checking entry to become active
                    /*
					IF (g_sJLInvitesMP[paramInviteSlot].jliInvitorIsPlayer)
                    AND NOT (g_sJLInvitesMP[tempLoop].jliInvitorIsPlayer)
                        checkingEntryTakesPriority = TRUE
                        
                        NET_PRINT("...KGM MP [JobList]: ")
                        NET_PRINT_TIME()
                        NET_PRINT("Found an existing entry from a NPC not marked as a duplicate, so marking it as a duplicate instead of this one. Other Slot: ")
                        NET_PRINT_INT(tempLoop)
                        NET_PRINT("   [")
                        TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLInvitesMP[tempLoop].jliNPC)
                        NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
                        NET_PRINT("]")
                        NET_NL()
						
                    ENDIF
                    */
					
                    // If the existing non-duplicate invite is closed then mark it as a duplicate to allow this checking entry invite to take priority if it is still active
                    IF NOT (checkingEntryTakesPriority)
                        IF (Is_Invitation_In_Slot_Allowed_To_Be_Displayed(paramInviteSlot))
                        AND NOT (Is_Invitation_In_Slot_Allowed_To_Be_Displayed(tempLoop))
                            checkingEntryTakesPriority = TRUE
                        
                            NET_PRINT("...KGM MP [JobList]: ")
                            NET_PRINT_TIME()
                            NET_PRINT("Found an existing closed entry not marked as a duplicate, so marking it as a duplicate instead of this open invitation. Other Slot: ")
                            NET_PRINT_INT(tempLoop)
                            NET_NL()
                        ENDIF
                    ENDIF
                    
					// Only mark it as a duplicate if the invitors of both invites are the same type (NPC/Player)
					IF (g_sJLInvitesMP[paramInviteSlot].jliInvitorIsPlayer AND g_sJLInvitesMP[tempLoop].jliInvitorIsPlayer)
					OR (NOT g_sJLInvitesMP[paramInviteSlot].jliInvitorIsPlayer AND NOT g_sJLInvitesMP[tempLoop].jliInvitorIsPlayer)
	                    IF (checkingEntryTakesPriority)
	                        // ...the checking entry takes priority over the existing non-duplicate entry, so mark the existing entry as a duplicate
	                        g_sJLInvitesMP[tempLoop].jliIsDuplicate = TRUE
	                    ELSE
	                        // ...this is a duplicate
	                        RETURN TRUE
	                    ENDIF
					ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDREPEAT
    
    // Didn't find a non-duplicate entry with matching details
    #IF IS_DEBUG_BUILD
        // Output debug if this entry has just been marked as Not A Duplicate
        IF (Is_Invitation_In_Slot_A_Duplicate(paramInviteSlot))
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Invite is not a duplicate. Invite Slot: ")
            NET_PRINT_INT(paramInviteSlot)
            NET_PRINT("   [")
            IF (g_sJLInvitesMP[paramInviteSlot].jliInvitorIsPlayer)
                NET_PRINT(GET_PLAYER_NAME(g_sJLInvitesMP[paramInviteSlot].jliPlayer))
            ELSE
                TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLInvitesMP[paramInviteSlot].jliNPC)
                NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            ENDIF
            NET_PRINT("]")
            NET_NL()
        ENDIF
    #ENDIF
            
    RETURN FALSE

ENDFUNC




// ===========================================================================================================
//      Fake JobList Invite Data Access Functions
// ===========================================================================================================

// PURPOSE: Check if a fake joblist Invite has been received
//
// RETURN VALUE:            BOOL            TRUE if a fake joblist invite has been received, FALSE if not
FUNC BOOL Has_Fake_Joblist_Invite_Been_Received()
    RETURN (g_fakeJoblistInvite.fjiReceived)
ENDFUNC




// ===========================================================================================================
//      Corona Spaces Availability Functions
// ===========================================================================================================

// PURPOSE: Only allow Invites to be available if there is still space in tehcorona for another player
//
// INPUT PARAMS:        paramInviteArrayPos     The Invite array position being checked
// RETURN VALUE:        BOOL                    TRUE if the invite should still be available, FALSE if not
FUNC BOOL Check_If_Corona_Still_Has_Space_For_More_Players(INT paramInviteArrayPos)
    
    // Always return 'YES' if the invitor is an in-game character
    IF NOT (g_sJLInvitesMP[paramInviteArrayPos].jliInvitorIsPlayer)
        RETURN TRUE
    ENDIF
    
    // Use the invitor's player index in order to get the timer from the person in charge of the vote
    PLAYER_INDEX    theInvitor      = g_sJLInvitesMP[paramInviteArrayPos].jliPlayer
    
    // Check if the invitor is still allocated to a mission on the Mission Controller
    // KGM 25/1/13: For simplicity, this doesn't take CnC team information into account just now
    RETURN (Can_Another_Player_Be_Reserved_On_This_Players_Mission(theInvitor))

ENDFUNC





// ===========================================================================================================
//      JobList App Functions
// ===========================================================================================================

// PURPOSE: Check if the Joblist App is active
//
// RETURN VALUE:            BOOL            TRUE if the Joblist App is on-screen, FALSE if not
FUNC BOOL Is_Joblist_App_On_Screen()
    RETURN (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appMPJobListNEW")) > 0)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Allow the Joblist App to auto-launch next time the cellphone is taken out
PROC Allow_Joblist_App_Autolaunch()
    
    g_autolaunchJoblistApp = TRUE
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Joblist App Auto-launch flag being set to TRUE (NOTE: It may already have been TRUE)") NET_NL()
    #ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Allow the Joblist App to auto-launch next time the cellphone is taken out
PROC Cancel_Joblist_App_Autolaunch()

    g_autolaunchJoblistApp = FALSE
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Joblist App Auto-launch flag being set to FALSE") NET_NL()
    #ENDIF

ENDPROC

FUNC BOOL IS_GAMERHANDLE_BLOCKED(GAMER_HANDLE tGH)

	IF NOT IS_GAMER_HANDLE_VALID(tGH)
		RETURN TRUE
	ENDIF

	RETURN NETWORK_IS_PLAYER_ON_BLOCKLIST(tGH)
ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain the global that determines whether the Joblist App should auto-launch or not
// NOTES:   This global gets set when a new Invite becomes active andcleared when all Invites have been read (ie: listed on the Joblist App)
PROC Maintain_Joblist_App_Autolaunch()

    // Nothing to do if the auto-launch flag isn't set
    IF NOT (g_autolaunchJoblistApp)
        EXIT
    ENDIF
    
    // The auto-launch flag is set
    // KGM 4/12/12: Maintain this if a fake Joblist Invite for the tutorial has been received but not accepted by the player
    IF (Has_Fake_Joblist_Invite_Been_Received())
    AND NOT (g_fakeJoblistInvite.fjiAccepted)
        EXIT
    ENDIF
    
    // Check real joblist entries
    // Check if there are any entries that should still keep the flag active (if any are not read then keep the flag active)
    INT tempLoop = 0
    
    REPEAT g_numMPJobListInvitations tempLoop
        IF (g_sJLInvitesMP[tempLoop].jliActive)
            IF NOT (g_sJLInvitesMP[tempLoop].jliInviteRead)
                // ...Active Invite not read, so keep the flag set
                EXIT
            ENDIF
        ENDIF
    ENDREPEAT
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("All Active Joblist Invites have been read") NET_NL()
    #ENDIF

    // All active invites are read, so clear the flag
    Cancel_Joblist_App_Autolaunch()

ENDPROC



// ===========================================================================================================
//      Maintain DPADUP Invite Help Flag
// ===========================================================================================================

// PURPOSE: Set a flag when the DPADUP Invite help is allowed to be displayed and clear it again when it's not - for Dave
PROC Maintain_DPADUP_Invite_Help_Flag()

    // Is there a most recent feed on display?
    INT currentFeedEntry = THEFEED_GET_LAST_SHOWN_PHONE_ACTIVATABLE_FEED_ID()
    IF (currentFeedEntry = -1)
        // ...no, so clear the invite help flag
        CLEAR_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_DO_QUICK_INVITE)
        
        EXIT
    ENDIF
    
    // There is a most recent feed entry, so set the invite help flag if the feed entry is a joblist invite
    IF (currentFeedEntry != g_Last_App_ActivatableFeedID)
        EXIT
    ENDIF
    
    IF (g_Last_App_ActivatableType = ACT_APP_INVITE)
        SET_BIT(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_DO_QUICK_INVITE)
    ENDIF

ENDPROC



// ===========================================================================================================
//      Maintain Post Skyswoop Invite Flag
// ===========================================================================================================

// PURPOSE:	Introduces a delay after a skyswoop before an invite gets displayed on the feed
// NOTES:	KGM 7/11/14 [BUG 2108567]: Early invites sent to the feed on entry from SP seem to get cleared a few frames later - I'm assuming by some feed reset somewhere.
//				This short delay is to try to avoid that.
//			KGM 9/11/14 [BUG 2119263]: Introduce a delay after teleporting and screen being not faded in too.
PROC Maintain_Post_Player_Warp_Delay()

	IF NOT (NETWORK_IS_GAME_IN_PROGRESS())
		EXIT
	ENDIF
	
	IF (IS_SKYSWOOP_AT_GROUND())
	AND NOT (IS_PLAYER_TELEPORT_ACTIVE())
	AND (IS_SCREEN_FADED_IN())
	AND NOT (g_bCelebrationScreenIsActive)
	AND NOT (IS_POST_MISSION_SCENE_ACTIVE())
		EXIT
	ENDIF
	
	// Post Player Warp, so set a short timeout delay
	g_sJoblistUpdate.postPlayerWarpDelay = GET_GAME_TIMER() + POST_PLAYER_WARP_INVITE_DISPLAY_DELAY_msec

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the post-skyswoop delay has expired
//
// RETURN VALUE:			BOOL		TRUE if the delay has expired, FALSE if not
FUNC BOOL Has_Post_Player_Warp_Delay_Expired()

	IF (g_sJoblistUpdate.postPlayerWarpDelay = 0)
		RETURN TRUE
	ENDIF
	
	IF (GET_GAME_TIMER() > g_sJoblistUpdate.postPlayerWarpDelay)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if the invite should be delayed due to a player warp of some description
//
// RETURN VALUE:			BOOL		TRUE if there is a player warp delay, FALSE if not
//
// NOTES:	KGM 7/11/14 [BUG 2108567]: Early invites sent to the feed on entry from SP seem to get cleared a few frames later - I'm assuming by some feed reset somewhere.
//				This short delay is to try to avoid that.
//			KGM 9/11/14 [BUG 2119263]: Introduce a delay after teleporting and screen being not faded in too.
FUNC BOOL Is_There_An_Active_Player_Warp_Delay()

	IF NOT (IS_SKYSWOOP_AT_GROUND())
		RETURN TRUE
	ENDIF
	
	IF (IS_PLAYER_TELEPORT_ACTIVE())
		RETURN TRUE
	ENDIF
	
	IF NOT (IS_SCREEN_FADED_IN())
		RETURN TRUE
	ENDIF
	
	IF NOT (Has_Post_Player_Warp_Delay_Expired())
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


// ===========================================================================================================
//      Fake JobList Invite Functions
// ===========================================================================================================

// PURPOSE: Set the fake joblist invite as 'accepted' so that the calling script can be informed
PROC Set_Fake_Joblist_Invite_As_Accepted_By_Player()

    // Ensure there is an active fake joblist invite
    IF NOT (Has_Fake_Joblist_Invite_Been_Received())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("ERROR: Set_Fake_Joblist_Invite_As_Accepted_By_Player() - Fake Joblist Invite has not been received") NET_NL()
            SCRIPT_ASSERT("Set_Fake_Joblist_Invite_As_Accepted_By_Player(): A fake joblist invite for tutorial hasn't been received. Tell Keith.")
        #ENDIF
        
        EXIT
    ENDIF

    // Ignore if the fake joblist invite has already been accepted
    IF (g_fakeJoblistInvite.fjiAccepted)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("ERROR: Set_Fake_Joblist_Invite_As_Accepted_By_Player() - Fake Joblist Invite has already been accepted") NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // Set the joblist invite as accepted
    g_fakeJoblistInvite.fjiAccepted = TRUE
    
    // Kill the feed message if it is still on display
    THEFEED_REMOVE_ITEM(g_fakeJoblistInvite.fjiFeedID)
    
    // Don't auto-launch the joblist next time the cellphone is brought up
    Cancel_Joblist_App_Autolaunch()

ENDPROC



// ===========================================================================================================
//      JobList Clear Functions
// ===========================================================================================================

// PURPOSE: Clear the JobList array
PROC Clear_Joblist_Details()

    INT tempLoop = 0
    
    REPEAT MAX_MP_JOBLIST_ENTRIES tempLoop
        g_sJobListMP[tempLoop].jlState          = NO_MP_JOBLIST_ENTRY
        g_sJobListMP[tempLoop].jlUniqueID       = NO_UNIQUE_ID
        g_sJobListMP[tempLoop].jlMissionAsInt   = ENUM_TO_INT(eNULL_MISSION)
		g_sJoblistMP[tempLoop].jlSubtype		= -1
        g_sJobListMP[tempLoop].jlInstance       = DEFAULT_MISSION_INSTANCE
        g_sJobListMP[tempLoop].jlBitfield       = ALL_MISSION_CONTROL_BITS_CLEAR
        g_sJobListMP[tempLoop].jlCoords         = << 0.0, 0.0, 0.0 >>
        g_sJobListMP[tempLoop].jlInvitorName    = ""
        g_sJobListMP[tempLoop].jlMissionName    = ""
        g_sJobListMP[tempLoop].jlDescription    = ""
        g_sJobListMP[tempLoop].jlDescHash       = 0
		g_sJobListMP[tempLoop].jlMinPlayers		= 0
		g_sJobListMP[tempLoop].jlMaxPlayers		= 0
		g_sJobListMP[tempLoop].jlWagerH2H		= -1
		g_sJobListMP[tempLoop].jlHeistPackedInt	= 0
		g_sJobListMP[tempLoop].jlOrderIndex		= -1
		g_sJobListMP[tempLoop].jlIsPushBikeOnly	= FALSE
		g_sJobListMP[tempLoop].jlPropertyType		= 0
		
    ENDREPEAT
    
    g_numMPJobListEntries = 0

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Clear one Invite array entry
//
// INPUT PARAMS:            paramSlot           Invitation Array position to be cleared
PROC Clear_One_Invite_Array_Slot(INT paramSlot)

    g_sJLInvitesMP[paramSlot].jliActive                 = FALSE
    g_sJLInvitesMP[paramSlot].jliAllowDisplay           = FALSE
    g_sJLInvitesMP[paramSlot].jliIsDuplicate            = FALSE
    g_sJLInvitesMP[paramSlot].jliInviteRead             = FALSE
	g_sJLInvitesMP[paramSlot].jliReAdding				= FALSE
    g_sJLInvitesMP[paramSlot].jliCoords                 = << 0.0, 0.0, 0.0 >>
    g_sJLInvitesMP[paramSlot].jliPlayer                 = INVALID_PLAYER_INDEX()
    g_sJLInvitesMP[paramSlot].jliNPC                    = NO_CHARACTER
    g_sJLInvitesMP[paramSlot].jliInvitorIsPlayer        = TRUE
    g_sJLInvitesMP[paramSlot].jliCreatorID              = ENUM_TO_INT(eNULL_MISSION)
    g_sJLInvitesMP[paramSlot].jliType                   = -1
	g_sJLInvitesMP[paramSlot].jliSubtype				= -1
    g_sJLInvitesMP[paramSlot].jliDescHash               = 0
    g_sJLInvitesMP[paramSlot].jliDescLoadVars.nHashOld  = 0
    g_sJLInvitesMP[paramSlot].jliDescLoadVars.iRequest  = 0
    g_sJLInvitesMP[paramSlot].jliDescLoadVars.bSucess   = FALSE
    g_sJLInvitesMP[paramSlot].jliRequireDesc            = FALSE
	g_sJLInvitesMP[paramSlot].jliUseHeistCutsceneDesc	= FALSE
    g_sJLInvitesMP[paramSlot].jliMissionName            = ""
    g_sJLInvitesMP[paramSlot].jliDescription            = ""
    g_sJLInvitesMP[paramSlot].jliFeedID                 = NO_JOBLIST_INVITE_FEED_ID
	g_sJLInvitesMP[paramSlot].jliOnHours				= MATC_ALL_DAY
	g_sJLInvitesMP[paramSlot].jliToDownload				= FALSE
	g_sJLInvitesMP[paramSlot].jliDownloading			= FALSE
	g_sJLInvitesMP[paramSlot].jliDownloaded				= FALSE
	g_sJLInvitesMP[paramSlot].jliMinPlayers				= 0
	g_sJLInvitesMP[paramSlot].jliMaxPlayers				= 0
	g_sJLInvitesMP[paramSlot].jliWagerH2H				= -1
	g_sJLInvitesMP[paramSlot].jliIsUnplayed				= FALSE
	g_sJLInvitesMP[paramSlot].jliHeistPackedInt			= 0
    g_sJLInvitesMP[paramSlot].jliOrderIndex				= -1
	g_sJLInvitesMP[paramSlot].jliIsPushBikeOnly			= FALSE
	g_sJLInvitesMP[paramSlot].jliRepeatInvite			= FALSE
	g_sJLInvitesMP[paramSlot].jliRepeatTimeout			= 0
	g_sJLInvitesMP[paramSlot].jliPermissionCheckID		= 0
	g_sJLInvitesMP[paramSlot].jliDoingPermissionCheck	= FALSE
	g_sJLInvitesMP[paramSlot].jliPermissionCheckFail	= FALSE
	g_sJLInvitesMP[paramSlot].jliDoGamerToGamerCheck	= FALSE
	
    IF (NETWORK_IS_GAME_IN_PROGRESS())
        g_sJLInvitesMP[paramSlot].jliHeadshotTimeout    = GET_NETWORK_TIME()
        g_sJLInvitesMP[paramSlot].jliAllowAgainTimeout  = GET_NETWORK_TIME()
    ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Clear the Invite array and any playerBD Invite data
PROC Clear_Invite_Details()

    INT tempLoop = 0
    
    REPEAT MAX_MP_JOBLIST_ENTRIES tempLoop
        Clear_One_Invite_Array_Slot(tempLoop)
    ENDREPEAT
    
    g_numMPJobListInvitations   = 0
    g_joblistInviteReceived     = FALSE
    
    // Set the initial 'recent invite timeout' time
    IF (NETWORK_IS_GAME_IN_PROGRESS())
        g_inviteRecentlyReceivedTimeout = GET_NETWORK_TIME()
    ENDIF
    
    IF NOT IS_TRANSITION_SESSION_RESTARTING()
    AND NOT IS_TRANSITION_SESSION_LAUNCHING()
        // Clear PlayerBD
        invitePlayerAptData.ipInviteApartment   = -1
        PRINTLN("Clear_Invite_Details: invitePlayerAptData.ipInviteApartment = -1")
        CLEAR_GAMER_HANDLE_STRUCT(invitePlayerAptData.ipInvitingPlayer)
    ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Clear one Cross-Session Invite array entry
//
// INPUT PARAMS:            paramSlot           CSInvite Array position to be cleared
PROC Clear_One_CSInvite_Array_Slot(INT paramSlot)

    g_sJLCSInvitesMP[paramSlot].jcsiActive                  = FALSE
    g_sJLCSInvitesMP[paramSlot].jcsiAllowDisplay            = FALSE
	g_sJLCSInvitesMP[paramSlot].jcsiInviteRead	            = FALSE
    g_sJLCSInvitesMP[paramSlot].jcsiInvitorGamerTag         = ""
    g_sJLCSInvitesMP[paramSlot].jcsiInvitorDisplayName      = ""
	g_sJLCSInvitesMP[paramSlot].jcsiDisplayNameID			= NO_DISPLAY_NAME_RETRIEVAL
	g_sJLCSInvitesMP[paramSlot].jcsiDisplayNameTimeout		= GET_GAME_TIMER()
    g_sJLCSInvitesMP[paramSlot].jcsiCreatorID               = ILLEGAL_CREATOR_ID
    g_sJLCSInvitesMP[paramSlot].jcsiType                    = -1
	g_sJLCSInvitesMP[paramSlot].jcsiSubtype					= -1
    g_sJLCSInvitesMP[paramSlot].jcsiDescHash                = 0
    g_sJLCSInvitesMP[paramSlot].jcsiDescLoadVars.nHashOld   = 0
    g_sJLCSInvitesMP[paramSlot].jcsiDescLoadVars.iRequest   = 0
    g_sJLCSInvitesMP[paramSlot].jcsiDescLoadVars.bSucess    = FALSE
    g_sJLCSInvitesMP[paramSlot].jcsiRequireDesc             = FALSE
    g_sJLCSInvitesMP[paramSlot].jcsiMissionName             = ""
    g_sJLCSInvitesMP[paramSlot].jcsiDescription             = ""
    g_sJLCSInvitesMP[paramSlot].jcsiPlaylistCurrent         = 0
    g_sJLCSInvitesMP[paramSlot].jcsiPlaylistTotal           = 0
    g_sJLCSInvitesMP[paramSlot].jcsiInviteID                = -1
    g_sJLCSInvitesMP[paramSlot].jcsiFeedID                  = NO_JOBLIST_INVITE_FEED_ID
	g_sJLCSInvitesMP[paramSlot].jcsiIsUnplayed				= FALSE
	g_sJLCSInvitesMP[paramSlot].jcsiOrderIndex				= -1
	g_sJLCSInvitesMP[paramSlot].jcsiIsPushBikeOnly			= FALSE
	g_sJLCSInvitesMP[paramSlot].jcsiRepeatInvite			= FALSE
	g_sJLCSInvitesMP[paramSlot].jcsiRepeatTimeout			= 0
	g_sJLCSInvitesMP[paramSlot].jcsiInvitorIsFriend			= FALSE
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Clear the Cross-Session Invite array
PROC Clear_CSInvite_Details()

    INT tempLoop = 0
    
    REPEAT MAX_MP_JOBLIST_ENTRIES tempLoop
        Clear_One_CSInvite_Array_Slot(tempLoop)
    ENDREPEAT
    
    g_numMPCSInvites    = 0

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Clear one Basic Invite array entry
//
// INPUT PARAMS:            paramSlot           RVInvite Array position to be cleared
PROC Clear_One_RVInvite_Array_Slot(INT paramSlot)

    g_sJLRVInvitesMP[paramSlot].jrviActive              	= FALSE
    g_sJLRVInvitesMP[paramSlot].jrviAccepted            	= FALSE
    g_sJLRVInvitesMP[paramSlot].jrviAllowDisplay        	= FALSE
	g_sJLRVInvitesMP[paramSlot].jrviInviteRead          	= FALSE
    g_sJLRVInvitesMP[paramSlot].jrviPlayer              	= INVALID_PLAYER_INDEX()
    g_sJLRVInvitesMP[paramSlot].jrviNPC                 	= NO_CHARACTER
    g_sJLRVInvitesMP[paramSlot].jrviInvitorIsPlayer     	= TRUE
    g_sJLRVInvitesMP[paramSlot].jrviType                	= -1
    g_sJLRVInvitesMP[paramSlot].jrviSubType             	= -1
    g_sJLRVInvitesMP[paramSlot].jrviMissionName         	= ""
    g_sJLRVInvitesMP[paramSlot].jrviFeedID              	= NO_JOBLIST_INVITE_FEED_ID
    g_sJLRVInvitesMP[paramSlot].jrviOptionalTL          	= ""
    g_sJLRVInvitesMP[paramSlot].jrviOptionalInt1        	= -1
    g_sJLRVInvitesMP[paramSlot].jrviOptionalInt2        	= -1
    g_sJLRVInvitesMP[paramSlot].jrviRequestClosed			= FALSE
    g_sJLRVInvitesMP[paramSlot].jrviAllowDelete				= FALSE
	g_sJLRVInvitesMP[paramSlot].jrviOrderIndex				= -1
	g_sJLRVInvitesMP[paramSlot].jrviPermissionCheckID		= 0
	g_sJLRVInvitesMP[paramSlot].jrviDoingPermissionCheck	= FALSE
	g_sJLRVInvitesMP[paramSlot].jrviPermissionCheckFail		= FALSE
	g_sJLRVInvitesMP[paramSlot].jrviRootId					= 0
	//g_sJLRVInvitesMP[paramSlot].jrviPropertyType					= FALSE
    
    IF (NETWORK_IS_GAME_IN_PROGRESS())
    	g_sJLRVInvitesMP[paramSlot].jrviClosedTimeout	= GET_NETWORK_TIME()
    ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Clear the Basic Invite array
PROC Clear_RVInvite_Details()

    INT tempLoop = 0
    
    REPEAT MAX_MP_JOBLIST_ENTRIES tempLoop
        Clear_One_RVInvite_Array_Slot(tempLoop)
    ENDREPEAT
    
    g_numMPRVInvites    = 0

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Clear the Joblist Warp Details
PROC Clear_Joblist_Warp_Details()

    g_structJobListWarpMP emptyJoblistWarp
    g_sJoblistWarpMP = emptyJoblistWarp
    
    g_sJoblistWarpMP.jlwCoords      = << 0.0, 0.0, 0.0 >>
    g_sJoblistWarpMP.jlwOffset      = << 0.0, 0.0, 0.0 >>

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Clear the Fake Joblist Details
PROC Clear_Fake_Invite_Details()

    g_structFakeJoblistInviteMP emptyFakeInvite
    g_fakeJoblistInvite = emptyFakeInvite

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Clear the Invite Acceptance Details
PROC Clear_Invite_Acceptance_Details()

    g_structInviteAcceptanceMP emptyInviteAcceptance
    g_sInviteAcceptanceMP = emptyInviteAcceptance
    
    g_sInviteAcceptanceMP.iaInviteCoords    = << 0.0, 0.0, 0.0 >>
    g_sInviteAcceptanceMP.iaSafetyTimeout   = GET_GAME_TIMER()    //Changed by Steve T from GET_NETWORK_TIME() at Keith's request to fix NT 1599405 

ENDPROC





// ===========================================================================================================
//      Freemode Specific functions
//      (This can probably be moved to a more general location)
// ===========================================================================================================

// PURPOSE:	Work out if this player's active heist strand is on the Prep Board or the Finale Board
//
// RETURN VALUE:		INT						Either FMMC_MISSION_TYPE_HEIST or FMMC_MISSION_TYPE_PLANNING, or -1
//
// NOTES:	Based on an email sent by Alastair 22/1/15 at 11:46: "Is heist planning complete - outside apartment"
FUNC INT Get_Leader_Active_Heist_Strand_At_Planning_Or_Finale()

	// Get the number of Prep Missions in the player's currently active Heist Strand
	TEXT_LABEL_23 thisContentID = ""
	
	// Is there an active Heist Strand?
	thisContentID = GET_HEIST_FINALE_STAT_DATA()
	IF (IS_STRING_NULL_OR_EMPTY(thisContentID))
	OR (ARE_STRINGS_EQUAL(".", thisContentID))
		PRINTLN(".KGM [JobList][Heist]: Get_Leader_Active_Heist_Strand_At_Planning_Or_Finale() - THERE IS NO ACTIVE HEIST STRAND")
		RETURN (-1)
	ENDIF
	
	INT tempLoop = 0
	INT numPreps = 0
	
	// Get the number of prep missions in the Heist Strand (NOTE: The stats for the Prep Missions run from 1 onwards)
	REPEAT MAX_HEIST_PLANNING_ROWS tempLoop
		// Hardcoded replacement for GET_CONTENTID_FOR_SLOT()
		SWITCH (tempLoop)
			CASE 0		thisContentID = GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_1)	BREAK
			CASE 1		thisContentID = GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_2)	BREAK
			CASE 2		thisContentID = GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_3)	BREAK
			CASE 3		thisContentID = GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_4)	BREAK
			CASE 4		thisContentID = GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_5)	BREAK
			CASE 5		thisContentID = GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_6)	BREAK
			CASE 6		thisContentID = GET_MP_STRING_CHARACTER_STAT(MP_STAT_HEIST_MISSION_RCONT_ID_7)	BREAK
			
			DEFAULT
				SCRIPT_ASSERT("Get_Leader_Active_Heist_Strand_At_Planning_Or_Finale() - ERROR: MAX_HEIST_PLANNING_ROWS must have increased in size")
				thisContentID = ""
				BREAK
		ENDSWITCH
		
		// Is this an active Prep mission?
		IF NOT (IS_STRING_NULL_OR_EMPTY(thisContentID))
		AND NOT (ARE_STRINGS_EQUAL(".", thisContentID))
			numPreps++
		ENDIF
	ENDREPEAT
	
	// Ensure there are Prep Missions
	IF (numPreps = 0)
		PRINTLN(".KGM [JobList][Heist]: Get_Leader_Active_Heist_Strand_At_Planning_Or_Finale() - THERE ARE NO PREP MISSIONS IN ACTIVE HEIST")
		RETURN (-1)
	ENDIF
	
	// Find out how many Prep Missions have been completed
	INT bitsetPrepsComplete = GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_PLANNING_STAGE)
	INT bitRange			= 0
	
	// Have all the found Prep Missions been completed?
	REPEAT numPreps tempLoop
		// Hardcoded replacement for GET_BIT_RANGE_FROM_MISSION_ORDER() which I can't access from here
		bitRange = tempLoop * MAX_HEIST_BITFIELD_SIZE
		
		IF (bitRange != -1)
			IF NOT (IS_BIT_SET(bitsetPrepsComplete, bitRange))
				// Prep Not Complete, so return PREP MISSION
				PRINTLN(".KGM [JobList][Heist]: Get_Leader_Active_Heist_Strand_At_Planning_Or_Finale() - Return FMMC_MISSION_TYPE_PLANNING")
				RETURN (FMMC_MISSION_TYPE_PLANNING)
			ENDIF
			
		ENDIF
	ENDREPEAT
	
	// All Preps Complete
	PRINTLN(".KGM [JobList][Heist]: Get_Leader_Active_Heist_Strand_At_Planning_Or_Finale() - Return FMMC_MISSION_TYPE_HEIST")
	RETURN (FMMC_MISSION_TYPE_HEIST)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the cellID that corresponds to the current FM activity
//
// INPUT PARAMS:        paramMissionFM          FM Mission as INT
// INPUT PARAMS:        paramSubtype			Mission Subtype, or -1 if not known or required
// RETURN VALUE:        STRING                  The Text Label
FUNC STRING Get_FM_Joblist_Activity_TextLabel(INT paramMissionFM, INT paramSubtype = -1, INT propertyType = -1, INT iRootId = -1)
	DEBUG_PRINTCALLSTACK()
	IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(iRootId)
		RETURN "AREN_SERIES"
	ENDIF
	
	UNUSED_PARAMETER(iRootId)
    SWITCH (paramMissionFM)
        CASE FMMC_TYPE_SURVIVAL             RETURN ("CELL_2613")
        CASE FMMC_TYPE_IMPROMPTU_DM         RETURN ("CELL_2615")
        CASE FMMC_TYPE_GANGHIDEOUT          RETURN ("CELL_2616")
        CASE FMMC_TYPE_BASE_JUMP            RETURN ("CELL_2618")
        CASE FMMC_TYPE_MG_GOLF              RETURN ("CELL_2620")
        CASE FMMC_TYPE_MG_PILOT_SCHOOL      RETURN ("CELL_PILOTS")
        CASE FMMC_TYPE_MG_TENNIS            RETURN ("CELL_2621")
        CASE FMMC_TYPE_MG_SHOOTING_RANGE    RETURN ("CELL_2622")
        CASE FMMC_TYPE_MG_DARTS             RETURN ("CELL_2623")
        CASE FMMC_TYPE_MG_ARM_WRESTLING     RETURN ("CELL_2624")
        CASE FMMC_TYPE_PLAYLIST             RETURN ("CELL_2625")
        CASE FMMC_TYPE_HOLD_UPS             RETURN ("CELL_2627")
        CASE FMMC_TYPE_RACE_TO_POINT        RETURN ("CELL_2628")
        CASE FMMC_TYPE_APARTMENT      
		
		IF propertyType = SE_INVITE_TO_APARTMENT_YACHT
			RETURN ("BLIP_455")
		ELIF propertyType = SE_INVITE_TO_APARTMENT_OFFICE
			RETURN ("PM_SPAWN_OFFICE")
		ELIF propertyType = SE_INVITE_TO_APARTMENT_CLUBHOUSE
			RETURN ("PI_BIK_4_1_O1")
		ELIF propertyType = SE_INVITE_TO_APARTMENT_OFF_GAR
			RETURN ("PROP_OFFG")
		ELIF propertyType = SE_INVITE_TO_APARTMENT_OFF_AUTO_SHOP
			RETURN ("PROP_OFFG_SHOP")
		ENDIF
			RETURN ("CELL_2630")
		BREAK
		CASE FMMC_TYPE_SIMPLE_INTERIOR
			SWITCH INT_TO_ENUM(SIMPLE_INTERIOR_TYPE, propertyType)
				CASE SIMPLE_INTERIOR_TYPE_FACTORY			RETURN "BRS_BUSNS"		BREAK
				CASE SIMPLE_INTERIOR_TYPE_IE_GARAGE			RETURN "CELL_IEWHS"		BREAK
				CASE SIMPLE_INTERIOR_TYPE_BUNKER			RETURN "CELL_BUNKER"	BREAK
				CASE SIMPLE_INTERIOR_TYPE_ARMORY_TRUCK		RETURN "CELL_ARMORYTRU"	BREAK
				CASE SIMPLE_INTERIOR_TYPE_HANGAR			RETURN "CELL_HANGAR"	BREAK
				CASE SIMPLE_INTERIOR_TYPE_ARMORY_AIRCRAFT	RETURN "CELL_ARMAIR"	BREAK
				CASE SIMPLE_INTERIOR_TYPE_DEFUNCT_BASE		RETURN "CELL_DBASE"		BREAK
				CASE SIMPLE_INTERIOR_TYPE_BUSINESS_HUB		RETURN "CELL_CLUB"		BREAK
				CASE SIMPLE_INTERIOR_TYPE_HACKER_TRUCK		RETURN "CELL_HTRUCK"	BREAK
				CASE SIMPLE_INTERIOR_TYPE_ARENA_GARAGE		RETURN "CELL_AGARAGE"	BREAK
				CASE SIMPLE_INTERIOR_TYPE_CASINO_APARTMENT	RETURN "CELL_CASAPT"	BREAK
				CASE SIMPLE_INTERIOR_TYPE_ARCADE			RETURN "CELL_ARCADE"	BREAK
				CASE SIMPLE_INTERIOR_TYPE_SUBMARINE			RETURN "CELL_SUBMARINE"	BREAK				
				CASE SIMPLE_INTERIOR_TYPE_AUTO_SHOP			RETURN "CELL_AUTO_SHP"	BREAK
				CASE SIMPLE_INTERIOR_TYPE_CAR_MEET			RETURN "CELL_CM"		BREAK				
				CASE SIMPLE_INTERIOR_TYPE_FIXER_HQ			RETURN "CELL_FIX_HQ"	BREAK
				#IF FEATURE_DLC_2_2022
				CASE SIMPLE_INTERIOR_TYPE_MULTISTOREY_GARAGE	RETURN "CELL_MS_G"		BREAK
				CASE SIMPLE_INTERIOR_TYPE_JUGGALO_HIDEOUT		RETURN "CELL_JUGHID"	BREAK
				#ENDIF
				CASE SIMPLE_INTERIOR_TYPE_MUSIC_STUDIO
					IF paramSubtype = SI_EVENT_INVITE_PRIVATE
						RETURN "CELL_REC_A_SM"
					ELSE
						RETURN "CELL_REC_A"
					ENDIF
				BREAK				
			ENDSWITCH
		BREAK
		
		CASE FMMC_TYPE_HEIST_ISLAND_INVITE	RETURN "CELL_B_PARTY"	BREAK
		
        CASE FMMC_TYPE_GROUP_QUICKMATCH     RETURN ("CELL_2630Q")
        CASE FMMC_TYPE_LESTER_KILL_TARGET   RETURN ("CELL_2629")
		
		CASE FMMC_TYPE_TIME_TRIAL			RETURN ("CELL_2631")
		
		CASE FMMC_TYPE_GB_BOSS_DEATHMATCH			
		 	IF IS_PLAYER_ON_BIKER_DM()	
				RETURN("CELL_BIKEDM")
			ELSE
				RETURN("CELL_BOSSVBOSS")
			ENDIF
		BREAK
        // These require additional info
        CASE FMMC_TYPE_HEIST_PHONE_INVITE
        CASE FMMC_TYPE_HEIST_PHONE_INVITE_2
		CASE FMMC_TYPE_LOW_FLOW_PHONE_INVITE
        CASE FMMC_TYPE_MISSION
        CASE FMMC_TYPE_DEATHMATCH
        CASE FMMC_TYPE_RACE
		CASE FMMC_TYPE_VCM_QUICK_INVITE
		CASE FMMC_TYPE_IH_QUICK_INVITE
		CASE FMMC_TYPE_FMC_INVITE
            BREAK
            
		#IF FEATURE_COPS_N_CROOKS
		CASE FMMC_TYPE_CNC_INVITE
			RETURN "CNC_INVITE_TTL"
		#ENDIF
		
		CASE FMMC_TYPE_SANDBOX_ACTIVITY
			SWITCH INT_TO_ENUM(SANDBOX_ACTIVITY_VARIATION, paramSubtype)
				CASE SAV_TIME_TRIAL
					RETURN "TSA_QUIT_TT"
				
				CASE SAV_CHECKPOINT_DASH
					RETURN "TSA_QUIT_CD"
				
				CASE SAV_HEAD_TO_HEAD
					RETURN "TSA_QUIT_HH"
				
				CASE SAV_SPRINT
					RETURN "TSA_QUIT_SR"
			ENDSWITCH
		BREAK
		
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE FMMC_TYPE_HSW_TIME_TRIAL
			RETURN "HSWTT_QUIT"
			
		CASE FMMC_TYPE_HSW_SETUP
			RETURN "HSWS_QUIT"
		#ENDIF
		
        // Unrecognised
        DEFAULT
            SCRIPT_ASSERT("Get_FM_Joblist_Activity_TextLabel() - ERROR: Unknown FM activity. Tell Keith.")
            RETURN ("CELL_250")
            
    ENDSWITCH
    
    // Activities with Subtypes
	STRING          tempTextstring
	TEXT_LABEL_63   tempTextLabel	= ""
        
	
    SWITCH (paramMissionFM)
		CASE FMMC_TYPE_IH_QUICK_INVITE
			RETURN ("IH_END_NAME")
			
		CASE FMMC_TYPE_VCM_QUICK_INVITE
			RETURN ("FMMC_VCASINO")
	
		// KGM 22/1/15 [BUG 2207793]: Display 'Heist' or 'Setup' as appropriate
		CASE FMMC_TYPE_HEIST_PHONE_INVITE_2
			RETURN ("FMMC_RSTAR_MHS2")
		CASE FMMC_TYPE_HEIST_PHONE_INVITE
			IF (paramSubtype = FMMC_MISSION_TYPE_PLANNING)
				RETURN ("FMMC_RSTAR_HP")
			ENDIF
			// Use default
			RETURN ("FMMC_RSTAR_MHST")
			
		CASE FMMC_TYPE_LOW_FLOW_PHONE_INVITE
			RETURN ("FMMC_RSTAR_MSL")
		
        CASE FMMC_TYPE_MISSION
			IF (paramSubtype >= 0)
	            tempTextLabel  = Get_FM_Mission_Blip_Name_From_Subtype(paramSubtype, TRUE)
	            tempTextstring = CONVERT_TEXT_LABEL_TO_STRING(tempTextLabel)
	            RETURN (tempTextstring)
			ENDIF
			// Use default
			RETURN ("CELL_2610")
            
        CASE FMMC_TYPE_DEATHMATCH   
			IF (paramSubtype >= 0)
	            tempTextLabel  = Get_FM_Deathmatch_Blip_Name_From_Subtype(paramSubtype, TRUE)
	            tempTextstring = CONVERT_TEXT_LABEL_TO_STRING(tempTextLabel)
	            RETURN (tempTextstring)
 			ENDIF
			// Use default
			RETURN ("CELL_2611")
           
        CASE FMMC_TYPE_RACE         
			IF (paramSubtype >= 0)
	            tempTextLabel  = Get_FM_Race_Blip_Name_From_Subtype(paramSubtype, TRUE)
	            tempTextstring = CONVERT_TEXT_LABEL_TO_STRING(tempTextLabel)
	            RETURN (tempTextstring)
 			ENDIF
			// Use default
			RETURN ("CELL_2612")
		
		CASE FMMC_TYPE_FMC_INVITE
			tempTextstring = GET_FMC_INVITES_CONTENT_NAME_LABEL(INT_TO_ENUM(FMC_INVITES_CONTENT_TYPE, paramSubtype))
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tempTextstring)
				RETURN tempTextstring
			ENDIF
		BREAK
    ENDSWITCH
    
    // Failed to find the text label
    SCRIPT_ASSERT("Get_FM_Joblist_Activity_TextLabel() - ERROR: Unknown FM activity with subtype. Tell Keith.")
    RETURN ("CELL_250")

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the BlipSprite that corresponds to the current FM activity
//
// INPUT PARAMS:        paramMissionFM          FM Mission as INT
// INPUT PARAMS:        paramSubtype			Mission Subtype, or -1 if not known or required
// RETURN VALUE:        BLIP_SPRITE             The Blip Sprite
FUNC BLIP_SPRITE Get_Joblist_BlipID(INT paramMissionFM, INT paramSubtype = -1, INT iHash = 0)

	IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(iHash)
		RETURN RADAR_TRACE_RACE_WARS
	ENDIF
	
	IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_COMMUNITY_SERIES(iHash)
		RETURN RADAR_TRACE_COMMUNITY_SERIES
	ENDIF
	
    SWITCH (paramMissionFM)
        CASE FMMC_TYPE_SURVIVAL             RETURN (RADAR_TRACE_HORDE)
        CASE FMMC_TYPE_BASE_JUMP            RETURN (RADAR_TRACE_BASE_JUMP)
        // These require additional info
        CASE FMMC_TYPE_HEIST_PHONE_INVITE
        CASE FMMC_TYPE_HEIST_PHONE_INVITE_2
        CASE FMMC_TYPE_MISSION
        CASE FMMC_TYPE_DEATHMATCH
        CASE FMMC_TYPE_RACE
		CASE FMMC_TYPE_LOW_FLOW_PHONE_INVITE
		CASE FMMC_TYPE_VCM_QUICK_INVITE
            BREAK
			
		// No Blip
        CASE FMMC_TYPE_MG_GOLF
        CASE FMMC_TYPE_MG_TENNIS
        CASE FMMC_TYPE_MG_SHOOTING_RANGE
        CASE FMMC_TYPE_MG_DARTS
        CASE FMMC_TYPE_MG_ARM_WRESTLING
        CASE FMMC_TYPE_APARTMENT
		CASE FMMC_TYPE_IMPROMPTU_DM
        CASE FMMC_TYPE_GANGHIDEOUT
        CASE FMMC_TYPE_MG_PILOT_SCHOOL
        CASE FMMC_TYPE_RACE_TO_POINT
             RETURN (RADAR_TRACE_INVALID)
           
        // Unrecognised
        DEFAULT
            RETURN (RADAR_TRACE_INVALID)
            
    ENDSWITCH
    
    // Activities with Subtypes
    SWITCH (paramMissionFM)
		CASE FMMC_TYPE_IH_QUICK_INVITE
			RETURN (RADAR_TRACE_HEIST)
	
		CASE FMMC_TYPE_VCM_QUICK_INVITE
			RETURN (RADAR_TRACE_CASINO)
	
		// KGM 22/1/15 [BUG 2207793]: Display 'Heist' or 'Setup' as appropriate
		CASE FMMC_TYPE_HEIST_PHONE_INVITE
		CASE FMMC_TYPE_HEIST_PHONE_INVITE_2
			IF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(iHash)
				RETURN (RADAR_TRACE_HEIST)
			ENDIF
			IF (paramSubtype = FMMC_MISSION_TYPE_PLANNING)
				RETURN (RADAR_TRACE_HEIST_PREP)
			ENDIF
			// Use default
			RETURN (RADAR_TRACE_HEIST)
		
		CASE FMMC_TYPE_LOW_FLOW_PHONE_INVITE
			RETURN RADAR_TRACE_MP_LAMAR
			
        CASE FMMC_TYPE_MISSION
			IF (paramSubtype >= 0)
	            RETURN (GET_FM_MISSION_BLIP_TYPE_FROM_SUB_TYPE(paramSubtype))
			ENDIF
			// Use default
			RETURN (RADAR_TRACE_HEIST)
            
        CASE FMMC_TYPE_DEATHMATCH   
			IF (paramSubtype >= 0)
	            RETURN (Get_FM_Deathmatch_Blip_From_Subtype(paramSubtype))
 			ENDIF
			// Use default
			RETURN (RADAR_TRACE_HEIST)
           
        CASE FMMC_TYPE_RACE         
			IF (paramSubtype >= 0)
	            RETURN (GET_FM_RACE_BLIP_TYPE_FROM_SUB_TYPE(paramSubtype))
 			ENDIF
			// Use default
			RETURN (RADAR_TRACE_HEIST)
    ENDSWITCH
    
    // Failed to find the blip
    RETURN (RADAR_TRACE_INVALID)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this activity type should be highlighted if unplayed and append an appropriate piece of text to the activity name string
//
// INPUT PARAMS:        paramUnplayed			TRUE only if Unplayed, FALSE if played, or data is not locally available, or Minigame, etc
//						creatorID				For checking if mission is RockstarCreated, RockstarVerified or not
// RETURN PARAM:		refActivityTL			The text label where the additiona string should be appended onto
PROC Append_Played_Status_To_Activity_String(BOOL paramUnplayed, INT creatorID, TEXT_LABEL_63 &refActivityTL)
	// If not unplayed then nothing to do (this may change)
	// OR Only Add the new tag if it is RockstarCreated or RockstarVerified.
	IF NOT (paramUnplayed) 
	OR (creatorID != FMMC_ROCKSTAR_CREATOR_ID AND creatorID != FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID)
		EXIT
	ENDIF
		
	refActivityTL += GET_FILENAME_FOR_AUDIO_CONVERSATION("JL_UNPLAYED")
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Generate a Fake ContentID for minigames
//
// INPUT PARAMS:        paramTypeFM         The Minigame Type
//                      paramVariation      The Variation
// RETURN VALUE:        STRING              A Fake Minigame ContentID
FUNC TEXT_LABEL_23 Generate_Fake_Minigame_ContentID(INT paramTypeFM, INT paramVariation)

    TEXT_LABEL_23 returnTL = ""

    SWITCH (paramTypeFM)
        CASE FMMC_TYPE_MG_GOLF              returnTL = "FAKE_GOLF___"       BREAK
        CASE FMMC_TYPE_MG_PILOT_SCHOOL      returnTL = "FAKE_PILOTS_"       BREAK
        CASE FMMC_TYPE_MG_TENNIS            returnTL = "FAKE_TENNIS_"       BREAK
        CASE FMMC_TYPE_MG_SHOOTING_RANGE    returnTL = "FAKE_SHOOTR_"       BREAK
        CASE FMMC_TYPE_MG_DARTS             returnTL = "FAKE_DARTS__"       BREAK
        CASE FMMC_TYPE_MG_ARM_WRESTLING     returnTL = "FAKE_ARMWRS_"       BREAK
            BREAK
            
        DEFAULT
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: Generate_Fake_Minigame_ContentID() - ERROR: Activity Type is not a minigame. Type: ") NET_PRINT_INT(paramTypeFM) NET_NL()
                SCRIPT_ASSERT("Generate_Fake_Minigame_ContentID() - ERROR: Activity Type is not a minigame. Tell Keith.")
            #ENDIF
            
            RETURN (returnTL)
    ENDSWITCH
    
    // Generate the return value
    returnTL += paramVariation
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: Generate Fake Minigame ContentID: ") NET_PRINT(returnTL) NET_NL()
    #ENDIF
    
    RETURN (returnTL)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the Minigame Type if the passed in string matches a TL in Get_FM_Joblist_Activity_TextLabel()
//
// INPUT PARAMS:        paramTL         // The TextLabel
// REFERENCE PARAMS:    refType         // The FM Mission Type from a fake contentID
//                      refVariation    // The Variation from the Fake ContentID
// RETURN VALUE:        BOOL            // TRUE if a match was found, otherwise FALSE
//
// NOTES:   This was added so that cross-session invites to minigames could be recognised
//          Heavily hardcoded to the above function. Fake ContentID = 'FAKE_' + 'ARMWRS_' + 1
FUNC BOOL Get_MiniGame_Type_And_Variation_From_TL(STRING paramTL, INT &refType, INT &refVariation)

    IF (IS_STRING_NULL_OR_EMPTY(paramTL))
        RETURN FALSE
    ENDIF
    
    INT theLength = GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramTL)
    IF NOT (theLength > 12)
        RETURN FALSE
    ENDIF
    
    // Is this a Fake ContentID?
    TEXT_LABEL_7 fakePrefix = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME_BYTES(paramTL, 0, 5)
    IF NOT (ARE_STRINGS_EQUAL(fakePrefix, "FAKE_"))
        RETURN FALSE
    ENDIF
    
    // Get the MiniGame Type
    TEXT_LABEL_15 minigameText = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME_BYTES(paramTL, 5, 12)
    
    IF (ARE_STRINGS_EQUAL(minigameText, "GOLF___"))
        refType = FMMC_TYPE_MG_GOLF
    ELIF (ARE_STRINGS_EQUAL(minigameText, "TENNIS_"))
        refType = FMMC_TYPE_MG_TENNIS
    ELIF (ARE_STRINGS_EQUAL(minigameText, "PILOTS_"))
        refType = FMMC_TYPE_MG_PILOT_SCHOOL
    ELIF (ARE_STRINGS_EQUAL(minigameText, "SHOOTR_"))
        refType = FMMC_TYPE_MG_SHOOTING_RANGE
    ELIF (ARE_STRINGS_EQUAL(minigameText, "DARTS__"))
        refType = FMMC_TYPE_MG_DARTS
    ELIF (ARE_STRINGS_EQUAL(minigameText, "ARMWRS_"))
        refType = FMMC_TYPE_MG_ARM_WRESTLING
    ELSE
        RETURN FALSE
    ENDIF
    
    // Get the Variation
    TEXT_LABEL_7 variationText = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME_BYTES(paramTL, 12, theLength)

    IF NOT (STRING_TO_INT(variationText, refVariation))
        RETURN FALSE
    ENDIF
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: Convert Fake Minigame ContentID to Minigame Type: ")
        NET_PRINT_INT(refType)
        NET_PRINT(" and variation: ")
        NET_PRINT_INT(refVariation)
        NET_NL()
    #ENDIF
    
    // Found a minigame
    RETURN TRUE

ENDFUNC




// ===========================================================================================================
//      InCar Buddies functions
// ===========================================================================================================

// PURPOSE: Store the PlayerIndex of another player in the same car
// NOTES:   This passed back a player_index used by Bobby to buddy the player with someone else in the car when the player accepts an invite to a Rally
FUNC PLAYER_INDEX Store_InCar_Buddy()
	
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: Looking for a buddy in the player's car: ")
    #ENDIF

    PLAYER_INDEX thereIsNoBuddy = INVALID_PLAYER_INDEX()
	
    // Ensure player is OK
    IF NOT (IS_NET_PLAYER_OK(PLAYER_ID()))
        #IF IS_DEBUG_BUILD
            NET_PRINT("IGNORE - Player is not OK") NET_NL()
        #ENDIF
        
        RETURN thereIsNoBuddy
    ENDIF
    
    // Ensure player is in a car
    IF NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
        #IF IS_DEBUG_BUILD
            NET_PRINT("IGNORE - Player is not in a vehicle") NET_NL()
        #ENDIF
        
        RETURN thereIsNoBuddy
    ENDIF
        
    // Get the vehicle index for the player's vehicle
    VEHICLE_INDEX theVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
    
    // Ensure the vehicle is driveable
    IF NOT (IS_VEHICLE_DRIVEABLE(theVehicle))
        #IF IS_DEBUG_BUILD
            NET_PRINT("IGNORE - Player Vehicle is not driveable") NET_NL()
        #ENDIF
        
        RETURN thereIsNoBuddy
    ENDIF

    // How many occupants (passengers + driver)?
    INT occupantsToCheck = GET_VEHICLE_NUMBER_OF_PASSENGERS(theVehicle)
    IF NOT (IS_VEHICLE_SEAT_FREE(theVehicle, VS_DRIVER))
        occupantsToCheck++
    ENDIF
        
    // If there is only one occupant then it must be the player
    IF (occupantsToCheck = 1)
        #IF IS_DEBUG_BUILD
            NET_PRINT("IGNORE - There is only one vehicle occupant so it must be the player") NET_NL()
        #ENDIF
        
        RETURN thereIsNoBuddy
    ENDIF
    
    // The vehicle contains more than one occupant
    // Go through all vehicle seats until the first buddy is found
    // NOTE: using VS_EXTRA_RIGHT_3 is the highest seat ID for the safety cutoff - this may change
    VEHICLE_SEAT    thisSeatId      = VS_DRIVER
    PED_INDEX       theOccupant
    INT             thisSeatIdAsInt = ENUM_TO_INT(thisSeatId)
    INT             safetyCutoff    = ENUM_TO_INT(VS_EXTRA_RIGHT_3)
    
    WHILE (thisSeatIdAsInt <= safetyCutoff)
        thisSeatId = INT_TO_ENUM(VEHICLE_SEAT, thisSeatIdAsInt)

        // Is the seat occupied?
        IF NOT (IS_VEHICLE_SEAT_FREE(theVehicle, thisSeatId))
            // ...the seat is occupied - ignore this seat if it is the player
            theOccupant = GET_PED_IN_VEHICLE_SEAT(theVehicle, thisSeatId)
            IF (DOES_ENTITY_EXIST(theOccupant))
                IF (theOccupant != PLAYER_PED_ID())
                    // ...not the player, but ensure it is another player
                    IF (IS_PED_A_PLAYER(theOccupant))
                        // ...occupant is another player, so pass this back to the calling script
                        #IF IS_DEBUG_BUILD
                            NET_PRINT("Found a Buddy Player: ") NET_PRINT(GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(theOccupant))) NET_NL()
                        #ENDIF
            
                        RETURN (NETWORK_GET_PLAYER_INDEX_FROM_PED(theOccupant))
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    
        
        // Next Seat
        thisSeatIdAsInt++
    ENDWHILE
    
    // Failed to find a buddy player in any seat
    #IF IS_DEBUG_BUILD
        NET_PRINT("IGNORE - There are no Buddy Players in teh vehicle") NET_NL()
    #ENDIF
        
    RETURN thereIsNoBuddy

ENDFUNC




// ===========================================================================================================
//      JobList Warp access functions
//      A Joblist warp is a warp instigated by, for example, accepting an Invitation on the Joblist
// ===========================================================================================================

// PURPOSE: Check if a joblist warp is in progress
//
// RETURN VALUE:        BOOL            TRUE if one is in progress, otherwise FALSE
FUNC BOOL Is_Joblist_Warp_In_Progress()
    RETURN (g_sJoblistWarpMP.jlwIsActive)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a joblist skyswoop is in progress
//
// RETURN VALUE:        BOOL            TRUE if a joblist skyswoop is in progress, otherwise FALSE
FUNC BOOL Is_Joblist_Skyswoop_In_Progress()

    IF NOT (Is_Joblist_Warp_In_Progress())
		RETURN FALSE
	ENDIF
	
	RETURN (g_sJoblistWarpMP.jlwSkyswoop)
	
ENDFUNC


FUNC BOOL IS_THIS_A_GANG_OPS_BY_VECTOR_HACK(TEXT_LABEL_23 paramFilename) 
	INT	tempLoop = 0
	INT filenameHash = GET_HASH_KEY(paramFilename)
	VECTOR vBoard = GANGOPS_GET_GANG_OPS_BOARD_START_POS()
	REPEAT g_numAtCoordsMP tempLoop
		IF g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash
			IF ARE_VECTORS_ALMOST_EQUAL(g_sAtCoordsMP[tempLoop].matcCoords, vBoard, 3.0)
      			PRINTLN("KGM [Joblist] ARE_VECTORS_ALMOST_EQUAL(g_sAtCoordsMP[", tempLoop, "].matcCoords, vBoard = ",vBoard)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

#IF FEATURE_CASINO_HEIST
FUNC BOOL IS_THIS_A_CASINO_HEIST_BY_VECTOR_HACK(TEXT_LABEL_23 paramFilename) 
	INT	tempLoop = 0
	INT filenameHash = GET_HASH_KEY(paramFilename)
	VECTOR vBoard = GET_CASINO_HEIST_BOARD_START_POS()
	REPEAT g_numAtCoordsMP tempLoop
		IF g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash
			IF ARE_VECTORS_ALMOST_EQUAL(g_sAtCoordsMP[tempLoop].matcCoords, vBoard, 3.0)
      			PRINTLN("KGM [Joblist] ARE_VECTORS_ALMOST_EQUAL(g_sAtCoordsMP[", tempLoop, "].matcCoords, vBoard = ",vBoard)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC
#ENDIF


#IF FEATURE_HEIST_ISLAND
FUNC BOOL IS_THIS_A_HEIST_ISLAND_BY_VECTOR_HACK(TEXT_LABEL_23 paramFilename) 
	INT	tempLoop = 0
	INT filenameHash = GET_HASH_KEY(paramFilename)
	VECTOR vBoard = GET_HEIST_ISLAND_BOARD_START_POS()
	REPEAT g_numAtCoordsMP tempLoop
		IF g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash
			IF ARE_VECTORS_ALMOST_EQUAL(g_sAtCoordsMP[tempLoop].matcCoords, vBoard, 3.0)
      			PRINTLN("KGM [Joblist] ARE_VECTORS_ALMOST_EQUAL(g_sAtCoordsMP[", tempLoop, "].matcCoords, vBoard = ",vBoard)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC
#ENDIF

FUNC BOOL IS_THIS_A_TUNER_ROBBERY_BY_VECTOR_HACK(TEXT_LABEL_23 paramFilename) 
	INT	tempLoop = 0
	INT filenameHash = GET_HASH_KEY(paramFilename)
	VECTOR vBoard = GET_TUNER_BOARD_START_POS()
	REPEAT g_numAtCoordsMP tempLoop
		IF g_sAtCoordsMP[tempLoop].matcIdCloudnameHash = filenameHash
			IF ARE_VECTORS_ALMOST_EQUAL(g_sAtCoordsMP[tempLoop].matcCoords, vBoard, 3.0)
      			PRINTLN("KGM [Joblist] ARE_VECTORS_ALMOST_EQUAL(g_sAtCoordsMP[", tempLoop, "].matcCoords, vBoard = ",vBoard)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Setup a new joblist fadeout and warp
//
// INPUT PARAMS:        paramCoords         		The Warp Location
//                      paramUniqueID       		The UniqueID for the mission being warped to
//						paramFMType					The FMMC Mission Type
//						paramSubtype				The Subtype
//						paramInvitingPlayerID		The Inviting PlayerID
//						paramMatcUniqueID			The MissionsAtCoords uniqueID of the mission the player has accepted the invite for and been reserved on
//                      paramUseSkySwoop    		[DEFAULT = TRUE] TRUE to use Skyswoop, FALSE to use teleport
PROC Setup_Joblist_FadeOut_And_Warp(VECTOR paramCoords, TEXT_LABEL_23 sContentID, INT paramFMType, INT paramSubtype, PLAYER_INDEX paramInvitingPlayerID, INT paramMissionUniqueID, BOOL paramUseSkySwoop = TRUE, INT irootContentId = 0)

    IF (Is_Joblist_Warp_In_Progress())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Setup_Joblist_Warp() - ERROR: Joblist Warp is already in progress so ignoring new request.") NET_NL()
            SCRIPT_ASSERT("Setup_Joblist_Warp() - ERROR: Joblist Warp is already in progress. Ignoring new request. Tell Keith.")
        #ENDIF
        
        EXIT
    ENDIF
    
    // Let MissionsAtCoords know that the mission is being triggered by Invite
	Set_MissionsAtCoords_Mission_Has_Had_Invite_Accepted_By_Filename(sContentID)
    
    // Clear the Feed Entry if still active
    IF (g_sInviteAcceptanceMP.iaFeedID != NO_JOBLIST_INVITE_FEED_ID)
        THEFEED_REMOVE_ITEM(g_sInviteAcceptanceMP.iaFeedID)
		Joblist_Remove_All_Invite_Feed_Messages()
        HIDE_HUD_AND_RADAR_THIS_FRAME()
        THEFEED_HIDE_THIS_FRAME()
    ENDIF
	BOOL bGangOpsHeaderDownloaded
    IF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(g_sTransitionSessionData.iGangOpsMissingHeaderDataRootId)
	AND IS_THIS_A_GANG_OPS_BY_VECTOR_HACK(sContentID)
		bGangOpsHeaderDownloaded = TRUE
        PRINTLN("KGM [Joblist] bGangOpsHeaderDownloaded = ", bGangOpsHeaderDownloaded)
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	BOOL bCasinoHeistHeaderDownloaded
    IF IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_sTransitionSessionData.iCasinoHeistMissingHeaderDataRootId)
	AND IS_THIS_A_CASINO_HEIST_BY_VECTOR_HACK(sContentID)
		bCasinoHeistHeaderDownloaded = TRUE
        PRINTLN("KGM [Joblist] bCasinoHeistHeaderDownloaded = ", bCasinoHeistHeaderDownloaded)
	ENDIF
	#ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	BOOL bHeistIslandHeaderDownloaded
    IF IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_sTransitionSessionData.iHeistIslandMissingHeaderDataRootId)
	AND IS_THIS_A_HEIST_ISLAND_BY_VECTOR_HACK(sContentID)
		bHeistIslandHeaderDownloaded = TRUE
        PRINTLN("KGM [Joblist] bHeistIslandHeaderDownloaded = ", bHeistIslandHeaderDownloaded)
	ENDIF
	#ENDIF
	
	BOOL bTunerRobberyHeaderDownloaded
    IF IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_sTransitionSessionData.iTunerRobberyMissingHeaderDataRootId)
	AND IS_THIS_A_TUNER_ROBBERY_BY_VECTOR_HACK(sContentID)
		bTunerRobberyHeaderDownloaded = TRUE
        PRINTLN("KGM [Joblist] bTunerRobberyHeaderDownloaded = ", bTunerRobberyHeaderDownloaded)
	ENDIF
    
	// KGM 9/6/13: If the mission is an 'immediate launch' mission then don't fadeout and warp - it should launch where the player is
    // NOTE: Exclude Null ContentIDs
    IF NOT (IS_STRING_NULL_OR_EMPTY(sContentID))
    AND (Get_MissionsAtCoords_Is_Immediate_Launch_By_Filename(sContentID))
	AND NOT GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(irootContentId)
	AND NOT CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(irootContentId)
	AND NOT bGangOpsHeaderDownloaded
	#IF FEATURE_CASINO_HEIST
	AND NOT IS_THIS_MISSION_A_CASINO_HEIST_MISSION(irootContentId)
	AND NOT bCasinoHeistHeaderDownloaded
	#ENDIF
	#IF FEATURE_HEIST_ISLAND
	AND NOT IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(irootContentId)
	AND NOT bHeistIslandHeaderDownloaded
	#ENDIF
	AND NOT IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(irootContentId)
	AND NOT bTunerRobberyHeaderDownloaded
        // Copying stuff from when a Joblist Warp ends to ensure the data is setup correctly
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ") NET_PRINT_TIME() NET_PRINT("- Joblist Warp not required - mission should launch immediately") NET_NL()
            NET_PRINT("          Player's Current Position: ") NET_PRINT_VECTOR(GET_PLAYER_COORDS(PLAYER_ID())) NET_NL()
            NET_PRINT("          Mission's Actual Position: ") NET_PRINT_VECTOR(paramCoords) NET_NL()
            NET_PRINT("          Buddy In Car value set to: ")
            IF (g_piPlayerInMyVehicleWhenInvited = INVALID_PLAYER_INDEX())
            OR NOT (IS_NET_PLAYER_OK(g_piPlayerInMyVehicleWhenInvited))
                NET_PRINT("NO VALID PLAYER")
            ELSE
                NET_PRINT(GET_PLAYER_NAME(g_piPlayerInMyVehicleWhenInvited))
            ENDIF
            NET_NL()
        #ENDIF
        
        // Clear the initial spawn coords if the player has accepted an invite after joining the game but before moving
        g_vInitialSpawnLocation = << 0.0, 0.0, 0.0 >>
        PRINTLN("g_vInitialSpawnLocation = ", g_vInitialSpawnLocation)
        
        // Bobby needed this - not sure if it's still needed or not - should be set to the mission coords, not the player coords
        g_vStartingPointWarppedToWithVote = paramCoords
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: Setting vector g_vStartingPointWarppedToWithVote: ") NET_PRINT_VECTOR(g_vStartingPointWarppedToWithVote) NET_NL()
        #ENDIF
        
        // Requested by Bobby and James in 1597391
        VECTOR playerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
        SET_PLAYER_LEAVING_CORONA_VECTOR(playerPos) 
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: SET_PLAYER_LEAVING_CORONA_VECTOR (wsed when player walks out of corona) to : ") NET_PRINT_VECTOR(playerPos) NET_NL()
        #ENDIF
        
        // Ensure the mission gets temporarily unlocked so that the player can join it
        Temporarily_Unlock_MissionsAtCoords_Mission_By_Filename(sContentID)
		
		//Unblock the mission
        Unblock_One_MissionsAtCoords_Mission(sContentID)
		
        // Maintain hiding the radar and Feed until the player is in the corona
        g_sJoblistWarpMP.jlwIsActive            = TRUE
        g_sJoblistWarpMP.jlwImmediateLaunch     = TRUE
        g_sJoblistWarpMP.jlwContentID           = sContentID
        g_sJoblistWarpMP.jlwSafetyStopFrame     = GET_FRAME_COUNT() + HIDE_HUD_SAFETY_FRAME_TIMEOUT
        
        // JamesA: Prevent wanted level from being awarded if player warps to job 
        // (May need a global flag to monitor this if used for ambient activities around FM)
        UPDATE_PLAYER_WANTED_LEVEL_JOINING_MISSION()
        
        EXIT
    ENDIF

    // Setup the new safe warp offset from the corona, use the smaller corona radius as the offset radius
    // Remove player controls
    // KGM 12/7/13: Don't make the player invisible during a skyswoop - wait until the camera is off the ground
    IF (paramUseSkySwoop)		
		// This is where the player is having their tasks killed, add condtions here for exceptions to prevent this
		IF IS_PLAYER_CLIMBING(PLAYER_ID())											// Stops ped falling off ladder when accepting invite
		OR GET_PED_PARACHUTE_STATE(GET_PLAYER_PED(PLAYER_ID())) >= PPS_SKYDIVING	// Stops ped falling off parachute when accepting invite
		OR IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_CINEMA) 	
			PRINTLN("...KGM MP [MMM][JobList]: Player is climbing ladder, using a parachute, or in the cinema")
	    	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		ELSE 
			PRINTLN("...KGM MP [MMM][JobList]: Player is NOT climbing ladder, NOT using a parachute, and NOT in the cinema")
			NET_SET_PLAYER_CONTROL_FLAGS eFlags = NSPC_NONE
			 IF IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_sTransitionSessionData.iCasinoHeistMissingHeaderDataRootId)
			 #IF FEATURE_HEIST_ISLAND
			 OR IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_sTransitionSessionData.iHeistIslandMissingHeaderDataRootId)
			 #ENDIF
			 OR IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_sTransitionSessionData.iTunerRobberyMissingHeaderDataRootId)
				PRINTLN("...KGM MP [MMM][JobList]: IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_sTransitionSessionData.iCasinoHeistMissingHeaderDataRootId) soeFlags = NSPC_SET_INVISIBLE ")
				eFlags = NSPC_SET_INVISIBLE
			ELSE
				PRINTLN("...KGM MP [MMM][JobList]: NOT IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_sTransitionSessionData.iCasinoHeistMissingHeaderDataRootId)")
			ENDIF
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, eFlags)
		ENDIF 
    ELSE
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE)
    ENDIF
    
    INT     playerIDAsInt   = NATIVE_TO_INT(PLAYER_ID())
    BOOL    usePlayerCoords = FALSE
    VECTOR  playerCoords    = << 0.0, 0.0, 0.0 >>
    
    IF NOT (IS_PED_INJURED(PLAYER_PED_ID()))
        usePlayerCoords = TRUE
        playerCoords    = GET_PLAYER_COORDS(PLAYER_ID())
    ENDIF
	
	// KGM 12/3/15 [BUG 2229204]: Very slightly reduce the 'safe offset' radius to prevent a calculation positioning the player millimetres out of the corona for large-session 2-player jobs
	// KGM 13/5/15 [BUG 2229184]: Had to reduce this radius again very slightly. A player with a PID >= 16 accepting a minigame invite to a player with PID < 16 ended up 1.400076m away from centre - allowed radius is 1.4m
	FLOAT safeOffset_m = ciIN_LOCATE_DISTANCE_INSIDE - 0.1
    
	IF IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_sTransitionSessionData.iHeistIslandMissingHeaderDataRootId)
		safeOffset_m = 0.1
		PRINTLN("...KGM MP [MMM][JobList]: Setup_Joblist_FadeOut_And_Warp - Island heist, safeOffset_m = 0.1")
	ENDIF
	
    g_sJoblistWarpMP.jlwIsActive            	= TRUE
    g_sJoblistWarpMP.jlwImmediateLaunch     	= FALSE
    g_sJoblistWarpMP.jlwSkyswoop            	= paramUseSkySwoop
    g_sJoblistWarpMP.jlwFadedOut            	= FALSE
    g_sJoblistWarpMP.jlwPlayerMoved         	= FALSE
    g_sJoblistWarpMP.jlwSwoopUpDone         	= FALSE
    g_sJoblistWarpMP.jlwCoords              	= paramCoords
    g_sJoblistWarpMP.jlwOffset              	= GET_SAFE_WARP_COORD_IN_CIRCLE(playerIDAsInt, paramCoords, safeOffset_m)
    g_sJoblistWarpMP.jlwSkyswoopType        	= SWITCH_TYPE_AUTO
    g_sJoblistWarpMP.jlwContentID           	= sContentID
    g_sJoblistWarpMP.jlwSafetyStopFrame     	= 0
	g_sJoblistWarpMP.jlwForceKillProperty		= FALSE
	g_sJoblistWarpMP.jlwDoEstablishingShot		= FALSE
	g_sJoblistWarpMP.jlwEstShotSafetyTimeout	= GET_GAME_TIMER()
	g_sJoblistWarpMP.jlwEstShotInvitingPlayer	= INVALID_PLAYER_INDEX()
    
    IF  (paramUseSkySwoop)
    AND (usePlayerCoords)
        g_sJoblistWarpMP.jlwSkyswoopType    = GET_SWITCH_TYPE_FOR_START_AND_END_COORDS(playerCoords, g_sJoblistWarpMP.jlwOffset)
        
        // Don't allow a short switch
        IF (g_sJoblistWarpMP.jlwSkyswoopType = SWITCH_TYPE_SHORT)
            g_sJoblistWarpMP.jlwSkyswoopType = SWITCH_TYPE_MEDIUM
        ENDIF
        
        // Don't allow a medium skyswoop if the player is in a garage or safehouse interior
        // NOTE: This is because the above function returns 'short swoop' if the start or end point is in a garage and then the above 'short swoop' test changes the short to medium,
        //          but I want it to always do a long swoop while the interiors are underground
        IF (g_sJoblistWarpMP.jlwSkyswoopType = SWITCH_TYPE_MEDIUM)
            IF (IS_POINT_IN_A_PROPERTY_GARAGE_INTERIOR(playerCoords))
            OR (IS_POINT_IN_A_PROPERTY_HOUSE_INTERIOR(playerCoords))
            OR (IS_POINT_IN_A_PROPERTY_GARAGE_INTERIOR(g_sJoblistWarpMP.jlwOffset))
            OR (IS_POINT_IN_A_PROPERTY_HOUSE_INTERIOR(g_sJoblistWarpMP.jlwOffset))
                g_sJoblistWarpMP.jlwSkyswoopType = SWITCH_TYPE_LONG
            ENDIF
        ENDIF
        
        // Set up Spinner
        BEGIN_TEXT_COMMAND_BUSYSPINNER_ON("FMMC_DOWNLOAD")
        END_TEXT_COMMAND_BUSYSPINNER_ON(ENUM_TO_INT(LOADING_ICON_SPINNER))
    
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ") NET_PRINT_TIME() NET_PRINT("BUSYSPINNER ON") NET_NL()
        #ENDIF
    ENDIF
    
    // Clear the initial spawn coords if the player has accepted an invite after joining the game but before moving
    g_vInitialSpawnLocation = << 0.0, 0.0, 0.0 >>
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("New Joblist FadeOut and Warp Accepted to: ") NET_PRINT_VECTOR(paramCoords)
        NET_PRINT(" [Safe Offset Coords: ") NET_PRINT_VECTOR(g_sJoblistWarpMP.jlwOffset) NET_PRINT("]")
        IF (paramUseSkySwoop)
            NET_PRINT(" [Using SKYSWOOP]")
            SWITCH (g_sJoblistWarpMP.jlwSkyswoopType)
                CASE SWITCH_TYPE_AUTO       NET_PRINT(" [USING SKYSWOOP AUTO]")     BREAK
                CASE SWITCH_TYPE_SHORT      NET_PRINT(" [USING SKYSWOOP SHORT]")    BREAK
                CASE SWITCH_TYPE_MEDIUM     NET_PRINT(" [USING SKYSWOOP MEDIUM]")   BREAK
                CASE SWITCH_TYPE_LONG       NET_PRINT(" [USING SKYSWOOP LONG]")     BREAK
            ENDSWITCH
        ELSE
            NET_PRINT(" [Using TELEPORT]")
        ENDIF
        NET_NL()
        NET_PRINT("      (IF initial spawn location is still set because player has just entered the game, it gets cleared here).")
        NET_NL()
    #ENDIF

	// KGM 17/6/14: For Heists when the player is accepting an invite while in a property then allow the property to cleanup after the player is warped during the skyswoop
	// KGM 19/6/14: ...exception, don't kill the apartment if the player and the inviting player are in the same apartment.
	// KGM 18/8/14: BUG 1908052 - Let the Heist Controller know the invite is being accepted so the reservation can be cleaned up if the corona gets deleted before being Matc registered.
	IF (paramFMType = FMMC_TYPE_MISSION)
		IF (paramSubtype = FMMC_MISSION_TYPE_HEIST)
		OR (paramSubtype = FMMC_MISSION_TYPE_PLANNING)
			// Kill apartment?
			IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("AM_MP_PROPERTY_INT")) > 0)
				IF (IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE))
					BOOL killApartment = TRUE
					IF (paramInvitingPlayerID != INVALID_PLAYER_INDEX())
						IF (ARE_PLAYERS_IN_SAME_PROPERTY(PLAYER_ID(), paramInvitingPlayerID, TRUE))
							PRINTLN("...KGM MP [JobList][Heist]: ...Accepting Same-Session Invite into a Heist Corona while in Inviting Player's apartment same interior - don't kill the apartment")
							killApartment = FALSE
							
							// KGM 30/8/14: BUG 2008626 - The apartment is not going to be killed and therefore won't be initialised, so the apartment details are not needed.
							//				These need cleaned up to prevent them lingering for when the next apartment is entered.
							PRINTLN("...KGM MP [JobList][Heist]: ...The apartment is not being killed, so the Joblist Heist Apartment Details for AM_MP_PROPERTY_INT init are not needed")
							Clear_Same_Session_Invite_Heist_Apartment_Details()
						ENDIF
					ENDIF
					
					IF (killApartment)
						PRINTLN("...KGM MP [JobList][Heist]: ...Accepting Same-Session Invite into a Heist Corona while in an apartment - allow property to force cleanup during skyswoop")
						g_sJoblistWarpMP.jlwForceKillProperty = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			// Let Heist Controller know about the invite acceptance
			PRINTLN(".KGM [JobList][Heist]: Let Heist Controller know an Invite has been accepted to another player's Heist")
			IF (paramInvitingPlayerID != INVALID_PLAYER_INDEX())
				// KGM 28/2/15 [BUG 2254652]: The inviter may not be the Heist Leader, so check if the inviter is in a different player's apartment in this session
				PLAYER_INDEX theLeader = paramInvitingPlayerID
				GAMER_HANDLE inviterGH = GET_GAMER_HANDLE_PLAYER(paramInvitingPlayerID)
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [JobList][Heist]: Inviter GamerHandle:")
					DEBUG_PRINT_GAMER_HANDLE(inviterGH)
				#ENDIF
				GAMER_HANDLE apartmentOwnerGH
				GET_GAMER_HANDLE_OF_PROPERTY_OWNER(paramInvitingPlayerID, apartmentOwnerGH)
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [JobList][Heist]: Owner of Inviter's Current Property GamerHandle:")
					DEBUG_PRINT_GAMER_HANDLE(apartmentOwnerGH)
				#ENDIF
				IF NOT (NETWORK_ARE_HANDLES_THE_SAME(inviterGH, apartmentOwnerGH))
					// ...the inviter and the inviter's current apartment owner are not the same
					// If the apartment owner is in this session then treat that player as the Leader
					IF (NETWORK_IS_GAMER_IN_MY_SESSION(apartmentOwnerGH))
						PRINTLN(".KGM [JobList][Heist]: ...Inviter is not the apartment owner and the apartment owner is in the session, so apartment owner is Leader")
						theLeader = NETWORK_GET_PLAYER_FROM_GAMER_HANDLE(apartmentOwnerGH)
					ENDIF
				ENDIF
				Invite_Accepted_To_Other_Player_Active_Heist_Corona(theLeader, sContentID, paramMissionUniqueID)
			ELSE
				PRINTLN("...KGM MP [JobList][Heist]: ...IGNORED: Inviting Player is INVALID")
			ENDIF
			
			// KGM 1/9/14: An establishing shot is required for Heist same-session invite acceptances
			PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS]: Establishing Shot required for same-session Invite accepted to a Heist corona. Safety Timeout: ", INVITE_ESTABLISHING_SHOT_SAFETY_TIMEOUT_msec, " msec")
			g_sJoblistWarpMP.jlwDoEstablishingShot = TRUE
			g_sJoblistWarpMP.jlwEstShotSafetyTimeout = GET_GAME_TIMER() + INVITE_ESTABLISHING_SHOT_SAFETY_TIMEOUT_msec
			g_sJoblistWarpMP.jlwEstShotInvitingPlayer = paramInvitingPlayerID
			
			// JA 28/11/14: Expanded the below function so we can specify if a player accepted an invite (in session). Currently only used here for the establishing heist shots - we could open this up to all Jobs if needed.
			BROADCAST_JOBLIST_INVITE_REPLY(paramInvitingPlayerID, FALSE, FALSE, TRUE)
			
			// KGM 1/9/14:	If the corona is already setup, then need to block it here to allow the establishing shot stuff to work
			// NOTE:		If the corona isn't already setup this call will fail, and hte corona will be blocked when it does get setup
			PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS]: SAME SESSION INVITE - Blocking heist corona if already setup: ", g_sJoblistWarpMP.jlwContentID, " (", g_sJoblistWarpMP.jlwCoords, ")")
			Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_By_Filename(g_sJoblistWarpMP.jlwContentID, FALSE, TRUE)
			IF (GET_HEIST_CORONA_BLOCKED_STATE() = HEIST_INTRO_STATE_BLOCKED)
				g_HeistSharedClient.tlHeistContentID = g_sJoblistWarpMP.jlwContentID
				PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS]: SAME SESSION INVITE - CORONA SUCCESSFULLY BLOCKED")
			ELSE
				PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS]: SAME SESSION INVITE - FAILED - Corona must not be set up yet. Will get blocked when setup.")
				
				// KGM 13/10/14 [BUG 2066538]: Ensure another mission can't sneak in  as the player is accepting the invite to the Heist coroan preior to the corona being setup
				PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS]: SAME SESSION INVITE - INSTEAD: Reserving this contentID to be exclusively triggered: ", g_sJoblistWarpMP.jlwContentID)
				Register_A_MissionsAtCoords_Exclusive_ContentID(g_sJoblistWarpMP.jlwContentID)
			ENDIF
		ENDIF
	ENDIF
		// FEATURE_HEIST_PLANNING
			
	// KGM 4/9/14 [BUG 2015150]: Clear the cross-session invite into Heist flag for all same-session invite acceptances - this didn't cause the bug (an SP Bail did) but it is a safety measure
	g_sPlayerAcceptedCrossSessionInviteToHeist = FALSE
	PRINTLN(".KGM [JobList][Heist]: g_sPlayerAcceptedCrossSessionInviteToHeist set to FALSE - this is a same-session invite acceptance")
	
	// Unreferenced until Heists are activated
	UNUSED_PARAMETER(paramInvitingPlayerID)
	UNUSED_PARAMETER(paramMissionUniqueID)
	UNUSED_PARAMETER(paramSubtype)
	UNUSED_PARAMETER(paramFMType)

ENDPROC




// ===========================================================================================================
//      Invite Acceptance access functions
//      (Controls an Invite being accepted on the Joblist)
// ===========================================================================================================

// PURPOSE: Check if the invitation is being accepted
//
// RETURN VALUE:        BOOL            TRUE if an invitation is being accepted, otherwise FALSE
FUNC BOOL Is_Invite_Being_Accepted()
    RETURN (g_sInviteAcceptanceMP.iaInviteAccepted)
ENDFUNC


FUNC BOOL IS_ANY_LOCAL_INVITE_OR_NPC_INVITE_BEING_PROCESSED()
	IF Is_Invite_Being_Accepted()
	OR GET_FM_JOB_ENTERY_TYPE() = ciMISSION_ENTERY_TYPE_NPC_INVITE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if there is an ongoing reservation request during Invite Acceptance
//
// RETURN VALUE:        BOOL            TRUE if there is an ongoing reservation request, otherwise FALSE
FUNC BOOL Is_Invite_Waiting_For_Reservation_Confirmation()
    RETURN (g_sInviteAcceptanceMP.iaMissionUniqueID != NO_UNIQUE_ID)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the reservation request for the Invite failed
//
// RETURN VALUE:        BOOL            TRUE if the reservation request failed, otherwise FALSE
//
// NOTE:    When failed, the other reservation request variables will have been cleared
FUNC BOOL Has_Invite_Reservation_Request_Failed()
    RETURN (g_sInviteAcceptanceMP.iaReservationFailed)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the reservation request for the Invite was accepted
//
// RETURN VALUE:        BOOL            TRUE if the reservation request was accepted, otherwise FALSE
//
// NOTES:   If acceptance is ongoing and there is no UniqueID and the request wasn't FAILED, then it must have been accepted
FUNC BOOL Has_Invite_Reservation_Request_Been_Accepted()

    IF NOT (Is_Invite_Being_Accepted())
        RETURN FALSE
    ENDIF
    
    IF (Is_Invite_Waiting_For_Reservation_Confirmation())
        RETURN FALSE
    ENDIF
    
    IF (Has_Invite_Reservation_Request_Failed())
        RETURN FALSE
    ENDIF
    
    RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Sets up an invite being accepted (following the reeservation request being sent to the server)
//
// INPUT PARAMS:        paramUniqueID           The Mission Request UniqueID (used to query for feedback)
//                      paramCoords             The Corona coords
//                      paramFMType             The FM Mission Type
//						paramSubtype			The Mission Subtype
//                      paramMissionName        The FM Mission Name
//                      paramFeedID             The FeedID of the invite being accepted
//						paramInvitingPlayerID	The PlayerID of the player whose invite is being accepted
PROC Setup_Invite_Being_Accepted(INT paramUniqueID, VECTOR paramCoords, INT paramFMType, INT paramSubtype, TEXT_LABEL_63 paramMissionName, INT paramFeedID, STRING sContentID,INT iInviteeAppartment, PLAYER_INDEX paramInvitingPlayerID, INT paramrootContentId )

    // Ensure an acceptance isn't already being processed
    IF (Is_Invite_Being_Accepted())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Setup_Invite_Being_Accepted() ERROR - An invitation is already being accepted.") NET_NL()
            
            SCRIPT_ASSERT("Setup_Invite_Being_Accepted(): ERROR - An Invite is already being accepted. Ignoring New Request, but this will probably cause bad stuff to happen. Tell Keith.")
        #ENDIF
        
        EXIT
    ENDIF
    
    // Check the UniqueID is valid
    IF (paramUniqueID = NO_UNIQUE_ID)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Setup_Invite_Being_Accepted() ERROR - Mission Request UniqueID passed as parameter is NO_UNIQUE_ID.") NET_NL()
            
            SCRIPT_ASSERT("Setup_Invite_Being_Accepted(): ERROR - UniqueID is NO_UNIQUE_ID. Ignoring New Request and setting Invitation Reservation FAILED flag to TRUE. Tell Keith.")
        #ENDIF
        
        g_sInviteAcceptanceMP.iaReservationFailed = TRUE
        EXIT
    ENDIF
    
    // Clear the Invite variables and set up the details of the new invitation being accepted
    Clear_Invite_Acceptance_Details()
    
    g_sInviteAcceptanceMP.iaInviteAccepted      = TRUE
    g_sInviteAcceptanceMP.iaMissionUniqueID     = paramUniqueID
    g_sInviteAcceptanceMP.iaFMType              = paramFMType
    g_sInviteAcceptanceMP.iaSubtype				= paramSubtype
    g_sInviteAcceptanceMP.iaMissionName         = paramMissionName
    g_sInviteAcceptanceMP.iaReservationFailed   = FALSE
    g_sInviteAcceptanceMP.iaInviteCoords        = paramCoords
    g_sInviteAcceptanceMP.iaFeedID              = paramFeedID
    g_sInviteAcceptanceMP.iaContentID           = sContentID
    g_sInviteAcceptanceMP.iaInviteeAppartment   = iInviteeAppartment
	g_sInviteAcceptanceMP.iaInvitingPlayer		= paramInvitingPlayerID
	g_sInviteAcceptanceMP.irootContentId		= paramrootContentId

    //g_sInviteAcceptanceMP.iaSafetyTimeout       = GET_TIME_OFFSET(GET_NETWORK_TIME(), INVITE_ACCEPT_SAFETY_TIMEOUT_msec)   //Removed by Steve T at Keith's request to fix NT 1599405 

    g_sInviteAcceptanceMP.iaSafetyTimeout       = (GET_GAME_TIMER() + INVITE_ACCEPT_SAFETY_TIMEOUT_msec)  //Added by Steve T at Keith's request to fix NT 1599405 

    
    // Acceptance successfully setup
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Invite Acceptance setup. Waiting for confirmation of reservation.")
        NET_NL()
        NET_PRINT("      [uniqueID: ")
        NET_PRINT_INT(paramUniqueID)
        NET_PRINT("] [Coords: ")
        NET_PRINT_VECTOR(paramCoords)
        NET_PRINT("] [FM Type: ")
        NET_PRINT_INT(paramFMType)
        NET_PRINT("] [Subtype: ")
        NET_PRINT_INT(paramSubtype)
        NET_PRINT("] [Mission Name: ")
        NET_PRINT(paramMissionName)
        NET_PRINT("] [FeedID: ")
        NET_PRINT_INT(paramFeedID)
        NET_PRINT("] [ContentID: ")
        NET_PRINT(sContentID)
        NET_PRINT("]")
		NET_PRINT("] [RootContentId: ")
        NET_PRINT_INT(paramrootContentId)
        NET_PRINT("]")
        NET_NL()
    #ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain waiting for a response from the Mission Controller following reservation request
//
// RETURN VALUE:        BOOL            TRUE if still waiting for acknowledgement, otherwise FALSE
FUNC BOOL Wait_For_Invite_Reservation_Confirmation()

    IF NOT (Is_Invite_Waiting_For_Reservation_Confirmation())
        RETURN FALSE
    ENDIF
    
    // Waiting for a response, so check if there is one
    // Has the reservation request failed?
    IF (Has_MP_Mission_Request_Failed(g_sInviteAcceptanceMP.iaMissionUniqueID))
        // ...mission has broadcast 'FAILED'
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: Confirmation Denied when trying to reserve place on Invite Mission.") NET_NL()
        #ENDIF

        // Clear the Invite Acceptance variables
        Clear_Invite_Acceptance_Details()
        
        // Set the 'FAILED' flag so that the Joblist App can react appropriately
        g_sInviteAcceptanceMP.iaReservationFailed = TRUE
 		
		// Restore and Refund the Wanted level if the player paid to clear it
		Restore_And_Refund_MissionsAtCoords_Cleared_Wanted_Level()
		
		// Clear Joblist Invite Apartment Details
		Clear_Same_Session_Invite_Heist_Apartment_Details()
       
	   	CLEAR_FM_JOB_ENTERY_TYPE()
	   
        RETURN FALSE
    ENDIF
    
    // Has the reservation request been confirmed?
    INT ignoreNumReservedPlayers    = 0
    INT ignoreBitsReservedPlayers   = ALL_MISSIONS_AT_COORDS_BITS_CLEAR
    
    // NOTE: Use the current uniqueID for the check - ignore if it gets updated - the MissionsAtCoords will try to reserve the player again and will handle any problems and data storage
    IF NOT (Has_MP_Mission_Request_Just_Broadcast_Confirm_Reservation(g_sInviteAcceptanceMP.iaMissionUniqueID, ignoreNumReservedPlayers, ignoreBitsReservedPlayers))
        // ...confirmation still not received, so keep on waiting unless the safety timeout has expired
        //IF (IS_TIME_LESS_THAN(GET_NETWORK_TIME(), g_sInviteAcceptanceMP.iaSafetyTimeout))  //Removed by Steve T at Keith's request to fix NT 1599405 
        IF  GET_GAME_TIMER() < g_sInviteAcceptanceMP.iaSafetyTimeout    //Added by Steve T at Keith's request to fix NT 1599405 
            // ...keep waiting
            RETURN TRUE
        ENDIF
        
        // Safety Timeout has expired, so class the request as 'failed'
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Safety Timeout expired when trying to reserve place on Invite Mission - Classing as FAILED.") NET_NL()
        #ENDIF

        // Clear the Invite Acceptance variables
        Clear_Invite_Acceptance_Details()
        
        // Set the 'FAILED' flag so that the Joblist App can react appropriately
        g_sInviteAcceptanceMP.iaReservationFailed = TRUE
        
        // Let the player know the invite acceptance failed so that it can be tried again.
        PRINT_HELP("RESERVE_FAIL")    
		
		// Restore and Refund the Wanted level if the player paid to clear it
		Restore_And_Refund_MissionsAtCoords_Cleared_Wanted_Level()
		
		// Clear Joblist Invite Apartment Details
		Clear_Same_Session_Invite_Heist_Apartment_Details()
        
        RETURN FALSE
    ENDIF
    
    INT updatedUniqueID = Get_MP_Mission_Request_Changed_UniqueID(g_sInviteAcceptanceMP.iaMissionUniqueID)
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Confirmation Received for Reservation Request. UniqueID: ")
        NET_PRINT_INT(g_sInviteAcceptanceMP.iaMissionUniqueID)
        NET_PRINT("  [Updated UniqueID: ")
        NET_PRINT_INT(updatedUniqueID)
        NET_PRINT("]")
        NET_NL()
    #ENDIF
    
    g_sInviteAcceptanceMP.iaMissionUniqueID = updatedUniqueID
    
    // If the player is now not ok or dead, unreserve player for the mission
    IF NOT (IS_NET_PLAYER_OK(PLAYER_ID()))
        #IF IS_DEBUG_BUILD
            NET_PRINT("      - PLAYER IS NOW NOT OK OR DEAD - UNRESERVE PLAYER FOR MISSION AND DON'T WARP") NET_NL()
        #ENDIF
        
        Broadcast_Cancel_Mission_Reservation_By_Player(g_sInviteAcceptanceMP.iaMissionUniqueID)
        
        Clear_Joblist_Warp_Details()
        Clear_Invite_Acceptance_Details()
        
        HANG_UP_AND_PUT_AWAY_PHONE()
		
		// Clear the Wanted level if the player paid to clear it
		Clear_MissionsAtCoords_Cleared_Wanted_Level()
		
		// Clear Joblist Invite Apartment Details
		Clear_Same_Session_Invite_Heist_Apartment_Details()
        
        // Acknowledgement received
        RETURN FALSE
    ENDIF
    
    // If there is now a Focus Mission then the player must have walked into a corona at the same time as accepting the invite
    IF (Is_There_A_MissionsAtCoords_Focus_Mission())
        #IF IS_DEBUG_BUILD
            NET_PRINT("      - PLAYER HAS A FOCUS MISSION (probably by walking into corona) - UNRESERVE PLAYER FOR INVITE MISSION AND DON'T WARP") NET_NL()
        #ENDIF
        
        Broadcast_Cancel_Mission_Reservation_By_Player(g_sInviteAcceptanceMP.iaMissionUniqueID)
        
        Clear_Joblist_Warp_Details()
        Clear_Invite_Acceptance_Details()
        
        HANG_UP_AND_PUT_AWAY_PHONE()
		
		// Restore and Refund the Wanted level if the player paid to clear it
		Restore_And_Refund_MissionsAtCoords_Cleared_Wanted_Level()
		
		// Clear Joblist Invite Apartment Details
		Clear_Same_Session_Invite_Heist_Apartment_Details()
        
        // Acknowledgement received
        RETURN FALSE
    ENDIF
    
    IF (g_sInviteAcceptanceMP.iaInviteeAppartment > -1)
        DISABLE_MP_PROPERTY_INTERIORS(g_sInviteAcceptanceMP.iaInviteeAppartment, FALSE)
    ENDIF
    
    
    // Set the NPC Invite Accept Details as Reserved if applicable
    IF (g_sCMInviteAccept.ciInvitingContact != NO_CHARACTER)
        g_sCMInviteAccept.ciReservedForJob = TRUE
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: Updating NPC Details for Accepted Invite (for CMs): ")
            TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sCMInviteAccept.ciInvitingContact)
            NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            NET_PRINT(" Filename: ")
            NET_PRINT(g_sCMInviteAccept.ciCloudFilename)
            NET_PRINT(" - PLAYER NOW RESERVED")
            NET_NL()
        #ENDIF
    ENDIF
    
    // Broadcast to other players in the same car that the player is being warped because of invite acceptance
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: About to broadcast Player Warped Because Of Invite (If in car): ")
        NET_PRINT(" [MissionName: ") NET_PRINT(g_sInviteAcceptanceMP.iaMissionName) NET_PRINT("]")
        NET_PRINT(" [FM Type: ") NET_PRINT_INT(g_sInviteAcceptanceMP.iaFMType) NET_PRINT("]")
        NET_NL()
    #ENDIF
    
    BROADCAST_EVENT_PLAYER_WARPED_BECAUSE_OF_INVITE(g_sInviteAcceptanceMP.iaFMType, g_sInviteAcceptanceMP.iaMissionName, g_sInviteAcceptanceMP.iaSubtype)
    
    // We're now committed to the FadeOut and Warp
    // Store the ID of another player in the player's car so they can be buddied for the rally - global used by Bobby
    g_piPlayerInMyVehicleWhenInvited = Store_InCar_Buddy()    
    
    // Setup the fadeout and warp
    Setup_Joblist_FadeOut_And_Warp(g_sInviteAcceptanceMP.iaInviteCoords, g_sInviteAcceptanceMP.iaContentID, g_sInviteAcceptanceMP.iaFMType, g_sInviteAcceptanceMP.iaSubtype, g_sInviteAcceptanceMP.iaInvitingPlayer, g_sInviteAcceptanceMP.iaMissionUniqueID, DEFAULT, g_sInviteAcceptanceMP.irootContentId)
    
	// make sure no other players can save my old vehicle if i'm the driver (see 1778262)
	SET_MY_VEHICLE_AS_NON_SAVEABLE()	
    
    g_sInviteAcceptanceMP.iaMissionUniqueID = NO_UNIQUE_ID
	
    RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain an invitation being accepted
PROC Maintain_Invite_Acceptance()

    IF NOT (Is_Invite_Being_Accepted())
        EXIT
    ENDIF
    
	// Disable the restricted controls for the short while whilst invite is being accepted
	g_inviteControlTimeout = GET_GAME_TIMER() + INVITE_ACCEPT_CONTROLS_BLOCK_TIMEOUT_msec
	
    // Invite Acceptance ongoing
    IF (Wait_For_Invite_Reservation_Confirmation())
        EXIT
    ENDIF
    
    // No longer waiting, nothin gto do if the reservation was denied
    IF (Has_Invite_Reservation_Request_Failed())
        EXIT
    ENDIF
    
    // Cleanup the Invite Acceptance variables when the warp is finished
    IF (Is_Joblist_Warp_In_Progress())
        EXIT
    ENDIF
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: Maintain_Invite_Acceptance - Joblist Warp is no longer in progress. Clearing the variables.") NET_NL()
    #ENDIF
	
	// Hide the Radar for one last frame to prevent a one-frame popup during an establishing shot sequence
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	THEFEED_HIDE_THIS_FRAME()
    
    Clear_Invite_Acceptance_Details()

ENDPROC


// ===========================================================================================================
//      JobList Invite and Cross-Session Feed Functions
//      (Displaying Joblist Invites in the feed)
// ===========================================================================================================

// PURPOSE:	If necessary, display a reminder feed message instead of an actual feed message
//
// INPUT PARAMS:		paramInviteSlot			The Invite Slot
// RETURN VALUE:		BOOL					TRUE if a reminder feed message was displayed, FALSE if a normal feed message should be displayed
//
// NOTES:	Added for Contact Missions that get re-added after a transiton, to batch them all up into one reminder feed message
FUNC BOOL Display_Reminder_For_ReAdded_Invite(INT paramInviteSlot)

	// Is this Invite being re-added?
	IF NOT (g_sJLInvitesMP[paramInviteSlot].jliReAdding)
		// ...Invite is not being re-added after transition, so display full feed message
		RETURN FALSE
	ENDIF

	// The reminder is being re-added
	// Check if a new reminder needs displayed
	INT thisFrame = GET_FRAME_COUNT()
	
	IF (thisFrame > g_sJoblistUpdate.reAddedFeedFrameTimeout)
		// ...need to display the Feed Reminder message
    	BEGIN_TEXT_COMMAND_THEFEED_POST("REMINDER_CM")
		END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE, FALSE)
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Joblist Invite is being re-added after transition. Displaying Reminder. Slot: ")
            NET_PRINT_INT(paramInviteSlot)
            NET_NL()
        #ENDIF
	ELSE
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Joblist Invite is being re-added after transition. Reminder recently displayed so don't need to display another. Slot: ")
            NET_PRINT_INT(paramInviteSlot)
            NET_NL()
        #ENDIF
	ENDIF
	
	// Clear the FeedID back to 'none'
	g_sJLInvitesMP[paramInviteSlot].jliFeedID = -1
	
	// KGM 5/2/14: If the max players is 0, then this data needs calculated here for re-added missions (ie: currently only Contact Missions) because
	//				this data usually gets re-calculated when the feed message is being generated for display (to prevent this search being done for multiple missions per-frame)
	IF (g_sJLInvitesMP[paramInviteSlot].jliMaxPlayers = 0)
		MP_MISSION_ID_DATA thisMissionIDdata
		Search_For_And_Retrieve_MissionIdData_From_Cloud_Filename(g_sJLInvitesMP[paramInviteSlot].jliContentID, thisMissionIDdata)
		g_sJLInvitesMP[paramInviteSlot].jliMinPlayers = Get_Minimum_Players_Required_For_FM_Cloud_Loaded_Activity(thisMissionIDdata)
		g_sJLInvitesMP[paramInviteSlot].jliMaxPlayers = Get_Maximum_Players_Required_For_FM_Cloud_Loaded_Activity(thisMissionIDdata)
		g_sJLInvitesMP[paramInviteSlot].jliRootContentIdHash = Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(thisMissionIDdata)
		g_sJLInvitesMP[paramInviteSlot].jliAdversaryModeType = Get_AdversaryModeType_For_FM_Cloud_Loaded_Activity(thisMissionIDdata)
	ENDIF
    
	// Update the 'new invites' indicator
	Increment_New_Invites_Indicator()
	
	// Update frame timeout for when another Reminder can be displayed
	g_sJoblistUpdate.reAddedFeedFrameTimeout = thisFrame + RE_ADDED_INVITE_BATCH_FRAME_TIMEOUT
	
	// Reminder was displayed or was recently displayed
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets Feed Icon based on a missions type and subtype and associated tunables
//
// INPUT PARAMS:		iconToSet				The Icon that is being used elsewhere that is being set (passed by reference)
//						altIconToSet			Secondary Icon that can also be set (positioned below the primary one, passed by reference)
//						contentIdHash			The missions content ID Hash value. Used to find associated modifier ID list
//						missionType				The mission Type as an INT
//						missionSubType			The mission SubType as an INT
//
// NOTES:	Added for setting cash and XP badges on the invite feed. The first iconToSet should remain the same unless both values are set
//			if they are then it shall be set to RP.

PROC Set_Invite_Icon(FEED_TEXT_ICON& iconToSet, FEED_TEXT_ICON& altIconToSet, INT contentIdHash, INT missionType, INT missionSubType, BOOL isPushBikeOnly = FALSE, INT RootcontentIdHash = 0, INT iAdversaryModeType = 0)	
	
	IF g_sMPTunables.bdisable_modifier_badges
		PRINTLN("[MMM][JobList] No Badge Displayed on Feed - Displaying of badges has been overridden by g_sMPTunables.bdisable_modifier_badges")
		EXIT
	ENDIF 
	
//	IF missionType = FMMC_TYPE_MISSION
//	AND missionSubType = FMMC_MISSION_TYPE_FLOW_MISSION
//		PRINTLN("[MMM][JobList] No Badge Displayed on Feed - Displaying of badges has been overridden by FMMC_MISSION_TYPE_FLOW_MISSION")
//		EXIT
//	ENDIF
	
	FLOAT xpMultiplier = 0
	FLOAT cashMultiplier = 0
	FLOAT apMultiplier = 0
	
	PRINTLN("[MMM][JobList] ----------------------------------------------- ")
	
	BOOL bAdversary = IS_THIS_AN_ADVERSARY_MODE_MISSION(RootcontentIdHash, iAdversaryModeType)
	BOOL bGangOps = GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(RootcontentIdHash)
	BOOL bArena = CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(RootcontentIdHash)
	BOOL bMember = FALSE
	
	#IF FEATURE_GTAO_MEMBERSHIP
	IF IS_GTAO_MEMBERSHIP_CONTENT_ENABLED()
		bMember = TRUE
	ENDIF
	#ENDIF
	
	Get_RP_And_Cash_Values(xpMultiplier, cashMultiplier, apMultiplier, contentIdHash, missionType, missionSubType, isPushBikeOnly, bAdversary, bGangOps, bArena, bMember)
	
	IF apMultiplier > 0
		IF cashMultiplier > 1
			PRINTLN("[MMM][JobList] Set_Invite_Icon - Ap - Displaying both Badges (AP and Cash)")
			iconToSet = TEXT_ICON_AP 	
			altIconToSet = TEXT_ICON_CASH
		ELIF xpMultiplier > 1
			PRINTLN("[MMM][JobList] Set_Invite_Icon - Ap - Displaying both Badges (AP and RP)")
			iconToSet = TEXT_ICON_AP 	
			altIconToSet = TEXT_ICON_RP
		ELSE
			PRINTLN("[MMM][JobList] Set_Invite_Icon - Ap - Displaying just AP Badge")
			altIconToSet = TEXT_ICON_AP
		ENDIF
	ELSE
		IF cashMultiplier > 1 AND xpMultiplier > 1		
			PRINTLN("[MMM][JobList] Set_Invite_Icon - Displaying both Badges")
			iconToSet = TEXT_ICON_RP 	
			altIconToSet = TEXT_ICON_CASH
			
			#IF FEATURE_COPS_N_CROOKS
			IF missionType = FMMC_TYPE_CNC_INVITE
				iconToSet = TEXT_ICON_XP_ALT 	
				altIconToSet = TEXT_ICON_CASH_ALT
			ENDIF
			#ENDIF
			
		ELIF cashMultiplier > 1
			PRINTLN("[MMM][JobList] Set_Invite_Icon - Displaying just Cash Badge")
			altIconToSet = TEXT_ICON_CASH
			
			#IF FEATURE_COPS_N_CROOKS
			IF missionType = FMMC_TYPE_CNC_INVITE
				altIconToSet = TEXT_ICON_CASH_ALT
			ENDIF
			#ENDIF
			
		ELIF xpMultiplier > 1
			PRINTLN("[MMM][JobList] Set_Invite_Icon - Displaying just XP Badge")
			altIconToSet = TEXT_ICON_RP
			
			#IF FEATURE_COPS_N_CROOKS
			IF missionType = FMMC_TYPE_CNC_INVITE
				altIconToSet = TEXT_ICON_XP_ALT
			ENDIF
			#ENDIF
			
		ELSE
			PRINTLN("[MMM][JobList] Set_Invite_Icon - No Badge Displayed")
		ENDIF	
	ENDIF
	
	PRINTLN("[MMM][JobList] ----------------------------------------------- ")
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Splits a float value on one decimel place and returns it as a string. e.g. 1.5f will become "1.5"
//
// INPUT PARAMS:		value				The input float value that is being converted
//
// RETURNS:	Returns the converted value in string format so it can be appeneded or displayed as a string	
//
// NOTES:	Does not return the .0 part, so 1.0 will become 1. This could easily be toggled but not sure if it will be needed.
//			Should probably be moved to another location so that it can be accessed anywhere
FUNC STRING Get_Float_As_String(FLOAT value)	
	INT firstValue = FLOOR(value)
	INT secondValue = FLOOR((value-firstValue) * 10)
	
	TEXT_LABEL_3 returnValue = firstValue
	IF secondValue > 0
		returnValue += "."
		returnValue += secondValue
	ENDIF
	
	RETURN CONVERT_TEXT_LABEL_TO_STRING(returnValue)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Gets the text badge to be used for displaying RP and Cash badges on the phone as a string
//
// INPUT PARAMS:		contentIdHash			The missions content ID Hash value. Used to find associated modifier ID list
//						missionType				The mission Type as an INT
//						missionSubType			The mission SubType as an INT
//
// RETURNS:	Returns the badge value in string format including the multiplier value so 
//			it can be appeneded or displayed as a string	
//
// NOTES:	Does not return the .0 part, so 1.0 will become 1. This could easily be toggled but not sure if it will be needed.
//			Should probably be moved to another location so that it can be accessed anywhere
FUNC STRING Get_Text_Append_Badge(INT contentIdHash, INT missionType, INT missionSubType, BOOL isPushBikeOnly = FALSE, INT RootcontentIdHash = 0, INT iAdversaryModeType = 0)
	
	IF g_sMPTunables.bdisable_modifier_badges
		PRINTLN("[MMM][JobList] No Badge Displayed on Phone - Displaying of badges has been overridden by g_sMPTunables.bdisable_modifier_badges")
		RETURN CONVERT_TEXT_LABEL_TO_STRING("") 
	ENDIF 
	
//	IF missionType = FMMC_TYPE_MISSION
//	AND missionSubType = FMMC_MISSION_TYPE_FLOW_MISSION
//		RETURN CONVERT_TEXT_LABEL_TO_STRING("") 
//	ENDIF
	
	FLOAT xpMultiplier = 0
	FLOAT cashMultiplier = 0
	FLOAT apMultiplier = 0
	
	TEXT_LABEL_63 textAppend = ""
	
	BOOL bAdversary = IS_THIS_AN_ADVERSARY_MODE_MISSION(RootcontentIdHash, iAdversaryModeType)
	BOOL bGangOps = GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(RootcontentIdHash)
	BOOL bArena = CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_ARENA_SERIES(RootcontentIdHash)
	BOOL bMember = FALSE
	
	#IF FEATURE_GTAO_MEMBERSHIP
	IF IS_GTAO_MEMBERSHIP_CONTENT_ENABLED()
		bMember = TRUE
	ENDIF
	#ENDIF
	
	PRINTLN("[MMM][JobList] ----------------------------------------------- ")
	Get_RP_And_Cash_Values(xpMultiplier, cashMultiplier, apMultiplier, contentIdHash, missionType, missionSubType, isPushBikeOnly, bAdversary, bGangOps, bArena, bMember)
	
	IF apMultiplier > 0
		IF cashMultiplier > 1
			PRINTLN("[MMM][JobList] Get_Text_Append_Badge - Ap - Appending both Badges (AP and Cash)")
			textAppend = " " 
			textAppend += Get_Float_As_String(apMultiplier)
			textAppend += ("x ~BLIP_AP~ ")
			textAppend += Get_Float_As_String(cashMultiplier)
			textAppend += ("x ~BLIP_CASH~") 
		ELIF xpMultiplier > 1
			PRINTLN("[MMM][JobList] Get_Text_Append_Badge - Ap - Appending both Badges (AP and RP)")
			textAppend = " " 
			textAppend += Get_Float_As_String(apMultiplier)
			textAppend += ("x ~BLIP_AP~ ")
			textAppend += Get_Float_As_String(cashMultiplier)
			textAppend += ("x ~BLIP_RP~") 
		ELSE
			PRINTLN("[MMM][JobList] Get_Text_Append_Badge - Ap - Appending just AP Badge")
			textAppend = " "
			textAppend += Get_Float_As_String(apMultiplier)
			textAppend += ("x ~BLIP_AP~") 
		ENDIF
	ELSE
		IF cashMultiplier > 1 AND xpMultiplier > 1		
			PRINTLN("[MMM][JobList] Get_Text_Append_Badge - Appending both Badges")
			//textAppend = ("RP, CASH")
			textAppend = " " 
			textAppend += Get_Float_As_String(xpMultiplier)
			textAppend += ("x ~BLIP_RP~ ")
			textAppend += Get_Float_As_String(cashMultiplier)
			textAppend += ("x ~BLIP_CASH~") 
		ELIF cashMultiplier > 1
			PRINTLN("[MMM][JobList] Get_Text_Append_Badge - Appending just Cash Badge")
			//textAppend = ("CASH")
			textAppend = " "
			textAppend += Get_Float_As_String(cashMultiplier)
			textAppend += ("x ~BLIP_CASH~") 
		ELIF xpMultiplier > 1
			PRINTLN("[MMM][JobList] Get_Text_Append_Badge - Appending just XP Badge")
			//textAppend = (" RP") 
			textAppend = " "
			textAppend += Get_Float_As_String(xpMultiplier)
			textAppend += ("x ~BLIP_RP~") 
			//textAppend = (" (" + xpMultiplier + "RP)") 
		ELSE
			PRINTLN("[MMM][JobList] Get_Text_Append_Badge - No Badge Displayed")
			textAppend = ("") 
		ENDIF
	ENDIF
	
	PRINTLN("[MMM][JobList] ----------------------------------------------- ")
	
	RETURN CONVERT_TEXT_LABEL_TO_STRING(textAppend)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Use the raw data to get this Heist Finale RCID Hash from the ContentID string
FUNC INT Joblist_Get_Heist_Finale_RCID_From_ContentID(TEXT_LABEL_23 paramContentID)

	INT tempLoop = 0
	REPEAT g_numLocalMPHeists tempLoop
		IF (ARE_STRINGS_EQUAL(paramContentID, g_sLocalMPHeists[tempLoop].lhsMissionIdData.idCloudFilename))
			RETURN (Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(g_sLocalMPHeists[tempLoop].lhsMissionIdData))
		ENDIF
	ENDREPEAT
	
	RETURN 0

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Display a Help Message along with the Heist Versus Mission?
//
// INPUT PARAMS:		paramInviteSlot		The Invite slot containing the current invite being sent to the feed
//
// NOTES:	All Versus Mission invites are sent by Martin
PROC Display_Heist_Versus_Help_Message_With_Invite(INT paramInviteSlot)

	IF (g_sJLInvitesMP[paramInviteSlot].jliInvitorIsPlayer)
		EXIT
	ENDIF
	
	IF (g_sJLInvitesMP[paramInviteSlot].jliType != FMMC_TYPE_MISSION)
		EXIT
	ENDIF
	
	IF (g_sJLInvitesMP[paramInviteSlot].jliSubtype != FMMC_MISSION_TYPE_VERSUS)
		EXIT
	ENDIF
	
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		EXIT
	ENDIF
	
	//url:bugstar:2232026 - Help text flashed up for a moment when I received an invite, while I was on the Heist selection board.
	//Dont show if we're on the board.
	IF ON_HEIST_STRAND_PLANNING_BOARD()
		EXIT
	ENDIF
	
	// This is a Versus Mission Invite sent by Martin - display the help message
	// "NEW VERSUS MODE AVAILABLE: accept Martin's invite on your Job List to try out this new mode."
	//PRINT_HELP("HVS_INVITE")
	PRINTLN(".KGM [Joblist][Heist]: Display Versus Help message along with Heist Versus feed message.")

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display a Joblist Entry as a Text Message Feed Message
//
// INPUT PARAMS:        paramInviteSlot         The Array Position for this Joblist invitation
//						paramIsReminder			[DEFAULT=FALSE] TRUE if this is a reminder for a previously sent Invite, FALSE if not a reminder
//
// NOTES:   KGM 26/6/13: This has become a fucking mess!
PROC Display_Invite_As_Feed_Message(INT paramInviteSlot, BOOL paramIsReminder = FALSE)

	PRINTLN(".KGM [JobList]: Display_Invite_As_Feed_Message() - called")

	// Whatever happens, clear out the 'repeat' variables here - if there's a problem we don't want a repeat
	BOOL repeatRequired = g_sJLInvitesMP[paramInviteSlot].jliRepeatInvite
	
	g_sJLInvitesMP[paramInviteSlot].jliRepeatInvite		= FALSE
	g_sJLInvitesMP[paramInviteSlot].jliRepeatTimeout	= 0

    IF (GET_CURRENT_GAMEMODE() != GAMEMODE_FM)
        NET_PRINT("Display_Invite_As_Feed_Message(): This function is currently Freemode-specific. This is probably a timing issue - during a transition the gamemode temporarily swaps from being Freemode.") NET_NL()
        EXIT
    ENDIF
	
	// Don't display a feed message if the player is in SCTV
	IF (IS_PLAYER_SCTV(PLAYER_ID()))
        // Clear the FeedID back to 'none'
        g_sJLInvitesMP[paramInviteSlot].jliFeedID = -1
    
	    // Update the 'new invites' indicator
	    Increment_New_Invites_Indicator()
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Player is in SCTV. Not displaying Invite Feed Message. Slot: ")
            NET_PRINT_INT(paramInviteSlot)
            NET_NL()
        #ENDIF
        
        EXIT
	ENDIF
    
    // Don't display a feed entry if suppressed for some reason
    // (ie: a 'lose your wanted level' feed entry was sent instead)
    IF (g_sJLInvitesMP[paramInviteSlot].jliFeedID = INVITE_FEED_ENTRY_NOT_REQUIRED)
        // Clear the FeedID back to 'none'
        g_sJLInvitesMP[paramInviteSlot].jliFeedID = -1
    
	    // Update the 'new invites' indicator
		IF NOT (paramIsReminder)
	    	Increment_New_Invites_Indicator()
		ENDIF
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Joblist Invite didn't need sent to feed (possibly had been replaced by 'lose your wanted level' feed message). Slot: ")
            NET_PRINT_INT(paramInviteSlot)
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
	
	GAMER_HANDLE tGH = GET_GAMER_HANDLE_PLAYER(g_sJLInvitesMP[paramInviteSlot].jliPlayer)
	
	// KGM 15/7/15 [BUG 2422920]: Non-Friend invite feed messages don't display during FM Events under certain conditions (the invite still appears in joblist)
	BOOL allowFmEventInviteFeedMessage = TRUE
	IF (IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID()))
	OR (IS_PLAYER_PERMANENT_PARTICIPANT_TO_ANY_EVENT(PLAYER_ID()))
		PRINTLN(".KGM [JobList]: Display_Invite_As_Feed_Message(): Player is a Critical or Permanent Participant in an FM Event")
		
		// Only invites from friends should display a feed message if critical or permanent
		IF (g_sJLInvitesMP[paramInviteSlot].jliInvitorIsPlayer)
			PRINTLN(".KGM [JobList]: Display_Invite_As_Feed_Message(): ...inviter is a player: ", GET_PLAYER_NAME(g_sJLInvitesMP[paramInviteSlot].jliPlayer))
			
			// Invitor is a player
			IF NOT (NETWORK_IS_FRIEND(tGH))
				PRINTLN(".KGM [JobList]: Display_Invite_As_Feed_Message(): ...BUT: inviter is not a friend, so this should not display a feed message")
				// Inviter is not a friend
				allowFmEventInviteFeedMessage = FALSE
			ELSE
				PRINTLN(".KGM [JobList]: Display_Invite_As_Feed_Message(): ...AND: inviter is a friend, so can display a feed message")
			ENDIF
		ELSE
			PRINTLN(".KGM [JobList]: Display_Invite_As_Feed_Message(): ...BUT: inviter is not a player, so this should not display a feed message")
			allowFmEventInviteFeedMessage = FALSE
		ENDIF
	ELSE
		PRINTLN(".KGM [JobList]: Display_Invite_As_Feed_Message(): Player is NOT a Critical or Permanent Participant in an FM Event so can display a feed message")
	ENDIF
	
	//url:bugstar:7715975 - Gen9 - [R5304] The blocking user is able to view the blocked player's UGC Motorcycle Club emblem from the phone invite.
	IF IS_GAMERHANDLE_BLOCKED(tGH)
		PRINTLN(".KGM [JobList]: Display_Invite_As_Feed_Message(): ...BUT: IS_GAMERHANDLE_BLOCKED, so this should not display a feed message")
		allowFmEventInviteFeedMessage = FALSE
	ENDIF
	
    // Get the Player Headshot
    BOOL            invitorIsPlayer     = g_sJLInvitesMP[paramInviteSlot].jliInvitorIsPlayer
    PEDHEADSHOT_ID  theHeadshotID       = NULL
    BOOL            isPlaylistInvite    = FALSE
    BOOL            isChallangeInvite   = FALSE
	BOOL			isHeadToHeadInvite	= FALSE
	BOOL			isTournamentQualifyerInvite 	= FALSE
	
	// Get amount of players required for this mission
	// NOTE: This will be gathered already for Player Invites, but still needs gathered for NPC invites
    IF NOT (invitorIsPlayer)
		// ...still need to gather this data for NPC invite
		// TEMP: Use a specific minigame check - hopefully this will become obsolete with the fake minigame contentIDs
		IF (g_sJLInvitesMP[paramInviteSlot].jliCreatorID = FMMC_MINI_GAME_CREATOR_ID)
			MP_MISSION missionID = Convert_FM_Mission_Type_To_MissionID(g_sJLInvitesMP[paramInviteSlot].jliType)
			
			g_sJLInvitesMP[paramInviteSlot].jliMinPlayers = Get_Minimum_Number_Of_Players_Required_For_MP_Mission(missionID)
			g_sJLInvitesMP[paramInviteSlot].jliMaxPlayers = GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionID)
		ELSE
			MP_MISSION_ID_DATA thisMissionIDdata
			IF (Search_For_And_Retrieve_MissionIdData_From_Cloud_Filename(g_sJLInvitesMP[paramInviteSlot].jliContentID,thisMissionIDdata))
				g_sJLInvitesMP[paramInviteSlot].jliMinPlayers = Get_Minimum_Players_Required_For_FM_Cloud_Loaded_Activity(thisMissionIDdata)
				g_sJLInvitesMP[paramInviteSlot].jliMaxPlayers = Get_Maximum_Players_Required_For_FM_Cloud_Loaded_Activity(thisMissionIDdata)
				g_sJLInvitesMP[paramInviteSlot].jliIsPushBikeOnly = Is_FM_Cloud_Loaded_Activity_A_Bicycle_Race(thisMissionIDdata)	// TODO: Possibly make this part of the invite in the same way as 'unplayed' is
				BOOL isPlayed = Get_Played_Status_FM_Cloud_Loaded_Activity(thisMissionIDdata)
				IF NOT (isPlayed)
					g_sJLInvitesMP[paramInviteSlot].jliIsUnplayed = TRUE
				ENDIF
			ENDIF
		ENDIF
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Joblist Invite is from NPC. Slot: ")
            NET_PRINT_INT(paramInviteSlot)
			NET_PRINT(" - Gathered Min/Max players: ")
			NET_PRINT_INT(g_sJLInvitesMP[paramInviteSlot].jliMinPlayers)
			NET_PRINT(" / ")
			NET_PRINT_INT(g_sJLInvitesMP[paramInviteSlot].jliMaxPlayers)
            NET_NL()
        #ENDIF
	ENDIF
	
	IF NOT (allowFmEventInviteFeedMessage)
		// Invite Feed Message for FM Eventis not required
		PRINTLN(".KGM [JobList]: Display_Invite_As_Feed_Message(): Invite Feed message suppressed due to FM Event participation rules")
		
		// Ensure the 'new invites' indicators get updated as appropriate
		IF NOT (paramIsReminder)
		    Set_New_Invite_Received()
		    
		    // Update the 'new invites' indicator
		    Increment_New_Invites_Indicator()
		ENDIF
		
		EXIT
	ELSE
		PRINTLN(".KGM [JobList]: Display_Invite_As_Feed_Message(): Invite Feed message is NOT suppressed due to FM Event participation rules")
	ENDIF
	
	// Check if a reminder Feed Message is required instead
	IF (Display_Reminder_For_ReAdded_Invite(paramInviteSlot))
		EXIT
	ENDIF
	
    IF (invitorIsPlayer)
        theHeadshotID = Get_HeadshotID_For_Player(g_sJLInvitesMP[paramInviteSlot].jliPlayer)
        
        IF (IS_PLAYER_ON_A_PLAYLIST(g_sJLInvitesMP[paramInviteSlot].jliPlayer))
            isPlaylistInvite = TRUE
			IF (IS_PLAYER_ON_A_PLAYLIST_TOURNAMENT_QUALIFYER_PLAYLIST(g_sJLInvitesMP[paramInviteSlot].jliPlayer))
				isTournamentQualifyerInvite = TRUE
			ENDIF
        ENDIF
    ENDIF
    
    TEXT_LABEL_63   theTXD
    
    IF (invitorIsPlayer)
        IF (theHeadshotID = NULL)
            theTXD = "CHAR_DEFAULT"
        ELSE
            theTXD = GET_PEDHEADSHOT_TXD_STRING(theHeadshotID)
        ENDIF
    ELSE
        theTXD = GET_FILENAME_FOR_AUDIO_CONVERSATION(Get_NPC_Headshot(g_sJLInvitesMP[paramInviteSlot].jliNPC))
    ENDIF
    
    // Decide what needs to be displayed in the feed
    // NOTE: This has become a bit complicated
    TEXT_LABEL_15   textConfig          = "JL_INVITE"
    BOOL            displayMissionName  = FALSE
    BOOL            displayDescription  = FALSE
             
    TEXT_LABEL_31   PlaylistTitle       = ""    
    
    TEXT_LABEL_63   activityType        = Get_FM_Joblist_Activity_TextLabel(g_sJLInvitesMP[paramInviteSlot].jliType, g_sJLInvitesMP[paramInviteSlot].jliSubtype, DEFAULT, g_sJLInvitesMP[paramInviteSlot].jliRootContentIdHash)      
    TEXT_LABEL_63   activityAsString    = GET_FILENAME_FOR_AUDIO_CONVERSATION(activityType)
           
	PRINTLN(".KGM [JobList]: Display_Invite_As_Feed_Message() - type = ", g_sJLInvitesMP[paramInviteSlot].jliType)
	PRINTLN(".KGM [JobList]: Display_Invite_As_Feed_Message() - subtype = ", g_sJLInvitesMP[paramInviteSlot].jliSubtype)
	PRINTLN(".KGM [JobList]: Display_Invite_As_Feed_Message() - root content id hash = ", g_sJLInvitesMP[paramInviteSlot].jliRootContentIdHash)
	PRINTLN(".KGM [JobList]: Display_Invite_As_Feed_Message() - activityAsString = ", activityAsString)
				
    // Temp for CWP until I change over to using the new 500-char string method
    STRING          theDescription      = ""
	UGC_DESCRIPTION	splitDescriptions
	BOOL			foundDescription	= FALSE
    
    IF (g_sJLInvitesMP[paramInviteSlot].jliDescHash != 0)
	AND NOT (g_sJLInvitesMP[paramInviteSlot].jliUseHeistCutsceneDesc)
        theDescription = UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(g_sJLInvitesMP[paramInviteSlot].jliDescHash, INVITE_MAX_DESCRIPTION_LENGTH)
		Split_Cached_Description_Into_TL63s(g_sJLInvitesMP[paramInviteSlot].jliDescHash, splitDescriptions)
        IF NOT (IS_STRING_NULL_OR_EMPTY(theDescription))
			foundDescription = TRUE
		ENDIF
	ELSE
    	IF (g_sJLInvitesMP[paramInviteSlot].jliDescHash = 0)
		AND NOT (g_sJLInvitesMP[paramInviteSlot].jliUseHeistCutsceneDesc)
			// KGM 16/3/15 [BUG 2275132]: Heist Prep details from the cache will have to also retrieve the cached description
			IF (g_sJLInvitesMP[paramInviteSlot].jliType = FMMC_TYPE_MISSION)
			AND (g_sJLInvitesMP[paramInviteSlot].jliSubtype = FMMC_MISSION_TYPE_PLANNING)
				// This is a Heist Prep, so the description may be cached along with the heist prep header details
				// NOTE: This is the new script cache setup by Bobby, not the Code Cache of the descriptions
				Copy_Description_From_Heist_Prep_Cache_Into_TL63s(g_sJLInvitesMP[paramInviteSlot].jliContentID, splitDescriptions)
				foundDescription = TRUE
			ENDIF
		ENDIF
    ENDIF
	
	// KGM 4/11/14 [BUG 2111097]: For NPC descriptions, check if a specific piece of description text should be displayed
	IF NOT (invitorIsPlayer)
		IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJLInvitesMP[paramInviteSlot].jliDescription))
			theDescription = GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sJLInvitesMP[paramInviteSlot].jliDescription)
			PRINTLN(".KGM [JobList]: NPC Invite is displaying specific description. Slot: ", paramInviteSlot, ". TL: ", g_sJLInvitesMP[paramInviteSlot].jliDescription)
			
			// Also 'split' it as though it is a 500-length description (clearing out the remainder)
			Split_Full_Description_String_Into_TL63s(theDescription, splitDescriptions)
	        IF NOT (IS_STRING_NULL_OR_EMPTY(theDescription))
				foundDescription = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// KGM 14/11/14 [BUG 2157535]: For Heist Cutscene Invites, need to use the umbrella description
	IF (invitorIsPlayer)
		IF (g_sJLInvitesMP[paramInviteSlot].jliUseHeistCutsceneDesc)
			INT thisHeistRCID = Joblist_Get_Heist_Finale_RCID_From_ContentID(g_sJLInvitesMP[paramInviteSlot].jliContentID)
			TEXT_LABEL_15 theUmbrellaTL = GET_HEIST_STRAND_GENERIC_DESCRIPTION(thisHeistRCID)
			
			IF NOT (IS_STRING_NULL_OR_EMPTY(theUmbrellaTL))
				theDescription = GET_FILENAME_FOR_AUDIO_CONVERSATION(theUmbrellaTL)
				PRINTLN(".KGM [JobList][Heist]: Invite to Heist Cutscene displaying Umbrella Description. Slot: ", paramInviteSlot, ". TL: ", theUmbrellaTL)
				
				// Also 'split' it as though it is a 500-length description (clearing out the remainder)
				Split_Full_Description_String_Into_TL63s(theDescription, splitDescriptions)
		        IF NOT (IS_STRING_NULL_OR_EMPTY(theDescription))
					foundDescription = TRUE
				ENDIF
			ELSE
				PRINTLN(".KGM [JobList][Heist]: Invite to Heist Cutscene but umbrella TL is NULL or EMPTY.")
			ENDIF
		ENDIF
	ENDIF
    
     // Information about the jobs in a playlist
    INT currentPlaylistJob  = 0
    INT totalPlaylistJobs   = 0
		
	INT minPlayers = g_sJLInvitesMP[paramInviteSlot].jliMinPlayers
	INT maxPlayers = g_sJLInvitesMP[paramInviteSlot].jliMaxPlayers
	BOOL isUnplayed = g_sJLInvitesMP[paramInviteSlot].jliIsUnplayed
	
	BOOL sameMinMaxPlayers = FALSE
	IF (minPlayers = maxPlayers)
		sameMinMaxPlayers = TRUE
	ENDIF
	
	// KGM 19/10/14: Don't treat Heist Prep missions (which need to be downloaded) as UGC Invites
	BOOL ugcInvite = FALSE
	IF (g_sJLInvitesMP[paramInviteSlot].jliCreatorID < NUM_NETWORK_PLAYERS)
		ugcInvite = TRUE
		IF (g_sJLInvitesMP[paramInviteSlot].jliType = FMMC_TYPE_MISSION)
			IF (g_sJLInvitesMP[paramInviteSlot].jliSubtype = FMMC_MISSION_TYPE_HEIST)
			OR (g_sJLInvitesMP[paramInviteSlot].jliSubtype = FMMC_MISSION_TYPE_PLANNING)
				ugcInvite = FALSE
				
		        #IF IS_DEBUG_BUILD
		            NET_PRINT("...KGM MP [JobList]: ")
		            NET_PRINT_TIME()
					NET_PRINT("Heist-related mission with a non-Rockstar CreatorID, but still display as Rockstar Created.")
					NET_NL()
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
    
    IF (isPlaylistInvite)
//        GlobalplayerBD_FM[NATIVE_TO_INT(g_sJLInvitesMP[paramInviteSlot].jliPlayer)].sPlaylistVars.iCurrentPlayListPosition + 1
        currentPlaylistJob  = GlobalplayerBD_FM_2[NATIVE_TO_INT(g_sJLInvitesMP[paramInviteSlot].jliPlayer)].iPlaylistStartPos
        totalPlaylistJobs   = GlobalplayerBD_FM[NATIVE_TO_INT(g_sJLInvitesMP[paramInviteSlot].jliPlayer)].sPlaylistVars.iLength
        PlaylistTitle       = GlobalplayerBD_FM[NATIVE_TO_INT(g_sJLInvitesMP[paramInviteSlot].jliPlayer)].tl31PlaylistName
        
        if IS_PLAYER_ON_A_CHALLENGE(g_sJLInvitesMP[paramInviteSlot].jliPlayer)
        or IS_PLAYER_SETTING_CHALLENGE(g_sJLInvitesMP[paramInviteSlot].jliPlayer)
        OR IS_PLAYER_ON_A_H2H_PLAYLIST(g_sJLInvitesMP[paramInviteSlot].jliPlayer)
			IF (IS_PLAYER_ON_A_H2H_PLAYLIST(g_sJLInvitesMP[paramInviteSlot].jliPlayer))
				isHeadToHeadInvite = TRUE
			ELSE
            	isChallangeInvite = true
			ENDIF
        endif
    ENDIF
        
    // Should additional details be displayed other than the Mission Type?
    IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJLInvitesMP[paramInviteSlot].jliMissionName))
        // ...also display the mission name
        textConfig          = "JLI_PP_ND"
        displayMissionName  = TRUE
        
        // Should the Mission Description be displayed?
        IF (foundDescription)
            // ...also display the mission description
            textConfig          = "JLI_PP_NDD"
            displayDescription  = TRUE
            
            // Display 'playlist' or 'Rockstar' along with name and description?
            IF (isPlaylistInvite)
				// Playlist
                if isChallangeInvite
                    textConfig = "JL_INVITE_CHND"
				ELIF (isHeadToHeadInvite)
					textConfig = "JL_INVITE_H2HND"
				ELIF (isTournamentQualifyerInvite)
					textConfig = "JL_INV_TQD"
                else
                    textConfig = "JL_INV_PND_DI"
                endif
			ELSE
				// Not a Playlist
				IF (sameMinMaxPlayers)
					// Min and Max players are the same
					IF (ugcInvite)
						// Non-Rockstar mission
						textConfig = "JLI_P_NDDNR"
					ELSE
						// Rockstar mission
						textConfig = "JLI_P_NDD"
					ENDIF
				ELSE
					// Min and Max Players are different
					IF (ugcInvite)
						// Non-Rockstar mission
						textConfig = "JLI_PP_NDDNR"
					ELSE
						// Rockstar mission
						textConfig = "JLI_PP_NDD"
					ENDIF
				ENDIF
            ENDIF
        ELSE
			// ...don't display mission description
            // Display 'playlist' or 'Rockstar' along with name?
            IF (isPlaylistInvite)
                if isChallangeInvite
                    textConfig = "JL_INVITE_CHN"
				ELIF (isHeadToHeadInvite)
					textConfig = "JL_INVITE_H2HN"
				ELIF (isTournamentQualifyerInvite)
					textConfig = "JL_INV_TQN"
                else
                    textConfig = "JL_INV_PN_DI"
                endif
			ELSE
				// Not a Playlist
				IF (sameMinMaxPlayers)
					// Min and Max players are the same
					IF (ugcInvite)
						// Non-Rockstar mission
						textConfig = "JLI_P_NDNR"
					ELSE
						// Rockstar mission
						textConfig = "JLI_P_ND"
					ENDIF
				ELSE
					// Min and Max Players are different
					IF (ugcInvite)
						// Non-Rockstar mission
						textConfig = "JLI_PP_NDNR"
					ELSE
						// Rockstar mission
						textConfig = "JLI_PP_ND"
					ENDIF
				ENDIF
            ENDIF
        ENDIF
    ELSE
        // Display 'playlist'
        IF (isPlaylistInvite)
            if isChallangeInvite
                textConfig = "JL_INVITE_CH"
			ELIF (isHeadToHeadInvite)
				textConfig = "JL_INVITE_H2H"
			ELIF (isTournamentQualifyerInvite)
				textConfig = "JL_INV_TQ"
            else
                textConfig = "JL_INV_P_DI"
            endif
        ENDIF
    ENDIF
    
    // Gather the Invitor Name information and wrap it up in 'condensed font' markers
	// Also get and display the inviters crew tag if it is the same as the players
    TEXT_LABEL_63 invitorName
	TEXT_LABEL_15 inviterCTag
	
    IF (invitorIsPlayer)
        invitorName = "<C>"
        invitorName += GET_PLAYER_NAME(g_sJLInvitesMP[paramInviteSlot].jliPlayer)
        invitorName += "</C>"
		
		// Get the inviting players Gamer_Handle
		INT inviteCID = GET_MP_PLAYER_CLAN_ID(tGH)
		INT localCID = GET_LOCAL_PLAYER_CLAN_ID()
		
		// Compare the two players crew IDs
		IF inviteCID = localCID
			PRINTLN("[MMM][JobList] Same Session Invite : Crew IDs match : Now Getting Crew Tag for Scaleform...")
			GET_PLAYER_CREW_TAG_FOR_SCALEFORM(g_sJLInvitesMP[paramInviteSlot].jliPlayer, inviterCTag)
			PRINTLN("[MMM][JobList] GET_PLAYER_CREW_TAG_FOR_SCALEFORM : COMPLETE, TAG : ", inviterCTag )
		ELSE
			PRINTLN("[MMM][JobList] Same Session Invite : Crew IDs ", inviteCID, " and ", localCID ," do not match")
		ENDIF		
    ELSE
        TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLInvitesMP[paramInviteSlot].jliNPC)
        invitorName += GET_FILENAME_FOR_AUDIO_CONVERSATION(npcNameTL)
    ENDIF
	
    //Distance to mission
    Int iInviteDistance = 0
    IF NOT (IS_PED_INJURED(PLAYER_PED_ID()))
        vector playerCoords     = GET_PLAYER_COORDS(PLAYER_ID())
        iInviteDistance         = ROUND(GET_DISTANCE_BETWEEN_COORDS(playerCoords, g_sJLInvitesMP[paramInviteSlot].jliCoords))        
    endif
    
    // The Feed text Icon to use
    FEED_TEXT_ICON theInviteIcon = TEXT_ICON_INVITE
	FEED_TEXT_ICON theAltIcon	 = TEXT_ICON_BLANK
	INT iHash = GET_HASH_KEY(g_sJLInvitesMP[paramInviteSlot].jliContentID)
	PRINTLN("[MMM][JobList] Setting Invite Icon for Same Session Invite ...")	
	Set_Invite_Icon(theInviteIcon, theAltIcon, 
					iHash, 
					g_sJLInvitesMP[paramInviteSlot].jliType, 
					g_sJLInvitesMP[paramInviteSlot].jliSubtype, 
					g_sJLInvitesMP[paramInviteSlot].jliIsPushBikeOnly, 
					g_sJLInvitesMP[paramInviteSlot].jliRootContentIdHash, 
					g_sJLInvitesMP[paramInviteSlot].jliAdversaryModeType)
		
    // Display the Feed Entry and store the returned Feed ID
    Set_Feed_Flash_Style_For_Invites()
	Set_Feed_Background_Colour_For_Invite(g_sJLInvitesMP[paramInviteSlot].jliType, g_sJLInvitesMP[paramInviteSlot].jliSubtype, g_sJLInvitesMP[paramInviteSlot].jliRootContentIdHash, g_sJLInvitesMP[paramInviteSlot].jliAdversaryModeType)
	
	#IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
		NET_PRINT("Displaying Invite Feed Message using this Text Label Configuration: ")
		NET_PRINT(textConfig)
		NET_PRINT(" (")
		NET_PRINT(GET_STRING_FROM_TEXT_FILE(textConfig))
		NET_PRINT(")")
		NET_NL()
	#ENDIF
    
    if(isPlaylistInvite)
        //  Playlist: ~a~ (Job ~1~ of ~1~)~n~~a~ (~1~m)~n~~a~
        //  Challenge: ~a~ (Job ~1~ of ~1~)~n~~a~~n~~a~
		//	Head To Head: ~a~ (Jobs: ~1~), Wager ~1~~n~~a~
		BEGIN_TEXT_COMMAND_THEFEED_POST(textConfig)         
            NET_PRINT("....CV PlaylistTitle: ")NET_PRINT(PlaylistTitle)NET_NL()
            ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(PlaylistTitle)
			IF NOT (isHeadToHeadInvite)
            	ADD_TEXT_COMPONENT_INTEGER(currentPlaylistJob)
			ENDIF
            ADD_TEXT_COMPONENT_INTEGER(totalPlaylistJobs)
			IF (isHeadToHeadInvite)
				ADD_TEXT_COMPONENT_INTEGER(g_sJLInvitesMP[paramInviteSlot].jliWagerH2H)
			ENDIF
            ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sJLInvitesMP[paramInviteSlot].jliMissionName)   
            if not isChallangeInvite
			AND NOT (isHeadToHeadInvite)
                ADD_TEXT_COMPONENT_INTEGER(iInviteDistance)//display distance to mission in meters
            endif
            IF (displayDescription)
			 	#IF IS_DEBUG_BUILD
			        NET_PRINT("...KGM MP [JobList]: ")
			        NET_PRINT_TIME()
					NET_PRINT("Displaying these description split lines for playlist:")
					NET_NL()
				#ENDIF
				
               	INT descLoop = 0
				REPEAT NUM_TEXTLABELS_IN_UGC_DESCRIPTION descLoop
					ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(splitDescriptions.TextLabel[descLoop])
					
					#IF IS_DEBUG_BUILD
						NET_PRINT("      ")
						NET_PRINT_INT(descLoop)
						NET_PRINT(".  ")
						NET_PRINT(splitDescriptions.TextLabel[descLoop])
						NET_NL()
					#ENDIF
				ENDREPEAT

        ENDIF
		
		// Now works with the new functions when including latest game_stream.gfx file
		g_sJLInvitesMP[paramInviteSlot].jliFeedID = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT_WITH_CREW_TAG_AND_ADDITIONAL_ICON(theTXD, theTXD, TRUE, theInviteIcon, invitorName, activityAsString, 1, inviterCTag, theAltIcon)
		PRINTLN(".KGM [JobList]: Display_Invite_As_Feed_Message() - END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT_WITH_CREW_TAG_AND_ADDITIONAL_ICON [1]")
   	else
		// (~1~ to ~1~ Players)~n~~a~ (~1~m)~n~~a~
		BEGIN_TEXT_COMMAND_THEFEED_POST(textConfig)
			ADD_TEXT_COMPONENT_INTEGER(minPlayers)
			IF NOT (sameMinMaxPlayers)
	            ADD_TEXT_COMPONENT_INTEGER(maxPlayers)
			ENDIF
        // If required, display the mission name
        IF (displayMissionName)
            ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sJLInvitesMP[paramInviteSlot].jliMissionName)
                ADD_TEXT_COMPONENT_INTEGER(iInviteDistance)//display distance to mission in meters
            // If required, also display the mission description
            IF (displayDescription)
			 	#IF IS_DEBUG_BUILD
			        NET_PRINT("...KGM MP [JobList]: ")
			        NET_PRINT_TIME()
					NET_PRINT("Displaying these description split lines:")
					NET_NL()
				#ENDIF
				
                INT descLoop = 0
				REPEAT NUM_TEXTLABELS_IN_UGC_DESCRIPTION descLoop
					ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(splitDescriptions.TextLabel[descLoop])
					
					#IF IS_DEBUG_BUILD
						NET_PRINT("      ")
						NET_PRINT_INT(descLoop)
						NET_PRINT(".  ")
						NET_PRINT(splitDescriptions.TextLabel[descLoop])
						NET_NL()
					#ENDIF
				ENDREPEAT
            ENDIF
        ELSE            
            ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY("")// Nothing to display
        ENDIF
		
		Append_Played_Status_To_Activity_String(isUnplayed, g_sJLInvitesMP[paramInviteSlot].jliCreatorID, activityAsString)
		// Now works with the new functions when including latest game_stream.gfx file
		g_sJLInvitesMP[paramInviteSlot].jliFeedID = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT_WITH_CREW_TAG_AND_ADDITIONAL_ICON(theTXD, theTXD, TRUE, theInviteIcon, invitorName, activityAsString, 1, inviterCTag, theAltIcon)
		PRINTLN(".KGM [JobList]: Display_Invite_As_Feed_Message() - END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT_WITH_CREW_TAG_AND_ADDITIONAL_ICON [2]")
   ENDIF
   
    Clear_Feed_Flash_Style_For_Invites()
    Store_Feed_Message_As_Most_Recent_For_AutoLaunch(g_sJLInvitesMP[paramInviteSlot].jliFeedID)
    Play_Invite_Received_Sound_Effect()
	
	IF NOT (paramIsReminder)
	    Set_New_Invite_Received()
	    
	    // Update the 'new invites' indicator
	    Increment_New_Invites_Indicator()
	ENDIF
	
	// If a repeat is required then setup the repeat timer - we use an active repeat timer to decide whether to re-display the invite or not, the 'repeat flag' will be false now
	IF (repeatRequired)
		g_sJLInvitesMP[paramInviteSlot].jliRepeatTimeout = GET_GAME_TIMER() + REPEAT_INVITE_DELAY_msec
	ENDIF
	
	// If a reminder, ensure app auto-launch works again
	IF (paramIsReminder)
		Allow_Joblist_App_Autolaunch()
	ENDIF
	
	// KGM 17/12/14 [BUG 2169672]: Versus Mission Invites should display a help message.
	Display_Heist_Versus_Help_Message_With_Invite(paramInviteSlot)
    
    // Display some debug output
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
		IF (paramIsReminder)
			NET_PRINT("[REMINDER] ")
		ENDIF
        NET_PRINT("Joblist Invite Sent To Feed:")
		NET_NL()
        IF (invitorIsPlayer)
            NET_PRINT("          Player       : ") NET_PRINT(GET_PLAYER_NAME(g_sJLInvitesMP[paramInviteSlot].jliPlayer)) NET_NL()
        ELSE
            TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLInvitesMP[paramInviteSlot].jliNPC)    
            NET_PRINT("          NPC          : ") NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL)) NET_NL()
        ENDIF
        NET_PRINT("          Headshot TXD : ") NET_PRINT(theTXD) NET_NL()
        NET_PRINT("          Type         : ") NET_PRINT(activityAsString)
        IF (isPlaylistInvite)
            NET_PRINT(" [Playlist Job ")
            NET_PRINT_INT(currentPlaylistJob)
            NET_PRINT(" of ")
            NET_PRINT_INT(totalPlaylistJobs)
            NET_PRINT("]")
        ENDIF
        NET_NL()
        IF (displayMissionName)
            NET_PRINT("          Mission Name : ") NET_PRINT(g_sJLInvitesMP[paramInviteSlot].jliMissionName) NET_NL()       
        ENDIF
        IF (displayDescription)
            NET_PRINT("          Description  : ") NET_PRINT(theDescription) NET_NL()       
        ENDIF
		IF (repeatRequired)
            NET_PRINT("          Repeat Invite: ") NET_PRINT_INT(REPEAT_INVITE_DELAY_msec) NET_PRINT(" msec") NET_NL()
		ENDIF
    #ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a Reminder Invite feed message is required
//
// INPUT PARAMS:		paramInviteSlot			The array position within the invite array
PROC Send_Reminder_Invite_Feed_Message_If_Required(INT paramInviteSlot)

	// A reminder is only required if the timeout timer is active
	IF (g_sJLInvitesMP[paramInviteSlot].jliRepeatTimeout = 0)
		EXIT
	ENDIF
	
	// A repeat is required, so check for timeout
	IF (GET_GAME_TIMER() < g_sJLInvitesMP[paramInviteSlot].jliRepeatTimeout)
		EXIT
	ENDIF
	
	// Timeout has expired, so reset the timer
	g_sJLInvitesMP[paramInviteSlot].jliRepeatTimeout = 0
	
	// Is a reminder allowed?
    // KGM 24/8/13: Don't send invites if post-mission cleanup is active
    IF (IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID()))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Ignoring Invite Feed Reminder - Post Mission Cleanup is still active")
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // KGM 24/8/13: Don't send invites during a transition to the betting screens, or a restart random transition
    IF (IS_TRANSITION_SESSION_RESTARTING())
    OR (HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Ignoring Invite Feed Reminder - Transition Session Restarting, or doing Restart Random")
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
	
	// KGM 9/6/14: BUG 1877275 - Feed message appeared during skycam transition back to Freemode.
	IF (Is_There_An_Active_Player_Warp_Delay())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Ignoring Invite Feed Reminder - Post-player warp delay still active")
            NET_NL()
        #ENDIF
        
        EXIT
	ENDIF
	
	// Ignore if the Joblist App is already active
	IF DOES_SCRIPT_EXIST("appMPJobListNEW")
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appMPJobListNEW")) > 0
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Ignoring Invite Feed Reminder - Player is already viewing Joblist")
            NET_NL()
        #ENDIF
        
        EXIT
	ENDIF
	
	// Ignore if the Invite isn't allowed to be displayed
	IF NOT (g_sJLInvitesMP[paramInviteSlot].jliAllowDisplay)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Ignoring Invite Feed Reminder - This Invite is not allowed to be displayed")
            NET_NL()
        #ENDIF
        
        EXIT
	ENDIF
	
	// Send the reminder
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Sending Invite Feed Reminder for slot: ")
		NET_PRINT_INT(paramInviteSlot)
		NET_PRINT(" [DISPLAY AT TOP AGAIN]")
        NET_NL()
    #ENDIF
	
	// ...bring back to the top of the list
	g_sJLInvitesMP[paramInviteSlot].jliOrderIndex = g_inviteOrderCounter
	g_inviteOrderCounter++
		
	// ...re-display on joblist
	BOOL isReminder = TRUE
	Display_Invite_As_Feed_Message(paramInviteSlot, isReminder)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the name to display on a Cross-Session Invite
//
// INPUT PARAMS:		paramArrayPos			The array position within the Cross-Session Invite array
// RETURN VALUE:		TEXT_LABEL_63			Either the GamerTag or (XboxOne only), the DisplayName
//
// NOTES:	XboxOne allows players to agree to see their real names instead of a gamer tag
FUNC TEXT_LABEL_63 Get_CSInvite_Name_For_Display(INT paramArrayPos)
	
	// For XboxOne, if there is a valid DisplayName, use it
	IF (IS_XBOX_PLATFORM())
		IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJLCSInvitesMP[paramArrayPos].jcsiInvitorDisplayName))
			// ...valid display name
			RETURN (g_sJLCSInvitesMP[paramArrayPos].jcsiInvitorDisplayName)
		ENDIF
	ENDIF
	
	// Return the GamerTag passed with the CS Invite
	TEXT_LABEL_63 gamerTagAsTL63 = g_sJLCSInvitesMP[paramArrayPos].jcsiInvitorGamerTag
	RETURN (gamerTagAsTL63)

ENDFUNC

#IF FEATURE_COPS_N_CROOKS
FUNC BOOL IS_THIS_CLOUD_FILE_NAME_ARCADE_MODE(STRING paramCloudFilename)
	IF ARE_STRINGS_EQUAL(paramCloudFilename, "HUD_LBD_CNC")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
#ENDIF

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display a Joblist Cross-Session Invite as a Text Message Feed Message
//
// INPUT PARAMS:        paramInviteSlot         The Array Position for this Joblist CSInvite
//						paramIsReminder			[DEFAULT=FALSE] TRUE if this is a reminder for a previously sent CSInvite, FALSE if not a reminder
PROC Display_CSInvite_As_Feed_Message(INT paramInviteSlot, BOOL paramIsReminder = FALSE)

	// Whatever happens, clear out the 'repeat' variables here - if there's a problem we don't want a repeat
	BOOL repeatRequired = g_sJLCSInvitesMP[paramInviteSlot].jcsiRepeatInvite
	
	g_sJLCSInvitesMP[paramInviteSlot].jcsiRepeatInvite	= FALSE
	g_sJLCSInvitesMP[paramInviteSlot].jcsiRepeatTimeout	= 0

	// Is functionality allowed?
    BOOL allowFunctionality = FALSE
    
    IF (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
    OR (GET_CURRENT_GAMEMODE() = GAMEMODE_SP)
    OR NOT (NETWORK_IS_GAME_IN_PROGRESS())
        allowFunctionality = TRUE
    ENDIF
    
    IF NOT (allowFunctionality)
        NET_PRINT("Display_CSInvite_As_Feed_Message(): This function is currently Freemode- and SP-specific. This is probably a timing issue - during a transition the gamemode temporarily swaps from being Freemode.") NET_NL()
        EXIT
    ENDIF
	
	// Don't display a feed message if the player is in SCTV
	IF (IS_PLAYER_SCTV(PLAYER_ID()))
        // Clear the FeedID back to 'none'
        g_sJLCSInvitesMP[paramInviteSlot].jcsiFeedID = -1
    
	    // Update the 'new invites' indicator
	    Increment_New_Invites_Indicator()
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Player is in SCTV. Not displaying CSInvite Feed Message. Slot: ")
            NET_PRINT_INT(paramInviteSlot)
            NET_NL()
        #ENDIF
        
        EXIT
	ENDIF
    
    // Don't display a feed entry if suppressed for some reason
    // (ie: a 'lose your wanted level' feed entry was sent instead)
    IF (g_sJLCSInvitesMP[paramInviteSlot].jcsiFeedID = INVITE_FEED_ENTRY_NOT_REQUIRED)
        // Clear the FeedID back to 'none'
        g_sJLCSInvitesMP[paramInviteSlot].jcsiFeedID = -1
	    
	    // Update the 'new invites' indicator
		IF NOT (paramIsReminder)
	    	Increment_New_Invites_Indicator()
		ENDIF
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Joblist CSInvite didn't need sent to feed (possibly had been replaced by 'lose your wanted level' feed message). Slot: ")
            NET_PRINT_INT(paramInviteSlot)
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
	
	// KGM 15/7/15 [BUG 2422920]: Non-Friend invite feed messages don't display during FM Events under certain conditions (the invite still appears in joblist)
	// For cross-session invites, Invitor can only be a player but may not be a friend
	IF (IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID()))
	OR (IS_PLAYER_PERMANENT_PARTICIPANT_TO_ANY_EVENT(PLAYER_ID()))
		// Player is critical or a permanent participant of an FM Event
		PRINTLN(".KGM [JobList]: Display_CSInvite_As_Feed_Message(): Player is Critical or Permanent on an FM Event")
			
		IF NOT (g_sJLCSInvitesMP[paramInviteSlot].jcsiInvitorIsFriend)
			// Invitor is not a friend, so should not display an Invite Feed Message
			PRINTLN(".KGM [JobList]: Display_CSInvite_As_Feed_Message(): ...Inviter is NOT a friend so should not display an Invite Feed Message due to FM Event participation rules")

			// Ensure the 'new invites' indicators get updated as appropriate
			IF NOT (paramIsReminder)
			    Set_New_Invite_Received()
			    Increment_New_Invites_Indicator()
			ENDIF
			
			EXIT
		ELSE
			PRINTLN(".KGM [JobList]: Display_CSInvite_As_Feed_Message(): ...Inviter is a friend so can display an Invite Feed Message")
		ENDIF
	ELSE
		PRINTLN(".KGM [JobList]: Display_CSInvite_As_Feed_Message(): Player is NOT Critical or Permanent on an FM Event so can display an Invite Feed Message")
	ENDIF
	
	//url:bugstar:7715975 - Gen9 - [R5304] The blocking user is able to view the blocked player's UGC Motorcycle Club emblem from the phone invite.
	GAMER_HANDLE tGH = GET_GAMER_HANDLE_PLAYER(g_sJLInvitesMP[paramInviteSlot].jliPlayer)
	IF IS_GAMERHANDLE_BLOCKED(tGH)
		PRINTLN(".KGM [JobList]: Display_CSInvite_As_Feed_Message(): ... IS_GAMERHANDLE_BLOCKED, so this should not display a feed message")
		EXIT
	ENDIF

    // Get the Headshot
    TEXT_LABEL_63   theTXD              = "CHAR_DEFAULT"
    BOOL            isPlaylistInvite    = FALSE
    BOOL            isChallangeInvite   = FALSE
	BOOL			isHeadToHeadInvite	= FALSE
	BOOL			isTournamentQualifyerInvite 	= FALSE
	INT				headToHeadWager		= -1

    IF (g_sJLCSInvitesMP[paramInviteSlot].jcsiPlaylistTotal > 0)
        isPlaylistInvite = TRUE
    ENDIF
    
    // Decide what needs to be displayed in the feed
    // NOTE: This has become a bit complicated
    TEXT_LABEL_15   textConfig          = "JL_INVITE"
    BOOL            displayMissionName  = FALSE
    BOOL            displayDescription  = FALSE
    TEXT_LABEL_63   activityType        = Get_FM_Joblist_Activity_TextLabel(g_sJLCSInvitesMP[paramInviteSlot].jcsiType, g_sJLCSInvitesMP[paramInviteSlot].jcsiSubtype, DEFAULT, g_sJLCSInvitesMP[paramInviteSlot].jlRootContentIdHash)
    TEXT_LABEL_63   activityAsString    = GET_FILENAME_FOR_AUDIO_CONVERSATION(activityType)	
	
    // Temp for CWP until I change over to using the new 500-char string method
    STRING          theDescription      = ""
	UGC_DESCRIPTION	splitDescriptions
	BOOL			foundDescription	= FALSE
    
    IF (g_sJLCSInvitesMP[paramInviteSlot].jcsiDescHash != 0)
        theDescription = UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(g_sJLCSInvitesMP[paramInviteSlot].jcsiDescHash, INVITE_MAX_DESCRIPTION_LENGTH)
		Split_Cached_Description_Into_TL63s(g_sJLCSInvitesMP[paramInviteSlot].jcsiDescHash, splitDescriptions)
        IF NOT (IS_STRING_NULL_OR_EMPTY(theDescription))
			foundDescription = TRUE
		ENDIF
	ELSE
		// KGM 16/3/15 [BUG 2275132]: Heist Prep details from the cache will have to also retrieve the cached description
		IF (g_sJLCSInvitesMP[paramInviteSlot].jcsiType = FMMC_TYPE_MISSION)
		AND (g_sJLCSInvitesMP[paramInviteSlot].jcsiSubtype = FMMC_MISSION_TYPE_PLANNING)
			// This is a Heist Prep, so the description may be cached along with the heist prep header details
			// NOTE: This is the new script cache setup by Bobby, not the Code Cache of the descriptions
			Copy_Description_From_Heist_Prep_Cache_Into_TL63s(g_sJLCSInvitesMP[paramInviteSlot].jcsiContentID, splitDescriptions)
			foundDescription = TRUE
		ENDIF
    ENDIF
    
    // Information about the jobs in a playlist
    INT currentPlaylistJob  = 0
    INT totalPlaylistJobs   = 0
    
    IF (isPlaylistInvite)
		// KGM 9/12/13: NOTE, a current playlist job position > CROSS_SESSION_TOUR_QUAL_OFFSET means a Tournament Qualifyer playlist!
		IF (g_sJLCSInvitesMP[paramInviteSlot].jcsiPlaylistCurrent >= CROSS_SESSION_TOUR_QUAL_OFFSET)
        	currentPlaylistJob  = g_sJLCSInvitesMP[paramInviteSlot].jcsiPlaylistCurrent + 1	- CROSS_SESSION_TOUR_QUAL_OFFSET
			isTournamentQualifyerInvite = TRUE
		// KGM 9/12/13: NOTE, a current playlist job position > CROSS_SESSION_H2H_WAGER_OFFSET means a H2H Playlist with a wager, the wager is currentplaylist value - CROSS_SESSION_H2H_WAGER_OFFSET
		ELIF (g_sJLCSInvitesMP[paramInviteSlot].jcsiPlaylistCurrent >= CROSS_SESSION_H2H_WAGER_OFFSET)
			isHeadToHeadInvite = TRUE
			headToHeadWager = g_sJLCSInvitesMP[paramInviteSlot].jcsiPlaylistCurrent - CROSS_SESSION_H2H_WAGER_OFFSET
		ELSE
        	currentPlaylistJob  = g_sJLCSInvitesMP[paramInviteSlot].jcsiPlaylistCurrent + 1
		ENDIF
        totalPlaylistJobs   = g_sJLCSInvitesMP[paramInviteSlot].jcsiPlaylistTotal
//      if IS_PLAYER_ON_A_CHALLENGE(g_sJLCSInvitesMP[paramInviteSlot].jliPlayer)
//      or IS_PLAYER_SETTING_CHALLENGE(g_sJLCSInvitesMP[paramInviteSlot].jliPlayer)
//      OR IS_PLAYER_ON_A_H2H_PLAYLIST(g_sJLCSInvitesMP[paramInviteSlot].jliPlayer)
//          isChallangeInvite = true
//      endif
    ENDIF
    
    
    // Should additional details be displayed other than the Mission Type?
    IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJLCSInvitesMP[paramInviteSlot].jcsiMissionName))
        // ...also display the mission name
        textConfig          = "JL_INVITE_N"
        displayMissionName  = TRUE
        
        // Should the Mission Description be displayed?
// KGM: Both of these methods are obsolete - need to swap to new 500-char desc method
//      IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJLCSInvitesMP[paramInviteSlot].jcsiDescription))
        IF (foundDescription)
            // ...also display the mission description
            textConfig          = "JL_INV_ND"
            displayDescription  = TRUE
            
            // Display 'playlist' or 'Rockstar' along with name and description?
            IF (isPlaylistInvite)
                if isChallangeInvite
                    textConfig = "JL_INVITE_CHND"
				ELIF (isHeadToHeadInvite)
                    textConfig = "JL_INVITE_H2HND"
				ELIF isTournamentQualifyerInvite
                    textConfig = "JL_INV_TQD"					
                else
                    textConfig = "JL_INV_PND"
                endif
            ENDIF
        ELSE
            // Display 'playlist' or 'Rockstar' along with name?
            IF (isPlaylistInvite)
                if isChallangeInvite
                    textConfig = "JL_INVITE_CHN"
				ELIF (isHeadToHeadInvite)
                    textConfig = "JL_INVITE_H2HN"					
				ELIF isTournamentQualifyerInvite
                    textConfig = "JL_INV_TQN"					
                else
                    textConfig = "JL_INVITE_PN"
                endif
            ENDIF
        ENDIF
    ELSE
        // Display 'playlist'
        IF (isPlaylistInvite)
            if isChallangeInvite
                textConfig = "JL_INVITE_CH"
			ELIF (isHeadToHeadInvite)
                textConfig = "JL_INVITE_H2H"
			ELIF isTournamentQualifyerInvite
                textConfig = "JL_INV_TQ"
            else
                textConfig = "JL_INVITE_P"
            endif
        ENDIF
    ENDIF
	
	// Gather the Invitor Name information and wrap it up in 'condensed font' markers
	TEXT_LABEL_63 invitorName = "<C>"
	TEXT_LABEL_15 inviterCTag
	TEXT_LABEL_63 displayName = Get_CSInvite_Name_For_Display(paramInviteSlot)
    invitorName += displayName
    invitorName += "</C>"
	/*
	GAMER_HANDLE tGH = GET_GAMER_HANDLE_PLAYER(g_sJLRVInvitesMP[paramInviteSlot].jrviPlayer)
    // TODO: CS Crew Tag display is not working. 
	INT inviteCID = GET_MP_PLAYER_CLAN_ID(tGH)
	INT localCID = GET_LOCAL_PLAYER_CLAN_ID()
	//NET_PRINT("[MMM][JobList] Inviter CS Gamerhandle") DEBUG_PRINT_GAMER_HANDLE(tGH) NET_NL()
	
	IF inviteCID = localCID
		PRINTLN("[MMM][JobList] Inviter and Local Player Crews are the same")
		int loadStage = 0
		PRINTLN("[MMM][JobList] Inviter Name : ", g_sJLCSInvitesMP[paramInviteSlot].jcsiInvitorGamerTag)
		PRINTLN("[MMM][JobList] CrossSession, Starting GET_CREW_TAG, inviterCTag before : ", inviterCTag)
		GET_PLAYER_CREW_TAG_FOR_SCALEFORM(GET_PLAYER_INDEX(), inviterCTag)
		PRINTLN("[MMM][JobList] GET_PLAYER_CREW_TAG_FOR_SCALEFORM, OUTPUT : ", inviterCTag )
	ELSE
		PRINTLN("[MMM][JobList] CS Invite : Crew Tags ", GET_MP_PLAYER_CLAN_ID(tGH), " and ", GET_LOCAL_PLAYER_CLAN_ID(), " do not match" )
	ENDIF
	*/
    // The Feed text Icon to use
    FEED_TEXT_ICON theInviteIcon = TEXT_ICON_INVITE
	FEED_TEXT_ICON theAltIcon	 = TEXT_ICON_BLANK
	INT iHash = GET_HASH_KEY(g_sJLCSInvitesMP[paramInviteSlot].jcsiContentID)
	Set_Invite_Icon(theInviteIcon, theAltIcon, 
					iHash, 
					g_sJLCSInvitesMP[paramInviteSlot].jcsiType, 
					g_sJLCSInvitesMP[paramInviteSlot].jcsiSubtype, 
					g_sJLCSInvitesMP[paramInviteSlot].jcsiIsPushBikeOnly, 
					g_sJLCSInvitesMP[paramInviteSlot].jlRootContentIdHash,
					g_sJLCSInvitesMP[paramInviteSlot].jlAdversaryModeType)
	
    // Display the Feed Entry and store the returned Feed ID
    Set_Feed_Flash_Style_For_Invites()
	Set_Feed_Background_Colour_For_Invite(g_sJLCSInvitesMP[paramInviteSlot].jcsiType, g_sJLCSInvitesMP[paramInviteSlot].jcsiSubtype, g_sJLCSInvitesMP[paramInviteSlot].jlRootContentIdHash, g_sJLCSInvitesMP[paramInviteSlot].jlAdversaryModeType)
	
    BEGIN_TEXT_COMMAND_THEFEED_POST(textConfig)
        // If a Playlist, display the number of jobs in the playlist
        IF (isPlaylistInvite)
			IF (isHeadToHeadInvite)
		        // If a Head To HEad, display the number of jobs and the wager
	            ADD_TEXT_COMPONENT_INTEGER(totalPlaylistJobs)
	            ADD_TEXT_COMPONENT_INTEGER(headToHeadWager)
			ELSE
	            ADD_TEXT_COMPONENT_INTEGER(currentPlaylistJob)
	            ADD_TEXT_COMPONENT_INTEGER(totalPlaylistJobs)
			ENDIF
        ENDIF
        
        // If required, display the mission name
        IF (displayMissionName)
            ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sJLCSInvitesMP[paramInviteSlot].jcsiMissionName)
            
            // If required, also display the mission description
            IF (displayDescription)
	            INT descLoop = 0
				REPEAT NUM_TEXTLABELS_IN_UGC_DESCRIPTION descLoop
					ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(splitDescriptions.TextLabel[descLoop])
				ENDREPEAT
            ENDIF
        ENDIF
		
		#IF FEATURE_COPS_N_CROOKS
		IF (!displayMissionName)
			IF IS_THIS_CLOUD_FILE_NAME_ARCADE_MODE(g_sJLCSInvitesMP[paramInviteSlot].jcsiContentID)
				activityAsString = GET_FILENAME_FOR_AUDIO_CONVERSATION(g_sJLCSInvitesMP[paramInviteSlot].jcsiContentID)
			ENDIF
		ENDIF
		#ENDIF
		
		IF NOT (isPlaylistInvite)
		AND NOT (displayMissionName)
			ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY("")
		ENDIF
		
		IF NOT (isPlaylistInvite)
			Append_Played_Status_To_Activity_String(g_sJLCSInvitesMP[paramInviteSlot].jcsiIsUnplayed, g_sJLCSInvitesMP[paramInviteSlot].jcsiCreatorID , activityAsString)
		ENDIF
		
    g_sJLCSInvitesMP[paramInviteSlot].jcsiFeedID = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT_WITH_CREW_TAG_AND_ADDITIONAL_ICON(theTXD, theTXD, TRUE, theInviteIcon, invitorName, activityAsString, 1, inviterCTag, theAltIcon)
	
    Clear_Feed_Flash_Style_For_Invites()
    Store_Feed_Message_As_Most_Recent_For_AutoLaunch(g_sJLCSInvitesMP[paramInviteSlot].jcsiFeedID)
    Play_Invite_Received_Sound_Effect()
	
	IF NOT (paramIsReminder)
	    Set_New_Invite_Received()
	    
	    // Update the 'new invites' indicator
	    Increment_New_Invites_Indicator()
	ENDIF
	
	// If a repeat is required then setup the repeat timer - we use an active repeat timer to decide whether to re-display the invite or not, the 'repeat flag' will be false now
	IF (repeatRequired)
		g_sJLCSInvitesMP[paramInviteSlot].jcsiRepeatTimeout	= GET_GAME_TIMER() + REPEAT_INVITE_DELAY_msec
	ENDIF
	
	// If a reminder, ensure app auto-launch works again
	IF (paramIsReminder)
		Allow_Joblist_App_Autolaunch()
	ENDIF

    // Display some debug output
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Cross-Session Invite Sent To Feed:") NET_NL()
        NET_PRINT("          Player       : ") NET_PRINT(g_sJLCSInvitesMP[paramInviteSlot].jcsiInvitorGamerTag) NET_NL()
		IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJLCSInvitesMP[paramInviteSlot].jcsiInvitorDisplayName))
        	NET_PRINT("          Display Name : ") NET_PRINT(g_sJLCSInvitesMP[paramInviteSlot].jcsiInvitorDisplayName) NET_NL()
		ENDIF
        NET_PRINT("          Headshot TXD : ") NET_PRINT(theTXD) NET_NL()
        NET_PRINT("          Type         : ") NET_PRINT(activityAsString)
        IF (isPlaylistInvite)
            NET_PRINT(" [Playlist Job ")
            NET_PRINT_INT(currentPlaylistJob)
            NET_PRINT(" of ")
            NET_PRINT_INT(totalPlaylistJobs)
            NET_PRINT("]")
        ENDIF
        NET_NL()
        IF (displayMissionName)
            NET_PRINT("          Mission Name : ") NET_PRINT(g_sJLCSInvitesMP[paramInviteSlot].jcsiMissionName) NET_NL()        
        ENDIF
        IF (displayDescription)
            NET_PRINT("          Description  : ") NET_PRINT(theDescription) NET_NL()        
        ENDIF
		IF (repeatRequired)
            NET_PRINT("          Repeat Invite: ") NET_PRINT_INT(REPEAT_INVITE_DELAY_msec) NET_PRINT(" msec") NET_NL()
		ENDIF
    #ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a Reminder Cross-session Invite feed message is required
//
// INPUT PARAMS:		paramCSInviteSlot		The array position within the cross-session invite array
PROC Send_Reminder_CSInvite_Feed_Message_If_Required(INT paramCSInviteSlot)

	// A reminder is only required if the timeout timer is active
	IF (g_sJLCSInvitesMP[paramCSInviteSlot].jcsiRepeatTimeout = 0)
		EXIT
	ENDIF
	
	// A repeat is required, so check for timeout
	IF (GET_GAME_TIMER() < g_sJLCSInvitesMP[paramCSInviteSlot].jcsiRepeatTimeout)
		EXIT
	ENDIF
	
	// Timeout has expired, so reset the timer
	g_sJLCSInvitesMP[paramCSInviteSlot].jcsiRepeatTimeout = 0
	
	// Is a reminder allowed?
    // KGM 24/8/13: Don't send invites if post-mission cleanup is active
    IF (IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID()))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Ignoring Cross-Session Invite Feed Reminder - Post Mission Cleanup is still active")
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // KGM 24/8/13: Don't send invites during a transition to the betting screens, or a restart random transition
    IF (IS_TRANSITION_SESSION_RESTARTING())
    OR (HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Ignoring Cross-Session Invite Feed Reminder - Transition Session Restarting, or doing Restart Random")
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
	
	// KGM 9/6/14: BUG 1877275 - Feed message appeared during skycam transition back to Freemode.
	IF (Is_There_An_Active_Player_Warp_Delay())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Ignoring Cross-Session Invite Feed Reminder - Post-player warp delay still active")
            NET_NL()
        #ENDIF
        
        EXIT
	ENDIF
	
	// Ignore if the Joblist App is already active
	IF DOES_SCRIPT_EXIST("appMPJobListNEW")
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appMPJobListNEW")) > 0
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Ignoring Cross-Session Invite Feed Reminder - Player is already viewing Joblist")
            NET_NL()
        #ENDIF
        
        EXIT
	ENDIF
	
	// Ignore if the Invite isn't allowed to be displayed
	IF NOT (g_sJLCSInvitesMP[paramCSInviteSlot].jcsiAllowDisplay)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Ignoring Cross-Session Invite Feed Reminder - This CSInvite is not allowed to be displayed")
            NET_NL()
        #ENDIF
        
        EXIT
	ENDIF
	
	// Send the reminder
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Sending Cross-Session Invite Feed Reminder for slot: ")
		NET_PRINT_INT(paramCSInviteSlot)
		NET_PRINT(" [DISPLAY AT TOP AGAIN]")
        NET_NL()
    #ENDIF
	
	// ...bring back to the top of the list
	g_sJLCSInvitesMP[paramCSInviteSlot].jcsiOrderIndex = g_inviteOrderCounter
	g_inviteOrderCounter++
		
	// ...re-display on joblist
	BOOL isReminder = TRUE
	Display_CSInvite_As_Feed_Message(paramCSInviteSlot, isReminder)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display a Joblist Basic Invite as a Text Message Feed Message
//
// INPUT PARAMS:        paramInviteSlot         The Array Position for this Joblist Basic Invite
PROC Display_RVInvite_As_Feed_Message(INT paramInviteSlot)

    IF (GET_CURRENT_GAMEMODE() != GAMEMODE_FM)
        NET_PRINT("Display_RVInvite_As_Feed_Message(): This function is currently Freemode-specific. This is probably a timing issue - during a transition the gamemode temporarily swaps from being Freemode.") NET_NL()
        EXIT
    ENDIF
	
	// Don't display a feed message if the player is in SCTV
	IF (IS_PLAYER_SCTV(PLAYER_ID()))
        // Clear the FeedID back to 'none'
        g_sJLRVInvitesMP[paramInviteSlot].jrviFeedID = -1
    
	    // Update the 'new invites' indicator
	    Increment_New_Invites_Indicator()
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Player is in SCTV. Not displaying RVInvite Feed Message. Slot: ")
            NET_PRINT_INT(paramInviteSlot)
            NET_NL()
        #ENDIF
        
        EXIT
	ENDIF
	
	// KGM 15/7/15 [BUG 2422920]: Non-Friend Basic Invite feed messages don't display during FM Events under certain conditions (the invite still appears in joblist)
	BOOL allowFmEventInviteFeedMessage = TRUE
	IF (IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID()))
	OR (IS_PLAYER_PERMANENT_PARTICIPANT_TO_ANY_EVENT(PLAYER_ID()))
		PRINTLN(".KGM [JobList]: Display_RVInvite_As_Feed_Message(): Player is Critical or Permanent on an FM Event")
		
		IF (g_sJLRVInvitesMP[paramInviteSlot].jrviInvitorIsPlayer)
			// Invitor is a player
			PRINTLN(".KGM [JobList]: Display_RVInvite_As_Feed_Message(): Player is Critical or Permanent on an FM Event")
			
			GAMER_HANDLE tGH = GET_GAMER_HANDLE_PLAYER(g_sJLRVInvitesMP[paramInviteSlot].jrviPlayer)
			IF NOT (NETWORK_IS_FRIEND(tGH))
				// Invitor is not a friend
				allowFmEventInviteFeedMessage = FALSE
				PRINTLN(".KGM [JobList]: Display_RVInvite_As_Feed_Message(): ...inviter is NOT a friend so an Invite Feed Message should not be displayed")
			ELSE
				PRINTLN(".KGM [JobList]: Display_RVInvite_As_Feed_Message(): ...inviter is a friend so an Invite Feed Message can be displayed")
			ENDIF
		ELSE
			allowFmEventInviteFeedMessage = FALSE
			PRINTLN(".KGM [JobList]: Display_RVInvite_As_Feed_Message(): ...inviter is NOT a player so an Invite Feed Message should not be displayed")
		ENDIF
	ELSE
		PRINTLN(".KGM [JobList]: Display_RVInvite_As_Feed_Message(): Player is NOT Critical or Permanent on an FM Event, so an Invite Feed Message can be displayed")
	ENDIF
	
	IF NOT (allowFmEventInviteFeedMessage)
		// Player is critical or a permanent participant of an FM Event, so don;t display this invite in the feed
		PRINTLN(".KGM [JobList]: Display_RVInvite_As_Feed_Message(): Basic Invite Feed message suppressed due to FM Event participation rules")
	
		// Ensure the 'new invites' indicators get updated as appropriate
	    Set_New_Invite_Received()
	    Increment_New_Invites_Indicator()
		
		EXIT
	ELSE
		PRINTLN(".KGM [JobList]: Display_RVInvite_As_Feed_Message(): Basic Invite Feed message is NOT suppressed due to FM Event participation rules")
	ENDIF

    // Get the Player Headshot
    BOOL            invitorIsPlayer     = g_sJLRVInvitesMP[paramInviteSlot].jrviInvitorIsPlayer
    PEDHEADSHOT_ID  theHeadshotID       = NULL
    
    IF (invitorIsPlayer)
        theHeadshotID = Get_HeadshotID_For_Player(g_sJLRVInvitesMP[paramInviteSlot].jrviPlayer)
    ENDIF
    
    TEXT_LABEL_63   theTXD
    
    IF (invitorIsPlayer)
        IF (theHeadshotID = NULL)
            theTXD = "CHAR_DEFAULT"
        ELSE
            theTXD = GET_PEDHEADSHOT_TXD_STRING(theHeadshotID)
        ENDIF
    ELSE
        theTXD = GET_FILENAME_FOR_AUDIO_CONVERSATION(Get_NPC_Headshot(g_sJLRVInvitesMP[paramInviteSlot].jrviNPC))
    ENDIF
    
    // Decide what needs to be displayed in the feed
    TEXT_LABEL_15   textConfig          = "JL_RVINVITE"
    BOOL            displayMissionName  = FALSE
    TEXT_LABEL_63   activityType        = Get_FM_Joblist_Activity_TextLabel(g_sJLRVInvitesMP[paramInviteSlot].jrviType, g_sJLRVInvitesMP[paramInviteSlot].jrviSubType,g_sJLRVInvitesMP[paramInviteSlot].jrviPropertyType, g_sJLRVInvitesMP[paramInviteSlot].jrviRootId)
    TEXT_LABEL_63   activityAsString    = GET_FILENAME_FOR_AUDIO_CONVERSATION(activityType)
	
    // Should additional details be displayed other than the Mission Type?
    IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJLRVInvitesMP[paramInviteSlot].jrviMissionName))
        // ...also display the mission name
        textConfig          = "JL_RVINVITE_N"
        displayMissionName  = TRUE
    ENDIF
    
    // Gather the Invitor Name information and wrap it up in 'condensed font' markers    
    TEXT_LABEL_63 invitorName
	TEXT_LABEL_15 inviterCTag
	
    IF (invitorIsPlayer)
        invitorName = "<C>"
        invitorName += GET_PLAYER_NAME(g_sJLRVInvitesMP[paramInviteSlot].jrviPlayer)
        invitorName += "</C>"
		
		GAMER_HANDLE tGH = GET_GAMER_HANDLE_PLAYER(g_sJLRVInvitesMP[paramInviteSlot].jrviPlayer)
		IF GET_MP_PLAYER_CLAN_ID(tGH) = GET_LOCAL_PLAYER_CLAN_ID()
			PRINTLN("[MMM][JobList] RV Invite : Crew Tags match")
			GET_PLAYER_CREW_TAG_FOR_SCALEFORM(g_sJLRVInvitesMP[paramInviteSlot].jrviPlayer, inviterCTag)
			PRINTLN("[MMM][JobList] GET_PLAYER_CREW_TAG_FOR_SCALEFORM, OUTPUT : ", inviterCTag )
		ELSE
			PRINTLN("[MMM][JobList] RV Invite : Crew Tags ", GET_MP_PLAYER_CLAN_ID(tGH), " and ", GET_LOCAL_PLAYER_CLAN_ID(), " do not match" )
		ENDIF
    ELSE
        TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLRVInvitesMP[paramInviteSlot].jrviNPC)
        invitorName += GET_FILENAME_FOR_AUDIO_CONVERSATION(npcNameTL)
    ENDIF    
    
    // The Feed text Icon to use
    FEED_TEXT_ICON theInviteIcon = TEXT_ICON_INVITE
		
    // Display the Feed Entry and store the returned Feed ID
    Set_Feed_Flash_Style_For_Invites()
	Set_Feed_Background_Colour_For_Invite(g_sJLRVInvitesMP[paramInviteSlot].jrviType, g_sJLRVInvitesMP[paramInviteSlot].jrviSubType, g_sJLRVInvitesMP[paramInviteSlot].jrviRootId)
	
    BEGIN_TEXT_COMMAND_THEFEED_POST(textConfig)
        // If required, display the mission name
        IF (displayMissionName)
            ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_sJLRVInvitesMP[paramInviteSlot].jrviMissionName)
        ELSE
            // Nothing to display
            ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY("")
        ENDIF
    g_sJLRVInvitesMP[paramInviteSlot].jrviFeedID = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT_WITH_CREW_TAG(theTXD, theTXD, TRUE, theInviteIcon, invitorName, activityAsString, 1, inviterCTag)
    Clear_Feed_Flash_Style_For_Invites()
    Store_Feed_Message_As_Most_Recent_For_AutoLaunch(g_sJLRVInvitesMP[paramInviteSlot].jrviFeedID)
    Play_Invite_Received_Sound_Effect()
    Set_New_Invite_Received()
    
    // Update the 'new invites' indicator
    Increment_New_Invites_Indicator()
    
    // Display some debug output
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Joblist Basic Invite Sent To Feed:") NET_NL()
        IF (invitorIsPlayer)
            NET_PRINT("          Player       : ") NET_PRINT(GET_PLAYER_NAME(g_sJLRVInvitesMP[paramInviteSlot].jrviPlayer)) NET_NL()
        ELSE
            TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLRVInvitesMP[paramInviteSlot].jrviNPC) 
            NET_PRINT("          NPC          : ") NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL)) NET_NL()
        ENDIF
        NET_PRINT("          Headshot TXD : ") NET_PRINT(theTXD) NET_NL()
        NET_PRINT("          Type         : ") NET_PRINT(activityAsString)
        NET_NL()
        IF (displayMissionName)
            NET_PRINT("          Mission Name : ") NET_PRINT(g_sJLRVInvitesMP[paramInviteSlot].jrviMissionName) NET_NL()        
        ENDIF
    #ENDIF

ENDPROC

// -----------------------------------------------------------------------------------------------------------

// ===========================================================================================================
//      JobList Invite Headshot Generation Functions
// ===========================================================================================================

// PURPOSE:  Maintain the Activation State and Headshot Generation for a Joblist Invite
//
// INPUT PARAMS:            paramInviteSlot     	The Joblist invitation array position
//							paramBlockedByFocus		TRUE if the invite is blocked only because of having a Focus Mission, otherwise FALSE
PROC Maintain_Invite_Activation_And_Headshot_Generation(INT paramInviteSlot, BOOL paramBlockedByFocus)

    PLAYER_INDEX        invitingPlayer  = g_sJLInvitesMP[paramInviteSlot].jliPlayer
    BOOL                invitorIsPlayer = g_sJLInvitesMP[paramInviteSlot].jliInvitorIsPlayer
    
    #IF IS_DEBUG_BUILD
        enumCharacterList   invitingNPC     = g_sJLInvitesMP[paramInviteSlot].jliNPC
    #ENDIF
    
    // Do nothing if the inviting player has left the game or is not currently alive
    IF (invitorIsPlayer)
        IF NOT (IS_NET_PLAYER_OK(invitingPlayer, TRUE))
            EXIT
        ENDIF
    ENDIF

    // Is the Joblist Invite already active
    IF (g_sJLInvitesMP[paramInviteSlot].jliActive)
        // ...already active, so maintain the headshot
        IF (invitorIsPlayer)
            Keep_Player_Headshot_Active(invitingPlayer)
        ENDIF
		
		// If the Invite is active, check if a reminder is required
		Send_Reminder_Invite_Feed_Message_If_Required(paramInviteSlot)
        
        EXIT
    ENDIF
    
    // This Joblist Invite isn't already active, so check if a headshot has been generated for this player
    PEDHEADSHOT_ID playerHeadshotID = NULL
    
    IF (invitorIsPlayer)
        playerHeadshotID = Get_HeadshotID_For_Player(invitingPlayer)
        IF (playerHeadshotID = NULL)
            // ...headshot is not ready, so check for headshot generation timeout (this is to prevent holding the Invite up for too long waiting on a headshot to be generated)
            IF (IS_TIME_LESS_THAN(GET_NETWORK_TIME(), g_sJLInvitesMP[paramInviteSlot].jliHeadshotTimeout))
                // ...still within timeout time, so keep waiting
                #IF IS_DEBUG_BUILD
                    NET_PRINT("...KGM MP [JobList]: ")
                    NET_PRINT_TIME()
                    NET_PRINT("Delaying Invite On Joblist - still waiting on headshot generation: ")
                    NET_PRINT(GET_PLAYER_NAME(invitingPlayer))
                    NET_PRINT(" [Current Time: ")
                    NET_PRINT(GET_TIME_AS_STRING(GET_NETWORK_TIME()))
                    NET_PRINT("] [Timeout: ")
                    NET_PRINT(GET_TIME_AS_STRING(g_sJLInvitesMP[paramInviteSlot].jliHeadshotTimeout))
                    NET_PRINT("]")
                    NET_NL()
                #ENDIF
                
                EXIT
            ENDIF
        ENDIF
    ENDIF
    
    // KGM 13/7/13: Contact Mission invites are re-sent quickly after being on mission but they appear during the blurry screen, so delay until everything is good to go again
    IF (IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID()))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Delaying Invite display on Feed - Post Mission Cleanup is still active")
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // KGM 24/8/13: Don't send invites during a transition to the betting screens, or a restart random transition
    IF (IS_TRANSITION_SESSION_RESTARTING())
    OR (HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Delaying Invite display on Feed - Transition Session Restarting, or doing Restart Random")
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
	
	// KGM 7/11/14: BUG 2108567 - Feed message appeared during skycam into Freemode.
	IF (Is_There_An_Active_Player_Warp_Delay())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Ignoring Invite - Post-player warp delay still active")
            NET_NL()
        #ENDIF
        
        EXIT
	ENDIF
     
    // Don't send feed message after all other tests have been performed if the invite can't currently be displayed
    IF NOT (g_sJLInvitesMP[paramInviteSlot].jliAllowDisplay)
		// KGM 4/12/14 [BUG 2135305]: If the invite is only blocked because the player has a Focus Mission and the player is in a corona, then allow the help message and invite indicator to display
		// KGM 19/1/15 [BUG 2199086]: Amendment to the above rule - don't display if performing a planning board tutorial (which is a series of help messages that would get interrupted by this one)
		IF (paramBlockedByFocus)
		AND (IS_PLAYER_IN_CORONA())
			IF NOT (g_forceDisplayPlayerInvitesIndicator)
				// NOTE: This checks for the prep of finale Board doing a tutorial sequence
				IF NOT (GET_HEIST_HELP_SEQUENCE_IN_PROGRESS())
				AND NOT (IS_HELP_MESSAGE_BEING_DISPLAYED())
					#IF IS_DEBUG_BUILD
			            NET_PRINT("...KGM MP [JobList]: ")
			            NET_PRINT_TIME()
			            NET_PRINT("Invite Blocked only by Focus Mission and Player in Corona.")
			            NET_NL()
			            NET_PRINT("...KGM MP [JobList]: Set g_forceDisplayPlayerInvitesIndicator = TRUE")
			            NET_NL()
			            NET_PRINT("...KGM MP [JobList]: Set g_displayCoronaInviteHelp = TRUE")
			            NET_NL()
					#ENDIF
					
					//PRINT_HELP("CORONA_INVITES")
					g_forceDisplayPlayerInvitesIndicator	= TRUE
					g_displayCoronaInviteHelp				= TRUE
				ELSE
					#IF IS_DEBUG_BUILD
			            NET_PRINT("...KGM MP [JobList]: ")
			            NET_PRINT_TIME()
			            NET_PRINT("Invite received while Player in Corona ")
						IF (GET_HEIST_HELP_SEQUENCE_IN_PROGRESS())
							NET_PRINT("BUT planning board help tutorial active. Delay displaying invite indicator.")
						ELSE
							NET_PRINT("BUT help text is on display. Delay displaying invite indicator.")
						ENDIF
						NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		
        EXIT
    ENDIF
   
    // Activate the Invite
    g_sJLInvitesMP[paramInviteSlot].jliActive   = TRUE
    
    // Don't send feed message if this Invite is a duplicate
    IF (Is_Invitation_In_Slot_A_Duplicate(paramInviteSlot))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Displaying Invite In Feed and On Joblist: ")
            NET_PRINT_INT(paramInviteSlot)
            NET_PRINT(" - ")
            NET_PRINT(GET_STRING_FROM_TEXT_FILE(Get_FM_Joblist_Activity_TextLabel(g_sJLInvitesMP[paramInviteSlot].jliType, g_sJLInvitesMP[paramInviteSlot].jliSubtype,g_sJLRVInvitesMP[paramInviteSlot].jrviPropertyType, g_sJLRVInvitesMP[paramInviteSlot].jrviRootId)))
            IF (invitorIsPlayer)
                NET_PRINT("   [From Player: ")
                NET_PRINT(GET_PLAYER_NAME(invitingPlayer))
            ELSE
                NET_PRINT("   [From NPC: ")
                TEXT_LABEL  npcNameTL = Get_NPC_Name(invitingNPC)
                NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            ENDIF
            NET_PRINT("] - INVITE IS A DUPLICATE SO FEED MESSAGE NOT SENT")
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // Display this Invite in the feed
    Display_Invite_As_Feed_Message(paramInviteSlot)
    
    // Make sure the Joblist App auto-launches when the cellphone is next taken out
    Allow_Joblist_App_Autolaunch()
    
    // Restart the 'Recent Invite' timer when the player is made aware of it
    Update_Recent_Invite_Received_Timer()
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Displaying Invite In Feed and On Joblist: ")
        NET_PRINT_INT(paramInviteSlot)
        NET_PRINT(" - ")
        NET_PRINT(GET_STRING_FROM_TEXT_FILE(Get_FM_Joblist_Activity_TextLabel(g_sJLInvitesMP[paramInviteSlot].jliType, g_sJLInvitesMP[paramInviteSlot].jliSubtype,g_sJLRVInvitesMP[paramInviteSlot].jrviPropertyType, g_sJLRVInvitesMP[paramInviteSlot].jrviRootId)))
        IF (invitorIsPlayer)
            NET_PRINT("   [From Player: ")
            NET_PRINT(GET_PLAYER_NAME(invitingPlayer))
        ELSE
            TEXT_LABEL  npcNameTL = Get_NPC_Name(invitingNPC)
            NET_PRINT("   [From NPC: ")
            NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
        ENDIF
        NET_PRINT("]")
        IF (invitorIsPlayer)
            IF (playerHeadshotID = NULL)
                NET_PRINT(" - FAILED TO GENERATE HEADSHOT BEFORE TIMEOUT")
            ENDIF
        ENDIF
        NET_PRINT(" [Feed ID: ")
        NET_PRINT_INT(g_sJLInvitesMP[paramInviteSlot].jliFeedID)
        NET_PRINT("]")
        NET_NL()
    #ENDIF
    
ENDPROC




// ===========================================================================================================
//      JobList Invite Storage functions
//      Invitations are stored on an array and as many are added to the joblist as there is room for
//      The newest entries are always at the start of the array and there should be one entry max per player
// ===========================================================================================================

// PURPOSE: Find the invitation slot containing an Invitation from a specific player
//
// INPUT PARAMS:        paramPlayerID           The PlayerID being searched for
// RETURN VALUE:        INT                     The Slot containing this player's invitation, or NO_JOBLIST_ARRAY_ENTRY if none
FUNC INT Get_Joblist_Invitation_Slot_For_Player(PLAYER_INDEX paramPlayerID)

    INT tempLoop = 0
    REPEAT g_numMPJobListInvitations tempLoop
        IF (g_sJLInvitesMP[tempLoop].jliInvitorIsPlayer)
            IF (g_sJLInvitesMP[tempLoop].jliPlayer = paramPlayerID)
                // Found an invitation by the selected player
                RETURN tempLoop
            ENDIF
        ENDIF
    ENDREPEAT
    
    // No invitation by selected player
    RETURN NO_JOBLIST_ARRAY_ENTRY

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Find the invitation slot containing an Invitation from a specific In-Game Character
//
// INPUT PARAMS:        paramCharacterID        The Character Enum being searched for
//                      paramMissionname        The Mission Name
//                      paramMatchMission       [DEFAULT = FALSE] TRUE if the mission names need to match (so specific mission for contact), FALSE if only the contact needs to match (so any mission for contact)
// RETURN VALUE:        INT                     The Slot containing this character's invitation, or NO_JOBLIST_ARRAY_ENTRY if none
FUNC INT Get_Joblist_Invitation_Slot_For_NPC(enumCharacterList paramCharacterID, STRING paramMissionName, BOOL paramMatchMission = FALSE)

    INT tempLoop = 0
    REPEAT g_numMPJobListInvitations tempLoop
        IF NOT (g_sJLInvitesMP[tempLoop].jliInvitorIsPlayer)
            IF (g_sJLInvitesMP[tempLoop].jliNPC = paramCharacterID)
                // Found an invitation by the selected in-game character
                // ...if the mission name is important then ensure it matches
                IF NOT (paramMatchMission)
                    // ...matching mission isn't important, so treat as a match
                    RETURN tempLoop
                ENDIF
                
                IF (IS_STRING_NULL_OR_EMPTY(paramMissionName))
                    // ...mission name isn't stored, so treat as a match
                    RETURN tempLoop
                ENDIF
                
                // Mission Name is important
                IF (ARE_STRINGS_EQUAL(paramMissionName, g_sJLInvitesMP[tempLoop].jliMissionName))
                    // ...the mission names match
                    RETURN tempLoop
                ENDIF
            ENDIF
        ENDIF
    ENDREPEAT
    
    // No invitation by selected in-game character
    RETURN NO_JOBLIST_ARRAY_ENTRY

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Delete the entry in the specified invitation slot
//
// INPUT PARAMS:        paramSlot               The slot within the Invitation Array that needs to be deleted - all entries after it in the array will be shuffled up
//						paramAllowMarkAsSpam	[DEFAULT = TRUE] TRUE if the invite, on delete, should get marked as SPAM to avoid it repeating if the player deletes it, FALSE means don't mark as SPAM
//
// NOTES:	KGM 24/11/14 [BUG 2121954]: Added the conditional 'mark as spam' so that an old invite replaced by an identical newer invite doesn't mark the invite details as SPAM when the old version calls this function.
PROC Delete_Joblist_Invitation_In_Invitation_Slot(INT paramSlot, BOOL paramAllowMarkAsSpam = TRUE)

    // Ensure the passed-in slot is valid
    IF (paramSlot >= g_numMPJobListInvitations)
        // ...slot isn't valid
        #IF IS_DEBUG_BUILD
            NET_PRINT(".KGM [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Invitation Entry Slot Invalid. Slot to be deleted: ")
            NET_PRINT_INT(paramSlot)
            NET_PRINT(" [Current Slots In Use: ")
            NET_PRINT_INT(g_numMPJobListInvitations)
            NET_PRINT("]")
            NET_NL()
        #ENDIF

        EXIT
    ENDIF
	
	// Only mark the invite as spam if it is not closed
	// KGM 24/11/14 [BUG 2121954]: Added the conditional check
	IF (paramAllowMarkAsSpam)
		IF (Is_Invitation_In_Slot_Allowed_To_Be_Displayed(paramSlot))
			// Check for a free slot on the spamJoblist
			INT i = 0
		   	FOR i = 0 TO MAX_MP_JOBLIST_ENTRIES - 1 			
				IF g_sJLInvitesMP[paramSlot].jliInvitorIsPlayer
					IF GET_GAME_TIMER() > g_spamJobList[i].Timer OR g_spamJobList[i].Timer = 0
						IF g_sJLInvitesMP[paramSlot].jliAllowDisplay
							g_spamJobList[i].jlInvitorIndex		= g_sJLInvitesMP[paramSlot].jliPlayer
							g_spamJobList[i].jliContentIDHash	= GET_HASH_KEY(g_sJLInvitesMP[paramSlot].jliContentID)
							g_spamJobList[i].Timer				= GET_GAME_TIMER() + 30000	
							i = MAX_MP_JOBLIST_ENTRIES
        
					        #IF IS_DEBUG_BUILD
					            NET_PRINT(".KGM [JobList]: ")
					            NET_PRINT_TIME()
					            NET_PRINT("Adding Deleted Invite to SPAM list.")
					            NET_NL()
					        #ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ELSE
        #IF IS_DEBUG_BUILD
            NET_PRINT(".KGM [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Not adding deleted Invite to SPAM list. paramAllowMarkAsSpam = FALSE.")
            NET_NL()
        #ENDIF
	ENDIF
	
    // Slot is valid, so delete the feed entry if it exists
    IF (g_sJLInvitesMP[paramSlot].jliFeedID != NO_JOBLIST_INVITE_FEED_ID)
        THEFEED_REMOVE_ITEM(g_sJLInvitesMP[paramSlot].jliFeedID)
        
        #IF IS_DEBUG_BUILD
            NET_PRINT(".KGM [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Requested removal of Feed Entry if on display: ")
            NET_PRINT_INT(g_sJLInvitesMP[paramSlot].jliFeedID)
            NET_NL()
        #ENDIF
    ENDIF
    
    // Clean up the 500-char description details for the slot they exist
    IF NOT (g_sJLInvitesMP[paramSlot].jliRequireDesc)
        IF (g_sJLInvitesMP[paramSlot].jliDescHash != 0)
            UGC_RELEASE_CACHED_DESCRIPTION(g_sJLInvitesMP[paramSlot].jliDescHash)
            
            #IF IS_DEBUG_BUILD
                NET_PRINT(".KGM [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("Requested removal of 500-char description with this hash: ")
                NET_PRINT_INT(g_sJLInvitesMP[paramSlot].jliDescHash)
                NET_NL()
            #ENDIF
        ENDIF
    ENDIF
   
    // Move all entries after this in the array down one place to cover it
    INT toSlot = paramSlot
    INT fromSlot = toSlot + 1
    
    WHILE (fromSlot < g_numMPJobListInvitations)
        g_sJLInvitesMP[toSlot] = g_sJLInvitesMP[fromSlot]
        toSlot++
        fromSlot++
    ENDWHILE
    
    // Need to clear the last slot that used to contain valid data
    Clear_One_Invite_Array_Slot(toSlot)
    
    // Need to reduce the number of entries in the array
    g_numMPJobListInvitations--
    
    #IF IS_DEBUG_BUILD
        NET_PRINT(".KGM [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Invitation Entry Deleted In Slot: ")
        NET_PRINT_INT(paramSlot)
        NET_NL()
    #ENDIF
	
	// KGM 27/3/14: If the 'new invite' flag is set and the 'num new invites' indicator is greater than 0 then clear the 'new invite' flag
	//				This isn't entirely accurate but that flag is only needed at the start of the game for a help message from DaveW, so
	//				it's better for the help to be delayed temporarily rather than, as can happen just now, it gets displayed but the
	//				invite has already been deleted
	IF (g_joblistInviteReceived)
		IF (g_numMPJobListEntries > 0)
			PRINTLN(".KGM [Joblist]: Clearing 'Joblist Invite Received' flag because an Invite Entry has been deleted")
			Clear_New_Invite_Received()
		ENDIF
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain the downloading of any Invitations
PROC Maintain_Joblist_Invitations_Download()

	// Nothing to do for Invitations if a CSInvite is downloading a mission header
	IF (g_csInviteBeingDownloaded)
		EXIT
	ENDIF

    // Nothing to do if there are no Invitations, but clear the Invitation being downloaded flag because there can't be one being downloaded
    IF (g_numMPJobListInvitations = 0)
		g_invitationBeingDownloaded = FALSE
        EXIT
    ENDIF
	
	// Some debug output
	#IF IS_DEBUG_BUILD
		IF (g_invitationBeingDownloaded)
			NET_PRINT(".KGM [JobList]: Invitation Header Download flag is TRUE") NET_NL()
		ENDIF
	#ENDIF

    // Gather Details
    INT beingDownloadedSlot = ILLEGAL_ARRAY_POSITION
    INT nextDownloadSlot    = ILLEGAL_ARRAY_POSITION
    INT tempLoop            = 0
    
    REPEAT g_numMPJobListInvitations tempLoop
        // ...being downloaded?
        IF (beingDownloadedSlot = ILLEGAL_ARRAY_POSITION)
            IF (g_sJLInvitesMP[tempLoop].jliDownloading)
                beingDownloadedSlot = tempLoop
            ENDIF
        ENDIF
        
        // ...to be downloaded?
        IF (nextDownloadSlot = ILLEGAL_ARRAY_POSITION)
            IF (g_sJLInvitesMP[tempLoop].jliToDownload)
                nextDownloadSlot = tempLoop
            ENDIF
        ENDIF
    ENDREPEAT
	
	// Maintain the 'invitation being downloaded' flag based on whether or not a mission download is set for an invitation
	IF (beingDownloadedSlot = ILLEGAL_ARRAY_POSITION)
		// ...no invitation mission download
		g_invitationBeingDownloaded = FALSE
	ELSE
		// ...invitation mission being downloaded
		#IF IS_DEBUG_BUILD
			IF NOT (g_invitationBeingDownloaded)
				NET_PRINT(".KGM [JobList]: New Invitation Header Download Starting: Array Position ") NET_PRINT_INT(beingDownloadedSlot) NET_NL()
			ENDIF
		#ENDIF
		
		g_invitationBeingDownloaded = TRUE
	ENDIF
    
    // Is there mission header data being downloaded?
    IF (beingDownloadedSlot != ILLEGAL_ARRAY_POSITION)
        // Load the Header Data from the cloud
        // Safety Check - relaunch the mission loading script if it isn't launched (this should never be the case)
        IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("net_cloud_mission_loader")) = 0)
            REQUEST_SCRIPT("net_cloud_mission_loader")
            IF (HAS_SCRIPT_LOADED("net_cloud_mission_loader"))
                START_NEW_SCRIPT("net_cloud_mission_loader", FRIEND_STACK_SIZE)
                SET_SCRIPT_AS_NO_LONGER_NEEDED("net_cloud_mission_loader")
            ELSE
                #IF IS_DEBUG_BUILD
                    NET_PRINT(".KGM [JobList]: Invitation Header Download - Waiting for script to launch: net_cloud_mission_loader") NET_NL()
                #ENDIF
                    
                EXIT
            ENDIF
        ENDIF
        
        // Load the header data from the cloud
        IF NOT (Load_Cloud_Loaded_Header_Data(g_sJLInvitesMP[beingDownloadedSlot].jliContentID))
            #IF IS_DEBUG_BUILD
                NET_PRINT(".KGM [JobList]: Invitation: Still waiting for the Cloud-Loaded Header Data to be available: ")
				NET_PRINT(g_sJLInvitesMP[beingDownloadedSlot].jliContentID)
				NET_NL()
            #ENDIF
                
            EXIT
        ENDIF
        
        // Cloud-Load complete
		// Always, set the 'invitation being downloaded' flag to false to open this up to another invite
		g_invitationBeingDownloaded = FALSE
		
        // Check if it downloaded successfully
        IF NOT (Did_Cloud_Loaded_Header_Data_Load_Successfully())
            // ...download failed, so delete this entry and don't do any more array processing this frame in this function
            Delete_Joblist_Invitation_In_Invitation_Slot(beingDownloadedSlot)
            
            #IF IS_DEBUG_BUILD
                NET_PRINT(".KGM [JobList]: Download failed. Removing Invitation.") NET_NL()
            #ENDIF
            
            EXIT
        ENDIF
        
        // Download successful, so update the appropriate variables, treat as player's own UGC
        g_sJLInvitesMP[beingDownloadedSlot].jliToDownload		= FALSE
        g_sJLInvitesMP[beingDownloadedSlot].jliDownloading		= FALSE
        g_sJLInvitesMP[beingDownloadedSlot].jliDownloaded		= TRUE
        g_sJLInvitesMP[beingDownloadedSlot].jliCreatorID		= NATIVE_TO_INT(PLAYER_ID())
        g_sJLInvitesMP[beingDownloadedSlot].jliType				= g_sCloudMissionLoaderMP.cmlReturnType
		g_sJLInvitesMP[beingDownloadedSlot].jliSubtype			= g_sCloudMissionLoaderMP.cmlReturnSubtype
        g_sJLInvitesMP[beingDownloadedSlot].jliMissionName		= g_sCloudMissionLoaderMP.cmlReturnMissionName
		g_sJLInvitesMP[beingDownloadedSlot].jliDescHash			= g_cloudLoadedMissionHeader.iMissionDecHash
		g_sJLInvitesMP[beingDownloadedSlot].jliMinPlayers		= g_cloudLoadedMissionHeader.iMinPlayers
		g_sJLInvitesMP[beingDownloadedSlot].jliMaxPlayers		= g_cloudLoadedMissionHeader.iMaxPlayers
		
		// KGM 4/11/14 [BUG 2111097]: Storing this description is probably obsolete anyway, but don't store it for NPCs if teh description is already not NULL.
		// (I'm using this field now to allow NPCs to pass specific description text (for first invite to a Heist Versus mission)
		// KGM 14/12/14 [BUG 2157535]: This field is obsolete so I'm now using it to indicate a Heist Cutscene Umbrella description should be displayed, so don't fill it here
//		IF (g_sJLInvitesMP[beingDownloadedSlot].jliInvitorIsPlayer)
//        	g_sJLInvitesMP[beingDownloadedSlot].jliDescription		= g_sCloudMissionLoaderMP.cmlReturnDescription
//		ENDIF
        
        #IF IS_DEBUG_BUILD
            NET_PRINT(".KGM [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Header Downloaded. Updating Invitation Details At Array Location: ")
            NET_PRINT_INT(beingDownloadedSlot)
            NET_NL()
            NET_PRINT("      MissionName: ")
            NET_PRINT(g_sJLInvitesMP[beingDownloadedSlot].jliMissionName)
            NET_PRINT("  [")
            NET_PRINT(g_sJLInvitesMP[beingDownloadedSlot].jliDescription)
			IF NOT (g_sJLInvitesMP[beingDownloadedSlot].jliInvitorIsPlayer)
				NET_PRINT(" - not updated for NPC Invites")
			ENDIF
            NET_PRINT("]")
            NET_NL()
            NET_PRINT("      InvitorName: ")
            IF (g_sJLInvitesMP[beingDownloadedSlot].jliInvitorIsPlayer)
                IF g_sJLInvitesMP[beingDownloadedSlot].jliPlayer != INVALID_PLAYER_INDEX() 
				AND (IS_NET_PLAYER_OK(g_sJLInvitesMP[beingDownloadedSlot].jliPlayer, FALSE))
                    NET_PRINT(GET_PLAYER_NAME(g_sJLInvitesMP[beingDownloadedSlot].jliPlayer))
                ELSE
                    NET_PRINT("UNKNOWN PLAYER")
                ENDIF
            ELSE
                TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLInvitesMP[beingDownloadedSlot].jliNPC)
                NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            ENDIF
            IF (Is_There_A_MissionsAtCoords_Focus_Mission())
                NET_PRINT(" [PLAYER HAS A FOCUS MISSION]")
            ELSE
                NET_PRINT(" [player has no focus mission]")
            ENDIF
// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
//            IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
//                NET_PRINT(" [HAS WANTED LEVEL]")
//            ELSE
//                NET_PRINT(" [no wanted level]")
//            ENDIF
            NET_NL()
        #ENDIF
    ENDIF
    
    // Is there mission header data waiting to be downloaded?
	// KGM 6/3/14: BUG 1706109 - Don't start a new download if the player is reserved or active on any mission
	// KGM 31/1/15 [BUG 2220109]: Don;t start a download if the player is Quick Restarting or doing a Rounds mission
    IF (nextDownloadSlot != ILLEGAL_ARRAY_POSITION)
		IF NOT (Is_Player_Allocated_For_Any_Mission(PLAYER_ID()))
			IF (GET_TRANSITION_SESSION_VOTE_STATUS() != ciFMMC_EOM_VOTE_STATUS_ROUNDS_MISSION)
			AND (GET_TRANSITION_SESSION_VOTE_STATUS() != ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART)
			AND (GET_TRANSITION_SESSION_VOTE_STATUS() != ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP)
		        g_sJLInvitesMP[nextDownloadSlot].jliToDownload       = FALSE
		        g_sJLInvitesMP[nextDownloadSlot].jliDownloading      = TRUE
		        
		        #IF IS_DEBUG_BUILD
		            NET_PRINT(".KGM [JobList]: ")
		            NET_PRINT_TIME()
		            NET_PRINT("Invitation Details Setting Up Download At Array Location: ")
		            NET_PRINT_INT(nextDownloadSlot)
		            NET_NL()
		        #ENDIF
				
				// KGM 16/3/15 [BUG 2279487]: Use the cached data for Heist Prep Mission downloads if available
				IF (g_sJLInvitesMP[nextDownloadSlot].jliType = FMMC_TYPE_MISSION)
					// The cache contains heist planning mission header details and this is a heist planning invite, so check the cache
					PRINTLN(".KGM [JobList]: The next download slot is for a Mission - check for cached Heist Prep data. ContentID: ", g_sJLInvitesMP[nextDownloadSlot].jliContentID)
					INT cachedArrayPos = Find_Cached_Mission_Header_Data(g_sJLInvitesMP[nextDownloadSlot].jliContentID)
					IF (cachedArrayPos != ILLEGAL_ARRAY_POSITION)
						PRINTLN(".KGM [JobList]: Invitation Details Header Data found in cache. Invite Slot: ", nextDownloadSlot)
						
						// Store the variables
				        g_sJLInvitesMP[nextDownloadSlot].jliToDownload		= FALSE
				        g_sJLInvitesMP[nextDownloadSlot].jliDownloading		= FALSE
				        g_sJLInvitesMP[nextDownloadSlot].jliDownloaded		= TRUE
				        g_sJLInvitesMP[nextDownloadSlot].jliCreatorID		= NATIVE_TO_INT(PLAYER_ID())
				        g_sJLInvitesMP[nextDownloadSlot].jliType			= g_FMMC_PLANNING_MISSION.sMissionHeaderVars[cachedArrayPos].iType
						g_sJLInvitesMP[nextDownloadSlot].jliSubtype			= g_FMMC_PLANNING_MISSION.sMissionHeaderVars[cachedArrayPos].iSubType
				        g_sJLInvitesMP[nextDownloadSlot].jliMissionName		= g_FMMC_PLANNING_MISSION.sMissionHeaderVars[cachedArrayPos].tlMissionName
						g_sJLInvitesMP[nextDownloadSlot].jliDescHash		= 0	// For now - will need to handle the description
						g_sJLInvitesMP[nextDownloadSlot].jliMinPlayers		= g_FMMC_PLANNING_MISSION.sMissionHeaderVars[cachedArrayPos].iMinPlayers
						g_sJLInvitesMP[nextDownloadSlot].jliMaxPlayers		= g_FMMC_PLANNING_MISSION.sMissionHeaderVars[cachedArrayPos].iMaxPlayers
						
				        #IF IS_DEBUG_BUILD
				            NET_PRINT(".KGM [JobList]: ")
				            NET_PRINT_TIME()
				            NET_PRINT("Header From Cache. Updating Invitation Details At Array Location: ")
				            NET_PRINT_INT(nextDownloadSlot)
				            NET_NL()
				            NET_PRINT("      MissionName: ")
				            NET_PRINT(g_sJLInvitesMP[nextDownloadSlot].jliMissionName)
				            NET_PRINT("  [")
				            NET_PRINT(g_sJLInvitesMP[nextDownloadSlot].jliDescription)
							IF NOT (g_sJLInvitesMP[nextDownloadSlot].jliInvitorIsPlayer)
								NET_PRINT(" - not updated for NPC Invites")
							ENDIF
				            NET_PRINT("]")
				            NET_NL()
				            NET_PRINT("      InvitorName: ")
				            IF (g_sJLInvitesMP[nextDownloadSlot].jliInvitorIsPlayer)
				                IF g_sJLInvitesMP[nextDownloadSlot].jliPlayer != INVALID_PLAYER_INDEX() 
								AND (IS_NET_PLAYER_OK(g_sJLInvitesMP[nextDownloadSlot].jliPlayer, FALSE))
				                    NET_PRINT(GET_PLAYER_NAME(g_sJLInvitesMP[nextDownloadSlot].jliPlayer))
				                ELSE
				                    NET_PRINT("UNKNOWN PLAYER")
				                ENDIF
				            ELSE
				                TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLInvitesMP[nextDownloadSlot].jliNPC)
				                NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
				            ENDIF
				            IF (Is_There_A_MissionsAtCoords_Focus_Mission())
				                NET_PRINT(" [PLAYER HAS A FOCUS MISSION]")
				            ELSE
				                NET_PRINT(" [player has no focus mission]")
				            ENDIF
				            NET_NL()
				        #ENDIF
					ENDIF
				ENDIF
			ELSE
		        #IF IS_DEBUG_BUILD
		            NET_PRINT(".KGM [JobList]: ")
		            NET_PRINT_TIME()
		            NET_PRINT("Invitation Details Download At Array Location: ")
		            NET_PRINT_INT(nextDownloadSlot)
					NET_PRINT(" - DELAY, DOING ROUNDS OR QUICK RESTART")
		            NET_NL()
		        #ENDIF
			ENDIF
		ELSE
	        #IF IS_DEBUG_BUILD
	            NET_PRINT(".KGM [JobList]: ")
	            NET_PRINT_TIME()
	            NET_PRINT("Invitation Details Download At Array Location: ")
	            NET_PRINT_INT(nextDownloadSlot)
				NET_PRINT(" - DELAY, PLAYER RESERVED OR ACTIVE ON A MISSION")
	            NET_NL()
	        #ENDIF
		ENDIF
    ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check the validity of the invitations on the Joblist Invitations list, clean up if any are no longer valid, and maintain the headshots of those that are still valid
// NOTES:   This will be scheduled so it will only check one per frame
PROC Maintain_Joblist_Invitations()
    
    // Perform any every frame maintenance
    Maintain_Joblist_Invitations_Download()

    // Are there any invitations?
    IF (g_numMPJobListInvitations = 0)
        // ...no
        EXIT
    ENDIF
    
    // There are invitations, so maintain one each frame
    IF (g_nextJoblistInvitationCheckSlot >= g_numMPJobListInvitations)
        // ...wrap
        g_nextJoblistInvitationCheckSlot = 0
    ENDIF
    
    // Check the invitation at the 'next check' array position
    INT         checkPosition       = g_nextJoblistInvitationCheckSlot
    BOOL        stillValid          = TRUE
    MP_GAMEMODE thisGameMode        = GET_CURRENT_GAMEMODE()
    BOOL        inModeFM            = (thisGameMode = GAMEMODE_FM)
    BOOL        invitorIsPlayer     = g_sJLInvitesMP[checkPosition].jliInvitorIsPlayer
    INT         uniqueID            = NO_UNIQUE_ID
    INT         missionRequestSlot  = ILLEGAL_ARRAY_POSITION
    
    // The check position is stored, so always increase the check position for next frame
    g_nextJoblistInvitationCheckSlot++
    
    // Ignore this invitation if the mission details still need to be downloaded
    IF NOT (g_sJLInvitesMP[checkPosition].jliDownloaded)
		// Ignore this if we need to show the help message and invite signifier in a corona
		IF (IS_PC_VERSION())
			EXIT
		ENDIF
		
		IF (invitorIsPlayer)
		AND (Is_There_A_MissionsAtCoords_Focus_Mission())
		AND (IS_PLAYER_IN_CORONA())
		AND NOT (g_forceDisplayPlayerInvitesIndicator)
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Invitation Not Downloaded, but allow player invite through while in corona to allow help message and invite indicator")
			NET_NL()
		ELSE
        	EXIT
		ENDIF
    ENDIF
    
    // Ignore this invitation if the asynchonous block check is still waiting for a result
    IF (g_sJLInvitesMP[checkPosition].jliDoingPermissionCheck)
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [JobList]: Delay Invite Processing - still waiting for asynchonous Block check. CheckID: ", g_sJLInvitesMP[checkPosition].jliPermissionCheckID)
		#ENDIF
		
        EXIT
    ENDIF
    
    // Is the inviting player still ok?
    IF (invitorIsPlayer)
        IF NOT (IS_NET_PLAYER_OK(g_sJLInvitesMP[checkPosition].jliPlayer))
            // ...not ok
            stillValid = FALSE
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("Cleanup Invite. Inviting Player is not ok. Slot: ")
                NET_PRINT_INT(checkPosition)
                NET_NL()
            #ENDIF
        ENDIF
    ENDIF
    
    // Is the inviting player on mission now?
    // KGM TEMP: Use different checks for different gamemodes, hopefully this will be unified, but invites don't currently occur in CnC
    IF (invitorIsPlayer)
        IF (stillValid)
            IF (inModeFM)
                // Freemode - this will change to use Is_Player_On_Or_Triggering_Any_Mission() once everything goes through mission controller
                IF (IS_PLAYER_ON_ANY_FM_MISSION(g_sJLInvitesMP[checkPosition].jliPlayer, FALSE))
                    stillValid = FALSE
            
                    #IF IS_DEBUG_BUILD
                        NET_PRINT("...KGM MP [JobList]: ")
                        NET_PRINT_TIME()
                        NET_PRINT("Cleanup Invite. Inviting Player is now on an FM mission. Slot: ")
                        NET_PRINT_INT(checkPosition)
                        NET_PRINT("   [Invite from: ")
                        NET_PRINT(GET_PLAYER_NAME(g_sJLInvitesMP[checkPosition].jliPlayer))
                        NET_PRINT("]")
                        NET_NL()
                    #ENDIF
                ENDIF
            ELSE
                // Not Freemode (invites not sent from any other mode)
                stillValid = FALSE
            ENDIF
        ENDIF
    ENDIF
	
	// XBOXONE only - a refined user-to-user check may still be required. If the result is available and the check returns FALSE then need to kill the invite.
	IF (invitorIsPlayer)
		IF (stillValid)
			IF (g_sJLInvitesMP[checkPosition].jliDoGamerToGamerCheck)
				IF (Wait_For_Gamer_To_Gamer_Content_Restriction_Result(g_sJLInvitesMP[checkPosition].jliPlayer))
					// ...still waiting
					EXIT
				ENDIF
				
				// The result is available, so we don't need to wait for it any more, whatever the result
				g_sJLInvitesMP[checkPosition].jliDoGamerToGamerCheck = FALSE
				
				// Result is available, so check it
				IF (Check_If_Gamer_To_Gamer_Content_Restriction_Exists(g_sJLInvitesMP[checkPosition].jliPlayer))
					// ...restriction exists
					stillValid = FALSE
					
					// ...let the invitor know
					BROADCAST_JOBLIST_INVITE_REPLY(g_sJLInvitesMP[checkPosition].jliPlayer, TRUE, FALSE)
					
					// ...display a help message if appropriate
					Display_Restriction_Help_Message()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
   
    // Clean up an invite from an in-game character if this player is on a mission
    IF NOT (invitorIsPlayer)
        IF (stillValid)
            IF (Is_Player_On_Or_Triggering_Any_Mission())
                stillValid = FALSE
        
                #IF IS_DEBUG_BUILD
                    NET_PRINT("...KGM MP [JobList]: ")
                    NET_PRINT_TIME()
                    NET_PRINT("Cleanup Invite from NPC. This Player is now on a mission. Slot: ")
                    NET_PRINT_INT(checkPosition)
                    NET_PRINT("   [Invite from: ")
                    
                    TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLInvitesMP[checkPosition].jliNPC)
                    NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
                    NET_PRINT("]")
                    NET_NL()
                #ENDIF
            ENDIF
        ENDIF
    ENDIF
    
    // Is the inviting player is no longer reserved for a mission
    IF (invitorIsPlayer)
        IF (stillValid)
            uniqueID = Get_UniqueID_For_This_Players_Mission(g_sJLInvitesMP[checkPosition].jliPlayer)
            IF (uniqueID = NO_UNIQUE_ID)
                stillValid = FALSE
        
                #IF IS_DEBUG_BUILD
                    NET_PRINT("...KGM MP [JobList]: ")
                    NET_PRINT_TIME()
                    NET_PRINT("Cleanup Invite. Inviting Player is no longer on a mission. Slot: ")
                    NET_PRINT_INT(checkPosition)
                    NET_PRINT("   [")
                    NET_PRINT(GET_PLAYER_NAME(g_sJLInvitesMP[checkPosition].jliPlayer))
                    NET_PRINT("]")
                    NET_NL()
                #ENDIF
            ELSE
                missionRequestSlot = Find_Slot_Containing_This_MP_Mission(uniqueID)
                IF NOT (Is_Player_Reserved_For_MP_Mission_Request_Slot(missionRequestSlot, g_sJLInvitesMP[checkPosition].jliPlayer))
                    stillValid = FALSE
            
                    #IF IS_DEBUG_BUILD
                        NET_PRINT("...KGM MP [JobList]: ")
                        NET_PRINT_TIME()
                        NET_PRINT("Cleanup Invite. Inviting Player is no longer reserved for the same mission. Slot: ")
                        NET_PRINT_INT(checkPosition)
                        NET_PRINT("   [")
                        NET_PRINT(GET_PLAYER_NAME(g_sJLInvitesMP[checkPosition].jliPlayer))
                        NET_PRINT("]")
                        NET_NL()
                    #ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDIF
    
    // Is the Invite outwith its 'on hours'?
    // Delete immediately if it has never been shown to the player, delete after one in-game hour if it has been shown
    BOOL openAtThisHour = TRUE
    IF (stillValid)
        openAtThisHour = Can_Invite_Be_Displayed_At_This_Hour(g_sJLInvitesMP[checkPosition].jliOnHours)
        
        IF NOT (openAtThisHour)
            // ...the invite can't now be displayed
            // If it has never been displayed then kill it
            IF NOT (g_sJLInvitesMP[checkPosition].jliActive)
                stillValid = FALSE
        
                #IF IS_DEBUG_BUILD
                    NET_PRINT("...KGM MP [JobList]: ")
                    NET_PRINT_TIME()
                    NET_PRINT("Cleanup Invite. Invite is passed it's 'on time' but has never been displayed. Slot: ")
                    NET_PRINT_INT(checkPosition)
                    NET_NL()
                #ENDIF
            ENDIF
            
            // If it has been displayed, then keep it open displayed as 'closed' for one game hour then kill it
            IF (stillValid)
                IF NOT (Was_Invite_Ok_To_Display_Last_Hour(g_sJLInvitesMP[checkPosition].jliOnHours))
                    // ...the invite wasn't ok to display last hour so would have been marked 'closed', so kill it now instead of keeping it closed for another hour
                    stillValid = FALSE
        
                    #IF IS_DEBUG_BUILD
                        NET_PRINT("...KGM MP [JobList]: ")
                        NET_PRINT_TIME()
                        NET_PRINT("Cleanup Invite. Invite is passed it's 'on time' and has been displayed as 'closed' for an hour. Slot: ")
                        NET_PRINT_INT(checkPosition)
                        NET_NL()
                    #ENDIF
                ENDIF
            ENDIF
            
            // If still valid, it must need to get marked as 'closed' so let it drop to the display routines
        ENDIF
    ENDIF
	
	// KGM 12/7/14: If the invite has been asynchronously blocked then it is not still valid
	// NOTE: This means the invite is from a player that this player has blocked
     IF (invitorIsPlayer)
        IF (stillValid)
			IF (g_sJLInvitesMP[checkPosition].jliPermissionCheckFail)
                // ...the invite is from a player that is blocked, so clean it up and let teh invitor know
                stillValid = FALSE
				
				// Let invitor know 'blocked'
				BROADCAST_JOBLIST_INVITE_REPLY(g_sJLInvitesMP[checkPosition].jliPlayer, TRUE, FALSE)
    
                #IF IS_DEBUG_BUILD
                    NET_PRINT("...KGM MP [JobList]: ")
                    NET_PRINT_TIME()
                    NET_PRINT("Cleanup Invite. Invite has failed the asynchonous block check so is from a player that this player has blocked.")
                    NET_NL()
                #ENDIF
			ENDIF
		ENDIF
	ENDIF
    
    // Cleanup?
    IF NOT (stillValid)
		// MMM 1695633 - Check if the invite that is no longer valid has been read, 
		// if it hasn't we need to decrement the indicator 
		IF NOT (g_sJLInvitesMP[checkPosition].jliInviteRead) 
		AND g_sJLInvitesMP[checkPosition].jliActive 
		AND g_sJLInvitesMP[checkPosition].jliAllowDisplay
			NET_PRINT("[MMM][JobList] Same Session invite no longer exists and has not been read. Decrementing Invite Counter.")
			NET_NL()
			
			g_sJLInvitesMP[checkPosition].jliInviteRead = TRUE	// Probably not needed, but just incase	
			Decrement_New_Invites_Indicator()
		ENDIF
		
        // ...Invite needs to be cleaned up
        Delete_Joblist_Invitation_In_Invitation_Slot(checkPosition)
        EXIT
    ENDIF
    
    // The Invite is still valid
    // Get the description if required
    IF (g_sJLInvitesMP[checkPosition].jliRequireDesc)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Invite still requires the description, so maintain retrieval. Slot: ")
            NET_PRINT_INT(checkPosition)
            NET_NL()
        #ENDIF
        
        IF (g_sJLInvitesMP[checkPosition].jliDescHash = 0)
            // ...there is no description hash
            #IF IS_DEBUG_BUILD
                NET_PRINT("      BUT: MissionDescHash = 0, so no description to download, so setting description to not required") NET_NL()
            #ENDIF
            
            g_sJLInvitesMP[checkPosition].jliRequireDesc = FALSE
        ELSE
            // ...use the description hash to ask code to make the description available
            IF (REQUEST_AND_LOAD_CACHED_DESCRIPTION(g_sJLInvitesMP[checkPosition].jliDescHash, g_sJLInvitesMP[checkPosition].jliDescLoadVars))
                #IF IS_DEBUG_BUILD
                    NET_PRINT("...KGM MP [JobList]: ")
                    NET_PRINT_TIME()
                    NET_PRINT("Description Retrieved. Slot: ")
                    NET_PRINT_INT(checkPosition)
                    NET_NL()
                    NET_PRINT("      - ")
                    NET_PRINT(UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(g_sJLInvitesMP[checkPosition].jliDescHash, INVITE_MAX_DESCRIPTION_LENGTH))
                    NET_NL()
                #ENDIF
                
                // ...description is now ready
                g_sJLInvitesMP[checkPosition].jliRequireDesc = FALSE
            ELSE
                // ...description is not yet ready, so keep waiting
                #IF IS_DEBUG_BUILD
                    NET_PRINT("      BUT: Description not yet ready, so keep waiting") NET_NL()
                #ENDIF
                
                EXIT
            ENDIF
        ENDIF
    ENDIF
    
    // Hide the invite if a restriction is in place
    #IF IS_DEBUG_BUILD
        BOOL DEBUG_missionHasSpace      = TRUE
        BOOL DEBUG_hasFocusMission      = FALSE
        BOOL DEBUG_onAmbientTutorial    = FALSE
        BOOL DEBUG_closedAtThisHour     = FALSE
		BOOL DEBUG_spectating			= FALSE
		BOOL DEBUG_mocapCutscene		= FALSE
    #ENDIF
    
    BOOL    displayInvite               = TRUE
	BOOL    inviteBlockedByFocusMission	= FALSE
	BOOL    inviteBlockedForOtherReason = FALSE
    
    // Hide the invite if it should be marked as 'closed' in the hour after closing time
    IF NOT (openAtThisHour)
        displayInvite = FALSE
		inviteBlockedForOtherReason = TRUE

        #IF IS_DEBUG_BUILD
            DEBUG_closedAtThisHour  = TRUE
        #ENDIF
    ENDIF
    
    // Hide the invite if there is no room in the corona for more players
    IF NOT (Check_If_Corona_Still_Has_Space_For_More_Players(checkPosition))
        displayInvite = FALSE
		inviteBlockedForOtherReason = TRUE

        #IF IS_DEBUG_BUILD
            DEBUG_missionHasSpace   = FALSE
        #ENDIF
    ENDIF
    
    // Hide the invite if the MissionAtCoords system already has a Focus Mission
    // NOTE: A player gets reserved for the Focus Mission, but there are a few frames when the reservation request is being sent over the network
    IF (Is_There_A_MissionsAtCoords_Focus_Mission())
        displayInvite = FALSE
		inviteBlockedByFocusMission = TRUE

        #IF IS_DEBUG_BUILD
            DEBUG_hasFocusMission   = TRUE
        #ENDIF
    ENDIF
    
    // Hide the invite if the player is on an ambient tutorial mission
    IF (IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL())
        displayInvite = FALSE
		inviteBlockedForOtherReason = TRUE

        #IF IS_DEBUG_BUILD
            DEBUG_onAmbientTutorial = TRUE
        #ENDIF
    ENDIF
	
	// Hide if the player is spectating
	IF (IS_A_SPECTATOR_CAM_ACTIVE())
        displayInvite = FALSE
		inviteBlockedForOtherReason = TRUE

        #IF IS_DEBUG_BUILD
            DEBUG_spectating = TRUE
        #ENDIF
	ENDIF
	
	// Hide if a Mocap Cutscene is playing
	IF (IS_CUTSCENE_PLAYING())
        displayInvite = FALSE
		inviteBlockedForOtherReason = TRUE

        #IF IS_DEBUG_BUILD
            DEBUG_mocapCutscene = TRUE
        #ENDIF
	ENDIF
	
	// Hide player is an animal
	IF IS_LOCAL_PLAYER_AN_ANIMAL()
        displayInvite = FALSE
		inviteBlockedForOtherReason = TRUE

        #IF IS_DEBUG_BUILD
            DEBUG_mocapCutscene = TRUE
        #ENDIF
	ENDIF
    
    // Check if the display status has changed
    IF (g_sJLInvitesMP[checkPosition].jliAllowDisplay != displayInvite)
        // The display flag has changed
        g_sJLInvitesMP[checkPosition].jliAllowDisplay = displayInvite
        
        #IF IS_DEBUG_BUILD
            IF (displayInvite)
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("Invite is allowed to be displayed on the joblist: ")
            ELSE
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("Invite is not allowed to be displayed on Joblist")
                IF NOT (DEBUG_missionHasSpace)
                    NET_PRINT(" [CORONA FULL OR DATA INCONSISTENT]")
                ENDIF
                IF (DEBUG_hasFocusMission)
                    NET_PRINT(" [PLAYER HAS A FOCUS MISSION]")
                ENDIF
                IF (DEBUG_onAmbientTutorial)
                    NET_PRINT(" [PLAYER ON AMBIENT TUTORIAL]")
                ENDIF
                IF (DEBUG_closedAtThisHour)
                    NET_PRINT(" [CLOSED AT THIS HOUR: ")
                    NET_PRINT_INT(GET_CLOCK_HOURS())
                    NET_PRINT("]")
                ENDIF
                IF (DEBUG_spectating)
                    NET_PRINT(" [SPECTATING]")
                ENDIF
				IF (DEBUG_mocapCutscene)
					NET_PRINT(" [MOCAP]")
				ENDIF
                NET_PRINT(": ")
            ENDIF
            NET_PRINT_INT(checkPosition)
            NET_PRINT(" - ")
            NET_PRINT(GET_STRING_FROM_TEXT_FILE(Get_FM_Joblist_Activity_TextLabel(g_sJLInvitesMP[checkPosition].jliType, g_sJLInvitesMP[checkPosition].jliSubtype,g_sJLRVInvitesMP[checkPosition].jrviPropertyType, g_sJLRVInvitesMP[checkPosition].jrviRootId)))
            IF (invitorIsPlayer)
                NET_PRINT("   [From Player: ")
                NET_PRINT(GET_PLAYER_NAME(g_sJLInvitesMP[checkPosition].jliPlayer))
            ELSE
                TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLInvitesMP[checkPosition].jliNPC)
                
                NET_PRINT("   [From NPC: ")
                NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            ENDIF
            NET_PRINT("]")
            NET_NL()
        #ENDIF
    ENDIF

    // Maintain Activation - this won't send the feed invite if the invite isn't currently allowed to be displayed
    // NOTE: If the invite is blocked ONLY because of a focus mission and the player is in a corona, then a help message and invite signifier may still need to be displayed
	BOOL inviteBlockedOnlyByFocusMission = FALSE
  
  	IF NOT (IS_PC_VERSION())
		IF (inviteBlockedByFocusMission)
		AND NOT (inviteBlockedForOtherReason)
			IF (invitorIsPlayer)
		    	inviteBlockedOnlyByFocusMission = TRUE
			ENDIF
		ENDIF
	ENDIF
    
    Maintain_Invite_Activation_And_Headshot_Generation(checkPosition, inviteBlockedOnlyByFocusMission)
    
    // If the Joblist App is active then set the invite as 'read'
    IF NOT (g_sJLInvitesMP[checkPosition].jliInviteRead)
        IF (Is_Joblist_App_On_Screen())
            g_sJLInvitesMP[checkPosition].jliInviteRead = TRUE
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("Invite from player has now been listed on Joblist - marking as READ: ")
                NET_PRINT_INT(checkPosition)
                NET_PRINT("   [")
                IF (invitorIsPlayer)
                    NET_PRINT(GET_PLAYER_NAME(g_sJLInvitesMP[checkPosition].jliPlayer))
                ELSE
                    TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLInvitesMP[checkPosition].jliNPC)
                    NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
                ENDIF
                NET_PRINT("]")
                NET_NL()
            #ENDIF
            
            // KGM 14/7/13: This is quick and dirty way to reset new invites indicator by checking if the app is on screen, even if this invite isn't on display
            //              - the indicator isn't working correctly yet
            //Reset_New_Invites_Indicator()
        ENDIF
    ENDIF
    
    // Maintain the 'duplicate' flag - if this mission was a duplicate of an existing invite with duplicate details that has since been deleted then this invite will no longer be marked as duplicate
    g_sJLInvitesMP[checkPosition].jliIsDuplicate = Check_If_Duplicate_Invite(checkPosition)
    
ENDPROC




// ===========================================================================================================
//      JobList Cross-Session Invite Storage functions
//      Invitations are stored on an array and as many are added to the joblist as there is room for
//      The newest entries are always at the start of the array and there should be one entry max per player
// ===========================================================================================================

// PURPOSE: Find the Cross-Session Invite slot containing an Invitation from this InvitorGamerTag
//
// INPUT PARAMS:        paramInvitorGamerTag    The GamerTag being searched for
// RETURN VALUE:        INT                     The Slot containing this invitor's invitation, or NO_JOBLIST_ARRAY_ENTRY if none
FUNC INT Get_Joblist_CSInvite_Slot_For_InvitorGamerTag(TEXT_LABEL_63 paramInvitorGamerTag)

    INT tempLoop = 0
    REPEAT g_numMPCSInvites tempLoop
        IF (ARE_STRINGS_EQUAL(paramInvitorGamerTag, g_sJLCSInvitesMP[tempLoop].jcsiInvitorGamerTag))
            // Found an invitation by this invitor
            RETURN tempLoop
        ENDIF
    ENDREPEAT
    
    // No Cross-Session Invite by selected InvitorName
    RETURN NO_JOBLIST_ARRAY_ENTRY

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Find the Cross-Session Invite slot containing an Invitation from this Invitor DisplayName (defaulting back to GamerTag if necessary)
//
// INPUT PARAMS:        paramDisplayName        The Displayname being searched for
// RETURN VALUE:        INT                     The Slot containing this invitor's invitation, or NO_JOBLIST_ARRAY_ENTRY if none
FUNC INT Get_Joblist_CSInvite_Slot_For_InvitorDisplayName(TEXT_LABEL_63 paramDisplayName)

    INT tempLoop = 0
	
	// First, search the display names
    REPEAT g_numMPCSInvites tempLoop
        IF (ARE_STRINGS_EQUAL(paramDisplayName, g_sJLCSInvitesMP[tempLoop].jcsiInvitorDisplayName))
            // Found an invitation by this Display Name
            RETURN tempLoop
        ENDIF
    ENDREPEAT
	
	// If not found, try the GamerTags
	RETURN (Get_Joblist_CSInvite_Slot_For_InvitorGamerTag(paramDisplayName))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Delete a Cross-Session Invite with InviteID
//
// INPUT PARAMS:        paramInviteID           The InviteID to be deleted from the code array
PROC Delete_Cross_Session_Invite_With_This_InviteID(INT paramInviteID)

    INT inviteIndex = NETWORK_GET_PRESENCE_INVITE_INDEX_BY_ID(paramInviteID)
    
    IF (inviteIndex < 0)
    OR (inviteIndex >= NETWORK_GET_NUM_PRESENCE_INVITES())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT(" Presence Invite does not exist. Probably already removed.")
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // Still exists
    IF (NETWORK_REMOVE_PRESENCE_INVITE(inviteIndex))
        // Succeeded
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT(" Removed Presence Invite with this Index: ")
            NET_PRINT_INT(inviteIndex)
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // Failed
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Tried to Remove Presence Invite with this Index: ")
        NET_PRINT_INT(inviteIndex)
        NET_PRINT(" - CODE FUNCTION RETURNED FALSE")
        NET_NL()
    #ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Delete the entry in the specified Cross-Session Invite slot
//
// INPUT PARAMS:        paramSlot               The slot within the Cross-Session Invite Array that needs to be deleted - all entries after it in the array will be shuffled up
PROC Delete_Joblist_CSInvite_In_Invitation_Slot(INT paramSlot)

    // Ensure the passed-in slot is valid
    IF (paramSlot >= g_numMPCSInvites)
        // ...slot isn't valid
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Cross-Session Invite Entry Slot Invalid. Slot to be deleted: ")
            NET_PRINT_INT(paramSlot)
            NET_PRINT(" [Current Slots In Use: ")
            NET_PRINT_INT(g_numMPCSInvites)
            NET_PRINT("]")
            NET_NL()
        #ENDIF

        EXIT
    ENDIF
    
	INT i = 0
   	FOR i = 0 TO MAX_MP_JOBLIST_ENTRIES - 1 	
		IF GET_GAME_TIMER() > g_spamJobList[i].Timer OR g_spamJobList[i].Timer = 0	// Check if the time has expired, if it has this slot is free to use to store spam.
			
			IF NETWORK_GET_NUM_PRESENCE_INVITES() > 0 
			AND g_sJLCSInvitesMP[paramSlot].jcsiInviteID < NETWORK_GET_NUM_PRESENCE_INVITES()
				PRINTLN("[MMM][JobList] Deleted CS Invite, Storing for Spam")
				PRINTLN("[MMM][JobList] Using Invite ID : ", g_sJLCSInvitesMP[paramSlot].jcsiInviteID)
				PRINTLN("[MMM][JobList] NETWORK_GET_PRESENCE_INVITE_SESSION_ID() : ", NETWORK_GET_PRESENCE_INVITE_SESSION_ID(g_sJLCSInvitesMP[paramSlot].jcsiInviteID))
				g_spamJobList[i].jlcsInviteSessionID = NETWORK_GET_PRESENCE_INVITE_SESSION_ID(g_sJLCSInvitesMP[paramSlot].jcsiInviteID)
				g_spamJobList[i].Timer				 = GET_GAME_TIMER() + 30000	
				i = MAX_MP_JOBLIST_ENTRIES
			ENDIF
		ENDIF			
	ENDFOR
	
    // Slot is valid, so delete the feed entry if it exists
    IF (g_sJLCSInvitesMP[paramSlot].jcsiFeedID != NO_JOBLIST_INVITE_FEED_ID)
        THEFEED_REMOVE_ITEM(g_sJLCSInvitesMP[paramSlot].jcsiFeedID)
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Requested removal of Feed Entry if on display: ")
            NET_PRINT_INT(g_sJLCSInvitesMP[paramSlot].jcsiFeedID)
            NET_NL()
        #ENDIF
    ENDIF
    
    // Clean up the 500-char description details for the slot they exist
    IF NOT (g_sJLCSInvitesMP[paramSlot].jcsiRequireDesc)
        IF (g_sJLCSInvitesMP[paramSlot].jcsiDescHash != 0)
            UGC_RELEASE_CACHED_DESCRIPTION(g_sJLCSInvitesMP[paramSlot].jcsiDescHash)
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("Requested removal of 500-char description with this hash: ")
                NET_PRINT_INT(g_sJLCSInvitesMP[paramSlot].jcsiDescHash)
                NET_NL()
            #ENDIF
        ENDIF
    ENDIF
    
    // Let code know the entry has been deleted if it still exists.
    Delete_Cross_Session_Invite_With_This_InviteID(g_sJLCSInvitesMP[paramSlot].jcsiInviteID)
    
    // Move all entries after this in the array down one place to cover it
    INT toSlot = paramSlot
    INT fromSlot = toSlot + 1
    
    WHILE (fromSlot < g_numMPCSInvites)
        g_sJLCSInvitesMP[toSlot] = g_sJLCSInvitesMP[fromSlot]
        toSlot++
        fromSlot++
    ENDWHILE
    
    // Need to clear the last slot that used to contain valid data
    Clear_One_CSInvite_Array_Slot(toSlot)
    
    // Need to reduce the number of entries in the array
    g_numMPCSInvites--
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Cross-Session Invite Entry Deleted In Slot: ")
        NET_PRINT_INT(paramSlot)
        NET_NL()
    #ENDIF
	
	// KGM 27/3/14: If the 'new invite' flag is set and the 'num new invites' indicator is greater than 0 then clear the 'new invite' flag
	//				This isn't entirely accurate but that flag is only needed at the start of the game for a help message from DaveW, so
	//				it's better for the help to be delayed temporarily rather than, as can happen just now, it gets displayed but the
	//				invite has already been deleted
	IF (g_joblistInviteReceived)
		IF (g_numMPJobListEntries > 0)
			PRINTLN("...KGM MP [Joblist]: Clearing 'Joblist Invite Received' flag because a Cross-Session Invite Entry has been deleted")
			Clear_New_Invite_Received()
		ENDIF
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a cross-session invite still exists
//
// INPUT PARAMS:        paramSlot               The slot within the Cross-Session Invite Array being checked
FUNC BOOL Does_Cross_Session_Invite_Still_Exist(INT paramSlot)

    // Get this invite's Invite Index
    INT inviteIndex = NETWORK_GET_PRESENCE_INVITE_INDEX_BY_ID(g_sJLCSInvitesMP[paramSlot].jcsiInviteID)
    
    IF (inviteIndex < 0)
    OR (inviteIndex >= NETWORK_GET_NUM_PRESENCE_INVITES())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Cross-Session Invite No longer exists. Slot: ")
            NET_PRINT_INT(paramSlot)
            NET_PRINT("  [Invite Index: ")
            NET_PRINT_INT(inviteIndex)
            NET_PRINT("]")
            NET_NL()
        #ENDIF
        
        RETURN FALSE
    ENDIF
    
    // Still exists
    RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain the downloading of any CSInvites
PROC Maintain_Joblist_CSInvites_Download()

	// Nothing to do for CSInvites if an Invitation is downloading a mission header
	IF (g_invitationBeingDownloaded)
		EXIT
	ENDIF

    // Nothing to do if there are no Cross-Session Invites, but clear the CSInvite being downloaded flag because there can't be one being downloaded
    IF (g_numMPCSInvites = 0)
		g_csInviteBeingDownloaded = FALSE
        EXIT
    ENDIF
	
	// Some debug output
	#IF IS_DEBUG_BUILD
		IF (g_csInviteBeingDownloaded)
			NET_PRINT("...KGM MP [JobList]: CSInvite Header Download flag is TRUE") NET_NL()
		ENDIF
	#ENDIF

    // Gather Details
    INT beingDownloadedSlot = ILLEGAL_ARRAY_POSITION
    INT nextDownloadSlot    = ILLEGAL_ARRAY_POSITION
    INT tempLoop            = 0
    
    REPEAT g_numMPCSInvites tempLoop
        // ...being downloaded?
        IF (beingDownloadedSlot = ILLEGAL_ARRAY_POSITION)
            IF (g_sJLCSInvitesMP[tempLoop].jcsiDownloading)
                beingDownloadedSlot = tempLoop
            ENDIF
        ENDIF
        
        // ...to be downloaded?
        IF (nextDownloadSlot = ILLEGAL_ARRAY_POSITION)
            IF (g_sJLCSInvitesMP[tempLoop].jcsiToDownload)
                nextDownloadSlot = tempLoop
            ENDIF
        ENDIF
    ENDREPEAT
 	
	// Maintain the 'CSInvite being downloaded' flag based on whether or not a mission download is set for a CSInvite
	IF (beingDownloadedSlot = ILLEGAL_ARRAY_POSITION)
		// ...no invitation mission download
		g_csInviteBeingDownloaded = FALSE
	ELSE
		// ...invitation mission being downloaded
		#IF IS_DEBUG_BUILD
			IF NOT (g_csInviteBeingDownloaded)
				NET_PRINT("...KGM MP [JobList]: New CSInvite Header Download Starting: Array Position ") NET_PRINT_INT(beingDownloadedSlot) NET_NL()
			ENDIF
		#ENDIF
		
		g_csInviteBeingDownloaded = TRUE
	ENDIF
   
    // Is there mission header data being downloaded?
    IF (beingDownloadedSlot != ILLEGAL_ARRAY_POSITION)
        // Load the Header Data from the cloud
        // Safety Check - relaunch the mission loading script if it isn't launched (this should never be the case)
        IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("net_cloud_mission_loader")) = 0)
            REQUEST_SCRIPT("net_cloud_mission_loader")
            IF (HAS_SCRIPT_LOADED("net_cloud_mission_loader"))
                START_NEW_SCRIPT("net_cloud_mission_loader", FRIEND_STACK_SIZE)
                SET_SCRIPT_AS_NO_LONGER_NEEDED("net_cloud_mission_loader")
            ELSE
                #IF IS_DEBUG_BUILD
                    NET_PRINT("...KGM MP [JobList]: CSInvite Header Download - Waiting for script to launch: net_cloud_mission_loader") NET_NL()
                #ENDIF
                    
                EXIT
            ENDIF
        ENDIF
        
        // Load the header data from the cloud
        IF NOT (Load_Cloud_Loaded_Header_Data(g_sJLCSInvitesMP[beingDownloadedSlot].jcsiContentID))
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: CSInvite: Still waiting for the Cloud-Loaded Header Data to be available: ")
				NET_PRINT(g_sJLCSInvitesMP[beingDownloadedSlot].jcsiContentID)
				NET_NL()
            #ENDIF
                
            EXIT
        ENDIF
        
        // Cloud-Load complete
		// Always, set the 'CSInvite being downloaded' flag to false to open this up to another invite
		g_csInviteBeingDownloaded = FALSE
		
        // Check if it downloaded successfully
        IF NOT (Did_Cloud_Loaded_Header_Data_Load_Successfully())
            // ...download failed, so delete this entry and don't do any more array processing this frame in this function
            Delete_Joblist_CSInvite_In_Invitation_Slot(beingDownloadedSlot)
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: Download failed. Removing CSInvite.") NET_NL()
            #ENDIF
            
            EXIT
        ENDIF
        
        // Download successful, so update the appropriate variables, treat as player's own UGC
        g_sJLCSInvitesMP[beingDownloadedSlot].jcsiToDownload        = FALSE
        g_sJLCSInvitesMP[beingDownloadedSlot].jcsiDownloading       = FALSE
        g_sJLCSInvitesMP[beingDownloadedSlot].jcsiDownloaded        = TRUE
        g_sJLCSInvitesMP[beingDownloadedSlot].jcsiCreatorID         = NATIVE_TO_INT(PLAYER_ID())
        g_sJLCSInvitesMP[beingDownloadedSlot].jcsiType              = g_sCloudMissionLoaderMP.cmlReturnType
		g_sJLCSInvitesMP[beingDownloadedSlot].jcsiSubtype			= g_sCloudMissionLoaderMP.cmlReturnSubtype
        g_sJLCSInvitesMP[beingDownloadedSlot].jcsiMissionName       = g_sCloudMissionLoaderMP.cmlReturnMissionName
        g_sJLCSInvitesMP[beingDownloadedSlot].jcsiDescription       = g_sCloudMissionLoaderMP.cmlReturnDescription
		g_sJLCSInvitesMP[beingDownloadedSlot].jcsiDescHash			= g_cloudLoadedMissionHeader.iMissionDecHash
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Header Downloaded. Updating Cross-Session Invite Details At Array Location: ")
            NET_PRINT_INT(beingDownloadedSlot)
            NET_NL()
            NET_PRINT("      MissionName: ")
            NET_PRINT(g_sJLCSInvitesMP[beingDownloadedSlot].jcsiMissionName)
            NET_PRINT("  [")
            NET_PRINT(g_sJLCSInvitesMP[beingDownloadedSlot].jcsiDescription)
            NET_PRINT("]")
            IF (g_sJLCSInvitesMP[beingDownloadedSlot].jcsiPlaylistTotal > 0)
				// KGM 9/12/13: NOTE, a current playlist job position > CROSS_SESSION_H2H_WAGER_OFFSET means a H2H Playlist with a wager, the wager is currentplaylist value - CROSS_SESSION_H2H_WAGER_OFFSET
				IF (g_sJLCSInvitesMP[beingDownloadedSlot].jcsiPlaylistCurrent >= CROSS_SESSION_H2H_WAGER_OFFSET)
					NET_PRINT(" - Head-To-Head Playlist Wager: ")
					NET_PRINT_INT(g_sJLCSInvitesMP[beingDownloadedSlot].jcsiPlaylistCurrent - CROSS_SESSION_H2H_WAGER_OFFSET)
					NET_PRINT(" [Total Playlist Jobs: ")
		            NET_PRINT_INT(g_sJLCSInvitesMP[beingDownloadedSlot].jcsiPlaylistTotal)
					NET_PRINT("]")
				ELSE
	                NET_PRINT(" - Playlist ")
	                NET_PRINT_INT(g_sJLCSInvitesMP[beingDownloadedSlot].jcsiPlaylistCurrent)
	                NET_PRINT("/")
	                NET_PRINT_INT(g_sJLCSInvitesMP[beingDownloadedSlot].jcsiPlaylistTotal)
				ENDIF
            ENDIF
            NET_NL()
            NET_PRINT("      InvitorName: ")
            NET_PRINT(g_sJLCSInvitesMP[beingDownloadedSlot].jcsiInvitorGamerTag)
            IF (Is_There_A_MissionsAtCoords_Focus_Mission())
                NET_PRINT(" [PLAYER HAS A FOCUS MISSION]")
            ELSE
                NET_PRINT(" [player has no focus mission]")
            ENDIF
// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
//            IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
//                NET_PRINT(" [HAS WANTED LEVEL]")
//            ELSE
//                NET_PRINT(" [no wanted level]")
//            ENDIF
            NET_NL()
        #ENDIF
    ENDIF
    
    // Is there mission header data waiting to be downloaded?
    IF (nextDownloadSlot != ILLEGAL_ARRAY_POSITION)
		IF NOT (Is_Player_Allocated_For_Any_Mission(PLAYER_ID()))
		AND NOT NETWORK_IS_ACTIVITY_SESSION()
			IF (GET_TRANSITION_SESSION_VOTE_STATUS() != ciFMMC_EOM_VOTE_STATUS_ROUNDS_MISSION)
			AND (GET_TRANSITION_SESSION_VOTE_STATUS() != ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART)
			AND (GET_TRANSITION_SESSION_VOTE_STATUS() != ciFMMC_EOM_VOTE_STATUS_QUICK_RESTART_SWAP)
		        g_sJLCSInvitesMP[nextDownloadSlot].jcsiToDownload       = FALSE
		        g_sJLCSInvitesMP[nextDownloadSlot].jcsiDownloading      = TRUE
		        
		        #IF IS_DEBUG_BUILD
		            NET_PRINT("...KGM MP [JobList]: ")
		            NET_PRINT_TIME()
		            NET_PRINT("Cross-Session Invite Details Setting up Download At Array Location: ")
		            NET_PRINT_INT(nextDownloadSlot)
		            NET_NL()
		        #ENDIF
				
				// KGM 16/3/15 [BUG 2279487]: Use the cached data for Heist Prep Mission downloads if available
				IF (g_sJLCSInvitesMP[nextDownloadSlot].jcsiType = FMMC_TYPE_MISSION)
					// The cache contains heist planning mission header details and this is a heist planning invite, so check the cache
					PRINTLN(".KGM [JobList]: The next CSInvite download slot is for a Mission - check for cached Heist Prep data. ContentID: ", g_sJLCSInvitesMP[nextDownloadSlot].jcsiContentID)
					INT cachedArrayPos = Find_Cached_Mission_Header_Data(g_sJLCSInvitesMP[nextDownloadSlot].jcsiContentID)
					IF (cachedArrayPos != ILLEGAL_ARRAY_POSITION)
						PRINTLN(".KGM [JobList]: CSInvite Details Header Data found in cache. CSInvite Slot: ", nextDownloadSlot)
						
						// Store the variables
				        g_sJLCSInvitesMP[nextDownloadSlot].jcsiToDownload		= FALSE
				        g_sJLCSInvitesMP[nextDownloadSlot].jcsiDownloading		= FALSE
				        g_sJLCSInvitesMP[nextDownloadSlot].jcsiDownloaded		= TRUE
				        g_sJLCSInvitesMP[nextDownloadSlot].jcsiCreatorID		= NATIVE_TO_INT(PLAYER_ID())
				        g_sJLCSInvitesMP[nextDownloadSlot].jcsiType				= g_FMMC_PLANNING_MISSION.sMissionHeaderVars[cachedArrayPos].iType
						g_sJLCSInvitesMP[nextDownloadSlot].jcsiSubtype			= g_FMMC_PLANNING_MISSION.sMissionHeaderVars[cachedArrayPos].iSubType
				        g_sJLCSInvitesMP[nextDownloadSlot].jcsiMissionName		= g_FMMC_PLANNING_MISSION.sMissionHeaderVars[cachedArrayPos].tlMissionName
						g_sJLCSInvitesMP[nextDownloadSlot].jcsiDescription		= ""
						g_sJLCSInvitesMP[nextDownloadSlot].jcsiDescHash			= 0	// For now - will need to handle the description
						
				        #IF IS_DEBUG_BUILD
				            NET_PRINT("...KGM MP [JobList]: ")
				            NET_PRINT_TIME()
				            NET_PRINT("Header Cached. Updating Cross-Session Invite Details At Array Location: ")
				            NET_PRINT_INT(nextDownloadSlot)
				            NET_NL()
				            NET_PRINT("      MissionName: ")
				            NET_PRINT(g_sJLCSInvitesMP[nextDownloadSlot].jcsiMissionName)
				            NET_PRINT("  [")
				            NET_PRINT(g_sJLCSInvitesMP[nextDownloadSlot].jcsiDescription)
				            NET_PRINT("]")
				            IF (g_sJLCSInvitesMP[nextDownloadSlot].jcsiPlaylistTotal > 0)
								// KGM 9/12/13: NOTE, a current playlist job position > CROSS_SESSION_H2H_WAGER_OFFSET means a H2H Playlist with a wager, the wager is currentplaylist value - CROSS_SESSION_H2H_WAGER_OFFSET
								IF (g_sJLCSInvitesMP[nextDownloadSlot].jcsiPlaylistCurrent >= CROSS_SESSION_H2H_WAGER_OFFSET)
									NET_PRINT(" - Head-To-Head Playlist Wager: ")
									NET_PRINT_INT(g_sJLCSInvitesMP[nextDownloadSlot].jcsiPlaylistCurrent - CROSS_SESSION_H2H_WAGER_OFFSET)
									NET_PRINT(" [Total Playlist Jobs: ")
						            NET_PRINT_INT(g_sJLCSInvitesMP[nextDownloadSlot].jcsiPlaylistTotal)
									NET_PRINT("]")
								ELSE
					                NET_PRINT(" - Playlist ")
					                NET_PRINT_INT(g_sJLCSInvitesMP[nextDownloadSlot].jcsiPlaylistCurrent)
					                NET_PRINT("/")
					                NET_PRINT_INT(g_sJLCSInvitesMP[nextDownloadSlot].jcsiPlaylistTotal)
								ENDIF
				            ENDIF
				            NET_NL()
				            NET_PRINT("      InvitorName: ")
//				            NET_PRINT(g_sJLCSInvitesMP[nextDownloadSlot].jcsiInvitorName)
				            IF (Is_There_A_MissionsAtCoords_Focus_Mission())
				                NET_PRINT(" [PLAYER HAS A FOCUS MISSION]")
				            ELSE
				                NET_PRINT(" [player has no focus mission]")
				            ENDIF
				            NET_NL()
				        #ENDIF
					ENDIF
				ENDIF
			ELSE
		        #IF IS_DEBUG_BUILD
		            NET_PRINT("...KGM MP [JobList]: ")
		            NET_PRINT_TIME()
		            NET_PRINT("Cross-Session Invite Details Download At Array Location: ")
		            NET_PRINT_INT(nextDownloadSlot)
					NET_PRINT(" - DELAY, DOING ROUNDS OR QUICK RESTART")
		            NET_NL()
		        #ENDIF
			ENDIF
		ELSE
	        #IF IS_DEBUG_BUILD
	            NET_PRINT("...KGM MP [JobList]: ")
	            NET_PRINT_TIME()
	            NET_PRINT("Cross-Session Invite Details Download At Array Location: ")
	            NET_PRINT_INT(nextDownloadSlot)
				NET_PRINT(" - DELAY, PLAYER RESERVED OR ACTIVE ON A MISSION")
	            NET_NL()
	        #ENDIF
		ENDIF
    ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check the validity of the invitations on the Joblist Cross-Session Invites list, clean up if any are no longer valid
// NOTES:   This will be scheduled so it will only check one per frame
PROC Maintain_Joblist_CSInvites()
    
    // Perform any every frame maintenance
	Maintain_Asynchronous_CSInvites_DisplayName_Retrieval()
    Maintain_Joblist_CSInvites_Download()

    // Are there any invitations left (it's possible on download failure that there are now none)?
    IF (g_numMPCSInvites = 0)
        // ...no
        EXIT
    ENDIF
    
    // There are invitations, do a general maintenance check of one each frame
    IF (g_nextJoblistCSInviteCheckSlot >= g_numMPCSInvites)
        // ...wrap
        g_nextJoblistCSInviteCheckSlot = 0
    ENDIF
    
    // Check the invitation at the 'next check' array position
    INT         checkPosition       = g_nextJoblistCSInviteCheckSlot
    
    // The check position is stored, so always increase the check position for next frame
    g_nextJoblistCSInviteCheckSlot++
    
    // The Invite is still valid
    // Ignore this invite if the mission details still need to be downloaded
    IF NOT (g_sJLCSInvitesMP[checkPosition].jcsiDownloaded)
		// Ignore this if we need to show the help message and invite signifier in a corona
		IF (IS_PC_VERSION())
			EXIT
		ENDIF
		
		IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		AND (IS_PLAYER_IN_CORONA())
		AND NOT (g_forceDisplayPlayerInvitesIndicator)
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("CSInvite Not Downloaded, but allow player invite through while in corona to allow help message and invite indicator")
			NET_NL()
		ELSE
        	EXIT
		ENDIF
    ENDIF
    
    // Get the description if required
    IF (g_sJLCSInvitesMP[checkPosition].jcsiRequireDesc)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("CSInvite still requires the description, so maintain retrieval. Slot: ")
            NET_PRINT_INT(checkPosition)
            NET_NL()
        #ENDIF
        
        IF (g_sJLCSInvitesMP[checkPosition].jcsiDescHash = 0)
            // ...there is no description hash
            #IF IS_DEBUG_BUILD
                NET_PRINT("      BUT: MissionDescHash = 0, so no description to download, so setting description to not required") NET_NL()
            #ENDIF
            
            g_sJLCSInvitesMP[checkPosition].jcsiRequireDesc = FALSE
        ELSE
            // ...use the description hash to ask code to make the description available
            IF (REQUEST_AND_LOAD_CACHED_DESCRIPTION(g_sJLCSInvitesMP[checkPosition].jcsiDescHash, g_sJLCSInvitesMP[checkPosition].jcsiDescLoadVars))
                #IF IS_DEBUG_BUILD
                    NET_PRINT("...KGM MP [JobList]: ")
                    NET_PRINT_TIME()
                    NET_PRINT("Description Retrieved. Slot: ")
                    NET_PRINT_INT(checkPosition)
                    NET_NL()
                    NET_PRINT("      - ")
                    NET_PRINT(UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(g_sJLCSInvitesMP[checkPosition].jcsiDescHash, INVITE_MAX_DESCRIPTION_LENGTH))
                    NET_NL()
                #ENDIF
                
                // ...description is now ready
                g_sJLCSInvitesMP[checkPosition].jcsiRequireDesc = FALSE
            ELSE
                // ...description is not yet ready, so keep waiting
                #IF IS_DEBUG_BUILD
                    NET_PRINT("      BUT: Description not yet ready, so keep waiting") NET_NL()
                #ENDIF
                
                EXIT
            ENDIF
        ENDIF
    ENDIF
    
    // Ignore this invite if the DisplayName Request is still ongoing - this is checked after the description retrieval maintenance so they can occur at the same time
	// NOTE: Don't block the invite if teh display name request has been ongoing for too long
    IF (g_sJLCSInvitesMP[checkPosition].jcsiDisplayNameID != NO_DISPLAY_NAME_RETRIEVAL)
		IF (GET_GAME_TIMER() < g_sJLCSInvitesMP[checkPosition].jcsiDisplayNameTimeout)
        	EXIT
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [JobList]: ")
			NET_PRINT_TIME()
			NET_PRINT("Asynchronous DisplayName for RequestID: ")
			NET_PRINT_INT(g_sJLCSInvitesMP[checkPosition].jcsiDisplayNameID)
			NET_PRINT(" - Ongoing for too long, so no longer blocking the invite")
			NET_NL()
		#ENDIF
    ENDIF
    
    // Delete the invite if no longer active
    IF NOT (Does_Cross_Session_Invite_Still_Exist(checkPosition))
		// MMM 1695633 - Check if the invite that is no longer valid has been read, 
		// if it hasn't we need to decrement the indicator
		IF NOT (g_sJLCSInvitesMP[checkPosition].jcsiInviteRead) AND g_sJLCSInvitesMP[checkPosition].jcsiActive
			NET_PRINT("[MMM][JobList] Cross Session invite no longer exists and has not been read. Decrementing Invite Counter.")
			NET_NL()
			g_sJLCSInvitesMP[checkPosition].jcsiInviteRead = TRUE	// Probably not needed, but just incase	
			Decrement_New_Invites_Indicator()
		ENDIF
	
        Delete_Joblist_CSInvite_In_Invitation_Slot(checkPosition)
        
        EXIT
    ENDIF
    
    // Hide the invite if a restriction is in place
    #IF IS_DEBUG_BUILD
        BOOL DEBUG_hasFocusMission      = FALSE
        BOOL DEBUG_onAmbientTutorial    = FALSE
		BOOL DEBUG_spectating			= FALSE
		BOOL DEBUG_mocapCutscene		= FALSE
		BOOL DEBUG_phoneDisabledSP		= FALSE
    #ENDIF
    
    BOOL    displayInvite               = TRUE
	BOOL    inviteBlockedByFocusMission	= FALSE
	BOOL    inviteBlockedForOtherReason = FALSE
    
    // Hide the invite if the MissionAtCoords system already has a Focus Mission (MP only)
    // NOTE: A player gets reserved for the Focus Mission, but there are a few frames when the reservation request is being sent over the network
    IF (NETWORK_IS_GAME_IN_PROGRESS())
        IF (Is_There_A_MissionsAtCoords_Focus_Mission())
            displayInvite               = FALSE
			inviteBlockedByFocusMission	= TRUE

            #IF IS_DEBUG_BUILD
                DEBUG_hasFocusMission   = TRUE
            #ENDIF
        ENDIF
    ENDIF
    
    // Hide the invite if the player is on an ambient tutorial mission (MP only)
    IF (NETWORK_IS_GAME_IN_PROGRESS())
        IF (IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL())
            displayInvite               = FALSE
			inviteBlockedForOtherReason = TRUE

            #IF IS_DEBUG_BUILD
                DEBUG_onAmbientTutorial = TRUE
            #ENDIF
        ENDIF
    ENDIF
	
	// Hide if the player is spectating
    IF (NETWORK_IS_GAME_IN_PROGRESS())
		IF (IS_A_SPECTATOR_CAM_ACTIVE())
	        displayInvite = FALSE
			inviteBlockedForOtherReason = TRUE

	        #IF IS_DEBUG_BUILD
	            DEBUG_spectating = TRUE
	        #ENDIF
		ENDIF
	ENDIF
	
	// Hide if a Mocap Cutscene is playing
    IF (IS_CUTSCENE_PLAYING())
		displayInvite = FALSE
		inviteBlockedForOtherReason = TRUE

		#IF IS_DEBUG_BUILD
			DEBUG_mocapCutscene = TRUE
		#ENDIF
	ENDIF
	
	// Hide in SP if the cellphone is disabled
    IF NOT (NETWORK_IS_GAME_IN_PROGRESS())
        IF (IS_CELLPHONE_DISABLED_OR_DISABLED_THIS_FRAME_ONLY())
            displayInvite               = FALSE
			inviteBlockedForOtherReason = TRUE

            #IF IS_DEBUG_BUILD
                DEBUG_phoneDisabledSP   = TRUE
            #ENDIF
        ENDIF
    ENDIF
    
    // Check if the display status has changed
    IF (g_sJLCSInvitesMP[checkPosition].jcsiAllowDisplay != displayInvite)
        // The display flag has changed
        g_sJLCSInvitesMP[checkPosition].jcsiAllowDisplay = displayInvite
        
        #IF IS_DEBUG_BUILD
            IF (displayInvite)
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("Cross-Session Invite is allowed to be displayed on the joblist: ")
            ELSE
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("Cross-Session Invite is not allowed to be displayed on Joblist")
                IF (DEBUG_hasFocusMission)
                    NET_PRINT(" [PLAYER HAS A FOCUS MISSION]")
                ENDIF
                IF (DEBUG_onAmbientTutorial)
                    NET_PRINT(" [PLAYER ON AMBIENT TUTORIAL]")
                ENDIF
				IF (DEBUG_spectating)
					 NET_PRINT(" [SPECTATING]")
				ENDIF
				IF (DEBUG_mocapCutscene)
					 NET_PRINT(" [MOCAP]")
				ENDIF
				IF (DEBUG_phoneDisabledSP)
					NET_PRINT("  [PHONE DISABLED IN SP]")
				ENDIF
                NET_PRINT(": ")
            ENDIF
            NET_PRINT_INT(checkPosition)
            NET_PRINT(" - ")
            NET_PRINT(GET_STRING_FROM_TEXT_FILE(Get_FM_Joblist_Activity_TextLabel(g_sJLCSInvitesMP[checkPosition].jcsiType, g_sJLCSInvitesMP[checkPosition].jcsiSubtype,g_sJLRVInvitesMP[checkPosition].jrviPropertyType, g_sJLRVInvitesMP[checkPosition].jrviRootId)))
            NET_PRINT("   [From Invitor: ")
			TEXT_LABEL_63 displayName = Get_CSInvite_Name_For_Display(checkPosition)
			NET_PRINT(displayName)
            NET_PRINT("]")
            NET_NL()
        #ENDIF
    ENDIF
	
	// If the Joblist App is active then set the invite as 'read'
    IF NOT (g_sJLCSInvitesMP[checkPosition].jcsiInviteRead)
        IF (Is_Joblist_App_On_Screen())
            g_sJLCSInvitesMP[checkPosition].jcsiInviteRead = TRUE
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("CS Invite from player has now been listed on Joblist - marking as READ: ")
                NET_PRINT_INT(checkPosition)
                NET_PRINT("   [")
				TEXT_LABEL_63 displayName = Get_CSInvite_Name_For_Display(checkPosition)
				NET_PRINT(displayName)
			    NET_PRINT("]")
                NET_NL()
            #ENDIF
            
            // KGM 14/7/13: This is quick and dirty way to reset new invites indicator by checking if the app is on screen, even if this invite isn't on display
            //              - the indicator isn't working correctly yet
            //Reset_New_Invites_Indicator()
        ENDIF
    ENDIF
	
    // Is the Joblist CSInvite already active?
    IF (g_sJLCSInvitesMP[checkPosition].jcsiActive)
		// If the Invite is active, check if a reminder is required
		Send_Reminder_CSInvite_Feed_Message_If_Required(checkPosition)
		
        EXIT
    ENDIF
    
    // KGM 24/8/13: Don't send invites if post-mission cleanup is active
    IF (IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID()))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Delaying Cross-Session Invite display on Feed - Post Mission Cleanup is still active")
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // KGM 24/8/13: Don't send invites during a transition to the betting screens, or a restart random transition
    IF (IS_TRANSITION_SESSION_RESTARTING())
    OR (HAS_TRANSITION_SESSION_SELECTED_RESTART_RANDOM())
	OR (IS_A_STRAND_MISSION_BEING_INITIALISED())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Delaying Cross-Session Invite display on Feed - Transition Session Restarting, or doing Restart Random")
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
	
	// KGM 9/6/14: BUG 1877275 - Feed message appeared during skycam transition back to Freemode.
	IF (Is_There_An_Active_Player_Warp_Delay())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Delaying Cross-Session Invite display on Feed - Post-player warp delay still active")
            NET_NL()
        #ENDIF
        
        EXIT
	ENDIF
    
    // Don't send feed message if the invite can't currently be displayed
	// KGM 4/12/14 [BUG 2135305]: If the invite is blocked only because the player has a focus mission and is in a corona then display the invite indicator and help message
	// KGM 19/1/15 [BUG 2199086]: Amendment to the above rule - don't display if performing a planning board tutorial (which is a series of help messages that would get interrupted by this one)
    IF NOT (g_sJLCSInvitesMP[checkPosition].jcsiAllowDisplay)
		IF NOT (IS_PC_VERSION())
			IF NOT (g_forceDisplayPlayerInvitesIndicator)
				IF (inviteBlockedByFocusMission)
				AND NOT (inviteBlockedForOtherReason)
					// NOTE: This checks for the prep of finale Board doing a tutorial sequence
					IF NOT (GET_HEIST_HELP_SEQUENCE_IN_PROGRESS())
					AND NOT (IS_HELP_MESSAGE_BEING_DISPLAYED())
						// Invite is only blocked by having a Focus Mission
						IF (IS_PLAYER_IN_CORONA())
							// ...and player is in a corona, so display indicator and help message
							#IF IS_DEBUG_BUILD
					            NET_PRINT("...KGM MP [JobList]: ")
					            NET_PRINT_TIME()
					            NET_PRINT("Cross-Session Invite Blocked only by Focus Mission and Player in Corona.")
					            NET_NL()
					            NET_PRINT("...KGM MP [JobList]: Set g_forceDisplayPlayerInvitesIndicator = TRUE")
					            NET_NL()
					            NET_PRINT("...KGM MP [JobList]: Set g_displayCoronaInviteHelp = TRUE")
					            NET_NL()
							#ENDIF
							
							//PRINT_HELP("CORONA_INVITES")
							g_forceDisplayPlayerInvitesIndicator	= TRUE
							g_displayCoronaInviteHelp				= TRUE
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
				            NET_PRINT("...KGM MP [JobList]: ")
				            NET_PRINT_TIME()
				            NET_PRINT("Cross-Session Invite received while Player in Corona ")
							IF (GET_HEIST_HELP_SEQUENCE_IN_PROGRESS())
								NET_PRINT("BUT planning board help tutorial active. Delay displaying invite indicator.")
							ELSE
								NET_PRINT("BUT help text on display. Delay displaying invite indicator.")
							ENDIF
							NET_NL()
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
        EXIT
    ENDIF
    
    // Activate the Invite
    g_sJLCSInvitesMP[checkPosition].jcsiActive  = TRUE
    
    // Display this Cross-Session Invite in the feed
    Display_CSInvite_As_Feed_Message(checkPosition)
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Displaying Cross-Session Invite In Feed and On Joblist: ")
        NET_PRINT_INT(checkPosition)
        NET_PRINT(" - ")
        NET_PRINT(GET_STRING_FROM_TEXT_FILE(Get_FM_Joblist_Activity_TextLabel(g_sJLCSInvitesMP[checkPosition].jcsiType, g_sJLCSInvitesMP[checkPosition].jcsiSubtype,g_sJLRVInvitesMP[checkPosition].jrviPropertyType, g_sJLRVInvitesMP[checkPosition].jrviRootId)))
        NET_PRINT("   [From Invitor: ")
        NET_PRINT(g_sJLCSInvitesMP[checkPosition].jcsiInvitorGamerTag)
        NET_PRINT("]")
		IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJLCSInvitesMP[checkPosition].jcsiInvitorDisplayName))
	        NET_PRINT("   [Display Name: ")
	        NET_PRINT(g_sJLCSInvitesMP[checkPosition].jcsiInvitorDisplayName)
	        NET_PRINT("]")
		ENDIF
        NET_PRINT(" [Feed ID: ")
        NET_PRINT_INT(g_sJLCSInvitesMP[checkPosition].jcsiFeedID)
        NET_PRINT("]")
        NET_NL()
    #ENDIF
    
ENDPROC




// ===========================================================================================================
//      JobList Return Value (Basic) Invite Storage functions
//      Invitations are stored on an array and as many are added to the joblist as there is room for
//      The newest entries are always at the start of the array and there should be one entry max per player
// ===========================================================================================================

// PURPOSE: Find the Basic Invite slot containing an Invitation from this Player
//
// INPUT PARAMS:        paramPlayerID           The PlayerID being searched for
// RETURN VALUE:        INT                     The Slot containing this player's invitation, or NO_JOBLIST_ARRAY_ENTRY if none
FUNC INT Get_Joblist_RVInvite_Slot_For_Player(PLAYER_INDEX paramPlayerID)

    INT tempLoop = 0
    REPEAT g_numMPRVInvites tempLoop
        IF (g_sJLRVInvitesMP[tempLoop].jrviInvitorIsPlayer)
            IF (g_sJLRVInvitesMP[tempLoop].jrviPlayer = paramPlayerID)
                // Found an invitation by the selected player
                RETURN tempLoop
            ENDIF
        ENDIF
    ENDREPEAT
    
    // No invitation by selected player
    RETURN NO_JOBLIST_ARRAY_ENTRY

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Find the Basic Invite slot containing an Invitation from a specific In-Game Character
//
// INPUT PARAMS:        paramCharacterID        The Character Enum being searched for
// RETURN VALUE:        INT                     The Slot containing this character's invitation, or NO_JOBLIST_ARRAY_ENTRY if none
FUNC INT Get_Joblist_RVInvite_Slot_For_NPC(enumCharacterList paramCharacterID)

    INT tempLoop = 0
    REPEAT g_numMPRVInvites tempLoop
        IF NOT (g_sJLRVInvitesMP[tempLoop].jrviInvitorIsPlayer)
            IF (g_sJLRVInvitesMP[tempLoop].jrviNPC = paramCharacterID)
                // Found an invitation by the selected in-game character
                RETURN tempLoop
            ENDIF
        ENDIF
    ENDREPEAT
    
    // No invitation by selected in-game character
    RETURN NO_JOBLIST_ARRAY_ENTRY

ENDFUNC

FUNC INT Get_Joblist_RVInvite_Slot_For_NPC_With_Type(enumCharacterList paramInvitorNPC, INT paramType)
    INT tempLoop = 0
    REPEAT g_numMPRVInvites tempLoop
        IF NOT (g_sJLRVInvitesMP[tempLoop].jrviInvitorIsPlayer)
            IF (g_sJLRVInvitesMP[tempLoop].jrviNPC = paramInvitorNPC)
			AND g_sJLRVInvitesMP[tempLoop].jrviType = paramType
                // Found an invitation by the selected in-game character
                RETURN tempLoop
            ENDIF
        ENDIF
    ENDREPEAT
    
    // No invitation by selected in-game character
    RETURN NO_JOBLIST_ARRAY_ENTRY

ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Delete the entry in the specified Basic Invite slot
//
// INPUT PARAMS:        paramSlot               The slot within the Basic Invite Array that needs to be deleted - all entries after it in the array will be shuffled up
PROC Delete_Joblist_RVInvite_In_Invitation_Slot(INT paramSlot)

    // Ensure the passed-in slot is valid
    IF (paramSlot >= g_numMPRVInvites)
        // ...slot isn't valid
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Basic Invite Entry Slot Invalid. Slot to be deleted: ")
            NET_PRINT_INT(paramSlot)
            NET_PRINT(" [Current Slots In Use: ")
            NET_PRINT_INT(g_numMPRVInvites)
            NET_PRINT("]")
            NET_NL()
        #ENDIF

        EXIT
    ENDIF
    
    // Slot is valid, so delete the feed entry if it exists
    IF (g_sJLRVInvitesMP[paramSlot].jrviFeedID != NO_JOBLIST_INVITE_FEED_ID)
        THEFEED_REMOVE_ITEM(g_sJLRVInvitesMP[paramSlot].jrviFeedID)
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Requested removal of Feed Entry if on display: ")
            NET_PRINT_INT(g_sJLRVInvitesMP[paramSlot].jrviFeedID)
            NET_NL()
        #ENDIF
    ENDIF
    
    // Move all entries after this in the array down one place to cover it
    INT toSlot = paramSlot
    INT fromSlot = toSlot + 1
    
    WHILE (fromSlot < g_numMPRVInvites)
        g_sJLRVInvitesMP[toSlot]    = g_sJLRVInvitesMP[fromSlot]
        g_sJLRVInvitesMP[toSlot] = g_sJLRVInvitesMP[fromSlot]
        toSlot++
        fromSlot++
    ENDWHILE
    
    // Need to clear the last slot that used to contain valid data
    Clear_One_RVInvite_Array_Slot(toSlot)
    
    // Need to reduce the number of entries in the array
    g_numMPRVInvites--
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Basic Invite Entry Deleted In Slot: ")
        NET_PRINT_INT(paramSlot)
        NET_NL()
    #ENDIF
	
	// KGM 27/3/14: If the 'new invite' flag is set and the 'num new invites' indicator is greater than 0 then clear the 'new invite' flag
	//				This isn't entirely accurate but that flag is only needed at the start of the game for a help message from DaveW, so
	//				it's better for the help to be delayed temporarily rather than, as can happen just now, it gets displayed but the
	//				invite has already been deleted
	IF (g_joblistInviteReceived)
		IF (g_numMPJobListEntries > 0)
			PRINTLN("...KGM MP [Joblist]: Clearing 'Joblist Invite Received' flag because a Basic Invite Entry has been deleted")
			Clear_New_Invite_Received()
		ENDIF
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set the Basic Invite as 'accepted' so that the calling script can be informed
//
// INPUT PARAMS:        paramSlot               The slot within the Basic Invite Array that needs to be deleted - all entries after it in the array will be shuffled up
PROC Set_Joblist_RVInvite_As_Accepted_By_Player(INT paramSlot)

    // Ensure the passed-in slot is valid
    IF (paramSlot >= g_numMPRVInvites)
        // ...slot isn't valid
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Basic Invite Entry Slot Invalid. Invite Slot Being Accepted: ")
            NET_PRINT_INT(paramSlot)
            NET_PRINT(" [Current Slots In Use: ")
            NET_PRINT_INT(g_numMPRVInvites)
            NET_PRINT("]")
            NET_NL()
        #ENDIF

        EXIT
    ENDIF
    
    // Slot is valid
    // Ignore if the Basic Invite has already been accepted
    IF (g_sJLRVInvitesMP[paramSlot].jrviAccepted)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("ERROR: Set_Joblist_RVInvite_As_Accepted_By_Player() - Basic Invite has already been accepted") NET_NL()
        #ENDIF
        
        EXIT
    ENDIF

    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Basic Invite accepted. Slot: ")
        NET_PRINT_INT(paramSlot)
        NET_PRINT(" - ")
        NET_PRINT(GET_STRING_FROM_TEXT_FILE(Get_FM_Joblist_Activity_TextLabel(g_sJLRVInvitesMP[paramSlot].jrviType, g_sJLRVInvitesMP[paramSlot].jrviSubType,g_sJLRVInvitesMP[paramSlot].jrviPropertyType, g_sJLRVInvitesMP[paramSlot].jrviRootId)))
        NET_NL()
    #ENDIF
    
    // Set the joblist invite as accepted
    g_sJLRVInvitesMP[paramSlot].jrviAccepted = TRUE
    
    // Kill the feed message if it is still on display
    THEFEED_REMOVE_ITEM(g_sJLRVInvitesMP[paramSlot].jrviFeedID)
	Joblist_Remove_All_Invite_Feed_Messages()
    
    // Don't auto-launch the joblist next time the cellphone is brought up
    Cancel_Joblist_App_Autolaunch()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check the validity of the invitations on the Joblist Invitations list, clean up if any are no longer valid, and maintain the headshots of those that are still valid
// NOTES:   This will be scheduled so it will only check one per frame
PROC Maintain_Joblist_RVInvites()

    // Are there any invitations?
    IF (g_numMPRVInvites = 0)
        // ...no
        EXIT
    ENDIF
    
    // There are invitations, so maintain one each frame
    IF (g_nextJoblistRVInviteCheckSlot >= g_numMPRVInvites)
        // ...wrap
        g_nextJoblistRVInviteCheckSlot = 0
    ENDIF
    
    // Check the invitation at the 'next check' array position
    INT         checkPosition       = g_nextJoblistRVInviteCheckSlot
    BOOL        stillValid          = TRUE
    MP_GAMEMODE thisGameMode        = GET_CURRENT_GAMEMODE()
    BOOL        inModeFM            = (thisGameMode = GAMEMODE_FM)
    BOOL        invitorIsPlayer     = g_sJLRVInvitesMP[checkPosition].jrviInvitorIsPlayer
    
    // The check position is stored, so always increase the check position for next frame
    g_nextJoblistRVInviteCheckSlot++
    
    // Ignore this Basic Invite if the asynchonous block check is still waiting for a result
    IF (g_sJLRVInvitesMP[checkPosition].jrviDoingPermissionCheck)
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [JobList]: Delay Basic Invite Processing - still waiting for asynchonous Block check. CheckID: ", g_sJLRVInvitesMP[checkPosition].jrviPermissionCheckID)
		#ENDIF
		
        EXIT
    ENDIF
    
    // Is the inviting player still ok?
    IF (invitorIsPlayer)
        IF NOT (IS_NET_PLAYER_OK(g_sJLRVInvitesMP[checkPosition].jrviPlayer, FALSE))
            // ...not ok
            stillValid = FALSE
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("Cleanup Basic Invite. Inviting Player is not ok. Slot: ")
                NET_PRINT_INT(checkPosition)
                NET_NL()
            #ENDIF
        ENDIF
    ENDIF
    
    // Is the inviting player on mission now?
    // KGM TEMP: Use different checks for different gamemodes, hopefully this will be unified, but invites don't currently occur in CnC
    IF (invitorIsPlayer)
        IF (stillValid)
            IF (inModeFM)
                // Freemode - this will change to use Is_Player_On_Or_Triggering_Any_Mission() once everything goes through mission controller
                IF (IS_PLAYER_ON_ANY_FM_MISSION(g_sJLRVInvitesMP[checkPosition].jrviPlayer, FALSE))
                AND g_sJLRVInvitesMP[checkPosition].jrviType != FMMC_TYPE_RACE_TO_POINT     //RLB 14/08/13: This is so that Impromptu Race invites are displayed and selectable when on Missions.
                    stillValid = FALSE
            
                    #IF IS_DEBUG_BUILD
                        NET_PRINT("...KGM MP [JobList]: ")
                        NET_PRINT_TIME()
                        NET_PRINT("Cleanup Basic Invite. Inviting Player is now on an FM mission. Slot: ")
                        NET_PRINT_INT(checkPosition)
                        NET_PRINT("   [Invite from: ")
                        NET_PRINT(GET_PLAYER_NAME(g_sJLRVInvitesMP[checkPosition].jrviPlayer))
                        NET_PRINT("]")
                        NET_NL()
                    #ENDIF
                ENDIF
            ELSE
                // Not Freemode (invites not sent from any other mode)
                stillValid = FALSE
            ENDIF
        ENDIF
    ENDIF
    
    // Clean up an invite from an in-game character if this player is on a mission
    IF NOT (invitorIsPlayer)
        IF (stillValid)
            IF (Is_Player_On_Or_Triggering_Any_Mission())
            AND g_sJLRVInvitesMP[checkPosition].jrviType != FMMC_TYPE_RACE_TO_POINT     //RLB 14/08/13: This is so that Impromptu Race invites are displayed and selectable when on Missions.
               stillValid = FALSE
        
                #IF IS_DEBUG_BUILD
                    NET_PRINT("...KGM MP [JobList]: ")
                    NET_PRINT_TIME()
                    NET_PRINT("Cleanup Basic Invite from NPC. This Player is now on a mission. Slot: ")
                    NET_PRINT_INT(checkPosition)
                    NET_PRINT("   [Invite from: ")
                    
                    TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLRVInvitesMP[checkPosition].jrviNPC)
                    NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
                    NET_PRINT("]")
                    NET_NL()
                #ENDIF
            ENDIF
        ENDIF
    ENDIF
	
	// KGM 12/7/14: If the Basic Invite has been asynchronously blocked then it is not still valid
	// NOTE: This means the Basic Invite is from a player that this player has blocked
     IF (invitorIsPlayer)
        IF (stillValid)
			IF (g_sJLRVInvitesMP[checkPosition].jrviPermissionCheckFail)
                // ...the invite is from a player that is blocked, so clean it up and let teh invitor know
                stillValid = FALSE
    
                #IF IS_DEBUG_BUILD
                    NET_PRINT("...KGM MP [JobList]: ")
                    NET_PRINT_TIME()
                    NET_PRINT("Cleanup Basic Invite. Basic Invite has failed the asynchonous block check so is from a player that this player has blocked.")
                    NET_NL()
                #ENDIF
			ENDIF
		ENDIF
	ENDIF
    
    // Cleanup an invite if it has been requested closed by script and the 'display CLOSED' timer has expired
    IF (stillValid)
        IF (g_sJLRVInvitesMP[checkPosition].jrviRequestClosed)
            IF (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sJLRVInvitesMP[checkPosition].jrviClosedTimeout))
                // ...timer has expired
                stillValid = FALSE
        
                #IF IS_DEBUG_BUILD
                    NET_PRINT("...KGM MP [JobList]: ")
                    NET_PRINT_TIME()
                    NET_PRINT("Cleanup Basic Invite. It was requested to be closed and the timer has now elapsed. Slot: ")
                    NET_PRINT_INT(checkPosition)
                    NET_PRINT("   [Invite from: ")
                    
                    IF (invitorIsPlayer)
                        NET_PRINT(GET_PLAYER_NAME(g_sJLRVInvitesMP[checkPosition].jrviPlayer))
                    ELSE
                        TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLRVInvitesMP[checkPosition].jrviNPC)
                        NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
                    ENDIF
                    NET_PRINT("]")
                    NET_NL()
                #ENDIF
            ENDIF
        ENDIF
    ENDIF
    
    // Cleanup?
    IF NOT (stillValid)
		// MMM 1695633 - Check if the invite that is no longer valid has been read, 
		// if it hasn't we need to decrement the indicator		
		IF NOT (g_sJLRVInvitesMP[checkPosition].jrviInviteRead) AND g_sJLRVInvitesMP[checkPosition].jrviActive
			NET_PRINT("[MMM][JobList] Basic Invite is being cleaned up. Decrementing Invite Counter.")
        	NET_NL()
			g_sJLRVInvitesMP[checkPosition].jrviInviteRead = TRUE	// Probably not needed, but just incase	
			Decrement_New_Invites_Indicator()
		ENDIF
        // ...Invite needs to be cleaned up
        Delete_Joblist_RVInvite_In_Invitation_Slot(checkPosition)
        EXIT
    ENDIF
    
    // The Invite is still valid
    // Hide the invite if a restriction is in place
    #IF IS_DEBUG_BUILD
        BOOL DEBUG_hasFocusMission      = FALSE
// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
//        BOOL DEBUG_hasWantedLevel       = FALSE
        BOOL DEBUG_onAmbientTutorial    = FALSE
        BOOL DEBUG_isMarkedAsClosed     = FALSE
		BOOL DEBUG_spectating			= FALSE
		BOOL DEBUG_mocapCutscene		= FALSE
		BOOL DEBUG_onFmMission			= FALSE
		BOOL DEBUG_apartmentBlocked		= FALSE
    #ENDIF
    
    BOOL displayInvite = TRUE
    
    // Hide the invite if the MissionAtCoords system already has a Focus Mission
    // NOTE: A player gets reserved for the Focus Mission, but there are a few frames when the reservation request is being sent over the network
    IF (Is_There_A_MissionsAtCoords_Focus_Mission())
    AND g_sJLRVInvitesMP[checkPosition].jrviType != FMMC_TYPE_RACE_TO_POINT     //RLB 14/08/13: This is so that Impromptu Race invites are displayed and selectable when on Missions.
        displayInvite = FALSE

        #IF IS_DEBUG_BUILD
            DEBUG_hasFocusMission   = TRUE
        #ENDIF
    ENDIF
    
// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
//    // For a Basic Invite from a player, still send it if the receivor has a wanted level
//    IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
//        IF NOT (invitorIsPlayer)
//            // Basic Invites from NPC's (not players) should be hidden
//            displayInvite = FALSE
//        ENDIF
//
//        #IF IS_DEBUG_BUILD
//            DEBUG_hasWantedLevel    = TRUE
//        #ENDIF
//    ENDIF
    
    // Hide the invite in some situations if the player is on an ambient tutorial mission
	IF (IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL())
	    IF (invitorIsPlayer)
			// Invitor is player, so block unless Impromptu Race
		    IF g_sJLRVInvitesMP[checkPosition].jrviType != FMMC_TYPE_RACE_TO_POINT
	            displayInvite = FALSE

	            #IF IS_DEBUG_BUILD
	                DEBUG_onAmbientTutorial = TRUE
	            #ENDIF
			ENDIF
		ELSE
			// Invitor is not player, so block Heist Quick Invites
    		IF (g_sJLRVInvitesMP[checkPosition].jrviType = FMMC_TYPE_HEIST_PHONE_INVITE)
    		OR (g_sJLRVInvitesMP[checkPosition].jrviType = FMMC_TYPE_HEIST_PHONE_INVITE_2)
			OR (g_sJLRVInvitesMP[checkPosition].jrviType = FMMC_TYPE_VCM_QUICK_INVITE)
			OR (g_sJLRVInvitesMP[checkPosition].jrviType = FMMC_TYPE_IH_QUICK_INVITE)
	            displayInvite = FALSE

	            #IF IS_DEBUG_BUILD
	                DEBUG_onAmbientTutorial = TRUE
	            #ENDIF
			ENDIF
        ENDIF
    ENDIF
    
    // Hide the Heist Quick Invite if on an FM Mission (ie: 1-on-1 DM)
    IF (g_sJLRVInvitesMP[checkPosition].jrviType = FMMC_TYPE_HEIST_PHONE_INVITE)
    OR (g_sJLRVInvitesMP[checkPosition].jrviType = FMMC_TYPE_HEIST_PHONE_INVITE_2)
    OR (g_sJLRVInvitesMP[checkPosition].jrviType = FMMC_TYPE_VCM_QUICK_INVITE)
    OR (g_sJLRVInvitesMP[checkPosition].jrviType = FMMC_TYPE_IH_QUICK_INVITE)
        IF (IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID()))
            displayInvite = FALSE

            #IF IS_DEBUG_BUILD
                DEBUG_onFmMission = TRUE
            #ENDIF
        ENDIF
    ENDIF
	
	IF (g_sJLRVInvitesMP[checkPosition].jrviType = FMMC_TYPE_APARTMENT)
	OR (g_sJLRVInvitesMP[checkPosition].jrviType = FMMC_TYPE_SIMPLE_INTERIOR)
		IF IS_PLAYER_BLOCKED_FROM_PROPERTY()
			displayInvite = FALSE

            #IF IS_DEBUG_BUILD
                DEBUG_apartmentBlocked = TRUE
            #ENDIF
		ENDIF
	ENDIF
    
    // Hide the invite if the invite has been marked as closed (ignore timer - that is used to clean it up in another fucntion)
    IF (g_sJLRVInvitesMP[checkPosition].jrviRequestClosed)
        displayInvite = FALSE

        #IF IS_DEBUG_BUILD
            DEBUG_isMarkedAsClosed = TRUE
        #ENDIF
    ENDIF
	
	// Hide if the player is spectating
	IF (IS_A_SPECTATOR_CAM_ACTIVE())
        displayInvite = FALSE
//      inviteBlockedForOtherReason = TRUE

        #IF IS_DEBUG_BUILD
            DEBUG_spectating = TRUE
        #ENDIF
	ENDIF
	
	// Hide if a Mocap Cutscene is playing
	IF (IS_CUTSCENE_PLAYING())
        displayInvite = FALSE
//      inviteBlockedForOtherReason = TRUE

        #IF IS_DEBUG_BUILD
            DEBUG_mocapCutscene = TRUE
        #ENDIF
	ENDIF
    
    // Check if the display status has changed
    IF (g_sJLRVInvitesMP[checkPosition].jrviAllowDisplay != displayInvite)
        // The display flag has changed
        g_sJLRVInvitesMP[checkPosition].jrviAllowDisplay = displayInvite
        
        #IF IS_DEBUG_BUILD
            IF (displayInvite)
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("Basic Invite is allowed to be displayed on the joblist: ")
            ELSE
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("Basic Invite is not allowed to be displayed on Joblist")
                IF (DEBUG_hasFocusMission)
                    NET_PRINT(" [PLAYER HAS A FOCUS MISSION]")
                ENDIF
// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
//                IF (DEBUG_hasWantedLevel)
//                    NET_PRINT(" [PLAYER HAS A WANTED LEVEL")
//                    IF (invitorIsPlayer)
//                        NET_PRINT("- BUT IGNORE THIS FOR BASIC INVITES FROM PLAYERS")
//                    ENDIF
//                    NET_PRINT("]")
//                ENDIF
                IF (DEBUG_onAmbientTutorial)
                    NET_PRINT(" [PLAYER ON AMBIENT TUTORIAL]")
                ENDIF
                IF (DEBUG_onFmMission)
                    NET_PRINT(" [PLAYER ON FM MISSION]")
                ENDIF
                IF (DEBUG_isMarkedAsClosed)
                    NET_PRINT(" [INVITE REQUESTED CLOSED]")
                ENDIF
                IF (DEBUG_spectating)
                    NET_PRINT(" [SPECTATING]")
                ENDIF
                IF (DEBUG_mocapCutscene)
                    NET_PRINT(" [MOCAP]")
                ENDIF
				IF (DEBUG_apartmentBlocked)
					NET_PRINT(" [APARTMENT BLOCKED]")
				ENDIF
                NET_PRINT(": ")
            ENDIF
            NET_PRINT_INT(checkPosition)
            NET_PRINT(" - ")
            NET_PRINT(GET_STRING_FROM_TEXT_FILE(Get_FM_Joblist_Activity_TextLabel(g_sJLRVInvitesMP[checkPosition].jrviType, g_sJLRVInvitesMP[checkPosition].jrviSubType,g_sJLRVInvitesMP[checkPosition].jrviPropertyType, g_sJLRVInvitesMP[checkPosition].jrviRootId)))
            IF (invitorIsPlayer)
                NET_PRINT("   [From Player: ")
                NET_PRINT(GET_PLAYER_NAME(g_sJLRVInvitesMP[checkPosition].jrviPlayer))
            ELSE
                TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLRVInvitesMP[checkPosition].jrviNPC)
                
                NET_PRINT("   [From NPC: ")
                NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            ENDIF
            NET_PRINT("]")
            NET_NL()
        #ENDIF
    ENDIF
	
	// If the Joblist App is active then set the invite as 'read'
    IF NOT (g_sJLRVInvitesMP[checkPosition].jrviInviteRead)
        IF (Is_Joblist_App_On_Screen())
            g_sJLRVInvitesMP[checkPosition].jrviInviteRead = TRUE
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("RV Invite from player has now been listed on Joblist - marking as READ: ")
                NET_PRINT_INT(checkPosition)
                NET_PRINT("   [")
                IF (invitorIsPlayer)
                    NET_PRINT(GET_PLAYER_NAME(g_sJLRVInvitesMP[checkPosition].jrviPlayer))
                ELSE
                    TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLRVInvitesMP[checkPosition].jrviNPC)
                    NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
                ENDIF
                NET_PRINT("]")
                NET_NL()
            #ENDIF
            
            // KGM 14/7/13: This is quick and dirty way to reset new invites indicator by checking if the app is on screen, even if this invite isn't on display
            //              - the indicator isn't working correctly yet
            //Reset_New_Invites_Indicator()
        ENDIF
    ENDIF

    // Is the Joblist RVInvite already active?
    IF (g_sJLRVInvitesMP[checkPosition].jrviActive)
        EXIT
    ENDIF
    
    // Don't send feed message if the invite can't currently be displayed
    IF NOT (g_sJLRVInvitesMP[checkPosition].jrviAllowDisplay)
        EXIT
    ENDIF
	
	// KGM 7/11/14: BUG 2108567 - Feed message appeared during skycam into Freemode.
	IF (Is_There_An_Active_Player_Warp_Delay())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Ignoring Basic Invite Feed - Post-player warp delay still active")
            NET_NL()
        #ENDIF
        
        EXIT
	ENDIF
    
    // Activate the Invite
    g_sJLRVInvitesMP[checkPosition].jrviActive  = TRUE
    
    // Display this Basic Invite in the feed
    Display_RVInvite_As_Feed_Message(checkPosition)
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Displaying Basic Invite In Feed and On Joblist: ")
        NET_PRINT_INT(checkPosition)
        NET_PRINT(" - ")
        NET_PRINT(GET_STRING_FROM_TEXT_FILE(Get_FM_Joblist_Activity_TextLabel(g_sJLRVInvitesMP[checkPosition].jrviType, g_sJLRVInvitesMP[checkPosition].jrviSubType,g_sJLRVInvitesMP[checkPosition].jrviPropertyType, g_sJLRVInvitesMP[checkPosition].jrviRootId)))
        IF (invitorIsPlayer)
            NET_PRINT("   [From Player: ")
            NET_PRINT(GET_PLAYER_NAME(g_sJLRVInvitesMP[checkPosition].jrviPlayer))
        ELSE
            NET_PRINT("   [From NPC: ")
            TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLRVInvitesMP[checkPosition].jrviNPC)
            NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
        ENDIF
        NET_PRINT("]")
        NET_PRINT(" [Feed ID: ")
        NET_PRINT_INT(g_sJLRVInvitesMP[checkPosition].jrviFeedID)
        NET_PRINT("]")
        NET_NL()
    #ENDIF
        
ENDPROC





// ===========================================================================================================
//      JobList Access To Mission Control System
//      (Host Broadcast Data on Client Machine - so the data will be slightly out of date)
// ===========================================================================================================

// PURPOSE: Find the mission request slot containing the mission that this player is allocated to
//
// RETURN VALUE:            INT         The array position containing the mission, or FAILED_TO_FIND_MISSION if not allocated
FUNC INT Get_MP_Mission_Request_ArrayPos_Player_Is_Allocated_To()
    
    // This will try to find a mission that this player has been allocated to
    // NOTE: These checks are made against the local copy of the mission control host broadcast data so may be a bit out of date due to network lag
    INT tempLoop = 0
    REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
        IF NOT (Is_MP_Mission_Request_Slot_Empty(tempLoop))
            IF (Is_Player_Reserved_For_MP_Mission_Request_Slot(tempLoop, PLAYER_ID()))
            OR (Is_Player_Active_On_MP_Mission_Request_Slot(tempLoop, PLAYER_ID()))
            OR (Is_Player_Confirmed_For_MP_Mission_Request_Slot(tempLoop, PLAYER_ID()))
            OR (Is_Player_Joining_MP_Mission_Request_Slot(tempLoop, PLAYER_ID()))
                // Found a mission that this player is allocated to
                RETURN tempLoop
            ENDIF
        ENDIF
    ENDREPEAT
    
    // Player isn't allocated to any mission
// KGM 15/2/12: Comment out spam
//  #IF IS_DEBUG_BUILD
//      NET_PRINT("           Player isn't allocated to any mission") NET_NL()
//  #ENDIF
                
    RETURN FAILED_TO_FIND_MISSION
    
ENDFUNC



// ===========================================================================================================
//      JobList Maintenance Functions
// ===========================================================================================================

// PURPOSE: Fill the details of a JobList slot
//
// INPUT PARAMS:        paramState          The state of the joblist entry in this slot (eg: current mission, joinable mission, etc)
//                      paramUniqueID       The UniqueID used by the Mission Request system
//                      paramMissionID      The Mission ID
// RETURN VALUE:        BOOL                TRUE if the details were successfully stored, FALSE if not
//
// NOTES:   Assumes g_numMPJobListEntries points to the next empty array element.
//          g_numMPJobListEntries gets incremented if the data is successfully stored.
FUNC BOOL Add_Details_To_JobList(g_eMPJobListStates paramState, INT paramUniqueID, MP_MISSION paramMissionID)

    // Is there room in the array for another entry?
    IF (g_numMPJobListEntries >= MAX_MP_JOBLIST_ENTRIES)
        SCRIPT_ASSERT("Add_Details_To_JobList(): JobList was full - Tell Keith")
        RETURN FALSE
    ENDIF
    
    IF (paramMissionID = eNULL_MISSION)
        SCRIPT_ASSERT("Add_Details_To_JobList(): MissionID was eNULL_MISSION")
        RETURN FALSE
    ENDIF
    
    // Error checking with the new system
    IF (paramUniqueID = NO_UNIQUE_ID)
        SCRIPT_ASSERT("Add_Details_To_JobList(): UniqueID was NO_UNIQUE_ID")
        RETURN FALSE
    ENDIF
    
    // Store this entry
    g_sJobListMP[g_numMPJobListEntries].jlState         = paramState
    g_sJobListMP[g_numMPJobListEntries].jlMissionAsInt  = ENUM_TO_INT(paramMissionID)
	g_sJobListMP[g_numMPJobListEntries].jlSubtype		= -1
    g_sJobListMP[g_numMPJobListEntries].jlUniqueID      = paramUniqueID
    
    #IF IS_DEBUG_BUILD
//      NET_PRINT("           Added MP JobList Entry: Slot ")
//      NET_PRINT_INT(g_numMPJobListEntries)
//      SWITCH (paramState)
//          CASE MP_JOBLIST_CURRENT_MISSION
//              NET_PRINT(" [CURRENT]  ")
//              BREAK
//          CASE MP_JOBLIST_JOINABLE_MISSION
//              NET_PRINT(" [JOINABLE] ")
//              BREAK
//          CASE MP_JOBLIST_INVITATION
//              NET_PRINT(" [INVITATION] ")
//              BREAK
//          CASE NO_MP_JOBLIST_ENTRY
//              NET_PRINT(" [NONE ???] ")
//              BREAK
//          DEFAULT
//              NET_PRINT(" [UNKNOWN]  ")
//              BREAK
//      ENDSWITCH
//      
//      NET_PRINT(GET_MP_MISSION_NAME(paramMissionID))
//      
//      NET_PRINT(" [UNIQUEID = ") NET_PRINT_INT(paramUniqueID) NET_PRINT("]")
//      NET_NL()
    #ENDIF
    
    // Increment the number of joblist entries counter
    g_numMPJobListEntries++
    
    RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: To check if these details already exist in the FM Joblist array
//
// INPUT PARAMS:        paramPlayerGBD      The Player's FM GBD Array Slot
//                      paramFMMCType       The Type of FM activity
//                      paramCreatorID      The mission creator (player Index) as an INT
//                      paramBountyTarget   TRUE if the mission contains a Bounty Target, otherwise FALSE
// RETURN VALUE:        BOOL                TRUE if this data already exists, FALSE if it doesn't
//
// NOTES:   TEMP: When FM uses the Mission Controller then all this would be handled with the existing functions
FUNC BOOL Check_If_This_FM_Joblist_Entry_Exists_And_Update_Bitflags(INT paramPlayerGBD, INT paramFMMCType, INT paramCreatorID, BOOL paramBountyTarget)

    INT     tempLoop                = 0
    INT     checkPlayerGBD          = -1
    VECTOR  thisMissionLocation     = GlobalplayerBD_FM[paramPlayerGBD].currentMissionData.mdPrimaryCoords
    VECTOR  checkMissionLocation
    
    CONST_FLOAT     VECTOR_CHECK_DISTANCE_m     0.3
    
    REPEAT g_numMPJobListEntries tempLoop
        // Does this entry match all the passed in details?
        checkPlayerGBD = g_sJobListMP[tempLoop].jlUniqueID
        
        IF (g_sJobListMP[tempLoop].jlMissionAsInt = paramFMMCType)
        AND (g_sJobListMP[tempLoop].jlInstance = paramCreatorID)
            // ...all the passed-in details match, so check the start location
            checkMissionLocation = GlobalplayerBD_FM[checkPlayerGBD].currentMissionData.mdPrimaryCoords
            IF (GET_DISTANCE_BETWEEN_COORDS(thisMissionLocation, checkMissionLocation) <= VECTOR_CHECK_DISTANCE_m)
                // ...same mission, check if some flags need set
                // Bounty?
                IF (paramBountyTarget)
                    SET_BIT(g_sJobListMP[tempLoop].jlBitfield, JOBLIST_BITFLAG_CONTAINS_BOUNTY_TARGET)
                ENDIF
                
                // Not Joinable?
                // NOTE: This is because the not joinable flag is player Broadcast data - if any player on the mission has this flag set then the mission should not be joinable.
                //          There were bugs due to an inconsistency where the first player found on the mission had the flag as 'joinable' and so the mission was joinable,
                //          even though other players had the mission as not joinable. I'll now do a pass of the Joblist after creation and remove all 'not joinable' missions.
                IF (IS_BIT_SET(GlobalplayerBD_FM[paramPlayerGBD].currentMissionData.iBitSet, ciMissionIsNotJoinable))
                    SET_BIT(g_sJobListMP[tempLoop].jlBitfield, JOBLIST_BITFLAG_MISSION_NOT_JOINABLE)
                ENDIF
        
                RETURN TRUE
            ENDIF
        ENDIF
    ENDREPEAT
    
    // mission not in array
    RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: To check if a playlist already exists on the joblist array
// INPUT PARAMS:        paramPlayerGBD      The Player's FM GBD Array Slot
// RETURN VALUE:        BOOL                TRUE if this data already exists, FALSE if it doesn't
//
// NOTES:   HOPEFULLY TEMP: When FM uses the Mission Controller then all this would be handled with the existing functions
FUNC BOOL Check_If_This_FM_Joblist_Playlist_Exists_And_Update_Bitflags(INT paramPlayerGBD)

    INT     tempLoop            = 0
    INT     thisPlaylistID      = GlobalplayerBD_FM[paramPlayerGBD].sPlaylistVars.iPlayListInstance
    INT     checkPlaylistID     = 0
    
    REPEAT g_numMPJobListEntries tempLoop
        // Does this entry match an existing playlist ID?
        checkPlaylistID = g_sJobListMP[tempLoop].jlInstance
        
        IF (thisPlaylistID = checkPlaylistID)
            // ...same playlist and FM Mission Type, so return TRUE
            // NOTE: Ignoring BOUNTY and JOINABLE flags for now
            RETURN TRUE
        ENDIF
    ENDREPEAT
    
    // playlist not in array
    RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Add the FM Mission details to the Joblist
//
// INPUT PARAMS:        paramState          The state of the joblist entry in this slot (eg: current mission, joinable mission, etc)
//                      paramPlayerIDAsInt  The Player Index of this player as an INT (used for accessing the Bounty PlayerBits)
//                      paramPlayerGBD      The Player's FM GBD Array Slot (used for accessing the FM PlayerBD)
//                      paramFMMCType       The Type of FM activity
// RETURN VALUE:        BOOL                TRUE if this data is added to the array, FALSE if the data is already in the array or not being added
//
// NOTES:   TEMP: This forces the data to use the CnC variables - we should be able to remove this when FM uses Mission Controller
FUNC BOOL Add_Details_To_FM_JobList(g_eMPJobListStates paramState, INT paramPlayerIDAsInt, INT paramPlayerGBD, INT paramFMMCType)
    
    // Gather other details for this mission
    INT     theCreatorID        = GlobalplayerBD_FM[paramPlayerGBD].currentMissionData.mdID.idCreator
    INT     thePlaylistID       = GlobalplayerBD_FM[paramPlayerGBD].sPlaylistVars.iPlayListInstance
	
	// KGM 28/4/11 [BUG 2325536]: Time Trial is an ambient activity that can be cancelled through the joblist, so treat as 'minigame creator' as a workaround
	//		so that the current details get displayed
	IF (paramFMMCType = FMMC_TYPE_TIME_TRIAL)
	OR (paramFMMCType = FMMC_TYPE_SANDBOX_ACTIVITY)
	OR (paramFMMCType = FMMC_TYPE_HSW_TIME_TRIAL)
	OR (paramFMMCType = FMMC_TYPE_HSW_SETUP)
		theCreatorID = FMMC_MINI_GAME_CREATOR_ID
	ENDIF
    
    // KGM 16/11/12 - NO LONGER DISPLAYING 'BOUNTY' INDICATOR - THIS MAY BECOME FULLY OBSOLETE, NOT SURE YET
    paramPlayerIDAsInt = paramPlayerIDAsInt
    BOOL    aBountyTarget       = FALSE     // GlobalServerBD_FM.currentBounties[paramPlayerIDAsInt].bTargeted
    
    // KGM 29/11/12: This function is getting messy, but I'll try to tidy it up later
    BOOL isOnPlaylist = (paramState = MP_JOBLIST_JOINABLE_PLAYLIST)
    
    // Is this entry already in the array?
    IF (isOnPlaylist)
        IF (Check_If_This_FM_Joblist_Playlist_Exists_And_Update_Bitflags(paramPlayerGBD))
            // ...already exists, so return FALSE so that it doesn't get added to the array again
            RETURN FALSE
        ENDIF
    ELSE
        IF (Check_If_This_FM_Joblist_Entry_Exists_And_Update_Bitflags(paramPlayerGBD, paramFMMCType, theCreatorID, aBountyTarget))
            // ...already exists, so return FALSE so that it doesn't get added to the array again
            RETURN FALSE
        ENDIF
    ENDIF

    // Not in the array, so check if there is room in the array for another entry
    IF (g_numMPJobListEntries >= MAX_MP_JOBLIST_ENTRIES)
        SCRIPT_ASSERT("Add_Details_To_FM_JobList(): FM JobList was full of active missions - Tell Keith")
        RETURN FALSE
    ENDIF
    
    // There is room, so add it to the joblist
    IF (isOnPlaylist)
        // NOTE: The data is being forced into variables that are badly-named for FM Playlist, but this should be temporary
        //      uniqueID    :   being used for PLAYER FM GBD SLOT
        //      missionAsInt:   being used for FMMC TYPE
        //      instance    :   being used for Playlist Instance (AS INT)
        g_sJobListMP[g_numMPJobListEntries].jlState         = paramState
        g_sJobListMP[g_numMPJobListEntries].jlUniqueID      = paramPlayerGBD
        g_sJobListMP[g_numMPJobListEntries].jlMissionAsInt  = paramFMMCType
		g_sJobListMP[g_numMPJobListEntries].jlSubtype		= -1
        g_sJobListMP[g_numMPJobListEntries].jlInstance      = thePlaylistID
        
        // Store the current playlist mission name
        g_sJobListMP[g_numMPJobListEntries].jlMissionName   = GlobalplayerBD_MissionName[paramPlayerGBD].tl63MissionName
        
        // Check if the PlayList lobby flag needs set (this is used to ensure the joblist display gets refreshed when the player moves between lobby/activity and vice versa)
        IF (paramFMMCType = FMMC_TYPE_PLAYLIST)
            SET_BIT(g_sJobListMP[g_numMPJobListEntries].jlBitfield, JOBLIST_BITFLAG_ON_PLAYLIST_LOBBY)
        ENDIF
    ELSE
        // NOTE: The data is being forced into variables that are badly-named for FM, but this should be temporary
        //      uniqueID    :   being used for PLAYER FM GBD SLOT
        //      missionAsInt:   being used for FMMC TYPE
        //      instance    :   being used for CREATOR ID (AS INT)
        g_sJobListMP[g_numMPJobListEntries].jlState         = paramState
        g_sJobListMP[g_numMPJobListEntries].jlUniqueID      = paramPlayerGBD
        g_sJobListMP[g_numMPJobListEntries].jlMissionAsInt  = paramFMMCType
        g_sJobListMP[g_numMPJobListEntries].jlInstance      = theCreatorID
        g_sJobListMP[g_numMPJobListEntries].jlDescHash      = g_sCurrentMissionDesc.descHash        // The Mission Triggerer will have the current mission description available if there is one
		
		// ...and the subtype
		INT theSubtype = -1
		IF (paramState = MP_JOBLIST_CURRENT_MISSION)
			IF (Is_Player_On_Or_Triggering_Any_Mission())
				IF (GlobalplayerBD_FM[paramPlayerGBD].iCurrentMissionType = FMMC_TYPE_MISSION)
					theSubtype = g_FMMC_STRUCT.iMissionSubType
				ENDIF
			ELSE
				IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
					SWITCH paramFMMCType
						CASE FMMC_TYPE_SANDBOX_ACTIVITY
							theSubtype = GB_GET_CONTRABAND_VARIATION_PLAYER_IS_ON(PLAYER_ID())
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		ENDIF
		g_sJobListMP[g_numMPJobListEntries].jlSubtype	= theSubtype
        
		//If it's a mini game, then clear the property type and the root ID as we don't need them
        IF (theCreatorID = FMMC_MINI_GAME_CREATOR_ID)
			g_sJobListMP[g_numMPJobListEntries].jlPropertyType = 0
			g_sJobListMP[g_numMPJobListEntries].jlRootContentIdHash = 0
		ELSE
       	 // Only store the mission name if the creator ID is not MINIGAME (MiniGames have no mission name)
			IF NOT (IS_STRING_NULL_OR_EMPTY(g_sTriggerMP.mtMissionData.mdID.idCloudFilename))
	            g_sJobListMP[g_numMPJobListEntries].jlMissionName	= Get_Mission_Name_For_FM_Cloud_Loaded_Activity(g_sTriggerMP.mtMissionData.mdID)
	       	 ENDIF
        ENDIF
        
        // Check if the Bounty Target flag needs set
        IF (aBountyTarget)
            SET_BIT(g_sJobListMP[g_numMPJobListEntries].jlBitfield, JOBLIST_BITFLAG_CONTAINS_BOUNTY_TARGET)
        ENDIF
        
        // Check if the Not Joinable flag needs set
        IF (IS_BIT_SET(GlobalplayerBD_FM[paramPlayerGBD].currentMissionData.iBitSet, ciMissionIsNotJoinable))
            SET_BIT(g_sJobListMP[g_numMPJobListEntries].jlBitfield, JOBLIST_BITFLAG_MISSION_NOT_JOINABLE)
        ENDIF
    ENDIF
    
    // Increment the number of joblist entries counter
    g_numMPJobListEntries++
    
    RETURN TRUE
    
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Checks if the mission data is valid - this is a bit of a workaround script
//
// INPUT PARAMS:        paramPlayerID       Player Index of the player whose mission is being checked
// RETURN VALUE:        BOOL                TRUE if the player's mission data is valid, otherwise FALSE
//
// NOTES:   Workaround to overcome two issues:
//              the creatorID being invalid (because another player's data didn't arrive in time)
//              the location being 0,0,0 (this is disguising what may be a real issue)
FUNC BOOL Check_If_FM_Mission_Data_Is_Valid_For_Player(INT paramPlayerGBD)

    INT     theCreatorID            = GlobalplayerBD_FM[paramPlayerGBD].currentMissionData.mdID.idCreator
    VECTOR  thisMissionLocation     = GlobalplayerBD_FM[paramPlayerGBD].currentMissionData.mdPrimaryCoords
    VECTOR  originVector            = << 0.0, 0.0, 0.0 >>
    
    CONST_FLOAT     VECTOR_CHECK_DISTANCE_m     0.3
    
    IF (GET_DISTANCE_BETWEEN_COORDS(thisMissionLocation, originVector) <= VECTOR_CHECK_DISTANCE_m)
        // ...data not valid - mission at the origin
        RETURN FALSE
    ENDIF
    
    // Mission is not at the origin, so check if the creator ID is valid
    IF (theCreatorID < 0)
        // ...data not valid - creator ID is invalid
        RETURN FALSE
    ENDIF
    
    // Mission Data appears to be valid
    RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Fill the details of the mission the player is currently on
//
// INPUT PARAMS:        paramMissionSlot    The mission slot within the mission request array containing the mission that this player is allocated to
// RETURN VALUE:        BOOL                TRUE if mission details have been stored, otherwise FALSE
//
// NOTES:   Assumes the Player is OK.
//          JobList cancelling is currently not allowed.
FUNC BOOL Add_Current_Mission_Details_To_JobList(INT paramMissionSlot)
    
    // If the mission slot = FAILED_TO_FIND_MISSION then the player is not allocated to any mission
    IF (paramMissionSlot = FAILED_TO_FIND_MISSION)
        RETURN FALSE
    ENDIF
    
    // NOTE: These checks are made against the local copy of the mission control host broadcast data so may be a bit out of date due to network lag
    IF (Is_Player_Active_On_MP_Mission_Request_Slot(paramMissionSlot, PLAYER_ID()))
        // Player is active on this mission, (add it to the joblist if it can be cancelled), return TRUE
        #IF IS_DEBUG_BUILD
//          NET_PRINT("           Player is Active on this Mission Control Mission request slot: ") NET_PRINT_INT(paramMissionSlot) NET_NL()
        #ENDIF
        
        RETURN TRUE
    ENDIF
    
    // Player is not active on the mission, so return FALSE
    // NOTE: Still return FALSE if the player is joining the mission or is otherwise allocated to it - a new check in the calling routine will keep things correct
    #IF IS_DEBUG_BUILD
//      NET_PRINT("           Player is Allocated to but not Active on this Mission Control Mission request slot: ") NET_PRINT_INT(paramMissionSlot) NET_NL()
    #ENDIF
        
    RETURN FALSE
    
    // Currently on mission, so store the details - NOTE: Players currently can't cancel from the JobList
    //RETURN (Add_Details_To_JobList(MP_JOBLIST_CURRENT_MISSION, currentMission.mdID.idMission))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Fill the details of any missions that are joinable by the player
//
// RETURN VALUE:        BOOL        TRUE if mission details have been stored, otherwise FALSE
//
// NOTES:   Assumes the Player is OK.
//          Doesn't add details of the player's current mission.
FUNC BOOL Add_Joinable_Mission_Details_To_JobList()

    MP_MISSION      thisMissionID       = eNULL_MISSION                 // Used by the old Mission Launching routines
    INT             thisUniqueID        = NO_UNIQUE_ID                  // Used when the new Mission Control system is active
    BOOL            detailsStored       = FALSE
    INT             tempLoop            = 0

    // Get the details of the player's current mission so it can be excluded
//  MP_MISSION_DATA currentMission          = GET_PLAYER_MP_MISSION_DATA(PLAYER_ID())
//  MP_MISSION      currentMissionID        = currentMission.mdID.idMission
//  INT             currentInstanceID       = currentMission.iInstanceId
//  BOOL            thisIsPlayersMission    = FALSE
    
    // Get any player details needed
    INT             playerTeam          = GET_PLAYER_TEAM(PLAYER_ID())
    
    // The new mission control system is active, so find any missions that this player can join
    // NOTE: These checks are made against the local copy of the mission control host broadcast data so may be a bit out of date due to network lag
    g_eMPMissionStatus thisMissionStatus = NO_MISSION_REQUEST_RECEIVED
    BOOL possibleJoblistMission = FALSE
    
    #IF IS_DEBUG_BUILD
//      NET_PRINT("           Building List of Joinable Missions") NET_NL()
    #ENDIF
        
    REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
        thisMissionStatus = Get_MP_Mission_Request_Slot_Status(tempLoop)
        possibleJoblistMission = FALSE
        
        SWITCH (thisMissionStatus)
            // Check if these can 
            CASE MP_MISSION_STATE_ACTIVE
            CASE MP_MISSION_STATE_ACTIVE_SECONDARY
                possibleJoblistMission = TRUE
                BREAK
                
            // Ignore missions with these states - they can't be offered to the player at the moment
            CASE MP_MISSION_STATE_OFFERED
            CASE MP_MISSION_STATE_RECEIVED
            CASE MP_MISSION_STATE_RESERVED
            CASE NO_MISSION_REQUEST_RECEIVED
                possibleJoblistMission = FALSE
                BREAK
                
            DEFAULT
                SCRIPT_ASSERT("Add_Joinable_Mission_Details_To_JobList() - Unrecognised Mission Request State. Add new state to Joblist SWITCH function. Tell Keith.")
                possibleJoblistMission = FALSE
                BREAK
        ENDSWITCH
        
        IF (possibleJoblistMission)
            // Don't offer to the player if the player is excluded from the mission (but do still offer it if the player rejected it)
            IF NOT (Is_Player_Excluded_From_MP_Mission_Request_Slot(tempLoop, PLAYER_ID()))
                // ...not excluded, so check if the mission is joinable for this player's team
                IF (Is_Team_Joinable_For_Mission(tempLoop, playerTeam))
                OR (Does_MP_Mission_Request_Allow_Players_To_Join_Mission(tempLoop))
                    // ...player's team is joinable for this mission, so check if the mission is full for the player's team
                    IF NOT (Check_If_Mission_Or_Team_Full(tempLoop, playerTeam))
                        thisMissionID   = Get_MP_Mission_Request_MissionID(tempLoop)
                        thisUniqueID    = Get_MP_Mission_Request_UniqueID(tempLoop)
                        IF (Add_Details_To_JobList(MP_JOBLIST_JOINABLE_MISSION, thisUniqueID, thisMissionID))
                            // ...details successfully stored
                            detailsStored = TRUE
                        ELSE
                            #IF IS_DEBUG_BUILD
//                              NET_PRINT("           Slot ")
//                              NET_PRINT_INT(tempLoop)
//                              NET_PRINT(" - Potential Joblist Mission ")
//                              NET_PRINT("[FAILED TO ADD DETAILS TO JOBLIST]") NET_NL()
                            #ENDIF
                        ENDIF
                    ELSE
                        #IF IS_DEBUG_BUILD
//                          NET_PRINT("           Slot ")
//                          NET_PRINT_INT(tempLoop)
//                          NET_PRINT(" - Potential Joblist Mission ")
//                          NET_PRINT("[MISSION FULL FOR TEAM]") NET_NL()
                        #ENDIF
                    ENDIF
                ENDIF
            ELSE
                #IF IS_DEBUG_BUILD
//                  NET_PRINT("           Slot ")
//                  NET_PRINT_INT(tempLoop)
//                  NET_PRINT(" - Potential Joblist Mission ")
//                  NET_PRINT("[PLAYER EXCLUDED FROM MISSION]") NET_NL()
                #ENDIF
            ENDIF
        ENDIF
    ENDREPEAT
    
    // Return whether or not details Joinable missions were found and stored
    RETURN (detailsStored)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Checks if a specified FM missiontype allows players to quit mid-mission
//
// INPUT PARAMS:        paramMissionTypeFM      The FM Mission Type
// RETURN VALUE:        BOOL                    TRUE if the mission type can be quit mid-mission, otherwise FALSE
FUNC BOOL Can_This_FM_Mission_Type_Be_Quit_MidMission(INT paramMissionTypeFM)

    SWITCH (paramMissionTypeFM)
        // These can't be quit mid-mission
        CASE FMMC_TYPE_GANGHIDEOUT
//      CASE FMMC_TYPE_RACE_TO_POINT        // TEMP: Until I get this supported properly
//      CASE FMMC_TYPE_IMPROMPTU_DM         // TEMP: Until I get this supported properly
            RETURN FALSE
    ENDSWITCH
    
	IF GB_IS_GLOBAL_NON_BD_BIT1_SET(eGB_GLOBAL_NON_BD_BITSET_1_DISABLE_QUIT_MID_MISSION)
		RETURN FALSE
	ENDIF
	
    // All other types can be quit mid-mission
    RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Add the current FM mission details to the joblist
//
// REFERENCE PARAM:		refAllowRVInviteDetails		TRUE if RV Invite details are also allowed to be displayed along with this current invite
// RETURN VALUE:        BOOL        				TRUE if the player is on an FM mission (even if the mission details haven't been stored), otherwise FALSE
//
// NOTES:   TEMP - This function is only being used until FM uses the Mission Controller, so all variables are being mangled to fit into the existing CnC details struct
//			KGM 28/4/15 [BUG 2325536]: Add a return value to decide if RV Invite details are also allowed to be displayed with this current invite
FUNC BOOL Add_Current_FM_Mission_Details_To_Joblist(BOOL &refAllowRVInviteDetails)

	refAllowRVInviteDetails = TRUE
	
	PRINTLN("...KGM MP [JobList] Add_Current_FM_Mission_Details_To_Joblist")

    IF NOT  (IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID()))
    AND NOT (Is_Player_On_Or_Triggering_Any_Mission())
    AND NOT (IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_GANGHIDEOUT))
	
    AND NOT (IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL))
	AND NOT (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_SANDBOX_ACTIVITY)
	AND NOT (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_HSW_TIME_TRIAL)
	AND NOT (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_HSW_SETUP)
        RETURN FALSE
    ENDIF

    // Player is on an FM mission, so always return TRUE from now on - add the mission to the Joblist if possible
    
    // Ensure player is ok, otherwise don't add the mission to the joblist
    IF NOT (IS_NET_PLAYER_OK(PLAYER_ID()))
        RETURN TRUE
    ENDIF

    // Ensure player is on GBD array, otherwise don't add the mission to the joblist
    IF NOT (IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID()))
        RETURN TRUE
    ENDIF
	
	
//  // Don't display the mission if the player is on a playlist - this is all lobby controlled
//  IF (IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID()))
//      RETURN TRUE
//  ENDIF

    // Gather the details
    INT     playerGBD       = NATIVE_TO_INT(PLAYER_ID())
    INT     fmmcType        = GlobalplayerBD_FM[playerGBD].iCurrentMissionType
    INT     playerIdAsInt   = NATIVE_TO_INT(PLAYER_ID())
    
    // Can this mission be cancelled using the phone?
    IF NOT (Can_This_FM_Mission_Type_Be_Quit_MidMission(fmmcType))
	PRINTLN("...KGM MP [JobList] Add_Current_FM_Mission_Details_To_Joblist - Can_This_FM_Mission_Type_Be_Quit_MidMission")
        RETURN TRUE
    ENDIF

	// KGM 28/4/15 [BUG 2325536]: Deal with Time Trial Ambient Activity as a special case 'current mission' that can be terminated
	IF (IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_TIME_TRIAL))
	    Add_Details_To_FM_JobList(MP_JOBLIST_CURRENT_MISSION, playerIdAsInt, playerGBD, FMMC_TYPE_TIME_TRIAL)
		refAllowRVInviteDetails = FALSE
	    RETURN TRUE
	ELSE
		INT iFMMCType = GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID())
		PRINTLN("...KGM MP [JobList] Add_Current_FM_Mission_Details_To_Joblist - GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION")
		
		SWITCH iFMMCType
			CASE FMMC_TYPE_SANDBOX_ACTIVITY
			CASE FMMC_TYPE_HSW_TIME_TRIAL
			CASE FMMC_TYPE_HSW_SETUP
				Add_Details_To_FM_JobList(MP_JOBLIST_CURRENT_MISSION, playerIdAsInt, playerGBD, iFMMCType)
				refAllowRVInviteDetails = FALSE
				
				RETURN TRUE
		ENDSWITCH
	ENDIF
	PRINTLN("...KGM MP [JobList] Add_Current_FM_Mission_Details_To_Joblist - Add_Details_To_FM_JobList")

    // Store the details - ignore the return value, we'll be returning TRUE whether or not the details were stored
    Add_Details_To_FM_JobList(MP_JOBLIST_CURRENT_MISSION, playerIdAsInt, playerGBD, fmmcType)
    RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Checks if a specified FM missiontype allows players to join mid-mission
//
// INPUT PARAMS:        paramMissionTypeFM      The FM Mission Type
// RETURN VALUE:        BOOL                    TRUE if the mission type can be joined mid-mission, otherwise FALSE
FUNC BOOL Can_This_FM_Mission_Type_Be_Joined_MidMission(INT paramMissionTypeFM)

    SWITCH (paramMissionTypeFM)
        // These can be joined mid-mission
//      CASE FMMC_TYPE_DEATHMATCH
//      CASE FMMC_TYPE_MISSION
//      CASE FMMC_TYPE_RACE
        CASE FMMC_TYPE_GANGHIDEOUT
//      CASE FMMC_TYPE_SURVIVAL
            RETURN TRUE
    ENDSWITCH
    
    // All other types can't be joined mid-mission
    RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Fill the Joblist array with FM mission details
//
// RETURN VALUE:        BOOL        TRUE if mission details have been stored, otherwise FALSE
//
// NOTES:   TEMP - This function is only being used until FM uses the Mission Controller, so all variables are being mangled to fit into the existing CnC details struct
FUNC BOOL Add_Joinable_FM_Mission_Details_To_Joblist()

    // Build up a list of all active FM Missions - use the vector to distinguish between them
    PLAYER_INDEX        thisPlayer
    g_eMPJobListStates  joblistState    = MP_JOBLIST_JOINABLE_MISSION
    BOOL                detailsStored   = FALSE
    BOOL                isOnPlaylist    = FALSE
    BOOL                tryToAddDetails = FALSE
    INT                 firstArrayPos   = g_numMPJobListEntries     // Used as the first array position when checking for 'not joinable' missions
    INT                 tempLoop        = 0
    INT                 playerGBD       = 0
    INT                 fmmcType

    REPEAT NUM_NETWORK_PLAYERS tempLoop
        thisPlayer = INT_TO_PLAYERINDEX(tempLoop)
        // Player OK?
        IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
            // In GBD Array?
            IF (IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(thisPlayer))
                // On an FM activity?
                IF (IS_PLAYER_ON_ANY_FM_MISSION(thisPlayer, FALSE))
                    isOnPlaylist    = IS_PLAYER_ON_A_PLAYLIST(thisPlayer)
                    playerGBD       = NATIVE_TO_INT(ThisPlayer)
                    fmmcType        = GlobalplayerBD_FM[playerGBD].iCurrentMissionType
                    
                    IF (isOnPlaylist)
                    AND (fmmcType < 0)
                        // ...on playlist, but not on a mission
                        fmmcType = FMMC_TYPE_PLAYLIST
                    ENDIF
                    
                    // If on playlist, ignore other checks and just try to add the details
                    tryToAddDetails = FALSE
                    IF (isOnPlaylist)
                        tryToAddDetails = TRUE
                    ENDIF
                    
                    IF NOT (tryToAddDetails)
                    AND (fmmcType >= 0)
                        // ...perform various checks to see if these details are valid for trying to add to joblist
                        IF (IS_FM_TYPE_UNLOCKED(fmmcType))
                        AND (Can_This_FM_Mission_Type_Be_Joined_MidMission(fmmcType))
                        AND (Check_If_FM_Mission_Data_Is_Valid_For_Player(playerGBD))
                            tryToAddDetails = TRUE
                        ENDIF
                    ENDIF

                    // On an FM mission that is unlocked for this player and can be joined mid-mission?
                    IF (tryToAddDetails)
                        // KGM TEMP 19/11/12: Additional Info to try to track down Gang Hideout problem where variation is -1
                        #IF IS_DEBUG_BUILD
                            IF (fmmcType = FMMC_TYPE_GANGHIDEOUT)
                                NET_PRINT("...KGM MP: Found Player on Gang Hideout: ")
                                NET_PRINT(GET_PLAYER_NAME(thisPlayer))
                                NET_PRINT("  [PlayerGDB: ")
                                NET_PRINT_INT(playerGBD)
                                NET_PRINT("]  [Coords = ")
                                NET_PRINT_VECTOR(GlobalplayerBD_FM[playerGBD].currentMissionData.mdPrimaryCoords)
                                NET_PRINT("]  [Mission Variation = ")
                                NET_PRINT_INT(GlobalplayerBD_FM[playerGBD].currentMissionData.mdID.idVariation)
                                NET_PRINT("]")
                                NET_NL()
                            ENDIF
                        #ENDIF
                        
                        // Set the Joblist State
                        joblistState = MP_JOBLIST_JOINABLE_MISSION
                        IF (isOnPlaylist)
                            joblistState = MP_JOBLIST_JOINABLE_PLAYLIST
                        ENDIF
                    
                        // Add to the Joblist if this is the first player on this mission
                        IF (Add_Details_To_FM_JobList(joblistState, tempLoop, playerGBD, fmmcType))
                            detailsStored = TRUE
                        ENDIF
                    ENDIF
                ENDIF
            ENDIF
        ENDIF
    ENDREPEAT
    
    // Are there any details stored?
    IF NOT (detailsStored)
        RETURN FALSE
    ENDIF
    
    // There are details stored, so check if any of them are classed as not joinable and remove them
    INT checkPos    = firstArrayPos
    INT storePos    = firstArrayPos
    
    WHILE (checkPos < g_numMPJobListEntries)
        // Is the entry in this position is joinable, then keep it in the array
        IF NOT (IS_BIT_SET(g_sJobListMP[checkPos].jlBitfield, JOBLIST_BITFLAG_MISSION_NOT_JOINABLE))
            // ...this is NOT NotJoinable (so it IS joinable), so keep it - move it up the array if there are any not joinable entries above here
            IF (checkPos != storePos)
                // ...move the entry
                g_sJobListMP[storePos] = g_sJobListMP[checkPos]
            ENDIF
            
            storePos++
        ENDIF
        
        checkPos++
    ENDWHILE
    
    // Make a note of the new end array position
    INT holdNewMaxEntries = storePos
    
    // Clear out any entries that did contain data but are now empty
    g_structMissionJobListMP emptyJoblistArrayPos
    WHILE (storePos < g_numMPJobListEntries)
        g_sJobListMP[storePos] = emptyJoblistArrayPos
        storePos++
    ENDWHILE
    
    // Update the max array positions used
    g_numMPJobListEntries = holdNewMaxEntries
    
    // Do any of the added details still remain?
    IF (g_numMPJobListEntries = firstArrayPos)
        // ...no, they've all been removed
        RETURN FALSE
    ENDIF
    
    // There are some new details stored
    RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Fill the details of any Invitations that are available
//
// INPUT PARAMS:        paramFeedID     [DEFAULT = -1] If set, then display only the invitation associated with this FeedID
// RETURN VALUE:        BOOL            TRUE if invitation details have been stored, otherwise FALSE
//
// NOTES:   Assumes the Player is OK.
FUNC BOOL Add_Invitation_Details_To_Joblist(INT paramFeedID = -1)

    // Fill the array with as many invitations as there are available starting with the newest
    BOOL        detailsStored       = FALSE
    INT         tempLoop            = 0
    BOOL        tryToAdd            = FALSE
    
    // TEMP: There are no invitations so this should never be greater than 0 for now
    REPEAT g_numMPJobListInvitations tempLoop
        // Make sure there is room on the Joblist for another invitation
        IF (g_numMPJobListEntries >= MAX_MP_JOBLIST_ENTRIES)
            // ...joblist is full
            RETURN (detailsStored)
        ENDIF
        
        // There is still room on the Joblist, so add this invitation if it is active
        IF (g_sJLInvitesMP[tempLoop].jliActive)
            // Does this Invite have the specified FeedID if appropriate
            tryToAdd = FALSE
            IF (paramFeedID = -1)
                tryToAdd = TRUE
            ENDIF
            IF NOT (tryToAdd)
                IF (paramFeedID = g_sJLInvitesMP[tempLoop].jliFeedID)
                    tryToAdd = TRUE
                ENDIF
            ENDIF
            
            IF (tryToAdd)
                // KGM 29/1/13: Don't display an entry if it is a duplicate of an existing Invite from a different player
                IF NOT (Is_Invitation_In_Slot_A_Duplicate(tempLoop))
                    // NOTE: The data is being forced into variables that are badly-named for FM, but this should be temporary
                    //      uniqueID    :   being used for PLAYER INDEX AS INT
                    //      missionAsInt:   being used for FMMC TYPE
                    //      instance    :   being used for CREATOR ID (AS INT)
                    g_sJobListMP[g_numMPJobListEntries].jlBitfield      = ALL_MISSION_CONTROL_BITS_CLEAR
                    g_sJobListMP[g_numMPJobListEntries].jlState         = MP_JOBLIST_INVITATION
                    IF (g_sJLInvitesMP[tempLoop].jliInvitorIsPlayer)
                        g_sJobListMP[g_numMPJobListEntries].jlUniqueID      = NATIVE_TO_INT(g_sJLInvitesMP[tempLoop].jliPlayer)
                        SET_BIT(g_sJobListMP[g_numMPJobListEntries].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
                    ELSE
                        g_sJobListMP[g_numMPJobListEntries].jlUniqueID      = ENUM_TO_INT(g_sJLInvitesMP[tempLoop].jliNPC)
                    ENDIF
                    g_sJobListMP[g_numMPJobListEntries].jlMissionAsInt  		= g_sJLInvitesMP[tempLoop].jliType
					g_sJobListMP[g_numMPJobListEntries].jlSubtype				= g_sJLInvitesMP[tempLoop].jliSubtype
                    g_sJobListMP[g_numMPJobListEntries].jlInstance      		= g_sJLInvitesMP[tempLoop].jliCreatorID
                    g_sJobListMP[g_numMPJobListEntries].jlCoords        		= g_sJLInvitesMP[tempLoop].jliCoords
                    g_sJobListMP[g_numMPJobListEntries].jlMissionName   		= g_sJLInvitesMP[tempLoop].jliMissionName
                    g_sJobListMP[g_numMPJobListEntries].jlDescription  		 	= g_sJLInvitesMP[tempLoop].jliDescription
                    g_sJobListMP[g_numMPJobListEntries].jlDescHash      		= g_sJLInvitesMP[tempLoop].jliDescHash
                    g_sJobListMP[g_numMPJobListEntries].jlRootContentIdHash	    = g_sJLInvitesMP[tempLoop].jliRootContentIdHash
                    g_sJobListMP[g_numMPJobListEntries].jlAdversaryModeType	    = g_sJLInvitesMP[tempLoop].jliAdversaryModeType
                    g_sJobListMP[g_numMPJobListEntries].jlQuickMatch	    	= g_sJLInvitesMP[tempLoop].jliQuickMatch
                    g_sJobListMP[g_numMPJobListEntries].jlContentID     		= g_sJLInvitesMP[tempLoop].jliContentID
					g_sJobListMP[g_numMPJobListEntries].jlMinPlayers			= g_sJLInvitesMP[tempLoop].jliMinPlayers
					g_sJobListMP[g_numMPJobListEntries].jlMaxPlayers			= g_sJLInvitesMP[tempLoop].jliMaxPlayers
					g_sJobListMP[g_numMPJobListEntries].jlWagerH2H				= g_sJLInvitesMP[tempLoop].jliWagerH2H
					g_sJobListMP[g_numMPJobListEntries].jlHeistPackedInt		= g_sJLInvitesMP[tempLoop].jliHeistPackedInt
                    g_sJobListMP[g_numMPJobListEntries].jlOrderIndex			= g_sJLInvitesMP[tempLoop].jliOrderIndex	// MMM 1645958 - 16/06/2014 
					g_sJobListMP[g_numMPJobListEntries].jlIsPushBikeOnly		= g_sJLInvitesMP[tempLoop].jliIsPushBikeOnly 
					
                    // KGM 3/3/13: If the invitation is not allowed to be displayed, still display it, but display 'closed'
                    IF NOT (Is_Invitation_In_Slot_Allowed_To_Be_Displayed(tempLoop))
                        SET_BIT(g_sJobListMP[g_numMPJobListEntries].jlBitfield, JOBLIST_BITFLAG_INVITE_CLOSED)
                    ENDIF
                    
                    // Increase the number of stored entries on the Joblist
                    g_numMPJobListEntries++
                    
                    detailsStored = TRUE
                ENDIF
            ENDIF
        ENDIF
    ENDREPEAT
    
    // Return whether or not details were stored
    RETURN (detailsStored)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Fill the details of any Cross-Session Invites that are available
//
// INPUT PARAMS:        paramFeedID     [DEFAULT = -1] If set, then display only the invitation associated with this FeedID
// RETURN VALUE:        BOOL            TRUE if cross-session invite details have been stored, otherwise FALSE
//
// NOTES:   Assumes the Player is OK.
FUNC BOOL Add_CSInvite_Details_To_Joblist(INT paramFeedID = -1)

    // Fill the array with as many invitations as there are available starting with the newest
    BOOL        detailsStored       = FALSE
    INT         tempLoop            = 0
    BOOL        tryToAdd            = FALSE
    
    // TEMP: There are no invitations so this should never be greater than 0 for now
    REPEAT g_numMPCSInvites tempLoop
        // Make sure there is room on the Joblist for another invitation
        IF (g_numMPJobListEntries >= MAX_MP_JOBLIST_ENTRIES)
            // ...joblist is full
            RETURN (detailsStored)
        ENDIF
        
        // There is still room on the Joblist, so add this Cross-Session Invite if it is active
        IF (g_sJLCSInvitesMP[tempLoop].jcsiActive)
            // Does this Invite have the specified FeedID if appropriate
            tryToAdd = FALSE
            IF (paramFeedID = -1)
                tryToAdd = TRUE
            ENDIF
            IF NOT (tryToAdd)
                IF (paramFeedID = g_sJLCSInvitesMP[tempLoop].jcsiFeedID)
                    tryToAdd = TRUE
                ENDIF
            ENDIF
            
            IF (tryToAdd)
                // NOTE: The data is being forced into variables that are badly-named for FM, but this should be temporary
                //      missionAsInt  : being used for FMMC TYPE
                //      instance      : being used for CREATOR ID (AS INT)
                //      jlUniqueID    : being used for INVITE ID
                g_sJobListMP[g_numMPJobListEntries].jlBitfield      	= ALL_MISSION_CONTROL_BITS_CLEAR
                g_sJobListMP[g_numMPJobListEntries].jlState        	 	= MP_JOBLIST_CS_INVITE
                g_sJobListMP[g_numMPJobListEntries].jlMissionAsInt 	 	= g_sJLCSInvitesMP[tempLoop].jcsiType
				g_sJoblistMP[g_numMPJobListEntries].jlSubtype			= g_sJLCSInvitesMP[tempLoop].jcsiSubtype
                g_sJobListMP[g_numMPJobListEntries].jlInstance      	= g_sJLCSInvitesMP[tempLoop].jcsiCreatorID
                g_sJobListMP[g_numMPJobListEntries].jlInvitorName   	= Get_CSInvite_Name_For_Display(tempLoop)
                g_sJobListMP[g_numMPJobListEntries].jlMissionName   	= g_sJLCSInvitesMP[tempLoop].jcsiMissionName
                g_sJobListMP[g_numMPJobListEntries].jlDescription   	= g_sJLCSInvitesMP[tempLoop].jcsiDescription
                g_sJobListMP[g_numMPJobListEntries].jlDescHash      	= g_sJLCSInvitesMP[tempLoop].jcsiDescHash
                g_sJobListMP[g_numMPJobListEntries].jlUniqueID      	= g_sJLCSInvitesMP[tempLoop].jcsiInviteID
                g_sJobListMP[g_numMPJobListEntries].jlContentID     	= g_sJLCSInvitesMP[tempLoop].jcsiContentID
                g_sJobListMP[g_numMPJobListEntries].jlRootContentIdHash = g_sJLCSInvitesMP[tempLoop].jlRootContentIdHash
                g_sJobListMP[g_numMPJobListEntries].jlAdversaryModeType = g_sJLCSInvitesMP[tempLoop].jlAdversaryModeType
                g_sJobListMP[g_numMPJobListEntries].jlQuickMatch 		= g_sJLCSInvitesMP[tempLoop].jlQuickMatch
				g_sJobListMP[g_numMPJobListEntries].jlOrderIndex		= g_sJLCSInvitesMP[tempLoop].jcsiOrderIndex
				g_sJobListMP[g_numMPJobListEntries].jlIsPushBikeOnly 	= g_sJLCSInvitesMP[tempLoop].jcsiIsPushBikeOnly
				// KGM 9/13/12: Store the head-to-head playlist wager amount if applicable
				IF (g_sJLCSInvitesMP[tempLoop].jcsiPlaylistCurrent >= CROSS_SESSION_H2H_WAGER_OFFSET)
				AND NOT (g_sJLCSInvitesMP[tempLoop].jcsiPlaylistCurrent >= CROSS_SESSION_TOUR_QUAL_OFFSET)
					g_sJobListMP[g_numMPJobListEntries].jlWagerH2H	= (g_sJLCSInvitesMP[tempLoop].jcsiPlaylistCurrent - CROSS_SESSION_H2H_WAGER_OFFSET)
				ELSE
					g_sJobListMP[g_numMPJobListEntries].jlWagerH2H	= -1
				ENDIF
                    
                // KGM 3/3/13: If the invitation is not allowed to be displayed, still display it, but display 'closed'
                IF NOT (Is_CSInvite_In_Slot_Allowed_To_Be_Displayed(tempLoop))
                    SET_BIT(g_sJobListMP[g_numMPJobListEntries].jlBitfield, JOBLIST_BITFLAG_INVITE_CLOSED)
                ENDIF
                
                // KGM 23/6/13: All Cross-Session Invites are from players, so set the bitflag
                SET_BIT(g_sJobListMP[g_numMPJobListEntries].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
				
				// KGM 15/6/14: Heist PackedInt isn't required for Cross-session Invites - the apartment scripts gets the data from transition variables
				g_sJobListMP[g_numMPJobListEntries].jlHeistPackedInt	= 0
                
                // Increase the number of stored entries on the Joblist
                g_numMPJobListEntries++
                    
                detailsStored = TRUE
            ENDIF
        ENDIF
    ENDREPEAT
    
    // Return whether or not details were stored
    RETURN (detailsStored)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Fill the details of any Basic Invites that are available
//
// INPUT PARAMS:        paramFeedID     [DEFAULT = -1] If set, then display only the invitation associated with this FeedID
// RETURN VALUE:        BOOL            TRUE if Basic Invite details have been stored, otherwise FALSE
//
// NOTES:   Assumes the Player is OK.
FUNC BOOL Add_RVInvite_Details_To_Joblist(INT paramFeedID = -1)

    // Fill the array with as many invitations as there are available starting with the newest
// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
//    BOOL        hasWantedLevel      = (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
    BOOL        detailsStored       = FALSE
    BOOL        allowInclusion      = FALSE
    INT         tempLoop            = 0
    
    // TEMP: There are no invitations so this should never be greater than 0 for now
    REPEAT g_numMPRVInvites tempLoop
        // Make sure there is room on the Joblist for another invitation
        IF (g_numMPJobListEntries >= MAX_MP_JOBLIST_ENTRIES)
            // ...joblist is full
            RETURN (detailsStored)
        ENDIF
        
        // There is still room on the Joblist, so add this invitation if it is active and allowed to be displayed
        IF (g_sJLRVInvitesMP[tempLoop].jrviActive)
            // Don't add this if we're looking only for a specific Invite based on the feedID
            allowInclusion = FALSE
            
            IF (paramFeedID = -1)
                allowInclusion = TRUE
            ENDIF
            IF NOT (allowInclusion)
                IF (paramFeedID = g_sJLCSInvitesMP[tempLoop].jcsiFeedID)
                    allowInclusion = TRUE
                ENDIF
            ENDIF
            
// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
//            // Also don't add this one if it is an NPC invite and the player has a wanted level
//            IF (allowInclusion)
//                IF (hasWantedLevel)
//                AND NOT (g_sJLRVInvitesMP[tempLoop].jrviInvitorIsPlayer)
//                    allowInclusion = FALSE
//                ENDIF
//            ENDIF

			// KGM 12/11/14 [BUG 2120290]: Ensure Quick Heist invites are suppressed if they shouldn't be displayed
			IF (allowInclusion)
				IF (g_sJLRVInvitesMP[tempLoop].jrviType = FMMC_TYPE_HEIST_PHONE_INVITE)
				OR (g_sJLRVInvitesMP[tempLoop].jrviType = FMMC_TYPE_HEIST_PHONE_INVITE_2)
				OR (g_sJLRVInvitesMP[tempLoop].jrviType = FMMC_TYPE_VCM_QUICK_INVITE)
				OR (g_sJLRVInvitesMP[tempLoop].jrviType = FMMC_TYPE_IH_QUICK_INVITE)
					IF NOT (Is_RVInvite_In_Slot_Allowed_To_Be_Displayed(tempLoop))
						allowInclusion = FALSE
					ENDIF
				ENDIF
			ENDIF
            
            IF (allowInclusion)
                // NOTE: The data is being forced into variables that are badly-named for FM, but this should be temporary
                //      uniqueID    :   being used for PLAYER INDEX AS INT
                //      missionAsInt:   being used for FMMC TYPE
                //      Description :   being used for OPTIONAL TEXT LABEL
                //      DescHash    :   being used for OPTIONAL INTEGER 1
                //      Instance    :   being used for OPTIONAL INTEGER 2
				// KGM 22/1/15 [BUG 2207793]: Basic Invites now hold a subtype, used by Quick Heist invites to specify PLANNING or FINALE
                g_sJobListMP[g_numMPJobListEntries].jlBitfield      = ALL_MISSION_CONTROL_BITS_CLEAR
                g_sJobListMP[g_numMPJobListEntries].jlState         = MP_JOBLIST_RV_INVITE
                IF (g_sJLRVInvitesMP[tempLoop].jrviInvitorIsPlayer)
                    g_sJobListMP[g_numMPJobListEntries].jlUniqueID      = NATIVE_TO_INT(g_sJLRVInvitesMP[tempLoop].jrviPlayer)
                    SET_BIT(g_sJobListMP[g_numMPJobListEntries].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER)
                ELSE
                    g_sJobListMP[g_numMPJobListEntries].jlUniqueID      = ENUM_TO_INT(g_sJLRVInvitesMP[tempLoop].jrviNPC)
                ENDIF
                g_sJobListMP[g_numMPJobListEntries].jlMissionAsInt  	= g_sJLRVInvitesMP[tempLoop].jrviType
				g_sJobListMP[g_numMPJobListEntries].jlSubtype			= g_sJLRVInvitesMP[tempLoop].jrviSubType
                g_sJobListMP[g_numMPJobListEntries].jlMissionName   	= g_sJLRVInvitesMP[tempLoop].jrviMissionName
                g_sJobListMP[g_numMPJobListEntries].jlRootContentIdHash = g_sJLRVInvitesMP[tempLoop].jrviRootId
                g_sJobListMP[g_numMPJobListEntries].jlForceToTop		= g_sJLRVInvitesMP[tempLoop].jliForceToTop
                g_sJobListMP[g_numMPJobListEntries].jlContentID     	= ""
				g_sJobListMP[g_numMPJobListEntries].jlWagerH2H			= -1
				g_sJobListMP[g_numMPJobListEntries].jlHeistPackedInt	= 0
                    
                // KGM 3/3/13: If the invitation is not allowed to be displayed, still display it, but display 'closed'
                IF NOT (Is_RVInvite_In_Slot_Allowed_To_Be_Displayed(tempLoop))
                    SET_BIT(g_sJobListMP[g_numMPJobListEntries].jlBitfield, JOBLIST_BITFLAG_INVITE_CLOSED)
                ENDIF
				
				// KGM 17/9/13: Check if the Invite is allowed to be deleted
				IF (g_sJLRVInvitesMP[tempLoop].jrviAllowDelete)
					SET_BIT(g_sJobListMP[g_numMPJobListEntries].jlBitfield, JOBLIST_BITFLAG_ALLOW_DELETE)
				ENDIF
                
                // KGM 20/6/13: Impromptu Race needs additional details displayed on the Invite Screen
                g_sJobListMP[g_numMPJobListEntries].jlDescription 	 	= g_sJLRVInvitesMP[tempLoop].jrviOptionalTL
                g_sJobListMP[g_numMPJobListEntries].jlDescHash     		= g_sJLRVInvitesMP[tempLoop].jrviOptionalInt1
                g_sJobListMP[g_numMPJobListEntries].jlInstance     		= g_sJLRVInvitesMP[tempLoop].jrviOptionalInt2
                g_sJobListMP[g_numMPJobListEntries].jlOrderIndex 		= g_sJLRVInvitesMP[tempLoop].jrviOrderIndex
				g_sJobListMP[g_numMPJobListEntries].jlPropertyType		= g_sJLRVInvitesMP[tempLoop].jrviPropertyType
																								
                // Increase the number of stored entries on the Joblist
                g_numMPJobListEntries++
                
                detailsStored = TRUE
            ENDIF
        ENDIF
    ENDREPEAT
    
    // Return whether or not details were stored
    RETURN (detailsStored)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: If there is a most recent feed entry that is an invite then add only that entry to the joblist
//
// RETURN VALUE:        BOOL        TRUE if an invite was added, otherwise FALSE
//
// NOTES:   Added for the DPADUP quick display option so that only the invite associated with the most recent feed entry gets added
FUNC BOOL Add_Invite_Associated_With_Most_Recent_Feed_Entry_Only_To_Joblist()

    IF NOT (g_ShouldForceSelectionOfLatestAppItem)
        RETURN FALSE
    ENDIF
    
    IF (g_Last_App_ActivatableType != ACT_APP_INVITE)
        RETURN FALSE
    ENDIF
    
    // Check if the feed entry is still relevant
    IF (THEFEED_GET_LAST_SHOWN_PHONE_ACTIVATABLE_FEED_ID() < 0)
        RETURN FALSE
    ENDIF
    
    // The feed entry is still relevant, so find it in the invites arrays
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [Joblist]: Setting up Joblist Entry with this current Feed ID only: ")
        NET_PRINT_INT(g_Last_App_ActivatableFeedID)
        NET_NL()
    #ENDIF
    
    // Try to add it if it is on the Invites list
    IF (Add_Invitation_Details_To_Joblist(g_Last_App_ActivatableFeedID))
        RETURN TRUE
    ENDIF
    
    // Try to add it if it is on the Cross-Session Invites list
    IF (Add_CSInvite_Details_To_Joblist(g_Last_App_ActivatableFeedID))
        RETURN TRUE
    ENDIF
    
    // Try to add it if it is on the Basic Invites list
    IF (Add_RVInvite_Details_To_Joblist(g_Last_App_ActivatableFeedID))
        RETURN TRUE
    ENDIF
    
	
	
    // Not found
    RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Prioritise Player Invites over NPC invites
// NOTES:   Currently NPC invites should be suppressed if there are any player invites
PROC Prioritise_Player_Invites_Over_NPC_Invites()

    INT tempLoop        = 0
    INT playerInvites   = 0
    INT npcInvites      = 0

    // Find out how many of each type of invite there are
    REPEAT g_numMPJobListEntries tempLoop
        IF (g_sJobListMP[tempLoop].jlState = MP_JOBLIST_INVITATION)
        OR (g_sJobListMP[tempLoop].jlState = MP_JOBLIST_CS_INVITE)
        OR (g_sJobListMP[tempLoop].jlState = MP_JOBLIST_RV_INVITE)
            IF (IS_BIT_SET(g_sJobListMP[tempLoop].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER))
                playerInvites++
            ELSE
                npcInvites++
            ENDIF
        ENDIF
    ENDREPEAT
    
    // If there isn't a mix of both types of invite then nothing to do
    IF (playerInvites = 0)
    OR (npcInvites = 0)
        EXIT
    ENDIF
    
    // There is a mix of both, so remove all NPC invites
    INT theTo = 0
    INT theFrom = 0
    
    REPEAT g_numMPJobListEntries theFrom
        IF (g_sJobListMP[theFrom].jlState = MP_JOBLIST_INVITATION)
        OR (g_sJobListMP[theFrom].jlState = MP_JOBLIST_CS_INVITE)
        OR (g_sJobListMP[theFrom].jlState = MP_JOBLIST_RV_INVITE)
            IF (IS_BIT_SET(g_sJobListMP[theFrom].jlBitfield, JOBLIST_BITFLAG_INVITOR_IS_PLAYER))
            AND NOT (IS_BIT_SET(g_sJobListMP[theFrom].jlBitfield, JOBLIST_BITFLAG_INVITE_CLOSED))
                // ...found a player invite, so shuffle it up the array to cover any NPC invites
                IF (theFrom != theTo)
                    g_sJobListMP[theTo] = g_sJobListMP[theFrom]
                ENDIF
                theTo++
            ENDIF
        ELSE
            // ...found something that wasn't an invite, so shuffle it up the array
            IF (theFrom != theTo)
                g_sJobListMP[theTo] = g_sJobListMP[theFrom]
            ENDIF
            theTo++
        ENDIF
    ENDREPEAT
    
    // Clear out the remaining array details
    tempLoop = theTo
    
    WHILE (tempLoop < g_numMPJobListEntries)
        g_sJobListMP[tempLoop].jlState = NO_MP_JOBLIST_ENTRY
        tempLoop++
    ENDWHILE
    
    // Modify the number of Joblist entries
    g_numMPJobListEntries = theTo

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Display Console Log output when updating the joblist
//
// INPUT PARAMS:            paramText           The text to be output to the console log    
#IF IS_DEBUG_BUILD
PROC Debug_Output_Joblist_Update_Debug_Text_To_Console_Log(STRING paramText)

    NET_PRINT("...KGM MP: Update JobList Details") NET_NL()
    NET_PRINT("           ")
    NET_PRINT(paramText)
    NET_NL()

ENDPROC
#ENDIF


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the new joblist details differ from the stored details
//
// RETURN PARAMS:           paramJobList        The copy of the Joblist struct to be tested against the updated details
// RETURN VALUE:            BOOL                TRUE if the joblist has changed, otherwise FALSE
FUNC BOOL Has_JobList_Details_Changed(m_sCopyJobList &paramJobList)

    // If the two total entries values are different then the joblist has been modified
    IF NOT (paramJobList.numEntries = g_numMPJobListEntries)
        RETURN TRUE
    ENDIF
    
    // The two totals are the same, so need to check the actual contents - I can probably get off with just comparing the uniqueID field
    // KGM 31/7/12: or FM I may need to compare more fields as a TEMP fix until FM uses Mission Controller,
    //              but I'm going to leave this just checking the uniqueID (the playerGBD slot for FM) for now because it may be enough
    // KGM 14/9/12: POSSIBLY TEMP: Also check the Bitfield - this is to highlight when a Bounty Target has changed
    INT tempLoop = 0
    REPEAT MAX_MP_JOBLIST_ENTRIES tempLoop
        IF NOT (paramJobList.sJobList[tempLoop].jlUniqueID = g_sJobListMP[tempLoop].jlUniqueID)
        OR NOT (paramJobList.sJobList[tempLoop].jlBitfield = g_sJobListMP[tempLoop].jlBitfield)
            RETURN TRUE
        ENDIF
    ENDREPEAT
    
    // Contents are the same
    RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a specific ambient activity should prevent the joblist from appearing
//
// RETURN VALUE:        BOOL                    TRUE if the joblist should be blocked, otherwise FALSE
FUNC BOOL Should_This_Ambient_Activity_Block_Joblist()
    RETURN FALSE
ENDFUNC

// PURPOSE: Switches the information for two joblist entries. Specify the indexes and it will try to switch the
//			information. Used to reorder the jobslist so that in can be displayed in time order.
//
// PARAMS: 	Two indices for positions in the g_sJobListMP that are to be swapped 
//
// NOTES: 	Currently doesn't do any checks so when calling you need to make sure you have correctly 
//			calculated the indices. Added to solve bug 1645958
//
// AUTHOR: 	Mark M. Miller (23/06/2014)

PROC Swap_Joblist_Entries(INT index1, INT index2)
	g_structMissionJobListMP t = g_sJobListMP[index1]
	g_sJobListMP[index1] = g_sJobListMP[index2]
	g_sJobListMP[index2] = t
ENDPROC

// PURPOSE: Sorts the job list into the correct order
//
// NOTES:   The top value that is displayed on the jobslist is actually the 0 indexed value, as such it is displayed 
//			in this order 0,1...10,11 etc. However the order index with the highest value is the one that should be 
//			displayed most recently. Order indices of 4, 12 and 23. 23 is the most recent index. As such, these values 
//			would be mapped as follows : [23 -> 0], [12 -> 1],[4 -> 2]. Just mentioning this as it can be counter 
//			intuitive at first. Added to solve bug 1645958
// 
// AUTHOR:  Mark M. Miller (23/06/2014)
PROC Sort_Joblist()
	// MMM 16/06/2014 - Added sort, sorts the array into the correct order using bubble sort. 
	BOOL changed = TRUE
	WHILE changed			// Keep sorting whilst any elements have moved
		changed = FALSE
//		INT sortIndex = g_numMPRVInvites	// Since we want RV invites at the top and we know RV invites will already be at the top we can skip these.	(KGM 15/9/14: Include these now)
		INT sortIndex = 0
		FOR sortIndex = 0 TO MAX_MP_JOBLIST_ENTRIES	-1			
			IF g_sJobListMP[sortIndex].jlOrderIndex != -1 
			AND g_sJobListMP[sortIndex].jlState != NO_MP_JOBLIST_ENTRY 
			AND !g_sJobListMP[sortIndex].jlForceToTop 
				IF sortIndex - 1 > 0 
				AND g_sJobListMP[sortIndex].jlOrderIndex > g_sJobListMP[sortIndex - 1].jlOrderIndex 
				AND g_sJobListMP[sortIndex-1].jlState != NO_MP_JOBLIST_ENTRY
				//AND !g_sJobListMP[sortIndex-1].jlForceToTop 
					Swap_Joblist_Entries(sortIndex, sortIndex - 1)
					changed = TRUE
				ELIF sortIndex + 1 < MAX_MP_JOBLIST_ENTRIES 
				AND g_sJobListMP[sortIndex].jlOrderIndex < g_sJobListMP[sortIndex + 1].jlOrderIndex  
				AND g_sJobListMP[sortIndex+1].jlState != NO_MP_JOBLIST_ENTRY
				AND !g_sJobListMP[sortIndex+1].jlForceToTop 
					Swap_Joblist_Entries(sortIndex, sortIndex + 1)
					changed = TRUE
				ENDIF
			ENDIF
		ENDFOR
	ENDWHILE
ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Update the list of jobs available for this player.
//
// RETURN VALUE:        BOOL            TRUE if the data has changed, otherwise FALSE
//
// NOTES:   If the player is on-mission then only the current mission will be on the joblist.
FUNC BOOL Update_JobList_Details()
    
    // I'm going to leave all the checks within this function but wrap them in a mode check - this is a bit untidy, but should make it easier to ensure all checks in all modes are done
    BOOL        temp_usingFreemodeDataOnly      = TRUE
    
    BOOL        joinableAdded       = FALSE
    BOOL        rvInvitesAdded      = FALSE
    BOOL        invitationsAdded    = FALSE
    BOOL        csInvitesAdded      = FALSE
// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
//    BOOL        hasWantedLevel      = (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
    
    // KGM 7/3/13: I think this is to prevent the testbed from calling this
    BOOL doUpdate = FALSE
    IF NOT (NETWORK_IS_GAME_IN_PROGRESS())
    OR (GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
        doUpdate = TRUE
    ENDIF
    
    IF NOT (doUpdate)
        RETURN FALSE
    ENDIF

    // Copy the current joblist array
    m_sCopyJobList holdJobList  
    
    INT tempLoop = 0
    REPEAT MAX_MP_JOBLIST_ENTRIES tempLoop
        holdJobList.sJobList[tempLoop] = g_sJobListMP[tempLoop]
    ENDREPEAT
    
    holdJobList.numEntries = g_numMPJobListEntries
    
    // Clear the array
    Clear_Joblist_Details()
    
    // Before doing the Flow Flag check, ensure the player is in the GBD_SLOT array
    IF (NETWORK_IS_GAME_IN_PROGRESS())
        IF NOT (IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID()))
            IF (Has_JobList_Details_Changed(holdJobList))
                #IF IS_DEBUG_BUILD
                    Debug_Output_Joblist_Update_Debug_Text_To_Console_Log("NO JOBLIST DETAILS: Player is not in GBD_SLOT")
                #ENDIF
                
                RETURN TRUE
            ENDIF
            
            RETURN FALSE
        ENDIF
    ENDIF

    IF Should_This_Ambient_Activity_Block_Joblist()
        IF (Has_JobList_Details_Changed(holdJobList))
            #IF IS_DEBUG_BUILD
                Debug_Output_Joblist_Update_Debug_Text_To_Console_Log("NO JOBLIST DETAILS: Player is on a Joblist-Blocking Ambient Activity")
            #ENDIF
            
            RETURN TRUE
        ENDIF
        
        RETURN FALSE
    ENDIF
    
    // Only do this when the joblist returns back to using Mission Controller data
    IF NOT (temp_usingFreemodeDataOnly)
        // Get the array position of a mission on which this player is reserved or active
        INT myMissionArrayPosition = Get_MP_Mission_Request_ArrayPos_Player_Is_Allocated_To()
            
        // If the player is on a mission then store those details
        IF (Add_Current_Mission_Details_To_JobList(myMissionArrayPosition))
            IF (Has_JobList_Details_Changed(holdJobList))
                RETURN TRUE
            ENDIF
            
            RETURN FALSE
        ENDIF
        
        // Nothing more to do if the player is allocated to a mission
        IF NOT (myMissionArrayPosition = FAILED_TO_FIND_MISSION)
            IF (Has_JobList_Details_Changed(holdJobList))
                RETURN TRUE
            ENDIF
            
            RETURN FALSE
        ENDIF
        
        // Player is not allocated to a mission, so store all joinable mission details
        IF (Add_Joinable_Mission_Details_To_JobList())
            IF (Has_JobList_Details_Changed(holdJobList))
                RETURN TRUE
            ENDIF
            
            RETURN FALSE
        ENDIF
    ENDIF
    
    // Do this while the joblist is still temporarily using only FM data
    IF (temp_usingFreemodeDataOnly)
        // Ensure the FM Tutorial has been done
        // KGM NOTE: Change this to a generic function that calls the mode-specific function
        IF (NETWORK_IS_GAME_IN_PROGRESS())
            IF NOT (HAS_FM_RACE_TUT_BEEN_DONE())
			AND NOT (GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_HSW_SETUP)
				PRINTLN("...KGM MP [JobList] Add_Current_FM_Mission_Details_To_Joblist - HAS_FM_RACE_TUT_BEEN_DONE")
                IF (Has_JobList_Details_Changed(holdJobList))
                    RETURN TRUE
                ENDIF
                
                RETURN FALSE
            ENDIF
        ENDIF
        
        // Add current mission if there is one if network game in progress
        // NOTE: In SP, quitting during a mission leaves the Is_Player_On_Or_Triggering_Any_Mission() returning TRUE
        IF (NETWORK_IS_GAME_IN_PROGRESS())
			BOOL allowRVInviteDetails = TRUE
            IF (Add_Current_FM_Mission_Details_To_Joblist(allowRVInviteDetails))
				// KGM 28/4/15 [BUG 2325536]: Made this conditional so that Time Trial wouldn't have additional invites listed
				IF (allowRVInviteDetails)
                	Add_RVInvite_Details_To_Joblist()       //RLB 14/08/13: This is so that Impromptu Race invites are displayed and selectable when on Missions.
				ENDIF
                			
                IF (Has_JobList_Details_Changed(holdJobList))
                    RETURN TRUE
                ENDIF
                
                RETURN FALSE
            ENDIF
        ENDIF
        
        // Set up only the invite associsted with an active most recent feed entry if relevant
        IF (Add_Invite_Associated_With_Most_Recent_Feed_Entry_Only_To_Joblist())
            IF (Has_JobList_Details_Changed(holdJobList))
                RETURN TRUE
            ENDIF
            
            RETURN FALSE
        ENDIF
        
        // Player is not on any FM mission, so generate the joblist
// KGM 9/9/13 - COmmented this out because it's now messy, added new version below ignoring Wanted level change but adding SP checks
//        // TEMP: These functions mangle and twist the CnC variables so that this works for FM because FM doesn't yet use Mission Controller
//        // Basic Invites with return values may get added even with a wanted level
//        rvInvitesAdded          = Add_RVInvite_Details_To_Joblist()
//// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
////        IF NOT (hasWantedLevel)
////            // ...these invites shouldn't get added when the player has a wanted level
//          // KGM 31/8/13: These should get addd even with a Wanted Level now
//            invitationsAdded    = Add_Invitation_Details_To_Joblist()
//            csInvitesAdded      = Add_CSInvite_Details_To_Joblist()
//            joinableAdded       = Add_Joinable_FM_Mission_Details_To_Joblist()
////        ENDIF

// KGM 9/9/13: New version of above lines
        
		IF (NETWORK_IS_GAME_IN_PROGRESS())
            rvInvitesAdded      = Add_RVInvite_Details_To_Joblist()
            invitationsAdded    = Add_Invitation_Details_To_Joblist()
			csInvitesAdded      = Add_CSInvite_Details_To_Joblist()	
			
			Sort_Joblist()	// Added to solve bug 1645958 MMM - Run a sort after adding all other elements to the list
			
        ELSE
			csInvitesAdded      = Add_CSInvite_Details_To_Joblist()
		ENDIF
        
// KGM 14/9/13: Nothing is joinable mid-mission anymore, so commenting this out because it could sometimes find a Gang Attack.
//        IF (NETWORK_IS_GAME_IN_PROGRESS())
//            joinableAdded       = Add_Joinable_FM_Mission_Details_To_Joblist()
//        ENDIF

                
        // Have any details changed?
        IF (rvInvitesAdded)
        OR (joinableAdded)
        OR (invitationsAdded)
        OR (csInvitesAdded)

//          Prioritise_Player_Invites_Over_NPC_Invites()        // KGM 27/8/13: Mix NPC and Player invites again
            IF (Has_JobList_Details_Changed(holdJobList))
                RETURN TRUE
            ENDIF
            
            RETURN FALSE
        ENDIF
    ENDIF
    	
    // All Checks Completed, so if anything changed
    IF (Has_JobList_Details_Changed(holdJobList))	
		
        RETURN TRUE
    ENDIF
    
    RETURN FALSE

ENDFUNC




// ===========================================================================================================
//      The Joblist Updating Control Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Joblist Update Timer Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Sets the timeout value.
PROC Set_Joblist_Update_Timeout()
    //g_sJoblistUpdate.timeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), NEXT_JOBLIST_UPDATE_TIME_msec)  //Removed by Steve T at Keith's request to fix NT 1599405 
    g_sJoblistUpdate.timeout = GET_GAME_TIMER() + NEXT_JOBLIST_UPDATE_TIME_msec
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the joblist timer has expired.
//
// RETURN VALUE:        BOOL            TRUE if the Joblist Update timer has expired.
FUNC BOOL Has_Joblist_Update_Timer_Expired()
    
    
    //RETURN (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sJoblistUpdate.timeout))   //Removed by Steve T at Keith's request to fix NT 1599405 
    IF GET_GAME_TIMER() > g_sJoblistUpdate.timeout
        RETURN TRUE
    ELSE
        RETURN FALSE
    ENDIF

ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Maintenance Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain the FadeOut before the JobList warp
//
// RETURN VALUE:        BOOL            TRUE if the screen is faded out, otherwise FALSE
FUNC BOOL Maintain_Joblist_FadeOut_Before_Warp()
    
    IF (IS_SCREEN_FADED_OUT())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: Maintain_Joblist_Warp_FadeOut(): Screen is Faded Out") NET_NL()
        #ENDIF
        
        RETURN TRUE
    ENDIF
    
    // ...screen is not faded out
    IF (IS_SCREEN_FADED_IN())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: Maintain_Joblist_Warp_FadeOut(): Screen is faded in, so start fadeout") NET_NL()
        #ENDIF
        
        DO_SCREEN_FADE_OUT(500)
        
        RETURN FALSE
    ENDIF
    
    // Screen is not faded in, so it must be either fading in or fading out - either way we need to wait until the fade ends before taking next action
    #IF IS_DEBUG_BUILD
        IF (IS_SCREEN_FADING_IN())
            NET_PRINT("...KGM MP [JobList]: Maintain_Joblist_Warp_FadeOut(): Screen is fading in, so need to wait for that to end before starting a fade out") NET_NL()
        ENDIF

        IF (IS_SCREEN_FADING_OUT())
            NET_PRINT("...KGM MP [JobList]: Maintain_Joblist_Warp_FadeOut(): Screen is fading out, so need to wait for that to end") NET_NL()
        ENDIF
    #ENDIF
    
    RETURN FALSE
    
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Move the player to the Joblist Warp Offset
//
// RETURN VALUE:        BOOL            TRUE if the player got moved, FALSE if not
FUNC BOOL Move_Player_To_Offset_During_Joblist_SkySwoop()

    IF (IS_PED_INJURED(PLAYER_PED_ID()))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: Delay Setting Player Coords During Joblist SkySwoop - PLAYER INJURED") NET_NL()
        #ENDIF
    
        RETURN FALSE
    ENDIF
    
    IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
	    #IF IS_DEBUG_BUILD
	    NET_PRINT("...KGM MP [JobList]: Setting PlayerSET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE) ")NET_NL()
	    #ENDIF
   		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
		RETURN FALSE
	ENDIF
	
    // Move the player
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: Setting Player Coords and making player invisible during SkySwoop Up: ") NET_PRINT_VECTOR(g_sJoblistWarpMP.jlwOffset) NET_NL()
    #ENDIF
    
    VECTOR warpPosition = g_sJoblistWarpMP.jlwOffset
    warpPosition.z += 1.0
   	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE  | NSPC_FREEZE_POSITION )	// Added extra param for 2002686 - This is sometimes called when trying to warp to a mission. It trys to unfreeze the player during skycam which is bad. as such I'm setting it to freeze instead which should maintain a freeze. 
	SET_PLAYER_LEAVING_CORONA_VECTOR(GET_PLAYER_PERCEIVED_COORDS(PLAYER_ID()))
	SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), warpPosition)
    FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)

	IF (g_sJoblistWarpMP.jlwForceKillProperty)
		g_sJoblistHeistInviteShouldKillApartment = TRUE
		PRINTLN("...KGM MP [JobList][Heist]: ...Player has moved - force kill property (player must be accepting an invite to a Heist corona while in an apartment)")
		PRINTLN("...KGM MP [JobList][Heist]: ...Done by setting g_sJoblistHeistInviteShouldKillApartment to TRUE")
	ENDIF
	
    RETURN TRUE
                
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain a warp instigated by the Joblist (ie: when accepting an invitation)
// NOTES:   Fades in on warp completion
PROC Maintain_Joblist_FadeOut_And_Warp()

    IF NOT (Is_Joblist_Warp_In_Progress())
        EXIT
    ENDIF
    
    // For Immediate Launch invite acceptance (Contact Missions), maintain hiding the radar and Feed until the player is in the corona
    IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJoblistWarpMP.jlwContentID))
    AND (g_sJoblistWarpMP.jlwImmediateLaunch)
        #IF IS_DEBUG_BUILD
            NET_PRINT(".KGM [JobList]: Joblist Immediate Launch Mission: Hiding HUD and FEED and CASH") NET_NL()
        #ENDIF
    
        HIDE_HUD_AND_RADAR_THIS_FRAME()
        THEFEED_HIDE_THIS_FRAME()
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
        
        // Finished, or Safety Timeout
        IF (IS_PLAYER_IN_CORONA())
        OR (GET_FRAME_COUNT() > g_sJoblistWarpMP.jlwSafetyStopFrame)
            // Let something else maintain hiding the HUD
            Clear_Joblist_Warp_Details()
        ENDIF
        
        EXIT
    ENDIF
    
    // KGM 21/2/13: Fade the screen as part of the warp control - to ensure there are no problems if the Joblist is forced away mid-fade
    // KGM 21/3/13: Only do fade if using teleport, don't fade for skyswoop
    IF NOT (g_sJoblistWarpMP.jlwSkyswoop)
        IF NOT (g_sJoblistWarpMP.jlwFadedOut)
            IF NOT (Maintain_Joblist_FadeOut_Before_Warp())
                EXIT
            ENDIF
            
            g_sJoblistWarpMP.jlwFadedOut = TRUE
        ENDIF
    ENDIF
    
    // If using skyswoop and not in sky, then maintain skyswoop up
    IF (g_sJoblistWarpMP.jlwSkyswoop)
        IF NOT (g_sJoblistWarpMP.jlwSwoopUpDone)
            #IF IS_DEBUG_BUILD
                NET_PRINT(".KGM [JobList]: SkySwoop Up") NET_NL()
            #ENDIF
    
            HIDE_HUD_AND_RADAR_THIS_FRAME()
            THEFEED_HIDE_THIS_FRAME()
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
		
			// KGM 8/6/14: BUG 2364614 - Don't skyswoop up if there is a bail condition active, this is causing a skyswoop up/down clash. Allow the bail to cleanup the game.
			IF (IS_TRANSITION_ACTIVE())
	            PRINTLN(".KGM [JobList]: ...IGNORE SKYSWOOP UP (IS_TRANSITION_ACTIVE() = TRUE) - A BAIL MUST BE GETTING DONE")
				EXIT
			ENDIF
			
            VECTOR theEnd = g_sJoblistWarpMP.jlwOffset
            IF NOT (SET_SKYSWOOP_UP(FALSE, FALSE, DEFAULT, g_sJoblistWarpMP.jlwSkyswoopType, DEFAULT, theEnd.x, theEnd.y, theEnd.z))
                // Move the player after the first swoop up stage (unless it's a short switch)
                IF NOT (g_sJoblistWarpMP.jlwPlayerMoved)
                    IF IS_PLAYER_SWITCH_IN_PROGRESS()
                        IF (GET_PLAYER_SWITCH_TYPE() != SWITCH_TYPE_SHORT)
                            IF (GET_PLAYER_SWITCH_STATE() > SWITCH_STATE_PREP_FOR_CUT)
                                IF (Move_Player_To_Offset_During_Joblist_SkySwoop())
									IF IS_PAUSE_MENU_ACTIVE()
									AND NOT IS_PLAYER_IN_CORONA()
										SET_FRONTEND_ACTIVE(FALSE)
									ENDIF
                                    // Temporarily unlock the mission after the player moves
									Temporarily_Unlock_MissionsAtCoords_Mission_By_Filename(g_sJoblistWarpMP.jlwContentID)
		
									// Unblock this one mission (if all are walk-ins blocked for an FM Event)
							        Unblock_One_MissionsAtCoords_Mission(g_sJoblistWarpMP.jlwContentID)
												
                                    g_sJoblistWarpMP.jlwPlayerMoved = TRUE
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDIF
                ENDIF
    
                EXIT
            ENDIF
            
            // Skyswoop up completed, so move the player if the player hasn't already been moved
            #IF IS_DEBUG_BUILD
                NET_PRINT(".KGM [JobList]: SkySwoop Up COMPLETE") NET_NL()
            #ENDIF
            
            IF NOT (g_sJoblistWarpMP.jlwPlayerMoved)
                IF (Move_Player_To_Offset_During_Joblist_SkySwoop())
                    // Temporarily unlock the mission after the player moves
					Temporarily_Unlock_MissionsAtCoords_Mission_By_Filename(g_sJoblistWarpMP.jlwContentID)
		
					// Unblock this one mission (if all are walk-ins blocked for an FM Event)
			        Unblock_One_MissionsAtCoords_Mission(g_sJoblistWarpMP.jlwContentID)
									
                    g_sJoblistWarpMP.jlwPlayerMoved = TRUE
                ENDIF
            ENDIF

			IF IS_LOCAL_PLAYER_ON_HEIST_ISLAND()
				PRINTLN(".KGM [JobList]: HEIST_ISLAND_LOADING__UNLOAD_ISLAND")						
			    HEIST_ISLAND_LOADING__UNLOAD_ISLAND()
			ENDIF
			
            g_sJoblistWarpMP.jlwSwoopUpDone = TRUE
        ENDIF
    ENDIF
    
	
    // KGM 3/12/12: This is needed by Bobby to ensure that a player can be invited into an activity that the player hasn't personally yet unlocked
    // NOTE: There may be a safer way to do this - but it works so we'll stick with it for now.
    g_vStartingPointWarppedToWithVote = g_sJoblistWarpMP.jlwCoords
    #IF IS_DEBUG_BUILD
        NET_PRINT(".KGM [JobList]: Setting vector g_vStartingPointWarppedToWithVote: ") NET_PRINT_VECTOR(g_vStartingPointWarppedToWithVote) NET_NL()
    #ENDIF
    
    // KGM 21/3/13: Maintain teleport if not using the skyswoop method
    IF NOT (g_sJoblistWarpMP.jlwSkyswoop)
        IF NOT (NET_WARP_TO_COORD(g_sJoblistWarpMP.jlwOffset, 0.0, FALSE))
            EXIT
        ENDIF
    ENDIF
    
		IF GANGOPS_FLOW_IS_A_GANGOPS_MISSION_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash)
		#IF IS_DEBUG_BUILD
		AND NOT g_bLaunchedViaZmenu
		AND NOT g_SimpleInteriorData.bJoinedHeistWithUnknownInterior
		#ENDIF
			IF NOT IS_BIT_SET(g_SimpleInteriorData.iThirdBS, BS3_SIMPLE_INTERIOR_GLOBAL_DATA_COMPLETED_DEFUNCT_BASE_HEIST_INT_REFRESH)
				PRINTLN("[TS] [CORONA] .KGM [JobList]: Holding skycam in the sky for BS3_SIMPLE_INTERIOR_GLOBAL_DATA_COMPLETED_DEFUNCT_BASE_HEIST_INT_REFRESH")
				EXIT
			ENDIF
		ENDIF
		
		#IF FEATURE_CASINO_HEIST
		
		IF (IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		OR IS_THIS_MISSION_A_CASINO_HEIST_MISSION(g_sTransitionSessionData.iCasinoHeistMissingHeaderDataRootId))
		#IF IS_DEBUG_BUILD
		AND NOT g_bLaunchedViaZmenu
		AND NOT g_SimpleInteriorData.bJoinedHeistWithUnknownInterior
		#ENDIF
			IF NOT IS_BIT_SET(g_SimpleInteriorData.iSeventhBS, BS7_SIMPLE_INTERIOR_GLOBAL_DATA_COMPLETED_ARCADE_HEIST_INT_REFRESH)
				PRINTLN("[TS] [CORONA] .KGM [JobList]: Holding skycam in the sky for BS7_SIMPLE_INTERIOR_GLOBAL_DATA_COMPLETED_ARCADE_HEIST_INT_REFRESH")
				EXIT
			ENDIF
		ENDIF
		#ENDIF
		
		#IF FEATURE_HEIST_ISLAND
		IF (IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		OR IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_sTransitionSessionData.iHeistIslandMissingHeaderDataRootId))
		#IF IS_DEBUG_BUILD
		AND NOT g_bLaunchedViaZmenu
		AND NOT g_SimpleInteriorData.bJoinedHeistWithUnknownInterior
		#ENDIF
			IF NOT IS_BIT_SET(g_SimpleInteriorData.iEighthBS, BS8_SIMPLE_INTERIOR_GLOBAL_DATA_COMPLETED_SUBMARINE_HEIST_INT_REFRESH)
				PRINTLN("[TS] [CORONA] .KGM [JobList]: Holding skycam in the sky for BS8_SIMPLE_INTERIOR_GLOBAL_DATA_COMPLETED_SUBMARINE_HEIST_INT_REFRESH")
				EXIT
			ENDIF
		ENDIF
		#ENDIF
	
		IF (IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_FMMC_STRUCT.iRootContentIDHash)
		OR IS_THIS_MISSION_A_TUNER_ROBBERY_FINALE(g_sTransitionSessionData.iTunerRobberyMissingHeaderDataRootId))
		#IF IS_DEBUG_BUILD
		AND NOT g_bLaunchedViaZmenu
		AND NOT g_SimpleInteriorData.bJoinedHeistWithUnknownInterior
		#ENDIF
			IF NOT IS_BIT_SET(g_SimpleInteriorData.iNinthBS, BS9_SIMPLE_INTERIOR_GLOBAL_DATA_COMPLETED_AUTO_SHOP_HEIST_INT_REFRESH)
				PRINTLN("[TS] [CORONA] .KGM [JobList]: Holding skycam in the sky for BS9_SIMPLE_INTERIOR_GLOBAL_DATA_COMPLETED_AUTO_SHOP_HEIST_INT_REFRESH")
				EXIT
			ENDIF
		ENDIF
	
    // KGM 21/3/13: Maintain Skyswoop down if using that method
    IF (g_sJoblistWarpMP.jlwSkyswoop)
        #IF IS_DEBUG_BUILD
            NET_PRINT(".KGM [JobList]: SkySwoop Down") NET_NL()
        #ENDIF
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()
        THEFEED_HIDE_THIS_FRAME()
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
		
		// KGM 9/9/14: BUG 2022895 - Don't skyswoop down if there is a bail condition active, this is causing a skyswoop up/down clash. Allow the bail to cleanup the game.
		IF (IS_TRANSITION_ACTIVE())
            PRINTLN(".KGM [JobList]: ...IGNORE SKYSWOOP DOWN (IS_TRANSITION_ACTIVE() = TRUE) - A BAIL MUST BE GETTING DONE")
			EXIT
		ENDIF
	
		// KGM 1/9/14 (BUG 2004021): If an establishing shot is needed then wait for the conditions to be correct
		IF (g_sJoblistWarpMP.jlwDoEstablishingShot)
			IF (GET_HEIST_CORONA_BLOCKED_STATE() = HEIST_INTRO_STATE_BLOCKED)
			
				PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - HEIST_INTRO_STATE_BLOCKED, check if SkyCam stage is HEIST_PROPERTY_HOLD_STAGE...")
			
				IF NOT GET_HEIST_SUPPRESS_CULL_LEADERS_APARTMENT()
					SET_HEIST_SUPPRESS_CULL_LEADERS_APARTMENT(TRUE)
				ENDIF
				
				HEIST_PROPERTY_CAM_STAGE aStage
				aStage = RUN_HEIST_PROPERTY_CAMERA()
				
				IF aStage = HEIST_PROPERTY_HOLD_STAGE
				    //This means the script camera is rendering the apartment exterior. Do your thing
				    //When you’re finished your thing and want out this stage call SET_HEIST_PROPERTY_CAMERA_TO_MOVE_INTO_INTERIOR()
					PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - SkyCam stage = HEIST_PROPERTY_HOLD_STAGE, setting HEIST_INTRO_STATE_UNBLOCKED.")
					SET_HEIST_CORONA_BLOCKED_STATE(HEIST_INTRO_STATE_UNBLOCKED)
					
					PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - Establishing Shot complete - clearing flag")
					g_sJoblistWarpMP.jlwDoEstablishingShot 		= FALSE
					g_sJoblistWarpMP.jlwEstShotSafetyTimeout	= GET_GAME_TIMER()
					g_sJoblistWarpMP.jlwEstShotInvitingPlayer	= INVALID_PLAYER_INDEX()
					
					PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - Unblocking Corona again to allow it to trigger: ", g_HeistSharedClient.tlHeistContentID)
					Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_By_Filename(g_HeistSharedClient.tlHeistContentID, TRUE)
					g_HeistSharedClient.tlHeistContentID = ""

				ELIF aStage = HEIST_PROPERTY_CLEANUP_CAMERA_DOWN
					IF NOT g_HeistSharedClient.bSupressPropertyCamCleanup
						PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - Setting bSupressPropertyCamCleanup to TRUE.")
						g_HeistSharedClient.bSupressPropertyCamCleanup = TRUE
					ENDIF
					EXIT
					
				ELSE
					PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - SkyCam stage != HEIST_PROPERTY_HOLD_STAGE, wait for SkyCam...")
					
					//If the player we are joining is no longer in a heist then clean up
					BOOL cleanupDueToLeaderQuit = FALSE
					IF NOT (NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex)))
						// ...Leader is no longer in the session, so cleanup
						PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - HEIST_PROPERTY_HOLD_STAGE - Cleanup Establishing Shot: Leader not active in session")
						cleanupDueToLeaderQuit = TRUE
					ELSE
						// ...Leader is still in the session
						IF NOT (IS_THIS_PLAYER_IN_CORONA(INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex)))
							// Leader is not in corona
							PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - HEIST_PROPERTY_HOLD_STAGE - Cleanup Establishing Shot: Leader not in corona")
							cleanupDueToLeaderQuit = TRUE
						ENDIF
					ENDIF
					
					IF (cleanupDueToLeaderQuit)
						PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - HEIST_PROPERTY_HOLD_STAGE - Cleanup Due To Leader Quit: SET_HEIST_PROPERTY_CAMERA_STAT_CLEANUP")
						SET_HEIST_PROPERTY_CAMERA_STAT_CLEANUP()
						
						PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - HEIST_PROPERTY_HOLD_STAGE - Cleanup Due To Leader Quit: setting HEIST_INTRO_STATE_NONE.")
						SET_HEIST_CORONA_BLOCKED_STATE(HEIST_INTRO_STATE_NONE)
						
						PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - HEIST_PROPERTY_HOLD_STAGE - Cleanup Due To Leader Quit: clearing Establishing Shot flag")
						g_sJoblistWarpMP.jlwDoEstablishingShot 		= FALSE
						g_sJoblistWarpMP.jlwEstShotSafetyTimeout	= GET_GAME_TIMER()
						g_sJoblistWarpMP.jlwEstShotInvitingPlayer	= INVALID_PLAYER_INDEX()
						
						PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - HEIST_PROPERTY_HOLD_STAGE - Cleanup Due To Leader Quit: Unblocking Corona again to allow it to trigger: ", g_HeistSharedClient.tlHeistContentID)
						Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_By_Filename(g_HeistSharedClient.tlHeistContentID, TRUE)
						g_HeistSharedClient.tlHeistContentID = ""
					ENDIF
					
					EXIT
				ENDIF
			ELSE
				// Check for a condition that would make the establishing shot quit
				BOOL quitEstablishingShot = FALSE
				BOOL unreservePlayer = TRUE
				
				//If the player we are joining is no longer in a heist then clean up
				IF NOT (NETWORK_IS_PLAYER_ACTIVE(g_sJoblistWarpMP.jlwEstShotInvitingPlayer))
					// ...Leader is no longer in the session, so cleanup
					PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - Waiting For Corona - Cleanup Establishing Shot: Leader not active in session")
					quitEstablishingShot = TRUE
				ELSE
					// ...Leader is still in the session
					IF NOT (IS_THIS_PLAYER_IN_CORONA(g_sJoblistWarpMP.jlwEstShotInvitingPlayer))
						// Leader is not in corona
						PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - Waiting For Corona - Cleanup Establishing Shot: Leader not in corona")
						quitEstablishingShot = TRUE
					ENDIF
				ENDIF
				
				IF NOT (quitEstablishingShot)
					IF (GET_GAME_TIMER() > g_sJoblistWarpMP.jlwEstShotSafetyTimeout)
						PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - Waiting For Corona - Cleanup Establishing Shot: Safety Timeout Expired")
						quitEstablishingShot = TRUE
						
						// KGM 29/1/15 [BUG 2217352]: Don't unreserve the player from the mission on safety timeout if the player is in an activity session and has a Focus Mission.
						//		If in an Activity Session, something has gone wrong. The Joblist establishing shot probably wasn't needed because I don't think it should be possible to accept
						//			a same-session invite in an Activity Session unless something bizarre has gone wrong so unreserving the player on establishing shot
						//			safety timeout could actually cause issues by removing the player from a real corona reservation.
						IF (NETWORK_IS_ACTIVITY_SESSION())
							IF (Is_There_A_MissionsAtCoords_Focus_Mission())
								PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - Player is in an Activity Session and has a Focus Mission. DON'T Unreserve the player.")
								unreservePlayer = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
					
				IF (quitEstablishingShot)
					// If quitting the establishing shot, allow the skyswoop to swoop down without an establishing shot - this will kick in next frame
					PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - Safety Timeout expired. Allow the Skycam to Swoop Down without an establishing shot - clearing flag")
					g_sJoblistWarpMP.jlwDoEstablishingShot 		= FALSE
					g_sJoblistWarpMP.jlwEstShotSafetyTimeout	= GET_GAME_TIMER()
					g_sJoblistWarpMP.jlwEstShotInvitingPlayer	= INVALID_PLAYER_INDEX()
					
					// KGM 11/1/15 [BUG 2192732]: Avoid an ignorable assert caused by passing in a NULL contentID
					TEXT_LABEL_23 contentIdToUse = g_HeistSharedClient.tlHeistContentID
					IF (IS_STRING_NULL_OR_EMPTY(contentIdToUse))
						PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - g_HeistSharedClient.tlHeistContentID is NULL so using g_sJoblistWarpMP.jlwContentID: ", g_sJoblistWarpMP.jlwContentID)
						
						contentIdToUse = g_sJoblistWarpMP.jlwContentID
						
						IF (IS_STRING_NULL_OR_EMPTY(contentIdToUse))
							PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - g_sJoblistWarpMP.jlwContentID is also NULL so can't call Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_By_Filename() with TRUE")
						ENDIF
					ENDIF
					
					IF NOT (IS_STRING_NULL_OR_EMPTY(contentIdToUse))
						Change_MissionsAtCoords_Mission_Do_Not_Trigger_Status_To_Allow_Trigger_By_Filename(contentIdToUse, TRUE)
					ENDIF
					
					PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - setting HEIST_INTRO_STATE_NONE to allow it to trigger")
					SET_HEIST_CORONA_BLOCKED_STATE(HEIST_INTRO_STATE_NONE)
					g_HeistSharedClient.tlHeistContentID = ""
					
					// Also, unreserve the player from any reserved mission just in case the corona is never going to appear - this will prevent the player remaining reserved forever
					IF (unreservePlayer)
						PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - Safety Timeout expired. ALSO: Unreserve player from mission just in case the corona never gets setup")
						Unreserve_MissionsAtCoords_Player_From_Any_PreReserved_Mission()
					ELSE
						PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - Safety Timeout expired. NOTE: Not unreserving the player from any pre-reserved mission.")
					ENDIF
				ELSE
					// Keep waiting for the establishing shot
					#IF IS_DEBUG_BUILD
						INT safetyMsecRemaining = g_sJoblistWarpMP.jlwEstShotSafetyTimeout - GET_GAME_TIMER()
						PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot - HEIST_INTRO_STATE_BLOCKED not yet set - keep waiting for corona. Safety Timeout remaining: ", safetyMsecRemaining, " msec")
					#ENDIF
				ENDIF
				
				EXIT
			ENDIF

		ELSE
			PRINTLN(".KGM [JobList][Heist][AMEC][HEIST_ANIMS] - Establishing Shot not required - Allow a standard Skyswoop down.")
			
	        IF NOT (SET_SKYSWOOP_DOWN(NULL, FALSE, DEFAULT, FALSE))
	            IF IS_SKYCAM_PAST_JUMPCUT_DECENT()
				AND NOT IS_THIS_PLAYER_LEAVING_THE_CORONA(PLAYER_ID())
	                IF NOT ANIMPOSTFX_IS_RUNNING(GET_CORONA_FX_IN())
	                    PRINTLN(".KGM [JobList] ANIMPOSTFX_PLAY(", GET_CORONA_FX_IN(), ", 0, TRUE)")
	                    ANIMPOSTFX_PLAY(GET_CORONA_FX_IN(), 0, TRUE)
	                ENDIF
	            ENDIF
				
				#IF IS_DEBUG_BUILD
				IF GET_HEIST_CORONA_BLOCKED_STATE() = HEIST_INTRO_STATE_BLOCKED
					SET_HEIST_CORONA_BLOCKED_STATE(HEIST_INTRO_STATE_NONE)
				ENDIF
				#ENDIF
	        
	            EXIT
	        ENDIF
			
		ENDIF
    ENDIF
    
    // KGM 12/9/13: To avoid having player control with a blurry screen if the player was walked out of the corona due to a problem as the skyswoop
    //              is ongoing, only switch on teh blur if there is a focus mission, and switch it off if not - this may be wrong!
    IF NOT ANIMPOSTFX_IS_RUNNING(GET_CORONA_FX_IN())
        // blur effect not running
        IF (Is_There_A_MissionsAtCoords_Focus_Mission())
		AND NOT IS_THIS_PLAYER_LEAVING_THE_CORONA(PLAYER_ID())
            // ...there is a focus mission so switch on the blur
            PRINTLN("ANIMPOSTFX_PLAY(", GET_CORONA_FX_IN(), ", 0, TRUE) - after skyswoop, blur was not running and there is a focus mission")
            ANIMPOSTFX_PLAY(GET_CORONA_FX_IN(), 0, TRUE)
        ENDIF
    ELSE
        // blur effect is running
        IF NOT (Is_There_A_MissionsAtCoords_Focus_Mission())
        AND NOT (IS_PLAYER_IN_CORONA())
            // ...there is not a focus mission so switch off the blur
            PRINTLN("[CORONA] CLEANUP_CORONA - STOP_ALL_CORONA_FX_IN() - after skyswoop, blur was running but there is not a focus mission and player is not in a corona")
            STOP_ALL_CORONA_FX_IN()
        ENDIF
    ENDIF

    // Warp Complete
    // UInfreeze unless the player is frozen by the Corona routines
    IF NOT (IS_PED_INJURED(PLAYER_PED_ID()))
        IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
            IF NOT (IS_PLAYER_IN_CORONA())
                FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
            ENDIF
        ENDIF
    ENDIF
    
    #IF IS_DEBUG_BUILD
        NET_PRINT(".KGM [JobList]: ") NET_PRINT_TIME() NET_NL()
        NET_PRINT("     Joblist Warp Completed to: ") NET_PRINT_VECTOR(g_sJoblistWarpMP.jlwOffset) NET_NL()
        NET_PRINT("     Player's Current Position: ") NET_PRINT_VECTOR(GET_PLAYER_COORDS(PLAYER_ID())) NET_NL()
        NET_PRINT("     Mission's Actual Position: ") NET_PRINT_VECTOR(g_sJoblistWarpMP.jlwCoords) NET_NL()
        NET_PRINT("     Buddy In Car value set to: ")
        IF (g_piPlayerInMyVehicleWhenInvited = INVALID_PLAYER_INDEX())
        OR NOT (IS_NET_PLAYER_OK(g_piPlayerInMyVehicleWhenInvited))
            NET_PRINT("NO VALID PLAYER")
        ELSE
            NET_PRINT(GET_PLAYER_NAME(g_piPlayerInMyVehicleWhenInvited))
        ENDIF
        NET_NL()
    #ENDIF
    
    // Switch off Busy Spinner
	IF NOT ARE_WE_CURRENTLY_GETTING_CONTENT_BY_ID()
	AND NOT IS_CORONA_DOWNLOADING_FROM_UGC_SERVER()
		BUSYSPINNER_OFF()
	    #IF IS_DEBUG_BUILD
	        NET_PRINT(".KGM [JobList]: ") NET_PRINT_TIME() NET_PRINT("BUSYSPINNER OFF") NET_NL()
	    #ENDIF
	ENDIF
	
	//Reset the flag that an external heist camera is Running. 
	SET_PROPERTY_SKYCAM(FALSE)
    
    // After using a teleport, ensure the mission gets temporarily unlocked so that the player can join it
    IF NOT (g_sJoblistWarpMP.jlwSkyswoop)
        Temporarily_Unlock_MissionsAtCoords_Mission_By_Filename(g_sJoblistWarpMP.jlwContentID)
    ENDIF

    // Give the player control back and make visible and fade in, unless the player is in a corona
    IF NOT (IS_PLAYER_IN_CORONA())
	AND HeistPropertyCamStages != HEIST_PROPERTY_HOLD_STAGE
	AND HeistPropertyCamStages != HEIST_PROPERTY_SETUP_STATIC_CAM
	AND HeistPropertyCamStages != HEIST_PROPERTY_CLEANUP_CAMERA_DOWN
	AND HeistPropertyCamStages != HEIST_PROPERTY_HOLD_STAGE
	AND HeistPropertyCamStages != HEIST_PROPERTY_LOAD_INTERIOR
	AND HeistPropertyCamStages != HEIST_PROPERTY_WAIT_FOR_LOAD
        NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
    ENDIF
        
    Clear_Joblist_Warp_Details()
    
    // JamesA: Prevent wanted level from being awarded if player warps to job 
    // (May need a global flag to monitor this if used for ambient activities around FM)
    UPDATE_PLAYER_WANTED_LEVEL_JOINING_MISSION()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain a flag of unread invites from players so that the icon can display beside the radar
// NOTES:	KGM 6/11/14 [BUG 2060056]
PROC Maintain_Unread_Player_Invites_Indicator()

	g_displayPlayerInvitesIndicator = FALSE
	
	INT tempLoop = 0
	
	// Any unread player same-session invites?
	REPEAT g_numMPJobListInvitations tempLoop
		IF NOT (g_sJLInvitesMP[tempLoop].jliInviteRead) 
		AND (g_sJLInvitesMP[tempLoop].jliInvitorIsPlayer)
		AND (g_sJLInvitesMP[tempLoop].jliActive)
		AND (g_sJLInvitesMP[tempLoop].jliAllowDisplay)
			g_displayPlayerInvitesIndicator = TRUE
			EXIT
		ENDIF
	ENDREPEAT
	
	// Any unread player cross-session invites?
	REPEAT g_numMPCSInvites tempLoop
		IF NOT (g_sJLCSInvitesMP[tempLoop].jcsiInviteRead) 
		AND (g_sJLCSInvitesMP[tempLoop].jcsiActive)
		AND (g_sJLCSInvitesMP[tempLoop].jcsiAllowDisplay)
			g_displayPlayerInvitesIndicator = TRUE
			EXIT
		ENDIF
	ENDREPEAT
	
	// Any unread basic invites that should display the Player Invite indicator?
	// KGM 29/4/15 [BUG 2327028]: Ensure Friends QuickMatch displays the Player Invite indicator
	REPEAT g_numMPRVInvites tempLoop
		IF NOT (g_sJLRVInvitesMP[tempLoop].jrviInviteRead) 
		AND (g_sJLRVInvitesMP[tempLoop].jrviInvitorIsPlayer)
		AND (g_sJLRVInvitesMP[tempLoop].jrviActive)
		AND (g_sJLRVInvitesMP[tempLoop].jrviAllowDisplay)
			SWITCH (g_sJLRVInvitesMP[tempLoop].jrviType)
				// Basic Invites types that should display the invite indicator
				CASE FMMC_TYPE_GROUP_QUICKMATCH
					g_displayPlayerInvitesIndicator = TRUE
					EXIT
			ENDSWITCH
		ENDIF
	ENDREPEAT

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the control flags that allow a specific help message and the Invites Indicator to appear over the Corona screens
PROC Maintain_Corona_Invites_Controls()

	// If the player isn't in a corona, then clear the control flags
	IF (g_forceDisplayPlayerInvitesIndicator)
		IF NOT (IS_PLAYER_IN_CORONA())
		OR (IS_CUTSCENE_PLAYING())
			#IF IS_DEBUG_BUILD
				IF NOT (IS_PLAYER_IN_CORONA())
					PRINTLN(".KGM [JobList]: Player is no longer in a corona. Clear g_forceDisplayPlayerInvitesIndicator flag.")
				ELSE
					PRINTLN(".KGM [JobList]: Player is now playing a mocap cutscene. Clear g_forceDisplayPlayerInvitesIndicator flag.")
				ENDIF
				
				IF (g_displayCoronaInviteHelp)
					PRINTLN(".KGM [JobList]: Player is no longer in a corona. Clear g_displayCoronaInviteHelp flag.")
				ENDIF
			#ENDIF
			
			g_displayCoronaInviteHelp			 = FALSE
			g_forceDisplayPlayerInvitesIndicator = FALSE
			
			EXIT
		ENDIF
	ENDIF

	// If the 'allow help' flag is on display, clear it once the help is being displayed
	IF (g_displayCoronaInviteHelp)
		IF (IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CORONA_INVITES"))
			g_displayCoronaInviteHelp = FALSE
			
			PRINTLN(".KGM [JobList]: Corona Invites Help Message now on display. Clear g_displayCoronaInviteHelp flag.")
		ENDIF
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: A regular maintenance function.
//
// INPUT PARAMS:        paramFromSP     [DEFAULT = FALSE] TRUE means the update request is from SP, FALSE means from MP
//
// NOTES:   On timeout, will set the 'doUpdate' flag if the F9 screen is active or the Joblist App is on display
PROC Maintain_MP_Joblist_Update(BOOL paramFromSP = FALSE)
	
	IF GET_GAME_TIMER() < g_inviteControlTimeout
		Ensure_Exclusive_Joblist_Controls()		
	ENDIF
	
	// A debug-only header download consistency check - a max of one of these should be TRUE at any one time
	#IF IS_DEBUG_BUILD
		IF (g_invitationBeingDownloaded)
		AND (g_csInviteBeingDownloaded)
        	NET_PRINT("...KGM MP [JobList]: ") NET_PRINT_TIME() NET_PRINT("ERROR: Both an Invitation and a CSInvite thinks they are downloading a mission header at the same time") NET_NL()
			SCRIPT_ASSERT("Maintain_MP_Joblist_Update() - ERROR: Both an Invitation and a CSInvite are downloading a mission header at the same time. Tell Keith.")
		ENDIF
	#ENDIF

    // A joblist fadeout and warp needs maintained every frame until it completes
    IF NOT (paramFromSP)
        // MP only
        Maintain_Invite_Acceptance()
        Maintain_Joblist_FadeOut_And_Warp()
        Maintain_DPADUP_Invite_Help_Flag()
		Maintain_Post_Player_Warp_Delay()
    ENDIF
    
    // Check the joblist invitations to see if they are still valid
    IF NOT (paramFromSP)
        // MP only
        Maintain_Joblist_Invitations()
        Maintain_Joblist_RVInvites()
        Maintain_Joblist_App_Autolaunch()
		Maintain_Cross_Session_Invite_Recent_Acceptance_Timer()
    ENDIF
	
    Maintain_Joblist_CSInvites()
	Maintain_Unread_Player_Invites_Indicator()
	Maintain_Corona_Invites_Controls()
    
    // Clear the updated flag
    g_sJoblistUpdate.isUpdated = FALSE
    
    // Clear the Wanted Level Feed Message flag if there is no wanted level
// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
//    IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)
//        IF (g_sJoblistUpdate.wantedFeedDisplayed)
//            g_sJoblistUpdate.wantedFeedDisplayed = FALSE
//            
//            THEFEED_REMOVE_ITEM(g_sJoblistUpdate.wantedFeedID)
//            g_sJoblistUpdate.wantedFeedID = NO_JOBLIST_INVITE_FEED_ID
//        ENDIF
//    ENDIF
    
    // Check for timeout
    IF NOT (Has_Joblist_Update_Timer_Expired())
        EXIT
    ENDIF
    
    // Timer has expired, so reset the timer
    Set_Joblist_Update_Timeout()
    
    // Perform the update if allowed
    BOOL doUpdate = FALSE
    // ...check if the joblist app is active
    IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appMPJobListNew")) > 0)
        // ...joblist is active, but don't do the update if the app has disabled updates
        IF NOT (g_sJoblistUpdate.joblistActiveUpdate)
            EXIT
        ENDIF
        
        // Joblist is active and it hasn't disabled updates, so do the update
        doUpdate = TRUE
    ENDIF
    
    // ...if the joblist app isn't active or already reequesting an update, then do the update if the F9 screen is on-display
    #IF IS_DEBUG_BUILD
        IF NOT (doUpdate)
        AND NOT (paramFromSP)
            // MP F9 screen only
            IF (g_MPF9ScreenIsRendering)
                doUpdate = TRUE
            ENDIF
        ENDIF
    #ENDIF
    
    // ...do the update if Brenda's ToDo list is on screen if nothing else has already requested it
    IF NOT (doUpdate)
    AND NOT (paramFromSP)
        // MP Only
        IF (g_sJoblistUpdate.missionsOnScreen)
            doUpdate = TRUE
        ENDIF
    ENDIF
	
	// KGM 27/3/14: Do an update if the 'new job' indicator is set so that it can be removed if there are no invites remaining in the joblist
	BOOL checkNewJobIndicatorValidity = FALSE
	IF NOT (doUpdate)
	AND NOT (paramFromSP)
		IF (g_numNewJoblistInvites > 0)
			doUpdate = TRUE
			checkNewJobIndicatorValidity = TRUE
		ENDIF
	ENDIF
    
    // Do the update?
    IF NOT (doUpdate)
        EXIT
    ENDIF

    // Perform the update and set the 'updated' global if necessary - this global will be used to decide if the scaleform display needs refreshed if the joblist is active
    g_sJoblistUpdate.isUpdated = Update_JobList_Details()
    
    // Clear the 'missions onscreen' flag - this menas it may stay 'true' for a few frames after it has been switched off, but that just means the joblist update may occur
    //  one extra time when it wasn't necessary
    g_sJoblistUpdate.missionsOnScreen = FALSE
	
	// KGM 27/3/14: If the update was to check the validity of the New Job indicator, then clear it if there are no joblist entries
	IF (checkNewJobIndicatorValidity)
		IF (g_numMPJobListEntries = 0)
		
			IF NOT (paramFromSP)
				// Exit the reset if we have Biker invites in the JobList
				IF g_sBossAgencyApp.iTotalUnreadBikerInvites > 0
					EXIT
				ENDIF
			ENDIF
		
			#IF IS_DEBUG_BUILD
        		NET_PRINT("...KGM MP [JobList]: ") NET_PRINT_TIME() NET_PRINT("There are no Joblist Entries, so clearing 'New Job' flag and indicator if relevant") NET_NL()
			#ENDIF
			// The cellphone indicator
			PRINTLN("[MMM][JobList] Setting g_numNewJoblistInvites to 0 as there are no entries")
		    Reset_New_Invites_Indicator("[MMM][JobList] Calling Reset_New_Invites_Indicator from Maintain_MP_Joblist_Update")
			Dynamically_Update_New_Invites_Indicator()
			PRINTLN("*** KGMTIME: Clearing New Job Indicator")
		ENDIF
	ENDIF
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Maintenance Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Cleanup any 500-char descriptions that are still lingering around
PROC Terminate_MP_Joblist_Update()

    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Terminate MP Joblist - clean up any 500-char descriptions")
        NET_NL()
    #ENDIF

    INT tempLoop = 0

    // Invitations
    REPEAT g_numMPJobListInvitations tempLoop
        IF NOT (g_sJLInvitesMP[tempLoop].jliRequireDesc)
            IF (g_sJLInvitesMP[tempLoop].jliDescHash != 0)
                UGC_RELEASE_CACHED_DESCRIPTION(g_sJLInvitesMP[tempLoop].jliDescHash)
                
                #IF IS_DEBUG_BUILD
                    NET_PRINT("      ")
                    NET_PRINT_INT(tempLoop)
                    NET_PRINT(" [Invitation] Requested removal of 500-char description with this hash: ")
                    NET_PRINT_INT(g_sJLInvitesMP[tempLoop].jliDescHash)
                    NET_NL()
                #ENDIF
            ENDIF
        ENDIF
    ENDREPEAT
    
    // CS Invitations
    REPEAT g_numMPCSInvites tempLoop
        IF NOT (g_sJLCSInvitesMP[tempLoop].jcsiRequireDesc)
            IF (g_sJLCSInvitesMP[tempLoop].jcsiDescHash != 0)
                UGC_RELEASE_CACHED_DESCRIPTION(g_sJLCSInvitesMP[tempLoop].jcsiDescHash)
                
                #IF IS_DEBUG_BUILD
                    NET_PRINT("      ")
                    NET_PRINT_INT(tempLoop)
                    NET_PRINT(" [CS Invite]  Requested removal of 500-char description with this hash: ")
                    NET_PRINT_INT(g_sJLCSInvitesMP[tempLoop].jcsiDescHash)
                    NET_NL()
                #ENDIF
            ENDIF
        ENDIF
    ENDREPEAT
	
	// Clear the 'Unread Player Invites' indicator
	g_displayPlayerInvitesIndicator = FALSE

ENDPROC




// ===========================================================================================================
//      The Joblist External Access Functions
// ===========================================================================================================

// PURPOSE: Retrieve the FM Type of the first job in the FM joblist (updates the joblist if not recently updated)
//
// RETURN VALUE:            INT         FM Mission Type, or NO_FM_JOINABLE_JOBS
//
// NOTES:   Re-generating the joblist is expensive so I can't have external routines updating it all the time, so I'm making this as specific as possible for DaveW who will schedule the request.
//          This is used by Dave to send a text message about joinable activities to players that haven't done anything for a while.
FUNC INT Get_A_Joinable_FM_Mission_Type()

    // Outputting a message when this is called - this will deliberately spam if there are too many calls so I should hopefully spot this in some bug's logs and correct it
    // NOTE: Not the greatest way to control this, but it was added for Dave Watson who is aware of the issues and is going to schedule this infrequently - the spam is for additional unexpected function callers
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP: Get_A_Joinable_FM_Mission_Type() - called by ")
        NET_PRINT(GET_THIS_SCRIPT_NAME())
        NET_PRINT(" - EXPENSIVE FUNCTION: THIS SHOULD ONLY BE GETTING CALLED INFREQUENTLY")
        NET_NL()
    #ENDIF
    
    // This should only be called in Freemode
    IF (GET_CURRENT_GAMEMODE() != GAMEMODE_FM)
        #IF IS_DEBUG_BUILD
            NET_PRINT("Get_A_Joinable_FM_Mission_Type() - ERROR: Freemode-only function called when not in Freemode. This may be due to performing a transition session") NET_NL()
        #ENDIF
        
        RETURN NO_FM_JOINABLE_JOBS
    ENDIF
    
    // Because this is only intended for use by DaveW, I'm going to regenerate the data each time he calls it so that the information is fully up-to-date. Dave will handle scheduling within his routine.
    // Don't do the update if the app has disabled updates - this means the joblist App is on-screen and active
    IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appMPJobListNew")) > 0)
        IF NOT (g_sJoblistUpdate.joblistActiveUpdate)
            RETURN NO_FM_JOINABLE_JOBS
        ENDIF
    ENDIF
    
    // Don't do the update if the update has already occurred this frame, just return the generated details
    IF NOT (g_sJoblistUpdate.isUpdated)
        // ...update the Joblist - may as well update the isUpdated flag too - if the joblist is on-screen then this will allow it to refresh to the latest data
        PRINTLN("[MMM] Called from Get_A_Joinable_FM_Mission_Type")
		g_sJoblistUpdate.isUpdated = Update_JobList_Details()
    ENDIF
    
    // If there are no entries then do nothing more
    IF (g_numMPJobListEntries = 0)
        RETURN NO_FM_JOINABLE_JOBS
    ENDIF
    
    // If there are joinable activities then the first joblist array entry should be marked as containing a joinable activity
    IF (g_sJobListMP[0].jlState != MP_JOBLIST_JOINABLE_MISSION)
    AND (g_sJobListMP[0].jlState != MP_JOBLIST_JOINABLE_PLAYLIST)
        RETURN NO_FM_JOINABLE_JOBS
    ENDIF
    
    // Return the FM Mission Type of the first joblist array entry
    RETURN (g_sJobListMP[0].jlMissionAsInt)

ENDFUNC



// ===========================================================================================================
//      Invites On Joblist Public Access Functions
// ===========================================================================================================

// PURPOSE: Make sure that the strings passed with an invite are of an appropriate length
//
// INPUT PARAMS:            paramMissionName    The Mission Name (as a STRING)
//                          paramDescription    The Mission Description (as a STRING)
// RETURN VALUE:            BOOL                TRUE if the data is good to go, FALSE if there is a problem
FUNC BOOL Check_Consistency_Of_Invite_Data_Strings(STRING paramMissionName, STRING paramDescription)
    
    // Do some consistency checking on the STRINGS
    IF NOT (IS_STRING_NULL_OR_EMPTY(paramMissionName))
        IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramMissionName) > MAX_JOBLIST_MISSION_NAME_STRING_LENGTH)
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("ERROR: Mission Name string is too long: ")
                NET_PRINT(paramMissionName)
                NET_PRINT("  (Expected Max Length: ")
                NET_PRINT_INT(MAX_JOBLIST_MISSION_NAME_STRING_LENGTH)
                NET_PRINT(")")
                NET_NL()
                SCRIPT_ASSERT("Check_Consistency_Of_Invite_Data_Strings(): The Mission Name string is longer than expected. Tell Keith.")
            #ENDIF
            
            RETURN FALSE
        ENDIF
    ENDIF

    IF NOT (IS_STRING_NULL_OR_EMPTY(paramDescription))
        IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramDescription) > MAX_JOBLIST_MISSION_DESC_STRING_LENGTH)
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("ERROR - Mission Description string is too long: ")
                NET_NL()
                NET_PRINT("                     ")
                NET_PRINT(paramDescription)
                NET_PRINT("  (Expected Max Length: ")
                NET_PRINT_INT(MAX_JOBLIST_MISSION_DESC_STRING_LENGTH)
                NET_PRINT(")")
                NET_NL()
                SCRIPT_ASSERT("Check_Consistency_Of_Invite_Data_Strings(): The Description string is longer than expected. Tell Keith.")
            #ENDIF
            
            RETURN FALSE
        ENDIF
    ENDIF
    
    // Strings OK
    RETURN TRUE
    
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this invite needs a reminder feed message to be sent shortly afterwards
//
// INPUT PARAMS:			paramPlayerID              	The Invitor's PlayerIndex
// RETURN VALUE:			BOOL						TRUE if a reminder is required, otherwise FALSE
//
// NOTES:	A reminder should be sent for an invite to a single job from a Friend
FUNC BOOL Check_If_Invite_Reminder_Will_Be_Required(PLAYER_INDEX paramPlayerID)

	IF (paramPlayerID = INVALID_PLAYER_INDEX())
		// ...invitor is not a friend
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Invite Reminder not required: Invitor is an NPC.")
            NET_NL()
		#ENDIF
				
		RETURN FALSE
	ENDIF

	IF NOT (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
		// ...invitor is not OK
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Invite Reminder not required: Inviting player is not OK.")
            NET_NL()
		#ENDIF
				
		RETURN FALSE
	ENDIF

	GAMER_HANDLE invitorGH = GET_GAMER_HANDLE_PLAYER(paramPlayerID)
	IF NOT (NETWORK_IS_FRIEND(invitorGH))
		// ...not a friend
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Invite Reminder not required: Invitor is not a friend.")
            NET_NL()
		#ENDIF
				
		RETURN FALSE
	ENDIF
	
	IF (IS_PLAYER_ON_A_PLAYLIST(paramPlayerID))
		// ...ignore for playlists (although we can probably allow this - just worried the data goes out of date)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Invite Reminder not required: Invite is to a playlist.")
            NET_NL()
		#ENDIF
				
		RETURN FALSE
	ENDIF
	
	// Allow a Reminder to be sent
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Invite Reminder ALLOWED.")
        NET_NL()
	#ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store details on the Invite Details array to display on the Joblist where appropriate
//
// INPUT PARAMS:            paramType               Type of Mission
//							paramSubtype			Mission Subtype
//                          paramCoords             Location of Mission
//                          paramCreatorID          CreatorID of mission
//                          paramInvitorAsInt       Player Index or enumCharacterList of player or in-game character that sent the invite (as an INT)
//                          paramInvitorIsPlayer    TRUE if the invitor is a player, FALSE if it is an in-game charancter
//                          paramMissionName        The Mission Name (as a STRING)
//                          paramDescription        The Mission Description (as a STRING)
//                          paramContentID          The ContentID
//                          paramOnHours            The On Hours for the invite (as a bitset where the first 24 bits each represent an hour)
//							paramReAdding			TRUE if the invite is being re-added after transition, FALSE if not
//							paramDownload			TRUE if the mission header details need to be downloaded, FALSE if not
//							paramMinPlayers			The minimum number of players for the mission
//							paramMaxPlayers			The maximum number of players for the mission
//							paramUnplayed			TRUE if the mission is UNPLAYED, FALSE if the mission is played or teh data isn't locally available, or is a minigame, etc
//							paramHeistPackedInt		Used to setup correct apartment when accepting a same-session invite to a Heist Corona
//							paramPushBikeOnly		TRUE if the mission is a BIKE race that is a pushbike race (rather than a motorbike race), FALSE in all other situations
//							paramCheckGamerToGamer	TRUE if the gamer to gamer content restriction check [used by Durango as refinement] needs to be done, otherwise FALSE
PROC private_Store_Invite_Details_On_Joblist(	INT paramType, INT paramSubtype, VECTOR paramCoords, INT paramCreatorID, INT paramInvitorAsInt, BOOL paramInvitorIsPlayer,
												STRING paramMissionName, STRING paramDescription, TEXT_LABEL_23 paramContentID, INT jliRootContentIdHash, INT paramOnHours, BOOL paramReAdding,
												BOOL paramToDownload, INT paramMinPlayers, INT paramMaxPlayers, BOOL paramUnplayed, INT paramHeistPackedInt, BOOL paramPushBikeOnly,
												BOOL paramCheckGamerToGamer, INT jliAdversaryModeType, BOOL jliQuickMatch = FALSE)
    
    PLAYER_INDEX        thePlayer   = INVALID_PLAYER_INDEX()
    enumCharacterList   theNPC      = NO_CHARACTER
    
    IF (paramInvitorIsPlayer)
        thePlayer   = INT_TO_PLAYERINDEX(paramInvitorAsInt)
    ELSE
        theNPC      = INT_TO_ENUM(enumCharacterList, paramInvitorAsInt)
    ENDIF

    // Ignore Invitations from players for solo sessions (just in case - these should never happen!)
    IF (paramInvitorIsPlayer)
        IF (Joblist_Is_This_A_Solo_Session())
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("SOLO SESSION - Ditching Invitation from player: ")
                NET_PRINT(GET_PLAYER_NAME(thePlayer))
                NET_NL()
            #ENDIF
		
			// Let the invitor know
			BROADCAST_JOBLIST_INVITE_REPLY(thePlayer, TRUE, FALSE)
            
            EXIT
        ENDIF
    ENDIF
    
    // This should be the only place that adds entries to the array, so move the array contents down to make room at slot 0
    INT toPos = g_numMPJobListInvitations
    IF (toPos >= MAX_MP_JOBLIST_ENTRIES)
        toPos--
    ENDIF
    
    INT fromPos = toPos - 1
    WHILE (fromPos >= 0)
        g_sJLInvitesMP[toPos] = g_sJLInvitesMP[fromPos]
        toPos--
        fromPos--
    ENDWHILE
    
    // There is a gap now at slot 0, so increase the number of entries on the array
    g_numMPJobListInvitations += 1
    IF (g_numMPJobListInvitations > MAX_MP_JOBLIST_ENTRIES)
        g_numMPJobListInvitations = MAX_MP_JOBLIST_ENTRIES
    ENDIF
    
    // NOTE: Storage Slot should always be 0 for a new entry
    INT storageSlot = 0
    
    g_sJLInvitesMP[storageSlot].jliActive                   = FALSE
    g_sJLInvitesMP[storageSlot].jliAllowDisplay             = FALSE
    g_sJLInvitesMP[storageSlot].jliInviteRead               = FALSE
    g_sJLInvitesMP[storageSlot].jliReAdding					= paramReAdding
    g_sJLInvitesMP[storageSlot].jliCoords                   = paramCoords
    g_sJLInvitesMP[storageSlot].jliPlayer                   = thePlayer
    g_sJLInvitesMP[storageSlot].jliNPC                      = theNPC
    g_sJLInvitesMP[storageSlot].jliInvitorIsPlayer          = paramInvitorIsPlayer
    g_sJLInvitesMP[storageSlot].jliCreatorID                = paramCreatorID
    g_sJLInvitesMP[storageSlot].jliType                     = paramType
	g_sJLInvitesMP[storageSlot].jliSubtype					= paramSubtype
	g_sJLInvitesMP[storageSlot].jliRootContentIdHash		= jliRootContentIdHash
	g_sJLInvitesMP[storageSlot].jliAdversaryModeType		= jliAdversaryModeType
	g_sJLInvitesMP[storageSlot].jliQuickMatch				= jliQuickMatch
    g_sJLInvitesMP[storageSlot].jliDescHash                 = Get_MissionDescHash_From_Creator_And_ContentID(paramCreatorID, paramContentID)
    g_sJLInvitesMP[storageSlot].jliDescLoadVars.nHashOld    = 0
    g_sJLInvitesMP[storageSlot].jliDescLoadVars.iRequest    = 0
    g_sJLInvitesMP[storageSlot].jliDescLoadVars.bSucess     = FALSE
    g_sJLInvitesMP[storageSlot].jliRequireDesc              = TRUE
	g_sJLInvitesMP[storageSlot].jliUseHeistCutsceneDesc		= FALSE
    g_sJLInvitesMP[storageSlot].jliMissionName              = paramMissionName
    g_sJLInvitesMP[storageSlot].jliDescription              = paramDescription
    g_sJLInvitesMP[storageSlot].jliHeadshotTimeout          = GET_TIME_OFFSET(GET_NETWORK_TIME(), MAX_INVITE_DELAY_FOR_HEADSHOT_msec)
    g_sJLInvitesMP[storageSlot].jliFeedID                   = NO_JOBLIST_INVITE_FEED_ID
    // Set the 'allow again' timeout time for this invite - withint this timeout period duplicate entries wuill be ignore to avoid autofill spamming
    g_sJLInvitesMP[storageSlot].jliAllowAgainTimeout        = GET_TIME_OFFSET(GET_NETWORK_TIME(), ALLOW_DUPLICATE_INVITE_DELAY_msec)
    
    g_sJLInvitesMP[storageSlot].jliContentID                = paramContentID
    g_sJLInvitesMP[storageSlot].jliOnHours                  = paramOnHours
	g_sJLInvitesMP[storageSlot].jliMinPlayers				= paramMinPlayers
	g_sJLInvitesMP[storageSlot].jliMaxPlayers				= paramMaxPlayers
	g_sJLInvitesMP[storageSlot].jliIsUnplayed				= paramUnplayed
	g_sJLInvitesMP[storageSlot].jliIsPushBikeOnly 			= paramPushBikeOnly
	g_sJLInvitesMP[storageSlot].jliDoGamerToGamerCheck		= paramCheckGamerToGamer

	
	// Check this after all other details are setup    
	g_sJLInvitesMP[storageSlot].jliIsDuplicate              = Check_If_Duplicate_Invite(storageSlot)	// Moved this to the bottom so we can have access to the other information
	
	// KGM 15/6/14: Store the Heist Packed Int, as required to setup the correct apartment when accepting a SAME-SESSION (only) invite into a Heist Corona
	// NOTE: For cross-session invites the apartment init gets this info from transition variables
	g_sJLInvitesMP[storageSlot].jliHeistPackedInt			= paramHeistPackedInt
	
	// Friends will get a repeat of the invite as a reminder shortly afterwards
	g_sJLInvitesMP[storageSlot].jliRepeatInvite				= Check_If_Invite_Reminder_Will_Be_Required(thePlayer)
	g_sJLInvitesMP[storageSlot].jliRepeatTimeout			= 0
	
	// 1987439 - Added the readding check to sort ordering problems when transistioning and keeping contact mission invites
	IF g_sJLInvitesMP[storageSlot].jliReAdding
		g_sJLInvitesMP[storageSlot].jliOrderIndex 			= 0
	ELSE	
		g_sJLInvitesMP[storageSlot].jliOrderIndex			= g_inviteOrderCounter	
	ENDIF 
	g_inviteOrderCounter++	// MMM 1645958 - Added to store the order of the invites

	
	// KGM 9/12/13: For Head-To-Head Playlists, store the wager amount
	g_sJLInvitesMP[storageSlot].jliWagerH2H = -1
	IF (paramInvitorIsPlayer)
		IF (IS_PLAYER_ON_A_PLAYLIST(thePlayer))
			IF (IS_PLAYER_ON_A_H2H_PLAYLIST(thePlayer))
				g_sJLInvitesMP[storageSlot].jliWagerH2H = GlobalplayerBD_FM[NATIVE_TO_INT(thePlayer)].sClientCoronaData.iPlaylistWager
			ENDIF
		ENDIF
	ENDIF
    
    // A description isn't needed for Minigames
    IF (paramCreatorID = FMMC_MINI_GAME_CREATOR_ID)
        g_sJLInvitesMP[storageSlot].jliRequireDesc = FALSE
    ENDIF
	
	// A description isn't needed for NPC Invites that have passed specific description text
	// KGM 4/11/14 [BUG 2111097]: Now using the old description field to allow NPC Invites to use specific description text (for first Heist Versus Invite)
	IF NOT (paramInvitorIsPlayer)
		IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJLInvitesMP[storageSlot].jliDescription))
			g_sJLInvitesMP[storageSlot].jliRequireDesc = FALSE
		ENDIF
	ENDIF
	
	// Check if this is an invite into a Heist Cutscene and, if so, use the Heist Umbrella Description instead
	IF (paramInvitorIsPlayer)
		IF (g_sJLInvitesMP[storageSlot].jliType = FMMC_TYPE_MISSION)
			IF (g_sJLInvitesMP[storageSlot].jliSubtype = FMMC_MISSION_TYPE_HEIST)
				IF NOT (IS_STRING_NULL_OR_EMPTY(g_sJLInvitesMP[storageSlot].jliDescription))
					IF (ARE_STRINGS_EQUAL(g_sJLInvitesMP[storageSlot].jliDescription, "[HEIST PREP CUTSCENE]"))
						// Clear the 'require desc' flag and set the 'use heist umbrella desc' flag. Leave the description as-is so it can be passed to the Joblist app.
						g_sJLInvitesMP[storageSlot].jliRequireDesc			= FALSE
						g_sJLInvitesMP[storageSlot].jliUseHeistCutsceneDesc	= TRUE
						
					    #IF IS_DEBUG_BUILD
					        NET_PRINT("...KGM MP [JobList]: ")
					        NET_PRINT_TIME()
					        NET_PRINT("This is a Heist Cutscene, so use the umbrella description")
							NET_NL()
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
    
    // Update the 'Recent Invite' timer when an invite is accepted (this will block new phonecalls even if the invite can't yet be activated)
    Update_Recent_Invite_Received_Timer()
	
	// Set the 'download' flags
	IF (paramToDownload)
		// ...details require downloaded
		g_sJLInvitesMP[storageSlot].jliToDownload		= TRUE
		g_sJLInvitesMP[storageSlot].jliDownloading		= FALSE
		g_sJLInvitesMP[storageSlot].jliDownloaded		= FALSE
	ELSE
		// ...details available
		g_sJLInvitesMP[storageSlot].jliToDownload		= FALSE
		g_sJLInvitesMP[storageSlot].jliDownloading		= FALSE
		g_sJLInvitesMP[storageSlot].jliDownloaded		= TRUE
	ENDIF
	
	// KGM 12/7/14: NextGen only. An Asynchronous Permissions Check is required on NextGen for player to player invites.
	//				This ensures that an invite received by this player from another player that this player has blocked doesn't get received.
	// NOTE: ONLY DO THIS FOR XBOX ONE.
	g_sJLInvitesMP[storageSlot].jliPermissionCheckID	= 0
	g_sJLInvitesMP[storageSlot].jliDoingPermissionCheck	= FALSE
	g_sJLInvitesMP[storageSlot].jliPermissionCheckFail	= FALSE
	
	IF (IS_XBOX_PLATFORM())
		IF (paramInvitorIsPlayer)
			// KGM SAFETY SWITCH-OFF
			BOOL checkPermissions = TRUE
			#IF IS_DEBUG_BUILD
				IF (GET_COMMANDLINE_PARAM_EXISTS("sc_tempKeithNGBlockCheckOff"))
					checkPermissions = FALSE
					PRINTLN(".KGM [JobList]: Invite Asynchronous permissions start check OFF because of -sc_tempKeithNGBlockCheckOff commandline parameter")
				ENDIF
			#ENDIF
			
			IF (checkPermissions)
			// END KGM SAFETY SWITCH-OFF
		
			// Setup the Asynchronous Permissions check and store the return ID
			GAMER_HANDLE ghInvitingPlayer = GET_GAMER_HANDLE_PLAYER(thePlayer)
			INT theCheckID = NETWORK_START_COMMUNICATION_PERMISSIONS_CHECK(ghInvitingPlayer)
			
			IF (theCheckID != -1)
				g_sJLInvitesMP[storageSlot].jliPermissionCheckID	= theCheckID
				g_sJLInvitesMP[storageSlot].jliDoingPermissionCheck	= TRUE
				g_sJLInvitesMP[storageSlot].jliPermissionCheckFail	= FALSE
				
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [JobList]: Starting Asynchronous Permissions check on GH of this Inviting Player: ", GET_PLAYER_NAME(thePlayer), ". CheckID: ", g_sJLInvitesMP[storageSlot].jliPermissionCheckID)
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [JobList]: Asynchronous Permissions check on GH of this Inviting Player: ", GET_PLAYER_NAME(thePlayer), " returned a CheckID of -1. - IGNORING CHECK. ALLOWING INVITE")
				#ENDIF
			ENDIF

			// KGM SAFETY SWITCH-OFF
			ENDIF
			// END KGM SAFETY SWITCH-OFF
		ENDIF
	ENDIF
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Storing Invitation Details At Array Location: ")
        NET_PRINT_INT(storageSlot)
		NET_PRINT("  Display Order: ")
		NET_PRINT_INT(g_sJLInvitesMP[storageSlot].jliOrderIndex)
        NET_NL()
        NET_PRINT("      ContentID: ")
        NET_PRINT(paramContentID)
        NET_PRINT("   Coords: ")
        NET_PRINT_VECTOR(paramCoords)
        TEXT_LABEL_63 hoursOpenString = Convert_Hours_From_Bitfield_To_TL(paramOnHours)
        NET_PRINT("   On Hours: ")
        NET_PRINT(hoursOpenString)
        NET_PRINT("   Heist PackedInt: ")
        NET_PRINT_INT(paramHeistPackedInt)
		IF (g_sJLInvitesMP[storageSlot].jliWagerH2H >= 0)
			NET_PRINT("   H2H Wager: ")
			NET_PRINT_INT(g_sJLInvitesMP[storageSlot].jliWagerH2H)
		ENDIF
		IF (g_sJLInvitesMP[storageSlot].jliIsPushBikeOnly)
			NET_PRINT(" [CYCLE RACE]")
		ENDIF
		IF (g_sJLInvitesMP[storageSlot].jliRepeatInvite)
	        NET_PRINT("  [ALLOW REPEAT]")
		ENDIF
		NET_PRINT("   Min - Max Players: ")
		IF (paramInvitorIsPlayer)
			IF (paramToDownload)
				NET_PRINT("[Gathered when header data downloaded]")
			ELSE
				NET_PRINT_INT(paramMinPlayers)
				NET_PRINT(" - ")
				NET_PRINT_INT(paramMaxPlayers)
			ENDIF
		ELSE
			NET_PRINT("[Calculated later for NPC Invites]")
		ENDIF
		NET_NL()
        NET_PRINT("         ")
        IF (Check_If_Corona_Still_Has_Space_For_More_Players(storageSlot))
            NET_PRINT(" [corona allows more players]")
        ELSE
            NET_PRINT(" [CORONA FULL OR DATA INCONSISTENT]")
        ENDIF
        IF (Is_There_A_MissionsAtCoords_Focus_Mission())
            NET_PRINT(" [PLAYER HAS A FOCUS MISSION]")
        ELSE
            NET_PRINT(" [player has no focus mission]")
        ENDIF
        IF (g_sJLInvitesMP[storageSlot].jliIsDuplicate)
            NET_PRINT(" [DUPLICATE (hold in reserve)]")
        ELSE
            NET_PRINT(" [not a duplicate]")
        ENDIF
// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
//        IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
//            NET_PRINT(" [HAS WANTED LEVEL]")
//        ELSE
//            NET_PRINT(" [no wanted level]")
//        ENDIF
        IF (Can_Invite_Be_Displayed_At_This_Hour(paramOnHours))
            NET_PRINT(" [open at this hour: ")
        ELSE
            NET_PRINT(" [CLOSED AT THIS HOUR: ")
        ENDIF
        NET_PRINT_INT(GET_CLOCK_HOURS())
        NET_PRINT("]")
        NET_NL()
        NET_PRINT("      Setting Allow Again Timeout Time: ")
        NET_PRINT(GET_TIME_AS_STRING(g_sJLInvitesMP[storageSlot].jliAllowAgainTimeout))
        NET_NL()
		IF (g_sJLInvitesMP[storageSlot].jliDoGamerToGamerCheck)
        	NET_PRINT("      Still waiting for refined Gamer-To-Gamer Content Restriction Check result to be available")
		ENDIF
		NET_NL()
        
        // TEMP array output
        NET_NL()
        NET_PRINT("...Invitation Array. Num Entries: ") NET_PRINT_INT(g_numMPJobListInvitations) NET_NL()
        INT tempLoop = 0
        REPEAT g_numMPJobListInvitations tempLoop
            NET_PRINT("   ")
            NET_PRINT_INT(tempLoop)
            NET_PRINT("   ")
            IF (g_sJLInvitesMP[tempLoop].jliInvitorIsPlayer)
                IF (IS_NET_PLAYER_OK(g_sJLInvitesMP[tempLoop].jliPlayer, FALSE))
                    NET_PRINT(GET_PLAYER_NAME(g_sJLInvitesMP[tempLoop].jliPlayer))
                ELSE
                    NET_PRINT("UNKNOWN PLAYER")
                ENDIF
            ELSE
                TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLInvitesMP[tempLoop].jliNPC)
                NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            ENDIF
			NET_PRINT(" [")
			NET_PRINT_INT(g_sJLInvitesMP[tempLoop].jliOrderIndex)
			NET_PRINT("]")
            NET_PRINT("   ")
            NET_PRINT_VECTOR(g_sJLInvitesMP[tempLoop].jliCoords)
			IF (g_sJLInvitesMP[tempLoop].jliReAdding)
				NET_PRINT(" (Re-Adding)")
			ENDIF
            IF (g_sJLInvitesMP[tempLoop].jliToDownload)
                NET_PRINT(" [TO DOWNLOAD]")
            ENDIF
            IF (g_sJLInvitesMP[tempLoop].jliDownloading)
                NET_PRINT(" [DOWNLOADING]")
            ENDIF
            IF (g_sJLInvitesMP[tempLoop].jliDownloaded)
                NET_PRINT(" [DOWNLOADED]")
            ENDIF
            NET_NL()
        ENDREPEAT
    #ENDIF
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store the details of an invite to a mission from another player.
//
// INPUT PARAMS:            paramType           	Type of Mission
//                          paramCoords         	Location of Mission
//                          paramCreatorID      	CreatorID of mission
//                          paramPlayerID       	Player Index of player that sent the invite
//                          paramMissionName    	The Mission Name (as a STRING)
//                          paramDescription    	The Mission Description (as a STRING)
//                          paramContentID      	The ContentID (cloud filename)
//							paramHeistPackedInt		A Packed INT used to setup the correct apartment when accepting a same-session invite into a Heist Corona
PROC Store_Invite_Details_For_Joblist_From_Player(INT paramType, VECTOR paramCoords, INT paramCreatorID, PLAYER_INDEX paramPlayerID, STRING paramMissionName, STRING paramDescription, TEXT_LABEL_23 paramContentID, INT paramHeistPackedInt)
		
    // Dump this invite if a privileges restriction exists
	GAMER_HANDLE paramInvitorGH = GET_GAMER_HANDLE_PLAYER(paramPlayerID)
    IF (Check_If_Underage_Privilege_Restrictions_Exist(paramCreatorID, paramInvitorGH))
		// Let the invitor know
		BROADCAST_JOBLIST_INVITE_REPLY(paramPlayerID, TRUE, FALSE)
		
		// Display a help message if appropriate
		Display_Restriction_Help_Message()
		
        EXIT
    ENDIF
    
	BOOL doGamerToGamerCheck = FALSE
	BOOL restrictedContent = FALSE
    IF (Check_If_Content_Privilege_Restrictions_Exist(paramCreatorID, paramInvitorGH))
		restrictedContent = TRUE
		
		// KGM 29/9/14 [BUG 2052076]: On XboxOne the restriction can be too severe, so need to do a gamer-to-gamer additional check
		//				This check is carried out in code when a player joins a session but there may be a few frames delay until it's ready
		// NOTE: These additional checks should only be needed for same-session invites. Cross-session ones will be handled by code.
		IF (Wait_For_Gamer_To_Gamer_Content_Restriction_Result(paramPlayerID))
			// ...still need to wait for the Gamer To Gamer Check Result to be available, so we don;t know if the content is restricted yet
			doGamerToGamerCheck	= TRUE
			restrictedContent	= FALSE
		ELSE
			// ...the result is immediately available, so check for restriction
			IF NOT (Check_If_Gamer_To_Gamer_Content_Restriction_Exists(paramPlayerID))
				// ...a restriction between these gamers doesn't exist, so allow the content
				restrictedContent = FALSE
			ENDIF
		ENDIF
	
		// Does a known restriction exist?
		IF (restrictedContent)
			// Yes, Let the invitor know
			BROADCAST_JOBLIST_INVITE_REPLY(paramPlayerID, TRUE, FALSE)
			
			// Display a help message if appropriate
			Display_Restriction_Help_Message()
			
	        EXIT
		ENDIF
    ENDIF
    
	IF NOT (XBOX_ONLY_Check_If_Player_Has_LIVE_Privileges())
		// Let the invitor know that the invite is blocked because there are no LIVE privileges
		BROADCAST_JOBLIST_INVITE_REPLY(paramPlayerID, TRUE, FALSE)
		
		// Display a help message
		BOOL displayXboxLiveHelp = TRUE
		Display_Restriction_Help_Message(displayXboxLiveHelp)
  
        EXIT
	ENDIF
			
	// NOTE: For DURANGO version, this check must take place after the Content Privilege check so that the refined GamerToGamer check has been considered
	BOOL displayXboxLiveHelpIfFriendsRestriction = FALSE
    IF (Check_If_Invite_Blocked_By_Friends_Only_Restriction(paramCreatorID, displayXboxLiveHelpIfFriendsRestriction))
		// Let the invitor know
		BROADCAST_JOBLIST_INVITE_REPLY(paramPlayerID, TRUE, FALSE)
		
		// Display a help message if appropriate
		Display_Restriction_Help_Message(displayXboxLiveHelpIfFriendsRestriction)
		
        EXIT
    ENDIF
	
	// KGM 25/8/14 [BUG 2001821] - Communications Check no longer needed on any console (NETWORK_CAN_COMMUNICATE_WITH_GAMER)
    
    // Ditch invites if the player hasn't completed the initial ambient tutorials
    IF NOT (HAS_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(PLAYER_ID()))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Player hasn't completed initial ambient tutorials - Ditching Invite from Player: ")
            NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
            NET_NL()
        #ENDIF
		
		// Let the invitor know
		BROADCAST_JOBLIST_INVITE_REPLY(paramPlayerID, TRUE, FALSE)
        
        EXIT
    ENDIF
    
    IF NOT (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT(" - Don't allow invites until Cloud Data loaded") NET_NL()
        #ENDIF
		
		// Let the invitor know
		BROADCAST_JOBLIST_INVITE_REPLY(paramPlayerID, TRUE, FALSE)
		
        EXIT
    ENDIF

    BOOL isInvitorOk = IS_NET_PLAYER_OK(paramPlayerID, FALSE)

    IF NOT (Check_Consistency_Of_Invite_Data_Strings(paramMissionName, paramDescription))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("- FAILED (Data Inconsistency): attempting Store_Invite_Details_For_Joblist_From_Player(): ")
            IF (isInvitorOk)
                NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
            ENDIF
            NET_NL()
        #ENDIF
		
		// Let the invitor know
		BROADCAST_JOBLIST_INVITE_REPLY(paramPlayerID, TRUE, FALSE)
		
        EXIT
    ENDIF
    // TODO: Tidy the debug output in this area
	PRINTLN("[MMM][JobList] Checking if this invite is SPAM ...	")
	PRINTLN("[MMM][JobList] Invitor Name :	", GET_PLAYER_NAME(paramPlayerID))
	PRINTLN("[MMM][JobList] Content ID Hash : ", GET_HASH_KEY(paramContentID))
	PRINTLN("[MMM][JobList] Current Time : ", GET_GAME_TIMER())
	PRINTLN("[MMM][JobList] -------------------------------------------------- ")
	
	INT i = 0
	FOR i = 0 TO MAX_MP_JOBLIST_ENTRIES - 1
		
				
		IF g_spamJobList[i].jlInvitorIndex = paramPlayerID 
		AND g_spamJobList[i].jliContentIDHash = GET_HASH_KEY(paramContentID)
		AND GET_GAME_TIMER() < g_spamJobList[i].Timer 					
        	PRINTLN("[MMM][JobList] This invite has been marked as spam, need to wait for 30 seconds until it will reach the player again. ...	")
			
			g_spamJobList[i].Timer = GET_GAME_TIMER() + 30000
			
			i = MAX_MP_JOBLIST_ENTRIES
			PRINTLN("[MMM][JobList] -------------------------------------------------- ")
			
			// Let the invitor know
			BROADCAST_JOBLIST_INVITE_REPLY(paramPlayerID, TRUE, FALSE)
			EXIT
		ENDIF
	ENDFOR
	PRINTLN("[MMM][JobList] -------------------------------------------------- ")
	
	IF Is_Player_Excluded_From_MP_Mission( PLAYER_ID(), Get_UniqueID_For_This_Players_Mission(paramPlayerID))
		PRINTLN("[MMM][JobList] The player was previously kicked from this session so the invite has been blocked.")
		
		// Let the invitor know
		BROADCAST_JOBLIST_INVITE_REPLY(paramPlayerID, TRUE, FALSE)
		EXIT
	ENDIF
	
	GAMER_HANDLE thisPlayerGH = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
	
	IF NOT NETWORK_HAS_SOCIAL_CLUB_ACCOUNT() OR NOT NETWORK_CLAN_PLAYER_IS_ACTIVE(thisPlayerGH)
		// TODO: Check if the invite is a crew challenge
		//		 This needs testing before submission.
		IF IS_PLAYER_ON_A_CHALLENGE(paramPlayerID)
      	OR IS_PLAYER_SETTING_CHALLENGE(paramPlayerID)
	    OR IS_PLAYER_ON_A_H2H_PLAYLIST(paramPlayerID)
			PRINTLN("[MMM][JobList] Local player is not signed in to social club/isn't part of a crew so this invite has been blocked")
			BROADCAST_JOBLIST_INVITE_REPLY(paramPlayerID, TRUE, FALSE)
			EXIT
		ENDIF
	ENDIF
	
    // Get the local language version of the mission name if it exists
    MP_MISSION_ID_DATA theMissionIdData
    Clear_MP_MISSION_ID_DATA_Struct(theMissionIdData)
    
    TEXT_LABEL_63	theMissionName	= ""
	INT				theSubtype		= -1
	BOOL			toDownload		= FALSE
	INT				minPlayers		= 0
	INT				maxPlayers		= 0
	BOOL			isUnplayed		= FALSE
	BOOL			isPushBikeRace	= FALSE
	INT 			jliRootContentIdHash
	INT 			jliAdversaryModeType

	// NOTE: Using isUnplayed rather than isPlayed because we want to treat missions where the data isn't available locally as 'played' rather than tag them with 'NEW'
    
    IF (Search_For_And_Retrieve_MissionIdData_From_Cloud_Filename(paramContentID, theMissionIdData, FALSE))
        // ...data exists locally, so grab the local mission name
        theMissionName	= Get_Mission_Name_For_FM_Cloud_Loaded_Activity(theMissionIdData)
		theSubtype		= Get_SubType_For_FM_Cloud_Loaded_Activity(theMissionIdData)
		minPlayers		= Get_Minimum_Players_Required_For_FM_Cloud_Loaded_Activity(theMissionIdData)
		maxPlayers		= Get_Maximum_Players_Required_For_FM_Cloud_Loaded_Activity(theMissionIdData)
		isPushBikeRace	= Is_FM_Cloud_Loaded_Activity_A_Bicycle_Race(theMissionIDdata)
		toDownload		= FALSE
		jliRootContentIdHash = Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(theMissionIDdata)
		jliAdversaryModeType = Get_AdversaryModeType_For_FM_Cloud_Loaded_Activity(theMissionIDdata)
		
		BOOL isPlayed = Get_Played_Status_FM_Cloud_Loaded_Activity(theMissionIdData)
		IF NOT (isPlayed)
			isUnplayed = TRUE
		ENDIF
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Mission Name Replacement: ")
            NET_PRINT(paramMissionName)
            NET_PRINT("  Local Language: ")
            NET_PRINT(theMissionName)
            NET_NL()
        #ENDIF
    ELSE
        // ...data doesn't exist locally, so use the passed in mission name
        theMissionName	= paramMissionName
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Using Passed-In Parameter for the mission name: ")
            NET_PRINT(theMissionName)
            NET_NL()
        #ENDIF
		
		// If this is a Minigame then a download is not required and the min and max players are known
		IF (paramCreatorID = FMMC_MINI_GAME_CREATOR_ID)
			// ...this is a Minigame, so store the min and max players and a download of the mission header is not required
			MP_MISSION missionID = Convert_FM_Mission_Type_To_MissionID(paramType)
			
			minPlayers	= Get_Minimum_Number_Of_Players_Required_For_MP_Mission(missionID)
			maxPlayers	= GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionID)
			toDownload	= FALSE
		ELSE
			// ...this is not a minigame, so a download of the mission data is required
			toDownload = TRUE
		ENDIF
    ENDIF
	
	// KGM 2/2/15 [BUG 2214425]: Clean up any existing cross-session invites from a player if a same-session invite arrives.
	IF (isInvitorOk)
		TEXT_LABEL_63 playerNameAsTL63 = GET_PLAYER_NAME(paramPlayerID)
		INT csSlot = Get_Joblist_CSInvite_Slot_For_InvitorGamerTag(playerNameAsTL63)
		IF (csSlot != NO_JOBLIST_ARRAY_ENTRY)
	        // ...an existing entry exists for this player, so delete it
	        #IF IS_DEBUG_BUILD
	            NET_PRINT(".KGM [JobList]: ")
	            NET_PRINT_TIME()
	            NET_PRINT("Player ")
				NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
				NET_PRINT(". New Invitation (same-session) BUT CSInvite from Same Player Already Exists. Slot: ")
	            NET_PRINT_INT(csSlot)
				NET_PRINT(" - DELETING CSInvite")
	            NET_NL()
	        #ENDIF
			
        	Delete_Joblist_CSInvite_In_Invitation_Slot(csSlot)
		ENDIF
	ENDIF
    
    // Only allow one invitation per player, so supersede any existing entry (also move it back to the top of the array)
    INT existingSlot = Get_Joblist_Invitation_Slot_For_Player(paramPlayerID)
    IF (existingSlot != NO_JOBLIST_ARRAY_ENTRY)
        // ...an existing entry exists for this player
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Player Invitation Already Exists: ")
            NET_PRINT_INT(existingSlot)
            NET_NL()
        #ENDIF
        
        // Is this a duplicate invite from the same player?
        IF (ARE_VECTORS_ALMOST_EQUAL(g_sJLInvitesMP[existingSlot].jliCoords, paramCoords, 3.0))
			// KGM 2/2/15 [BUG 2171733]: Foreign Language Heist Prep Invites are not being 'duplicated' because the details need downloaded and the passed-in mission name is in the
			//				foreign language but the stored mission name gets replaced with the local language version after download. If a download is required, compare contentIDs instead.
			BOOL duplicateDetails = FALSE
			IF (toDownload)
 				// KGM 2/2/15: New comparison, used if the header details need downloaded
           		IF (ARE_STRINGS_EQUAL(g_sJLInvitesMP[existingSlot].jliContentID, paramContentID))
					duplicateDetails = TRUE
				ENDIF
			ELSE
				// KGM 2/2/15: Original comparison, but doesn't work for a foreign language invite to a mission that requires downloaded (ie: Heist Prep Mission) because the mission name changes
            	IF (ARE_STRINGS_EQUAL(g_sJLInvitesMP[existingSlot].jliMissionName, theMissionName))
					duplicateDetails = TRUE
				ENDIF
			ENDIF
			IF (duplicateDetails)
                // ...duplicate details, so check for allow again timeout
                IF (IS_TIME_LESS_THAN(GET_NETWORK_TIME(), g_sJLInvitesMP[existingSlot].jliAllowAgainTimeout))
                    // ...timer hasn't expired, so this is a duplicate.
                    // Update the 'allow again' timeout based the newest invite, but nothing else to do with this one
                    g_sJLInvitesMP[existingSlot].jliAllowAgainTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), ALLOW_DUPLICATE_INVITE_DELAY_msec)
                    
                    #IF IS_DEBUG_BUILD
						IF (toDownload)
                        	NET_PRINT("      BUT: This same invite was recently received (based on ContentID). Treating as an autofill spam and ignoring.") NET_NL()
						ELSE
                        	NET_PRINT("      BUT: This same invite was recently received (based on MissionName). Treating as an autofill spam and ignoring.") NET_NL()
						ENDIF
                        NET_PRINT("         [Coords                     : ") NET_PRINT_VECTOR(paramCoords) NET_PRINT("]") NET_NL()
                        NET_PRINT("         [Mission Name               : ") NET_PRINT(theMissionName) NET_PRINT("]") NET_NL()
                        NET_PRINT("         [ContentID                  : ") NET_PRINT(paramContentID) NET_PRINT("]") NET_NL()
                        NET_PRINT("         [Updated Allow Again Timeout: ") NET_PRINT(GET_TIME_AS_STRING(g_sJLInvitesMP[existingSlot].jliAllowAgainTimeout)) NET_PRINT("]") NET_NL()
                    #ENDIF
                    
                    EXIT
                ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					NET_PRINT("      BUT: Not recently received, so replace the details.") NET_NL()
				#ENDIF
            ENDIF
        ENDIF
        
		// KGM 24/11/14: We're deleting an old invite to replace it with an identical invite - don't mark as SPAM when deleted
		BOOL allowMarkAsSpam = FALSE
        Delete_Joblist_Invitation_In_Invitation_Slot(existingSlot, allowMarkAsSpam)
    ENDIF
    
    IF NOT (isInvitorOk)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Store_Invite_Details_For_Joblist_From_Player() - IGNORED: Inviting player is not OK")
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // Store the details
    INT     invitingPlayerAsInt = NATIVE_TO_INT(paramPlayerID)
    BOOL    invitorIsPlayer     = TRUE
    INT     theOnHours          = MATC_ALL_DAY
	BOOL	isReAdding			= FALSE
    
    private_Store_Invite_Details_On_Joblist(paramType, theSubtype, paramCoords, paramCreatorID, invitingPlayerAsInt, invitorIsPlayer, theMissionName, paramDescription, paramContentID, jliRootContentIdHash, theOnHours, isReAdding, toDownload, minPlayers, maxPlayers, isUnplayed, paramHeistPackedInt, isPushBikeRace, doGamerToGamerCheck, jliAdversaryModeType)
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store the details of an invite to a mission from an in-game character.
//
// INPUT PARAMS:            paramType           Type of Mission
//							paramSubtype		Mission Subtype
//                          paramCoords         Location of Mission
//                          paramCreatorID      CreatorID of mission
//                          paramNPC            enumCharacterList of in-game character that sent the invite
//                          paramMissionName    The Mission Name (as a STRING)
//                          paramDescription    Usually "" unless a specific piece of description text is required (KGM 4/11/14 [BUG 2111097]: Allows first Heist Versus Unlock to have specific decription
//                          paramContentID      The ContentID (cloud filename)
//                          paramReAdding       [DEFAULT = FALSE] TRUE if this NPC invite is being re-added after a transition, otherwise FALSE
//                          paramOnHours        [DEFAULT = MATC_ALL_DAY] The hours as a bitfield represnting invite availability (first 24 bits represent 24 hours)
PROC Store_Invite_Details_For_Joblist_From_NPC(INT paramType, INT paramSubtype, VECTOR paramCoords, INT paramCreatorID, enumCharacterList paramCharacterID, STRING paramMissionName, STRING paramDescription, TEXT_LABEL_23 paramContentID, BOOL paramReAdding = FALSE, INT paramOnHours = MATC_ALL_DAY, BOOL jliQuickMatch = FALSE)

    // Disable these invites using a command-line parameter - used when creating a trailer
    #IF IS_DEBUG_BUILD
        IF (GET_COMMANDLINE_PARAM_EXISTS("sc_noInvitesFromNPC"))
            NET_PRINT("...KGM MP [JobList]: NPC INVITES DISABLED BY COMMAND LINE PARAMETER: -sc_noInvitesFromNPC") NET_NL()
            
            EXIT
        ENDIF
    #ENDIF

    IF (paramCharacterID = NO_CHARACTER)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT(" Store_Invite_Details_For_Joblist_From_NPC() - ERROR: Character is NO_CHARACTER. Ignoring request.")
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // KGM 7/8/13: If re-adding a Contact Mission then that specific mission data is ready, so ignore this check which was blocking the invite
    IF NOT (paramReAdding)
        IF NOT (Is_Initial_Cloud_Loaded_Mission_Data_Ready())
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT(" - Don't allow invites until Cloud Data loaded") NET_NL()
            #ENDIF
            
            EXIT
        ENDIF
    ENDIF

    IF NOT (Check_Consistency_Of_Invite_Data_Strings(paramMissionName, paramDescription))
        #IF IS_DEBUG_BUILD
            TEXT_LABEL  npcNameTL = Get_NPC_Name(paramCharacterID)
            
            NET_PRINT("         - FAILED: attempting Store_Invite_Details_For_Joblist_From_NPC(): ")
            NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // Only allow one invitation per NPC, so supersede any existing entry (also move it back to the top of the array)
    // KGM 6/8/13: Changed to allow multiple invites from same NPC for different missions
    BOOL matchMission = TRUE
    INT existingSlot = Get_Joblist_Invitation_Slot_For_NPC(paramCharacterID, paramMissionName, matchMission)
    IF (existingSlot != NO_JOBLIST_ARRAY_ENTRY)
        // ...an existing entry exists for this NPC, so delete it
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Player Invitation Already Exists: ")
            NET_PRINT_INT(existingSlot)
            NET_NL()
        #ENDIF
        
		// KGM 24/11/14: We're deleting an old invite to replace it with an identical invite - don't mark as SPAM when deleted
		BOOL allowMarkAsSpam = FALSE
        Delete_Joblist_Invitation_In_Invitation_Slot(existingSlot, allowMarkAsSpam)
    ENDIF
	
	// Has a specific description been passed
	#IF IS_DEBUG_BUILD
		IF NOT (IS_STRING_NULL_OR_EMPTY(paramDescription))
			// ...yes
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("NPC Invite requires specific description: ")
			NET_PRINT(paramDescription)
            NET_NL()
		ENDIF
	#ENDIF
	
     // Get the local language version of the mission name if it exists
    MP_MISSION_ID_DATA theMissionIdData
    Clear_MP_MISSION_ID_DATA_Struct(theMissionIdData)
	INT 			jliRootContentIdHash
	INT 			jliAdversaryModeType
	
	// NOTE: Using isUnplayed rather than isPlayed because we want to treat missions where the data isn't available locally as 'played' rather than tag them with 'NEW'
    
    IF (Search_For_And_Retrieve_MissionIdData_From_Cloud_Filename(paramContentID, theMissionIdData, FALSE))
		jliRootContentIdHash = Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(theMissionIDdata)
		jliAdversaryModeType = Get_AdversaryModeType_For_FM_Cloud_Loaded_Activity(theMissionIDdata)
	ENDIF	
		
    // Store the details
	// NOTE: For an NPC Invite, min and max players and the 'played' status will be calculated when needed 
	//		(this is to prevent doing a number of big seraches on the same frame here if multiple NPC invites are received in the same frame)
    INT     invitingNPCAsInt    		= ENUM_TO_INT(paramCharacterID)
    BOOL    invitorIsPlayer     		= FALSE
	BOOL	toDownload					= FALSE
	INT		ignoreMinPlayers			= 0
	INT		ignoreMaxPlayers			= 0
	BOOL	ignoreUnplayed				= FALSE
	INT		ignoreHeistPackedInt		= 0
	BOOL	ignoreIsPushBikeRace		= FALSE
	BOOL	ignoreDoGamerToGamerCheck	= FALSE
    
    private_Store_Invite_Details_On_Joblist(paramType, paramSubtype, paramCoords, paramCreatorID, invitingNPCAsInt, invitorIsPlayer, paramMissionName, paramDescription, paramContentID, jliRootContentIdHash, paramOnHours, paramReAdding, toDownload, ignoreMinPlayers, ignoreMaxPlayers, ignoreUnplayed, ignoreHeistPackedInt, ignoreIsPushBikeRace, ignoreDoGamerToGamerCheck, jliAdversaryModeType, jliQuickMatch)
    
ENDPROC


//Added as a function to fix the intelesence errors
FUNC BOOL HAS_FIRST_SP_MISSION_BEEN_PASSED()
	#if USE_CLF_DLC		
		IF (g_savedGlobalsClifford.sFlow.missionSavedData[SP_MISSION_CLF_TRAIN].completed) // check the first agent T mission
			RETURN TRUE
		ENDIF
	#endif
	#if USE_NRM_DLC	
		IF (g_savedGlobalsnorman.sFlow.missionSavedData[SP_MISSION_NRM_SUR_START].completed) // check the first NRM mission
			RETURN TRUE
			
		ENDIF
	#endif
	
	#if not USE_CLF_DLC
	#IF NOT USE_NRM_DLC
		IF (g_savedGlobals.sFlow.missionSavedData[SP_MISSION_ARMENIAN_1].completed)
			RETURN TRUE
		ENDIF
	#endif
	#endif
	
	RETURN FALSE
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this cross-session invite needs a reminder feed message to be sent shortly afterwards
//
// INPUT PARAMS:			paramInvitorGH              The Invitor's Gamer Handle
//							paramInviteSlot				The slot the CSInvite details have been store in
// RETURN VALUE:			BOOL						TRUE if a reminder is required, otherwise FALSE
//
// NOTES:	A reminder should be sent for an invite to a single job from a Friend
FUNC BOOL Check_If_CSInvite_Reminder_Will_Be_Required(GAMER_HANDLE paramInvitorGH, INT paramInviteSlot)

	IF NOT (NETWORK_IS_FRIEND(paramInvitorGH))
		// ...not a friend
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("CS Invite Reminder not required: Invitor is not a friend.")
            NET_NL()
		#ENDIF
				
		RETURN FALSE
	ENDIF
	
	IF (g_sJLCSInvitesMP[paramInviteSlot].jcsiPlaylistTotal > 0)
		// ...ignore for playlists (although we can probably allow this - just worried the data goes out of date)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("CS Invite Reminder not required: Invite is to a playlist.")
            NET_NL()
		#ENDIF
				
		RETURN FALSE
	ENDIF
	
	// Allow a Reminder to be sent
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("CS Invite Reminder ALLOWED.")
        NET_NL()
	#ENDIF
	
	RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store the details of a cross-session invite to a mission from another player.
//
// INPUT PARAMS:            paramCloudFilename          Cloud Filename for mission
//                          paramInvitorName            Invitor Name
//                          paramInviteID               The InviteID
//                          paramInvitorGH              The Invitor's Gamer Handle
//                          paramTotalInPlaylist        [DEFAULT = 0] The total number of jobs in the playlist (or 0 if not a playlist)
//                          paramCurrentPlaylistJob     [DEFAULT = 0] The current job in the playlist
FUNC BOOL Store_Cross_Session_Invite_Details_For_Joblist(TEXT_LABEL_23 paramCloudFilename, TEXT_LABEL_63 paramInvitorName, INT paramInviteID, GAMER_HANDLE paramInvitorGH, INT paramTotalInPlaylist = 0, INT paramCurrentPlaylistJob = 0)

	// KGM 26/2/15 [BUG 2250287]: Current Playlist value may be getting used to indicate 'show in sp' for some non-friend invites
	BOOL showEvenIfNonFriendInSP = FALSE
	IF (paramCurrentPlaylistJob >= CROSS_SESSION_SHOW_IN_SP_OFFSET)
		paramCurrentPlaylistJob -= CROSS_SESSION_SHOW_IN_SP_OFFSET
		showEvenIfNonFriendInSP = TRUE
		PRINTLN(".KGM [Joblist]: CSInvite has 'show to non-friends in SP' flag set. CurrentPlaylistJob value now: ", paramCurrentPlaylistJob)
	ENDIF
		
	IF (NETWORK_IS_GAME_IN_PROGRESS())
	    // Ignore cross-session invites from players for solo sessions
	    IF (Joblist_Is_This_A_Solo_Session())
	        #IF IS_DEBUG_BUILD
	            NET_PRINT("...KGM MP [JobList]: ")
	            NET_PRINT_TIME()
	            NET_PRINT("SOLO SESSION - Ditching Cross-Session Invite from player: ")
	            NET_PRINT(paramInvitorName)
	            NET_NL()
	        #ENDIF
	        
	        // Tell Code to delete the Invite
	        Delete_Cross_Session_Invite_With_This_InviteID(paramInviteID)
	        RETURN FALSE
	    ENDIF
	    
	    // Ditch Cross-Session Invites if the player is playing the tutorial
	    IF (IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL())
	        #IF IS_DEBUG_BUILD
	            NET_PRINT("...KGM MP [JobList]: ")
	            NET_PRINT_TIME()
	            NET_PRINT("Player is playing the tutorial - Ditching Cross Session Invite with InviteID: ")
	            NET_PRINT_INT(paramInviteID)
	            NET_NL()
	        #ENDIF
	        
	        // Tell Code to delete the Invite
	        Delete_Cross_Session_Invite_With_This_InviteID(paramInviteID)
	        RETURN FALSE
	    ENDIF
	ENDIF
	    
    // Ditch Cross-Session Invites to players in SP unless they are from Friends
    IF NOT (NETWORK_IS_GAME_IN_PROGRESS())
		IF (showEvenIfNonFriendInSP)
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("Playing SP and Inviter is not a friend, BUT showEvenIfNonFriendInSP = TRUE so allow Cross Session Invite through. InviteID: ")
                NET_PRINT_INT(paramInviteID)
                NET_NL()
            #ENDIF
		ELSE
	        IF NOT (NETWORK_IS_FRIEND(paramInvitorGH))
	            #IF IS_DEBUG_BUILD
	                NET_PRINT("...KGM MP [JobList]: ")
	                NET_PRINT_TIME()
	                NET_PRINT("Playing SP and Inviter is not a friend - Ditching Cross Session Invite with InviteID: ")
	                NET_PRINT_INT(paramInviteID)
	                NET_NL()
	            #ENDIF
	        
	            // Tell Code to delete the Invite
	            Delete_Cross_Session_Invite_With_This_InviteID(paramInviteID)
	           	RETURN FALSE
	        ENDIF
		ENDIF
		
		// Don't accept the invite in SP if the Prologue phone is in use
        IF (g_Use_Prologue_Cellphone)
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("Playing SP and using Prologue Phone - Ditching Cross Session Invite with InviteID: ")
                NET_PRINT_INT(paramInviteID)
                NET_NL()
            #ENDIF
        
            // Tell Code to delete the Invite
            Delete_Cross_Session_Invite_With_This_InviteID(paramInviteID)
	        RETURN FALSE
        ENDIF
		
		// Don't accept the invite in SP if Armenian1 mission hasn't been complete
		// HOWEVER: this will be false if on a mission replay, so don't do this check if on a replay
		// KGM 7/10/13: This check is blocking MP invites being accepted in non-flow SP, so only do this if not in debug mode
		BOOL keepOnlyIfArmenian1Complete = TRUE
		#IF IS_DEBUG_BUILD
			keepOnlyIfArmenian1Complete = FALSE
		#ENDIF
		
		IF NOT HAS_FIRST_SP_MISSION_BEEN_PASSED()		
			IF NOT (IS_REPEAT_PLAY_ACTIVE())
	            #IF IS_DEBUG_BUILD
	                NET_PRINT("...KGM MP [JobList]: ")
	                NET_PRINT_TIME()
	                NET_PRINT("Playing SP and Armenian1 is not complete (and not on a replay)")
					IF (keepOnlyIfArmenian1Complete)
						// ...armenian1 is not complete, so delete invite because it is only kept if Armenian1 was complete
						NET_PRINT(" - Ditching Cross Session Invite with InviteID: ")
	                	NET_PRINT_INT(paramInviteID)
					ELSE
						// ...armenian1 is not complete, but we are keeping the invite anyway
						NET_PRINT(" - BUT USING DEBUG SCRIPTS, so allowing invite to be displayed")
					ENDIF
	                NET_NL()
	            #ENDIF
	        
	            // Tell Code to delete the Invite because Armenian1 is not complete
				IF (keepOnlyIfArmenian1Complete)
	            	Delete_Cross_Session_Invite_With_This_InviteID(paramInviteID)
		           
		            RETURN FALSE
				ENDIF
	        ENDIF
		ENDIF
   ENDIF
    
    // Ditch cross-session invutes if the player hasn't completed the initial ambient tutorials
    IF (NETWORK_IS_GAME_IN_PROGRESS())  //If the player is in SP disregard this check - added by Steve T to prevent the MP to SP invite bombing out.
        IF NOT (HAS_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(PLAYER_ID()))
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("Player hasn't completed initial ambient tutorials - Ditching Cross Session Invite with InviteID: ")
                NET_PRINT_INT(paramInviteID)
                NET_NL()
            #ENDIF
        
            // Tell Code to delete the Invite
            Delete_Cross_Session_Invite_With_This_InviteID(paramInviteID)
           
            RETURN FALSE
        ENDIF
    ENDIF
    
    // Check Player Name
    IF (IS_STRING_NULL_OR_EMPTY(paramInvitorName))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("InvitorName is NULL or EMPTY - Ditching Cross Session Invite with InviteID: ")
            NET_PRINT_INT(paramInviteID)
            NET_NL()
            SCRIPT_ASSERT("Store_Cross_Session_Invite_Details_For_Joblist() - ERROR: InvitorName is NULL or EMPTY. Ditching Cross-Session Invite. Tell Keith.")
        #ENDIF
        
        // Tell Code to delete the Invite
        Delete_Cross_Session_Invite_With_This_InviteID(paramInviteID)   
        RETURN FALSE
    ENDIF
     
    // Check ContentID
    IF (IS_STRING_NULL_OR_EMPTY(paramCloudFilename))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("ContentID is NULL or EMPTY - Ditching Cross Session Invite with InviteID: ")
            NET_PRINT_INT(paramInviteID)
            NET_NL()
            SCRIPT_ASSERT("Store_Cross_Session_Invite_Details_For_Joblist() - ERROR: ContentID is NULL or EMPTY. Ditching Cross-Session Invite. Tell Keith.")
        #ENDIF
        
        // Tell Code to delete the Invite
        Delete_Cross_Session_Invite_With_This_InviteID(paramInviteID)           
        RETURN FALSE
    ENDIF
   
    // Only allow one invitation per player, so supersede any existing entry (also move it back to the top of the array)
	TEXT_LABEL_63 invitorAsTL63 = paramInvitorName
    INT existingSlot = Get_Joblist_CSInvite_Slot_For_InvitorGamerTag(invitorAsTL63)
    IF (existingSlot != NO_JOBLIST_ARRAY_ENTRY)
        // ...an existing entry exists for this player, so delete it
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Player Cross-Session Invite Already Exists: ")
            NET_PRINT_INT(existingSlot)
            NET_NL()
        #ENDIF
        
        Delete_Joblist_CSInvite_In_Invitation_Slot(existingSlot)
    ENDIF
    
	// TODO:
	INT inviteIndex = NETWORK_GET_PRESENCE_INVITE_INDEX_BY_ID(paramInviteID)
	PRINTLN("[MMM][JobList] Checking is Invite Spam")
	PRINTLN("[MMM][JobList] Invite Index : ", inviteIndex)
	INT value = NETWORK_GET_PRESENCE_INVITE_SESSION_ID(inviteIndex)
	PRINTLN("[MMM][JobList] NETWORK_GET_PRESENCE_INVITE_SESSION_ID : ", value)
	INT i = 0
	FOR i = 0 TO MAX_MP_JOBLIST_ENTRIES - 1
		PRINTLN("[MMM][JobList] -------------------------------------------------- ")		
		PRINTLN("[MMM][JobList] Stored Value : ", g_spamJobList[i].jlcsInviteSessionID)
		IF  value = g_spamJobList[i].jlcsInviteSessionID
		AND GET_GAME_TIMER() < g_spamJobList[i].Timer
			PRINTLN("[MMM][JobList] This invite has been marked as spam, need to wait for 30 seconds until it will reach the player again. ...	")
			
			g_spamJobList[i].Timer = GET_GAME_TIMER() + 30000
			
			i = MAX_MP_JOBLIST_ENTRIES
			PRINTLN("[MMM][JobList] -------------------------------------------------- ")
			
			// Let the invitor know	
			Delete_Cross_Session_Invite_With_This_InviteID(paramInviteID)
			//BROADCAST_JOBLIST_INVITE_REPLY(paramPlayerID, TRUE, FALSE)	// Only works on samesession
			
			RETURN FALSE
		ENDIF 
	
	ENDFOR
	PRINTLN("[MMM][JobList] -------------------------------------------------- ")
	
    // Check if the Cloud-Loaded Header Data is locally available
    MP_MISSION_ID_DATA theMissionIdData
    Clear_MP_MISSION_ID_DATA_Struct(theMissionIdData)
    
    INT             fmmcMissionType = FMMC_TYPE_MISSION
	INT				missionSubtype	= 0
    INT             theCreatorID    = FMMC_ROCKSTAR_CREATOR_ID
    INT             theVariation    = 0
    TEXT_LABEL_63   theMissionName  = ""
    TEXT_LABEL_63   theDescription  = ""
    INT             DescHash        = 0
    BOOL            requireDesc     = FALSE
    BOOL            toBeDownloaded  = FALSE
    BOOL            isDownloaded    = TRUE
	BOOL			isUnplayed		= FALSE
	BOOL 			isPushBikeOnly	= FALSE
	INT 			RootContentIdHash
	INT				AdversaryModeType
	// NOTE: IsUnplayed is used because we only want to tag the mission if 'unplayed' - if the data isn't locally available then we shouldn't tag it
    
    // Gather the details
    IF (Get_MiniGame_Type_And_Variation_From_TL(paramCloudFilename, fmmcMissionType, theVariation))
        // ...this is a minigame
        theCreatorID        = FMMC_MINI_GAME_CREATOR_ID
        requireDesc         = FALSE
        
        // For Now, do nothing with the variation
        theVariation = theVariation
    ELSE
        // Check if the mission details are locally available
        IF (Search_For_And_Retrieve_MissionIdData_From_Cloud_Filename(paramCloudFilename, theMissionIdData, FALSE))
            theCreatorID        = theMissionIdData.idCreator
            fmmcMissionType     = Get_FM_Mission_Type_For_FM_Cloud_Loaded_Activity(theCreatorID, theMissionIdData.idVariation)
			missionSubtype		= Get_SubType_For_FM_Cloud_Loaded_Activity(theMissionIdData)
            theMissionName      = Get_Mission_Name_For_FM_Cloud_Loaded_Activity(theMissionIdData)
            theDescription      = Get_Mission_Description_For_FM_Cloud_Loaded_Activity(theMissionIdData)
            requireDesc         = TRUE
            DescHash            = Get_Mission_Dec_Hash_For_FM_Cloud_Loaded_Activity(theMissionIdData)
			isPushBikeOnly		= Is_FM_Cloud_Loaded_Activity_A_Bicycle_Race(theMissionIdData)
			RootContentIdHash   = Get_RootContentIDHash_For_FM_Cloud_Loaded_Activity(theMissionIdData)
			AdversaryModeType	= Get_AdversaryModeType_For_FM_Cloud_Loaded_Activity(theMissionIdData)
			BOOL isPlayed = Get_Played_Status_FM_Cloud_Loaded_Activity(theMissionIdData)
			IF NOT (isPlayed)
				isUnplayed = TRUE
			ENDIF
        ELSE
            // The Mission Header data is not locally available, so treat this as another player's UGC and check if a privilege restriction exists
            // ...use the player's own playerIndex as the creatorID to ensure it isn't classed as Rockstar Created
			BOOL isUnderageInSP = FALSE
            IF (Check_If_Underage_Privilege_Restrictions_Exist(NATIVE_TO_INT(PLAYER_ID()), paramInvitorGH))
				// KGM 22/9/14 [BUG 2044030]: We can't tell if it's a Rockstar Created mission or not. Previously we ditched the invite.
				//								Now we'll let it go through and the decision will be made as the player tries to join the mission.
				IF NOT (NETWORK_IS_GAME_IN_PROGRESS())
					PRINTLN("...KGM MP [JobList]: Cross-session invite received by underage player in SP. Can't check if mission is R* or UGC here, so allow it to go through to test when joining mission.")
					isUnderageInSP = TRUE
				ELSE
                	// Tell Code to delete the Invite
               	 	Delete_Cross_Session_Invite_With_This_InviteID(paramInviteID)
 		
					// Display a help message if appropriate
//					IF (IS_XBOX360_VERSION() OR IS_PS3_VERSION())
//					AND NOT (NETWORK_IS_GAME_IN_PROGRESS())
//						// For LastGen SP, display a slightly different error message that doesn't mention UGC
//						Display_LastGen_SP_Restriction_Help_Message()
//					ELSE
						// Otherwise use the standard message
						Display_Restriction_Help_Message()
//					ENDIF
	          
	                RETURN FALSE
				ENDIF
            ENDIF
    
			// ...check if Network privileges are good
			// KGM 26/9/14 [BUG 2044030]: As a follow on from the new check above for underage accounts in SP, also need to allow this check to pass
			// JA 13/11/15 [BUG 2601590]: Code now perform the privilege check for us when the it is a presence invite
			
			
			IF NOT IS_XBOX_PLATFORM()
				IF NOT (isUnderageInSP)
				    IF (Check_If_Content_Privilege_Restrictions_Exist(NATIVE_TO_INT(PLAYER_ID()), paramInvitorGH))
		                // Tell Code to delete the Invite
		                Delete_Cross_Session_Invite_With_This_InviteID(paramInviteID)
						
						// Display a help message if appropriate
						Display_Restriction_Help_Message()
						
				        RETURN FALSE
				    ENDIF
				ELSE
					PRINTLN("...KGM MP [JobList]: Cross-session invite received by underage player in SP. Also ignoring the Content Privileges Check.")
				ENDIF
			ELSE
				PRINTLN("...KGM MP [JobList]: Cross-session invite received on XB1. Ignoring the Content Privileges Check.")
			ENDIF
            
			// ...check if Xbox Live privileges are good
			IF NOT (XBOX_ONLY_Check_If_Player_Has_LIVE_Privileges())
                // Tell Code to delete the Invite
                Delete_Cross_Session_Invite_With_This_InviteID(paramInviteID)
 		
				// Display a help message
				BOOL displayXboxLiveHelp = TRUE
				Display_Restriction_Help_Message(displayXboxLiveHelp)
          
                RETURN FALSE
			ENDIF
			
            // ...check if the content is restricted based on the invitor's gamer handle
			// KGM 1/10/14 [BUG 2044030]: As a follow on from the new check above for underage accounts in SP, also need to allow this check to pass
			BOOL displayXboxLiveHelpIfFriendsRestriction = FALSE
			IF NOT (isUnderageInSP)
	            IF (Check_If_CSInvite_GH_Blocked_By_Friends_Only_Restriction(paramInvitorGH, displayXboxLiveHelpIfFriendsRestriction, TRUE))
	                // Tell Code to delete the Invite
	                Delete_Cross_Session_Invite_With_This_InviteID(paramInviteID)
			
					// Display a help message if appropriate
					Display_Restriction_Help_Message(displayXboxLiveHelpIfFriendsRestriction)
	           
	                RETURN FALSE
	            ENDIF
			ELSE
				PRINTLN("...KGM MP [JobList]: Cross-session invite received by underage player in SP. Also ignoring the Blocked By Friends Only Check.")
			ENDIF
            
			#IF FEATURE_COPS_N_CROOKS
			IF IS_THIS_CLOUD_FILE_NAME_ARCADE_MODE(paramCloudFilename)
			
				// Arcade mode invite, no download or description needed
				PRINTLN("...KGM MP [JobList]: Invite is for arcade mode with descriptor ", paramCloudFilename, " (", GET_FILENAME_FOR_AUDIO_CONVERSATION(paramCloudFilename),").")
			
				toBeDownloaded      = FALSE
	            isDownloaded        = TRUE
	            requireDesc         = FALSE
				fmmcMissionType		= FMMC_TYPE_CNC_INVITE
				
			ELSE
			#ENDIF
			
            // Need to download the mission
            toBeDownloaded      = TRUE
            isDownloaded        = FALSE
            requireDesc         = TRUE
			
			#IF FEATURE_COPS_N_CROOKS
			ENDIF
			#ENDIF
        ENDIF
    ENDIF
    
    // Store the details
    // This should be the only place that adds entries to the array, so move the array contents down to make room at slot 0
    INT toPos = g_numMPCSInvites
    IF (toPos >= MAX_MP_JOBLIST_ENTRIES)
        toPos--
    ENDIF
    
    INT fromPos = toPos - 1
    WHILE (fromPos >= 0)
        g_sJLCSInvitesMP[toPos] = g_sJLCSInvitesMP[fromPos]
        toPos--
        fromPos--
    ENDWHILE
    
    // There is a gap now at slot 0, so increase the number of entries on the array
    g_numMPCSInvites += 1
    IF (g_numMPCSInvites > MAX_MP_JOBLIST_ENTRIES)
        g_numMPCSInvites = MAX_MP_JOBLIST_ENTRIES
    ENDIF
    
    // NOTE: Storage Slot should always be 0 for a new entry
    INT storageSlot = 0
    
    // General variables
    g_sJLCSInvitesMP[storageSlot].jcsiActive                = FALSE
    g_sJLCSInvitesMP[storageSlot].jcsiAllowDisplay          = FALSE
	g_sJLCSInvitesMP[storageSlot].jcsiInviteRead            = FALSE
    g_sJLCSInvitesMP[storageSlot].jcsiToDownload            = toBeDownloaded
    g_sJLCSInvitesMP[storageSlot].jcsiDownloading           = FALSE
    g_sJLCSInvitesMP[storageSlot].jcsiDownloaded            = isDownloaded
    g_sJLCSInvitesMP[storageSlot].jcsiContentID             = paramCloudFilename
    g_sJLCSInvitesMP[storageSlot].jlRootContentIdHash       = RootContentIdHash
    g_sJLCSInvitesMP[storageSlot].jlAdversaryModeType       = AdversaryModeType
    g_sJLCSInvitesMP[storageSlot].jcsiInvitorGamerTag       = paramInvitorName
	g_sJLCSInvitesMP[storageSlot].jcsiInvitorDisplayName	= ""
    g_sJLCSInvitesMP[storageSlot].jcsiCreatorID             = theCreatorID
    g_sJLCSInvitesMP[storageSlot].jcsiType                  = fmmcMissionType
	g_sJLCSInvitesMP[storageSlot].jcsiSubtype				= missionSubtype
    g_sJLCSInvitesMP[storageSlot].jcsiInviteID              = paramInviteID
    g_sJLCSInvitesMP[storageSlot].jcsiMissionName           = theMissionName
    g_sJLCSInvitesMP[storageSlot].jcsiDescription           = theDescription
    g_sJLCSInvitesMP[storageSlot].jcsiDescHash              = DescHash
    g_sJLCSInvitesMP[storageSlot].jcsiDescLoadVars.nHashOld = 0
    g_sJLCSInvitesMP[storageSlot].jcsiDescLoadVars.iRequest = 0
    g_sJLCSInvitesMP[storageSlot].jcsiDescLoadVars.bSucess  = FALSE
    g_sJLCSInvitesMP[storageSlot].jcsiRequireDesc           = requireDesc
    g_sJLCSInvitesMP[storageSlot].jcsiPlaylistTotal         = paramTotalInPlaylist
    g_sJLCSInvitesMP[storageSlot].jcsiFeedID                = NO_JOBLIST_INVITE_FEED_ID
	g_sJLCSInvitesMP[storageSlot].jcsiIsUnplayed			= isUnplayed
	g_sJLCSInvitesMP[storageSlot].jcsiOrderIndex			= g_inviteOrderCounter
	g_sJLCSInvitesMP[storageSlot].jcsiIsPushBikeOnly		= isPushBikeOnly
	g_sJLCSInvitesMP[storageSlot].jcsiInvitorIsFriend		= NETWORK_IS_FRIEND(paramInvitorGH)
	
	// KGM 14/12/14 [BUG 2157535]: I'll need to address this, but for now - don't display the description for a Heist Finale
	// NOTE: What I'll need to do is find out if it is a Cutscene invite and display the umbrella description instead
	IF (g_sJLCSInvitesMP[storageSlot].jcsiType = FMMC_TYPE_MISSION)
		IF (g_sJLCSInvitesMP[storageSlot].jcsiSubtype = FMMC_MISSION_TYPE_HEIST)
			g_sJLCSInvitesMP[storageSlot].jcsiRequireDesc = FALSE
			
		    #IF IS_DEBUG_BUILD
		        NET_PRINT("...KGM MP [JobList]: ")
		        NET_PRINT_TIME()
		        NET_PRINT("TEMP: Not displaying description for cross-session Heist Finale Invite because invites to cutscenes will need to use umbrella description.")
				NET_NL()
			#ENDIF
		ENDIF
	ENDIF
	
	// Friends will get a repeat of the invite as a reminder shortly afterwards
	g_sJLCSInvitesMP[storageSlot].jcsiRepeatInvite			= Check_If_CSInvite_Reminder_Will_Be_Required(paramInvitorGH, storageSlot)
	g_sJLCSInvitesMP[storageSlot].jcsiRepeatTimeout			= 0
	
	// KGM 9/12/13: NOTE, a current playlist job position > CROSS_SESSION_H2H_WAGER_OFFSET means a H2H Playlist with a wager, the wager is currentplaylist value - CROSS_SESSION_H2H_WAGER_OFFSET
    g_sJLCSInvitesMP[storageSlot].jcsiPlaylistCurrent       = paramCurrentPlaylistJob
	
	g_inviteOrderCounter++	// MMM 1645958 - Added to store the order of the invites
	// KGM 21/8/14: For XboxOne only, the players may have agreed to display Names instead of GamerTags, so this data needs to be retrieved asychronously
	g_sJLCSInvitesMP[storageSlot].jcsiDisplayNameID			= Start_Asynchronous_DisplayName_Retrieval_If_Required(paramInvitorGH)
	
	IF (g_sJLCSInvitesMP[storageSlot].jcsiDisplayNameID != NO_DISPLAY_NAME_RETRIEVAL)
		// Start a safety timeout for display name retrieval
		g_sJLCSInvitesMP[storageSlot].jcsiDisplayNameTimeout = GET_GAME_TIMER() + DISPLAY_NAME_RETRIEVAL_SAFETY_TIMEOUT_msec
	ENDIF
   
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Storing Cross-Session Invite Details At Array Location: ")
        NET_PRINT_INT(storageSlot)
		NET_PRINT("  Display Order: ")
		NET_PRINT_INT(g_sJLCSInvitesMP[storageSlot].jcsiOrderIndex)
        NET_NL()
        NET_PRINT("      MissionName: ")
        NET_PRINT(g_sJLCSInvitesMP[storageSlot].jcsiMissionName)
		IF (g_sJLCSInvitesMP[storageSlot].jcsiRepeatInvite)
	        NET_PRINT("  [ALLOW REPEAT]")
		ENDIF
        IF (paramTotalInPlaylist > 0)
			IF (paramCurrentPlaylistJob >= CROSS_SESSION_TOUR_QUAL_OFFSET)
				NET_PRINT(" - This is a Tournamnet Qualifyer Invite")
				NET_PRINT(" [Total Playlist Jobs: ")
	            NET_PRINT_INT(paramTotalInPlaylist)
				NET_PRINT("]")
			ELIF (paramCurrentPlaylistJob >= CROSS_SESSION_H2H_WAGER_OFFSET)
				NET_PRINT(" - Head-To-Head Playlist Wager: ")
				NET_PRINT_INT(paramCurrentPlaylistJob - CROSS_SESSION_H2H_WAGER_OFFSET)
				NET_PRINT(" [Total Playlist Jobs: ")
	            NET_PRINT_INT(paramTotalInPlaylist)
				NET_PRINT("]")
			ELSE
	            NET_PRINT(" - Playlist ")
	            NET_PRINT_INT(paramCurrentPlaylistJob)
	            NET_PRINT("/")
	            NET_PRINT_INT(paramTotalInPlaylist)
			ENDIF
        ENDIF
        NET_NL()
        NET_PRINT("      InvitorName: ")
        NET_PRINT(paramInvitorName)
		IF (g_sJLCSInvitesMP[storageSlot].jcsiInvitorIsFriend)
			NET_PRINT(" [FRIEND]")
		ENDIF
        IF (Is_There_A_MissionsAtCoords_Focus_Mission())
            NET_PRINT(" [PLAYER HAS A FOCUS MISSION]")
        ELSE
            NET_PRINT(" [player has no focus mission]")
        ENDIF
// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
//        IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
//            NET_PRINT(" [HAS WANTED LEVEL]")
//        ELSE
//            NET_PRINT(" [no wanted level]")
//        ENDIF
        NET_NL()
        NET_PRINT("      CloudFilename: ")
        NET_PRINT(paramCloudFilename)
        IF (toBeDownloaded)
            NET_PRINT(" [TO BE DOWNLOADED]")
        ENDIF
        IF (isDownloaded)
            NET_PRINT(" [DOWNLOAD NOT REQUIRED]")
        ENDIF
		IF (g_sJLCSInvitesMP[storageSlot].jcsiDisplayNameID != NO_DISPLAY_NAME_RETRIEVAL)
            NET_PRINT(" [REQUESTING DISPLAY NAME. ID: ") NET_PRINT_INT(g_sJLCSInvitesMP[storageSlot].jcsiDisplayNameID) NET_PRINT("]")
		ELSE
			NET_PRINT(" [display name not required]")
		ENDIF
        NET_NL()
        
        // Array output
        NET_NL()
        NET_PRINT("...Cross-Session Invite Array. Num Entries: ") NET_PRINT_INT(g_numMPCSInvites) NET_NL()
        INT tempLoop = 0
        REPEAT g_numMPCSInvites tempLoop
            NET_PRINT("   ")
            NET_PRINT_INT(tempLoop)
            NET_PRINT("   ")
            NET_PRINT(g_sJLCSInvitesMP[tempLoop].jcsiInvitorGamerTag)
			NET_PRINT(" [")
			NET_PRINT_INT(g_sJLCSInvitesMP[tempLoop].jcsiOrderIndex)
			NET_PRINT("]")
            IF (g_sJLCSInvitesMP[tempLoop].jcsiToDownload)
                NET_PRINT(" [TO DOWNLOAD]")
            ENDIF
            IF (g_sJLCSInvitesMP[tempLoop].jcsiDownloading)
                NET_PRINT(" [DOWNLOADING]")
            ENDIF
            IF (g_sJLCSInvitesMP[tempLoop].jcsiDownloaded)
                NET_PRINT(" [DOWNLOADED]")
            ENDIF
            NET_NL()
        ENDREPEAT
    #ENDIF
     
	RETURN TRUE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store details on the Basic Invite Details array to display on the Joblist where appropriate
//
// INPUT PARAMS:            paramType               Type of Mission
//                          paramInvitorAsInt       Player Index or enumCharacterList of player or in-game character that sent the invite (as an INT)
//                          paramInvitorIsPlayer    TRUE if the invitor is a player, FALSE if it is an in-game charancter
//                          paramMissionName        The Mission Name (as a STRING)
//                          paramTL                 An Additional Text Label to hold additional details for the Invite screen
//							paramAllowDelete		[DEFAULT = TRUE] TRUE if the Invite can be deleted, FALSE if not
//                          paramInt                [DEFAULT = -1] An optional INT to hold additional details for the Invite screen
//                          paramInt2               [DEFAULT = -1] An optional INT to hold additional details for the Invite screen
PROC private_Store_RVInvite_Details_On_Joblist(INT paramType, INT paramInvitorAsInt, BOOL paramInvitorIsPlayer, STRING paramMissionName, STRING paramTL, BOOL paramAllowDelete = TRUE, INT paramInt = -1, INT paramInt2 = -1,INT propertyType = -1, INT RootId = 0, BOOL bForceToTop = FALSE)
    
    PLAYER_INDEX        thePlayer   = INVALID_PLAYER_INDEX()
    enumCharacterList   theNPC      = NO_CHARACTER
    
    IF (paramInvitorIsPlayer)
        thePlayer   = INT_TO_PLAYERINDEX(paramInvitorAsInt)
    ELSE
        theNPC      = INT_TO_ENUM(enumCharacterList, paramInvitorAsInt)
    ENDIF

    // Ignore basic invites from players for solo sessions (just in case - these should never happen!)
    IF (paramInvitorIsPlayer)
        IF (Joblist_Is_This_A_Solo_Session())
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("SOLO SESSION - Ditching Basic Invite from player: ")
                NET_PRINT(GET_PLAYER_NAME(thePlayer))
                NET_NL()
            #ENDIF
            
            EXIT
        ENDIF
    ENDIF
    
    // This should be the only place that adds entries to the array, so move the array contents down to make room at slot 0
    INT toPos = g_numMPRVInvites
    IF (toPos >= MAX_MP_JOBLIST_ENTRIES)
        toPos--
    ENDIF
    
    INT fromPos = toPos - 1
    WHILE (fromPos >= 0)
        g_sJLRVInvitesMP[toPos]     = g_sJLRVInvitesMP[fromPos]
        toPos--
        fromPos--
    ENDWHILE
    
    // There is a gap now at slot 0, so increase the number of entries on the array
    g_numMPRVInvites += 1
    IF (g_numMPRVInvites > MAX_MP_JOBLIST_ENTRIES)
        g_numMPRVInvites = MAX_MP_JOBLIST_ENTRIES
    ENDIF
    
    // NOTE: Storage Slot should always be 0 for a new entry
    INT storageSlot = 0
	
   	IF g_sJLRVInvitesMP[1].jliForceToTop
        PRINTLN("...KGM MP [JobList]: g_sJLRVInvitesMP[1].jrviLockToTop = TRUE")
		g_sJLRVInvitesMP[0] = g_sJLRVInvitesMP[1]
        PRINTLN("...KGM MP [JobList]: g_sJLRVInvitesMP[0] = g_sJLRVInvitesMP[1]")
		g_structJobListRVInvitesMP g_structJobListRVInvitesMPMPTemp
		g_sJLRVInvitesMP[1] = g_structJobListRVInvitesMPMPTemp
		storageSlot = 1
	ENDIF
	
	IF storageSlot = 0
   		g_sJLRVInvitesMP[storageSlot].jliForceToTop		= bForceToTop
	ELSE
   		g_sJLRVInvitesMP[storageSlot].jliForceToTop		= FALSE
	ENDIF
    
    g_sJLRVInvitesMP[storageSlot].jrviActive            = FALSE
    g_sJLRVInvitesMP[storageSlot].jrviAccepted          = FALSE
    g_sJLRVInvitesMP[storageSlot].jrviAllowDisplay      = FALSE
	g_sJLRVInvitesMP[storageSlot].jrviInviteRead      	= FALSE
    g_sJLRVInvitesMP[storageSlot].jrviPlayer            = thePlayer
    g_sJLRVInvitesMP[storageSlot].jrviNPC               = theNPC
    g_sJLRVInvitesMP[storageSlot].jrviInvitorIsPlayer   = paramInvitorIsPlayer
    g_sJLRVInvitesMP[storageSlot].jrviType              = paramType
	g_sJLRVInvitesMP[storageSlot].jrviSubType			= -1
    g_sJLRVInvitesMP[storageSlot].jrviMissionName       = paramMissionName
    g_sJLRVInvitesMP[storageSlot].jrviFeedID            = NO_JOBLIST_INVITE_FEED_ID
    g_sJLRVInvitesMP[storageSlot].jrviOptionalTL        = paramTL
    g_sJLRVInvitesMP[storageSlot].jrviOptionalInt1      = paramInt
    g_sJLRVInvitesMP[storageSlot].jrviOptionalInt2      = paramInt2
    g_sJLRVInvitesMP[storageSlot].jrviRequestClosed  	= FALSE
    g_sJLRVInvitesMP[storageSlot].jrviClosedTimeout  	= GET_NETWORK_TIME()
	g_sJLRVInvitesMP[storageSlot].jrviAllowDelete		= paramAllowDelete
	g_sJLRVInvitesMP[storageSlot].jrviOrderIndex		= g_inviteOrderCounter
	g_sJLRVInvitesMP[storageSlot].jrviPropertyType 		= propertyType
	g_sJLRVInvitesMP[storageSlot].jrviRootId 			= RootId
	
	// KGM 22/1/15 [BUG 2207793]: For a Quick Heist Invite I need to know if it's for a Setup or Finale now, so storing the MissionSubtypeID to specify
	IF (paramType = FMMC_TYPE_HEIST_PHONE_INVITE)
	OR (paramType = FMMC_TYPE_HEIST_PHONE_INVITE_2)
		// Store the Quick Heist Invite SubType so that the correct text gets displayed in certain places
		g_sJLRVInvitesMP[storageSlot].jrviSubType = Get_Leader_Active_Heist_Strand_At_Planning_Or_Finale()
	ELIF paramType = FMMC_TYPE_FMC_INVITE
		g_sJLRVInvitesMP[storageSlot].jrviSubType = paramInt
	ELIF paramType = FMMC_TYPE_SIMPLE_INTERIOR
		//For simple interior invites we now need to consider private or public instances
		g_sJLRVInvitesMP[storageSlot].jrviSubType = paramInt
	ENDIF
	
	g_inviteOrderCounter++	// MMM 1645958 - Added to store the order of the invites (Basic Invites now also use the counter - added by Keith 15/9/14)
    
    // Update the 'Recent Invite' timer when an invite is accepted (this will block new phonecalls even if the invite can't yet be activated)
    Update_Recent_Invite_Received_Timer()
	
	// KGM 12/7/14: NextGen only. An Asynchronous Permissions Check is required on NextGen for player to player invites.
	//				This ensures that an invite received by this player from another player that this player has blocked doesn't get received.
	// NOTE: ONLY DO THIS FOR XBOX ONE.
	g_sJLRVInvitesMP[storageSlot].jrviPermissionCheckID		= 0
	g_sJLRVInvitesMP[storageSlot].jrviDoingPermissionCheck	= FALSE
	g_sJLRVInvitesMP[storageSlot].jrviPermissionCheckFail	= FALSE
			
	IF (IS_XBOX_PLATFORM())
		IF (paramInvitorIsPlayer)
			// KGM SAFETY SWITCH-OFF
			BOOL checkPermissions = TRUE
			#IF IS_DEBUG_BUILD
				IF (GET_COMMANDLINE_PARAM_EXISTS("sc_tempKeithNGBlockCheckOff"))
					checkPermissions = FALSE
					PRINTLN(".KGM [JobList]: Basic Invite Asynchronous permissions start check OFF because of -sc_tempKeithNGBlockCheckOff commandline parameter")
				ENDIF
			#ENDIF
			
			IF (checkPermissions)
			// END KGM SAFETY SWITCH-OFF
		
			// Setup the Asynchronous Permissions check and store the return ID
			GAMER_HANDLE ghInvitingPlayer = GET_GAMER_HANDLE_PLAYER(thePlayer)
			INT theCheckID = NETWORK_START_COMMUNICATION_PERMISSIONS_CHECK(ghInvitingPlayer)
			
			IF (theCheckID != -1)
				g_sJLRVInvitesMP[storageSlot].jrviPermissionCheckID		= theCheckID
				g_sJLRVInvitesMP[storageSlot].jrviDoingPermissionCheck	= TRUE
				g_sJLRVInvitesMP[storageSlot].jrviPermissionCheckFail	= FALSE
				
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [JobList]: Starting Asynchronous Permissions check on GH of this Basic Invite Player: ", GET_PLAYER_NAME(thePlayer), ". CheckID: ", g_sJLRVInvitesMP[storageSlot].jrviPermissionCheckID)
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [JobList]: Asynchronous Permissions check on GH of this Inviting Player: ", GET_PLAYER_NAME(thePlayer), " returned a CheckID of -1. - IGNORING CHECK. ALLOWING BASIC INVITE")
				#ENDIF
			ENDIF
		
			// KGM SAFETY SWITCH-OFF
			ENDIF
			// END KGM SAFETY SWITCH-OFF
		ENDIF
	ENDIF
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Storing Basic Invite Details At Array Location: ")
        NET_PRINT_INT(storageSlot)
		NET_PRINT("  Display Order: ")
		NET_PRINT_INT(g_sJLRVInvitesMP[storageSlot].jrviOrderIndex)
        NET_NL()
        NET_PRINT("      Mission: ")
        NET_PRINT(paramMissionName)
        IF (Is_There_A_MissionsAtCoords_Focus_Mission())
            NET_PRINT(" [PLAYER HAS A FOCUS MISSION]")
        ELSE
            NET_PRINT(" [player has no focus mission]")
        ENDIF
// KGM 31/8/13: ToDo 1605311 (Pay to lose Wanted Level when accepting invites)
//        IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
//            NET_PRINT(" [HAS WANTED LEVEL - BUT IGNORE THIS]")
//        ELSE
//            NET_PRINT(" [no wanted level]")
//        ENDIF
        NET_NL()
        
        // TEMP array output
        NET_NL()
        NET_PRINT("...Basic Invite Array. Num Entries: ") NET_PRINT_INT(g_numMPRVInvites) NET_NL()
        INT tempLoop = 0
        REPEAT g_numMPRVInvites tempLoop
            NET_PRINT("   ")
            NET_PRINT_INT(tempLoop)
            NET_PRINT("   ")
            IF (g_sJLRVInvitesMP[tempLoop].jrviInvitorIsPlayer)
                IF (IS_NET_PLAYER_OK(g_sJLRVInvitesMP[tempLoop].jrviPlayer, FALSE))
                    NET_PRINT(GET_PLAYER_NAME(g_sJLRVInvitesMP[tempLoop].jrviPlayer))
                ELSE
                    NET_PRINT("UNKNOWN PLAYER")
                ENDIF
            ELSE
                TEXT_LABEL  npcNameTL = Get_NPC_Name(g_sJLRVInvitesMP[tempLoop].jrviNPC)
                NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            ENDIF
            NET_PRINT("   ")
            NET_PRINT(Get_FM_Joblist_Activity_TextLabel(g_sJLRVInvitesMP[tempLoop].jrviType, g_sJLRVInvitesMP[tempLoop].jrviSubType,g_sJLRVInvitesMP[tempLoop].jrviPropertyType,g_sJLRVInvitesMP[tempLoop].jrviRootId))
			NET_PRINT(" [")
			NET_PRINT_INT(g_sJLRVInvitesMP[tempLoop].jrviOrderIndex)
			NET_PRINT("]")
            NET_NL()
        ENDREPEAT
    #ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store the details of an invite from a player that only expects a flag to be set when accepted (ie: no warp, fadeout, etc).
//
// INPUT PARAMS:            paramPlayerID       PlayerID for Invitor
//                          paramType           Type of Mission
//                          paramMissionName    The Mission Name as a literal string
//                          paramTL             An Additional Text Label to hold additional details for the Invite screen
//							paramAllowDelete	[DEFAULT = TRUE] TRUE if this basic invite can be deleted, otherwise FALSE
//                          paramInt            [DEFAULT = -1] An optional INT to hold additional details for the Invite screen
//                          paramInt2           [DEFAULT = -1] An optional INT to hold additional details for the Invite screen
PROC Store_Basic_Invite_Details_For_Joblist_From_Player_With_Params(PLAYER_INDEX paramPlayerID, INT paramType, STRING paramMissionName, STRING paramTL, BOOL paramAllowDelete = TRUE, INT paramInt = -1, INT paramInt2 = -1, INT inviteTypeProperty = -1)
    
    // Ditch Basic Invites from players if the player is playing the tutorial
    IF (IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Player is playing the tutorial - Ditching Basic Invite for ")
            NET_PRINT(paramMissionName)
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // Check Player Name
    IF NOT (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Store_Basic_Invite_Details_For_Joblist_From_Player() - IGNORED: Inviting player is not OK")
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
	
	// KGM 25/8/14 [BUG 2001821] - Communications Check no longer needed on any console (NETWORK_CAN_COMMUNICATE_WITH_GAMER)
    
    // Only allow one invitation per player, so supersede any existing entry (also move it back to the top of the array)
    INT existingSlot = Get_Joblist_RVInvite_Slot_For_Player(paramPlayerID)
    IF (existingSlot != NO_JOBLIST_ARRAY_ENTRY)
        // ...an existing entry exists for this player, so delete it
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Player Basic Invitation already exists: ")
            NET_PRINT_INT(existingSlot)
            NET_NL()
        #ENDIF
		
        Delete_Joblist_RVInvite_In_Invitation_Slot(existingSlot)
    ENDIF
    
    // Store the details
    INT     invitingPlayerAsInt = NATIVE_TO_INT(paramPlayerID)
    BOOL    invitorIsPlayer     = TRUE
    
    private_Store_RVInvite_Details_On_Joblist(paramType, invitingPlayerAsInt, invitorIsPlayer, paramMissionName, paramTL, paramAllowDelete, paramInt, paramInt2,inviteTypeProperty )
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store the details of an invite from a player that only expects a flag to be set when accepted (ie: no warp, fadeout, etc).
//
// INPUT PARAMS:            paramPlayerID       PlayerID for Invitor
//                          paramType           Type of Mission
//                          paramMissionName    The Mission Name as a literal string
//							paramAllowDelete	[DEFAULT = TRUE] TRUE if this basic invite can be deleted, otherwise FALSE
PROC Store_Basic_Invite_Details_For_Joblist_From_Player(PLAYER_INDEX paramPlayerID, INT paramType, STRING paramMissionName, BOOL paramAllowDelete = TRUE)
    
    // Forward the details to the main function that accepts additional parameters
    TEXT_LABEL_31   ignoreTL            = ""
    
    Store_Basic_Invite_Details_For_Joblist_From_Player_With_Params(paramPlayerID, paramType, paramMissionName, ignoreTL, paramAllowDelete)
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store the details of an invite from an NPC that only expects a flag to be set when accepted (ie: no warp, fadeout, etc).
//
// INPUT PARAMS:            paramInvitorNPC     The charSheetID of the Inviting NPC
//                          paramType           Type of Mission
//                          paramMissionName    The Mission Name as a literal string
//							paramAllowDelete	[DEFAULT = TRUE] TRUE if this basic invite can be deleted, otherwise FALSE
PROC Store_Basic_Invite_Details_For_Joblist_From_NPC(enumCharacterList paramInvitorNPC, INT paramType, STRING paramMissionName, BOOL paramAllowDelete = TRUE, INT iStrandRootId = 0, BOOL bOverWrite = TRUE, BOOL bForceToTop = FALSE)
    
    // Only allow one invitation per player, so supersede any existing entry (also move it back to the top of the array)
    INT existingSlot = Get_Joblist_RVInvite_Slot_For_NPC(paramInvitorNPC)
   	IF bOverWrite
		IF (existingSlot != NO_JOBLIST_ARRAY_ENTRY)
	        // ...an existing entry exists for this NPC, so delete it
	        #IF IS_DEBUG_BUILD
	            NET_PRINT("...KGM MP [JobList]: ")
	            NET_PRINT_TIME()
	            NET_PRINT("NPC Basic Invitation already exists: ")
	            NET_PRINT_INT(existingSlot)
	            NET_NL()
	        #ENDIF
	        
	        Delete_Joblist_RVInvite_In_Invitation_Slot(existingSlot)
	    ENDIF
    ENDIF
    
    // Store the details
    INT             invitingNPCAsInt    = ENUM_TO_INT(paramInvitorNPC)
    BOOL            invitorIsPlayer     = FALSE
    TEXT_LABEL_31   ignoreTL            = ""
    
    private_Store_RVInvite_Details_On_Joblist(paramType, invitingNPCAsInt, invitorIsPlayer, paramMissionName, ignoreTL, paramAllowDelete, DEFAULT, DEFAULT, DEFAULT, iStrandRootId, bForceToTop)
    
ENDPROC

PROC Store_Basic_Invite_Details_For_Joblist_From_NPC_With_Params(enumCharacterList paramInvitorNPC, INT paramType, STRING paramMissionName, STRING paramTL, BOOL paramAllowDelete = TRUE, INT iStrandRootId = 0, BOOL bOverWrite = TRUE, BOOL bForceToTop = FALSE, INT paramInt = -1, INT paramInt2 = -1, INT inviteTypeProperty = -1)
    
    
    INT existingSlot = Get_Joblist_RVInvite_Slot_For_NPC(paramInvitorNPC)
	IF bOverWrite
	    IF (existingSlot != NO_JOBLIST_ARRAY_ENTRY)
	        // ...an existing entry exists for this player, so delete it
	        #IF IS_DEBUG_BUILD
	            NET_PRINT("...KGM MP [JobList]: ")
	            NET_PRINT_TIME()
	            NET_PRINT("NPC Basic Invitation already exists: ")
	            NET_PRINT_INT(existingSlot)
	            NET_NL()
	        #ENDIF
			
	        Delete_Joblist_RVInvite_In_Invitation_Slot(existingSlot)
	    ENDIF
    ENDIF
    // Store the details
    INT    			invitingNPCAsInt	= ENUM_TO_INT(paramInvitorNPC)
    BOOL    		invitorIsPlayer     = FALSE
    
    private_Store_RVInvite_Details_On_Joblist(paramType, invitingNPCAsInt, invitorIsPlayer, paramMissionName, paramTL, paramAllowDelete, paramInt, paramInt2, inviteTypeProperty, iStrandRootId, bForceToTop)
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a Basic Invite from this player exists
//
// INPUT PARAMS:            paramPlayerID       The Player Index of the inviting player
//                          paramType           The FM Type being checked for
// RETURN VALUE:            BOOL                TRUE if a Basic Invite from a Player has been received and accepted by the player, otherwise FALSE
FUNC BOOL Does_Basic_Invite_From_Player_Exist(PLAYER_INDEX paramPlayerID, INT paramType)

    // Ignore if solo session
    IF (Joblist_Is_This_A_Solo_Session())
        RETURN FALSE
    ENDIF

    // Ignore if there isn't a Basic Invite from this player
    INT existingSlot = Get_Joblist_RVInvite_Slot_For_Player(paramPlayerID)
    IF (existingSlot = NO_JOBLIST_ARRAY_ENTRY)
        RETURN FALSE
    ENDIF
    
    // There is a Basic Invite, so check if it is for the correct activity type
    IF NOT (paramType = g_sJLRVInvitesMP[existingSlot].jrviType)
        RETURN FALSE
    ENDIF
    
    // Invite Exists
    RETURN TRUE

ENDFUNC

// PURPOSE: Check if a Basic Invite from this player exists and returns the slot
//
// INPUT PARAMS:            paramPlayerID       The Player Index of the inviting player
//                          paramType           The FM Type being checked for
// RETURN VALUE:            BOOL                TRUE if a Basic Invite from a Player has been received and accepted by the player, otherwise FALSE
FUNC BOOL Get_Basic_Invite_Slot_From_Player(PLAYER_INDEX paramPlayerID, INT paramType, INT &iSlot)

    // Ignore if solo session
    IF (Joblist_Is_This_A_Solo_Session())
        RETURN FALSE
    ENDIF

    // Ignore if there isn't a Basic Invite from this player
    INT existingSlot = Get_Joblist_RVInvite_Slot_For_Player(paramPlayerID)
    IF (existingSlot = NO_JOBLIST_ARRAY_ENTRY)
        RETURN FALSE
    ENDIF
    
    // There is a Basic Invite, so check if it is for the correct activity type
    IF NOT (paramType = g_sJLRVInvitesMP[existingSlot].jrviType)
        RETURN FALSE
    ENDIF
    
	iSlot = existingSlot
	
    // Invite Exists
    RETURN TRUE

ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if a Basic Invite from this NPC exists
//
// INPUT PARAMS:            paramInvitorNPC     The charSheetID of the Inviting NPC
//                          paramType           The FM Type being checked for
// RETURN VALUE:            BOOL                TRUE if a Basic Invite from a Player has been received and accepted by the player, otherwise FALSE
FUNC BOOL Does_Basic_Invite_From_NPC_Exist(enumCharacterList paramInvitorNPC, INT paramType)

    // Ignore if there isn't a Basic Invite from this NPC
    INT existingSlot = Get_Joblist_RVInvite_Slot_For_NPC(paramInvitorNPC)
    IF (existingSlot = NO_JOBLIST_ARRAY_ENTRY)
        RETURN FALSE
    ENDIF
    
    // There is a Basic Invite, so check if it is for the correct activity type
    IF NOT (paramType = g_sJLRVInvitesMP[existingSlot].jrviType)
        RETURN FALSE
    ENDIF
    
    // Invite Exists
    RETURN TRUE

ENDFUNC

FUNC BOOL Does_Basic_Invite_From_NPC_Exist_of_Type(enumCharacterList paramInvitorNPC, INT paramType)
    INT tempLoop = 0
    REPEAT g_numMPRVInvites tempLoop
        IF NOT (g_sJLRVInvitesMP[tempLoop].jrviInvitorIsPlayer)
            IF (g_sJLRVInvitesMP[tempLoop].jrviNPC = paramInvitorNPC)
			AND g_sJLRVInvitesMP[tempLoop].jrviType = paramType
                // Found an invitation by the selected in-game character
                RETURN TRUE
            ENDIF
        ENDIF
    ENDREPEAT
    
    // No invitation by selected in-game character
    RETURN FALSE

ENDFUNC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the player has accepted a Basic Invite from a player
//
// INPUT PARAMS:            paramPlayerID       The Player Index of the inviting player
//                          paramType           The FM Type being checked for
// RETURN VALUE:            BOOL                TRUE if a Basic Invite from a Player has been received and accepted by the player, otherwise FALSE
//
// NOTES:   This function will also deactivate the Basic Invite, so it's a one-off
FUNC BOOL Has_Player_Accepted_Basic_Invite_From_Player(PLAYER_INDEX paramPlayerID, INT paramType)
	
    // Ignore if solo session
    IF (Joblist_Is_This_A_Solo_Session())
        RETURN FALSE
    ENDIF

    // Ignore if there isn't a Basic Invite from this player
    INT existingSlot = Get_Joblist_RVInvite_Slot_For_Player(paramPlayerID)
    IF (existingSlot = NO_JOBLIST_ARRAY_ENTRY)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("ERROR: Has_Player_Accepted_Basic_Invite_From_Player() - There is no Basic Invite from this player: ")
            NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
            NET_NL()
        #ENDIF
        
        RETURN FALSE
    ENDIF
    
    // There is a Basic Invite, so check if it is for the correct activity type
    IF NOT (paramType = g_sJLRVInvitesMP[existingSlot].jrviType)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("ERROR: Has_Player_Accepted_Basic_Invite_From_Player() - Basic Invite from this player is for a different mission type: ")
            NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
            NET_NL()
        #ENDIF
        
        RETURN FALSE
    ENDIF
    
    // Check if it has been accepted by the player
    IF NOT (g_sJLRVInvitesMP[existingSlot].jrviAccepted)
        RETURN FALSE
    ENDIF
    
    // The Basic Invite has been accepted and this is about to be returned to the calling script, so cleanup the Basic Invite
    Delete_Joblist_RVInvite_In_Invitation_Slot(existingSlot)
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Calling script is being informed the Basic Invite from Player has been accepted. Cleaning up the Basic Invite. [Requested by: ")
        NET_PRINT(GET_THIS_SCRIPT_NAME())
        NET_PRINT("]")
        NET_NL()
    #ENDIF
    
	g_inviteControlTimeout = GET_GAME_TIMER() + INVITE_ACCEPT_CONTROLS_BLOCK_TIMEOUT_msec
	
    RETURN TRUE

ENDFUNC



FUNC BOOL Has_Player_Accepted_Basic_Invite_From_NPC_Of_Type(enumCharacterList paramInvitorNPC, INT paramType)
    INT tempLoop = 0
    REPEAT g_numMPRVInvites tempLoop
        IF NOT (g_sJLRVInvitesMP[tempLoop].jrviInvitorIsPlayer)
            IF (g_sJLRVInvitesMP[tempLoop].jrviNPC = paramInvitorNPC)
   			AND (paramType = g_sJLRVInvitesMP[tempLoop].jrviType)
			AND (g_sJLRVInvitesMP[tempLoop].jrviAccepted)
				g_inviteControlTimeout = GET_GAME_TIMER() + INVITE_ACCEPT_CONTROLS_BLOCK_TIMEOUT_msec
 				Delete_Joblist_RVInvite_In_Invitation_Slot(tempLoop)
			    #IF IS_DEBUG_BUILD
			        NET_PRINT("...KGM MP [JobList]: ")
			        NET_PRINT_TIME()
			        NET_PRINT("Calling script is being informed the Basic Invite from NPC has been accepted. Cleaning up the Basic Invite. [Requested by: ")
			        NET_PRINT(GET_THIS_SCRIPT_NAME())
			        NET_PRINT("]")
			        NET_NL()
			    #ENDIF
                RETURN TRUE
            ENDIF
        ENDIF
    ENDREPEAT
	    
    // No invitation by selected in-game character
    RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the player has accepted a Basic Invite from a NPC
//
// INPUT PARAMS:            paramInvitorNPC     The charSheetID of the Inviting NPC
//                          paramType           The FM Type being checked for
// RETURN VALUE:            BOOL                TRUE if a Basic Invite from a NPC has been received and accepted by the player, otherwise FALSE
//
// NOTES:   This function will also deactivate the Basic Invite, so it's a one-off
FUNC BOOL Has_Player_Accepted_Basic_Invite_From_NPC(enumCharacterList paramInvitorNPC, INT paramType)
	
    // Ignore if there isn't a Basic Invite from this NPC
    INT existingSlot = Get_Joblist_RVInvite_Slot_For_NPC(paramInvitorNPC)
    IF (existingSlot = NO_JOBLIST_ARRAY_ENTRY)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("ERROR: Has_Player_Accepted_Basic_Invite_From_NPC() - There is no Basic Invite from this NPC: ")
            TEXT_LABEL  npcNameTL = Get_NPC_Name(paramInvitorNPC)
            NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            NET_NL()
        #ENDIF
        
        RETURN FALSE
    ENDIF
    
    // There is a Basic Invite, so check if it is for the correct activity type
    IF NOT (paramType = g_sJLRVInvitesMP[existingSlot].jrviType)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("ERROR: Has_Player_Accepted_Basic_Invite_From_NPC() - Basic Invite from this NPC is for a different mission type: ")
            TEXT_LABEL  npcNameTL = Get_NPC_Name(paramInvitorNPC)
            NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            NET_NL()
        #ENDIF
        
        RETURN FALSE
    ENDIF
    
    // Check if it has been accepted by the player
    IF NOT (g_sJLRVInvitesMP[existingSlot].jrviAccepted)
        RETURN FALSE
    ENDIF
    
    // The Basic Invite has been accepted and this is about to be returned to the calling script, so cleanup the Basic Invite
    Delete_Joblist_RVInvite_In_Invitation_Slot(existingSlot)
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Calling script is being informed the Basic Invite from NPC has been accepted. Cleaning up the Basic Invite. [Requested by: ")
        NET_PRINT(GET_THIS_SCRIPT_NAME())
        NET_PRINT("]")
        NET_NL()
    #ENDIF
    
	g_inviteControlTimeout = GET_GAME_TIMER() + INVITE_ACCEPT_CONTROLS_BLOCK_TIMEOUT_msec
	
    RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Allow the Basic Invite from Player to be cancelled
//
// INPUT PARAMS:            paramPlayerID       The Player Index of the inviting player
//                          paramType           The FM Type being checked for

// NOTE:    It will also be automatically cancelled when the calling script has been informed that the player accepted it
PROC Cancel_Basic_Invite_From_Player(PLAYER_INDEX paramPlayerID, INT paramType)

    INT existingSlot = Get_Joblist_RVInvite_Slot_For_Player(paramPlayerID)
    IF (existingSlot = NO_JOBLIST_ARRAY_ENTRY)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Cancel_Basic_Invite_From_Player() - There is no Basic Invite from this player: ")
            NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // There is a Basic Invite, so check if it is for the correct activity type
    IF NOT (paramType = g_sJLRVInvitesMP[existingSlot].jrviType)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Cancel_Basic_Invite_From_Player() - Basic Invite from this player is for a different mission type: ")
            NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
	
	// MMM 1695633 - Check if the invite that is no longer valid has been read, 
	// if it hasn't we need to decrement the indicator		
	IF NOT (g_sJLRVInvitesMP[existingSlot].jrviInviteRead) AND g_sJLRVInvitesMP[existingSlot].jrviActive
		NET_PRINT("[MMM][JobList] Basic Invite from Player Cancelled. Decrementing Invite Counter.")
    	NET_NL()
		g_sJLRVInvitesMP[existingSlot].jrviInviteRead = TRUE	// Probably not needed, but just incase	
		Decrement_New_Invites_Indicator()
	ENDIF
	
    Delete_Joblist_RVInvite_In_Invitation_Slot(existingSlot)
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Basic Invite from Player has been cancelled. [Requested by: ")
        NET_PRINT(GET_THIS_SCRIPT_NAME())
        NET_PRINT("] - ")
        NET_PRINT(GET_PLAYER_NAME(paramPlayerID))
        NET_NL()
    #ENDIF
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Allow the Basic Invite from NPC to be cancelled
//
// INPUT PARAMS:            paramInvitorNPC     The charSheetID of the Inviting NPC
//                          paramType           The FM Type being checked for
//
// NOTE:    It will also be automatically cancelled when the calling script has been informed that the player accepted it
PROC Cancel_Basic_Invite_From_NPC(enumCharacterList paramInvitorNPC, INT paramType)

    INT existingSlot = Get_Joblist_RVInvite_Slot_For_NPC(paramInvitorNPC)
    IF (existingSlot = NO_JOBLIST_ARRAY_ENTRY)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Cancel_Basic_Invite_From_NPC() - There is no Basic Invite from this NPC: ")
            TEXT_LABEL  npcNameTL = Get_NPC_Name(paramInvitorNPC)
            NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // There is a Basic Invite, so check if it is for the correct activity type
    IF NOT (paramType = g_sJLRVInvitesMP[existingSlot].jrviType)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Cancel_Basic_Invite_From_NPC() - Basic Invite from this NPC is for a different mission type: ")
            TEXT_LABEL  npcNameTL = Get_NPC_Name(paramInvitorNPC)
            NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
	
	// MMM Invite Decrement
	IF NOT (g_sJLRVInvitesMP[existingSlot].jrviInviteRead) AND g_sJLRVInvitesMP[existingSlot].jrviActive
		NET_PRINT("[MMM][JobList] RV Invite from NPC Cancelled. Decrementing Invite Counter. ")
    	NET_NL()
		g_sJLRVInvitesMP[existingSlot].jrviInviteRead = TRUE	// Probably not needed, but just incase	
		Decrement_New_Invites_Indicator()
	ENDIF
	
    Delete_Joblist_RVInvite_In_Invitation_Slot(existingSlot)
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Basic Invite from NPC has been cancelled. [Requested by: ")
        NET_PRINT(GET_THIS_SCRIPT_NAME())
        NET_PRINT("] - ")
        TEXT_LABEL  npcNameTL = Get_NPC_Name(paramInvitorNPC)
        NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
        NET_NL()
    #ENDIF
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Allow the Basic Invite from NPC to be cancelled
//
// INPUT PARAMS:            paramType           The FM Type being checked for
//
// NOTE:    KGM 29/1/15 Added to fix BUG 2217463
PROC Cancel_Any_Basic_Invite_Of_Type(INT paramType)

	#IF IS_DEBUG_BUILD
		PRINTLN(".KGM [JobList]: Cancel_Any_Basic_Invite_Of_Type() called - see callstack")
		DEBUG_PRINTCALLSTACK()
	#ENDIF

    INT tempLoop = 0
    REPEAT g_numMPRVInvites tempLoop
        IF NOT (g_sJLRVInvitesMP[tempLoop].jrviInvitorIsPlayer)
            IF (g_sJLRVInvitesMP[tempLoop].jrviType = paramType)
                // Found an Basic Invite of the correct type
			 	// MMM Invite Decrement
				IF NOT (g_sJLRVInvitesMP[tempLoop].jrviInviteRead) AND g_sJLRVInvitesMP[tempLoop].jrviActive
					PRINTLN(".KGM [JobList] RV Invite Of Type Cancelled. Decrementing Invite Counter. ")
					g_sJLRVInvitesMP[tempLoop].jrviInviteRead = TRUE	// Probably not needed, but just incase	
					Decrement_New_Invites_Indicator()
				ENDIF
				
			    Delete_Joblist_RVInvite_In_Invitation_Slot(tempLoop)
			    
			    #IF IS_DEBUG_BUILD
			        NET_PRINT(".KGM [JobList]: ")
			        NET_PRINT_TIME()
			        NET_PRINT("Basic Invite of Type has been cancelled. [Requested by: ")
			        NET_PRINT(GET_THIS_SCRIPT_NAME())
			        NET_PRINT("]")
			        NET_NL()
			    #ENDIF
           ENDIF
        ENDIF
    ENDREPEAT
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Allow the Basic Invite from NPC to be marked as closed
//
// INPUT PARAMS:            paramInvitorNPC     The charSheetID of the Inviting NPC
//                          paramType           The FM Type being checked for
PROC Mark_Basic_Invite_From_NPC_As_Closed(enumCharacterList paramInvitorNPC, INT paramType)

    INT existingSlot = Get_Joblist_RVInvite_Slot_For_NPC(paramInvitorNPC)
    IF (existingSlot = NO_JOBLIST_ARRAY_ENTRY)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Mark_Basic_Invite_From_NPC_As_Closed() - There is no Basic Invite from this NPC: ")
            TEXT_LABEL  npcNameTL = Get_NPC_Name(paramInvitorNPC)
            NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF
    
    // There is a Basic Invite, so check if it is for the correct activity type
    IF NOT (paramType = g_sJLRVInvitesMP[existingSlot].jrviType)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("Mark_Basic_Invite_From_NPC_As_Closed() - Basic Invite from this NPC is for a different mission type: ")
            TEXT_LABEL  npcNameTL = Get_NPC_Name(paramInvitorNPC)
            NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
            NET_NL()
        #ENDIF
        
        EXIT
    ENDIF

    // Set the 'closed' timeout timer - the mission will be automatically deleted on timeout
    g_sJLRVInvitesMP[existingSlot].jrviRequestClosed = TRUE
    g_sJLRVInvitesMP[existingSlot].jrviClosedTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), BASIC_INVITE_CLOSED_DELAY_msec)
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Basic Invite from NPC has been cancelled. [Requested by: ")
        NET_PRINT(GET_THIS_SCRIPT_NAME())
        NET_PRINT("] - ")
        TEXT_LABEL  npcNameTL = Get_NPC_Name(paramInvitorNPC)
        NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
        NET_NL()
    #ENDIF
    
ENDPROC


PROC Delete_Excluded_Invites_On_Joblist()
	PRINTLN("[MMM][JobList] Delete_Excluded_Invites_On_Joblist()")
	INT tempLoop = 0	
    REPEAT g_numMPJobListInvitations tempLoop
		IF g_sJLInvitesMP[tempLoop].jliInvitorIsPlayer
			PRINTLN("[MMM][JobList] Checking invite in slot : ", tempLoop)
			IF Is_Player_Excluded_From_MP_Mission( PLAYER_ID(), Get_UniqueID_For_This_Players_Mission(g_sJLInvitesMP[tempLoop].jliPlayer))
				PRINTLN("[MMM][JobList] Deleting Joblist Invite")
				Delete_Joblist_Invitation_In_Invitation_Slot(tempLoop)
			ELSE
				PRINTLN("[MMM][JobList] Local Player is NOT excluded")
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC


// ===========================================================================================================
//      Fake Joblist Entries For Tutorial Functions
// ===========================================================================================================

// PURPOSE: Store a fake joblist entry for use by the Tutorial
//
// INPUT PARAMS:            paramSenderChar         The charSheetID of the sender
//                          paramTypeFM             The FM Mission Type
//                          paramMissionName        The Mission Name as a literal string
//
// NOTES:   KGM 4/12/12: This invite is from an in-game character, so everything is unique from a normal joblist invite, so this function will be a one-off
//                       unique function that handles everything about the storage of the invite, and the JobList app will handle the unique display requirements.
PROC Store_Fake_Joblist_Invite_For_Tutorial(enumCharacterList paramSenderChar, INT paramTypeFM, STRING paramMissionName)

    // Currently Freemode Only
    IF (GET_CURRENT_GAMEMODE() != GAMEMODE_FM)
        SCRIPT_ASSERT("Store_Fake_Joblist_Invite_For_Tutorial(): This function is currently Freemode-specific. Tell Keith.")
        EXIT
    ENDIF
    
    // Do some consistency checking on the STRINGS
    IF NOT (IS_STRING_NULL_OR_EMPTY(paramMissionName))
        IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramMissionName) > MAX_JOBLIST_MISSION_NAME_STRING_LENGTH)
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [JobList]: ")
                NET_PRINT_TIME()
                NET_PRINT("ERROR: Store_Fake_Joblist_Invite_For_Tutorial() - Mission Name string is too long: ")
                NET_PRINT(paramMissionName)
                NET_PRINT("  (Expected Max Length: ")
                NET_PRINT_INT(MAX_JOBLIST_MISSION_NAME_STRING_LENGTH)
                NET_PRINT(")")
                NET_NL()
                SCRIPT_ASSERT("Store_Fake_Joblist_Invite_For_Tutorial(): The Mission Name string is longer than expected. Tell Keith.")
            #ENDIF
            
            EXIT
        ENDIF
    ENDIF
    
    // Store the fake Joblist details
    g_fakeJoblistInvite.fjiReceived     = TRUE
    g_fakeJoblistInvite.fjiAccepted     = FALSE
    g_fakeJoblistInvite.fjiSender       = paramSenderChar
    g_fakeJoblistInvite.fjiType         = paramTypeFM
    g_fakeJoblistInvite.fjiMissionName  = paramMissionName
    g_fakeJoblistInvite.fjiFeedID       = NO_JOBLIST_INVITE_FEED_ID
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Storing Fake Invitation Details For Tutorial")
        NET_NL()
        NET_PRINT("      Sender      : ")
        TEXT_LABEL  npcNameTL = Get_NPC_Name(paramSenderChar)
        NET_PRINT(GET_STRING_FROM_TEXT_FILE(npcNameTL))
        NET_NL()
        NET_PRINT("      Type        : ")
        NET_PRINT(GET_STRING_FROM_TEXT_FILE(Get_FM_Joblist_Activity_TextLabel(g_fakeJoblistInvite.fjiType)))
        NET_NL()
        NET_PRINT("      Mission Name: ")
        NET_PRINT(g_fakeJoblistInvite.fjiMissionName)
        NET_NL()
    #ENDIF
    
    // Display this Invite in the feed
    TEXT_LABEL_15   textConfig          = "JL_INVITE_N"
    TEXT_LABEL_63   activityType        = Get_FM_Joblist_Activity_TextLabel(paramTypeFM)
    TEXT_LABEL_63   activityAsString    = GET_FILENAME_FOR_AUDIO_CONVERSATION(activityType)
    
    // Need to get the contents of the text label from the text fil and pass that as the TXD
    TEXT_LABEL_63   theTXD              = GET_FILENAME_FOR_AUDIO_CONVERSATION(GLOBAL_CHARACTER_SHEET_GET_PICTURE(paramSenderChar))
    
    // Wrap the InvitorName up in 'condensed font' markers
    TEXT_LABEL_63   senderChar          = Get_NPC_Name(paramSenderChar)
    TEXT_LABEL_63   invitorName         = "<C>"
    invitorName += GET_FILENAME_FOR_AUDIO_CONVERSATION(senderChar)
    invitorName += "</C>"
    
    // The Feed text Icon to use
    FEED_TEXT_ICON theInviteIcon = TEXT_ICON_INVITE
		
    // ...display the Feed Entry and store the returned Feed ID
    Set_Feed_Flash_Style_For_Invites()
    BEGIN_TEXT_COMMAND_THEFEED_POST(textConfig)
        // Display the mission name
        ADD_TEXT_COMPONENT_SUBSTRING_KEYBOARD_DISPLAY(g_fakeJoblistInvite.fjiMissionName)
    g_fakeJoblistInvite.fjiFeedID = END_TEXT_COMMAND_THEFEED_POST_MESSAGETEXT(theTXD, theTXD, TRUE, theInviteIcon, invitorName, activityAsString)
    Clear_Feed_Flash_Style_For_Invites()
    Store_Feed_Message_As_Most_Recent_For_AutoLaunch(g_fakeJoblistInvite.fjiFeedID)
    Play_Invite_Received_Sound_Effect()
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ") NET_PRINT_TIME() NET_PRINT("Fake Joblist Invite For Tutorial Sent To Feed:") NET_NL()
        NET_PRINT("          Sender Char  : ") NET_PRINT(senderChar) NET_PRINT("  [") NET_PRINT(GET_STRING_FROM_TEXT_FILE(senderChar)) NET_PRINT("]") NET_NL()
        NET_PRINT("          Headshot TXD : ") NET_PRINT(theTXD) NET_NL()
        NET_PRINT("          Type         : ") NET_PRINT(activityAsString) NET_NL()
        NET_PRINT("          Mission Name : ") NET_PRINT(g_fakeJoblistInvite.fjiMissionName) NET_NL()       
    #ENDIF
    
    // Make sure the Joblist App auto-launches when the cellphone is next taken out
    Allow_Joblist_App_Autolaunch()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the player has accepted the fake joblist invite
//
// RETURN VALUE:            BOOL        TRUE if a fake joblist invite has been received and accepted by the player, otherwise FALSE
//
// NOTES:   This function will also deactivate the fake joblist invite, so it's a one-off
FUNC BOOL Has_Player_Accepted_Fake_Joblist_Invite()

    // Ignore if there is no active fake joblist invite
    IF NOT (Has_Fake_Joblist_Invite_Been_Received())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: ")
            NET_PRINT_TIME()
            NET_PRINT("ERROR: Has_Fake_Joblist_Invite_Been_Accepted_By_Player() - Fake Joblist Invite has not been received") NET_NL()
            SCRIPT_ASSERT("Has_Fake_Joblist_Invite_Been_Accepted_By_Player(): A fake joblist invite for tutorial hasn't been received. Tell Keith.")
        #ENDIF
        
        RETURN FALSE
    ENDIF
    
    // There is a fake joblist invite, so check if it has been accepted by the player
    IF NOT (g_fakeJoblistInvite.fjiAccepted)
        RETURN FALSE
    ENDIF
    
    // The fake joblist invite has been accepted and this is about to be returned to the calling script, so cleanup the fake joblist invite
    g_fakeJoblistInvite.fjiReceived = FALSE
    g_fakeJoblistInvite.fjiAccepted = FALSE
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Calling script is being informed the fake joblist invite has been accepted. Cleaning up the fake joblist invite. [Requested by: ")
        NET_PRINT(GET_THIS_SCRIPT_NAME())
        NET_PRINT("]")
        NET_NL()
    #ENDIF
    
    RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Allow the fake joblist invite to be cancelled
// NOTE:    It will also be automatically cancelled when the calling script has been informed that the player accepted it
PROC Cancel_Fake_Joblist_Invite()

    Clear_Fake_Invite_Details()
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [JobList]: ")
        NET_PRINT_TIME()
        NET_PRINT("Fake Joblist Invite has been cancelled. [Requested by: ")
        NET_PRINT(GET_THIS_SCRIPT_NAME())
        NET_PRINT("]")
        NET_NL()
    #ENDIF
    
ENDPROC




// ===========================================================================================================
//      Initialisation Functions
// ===========================================================================================================

// PURPOSE: Initialise any cross-session invites that already exist
PROC Initialise_Existing_Cross_Session_Invites()

    #IF IS_DEBUG_BUILD
        PRINTSTRING(".KGM [JobList]: Initialise_Existing_Cross_Session_Invites()") PRINTNL()
    #ENDIF

    
    INT maxInvites = NETWORK_GET_NUM_PRESENCE_INVITES()
    IF (maxInvites <= 0)
        #IF IS_DEBUG_BUILD
            PRINTSTRING("      - NO CROSS-SESSION INVITES EXIST ON INITIALISATION") PRINTNL()
        #ENDIF
    
        EXIT
    ENDIF
    
    #IF IS_DEBUG_BUILD
        PRINTSTRING("      - NUMBER OF ACTIVE CROSS-SESSION INVITES: ") PRINTINT(maxInvites) PRINTNL()
    #ENDIF
    
    TEXT_LABEL_23   theContentID        = ""
    TEXT_LABEL_63   theInviter          = ""
    INT             theID               = 0
    GAMER_HANDLE    theInvitorGH
    BOOL            gotGH               = FALSE
    INT             totalPlaylist       = 0
    INT             currentPlaylist     = 0
    INT             invitesLoop         = 0
	
	// KGM 12/3/15 [BUG 2273841]: Ensure this can't get stuck in an endless loop if there are Presence Server issue that prevent the deletion of some invites during this setup
	INT safetyStuckLoopBreakoutValue = maxInvites * 2
	// ...(paranoid additional safety check)
	IF (safetyStuckLoopBreakoutValue > 50)
		safetyStuckLoopBreakoutValue = 50
	ENDIF
	INT safetyStuckLoopCounter = 0
        
    WHILE invitesLoop < maxInvites 
        theContentID    = NETWORK_GET_PRESENCE_INVITE_CONTENT_ID(invitesLoop)
        theInviter      = NETWORK_GET_PRESENCE_INVITE_INVITER(invitesLoop)
        theID           = NETWORK_GET_PRESENCE_INVITE_ID(invitesLoop)
        gotGH           = NETWORK_GET_PRESENCE_INVITE_HANDLE(invitesLoop, theInvitorGH)
        totalPlaylist   = NETWORK_GET_PRESENCE_INVITE_PLAYLIST_LENGTH(invitesLoop)
        currentPlaylist = NETWORK_GET_PRESENCE_INVITE_PLAYLIST_CURRENT(invitesLoop)
    
        #IF IS_DEBUG_BUILD
            PRINTSTRING("      ")
            PRINTINT(invitesLoop)
            PRINTSTRING("  CONTENT ID: ")
            PRINTSTRING(theContentID)
            PRINTSTRING("  INVITER: ")
            PRINTSTRING(theInviter)
            PRINTSTRING("  ID: ")
            PRINTINT(theID)
            PRINTSTRING("  TOTAL PLAYLIST: ")
            PRINTINT(totalPlaylist)
            PRINTSTRING("  CURRENT PLAYLIST: ")
            PRINTINT(currentPlaylist)
            IF (gotGH)
                PRINTSTRING(" - Got GamerHandle")
            ELSE
                PRINTSTRING(" - Failed To Get GamerHandle")
            ENDIF
            PRINTNL()
        #ENDIF
        
        IF (gotGH)
            IF NOT (IS_STRING_NULL_OR_EMPTY(theContentID))
                // Store it
                IF NOT Store_Cross_Session_Invite_Details_For_Joblist(theContentID, theInviter, theID, theInvitorGH, totalPlaylist, currentPlaylist)
            		maxInvites = NETWORK_GET_NUM_PRESENCE_INVITES()
				ELSE
					invitesLoop++
				ENDIF
			ELSE
                #IF IS_DEBUG_BUILD
                    PRINTSTRING("      - ContentID is NULL, so not storing existing cross session invite") PRINTNL()
                #ENDIF
            ENDIF
        ELSE
            #IF IS_DEBUG_BUILD
                PRINTSTRING("      - Failed to return GamerHandle, so not storing existing cross session invite") PRINTNL()
            #ENDIF
        ENDIF
		
		// KGM 12/3/15 [BUG 2273841]: Ensure this can't get stuck in an endless loop if there are Presence Server issue that prevent the deletion of some invites during this setup
		safetyStuckLoopCounter++
		IF (safetyStuckLoopCounter > safetyStuckLoopBreakoutValue)
			PRINTLN(".KGM [JobList]: ...GAME SEEMS TO BE STUCK IN A LOOP ATTEMPTING TO DELETE CSINVITES BUT FAILING. USING SAFETY LOOP BREAKOUT TO AVOID ENDLESS LOOP")
			SCRIPT_ASSERT("Initialise_Existing_Cross_Session_Invites() - Safety Bail hit. Tell Keith.")
			EXIT
		ENDIF
    ENDWHILE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: A one-off Joblist Updating initialisation.
//
// INPUT PARAMS:        paramFromSP         [DEFAULT = FALSE] A flag that allows any differences in initialisation between SP and MP
PROC Initialise_MP_Joblist_Update(BOOL paramFromSP = FALSE)

    IF NOT (paramFromSP)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [JobList]: Initialise_MP_Joblist_Update") NET_NL()
        #ENDIF
    ENDIF

    // Clear the variables
    Clear_Joblist_Details()
    Clear_Invite_Details()
    Clear_CSInvite_Details()
    Clear_RVInvite_Details()
    Clear_Fake_Invite_Details()
    Clear_Invite_Acceptance_Details()
    Clear_Joblist_Warp_Details()
	Clear_Same_Session_Invite_Heist_Apartment_Details()
    
    // Clear the update flags and set the initial update timeout
    g_structJoblistUpdate emptyJoblistUpdateStruct
    g_sJoblistUpdate = emptyJoblistUpdateStruct
    
    Set_Joblist_Update_Timeout()
    
    // Clear the New Invites indicator
	Reset_New_Invites_Indicator("[MMM][JobList] Calling Reset_New_Invites_Indicator from Initialise_MP_Joblist_Update")
	
	Dynamically_Update_New_Invites_Indicator()
	
	// Clear the 'downloading header details' access control functions
	g_invitationBeingDownloaded		= FALSE
	g_csInviteBeingDownloaded		= FALSE
	
	// Set the 'first invite' back to 1 (not 0 - we keep 0 for re-adding ContactMission invites)
	g_inviteOrderCounter = 1
    
    // Store Existing Cross-Session Invites
    // The events don't run in the store, etc, so this adds any invites that should already be available
    Initialise_Existing_Cross_Session_Invites()

ENDPROC

// ===========================================================================================================
//      SinglePlayer Cross-session Invites Joblist Functions
// ===========================================================================================================

// PURPOSE: Allows any data to be cleared as the players enter SP
PROC Initialise_SP_Joblist()

    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP: Initialise_SP_Joblist") NET_NL()
    #ENDIF

    // Clear the variables
    BOOL fromSP = TRUE
    Initialise_MP_Joblist_Update(fromSP)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Allow cutdown SP joblist maintenance - only cross-session invites need maintained in SP
PROC Maintain_SP_Joblist()

    // Call the MP Update but with a few MP-Specific things ignored
    BOOL fromSP = TRUE
    Maintain_MP_Joblist_Update(fromSP)

ENDPROC
