//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_ARCADE_PEDS_VARS.sch																				//
// Description: Header file containing variables, structs and enums for the Arcade peds.								//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		05/09/19																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_realty_arcade.sch"
#IF FEATURE_CASINO_HEIST

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ CONSTANTS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

CONST_INT MAX_NUM_ARCADE_PEDS 									17
CONST_INT MAX_NUM_ARCADE_PEDS_PRE_SETUP 						2
CONST_INT MAX_NUM_ARCADE_PEDS_CREATED_PER_FRAME 				2
CONST_INT MAX_NUM_ARCADE_PED_OBJECTS							2
CONST_INT MAX_NUM_ARCADE_JIMMY_SPAWN_LOCATIONS					6
CONST_INT MAX_NUM_ARCADE_HEIST_DRIVER_SPAWN_LOCATIONS			4
CONST_INT MAX_NUM_ARCADE_HEIST_HACKER_SPAWN_LOCATIONS			4
CONST_INT MAX_NUM_ARCADE_HEIST_WEAPON_EXPERT_SPAWN_LOCATIONS	3
CONST_INT MAX_NUM_ARCADE_PED_ANIM_CLIPS_IN_ACTIVITY				4
CONST_INT MAX_NUM_ARCADE_MACHINE_TYPES							14

CONST_INT PED_LEVEL_1_OWNED_ARCADE_MACHINE_TYPES				2
CONST_INT PED_LEVEL_2_OWNED_ARCADE_MACHINE_TYPES				5
CONST_INT PED_LEVEL_3_OWNED_ARCADE_MACHINE_TYPES				7
CONST_INT PED_LEVEL_4_OWNED_ARCADE_MACHINE_TYPES				9
CONST_INT PED_LEVEL_5_OWNED_ARCADE_MACHINE_TYPES				11
CONST_INT PED_LEVEL_6_OWNED_ARCADE_MACHINE_TYPES				13

CONST_INT PED_LEVEL_1_INSTALLED_ARCADE_MACHINES					2
CONST_INT PED_LEVEL_2_INSTALLED_ARCADE_MACHINES					5
CONST_INT PED_LEVEL_3_INSTALLED_ARCADE_MACHINES					9
CONST_INT PED_LEVEL_4_INSTALLED_ARCADE_MACHINES					14
CONST_INT PED_LEVEL_5_INSTALLED_ARCADE_MACHINES					17
CONST_INT PED_LEVEL_6_INSTALLED_ARCADE_MACHINES					20

CONST_INT ARCADE_PATRON_PED_STARTING_INDEX						5
CONST_INT ARCADE_PATRON_PED_FADE_TIME_MS						750

CONST_INT EVENT_SAFETY_TIME_MS									2000
CONST_INT CREW_GREETING_BUFFER_TIME_MS							30000
CONST_INT PED_SPEECH_HACKER_COMPUTER_MINIMUM_TIME_OFFSET_MS		10000
CONST_INT PED_SPEECH_HACKER_COMPUTER_MAXIMUM_TIME_OFFSET_MS		15001
CONST_INT PED_SPEECH_MINIMUM_TIME_OFFSET_MS						30000
CONST_INT PED_SPEECH_MAXIMUM_TIME_OFFSET_MS						40001
CONST_INT PED_SPEECH_POPULARITY_LEVEL							4
CONST_INT PED_SPEECH_LOITER_TIME_MS								60000
CONST_INT PED_SPEECH_BUFFER_TIME_MS								15000

// Different distances for crew 
// members so they don't trigger together
CONST_FLOAT PED_DRIVER_GREET_DISTANCE							2.0
CONST_FLOAT PED_DRIVER_BYE_DISTANCE								5.0

CONST_FLOAT PED_HACKER_GREET_DISTANCE							2.0
CONST_FLOAT PED_HACKER_BYE_DISTANCE								3.5

CONST_FLOAT PED_WEAPON_EXPERT_GREET_DISTANCE					2.0
CONST_FLOAT PED_WEAPON_EXPERT_BYE_DISTANCE						6.0

CONST_FLOAT PED_GREET_DISTANCE									2.0
CONST_FLOAT PED_BYE_DISTANCE									4.0
CONST_FLOAT PED_LISTEN_DISTANCE									7.0
CONST_FLOAT PED_JIMMY_LISTEN_DISTANCE							5.0

// PedData.iBS
CONST_INT BS_ARCADE_PEDS_HIDE_PEDS_FOR_CABINET_MANAGEMENT_MENU	0
CONST_INT BS_ARCADE_PEDS_PRIVATE_MODE_CHANGED					1

// PedData.ArcadePed[].iBS
CONST_INT BS_ARCADE_PED_DATA_SET_FADE_TIMER						0
CONST_INT BS_ARCADE_PED_DATA_FADE_COMPLETE						1
CONST_INT BS_ARCADE_PEDS_SPEECH_EVENT_SENT						2
CONST_INT BS_ARCADE_PEDS_SPEECH_GREETING_TRIGGERED				3
CONST_INT BS_ARCADE_PEDS_SPEECH_GREETING_PLAYED					4
CONST_INT BS_ARCADE_PEDS_SPEECH_BYE_TRIGGERED					5
CONST_INT BS_ARCADE_PEDS_SPEECH_BYE_PLAYED						6
CONST_INT BS_ARCADE_PEDS_SPEECH_BUMP_TRIGGERED					7
CONST_INT BS_ARCADE_PEDS_SPEECH_BUMP_PLAYED						8
CONST_INT BS_ARCADE_PEDS_LOITER_TIMER_SET						9
CONST_INT BS_ARCADE_PEDS_HEIST_DRIVER_LOITER_TIMER_SET			10
CONST_INT BS_ARCADE_PEDS_HEIST_HACKER_LOITER_TIMER_SET			11
CONST_INT BS_ARCADE_PEDS_HEIST_WEAPONS_EXPERT_LOITER_TIMER_SET	12
CONST_INT BS_ARCADE_PEDS_SPEECH_UPGRADE_TRIGGERED				13

// serverBD.iBS
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_ACTIVITY_SET_JIMMY				0
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_POPULARITY_SET_JIMMY				1
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_CABINETS_SET_JIMMY				2
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_CABINET_TYPE_SET_JIMMY			3
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_WHATS_UP_SET_JIMMY				4
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_ACTIVITY_SET_DRIVER				5
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_IDLE_SET_DRIVER					6
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_NO_VEHICLE_SET_DRIVER			7
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_TYPE_ACTIVITY_SET_HACKER				8
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_SPACE_MONKEY_SET			9
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_LAST_GUNSLINGER_SET		10
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_WIZARDS_SLEVE_SET		11
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_NIGHT_DRIVE_CAR_SET		12
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_NIGHT_DRIVE_TRUCK_SET	13
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_NIGHT_DRIVE_BIKE_SET		14
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_DEFENDER_OF_FAITH_SET	15
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_MONKEYS_PARADISE_SET		16
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_PENETRATOR_SET			17
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_INVADE_AND_PERSUADE_SET	18
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_STREET_CRIMES_SET		19
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_FORTUNE_TELLER_SET		20
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_CLAW_CRANE_SET			21
CONST_INT BS_SERVER_ARCADE_PEDS_SPEECH_CONTROLLER_CABINET_TYPE_LOVE_METER_SET			22

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ STRUCTS ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

STRUCT SERVER_BROADCAST_DATA
	INT iBS
	INT iLevel = 0
	INT iLayout = -1
	INT iJimmySpeechTimeOffsetMS = 0
	INT iDriverSpeechTimeOffsetMS = 0
	INT iHackerSpeechTimeOffsetMS = 0
	INT iJimmySpawnLocation = 0
	INT iHeistDriverSpawnLocation = 0
	INT iHeistHackerSpawnLocation = 0
	INT iHeistWeaponExpertSpawnLocation = 0
	INT iOwnedArcadeTypes = 0
	INT iInstalledArcadeMachines = 0
	BOOL bServerDataInitialised = FALSE
	BOOL bTVOn = FALSE
	PED_SPEECH_TYPE eJimmySpeechType = PST_INVALID
	ARCADE_CABINETS eJimmyCabinetSpeech = ARCADE_CABINET_INVALID
	SCRIPT_TIMER stJimmySpeechTimer
	PED_SPEECH_TYPE eDriverSpeechType = PST_INVALID
	SCRIPT_TIMER stDriverSpeechTimer
	PED_SPEECH_TYPE eHackerSpeechType = PST_INVALID
	SCRIPT_TIMER stHackerSpeechTimer
	
	// Each ped only runs one timer at a time.
	// Following timers will run after speech timers and not along side.
	SCRIPT_TIMER stJimmyBufferSpeechTimer
	SCRIPT_TIMER stDriverBufferSpeechTimer
	SCRIPT_TIMER stHackerBufferSpeechTimer
ENDSTRUCT

STRUCT PED_DATA
	PED_INDEX PedID
	OBJECT_INDEX ObjectID[MAX_NUM_ARCADE_PED_OBJECTS]
	TIME_DATATYPE tdPedFadeTimer
	
	INT iBS
	INT iPackedDrawable
	INT iPackedTexture
	INT iActivity
	INT iLevel = 1
	INT iSyncSceneID = -1
	INT iPedModelVariation = 0
	FLOAT fHeading
	VECTOR vPosition
	BOOL bFemale = TRUE
	BOOL bSkipPed = FALSE
	BOOL bBasementPed = FALSE
	
	PED_SPEECH_TYPE eCurrentSpeech = PST_GREETING
	SCRIPT_TIMER stEventSafetyTimer
	
	#IF IS_DEBUG_BUILD
	BOOL bHasBeenEdited
	BOOL bApplyChanges
	#ENDIF
ENDSTRUCT

STRUCT ARCADE_PED_DATA
	PED_DATA ArcadePed[MAX_NUM_ARCADE_PEDS]
	
	INT iBS
	INT iAudioState = 0
	INT iPedsCreatedThisFrame = 0
	INT iLoiterPedID
	
	PLAYER_INDEX arcadeOwner
	BOOL bCabinetsSetupMissionComplete = FALSE
	BOOL bScopeOutSetupMissionComplete = FALSE
	
	OBJECT_INDEX objBroom
	OBJECT_INDEX objHackerChair
	OBJECT_INDEX objWeaponExpertChair
	BOOL bCutscenePlaying = FALSE
	
	CASINO_HEIST_DRIVERS eHeistDriver = CASINO_HEIST_DRIVER__NONE
	CASINO_HEIST_HACKERS eHeistHacker = CASINO_HEIST_HACKER__NONE
	CASINO_HEIST_WEAPON_EXPERTS eHeistWeaponExpert = CASINO_HEIST_WEAPON_EXPERT__NONE
	
	BOOL bAcquiredGetawayVehicles = FALSE
	BOOL bEnteredViaBasement = FALSE
	BOOL bPrivateModeActive = FALSE
	
	SCRIPT_TIMER stLoiterTimer
	SCRIPT_TIMER stLoiterTimerBuffer
	
	SCRIPT_TIMER stDriverGreetingTimer
	SCRIPT_TIMER stHackerGreetingTimer
	SCRIPT_TIMER stWeaponsExpertGreetingTimer
	
	#IF IS_DEBUG_BUILD
	BOOL bOutputToFile
	BOOL bOutputClothingComps
	BOOL bShowDebug
	BOOL bRecreatePeds
	BOOL bDrawSpeechAreas
	INT iLastLayout = -1
	INT iCurrentLayout = -1
	INT iLastLevel = -1
	INT iCurrentLevel = -1
	INT iCount_Level[6]
	
	TEXT_WIDGET_ID twJimmySpawnLocation
	TEXT_WIDGET_ID twHeistDriverSpawnLocation
	TEXT_WIDGET_ID twHeistHackerSpawnLocation
	TEXT_WIDGET_ID twHeistWeaponExpertSpawnLocation
	TEXT_WIDGET_ID twJimmySpeechType
	TEXT_WIDGET_ID twJimmyCabinetSpeechType
	TEXT_WIDGET_ID twDriverSpeechType
	TEXT_WIDGET_ID twHackerSpeechType
	
	INT iGameTimeJimmySpeechSet
	INT iTimeUntilJimmySpeechMS
	INT iJimmyBufferTimeMS
	INT iJimmyBufferTimerSet
	
	INT iGameTimeDriverSpeechSet
	INT iTimeUntilDriverSpeechMS
	INT iDriverBufferTimeMS
	INT iDriverBufferTimerSet
	
	INT iGameTimeHackerSpeechSet
	INT iTimeUntilHackerSpeechMS
	INT iHackerBufferTimeMS
	INT iHackerBufferTimerSet
	
	INT iLoiterTimeSet
	INT iLoiterTimerMS
	INT iLoiterBufferTimeMS
	INT iLoiterBufferTimerSet
	
	INT iDriverGreetingBufferTimeMS
	INT iHackerGreetingBufferTimeMS
	INT iWeaponsExpertGreetingBufferTimeMS
	
	INT iDriverGreetingBufferTimerSet
	INT iHackerGreetingBufferTimerSet
	INT iWeaponsExpertGreetingBufferTimerSet
	#ENDIF
ENDSTRUCT

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ ENUMS ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

ENUM ARCADE_ACTIVITY
	AA_AMBIENT = 0,
	AA_HANGOUT_WITHDRINK_M_01A,
	AA_HANGOUT_WITHDRINK_M_01B,
	AA_HANGOUT_F_3A,
	AA_HANGOUT_F_3B,
	AA_HANGOUT_CONVO_F_2A,
	AA_HANGOUT_WITHDRINK_F_01B,
	AA_JIMMY_PRE_CABINET_SETUP,
	AA_JIMMY_PRE_SCOPE_OUT_SETUP,
	AA_WENDY_PRE_CABINET_SETUP,
	AA_WENDY_PRE_SCOPE_OUT_SETUP,
	AA_JIMMY_SOFA_WATCHING_TV,
	AA_JIMMY_SOFA_DRINKING_WATCHING_TV,
	AA_JIMMY_STANDING_DRINKING,
	AA_JIMMY_STANDING_PHONE_CALL,
	AA_JIMMY_STANDING_TEXTING,
	AA_JIMMY_LYING_DOWN_SMOKING,
	AA_JIMMY_PHONE_LEANING,
	AA_HEIST_DRIVER_INSPECTING,
	AA_HEIST_DRIVER_IDLE,
	AA_HEIST_DRIVER_CLIPBOARD,
	AA_HEIST_HACKER_COMPUTER,
	AA_HEIST_HACKER_COFFEE,
	AA_HEIST_HACKER_TABLET,
	AA_HEIST_WE_SAT_WITH_GUN,
	AA_HEIST_WE_TESTING_GUN,
	AA_HEIST_WE_IDLE_PHONE,
	AA_ELBOWS_ON_BAR_M,
	AA_TOTAL
ENDENUM
#ENDIF //FEATURE_CASINO_HEIST
