USING "rage_builtins.sch"
USING "globals.sch"
USING "net_comms.sch"



// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Comms_Public.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all MP Communications public functions.
//		NOTES			:	This provides a single point of access to all communication systems: cellphone, etc.
//							Should be included in all scripts that want to communicate with the player.
//							End of Mission Messages have their own header.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************





// ===========================================================================================================
//      Checking For Disabled Communications
// ===========================================================================================================

// PURPOSE:	Checks if Communications are enabled or not
//
// RETURN VALUE:		BOOL			TRUE if the phonecalls and radio messages are disabled, otherwise FALSE
// NOTES:	KGM 3/10/11: VERY BASIC, JUST CHECKS IF PHONE DISABLED FOR NOW - NEEDS BETTER INTEGRATION
FUNC BOOL Is_MP_Comms_Disabled()
	RETURN (Are_MP_Comms_Devices_Disabled())
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if Incoming Calls are enabled or not
//
// RETURN VALUE:		BOOL			TRUE if incoming calls are blocked, otherwise FALSE
//
// NOTES:	Will also return TRUE if the MP Comms is disabled
FUNC BOOL Are_MP_Comms_Incoming_Calls_Disabled()

	IF (g_sCommsBlockMP.blockIncomingCalls)
		RETURN TRUE
	ENDIF
	
	// Incoming calls aren't specifically blocked, so check if a general block is in place which would also block incoming calls
	RETURN (Is_MP_Comms_Disabled())
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if Player is on a voice chat
//
// RETURN VALUE:		BOOL			TRUE if the player is on a voice chat, otherwise FALSE
//
// NOTES:	This wraps up a Conor global
FUNC BOOL Is_Player_On_Voice_Chat_Phonecall()
	RETURN (voiceSession.bInVoiceSession)
ENDFUNC





// ===========================================================================================================
//      Requests To Play Specific Types of Communications
// ===========================================================================================================

// PURPOSE:	Request to play End of Mission message
//
// INPUT PARAMS:		paramGroupID				The Subtitle Group ID field from dialogueStar
//						paramRootID					The conversation Root field from dialogueStar
// REFERENCE PARAMS:	paramConversation			The ped dialogue struct containing voice details
// RETURN VALUE:		BOOL						TRUE if the EOM message is now playing, otherwise FALSE
//
// NOTES:	Current default device for all players - Radio.
// 			This needs called each frame until TRUE and so should only be called by the end of mission message header.
//			Missions should call Store_MP_End_Of_Mission_Message() within net_comms_EOM.sch so they can then forget about it.
FUNC BOOL Request_MP_Comms_End_Of_Mission_Message(structPedsForConversation &paramConversation, STRING paramGroupID, STRING paramRootID)

	IF (Are_MP_Comms_Incoming_Calls_Disabled())
		RETURN FALSE
	ENDIF

	// Ignore phonecalls if player on a voice chat phonecall
	IF (Is_Player_On_Voice_Chat_Phonecall())
		RETURN FALSE
	ENDIF

	STRING	ignoreComponentString	= NULL
	BOOL	componentIsPlayerName	= FALSE
	INT		ignoreComponentInt		= NO_INT_SUBSTRING_COMPONENT_VALUE
	INT		commsModifiers			= ALL_MP_COMMS_MODIFIERS_CLEAR
	
	RETURN (Request_Play_MP_Communication(paramConversation, NO_CHARACTER, paramGroupID, paramRootID, MP_COMMS_TYPE_EOM_MESSAGE, MP_COMMS_DEVICE_DEFAULT, commsModifiers, ignoreComponentString, componentIsPlayerName, ignoreComponentInt, ignoreComponentString, ignoreComponentString))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Request to play a phonecall or radio Communication.
//
// INPUT PARAMS:		paramCharSheetID			The CharSheetID for the contact making the phonecall
//						paramGroupID				The Subtitle Group ID field from dialogueStar
//						paramRootID					The conversation Root field from dialogueStar
//						paramModifiers				[DEFAULT = ALL_MP_COMMS_MODIFIERS_CLEAR] Any phonecall modifiers
//						paramDevice 				[DEFAULT = MP_COMMS_DEVICE_DEFAULT]	The Communication Device on which to play the comms
// REFERENCE PARAMS:	paramConversation			The ped dialogue struct containing voice details
// RETURN VALUE:		BOOL						TRUE if the message is now playing, otherwise FALSE
//
// NOTES:	Current default devices: cellphone
FUNC BOOL Request_MP_Comms_Message(structPedsForConversation &paramConversation, enumCharacterList paramCharSheetID, STRING paramGroupID, STRING paramRootID, INT paramModifiers = ALL_MP_COMMS_MODIFIERS_CLEAR, g_eMPCommsDevice paramDevice=MP_COMMS_DEVICE_DEFAULT)

	IF (Are_MP_Comms_Incoming_Calls_Disabled())
		RETURN FALSE
	ENDIF

	// Ignore phonecalls if player on a voice chat phonecall
	IF (Is_Player_On_Voice_Chat_Phonecall())
		RETURN FALSE
	ENDIF

	STRING	ignoreComponentString	= NULL
	BOOL	componentIsPlayerName	= FALSE
	INT		ignoreComponentInt		= NO_INT_SUBSTRING_COMPONENT_VALUE
	
	RETURN (Request_Play_MP_Communication(paramConversation, paramCharSheetID, paramGroupID, paramRootID, MP_COMMS_TYPE_STANDARD_MESSAGE, paramDevice, paramModifiers, ignoreComponentString, componentIsPlayerName, ignoreComponentInt, ignoreComponentString, ignoreComponentString))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Request to play a phonecall or radio Communication that gives the player the option to Join a Mission (or other Activity).
//
// INPUT PARAMS:		paramCharSheetID			The CharSheetID for the contact making the phonecall
//						paramGroupID				The Subtitle Group ID field from dialogueStar
//						paramRootID					The conversation Root field from dialogueStar
//						paramDevice					[DEFAULT = MP_COMMS_DEVICE_DEFAULT]	The Communication Device on which to play the comms
// REFERENCE PARAMS:	paramConversation			The ped dialogue struct containing voice details
// RETURN VALUE:		BOOL						TRUE if the message is now playing, otherwise FALSE
//
// NOTES:	Current default device - Cellphone.
//			This will force the cellphone on-screen without giving the player the option to reject the call (but will give the player the option to decline the activity).
//			The calling script can use the query for YES and query for NO functions below to retrieve the player's reply.
FUNC BOOL Request_MP_Comms_Offer_Mission(structPedsForConversation &paramConversation, enumCharacterList paramCharSheetID, STRING paramGroupID, STRING paramRootID, g_eMPCommsDevice paramDevice=MP_COMMS_DEVICE_DEFAULT)

	IF (Are_MP_Comms_Incoming_Calls_Disabled())
		RETURN FALSE
	ENDIF

	// Ignore phonecalls if player on a voice chat phonecall
	IF (Is_Player_On_Voice_Chat_Phonecall())
		RETURN FALSE
	ENDIF

	STRING	ignoreComponentString	= NULL
	BOOL	componentIsPlayerName	= FALSE
	INT		ignoreComponentInt		= NO_INT_SUBSTRING_COMPONENT_VALUE
	INT		commsModifiers			= ALL_MP_COMMS_MODIFIERS_CLEAR
	
	RETURN (Request_Play_MP_Communication(paramConversation, paramCharSheetID, paramGroupID, paramRootID, MP_COMMS_TYPE_OFFER_MISSION, paramDevice, commsModifiers, ignoreComponentString, componentIsPlayerName, ignoreComponentInt, ignoreComponentString, ignoreComponentString))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Request to play a phonecall or radio Communication where the player is being forced to join a mission (or other Activity).
//
// INPUT PARAMS:		paramCharSheetID			The CharSheetID for the contact making the phonecall
//						paramGroupID				The Subtitle Group ID field from dialogueStar
//						paramRootID					The conversation Root field from dialogueStar
//						paramDevice					[DEFAULT = MP_COMMS_DEVICE_DEFAULT]	The Communication Device on which to play the comms
// REFERENCE PARAMS:	paramConversation			The ped dialogue struct containing voice details
// RETURN VALUE:		BOOL						TRUE if the message is now playing, otherwise FALSE
//
// NOTES:	Current default devices: cellphone
//			This will force the cellphone on-screen without giving the player the option to reject the call.
FUNC BOOL Request_MP_Comms_Force_Onto_Mission(structPedsForConversation &paramConversation, enumCharacterList paramCharSheetID, STRING paramGroupID, STRING paramRootID, g_eMPCommsDevice paramDevice=MP_COMMS_DEVICE_DEFAULT)

	IF (Are_MP_Comms_Incoming_Calls_Disabled())
		RETURN FALSE
	ENDIF

	// Ignore phonecalls if player on a voice chat phonecall
	IF (Is_Player_On_Voice_Chat_Phonecall())
		RETURN FALSE
	ENDIF

	STRING	ignoreComponentString	= NULL
	BOOL	componentIsPlayerName	= FALSE
	INT		ignoreComponentInt		= NO_INT_SUBSTRING_COMPONENT_VALUE
	INT		commsModifiers			= ALL_MP_COMMS_MODIFIERS_CLEAR
	
	RETURN (Request_Play_MP_Communication(paramConversation, paramCharSheetID, paramGroupID, paramRootID, MP_COMMS_TYPE_FORCE_ONTO_MISSION, paramDevice, commsModifiers, ignoreComponentString, componentIsPlayerName, ignoreComponentInt, ignoreComponentString, ignoreComponentString))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Request to play a Barter Text Message (a txtmsg that requires an answer from three possible choices).
//
// INPUT PARAMS:		paramPlayerID				The PlayerID for the player sending the text message
//						paramSubtitleID				The Subtitle text label for the txtmsg text
//						paramCash					[DEFAULT = 0] An additional integer component within the text message that represents the cash value
// RETURN VALUE:		BOOL						TRUE if the text message has been sent, otherwise FALSE
//
// NOTES:	The calling script can use the query for YES and query for NO and query for SPECIAL functions below to retrieve the player's reply.
FUNC BOOL Request_MP_Comms_Barter_Txtmsg(PLAYER_INDEX paramPlayerID, STRING paramSubtitleID, INT paramCash = 0)

	// Ensure the Player Index is valid
	IF NOT (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms]: Request_MP_Comms_Barter_Txtmsg: ADDITIONAL INFO FOR ERROR - Player Index is Illegal.")
			NET_NL()
			NET_PRINT("...........Requested By Script: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			NET_PRINT("...........SubtitleID: ")
			NET_PRINT(paramSubtitleID)
			NET_PRINT(" [")
			NET_PRINT(GET_STRING_FROM_TEXT_FILE(paramSubtitleID))
			NET_PRINT("]")
			NET_NL()
			NET_PRINT("...........Cash Value: ")
			NET_PRINT_INT(paramCash)
			NET_NL()
		#ENDIF
		
		SCRIPT_ASSERT("Request_MP_Comms_Barter_Txtmsg(): Player Index is Illegal. Look at Console Log. Tell Keith.")
		
		RETURN FALSE
	ENDIF
	
	// Store Substring Components
	STRING	ignorecomponentString	= NULL
	BOOL	componentIsPlayerName	= FALSE
	
	// Setup Txtmsg Modifiers for Barter Txtmsgs
	INT commsModifiers = ALL_MP_COMMS_MODIFIERS_CLEAR
	SET_BIT(commsModifiers, MP_COMMS_MODIFIER_COMMS_FORCE_ONSCREEN)
	SET_BIT(commsModifiers, MP_COMMS_MODIFIER_TXTMSG_ALLOW_BARTER)

	structPedsForConversation ignoreConversationStruct
	INT playerIndexAsInt = NATIVE_TO_INT(paramPlayerID)
	RETURN (Request_Play_MP_Communication(ignoreConversationStruct, NO_CHARACTER, Get_MP_Txtmsg_BlockID(), paramSubtitleID, MP_COMMS_TYPE_REPLY_TXTMSG, MP_COMMS_DEVICE_TXTMSG, commsModifiers, ignorecomponentString, componentIsPlayerName, paramCash, ignorecomponentString, ignorecomponentString, playerIndexAsInt))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Request to play a Text Message (including substring components) from a Story Character.
//
// INPUT PARAMS:		paramCharSheetID				The CharSheetID for the character sending the text message
//						paramSubtitleID					The Subtitle text label for the txtmsg text
//						paramComponentTextLabel			The Substring Component: Text Label
//						paramComponentIsPlayerName		[DEFAULT = FALSE] The Text Label component is a player name
//						paramComponentInt				[DEFAULT = NO_INT_SUBSTRING_COMPONENT_VALUE] The Substring Component: Int
//                      paramComponentLiteralString1	[DEFAULT = NULL] The first Literal String Component
//                      paramComponentLiteralString2	[DEFAULT = NULL] The second Literal String Component
//						paramRequireYesNo				[DEFAULT = FALSE] TRUE if a Yes/No reply is required, FALSE if not
//						paramModifiers					[DEFAULT = ALL_MP_COMMS_MODIFIERS_CLEAR] Modifiers to the standard txtmsg behaviour
// RETURN VALUE:		BOOL							TRUE if the text message has been sent, otherwise FALSE
FUNC BOOL Request_MP_Comms_Txtmsg_With_Components(enumCharacterList paramCharSheetID, STRING paramSubtitleID, STRING paramComponentTextLabel, BOOL paramComponentIsPlayerName = FALSE, INT paramComponentInt = NO_INT_SUBSTRING_COMPONENT_VALUE, STRING paramComponentLiteralString1 = NULL, STRING paramComponentLiteralString2 = NULL, BOOL paramRequireYesNo = FALSE, INT paramModifiers = ALL_MP_COMMS_MODIFIERS_CLEAR)

	structPedsForConversation ignoreConversationStruct
	
	// Choose the txtmsg type
	INT txtmsgType = MP_COMMS_TYPE_TXTMSG
	IF (paramRequireYesNo)
		txtmsgType = MP_COMMS_TYPE_REPLY_TXTMSG
	ENDIF
	
	RETURN (Request_Play_MP_Communication(ignoreConversationStruct, paramCharSheetID, Get_MP_Txtmsg_BlockID(), paramSubtitleID, txtmsgType, MP_COMMS_DEVICE_TXTMSG, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Request to play a Text Message from a Story Character.
//
// INPUT PARAMS:		paramCharSheetID			The CharSheetID for the character sending the text message
//						paramSubtitleID				The Subtitle text label for the txtmsg text
//						paramRequireYesNo			[DEFAULT = FALSE] TRUE if a Yes/No reply is required, FALSE if not
//						paramModifiers				[DEFAULT = ALL_MP_COMMS_MODIFIERS_CLEAR] Modifiers to the standard txtmsg behaviour
// RETURN VALUE:		BOOL						TRUE if the text message has been sent, otherwise FALSE
FUNC BOOL Request_MP_Comms_Txtmsg(enumCharacterList paramCharSheetID, STRING paramSubtitleID, BOOL paramRequireYesNo = FALSE, INT paramModifiers = ALL_MP_COMMS_MODIFIERS_CLEAR)

	STRING	ignoreComponentString	= NULL
	BOOL	componentIsPlayerName	= FALSE
	INT		ignoreComponentInt		= NO_INT_SUBSTRING_COMPONENT_VALUE
	
	// Pass to the Component Version for processing
	RETURN (Request_MP_Comms_Txtmsg_With_Components(paramCharSheetID, paramSubtitleID, ignoreComponentString, componentIsPlayerName, ignoreComponentInt, ignoreComponentString, ignoreComponentString, paramRequireYesNo, paramModifiers))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Request to play a Text Message (including substring components) from a Player.
//
// INPUT PARAMS:		paramPlayerID					The PlayerID for the player sending the text message
//						paramSubtitleID					The Subtitle text label for the txtmsg text
//						paramComponentTextLabel			The Substring Component: Text Label
//						paramComponentIsPlayerName		[DEFAULT = FALSE] The Text Label component is a player name
//						paramComponentInt				[DEFAULT = NO_INT_SUBSTRING_COMPONENT_VALUE] The Substring Component: Int
//                      paramComponentLiteralString1	[DEFAULT = NULL] The first Literal String Component
//                      paramComponentLiteralString2	[DEFAULT = NULL] The second Literal String Component
//						paramRequireYesNo				[DEFAULT = FALSE] TRUE if a Yes/No reply is required, FALSE if not
//						paramModifiers					[DEFAULT = ALL_MP_COMMS_MODIFIERS_CLEAR] Modifiers to the standard txtmsg behaviour
// RETURN VALUE:		BOOL							TRUE if the text message has been sent, otherwise FALSE
FUNC BOOL Request_MP_Comms_Txtmsg_With_Components_From_Player(PLAYER_INDEX paramPlayerID, STRING paramSubtitleID, STRING paramComponentTextLabel, BOOL paramComponentIsPlayerName = FALSE, INT paramComponentInt = NO_INT_SUBSTRING_COMPONENT_VALUE, STRING paramComponentLiteralString1 = NULL, STRING paramComponentLiteralString2 = NULL, BOOL paramRequireYesNo = FALSE, INT paramModifiers = ALL_MP_COMMS_MODIFIERS_CLEAR)

	// Ensure the Player Index is valid
	IF NOT (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms]: Request_MP_Comms_Txtmsg_From_Player: ADDITIONAL INFO FOR ERROR - Player Index is Illegal.")
			NET_NL()
			NET_PRINT("...........Requested By Script: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			NET_PRINT("...........SubtitleID: ")
			NET_PRINT(paramSubtitleID)
			NET_PRINT(" [")
			NET_PRINT(GET_STRING_FROM_TEXT_FILE(paramSubtitleID))
			NET_PRINT("]")
			NET_NL()
		#ENDIF
		
		SCRIPT_ASSERT("Request_MP_Comms_Txtmsg_From_Player(): Player Index is Illegal. Look at Console Log. Tell Keith.")
		
		RETURN FALSE
	ENDIF

	structPedsForConversation ignoreConversationStruct
	
	// Choose the txtmsg type
	INT txtmsgType = MP_COMMS_TYPE_TXTMSG
	IF (paramRequireYesNo)
		txtmsgType = MP_COMMS_TYPE_REPLY_TXTMSG
	ENDIF
	
	INT playerIndexAsInt = NATIVE_TO_INT(paramPlayerID)
	RETURN (Request_Play_MP_Communication(ignoreConversationStruct, NO_CHARACTER, Get_MP_Txtmsg_BlockID(), paramSubtitleID, txtmsgType, MP_COMMS_DEVICE_TXTMSG, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2, playerIndexAsInt))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Request to play a Text Message with no replies from a Player.
//
// INPUT PARAMS:		paramPlayerID				The PlayerID for the player sending the text message
//						paramSubtitleID				The Subtitle text label for the txtmsg text
//						paramRequireYesNo			[DEFAULT = FALSE] TRUE if a Yes/No reply is required, FALSE if not
//						paramModifiers				[DEFAULT = ALL_MP_COMMS_MODIFIERS_CLEAR] Modifiers to the standard txtmsg behaviour
// RETURN VALUE:		BOOL						TRUE if the text message has been sent, otherwise FALSE
FUNC BOOL Request_MP_Comms_Txtmsg_From_Player(PLAYER_INDEX paramPlayerID, STRING paramSubtitleID, BOOL paramRequireYesNo = FALSE, INT paramModifiers = ALL_MP_COMMS_MODIFIERS_CLEAR)

	STRING	ignoreComponentString	= NULL
	BOOL	componentIsPlayerName	= FALSE
	INT		ignoreComponentInt		= NO_INT_SUBSTRING_COMPONENT_VALUE

	RETURN (Request_MP_Comms_Txtmsg_With_Components_From_Player(paramPlayerID, paramSubtitleID, ignoreComponentString, componentIsPlayerName, ignoreComponentInt, ignoreComponentString, ignoreComponentString, paramRequireYesNo, paramModifiers))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Request to send an Email (including substring components) from a Story Character.
//
// INPUT PARAMS:		paramCharSheetID				The CharSheetID for the character sending the email
//						paramContentsID					The Contents text label for the email text
//						paramComponentTextLabel			The Substring Component: Text Label
//						paramComponentIsPlayerName		[DEFAULT = FALSE] The Text Label component is a player name
//						paramComponentInt				[DEFAULT = NO_INT_SUBSTRING_COMPONENT_VALUE] The Substring Component: Int
//                      paramComponentLiteralString1	[DEFAULT = NULL] The first Literal String Component
//                      paramComponentLiteralString2	[DEFAULT = NULL] The second Literal String Component
//						paramRequireYesNo				[DEFAULT = FALSE] TRUE if a Yes/No reply is required, FALSE if not
//						paramModifiers					[DEFAULT = ALL_MP_COMMS_MODIFIERS_CLEAR] Modifiers to the standard email behaviour
// RETURN VALUE:		BOOL							TRUE if the email has been sent, otherwise FALSE
FUNC BOOL Request_MP_Comms_Email_With_Components(enumCharacterList paramCharSheetID, STRING paramContentsID, STRING paramComponentTextLabel, BOOL paramComponentIsPlayerName = FALSE, INT paramComponentInt = NO_INT_SUBSTRING_COMPONENT_VALUE, STRING paramComponentLiteralString1 = NULL, STRING paramComponentLiteralString2 = NULL, BOOL paramRequireYesNo = FALSE, INT paramModifiers = ALL_MP_COMMS_MODIFIERS_CLEAR)

	structPedsForConversation ignoreConversationStruct
	
	// Choose the email type
	INT emailType = MP_COMMS_TYPE_EMAIL
// KGM 14/1/14: Ignore YesNo for now until replies are implemented
UNUSED_PARAMETER(paramRequireYesNo)
//	IF (paramRequireYesNo)
//		emailType = MP_COMMS_TYPE_REPLY_TXTMSG
//	ENDIF
	
	RETURN (Request_Play_MP_Communication(ignoreConversationStruct, paramCharSheetID, Get_MP_Email_BlockID(), paramContentsID, emailType, MP_COMMS_DEVICE_EMAIL, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Request to play an email from a Story Character.
//
// INPUT PARAMS:		paramCharSheetID			The CharSheetID for the character sending the email
//						paramContentsID				The Contents text label for the email text
//						paramRequireYesNo			[DEFAULT = FALSE] TRUE if a Yes/No reply is required, FALSE if not
//						paramModifiers				[DEFAULT = ALL_MP_COMMS_MODIFIERS_CLEAR] Modifiers to the standard email behaviour
// RETURN VALUE:		BOOL						TRUE if the email has been sent, otherwise FALSE
FUNC BOOL Request_MP_Comms_Email(enumCharacterList paramCharSheetID, STRING paramContentsID, BOOL paramRequireYesNo = FALSE, INT paramModifiers = ALL_MP_COMMS_MODIFIERS_CLEAR)

	STRING	ignoreComponentString	= NULL
	BOOL	componentIsPlayerName	= FALSE
	INT		ignoreComponentInt		= NO_INT_SUBSTRING_COMPONENT_VALUE
	
	// Pass to the Component Version for processing
	RETURN (Request_MP_Comms_Email_With_Components(paramCharSheetID, paramContentsID, ignoreComponentString, componentIsPlayerName, ignoreComponentInt, ignoreComponentString, ignoreComponentString, paramRequireYesNo, paramModifiers))
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Request to play an Email (including substring components) from a Player.
//
// INPUT PARAMS:		paramPlayerID					The PlayerID for the player sending the email
//						paramContentsID					The Subtitle text label for the email text
//						paramComponentTextLabel			The Substring Component: Text Label
//						paramComponentIsPlayerName		[DEFAULT = FALSE] The Text Label component is a player name
//						paramComponentInt				[DEFAULT = NO_INT_SUBSTRING_COMPONENT_VALUE] The Substring Component: Int
//                      paramComponentLiteralString1	[DEFAULT = NULL] The first Literal String Component
//                      paramComponentLiteralString2	[DEFAULT = NULL] The second Literal String Component
//						paramRequireYesNo				[DEFAULT = FALSE] TRUE if a Yes/No reply is required, FALSE if not
//						paramModifiers					[DEFAULT = ALL_MP_COMMS_MODIFIERS_CLEAR] Modifiers to the standard Email behaviour
// RETURN VALUE:		BOOL							TRUE if the email has been sent, otherwise FALSE
FUNC BOOL Request_MP_Comms_Email_With_Components_From_Player(PLAYER_INDEX paramPlayerID, STRING paramContentsID, STRING paramComponentTextLabel, BOOL paramComponentIsPlayerName = FALSE, INT paramComponentInt = NO_INT_SUBSTRING_COMPONENT_VALUE, STRING paramComponentLiteralString1 = NULL, STRING paramComponentLiteralString2 = NULL, BOOL paramRequireYesNo = FALSE, INT paramModifiers = ALL_MP_COMMS_MODIFIERS_CLEAR)
	
	SCRIPT_ASSERT("Request_MP_Comms_Email_With_Components_From_Player(): Email sending from player is not supported. Use the eyeFind System instead")
	
	paramPlayerID = paramPlayerID
	paramContentsID = paramContentsID
	paramComponentTextLabel = paramComponentTextLabel
	paramComponentIsPlayerName = paramComponentIsPlayerName
	paramComponentInt = paramComponentInt
	paramComponentLiteralString1 = paramComponentLiteralString1
	paramComponentLiteralString2 = paramComponentLiteralString2
	paramRequireYesNo = paramRequireYesNo
	paramModifiers = paramModifiers
	
	RETURN FALSE
	/*
	// Ensure the Player Index is valid
	IF NOT (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms]: Request_MP_Comms_Email_With_Components_From_Player: ADDITIONAL INFO FOR ERROR - Player Index is Illegal.")
			NET_NL()
			NET_PRINT("...........Requested By Script: ")
			NET_PRINT(GET_THIS_SCRIPT_NAME())
			NET_NL()
			NET_PRINT("...........ContentsID: ")
			NET_PRINT(paramContentsID)
			NET_PRINT(" [")
			NET_PRINT(GET_STRING_FROM_TEXT_FILE(paramContentsID))
			NET_PRINT("]")
			NET_NL()
		#ENDIF
		
		SCRIPT_ASSERT("Request_MP_Comms_Email_With_Components_From_Player(): Player Index is Illegal. Look at Console Log. Tell Keith.")
		
		RETURN FALSE
	ENDIF

	structPedsForConversation ignoreConversationStruct
	
	// Choose the email type
	INT emailType = MP_COMMS_TYPE_EMAIL
// KGM 14/1/14: Ignore YesNo for now until replies are implemented
//UNUSED_PARAMETER(paramRequireYesNo)
	IF (paramRequireYesNo)
		emailType = MP_COMMS_TYPE_REPLY_EMAIL// MP_COMMS_TYPE_REPLY_TXTMSG	// TODO: Investigate how to text messages group all their text
	ENDIF
	
	INT playerIndexAsInt = NATIVE_TO_INT(paramPlayerID)
	RETURN (Request_Play_MP_Communication(ignoreConversationStruct, NO_CHARACTER, Get_MP_Email_BlockID(), paramContentsID, emailType, MP_COMMS_DEVICE_EMAIL, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2, playerIndexAsInt))
	*/
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Request to play an Email with no replies from a Player.
//
// INPUT PARAMS:		paramPlayerID				The PlayerID for the player sending the email
//						paramContentsID				The Subtitle text label for the email text
//						paramRequireYesNo			[DEFAULT = FALSE] TRUE if a Yes/No reply is required, FALSE if not
//						paramModifiers				[DEFAULT = ALL_MP_COMMS_MODIFIERS_CLEAR] Modifiers to the standard email behaviour
// RETURN VALUE:		BOOL						TRUE if the Email has been sent, otherwise FALSE
FUNC BOOL Request_MP_Comms_Email_From_Player(PLAYER_INDEX paramPlayerID, STRING paramContentsID, BOOL paramRequireYesNo = FALSE, INT paramModifiers = ALL_MP_COMMS_MODIFIERS_CLEAR)
	
	SCRIPT_ASSERT("Request_MP_Comms_Email_From_Player(): Email sending from player is not supported. Use the eyeFind System instead")
	
	paramPlayerID = paramPlayerID
	paramContentsID = paramContentsID	
	paramRequireYesNo = paramRequireYesNo
	paramModifiers = paramModifiers
	
	RETURN FALSE
	/*
	STRING	ignoreComponentString	= NULL
	BOOL	componentIsPlayerName	= FALSE
	INT		ignoreComponentInt		= NO_INT_SUBSTRING_COMPONENT_VALUE

	RETURN (Request_MP_Comms_Email_With_Components_From_Player(paramPlayerID, paramContentsID, ignoreComponentString, componentIsPlayerName, ignoreComponentInt, ignoreComponentString, ignoreComponentString, paramRequireYesNo, paramModifiers))
	*/
ENDFUNC





// ===========================================================================================================
//      Active Communication Access Functions
// ===========================================================================================================

// PURPOSE:	Checks if any MP Communication Device is on-screen.
//
// RETURN VALUE:		BOOL			TRUE if any MP communication device is on-screen, otherwise FALSE
//
// NOTES:	Use this to check if the Cellphone (and any future comms devices) are actually on-screen
//			(This will only return TRUE if the cellphone is actually on-screen)
FUNC BOOL Is_Any_MP_Comms_Device_Onscreen()

	IF (IS_PHONE_ONSCREEN())
		RETURN TRUE
	ENDIF
	
	// No MP Comms device is currently on-screen (although a device may be actively communicating).
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if any MP Communication Device is actively playing a communication.
//
// RETURN VALUE:		BOOL			TRUE if any MP communication is actively playing a communication, otherwise FALSE
//
// NOTES:	Use this to check if a Comms Device has an active message - whether or not the comms device is on-screen
//			(This will return TRUE if a police radio message or a YES/NO text message that hasn't been forced on-screen is currently active)
FUNC BOOL Is_Any_MP_Comms_Device_Actively_Communicating()
	RETURN (Is_There_An_Active_MP_Communication_Request())
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if there is an active MP phonecall to the player.
//
// RETURN VALUE:		BOOL			TRUE if there is an active phonecall to the player, otherwise FALSE
FUNC BOOL Is_There_An_Active_MP_Comms_Phonecall_To_The_Player()

	IF NOT (Is_There_An_Active_MP_Communication_Request())
		RETURN FALSE
	ENDIF
	
	RETURN (Is_MP_Communication_A_Phonecall())
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the MP Communication is still Playing
//
// RETURN VALUE:		BOOL			TRUE if there is an active MP communication, otherwise FALSE
//
// NOTES:	Once a Communication Request has been told it has been activated, this function can be used to check for it finishing.
FUNC BOOL Is_MP_Comms_Still_Playing()

	// KGM 10/7/11: It may be important to also wait until the cellphone has moved off-screen before returning TRUE, but since a new communication
	//				can't be started until the cellphone is off-screen I won't bother with that check here for now - it may be too strict and could
	//				potentially lead to this function never returning TRUE if the cellphone got manually brought up again immediately with no delay.

	RETURN (Is_There_An_Active_MP_Communication_Request())
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if there is an active MP comms or an outstanding request for an MP Comms
//
// RETURN VALUE:		BOOL			TRUE if there is an active or Pending MP communication, otherwise FALSE
//
// NOTES:	A Pending communication is one that has been requested this frame or last frame.
FUNC BOOL Is_Any_MP_Comms_Active_Or_Pending()
	RETURN (Is_There_An_Existing_MP_Communication_Request())
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the MP Communication completed successfully
//
// RETURN VALUE:		BOOL			TRUE if the message completed successfully, otherwise FALSE if the player probably didn't get a chance to read it for whatever reason (forced away, etc)
//
// NOTES:	Once a Communication Request has finished, important calls can check in case it may need to be re-sent.
FUNC BOOL Did_MP_Comms_Complete_Successfully()
	RETURN (g_sCommsMP.commsEndStatus.cesEndedSuccessfully)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the player replied YES to current MP Communication.
//
// RETURN VALUE:		BOOL						TRUE if the player replied YES, otherwise FALSE
//
// NOTES:	Call this after the communication has ended (Is_MP_Comms_Still_Playing)
FUNC BOOL Did_Player_Reply_Yes_To_MP_Comms()

	// The calling routine shouldn't poll for this until it has been told the communication has finished
	IF (Is_There_An_Active_MP_Communication_Request())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Did_Player_Reply_Yes_To_MP_Comms() - MP Communication still active - poll AFTER finished. Query came from this script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		#ENDIF
	
		SCRIPT_ASSERT("Did_Player_Reply_Yes_To_MP_Comms() - There is still an active request - see console log. Tell Keith. (NOTE: This assert may be too strict)")
		
		RETURN FALSE
	ENDIF
	
	// Has this query come from the script that requested the original MP communication?
	IF NOT (Is_Yes_No_Reply_Query_Being_Made_By_The_Correct_Script())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Did_Player_Reply_Yes_To_MP_Comms() - The wrong script queried for the Mp Communication Reply.") NET_NL()
			NET_PRINT("..........Query is being made from this script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
			NET_PRINT("..........Original MP Communication Request came from this script: ") NET_PRINT("<SCRIPT NAME>") NET_NL()
		#ENDIF
	
		SCRIPT_ASSERT("Did_Player_Reply_Yes_To_MP_Comms() - The wrong script is querying for reply - see console log. Tell Keith.")
		
		RETURN FALSE
	ENDIF
	
	BOOL playerSaidYes = Has_MP_Communication_Received_A_Yes_Reply()
	
	#IF IS_DEBUG_BUILD
		IF (playerSaidYes)
			NET_PRINT("...KGM MP [MPComms]: Did_Player_Reply_Yes_To_MP_Comms(): script informed that player said 'YES' - ") PRINTSTRING(GET_THIS_SCRIPT_NAME()) NET_NL()
		ENDIF
	#ENDIF
	
	RETURN (playerSaidYes)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the player replied NO to current MP Communication.
//
// RETURN VALUE:		BOOL						TRUE if the player replied NO, otherwise FALSE
//
// NOTES:	Call this after the communication has ended (Is_MP_Comms_Still_Playing)
FUNC BOOL Did_Player_Reply_No_To_MP_Comms()

	// The calling routine shouldn't poll for this until it has been told the communication has finished
	IF (Is_There_An_Active_MP_Communication_Request())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Did_Player_Reply_No_To_MP_Comms() - MP Communication still active - poll AFTER finished. Query came from this script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		#ENDIF
	
		SCRIPT_ASSERT("Did_Player_Reply_No_To_MP_Comms() - There is still an active request - see console log. Tell Keith. (NOTE: This assert may be too strict)")
		
		RETURN FALSE
	ENDIF
	
	// Has this query come from the script that requested the original MP communication?
	IF NOT (Is_Yes_No_Reply_Query_Being_Made_By_The_Correct_Script())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Did_Player_Reply_No_To_MP_Comms() - The wrong script queried for the Mp Communication Reply.") NET_NL()
			NET_PRINT("..........Query is being made from this script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
			NET_PRINT("..........Original MP Communication Request came from this script: ") NET_PRINT("<SCRIPT NAME>") NET_NL()
		#ENDIF
	
		SCRIPT_ASSERT("Did_Player_Reply_No_To_MP_Comms() - The wrong script is querying for reply - see console log. Tell Keith.")
		
		RETURN FALSE
	ENDIF
	
	BOOL playerSaidNo = Has_MP_Communication_Received_A_No_Reply()
	
	#IF IS_DEBUG_BUILD
		IF (playerSaidNo)
			NET_PRINT("...KGM MP [MPComms]: Did_Player_Reply_No_To_MP_Comms(): script informed that player said 'NO' - ") PRINTSTRING(GET_THIS_SCRIPT_NAME()) NET_NL()
		ENDIF
	#ENDIF
	
	RETURN (playerSaidNo)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the player replied SPECIAL to current MP Communication.
//
// RETURN VALUE:		BOOL						TRUE if the player replied SPECIAL, otherwise FALSE
//
// NOTES:	Call this after the communication has ended (Is_MP_Comms_Still_Playing)
//			'SPECIAL' is for special replies like 'BARTER' as a txtmsg reply
FUNC BOOL Did_Player_Reply_With_A_Special_Reply_To_MP_Comms()

	// The calling routine shouldn't poll for this until it has been told the communication has finished
	IF (Is_There_An_Active_MP_Communication_Request())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Did_Player_Reply_With_A_Special_Reply_To_MP_Comms() - MP Communication still active - poll AFTER finished. Query came from this script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		#ENDIF
	
		SCRIPT_ASSERT("Did_Player_Reply_With_A_Special_Reply_To_MP_Comms() - There is still an active request - see console log. Tell Keith. (NOTE: This assert may be too strict)")
		
		RETURN FALSE
	ENDIF
	
	// Has this query come from the script that requested the original MP communication?
	IF NOT (Is_Yes_No_Reply_Query_Being_Made_By_The_Correct_Script())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Did_Player_Reply_With_A_Special_Reply_To_MP_Comms() - The wrong script queried for the Mp Communication Reply.") NET_NL()
			NET_PRINT("..........Query is being made from this script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
			NET_PRINT("..........Original MP Communication Request came from this script: ") NET_PRINT("<SCRIPT NAME>") NET_NL()
		#ENDIF
	
		SCRIPT_ASSERT("Did_Player_Reply_With_A_Special_Reply_To_MP_Comms() - The wrong script is querying for reply - see console log. Tell Keith.")
		
		RETURN FALSE
	ENDIF
	
	BOOL playerSaidSpecial = Has_MP_Communication_Received_A_Special_Reply()
	
	#IF IS_DEBUG_BUILD
		IF (playerSaidSpecial)
			NET_PRINT("...KGM MP [MPComms]: Did_Player_Reply_With_A_Special_Reply_To_MP_Comms(): script informed that player said 'SPECIAL' - ") PRINTSTRING(GET_THIS_SCRIPT_NAME()) NET_NL()
		ENDIF
	#ENDIF
	
	RETURN (playerSaidSpecial)

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Cancel Communication Functions
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Kill a specific Communication
//
// INPUT PARAMS:		paramGroupID				The Subtitle Group ID field from dialogueStar
//						paramRootID					The conversation Root field from dialogueStar
PROC Kill_This_MP_Comms(STRING paramGroupID, STRING paramRootID)
	Cancel_MP_Communication(GET_THIS_SCRIPT_NAME(), paramGroupID, paramRootID)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Kill a specific Txtmsg Communication
//
// INPUT PARAMS:		paramSubtitleID					The Text Label containing the text message text
PROC Kill_This_MP_Txtmsg_Comms(STRING paramSubtitleID)
	Cancel_MP_Communication(GET_THIS_SCRIPT_NAME(), Get_MP_Txtmsg_BlockID(), paramSubtitleID)
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Kill any Communication from this script
PROC Kill_Any_MP_Comms_From_This_Script()
	Cancel_MP_Communication(GET_THIS_SCRIPT_NAME(), "", "")
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Kill any Communication
PROC Kill_Any_MP_Comms()
	Cancel_MP_Communication("", "", "")
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Deletes a text message on the cellphone text messages page
PROC Delete_Text_Messages_With_This_Label(STRING paramSubtitleID)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_PRINT("...KGM MP [MPComms]: Delete_Text_Messages_With_This_Label: ")
		NET_PRINT(paramSubtitleID)
		NET_PRINT(" [Requested By: ")
		NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT("]")
		NET_NL()
	#ENDIF

	Delete_Txtmsgs_On_Text_Message_List(paramSubtitleID)
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Deletes an email on the cellphone emails page
PROC Delete_Emails_With_This_Label(STRING paramContentID)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_PRINT("...KGM MP [MPComms]: Delete_Emails_With_This_Label: ")
		NET_PRINT(paramContentID)
		NET_PRINT(" [Requested By: ")
		NET_PRINT(GET_THIS_SCRIPT_NAME())
		NET_PRINT("]")
		NET_NL()
	#ENDIF

	Delete_Email_On_Email_List(paramContentID)
	
ENDPROC





// ===========================================================================================================
//      General Communication Public Access Functions
// ===========================================================================================================

// PURPOSE:	Communications are disabled - prevents phonecalls and radio messages
// NOTES:	KGM 3/10/11: VERY BASIC, JUST DISABLES PHONE FOR NOW - NEEDS BETTER INTEGRATION
PROC Disable_MP_Comms()
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_PRINT("...KGM MP [MPComms]: MP Comms Disabled By: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
	#ENDIF

	HANG_UP_AND_PUT_AWAY_PHONE()
	DISABLE_CELLPHONE(TRUE)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Communications are enabled - allows phonecalls and radio messages
// NOTES:	KGM 3/10/11: VERY BASIC, JUST DISABLES PHONE FOR NOW - NEEDS BETTER INTEGRATION
PROC Enable_MP_Comms()
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_PRINT("...KGM MP [MPComms]: MP Comms Enabled By: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
	#ENDIF

	DISABLE_CELLPHONE(FALSE)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets up a temporary cellphone block that lasts a few seconds
// NOTES:	This can be used to cover script-loading times if necssary to avoid sneaky phonecalls nipping during the transition into the script
PROC Enable_Temporary_MP_Comms_Block()
	
	#IF IS_DEBUG_BUILD
		IF NOT (g_sCommsBlockMP.blockActive)
			DEBUG_PRINTCALLSTACK()
			NET_PRINT("...KGM MP [MPComms]: Temporary MP Comms Block Enabled By: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		ENDIF
	#ENDIF

	// Disable the cellphone this frame
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	// Set up the maintenance function to ensure it gets disabled every frame for the required period of time
	g_sCommsBlockMP.blockActive		= TRUE
	g_sCommsBlockMP.blockTimeout	= GET_TIME_OFFSET(GET_NETWORK_TIME(), MP_COMMS_TEMPORARY_BLOCK_TIMEOUT_msec)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Sets up a temporary cellphone block that lasts a few seconds
// NOTES:	This can be used to cover script-loading times if necssary to avoid sneaky phonecalls nipping during the transition into the script
PROC Enable_Temporary_MP_Comms_Incoming_Calls_Block()
	
	#IF IS_DEBUG_BUILD
		IF NOT (g_sCommsBlockMP.blockIncomingCalls)
			DEBUG_PRINTCALLSTACK()
			NET_PRINT("...KGM MP [MPComms]: ")
			NET_PRINT_TIME()
			NET_PRINT(" Temporary MP Comms Incoming Calls Block Enabled By: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
		ENDIF
	#ENDIF
	
	// Set up the maintenance function to ensure it gets disabled every frame for the required period of time
	g_sCommsBlockMP.blockIncomingCalls			= TRUE
	g_sCommsBlockMP.blockIncomingCallsTimeout	= GET_TIME_OFFSET(GET_NETWORK_TIME(), MP_COMMS_INCOMING_CALLS_BLOCK_TIMEOUT_msec)

ENDPROC




