USING "ped_component_data.sch"

PROC CREATE_CLONE_OF_PED_AT_COORDS(PED_INDEX &ped, PED_INDEX pedToClone, VECTOR vPos, FLOAT fHeading, INT iPlayerID, BOOL bWearingProblemMaskOrHelmet)
	IF NOT DOES_ENTITY_EXIST(ped)
		
		#IF IS_DEBUG_BUILD
		
			PRINTLN("[NETCELEBRATION] - CREATE_CLONE_OF_PED_AT_COORDS - iPlayerID = ", iPlayerID)
			
			IF iPlayerID > -1
			AND iPlayerID < NUM_NETWORK_PLAYERS
				STRING tlName = GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iPlayerID))
				PRINTLN("[NETCELEBRATION] - CREATE_CLONE_OF_PED_AT_COORDS - player name = ", tlName)
				PRINTLN("[NETCELEBRATION] - CREATE_CLONE_OF_PED_AT_COORDS - iMyCurrentHair = ", GlobalplayerBD[iPlayerID].iMyCurrentHair)
			ENDIF
			
		#ENDIF
		
		//This is just to avoid dead ped asserts, need a better way to handle this.
		IF NOT IS_PED_INJURED(pedToClone)
			
		ENDIF
		
		ped = CLONE_PED(pedToClone, FALSE, FALSE, FALSE)
		IF IS_PED_INJURED(ped)
			REVIVE_INJURED_PED(ped)
		ENDIF
		
		CLEAR_PED_BLOOD_DAMAGE(ped)
		SET_ENTITY_COORDS(ped, vPos)
		SET_ENTITY_HEADING(ped, fHeading)
		SET_ENTITY_COLLISION(ped, FALSE)
		SET_ENTITY_INVINCIBLE(ped, TRUE)
		FREEZE_ENTITY_POSITION(ped, TRUE)	
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, TRUE)
		SET_CURRENT_PED_WEAPON(ped, WEAPONTYPE_UNARMED, TRUE)
		SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAND, 0, 0)
		
		IF bWearingProblemMaskOrHelmet
			
			SET_PED_HELMET(ped, FALSE)
			REMOVE_PED_HELMET(ped, TRUE)
			SET_PED_COMPONENT_VARIATION(ped, PED_COMP_BERD, 0, 0)
			CLEAR_PED_PROP(ped, ANCHOR_HEAD)
			
			// Grab stored hair
			IF iPlayerID > -1
			AND iPlayerID < NUM_NETWORK_PLAYERS
				PED_COMP_ITEM_DATA_STRUCT sTempCompData = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(ped), COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, GlobalplayerBD[iPlayerID].iMyCurrentHair))
				SET_PED_COMPONENT_VARIATION(ped, PED_COMP_HAIR, sTempCompData.iDrawable, sTempCompData.iTexture)
				SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TEETH, 0, 0)
				PRINTLN("[NETCELEBRATION] - calling GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(ped), COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, GlobalplayerBD[iPlayerID].iplayershair)). iMyCurrentHair = ", GlobalplayerBD[iPlayerID].iMyCurrentHair)
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[NETCELEBRATION] - not doing hair, iPlayerID = ", iPlayerID, ", which is not a valid Id.")
			#ENDIF
			ENDIF
			
		ENDIF
		
		IF IS_PED_WEARING_PILOT_SUIT(ped)
			SET_PED_COMPONENT_VARIATION(ped, PED_COMP_TEETH, 0, 0)
		ENDIF
		
		CLEAR_PED_TASKS_IMMEDIATELY(ped)
		FINALIZE_HEAD_BLEND(ped)
	ENDIF
ENDPROC


//EOF

