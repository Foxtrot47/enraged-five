USING "rage_builtins.sch"
USING "globals.sch"
USING "net_mission.sch" 
USING "shared_hud_displays.sch"
USING "Net_Entity_Controller.sch"
USING "net_transition_sessions.sch"
USING "net_vote_at_blip_public.sch"		// KGM 15/9/12: Extracted some functionality from a flow header into a generic header

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		HEADER NAME		:	net_vote_at_blip.sch
//		CREATED			:	BOBBY WRIGHT
//		DESCRIPTION		:	FOR MP VOTING AT A BLIP AND THat
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


//Max number of votes
CONST_INT ciMP_VOTE_TIME_DELAY 			200

//Vote bit set CONSTINTS
CONST_INT ciVOTE_BIT_SET_0				0
CONST_INT ciVOTE_BIT_SET_1				1
CONST_INT ciVOTE_BIT_SET_2				2
CONST_INT ciVOTE_BIT_SET_3				3
CONST_INT ciVOTE_BIT_SET_4				4
CONST_INT ciVOTE_BIT_SET_5				5
CONST_INT ciVOTE_BIT_SET_6				6
CONST_INT ciVOTE_BIT_SET_7				7
CONST_INT ciVOTE_BIT_SET_8				8
CONST_INT ciVOTE_BIT_SET_9				9
CONST_INT ciVOTE_BIT_SET_10				10
CONST_INT ciVOTE_BIT_SET_11				11
CONST_INT ciVOTE_BIT_SET_12				12
CONST_INT ciVOTE_BIT_SET_13				13
CONST_INT ciVOTE_BIT_SET_14				14

//VotePassed constints
CONST_INT ciVOTEPASSED_BIT_SET_0		0
CONST_INT ciVOTEPASSED_BIT_SET_1		1
CONST_INT ciVOTEPASSED_BIT_SET_2		2
CONST_INT ciVOTEPASSED_BIT_SET_3		3
CONST_INT ciVOTEPASSED_BIT_SET_4		4
CONST_INT ciVOTEPASSED_BIT_SET_5		5
CONST_INT ciVOTEPASSED_BIT_SET_6		6
CONST_INT ciVOTEPASSED_BIT_SET_7		7
CONST_INT ciVOTEPASSED_BIT_SET_8		8
CONST_INT ciVOTEPASSED_BIT_SET_9		9
CONST_INT ciVOTEPASSED_BIT_SET_10		10
CONST_INT ciVOTEPASSED_BIT_SET_11		11
CONST_INT ciVOTEPASSED_BIT_SET_12		12
CONST_INT ciVOTEPASSED_BIT_SET_13		13
CONST_INT ciVOTEPASSED_BIT_SET_14		14

//Shout the vote be re-checked
//CONST_INT biReCheckVote					30



//CONST_INT tiCORONA_TRIGGER_TIMER					90000		// KGM 13/12/12: Moved this to MP_Globals_FM.sch so I can get access to it in net_mission_joblist.sch
CONST_INT tiCORONA_INVITE_INCREASE_TIMER				30000
CONST_INT tiCORONA_TIMER_SET_AFTER_VOTE_PASSED_BETTING	20000
CONST_INT tiCORONA_TIMER_SET_AFTER_VOTE_PASSED			5000
CONST_INT tiCORONA_TIMER_SET_AFTER_VOTE_EXTENDED		15000
CONST_INT ciRACE_MENU_VEHICLE_STATS_HELP 0

CONST_INT ciMENU_SET_UP 						0
CONST_INT ciAM_I_ALLOWED_TO_VOTE			 	1
CONST_INT ciAM_I_ALLOWED_TO_BET					2
CONST_INT ciI_USED_TO_BE_A_CLIENT_ON_THIS_MENU	3
CONST_INT ciRESET_UP_MENU	4
CONST_INT ciMAX_RACE_HUD 8


STRUCT MainPlacementData

	FLOAT x
	FLOAT y
	
	TEXT_PLACEMENT 		TextPlacement[7]
	SPRITE_PLACEMENT 	SpritePlacement[17]
	TEXT_STYLE   		textStyle[ciMAX_RACE_HUD]
	
ENDSTRUCT


/// PURPOSE:resets the PLAYER vote variables 
///    Gets called just before the variables are diclatred as broadcast data
/// PARAMS:
///    mpServerVoteStruct - the server vote struct to reset.
PROC RESET_HAIR_BD()
	INT iCount
	FOR iCount = 0 TO (NUM_NETWORK_PLAYERS - 1)
			GlobalplayerBD[iCount].iMyCurrentHair = -1
	ENDFOR
	PRINTSTRING("...RESET_HAIR_BD MP_VOTE_SYSTEM -  GlobalplayerBD")
ENDPROC

PROC RESET_GlobalplayerBD_FM_2()
	INT iCount
	GlobalPlayerBroadcastDataFM_2 GlobalplayerBD_FM_2_Temp
	FOR iCount = 0 TO (NUM_NETWORK_PLAYERS - 1)
		GlobalplayerBD_FM_2[iCount] = GlobalplayerBD_FM_2_Temp
	ENDFOR
	PRINTSTRING("...BWW MP_VOTE_SYSTEM -  RESET_GlobalplayerBD_FM_2")
ENDPROC

/// PURPOSE:resets the server vote variables 
///    Gets called just before the variables are diclatred as broadcast data
/// PARAMS:
///    mpServerVoteStruct - the server vote struct to reset. 
PROC RESET_MP_VOTE_VARAIABLES(MP_SERVER_VOTE_STRUCT &mpServerVoteStruct)
	INT iCount
	FOR iCount = 0 TO (NUM_NETWORK_REAL_PLAYERS() - 1)
		mpServerVoteStruct.missionVote[iCount]		 	= ciNULL_MISSION
		mpServerVoteStruct.missionVariation[iCount] 	= ciNULL_VARIATION
	ENDFOR
	PRINTSTRING("...BWW MP_VOTE_SYSTEM -  RESET_MP_VOTE_VARAIABLES")
ENDPROC


PROC DISABLE_VOTE_THIS_FRAME(BOOL bDisable)
	g_bDisableVoteThisFrame = bDisable
ENDPROC

PROC DISABLE_VOTE_BAR_THIS_FRAME(BOOL bDisable)
	g_bDisableVoteBarThisFrame = bDisable
ENDPROC

FUNC BOOL IS_VOTE_BAR_DISABLED_THIS_FRAME()
	RETURN g_bDisableVoteBarThisFrame
ENDFUNC
FUNC BOOL IS_VOTE_DISABLED_THIS_FRAME()
	RETURN g_bDisableVoteThisFrame
ENDFUNC
/// PURPOSE:Cleans up vote variables for an indavidual player
///    
/// PARAMS:
///    iMyGBD - the player that is being cleaned up
PROC CLEAN_UP_VOTE_VARAIABLES(INT iMyGBD)
	IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteCleanedUp)
		CLEAN_UP_MY_VOTE_VARIABLES(iMyGBD, !IS_TRANSITION_ACTIVE())
		SET_BIT  (GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteCleanedUp)
		PRINTSTRING("...BWW MP_VOTE_SYSTEM Clean up the vote varaiables")PRINTNL()
	ENDIF	
ENDPROC
/// PURPOSE:cleans up a server vote
///    
/// PARAMS:
///    iVoteID - the server vote to be cleaned up
PROC CLEANUP_MP_FLOW_VOTE(INT &iVoteID #IF IS_DEBUG_BUILD, INT iVoteNumber #ENDIF)
	#IF IS_DEBUG_BUILD
		PRINTSTRING("************************")PRINTNL()
		PRINTSTRING("CLEANUP_MP_FLOW_VOTE T(")PRINTINT(GET_CLOUD_TIME_AS_INT())PRINTSTRING(")")PRINTNL()
		PRINTSTRING("iVoteNumber = [")PRINTINT(iVoteNumber)PRINTSTRING("]")PRINTNL()
		PRINTSTRING("iVoteID = [")PRINTINT(iVoteID)PRINTSTRING("]")PRINTNL()
		PRINTSTRING("because.....")
		PRINTNL()
	#ENDIF
	iVoteID = 0
ENDPROC
/// PURPOSE:starts a new Vote
///    
/// PARAMS:
///    iVoteID - the id of the vote to start. 
/// RETURNS:true
///    
FUNC BOOL START_A_NEW_SERVER_MP_VOTE(INT &iVoteID #IF IS_DEBUG_BUILD, INT iVoteNumber #ENDIF) 
	iVoteID = GET_RANDOM_INT_IN_RANGE(5535, 65535) + GET_RANDOM_INT_IN_RANGE(5535, 65535) +GET_RANDOM_INT_IN_RANGE(5535, 65535)
	#IF IS_DEBUG_BUILD
		PRINTSTRING("************************")PRINTNL()
		PRINTSTRING("START_A_NEW_SERVER_MP_VOTE T(")PRINTINT(GET_CLOUD_TIME_AS_INT())PRINTSTRING(")")PRINTNL()
		PRINTSTRING("iVoteNumber = [")PRINTINT(iVoteNumber)PRINTSTRING("]")PRINTNL()
		PRINTSTRING("iVoteID = [")PRINTINT(iVoteID)PRINTSTRING("]")PRINTNL()
		PRINTSTRING("because.....")
		PRINTNL()
	#ENDIF
	
	RETURN TRUE
ENDFUNC

//Cleans up the server vote
PROC CLEAN_UP_SERVER_VOTE(MP_SERVER_VOTE_STRUCT &mpServerVoteStruct, INT iParticipant)
	IF GlobalplayerBD_FM_2[iParticipant].iVoteToUse != -1
		IF mpServerVoteStruct.iVoteID[GlobalplayerBD_FM_2[iParticipant].iVoteToUse] != 0
			CLEANUP_MP_FLOW_VOTE(mpServerVoteStruct.iVoteID[GlobalplayerBD_FM_2[iParticipant].iVoteToUse] #IF IS_DEBUG_BUILD, GlobalplayerBD_FM_2[iParticipant].iVoteToUse #ENDIF)
			PRINTSTRING("...CLEAN_UP_SERVER_VOTE")PRINTNL()			
			mpServerVoteStruct.missionVote[GlobalplayerBD_FM_2[iParticipant].iVoteToUse] 		= ciNULL_MISSION
			mpServerVoteStruct.missionVariation[GlobalplayerBD_FM_2[iParticipant].iVoteToUse] 	= ciNULL_VARIATION
			mpServerVoteStruct.iVoteTut[GlobalplayerBD_FM_2[iParticipant].iVoteToUse] 				= -1
			PRINTSTRING("...BWW MP_VOTE_SYSTEM CLEANUP_VOTE - ") PRINTINT(GlobalplayerBD_FM_2[iParticipant].iVoteToUse)PRINTNL()
			CLEAR_BIT(mpServerVoteStruct.iBitSet, ciVOTE_BIT_SET_0 + GlobalplayerBD_FM_2[iParticipant].iVoteToUse)
		ENDIF
	ENDIF	
ENDPROC

			
/// PURPOSE:Cleans up a participants vote and that
PROC CHECK_FOR_VOTE_CLEAN_UP(BOOL bDontCleanUpIwantToVote, BOOL bIsFreeMode = FALSE)
	IF bDontCleanUpIwantToVote = FALSE
		INT iMyParticipantID = NATIVE_TO_INT(PLAYER_ID())
		IF IS_BIT_SET(GlobalplayerBD_FM_2[iMyParticipantID].iBitSet, biIwantToVote) 
			PRINTSTRING("BWW...MP_VOTE_SYSTEM -  CHECK_FOR_VOTE_CLEAN_UP - Cleaning up biIwantToVote")PRINTNL()
			CLEAN_UP_MY_VOTE_VARIABLES(iMyParticipantID)
		ENDIF
		IF GlobalplayerBD_FM_2[iMyParticipantID].bHasPlayerVoted = TRUE	
			PRINTSTRING("BWW...MP_VOTE_SYSTEM -  CHECK_FOR_VOTE_CLEAN_UP Cleaning up GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].bHasPlayerVoted = FALSE")PRINTNL()
			GlobalplayerBD_FM_2[iMyParticipantID].bHasPlayerVoted = FALSE
		ENDIF
		IF bIsFreeMode
			IF IS_BIT_SET(GlobalplayerBD_FM[iMyParticipantID].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataCleared)
				CLEAR_BIT(GlobalplayerBD_FM[iMyParticipantID].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataCleared)
			ENDIF
			IF IS_BIT_SET(GlobalplayerBD_FM[iMyParticipantID].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
				CLEAR_BIT(GlobalplayerBD_FM[iMyParticipantID].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionDataLoaded)
			ENDIF
			IF IS_BIT_SET(GlobalplayerBD_FM[iMyParticipantID].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_ReadyToVote)
				CLEAR_BIT(GlobalplayerBD_FM[iMyParticipantID].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_ReadyToVote)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:get the minimum number of people required to start the vote for the mission
///    
/// PARAMS:
///    iMissionPassed - the mission that is being started
///    iVarationPassed - the varaition of that mission that is being started
/// RETURNS: the minimnum number required to start that mission
///    
FUNC INT GET_VOTE_MIN_START_NUMBER(INT iMissionPassed)
	//PRINTSTRING("BWW...MP_VOTE_SYSTEM - iMissionPassed = ")PRINTINT(iMissionPassed) PRINTNL()  FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID 
//	PRINTSTRING("BWW...MP_VOTE_SYSTEM - iVarationPassed = ")PRINTINT(iVarationPassed) PRINTNL()  FMMC_ROCKSTAR_HIDEOUT_CREATOR_ID	 
	IF iMissionPassed = ciNULL_MISSION
		RETURN 99
	ENDIF

	RETURN 1
	INT iMyParticipantID = NATIVE_TO_INT(PLAYER_ID())
	IF (GlobalplayerBD_FM_2[iMyParticipantID].missionType = FMMC_TYPE_DEATHMATCH)
		RETURN 2
	ELIF (GlobalplayerBD_FM_2[iMyParticipantID].missionType = FMMC_TYPE_MISSION)
		IF g_FMMC_STRUCT.iMinNumberOfTeams > 0
			RETURN g_FMMC_STRUCT.iMinNumberOfTeams
		ELSE
			RETURN 1
		ENDIF
	ELIF GlobalplayerBD_FM[iMyParticipantID].iCreatorIDOfMIssionIwantToStart = FMMC_MINI_GAME_CREATOR_ID
		RETURN GET_FMMC_MINI_GAME_MIN_START_NUMBER(GlobalplayerBD_FM_2[iMyParticipantID].missionType)
	ELSE
		RETURN 1
	ENDIF

ENDFUNC


/// PURPOSE:get the number of people required for the vote to automatically pass (maximum)
///    
/// PARAMS:
///    iMissionPassed - the mission that is being started
///    iVarationPassed - the varaition of that mission that is being started
/// RETURNS: the number required to start that mission
///    
FUNC INT GET_VOTE_PASS_NUMBER(INT iMissionPassed)	
	
	//PRINTSTRING("BWW...MP_VOTE_SYSTEM - iMissionPassed = ")PRINTINT(iMissionPassed) PRINTNL()
	//PRINTSTRING("BWW...MP_VOTE_SYSTEM - iVarationPassed = ")PRINTINT(iVarationPassed) PRINTNL()
	IF iMissionPassed = ciNULL_MISSION
		RETURN 99
	ENDIF
	
	INT iMyParticipantID = NATIVE_TO_INT(PLAYER_ID())
	IF GlobalplayerBD_FM[iMyParticipantID].iCreatorIDOfMIssionIwantToStart = FMMC_ROCKSTAR_CREATOR_ID
	OR GlobalplayerBD_FM[iMyParticipantID].iCreatorIDOfMIssionIwantToStart = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
	OR GlobalplayerBD_FM[iMyParticipantID].iCreatorIDOfMIssionIwantToStart = FMMC_ROCKSTAR_HIDEOUT_CREATOR_ID
		RETURN 16
	
	ELIF GlobalplayerBD_FM[iMyParticipantID].iCreatorIDOfMIssionIwantToStart = FMMC_MINI_GAME_CREATOR_ID
		PRINTLN("GET_FMMC_MINI_GAME_MAX_START_NUMBER")
		RETURN GET_FMMC_MINI_GAME_MAX_START_NUMBER(GlobalplayerBD_FM_2[iMyParticipantID].missionType)
	ELSE
		//Get the playerindex of the person mission that I want to start
		PLAYER_INDEX piTemp = INT_TO_NATIVE(PLAYER_INDEX, GlobalplayerBD_FM[iMyParticipantID].iCreatorIDOfMIssionIwantToStart)
		IF piTemp != INT_TO_NATIVE(PLAYER_INDEX, -1)
			IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(piTemp)
				INT iMaxAmount = 0 //GlobalplayerBD_FM[NATIVE_TO_INT(piTemp)].loadedMissionData.iMaxStartNumber // Removed as debug only so would always be 0
				IF iMaxAmount < 2 
					iMaxAmount = 2
				ELIF iMaxAmount >= NUM_NETWORK_PLAYERS
					iMaxAmount = (NUM_NETWORK_PLAYERS - 1)
				ENDIF				
				#IF IS_DEBUG_BUILD
					IF (GET_GAME_TIMER() % 5000) < 75
						PRINTSTRING("GET_VOTE_PASS_NUMBER = ")
						PRINTINT(iMaxAmount)PRINTNL()
					ENDIF
				#ENDIF
				RETURN iMaxAmount
			ENDIF
		ENDIF
	ENDIF
	//Default
	RETURN 16
	
ENDFUNC

FUNC INT GET_PLAYER_WHO_IS_VOTING(MP_SERVER_VOTE_STRUCT &mpVoteStruct)
	RETURN mpVoteStruct.iParticipantInChargeOfVote[GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iVoteToUse]
ENDFUNC

FUNC BOOL AM_I_IN_CHARGE_OF_THIS_VOTE(MP_SERVER_VOTE_STRUCT &mpVoteStruct)
	IF NOT HAS_PLAYLIST_HAS_DONE_INITIAL_TRANSITION()
	AND NOT HAS_JOB_HAS_LAUNCHED_INITIAL_TRANSITION_SESSION()
		IF AM_I_HOST_OF_THIS_TRANSITION_SESSION()
			RETURN TRUE
		ENDIF	
	ELSE
		INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
		IF GlobalplayerBD_FM_2[iMyGBD].iVoteToUse >= 0
			RETURN (mpVoteStruct.iParticipantInChargeOfVote[GlobalplayerBD_FM_2[iMyGBD].iVoteToUse] = iMyGBD)
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_THIS_PLAYER_VOTED(INT iPlayerToCheck)
	IF (IS_ARENA_WARS_JOB()
	OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_KART_BATTLE(g_FMMC_STRUCT.iAdversaryModeType))
	AND GET_CORONA_STATUS() = CORONA_STATUS_IN_GENERIC_JOB_CORONA
		RETURN IS_BIT_SET(GlobalplayerBD_FM_2[iPlayerToCheck].iBitSet, biIPressedVoteSecondTime)
	ENDIF
	RETURN IS_BIT_SET(GlobalplayerBD_FM_2[iPlayerToCheck].iBitSet, biIPressedVote)
ENDFUNC

FUNC  BOOL HAS_PLAYER_IN_CHARGE_OF_MENU_VOTED(MP_SERVER_VOTE_STRUCT &mpVoteStruct)
	INT iPlayerInCharge = GET_PLAYER_WHO_IS_VOTING(mpVoteStruct)
	IF  iPlayerInCharge >= 0 
	AND iPlayerInCharge < NUM_NETWORK_PLAYERS
		RETURN HAS_THIS_PLAYER_VOTED(iPlayerInCharge)
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

/// PURPOSE:
///    Send event to players to set visibility in corona as joining vote
/// PARAMS:
///    iVoteID - Vote player is joining
PROC BROADCAST_PLAYER_JOINED_NEW_VOTE(INT iVoteID)	
	EVENT_STRUCT_PLAYER_JOINED_VOTE Event
	Event.Details.Type = SCRIPT_EVENT_PLAYER_JOINED_VOTE
	Event.Details.FromPlayerIndex = PLAYER_ID()
	Event.iVoteID = iVoteID

	INT iSendTo = ALL_PLAYERS(FALSE)
	IF NOT (iSendTo = 0)
	
		NET_PRINT("BROADCAST_PLAYER_JOINED_NEW_VOTE called...") NET_NL()
		NET_PRINT("		iVoteID: ") NET_PRINT_INT(iVoteID) NET_NL()
		NET_PRINT_STRINGS("		From script: ", GET_THIS_SCRIPT_NAME()) NET_NL()
	
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, Event, SIZE_OF(Event), iSendTo)
	ELSE
		NET_PRINT("BROADCAST_PLAYER_JOINED_NEW_VOTE - playerflags = 0 so not broadcasting") NET_NL()
	ENDIF
ENDPROC

/// PURPOSE:checks to see if a player is on ANY  misssion
///    
/// PARAMS:
///    playerID - the player
/// RETURNS:true is that player is on that mission
///    
FUNC BOOL IS_VOTING_PLAYER_ON_ANY_MISSION(PLAYER_INDEX playerID)
		RETURN IS_PLAYER_ON_ANY_FM_MISSION(playerID)

ENDFUNC
/// PURPOSE:checks to see if a player is on the  misssion that is passed to it
///    
/// PARAMS:
///    playerID - the player
///    iMissionPassed - the mission
///    iVarationPassed - the varation of that mission
/// RETURNS:true is that player is on that mission
///   
FUNC BOOL IS_VOTING_PLAYER_ON_THIS_MISSION(PLAYER_INDEX playerID, INT iMissionPassed, INT iVarationPassed, INT iFMCreatorID = 0 )
	IF iMissionPassed = ciNULL_MISSION
		RETURN FALSE
	ENDIF
	RETURN IS_PLAYER_ON_FM_MISSION(playerID, iMissionPassed, iVarationPassed, iFMCreatorID)

ENDFUNC



//#IF IS_DEBUG_BUILD
//PROC PRINT_VOTE_PASSED_DEBUG()
//	INT iParticipant
//	INT iMyParticipantID = NATIVE_TO_INT(PLAYER_ID()) 
//	//COunt the number of people that have voted like
//	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
//		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
//			//IF they are active see if they are on the same beautiful vote as I.
//			IF GlobalplayerBD_FM_2[iMyParticipantID].iVoteToUse = GlobalplayerBD_FM_2[iParticipant].iVoteToUse 
//				//Incrament the number in my vote please
//				PRINTSTRING("...BWW MP_VOTE_SYSTEM iParticipant - ") 
//				PRINTINT(iParticipant) 
//				PRINTSTRING(" - is in my vote - ")
//				PRINTSTRING(GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))))
//				PRINTNL()
//			ENDIF	
//			//If that player has voted then set a bool to the correct state.
//			IF HAS_THIS_PLAYER_VOTED(iParticipant)
//				PRINTSTRING("...BWW MP_VOTE_SYSTEM iParticipant - ")
//				PRINTINT(iParticipant)
//				PRINTSTRING(" - Has Voted - ")
//				PRINTSTRING(GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))))
//				PRINTNL()
//			ENDIF
//		ENDIF
//	ENDREPEAT
//ENDPROC
//#ENDIF

FUNC BOOL IS_IT_OK_TO_DRAW_VOTE_BARS(BOOL bVoted)

	IF bVoted
		RETURN TRUE
	ENDIF
	IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_ReadyToVote)
		RETURN FALSE
	ENDIF
	
		
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND NOT IS_PED_ON_MOUNT(PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:returns true is a vote has passed
///    
/// PARAMS:
///    mpServerVoteStruct - the server vote struct passed
///    sVoteTitle - the string to print when some one is to vote
///    sPlayerVotedTitle - the string to say when some one has votes
/// RETURNS:true is a voete has passsed
FUNC BOOL HAS_MP_LOCATION_VOTE_PASSED(MP_SERVER_VOTE_STRUCT &mpServerVoteStruct, FLOAT fVoteFractionToPass = 1.0  #IF IS_DEBUG_BUILD, INT iDedugForceNumberInVote = 0, INT iDedugForceNumberVoted = 0 #ENDIF)
	
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	INT iParticipant
	INT iNumberVoted
	INT iNumberInVote
	INT iBitSetVoted

	
	INT i
	FOR i = 0 TO (NUM_NETWORK_PLAYERS - 1)
		g_piPlayersInMyVote[i] = INT_TO_NATIVE(PLAYER_INDEX, -1)
	ENDFOR
	
	//COunt the number of people that have voted like
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			//If the player is active then check to see the status of their flow flags.
			PLAYER_INDEX 	piPlayer	= NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(piPlayer)
				INT iPlayerGBD = NATIVE_TO_INT(piPlayer)
				//IF they are active see if they are on the same beautiful vote as I.
				IF GlobalplayerBD_FM_2[iMyGBD].iVoteToUse = GlobalplayerBD_FM_2[iPlayerGBD].iVoteToUse 
					SET_BIT(g_iMPonMyVoteINTbitSet, iPlayerGBD)
					g_piPlayersInMyVote[g_iNoOfOtherPlayersinMyVote] = piPlayer
					g_iNoOfOtherPlayersinMyVote++
					//Incrament the number in my vote please
					iNumberInVote++
					#IF IS_DEBUG_BUILD
						IF (GET_GAME_TIMER() % 5000) < 75
							PRINTSTRING("...BWW MP_VOTE_SYSTEM iParticipant - ") 
							PRINTINT(iPlayerGBD) 
							PRINTSTRING(" - is in my vote - ")
							PRINTSTRING(GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))))
							PRINTNL()
						ENDIF	
					#ENDIF	
					//If that player has voted then set a bool to the correct state.
					IF HAS_THIS_PLAYER_VOTED(iPlayerGBD)
						#IF IS_DEBUG_BUILD
							IF (GET_GAME_TIMER() % 5000) < 75
								PRINTSTRING("...BWW MP_VOTE_SYSTEM iParticipant - ")
								PRINTINT(iPlayerGBD)
								PRINTSTRING(" - Has Voted - ")
								PRINTSTRING(GET_PLAYER_NAME(piPlayer))
								PRINTNL()
							ENDIF	
						#ENDIF
						//Set the bit to sat that the person has voted. 
						SET_BIT(iBitSetVoted, iNumberVoted)
						//incrament the number in the vote
						iNumberVoted++
						//ccap the number in the vote
						IF iNumberVoted >= NUM_NETWORK_PLAYERS
							iNumberVoted = NUM_NETWORK_PLAYERS
						ENDIF
					ENDIF 
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
		
	//make an INT to cap the number. 
	INT iNumberInVotToSetUp = iNumberInVote //mpServerVoteStruct.iNumberInVote[GlobalplayerBD_FM_2[iMyParticipantID].iVoteToUse]
//	IF iNumberInVote != mpServerVoteStruct.iNumberInVote[GlobalplayerBD_FM_2[iMyParticipantID].iVoteToUse]
//		SCRIPT_ASSERT("iNumberInVote != mpServerVoteStruct.iNumberInVote[GlobalplayerBD_FM_2[iMyParticipantID].iVoteToUse] bug to Bobby wright")
//	ENDIF
	//Cap the number
	#IF IS_DEBUG_BUILD
	IF iDedugForceNumberInVote > iNumberInVotToSetUp
		iNumberInVote        = iDedugForceNumberInVote
		iNumberInVotToSetUp  = iDedugForceNumberInVote
		PRINTSTRING("...BWW Setting  -----       iNumberInVote        = iDedugForceNumberInVote")PRINTNL()
		PRINTSTRING("...BWW Setting  -----       iNumberInVotToSetUp  = iDedugForceNumberInVote")PRINTNL()
	ENDIF
	IF iDedugForceNumberVoted > iNumberVoted
		iNumberVoted        = iDedugForceNumberVoted
		PRINTSTRING("...BWW Setting  -----       iNumberVoted  = iDedugForceNumberVoted")PRINTNL()
	ENDIF
	#ENDIF
	IF iNumberInVotToSetUp > NUM_NETWORK_PLAYERS
		iNumberInVotToSetUp = NUM_NETWORK_PLAYERS
	ENDIF
	INT iPlayerTeam = GET_PLAYER_TEAM(PLAYER_ID())
	IF iPlayerTeam = -1
		iPlayerTeam = NATIVE_TO_INT(PLAYER_ID())
	ENDIF
	//if the number in the vote is greater than the minum number of people required then a vote is allowed to go ahead	
	INT iVoteMinStart = GET_VOTE_MIN_START_NUMBER(mpServerVoteStruct.missionVote[GlobalplayerBD_FM_2[iMyGBD].iVoteToUse])
	
	IF (IS_ARENA_WARS_JOB()
	OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_KART_BATTLE(g_FMMC_STRUCT.iAdversaryModeType))
	AND GET_CORONA_STATUS() = CORONA_STATUS_IN_GENERIC_JOB_CORONA
		iVoteMinStart = iNumberInVotToSetUp
		PRINTLN("...BWW Setting  -----   Arena Wars Job Vehicle Selection ---- iVoteMinStart = iNumberInVotToSetUp (", iVoteMinStart, ")")
	ENDIF
	
	//If the vote has been sucessful the pass the vote. 
	IF fVoteFractionToPass < 1.0
		IF iNumberVoted < 2
			fVoteFractionToPass = 1.0
		ENDIF
	ENDIF
	GlobalplayerBD_FM_2[iMyGBD].iNumberInVote = mpServerVoteStruct.iNumberInVote[GlobalplayerBD_FM_2[iMyGBD].iVoteToUse]
	//if more than the minimum have voted
	IF iNumberVoted >= iVoteMinStart
	//AND MORE THAN 50% have voted.
		IF TO_FLOAT(iNumberVoted) >= (TO_FLOAT(mpServerVoteStruct.iNumberInVote[GlobalplayerBD_FM_2[iMyGBD].iVoteToUse]) * fVoteFractionToPass)
			IF mpServerVoteStruct.iNumberInVote[GlobalplayerBD_FM_2[iMyGBD].iVoteToUse] >= iVoteMinStart
		
				#IF IS_DEBUG_BUILD
				PRINTLN("...BWW Vote Passed Details	:")
				PRINTLN("...			iNumberVoted:			", iNumberVoted)
				PRINTLN("...			iVoteMinStart:			", iVoteMinStart)
				PRINTLN("...			fVoteFractionToPass:	", fVoteFractionToPass)
				PRINTLN("...			mpServerVoteStruct.iNumberInVote[GlobalplayerBD_FM_2[", iMyGBD,"].iVoteToUse]:	", mpServerVoteStruct.iNumberInVote[GlobalplayerBD_FM_2[iMyGBD].iVoteToUse])
				#ENDIF
			
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	//Vote has not passed
	RETURN FALSE
ENDFUNC

/// PURPOSE:Cheks to see if the team that the person is on is important
/// PARAMS:
///    mpServerVoteStruct 	- The server vote struct passed
///    iGBDToCheck 			- The GBD slot to check 
///    iVoteID 				- The Vote to check
/// RETURNS:True if it is ok to vote with this player
FUNC BOOL CAN_I_VOTE_WITH_THIS_TUT_SESSION(MP_SERVER_VOTE_STRUCT &mpServerVoteStruct, INT iGBDToCheck, INT iVoteID)
	IF GlobalplayerBD_FM_2[iGBDToCheck].iFmTutorialSession = -1
		RETURN TRUE
	ENDIF
	IF mpServerVoteStruct.iVoteTut[iVoteID] = GlobalplayerBD_FM_2[iGBDToCheck].iFmTutorialSession
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
FUNC  BOOL HAS_THIS_VOTE_PASSED(MP_SERVER_VOTE_STRUCT &mpServerVoteStruct, INT iMyGBD, FLOAT fVoteFractionToPass = 1.0 #IF IS_DEBUG_BUILD, INT iDedugForceNumberInVote = 0, INT iDedugForceNumberVoted = 0 #ENDIF)
	//If the vote has passed
	IF HAS_MP_LOCATION_VOTE_PASSED(mpServerVoteStruct, fVoteFractionToPass  #IF IS_DEBUG_BUILD, iDedugForceNumberInVote, iDedugForceNumberVoted #ENDIF)
		RETURN TRUE
	ENDIF
								
	//Or There are enough people for the vote to auto pass
//	IF g_iMPGameMode = GAMEMODE_FM
//		IF mpServerVoteStruct.iNumberInVote[GlobalplayerBD_FM_2[iMyGBD].iVoteToUse] >= GET_VOTE_PASS_NUMBER(GlobalplayerBD_FM_2[iMyGBD].missionToStart , GlobalplayerBD_FM_2 [iMyGBD].missionVariation)
//		AND mpServerVoteStruct.iNumberInVote[GlobalplayerBD_FM_2[iMyGBD].iVoteToUse] > 2
//			RETURN TRUE
//		ENDIF 
//	ELSE
//		IF mpServerVoteStruct.iNumberInVote[GlobalplayerBD_FM_2[iMyGBD].iVoteToUse] >= GET_VOTE_PASS_NUMBER(GlobalplayerBD_FM_2[iMyGBD].missionToStart , GlobalplayerBD_FM_2 [iMyGBD].missionVariation)
//			RETURN TRUE
//		ENDIF
//	ENDIF
	
	//Or the server bit is set to say that my vote has passed. 
	IF IS_BIT_SET(mpServerVoteStruct.iBitSet2, ciVOTEPASSED_BIT_SET_0 + GlobalplayerBD_FM_2[iMyGBD].iVoteToUse)
		RETURN TRUE
	ENDIF						
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_I_VOTED(INT iMyGBD)
	IF iMyGBD <> -1
		RETURN GlobalplayerBD_FM_2[iMyGBD].bHasPlayerVoted
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SET_VOTE(INT iMyGBD, BOOL bVote, BOOl bClearBoth = FALSE)
	PRINTLN("...BWW MP_VOTE_SYSTEM - SET_VOTE - ", GET_STRING_FROM_BOOL(bVote))
	IF bVote
		PRINTLN("...BWW MP_VOTE_SYSTEM - I have voted.  iMyGBD = ", iMyGBD, ". T(", GET_CLOUD_TIME_AS_INT(), ")")
	
		IF (IS_ARENA_WARS_JOB()
		OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_KART_BATTLE(g_FMMC_STRUCT.iAdversaryModeType))
		AND GET_CORONA_STATUS() = CORONA_STATUS_IN_GENERIC_JOB_CORONA
			SET_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biIPressedVoteSecondTime)
		ELSE
			SET_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biIPressedVote)
		ENDIF
		
		GlobalplayerBD_FM_2[iMyGBD].bHasPlayerVoted = TRUE
		
		GlobalplayerBD_FM[iMyGBD].sClientCoronaData.iClientSyncID++	// Increment client ID for corona updates
		
		IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING() 
			PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FREEMODE_SOUNDSET")
		ENDIF
		
		DEBUG_PRINTCALLSTACK()
	ELSE
		PRINTLN("...BWW MP_VOTE_SYSTEM - I have UN-Voted. iMyGBD = ", iMyGBD, ". T(", GET_CLOUD_TIME_AS_INT(), "); bClearBoth = ", GET_STRING_FROM_BOOL(bClearBoth))
	
		IF bClearBoth
			CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biIPressedVote)
			CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biIPressedVoteSecondTime)
		ELSE
			IF (IS_ARENA_WARS_JOB()
			OR IS_THIS_ROCKSTAR_MISSION_NEW_DM_KART_BATTLE(g_FMMC_STRUCT.iAdversaryModeType))
			AND GET_CORONA_STATUS() = CORONA_STATUS_IN_GENERIC_JOB_CORONA
				CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biIPressedVoteSecondTime)
			ELSE
				CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biIPressedVote)
			ENDIF
		ENDIF
		
		GlobalplayerBD_FM[iMyGBD].sClientCoronaData.iClientSyncID++	// Increment client ID for corona updates
		
		GlobalplayerBD_FM_2[iMyGBD].bHasPlayerVoted = FALSE
		
		IF NOT IS_AN_AIRLOCK_STRAND_TRANSITION_RUNNING() 
			PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FREEMODE_SOUNDSET")
		ENDIF
		
		DEBUG_PRINTCALLSTACK()
	ENDIF
ENDPROC

PROC SET_ME_AS_VOTED(INT iMyGBD)
	SET_VOTE(iMyGBD, TRUE)
ENDPROC

PROC CLEAR_ME_AS_VOTED(INT iMyGBD, BOOL bClearBoth = FALSE)
	SET_VOTE(iMyGBD, FALSE, bClearBoth)
ENDPROC


PROC SET_THAT_THIS_IS_A_SHORT_TIME_CORONA()
	PRINTLN("[TS] SET_THAT_THIS_IS_A_SHORT_TIME_CORONA")
	SET_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iBitSet, bi_ShortTimeCorona)
ENDPROC

PROC CLEAR_THAT_THIS_IS_A_SHORT_TIME_CORONA()
	PRINTLN("[TS] CLEAR_THAT_THIS_IS_A_SHORT_TIME_CORONA")
	CLEAR_BIT(GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].iBitSet, bi_ShortTimeCorona)
ENDPROC

FUNC BOOL IS_THIS_IS_A_SHORT_TIME_CORONA(INT iVoteParticipant)
	RETURN IS_BIT_SET(GlobalplayerBD_FM_2[iVoteParticipant].iBitSet, bi_ShortTimeCorona)
ENDFUNC

FUNC BOOL IS_FM_VOTE_FULL(MP_SERVER_VOTE_STRUCT &mpServerVoteStruct, INT iMyGBD, INT iVoteID)

		IF mpServerVoteStruct.iNumberInVote[iVoteID] >= GET_VOTE_PASS_NUMBER(GlobalplayerBD_FM_2[iMyGBD].missionToStart , GlobalplayerBD_FM_2 [iMyGBD].missionVariation)
			RETURN TRUE
		ENDIF
	RETURN FALSE
ENDFUNC

// Check if PC mouse is clicking the correct menu element when confirming ready to play.
FUNC BOOL IS_PC_READY_TO_PLAY_CLICKED()

	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		IF NOT IS_WARNING_MESSAGE_ACTIVE()
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				IF PAUSE_MENU_GET_MOUSE_HOVER_UNIQUE_ID() = 99 // Corona ready
				OR PAUSE_MENU_GET_MOUSE_HOVER_UNIQUE_ID() = 13 // Heist ready
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC


//Maintains the participant and server checks
PROC MAINTAIN_PARTICIPENT_VOTE_CHECKS(MP_SERVER_VOTE_STRUCT &mpServerVoteStruct, LOCAL_VOTE_STRUCT &sVoteStruct, INT iMyGBD, BOOL &bShouldRun,  FLOAT fVoteFractionToPass = 1.0 #IF IS_DEBUG_BUILD, INT iDedugForceNumberInVote = 0, INT iDedugForceNumberVoted = 0 #ENDIF )

	//Set the voting bit set INT to Zero
	g_iMPonMyVoteINTbitSet = 0	
	
	//Clear that it should not run if not one of these states
	SWITCH GlobalplayerBD_FM[iMyGBD].iFmLauncherGameState
		CASE FMMC_LAUNCHER_STATE_IN_CORONA
		CASE FMMC_LAUNCHER_STATE_RUNNING
		CASE FMMC_LAUNCHER_STATE_RUN_PLAY_LIST
			//Nothing
		BREAK
		
		//Not one of the above, so not going to run
		DEFAULT
			bShouldRun = FALSE
		BREAK	
		
	ENDSWITCH
	
	IF bShouldRun
		INT i
		
		GlobalplayerBD_FM_2[iMyGBD].iTeam = GET_PLAYER_TEAM(PLAYER_ID())
		
		//IF bDountCountVote
			//IS_BIT_SET(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biIwantToVote)
			//GlobalplayerBD_FM_2[iMyGBD].missionToStart = eCR_Convoy_Steal
		//ENDIF
		//Deal with the voting	
		IF IS_BIT_SET(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biIwantToVote)
			IF IS_BIT_SET(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteCleanedUp)
				CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteCleanedUp)
				PRINTSTRING("...BWW MP_VOTE_SYSTEM -  resetting reset bit flag biIwantToVote")PRINTNL()
			ENDIF
			IF IS_BIT_SET(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biILeftTheArea)
				CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biILeftTheArea)
				PRINTSTRING("...BWW MP_VOTE_SYSTEM -  resetting reset bit flag biILeftTheArea")PRINTNL()
			ENDIF
			
			IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteSetUp)
				//#IF IS_DEBUG_BUILD
					//BOOL bFoundVote = FALSE
				//#ENDIF				
				//Find out if there is not a vote going on
				FOR i = 0 TO (NUM_NETWORK_REAL_PLAYERS() -1)
					IF mpServerVoteStruct.missionVote[i] 		= GlobalplayerBD_FM_2[iMyGBD].missionToStart	
					AND mpServerVoteStruct.missionVariation[i] 	= GlobalplayerBD_FM_2[iMyGBD].missionVariation
					AND CAN_I_VOTE_WITH_THIS_TUT_SESSION(mpServerVoteStruct, iMyGBD, i)
						IF NETWORK_IS_ACTIVITY_SESSION()
						OR (mpServerVoteStruct.iNumberInVote[i] 	< GlobalplayerBD_FM_2[iMyGBD].iVoteMaximum	)
						OR (mpServerVoteStruct.iNumberInVote[i] = 1 AND GlobalplayerBD_FM_2[iMyGBD].iVoteMaximum = 1)
							//IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								//IF (NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								//AND NOT IS_PED_ON_MOUNT(PLAYER_PED_ID()))
								//OR IS_AREA_TRIGGERED_MISSION(GlobalplayerBD_FM_2[iMyGBD].missionType)
									#IF IS_DEBUG_BUILD
										PRINTSTRING("...BWW MP_VOTE_SYSTEM - There is a vote going on for the mission I want to vote on, joining that vote")
										PRINTNL()
									#ENDIF	
									//Set that to the vote I will use
									GlobalplayerBD_FM_2[iMyGBD].iVoteToUse = i
									
									// Broadcast to set visibility for other players 
									BROADCAST_PLAYER_JOINED_NEW_VOTE(i)
									
									IF IS_BIT_SET(mpServerVoteStruct.iBitset_IsMultiVote, GlobalplayerBD_FM_2[iMyGBD].iVoteToUse)
										SET_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biImOnMultiCoronaVote)
									ELSE
										CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biImOnMultiCoronaVote)
									ENDIF
									SET_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteSetUp)
									PRINTSTRING("...BWW MP_VOTE_SYSTEM - GlobalplayerBD_FM_2[].iBitSet, biVoteSetUp")PRINTNL()
									i = 99
									#IF IS_DEBUG_BUILD
										PRINTSTRING("--[VOTE_ID]--")PRINTLN()
										PRINTSTRING("GlobalplayerBD_FM_2[iMyGBD].iVoteToUse VOTE ID mpServerVoteStruct.iVoteID[")
										PRINTINT(GlobalplayerBD_FM_2[iMyGBD].iVoteToUse)
										PRINTSTRING("] = [")
										PRINTINT(mpServerVoteStruct.iVoteID[GlobalplayerBD_FM_2[iMyGBD].iVoteToUse])
										PRINTSTRING("]")
										PRINTNL()
									#ENDIF		
									//#IF IS_DEBUG_BUILD
									//	bFoundVote = TRUE
									//#ENDIF	
								//ENDIF
							//#IF IS_DEBUG_BUILD
							//ELSE
							//	PRINTSTRING("...BWW MP_VOTE_SYSTEM - IS_PED_INJURED = TRUE")
							//#ENDIF	
							//ENDIF
						//ELSE
						//	PRINTLN("mpServerVoteStruct.iNumberInVote[", i, "] = ", mpServerVoteStruct.iNumberInVote[i])
						//	PRINTLN("GlobalplayerBD_FM_2[", iMyGBD, "].iVoteMaximum    = ", GlobalplayerBD_FM_2[iMyGBD].iVoteMaximum)
						ENDIF
					ENDIF
				ENDFOR
			ELSE
				//Deal with voting here
				IF GlobalplayerBD_FM_2[iMyGBD].iVoteToUse != -1
					//IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteHasPassed)	
						IF IS_BIT_SET(mpServerVoteStruct.iBitSet, ciVOTE_BIT_SET_0 + GlobalplayerBD_FM_2[iMyGBD].iVoteToUse)
							GlobalplayerBD_FM_2[iMyGBD].iNumberInVote = 0
							g_iNoOfOtherPlayersinMyVote = 0
							g_iNoOfPlayersForRightMenu = 0
							IF mpServerVoteStruct.iNumberInVote[GlobalplayerBD_FM_2[iMyGBD].iVoteToUse] >= GET_VOTE_MIN_START_NUMBER(GlobalplayerBD_FM_2[iMyGBD].missionToStart)
								
								IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteIsGoinOn)
									SET_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteIsGoinOn)
									//PRINTSTRING("...BWW MP_VOTE_SYSTEM - GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteIsGoinOn")PRINTNL()
								ENDIF
								
								//IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								IF NOT IS_VOTE_DISABLED_THIS_FRAME()
									//IF (NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									//AND NOT IS_PED_ON_MOUNT(PLAYER_PED_ID()))
									//OR IS_AREA_TRIGGERED_MISSION(GlobalplayerBD_FM_2[iMyGBD].missionType)
										//IF NOT IS_PED_RUNNING(PLAYER_PED_ID())
										//AND NOT IS_PHONE_ONSCREEN()
										IF IS_SCREEN_FADED_IN()
										//AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
											//Check for vote button being pressed
											IF HAS_THIS_PLAYER_VOTED(iMyGBD)
												IF (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) AND NOT NETWORK_TEXT_CHAT_IS_TYPING())
												OR IS_PC_READY_TO_PLAY_CLICKED()
													IF sVoteStruct.iVoteTimer < GET_GAME_TIMER()
														CLEAR_ME_AS_VOTED(iMyGBD)	
														sVoteStruct.iVoteTimer = GET_GAME_TIMER() + ciMP_VOTE_TIME_DELAY
														//SET_BIT(sFmmcLauncherMenu.iButtonPressedBS, FM_PRESSED_BS_ACCEPT)
													ENDIF
												ENDIF
											ELSE
												IF (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)  AND NOT NETWORK_TEXT_CHAT_IS_TYPING())	
												OR IS_PC_READY_TO_PLAY_CLICKED()
													IF sVoteStruct.iVoteTimer < GET_GAME_TIMER()
														SET_ME_AS_VOTED(iMyGBD)															
														sVoteStruct.iVoteTimer = GET_GAME_TIMER() + ciMP_VOTE_TIME_DELAY
														//SET_BIT(sFmmcLauncherMenu.iButtonPressedBS, FM_PRESSED_BS_ACCEPT)
													ENDIF
												ENDIF
											ENDIF
											#IF IS_DEBUG_BUILD
											IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_NONE, "J Skip")
												IF NOT HAS_THIS_PLAYER_VOTED(iMyGBD)
													NET_PRINT("...BWW I have voted. - J Skip")NET_NL()
													SET_ME_AS_VOTED(iMyGBD)															
												ENDIF
											ENDIF
											#ENDIF
										ENDIF
									//ENDIF
								ENDIF
								
								//If has the vote passed
								IF HAS_THIS_VOTE_PASSED(mpServerVoteStruct, iMyGBD, fVoteFractionToPass #IF IS_DEBUG_BUILD, iDedugForceNumberInVote, iDedugForceNumberVoted #ENDIF)
									//IF HAS_PLAYER_IN_CHARGE_OF_MENU_VOTED(mpServerVoteStruct)
										SET_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteHasPassed)
										//Print some extra debug information
										#IF IS_DEBUG_BUILD
											IF IS_BIT_SET(mpServerVoteStruct.iBitSet2, ciVOTEPASSED_BIT_SET_0 + GlobalplayerBD_FM_2[iMyGBD].iVoteToUse)
												PRINTSTRING("...BWW MP_VOTE_SYSTEM - VotePassed, IS_BIT_SET(mpServerVoteStruct.iBitSet, ciVOTEPASSED_BIT_SET_0 + GlobalplayerBD_FM_2[iMyGBD].iVoteToUse)")PRINTNL()
											ENDIF
											IF mpServerVoteStruct.iNumberInVote[GlobalplayerBD_FM_2[iMyGBD].iVoteToUse] >= GET_VOTE_PASS_NUMBER(GlobalplayerBD_FM_2[iMyGBD].missionToStart)
												PRINTSTRING("...BWW MP_VOTE_SYSTEM - VotePassed, mpServerVoteStruct.iNumberInVote[GlobalplayerBD_FM_2[iMyGBD].iVoteToUse] >= GET_VOTE_PASS_NUMBER()")PRINTNL()
											ENDIF
											PRINTSTRING("...BWW MP_VOTE_SYSTEM - VotePassed, Magic")PRINTNL()
										#ENDIF		
									//ENDIF
								ENDIF
//								#IF IS_DEBUG_BUILD
//									IF (GET_GAME_TIMER() % 5000) < 75
//										PRINTSTRING("...BWW MP_VOTE_SYSTEM - enough people in vote ")PRINTNL()
//									ENDIF	
//								#ENDIF
							ELSE
//								#IF IS_DEBUG_BUILD
//									IF (GET_GAME_TIMER() % 5000) < 75
//										PRINTSTRING("...BWW MP_VOTE_SYSTEM - Not enough people in vote [Got: ")
//										PRINTINT(mpServerVoteStruct.iNumberInVote[GlobalplayerBD_FM_2[iMyGBD].iVoteToUse])
//										PRINTSTRING("  Need: ")
//										PRINTINT(GET_VOTE_MIN_START_NUMBER(GlobalplayerBD_FM_2[iMyGBD].missionToStart))
//										PRINTSTRING("]")
//										PRINTNL()
//										PRINTLN("missionToStart =", GlobalplayerBD_FM_2[iMyGBD].missionToStart)
//										PRINTLN("missionVariation = ", GlobalplayerBD_FM_2 [iMyGBD].missionVariation)
//									ENDIF	
//								#ENDIF		
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
//								IF (GET_GAME_TIMER() % 5000) < 75									
//									PRINTSTRING("...BWW MP_VOTE_SYSTEM - not set up ")PRINTNL()
//								ENDIF	
							#ENDIF	
						ENDIF
//					ELSE
//						// NOTE: Bobby and James see this as legacy and removed due to 1892471.
//						//So players can still vote after the vote has passed but can;t unvote - 1327209 - Was pressing A with Ready highlighted but a READY icon wasn’t displaying next to my name.
//						IF NOT IS_VOTE_DISABLED_THIS_FRAME()
//							IF NOT HAS_THIS_PLAYER_VOTED(iMyGBD)
//								IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)		
//									IF sVoteStruct.iVoteTimer < GET_GAME_TIMER()
//										PRINTSTRING("...BWW MP_VOTE_SYSTEM - SET_ME_AS_VOTED after vote has passed ")PRINTNL()
//										SET_ME_AS_VOTED(iMyGBD)															
//										sVoteStruct.iVoteTimer = GET_GAME_TIMER() + ciMP_VOTE_TIME_DELAY
//										SET_BIT(iButtonBS, FM_PRESSED_BS_ACCEPT)
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					
//						//The vote has been set up and the vote has passed
//						//Check to see if I'm on a mission and then clean every thing up
//						IF IS_VOTING_PLAYER_ON_ANY_MISSION(PLAYER_ID())
//							IF IS_VOTING_PLAYER_ON_THIS_MISSION(PLAYER_ID(), GlobalplayerBD_FM_2[iMyGBD].missionToStart, GlobalplayerBD_FM_2[iMyGBD].missionVariation, GlobalplayerBD_FM_2[iMyGBD].iCreatorID)
//								PRINTSTRING("...BWW MP_VOTE_SYSTEM - Player is now on the mission that we wanted them to be on, mazing")
//								IF IS_BIT_SET(mpServerVoteStruct.iBitSet, ciVOTE_BIT_SET_0 + GlobalplayerBD_FM_2[iMyGBD].iVoteToUse)
//									CLEAN_UP_VOTE_VARAIABLES(iMyGBD)
//								ENDIF
//							ENDIF
//						ENDIF
//						#IF IS_DEBUG_BUILD
//							//IF (GET_GAME_TIMER() % 2000) < 75
//								//PRINTSTRING("...BWW MP_VOTE_SYSTEM - This bit is set - GlobalplayerBD_FM_2[].iBitSet, biVoteHasPassed - WAITING TO BE ON MISSION")PRINTNL()
//							//ENDIF	
//						#ENDIF	
//					ENDIF
				ELSE
					//PRINTSTRING("...BWW MP_VOTE_SYSTEM - GlobalplayerBD_FM_2[iMyGBD].iVoteToUse = -1...  Resetting the vote set up variable")PRINTNL()
					CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteSetUp)
					IF IS_BIT_SET(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteIsGoinOn)
						CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteIsGoinOn)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteIsGoinOn)
				CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteIsGoinOn)
			ENDIF
	//		IF IS_BIT_SET(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biILeftTheArea)
				//If I don't want to vote any more for some reason then clean every thing up
	//			CLEAN_UP_VOTE_VARAIABLES(iMyGBD)
	//		ENDIF
		ENDIF
	ENDIF
	DISABLE_VOTE_THIS_FRAME(FALSE)

ENDPROC

PROC SERVER_LOOP_VOTE_CHECKS(PLAYER_INDEX piPlayer, INT iParticipant, MP_SERVER_VOTE_STRUCT &mpServerVoteStruct , LOCAL_VOTE_STRUCT &sVoteStruct)
	INT iPlayerGBD = NATIVE_TO_INT(piPlayer)
	INT i
	INT iVoteToUse
	
	IF IS_BIT_SET(GlobalplayerBD_FM_2[iPlayerGBD].iBitSet, biILeftTheArea)
		IF NOT IS_BIT_SET(mpServerVoteStruct.iLeftAreaBitSet, iParticipant)
			SET_BIT(mpServerVoteStruct.iLeftAreaBitSet, iParticipant)
			//SET_BIT(mpServerVoteStruct.iBitSet, biReCheckVote)
			mpServerVoteStruct.bReCheckVote = TRUE
		ENDIF
	ELSE
		IF IS_BIT_SET(mpServerVoteStruct.iLeftAreaBitSet, iParticipant)
			CLEAR_BIT(mpServerVoteStruct.iLeftAreaBitSet, iParticipant)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM_2[iPlayerGBD].iBitSet, biIwantToVote)
		IF NOT IS_BIT_SET(mpServerVoteStruct.iEnteredBitSet, iParticipant)
			SET_BIT(mpServerVoteStruct.iEnteredBitSet, iParticipant)
			//SET_BIT(mpServerVoteStruct.iBitSet, biReCheckVote)
			mpServerVoteStruct.bReCheckVote = TRUE
		ENDIF
	ELSE
		IF IS_BIT_SET(mpServerVoteStruct.iEnteredBitSet, iParticipant)
			CLEAR_BIT(mpServerVoteStruct.iEnteredBitSet, iParticipant)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM_2[iPlayerGBD].iBitSet, biVoteHasPassed)
		IF NOT IS_BIT_SET(mpServerVoteStruct.iVotedBitSet, iParticipant)
			SET_BIT(mpServerVoteStruct.iVotedBitSet, iParticipant)
			//SET_BIT(mpServerVoteStruct.iBitSet, biReCheckVote)
			mpServerVoteStruct.bReCheckVote = TRUE
		ENDIF
	ELSE
		IF IS_BIT_SET(mpServerVoteStruct.iVotedBitSet, iParticipant)
			CLEAR_BIT(mpServerVoteStruct.iVotedBitSet, iParticipant)
		ENDIF
	ENDIF
	//BOOL bSetUpMultiVote = FALSE
	//Deal with the voting	
	IF IS_BIT_SET(GlobalplayerBD_FM_2[iPlayerGBD].iBitSet, biIwantToVote)
		IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[iPlayerGBD].iBitSet, biVoteSetUp)
		//AND NOT IS_BIT_SET(GlobalplayerBD_FM_2[iPlayerGBD].iBitSet, biVoteHasPassed)	
			iVoteToUse = -1
			//Find out if there is not a vote going on for the mission that they want to play
			FOR i = 0 TO (NUM_NETWORK_REAL_PLAYERS() -1)
				IF mpServerVoteStruct.missionVote[i] 		= GlobalplayerBD_FM_2[iPlayerGBD].missionToStart	
				AND mpServerVoteStruct.missionVariation[i] 	= GlobalplayerBD_FM_2[iPlayerGBD].missionVariation	
				AND CAN_I_VOTE_WITH_THIS_TUT_SESSION(mpServerVoteStruct, iPlayerGBD, i)
					IF (mpServerVoteStruct.iNumberInVote[i] 	< GlobalplayerBD_FM_2[iPlayerGBD].iVoteMaximum)
					OR (mpServerVoteStruct.iNumberInVote[i] = 1 AND GlobalplayerBD_FM_2[iPlayerGBD].iVoteMaximum = 1)
						#IF IS_DEBUG_BUILD
							IF GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))) != GlobalplayerBD_FM_2[iPlayerGBD].iTeam
								SCRIPT_ASSERT("GET_PLAYER_TEAM(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))) != GlobalplayerBD_FM_2[iPlayerGBD].iTeam")										
							ENDIF 
						#ENDIF
						iVoteToUse = i 
						//PRINTSTRING("...BWW MP_VOTE_SYSTEM - iParticipant")PRINTINT(iParticipant)
						//PRINTSTRING(" Found A vote to use -")PRINTINT(iVoteToUse)PRINTNL()
						i = 99
						CLEAR_BIT(mpServerVoteStruct.iBitset_KickPlayerOutCorona, iPlayerGBD)
					ELSE
						//This vote is full get out of here
						IF NOT IS_BIT_SET(mpServerVoteStruct.iBitset_KickPlayerOutCorona, iPlayerGBD)
							SET_BIT(mpServerVoteStruct.iBitset_KickPlayerOutCorona, iPlayerGBD)
							//PRINTLN("SET_BIT(mpServerVoteStruct.iBitset_KickPlayerOutCorona, ", iPlayerGBD, ")")
						ENDIF
						EXIT
//						bSetUpMultiVote = TRUE
//						PRINTLN("mpServerVoteStruct.iNumberInVote[", i, "] = ", mpServerVoteStruct.iNumberInVote[i])
//						PRINTLN("GlobalplayerBD_FM_2[", iPlayerGBD, "].iVoteMaximum    = ", GlobalplayerBD_FM_2[iPlayerGBD].iVoteMaximum)
					ENDIF
				ENDIF
			ENDFOR
			//If there is no vote then set one up
			IF iVoteToUse = -1
				//Find out what vote to use
				FOR i = 0 TO (NUM_NETWORK_REAL_PLAYERS() -1)
					IF mpServerVoteStruct.iVoteID[i] = 0
						iVoteToUse = i 
						//PRINTSTRING("...BWW MP_VOTE_SYSTEM - Found a vote to use [")PRINTINT(iVoteToUse)PRINTSTRING("]")PRINTNL()
						i = 99
					ENDIF
				ENDFOR
				//If there are no votes avaliable then give a script assert
				IF iVoteToUse = -1
					#IF IS_DEBUG_BUILD
						//PRINTSTRING("...BWW MP_VOTE_SYSTEM - There are no vote slots avaliable - Bug to BObby Wright")PRINTNL()
						SCRIPT_ASSERT("...BWW MP_VOTE_SYSTEM - There are no vote slots avaliable - Bug to BObby Wright")
					#ENDIF							
				ELSE
					//A vote has been found, set it up
					mpServerVoteStruct.iNumberInVote[iVoteToUse] = 1
					#IF IS_DEBUG_BUILD
						IF sVoteStruct.bAllowVotesWithOnePlayer
							mpServerVoteStruct.iNumberInVote[iVoteToUse] = 1
						///	PRINTSTRING("...BWW MP_VOTE_SYSTEM -  DEBUG NUMBER OVER RIDE")
						ENDIF
					#ENDIF
					IF START_A_NEW_SERVER_MP_VOTE(mpServerVoteStruct.iVoteID[iVoteToUse] #IF IS_DEBUG_BUILD, iVoteToUse #ENDIF)	//mpServerVoteStruct.iNumberInVote[iVoteToUse] ,
						SET_BIT(mpServerVoteStruct.iBitSet, ciVOTE_BIT_SET_0 +iVoteToUse)
						sVoteStruct.iVoteCleanUpTimer[iVoteToUse] = 0
						mpServerVoteStruct.missionVote[iVoteToUse] 			= GlobalplayerBD_FM_2[iPlayerGBD].missionToStart	
						mpServerVoteStruct.missionVariation[iVoteToUse] 	= GlobalplayerBD_FM_2[iPlayerGBD].missionVariation
						mpServerVoteStruct.iVoteTut[iVoteToUse]				= GlobalplayerBD_FM_2[iPlayerGBD].iFmTutorialSession
						PRINTLN("...BWW MP_VOTE_SYSTEM - mpServerVoteStruct.iVoteTut[", iVoteToUse, "] = ", mpServerVoteStruct.iVoteTut[iVoteToUse])

						CLEAR_BIT(mpServerVoteStruct.iBitset_IsMultiVote, iVoteToUse)
					ENDIF	
				ENDIF	
			ELSE
				#IF IS_DEBUG_BUILD
					IF (GET_GAME_TIMER() % 1000) < 75
						PRINTSTRING("...BWW MP_VOTE_SYSTEM - Vote is already set up")PRINTNL()
					ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(mpServerVoteStruct.iBitset_KickPlayerOutCorona, iPlayerGBD)
			PRINTLN("CLEAR_BIT(mpServerVoteStruct.iBitset_KickPlayerOutCorona, ", iPlayerGBD, ")")
			CLEAR_BIT(mpServerVoteStruct.iBitset_KickPlayerOutCorona, iPlayerGBD)
		ENDIF
	ENDIF
ENDPROC


//Maintains the participant and server checks
INT iStaggeredVoteCheckNonLooped
PROC MAINTAIN_SERVER_VOTE_CHECKS_LOOPED(MP_SERVER_VOTE_STRUCT &mpServerVoteStruct, LOCAL_VOTE_STRUCT &sVoteStruct)

//Deal with host stuff here
	INT iParticipant	
	INT i
	//INT iVoteToUse
	
	//Pre Player vote checks
	sVoteStruct.iReCheckVoteTimer = 0
	
	//Check through the votes to see if they are ok.
	//FOR i = 0 TO (NUM_NETWORK_REAL_PLAYERS() -1)
	i = iStaggeredVoteCheckNonLooped	
	IF mpServerVoteStruct.missionVote[i] != ciNULL_MISSION
			
		sVoteStruct.iNumberThatWantToVote[i]= 0
		
		//PRINTLN("iStaggeredVoteCheckNonLooped = ", iStaggeredVoteCheckNonLooped)
		
		//ENDFOR
		//Recount the number of people that want to play
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
				//If the player is active then check to see the status of their flow flags.
				PLAYER_INDEX 	piPlayer	= NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(piPlayer)
					INT iPlayerGBD = NATIVE_TO_INT(piPlayer)
					//FOR i = 0 TO (NUM_NETWORK_REAL_PLAYERS() -1)
					i = iStaggeredVoteCheckNonLooped
					IF i = GlobalplayerBD_FM_2[iPlayerGBD].iVoteToUse
						sVoteStruct.iNumberThatWantToVote[i]++
					ENDIF
					//ENDFOR
				ENDIF
			ENDIF
		ENDREPEAT
			
		//More people want to vote than the current set up vote, Clean it up and make a new one
		IF sVoteStruct.iNumberThatWantToVote[i] = 0
			IF IS_BIT_SET(mpServerVoteStruct.iBitSet, ciVOTE_BIT_SET_0 + i)
				IF sVoteStruct.iVoteCleanUpTimer[i] = 0
					sVoteStruct.iVoteCleanUpTimer[i] = GET_GAME_TIMER() + 1800
				ELIF GET_GAME_TIMER() > sVoteStruct.iVoteCleanUpTimer[i]
					sVoteStruct.iVoteCleanUpTimer[i] = 0
					CLEANUP_MP_FLOW_VOTE(mpServerVoteStruct.iVoteID[i] #IF IS_DEBUG_BUILD, i #ENDIF)
					PRINTSTRING("...not enough people")PRINTNL()		
					CLEAR_BIT(mpServerVoteStruct.iBitSet, ciVOTE_BIT_SET_0 + i)
					mpServerVoteStruct.missionVote[i] 	 = ciNULL_MISSION
					mpServerVoteStruct.missionVariation[i] = ciNULL_VARIATION
					mpServerVoteStruct.iVoteTut[i]		= -1	
					CLEAR_BIT(mpServerVoteStruct.iBitSet2, ciVOTEPASSED_BIT_SET_0 + i)
				ENDIF
			ENDIF
		
		//If the number that want to vote is less than what the vote has been set up for then clean it up
		ELIF mpServerVoteStruct.iNumberInVote[i] < sVoteStruct.iNumberThatWantToVote[i]
			IF IS_BIT_SET(mpServerVoteStruct.iBitSet, ciVOTE_BIT_SET_0 + i)
				CLEANUP_MP_FLOW_VOTE(mpServerVoteStruct.iVoteID[i] #IF IS_DEBUG_BUILD, i #ENDIF)
				sVoteStruct.iVoteCleanUpTimer[i] = 0
				PRINTSTRING(" - As More people want to vote than the vote has been set up for")PRINTNL()
				CLEAR_BIT(mpServerVoteStruct.iBitSet, ciVOTE_BIT_SET_0 + i)
			ENDIF
			mpServerVoteStruct.iNumberInVote[i] = sVoteStruct.iNumberThatWantToVote[i]				
			
		//If the number that want to vote is more than what the vote has been set up for then clean up the vote
		ELIF mpServerVoteStruct.iNumberInVote[i] > sVoteStruct.iNumberThatWantToVote[i]
			IF IS_BIT_SET(mpServerVoteStruct.iBitSet, ciVOTE_BIT_SET_0 + i)
				CLEANUP_MP_FLOW_VOTE(mpServerVoteStruct.iVoteID[i] #IF IS_DEBUG_BUILD, i #ENDIF)
				sVoteStruct.iVoteCleanUpTimer[i] = 0
				PRINTSTRING("...Less people are now in the vote than it was originally set up for")PRINTNL()
				CLEAR_BIT(mpServerVoteStruct.iBitSet, ciVOTE_BIT_SET_0 + i)
			ENDIF
			mpServerVoteStruct.iNumberInVote[i] = sVoteStruct.iNumberThatWantToVote[i]
		ENDIF	
		
		//If the number that want to vote is more than 1 then 
		IF mpServerVoteStruct.iNumberInVote[i] = sVoteStruct.iNumberThatWantToVote[i]
		AND sVoteStruct.iNumberThatWantToVote[i] != 0
			//And the vote has not been set up and the vote is for a valid mission
			IF NOT IS_BIT_SET(mpServerVoteStruct.iBitSet, ciVOTE_BIT_SET_0 + i)				
				IF START_A_NEW_SERVER_MP_VOTE(mpServerVoteStruct.iVoteID[i] #IF IS_DEBUG_BUILD, i #ENDIF)	//mpServerVoteStruct.iNumberInVote[i] ,
					SET_BIT(mpServerVoteStruct.iBitSet, ciVOTE_BIT_SET_0 + i)
					sVoteStruct.iVoteCleanUpTimer[i] = 0
					
					//PRINTSTRING("... the number of people in the vote had changed") PRINTNL()		
					//PRINTSTRING("...The criteria = ")PRINTNL()
					//PRINTSTRING("mpServerVoteStruct.missionVote[")PRINTINT(i)PRINTSTRING("] = ")	
					PRINTINT(mpServerVoteStruct.missionVote[i])PRINTNL()
					//PRINTSTRING("mpServerVoteStruct.missionVariation[")PRINTINT(i)PRINTSTRING("] = ")	
					PRINTINT(mpServerVoteStruct.missionVariation[i])PRINTNL()
					//PRINTSTRING("mpServerVoteStruct.iVoteGang[")PRINTINT(i)PRINTSTRING("] = ")	
					//PRINTINT(mpServerVoteStruct.iVoteGang[i])PRINTNL()
					//PRINTSTRING("mpServerVoteStruct.iVoteTut[")PRINTINT(i)PRINTSTRING("] = ")	
					PRINTINT(mpServerVoteStruct.iVoteTut[i])PRINTNL()
					
					
					IF mpServerVoteStruct.iNumberInVote[i] >= GET_VOTE_MIN_START_NUMBER(mpServerVoteStruct.missionVote[i])
						//PRINTSTRING("...BWW  MP_VOTE_SYSTEM - mpServerVoteStruct.iNumberInVote[")PRINTINT(i)PRINTSTRING("] >= GET_VOTE_MIN_START_NUMBER")PRINTNL()	
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(mpServerVoteStruct.iBitSet, ciVOTE_BIT_SET_0 + i)
			IF mpServerVoteStruct.iNumberInVote[i] = sVoteStruct.iNumberThatWantToVote[i]
			AND sVoteStruct.iNumberThatWantToVote[i] != 0					
				IF mpServerVoteStruct.iNumberInVote[i] >= GET_VOTE_MIN_START_NUMBER(mpServerVoteStruct.missionVote[i])
					//And the vote has not been set up and the vote is for a valid mission
					//PRINTSTRING(" - mpServerVoteStruct.iNumberInVote[i] >= in Vote is false -") PRINTINT(i)PRINTNL()	
					IF START_A_NEW_SERVER_MP_VOTE(mpServerVoteStruct.iVoteID[i] #IF IS_DEBUG_BUILD, i #ENDIF)	//mpServerVoteStruct.iNumberInVote[i] ,
						SET_BIT(mpServerVoteStruct.iBitSet, ciVOTE_BIT_SET_0 + i)
						sVoteStruct.iVoteCleanUpTimer[i] = 0
						#IF IS_DEBUG_BUILD
							//PRINTSTRING(" - With Correct number of players =") PRINTINT(sVoteStruct.iNumberThatWantToVote[i])PRINTNL()		
							//PRINTSTRING("...The criteria = ")PRINTNL()
							//PRINTSTRING("mpServerVoteStruct.missionVote[")PRINTINT(i)PRINTSTRING("] = ")	
							PRINTINT(mpServerVoteStruct.missionVote[i])PRINTNL()
							//PRINTSTRING("mpServerVoteStruct.missionVariation[")PRINTINT(i)PRINTSTRING("] = ")	
							PRINTINT(mpServerVoteStruct.missionVariation[i])PRINTNL()
							//PRINTSTRING("mpServerVoteStruct.iVoteGang[")PRINTINT(i)PRINTSTRING("] = ")	
							//PRINTINT(mpServerVoteStruct.iVoteGang[i])PRINTNL()
							//PRINTSTRING("mpServerVoteStruct.iVoteTut[")PRINTINT(i)PRINTSTRING("] = ")	
							PRINTINT(mpServerVoteStruct.iVoteTut[i])PRINTNL()
							
						#ENDIF
					ENDIF							
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_BIT_SET(mpServerVoteStruct.iBitSet, ciVOTE_BIT_SET_0 + i)
			CLEANUP_MP_FLOW_VOTE(mpServerVoteStruct.iVoteID[i] #IF IS_DEBUG_BUILD, i #ENDIF)
			sVoteStruct.iVoteCleanUpTimer[i] = 0
			PRINTSTRING("...the vote is for eNull_Mission")PRINTNL()
			CLEAR_BIT(mpServerVoteStruct.iBitSet, ciVOTE_BIT_SET_0 			+ i)
			CLEAR_BIT(mpServerVoteStruct.iBitSet2, ciVOTEPASSED_BIT_SET_0 	+ i)
			mpServerVoteStruct.missionVote[i] 		= ciNULL_MISSION
			mpServerVoteStruct.missionVariation[i] 	= ciNULL_VARIATION
			//mpServerVoteStruct.iVoteGang[i]			= -1
			mpServerVoteStruct.iVoteTut[i]			= -1
			
		ENDIF
	ENDIF
	//ENDFOR	
	
	iStaggeredVoteCheckNonLooped += 1
	IF (iStaggeredVoteCheckNonLooped = NUM_NETWORK_REAL_PLAYERS())
		iStaggeredVoteCheckNonLooped = 0
	ENDIF
	
ENDPROC



