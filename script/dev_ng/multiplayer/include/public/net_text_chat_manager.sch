USING "rage_builtins.sch"
USING "globals.sch"
USING "cellphone_public.sch"
USING "net_sctv_common.sch"

PROC MANTAIN_TEXT_CHAT_ACTIVATION()
	BOOL bChatShouldBeDisabled
	IF IS_PC_VERSION()
		// Switch
		IF IS_PLAYER_PED_SWITCH_IN_PROGRESS()
			bChatShouldBeDisabled = TRUE
		ENDIF
		
		// Pause Menu
		IF IS_PAUSE_MENU_ACTIVE()
		AND NOT IS_PLAYER_IN_CORONA()
			bChatShouldBeDisabled = TRUE
		ENDIF
		
		// Cellphone / Internet - Covered by Cellphone check?
		IF IS_PHONE_ONSCREEN()
			bChatShouldBeDisabled = TRUE
		ENDIF
		
		// Celebration Screens
		IF IS_PLAYER_RUNNING_ANY_TYPE_OF_POST_MISSION_SCENE(PLAYER_ID())
			bChatShouldBeDisabled = TRUE
		ENDIF
		
		// MP Transitions
		IF IS_TRANSITION_ACTIVE()
			bChatShouldBeDisabled = TRUE
		ENDIF
		
//		// Shop Menus / Apartment / Garage / Prostitute Menu // No longer needed Bug #2336482
//		IF IS_CUSTOM_MENU_ON_SCREEN()
//			bChatShouldBeDisabled = TRUE
//		ENDIF
		
		// Interaction Menu
		IF MPGlobalsInteractions.bAnyInteractionIsPlaying
			bChatShouldBeDisabled = TRUE
		ENDIF
		
		// This should kill the chat box on celebration screens.
		IF THEFEED_IS_PAUSED()
		AND !IS_LOCAL_PLAYER_ON_END_OF_JOB_VOTE_SCREEN() // But not on the NJVS
		AND !IS_CUSTOM_MENU_ON_SCREEN()
		AND !IS_PLAYER_IN_CORONA()
			bChatShouldBeDisabled = TRUE
		ENDIF

		// Radio - Code?
		// Weapon Wheel - Code?
		// Sniper Scope - Code?
		
		IF bChatShouldBeDisabled 
			CPRINTLN(DEBUG_PAUSE_MENU, "MP_TEXT_CHAT_DISABLE")
			CLOSE_MP_TEXT_CHAT()
		ENDIF
		
		MP_TEXT_CHAT_DISABLE(bChatShouldBeDisabled) // Will need a global to track this to avoid spamming every frame
	ENDIF
ENDPROC

