//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_BASE.sch																						//
// Description: Header file containing look ups tables for ped data.													//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		26/02/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "net_peds_vars.sch"
USING "rc_helper_functions.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ ENUMS ╞═════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

ENUM PED_INTERFACE_PROCEDURES
	E_SHOULD_PED_SCRIPT_LAUNCH,
	E_IS_PARENT_A_SIMPLE_INTERIOR,
	E_GET_LOCAL_PED_TOTAL,
	E_GET_NETWORK_PED_TOTAL,
	E_GET_SERVER_PED_LAYOUT_TOTAL,
	E_GET_SERVER_PED_LAYOUT,
	E_GET_SERVER_PED_LEVEL,
	E_GET_PED_LOCAL_COORDS_BASE_POSITION,
	E_GET_PED_LOCAL_HEADING_BASE_HEADING,
	E_SET_PED_DATA,
	E_SET_PED_SERVER_DATA,
	E_GET_PED_ANIM_DATA,
	E_GET_PED_PROP_ANIM_DATA,
	E_GET_PED_PROP_WEAPON_DATA,
	E_IS_PLAYER_IN_PARENT_PROPERTY,
	E_DOES_ACTIVE_PED_TOTAL_CHANGE,
	E_GET_ACTIVE_PED_LEVEL_THRESHOLD,
	E_SET_LOCAL_PED_PROPERTIES,
	E_SET_NETWORK_PED_PROPERTIES,
	E_SET_PED_PROP_INDEXES,
	E_SET_PED_ALT_MOVEMENT,
	E_HAS_PED_BEEN_CREATED,
	E_CAN_PED_PLAY_SPEECH,
	E_SET_PED_SPEECH_DATA,
	E_GET_PED_SPEECH_TYPE,
	E_GET_PED_CONTROLLER_SPEECH,
	E_GET_PED_CONVO_DATA,
	E_GET_PED_CULLING_LOCATE_DATA,
	E_SET_PED_PATROL_DATA,
	E_SET_PED_HEAD_TRACKING_DATA,
	E_SET_PED_CHANGED_CAPSULE_SIZE_DATA,
	E_DOES_PED_LOCATION_USE_DANCING_PEDS,
	E_SET_PED_PARENT_DATA,
	E_SHOULD_HIDE_CUTSCENE_PED,
	E_CAN_PED_HEAD_TRACK,
	E_LOCATION_SPECIFIC_UPDATE
ENDENUM

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ FUNCTION POINTERS ╞═══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

TYPEDEF FUNC BOOL 			_T_SHOULD_PED_SCRIPT_LAUNCH				()
TYPEDEF FUNC BOOL 			_T_IS_PARENT_A_SIMPLE_INTERIOR			()
TYPEDEF FUNC INT			_T_GET_LOCAL_PED_TOTAL					(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
TYPEDEF FUNC INT			_T_GET_NETWORK_PED_TOTAL				(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
TYPEDEF FUNC INT			_T_GET_SERVER_PED_LAYOUT_TOTAL			()
TYPEDEF FUNC INT 			_T_GET_SERVER_PED_LAYOUT				()
TYPEDEF FUNC INT 			_T_GET_SERVER_PED_LEVEL					()
TYPEDEF FUNC VECTOR 		_T_GET_PED_LOCAL_COORDS_BASE_POSITION	(PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
TYPEDEF FUNC FLOAT			_T_GET_PED_LOCAL_HEADING_BASE_HEADING	(PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
TYPEDEF PROC 				_T_SET_PED_DATA							(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, INT iPed, INT iLayout, BOOL bSetPedArea = TRUE)
TYPEDEF PROC 				_T_SET_PED_SERVER_DATA					(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
TYPEDEF PROC 				_T_GET_PED_ANIM_DATA					(PED_ACTIVITIES eActivity, PED_ANIM_DATA &pedAnimData, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, INT iPedID = 0, PED_TRANSITION_STATES eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE, BOOL bPedTransition = FALSE)
TYPEDEF PROC 				_T_GET_PED_PROP_ANIM_DATA				(PED_ACTIVITIES eActivity, PED_PROP_ANIM_DATA &pedPropAnimData, INT iProp = 0, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bHeeledPed = FALSE)
TYPEDEF PROC 				_T_GET_PED_PROP_WEAPON_DATA				(PED_PROP_WEAPON_DATA &pedPropWeaponData, INT iPed)
TYPEDEF FUNC BOOL			_T_IS_PLAYER_IN_PARENT_PROPERTY			(PLAYER_INDEX playerID)
TYPEDEF FUNC BOOL			_T_DOES_ACTIVE_PED_TOTAL_CHANGE			()
TYPEDEF FUNC INT 			_T_GET_ACTIVE_PED_LEVEL_THRESHOLD		(INT iLevel)
TYPEDEF PROC 				_T_SET_LOCAL_PED_PROPERTIES				(PED_INDEX &PedID, INT iPed)
TYPEDEF PROC 				_T_SET_NETWORK_PED_PROPERTIES			(NETWORK_INDEX &NetworkPedID, INT &iPedDataBS[PEDS_DATA_BITSET_ARRAY_SIZE])
TYPEDEF PROC 				_T_SET_PED_PROP_INDEXES					(PED_INDEX &PedID, INT iPed, INT iLayout, PED_MODELS ePedModel)
TYPEDEF PROC 				_T_SET_PED_ALT_MOVEMENT					(PED_INDEX &PedID, INT iPed)
TYPEDEF FUNC BOOL			_T_HAS_PED_BEEN_CREATED					(PEDS_DATA &Data, INT iLevel)
TYPEDEF PROC				_T_SET_PED_SPEECH_DATA					(SPEECH_DATA &SpeechData, INT iLayout, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
TYPEDEF FUNC BOOL			_T_CAN_PED_PLAY_SPEECH					(PED_INDEX PedID, INT iPed, INT iLayout, PED_SPEECH ePedSpeech)
TYPEDEF FUNC BOOL			_T_CAN_PED_HEAD_TRACK					(PED_INDEX PedID, INT iPed, INT iLayout)
TYPEDEF FUNC PED_SPEECH		_T_GET_PED_SPEECH_TYPE					(INT iPed, PED_ACTIVITIES ePedActivity, INT iSpeech)
TYPEDEF FUNC PED_SPEECH 	_T_GET_PED_CONTROLLER_SPEECH			(PED_SPEECH &eCurrentSpeech, INT iPed, PED_ACTIVITIES ePedActivity)
TYPEDEF PROC				_T_GET_PED_CONVO_DATA					(PED_CONVO_DATA &convoData, INT iPed, PED_ACTIVITIES ePedActivity, PED_SPEECH ePedSpeech)
TYPEDEF PROC 				_T_GET_PED_CULLING_LOCATE_DATA			(PED_AREAS ePedArea, PED_CULLING_LOCATE_DATA &pedCullingLocateData)
TYPEDEF PROC 				_T_SET_PED_PATROL_DATA					(SEQUENCE_INDEX &SequenceIndex, INT iLayout, INT iArrayID, INT &iPatrolPedID[], BOOL &bShowProp[], INT iTask, INT &iMaxSequence, BOOL bNetworkPed = FALSE)
TYPEDEF PROC 				_T_SET_PED_HEAD_TRACKING_DATA			(HEAD_TRACKING_DATA &HeadtrackingData, INT iLayout, INT iArrayID, INT &iHeadTrackingPedID[], BOOL bNetworkPed = FALSE)
TYPEDEF PROC 				_T_SET_PED_CHANGED_CAPSULE_SIZE_DATA	(CHANGED_CAPSULE_SIZE_DATA &ChangedCapsuleSizeData, INT iLayout, INT iArrayID, INT &iChandedCapsuleSizePedID[], BOOL bNetworkPed = FALSE)
TYPEDEF FUNC BOOL			_T_DOES_PED_LOCATION_USE_DANCING_PEDS	()
TYPEDEF PROC 				_T_SET_PED_PARENT_DATA					(PARENT_PED_DATA &ParentPedData, INT iLayout, INT iArrayID, INT &iParentPedData[], BOOL bNetworkPed = FALSE)
TYPEDEF FUNC BOOL 			_T_SHOULD_HIDE_CUTSCENE_PED				(SCRIPT_PED_DATA &LocalData, INT iLayout, INT iPed)
TYPEDEF PROC                _T_LOCATION_SPECIFIC_UPDATE				(SCRIPT_PED_DATA &LocalData, SERVER_PED_DATA &ServerBD)
//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ INTERFACE ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

STRUCT PED_INTERFACE
	// Script Launching
	_T_SHOULD_PED_SCRIPT_LAUNCH				returnShouldPedScriptLaunch
	_T_IS_PARENT_A_SIMPLE_INTERIOR			returnIsParentASimpleInterior
	
	// Ped Data
	_T_GET_LOCAL_PED_TOTAL					returnGetLocalPedTotal
	_T_GET_NETWORK_PED_TOTAL				returnGetNetworkPedTotal
	_T_GET_SERVER_PED_LAYOUT_TOTAL			returnGetServerPedLayoutTotal
	_T_GET_SERVER_PED_LAYOUT				returnGetServerPedLayout
	_T_GET_SERVER_PED_LEVEL					returnGetServerPedLevel
	_T_GET_PED_LOCAL_COORDS_BASE_POSITION	returnGetPedLocalCoordsBasePosition
	_T_GET_PED_LOCAL_HEADING_BASE_HEADING	returnGetPedLocalHeadingBaseHeading
	_T_SET_PED_DATA 						setPedData
	_T_SET_PED_SERVER_DATA					setPedServerData
	_T_GET_PED_ANIM_DATA					getPedAnimData
	_T_GET_PED_PROP_ANIM_DATA				getPedPropAnimData
	_T_GET_PED_PROP_WEAPON_DATA				getPedPropWeaponData
	_T_IS_PLAYER_IN_PARENT_PROPERTY			returnIsPlayerInParentProperty
	_T_DOES_ACTIVE_PED_TOTAL_CHANGE			returnDoesActivePedTotalChange
	_T_GET_ACTIVE_PED_LEVEL_THRESHOLD		getActivePedLevelThreshold
	
	// Ped Creation
	_T_SET_LOCAL_PED_PROPERTIES				setLocalPedProperties
	_T_SET_NETWORK_PED_PROPERTIES			setNetworkPedProperties
	_T_SET_PED_PROP_INDEXES					setPedPropIndexes
	_T_SET_PED_ALT_MOVEMENT					setPedAltMovement
	_T_HAS_PED_BEEN_CREATED					returnHasPedBeenCreated
	
	// Ped Speech
	_T_SET_PED_SPEECH_DATA					setPedSpeechData
	_T_CAN_PED_PLAY_SPEECH					returnCanPedPlaySpeech
	_T_GET_PED_SPEECH_TYPE					returnGetPedSpeechType
	_T_GET_PED_CONTROLLER_SPEECH			returnGetPedControllerSpeech
	_T_GET_PED_CONVO_DATA					getPedConvoData
	
	// Ped Culling
	_T_GET_PED_CULLING_LOCATE_DATA			getPedCullingLocateData
	
	// Ped Patrol
	_T_SET_PED_PATROL_DATA					setPedPatrolData
	
	// Ped Head Tracking
	_T_SET_PED_HEAD_TRACKING_DATA			setPedHeadTrackingData
	_T_CAN_PED_HEAD_TRACK					returnCanPedHeadTrack
	
	// Ped Changed Capsule Size
	_T_SET_PED_CHANGED_CAPSULE_SIZE_DATA	setPedChangedCapsuleSizeData
	
	// Dancing Data
	_T_DOES_PED_LOCATION_USE_DANCING_PEDS	returnDoesPedLocationUseDancingPeds
	
	// Parent Ped Data
	_T_SET_PED_PARENT_DATA					setPedParentData
	
	// Cutscene Peds
	_T_SHOULD_HIDE_CUTSCENE_PED				returnShouldHideCutscenePed	
	
	_T_LOCATION_SPECIFIC_UPDATE				locationSpecificUpdate
ENDSTRUCT

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ CLEAR DATA ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Clears a ped bitset - all bits.
/// PARAMS:
///    iBitSet - Bitset array to clear.
PROC CLEAR_PEDS_BITSET(INT &iBitSet[])
	INT i
	REPEAT COUNT_OF(iBitSet) i
		iBitSet[i] = 0
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Resets a peds packed clothing component drawable and texture vars to -1.
/// PARAMS:
///    iPackedDrawable - Packed drawable array which holds all the peds clothing component drawables.
///    iPackedTexture - Packed texture array which holds all the peds clothing component textures.
PROC RESET_PED_PACKED_CLOTHING_COMPONENT_VARS(INT &iPackedDrawable[MAX_NUM_PED_CLOTHING_COMPONENT_VARS], INT &iPackedTexture[MAX_NUM_PED_CLOTHING_COMPONENT_VARS])
	
	INT iVar
	REPEAT MAX_NUM_PED_CLOTHING_COMPONENT_VARS iVar
		iPackedDrawable[iVar] = -1
		iPackedTexture[iVar] = -1
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Resets the PEDS_DATA struct.
///    Note: Not all data is cleared here! Some has to persist.
/// PARAMS:
///    Data - Struct to reset.
PROC RESET_PED_DATA(PEDS_DATA &Data)
	CLEAR_PEDS_BITSET(Data.iBS)
	CLEAR_PEDS_BITSET(Data.iCustomTaskBS)
	RESET_PED_PACKED_CLOTHING_COMPONENT_VARS(Data.iPackedDrawable, Data.iPackedTexture)
	Data.iActivity										= 0
	Data.iLevel 										= 0
	Data.vRotation										= <<0.0, 0.0, -1.0>>
	
	Data.animData.iSyncSceneID 							= -1
	Data.animData.iPrevClip 							= 0
	Data.animData.iMaxClips 							= 0
	Data.animData.iLoopingClips 						= 0
	Data.animData.iTaskAnimDurationMS					= -1
	Data.animData.iTaskAnimStartTimeMS					= -1
	Data.animData.iCurrentClip							= 0
	Data.animData.iSumAnimDurationMS					= 0
ENDPROC

/// PURPOSE:
///    Resets the PATROL_DATA struct.
/// PARAMS:
///    PatrolData - Struct to reset.
PROC RESET_PED_PATROL_DATA(PATROL_DATA &PatrolData)
	PATROL_DATA blankPatrolData
	PatrolData = blankPatrolData
ENDPROC

/// PURPOSE:
///    Resets the HEAD_TRACKING_DATA struct.
/// PARAMS:
///    HeadTrackingData - Struct to reset.
PROC RESET_PED_HEAD_TRACKING_DATA(HEAD_TRACKING_DATA &HeadTrackingData)
	HEAD_TRACKING_DATA blankHeadTrackingData
	HeadTrackingData = blankHeadTrackingData
ENDPROC

/// PURPOSE:
///    Resets the SPEECH_DATA struct.
/// PARAMS:
///    SpeechData - Struct to reset.
PROC RESET_PED_SPEECH_DATA(SPEECH_DATA &SpeechData)
	SPEECH_DATA blankSpeechData
	SpeechData = blankSpeechData
ENDPROC

/// PURPOSE:
///    Resets the PARENT_PED_DATA struct.
/// PARAMS:
///    ParentPedData - Struct to reset.
PROC RESET_PED_PARENT_DATA(PARENT_PED_DATA &ParentPedData)
	PARENT_PED_DATA blankParentPedData
	ParentPedData = blankParentPedData
ENDPROC

/// PURPOSE:
///    Resets the PED_CONVO_DATA struct.
/// PARAMS:
///    PedConvoData - Struct to reset.
PROC RESET_PED_CONVO_DATA(PED_CONVO_DATA &PedConvoData)
	PedConvoData.iSpeakerID								= -1
	PedConvoData.bPlayAsConvo							= FALSE
	PedConvoData.bConvoRequiresAnimation				= FALSE
	PedConvoData.bAddChildPedsToConvo					= FALSE
	PedConvoData.sRootName								= ""
	PedConvoData.sCharacterVoice						= ""
	PedConvoData.sSubtitleTextBlock						= ""
ENDPROC

/// PURPOSE:
///    Resets the PED_VFX_DATA struct.
/// PARAMS:
///    MainData - Struct to reset.
PROC RESET_PED_MAIN_VFX_DATA(PED_VFX_DATA &MainData)
	MainData.sPTFXAssetName								= ""
	MainData.sPTFXEffectName							= ""
	MainData.sPTFXSoundName								= ""
	MainData.sPTFXSoundSetName							= ""
	MainData.bPTFXSoundOverNetwork						= FALSE
	MainData.iPropID									= -1
	MainData.iPFTXSoundNetworkRange						= 0
	MainData.fScale										= 1.0
	MainData.vCoordOffset								= <<0.0, 0.0, 0.0>>
	MainData.vRotationOffset							= <<0.0, 0.0, 0.0>>
	MainData.eBoneTag									= BONETAG_NULL
ENDPROC

/// PURPOSE:
///    Resets the PED_VFX_LOOPING_DATA struct.
/// PARAMS:
///    VFXLoopingData - Struct to reset.
PROC RESET_PED_LOOPING_VFX_DATA(PED_VFX_LOOPING_DATA &VFXLoopingData)
	VFXLoopingData.sAnimEventEndName					= ""
	VFXLoopingData.sAnimEventStartName					= ""
	VFXLoopingData.bLoopForever							= FALSE
	VFXLoopingData.bUseCustomPhase						= FALSE
	RESET_PED_MAIN_VFX_DATA(VFXLoopingData.MainData)
ENDPROC

/// PURPOSE:
///    Resets the PED_VFX_ONE_SHOT_DATA struct.
/// PARAMS:
///    VFXOneShotData - Struct to reset.
PROC RESET_PED_ONE_SHOT_VFX_DATA(PED_VFX_ONE_SHOT_DATA &VFXOneShotData)
	INT iOneShotTrigger = 0
	REPEAT MAX_NUM_PED_VFX_ONE_SHOT_TRIGGERS iOneShotTrigger
		VFXOneShotData.sAnimEvent[iOneShotTrigger] 		= ""
	ENDREPEAT
	VFXOneShotData.bIgnoreAnimEventPhases 				= FALSE
	VFXOneShotData.bIgnoreAnimEvents	 				= FALSE
	RESET_PED_MAIN_VFX_DATA(VFXOneShotData.MainData)
ENDPROC

/// PURPOSE:
///    Resets the PED_ANIM_DATA Struct. 
/// PARAMS:
///    pedAnimData - Struct to reset.
PROC RESET_PED_ANIM_DATA(PED_ANIM_DATA &pedAnimData)
	PED_ANIM_DATA blankPedAnimData
	pedAnimData = blankPedAnimData
	// Ensure bitsets are cleared and strings are null
	CLEAR_PEDS_BITSET(pedAnimData.iBS)
	pedAnimData.sAnimDict								= ""
	pedAnimData.sAnimClip								= ""
	pedAnimData.sFacialAnimClip							= ""
	pedAnimData.sFacialAnimClip							= ""
	pedAnimData.sAltBaseAnimClip						= ""
	pedAnimData.bVFXAnimClip							= FALSE
	RESET_PED_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData)
	RESET_PED_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData)
ENDPROC

/// PURPOSE:
///    Resets the PED_PROP_ANIM_DATA Struct. 
///	   Note: Not all data is cleared here! Some have to persist.
/// PARAMS:
///    pedPropAnimData - Struct to reset.
PROC RESET_PED_PROP_ANIM_DATA(PED_PROP_ANIM_DATA &pedPropAnimData)
	pedPropAnimData.sPropAnimDict						= ""
	pedPropAnimData.sPropAnimClip						= ""
	pedPropAnimData.iFlags								= 0
	pedPropAnimData.fBlendInDelta						= INSTANT_BLEND_IN
	pedPropAnimData.fBlendOutDelta						= INSTANT_BLEND_OUT
	pedPropAnimData.fMoverBlendInDelta					= INSTANT_BLEND_IN
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════╡ TRANSFORM FUNCTIONS ╞══════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Transforms local coords into world coords given a base position and heading.
/// PARAMS:
///    vBasePos - Base position to offset from.
///    fBaseHeading - Base heading to offset from.
///    vLocalCoords - The local coords to transform into world coords.
/// RETURNS: Transformed world coords as a VECTOR.
FUNC VECTOR TRANSFORM_LOCAL_COORDS_TO_WORLD_COORDS(VECTOR vBasePos, FLOAT fBaseHeading, VECTOR vLocalCoords)
	RETURN GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vBasePos, fBaseHeading, vLocalCoords)
ENDFUNC

/// PURPOSE:
///    Transforms a local heading into a world heading given a base heading.
/// PARAMS:
///    fBaseHeading - The base heading to offset from.
///    fLocalHeading - The local heading to transform into a world heading.
/// RETURNS: Transform world heading as a FLOAT.
FUNC FLOAT TRANSFORM_LOCAL_HEADING_TO_WORLD_HEADING(FLOAT fBaseHeading, FLOAT fLocalHeading)
	FLOAT fResult = fLocalHeading + fBaseHeading
	
	WHILE fResult < 0.0
		fResult += 360.0
	ENDWHILE
	WHILE fResult >= 360.0
		fResult -= 360.0
	ENDWHILE
	
	RETURN fResult
ENDFUNC

/// PURPOSE:
///    Transforms world coords into local coords given a base position and heading.
/// PARAMS:
///    vBasePos - Base position to offset from.
///    fBaseHeading - Base heading to offset from.
///    vWorldCoords - The world coords to transform into local coords.
/// RETURNS: Transformed local coords as a VECTOR.
FUNC VECTOR TRANSFORM_WORLD_COORDS_TO_LOCAL_COORDS(VECTOR vBasePos, FLOAT fBaseHeading, VECTOR vWorldCoords)
	VECTOR vResult = vWorldCoords - vBasePos
	RotateVec(vResult, <<0.0, 0.0, fBaseHeading * -1.0 >>)
	RETURN vResult
ENDFUNC

/// PURPOSE:
///    Transforms a world heading into a local heading given a base heading.
/// PARAMS:
///    fBaseHeading - The base heading to offset from.
///    fWorldHeading - The world heading to transform into a local heading.
/// RETURNS: Transform local heading as a FLOAT.
FUNC FLOAT TRANSFORM_WORLD_HEADING_TO_LOCAL_HEADING(FLOAT fBaseHeading, FLOAT fWorldHeading)
	FLOAT fDiff = fWorldHeading - fBaseHeading
	
	WHILE fDiff < 0.0
		fDiff += 360.0
	ENDWHILE
	WHILE fDiff >= 360.0
		fDiff -= 360.0
	ENDWHILE
	
	RETURN fDiff
ENDFUNC
#ENDIF	// FEATURE_HEIST_ISLAND
