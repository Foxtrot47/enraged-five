//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	net_arena_garage_cinematic_cam.sch							//
//		AUTHOR			:	Tom Turner													//
//		DESCRIPTION		:	The cinematic camera sequence used in the arena garage mod	//
//							shop.														//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_cinematic_cam_sequencer.sch"
USING "commands_pad.sch"


/// PURPOSE: 
///    Enum for arena garages camera behaviours,
///    used as an index for camBehaviours array
ENUM ARENA_GARAGE_CAM_BEHAVIOURS
	FIXED_CAMERA_MOD_AREA = 0,
	FIXED_CAMERA_MOD_AREA_TWO,
	LERP_CAMERA_MOD_AREA,
	LERP_CAMERA_MOD_AREA_TWO,
	ORBIT_CAMERA_MOD_AREA
ENDENUM


//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡    CAMERA BEHAVIOUR FUNC   ╞═══════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Sets up the camera behaviours for the mod cinematic area
/// PARAMS:
///    iBehaviourIndex - the index of the camera the sequencer needs
FUNC CINEMATIC_CAM_BEHAVIOUR_STRUCT GET_MOD_AREA_CAMERAS(INT iBehaviourIndex)
	
	ENTITY_INDEX playerPedId = PLAYER_PED_ID()
	CINEMATIC_CAM_BEHAVIOUR_STRUCT behaviour
	
	SWITCH INT_TO_ENUM(ARENA_GARAGE_CAM_BEHAVIOURS, iBehaviourIndex)
		CASE FIXED_CAMERA_MOD_AREA
		
			CINEMATIC_CAM_BEHAVIOUR__INIT(behaviour, <<220.8377, 5184.3389, -84.4050>>, <<-8.3189, 0.0000, 106.7705>>, 65, 5000)
			CB_ADD_CAMERA_SHAKE(behaviour, 1, "HAND_SHAKE")
			CB_ADD_LOOK_AT_ENTITY(behaviour, playerPedId)
			
		BREAK
		CASE FIXED_CAMERA_MOD_AREA_TWO
			
			CINEMATIC_CAM_BEHAVIOUR__INIT(behaviour, <<200.5144, 5168.3252, -84.1420>>, <<-17.0603, -0.0000, -26.2377>>, 65, 5000)
			CB_ADD_CAMERA_SHAKE(behaviour, 0.4, "HAND_SHAKE")
			CB_ADD_LOOK_AT_ENTITY(behaviour, playerPedId)
			
		BREAK
		CASE LERP_CAMERA_MOD_AREA
			
			CINEMATIC_CAM_BEHAVIOUR__INIT(behaviour, <<194.2320, 5173.9277, -84.1225>>, <<-15.9825, 0.0000, -92.5856>>, 65)
			CB_ADD_CAMERA_LERP(behaviour, <<194.2320, 5173.9277, -84.1225>>, <<-15.9825, 0.0000, -92.5856>>,
				<<194.2320, 5186.5435, -84.1225>>, <<-15.9825, 0.0000, -92.5856>>, 5000, GRAPH_TYPE_SLOW_IN_OUT, 1000)		
			CB_ADD_LOOK_AT_ENTITY(behaviour, playerPedId)
			CB_ADD_CAMERA_SHAKE(behaviour, 0.2, "HAND_SHAKE")
			
		BREAK
		CASE LERP_CAMERA_MOD_AREA_TWO
			
			CINEMATIC_CAM_BEHAVIOUR__INIT(behaviour, <<214.9737, 5171.6992, -89.1365>>, <<4.4310, 0.0000, 30.2130>>, 65)
			CB_ADD_CAMERA_LERP(behaviour, <<214.9737, 5171.6992, -89.1365>>, <<4.4310, 0.0000, 30.2130>>,
				<<209.2224, 5169.1694, -83.4219>>, <<-27.1282, -0.0000, 8.3028>>, 5000, GRAPH_TYPE_CUBIC_EASE_IN_OUT, 2000)		
			CB_ADD_CAMERA_SHAKE(behaviour, 0.2, "HAND_SHAKE")
			CB_ADD_LOOK_AT_ENTITY(behaviour, playerPedId)
			
		BREAK
		CASE ORBIT_CAMERA_MOD_AREA
		
			CINEMATIC_CAM_BEHAVIOUR__INIT(behaviour, <<205.4173, 5180.2554, -85.1631>>, <<-3.2330, -0.0000, 176.8633>>, 65)
			CB_ADD_ORBIT_ENTITY(behaviour, playerPedId, 9.5, 15, 18000, 1, 180, <<0, 0, 3>>)
			CB_ADD_LOOK_AT_ENTITY(behaviour, playerPedId)
		
		BREAK	
	ENDSWITCH
	
	RETURN behaviour
ENDFUNC


//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡    ENTRY POINT    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Runs the arena carmod cinematic camera.
/// PARAMS:
///    sArenaGarageCinematic - the cinematic instance
PROC UPDATE_ARENA_CARMOD_CINEMATIC_CAM(CINEMATIC_CAM_SEQUENCE_STRUCT &sInst)
	
	// Run through cinematic for arena car modding
	CINEMATIC_CAM_SEQUENCE__MAINTAIN(sInst, COUNT_OF(ARENA_GARAGE_CAM_BEHAVIOURS), &GET_MOD_AREA_CAMERAS)
	
	// Add profile marker if needed
	#IF IS_DEBUG_BUILD
	#IF SCRIPT_PROFILER_ACTIVE
	ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_ARENA_CINEMATIC_CAM")
	#ENDIF	
	#ENDIF

ENDPROC
