///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        net_test_drive_launching.sch																		///																			
///																													///				
/// Description:  Wrappers to start a test drive of any given vehicle.												///
///	Usage:																											///
///    1. Call SET_SELECTED_TEST_DRIVE_VEHICLE_INDEX first, passing in the vehicle you want to test drive.			///
///    2. Add a spawn point by calling ADD_TEST_DRIVE_VEHICLE_SPAWN_POINT (max 32 spawn points).					///
///    3. (Optional) Add a 'Return Test Vehicle' location by calling ADD_TEST_DRIVE_VEHICLE_RETURN_POINT.			///
///    4. Call SET_TEST_DRIVE_LAUNCH_SCRIPT to indicate where the test drive is being launched from.				///
///    5. Call START_TEST_DRIVE every frame to begin the test drive flow.											///
///																													///
/// Written by:  Joe Davis																							///		
/// Date:		04/04/2022																							///
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_test_drive_public.sch"

#IF FEATURE_DLC_1_2022

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ DEBUG ╞══════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING GET_TEST_DRIVE_STATE_AS_STRING(VEHICLE_TEST_DRIVE_STATE eState)
	SWITCH eState
		CASE VTDS_DEFAULT					RETURN "VTDS_DEFAULT"
		CASE VTDS_PLAY_CUTSCENE				RETURN "VTDS_PLAY_CUTSCENE"
		CASE VTDS_REQUEST_SCRIPT			RETURN "VTDS_REQUEST_SCRIPT"
		CASE VTDS_SUCCESS					RETURN "VTDS_SUCCESS"
	ENDSWITCH
	
	RETURN "NULL"
ENDFUNC

FUNC STRING GET_TEST_DRIVE_LAUNCH_SCRIPT_AS_STRING(VEHICLE_TEST_DRIVE_LAUNCH_SCRIPT eLaunchScript)
	SWITCH eLaunchScript
		CASE VTDLS_DEFAULT					RETURN "VTDLS_DEFAULT"
		CASE VTDLS_SIMEON_SHOWROOM			RETURN "VTDLS_SIMEON_SHOWROOM"
	ENDSWITCH
	
	RETURN "NULL"
ENDFUNC

#ENDIF

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ FUNCTIONS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Sets the enum of where we're launching the test drive from, so we can then have custom help text / actions in net_test_drive.sc
PROC SET_TEST_DRIVE_LAUNCH_SCRIPT(VEHICLE_TEST_DRIVE_STRUCT &sData, VEHICLE_TEST_DRIVE_LAUNCH_SCRIPT eLaunchScript)
	IF sData.eLaunchScript != eLaunchScript
		PRINTLN(TD_PRINT_LABEL(), "SET_TEST_DRIVE_LAUNCH_SCRIPT - Launching test drive from ", GET_TEST_DRIVE_LAUNCH_SCRIPT_AS_STRING(eLaunchScript))
		sData.eLaunchScript = eLaunchScript
	ENDIF
ENDPROC

/// PURPOSE:
///    Wrapper for setting the test drive state.
PROC SET_TEST_DRIVE_VEHICLE_STATE(VEHICLE_TEST_DRIVE_STRUCT &sData, VEHICLE_TEST_DRIVE_STATE eNewState)
	IF sData.eTestDriveState != eNewState
		PRINTLN(TD_PRINT_LABEL(), "SET_TEST_DRIVE_VEHICLE_STATE - Changing state from", GET_TEST_DRIVE_STATE_AS_STRING(sData.eTestDriveState), " to ", GET_TEST_DRIVE_STATE_AS_STRING(eNewState))
		sData.eTestDriveState = eNewState
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the vehicle test drive index.
PROC SET_SELECTED_TEST_DRIVE_VEHICLE_INDEX(VEHICLE_TEST_DRIVE_STRUCT &sData, VEHICLE_INDEX vehToTestDrive)
	IF IS_ENTITY_ALIVE(vehToTestDrive)
		sData.viVehToTestDrive = vehToTestDrive
	ELSE
		ASSERTLN(TD_PRINT_LABEL(), "SET_SELECTED_TEST_DRIVE_VEHICLE_INDEX - Vehicle is dead.")
	ENDIF
ENDPROC

/// PURPOSE:
///    Get selected test drive vehicle index.
FUNC VEHICLE_INDEX GET_SELECTED_TEST_DRIVE_VEHICLE(VEHICLE_TEST_DRIVE_STRUCT &sData)
	RETURN sData.viVehToTestDrive
ENDFUNC

/// PURPOSE:
///    Used to add a position for where the player will spawn in the test vehicle.
PROC ADD_TEST_DRIVE_VEHICLE_SPAWN_POINT(VEHICLE_TEST_DRIVE_STRUCT &sData, VECTOR vSpawnPoint, FLOAT fHeading)
	IF IS_VECTOR_ZERO(vSpawnPoint)
		ASSERTLN(TD_PRINT_LABEL(), "ADD_TEST_DRIVE_VEHICLE_SPAWN_POINT - Spawn point is 0.")
		EXIT
	ENDIF
	
	IF sData.iSpawnPointCounter < 0
	OR sData.iSpawnPointCounter >= NUM_NETWORK_PLAYERS
		ASSERTLN(TD_PRINT_LABEL(), "ADD_TEST_DRIVE_VEHICLE_SPAWN_POINT - Maximum spawn points reached!")
		EXIT
	ENDIF
	
	INT iCounter = sData.iSpawnPointCounter
	PRINTLN(TD_PRINT_LABEL(), "ADD_TEST_DRIVE_VEHICLE_SPAWN_POINT - Adding spawn point! Point = ", iCounter, " Coords = ", GET_STRING_FROM_VECTOR(vSpawnPoint), " Heading = ", GET_STRING_FROM_FLOAT(fHeading))
	sData.sSpawnPoints[iCounter].vCoords = vSpawnPoint
	sData.sSpawnPoints[iCounter].fHeading = fHeading
	sData.iSpawnPointCounter++
ENDPROC

/// PURPOSE:
///    Clears all spawn point data.
PROC CLEANUP_TEST_DRIVE_VEHICLE_SPAWN_POINTS(VEHICLE_TEST_DRIVE_STRUCT &sData)
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		sData.sSpawnPoints[i].vCoords = ZERO_VECTOR()
		sData.sSpawnPoints[i].fHeading = 0.0
	ENDREPEAT
	
	sData.iSpawnPointCounter = 0
ENDPROC

/// PURPOSE:
///    Used to add a position for where the player can return the vehicle and end the test drive.
/// PARAMS:
///    sData - Data struct
///    vReturnPoint - Location of the return point.
///    tlPrompt - The prompt which displays when player is in the point (leave empty to immediately fade to black).
PROC ADD_TEST_DRIVE_VEHICLE_RETURN_POINT(VEHICLE_TEST_DRIVE_STRUCT &sData, VECTOR vReturnPoint, BLIP_SPRITE eBlipSprite, TEXT_LABEL_15 tlBlipName)
	IF IS_VECTOR_ZERO(vReturnPoint)
		ASSERTLN(TD_PRINT_LABEL(), "ADD_TEST_DRIVE_VEHICLE_RETURN_POINT - Return point is 0.")
		EXIT
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(tlBlipName)
		ASSERTLN(TD_PRINT_LABEL(), "ADD_TEST_DRIVE_VEHICLE_RETURN_POINT - Blip name is empty.")
		EXIT
	ENDIF
	
	sData.sReturnPoint.vReturnPoint 	= vReturnPoint
	sData.sReturnPoint.eBlipSprite		= eBlipSprite
	sData.sReturnPoint.tlBlipName 		= tlBlipName
ENDPROC

/// PURPOSE:
///    Cleans up all the data associated with the test drive system.
PROC CLEANUP_TEST_DRIVE_VEHICLE_DATA(VEHICLE_TEST_DRIVE_STRUCT &sData)
	sData.viVehToTestDrive 	= NULL
	
	RESET_VEHICLE_SETUP_STRUCT_MP(sData.sVehSetup)
	CLEANUP_TEST_DRIVE_VEHICLE_SPAWN_POINTS(sData)
	SET_TEST_DRIVE_VEHICLE_STATE(sData, VTDS_DEFAULT)
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ STATES ╞═════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Main update function for default state. 
PROC MAINTAIN_TEST_DRIVE_VEHICLE_STATE_DEFAULT(VEHICLE_TEST_DRIVE_STRUCT &sData, BOOL bPlayCutscene)
	IF sData.viVehToTestDrive = NULL
		ASSERTLN(TD_PRINT_LABEL(), "START_TEST_DRIVE - Vehicle Index is NULL. Make sure to call SET_SELECTED_TEST_DRIVE_VEHICLE_INDEX first.")
		EXIT
	ENDIF
	
	IF IS_VECTOR_ZERO(sData.sSpawnPoints[0].vCoords)
		ASSERTLN(TD_PRINT_LABEL(), "START_TEST_DRIVE - First spawn point is 0. Make sure to call ADD_TEST_DRIVE_VEHICLE_SPAWN_POINT first.")
		EXIT
	ENDIF
	
	GET_VEHICLE_SETUP_MP(sData.viVehToTestDrive, sData.sVehSetup)
	IF bPlayCutscene
		SET_TEST_DRIVE_VEHICLE_STATE(sData, VTDS_PLAY_CUTSCENE)
	ELSE
		SET_TEST_DRIVE_VEHICLE_STATE(sData, VTDS_REQUEST_SCRIPT)
	ENDIF
ENDPROC

PROC MAINTAIN_TEST_DRIVE_VEHICLE_STATE_PLAY_CUTSCENE(VEHICLE_TEST_DRIVE_STRUCT &sData)
	IF PLAY_ENTER_VEHICLE_CUTSCENE(sData.sCutscene, sData.viVehToTestDrive, TRUE)
		SET_TEST_DRIVE_VEHICLE_STATE(sData, VTDS_REQUEST_SCRIPT)
	ENDIF
ENDPROC

/// PURPOSE:
///    Main update for requesting and launching the test drive script, passing in the data for the vehicle and spawn points.
PROC MAINTAIN_TEST_DRIVE_VEHICLE_STATE_REQUEST_SCRIPT(VEHICLE_TEST_DRIVE_STRUCT &sData)
	IF IS_SCREEN_FADED_IN()
	AND NOT IS_SCREEN_FADING_OUT()
		DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
	ENDIF
	
	REQUEST_SCRIPT(GET_TEST_DRIVE_SCRIPT_NAME())
	IF HAS_SCRIPT_LOADED(GET_TEST_DRIVE_SCRIPT_NAME())
	AND IS_SCREEN_FADED_OUT()
		VEHICLE_TEST_DRIVE_SCRIPT_DATA_STRUCT sScriptData
		CLONE_VEHICLE_SETUP_STRUCT_MP(sData.sVehSetup, sScriptData.sVehSetup)
		
		INT i
		REPEAT NUM_NETWORK_PLAYERS i
			sScriptData.sSpawnPoints[i].vCoords = sData.sSpawnPoints[i].vCoords
			sScriptData.sSpawnPoints[i].fHeading = sData.sSpawnPoints[i].fHeading
		ENDREPEAT
		
		IF NOT IS_VECTOR_ZERO(sData.sReturnPoint.vReturnPoint)
			sScriptData.sReturnPoint.vReturnPoint = sData.sReturnPoint.vReturnPoint
			sScriptData.sReturnPoint.eBlipSprite = sData.sReturnPoint.eBlipSprite
			sScriptData.sReturnPoint.tlBlipName = sData.sReturnPoint.tlBlipName
		ENDIF
		
		sScriptData.eLaunchScript = sData.eLaunchScript
		
		CLEANUP_ENTER_VEHICLE_CUTSCENE(sData.sCutscene, sData.viVehToTestDrive)
		START_NEW_SCRIPT_WITH_ARGS(GET_TEST_DRIVE_SCRIPT_NAME(), sScriptData, SIZE_OF(sScriptData), MULTIPLAYER_MISSION_STACK_SIZE)
		SET_SCRIPT_AS_NO_LONGER_NEEDED(GET_TEST_DRIVE_SCRIPT_NAME())	
		SET_TEST_DRIVE_VEHICLE_STATE(sData, VTDS_SUCCESS)
	ENDIF
ENDPROC

/// PURPOSE:
///    Starts the test drive sequence for the local player. Screen will fade out, spawn the player
///    in a clone of the given vehicle, then start the test drive script.
/// PARAMS:
///    vehToTestDrive - Vehicle to test drive. 
/// RETURNS:
///    TRUE when test drive launching is complete. 
FUNC BOOL START_TEST_DRIVE(VEHICLE_TEST_DRIVE_STRUCT &sData, BOOL bPlayCutscene)
	
	SWITCH sData.eTestDriveState
		CASE VTDS_DEFAULT
			MAINTAIN_TEST_DRIVE_VEHICLE_STATE_DEFAULT(sData, bPlayCutscene)
		BREAK
		CASE VTDS_PLAY_CUTSCENE
			MAINTAIN_TEST_DRIVE_VEHICLE_STATE_PLAY_CUTSCENE(sData)
		BREAK
		CASE VTDS_REQUEST_SCRIPT
			MAINTAIN_TEST_DRIVE_VEHICLE_STATE_REQUEST_SCRIPT(sData)
		BREAK
		CASE VTDS_SUCCESS
			CLEANUP_TEST_DRIVE_VEHICLE_DATA(sData)
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

#ENDIF // #IF FEATURE_DLC_1_2022
