USING "rage_builtins.sch"
USING "globals.sch"
USING "dialogue_public.sch"
USING "net_player_headshots.sch"
USING "net_mission_joblist_public.sch"

#IF IS_DEBUG_BUILD
    USING "net_comms_debug.sch"
#ENDIF




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Comms.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all Communications control routines.
//      NOTES           :   A maintenance function will be called each frame from the MP main script. This script will control all
//                              accesses to MP communications systems like the cellphone.
//                              It will grant permission to one script to access these systems and ensure no other scripts get access.
//
//                          Communications request permission to play through a communications candidate system. When a request is
//                              received the details are stored. Any other requests in the same frame of a lesser or equal priority are
//                              denied. The details of any other higher priority requests on the same frame replace the existing details.
//                              On the next frame, requests of a lesser or equal priority are denied, requests of a higher priority
//                              replace the details of the existing request. If the same higher priority request is received again then
//                              the details are passed to the dialogue handler to attempt to play the communication. If successful
//                              then the script is informed that its communication is being played and all other requests are denied
//                              regardless of priority until the communication has ended. If unsuccessful, then the same procedure is
//                              carried out on the next frame until a request is successful.
//
//                          Txtmsg communications will be marked as complete immediately they get sent unless they require a reply.
//                              These will only be marked as complete when the reply has been stored. There should be a delay between
//                              text messages, but txtmsgs that don't require a reply shouldn't delay phonecalls.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      CONSTS and Variables
// ===========================================================================================================

// Used as a default INT value when there is no Int substring Component
STRING      NO_STRING_SUBSTRING_COMPONENT_VALUE         = "NULL"




// ===========================================================================================================
//      Voice Chat Block Routines
// ===========================================================================================================

// PURPOSE:	Set a voice chat block to 'ringing'
PROC Set_Voice_Chat_Block_Call_Ringing()

	#IF IS_DEBUG_BUILD
		IF NOT (g_voiceChatBlockMP = MP_COMMS_VC_BLOCK_NONE)
			NET_PRINT("...KGM MP [MPComms][VCB]: Trying to set Voice Chat Block to RINGING, but it should be NONE. Letting things continue.") NET_NL()
			SCRIPT_ASSERT("Set_Voice_Chat_Block_Call_Ringing(): ERROR - Voice Chat Block status is being set to RINGING but it isn't currently NONE. Tell Keith.")
		ENDIF
	#ENDIF

	g_voiceChatBlockMP = MP_COMMS_VC_BLOCK_RINGING
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MPComms][VCB]: Setting Voice Chat Block to RINGING") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set a voice chat block to 'talking'
PROC Set_Voice_Chat_Block_Call_Talking()

	#IF IS_DEBUG_BUILD
		IF NOT (g_voiceChatBlockMP = MP_COMMS_VC_BLOCK_RINGING)
			NET_PRINT("...KGM MP [MPComms][VCB]: Trying to set Voice Chat Block to TALKING, but it should be RINGING. Letting things continue.") NET_NL()
			SCRIPT_ASSERT("Set_Voice_Chat_Block_Call_Talking(): ERROR - Voice Chat Block status is being set to TALKING but it isn't currently RINGING. Tell Keith.")
		ENDIF
	#ENDIF

	g_voiceChatBlockMP = MP_COMMS_VC_BLOCK_TALKING
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MPComms][VCB]: Setting Voice Chat Block to TALKING") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set a voice chat block to 'none'
PROC Set_Voice_Chat_Block_Call_To_None()

	g_voiceChatBlockMP = MP_COMMS_VC_BLOCK_NONE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MPComms][VCB]: Setting Voice Chat Block to NONE") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain Voice Chat Block
PROC Maintain_Voice_Chat_Block()

	// Voice Chat Block in progress?
	IF (g_voiceChatBlockMP = MP_COMMS_VC_BLOCK_NONE)
		EXIT
	ENDIF
	
	// There is some form of voice chat block required
	SWITCH (g_voiceChatBlockMP)
		// Ringing?
		CASE MP_COMMS_VC_BLOCK_RINGING
			// ...has the call ended without being connected? If yes, then no voice chat block is required.
			IF NOT (g_sCommsMP.priorityComms.ccState = MP_COMMS_ACTIVE)
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MPComms][VCB]: Call not connected so Voice Chat Block not required") NET_NL()
				#ENDIF
				
				Set_Voice_Chat_Block_Call_To_None()
				
				EXIT
			ENDIF
			
			// ...has the call progressed on to talking?
			IF (IS_CELLPHONE_CONVERSATION_PLAYING())
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MPComms][VCB]: Call connected so Voice Chat Block is now required. Setting NETWORK_SET_VOICE_ACTIVE(FALSE).") NET_NL()
				#ENDIF
				
				NETWORK_SET_VOICE_ACTIVE(FALSE)
				
				Set_Voice_Chat_Block_Call_Talking()
				
				EXIT
			ENDIF
			BREAK
			
		// Talking?
		CASE MP_COMMS_VC_BLOCK_TALKING
			// ...has the call ended? If yes, then clear the voice chat block.
			IF NOT (g_sCommsMP.priorityComms.ccState = MP_COMMS_ACTIVE)
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MPComms][VCB]: Call ended so clear the Voice Chat Block. Setting NETWORK_SET_VOICE_ACTIVE(TRUE).") NET_NL()
				#ENDIF
				
				NETWORK_SET_VOICE_ACTIVE(TRUE)
				
				Set_Voice_Chat_Block_Call_To_None()
				
				EXIT
			ENDIF
			BREAK
	ENDSWITCH

ENDPROC




// ===========================================================================================================
//      Communication Candidate Global Control Variable Check, Set, and Clear Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Clear Communication Request Routines
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Clear out all the favoured MP Communication Request values
//
// INPUT PARAMS:    paramBeingInitialised       TRUE if this is being called during initialisation [default = FALSE]
PROC Clear_Existing_MP_Communication_Request(BOOL paramBeingInitialised = FALSE)

    g_sCommsMP.priorityComms.ccState            = NO_CURRENT_MP_COMMS_REQUEST
    g_sCommsMP.priorityComms.ccHashScriptName   = 0
    g_sCommsMP.priorityComms.ccHashGroupRoot    = 0
	g_sCommsMP.priorityComms.ccHashBodyText		= 0
    g_sCommsMP.priorityComms.ccRootID           = ""
    g_sCommsMP.priorityComms.ccType             = NO_MP_COMMS_TYPE
    g_sCommsMP.priorityComms.ccDevice           = MP_COMMS_DEVICE_UNKNOWN
    g_sCommsMP.priorityComms.ccReply            = NO_MP_COMMS_REPLY_REQUIRED
    g_sCommsMP.priorityComms.ccTimeoutInitialised = FALSE
    
    // Don't update the widgets if being initialised - the widgets don't yet exist
    IF (paramBeingInitialised)
        EXIT
    ENDIF
	
    #IF IS_DEBUG_BUILD
        // Store the text widgets that display text that is otherwise non-persistent
        g_WIDGET_MP_Comms_ScriptName            = ""
        g_WIDGET_MP_Comms_CharSheetID           = ""
        g_WIDGET_MP_Comms_PlayerID              = ""
        g_WIDGET_MP_Comms_GroupID               = ""
        g_WIDGET_MP_Comms_ConvRootID            = ""
        g_WIDGET_MP_Comms_CommsTypeID           = ""
        g_WIDGET_MP_Comms_DeviceID              = ""
        g_WIDGET_MP_Comms_ReplyID               = ""
        g_WIDGET_MP_Comms_Component_TL          = ""
        
        Update_MP_Communications_Text_Widgets()
        
        // Other debug-only data
        g_WIDGET_MP_Comms_Component_INT         = NO_INT_SUBSTRING_COMPONENT_VALUE
    #ENDIF

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Communication End Status Variables
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Communication as requiring an aborted check on completion
PROC Set_MP_Communication_End_Status_Requires_Aborted_Check()

	g_sCommsMP.commsEndStatus.cesRequiresAbortedCheck	= TRUE
	g_sCommsMP.commsEndStatus.cesEndedSuccessfully		= FALSE
	
	#IF IS_DEBUG_BUILD
	       NET_PRINT("...KGM MP [MPComms]: Setting the MP Communication End Status As Requiring Aborted Check") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Set the Communication as not requiring an aborted check on completion
PROC Set_MP_Communication_End_Status_Does_Not_Require_Aborted_Check()

	g_sCommsMP.commsEndStatus.cesRequiresAbortedCheck	= FALSE
	g_sCommsMP.commsEndStatus.cesEndedSuccessfully		= FALSE
	
	#IF IS_DEBUG_BUILD
	       NET_PRINT("...KGM MP [MPComms]: Setting the MP Communication End Status As Not Requiring Aborted Check") NET_NL()
	#ENDIF

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Communication Device Checking Routines
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Checks if Communications are enabled or not
//
// RETURN VALUE:        BOOL            TRUE if the phonecalls and radio messages are disabled, otherwise FALSE
// NOTES:   KGM 3/10/11: VERY BASIC, JUST CHECKS IF PHONE DISABLED FOR NOW - NEEDS BETTER INTEGRATION
FUNC BOOL Are_MP_Comms_Devices_Disabled()
    RETURN (IS_CELLPHONE_DISABLED_OR_DISABLED_THIS_FRAME_ONLY())
ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Communication Request State Routines - Set and Check
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set currently favoured communication request as 'received this frame'
PROC Set_MP_Communication_Request_As_Received_This_Frame()
    g_sCommsMP.priorityComms.ccState = MP_COMMS_RECEIVED_THIS_FRAME
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the currently favoured communication request was received this frame
FUNC BOOL Was_MP_Communication_Request_Received_This_Frame()
    RETURN (g_sCommsMP.priorityComms.ccState = MP_COMMS_RECEIVED_THIS_FRAME)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set currently favoured communication request as 'received previous frame'
PROC Set_MP_Communication_Request_As_Received_On_Previous_Frame()
    g_sCommsMP.priorityComms.ccState = MP_COMMS_RECEIVED_PREVIOUS_FRAME
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the currently favoured communication request was received on previous frame
FUNC BOOL Was_MP_Communication_Request_Received_On_Previous_Frame()
    RETURN (g_sCommsMP.priorityComms.ccState = MP_COMMS_RECEIVED_PREVIOUS_FRAME)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Is there a current favoured communication request?
//
// RETURN VALUE:        BOOL                        TRUE if there is a current favoured communication, otherwise FALSE
FUNC BOOL Is_There_An_Existing_MP_Communication_Request()

    IF (g_sCommsMP.priorityComms.ccState = NO_CURRENT_MP_COMMS_REQUEST)
        RETURN FALSE
    ENDIF
    
    RETURN TRUE
    
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set currently favoured communication request as 'active'
PROC Set_MP_Communication_Request_As_Active()
    g_sCommsMP.priorityComms.ccState = MP_COMMS_ACTIVE
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Is there an active communication request?
//
// RETURN VALUE:        BOOL                        TRUE if there is an active communication, otherwise FALSE
FUNC BOOL Is_There_An_Active_MP_Communication_Request()
    RETURN (g_sCommsMP.priorityComms.ccState = MP_COMMS_ACTIVE)
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Communication Request Device Routines - Set and Check
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set currently favoured communication request as a radio communication
PROC Set_MP_Communication_Device_As_Radio()

    g_sCommsMP.priorityComms.ccDevice = MP_COMMS_DEVICE_RADIO
    
    // Update Widgets
    #IF IS_DEBUG_BUILD
        g_WIDGET_MP_Comms_DeviceID  = Convert_MP_Comms_Device_To_String(g_sCommsMP.priorityComms.ccDevice)
        Update_MP_Communications_Text_Widgets()
    #ENDIF
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Checks if the currently favoured communication request is a radio communication
//
// RETURN VALUE:        BOOL            TRUE if radio, otherwise FALSE
FUNC BOOL Is_MP_Communication_A_Radio_Message()

    IF (g_sCommsMP.priorityComms.ccDevice = MP_COMMS_DEVICE_RADIO)
        RETURN TRUE
    ENDIF
    
    // Not a radio communication
    RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set currently favoured communication request as a cellphone communication
PROC Set_MP_Communication_Device_As_Cellphone()

    g_sCommsMP.priorityComms.ccDevice = MP_COMMS_DEVICE_CELLPHONE
    
    // Update Widgets
    #IF IS_DEBUG_BUILD
        g_WIDGET_MP_Comms_DeviceID  = Convert_MP_Comms_Device_To_String(g_sCommsMP.priorityComms.ccDevice)
        Update_MP_Communications_Text_Widgets()
    #ENDIF
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Checks if the currently favoured communication request is a cellphone communication
//
// RETURN VALUE:        BOOL            TRUE if phonecall, otherwise FALSE
FUNC BOOL Is_MP_Communication_A_Phonecall()

    IF (g_sCommsMP.priorityComms.ccDevice = MP_COMMS_DEVICE_CELLPHONE)
        RETURN TRUE
    ENDIF
    
    // Not a phonecall
    RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set currently favoured communication request as a txtmsg communication
PROC Set_MP_Communication_Device_As_Txtmsg()

    g_sCommsMP.priorityComms.ccDevice = MP_COMMS_DEVICE_TXTMSG
    
    // Update Widgets
    #IF IS_DEBUG_BUILD
        g_WIDGET_MP_Comms_DeviceID  = Convert_MP_Comms_Device_To_String(g_sCommsMP.priorityComms.ccDevice)
        Update_MP_Communications_Text_Widgets()
    #ENDIF
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Checks if the currently favoured communication request is a txtmsg communication
//
// RETURN VALUE:        BOOL            TRUE if txtmsg, otherwise FALSE
FUNC BOOL Is_MP_Communication_A_Txtmsg()

    IF (g_sCommsMP.priorityComms.ccDevice = MP_COMMS_DEVICE_TXTMSG)
        RETURN TRUE
    ENDIF
    
    // Not a txtmsg
    RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set currently favoured communication request as an Email communication
PROC Set_MP_Communication_Device_As_Email()

    g_sCommsMP.priorityComms.ccDevice = MP_COMMS_DEVICE_EMAIL
    
    // Update Widgets
    #IF IS_DEBUG_BUILD
        g_WIDGET_MP_Comms_DeviceID  = Convert_MP_Comms_Device_To_String(g_sCommsMP.priorityComms.ccDevice)
        Update_MP_Communications_Text_Widgets()
    #ENDIF
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Checks if the currently favoured communication request is an email communication
//
// RETURN VALUE:        BOOL            TRUE if email, otherwise FALSE
FUNC BOOL Is_MP_Communication_An_Email()

    IF (g_sCommsMP.priorityComms.ccDevice = MP_COMMS_DEVICE_EMAIL)
        RETURN TRUE
    ENDIF
    
    // Not an email
    RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Returns the Text Label containing the RootID (or SubtitleID)
//
// RETURN VALUE:        TEXT_LABEL_15           The Root ID (or the Subtitle ID)
//
// NOTES:   Stored so that a txtmsg can be queried for a reply.
FUNC TEXT_LABEL_15 Get_MP_Communication_RootID()
    RETURN (g_sCommsMP.priorityComms.ccRootID)
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Communication Request Reply Routines - Set and Check
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: When active, copy the existing MP Communication details into the reply struct
PROC Copy_Reply_Details_From_Existing_MP_Communication()

	// To decide whether to output the details to the file or not
	#IF IS_DEBUG_BUILD
		INT				existingScriptNameHash	= g_sCommsMP.commsHoldReply.crHashScriptName
		g_eMPCommsReply	existingReply			= g_sCommsMP.commsHoldReply.crReply
	#ENDIF
    
    // Copy the data
    g_sCommsMP.commsHoldReply.crHashScriptName  = g_sCommsMP.priorityComms.ccHashScriptName
    g_sCommsMP.commsHoldReply.crReply           = g_sCommsMP.priorityComms.ccReply
    
    // Debug Stuff
    #IF IS_DEBUG_BUILD
        // ...console log output, if there are changes
		IF (existingScriptNameHash	!= g_sCommsMP.commsHoldReply.crHashScriptName)
		OR (existingReply			!= g_sCommsMP.commsHoldReply.crReply)
	        NET_NL()
	        NET_PRINT("...KGM MP [MPComms]: Copying the Active MP Communication Reply Status into the Reply holding struct") NET_NL()
	        NET_PRINT("           from script: ") NET_PRINT(g_WIDGET_MP_Comms_ScriptName) NET_NL()
	        NET_PRINT("           storing this reply status: ") NET_PRINT(Convert_MP_Comms_Reply_To_String(g_sCommsMP.commsHoldReply.crReply)) NET_NL()
		ENDIF
        
        // ...update Reply widgets
        Update_MP_Communication_Reply_Text_Widgets()
    #ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Ensures that the script that requested the original MP communication is the script checking for the reply.
//
// RETURN VALUE:        BOOL            TRUE if the same script requested the communication and is checking the reply, otherwise FALSE.
FUNC BOOL Is_Yes_No_Reply_Query_Being_Made_By_The_Correct_Script()

    INT hashScriptName = GET_HASH_KEY(GET_THIS_SCRIPT_NAME())
    RETURN (g_sCommsMP.commsHoldReply.crHashScriptName = hashScriptName)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the Active MP Comms needs a reply
//
// RETURN VALUE:        BOOL        TRUE if a reply is required, otherwise FALSE
//
// NOTES:   Queries the Existing MP Communication data, not the Reply holding data
FUNC BOOL Does_MP_Communication_Require_A_Reply()

    IF (g_sCommsMP.priorityComms.ccReply = NO_MP_COMMS_REPLY_REQUIRED)
        RETURN FALSE
    ENDIF
    
    // Reply required
    RETURN TRUE
    
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Is The Active MP Comms Waiting For A Reply?
//
// RETURN VALUE:        BOOL        TRUE if waiting for a reply, otherwise FALSE
//
// NOTES:   Queries the Existing MP Communication data, not the Reply holding data
FUNC BOOL Is_MP_Communication_Waiting_For_A_Reply()

    IF (g_sCommsMP.priorityComms.ccReply = MP_COMMS_REPLY_REQUIRED)
        RETURN TRUE
    ENDIF
    
    // Not waiting for a reply (may already have received one, or may not need one)
    RETURN FALSE
    
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Has the Active, or Most Recent, MP Comms had a YES reply?
//
// RETURN VALUE:        BOOL        TRUE if replied YES, otherwise FALSE
//
// NOTES:   Queries the Reply holding data, not the Existing MP Communication data 
FUNC BOOL Has_MP_Communication_Received_A_Yes_Reply()

    IF (g_sCommsMP.commsHoldReply.crReply = MP_COMMS_REPLY_YES)
        RETURN TRUE
    ENDIF
    
    // Hasn't received a YES reply
    RETURN FALSE
    
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Has the Active, or Most Recent, MP Comms Had a NO reply?
//
// RETURN VALUE:        BOOL        TRUE if replied NO, otherwise FALSE
//
// NOTES:   Queries the Reply holding data, not the Existing MP Communication data 
FUNC BOOL Has_MP_Communication_Received_A_No_Reply()

    IF (g_sCommsMP.commsHoldReply.crReply = MP_COMMS_REPLY_NO)
        RETURN TRUE
    ENDIF
    
    // Hasn't received a NO reply
    RETURN FALSE
    
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Has the Active, or Most Recent, MP Comms Had a SPECIAL reply?
//
// RETURN VALUE:        BOOL        TRUE if replied SPECIAL, otherwise FALSE
//
// NOTES:   Queries the Reply holding data, not the Existing MP Communication data
//          SPECIAL can be txtmsg 'Barter' reply, etc.
FUNC BOOL Has_MP_Communication_Received_A_Special_Reply()

    IF (g_sCommsMP.commsHoldReply.crReply = MP_COMMS_REPLY_SPECIAL)
        RETURN TRUE
    ENDIF
    
    // Hasn't received a NO reply
    RETURN FALSE
    
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set currently favoured communication as requiring a reply
//
// NOTES:   Updates the Existing MP Communication data, not the Reply holding data, because the communication is not yet active and may never activate
PROC Set_MP_Communication_Reply_As_Required()

    g_sCommsMP.priorityComms.ccReply = MP_COMMS_REPLY_REQUIRED
    
    // Update Widgets
    #IF IS_DEBUG_BUILD
        g_WIDGET_MP_Comms_ReplyID   = Convert_MP_Comms_Reply_To_String(g_sCommsMP.priorityComms.ccReply)
        Update_MP_Communications_Text_Widgets()
    #ENDIF
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set currently favoured communication as not requiring a reply
//
// NOTES:   Updates the Existing MP Communication data, not the Reply holding data, because the communication is not yet active and may never activate
PROC Set_MP_Communication_Reply_As_Not_Required()

    g_sCommsMP.priorityComms.ccReply = NO_MP_COMMS_REPLY_REQUIRED
    
    // Update Widgets
    #IF IS_DEBUG_BUILD
        g_WIDGET_MP_Comms_ReplyID   = Convert_MP_Comms_Reply_To_String(g_sCommsMP.priorityComms.ccReply)
        Update_MP_Communications_Text_Widgets()
    #ENDIF
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set the MP Communication Reply timeout
//
// INPUT PARAMS:        paramTime           The time in msec that needs to be added to the net timer for the Reply Timeout
PROC Set_MP_Communication_Reply_Timeout(INT paramTime)

    g_sCommsMP.priorityComms.ccTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME() , paramTime)
	g_sCommsMP.priorityComms.ccTimeoutInitialised = TRUE
    
    // Setting Timeout
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [MPComms]: Setting Reply Timeout: ") NET_PRINT_INT(paramTime) NET_NL()
    #ENDIF
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the MP Communication Reply Timeout has expired
//
// RETURN VALUE:        BOOL            TRUE if the Reply Timer has expired, otherwise FALSE
FUNC BOOL Has_MP_Communication_Reply_Timer_Expired()

    // Is a timer check required?
    IF NOT (g_sCommsMP.priorityComms.ccTimeoutInitialised)
        RETURN FALSE
    ENDIF

    // A timer check is required
    IF IS_TIME_LESS_THAN(GET_NETWORK_TIME() , g_sCommsMP.priorityComms.ccTimeout)
        RETURN FALSE
    ENDIF
    
    // Timer has expired
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [MPComms]: Reply Timer has expired") NET_NL()
    #ENDIF
    
    // Reset the timer
    g_sCommsMP.priorityComms.ccTimeoutInitialised = FALSE
    
    RETURN TRUE
    
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set active communication as having received a positive reply
//
// NOTES:   Updates the Existing MP Communication data, and copies to the Reply holding data
PROC Set_MP_Communication_Reply_As_Yes()

    // Ensure there is an active communication
    IF NOT (Is_There_An_Active_MP_Communication_Request())
        SCRIPT_ASSERT("Set_MP_Communication_Reply_As_Yes(): There is no active communication")
        EXIT
    ENDIF
    
    // Ensure there has not already been a 'YES' or 'NO' or 'SPECIAL' reply
    IF (Has_MP_Communication_Received_A_Yes_Reply())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Set_MP_Communication_Reply_As_Yes() - Received a second YES reply for this active communication") NET_NL()
            Debug_Output_MP_Comms_Details_For_Existing_Request()
        #ENDIF
        
        SCRIPT_ASSERT("Set_MP_Communication_Reply_As_Yes(): A 'YES' reply has already been received - see console log. Tell Keith.")
        EXIT
    ENDIF
    
    IF (Has_MP_Communication_Received_A_No_Reply())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Set_MP_Communication_Reply_As_Yes() - Received a YES reply after receiving a NO reply for this active communication") NET_NL()
            Debug_Output_MP_Comms_Details_For_Existing_Request()
        #ENDIF
        
        SCRIPT_ASSERT("Set_MP_Communication_Reply_As_Yes(): A 'NO' reply has already been received - see console log. Tell Keith.")
        EXIT
    ENDIF
    
    IF (Has_MP_Communication_Received_A_Special_Reply())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Set_MP_Communication_Reply_As_Yes() - Received a YES reply after receiving a SPECIAL reply for this active communication") NET_NL()
            Debug_Output_MP_Comms_Details_For_Existing_Request()
        #ENDIF
        
        SCRIPT_ASSERT("Set_MP_Communication_Reply_As_Yes(): A 'SPECIAL' reply has already been received - see console log. Tell Keith.")
        EXIT
    ENDIF
    
    // Set to YES
    g_sCommsMP.priorityComms.ccReply = MP_COMMS_REPLY_YES
    
    // Clear the cellphone response - just to be safe
    IF (Is_MP_Communication_A_Phonecall())
        CLEAR_RESPONSE_TO_MP_JOB_OFFER_PROMPT()
    ENDIF
    
    // Update Widgets
    #IF IS_DEBUG_BUILD
        g_WIDGET_MP_Comms_ReplyID   = Convert_MP_Comms_Reply_To_String(g_sCommsMP.priorityComms.ccReply)
        Update_MP_Communications_Text_Widgets()
    #ENDIF
    
    // Copy this to the Reply struct
    Copy_Reply_Details_From_Existing_MP_Communication()
        
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set active communication as having received a negative reply
//
// NOTES:   Updates the Existing MP Communication data, and copies to the Reply holding data
PROC Set_MP_Communication_Reply_As_No()

    // Ensure there is an active communication
    IF NOT (Is_There_An_Active_MP_Communication_Request())
        SCRIPT_ASSERT("Set_MP_Communication_Reply_As_No(): There is no active communication")
        EXIT
    ENDIF
    
    // Ensure there has not already been a 'YES' or 'NO' or 'SPECIAL' reply
    IF (Has_MP_Communication_Received_A_Yes_Reply())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Set_MP_Communication_Reply_As_No() - Received a NO reply after receiving a YES reply for this active communication") NET_NL()
            Debug_Output_MP_Comms_Details_For_Existing_Request()
        #ENDIF
        
        SCRIPT_ASSERT("Set_MP_Communication_Reply_As_No(): A 'YES' reply has already been received - see console log. Tell Keith.")
        EXIT
    ENDIF
    
    IF (Has_MP_Communication_Received_A_No_Reply())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Set_MP_Communication_Reply_As_No() - Received a second NO reply for this active communication") NET_NL()
            Debug_Output_MP_Comms_Details_For_Existing_Request()
        #ENDIF
        
        SCRIPT_ASSERT("Set_MP_Communication_Reply_As_No(): A 'NO' reply has already been received - see console log. Tell Keith.")
        EXIT
    ENDIF
    
    IF (Has_MP_Communication_Received_A_Special_Reply())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Set_MP_Communication_Reply_As_No() - Received a NO reply after receiving a SPECIAL reply for this active communication") NET_NL()
            Debug_Output_MP_Comms_Details_For_Existing_Request()
        #ENDIF
        
        SCRIPT_ASSERT("Set_MP_Communication_Reply_As_No(): A 'SPECIAL' reply has already been received - see console log. Tell Keith.")
        EXIT
    ENDIF
    
    // Set to NO
    g_sCommsMP.priorityComms.ccReply = MP_COMMS_REPLY_NO
    
    // Clear the cellphone response - just to be safe
    IF (Is_MP_Communication_A_Phonecall())
        CLEAR_RESPONSE_TO_MP_JOB_OFFER_PROMPT()
    ENDIF
    
    // Update Widgets
    #IF IS_DEBUG_BUILD
        g_WIDGET_MP_Comms_ReplyID   = Convert_MP_Comms_Reply_To_String(g_sCommsMP.priorityComms.ccReply)
        Update_MP_Communications_Text_Widgets()
    #ENDIF
    
    // Copy this to the Reply struct
    Copy_Reply_Details_From_Existing_MP_Communication()
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Set active communication as having received a 'special' reply
//
// NOTES:   Updates the Existing MP Communication data, and copies to the Reply holding data
PROC Set_MP_Communication_Reply_As_Special()

    // Ensure there is an active communication
    IF NOT (Is_There_An_Active_MP_Communication_Request())
        SCRIPT_ASSERT("Set_MP_Communication_Reply_As_Special(): There is no active communication")
        EXIT
    ENDIF
    
    // Ensure there has not already been a 'YES' or 'NO' or 'SPECIAL' reply
    IF (Has_MP_Communication_Received_A_Yes_Reply())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Set_MP_Communication_Reply_As_Special() - Received a SPECIAL reply after receiving a YES reply for this active communication") NET_NL()
            Debug_Output_MP_Comms_Details_For_Existing_Request()
        #ENDIF
        
        SCRIPT_ASSERT("Set_MP_Communication_Reply_As_Special(): A 'YES' reply has already been received - see console log. Tell Keith.")
        EXIT
    ENDIF
    
    IF (Has_MP_Communication_Received_A_No_Reply())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Set_MP_Communication_Reply_As_Special() - Received a SPECIAL reply after receiving a NO reply for this active communication") NET_NL()
            Debug_Output_MP_Comms_Details_For_Existing_Request()
        #ENDIF
        
        SCRIPT_ASSERT("Set_MP_Communication_Reply_As_Special(): A 'NO' reply has already been received - see console log. Tell Keith.")
        EXIT
    ENDIF
    
    IF (Has_MP_Communication_Received_A_Special_Reply())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Set_MP_Communication_Reply_As_Special() - Received a second SPECIAL reply for this active communication") NET_NL()
            Debug_Output_MP_Comms_Details_For_Existing_Request()
        #ENDIF
        
        SCRIPT_ASSERT("Set_MP_Communication_Reply_As_Special(): A 'SPECIAL' reply has already been received - see console log. Tell Keith.")
        EXIT
    ENDIF
    
    // Set to SPECIAL
    g_sCommsMP.priorityComms.ccReply = MP_COMMS_REPLY_SPECIAL
    
    // Clear the cellphone response - just to be safe
    IF (Is_MP_Communication_A_Phonecall())
        CLEAR_RESPONSE_TO_MP_JOB_OFFER_PROMPT()
    ENDIF
    
    // Update Widgets
    #IF IS_DEBUG_BUILD
        g_WIDGET_MP_Comms_ReplyID   = Convert_MP_Comms_Reply_To_String(g_sCommsMP.priorityComms.ccReply)
        Update_MP_Communications_Text_Widgets()
    #ENDIF
    
    // Copy this to the Reply struct
    Copy_Reply_Details_From_Existing_MP_Communication()
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the passed in type of communication requires the player to reply
//
// INPUT PARAMS:        paramCommsType              An INT representing the type of Comms
// RETURN VALUE:        BOOL                        TRUE if the passed in communication type requires a reply, otherwise NO
FUNC BOOL Does_This_MP_Communication_Type_Require_A_Reply(INT paramCommsType)

    SWITCH (paramCommsType)
        // Requires a Reply
        CASE MP_COMMS_TYPE_OFFER_MISSION
        CASE MP_COMMS_TYPE_REPLY_TXTMSG
            RETURN TRUE
            
        // Doesn't Require a Reply
        CASE NO_MP_COMMS_TYPE
        CASE MP_COMMS_TYPE_STANDARD_MESSAGE
        CASE MP_COMMS_TYPE_FORCE_ONTO_MISSION
        CASE MP_COMMS_TYPE_EOM_MESSAGE
        CASE MP_COMMS_TYPE_TXTMSG
		CASE MP_COMMS_TYPE_EMAIL
            RETURN FALSE
    ENDSWITCH
    
    // Unknown MP Communication Type
    SCRIPT_ASSERT("Does_This_MP_Communication_Type_Require_A_Reply() - Unknown Communication Type - Add to SWITCH statement")
    RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Store the Reply Requirements for the current favoured MP Communication based on the type of communication
//
// INPUT PARAMS:        paramCommsType              An INT representing the type of Comms
PROC Store_MP_Communication_Reply_Requirements(INT paramCommsType)

    IF (Does_This_MP_Communication_Type_Require_A_Reply(paramCommsType))
        Set_MP_Communication_Reply_As_Required()
        EXIT
    ENDIF
    
    // No reply required
    Set_MP_Communication_Reply_As_Not_Required()

ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Component Substring Commands (used to pass numbers and strings as sub-parts of a main string)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Add the new component onto the current list of components
//
// INPUT PARAMS:        paramAdditionalComponent        The component to add
// RETURN PARAMS:       paramComponentsUsed             The current list of components that will get updated
PROC Update_Components_Used(enumTxtMsgSpecialComponents paramAdditionalComponent, enumTxtMsgSpecialComponents &paramComponentsUsed)

    // Is there a new component to add?
    IF (paramAdditionalComponent = NO_SPECIAL_COMPONENTS)
        EXIT
    ENDIF
    
    // Ensure the additional component is a single component
    BOOL addString = FALSE
    BOOL addNumber = FALSE
    
    SWITCH paramAdditionalComponent
        CASE STRING_COMPONENT
            addString = TRUE
            BREAK
            
        CASE NUMBER_COMPONENT
            addNumber = TRUE
            BREAK
            
        DEFAULT
            SCRIPT_ASSERT("Update_Components_Used(): The new component is illegal - Tell Keith.")
            EXIT
    ENDSWITCH
    
    // Add a string?
    IF (addString)
        SWITCH paramComponentsUsed
            CASE STRING_COMPONENT
            CASE STRING_AND_NUMBER_COMPONENT
                // ...already added so nothing to do
                BREAK
                
            CASE NO_SPECIAL_COMPONENTS
                paramComponentsUsed = STRING_COMPONENT
                BREAK
                
            CASE NUMBER_COMPONENT
                paramComponentsUsed = STRING_AND_NUMBER_COMPONENT
                BREAK
                
            DEFAULT
                SCRIPT_ASSERT("Update_Components_Used(): Trying to add a STRING but the current components used enum is UNKNOWN. Support for the new component needs added. Tell Keith.")
                EXIT
        ENDSWITCH
        
        EXIT
    ENDIF
    
    // Add a number?
    IF (addNumber)
        SWITCH paramComponentsUsed
            CASE NUMBER_COMPONENT
            CASE STRING_AND_NUMBER_COMPONENT
                // ...already added so nothing to do
                BREAK
                
            CASE NO_SPECIAL_COMPONENTS
                paramComponentsUsed = NUMBER_COMPONENT
                BREAK
                
            CASE STRING_COMPONENT
                paramComponentsUsed = STRING_AND_NUMBER_COMPONENT
                BREAK
                
            DEFAULT
                SCRIPT_ASSERT("Update_Components_Used(): Trying to add a NUMBER but the current components used enum is UNKNOWN. Support for the new component needs added. Tell Keith.")
                EXIT
        ENDSWITCH
        
        EXIT
    ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Get the String that forms the substring component
//
// INPUT PARAMS:        paramComponentTextLabel         The Text Label used as a sub-string component
//						paramComponentIsPlayerName		The Text Label component is a player name
// RETURN PARAMS:       paramComponentsUsed             Updates the Components Used enum so that the Text Message system knows what components are being passed in
// RETURN VALUE:        STRING                          The string contents from the Text Label, or NO_STRING_SUBSTRING_COMPONENT_VALUE
//
// NOTES:   The txtmsg system always requires the actual display string rather than a text label pointing to the string
FUNC STRING Get_Component_String(STRING paramComponentTextLabel, BOOL paramComponentIsPlayerName, enumTxtMsgSpecialComponents &paramComponentsUsed)

    // Debug Check - check if the string component has already been returned - if it has, ASSERT but do the conversion again anyway
    #IF IS_DEBUG_BUILD
        SWITCH (paramComponentsUsed)
            CASE STRING_COMPONENT
            CASE STRING_AND_NUMBER_COMPONENT
                // String Component has already been stored, so assert to flag it as an error, but then just do the conversion as normal
                SCRIPT_ASSERT("Get_Component_String(): The String Component has already been processed - it should not be processed twice. Tell Keith.")
                BREAK
        ENDSWITCH
    #ENDIF
    
    // If the component string is NULL or EMPTY then there is no component string
    IF (Is_String_Null_Or_Empty(paramComponentTextLabel))
        RETURN NO_STRING_SUBSTRING_COMPONENT_VALUE
    ENDIF
    
    // If the component substring is already the default 'empty' string then there is no component string
    IF (ARE_STRINGS_EQUAL(paramComponentTextLabel, NO_STRING_SUBSTRING_COMPONENT_VALUE))
        RETURN NO_STRING_SUBSTRING_COMPONENT_VALUE
    ENDIF
    
    // Update the Components Used
    Update_Components_Used(STRING_COMPONENT, paramComponentsUsed)
	
	// Return the string as-is if it is a player name
	IF (paramComponentIsPlayerName)
		RETURN (paramComponentTextLabel)
	ENDIF
    
    // Return the string (when it is a text label)
    // KGM 20/10/11: GET STRING FROM TEXT FILE is debug only, but there is a workaround used for communications as mentioned by Steve
    RETURN (GET_FILENAME_FOR_AUDIO_CONVERSATION(paramComponentTextLabel))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Get the Int that forms the substring component
//
// INPUT PARAMS:        paramComponentInt               The Integer used as a sub-string component
// RETURN PARAMS:       paramComponentsUsed             Updates the Components Used enum so that the Text Message system knows what components are being passed in
// RETURN VALUE:        INT                             The INT component, or NO_INT_SUBSTRING_COMPONENT_VALUE
FUNC INT Get_Component_Int(INT paramComponentInt, enumTxtMsgSpecialComponents &paramComponentsUsed)

    // Debug Check - check if the number component has already been returned - if it has, ASSERT but do the conversion again anyway
    #IF IS_DEBUG_BUILD
        SWITCH (paramComponentsUsed)
            CASE NUMBER_COMPONENT
            CASE STRING_AND_NUMBER_COMPONENT
                // Number Component has already been stored, so assert to flag it as an error, but then just do the conversion as normal
                SCRIPT_ASSERT("Get_Component_Int(): The Int Component has already been processed - it should not be processed twice. Tell Keith.")
                BREAK
        ENDSWITCH
    #ENDIF
    
    // If the component Int is already the default 'empty' INT then there is no component INT
    IF (paramComponentInt = NO_INT_SUBSTRING_COMPONENT_VALUE)
        RETURN NO_INT_SUBSTRING_COMPONENT_VALUE
    ENDIF
    
    // Update the Components Used
    Update_Components_Used(NUMBER_COMPONENT, paramComponentsUsed)
    
    // Return the int (just return the passed in value)
    RETURN (paramComponentInt)

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Active Communication Routines
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the active communication is a Txtmsg that requires a reply
//
// RETURN VALUE:        BOOL            TRUE if the active communication is a txtmsg that needs a reply, otherwise FALSE
FUNC BOOL Is_Active_Communication_A_Txtmsg_With_Reply()

    IF NOT (Is_MP_Communication_A_Txtmsg())
        RETURN FALSE
    ENDIF
    
    IF NOT (Does_MP_Communication_Require_A_Reply())
        RETURN FALSE
    ENDIF

    // The active communication is a txtmsg that requires a reply
    RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Checks if the active communication is still active
//
// RETURN VALUE:        BOOL            TRUE if the active communication is still in progress, otherwise FALSE
FUNC BOOL Is_The_Active_Communication_Still_Active()

    // Check for a cellphone or radio conversation still ongoing
    IF (IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
        RETURN TRUE
    ENDIF
    
    // Check if the last communication was a txtmsg that requires a reply
    // NOTE: Don't check for reply received - this will be done later and will delete the txtmsg details
    IF (Is_Active_Communication_A_Txtmsg_With_Reply())
        RETURN TRUE
    ENDIF
    
    // Active communication has ended
    RETURN FALSE

ENDFUNC




// ===========================================================================================================
//      Some Functions That Get Redirected To Standard Cellphone Routines
// ===========================================================================================================

// PURPOSE:	A function to call the two calls needed to clear an existing text message from the list
//
// INPUT PARAMS:			paramRootID			The rootID for the text message
PROC Delete_Txtmsgs_On_Text_Message_List(STRING paramRootID)

	// If this text message is the priority YES/NO text message and it is currently on display on the phone then close the text message on the phone
	IF (Is_There_An_Active_MP_Communication_Request())
	AND (Is_Active_Communication_A_Txtmsg_With_Reply())
		TEXT_LABEL_15	activeRoot		= Get_MP_Communication_RootID()
		INT				activeRootHash	= GET_HASH_KEY(activeRoot)
		INT				checkRootHash	= GET_HASH_KEY(paramRootID)
		
		IF (activeRootHash = checkRootHash)
			// ...txtmsg being deleted is the active text message - is it on display?
			IF (IS_TEXTMESSAGE_LIST_IN_SINGLE_MESSAGE_VIEW())
				// ...yes (because a YES/NO txtmsg is the only one that can be on the single message view if active)
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MPComms]: Txtmsg being deleted is the active txtmsg currently on view on the phone. Closing phone view.")
					NET_NL()
	            	NET_PRINT("           Root: ")
					NET_PRINT(paramRootID)
					NET_PRINT("   [")
					NET_PRINT(GET_STRING_FROM_TEXT_FILE(paramRootID))
					NET_PRINT("]")
					NET_NL()
				#ENDIF
				
				AUTO_RETURN_TO_FULL_TEXT_MESSAGE_LIST_VIEW()
			ENDIF
		ENDIF
	ENDIF

	// Unlock, then delete
    UNLOCK_TEXT_MESSAGE_BY_LABEL(paramRootID)
    DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER(paramRootID)
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	A function to call the two calls needed to clear an existing email from the list
//
// INPUT PARAMS:			paramContentID			The contentID for the email
PROC Delete_Email_On_Email_List(STRING paramContentID)

	// Delete - this should unlock then delete
    DELETE_EMAIL_BY_LABEL_FROM_CURRENT_PLAYER(paramContentID)
		
ENDPROC





// ===========================================================================================================
//      Communication Triggering Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      Communication Triggering Using Radio
//      (The radio is essentially just a spoken conversation that blocks other cellphone calls, etc)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Attempts to play a communication using the radio
//
// INPUT PARAMS:        paramGroupID                The Subtitle Group ID field from dialogueStar
//                      paramRootID                 The conversation Root field from dialogueStar
// REFERENCE PARAMS:    paramConversation           The ped dialogue struct containing voice details
// RETURN VALUE:        BOOL                        TRUE if this communication is now actively being played, otherwise FALSE
//
// NOTES:   Assumes all parameters have been previously checked and are valid
FUNC BOOL Play_MP_Radio_Communication(structPedsForConversation &paramConversation, STRING paramGroupID, STRING paramRootID)

    IF (CREATE_CONVERSATION(paramConversation, paramGroupID, paramRootID, CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT))
        #IF IS_DEBUG_BUILD
            NET_NL()
            NET_PRINT("...KGM MP [MPComms]: MP Radio Communication is now active with priority: CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT") NET_NL()
            NET_PRINT("           From Script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
            NET_PRINT("           Subtitle Group ID: ") NET_PRINT(paramGroupID) NET_PRINT("   Root: ") NET_PRINT(paramRootID) NET_NL()
        #ENDIF
        
        // Update the device used to play the communication
        Set_MP_Communication_Device_As_Radio()
        
        // The favoured MP Communication request is now active
        Set_MP_Communication_Request_As_Active()
		
		// A Radio Communication requires an aborted check
		Set_MP_Communication_End_Status_Requires_Aborted_Check()
        
        RETURN TRUE
    ENDIF
    
    RETURN FALSE

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Communication Triggering Using Cellphone
//      (This cellphone call doesn't look for a YES/NO response)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Attempts to play a communication using the cellphone.
//
// INPUT PARAMS:        paramCharSheetID            The charSheetID for the contact making the phonecall
//                      paramGroupID                The Subtitle Group ID field from dialogueStar
//                      paramRootID                 The conversation Root field from dialogueStar
//						paramModifiers				Any cellphone call modifiers
// REFERENCE PARAMS:    paramConversation           The ped dialogue struct containing voice details
// RETURN VALUE:        BOOL                        TRUE if this communication is now actively being played, otherwise FALSE
//
// NOTES:   Assumes all parameters have been previously checked and are valid
FUNC BOOL Play_MP_Cellphone_Communication(structPedsForConversation &paramConversation, enumCharacterList paramCharSheetID, STRING paramGroupID, STRING paramRootID, INT paramModifiers)

    // Don't make phonecall if the player is sprinting
    // KGM 26/10/11: This is to avoid the player accidentally answering it
	// KGM 9/6/14: Actually wrap the sprinting check up in the death check instead of the quick-fix way this was added previously. If the player is injured they won't be sprinting.
	IF NOT (IS_PED_INJURED(PLAYER_PED_ID()))	//Fix for death check assert
	    IF (IS_PED_SPRINTING(PLAYER_PED_ID()))
	        #IF IS_DEBUG_BUILD
	            NET_PRINT("...KGM MP [MPComms]: Delaying phonecall - Player is Sprinting") NET_NL()
	        #ENDIF
	        
	        RETURN FALSE
	    ENDIF
	ENDIF

	BOOL callActive = FALSE
	
	enumConversationPriority thePriority = CONV_PRIORITY_FLOW_ONLY_USE_AMBIENT_SLOT
	IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_PRIORITY_PHONECALL))
		thePriority = CONV_PRIORITY_VERY_HIGH
        
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms]: Comms Modifier applied: MP_COMMS_MODIFIER_PRIORITY_PHONECALL - using CONV_PRIORITY_VERY_HIGH") NET_NL()
        #ENDIF
	ENDIF
	
	IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_FORCE_PLAYER_TO_ANSWER))
		// Forces the call to be answered
		callActive = CHAR_CALL_PLAYER_CELLPHONE_FORCE_ANSWER(paramConversation, paramCharSheetID, paramGroupID, paramRootID, thePriority)
	ELSE
		// Gives the player the choice to answer
		callActive = CHAR_CALL_PLAYER_CELLPHONE(paramConversation, paramCharSheetID, paramGroupID, paramRootID, thePriority)
	ENDIF
	
    IF (callActive)
		// Should the hangup option be disabled for this call?
		IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_NO_HANGUP_FOR_PHONECALL))
			DISABLE_HANGUP_FOR_THIS_CALL(TRUE)
		ENDIF
		
		// Check if the call should continue even if the player brings up the pause menu
		IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_NO_HANGUP_ON_PAUSEMENU))
			KEEP_MP_CALL_ACTIVE_ON_PAUSE_MENU(TRUE)
		ENDIF
	
        #IF IS_DEBUG_BUILD
            NET_NL()
            NET_PRINT("...KGM MP [MPComms]: MP Cellphone Communication is now active") NET_NL()
            NET_PRINT("           From Script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
            NET_PRINT("           Caller: ") NET_PRINT(Convert_MP_CharSheetID_To_String(paramCharSheetID)) NET_NL()
            NET_PRINT("           Subtitle Group ID: ") NET_PRINT(paramGroupID) NET_PRINT("   Root: ") NET_PRINT(paramRootID) NET_NL()
			IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_NO_HANGUP_FOR_PHONECALL))
				NET_PRINT("           [DISABLED HANGUP OPTION FOR THIS PHONECALL]") NET_NL()
			ENDIF
			IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_NO_HANGUP_ON_PAUSEMENU))
				NET_PRINT("           [KEEP THIS CALL PLAYING WHEN PLAYER ON PAUSE MENU]") NET_NL()
			ENDIF
        #ENDIF
        
        // Update the device used to play the communication
        Set_MP_Communication_Device_As_Cellphone()
        
        // The favoured MP Communication request is now active
        Set_MP_Communication_Request_As_Active()
 		
		// A Cellphone Communication requires an aborted check
		Set_MP_Communication_End_Status_Requires_Aborted_Check()
		
		// A voice chat block may be required while a contact phonecall is in progress
		Set_Voice_Chat_Block_Call_Ringing()
       
        RETURN TRUE
    ENDIF
    
    RETURN FALSE

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Communication Triggering Using Cellphone - Requires YES/NO Reply
//      (This cellphone call expects a YES/NO response)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Attempts to play a communication using the cellphone that requires a reply.
//
// INPUT PARAMS:        paramCharSheetID            The charSheetID for the contact making the phonecall
//                      paramGroupID                The Subtitle Group ID field from dialogueStar
//                      paramRootID                 The conversation Root field from dialogueStar
//                      paramPrompt                 A text label for the YES/NO prompt that should appear on the cellphone
// REFERENCE PARAMS:    paramConversation           The ped dialogue struct containing voice details
// RETURN VALUE:        BOOL                        TRUE if this communication is now actively being played, otherwise FALSE
//
// NOTES:   Assumes all parameters have been previously checked and are valid
FUNC BOOL Play_MP_Cellphone_Communication_With_Reply(structPedsForConversation &paramConversation, enumCharacterList paramCharSheetID, STRING paramGroupID, STRING paramRootID, STRING paramPrompt)

    // Don't make phonecall if the player is sprinting
    // KGM 26/10/11: This is to avoid the player accidentally answering it
   	IF NOT IS_PED_INJURED(PLAYER_PED_ID())	//Fix for death check assert
	ENDIF
	IF (IS_PED_SPRINTING(PLAYER_PED_ID()))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms]: Delaying phonecall - Player is Sprinting") NET_NL()
        #ENDIF
        
        RETURN FALSE
    ENDIF

    IF (CHAR_CALL_PLAYER_CELLPHONE_WITH_MP_JOB_OFFER_AUTO_PICKUP(paramConversation, paramCharSheetID, paramGroupID, paramRootID, CONV_PRIORITY_VERY_HIGH, paramPrompt))
        #IF IS_DEBUG_BUILD
            NET_NL()
            NET_PRINT("...KGM MP [MPComms]: MP Cellphone Communication With Reply is now active") NET_NL()
            NET_PRINT("           From Script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
            NET_PRINT("           Caller: ") NET_PRINT(Convert_MP_CharSheetID_To_String(paramCharSheetID)) NET_NL()
            NET_PRINT("           Subtitle Group ID: ") NET_PRINT(paramGroupID) NET_PRINT("   Root: ") NET_PRINT(paramRootID) NET_NL()
            NET_PRINT("           Cellphone Prompt: ") NET_PRINT(paramPrompt) NET_PRINT("   (text='") NET_PRINT(GET_STRING_FROM_TEXT_FILE(paramPrompt)) NET_PRINT("')") NET_NL()
        #ENDIF
        
        // Clear the cellphone response
        CLEAR_RESPONSE_TO_MP_JOB_OFFER_PROMPT()
        
        // Update the device used to play the communication
        Set_MP_Communication_Device_As_Cellphone()
        
        // The favoured MP Communication request is now active
        Set_MP_Communication_Request_As_Active()
		
		// A Cellphone Communication requires an aborted check
		Set_MP_Communication_End_Status_Requires_Aborted_Check()
		
		// A voice chat block may be required while a contact phonecall is in progress
		Set_Voice_Chat_Block_Call_Ringing()
        
        RETURN TRUE
    ENDIF
    
    RETURN FALSE

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Generic Text Message Communication Triggering (Using Cellphone) - All other functions call this one
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: All MP Text Message communications are issued by this function.
//
// INPUT PARAMS:        paramCharSheetID            	The charSheetID for the contact making the phonecall (if required)
//                      paramPlayerIdAsInt          	The playerIndex (as an INT) for the player making the phonecall (if required)
//                      paramSubtitleID             	The conversation SubtitleID field
//                      paramRequireReply           	TRUE if a reply is required, otherwise FALSE
//						paramModifiers					Flags that change the basic behaviour
//                      paramComponentTextLabel     	The Substring Component: TextLabel (as String)
//						paramComponentIsPlayerName		The Text Label component is a player name
//                      paramComponentInt           	The Substring Component: Int
//                      paramComponentLiteralString1	The first Literal String Component
//                      paramComponentLiteralString2	The second Literal String Component
// RETURN VALUE:        BOOL                        	TRUE if this text message has been sent, otherwise FALSE
//
// NOTES:   Assumes all parameters have been previously checked and are valid
FUNC BOOL Send_MP_Txtmsg_Common_Communication(enumCharacterList paramCharSheetID, INT paramPlayerIdAsInt, STRING paramSubtitleID, BOOL paramRequireReply, INT paramModifiers, STRING paramComponentTextLabel, BOOL paramComponentIsPlayerName, INT paramComponentInt, STRING paramComponentLiteralString1, STRING paramComponentLiteralString2)
    
    // If the CharSheetID is NO_CHARACTER, then the PlayerID needs to be valid
    PLAYER_INDEX	thePlayerID					= INVALID_PLAYER_INDEX()
	PEDHEADSHOT_ID	theHeadshot					= NULL
	STRING			theComponentSenderString
	
    IF (paramCharSheetID = NO_CHARACTER)
        // The CharSheetID is invalid, so the txt must be from a player
        thePlayerID = INT_TO_NATIVE(PLAYER_INDEX, paramPlayerIdAsInt)
		theComponentSenderString = GET_PLAYER_NAME(thePlayerID)
		
		// Retrieve the player headshot
		theHeadshot = Get_HeadshotID_For_Player(thePlayerID)
		IF (theHeadshot = NULL)
			// ...player's headshot isn't generated yet, has timeout occurred?
			IF (IS_TIME_LESS_THAN(GET_NETWORK_TIME(), g_sCommsMP.priorityComms.ccHeadshotTimeout))
				// ...timeout hasn't occurred, so delay issuing the text message to give the headshot time to generate
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MPComms]: Delaying Text Message for headshot: ")
					NET_PRINT(GET_PLAYER_NAME(thePlayerID))
					NET_PRINT(" [Current Time: ")
					NET_PRINT(GET_TIME_AS_STRING(GET_NETWORK_TIME()))
					NET_PRINT("] [Timeout: ")
					NET_PRINT(GET_TIME_AS_STRING(g_sCommsMP.priorityComms.ccHeadshotTimeout))
					NET_PRINT("]")
					NET_NL()
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			// Timeout has occurred, so send the text message without the headshot being ready
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MPComms]: Text Message headshot generation timeout has elasped: ")
				NET_PRINT(GET_PLAYER_NAME(thePlayerID))
				NET_PRINT(" [Current Time: ")
				NET_PRINT(GET_TIME_AS_STRING(GET_NETWORK_TIME()))
				NET_PRINT("] [Timeout: ")
				NET_PRINT(GET_TIME_AS_STRING(g_sCommsMP.priorityComms.ccHeadshotTimeout))
				NET_PRINT("]")
				NET_PRINT(" - trying to send the text message anyway")
				NET_NL()
			#ENDIF
		ENDIF
		
		// Headshot is generated, so store it in the global for the cellphone system to use in the feed and the text message
		g_playerTxtmsgHeadshotMP = theHeadshot
    ELSE
        // The CharSheetID is valid, so the txt is from a character
		theComponentSenderString = ""
    ENDIF
    
    // Get the contents of basic Substring Components: textlabel/int
    enumTxtMsgSpecialComponents theComponentsUsed = NO_SPECIAL_COMPONENTS
    STRING theComponentString = Get_Component_String(paramComponentTextLabel, paramComponentIsPlayerName, theComponentsUsed)
    INT theComponentInt = Get_Component_Int(paramComponentInt, theComponentsUsed)
	
	// Get the contents of the additional Literla String Components
	INT numLiteralStrings = 0
	IF NOT (IS_STRING_NULL_OR_EMPTY(paramComponentLiteralString1))
		numLiteralStrings++
		
		IF NOT (IS_STRING_NULL_OR_EMPTY(paramComponentLiteralString2))
			numLiteralStrings++
		ENDIF
	ENDIF
	
	// KGM 20/8/13: Workaround: Sometimes we want to ignore the 23-char component string and use the first 63-char literal string instead.
	//				To achieve this we're passing in a string of '~a~~a~' and a component string with a space and then the literal string.
	//				This does create a space in the message, so I'm going to replace it with an empty string here which seems to work.
	IF (ARE_STRINGS_EQUAL(theComponentString, " "))
		theComponentString = ""
	ENDIF

    // Signifier?
    BOOL useSignifier = TRUE
    
    // Allow Barter as a player reply?
    enumTxtMsgIsBarterRequired	barterID	= NO_BARTER_REQUIRED
	enumTxtMsgIsReplyRequired	replyID		= NO_REPLY_REQUIRED
	enumTxtMsgLockedStatus		lockedID	= TXTMSG_UNLOCKED
	enumTxtMsgMissionCritical	criticalID	= TXTMSG_NOT_CRITICAL
	
    IF (paramRequireReply)
		replyID		= REPLY_IS_REQUIRED
		lockedID	= TXTMSG_LOCKED
		criticalID	= TXTMSG_CRITICAL
		
	    IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_TXTMSG_ALLOW_BARTER))
	        barterID = BARTER_IS_REQUIRED
	    ENDIF
	ENDIF
	
	// Force the txtmsg to be locked (as requested by the calling script?)
	IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_COMMS_LOCKED))
	    lockedID = TXTMSG_LOCKED
	ENDIF
    
    // Try to send the text message
    // Choose the correct txtmsg command to use
    BOOL txtmsgSent = FALSE
    IF NOT (theComponentsUsed = NO_SPECIAL_COMPONENTS)
    OR NOT IS_STRING_NULL_OR_EMPTY(theComponentSenderString)
		// Use the txtmsg with components command
        txtmsgSent = SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS(paramCharSheetID, paramSubtitleID, lockedID, theComponentString, theComponentInt, theComponentSenderString, theComponentsUsed, criticalID, TXTMSG_DO_NOT_AUTO_UNLOCK, replyID, ICON_STANDARD_TEXTMSG, useSignifier, barterID, CANNOT_CALL_SENDER, numLiteralStrings, paramComponentLiteralString1, paramComponentLiteralString2)
    ELSE
        // Use the standard txtmsg command
        txtmsgSent = SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(paramCharSheetID, paramSubtitleID, lockedID, criticalID, TXTMSG_DO_NOT_AUTO_UNLOCK, replyID, ICON_STANDARD_TEXTMSG, useSignifier, barterID, CANNOT_CALL_SENDER)
    ENDIF
    
    // Check if the txtmsg got sent
    IF NOT (txtmsgSent)
        RETURN FALSE
    ENDIF
	
	// Force cellphone on-screen
	IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_COMMS_FORCE_ONSCREEN))
        // The communication was successful, so force the cellphone on-screen if required
        LAUNCH_CELLPHONE_FROM_CONTROLLER_SCRIPT()
	ENDIF
            
    // Txtmsg got successfully sent
    #IF IS_DEBUG_BUILD
        NET_NL()
        NET_PRINT("...KGM MP [MPComms]: MP Text Message Communication has been sent") NET_NL()
        NET_PRINT("           From Script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
        NET_PRINT("           Sender is a player?    [PlayerName = ") NET_PRINT(theComponentSenderString) NET_PRINT("]") NET_NL()
        NET_PRINT("           Sender is a character? [Character  = ") NET_PRINT(Convert_MP_CharSheetID_To_String(paramCharSheetID)) NET_PRINT("]") NET_NL()
        NET_PRINT("           Subtitle ID: ") NET_PRINT(paramSubtitleID) NET_NL()
        SWITCH theComponentsUsed
            CASE NO_SPECIAL_COMPONENTS
                NET_PRINT("              no Substring Components") NET_NL()
                BREAK
            CASE STRING_COMPONENT
                NET_PRINT("              with String Component: ") NET_PRINT(paramComponentTextLabel) NET_PRINT("  [") NET_PRINT(theComponentString) NET_PRINT("]") NET_NL()
                BREAK
            CASE NUMBER_COMPONENT
                NET_PRINT("              with Number Component: ") NET_PRINT_INT(paramComponentInt) NET_NL()
                BREAK
            CASE STRING_AND_NUMBER_COMPONENT
                NET_PRINT("              with String Component: ") NET_PRINT(paramComponentTextLabel) NET_PRINT("  [") NET_PRINT(theComponentString) NET_PRINT("]") NET_NL()
                NET_PRINT("              with Number Component: ") NET_PRINT_INT(paramComponentInt) NET_NL()
                BREAK
            DEFAULT
                NET_PRINT("              UNKNOWN COMPONENT ENUM") NET_NL()
                BREAK
        ENDSWITCH
        NET_PRINT("           Force OnScreen?: ")
		IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_COMMS_FORCE_ONSCREEN))
			NET_PRINT("YES") NET_NL()
		ELSE
			NET_PRINT("NO") NET_NL()
		ENDIF
        NET_PRINT("           Allow Player To Delete?: ")
		IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_COMMS_LOCKED))
			NET_PRINT("NO") NET_NL()
		ELSE
			NET_PRINT("YES") NET_NL()
		ENDIF
        NET_PRINT("           Reply Required?: ")
        IF (paramRequireReply)
			NET_PRINT("YES") NET_NL()
			NET_PRINT("           Allow Barter Reply?: ")
            IF (barterID = BARTER_IS_REQUIRED)
				NET_PRINT("YES") NET_NL()
            ELSE
				NET_PRINT("NO") NET_NL()
            ENDIF
        ELSE
            NET_PRINT("NO") NET_NL()
        ENDIF
    #ENDIF
    
    // Update the device used to play the communication
    Set_MP_Communication_Device_As_Txtmsg()
    
    // The favoured MP Communication request is now active
    Set_MP_Communication_Request_As_Active()
		
	// A Text Message Communication does not require an aborted check
	Set_MP_Communication_End_Status_Does_Not_Require_Aborted_Check()
    
    RETURN TRUE

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Text Message Communication Triggering (Using Cellphone) - Requires no reply
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Attempts to play a text message communication using the cellphone.
//
// INPUT PARAMS:        paramCharSheetID            	The charSheetID for the contact making the phonecall (if required)
//                      paramPlayerIdAsInt          	The playerIndex (as an INT) for the player making the phonecall (if required)
//                      paramSubtitleID             	The conversation SubtitleID field
//						paramModifiers					Flags to modify the basic behaviour
//                      paramComponentTextLabel     	The Substring Component: TextLabel (as String)
//						paramComponentIsPlayerName		The Text Label component is a player name
//                      paramComponentInt           	The Substring Component: Int
//                      paramComponentLiteralString1	The first Literal String Component
//                      paramComponentLiteralString2	The second Literal String Component
// RETURN VALUE:        BOOL                        	TRUE if this text message has been sent, otherwise FALSE
//
// NOTES:   Assumes all parameters have been previously checked and are valid
FUNC BOOL Send_MP_Txtmsg_Communication(enumCharacterList paramCharSheetID, INT paramPlayerIdAsInt, STRING paramSubtitleID, INT paramModifiers, STRING paramComponentTextLabel, BOOL paramComponentIsPlayerName, INT paramComponentInt, STRING paramComponentLiteralString1, STRING paramComponentLiteralString2)
    
    BOOL replyRequired = FALSE
    RETURN (Send_MP_Txtmsg_Common_Communication(paramCharSheetID, paramPlayerIdAsInt, paramSubtitleID, replyRequired, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2))

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Text Message Communication Triggering (Using Cellphone) - Requires YES/NO and optional BARTER Reply
//      (This text message expects a YES/NO and optional BARTER response)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Attempts to play a text message communication using the cellphone that requires a reply.
//
// INPUT PARAMS:        paramCharSheetID            	The charSheetID for the contact making the phonecall (if required)
//                      paramPlayerIdAsInt          	The playerIndex (as an INT) for the player making the phonecall (if required)
//                      paramSubtitleID             	The conversation SubtitleID field
//						paramModifiers					Flags to modify the basic behaviour
//                      paramComponentTextLabel     	The Substring Component: TextLabel (as String)
//						paramComponentIsPlayerName		The Text Label component is a player name
//                      paramComponentInt           	The Substring Component: Int
//                      paramComponentLiteralString1	The first Literal String Component
//                      paramComponentLiteralString2	The second Literal String Component
// RETURN VALUE:        BOOL                        	TRUE if this text message has been sent, otherwise FALSE
//
// NOTES:   Assumes all parameters have been previously checked and are valid
FUNC BOOL Send_MP_Txtmsg_Communication_With_Reply(enumCharacterList paramCharSheetID, INT paramPlayerIdAsInt, STRING paramSubtitleID, INT paramModifiers, STRING paramComponentTextLabel, BOOL paramComponentIsPlayerName, INT paramComponentInt, STRING paramComponentLiteralString1, STRING paramComponentLiteralString2)

    // Don't make phonecall if the player is sprinting
    // KGM 26/10/11: This is to avoid the player accidentally answering it
    IF NOT IS_PED_INJURED(PLAYER_PED_ID())	//Fix for death check assert
	ENDIF
	IF (IS_PED_SPRINTING(PLAYER_PED_ID()))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms]: Delaying text message - Player is Sprinting") NET_NL()
        #ENDIF
        
        RETURN FALSE
    ENDIF
    
    // Update the device used to play the communication
    Set_MP_Communication_Device_As_Txtmsg()
    
    BOOL replyRequired = TRUE
    IF (Send_MP_Txtmsg_Common_Communication(paramCharSheetID, paramPlayerIdAsInt, paramSubtitleID, replyRequired, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2))
        // The favoured MP Communication should timeout
        Set_MP_Communication_Reply_Timeout(MP_COMMS_DEFAULT_TXTMSG_REPLY_TIMEOUT_msec)
        
        RETURN TRUE
    ENDIF
    
    // Txtmsg hasn't yet been sent
    RETURN FALSE

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Generic Email Communication Triggering - All other functions call this one
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: All MP Email communications are issued by this function.
//
// INPUT PARAMS:        paramCharSheetID            	The charSheetID for the contact sending the email (if required)
//                      paramPlayerIdAsInt          	The playerIndex (as an INT) for the player sending the email (if required)
//                      paramContentsID             	The Email contents text label (also add another text label with '_SUB' postfixed to it for the Email Subject field string)
//                      paramRequireReply           	TRUE if a reply is required, otherwise FALSE
//						paramModifiers					Flags that change the basic behaviour
//                      paramComponentTextLabel     	The Substring Component: TextLabel (as String)
//						paramComponentIsPlayerName		The Text Label component is a player name
//                      paramComponentInt           	The Substring Component: Int
//                      paramComponentLiteralString1	The first Literal String Component
//                      paramComponentLiteralString2	The second Literal String Component
// RETURN VALUE:        BOOL                        	TRUE if this Email has been sent, otherwise FALSE
//
// NOTES:   Assumes all parameters have been previously checked and are valid
FUNC BOOL Send_MP_Email_Common_Communication(enumCharacterList paramCharSheetID, INT paramPlayerIdAsInt, STRING paramContentsID, BOOL paramRequireReply, INT paramModifiers, STRING paramComponentTextLabel, BOOL paramComponentIsPlayerName, INT paramComponentInt, STRING paramComponentLiteralString1, STRING paramComponentLiteralString2)
    
    // If the CharSheetID is NO_CHARACTER, then the PlayerID needs to be valid
    PLAYER_INDEX	thePlayerID					= INVALID_PLAYER_INDEX()
	PEDHEADSHOT_ID	theHeadshot					= NULL
	STRING			theComponentSenderString
	
    IF (paramCharSheetID = NO_CHARACTER)
        // The CharSheetID is invalid, so the txt must be from a player
        thePlayerID = INT_TO_NATIVE(PLAYER_INDEX, paramPlayerIdAsInt)
		theComponentSenderString = GET_PLAYER_NAME(thePlayerID)
		
		// Retrieve the player headshot
		theHeadshot = Get_HeadshotID_For_Player(thePlayerID)
		IF (theHeadshot = NULL)
			// ...player's headshot isn't generated yet, has timeout occurred?
			IF (IS_TIME_LESS_THAN(GET_NETWORK_TIME(), g_sCommsMP.priorityComms.ccHeadshotTimeout))
				// ...timeout hasn't occurred, so delay issuing the email to give the headshot time to generate
				#IF IS_DEBUG_BUILD
					NET_PRINT("...KGM MP [MPComms]: Delaying Email for headshot: ")
					NET_PRINT(GET_PLAYER_NAME(thePlayerID))
					NET_PRINT(" [Current Time: ")
					NET_PRINT(GET_TIME_AS_STRING(GET_NETWORK_TIME()))
					NET_PRINT("] [Timeout: ")
					NET_PRINT(GET_TIME_AS_STRING(g_sCommsMP.priorityComms.ccHeadshotTimeout))
					NET_PRINT("]")
					NET_NL()
				#ENDIF
				
				RETURN FALSE
			ENDIF
			
			// Timeout has occurred, so send the email without the headshot being ready
			#IF IS_DEBUG_BUILD
				NET_PRINT("...KGM MP [MPComms]: Email headshot generation timeout has elasped: ")
				NET_PRINT(GET_PLAYER_NAME(thePlayerID))
				NET_PRINT(" [Current Time: ")
				NET_PRINT(GET_TIME_AS_STRING(GET_NETWORK_TIME()))
				NET_PRINT("] [Timeout: ")
				NET_PRINT(GET_TIME_AS_STRING(g_sCommsMP.priorityComms.ccHeadshotTimeout))
				NET_PRINT("]")
				NET_PRINT(" - trying to send the email anyway")
				NET_NL()
			#ENDIF
		ENDIF
		
// KGM 14/1/14: Not sure if this headshot global is still required for emails
//		// Headshot is generated, so store it in the global for the cellphone system to use in the feed and the email
		//g_playerTxtmsgHeadshotMP = theHeadshot
    ELSE
        // The CharSheetID is valid, so the txt is from a character
		theComponentSenderString = ""
    ENDIF
    
    // Get the contents of basic Substring Components: textlabel/int

    enumTxtMsgSpecialComponents theComponentsUsed = NO_SPECIAL_COMPONENTS
    STRING theComponentString = Get_Component_String(paramComponentTextLabel, paramComponentIsPlayerName, theComponentsUsed)
    INT theComponentInt = Get_Component_Int(paramComponentInt, theComponentsUsed)
	
	// Get the contents of the additional Literal String Components
	INT numLiteralStrings = 0
	IF NOT (IS_STRING_NULL_OR_EMPTY(paramComponentLiteralString1))
		numLiteralStrings++
		
		IF NOT (IS_STRING_NULL_OR_EMPTY(paramComponentLiteralString2))
			numLiteralStrings++
		ENDIF
	ENDIF
	
	// KGM 20/8/13: Workaround: Sometimes we want to ignore the 23-char component string and use the first 63-char literal string instead.
	//				To achieve this we're passing in a string of '~a~~a~' and a component string with a space and then the literal string.
	//				This does create a space in the message, so I'm going to replace it with an empty string here which seems to work.
	IF (ARE_STRINGS_EQUAL(theComponentString, " "))
		theComponentString = ""
	ENDIF

    // Signifier?
    BOOL useSignifier = TRUE
    
    // Allow Barter as a player reply?
    enumMPemailIsBarterRequired	barterID	= MPE_NO_BARTER_REQUIRED
	enumMPemailIsReplyRequired	replyID		= MPE_NO_REPLY_REQUIRED
	enumMPemailLockedStatus		lockedID	= email_UNLOCKED
	enumMPemailMissionCritical	criticalID	= EMAIL_NOT_CRITICAL
	
// KGM 14/1/14: Currently no reply supported
//paramRequireReply = FALSE
    IF (paramRequireReply)
		replyID		= MPE_REPLY_IS_REQUIRED
		lockedID	= email_LOCKED
		criticalID	= EMAIL_CRITICAL
		
	    IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_TXTMSG_ALLOW_BARTER))
	        barterID = MPE_BARTER_IS_REQUIRED
	    ENDIF
	ENDIF
	
	// Force the email to be locked (as requested by the calling script?)
	IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_COMMS_LOCKED))
	    lockedID = email_LOCKED
	ENDIF
    
    // Try to send the email
    // Choose the correct email command to use
    BOOL emailSent = FALSE
	
// KGM 14/1/14: Currently no special components supported?
    IF NOT (theComponentsUsed = NO_SPECIAL_COMPONENTS)
    OR NOT IS_STRING_NULL_OR_EMPTY(theComponentSenderString)
		// Use the txtmsg with components command
		PRINTLN("[MMM] SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS")
        emailSent = SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER_WITH_SPECIAL_COMPONENTS(paramCharSheetID, 
																				 paramContentsID, 
																				 lockedID, 
																				 theComponentString, 
																				 theComponentInt, 
																				 theComponentSenderString, 
																				 theComponentsUsed, 
																				 criticalID, 
																				 EMAIL_DO_NOT_AUTO_UNLOCK , 
																				 replyID, 
																				 ICON_STANDARD_TEXTMSG, 
																				 useSignifier, 
																				 barterID, 
																				 MPE_CANNOT_CALL_SENDER, 
																				 numLiteralStrings, 
																				 paramComponentLiteralString1,
																				 paramComponentLiteralString2)
    ELSE
        // Use the standard email command
        emailSent = SEND_EMAIL_MESSAGE_TO_CURRENT_PLAYER(paramCharSheetID, 
														 paramContentsID, 
														 lockedID, 
														 criticalID, 
														 EMAIL_DO_NOT_AUTO_UNLOCK, 
														 replyID, 
														 ICON_STANDARD_TEXTMSG, 
														 useSignifier,
														 barterID, 
														 MPE_CANNOT_CALL_SENDER)
    ENDIF
    
    // Check if the email got sent
    IF NOT (emailSent)
        RETURN FALSE
    ENDIF
	
	// Force cellphone on-screen
	IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_COMMS_FORCE_ONSCREEN))
        // The communication was successful, so force the cellphone on-screen if required
        LAUNCH_CELLPHONE_FROM_CONTROLLER_SCRIPT()
	ENDIF
            
    // Email got successfully sent
    #IF IS_DEBUG_BUILD
        NET_NL()
        NET_PRINT("...KGM MP [MPComms]: MP Email has been sent") NET_NL()
        NET_PRINT("           From Script: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
        NET_PRINT("           Sender is a player?    [PlayerName = ") NET_PRINT(theComponentSenderString) NET_PRINT("]") NET_NL()
        NET_PRINT("           Sender is a character? [Character  = ") NET_PRINT(Convert_MP_CharSheetID_To_String(paramCharSheetID)) NET_PRINT("]") NET_NL()
        NET_PRINT("           Subtitle ID: ") NET_PRINT(paramContentsID) NET_NL()
// KGM 14/1/14: Currently no special components supported?
        SWITCH theComponentsUsed
            CASE NO_SPECIAL_COMPONENTS
                NET_PRINT("              no Substring Components") NET_NL()
                BREAK
           CASE STRING_COMPONENT
                NET_PRINT("              with String Component: ") NET_PRINT(paramComponentTextLabel) NET_PRINT("  [") NET_PRINT(theComponentString) NET_PRINT("]") NET_NL()
                BREAK
            CASE NUMBER_COMPONENT
                NET_PRINT("              with Number Component: ") NET_PRINT_INT(paramComponentInt) NET_NL()
                BREAK
            CASE STRING_AND_NUMBER_COMPONENT
                NET_PRINT("              with String Component: ") NET_PRINT(paramComponentTextLabel) NET_PRINT("  [") NET_PRINT(theComponentString) NET_PRINT("]") NET_NL()
                NET_PRINT("              with Number Component: ") NET_PRINT_INT(paramComponentInt) NET_NL()
                BREAK
            DEFAULT
                NET_PRINT("              UNKNOWN COMPONENT ENUM") NET_NL()
                BREAK
        ENDSWITCH
        NET_PRINT("           Force OnScreen?: ")
		IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_COMMS_FORCE_ONSCREEN))
			NET_PRINT("YES") NET_NL()
		ELSE
			NET_PRINT("NO") NET_NL()
		ENDIF
        NET_PRINT("           Allow Player To Delete?: ")
		IF (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_COMMS_LOCKED))
			NET_PRINT("NO") NET_NL()
		ELSE
			NET_PRINT("YES") NET_NL()
		ENDIF
        NET_PRINT("           Reply Required?: ")
        IF (paramRequireReply)
			NET_PRINT("YES") NET_NL()
			NET_PRINT("           Allow Barter Reply?: ")
            IF (barterID = MPE_BARTER_IS_REQUIRED)
				NET_PRINT("YES") NET_NL()
            ELSE
				NET_PRINT("NO") NET_NL()
            ENDIF
        ELSE
            NET_PRINT("NO") NET_NL()
        ENDIF
    #ENDIF
    
    // Update the device used to play the communication
    Set_MP_Communication_Device_As_Email()
    
    // The favoured MP Communication request is now active
    Set_MP_Communication_Request_As_Active()
		
	// A Text Message Communication does not require an aborted check
	Set_MP_Communication_End_Status_Does_Not_Require_Aborted_Check()
    
    RETURN TRUE

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Email Communication Triggering - Requires no reply
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Attempts to play an email communication (on the cellphone app).
//
// INPUT PARAMS:        paramCharSheetID            	The charSheetID for the contact making the phonecall (if required)
//                      paramPlayerIdAsInt          	The playerIndex (as an INT) for the player making the phonecall (if required)
//                      paramContentsID             	The conversation ContentsID field
//						paramModifiers					Flags to modify the basic behaviour
//                      paramComponentTextLabel     	The Substring Component: TextLabel (as String)
//						paramComponentIsPlayerName		The Text Label component is a player name
//                      paramComponentInt           	The Substring Component: Int
//                      paramComponentLiteralString1	The first Literal String Component
//                      paramComponentLiteralString2	The second Literal String Component
// RETURN VALUE:        BOOL                        	TRUE if this email has been sent, otherwise FALSE
//
// NOTES:   Assumes all parameters have been previously checked and are valid
FUNC BOOL Send_MP_Email_Communication(enumCharacterList paramCharSheetID, INT paramPlayerIdAsInt, STRING paramContentsID, INT paramModifiers, STRING paramComponentTextLabel, BOOL paramComponentIsPlayerName, INT paramComponentInt, STRING paramComponentLiteralString1, STRING paramComponentLiteralString2)
    
	Set_MP_Communication_Device_As_Email()
	
    BOOL replyRequired = FALSE
    RETURN (Send_MP_Email_Common_Communication(paramCharSheetID, paramPlayerIdAsInt, paramContentsID, replyRequired, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2))

ENDFUNC

//TODO: Check over this as I'm not sure how it's meant to work...
FUNC BOOL Send_MP_Email_Communication_With_Reply(enumCharacterList paramCharSheetID, INT paramPlayerIdAsInt, STRING paramContentsID, INT paramModifiers, STRING paramComponentTextLabel, BOOL paramComponentIsPlayerName, INT paramComponentInt, STRING paramComponentLiteralString1, STRING paramComponentLiteralString2)
    
	Set_MP_Communication_Device_As_Email()
	
    BOOL replyRequired = TRUE
    IF (Send_MP_Email_Common_Communication(paramCharSheetID, paramPlayerIdAsInt, paramContentsID, replyRequired, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2))
		Set_MP_Communication_Reply_Timeout(MP_COMMS_DEFAULT_TXTMSG_REPLY_TIMEOUT_msec)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Communication Triggering Interface Routines
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Attempts to play a communication using the correct MP in-game communication device
//
// INPUT PARAMS:        paramCharSheetID            	The CharSheetID for the contact making the communication (if required)
//                      paramPlayerIdAsInt          	The PlayerIndex as an INT for the player making the communication (if required)
//                      paramGroupID                	The Subtitle Group ID field from dialogueStar
//                      paramRootID                 	The conversation Root field from dialogueStar
//                      paramCommsType              	An INT representing the type of Comms - also used to prioritise (CONST_INTs defined in MP_globals_Comms.sch)
//                      paramDevice                 	The Comms Device to use
//						paramModifiers					Flags that modify the basic behaviour
//                      paramComponentTextLabel     	The Substring Component: TextLabel (as String)
//						paramComponentIsPlayerName		The Text Label component is a player name
//                      paramComponentInt           	The Substring Component: Int
//                      paramComponentLiteralString1	The first Literal String Component
//                      paramComponentLiteralString2	The second Literal String Component
// REFERENCE PARAMS:    paramConversation           	The ped dialogue struct containing voice details
// RETURN VALUE:        BOOL                        	TRUE if this communication is now actively being played, otherwise FALSE
//
// NOTES:   Assumes all parameters have been previously checked and are valid
FUNC BOOL Play_MP_Communication(structPedsForConversation &paramConversation, enumCharacterList paramCharSheetID, INT paramPlayerIdAsInt, STRING paramGroupID, STRING paramRootID, INT paramCommsType, g_eMPCommsDevice paramDevice, INT paramModifiers, STRING paramComponentTextLabel, BOOL paramComponentIsPlayerName, INT paramComponentInt, STRING paramComponentLiteralString1, STRING paramComponentLiteralString2)

    // Copy the Reply Requirements into the Comms Reply struct
    Copy_Reply_Details_From_Existing_MP_Communication()
    
    // Error Checking: If the device is unknown then make it the default device
    IF (paramDevice = MP_COMMS_DEVICE_UNKNOWN)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Play_MP_Communication() - Request to play communication using communication device of MP_COMMS_DEVICE_UNKNOWN") NET_NL()
            Debug_Output_MP_Comms_Details(GET_THIS_SCRIPT_NAME(), paramGroupID, paramRootID, paramCommsType, paramDevice, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2)
            SCRIPT_ASSERT("Play_MP_Communication() - illegal communication device - check logs for more details. Tell Keith.")
        #ENDIF
        
        paramDevice = MP_COMMS_DEVICE_DEFAULT
    ENDIF

    // End of Mission Messages
    IF (paramCommsType = MP_COMMS_TYPE_EOM_MESSAGE)
        // ...default device is radio
        RETURN (Play_MP_Radio_Communication(paramConversation, paramGroupID, paramRootID))
    ENDIF

    // Offer Mission Messages - expect a YES or NO reply
    IF (paramCommsType = MP_COMMS_TYPE_OFFER_MISSION)
        // ...default device is cellphone
        TEXT_LABEL_15 offerMissionPrompt = "CELL_226"
        RETURN (Play_MP_Cellphone_Communication_With_Reply(paramConversation, paramCharSheetID, paramGroupID, paramRootID, offerMissionPrompt))
    ENDIF
    
    // Standard Messages
    // Force Onto Mission Messages
    IF (paramCommsType = MP_COMMS_TYPE_STANDARD_MESSAGE)
    OR (paramCommsType = MP_COMMS_TYPE_FORCE_ONTO_MISSION)
        // ...default device is Cellphone, so check for a request to use another device.
        IF (paramDevice = MP_COMMS_DEVICE_RADIO)
            RETURN (Play_MP_Radio_Communication(paramConversation, paramGroupID, paramRootID))
        ENDIF
        
        // Use default device
        RETURN (Play_MP_Cellphone_Communication(paramConversation, paramCharSheetID, paramGroupID, paramRootID, paramModifiers))
    ENDIF
    
	// Reply Text Messages
	IF (paramCommsType = MP_COMMS_TYPE_REPLY_TXTMSG)
		// A Yes/No reply is required (the function itself will check for the 'barter' option)
		RETURN (Send_MP_Txtmsg_Communication_With_Reply(paramCharSheetID, paramPlayerIdAsInt, paramRootID, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2))
	ENDIF
		
    // Standard Text Messages
    IF (paramCommsType = MP_COMMS_TYPE_TXTMSG)
        RETURN (Send_MP_Txtmsg_Communication(paramCharSheetID, paramPlayerIdAsInt, paramRootID, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2))
    ENDIF
		
    // Standard Email
    IF (paramCommsType = MP_COMMS_TYPE_EMAIL)
        RETURN (Send_MP_Email_Communication(paramCharSheetID, paramPlayerIdAsInt, paramRootID, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2))
    ENDIF
	
	// Email with Reply
	IF (paramCommsType = MP_COMMS_TYPE_REPLY_EMAIL )
        RETURN (Send_MP_Email_Communication_With_Reply(paramCharSheetID, paramPlayerIdAsInt, paramRootID, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2))
    ENDIF
    
    // Check for the illegal value - debug only
    #IF IS_DEBUG_BUILD
        IF (paramCommsType = NO_MP_COMMS_TYPE)
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Play_MP_Communication() - Request to play communication using communication type is NO_MP_COMMS_TYPE") NET_NL()
            Debug_Output_MP_Comms_Details(GET_THIS_SCRIPT_NAME(), paramGroupID, paramRootID, paramCommsType, paramDevice, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2)
            SCRIPT_ASSERT("Play_MP_Communication() - illegal communication type - check logs for more details. Tell Keith.")
        ENDIF
    #ENDIF
    
    // Unknown communication type - it will need to be set up - debug only
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Play_MP_Communication() - UNKNOWN COMMUNICATION TYPE - need to set it up") NET_NL()
        Debug_Output_MP_Comms_Details(GET_THIS_SCRIPT_NAME(), paramGroupID, paramRootID, paramCommsType, paramDevice, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2)
        SCRIPT_ASSERT("Play_MP_Communication() - unknown communication type - check logs for more details. Tell Keith.")
    #ENDIF
    
    RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Clears the existing MP communication
// NOTES:   Assume all checks as to whether it is ok to do this have been made prior to calling this.
PROC Cancel_Existing_MP_Communication()

    // We want to interrupt the existing communication
    //  (the default for conversations is to let the last line play if KILL_ANY_CONVERSATION() is used)
    // NOTES:   The dialogue handler will ignore the call if it is not relevant.
    
    #IF IS_DEBUG_BUILD
        NET_PRINT("...........The Communication Being Cancelled Was Requested By This Script ") NET_PRINT(g_WIDGET_MP_Comms_ScriptName) NET_NL()
        NET_PRINT("...........The Cancelled Communication Group ID ") NET_PRINT(g_WIDGET_MP_Comms_GroupID) NET_PRINT("   Cancelled Root ID ") NET_PRINT(g_WIDGET_MP_Comms_ConvRootID) NET_NL()
    #ENDIF
    
    IF (Is_MP_Communication_A_Phonecall())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...........The Cancelled Communication is a Phonecall, so calling KILL_PHONE_CONVERSATION()") NET_NL()
        #ENDIF
        
        KILL_PHONE_CONVERSATION()
    ENDIF
    
    IF (Is_MP_Communication_A_Radio_Message())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...........The Cancelled Communication is a Radio Message, so calling KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()") NET_NL()
        #ENDIF
        
        KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
    ENDIF
    
    IF (Is_MP_Communication_A_Txtmsg())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...........The Cancelled Communication is a txtmsg, so UNLOCKING message then deleting it by label: ") NET_PRINT(g_sCommsMP.priorityComms.ccRootID) NET_NL()
        #ENDIF
        
        // Unlock the txtmsg before deleting it
		Delete_Txtmsgs_On_Text_Message_List(g_sCommsMP.priorityComms.ccRootID)
    ENDIF
    
    IF (Is_MP_Communication_An_Email())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...........The Cancelled Communication is an email, so UNLOCKING email then deleting it by label: ") NET_PRINT(g_sCommsMP.priorityComms.ccRootID) NET_NL()
        #ENDIF
        
        // Unlock the email before deleting it
		Delete_Email_On_Email_List(g_sCommsMP.priorityComms.ccRootID)
    ENDIF
    
    // Clear the MP Comms details
    Clear_Existing_MP_Communication_Request()

ENDPROC




// ===========================================================================================================
//      Communication Candidate Routines
// ===========================================================================================================

// PURPOSE: Check if the current highest priority request was made by the specified script.
//
// INPUT PARAMS:        paramHashScriptName         The script name hash
// RETURN VALUE:        BOOL                        TRUE if the specified script made the request, otherwise FALSE
FUNC BOOL Did_Specified_Script_Make_The_Existing_Request(INT paramHashScriptName)

    // Is there isn't an existing favoured communication then these communication parameters can't be a match
    IF NOT (Is_There_An_Existing_MP_Communication_Request())
        RETURN FALSE
    ENDIF

    // Check if these parameters match the script name hash of the script that made the request.
    // ...script name hash
    IF NOT (g_sCommsMP.priorityComms.ccHashScriptName = paramHashScriptName)
        RETURN FALSE
    ENDIF
    
    // Yes
    RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if these Communication details are the same as the currently favoured communication
//
// INPUT PARAMS:        paramHashScriptName         The hash key for the script making the request
//                      paramHashGroupRoot          The hash key for the Group/Root requested to be played
//						paramHashBodyText			[default = 0] The hash key for any Body Text Component, or 0 to ignore the Body Text
// RETURN VALUE:        BOOL                        TRUE if these details match the current favoured communication details, otherwise FALSE
FUNC BOOL Is_This_MP_Communication_Request_The_Existing_Request(INT paramHashScriptName, INT paramHashGroupRoot, INT paramHashBodyText = 0)

    // Check if the existing request was made by the specified script
    IF NOT (Did_Specified_Script_Make_The_Existing_Request(paramHashScriptName))
        RETURN FALSE
    ENDIF

    // Yes, it was made by the specified script
    // Does the exiting request match these block ID/conversation root hash details
    IF (g_sCommsMP.priorityComms.ccHashGroupRoot != paramHashGroupRoot)
        RETURN FALSE
    ENDIF
	
	// If the body text shouldn't be ignored, test it
	IF (paramHashBodyText != 0)
		// ...the Body Text shouldn't be ignored, so check for a match
		IF (g_sCommsMP.priorityComms.ccHashBodyText != paramHashBodyText)
			RETURN FALSE
		ENDIF
	ENDIF

    // The parameters match
    RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if these Communication details are the same as the current active communication
//
// INPUT PARAMS:        paramHashScriptName         The hash key for the script making the request
//                      paramHashGroupRoot          The hash key for the Group/Root requested to be played
//						paramHashBodyText			the hash key for the Body Text if there is any (0 if none)
// RETURN VALUE:        BOOL                        TRUE if these details match the current favoured communication details, otherwise FALSE
FUNC BOOL Is_This_MP_Communication_Request_The_Active_Communication(INT paramHashScriptName, INT paramHashGroupRoot, INT paramHashBodyText)

    // If there isn't an active communication then these parameters can't match
    IF NOT (Is_There_An_Active_MP_Communication_Request())
        RETURN FALSE
    ENDIF
    
    // There is an active communication.
    // Check if these parameters match the details of the active communication.
    RETURN (Is_This_MP_Communication_Request_The_Existing_Request(paramHashScriptName, paramHashGroupRoot, paramHashBodyText))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Check if the priority passed as parameter is greater than the priority of the existing request
//
// INPUT PARAMS:        paramCommsType              The type of communication request - which also acts as its priority
// RETURN VALUE:        BOOL                        TRUE if this priority is greater than the current favoured communication priority, otherwise FALSE
FUNC BOOL Is_This_MP_Communication_Request_Of_Higher_Priority_Than_Existing_Request(INT paramCommsType)
    RETURN (paramCommsType > g_sCommsMP.priorityComms.ccType)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Stores the details of the new communication request
//
// INPUT PARAMS:        paramHashScriptName         The hash key for the script making the request
//                      paramHashGroupRoot          The hash key for the Group/Root requested to be played
//						paramHashBodyText			The hash key for the Body Text - this will be 0 if there is none
//                      paramRootID                 The RootID (or Subtitle ID)
//                      paramCommsType              The type of communication request
//                      paramDevice                 The device requested for the communication
//						paramModifiers				Flags that change the standard behaviour
PROC Store_New_MP_Communication_Request_Details(INT paramHashScriptName, INT paramHashGroupRoot, INT paramHashBodyText, STRING paramRootID, INT paramCommsType, g_eMPCommsDevice paramDevice, INT paramModifiers)

    // Ensure the existing request isn't active - debug only because this should be tested prior to calling this function
    #IF IS_DEBUG_BUILD
        IF (Is_There_An_Active_MP_Communication_Request())
            SCRIPT_ASSERT("Store_New_MP_Communication_Request_Details(): Attempting to overwrite the details of an active communication")
            EXIT
        ENDIF
    #ENDIF

    // Ensure the new request is of higher priority - debug only because this should be tested prior to calling this function
    #IF IS_DEBUG_BUILD
        IF NOT (Is_This_MP_Communication_Request_Of_Higher_Priority_Than_Existing_Request(paramCommsType))
            SCRIPT_ASSERT("Store_New_MP_Communication_Request_Details(): Attempting to overwrite the details of a higher priority communication request")
            EXIT
        ENDIF
    #ENDIF
    
    // Store the details
    g_sCommsMP.priorityComms.ccHashScriptName   = paramHashScriptName
    g_sCommsMP.priorityComms.ccHashGroupRoot    = paramHashGroupRoot
	g_sCommsMP.priorityComms.ccHashBodyText		= paramHashBodyText
    g_sCommsMP.priorityComms.ccRootID           = paramRootID
    g_sCommsMP.priorityComms.ccType             = paramCommsType
    g_sCommsMP.priorityComms.ccDevice           = paramDevice
	g_sCommsMP.priorityComms.ccModifiers		= paramModifiers
    
    // Store the Initial Reply State
    Store_MP_Communication_Reply_Requirements(paramCommsType)
    
    // Store the state as 'Received this frame'
    Set_MP_Communication_Request_As_Received_This_Frame()
	
	// Store a headshot timeout time when a request becomes the new priority request - this will ensure comms are delayed significantly waiting for a headshot to be generated
	g_sCommsMP.priorityComms.ccHeadshotTimeout	= GET_TIME_OFFSET(GET_NETWORK_TIME(), MP_COMMS_HEADSHOT_GENERATION_TIMEOUT_msec)
    
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//      Communication Candidate Helper Routines
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Returns a Hash Key based on the Block and Root string appended together
//
// INPUT PARAMS:        paramGroupID                The Subtitle Group ID field from dialogueStar
//                      paramRootID                 The conversation Root field from dialogueStar
// RETURN VALUE:        INT                         The Hash Key
//
// NOTES:   Assume all string length checks have been performed
FUNC INT Get_Hash_Key_Block_And_Root(STRING paramGroupID, STRING paramRootID)

    TEXT_LABEL_63 groupRootCombo = paramGroupID
    groupRootCombo += paramRootID
    
    RETURN (GET_HASH_KEY(groupRootCombo))

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Checks if communication requests are currently being accepted
//
// INPUT PARAMS:		paramModifiers		Any Modifiers that may affect this behaviour
// RETURN VALUE:        BOOL        		TRUE if communication requests are currently being accepted, otherwise FALSE
FUNC BOOL Are_MP_Communication_Requests_Being_Accepted(INT paramModifiers)

    // Ensure MP Comms Devices aren't currently blocked
    IF (Are_MP_Comms_Devices_Disabled())
		#IF IS_DEBUG_BUILD
			IF NOT (g_mpCommsBlockedByDisabledComms)
	            NET_PRINT(".KGM [MPComms]: IGNORING MP COMMS REQUESTS - MP Comms Devices are currently disabled") NET_NL()
			
				g_mpCommsBlockedByDisabledComms = TRUE
			ENDIF
		#ENDIF
    
        RETURN FALSE
    ENDIF
	
	#IF IS_DEBUG_BUILD
		// Comms no longer blocked by Devices being disabled
		IF (g_mpCommsBlockedByDisabledComms)
			NET_PRINT(".KGM [MPComms]: NO LONGER IGNORING MP COMMS REQUESTS - MP Comms Devices are no longer disabled") NET_NL()
		
			g_mpCommsBlockedByDisabledComms = FALSE
		ENDIF
	#ENDIF

    // If there is an active communication then requests are not being accepted
    IF (Is_There_An_Active_MP_Communication_Request())
		#IF IS_DEBUG_BUILD
			IF NOT (g_mpCommsBlockedByActiveComms)
	            NET_PRINT(".KGM [MPComms]: IGNORING MP COMMS REQUESTS - There is currently an Active Comms") NET_NL()
			
				g_mpCommsBlockedByActiveComms = TRUE
			ENDIF
		#ENDIF
    
       RETURN FALSE
    ENDIF
	
	#IF IS_DEBUG_BUILD
		// Comms no longer blocked by an active comms
		IF (g_mpCommsBlockedByActiveComms)
			NET_PRINT(".KGM [MPComms]: NO LONGER IGNORING MP COMMS REQUESTS - There is no longer an Active Comms") NET_NL()
		
			g_mpCommsBlockedByActiveComms = FALSE
		ENDIF
	#ENDIF
    
    // If the cellphone is on-screen then requests are not being accepted.
    // NOTE: This is to catch when the player has brought up the cellphone.
    IF (IS_PHONE_ONSCREEN())
		#IF IS_DEBUG_BUILD
			IF NOT (g_mpCommsBlockedByPhoneOnscreen)
	            NET_PRINT(".KGM [MPComms]: IGNORING MP COMMS REQUESTS - The cellphone is currently on-screen") NET_NL()
			
				g_mpCommsBlockedByPhoneOnscreen = TRUE
			ENDIF
		#ENDIF
    
       RETURN FALSE
    ENDIF
	
	#IF IS_DEBUG_BUILD
		// Comms no longer blocked by the cellphoen being on-screen
		IF (g_mpCommsBlockedByPhoneOnscreen)
			NET_PRINT(".KGM [MPComms]: NO LONGER IGNORING MP COMMS REQUESTS - The cellphone is no longer on-screen") NET_NL()
		
			g_mpCommsBlockedByPhoneOnscreen = FALSE
		ENDIF
	#ENDIF
	
	// Block if the player is skyswooping (having to use the globals directly)
	IF (g_TransitionData.SwoopStage != SKYSWOOP_NONE)
		#IF IS_DEBUG_BUILD
			IF NOT (g_mpCommsBlockedBySkyswoop)
	            NET_PRINT(".KGM [MPComms]: IGNORING MP COMMS REQUESTS - There is an active Skyswoop") NET_NL()
			
				g_mpCommsBlockedBySkyswoop = TRUE
			ENDIF
		#ENDIF
    
       RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		// Comms no longer blocked by skyswoop
		IF (g_mpCommsBlockedBySkyswoop)
			NET_PRINT(".KGM [MPComms]: NO LONGER IGNORING MP COMMS REQUESTS - The player is no longer skyswooping") NET_NL()
		
			g_mpCommsBlockedBySkyswoop = FALSE
		ENDIF
	#ENDIF
	
	// Block if player is performing a post-heist celebration
	IF (g_bCelebrationScreenIsActive)
	OR (IS_POST_MISSION_SCENE_ACTIVE())
		#IF IS_DEBUG_BUILD
			IF NOT (g_mpCommsBlockedByPostHeistCelebration)
	            NET_PRINT(".KGM [MPComms]: IGNORING MP COMMS REQUESTS - Player is performing a post-heist celebration") NET_NL()
			
				g_mpCommsBlockedByPostHeistCelebration = TRUE
			ENDIF
		#ENDIF
    
       RETURN FALSE
	ENDIF
	
	#IF IS_DEBUG_BUILD
		// Comms no longer blocked by post-heist celebration
		IF (g_mpCommsBlockedByPostHeistCelebration)
			NET_PRINT(".KGM [MPComms]: NO LONGER IGNORING MP COMMS REQUESTS - The player is no longer performing a post-heist celebration") NET_NL()
		
			g_mpCommsBlockedByPostHeistCelebration = FALSE
		ENDIF
	#ENDIF
    
    // If there is a Recent Invite received then requests are not being accepted.
	IF NOT (IS_BIT_SET(paramModifiers, MP_COMMS_MODIFIER_IGNORE_RECENT_INVITE))
	    IF (Is_There_A_Recent_Invite())
			#IF IS_DEBUG_BUILD
				IF NOT (g_mpCommsBlockedByRecentInvite)
		            NET_PRINT(".KGM [MPComms]: IGNORING MP COMMS REQUESTS - There has been a Recent Joblist Invite") NET_NL()
				
					g_mpCommsBlockedByRecentInvite = TRUE
				ENDIF
			#ENDIF
	    
	       RETURN FALSE
	    ENDIF
		
		#IF IS_DEBUG_BUILD
			// Comms no longer blocked by a recent invite
			IF (g_mpCommsBlockedByRecentInvite)
				NET_PRINT(".KGM [MPComms]: NO LONGER IGNORING MP COMMS REQUESTS - There is no longer a Recent Joblist Invite") NET_NL()
			
				g_mpCommsBlockedByRecentInvite = FALSE
			ENDIF
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
	    	IF (Is_There_A_Recent_Invite())
				NET_PRINT(".KGM [MPComms]: Recent Invite blocks comms - IGNORED FOR THIS COMMS by use of MP_COMMS_MODIFIER_IGNORE_RECENT_INVITE modifier") NET_NL()
			ENDIF
		#ENDIF
	ENDIF
    
    // Communication requests can be accepted
    RETURN TRUE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the text message block ID (all text messages are in the same blockID)
//    
// RETURN VALUE:        STRING          A STRING containing the text message block ID
FUNC STRING Get_MP_Txtmsg_BlockID()
    RETURN "TXTMSG"
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Return the email block ID (all emails are in the same blockID)
//    
// RETURN VALUE:        STRING          A STRING containing the email block ID
//
// NOTES:	Currently in TXTMSG blockID
FUNC STRING Get_MP_Email_BlockID()
    RETURN "TXTMSG"
ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Communication Candidate Interface Routines
// -----------------------------------------------------------------------------------------------------------

// PURPOSE: All requests to play a communication message will come through here - one request will be selected and triggered, the others will be denied
//
// INPUT PARAMS:        paramCharSheetID            	The CharSheetID of the caller
//                      paramGroupID                	The Subtitle Group ID field from dialogueStar
//                      paramRootID                 	The conversation Root field from dialogueStar
//                      paramCommsType              	An INT representing the type of Comms - also used to prioritise (CONST_INTs defined in MP_globals_Comms.sch)
//                      paramDevice                 	The Device ID with which to play the message
//						paramModifiers					Flags indicating modifiers to the basic behaviour (initially used for text messages, not for phonecalls)
//                      paramComponentTextLabel     	The Substring Component: TextLabel (as String)
//						paramComponentIsPlayerName		The Text Label component is a player name
//                      paramComponentInt           	The Substring Component: Int
//                      paramComponentLiteralString1	The first Literal String Component
//                      paramComponentLiteralString2	The second Literal String Component
//                      paramPlayerIdAsInt          	[DEFAULT= MP_COMMS_ILLEGAL_PLAYER_INDEX] If required, the Player Index as an INT
// REFERENCE PARAMS:    paramConversation           	The ped dialogue struct containing voice details
// RETURN VALUE:        BOOL                        	TRUE if this communication is the one that is active, otherwise FALSE
//
// NOTES:   See NOTES in file header for how the communication candidate system chooses a communication to play.
//          Assume the Components Struct contains legal data - there are functions used to fill the struct that contain error checking.
FUNC BOOL Request_Play_MP_Communication(structPedsForConversation &paramConversation, enumCharacterList paramCharSheetID, STRING paramGroupID, STRING paramRootID, INT paramCommsType, g_eMPCommsDevice paramDevice, INT paramModifiers, STRING paramComponentTextLabel, BOOL paramComponentIsPlayerName, INT paramComponentInt, STRING paramComponentLiteralString1, STRING paramComponentLiteralString2, INT paramPlayerIdAsInt = MP_COMMS_ILLEGAL_PLAYER_INDEX)
	
	#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_SkipAllMpComms")
			RETURN TRUE
		ENDIF
	#ENDIF
	
    IF (Is_String_Null_Or_Empty(paramGroupID))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Request_Play_MP_Communication() - Subtitle Group ID string is NULL or EMPTY") NET_NL()
            Debug_Output_MP_Comms_Details(GET_THIS_SCRIPT_NAME(), paramGroupID, paramRootID, paramCommsType, paramDevice, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2)
            SCRIPT_ASSERT("Request_Play_MP_Communication(): The Group ID string is NULL or empty - This should match the 'Subtitle Group ID' value in dialogueStar")
        #ENDIF
        
        RETURN FALSE
    ENDIF

    IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramGroupID) > MAX_LENGTH_MP_COMMS_GROUP_ID_STRING)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Request_Play_MP_Communication() - Subtitle Group ID string is too long") NET_NL()
            Debug_Output_MP_Comms_Details(GET_THIS_SCRIPT_NAME(), paramGroupID, paramRootID, paramCommsType, paramDevice, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2)
            SCRIPT_ASSERT("Request_Play_MP_Communication(): The Group ID string is longer than expected. Tell Keith.")
        #ENDIF
        
        RETURN FALSE
    ENDIF
    
    IF (Is_String_Null_Or_Empty(paramRootID))
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Request_Play_MP_Communication() - Root ID string is NULL or EMPTY") NET_NL()
            Debug_Output_MP_Comms_Details(GET_THIS_SCRIPT_NAME(), paramGroupID, paramRootID, paramCommsType, paramDevice, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2)
            SCRIPT_ASSERT("Request_Play_MP_Communication(): The Group ID string is NULL or empty - This should match the 'Root' value in dialogueStar")
        #ENDIF
        
        RETURN FALSE
    ENDIF

    IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramRootID) > MAX_LENGTH_MP_COMMS_ROOT_ID_STRING)
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Request_Play_MP_Communication() - Root ID string is too long") NET_NL()
            Debug_Output_MP_Comms_Details(GET_THIS_SCRIPT_NAME(), paramGroupID, paramRootID, paramCommsType, paramDevice, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2)
            SCRIPT_ASSERT("Request_Play_MP_Communication(): The Root ID string is longer than expected. Tell Keith.")
        #ENDIF
        
        RETURN FALSE
    ENDIF

    // Identify this communication
    INT hashScriptName  = GET_HASH_KEY(GET_THIS_SCRIPT_NAME())
    INT hashBlockRoot   = Get_Hash_Key_Block_And_Root(paramGroupID, paramRootID)
	INT hashBodyText	= 0
	IF NOT (IS_STRING_NULL_OR_EMPTY(paramComponentTextLabel))
		hashBodyText = GET_HASH_KEY(paramComponentTextLabel)
	ENDIF
    
    // Check if this communication is the one that is active
    IF (Is_This_MP_Communication_Request_The_Active_Communication(hashScriptName, hashBlockRoot, hashBodyText))
        // ...yes, this communication is active - the script shouldn't be requesting this communication again
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Request_Play_MP_Communication() - Request to play communication but script already knows communication is active") NET_NL()
            Debug_Output_MP_Comms_Details(GET_THIS_SCRIPT_NAME(), paramGroupID, paramRootID, paramCommsType, paramDevice, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2)
            SCRIPT_ASSERT("Request_Play_MP_Communication() - request made to play an already active MP communication - check logs for more details. Tell Keith.")
        #ENDIF
        
        RETURN TRUE
    ENDIF

    // This communication isn't active.
    // Are requests being accepted?
    IF NOT (Are_MP_Communication_Requests_Being_Accepted(paramModifiers))
        RETURN FALSE
    ENDIF
    
    // Communication requests are being accepted so there currently can't be an active communication
    // Is this request the currently favoured request being requested again?
    IF (Is_This_MP_Communication_Request_The_Existing_Request(hashScriptName, hashBlockRoot, hashBodyText))
        // ...yes, this is the existing request, so make sure it isn't being received again on the same frame
        IF (Was_MP_Communication_Request_Received_This_Frame())
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [MPComms]: ERROR: Request_Play_MP_Communication() - Request to play communication but request already received on the same frame") NET_NL()
                Debug_Output_MP_Comms_Details(GET_THIS_SCRIPT_NAME(), paramGroupID, paramRootID, paramCommsType, paramDevice, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2)
            #ENDIF
            
            RETURN FALSE
        ENDIF
    
        // Need to update the 'Received this frame' status to ensure the details don't get cleared next frame if the comms doesn't start immediately
        Set_MP_Communication_Request_As_Received_This_Frame()
        
        // Communication Request was received again on the next frame and is still the highest priority request.
        // Try to play the request - return TRUE if the request is now active, otherwise FALSE
        RETURN (Play_MP_Communication(paramConversation, paramCharSheetID, paramPlayerIdAsInt, paramGroupID, paramRootID, paramCommsType, paramDevice, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2))
    ENDIF
    
    // This request is not the currently favoured request.
    // Is this request of higher priority than the currently favoured request?
    IF NOT (Is_This_MP_Communication_Request_Of_Higher_Priority_Than_Existing_Request(paramCommsType))
        // ...no, this request is of lower or equal priority
        RETURN FALSE
    ENDIF
    
    // This request is of higher priority than the existing favoured request.
    // Store the details of this request.
    Store_New_MP_Communication_Request_Details(hashScriptName, hashBlockRoot, hashBodyText, paramRootID, paramCommsType, paramDevice, paramModifiers)
    
    #IF IS_DEBUG_BUILD
        // Store the text widgets that display text that is otherwise non-persistent
        g_WIDGET_MP_Comms_ScriptName            = GET_THIS_SCRIPT_NAME()
        g_WIDGET_MP_Comms_CharSheetID           = Convert_MP_CharSheetID_To_String(paramCharSheetID)
        IF (paramPlayerIdAsInt = MP_COMMS_ILLEGAL_PLAYER_INDEX)
			PRINTLN("[MMM] ILLEGAL PLAYER INDEX")
            g_WIDGET_MP_Comms_PlayerID          = ""
        ELSE
			PRINTLN("[MMM] VALID PLAYER INDEX")
            g_WIDGET_MP_Comms_PlayerID          = GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, paramPlayerIdAsInt))
        ENDIF
        g_WIDGET_MP_Comms_GroupID               = paramGroupID
        g_WIDGET_MP_Comms_ConvRootID            = paramRootID
        g_WIDGET_MP_Comms_CommsTypeID           = Convert_MP_Comms_Type_To_String(paramCommsType)
        g_WIDGET_MP_Comms_DeviceID              = Convert_MP_Comms_Device_To_String(paramDevice)
        g_WIDGET_MP_Comms_Component_TL          = paramComponentTextLabel
        
        Update_MP_Communications_Text_Widgets()
        
        // Also store any non-string components
        g_WIDGET_MP_Comms_Component_INT         = paramComponentInt
        
        // Output details to the console log
        NET_NL() NET_PRINT("...KGM MP [MPComms]: There is a new highest priority communication request") NET_NL()
        Debug_Output_MP_Comms_Details(g_WIDGET_MP_Comms_ScriptName, paramGroupID, paramRootID, paramCommsType, paramDevice, paramModifiers, paramComponentTextLabel, paramComponentIsPlayerName, paramComponentInt, paramComponentLiteralString1, paramComponentLiteralString2)
    #ENDIF
    
    RETURN FALSE

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Requests that the active MP Communication gets cancelled.
//
// INPUT PARAMS:        paramScriptName         Filled only if the communication being cancelled was requested by this script name.
//                      paramGroupID            Filled only if a specific communication is being cancelled.
//                      paramRootID             Filled only if a specific communication is being cancelled.
PROC Cancel_MP_Communication(STRING paramScriptName, STRING paramGroupID, STRING paramRootID)

    // Look for a specific communication to cancel
    BOOL    specificComms   = FALSE
    INT     groupRootHash   = 0
    
    IF NOT (Is_String_Null_Or_Empty(paramGroupID))
    AND NOT (Is_String_Null_Or_Empty(paramRootID))
        // Specific Comms required
        IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramGroupID) > MAX_LENGTH_MP_COMMS_GROUP_ID_STRING)
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Cancel_MP_Comunication() - Subtitle Group ID string is too long") NET_NL()
                NET_PRINT("      Group ID ") NET_PRINT(paramGroupID) NET_PRINT("   Root ID ") NET_PRINT(paramRootID) NET_NL()
                SCRIPT_ASSERT("Cancel_MP_Comunication(): The Group ID string is longer than expected. Tell Keith.")
            #ENDIF
            
            EXIT
        ENDIF

        IF (GET_LENGTH_OF_LITERAL_STRING_IN_BYTES(paramRootID) > MAX_LENGTH_MP_COMMS_ROOT_ID_STRING)
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Cancel_MP_Comunication() - Root ID string is too long") NET_NL()
                NET_PRINT("      Group ID ") NET_PRINT(paramGroupID) NET_PRINT("   Root ID ") NET_PRINT(paramRootID) NET_NL()
                SCRIPT_ASSERT("Cancel_MP_Comunication(): The Root ID string is longer than expected. Tell Keith.")
            #ENDIF
            
            EXIT
        ENDIF
        
        // Generate the Group/Root combo hash
        groupRootHash   = Get_Hash_Key_Block_And_Root(paramGroupID, paramRootID)
        specificComms   = TRUE
    ENDIF
    
    // Look for a specific script
    BOOL    specificScript  = FALSE
    INT     scriptNameHash  = 0
    
    IF NOT (Is_String_Null_Or_Empty(paramScriptName))
        scriptNameHash  = GET_HASH_KEY(paramScriptName)
        specificScript  = TRUE
    ENDIF
    
    // A debug only error check - because I don't think this is possible
    #IF IS_DEBUG_BUILD
        IF (specificComms)
        AND NOT (specificScript)
            NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Cancel_MP_Comunication() - Data inconsistency: the script name is NULL - this should be impossible!") NET_NL()
            NET_PRINT("      Group ID ") NET_PRINT(paramGroupID) NET_PRINT("   Root ID ") NET_PRINT(paramRootID) NET_NL()
            SCRIPT_ASSERT("Cancel_MP_Comunication(): A GroupID and RootID received but without a script name. Tell Keith.")
            
            EXIT
        ENDIF
    #ENDIF
    
    // Is the request for a specific communication to be cancelled?
    IF (specificComms)
    AND (specificScript)
        // ...yes, so cancel communication if it is the current highest priority communication
        IF (Is_This_MP_Communication_Request_The_Existing_Request(scriptNameHash, groupRootHash))
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [MPComms]: MP Specific Comms cancelled. Cancel request came from: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
            #ENDIF
            
            Cancel_Existing_MP_Communication()
        ENDIF
        
        EXIT
    ENDIF
    
    // Specific communication isn't being cancelled, so check if the request is for any communication from a specific script to be cancelled?
    IF (specificScript)
        // ...yes, so cancel communication if the current highest priority communication was requested by the specified script
        IF (Did_Specified_Script_Make_The_Existing_Request(scriptNameHash))
            #IF IS_DEBUG_BUILD
                NET_PRINT("...KGM MP [MPComms]: MP Comms Requested by Specific Script cancelled. Cancel request came from: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
            #ENDIF
            
            Cancel_Existing_MP_Communication()
        ENDIF
        
        EXIT
    ENDIF
    
    // Otherwise cancel any current communication
    IF (Is_There_An_Existing_MP_Communication_Request())
        #IF IS_DEBUG_BUILD
            NET_PRINT("...KGM MP [MPComms]: Any MP Comms cancelled. Cancel request came from: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_NL()
        #ENDIF
        
        Cancel_Existing_MP_Communication()
    ENDIF

ENDPROC



// ===========================================================================================================
//      The Main MP Communications Control Routines
// ===========================================================================================================

// PURPOSE: A one-off EOM initialisation.
PROC Initialise_MP_Communications()

    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [MPComms]: Initialise_MP_Communications") NET_NL()
    #ENDIF

    BOOL beingInitialised = TRUE
    Clear_Existing_MP_Communication_Request(beingInitialised)
	g_playerTxtmsgHeadshotMP	= NULL
	
	#IF IS_DEBUG_BUILD
		// Any special debug controls - these are to prevent console log spamming
		g_mpCommsBlockedByDisabledComms	= FALSE
		g_mpCommsBlockedByActiveComms	= FALSE
		g_mpCommsBlockedByPhoneOnscreen	= FALSE
		g_mpCommsBlockedByRecentInvite	= FALSE
		g_mpCommsBlockedBySkyswoop		= FALSE
	#ENDIF
	
	// Voice Chat Block clearout
	g_voiceChatBlockMP 	= MP_COMMS_VC_BLOCK_NONE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain the Cellphone Replies - if cellphone communication
PROC Maintain_MP_Communication_Cellphone_Replies()

    // Is communication a phonecall?
    IF NOT (Is_MP_Communication_A_Phonecall())
        EXIT
    ENDIF

    // Communication is a phonecall, so get the cellphone reply
    enumCellphonePromptResponse theReply = CHECK_RESPONSE_TO_MP_JOB_OFFER_PROMPT()
    SWITCH (theReply)
        // Still waiting?
        CASE RESPONSE_STORE_EMPTY
            EXIT
            
        // Received YES?
        CASE RESPONDED_YES
            Set_MP_Communication_Reply_As_Yes()
            EXIT
            
        // Received NO?
        CASE RESPONDED_NO
            Set_MP_Communication_Reply_As_No()
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...........Cancelling Communication because player has responded NO") NET_NL()
            #ENDIF
            
            Cancel_Existing_MP_Communication()
            EXIT
    ENDSWITCH
    
    // Unexpected Response
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [MPComms] EXTRA DETAILS FOR ASSERT: Maintain_MP_Communication_Replies() - Cellphone reply was something other than YES, NO, or EMPTY") NET_NL()
        Debug_Output_MP_Comms_Details_For_Existing_Request()
    #ENDIF
    
    SCRIPT_ASSERT("Maintain_MP_Communication_Replies(): Job Offer response from cellphone was unexpected - see console log for details. Tell Keith (or Steve).")
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain the Txtmsg Replies - if txtmsg communication
PROC Maintain_MP_Communication_Txtmsg_Replies()

    // Is communication a txtmsg?
    IF NOT (Is_MP_Communication_A_Txtmsg())
        EXIT
    ENDIF

    // Communication is a text message, so get the txtmsg reply
    TEXT_LABEL_15 theSubtitleID = Get_MP_Communication_RootID()
    enumTxtMsgIsReplyRequired theReply = GET_TEXT_MESSAGE_REPLY_STATUS(theSubtitleID)
    SWITCH (theReply)
        // Received YES?
        CASE REPLIED_YES
            Set_MP_Communication_Reply_As_Yes()
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...........Cancelling Txtmsg Communication after player response (YES)") NET_NL()
            #ENDIF
            
            Cancel_Existing_MP_Communication()
            HANG_UP_AND_PUT_AWAY_PHONE()
            EXIT
            
        // Received NO?
        CASE REPLIED_NO
            Set_MP_Communication_Reply_As_No()
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...........Cancelling Txtmsg Communication after player response (NO)") NET_NL()
            #ENDIF
            
            Cancel_Existing_MP_Communication()
            HANG_UP_AND_PUT_AWAY_PHONE()
            EXIT
            
        // Received BARTER?
        CASE REPLIED_BARTER
            Set_MP_Communication_Reply_As_Special()
            
            #IF IS_DEBUG_BUILD
                NET_PRINT("...........Cancelling Txtmsg Communication after player response(SPECIAL RESPONSE)") NET_NL()
            #ENDIF
            
            Cancel_Existing_MP_Communication()
            HANG_UP_AND_PUT_AWAY_PHONE()
            EXIT
    ENDSWITCH
    
    // Still waiting
    
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintains the communication replies - update the state variable
// NOTES:   Assumes all checks to ensure a conversation is in progress have been made prior to calling this function
PROC Maintain_MP_Communication_Replies()

    // Is a reply expected?
    IF NOT (Is_MP_Communication_Waiting_For_A_Reply())
        EXIT
    ENDIF
    
    // A reply is expected - has a reply been given?
    Maintain_MP_Communication_Cellphone_Replies()
    Maintain_MP_Communication_Txtmsg_Replies()
	//Maintain_MP_Communication_Email_Replies()	// TODO: Do we need an emails reply function?

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Ensure the communication requests - update the state variable
PROC Maintain_Active_MP_Communication()

    // Do nothing if there isn't an active communication
    IF NOT (Is_There_An_Active_MP_Communication_Request())
        EXIT
    ENDIF
    
    // There is an active communication, so check if it is still ongoing
    IF (Is_The_Active_Communication_Still_Active())
        // ...still ongoing - if a reply is expected, check for the reply
        Maintain_MP_Communication_Replies()
        
        // If the reply is STILL expected after checking for replies, then check for timeout
        IF (Is_MP_Communication_Waiting_For_A_Reply())
            // ...still waiting for a reply, so check for timeout
            IF (Has_MP_Communication_Reply_Timer_Expired())
                // ...reply timer has expired, so set reply as NO
                Set_MP_Communication_Reply_As_No()
    
                // ...cancel the communication (since it is still active)
                #IF IS_DEBUG_BUILD
                    NET_PRINT("...KGM MP [MPComms]: The Active MP Communication has timed out waiting for a reply") NET_NL()
                #ENDIF
                
                Cancel_Existing_MP_Communication()
            ENDIF
        ENDIF
        
        EXIT
    ENDIF
    
    // The active communication is no longer ongoing, so force a 'no' reply if a reply was still required and not received
    IF (Is_MP_Communication_Waiting_For_A_Reply())
        Set_MP_Communication_Reply_As_No()
    ENDIF
    
    // Clear out the communication request variables
    #IF IS_DEBUG_BUILD
        NET_PRINT("...KGM MP [MPComms]: The Active MP Communication has ended") NET_NL()
    #ENDIF
    
    Clear_Existing_MP_Communication_Request()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain Check to see if a recently active communication was aborted
// NOTE:	The ended successfully flag will stick around until the next communication is playing
PROC Maintain_Aborted_Communication_Check()

    // Do nothing if there is still an active communication
    IF (Is_There_An_Active_MP_Communication_Request())
        EXIT
    ENDIF
	
	// There isn't an active communication, so check if an aborted check is required (this will only be required in teh frame after an active communication ends)
	IF NOT (g_sCommsMP.commsEndStatus.cesRequiresAbortedCheck)
		EXIT
	ENDIF
	
	// This communication requires a success check, so perform the check and clear the flag to prevent it running again next frame
	g_sCommsMP.commsEndStatus.cesRequiresAbortedCheck = FALSE

	// Phonecall Aborted after acceptance?
	IF (WAS_LAST_PHONECALL_ABORTED_BY_SCRIPT_HANG_UP())
		// ...was aborted, so didn't end successfully
		g_sCommsMP.commsEndStatus.cesEndedSuccessfully = FALSE
		
		#IF IS_DEBUG_BUILD
		       NET_PRINT("...KGM MP [MPComms]: Call was aborted so did not end successfully") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF

	// Phonecall answered?
	IF (CHECK_CELLPHONE_LAST_CALL_REJECTED())
		// ...wasn't answered, so didn't end successfully
		g_sCommsMP.commsEndStatus.cesEndedSuccessfully = FALSE
		
		#IF IS_DEBUG_BUILD
		       NET_PRINT("...KGM MP [MPComms]: Call was not answered so did not end successfully") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Wasn't aborted or rejected, so ended successfully
	g_sCommsMP.commsEndStatus.cesEndedSuccessfully = TRUE
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Maintain Pending Communication Requests - update the state variable
PROC Maintain_MP_Communication_Requests()

    // Do nothing if the request is active (this is updated elsewhere)
    IF (Is_There_An_Active_MP_Communication_Request())
        EXIT
    ENDIF
    
    // Do nothing if there is no current request
    IF NOT (Is_There_An_Existing_MP_Communication_Request())
        EXIT
    ENDIF
    
    // If the favoured communication request was received in the previous frame, then delete it because the request wasn't made again on this frame
    IF (Was_MP_Communication_Request_Received_On_Previous_Frame())
        Clear_Existing_MP_Communication_Request()
        EXIT
    ENDIF
    
    // If the favoured communication request was received on this frame then mark it as received on previous frame - this is done to ensure it is
    //      getting repeatedly requested and also used to help check it isn't requested more than once on the same frame
    IF (Was_MP_Communication_Request_Received_This_Frame())
        Set_MP_Communication_Request_As_Received_On_Previous_Frame()
        EXIT
    ENDIF
    
    // Report an unknown Communications Candidate state
    #IF IS_DEBUG_BUILD
        SCRIPT_ASSERT("Maintain_MP_Communication_Requests() - unknown state for existing MP Communication Request. Tell Keith.")
    #ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Keep disabling the cellphone each frame until the block times out
PROC Maintain_Temporary_MP_Comms_Block()

	IF NOT (g_sCommsBlockMP.blockActive)
		EXIT
	ENDIF
	
	// Block is active, so check if it has timed out
	IF (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sCommsBlockMP.blockTimeout))
		// ...block has timed out
		g_sCommsBlockMP.blockActive = FALSE
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("...KGM MP [MPComms]: Temporary MP Comms Block Has Timed Out") NET_NL()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Keep disabling the cellphone
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Checks if the incoming calls block has timed out
PROC Maintain_Temporary_MP_Comms_Incoming_Calls_Block()

	IF NOT (g_sCommsBlockMP.blockIncomingCalls)
		EXIT
	ENDIF
	
	// Block is active, so check if it has timed out
	IF (IS_TIME_LESS_THAN(GET_NETWORK_TIME(), g_sCommsBlockMP.blockIncomingCallsTimeout))
		EXIT
	ENDIF
	
	// ...block has timed out, so allow incoming calls again
	g_sCommsBlockMP.blockIncomingCalls = FALSE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP [MPComms]: ")
		NET_PRINT_TIME()
		NET_PRINT(" Temporary MP Comms Incoming Calls Block Has Timed Out") NET_NL()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: This procedure should be called every frame to control access to the communications systems.
// NOTES:   End Of Mission Message will update from the game-specific client script.
PROC Maintain_MP_Communications()

    #IF IS_DEBUG_BUILD
        Maintain_MP_Communications_Widgets()
    #ENDIF
	
	// If the Blade is on-screen, disable the cellphone
	IF (IS_WARNING_MESSAGE_ACTIVE())
		IF (IS_CELLPHONE_CAMERA_IN_USE())
		OR g_bRunQuitHeistWarningScreen
		OR g_sBossAgencyApp.bBossAgencyAppRunningWarningScreen
			// BUG: 1355619 - we may need more refinement for this
//			#IF IS_DEBUG_BUILD
//				#IF FEATURE_HEIST_PLANNING
//				IF g_bRunQuitHeistWarningScreen
//					NET_PRINT("KGMTEMP-----> Phone SHOULD BE Disabled This Frame for HUD on-screen, but YOUR FLAG IS TRUE")
//				ELSE
//				#ENDIF
//					NET_PRINT("KGMTEMP-----> Phone SHOULD BE Disabled This Frame for HUD on-screen, but the Camera App is onscreen and the message may be from it, so not disabling.") NET_NL()
//				#IF FEATURE_HEIST_PLANNING
//				ENDIF
//				#ENDIF
//				#IF FEATURE_GANG_BOSS
//				IF g_sBossAgencyApp.bBossAgencyAppRunningWarningScreen
//					NET_PRINT("KGMTEMP-----> Phone SHOULD BE Disabled This Frame for HUD on-screen, but g_sBossAgencyApp.bBossAgencyAppRunningWarningScreen = TRUE")
//				ENDIF
//				#ENDIF
//			#ENDIF
		ELSE
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			
			//#IF IS_DEBUG_BUILD
				//NET_PRINT("KGMTEMP-----> Phone Disabled This Frame for HUD on-screen") NET_NL()
			//#ENDIF
		ENDIF
	ENDIF
	
	// Maintain the temporary MP Comms Blocks
	Maintain_Temporary_MP_Comms_Block()
	Maintain_Temporary_MP_Comms_Incoming_Calls_Block()

    // Maintain an active Communication - check for it ending, etc
    Maintain_Active_MP_Communication()
	
	// Maintain Check for the active communication being aborted
	Maintain_Aborted_Communication_Check()

    // Maintain the highest priority Communication Request
    Maintain_MP_Communication_Requests()
	
	// Maintain Voice Chat Block
	Maintain_Voice_Chat_Block()

ENDPROC



