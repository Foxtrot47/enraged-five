/* ===================================================================================== *\
		MISSION NAME	:	net_heists_pre_planning.sch
		AUTHOR			:	Alastair Costley - 2014/02/12
		DESCRIPTION		:	Contains code related to the pre-planning board that sits in the player's apt.
\* ===================================================================================== */


// INCLUDE FILES.
USING "globals.sch"
USING "commands_network.sch"
USING "net_include.sch"
USING "context_control_public.sch"
USING "net_ambience.sch"
USING "MP_Scaleform_Functions.sch"
USING "fmmc_corona_controller.sch"
USING "net_heists_event_control.sch"
USING "net_heists_scaleform_header.sch"
USING "fmmc_mp_setup_mission.sch"
USING "fmmc_cloud_loader.sch"
USING "mp_globals_common_definitions.sch"
USING "net_heists_common.sch"
USING "net_heists_strand.sch"
USING "menu_cursor_3d_public.sch"
// Board pos -781.85, 327.10, 208.20


TWEAK_FLOAT PLANNING_MOUSE_BASE_HEIGHT	0.052	
TWEAK_FLOAT PLANNING_MOUSE_GAP_ADJUST 	0.085

#IF IS_DEBUG_BUILD
BOOL bRenderDebugText = FALSE
BOOL bDebugResetSlotCachedData = FALSE
INT DEBUG_eFocusPoint, DEBUG_eHelpFlowState
WIDGET_GROUP_ID DEBUG_PrePlanWidgets
WIDGET_GROUP_ID DEBUG_PlanningParentWidgets
#ENDIF

#IF IS_DEBUG_BUILD
PROC DRAW_HEIST_PRE_PLANNING_WIDGETS()

	PRINTLN("[AMEC][DEBUG] - DRAW_HEIST_PRE_PLANNING_WIDGETS - Drawing heist pre-planning widgets.")

	INT index, i
	TEXT_LABEL_63 tlTemp1
	TEXT_LABEL_63 tlTemp2
	
	TEXT_LABEL_3 tlTempStr1 = "]["
	TEXT_LABEL_3 tlTempStr2 = "]: "
	

	DEBUG_PrePlanWidgets = DEBUG_PrePlanWidgets
	SET_CURRENT_WIDGET_GROUP(DEBUG_PlanningParentWidgets)
	DEBUG_PrePlanWidgets = START_WIDGET_GROUP("Heist Pre-Planning") 
	
		ADD_WIDGET_BOOL("Draw debug text on-screen?", bRenderDebugText)	
		ADD_WIDGET_BOOL("g_HeistPrePlanningClient.bSimulateHeistPropData", g_HeistPrePlanningClient.bSimulateHeistPropData)	
		ADD_WIDGET_BOOL("g_HeistPrePlanningClient.bDeleteHeistPropData", g_HeistPrePlanningClient.bDeleteHeistPropData)
		ADD_WIDGET_BOOL("g_HeistPrePlanningClient.bUpdateHeistPropData", g_HeistPrePlanningClient.bUpdateHeistPropData)
		
		START_WIDGET_GROUP("Variables - BD - Don't fuck with this.") 
		
			ADD_WIDGET_BOOL("bAllPrepsDone: ", GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bAllPrepsDone)
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Variables - Server - Don't fuck with this.") 

		STOP_WIDGET_GROUP()
				
		START_WIDGET_GROUP("Variables - Local - You can fuck with this.") 
		
			ADD_WIDGET_STRING(" ---------- INTEGERS ---------- ")
			
			ADD_WIDGET_INT_SLIDER("Button Press Delay", 			g_tiBUTTON_PRESS_DELAY, 0, 2500, 1)
			
			ADD_WIDGET_INT_SLIDER("iSelectedSetupMissions[0]: ", 	g_HeistPrePlanningClient.iSelectedSetupMissions[0], -100, 100, 1)
			ADD_WIDGET_INT_SLIDER("iSelectedSetupMissions[1]: ",	g_HeistPrePlanningClient.iSelectedSetupMissions[1], -100, 100, 1)
			ADD_WIDGET_INT_SLIDER("iSelectedSetupMissions[2]: ", 	g_HeistPrePlanningClient.iSelectedSetupMissions[2], -100, 100, 1)
			ADD_WIDGET_INT_SLIDER("iSelectedSetupMissions[3]: ", 	g_HeistPrePlanningClient.iSelectedSetupMissions[3], -100, 100, 1)
			ADD_WIDGET_INT_SLIDER("iSelectedSetupMissions[4]: ", 	g_HeistPrePlanningClient.iSelectedSetupMissions[4], -100, 100, 1)
			ADD_WIDGET_INT_SLIDER("iSelectedSetupMissions[5]: ", 	g_HeistPrePlanningClient.iSelectedSetupMissions[5], -100, 100, 1)
			
			ADD_WIDGET_INT_SLIDER("iCameraLocationIndex: ", 		g_HeistPrePlanningClient.iCameraLocationIndex, -100, 100, 1)
			ADD_WIDGET_INT_SLIDER("iTotalPrePlanRows: ", 			g_HeistPrePlanningClient.iTotalPrePlanRows, -100, 100, 1)
			ADD_WIDGET_INT_SLIDER("iAvailablePrePlanRows: ", 		g_HeistPrePlanningClient.iAvailablePrePlanRows, -100, 100, 1)
			ADD_WIDGET_INT_SLIDER("iCurrentBoardDepth: ", 			g_HeistPrePlanningClient.iCurrentBoardDepth, -100, 100, 1)
			
			ADD_WIDGET_INT_SLIDER("iHeistPrePlanContext: ", 		g_HeistPrePlanningClient.iHeistPrePlanContext, -100, 100, 1)
			ADD_WIDGET_INT_SLIDER("iHelpTextStateCached: ", 		g_HeistPrePlanningClient.iHelpTextStateCached, -100, 100, 1)
			ADD_WIDGET_INT_SLIDER("iCurrentPropertyOwnerIndex: ", 	g_HeistSharedClient.iCurrentPropertyOwnerIndex, -100, 100, 1)
			//ADD_WIDGET_INT_SLIDER("iRenderphaseBitset: ", 			g_HeistPrePlanningClient.iRenderphaseBitset, -100, 100, 1)
			ADD_WIDGET_INT_SLIDER("iHeistPlanningStatusBitset: ", 	g_HeistPrePlanningClient.iHeistPlanningStatusBitset, -100, 100, 1)
			
			ADD_WIDGET_INT_SLIDER("iPreReqFrameCount: ", 			g_HeistPrePlanningClient.iPreReqFrameCount, 0, 10, 1)
			ADD_WIDGET_INT_SLIDER("iBackgroundFrameCount: ", 		g_HeistPrePlanningClient.iBackgroundFrameCount, 0, 10, 1)
			ADD_WIDGET_INT_SLIDER("iInUseFrameCount: ", 			g_HeistPrePlanningClient.iInUseFrameCount, 0, 10, 1)
			
			ADD_WIDGET_STRING(" ---------- BOOLEANS ---------- ")
			
			ADD_WIDGET_BOOL("bHaveForcedFinaleMission", 			g_HeistPrePlanningClient.bHaveForcedFinaleMission)
			ADD_WIDGET_BOOL("bIsHeistPlanningActive", 				g_HeistPrePlanningClient.bIsHeistPlanningActive)
			ADD_WIDGET_BOOL("bAutoProgressToConfigure", 			g_TransitionSessionNonResetVars.bAutoProgressToConfigure)
			ADD_WIDGET_BOOL("bHaveDoneHelp", 						g_HeistPrePlanningClient.bHaveDoneHelp)
			ADD_WIDGET_BOOL("bHaveSessionMemberNames", 				g_HeistPrePlanningClient.bHaveSessionMemberNames)
			ADD_WIDGET_BOOL("bVerbosePropLogging", 					g_HeistPrePlanningClient.bVerbosePropLogging)
			ADD_WIDGET_BOOL("Reset bDebugResetSlotCachedData", 		bDebugResetSlotCachedData) 
			
			ADD_WIDGET_STRING(" ---------- PC MOUSE ---------- ")
			
			ADD_WIDGET_FLOAT_SLIDER("fBaseHeight: ", 	PLANNING_MOUSE_BASE_HEIGHT, 0.0, 1.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("fGapFudgeFactor: ", 	PLANNING_MOUSE_GAP_ADJUST, 0.0, 1.0, 0.01)
			
			ADD_WIDGET_STRING(" ---------- STRINGS ---------- ")
			
			tlTemp1 = "tlHeistFinaleRootContID: "
			tlTemp2 = g_HeistPrePlanningClient.tlHeistFinaleRootContID
			tlTemp1 += tlTemp2
			ADD_WIDGET_STRING(tlTemp1)
			
			FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
				FOR i = 0 TO (MAX_HEIST_PLAYERS_PER_SETUP-1)
					tlTemp1 = "tlMissionPlayerName["
					tlTemp1 += index
					tlTemp1 += tlTempStr1 // "]["
					tlTemp1 += i
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_STRING(tlTemp1)
					ADD_WIDGET_STRING(g_HeistSharedClient.tlMissionPlayerName[index][i])
				ENDFOR
			ENDFOR
			
			FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
				FOR i = 0 TO (MAX_HEIST_PLAYERS_PER_SETUP-1)
					tlTemp1 = "tlMissionPlayerUID["
					tlTemp1 += index
					tlTemp1 += tlTempStr1 // "]["
					tlTemp1 += i
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_STRING(tlTemp1)
					ADD_WIDGET_STRING(g_HeistSharedClient.tlMissionPlayerUID[index][i])
				ENDFOR
			ENDFOR
			
			ADD_WIDGET_STRING(" ---------- CUSTOM STRUCTS ---------- ")
			
			START_WIDGET_GROUP("HEIST_BOARD_DATA") 
				ADD_WIDGET_VECTOR_SLIDER("vBoardPosition: ", 	g_HeistSharedClient.vBoardPosition, -5000, 5000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("vBoardRotation: ", 	g_HeistSharedClient.vBoardRotation, -5000, 5000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("vBoardSize: ", 		g_HeistSharedClient.vBoardSize, -50, 50, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("vWorldSize: ", 		g_HeistSharedClient.vWorldSize, -50, 50, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("vMapPosition: ", 		g_HeistSharedClient.vMapPosition, -5000, 5000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("vMapRotation: ", 		g_HeistSharedClient.vMapRotation, -5000, 5000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("vMapSize: ", 			g_HeistSharedClient.vMapSize, -50, 50, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("vMapSize: ", 			g_HeistSharedClient.vMapSize, -50, 50, 0.001)
				
				FOR index = 0 TO (FMMC_MAX_RULES-1)
					tlTemp1 = " - RULE "
					tlTemp2	= " - "
					tlTemp1 += index
					tlTemp1 += tlTemp2
					ADD_WIDGET_STRING(tlTemp1)
				
					tlTemp1 = "iMapPins[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPrePlanningClient.sBoardData.iMapPins[index], -1, ((index * 100) + 99), 1)
					
					tlTemp1 = "iMapText[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPrePlanningClient.sBoardData.iMapText[index], -1, ((index * 100) + 99), 1)
					
					tlTemp1 = "iMapPostIt[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPrePlanningClient.sBoardData.iMapPostIt[index], -1, ((index * 100) + 99), 1)
					
					tlTemp1 = "iMapArrows[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPrePlanningClient.sBoardData.iMapArrows[index], -1, ((index * 100) + 99), 1)
					
					tlTemp1 = "iMapAreas[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPrePlanningClient.sBoardData.iMapAreas[index], -1, ((index * 100) + 99), 1)
					
					tlTemp1 = "iMapHighlights[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPrePlanningClient.sBoardData.iMapHighlights[index], -1, ((index * 100) + 99), 1)
					
					tlTemp1 = " - END RULE "
					tlTemp2	= " - "
					tlTemp1 += index
					tlTemp1 += tlTemp2
					ADD_WIDGET_STRING(tlTemp1)
					ADD_WIDGET_STRING(" ")
				ENDFOR
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("HEIST_PLANNING_DATA") 
				tlTemp1 = "sHeistTitle: "
				tlTemp2	= g_HeistPrePlanningClient.sPlanningData.sHeistTitle
				tlTemp1 += tlTemp2
				ADD_WIDGET_STRING(tlTemp1)
				
				tlTemp1 = "sHeistDescription: "
				tlTemp2	= g_HeistPrePlanningClient.sPlanningData.sHeistDescription
				tlTemp1 += tlTemp2
				ADD_WIDGET_STRING(tlTemp1)
				
				ADD_WIDGET_INT_READ_ONLY("iHeistDescription: ", g_HeistPrePlanningClient.sPlanningData.iHeistDescription)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("STRUCT_DL_PHOTO_VARS_LITE") 
				FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
					tlTemp1 = "iTextureDownloadHandle["
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_READ_ONLY(tlTemp1, g_HeistPrePlanningClient.sPhotoVars[index].iTextureDownloadHandle)
					
					tlTemp1 = "iDownLoadPorgress["
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPrePlanningClient.sPhotoVars[index].iDownLoadPorgress, -100, 100, 1)
					
					tlTemp1 = "iFailCount["
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPrePlanningClient.sPhotoVars[index].iFailCount, -100, 500, 1)
					
					tlTemp1 = "iAttempts["
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPrePlanningClient.sPhotoVars[index].iAttempts, -100, 500, 1)
					
					tlTemp1 = "bSucess["
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_BOOL(tlTemp1, g_HeistPrePlanningClient.sPhotoVars[index].bSucess)
				ENDFOR
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("HEIST_BOARD_CAMERA_DATA") 
				ADD_WIDGET_BOOL("bTransitionInProgress", 	g_HeistPrePlanningClient.sBoardCamData.bTransitionInProgress)
				ADD_WIDGET_VECTOR_SLIDER("vCamCurrPos: ", 	g_HeistPrePlanningClient.sBoardCamData.vCamCurrPos, -5000, 5000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("vCamCurrRot: ", 	g_HeistPrePlanningClient.sBoardCamData.vCamCurrRot, -180, 180, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("vCamStartPos: ", 	g_HeistPrePlanningClient.sBoardCamData.vCamStartPos, -5000, 5000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("vCamStartRot: ", 	g_HeistPrePlanningClient.sBoardCamData.vCamStartRot, -180, 180, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("vCamEndPos: ", 	g_HeistPrePlanningClient.sBoardCamData.vCamEndPos, -5000, 5000, 0.001)
				ADD_WIDGET_VECTOR_SLIDER("vCamEndRot: ", 	g_HeistPrePlanningClient.sBoardCamData.vCamEndRot, -180, 180, 0.001)
				
				ADD_WIDGET_INT_SLIDER("FPS - iOperatingMode: ", g_HeistPrePlanningClient.sBoardCamData.iOperatingMode, -1, 100, 1)
				ADD_WIDGET_INT_SLIDER("FPS - iLookXLimit: ", 	g_HeistPrePlanningClient.sBoardCamData.sFPSCamData.iLookXLimit, -360, 360, 1)
				ADD_WIDGET_INT_SLIDER("FPS - iLookYLimit: ", 	g_HeistPrePlanningClient.sBoardCamData.sFPSCamData.iLookYLimit, -360, 360, 1)
				ADD_WIDGET_INT_SLIDER("FPS - iRollLimit: ", 	g_HeistPrePlanningClient.sBoardCamData.sFPSCamData.iRollLimit, -100, 100, 1)
				ADD_WIDGET_FLOAT_SLIDER("FPS - fMaxZoom: ", 	g_HeistPrePlanningClient.sBoardCamData.sFPSCamData.fMaxZoom, -1, 100, 1)
				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("HEIST_BOARD_STATE") 
				DEBUG_eFocusPoint = ENUM_TO_INT(			g_HeistPrePlanningClient.sBoardState.eFocusPoint)
				ADD_WIDGET_INT_SLIDER("eFocusPoint: ", 		DEBUG_eFocusPoint, -100, 100, 1)
				ADD_WIDGET_INT_SLIDER("iHighlight: ", 		g_HeistPrePlanningClient.sBoardState.iHighlight, -100, 100, 1)
				ADD_WIDGET_INT_SLIDER("iSubHighlight: ", 	g_HeistPrePlanningClient.sBoardState.iSubHighlight, -100, 100, 1)
				ADD_WIDGET_INT_SLIDER("iRightArrow: ", 		g_HeistPrePlanningClient.sBoardState.iRightArrow, -100, 100, 1)
				ADD_WIDGET_INT_SLIDER("iLeftArrow: ", 		g_HeistPrePlanningClient.sBoardState.iLeftArrow, -100, 100, 1)
				ADD_WIDGET_INT_SLIDER("iSubItemSelection: ",g_HeistPrePlanningClient.sBoardState.iSubItemSelection, -100, 100, 1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("CACHED_MISSION_DESCRIPTION_LOAD_VARS") 
				ADD_WIDGET_INT_READ_ONLY("nHashOld: ", 		g_HeistPrePlanningClient.sDescLoadVars.nHashOld)
				ADD_WIDGET_INT_SLIDER("iRequest: ", 		g_HeistPrePlanningClient.sDescLoadVars.iRequest, -1000, 10000, 1)
				ADD_WIDGET_BOOL("bSuccess", 				g_HeistPrePlanningClient.sDescLoadVars.bSucess)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("HEIST_HELP_TEXT_DATA") 
				DEBUG_eHelpFlowState = ENUM_TO_INT(			g_HeistPrePlanningClient.sHelpTextData.eHelpFlowState)
				ADD_WIDGET_INT_SLIDER("eHelpFlowState: ", 	DEBUG_eHelpFlowState, -100, 100, 1)
				ADD_WIDGET_BOOL("bInitialisedTimer", 		g_HeistPrePlanningClient.sHelpTextData.sNetTimer.bInitialisedTimer)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Prop / Scaleform Positioning")
			
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_PLAN_LOC") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PLAN_LOC_X", HEIST_PLAN_LOC_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PLAN_LOC_Y", HEIST_PLAN_LOC_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PLAN_LOC_Z", HEIST_PLAN_LOC_Z, -1000.0, 1000.0, 0.001)
								
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PLAN_LOC_ROT_X", HEIST_PLAN_LOC_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PLAN_LOC_ROT_Y", HEIST_PLAN_LOC_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PLAN_LOC_ROT_Z", HEIST_PLAN_LOC_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_CAM_POS_LOC") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_CAM_POS_LOC_X", HEIST_CAM_POS_LOC_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_CAM_POS_LOC_Y", HEIST_CAM_POS_LOC_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_CAM_POS_LOC_Z", HEIST_CAM_POS_LOC_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_CAM_POS_LOC_ROT_X", HEIST_CAM_POS_LOC_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_CAM_POS_LOC_ROT_Y", HEIST_CAM_POS_LOC_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_CAM_POS_LOC_ROT_Z", HEIST_CAM_POS_LOC_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_PROP_LOC - OLD") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PROP_LOC_X", HEIST_PROP_LOC_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PROP_LOC_Y", HEIST_PROP_LOC_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PROP_LOC_Z", HEIST_PROP_LOC_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PROP_LOC_ROT_X", HEIST_PROP_LOC_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PROP_LOC_ROT_Y", HEIST_PROP_LOC_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PROP_LOC_ROT_Z", HEIST_PROP_LOC_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_PROP_LOC - BUISNESS") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PROP_LOC_X", HEIST_PROP_LOC_BUS_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PROP_LOC_Y", HEIST_PROP_LOC_BUS_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PROP_LOC_Z", HEIST_PROP_LOC_BUS_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PROP_LOC_ROT_X", HEIST_PROP_LOC_BUS_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PROP_LOC_ROT_Y", HEIST_PROP_LOC_BUS_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_PROP_LOC_ROT_Z", HEIST_PROP_LOC_BUS_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_BOARD_LOC") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_BOARD_LOC_X", HEIST_BOARD_LOC_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_BOARD_LOC_Y", HEIST_BOARD_LOC_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_BOARD_LOC_Z", HEIST_BOARD_LOC_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_BOARD_LOC_ROT_X", HEIST_BOARD_LOC_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_BOARD_LOC_ROT_Y", HEIST_BOARD_LOC_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_BOARD_LOC_ROT_Z", HEIST_BOARD_LOC_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_BOARD_CENTRE_LOC") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_BOARD_CENTRE_LOC_X", HEIST_BOARD_CENTRE_LOC_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_BOARD_CENTRE_LOC_Y", HEIST_BOARD_CENTRE_LOC_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_BOARD_CENTRE_LOC_Z", HEIST_BOARD_CENTRE_LOC_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_BOARD_CENTRE_LOC_ROT_X", HEIST_BOARD_CENTRE_LOC_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_BOARD_CENTRE_LOC_ROT_Y", HEIST_BOARD_CENTRE_LOC_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_BOARD_CENTRE_LOC_ROT_Z", HEIST_BOARD_CENTRE_LOC_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_CAM_POS_BOARD") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_CAM_POS_BOARD_X", HEIST_CAM_POS_BOARD_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_CAM_POS_BOARD_Y", HEIST_CAM_POS_BOARD_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_CAM_POS_BOARD_Z", HEIST_CAM_POS_BOARD_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_CAM_POS_BOARD_ROT_X", HEIST_CAM_POS_BOARD_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_CAM_POS_BOARD_ROT_Y", HEIST_CAM_POS_BOARD_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_CAM_POS_BOARD_ROT_Z", HEIST_CAM_POS_BOARD_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ROOM_PROP") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ROOM_PROP_PIVOT_X", HEIST_ROOM_PROP_PIVOT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ROOM_PROP_PIVOT_Y", HEIST_ROOM_PROP_PIVOT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ROOM_PROP_PIVOT_Z", HEIST_ROOM_PROP_PIVOT_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ROOM_PROP_PIVOT_ROT_X", HEIST_ROOM_PROP_PIVOT_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ROOM_PROP_PIVOT_ROT_Y", HEIST_ROOM_PROP_PIVOT_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ROOM_PROP_PIVOT_ROT_Z", HEIST_ROOM_PROP_PIVOT_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_MAP_LOC") 
					ADD_WIDGET_BOOL("Update", g_HeistSharedClient.DEBUG_bUpdateMapDetails)
					ADD_WIDGET_STRING(" ---------- BUSINESS POS/ROT ---------- ")
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_BUS_POS_X", HEIST_MAP_BUS_POS_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_BUS_POS_Y", HEIST_MAP_BUS_POS_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_BUS_POS_Z", HEIST_MAP_BUS_POS_Z, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_BUS_POS_ROT_X", HEIST_MAP_BUS_POS_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_BUS_POS_ROT_Y", HEIST_MAP_BUS_POS_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_BUS_POS_ROT_Z", HEIST_MAP_BUS_POS_ROT_Z, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_STRING(" ---------- OLD POS/ROT ---------- ")
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_POS_X", HEIST_MAP_POS_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_POS_Y", HEIST_MAP_POS_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_POS_Z", HEIST_MAP_POS_Z, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_POS_ROT_X", HEIST_MAP_POS_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_POS_ROT_Y", HEIST_MAP_POS_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_POS_ROT_Z", HEIST_MAP_POS_ROT_Z, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_STRING(" ---------- SIZE ---------- ")
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_SIZE_X", HEIST_MAP_SIZE_X, 0.0, 10.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_SIZE_Y", HEIST_MAP_SIZE_Y, 0.0, 10.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_SIZE_Z", HEIST_MAP_SIZE_Z, 0.0, 10.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_WORLD_SIZE_X", HEIST_MAP_WORLD_SIZE_X, 0.0, 10.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_WORLD_SIZE_Y", HEIST_MAP_WORLD_SIZE_Y, 0.0, 10.0, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_MAP_WORLD_SIZE_Z", HEIST_MAP_WORLD_SIZE_Z, 0.0, 10.0, 0.1)
				STOP_WIDGET_GROUP()
		
			STOP_WIDGET_GROUP()
		
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	
	CLEAR_CURRENT_WIDGET_GROUP(DEBUG_PlanningParentWidgets)
ENDPROC
#ENDIF


/// PURPOSE:
///    Check if the currently highlighted heist pre-planning mission has already been completed.
/// RETURNS:
///    TRUE if completed.
FUNC BOOL IS_HIGHLIGHTED_HEIST_MISSION_ALREADY_COMPLETED()

	IF g_HeistPrePlanningClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX < 0
	OR g_HeistPrePlanningClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX > (MAX_HEIST_PLANNING_ROWS-1)
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HIGHLIGHTED_HEIST_MISSION_ALREADY_COMPLETED - ERROR! (Highlighter index - offset) is out of range! Value: ", g_HeistPrePlanningClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX)
		#ENDIF
	
		RETURN FALSE
	ENDIF

	IF g_HeistPrePlanningClient.iSelectedSetupMissions[g_HeistPrePlanningClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX] != INVALID_HEIST_INDEX
	AND g_HeistPrePlanningClient.iSelectedSetupMissions[g_HeistPrePlanningClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX] != INVALID_HEIST_DATA
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC 


/// PURPOSE:
///    Check if the currently highlighted heist pre-planning mission has already been completed.
/// RETURNS:
///    TRUE if completed.
FUNC BOOL IS_HEIST_MISSION_ALREADY_COMPLETED(INT iMissionIndex)

	IF iMissionIndex < 0
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_MISSION_ALREADY_COMPLETED - ERROR! iMissionIndex is out of range! Value: ", iMissionIndex)
		#ENDIF
	
		RETURN FALSE
	ENDIF

	IF g_HeistPrePlanningClient.iSelectedSetupMissions[iMissionIndex] != INVALID_HEIST_INDEX
	AND g_HeistPrePlanningClient.iSelectedSetupMissions[iMissionIndex] != INVALID_HEIST_DATA
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC 


/// PURPOSE:
///    Get the heist leader's name.
/// RETURNS:
///    STRING name.
FUNC STRING GET_HEIST_LEADER_NAME()

	STRING returnValue = "ERROR!"
	
	IF g_HeistSharedClient.iCurrentPropertyOwnerIndex != INVALID_HEIST_DATA
		returnValue = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex)))
	ELSE
		IF AM_I_LEADER_OF_HEIST()
			returnValue = GET_PLAYER_NAME(PLAYER_ID())
		ENDIF
	ENDIF
	
	RETURN returnValue

ENDFUNC


/// PURPOSE:
///    Get the currently completed stages of the player's heist pre-planning board.
/// RETURNS:
///    INT - Total stages complete, zero indexed.
FUNC INT GET_COMPLETED_HEIST_PRE_PLANNING_STAGES()
	
	INT index, iStageTotal
	
	FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
	
		IF g_HeistPrePlanningClient.iSelectedSetupMissions[index] != INVALID_HEIST_INDEX
		AND g_HeistPrePlanningClient.iSelectedSetupMissions[index] != INVALID_HEIST_DATA
			iStageTotal++
		ENDIF
		
	ENDFOR
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_PREPLAN] - GET_COMPLETED_HEIST_PRE_PLANNING_STAGES - Heist preplanning stages complete:", iStageTotal)
	#ENDIF
	
	RETURN iStageTotal
	
ENDFUNC


/// PURPOSE:
///    Inspect the data stored for the first prep mission to determine if it has been loaded or not.
///    Each strand must have at least 1 prep mission, but not guaranteed more than that.
/// PARAMS:
///    sHeistData - Heist data struct
/// RETURNS:
///    TRUE if data is available, FALSE if data is unavailable.
FUNC BOOL HAS_HEIST_HEADER_DATA_BEEN_PARSED(HEIST_PLANNING_DATA& sHeistData)

	IF sHeistData.sMissionData[0].iMissionDescriptionHash = 0
		// No description hash, this is suspicious.
		IF IS_STRING_NULL_OR_EMPTY(sHeistData.sMissionData[0].sMissionTitle)
			// At this point, we have no description and no title, so the data does
			// not exist (even if it has been parsed technically but the data is trash).
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


/// PURPOSE:
///    Manage loading the scaleform movies used for heist planning.
/// RETURNS:
///    TRUE once loaded.
FUNC BOOL LOAD_HEIST_PRE_PLANNING_SCALEFORM()

	IF NOT LOAD_HEIST_SCALEFORM_MOVIES()
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - LOAD_HEIST_PRE_PLANNING_SCALEFORM - Waiting on heist scaleform movies.")
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
//	PRINTLN("[AMEC][HEIST_PREPLAN] - LOAD_HEIST_PRE_PLANNING_SCALEFORM - Heist planning scaleform successfully loaded. Board index: ", NATIVE_TO_INT(g_HeistSharedClient.PlanningBoardIndex),", Map index: ", NATIVE_TO_INT(g_HeistSharedClient.PlanningMapIndex))
	
	GET_PLAYER_PROPERTY_HEIST_LOCATION(g_HeistSharedClient.vBoardPosition, MP_PROP_ELEMENT_HEIST_BOARD_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	GET_PLAYER_PROPERTY_HEIST_ROTATION(g_HeistSharedClient.vBoardRotation, MP_PROP_ELEMENT_HEIST_BOARD_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	g_HeistSharedClient.vBoardSize 		= <<1.0, 1.0, 1.0>>
	g_HeistSharedClient.vWorldSize 		= <<4.025, 2.300, 1.0>>
	
	
	GET_PLAYER_PROPERTY_HEIST_LOCATION(g_HeistSharedClient.vMapPosition, MP_PROP_ELEMENT_HEIST_MAP_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	GET_PLAYER_PROPERTY_HEIST_ROTATION(g_HeistSharedClient.vMapRotation, MP_PROP_ELEMENT_HEIST_MAP_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	g_HeistSharedClient.vMapSize = <<HEIST_MAP_SIZE_X, HEIST_MAP_SIZE_Y, HEIST_MAP_SIZE_Z>>
	g_HeistSharedClient.vMapWorldSize = <<HEIST_MAP_WORLD_SIZE_X, HEIST_MAP_WORLD_SIZE_Y, HEIST_MAP_WORLD_SIZE_Z>>

//	PRINTLN("[AMEC][HEIST_PREPLAN] - LOAD_HEIST_PRE_PLANNING_SCALEFORM - Scaleform movie for pre-planning board has loaded.")
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Request the audio data for the heist planning board.
/// RETURNS:
///    TRUE once loaded.
FUNC BOOL LOAD_HEIST_PRE_PLANNING_AUDIO()

	IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_PLANNING_BOARD")
		PRINTLN("[AMEC][HEIST_PREPLAN] - LOAD_HEIST_PRE_PLANNING_AUDIO - Still loading audio bank.")
		
		#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("[AMEC_FINAL] - LOAD_HEIST_PRE_PLANNING_AUDIO = FALSE")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Parse the header data to grab the initial details (such as title, description hash etc).
/// RETURNS:
///    TRUE once initial parsing is complete.
FUNC BOOL IS_HEIST_HEADER_DATA_PARSED(BOOL bIsRemote)

	TEXT_LABEL_23 tlMissionRootContID
	INT iMissionIndex, index
	BOOL bContentFound
	
	// Is a guest (not the heist leader). This means we used the data saved by the leader before.
	IF bIsRemote
		tlMissionRootContID = g_HeistPrePlanningClient.tlHeistFinaleRootContID
	ELSE
		tlMissionRootContID = GET_HEIST_FINALE_STAT_DATA()
		g_HeistPrePlanningClient.tlHeistFinaleRootContID = tlMissionRootContID
	ENDIF
	
	
	IF g_HeistPrePlanningClient.sPlanningData.iHeistDescription = 0
		iMissionIndex = GET_INDEX_OF_HEIST_MISSION(GET_HASH_KEY(tlMissionRootContID)) // FINALE
		
		IF iMissionIndex != -1
			g_HeistPrePlanningClient.sPlanningData.iHeistDescription = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionIndex].iMissionDecHash
			g_HeistPrePlanningClient.sPlanningData.sHeistTitle = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionIndex].tlMissionName
			PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_HEADER_DATA_PARSED - Heist details loaded:")
			PRINTLN("[AMEC][HEIST_PREPLAN] - ... iHeistDescription: ", 	g_HeistPrePlanningClient.sPlanningData.iHeistDescription)
			PRINTLN("[AMEC][HEIST_PREPLAN] - ... sHeistTitle: ", 		g_HeistPrePlanningClient.sPlanningData.sHeistTitle)
			
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[AMEC_FINAL] - IS_HEIST_HEADER_DATA_PARSED - Heist details loaded:")
				PRINTLN_FINAL("[AMEC_FINAL] - ... iHeistDescription: ", g_HeistPrePlanningClient.sPlanningData.iHeistDescription)
				PRINTLN_FINAL("[AMEC_FINAL] - ... sHeistTitle: ", 		g_HeistPrePlanningClient.sPlanningData.sHeistTitle)
			#ENDIF
			
		ELSE
			g_HeistPrePlanningClient.sPlanningData.sHeistDescription = ""
			g_HeistPrePlanningClient.sPlanningData.sHeistTitle = ""
			g_HeistPrePlanningClient.sPlanningData.iHeistDescription = INVALID_HEIST_DATA
			PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_HEADER_DATA_PARSED - ERROR! Heist root details NOT LOADED.")
		ENDIF
	ENDIF
	
	
	IF NOT HAS_HEIST_HEADER_DATA_BEEN_PARSED(g_HeistPrePlanningClient.sPlanningData)
		
		g_HeistPrePlanningClient.iTotalPrePlanRows = 0
		CLEAN_ONLY_HEIST_PLANNING_MISSION_DATA()
		
		FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
	
			TEXT_LABEL_23 tlTempContID = GET_CONTENTID_FOR_SLOT(index)
			
			PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_HEADER_DATA_PARSED - # Branch # Checking validity of contentID: ", tlTempContID)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(tlTempContID)
			
				PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_HEADER_DATA_PARSED - # Branch # sub-mission contentID hash: ", GET_HASH_KEY(tlTempContID), ". Getting index...")
			
				iMissionIndex = GET_INDEX_OF_HEIST_MISSION(GET_HASH_KEY(tlTempContID))
				
				IF iMissionIndex >= 0
				
					g_HeistPrePlanningClient.iTotalPrePlanRows++
				
					g_HeistPrePlanningClient.sPlanningData.sMissionData[index].sMissionTitle = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionIndex].tlMissionName
				 	g_HeistPrePlanningClient.sPlanningData.sMissionData[index].sBranchDetails[0].iPhotoVersion = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionIndex].iPhotoVersion
					g_HeistPrePlanningClient.sPlanningData.sMissionData[index].iMinPlayers = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionIndex].iMinPlayers
					g_HeistPrePlanningClient.sPlanningData.sMissionData[index].iTimeAvailable = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionIndex].iLaunchTimesBit
					g_HeistPrePlanningClient.sPlanningData.sMissionData[index].iMissionDescriptionHash = g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionIndex].iMissionDecHash
					
					#IF USE_FINAL_PRINTS
						PRINTLN_FINAL("[AMEC_FINAL] - IS_HEIST_HEADER_DATA_PARSED - # Mission: ",index," # Heist MISSION details loaded: ", g_HeistPrePlanningClient.sPlanningData.sMissionData[index].sMissionTitle)
						PRINTLN_FINAL("[AMEC_FINAL] - ... iTotalRows: 	", 	g_HeistPrePlanningClient.iTotalPrePlanRows)
					#ENDIF
					
					PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_HEADER_DATA_PARSED - # Branch # Heist MISSION details loaded:")
					PRINTLN("[AMEC][HEIST_PREPLAN] - ... sMissionTitle: ", 	g_HeistPrePlanningClient.sPlanningData.sMissionData[index].sMissionTitle)
					PRINTLN("[AMEC][HEIST_PREPLAN] - ... iPhotoVersion: ", 	g_HeistPrePlanningClient.sPlanningData.sMissionData[index].sBranchDetails[0].iPhotoVersion)
					PRINTLN("[AMEC][HEIST_PREPLAN] - ... iTotalRows: 	", 	g_HeistPrePlanningClient.iTotalPrePlanRows)
					
					bContentFound = TRUE
					
				ELSE
					PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_HEADER_DATA_PARSED - # Branch # ERROR! Heist MISSION index not found in mission list! contentID: ", tlTempContID)
				ENDIF
				
			ELSE
				PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_HEADER_DATA_PARSED - # Branch # No contentID found for index: ", index)
			ENDIF

		ENDFOR
		
	ELSE
		PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_HEADER_DATA_PARSED - # Branch # Already parsed header data.")
		bContentFound = TRUE
	ENDIF 
	
	RETURN bContentFound

ENDFUNC


/// PURPOSE:
///    When a guest in someone else's apartment, make sure that the heist hasn't been abandoned.
/// RETURNS:
///    TRUE if heist playerBD is NULL or EMPTY (abandoned).
FUNC BOOL HAS_HEIST_BEEN_CANCELLED_GUEST_ONLY(SCRIPT_TIMER & sStuckTimer)

	// This only needs to be run on remote clients / guests.
	IF NOT IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	INT iAptOwnerPlayerIndex = g_HeistSharedClient.iCurrentPropertyOwnerIndex
	
	IF iAptOwnerPlayerIndex < 0
	OR iAptOwnerPlayerIndex > (NUM_NETWORK_PLAYERS-1)
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - HAS_HEIST_BEEN_CANCELLED_GUEST_ONLY - ERROR! iAptOwnerParticipantIndex is OUT OF RANGE. Value: ", iAptOwnerPlayerIndex)
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	//If the leader has 
	IF INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex) != PLAYER_ID()
	AND NOT IS_HEIST_LEADER_OK()
		IF NOT HAS_NET_TIMER_STARTED(sStuckTimer)
			START_NET_TIMER(sStuckTimer)
			PRINTLN("[AMEC][HEIST_PREPLAN] - HAS_HEIST_BEEN_CANCELLED_GUEST_ONLY - START_NET_TIMER")
		ELIF HAS_NET_TIMER_EXPIRED(sStuckTimer, 7000) 
			PRINTLN("[AMEC][HEIST_PREPLAN] - HAS_HEIST_BEEN_CANCELLED_GUEST_ONLY - HAS_NET_TIMER_EXPIRED(sStuckTimer, 7000) clean up")
			SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_CLEANUP_THREAD_BUFFER)
			CLEAN_HEIST_SHARED_DATA()			
			RETURN TRUE
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(sStuckTimer)
			RESET_NET_TIMER(sStuckTimer)
		ENDIF
	ENDIF
	
	RETURN FALSE

ENDFUNC



/// PURPOSE:
///    Get the relative index of the next recommended mission to complete.
/// RETURNS:
///    INT relative mission index.
FUNC INT GET_NEXT_HEIST_MISSION_TO_PLAY()
	
	INT index
	FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
	
		IF g_HeistPrePlanningClient.iSelectedSetupMissions[index] = INVALID_HEIST_DATA
		OR g_HeistPrePlanningClient.iSelectedSetupMissions[index] = INVALID_HEIST_INDEX
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_PREPLAN] - GET_NEXT_HEIST_MISSION_TO_PLAY - Getting next incomplete mission. Value: ", index)
			#ENDIF
		
			RETURN index + ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX
		ENDIF
	
	ENDFOR
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_PREPLAN] - GET_NEXT_HEIST_MISSION_TO_PLAY - WARNING! Could not find an invalid (incomplete) heist to set as default target, reverting to 0.")
	#ENDIF
	
	RETURN 0
	
ENDFUNC


/// PURPOSE:
///    Check if the description hash has been resolved by code.
/// RETURNS:
///    TRUE if description straing available.
FUNC BOOL IS_HEIST_DESCRIPTION_READY()
	
	IF GET_HEIST_PREPLAN_DESC_DOWNLOADED()
		RETURN TRUE
	ENDIF
	
	BOOL bDescriptionReady = TRUE
	
	IF NOT (REQUEST_AND_LOAD_CACHED_DESCRIPTION_FOR_HEIST(g_HeistPrePlanningClient.sPlanningData.iHeistDescription, g_HeistPrePlanningClient.sDescLoadVars))
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_DESCRIPTION_READY - Waiting on base HASH: ", g_HeistPrePlanningClient.sPlanningData.iHeistDescription)
		#ENDIF
		
		#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("[AMEC_FINAL] - IS_HEIST_DESCRIPTION_READY - Waiting on base hash: ", g_HeistPrePlanningClient.sPlanningData.iHeistDescription)
		#ENDIF
	
		bDescriptionReady = FALSE
    ENDIF
	
	INT index
	FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
	
		IF g_HeistPrePlanningClient.sPlanningData.sMissionData[index].iMissionDescriptionHash != 0
			IF NOT (REQUEST_AND_LOAD_CACHED_DESCRIPTION_FOR_HEIST(g_HeistPrePlanningClient.sPlanningData.sMissionData[index].iMissionDescriptionHash, 
														g_HeistPrePlanningClient.sPlanningData.sMissionData[index].sMissionDescLoadVars))
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_DESCRIPTION_READY - Waiting on sub HASH: ", g_HeistPrePlanningClient.sPlanningData.sMissionData[index].iMissionDescriptionHash)
				#ENDIF
			
				bDescriptionReady = FALSE
				
			ELSE
			
				IF NOT g_HeistPrePlanningClient.sPlanningData.sMissionData[index].sMissionDescLoadVars.bSucess
				
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_DESCRIPTION_READY - ERROR! REQUEST_AND_LOAD_CACHED_DESCRIPTION returned TRUE, but bSucess = FALSE. Hash: ", g_HeistPrePlanningClient.sPlanningData.sMissionData[index].iMissionDescriptionHash)
					#ENDIF
					
					bDescriptionReady = FALSE
				
				ENDIF 
														
			ENDIF
		ENDIF
		
	ENDFOR
	
	IF bDescriptionReady
		PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_DESCRIPTION_READY - All description strings are available; beginning dump: ")
		
		#IF USE_FINAL_PRINTS
			PRINTLN_FINAL("[AMEC_FINAL] - IS_HEIST_DESCRIPTION_READY - All description strings are available")
		#ENDIF
		
		STRING sText
		INT iStartCharacterIndex, iStringLengthInCharacters, iArrayIndex
		BOOL bFinishedCopying
		//Loop all descriptions and assign them in the the aweful array:
		//g_HeistPrePlanningClient.sPlanningData.sMissionData[index].tl63MissionDecription[iArrayIndex]
		FOR index = 0 TO (g_HeistPrePlanningClient.iTotalPrePlanRows-1)
			//Remove old
			FOR iArrayIndex = 0 TO (FMMC_NUM_LABELS_FOR_DESCRIPTION-1)
				g_HeistPrePlanningClient.sPlanningData.sMissionData[index].tl63MissionDecription[iArrayIndex] = ""
			ENDFOR
			//Reset data
			iStartCharacterIndex = 0
			iArrayIndex = 0
			bFinishedCopying = FALSE
			//Get the cached description:
			sText = UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(g_HeistPrePlanningClient.sPlanningData.sMissionData[index].iMissionDescriptionHash, INVITE_MAX_DESCRIPTION_LENGTH)	
			iStringLengthInCharacters = GET_LENGTH_OF_LITERAL_STRING(sText)
			PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_DESCRIPTION_READY ...  String[",index,"]:	[", sText, "] - iStringLengthInCharacters = ", iStringLengthInCharacters)
			//Copy this string into chunks using a byte limit
			WHILE NOT bFinishedCopying
				//If we've got to the end of the string then we're done
				IF iStartCharacterIndex >= iStringLengthInCharacters
					PRINTLN("bFinishedCopying index - ", index, " (iStartCharacterIndex >= iStringLengthInCharacters)")
					PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_DESCRIPTION_READY = ", g_HeistPrePlanningClient.sPlanningData.sMissionData[index].tl63MissionDecription[0], g_HeistPrePlanningClient.sPlanningData.sMissionData[index].tl63MissionDecription[1], g_HeistPrePlanningClient.sPlanningData.sMissionData[index].tl63MissionDecription[2], g_HeistPrePlanningClient.sPlanningData.sMissionData[index].tl63MissionDecription[3], 
																					g_HeistPrePlanningClient.sPlanningData.sMissionData[index].tl63MissionDecription[4], g_HeistPrePlanningClient.sPlanningData.sMissionData[index].tl63MissionDecription[5], g_HeistPrePlanningClient.sPlanningData.sMissionData[index].tl63MissionDecription[6], g_HeistPrePlanningClient.sPlanningData.sMissionData[index].tl63MissionDecription[7])
					bFinishedCopying = TRUE
				ELSE
					//If we're over the array size then we done
					IF iArrayIndex >= FMMC_NUM_LABELS_FOR_DESCRIPTION
						PRINTLN("bFinishedCopying index - ", index, " (iArrayIndex >= FMMC_NUM_LABELS_FOR_DESCRIPTION)")
						bFinishedCopying = TRUE
					ELSE
						//Get the next chunk
						g_HeistPrePlanningClient.sPlanningData.sMissionData[index].tl63MissionDecription[iArrayIndex] = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME_WITH_BYTE_LIMIT(sText, iStartCharacterIndex, iStringLengthInCharacters, 63)
						//Print
						PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_DESCRIPTION_READY g_HeistPrePlanningClient.sPlanningData.sMissionData[", index, "].tl63MissionDecription[", iArrayIndex, "] = ", g_HeistPrePlanningClient.sPlanningData.sMissionData[index].tl63MissionDecription[iArrayIndex])
						//Get the length of the chunk that we've just grabbed so we can move start position
						iStartCharacterIndex += GET_LENGTH_OF_LITERAL_STRING(g_HeistPrePlanningClient.sPlanningData.sMissionData[index].tl63MissionDecription[iArrayIndex])
						PRINTLN("iStartCharacterIndex is now ", iStartCharacterIndex)
						//Move to the next array
						iArrayIndex++
						PRINTLN("iArrayIndex is now ", iArrayIndex)
					ENDIF
				ENDIF				
			ENDWHILE
		ENDFOR
		
		SET_HEIST_PREPLAN_DESC_DOWNLOADED(TRUE)
		
	ENDIF
	
	RETURN bDescriptionReady
	
ENDFUNC


/// PURPOSE:
///    Load the pictures for heist pre-planning missions.
/// PARAMS:
///    sGetUGC_content - 
/// RETURNS:
///    TRUE once loaded.
FUNC BOOL LOAD_HEIST_PRE_PLANNING_MISSION_PICTURES(BOOL bBackgroundDownload)
	
	// Make sure that we don't keep hammering the download once it has finished.
	IF bBackgroundDownload
		IF NOT GET_HEIST_PREP_RETRY_DOWNLOAD_PICTURES()
			CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - LOAD_HEIST_PRE_PLANNING_MISSION_PICTURES - bBackgroundDownload = TRUE, bKeepDownloadingPictures = FALSE. Background picture download is complete.")
			RETURN FALSE
		ENDIF
	ENDIF 
	
	INT index//, iPhotoPath
	BOOL bTextureLoading
	
	FOR index = 0 TO (g_HeistPrePlanningClient.iTotalPrePlanRows-1)
	
		TEXT_LABEL_23 tlMissionRootContID = GET_CONTENTID_FOR_SLOT(index)
		TEXT_LABEL_23 contentID = GET_CONTENTID_FROM_ROOT_CONTENTID(GET_HASH_KEY(tlMissionRootContID))
		//iPhotoPath = GET_PHOTO_PATH_FROM_ROOT_CONTENTID(GET_HASH_KEY(tlMissionRootContID))
		STRING sTextureName = ""
		
		IF GET_HASH_KEY(tlMissionRootContID) = g_sMPTUNABLES.iroot_id_HASH_Pacific_Standard_Hack
			IF g_HeistPrePlanningClient.iCirclePrepMission[index] != ci_HEIST_PICTURE_DO_CIRCLE
				PRINTLN("[AMEC][HEIST_PREPLAN] - LOAD_HEIST_PRE_PLANNING_MISSION_PICTURES - Mission: ", tlMissionRootContID, " is registered as a DO CIRCLE mission, adding GFX marker.")
				g_HeistPrePlanningClient.iCirclePrepMission[index] = ci_HEIST_PICTURE_DO_CIRCLE
			ENDIF
		ENDIF
		
		// Check to make sure we're not downloading pictures that have already been downloaded.
		IF NOT g_HeistPrePlanningClient.sPhotoVars[index].bSucess
			CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - LOAD_HEIST_PRE_PLANNING_MISSION_PICTURES - ROW[",index,"] - !!! Loading photo for FMMC lite using ContentID: ", contentID, " - Background: ",bBackgroundDownload)//, iPhotoPath: ", iPhotoPath)	
			IF DOWNLOAD_PHOTO_FOR_FMMC_LITE(g_HeistPrePlanningClient.sPhotoVars[index], contentID, 0, 75, 1, TRUE)
				IF g_HeistPrePlanningClient.sPhotoVars[index].bSucess
					sTextureName = TEXTURE_DOWNLOAD_GET_NAME(g_HeistPrePlanningClient.sPhotoVars[index].iTextureDownloadHandle)
					CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - LOAD_HEIST_PRE_PLANNING_MISSION_PICTURES - ROW[",index,"] - TextureName = ", sTextureName)
				ELSE
					CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - LOAD_HEIST_PRE_PLANNING_MISSION_PICTURES - ROW[",index,"] - FAILED to load image, continuing without.")
				ENDIF
			ELSE
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - LOAD_HEIST_PRE_PLANNING_MISSION_PICTURES - ROW[",index,"] - ... still loading.")
				bTextureLoading = TRUE
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sTextureName)
			AND g_HeistPrePlanningClient.sPhotoVars[index].bSucess
				PRINTLN("[AMEC][HEIST_PREPLAN] - LOAD_HEIST_PRE_PLANNING_MISSION_PICTURES - ROW[",index,"] - Picture ",index," texture downloaded: ", sTextureName)
				g_HeistPrePlanningClient.sPlanningData.sMissionData[index].sBranchDetails[0].tlPicture = sTextureName
			ENDIF
		ENDIF
	ENDFOR

	// There's a texture/picture still loading, go back and try again!
	IF bTextureLoading
		RETURN FALSE
	ENDIF
	
//	PRINTLN("[AMEC][HEIST_PREPLAN] - LOAD_HEIST_PRE_PLANNING_MISSION_PICTURES - All pictures successfully loaded.")	
	
	RETURN TRUE
	
ENDFUNC


/// PURPOSE:
///    Get the currently selected mission index from the planning board.
/// RETURNS:
///    0 to MAX_HEIST_PLANNING_ROWS
FUNC INT GET_SELECTED_ROW_NOT_OFFSET() 

	INT iSelectedItemNoOffset = (g_HeistPrePlanningClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX)
	
	// Stop any negative index pointers being returned.
	IF iSelectedItemNoOffset < MAX_HEIST_PLANNING_ROWS
	AND iSelectedItemNoOffset >= 0
		RETURN iSelectedItemNoOffset
	ELSE	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - GET_SELECTED_ROW_NOT_OFFSET - ERROR! Selected row (no offset) was not in bounds of the array! Highlight: ",g_HeistPrePlanningClient.sBoardState.iHighlight,". Highlight(no offset): ",iSelectedItemNoOffset)
		#ENDIF
		
		RETURN 0
		
	ENDIF
ENDFUNC


/// PURPOSE:
///    Check if there are any actie conditions that require the heist pre-planning board to be hidden.
/// RETURNS:
///    TRUE if DO show.
FUNC BOOL SHOULD_HEIST_PREPLAN_BOARD_BE_ACTIVE(BOOL bForceInactiveBoard, BOOL& bExitFlow)

	// Every frame checks.
	
	// This is required because if we hit this check 4 frames after starting the heist prep board, it will begin
	// the process of loading the scaleform anyway. The point of this check to to completely prevent loading.
	IF SHOULD_HEIST_JOB_INVITE_HOLD_PLANNING_BOARD()
		PRINTLN("[TS][HJOBI][AMEC][HEIST_PREPLAN] - SHOULD_HEIST_PREPLAN_BOARD_BE_ACTIVE - SHOULD_HEIST_JOB_INVITE_HOLD_PLANNING_BOARD = TRUE, don't allow pre-planning.")
		bExitFlow = TRUE
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN_SPEW] - SHOULD_HEIST_PREPLAN_BOARD_BE_ACTIVE - IS_PLAYER_SCTV = TRUE, exit.")
		bExitFlow = TRUE
		RETURN FALSE
	ENDIF
	
	// Is player in a property? Needed to be checked every frame for rapid reactive session changing, also prevents exiting to 
	// skycam when property is invalid.
	IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
	OR GET_HEIST_SUPPRESS_HEIST_BOARD()
		IF IS_MP_HEIST_PRE_PLANNING_ACTIVE()
			PRINTLN("[AMEC][HEIST_PREPLAN_SPEW] - SHOULD_HEIST_PREPLAN_BOARD_BE_ACTIVE - Not in property AND heist is ACTIVE, hide board. g_HeistSharedClient.bSupressPlanningBoard: ", g_HeistSharedClient.bSupressPlanningBoard)
			SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_CLEANUP_THREAD_BUFFER)
			CLEAN_HEIST_SHARED_DATA()
		ELSE
//			CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN_SPEW] - SHOULD_HEIST_PREPLAN_BOARD_BE_ACTIVE - Not in property, and no active heist, hide board. g_HeistSharedClient.bSupressPlanningBoard: ", g_HeistSharedClient.bSupressPlanningBoard)
			bExitFlow = TRUE
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_THIS_A_HEIST_PROPERTY(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN_SPEW] - SHOULD_HEIST_PREPLAN_BOARD_BE_ACTIVE - IS_THIS_A_HEIST_PROPERTY() = FALSE, property ID: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		bExitFlow = TRUE
		RETURN FALSE
	ENDIF

	// Staggered frame checks
	SWITCH g_HeistPrePlanningClient.iPreReqFrameCount

		DEFAULT g_HeistPrePlanningClient.iPreReqFrameCount = 0 BREAK

		CASE 0

			// Is heist exit cutscene active?
			IF GET_HEIST_EXIT_CUTS_FLOW() = HEIST_CUTS_FLOW_STATE_FINISHED
			OR GET_HEIST_EXIT_CUTS_FLOW() = HEIST_CUTS_FLOW_STATE_LOADING
			OR GET_HEIST_EXIT_CUTS_FLOW() = HEIST_CUTS_FLOW_STATE_WAIT_FOR_SCENE
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN_SPEW] - SHOULD_HEIST_PREPLAN_BOARD_BE_ACTIVE - Finale exit ACTIVE, hide board...")
				bExitFlow = TRUE
				RETURN FALSE
			ENDIF
			
			IF GET_HEIST_PREPLAN_STATE() = HEIST_PRE_FLOW_EXIT_PENDING_THREAD_BUFFER
			OR GET_HEIST_PREPLAN_STATE() = HEIST_PRE_FLOW_EXIT_PENDING
				RETURN TRUE
			ENDIF
			
			g_HeistPrePlanningClient.iPreReqFrameCount++
			
		BREAK
		
		CASE 1
	
			// Placeholder
			g_HeistPrePlanningClient.iPreReqFrameCount++
			
		BREAK
		
		CASE 2
	
			// Is a finale active?
			IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN_SPEW] - SHOULD_HEIST_PREPLAN_BOARD_BE_ACTIVE - Currently on heist FINALE, hide board...")
				bExitFlow = TRUE
				RETURN FALSE
			ENDIF
			
			IF g_bReducedApartmentCreationTurnedOn
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN_SPEW] - SHOULD_HEIST_PREPLAN_BOARD_BE_ACTIVE - g_bReducedApartmentCreationTurnedOn")
				bExitFlow = TRUE
				RETURN FALSE
			ENDIF
			
			g_HeistPrePlanningClient.iPreReqFrameCount++
	
		BREAK
		
		CASE 3
	
			IF GET_HEIST_QUICK_RESTART_IN_PROGRESS()
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN_SPEW] - SHOULD_HEIST_PREPLAN_BOARD_BE_ACTIVE - GET_HEIST_QUICK_RESTART_IN_PROGRESS() = TRUE.")
				bExitFlow = TRUE
				RETURN FALSE
			ENDIF
			
			IF IS_MP_HEIST_FAKE_FINALE_ACTIVE()
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN_SPEW] - SHOULD_HEIST_PREPLAN_BOARD_BE_ACTIVE - IS_MP_HEIST_FAKE_FINALE_ACTIVE() = TRUE.")
				bExitFlow = TRUE
				RETURN FALSE
			ENDIF
			
			IF GET_HEIST_PREPLAN_STATE() != HEIST_PRE_FLOW_START_THREAD_BUFFER
			AND GET_HEIST_PREPLAN_STATE() != HEIST_PRE_FLOW_START
				IF bForceInactiveBoard
					CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN_SPEW] - SHOULD_HEIST_PREPLAN_BOARD_BE_ACTIVE - bForceInactiveBoard = TRUE.")
					bExitFlow = FALSE
					RETURN FALSE
				ENDIF
			ENDIF
			
			g_HeistPrePlanningClient.iPreReqFrameCount++
			
		BREAK
		
		CASE 4
		
			IF NOT AM_I_LEADER_OF_HEIST(TRUE)
			AND g_HeistSharedClient.iCurrentPropertyOwnerIndex >= 0
			AND g_HeistSharedClient.iCurrentPropertyOwnerIndex < NUM_NETWORK_PLAYERS
			AND IS_MP_HEIST_PRE_PLANNING_ACTIVE()
				IF IS_STRING_NULL_OR_EMPTY(GlobalplayerBD_FM_HeistPlanning[g_HeistSharedClient.iCurrentPropertyOwnerIndex].tlHeistFinaleRootContID)
				OR ARE_STRINGS_EQUAL(GlobalplayerBD_FM_HeistPlanning[g_HeistSharedClient.iCurrentPropertyOwnerIndex].tlHeistFinaleRootContID, ".")
					CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN_SPEW] - SHOULD_HEIST_PREPLAN_BOARD_BE_ACTIVE - GUEST - BD for tlHeistFinaleRootContID is INVALID.")
					bExitFlow = FALSE
					RETURN FALSE
				ENDIF
			ENDIF
			
			g_HeistPrePlanningClient.iPreReqFrameCount++

		BREAK
		
	ENDSWITCH
	
	bExitFlow = FALSE

	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Do we need to move from standard heist prep state to an inactive/background mode. Used for
///    when players fail the first Fleeca mission.
/// RETURNS:
///    TRUE if Fleeca first mission is not finished, but the heist strand is active.
FUNC BOOL IS_HEIST_BACKGROUND_BOARD_REQUIRED()

	IF AM_I_LEADER_OF_HEIST(TRUE)
	AND NOT GET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD()
	AND IS_MP_HEIST_STRAND_ACTIVE()
	AND NOT NETWORK_IS_ACTIVITY_SESSION()
		
		TEXT_LABEL_23 tlStrandRootContID = GET_HEIST_FINALE_STAT_DATA()
		
		IF (GET_HASH_KEY(tlStrandRootContID) = g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job)
		
			IF g_HeistPrePlanningClient.iSelectedSetupMissions[0] = ci_HEIST_MISSION_NOT_COMPLETE
			
				PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_BACKGROUND_BOARD_REQUIRED - Fleeca heist active, and first mission not yet finished. Returning TRUE.")
				
				IF NOT GET_HEIST_BACKGROUND_MODE_ACTIVE()
					PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_BACKGROUND_BOARD_REQUIRED - New state transition, clean up previous details (heist planning).")
					CLEAN_HEIST_PRE_PLANNING_MAP(HAS_SCALEFORM_MOVIE_LOADED(g_HeistSharedClient.PlanningMapIndex))
					CLEAN_HEIST_SHARED_DATA()
					SET_HEIST_BACKGROUND_MODE_ACTIVE(TRUE)
				ENDIF
				
				IF GET_HEIST_UPDATE_AVAILABLE()
					SET_HEIST_UPDATE_AVAILABLE(FALSE)
				ENDIF

				RETURN TRUE
				
			ELIF g_HeistPrePlanningClient.iSelectedSetupMissions[0] = ci_HEIST_MISSION_COMPLETE
			
				PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_BACKGROUND_BOARD_REQUIRED - Fleeca heist active, and first mission IS finished. Returning FALSE.")
			
				IF GET_HEIST_BACKGROUND_MODE_ACTIVE()
					PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_BACKGROUND_BOARD_REQUIRED - New state transition, clean up previous details (background).")
					CLEAN_HEIST_SHARED_DATA()
					SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_CLEANUP_THREAD_BUFFER)
					SET_HEIST_BACKGROUND_MODE_ACTIVE(FALSE)
				ENDIF
				
				IF NOT GET_HEIST_UPDATE_AVAILABLE()
					SET_HEIST_UPDATE_AVAILABLE(TRUE)
				ENDIF
				
				RETURN FALSE
		
			ENDIF
			
		ELSE
		
			IF GET_HEIST_BACKGROUND_MODE_ACTIVE()
				PRINTLN("[AMEC][HEIST_PREPLAN] - IS_HEIST_BACKGROUND_BOARD_REQUIRED - CRITICAL ERROR! Background mode enabled for a NON Fleeca heist strand! Disabling...")
				SET_HEIST_BACKGROUND_MODE_ACTIVE(FALSE)
			ENDIF
		
		ENDIF
		
	ENDIF
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Get the names of the people currently in this session and save them ready for the update method to place onto the pre-planning board.
///    Safe to call every frame.
PROC GET_SESSION_PLAYER_NAMES()

	// There is a special case here for the leader.
	IF g_HeistPrePlanningClient.bHaveSessionMemberNames
		IF GET_HEIST_AM_I_HEIST_LEADER_GLOBAL()
			IF NOT g_HeistPrePlanningClient.bPlayerListUpdate
				EXIT
			ENDIF
		ELSE
			EXIT
		ENDIF
	ENDIF
	
	INT index, iPointer, iAptOwnerIndex, iMissionIndex
	
	iAptOwnerIndex = g_HeistSharedClient.iCurrentPropertyOwnerIndex
	
	IF iAptOwnerIndex > INVALID_HEIST_DATA
	AND iAptOwnerIndex < NUM_NETWORK_PLAYERS
		iMissionIndex = GlobalplayerBD_FM_HeistPlanning[iAptOwnerIndex].iCurrentHeistSetupIndex
	ELSE
		iMissionIndex = INVALID_HEIST_DATA
	ENDIF
	
	IF iMissionIndex = INVALID_HEIST_DATA
		PRINTLN("[AMEC][HEIST_PREPLAN] - GET_SESSION_PLAYER_NAMES - ERROR! iMissionIndex is INVALID, dropping out to wait for updated valid data. Leader ID: ", iAptOwnerIndex, ", iMissionIndex: ", iMissionIndex)
		IF AM_I_LEADER_OF_HEIST()
			GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].iCurrentHeistSetupIndex = g_TransitionSessionNonResetVars.iHeistSetupIdx
			PRINTLN("[AMEC][HEIST_PREPLAN] - GET_SESSION_PLAYER_NAMES - Set my playerBD iCurrentHeistSetupIndex to backup value: ", g_TransitionSessionNonResetVars.iHeistSetupIdx)
		ENDIF
		EXIT // Can't go further than this without a mission ID.
	ENDIF
	
//	PRINTLN("[AMEC][HEIST_PREPLAN] - GET_SESSION_PLAYER_NAMES - Getting names for current session members: ")	

	INT iPlayersBitset = Get_Bitfield_Of_Players_On_Or_Joining_Mission(Get_UniqueID_For_This_Players_Mission(PLAYER_ID()))
	PLAYER_INDEX thisPlayer
      
   	REPEAT NUM_NETWORK_PLAYERS index
        IF IS_BIT_SET(iPlayersBitset, index)
            // This PlayerID is on or joining the mission
			
			thisPlayer = INT_TO_PLAYERINDEX(index)
			
			// Best do a safety check for player OK too
			IF (IS_NET_PLAYER_OK(thisPlayer, FALSE))
      			// THIS PLAYER IS ON OR JOINING THE MISSION
				
				IF iPointer < 0
					iPointer = 0
				ELIF iPointer > (MAX_HEIST_PLAYERS_PER_SETUP-1)
					iPointer = (MAX_HEIST_PLAYERS_PER_SETUP-1)
				ENDIF
				
				IF iAptOwnerIndex = INVALID_HEIST_DATA
					PRINTLN("[AMEC][HEIST_PREPLAN] - GET_SESSION_PLAYER_NAMES - ERROR! iCurrentPropertyOwnerIndex is INVALID! Failing over to 0 to prevent array overrun.")
					iAptOwnerIndex = 0
				ENDIF
				
//				PRINTLN("[AMEC][HEIST_PREPLAN] - GET_SESSION_PLAYER_NAMES - Getting active setup mission. AptOwner: ",iAptOwnerIndex)
				
				g_HeistSharedClient.tlMissionPlayerName[iMissionIndex][iPointer] = GET_PLAYER_NAME(thisPlayer)
			
				iPointer++
				
//				PRINTLN("[AMEC][HEIST_PREPLAN] - GET_SESSION_PLAYER_NAMES - Putting player on board: PlayerName: ", GET_PLAYER_NAME(thisPlayer), ", PlayerID: ", NATIVE_TO_INT(thisPlayer), " at index: ",iMissionIndex)
				
			ENDIF
        ENDIF
    ENDREPEAT
	
	g_HeistPrePlanningClient.bHaveSessionMemberNames = TRUE

ENDPROC

/// PURPOSE:
///    Start a timer for the e4stablishing shot. This is generally 3 seconds, but if loading has not finished, it may take longer.
PROC START_TUTORIAL_AUTO_PROGRESS_TIMER()
	PRINTLN("[AMEC][HEIST_PREPLAN] - START_TUTORIAL_AUTO_PROGRESS_TIMER - Setting Establishing Shot Timer to ", ci_HEIST_TUTORIAL_AUTO_TIMER, "msec")
	g_HeistPrePlanningClient.tutorialAutoTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), ci_HEIST_TUTORIAL_AUTO_TIMER)
ENDPROC

// PURPOSE:	Check if the safety timeout time has elapsed
FUNC BOOL HAS_TUTORIAL_AUTO_PROGRESS_TIMER_EXPIRED()
	RETURN (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_HeistPrePlanningClient.tutorialAutoTimer))
ENDFUNC


/// PURPOSE:
///    Set the selected setup missions for the heist to be invalid.
PROC SET_STATUS_ARRAY_TO_EMPTY()
	
	INT index
	FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
		g_HeistPrePlanningClient.iSelectedSetupMissions[index] = -1
	ENDFOR
	
ENDPROC


/// PURPOSE:
///    Get the depth data from (stats for leader, BD for remote) and place them into an array for easy parsing.
///    ONLY CALL ONCE PER INIT.
PROC SET_HEIST_MISSION_DEPTH_DATA(BOOL bAsLeader)

	INT index

	IF bAsLeader

		PRINTLN("[AMEC][HEIST_PREPLAN] - SET_HEIST_MISSION_DEPTH_DATA - LEADER - Set iHeistMissionDepth array using stats.")
	
		FOR index = 0 TO (g_HeistPrePlanningClient.iTotalPrePlanMissions-1)
			g_HeistPrePlanningClient.iHeistMissionDepth[index] = GET_HEIST_PLANNING_MISSION_DEPTH_STAT(index)
			PRINTLN("[AMEC][HEIST_PREPLAN] - SET_HEIST_MISSION_DEPTH_DATA - LEADER ...  iHeistMissionDepth[",index,"]:	",g_HeistPrePlanningClient.iHeistMissionDepth[index])
		ENDFOR
		
	ELSE
	
		PRINTLN("[AMEC][HEIST_PREPLAN] - SET_HEIST_MISSION_DEPTH_DATA - GUEST - Set iHeistMissionDepth array using BD.")
	
		FOR index = 0 TO (g_HeistPrePlanningClient.iTotalPrePlanMissions-1)
			g_HeistPrePlanningClient.iHeistMissionDepth[index] = GlobalplayerBD_FM_HeistPlanning[g_HeistSharedClient.iCurrentPropertyOwnerIndex].sSetupMissionData[index].iBoardLevel
			PRINTLN("[AMEC][HEIST_PREPLAN] - SET_HEIST_MISSION_DEPTH_DATA - GUEST ...  iHeistMissionDepth[",index,"]:	",g_HeistPrePlanningClient.iHeistMissionDepth[index])
		ENDFOR
	
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Create the initial pre-planning slots on the board ready for the update.
PROC CREATE_HEIST_PRE_PLANNING_SLOTS()

	//STRING sMissionTitle, sTextureName, sImageName
	INT index
			
	SET_HEIST_PRE_PLANNING_NAME(g_HeistSharedClient.PlanningBoardIndex, 
								g_HeistPrePlanningClient.sPlanningData.sHeistTitle, 
								"HEIST_PP_INFO",
								"")	// Always set this up as empty (we will populate this title when we grab the names)
								//UGC_GET_CACHED_DESCRIPTION_WITH_NO_TRANSLATION_CHECK(g_HeistPrePlanningClient.sPlanningData.iHeistDescription, INVITE_MAX_DESCRIPTION_LENGTH),
								//GET_HEIST_LEADER_NAME())  // Removed as part of 1845891.


	FOR index = 0 TO (g_HeistPrePlanningClient.iTotalPrePlanRows-1)
		SET_HEIST_PRE_PLANNING_SLOT_EMPTY(g_HeistSharedClient.PlanningBoardIndex)												
	ENDFOR		
						
	// Initialise the board with the above data.
	SET_HEIST_PRE_PLANNING_INITIALISE(g_HeistSharedClient.PlanningBoardIndex) 
	
	// Set the scaleform movie state to the prep board.
	//SET_HEIST_SWITCH_TO_HEIST_BOARD(g_HeistSharedClient.PlanningBoardIndex) 

	#IF IS_DEBUG_BUILD
		DRAW_HEIST_PRE_PLANNING_WIDGETS()
		PRINTLN("[AMEC][HEIST_PREPLAN] - CREATE_HEIST_PRE_PLANNING_SLOTS - Finished setting initial slot data.")
	#ENDIF
	
	#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("[AMEC_FINAL] - CREATE_HEIST_PRE_PLANNING_SLOTS - Created template planning board slots.")
	#ENDIF
	
ENDPROC


/// PURPOSE:
///    Create the zoomable planning camera.
PROC CREATE_HEIST_PRE_PLANNING_FIRST_PERSON_CAMERA(INT iCameraPos, BOOL bDisableShake)

	SET_HEIST_FPS_CAM_FAR(g_HeistPrePlanningClient.sBoardCamData)

	INIT_FIRST_PERSON_CAMERA(	g_HeistPrePlanningClient.sBoardCamData.sFPSCamData, 
								GET_CAMERA_POSITION_FROM_INT(iCameraPos), 
								GET_CAMERA_ROTATION_FROM_INT(ci_HEIST_CAMERA_ROT_GENERIC), 
								GET_CAMERA_FOV_FROM_INT(iCameraPos, GET_IS_WIDESCREEN(), g_HeistPrePlanningClient.bHaveDoneHelp), 
								g_HeistPrePlanningClient.sBoardCamData.sFPSCamData.iLookXLimit, 
								g_HeistPrePlanningClient.sBoardCamData.sFPSCamData.iLookYLimit, 
								g_HeistPrePlanningClient.sBoardCamData.sFPSCamData.iRollLimit, 
								g_HeistPrePlanningClient.sBoardCamData.sFPSCamData.fMaxZoom, 
								TRUE, 0, -1, bDisableShake)
	
	g_HeistPrePlanningClient.iCameraLocationIndex = iCameraPos

	SET_WIDESCREEN_BORDERS(TRUE,0)	

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_PREPLAN] - CREATE_HEIST_PRE_PLANNING_FIRST_PERSON_CAMERA - Created FPS cam.")
	#ENDIF

ENDPROC

PROC CREATE_HEIST_PRE_PLANNING_BOARD_CAMERA(BOOL bDisableShake)

	// All of the loading is complete, start the help timer if required.
	IF NOT g_HeistPrePlanningClient.bHaveDoneHelp
		
		// Always start with overview.
		g_HeistPrePlanningClient.sHelpTextData.eHelpFlowState = HEIST_HELP_FLOW_OVERVIEW
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_PLANNING_MODE - bHaveDoneHelp is FALSE, setting help text mode to ACTIVE.")
		#ENDIF
		CREATE_HEIST_PRE_PLANNING_FIRST_PERSON_CAMERA(ci_HEIST_CAMERA_POS_BOARD, TRUE)
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_PLANNING_MODE - bHaveDoneHelp is TRUE, setting help text mode to INACTIVE, disable shake: ", bDisableShake)
		#ENDIF
		CREATE_HEIST_PRE_PLANNING_FIRST_PERSON_CAMERA(ci_HEIST_CAMERA_POS_BOARD, bDisableShake)
	ENDIF

ENDPROC


/// PURPOSE:
///    Maintain the scaleform instructional movie (bottom right). This handles all of it's own state and is mostly automated. If a new state is added to the
///    planning board, support must also be extended here.
/// PARAMS:
///    sScaleformStruct - 
PROC UPDATE_HEIST_PRE_PLANNING_SCALEFORM_INSTRUCTIONS(SCALEFORM_INSTRUCTIONAL_BUTTONS &sScaleformStruct, BOOL bAsGuest = FALSE)
									//INT								iSeconds)
	
	SPRITE_PLACEMENT spPlacementLocation = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	
	// Don't render the buttons if the player is typing in network chat.
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF
	
	IF GET_HEIST_PREPLAN_UPDATE_INSTR_BUTTONS()
		
		REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sScaleformStruct)
		
		IF NOT GET_IS_WIDESCREEN()
		OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
		      SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sScaleformStruct, 0.5)
		ELSE
		      SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sScaleformStruct, 0.7)
		ENDIF
		
		IF bAsGuest
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HEIST_IB_QUIT", sScaleformStruct, TRUE)
			IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_RSTICK_ALL, "HEIST_IB_LOOK", sScaleformStruct) // Look Around
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y, "HEIST_IB_ZOOM", sScaleformStruct)// Zoom
			ELSE
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_CURSOR, "HEIST_IB_LOOK", sScaleformStruct) // Look Around
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_CURSOR_SCROLL, "HEIST_IB_ZOOM", sScaleformStruct) // Zoom
			ENDIF
		ELSE
		
			SWITCH g_HeistPrePlanningClient.sBoardState.eFocusPoint
			
				// Camera focus point is the map.
				CASE HEIST_FOCUS_MAP
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HEIST_IB_QUIT", sScaleformStruct, TRUE)
					
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_X, "HEIST_IB_BOARD", sScaleformStruct, TRUE) // View map.
					ELSE
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_RB, "HEIST_IB_BOARD", sScaleformStruct, TRUE) // View player list.
					ENDIF
										
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, "HEIST_IB_ZOOM", sScaleformStruct)// Zoom
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, "HEIST_IB_LOOK", sScaleformStruct) // Look Around
				
					BREAK
					
				// Camera focus point is he general board / player list.	
				CASE HEIST_FOCUS_BOARD
				
					IF NOT IS_HIGHLIGHTED_HEIST_MISSION_ALREADY_COMPLETED()
						IF IS_MISSION_DEPTH_EQUAL_OR_BELOW_BOARD_DEPTH(g_HeistPrePlanningClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX)
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, "HEIST_IB_LAUNCH", sScaleformStruct, TRUE)
						ENDIF
					ENDIF
				
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HEIST_IB_QUIT", sScaleformStruct, TRUE) // Go back.
					
									
					// We use the same input on pc to swap between map and board
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_X, "HEIST_IB_MAP", sScaleformStruct, TRUE) // View map.
					ELSE
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_LB, "HEIST_IB_MAP", sScaleformStruct, TRUE) // View map.
					ENDIF
				
					// Can't look around or zoom when using mouse on the planning board.
					IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, "HEIST_IB_ZOOM", sScaleformStruct)// Zoom
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, "HEIST_IB_LOOK", sScaleformStruct) // Look 
					ENDIF
				
					IF NOT GET_HEIST_PREPLAN_DISABLE_NAVIGATION_BUTTON()
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_UD, "HEIST_IB_NAV", sScaleformStruct) // Navigate controls.
					ELSE
						PRINTLN("[AMEC][HEIST_PREPLAN] - UPDATE_HEIST_PRE_PLANNING_SCALEFORM_INSTRUCTIONS - bDisableNavigateButton = TRUE, not adding nav option.")
					ENDIF

					BREAK
			
			ENDSWITCH
		
		ENDIF

		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(	g_HeistSharedClient.PlanningInstButtonsIndex,
												spPlacementLocation,
												sScaleformStruct,
												SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sScaleformStruct))
												
		SET_HEIST_PREPLAN_UPDATE_INSTR_BUTTONS(FALSE)
		
	ELSE
	
		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(	g_HeistSharedClient.PlanningInstButtonsIndex,
												spPlacementLocation,
												sScaleformStruct,
												FALSE)
												
	ENDIF
								
ENDPROC


/// PURPOSE:
///    Update everything related to the heist planning map (E.G. pins, text, post-its, arrows etc). This can safely be called every frame.
PROC UPDATE_HEIST_PRE_PLANNING_MAP()
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_PREPLAN] - UPDATE_HEIST_PRE_PLANNING_MAP - LAYERS - ****************** UPDATING MAP ******************")
	#ENDIF

	// Firstly reset all previous highlighting / selection indicators.
	INT iTotal, iPointer, index, iSelectedItem
	
	iSelectedItem = (g_HeistPrePlanningClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX)
	
	FOR index = 0 TO (MAX_HEIST_PLANNING_MISSIONS-1)	
		
		IF index = iSelectedItem
		
			PRINTLN("[AMEC][HEIST_PREPLAN] - UPDATE_HEIST_PRE_PLANNING_MAP - mission index and iSelectedItem are EQUAL, adjust selected mission's graphics...")
		
			// Update map HIGHLIGHTS.
			iTotal = g_HeistPrePlanningClient.sBoardData.iMapHighlights[index] 
			
			PRINTLN("[AMEC][HEIST_PREPLAN] - UPDATE_HEIST_PRE_PLANNING_MAP - LAYERS - IGHLIGHT: iTotal = ", iTotal, ". Corrected total: ", iTotal - (index * 100))
		
			FOR iPointer = (index * 100) TO iTotal
				IF iTotal - iPointer > 0
					PRINTLN("[AMEC][HEIST_PREPLAN] - UPDATE_HEIST_PRE_PLANNING_MAP - LAYERS - ** Bringing forward HIGHLIGHT: ",iPointer," for SELECTION: ", index+1)
					BRING_HEIST_MAP_HIGHLIGHT_TO_FRONT(g_HeistSharedClient.PlanningMapIndex, iPointer)
				ENDIF
			ENDFOR
			
			// Update map POST-IT notes.
			iTotal = g_HeistPrePlanningClient.sBoardData.iMapPostIt[index] 
			
			PRINTLN("[AMEC][HEIST_PREPLAN] - UPDATE_HEIST_PRE_PLANNING_MAP - LAYERS - POST-IT: iTotal = ", iTotal, ". Corrected total: ", iTotal - (index * 100))
		
			FOR iPointer = (index * 100) TO iTotal
				IF iTotal - iPointer > 0
					PRINTLN("[AMEC][HEIST_PREPLAN] - UPDATE_HEIST_PRE_PLANNING_MAP - LAYERS - ** Bringing forward POST-IT: ",iPointer," for SELECTION: ", index+1)
					BRING_HEIST_MAP_POSTIT_TO_FRONT(g_HeistSharedClient.PlanningMapIndex, iPointer)
				ENDIF
			ENDFOR
		
		ENDIF
	
	ENDFOR

ENDPROC 


/// PURPOSE:
///    Progress the pre-planning board to launching a mission, also handles the lite cleanup of the board,
///    and returns control to the player for navigation on the corona screen.
PROC LAUNCH_HEIST_PRE_PLANNING_MISSION(INT iForceMissionIndex = -1)

	IF IS_HIGHLIGHTED_HEIST_MISSION_ALREADY_COMPLETED()
		PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - ERROR! User executed launch against already completed mission!")
		EXIT
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - Launching heist corona from activity session (following a cutscene).")
		SET_HEIST_ENTERING_CORONA_FROM_HEIST_OWNER_MODE(TRUE)
		
		IF NOT HEIST_IS_NETWORK_VOICE_ACTIVE()
			HEIST_SET_NETWORK_VOICE_ACTIVE(TRUE)
		ENDIF
	ENDIF
	
	CLEAR_ALL_BIG_MESSAGES()
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	PLAY_SOUND_FRONTEND(-1,"Highlight_Accept","DLC_HEIST_PLANNING_BOARD_SOUNDS")

	//NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	IF NOT g_TransitionSessionNonResetVars.bStraightFromHeistCutscene
		// Move the player to the corona location.
		VECTOR vHeistCoronaLocation
		GET_PLAYER_PROPERTY_HEIST_LOCATION(vHeistCoronaLocation, MP_PROP_ELEMENT_HEIST_PLAN_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vHeistCoronaLocation)
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - bStraightFromHeistCutscene = FALSE, teleporting player to centre of corona: ", vHeistCoronaLocation)
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - bStraightFromHeistCutscene = TRUE, NOT teleporting player to centre of corona.")
		#ENDIF
		g_TransitionSessionNonResetVars.bStraightFromHeistCutscene = FALSE
	ENDIF

	INT iMissionIndex

	IF iForceMissionIndex > -1
		iMissionIndex = iForceMissionIndex
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - Setting launch mission index to passed in forced value: ", iMissionIndex)
		#ENDIF
	ELSE
		iMissionIndex = g_HeistPrePlanningClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - Setting launch mission index to highlighter value(corrected): ", iMissionIndex)
		#ENDIF
	ENDIF

	TEXT_LABEL_23 tlMissionRootContID = GET_CONTENTID_FOR_SLOT(iMissionIndex)
	
	TEXT_LABEL_23 tlMissionContID = GET_CONTENTID_FROM_ROOT_CONTENTID(GET_HASH_KEY(tlMissionRootContID))
	
	IF IS_STRING_NULL_OR_EMPTY(tlMissionContID)
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - CRITICAL ERROR! GET_CONTENTID_FROM_ROOT_CONTENTID returned an INVALID mission rContID! Is the root contentID correct?")
			SCRIPT_ASSERT("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - CRITICAL ERROR! GET_CONTENTID_FROM_ROOT_CONTENTID returned an INVALID mission rContID! Is the root contentID correct?")
		#ENDIF
		
		EXIT
	
	ENDIF
	
	IF IS_SOCIAL_CLUB_ACTIVE()
		PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - Calling CLOSE_SOCIAL_CLUB_MENU().")
		CLOSE_SOCIAL_CLUB_MENU()		
	ENDIF
		
	PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - Launching heist mission...")
	
	SET_HEIST_AM_I_HEIST_LEADER_GLOBAL(TRUE)
	SET_HEIST_NONRESET_AUTO_CONFIGURE(FALSE)
	
	PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - Setting HEIST BD iPreviousPropertyIndex to: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty, " on player: ",NETWORK_PLAYER_ID_TO_INT())
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].iPreviousPropertyIndex = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
	PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - Setting HEIST BD iCurrentHeistSetupIndex to: ", (iMissionIndex), " on player: ",NETWORK_PLAYER_ID_TO_INT())
	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].iCurrentHeistSetupIndex = (iMissionIndex)
	g_TransitionSessionNonResetVars.iHeistSetupIdx = (iMissionIndex)
	
	PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - iPreviousPropertyIndex = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

	Setup_My_Heist_Corona_From_ContentID(tlMissionContID)
	
	IF g_HeistPrePlanningClient.iHeistPrePlanContext != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(g_HeistPrePlanningClient.iHeistPrePlanContext)
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_PRE_VIEW")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_PRE_DONE")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_2")
		CLEAR_HELP()
	ENDIF
	
	IF NOT IS_TRANSITION_ACTIVE()
		SET_SKYFREEZE_FROZEN()
		TRIGGER_CORONA_CAMERA_BLUR(TRUE)
	ELSE
		PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - Warning! Transition is active when launching mission. g_b_IsInTransition: ", g_b_IsInTransition)
	ENDIF
			
//	SET_CORONA_HEIST_IS_READY_TO_START_JOB(FALSE)

	SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_HEIST_PLANNING_BOARD)
	PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - Set job entry type: ciMISSION_ENTERY_TYPE_HEIST_PLANNING_BOARD")

	SET_HEIST_CORONA_FROM_PLANNING_BOARD(TRUE)
	
	#IF IS_DEBUG_BUILD
	IF DO_HEIST_TRIGGER_MID_STRAND_CUTSCENE()
	
		INT iRContID = GET_HASH_KEY(g_HeistPrePlanningClient.tlHeistFinaleRootContID)
		PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - DEBUG - Launching mission when DO_HEIST_TRIGGER_MID_STRAND_CUTSCENE = TRUE, this must be caused by debug. iRContID: ", iRContID)	
		SET_HEIST_TRIGGER_MID_STRAND_CUTSCENE(DOES_HEIST_REQUIRE_MID_STRAND_CUTSCENE(GET_HASH_KEY(g_HeistPrePlanningClient.tlHeistFinaleRootContID), g_HeistPrePlanningClient.iCurrentBoardDepth))
		IF (Is_This_Tutorial_Heist_RCID_Hash(iRContID))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_TUT, GET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_TUT) + 1)
			PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - DEBUG - Setting TUT mid cutscene to done.")
		ELIF (Is_This_Prison_Heist_RCID_Hash(iRContID))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_PRISON, GET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_PRISON) + 1)
			PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - DEBUG - Setting PRISON mid cutscene to done.")
		ELIF (Is_This_Biolab_Heist_RCID_Hash(iRContID))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_HUMANE, GET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_HUMANE) + 1)
			PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - DEBUG - Setting HUMAN mid cutscene to done.")
		ELIF (Is_This_Chicken_Heist_RCID_Hash(iRContID))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_NARC, GET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_NARC) + 1)
			PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - DEBUG - Setting NARCOTICS mid cutscene to done.")
		ELIF (Is_This_Ornate_Heist_RCID_Hash(iRContID))
			SET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_ORNATE, GET_MP_INT_CHARACTER_STAT(MP_STAT_CUTSCENE_MID_ORNATE) + 1)
			PRINTLN("[AMEC][HEIST_PREPLAN] - LAUNCH_HEIST_PRE_PLANNING_MISSION - DEBUG - Setting ORNATE mid cutscene to done.")
		ENDIF
		
	ENDIF
	#ENDIF
	
	// We now delay the clean up for a frame since the camera needs to persist for the renderphases to take affect.
	SET_HEIST_FLOW_TOGGLE_RENDERPHASE_STATE(ci_HEIST_BS_RP_LEADER_LAUNCHED_FROM_BOARD)
	
	// See above (2176595)
	//CLEAN_HEIST_PRE_PLANNING_LITE(FALSE)
	
	SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_UPDATE_APT_THREAD_BUFFER)
	
	SET_HEIST_SUPPRESS_HUD_FOR_PLANNING(TRUE)

ENDPROC

/// PURPOSE:
///    Saves our any key flags / data we need for a pre planning mission going into the mission
/// PARAMS:
///    sLaunchMissionDetails - Details of the Job
PROC PROCESS_HEIST_PRE_PLANNING_LAUNCHING(MISSION_TO_LAUNCH_DETAILS &sLaunchMissionDetails)

	// Quit out if this is a quick restart or strand mission
	IF IS_CORONA_INITIALISING_A_QUICK_RESTART()
	OR IS_A_STRAND_MISSION_BEING_INITIALISED()
		PRINTLN("[AMEC][HEIST_PREPLAN] - PROCESS_HEIST_PRE_PLANNING_LAUNCHING - Corona is quick restart or strand. EXIT")
		EXIT
	ENDIF

	IF IS_THIS_MISSION_A_PRE_PLANNING_HEIST(sLaunchMissionDetails.iMissionType, sLaunchMissionDetails.bIsHeistPlanning)
		IF GET_HEIST_AM_I_HEIST_LEADER_GLOBAL()
			PRINTLN("[AMEC][HEIST_PREPLAN] - PROCESS_HEIST_PRE_PLANNING_LAUNCHING - Setting HEIST BD iPreviousPropertyIndex to: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty, " on player: ",NETWORK_PLAYER_ID_TO_INT())
			GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].iPreviousPropertyIndex = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
		#IF IS_DEBUG_BUILD
		ELSE
			PRINTLN("[AMEC][HEIST_PREPLAN] - PROCESS_HEIST_PRE_PLANNING_LAUNCHING - Local player not the leader.")
		#ENDIF
		ENDIF
		
		SET_HEIST_EXIT_CUTS_FLOW(HEIST_CUTS_FLOW_STATE_STARTING)		
	ENDIF
ENDPROC


#IF IS_DEBUG_BUILD
PROC DEBUG_DRAW_PREPLAN_DATA()
	
	//	Test to reset the slot data and check the highlight reappears.
	IF bDebugResetSlotCachedData
		
		g_HeistPrePlanningClient.sHeistDataCache[0].sTitle = -1
		SET_HEIST_UPDATE_AVAILABLE(TRUE)
	
		bDebugResetSlotCachedData = FALSE
	ENDIF
	
	IF bRenderDebugText

		TEXT_LABEL_63 tempDebug1 = "FlowState: "
		TEXT_LABEL_63 tempDebug2 = ENUM_TO_INT(GET_HEIST_PREPLAN_STATE())
		tempDebug1 += tempDebug2
		SET_TEXT_COLOUR(255, 1, 1, 255)
		SET_TEXT_SCALE(0.0000, 0.4300)
		SET_TEXT_WRAP(0.0, 1.0)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.05, "STRING", tempDebug1)
		
		tempDebug1 = "Highlighting(M/S/L/R): "
		tempDebug2 = g_HeistPrePlanningClient.sBoardState.iHighlight
		TEXT_LABEL_63 tempDebug3 = " : "
		tempDebug1 += tempDebug2
		tempDebug1 += tempDebug3
		
		tempDebug2 = g_HeistPrePlanningClient.sBoardState.iSubHighlight
		tempDebug1 += tempDebug2
		tempDebug1 += tempDebug3
		
		tempDebug2 = g_HeistPrePlanningClient.sBoardState.iLeftArrow
		tempDebug1 += tempDebug2
		tempDebug1 += tempDebug3
		
		tempDebug2 = g_HeistPrePlanningClient.sBoardState.iRightArrow
		tempDebug1 += tempDebug2
		SET_TEXT_COLOUR(255, 1, 1, 255)
		SET_TEXT_SCALE(0.0000, 0.4300)
		SET_TEXT_WRAP(0.0, 1.0)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.075, "STRING", tempDebug1)	
		
		tempDebug1 = "FocusPoint: "
		tempDebug2 = ENUM_TO_INT(g_HeistPrePlanningClient.sBoardState.eFocusPoint)
		tempDebug1 += tempDebug2
		SET_TEXT_COLOUR(255, 1, 1, 255)
		SET_TEXT_SCALE(0.0000, 0.4300)
		SET_TEXT_WRAP(0.0, 1.0)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.10, "STRING", tempDebug1)

		tempDebug1 = "CameraLocIndex: "
		tempDebug2 = ENUM_TO_INT(g_HeistPrePlanningClient.iCameraLocationIndex)
		tempDebug1 += tempDebug2
		SET_TEXT_COLOUR(255, 1, 1, 255)
		SET_TEXT_SCALE(0.0000, 0.4300)
		SET_TEXT_WRAP(0.0, 1.0)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.125, "STRING", tempDebug1)
		
		tempDebug1 = "BoardDepth: "
		tempDebug2 = g_HeistPrePlanningClient.iCurrentBoardDepth
		tempDebug1 += tempDebug2
		SET_TEXT_COLOUR(255, 1, 1, 255)
		SET_TEXT_SCALE(0.0000, 0.4300)
		SET_TEXT_WRAP(0.0, 1.0)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.15, "STRING", tempDebug1)
		
	ENDIF

ENDPROC
#ENDIF



/* ======================================================================= *\
	HEIST PRE PLAN - FLOW PARENT METHODS
\* ======================================================================= */


/// PURPOSE:
///    Update the heist pre-planning board via the heist scaleform header.
///    Call every frame.
PROC UPDATE_HEIST_PRE_PLANNING_BOARD()

	IF NOT GET_HEIST_UPDATE_AVAILABLE()
		EXIT
	ENDIF
	
	INT index
	
	IF NOT AM_I_LEADER_OF_HEIST()
		IF g_HeistPrePlanningClient.sBoardState.iSubHighlight != INVALID_HEIST_INDEX
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_PREPLAN] - UPDATE_HEIST_PRE_PLANNING_BOARD - GUEST - iSubHighlight != ",INVALID_HEIST_INDEX,", forcing value to: ", INVALID_HEIST_INDEX)
			#ENDIF
			g_HeistPrePlanningClient.sBoardState.iSubHighlight = INVALID_HEIST_INDEX
		ENDIF
	ENDIF
	
	// Depending on the specific cutscene, it might make changes to the required board state. Currently, only mid-point tutorial cutscene needs this,
	// but it could be reused for other scenes easily.
	SWITCH GET_HEIST_TUT_CUTS_MIDPOINT_BOARD_STATE()
	
		DEFAULT
			SET_HEIST_PRE_PLAN_TEXT_VISIBLE(g_HeistSharedClient.PlanningBoardIndex, TRUE)
			SET_HEIST_PRE_PLANNING_SHOW_IMAGES(g_HeistSharedClient.PlanningBoardIndex, TRUE)
			
			IF g_HeistPrePlanningClient.bHeistMapClean
				CREATE_HEIST_PRE_PLANNING_MAP_GRAPHICS()
				g_HeistPrePlanningClient.bHeistMapClean = FALSE
			ENDIF
			
			BREAK
			
		CASE HEIST_TUT_CUTS_EMPTY
			SET_HEIST_PRE_PLAN_TEXT_VISIBLE(g_HeistSharedClient.PlanningBoardIndex, FALSE)
			SET_HEIST_PRE_PLANNING_SHOW_IMAGES(g_HeistSharedClient.PlanningBoardIndex, FALSE)
			
			IF NOT g_HeistPrePlanningClient.bHeistMapClean
				CLEAN_HEIST_PRE_PLANNING_MAP(TRUE)
				g_HeistPrePlanningClient.bHeistMapClean = TRUE
			ENDIF
			
			BREAK
			
		CASE HEIST_TUT_CUTS_TEXT
			SET_HEIST_PRE_PLAN_TEXT_VISIBLE(g_HeistSharedClient.PlanningBoardIndex, TRUE)
			SET_HEIST_PRE_PLANNING_SHOW_IMAGES(g_HeistSharedClient.PlanningBoardIndex, FALSE)
			
			IF NOT g_HeistPrePlanningClient.bHeistMapClean
				CLEAN_HEIST_PRE_PLANNING_MAP(TRUE)
				g_HeistPrePlanningClient.bHeistMapClean = TRUE
			ENDIF
			
			BREAK
			
		CASE HEIST_TUT_CUTS_PICTURES
			SET_HEIST_PRE_PLAN_TEXT_VISIBLE(g_HeistSharedClient.PlanningBoardIndex, TRUE)
			SET_HEIST_PRE_PLANNING_SHOW_IMAGES(g_HeistSharedClient.PlanningBoardIndex, TRUE)
			
			IF NOT g_HeistPrePlanningClient.bHeistMapClean
				CLEAN_HEIST_PRE_PLANNING_MAP(TRUE)
				g_HeistPrePlanningClient.bHeistMapClean = TRUE
			ENDIF
			
			BREAK
			
		CASE HEIST_TUT_CUTS_MAP
			SET_HEIST_PRE_PLAN_TEXT_VISIBLE(g_HeistSharedClient.PlanningBoardIndex, TRUE)
			SET_HEIST_PRE_PLANNING_SHOW_IMAGES(g_HeistSharedClient.PlanningBoardIndex, TRUE)
			
			IF g_HeistPrePlanningClient.bHeistMapClean
				CREATE_HEIST_PRE_PLANNING_MAP_GRAPHICS()
				g_HeistPrePlanningClient.bHeistMapClean = FALSE
			ENDIF
			
			BREAK
	
	ENDSWITCH
	
	IF g_HeistPrePlanningClient.bHaveDoneHelp
		SET_HEIST_PLANNING_HIGHLIGHT_ITEM(	g_HeistSharedClient.PlanningBoardIndex,
											g_HeistPrePlanningClient.sBoardState.iHighlight,
											g_HeistPrePlanningClient.sBoardState.iSubHighlight,
											-1, -1, TRUE)
	ENDIF
	
	BOOL bShowCircle, bTutorialStrand
	TEXT_LABEL_23 tlTextureID
	
	IF AM_I_LEADER_OF_HEIST(TRUE)
	
		TEXT_LABEL_23 tlContID = GET_HEIST_FINALE_STAT_DATA()
	
		IF GET_HASH_KEY(tlContID) = g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job
			bTutorialStrand = TRUE
		ENDIF
	ELSE
		IF g_HeistSharedClient.iCurrentPropertyOwnerIndex > INVALID_HEIST_DATA
		AND g_HeistSharedClient.iCurrentPropertyOwnerIndex < NUM_NETWORK_PLAYERS
			IF GET_HASH_KEY(GlobalplayerBD_FM_HeistPlanning[g_HeistSharedClient.iCurrentPropertyOwnerIndex].tlHeistFinaleRootContID) = g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job
				bTutorialStrand = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	FOR index = 0 TO (g_HeistPrePlanningClient.iTotalPrePlanRows-1)
	
//		// Removed for B*: 2250383.
//		IF g_HeistPrePlanningClient.iCirclePrepMission[index] = ci_HEIST_PICTURE_DO_CIRCLE
//			bShowCircle = TRUE
//		ELSE
//			bShowCircle = FALSE
//		ENDIF

		bShowCircle = FALSE

		IF g_HeistPrePlanningClient.sPhotoVars[index].bSucess
			tlTextureID = g_HeistPrePlanningClient.sPlanningData.sMissionData[index].sBranchDetails[0].tlPicture
			PRINTLN("[AMEC][HEIST_PREPLAN] - UPDATE_HEIST_PRE_PLANNING_BOARD - bSucess = TRUE for sPhotoVars index: ",index," with texture: ", tlTextureID)
		ELSE
			tlTextureID = ""
			PRINTLN("[AMEC][HEIST_PREPLAN] - UPDATE_HEIST_PRE_PLANNING_BOARD - bSucess = FALSE for sPhotoVars index: ",index)
		ENDIF

		SET_HEIST_PRE_PLANNING_UPDATE_PLANNING_SLOT(g_HeistSharedClient.PlanningBoardIndex,
													index, 
													"MPHEIST_BIOLAB",
													g_HeistPrePlanningClient.sPlanningData.sMissionData[index].sMissionTitle,
													tlTextureID, 
													g_HeistPrePlanningClient.sPlanningData.sMissionData[index].sBranchDetails[0].sPictureName,
													"", "",
													"", "",
													g_HeistPrePlanningClient.iSelectedSetupMissions[index],
													IS_MISSION_DEPTH_EQUAL_OR_BELOW_BOARD_DEPTH(index),
													bShowCircle,
													bTutorialStrand)
	
	
		SET_HEIST_PRE_PLANNING_UPDATE_SLOT_LEFT(	g_HeistSharedClient.PlanningBoardIndex, index)
		
		
		INT missionIndexFinal = -1
		IF Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
		
			IF g_HeistSharedClient.iCurrentPropertyOwnerIndex > INVALID_HEIST_DATA
			AND g_HeistSharedClient.iCurrentPropertyOwnerIndex < NUM_NETWORK_PLAYERS
			
				INT iRootContentID = g_FMMC_STRUCT.iRootContentIdHash
				TEXT_LABEL_23 tPrepRCID = GlobalplayerBD_FM_HeistPlanning[g_HeistSharedClient.iCurrentPropertyOwnerIndex].sSetupMissionData[index].tl23MissionRootContID
				
				IF NOT IS_STRING_NULL_OR_EMPTY(tPrepRCID) AND NOT ARE_STRINGS_EQUAL(tPrepRCID, ".")
					STRING sPrepRCID = TEXT_LABEL_INTO_STRING(tPrepRCID)
					INT iHash = GET_HASH_KEY(sPrepRCID)
					IF iRootContentID = iHash
						missionIndexFinal = index
					ENDIF
				ENDIF
				
			ENDIF
			
		ENDIF
		//check if player is on a heist planning mission, if so - hide current mission medals on planning board
		IF Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()) 
		AND missionIndexFinal = index
			IF SET_HEIST_PRE_PLANNING_UPDATE_SLOT_RIGHT_NO_MEDALS(g_HeistSharedClient.PlanningBoardIndex, index,
													"",
													g_HeistSharedClient.tlMissionPlayerName[index][0],
													g_HeistSharedClient.tlMissionPlayerName[index][1],
													g_HeistSharedClient.tlMissionPlayerName[index][2],
													g_HeistSharedClient.tlMissionPlayerName[index][3])	
																										
	UPDATE_HEIST_PRE_PLANNING_NAME(	g_HeistSharedClient.PlanningBoardIndex,
											g_HeistPrePlanningClient.sPlanningData.sHeistTitle,
											"HEIST_PP_INFO",
											"HEIST_PP_TEAM")	
			ENDIF
		ELSE
			IF SET_HEIST_PRE_PLANNING_UPDATE_SLOT_RIGHT(g_HeistSharedClient.PlanningBoardIndex, index,
													"",
													g_HeistSharedClient.tlMissionPlayerName[index][0],
													g_HeistSharedClient.tlMissionPlayerName[index][1],
													g_HeistSharedClient.tlMissionPlayerName[index][2],
													g_HeistSharedClient.tlMissionPlayerName[index][3])	
																										
			// If the above function has updated the right hand text, we should show the title 'PLAYERS'
			UPDATE_HEIST_PRE_PLANNING_NAME(	g_HeistSharedClient.PlanningBoardIndex,
											g_HeistPrePlanningClient.sPlanningData.sHeistTitle,
											"HEIST_PP_INFO",
											"HEIST_PP_TEAM")	
			ENDIF
		ENDIF
														
	ENDFOR
	
ENDPROC


PROC PROCESS_HEIST_BACKGROUND_DOWNLOADS()

	IF GET_HEIST_PREP_RETRY_DOWNLOAD_HEADERS()
		IF IS_HEIST_HEADER_DATA_PARSED(IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID()))
			SET_HEIST_PREP_RETRY_DOWNLOAD_HEADERS(FALSE)
			SET_HEIST_UPDATE_AVAILABLE(TRUE)
		ELSE
			// If we don't have header data, there's no point in attempting to get picture / text data.
			PRINTLN("[AMEC][HEIST_PREPLAN] - PROCESS_HEIST_BACKGROUND_DOWNLOADS - HEADERS - Still downloading header data...")
			EXIT
		ENDIF
	ENDIF

	IF GET_HEIST_PREP_RETRY_DOWNLOAD_PICTURES()
		IF LOAD_HEIST_PRE_PLANNING_MISSION_PICTURES(TRUE)
			SET_HEIST_PREP_RETRY_DOWNLOAD_PICTURES(FALSE)
			SET_HEIST_UPDATE_AVAILABLE(TRUE)
		ENDIF
	ENDIF
	
	IF GET_HEIST_PREP_RETRY_DOWNLOAD_DESC()
		IF IS_HEIST_DESCRIPTION_READY()
			SET_HEIST_PREP_RETRY_DOWNLOAD_DESC(FALSE)
			SET_HEIST_UPDATE_AVAILABLE(TRUE)
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE:
///    Begin the process of smoothly moving the camera to the specified location.
PROC HEIST_PRE_PLAN_CAM_MOVE_IN()

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_PLAN_CAM_MOVE_IN - Move heist pre-plan camera inwards. Cam location: ", g_HeistPrePlanningClient.iCameraLocationIndex)
	#ENDIF
	
	SWITCH g_HeistPrePlanningClient.iCameraLocationIndex
	
		/******************** LAYER 1 ********************/ // AKA OVERVIEW 
		CASE ci_HEIST_CAMERA_POS_OVERVIEW
			g_HeistPrePlanningClient.iCameraLocationIndex = ci_HEIST_CAMERA_POS_BOARD
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			g_HeistPrePlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
			
			TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPrePlanningClient.sBoardCamData, ci_HEIST_CAMERA_POS_BOARD)
			BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Begin the process of smoothly moving the camera to the specified location.
PROC HEIST_PRE_PLAN_CAM_MOVE_OUT()

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_PLAN_CAM_MOVE_OUT - Move heist pre-plan camera outwards. Cam location: ", g_HeistPrePlanningClient.iCameraLocationIndex)
	#ENDIF

	SWITCH g_HeistPrePlanningClient.iCameraLocationIndex
		/******************** LAYER 2 ********************/
		CASE ci_HEIST_CAMERA_POS_MAP_TUTORIAL
			g_HeistPrePlanningClient.iCameraLocationIndex = ci_HEIST_CAMERA_POS_OVERVIEW
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			g_HeistPrePlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_OVERVIEW
			
			TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPrePlanningClient.sBoardCamData, ci_HEIST_CAMERA_POS_OVERVIEW)
			BREAK
		
		CASE ci_HEIST_CAMERA_POS_BOARD
			g_HeistPrePlanningClient.iCameraLocationIndex = ci_HEIST_CAMERA_POS_OVERVIEW
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			g_HeistPrePlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_OVERVIEW
			
			TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPrePlanningClient.sBoardCamData, ci_HEIST_CAMERA_POS_OVERVIEW)
			BREAK

	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Begin the process of smoothly moving the camera to the specified location.
PROC HEIST_PRE_PLAN_CAM_MOVE_LEFT()

	// We don't want to pan the camera anywhere when the leader is in sub configuration mode. This is
	// to keep focus on the actual modification, and force the process of "saving" the changes.
	IF g_HeistPrePlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_PLAN_CAM_MOVE_LEFT - Move heist pre-plan camera left. Cam location: ", g_HeistPrePlanningClient.iCameraLocationIndex)
	#ENDIF
			
	FLOAT fPreviousBaseFOV = g_HeistPrePlanningClient.sBoardCamData.sFPSCamData.fBaseCamFov	// Save local copy to pass into Trigger.
	
	SWITCH g_HeistPrePlanningClient.iCameraLocationIndex
	
		/******************** LAYER 2 ********************/
		CASE ci_HEIST_CAMERA_POS_OVERVIEW
			g_HeistPrePlanningClient.iCameraLocationIndex = ci_HEIST_CAMERA_POS_MAP_TUTORIAL  
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			g_HeistPrePlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_MAP
			g_HeistPrePlanningClient.iSelectedPrepMission = g_HeistPrePlanningClient.sBoardState.iHighlight
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_2")
				CLEAR_HELP()
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_PREPLAN_AUDIO] - HEIST_PRE_PLAN_CAM_MOVE_LEFT - Playing Zoom_Left.")
			#ENDIF
			PLAY_SOUND_FRONTEND(-1,"Zoom_Left","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			SET_HEIST_FPS_CAM_NEAR(g_HeistPrePlanningClient.sBoardCamData)
			TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPrePlanningClient.sBoardCamData, ci_HEIST_CAMERA_POS_MAP_TUTORIAL, DEFAULT, fPreviousBaseFOV, cf_HEIST_CAM_MAX_ZOOM_FAR)
			SET_HEIST_UPDATE_AVAILABLE(TRUE)
			
			SET_HEIST_PREPLAN_UPDATE_INSTR_BUTTONS(TRUE)
			
			BREAK
			
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Begin the process of smoothly moving the camera to the specified location.
PROC HEIST_PRE_PLAN_CAM_MOVE_RIGHT()

	// We don't want to pan the camera anywhere when the leader is in sub configuration mode. This is
	// to keep focus on the actual modification, and force the process of "saving" the changes.
	IF g_HeistPrePlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_PLAN_CAM_MOVE_RIGHT - Move heist pre-plan camera right. Cam location: ", g_HeistPrePlanningClient.iCameraLocationIndex)
	#ENDIF
			
	FLOAT fPreviousBaseFOV = g_HeistPrePlanningClient.sBoardCamData.sFPSCamData.fBaseCamFov	// Save local copy to pass into Trigger.
	
	SWITCH g_HeistPrePlanningClient.iCameraLocationIndex
		
		//CASE ci_HEIST_CAMERA_POS_MAP RETURN ci_HEIST_CAMERA_POS_MAP_TUTORIAL	
			
		/******************** LAYER 2 ********************/
		//CASE ci_HEIST_CAMERA_POS_MAP
		CASE ci_HEIST_CAMERA_POS_MAP_TUTORIAL
			g_HeistPrePlanningClient.iCameraLocationIndex = ci_HEIST_CAMERA_POS_OVERVIEW  //ci_HEIST_CAMERA_POS_BOARD
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			// Set the highlighting so that that PLAYER list is selected, and player 0 is set for navigation.
			g_HeistPrePlanningClient.sBoardState.iHighlight = g_HeistPrePlanningClient.iSelectedPrepMission
			g_HeistPrePlanningClient.sBoardState.iSubHighlight = 0
			g_HeistPrePlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD 
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_PREPLAN_AUDIO] - HEIST_PRE_PLAN_CAM_MOVE_RIGHT - Playing Zoom_Right.")
			#ENDIF
			PLAY_SOUND_FRONTEND(-1,"Zoom_Right","DLC_HEIST_PLANNING_BOARD_SOUNDS")

			SET_HEIST_FPS_CAM_FAR(g_HeistPrePlanningClient.sBoardCamData)
			TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPrePlanningClient.sBoardCamData, ci_HEIST_CAMERA_POS_OVERVIEW, DEFAULT, fPreviousBaseFOV, cf_HEIST_CAM_MAX_ZOOM_NEAR)  //ci_HEIST_CAMERA_POS_BOARD)
			SET_HEIST_UPDATE_AVAILABLE(TRUE)
				
			SET_HEIST_PREPLAN_UPDATE_INSTR_BUTTONS(TRUE)
				
			BREAK
			
	ENDSWITCH
	
ENDPROC


/// PURPOSE:
///    Manage and maintain the UP/DOWN/LEFT/RIGHT selection indexes driven from by front-end controls.
/// PARAMS:
///    eHighlightType - ENUM - Direction of travel for the highlighter.
PROC UPDATE_INPUT_SELECTION_DETAILS(HEIST_HIGHLIGHT_TYPE eHighlightType = HEIST_HIGHLIGHT_NONE)

	// The leader is the only one with SUB highlight access, normal members can only use the basic player highlight.
	SWITCH eHighlightType
	
		CASE HEIST_HIGHLIGHT_DOWN
		
			// Change the highlighter behavious depending on region.
			SWITCH g_HeistPrePlanningClient.sBoardState.eFocusPoint
			
				CASE HEIST_FOCUS_BOARD
				
					SET_HEIST_UPDATE_AVAILABLE(TRUE)
				
					// PLAYER highlight region. IF IS_MISSION_DEPTH_EQUAL_OR_BELOW_BOARD_DEPTH(index)
					IF g_HeistPrePlanningClient.sBoardState.iHighlight >= ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX + g_HeistPrePlanningClient.iTotalPrePlanRows-1
						g_HeistPrePlanningClient.sBoardState.iHighlight = ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX
					ELSE
						g_HeistPrePlanningClient.sBoardState.iHighlight++
					ENDIF
					
					IF IS_HIGHLIGHTED_HEIST_MISSION_ALREADY_COMPLETED()
					
						IF g_HeistPrePlanningClient.iMissionCompleteChecklist < MAX_HEIST_PLANNING_ROWS
							g_HeistPrePlanningClient.iMissionCompleteChecklist++
							
							UPDATE_INPUT_SELECTION_DETAILS(eHighlightType)
						ELSE
							PRINTLN("[AMEC][HEIST_PREPLAN_AUDIO] - UPDATE_INPUT_SELECTION_DETAILS - DOWN - iMissionCompleteChecklist >= MAX_HEIST_PLANNING_ROWS, could not find another available prep mission. Disabling navigate.")
							g_HeistPrePlanningClient.iMissionCompleteChecklist = 0
							EXIT
						ENDIF	
						
					ELSE
					
						g_HeistPrePlanningClient.iMissionCompleteChecklist = 0
					
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_PREPLAN] - UPDATE_INPUT_SELECTION_DETAILS - Highlight direction: ", eHighlightType)
						#ENDIF
						
						IF NOT IS_MISSION_DEPTH_EQUAL_OR_BELOW_BOARD_DEPTH(g_HeistPrePlanningClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX)
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_2")
								PRINT_HELP_FOREVER("HEIST_NOTE_2")
							ENDIF
						ELSE
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_2")
								CLEAR_HELP()
							ENDIF
						ENDIF
						
						//SET_HIGHLIGHT_TO_SELECTED_MISSION()
						g_HeistPrePlanningClient.sBoardState.iSubHighlight = 0
						
						UPDATE_HEIST_PRE_PLANNING_MAP()
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_PREPLAN_AUDIO] - UPDATE_INPUT_SELECTION_DETAILS - DOWN - Playing Highlight_Move.")
						#ENDIF
						PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
							
					ENDIF
					
					BOOL bAllButOneDone
					bAllButOneDone = ARE_ALL_HEIST_MISSIONS_BUT_ONE_COMPLETE()
					SET_HEIST_PREPLAN_DISABLE_NAVIGATION_BUTTON(bAllButOneDone)
					SET_HEIST_PREPLAN_UPDATE_INSTR_BUTTONS(TRUE)

					BREAK
			
			ENDSWITCH
			
			BREAK
			
		CASE HEIST_HIGHLIGHT_UP
		
			// Change the highlighter behavious depending on region.
			SWITCH g_HeistPrePlanningClient.sBoardState.eFocusPoint
			
				CASE HEIST_FOCUS_BOARD
				
					SET_HEIST_UPDATE_AVAILABLE(TRUE)
				
					// PLAYER highlight region.
					IF g_HeistPrePlanningClient.sBoardState.iHighlight <= ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX
						g_HeistPrePlanningClient.sBoardState.iHighlight = ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX + g_HeistPrePlanningClient.iTotalPrePlanRows-1
					ELSE
						g_HeistPrePlanningClient.sBoardState.iHighlight--
					ENDIF
					
					IF IS_HIGHLIGHTED_HEIST_MISSION_ALREADY_COMPLETED()
					
						IF g_HeistPrePlanningClient.iMissionCompleteChecklist < MAX_HEIST_PLANNING_ROWS
							g_HeistPrePlanningClient.iMissionCompleteChecklist++
							
							UPDATE_INPUT_SELECTION_DETAILS(eHighlightType)
							EXIT
						ELSE
							PRINTLN("[AMEC][HEIST_PREPLAN_AUDIO] - UPDATE_INPUT_SELECTION_DETAILS - UP - iMissionCompleteChecklist >= MAX_HEIST_PLANNING_ROWS, could not find another available prep mission. Disabling navigate.")
							g_HeistPrePlanningClient.iMissionCompleteChecklist = 0
							EXIT
						ENDIF
					
					ELSE
					
						g_HeistPrePlanningClient.iMissionCompleteChecklist = 0
					
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_PREPLAN] - UPDATE_INPUT_SELECTION_DETAILS - Highlight direction: ", eHighlightType)
						#ENDIF
						
						IF NOT IS_MISSION_DEPTH_EQUAL_OR_BELOW_BOARD_DEPTH(g_HeistPrePlanningClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX)
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_2")
								PRINT_HELP_FOREVER("HEIST_NOTE_2")
							ENDIF
						ELSE
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_2")
								CLEAR_HELP()
							ENDIF
						ENDIF
						
						//SET_HIGHLIGHT_TO_SELECTED_MISSION()
						g_HeistPrePlanningClient.sBoardState.iSubHighlight = 0
						
						UPDATE_HEIST_PRE_PLANNING_MAP()
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_PREPLAN_AUDIO] - UPDATE_INPUT_SELECTION_DETAILS - UP - Playing Highlight_Move.")
						#ENDIF
						PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
					
					ENDIF
					
					BOOL bAllButOneDone
					bAllButOneDone = ARE_ALL_HEIST_MISSIONS_BUT_ONE_COMPLETE()
					SET_HEIST_PREPLAN_DISABLE_NAVIGATION_BUTTON(bAllButOneDone)
					SET_HEIST_PREPLAN_UPDATE_INSTR_BUTTONS(TRUE)
					
					BREAK
				
			ENDSWITCH
			
			BREAK
			
	ENDSWITCH

ENDPROC


/// PURPOSE:
///    Get the camera location index for where the camera is currently located.
/// RETURNS:
///    CONST_INT location ID.
FUNC INT GET_HEIST_PRE_PLAN_CAMERA_LOCATION_INDEX()

	SWITCH g_HeistPrePlanningClient.sHelpTextData.eHelpFlowState
		DEFAULT							RETURN INVALID_HEIST_DATA
		CASE HEIST_HELP_FLOW_OVERVIEW 	RETURN ci_HEIST_CAMERA_POS_OVERVIEW
		CASE HEIST_HELP_FLOW_OVERVIEW_2	RETURN ci_HEIST_CAMERA_POS_OVERVIEW
		CASE HEIST_HELP_FLOW_TABLE 		RETURN ci_HEIST_CAMERA_POS_BOARD
		CASE HEIST_HELP_FLOW_STORAGE	RETURN ci_HEIST_CAMERA_POS_STOARGE
		CASE HEIST_HELP_FLOW_STORAGE_2	RETURN ci_HEIST_CAMERA_POS_STOARGE
		CASE HEIST_HELP_FLOW_MEMBERS	RETURN ci_HEIST_CAMERA_POS_OVERVIEW
	ENDSWITCH

ENDFUNC


/// PURPOSE:
///    Get the camera rotation index for where the camera is currently located.
/// RETURNS:
///    CONST_INT rotation ID.
FUNC INT GET_HEIST_PRE_PLAN_CAMERA_ROTATION_INDEX()

	SWITCH g_HeistPrePlanningClient.sHelpTextData.eHelpFlowState
		DEFAULT							RETURN ci_HEIST_CAMERA_ROT_GENERIC
		CASE HEIST_HELP_FLOW_STORAGE	RETURN ci_HEIST_CAMERA_ROT_STOARGE
		CASE HEIST_HELP_FLOW_STORAGE_2  RETURN ci_HEIST_CAMERA_ROT_STOARGE
	ENDSWITCH

ENDFUNC


/// PURPOSE:
///    Maintain the smooth camera transitions and FPS cam position.
PROC UPDATE_HEIST_PRE_PLANNING_CAMERA()

	// Ensure the camera keeps moving smoothly to the target destination.
	MAINTAIN_SMOOTH_CAM_TRANSITION(g_HeistPrePlanningClient.sBoardCamData, FALSE, FALSE)
	
ENDPROC


/// PURPOSE:
///    Get the inputs for the heist pre-planning board. Safe to call every frame.
PROC UPDATE_HEIST_PRE_PLANNING_INPUTS_AND_CAMERA(BOOL bAsGuest)

	// Fix for 2165189, make sure we don't listen for inputs if the pause menu is up.
	IF IS_PAUSE_MENU_ACTIVE()
		EXIT
	ENDIF
	
	// Fix for 2271532, make sure we don't listen for inputs if the player is actioning an invite.
	IF IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()
	OR IS_WARNING_MESSAGE_ACTIVE()
		EXIT
	ENDIF

	// Default look and zoom params
	BOOL bAllowLook = TRUE
	BOOL bAllowZoom = TRUE
	BOOL bMouseGrabCamera = FALSE
	FLOAT fMouseSensitivity = 0.2
	
	FLOAT fBaseHeight
	FLOAT fGapFudgeFactor
	

	HEIST_CONTROL_TYPE eHeistControlType = HEIST_CONTROL_NONE
		
	// PC keyboard and mouse support
	IF IS_USING_KEYBOARD_AND_MOUSE( FRONTEND_CONTROL)
	AND NOT bAsGuest
	
		//FLOAT fBaseHeight = 0.052		// For 7 item lists
		//FLOAT fGapFudgeFactor = 0.085   // For 7 Item lists

		SET_MOUSE_CURSOR_THIS_FRAME()
		
		SET_INPUT_EXCLUSIVE( FRONTEND_CONTROL, INPUT_CURSOR_X )
		SET_INPUT_EXCLUSIVE( FRONTEND_CONTROL, INPUT_CURSOR_Y )
		
		UPDATE_MENU_CURSOR_GLOBALS()
				
		//DISPLAY_TEXT_WITH_FLOAT(0.1, 0.2, "NUMBER", fBaseHeight, 3 )
		//DISPLAY_TEXT_WITH_FLOAT(0.1, 0.3, "NUMBER", fGapFudgeFactor, 3 )
		
		// This defines the split between the map and the planning board so you can click the mouse to focus on the board or map.
		VECTOR vSplitter = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_HeistSharedClient.vBoardPosition, g_HeistSharedClient.vBoardRotation.z, <<0.1, 0.0, -g_HeistSharedClient.vWorldSize.y / 2 >> )
		VECTOR vSplitterOnScreenPos
		
		IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vSplitter, vSplitterOnScreenPos.x, vSplitterOnScreenPos.y)
			vSplitterOnScreenPos.x = -1
		ENDIF
		
		// Default mouse pointer
		SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
		
		//DRAW_DEBUG_SPHERE(vSplitter, 0.1)
		
		// Board mouse navigation
		IF g_HeistPrePlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
		
		//DISPLAY_TEXT_WITH_NUMBER(0.1, 0.1, "NUMBER",  ENUM_TO_INT(g_HeistPrePlanningClient.sBoardState.eFocusPoint))
		
			bAllowLook = FALSE
			bAllowZoom = FALSE
			
			IF g_HeistPrePlanningClient.iTotalPrePlanRows = 1
			
				fBaseHeight = PLANNING_MOUSE_BASE_HEIGHT
				fGapFudgeFactor = PLANNING_MOUSE_GAP_ADJUST 
			
			ELIF g_HeistPrePlanningClient.iTotalPrePlanRows = 2
			
				fBaseHeight = 0.262
				fGapFudgeFactor = 0.465
			
			ELSE
				
				fBaseHeight = PLANNING_MOUSE_BASE_HEIGHT
				fGapFudgeFactor = PLANNING_MOUSE_GAP_ADJUST 
			
			ENDIF
			
			// Detect menu items.
			FLOAT fYGap = (g_HeistSharedClient.vWorldSize.z - fGapFudgeFactor) /  g_HeistPrePlanningClient.iTotalPrePlanRows
			FLOAT fY
			
			INT iHighlightedItem = -1
			
			VECTOR vBoardDimensions =  g_HeistSharedClient.vWorldSize
			
			vBoardDimensions.x *= 0.60 // Adjust size for heist planning board.
			
			// Work out where the menu items are
			INT i = 0
			WHILE i < g_HeistPrePlanningClient.iTotalPrePlanRows
				
				// Detect if mouse is inside the item area.
				fY = fBaseHeight + (i * fYGap)	
				IF IS_CURSOR_INSIDE_3D_MENU_ITEM( 0.006, fY, 0.948, 0.050, g_HeistSharedClient.vBoardPosition, g_HeistSharedClient.vBoardRotation.z, vBoardDimensions )	
					iHighlightedItem = i			
				ENDIF
				
				++ i
			
			ENDWHILE
			
			// Highlighting a valid item.
			IF iHighlightedItem > -1
			
				SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
					
				// Mouse click
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		
					// Select new item
					IF iHighlightedItem != g_HeistPrePlanningClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX
					
						g_HeistPrePlanningClient.sBoardState.iHighlight = iHighlightedItem + ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX
						g_HeistPrePlanningClient.sBoardState.iSubHighlight = 0
					
						SET_HEIST_PLANNING_HIGHLIGHT_ITEM(	g_HeistSharedClient.PlanningBoardIndex,
													g_HeistPrePlanningClient.sBoardState.iHighlight,
													g_HeistPrePlanningClient.sBoardState.iSubHighlight,
													-1, -1, TRUE)
					ELSE
						// Confirm current item
						eHeistControlType = HEIST_CONTROL_A
					ENDIF
				
				ENDIF
				
					
			ELSE
				
				// Not highlighting a menu item Check for map click
				IF vSplitterOnScreenPos.x > 0.0
				
					IF g_fMenuCursorX < vSplitterOnScreenPos.x
					AND g_fMenuCursorY < 0.9
						
						SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW_LEFT)
						
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
							eHeistControlType = HEIST_CONTROL_LB
						ENDIF
					
					ENDIF
					
				ENDIF
								
			ENDIF
			
			// Go to map
			IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_FRONTEND_X)
				eHeistControlType = HEIST_CONTROL_LB
			ENDIF
			
		ENDIF
		
		IF g_HeistPrePlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_MAP
		
			bAllowLook = TRUE
			bAllowZoom = TRUE
		
			// Check for planning board click
			IF vSplitterOnScreenPos.x > 0.0
			
				IF g_fMenuCursorY < 0.9
			
					IF g_fMenuCursorX > vSplitterOnScreenPos.x
					
						SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW_RIGHT)
						
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
							eHeistControlType = HEIST_CONTROL_RB
						ENDIF
					
					// Scroll map using mouse
					ELSE
										
						// Hold left mouse to move
						IF IS_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
							SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_GRAB)
							bMouseGrabCamera = TRUE
							fMouseSensitivity = 6.0
						ELSE
							SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_OPEN)
							bMouseGrabCamera = FALSE
							fMouseSensitivity = 0.2
						ENDIF
					
					ENDIF
				
				ENDIF
				
			ENDIF
			
			// Go to board
			IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_FRONTEND_X)
				eHeistControlType = HEIST_CONTROL_RB
			ENDIF
					
		ENDIF
		
		// Cancel
		IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
			eHeistControlType = HEIST_CONTROL_B
		ENDIF
	
	ENDIF
		
	// Ensure the camera keeps moving smoothly to the target destination.
	MAINTAIN_SMOOTH_CAM_TRANSITION(g_HeistPrePlanningClient.sBoardCamData, bAllowLook, bAllowZoom, bMouseGrabCamera, fMouseSensitivity)

	IF AM_I_LEADER_OF_HEIST()
		IF NOT g_HeistPrePlanningClient.bHaveDoneHelp
			EXIT
		ENDIF
	ENDIF
		
	// If PC support hasn't set a control type then get the gamepad heist inputs
	IF eHeistControlType = HEIST_CONTROL_NONE
		eHeistControlType = GET_HEIST_INPUTS_THIS_FRAME_ONLY(g_HeistPrePlanningClient.sBoardInputData)
	ENDIF
	
	SWITCH eHeistControlType
	
		DEFAULT
		
//			#IF IS_DEBUG_BUILD
//				SCRIPT_ASSERT("[AMEC][HEIST_PREPLAN] - UPDATE_HEIST_INPUTS_AND_CAMERA - Heist control returned was not marked as valid in the HEIST_CONTROL_TYPE enum!")
//				PRINTLN("[AMEC][HEIST_PREPLAN] - UPDATE_HEIST_INPUTS_AND_CAMERA - Heist control returned was not marked as valid in the HEIST_CONTROL_TYPE enum! Value: ", ENUM_TO_INT(GET_HEIST_INPUTS_THIS_FRAME_ONLY(g_HeistPrePlanningClient.sBoardInputData)))
//			#ENDIF
		
			BREAK
	
		CASE HEIST_CONTROL_UP
		
			IF NOT bAsGuest
		
				//IF g_HeistPrePlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_OVERVIEW
					// Scroll up an entry.  
					UPDATE_INPUT_SELECTION_DETAILS(HEIST_HIGHLIGHT_UP)
				//ENDIF
			
			ENDIF
		
			BREAK
			
		CASE HEIST_CONTROL_DOWN
		
			IF NOT bAsGuest
			
				//IF g_HeistPrePlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_OVERVIEW
					// Scroll up an entry.  
					UPDATE_INPUT_SELECTION_DETAILS(HEIST_HIGHLIGHT_DOWN)
				//ENDIF
			
			ENDIF
		
			BREAK
			
		CASE HEIST_CONTROL_LEFT
		
			IF NOT bAsGuest
					
				IF g_HeistPrePlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
				OR g_HeistPrePlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
					// Scroll left across a specific player's details. 
					UPDATE_INPUT_SELECTION_DETAILS(HEIST_HIGHLIGHT_SUB_LEFT)
				ENDIF
			
			ENDIF
			
			BREAK
			
		CASE HEIST_CONTROL_RIGHT
		
			IF NOT bAsGuest
		
				IF g_HeistPrePlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
				OR g_HeistPrePlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
					// Scroll right across a specific player's details. 
					UPDATE_INPUT_SELECTION_DETAILS(HEIST_HIGHLIGHT_SUB_RIGHT)
				ENDIF
			
			ENDIF
		
			BREAK
			
		CASE HEIST_CONTROL_LB
		
			IF NOT bAsGuest
				IF g_HeistPrePlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_MAP	//	Fix for B*2185642 - RowanJ
				
					// Ensure camera on overview when switching to map
					g_HeistPrePlanningClient.iCameraLocationIndex = ci_HEIST_CAMERA_POS_OVERVIEW
					HEIST_PRE_PLAN_CAM_MOVE_LEFT()
					
				ENDIF
			ENDIF
		
			BREAK
			
		CASE HEIST_CONTROL_RB
		
			IF NOT bAsGuest
		
				//IF g_HeistPrePlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_OVERVIEW
					HEIST_PRE_PLAN_CAM_MOVE_RIGHT()
				//ENDIF
			
			ENDIF
		
			BREAK
			
		CASE HEIST_CONTROL_X
		
			// Do nothing.
		
			BREAK
			
		CASE HEIST_CONTROL_B
		
			SET_HEIST_NONRESET_AUTO_CONFIGURE(FALSE)
			g_TransitionSessionNonResetVars.bStraightFromHeistCutscene 	= FALSE
			
			IF NETWORK_IS_ACTIVITY_SESSION()
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_EXIT_CONFIRM_THREAD_BUFFER)
			ELSE
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_UPDATE_APT_THREAD_BUFFER)
				CLEAN_HEIST_PRE_PLANNING_LITE()
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_PREPLAN_AUDIO] - UPDATE_HEIST_PRE_PLANNING_INPUTS_AND_CAMERA - HEIST_CONTROL_B - Playing Highlight_Cancel.")
			#ENDIF
			PLAY_SOUND_FRONTEND(-1,"Highlight_Cancel","DLC_HEIST_PLANNING_BOARD_SOUNDS")

			BREAK
			
		CASE HEIST_CONTROL_A
		
			IF NOT bAsGuest
		
				IF g_HeistPrePlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
						
					IF IS_MISSION_DEPTH_EQUAL_OR_BELOW_BOARD_DEPTH(g_HeistPrePlanningClient.sBoardState.iHighlight - ci_HEIST_HIGHLIGHT_PRE_PLAN_START_INDEX)
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_PREPLAN_AUDIO] - UPDATE_HEIST_PRE_PLANNING_INPUTS_AND_CAMERA - HEIST_CONTROL_A - Playing Highlight_Accept.")
						#ENDIF
						PLAY_SOUND_FRONTEND(-1,"Highlight_Accept","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						
						LAUNCH_HEIST_PRE_PLANNING_MISSION()
					ELSE
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_2")
							PRINT_HELP_FOREVER("HEIST_NOTE_2")
						ENDIF
					ENDIF
						
				ENDIF
			
			ENDIF
			
			BREAK
	
	ENDSWITCH
	
	
ENDPROC


/// PURPOSE:
///    Update the help text shown for the first time that a player uses the heist planning board.
PROC UPDATE_HEIST_PRE_PLAN_HELP_TEXT_PROMPTS()
	
	//FLOAT fFOV
	IF ENUM_TO_INT(g_HeistPrePlanningClient.sHelpTextData.eHelpFlowState) != g_HeistPrePlanningClient.iHelpTextStateCached
		g_HeistPrePlanningClient.iHelpTextStateCached = ENUM_TO_INT(g_HeistPrePlanningClient.sHelpTextData.eHelpFlowState)
		
		g_HeistPrePlanningClient.sBoardState.iHighlight = INVALID_HEIST_INDEX
		
		TEXT_LABEL_23 tlHelpLabel = GET_HEIST_HELP_TEXT_LABEL(g_HeistPrePlanningClient.sHelpTextData, TRUE)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tlHelpLabel)
		
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tlHelpLabel)
				PRINT_HELP_FOREVER(tlHelpLabel)
			ENDIF
			
			INT iCamLocation = GET_HEIST_PRE_PLAN_CAMERA_LOCATION_INDEX()
			INT iCamRotation = GET_HEIST_PRE_PLAN_CAMERA_ROTATION_INDEX()
			
			IF iCamLocation != INVALID_HEIST_DATA
			AND iCamRotation != INVALID_HEIST_DATA
				g_HeistPrePlanningClient.iCameraLocationIndex = iCamLocation
				TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPrePlanningClient.sBoardCamData, iCamLocation, iCamRotation)
			ENDIF
		ELSE
		
			IF g_HeistPrePlanningClient.iCameraLocationIndex != ci_HEIST_CAMERA_POS_BOARD
				PRINTLN("[AMEC][HEIST_PREPLAN] - iCameraLocationIndex != ci_HEIST_CAMERA_POS_BOARD, do final interp to planning board centre.")
				CLEAR_HELP()
				g_HeistPrePlanningClient.iCameraLocationIndex 	= ci_HEIST_CAMERA_POS_BOARD
				SET_ALL_HEIST_HIGHLIGHTS(g_HeistPrePlanningClient.sBoardState, GET_NEXT_HEIST_MISSION_TO_PLAY())
				TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPrePlanningClient.sBoardCamData, ci_HEIST_CAMERA_POS_BOARD)
			ENDIF
		ENDIF

	ENDIF
	
ENDPROC


/// PURPOSE:
///    Keep checking that all background scripts and services are ready before forcing the finale corona ot appear. Safe to call every frame.
PROC MONITOR_HEIST_PROGRESSION_TO_FINALE()

	IF IS_HEIST_PRE_PLANNING_COMPLETE(g_HeistSharedClient.iCurrentPropertyOwnerIndex)
	AND NOT IS_PLAYER_IN_CORONA()
	
		SWITCH GET_HEIST_FINALE_STATE() 
			
			CASE HEIST_FLOW_FAKE_LAUNCH
			CASE HEIST_FLOW_FAKE_INIT
			CASE HEIST_FLOW_FAKE_UPDATE
			CASE HEIST_FLOW_FAKE_CLEANUP
				// Fake board already in progress.
				BREAK
				
			DEFAULT
				CLEAN_HEIST_SHARED_DATA()
				SET_HEIST_FINALE_STATE(HEIST_FLOW_FAKE_INIT)
				BREAK
			
		ENDSWITCH
		
	ENDIF

ENDPROC


/// PURPOSE:
///    Check if any strand-board specific help-text is active. Used for distance based culling.
/// RETURNS:
///    TRUE if no strand messages are active.
FUNC BOOL IS_A_HEIST_PREP_HELP_MESSAGE_CURRENTLY_DISPLAYING()

	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_NET")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_WNTED")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_SOLO")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Make sure that there is space for help-text before forcing an update. Prevents rapid
///    fire help-text popping.
/// RETURNS:
///    TRUE when safe to print a new help message.
FUNC BOOL IS_IT_SAFE_TO_DISPLAY_HEIST_PREP_BOARD_HELP()

	IF IS_A_HEIST_PREP_HELP_MESSAGE_CURRENTLY_DISPLAYING()
		RETURN FALSE
	ENDIF
	
	// Make sure to defer to any active help-text.
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Get the target context state based on a number of prerequisites. Allows for diffing current state with
///    desired state (aka dynamic label updates depending on environmental factors).
/// RETURNS:
///    INT state value.
FUNC INT GET_HEIST_PREP_DESIRED_CONTEXT_REGISTRATION_STATE()

	// KGM 23/2/15 [BUG 2244469]: Check if a delay is needed for a one-off piece of special help text if the replay board has just become unlocked while there is an active heist
	BOOL delayForOneOffReplayHelp = FALSE
	IF (g_hasReplayBoardBeenUnlocked)
	AND NOT (IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_HEIST_RPLY_BOARD))
		PRINTLN(".KGM [Heist][AMEC][HEIST_FAKE] - GET_HEIST_PREP_DESIRED_CONTEXT_REGISTRATION_STATE - DELAY INTERACTION: Replay Board Unlocked and one-off help text still needs to be displsyed.")
		delayForOneOffReplayHelp = TRUE
	ENDIF
	
	INT iReturnState

	IF NOT IS_PLAYER_IN_CORONA() // KGM 23/5/14: Added 'active heist corona' check to cover the corona range when the player walks out. Fixes bug: 1864336
	AND NOT GET_HEIST_DATA_DOWNLOAD_FAILED()
	AND NOT Does_This_Player_Have_An_Active_Heist_Corona()
	AND NOT GET_HEIST_NONRESET_AUTO_CONFIGURE()
	AND NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
	AND NOT Has_Cross_Session_Invite_Been_Recently_Accepted()
	AND NOT IS_HEIST_PRE_PLANNING_COMPLETE(g_HeistSharedClient.iCurrentPropertyOwnerIndex)
	AND NOT IS_THIS_A_SOLO_SESSION()
	AND NOT ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE()
	AND NOT (delayForOneOffReplayHelp)
	AND NOT IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL()
	AND NOT IS_HEIST_ACTIONING_A_QUICK_JOIN_TRANSITION()
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Appinternet")) < 1 // Fix for 1860441.
	AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
	AND (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), Get_Heist_Corona_Coords_For_Players_Currently_Occupied_Apartment()) < HEIST_CUTSCENE_TRIGGER_m)
	
		IF GET_HEIST_BACKGROUND_MODE_ACTIVE()
			iReturnState = ci_HEIST_REG_STATE_BACKGROUND
		ELIF (AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL() AND NOT SHOULD_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW())
		OR g_sMPTUNABLES.bDisable_Heist_On_Call_From_Planning_Board
		OR Is_Ped_Drunk(PLAYER_PED_ID())
			iReturnState = ci_HEIST_REG_STATE_ONCALL
		ELSE
			iReturnState = ci_HEIST_REG_STATE_DEFAULT
		ENDIF
		
	ELSE
		iReturnState = ci_HEIST_REG_STATE_NONE
	ENDIF
	
	RETURN iReturnState

ENDFUNC


/// PURPOSE:
///    Monitor for input, and automatically progress the script once detected.
PROC MONITOR_HEIST_BOARD_INTERACTION()
	
	INT iTargetContextState = GET_HEIST_PREP_DESIRED_CONTEXT_REGISTRATION_STATE()
	
	SWITCH iTargetContextState
	
		CASE ci_HEIST_REG_STATE_DEFAULT
		
			CLEAR_GENERIC_HEIST_HELP_TEXT()
			g_HeistSharedClient.bSuppressPlanningBoardContextIntention = IS_HELP_MESSAGE_BEING_DISPLAYED()
			
			IF GET_HEIST_PREP_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_DEFAULT
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_BOARD_INTERACTION - Unregister old context intention, prev state: ", GET_HEIST_PREP_CONTEXT_REGISTRATION_STATE())
				// This takes a few frames.
				RELEASE_CONTEXT_INTENTION(g_HeistPrePlanningClient.iHeistPrePlanContext)
				
				IF GET_HEIST_PREP_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_NONE
					SET_HEIST_PREP_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_NONE)
				ENDIF
			ENDIF
			
			IF g_HeistPrePlanningClient.iHeistPrePlanContext = NEW_CONTEXT_INTENTION
			AND NOT g_HeistSharedClient.bSuppressPlanningBoardContextIntention
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_BOARD_INTERACTION - Register new context intention, target state: ci_HEIST_REG_STATE_DEFAULT")
				REGISTER_CONTEXT_INTENTION(g_HeistPrePlanningClient.iHeistPrePlanContext, CP_MAXIMUM_PRIORITY, "HEIST_PRE_VIEW2")
				SET_HEIST_PREP_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_DEFAULT)
			ENDIF
			
		BREAK
		
		
		CASE ci_HEIST_REG_STATE_ONCALL
		
			CLEAR_GENERIC_HEIST_HELP_TEXT()
			g_HeistSharedClient.bSuppressPlanningBoardContextIntention = IS_HELP_MESSAGE_BEING_DISPLAYED()
			
			IF GET_HEIST_PREP_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_ONCALL
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_BOARD_INTERACTION - Unregister old context intention, prev state: ", GET_HEIST_PREP_CONTEXT_REGISTRATION_STATE())
				// This takes a few frames.
				RELEASE_CONTEXT_INTENTION(g_HeistPrePlanningClient.iHeistPrePlanContext)
				
				IF GET_HEIST_PREP_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_NONE
					SET_HEIST_PREP_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_NONE)
				ENDIF
			ENDIF
			
			IF g_HeistPrePlanningClient.iHeistPrePlanContext = NEW_CONTEXT_INTENTION
			AND NOT g_HeistSharedClient.bSuppressPlanningBoardContextIntention
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_BOARD_INTERACTION - Register new context intention, target state: ci_HEIST_REG_STATE_ONCALL")
				REGISTER_CONTEXT_INTENTION(g_HeistPrePlanningClient.iHeistPrePlanContext, CP_MAXIMUM_PRIORITY, "HEIST_PRE_VIEW")
				SET_HEIST_PREP_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_ONCALL)
			ENDIF
			
		BREAK
		
		
		CASE ci_HEIST_REG_STATE_BACKGROUND
		
			CLEAR_GENERIC_HEIST_HELP_TEXT()
			g_HeistSharedClient.bSuppressPlanningBoardContextIntention = IS_HELP_MESSAGE_BEING_DISPLAYED()
			
			IF GET_HEIST_PREP_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_BACKGROUND
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_BOARD_INTERACTION - Unregister old context intention, prev state: ", GET_HEIST_PREP_CONTEXT_REGISTRATION_STATE())
				// This takes a few frames.
				RELEASE_CONTEXT_INTENTION(g_HeistPrePlanningClient.iHeistPrePlanContext)
				
				IF GET_HEIST_PREP_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_NONE
					SET_HEIST_PREP_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_NONE)
				ENDIF
			ENDIF
			
			IF g_HeistPrePlanningClient.iHeistPrePlanContext = NEW_CONTEXT_INTENTION
			AND NOT g_HeistSharedClient.bSuppressPlanningBoardContextIntention
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_BOARD_INTERACTION - Register new context intention, target state: ci_HEIST_REG_STATE_BACKGROUND")
				REGISTER_CONTEXT_INTENTION(g_HeistPrePlanningClient.iHeistPrePlanContext, CP_MAXIMUM_PRIORITY, "HEIST_PRE_BG")
				SET_HEIST_PREP_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_BACKGROUND)
			ENDIF
		
		BREAK
		
		
		CASE ci_HEIST_REG_STATE_NONE
		
			IF GET_HEIST_PREP_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_NONE
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_BOARD_INTERACTION - Resetting saved context intention state, value: ", GET_HEIST_PREP_CONTEXT_REGISTRATION_STATE())
				SET_HEIST_PREP_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_NONE)
			ENDIF
			
			IF g_HeistPrePlanningClient.iHeistPrePlanContext != NEW_CONTEXT_INTENTION
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_HEIST_BOARD_INTERACTION - Resetting context intention.")
				RELEASE_CONTEXT_INTENTION(g_HeistPrePlanningClient.iHeistPrePlanContext)
			ENDIF
			
			IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), Get_Heist_Corona_Coords_For_Players_Currently_Occupied_Apartment()) < HEIST_CUTSCENE_TRIGGER_m)
		
				IF IS_IT_SAFE_TO_DISPLAY_HEIST_PREP_BOARD_HELP()
			
					IF ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE()
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD")
							CLEAR_HELP()
						ENDIF
						PRINT_HELP_FOREVER("FM_COR_HCLOUD")
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD")
							CLEAR_HELP()
						ENDIF
					ENDIF
					
					IF GET_HEIST_DATA_DOWNLOAD_FAILED()
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_NET")
							CLEAR_HELP()
						ENDIF
						PRINT_HELP_FOREVER("HEIST_ER_NET")
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_NET")
							CLEAR_HELP()
						ENDIF
					ENDIF
					
					IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_WNTED")
							CLEAR_HELP()
						ENDIF
						PRINT_HELP_FOREVER("HEIST_ER_WNTED")
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_WNTED")
							CLEAR_HELP()
						ENDIF
					ENDIF
					
					IF IS_THIS_A_SOLO_SESSION()
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_SOLO")
							CLEAR_HELP()
						ENDIF
						PRINT_HELP_FOREVER("HEIST_ER_SOLO")
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_SOLO")
							CLEAR_HELP()
						ENDIF
					ENDIF
					
				ENDIF
		
			ELSE
			
				IF IS_A_HEIST_PREP_HELP_MESSAGE_CURRENTLY_DISPLAYING()
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREP] - MONITOR_HEIST_BOARD_INTERACTION - Prep message is ACTIVE but distance is too far, culling help.")
					CLEAR_HELP()
				ENDIF
				
			ENDIF
		
		EXIT
			
	ENDSWITCH

	
	IF NOT IS_PAUSE_MENU_ACTIVE() // Fix for 2165189, make sure we don't listen for inputs if the pause menu is up.
	AND NOT IS_PAUSE_MENU_RESTARTING()
	AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
	
		IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(g_HeistPrePlanningClient.iHeistPrePlanContext) 
		AND HAS_CONTEXT_BUTTON_TRIGGERED(g_HeistPrePlanningClient.iHeistPrePlanContext, TRUE)
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_PREPLAN] - MONITOR_HEIST_BOARD_INTERACTION - Content intention fired, progressiong to second stage initialisation...")
			#ENDIF
		
			FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE(TRUE)
		
			RELEASE_CONTEXT_INTENTION(g_HeistPrePlanningClient.iHeistPrePlanContext)
			
			SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
			
			IF GET_HEIST_BACKGROUND_MODE_ACTIVE()
				
				PRINTLN("[AMEC][HEIST_FAKE] - MONITOR_HEIST_BOARD_INTERACTION - GET_HEIST_BACKGROUND_MODE_ACTIVE() = TRUE, launch Fleeca first job automatically.")
				LAUNCH_HEIST_PRE_PLANNING_MISSION(ci_HEIST_PRE_PLAN_MISSION_0)
			
			ELSE
			
				IF DO_HEIST_TRIGGER_MID_STRAND_CUTSCENE()
				
					SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_LAUNCH_CUTSCENE_THREAD_BUFFER)
					// Make sure we're not interrupted in the planning cam, also place this hear so that
					// the phone doesn't flick onto the screen for a frame or two before closing.
					DISABLE_CELLPHONE_THIS_FRAME_ONLY()
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_FAKE] - MONITOR_HEIST_BOARD_INTERACTION - DO_HEIST_TRIGGER_MID_STRAND_CUTSCENE() = TRUE, start loading anims.")
					#ENDIF
					
				ELSE
				
					SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_INIT_PLANNING_MODE_THREAD_BUFFER)
					// Make sure we're not interrupted in the planning cam, also place this hear so that
					// the phone doesn't flick onto the screen for a frame or two before closing.
					DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				
				ENDIF
			
			ENDIF
			
		ELIF IS_CONTEXT_INTENTION_HELP_DISPLAYING(g_HeistPrePlanningClient.iHeistPrePlanContext) 
		AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT_SECONDARY)
		AND NOT g_sMPTUNABLES.bDisable_Heist_On_Call_From_Planning_Board
		AND NOT GET_HEIST_BACKGROUND_MODE_ACTIVE()
		AND NOT Is_Ped_Drunk(PLAYER_PED_ID())
			IF REGISTER_FOR_HEIST_QUICK_MATCH_FROM_BOARD()
				RELEASE_CONTEXT_INTENTION(g_HeistPrePlanningClient.iHeistPrePlanContext)
			ENDIF
		ENDIF
	
	ENDIF

ENDPROC


PROC CLEAN_FAILED_HEIST_TRANSITION_SESSION()

	SET_NEWS_FEED_SPLASH_SCREEN_ACTIVE(FALSE)	
	TOGGLE_RENDERPHASES(TRUE) 
	TOGGLE_RENDERPHASES(TRUE)
	TRIGGER_SCREENBLUR_FADE_OUT(250)
	ANIMPOSTFX_STOP("MP_job_load")
	CLEAR_IS_HEIST_INTRO_TRANSITION_SESSION_IN_PROGRESS()
	CLEAR_DO_TRANSIITON_TO_NEW_FREEMODE_SESSION_AFER_CORONA_AND_HEIST_CUTSCENE()
	CLEAR_TRANSITION_SESSION_HIDE_HUD_ELEMENTS_DURING_TRANSITION_BACK_TO_FM()
	SET_TRANSITION_SESSION_RETURN_PLAYER_CONTROL_AFTER_CAM_DOWN()
	BUSYSPINNER_OFF()
	
	SET_ENTITY_HEADING(PLAYER_PED_ID(), POSITION_HEIST_PLAYERS_TO_FACE_BOARD(GET_ENTITY_COORDS(PLAYER_PED_ID())))
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)

ENDPROC


// PURPOSE:	KGM 12/8/14: Check if a fresh download of the Prep Missions into locations 500-518 of the Rockstar Created array is required
//
// RETURN VALUE:		BOOL			TRUE if a fresh download is required, otherwise FALSE
FUNC BOOL Is_A_Prep_Mission_Download_Required()

	// If there is a mission in the Player Broadcast Data then download it if it differs from what's already stored in array location 500
	// NOTE: We use a holding variable rather than directly checking location 500 so that the download function gets checked until completion.
	//		 If we didn't use a holding variable then we would stop checking the download function before completion so it wouldn't clean itself up

	INT playerAsIntToCheck = NETWORK_PLAYER_ID_TO_INT()

	IF (IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID()))
		// In another player's apartment
		IF (g_HeistSharedClient.iCurrentPropertyOwnerIndex = INVALID_HEIST_DATA)
			// ...but we don't know who owns the apartment, so exit here - this gets dealt with later in Alastair's routine
			#IF IS_DEBUG_BUILD
				PRINTLN(".KGM [Heist][AMEC][HEIST_PREPLAN] - Checking if Prep Mission Headers need downloaded. In an unknown player's apartment so returning NO for now.")
			#ENDIF
			
			RETURN FALSE
		ENDIF
		
		playerAsIntToCheck = g_HeistSharedClient.iCurrentPropertyOwnerIndex
	ENDIF
	
	// Get this player's first Prep Mission of their currently active Heist from their Broadcast Data
	TEXT_LABEL_23 thisPrepRCID = GlobalplayerBD_FM_HeistPlanning[playerAsIntToCheck].sSetupMissionData[0].tl23MissionRootContID
	
	IF (IS_STRING_NULL_OR_EMPTY(thisPrepRCID))
	OR (ARE_STRINGS_EQUAL(thisPrepRCID, "."))
		// ...there is no first Prep Mission in broadcast data, so nothing to do
		RETURN FALSE
	ENDIF
	
	// There is a first prep in Broadcast data, so check if there is still Planning data in Rockstar Array location 500+
	IF NOT (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
	AND (g_sLocalMPHeistControl.lhcDownloadAttempts < MAX_HEIST_PREP_MISSION_DOWNLOAD_ATTEMPTS)
		#IF IS_DEBUG_BUILD
			IF (g_sLocalMPHeistControl.lhcPrepsDownloadedIn500)
				PRINTLN(".KGM [Heist][AMEC][HEIST_PREPLAN] - Rockstar Created array location, ", FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD, " doesn't contain active data. Request download. Setting lhcPrepsDownloadedIn500 to FALSE")
			ENDIF
		#ENDIF
		
		g_sLocalMPHeistControl.lhcPrepsDownloadedIn500 = FALSE
	ENDIF
	
	// If the data is downloaded, a download is not required
	IF (g_sLocalMPHeistControl.lhcPrepsDownloadedIn500)
		RETURN FALSE
	ENDIF
	
	// A download is required
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    This method serves as the holding method before the player is even in an apartment and on a heist.
PROC HEIST_PRE_START_MONITOR_TRIGGER(BOOL bForceInactiveBoard, SCALEFORM_INSTRUCTIONAL_BUTTONS &scaleformStruct)

	#IF IS_DEBUG_BUILD
		// Clear an F9 screen indicator flag - it'll get set if the download is preventing the rest of this function from processing
		g_DEBUG_F9_Prep_Download_Active = FALSE
	#ENDIF
	
	IF bForceInactiveBoard
	OR GET_HEIST_DATA_DOWNLOAD_FAILED()
		
		IF bForceInactiveBoard
			CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - bForceInactiveBoard = TRUE, bypass normal heist pre planning stuff.")
		ELSE
			CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - GET_HEIST_DATA_DOWNLOAD_FAILED = TRUE, bypass normal heist pre planning stuff.")
			MONITOR_HEIST_BOARD_INTERACTION()
		ENDIF
		
		CONTROL_FM_HEIST_STRAND_BOARD(scaleformStruct #IF IS_DEBUG_BUILD , DEBUG_PrePlanWidgets #ENDIF , TRUE)
		//MAINTAIN_HEIST_BACKGROUND_BOARD()
		EXIT
	ENDIF
	
	// KGM 12/8/14: Download the Prep Missions into Rockstar Array Location 500+ (if required)
	IF (Is_A_Prep_Mission_Download_Required())
		PLAYER_INDEX heistLeader = PLAYER_ID()
		IF IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
			heistLeader = INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex)
		ENDIF
		
		// We need to suppress UI / Controls if we are downloading following a launch from the strand board
		IF GET_HEIST_STRAND_STATE() = HEIST_STRAND_FLOW_WAIT_FOR_STRAND
			PRINTLN("[AMEC][HEIST_STRAND] - HEIST_PRE_START_MONITOR_TRIGGER - GET_HEIST_STRAND_STATE() = HEIST_STRAND_FLOW_WAIT_FOR_STRAND - Suppress Frontend.")
			RUN_HEIST_PLANNING_IN_PROGRESS_EVERY_FRAME_COMMANDS()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - Downloading Prep Missions Headers for Player: ", GET_PLAYER_NAME(heistLeader))
		#ENDIF
		
		// KGM 20/1/15 [BUG 2203957]: In a few situations, don't download the mission header data again for the planning board - assume the data is already there - see bug for details
		BOOL doDownload = TRUE
		IF (IS_CORONA_INITIALISING_A_QUICK_RESTART())
			doDownload = FALSE
			PRINTLN(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - BUT: IGNORE DOWNLOAD (USE EXISTING DATA) - Corona Initialising Quick Restart")
			
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - BUT: IGNORE DOWNLOAD (USE EXISTING DATA) - Corona Initialising Quick Restart")
			#ENDIF
		ENDIF
		IF (doDownload)
			IF (IS_A_STRAND_MISSION_BEING_INITIALISED())
				doDownload = FALSE
				PRINTLN(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - BUT: IGNORE DOWNLOAD (USE EXISTING DATA) - Strand Mission Being Initialised")
				
				#IF USE_FINAL_PRINTS
					PRINTLN_FINAL(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - BUT: IGNORE DOWNLOAD (USE EXISTING DATA) - Strand Mission Being Initialised")
				#ENDIF
			ENDIF
		ENDIF
		IF (doDownload)
			IF (IS_THIS_A_ROUNDS_MISSION_FOR_CORONA())
				doDownload = FALSE
				PRINTLN(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - BUT: IGNORE DOWNLOAD (USE EXISTING DATA) - This is a Rounds Mission for corona")
				
				#IF USE_FINAL_PRINTS
					PRINTLN_FINAL(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - BUT: IGNORE DOWNLOAD (USE EXISTING DATA) - This is a Rounds Mission for corona")
				#ENDIF
			ENDIF
		ENDIF
		
		IF (doDownload)
			IF (IS_CLOUD_REFRESH_IS_USING_UGC())
				doDownload = FALSE
				PRINTLN(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - BUT: IGNORE DOWNLOAD (USE EXISTING DATA) - IS_CLOUD_REFRESH_IS_USING_UGC")
				
				#IF USE_FINAL_PRINTS
					PRINTLN_FINAL(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - BUT: IGNORE DOWNLOAD (USE EXISTING DATA) - IS_CLOUD_REFRESH_IS_USING_UGC")
				#ENDIF
			ENDIF
		ENDIF	

		// Sanity Check - ensure there's data in the first 'extra mission' slot
		IF NOT (doDownload)
			PRINTLN(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - Sanity Check that there is data in Rockstar Created Array extra content slot: ", FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD)
			
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - Sanity Check that there is data in Rockstar Created Array extra content slot: ", FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD)
			#ENDIF
			
			IF NOT (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - ERROR: There is no data in that slot. Request data download.")
					
					#IF USE_FINAL_PRINTS
						PRINTLN_FINAL(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - ERROR: There is no data in that slot. Request data download.")
					#ENDIF
					
					Debug_Output_Rockstar_Created_500_PLUS()
				#ENDIF
				
				doDownload = TRUE
			ENDIF
		ENDIF
		
		IF (doDownload)
			IF NOT (Load_Cloud_Loaded_Heist_Planning_Header_Data(heistLeader))
				#IF IS_DEBUG_BUILD
					// Set the F9 screen indicator flag
					g_DEBUG_F9_Prep_Download_Active = TRUE
				#ENDIF
				IF g_bIsHeistStageCommsActive
					CONTROL_FM_HEIST_STRAND_BOARD(scaleformStruct #IF IS_DEBUG_BUILD , DEBUG_PrePlanWidgets #ENDIF )
				ENDIF
				
				EXIT
			ENDIF
					
			IF NOT (Did_Cloud_Loaded_Heist_Planning_Header_Data_Load_Successfully())
				// Can another download attempt be made?
				g_sLocalMPHeistControl.lhcDownloadAttempts++
				IF (g_sLocalMPHeistControl.lhcDownloadAttempts < MAX_HEIST_PREP_MISSION_DOWNLOAD_ATTEMPTS)
					// ...yes
					#IF IS_DEBUG_BUILD
						PRINTLN(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - FAILED TO DOWNLOAD THE PREP MISSION HEADERS. RETRY. ATTEMPT: ", (g_sLocalMPHeistControl.lhcDownloadAttempts+1))
						// Set the F9 screen indicator flag
						g_DEBUG_F9_Prep_Download_Active = TRUE
					#ENDIF
					
					#IF USE_FINAL_PRINTS
						PRINTLN_FINAL(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - FAILED TO DOWNLOAD THE PREP MISSION HEADERS. RETRY. ATTEMPT: ", (g_sLocalMPHeistControl.lhcDownloadAttempts+1))
					#ENDIF
					
				ELSE
					PRINTLN(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - FAILED TO DOWNLOAD THE PREP MISSION HEADERS. ALL RETRY ATTEMPTS COMPLETE - KICK BACK TO SP")
						
					#IF USE_FINAL_PRINTS
						PRINTLN_FINAL(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - FAILED TO DOWNLOAD THE PREP MISSION HEADERS. ALL RETRY ATTEMPTS COMPLETE - KICK BACK TO SP")
					#ENDIF
						
					SET_HEIST_DATA_DOWNLOAD_FAILED(TRUE)
					
// KGM 13/2/15 [BUG 2224641 and others]: Based on an HTTP-Interceptor bug - if the data doesn't download (it's already tried three times and multiple times in the download routine itself), then kick back to SP
//					IF GET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD()
//					OR SHOULD_LAUNCH_HEIST_TUTORIAL_MID_STRAND_CUT_SCENE()
						PRINTLN(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - Header data failed to download. Kick Back to SP.")
						IF NOT IS_PLAYER_KICKED_TO_GO_OFFLINE()
							SET_PLAYER_KICKED_TO_GO_OFFLINE(TRUE)
						ENDIF
//					ENDIF
					
					g_sLocalMPHeistControl.lhcDownloadAttempts = 0
					EXIT
//					g_sLocalMPHeistControl.lhcPrepsDownloadedIn500 = TRUE
				ENDIF
				
				EXIT
			ENDIF
			
			PRINTLN(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - Prep Mission Headers Download Successful.")
			
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - Prep Mission Headers Download Successful.")
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - DOWNLOAD IGNORED. Rockstar Created array element 500+ Contents:")
				Debug_Output_Rockstar_Created_500_PLUS()
			#ENDIF
			
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - DOWNLOAD IGNORED. Rockstar Created array element 500+ Contents:")
			#ENDIF
		ENDIF
		
		// The Preps have downloaded
		g_sLocalMPHeistControl.lhcPrepsDownloadedIn500 = TRUE
		
		PRINTLN(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - g_sLocalMPHeistControl.lhcPrepsDownloadedIn500 to TRUE - The Preps have been downloaded")
		
		#IF USE_FINAL_PRINTS
			PRINTLN_FINAL(".KGM [Heist][AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - g_sLocalMPHeistControl.lhcPrepsDownloadedIn500 to TRUE - The Preps have been downloaded")
		#ENDIF
		
		// DEBUG TEST: Ensure all downloaded missions are classed as 'Planning'
		#IF IS_DEBUG_BUILD
			INT	thisArrayPos = FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED_TO_DOWNLOAD
			INT endArrayPos	 = FMMC_MAX_AT_ONCE_ROCKSTAR_CREATED
			
			WHILE (thisArrayPos < endArrayPos)
				IF (IS_BIT_SET(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iBitSet, ciROCKSTAR_CREATED_MISSION_IN_USE))
					IF (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iType = FMMC_TYPE_MISSION)
					AND (g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iSubType != FMMC_MISSION_TYPE_PLANNING)
						PRINTLN(".KGM [Heist][AMEC][HEIST_PREPLAN] - TEMPFIX: ", thisArrayPos, " - Changing Subtype to Planning. Downloaded as Mission Subtype ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iSubType)
						
						#IF USE_FINAL_PRINTS
							PRINTLN_FINAL(".KGM [Heist][AMEC][HEIST_PREPLAN] - TEMPFIX: ", thisArrayPos, " - Changing Subtype to Planning. Downloaded as Mission Subtype ", g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iSubType)
						#ENDIF
						
						//SCRIPT_ASSERT("HEIST_PRE_START_MONITOR_TRIGGER(): ERROR - Downloaded Prep Mission wasn't of Subtype 'Planning'. Tell Keith.")
						g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iSubType = FMMC_MISSION_TYPE_PLANNING
						
						CLEAR_BIT(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iBitSet, ciMISSION_OPTION_BS_CONTACT_MISSION)
						CLEAR_BIT(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iBitSet, ciMISSION_OPTION_BS_CTF)
						CLEAR_BIT(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iBitSet, ciMISSION_OPTION_BS_HEIST_HIGH_END)
						CLEAR_BIT(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iBitSet, ciMISSION_OPTION_BS_LTS)
						CLEAR_BIT(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iBitSet, ciMISSION_OPTION_BS_RANDOM_EVENT)
						CLEAR_BIT(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iBitSet, ciMISSION_OPTION_BS_RANDOM_VERSUS)
						
						SET_BIT(g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[thisArrayPos].iBitSet, ciMISSION_OPTION_BS_HEIST_PLANNING)
					ENDIF
				ENDIF
				
				thisArrayPos++
			ENDWHILE
		#ENDIF
	ENDIF


	INT index

	// Check to see if the local player is a guest of someone else's apt. If so, check that they have a heist
	// available and get the data, then skip to the board init stages.
	IF IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
		// Local player is a guest.
		// We NEED to know who the owner of the property is to extract the correct data from the player broadcast data struct.
		IF g_HeistSharedClient.iCurrentPropertyOwnerIndex = INVALID_HEIST_DATA
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - REMOTE - Waiting for property owner index to be populated by PROPERTY_INT script.")
			#ENDIF
		
			EXIT
		
		ENDIF 
		
		INT iAptOwnerPlayerIndex = g_HeistSharedClient.iCurrentPropertyOwnerIndex
		IF iAptOwnerPlayerIndex < 0
		OR iAptOwnerPlayerIndex > (NUM_NETWORK_PLAYERS-1)
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - REMOTE - ERROR! iCurrentPropertyOwnerIndex is OUT OF RANGE. Value: ", iAptOwnerPlayerIndex)
			#ENDIF
			
			EXIT
		ENDIF
		
		IF NOT NETWORK_IS_PLAYER_ACTIVE(INT_TO_PLAYERINDEX(iAptOwnerPlayerIndex))
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - REMOTE - Apartment owner is not an active player in the game, exiting heist planning scripts.")
			#ENDIF
			
			EXIT
			
		ENDIF

		// Is the heist set up? Check the root contentID. If there isn't a root content ID then the heist strand hasn't been started.
		IF IS_STRING_NULL_OR_EMPTY(GlobalplayerBD_FM_HeistPlanning[iAptOwnerPlayerIndex].tlHeistFinaleRootContID)
		OR ARE_STRINGS_EQUAL(GlobalplayerBD_FM_HeistPlanning[iAptOwnerPlayerIndex].tlHeistFinaleRootContID, ".")
			
			CONTROL_FM_HEIST_STRAND_BOARD(scaleformStruct #IF IS_DEBUG_BUILD , DEBUG_PrePlanWidgets #ENDIF )
			EXIT
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - REMOTE - Leader's BD is VALID, grabbing rContID for board load. Reported rContID: ", GlobalplayerBD_FM_HeistPlanning[iAptOwnerPlayerIndex].tlHeistFinaleRootContID)
		#ENDIF
		
		CLEAN_ALL_HEIST_STRAND_PLANNING()
		CLEAN_HEIST_SHARED_DATA(FALSE,FALSE) //Do not cull the apartment building 
		
		// Save out the data from the player to the local client. This can then be used whenever by the local client without needing the
		// heist leader participant details.
		g_HeistPrePlanningClient.tlHeistFinaleRootContID = GlobalplayerBD_FM_HeistPlanning[iAptOwnerPlayerIndex].tlHeistFinaleRootContID
		
		FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
			g_HeistPrePlanningClient.iSelectedSetupMissions[index] = GlobalplayerBD_FM_HeistPlanning[iAptOwnerPlayerIndex].iSelectedSetupMissions[index]
		ENDFOR

		// Begin the process of downloading the saved mission participants from the heist leader.
		BROADCAST_SCRIPT_EVENT_HEIST_PRE_PLAN_REQUEST_MISSION_MEMBERS(iAptOwnerPlayerIndex)
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - REMOTE - Trigger monitor successfully progressed, enabling heist board for remote client.")
		#ENDIF

		SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_INIT_APT_THREAD_BUFFER)
		
		EXIT
		
	ENDIF

	// Check stats to see if a heist has been accepted. If it has been accepted, but not yet printed, continue. If it has been accepted, and is HAS
	// been printed, then skip the rest of this stage.
	
	IF NOT IS_MP_HEIST_STRAND_ACTIVE()
	
		IF g_HeistPrePlanningClient.bIsHeistPlanningActive
			g_HeistPrePlanningClient.bIsHeistPlanningActive = FALSE
		ENDIF
		
		IF GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bPlanningBoardReady
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_MONITOR_TRIGGER - LEADER - bPlanningBoardReady was TRUE but no heist strand is active! Unsetting.")
			#ENDIF
			GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bPlanningBoardReady = FALSE
		ENDIF

		//MAINTAIN_HEIST_BACKGROUND_BOARD()
		CONTROL_FM_HEIST_STRAND_BOARD(scaleformStruct #IF IS_DEBUG_BUILD , DEBUG_PrePlanWidgets #ENDIF )
		
	ELSE
		g_HeistPrePlanningClient.bIsHeistPlanningActive = TRUE
		
		FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
			GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].iSelectedSetupMissions[index] = INVALID_HEIST_DATA
		ENDFOR
		
		CLEAN_HEIST_SHARED_DATA(FALSE)
		
		SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_INIT_APT_THREAD_BUFFER)
		
	ENDIF

ENDPROC


/// PURPOSE:
///    Check conditions / prerequisites for progressing to the heist pre-planning board initialisation. Also download all
///    data that might be required for the basic board functionality (heist descriptions, pictures etc).
PROC HEIST_PRE_START_INIT_APT(BOOL bShowPlanningImages)

	// EXIT CONDITIONS.

	// Now that we know the player is in AN apartment, we need to make sure it's the correct one.
	IF NOT IS_LOCAL_PLAYER_NEAR_HEIST_PLANNING_BOARD(ci_HEIST_BOARD_LOAD_IN_RANGE)
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - WARNING! PLAYER LOCATION NOT IN RANGE.")
//		#ENDIF
		CLEAN_ALL_HEIST_PRE_PLANNING()
		EXIT
	ENDIF
	
	BOOL bQuit
	
	IF g_HeistPrePlanningClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_SCALEFORM] != INVALID_HEIST_DATA
		IF NOT LOAD_HEIST_PRE_PLANNING_SCALEFORM()
			#IF IS_DEBUG_BUILD
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - WARNING! SCALEFORM NOT YET LOADED.")
			#ENDIF
			g_HeistPrePlanningClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_SCALEFORM]++
			bQuit = TRUE
		ENDIF
	ELSE
		IF NOT LOAD_HEIST_PRE_PLANNING_SCALEFORM()
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - !! CRITICAL ERROR !! SCALEFORM COULD NOT BE LOADED AFTER ",MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH," ATTEMPTS, TRYING AGAIN....")
			#ENDIF
			IF NOT g_HeistPrePlanningClient.bCriticalLoadFailure
				g_HeistPrePlanningClient.bCriticalLoadFailure = TRUE
			ENDIF
			
			IF NOT GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bPlanningBoardReady
				GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bPlanningBoardReady = TRUE // Make sure that other session members can progress without me should I fail to load.
			ENDIF
			
			IF GET_IS_HEIST_INTRO_TRANSITION_SESSION_IN_PROGRESS()
				CLEAN_FAILED_HEIST_TRANSITION_SESSION()
			ENDIF
			
			EXIT
		ELSE
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - SUCCESS! HEIST SCALEFORM LOADED AFTER ",MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH,"+ FRAMES.")
			#ENDIF
		ENDIF
	ENDIF
	
	// RECOVERABLE CONDITIONS.
	
	IF g_HeistPrePlanningClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_AUDIO] != INVALID_HEIST_DATA
		IF NOT LOAD_HEIST_PRE_PLANNING_AUDIO()
			#IF IS_DEBUG_BUILD
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - WARNING! AUDIO BANK NOT YET LOADED.")
			#ENDIF
			g_HeistPrePlanningClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_AUDIO]++
			bQuit = TRUE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - !! ERROR !! AUDIO BANK COULD NOT BE LOADED AFTER ",MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH," ATTEMPTS.")
		#ENDIF
	ENDIF
	
	IF g_HeistPrePlanningClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_UGC] != INVALID_HEIST_DATA
		IF NOT GET_FM_UGC_INITIAL_HAS_FINISHED()
			#IF IS_DEBUG_BUILD
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - WARNING! UGC INITIAL DATA NOT YET LOADED.")
			#ENDIF
			g_HeistPrePlanningClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_UGC]++
			bQuit = TRUE
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - !! ERROR !! UGC INITIAL DATA COULD NOT BE LOADED AFTER ",MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH," ATTEMPTS.")
		#ENDIF
	ENDIF
	
	IF g_HeistPrePlanningClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_HEADER] != INVALID_HEIST_DATA
		IF NOT IS_HEIST_HEADER_DATA_PARSED(IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID()))
			#IF IS_DEBUG_BUILD
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - WARNING! HEIST HEADER DATA NOT YET PARSED.")
			#ENDIF
			g_HeistPrePlanningClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_HEADER]++
			
			IF NOT GET_HEIST_PREP_RETRY_DOWNLOAD_HEADERS()
				bQuit = TRUE
			ELSE
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - GET_HEIST_PREP_RETRY_DOWNLOAD_HEADERS = TRUE, move on from picture download.")
				#ENDIF
			ENDIF
			
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - !! ERROR !! HEADER DATA COULD NOT BE PARSED AFTER ",MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH," ATTEMPTS.")
		#ENDIF
	ENDIF
	
	IF g_HeistPrePlanningClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_DESCRIPTIONS] != INVALID_HEIST_DATA
		IF NOT IS_HEIST_DESCRIPTION_READY()
			#IF IS_DEBUG_BUILD
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - WARNING! DESCRIPTION NOT YET LOADED.")
			#ENDIF
			g_HeistPrePlanningClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_DESCRIPTIONS]++
			
			IF NOT GET_HEIST_PREP_RETRY_DOWNLOAD_DESC()
				bQuit = TRUE
			ELSE
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - GET_HEIST_PREP_RETRY_DOWNLOAD_DESC = TRUE, move on from picture download.")
				#ENDIF
			ENDIF
			
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - !! ERROR !! DESCRIPTION COULD NOT BE LOADED AFTER ",MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH," ATTEMPTS.")
		#ENDIF
	ENDIF
	
	IF g_HeistPrePlanningClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_PICTURES] != INVALID_HEIST_DATA
		IF NOT LOAD_HEIST_PRE_PLANNING_MISSION_PICTURES(FALSE)
			#IF IS_DEBUG_BUILD
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - WARNING! MISSION PICTURES NOT YET LOADED.")
			#ENDIF
			g_HeistPrePlanningClient.iPreLoadAttempts[ci_HEIST_PRE_LOAD_ORDER_PICTURES]++
			
			IF NOT GET_HEIST_PREP_RETRY_DOWNLOAD_PICTURES()
				bQuit = TRUE
			ELSE
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - GET_HEIST_PREP_RETRY_DOWNLOAD_PICTURES = TRUE, move on from picture download.")
				#ENDIF
			ENDIF
			
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - !! ERROR !! MISSION PICTURES COULD NOT BE FULLY LOADED AFTER ",MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH," ATTEMPTS.")
		#ENDIF
	ENDIF
	
	INT index
	FOR index = 0 TO COUNT_OF(g_HeistPrePlanningClient.iPreLoadAttempts)-1
		IF g_HeistPrePlanningClient.iPreLoadAttempts[index] >= MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_LOW
			
			IF index = ci_HEIST_PRE_LOAD_ORDER_PICTURES
				
				IF NOT GET_HEIST_PREP_RETRY_DOWNLOAD_PICTURES()
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT -	Triggering background download of picture data.")
					#ENDIF
					SET_HEIST_PREP_RETRY_DOWNLOAD_PICTURES(TRUE)
				ENDIF
				
			ELIF index = ci_HEIST_PRE_LOAD_ORDER_HEADER
				
				IF NOT GET_HEIST_PREP_RETRY_DOWNLOAD_HEADERS()
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT -	Triggering background download of header data.")
					#ENDIF
					SET_HEIST_PREP_RETRY_DOWNLOAD_HEADERS(TRUE)
				ENDIF
				
			ELIF index = ci_HEIST_PRE_LOAD_ORDER_DESCRIPTIONS
				
				IF NOT GET_HEIST_PREP_RETRY_DOWNLOAD_DESC()
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT -	Triggering background download of description data.")
					#ENDIF
					SET_HEIST_PREP_RETRY_DOWNLOAD_DESC(TRUE)
				ENDIF
				
			ELSE
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT -	Item [",index,", ",GET_HEIST_PREPLAN_INIT_STATE_NAME(index),"] exceeded lower limit for download attempts, granting ",(MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH - g_HeistPrePlanningClient.iPreLoadAttempts[index])," additional frames.")
				#ENDIF
				
				IF g_HeistPrePlanningClient.iPreLoadAttempts[index] >= MAX_HEIST_PRE_PLAN_LOAD_ATTEMPTS_HIGH
					g_HeistPrePlanningClient.iPreLoadAttempts[index] = INVALID_HEIST_DATA
					
					// Added for url:bugstar:3230591 - For some reason heist data was delayed downloading, and then we got stuck
					// here without actually re-requesting the UGC data. 
					IF index = ci_HEIST_PRE_LOAD_ORDER_UGC
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - UGC Failed to load, resetting heist pre-planning progress to attempt re-downloading data.")
						#ENDIF
						
						CLEAN_ALL_HEIST_PRE_PLANNING()
						EXIT
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDFOR
	
	// Check the recoverable conditions. If any have failed, try them again next frame.
	IF bQuit
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT -		INFO	 - WAITING ON RECOVERABLE CONDITION...")
		#ENDIF
		EXIT
	ENDIF
	
	// Reset the array incase the local player abandons/restarts a new heist strand quickly.
	FOR index = 0 TO COUNT_OF(g_HeistPrePlanningClient.iPreLoadAttempts)-1
		g_HeistPrePlanningClient.iPreLoadAttempts[index] = 0
	ENDFOR
	
//	GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].iPreviousPropertyIndex = INVALID_HEIST_DATA // Removed for BS 1901504.

	g_HeistPrePlanningClient.bCriticalLoadFailure = FALSE
	
	GET_NUMBER_OF_MISSIONS_FOR_CURRENTLY_ACTIVE_HEIST_STRAND()
	
	// Only the leader needs to do this.
	IF NOT IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
	
		// Clean the local stats array to avoid any lingering data causing issues.
		SET_STATUS_ARRAY_TO_EMPTY()
	
		SET_HEIST_MISSION_DEPTH_DATA(TRUE)
		
		// Get the latest stats and update the stored values for the heist progress. Also update player broadcast data.
		UPDATE_HEIST_PROGRESS_STATS()
		
		// Create the context intentions for interacting with the pre-planning board.
		g_HeistPrePlanningClient.iHeistPrePlanContext = NEW_CONTEXT_INTENTION
		
		SET_HEIST_TRIGGER_MID_STRAND_CUTSCENE(DOES_HEIST_REQUIRE_MID_STRAND_CUTSCENE(GET_HASH_KEY(g_HeistPrePlanningClient.tlHeistFinaleRootContID), g_HeistPrePlanningClient.iCurrentBoardDepth))
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - DO_HEIST_TRIGGER_MID_STRAND_CUTSCENE() = ", PICK_STRING(DO_HEIST_TRIGGER_MID_STRAND_CUTSCENE(), "TRUE", "FALSE"))
		
		GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(PLAYER_ID())].bHeistPlanningDataReady = TRUE
		CDEBUG1LN(DEBUG_HEIST_PLAN_PROP, "HEIST_PRE_START_INIT_APT - Setting BD for bHeistPlanningDataReady to: TRUE")
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - Setting BD for bHeistPlanningDataReady to: TRUE")
		
		GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(PLAYER_ID())].iHeistPayBandCosmetic = GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_TOTAL_REWARD_COSMETIC)
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - Setting BD for iHeistPayBandCosmetic to: ", GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(PLAYER_ID())].iHeistPayBandCosmetic)
	// Remote guest ONLY.
	ELSE 
		SET_HEIST_MISSION_DEPTH_DATA(FALSE)
	ENDIF

	g_HeistPrePlanningClient.iAvailablePrePlanRows = 0	
	
	FOR index = 0 TO (g_HeistPrePlanningClient.iTotalPrePlanMissions-1)
		IF IS_MISSION_DEPTH_EQUAL_OR_BELOW_BOARD_DEPTH(index)
			g_HeistPrePlanningClient.iAvailablePrePlanRows++
		ENDIF
	ENDFOR
	
	PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - iAvailablePrePlanRows = ", g_HeistPrePlanningClient.iAvailablePrePlanRows)

	CREATE_HEIST_PRE_PLANNING_SLOTS()
	
	CREATE_HEIST_PRE_PLANNING_MAP_GRAPHICS()
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_PRE_DONE")
		CLEAR_HELP()
		IF g_HeistPrePlanningClient.iHeistPrePlanContext != NEW_CONTEXT_INTENTION
			RELEASE_CONTEXT_INTENTION(g_HeistPrePlanningClient.iHeistPrePlanContext)
		ENDIF
	ENDIF
	
	IF NOT bShowPlanningImages
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - bShowPlanningImages = FALSE, setting images to INVISIBLE.")
		#ENDIF
		SET_HEIST_PRE_PLANNING_SHOW_IMAGES(g_HeistSharedClient.PlanningBoardIndex, FALSE)
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_APT - bShowPlanningImages = TRUE, showing images are normal.")
		#ENDIF
		SET_HEIST_PRE_PLANNING_SHOW_IMAGES(g_HeistSharedClient.PlanningBoardIndex, TRUE)
	ENDIF

	// First is used for the anim that plays after the corona transition. At that point, heist planning will reboot and we
	// need to keep the anims loaded.
	IF NOT Is_There_A_MissionsAtCoords_Focus_Mission()        // Player has walked into a corona – goes FALSE during transition
  	AND NOT IS_PLAYER_IN_CORONA()                              // James is processing corona routines
  	AND NOT IS_TRANSITION_SESSION_RESTARTING()      
	AND NOT NETWORK_IS_ACTIVITY_SESSION()
		CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimFirst, FALSE)
	ENDIF

	CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimSecond, FALSE)
	CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimThird, FALSE)
	
	SET_HEIST_UPDATE_AVAILABLE(TRUE)
	
	SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_UPDATE_APT_THREAD_BUFFER)
	
ENDPROC



/* ======================================================================= *\
	HEIST PRE PLAN - STATE MANAGEMENT
\* ======================================================================= */


/// PURPOSE:
///    Update the heist pre-planning board in the background, maning when the player is not interacting with it.
///    This is the primary state of the board.
PROC HEIST_PRE_START_UPDATE_BOARD_BACKGROUND()

	// Replacement for the previous event system that caused a race condition. Check for heist elader's broadcast data
	// to make sure that the heist stuff is valid. If not, abandon ship!
	SCRIPT_TIMER sStuckTimer
	IF HAS_HEIST_BEEN_CANCELLED_GUEST_ONLY(sStuckTimer)
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - Heist leader has abandoned their current heist, move to cleanup.")
		EXIT
	ENDIF
	
	IF NOT GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bPlanningBoardReady
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - Setting local player BD bPlanningBoardReady to TRUE")
		GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bPlanningBoardReady = TRUE
	ENDIF
	
	PROCESS_HEIST_BACKGROUND_DOWNLOADS()

	// Get the latest stats and update the stored values for the heist progress.
	UPDATE_HEIST_PROGRESS_STATS()
	
	// Check if we're on fleeca, if so, make sure that we only show the background board
	// if the first mission isn't finished.
	IF IS_HEIST_BACKGROUND_BOARD_REQUIRED()
	
		MAINTAIN_HEIST_BACKGROUND_BOARD()
		
		IF NOT IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
			MONITOR_HEIST_BOARD_INTERACTION()
		ENDIF
		
		// This is in the UPDATE and not the INIT because we need to be continually checking if the progression has reached the final stage
		// because anyone who is hanging around in the owners apartment will want to see the update and get the corona?
		MONITOR_HEIST_PROGRESSION_TO_FINALE()
		
		IF GET_HEIST_NONRESET_AUTO_CONFIGURE()
			IF IS_SKYSWOOP_AT_GROUND()
				IF NOT g_HeistPrePlanningClient.bLaunchedCoronaFromPhone
					PRINTLN("[AMEC][HEIST_FAKE] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - IS_HEIST_BACKGROUND_BOARD_REQUIRED() = TRUE, and GET_HEIST_NONRESET_AUTO_CONFIGURE() = TRUE, launching heist corona automatically.")
					LAUNCH_HEIST_PRE_PLANNING_MISSION(ci_HEIST_PRE_PLAN_MISSION_0)
					g_HeistPrePlanningClient.bLaunchedCoronaFromPhone = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		EXIT
		
	ENDIF

	// Check if updates are required.
	UPDATE_HEIST_PRE_PLANNING_BOARD()
	
	// Draw the board on the wall.
	BOOL bDrawBoard = TRUE
	
	IF GET_HEIST_TUT_CUTS_MIDPOINT_BOARD_STATE() = HEIST_TUT_CUTS_EMPTY
		bDrawBoard = FALSE
	ENDIF
	
	DRAW_HEIST_SCALEFORM(bDrawBoard)
	
	// Only the leader needs to do this.
	IF NOT IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
		
		// Check if the heist owner has triggered any interaction with the board.
		MONITOR_HEIST_BOARD_INTERACTION()

		// This is in the UPDATE and not the INIT because we need to be continually checking if the progression has reached the final stage
		// because anyone who is hanging around in the owners apartment will want to see the update and get the corona?
		MONITOR_HEIST_PROGRESSION_TO_FINALE()
		
		// Pre-load heist intro animations if the player happens to be in the apartment already (not lauching from phone).
		IF DO_HEIST_TRIGGER_MID_STRAND_CUTSCENE()
		AND NOT GET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD()
		AND NOT IS_PLAYER_IN_A_FMMC_CORONA(PLAYER_ID())
		
			g_enumActiveHeistAnim eHeistAnimSecond
			g_enumActiveHeistAnim eHeistAnimThird
			
			eHeistAnimSecond 	= GET_HEIST_INTO_ANIM_PRE(	GET_HASH_KEY(g_HeistPrePlanningClient.tlHeistFinaleRootContID), 
															(NOT IS_PLAYER_PED_FEMALE(INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex))),
															FALSE,
															TRUE,
															FALSE)
			
			eHeistAnimThird		= GET_HEIST_INTO_ANIM_DURING(GET_HASH_KEY(g_HeistPrePlanningClient.tlHeistFinaleRootContID), 
															(NOT IS_PLAYER_PED_FEMALE(INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex))),
															FALSE,
															TRUE,
															FALSE)

			// The first animation is gender neutral, the other 2 need specific animations for different sexes.
			PRELOAD_HEIST_ANIM_ASSETS(HEIST_SELECTED_ANIM_PHONE, eHeistAnimSecond, eHeistAnimThird)
		
		ENDIF
	
	ENDIF
	
	IF GET_HEIST_NONRESET_AUTO_CONFIGURE()
		IF g_TransitionSessionNonResetVars.bWaitingForAutoConfigure
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - bAutoProgressToConfigure is TRUE but bWaitingForAutoConfigure is also TRUE. Waiting for FALSE...")
			#ENDIF
		ELSE
			IF IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
			
				IF GET_HEIST_JIP_TO_PLANNING_BOARD()
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - GET_HEIST_JIP_TO_PLANNING_BOARD = TRUE, hold off moving to guest state.")
					#ENDIF
					EXIT
				ENDIF
				
				IF GET_HEIST_PROPERTY_CAM_STAGE() != HEIST_PROPERTY_FINISHED
				AND GET_HEIST_PROPERTY_CAM_STAGE() != HEIST_PROPERTY_START
					#IF IS_DEBUG_BUILD
						HEIST_PROPERTY_CAM_STAGE aStage
						aStage = GET_HEIST_PROPERTY_CAM_STAGE()
						PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - Cannot load heist planning board yet, GET_HEIST_PROPERTY_CAM_STAGE() = ", GET_HEIST_PROPERTY_CAM_STATE_NAME(aStage))
					#ENDIF
					EXIT
				ENDIF
			
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - bAutoProgressToConfigure is TRUE, moving to guest mode.")
				#ENDIF
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_INIT_GUEST_MODE_THREAD_BUFFER)
				CREATE_HEIST_PRE_PLANNING_FIRST_PERSON_CAMERA(ci_HEIST_CAMERA_POS_BOARD, TRUE)
				
				// Make sure our PED doesn't get in the way.
				SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
				HIDE_ALL_OTHER_PLAYERS_FOR_CORONA()
				
				g_HeistPrePlanningClient.bFPSCamActive = TRUE
				g_HeistPrePlanningClient.bHeistCamShakeDisabled = TRUE
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - Disable automatic camera shake.")
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - bAutoProgressToConfigure is TRUE but IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY() = FALSE. Moving to HEIST_PRE_FLOW_INIT_PLANNING_MODE_THREAD_BUFFER")
				#ENDIF
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_INIT_PLANNING_MODE_THREAD_BUFFER)
				CREATE_HEIST_PRE_PLANNING_FIRST_PERSON_CAMERA(ci_HEIST_CAMERA_POS_BOARD, TRUE)
				
				// Make sure our PED doesn't get in the way.
				SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
				HIDE_ALL_OTHER_PLAYERS_FOR_CORONA()
				
				g_HeistPrePlanningClient.bFPSCamActive = TRUE
				g_HeistPrePlanningClient.bHeistCamShakeDisabled = TRUE
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - Disable automatic camera shake.")
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH g_HeistPrePlanningClient.iBackgroundFrameCount
	
		DEFAULT g_HeistPrePlanningClient.iBackgroundFrameCount = 0 BREAK
		
		CASE 0
	
			// If the local player is on a heist planning mission, get the members that he is playing with.
			// These are then temproraily displayed on the heist board until the end when the leader resets them.
			IF Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
				GET_SESSION_PLAYER_NAMES()
			ENDIF	
			
			g_HeistPrePlanningClient.iBackgroundFrameCount++
			
		BREAK
		
		CASE 1

			IF GET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD()
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - GET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD = TRUE, loading heist tut mid-point corona.")
				#ENDIF
					
				IF NOT GET_HEIST_CORONA_ACTIVE()

					IF AM_I_LEADER_OF_HEIST(TRUE)
					
						TEXT_LABEL_23 tlContentID
						tlContentID = GET_CONTENTID_FROM_ROOT_CONTENTID(GET_HASH_KEY(g_HeistPrePlanningClient.tlHeistFinaleRootContID))

						IF Setup_My_Heist_Corona_From_ContentID(tlContentID, TRUE, FALSE, TRUE)
							PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND(leader) - Corona for Mid Strand cutscene triggered")
							SET_HEIST_CORONA_ACTIVE(TRUE)
							
							VECTOR vHeistCoronaLocation
							GET_PLAYER_PROPERTY_HEIST_LOCATION(vHeistCoronaLocation, MP_PROP_ELEMENT_HEIST_PLAN_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
							SET_ENTITY_COORDS(PLAYER_PED_ID(), vHeistCoronaLocation)
							
							#IF IS_DEBUG_BUILD
								PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND(leader) - Setting player to centre of corona at: ", vHeistCoronaLocation)
							#ENDIF
							
							g_HeistPrePlanningClient.tutorialCutsceneTransTimer = GET_NETWORK_TIME()
							PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND(leader) - Setting timeout to start from now.")
							
							g_HeistPrePlanningClient.iBackgroundFrameCount++
							EXIT
							
						ENDIF
					
					ENDIF
					
					VECTOR vHeistCoronaLocation
					GET_PLAYER_PROPERTY_HEIST_LOCATION(vHeistCoronaLocation, MP_PROP_ELEMENT_HEIST_PLAN_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vHeistCoronaLocation)
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - Setting player to centre of corona at: ", vHeistCoronaLocation)
					#ENDIF
					
					START_TUTORIAL_AUTO_PROGRESS_TIMER()
					
					SET_HEIST_CORONA_ACTIVE(TRUE)
					
					g_HeistPrePlanningClient.tutorialCutsceneTransTimer = GET_NETWORK_TIME()
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - Setting timeout to start from now.")
							
				ENDIF
				
				IF NOT AM_I_LEADER_OF_HEIST()
					IF NETWORK_GET_NUM_PARTICIPANTS() <= 1
						IF GET_SKYFREEZE_STAGE() != SKYFREEZE_FROZEN
							PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - TIMEOUT ERROR! Requesting backbuffer for screen freeze next frame.")
							SET_SKYFREEZE_FROZEN(TRUE)
							g_HeistPrePlanningClient.iBackgroundFrameCount++
							EXIT
						ENDIF
						PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - TIMEOUT ERROR! Attempted to automatially progress to tutorial cutscene, but session members <= 1. Moving back to freemode.")
						CLEAR_HELP()
						SET_HEIST_TUTORIAL_MID_CUTSCENE_PRELOAD(FALSE)
						SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_EXIT_PENDING_THREAD_BUFFER)
						SET_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
						SET_IS_HEIST_ACTIVITY_SESSION_EXIT_IN_PROGRESS()
						CLEAR_TRANSITION_SESSION_LAUNCHING()
						CLEAR_TRANSITION_SESSION_RESTARTING()
						SETUP_TRANSITION_TO_SPAWN_OUT_OF_APARTMENT()
						SET_HEIST_SKIP_FIRST_SKYSWOOP_UP_CUT(TRUE)
						CLEAN_HEIST_PRE_PLANNING_LITE(FALSE)
						SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
					ENDIF
				ENDIF
				
				IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_HeistPrePlanningClient.tutorialCutsceneTransTimer)) >= ci_HEIST_TUTORIAL_AUTO_CUTSCENE_TIMER
					IF NOT IS_PLAYER_KICKED_TO_GO_OFFLINE()
						PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - CRITICAL ERROR! Automatic transition to cutscene corona from tutorial has failed. SET_PLAYER_KICKED_TO_GO_OFFLINE(TRUE).")
						SET_PLAYER_KICKED_TO_GO_OFFLINE(TRUE)
					ENDIF
				ENDIF
				
			ENDIF
			
			g_HeistPrePlanningClient.iBackgroundFrameCount++
			
		BREAK
		
		CASE 2

			// Make sure that when the player leaves the area, we clean all used assets out of memory.
			IF NOT IS_LOCAL_PLAYER_NEAR_HEIST_PLANNING_BOARD(ci_HEIST_BOARD_LOAD_IN_RANGE)
			AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_BACKGROUND - Player left the planning board area! Moving to cleanup.")
				#ENDIF
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_CLEANUP_THREAD_BUFFER)
				CLEAN_HEIST_SHARED_DATA()
			ENDIF
			
			g_HeistPrePlanningClient.iBackgroundFrameCount++
			
		BREAK
		
	ENDSWITCH
	
	IF GET_HEIST_UPDATE_AVAILABLE()
		SET_HEIST_UPDATE_AVAILABLE(FALSE)
	ENDIF

ENDPROC


/// PURPOSE:
///    Initialise method for the heist pre-planning board. Activates after all pre-requisite conditions
///    for starting the render the movie have been met.
PROC HEIST_PRE_START_INIT_PLANNING_MODE()

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_PLANNING_MODE - Starting second stage planning mode initialisation.")
	#ENDIF
	
	//IF NOT IS_SKYSWOOP_AT_GROUND()
	IF NOT IS_SKYCAM_ON_LAST_CUT()
	AND IS_PLAYER_SWITCH_IN_PROGRESS() 
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_PLANNING_MODE - IS_SKYCAM_ON_LAST_CUT() = FALSE, wait for TRUE...")
		#ENDIF
		EXIT
	ELSE
	
		IF NOT g_HeistPrePlanningClient.bCameraCreated
	
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_PLANNING_MODE - IS_SKYCAM_ON_LAST_CUT() = TRUE, move to init planning.")
			#ENDIF
			
			IF NOT g_bSuppressLaptopContextIntention
				PRINTLN("[RBJ] - net_heist_pre_planning - HEIST_PRE_START_INIT_PLANNING_MODE - g_bSuppressLaptopContextIntention set to TRUE to supress laptop trigger.")
				g_bSuppressLaptopContextIntention = TRUE
			ENDIF
			
			IF NOT GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bPlanningBoardReady
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_PLANNING_MODE - Setting local player BD bPlanningBoardReady to TRUE")
				GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bPlanningBoardReady = TRUE
			ENDIF

			g_HeistPrePlanningClient.iCameraLocationIndex 				= ci_HEIST_CAMERA_POS_BOARD
			g_HeistPrePlanningClient.sBoardCamData.iCamCurrentTarget 	= ci_HEIST_CAMERA_POS_BOARD
			g_HeistPrePlanningClient.sBoardState.eFocusPoint 			= HEIST_FOCUS_BOARD
			g_HeistPrePlanningClient.bHaveDoneHelp 						= GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_PRE_PLAN_DONE_HELP_0)
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_PRE_VIEW")
				CLEAR_HELP()
			ENDIF
			
			IF NOT g_HeistPrePlanningClient.bHaveDoneHelp
				SET_ALL_HEIST_HIGHLIGHTS(g_HeistPrePlanningClient.sBoardState, INVALID_HEIST_INDEX)
			ELSE
				SET_ALL_HEIST_HIGHLIGHTS(g_HeistPrePlanningClient.sBoardState, GET_NEXT_HEIST_MISSION_TO_PLAY())
			ENDIF
			
			// All of the loading is complete, start the help timer if required.
			IF NOT g_HeistPrePlanningClient.bFPSCamActive
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_PLANNING_MODE - bFPSCamActive = FALSE, creating FPS cam.")
				#ENDIF
				// Disable shake cam for prep board tutorial
				IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_PRE_PLAN_DONE_HELP_0)
					CREATE_HEIST_PRE_PLANNING_FIRST_PERSON_CAMERA(ci_HEIST_CAMERA_POS_BOARD, TRUE)
				ELSE
					CREATE_HEIST_PRE_PLANNING_FIRST_PERSON_CAMERA(ci_HEIST_CAMERA_POS_BOARD, FALSE)
				ENDIF
			ENDIF
			
			g_HeistPrePlanningClient.bFPSCamActive = FALSE
			
			PRINTLN("[RBJ] - net_heist_pre_planning - HEIST_PRE_START_INIT_PLANNING_MODE - IS_SKYCAM_ON_LAST_CUT - SET_PLAYER_INVISIBLE_LOCALLY called.")
			// Make sure our PED doesn't get in the way.
			
			
			IF NETWORK_IS_ACTIVITY_SESSION()
				VECTOR vHeistCoronaLocation
				GET_PLAYER_PROPERTY_HEIST_LOCATION(vHeistCoronaLocation, MP_PROP_ELEMENT_HEIST_PLAN_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vHeistCoronaLocation)
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_PLANNING_MODE - Player is in activity session, foce their position to corona centre-point.")
				#ENDIF
			ENDIF
			
			SET_HEIST_UPDATE_AVAILABLE(TRUE)
			SET_HEIST_PREPLAN_UPDATE_INSTR_BUTTONS(TRUE)
			g_HeistPrePlanningClient.bCameraCreated = TRUE
			
		ELSE
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_PLANNING_MODE - bCameraCreated = TRUE, waiting for IS_SKYSWOOP_AT_GROUND.")
		ENDIF
		
		IF ANIMPOSTFX_IS_RUNNING("SwitchOpenNeutral")
		OR ANIMPOSTFX_IS_RUNNING("SwitchOpenNeutralMid")
			PRINTLN("[RBJ] - net_heists_pre_planning - HEIST_PRE_START_INIT_PLANNING_MODE - PostFx running so switching all off.")
			ANIMPOSTFX_STOP_ALL()
		ENDIF
	
		CLEAR_ALL_BIG_MESSAGES()
		THEFEED_HIDE_THIS_FRAME()
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
		DISABLE_INTERACTION_MENU()
		
		SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
		HIDE_ALL_OTHER_PLAYERS_FOR_CORONA()
		
		DISABLE_ALL_MP_HUD()
		DISABLE_SCRIPT_HUD(HUDPART_AWARDS, TRUE)
		SET_DPADDOWN_ACTIVE(FALSE)
		
		// Keep the board alive during transition.
		DRAW_HEIST_SCALEFORM()	
		
	ENDIF	
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		EXIT
	ELSE
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_FREEZE_POSITION) // 
	ENDIF
	
	BOOL bAllButOneDone
	bAllButOneDone = ARE_ALL_HEIST_MISSIONS_BUT_ONE_COMPLETE()
	SET_HEIST_PREPLAN_DISABLE_NAVIGATION_BUTTON(bAllButOneDone)
	SET_HEIST_PREPLAN_UPDATE_INSTR_BUTTONS(TRUE)

#IF FEATURE_GEN9_STANDALONE
	HEIST_UDS_ACTIVITY_ENTER_PRE_PLANNING_BOARD()
#ENDIF // FEATURE_GEN9_STANDALONE
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_PLANNING_MODE - Progressing to global update...")
	#ENDIF
	
	SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_UPDATE_ALL_THREAD_BUFFER)
	
ENDPROC


/// PURPOSE:
///    Initialise method for the heist pre-planning board. Activates after all pre-requisite conditions
///    for starting the render the movie have been met.
PROC HEIST_PRE_START_INIT_GUEST_IDLE_MODE()

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_GUEST_IDLE_MODE - Starting guest idle state init.")
	#ENDIF
	
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		CLEAR_HELP()
	ENDIF
	
	CLEAR_ALL_BIG_MESSAGES()
	THEFEED_HIDE_THIS_FRAME()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
	DISABLE_INTERACTION_MENU()
	
	IF NOT IS_PLAYER_IN_CORONA()
		PRINT_HELP_FOREVER("HEIST_HELP_10")
	ENDIF
	
	IF NOT GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bPlanningBoardReady
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_GUEST_IDLE_MODE - Setting local player BD bPlanningBoardReady to TRUE")
		GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bPlanningBoardReady = TRUE
	ENDIF
	
	NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_FREEZE_POSITION | NSPC_CAN_BE_TARGETTED | NSPC_ALLOW_PLAYER_DAMAGE | NSPC_LEAVE_CAMERA_CONTROL_ON)
	
	g_HeistPrePlanningClient.iCameraLocationIndex 				= ci_HEIST_CAMERA_POS_BOARD
	g_HeistPrePlanningClient.sBoardCamData.iCamCurrentTarget 	= ci_HEIST_CAMERA_POS_BOARD
	g_HeistPrePlanningClient.sBoardState.eFocusPoint 			= HEIST_FOCUS_BOARD
	
	IF NOT g_HeistPrePlanningClient.bFPSCamActive
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_GUEST_IDLE_MODE - bFPSCamActive = FALSE, creating FPS cam.")
		#ENDIF
		CREATE_HEIST_PRE_PLANNING_FIRST_PERSON_CAMERA(ci_HEIST_CAMERA_POS_BOARD, FALSE)
	ENDIF
	
	g_HeistPrePlanningClient.bFPSCamActive = FALSE
	
	// Make sure our PED doesn't get in the way.
	SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
	HIDE_ALL_OTHER_PLAYERS_FOR_CORONA()
	
	DISABLE_ALL_MP_HUD()
	DISABLE_SCRIPT_HUD(HUDPART_AWARDS, TRUE)
	SET_DPADDOWN_ACTIVE(FALSE)
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		VECTOR vHeistCoronaLocation
		GET_PLAYER_PROPERTY_HEIST_LOCATION(vHeistCoronaLocation, MP_PROP_ELEMENT_HEIST_PLAN_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), vHeistCoronaLocation)
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_GUEST_IDLE_MODE - Player is in activity session, foce their position to corona centre-point.")
		#ENDIF
	ENDIF
	
	// Keep the board alive during transition.
	DRAW_HEIST_SCALEFORM()	
	
	SET_HEIST_UPDATE_AVAILABLE(TRUE)
	SET_HEIST_PREPLAN_UPDATE_INSTR_BUTTONS(TRUE)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_INIT_GUEST_IDLE_MODE - Progressing to guest idle update...")
	#ENDIF
	
	SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_UPDATE_GUEST_THREAD_BUFFER)
	
ENDPROC


/// PURPOSE:
///    Now that we know that the board is fully initialised, and the user is activly interacting,
///    we can start to moinitor otherinputs and update the highlighters.
PROC HEIST_PRE_START_UPDATE_BOARD_ALL(SCALEFORM_INSTRUCTIONAL_BUTTONS &scaleformStruct)

	IF GET_HEIST_NONRESET_AUTO_CONFIGURE()
		PRINTLN("[RBJ] - net_heists_pre_planning - HEIST_PRE_START_UPDATE_BOARD_ALL - CLEAR_HEIST_NONRESET_AUTO_CONFIGURE as GET_HEIST_NONRESET_AUTO_CONFIGURE = TRUE.")
		CLEAR_HEIST_NONRESET_AUTO_CONFIGURE()
	ENDIF

	THEFEED_HIDE_THIS_FRAME()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
	INVALIDATE_IDLE_CAM()
	SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
	HIDE_ALL_OTHER_PLAYERS_FOR_CORONA()
	DISABLE_FRONTEND_THIS_FRAME()
	DISABLE_INTERACTION_MENU()
	
	IF IS_PAUSE_MENU_ACTIVE()
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_ALL - IS_PAUSE_MENU_ACTIVE = TRUE, Attempting to kill pause menu with SET_FRONTEND_ACTIVE(FALSE)")
		SET_FRONTEND_ACTIVE(FALSE)
	ENDIF
			
	IF g_HeistPrePlanningClient.bHeistCamShakeDisabled
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_ALL - Manually enabling camera shake.")
		SHAKE_CAM(g_HeistPrePlanningClient.sBoardCamData.sFPSCamData.theCam, "HAND_SHAKE", 0.13)
		g_HeistPrePlanningClient.bHeistCamShakeDisabled = FALSE
	ENDIF
	
	PROCESS_HEIST_BACKGROUND_DOWNLOADS()
	
	IF NOT g_HeistPrePlanningClient.bHaveDoneHelp
		
		IF GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(PLAYER_ID())].iPrepHelpSequence != ci_HEIST_PREP_HELP_SEQUENCE_ACTIVE
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_ALL - Setting bOnPrepHelpSequence to ci_HEIST_PREP_HELP_SEQUENCE_ACTIVE.")
			GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(PLAYER_ID())].iPrepHelpSequence = ci_HEIST_PREP_HELP_SEQUENCE_ACTIVE
		ENDIF
	
		DISABLE_HUD_ELEMENTS_FOR_CORONA(FALSE)
	
		IF MANAGE_HEIST_HELP_TEXT_STATE(g_HeistPrePlanningClient.sHelpTextData)
		AND NOT g_HeistPrePlanningClient.sBoardCamData.bTransitionInProgress
			
			// Make sure the help-text is totally up-to-date before exiting. This also catches the finished state and lets the camera
			// finish interoplating back to the centre of the planning board before destroying the camera.
			UPDATE_HEIST_PRE_PLAN_HELP_TEXT_PROMPTS()
			
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_ALL - Help text state manager returned TRUE, dropping out of help-text.")
		
			g_HeistPrePlanningClient.bHaveDoneHelp = TRUE
			SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_PRE_PLAN_DONE_HELP_0, g_HeistPrePlanningClient.bHaveDoneHelp)
			
			CLEAR_FIRST_PERSON_CAMERA(g_HeistPrePlanningClient.sBoardCamData.sFPSCamData)
			
			CLEAR_HEIST_CAM_OPERATING_MODE(g_HeistPrePlanningClient.sBoardCamData)
			
			CREATE_HEIST_PRE_PLANNING_FIRST_PERSON_CAMERA(ci_HEIST_CAMERA_POS_BOARD, FALSE) 
			
			PRINT_HELP("HEIST_HELP_11", ci_HEIST_HELP_TEXT_FINAL_DURATION)
			
			IF GET_HEIST_HELP_SEQUENCE_IN_PROGRESS()
				SET_HEIST_HELP_SEQUENCE_IN_PROGRESS(FALSE)
			ENDIF
			
			SET_HEIST_UPDATE_AVAILABLE(TRUE)
			
		ELSE
			IF NOT GET_HEIST_HELP_SEQUENCE_IN_PROGRESS()
				SET_HEIST_HELP_SEQUENCE_IN_PROGRESS(TRUE)
			ENDIF
		
			UPDATE_HEIST_PRE_PLAN_HELP_TEXT_PROMPTS()
			
			// We only want to update the camera at this point, as the user doesn't have contol.
			UPDATE_HEIST_PRE_PLANNING_CAMERA()
		ENDIF
		
	ELSE
	
		IF GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(PLAYER_ID())].iPrepHelpSequence != ci_HEIST_PREP_HELP_SEQUENCE_INACTIVE
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_ALL - Setting bOnPrepHelpSequence to ci_HEIST_PREP_HELP_SEQUENCE_INACTIVE.")
			GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(PLAYER_ID())].iPrepHelpSequence = ci_HEIST_PREP_HELP_SEQUENCE_INACTIVE
		ENDIF
	
		// Update the scaleform instructional buttons to tell the user how to navigate.
		UPDATE_HEIST_PRE_PLANNING_SCALEFORM_INSTRUCTIONS(scaleformStruct)
		
		// Disable any background HUD elements that might get in the way. Must be called every frame.
		DISABLE_HUD_ELEMENTS_FOR_CORONA(FALSE)
		
		// Check for inputs.
		IF NOT IS_PAUSE_MENU_ACTIVE() 	
		AND NOT IS_PAUSE_MENU_RESTARTING()
		
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		
			// Update the icons if the control scheme changes.
			IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
				SET_HEIST_PREPLAN_UPDATE_INSTR_BUTTONS(TRUE)
			ENDIF
		
			UPDATE_HEIST_PRE_PLANNING_INPUTS_AND_CAMERA(FALSE)
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_2")
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
	
	// Get the latest stats and update the stored values for the heist progress.
	UPDATE_HEIST_PROGRESS_STATS()
	
	// Check if updates are required.
	UPDATE_HEIST_PRE_PLANNING_BOARD()	

	// Draw the board on the wall.
	DRAW_HEIST_SCALEFORM()	
	
	SWITCH g_HeistPrePlanningClient.iInUseFrameCount 

		DEFAULT g_HeistPrePlanningClient.iInUseFrameCount = 0 BREAK
		
		CASE 0

			// Make sure that when the player leaves the area, we clean all used assets out of memory.
			IF NOT IS_LOCAL_PLAYER_NEAR_HEIST_PLANNING_BOARD(ci_HEIST_BOARD_LOAD_IN_RANGE)
			AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_ALL - Player left the planning board area! Moving to cleanup.")
				#ENDIF
				CLEAN_HEIST_SHARED_DATA()
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_CLEANUP_THREAD_BUFFER)
			ENDIF
			
			g_HeistPrePlanningClient.iInUseFrameCount++
			
		BREAK
		
		CASE 1
	
			// Check if we're on fleeca, if so, make sure that we only show the background board
			// if the first mission isn't finished.
			IF IS_HEIST_BACKGROUND_BOARD_REQUIRED()
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_ALL - IS_HEIST_BACKGROUND_BOARD_REQUIRED() = TRUE, move to background.")
				CLEAN_HEIST_PRE_PLANNING_LITE()
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_UPDATE_APT_THREAD_BUFFER)
			ENDIF
			
			IF NOT g_bPlayerInProcessOfExitingInterior
				IF IS_SCREEN_FADED_OUT()
					IF NOT IS_SCREEN_FADING_IN()
						PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_ALL - ERROR! Screen was faded out when hesit planning was in interactive mode! Fading back in. g_bPlayerInProcessOfExitingInterior = FALSE.")
						DO_SCREEN_FADE_IN(500)
					ENDIF
				ENDIF
			ENDIF
			
			g_HeistPrePlanningClient.iInUseFrameCount++
			
		BREAK
		
		CASE 2
	
			IF IS_THIS_PLAYER_SCTV_OR_JOINING_AS_SCTV(PLAYER_ID()) 
				CLEAN_HEIST_PRE_PLANNING_LITE()
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_UPDATE_APT_THREAD_BUFFER)
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_ALL - Player has enabled SCTV, returning to background update.")
				EXIT
			ENDIF
			
			// Replacement for the previous event system that caused a race condition. Check for heist elader's broadcast data
			// to make sure that the heist stuff is valid. If not, abandon ship!
			SCRIPT_TIMER sStuckTimer
			IF HAS_HEIST_BEEN_CANCELLED_GUEST_ONLY(sStuckTimer)
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_ALL - Heist leader has abandoned their current heist, move to cleanup.")
				g_HeistPrePlanningClient.iInUseFrameCount++
				EXIT
			ENDIF
			
			g_HeistPrePlanningClient.iInUseFrameCount++
		
		BREAK
	
		CASE 3
	
			// Make sure that the leader can never end up in a corona and in heist pre-planning configure mode.
			IF IS_PLAYER_IN_CORONA()
			AND NOT GET_HEIST_NONRESET_AUTO_CONFIGURE()
				CLEAN_HEIST_PRE_PLANNING_LITE()
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_UPDATE_APT_THREAD_BUFFER)
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_ALL - ERROR! Heist leader is in CONFIGURE and also in a CORONA, failing over to background mode to prevent further errors.")
				EXIT
			ENDIF
			
			g_HeistPrePlanningClient.iInUseFrameCount++
			
		BREAK
		
	ENDSWITCH
	
	IF ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE()
		IF NETWORK_IS_ACTIVITY_SESSION()
			IF AM_I_LEADER_OF_HEIST()
			
				CLEAR_HELP()
			
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_EXIT_PENDING_THREAD_BUFFER)
				SET_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
				SET_IS_HEIST_ACTIVITY_SESSION_EXIT_IN_PROGRESS()
				
				CLEAR_TRANSITION_SESSION_LAUNCHING()
				CLEAR_TRANSITION_SESSION_RESTARTING()
				
				BROADCAST_GENERAL_EVENT(GENERAL_EVENT_HEIST_PREPLAN_EXIT_GUEST_MODE, ALL_PLAYERS())
				SETUP_TRANSITION_TO_SPAWN_TOGETHER_IN_APARTMENT()

				TOGGLE_RENDERPHASES(FALSE)
				SET_HEIST_SKIP_FIRST_SKYSWOOP_UP_CUT(TRUE)
				CLEAN_HEIST_PRE_PLANNING_LITE(FALSE)
				SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
				
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_ALL - ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE = TRUE in ACTIVITY session, kick all members.")
			
			ENDIF
			
		ELSE
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_ALL - ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE = TRUE, kick back to background mode.")
			SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_UPDATE_APT_THREAD_BUFFER)
			CLEAN_HEIST_PRE_PLANNING_LITE()
		ENDIF
	ENDIF
	
	IF GET_HEIST_UPDATE_AVAILABLE()
		SET_HEIST_UPDATE_AVAILABLE(FALSE)
	ENDIF

ENDPROC


/// PURPOSE:
///    Now that we know that the board is fully initialised, and the user is activly interacting,
///    we can start to moinitor otherinputs and update the highlighters.
PROC HEIST_PRE_START_UPDATE_BOARD_GUEST_IDLE(SCALEFORM_INSTRUCTIONAL_BUTTONS &scaleformStruct, SCRIPT_TIMER & sStuckTimer)

	IF NOT g_bPlayerInProcessOfExitingInterior
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_SCREEN_FADING_IN()
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_GUEST_IDLE - ERROR! Screen was faded out when hesit planning was in interactive mode! Fading back in. g_bPlayerInProcessOfExitingInterior = FALSE.")
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	ENDIF
	
	THEFEED_HIDE_THIS_FRAME()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
	INVALIDATE_IDLE_CAM()
	SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
	HIDE_ALL_OTHER_PLAYERS_FOR_CORONA()
	DISABLE_INTERACTION_MENU()
	
	IF g_HeistPrePlanningClient.bHeistCamShakeDisabled
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_GUEST_IDLE - Manually enabling camera shake.")
		SHAKE_CAM(g_HeistPrePlanningClient.sBoardCamData.sFPSCamData.theCam, "HAND_SHAKE", 0.13)
		g_HeistPrePlanningClient.bHeistCamShakeDisabled = FALSE
	ENDIF
	
	PROCESS_HEIST_BACKGROUND_DOWNLOADS()

	// Check for inputs.
	IF NOT IS_PAUSE_MENU_ACTIVE() 	
	AND NOT IS_PAUSE_MENU_RESTARTING()
	
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE) // Needed for PC so we can use Esc to quit.
		
		// Update the icons if the control scheme changes.
		IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
			SET_HEIST_PREPLAN_UPDATE_INSTR_BUTTONS(TRUE)
		ENDIF
	
	
		UPDATE_HEIST_PRE_PLANNING_INPUTS_AND_CAMERA(TRUE)
	ENDIF
		
	// Update the scaleform instructional buttons to tell the user how to navigate.
	UPDATE_HEIST_PRE_PLANNING_SCALEFORM_INSTRUCTIONS(scaleformStruct, TRUE)
		
	// Make sure we're not interrupted in the planning cam.
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()

	// Check if updates are required.
	UPDATE_HEIST_PRE_PLANNING_BOARD()	

	// Draw the board on the wall.
	DRAW_HEIST_SCALEFORM()
	
	// Staggered processes
	
	SWITCH g_HeistPrePlanningClient.iInUseFrameCount 

		DEFAULT g_HeistPrePlanningClient.iInUseFrameCount = 0 BREAK
		
		CASE 0
	
			// Make sure that when the player leaves the area, we clean all used assets out of memory.
			IF NOT IS_LOCAL_PLAYER_NEAR_HEIST_PLANNING_BOARD(ci_HEIST_BOARD_LOAD_IN_RANGE)
			AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_GUEST_IDLE - Player left the planning board area! Moving to cleanup.")
				#ENDIF
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_CLEANUP_THREAD_BUFFER)
				CLEAN_HEIST_SHARED_DATA()
			ENDIF
			
			g_HeistPrePlanningClient.iInUseFrameCount++
			
		BREAK
		
		CASE 1
	
			IF NETWORK_IS_ACTIVITY_SESSION()
				IF NOT IS_HEIST_LEADER_OK()			
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_GUEST_IDLE - IS_HEIST_LEADER_OK = FALSE, move to HEIST_PRE_FLOW_EXIT_PENDING.")
					CLEAR_HELP()
					SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_EXIT_PENDING_THREAD_BUFFER)
					SET_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
					SET_IS_HEIST_ACTIVITY_SESSION_EXIT_IN_PROGRESS()
					CLEAR_TRANSITION_SESSION_LAUNCHING()
					CLEAR_TRANSITION_SESSION_RESTARTING()
					SETUP_TRANSITION_TO_SPAWN_OUT_OF_APARTMENT()
					TOGGLE_RENDERPHASES(FALSE)
					SET_HEIST_SKIP_FIRST_SKYSWOOP_UP_CUT(TRUE)
					CLEAN_HEIST_PRE_PLANNING_LITE(FALSE)
					SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
					g_HeistPrePlanningClient.iInUseFrameCount++
					EXIT
				ENDIF
			ENDIF
			
			g_HeistPrePlanningClient.iInUseFrameCount++
			
		BREAK
		
		CASE 2
	
			IF GET_HEIST_EXIT_ACTIVITY_SESSION_AFTER_CUTSCENE()
				IF NETWORK_IS_ACTIVITY_SESSION()	
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_GUEST_IDLE - GET_HEIST_EXIT_ACTIVITY_SESSION_AFTER_CUTSCENE = TRUE, move to HEIST_PRE_FLOW_EXIT_PENDING.")
					CLEAR_HELP()
					SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_EXIT_PENDING_THREAD_BUFFER)
					SET_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
					SET_IS_HEIST_ACTIVITY_SESSION_EXIT_IN_PROGRESS()
					CLEAR_TRANSITION_SESSION_LAUNCHING()
					CLEAR_TRANSITION_SESSION_RESTARTING()
					SETUP_TRANSITION_TO_SPAWN_OUT_OF_APARTMENT()
					TOGGLE_RENDERPHASES(FALSE)
					SET_HEIST_SKIP_FIRST_SKYSWOOP_UP_CUT(TRUE)
					CLEAN_HEIST_PRE_PLANNING_LITE(FALSE)
					CLEAR_HEIST_EXIT_ACTIVITY_SESSION_AFTER_CUTSCENE()
					SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
					g_HeistPrePlanningClient.iInUseFrameCount++
					EXIT
				ENDIF
			ENDIF
			
			g_HeistPrePlanningClient.iInUseFrameCount++
			
		BREAK
		
		CASE 3
		
			// Replacement for the previous event system that caused a race condition. Check for heist elader's broadcast data
			// to make sure that the heist stuff is valid. If not, abandon ship!
			IF HAS_HEIST_BEEN_CANCELLED_GUEST_ONLY(sStuckTimer)
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_GUEST_IDLE - Heist leader has abandoned their current heist, move to cleanup.")
				g_HeistPrePlanningClient.iInUseFrameCount++
				EXIT
			ENDIF
			
			IF IS_THIS_PLAYER_SCTV_OR_JOINING_AS_SCTV(PLAYER_ID()) 
				CLEAN_HEIST_PRE_PLANNING_LITE()
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_UPDATE_APT_THREAD_BUFFER)
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_GUEST_IDLE - Player has enabled SCTV, returning to background update.")
				g_HeistPrePlanningClient.iInUseFrameCount++
				EXIT
			ENDIF
			
			g_HeistPrePlanningClient.iInUseFrameCount++
			
		BREAK
	
		CASE 4
	
			IF g_HeistSharedClient.iCurrentPropertyOwnerIndex > INVALID_HEIST_DATA 
			AND g_HeistSharedClient.iCurrentPropertyOwnerIndex < NUM_NETWORK_PLAYERS
			
				SWITCH GlobalplayerBD_FM_HeistPlanning[g_HeistSharedClient.iCurrentPropertyOwnerIndex].iPrepHelpSequence
				
					CASE ci_HEIST_PREP_HELP_SEQUENCE_UNSET
						// Do nothing, waiting for broadcast data sync.
						CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_GUEST_IDLE - iPrepHelpSequence = UNSET, waiting for network sync.")
						
						BREAK
					
					CASE ci_HEIST_PREP_HELP_SEQUENCE_INACTIVE
					
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_HELP_18")
							PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_GUEST_IDLE - Printing help text HEIST_HELP_18 forever.")
							PRINT_HELP_FOREVER("HEIST_HELP_18")
						ENDIF
					
						BREAK
						
					CASE ci_HEIST_PREP_HELP_SEQUENCE_ACTIVE
					
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_HELP_37")
							PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_UPDATE_BOARD_GUEST_IDLE - Printing help text HEIST_HELP_35 forever.")
							PRINT_HELP_FOREVER("HEIST_HELP_37")
						ENDIF
						
						BREAK

				ENDSWITCH
			ENDIF
			
			g_HeistPrePlanningClient.iInUseFrameCount++
			
		BREAK

	ENDSWITCH
	
	IF GET_HEIST_UPDATE_AVAILABLE()
		SET_HEIST_UPDATE_AVAILABLE(FALSE)
	ENDIF
	
ENDPROC


PROC HEIST_PRE_START_CONFIRM_EXIT(SCALEFORM_INSTRUCTIONAL_BUTTONS &scaleformStruct)

	// To stop the board visibly disappearing when entering the conrifm exit state, keep drawing the background elements but don't do any updating.	
	// Render scaleform movie.
	DRAW_HEIST_SCALEFORM()	
	
	BOOL bIsGuest = TRUE
	
	IF AM_I_LEADER_OF_HEIST()
		bIsGuest = FALSE
	ENDIF
	
	// Update the scaleform instructional buttons to tell the user how to navigate.
	UPDATE_HEIST_PRE_PLANNING_SCALEFORM_INSTRUCTIONS(scaleformStruct, bIsGuest)
	
	THEFEED_HIDE_THIS_FRAME()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
	HIDE_ALL_OTHER_PLAYERS_FOR_CORONA()
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
	INVALIDATE_IDLE_CAM()
	DISABLE_INTERACTION_MENU()

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_CONFIRM_EXIT - Waiting for confirmation of exit.")
	#ENDIF
	
	IF GET_SKYFREEZE_STAGE() != SKYFREEZE_FROZEN
		SET_SKYFREEZE_FROZEN(TRUE)
	ENDIF

	BOOL bQuit

	IF CONTROL_FM_MISSION_WARNING_SCREEN(bQuit, g_HeistPrePlanningClient.iWarningScreenButtonBitset)

		IF bQuit
		
			CLEAR_HELP()
		
			SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_EXIT_PENDING_THREAD_BUFFER)
			SET_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
			SET_IS_HEIST_ACTIVITY_SESSION_EXIT_IN_PROGRESS()
			
			CLEAR_TRANSITION_SESSION_LAUNCHING()
			CLEAR_TRANSITION_SESSION_RESTARTING()
			
			IF AM_I_LEADER_OF_HEIST()
				BROADCAST_GENERAL_EVENT(GENERAL_EVENT_HEIST_PREPLAN_EXIT_GUEST_MODE, ALL_PLAYERS())
				SETUP_TRANSITION_TO_SPAWN_TOGETHER_IN_APARTMENT()
			ELSE
				SETUP_TRANSITION_TO_SPAWN_OUT_OF_APARTMENT()
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_CONFIRM_EXIT - Member leaving heist activity, don't respawn in apartment. SETUP_TRANSITION_TO_SPAWN_OUT_OF_APARTMENT().")
				#ENDIF
			ENDIF

			SET_HEIST_SKIP_FIRST_SKYSWOOP_UP_CUT(TRUE)
			CLEAN_HEIST_PRE_PLANNING_LITE(FALSE)
			SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()

#IF FEATURE_GEN9_STANDALONE
			HEIST_UDS_ACTIVITY_EXIT_PRE_PLANNING_BOARD()
#ENDIF // FEATURE_GEN9_STANDALONE
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_CONFIRM_EXIT - Quit confirmed, moving to cleanup.")
			#ENDIF
			
		ELSE
		
			SET_SKYFREEZE_CLEAR()
		
			IF AM_I_LEADER_OF_HEIST()	
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_UPDATE_ALL_THREAD_BUFFER)
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_CONFIRM_EXIT - Quit cancelled, moving back to update for leader.")
				#ENDIF
			ELSE
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_UPDATE_GUEST_THREAD_BUFFER)
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_CONFIRM_EXIT - Quit cancelled, moving back to update for guest.")
				#ENDIF
			ENDIF
			
		ENDIF

	ENDIF

ENDPROC


PROC HEIST_PRE_START_CUTSCENE()

	DRAW_HEIST_SCALEFORM()	
	INVALIDATE_IDLE_CAM()
	
	IF NOT IS_CORONA_BIT_SET(CORONA_PLAYER_FULLY_ON_INTRO_SCREEN)
		SUPPRESS_FRONTEND_RENDERING_THIS_FRAME()
	ENDIF

	IF NOT GET_HEIST_CORONA_ACTIVE()

		TEXT_LABEL_23 tlContentID = GET_CONTENTID_FROM_ROOT_CONTENTID(GET_HASH_KEY(g_HeistPrePlanningClient.tlHeistFinaleRootContID))

		IF Setup_My_Heist_Corona_From_ContentID(tlContentID, TRUE, FALSE, TRUE)
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_CUTSCENE - Corona for Mid Strand cutscene triggered")
			 
			// Do i want to move to HEIST_PRE_FLOW_UPDATE_APT_THREAD_BUFFER?
			VECTOR vHeistCoronaLocation
			GET_PLAYER_PROPERTY_HEIST_LOCATION(vHeistCoronaLocation, MP_PROP_ELEMENT_HEIST_PLAN_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vHeistCoronaLocation)
			
			SET_HEIST_CORONA_ACTIVE(TRUE)
		ENDIF
		
	ENDIF

	// When switching from the phone, we only want to play the second half of the animation.
	IF g_HeistPrePlanningClient.bLaunchFromPhone
	
		IF NOT IS_SKYSWOOP_AT_GROUND()
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_CUTSCENE - Triggering launch of heist mid-point cutscene from PHONE.")
			#ENDIF
		
			g_enumActiveHeistAnim eHeistAnimSecond
			g_enumActiveHeistAnim eHeistAnimThird
						
			eHeistAnimSecond 	= GET_HEIST_INTO_ANIM_PRE(	GET_HASH_KEY(g_HeistPrePlanningClient.tlHeistFinaleRootContID), 
															(NOT IS_PLAYER_PED_FEMALE(INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex))),
															FALSE,
															TRUE,
															FALSE)
			
			eHeistAnimThird		= GET_HEIST_INTO_ANIM_DURING(GET_HASH_KEY(g_HeistPrePlanningClient.tlHeistFinaleRootContID), 
															(NOT IS_PLAYER_PED_FEMALE(INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex))),
															FALSE,
															TRUE,
															FALSE)

			IF NOT IS_ANIM_CONTROLLER_ALREADY_ACTIVE(g_HeistSharedClient.sHeistAnimSecond) 	OR NOT ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimSecond)
			OR NOT IS_ANIM_CONTROLLER_ALREADY_ACTIVE(g_HeistSharedClient.sHeistAnimThird) 	OR NOT ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimThird)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_CUTSCENE - WARNING! Attempted launch of mid-point without building controllers; doing last minuite build.")
				#ENDIF
				
				PRELOAD_HEIST_ANIM_ASSETS_2(g_HeistSharedClient.sHeistAnimSecond, eHeistAnimSecond, 1,
											g_HeistSharedClient.sHeistAnimThird, eHeistAnimThird, 2)
			ENDIF

			IF NOT HAS_HEIST_STRAND_ANIM_FINISHED(g_HeistSharedClient.sHeistAnimSecond)
				MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond, DEFAULT, DEFAULT, FALSE, FALSE, TRUE, TRUE)
			ENDIF
			
			EXIT
		
		// Make sure that the switch has finished before starting the intro anim to avoid cutting the start off.
		ELSE
		
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond, DEFAULT, DEFAULT, FALSE, DEFAULT, TRUE)
		
			IF g_HeistSharedClient.sHeistAnimSecond.bAtLastFrame
				//	---	B* 2145099 RowanJ	-	Stop MenuMGHeistIntro when corona comes on and change to MenuMGHeistTint
				IF ANIMPOSTFX_IS_RUNNING("MenuMGHeistIntro")
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_FINALE_LAUNCH_FINALE_FROM_FAKE - Stopping heist PostFX: MenuMGHeistIntro.")
					ANIMPOSTFX_STOP("MenuMGHeistIntro")
				ENDIF
				
				IF NOT ANIMPOSTFX_IS_RUNNING("MenuMGHeistIn")
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_FINALE_LAUNCH_FINALE_FROM_FAKE - Triggering heist PostFX: MenuMGHeistIn.")
					ANIMPOSTFX_PLAY("MenuMGHeistIn", 0, TRUE)
					TOGGLE_RENDERPHASES(FALSE, TRUE)
				ENDIF
			ENDIF
		
			IF NOT HAS_HEIST_STRAND_ANIM_FINISHED(g_HeistSharedClient.sHeistAnimSecond)
			OR g_HeistSharedClient.sHeistAnimSecond.bAtLastFrame	
				EXIT
			ENDIF
		ENDIF
		
	ELSE
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_CUTSCENE - Triggering launch of heist mid-point cutscene from ROOM.")
		#ENDIF
	
		IF NOT g_HeistPrePlanningClient.bWaitingForAnims
			IF NOT ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimFirst)
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_CUTSCENE - Triggering launch of heist mid-point cutscene from ROOM.")
				#ENDIF
				NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_CLEAR_TASKS | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION )
				g_HeistPrePlanningClient.bWaitingForAnims = TRUE
			ENDIF
		ENDIF
	
		IF NOT HAS_HEIST_STRAND_ANIM_FINISHED(g_HeistSharedClient.sHeistAnimFirst)
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimFirst, DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE)
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond, TRUE, DEFAULT, DEFAULT, DEFAULT, TRUE)
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimThird, TRUE, DEFAULT, DEFAULT, DEFAULT, TRUE)
			EXIT
		ELSE
			
			IF g_HeistSharedClient.sHeistAnimSecond.bAtLastFrame
				//	---	B* 2145099 RowanJ	-	Stop MenuMGHeistIntro when corona comes on and change to MenuMGHeistTint
				IF ANIMPOSTFX_IS_RUNNING("MenuMGHeistIntro")
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_CUTSCENE - Stopping heist PostFX: MenuMGHeistIntro.")
					ANIMPOSTFX_STOP("MenuMGHeistIntro")
				ENDIF
				
				IF NOT ANIMPOSTFX_IS_RUNNING("MenuMGHeistIn")
					PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_CUTSCENE - Triggering heist PostFX: MenuMGHeistIn.")
					ANIMPOSTFX_PLAY("MenuMGHeistIn", 0, TRUE)
					TOGGLE_RENDERPHASES(FALSE, TRUE)
				ENDIF
			ENDIF
		
			IF NOT g_HeistSharedClient.sHeistAnimFirst.bBasicCleanupFinished
				MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimFirst, FALSE, DEFAULT, DEFAULT, DEFAULT, TRUE)
			ENDIF
				
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimThird, TRUE, DEFAULT, DEFAULT, DEFAULT, TRUE)
			
			IF NOT HAS_HEIST_STRAND_ANIM_FINISHED(g_HeistSharedClient.sHeistAnimSecond)
			OR g_HeistSharedClient.sHeistAnimSecond.bAtLastFrame
				EXIT
			ENDIF
		ENDIF
		
	ENDIF

	g_HeistPrePlanningClient.bLaunchFromPhone = FALSE

	CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimFirst, FALSE)
	CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimSecond, FALSE)
	
	PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_CUTSCENE - Cutscene setup complete, moving to idle / cleanup.")
	
	g_HeistPrePlanningClient.bWaitingForAnims	= FALSE
	SET_HEIST_CORONA_ACTIVE(FALSE)
	
	MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond)
	MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimThird, DEFAULT, FALSE, DEFAULT, TRUE, TRUE)
	
	SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_IDLE_CUTSCENE_THREAD_BUFFER)

ENDPROC 


PROC HEIST_PRE_IDLE_CUTSCENE()

	DRAW_HEIST_SCALEFORM()	
	INVALIDATE_IDLE_CAM()
	
	IF NOT IS_CORONA_BIT_SET(CORONA_PLAYER_FULLY_ON_INTRO_SCREEN)
		SUPPRESS_FRONTEND_RENDERING_THIS_FRAME()
	ENDIF

	IF NOT ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimThird)
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_IDLE_CUTSCENE - ERROR! Attempted launch of idle anim without building anim controller; doing last minuite build.")
		#ENDIF
		
		g_enumActiveHeistAnim eHeistAnim
		
		IF (IS_PLAYER_PED_FEMALE(INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex)))
			eHeistAnim = HEIST_SELECTED_ANIM_WINE_IDLE
		ELSE
			eHeistAnim = HEIST_SELECTED_ANIM_WHISKY_IDLE
		ENDIF

		PRELOAD_HEIST_ANIM_ASSET_SINGLE(g_HeistSharedClient.sHeistAnimThird, eHeistAnim, FALSE)
	ENDIF

	MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimThird, DEFAULT, FALSE, DEFAULT, TRUE, TRUE)

ENDPROC 



/// PURPOSE:
///    Automatic heist pre-planning board cleanup state.
PROC HEIST_PRE_START_CLEAN_ALL()

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_CLEAN_ALL - Pre-planning progressed to clean-up automatically.")
	#ENDIF
	
//	IF NETWORK_IS_ACTIVITY_SESSION()
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_START_CLEAN_ALL - NETWORK_IS_ACTIVITY_SESSION = TRUE, pull up cam instead of doing one time cleanup.")
//		#ENDIF
//		SET_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
//		EXIT
//	ENDIF
	
	CLEAN_ALL_HEIST_PRE_PLANNING()

ENDPROC


PROC HEIST_PRE_SUPPRESS_RENDER()

	IF GET_HEIST_SUPPRESS_PLANNING_RENDER_ONLY()
	
		IF g_HeistSharedClient.PlanningBoardIndex != NULL
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_SUPPRESS_RENDER - BOARD, checking if scaleform is loaded. Index: ", NATIVE_TO_INT(g_HeistSharedClient.PlanningBoardIndex))
		
//			IF HAS_SCALEFORM_MOVIE_LOADED(g_HeistSharedClient.PlanningBoardIndex) // Removed for B*: 2300713
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_SUPPRESS_RENDER - UNLOADING PLANNING BOARD!")
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_HeistSharedClient.PlanningBoardIndex)
//			ENDIF
			g_HeistSharedClient.PlanningBoardIndex = NULL
		ENDIF

		IF g_HeistSharedClient.PlanningMapIndex != NULL
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_SUPPRESS_RENDER - MAP, checking if scaleform is loaded. Index: ", NATIVE_TO_INT(g_HeistSharedClient.PlanningMapIndex))
			
//			IF HAS_SCALEFORM_MOVIE_LOADED(g_HeistSharedClient.PlanningMapIndex)
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_SUPPRESS_RENDER - UNLOADING PLANNING MAP!")
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_HeistSharedClient.PlanningMapIndex)
//			ENDIF
			g_HeistSharedClient.PlanningMapIndex = NULL
		ENDIF
		
		IF g_HeistSharedClient.PlanningInstButtonsIndex != NULL
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_SUPPRESS_RENDER - INSTR BTNS, checking if scaleform is loaded. Index: ", NATIVE_TO_INT(g_HeistSharedClient.PlanningInstButtonsIndex))
				
//			IF HAS_SCALEFORM_MOVIE_LOADED(g_HeistSharedClient.PlanningInstButtonsIndex)
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_SUPPRESS_RENDER - UNLOADING INSTR BUTTONS!")
			SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(g_HeistSharedClient.PlanningInstButtonsIndex)
//			ENDIF
			g_HeistSharedClient.PlanningInstButtonsIndex = NULL
		ENDIF
		
		IF NOT GET_HEIST_SUPPRESS_HEIST_BOARD()
			SET_HEIST_SUPPRESS_HEIST_BOARD(TRUE)
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_SUPPRESS_RENDER - Setting bSupressPlanningBoard to TRUE.")
		ENDIF
	
	ELSE // Restore the heist assets and move back to original state.
	
		PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_SUPPRESS_RENDER - GET_HEIST_SUPPRESS_PLANNING_RENDER_ONLY = FALSE, re-loading heist assets.")
	
		IF GET_HEIST_SUPPRESS_HEIST_BOARD()
			SET_HEIST_SUPPRESS_HEIST_BOARD(FALSE)
			PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_PRE_SUPPRESS_RENDER - Setting bSupressPlanningBoard to FALSE.")
		ENDIF
	
		IF LOAD_HEIST_PRE_PLANNING_SCALEFORM()
			SET_HEIST_PREPLAN_STATE(g_HeistPrePlanningClient.eHeistCachedFlowState)
		ENDIF
	
	ENDIF

ENDPROC

/// PURPOSE:
///    Manage the general flow/state of the heist pre-planning board. 
PROC MANAGE_HEIST_PRE_PLANNING_STATE(SCALEFORM_INSTRUCTIONAL_BUTTONS &scaleformStruct, BOOL bShowPlanningImages, SCRIPT_TIMER & sStuckTimer, BOOL bForceInactiveBoard)
	
	SWITCH GET_HEIST_PREPLAN_STATE()
	
		CASE HEIST_PRE_FLOW_START_THREAD_BUFFER
		CASE HEIST_PRE_FLOW_START
		
			IF NOT GET_FM_UGC_INITIAL_HAS_FINISHED()
				PRINTLN("[AMEC][HEIST_PREPLAN] - MANAGE_HEIST_PRE_PLANNING_STATE - Waiting for FM_UGC_INITIAL_HAS_FINISHED...")
				EXIT
			ENDIF
			
			IF IS_MP_HEIST_STRAND_ACTIVE()
			AND Get_Current_Heist_Control_Stage() = HEIST_CLIENT_STAGE_NO_HEIST
			AND g_bFixFor7806381Active
				PRINTLN("[AMEC][HEIST_PREPLAN] - Waiting for Get_Current_Heist_Control_Stage() > HEIST_CLIENT_STAGE_NO_HEIST")
				EXIT				
			ENDIF
			
			IF g_bHoldBoardProgressionForBg
				PRINTLN("[AMEC][HEIST_PREPLAN] - Waiting for g_bHoldBoardProgressionForBg to clear")
				EXIT
			ENDIF
			
			IF GET_HEIST_PREPLAN_STATE() = HEIST_PRE_FLOW_START_THREAD_BUFFER
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_START)
			ENDIF 
			
			HEIST_PRE_START_MONITOR_TRIGGER(bForceInactiveBoard, scaleformStruct)
			
			BREAK
	
		CASE HEIST_PRE_FLOW_INIT_APT_THREAD_BUFFER
		CASE HEIST_PRE_FLOW_INIT_APT
		
			IF GET_HEIST_PREPLAN_STATE() = HEIST_PRE_FLOW_INIT_APT_THREAD_BUFFER
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_INIT_APT)
			ENDIF 
		
			HEIST_PRE_START_INIT_APT(bShowPlanningImages)
			
			BREAK
			
		CASE HEIST_PRE_FLOW_UPDATE_APT_THREAD_BUFFER
		CASE HEIST_PRE_FLOW_UPDATE_APT
		
			IF GET_HEIST_PREPLAN_STATE() = HEIST_PRE_FLOW_UPDATE_APT_THREAD_BUFFER			
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_UPDATE_APT)
			ENDIF 
		
			HEIST_PRE_START_UPDATE_BOARD_BACKGROUND()
			
			BREAK
			
		CASE HEIST_PRE_FLOW_INIT_PLANNING_MODE_THREAD_BUFFER
		CASE HEIST_PRE_FLOW_INIT_PLANNING_MODE
		
			IF GET_HEIST_PREPLAN_STATE() = HEIST_PRE_FLOW_INIT_PLANNING_MODE_THREAD_BUFFER
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_INIT_PLANNING_MODE)
			ENDIF
		
			HEIST_PRE_START_INIT_PLANNING_MODE()
			
			BREAK
			
		CASE HEIST_PRE_FLOW_INIT_GUEST_MODE_THREAD_BUFFER
		CASE HEIST_PRE_FLOW_INIT_GUEST_MODE
		
			IF GET_HEIST_PREPLAN_STATE() = HEIST_PRE_FLOW_INIT_GUEST_MODE_THREAD_BUFFER
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_INIT_GUEST_MODE)
			ENDIF 
		
			HEIST_PRE_START_INIT_GUEST_IDLE_MODE()
			
			BREAK
			
		CASE HEIST_PRE_FLOW_UPDATE_ALL_THREAD_BUFFER
		CASE HEIST_PRE_FLOW_UPDATE_ALL
		
			IF GET_HEIST_PREPLAN_STATE() = HEIST_PRE_FLOW_UPDATE_ALL_THREAD_BUFFER
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_UPDATE_ALL)
			ENDIF 
		
			HEIST_PRE_START_UPDATE_BOARD_ALL(scaleformStruct)
			
			BREAK
			
		CASE HEIST_PRE_FLOW_UPDATE_GUEST_THREAD_BUFFER
		CASE HEIST_PRE_FLOW_UPDATE_GUEST
		
			IF GET_HEIST_PREPLAN_STATE() = HEIST_PRE_FLOW_UPDATE_GUEST_THREAD_BUFFER
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_UPDATE_GUEST)
			ENDIF
		
			HEIST_PRE_START_UPDATE_BOARD_GUEST_IDLE(scaleformStruct, sStuckTimer)
			
			BREAK
			
		CASE HEIST_PRE_FLOW_EXIT_PENDING_THREAD_BUFFER
		CASE HEIST_PRE_FLOW_EXIT_PENDING
		
			IF GET_HEIST_PREPLAN_STATE() = HEIST_PRE_FLOW_EXIT_PENDING_THREAD_BUFFER
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_EXIT_PENDING)
			ENDIF
			
			// Do nothing here besides keep drawing the heist planning scaleform. This state is used in FMMC_Launcher 
			// as the functions required are only in that script.
			
			IF GET_HEIST_NONRESET_AUTO_CONFIGURE()
				SET_HEIST_NONRESET_AUTO_CONFIGURE(FALSE)
			ENDIF
			
			DRAW_HEIST_SCALEFORM()	
		
			BREAK
			
		CASE HEIST_PRE_FLOW_EXIT_CONFIRM_THREAD_BUFFER
		CASE HEIST_PRE_FLOW_EXIT_CONFIRM
		
			IF GET_HEIST_PREPLAN_STATE() = HEIST_PRE_FLOW_EXIT_CONFIRM_THREAD_BUFFER
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_EXIT_CONFIRM)
			ENDIF
			
			HEIST_PRE_START_CONFIRM_EXIT(scaleformStruct)
		
			BREAK
		
		CASE HEIST_PRE_FLOW_LAUNCH_CUTSCENE_THREAD_BUFFER
		CASE HEIST_PRE_FLOW_LAUNCH_CUTSCENE
		
			IF GET_HEIST_PREPLAN_STATE() = HEIST_PRE_FLOW_LAUNCH_CUTSCENE_THREAD_BUFFER
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_LAUNCH_CUTSCENE)
			ENDIF
			
			HEIST_PRE_START_CUTSCENE()
		
			BREAK
			
		CASE HEIST_PRE_FLOW_IDLE_CUTSCENE_THREAD_BUFFER
		CASE HEIST_PRE_FLOW_IDLE_CUTSCENE
		
			IF GET_HEIST_PREPLAN_STATE() = HEIST_PRE_FLOW_IDLE_CUTSCENE_THREAD_BUFFER
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_IDLE_CUTSCENE)
			ENDIF
			
			HEIST_PRE_IDLE_CUTSCENE()
		
			BREAK
			
		CASE HEIST_PRE_FLOW_CLEANUP_THREAD_BUFFER
		CASE HEIST_PRE_FLOW_CLEANUP
		
			IF GET_HEIST_PREPLAN_STATE() = HEIST_PRE_FLOW_CLEANUP_THREAD_BUFFER
				SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_CLEANUP)
			ENDIF 
		
			HEIST_PRE_START_CLEAN_ALL()
			
			IF g_TransitionSessionNonResetVars.bXThreadHeistAssetCleanup
			
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - MANAGE_HEIST_PRE_PLANNING_STATE - bXThreadHeistAssetCleanup = TRUE, cleaning shared heist assets.")
				#ENDIF
			
				CLEAN_HEIST_SHARED_DATA()
				g_TransitionSessionNonResetVars.bXThreadHeistAssetCleanup = FALSE
			
			ENDIF
			
			#IF IS_DEBUG_BUILD
			IF DOES_WIDGET_GROUP_EXIST(DEBUG_PrePlanWidgets)
				PRINTLN("[AMEC][HEIST_PREPLAN] - MANAGE_HEIST_PRE_PLANNING_STATE - WG: DEBUG_PrePlanWidgets EXISTS. Removing group.")
				DELETE_WIDGET_GROUP(DEBUG_PrePlanWidgets)
			ENDIF
			#ENDIF
			
			BREAK
			
		CASE HEIST_PRE_FLOW_SUPPRESS_RENDER
			
			HEIST_PRE_SUPPRESS_RENDER()
		
			BREAK
			
		DEFAULT
		
			// Now that we know the player is in AN apartment, we need to make sure it's the correct one.
			IF NOT IS_LOCAL_PLAYER_NEAR_HEIST_PLANNING_BOARD(ci_HEIST_BOARD_LOAD_IN_RANGE)
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_PREPLAN] - MANAGE_HEIST_PRE_PLANNING_STATE - WARNING! PLAYER LOCATION NOT IN RANGE.")
				#ENDIF
				
				IF NOT IS_PLAYER_SCTV(PLAYER_ID())
				AND GET_HEIST_JIP_TO_PLANNING_BOARD()
				AND GET_HEIST_PROPERTY_CAM_STAGE() = HEIST_PROPERTY_HOLD_STAGE
				AND GET_HEIST_NONRESET_AUTO_CONFIGURE()
				AND NETWORK_IS_ACTIVITY_SESSION()
					
					PRINTLN("[AMEC][HEIST_PREPLAN] - MANAGE_HEIST_PRE_PLANNING_STATE - ERROR! Doing activity heist board JIP, but not in range of planning board. Moving to kick from session.")
					
					IF TRANSITION_SESSION_CAM_PULL_DOWN_WHEN_SPECTATING_READY()
						CLEAR_TRANSITION_SESSION_CAM_PULL_DOWN_WHEN_SPECTATING_READY()
					ENDIF
					
					SET_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
					SET_IS_HEIST_ACTIVITY_SESSION_EXIT_IN_PROGRESS()
					CLEAR_TRANSITION_SESSION_LAUNCHING()
					CLEAR_TRANSITION_SESSION_RESTARTING()
					SETUP_TRANSITION_TO_SPAWN_OUT_OF_APARTMENT()
					TOGGLE_RENDERPHASES(FALSE)
					SET_HEIST_SKIP_FIRST_SKYSWOOP_UP_CUT(TRUE)
					SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
					PRINT_HELP("HEIST_ER_APT", 5000)
					
				ENDIF
				
				EXIT
			ENDIF
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_PREPLAN] - MANAGE_HEIST_PRE_PLANNING_STATE - Heist pre-planning state was invalid, reverting to INIT.")
			#ENDIF
			SET_HEIST_PREPLAN_STATE(HEIST_PRE_FLOW_START_THREAD_BUFFER)
			CLEAN_HEIST_NAME_DOWNLOAD_STATES() // If heist prep board ticks over, make sure to clean the name stuff. B*: 2232758
			
			BREAK
	ENDSWITCH
	
ENDPROC


// PURPOSE:	Maintain the Heist Pre-Planning Cutscene if required
PROC Maintain_Heist_PrePlanning_Cutscene(BOOL paramIsMidStrandCutscene, BOOL paramIsTutorialCutscene)

	IF paramIsMidStrandCutscene
	AND paramIsTutorialCutscene
		PRINTLN(".KGM [HEIST][CUTSCENE] CRITICAL ERROR! paramIsMidStrandCutscene AND paramIsTutorialCutscene are both TRUE, cannot select a single cutscene to play, exiting...")
		SCRIPT_ASSERT(".KGM [HEIST][CUTSCENE] CRITICAL ERROR! paramIsMidStrandCutscene AND paramIsTutorialCutscene are both TRUE, cannot select a single cutscene to play, exiting...")
		EXIT
	ENDIF

	// Anything to do?
	IF NOT (g_heistPrePlanningMocap.playMocap)
		EXIT
	ENDIF
	
	// If the player is in their own apartment to play this mocap then they are the Heist Leader
	BOOL isHeistLeader = AM_I_LEADER_OF_HEIST()
	
	IF NOT isHeistLeader
		IF g_HeistSharedClient.iCurrentPropertyOwnerIndex = NATIVE_TO_INT(INVALID_PLAYER_INDEX())
			PRINTLN(".KGM [HEIST][CUTSCENE] iCurrentPropertyOwnerIndex = INVALID_PLAYER_INDEX, waiting for valid data.")
			EXIT
		ENDIF
	ENDIF
	
	// NOTE: Assumes only the Ornate Bank Heist pre-planning cutscene will get into this function
	PRINTLN(".KGM [HEIST][CUTSCENE] Heist PRE-PLANNING Cutscene. RCID = ", g_heistPrePlanningMocap.finaleRCID, "  [Leader? ", isHeistLeader, "] [Mid-Strand? ", PICK_STRING(paramIsMidStrandCutscene, "TRUE", "FALSE"), "] [Tutorial? ", PICK_STRING(paramIsTutorialCutscene, "TRUE", "FALSE"), "]")
	
	INT				hashFinaleRCID		= GET_HASH_KEY(g_heistPrePlanningMocap.finaleRCID)
	BOOL			bIsPrePlanningCut	= !paramIsMidStrandCutscene
	TEXT_LABEL_23	heistCutsceneName	= TEMP_Get_Mocap_Name_From_FinaleRCID_Hash(hashFinaleRCID, bIsPrePlanningCut, FALSE, FALSE, paramIsMidStrandCutscene, paramIsTutorialCutscene)
	INT 			iCutsceneSlots 		= TEMP_Get_Mocap_Participant_Number_From_FinaleRCID_Hash(hashFinaleRCID)
	BOOL			allowHeistCutscene	= TRUE
	
	IF (IS_STRING_NULL_OR_EMPTY(heistCutsceneName))
		PRINTLN(".KGM [HEIST][CUTSCENE] Heist PrePlanning Cutscene Name not found - Ignoring")
		allowHeistCutscene = FALSE
	ENDIF
	
	IF (allowHeistCutscene)
	
		// If it is the pre planning intro cutscene then set the new heist strand
		IF bIsPrePlanningCut
			// AMEC TEMP (27/06/14): A new state needs to be added specifically to activating the strand when safe.
			IF GET_HEIST_ENTER_CUTS_STATE() = HMS_STAGE_INACTIVE
			
				// Save the leader's handle so that we can make sure the leader is present throughout the transition.
				g_TransitionSessionNonResetVars.ghHeistLeader = GET_GAMER_HANDLE_PLAYER(INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex))
			
				// Setup the Heist Strand for the Leader
				IF isHeistLeader
					IF NOT IS_MP_HEIST_STRAND_ACTIVE()
						// KGM 11/10/14: At this point the Cash Reward Band is still stored in the pre-launch download variables
						IF NOT private_SETUP_DATA_FOR_NEW_HEIST_PLANNING_STRAND(g_heistPrePlanningMocap.finaleRCID, CALCULATE_HEIST_STRAND_ENTRY_FEE(g_sPreHeistDownloadData.phddReward, hashFinaleRCID))
							EXIT
						ELSE
							PRINTLN(".KGM [HEIST][CUTSCENE] HMS_STAGE_INACTIVE - Setting up the Heist Strand for this Heist Leader (this player is in their own property).")
						ENDIF
					ELSE
						PRINTLN(".KGM [HEIST][CUTSCENE] HMS_STAGE_INACTIVE - ERROR! Attempted to set up new heist strand when IS_MP_HEIST_STRAND_ACTIVE() returned TRUE!")
					ENDIF
				ELSE
					PRINTLN(".KGM [HEIST][CUTSCENE] HMS_STAGE_INACTIVE - This player is not in their own property, so not setting up the Heist strand.")
				ENDIF
			ENDIF
		ENDIF
	
		// If the blur effects are running then stop them
		// KGM NOTE: We may be able to move this into the 'start cutscene' stage of the Heist Cutscene maintenance routine.
		//			Currently including "FMMC_Corona_Controller.sch" in there causes problems.
		// AMEC: this needs to be moved into the correct state in the cutscene FSM soon.
		IF GET_HEIST_ENTER_CUTS_SUB_STATE() = HMS_SUB_STAGE_WAIT AND IS_HEIST_ANIM_PLAYING(g_HeistSharedClient.sHeistAnimFirst)
		OR GET_HEIST_ENTER_CUTS_STATE() = HMS_STAGE_CUTSCENE_JUST_STARTED AND (IS_THIS_CUTSCENE_PRISON_BESPOKE(heistCutsceneName) OR g_replayHeistIsFromBoard)
		OR g_heistMocap.hasSafetyTimedOut
		OR g_heistMocap.bForceScreenCleanup
			IF g_heistMocap.bHaveCleanedScreen = FALSE
				PRINTLN(".KGM [HEIST][CUTSCENE] - HMS_SUB_STAGE_CREATE_CAMERA - Cleaning up Screen Effects before playing Heist Cutscene")
				SET_FRONTEND_ACTIVE(FALSE)
				CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam, g_FMMC_STRUCT.iMissionType, TRUE, FALSE, FALSE)			
				CORONA_CLEAN_UP_SCREEN_STATE(FALSE)	
//				ANIMPOSTFX_PLAY(GET_CORONA_FX_OUT(TRUE), 0, TRUE) // Removed as per 2094482.
				
				IF HEIST_IS_NETWORK_VOICE_ACTIVE()
					HEIST_SET_NETWORK_VOICE_ACTIVE(FALSE)
				ENDIF
	
				g_heistMocap.bHaveCleanedScreen = TRUE
			ENDIF
		ENDIF
		
		BOOL allowSkip = FALSE
		
		// Allow skip if Heist Leader AND Heist has been completed at least once as Leader (skip not available for tutorial).
		allowSkip = isHeistLeader
		IF (allowSkip)
			allowSkip = (GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(hashFinaleRCID, TRUE) > 0)
			
// KGM 6/2/14 [BUG 2222707]: Removed this which stopped the tutorial cutscene from being skipped. It was added when the new tutorial cutscene stuff was initially added
//	while making sure cross-session and JIP works, probably to avoid an issue. That issue probably doesn't exist anymore - the cutscene skipping routgines have been tidied.
//			IF (allowSkip)
//				IF (paramIsTutorialCutscene)
//					allowSkip = FALSE
//				ENDIF
//			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF (GET_COMMANDLINE_PARAM_EXISTS("sc_HeistCutSkippable"))
					allowSkip = TRUE
				ENDIF
			#ENDIF
		ENDIF
		
		// Pre-Planning Cutscenes shouldn't fade at the end
		BOOL isFinale = FALSE

		IF NOT (Maintain_Net_Heist_Cutscenes(heistCutsceneName, allowSkip, isFinale, paramIsTutorialCutscene, iCutsceneSlots))
			EXIT
		ENDIF
		
		PRINTLN(".KGM [HEIST][CUTSCENE] Heist Pre-Planning Cutscene COMPLETE")
	ENDIF
	
	

	// Clear out the control variables (these get set in CONTROL_FM_PRE_PLANNING_HEIST_CUTSCENE())
	// NOTE: ...don't clear the g_heistPrePlanningMocap.mocapRequested variable, this will be cleared when FMMC_Launcher told mocap finished
	g_heistPrePlanningMocap.playMocap	= FALSE
	g_heistPrePlanningMocap.finaleRCID	= ""
	g_heistPrePlanningMocap.bCutscenePropsInit = FALSE

	IF NOT (allowHeistCutscene)
		PRINTLN(".KGM [HEIST][CUTSCENE] Heist Cutscenes NOT ALLOWED - quitting PRE-PLANNING Cutscene")
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Returns TRUE when the pre planning heist cutscene has finished.
FUNC BOOL CONTROL_FM_PRE_PLANNING_HEIST_CUTSCENE(INT paramHashFinaleRCID, BOOL &bCompleted, BOOL bMidStrandCutscene, BOOL bTutorialCutscene = FALSE)
	
	IF bTutorialCutscene
	AND bMidStrandCutscene
		PRINTLN(".KGM [Heist][Cutscene] CONTROL_FM_PRE_PLANNING_HEIST_CUTSCENE() - CRITICAL ERROR! bTutorialCutscene AND bMidStrandCutscene are TRUE, cannot progress to any cutscene!")
		SCRIPT_ASSERT(".KGM [Heist][Cutscene] CONTROL_FM_PRE_PLANNING_HEIST_CUTSCENE() - CRITICAL ERROR! bTutorialCutscene AND bMidStrandCutscene are TRUE, cannot progress to any cutscene! Tell Alastair.")
		RETURN TRUE
	ENDIF
	
	// Set our flag to FALSe
	bCompleted = FALSE
	
	// KGM 25/9/14: For the Tutorial Cutscene, the passed in Hash WON'T be for a Finale but instead will be for the first Prep Mission of the Tutorial
	//				To get the correct cutscene, we need to swap this for the Finale RCID
	INT hashRCIDToSearchFor = paramHashFinaleRCID
	IF (bTutorialCutscene)
		hashRCIDToSearchFor = g_sMPTUNABLES.iroot_id_HASH_The_Flecca_Job
	ENDIF
	
	TEXT_LABEL_23 finaleRCID = Get_Finale_RootContentID_From_Hash(hashRCIDToSearchFor)
	IF (IS_STRING_NULL_OR_EMPTY(finaleRCID))
		PRINTLN(".KGM [Heist][Cutscene] CONTROL_FM_PRE_PLANNING_HEIST_CUTSCENE() - ERROR: Failed to find RootContentID from Hash ", hashRCIDToSearchFor)
		
		// Treat cutscene as played
		RETURN TRUE
	ENDIF
	
	// Make sure we have generated the basic props for this cutscene
	IF NOT (g_heistPrePlanningMocap.bCutscenePropsInit)
		IF NOT SETUP_HEIST_ROOM_PROPS(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty, DEFAULT, hashRCIDToSearchFor)
			
			PRINTLN(".KGM [Heist][Cutscene] CONTROL_FM_PRE_PLANNING_HEIST_CUTSCENE() - Loading and Creating heist room props")
			
			RETURN FALSE
		ELSE
			g_heistPrePlanningMocap.bCutscenePropsInit = TRUE
			PRINTLN(".KGM [Heist][Cutscene] CONTROL_FM_PRE_PLANNING_HEIST_CUTSCENE() - All props generated for cutscene")
		ENDIF
	ENDIF
	
	IF NOT (g_heistPrePlanningMocap.mocapRequested)
		PRINTLN(".KGM [Heist][Cutscene] Setting up a new Pre-Planning Intro Mocap for Heist Finale strand: ", hashRCIDToSearchFor)
		
		g_heistPrePlanningMocap.mocapRequested	= TRUE
		g_heistPrePlanningMocap.playMocap		= TRUE
		g_heistPrePlanningMocap.finaleRCID		= finaleRCID
		
		RETURN FALSE
	ENDIF
	
	IF NOT IS_CORONA_BIT_SET(CORONA_HEIST_SUSPEND_MIN_PLAYER_CHECK)
		PRINTLN(".KGM [Heist][Cutscene] Setting corona bit(CORONA_HEIST_SUSPEND_MIN_PLAYER_CHECK)")
		SET_CORONA_BIT(CORONA_HEIST_SUSPEND_MIN_PLAYER_CHECK)
	ENDIF
	
	// AMEC - Moved from main pre-planning maintain as pre-planning is no longer always running.
	Maintain_Heist_PrePlanning_Cutscene(bMidStrandCutscene, bTutorialCutscene)
	
	IF (g_heistPrePlanningMocap.playMocap)
		// ...not finished
		RETURN FALSE
	ENDIF
	
	// Mocap Finished, so clear out the variables
	g_heistPrePlanningMocap.mocapRequested		= FALSE
	g_heistPrePlanningMocap.playMocap			= FALSE
	g_heistPrePlanningMocap.bCutscenePropsInit	= FALSE
	g_heistPrePlanningMocap.finaleRCID			= ""
	
	// Set our passed bool to indicate we have seen the complete cutscene
	bCompleted = TRUE
	
	PRINTLN(".KGM [Heist][Cutscene] FMMC_Launcher told that Pre-Planning Into Mocap has finished for Heist Finale strand: ", hashRCIDToSearchFor)
		
	RETURN TRUE
	
ENDFUNC


/// PURPOSE:
///    Call every frame to keep the heist pre-planning board alive.
PROC CONTROL_FM_HEIST_PRE_PLANNING_BOARD(	SCALEFORM_INSTRUCTIONAL_BUTTONS &scaleformStruct, 
											SCRIPT_TIMER& sStuckTimer 
											#IF IS_DEBUG_BUILD , WIDGET_GROUP_ID wgParentWidgetGroup #ENDIF , 
											BOOL bShowPlanningImages = TRUE,
											BOOL bForceInactiveBoard = FALSE)
	
	#IF IS_DEBUG_BUILD
		DEBUG_PlanningParentWidgets = wgParentWidgetGroup
	#ENDIF
	
	// Make sure that if we're in an apartment after a strand into to make sure that it's the leader's apartment.
	IF g_TransitionSessionNonResetVars.bStraightFromHeistCutscene
		IF NOT NETWORK_IS_GAMER_IN_MY_SESSION(g_TransitionSessionNonResetVars.ghHeistLeader)
			PRINTLN("[AMEC][HEIST_PREPLAN] - CONTROL_FM_HEIST_PRE_PLANNING_BOARD - ERROR! Heist leader is not in my session after strand intro cutscene! Leader: ", NETWORK_MEMBER_ID_FROM_GAMER_HANDLE(g_TransitionSessionNonResetVars.ghHeistLeader))
			CLEAR_HEIST_PRE_PLANNING_DETAILS_AFTER_FAILED_TRANSITION()
			EXIT
		ENDIF
	ENDIF
	
	// Maintain any cleanup following a toggle renderphase (do this first in the frame so we handle anything in future functions)
	PROCESS_HEIST_RENDER_PHASE_STATES()
	
	BOOL bExitFlow
	BOOL bCleanupSharedData
	
	IF NOT SHOULD_HEIST_PREPLAN_BOARD_BE_ACTIVE(bForceInactiveBoard, bExitFlow)
		IF GET_HEIST_PREPLAN_STATE() != HEIST_PRE_FLOW_NONE
			PRINTLN("[AMEC][HEIST_PREPLAN] - CONTROL_FM_HEIST_PRE_PLANNING_BOARD - Force cleaning heist pre-planning board.")
			CLEAN_ALL_HEIST_PRE_PLANNING()
			
			bCleanupSharedData = TRUE
		ENDIF
		
		IF GET_HEIST_STRAND_STATE() != HEIST_STRAND_FLOW_NONE
			PRINTLN("[AMEC][HEIST_PREPLAN] - CONTROL_FM_HEIST_PRE_PLANNING_BOARD - Force cleaning heist strand board.")
			CLEAN_ALL_HEIST_STRAND_PLANNING()
			
			bCleanupSharedData = TRUE	
		ENDIF
		
		IF bCleanupSharedData
			PRINTLN("[AMEC][HEIST_PREPLAN] - CONTROL_FM_HEIST_PRE_PLANNING_BOARD - Cleaning up the shared heist data.")
			CLEAN_HEIST_SHARED_DATA(TRUE, FALSE) // url:bugstar:2061912 - Adding flag prevents the Heist leader's building from being partially transparent when joining corona. ST
		ENDIF
				
		IF bExitFlow
			EXIT
		ENDIF
	ELSE
		IF IS_CURRENT_GEN_HEIST()
		AND NETWORK_IS_ACTIVITY_SESSION()
		
			VECTOR vHeistBoardPos
			GET_PLAYER_PROPERTY_HEIST_LOCATION(vHeistBoardPos, MP_PROP_ELEMENT_HEIST_BOARD_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		
			IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vHeistBoardPos) < ci_HEIST_PLANNING_RANDER_RANGE)
			
				// We have run out of memory on CG, so we need to try and cut out the heist planning board as much as possible.
				// 35m should be enough so that the board is active is most (if not all) of the upstair interior, but disabled
				// when the player is in the garage. This is only a problem when on missions.
				
				IF NOT GET_HEIST_SUPPRESS_PLANNING_RENDER_ONLY()
					SET_HEIST_SUPPRESS_PLANNING_RENDER_ONLY(TRUE)
				ENDIF
				
			ENDIF
		
		ENDIF
	ENDIF
	
	IF AM_I_LEADER_OF_HEIST(TRUE)
		IF GET_HEIST_START_SENDING_MEMBER_DATA()
			PRINTLN("[AMEC][HEIST_PREPLAN] - CONTROL_FM_HEIST_PRE_PLANNING_BOARD - GET_HEIST_START_SENDING_MEMBER_DATA() = TRUE, maintain member data transfer.")
		 	MAINTAIN_HEIST_MEMBER_DATA_TRANSFER_PROTOCOL()
		ENDIF
	ELSE
		MAINTAIN_REQUEST_FOR_HEIST_MEMBER_DATA()
	ENDIF
	
	DOWNLOAD_HEIST_PARTICIPANT_DATA(AM_I_LEADER_OF_HEIST(TRUE), g_HeistSharedClient.iMemberDataLoop, g_HeistSharedClient.iMemberDataInnerLoop)
	
	// Intercept the heist logic and prevent new mission header data getting downloaded in specific situations.
	IF NETWORK_IS_ACTIVITY_SESSION()
		IF NOT GET_HEIST_AM_I_HEIST_LEADER_GLOBAL()
			IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
			OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())
				bForceInactiveBoard = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// Hides the hud when needed
	PROCESS_HEIST_PLANNING_ROOM_HUD()
	
	MANAGE_HEIST_PRE_PLANNING_STATE(scaleformStruct, bShowPlanningImages, sStuckTimer, bForceInactiveBoard)
	
	#IF IS_DEBUG_BUILD
		DEBUG_DRAW_PREPLAN_DATA()
	#ENDIF

ENDPROC




// End of file.
