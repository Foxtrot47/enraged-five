
USING "globals.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"
USING "commands_event.sch"
USING "net_wait_zero.sch"

#IF FEATURE_CASINO
//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ ENUM ╞═══════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

ENUM VEHICLE_REWARD_STAGE
	VEH_REWARD_IDLE_STAGE,
	VEH_REWARD_SPAWN_VEHICLE_STAGE,
	VEH_REWARD_INIT_STAGE,
	VEH_REWARD_WAIT_FOR_PLAYER_ENTER_STAGE,
	VEH_REWARD_CHANGE_TO_PV_STAGE,
	VEH_REWARD_CLEANUP_STAGE
ENDENUM

ENUM VEHICLE_REWARD
	VEHICLE_REWARD_INVALID = -1
	,VEHICLE_REWARD_CASINO		
	
	#IF FEATURE_HEIST_ISLAND
	,VEHICLE_REWARD_KEINEMUSIK // Awarded for completing all 4 request missions for Keinemusik
	#ENDIF
ENDENUM

#IF IS_DEBUG_BUILD
DEBUGONLY FUNC STRING DEBUG_GET_VEHICLE_REWARD_AS_STRING(VEHICLE_REWARD eEnum)
	SWITCH eEnum
		CASE VEHICLE_REWARD_INVALID		RETURN	"VEHICLE_REWARD_INVALID"
		CASE VEHICLE_REWARD_CASINO		RETURN	"VEHICLE_REWARD_CASINO"
		#IF FEATURE_HEIST_ISLAND
		CASE VEHICLE_REWARD_KEINEMUSIK	RETURN	"VEHICLE_REWARD_KEINEMUSIK"
		#ENDIF
	ENDSWITCH

	ASSERTLN("DEBUG_GET_VEHICLE_REWARD_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC
#ENDIF

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════╡ CONSTS ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛
CONST_INT VEHICLE_REWARD_LOCAL_BS_RESERVE_VEHICLE							0
CONST_INT VEHICLE_REWARD_LOCAL_BS_REWARD_VEHICLE_CHANGED_TO_PV				1
CONST_INT VEHICLE_REWARD_LOCAL_BS_PICK_RANDOM_SPAWN_POINT					2


//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ STRUCTS ╞════════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

STRUCT VEHICLE_REWARD_DATA
	INT iLocalBS, iSpawnIndex
	INT iMP_ReplaceGarageVehSlot
	INT iMP_ReplaceGarageDisplaySlot
	INT iTransactionResult, iResultSlot, iDisplaySlot, iControlState
	INT iStaggerPlayer
	BOOL bSetAsMissionEntity
	
	VEHICLE_REWARD eReward
	MODEL_NAMES rewardVehModel
	VEHICLE_INDEX remoteRewardVeh[NUM_NETWORK_PLAYERS]
	
	BLIP_INDEX rewardVehBlip
	
	VEHICLE_REWARD_STAGE vehRewardStage
ENDSTRUCT

STRUCT MP_TRACKER_APPLY_STRUCT
	BOOL bDoNoSaveWarning
	INT iPVReturnState
	FE_WARNING_FLAGS WarningBS
	BOOL bAcceptPressed
	BOOL bPurchased
	BOOL bOverwritten
	BOOL bReplace
ENDSTRUCT

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ FUNCTION HELPER ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING GET_VEHICLE_REWARD_STAGE_NAME(VEHICLE_REWARD_DATA &vehRewardData)
	SWITCH vehRewardData.vehRewardStage
		CASE VEH_REWARD_IDLE_STAGE						RETURN "VEH_REWARD_IDLE_STAGE"					BREAK
		CASE VEH_REWARD_INIT_STAGE						RETURN "VEH_REWARD_INIT_STAGE"					BREAK
		CASE VEH_REWARD_SPAWN_VEHICLE_STAGE				RETURN "VEH_REWARD_SPAWN_VEHICLE_STAGE"			BREAK
		CASE VEH_REWARD_WAIT_FOR_PLAYER_ENTER_STAGE		RETURN "VEH_REWARD_WAIT_FOR_PLAYER_ENTER_STAGE"	BREAK
		CASE VEH_REWARD_CHANGE_TO_PV_STAGE				RETURN "VEH_REWARD_CHANGE_TO_PV_STAGE"			BREAK
		CASE VEH_REWARD_CLEANUP_STAGE					RETURN "VEH_REWARD_CLEANUP_STAGE"				BREAK
	ENDSWITCH
	RETURN "INVALID_STAGE"
ENDFUNC
#ENDIF

FUNC VEHICLE_REWARD_STAGE GET_VEHICLE_REWARD_STAGE(VEHICLE_REWARD_DATA &vehRewardData)
	RETURN vehRewardData.vehRewardStage
ENDFUNC 

PROC SET_VEHICLE_REWARD_STAGE(VEHICLE_REWARD_DATA &vehRewardData, VEHICLE_REWARD_STAGE stage)
	IF GET_VEHICLE_REWARD_STAGE(vehRewardData) != stage
		vehRewardData.vehRewardStage = stage
		PRINTLN("[AM_MP_VEH_REWARD] SET_VEHICLE_REWARD_STAGE ", GET_VEHICLE_REWARD_STAGE_NAME(vehRewardData))
	ENDIF	
ENDPROC

FUNC BOOL IS_SAFE_TO_DISPLAY_HELP_AND_BLIP()
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
	OR IS_HELP_MESSAGE_ON_SCREEN()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
		PRINTLN("[AM_MP_VEH_REWARD] - IS_SAFE_TO_DISPLAY_HELP_AND_BLIP - false: player is critical to FM event.")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_DELIVERING_BOUNTY()
		PRINTLN("[AM_MP_VEH_REWARD] - IS_SAFE_TO_DISPLAY_HELP_AND_BLIP - false: IS_LOCAL_PLAYER_DELIVERING_BOUNTY")
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC
#ENDIF



