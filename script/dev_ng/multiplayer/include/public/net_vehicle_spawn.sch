USING "globals.sch"
USING "freemode_header.sch"

/// PURPOSE: Vehicle request type ID
ENUM VEHICLE_REQUEST_TYPE
	VRT_BOAT = 0,
	VRT_HELI,
	VRT_CAR,
	VRT_PLANE,
	VRT_BLIMP
ENDENUM

CONST_INT NUMBER_OF_VEHICLE_SPAWN_VEHICLES 20

/// PURPOSE:
///    Gets the vehicle model unlocked at the specified rank
/// PARAMS:
///    iRank - rank to check
/// RETURNS:
///    vehicle model as MODEL_NAMES
FUNC MODEL_NAMES GET_VEHICLE_UNLOCKED_AT_RANK(INT iRank)
	
	SWITCH iRank
		CASE 5
			RETURN SEASHARK
		BREAK
		CASE 7
			RETURN MAVERICK
		BREAK
		CASE 10
			RETURN BARRACKS
		BREAK
		CASE 12
			RETURN MAMMATUS
		BREAK
		CASE 15
			RETURN BLAZER
		BREAK
		CASE 17
			RETURN JETMAX
		BREAK
		CASE 20
			RETURN VELUM
		BREAK
		CASE 22
			RETURN CARGOBOB
		BREAK
		CASE 25
			RETURN RIOT
		BREAK
		CASE 27
			RETURN SUBMERSIBLE
		BREAK
		CASE 30
			RETURN FROGGER
		BREAK
		CASE 32
			RETURN TITAN
		BREAK
		CASE 35
			RETURN BULLDOZER
		BREAK
		CASE 37
			RETURN ANNIHILATOR
		BREAK
		CASE 40
			RETURN STUNT
		BREAK
		CASE 42
			RETURN BUZZARD
		BREAK
		CASE 44
			RETURN LAZER
		BREAK
		CASE 46
			RETURN STRETCH
		BREAK
		CASE 48
			RETURN MARQUIS
		BREAK
		CASE 50
			RETURN LUXOR
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE:
///    Gets the number of vehicles unlocked so far at the rank supplied
/// PARAMS:
///    iRank - rank to check
/// RETURNS:
///    number of vehicles unlocked as an INT
FUNC INT GET_NUMBER_OF_VEHICLES_UNLOCKED(INT iRank)

		IF iRank >=5 AND iRank <7
			RETURN 1
		ENDIF
		IF iRank >=7 AND iRank <10
			RETURN 2
		ENDIF
		IF iRank >=10 AND iRank <12
			RETURN 3
		ENDIF
		IF iRank >=12 AND iRank <15
			RETURN 4
		ENDIF
		IF iRank >=15 AND iRank <17
			RETURN 5
		ENDIF
		IF iRank >=17 AND iRank <20
			RETURN 6
		ENDIF
		IF iRank >=20 AND iRank <22
			RETURN 7
		ENDIF
		IF iRank >=22 AND iRank <25
			RETURN 8
		ENDIF
		IF iRank >=25 AND iRank <27
			RETURN 9
		ENDIF
		IF iRank >=27 AND iRank <30
			RETURN 10
		ENDIF
		IF iRank >=30 AND iRank <32
			RETURN 11
		ENDIF
		IF iRank >=32 AND iRank <35
			RETURN 12
		ENDIF
		IF iRank >=35 AND iRank <37
			RETURN 13
		ENDIF
		IF iRank >=37 AND iRank <40
			RETURN 14
		ENDIF
		IF iRank >=40 AND iRank <42
			RETURN 15
		ENDIF
		IF iRank >=42 AND iRank <44
			RETURN 16
		ENDIF
		IF iRank >=44 AND iRank <46
			RETURN 17
		ENDIF
		IF iRank >=46 AND iRank <48
			RETURN 18
		ENDIF
		IF iRank >=48 AND iRank <50 
			RETURN 19
		ENDIF
		IF iRank >=50
			RETURN 20
		ENDIF

	
	RETURN 0
ENDFUNC


/// PURPOSE:
///    Gets the rank required for a specified vehicle model
/// PARAMS:
///    thisModel - vehicle model to get rank for
/// RETURNS:
///    rank required for the vehicle model as an INT
FUNC INT GET_RANK_REQUIRED_FOR_VEHICLE(MODEL_NAMES thisModel)
	
	INT i
	REPEAT MAX_FM_RANK i
		IF GET_VEHICLE_UNLOCKED_AT_RANK(i) = thisModel
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the number of ambient vehicle spawn vehicles the local player has unlocked
/// RETURNS:
///    number of vehicles as an INT
FUNC INT GET_NUMBER_OF_VEHICLE_SPAWN_VEHICLES_UNLOCKED()
	INT iRank = GET_PLAYER_FM_RANK(PLAYER_ID())
	IF iRank >= 	50
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES
	ELIF iRank >= 	48
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 1
	ELIF iRank >= 	46
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 2
	ELIF iRank >= 	44
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 3
	ELIF iRank >= 	42
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 4
	ELIF iRank >= 	40
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 5
	ELIF iRank >= 	37
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 6
	ELIF iRank >= 	35
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 7
	ELIF iRank >= 	32
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 8
	ELIF iRank >= 	30
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 9
	ELIF iRank >= 	27
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 10
	ELIF iRank >= 	25
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 11
	ELIF iRank >= 	22
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 12
	ELIF iRank >= 	20
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 13
	ELIF iRank >= 	17
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 14
	ELIF iRank >= 	15
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 15
	ELIF iRank >= 	12
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 16
	ELIF iRank >= 	10
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 17
	ELIF iRank >= 	7
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 18
	ELIF iRank >= 	5
		RETURN NUMBER_OF_VEHICLE_SPAWN_VEHICLES - 19
	ELSE
		RETURN 0
	ENDIF
ENDFUNC

/// PURPOSE:
///    Gets the vehicle model from an ambient spawn vehicleID
/// PARAMS:
///    index - ambient spawn vehicle ID to get the model for
/// RETURNS:
///    ambient spawn vehicle model as a MODEL_NAMES
FUNC MODEL_NAMES GET_VEHICLE_SPAWN_MODEL_FROM_INDEX(INT index)
	SWITCH index
		CASE 0
			RETURN SEASHARK
		BREAK
		CASE 1
			RETURN MAVERICK
		BREAK
		CASE 2
			RETURN BARRACKS
		BREAK
		CASE 3
			RETURN MAMMATUS
		BREAK
		CASE 4
			RETURN BLAZER
		BREAK
		CASE 5
			RETURN JETMAX
		BREAK
		CASE 6
			RETURN VELUM
		BREAK
		CASE 7
			RETURN CARGOBOB
		BREAK
		CASE 8
			RETURN RIOT
		BREAK
		CASE 9
			RETURN SUBMERSIBLE
		BREAK
		CASE 10
			RETURN FROGGER
		BREAK
		CASE 11
			RETURN TITAN
		BREAK
		CASE 12
			RETURN BULLDOZER
		BREAK
		CASE 13
			RETURN ANNIHILATOR
		BREAK
		CASE 14
			RETURN STUNT
		BREAK
		CASE 15
			RETURN BUZZARD
		BREAK
		CASE 16
			RETURN LAZER
		BREAK
		CASE 17
			RETURN STRETCH
		BREAK
		CASE 18
			RETURN MARQUIS
		BREAK
		CASE 19
			RETURN LUXOR
		BREAK
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

///////////////////////////////////
///    VEHICLE SPAWN DATA		///
///////////////////////////////////

CONST_INT NUMBER_OF_VEHICLE_SPAWN_POINTS							80
CONST_INT NUMBER_OF_BITSETS_REQUIRED_FOR_VEHICLE_SPAWN_POINTS		3

/// PURPOSE:
///    Gets the centre position of a vehicle spawn point
/// PARAMS:
///    iVSID - vehicle spawn point ID to get centre of
/// RETURNS:
///    vehicle spawn point centre as a VECTOR
FUNC VECTOR GET_VEHICLE_SPAWN_POINT_CENTRE(INT iVSID)
	SWITCH iVSID
		CASE 0		//Del Perro Pier
			RETURN <<-1625.6124, -1164.2813, 0.9334>>
		BREAK
		CASE 1		//Puerto Del Sol Water
			RETURN <<-842.4, -1450.9, 6.1216>>
		BREAK
		CASE 2		//Sandy Shores
			RETURN <<1518.5203, 3793.1348, 32.5059>>
		BREAK
		CASE 3		//Paleto Bay
			RETURN <<-733.5297, 6058.2334, -0.2164>>
		BREAK
		CASE 4		//Puerto Del Sol Heliport
			RETURN <<-731.8922, -1452.2524, 5.4344>>
		BREAK
		CASE 5		//Airport Helipad
			RETURN <<-1146.1617, -2864.1794, 12.9484>>
		BREAK
		CASE 6		//Paleto Police Station Heli
			RETURN <<-475.3545, 5988.7290, 30.3369>>
		BREAK
		CASE 7		//Alamo Sea Helipad
			RETURN <<1771.0804, 3240.0198, 41.1395>>
		BREAK
		CASE 8		//Alamo Sea Airstrip
			RETURN <<1715.4431, 3262.2637, 40.1312>>
		BREAK
		CASE 9		//Merryweather Dock Car
			RETURN <<520.1676, -3025.9404, 5.0005>>
		BREAK
		CASE 10		//Merryweather Dock Boat
			RETURN <<575.7730, -3135.9963, -8.9949>>
		BREAK
		CASE 11		//Studio Carpark
			RETURN <<-1093.3718, -438.2651, 35.5541>>
		BREAK
		CASE 12		//Desert Rest Stop
			RETURN <<324.8864, 3420.4375, 35.5840>>
		BREAK
		CASE 13		//Paleto Ammunation
			RETURN <<-323.5444, 6099.3169, 30.4632>>
		BREAK
		CASE 14		//Airport (Upper)
			RETURN <<-1148.3405, -2395.4666, 12.9455>>
		BREAK
		CASE 15		//Airport (Lower)
			RETURN <<-1393.2258, -3264.6533, 12.9452>>
		BREAK
		CASE 16		//Grapeseed Airstrip
			RETURN <<2111.0845, 4778.6079, 40.0314>>
		BREAK
		CASE 17		//Del Perro Beach
			RETURN <<-1598.6852, -1118.4187, 1.7377>>
		BREAK
		CASE 18		//Racetrack
			RETURN <<1130.0779, 57.6775, 79.7564>>
		BREAK
		CASE 19		//Grapeseed Farm
			RETURN <<2555.8350, 4682.1367, 32.9564>>
		BREAK
		CASE 20		//Harmong Farm
			RETURN <<-114.7001, 2803.0520, 52.0026>>
		BREAK
		CASE 21		//Chumash Beach
			RETURN <<-3239.9932, 1348.4573, -1.5927>>
		BREAK
		CASE 22		//Country Pier
			RETURN <<3857.8713, 4462.2783, 1.7289>>
		BREAK
		CASE 23		//Port of LS Helipad
			RETURN <<478.3669, -3370.2512, 5.0701>>
		BREAK
		CASE 24		//South LS Impound
			RETURN <<391.9728, -1620.7030, 28.2928>>
		BREAK
		CASE 25		//Vespucci Police Station
			RETURN <<-1052.0686, -857.0933, 3.8774>>
		BREAK
		CASE 26		//Sandy Shores Police Station Car
			RETURN <<1871.8719, 3688.0383, 32.6619>>
		BREAK
		CASE 27		//Paleto Police Station Car
			RETURN <<-481.9919, 6024.1719, 30.3405>>
		BREAK
		CASE 28		//Country Pier 2
			RETURN <<3896.3059, 4256.5562, -4.8117>>
		BREAK
		CASE 29		//Chilead Bay
			RETURN <<-1606.4572, 5232.1812, -2.0718>>
		BREAK
		CASE 30		//Airport (Hangar)
			RETURN <<-1269.0205, -3376.4868, 12.9450>>
		BREAK
		CASE 31		//Downtown Site
			RETURN <<-139.0162, -998.1196, 26.2774>>
		BREAK
		CASE 32		//Little Seoul Site
			RETURN <<-829.9290, -805.6035, 18.1434>>
		BREAK
		CASE 33		//Grand Senora Site
			RETURN <<844.3568, 2371.8005, 52.6607>>
		BREAK
		CASE 34		//Paleto Site
			RETURN <<63.4185, 6516.1045, 30.5574>>
		BREAK
		CASE 35		//Military Base
			RETURN <<-2004.8624, 3018.9309, 31.8102>>
		BREAK
		CASE 36		//Oriental Theatre
			RETURN <<293.9662, 175.6589, 103.0877>>
		BREAK
		CASE 37		//Vinewood
			RETURN <<-311.6394, 226.6151, 86.8577>>
		BREAK
		CASE 38		//Madrazo's Mansion
			RETURN <<1407.9883, 1118.8448, 113.8379>>
		BREAK
		CASE 39		//Vinyard
			RETURN <<-1889.7390, 2047.2792, 139.8724>>
		BREAK
		CASE 40		//Chumash Pier
			RETURN <<-3418.4014, 947.1168, -2.4595>>
		BREAK
		CASE 41		//Alamo Sea Pier
			RETURN <<708.3345, 4102.0752, 27.1703>>
		BREAK
		CASE 42		//Airport (Executive Hangar)
			RETURN <<-970.3618, -2980.6448, 12.9451>>
		BREAK
		CASE 43		//Central Hospital Helipad
			RETURN <<306.7414, -1459.1448, 45.5095>>
		BREAK
		CASE 44		//Paleto Bay Salvage
			RETURN <<-190.3906, 6287.0176, 30.4893>>
		BREAK
		CASE 45		//San Chianski Farm
			RETURN <<2925.4292, 4635.0425, 47.5451>>
		BREAK
		CASE 46		//Zancudo Gasworks
			RETURN <<-1647.0386, 3089.3809, 30.6732>>
		BREAK
		CASE 47		//Palomino Bay
			RETURN <<2858.6880, -639.0317, -2.6077>>
		BREAK
		CASE 48		//El Gordo Lighthouse
			RETURN <<3385.1621, 5187.8013, -2.4114>>
		BREAK
		CASE 49		//Government Facility
			RETURN <<2503.0769, -391.1114, 93.1199>>
		BREAK
		CASE 50		//Dashound Bus Centre
			RETURN <<434.5729, -622.2773, 27.5001>>
		BREAK
		CASE 51		//Palmer-Taylor Power Station
			RETURN <<2689.1597, 1643.1106, 23.5992>>
		BREAK
		CASE 52		//Great Chaparral Farm
			RETURN <<-86.2156, 1909.3109, 195.7091>>
		BREAK
		CASE 53		//Bayview Lodge
			RETURN <<-702.2061, 5805.3604, 16.2945>>
		BREAK
		CASE 54		//Airport (Upper Runway)
			RETURN <<-911.9402, -3288.8486, 12.9444>>
		BREAK
		CASE 55		//Airport (Construction)
			RETURN <<-1027.5154, -3452.3435, 12.9443>>
		BREAK
		CASE 56		//Airport (Lower Runway)
			RETURN <<-1294.3212, -2215.7461, 12.9334>>
		BREAK
		CASE 57		//Airport (Gates)
			RETURN <<-1372.3783, -2739.9844, 12.9449>>
		BREAK
		CASE 58		//Alamo Sea Airstrip (Mid)
			RETURN <<1445.4724, 3071.8386, 39.9571>>
		BREAK
		CASE 59	//	LSIA - Central
			RETURN <<-1130.9268, -2420.5818, 12.9452>>
		BREAK
		CASE 60	//	LSIA - Runway - North
			RETURN <<-1503.9265, -2497.7522, 12.9444>>
		BREAK
		CASE 61	//	LSIA - West
			RETURN <<-1464.9388, -2933.6392, 13.3136>>
		BREAK
		CASE 62	//	LSIA - South
			RETURN <<-886.0055, -3310.2036, 13.0472>>
		BREAK
		CASE 63	//	SS Airfield - East
			RETURN <<1678.9359, 3243.9736, 39.7616>>
		BREAK
		CASE 64	//	SS Airfield - South
			RETURN <<1511.1600, 3109.9587, 39.5316>>
		BREAK
		CASE 65	//	SS Airfield - Central
			RETURN <<1455.2269, 3183.4934, 39.4141>>
		BREAK
		CASE 66	//	SS Airfield - West
			RETURN <<1154.4531, 3102.0603, 39.4141>>
		BREAK
		CASE 67	//	McKenzie Airfield
			RETURN <<2079.8960, 4782.5713, 40.2002>>
		BREAK
		CASE 68	//	ULSA
			RETURN <<-1722.1332, 166.6716, 63.3710>>
		BREAK
		CASE 69	//	Del Perro
			RETURN <<-1819.0150, -734.5273, 8.1408>>
		BREAK
		CASE 70	//	Paleto Bay
			RETURN <<-311.7762, 6511.5908, 2.2027>>
		BREAK
		CASE 71	//	Sandy Shores
			RETURN <<1443.4319, 3539.5364, 34.8291>>
		BREAK
		CASE 72	//	Terminal
			RETURN <<1091.9177, -3099.8457, 4.8970>>
		BREAK
		CASE 73	//	Elysian Island
			RETURN <<39.9872, -2479.0266, 5.0068>>
		BREAK
		CASE 74	//	El Burro Heights
			RETURN <<1628.5454, -1882.2861, 104.5179>>
		BREAK
		CASE 75	//	Cypress Flats
			RETURN <<1628.5454, -1882.2861, 104.5179>>
		BREAK
		CASE 76	//	Tatavium Mountains
			RETURN <<2566.2424, 396.0424, 107.4626>>
		BREAK
		CASE 77	//	Rex Diner
			RETURN <<2526.0322, 2609.6711, 36.9449>>
		BREAK
		CASE 78	//	You Tool
			RETURN <<2758.8118, 3467.9751, 54.7159>>
		BREAK
		CASE 79	//	Up n Atom
			RETURN <<1556.5394, 6463.3491, 22.4280>>
		BREAK
	ENDSWITCH
	RETURN <<0,0,0>>
ENDFUNC


/// PURPOSE:
///    Gets the number of vehicle model types available at the specified vehicle spawn point
/// PARAMS:
///    iVSID - vehicle spawn point to get the number of vehicle types of
/// RETURNS:
///    number of vehicle types as an INT
FUNC INT GET_NUMBER_OF_VEHICLES_AVAILABLE_AT_SPAWN_POINT(INT iVSID)
	
	SWITCH iVSID
		CASE 0		//Del Perro Pier
			RETURN 1
		BREAK
		CASE 1		//Puerto Del Sol Water
			RETURN 4
		BREAK
		CASE 2		//Sandy Shores
			RETURN 1
		BREAK
		CASE 3		//Paleto Bay
			RETURN 2
		BREAK
		CASE 4		//Puerto Del Sol Heliport
			RETURN 3
		BREAK
		CASE 5		//Airport Helipad
			RETURN 5
		BREAK
		CASE 6		//Paleto Police Station Heli
			RETURN 3
		BREAK
		CASE 7		//Alamo Sea Helipad
			RETURN 5
		BREAK
		CASE 8		//Alamo Sea Airstrip
			RETURN 5
		BREAK
		CASE 9		//Merryweather Dock Car
			RETURN 1
		BREAK
		CASE 10		//Merryweather Dock Boat
			RETURN 1
		BREAK
		CASE 11		//Studio Carpark
			RETURN 1
		BREAK
		CASE 12		//Desert Rest Stop
			RETURN 1
		BREAK
		CASE 13		//Paleto Ammunation
			RETURN 1
		BREAK
		CASE 14		//Airport (Upper)
			RETURN 3
		BREAK
		CASE 15		//Airport (Lower)
			RETURN 3
		BREAK
		CASE 16		//Grapeseed Airstrip
			RETURN 3
		BREAK
		CASE 17		//Del Perro Beach
			RETURN 1
		BREAK
		CASE 18		//Racetrack
			RETURN 1
		BREAK
		CASE 19		//Grapeseed Farm
			RETURN 1
		BREAK
		CASE 20		//Harmong Farm
			RETURN 1
		BREAK
		CASE 21		//Chumash Beach
			RETURN 1
		BREAK
		CASE 22		//Country Pier
			RETURN 1
		BREAK
		CASE 23		//Port of LS Helipad
			RETURN 2
		BREAK
		CASE 24		//South LS Impound
			RETURN 1
		BREAK
		CASE 25		//Vespucci Police Station
			RETURN 1
		BREAK
		CASE 26		//Sandy Shores Police Station Car
			RETURN 1
		BREAK
		CASE 27		//Paleto Police Station Car
			RETURN 1
		BREAK
		CASE 28		//Country Pier 2
			RETURN 1
		BREAK
		CASE 29		//Chilead Bay
			RETURN 3
		BREAK
		CASE 30		//Airport (Hangar)
			RETURN 1
		BREAK
		CASE 31		//Downtown Site
			RETURN 1
		BREAK
		CASE 32		//Little Seoul Site
			RETURN 1
		BREAK
		CASE 33		//Grand Senora Site
			RETURN 1
		BREAK
		CASE 34		//Paleto Site
			RETURN 1
		BREAK
		CASE 35		//Military Base
			RETURN 5
		BREAK
		CASE 36		//Oriental Theatre
			RETURN 1
		BREAK
		CASE 37		//Vinewood
			RETURN 1
		BREAK
		CASE 38		//Madrazo's Mansion
			RETURN 1
		BREAK
		CASE 39		//Vinyard
			RETURN 1
		BREAK
		CASE 40		//Chumash Pier
			RETURN 1
		BREAK
		CASE 41		//Alamo Sea Pier
			RETURN 1
		BREAK
		CASE 42		//Airport (Executive Hangar)
			RETURN 1
		BREAK
		CASE 43		//Central Hospital Helipad
			RETURN 5
		BREAK
		CASE 44		//Paleto Bay Salvage
			RETURN 2
		BREAK
		CASE 45		//San Chianski Farm
			RETURN 1
		BREAK
		CASE 46		//Zancudo Gasworks
			RETURN 2
		BREAK
		CASE 47		//Palomino Bay
			RETURN 2
		BREAK
		CASE 48		//El Gordo Lighthouse
			RETURN 1
		BREAK
		CASE 49		//Government Facility
			RETURN 2
		BREAK
		CASE 50		//Dashound Bus Centre
			RETURN 0
		BREAK
		CASE 51		//Palmer-Taylor Power Station
			RETURN 0
		BREAK
		CASE 52		//Great Chaparral Farm
			RETURN 0
		BREAK
		CASE 53		//Bayview Lodge
			RETURN 0
		BREAK
		CASE 54		//Airport (Upper Runway)
			RETURN 0
		BREAK
		CASE 55		//Airport (Construction)
			RETURN 0
		BREAK
		CASE 56		//Airport (Lower Runway)
			RETURN 0
		BREAK
		CASE 57		//Airport (Gates)
			RETURN 0
		BREAK
		CASE 58		//Alamo Sea Airstrip (Mid)
			RETURN 0
		BREAK
		CASE 59	RETURN 1 BREAK
		CASE 60	RETURN 1 BREAK
		CASE 61	RETURN 1 BREAK
		CASE 62	RETURN 1 BREAK
		CASE 63	RETURN 1 BREAK
		CASE 64	RETURN 1 BREAK
		CASE 65	RETURN 1 BREAK
		CASE 66	RETURN 1 BREAK
		CASE 67	RETURN 1 BREAK
		CASE 68	RETURN 1 BREAK
		CASE 69	RETURN 1 BREAK
		CASE 70	RETURN 1 BREAK
		CASE 71	RETURN 1 BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

//MUST BE IN ORDER OF WHAT LEVEL THEY UNLOCK
/// PURPOSE:
///    Gets the vehicle model for a vehicle unlocked at the specified vehicle spawn point
/// PARAMS:
///    iVSID - vehicle spawn point ID to get vehicle model for
///    iVehicleID - values of the vehicle model to get (0 being first vehicle unlocked, 1 second etc up until GET_NUMBER_OF_VEHICLES_AVAILABLE_AT_SPAWN_POINT)
/// RETURNS:
///    vehicle model as MODEL_NAMES
FUNC MODEL_NAMES GET_VEHICLE_SPAWN_POINT_MODEL(INT iVSID, INT iVehicleID)
	SWITCH iVSID
		CASE 0		//Del Perro Pier
			SWITCH iVehicleID
				CASE 0
					RETURN SEASHARK
				BREAK
			ENDSWITCH
		BREAK
		CASE 1		//Puerto Del Sol Water
			SWITCH iVehicleID
				CASE 0
					RETURN SEASHARK
				BREAK
				CASE 1
					RETURN JETMAX
				BREAK
				CASE 2
					RETURN SUBMERSIBLE
				BREAK
				CASE 3
					RETURN MARQUIS
				BREAK
			ENDSWITCH
		BREAK
		CASE 2		//Sandy Shores
			SWITCH iVehicleID
				CASE 0
					RETURN SEASHARK
				BREAK
			ENDSWITCH
		BREAK
		CASE 3		//Paleto Bay
			SWITCH iVehicleID
				CASE 0
					RETURN SEASHARK
				BREAK
				CASE 1
					RETURN JETMAX
				BREAK
			ENDSWITCH
		BREAK
		CASE 4		//Puerto Del Sol Heliport
			SWITCH iVehicleID
				CASE 0
					RETURN MAVERICK
				BREAK
				CASE 1
					RETURN FROGGER
				BREAK
				CASE 2
					RETURN ANNIHILATOR
				BREAK
			ENDSWITCH
		BREAK
		CASE 5		//Airport Helipad
			SWITCH iVehicleID
				CASE 0
					RETURN MAVERICK
				BREAK
				CASE 1
					RETURN CARGOBOB
				BREAK
				CASE 2
					RETURN FROGGER
				BREAK
				CASE 3
					RETURN ANNIHILATOR
				BREAK
				CASE 4
					RETURN BUZZARD
				BREAK
			ENDSWITCH
		BREAK
		CASE 6		//Paleto Police Station Heli
			SWITCH iVehicleID
				CASE 0
					RETURN MAVERICK
				BREAK
				CASE 1
					RETURN ANNIHILATOR
				BREAK
				CASE 2
					RETURN BUZZARD
				BREAK
			ENDSWITCH
		BREAK
		CASE 7		//Alamo Sea Helipad
			SWITCH iVehicleID
				CASE 0
					RETURN MAVERICK
				BREAK
				CASE 1
					RETURN CARGOBOB
				BREAK
				CASE 2
					RETURN FROGGER
				BREAK
				CASE 3
					RETURN ANNIHILATOR
				BREAK
				CASE 4
					RETURN BUZZARD
				BREAK
			ENDSWITCH
		BREAK
		CASE 8		//Alamo Sea Airstrip
			SWITCH iVehicleID
				CASE 0
					RETURN MAMMATUS
				BREAK
				CASE 1
					RETURN VELUM
				BREAK
				CASE 2
					RETURN TITAN
				BREAK
				CASE 3
					RETURN STUNT
				BREAK
				CASE 4
					RETURN LUXOR
				BREAK
			ENDSWITCH
		BREAK
		CASE 9		//Merryweather Dock Car
			SWITCH iVehicleID
				CASE 0
					RETURN BARRACKS
				BREAK
			ENDSWITCH
		BREAK
		CASE 10		//Merryweather Dock Boat
			SWITCH iVehicleID
				CASE 0
					RETURN SUBMERSIBLE
				BREAK
			ENDSWITCH
		BREAK
		CASE 11		//Studio Carpark
			SWITCH iVehicleID
				CASE 0
					RETURN BARRACKS
				BREAK
			ENDSWITCH
		BREAK
		CASE 12		//Desert Rest Stop
			SWITCH iVehicleID
				CASE 0
					RETURN BARRACKS
				BREAK
			ENDSWITCH
		BREAK
		CASE 13		//Paleto Ammunation
			SWITCH iVehicleID
				CASE 0
					RETURN BARRACKS
				BREAK
			ENDSWITCH
		BREAK
		CASE 14		//Airport (Upper)
			SWITCH iVehicleID
				CASE 0
					RETURN MAMMATUS
				BREAK
				CASE 1
					RETURN VELUM
				BREAK
				CASE 2
					RETURN STUNT
				BREAK
			ENDSWITCH
		BREAK
		CASE 15		//Airport (Lower)
			SWITCH iVehicleID
				CASE 0
					RETURN MAMMATUS
				BREAK
				CASE 1
					RETURN VELUM
				BREAK
				CASE 2
					RETURN STUNT
				BREAK
			ENDSWITCH
		BREAK
		CASE 16		//Grapeseed Airstrip
			SWITCH iVehicleID
				CASE 0
					RETURN MAMMATUS
				BREAK
				CASE 1
					RETURN VELUM
				BREAK
				CASE 2
					RETURN STUNT
				BREAK
			ENDSWITCH
		BREAK
		CASE 17		//Del Perro Beach
			SWITCH iVehicleID
				CASE 0
					RETURN BLAZER
				BREAK
			ENDSWITCH
		BREAK
		CASE 18		//Racetrack
			SWITCH iVehicleID
				CASE 0
					RETURN BLAZER
				BREAK
			ENDSWITCH
		BREAK
		CASE 19		//Grapeseed Farm
			SWITCH iVehicleID
				CASE 0
					RETURN BLAZER
				BREAK
			ENDSWITCH
		BREAK
		CASE 20		//Harmong Farm
			SWITCH iVehicleID
				CASE 0
					RETURN BLAZER
				BREAK
			ENDSWITCH
		BREAK
		CASE 21		//Chumash Beach
			SWITCH iVehicleID
				CASE 0
					RETURN JETMAX
				BREAK
			ENDSWITCH
		BREAK
		CASE 22		//Country Pier
			SWITCH iVehicleID
				CASE 0
					RETURN JETMAX
				BREAK
			ENDSWITCH
		BREAK
		CASE 23		//Port of LS Helipad
			SWITCH iVehicleID
				CASE 0
					RETURN CARGOBOB
				BREAK
				CASE 1
					RETURN BUZZARD
				BREAK
			ENDSWITCH
		BREAK
		CASE 24		//South LS Impound
			SWITCH iVehicleID
				CASE 0
					RETURN RIOT
				BREAK
			ENDSWITCH
		BREAK
		CASE 25		//Vespucci Police Station
			SWITCH iVehicleID
				CASE 0
					RETURN RIOT
				BREAK
			ENDSWITCH
		BREAK
		CASE 26		//Sandy Shores Station Car
			SWITCH iVehicleID
				CASE 0
					RETURN RIOT
				BREAK
			ENDSWITCH
		BREAK
		CASE 27		//Paleto Station Car
			SWITCH iVehicleID
				CASE 0
					RETURN RIOT
				BREAK
			ENDSWITCH
		BREAK
		CASE 28		//Country Pier 2
			SWITCH iVehicleID
				CASE 0
					RETURN SUBMERSIBLE
				BREAK
			ENDSWITCH
		BREAK
		CASE 29		//Chilead Bay
			SWITCH iVehicleID
				CASE 0
					RETURN SEASHARK
				BREAK
				CASE 1
					RETURN SUBMERSIBLE
				BREAK
				CASE 2
					RETURN MARQUIS
				BREAK
			ENDSWITCH
		BREAK
		CASE 30		//Airport (Hangar)
			SWITCH iVehicleID
				CASE 0
					RETURN TITAN
				BREAK
			ENDSWITCH
		BREAK
		CASE 31		//Downtown Site
			SWITCH iVehicleID
				CASE 0
					RETURN BULLDOZER
				BREAK
			ENDSWITCH
		BREAK
		CASE 32		//Little Seoul Site
			SWITCH iVehicleID
				CASE 0
					RETURN BULLDOZER
				BREAK
			ENDSWITCH
		BREAK
		CASE 33		//Grand Senora Site
			SWITCH iVehicleID
				CASE 0
					RETURN BULLDOZER
				BREAK
			ENDSWITCH
		BREAK
		CASE 34		//Paleto Site
			SWITCH iVehicleID
				CASE 0
					RETURN BULLDOZER
				BREAK
			ENDSWITCH
		BREAK
		CASE 35		//Military Base
			SWITCH iVehicleID
				CASE 0
					RETURN LAZER
				BREAK
			ENDSWITCH
		BREAK
		CASE 36		//Oriental Theatre
			SWITCH iVehicleID
				CASE 0
					RETURN STRETCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 37		//Vinewood
			SWITCH iVehicleID
				CASE 0
					RETURN STRETCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 38		//Madrazo's Mansion
			SWITCH iVehicleID
				CASE 0
					RETURN STRETCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 39		//Vinyard
			SWITCH iVehicleID
				CASE 0
					RETURN STRETCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 40		//Chumash Pier
			SWITCH iVehicleID
				CASE 0
					RETURN MARQUIS
				BREAK
			ENDSWITCH
		BREAK
		CASE 41		//Alamo Sea Pier
			SWITCH iVehicleID
				CASE 0
					RETURN MARQUIS
				BREAK
			ENDSWITCH
		BREAK
		CASE 42		//Airport (Executive Hangar)
			SWITCH iVehicleID
				CASE 0
					RETURN LUXOR
				BREAK
			ENDSWITCH
		BREAK
		CASE 43		//Central Hospital Helipad
			SWITCH iVehicleID
				CASE 0
					RETURN MAVERICK
				BREAK
				CASE 1
					RETURN CARGOBOB
				BREAK
				CASE 2
					RETURN FROGGER
				BREAK
				CASE 3
					RETURN ANNIHILATOR
				BREAK
				CASE 4
					RETURN BUZZARD
				BREAK
			ENDSWITCH
		BREAK
		CASE 44		//Paleto Bay Salvage
			SWITCH iVehicleID
				CASE 0
					RETURN BLAZER
				BREAK
				CASE 1
					RETURN BULLDOZER
				BREAK
			ENDSWITCH
		BREAK
		CASE 45		//San Chianski Farm
			SWITCH iVehicleID
				CASE 0
					RETURN BARRACKS
				BREAK
			ENDSWITCH
		BREAK
		CASE 46		//Zancudo Gasworks
			SWITCH iVehicleID
				CASE 0
					RETURN BARRACKS
				BREAK
				CASE 1
					RETURN BLAZER
				BREAK
			ENDSWITCH
		BREAK
		CASE 47		//Palomino Bay
			SWITCH iVehicleID
				CASE 0
					RETURN SEASHARK
				BREAK
				CASE 1
					RETURN JETMAX
				BREAK
			ENDSWITCH
		BREAK
		CASE 48		//El Gordo Lighthouse
			SWITCH iVehicleID
				CASE 0
					RETURN SEASHARK
				BREAK
			ENDSWITCH
		BREAK
		CASE 49		//Government Facility
			SWITCH iVehicleID
				CASE 0
					RETURN MAVERICK
				BREAK
				CASE 1
					RETURN ANNIHILATOR
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE:
///    Checks if the specified vehicle request type can be spawned at the specified vehicle spawn point
/// PARAMS:
///    iVSID - vehicle spawn point ID to check
///    thisVRT - vehicle request type to check
/// RETURNS:
///    true/false
FUNC BOOL CAN_VEHICLE_REQUEST_TYPE_SPAWN_AT_VEHICLE_SPAWN_POINT(INT iVSID, VEHICLE_REQUEST_TYPE thisVRT)
	SWITCH iVSID
		CASE 0		//Del Perro Pier
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN TRUE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 1		//Puerto Del Sol Water
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN TRUE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 2		//Sandy Shores
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN TRUE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 3		//Paleto Bay
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN TRUE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 4		//Puerto Del Sol Heliport
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN TRUE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 5		//Airport Helipad
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN TRUE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 6		//Paleto Police Station Heli
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN TRUE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 7		//Alamo Sea Helipad
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN TRUE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 8		//Alamo Sea Airstrip
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN TRUE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 9		//Merryweather Dock Car
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 10		//Merryweather Dock Boat
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN TRUE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 11		//Studio Carpark
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 12		//Desert Rest Stop
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 13		//Paleto Ammunation
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 14		//Airport (Upper)
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN TRUE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 15		//Airport (Lower)
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN TRUE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 16		//Grapeseed Airstrip
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN TRUE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 17		//Del Perro Beach
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 18		//Racetrack
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 19		//Grapeseed Farm
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 20		//Harmong Farm
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 21		//Chumash Beach
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN TRUE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 22		//Country Pier
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN TRUE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 23		//Port of LS Helipad
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN TRUE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 24		//South LS Impound
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 25		//Vespucci Police Station
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 26		//Sandy Shores Police Station Car
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 27		//Paleto Police Station Car
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 28		//Country Pier 2
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN TRUE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 29		//Chilead Bay
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN TRUE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 30		//Airport (Hangar)
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN TRUE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 31		//Downtown Site
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 32		//Little Seoul Site
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 33		//Grand Senora Site
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 34		//Paleto Site
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 35		//Military Base
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN TRUE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN TRUE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 36		//Oriental Theatre
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 37		//Vinewood
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 38		//Madrazo's Mansion
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 39		//Vinyard
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 40		//Chumash Pier
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN TRUE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 41		//Alamo Sea Pier
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN TRUE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 42		//Airport (Executive Hangar)
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN TRUE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 43		//Central Hospital Helipad
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN TRUE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 44		//Paleto Bay Salvage
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 45		//San Chianski Farm
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 46		//Zancudo Gasworks
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 47		//Palomino Bay
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN TRUE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 48		//El Gordo Lighthouse
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN TRUE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 49		//Government Facility
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN TRUE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 50		//Dashound Bus Centre
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 51		//Palmer-Taylor Power Station
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 52		//Great Chaparral Farm
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 53		//Bayview Lodge
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 54 	//Airport (Upper Runway)
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN TRUE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 55		//Airport (Construction)
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN TRUE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 56		//Airport (Lower Runway)
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN TRUE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 57		//Airport (Gates)
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN TRUE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 58		//Alamo Sea Airstrip (Mid)
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN TRUE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 59		// LSIA - Central
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 60		// LSIA - Runway - North
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 61		// LSIA - West
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 62		// LSIA - South
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 63		// SS Airfield - East
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 64		// SS Airfield - South
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 65		// SS Airfield - Central
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 66		// SS Airfield - West
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 67		// McKenzie Airfield
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 68		// ULSA
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 69		// Del Perro
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 70		// Paleto Bay
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 71		// Sandy Shores
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN FALSE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 72		//Terminal
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 73		//Elysian Island
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 74		//El Burro Heights
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 75		//Cypress Flats
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 76		//Tatavium Mountains
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 77		//Rex Diner
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 78		//You Tool
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 79		//Up n Atom
			SWITCH thisVRT
				CASE VRT_BOAT
					RETURN FALSE
				BREAK
				CASE VRT_HELI
					RETURN FALSE
				BREAK
				CASE VRT_CAR
					RETURN TRUE
				BREAK
				CASE VRT_PLANE
					RETURN FALSE
				BREAK
				CASE VRT_BLIMP
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the number of individual spawn coordinates at the specified vehicle spawn point
/// PARAMS:
///    iVSID - vehicle spawn point ID to get number of spawn coordinates for
/// RETURNS:
///    number of vehicle spawn coordinates as an INT
FUNC INT GET_NUMBER_OF_VEHICLE_SPAWN_POINT_COORDS(INT iVSID, BOOL bIncludeExtraCoords = FALSE)
	SWITCH iVSID
		CASE 0		//Del Perro Pier
			RETURN 4
		BREAK
		CASE 1		//Puerto Del Sol Water
			RETURN 7
		BREAK
		CASE 2		//Sandy Shores
			RETURN 4
		BREAK
		CASE 3		//Paleto Bay
			RETURN 4
		BREAK
		CASE 4		//Puerto Del Sol Heliport
			RETURN 4
		BREAK
		CASE 5		//Airport Helipad
			RETURN 4
		BREAK
		CASE 6		//Paleto Police Station Heli
			RETURN 2
		BREAK
		CASE 7		//Alamo Sea Helipad
			RETURN 4
		BREAK
		CASE 8		//Alamo Sea Airstrip
			RETURN 2
		BREAK
		CASE 9		//Merryweather Dock Car
			RETURN 4
		BREAK
		CASE 10		//Merryweather Dock Boat
			RETURN 4
		BREAK
		CASE 11		//Studio Carpark
			RETURN 4
		BREAK
		CASE 12		//Desert Rest Stop
			RETURN 4
		BREAK
		CASE 13		//Paleto Ammunation
			RETURN 4
		BREAK
		CASE 14		//Airport (Upper)
			RETURN 4
		BREAK
		CASE 15		//Airport (Lower)
			RETURN 2
		BREAK
		CASE 16		//Grapeseed Airstrip
			RETURN 2
		BREAK
		CASE 17		//Del Perro Beach
			RETURN 4
		BREAK
		CASE 18		//Racetrack
			RETURN 4
		BREAK
		CASE 19		//Grapeseed Farm
			RETURN 4
		BREAK
		CASE 20		//Harmong Farm
			RETURN 4
		BREAK
		CASE 21		//Chumash Beach
			RETURN 4
		BREAK
		CASE 22		//Country Pier
			RETURN 4
		BREAK
		CASE 23		//Port of LS Helipad
			RETURN 4
		BREAK
		CASE 24		//South LS Impound
			RETURN 1
		BREAK
		CASE 25		//Vespucci Police Station
			RETURN 1
		BREAK
		CASE 26		//Sandy Shores Police Station Car
			RETURN 1
		BREAK
		CASE 27		//Paleto Police Station Car
			RETURN 1
		BREAK
		CASE 28		//Country Pier 2
			RETURN 4
		BREAK
		CASE 29		//Chilead Bay
			RETURN 3
		BREAK
		CASE 30		//Airport (Hangar)
			IF bIncludeExtraCoords
				RETURN 2	// Disabled for url:bugstar:3697676 (conflicts with Hangar properties)
			ENDIF
			RETURN 1
		BREAK
		CASE 31		//Downtown Site
			RETURN 4
		BREAK
		CASE 32		//Little Seoul Site
			RETURN 4
		BREAK
		CASE 33		//Grand Senora Site
			RETURN 4
		BREAK
		CASE 34		//Paleto Site
			RETURN 4
		BREAK
		CASE 35		//Military Base
			RETURN 5
		BREAK
		CASE 36		//Oriental Theatre
			RETURN 4
		BREAK
		CASE 37		//Vinewood
			RETURN 4
		BREAK
		CASE 38		//Madrazo's Mansion
			RETURN 4
		BREAK
		CASE 39		//Vinyard
			RETURN 4
		BREAK
		CASE 40		//Chumash Pier
			RETURN 4
		BREAK
		CASE 41		//Alamo Sea Pier
			RETURN 4
		BREAK
		CASE 42		//Airport (Executive Hangar)
			RETURN 5
		BREAK
		CASE 43		//Central Hospital Helipad
			RETURN 3
		BREAK
		CASE 44		//Paleto Bay Salvage
			RETURN 4
		BREAK
		CASE 45		//San Chianski Farm
			RETURN 4
		BREAK
		CASE 46		//Zancudo Gasworks
			RETURN 4
		BREAK
		CASE 47		//Palomino Bay
			RETURN 4
		BREAK
		CASE 48		//El Gordo Lighthouse
			RETURN 1
		BREAK
		CASE 49		//Government Facility
			RETURN 2
		BREAK
		CASE 50		//Dashound Bus Centre
			RETURN 4
		BREAK
		CASE 51		//Palmer-Taylor Power Station
			RETURN 4
		BREAK
		CASE 52		//Great Chaparral Farm
			RETURN 5
		BREAK
		CASE 53		//Bayview Lodge
			RETURN 4
		BREAK
		CASE 54 	//Airport (Upper Runway)
			RETURN 3
		BREAK
		CASE 55		//Airport (Construction)
			RETURN 5
		BREAK
		CASE 56		//Airport (Lower Runway)
			RETURN 3
		BREAK
		CASE 57		//Airport (Gates)
			RETURN 4
		BREAK
		CASE 58		//Alamo Sea Airstrip (Mid)
			RETURN 2
		BREAK
		CASE 59	//	LSIA - Central
			RETURN 6
		BREAK
		CASE 60	//	LSIA - Runway - North
			RETURN 5
		BREAK
		CASE 61	//	LSIA - West
			RETURN 7
		BREAK
		CASE 62	//	LSIA - South
			RETURN 5
		BREAK
		CASE 63	//	SS Airfield - East
			RETURN 4
		BREAK
		CASE 64	//	SS Airfield - South
			RETURN 4
		BREAK
		CASE 65	//	SS Airfield - Central
			RETURN 4
		BREAK
		CASE 66	//	SS Airfield - West
			RETURN 5
		BREAK
		CASE 67	//	McKenzie Airfield
			RETURN 2
		BREAK
		CASE 68	//	ULSA
			RETURN 1
		BREAK
		CASE 69	//	Del Perro
			RETURN 3
		BREAK
		CASE 70	//	Paleto Bay
			RETURN 2
		BREAK
		CASE 71	//	Sandy Shores
			RETURN 2
		BREAK
		CASE 72	//	Terminal
			RETURN 4
		BREAK
		CASE 73	//	Elysian Island
			RETURN 4
		BREAK
		CASE 74	//	El Burro Heights
			RETURN 4
		BREAK
		CASE 75	//	Cypress Flats
			RETURN 4
		BREAK
		CASE 76	//	Tatavium Mountains
			RETURN 4
		BREAK
		CASE 77	//	Rex Diner
			RETURN 4
		BREAK
		CASE 78	//	You Tool
			RETURN 4
		BREAK
		CASE 79	//	Up n Atom
			RETURN 4
		BREAK
	ENDSWITCH
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets a vehicle spawn coordinate at a specified vehicle spawn point
/// PARAMS:
///    iVSID - vehicle spawn point ID to get a coordinate from
///    iCoordID - coordinate ID to get coordinate of - maximum amount from GET_NUMBER_OF_VEHICLE_SPAWN_POINT_COORDS
/// RETURNS:
///    vehicle spawn coordinate as a VECTOR
FUNC VECTOR GET_VEHICLE_SPAWN_POINT_COORD(INT iVSID, INT iCoordID)
	SWITCH iVSID
		CASE 0		//Del Perro Pier
			SWITCH iCoordID
				CASE 0
					RETURN <<-1631.2458, -1163.3325, 0.3>>
				BREAK
				CASE 1
					RETURN <<-1625.3490, -1169.7495, 0.3>>
				BREAK
				CASE 2
					RETURN <<-1797.3087, -1230.5851, -9.1264>>
				BREAK
				CASE 3
					RETURN <<-1664.4729, -1196.3346, -5.0222>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 1		//Puerto Del Sol Water
			SWITCH iCoordID
				CASE 0
					RETURN <<-749.8039, -1354.6741, 0.0>>
				BREAK
				CASE 1
					RETURN <<-910.5193, -1470.9167, 0.0000>>
				BREAK
				CASE 2
					RETURN <<-856.5488, -1327.5321, 0.0>>
				BREAK
				CASE 3
					RETURN <<-964.4990, -1389.4095, 0.0>>
				BREAK
				CASE 4
					RETURN <<-886.3637, -1406.7307, 0.0>>
				BREAK
				CASE 5
					RETURN <<-748.0861, -1356.1794, 0.0>>
				BREAK
				CASE 6
					RETURN <<-831.1410, -1405.7360, 0.0>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 2		//Sandy Shores
			SWITCH iCoordID
				CASE 0
					RETURN <<1512.6337, 3835.7131, 26.8383>>
				BREAK
				CASE 1
					RETURN <<1559.5784, 3856.3381, 28.1415>>
				BREAK
				CASE 2
					RETURN <<1496.6057, 3884.2554, 24.4969>>
				BREAK
				CASE 3
					RETURN <<1459.2144, 3840.7327, 24.6070>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 3		//Paleto Bay
			SWITCH iCoordID
				CASE 0
					RETURN <<-784.8182, 6041.9985, -0.7305>>
				BREAK
				CASE 1
					RETURN <<-883.2603, 5894.4834, -0.8811>>
				BREAK
				CASE 2
					RETURN <<-860.3978, 5868.2349, -1.5490>>
				BREAK
				CASE 3
					RETURN <<-838.3624, 6100.2554, -1.3427>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 4		//Puerto Del Sol Heliport
			SWITCH iCoordID
				CASE 0
					RETURN <<-746.1063, -1469.4550, 4.0005>>
				BREAK
				CASE 1
					RETURN <<-746.0370, -1433.3291, 4.0005>>
				BREAK
				CASE 2
					RETURN <<-721.4189, -1473.2883, 4.0005>>
				BREAK
				CASE 3
					RETURN <<-724.7337, -1444.4575, 4.0005>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 5		//Airport Helipad
			SWITCH iCoordID
				CASE 0
					RETURN <<-1178.3575, -2845.7083, 12.9489>>
				BREAK
				CASE 1
					RETURN <<-1145.9777, -2864.4158, 12.9484>>
				BREAK
				CASE 2
					RETURN <<-1112.5930, -2883.8840, 12.9473>>
				BREAK
				CASE 3
					RETURN <<-1155.3302, -2926.5520, 12.9451>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 6		//Paleto Police Station Heli
			SWITCH iCoordID
				CASE 0
					RETURN <<-475.1143, 5989.2451, 30.3369>>
				BREAK
				CASE 1
					RETURN <<-501.8407, 6009.7686, 32.2276>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 7		//Alamo Sea Helipad
			SWITCH iCoordID
				CASE 0
					RETURN <<1771.0804, 3240.0198, 41.1395>>
				BREAK
				CASE 1
					RETURN <<1797.5587, 3245.2415, 41.6465>>
				BREAK
				CASE 2
					RETURN <<1741.4722, 3204.2476, 42.1659>>
				BREAK
				CASE 3
					RETURN <<1781.0540, 3260.4707, 40.9584>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 8		//Alamo Sea Airstrip
			SWITCH iCoordID
				CASE 0
					RETURN <<1731.4932, 3309.8779, 40.2235>>
				BREAK
				CASE 1
					RETURN <<1738.0239, 3246.9741, 40.4643>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 9		//Merryweather Dock Car
			SWITCH iCoordID
				CASE 0
					RETURN <<528.8665, -3047.8489, 5.0696>>
				BREAK
				CASE 1
					RETURN <<535.2628, -3008.0715, 5.0455>>
				BREAK
				CASE 2
					RETURN <<503.1337, -3007.9312, 5.0455>>
				BREAK
				CASE 3
					RETURN <<460.3771, -3025.6772, 5.0006>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 10		//Merryweather Dock Boat
			SWITCH iCoordID
				CASE 0
					RETURN <<580.5875, -3226.6113, 1.0>>
				BREAK
				CASE 1
					RETURN <<534.6127, -3191.5513, 1.0>>
				BREAK
				CASE 2
					RETURN <<571.5367, -3179.8369, -15.8708>>
				BREAK
				CASE 3
					RETURN <<519.5238, -3229.7837, -17.9805>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 11		//Studio Carpark
			SWITCH iCoordID
				CASE 0
					RETURN <<-1093.3718, -438.2651, 35.5541>>
				BREAK
				CASE 1
					RETURN <<-1086.3149, -468.2271, 35.0215>>
				BREAK
				CASE 2
					RETURN <<-1094.1876, -455.3147, 34.9132>>
				BREAK
				CASE 3
					RETURN <<-1085.4437, -484.4470, 35.6160>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 12		//Desert Rest Stop
			SWITCH iCoordID
				CASE 0
					RETURN <<324.8864, 3420.4375, 35.5840>>
				BREAK
				CASE 1
					RETURN <<321.7055, 3388.2559, 35.4034>>
				BREAK
				CASE 2
					RETURN <<356.4297, 3421.4402, 35.1749>>
				BREAK
				CASE 3
					RETURN <<326.8226, 3462.8713, 34.9034>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 13		//Paleto Ammunation
			SWITCH iCoordID
				CASE 0
					RETURN <<-323.5444, 6099.3169, 30.4632>>
				BREAK
				CASE 1
					RETURN <<-369.1790, 6062.4014, 30.5001>>
				BREAK
				CASE 2
					RETURN <<-335.1542, 6067.3931, 30.2426>>
				BREAK
				CASE 3
					RETURN <<-284.1803, 6119.2393, 30.5149>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 14		//Airport (Upper)
			SWITCH iCoordID
				CASE 0
					RETURN <<-1137.6566, -2413.3889, 12.9455>>
				BREAK
				CASE 1
					RETURN <<-1064.2714, -2393.0964, 12.9446>>
				BREAK
				CASE 2
					RETURN <<-1204.9109, -2352.3337, 12.9446>>
				BREAK
				CASE 3
					RETURN <<-1297.1720, -2478.1223, 12.9452>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 15		//Airport (Lower)
			SWITCH iCoordID
				CASE 0
					RETURN <<-1408.8335, -3247.4534, 12.9452>>
				BREAK
				CASE 1
					RETURN <<-1359.2195, -3277.7383, 12.9452>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 16		//Grapeseed Airstrip
			SWITCH iCoordID
				CASE 0
					RETURN <<2132.7222, 4784.7900, 39.9702>>
				BREAK
				CASE 1
					RETURN <<2086.7317, 4768.6528, 40.2110>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 17		//Del Perro Beach
			SWITCH iCoordID
				CASE 0
					RETURN <<-1598.6852, -1118.4187, 1.7377>>
				BREAK
				CASE 1
					RETURN <<-1584.3942, -1077.3564, 4.8847>>
				BREAK
				CASE 2
					RETURN <<-1569.4575, -1068.5690, 5.5234>>
				BREAK
				CASE 3
					RETURN <<-1558.0049, -1054.7091, 5.5471>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 18		//Racetrack
			SWITCH iCoordID
				CASE 0
					RETURN <<1117.0767, 46.5436, 79.7564>>
				BREAK
				CASE 1
					RETURN <<1137.7690, 51.3561, 79.7561>>
				BREAK
				CASE 2
					RETURN <<1130.1080, 76.4697, 79.7564>>
				BREAK
				CASE 3
					RETURN <<1144.4741, 75.4546, 79.7561>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 19		//Grapeseed Farm
			SWITCH iCoordID
				CASE 0
					RETURN <<2553.3821, 4670.1807, 32.9500>>
				BREAK
				CASE 1
					RETURN <<2556.7551, 4689.3447, 32.9077>>
				BREAK
				CASE 2
					RETURN <<2569.0935, 4697.0547, 33.0596>>
				BREAK
				CASE 3
					RETURN <<2591.3877, 4719.1714, 32.8897>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 20		//Harmong Farm
			SWITCH iCoordID
				CASE 0
					RETURN <<-99.1753, 2797.6013, 52.3487>>
				BREAK
				CASE 1
					RETURN <<-126.8620, 2800.1912, 52.1080>>
				BREAK
				CASE 2
					RETURN <<-76.7012, 2798.6885, 52.5195>>
				BREAK
				CASE 3
					RETURN <<-50.5863, 2840.7839, 54.5553>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 21		//Chumash Beach
			SWITCH iCoordID
				CASE 0
					RETURN <<-3255.3445, 1342.8015, 0.3>>
				BREAK
				CASE 1
					RETURN <<-3249.7971, 1372.7983, 0.3>>
				BREAK
				CASE 2
					RETURN <<-3293.8193, 1314.5510, -5.1295>>
				BREAK
				CASE 3
					RETURN <<-3297.4692, 1280.7610, -3.7046>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 22		//Country Pier
			SWITCH iCoordID
				CASE 0
					RETURN <<3864.1821, 4467.8662, 0.0>>
				BREAK
				CASE 1
					RETURN <<3855.9053, 4455.5151, 0.0>>
				BREAK
				CASE 2
					RETURN <<3847.0715, 4502.6187, -2.1762>>
				BREAK
				CASE 3
					RETURN <<3893.6558, 4450.7085, -7.3468>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 23		//Port of LS Helipad
			SWITCH iCoordID
				CASE 0
					RETURN <<478.3669, -3370.2512, 5.0701>>
				BREAK
				CASE 1
					RETURN <<475.2874, -3305.3711, 5.0699>>
				BREAK
				CASE 2
					RETURN <<504.1496, -3368.3110, 5.0699>>
				BREAK
				CASE 3
					RETURN <<502.9900, -3309.8518, 5.0699>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 24		//South LS Impound
			SWITCH iCoordID
				CASE 0
					RETURN <<391.9728, -1620.7030, 28.2928>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 25		//Vespucci Police Station
			SWITCH iCoordID
				CASE 0
					RETURN <<-1052.0686, -857.0933, 3.8774>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 26		//Sandy Shores Police Station Car
			SWITCH iCoordID
				CASE 0
					RETURN <<1871.8719, 3688.0383, 32.6619>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 27		//Paleto Police Station Car
			SWITCH iCoordID
				CASE 0
					RETURN <<-481.9919, 6024.1719, 30.3405>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 28		//Country Pier 2
			SWITCH iCoordID
				CASE 0
					RETURN <<3904.3059, 4256.5562, 0.0>>
				BREAK
				CASE 1
					RETURN <<3914.1848, 4232.4927, -8.7459>>
				BREAK
				CASE 2
					RETURN <<3916.1509, 4312.5156, -3.5977>>
				BREAK
				CASE 3
					RETURN <<3956.0437, 4281.0137, -8.6422>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 29		//Chilead Bay
			SWITCH iCoordID
				CASE 0
					RETURN <<-1596.8855, 5270.3662, 0.0>>
				BREAK
				CASE 1
					RETURN <<-1578.7537, 5229.8340, -2.0372>>
				BREAK
				CASE 2
					RETURN <<-1626.2391, 5245.2842, -2.9461>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 30		//Airport (Hangar)
			SWITCH iCoordID
				CASE 0
					RETURN <<-1269.0205, -3376.4868, 12.9450>>
				BREAK
				CASE 1
					RETURN <<-1029.3981, -3450.0459, 12.9443>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 31		//Downtown Site
			SWITCH iCoordID
				CASE 0
					RETURN <<-174.4752, -1027.1837, 26.2740>>
				BREAK
				CASE 1
					RETURN <<-122.7135, -1035.7708, 26.2740>>
				BREAK
				CASE 2
					RETURN <<-115.7006, -966.1431, 26.2815>>
				BREAK
				CASE 3	
					RETURN <<-15.5297, -1032.0538, 27.9431>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 32		//Little Seoul Site
			SWITCH iCoordID
				CASE 0
					RETURN <<-831.7932, -805.6157, 18.1033>>
				BREAK
				CASE 1
					RETURN <<-830.1885, -764.1763, 20.6692>>
				BREAK
				CASE 2
					RETURN <<-788.5379, -813.4049, 19.6193>>
				BREAK
				CASE 3
					RETURN <<-835.4496, -749.3867, 22.0796>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 33		//Grand Senora Site
			SWITCH iCoordID
				CASE 0
					RETURN <<844.3568, 2371.8005, 52.6607>>
				BREAK
				CASE 1
					RETURN <<896.6462, 2352.7434, 50.6525>>
				BREAK
				CASE 2
					RETURN <<842.4652, 2341.3550, 49.3855>>
				BREAK
				CASE 3
					RETURN <<847.5488, 2404.7251, 52.0045>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 34		//Paleto Site
			SWITCH iCoordID
				CASE 0
					RETURN <<41.2553, 6550.5820, 30.4309>>
				BREAK
				CASE 1
					RETURN <<81.7379, 6497.6846, 30.3527>>
				BREAK
				CASE 2
					RETURN <<95.5255, 6532.8296, 30.5575>>
				BREAK
				CASE 3
					RETURN <<88.3115, 6571.9595, 30.4888>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 35		//Military Base
			SWITCH iCoordID
				CASE 0
					RETURN <<-1973.5728, 2814.7087, 31.8102>>
				BREAK
				CASE 1
					RETURN <<-2144.2012, 3020.4082, 31.8099>>
				BREAK
				CASE 2
					RETURN <<-2014.3087, 2947.8784, 31.8102>>
				BREAK
				CASE 3
					RETURN <<-1925.9717, 3023.6125, 31.8102>>
				BREAK
				CASE 4
					RETURN <<-2120.6848, 3136.5005, 31.8100>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 36		//Oriental Theatre
			SWITCH iCoordID
				CASE 0
					RETURN <<334.3942, 133.9089, 102.0650>>
				BREAK
				CASE 1
					RETURN <<326.1827, 163.3389, 102.4264>>
				BREAK
				CASE 2
					RETURN <<270.0640, 144.1543, 103.3251>>
				BREAK
				CASE 3
					RETURN <<251.1784, 191.4102, 103.9018>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 37		//Vinewood
			SWITCH iCoordID
				CASE 0
					RETURN <<-311.6394, 226.6151, 86.8577>>
				BREAK
				CASE 1
					RETURN <<-359.9595, 263.0676, 83.6839>>
				BREAK
				CASE 2
					RETURN <<-335.8146, 271.2740, 84.9006>>
				BREAK
				CASE 3
					RETURN <<-306.3647, 275.8736, 86.8443>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 38		//Madrazo's Mansion
			SWITCH iCoordID
				CASE 0
					RETURN <<1407.9883, 1118.8448, 113.8379>>
				BREAK
				CASE 1
					RETURN <<1448.0789, 1111.7440, 113.3354>>
				BREAK
				CASE 2
					RETURN <<1360.4187, 1187.9432, 111.4769>>
				BREAK
				CASE 3
					RETURN <<1361.0031, 1165.1925, 112.5676>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 39		//Vinyard
			SWITCH iCoordID
				CASE 0
					RETURN <<-1889.7390, 2047.2792, 139.8724>>
				BREAK
				CASE 1
					RETURN <<-1887.4347, 2018.5653, 139.7667>>
				BREAK
				CASE 2
					RETURN <<-1906.6925, 2039.1638, 139.7387>>
				BREAK
				CASE 3
					RETURN <<-1896.5612, 1992.0033, 141.0487>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 40		//Chumash Pier
			SWITCH iCoordID
				CASE 0
					RETURN <<-3418.4014, 947.1168, 0.0>>
				BREAK
				CASE 1
					RETURN <<-3439.1338, 965.0126, -4.3285>>
				BREAK
				CASE 2
					RETURN <<-3412.4016, 999.9906, -5.6799>>
				BREAK
				CASE 3
					RETURN <<-3346.5857, 996.5172, -1.9815>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 41		//Alamo Sea Pier
			SWITCH iCoordID
				CASE 0
					RETURN <<708.3345, 4102.0752, 27.1703>>
				BREAK
				CASE 1
					RETURN <<729.6142, 4099.8237, 26.2801>>
				BREAK
				CASE 2
					RETURN <<668.8455, 4152.5278, 25.8643>>
				BREAK
				CASE 3
					RETURN <<787.9177, 4127.0186, 27.2885>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 42		//Airport (Executive Hangar)
			SWITCH iCoordID
				CASE 0
					RETURN <<-970.3618, -2980.6448, 12.9451>>
				BREAK
				CASE 1
					RETURN <<-988.0789, -3013.4929, 12.9451>>
				BREAK
				CASE 2
					RETURN <<-1005.7699, -2926.3755, 12.9479>>
				BREAK
				CASE 3
					RETURN <<-936.8502, -3096.9810, 12.9444>>
				BREAK
				CASE 4
					RETURN <<-1061.6713, -2917.6487, 12.9499>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 43		//Central Hospital Helipad
			SWITCH iCoordID
				CASE 0
					RETURN <<298.9857, -1454.1910, 45.5095>>
				BREAK
				CASE 1
					RETURN <<312.6378, -1466.2029, 45.5095>>
				BREAK
				CASE 2
					RETURN <<304.9639, -1418.5284, 40.5093>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 44		//Paleto Bay Salvage
			SWITCH iCoordID
				CASE 0
					RETURN <<-169.3283, 6273.0439, 30.4894>>
				BREAK
				CASE 1
					RETURN <<-200.9346, 6274.8086, 30.4893>>
				BREAK
				CASE 2
					RETURN <<-190.6302, 6299.5059, 30.4879>>
				BREAK
				CASE 3
					RETURN <<-187.4057, 6326.4824, 30.5231>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 45		//San Chianski Farm
			SWITCH iCoordID
				CASE 0
					RETURN <<2925.4292, 4635.0425, 47.5451>>
				BREAK
				CASE 1
					RETURN <<2943.7219, 4640.2212, 47.5447>>
				BREAK
				CASE 2
					RETURN <<2915.0530, 4616.5332, 47.3302>>
				BREAK
				CASE 3
					RETURN <<2941.7781, 4654.9277, 47.5449>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 46		//Zancudo Gasworks
			SWITCH iCoordID
				CASE 0
					RETURN <<-1654.6954, 3043.8867, 30.8236>>
				BREAK
				CASE 1
					RETURN <<-1661.3177, 3114.8389, 30.7181>>
				BREAK
				CASE 2
					RETURN <<-1618.9136, 3115.7954, 30.5577>>
				BREAK
				CASE 3
					RETURN <<-1673.0116, 3084.1462, 30.5473>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 47		//Palomino Bay
			SWITCH iCoordID
				CASE 0
					RETURN <<2873.6372, -669.3932, 0.0>>
				BREAK
				CASE 1
					RETURN <<2870.9971, -614.9962, 0.0>>
				BREAK
				CASE 2
					RETURN <<2878.0408, -584.0059, -4.0223>>
				BREAK
				CASE 3
					RETURN <<2879.3530, -705.5502, -2.5937>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 48		//El Gordo Lighthouse
			SWITCH iCoordID
				CASE 0
					RETURN <<3385.1621, 5187.8013, 0.0>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 49		//Government Facility
			SWITCH iCoordID
				CASE 0
					RETURN <<2511.6997, -342.5626, 117.1864>>
				BREAK
				CASE 1
					RETURN <<2512.2380, -427.2642, 117.1888>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 50		//Dashound Bus Centre
			SWITCH iCoordID
				CASE 0
					RETURN <<401.4161, -629.7783, 27.5002>>
				BREAK
				CASE 1
					RETURN <<441.2266, -572.7964, 27.4998>>
				BREAK
				CASE 2
					RETURN <<474.7617, -587.5326, 27.4997>>
				BREAK
				CASE 3
					RETURN <<454.1134, -592.4307, 27.4998>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 51		//Palmer-Taylor Power Station
			SWITCH iCoordID
				CASE 0
					RETURN <<2677.9473, 1392.4503, 23.5557>>
				BREAK
				CASE 1
					RETURN <<2769.8806, 1478.7505, 23.5236>>
				BREAK
				CASE 2
					RETURN <<2673.7827, 1671.0531, 23.4886>>
				BREAK
				CASE 3
					RETURN <<2795.7600, 1654.6404, 23.4886>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 52		//Great Chaparral Farm
			SWITCH iCoordID
				CASE 0
					RETURN <<-49.5069, 1965.8479, 188.7261>>
				BREAK
				CASE 1
					RETURN <<-52.8253, 1949.4172, 189.1861>>
				BREAK
				CASE 2
					RETURN <<-143.8556, 1894.9980, 196.3254>>
				BREAK
				CASE 3
					RETURN <<-128.0658, 1931.7496, 195.4646>>
				BREAK
				CASE 4
					RETURN <<-57.6033, 1913.4247, 194.3614>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 53		//Bayview Lodge
			SWITCH iCoordID
				CASE 0
					RETURN <<-678.5846, 5795.7666, 16.3310>>
				BREAK
				CASE 1
					RETURN <<-671.1857, 5840.7593, 16.3313>>
				BREAK
				CASE 2
					RETURN <<-724.7985, 5821.6597, 16.2562>>
				BREAK
				CASE 3
					RETURN <<-696.0164, 5770.6606, 16.3310>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 54 	//Airport (Upper Runway)
			SWITCH iCoordID
				CASE 0
					RETURN <<-911.9402, -3288.8486, 12.9444>>
				BREAK
				CASE 1
					RETURN <<-954.3091, -3363.9397, 12.9444>>
				BREAK
				CASE 2
					RETURN <<-869.6125, -3218.1418, 12.9444>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 55		//Airport (Construction)
			SWITCH iCoordID
				CASE 0
					RETURN <<-987.2319, -3414.7571, 12.8401>>
				BREAK
				CASE 1
					RETURN <<-1015.5156, -3414.7571, 12.8401>>
				BREAK
				CASE 2
					RETURN <<-955.7935, -3527.4592, 13.0788>>
				BREAK
				CASE 3
					RETURN <<-1027.5001, -3525.8320, 13.1484>>
				BREAK
				CASE 4
					RETURN <<-1088.2190, -3509.0085, 13.1484>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 56		//Airport (Lower Runway)
			SWITCH iCoordID
				CASE 0
					RETURN <<-1259.8000, -2176.3967, 12.9446>>
				BREAK
				CASE 1
					RETURN <<-1252.2009, -2244.8457, 12.9451>>
				BREAK
				CASE 2
					RETURN <<-1325.7961, -2192.6245, 12.9448>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 57		//Airport (Gates)
			SWITCH iCoordID
				CASE 0
					RETURN <<-1468.9028, -2730.9407, 12.9449>>
				BREAK
				CASE 1
					RETURN <<-1432.6989, -2667.5483, 12.9449>>
				BREAK
				CASE 2
					RETURN <<-1404.6173, -2793.9099, 12.9449>>
				BREAK
				CASE 3
					RETURN <<-1325.0681, -2838.7476, 12.9449>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 58		//Alamo Sea Airstrip (Mid)
			SWITCH iCoordID
				CASE 0
					RETURN <<1402.1692, 3000.3640, 39.5511>>
				BREAK
				CASE 1
					RETURN <<1473.4072, 3120.1279, 39.5341>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 59	//	LSIA - Central
			SWITCH iCoordID
				CASE 0	RETURN <<-1130.9268, -2420.5818, 12.9452>>	BREAK
				CASE 1	RETURN <<-1246.8815, -2279.6345, 12.9445>>	BREAK
				CASE 2	RETURN <<-1336.8336, -2464.5513, 12.9452>>	BREAK
				CASE 3	RETURN <<-1193.2859, -2584.1899, 12.9452>>	BREAK
				CASE 4	RETURN <<-1438.4240, -2664.2854, 12.9449>>	BREAK
				CASE 5	RETURN <<-1419.1395, -2544.8435, 12.9451>>	BREAK
			ENDSWITCH
		BREAK
		CASE 60	//	LSIA - Runway - North
			SWITCH iCoordID
				CASE 0	RETURN <<-1503.9265, -2497.7522, 12.9444>>	BREAK
				CASE 1	RETURN <<-1459.6239, -2422.5613, 12.9451>>	BREAK
				CASE 2	RETURN <<-1420.0847, -2355.0066, 12.9451>>	BREAK
				CASE 3	RETURN <<-1380.0096, -2288.2041, 12.9448>>	BREAK
				CASE 4	RETURN <<-1341.4946, -2221.8513, 12.9448>>	BREAK
			ENDSWITCH
		BREAK
		CASE 61	//	LSIA - West
			SWITCH iCoordID
				CASE 0	RETURN <<-1464.9388, -2933.6392, 13.3136>>	BREAK
				CASE 1	RETURN <<-1502.6829, -2989.0210, 13.3138>>	BREAK
				CASE 2	RETURN <<-1616.9141, -2837.1240, 13.3135>>	BREAK
				CASE 3	RETURN <<-1554.2789, -3065.3960, 13.1468>>	BREAK
				CASE 4	RETURN <<-1747.2324, -3073.1743, 13.1912>>	BREAK
				CASE 5	RETURN <<-1807.6337, -3037.5349, 13.1465>>	BREAK
				CASE 6	RETURN <<-1826.0834, -2935.8301, 13.1465>>	BREAK
			ENDSWITCH
		BREAK
		CASE 62	//	LSIA - South
			SWITCH iCoordID
				CASE 0	RETURN <<-886.0055, -3310.2036, 13.0472>>	BREAK
				CASE 1	RETURN <<-940.1698, -3228.7964, 13.3136>>	BREAK
				CASE 2	RETURN <<-1137.0992, -3195.8445, 13.2978>>	BREAK
				CASE 3	RETURN <<-1189.4568, -3085.9675, 13.0472>>	BREAK
				CASE 4	RETURN <<-1315.5696, -3000.7571, 13.3025>>	BREAK
			ENDSWITCH
		BREAK
		CASE 63	//	SS Airfield - East
			SWITCH iCoordID
				CASE 0	RETURN <<1678.9359, 3243.9736, 39.7616>>	BREAK
				CASE 1	RETURN <<1604.2278, 3223.5203, 39.4116>>	BREAK
				CASE 2	RETURN <<1531.5830, 3203.5911, 39.4116>>	BREAK
				CASE 3	RETURN <<1565.2982, 3164.1462, 39.5316>>	BREAK
			ENDSWITCH
		BREAK
		CASE 64	//	SS Airfield - South
			SWITCH iCoordID
				CASE 0	RETURN <<1511.1600, 3109.9587, 39.5316>>	BREAK
				CASE 1	RETURN <<1458.4928, 3057.2214, 39.5316>>	BREAK
				CASE 2	RETURN <<1405.4327, 3004.8906, 39.5444>>	BREAK
				CASE 3	RETURN <<1464.7404, 3118.8740, 39.5342>>	BREAK
			ENDSWITCH
		BREAK
		CASE 65	//	SS Airfield - Central
			SWITCH iCoordID
				CASE 0	RETURN <<1455.2269, 3183.4934, 39.4141>>	BREAK
				CASE 1	RETURN <<1378.6233, 3162.7925, 39.4141>>	BREAK
				CASE 2	RETURN <<1301.1919, 3141.4705, 39.4141>>	BREAK
				CASE 3	RETURN <<1229.7823, 3122.5171, 39.4141>>	BREAK
			ENDSWITCH
		BREAK
		CASE 66	//	SS Airfield - West
			SWITCH iCoordID
				CASE 0	RETURN <<1154.4531, 3102.0603, 39.4141>>	BREAK
				CASE 1	RETURN <<1078.2600, 3080.7129, 39.5790>>	BREAK
				CASE 2	RETURN <<1286.7186, 3070.7073, 39.5342>>	BREAK
				CASE 3	RETURN <<1216.2072, 3050.8516, 39.5341>>	BREAK
				CASE 4	RETURN <<1140.9495, 3029.6113, 39.4648>>	BREAK
			ENDSWITCH
		BREAK
		CASE 67	//	McKenzie Airfield
			SWITCH iCoordID
				CASE 0	RETURN <<2079.8960, 4782.5713, 40.2002>>	BREAK
				CASE 1	RETURN <<2016.1016, 4753.4526, 40.0604>>	BREAK
			ENDSWITCH
		BREAK
		CASE 68	//	ULSA
			SWITCH iCoordID
				CASE 0	RETURN <<-1722.1332, 166.6716, 63.3710>>	BREAK
			ENDSWITCH
		BREAK
		CASE 69	//	Del Perro
			SWITCH iCoordID
				CASE 0	RETURN <<-1819.0150, -734.5273, 8.1408>>	BREAK
				CASE 1	RETURN <<-1446.4752, -1239.2555, 2.2830>>	BREAK
				CASE 2	RETURN <<-1185.5043, -1726.0905, 3.3115>>	BREAK
			ENDSWITCH
		BREAK
		CASE 70	//	Paleto Bay
			SWITCH iCoordID
				CASE 0	RETURN <<-311.7762, 6511.5908, 2.2027>>	BREAK
				CASE 1	RETURN <<-402.0328, 6458.5898, 2.2895>>	BREAK
			ENDSWITCH
		BREAK
		CASE 71	//	Sandy Shores
			SWITCH iCoordID
				CASE 0	RETURN <<1443.4319, 3539.5364, 34.8291>>	BREAK
				CASE 1	RETURN <<2217.6877, 3775.7876, 34.4470>>	BREAK
			ENDSWITCH
		BREAK
		CASE 72		//Terminal
			SWITCH iCoordID
				CASE 0
					RETURN <<1091.9177, -3099.8457, 4.8970>>
				BREAK
				CASE 1
					RETURN <<1112.9907, -3099.1272, 4.8752>>
				BREAK
				CASE 2
					RETURN <<1129.5830, -3080.4741, 4.8185>>
				BREAK
				CASE 3
					RETURN <<1066.6133, -3072.6599, 4.9010>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 73		//Elysian Island
			SWITCH iCoordID
				CASE 0
					RETURN <<39.9872, -2479.0266, 5.0068>>
				BREAK
				CASE 1
					RETURN <<90.5073, -2479.2559, 5.0014>>
				BREAK
				CASE 2
					RETURN <<-52.8261, -2415.6008, 5.0002>>
				BREAK
				CASE 3
					RETURN <<55.9511, -2528.6309, 5.0048>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 74		//El Burro Heights
			SWITCH iCoordID
				CASE 0
					RETURN <<1628.5454, -1882.2861, 104.5179>>
				BREAK
				CASE 1
					RETURN <<1581.9283, -1833.1985, 92.8489>>
				BREAK
				CASE 2
					RETURN <<1676.6027, -1860.8796, 107.3806>>
				BREAK
				CASE 3
					RETURN <<1607.5465, -1952.4623, 100.3355>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 75		//Cypress Flats
			SWITCH iCoordID
				CASE 0
					RETURN <<745.3156, -1956.3079, 28.2918>>
				BREAK
				CASE 1
					RETURN <<743.4697, -2012.8807, 28.2908>>
				BREAK
				CASE 2
					RETURN <<736.3544, -1939.9500, 28.2920>>
				BREAK
				CASE 3
					RETURN <<728.9971, -1986.1466, 28.2920>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 76		//Tatavium Mountains
			SWITCH iCoordID
				CASE 0
					RETURN <<2566.2424, 396.0424, 107.4626>>
				BREAK
				CASE 1
					RETURN <<2553.2522, 439.8982, 107.4528>>
				BREAK
				CASE 2
					RETURN <<2588.6858, 414.8714, 107.4568>>
				BREAK
				CASE 3
					RETURN <<2554.3843, 343.8869, 107.4675>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 77		//Rex Diner
			SWITCH iCoordID
				CASE 0
					RETURN <<2526.0322, 2609.6711, 36.9449>>
				BREAK
				CASE 1
					RETURN <<2529.3511, 2635.1079, 36.9507>>
				BREAK
				CASE 2
					RETURN <<2532.9399, 2581.2747, 36.9449>>
				BREAK
				CASE 3
					RETURN <<2552.2705, 2638.0447, 37.0085>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 78		//You Tool
			SWITCH iCoordID
				CASE 0
					RETURN <<2758.8118, 3467.9751, 54.7159>>
				BREAK
				CASE 1
					RETURN <<2733.7463, 3415.1140, 55.7100>>
				BREAK
				CASE 2
					RETURN <<2774.6243, 3519.9812, 53.6847>>
				BREAK
				CASE 3
					RETURN <<2709.4685, 3429.7905, 54.6707>>
				BREAK
			ENDSWITCH
		BREAK
		CASE 79		//Up n Atom
			SWITCH iCoordID
				CASE 0
					RETURN <<1556.5394, 6463.3491, 22.4280>>
				BREAK
				CASE 1
					RETURN <<1606.0797, 6451.6855, 24.2255>>
				BREAK
				CASE 2
					RETURN <<1615.6696, 6428.8340, 25.5404>>
				BREAK
				CASE 3
					RETURN <<1550.2209, 6486.7070, 21.8521>>
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN <<0,0,0>>
ENDFUNC


/// PURPOSE:
///    Gets a vehicle spawn heading at a specified vehicle spawn point
/// PARAMS:
///    iVSID - vehicle spawn point ID to get a heading from
///    iHeadingID - heading ID to get heading of - maximum amount from GET_NUMBER_OF_VEHICLE_SPAWN_POINT_COORDS
/// RETURNS:
///    vehicle spawn heading as a FLOAT
FUNC FLOAT GET_VEHICLE_SPAWN_POINT_HEADING(INT iVSID, INT iHeadingID)
	SWITCH iVSID
		CASE 0		//Del Perro Pier
			SWITCH iHeadingID
				CASE 0
					RETURN 128.3942
				BREAK
				CASE 1
					RETURN 126.3376
				BREAK
				CASE 2
					RETURN 297.7065
				CASE 3
					RETURN 141.7598
				BREAK
			ENDSWITCH
		BREAK
		CASE 1		//Puerto Del Sol Water
			SWITCH iHeadingID
				CASE 0
					RETURN 48.7493
				BREAK
				CASE 1
					RETURN 198.9416
				BREAK
				CASE 2
					RETURN 289.8739
				BREAK
				CASE 3
					RETURN 200.5427
				BREAK
				CASE 4
					RETURN 108.7796
				BREAK
				CASE 5
					RETURN 231.5205
				BREAK
				CASE 6
					RETURN 113.5552
				BREAK
			ENDSWITCH
		BREAK
		CASE 2		//Sandy Shores
			SWITCH iHeadingID
				CASE 0
					RETURN 54.4917
				BREAK
				CASE 1
					RETURN 46.0841
				BREAK
				CASE 2
					RETURN 86.6669
				BREAK
				CASE 3
					RETURN 354.5429
				BREAK
			ENDSWITCH
		BREAK
		CASE 3		//Paleto Bay
			SWITCH iHeadingID
				CASE 0
					RETURN 18.5108
				BREAK
				CASE 1
					RETURN 104.1463
				BREAK
				CASE 2
					RETURN 85.4181
				BREAK
				CASE 3
					RETURN 9.0785
				BREAK
			ENDSWITCH
		BREAK
		CASE 4		//Puerto Del Sol Heliport
			SWITCH iHeadingID
				CASE 0
					RETURN 141.0742
				BREAK
				CASE 1
					RETURN 227.2749
				BREAK
				CASE 2
					RETURN 47.0297
				BREAK
				CASE 3
					RETURN 316.7026
				BREAK
			ENDSWITCH
		BREAK
		CASE 5		//Airport Helipad
			SWITCH iHeadingID
				CASE 0
					RETURN 328.7320
				BREAK
				CASE 1
					RETURN 329.0177
				BREAK
				CASE 2
					RETURN 330.9617
				BREAK
				CASE 3
					RETURN 330.4895
				BREAK
			ENDSWITCH
		BREAK
		CASE 6		//Paleto Police Station Heli
			SWITCH iHeadingID
				CASE 0
					RETURN 311.0102
				BREAK
				CASE 1
					RETURN 33.6118
				BREAK
			ENDSWITCH
		BREAK
		CASE 7		//Alamo Sea Helipad
			SWITCH iHeadingID
				CASE 0
					RETURN 281.0591
				BREAK
				CASE 1
					RETURN 90.7711
				BREAK
				CASE 2
					RETURN 73.0547
				BREAK
				CASE 3
					RETURN 74.2989
				BREAK
			ENDSWITCH
		BREAK
		CASE 8		//Alamo Sea Airstrip
			SWITCH iHeadingID
				CASE 0
					RETURN 194.2706
				BREAK
				CASE 1
					RETURN 60.7644
				BREAK
			ENDSWITCH
		BREAK
		CASE 9		//Merryweather Dock Car
			SWITCH iHeadingID
				CASE 0
					RETURN 269.9959
				BREAK
				CASE 1
					RETURN 90.0267
				BREAK
				CASE 2
					RETURN 268.5896
				BREAK
				CASE 3
					RETURN 358.5432
				BREAK
			ENDSWITCH
		BREAK
		CASE 10		//Merryweather Dock Boat
			SWITCH iHeadingID
				CASE 0
					RETURN 176.2578 
				BREAK
				CASE 1
					RETURN 178.7439 
				BREAK
				CASE 2
					RETURN 178.7833
				BREAK
				CASE 3
					RETURN 182.9564
				BREAK
			ENDSWITCH
		BREAK
		CASE 11		//Studio Carpark
			SWITCH iHeadingID
				CASE 0
					RETURN 207.6093
				BREAK
				CASE 1
					RETURN 297.1650
				BREAK
				CASE 2
					RETURN 297.3540
				BREAK
				CASE 3
					RETURN 296.2361
				BREAK
			ENDSWITCH
		BREAK
		CASE 12		//Desert Rest Stop
			SWITCH iHeadingID
				CASE 0
					RETURN 254.9279
				BREAK
				CASE 1
					RETURN 71.8704
				BREAK
				CASE 2
					RETURN 68.4953
				BREAK
				CASE 3
					RETURN 267.2875
				BREAK
			ENDSWITCH
		BREAK
		CASE 13		//Paleto Ammunation
			SWITCH iHeadingID
				CASE 0
					RETURN 223.7918
				BREAK
				CASE 1
					RETURN 17.6402
				BREAK
				CASE 2
					RETURN 226.2552
				BREAK
				CASE 3
					RETURN 226.7396
				BREAK
			ENDSWITCH
		BREAK
		CASE 14		//Airport (Upper)
			SWITCH iHeadingID
				CASE 0
					RETURN 108.2881
				BREAK
				CASE 1
					RETURN 63.4159
				BREAK
				CASE 2
					RETURN 238.7379
				BREAK
				CASE 3
					RETURN 61.5823
				BREAK
			ENDSWITCH
		BREAK
		CASE 15		//Airport (Lower)
			SWITCH iHeadingID
				CASE 0
					RETURN 295.6942
				BREAK
				CASE 1
					RETURN 352.7602
				BREAK
			ENDSWITCH
		BREAK
		CASE 16		//Grapeseed Airstrip
			SWITCH iHeadingID
				CASE 0
					RETURN 26.9609
				BREAK
				CASE 1
					RETURN 81.5986
				BREAK
			ENDSWITCH
		BREAK
		CASE 17		//Del Perro Beach
			SWITCH iHeadingID
				CASE 0
					RETURN 262.7139
				BREAK
				CASE 1
					RETURN 232.9363
				BREAK
				CASE 2
					RETURN 232.3536
				BREAK
				CASE 3
					RETURN 233.2846
				BREAK
			ENDSWITCH
		BREAK
		CASE 18		//Racetrack
			SWITCH iHeadingID
				CASE 0
					RETURN 269.5952
				BREAK
				CASE 1
					RETURN 57.4738
				BREAK
				CASE 2
					RETURN 296.6961
				BREAK
				CASE 3
					RETURN 236.5353
				BREAK
			ENDSWITCH
		BREAK
		CASE 19		//Grapeseed Farm
			SWITCH iHeadingID
				CASE 0
					RETURN 19.2357
				BREAK
				CASE 1
					RETURN 66.2577
				BREAK
				CASE 2
					RETURN 95.9687
				BREAK
				CASE 3
					RETURN 90.1139
				BREAK
			ENDSWITCH
		BREAK
		CASE 20		//Harmong Farm
			SWITCH iHeadingID
				CASE 0
					RETURN 280.1033
				BREAK
				CASE 1
					RETURN 328.2375
				BREAK
				CASE 2
					RETURN 253.5810
				BREAK
				CASE 3
					RETURN 171.8541
				BREAK
			ENDSWITCH
		BREAK
		CASE 21		//Chumash Beach
			SWITCH iHeadingID
				CASE 0
					RETURN 45.3565
				BREAK
				CASE 1
					RETURN 56.6296
				BREAK
				CASE 2
					RETURN 79.0783
				BREAK
				CASE 3
					RETURN 80.3808
				BREAK
			ENDSWITCH
		BREAK
		CASE 22		//Country Pier
			SWITCH iHeadingID
				CASE 0
					RETURN 271.0457
				BREAK
				CASE 1
					RETURN 266.7824
				BREAK
				CASE 2
					RETURN 27.9728
				BREAK
				CASE 3
					RETURN 320.9822
				BREAK
			ENDSWITCH
		BREAK
		CASE 23		//Port of LS Helipad
			SWITCH iHeadingID
				CASE 0
					RETURN 357.9262
				BREAK
				CASE 1
					RETURN 179.5456
				BREAK
				CASE 2
					RETURN 0.8311
				BREAK
				CASE 3
					RETURN 1.7635
				BREAK
			ENDSWITCH
		BREAK
		CASE 24		//South LS Impound
			SWITCH iHeadingID
				CASE 0
					RETURN 319.9087
				BREAK
			ENDSWITCH
		BREAK
		CASE 25		//Vespucci Police Station
			SWITCH iHeadingID
				CASE 0
					RETURN 127.4169
				BREAK
			ENDSWITCH
		BREAK
		CASE 26		//Sandy Shores Police Station Car
			SWITCH iHeadingID
				CASE 0
					RETURN 178.1762
				BREAK
			ENDSWITCH
		BREAK
		CASE 27		//Paleto Police Station Car
			SWITCH iHeadingID
				CASE 0
					RETURN 223.3915
				BREAK
			ENDSWITCH
		BREAK
		CASE 28		//Country Pier 2
			SWITCH iHeadingID
				CASE 0
					RETURN 242.5401
				BREAK
				CASE 1
					RETURN 288.2003
				BREAK
				CASE 2
					RETURN 279.8115
				BREAK
				CASE 3
					RETURN 208.2498
				BREAK
			ENDSWITCH
		BREAK
		CASE 29		//Chilead Bay
			SWITCH iHeadingID
				CASE 0
					RETURN 26.4664
				BREAK
				CASE 1
					RETURN 14.9960
				BREAK
				CASE 2
					RETURN 29.2074
				BREAK
			ENDSWITCH
		BREAK
		CASE 30		//Airport (Hangar)
			SWITCH iHeadingID
				CASE 0
					RETURN 330.5493
				BREAK
				CASE 1
					RETURN 60.3130
				BREAK
			ENDSWITCH
		BREAK
		CASE 31		//Downtown Site
			SWITCH iHeadingID
				CASE 0
					RETURN 210.3199
				BREAK
				CASE 1
					RETURN 184.9920
				BREAK
				CASE 2
					RETURN 336.1376
				BREAK
				CASE 3
					RETURN 71.1016
				BREAK
			ENDSWITCH
		BREAK
		CASE 32		//Little Seoul Site
			SWITCH iHeadingID
				CASE 0
					RETURN 89.7232
				BREAK
				CASE 1
					RETURN 89.9283
				BREAK
				CASE 2
					RETURN 181.3048
				BREAK
				CASE 3
					RETURN 89.6032
				BREAK
			ENDSWITCH
		BREAK
		CASE 33		//Grand Senora Site
			SWITCH iHeadingID
				CASE 0
					RETURN 162.5932
				BREAK
				CASE 1
					RETURN 358.3655
				BREAK
				CASE 2
					RETURN 217.4120
				BREAK
				CASE 3
					RETURN 87.4223
				BREAK

			ENDSWITCH
		BREAK
		CASE 34		//Paleto Site
			SWITCH iHeadingID
				CASE 0
					RETURN 196.5324
				BREAK
				CASE 1
					RETURN 41.3923
				BREAK
				CASE 2
					RETURN 221.6984
				BREAK
				CASE 3
					RETURN 207.9563
				BREAK
			ENDSWITCH
		BREAK
		CASE 35		//Military Base
			SWITCH iHeadingID
				CASE 0
					RETURN 60.0988
				BREAK
				CASE 1
					RETURN 326.8047
				BREAK
				CASE 2
					RETURN 330.1870
				BREAK
				CASE 3
					RETURN 329.5951
				BREAK
				CASE 4
					RETURN 328.4178
				BREAK
			ENDSWITCH
		BREAK
		CASE 36		//Oriental Theatre
			SWITCH iHeadingID
				CASE 0
					RETURN 70.9345
				BREAK
				CASE 1
					RETURN 68.5237
				BREAK
				CASE 2
					RETURN 341.2603
				BREAK
				CASE 3
					RETURN 70.0476
				BREAK
			ENDSWITCH
		BREAK
		CASE 37		//Vinewood
			SWITCH iHeadingID
				CASE 0
					RETURN 286.6174
				BREAK
				CASE 1
					RETURN 181.1843
				BREAK
				CASE 2
					RETURN 180.3838
				BREAK
				CASE 3
					RETURN 179.6852
				BREAK
			ENDSWITCH
		BREAK
		CASE 38		//Madrazo's Mansion
			SWITCH iHeadingID
				CASE 0
					RETURN 89.9501
				BREAK
				CASE 1
					RETURN 90.9095
				BREAK
				CASE 2
					RETURN 90.6976
				BREAK
				CASE 3
					RETURN 181.1100
				BREAK
			ENDSWITCH
		BREAK
		CASE 39		//Vinyard
			SWITCH iHeadingID
				CASE 0
					RETURN 70.5529
				BREAK
				CASE 1
					RETURN 168.2520
				BREAK
				CASE 2
					RETURN 163.5166
				BREAK
				CASE 3
					RETURN 8.6161
				BREAK
			ENDSWITCH
		BREAK
		CASE 40		//Chumash Pier
			SWITCH iHeadingID
				CASE 0
					RETURN 91.3514
				BREAK
				CASE 1
					RETURN 185.1698
				BREAK
				CASE 2
					RETURN 90.4621
				BREAK
				CASE 3
					RETURN 92.1552
				BREAK
			ENDSWITCH
		BREAK
		CASE 41		//Alamo Sea Pier
			SWITCH iHeadingID
				CASE 0
					RETURN 179.8932
				BREAK
				CASE 1
					RETURN 176.2116
				BREAK
				CASE 2
					RETURN 222.7799
				BREAK
				CASE 3
					RETURN 267.6429
				BREAK
			ENDSWITCH
		BREAK
		CASE 42		//Airport (Executive Hangar)
			SWITCH iHeadingID
				CASE 0
					RETURN 59.3762
				BREAK
				CASE 1
					RETURN 59.3762
				BREAK
				CASE 2
					RETURN 150.0709
				BREAK
				CASE 3
					RETURN 106.7946
				BREAK
				CASE 4
					RETURN 151.1216
				BREAK
			ENDSWITCH
		BREAK
		CASE 43		//Central Hospital Helipad
			SWITCH iHeadingID
				CASE 0
					RETURN 318.5715
				BREAK
				CASE 1
					RETURN 319.2958
				BREAK
				CASE 2
					RETURN 49.7571
				BREAK
			ENDSWITCH
		BREAK
		CASE 44		//Paleto Bay Salvage
			SWITCH iHeadingID
				CASE 0
					RETURN 37.0057
				BREAK
				CASE 1
					RETURN 314.6925
				BREAK
				CASE 2
					RETURN 41.9311
				BREAK
				CASE 3
					RETURN 45.0685
				BREAK
			ENDSWITCH
		BREAK
		CASE 45		//San Chianski Farm
			SWITCH iHeadingID
				CASE 0
					RETURN 316.0815
				BREAK
				CASE 1
					RETURN 48.8681
				BREAK
				CASE 2
					RETURN 44.5158
				BREAK
				CASE 3
					RETURN 314.9323
				BREAK
			ENDSWITCH
		BREAK
		CASE 46		//Zancudo Gasworks
			SWITCH iHeadingID
				CASE 0
					RETURN 290.6283 
				BREAK
				CASE 1
					RETURN 179.5908
				BREAK
				CASE 2
					RETURN 128.0372
				BREAK
				CASE 3
					RETURN 221.3842
				BREAK
			ENDSWITCH
		BREAK
		CASE 47		//Palomino Bay
			SWITCH iHeadingID
				CASE 0
					RETURN 313.5646
				BREAK
				CASE 1
					RETURN 271.7294
				BREAK
				CASE 2
					RETURN 285.8962
				BREAK
				CASE 3
					RETURN 260.7833
				BREAK
			ENDSWITCH
		BREAK
		CASE 48		//El Gordo Lighthouse
			SWITCH iHeadingID
				CASE 0
					RETURN 264.2661
				BREAK
			ENDSWITCH
		BREAK
		CASE 49		//Government Facility
			SWITCH iHeadingID
				CASE 0
					RETURN 45.2840
				BREAK
				CASE 1
					RETURN 47.2513
				BREAK
			ENDSWITCH
		BREAK
		CASE 50		//Dashound Bus Centre
			SWITCH iHeadingID
				CASE 0
					RETURN 333.0959
				BREAK
				CASE 1
					RETURN 149.2864
				BREAK
				CASE 2
					RETURN 173.8146
				BREAK
				CASE 3
					RETURN 264.2520
				BREAK
			ENDSWITCH
		BREAK
		CASE 51		//Palmer-Taylor Power Station
			SWITCH iHeadingID
				CASE 0
					RETURN 0.2415
				BREAK
				CASE 1
					RETURN 162.6802
				BREAK
				CASE 2
					RETURN 268.9426
				BREAK
				CASE 3
					RETURN 89.0153
				BREAK
			ENDSWITCH
		BREAK
		CASE 52		//Great Chaparral Farm
			SWITCH iHeadingID
				CASE 0
					RETURN 297.1316
				BREAK
				CASE 1
					RETURN 27.0734
				BREAK
				CASE 2
					RETURN 268.7530
				BREAK
				CASE 3
					RETURN 359.9451
				BREAK
				CASE 4
					RETURN 95.3986
				BREAK
			ENDSWITCH
		BREAK
		CASE 53		//Bayview Lodge
			SWITCH iHeadingID
				CASE 0
					RETURN 243.3595
				BREAK
				CASE 1
					RETURN 46.2343
				BREAK
				CASE 2
					RETURN 264.9337
				BREAK
				CASE 3
					RETURN 67.2050
				BREAK
			ENDSWITCH
		BREAK
		CASE 54		//Airport (Upper Runway)
			SWITCH iHeadingID
				CASE 0
					RETURN 60.6503
				BREAK
				CASE 1
					RETURN 59.6904
				BREAK
				CASE 2
					RETURN 60.0332
				BREAK
			ENDSWITCH
		BREAK
		CASE 55		//Airport (Construction)
			SWITCH iHeadingID
				CASE 0
					RETURN 330.3975
				BREAK
				CASE 1
					RETURN 328.2082
				BREAK
				CASE 2
					RETURN 328.7614
				BREAK
				CASE 3
					RETURN 16.6396
				BREAK
				CASE 4
					RETURN 329.3139
				BREAK
			ENDSWITCH
		BREAK
		CASE 56		//Airport (Lower Runway)
			SWITCH iHeadingID
				CASE 0
					RETURN 149.9306
				BREAK
				CASE 1
					RETURN 91.1942
				BREAK
				CASE 2
					RETURN 149.7415
				BREAK
			ENDSWITCH
		BREAK
		CASE 57		//Airport (Gates)
			SWITCH iHeadingID
				CASE 0
					RETURN 240.1494
				BREAK
				CASE 1
					RETURN 240.9740
				BREAK
				CASE 2
					RETURN 149.6880
				BREAK
				CASE 3
					RETURN 150.7797
				BREAK
			ENDSWITCH
		BREAK
		CASE 58		//Alamo Sea Airstrip (Mid)
			SWITCH iHeadingID
				CASE 0
					RETURN 314.5487
				BREAK
				CASE 1
					RETURN 104.3043
				BREAK
			ENDSWITCH
		BREAK
		CASE 59	//	LSIA - Central
			SWITCH iHeadingID
				CASE 0RETURN 61.7987	BREAK
				CASE 1	RETURN 61.7987	BREAK
				CASE 2	RETURN 61.1987	BREAK
				CASE 3	RETURN 61.1987	BREAK
				CASE 4	RETURN 60.1985	BREAK
				CASE 5	RETURN 329.3985	BREAK
			ENDSWITCH
		BREAK
		CASE 60	//	LSIA - Runway - North
			SWITCH iHeadingID
				CASE 0	RETURN 329.5986	BREAK
				CASE 1	RETURN 329.5986	BREAK
				CASE 2	RETURN 329.5986	BREAK
				CASE 3	RETURN 329.5986	BREAK
				CASE 4	RETURN 329.5986	BREAK
			ENDSWITCH
		BREAK
		CASE 61	//	LSIA - West
			SWITCH iHeadingID
				CASE 0	RETURN 60.8000	BREAK
				CASE 1	RETURN 60.8000	BREAK
				CASE 2	RETURN 60.8000	BREAK
				CASE 3	RETURN 60.8000	BREAK
				CASE 4	RETURN 329.9999	BREAK
				CASE 5	RETURN 329.9999	BREAK
				CASE 6	RETURN 329.9999	BREAK
			ENDSWITCH
		BREAK
		CASE 62	//	LSIA - South
			SWITCH iHeadingID
				CASE 0	RETURN 328.9999	BREAK
				CASE 1	RETURN 60.8000	BREAK
				CASE 2	RETURN 60.8000	BREAK
				CASE 3	RETURN 60.8000	BREAK
				CASE 4	RETURN 60.8000	BREAK
			ENDSWITCH
		BREAK
		CASE 63	//	SS Airfield - East
			SWITCH iHeadingID
				CASE 0	RETURN 104.9994	BREAK
				CASE 1	RETURN 104.9994	BREAK
				CASE 2	RETURN 104.9994	BREAK
				CASE 3	RETURN 134.3992	BREAK
			ENDSWITCH
		BREAK
		CASE 64	//	SS Airfield - South
			SWITCH iHeadingID
				CASE 0	RETURN 134.3992	BREAK
				CASE 1	RETURN 134.3992	BREAK
				CASE 2	RETURN 134.3992	BREAK
				CASE 3	RETURN 106.3988	BREAK
			ENDSWITCH
		BREAK
		CASE 65	//	SS Airfield - Central
			SWITCH iHeadingID
				CASE 0	RETURN 104.9994	BREAK
				CASE 1	RETURN 104.9994	BREAK
				CASE 2	RETURN 104.9994	BREAK
				CASE 3	RETURN 104.9994	BREAK
			ENDSWITCH
		BREAK
		CASE 66	//	SS Airfield - West
			SWITCH iHeadingID
				CASE 0	RETURN 104.9994	BREAK
				CASE 1	RETURN 104.9994	BREAK
				CASE 2	RETURN 106.3988	BREAK
				CASE 3	RETURN 106.3988	BREAK
				CASE 4	RETURN 106.3988	BREAK
			ENDSWITCH
		BREAK
		CASE 67	//	McKenzie Airfield
			SWITCH iHeadingID
				CASE 0	RETURN 112.5996	BREAK
				CASE 1	RETURN 114.9995	BREAK
			ENDSWITCH
		BREAK
		CASE 68	//	ULSA
			SWITCH iHeadingID
				CASE 0	RETURN 214.9980	BREAK
			ENDSWITCH
		BREAK
		CASE 69	//	Del Perro
			SWITCH iHeadingID
				CASE 0	RETURN 220.5979	BREAK
				CASE 1	RETURN 199.5980	BREAK
				CASE 2	RETURN 307.7978	BREAK
			ENDSWITCH
		BREAK
		CASE 70	//	Paleto Bay
			SWITCH iHeadingID
				CASE 0	RETURN 128.3974	BREAK
				CASE 1	RETURN 121.1973	BREAK
			ENDSWITCH
		BREAK
		CASE 71	//	Sandy Shores
			SWITCH iHeadingID
				CASE 0	RETURN 80.7957	BREAK
				CASE 1	RETURN 117.3954	BREAK
			ENDSWITCH
		BREAK
		CASE 72		//Terminal
			SWITCH iHeadingID
				CASE 0
					RETURN 90.0882
				BREAK
				CASE 1
					RETURN 179.3112
				BREAK
				CASE 2
					RETURN 91.4815
				BREAK
				CASE 3
					RETURN 1.0740
				BREAK
			ENDSWITCH
		BREAK
		CASE 73		//Elysian Island
			SWITCH iHeadingID
				CASE 0
					RETURN 144.7926
				BREAK
				CASE 1
					RETURN 54.3723
				BREAK
				CASE 2
					RETURN 269.6087
				BREAK
				CASE 3
					RETURN 325.9204
				BREAK
			ENDSWITCH
		BREAK
		CASE 74		//El Burro Heights
			SWITCH iHeadingID
				CASE 0
					RETURN 160.0324
				BREAK
				CASE 1
					RETURN 267.5354
				BREAK
				CASE 2
					RETURN 299.4446
				BREAK
				CASE 3
					RETURN 348.3851
				BREAK
			ENDSWITCH
		BREAK
		CASE 75		//Cypress Flats
			SWITCH iHeadingID
				CASE 0
					RETURN 265.5181
				BREAK
				CASE 1
					RETURN 264.2004
				BREAK
				CASE 2
					RETURN 173.7317
				BREAK
				CASE 3
					RETURN 174.4608
				BREAK
			ENDSWITCH
		BREAK
		CASE 76		//Tatavium Mountains
			SWITCH iHeadingID
				CASE 0
					RETURN 176.6031
				BREAK
				CASE 1
					RETURN 314.7473
				BREAK
				CASE 2
					RETURN 180.1860
				BREAK
				CASE 3
					RETURN 268.4422
				BREAK
			ENDSWITCH
		BREAK
		CASE 77		//Rex Diner
			SWITCH iHeadingID
				CASE 0
					RETURN 280.5076
				BREAK
				CASE 1
					RETURN 270.4875
				BREAK
				CASE 2
					RETURN 354.9289
				BREAK
				CASE 3
					RETURN 285.3475
				BREAK
			ENDSWITCH
		BREAK
		CASE 78		//You Tool
			SWITCH iHeadingID
				CASE 0
					RETURN 157.9527
				BREAK
				CASE 1
					RETURN 247.5947
				BREAK
				CASE 2
					RETURN 249.4453
				BREAK
				CASE 3
					RETURN 247.9698
				BREAK
			ENDSWITCH
		BREAK
		CASE 79		//Up n Atom
			SWITCH iHeadingID
				CASE 0
					RETURN 213.8149
				BREAK
				CASE 1
					RETURN 169.7626
				BREAK
				CASE 2
					RETURN 68.4215
				BREAK
				CASE 3
					RETURN 230.3543
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN 0.0
ENDFUNC

/// PURPOSE:
///    Checks if a specified model can spawn at a specified vehicle spawn point
/// PARAMS:
///    iVSID - vehicle spawn point ID to check
///    thisModel - vehicle model to check
/// RETURNS:
///    true/false
FUNC BOOL CHECK_SPECIAL_CASES_OF_VEHICLE_SPAWN_POINT(INT iVSID, MODEL_NAMES thisModel)
	SWITCH iVSID
		CASE 0		//Del Perro Pier
			IF thisModel <> SEASHARK
			AND thisModel <> SEASHARK2
				RETURN FALSE
			ENDIF
		BREAK
		CASE 38
			IF thisModel = RHINO
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	IF thisModel = BLIMP3
	AND iVSID < 60
		RETURN FALSE
	ELIF thisModel <> BLIMP3
	AND iVSID >= 60
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if a specified model can spawn at a specified spawn coordinate with specified vehicle spawn point
/// PARAMS:
///    iVSID - vehicle spawn point ID to check
///    iCoordID - vehicle spawn coordinate ID to check
///    thisModel - vehicle model to check
/// RETURNS:
///    true/false
FUNC BOOL CHECK_SPECIAL_CASES_OF_VEHICLE_SPAWN_COORDS(INT iVSID, INT iCoordID, MODEL_NAMES thisModel)
	SWITCH iVSID
		CASE 1		//Puerto Del Sol Water
			SWITCH iCoordID
				CASE 0
					IF thisModel = SUBMERSIBLE
						RETURN FALSE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 17		//Grapeseed Airstrip
			SWITCH iCoordID
				CASE 0
					IF thisModel = SHAMAL
						RETURN FALSE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF thisModel = RIOT
		INT PLAYERNUMBER
		REPEAT NUM_NETWORK_PLAYERS PLAYERNUMBER
			IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(PLAYERNUMBER))
			AND GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(INT_TO_PLAYERINDEX(PLAYERNUMBER)) = FMMC_TYPE_GB_CONTRABAND_BUY 
				RETURN FALSE
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CHECK_SPECIAL_CONDITIONS_OF_VEHICLE_SPAWN_POINT(INT iVSID, MODEL_NAMES thisModel)
	IF thisModel = BLIMP3
		VEHICLE_INDEX vehID = GET_CLOSEST_VEHICLE(GET_VEHICLE_SPAWN_POINT_CENTRE(iVSID), 20.0, thisModel, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_ALLOW_BLIMPS)
		IF DOES_ENTITY_EXIST(vehID)
			RETURN FALSE
		ENDIF
		
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns if the specified vehicle spawn point can be used for delivering pegasus and lester vehicles
/// PARAMS:
///    iVSID - vehicle spawn point ID to check
/// RETURNS:
///    true/false
FUNC BOOL IS_SPAWN_POINT_SUITABLE_FOR_DELIVERY(INT iVSID)
	SWITCH iVSID
		CASE 24		//South LS Impound
		CASE 25		//Vespucci Police Station
		CASE 26		//Sandy Shores Police Station Car
		CASE 27		//Paleto Police Station Car
			RETURN FALSE
		BREAK
		CASE 35		//Military Base
			IF DOES_PLAYER_OWN_AN_AB_HANGAR(PLAYER_ID())
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns if the specified vehicle spawn point can be used for submersibles
/// PARAMS:
///    iVSID - vehicle spawn point ID to check
/// RETURNS:
///    true/false
FUNC BOOL IS_SPAWN_POINT_SUITABLE_FOR_SUBMERSIBLE(INT iVSID)
	SWITCH iVSID
		CASE 3		//Paleto Bay
			RETURN FALSE
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Returns if a specified vehicle model is large enough that it requires extra room to be safe to spawn
/// PARAMS:
///    thisModel - vehicle model to check
/// RETURNS:
///    true/false
FUNC BOOL IS_VEHICLE_SUBMERSIBLE(MODEL_NAMES thisModel)
	IF thisModel = SUBMERSIBLE
	OR thisModel = SUBMERSIBLE2
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC	


/// PURPOSE:
///    Returns if the specified vehicle spawn coordinate is capable of spawning large vehicles
/// PARAMS:
///    iVSID - Vehicle spawn point ID to check
///    iCoordID - Vehicle spawn coordinate ID to check
/// RETURNS:
///    true/false
FUNC BOOL IS_VEHICLE_SPAWN_COORD_BIG(INT iVSID, INT iCoordID)
	SWITCH iVSID
		CASE 0		//Del Perro Pier
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN FALSE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 1		//Puerto Del Sol Water
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 2		//Sandy Shores
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 3		//Paleto Bay
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 4		//Puerto Del Sol Heliport
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 5		//Airport Helipad
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 6		//Paleto Police Station Heli
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 7		//Alamo Sea Helipad
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 8		//Alamo Sea Airstrip
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 9		//Merryweather Dock Car
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 10		//Merryweather Dock Boat
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 11		//Studio Carpark
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN FALSE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 12		//Desert Rest Stop
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 13		//Paleto Ammunation
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 14		//Airport (Upper)
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN FALSE
				BREAK
				CASE 2
					RETURN FALSE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 15		//Airport (Lower)
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 16		//Grapeseed Airstrip
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 17		//Del Perro Beach
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 18		//Racetrack
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 19		//Grapeseed Farm
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 20		//Harmong Farm
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN FALSE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 21		//Chumash Beach
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 22		//Country Pier
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 23		//Port of LS Helipad
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 24		//South LS Impound
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 25		//Vespucci Police Station
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 26		//Sandy Shores Police Station Car
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 27		//Paleto Police Station Car
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 28		//Country Pier 2
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 29		//Chilead Bay
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 30		//Airport (Hangar)
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 31		//Downtown Site
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN FALSE
				BREAK
				CASE 2
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 32		//Little Seoul Site
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 33		//Grand Senora Site
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 34		//Paleto Site
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 35		//Military Base
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN FALSE
				BREAK
				CASE 2
					RETURN FALSE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 36		//Oriental Theatre
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 37		//Vinewood
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 38		//Madrazo's Mansion
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 39		//Vinyard
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 40		//Chumash Pier
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 41		//Alamo Sea Pier
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 42		//Airport (Executive Hangar)
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 43		//Central Hospital Helipad
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 44		//Paleto Bay Salvage
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 45		//San Chianski Farm
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 46		//Zancudo Gasworks
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 47		//Palomino Bay
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 48		//El Gordo Lighthouse
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 49		//Government Facility
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 50		//Dashound Bus Centre
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 51		//Palmer-Taylor Power Station
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 52		//Great Chaparral Farm
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN FALSE
				BREAK
				CASE 2
					RETURN FALSE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 53		//Bayview Lodge
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 54		//Airport (Lower Runway)
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 55		//Airport (Construction)
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 56		//Airport (Lower Runway)
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 57		//Airport (Gates)
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 58		//Alamo Sea Airstrip (Mid)
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 59	//	LSIA - Central
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
				CASE 5
					RETURN TRUE
				BREAK
				CASE 6
					RETURN TRUE
				BREAK
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 60	//	LSIA - Runway - North
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
				CASE 5
					RETURN TRUE
				BREAK
				CASE 6
					RETURN TRUE
				BREAK
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 61	//	LSIA - West
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
				CASE 5
					RETURN TRUE
				BREAK
				CASE 6
					RETURN TRUE
				BREAK
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 62	//	LSIA - South
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
				CASE 5
					RETURN TRUE
				BREAK
				CASE 6
					RETURN TRUE
				BREAK
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 63	//	SS Airfield - East
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
				CASE 5
					RETURN TRUE
				BREAK
				CASE 6
					RETURN TRUE
				BREAK
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 64	//	SS Airfield - South
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
				CASE 5
					RETURN TRUE
				BREAK
				CASE 6
					RETURN TRUE
				BREAK
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 65	//	SS Airfield - Central
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
				CASE 5
					RETURN TRUE
				BREAK
				CASE 6
					RETURN TRUE
				BREAK
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 66	//	SS Airfield - West
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
				CASE 5
					RETURN TRUE
				BREAK
				CASE 6
					RETURN TRUE
				BREAK
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 67	//	McKenzie Airfield
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
				CASE 5
					RETURN TRUE
				BREAK
				CASE 6
					RETURN TRUE
				BREAK
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 68	//	ULSA
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
				CASE 5
					RETURN TRUE
				BREAK
				CASE 6
					RETURN TRUE
				BREAK
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 69	//	Del Perro
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
				CASE 5
					RETURN TRUE
				BREAK
				CASE 6
					RETURN TRUE
				BREAK
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 70	//	Paleto Bay
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
				CASE 5
					RETURN TRUE
				BREAK
				CASE 6
					RETURN TRUE
				BREAK
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 71	//	Sandy Shores
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
				CASE 4
					RETURN TRUE
				BREAK
				CASE 5
					RETURN TRUE
				BREAK
				CASE 6
					RETURN TRUE
				BREAK
				CASE 7
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 72		//Terminal
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 73		//Elysian Island
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 74		//El Burro Heights
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 75		//Cypress Flats
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 76		//Tatavium Mountains
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 77		//Rex Diner
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 78		//You Tool
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 79		//Up n Atom
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
				CASE 2
					RETURN TRUE
				BREAK
				CASE 3
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if the specified vehicle spawn coordinate is capable of spawning really large vehicles
/// PARAMS:
///    iVSID - Vehicle spawn point ID to check
///    iCoordID - Vehicle spawn coordinate ID to check
/// RETURNS:
///    true/false
FUNC BOOL IS_VEHICLE_SPAWN_COORD_REALLY_BIG(INT iVSID, INT iCoordID)
	SWITCH iVSID
		CASE 0		//Del Perro Pier
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN FALSE
				BREAK
				CASE 2
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 1		//Puerto Del Sol Water
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN FALSE
				BREAK
				CASE 2
					RETURN FALSE
				BREAK
				CASE 3
					RETURN FALSE
				BREAK
				CASE 4
					RETURN FALSE
				BREAK
				CASE 5
					RETURN FALSE
				BREAK
				CASE 6
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 2		//Sandy Shores
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 3		//Paleto Bay
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN FALSE
				BREAK
				CASE 2
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE 10		//Merryweather Dock Boat
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE 22		//Chumash Beach
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 23		//Country Pier
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
				CASE 1
					RETURN FALSE
				BREAK
				CASE 2
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE 29		//Country Pier 2
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		CASE 30		//Chilead Bay
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE 41		//Chumash Pier
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		
		CASE 48		//Palomino Bay
			SWITCH iCoordID
				CASE 0
					RETURN TRUE
				BREAK
				CASE 1
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		CASE 49		//El Gordo Lighthouse
			SWITCH iCoordID
				CASE 0
					RETURN FALSE
				BREAK
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if the vehicle spawn point is located within an airport
/// PARAMS:
///    iVSID - vehicle spawn point ID to check
/// RETURNS:
///    true/false
FUNC BOOL IS_VEHICLE_SPAWN_AREA_IN_AIRPORT(INT iVSID)
	SWITCH iVSID
		CASE 5		//Airport Helipad
		CASE 14		//Airport (Upper)
		CASE 15		//Airport (Lower)
		CASE 31		//Airport (Hangar)
		CASE 43		//Airport (Executive Hangar)
		CASE 55		//Airport (Upper Runway)
		CASE 56		//Airport (Construction)
		CASE 57		//Airport (Lower Runway)
		CASE 58		//Airport (Gates)
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if a specified vehicle model is large enough that it requires extra room to be safe to spawn
/// PARAMS:
///    thisModel - vehicle model to check
/// RETURNS:
///    true/false
FUNC BOOL DOES_VEHICLE_NEED_BIG_SPAWN_COORD(MODEL_NAMES thisModel)
	IF thisModel = STUNT
	OR thisModel = CUBAN800
	OR thisModel = DUSTER
	OR thisModel = FROGGER
	OR thisModel = MAVERICK
	OR thisModel = MARQUIS
	OR thisModel = JETMAX
	OR thisModel = SQUALO
	OR thisModel = TROPIC
	OR thisModel = SEASHARK
	OR thisModel = SUBMERSIBLE
	OR thisModel = BUZZARD
	OR thisModel = SUNTRAP
	OR thisModel = CRUSADER
	OR thisModel = ANNIHILATOR
	OR thisModel = MAMMATUS
	OR thisModel = STRETCH
	OR thisModel = BARRACKS
	OR thisModel = BESRA
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC	

/// PURPOSE:
///    Returns if a specified vehicle model is large enough that it requires extra room to be safe to spawn
/// PARAMS:
///    thisModel - vehicle model to check
/// RETURNS:
///    true/false
FUNC BOOL DOES_VEHICLE_NEED_REALLY_BIG_SPAWN_COORD(MODEL_NAMES thisModel)
	IF thisModel = TUG
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC	


/// PURPOSE:
///    Returns if a specified vehicle spawn point has any vehicle spawn coordinates that are capable of spawning large vehicles
/// PARAMS:
///    iVSID - vehicle spawn point ID to check
/// RETURNS:
///    true/false
FUNC BOOL DOES_VEHICLE_SPAWN_POINT_HAVE_BIG_SPAWN_COORD(INT iVSID)
	
	INT i
	
	REPEAT GET_NUMBER_OF_VEHICLE_SPAWN_POINT_COORDS(iVSID) i
		IF IS_VEHICLE_SPAWN_COORD_BIG(iVSID, i)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if a specified vehicle spawn point has any vehicle spawn coordinates that are capable of spawning large vehicles
/// PARAMS:
///    iVSID - vehicle spawn point ID to check
/// RETURNS:
///    true/false
FUNC BOOL DOES_VEHICLE_SPAWN_POINT_HAVE_REALLY_BIG_SPAWN_COORD(INT iVSID)
	
	INT i
	
	REPEAT GET_NUMBER_OF_VEHICLE_SPAWN_POINT_COORDS(iVSID) i
		IF IS_VEHICLE_SPAWN_COORD_REALLY_BIG(iVSID, i)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Returns if a specified vehicle is suitable for a Lester Locate Vehicle request
/// PARAMS:
///    thisModel - the model to check
/// RETURNS:
///    true/false
FUNC BOOL IS_VEHICLE_SUITABLE_FOR_LESTER_REQUEST(MODEL_NAMES thisModel)

	IF thisModel = RIOT
	OR thisModel = BULLDOZER
	OR thisModel = BLAZER
		RETURN FALSE
	ENDIF	

	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Returns if a specified vehicle is suitable for spawning during Criminal Damage
///    thisModel - the model to check
/// RETURNS:
///    true/false
FUNC BOOL IS_VEHICLE_SUITABLE_FOR_CRIMINAL_DAMAGE(MODEL_NAMES thisModel)

	INT iModelValue 

	VEHICLE_VALUE_DATA sValueData
	IF GET_VEHICLE_VALUE(sValueData,thisModel)
		iModelValue = sValueData.iStandardPrice
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_VEHICLE_SPAWN === IS_VEHICLE_SUITABLE_FOR_CRIMINAL_DAMAGE - GET_VEHICLE_VALUE = ",iModelValue)
	ENDIF
	
	IF iModelValue <= 0
		iModelValue = GET_VEHICLE_MODEL_VALUE(thisModel)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_VEHICLE_SPAWN === IS_VEHICLE_SUITABLE_FOR_CRIMINAL_DAMAGE - No vehicle value, using = GET_VEHICLE_MODEL_VALUE = ",iModelValue)
	ENDIF

	IF iModelValue > 400000
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_VEHICLE_SPAWN === IS_VEHICLE_SUITABLE_FOR_CRIMINAL_DAMAGE - Vehicle ",GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(thisModel), " is unsuitable for Criminal Damage - not spawning.")
		#ENDIF
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

FUNC BOOL IS_THIS_VEHICLE_A_PEGASUS_VEHICLE(VEHICLE_INDEX vehID)

	IF DECOR_IS_REGISTERED_AS_TYPE("CreatedByPegasus", DECOR_TYPE_BOOL)
	AND DECOR_EXIST_ON(vehID, "CreatedByPegasus")
	AND DECOR_GET_BOOL(vehID, "CreatedByPegasus")
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

PROC ar_ATTACH_VEHICLE_TO_TRAILER(VEHICLE_INDEX VehicleIndex, VEHICLE_INDEX TrailerIndex, FLOAT fTrailerHeadingOffset, FLOAT InverseMassScale = 1.0)
	/*
	VECTOR vVehicleCoord = GET_ENTITY_COORDS(VehicleIndex)
	FLOAT fVehicleHeading = GET_ENTITY_HEADING(VehicleIndex)
	
//	VECTOR vTrailerCoord = GET_ENTITY_COORDS(TrailerIndex)
	
	VECTOR vHinchOffset = <<0,-2,0>>	//<<0,-7.5,1>>
	VECTOR vHinchCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VehicleIndex, vHinchOffset)
//	DRAW_DEBUG_SPHERE(vHinchCoord, 0.25, 255,0,0,255)
	
	VECTOR vTrailerHinchDiff = <<0,-7.5,0>>-vHinchOffset	//vTrailerCoord-vHinchCoord
	VECTOR vNewTrailerCoord = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vHinchCoord, fVehicleHeading+fTrailerHeadingOffset, vTrailerHinchDiff)
//	DRAW_DEBUG_SPHERE(vNewTrailerCoord, 0.25, 0,255,0,255)
	
	SET_ENTITY_COORDS(VehicleIndex, vVehicleCoord)
	SET_ENTITY_HEADING(VehicleIndex, fVehicleHeading)
	
	SET_ENTITY_COORDS(TrailerIndex, vNewTrailerCoord)
	SET_ENTITY_HEADING(TrailerIndex, fVehicleHeading+fTrailerHeadingOffset) */
	UNUSED_PARAMETER(fTrailerHeadingOffset)
	
	ATTACH_VEHICLE_TO_TRAILER(VehicleIndex, TrailerIndex, InverseMassScale)
	CPRINTLN(DEBUG_NET_AMBIENT, "ar_ATTACH_VEHICLE_TO_TRAILER - ",
			GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(VehicleIndex),
			" to ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(TrailerIndex),
			", fTrailerHeadingOffset:", fTrailerHeadingOffset, ", InverseMassScale:", InverseMassScale)
	DEBUG_PRINTCALLSTACK()
ENDPROC

CONST_INT COL_CREW_1_SET	1
CONST_INT COL_CREW_2_SET	2

PROC SET_GR_TRAILER_SETUP_MP(VEHICLE_INDEX VehicleID, VEHICLE_SETUP_STRUCT_MP &VehicleSetupMP)
	IF NOT IS_ENTITY_DEAD(VehicleID)
		CPRINTLN(DEBUG_NET_AMBIENT, "SET_GR_TRAILER_SETUP_MP ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(VehicleID))
	ENDIF
	UNUSED_PARAMETER(VehicleID)
	
	VehicleSetupMP.VehicleSetup.iColour1 = GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_1)
	VehicleSetupMP.VehicleSetup.iColour2 = GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_2)
	VehicleSetupMP.VehicleSetup.iColourExtra1 = GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_EXTRA_1)
	VehicleSetupMP.VehicleSetup.iColourExtra2 = GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_EXTRA_2)
	VehicleSetupMP.VehicleSetup.iModIndex[MOD_LIVERY] = GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_LIVERY)
	VehicleSetupMP.VehicleSetup.iPlateIndex = GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_PLATE_INDEX)
	
	IF !HAS_PLAYER_OWN_TRUCK_COMMAND_CENTER()
		VehicleSetupMP.VehicleSetup.iModIndex[MOD_ROOF] = 0
	ELSE
		IF GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_ROOF) != 2
			VehicleSetupMP.VehicleSetup.iModIndex[MOD_ROOF] = 1
		ELSE
			VehicleSetupMP.VehicleSetup.iModIndex[MOD_ROOF] = 2
		ENDIF
	ENDIF	
	
	IF NOT IS_BIT_SET(GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_CREW_1), COL_CREW_1_SET) 
	AND NOT IS_BIT_SET(GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_CREW_1), COL_CREW_2_SET)
		CLEAR_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_CUSTOM_PRIMARY)
		CLEAR_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_CUSTOM_SECONDARY)
	ELSE
		INT iCustomR, iCustomG, iCustomB
		GET_PLAYER_CREW_COLOUR(iCustomR, iCustomG, iCustomB) 
		VehicleSetupMP.VehicleSetup.iCustomR = iCustomR
		VehicleSetupMP.VehicleSetup.iCustomG = iCustomG
		VehicleSetupMP.VehicleSetup.iCustomB = iCustomB
		IF IS_BIT_SET(GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_CREW_1), COL_CREW_1_SET)
			SET_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_CUSTOM_PRIMARY)
		ELSE
			CLEAR_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_CUSTOM_PRIMARY)
		ENDIF
		IF IS_BIT_SET(GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_CREW_1), COL_CREW_2_SET)
			SET_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_CUSTOM_SECONDARY)
		ELSE
			CLEAR_BIT(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_CUSTOM_SECONDARY)
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_NET_AMBIENT, "SET_GR_TRAILER_SETUP_MP livery:  ",VehicleSetupMP.VehicleSetup.iModIndex[MOD_LIVERY])
	CPRINTLN(DEBUG_NET_AMBIENT, "SET_GR_TRAILER_SETUP_MP roof:  ",VehicleSetupMP.VehicleSetup.iModIndex[MOD_ROOF])
ENDPROC

PROC GET_GR_TRAILER_SETUP_MP(VEHICLE_INDEX VehicleID, VEHICLE_SETUP_STRUCT_MP &VehicleSetupMP)
	CPRINTLN(DEBUG_NET_AMBIENT, "GET_GR_TRAILER_SETUP_MP ", GET_MODEL_NAME_OF_VEHICLE_FOR_DEBUG_ONLY(VehicleID))
	UNUSED_PARAMETER(VehicleID)
	
	SET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_1, VehicleSetupMP.VehicleSetup.iColour1)
	SET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_2, VehicleSetupMP.VehicleSetup.iColour2)
	SET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_EXTRA_1, VehicleSetupMP.VehicleSetup.iColourExtra1)
	SET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_EXTRA_2, VehicleSetupMP.VehicleSetup.iColourExtra2)
	
	IF HAS_PLAYER_OWN_TRUCK_COMMAND_CENTER()
		SET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_ROOF, VehicleSetupMP.VehicleSetup.iModIndex[MOD_ROOF])
	ENDIF
	
	INT iCrewBitset = 0
	IF IS_BIT_SET(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_CUSTOM_PRIMARY)
		SET_BIT(iCrewBitset, COL_CREW_1_SET)
	ENDIF
	IF IS_BIT_SET(VehicleSetupMP.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_CUSTOM_SECONDARY)
		SET_BIT(iCrewBitset, COL_CREW_2_SET)
	ENDIF
	SET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_COL_CREW_1, iCrewBitset)
	
	CPRINTLN(DEBUG_NET_AMBIENT, "GET_GR_TRAILER_SETUP_MP roof:  ",VehicleSetupMP.VehicleSetup.iModIndex[MOD_ROOF])
	CPRINTLN(DEBUG_NET_AMBIENT, "GET_GR_TRAILER_SETUP_MP livery:  ",VehicleSetupMP.VehicleSetup.iModIndex[MOD_LIVERY])
	SET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_LIVERY, VehicleSetupMP.VehicleSetup.iModIndex[MOD_LIVERY])
	SET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_PLATE_INDEX , VehicleSetupMP.VehicleSetup.iPlateIndex)
ENDPROC

PROC SET_COMMON_PROPERTIES_FOR_TRUCK_VEHICLE(NETWORK_INDEX niVehicle, NETWORK_INDEX niDriver)
	// store my truck vehicle as a global so i can access it from other scripts
	MPGlobalsAmbience.vehTruckVehicle[0] = NET_TO_VEH(niVehicle)
	MPGlobalsAmbience.vehTruckVehicle[1] = NET_TO_VEH(niDriver)
	
	MODEL_NAMES thisModel = GET_ENTITY_MODEL(NET_TO_VEH(niVehicle))
	
	#IF IS_DEBUG_BUILD
	VECTOR vCoords = GET_ENTITY_COORDS(NET_TO_VEH(niVehicle))
	FLOAT fHeading = GET_ENTITY_HEADING(NET_TO_VEH(niVehicle))
	#ENDIF
	
							
	INT iDecoratorValue
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === DECOR_IS_REGISTERED_AS_TYPE")
		IF DECOR_EXIST_ON(NET_TO_VEH(niVehicle), "MPBitset")	
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === DECOR_EXIST_ON")
			iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(niVehicle), "MPBitset")
		ENDIF
		DECOR_SET_INT(NET_TO_VEH(niVehicle), "MPBitset", iDecoratorValue)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MPBitset = ",iDecoratorValue)
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("Player_Truck", DECOR_TYPE_INT)
		IF NOT DECOR_EXIST_ON(MPGlobalsAmbience.vehTruckVehicle[0], "Player_Truck")
			DECOR_SET_INT(MPGlobalsAmbience.vehTruckVehicle[0], "Player_Truck", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
		ENDIF
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("Player_Truck", DECOR_TYPE_INT)
		IF NOT DECOR_EXIST_ON(MPGlobalsAmbience.vehTruckVehicle[1], "Player_Truck")
			DECOR_SET_INT(MPGlobalsAmbience.vehTruckVehicle[1], "Player_Truck", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
		ENDIF
	ENDIF
	
	SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(niVehicle, PLAYER_ID(), TRUE)
	
	IF GET_NUM_MOD_KITS(MPGlobalsAmbience.vehTruckVehicle[0]) > 0
		SET_VEHICLE_MOD_KIT(MPGlobalsAmbience.vehTruckVehicle[0], 0)
	ENDIF
	
	INT iSaveSlot
	MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_MODDED_ARMOURY_TRUCK  , iSaveSlot)
	PRINTLN("SET_COMMON_PROPERTIES_FOR_TRUCK_VEHICLE - iSaveSlot: ", iSaveSlot)
	IF iSaveSlot > 0 
		MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(MPGlobalsAmbience.vehTruckVehicle[0],iSaveSlot)
	ENDIF
	
	IF GET_NUM_MOD_KITS(MPGlobalsAmbience.vehTruckVehicle[1]) > 0
		SET_VEHICLE_MOD_KIT(MPGlobalsAmbience.vehTruckVehicle[1], 0)
	ENDIF
	
	VEHICLE_SETUP_STRUCT_MP sData
	GET_VEHICLE_SETUP_MP(MPGlobalsAmbience.vehTruckVehicle[1], sData)
	SET_GR_TRAILER_SETUP_MP(MPGlobalsAmbience.vehTruckVehicle[1], sData)
	SET_VEHICLE_SETUP_MP(MPGlobalsAmbience.vehTruckVehicle[1], sData, DEFAULT, DEFAULT, TRUE)
	IF !HAS_PLAYER_OWN_TRUCK_COMMAND_CENTER()
		sData.VehicleSetup.iModIndex[MOD_ROOF] = 0
	ELSE
		IF GET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_ROOF) != 2
			SET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_ROOF, 1)
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
		ELSE
			SET_MP_TRUCK_MOD_STAT(TRUCK_MOD_TRAILER_MOD_ROOF, 2)
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
		ENDIF
	ENDIF
	CPRINTLN(DEBUG_NET_AMBIENT, "SET_COMMON_PROPERTIES_FOR_TRUCK_VEHICLE roof:  ",sData.VehicleSetup.iModIndex[MOD_ROOF])
	
	IF sData.VehicleSetup.iModIndex[MOD_ROOF] = 0
	OR sData.VehicleSetup.iModIndex[MOD_ROOF] = 255
		REMOVE_VEHICLE_MOD(MPGlobalsAmbience.vehTruckVehicle[1],MOD_ROOF)
	ELSE
		SET_VEHICLE_MOD(MPGlobalsAmbience.vehTruckVehicle[1],MOD_ROOF, sData.VehicleSetup.iModIndex[MOD_ROOF]-1)
	ENDIF
	
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(niVehicle),FALSE)
	
	//truck
	SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(MPGlobalsAmbience.vehTruckVehicle[0], FALSE)
	SET_VEHICLE_STRONG(MPGlobalsAmbience.vehTruckVehicle[0], TRUE)
	IF (thisModel = PHANTOM3)
		SET_VEHICLE_DAMAGE_SCALE(MPGlobalsAmbience.vehTruckVehicle[0], g_sMPTunables.fGR_MOC_PHANTOM3_DAMAGE_SCALE)
	ELSE
		SET_VEHICLE_DAMAGE_SCALE(MPGlobalsAmbience.vehTruckVehicle[0], g_sMPTunables.fGR_MOC_HAULER2_DAMAGE_SCALE)
	ENDIF
	SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(MPGlobalsAmbience.vehTruckVehicle[0], TRUE)
	
	//trailer
	SET_VEHICLE_TYRES_CAN_BURST(MPGlobalsAmbience.vehTruckVehicle[1],FALSE)
	SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(MPGlobalsAmbience.vehTruckVehicle[1], FALSE)
	SET_VEHICLE_STRONG(MPGlobalsAmbience.vehTruckVehicle[1], TRUE)
	SET_VEHICLE_DAMAGE_SCALE(MPGlobalsAmbience.vehTruckVehicle[1], g_sMPTunables.fGR_MOC_TRAILERLARGE_DAMAGE_SCALE)
	SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(MPGlobalsAmbience.vehTruckVehicle[1], TRUE)
	SET_VEHICLE_NUMBER_PLATE_TEXT(MPGlobalsAmbience.vehTruckVehicle[1], GET_VEHICLE_NUMBER_PLATE_TEXT(MPGlobalsAmbience.vehTruckVehicle[0]))
	
	
//	SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(MPGlobalsAmbience.vehTruckVehicle[1],FALSE)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(thisModel)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_COMMON_PROPERTIES_FOR_TRUCK_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(thisModel), 
								", vCoords ", vCoords, ", fHeading ", fHeading, ")")
	#ENDIF

	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bBlipMyTruckVehicleForGangMembers = MPGlobalsAmbience.bBlipTruckVehicleForGangMembers
ENDPROC

PROC SET_COMMON_PROPERTIES_FOR_AVENGER_VEHICLE(NETWORK_INDEX niVehicle)
	// store my truck vehicle as a global so i can access it from other scripts
	MPGlobalsAmbience.vehAvengerVehicle = NET_TO_VEH(niVehicle)
	
	MODEL_NAMES thisModel = GET_ENTITY_MODEL(NET_TO_VEH(niVehicle))
	
	#IF IS_DEBUG_BUILD
	VECTOR vCoords = GET_ENTITY_COORDS(NET_TO_VEH(niVehicle))
	FLOAT fHeading = GET_ENTITY_HEADING(NET_TO_VEH(niVehicle))
	#ENDIF
	
	INT iDecoratorValue
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === DECOR_IS_REGISTERED_AS_TYPE")
		IF DECOR_EXIST_ON(NET_TO_VEH(niVehicle), "MPBitset")	
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === DECOR_EXIST_ON")
			iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(niVehicle), "MPBitset")
		ENDIF
		DECOR_SET_INT(NET_TO_VEH(niVehicle), "MPBitset", iDecoratorValue)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MPBitset = ",iDecoratorValue)
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("Player_Avenger", DECOR_TYPE_INT)
		IF NOT DECOR_EXIST_ON(MPGlobalsAmbience.vehAvengerVehicle, "Player_Avenger")
			DECOR_SET_INT(MPGlobalsAmbience.vehAvengerVehicle, "Player_Avenger", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
		ENDIF
	ENDIF
	
	SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(niVehicle, PLAYER_ID(), TRUE)
	
	IF GET_NUM_MOD_KITS(MPGlobalsAmbience.vehAvengerVehicle) > 0
		SET_VEHICLE_MOD_KIT(MPGlobalsAmbience.vehAvengerVehicle, 0)
	ENDIF

	INT iSaveSlot
	MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_AVENGER_HELI  , iSaveSlot)
	PRINTLN("SET_COMMON_PROPERTIES_FOR_AVENGER_VEHICLE - iSaveSlot: ", iSaveSlot)
	IF iSaveSlot > 0 
		MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(MPGlobalsAmbience.vehAvengerVehicle,iSaveSlot)
	ENDIF
	
//	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(niVehicle),FALSE)
	
	SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(MPGlobalsAmbience.vehAvengerVehicle, FALSE)
	SET_VEHICLE_STRONG(MPGlobalsAmbience.vehAvengerVehicle, TRUE)
	SET_VEHICLE_DAMAGE_SCALE(MPGlobalsAmbience.vehAvengerVehicle, g_sMPTunables.fGO_AOC_AVENGER_DAMAGE_SCALE)
	SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(MPGlobalsAmbience.vehAvengerVehicle, TRUE)
	SET_DISABLE_HELI_EXPLODE_FROM_BODY_DAMAGE(NET_TO_VEH(niVehicle),TRUE)
	APPLY_WEAPONS_TO_VEHICLE(MPGlobalsAmbience.vehAvengerVehicle, TRUE)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(thisModel)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_COMMON_PROPERTIES_FOR_AVENGER_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(thisModel), 
								", vCoords ", vCoords, ", fHeading ", fHeading, ")")
	#ENDIF

	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bBlipMyPlaneVehicleForGangMembers = MPGlobalsAmbience.bBlipPlaneVehicleForGangMembers
ENDPROC

PROC SET_COMMON_PROPERTIES_FOR_HACKER_TRUCK_VEHICLE(NETWORK_INDEX niVehicle)
	// store my Truck_1 vehicle as a global so i can access it from other scripts
	MPGlobalsAmbience.vehHackerTruck = NET_TO_VEH(niVehicle)
	
	MODEL_NAMES thisModel = GET_ENTITY_MODEL(NET_TO_VEH(niVehicle))
	
	#IF IS_DEBUG_BUILD
	VECTOR vCoords = GET_ENTITY_COORDS(NET_TO_VEH(niVehicle))
	FLOAT fHeading = GET_ENTITY_HEADING(NET_TO_VEH(niVehicle))
	#ENDIF
	
							
	INT iDecoratorValue
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === DECOR_IS_REGISTERED_AS_TYPE")
		IF DECOR_EXIST_ON(NET_TO_VEH(niVehicle), "MPBitset")	
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === DECOR_EXIST_ON")
			iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(niVehicle), "MPBitset")
		ENDIF
		DECOR_SET_INT(NET_TO_VEH(niVehicle), "MPBitset", iDecoratorValue)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MPBitset = ",iDecoratorValue)
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("Player_Hacker_Truck", DECOR_TYPE_INT)
		IF NOT DECOR_EXIST_ON(MPGlobalsAmbience.vehHackerTruck, "Player_Hacker_Truck")
			DECOR_SET_INT(MPGlobalsAmbience.vehHackerTruck, "Player_Hacker_Truck", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
		ENDIF
	ENDIF
	
	SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(niVehicle, PLAYER_ID(), TRUE)
	
	IF GET_NUM_MOD_KITS(MPGlobalsAmbience.vehHackerTruck) > 0
		SET_VEHICLE_MOD_KIT(MPGlobalsAmbience.vehHackerTruck, 0)
	ENDIF
	
	//#IF DEFINED(DISPLAY_SLOT_MODDED_HACKER_TRUCK)
	INT iSaveSlot
	MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_MEGAWARE_MOC   , iSaveSlot)
	PRINTLN("SET_COMMON_PROPERTIES_FOR_HACKER_TRUCK_VEHICLE - iSaveSlot: ", iSaveSlot)
	IF iSaveSlot > 0 
		MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(MPGlobalsAmbience.vehHackerTruck,iSaveSlot)
	ENDIF
	//#ENDIF
	
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(niVehicle),FALSE)
	
	//Hacker Truck
	SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(MPGlobalsAmbience.vehHackerTruck, FALSE)
	SET_VEHICLE_STRONG(MPGlobalsAmbience.vehHackerTruck, TRUE)
	SET_VEHICLE_DAMAGE_SCALE(MPGlobalsAmbience.vehHackerTruck, g_sMPTunables.fBB_MOC_TERBYTE_DAMAGE_SCALE)
	SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(MPGlobalsAmbience.vehHackerTruck, TRUE)
	SET_VEHICLE_TYRES_CAN_BURST(MPGlobalsAmbience.vehHackerTruck,FALSE)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(thisModel)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_COMMON_PROPERTIES_FOR_HACKER_TRUCK_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(thisModel), 
								", vCoords ", vCoords, ", fHeading ", fHeading, ")")
	#ENDIF

	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bBlipMyHackertruckVehicleForGangMembers = MPGlobalsAmbience.bBlipHackertruckVehicleForGangMembers
ENDPROC

#IF FEATURE_DLC_2_2022
PROC SET_COMMON_PROPERTIES_FOR_ACID_LAB_VEHICLE(NETWORK_INDEX niVehicle)
	// store my Truck_1 vehicle as a global so i can access it from other scripts
	MPGlobalsAmbience.vehAcidLab = NET_TO_VEH(niVehicle)
	
	MODEL_NAMES thisModel = GET_ENTITY_MODEL(NET_TO_VEH(niVehicle))
	
	#IF IS_DEBUG_BUILD
	VECTOR vCoords = GET_ENTITY_COORDS(NET_TO_VEH(niVehicle))
	FLOAT fHeading = GET_ENTITY_HEADING(NET_TO_VEH(niVehicle))
	#ENDIF
						
	INT iDecoratorValue
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === DECOR_IS_REGISTERED_AS_TYPE")
		IF DECOR_EXIST_ON(NET_TO_VEH(niVehicle), "MPBitset")	
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === DECOR_EXIST_ON")
			iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(niVehicle), "MPBitset")
		ENDIF
		DECOR_SET_INT(NET_TO_VEH(niVehicle), "MPBitset", iDecoratorValue)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MPBitset = ",iDecoratorValue)
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("Player_Acid_Lab", DECOR_TYPE_INT)
		IF NOT DECOR_EXIST_ON(MPGlobalsAmbience.vehAcidLab, "Player_Acid_Lab")
			DECOR_SET_INT(MPGlobalsAmbience.vehAcidLab, "Player_Acid_Lab", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
		ENDIF
	ENDIF
	
	SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(niVehicle, PLAYER_ID(), TRUE)
	
	IF GET_NUM_MOD_KITS(MPGlobalsAmbience.vehAcidLab) > 0
		SET_VEHICLE_MOD_KIT(MPGlobalsAmbience.vehAcidLab, 0)
	ENDIF
	
	//#IF DEFINED(DISPLAY_SLOT_MODDED_HACKER_TRUCK)
//	INT iSaveSlot
//	MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_MEGAWARE_MOC   , iSaveSlot)
//	PRINTLN("SET_COMMON_PROPERTIES_FOR_HACKER_TRUCK_VEHICLE - iSaveSlot: ", iSaveSlot)
//	IF iSaveSlot > 0 
//		MP_SAVE_VEHICLE_APPLY_SAVED_DETAILS_TO_VEHICLE(MPGlobalsAmbience.vehAcidLab,iSaveSlot)
//	ENDIF
	//#ENDIF
	
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(niVehicle),FALSE)
	
	//Acid lab
	SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(MPGlobalsAmbience.vehAcidLab, FALSE)
	SET_VEHICLE_STRONG(MPGlobalsAmbience.vehAcidLab, TRUE)
	SET_VEHICLE_DAMAGE_SCALE(MPGlobalsAmbience.vehAcidLab, g_sMPTunables.fACID_LAB_DAMAGE_SCALE)
	SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(MPGlobalsAmbience.vehAcidLab, TRUE)
	SET_VEHICLE_TYRES_CAN_BURST(MPGlobalsAmbience.vehAcidLab,FALSE)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(thisModel)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_COMMON_PROPERTIES_FOR_ACID_LAB_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(thisModel), 
								", vCoords ", vCoords, ", fHeading ", fHeading, ")")
	#ENDIF

	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bBlipMyAcidLabVehicleForGangMembers = MPGlobalsAmbience.bBlipAcidLabVehicleForGangMembers
ENDPROC
#ENDIF

#IF FEATURE_HEIST_ISLAND
PROC SET_COMMON_PROPERTIES_FOR_SUBMARINE_VEHICLE(NETWORK_INDEX niVehicle)
	MPGlobalsAmbience.vehSubmarine = NET_TO_VEH(niVehicle)
	
	MODEL_NAMES thisModel = GET_ENTITY_MODEL(NET_TO_VEH(niVehicle))
	
	#IF IS_DEBUG_BUILD
	VECTOR vCoords = GET_ENTITY_COORDS(NET_TO_VEH(niVehicle))
	FLOAT fHeading = GET_ENTITY_HEADING(NET_TO_VEH(niVehicle))
	#ENDIF
							
	INT iDecoratorValue
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === DECOR_IS_REGISTERED_AS_TYPE")
		IF DECOR_EXIST_ON(NET_TO_VEH(niVehicle), "MPBitset")	
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === DECOR_EXIST_ON")
			iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(niVehicle), "MPBitset")
		ENDIF
		DECOR_SET_INT(NET_TO_VEH(niVehicle), "MPBitset", iDecoratorValue)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MPBitset = ",iDecoratorValue)
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("Player_Submarine", DECOR_TYPE_INT)
		IF NOT DECOR_EXIST_ON(MPGlobalsAmbience.vehSubmarine, "Player_Submarine")
			DECOR_SET_INT(MPGlobalsAmbience.vehSubmarine, "Player_Submarine", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
		ENDIF
	ENDIF
	
	SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(niVehicle, PLAYER_ID(), TRUE)
	
	//url:bugstar:6759785 - Fix for submarine not existing for players in freemode if owner is in a tutorial session
	//SET_NETWORK_ID_EXISTS_ON_ALL_MACHINES(niVehicle, TRUE)
	NETWORK_ALLOW_CLONING_WHILE_IN_TUTORIAL(niVehicle, TRUE)
	
	IF GET_NUM_MOD_KITS(MPGlobalsAmbience.vehSubmarine) > 0
		SET_VEHICLE_MOD_KIT(MPGlobalsAmbience.vehSubmarine, 0)
	ENDIF

	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(MPGlobalsAmbience.vehSubmarine,FALSE)
	
	SET_VEHICLE_ALLOW_NO_PASSENGERS_LOCKON(MPGlobalsAmbience.vehSubmarine, TRUE)
	
	//Submarine
	SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(MPGlobalsAmbience.vehSubmarine, FALSE)
	SET_VEHICLE_STRONG(MPGlobalsAmbience.vehSubmarine, TRUE)
	SET_VEHICLE_DAMAGE_SCALE(MPGlobalsAmbience.vehSubmarine, g_sMPTunables.fSUBMARINE_DAMAGE_SCALE)
	SET_ENTITY_PROOFS(MPGlobalsAmbience.vehSubmarine, FALSE, TRUE, FALSE, FALSE, FALSE)
	SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(MPGlobalsAmbience.vehSubmarine, TRUE)
	
	//manually setting health to max health for url:bugstar:6851099
	SET_ENTITY_HEALTH(MPGlobalsAmbience.vehSubmarine, 1000)
	
	SET_SUBMARINE_PROPERTY_COLOURS(MPGlobalsAmbience.vehSubmarine, GET_LOCAL_PLAYER_SUBMARINE_COLOUR())
	SET_SUBMARINE_PROPERTY_FLAG(MPGlobalsAmbience.vehSubmarine, GET_LOCAL_PLAYER_SUBMARINE_FLAG())
	
	SET_MODEL_AS_NO_LONGER_NEEDED(thisModel)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_COMMON_PROPERTIES_FOR_SUBMARINE_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(thisModel), 
								", vCoords ", vCoords, ", fHeading ", fHeading, ")")
	#ENDIF

	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bBlipMySubmarineVehicleForGangMembers = MPGlobalsAmbience.bBlipSubmarineVehicleForGangMembers
ENDPROC
#ENDIF


#IF FEATURE_HEIST_ISLAND
PROC SET_COMMON_PROPERTIES_FOR_SUBMARINE_DINGHY_VEHICLE(NETWORK_INDEX niVehicle)
	MPGlobalsAmbience.vehSubmarineDinghy = NET_TO_VEH(niVehicle)
	
	MODEL_NAMES thisModel = GET_ENTITY_MODEL(NET_TO_VEH(niVehicle))
	
	#IF IS_DEBUG_BUILD
	VECTOR vCoords = GET_ENTITY_COORDS(NET_TO_VEH(niVehicle))
	FLOAT fHeading = GET_ENTITY_HEADING(NET_TO_VEH(niVehicle))
	#ENDIF
	
	INT iDecoratorValue
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === DECOR_IS_REGISTERED_AS_TYPE")
		IF DECOR_EXIST_ON(NET_TO_VEH(niVehicle), "MPBitset")	
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === DECOR_EXIST_ON")
			iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(niVehicle), "MPBitset")
		ENDIF
		DECOR_SET_INT(NET_TO_VEH(niVehicle), "MPBitset", iDecoratorValue)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MPBitset = ",iDecoratorValue)
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("Player_Submarine_Dinghy", DECOR_TYPE_INT)
		IF NOT DECOR_EXIST_ON(MPGlobalsAmbience.vehSubmarineDinghy, "Player_Submarine_Dinghy")
			DECOR_SET_INT(MPGlobalsAmbience.vehSubmarineDinghy, "Player_Submarine_Dinghy", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
		ENDIF
	ENDIF
	
	SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(niVehicle, PLAYER_ID(), TRUE)
	
	IF GET_NUM_MOD_KITS(MPGlobalsAmbience.vehSubmarineDinghy) > 0
		SET_VEHICLE_MOD_KIT(MPGlobalsAmbience.vehSubmarineDinghy, 0)
	ENDIF
	
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(niVehicle),FALSE)
	SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(MPGlobalsAmbience.vehSubmarineDinghy, FALSE)
	SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(MPGlobalsAmbience.vehSubmarineDinghy, TRUE)
	
	IF g_sMPTunables.IH_RADIO_ENABLE_KULT_FM
		START_VEHICLE_AND_TURN_ON_RADIO(MPGlobalsAmbience.vehSubmarineDinghy, "RADIO_34_DLC_HEI4_KULT")
	ENDIF
	
	IF CAN_ANCHOR_BOAT_HERE(MPGlobalsAmbience.vehSubmarineDinghy)
		SET_BOAT_ANCHOR(MPGlobalsAmbience.vehSubmarineDinghy, TRUE)
	ELSE
		PRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === Failed to anchor Dinghy")
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(thisModel)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_COMMON_PROPERTIES_FOR_SUBMARINE_DINGHY_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(thisModel), 
								", vCoords ", vCoords, ", fHeading ", fHeading, ")")
	#ENDIF
	
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bBlipMySubmarineDinghyVehicleForGangMembers = MPGlobalsAmbience.bBlipSubmarineDinghyVehicleForGangMembers

ENDPROC
#ENDIF

#IF FEATURE_DLC_2_2022
PROC SET_COMMON_PROPERTIES_FOR_SUPPORT_BIKE_VEHICLE(NETWORK_INDEX niVehicle)
	MPGlobalsAmbience.vehSupportBike = NET_TO_VEH(niVehicle)
	
	MODEL_NAMES thisModel = GET_ENTITY_MODEL(NET_TO_VEH(niVehicle))
	
	#IF IS_DEBUG_BUILD
	VECTOR vCoords = GET_ENTITY_COORDS(NET_TO_VEH(niVehicle))
	FLOAT fHeading = GET_ENTITY_HEADING(NET_TO_VEH(niVehicle))
	#ENDIF
	
	INT iDecoratorValue
	IF DECOR_IS_REGISTERED_AS_TYPE("MPBitset", DECOR_TYPE_INT)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === DECOR_IS_REGISTERED_AS_TYPE")
		IF DECOR_EXIST_ON(NET_TO_VEH(niVehicle), "MPBitset")	
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === DECOR_EXIST_ON")
			iDecoratorValue = DECOR_GET_INT(NET_TO_VEH(niVehicle), "MPBitset")
		ENDIF
		DECOR_SET_INT(NET_TO_VEH(niVehicle), "MPBitset", iDecoratorValue)
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === MPBitset = ",iDecoratorValue)
	ENDIF
	
	IF DECOR_IS_REGISTERED_AS_TYPE("Player_Support_Bike_Vehicle", DECOR_TYPE_INT)
		IF NOT DECOR_EXIST_ON(MPGlobalsAmbience.vehSupportBike, "Player_Support_Bike_Vehicle")
			DECOR_SET_INT(MPGlobalsAmbience.vehSupportBike, "Player_Support_Bike_Vehicle", NETWORK_HASH_FROM_PLAYER_HANDLE(PLAYER_ID()))
		ENDIF
	ENDIF
	
	SET_NETWORK_ID_ALWAYS_EXISTS_FOR_PLAYER(niVehicle, PLAYER_ID(), TRUE)
	
	IF GET_NUM_MOD_KITS(MPGlobalsAmbience.vehSupportBike) > 0
		SET_VEHICLE_MOD_KIT(MPGlobalsAmbience.vehSupportBike, 0)
	ENDIF
	
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(NET_TO_VEH(niVehicle),FALSE)
	SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(MPGlobalsAmbience.vehSupportBike, FALSE)
	SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(MPGlobalsAmbience.vehSupportBike, TRUE)
	
	IF g_sMPTunables.IH_RADIO_ENABLE_KULT_FM
		START_VEHICLE_AND_TURN_ON_RADIO(MPGlobalsAmbience.vehSupportBike, "RADIO_34_DLC_HEI4_KULT")
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(thisModel)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_COMMON_PROPERTIES_FOR_SUPPORT_BIKE_VEHICLE( Model ", GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(thisModel), 
								", vCoords ", vCoords, ", fHeading ", fHeading, ")")
	#ENDIF
	
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bBlipMySupportBikeVehicleForGangMembers = MPGlobalsAmbience.bBlipSupportBikeVehicleForGangMembers

ENDPROC
#ENDIF

FUNC BOOL DOES_PLAYER_ARMORY_TRAILER_HAVE_TURRET_EQUIPPED(PLAYER_INDEX piPlayer)
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(piPlayer)].netID_TruckV[1])
		IF GET_VEHICLE_MOD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(piPlayer)].netID_TruckV[1]), MOD_ROOF) > 0
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PLAYER_ARMORY_AIRCRAFT_HAVE_TURRET_EQUIPPED(PLAYER_INDEX piPlayer)
	IF NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(piPlayer)].netID_AvengerV)
		IF GET_VEHICLE_MOD(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(piPlayer)].netID_AvengerV), MOD_ROOF) > 0
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Spawns a booked vehicle with the model, coordinates and heading supplied. Vehicle is networked. Stores the location the booked vehicle was created at
/// PARAMS:
///    thisModel - model to spawn with
///    vCoords - location to spawn at
///    fHeading - haeding to spawn with
/// RETURNS:
///    true when complete
FUNC BOOL SPAWN_BOOKED_TRUCK_VEHICLE(VECTOR vCoords, FLOAT fHeading, BOOL bFadeIn = FALSE, BOOl bInsideBunker = FALSE)
	IF NOT (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]) AND DOES_ENTITY_EXIST(MPGlobals.RemoteTruckV[NATIVE_TO_INT(PLAYER_ID())][0]))
	AND NOT (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]) AND DOES_ENTITY_EXIST(MPGlobals.RemoteTruckV[NATIVE_TO_INT(PLAYER_ID())][1]))
		IF REQUEST_LOAD_MODEL(GET_PLAYERS_GUNRUNNING_TRUCK_MODEL(PLAYER_ID()))
		AND REQUEST_LOAD_MODEL(TRAILERLARGE)
			IF CREATE_NET_VEHICLE(	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0],
					GET_PLAYERS_GUNRUNNING_TRUCK_MODEL(PLAYER_ID()), vCoords, fHeading, FALSE, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE , bInsideBunker)
			AND CREATE_NET_VEHICLE(	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1],
					TRAILERLARGE, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCoords, fHeading, << 0, -9, 1.9 >>), fHeading, FALSE, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE , bInsideBunker)
				
				SET_VEHICLE_AS_CURRENT_TRUCK_VEHICLE(
						NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]),
						NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]),
						DEFAULT, DEFAULT)
				
				SET_COMMON_PROPERTIES_FOR_TRUCK_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0], GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1])

				
				IF !bInsideBunker
					ATTACH_VEHICLE_TO_TRAILER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), DEFAULT)
					VEHICLE_SET_OVERRIDE_SIDE_RATIO(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]),1)
					SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]))
					SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
					SET_TRAILER_LEGS_RAISED(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))
				ELSE
					ATTACH_VEHICLE_TO_TRAILER(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]),NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]))

					SET_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), FALSE)
					SET_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), FALSE)
					FREEZE_ENTITY_POSITION(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), TRUE)
					FREEZE_ENTITY_POSITION(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), TRUE)
				//	SET_ENTITY_INVINCIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), TRUE)
				//	SET_ENTITY_PROOFS(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), TRUE, TRUE, TRUE, TRUE, TRUE, DEFAULT, TRUE, DEFAULT)
					SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), FALSE)
				//	SET_VEHICLE_ALLOW_HOMING_MISSLE_LOCKON(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), FALSE)
				ENDIF
				
				//truck
				SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), FALSE)
				SET_VEHICLE_STRONG(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), TRUE)
				IF (GET_PLAYERS_GUNRUNNING_TRUCK_MODEL(PLAYER_ID()) = PHANTOM3)
					SET_VEHICLE_DAMAGE_SCALE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), g_sMPTunables.fGR_MOC_PHANTOM3_DAMAGE_SCALE)
				ELSE
					SET_VEHICLE_DAMAGE_SCALE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), g_sMPTunables.fGR_MOC_HAULER2_DAMAGE_SCALE)
				ENDIF
				SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]), TRUE)
				
				//trailer
				SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), FALSE)
				SET_VEHICLE_STRONG(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), TRUE)
				SET_VEHICLE_DAMAGE_SCALE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), g_sMPTunables.fGR_MOC_TRAILERLARGE_DAMAGE_SCALE)
				SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), TRUE)
				SET_VEHICLE_NUMBER_PLATE_TEXT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]), GET_VEHICLE_NUMBER_PLATE_TEXT(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0])))
				
				IF bFadeIn
					NETWORK_FADE_IN_ENTITY(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[0]),TRUE)
					NETWORK_FADE_IN_ENTITY(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_TruckV[1]),TRUE)
				ENDIF				
				
				IF !bInsideBunker
					SET_ARMORY_TRUCK_IS_IN_BUNKER(FALSE)
				ELSE
					SET_ARMORY_TRUCK_IS_IN_BUNKER(TRUE)
				ENDIF
				
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYERS_GUNRUNNING_TRUCK_MODEL(PLAYER_ID()))
				SET_MODEL_AS_NO_LONGER_NEEDED(TRAILERLARGE)
				
				MPGlobalsAmbience.bCleanupPegasusVehicle = FALSE
				MPGlobalsAmbience.bForceDeletePegasusVehicle = FALSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_TRUCK_VEHICLE CREATE_NET_VEHICLE = TRUE")
				#ENDIF
				RETURN TRUE
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_TRUCK_VEHICLE fail as CREATE_NET_VEHICLE = FALSE")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_TRUCK_VEHICLE fail as REQUEST_LOAD_MODEL = FALSE")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_TRUCK_VEHICLE fail as DOES_ENTITY_EXIST = TRUE")
		#ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL SAFE_TO_SPAWN_AIRCRAFT_VEHICLE_AT_COORDS(VECTOR vCoords, BOOL bDeleteVehiclesAtCoords)
	VEHICLE_INDEX vehSearch[100]
	INT iVehicleCount = GET_ALL_VEHICLES(vehSearch)
	INT iVehicle
	VECTOR vAircraftCoords
	PLAYER_INDEX playerOwner
	
	REPEAT iVehicleCount iVehicle
		IF DOES_ENTITY_EXIST(vehSearch[iVehicle])
		AND GET_ENTITY_MODEL(vehSearch[iVehicle]) = GET_ARMORY_AIRCRAFT_MODEL()
			vAircraftCoords = GET_ENTITY_COORDS(vehSearch[iVehicle], FALSE)
			IF ARE_VECTORS_ALMOST_EQUAL(vCoords, vAircraftCoords, 5.0)
				
				#IF IS_DEBUG_BUILD
				IF DECOR_IS_REGISTERED_AS_TYPE("Player_Avenger", DECOR_TYPE_INT)
				AND DECOR_EXIST_ON(vehSearch[iVehicle], "Player_Avenger")
					PRINTLN("SAFE_TO_SPAWN_AIRCRAFT_VEHICLE_AT_COORDS - vehicle found with Player_Avenger decorator = ", DECOR_GET_INT(vehSearch[iVehicle], "Player_Avenger"))
				ELSE
					PRINTLN("SAFE_TO_SPAWN_AIRCRAFT_VEHICLE_AT_COORDS - vehicle found with no Player_Avenger decorator")
				ENDIF
				#ENDIF
				
				IF bDeleteVehiclesAtCoords
					playerOwner = GET_OWNER_OF_PERSONAL_ARMORY_AIRCRAFT(vehSearch[iVehicle])
					IF NOT IS_NET_PLAYER_OK(playerOwner, FALSE, FALSE)
						IF NETWORK_HAS_CONTROL_OF_ENTITY(vehSearch[iVehicle])
							PRINTLN("SAFE_TO_SPAWN_AIRCRAFT_VEHICLE_AT_COORDS - deleting vehicle!")
							SET_ENTITY_AS_MISSION_ENTITY(vehSearch[iVehicle], FALSE, TRUE)
							DELETE_VEHICLE(vehSearch[iVehicle])
						ELSE
							PRINTLN("SAFE_TO_SPAWN_AIRCRAFT_VEHICLE_AT_COORDS - requesting control so we can delete")
							NETWORK_REQUEST_CONTROL_OF_ENTITY(vehSearch[iVehicle])
						ENDIF
					ENDIF
				ENDIF
				
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Spawns a booked vehicle with the model, coordinates and heading supplied. Vehicle is networked. Stores the location the booked vehicle was created at
/// PARAMS:
///    thisModel - model to spawn with
///    vCoords - location to spawn at
///    fHeading - haeding to spawn with
/// RETURNS:
///    true when complete
FUNC BOOL SPAWN_BOOKED_AIRCRAFT_VEHICLE(VECTOR vCoords, FLOAT fHeading, BOOL bFadeIn = FALSE, BOOl bInsideBase = FALSE)
	IF NOT (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV) AND DOES_ENTITY_EXIST(MPGlobals.RemoteAvengerV[NATIVE_TO_INT(PLAYER_ID())]))
		IF REQUEST_LOAD_MODEL(GET_ARMORY_AIRCRAFT_MODEL())
			IF SAFE_TO_SPAWN_AIRCRAFT_VEHICLE_AT_COORDS(vCoords, TRUE)
				IF CREATE_NET_VEHICLE(	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV,
						GET_ARMORY_AIRCRAFT_MODEL(), vCoords, fHeading, FALSE, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE , bInsideBase)
					
					SET_VEHICLE_AS_CURRENT_AVENGER_VEHICLE(
							NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV),
							DEFAULT, DEFAULT)
					
					SET_COMMON_PROPERTIES_FOR_AVENGER_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV)

					
					IF !bInsideBase
						SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV))
					ELSE
						SET_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV), FALSE)
						FREEZE_ENTITY_POSITION(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV), TRUE)
					ENDIF
					
					//
					SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV), FALSE)
					SET_VEHICLE_STRONG(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV), TRUE)
					SET_VEHICLE_DAMAGE_SCALE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV), g_sMPTunables.fGO_AOC_AVENGER_DAMAGE_SCALE)
					SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV), TRUE)
					
					IF bFadeIn
						NETWORK_FADE_IN_ENTITY(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AvengerV),TRUE)
					ENDIF				
					
					IF !bInsideBase
						SET_ARMORY_AIRCRAFT_IS_IN_DEFUNCT_BASE(FALSE)
					ELSE
						SET_ARMORY_AIRCRAFT_IS_IN_DEFUNCT_BASE(TRUE)
					ENDIF
					
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_ARMORY_AIRCRAFT_MODEL())
					
					MPGlobalsAmbience.bCleanupPegasusVehicle = FALSE
					MPGlobalsAmbience.bForceDeletePegasusVehicle = FALSE
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_AIRCRAFT_VEHICLE CREATE_NET_VEHICLE = TRUE")
					#ENDIF
					RETURN TRUE
				ELSE
					#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_AIRCRAFT_VEHICLE fail as CREATE_NET_VEHICLE = FALSE")
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_AIRCRAFT_VEHICLE fail as SAFE_TO_SPAWN_AIRCRAFT_VEHICLE_AT_COORDS = FALSE")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_AIRCRAFT_VEHICLE fail as REQUEST_LOAD_MODEL = FALSE")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_AIRCRAFT_VEHICLE fail as DOES_ENTITY_EXIST = TRUE")
		#ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Spawns a booked vehicle with the model, coordinates and heading supplied. Vehicle is networked. Stores the location the booked vehicle was created at
/// PARAMS:
///    thisModel - model to spawn with
///    vCoords - location to spawn at
///    fHeading - haeding to spawn with
/// RETURNS:
///    true when complete
FUNC BOOL SPAWN_BOOKED_HACKER_TRUCK_VEHICLE(VECTOR vCoords, FLOAT fHeading, BOOL bFadeIn = FALSE, BOOl bInsideBusinessHub = FALSE)
	IF NOT (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV) AND DOES_ENTITY_EXIST(MPGlobals.RemoteHackertruckV[NATIVE_TO_INT(PLAYER_ID())]))
		IF REQUEST_LOAD_MODEL(GET_HACKER_TRUCK_MODEL())
			IF CREATE_NET_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV,
					GET_HACKER_TRUCK_MODEL(), vCoords, fHeading, FALSE, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE , bInsideBusinessHub)
				
				SET_VEHICLE_AS_CURRENT_HACKER_TRUCK_VEHICLE(
						NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV),
						DEFAULT, DEFAULT)
				
				SET_COMMON_PROPERTIES_FOR_HACKER_TRUCK_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV)

				
				IF !bInsideBusinessHub
					SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV))
				ELSE
					SET_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV), FALSE)
					FREEZE_ENTITY_POSITION(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV), TRUE)
				ENDIF
				
				//Hacker Truck
				SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV), FALSE)
				SET_VEHICLE_STRONG(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV), TRUE)
				SET_VEHICLE_DAMAGE_SCALE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV), g_sMPTunables.fBB_MOC_TERBYTE_DAMAGE_SCALE)
				SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV), TRUE)
				
				SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV), FALSE)

				IF bFadeIn
					NETWORK_FADE_IN_ENTITY(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_HackertruckV),TRUE)
				ENDIF				
				
				IF !bInsideBusinessHub
					SET_HACKER_TRUCK_IS_IN_BUSINESS_HUB(FALSE)
				ELSE
					SET_HACKER_TRUCK_IS_IN_BUSINESS_HUB(TRUE)
				ENDIF
				
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_HACKER_TRUCK_MODEL())
				
				MPGlobalsAmbience.bCleanupPegasusVehicle = FALSE
				MPGlobalsAmbience.bForceDeletePegasusVehicle = FALSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_HACKER_TRUCK_VEHICLE CREATE_NET_VEHICLE = TRUE")
				#ENDIF
				RETURN TRUE
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_HACKER_TRUCK_VEHICLE fail as CREATE_NET_VEHICLE = FALSE")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_HACKER_TRUCK_VEHICLE fail as REQUEST_LOAD_MODEL = FALSE")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_HACKER_TRUCK_VEHICLE fail as DOES_ENTITY_EXIST = TRUE")
		#ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

#IF FEATURE_DLC_2_2022
FUNC BOOL SPAWN_BOOKED_ACID_LAB_VEHICLE(VECTOR vCoords, FLOAT fHeading, BOOL bFadeIn = FALSE, BOOl bInsideJuggaloHideout = FALSE)
	IF NOT (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV) AND DOES_ENTITY_EXIST(MPGlobals.RemoteAcidLabV[NATIVE_TO_INT(PLAYER_ID())]))
		IF REQUEST_LOAD_MODEL(GET_ACID_LAB_MODEL())
			IF CREATE_NET_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV,
					GET_ACID_LAB_MODEL(), vCoords, fHeading, FALSE, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE , bInsideJuggaloHideout)
				
				SET_VEHICLE_AS_CURRENT_ACID_LAB_VEHICLE(
						NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV),
						DEFAULT, DEFAULT)
				
				SET_COMMON_PROPERTIES_FOR_ACID_LAB_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV)

				
				IF !bInsideJuggaloHideout
					SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV))
				ELSE
					SET_ENTITY_VISIBLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV), FALSE)
					FREEZE_ENTITY_POSITION(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV), TRUE)
				ENDIF
				
				//Acid lab
				SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV), FALSE)
				SET_VEHICLE_STRONG(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV), TRUE)
				SET_VEHICLE_DAMAGE_SCALE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV), g_sMPTunables.fACID_LAB_DAMAGE_SCALE)
				SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV), TRUE)
				
				SET_VEHICLE_TYRES_CAN_BURST(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV), FALSE)

				IF bFadeIn
					NETWORK_FADE_IN_ENTITY(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_AcidLabV),TRUE)
				ENDIF				
				
				IF !bInsideJuggaloHideout
					SET_ACID_LAB_IN_JUGGALO_HIDEOUT(FALSE)
				ELSE
					SET_ACID_LAB_IN_JUGGALO_HIDEOUT(TRUE)
				ENDIF
				
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_ACID_LAB_MODEL())
				
				MPGlobalsAmbience.bCleanupPegasusVehicle = FALSE
				MPGlobalsAmbience.bForceDeletePegasusVehicle = FALSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_ACID_LAB_VEHICLE CREATE_NET_VEHICLE = TRUE")
				#ENDIF
				RETURN TRUE
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_ACID_LAB_VEHICLE fail as CREATE_NET_VEHICLE = FALSE")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_ACID_LAB_VEHICLE fail as REQUEST_LOAD_MODEL = FALSE")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_ACID_LAB_VEHICLE fail as DOES_ENTITY_EXIST = TRUE")
		#ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC
#ENDIF

#IF FEATURE_HEIST_ISLAND
FUNC BOOL SPAWN_BOOKED_SUBMARINE_VEHICLE(VECTOR vCoords, FLOAT fHeading, BOOL bFadeIn = FALSE)

	IF NOT (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV) AND DOES_ENTITY_EXIST(MPGlobals.RemoteSubmarineV[NATIVE_TO_INT(PLAYER_ID())]))
		IF REQUEST_LOAD_MODEL(GET_SUBMARINE_MODEL())
			IF CREATE_NET_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV,
					GET_SUBMARINE_MODEL(), vCoords, fHeading, FALSE, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
				
				SET_VEHICLE_AS_CURRENT_SUBMARINE_VEHICLE(
						NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV),
						DEFAULT, DEFAULT)
				
				SET_COMMON_PROPERTIES_FOR_SUBMARINE_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV)

				SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV), FALSE)
				SET_VEHICLE_STRONG(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV), TRUE)
				SET_VEHICLE_DAMAGE_SCALE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV), g_sMPTunables.fSUBMARINE_DAMAGE_SCALE)
				SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV), TRUE)

				IF bFadeIn
					NETWORK_FADE_IN_ENTITY(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV),TRUE)
				ENDIF				
				
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_SUBMARINE_MODEL())
				
				MPGlobalsAmbience.bCleanupPegasusVehicle = FALSE
				MPGlobalsAmbience.bForceDeletePegasusVehicle = FALSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_SUBMARINE_VEHICLE CREATE_NET_VEHICLE = TRUE")
				#ENDIF
				RETURN TRUE
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_SUBMARINE_VEHICLE fail as CREATE_NET_VEHICLE = FALSE")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_SUBMARINE_VEHICLE fail as REQUEST_LOAD_MODEL = FALSE")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_SUBMARINE_VEHICLE fail as DOES_ENTITY_EXIST = TRUE")
		#ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL SPAWN_BOOKED_SUBMARINE_DINGHY_VEHICLE(VECTOR vCoords, FLOAT fHeading, BOOL bFadeIn = FALSE)
	IF NOT (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV) AND DOES_ENTITY_EXIST(MPGlobals.RemoteSubmarineDinghyV[NATIVE_TO_INT(PLAYER_ID())]))
		IF REQUEST_LOAD_MODEL(GET_SUBMARINE_DINGHY_MODEL())
			IF CREATE_NET_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV,
					GET_SUBMARINE_DINGHY_MODEL(), vCoords, fHeading, FALSE, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
				
				SET_VEHICLE_AS_CURRENT_SUBMARINE_DINGHY_VEHICLE(
						NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV),
						DEFAULT, DEFAULT)
				
				SET_COMMON_PROPERTIES_FOR_SUBMARINE_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV)

				SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV), TRUE)
				
				IF bFadeIn
					NETWORK_FADE_IN_ENTITY(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV),TRUE)
				ENDIF				
				
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_SUBMARINE_DINGHY_MODEL())
				
				MPGlobalsAmbience.bCleanupPegasusVehicle = FALSE
				MPGlobalsAmbience.bForceDeletePegasusVehicle = FALSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_SUBMARINE_DINGHY_VEHICLE CREATE_NET_VEHICLE = TRUE")
				#ENDIF
				RETURN TRUE
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_SUBMARINE_DINGHY_VEHICLE fail as CREATE_NET_VEHICLE = FALSE")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_SUBMARINE_DINGHY_VEHICLE fail as REQUEST_LOAD_MODEL = FALSE")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_SUBMARINE_DINGHY_VEHICLE fail as DOES_ENTITY_EXIST = TRUE")
		#ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC
#ENDIF

#IF FEATURE_DLC_2_2022
FUNC BOOL SPAWN_BOOKED_SUPPORT_BIKE_VEHICLE(VECTOR vCoords, FLOAT fHeading, BOOL bFadeIn = FALSE)
	IF NOT (NETWORK_DOES_NETWORK_ID_EXIST(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV) AND DOES_ENTITY_EXIST(MPGlobals.RemoteSupportBikeV[NATIVE_TO_INT(PLAYER_ID())]))
		IF REQUEST_LOAD_MODEL(GET_SUPPORT_BIKE_MODEL())
			IF CREATE_NET_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV,
					GET_SUPPORT_BIKE_MODEL(), vCoords, fHeading, FALSE, TRUE, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
				
				//SET_VEHICLE_AS_CURRENT_SUBMARINE_VEHICLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineV), DEFAULT, DEFAULT)
				SET_VEHICLE_AS_CURRENT_SUPPORT_BIKE_VEHICLE(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV), DEFAULT, DEFAULT)
				
				//SET_COMMON_PROPERTIES_FOR_SUBMARINE_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SubmarineDinghyV)
				SET_COMMON_PROPERTIES_FOR_SUPPORT_BIKE_VEHICLE(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV)
				
				SET_VEHICLE_EXPLODES_ON_EXPLOSION_DAMAGE_AT_ZERO_BODY_HEALTH(NET_TO_VEH(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV), TRUE)
				
				IF bFadeIn
					NETWORK_FADE_IN_ENTITY(NET_TO_ENT(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].netID_SupportBikeV),TRUE)
				ENDIF				
				
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_SUPPORT_BIKE_MODEL())
				
				MPGlobalsAmbience.bCleanupPegasusVehicle = FALSE
				MPGlobalsAmbience.bForceDeletePegasusVehicle = FALSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_SUPPORT_BIKE_VEHICLE CREATE_NET_VEHICLE = TRUE")
				#ENDIF
				RETURN TRUE
			ELSE
				#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_SUPPORT_BIKE_VEHICLE fail as CREATE_NET_VEHICLE = FALSE")
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_SUPPORT_BIKE_VEHICLE fail as REQUEST_LOAD_MODEL = FALSE")
			#ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SPAWN_BOOKED_SUPPORT_BIKE_VEHICLE fail as DOES_ENTITY_EXIST = TRUE")
		#ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC
#ENDIF

