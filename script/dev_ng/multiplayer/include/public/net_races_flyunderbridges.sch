// Checks for player flying under bridges in multiplayer races

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_entity.sch"
USING "commands_player.sch"
USING "commands_script.sch"

USING "script_player.sch"
USING "flow_public_core.sch"
USING "replay_public.sch"
USING "mission_stat_public.sch"

CONST_INT LA_RIVER_01		0
CONST_INT LA_RIVER_02		1
CONST_INT LA_RIVER_03		2
CONST_INT LA_RIVER_04		3
CONST_INT LA_RIVER_05		4
CONST_INT LA_RIVER_06		5
CONST_INT LA_RIVER_07		6
CONST_INT LA_RIVER_08		7
CONST_INT LA_RIVER_09		8
CONST_INT LA_RIVER_10		9
CONST_INT LA_RIVER_11		10
CONST_INT LA_RIVER_12		11
CONST_INT LA_RIVER_13		12
CONST_INT LA_RIVER_14		13
CONST_INT LA_RIVER_15		14
CONST_INT LA_RIVER_16		15
CONST_INT HIGHWAY_CS3		16
CONST_INT DOUBLEARCH_CS3	17
CONST_INT RAILARCH_CS1		18
CONST_INT RAILLOW_CS4		19
CONST_INT ROADLOW_CS4		20
CONST_INT RAILLOW_CS2		21
CONST_INT RAILLOW_CS3		22
CONST_INT MADISON_CS3		23
CONST_INT ROADLOW_CS6		24
CONST_INT RAILLOW_CH3		25
CONST_INT SEAARCH_CH3		26
CONST_INT RAILHIGH_CH3		27
CONST_INT RAILFREE_CH3		28
CONST_INT ROADSUSP_PO1		29
CONST_INT RAILLOW_CH1		30
CONST_INT RAILLOW_PO1		31
CONST_INT ROADLOW_PO1		32
CONST_INT KNIFE_SM01		33
CONST_INT KNIFE_2			34
CONST_INT KNIFE_3		35
CONST_INT KNIFE_4		36
CONST_INT DOMES_PO1			37
CONST_INT RAILROAD_CS2		38
CONST_INT HELI_UNDER		39
CONST_INT HELI_OVER			40
CONST_INT CANAL_TRACK		41
CONST_INT SPAGH_LOW			42
CONST_INT CHINOZ_BRIDGE			43
CONST_INT SC1_ARCH_BRIDGE		44
CONST_INT LOW_CAST_IRON			45
CONST_INT IND_PUMPS_1			46
CONST_INT IND_PUMPS_2			47
CONST_INT CH3_OVERPASS			48
CONST_INT CH3_RAILBRIDGE		49
CONST_INT KNIFE_5		50
CONST_INT KNIFE_6		51
CONST_INT KNIFE_7		52
CONST_INT KNIFE_8		53
CONST_INT KNIFE_9		54
CONST_INT KNIFE_10		55
CONST_INT KNIFE_11		56
CONST_INT KNIFE_12		57
CONST_INT KNIFE_13		58
CONST_INT KNIFE_14		59
CONST_INT KNIFE_15		60
CONST_INT BRIDGE_47 	61
CONST_INT BRIDGE_48 	62
CONST_INT BRIDGE_49		63
CONST_INT BRIDGE_50		64

CONST_INT NUMBER_OF_BRIDGES 	50
CONST_INT NUMBER_OF_KNIVES 15
CONST_INT NUMBER_OF_CHALLENGES 	65

INT currentBridge 
INT underBridgeStage = 0

BOOL bEnteredArea1, bEnteredArea2, bExitViaArea1, bExitViaArea2, bPassingThroughAreas

BOOL bCompletedABridge

VECTOR vPlayerPosition

VECTOR vTriggerPoint[NUMBER_OF_CHALLENGES]
FLOAT fStartChecking = 200*200
VECTOR vArea1Min, vArea1Max
VECTOR vArea2Min, vArea2Max
FLOAT fAreaWidth

FUNC BOOL IS_INDEX_A_KNIFE_FLIGHT_MP(INT iFlightIndex)
	SWITCH iFlightIndex
		CASE KNIFE_SM01
		CASE KNIFE_2
		CASE KNIFE_3
		CASE KNIFE_4
		CASE KNIFE_5
		CASE KNIFE_6
		CASE KNIFE_7
		CASE KNIFE_8
		CASE KNIFE_9
		CASE KNIFE_10
		CASE KNIFE_11	
		CASE KNIFE_12
		CASE KNIFE_13
		CASE KNIFE_14
		CASE KNIFE_15
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_BRIDGE_DIFFICULT(INT iFlightIndex)
	SWITCH iFlightIndex
		CASE LA_RIVER_01
		CASE LA_RIVER_02
		CASE LA_RIVER_09
		CASE LA_RIVER_13
		CASE LA_RIVER_14
		CASE ROADLOW_CS4
		CASE RAILLOW_PO1
		CASE ROADLOW_PO1
		CASE IND_PUMPS_1
		CASE IND_PUMPS_2
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC SETUP_BRIDGE_INFO()

	vTriggerPoint[LA_RIVER_01]	= <<1083, -231, 60>>
	
	vTriggerPoint[LA_RIVER_02]	= <<1024, -325, 60>>
	
	vTriggerPoint[LA_RIVER_03]	= <<910, -401, 43>>
	
	vTriggerPoint[LA_RIVER_04]	= <<721, -457, 26>>
	
	vTriggerPoint[LA_RIVER_05]	= <<643, -579, 26>>

	
	vTriggerPoint[LA_RIVER_06]	= <<590, -851, 26>>

	
	vTriggerPoint[LA_RIVER_07]	= <<590, -1023, 16>>
	
	vTriggerPoint[LA_RIVER_08]	= <<582, -1205, 24>>
	
	vTriggerPoint[LA_RIVER_09]	= <<608, -1335, 16>>
	
	vTriggerPoint[LA_RIVER_10]	= <<640, -1434, 16>>
	
	vTriggerPoint[LA_RIVER_11]	= <<671, -1742, 20>>
	
	vTriggerPoint[LA_RIVER_12]	= <<651, -2046, 16>>
	
	vTriggerPoint[LA_RIVER_13]	= <<603, -2505, 9>>
	
	vTriggerPoint[LA_RIVER_14]	= <<673, -2582, 4>>
	
	vTriggerPoint[LA_RIVER_15]	= <<728, -2594, 10>>
	
	vTriggerPoint[LA_RIVER_16]	= <<794, -2624, 27>>
	
	vTriggerPoint[HIGHWAY_CS3]		= <<-2663, 2594, 7.5>>
	
	vTriggerPoint[DOUBLEARCH_CS3]	= <<-1902, 4617, 30>>
	
	vTriggerPoint[RAILARCH_CS1]		= <<-513, 4427, 40>>
	 
	vTriggerPoint[RAILLOW_CS4]		= <<126, 3366, 40>>

	vTriggerPoint[ROADLOW_CS4]		= <<143, 3418, 36>>

	vTriggerPoint[RAILLOW_CS2]		= <<2822, 4978, 40>>
	
	vTriggerPoint[RAILLOW_CS3]		= <<-162, 4249, 40>>
	
	vTriggerPoint[MADISON_CS3]		= <<-408, 2964, 20>>
	
	vTriggerPoint[ROADLOW_CS6]		= <<-181, 2862, 38>>
	
	vTriggerPoint[RAILLOW_CH3]		= <<2558, 2201, 24>>
	
	vTriggerPoint[SEAARCH_CH3]		= <<2950, 803, 8>>
	
	vTriggerPoint[RAILHIGH_CH3]		= <<2369, -409, 80>>
	
	vTriggerPoint[RAILFREE_CH3]		= <<1906, -755, 84 >>
	
	vTriggerPoint[ROADSUSP_PO1]		= <<-403, -2333, 40>>
	
	vTriggerPoint[RAILLOW_CH1]		= <<-1429, 2649, 10>>
	
	vTriggerPoint[RAILLOW_PO1]		= <<219, -2315, 5>>
	
	vTriggerPoint[ROADLOW_PO1]		= <<350, -2315, 5>>
	
	vTriggerPoint[KNIFE_SM01]		= <<-1848, -333, 75>>
	
	vTriggerPoint[KNIFE_2]		= <<-693, -608, 69>>//<<-794, -595, 65>>
	
	vTriggerPoint[KNIFE_3]		= <<-1461, -582, 53>>//<<-890, -1261, 40>>

	vTriggerPoint[KNIFE_4]		= <<-1553, -546, 59>>//<<-969, -1304, 40>>

	vTriggerPoint[DOMES_PO1]		= <<338, -2758, 23>>
	
	vTriggerPoint[RAILROAD_CS2]		= <<1985, 6201, 53>>
	
	vTriggerPoint[HELI_UNDER]		= <<-713, -1538, 13>>
	
	vTriggerPoint[HELI_OVER]		= <<-659, -1518, 13>>
	
	vTriggerPoint[CANAL_TRACK]		= <<-620, -1502, 16>>
	
	vTriggerPoint[SPAGH_LOW]		= <<-445, -1575, 26>>
	
	vTriggerPoint[CHINOZ_BRIDGE]		= <<-373, -1680, 19>>
	
	vTriggerPoint[SC1_ARCH_BRIDGE]		= <<-212, -1805, 29>>
	
	vTriggerPoint[LOW_CAST_IRON]		= <<47, -2040, 18>>
	
	vTriggerPoint[IND_PUMPS_1]			= <<223, -2240, 6>>
	
	vTriggerPoint[IND_PUMPS_2]			= <<218, -2189, 6>>

	vTriggerPoint[CH3_OVERPASS]			= <<2308, 1124, 78>>
	
	vTriggerPoint[CH3_RAILBRIDGE]		= <<2349, 1174, 79>>
	
	vTriggerPoint[KNIFE_5]		= <<-1186, -365, 46>>

	vTriggerPoint[KNIFE_6]		= <<-916, -407, 93>>
	
	vTriggerPoint[KNIFE_7]		= <<-726, 235, 105>>
	
	vTriggerPoint[KNIFE_8]		= <<-774, 286, 112>>

	vTriggerPoint[KNIFE_9]		= <<271, 134, 125>>
	
	vTriggerPoint[KNIFE_10]		= <<377, -28, 125>>
	
	vTriggerPoint[KNIFE_11]		= <<121, -703, 150>>

	vTriggerPoint[KNIFE_12]		= <<-204, -784, 74>>
	
	vTriggerPoint[KNIFE_13]		= <<-287, -774, 72>>
	
	vTriggerPoint[KNIFE_14]		= <<-272, -824, 71>>
		
	vTriggerPoint[KNIFE_15]		= <<-230, -723, 80>>

	vTriggerPoint[BRIDGE_47]	= <<1822, 2044, 62>>
	
	vTriggerPoint[BRIDGE_48]	= <<2410, 2907, 44>>

	vTriggerPoint[BRIDGE_49]	= <<2686, 4858, 36>>

 	vTriggerPoint[BRIDGE_50]	= <<-1046, 4751, 244>>
ENDPROC

PROC GET_CURRENT_BRIDGE_INFO(INT bridgeIndex)
	SWITCH bridgeIndex
		CASE LA_RIVER_01 
			vArea1Min		= <<1103.013916,-233.037369,56.130039>>
			vArea1Max		= <<1073.190918,-214.847794,66.059296>>
			fAreaWidth		= 30
			vArea2Min 		= <<1093.588623,-248.592606,56.886391>>
			vArea2Max		= <<1063.774414,-230.142532,66.678467>>
		BREAK
		CASE LA_RIVER_02
			vArea1Min		= <<1044.181885,-324.590363,49.334076>>
			vArea1Max		= <<1016.709717,-307.738251,64.813431>>
			fAreaWidth		= 30
			vArea2Min 		= <<1007.983154,-320.615875,48.454296>>
			vArea2Max		= <<1036.006714,-337.420410,64.480797>>
		BREAK
		CASE LA_RIVER_03
			vArea1Min		= <<916.598999,-419.878204,35.627480>>
			vArea1Max		= <<910.379333,-383.882568,47.543388>>
			fAreaWidth		= 7
			vArea2Min 		= <<912.136169,-420.516144,35.380337>>
			vArea2Max		= <<906.895203,-384.677887,47.249256>>
		BREAK
		CASE LA_RIVER_04
			vArea1Min		= <<757.718872,-472.923950,19.253498>>
			vArea1Max		= <<696.593628,-420.211456,35.460842>>
			fAreaWidth		= 20.75
			vArea2Min 		= <<744.911438,-480.737335,19.065138>>
			vArea2Max		= <<682.561401,-429.553345,37.026600>>
		BREAK
		CASE LA_RIVER_05
			vArea1Min		= <<680.367676,-581.179199,14.214504>>
			vArea1Max		= <<599.810059,-528.305908,33.409580>>
			fAreaWidth		= 45.000000
			vArea2Min 		= <<667.369202,-610.535645,13.854013>>
			vArea2Max		= <<582.843262,-556.781799,33.403355>>
		BREAK
		CASE LA_RIVER_06
			vArea1Min		= <<644.249695,-844.750427,12.367073>>
			vArea1Max		= <<526.860840,-845.252075,35.989601>>
			fAreaWidth		= 25.000000
			vArea2Min 		= <<644.365906,-859.387756,12.596766>>
			vArea2Max		= <<526.861511,-857.520752,36.328571>>
		BREAK
		CASE LA_RIVER_07
			vArea1Min		= <<634.971985,-1011.640198,10.925943>>
			vArea1Max		= <<539.650085,-1024.017090,35.958515>>
			fAreaWidth		= 25.000000
			vArea2Min 		= <<634.961243,-1029.123047,10.618461>>
			vArea2Max		= <<543.489319,-1038.261475,35.959301>>
		BREAK
		CASE LA_RIVER_08
			vArea1Min		= <<645.722290,-1191.215332,10.451977>>
			vArea1Max		= <<524.301758,-1197.166870,39.611725>>
			fAreaWidth	= 50.000000
			vArea2Min 		= <<645.722290,-1228.966431,10.980150>>
			vArea2Max		= <<521.937866,-1217.607300,39.471722>>
		BREAK
		CASE LA_RIVER_09
			vArea1Min		= <<642.121643,-1295.730103,9.005976>> 
			vArea1Max		= <<568.170166,-1375.350830,20.129887>>
			fAreaWidth		= 7.000000
			vArea2Min 		= <<644.777222,-1298.168335,9.128529>>
			vArea2Max		= <<571.634155,-1378.644409,20.358023>>
		BREAK
		CASE LA_RIVER_10
			vArea1Min		= <<686.567505,-1444.709839,9.065001>>
			vArea1Max		= <<598.932800,-1444.437988,25.688457>>
			fAreaWidth		= 25.000000
			vArea2Min 		= <<682.302673,-1429.871948,9.890836>> 
			vArea2Max		= <<593.821655,-1432.995483,25.600724>>
		BREAK
		CASE LA_RIVER_11
			vArea1Min		= <<718.761658,-1734.312866,9.082874>>
			vArea1Max		= <<615.001709,-1725.897339,27.545851>>
			fAreaWidth		= 30.000000
			vArea2Min 		= <<717.535461,-1748.645630,9.363478>>
			vArea2Max		= <<614.217957,-1734.847534,27.357079>>
		BREAK
		CASE LA_RIVER_12
			vArea1Min		= <<694.316467,-2049.805664,0.009695>>
			vArea1Max		= <<618.684509,-2040.013672,25.834118>>
			fAreaWidth		= 30.000000
			vArea2Min 		= <<693.183594,-2063.225098,0.429037>> 
			vArea2Max		= <<607.594360,-2055.326416,26.918158>>
		BREAK
		CASE LA_RIVER_13
			vArea1Min		= <<642.667053,-2494.551270,0.468485>>
			vArea1Max		= <<570.180664,-2513.958740,11.787938>>
			fAreaWidth		= 20.000000
			vArea2Min 		= <<647.033875,-2506.202148,0.583701>> 
			vArea2Max		= <<571.141541,-2522.975342,10.450453>>
		BREAK
		CASE LA_RIVER_14
			vArea1Min		= <<691.823547,-2558.218506,0.363352>>
			vArea1Max		= <<645.886292,-2600.311279,9.898791>>
			fAreaWidth		= 10.250000
			vArea2Min 		= <<695.792847,-2561.033691,0.346731>>
			vArea2Max		= <<656.391907,-2601.971680,9.643657>>
		BREAK
		CASE LA_RIVER_15
			vArea1Min		= <<723.625427,-2562.170654,0.255647>>
			vArea1Max		= <<720.751953,-2619.769531,15.732105>>
			fAreaWidth		= 15.000000
			vArea2Min 		= <<735.764954,-2561.935059,0.311182>> 
			vArea2Max		= <<736.121399,-2618.766602,15.790609>>
		BREAK
		CASE LA_RIVER_16
			vArea1Min		= <<891.438660,-2603.120117,0>> 
			vArea1Max		= <<704.493164,-2634.793213,45>>
			fAreaWidth		= 20.000000
			vArea2Min 		= <<893.257813,-2616.235107,0>> 
			vArea2Max		= <<707.726135,-2647.695557,45>>
		BREAK
		CASE HIGHWAY_CS3
			vArea1Min			= <<-2669.586914,2491.959961,2.043799>>
			vArea1Max			= <<-2617.764648,2841.594727,14.082197>>
			fAreaWidth			= 26.500000
			vArea2Min 			= <<-2687.605713,2494.868164,2.608733>>
			vArea2Max			= <<-2637.084717,2846.875244,14.159884>>
		BREAK
		CASE DOUBLEARCH_CS3
			vArea1Min		= <<-1986.172607,4521.798828,0>>
			vArea1Max		= <<-1809.902588,4699.551270,53.508797>>
			fAreaWidth		= 17.000000
			vArea2Min 		= <<-1995.668091,4531.259277,0>>
			vArea2Max		= <<-1817.543457,4708.394531,53.509171>>
		BREAK
		CASE RAILARCH_CS1
			vArea1Min			= <<-526.026489,4472.442383,0>>
			vArea1Max			= <<-505.571442,4335.724609,86.733109>>
			fAreaWidth			= 10.000000
			vArea2Min 			= <<-519.928101,4476.345703,0>>
			vArea2Max			= <<-500.731323,4336.389160,86.712891>>
		BREAK
		CASE RAILLOW_CS4
			vArea1Min			= <<98.161499,3384.489014,45.451691>>
			vArea1Max			= <<154.974030,3350.694092,30.033585>>
			fAreaWidth			= 8.000000
			vArea2Min 			= <<152.680176,3346.793457,45.021557>>
			vArea2Max			= <<95.571877,3380.090576,30.432842>> 
		BREAK
		CASE ROADLOW_CS4
			vArea1Min			= <<147.860611,3406.796143,38.036716>>
			vArea1Max			= <<126.132935,3416.926758,30.029865>>
			fAreaWidth			= 14.50000
			vArea2Min 			= <<130.091629,3425.417236,38.056725>>
			vArea2Max			= <<151.870346,3415.391357,30.057804>>
		BREAK
		CASE RAILLOW_CS2
			vArea1Min			= <<2830.972168,4967.139648,34.560127>>
			vArea1Max			= <<2818.718750,4992.298340,51.290901>>
			fAreaWidth			= 10.000000
			vArea2Min 			= <<2826.766846,4964.185059,34.106361>>
			vArea2Max			= <<2814.216309,4989.983398,51.218491>>
		BREAK
		CASE RAILLOW_CS3
			vArea1Min			= <<-151.576416,4264.416992,31.047348>>
			vArea1Max			= <<-193.196228,4224.604004,43.872551>>
			fAreaWidth			= 10.000000
			vArea2Min 			= <<-148.384186,4261.071289,31.574089>>
			vArea2Max			= <<-190.471878,4222.076172,43.954426>>
		BREAK
		CASE MADISON_CS3
			vArea1Min			= <<-426.691864,2964.271973,14.848002>>
			vArea1Max			= <<-396.229797,2959.277588,23.506374>>
			fAreaWidth			= 7.000000
			vArea2Min 			= <<-425.028290,2967.861328,15.226991>>
			vArea2Max			= <<-395.607269,2962.606689,24.380791>>
		BREAK
		CASE ROADLOW_CS6
			vArea1Min			= <<-192.341370,2864.916260,30.725950>>
			vArea1Max			= <<-170.150940,2857.128174,43.941822>>
			fAreaWidth			= 10.000000
			vArea2Min 			= <<-192.012863,2871.602539,29.999426>>
			vArea2Max			= <<-169.595749,2863.838379,44.032505>>
		BREAK
		CASE RAILLOW_CH3
			vArea1Min			= <<2539.185059,2228.771729,18.610205>>
			vArea1Max			= <<2574.373291,2169.401367,27.265978>>
			fAreaWidth			= 10.000000
			vArea2Min 			= <<2543.707520,2231.401611,18.331003>>
			vArea2Max			= <<2578.078857,2171.583740,27.260571>>
		BREAK
		CASE SEAARCH_CH3
			vArea1Min			= <<2954.086670,815.720886,0.037901>>
			vArea1Max			= <<2933.189209,796.468811,12.983917>>
			fAreaWidth			= 35.000000
			vArea2Min 			= <<2966.122559,806.888855,0.544056>> 
			vArea2Max			= <<2950.801270,786.781555,11.745959>>
		BREAK
		CASE RAILHIGH_CH3
			vArea1Min			= <<2329.672607,-459.664764,70.242767>>
			vArea1Max			= <<2413.383789,-361.218842,91.778862>>
			fAreaWidth			= 12.000000
			vArea2Min 			= <<2324.751953,-455.523773,70.251450>>
			vArea2Max			= <<2407.409424,-356.200317,91.430832>>
		BREAK
		CASE RAILFREE_CH3
			vArea1Min			= <<1943.428467,-753.250977,80.179047>>
			vArea1Max			= <<1850.364502,-760.958740,93.025215>>
			fAreaWidth			= 7.000000
			vArea2Min 			= <<1943.365967,-758.287048,80.322914>>
			vArea2Max			= <<1851.353516,-765.180664,92.935455>>
		BREAK
		CASE ROADSUSP_PO1
			vArea1Min			= <<-271.284912,-2414.993164,-10>>
			vArea1Max			= <<-542.909607,-2225.855957,52.650043>>
			fAreaWidth			= 30.000000
			vArea2Min 			= <<-279.839874,-2427.211670,-10>> 
			vArea2Max			= <<-551.265564,-2237.790527,52.646339>>
		BREAK
		CASE RAILLOW_CH1
			vArea1Min			= <<-1483.000366,2691.427734,-10>>
			vArea1Max			= <<-1377.168213,2600.768799,15.955276>>
			fAreaWidth			= 12.000000
			vArea2Min 			= <<-1478.152100,2696.687988,-10>>
			vArea2Max			= <<-1378.503174,2608.697754,15.609236>>
		BREAK
		CASE RAILLOW_PO1
			vArea1Min			= <<222.151871,-2343.486572,0.039199>>
			vArea1Max			= <<222.684906,-2297.406982,7.088753>>
			fAreaWidth			= 12.000000
			vArea2Min 			= <<216.959015,-2343.486572,0.207734>>
			vArea2Max			= <<216.602036,-2295.445068,7.424279>>
		BREAK
		CASE ROADLOW_PO1
			vArea1Min			= <<346.462158,-2244.374023,0.159779>>
			vArea1Max			= <<346.834717,-2389.590576,7.852059>>
			fAreaWidth			= 20.000000
			vArea2Min 			= <<359.609039,-2244.467773,0.129684>>
			vArea2Max			= <<355.405426,-2390.258301,7.080691>>
		BREAK
		CASE KNIFE_SM01
			vArea1Min			= <<-1859.679932,-322.635742,56.163681>>
			vArea1Max			= <<-1836.613647,-335.414124,96.116104>>
			fAreaWidth			= 7.500000
			vArea2Min 			= <<-1860.269775,-327.863434,57.542999>>
			vArea2Max			= <<-1837.270874,-339.222717,95.693253>>
		BREAK
		CASE KNIFE_2
			vArea1Min			= <<-680.263245,-600.817993,69.112892>>
			vArea1Max			= <<-706.661255,-600.751526,30.476036>>
			fAreaWidth			= 31.500000
			vArea2Min 			= <<-680.607727,-618.365845,69.274963>>
			vArea2Max			= <<-706.359619,-618.238464,30.312347>>
		BREAK
		CASE KNIFE_3
			vArea1Min			= <<-1468.096069,-591.715759,67.055176>>
			vArea1Max			= <<-1454.699951,-573.451782,29.567360>>
			fAreaWidth			= 11.750000
			vArea2Min 			= <<-1474.902954,-591.121460,67.080910>>
			vArea2Max			= <<-1457.173096,-568.131592,29.440590>>
		BREAK
		CASE KNIFE_4
			vArea1Min			= <<-1544.957764,-537.147522,72.443474>>
			vArea1Max			= <<-1564.616211,-551.182922,32.861633>>
			fAreaWidth			= 11.750000
			vArea2Min 			= <<-1541.007935,-541.549377,71.619720>>
			vArea2Max			= <<-1561.218872,-555.868042,32.927902>> 
		BREAK
		CASE DOMES_PO1
			vArea1Min			= <<333.210785,-2727.273682,20.716625>>
			vArea1Max			= <<333.429688,-2791.608887,41.990227>>
			fAreaWidth			= 20.000000
			vArea2Min 			= <<343.112732,-2727.235840,20.236126>>
			vArea2Max			= <<343.667816,-2791.602295,41.379284>>
		BREAK
		CASE RAILROAD_CS2
			vArea1Min			= <<1928.071289,6228.355469,43.493977>>
			vArea1Max			= <<2039.882324,6167.397461,55.464050>> 
			fAreaWidth			= 13.000000
			vArea2Min 			= <<1931.820190,6235.633789,43.373817>>
			vArea2Max			= <<2039.597534,6176.525391,55.255970>> 
		BREAK
		CASE HELI_UNDER
			vArea1Min			= <<-736.430908,-1590.920776,10.808919>>
			vArea1Max			= <<-710.811035,-1516.349487,-0.098598>> 
			fAreaWidth			= 15.000000
			vArea2Min 			= <<-727.230713,-1585.221191,11.780270>>
			vArea2Max			= <<-700.020142,-1511.782593,-0.341655>> 
			BREAK
		CASE HELI_OVER
			vArea1Min			= <<-676.377502,-1548.552612,12.337475>>
			vArea1Max			= <<-659.328979,-1507.062866,-0.788618>> 
			fAreaWidth			= 25.000000
			vArea2Min 			= <<-654.203003,-1536.145996,9.191055>>
			vArea2Max			= <<-645.995422,-1500.219360,-2.406948>> 
		BREAK
		CASE CANAL_TRACK
			vArea1Min			= <<-624.234375,-1537.045288,12.601933>>
			vArea1Max			= <<-622.174927,-1472.876587,-0.292606>> 
			fAreaWidth			= 8.000000
			vArea2Min 			= <<-615.400330,-1536.650024,12.402705>>
			vArea2Max			= <<-619.738525,-1472.937134,-0.243267>> 
		BREAK
		CASE SPAGH_LOW
			vArea1Min			= <<-492.505707,-1632.457153,24.330700>>
			vArea1Max			= <<-418.208801,-1487.452148,0>>  
			fAreaWidth			= 25.000000
			vArea2Min 			= <<-486.201599,-1636.094971,24.208052>>
			vArea2Max			= <<-398.764832,-1490.440308,0>>  
		BREAK
		CASE CHINOZ_BRIDGE
			vArea1Min			= <<-359.354095,-1639.692749,13.134555>>
			vArea1Max			= <<-388.495483,-1690.945190,-0.183164>> 
			fAreaWidth			= 25.000000
			vArea2Min 			= <<-378.151825,-1705.659668,12.471957>>
			vArea2Max			= <<-348.959106,-1654.411011,0.193478>> 
		BREAK
		CASE SC1_ARCH_BRIDGE
			vArea1Min			= <<-243.443573,-1814.622803,25.694649>>
			vArea1Max			= <<-183.998734,-1780.644653,-0.085802>> 
			fAreaWidth			= 25.000000
			vArea2Min 			= <<-235.131912,-1822.093750,25.865416>>
			vArea2Max			= <<-175.510468,-1788.274780,-0.506062>>
		BREAK
		CASE LOW_CAST_IRON
			vArea1Min		=  <<84.555367,-2046.158813,13.307674>>
			vArea1Max		=  <<17.931641,-2045.151855,-0.031946>> 
			fAreaWidth		=  25.000000
			vArea2Min 		=  <<17.907875,-2035.772949,12.627057>>
			vArea2Max		=  <<84.572067,-2034.183838,0.048385>> 
		BREAK
		CASE IND_PUMPS_1
			vArea1Min		= <<221.502945,-2232.766357,9.886760>>
			vArea1Max		= <<221.682526,-2244.081055,4.479149>> 
			fAreaWidth		= 8.000000
			vArea2Min 		= <<218.583008,-2232.766357,9.886198>>
			vArea2Max		= <<218.728958,-2244.081055,4.535046>>
		BREAK
		CASE IND_PUMPS_2
			vArea1Min		= <<221.651779,-2184.634766,11.664567>>
			vArea1Max		= <<221.613937,-2208.137207,5.876424>>  
			fAreaWidth		= 8.000000
			vArea2Min 		= <<217.899536,-2184.634766,11.540504>>
			vArea2Max		= <<218.332657,-2208.137207,5.864793>> 
		BREAK
		CASE CH3_OVERPASS
			vArea1Min		= <<2326.570068,1096.030518,76.314575>>
			vArea1Max		= <<2290.332031,1136.130737,58.857056>> 
			fAreaWidth		= 21.000000
			vArea2Min 		= <<2334.453369,1103.066772,76.266029>>
			vArea2Max		= <<2297.845703,1142.896851,58.842430>>
		BREAK
		CASE SP_CH3_RAILBRIDGE
			vArea1Min		= <<2379.442139,1150.775513,58.796318>>
			vArea1Max		= <<2327.657959,1212.366333,72.833298>> 
			fAreaWidth		= 12.000000
			vArea2Min 		= <<2374.063721,1146.282104,58.833305>>
			vArea2Max		= <<2320.894531,1209.596069,72.792992>>
		BREAK
		CASE KNIFE_5
			vArea1Min		= <<-1179.405151,-355.255432,56.530029>>
			vArea1Max		= <<-1198.064087,-357.836304,35.355511>>
			fAreaWidth		= 12.500000
			vArea2Min 		= <<-1178.385254,-361.878418,56.150814>>
			vArea2Max		= <<-1197.103760,-364.700439,36.494755>> 
		BREAK
		CASE KNIFE_6
			vArea1Min		= <<-921.384583,-384.939972,137.018127>>
			vArea1Max		= <<-912.432373,-429.228973,36.701126>> 
			fAreaWidth		= 16.000000
			vArea2Min 		= <<-914.165771,-387.944366,137.079361>>
			vArea2Max		= <<-906.253357,-424.691040,47.118820>>  
		BREAK
		CASE KNIFE_7
			vArea1Min		= <<-740.256409,246.452850,132.292191>>
			vArea1Max		= <<-718.360229,201.004150,80.955711>> 
			fAreaWidth		= 22.000000
			vArea2Min 		= <<-726.642883,253.067642,132.331940>>
			vArea2Max		= <<-705.585815,210.433563,78.705727>>
		BREAK
		CASE KNIFE_8
			vArea1Min		= <<-771.206787,268.272888,132.168915>>
			vArea1Max		= <<-778.341675,313.114807,84.270538>> 
			fAreaWidth		= 16.000000
			vArea2Min 		= <<-770.803528,310.862518,137.416138>>
			vArea2Max		= <<-763.068054,269.044037,83.314743>> 
		BREAK
		CASE KNIFE_9
			vArea1Min		= <<259.220520,135.414612,136.708267>>
			vArea1Max		= <<279.239624,128.137939,100.823303>>
			fAreaWidth		= 16.000000
			vArea2Min 		= <<261.969360,142.967636,136.688919>>
			vArea2Max		= <<281.720337,134.955078,100.770416>>
		BREAK
		CASE KNIFE_10
			vArea1Min		= <<393.547974,-30.871658,152.663452>>
			vArea1Max		= <<369.962189,-23.884607,88.357758>> 
			fAreaWidth		= 8.000000
			vArea2Min 		= <<390.535797,-36.088818,152.781250>>
			vArea2Max		= <<368.127472,-28.818884,88.694473>> 
		BREAK
		CASE KNIFE_11
			vArea1Min		= <<114.313911,-648.429749,261.848785>>
			vArea1Max		= <<131.078156,-733.768372,39.343933>> 
			fAreaWidth		= 20.000000
			vArea2Min 		= <<124.846703,-646.657532,261.848785>>
			vArea2Max		= <<140.050247,-737.427002,39.349304>> 
		BREAK
		CASE KNIFE_12
			vArea1Min		= <<-215.918991,-823.843628,126.022392>>
			vArea1Max		= <<-193.223679,-761.778137,27.913818>> 
			fAreaWidth		= 20.000000
			vArea2Min 		= <<-225.396957,-820.393738,126.081223>>
			vArea2Max		= <<-202.943253,-758.257019,27.477341>>
		BREAK
		CASE KNIFE_13
			vArea1Min		= <<-296.472504,-802.081543,95.401085>>
			vArea1Max		= <<-278.135193,-747.784119,50.400459>>
			fAreaWidth		= 20.000000
			vArea2Min 		= <<-305.460236,-798.836914,95.481941>>
			vArea2Max		= <<-285.737640,-745.095886,49.576508>>
		BREAK
		CASE KNIFE_14
			vArea1Min		= <<-292.303436,-823.356873,95.376205>>
			vArea1Max		= <<-258.599060,-835.563232,27.979462>>
			fAreaWidth		= 20.000000
			vArea2Min 		= <<-288.920624,-814.022034,95.375565>>
			vArea2Max		= <<-255.211594,-826.256042,27.737495>>
		BREAK
		CASE KNIFE_15
			vArea1Min		= <<-256.358856,-714.783752,110.161659>>
			vArea1Max		= <<-212.905441,-730.532043,32.219460>> 
			fAreaWidth		= 20.000000
			vArea2Min 		= <<-253.772324,-705.663208,110.173569>>
			vArea2Max		= <<-210.058777,-722.674805,32.465363>>
		BREAK
		CASE BRIDGE_47
			vArea1Min		= <<1808.214478,1949.245850,71.737068>>
			vArea1Max		= <<1837.906250,2127.283203,52.804909>>
			fAreaWidth		= 9.750000
			vArea2Min 		= <<1802.785889,1950.261719,71.740021>>
			vArea2Max		= <<1831.994385,2127.432617,52.838928>>
		BREAK
		CASE BRIDGE_48
			vArea1Min		= <<2388.732910,2931.940918,46.626808>>
			vArea1Max		= <<2426.680664,2883.066162,36.215237>>
			fAreaWidth		= 10.000000
			vArea2Min 		= <<2392.546631,2934.867188,46.626797>>
			vArea2Max		= <<2430.332520,2885.908447,36.281479>>
		BREAK
		CASE BRIDGE_49
			vArea1Min		= <<2700.055664,4836.380859,42.078537>>
			vArea1Max		= <<2685.673340,4893.380371,30.908669>>
			fAreaWidth		= 20.750000
			vArea2Min		= <<2685.671875,4821.652832,42.184708>>
			vArea2Max		= <<2672.812256,4880.356445,31.133106>>
		BREAK
		CASE BRIDGE_50
			vArea1Min		= <<-1053.446411,4766.245117,234.325119>>
			vArea1Max		= <<-1040.263428,4737.156738,204.491638>>
			fAreaWidth		= 5.000000
			vArea2Min 		= <<-1051.414307,4767.192871,234.429306>>
			vArea2Max		= <<-1037.954224,4738.354492,204.528152>>
		BREAK
	ENDSWITCH
ENDPROC

PROC RESET_BRIDGE_CHECKS()
	bEnteredArea1 			= FALSE
	bEnteredArea2 			= FALSE
	bExitViaArea1 			= FALSE
	bExitViaArea2			= FALSE
	bPassingThroughAreas	= FALSE
	underBridgeStage = 0
ENDPROC

PROC GET_CURRENT_BRIDGE(BOOL bIncludeKnifeFlights = TRUE,BOOL bIncludeDifficultBridges = TRUE)
	INT i
	
	vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	FLOAT fMinDist = 9999999.90
	FLOAT fCurrentDist
	REPEAT NUMBER_OF_CHALLENGES i
		IF (bIncludeKnifeFlights OR NOT IS_INDEX_A_KNIFE_FLIGHT_MP(i))
		AND (bIncludeDifficultBridges OR NOT IS_BRIDGE_DIFFICULT(i))
		
			IF ARE_VECTORS_ALMOST_EQUAL(vPlayerPosition, vTriggerPoint[i],100.0)
				fCurrentDist = VDIST(vPlayerPosition, vTriggerPoint[i])
				IF fCurrentDist < fMinDist
					fMinDist = fCurrentDist 
				//	PRINTLN("<MP - flyUnderBridges> - GET_CURRENT_BRIDGE - currentBridge = ", currentBridge)
					currentBridge = i
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	GET_CURRENT_BRIDGE_INFO(currentBridge)

ENDPROC

FUNC BOOL HAS_PLAYER_FLOWN_UNDER_BRIDGE()

	IF bCompletedABridge = TRUE
		bCompletedABridge = FALSE
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

PROC CHECK_BRIDGES_COMPLETE(BOOL bIncludeKnifeFlights = TRUE, BOOL bAllowCollisions = FALSE, BOOL bIncludeDifficultBridges = TRUE)

	GET_CURRENT_BRIDGE(bIncludeKnifeFlights,bIncludeDifficultBridges)
	
	// for freemode challenges - be a bit more lax about locates - Rob B
	IF bAllowCollisions
		IF vArea1Min.z < vArea1Max.z
			vArea1Min.z = 0
		ELSE
			vArea1Max.z = 0
		ENDIF
		
		IF vArea2Min.z < vArea2Max.z
			vArea2Min.z = 0
		ELSE
			vArea2Max.z = 0
		ENDIF
	ENDIF
	
	SWITCH underBridgeStage
	
		CASE 0
//					IF TIMERA() > (1000/NUMBER_OF_BRIDGES) // Check each area only once per second.
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vTriggerPoint[currentBridge]) < fStartChecking // Wait until player is within fStartChecking before starting checks.
						IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
						OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
							IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
									bEnteredArea1 = TRUE
									underBridgeStage = 1
									PRINTLN("<MP - flyUnderBridges> - bEnteredArea1 ", currentBridge, ".")
								ENDIF
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
									bEnteredArea2 = TRUE
									underBridgeStage = 1
									PRINTLN("<MP - flyUnderBridges> - bEnteredArea2 ", currentBridge, ".")
								ENDIF
							ENDIF
						ENDIF
					ELSE
//								IF NOT bEnteredArea1
//								OR NOT bEnteredArea2
//									currentBridge++
//									IF currentBridge = NUMBER_OF_BRIDGES
//										currentBridge = 0
//									ENDIF
//								ENDIF
					ENDIF
				ENDIF
				
//					ENDIF
		BREAK
		
		
		CASE 1
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
				AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
					RESET_BRIDGE_CHECKS()
					PRINTLN("<MP - flyUnderBridges> - Player is no longer in a flying vehicle. Reset & wait.")
				ELSE
					IF NOT IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						RESET_BRIDGE_CHECKS()
						PRINTLN("<MP - flyUnderBridges> - Player's vehicle is no longer driveable. Reset & wait.")
					ELIF NOT IS_ENTITY_IN_AIR(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					AND NOT bAllowCollisions
						RESET_BRIDGE_CHECKS()
						PRINTLN("<MP - flyUnderBridges> - Player's vehicle is touching the ground. Reset & wait.")
					ENDIF
				ENDIF
			
		
		
				IF bEnteredArea1
				
					IF bPassingThroughAreas
						IF bExitViaArea2
							IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
								//Passed Through
								RESET_BRIDGE_CHECKS()
								bCompletedABridge = TRUE
								PRINTLN("<MP - flyUnderBridges> - Succesful! bExitViaArea2 for bridge number ", currentBridge, ".")
							ENDIF
						ELSE
							IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
							AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
								bExitViaArea2 = TRUE
							ELSE
								IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
								AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
									//Never passed through
									RESET_BRIDGE_CHECKS()
									PRINTLN("<MP - flyUnderBridges> - Unsuccessful pass under bridge ", currentBridge, ".")
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
						AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
							bPassingThroughAreas = TRUE
						ELSE
							IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
								//Never passed through
								RESET_BRIDGE_CHECKS()
								PRINTLN("<MP - flyUnderBridges> - Unsuccessful pass under bridge ", currentBridge, ".")
							ENDIF
						ENDIF
					ENDIF
					
				ELIF bEnteredArea2
					IF bPassingThroughAreas
						IF bExitViaArea1
							IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
								//Passed Through
								RESET_BRIDGE_CHECKS()
								bCompletedABridge = TRUE
								PRINTLN("<MP - flyUnderBridges> - Succesful! bExitViaArea1 for bridge number ", currentBridge, ".")
							ENDIF
						ELSE
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
								bExitViaArea1 = TRUE
							ELSE
								IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
								AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
									//Never passed through
									RESET_BRIDGE_CHECKS()
									PRINTLN("<MP - flyUnderBridges> - Unsuccessful pass under bridge ", currentBridge, ".")
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
						AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
							bPassingThroughAreas = TRUE
						ELSE
							IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea1Min, vArea1Max, fAreaWidth)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vArea2Min, vArea2Max, fAreaWidth)
								//Never passed through
								RESET_BRIDGE_CHECKS()
								PRINTLN("<MP - flyUnderBridges> - Unsuccessful pass under bridge ", currentBridge, ".")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK

	ENDSWITCH

ENDPROC

PROC GET_BRIDGE_LOCATIONS(VECTOR &vLoc[NUMBER_OF_CHALLENGES])
	
	INT i
	REPEAT NUMBER_OF_CHALLENGES i
		IF NOT IS_INDEX_A_KNIFE_FLIGHT_MP(i)
		AND NOT IS_BRIDGE_DIFFICULT(i)
			vLoc[i] = vTriggerPoint[i]
		ENDIF
	ENDREPEAT

ENDPROC

PROC GET_BRIDGE_AND_KNIFE_FLIGHT_LOCATIONS(VECTOR &vLoc[NUMBER_OF_CHALLENGES])
	
	INT i
	REPEAT NUMBER_OF_CHALLENGES i
		IF NOT IS_BRIDGE_DIFFICULT(i)
			vLoc[i] = vTriggerPoint[i]
		ENDIF
	ENDREPEAT

ENDPROC

#IF FEATURE_STUNT_FM_EVENTS
PROC GET_KNIFE_FLIGHT_LOCATIONS(VECTOR &vLoc[NUMBER_OF_CHALLENGES])
	
	INT i
	REPEAT NUMBER_OF_CHALLENGES i
		IF IS_INDEX_A_KNIFE_FLIGHT_MP(i)
			vLoc[i] = vTriggerPoint[i]
		ENDIF
	ENDREPEAT

ENDPROC
#ENDIF

FUNC INT GET_STORED_CURRENT_BRIDGE()

	RETURN currentBridge

ENDFUNC



