
// ====================================================================================
// ====================================================================================
//
// Name:        net_MP_TV.sch
// Description: Header for multiplayer TV, including CCTV and breaking news
// Written By:  David Gentles
//
// ====================================================================================
// ====================================================================================
//USING "net_hud_activating.sch"
USING "globals.sch"
// Game Headers
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_path.sch"

USING "freemode_header.sch"

USING "rage_builtins.sch"
USING "net_spectator_cam.sch"
USING "net_realty_details.sch"
USING "tv_control_public.sch"
USING "context_control_public.sch"
USING "net_realty_new.sch"

#IF IS_DEBUG_BUILD
USING "net_debug_log.sch"
USING "profiler.sch"
#ENDIF

/// PURPOSE: List of client stage IDs
ENUM MP_TV_CLIENT_STAGE
	MP_TV_CLIENT_STAGE_INIT,		//client is setting up the TV
	MP_TV_CLIENT_STAGE_WALKING,		//client is in the property but not seated, can sit down
	MP_TV_CLIENT_STAGE_SITTING,		//client is seated but not activated the tv, can activate the tv or leave the seat
	MP_TV_CLIENT_STAGE_ACTIVATED	//client is seated and has activated the tv, can now change channels and volume, or deactivate controlling the tv
ENDENUM

/// PURPOSE: List of server stage IDs
ENUM MP_TV_SERVER_STAGE
	MP_TV_SERVER_STAGE_INIT,	//server is setting up the TV
	MP_TV_SERVER_STAGE_ON,		//tv is on
	MP_TV_SERVER_STAGE_OFF		//tv is off
ENDENUM

/// PURPOSE: List of tv channel IDs
ENUM MP_TV_CHANNEL
	MP_TV_CHANNEL_CCTV_0 = 0,	//cctv channels are fullscreen, fixed camera channels with a filter applied
	MP_TV_CHANNEL_CCTV_1,
	MP_TV_CHANNEL_CCTV_2,
	MP_TV_CHANNEL_CCTV_GARAGE,
	
	MP_TV_CHANNEL_SPECTATE,		//spectate and news are variations on the spectator cam allowing the player to watch other players
	MP_TV_CHANNEL_NEWS,
	
	MP_TV_CHANNEL_CNT,			//cnt and weazel are the two channels from SP TV
	MP_TV_CHANNEL_WEAZEL,
	MP_TV_CHANNEL_ARCADE
ENDENUM

/// PURPOSE: List of TV channel changing switch state IDs
ENUM MP_TV_SWITCH_STATE
	MP_TV_SWITCH_STATE_SETUP_PROCESS = 0,
	MP_TV_SWITCH_STATE_CRIMINAL_LOST,	//special case for when news channel focus loses wanted level and channel has to shut down
	MP_TV_SWITCH_STATE_FADE_OUT,
	MP_TV_SWITCH_STATE_CLEANUP_CURRENT,
	MP_TV_SWITCH_STATE_SETUP_DESIRED,
	MP_TV_SWITCH_STATE_SETUP_LOAD_SCENE,
	MP_TV_SWITCH_STATE_WAIT_FOR_LOAD_SCENE,
	MP_TV_SWITCH_STATE_LOAD_INTERIOR,
	MP_TV_SWITCH_STATE_CLEANUP_LOAD_SCENE,
	MP_TV_SWITCH_STATE_SETUP_RENDERTARGET,
	MP_TV_SWITCH_STATE_WAIT_FOR_FILTER_TIME,
	MP_TV_SWITCH_STATE_FADE_IN,
	MP_TV_SWITCH_STATE_CLEANUP_PROCESS,
	MP_TV_SWITCH_STATE_WATCH_CHANNEL
ENDENUM

/// PURPOSE: List of info on force into TV seat result IDs
ENUM FORCE_INTO_TV_SEAT_RESULT
	FORCE_INTO_TV_SEAT_ATTEMPTING = 0,
	FORCE_INTO_TV_SEAT_FAIL,
	FORCE_INTO_TV_SEAT_SUCCESS,
	FORCE_INTO_TV_SEAT_ALREADY_SEATED
ENDENUM

CONST_FLOAT MP_TV_MAX_VOLUME				0.0
CONST_FLOAT MP_TV_MIN_VOLUME				-36.0
CONST_FLOAT MP_TV_VOLUME_INCREMENT			4.0//0.5
CONST_INT MP_TV_INCREMENT_WAIT_TIME			100
CONST_INT MP_TV_CHANNEL_CHANGE_WAIT_TIME	1000
CONST_INT MP_TV_FADE_TIME_CHANNEL_CHANGE	500//0
CONST_INT MP_TV_FADE_TIME_POWER_CHANGE		500//2500
CONST_INT MP_TV_NEWS_TICKER_TIME_UPDATE		5000
CONST_INT MP_TV_NUMBER_OF_NEWS_TICKER		5
CONST_INT MP_TV_SUSPECT_LOST_TIME			2000
CONST_INT MP_TV_ABANDON_SEAT_TIME			10000
CONST_INT MP_TV_HELP_TEXT_DISTANCE_SQR		81//9sqr
CONST_INT MP_TV_NUMBER_OF_CAMERAS			2
CONST_INT MP_TV_CHECK_PLAYERS_TIME			1000
CONST_INT MP_TV_LOAD_SCENE_TIMEOUT			10000
CONST_INT MP_TV_LOAD_INTERIOR_TIMEOUT		5000
CONST_INT MP_TV_SETUP_RENDERTARGET_TIMEOUT	10000
CONST_INT MP_TV_NUM_SEATS_LOW_END			4
CONST_INT MP_TV_NUM_SEATS_MID_RANGE			4
CONST_INT MP_TV_NUM_SEATS_HIGH_END			6
CONST_INT MP_TV_NUM_SEATS_BUSINESS			6


//local iBitset
CONST_INT MPTV_LOCAL_BS_UPDATE_BUTTONS								0
CONST_INT MPTV_LOCAL_BS_FOCUS_SET									1
CONST_INT MPTV_LOCAL_BS_DISABLED_SUICIDE							2
CONST_INT MPTV_LOCAL_BS_DISABLED_INTERACTION						3
CONST_INT MPTV_LOCAL_BS_OBJECT_CREATED								4
CONST_INT MPTV_LOCAL_BS_RENDER_TARGET_FOUND							5
CONST_INT MPTV_LOCAL_BS_STAND_UP_TASK_GIVEN							6
CONST_INT MPTV_LOCAL_BS_WATCH_TV_CAMERA_SETUP						7
CONST_INT MPTV_LOCAL_BS_WATCH_TV_CLEANUP							8
CONST_INT MPTV_LOCAL_BS_SERVER_TIMER_CHANNEL_CHANGE_SET				9
CONST_INT MPTV_LOCAL_BS_SERVER_TIMER_GARAGE_CHANNEL_CHANGE_SET		10
CONST_INT MPTV_LOCAL_BS_REVEAL_BUILDING_EXTERIOR					11
CONST_INT MPTV_LOCAL_BS_DISABLE_EXTERIOR_FOCUS						12
CONST_INT MPTV_LOCAL_BS_STATIC_OUTSIDE_CAM_ON						13
CONST_INT MPTV_LOCAL_BS_AUDIO_FRONTEND								14
CONST_INT MPTV_LOCAL_BS_SHOULD_RENDER_TARGET_EXIST					15
CONST_INT MPTV_LOCAL_BS_SCREEN_OBJECT_VISIBLE						16
CONST_INT MPTV_LOCAL_BS_DONT_RENDER_RADAR_IN_CLEANUP				17
CONST_INT MPTV_LOCAL_BS_CLEAR_DISPLAY								18
CONST_INT MPTV_LOCAL_BS_FORCE_SCALEFORM_DRAW						19
CONST_INT MPTV_LOCAL_BS_REQUEST_APARTMENT_AUDIO_SCENE				20
CONST_INT MPTV_LOCAL_BS_FORCE_STRIPPER_MUSIC						21
CONST_INT MPTV_LOCAL_BS_SET_FOR_STRIPPER_MUSIC						22
CONST_INT MPTV_LOCAL_BS_DRAW_LOADING_ICON							23
CONST_INT MPTV_LOCAL_BS_STORED_SEAT_POSITIONS						24
CONST_INT MPTV_LOCAL_BS_SET_WATCHING_TV								25
CONST_INT MPTV_LOCAL_BS_PAUSED_RENDERPHASE							26
CONST_INT MPTV_LOCAL_BS_OBJECT_GRABBED								27
CONST_INT MPTV_LOCAL_BS_CLEANED_UP									28
CONST_INT MPTV_LOCAL_BS_FORCE_STAGE_SITTING							29
CONST_INT MPTV_LOCAL_BS_DCTL_SEAT									30
CONST_INT MPTV_LOCAL_BS_SECOND_TV									31

// local iBitset2
CONST_INT MPTV_LOCAL_BS2_THIRD_TV									0

//local iSwitchBitset
CONST_INT MPTV_SWITCH_BS_LOAD_SCENE_REQUIRED				0
CONST_INT MPTV_SWITCH_BS_LOAD_SCENE_RUNNING					1
CONST_INT MPTV_SWITCH_BS_FADE_REQUIRED						2
CONST_INT MPTV_SWITCH_BS_TURNING_ON							3
CONST_INT MPTV_SWITCH_BS_TURNING_OFF						4
CONST_INT MPTV_SWITCH_BS_ACTIVATING							5
CONST_INT MPTV_SWITCH_BS_DEACTIVATING						6
CONST_INT MPTV_SWITCH_BS_REQUESTED_TURN_ON					7
CONST_INT MPTV_SWITCH_BS_REQUESTED_TURN_OFF					8
CONST_INT MPTV_SWITCH_BS_TURNED_ON							9
CONST_INT MPTV_SWITCH_BS_TURNED_OFF							10

CONST_INT ciMPTV_SETTINGS_BS_PREVENT_FADE_IN_THIS_FRAME		0

/// PURPOSE: Struct of data that is only requried locally
STRUCT MP_TV_LOCAL_DATA_STRUCT
	INT iBitset
	INT iBitset2
	
	//local tv render/control
	INT iRenderTarget = -1
	INT iContextButtonIntention = NEW_CONTEXT_INTENTION
	#IF FEATURE_BENCH_RELATIONSHIPS
	INT iContextButtonIntentionRel = NEW_CONTEXT_INTENTION
	#ENDIF
	OBJECT_INDEX objOverlay
	CAMERA_INDEX camCCTV
	CAMERA_INDEX camRoom
	SCALEFORM_INDEX sfTV
	INT iCurrentCamera = 0
	INT iSoundCCTVBackground = -1
	INT iSoundCCTVChangeCam = -1
	
	//local server
	INT iServerSeatStagger = 0
	TIME_DATATYPE timeServerChannelChange
	TIME_DATATYPE timeServerGarageChannelChange
	INT iServerPlayerStagger = 0
	
	//local client
	INT iClientSeatStagger = 0
	INT iNearestSeat = -1
	
	//local channel switch
	VECTOR vLoadSceneLocation
	VECTOR vLoadSceneRot
	INTERIOR_INSTANCE_INDEX interiorGarage
	INT iChannelSwitchTimer = 0
	MP_TV_SWITCH_STATE eSwitchState = MP_TV_SWITCH_STATE_WATCH_CHANNEL
	MP_TV_CHANNEL eSwitchChannelFrom = MP_TV_CHANNEL_CCTV_0
	MP_TV_CHANNEL eSwitchChannelTo = MP_TV_CHANNEL_CCTV_0
	INT iSwitchBitset = 0
	TIME_DATATYPE timeIncrement
	SCALEFORM_LOADING_ICON loadingIcon
	INT iTimeCheckPlayers
	
	TIME_DATATYPE timeNewsFilterNextUpdate
	FLOAT fNewsFilterScrollEntry = 0.0
	
	TIME_DATATYPE timeAbandonSeat
	SCRIPT_TIMER timeSetupRendertargetFail
	
	SCRIPT_TIMER timeoutInitStage
	
	//Force into TV seat
	INT iForceIntoTVSeatStage = 0
	
	INT iOwnerFindBail = 0
	
	TVCHANNELTYPE tvChannelTypeCurrent = TVCHANNELTYPE_CHANNEL_NONE
	MP_TV_CHANNEL eCurrentChannel = MP_TV_CHANNEL_CNT
	SPECTATOR_DATA_STRUCT specData
	
	MP_PROP_OFFSET_STRUCT seatPositions[7]
	MP_PROP_OFFSET_STRUCT seatLocates0[7]
	MP_PROP_OFFSET_STRUCT seatLocates1[7]
	
	#IF IS_DEBUG_BUILD
		FLOAT fWdCurrentRenderPosX
		FLOAT fWdCurrentRenderPosY
		FLOAT fWdCurrentRenderPosZ
		
		FLOAT fWdDesiredRenderPosX
		FLOAT fWdDesiredRenderPosY
		FLOAT fWdDesiredRenderPosZ
		
		BOOL bWdUpdateRenderPos = FALSE
		BOOL bWdWidgetsCreated = FALSE
	#ENDIF
	INT iBSSettings
	
	BOOL bInCasinoApartment = FALSE
	
	#IF FEATURE_CASINO_HEIST
	BOOL bInArcade = FALSE
	#ENDIF
	
	#IF FEATURE_FIXER
	BOOL bInFixerHQ = FALSE
	#ENDIF
ENDSTRUCT


//client iBitset
CONST_INT MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT			0
CONST_INT MPTV_CLIENT_BS_LEAVING_SEAT					1
CONST_INT MPTV_CLIENT_BS_OCCUPYING_SEAT					2
CONST_INT MPTV_CLIENT_BS_IN_GARAGE						3
CONST_INT MPTV_CLIENT_BS_ABANDONING_SEAT				4
CONST_INT MPTV_CLIENT_BS_PREVIOUS_SAVE_SET				5
CONST_INT MPTV_CLIENT_BS_PREVIOUS_SAVE_ON				6
CONST_INT MPTV_CLIENT_BS_GARAGE_PREVIOUS_SAVE_ON		7
CONST_INT MPTV_CLIENT_BS_RELATIONSHIP_SEAT      		8
CONST_INT MPTV_CLIENT_BS_RELATIONSHIP_SECOND_SEAT   	9
CONST_INT MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SECOND_SEAT	10
CONST_INT MPTV_CLIENT_BS_LEAVING_SECOND_SEAT			11
CONST_INT MPTV_CLIENT_BS_PLAYER_AT_SEAT_NODE_COORD		12

//client iInputBitset
CONST_INT MPTV_INPUT_POWER_ON						0
CONST_INT MPTV_INPUT_POWER_OFF						1
CONST_INT MPTV_INPUT_NEXT_CHANNEL					2
CONST_INT MPTV_INPUT_PREVIOUS_CHANNEL				3
CONST_INT MPTV_INPUT_VOLUME_UP						4
CONST_INT MPTV_INPUT_VOLUME_DOWN					5

/// PURPOSE: Client broadcast data struct
STRUCT MP_TV_CLIENT_DATA_STRUCT
	INT iBitset
	INT iInputBitset
	
	INT iPreviousSavedChannel = -1
	INT iPreviousSavedGarageChannel = -1
	
	MP_TV_CHANNEL eTimeOfInputChannel = MP_TV_CHANNEL_CCTV_0
	FLOAT fTimeOfInputVolume = 0.0
	
	INT iSeat = -1
	
	MP_TV_CHANNEL eDesiredChannel = MP_TV_CHANNEL_CNT
	MP_TV_CLIENT_STAGE eStage = MP_TV_CLIENT_STAGE_INIT
ENDSTRUCT


CONST_INT MAX_NUMBER_OF_SEATS 8

//seat iBitset
CONST_INT MPTV_SEAT_BS_OCCUPIED						0
CONST_INT MPTV_SEAT_BS_OCCUPIER_ENTERING			1
CONST_INT MPTV_SEAT_BS_OCCUPIER_LEAVING				2
CONST_INT MPTV_SEAT_BS_SECOND_OCCUPIER_LEAVING		3
CONST_INT MPTV_SEAT_BS_FULL 						4

/// PURPOSE: Server data struct for MP TV seat
STRUCT MP_TV_SEAT_STRUCT
	INT iBitset = 0
	
	INT iCurrentOccupyPlayer = -1
	#IF FEATURE_BENCH_RELATIONSHIPS
	INT iSecondOccupyPlayer = -1
	#ENDIF
ENDSTRUCT


//server iBitset
CONST_INT MPTV_SERVER_BS_NEWS_CHANNEL_LOST_CRIMINAL	0

/// PURPOSE: server broadcast data
STRUCT MP_TV_SERVER_DATA_STRUCT
	INT iBitset
	
	FLOAT fVolume = 0.0
	FLOAT fGarageVolume = 0.0
	
	MP_TV_SEAT_STRUCT sSeat[MAX_NUMBER_OF_SEATS]
	
	MP_TV_CHANNEL eCurrentChannel = MP_TV_CHANNEL_CNT
	MP_TV_CHANNEL eCurrentGarageChannel = MP_TV_CHANNEL_CNT
	
	MP_TV_SERVER_STAGE eStage = MP_TV_SERVER_STAGE_INIT
	MP_TV_SERVER_STAGE eGarageStage = MP_TV_SERVER_STAGE_INIT
ENDSTRUCT

PROC MP_TV_BLOCK_FADE_IN_THIS_FRAME(MP_TV_LOCAL_DATA_STRUCT& data)
	SET_BIT(data.iBSSettings, ciMPTV_SETTINGS_BS_PREVENT_FADE_IN_THIS_FRAME)
ENDPROC

#IF IS_DEBUG_BUILD
	
	/// PURPOSE:
	///    Get TV client stage debug print
	/// PARAMS:
	///    eStage - stage to get name of
	/// RETURNS:
	///    stage name as STRING
	FUNC STRING GET_MP_TV_CLIENT_STAGE_NAME(MP_TV_CLIENT_STAGE eStage)
		SWITCH eStage
			CASE MP_TV_CLIENT_STAGE_INIT
				RETURN "MP_TV_CLIENT_STAGE_INIT"
			BREAK
			CASE MP_TV_CLIENT_STAGE_WALKING
				RETURN "MP_TV_CLIENT_STAGE_WALKING"
			BREAK
			CASE MP_TV_CLIENT_STAGE_SITTING
				RETURN "MP_TV_CLIENT_STAGE_SITTING"
			BREAK
			CASE MP_TV_CLIENT_STAGE_ACTIVATED
				RETURN "MP_TV_CLIENT_STAGE_ACTIVATED"
			BREAK
		ENDSWITCH
		RETURN "INVALID_CLIENT_STAGE"
	ENDFUNC
	
	/// PURPOSE:
	///    Get TV server stage debug print
	/// PARAMS:
	///    eStage - stage to get name of
	/// RETURNS:
	///    stage name as STRING
	FUNC STRING GET_MP_TV_SERVER_STAGE_NAME(MP_TV_SERVER_STAGE eStage)
		SWITCH eStage
			CASE MP_TV_SERVER_STAGE_INIT
				RETURN "MP_TV_SERVER_STAGE_INIT"
			BREAK
			CASE MP_TV_SERVER_STAGE_ON
				RETURN "MP_TV_SERVER_STAGE_ON"
			BREAK
			CASE MP_TV_SERVER_STAGE_OFF
				RETURN "MP_TV_SERVER_STAGE_OFF"
			BREAK
		ENDSWITCH
		RETURN "INVALID_SERVER_STAGE"
	ENDFUNC
	
	/// PURPOSE:
	///    Get TV channel debug print
	/// PARAMS:
	///    eChannel - channel to get name of
	/// RETURNS:
	///    channel name as STRING
	FUNC STRING GET_MP_TV_CHANNEL_NAME(MP_TV_CHANNEL eChannel)
		SWITCH eChannel
			CASE MP_TV_CHANNEL_CCTV_0
				RETURN "MP_TV_CHANNEL_CCTV_0"
			BREAK
			CASE MP_TV_CHANNEL_CCTV_1
				RETURN "MP_TV_CHANNEL_CCTV_1"
			BREAK
			CASE MP_TV_CHANNEL_CCTV_2
				RETURN "MP_TV_CHANNEL_CCTV_2"
			BREAK
			CASE MP_TV_CHANNEL_CCTV_GARAGE
				RETURN "MP_TV_CHANNEL_CCTV_GARAGE"
			BREAK
			CASE MP_TV_CHANNEL_SPECTATE
				RETURN "MP_TV_CHANNEL_SPECTATE"
			BREAK
			CASE MP_TV_CHANNEL_NEWS
				RETURN "MP_TV_CHANNEL_NEWS"
			BREAK
			CASE MP_TV_CHANNEL_CNT
				RETURN "MP_TV_CHANNEL_CNT"
			BREAK
			CASE MP_TV_CHANNEL_WEAZEL
				RETURN "MP_TV_CHANNEL_WEAZEL"
			BREAK
			CASE MP_TV_CHANNEL_ARCADE
				RETURN "MP_TV_CHANNEL_ARCADE"
			BREAK
		ENDSWITCH
		RETURN "INVALID_CHANNEL"
	ENDFUNC
	
	/// PURPOSE:
	///    Get force into TV seat debug print
	/// PARAMS:
	///    eResult - result to get name of
	/// RETURNS:
	///    result name as STRING
	FUNC STRING GET_FORCE_INTO_TV_SEAT_RESULT_NAME(FORCE_INTO_TV_SEAT_RESULT eResult)
		SWITCH eResult
			CASE FORCE_INTO_TV_SEAT_ATTEMPTING
				RETURN "FORCE_INTO_TV_SEAT_ATTEMPTING"
			BREAK
			CASE FORCE_INTO_TV_SEAT_FAIL
				RETURN "FORCE_INTO_TV_SEAT_FAIL"
			BREAK
			CASE FORCE_INTO_TV_SEAT_SUCCESS
				RETURN "FORCE_INTO_TV_SEAT_SUCCESS"
			BREAK
		ENDSWITCH
		RETURN "INVALID_RESULT"
	ENDFUNC
	
	/// PURPOSE:
	///    Get TV channel switch state debug print
	/// PARAMS:
	///    eState - state to get name of
	/// RETURNS:
	///    state name as STRING
	FUNC STRING GET_MP_TV_SWITCH_STATE_NAME(MP_TV_SWITCH_STATE eState)
		SWITCH eState
			CASE MP_TV_SWITCH_STATE_SETUP_PROCESS
				RETURN "MP_TV_SWITCH_STATE_SETUP_PROCESS"
			BREAK
			CASE MP_TV_SWITCH_STATE_CRIMINAL_LOST
				RETURN "MP_TV_SWITCH_STATE_CRIMINAL_LOST"
			BREAK
			CASE MP_TV_SWITCH_STATE_FADE_OUT
				RETURN "MP_TV_SWITCH_STATE_FADE_OUT"
			BREAK
			CASE MP_TV_SWITCH_STATE_CLEANUP_CURRENT
				RETURN "MP_TV_SWITCH_STATE_CLEANUP_CURRENT"
			BREAK
			CASE MP_TV_SWITCH_STATE_SETUP_DESIRED
				RETURN "MP_TV_SWITCH_STATE_SETUP_DESIRED"
			BREAK
			CASE MP_TV_SWITCH_STATE_SETUP_LOAD_SCENE
				RETURN "MP_TV_SWITCH_STATE_SETUP_LOAD_SCENE"
			BREAK
			CASE MP_TV_SWITCH_STATE_WAIT_FOR_LOAD_SCENE
				RETURN "MP_TV_SWITCH_STATE_WAIT_FOR_LOAD_SCENE"
			BREAK
			CASE MP_TV_SWITCH_STATE_LOAD_INTERIOR
				RETURN "MP_TV_SWITCH_STATE_LOAD_INTERIOR"
			BREAK
			CASE MP_TV_SWITCH_STATE_CLEANUP_LOAD_SCENE
				RETURN "MP_TV_SWITCH_STATE_CLEANUP_LOAD_SCENE"
			BREAK
			CASE MP_TV_SWITCH_STATE_SETUP_RENDERTARGET
				RETURN "MP_TV_SWITCH_STATE_SETUP_RENDERTARGET"
			BREAK
			CASE MP_TV_SWITCH_STATE_WAIT_FOR_FILTER_TIME
				RETURN "MP_TV_SWITCH_STATE_WAIT_FOR_FILTER_TIME"
			BREAK
			CASE MP_TV_SWITCH_STATE_FADE_IN
				RETURN "MP_TV_SWITCH_STATE_FADE_IN"
			BREAK
			CASE MP_TV_SWITCH_STATE_CLEANUP_PROCESS
				RETURN "MP_TV_SWITCH_STATE_CLEANUP_PROCESS"
			BREAK
			CASE MP_TV_SWITCH_STATE_WATCH_CHANNEL
				RETURN "MP_TV_SWITCH_STATE_WATCH_CHANNEL"
			BREAK
		ENDSWITCH
		PRINTLN("POD: GET_MP_TV_SWITCH_STATE_NAME: ", eState)
		RETURN "INVALID_STATE "
	ENDFUNC
	
	/// PURPOSE:
	///    Creates widgets for mp tv
	/// PARAMS:
	///    MPTVLocal - local data struct
	PROC CREATE_MP_TV_WIDGETS(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal,MP_TV_SERVER_DATA_STRUCT &MPTVServer)
		
		IF NOT MPTVLocal.bWdWidgetsCreated
		AND NOT MPTVLocal.bInCasinoApartment
		#IF FEATURE_CASINO_HEIST
		AND NOT MPTVLocal.bInArcade
		#ENDIF
		#IF FEATURE_FIXER
		AND NOT MPTVLocal.bInFixerHQ
		#ENDIF
			WIDGET_GROUP_ID interiorWidgetGroup = GET_ID_OF_TOP_LEVEL_WIDGET_GROUP()
		
			SET_CURRENT_WIDGET_GROUP(interiorWidgetGroup)
			START_WIDGET_GROUP("MP TV")
				START_WIDGET_GROUP("Current Render Position")
					ADD_WIDGET_FLOAT_READ_ONLY("X", MPTVLocal.fWdCurrentRenderPosX)
					ADD_WIDGET_FLOAT_READ_ONLY("Y", MPTVLocal.fWdCurrentRenderPosY)
					ADD_WIDGET_FLOAT_READ_ONLY("Z", MPTVLocal.fWdCurrentRenderPosZ)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Desired Render Position")
					ADD_WIDGET_FLOAT_SLIDER("X", MPTVLocal.fWdDesiredRenderPosX, -5000.0, 5000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("Y", MPTVLocal.fWdDesiredRenderPosY, -5000.0, 5000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("Z", MPTVLocal.fWdDesiredRenderPosZ, -5000.0, 5000.0, 0.001)
					ADD_WIDGET_BOOL("Update Position", MPTVLocal.bWdUpdateRenderPos)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Volume")
					ADD_WIDGET_FLOAT_READ_ONLY("Apartment", MPTVServer.fVolume)
					ADD_WIDGET_FLOAT_READ_ONLY("Garage", MPTVServer.fGarageVolume)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP(interiorWidgetGroup)
			MPTVLocal.bWdWidgetsCreated = TRUE
		
		ENDIF
		
	ENDPROC
	
	/// PURPOSE:
	///    Update widgets for mp tv
	/// PARAMS:
	///    MPTVLocal - local data struct
	PROC UPDATE_MP_TV_WIDGETS(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	
		IF DOES_ENTITY_EXIST(MPTVLocal.objOverlay)
		
			VECTOR vTempRenderCoords = GET_ENTITY_COORDS(MPTVLocal.objOverlay)
			MPTVLocal.fWdCurrentRenderPosX = vTempRenderCoords.x
			MPTVLocal.fWdCurrentRenderPosY = vTempRenderCoords.y
			MPTVLocal.fWdCurrentRenderPosZ = vTempRenderCoords.z
			
			IF MPTVLocal.bWdUpdateRenderPos
			
				vTempRenderCoords = <<MPTVLocal.fWdDesiredRenderPosX, MPTVLocal.fWdDesiredRenderPosY, MPTVLocal.fWdDesiredRenderPosZ>>
				
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === Render widget moved to ", vTempRenderCoords)
				#ENDIF
				
				SET_ENTITY_COORDS(MPTVLocal.objOverlay, vTempRenderCoords)
				
				MPTVLocal.bWdUpdateRenderPos = FALSE
				
			ENDIF
			
		ENDIF
		
	ENDPROC
	
#ENDIF

/// PURPOSE:
///    Sets the tv channel switch state for the local player
/// PARAMS:
///    MPTVLocal - local data struct
///    eState - desired tv channel switch state
PROC SET_MP_TV_SWITCH_STATE(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_TV_SWITCH_STATE eState)
	#IF IS_DEBUG_BUILD
		IF MPTVLocal.eSwitchState != eState
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === setting MP TV switch state to ", GET_MP_TV_SWITCH_STATE_NAME(eState))
		ELSE	
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === already on stage ", GET_MP_TV_SWITCH_STATE_NAME(eState))
		ENDIF
	#ENDIF
	MPTVLocal.eSwitchState = eState
ENDPROC

/// PURPOSE:
///    Gets current local tv channel switch state
/// PARAMS:
///    MPTVLocal - local data struct
/// RETURNS:
///    current tv channel switch state as MP_TV_SWITCH_STATE
FUNC MP_TV_SWITCH_STATE GET_MP_TV_SWITCH_STATE(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	RETURN MPTVLocal.eSwitchState
ENDFUNC

/// PURPOSE:
///    Gets central coordinate of the garage interior to use when loading interior
/// PARAMS:
///    property - property type ID
/// RETURNS:
///    central coordinate of garage interior as a VECTOR
FUNC VECTOR GET_GARAGE_INTERIOR_LOAD_VECTOR(MP_PROPERTY_STRUCT &property)
	SWITCH property.iGarageSize
		CASE PROP_GARAGE_SIZE_2
			RETURN <<173.1295, -1002.7625, -98.5175>>
		BREAK
		CASE PROP_GARAGE_SIZE_6
			RETURN <<197.9970, -999.8384, -98.6535>>
		BREAK
		CASE PROP_GARAGE_SIZE_10
			RETURN <<228.1780, -996.2805, -98.7374>>
		BREAK
	ENDSWITCH
	
	
	IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
		RETURN << -1266.802246, -3014.836914, -50.0>>
	ENDIF
	
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		RETURN <<345.0041, 4842.0010, -59.9997>>
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE:
///    Gets interior name ID of the garage interior to use when loading interior
/// PARAMS:
///    property - property type ID
/// RETURNS:
///    interior name ID as a STRING
FUNC STRING GET_GARAGE_INTERIOR_LOAD_INTERIOR_NAME(MP_PROPERTY_STRUCT &property)
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())	
		RETURN "xm_x17dlc_int_02"
	ENDIF

	SWITCH property.iGarageSize
		CASE PROP_GARAGE_SIZE_2
			RETURN "v_garages"
		BREAK
		CASE PROP_GARAGE_SIZE_6
			RETURN "v_garagem"
		BREAK
		CASE PROP_GARAGE_SIZE_10
			RETURN "hei_dlc_garage_high_new"
		BREAK
	ENDSWITCH
	
	IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
		RETURN "sm_smugdlc_int_01"
	ENDIF
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets interior room name ID of the garage interior room to use when loading interior
/// PARAMS:
///    property - property type ID
/// RETURNS:
///    interior room name ID as a STRING
FUNC STRING GET_GARAGE_INTERIOR_LOAD_ROOM_NAME(MP_PROPERTY_STRUCT &property)
	SWITCH property.iGarageSize
		CASE PROP_GARAGE_SIZE_2
			RETURN "GtaMloRoom003"
		BREAK
		CASE PROP_GARAGE_SIZE_6
			RETURN "GtaMloRoom002"
		BREAK
		CASE PROP_GARAGE_SIZE_10
			RETURN "GtaMloRoom001"
		BREAK
	ENDSWITCH
	
	IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
		RETURN "GtaMloRoom001"
	ENDIF
	
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		RETURN "Int02_base_data"
	ENDIF
	
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets the tv position and rotation for a specific property
/// PARAMS:
///    MPTVClient - client broadcast data struct
///    property - property ID to get TV for
///    returnCoord - stores TV coordinate
///    returnRot - stores TV rotation
///    bForceGarage - force finding the TV in the garage
PROC GET_MP_TV_COORD_AND_ROT(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_PROPERTY_STRUCT &property, VECTOR &returnCoord, VECTOR &returnRot, BOOL bForceGarage = FALSE)
	#IF IS_DEBUG_BUILD
		println("GET_MP_TV_COORD_AND_ROT")
	#ENDIF
	MP_PROP_OFFSET_STRUCT tempProp
	returnCoord = <<0.0, 0.0, 0.0>>
	returnRot = <<0.0, 0.0, 0.0>>
	
//	IF NOT IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
		IF NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
		AND NOT bForceGarage
				
			IF IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
				IF g_iCurrentPropertyVariation = 1
				OR g_iCurrentPropertyVariation = 2
				OR g_iCurrentPropertyVariation = 5
				OR g_iCurrentPropertyVariation = 6
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_TV, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
					CDEBUG1LN(DEBUG_MP_TV, "Getting coords for property tv Interior Var:",g_iCurrentPropertyVariation, ", vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
					returnCoord = tempProp.vLoc
					returnRot = tempProp.vRot
				ELIF g_iCurrentPropertyVariation = 3
				OR g_iCurrentPropertyVariation = 7
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_TV_2, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
					CDEBUG1LN(DEBUG_MP_TV, "Getting coords for property tv Interior Var:",g_iCurrentPropertyVariation, ", vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
					returnCoord = tempProp.vLoc
					returnRot = tempProp.vRot
				ELIF g_iCurrentPropertyVariation = 4
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_TV_4, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
					CDEBUG1LN(DEBUG_MP_TV, "Getting coords for property tv Interior Var:",g_iCurrentPropertyVariation, ", vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
					returnCoord = tempProp.vLoc
					returnRot = tempProp.vRot
				ELSE
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_TV_3, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
					CDEBUG1LN(DEBUG_MP_TV, "Getting coords for property tv Interior Var:",g_iCurrentPropertyVariation, ", vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
					returnCoord = tempProp.vLoc
					returnRot = tempProp.vRot
				ENDIF
			ELIF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
				GET_POSITION_AS_OFFSET_FOR_YACHT(GET_YACHT_LOCAL_PLAYER_IS_ON(), MP_PROP_ELEMENT_TV, tempProp)
				CDEBUG1LN(DEBUG_MP_TV, "Getting coords for yacht tv: vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
				returnCoord = tempProp.vLoc
				returnRot = tempProp.vRot
			ELIF IS_PROPERTY_OFFICE(property.iIndex)
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_TV, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
				CDEBUG1LN(DEBUG_MP_TV, "Getting coords for office tv: vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
				returnCoord = tempProp.vLoc
				returnRot = tempProp.vRot
			ELIF IS_PROPERTY_CLUBHOUSE(property.iIndex)
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_TV, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
				CDEBUG1LN(DEBUG_MP_TV, "Getting coords for clubhouse tv: vLoc = ", returnCoord, ", vRot = ", returnRot)
				returnCoord = tempProp.vLoc
				returnRot = tempProp.vRot
			ELIF IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_TV, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex), TRUE)
				CDEBUG1LN(DEBUG_MP_TV, "Getting coords for office garage tv: vLoc = ", returnCoord, ", vRot = ", returnRot)
				returnCoord = tempProp.vLoc
				returnRot = tempProp.vRot
			ELIF IS_PLAYER_IN_HANGAR(PLAYER_ID())
				tempProp.vLoc = <<-1235.235,-3010.295,-42.873>>
				tempProp.vRot = <<0,0,-45>>
				returnCoord  = <<-1235.235,-3010.295,-42.873>>
				returnRot    = <<0,0,-45>>
				CDEBUG1LN(DEBUG_MP_TV, "Getting coords for hangar property tv: vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
			ELIF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
				tempProp.vLoc	= <<364.0089, 4838.958, -58.8157>> 
				tempProp.vRot 	= <<0, 0, 163.0>>
				returnCoord  	= <<364.0089, 4838.958, -58.8157>>
				returnRot    	= <<0, 0, 163.0>>
				CDEBUG1LN(DEBUG_MP_TV, "Getting coords for Facility property tv: vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
			ELIF MPTVLocal.bInCasinoApartment
				IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
					tempProp.vLoc	= <<971.2571, 75.6211, 116.6575>>
					tempProp.vRot 	= <<0.0, 0.0, 58.0>>
					returnCoord  	= <<971.2571, 75.6211, 116.6575>>
					returnRot    	= <<0.0, 0.0, 58.0>>
					CDEBUG1LN(DEBUG_MP_TV, "Getting coords for Facility property tv: vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
				ELSE
					tempProp.vLoc	= <<958.6348, 31.7808, 116.9165>>
					tempProp.vRot 	= <<0.0, 0.0, -122.0>>
					returnCoord  	= <<958.6348, 31.7808, 116.9165>>
					returnRot    	= <<0.0, 0.0, -122.0>>
					CDEBUG1LN(DEBUG_MP_TV, "Getting coords for Facility property tv: vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
				ENDIF
			#IF FEATURE_CASINO_HEIST
			ELIF MPTVLocal.bInArcade
				tempProp.vLoc	= <<2740.3860, -381.5987, -47.8532>>
				tempProp.vRot 	= <<0.0, 0.0, 0.0>>
				returnCoord  	= <<2740.3860, -381.5987, -47.8532>>
				returnRot    	= <<0.0, 0.0, 0.0>>
				CDEBUG1LN(DEBUG_MP_TV, "Getting coords for Arcade property tv: vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
			#ENDIF
			#IF FEATURE_FIXER
			ELIF MPTVLocal.bInFixerHQ																																
				
				IF IS_BIT_SET(MPTVLocal.iBitset2, MPTV_LOCAL_BS2_THIRD_TV) // Personal Quarters
					tempProp.vLoc	= << 10.8081, -7.22728, 10.14 >>
					tempProp.vRot 	= <<0.0, 0.0, 270.0>>
					returnCoord  	= << 10.8081, -7.22728, 10.14 >>
					returnRot    	= <<0.0, 0.0, 270.0>>
					CDEBUG1LN(DEBUG_MP_TV, "Getting coords for FixerHQ property tv: vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
				ELIF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV) // Operations Floor Back Room
					tempProp.vLoc	= << -8.47818, 12.379, 9.8998 >> 
					tempProp.vRot 	= <<0.0, 0.0, 0.0>>
					returnCoord  	= << -8.47818, 12.379, 9.8998 >>
					returnRot    	= <<0.0, 0.0, 0.0>>
					CDEBUG1LN(DEBUG_MP_TV, "Getting coords for FixerHQ property tv: vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
				ELSE // Boardroom
					tempProp.vLoc	= << -6.75788, 5.98396, 5.2013 >>
					tempProp.vRot 	= <<0.0, 0.0, 0.0>>
					returnCoord  	= << -6.75788, 5.98396, 5.2013 >>
					returnRot    	= <<0.0, 0.0, 0.0>>
					CDEBUG1LN(DEBUG_MP_TV, "Getting coords for FixerHQ property tv: vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
				ENDIF
								
				returnCoord = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(returnCoord, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))
				returnRot.z = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(returnRot.z, GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))	
			#ENDIF
			ELSE
				IF GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex) = PROPERTY_MEDIUM_APT_1
					tempProp.vLoc = <<337.2845,-996.6658,-99.0276>>
					tempProp.vRot = <<0,0,90>>
					returnCoord = <<337.2845,-996.6658,-99.0276>>
					returnRot = <<0,0,90>>
					CDEBUG1LN(DEBUG_MP_TV, "Getting coords for property tv: vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
				ELIF GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex) = PROPERTY_LOW_APT_1
					tempProp.vLoc = <<256.7323,-995.4481,-98.8606>>
					tempProp.vRot = <<0,0,45>>
					returnCoord = <<256.7323,-995.4481,-98.8606>>
					returnRot = <<0,0,45>>
					CDEBUG1LN(DEBUG_MP_TV, "Getting coords for property tv: vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
				ELSE
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_TV, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
					CDEBUG1LN(DEBUG_MP_TV, "Getting coords for property tv: vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
					returnCoord = tempProp.vLoc
					returnRot = tempProp.vRot
				ENDIF	
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_COORD_AND_ROT - bForceGarage")
			returnCoord = <<228.8359, -974.6591, -98.3713>>
		ENDIF
//	ELSE
//		IF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
//			GET_POSITION_AS_OFFSET_FOR_YACHT(GET_YACHT_LOCAL_PLAYER_IS_ON(), MP_PROP_ELEMENT_TV, tempProp)
//			CDEBUG1LN(DEBUG_MP_TV, "Getting coords for yacht tv: vLoc = ", tempProp.vLoc, ", vRot = ", tempProp.vRot)
//			returnCoord = tempProp.vLoc
//			returnRot = tempProp.vRot
//		ENDIF
//	ENDIF
	
ENDPROC

/// PURPOSE:
///    Gets the tv position for a specific property
/// PARAMS:
///    MPTVClient - client broadcast data struct
///    property - property ID to get TV for
///    bForceGarage - force finding the TV in the garage
/// RETURNS:
///    position of the TV as a VECTOR
FUNC VECTOR GET_MP_TV_POSITION(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_PROPERTY_STRUCT &property, BOOL bForceGarage = FALSE)
	
	VECTOR vTempCoord, vTempRot
	
	GET_MP_TV_COORD_AND_ROT(MPTVLocal, MPTVClient, property, vTempCoord, vTempRot, bForceGarage)
	
	RETURN vTempCoord
	
ENDFUNC

/// PURPOSE:
///    Gets the camera information for the camera angle used when watching the TV
/// PARAMS:
///    MPTVClient - client broadcast data
///    property - property ID that player is watching TV in
///    vPos - return vector of camera position
///    vRot - return vector of camera rotation
///    fFOV - return float of camera FOV
///    iCameraID - ID of which camera to use (most TVs have at least two shots)
/// RETURNS:
///    true if succesfully found camera
FUNC BOOL GET_MP_TV_ROOM_CAMERA_INFO(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_PROPERTY_STRUCT &property, VECTOR &vPos, VECTOR &vRot, FLOAT &fFOV, INT iCameraID)
	MP_PROP_OFFSET_STRUCT tempProp
	
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		SWITCH iCameraID
			CASE 0
				vPos = <<365.4817, 4844.3184, -58.6415>>
				vRot = <<-4.2269, -0.0000, 162.4367>>
				fFOV = 50
			BREAK
			CASE 1
				vPos = <<364.4599, 4840.5576, -58.2000>>
				vRot =  <<-1.0655, -0.0000, 163.2952>>
				fFOV = 50
			BREAK
		ENDSWITCH
		RETURN TRUE
	ENDIF
	
	IF MPTVLocal.bInCasinoApartment
		IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
			IF g_sShopSettings.playerRoom = -284825127
				SWITCH iCameraID
					CASE 0
						vPos = <<964.7457, 72.0163, 117.6699>>
						vRot = <<-4.7377, 0.0000, -59.4768>>
						fFOV = 38.6237
					BREAK
					
					CASE 1
						vPos = <<969.9574, 73.4718, 117.2680>>
						vRot = <<-4.1057, 0.0000, -31.2887>>
						fFOV = 38.6237
					BREAK
				ENDSWITCH
			ELSE
				SWITCH iCameraID
					CASE 0
						vPos = <<969.4342, 79.1508, 117.4149>>
						vRot = <<-4.2559, -0.0000, -65.3390>>
						fFOV = 41.4149
					BREAK
					
					CASE 1
						vPos = <<973.5876, 79.3592, 117.6854>>
						vRot = <<-17.1453, -0.0000, -32.9296>>
						fFOV = 28.6484
					BREAK
				ENDSWITCH
			ENDIF
		ELSE
			SWITCH iCameraID
				CASE 0
					vPos = <<953.5262, 37.3533, 117.6646>>
					vRot = <<-4.6269, -0.0000, -136.2769>>
					fFOV = 41.0797
				BREAK
				
				CASE 1
					vPos = <<955.1350, 34.0978, 117.6524>>
					vRot = <<-9.4598, -0.0000, -122.3616>>
					fFOV = 31.8450
				BREAK
			ENDSWITCH
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF MPTVLocal.bInArcade
		SWITCH iCameraID
			CASE 0
				vPos = <<2736.1882, -381.6178, -47.0644>>
				vRot = <<-16.2510, -0.0032, -89.9605>>
				fFOV = 50.0
			BREAK
			
			CASE 1
				vPos = <<2737.3960, -381.6180, -47.6195>>
				vRot = <<-3.6461, -0.0032, -89.7157>>
				fFOV = 37.9342
			BREAK
		ENDSWITCH
		
		RETURN TRUE
	ENDIF
	#ENDIF
	
	#IF FEATURE_FIXER
	IF MPTVLocal.bInFixerHQ	
		IF IS_BIT_SET(MPTVLocal.iBitset2, MPTV_LOCAL_BS2_THIRD_TV) // Personal Quarters
			SWITCH iCameraID
				CASE 0
					vPos = << 4.33672, -4.21228, 10.8111 >> // Original value at Hawick location <<379.3725, -63.3615, 113.1741>>
					vRot = <<-9.6823, 0.0000, 242.589905>> // Original Z value at Hawick location 132.5899
					fFOV = 38.6237
				BREAK
				
				CASE 1
					vPos = << 6.12079, -8.92598, 10.897 >> // Original value at Hawick location <<374.3329, -63.4258, 113.2600>>
					vRot = <<-8.9088, 0.0000, 301.724396>> // Original Z value at Hawick location -168.2756
					fFOV = 38.6237
				BREAK
			ENDSWITCH
		ELIF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV) // Operations Floor Back Room
			SWITCH iCameraID
				CASE 0
					vPos = << -10.6644, 7.81091, 10.7271 >> // Original value at Hawick location <<395.8013, -53.3772, 113.0901>>
					vRot = <<-7.1477, 0.0000, 327.856903>> // Original Z value at Hawick -142.1431
					fFOV = 38.6237
				BREAK
				
				CASE 1
					vPos = << -6.02023, 6.91382, 10.4768 >> // Original value at Hawick location <<393.3699, -57.4345, 112.8398>>
					vRot = <<-4.4274, -0.0000, 35.780403>> // Original Z value at Hawick location -74.2196
					fFOV = 38.6237
				BREAK
			ENDSWITCH
		ELSE
			SWITCH iCameraID // Boardroom
				CASE 0
					vPos = << -6.73211, -3.44156, 6.3379 >> // Original value at Hawick location <<383.8825, -53.2238, 108.7009>>
					vRot = <<-7.9605, -0.0000, 1.464500>> // Original Z value at Hawick location -108.5355
					fFOV = 41.0797
				BREAK
				
				CASE 1
					vPos = << -9.16653, 2.15108, 6.0347 >> // Original value at Hawick location <<389.9705, -52.8490, 108.3977>>
					vRot = <<-7.7646, 0.0000, 324.226990>> // Original Z value at Hawick location -145.7730
					fFOV = 31.8450
				BREAK
			ENDSWITCH
		ENDIF
		
		SIMPLE_INTERIORS eSimpleInterior = GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())
					
		vPos = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(vPos, eSimpleInterior)							
		vRot.z = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(vRot.z, eSimpleInterior)				
	
		RETURN TRUE
	ENDIF
	#ENDIF
	
	IF NOT IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
		SWITCH property.iGarageSize
			CASE PROP_GARAGE_SIZE_2
				SWITCH iCameraID
					CASE 0
						vPos = <<257.7857, -996.3652, -98.9071>>
						vRot = <<1.9738, 0.0000, 48.4787>>
						fFOV = 45.0000
						RETURN TRUE
					BREAK
					CASE 1
						vPos = <<264.6160, -1001.0403, -98.3349>>
						vRot = <<-4.2679, 0.0000, 54.5139>>
						fFOV = 27.6556
						RETURN TRUE
					BREAK
				ENDSWITCH
			BREAK
			CASE PROP_GARAGE_SIZE_6
				SWITCH iCameraID
					CASE 0
						vPos = <<340.3142, -996.5649, -98.5637>>
						vRot = <<-0.9291, -0.0000, 91.2929>>
						fFOV = 50.0000
						RETURN TRUE
					BREAK
					CASE 1
						vPos = <<345.8763, -994.3792, -98.3018>>
						vRot = <<-9.0438, -0.0000, 110.6934>>
						fFOV = 36.1457
						RETURN TRUE
					BREAK
				ENDSWITCH
			BREAK
			CASE PROP_GARAGE_SIZE_10
				IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
					SWITCH iCameraID
						CASE 0
							vPos = <<228.5240, -977.4732, -97.7586>>
							vRot = <<-4.0395, -0.0000, -4.4596>>
							fFOV = 45.0000
							RETURN TRUE
						BREAK
						CASE 1
							vPos = <<226.9101, -987.9648, -96.2153>>
							vRot = <<-9.8837, 0.0000, -6.0976>>
							fFOV = 22.6931
							RETURN TRUE
						BREAK
					ENDSWITCH
				ELSE
					IF NOT IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
						SWITCH iCameraID
							CASE 0
								IF IS_PROPERTY_BUSINESS_APARTMENT(property.iIndex)
									GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_ROOM_CAMERA_0, tempProp, PROPERTY_BUS_HIGH_APT_1)
								ELSE
									GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_ROOM_CAMERA_0, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
								ENDIF
								vPos = tempProp.vLoc
								vRot = tempProp.vRot
								fFOV = 45.0000
								IF IS_PROPERTY_STILT_APARTMENT(property.iIndex)
								OR IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
									fFOV = 53.5191
								ENDIF
								RETURN TRUE
							BREAK
							CASE 1
								IF IS_PROPERTY_BUSINESS_APARTMENT(property.iIndex)
									GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_ROOM_CAMERA_1, tempProp, PROPERTY_BUS_HIGH_APT_1)
								ELSE
									GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_ROOM_CAMERA_1, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
								ENDIF
								vPos = tempProp.vLoc
								vRot = tempProp.vRot
								fFOV = 33.4744
								IF IS_PROPERTY_STILT_APARTMENT(property.iIndex)
								OR IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
									fFOV = 53.5191
								ENDIF
								RETURN TRUE
							BREAK
						ENDSWITCH
					ELSE
						SWITCH iCameraID
							CASE 0
								GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_ROOM_CAMERA_0, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
								vPos = tempProp.vLoc
								vRot = tempProp.vRot
								fFOV =53.5191
								RETURN TRUE
							BREAK
							CASE 1
								IF g_iCurrentPropertyVariation = 3
								OR g_iCurrentPropertyVariation = 7
									GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_ROOM_CAMERA_2, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
								ELIF g_iCurrentPropertyVariation = 4
								OR g_iCurrentPropertyVariation = 8
									GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_ROOM_CAMERA_3, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
								ELSE
									GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_ROOM_CAMERA_1, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
								ENDIF
								vPos = tempProp.vLoc
								vRot = tempProp.vRot
								fFOV = 53.5191
								RETURN TRUE
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		IF IS_PROPERTY_OFFICE(property.iIndex)
		OR IS_PROPERTY_CLUBHOUSE(property.iIndex)
			SWITCH iCameraID
				CASE 0
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_ROOM_CAMERA_0, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
					vPos = tempProp.vLoc
					vRot = tempProp.vRot
					fFOV =53.5191
					RETURN TRUE
				BREAK
				CASE 1
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_ROOM_CAMERA_1, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
					vPos = tempProp.vLoc
					vRot = tempProp.vRot
					fFOV = 53.5191
					RETURN TRUE
				BREAK
			ENDSWITCH
		ENDIF
		
		IF IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
			SWITCH iCameraID
				CASE 0
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_ROOM_CAMERA_0, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex), TRUE)
					vPos = tempProp.vLoc
					vRot = tempProp.vRot
					fFOV =53.5191
					RETURN TRUE
				BREAK
				CASE 1
					GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_ROOM_CAMERA_1, tempProp, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex), TRUE)
					vPos = tempProp.vLoc
					vRot = tempProp.vRot
					fFOV = 53.5191
					RETURN TRUE
				BREAK
			ENDSWITCH
		ENDIF
		
		IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
			SWITCH iCameraID
				CASE 0
					vPos = <<-1240.7783, -3015.6904, -42.2563>>
					vRot = <<-6.5518, -0.0000, -48.5072>>
					fFOV =53.5191
					RETURN TRUE
				BREAK
				CASE 1
					vPos = <<-1236.9662, -3011.9661, -42.2660>>
					vRot = <<0.1736, -0.0265, -46.2496>>
					fFOV = 53.5191
					RETURN TRUE
				BREAK
			ENDSWITCH
		ENDIF
		
	ELSE	
		SWITCH iCameraID
			CASE 0
				GET_POSITION_AS_OFFSET_FOR_YACHT(GET_YACHT_LOCAL_PLAYER_IS_ON(), MP_PROP_ELEMENT_MPTV_ROOM_CAMERA_0, tempProp)
				vPos = tempProp.vLoc
				vRot = tempProp.vRot
				fFOV = 33.4744
				RETURN TRUE
			BREAK
			CASE 1
				GET_POSITION_AS_OFFSET_FOR_YACHT(GET_YACHT_LOCAL_PLAYER_IS_ON(), MP_PROP_ELEMENT_MPTV_ROOM_CAMERA_1, tempProp)
				vPos = tempProp.vLoc
				vRot = tempProp.vRot
				fFOV = 33.4744
				RETURN TRUE
			BREAK
		ENDSWITCH
		
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the object model of the TV in a specified property
/// PARAMS:
///    property - property ID of property to get TV model for
/// RETURNS:
///    TV model as a MODEL_NAMES
FUNC MODEL_NAMES GET_MP_TV_MODEL(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property, MP_TV_CLIENT_DATA_STRUCT &MPTVClient)
	
	IF IS_PROPERTY_BUSINESS_APARTMENT(property.iIndex)
		RETURN DUMMY_MODEL_FOR_SCRIPT
	ELIF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
		CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: returning prop_tv_flat_michael")
		RETURN prop_tv_flat_michael
	ELIF IS_PROPERTY_STILT_APARTMENT(property.iIndex, PROPERTY_STILT_APT_1_BASE_B)
		IF NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
			CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: returning APA_MP_STR_AVUNITS_04")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("apa_mp_h_str_AvUnitS_04"))
		ELSE
			CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: returning PROP_TV_FLAT_01 because we're in garage")
			RETURN PROP_TV_FLAT_01
		ENDIF
	ELIF IS_PROPERTY_STILT_APARTMENT(property.iIndex, PROPERTY_STILT_APT_5_BASE_A)
		IF NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
			CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: returning APA_MP_STR_AVUNITS_01")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("apa_mp_h_str_AvUnitS_01"))
		ELSE
			CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: returning PROP_TV_FLAT_01 because we're in garage")
			RETURN PROP_TV_FLAT_01
		ENDIF
	ELIF IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
	AND NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
		IF g_iCurrentPropertyVariation = 1
		OR g_iCurrentPropertyVariation = 2
		OR g_iCurrentPropertyVariation = 6
		
			CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: returning apa_mp_h_str_avunitm_01")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("apa_mp_h_str_avunitm_01"))
		ELIF g_iCurrentPropertyVariation = 5
			CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: returning apa_mp_h_str_avunitm_03")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("apa_mp_h_str_avunitm_03"))
		ELIF g_iCurrentPropertyVariation = 3
		OR g_iCurrentPropertyVariation = 7
			CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: returning prop_tv_flat_01")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_tv_flat_01"))	
		ELIF g_iCurrentPropertyVariation = 4
			CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: returning apa_mp_h_str_avunitl_01_b")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("apa_mp_h_str_avunitl_01_b"))	
		ELSE
			CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: returning apa_mp_h_str_avunitl_04")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("apa_mp_h_str_avunitl_04"))	
		ENDIF
	ELIF IS_PROPERTY_OFFICE(property.iIndex)
	AND NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)

		CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: Office returning ex_prop_ex_tv_flat_01")
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ex_prop_ex_tv_flat_01"))

	ELIF IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
	AND NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
		CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: Office mod garage returning ex_prop_ex_tv_flat_01")
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_tv_flat_01"))
	ELIF IS_PROPERTY_CLUBHOUSE(property.iIndex)
	AND NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
		IF GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex) = PROPERTY_CLUBHOUSE_1_BASE_A
			CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: Office returning prop_tv_flat_michael")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_tv_flat_michael"))
		ELSE
			CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: Office returning prop_tv_flat_02")
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_tv_flat_02"))
		ENDIF
	ELIF IS_PLAYER_IN_HANGAR(PLAYER_ID())
	AND NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
		CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: Hangar returning sm_prop_smug_tv_flat_01")
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_tv_flat_01"))
	ELIF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	AND NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
		CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: Facility returning hash of xm_prop_x17_tv_flat_01 : ", HASH("xm_prop_x17_tv_flat_01"))
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_tv_flat_01")) 
	ELIF MPTVLocal.bInCasinoApartment
	AND NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
		IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
			CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: Facility returning hash of vw_Prop_VW_TV_RT_01a : ", HASH("vw_Prop_VW_TV_RT_01a"))
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_VW_TV_RT_01a"))
		ELSE
			CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: Facility returning hash of vw_prop_vw_cinema_tv_01 : ", HASH("vw_prop_vw_cinema_tv_01"))
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_cinema_tv_01"))
		ENDIF
	#IF FEATURE_CASINO_HEIST
	ELIF MPTVLocal.bInArcade
	AND NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
		CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: Facility returning hash of ch_prop_ch_arcade_big_screen : ", HASH("ch_prop_ch_arcade_big_screen"))
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_TV_RT_01a"))
	#ENDIF
	#IF FEATURE_FIXER
	ELIF MPTVLocal.bInFixerHQ
	AND NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
		IF IS_BIT_SET(MPTVLocal.iBitset2, MPTV_LOCAL_BS2_THIRD_TV)
			CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: Facility returning hash of sf_prop_sf_tv_flat_scr_01a : ", HASH("sf_prop_sf_tv_flat_scr_01a"))
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_tv_flat_scr_01a"))
		ELSE
			CDEBUG1LN(DEBUG_MP_TV, "GET_MP_TV_MODEL: Facility returning hash of prop_tv_flat_01 : ", HASH("prop_tv_flat_01"))
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_tv_flat_01"))
		ENDIF
	#ENDIF
	ELSE
		SWITCH property.iGarageSize
			CASE PROP_GARAGE_SIZE_2
				RETURN PROP_TV_03
			BREAK
			CASE PROP_GARAGE_SIZE_6
				RETURN PROP_TV_FLAT_01
			BREAK
			CASE PROP_GARAGE_SIZE_10
				RETURN PROP_TV_FLAT_01
			BREAK
		ENDSWITCH
	ENDIF
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE:
///    Gets the overlay screen model for the specified property
/// PARAMS:
///    property - property ID to get the screen model of
/// RETURNS:
///    screen model as MODEL_NAMES
FUNC MODEL_NAMES GET_MP_TV_OVERLAY_MODEL(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property)
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("xm_prop_x17_tv_flat_01"))
	ELIF IS_PROPERTY_BUSINESS_APARTMENT(property.iIndex)
		RETURN PROP_TV_FLAT_01_SCREEN
	ELIF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
		RETURN PROP_TV_FLAT_01_SCREEN
	ELIF IS_PROPERTY_OFFICE(property.iIndex)
		RETURN PROP_TV_FLAT_01_SCREEN
	ELIF IS_PROPERTY_CLUBHOUSE(property.iIndex)
		IF GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex) = PROPERTY_CLUBHOUSE_1_BASE_A
			RETURN PROP_TV_FLAT_MICHAEL
		ELSE
			RETURN PROP_TV_FLAT_02
		ENDIF
	ELIF IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
		RETURN PROP_TV_FLAT_01
	ELIF IS_PLAYER_IN_HANGAR(PLAYER_ID())
		RETURN PROP_TV_FLAT_01_SCREEN //INT_TO_ENUM(MODEL_NAMES, HASH("sm_prop_smug_tv_flat_01"))
	ELIF MPTVLocal.bInCasinoApartment
		IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_Prop_VW_TV_RT_01a"))
		ELSE
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("vw_prop_vw_cinema_tv_01"))
		ENDIF
	#IF FEATURE_CASINO_HEIST
	ELIF MPTVLocal.bInArcade
		RETURN INT_TO_ENUM(MODEL_NAMES, HASH("ch_Prop_Ch_TV_RT_01a"))
	#ENDIF
	#IF FEATURE_FIXER
	ELIF MPTVLocal.bInFixerHQ
		IF IS_BIT_SET(MPTVLocal.iBitset2, MPTV_LOCAL_BS2_THIRD_TV)
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_tv_flat_scr_01a"))
		ELSE
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_tv_flat_01"))
		ENDIF
	#ENDIF
	ELSE
		SWITCH property.iGarageSize
			CASE PROP_GARAGE_SIZE_2
				RETURN PROP_TV_03_OVERLAY
			BREAK
			CASE PROP_GARAGE_SIZE_6
				RETURN PROP_TV_FLAT_01_SCREEN
			BREAK
			CASE PROP_GARAGE_SIZE_10
				RETURN PROP_TV_FLAT_01_SCREEN
			BREAK
		ENDSWITCH
	ENDIF
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE:
///    Gets the number of MP TV seats in a specified property
/// PARAMS:
///    property - property ID of property to get number of seats for
/// RETURNS:
///    number of seats as an INT
FUNC INT GET_MP_TV_NUMBER_OF_SEATS(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property)
	IF IS_PROPERTY_BUSINESS_APARTMENT(property.iIndex)
		RETURN MP_TV_NUM_SEATS_BUSINESS
	ELIF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
		RETURN 7
	ELIF IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
		RETURN 3
	ELIF IS_PROPERTY_STILT_APARTMENT(property.iIndex, PROPERTY_STILT_APT_1_BASE_B)
		RETURN 4
	ELIF IS_PROPERTY_STILT_APARTMENT(property.iIndex, PROPERTY_STILT_APT_5_BASE_A)
		RETURN 3
	ELIF IS_PROPERTY_OFFICE(property.iIndex)
		RETURN 3
	ELIF IS_PROPERTY_CLUBHOUSE(property.iIndex)
		IF GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex) = PROPERTY_CLUBHOUSE_1_BASE_A
			RETURN 3
		ELSE
			RETURN 4
		ENDIF	
	ELIF IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
		RETURN 4
	ELIF IS_PLAYER_IN_HANGAR(PLAYER_ID())
		RETURN 5
	ELIF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		RETURN 3
	ELIF MPTVLocal.bInCasinoApartment
		RETURN 8
	#IF FEATURE_CASINO_HEIST
	ELIF MPTVLocal.bInArcade
		RETURN 3
	#ENDIF
	#IF FEATURE_FIXER
	ELIF MPTVLocal.bInFixerHQ
		RETURN 8
	#ENDIF
	ELSE
		SWITCH property.iGarageSize
			CASE PROP_GARAGE_SIZE_2
				RETURN MP_TV_NUM_SEATS_LOW_END
			BREAK
			CASE PROP_GARAGE_SIZE_6
				RETURN MP_TV_NUM_SEATS_MID_RANGE
			BREAK
			CASE PROP_GARAGE_SIZE_10
				RETURN MP_TV_NUM_SEATS_HIGH_END
			BREAK
		ENDSWITCH
	ENDIF
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Gets the position of a specified seat in a specified property
/// PARAMS:
///    property - property ID to get the seat for
///    iSeatID - seat ID to get the position for
/// RETURNS:
///    seat position as a VECTOR
FUNC VECTOR GET_MP_TV_SEAT_SCENARIO_VECTOR(MP_PROPERTY_STRUCT &property, INT iSeatID, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	//MP_PROP_OFFSET_STRUCT tempProp
	IF NOT IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	AND NOT IS_PROPERTY_OFFICE(property.iIndex)
	AND NOT IS_PROPERTY_CLUBHOUSE(property.iIndex)
	AND NOT IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
	AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
	AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	AND NOT MPTVLocal.bInCasinoApartment
	#IF FEATURE_CASINO_HEIST
	AND NOT MPTVLocal.bInArcade
	#ENDIF
	#IF FEATURE_FIXER
	AND NOT MPTVLocal.bInFixerHQ
	#ENDIF
		SWITCH property.iGarageSize
			CASE PROP_GARAGE_SIZE_2
				SWITCH iSeatID
					CASE 0
						RETURN <<261.00, -995.88, -99.57>>
					BREAK
					CASE 1
						RETURN <<261.23, -997.19, -99.57>>
					BREAK
					CASE 2
						RETURN <<257.82, -998.77, -99.56>>
					BREAK
					CASE 3
						RETURN <<256.56, -998.43, -99.56>>
					BREAK
				ENDSWITCH
			BREAK
			CASE PROP_GARAGE_SIZE_6
				SWITCH iSeatID
					CASE 0
						RETURN <<340.69, -994.33, -99.63>>
					BREAK
					CASE 1
						RETURN <<341.82, -994.33, -99.63>>
					BREAK
					CASE 2
						RETURN <<342.93, -995.77, -99.63>>
					BREAK
					CASE 3
						RETURN <<342.93, -997.18, -99.63>>
					BREAK
				ENDSWITCH
			BREAK
			CASE PROP_GARAGE_SIZE_10
				IF iSeatID > -1 AND iSeatID < GET_MP_TV_NUMBER_OF_SEATS(MPTVLocal, property)
					RETURN MPTVLocal.seatPositions[iSeatID].vLoc
				ENDIF
			BREAK
		ENDSWITCH 
	ELSE
		IF iSeatID > -1 AND iSeatID < GET_MP_TV_NUMBER_OF_SEATS(MPTVLocal, property)
			RETURN MPTVLocal.seatPositions[iSeatID].vLoc
		ENDIF
	ENDIF
	CWARNINGLN(DEBUG_MP_TV, "GET_MP_TV_SEAT_SCENARIO_VECTOR RETURNING: <<0,0,0>>")
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE:
///    Gets the minimum coordinate of the angled locate for a specified seat of a specified property
/// PARAMS:
///    property - property ID of property to find seat in
///    iSeatID - seat ID of seat to get minimum coordinate of
/// RETURNS:
///    minimum coordinate of seat as a VECTOR
FUNC VECTOR GET_MP_TV_SEAT_LOCATE0(MP_PROPERTY_STRUCT &property, INT iSeatID, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	//MP_PROP_OFFSET_STRUCT tempProp
	IF NOT IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	AND NOT IS_PROPERTY_OFFICE(property.iIndex)
	AND NOT IS_PROPERTY_CLUBHOUSE(property.iIndex)
	AND NOT IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
	AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
	AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	AND NOT MPTVLocal.bInCasinoApartment
	#IF FEATURE_CASINO_HEIST
	AND NOT MPTVLocal.bInArcade
	#ENDIF
	#IF FEATURE_FIXER
	AND NOT MPTVLocal.bInFixerHQ
	#ENDIF
		SWITCH property.iGarageSize
			CASE PROP_GARAGE_SIZE_2
				SWITCH iSeatID
					CASE 0
						RETURN <<260.372034,-996.224487,-100.008621>>//<<260.3,-996.067322,-100.008621>>
					BREAK
					CASE 1
						RETURN <<259.833130,-997.476624,-100.008621>>
					BREAK
					CASE 2
						RETURN <<258.218811,-997.553162,-100.008621>>
					BREAK
					CASE 3
						RETURN <<256.878693,-997.150024,-100.008621>>
					BREAK
				ENDSWITCH
			BREAK
			CASE PROP_GARAGE_SIZE_6
				SWITCH iSeatID
					CASE 0
						RETURN <<340.673340,-995.377380,-100.204437>>
					BREAK
					CASE 1
						RETURN <<341.817535,-995.392944,-100.203133>>
					BREAK
					CASE 2
						RETURN <<341.863678,-995.791382,-100.203232>>
					BREAK
					CASE 3
						RETURN <<341.860321,-997.184143,-100.203644>>
					BREAK
				ENDSWITCH
			BREAK
			CASE PROP_GARAGE_SIZE_10
				IF iSeatID > -1 AND iSeatID < GET_MP_TV_NUMBER_OF_SEATS(MPTVLocal, property)
					RETURN MPTVLocal.seatLocates0[iSeatID].vLoc
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		IF iSeatID > -1 AND iSeatID < GET_MP_TV_NUMBER_OF_SEATS(MPTVLocal, property)
			RETURN MPTVLocal.seatLocates0[iSeatID].vLoc
		ENDIF
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE:
///    Gets the maximum coordinate of the angled locate for a specified seat of a specified property
/// PARAMS:
///    property - property ID of property to find seat in
///    iSeatID - seat ID of seat to get maximum coordinate of
/// RETURNS:
///    maximum coordinate of seat as a VECTOR
FUNC VECTOR GET_MP_TV_SEAT_LOCATE1(MP_PROPERTY_STRUCT &property, INT iSeatID, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	//MP_PROP_OFFSET_STRUCT tempProp
	IF NOT IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	AND NOT IS_PROPERTY_OFFICE(property.iIndex)
	AND NOT IS_PROPERTY_CLUBHOUSE(property.iIndex)
	AND NOT IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
	AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
	AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	AND NOT MPTVLocal.bInCasinoApartment
	#IF FEATURE_CASINO_HEIST
	AND NOT MPTVLocal.bInArcade
	#ENDIF
	#IF FEATURE_FIXER
	AND NOT MPTVLocal.bInFixerHQ
	#ENDIF
		SWITCH property.iGarageSize
			CASE PROP_GARAGE_SIZE_2
				SWITCH iSeatID
					CASE 0
						RETURN <<261.340454,-996.077026,-98.061058>>//<<261.301575,-995.844666,-98.113373>> 
					BREAK
					CASE 1
						RETURN <<261.516174,-997.127441,-98.114815>> 
					BREAK
					CASE 2
						RETURN <<257.756165,-999.100952,-98.312935>> 
					BREAK
					CASE 3
						RETURN <<256.452423,-998.751709,-98.309273>> 
					BREAK
				ENDSWITCH
			BREAK
			CASE PROP_GARAGE_SIZE_6
				SWITCH iSeatID
					CASE 0
						RETURN <<340.687286,-993.760437,-98.426643>> 
					BREAK
					CASE 1
						RETURN <<341.827118,-993.751099,-98.359337>> 
					BREAK
					CASE 2
						RETURN <<343.427307,-995.787964,-98.630676>> 
					BREAK
					CASE 3
						RETURN <<343.389862,-997.202148,-98.162422>> 
					BREAK
				ENDSWITCH
			BREAK
			CASE PROP_GARAGE_SIZE_10
				IF iSeatID > -1 AND iSeatID < GET_MP_TV_NUMBER_OF_SEATS(MPTVLocal, property)
					RETURN MPTVLocal.seatLocates1[iSeatID].vLoc
				ENDIF
			BREAK
		ENDSWITCH 
	ELSE
		IF iSeatID > -1 AND iSeatID < GET_MP_TV_NUMBER_OF_SEATS(MPTVLocal, property)
			//CDEBUG2LN(DEBUG_MP_TV, "GET_MP_TV_SEAT_LOCATE1: In Yacht or office and Returning: ", MPTVLocal.seatLocates1[iSeatID].vLoc, " for seat: ", iSeatID)
			RETURN MPTVLocal.seatLocates1[iSeatID].vLoc
		ENDIF
	ENDIF
	
	RETURN <<0,0,0>>
ENDFUNC

/// PURPOSE:
///    Gets the width of the angled locate for a specified seat of a specified property
/// PARAMS:
///    property - property ID of property to find seat in
///    iSeatID - seat ID of seat to get width of
/// RETURNS:
///    width of seat as a VECTOR
FUNC FLOAT GET_MP_TV_SEAT_LOCATE_WIDTH(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property, INT iSeatID)
	IF NOT IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
	AND NOT IS_PROPERTY_OFFICE(property.iIndex)
	AND NOT IS_PROPERTY_CLUBHOUSE(property.iIndex)
	AND NOT IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
	AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
	AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	AND NOT MPTVLocal.bInCasinoApartment
	#IF FEATURE_CASINO_HEIST
	AND NOT MPTVLocal.bInArcade
	#ENDIF
	#IF FEATURE_FIXER
	AND NOT MPTVLocal.bInFixerHQ
	#ENDIF
		SWITCH property.iGarageSize
			CASE PROP_GARAGE_SIZE_2
				SWITCH iSeatID
					CASE 0
						RETURN 1.5
					BREAK
					CASE 1
						RETURN 1.5
					BREAK
					CASE 2
						RETURN 1.5
					BREAK
					CASE 3
						RETURN 1.5
					BREAK
				ENDSWITCH
			BREAK
			CASE PROP_GARAGE_SIZE_6
				SWITCH iSeatID
					CASE 0
						RETURN 1.5
					BREAK
					CASE 1
						RETURN 1.5
					BREAK
					CASE 2
						RETURN 1.5
					BREAK
					CASE 3
						RETURN 1.5
					BREAK
				ENDSWITCH
			BREAK
			CASE PROP_GARAGE_SIZE_10
				SWITCH iSeatID
					CASE 0
						RETURN 1.0
					BREAK
					CASE 1
						RETURN 1.0
					BREAK
					CASE 2
						RETURN 1.0
					BREAK
					CASE 3
						RETURN 1.0
					BREAK
					CASE 4
						RETURN 1.0
					BREAK
					CASE 5
						RETURN 1.0
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH 
	
	ELSE
		SWITCH iSeatID
			CASE 0
				RETURN 1.0
			BREAK
			CASE 1
				RETURN 1.0
			BREAK
			CASE 2
				RETURN 1.0
			BREAK
			CASE 3
				RETURN 1.0
			BREAK
			CASE 4
				RETURN 1.0
			BREAK
			CASE 5
				RETURN 1.0
			BREAK
			case 6
				return 1.0
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN 0.0
ENDFUNC

/// PURPOSE:
///    Returns if the local player can switch to a specified channel
/// PARAMS:
///    thisChannel - desired channel
///    property - property ID of current property
/// RETURNS:
///    true/false
FUNC BOOL CAN_SWITCH_TO_THIS_MP_TV_CHANNEL(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_TV_CHANNEL thisChannel, MP_PROPERTY_STRUCT &property)
	INT iEntrance
	
	SWITCH thisChannel
		CASE MP_TV_CHANNEL_CCTV_0
		CASE MP_TV_CHANNEL_CCTV_1
		CASE MP_TV_CHANNEL_CCTV_2
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
			OR IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
			OR IS_PLAYER_IN_HANGAR(PLAYER_ID())
			OR IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
			OR MPTVLocal.bInCasinoApartment
			#IF FEATURE_CASINO_HEIST
			OR MPTVLocal.bInArcade
			#ENDIF
			#IF FEATURE_FIXER
			OR MPTVLocal.bInFixerHQ
			#ENDIF
				RETURN FALSE
			ENDIF
			
			iEntrance = ENUM_TO_INT(thisChannel)
			IF iEntrance < MAX_ENTRANCES
				IF property.entrance[iEntrance].iType <> 0
					IF property.cctv[iEntrance].vPos.x <> 0
					AND property.cctv[iEntrance].vPos.y <> 0
					AND property.cctv[iEntrance].vPos.z <> 0
						RETURN TRUE
					ELSE
						CDEBUG1LN(DEBUG_MP_TV, "CAN_SWITCH_TO_THIS_MP_TV_CHANNEL: CCTV: coords are 0,0,0, iEntrance = ", iEntrance)
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_MP_TV, "CAN_SWITCH_TO_THIS_MP_TV_CHANNEL: CCTV: iType = 0")
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_MP_TV, "CAN_SWITCH_TO_THIS_MP_TV_CHANNEL: CCTV: iEntrance > MAX_ENTRANCES: Turn False: iEntrance = ", iEntrance)
			ENDIF
		BREAK
		CASE MP_TV_CHANNEL_CCTV_GARAGE
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
			OR IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
			OR IS_PROPERTY_CLUBHOUSE(property.iIndex)
			OR IS_PLAYER_IN_HANGAR(PLAYER_ID())
			or IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
			OR MPTVLocal.bInCasinoApartment
			#IF FEATURE_CASINO_HEIST
			OR MPTVLocal.bInArcade
			#ENDIF
			#IF FEATURE_FIXER
			OR MPTVLocal.bInFixerHQ
			#ENDIF
				RETURN FALSE
			ENDIF
			//if property has a garage
			IF property.iGarageSize <> 0
				RETURN TRUE
			ENDIF
		BREAK
		CASE MP_TV_CHANNEL_SPECTATE
			IF MPTVLocal.bInCasinoApartment
			AND NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
				RETURN FALSE
			ENDIF
			
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
				RETURN FALSE
			ENDIF
			IF IS_ANY_PLAYER_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER(SPEC_MODE_TV_CHANNEL)
				CDEBUG1LN(DEBUG_MP_TV, "CAN_SWITCH_TO_THIS_MP_TV_CHANNEL: IS_ANY_PLAYER_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER: SPEC_MODE_TV_CHANNEL: TRUE")
				RETURN TRUE
			ELSE
				CDEBUG1LN(DEBUG_MP_TV, "CAN_SWITCH_TO_THIS_MP_TV_CHANNEL: IS_ANY_PLAYER_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER: SPEC_MODE_TV_CHANNEL: FALSE")
			ENDIF
		BREAK
		CASE MP_TV_CHANNEL_NEWS
			IF MPTVLocal.bInCasinoApartment
			AND NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
				RETURN FALSE
			ENDIF
			
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_MISSION
				RETURN FALSE
			ENDIF
			IF NOT IS_ANY_PLAYER_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER(SPEC_MODE_NEWS)
				CDEBUG1LN(DEBUG_MP_TV, "CAN_SWITCH_TO_THIS_MP_TV_CHANNEL: IS_ANY_PLAYER_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER: SPEC_MODE_NEWS: FALSE")
				RETURN FALSE
			ENDIF
			IF GlobalServerBD_FM.bMostWantedCriminalSet
				CDEBUG1LN(DEBUG_MP_TV, "CAN_SWITCH_TO_THIS_MP_TV_CHANNEL: IS_ANY_PLAYER_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER: SPEC_MODE_NEWS: TRUE, bMostWantedCriminalSet = TRUE")
				RETURN TRUE
			ENDIF
		BREAK
		CASE MP_TV_CHANNEL_CNT
		CASE MP_TV_CHANNEL_WEAZEL
			RETURN TRUE
		CASE MP_TV_CHANNEL_ARCADE
			IF MPTVLocal.bInArcade
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the channel type (scaleform, fullscreen etc) or a specified channel
/// PARAMS:
///    thisChannel - channel ID of channel to get channel type of
/// RETURNS:
///    channel type as TVCHANNELTYPE
FUNC TVCHANNELTYPE GET_MP_TV_CHANNEL_TVCHANNELTYPE(MP_TV_CHANNEL thisChannel)
	
	SWITCH thisChannel
		CASE MP_TV_CHANNEL_CNT
			RETURN TVCHANNELTYPE_CHANNEL_1
		BREAK
		
		CASE MP_TV_CHANNEL_WEAZEL
			RETURN TVCHANNELTYPE_CHANNEL_2
		BREAK
		
		CASE MP_TV_CHANNEL_SPECTATE
		CASE MP_TV_CHANNEL_NEWS
		CASE MP_TV_CHANNEL_CCTV_0
		CASE MP_TV_CHANNEL_CCTV_1
		CASE MP_TV_CHANNEL_CCTV_2
		CASE MP_TV_CHANNEL_CCTV_GARAGE
			RETURN TVCHANNELTYPE_CHANNEL_SPECIAL
		BREAK
		
		CASE MP_TV_CHANNEL_ARCADE
			RETURN TVCHANNELTYPE_CHANNEL_3
		BREAK
	ENDSWITCH
	
	RETURN TVCHANNELTYPE_CHANNEL_NONE
ENDFUNC

PROC SET_TV_CHANNEL_AND_PLAYLIST(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_TV_CHANNEL thisChannel)
	IF MPTVLocal.bInCasinoApartment
	AND NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
		TVCHANNELTYPE thisChannelType = GET_MP_TV_CHANNEL_TVCHANNELTYPE(thisChannel)
		
		SWITCH thisChannel
			CASE MP_TV_CHANNEL_CNT
				SET_TV_CHANNEL_PLAYLIST(thisChannelType, "PL_CINEMA_ACTION")
			BREAK
			
			CASE MP_TV_CHANNEL_WEAZEL
				SET_TV_CHANNEL_PLAYLIST(thisChannelType, "PL_CINEMA_CARTOON")
			BREAK
		ENDSWITCH
		
		SET_TV_CHANNEL(thisChannelType)
	ELSE
		TVCHANNELTYPE thisChannelType = GET_MP_TV_CHANNEL_TVCHANNELTYPE(thisChannel)
		
		IF thisChannelType = TVCHANNELTYPE_CHANNEL_SPECIAL
			SWITCH thisChannel
				CASE MP_TV_CHANNEL_SPECTATE
				CASE MP_TV_CHANNEL_NEWS
					SET_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_SPECIAL, GET_XML_PLAYLIST_FOR_TV_PLAYLIST(TV_PLAYLIST_SPECIAL_MP_WEAZEL))
				BREAK
				
				CASE MP_TV_CHANNEL_CCTV_0
				CASE MP_TV_CHANNEL_CCTV_1
				CASE MP_TV_CHANNEL_CCTV_2
				CASE MP_TV_CHANNEL_CCTV_GARAGE
					SET_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_SPECIAL, GET_XML_PLAYLIST_FOR_TV_PLAYLIST(TV_PLAYLIST_SPECIAL_MP_CCTV))
				BREAK
			ENDSWITCH
		ENDIF
		
		IF thisChannel = MP_TV_CHANNEL_ARCADE
			STRING sArcadePL = ""
			
			PLAYER_INDEX pOwner = GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()
			IF pOwner != INVALID_PLAYER_INDEX()
				SWITCH GET_OWNED_ARCADE_SIMPLE_INTERIOR(pOwner)
					CASE SIMPLE_INTERIOR_ARCADE_PALETO_BAY			sArcadePL = "ARCADE_PIXELPETES_PL"		BREAK
					CASE SIMPLE_INTERIOR_ARCADE_GRAPESEED			sArcadePL = "ARCADE_WONDERAMA_PL"		BREAK
					CASE SIMPLE_INTERIOR_ARCADE_DAVIS				sArcadePL = "ARCADE_THEWAREHOUSE_PL"	BREAK
					CASE SIMPLE_INTERIOR_ARCADE_WEST_VINEWOOD		sArcadePL = "ARCADE_EIGHTBIT_PL"		BREAK
					CASE SIMPLE_INTERIOR_ARCADE_ROCKFORD_HILLS		sArcadePL = "ARCADE_INSERTCOIN_PL"		BREAK
					CASE SIMPLE_INTERIOR_ARCADE_LA_MESA				sArcadePL = "ARCADE_VIDEOGEDDON_PL"		BREAK
				ENDSWITCH
			ELSE
				sArcadePL = "ARCADE_PIXELPETES_PL"
			ENDIF
			
			SET_TV_CHANNEL_PLAYLIST(TVCHANNELTYPE_CHANNEL_3, sArcadePL)
		ENDIF
		
		SET_TV_CHANNEL(thisChannelType)
	ENDIF
ENDPROC

/// PURPOSE:
///    Returns if the specified channel is a CCTV channel
/// PARAMS:
///    thisChannel - channel ID to check
/// RETURNS:
///    true/false
FUNC BOOL IS_MP_TV_CHANNEL_CCTV(MP_TV_CHANNEL thisChannel)
	
	IF thisChannel = MP_TV_CHANNEL_CCTV_0
	OR thisChannel = MP_TV_CHANNEL_CCTV_1
	OR thisChannel = MP_TV_CHANNEL_CCTV_2
	OR thisChannel = MP_TV_CHANNEL_CCTV_GARAGE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Returns if the specified channel is fullscreen
/// PARAMS:
///    thisChannel - channel ID to check
/// RETURNS:
///    true/false
FUNC BOOL IS_MP_TV_CHANNEL_FULLSCREEN(MP_TV_CHANNEL thisChannel)
	
	IF IS_MP_TV_CHANNEL_CCTV(thisChannel)
	OR thisChannel = MP_TV_CHANNEL_SPECTATE
	OR thisChannel = MP_TV_CHANNEL_NEWS
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the server stage the client requires based on if they are in the garage
/// PARAMS:
///    MPTVServer - server broadcast data
///    MPTVClient - client broadcast data
/// RETURNS:
///    server stage as a MP_TV_SERVER_STAGE
FUNC MP_TV_SERVER_STAGE GET_MP_TV_SERVER_STAGE(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient)
	
	IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
		RETURN MPTVServer.eGarageStage
	ELSE
		RETURN MPTVServer.eStage
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    Gets the server current channel the client requires based on if they are in the garage
/// PARAMS:
///    MPTVServer - server broadcast data
///    MPTVClient - client broadcast data
/// RETURNS:
///    tv channel as a MP_TV_CHANNEL
FUNC MP_TV_CHANNEL GET_MP_TV_SERVER_CURRENT_CHANNEL(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient)
	
	IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
		RETURN MPTVServer.eCurrentGarageChannel
	ELSE
		RETURN MPTVServer.eCurrentChannel
	ENDIF
	
ENDFUNC

/// PURPOSE:
///    Server sets the TV server stage
/// PARAMS:
///    MPTVServer - server broadcast data
///    eStage - desired stage
///    bGarage - true if in garage
PROC SET_MP_TV_SERVER_STAGE(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_SERVER_STAGE eStage, BOOL bGarage)
	IF bGarage
		#IF IS_DEBUG_BUILD
			IF MPTVServer.eGarageStage <> eStage
				CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV SERVER === setting garage stage to ", GET_MP_TV_SERVER_STAGE_NAME(eStage))
			ENDIF
		#ENDIF
		MPTVServer.eGarageStage = eStage
	ELSE
		#IF IS_DEBUG_BUILD
			IF MPTVServer.eStage <> eStage
				CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV SERVER === setting stage to ", GET_MP_TV_SERVER_STAGE_NAME(eStage))
			ENDIF
		#ENDIF
		MPTVServer.eStage = eStage
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the TV client stage
/// PARAMS:
///    MPTVClient - client broadcast data
///    eStage - desired stage
PROC SET_MP_TV_CLIENT_STAGE(MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_CLIENT_STAGE eStage)
	DEBUG_PRINTCALLSTACK()
	#IF IS_DEBUG_BUILD
		IF MPTVClient.eStage <> eStage
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV CLIENT === setting stage to ", GET_MP_TV_CLIENT_STAGE_NAME(eStage))
		ELSE
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV CLIENT === already on stage ", GET_MP_TV_CLIENT_STAGE_NAME(eStage))
		ENDIF
	#ENDIF
	MPTVClient.eStage = eStage
ENDPROC

/// PURPOSE:
///    Sets the local screen overlay object as visible
/// PARAMS:
///    MPTVLocal - local data struct
///    bTrue - true if screen overlay should be visible
PROC SET_MP_TV_SCREEN_OBJECT_VISIBLE(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, BOOL bTrue)
	IF DOES_ENTITY_EXIST(MPTVLocal.objOverlay)
		IF bTrue
			IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SCREEN_OBJECT_VISIBLE)
				SET_ENTITY_VISIBLE(MPTVLocal.objOverlay, TRUE)
				SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_SCREEN_OBJECT_VISIBLE)
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_SCREEN_OBJECT_VISIBLE)")
				#ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SCREEN_OBJECT_VISIBLE)
				SET_ENTITY_VISIBLE(MPTVLocal.objOverlay, FALSE)
				CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_SCREEN_OBJECT_VISIBLE)
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_SCREEN_OBJECT_VISIBLE)")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the server current TV volume (for local player to copy)
/// PARAMS:
///    MPTVServer - server broadcast data
///    MPTVClient - client broadcast data
/// RETURNS:
///    volume of tv as a float
FUNC FLOAT GET_MP_TV_SERVER_CURRENT_VOLUME(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient)
	
	IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
		RETURN MPTVServer.fGarageVolume
	ELSE
		RETURN MPTVServer.fVolume
	ENDIF
	
	RETURN 0.0
ENDFUNC

/// PURPOSE:
///    Gets the current desired TV volume, mostly being the server's volume, or a minimum volume in special circumstances
/// PARAMS:
///    MPTVServer - server broadcast data
///    MPTVClient - client broadcast data
///    MPTVLocal - local data struct
/// RETURNS:
///    desired volume as a FLOAT
FUNC FLOAT MP_TV_GET_DESIRED_TV_VOLUME(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
	AND MPTVLocal.bInCasinoApartment
		IF g_sShopSettings.playerRoom = 250816423
		OR g_sShopSettings.playerRoom = -1135649904
		OR g_sShopSettings.playerRoom = -478270020
		OR g_sShopSettings.playerRoom = 2084516274
			RETURN INTERP_FLOAT(GET_TV_VOLUME(), -50, 0.05)
		ELSE
			RETURN INTERP_FLOAT(GET_TV_VOLUME(), GET_MP_TV_SERVER_CURRENT_VOLUME(MPTVServer, MPTVClient), 0.05)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SET_FOR_STRIPPER_MUSIC)
	AND NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
		RETURN MP_TV_MIN_VOLUME
	ELSE
		RETURN GET_MP_TV_SERVER_CURRENT_VOLUME(MPTVServer, MPTVClient)
	ENDIF
	
	RETURN MP_TV_MIN_VOLUME
ENDFUNC

/// PURPOSE:
///    Updates the local tv volume to the correct value
/// PARAMS:
///    MPTVServer - server broadcast data
///    MPTVClient - client broadcast data
///    MPTVLocal - local data struct
PROC UPDATE_MP_TV_VOLUME(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)

	SET_TV_VOLUME(MP_TV_GET_DESIRED_TV_VOLUME(MPTVServer, MPTVClient, MPTVLocal))
	
ENDPROC

FUNC STRING GET_RENDERTARGET_NAME(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property)
	STRING sReturn = "tvscreen"
	
	IF IS_PROPERTY_OFFICE(property.iIndex)	
		sReturn = "ex_tvscreen"
	ENDIF
	
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		sReturn = "tv_flat_01"
	ENDIF
	
	IF MPTVLocal.bInCasinoApartment
		IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
			sReturn = "TV_RT_01a"
		ELSE
			sReturn = "tvscreen"
		ENDIF
	ENDIF
	
	#IF FEATURE_FIXER
	IF MPTVLocal.bInFixerHQ
		IF IS_BIT_SET(MPTVLocal.iBitset2, MPTV_LOCAL_BS2_THIRD_TV)
			sReturn = "tv_flat_scr_01a"
		ELSE
			sReturn = "tvscreen"
		ENDIF
	ENDIF
	#ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF MPTVLocal.bInArcade
		sReturn = "ch_TV_RT_01a"
	ENDIF
	#ENDIF
	
	IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
	OR g_bCleanupHangarTVRenderTarget
		sReturn = "TV_Flat_01"
	ENDIF
		
	CDEBUG1LN(DEBUG_MP_TV, "GET_RENDERTARGET/_NAME: returning: ", sReturn )
	RETURN sReturn 
ENDFUNC

FUNC BOOL SETUP_RENDERTARGET_FROM_OBJECT(MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property)
	CDEBUG1LN(DEBUG_MP_TV, "SETUP_RENDERTARGET_FROM_OBJECT")
	OBJECT_INDEX objTVProp
	
	IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SHOULD_RENDER_TARGET_EXIST)
		SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_SHOULD_RENDER_TARGET_EXIST)
		#IF IS_DEBUG_BUILD
		//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_SHOULD_RENDER_TARGET_EXIST)")
		#ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_OBJECT_GRABBED)

		VECTOR vOverlayCoords
		#IF IS_DEBUG_BUILD
		VECTOR vOverlayRotation
		#ENDIF
		CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === Property ID: ", property.iBuildingID)
		CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === TV Stage: ", ENUM_TO_INT(MPTVClient.eStage))
		
					
		objTVProp = GET_CLOSEST_OBJECT_OF_TYPE(GET_MP_TV_POSITION(MPTVLocal, MPTVClient, property, FALSE), 7.0, GET_MP_TV_MODEL(MPTVLocal, property, MPTVClient), FALSE)
		MPTVLocal.objOverlay = objTVProp
		IF objTVProp <> NULL
			vOverlayCoords = GET_ENTITY_COORDS(objTVProp)
			#IF IS_DEBUG_BUILD
			vOverlayRotation = GET_ENTITY_ROTATION(objTVProp)
			#ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === objTVProp is NULL ")
		ENDIF
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === Grabbed TV model: ", GET_MP_TV_MODEL(MPTVLocal, property, MPTVClient))
		CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === vOverlayRotation, ", vOverlayRotation)
		CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === vOverlayCoords, ", vOverlayCoords)
		#ENDIF
		
		IF vOverlayCoords.x <> 0.0
		OR vOverlayCoords.y <> 0.0
		OR vOverlayCoords.z <> 0.0

			IF IS_NAMED_RENDERTARGET_REGISTERED(GET_RENDERTARGET_NAME(MPTVLocal, property))
				RELEASE_NAMED_RENDERTARGET(GET_RENDERTARGET_NAME(MPTVLocal, property))
			ENDIF
		
			SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_OBJECT_GRABBED)

			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === Created TV model")
			ATTACH_TV_AUDIO_TO_ENTITY(MPTVLocal.objOverlay)
			//MPTVLocal.objOverlay = CREATE_OBJECT(GET_MP_TV_OVERLAY_MODEL(property), vOverlayCoords, FALSE, FALSE)
			//SET_ENTITY_ROTATION(MPTVLocal.objOverlay, vOverlayRotation)
			//SET_ENTITY_COORDS(MPTVLocal.objOverlay, vOverlayCoords)
			//SET_ENTITY_LOD_DIST(MPTVLocal.objOverlay, 200)
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SET_ENTITY_ROTATION(MPTVLocal.objOverlay, ", vOverlayRotation, ")")
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SET_ENTITY_COORDS(MPTVLocal.objOverlay, ", vOverlayCoords, ")")
			#ENDIF
			SET_MP_TV_SCREEN_OBJECT_VISIBLE(MPTVLocal, FALSE)
		ENDIF
	ELSE
	
		SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_OBJECT_CREATED)
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_OBJECT_CREATED)")
		#ENDIF
		
		IF NOT IS_NAMED_RENDERTARGET_REGISTERED(GET_RENDERTARGET_NAME(MPTVLocal, property))
			
			REGISTER_NAMED_RENDERTARGET(GET_RENDERTARGET_NAME(MPTVLocal, property))
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === registering rendertarget TV")
			#ENDIF
			
		ELSE
			IF DOES_ENTITY_EXIST(MPTVLocal.objOverlay)
				IF IS_MODEL_VALID(GET_ENTITY_MODEL(MPTVLocal.objOverlay))	
					IF IS_NAMED_RENDERTARGET_LINKED(GET_ENTITY_MODEL(MPTVLocal.objOverlay))
						MPTVLocal.iRenderTarget = GET_NAMED_RENDERTARGET_RENDER_ID(GET_RENDERTARGET_NAME(MPTVLocal, property))
						MPTVLocal.tvChannelTypeCurrent = GET_MP_TV_CHANNEL_TVCHANNELTYPE(MPTVLocal.eCurrentChannel)
						RESTORE_STANDARD_CHANNELS()
						SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)//SET_TV_CHANNEL(MPTVLocal.tvChannelTypeCurrent)
						
						SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_RENDER_TARGET_FOUND)
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_RENDER_TARGET_FOUND)")
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SETUP_RENDERTARGET = TRUE")
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SETUP_RENDERTARGET_FROM_OBJECT - IS_NAMED_RENDERTARGET_LINKED = TRUE")
						#ENDIF
						RESET_NET_TIMER(MPTVLocal.timeSetupRendertargetFail)
						RETURN TRUE
					ELSE
						RELEASE_NAMED_RENDERTARGET(GET_RENDERTARGET_NAME(MPTVLocal, property))
						LINK_NAMED_RENDERTARGET(GET_ENTITY_MODEL(MPTVLocal.objOverlay))

						#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === IS_NAMED_RENDERTARGET_LINKED = FALSE")
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === IS_MODEL_VALID(GET_ENTITY_MODEL(MPTVLocal.objOverlay)) - FALSE")
					#ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_OBJECT_GRABBED)
					CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_OBJECT_GRABBED)
				ENDIF	
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === DOES_ENTITY_EXIST(MPTVLocal.objOverlay)- FALSE")
				#ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(MPTVLocal.timeSetupRendertargetFail, MP_TV_SETUP_RENDERTARGET_TIMEOUT)
		RESET_NET_TIMER(MPTVLocal.timeSetupRendertargetFail)
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SETUP_RENDERTARGET_FROM_OBJECT return true due to timeout")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Setup screen overlay and render target for scaleform TV
/// PARAMS:
///    MPTVClient - client broadcast data
///    MPTVLocal - local data struct
///    property - current property ID
/// RETURNS:
///    true when complete
FUNC BOOL SETUP_RENDERTARGET(MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property)
	CDEBUG1LN(DEBUG_MP_TV, "SETUP_RENDERTARGET")
	BOOL bForceGarage = FALSE
	
	IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SHOULD_RENDER_TARGET_EXIST)
		SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_SHOULD_RENDER_TARGET_EXIST)
		#IF IS_DEBUG_BUILD
		//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_SHOULD_RENDER_TARGET_EXIST)")
		#ENDIF
	ENDIF
	
	IF MPTVLocal.eCurrentChannel = MP_TV_CHANNEL_CCTV_GARAGE
	AND MPTVClient.eStage = MP_TV_CLIENT_STAGE_ACTIVATED
	AND property.iGarageSize = PROP_GARAGE_SIZE_10
	AND NOT IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_OFF)
	AND NOT IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_DEACTIVATING)
	AND NOT IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
		CDEBUG1LN(DEBUG_MP_TV, "Setting to be in garage")
		bForceGarage = TRUE
	ELSE
		CDEBUG1LN(DEBUG_MP_TV, "Setting to not be in garage")
	ENDIF
	
	
	IF NOT DOES_ENTITY_EXIST(MPTVLocal.objOverlay)

//	AND NOT g_bCullUnnecessaryAptPropsHeistForHiestWalkOut
		VECTOR vOverlayCoords, vOverlayRotation
		
		IF IS_PROPERTY_BUSINESS_APARTMENT(property.iIndex)
		
			
			GET_MP_TV_COORD_AND_ROT(MPTVLocal, MPTVClient, property, vOverlayCoords, vOverlayRotation, bForceGarage)
			#IF IS_DEBUG_BUILD
			
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === vOverlayRotation, ", vOverlayRotation)
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === vOverlayCoords, ", vOverlayCoords)
			#ENDIF
		ELSE
		
			OBJECT_INDEX objTVProp
			
			objTVProp = GET_CLOSEST_OBJECT_OF_TYPE(GET_MP_TV_POSITION(MPTVLocal, MPTVClient, property, bForceGarage), 5.0, GET_MP_TV_MODEL(MPTVLocal, property, MPTVClient), FALSE)
			CDEBUG1LN(DEBUG_MP_TV, "SETUP_RENDERTARGET: GET_CLOSEST_OBJECT_OF_TYPE ")
			#IF IS_DEBUG_BUILD
			//VECTOR vTemp = GET_MP_TV_POSITION(MPTVLocal, MPTVClient, property, bForceGarage)
			//CDEBUG2LN(DEBUG_MP_TV, "SETUP_RENDERTARGET: GET_MP_TV_POSITION = ", vTemp, ", GET_MP_TV_MODEL = ", GET_MP_TV_MODEL(MPTVLocal, property, MPTVClient))
			#ENDIF
			IF objTVProp <> NULL
				vOverlayCoords = GET_ENTITY_COORDS(objTVProp)
				vOverlayRotation = GET_ENTITY_ROTATION(objTVProp)
			ENDIF
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === Grabbed TV model: ", GET_MP_TV_MODEL(MPTVLocal, property, MPTVClient))
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === vOverlayRotation, ", vOverlayRotation)
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === vOverlayCoords, ", vOverlayCoords)
			#ENDIF
			
		ENDIF
		
		IF vOverlayCoords.x <> 0.0
		OR vOverlayCoords.y <> 0.0
		OR vOverlayCoords.z <> 0.0
	
			IF IS_NAMED_RENDERTARGET_REGISTERED(GET_RENDERTARGET_NAME(MPTVLocal, property))
				RELEASE_NAMED_RENDERTARGET(GET_RENDERTARGET_NAME(MPTVLocal, property))
			ENDIF
		
			CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_OBJECT_CREATED)

			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === Created TV model")
			MPTVLocal.objOverlay = CREATE_OBJECT(GET_MP_TV_OVERLAY_MODEL(MPTVLocal, property), vOverlayCoords, FALSE, FALSE)
			SET_ENTITY_ROTATION(MPTVLocal.objOverlay, vOverlayRotation)
			SET_ENTITY_COORDS(MPTVLocal.objOverlay, vOverlayCoords)
			SET_ENTITY_LOD_DIST(MPTVLocal.objOverlay, 200)
			ATTACH_TV_AUDIO_TO_ENTITY(MPTVLocal.objOverlay)
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SET_ENTITY_ROTATION(MPTVLocal.objOverlay, ", vOverlayRotation, ")")
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SET_ENTITY_COORDS(MPTVLocal.objOverlay, ", vOverlayCoords, ")")
			#ENDIF
			SET_MP_TV_SCREEN_OBJECT_VISIBLE(MPTVLocal, FALSE)
		
		ENDIF
//	ELIF g_bCullUnnecessaryAptPropsHeistForHiestWalkOut
//			
//		//CDEBUG2LN(DEBUG_MP_TV, "Not creating TV MPTVLocal.objOverlay as we're in activity session and in corona or launching a heist session")
		
	ELSE
	
		SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_OBJECT_CREATED)
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_OBJECT_CREATED)")
		#ENDIF
		
		IF NOT IS_NAMED_RENDERTARGET_REGISTERED(GET_RENDERTARGET_NAME(MPTVLocal, property))
			
			REGISTER_NAMED_RENDERTARGET(GET_RENDERTARGET_NAME(MPTVLocal, property))
			#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === registering rendertarget TV")
			#ENDIF
			
		ELSE
			IF DOES_ENTITY_EXIST(MPTVLocal.objOverlay)
				IF IS_MODEL_VALID(GET_ENTITY_MODEL(MPTVLocal.objOverlay))	
					IF IS_NAMED_RENDERTARGET_LINKED(GET_ENTITY_MODEL(MPTVLocal.objOverlay))
						MPTVLocal.iRenderTarget = GET_NAMED_RENDERTARGET_RENDER_ID(GET_RENDERTARGET_NAME(MPTVLocal, property))
						MPTVLocal.tvChannelTypeCurrent = GET_MP_TV_CHANNEL_TVCHANNELTYPE(MPTVLocal.eCurrentChannel)
						RESTORE_STANDARD_CHANNELS()
						SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)//SET_TV_CHANNEL(MPTVLocal.tvChannelTypeCurrent)
						
						SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_RENDER_TARGET_FOUND)
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_RENDER_TARGET_FOUND)")
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SETUP_RENDERTARGET = TRUE")
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SETUP_RENDERTARGET - IS_NAMED_RENDERTARGET_LINKED = TRUE")
						#ENDIF
						RESET_NET_TIMER(MPTVLocal.timeSetupRendertargetFail)
						RETURN TRUE
					ELSE
						RELEASE_NAMED_RENDERTARGET(GET_RENDERTARGET_NAME(MPTVLocal, property))
						LINK_NAMED_RENDERTARGET(GET_ENTITY_MODEL(MPTVLocal.objOverlay))

						#IF IS_DEBUG_BUILD
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === IS_NAMED_RENDERTARGET_LINKED = FALSE")
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === IS_MODEL_VALID(GET_ENTITY_MODEL(MPTVLocal.objOverlay)) - FALSE")
					#ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_OBJECT_GRABBED)
					CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_OBJECT_GRABBED)
				ENDIF	
				#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === DOES_ENTITY_EXIST(MPTVLocal.objOverlay)- FALSE")
				#ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(MPTVLocal.timeSetupRendertargetFail, MP_TV_SETUP_RENDERTARGET_TIMEOUT)
		RESET_NET_TIMER(MPTVLocal.timeSetupRendertargetFail)
		#IF IS_DEBUG_BUILD
		CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SETUP_RENDERTARGET return true due to timeout")
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Clears the render target for the local machine
/// PARAMS:
///    MPTVLocal - 
PROC CLIENT_CLEAR_TV_RENDERTARGET(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === CLIENT_CLEAR_TV_RENDERTARGET")
	IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_RENDER_TARGET_FOUND)
		//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_RENDER_TARGET_FOUND)")
		IF DOES_ENTITY_EXIST(MPTVLocal.objOverlay)
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === DOES_ENTITY_EXIST(MPTVLocal.objOverlay)")
			IF IS_NAMED_RENDERTARGET_LINKED(GET_ENTITY_MODEL(MPTVLocal.objOverlay))
				SET_TEXT_RENDER_ID(MPTVLocal.iRenderTarget)
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
				SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
				DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 255)  // Clears the render target to black.
				SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
				#IF IS_DEBUG_BUILD
				CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === RENDERTARGET CLEARED")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === IS_NAMED_RENDERTARGET_LINKED(GET_ENTITY_MODEL(MPTVLocal.objOverlay)) = FALSE")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Destroy and unload the screen overlay and render target
/// PARAMS:
///    MPTVLocal - local data struct
PROC CLEANUP_RENDERTARGET(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property, MP_TV_CLIENT_DATA_STRUCT &MPTVClient , BOOL bSetTvToNull = TRUE)

	CLIENT_CLEAR_TV_RENDERTARGET(MPTVLocal)

	IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SHOULD_RENDER_TARGET_EXIST)
		CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_SHOULD_RENDER_TARGET_EXIST)
		#IF IS_DEBUG_BUILD
		//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_SHOULD_RENDER_TARGET_EXIST)")
		#ENDIF
	ENDIF

	CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_OBJECT_CREATED)
	
	IF DOES_ENTITY_EXIST(MPTVLocal.objOverlay)
	AND NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_OBJECT_GRABBED)
		IF GET_PROPERTY_SIZE_TYPE(property.iIndex) = PROP_SIZE_TYPE_MED_APT
		OR GET_PROPERTY_SIZE_TYPE(property.iIndex) = PROP_SIZE_TYPE_SMALL_APT
		OR (IS_BIT_SET(MPTVLocal.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
		AND NOT IS_PROPERTY_OFFICE(property.iIndex)
		AND NOT IS_PROPERTY_CLUBHOUSE(property.iIndex)
		AND NOT IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
		AND NOT IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
		#IF FEATURE_CASINO_HEIST
		AND NOT IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
		#ENDIF
		)
		OR IS_PROPERTY_BUSINESS_APARTMENT(property.iIndex)
		OR IS_PROPERTY_ORIGINAL_HIGH_END_APT(property.iIndex)
		
			#IF IS_DEBUG_BUILD
			IF GET_PROPERTY_SIZE_TYPE(property.iIndex) = PROP_SIZE_TYPE_MED_APT
				CDEBUG1LN(DEBUG_MP_TV, "CLEANUP_TV_PROP: Deleting render target: because PROP_SIZE_TYPE_MED_APT")
			ENDIF
			IF GET_PROPERTY_SIZE_TYPE(property.iIndex) = PROP_SIZE_TYPE_SMALL_APT
				CDEBUG1LN(DEBUG_MP_TV, "CLEANUP_TV_PROP: Deleting render target: because PROP_SIZE_TYPE_SMALL_APT")
			ENDIF
			IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
				CDEBUG1LN(DEBUG_MP_TV, "CLEANUP_TV_PROP: Deleting render target: because MPTV_CLIENT_BS_IN_GARAGE")
			ENDIF
			IF IS_PROPERTY_BUSINESS_APARTMENT(property.iIndex)
				CDEBUG1LN(DEBUG_MP_TV, "CLEANUP_TV_PROP: Deleting render target: because IS_PROPERTY_BUSINESS_APARTMENT")
			ENDIF
			IF IS_PROPERTY_ORIGINAL_HIGH_END_APT(property.iIndex)
				CDEBUG1LN(DEBUG_MP_TV, "CLEANUP_TV_PROP: Deleting render target: because IS_PROPERTY_ORIGINAL_HIGH_END_APT")
			ENDIF
			#ENDIF
		
			CDEBUG1LN(DEBUG_MP_TV, "CLEANUP_TV_PROP: Deleting MPTVLocal.objOverlay(The TV)")
			IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(MPTVLocal.objOverlay,FALSE)
				DELETE_OBJECT(MPTVLocal.objOverlay)
			ELSE
				CDEBUG1LN(DEBUG_MP_TV, "CLEANUP_TV_PROP: Deleting MPTVLocal.objOverlay(The TV)- Can't object does not belong to script.")
			ENDIF
		ELSE
			//MPTVLocal.objOverlay = NULL
			CDEBUG1LN(DEBUG_MP_TV, "CLEANUP_TV_PROP: not deleting tv objOverlay because it isnt' a scripted object as this is either a stilt, custom or yacht apartment")
		ENDIF
	ELSE
		IF bSetTvToNull
			IF MPTVClient.eStage != MP_TV_CLIENT_STAGE_ACTIVATED
				MPTVLocal.objOverlay = NULL
			ENDIF
		ENDIF	
		CDEBUG1LN(DEBUG_MP_TV, "CLEANUP_TV_PROP: MPTVLocal.objOverlay doesn't exist, not cleaning up")
	ENDIF
	
	IF IS_NAMED_RENDERTARGET_REGISTERED(GET_RENDERTARGET_NAME(MPTVLocal, property))
		#IF IS_DEBUG_BUILD
		//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === Releasing previous rendertarget TV")
		#ENDIF
		RELEASE_NAMED_RENDERTARGET(GET_RENDERTARGET_NAME(MPTVLocal, property))
		CDEBUG1LN(DEBUG_MP_TV, "CLEANUP_TV_PROP: RELEASE_NAMED_RENDERTARGET")
		
		IF g_bCleanupHangarTVRenderTarget
			CDEBUG1LN(DEBUG_MP_TV, "CLEANUP_TV_PROP: Clearing Flag: g_bCleanupHangarTVRenderTarget")
			g_bCleanupHangarTVRenderTarget = FALSE
		ENDIF
	ENDIF
	MPTVLocal.iRenderTarget = -1
	CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_RENDER_TARGET_FOUND)
	CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_OBJECT_GRABBED)
ENDPROC

/// PURPOSE:
///    Starts sound effect for changing to a cctv channel
/// PARAMS:
///    MPTVLocal - local data struct
PROC MP_TV_SOUND_START_CCTV_CHANGE_CAM(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	#IF IS_DEBUG_BUILD
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === MP_TV_START_CCTV_CHANGE_CAM_SOUND()")
	#ENDIF
	IF MPTVLocal.iSoundCCTVChangeCam = -1
		#IF IS_DEBUG_BUILD
		//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === PLAY_SOUND_FRONTEND(MPTVLocal.iSoundCCTVChangeCam, Change_Cam, MP_CCTV_SOUNDSET)")
		#ENDIF
		MPTVLocal.iSoundCCTVChangeCam = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(MPTVLocal.iSoundCCTVChangeCam, "Change_Cam", "MP_CCTV_SOUNDSET")
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops sound effect for changing to a cctv channel
/// PARAMS:
///    MPTVLocal - local data struct
PROC MP_TV_SOUND_STOP_CCTV_CHANGE_CAM(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	#IF IS_DEBUG_BUILD
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === MP_TV_STOP_CHANGE_CAM_SOUND()")
	#ENDIF
	IF MPTVLocal.iSoundCCTVChangeCam <> -1
		#IF IS_DEBUG_BUILD
		//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === STOP_SOUND(MPTVLocal.iSoundCCTVChangeCam)")
		#ENDIF
		STOP_SOUND(MPTVLocal.iSoundCCTVChangeCam)
		RELEASE_SOUND_ID(MPTVLocal.iSoundCCTVChangeCam)
		MPTVLocal.iSoundCCTVChangeCam = -1
	ENDIF
ENDPROC

/// PURPOSE:
///    Starts sound effect for background of a cctv channel
/// PARAMS:
///    MPTVLocal - local data struct
PROC MP_TV_SOUND_START_CCTV_BACKGROUND(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	#IF IS_DEBUG_BUILD
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === MP_TV_START_CCTV_BACKGROUND_SOUND()")
	#ENDIF
	IF MPTVLocal.iSoundCCTVBackground = -1
		#IF IS_DEBUG_BUILD
		//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === PLAY_SOUND_FRONTEND(MPTVLocal.iSoundCCTVBackground, Background, MP_CCTV_SOUNDSET)")
		#ENDIF
		MPTVLocal.iSoundCCTVBackground = GET_SOUND_ID()
		PLAY_SOUND_FRONTEND(MPTVLocal.iSoundCCTVBackground, "Background", "MP_CCTV_SOUNDSET")
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops sound effect for background of a cctv channel
/// PARAMS:
///    MPTVLocal - local data struct
PROC MP_TV_SOUND_STOP_CCTV_BACKGROUND(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	#IF IS_DEBUG_BUILD
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === MP_TV_STOP_CCTV_BACKGROUND_SOUND()")
	#ENDIF
	IF MPTVLocal.iSoundCCTVBackground <> -1
		#IF IS_DEBUG_BUILD
		//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === STOP_SOUND(MPTVLocal.iSoundCCTVBackground)")
		#ENDIF
		STOP_SOUND(MPTVLocal.iSoundCCTVBackground)
		RELEASE_SOUND_ID(MPTVLocal.iSoundCCTVBackground)
		MPTVLocal.iSoundCCTVBackground = -1
	ENDIF
ENDPROC

PROC MP_TV_FADE_IN(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal,INT iTime)
	
	IF IS_BIT_SET(MPTVLocal.iBSSettings, ciMPTV_SETTINGS_BS_PREVENT_FADE_IN_THIS_FRAME)
		CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === MP_TV_FADE_IN - NOT Fading in - ciMPTV_SETTINGS_BS_PREVENT_FADE_IN_THIS_FRAME == true")
		EXIT	
	ENDIF
	
	DO_SCREEN_FADE_IN(iTime)
	CLEAR_BIT(MPTVLocal.iSwitchBitset,MPTV_SWITCH_BS_FADE_REQUIRED)
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === MP_TV_FADE_IN(",iTime,")")

ENDPROC

PROC MP_TV_FADE_OUT(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal,INT iTime)
	DO_SCREEN_FADE_OUT(iTime)
	SET_BIT(MPTVLocal.iSwitchBitset,MPTV_SWITCH_BS_FADE_REQUIRED)	
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === MP_TV_FADE_OUT(",iTime,")")
ENDPROC

FUNC BOOL SHOULD_DISPLAY_HUD_CASH()
	IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
		SWITCH GET_SIMPLE_INTERIOR_TYPE_LOCAL_PLAYER_IS_IN()
			CASE SIMPLE_INTERIOR_TYPE_FIXER_HQ
				RETURN FALSE
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Tidies the tv ready to midly reset the data
/// PARAMS:
///    MPTVLocal - lcoal data struct
PROC TIDYUP_MP_TV(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	#IF IS_DEBUG_BUILD
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === Tidyup")
	#ENDIF
	
	MP_TV_SOUND_STOP_CCTV_CHANGE_CAM(MPTVLocal)
	MP_TV_SOUND_STOP_CCTV_BACKGROUND(MPTVLocal)
	
	//LOCAL
	CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLE_EXTERIOR_FOCUS)
	CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_STATIC_OUTSIDE_CAM_ON)
	CLEAR_MP_DECORATOR_BIT(PLAYER_ID(), MP_DECORATOR_BS_IS_SPECTATING)
	ENABLE_WEAPON_SWAP()
	Enable_MP_Comms()
	FORCE_CLEANUP_SPECTATOR_CAM(MPTVLocal.specData)
	IF NOT IS_BIT_SET(MPTVLocal.iBitset,MPTV_LOCAL_BS_DONT_RENDER_RADAR_IN_CLEANUP)
		SET_RADAR_ZOOM(0)
		DISPLAY_RADAR(TRUE)
	ENDIF
	UNLOCK_MINIMAP_ANGLE()
	UNLOCK_MINIMAP_POSITION()
	SET_NOISEOVERIDE(FALSE)
	SET_NOISINESSOVERIDE(0.0)
	
	IF SHOULD_DISPLAY_HUD_CASH()
		DISPLAY_CASH(TRUE)
	ENDIF
	
	IF DOES_CAM_EXIST(MPTVLocal.camCCTV)
	OR DOES_CAM_EXIST(MPTVLocal.camRoom)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
	ENDIF
	IF DOES_CAM_EXIST(MPTVLocal.camCCTV)
		DESTROY_CAM(MPTVLocal.camCCTV)
	ENDIF
	IF DOES_CAM_EXIST(MPTVLocal.camRoom)
		CPRINTLN(DEBUG_SPECTATOR, "=== MP_TV LOCAL SWITCH === DOES_CAM_EXIST = TRUE")
		SET_CAM_ACTIVE(MPTVLocal.camRoom, FALSE)
		DESTROY_CAM(MPTVLocal.camRoom)
	ENDIF
	STOP_AUDIO_SCENE("WATCHING_SAFEHOUSE_TV")
	Unpause_Objective_Text()
	ENABLE_MOVIE_SUBTITLES(FALSE)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
	ENDIF
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(MPTVLocal.sfTV)
	IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iWarpState < 1 // we don't want cleanup_spec. to be called constantly when the yacht is moving, calls clear help text every frame
		CLEANUP_SPECTATOR_CAM_HUD(MPTVLocal.specData)
	ENDIF
	FORCE_CLEANUP_SPECTATOR_CAM(MPTVLocal.specData)
	
	RESET_NET_TIMER(MPTVLocal.timeSetupRendertargetFail)
	
	CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_REVEAL_BUILDING_EXTERIOR)
	
	IF (NOT IS_SCREEN_FADED_IN())
		IF (NOT IS_SCREEN_FADING_IN())
			IF IS_BIT_SET(MPTVLocal.iSwitchBitset,MPTV_SWITCH_BS_FADE_REQUIRED)
				MP_TV_FADE_IN(MPTVLocal,0)	
			ENDIF
		ENDIF
	ENDIF
	
	IF g_bMPTVplayerWatchingTV
	AND IS_NET_PLAYER_OK(PLAYER_ID())
	AND NOT IS_PLAYER_TELEPORT_ACTIVE()
	AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
		NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		CPRINTLN(DEBUG_SPECTATOR, "=== MP_TV LOCAL SWITCH === TIDYUP = SETTING PLAYER CONTROL ON")
	ENDIF
	
	g_bMPTVplayerWatchingTV = FALSE
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === g_bMPTVplayerWatchingTV = FALSE")
	
ENDPROC

/// PURPOSE:
///    Completely cleans up the client data MP tv as part of cleanup of the apartment
/// PARAMS:
///    MPTVClient - client broadcast data
///    MPTVLocal - local data struct
///    property - current property ID
PROC CLEANUP_MP_TV_CLIENT(MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property)
	#IF IS_DEBUG_BUILD
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === Cleanup client")
	#ENDIF
	CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PLAYER_AT_SEAT_NODE_COORD)
	SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
	TIDYUP_MP_TV(MPTVLocal)
	
	IF IS_STRING_NULL_OR_EMPTY(g_drunkCameraTimeCycleName)
		println("CLEANUP_APARTMENT - TIMECYCLE OFF")
		CLEAR_TIMECYCLE_MODIFIER()
		
		IF SHOULD_REACTIVE_CASINO_APARTMENT_TCM()
			IF NOT IS_STRING_NULL_OR_EMPTY(g_tlCasinoApartmentTCM)
				SET_TIMECYCLE_MODIFIER(g_tlCasinoApartmentTCM)
			ENDIF
		ELIF SHOULD_REACTIVE_ARCADE_TCM()
			IF NOT IS_STRING_NULL_OR_EMPTY(g_tlArcadeTCM)
				SET_TIMECYCLE_MODIFIER(g_tlArcadeTCM)
			ENDIF
		ENDIF
	ELSE
		CLEAR_TIMECYCLE_MODIFIER()
		
		println("CLEANUP_APARTMENT - TIMECYCLE OFF, but keep \"", g_drunkCameraTimeCycleName, "\"")
		SET_TIMECYCLE_MODIFIER(g_drunkCameraTimeCycleName)
	ENDIF
	
	//RELEASE_AMBIENT_AUDIO_BANK()
	//LOCAL
	IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)
		CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)
	ENDIF
	
	IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
		CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
	ENDIF
	
	IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_LEAVING_SEAT)
		CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_LEAVING_SEAT)
	ENDIF	
	
	IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SECOND_SEAT)
		CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SECOND_SEAT)
	ENDIF
	
	CDEBUG2LN(DEBUG_MP_TV, "CLEANUP_MP_TV_CLIENT: clear occypy seats")
	
	IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLED_SUICIDE)
		ENABLE_KILL_YOURSELF_OPTION()
		CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLED_SUICIDE)
	ENDIF
	IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLED_INTERACTION)
		ENABLE_INTERACTION_MENU()
		CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLED_INTERACTION)
	ENDIF
	IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)
		CLEAR_FOCUS()
		//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === Cleanup client - CLEAR FOCUS")
		NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
		CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)
	ENDIF
	IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_LOAD_SCENE_RUNNING)
		NEW_LOAD_SCENE_STOP()
		CLEAR_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_LOAD_SCENE_RUNNING)
	ENDIF
	IF IS_BIT_SET(MPTVLocal.iBitset,MPTV_LOCAL_BS_PAUSED_RENDERPHASE)
		TOGGLE_RENDERPHASES(TRUE)
		CLEAR_BIT(MPTVLocal.iBitset,MPTV_LOCAL_BS_PAUSED_RENDERPHASE)
	ENDIF
	
	CLIENT_CLEAR_TV_RENDERTARGET(MPTVLocal)
	CLEANUP_RENDERTARGET(MPTVLocal, property,MPTVClient)
	
	CPRINTLN(DEBUG_SPECTATOR, "=== MP_TV LOCAL === here1")
	
	
	IF MPTVLocal.eCurrentChannel = MP_TV_CHANNEL_SPECTATE
	OR MPTVLocal.eCurrentChannel = MP_TV_CHANNEL_NEWS
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CLEAR_FOCUS()
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === Cleanup client - CLEAR FOCUS - B")
		ENDIF
	ENDIF
	CPRINTLN(DEBUG_SPECTATOR, "=== MP_TV LOCAL === here2")
	
	IF GET_MP_TV_OVERLAY_MODEL(MPTVLocal, property)  != DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_MP_TV_OVERLAY_MODEL(MPTVLocal, property))
	ENDIF
	
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)
	CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_APPROACHED_MP_TV_SEAT)
	
	CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_OBJECT_GRABBED)
	
	MPTVLocal.eCurrentChannel = MP_TV_CHANNEL_CNT
	SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_WATCH_CHANNEL)
	MPTVLocal.tvChannelTypeCurrent = TVCHANNELTYPE_CHANNEL_NONE
	g_iOverheadNamesState = -1					//Turn off player names
	
	IF MPTVLocal.iContextButtonIntention != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention) 
	ENDIF
	MPTVLocal.iBitset = 0
	MPTVLocal.iSwitchBitset = 0
	
	//CLIENT
	MPTVClient.eDesiredChannel = MP_TV_CHANNEL_CNT
	CPRINTLN(DEBUG_SPECTATOR, "=== MP_TV LOCAL === SET_MP_TV_CLIENT_STAGE")
	SET_MP_TV_CLIENT_STAGE(MPTVClient, MP_TV_CLIENT_STAGE_INIT)
	MPTVClient.iBitset = 0
	MPTVClient.iInputBitset = 0	
	CPRINTLN(DEBUG_SPECTATOR, "=== MP_TV LOCAL === end of CLEANUP_MP_TV_CLIENT")
ENDPROC

/// PURPOSE:
///    Completely cleans up the MP tv as part of cleanup of the apartment
/// PARAMS:
///    MPTVServer - server broadcast data
///    MPTVClient - client broadcast data
///    MPTVLocal - local data struct
///    property - current property ID
PROC CLEANUP_MP_TV(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property)
	IF MPTVLocal.bInCasinoApartment
	AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
		SET_TV_AUDIO_FRONTEND(FALSE)
	ENDIF
	
	g_bInTVLocate = FALSE
	#IF IS_DEBUG_BUILD
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === Cleanup")
	#ENDIF
	INT iNumberOfSeats = GET_MP_TV_NUMBER_OF_SEATS(MPTVLocal, property)
	#IF IS_DEBUG_BUILD
	CPRINTLN(DEBUG_AMBIENT,"POD: CLEANUP_MP_TV_CLIENT: GET_MP_TV_NUMBER_OF_SEATS ", GET_MP_TV_NUMBER_OF_SEATS(MPTVLocal, property))
	#ENDIF
	INT i = 0
	REPEAT iNumberOfSeats i
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_AMBIENT,"POD: CLEANUP_MP_TV_CLIENT: looping on seat: ", i)
		#ENDIF
		IF IS_BIT_SET(MPTVServer.sSeat[i].iBitset, MPTV_SEAT_BS_OCCUPIED)
			CLEAR_BIT(MPTVServer.sSeat[i].iBitset, MPTV_SEAT_BS_OCCUPIED)
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_AMBIENT,"POD: CLEANUP_MP_TV_CLIENT: cleaning player occupied seat: ", i)
			#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_AMBIENT,"POD: CLEANUP_MP_TV_CLIENT: player not occuping ", i," seat ")
			#ENDIF	
		ENDIF
	ENDREPEAT
	
	CLEANUP_MP_TV_CLIENT(MPTVClient, MPTVLocal, property)
	CLEAR_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_STOP_MPTV_SPECTATE)
	
	
	//SERVER
	MPTVLocal.iServerPlayerStagger = 0
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_GET_THIS_SCRIPT_IS_NETWORK_SCRIPT()
		AND NETWORK_IS_HOST_OF_THIS_SCRIPT() 
			MPTVServer.eCurrentChannel = MP_TV_CHANNEL_CNT
			MPTVServer.eCurrentGarageChannel = MP_TV_CHANNEL_CNT
			MPTVServer.iBitset = 0
			SET_MP_TV_SERVER_STAGE(MPTVServer, MP_TV_SERVER_STAGE_INIT, TRUE)
			SET_MP_TV_SERVER_STAGE(MPTVServer, MP_TV_SERVER_STAGE_INIT, FALSE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Store seats positions for MP TV in high end apartments
/// PARAMS:
///    MPTVLocal - client broadcast data
///    property - current property ID
PROC STORE_SEAT_POSITIONS(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal,MP_PROPERTY_STRUCT &property)
	IF GET_PROPERTY_SIZE_TYPE(property.iIndex) = PROP_SIZE_TYPE_LARGE_APT
	OR GET_PROPERTY_SIZE_TYPE(property.iIndex) = PROP_SIZE_TYPE_YACHT
	OR IS_PROPERTY_OFFICE(property.iIndex)
	OR IS_PROPERTY_CLUBHOUSE(property.iIndex)
		IF GET_PROPERTY_SIZE_TYPE(property.iIndex) = PROP_SIZE_TYPE_YACHT
			IF GET_YACHT_LOCAL_PLAYER_IS_ON() != -1
				INT iSeatID
				REPEAT GET_MP_TV_NUMBER_OF_SEATS(MPTVLocal, property) iSeatID
					
					CDEBUG1LN(DEBUG_MP_TV, "STORE_SEAT_POSITIONS: GET_YACHT_LOCAL_PLAYER_IS_ON = ", GET_YACHT_LOCAL_PLAYER_IS_ON())
					
					GET_POSITION_AS_OFFSET_FOR_YACHT(GET_YACHT_LOCAL_PLAYER_IS_ON(), MP_PROP_ELEMENT_MPTV_YACHT_SEAT_SCENARI0_0+ iSeatID, MPTVLocal.seatPositions[iSeatID])
					GET_POSITION_AS_OFFSET_FOR_YACHT(GET_YACHT_LOCAL_PLAYER_IS_ON(), MP_PROP_ELEMENT_MPTV_YACHT_SEAT_LOCATE0_0+ iSeatID, MPTVLocal.seatLocates0[iSeatID])
					GET_POSITION_AS_OFFSET_FOR_YACHT(GET_YACHT_LOCAL_PLAYER_IS_ON(), MP_PROP_ELEMENT_MPTV_YACHT_SEAT_LOCATE1_0+ iSeatID, MPTVLocal.seatLocates1[iSeatID])
					

					//MPTVLocal.seatPositions[iSeatID].vLoc.z += 1 //Move the locate back up
					CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === STORE_SEAT_POSITIONS Seat ",iSeatID," at ",MPTVLocal.seatPositions[iSeatID].vLoc)
					CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === STORE_SEAT_POSITIONS Seat ",iSeatID," Locate0 at ",MPTVLocal.seatLocates0[iSeatID].vLoc)
					CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === STORE_SEAT_POSITIONS Seat ",iSeatID," Locate1 at ",MPTVLocal.seatLocates1[iSeatID].vLoc)
				ENDREPEAT
				SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_STORED_SEAT_POSITIONS)
			ENDIF
		ELSE
			INT iSeatID
			REPEAT GET_MP_TV_NUMBER_OF_SEATS(MPTVLocal, property) iSeatID
				
				CDEBUG1LN(DEBUG_MP_TV, "STORE_SEAT_POSITIONS: GET_YACHT_LOCAL_PLAYER_IS_ON = ", GET_YACHT_LOCAL_PLAYER_IS_ON())
			
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_SEAT_SCENARIO_0 + iSeatID, MPTVLocal.seatPositions[iSeatID], GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_SEAT_LOCATE0_0 + iSeatID, MPTVLocal.seatLocates0[iSeatID], GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
				GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_SEAT_LOCATE1_0 + iSeatID, MPTVLocal.seatLocates1[iSeatID], GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
				

				//MPTVLocal.seatPositions[iSeatID].vLoc.z += 1 //Move the locate back up
				CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === STORE_SEAT_POSITIONS Seat ",iSeatID," at ",MPTVLocal.seatPositions[iSeatID].vLoc)
				CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === STORE_SEAT_POSITIONS Seat ",iSeatID," Locate0 at ",MPTVLocal.seatLocates0[iSeatID].vLoc)
				CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === STORE_SEAT_POSITIONS Seat ",iSeatID," Locate1 at ",MPTVLocal.seatLocates1[iSeatID].vLoc)
			ENDREPEAT
			SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_STORED_SEAT_POSITIONS)
		ENDIF
	ENDIF
	
	IF IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
		INT iSeatID
		REPEAT GET_MP_TV_NUMBER_OF_SEATS(MPTVLocal, property) iSeatID
			GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_SEAT_SCENARIO_0 + iSeatID, MPTVLocal.seatPositions[iSeatID], GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex), TRUE)
			GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_SEAT_LOCATE0_0 + iSeatID, MPTVLocal.seatLocates0[iSeatID], GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex), TRUE)
			GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_MPTV_SEAT_LOCATE1_0 + iSeatID, MPTVLocal.seatLocates1[iSeatID], GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex), TRUE)
			
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === STORE_SEAT_POSITIONS Seat ",iSeatID," at ",MPTVLocal.seatPositions[iSeatID].vLoc)
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === STORE_SEAT_POSITIONS Seat ",iSeatID," Locate0 at ",MPTVLocal.seatLocates0[iSeatID].vLoc)
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === STORE_SEAT_POSITIONS Seat ",iSeatID," Locate1 at ",MPTVLocal.seatLocates1[iSeatID].vLoc)
		ENDREPEAT
		SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_STORED_SEAT_POSITIONS)
	ENDIF
	
	IF IS_PLAYER_IN_HANGAR(PLAYER_ID())
		CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === STORE_SEAT_POSITIONS for hangar")
		MPTVLocal.seatLocates0[0].vLoc = <<-1239.192383,-3012.513428,-43.862206>>
		MPTVLocal.seatLocates1[0].vLoc = <<-1239.985107,-3013.378418,-42.362206>> 
		MPTVLocal.seatLocates0[1].vLoc = <<-1240.552002,-3013.516113,-43.862206>>
		MPTVLocal.seatLocates1[1].vLoc = <<-1240.529419,-3014.712891,-42.362206>>
		MPTVLocal.seatLocates0[2].vLoc = <<-1240.098877,-3014.867920,-43.862206>>
		MPTVLocal.seatLocates1[2].vLoc = <<-1239.238159,-3014.850342,-42.362206>>
		MPTVLocal.seatLocates0[3].vLoc = <<-1238.371582,-3014.850342,-43.862206>>
		MPTVLocal.seatLocates1[3].vLoc = <<-1239.238159,-3014.850342,-42.362206>>
		MPTVLocal.seatLocates0[4].vLoc = <<-1238.371582,-3014.850342,-43.862206>>
		MPTVLocal.seatLocates1[4].vLoc = <<-1237.363525,-3014.867920,-42.362206>>
	ENDIF
	
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === STORE_SEAT_POSITIONS for Facility")
		MPTVLocal.seatLocates0[0].vLoc = <<366.250854,4843.824707,-59.999626>>
		MPTVLocal.seatLocates1[0].vLoc = <<366.631104,4845.000000,-58.542606>>
		MPTVLocal.seatLocates0[1].vLoc = <<365.314087,4844.061035,-59.999626>>
		MPTVLocal.seatLocates1[1].vLoc = <<365.703979,4845.326172,-58.542606>>
		MPTVLocal.seatLocates0[2].vLoc = <<364.452332,4844.329590,-59.999626>>
		MPTVLocal.seatLocates1[2].vLoc = <<364.790710,4845.594238,-58.542606>>
	ENDIF	
ENDPROC

FUNC BOOL IS_PROPERTY_USING_OBJECT_TV(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, INT iCurrentProperty,  MP_TV_CLIENT_DATA_STRUCT MPTVClient)
	
	IF (IS_PROPERTY_YACHT_APARTMENT(iCurrentProperty)
	OR IS_PROPERTY_STILT_APARTMENT(iCurrentProperty)
	OR IS_PROPERTY_CUSTOM_APARTMENT(iCurrentProperty))
	AND (NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE))
		RETURN TRUE
	ENDIF
	
	IF IS_PROPERTY_OFFICE(iCurrentProperty)			
		RETURN TRUE
	ENDIF
	
	IF IS_PROPERTY_CLUBHOUSE(iCurrentProperty)
		RETURN TRUE
	ENDIF
	
	IF IS_PROPERTY_OFFICE_GARAGE(iCurrentProperty)
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_HANGAR(PLAYER_ID())	
		RETURN TRUE
	ENDIF
	
	IF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
		RETURN TRUE
	ENDIF
	
	IF MPTVLocal.bInCasinoApartment
		RETURN TRUE
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	IF MPTVLocal.bInArcade
		RETURN TRUE
	ENDIF
	#ENDIF
	
	#IF FEATURE_FIXER
	IF MPTVLocal.bInFixerHQ
		RETURN TRUE
	ENDIF
	#ENDIF
RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Initialise client for MP tv
/// PARAMS:
///    MPTVServer - server broadcast data
///    MPTVClient - client broadcast data
///    MPTVLocal - local data struct
///    property - current property ID
/// RETURNS:
///    true when complete
FUNC BOOL CLIENT_INIT(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property)
	CDEBUG1LN(DEBUG_MP_TV, "CLIENT_INIT")
	IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_STORED_SEAT_POSITIONS)
		STORE_SEAT_POSITIONS(MPTVLocal,property)
	ENDIF
	IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_RENDER_TARGET_FOUND)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== MP_TV LOCAL === CLIENT_INIT: RETURNING TRUE: MPTV_LOCAL_BS_RENDER_TARGET_FOUND TRUE ")
		#ENDIF
		RETURN TRUE
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== MP_TV LOCAL === CLIENT_INIT: MPTV_LOCAL_BS_RENDER_TARGET_FOUND FALSE ")
		#ENDIF
		IF GET_MP_TV_OVERLAY_MODEL(MPTVLocal, property) = DUMMY_MODEL_FOR_SCRIPT
			CDEBUG1LN(DEBUG_MP_TV, "CLIENT_INIT : GET_MP_TV_OVERLAY_MODEL() = DUMMY_MODEL_FOR_SCRIPT")
			RETURN FALSE
		ENDIF
		REQUEST_MODEL(GET_MP_TV_OVERLAY_MODEL(MPTVLocal, property))
		IF HAS_MODEL_LOADED(GET_MP_TV_OVERLAY_MODEL(MPTVLocal, property))
			IF GET_MP_TV_SERVER_STAGE(MPTVServer, MPTVClient) > MP_TV_SERVER_STAGE_INIT
				MPTVLocal.eCurrentChannel = GET_MP_TV_SERVER_CURRENT_CHANNEL(MPTVServer, MPTVClient)
				SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_WATCH_CHANNEL)
				
				SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_UPDATE_BUTTONS)
				
				
				
				IF IS_PROPERTY_USING_OBJECT_TV(MPTVLocal, property.iIndex,MPTVClient)
				
					CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SETUP_RENDERTARGET_FROM_OBJECT ")
					IF SETUP_RENDERTARGET_FROM_OBJECT(MPTVClient, MPTVLocal, property)
				
						IF MPTVServer.eStage = MP_TV_SERVER_STAGE_OFF
							SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_OFF)
							#IF IS_DEBUG_BUILD
							//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === INIT SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_OFF)")
							#ENDIF
						ELIF MPTVServer.eStage = MP_TV_SERVER_STAGE_ON
							SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_ON)
							#IF IS_DEBUG_BUILD
							//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === INIT SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_ON)")
							#ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							CREATE_MP_TV_WIDGETS(MPTVLocal,MPTVServer)
						#ENDIF
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SETUP_RENDERTARGET_FROM_OBJECT: RETURNING TRUE ")
						RETURN TRUE
					ELSE
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === INIT : Failed to setup renderTarget for TV")
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SETUP_RENDERTARGET ")
					IF SETUP_RENDERTARGET(MPTVClient, MPTVLocal, property)
				
						IF MPTVServer.eStage = MP_TV_SERVER_STAGE_OFF
							SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_OFF)
							#IF IS_DEBUG_BUILD
							//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === INIT SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_OFF)")
							#ENDIF
						ELIF MPTVServer.eStage = MP_TV_SERVER_STAGE_ON
							SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_ON)
							#IF IS_DEBUG_BUILD
							//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === INIT SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_ON)")
							#ENDIF
						ENDIF
						
						#IF IS_DEBUG_BUILD
							CREATE_MP_TV_WIDGETS(MPTVLocal,MPTVServer)
						#ENDIF
						
						RETURN TRUE
					ELSE
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === INIT : Failed to setup renderTarget for TV")
					ENDIF
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === INIT : Server still in init stage, can't proceed")
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === INIT : TV MODEL NOT LOADED")
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Server processes the seat logic, controlling who sits where
/// PARAMS:
///    MPTVServer - server broadcast data
///    MPTVStaggerClient - client broadcast data to check this frame
///    MPTVLocal - local data struct
///    property - current property ID
PROC SERVER_MAINTAIN_SEATS(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVStaggerClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property)
	INT iNumberOfSeats = GET_MP_TV_NUMBER_OF_SEATS(MPTVLocal, property)
	
//	IF NOT IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_RELATIONSHIP_SECOND_SEAT)
//		IF MPTVStaggerClient.iSeat <> -1 
//			IF IS_BIT_SET(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED)
//			AND MPTVServer.sSeat[MPTVStaggerClient.iSeat].iSecondOccupyPlayer = MPTVLocal.iServerPlayerStagger
//				IF MPTVServer.sSeat[MPTVStaggerClient.iSeat].iCurrentOccupyPlayer <> -1
//					PLAYER_INDEX tempPlayer 
//					tempPlayer = INT_TO_PLAYERINDEX(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iSecondOccupyPlayer)
//					
//					NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iSecondOccupyPlayer))
//					#IF IS_DEBUG_BUILD
//						//CDEBUG2LN(DEBUG_MP_TV, " SERVER_MAINTAIN_SEATS: Player: ", GET_PLAYER_NAME(tempPlayer), " , in a relationship")
//					#ENDIF
//					SET_BIT(tempPlayer.iBitSet, MPTV_CLIENT_BS_RELATIONSHIP_SECOND_SEAT)
//					SET_BIT(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_RELATIONSHIP_SECOND_SEAT)
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
		
	IF MPTVStaggerClient.iSeat <> -1 
		IF IS_BIT_SET(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED)
		AND MPTVServer.sSeat[MPTVStaggerClient.iSeat].iCurrentOccupyPlayer <> -1
		AND INT_TO_PARTICIPANTINDEX(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iCurrentOccupyPlayer) != INVALID_PARTICIPANT_INDEX()
		AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iCurrentOccupyPlayer))
		AND NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iCurrentOccupyPlayer)) <> INVALID_PLAYER_INDEX()
		AND NOT IS_PLAYER_IN_PROPERTY(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iCurrentOccupyPlayer)),FALSE)

			CLEAR_BIT(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED)
		ENDIF
	ENDIF

	
	#IF FEATURE_BENCH_RELATIONSHIPS
	IF MPTVStaggerClient.iSeat <> -1 
		IF NOT IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_RELATIONSHIP_SEAT)
			IF IS_BIT_SET(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED)
			AND MPTVServer.sSeat[MPTVStaggerClient.iSeat].iCurrentOccupyPlayer = MPTVLocal.iServerPlayerStagger
				IF MPTVServer.sSeat[MPTVStaggerClient.iSeat].iSecondOccupyPlayer <> -1
				AND NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iSecondOccupyPlayer))
				AND INT_TO_PARTICIPANTINDEX(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iSecondOccupyPlayer) != INVALID_PARTICIPANT_INDEX()
					NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iSecondOccupyPlayer))
					#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, " SERVER_MAINTAIN_SEATS: Player: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger)), " , in a relationship")
					#ENDIF
					//SET_BIT(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_RELATIONSHIP_SEAT)// when this is set, if it's by the host, then all players in playerBD get this flag set to true as well
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	#ENDIF 
	
	IF IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)
	OR IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SECOND_SEAT)
		#IF IS_DEBUG_BUILD
//			CDEBUG2LN(DEBUG_MP_TV, " SERVER_MAINTAIN_SEATS: MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT: TRUE")
		#ENDIF
		//organise entering
		IF MPTVStaggerClient.iSeat <> -1 
			IF NOT IS_BIT_SET(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED)
				MPTVServer.sSeat[MPTVStaggerClient.iSeat].iCurrentOccupyPlayer = MPTVLocal.iServerPlayerStagger
				SET_BIT(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED)
				SET_BIT(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIER_ENTERING)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "SERVER_MAINTAIN_SEATS: Setting seat: ", MPTVStaggerClient.iSeat, " to be occupied by ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger)))
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV SERVER === MPTVServer.sSeat[", MPTVStaggerClient.iSeat, "].iCurrentOccupyPlayer = ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger)))
				//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV SERVER === SET_BIT(MPTVServer.sSeat[", MPTVStaggerClient.iSeat, "].iBitset, MPTV_SEAT_BS_OCCUPIED)")
				//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV SERVER === SET_BIT(MPTVServer.sSeat[", MPTVStaggerClient.iSeat, "].iBitset, MPTV_SEAT_BS_OCCUPIER_ENTERING)")
				#ENDIF
			#IF FEATURE_BENCH_RELATIONSHIPS
			ELIF IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SECOND_SEAT)
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, " SEAT ALREADY OCCUPIED")
				#ENDIF
				// If the seat is already occupied
					// Then set it as occupied and full
				IF IS_BIT_SET(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED)
				AND NOT IS_BIT_SET(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_FULL)
					MPTVServer.sSeat[MPTVStaggerClient.iSeat].iSecondOccupyPlayer = MPTVLocal.iServerPlayerStagger
					SET_BIT(MPTVStaggerClient.iBitSet, MPTV_CLIENT_BS_RELATIONSHIP_SECOND_SEAT)
					SET_BIT(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_FULL)
					SET_BIT(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIER_ENTERING)
					//SET_BIT(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIER_ENTERING)
					#IF IS_DEBUG_BUILD
					
					
					//CDEBUG2LN(DEBUG_MP_TV, " SEAT ALREADY OCCUPIED: GOING FULL")
					//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV SERVER === MPTVServer.sSeat[", MPTVStaggerClient.iSeat, "].iSecondOccupyPlayer = ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger)))
					//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV SERVER === MPTVServer.sSeat[", MPTVStaggerClient.iSeat, "].iSecondOccupyPlayer = ", MPTVLocal.iServerPlayerStagger)
					//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV SERVER === SET_BIT(MPTVServer.sSeat[", MPTVStaggerClient.iSeat, "].iBitset, MPTV_SEAT_BS_OCCUPIED)")
					//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV SERVER === SET_BIT(MPTVServer.sSeat[", MPTVStaggerClient.iSeat, "].iBitset, MPTV_SEAT_BS_FULL)")
					////CDEBUG2LN(DEBUG_MP_TV, " === MP_TV SERVER === SET_BIT(MPTVServer.sSeat[", MPTVStaggerClient.iSeat, "].iBitset, MPTV_SEAT_BS_OCCUPIER_ENTERING)")
					#ENDIF
				ENDIF
			#ENDIF
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
//			CDEBUG2LN(DEBUG_MP_TV, " SERVER_MAINTAIN_SEATS: MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT: FALSE")
		#ENDIF
		//handshake entering
		IF MPTVStaggerClient.iSeat <> -1
			IF IS_BIT_SET(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIER_ENTERING)
				IF IS_BIT_SET(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED)
				AND MPTVServer.sSeat[MPTVStaggerClient.iSeat].iCurrentOccupyPlayer = MPTVLocal.iServerPlayerStagger
				#IF FEATURE_BENCH_RELATIONSHIPS
				OR MPTVServer.sSeat[MPTVStaggerClient.iSeat].iSecondOccupyPlayer = MPTVLocal.iServerPlayerStagger
				#ENDIF
					IF IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
						CLEAR_BIT(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIER_ENTERING)
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, " SERVER_MAINTAIN_SEATS CLEAR_BIT(MPTVServer.sSeat[", MPTVStaggerClient.iSeat, "].iBitset, MPTV_SEAT_BS_OCCUPIER_ENTERING)")
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV SERVER === CLEAR_BIT(MPTVServer.sSeat[", MPTVStaggerClient.iSeat, "].iBitset, MPTV_SEAT_BS_OCCUPIER_ENTERING)")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		//handshake leaving
		IF MPTVStaggerClient.iSeat <> -1
			IF IS_BIT_SET(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIER_LEAVING)
				IF NOT IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
					MPTVServer.sSeat[MPTVStaggerClient.iSeat].iCurrentOccupyPlayer = -1
					CLEAR_BIT(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED)
					CLEAR_BIT(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIER_LEAVING)
					CLEAR_BIT(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIER_ENTERING)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV SERVER === MPTVServer.sSeat[", MPTVStaggerClient.iSeat, "].iCurrentOccupyPlayer = -1")
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV SERVER === CLEAR_BIT(MPTVServer.sSeat[", MPTVStaggerClient.iSeat, "].iBitset, MPTV_SEAT_BS_OCCUPIED)")
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV SERVER === CLEAR_BIT(MPTVServer.sSeat[", MPTVStaggerClient.iSeat, "].iBitset, MPTV_SEAT_BS_OCCUPIER_LEAVING)")
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV SERVER === CLEAR_BIT(MPTVServer.sSeat[", MPTVStaggerClient.iSeat, "].iBitset, MPTV_SEAT_BS_OCCUPIER_ENTERING)")
					#ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
					IF IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_LEAVING_SEAT)
						IF MPTVServer.sSeat[MPTVStaggerClient.iSeat].iCurrentOccupyPlayer = MPTVLocal.iServerPlayerStagger
							SET_BIT(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIER_LEAVING)
						ENDIF 
					#IF FEATURE_BENCH_RELATIONSHIPS
					ELIF IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_LEAVING_SECOND_SEAT)
						IF MPTVServer.sSeat[MPTVStaggerClient.iSeat].iSecondOccupyPlayer = MPTVLocal.iServerPlayerStagger
							SET_BIT(MPTVServer.sSeat[MPTVStaggerClient.iSeat].iBitset, MPTV_SEAT_BS_SECOND_OCCUPIER_LEAVING)
						ENDIF 
					#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
		
	MPTVLocal.iServerSeatStagger++
	IF MPTVLocal.iServerSeatStagger >= iNumberOfSeats
		MPTVLocal.iServerSeatStagger = 0
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if a mp tv seat exissts
/// PARAMS:
///    MPTVLocal - local data struct
///    property - current property ID
///    iThisSeat - seat ID to check
/// RETURNS:
///    true/false
FUNC BOOL TEST_MP_TV_SEAT(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property, INT iThisSeat)
	#IF IS_DEBUG_BUILD
	VECTOR vTempLoc0 = GET_MP_TV_SEAT_LOCATE0(property, iThisSeat,MPTVLocal)
	VECTOR vTempLoc1 = GET_MP_TV_SEAT_LOCATE1(property, iThisSeat,MPTVLocal)
	CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === TEST_MP_TV_SEAT: iThisSeat ", iThisSeat, ", GET_MP_TV_SEAT_LOCATE0 = ", vTempLoc0 , ", GET_MP_TV_SEAT_LOCATE1 = ", vTempLoc1, ", GET_MP_TV_SEAT_LOCATE_WIDTH = ", GET_MP_TV_SEAT_LOCATE_WIDTH(MPTVLocal, property, iThisSeat))
	#ENDIF
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),	GET_MP_TV_SEAT_LOCATE0(property, iThisSeat,MPTVLocal), 
													GET_MP_TV_SEAT_LOCATE1(property, iThisSeat,MPTVLocal), 
													GET_MP_TV_SEAT_LOCATE_WIDTH(MPTVLocal, property, iThisSeat))
		IF DOES_SCENARIO_EXIST_IN_AREA(GET_MP_TV_SEAT_SCENARIO_VECTOR(property, iThisSeat, MPTVLocal), 1.0, TRUE)
			IF MPTVLocal.iNearestSeat = -1
			OR MPTVLocal.iNearestSeat = MPTVLocal.iClientSeatStagger
			OR VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_MP_TV_SEAT_SCENARIO_VECTOR(property, iThisSeat,MPTVLocal)) < VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_MP_TV_SEAT_SCENARIO_VECTOR(property, MPTVLocal.iNearestSeat,MPTVLocal))
				PRINTLN("POD: TEST_MP_TV_SEAT: TRUE")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    local player updates which seat they are nearest to
/// PARAMS:
///    MPTVLocal - local data struct
///    property - current property ID
PROC UPDATE_NEAREST_SEAT(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property)
	CDEBUG1LN(DEBUG_MP_TV, "UPDATE_NEAREST_SEAT")
	INT iNumberOfSeats = GET_MP_TV_NUMBER_OF_SEATS(MPTVLocal, property)
	INT i
//	CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === UPDATE_NEAREST_SEAT, MPTVLocal.iClientSeatStagger = ", MPTVLocal.iClientSeatStagger)
//	PRINTLN("POD: CLIENT_MAINTAIN_GETTING_A_SEAT:  UPDATE_NEAREST_SEAT")
	
	BOOL bInstantlyCheckAllSeats = FALSE
	
	IF TEST_MP_TV_SEAT(MPTVLocal, property, MPTVLocal.iClientSeatStagger)
		IF MPTVLocal.iNearestSeat <> MPTVLocal.iClientSeatStagger
			MPTVLocal.iNearestSeat = MPTVLocal.iClientSeatStagger
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === MPTVLocal.iNearestSeat = ", MPTVLocal.iClientSeatStagger)
			//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV LOCAL === MPTVLocal.iNearestSeat = ", MPTVLocal.iClientSeatStagger)
			#ENDIF
		ENDIF
	ELSE
		IF MPTVLocal.iNearestSeat = MPTVLocal.iClientSeatStagger
			MPTVLocal.iNearestSeat = -1
			//bInstantlyCheckAllSeats = TRUE
			PRINTLN("POD: UPDATE_NEAREST_SEAT: MPTVLocal.iNearestSeat = MPTVLocal.iClientSeatStagger")
		ENDIF
	ENDIF
	
	IF bInstantlyCheckAllSeats
		REPEAT iNumberOfSeats i
			IF TEST_MP_TV_SEAT(MPTVLocal, property, i)
				IF MPTVLocal.iNearestSeat <> i
					MPTVLocal.iNearestSeat = i
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === MPTVLocal.iNearestSeat = ", i)
					PRINTLN("=== MP_TV LOCAL === MPTVLocal.iNearestSeat = ", i)
					#ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		MPTVLocal.iClientSeatStagger = 0
	ELSE
		MPTVLocal.iClientSeatStagger++
		//CDEBUG3LN(DEBUG_MP_TV, "UPDATE_NEAREST_SEAT: MPTVLocal.iClientSeatStagger = ", MPTVLocal.iClientSeatStagger)
		//CDEBUG3LN(DEBUG_MP_TV, "UPDATE_NEAREST_SEAT: iNumberOfSeats ", iNumberOfSeats)
		IF MPTVLocal.iClientSeatStagger >= iNumberOfSeats
			MPTVLocal.iClientSeatStagger = 0
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Tasks the local player to use the nearest MP tv seat scenario
/// PARAMS:
///    MPTVClient - client data struct
///    property - current property ID
/// RETURNS:
///    true when complete
FUNC BOOL SIT_PLAYER_PED_IN_SEAT(MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_PROPERTY_STRUCT &property, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, BOOL bForceWarp)
	bForceWarp = bForceWarp
	IF IS_PED_PLAYING_BASE_CLIP_IN_SCENARIO(PLAYER_PED_ID())
		
		#IF IS_DEBUG_BUILD
		//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV LOCAL === SIT_PLAYER_PED_IN_SEAT ", MPTVClient.iSeat, " = TRUE")
		//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SIT_PLAYER_PED_IN_SEAT ", MPTVClient.iSeat, " = TRUE")
		#ENDIF
		RETURN TRUE
		
	ELSE
		//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SIT_PLAYER_PED_IN_SEAT: IS_PED_PLAYING_BASE_CLIP_IN_SCENARIO = FALSE")
		VECTOR scenarioSeatVector = GET_MP_TV_SEAT_SCENARIO_VECTOR(property, MPTVClient.iSeat, MPTVLocal)
		scenarioSeatVector = scenarioSeatVector
		CWARNINGLN(DEBUG_MP_TV, "GET_MP_TV_SEAT_SCENARIO_VECTOR RETURNING: ", scenarioSeatVector)
		IF NOT bForceWarp
			IF NOT PED_HAS_USE_SCENARIO_TASK(PLAYER_PED_ID())

				IF DOES_SCENARIO_EXIST_IN_AREA(GET_MP_TV_SEAT_SCENARIO_VECTOR(property, MPTVClient.iSeat,MPTVLocal), 2.0, TRUE)
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SIT_PLAYER_PED_IN_SEAT: TASK_USE_NEAREST_SCENARIO_TO_COORD")
					IF IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
					OR IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
					OR IS_PROPERTY_STILT_APARTMENT(property.iIndex)
					OR IS_PROPERTY_OFFICE(property.iIndex)
					OR IS_PROPERTY_CLUBHOUSE(property.iIndex)
					OR IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
					// Fix for B*2908533
					OR IS_PROPERTY_HIGH_END_APARTMENT(property.iIndex)
					OR IS_PLAYER_IN_HANGAR(PLAYER_ID())
					OR IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
					OR MPTVLocal.bInCasinoApartment
					#IF FEATURE_CASINO_HEIST
					OR MPTVLocal.bInArcade
					#ENDIF
					#IF FEATURE_FIXER
					OR MPTVLocal.bInFixerHQ
					#ENDIF
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_UseGoToPointForScenarioNavigation, TRUE)
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SIT_PLAYER_PED_IN_SEAT: Forcing ped to go stright to coord")
					ENDIF
					TASK_USE_NEAREST_SCENARIO_TO_COORD(PLAYER_PED_ID(), GET_MP_TV_SEAT_SCENARIO_VECTOR(property, MPTVClient.iSeat,MPTVLocal), 2.0, 0)
				ELSE
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SIT_PLAYER_PED_IN_SEAT: DOES_SCENARIO_EXIST_IN_AREA = FALSE")
				ENDIF
			ELSE	
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SIT_PLAYER_PED_IN_SEAT: PED_HAS_USE_SCENARIO_TASK = FALSE")
			ENDIF
		ELSE
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SIT_PLAYER_PED_IN_SEAT: bForceWarp = TRUE")
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_STANDING_AT_SOFA(PLAYER_INDEX player)
	RETURN IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(player)].iDCTL89BitSet, ciDCTL89_BD_STANDING_AT_SOFA)
ENDFUNC

/// PURPOSE:
///    Clears the local player task of using nearest MP tv seat scenario
/// PARAMS:
///    MPTVLocal - local data struct
/// RETURNS:
///    true when complete
FUNC BOOL STAND_PLAYER_PED_UP(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property)
	IF NOT IS_PROPERTY_OFFICE(property.iIndex)
	AND NOT IS_PROPERTY_CLUBHOUSE(property.iIndex)
	AND NOT IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
	AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
		IF NOT PED_HAS_USE_SCENARIO_TASK(PLAYER_PED_ID())
			SET_PED_SHOULD_IGNORE_SCENARIO_EXIT_COLLISION_CHECKS(PLAYER_PED_ID(), FALSE)
			CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_STAND_UP_TASK_GIVEN)
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_STAND_UP_TASK_GIVEN)")
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === STAND_PLAYER_PED_UP = TRUE")
			#ENDIF
			RETURN TRUE
		ELSE
			//IF IS_PED_ACTIVE_IN_SCENARIO(PLAYER_PED_ID())
				IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_STAND_UP_TASK_GIVEN)
				
					SET_PED_SHOULD_IGNORE_SCENARIO_EXIT_COLLISION_CHECKS(PLAYER_PED_ID(), TRUE)
				
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_STAND_UP_TASK_GIVEN)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_STAND_UP_TASK_GIVEN)")
					#ENDIF
				ENDIF
			//ENDIF
		ENDIF
	ELIF NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	AND NOT MPTVLocal.bInCasinoApartment
	#IF FEATURE_CASINO_HEIST
	AND NOT MPTVLocal.bInArcade
	#ENDIF
	#IF FEATURE_FIXER
	AND NOT MPTVLocal.bInFixerHQ
	#ENDIF
		IF NOT IS_PLAYER_STANDING_AT_SOFA(PLAYER_ID())
			CDEBUG1LN(DEBUG_MP_TV, "STAND_PLAYER_PED_UP: g_OfficeArmChairState = OFFICE_SEAT_TV_EXIT_SEAT")
			g_OfficeArmChairState = OFFICE_SEAT_TV_EXIT_SEAT //JAC_TV_EXIT_SEAT
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

///// PURPOSE:
/////    Informs you wheather of not a player is the second person on a seat
///// PARAMS:
/////    Player_PED_ID - player to check against
///// RETURNS:
/////    true if player is in a secondseated
//FUNC BOOL IS_PLAYER_IN_RELATIONSHIP_SECOND_SEAT(INT iPlayer)
//	IF IS_BIT_SET(playerBD[iPlayer].MPTVClient.iBitset, MPTV_CLIENT_BS_RELATIONSHIP_SECOND_SEAT)
//		RETURN TRUE
//	ELSE
//		RETURN FALSE
//	ENDIF
//ENDFUNC
//
PROC ASSIGN_TV_SEAT_PLAYER_IS_SITTING_IN(MP_TV_CLIENT_DATA_STRUCT &MPTVClient, INT iSeatID)
	SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)
	MPTVClient.iSeat = iSeatID
ENDPROC

FUNC BOOL OCCUPY_TV_SEAT(INT iPlayerID, MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)//, MP_PROPERTY_STRUCT &property)

	// If ped is trying to occupy seat
	// This could be the remote/client player,
	IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)
		#IF IS_DEBUG_BUILD
		//PRINTLN( " CLIENT_MAINTAIN_GETTING_A_SEAT:  MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT = TRUE")
		#ENDIF
		IF MPTVClient.iSeat != -1
			IF IS_BIT_SET(MPTVServer.sSeat[MPTVClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED)
				#IF IS_DEBUG_BUILD
				//PRINTLN( " CLIENT_MAINTAIN_GETTING_A_SEAT: MPTVServer.sSeat[MPTVClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED = TRUE")
				#ENDIF
				IF MPTVServer.sSeat[MPTVClient.iSeat].iCurrentOccupyPlayer <> -1
					CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)
					#IF IS_DEBUG_BUILD
					//PRINTLN( " MPTVClient.iSeat: ", MPTVClient.iSeat )
					////CDEBUG2LN(DEBUG_MP_TV, " Client side [MPTVClient.iSeat].iSecondOccupyPlayer: ", MPTVServer.sSeat[MPTVClient.iSeat].iSecondOccupyPlayer )
					//CDEBUG2LN(DEBUG_MP_TV, " iPlayerID: ", iPlayerID )
					//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)")
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)")
					#ENDIF
					
					IF MPTVServer.sSeat[MPTVClient.iSeat].iCurrentOccupyPlayer = iPlayerID
						MPTVLocal.timeAbandonSeat = GET_NETWORK_TIME()
						SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
						SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)
						#IF IS_DEBUG_BUILD
						//PRINTLN( " === MP_TV CLIENT === SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)")
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)")
						//PRINTLN("=== MP_TV CLIENT === SEAT REQUEST SUCCESS, OCCUPYING MPTVClient.iSeat = ", MPTVClient.iSeat)
						#ENDIF
						RETURN TRUE
					ELSE
						MPTVClient.iSeat = -1
						#IF IS_DEBUG_BUILD
						//PRINTLN("POD: === MP_TV CLIENT === SEAT REQUEST FAIL, OCCUPIED BY SOMEONE ELSE MPTVClient.iSeat = -1 ")
						//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === SEAT REQUEST FAIL, OCCUPIED BY SOMEONE ELSE MPTVClient.iSeat = -1")
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === SEAT REQUEST FAIL, OCCUPIED BY SOMEONE ELSE MPTVClient.iSeat = -1")
						#ENDIF
						RETURN FALSE
					ENDIF
				ENDIF
			ELSE
				//PRINTLN("CLIENT_MAINTAIN_GETTING_A_SEAT:  IS_BIT_SET(MPTVServer.sSeat[MPTVClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED) = FALSE ")
			ENDIF
		ELSE
			//PRINTLN("CLIENT_MAINTAIN_GETTING_A_SEAT:  MPTVClient.iSeat = -1 ")
			//MPTVClient.iSeat = 0
		ENDIF 
	ELSE
		#IF IS_DEBUG_BUILD
		//PRINTLN( " CLIENT_MAINTAIN_GETTING_A_SEAT:  IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)  FALSE")
		#ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Client finds the nearest valid MP seat, waits for player to request seat then requests server giving seat permission before using scenario
/// PARAMS:
///    MPTVServer - server broadcast data
///    MPTVClient - client broadcast data
///    MPTVLocal - local data struct
///    property - current property ID
///    bForceWarp - force pop the player into the seat
/// RETURNS:
///    true when successfully seated
FUNC BOOL CLIENT_MAINTAIN_GETTING_A_SEAT(	MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, 
											MP_PROPERTY_STRUCT &property, BOOL bForceWarp = FALSE)
	INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
	BOOL bReadyForContext
	
	#IF FEATURE_BENCH_RELATIONSHIPS
	BOOL bReadyForContextJoin
	#ENDIF
//	CDEBUG1LN(DEBUG_MP_TV, "CLIENT_MAINTAIN_GETTING_A_SEAT")
//	IF bForceWarp
//		//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT: FORCING WARP")
//		DEBUG_PRINTCALLSTACK()
//	ENDIF
	
	// If Ped is occupying seat
	IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
				
		#IF IS_DEBUG_BUILD
//		CDEBUG3LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT:  MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT = TRUE")
		#ENDIF
		
		#IF FEATURE_BENCH_RELATIONSHIPS
		IF NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_RELATIONSHIP_SEAT)
		AND NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_RELATIONSHIP_SECOND_SEAT)
		#ENDIF
			
			
			IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_TV_SEAT_TEXT)
				//MP_MAGDEMO RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention)
				//MP_MAGDEMO PRINT_HELP("MPTV_HLP1")//You can watch your CCTV attached to your garage.
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_TV_SEAT_TEXT, TRUE)
			ENDIF
		
			IF SIT_PLAYER_PED_IN_SEAT(MPTVClient, property, MPTVLocal, bForceWarp)
				RETURN TRUE
			ELSE
				IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPTVLocal.timeAbandonSeat) >= MP_TV_ABANDON_SEAT_TIME
				OR bForceWarp
					IF DOES_SCENARIO_EXIST_IN_AREA(GET_MP_TV_SEAT_SCENARIO_VECTOR(property, MPTVClient.iSeat, MPTVLocal), 2.0, TRUE)
						#IF IS_DEBUG_BUILD
						VECTOR tempDebugOutput
						tempDebugOutput = GET_MP_TV_SEAT_SCENARIO_VECTOR(property, MPTVClient.iSeat, MPTVLocal)
						PRINTLN("POD: CLIENT_MAINTAIN_GETTING_A_SEAT: GET_MP_TV_SEAT_SCENARIO_VECTOR: MPTVClient.iSeat: ", MPTVClient.iSeat, ", vector: ", tempDebugOutput)
						#ENDIF
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === CLEAR_PED_TASKS")
						TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(PLAYER_PED_ID(), GET_MP_TV_SEAT_SCENARIO_VECTOR(property, MPTVClient.iSeat, MPTVLocal), 1, 0)
						#IF IS_DEBUG_BUILD
						VECTOR tempPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
						VECTOR tempPos2 = GET_MP_TV_SEAT_SCENARIO_VECTOR(property, MPTVClient.iSeat, MPTVLocal)
						PRINTLN("POD: TV: PLAYER POSITION: ", tempPos, ", GET_MP_TV_SEAT_SCENARIO_VECTOR: ", tempPos2, ", iSeat: ", MPTVClient.iSeat)
						PRINTLN("POD: === MP_TV CLIENT === bForceWarp TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP")
//						CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === bForceWarp TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP")
						#ENDIF
						g_bSpawnTVIsActive = TRUE
						IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPTVLocal.timeAbandonSeat) >= MP_TV_ABANDON_SEAT_TIME+MP_TV_ABANDON_SEAT_TIME
							SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_ABANDONING_SEAT)
							SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_LEAVING_SEAT)
							
							#IF IS_DEBUG_BUILD
							//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_ABANDONING_SEAT)")
							//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_LEAVING_SEAT)")
							//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_ABANDONING_SEAT)")
							//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_LEAVING_SEAT)")
							#ENDIF
						ENDIF
					ELSE 
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === NOT DOES_SCENARIO_EXIST_IN_AREA")
					ENDIF
//					#IF IS_DEBUG_BUILD
//					#IF SCRIPT_PROFILER_ACTIVE
//					ADD_SCRIPT_PROFILE_MARKER("DOES_SCENARIO_EXIST_IN_AREA")
//					#ENDIF
//					#ENDIF
				ELSE 
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === NOT GET_TIME_DIFFERENCE or bForceWarp")
				ENDIF
			ENDIF
		#IF FEATURE_BENCH_RELATIONSHIPS
		ELSE
			#IF IS_DEBUG_BUILD
				
				IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_RELATIONSHIP_SEAT)
					//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT +++++++++++++ First player in seat +++++++", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger)))
				ENDIF
				
				IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_RELATIONSHIP_SECOND_SEAT)
					//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT +++++++++++++ Second player in seat +++++++", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger)))
					RETURN TRUE
				ENDIF
				
			#ENDIF
		ENDIF
		#ENDIF
		
	ELSE
		#IF IS_DEBUG_BUILD
		//CDEBUG3LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT:  MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT = FALSE")
		#ENDIF
		// If ped is trying to occupy seat
		// This could be the remote/client player,
		IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT:  MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT = TRUE")
			#ENDIF
			IF IS_BIT_SET(MPTVServer.sSeat[MPTVClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED)
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT: MPTVServer.sSeat[MPTVClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED = TRUE")
				#ENDIF
				IF MPTVServer.sSeat[MPTVClient.iSeat].iCurrentOccupyPlayer <> -1
					CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, " MPTVClient.iSeat: ", MPTVClient.iSeat )
					////CDEBUG2LN(DEBUG_MP_TV, " Client side [MPTVClient.iSeat].iSecondOccupyPlayer: ", MPTVServer.sSeat[MPTVClient.iSeat].iSecondOccupyPlayer )
					//CDEBUG2LN(DEBUG_MP_TV, " iPlayerID: ", iPlayerID )
					//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)")
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)")
					#ENDIF
					
					IF MPTVServer.sSeat[MPTVClient.iSeat].iCurrentOccupyPlayer = iPlayerID
						MPTVLocal.timeAbandonSeat = GET_NETWORK_TIME()
						SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
						SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)")
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)")
						CPRINTLN(DEBUG_ACT_CELEB,"=== MP_TV CLIENT === SEAT REQUEST SUCCESS, OCCUPYING MPTVClient.iSeat = ", MPTVClient.iSeat)
						#ENDIF
					ELSE
						MPTVClient.iSeat = -1
						#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_ACT_CELEB,"POD: === MP_TV CLIENT === SEAT REQUEST FAIL, OCCUPIED BY SOMEONE ELSE MPTVClient.iSeat = -1 ")
						//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === SEAT REQUEST FAIL, OCCUPIED BY SOMEONE ELSE MPTVClient.iSeat = -1")
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === SEAT REQUEST FAIL, OCCUPIED BY SOMEONE ELSE MPTVClient.iSeat = -1")
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE

			bReadyForContext = FALSE
			
			IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
			AND NOT IS_BIT_SET(MPGlobals.KillYourselfData.iBitSet, biKY_AnimStarted)
			AND NOT IS_PAUSE_MENU_ACTIVE_EX()
			AND NOT g_bPlayerInActivityArea
//				#IF IS_DEBUG_BUILD
				//CDEBUG3LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT: IS_NET_PLAYER_OK TRUE")
//				#ENDIF
				CDEBUG1LN(DEBUG_MP_TV, "UPDATE_NEAREST_SEAT")
				UPDATE_NEAREST_SEAT(MPTVLocal, property)
				
//				#IF IS_DEBUG_BUILD
//				#IF SCRIPT_PROFILER_ACTIVE
//				ADD_SCRIPT_PROFILE_MARKER("UPDATE_NEAREST_SEAT")
//				#ENDIF
//				#ENDIF
			
				IF MPTVLocal.iNearestSeat <> -1
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT: Nearest seat is valid")
					#ENDIF
					IF NOT IS_BIT_SET(MPTVServer.sSeat[MPTVLocal.iNearestSeat].iBitset, MPTV_SEAT_BS_OCCUPIED)
						#IF IS_DEBUG_BUILD
//						CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT: Nearest seat is unoccupied")
						#ENDIF
						bReadyForContext = TRUE
						g_bInTVLocate = TRUE
					#IF FEATURE_BENCH_RELATIONSHIPS
					ELSE	
						IF NOT IS_BIT_SET(MPTVServer.sSeat[MPTVLocal.iNearestSeat].iBitset, MPTV_SEAT_BS_FULL)
							#IF IS_DEBUG_BUILD
							//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === bReadyForContextJoin = TRUE")
							#ENDIF
							#IF IS_DEBUG_BUILD
//							CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === bReadyForContextJoin = TRUE")
							//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === SET_BIT: : player name: ",  GET_PLAYER_NAME(PLAYER_ID()))
							//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === SET_BIT: : iStagger: ",  GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger)))
							//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === SET_BIT: : iStagger: ",  MPTVLocal.iServerPlayerStagger)
							#ENDIF
							// POD: Found the nearest occupied seat
							bReadyForContextJoin = TRUE
							
						ELSE
//						 CDEBUG2LN(DEBUG_MP_TV, "CLIENT_MAINTAIN_GETTING_A_SEAT: Seat full")
						ENDIF
					#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
//						CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT: Nearest seat is already occupied")
						#ENDIF
					ENDIF
				ELSE	
					g_bInTVLocate = FALSE
					#IF IS_DEBUG_BUILD
//					CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT: Nearest seat invalid")
					#ENDIF
				ENDIF
			ENDIF
			
			IF bReadyForContext
				#IF IS_DEBUG_BUILD
				////CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT: bReadyForContext is true")
				#ENDIF
				IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_APPROACHED_MP_TV_SEAT)
					#IF IS_DEBUG_BUILD
//					CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_APPROACHED_MP_TV_SEAT)")
					#ENDIF
					SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_APPROACHED_MP_TV_SEAT)
				ENDIF
			
				IF MPTVLocal.iContextButtonIntention = NEW_CONTEXT_INTENTION
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT: registering context intention")
					#ENDIF
					//MP_MAGDEMO IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF IS_PLAYER_IN_OFFICE_PROPERTY(PLAYER_ID())
						REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_WALKOFF")
					ELSE
						REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_WALK")
					ENDIF
					//MP_MAGDEMO ENDIF
				ELSE
					IF HAS_CONTEXT_BUTTON_TRIGGERED(MPTVLocal.iContextButtonIntention)
						RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention)
						SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)
						MPTVClient.iSeat = MPTVLocal.iNearestSeat
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)")
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iSeat = ", MPTVLocal.iNearestSeat)
						#ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_APPROACHED_MP_TV_SEAT)
					#IF IS_DEBUG_BUILD
//					CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_APPROACHED_MP_TV_SEAT)")
					#ENDIF
					CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_APPROACHED_MP_TV_SEAT)
				ENDIF
				RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention)
			ENDIF
			
			
			
			#IF FEATURE_BENCH_RELATIONSHIPS
			IF bReadyForContextJoin
				IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_APPROACHED_MP_TV_SEAT) // as long as no one else is approaching a seat
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_APPROACHED_MP_TV_SEAT)")
					#ENDIF
					SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_APPROACHED_MP_TV_SEAT) 
				ENDIF
			
				IF MPTVLocal.iContextButtonIntentionRel = NEW_CONTEXT_INTENTION
					//MP_MAGDEMO IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//						REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "POD_MPREL_JOIN")
						REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntentionRel, CP_MEDIUM_PRIORITY, "Join Player")
					//MP_MAGDEMO ENDIF
				ELSE
					IF HAS_CONTEXT_BUTTON_TRIGGERED(MPTVLocal.iContextButtonIntentionRel)
					
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === MPTVClient.iSeat = ", MPTVLocal.iNearestSeat)
						//CDEBUG2LN(DEBUG_MP_TV, " Join triggered")
//						//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === SET_BIT: MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SECOND_SEAT: Second Seat: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX()))
						//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === SET_BIT: : player name: ",  GET_PLAYER_NAME(PLAYER_ID()))
						//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === SET_BIT: MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SECOND_SEAT: iStagger: ",  GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger)))
						//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === SET_BIT: MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SECOND_SEAT: iStagger: ",  MPTVLocal.iServerPlayerStagger)
						#ENDIF
						
						RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntentionRel)
						//SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)
						SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SECOND_SEAT)
						
						MPTVClient.iSeat = MPTVLocal.iNearestSeat
					ENDIF
				ENDIF
			ELSE
				IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_APPROACHED_MP_TV_SEAT)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, " === MP_TV CLIENT === CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_APPROACHED_MP_TV_SEAT)")
					#ENDIF
					CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_APPROACHED_MP_TV_SEAT)
				ENDIF
				RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntentionRel)
			ENDIF
			#ENDIF
			
			
			
		ENDIF
	ENDIF
				
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_SEAT_ANIM_FINISHED()
	
	IF g_bOverrideSeatAnimFinished
		PRINTLN("HAS_SEAT_ANIM_FINISHED() - g_bOverrideSeatAnimFinished TRUE - RETURN TRUE")
		RETURN TRUE
	ENDIF	
		
	IF IS_PLAYER_FEMALE()
		// Office anims.
		IF HAS_ENTITY_ANIM_FINISHED(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_A@BASE@", "ENTER")
		OR HAS_ENTITY_ANIM_FINISHED(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_B@BASE@", "ENTER")
		OR HAS_ENTITY_ANIM_FINISHED(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_C@BASE@", "ENTER")
		OR HAS_ENTITY_ANIM_FINISHED(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_D@BASE@", "ENTER")
		
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_A@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_A@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_A@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_A@BASE@", "BASE")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_A@BASE@", "A_TO_B")
		
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_B@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_B@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_B@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_B@BASE@", "BASE")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_B@BASE@", "B_TO_C")
		
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_C@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_C@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_C@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_C@BASE@", "BASE")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_C@BASE@", "C_TO_D")
		
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_D@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_D@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_D@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_D@BASE@", "BASE")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@FEMALE@VAR_D@BASE@", "D_TO_A")
		
			RETURN TRUE
		ENDIF
		
		// Clubhouse anims.
		IF HAS_ENTITY_ANIM_FINISHED(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_A@BASE@", "ENTER")
		OR HAS_ENTITY_ANIM_FINISHED(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@", "ENTER")
		OR HAS_ENTITY_ANIM_FINISHED(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_C@BASE@", "ENTER")
		
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_A@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_A@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_A@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_A@BASE@", "BASE")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_A@BASE@", "A_TO_B")
		
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@", "BASE")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_B@BASE@", "B_TO_C")
		
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_C@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_C@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_C@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_C@BASE@", "BASE")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@FEMALE@VAR_C@BASE@", "C_TO_A")
		
			RETURN TRUE
		ENDIF
	ELSE
		IF HAS_ENTITY_ANIM_FINISHED(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_A@BASE@", "ENTER")
		OR HAS_ENTITY_ANIM_FINISHED(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_B@BASE@", "ENTER")
		OR HAS_ENTITY_ANIM_FINISHED(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_C@BASE@", "ENTER")
		OR HAS_ENTITY_ANIM_FINISHED(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_D@BASE@", "ENTER")
		OR HAS_ENTITY_ANIM_FINISHED(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_E@BASE@", "ENTER")
		
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_A@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_A@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_A@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_A@BASE@", "BASE")
		
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_B@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_B@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_B@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_B@BASE@", "BASE")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_B@BASE@", "B_TO_C")
		
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_C@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_C@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_C@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_C@BASE@", "BASE")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_C@BASE@", "C_TO_D")
		
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_D@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_D@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_D@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_D@BASE@", "BASE")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_D@BASE@", "D_TO_E")
		
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_E@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_E@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_E@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_E@BASE@", "BASE")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@OFFICE@SEATING@MALE@VAR_E@BASE@", "E_TO_A")
		
			RETURN TRUE
		ENDIF
		
		// Clubhouse anims.
		IF HAS_ENTITY_ANIM_FINISHED(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_A@BASE@", "ENTER")
		OR HAS_ENTITY_ANIM_FINISHED(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@", "ENTER")
		OR HAS_ENTITY_ANIM_FINISHED(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_C@BASE@", "ENTER")
		
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_A@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_A@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_A@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_A@BASE@", "BASE")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_A@BASE@", "A_TO_B")
		
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@", "BASE")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_B@BASE@", "B_TO_C")
		
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_C@BASE@", "IDLE_A")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_C@BASE@", "IDLE_B")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_C@BASE@", "IDLE_C")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_C@BASE@", "BASE")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "ANIM@AMB@CLUBHOUSE@SEATING@MALE@VAR_C@BASE@", "C_TO_A")
		
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_LEAVE_OFFICE_COUCH()
	IF IS_PLAYER_IN_OFFICE_PROPERTY(PLAYER_ID())
		IF NOT HAS_SEAT_ANIM_FINISHED()
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

//FUNC BOOL START_RELATIONSHIP_ACTIVITY()
//ENDFUNC

/// PURPOSE:
///    Client maintains waiting for a player to request leaving a seat, and lets server know that they no longer are in seat
/// PARAMS:
///    MPTVServer - server broadcast data
///    MPTVClient - client broadcast data
///    MPTVLocal - local data struct
/// RETURNS:
///    true when player no longer in seat
FUNC BOOL CLIENT_MAINTAIN_LEAVING_A_SEAT(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property)
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTVDebug")
		PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT")
	ENDIF
	#ENDIF
	
	INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
	
	
	IF MPTVClient.iSeat = -1
		#IF IS_DEBUG_BUILD
		CWARNINGLN(DEBUG_SPECTATOR, "=== MP_TV CLIENT === MPTVClient.iSeat = -1)")
		#ENDIF
		RETURN TRUE
	ENDIF
	
		
	IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTVDebug")
			PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT - MPTV_CLIENT_BS_OCCUPYING_SEAT")
		ENDIF
		#ENDIF
		
		IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_LEAVING_SEAT)
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTVDebug")
				PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT - MPTV_CLIENT_BS_LEAVING_SEAT")
			ENDIF
			#ENDIF
			
			IF IS_BIT_SET(MPTVServer.sSeat[MPTVClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIER_LEAVING)
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTVDebug")
					PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT - MPTV_SEAT_BS_OCCUPIER_LEAVING")
				ENDIF
				#ENDIF
				
				IF STAND_PLAYER_PED_UP(MPTVLocal, property)
					CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)")
					#ENDIF
				ENDIF
			ENDIF
		#IF FEATURE_BENCH_RELATIONSHIPS
		ELIF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_LEAVING_SECOND_SEAT)
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTVDebug")
				PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT - MPTV_CLIENT_BS_LEAVING_SECOND_SEAT")
			ENDIF
			#ENDIF
			
			IF IS_BIT_SET(MPTVServer.sSeat[MPTVClient.iSeat].iBitset, MPTV_SEAT_BS_SECOND_OCCUPIER_LEAVING)
				// Make Player stand up
				// check if player is finished standing up
					// clear occupying seat flag
					
					
//				IF STAND_PLAYER_PED_UP(MPTVLocal)
//					CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
//					#IF IS_DEBUG_BUILD
//					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)")
//					#ENDIF
//				ENDIF
			ENDIF
		#ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTVDebug")
				PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT - Not leaving seat")
			ENDIF
			#ENDIF
			
			IF NOT IS_PAUSE_MENU_ACTIVE()
			AND NOT IS_PHONE_ONSCREEN()
			AND NOT IS_WARNING_MESSAGE_ACTIVE()
			AND NOT IS_BROWSER_OPEN()
			AND NOT IS_INTERACTION_MENU_OPEN()
			AND CAN_PLAYER_LEAVE_OFFICE_COUCH()
				IF (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE)) AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE))
				AND NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
				OR IS_PLAYER_IN_CORONA()
					#IF IS_DEBUG_BUILD
					IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTVDebug")
						PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT - Leaving seat")
					ENDIF
					#ENDIF
					
					RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention)
					IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_RELATIONSHIP_SECOND_SEAT)
						#IF IS_DEBUG_BUILD
							//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_LEAVING_A_SEAT: SECOND_SEAT: LEAVING")
						#ENDIF
						SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_LEAVING_SECOND_SEAT)
					ELSE
						#IF IS_DEBUG_BUILD
							//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_LEAVING_A_SEAT: FIRST_SEAT: LEAVING")
						#ENDIF
						SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_LEAVING_SEAT)
					ENDIF
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_LEAVING_SEAT)")
					#ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTVDebug")
					IF IS_PAUSE_MENU_ACTIVE()
						PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT - IS_PAUSE_MENU_ACTIVE")
					ENDIF
					
					IF IS_PHONE_ONSCREEN()
						PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT - IS_PHONE_ONSCREEN")
					ENDIF
					
					IF IS_WARNING_MESSAGE_ACTIVE()
						PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT - IS_WARNING_MESSAGE_ACTIVE")
					ENDIF
					
					IF IS_BROWSER_OPEN()
						PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT - IS_BROWSER_OPEN")
					ENDIF
					
					IF IS_INTERACTION_MENU_OPEN()
						PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT - IS_INTERACTION_MENU_OPEN")
					ENDIF
					
					IF NOT CAN_PLAYER_LEAVE_OFFICE_COUCH()
						PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT - CAN_PLAYER_LEAVE_OFFICE_COUCH")
					ENDIF
				ENDIF
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTVDebug")
			PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT - Not occupying seat")
		ENDIF
		#ENDIF
		
		IF MPTVServer.sSeat[MPTVClient.iSeat].iCurrentOccupyPlayer <> iPlayerID
		OR NOT IS_BIT_SET(MPTVServer.sSeat[MPTVClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED)
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTVDebug")
				PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT - Returning TRUE")
			ENDIF
			#ENDIF
			
			MPTVClient.iSeat = -1
			CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_LEAVING_SEAT)
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === PLAYER LEFT SEAT MPTVClient.iSeat = -1")
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_LEAVING_SEAT)")
			#ENDIF
			RETURN TRUE
		ELSE
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTVDebug")
				IF MPTVServer.sSeat[MPTVClient.iSeat].iCurrentOccupyPlayer = iPlayerID
					PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT - iCurrentOccupyPlayer")
				ENDIF
				
				IF IS_BIT_SET(MPTVServer.sSeat[MPTVClient.iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED)
					PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_LEAVING_A_SEAT - MPTV_SEAT_BS_OCCUPIED")
				ENDIF
			ENDIF
			#ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Requests all assets needed for the minigame.
FUNC BOOL LINE_MINIGAME_REQUEST_ALL_ASSETS()
	CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_REQUEST_ALL_ASSETS - Requesting assets for minigame.")
	IF REQUEST_SCRIPT_AUDIO_BANK("DLC_EXEC1/OFFICE_BOARDROOM")
		REQUEST_STREAMED_TEXTURE_DICT("LineArcadeMinigame")
		REQUEST_ADDITIONAL_TEXT("DCTL", MINIGAME_TEXT_SLOT)
		SET_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_ASSETS_REQUESTED)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Releases all assets previously loaded for the minigame.
PROC LINE_MINIGAME_CLEANUP_ALL_ASSETS()
	CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_CLEANUP_ALL_ASSETS.")
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("LineArcadeMinigame")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_EXEC1/OFFICE_BOARDROOM")
	CLEAR_ADDITIONAL_TEXT(MINIGAME_TEXT_SLOT, FALSE)
	CLEAR_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_ASSETS_REQUESTED)
	CLEAR_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_DO_ASSETS_REQUEST)
ENDPROC
PROC LINE_MINIGAME_CLEANUP_ALL_ASSETS_IF_NEEDED()
	IF IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_ASSETS_REQUESTED)
		CDEBUG2LN(DEBUG_MINIGAME, "[LINE_MINIGAME] LINE_MINIGAME_CLEANUP_ALL_ASSETS_IF_NEEDED.")
		LINE_MINIGAME_CLEANUP_ALL_ASSETS()
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the BD data to say I can play the game
/// PARAMS:
///    bState - the state of the bool
PROC DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(BOOL bState, BOOL bClearLaunchFlags = FALSE)
	IF bState
		IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_AVALIABLE_TO_PLAY)
			PRINTLN("[DONT89] - DONT89_SET_READY_TO_PLAY_GAME_BD_STATE - bState = TRUE")
			DEBUG_PRINTCALLSTACK()
			SET_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_DO_ASSETS_REQUEST)
		ENDIF
		SET_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_AVALIABLE_TO_PLAY)
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_AVALIABLE_TO_PLAY)
			PRINTLN("[DONT89] - DONT89_SET_READY_TO_PLAY_GAME_BD_STATE - bState = FALSE")
			DEBUG_PRINTCALLSTACK()
		ENDIF
		#ENDIF
		IF bClearLaunchFlags
			IF IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_ON_SOFA)
				PRINTLN("[DONT89] - NOT ciDONT_CROSS_89_ON_SOFA")
				CLEAR_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_ON_SOFA)
			ENDIF
			
			IF IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_AVALIABLE)
				SET_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_REFRESH_CONTROLS)
				CLEAR_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_AVALIABLE)
				PRINTLN("[DONT89] - NOT ciDONT_CROSS_89_AVALIABLE")
			ENDIF
		ENDIF
		CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_AVALIABLE_TO_PLAY)
	ENDIF
ENDPROC
PROC DONT89_SETIS_RUNNING_GAME_BD_STATE(BOOL bState)
	IF bState
		IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_IS_RUNNING_GAME)
			PRINTLN("[DONT89] - DONT89_SETIS_RUNNING_GAME_BD_STATE - bState = TRUE")
			DEBUG_PRINTCALLSTACK()
			LINE_MINIGAME_REQUEST_ALL_ASSETS()
		ENDIF
		SET_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_IS_RUNNING_GAME)
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_IS_RUNNING_GAME)
			PRINTLN("[DONT89] - DONT89_SETIS_RUNNING_GAME_BD_STATE - bState = FALSE")
			DEBUG_PRINTCALLSTACK()
		ENDIF
		#ENDIF
		CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_IS_RUNNING_GAME)
	ENDIF
ENDPROC

PROC DONT89_SETIS_STANDING_AT_SOFA_BD_STATE(BOOL bState)
	IF bState
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_STANDING_AT_SOFA)
			PRINTLN("[DONT89] - DONT89_SETIS_STANDING_AT_SOFA_BD_STATE - bState = TRUE")
			DEBUG_PRINTCALLSTACK()
		ENDIF
		#ENDIF
		SET_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_STANDING_AT_SOFA)
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_STANDING_AT_SOFA)
			PRINTLN("[DONT89] - DONT89_SETIS_STANDING_AT_SOFA_BD_STATE - bState = FALSE")
			DEBUG_PRINTCALLSTACK()
		ENDIF
		#ENDIF
		CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_STANDING_AT_SOFA)
	ENDIF
ENDPROC

PROC DONT89_SET_PLAYER_IS_ON_GAME_OVER_SCREEN(BOOL bState)
	IF bState
		SET_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_ON_GAME_OVER_SCREEN)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_ON_GAME_OVER_SCREEN)
	ENDIF
ENDPROC

FUNC BOOL DONT89_IS_REMOTE_PLAYER_ON_GAME_OVER_SCREEN(PLAYER_INDEX player)
	RETURN IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(player)].iDCTL89BitSet, ciDCTL89_BD_ON_GAME_OVER_SCREEN)
ENDFUNC


FUNC BOOL DONT89_IS_REMOTE_PLAYER_RUNNING_GAME(PLAYER_INDEX player)
	RETURN IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(player)].iDCTL89BitSet, ciDCTL89_BD_IS_RUNNING_GAME)
ENDFUNC

FUNC BOOL IS_REMOTE_PLAYER_RUNNING_DONT89_IN_OFFICE(BOOL bDontIncludeGameOver = FALSE)
	INT i
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
			PLAYER_INDEX tempPlayer
			tempPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i))
			IF DONT89_IS_REMOTE_PLAYER_RUNNING_GAME(tempPlayer) 
				IF bDontIncludeGameOver
					IF NOT DONT89_IS_REMOTE_PLAYER_ON_GAME_OVER_SCREEN(tempPlayer) 
						RETURN TRUE
					ENDIF
				ELSE
					RETURN TRUE 
				ENDIF
			ENDIF	
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

//Set that all players in my apartment should launch the script
PROC DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS(BOOL bState)
	IF bState
		#IF IS_DEBUG_BUILD
		IF NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_DO_REMOTE_LAUNCH)
			PRINTLN("[DONT89] - DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS - bState = TRUE")
			DEBUG_PRINTCALLSTACK()
		ENDIF
		#ENDIF
		SET_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_DO_REMOTE_LAUNCH)
	ELSE
		#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_DO_REMOTE_LAUNCH)
			PRINTLN("[DONT89] - DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS - bState = FALSE")
			DEBUG_PRINTCALLSTACK()
		ENDIF
		#ENDIF
//		IF IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_RUNNING)
//			PRINTLN("[DONT89] - NOT ciDONT_CROSS_89_RUNNING")
//			CLEAR_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_RUNNING)
//		ENDIF
//		IF IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_LAUNCHED)
//			PRINTLN("[DONT89] - NOT ciDONT_CROSS_89_LAUNCHED")
//			CLEAR_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_LAUNCHED)
//		ENDIF
		CLEAR_BIT(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_DO_REMOTE_LAUNCH)
	ENDIF
ENDPROC

//Set the flags that we need to launch the script
PROC DONT89_SET_LAUNCH_FLAGS(BOOL bSetRemoteLaunch = TRUE)
	LINE_MINIGAME_REQUEST_ALL_ASSETS()
	SET_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_RUNNING)
	CLEAR_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_LAUNCHED)
	IF bSetRemoteLaunch
		DONT89_SET_LAUNCH_FOR_REMOTE_PLAYERS(TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Counts all players that are in the office that are avaliable to play dont cross the line
/// RETURNS:
///    true when the number is more than 1
FUNC BOOL DONT89_ENOUGH_PLAYERS_IN_OFFICE_FOR_SELECTION()
	INT iPlayer
	PLAYER_INDEX aPlayerID
	INT iNumberOfOtherPlayers = 0
	BOOL bDoLaunch
	BOOL bCountedStanding
	CLEAR_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_DONT_GIVE_LAUNCH_RPR)
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		aPlayerID = INT_TO_PLAYERINDEX(iPlayer)
		IF aPlayerID != INVALID_PLAYER_INDEX()
		AND PLAYER_ID() != aPlayerID
		AND IS_NET_PLAYER_OK(aPlayerID)
		AND IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(aPlayerID)].iDCTL89BitSet, ciDCTL89_BD_AVALIABLE_TO_PLAY)
		AND ((IS_PLAYER_IN_OFFICE_PROPERTY(PLAYER_ID()) AND (GlobalplayerBD_FM[NATIVE_TO_INT(aPlayerID)].propertyDetails.iVisibilityID = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID))
		OR ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(aPlayerID, PLAYER_ID(), TRUE))
			IF NOT bCountedStanding
			OR NOT IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(aPlayerID)].iDCTL89BitSet, ciDCTL89_BD_STANDING_AT_SOFA)
				PRINTLN("[DONT89] - bCountedStanding = TRUE")
				iNumberOfOtherPlayers++
				bCountedStanding = TRUE
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[DONT89] - IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(aPlayerID)].iDCTL89BitSet, ciDCTL89_BD_STANDING_AT_SOFA) Not counting ", GET_PLAYER_NAME(aPlayerID))
				#ENDIF
			ENDIF
			IF NOT HAS_NET_TIMER_STARTED(g_sDontCrossTheLine89LaunchVars.stCoolDown)
			OR HAS_NET_TIMER_EXPIRED(g_sDontCrossTheLine89LaunchVars.stCoolDown, 500)
				IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(aPlayerID)].iDCTL89BitSet, ciDCTL89_BD_DO_REMOTE_LAUNCH)
					PRINTLN("[DONT89] - DONT89_ENOUGH_PLAYERS_IN_OFFICE_FOR_SELECTION - Launching because ", GET_PLAYER_NAME(aPlayerID) ," wants us to")
					bDoLaunch = TRUE
				ENDIF
			ENDIF
			IF DONT89_IS_REMOTE_PLAYER_RUNNING_GAME(aPlayerID)
				SET_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_DONT_GIVE_LAUNCH_RPR)
				PRINTLN("[DONT89] - DONT89_ENOUGH_PLAYERS_IN_OFFICE_FOR_SELECTION - SET_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_DONT_GIVE_LAUNCH_RPR) because ", GET_PLAYER_NAME(aPlayerID) ," is running game")
			ENDIF
		ENDIF
	ENDREPEAT
	#IF IS_DEBUG_BUILD
	IF g_sDontCrossTheLine89LaunchVars.iNumberOfOtherPlayers != iNumberOfOtherPlayers
		PRINTLN("[DONT89] -  DONT89_ENOUGH_PLAYERS_IN_OFFICE_FOR_SELECTION - g_sDontCrossTheLine89LaunchVars.iNumberOfOtherPlayers = ",  iNumberOfOtherPlayers)
	ENDIF
	#ENDIF
	
	IF bDoLaunch
		DONT89_SET_LAUNCH_FLAGS()
	ENDIF
	
	g_sDontCrossTheLine89LaunchVars.iNumberOfOtherPlayers = iNumberOfOtherPlayers
	RETURN g_sDontCrossTheLine89LaunchVars.iNumberOfOtherPlayers > 0
ENDFUNC

//Clean the flags
PROC DONT89_CLEAN_UP_LAUNCH_ARGS(BOOL bStartCoolDown = FALSE)
	PRINTLN("[DONT89] - DONT89_CLEAN_UP_LAUNCH_ARGS")
	g_sDontCrossTheLine89LaunchVars.iNumberOfOtherPlayers = 0
	g_sDontCrossTheLine89LaunchVars.iBitSet = 0
	IF NATIVE_TO_INT(PLAYER_ID()) != -1
		GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet = 0
	ENDIF
	IF bStartCoolDown
		START_NET_TIMER(g_sDontCrossTheLine89LaunchVars.stCoolDown)
	ELSE
		RESET_NET_TIMER(g_sDontCrossTheLine89LaunchVars.stCoolDown)
	ENDIF
ENDPROC

//Launch the script
FUNC BOOL DONT89_LAUNCH_CONTROLLER_SCRIPT()
	PRINTLN("[DONT89] - DONT89_LAUNCH_CONTROLLER_SCRIPT")
	MP_MISSION_DATA missionData
	missionData.mdID.idMission = eAM_DONT_CROSS_THE_LINE
	missionData.iInstanceId = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID
	
	IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
		missionData.iInstanceId = GET_SIMPLE_INTERIOR_INT_SCRIPT_INSTANCE() + ENUM_TO_INT(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))
	ENDIF
	
	IF NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(missionData)
		PRINTLN("[DONT89] - DONT89_LAUNCH_CONTROLLER_SCRIPT - RETURNING TRUE")
		RESET_NET_TIMER(g_sDontCrossTheLine89LaunchVars.stCoolDown)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//Check to see if any one in my office wants me to launch the script
FUNC BOOL DONT89_LAUNCH_SCRIPT_BECAUSE_REMOTE_PLAYER_WANTS_ME_TO(INT &iPlayerOnSofaCount)

	INT iPlayer
	PLAYER_INDEX aPlayerID
	INT iMyPlayerID = NATIVE_TO_INT(PLAYER_ID())
	BOOL bStanding
	BOOL bReturn = FALSE
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		aPlayerID = INT_TO_PLAYERINDEX(iPlayer)
		IF aPlayerID != INVALID_PLAYER_INDEX()
		AND PLAYER_ID() != aPlayerID
		AND IS_NET_PLAYER_OK(aPlayerID)
		AND IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(aPlayerID)].iDCTL89BitSet, ciDCTL89_BD_AVALIABLE_TO_PLAY)
		AND ((IS_PLAYER_IN_OFFICE_PROPERTY(PLAYER_ID()) AND (GlobalplayerBD_FM[NATIVE_TO_INT(aPlayerID)].propertyDetails.iVisibilityID = GlobalplayerBD_FM[iMyPlayerID].propertyDetails.iVisibilityID))
		OR ARE_PLAYERS_IN_SAME_SIMPLE_INTERIOR(aPlayerID, PLAYER_ID(), TRUE))
			
			bStanding = IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(aPlayerID)].iDCTL89BitSet, ciDCTL89_BD_STANDING_AT_SOFA)
			
			IF NOT bStanding
				iPlayerOnSofaCount++
				PRINTLN("[DONT8] - DONT89_LAUNCH_SCRIPT_BECAUSE_REMOTE_PLAYER_WANTS_ME_TO - iPlayerOnSofaCount = ", iPlayerOnSofaCount, " - player - ", GET_PLAYER_NAME(aPlayerID))
			ENDIF
			
			IF iPlayer < iMyPlayerID
			AND bStanding
				PRINTLN("[DONT89] - DONT89_LAUNCH_SCRIPT_BECAUSE_REMOTE_PLAYER_WANTS_ME_TO - bailing because ", GET_PLAYER_NAME(aPlayerID) ," wants to player ")
				RETURN FALSE
			ELIF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(aPlayerID)].iDCTL89BitSet, ciDCTL89_BD_DO_REMOTE_LAUNCH)
				IF NOT HAS_NET_TIMER_STARTED(g_sDontCrossTheLine89LaunchVars.stCoolDown)
				OR HAS_NET_TIMER_EXPIRED(g_sDontCrossTheLine89LaunchVars.stCoolDown, 500)
					PRINTLN("[DONT89] - DONT89_LAUNCH_SCRIPT_BECAUSE_REMOTE_PLAYER_WANTS_ME_TO - Launching because ", GET_PLAYER_NAME(aPlayerID) ," wants us to")
					bReturn = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN bReturn
ENDFUNC

/// PURPOSE:
///    Checks whether a ped can change poses while watching tv
/// RETURNS:
///    False when Female ped is wearing skirt/dress
FUNC BOOL CAN_PED_CHANGE_POSE_ON_COUCH()
	IF IS_PLAYER_FEMALE()
		PED_COMP_NAME_ENUM eStoredDress
		eStoredDress = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_JBIB) 
		PED_COMP_NAME_ENUM eStoredSkirt
		eStoredSkirt = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_LEGS) 
		
		#IF IS_DEBUG_BUILD
		IF IS_ITEM_A_DRESS(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_JBIB, eStoredDress) 
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CAN_PED_CHANGE_POSE: Ped is wearing a dress")
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CAN_PED_CHANGE_POSE: Ped is not wearing a dress")
		ENDIF
		
		IF IS_ITEM_A_SKIRT(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_LEGS, eStoredSkirt)  
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CAN_PED_CHANGE_POSE: Ped is wearing a skirt")
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CAN_PED_CHANGE_POSE: Ped is not wearing a skirt")
		ENDIF
		#ENDIF
		
		IF IS_ITEM_A_DRESS(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_JBIB, eStoredDress) 
		OR IS_ITEM_A_SKIRT(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_LEGS, eStoredSkirt) 
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Maintains player declaring that they want to change settings on the tv
/// PARAMS:
///    MPTVClient - client broadcast data
///    MPTVLocal - local data struct
/// RETURNS:
///    true when completed activating
FUNC BOOL CLIENT_MAINTAIN_ACTIVATING_TV(MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	BOOL bReadyForContext = FALSE
	BOOL bInOffice = IS_PLAYER_IN_OFFICE_PROPERTY(PLAYER_ID())
	BOOL bIsClubHouse = IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PLAYER_ID())
	BOOL bIsOfficeGarage = IS_PLAYER_IN_OFFICE_GARAGE_PROPERTY(PLAYER_ID())
	
	IF MPTVLocal.bInFixerHQ
	AND NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
	AND NOT IS_BIT_SET(MPTVLocal.iBitset2, MPTV_LOCAL_BS2_THIRD_TV)
		RETURN FALSE
	ENDIF
	
	IF NOT g_bCelebrationScreenIsActive
	AND NOT IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_RUNNING)
		IF IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
		AND NOT IS_BIT_SET(MPGlobals.KillYourselfData.iBitSet, biKY_AnimStarted)
		AND NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
		AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()	
		AND NOT IS_PAUSE_MENU_ACTIVE_EX()
			IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<226.861084,-975.741821,-99.999924>>, <<230.351517,-975.611084,-96.041504>>, 4.875000)
				AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()
					bReadyForContext = TRUE
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_LEAVING_SEAT)
				AND IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
					bReadyForContext = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	IF NOT IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_RUNNING)
		IF bReadyForContext
		AND (bInOffice
		OR (MPTVLocal.bInCasinoApartment
		AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
		#IF FEATURE_CASINO_HEIST
		OR (MPTVLocal.bInArcade
		AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
		#ENDIF
		#IF FEATURE_FIXER
		OR (MPTVLocal.bInFixerHQ
		AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
		#ENDIF)
			SET_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_ON_SOFA)
			IF MPTVClient.eStage != MP_TV_CLIENT_STAGE_ACTIVATED
			AND NOT IS_PLAYER_SCTV(PLAYER_ID())
			AND NOT IS_OWNER_PREVIEWING_INTERIOR()
			AND NOT IS_BROWSER_OPEN()
			AND NOT IS_CUSTOM_MENU_ON_SCREEN()
			AND NOT IS_WARNING_MESSAGE_ACTIVE()
			AND NOT NETWORK_IS_ACTIVITY_SESSION()
				DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(TRUE)
			ELSE
				DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE)
			ENDIF
			//If there's enough players then set it up that we can play
			IF IS_BIT_SET(GlobalplayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iDCTL89BitSet, ciDCTL89_BD_AVALIABLE_TO_PLAY)
			AND DONT89_ENOUGH_PLAYERS_IN_OFFICE_FOR_SELECTION()
			AND NOT IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_DONT_GIVE_LAUNCH_RPR)
				IF NOT IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_AVALIABLE)
					SET_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_AVALIABLE)
					SET_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_REFRESH_CONTROLS)
					PRINTLN("[DONT89] - ciDONT_CROSS_89_AVALIABLE")
				ENDIF
				//Set that we're ready to play
			//If there's no enough then clean up that we can play
			ELSE
				IF IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_AVALIABLE)
					SET_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_REFRESH_CONTROLS)
					CLEAR_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_AVALIABLE)
					PRINTLN("[DONT89] - NOT ciDONT_CROSS_89_AVALIABLE")
				ENDIF
			ENDIF
		ELSE
			//Clear that we're ready to play
			DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE, TRUE)
		ENDIF
	ELIF (bInOffice
	OR (MPTVLocal.bInCasinoApartment
	AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
	#IF FEATURE_CASINO_HEIST
	OR (MPTVLocal.bInArcade
	AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
	#ENDIF
	#IF FEATURE_FIXER
	OR (MPTVLocal.bInFixerHQ
	AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
	#ENDIF)
		REINIT_NET_TIMER(g_sDontCrossTheLine89LaunchVars.stPromptDelay,TRUE)
		//PRINTLN("CLIENT_MAINTAIN_ACTIVATING_TV: Turning on Delay context for exiting DCTL")
	ENDIF
	
	IF (bInOffice
	OR (MPTVLocal.bInCasinoApartment
	AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
	#IF FEATURE_CASINO_HEIST
	OR (MPTVLocal.bInArcade
	AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
	#ENDIF
	#IF FEATURE_FIXER
	OR (MPTVLocal.bInFixerHQ
	AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
	#ENDIF)
	AND HAS_NET_TIMER_STARTED(g_sDontCrossTheLine89LaunchVars.stPromptDelay)
	AND NOT HAS_NET_TIMER_EXPIRED(g_sDontCrossTheLine89LaunchVars.stPromptDelay,3000,TRUE)
		bReadyForContext = FALSE
		PRINTLN("CLIENT_MAINTAIN_ACTIVATING_TV: Delaying context for exiting DCTL")
	ENDIF
	
	IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention)
		MPTVLocal.iContextButtonIntention = NEW_CONTEXT_INTENTION
	ENDIF
	
	IF IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_REFRESH_CONTROLS)
	AND NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention)
		MPTVLocal.iContextButtonIntention = NEW_CONTEXT_INTENTION
		CLEAR_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_REFRESH_CONTROLS)
	ENDIF

	IF bReadyForContext
	AND NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
		IF bInPropertyOwnerNotPaidLastUtilityBill
			IF MPTVLocal.iContextButtonIntention = NEW_CONTEXT_INTENTION
				IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
					REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_GBILL")
				ELSE
					// Using different help on PC, as otherwise the wrong key is being shown.
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SBILL_KM")
					ELSE
						// This one for gamepad as it respects Japanese X/O Swap
					REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SBILL")
				ENDIF
			ENDIF
			ENDIF
		ELSE
			// B* url:bugstar:4319834
			IF g_sMPTunables.bON_CALL_BLOCKS_TV
				IF IS_PLAYER_IN_CORONA()
				OR AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
					RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention)
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF (MPTVLocal.bInCasinoApartment
			#IF FEATURE_CASINO_HEIST
			OR MPTVLocal.bInArcade
			#ENDIF
			#IF FEATURE_FIXER
			OR MPTVLocal.bInFixerHQ
			#ENDIF)
			AND NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
			AND g_bDontCrossRunning
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_SEAT")
				OR (IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_SEAT_KM"))
					RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention)
					MPTVLocal.iContextButtonIntention = NEW_CONTEXT_INTENTION
					CLEAR_HELP()
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_SEAT_NA")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_SEAT_NA_PC")
					RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention)
					MPTVLocal.iContextButtonIntention = NEW_CONTEXT_INTENTION
					CLEAR_HELP()
				ENDIF
			ENDIF
			
			IF MPTVLocal.iContextButtonIntention = NEW_CONTEXT_INTENTION
				//MP_MAGDEMO IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
						REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_GRGE")
					ELSE
						// Using different help on PC, as otherwise the wrong key is being shown.
						IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
							//If we are in an office we want to allow players to launch Don't Cross The Line '89
							IF (bInOffice
							OR (MPTVLocal.bInCasinoApartment
							AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
							#IF FEATURE_CASINO_HEIST
							OR (MPTVLocal.bInArcade
							AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
							#ENDIF
							#IF FEATURE_FIXER
							OR (MPTVLocal.bInFixerHQ
							AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
							#ENDIF)
							AND NOT IS_PAUSE_MENU_ACTIVE()
							AND NOT IS_CUSTOM_MENU_ON_SCREEN()
								IF IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_AVALIABLE)
									IF IS_REMOTE_PLAYER_RUNNING_DONT89_IN_OFFICE(TRUE)
										IF CAN_PED_CHANGE_POSE_ON_COUCH()
											REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEAT_KMO2")
										ELSE
											REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_ST_KMO2NP")
										ENDIF
									ELSE
										IF HAS_SEAT_ANIM_FINISHED()
											IF CAN_PED_CHANGE_POSE_ON_COUCH()
												REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEAT_KMO")
											ELSE
												REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEAT_KMONP")
											ENDIF
										ENDIF	
									ENDIF
								ELSE
									IF IS_REMOTE_PLAYER_RUNNING_DONT89_IN_OFFICE(TRUE)
										IF CAN_PED_CHANGE_POSE_ON_COUCH()
											REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEAT_KMO2")
										ELSE
											REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_ST_KMO2NP")
										ENDIF
									ELSE
										IF HAS_SEAT_ANIM_FINISHED()
											IF CAN_PED_CHANGE_POSE_ON_COUCH()
												REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEAT_KM_OF")
											ELSE
												REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_ST_KM_OFNP")
											ENDIF
										ENDIF	
									ENDIF
								ENDIF
							ELIF bIsClubHouse
							OR bIsOfficeGarage
								IF HAS_SEAT_ANIM_FINISHED()
									IF CAN_PED_CHANGE_POSE_ON_COUCH()
										REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEAT_CLUP")
									ELSE
										REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEAT_KM")
									ENDIF
								ELSE
									PRINTLN("CLIENT_MAINTAIN_ACTIVATING_TV - still playing sitting animation")
								ENDIF	
							ELSE
								IF (MPTVLocal.bInCasinoApartment
								#IF FEATURE_CASINO_HEIST
								OR MPTVLocal.bInArcade
								#ENDIF
								#IF FEATURE_FIXER
								OR MPTVLocal.bInFixerHQ
								#ENDIF)
								AND NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
								AND g_bDontCrossRunning
									REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEAT_NA_PC")
								ELSE
									REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEAT_KM")
								ENDIF
							ENDIF
						// This one for gamepad as it respects Japanese X/O Swap
						ELSE
							//If we are in an office we want to allow players to launch Don't Cross The Line '89
							IF bInOffice
							OR (MPTVLocal.bInCasinoApartment
							AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
							#IF FEATURE_CASINO_HEIST
							OR (MPTVLocal.bInArcade
							AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
							#ENDIF
							#IF FEATURE_FIXER
							OR (MPTVLocal.bInFixerHQ
							AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
							#ENDIF
								IF IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_AVALIABLE)
									IF IS_REMOTE_PLAYER_RUNNING_DONT89_IN_OFFICE(TRUE)
										IF CAN_PED_CHANGE_POSE_ON_COUCH()
											REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEATO2")
										ELSE
											REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEATO2NP")
										ENDIF
									ELSE
										CDEBUG1LN(DEBUG_MP_TV, "CLIENT_MAINTAIN_ACTIVATING_TV: bReadyForContext = true, ciDONT_CROSS_89_AVALIABLE: setting context intention")
										//Press ~INPUT_FRONTEND_ACCEPT~ to play "Don't Cross The Line". Or wait for more players to join.~n~Press ~INPUT_CONTEXT~ to watch TV.~n~Use ~INPUT_SCRIPT_LEFT_AXIS_X~ to change pose.~n~Press ~INPUT_FRONTEND_CANCEL~ to stand up.
										IF HAS_SEAT_ANIM_FINISHED()
											IF CAN_PED_CHANGE_POSE_ON_COUCH()
												REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEATO")
											ELSE
												REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEATONP")
											ENDIF
										ENDIF	
									ENDIF
								ELSE
									IF IS_REMOTE_PLAYER_RUNNING_DONT89_IN_OFFICE(TRUE)
										IF CAN_PED_CHANGE_POSE_ON_COUCH()
											REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEATO2")
										ELSE
											REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEATO2NP")
										ENDIF
									ELSE
//										CDEBUG1LN(DEBUG_MP_TV, "CLIENT_MAINTAIN_ACTIVATING_TV: bReadyForContext = true, NOT ciDONT_CROSS_89_AVALIABLE: setting context intention")
										//Wait for another player to sit down to player "Don't Cross The Line". Press ~INPUT_CONTEXT~ to watch TV.~n~Use ~INPUT_SCRIPT_LEFT_AXIS_X~ to change pose.~n~Press ~INPUT_FRONTEND_CANCEL~ to stand up.
										IF HAS_SEAT_ANIM_FINISHED()
											IF CAN_PED_CHANGE_POSE_ON_COUCH()
												REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEAT_OFF")
											ELSE
												REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEAT_OFFNP")
											ENDIF
										ENDIF	
									ENDIF
								ENDIF
							ELIF bIsClubHouse
							OR bIsOfficeGarage
								IF HAS_SEAT_ANIM_FINISHED()
									IF CAN_PED_CHANGE_POSE_ON_COUCH()
										REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEAT_CLU")
									ELSE
										REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEAT")
									ENDIF
								ELSE
								
									PRINTLN("CLIENT_MAINTAIN_ACTIVATING_TV - still playing sitting animation")
								ENDIF	
							ELSE
								IF (MPTVLocal.bInCasinoApartment
								#IF FEATURE_CASINO_HEIST
								OR MPTVLocal.bInArcade
								#ENDIF
								#IF FEATURE_FIXER
								OR MPTVLocal.bInFixerHQ
								#ENDIF)
								AND NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
								AND g_bDontCrossRunning
									REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEAT_NA")
								ELSE
									REGISTER_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention, CP_MEDIUM_PRIORITY, "MPTV_SEAT")
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				//MP_MAGDEMO ENDIF
			ELSE
				IF NOT IS_REMOTE_PLAYER_RUNNING_DONT89_IN_OFFICE()
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_SEATO2")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_SEATO2NP")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_SEAT_KMO2")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPTV_ST_KMO2NP")
						RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention)
					ENDIF
				ENDIF
				
				// B* url:bugstar:4319834
				IF g_sMPTunables.bON_CALL_BLOCKS_TV
					IF IS_PLAYER_IN_CORONA()
					OR AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
						RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention)
					ENDIF
				ENDIF
				
				IF HAS_CONTEXT_BUTTON_TRIGGERED(MPTVLocal.iContextButtonIntention)
					RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention)
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Create and make active the camera angle used when activated being able to interact with TV
/// PARAMS:
///    MPTVClient - client broadcast data
///    MPTVLocal - local data struct
///    property - current propert ID
///    bSetAsFocus - sets the camera as the game focus
/// RETURNS:
///    true when complete
FUNC BOOL CLIENT_SETUP_IN_ROOM_CAMERA(MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property, BOOL bSetAsFocus = FALSE)
	IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_WATCH_TV_CAMERA_SETUP)
		RETURN TRUE
	ELSE
		VECTOR vPos
		VECTOR vRot
		FLOAT fFOV
		IF GET_MP_TV_ROOM_CAMERA_INFO(MPTVLocal, MPTVClient, property, vPos, vRot, fFOV, MPTVLocal.iCurrentCamera)
			IF DOES_CAM_EXIST(MPTVLocal.camRoom)
				DESTROY_CAM(MPTVLocal.camRoom)
			ENDIF
			
			MPTVLocal.camRoom = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
			SET_CAM_COORD(MPTVLocal.camRoom, vPos)
			SET_CAM_ROT(MPTVLocal.camRoom, vRot)
			SET_CAM_FOV(MPTVLocal.camRoom, fFOV)
			SET_CAM_ACTIVE(MPTVLocal.camRoom, TRUE)
			SHAKE_CAM(MPTVLocal.camRoom, "HAND_SHAKE", 0.1)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			IF bSetAsFocus
				IF vPos.x <> 0.0
				OR vPos.y <> 0.0
				OR vPos.z <> 0.0
					SET_FOCUS_POS_AND_VEL(vPos, <<0,0,0>>)
				ENDIF
				NETWORK_SET_IN_FREE_CAM_MODE(TRUE)
				SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)")
				#ENDIF
			ENDIF
			
			IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
				TASK_TURN_PED_TO_FACE_COORD(PLAYER_PED_ID(), GET_MP_TV_POSITION(MPTVLocal, MPTVClient, property))
			ENDIF
			
			TASK_LOOK_AT_COORD(PLAYER_PED_ID(), GET_MP_TV_POSITION(MPTVLocal, MPTVClient, property), -1)
			
			START_AUDIO_SCENE("WATCHING_SAFEHOUSE_TV")
			
			SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_WATCH_TV_CAMERA_SETUP)
			SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_UPDATE_BUTTONS)
			#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)")
				//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_WATCH_TV_CAMERA_SETUP)")
			#ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Destory and make inactive the camera used when activated the TV
/// PARAMS:
///    MPTVLocal - local data struct
/// RETURNS:
///    true when complete
FUNC BOOL CLIENT_CLEANUP_IN_ROOM_CAMERA(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_WATCH_TV_CAMERA_SETUP)
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== MPTV LOCAL === CLIENT_CLEANUP_IN_ROOM_CAMERA: MPTV_LOCAL_BS_WATCH_TV_CAMERA_SETUP: NOT SET")
		#ENDIF
		RETURN TRUE
	ELSE
		#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_SPECTATOR, "=== MPTV LOCAL === CLIENT_CLEANUP_IN_ROOM_CAMERA: MPTV_LOCAL_BS_WATCH_TV_CAMERA_SETUP: SET")
		#ENDIF
		IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)
			CLEAR_FOCUS()
			//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === CLIENT_CLEANUP_IN_ROOM_CAMERA - CLEAR_FOCUS")
			NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
			CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)")
			#ENDIF
		ENDIF
		
		IF DOES_CAM_EXIST(MPTVLocal.camRoom)
			SET_CAM_ACTIVE(MPTVLocal.camRoom, FALSE)
			DESTROY_CAM(MPTVLocal.camRoom)
		ENDIF
		
		STOP_AUDIO_SCENE("WATCHING_SAFEHOUSE_TV")
		
		CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_WATCH_TV_CAMERA_SETUP)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_UPDATE_BUTTONS)
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Server changes the volume of the TV
/// PARAMS:
///    MPTVServer - server broadcast data
///    bIncrease - true if to increase volume
///    bGarage - true if tv is in a garage
PROC INCREMENT_TV_VOLUME(MP_TV_SERVER_DATA_STRUCT &MPTVServer, BOOL bIncrease, BOOL bGarage)
	
	IF bGarage
		IF bIncrease
			MPTVServer.fGarageVolume += MP_TV_VOLUME_INCREMENT
		ELSE
			MPTVServer.fGarageVolume -=  MP_TV_VOLUME_INCREMENT
		ENDIF
		IF MPTVServer.fGarageVolume >= MP_TV_MAX_VOLUME
			MPTVServer.fGarageVolume = MP_TV_MAX_VOLUME
		ENDIF
		IF MPTVServer.fGarageVolume <= MP_TV_MIN_VOLUME
			MPTVServer.fGarageVolume = MP_TV_MIN_VOLUME
		ENDIF
	ELSE
		IF bIncrease
			MPTVServer.fVolume += MP_TV_VOLUME_INCREMENT
		ELSE
			MPTVServer.fVolume -=  MP_TV_VOLUME_INCREMENT
		ENDIF
		IF MPTVServer.fVolume >= MP_TV_MAX_VOLUME
			MPTVServer.fVolume = MP_TV_MAX_VOLUME
		ENDIF
		IF MPTVServer.fVolume <= MP_TV_MIN_VOLUME
			MPTVServer.fVolume = MP_TV_MIN_VOLUME
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Returns if local player can currently switch between different activated camerea angles
/// PARAMS:
///    MPTVServer - server broadcast data
///    MPTVClient - client broadcast data
///    MPTVLocal - local data struct
/// RETURNS:
///    true/false
FUNC BOOL SHOULD_ALLOW_MP_TV_IN_ROOM_CAMERA_CHANGE(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	IF GET_MP_TV_SERVER_STAGE(MPTVServer, MPTVClient) = MP_TV_SERVER_STAGE_OFF
		RETURN TRUE
	ENDIF
	IF GET_MP_TV_SERVER_STAGE(MPTVServer, MPTVClient) = MP_TV_SERVER_STAGE_ON
		IF MPTVLocal.eCurrentChannel = MP_TV_CHANNEL_CNT
		OR MPTVLocal.eCurrentChannel = MP_TV_CHANNEL_WEAZEL
		OR MPTVLocal.eCurrentChannel = MP_TV_CHANNEL_ARCADE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Processes the player inputting that they want to change volume, channel etc and turns it into flags for the server
/// PARAMS:
///    MPTVServer - server broadcast data
///    MPTVClient - client broadcast data
///    MPTVLocal - local data struct
///    property - current property ID
/// RETURNS:
///    true if player declares they no longer want to interact with TV
FUNC BOOL CLIENT_MAINTAIN_MP_TV_INPUT(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property)
	IF NOT IS_PAUSE_MENU_ACTIVE()
	AND NOT IS_PHONE_ONSCREEN()
	AND NOT IS_WARNING_MESSAGE_ACTIVE()
	AND NOT IS_BROWSER_OPEN()
	AND MPTVLocal.eSwitchState = MP_TV_SWITCH_STATE_WATCH_CHANNEL
	AND NOT IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_RUNNING)
	
		// B* 2289087 - To prevent a PC keyboard and mouse control conflict with other safe-house activities
		// I'm using INPUT_CONTEXT to turn on/off the TV for keyboard and mouse controls only.
		
		CONTROL_ACTION caTurnOffTVInput
		
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			caTurnOffTVInput = INPUT_CONTEXT
		ELSE
			caTurnOffTVInput = INPUT_SCRIPT_RUP
		ENDIF
	
		IF IS_BIT_SET(MPTVClient.iInputBitset, MPTV_INPUT_POWER_ON)
			IF GET_MP_TV_SERVER_STAGE(MPTVServer, MPTVClient) = MP_TV_SERVER_STAGE_ON
				CLEAR_BIT(MPTVClient.iInputBitset, MPTV_INPUT_POWER_ON)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(MPTVClient.iInputBitset, MPTV_INPUT_POWER_OFF)
			IF GET_MP_TV_SERVER_STAGE(MPTVServer, MPTVClient) = MP_TV_SERVER_STAGE_OFF
				CLEAR_BIT(MPTVClient.iInputBitset, MPTV_INPUT_POWER_OFF)
			ENDIF
		ENDIF
		
		IF MPTVClient.eTimeOfInputChannel <> GET_MP_TV_SERVER_CURRENT_CHANNEL(MPTVServer, MPTVClient)
			CLEAR_BIT(MPTVClient.iInputBitset, MPTV_INPUT_NEXT_CHANNEL)
			CLEAR_BIT(MPTVClient.iInputBitset, MPTV_INPUT_PREVIOUS_CHANNEL)
			MPTVClient.eTimeOfInputChannel = GET_MP_TV_SERVER_CURRENT_CHANNEL(MPTVServer, MPTVClient)
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, 	"=== MPTV CLIENT === New channel ", GET_MP_TV_CHANNEL_NAME(GET_MP_TV_SERVER_CURRENT_CHANNEL(MPTVServer, MPTVClient)), 
			//							" seen, resetting channel change requests.")
			#ENDIF
		ENDIF
		
		IF MPTVClient.fTimeOfInputVolume <> GET_MP_TV_SERVER_CURRENT_VOLUME(MPTVServer, MPTVClient)
			CLEAR_BIT(MPTVClient.iInputBitset, MPTV_INPUT_VOLUME_UP)
			CLEAR_BIT(MPTVClient.iInputBitset, MPTV_INPUT_VOLUME_DOWN)
			MPTVClient.fTimeOfInputVolume = GET_MP_TV_SERVER_CURRENT_VOLUME(MPTVServer, MPTVClient)
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, 	"=== MPTV CLIENT === New volume ", GET_MP_TV_SERVER_CURRENT_VOLUME(MPTVServer, MPTVClient), 
			//							" seen, resetting volume change requests.")
			#ENDIF
		ENDIF
		
		ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		
		IF IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE))
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV CLIENT === CLIENT_MAINTAIN_MP_TV_INPUT = TRUE")
			#ENDIF
			IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_UPDATE_BUTTONS)
			DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE, TRUE)
			RETURN TRUE
		ELSE
			INT iHorizIncrement = 0
			INT iVertIncrement = 0
			IF !g_bDontCrossRunning
				IF SHOULD_ANALOGUE_SELECTION_BE_INCREMENTED(MPTVLocal.timeIncrement, iHorizIncrement, TRUE, -1, FALSE)
				OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, caTurnOffTVInput)
				OR ((MPTVLocal.eCurrentChannel = MP_TV_CHANNEL_CNT			//only volume on relevant channels

					OR MPTVLocal.eCurrentChannel = MP_TV_CHANNEL_WEAZEL
					OR MPTVLocal.eCurrentChannel = MP_TV_CHANNEL_ARCADE)
					AND SHOULD_ANALOGUE_SELECTION_BE_INCREMENTED(MPTVLocal.timeIncrement, iVertIncrement, FALSE, -1, FALSE))
				
					CLEAR_BIT(MPTVClient.iInputBitset, MPTV_INPUT_NEXT_CHANNEL)
					CLEAR_BIT(MPTVClient.iInputBitset, MPTV_INPUT_PREVIOUS_CHANNEL)
					CLEAR_BIT(MPTVClient.iInputBitset, MPTV_INPUT_POWER_ON)
					CLEAR_BIT(MPTVClient.iInputBitset, MPTV_INPUT_POWER_OFF)
					CLEAR_BIT(MPTVClient.iInputBitset, MPTV_INPUT_VOLUME_UP)
					CLEAR_BIT(MPTVClient.iInputBitset, MPTV_INPUT_VOLUME_DOWN)
					IF iHorizIncrement = 1
					OR iHorizIncrement = -1
						IF TIMERA() > 300
							IF iHorizIncrement = 1
								SET_BIT(MPTVClient.iInputBitset, MPTV_INPUT_NEXT_CHANNEL)
								#IF IS_DEBUG_BUILD
								//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV CLIENT === SET_BIT(MPTVClient.iInputBitset, MPTV_INPUT_NEXT_CHANNEL)")
								//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV CLIENT === MPTVClient.eTimeOfInputChannel = ", GET_MP_TV_CHANNEL_NAME(MPTVClient.eTimeOfInputChannel))
								#ENDIF
							ELIF iHorizIncrement = -1
								SET_BIT(MPTVClient.iInputBitset, MPTV_INPUT_PREVIOUS_CHANNEL)
								#IF IS_DEBUG_BUILD
								//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV CLIENT === SET_BIT(MPTVClient.iInputBitset, MPTV_INPUT_PREVIOUS_CHANNEL)")
								//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV CLIENT === MPTVClient.eTimeOfInputChannel = ", GET_MP_TV_CHANNEL_NAME(MPTVClient.eTimeOfInputChannel))
								#ENDIF
							ENDIF
							SETTIMERA(0)
						ELSE	
							CDEBUG1LN(DEBUG_SPECTATOR, "=== MPTV CLIENT === Channel change not registered, 300 millisecond timer not complete")
						ENDIF
						
					ELIF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, caTurnOffTVInput)
						IF GET_MP_TV_SERVER_STAGE(MPTVServer, MPTVClient) = MP_TV_SERVER_STAGE_OFF
							SET_BIT(MPTVClient.iInputBitset, MPTV_INPUT_POWER_ON)
							#IF IS_DEBUG_BUILD
							//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV CLIENT === SET_BIT(MPTVClient.iInputBitset, MPTV_INPUT_POWER_ON)")
							#ENDIF
						ELIF GET_MP_TV_SERVER_STAGE(MPTVServer, MPTVClient) = MP_TV_SERVER_STAGE_ON
							SET_BIT(MPTVClient.iInputBitset, MPTV_INPUT_POWER_OFF)
							#IF IS_DEBUG_BUILD
							//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV CLIENT === SET_BIT(MPTVClient.iInputBitset, MPTV_INPUT_POWER_OFF)")
							#ENDIF
						ENDIF
					ELIF iVertIncrement = 1
						SET_BIT(MPTVClient.iInputBitset, MPTV_INPUT_VOLUME_UP)
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV CLIENT === SET_BIT(MPTVClient.iInputBitset, MPTV_INPUT_VOLUME_UP)")
						//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV CLIENT === MPTVClient.fTimeOfInputVolume = ", MPTVClient.fTimeOfInputVolume)
						#ENDIF
					ELIF iVertIncrement = -1
						SET_BIT(MPTVClient.iInputBitset, MPTV_INPUT_VOLUME_DOWN)
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV CLIENT === SET_BIT(MPTVClient.iInputBitset, MPTV_INPUT_VOLUME_DOWN)")
						//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV CLIENT === MPTVClient.fTimeOfInputVolume = ", MPTVClient.fTimeOfInputVolume)
						#ENDIF
					ENDIF
				ENDIF
			ENDIF			
			IF SHOULD_ALLOW_MP_TV_IN_ROOM_CAMERA_CHANGE(MPTVServer, MPTVClient, MPTVLocal)
				IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_SELECT)
				
					MPTVLocal.iCurrentCamera++
					IF MPTVLocal.iCurrentCamera > MP_TV_NUMBER_OF_CAMERAS-1
						MPTVLocal.iCurrentCamera = 0
					ENDIF
				
					CLIENT_CLEANUP_IN_ROOM_CAMERA(MPTVLocal)
					CLIENT_SETUP_IN_ROOM_CAMERA(MPTVClient, MPTVLocal, property)
				ENDIF
			ENDIF
			IF NOT (GET_MP_TV_SERVER_STAGE(MPTVServer, MPTVClient) = MP_TV_SERVER_STAGE_ON
					AND MPTVLocal.eCurrentChannel = MP_TV_CHANNEL_SPECTATE)
				IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_UPDATE_BUTTONS)
				OR HAVE_CONTROLS_CHANGED(PLAYER_CONTROL)
					REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPTVLocal.specData.specHUDData.scaleformInstructionalButtons)
					
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(FALSE)), 
														"HUD_INPUT3", MPTVLocal.specData.specHUDData.scaleformInstructionalButtons)//BACK
					
					IF GET_MP_TV_SERVER_STAGE(MPTVServer, MPTVClient) = MP_TV_SERVER_STAGE_OFF
					AND !g_bDontCrossRunning
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caTurnOffTVInput), 
															"HUD_INPUT81", MPTVLocal.specData.specHUDData.scaleformInstructionalButtons)//TURN ON
															
					ELIF GET_MP_TV_SERVER_STAGE(MPTVServer, MPTVClient) = MP_TV_SERVER_STAGE_ON
					AND !g_bDontCrossRunning
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, caTurnOffTVInput), 
															"HUD_INPUT82", MPTVLocal.specData.specHUDData.scaleformInstructionalButtons)//TURN OFF
						
						IF MPTVLocal.bInCasinoApartment
						AND NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X), 
																"HUD_INPUT_97", MPTVLocal.specData.specHUDData.scaleformInstructionalButtons)//SELECT CHANNEL
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X), 
																"HUD_INPUT75", MPTVLocal.specData.specHUDData.scaleformInstructionalButtons)//SELECT CHANNEL
						ENDIF
						
						IF MPTVLocal.eCurrentChannel = MP_TV_CHANNEL_CNT		//only volume on relevant channels
						OR MPTVLocal.eCurrentChannel = MP_TV_CHANNEL_WEAZEL
						OR MPTVLocal.eCurrentChannel = MP_TV_CHANNEL_ARCADE
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y), 
																"HUD_INPUT77", MPTVLocal.specData.specHUDData.scaleformInstructionalButtons)//CHANGE VOLUME
						ENDIF
					ENDIF
					
					IF SHOULD_ALLOW_MP_TV_IN_ROOM_CAMERA_CHANGE(MPTVServer, MPTVClient, MPTVLocal)
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(	GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(PLAYER_CONTROL, INPUT_SCRIPT_SELECT), 
															"HUD_INPUT87", MPTVLocal.specData.specHUDData.scaleformInstructionalButtons)//CAMERA MODE
					ENDIF
					
					CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_UPDATE_BUTTONS)
				ENDIF	
				
				SPRITE_PLACEMENT thisSpritePlacement = 	GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
				
				SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(MPTVLocal.specData.specHUDData.scaleformInstructionalButtons, 1.0)
				
				RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPTVLocal.specData.specHUDData.sfButton, 
													thisSpritePlacement, 
													MPTVLocal.specData.specHUDData.scaleformInstructionalButtons, 
													SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(MPTVLocal.specData.specHUDData.scaleformInstructionalButtons))
				
				SET_INSTRUCTIONAL_BUTTONS_UNDER_HUD_THIS_FRAME()
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    prepares the local player to change channel
/// PARAMS:
///    MPTVLocal - local data struct
///    channelFrom - channel player is leaving
///    channelTo - channel player is going to
PROC CLIENT_SETUP_SWITCH_CHANNEL(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_TV_CHANNEL channelFrom, MP_TV_CHANNEL channelTo)
	MPTVLocal.iChannelSwitchTimer = 0
	SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_SETUP_PROCESS)
	MPTVLocal.eSwitchChannelFrom = channelFrom
	MPTVLocal.eSwitchChannelTo = channelTo
	CLEAR_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_ON)
	CLEAR_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_OFF)
	CLEAR_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_ACTIVATING)
	CLEAR_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_DEACTIVATING)
	#IF IS_DEBUG_BUILD
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === CLIENT_SETUP_SWITCH_CHANNEL")
	#ENDIF
	SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_UPDATE_BUTTONS)
ENDPROC

/// PURPOSE:
///    Sets up client turning on tv to the correct first channel
/// PARAMS:
///    MPTVLocal - local data struct
///    channelTo - first channel to change to
PROC CLIENT_SETUP_TV_TURN_ON(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_TV_CHANNEL channelTo)
	CLIENT_SETUP_SWITCH_CHANNEL(MPTVLocal, channelTo, channelTo)
	SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_ON)
	#IF IS_DEBUG_BUILD
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === CLIENT_SETUP_TV_TURN_ON")
	#ENDIF
	g_bTVOn = TRUE
ENDPROC

/// PURPOSE:
///    Sets up local player turning off tv
/// PARAMS:
///    MPTVLocal - local data struct
///    channelFrom - last channel watched
PROC CLIENT_SETUP_TV_TURN_OFF(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_TV_CHANNEL channelFrom)
	CLIENT_SETUP_SWITCH_CHANNEL(MPTVLocal, channelFrom, channelFrom)
	SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_OFF)
	#IF IS_DEBUG_BUILD
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === CLIENT_SETUP_TV_TURN_OFF")
	#ENDIF
	g_bTVOn = FALSE
ENDPROC

/// PURPOSE:
///    Sets up the client activating interacting with the tv
/// PARAMS:
///    MPTVLocal - 
///    channelTo - channel to being with
PROC CLIENT_SETUP_TV_ACTIVATING(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_TV_CHANNEL channelTo)
	SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_UPDATE_BUTTONS)
	CLIENT_SETUP_SWITCH_CHANNEL(MPTVLocal, channelTo, channelTo)
	ENABLE_MOVIE_SUBTITLES(TRUE)
	Pause_Objective_Text()
	SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_ACTIVATING)
	MPTVLocal.iCurrentCamera = 0
	#IF IS_DEBUG_BUILD
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === CLIENT_SETUP_TV_ACTIVATING")
	#ENDIF
	g_bMPTVplayerWatchingTV = TRUE
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === g_bMPTVplayerWatchingTV = TRUE")
ENDPROC

/// PURPOSE:
///    Sets up client deactivating interacting with the tv
/// PARAMS:
///    MPTVLocal - 
///    channelFrom - final channel watched
PROC CLIENT_SETUP_TV_DEACTIVATING(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_TV_CHANNEL channelFrom)
	CLIENT_SETUP_SWITCH_CHANNEL(MPTVLocal, channelFrom, channelFrom)
	ENABLE_MOVIE_SUBTITLES(FALSE)
	SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_DEACTIVATING)
	CLEAR_BIT(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_STOP_MPTV_SPECTATE)
	#IF IS_DEBUG_BUILD
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === CLIENT_SETUP_TV_DEACTIVATING")
	#ENDIF
ENDPROC


/// PURPOSE:
///    Maintains the tv channel on the tv in the lcoal player's current property
/// PARAMS:
///    MPTVServer - 
///    MPTVClient - 
///    MPTVLocal - 
PROC CLIENT_MAINTAIN_AMBIENT_TV(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	
	
	IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_WATCH_TV_CLEANUP)
	
		SWITCH GET_MP_TV_SERVER_STAGE(MPTVServer, MPTVClient)
			CASE MP_TV_SERVER_STAGE_ON
				IF MPTVLocal.eCurrentChannel <> GET_MP_TV_SERVER_CURRENT_CHANNEL(MPTVServer, MPTVClient)
					//IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_ON)
						CLIENT_SETUP_SWITCH_CHANNEL(MPTVLocal, MPTVLocal.eCurrentChannel, GET_MP_TV_SERVER_CURRENT_CHANNEL(MPTVServer, MPTVClient))
					//ENDIF
					MPTVLocal.tvChannelTypeCurrent = GET_MP_TV_CHANNEL_TVCHANNELTYPE(GET_MP_TV_SERVER_CURRENT_CHANNEL(MPTVServer, MPTVClient))
					MPTVLocal.eCurrentChannel = GET_MP_TV_SERVER_CURRENT_CHANNEL(MPTVServer, MPTVClient)
				ENDIF
				
				//Special case: Watching a high-end garage cctv, as it has a tv inside it.
				IF MPTVLocal.eCurrentChannel = MP_TV_CHANNEL_CCTV_GARAGE
				AND MPTVClient.eStage = MP_TV_CLIENT_STAGE_ACTIVATED
				AND GET_MP_TV_SWITCH_STATE(MPTVLocal) > MP_TV_SWITCH_STATE_FADE_IN	//fading into interior, or beyond (ie watching channel)
					SWITCH MPTVServer.eGarageStage
						CASE MP_TV_SERVER_STAGE_ON
							IF GET_TV_CHANNEL() <> GET_MP_TV_CHANNEL_TVCHANNELTYPE(MPTVServer.eCurrentGarageChannel)
								SET_TV_CHANNEL_AND_PLAYLIST(MPTVLocal, MPTVServer.eCurrentGarageChannel)
							ENDIF
						BREAK
						CASE MP_TV_SERVER_STAGE_OFF
							IF GET_TV_CHANNEL() <> TVCHANNELTYPE_CHANNEL_NONE
								SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
							ENDIF
						BREAK
					ENDSWITCH
				ELSE
					IF GET_TV_CHANNEL() <> MPTVLocal.tvChannelTypeCurrent
						SET_TV_CHANNEL_AND_PLAYLIST(MPTVLocal, MPTVLocal.eCurrentChannel)
					ENDIF
				ENDIF
				
				IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_REQUESTED_TURN_OFF)
					CLEAR_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_REQUESTED_TURN_OFF)
				ENDIF
				IF NOT IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_REQUESTED_TURN_ON)
					CLIENT_SETUP_TV_TURN_ON(MPTVLocal, MPTVLocal.eCurrentChannel)
					SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_REQUESTED_TURN_ON)
				ENDIF
			BREAK
			CASE MP_TV_SERVER_STAGE_OFF
				IF GET_TV_CHANNEL() <> TVCHANNELTYPE_CHANNEL_NONE
					SET_TV_CHANNEL(TVCHANNELTYPE_CHANNEL_NONE)
				ENDIF
				IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_REQUESTED_TURN_ON)
					CLEAR_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_REQUESTED_TURN_ON)
				ENDIF
				IF NOT IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_REQUESTED_TURN_OFF)
					CLIENT_SETUP_TV_TURN_OFF(MPTVLocal, MPTVLocal.eCurrentChannel)
					SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_REQUESTED_TURN_OFF)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_CLEAR_DISPLAY)
		CLIENT_CLEAR_TV_RENDERTARGET(MPTVLocal)
		#IF IS_DEBUG_BUILD
		//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_CLEAR_DISPLAY)")
		#ENDIF
		CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_CLEAR_DISPLAY)
	ELSE
		IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_RENDER_TARGET_FOUND)
			IF DOES_ENTITY_EXIST(MPTVLocal.objOverlay)
				IF IS_NAMED_RENDERTARGET_LINKED(GET_ENTITY_MODEL(MPTVLocal.objOverlay))
					SET_TEXT_RENDER_ID(MPTVLocal.iRenderTarget)
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
					SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
					DRAW_TV_CHANNEL(0.5,0.5,1.0,1.0,0.0,255,255,255,255)
					IF GET_TV_CHANNEL() = TVCHANNELTYPE_CHANNEL_NONE
						SET_MP_TV_SCREEN_OBJECT_VISIBLE(MPTVLocal, TRUE)
					ELSE
						SET_MP_TV_SCREEN_OBJECT_VISIBLE(MPTVLocal, TRUE)
						ATTACH_TV_AUDIO_TO_ENTITY(MPTVLocal.objOverlay)
					ENDIF
					SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
				ELSE
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === IS_NAMED_RENDERTARGET_LINKED(GET_ENTITY_MODEL(MPTVLocal.objOverlay)) = FALSE")
					#ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SHOULD_RENDER_TARGET_EXIST)
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_RENDER_TARGET_FOUND) = FALSE")
				#ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_STRIPPER_MUSIC)
		IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SET_FOR_STRIPPER_MUSIC)
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_SET_FOR_STRIPPER_MUSIC)")
			#ENDIF
			SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_SET_FOR_STRIPPER_MUSIC)
		ENDIF
	ELSE
		IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SET_FOR_STRIPPER_MUSIC)
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_SET_FOR_STRIPPER_MUSIC)")
			#ENDIF
			CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_SET_FOR_STRIPPER_MUSIC)
		ENDIF
	ENDIF
	
	UPDATE_MP_TV_VOLUME(MPTVServer, MPTVClient, MPTVLocal)
	
ENDPROC

/// PURPOSE:
///    Updates the news information on the overlay on teh news channel
/// PARAMS:
///    MPTVLocal - 
PROC REFRESH_MP_TV_NEWS_INFO(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	GAMER_HANDLE gamerPlayer
	NETWORK_CLAN_DESC crewPlayer
	TEXT_LABEL_15 tl15Line0, tl15Line1
	GET_NEWS_HUD_TEXT_LABELS(tl15Line0, tl15Line1)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(MPTVLocal.sfTV, "SET_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPTV_TITLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPTV_SBTTL")
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(MPTVLocal.sfTV, "CLEAR_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	END_SCALEFORM_MOVIE_METHOD()	
	BEGIN_SCALEFORM_MOVIE_METHOD(MPTVLocal.sfTV, "SET_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LABEL_WITH_PLAYER_NAME("MPTV_TICK0", GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED())))
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(MPTVLocal.sfTV, "SET_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(1)
		gamerPlayer = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
		IF IS_PLAYER_IN_ACTIVE_CLAN(gamerPlayer)
			crewPlayer = GET_PLAYER_CREW(NETWORK_GET_PLAYER_INDEX_FROM_PED(GET_SPECTATOR_CURRENT_FOCUS_PED()))
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LABEL_WITH_TL63("MPTV_TICK1", GET_CREW_CLAN_NAME(gamerPlayer, crewPlayer))
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPTV_TICK2")
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(MPTVLocal.sfTV, "SET_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(2)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tl15Line0)
	END_SCALEFORM_MOVIE_METHOD()
	BEGIN_SCALEFORM_MOVIE_METHOD(MPTVLocal.sfTV, "SET_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(3)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(tl15Line1)
	END_SCALEFORM_MOVIE_METHOD()
	
	MPTVLocal.fNewsFilterScrollEntry = 0
	
	BEGIN_SCALEFORM_MOVIE_METHOD(MPTVLocal.sfTV, "DISPLAY_SCROLL_TEXT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(MPTVLocal.fNewsFilterScrollEntry)
	END_SCALEFORM_MOVIE_METHOD()
	
	MPTVLocal.timeNewsFilterNextUpdate = GET_NETWORK_TIME()
	
	CLEAR_REDO_NEWS_HUD_DUE_TO_EVENT()
	
ENDPROC

/// PURPOSE:
///    Processes switching between the local machines currently displayed channel, and the channel that the local player should be watching
/// PARAMS:
///    MPTVServer - 
///    MPTVClient - 
///    MPTVLocal - 
///    property - 
/// RETURNS:
///    false always
FUNC BOOL CLIENT_MAINTAIN_CURRENT_WATCHED_CHANNEL(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property)
	BOOL bFinished = FALSE
	INT iEntrance 
	CCTV_DETAILS thisCCTV
	INT iEntranceType
	BOOL bFullscreenToFullscreen = FALSE
	BOOL bCCTVToCCTV = FALSE
	BOOL bReturnTrue = FALSE
	
	bFullscreenToFullscreen = FALSE
	IF NOT IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_ON)
	AND NOT IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_OFF)
	AND NOT IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_ACTIVATING)
	AND NOT IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_DEACTIVATING)
	AND IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eSwitchChannelFrom)
	AND IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eSwitchChannelTo)
		bFullscreenToFullscreen = TRUE
		IF IS_MP_TV_CHANNEL_CCTV(MPTVLocal.eSwitchChannelFrom)
		AND IS_MP_TV_CHANNEL_CCTV(MPTVLocal.eSwitchChannelTo)
			bCCTVToCCTV = TRUE
		ENDIF
	ENDIF
	
	
	SWITCH GET_MP_TV_SWITCH_STATE(MPTVLocal)
		CASE MP_TV_SWITCH_STATE_SETUP_PROCESS
		
			IF NOT IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_OFF)			//If special case: news channel lost suspect
			AND NOT IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_ON)
			AND NOT IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_ACTIVATING)
			AND NOT IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_DEACTIVATING)
			AND MPTVLocal.eSwitchChannelFrom = MP_TV_CHANNEL_NEWS
			AND IS_BIT_SET(MPTVServer.iBitset, MPTV_SERVER_BS_NEWS_CHANNEL_LOST_CRIMINAL)
			
				IF HAS_SCALEFORM_MOVIE_LOADED(MPTVLocal.sfTV)
					
					BEGIN_SCALEFORM_MOVIE_METHOD(MPTVLocal.sfTV, "SET_TEXT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPTV_CETTL")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPTV_CESTTL")
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
				
				MPTVLocal.camCCTV = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
				
				SET_CAM_COORD(MPTVLocal.camCCTV, GET_FINAL_RENDERED_CAM_COORD())
				SET_CAM_ROT(MPTVLocal.camCCTV, GET_FINAL_RENDERED_CAM_ROT())
				SET_CAM_FOV(MPTVLocal.camCCTV, GET_FINAL_RENDERED_CAM_FOV())
				SET_CAM_ACTIVE(MPTVLocal.camCCTV, TRUE)
				SHAKE_CAM(MPTVLocal.camCCTV, "HAND_SHAKE", 0.5)
				RENDER_SCRIPT_CAMS (TRUE, FALSE)
				SET_FOCUS_POS_AND_VEL(GET_FINAL_RENDERED_CAM_COORD(), <<0,0,0>>)
				////CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === SET_FOCUS_POS_AND_VEL ", GET_FINAL_RENDERED_CAM_COORD()) fix for build
				NETWORK_SET_IN_FREE_CAM_MODE(TRUE)
				SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)
				
				MPTVLocal.iChannelSwitchTimer = GET_GAME_TIMER()
				
				DISABLE_SPEC_CAN_FIND_NEW_FOCUS()
				
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === setup criminal lost camera")
				#ENDIF
				
				SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_CRIMINAL_LOST)
			ELSE
				bFinished = FALSE
				IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_OFF)
				OR IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_DEACTIVATING)
					IF NOT IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eSwitchChannelFrom)	//if we're turning off or deactivating when watching an in-room channel or off tv, we're done
					OR IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_OFF)
						#IF IS_DEBUG_BUILD
						IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_OFF)
							//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === we're deactivating a turned-off TV, we're done")
						ELSE
							//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === we're turning off when watching an in-room channel, we're done")
						ENDIF
						#ENDIF
						bFinished = TRUE
					ENDIF
				ELIF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_ACTIVATING)		//if we're activating when watching an in-room channel or off tv, we're done
					IF NOT IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eSwitchChannelTo)
					OR IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_OFF)
						#IF IS_DEBUG_BUILD
						IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_OFF)
							//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === we're activating a turned-off TV, we're done")
						ELSE
							//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === we're activating to watch an in-room channel, we're done")
						ENDIF
						#ENDIF
						bFinished = TRUE
					ENDIF
				ELIF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_ON)		//if we're turning on to watch an in-room channel, we're done
					IF NOT IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eSwitchChannelTo)
						#IF IS_DEBUG_BUILD
						IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_OFF)
							//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === we're turning on to watch an in-room channel, we're done")
						ENDIF
						#ENDIF
						bFinished = TRUE
					ENDIF
				ELSE
					IF NOT IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eSwitchChannelFrom) 	//if we're switching from an in-room channel to an in-room channel, we're done
					AND NOT IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eSwitchChannelTo)
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === we're switching from an in-room channel to an in-room channel, we're done")
						#ENDIF
						bFinished = TRUE
					ENDIF
				ENDIF
			
				IF bFinished
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === No need to fade")
					#ENDIF
					SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_UPDATE_BUTTONS)
					SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_CLEANUP_PROCESS)
				ELSE
					IF bCCTVToCCTV
						SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
					ENDIF
					SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_FADE_OUT)
				ENDIF
			ENDIF
		BREAK
		
		CASE MP_TV_SWITCH_STATE_CRIMINAL_LOST
			
			IF HAS_SCALEFORM_MOVIE_LOADED(MPTVLocal.sfTV)
			AND GET_GAME_TIMER() - MPTVLocal.iChannelSwitchTimer < MP_TV_SUSPECT_LOST_TIME
				IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
					SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
				ENDIF
			ELSE
				SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_FADE_OUT)
			ENDIF
			
		BREAK
		
		CASE MP_TV_SWITCH_STATE_FADE_OUT
		
			DISABLE_SPECTATOR_FADES_DURING_THIS_EVENT(TRUE)
			
			IF (NOT IS_SCREEN_FADED_OUT())
			AND (NOT bFullscreenToFullscreen)
				IF (NOT IS_SCREEN_FADING_OUT())
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === DO_SCREEN_FADE_OUT(0)")
					#ENDIF
					
					IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_ON)
					OR IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_ACTIVATING)
					OR ((NOT IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eSwitchChannelFrom)) AND IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eSwitchChannelTo))
						MP_TV_FADE_OUT(MPTVLocal,MP_TV_FADE_TIME_POWER_CHANGE)					
					ELSE
						MP_TV_FADE_OUT(MPTVLocal,MP_TV_FADE_TIME_CHANNEL_CHANGE)
					ENDIF
				ENDIF
			ELSE
				IF bFullscreenToFullscreen
					SET_TIMECYCLE_MODIFIER("CAMERA_secuirity_FUZZ")
					REFRESH_SCALEFORM_LOADING_ICON(MPTVLocal.loadingIcon)
					SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DRAW_LOADING_ICON)
					
					IF NOT g_LastPlayerAliveTinyRacers
						MP_TV_SOUND_START_CCTV_CHANGE_CAM(MPTVLocal)
					ENDIF
					
					TOGGLE_RENDERPHASES(FALSE)
					SET_BIT(MPTVLocal.iBitset,MPTV_LOCAL_BS_PAUSED_RENDERPHASE)
					IF NOT bCCTVToCCTV
						CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
					ENDIF
				ENDIF
			
				MPTVLocal.iChannelSwitchTimer = GET_GAME_TIMER()
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === fade out")
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === starting clean up of ", GET_MP_TV_CHANNEL_NAME(MPTVLocal.eSwitchChannelFrom))
				#ENDIF
				SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_CLEANUP_CURRENT)
			ENDIF
		BREAK
		CASE MP_TV_SWITCH_STATE_CLEANUP_CURRENT
			//IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_ON)
			//	bFinished = TRUE
			//ELSE
				SWITCH MPTVLocal.eSwitchChannelFrom
					CASE MP_TV_CHANNEL_CCTV_0
					CASE MP_TV_CHANNEL_CCTV_1
					CASE MP_TV_CHANNEL_CCTV_2
					CASE MP_TV_CHANNEL_CCTV_GARAGE
						//UNHIDE_SPECTATOR_PLAYER(MPTVData.specData.specCamData, TRUE)
						IF NOT bFullscreenToFullscreen
							println("CLEANUP_CURRENT (to nonfullscreen) - TIMECYCLE OFF")
							CLEAR_TIMECYCLE_MODIFIER()
						ENDIF
						
						IF NOT bCCTVToCCTV
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							IF DOES_CAM_EXIST(MPTVLocal.camCCTV)
								DESTROY_CAM(MPTVLocal.camCCTV)
							ENDIF
						ENDIF
						
						MP_TV_SOUND_STOP_CCTV_BACKGROUND(MPTVLocal)
						
						IF MPTVLocal.eSwitchChannelTo = MP_TV_CHANNEL_SPECTATE
						AND MPTVLocal.eSwitchChannelTo = MP_TV_CHANNEL_NEWS
							CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
							SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(MPTVLocal.sfTV)
						ENDIF
						
						IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)
							CLEAR_FOCUS()
							println("MP_TV_SWITCH_STATE_CLEANUP_CURRENT - CLEAR_FOCUS")
							NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
							CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)
						ENDIF
						
						IF MPTVLocal.eSwitchChannelFrom = MP_TV_CHANNEL_CCTV_GARAGE
							IF IS_VALID_INTERIOR(MPTVLocal.interiorGarage)
								UNPIN_INTERIOR(MPTVLocal.interiorGarage)
							ENDIF
						ENDIF
						
						STOP_AUDIO_SCENE("MP_CCTV_SCENE")
						
						CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_REVEAL_BUILDING_EXTERIOR)
						
						bFinished = TRUE
					BREAK
					CASE MP_TV_CHANNEL_SPECTATE
					CASE MP_TV_CHANNEL_NEWS
						IF NOT bFullscreenToFullscreen
							println("SWITCH FROM CCTV TO NEWS - TIMECYCLE OFF")
							CLEAR_TIMECYCLE_MODIFIER()
						ENDIF
						IF IS_THIS_SPECTATOR_CAM_ACTIVE(MPTVLocal.specData.specCamData)
							ENABLE_SPEC_CAN_FIND_NEW_FOCUS()
							DEACTIVATE_SPECTATOR_CAM(MPTVLocal.specData, DSCF_DONT_CLEANUP_TIMECYCLES)
						ELSE
							IF IS_THIS_SPECTATOR_CAM_OFF(MPTVLocal.specData.specCamData)
								
								RENDER_SCRIPT_CAMS(FALSE, FALSE)
								IF DOES_CAM_EXIST(MPTVLocal.camCCTV)
									DESTROY_CAM(MPTVLocal.camCCTV)
								ENDIF
								
								IF IS_MP_TV_CHANNEL_CCTV(MPTVLocal.eSwitchChannelTo)
									CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
									SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(MPTVLocal.sfTV)
								ENDIF
								
								IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)
									CLEAR_FOCUS()
									println("MP_TV_CHANNEL_SPECTATE or MP_TV_CHANNEL_NEWS - CLEAR_FOCUS")
									NETWORK_SET_IN_FREE_CAM_MODE(FALSE)
									CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)
								ENDIF
								
								bFinished = TRUE
							ENDIF
						ENDIF
					BREAK
					CASE MP_TV_CHANNEL_CNT
					CASE MP_TV_CHANNEL_WEAZEL
					CASE MP_TV_CHANNEL_ARCADE
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						bFinished = TRUE
					BREAK
				ENDSWITCH
			//ENDIF
			IF bFinished
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === finished clean up of ", GET_MP_TV_CHANNEL_NAME(MPTVLocal.eSwitchChannelFrom))
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === starting setup of ", GET_MP_TV_CHANNEL_NAME(MPTVLocal.eSwitchChannelTo))
				#ENDIF
				
				IF MPTVLocal.eSwitchChannelTo = MP_TV_CHANNEL_SPECTATE
					POPULATE_SPECTATOR_CAM_TARGET_LISTS_INSTANTLY(MPTVLocal.specData)
				ENDIF
				
				SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_SETUP_DESIRED)
			ENDIF
		BREAK
		CASE MP_TV_SWITCH_STATE_SETUP_DESIRED
			IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_OFF)
			OR IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_DEACTIVATING)
				IF CLIENT_SETUP_IN_ROOM_CAMERA(MPTVClient, MPTVLocal, property)
					IF IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eSwitchChannelFrom)
						PRINTLN("CASE MP_TV_SWITCH_STATE_SETUP_DESIRED - calling CLEANUP_RENDERTARGET")
						CLEANUP_RENDERTARGET(MPTVLocal, property,MPTVClient,FALSE)
						SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_LOAD_SCENE_REQUIRED)
						MPTVLocal.vLoadSceneLocation = GET_CAM_COORD(MPTVLocal.camRoom)
						MPTVLocal.vLoadSceneRot = GET_CAM_ROT(MPTVLocal.camRoom)
						SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_REQUEST_APARTMENT_AUDIO_SCENE)
					ENDIF
					SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(MPTVLocal.sfTV)
					CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
					bFinished = TRUE
				ELSE
					PRINTLN("CASE MP_TV_SWITCH_STATE_SETUP_DESIRED - waiting for CLIENT_SETUP_IN_ROOM_CAMERA- 1")
				ENDIF
			ELSE
				SWITCH MPTVLocal.eSwitchChannelTo
					CASE MP_TV_CHANNEL_CCTV_0
					CASE MP_TV_CHANNEL_CCTV_1
					CASE MP_TV_CHANNEL_CCTV_2
					CASE MP_TV_CHANNEL_CCTV_GARAGE
						
						ENABLE_MOVIE_SUBTITLES(FALSE)
						
						MPTVLocal.sfTV = REQUEST_SCALEFORM_MOVIE("SECURITY_CAM")
						
						IF HAS_SCALEFORM_MOVIE_LOADED(MPTVLocal.sfTV)
							BEGIN_SCALEFORM_MOVIE_METHOD(MPTVLocal.sfTV, "SET_LOCATION")
								IF MPTVLocal.eSwitchChannelTo = MP_TV_CHANNEL_CCTV_GARAGE
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPTV_CCTV3")
								ELSE
									IF MPTVLocal.eSwitchChannelTo = MP_TV_CHANNEL_CCTV_1
										IF IS_PROPERTY_OFFICE(property.iIndex)
											SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPTV_CCTV7")
										ELSE
										
											SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPTV_CCTV6")
										ENDIF
									ELSE
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPTV_CCTV0")
									ENDIF
								ENDIF
							END_SCALEFORM_MOVIE_METHOD()
							BEGIN_SCALEFORM_MOVIE_METHOD(MPTVLocal.sfTV, "SET_DETAILS")
								IF MPTVLocal.eSwitchChannelTo = MP_TV_CHANNEL_CCTV_GARAGE
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPTV_CCTV4")
								ELSE
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPTV_CCTV1")
								ENDIF
							END_SCALEFORM_MOVIE_METHOD()	
							BEGIN_SCALEFORM_MOVIE_METHOD(MPTVLocal.sfTV, "SET_TIME")	//calling without params so it is hidden
								/*SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_HOURS())//hours
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_MINUTES())//mins
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_SECONDS())//seconds
								IF MPTVLocal.eSwitchChannelTo = MP_TV_CHANNEL_CCTV_GARAGE
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPTV_CCTV5")
								ELSE
									SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("MPTV_CCTV2")
								ENDIF*/
							END_SCALEFORM_MOVIE_METHOD()
							
							iEntrance = ENUM_TO_INT(MPTVLocal.eSwitchChannelTo)
							#IF IS_DEBUG_BUILD
							//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === iEntrance = ", ENUM_TO_INT(MPTVLocal.eSwitchChannelTo))
							#ENDIF
							
							IF MPTVLocal.eSwitchChannelTo = MP_TV_CHANNEL_CCTV_GARAGE
								IF property.iGarageSize <> 0
									thisCCTV = property.garage.cctv
									iEntranceType = ENTRANCE_TYPE_GARAGE
									#IF IS_DEBUG_BUILD
									//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === iEntranceType = ENTRANCE_TYPE_GARAGE")
									#ENDIF
								ENDIF
							ELSE
								IF property.entrance[iEntrance].iType <> 0
									thisCCTV = property.cctv[iEntrance]
									iEntranceType = property.entrance[iEntrance].iType
									#IF IS_DEBUG_BUILD
									//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === iEntranceType = ", property.entrance[iEntrance].iType)
									#ENDIF
								ENDIF
							ENDIF
							
							IF iEntranceType <> 0
							AND thisCCTV.vPos.x <> 0.0
							AND thisCCTV.vPos.y <> 0.0
							AND thisCCTV.vPos.z <> 0.0
							AND thisCCTV.fFOV <> 0.0
								
								IF DOES_CAM_EXIST(MPTVLocal.camCCTV)
									DESTROY_CAM(MPTVLocal.camCCTV)
								ENDIF
								
								IF NOT bFullscreenToFullscreen
									println("SETUP_DESIRED - TIMECYCLE ON")
									SET_TIMECYCLE_MODIFIER("CAMERA_secuirity")
								ENDIF
								
								SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_LOAD_SCENE_REQUIRED)
								MPTVLocal.vLoadSceneLocation = thisCCTV.vPos
								MPTVLocal.camCCTV = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
								SET_CAM_COORD(MPTVLocal.camCCTV, thisCCTV.vPos)
								SET_CAM_ROT(MPTVLocal.camCCTV, thisCCTV.vRot)
								SET_CAM_FOV(MPTVLocal.camCCTV, thisCCTV.fFOV)
								SET_CAM_ACTIVE(MPTVLocal.camCCTV, TRUE)
								RENDER_SCRIPT_CAMS (TRUE, FALSE)
								
								SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLE_EXTERIOR_FOCUS)
								SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_STATIC_OUTSIDE_CAM_ON)
								SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_REVEAL_BUILDING_EXTERIOR)
								PRINTLN("CASE MP_TV_CHANNEL_CCTV_ - calling CLEANUP_RENDERTARGET")
								CLEANUP_RENDERTARGET(MPTVLocal, property,MPTVClient)
								
								IF MPTVLocal.eSwitchChannelFrom <> MP_TV_CHANNEL_SPECTATE
								AND MPTVLocal.eSwitchChannelFrom <> MP_TV_CHANNEL_NEWS
									SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
								ENDIF
								
								IF IS_AUDIO_SCENE_ACTIVE("MP_APARTMENT_SCENE")
									STOP_AUDIO_SCENE("MP_APARTMENT_SCENE")
									#IF IS_DEBUG_BUILD
									//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === STOP_AUDIO_SCENE(MP_APARTMENT_SCENE)")
									#ENDIF
								ENDIF
								IF IS_AUDIO_SCENE_ACTIVE("MP_GARAGE_SCENE")
									STOP_AUDIO_SCENE("MP_GARAGE_SCENE")
									#IF IS_DEBUG_BUILD
									//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === STOP_AUDIO_SCENE(MP_GARAGE_SCENE)")
									#ENDIF
								ENDIF
								START_AUDIO_SCENE("MP_CCTV_SCENE")
								
								bFinished = TRUE
							ELSE
								PRINTLN("CASE MP_TV_SWITCH_STATE_SETUP_DESIRED - waiting for positions to not be 0")
							ENDIF
						ELSE
							PRINTLN("CASE MP_TV_SWITCH_STATE_SETUP_DESIRED - waiting for scaleform Movie- 1")
						ENDIF
						
					BREAK
					CASE MP_TV_CHANNEL_SPECTATE
					
						ENABLE_MOVIE_SUBTITLES(FALSE)
					
						IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(MPTVLocal.specData.specCamData)
						AND IS_ANY_PLAYER_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER(SPEC_MODE_TV_CHANNEL)
							IF IS_BIT_SET(MPTVLocal.specData.specCamData.iBitSet, SPEC_CAM_BS_DEACTIVATED_DUE_TO_SKYSWOOP_UP)
								IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(MPTVLocal.specData.specCamData)
									DEACTIVATE_SPECTATOR_CAM(MPTVLocal.specData)	//, DSCF_DONT_CLEANUP_TIMECYCLES)
								ENDIF
								#IF IS_DEBUG_BUILD
								CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === Deactivating spectator cam for SPEC_CAM_BS_DEACTIVATED_DUE_TO_SKYSWOOP_UP")
								#ENDIF
								bFinished = TRUE
							ELSE
								IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)
									CLEAR_FOCUS()
									println("MP_TV_CHANNEL_SPECTATE - CLEAR_FOCUS")
									CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)
									#IF IS_DEBUG_BUILD
									//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === Clear focus for spectate CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)")
									#ENDIF
								ENDIF
								ACTIVATE_SPECTATOR_CAM(MPTVLocal.specData, SPEC_MODE_TV_CHANNEL, NULL, ASCF_ONLY_FIND_TARGET_IF_PLAYER_LIST_POPULATED)
							ENDIF
						ELSE
							IF IS_THIS_SPECTATOR_CAM_RUNNING(MPTVLocal.specData.specCamData)
								SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLE_EXTERIOR_FOCUS)
								SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_REVEAL_BUILDING_EXTERIOR)
								PRINTLN("CASE MP_TV_CHANNEL_SPECTATE - calling CLEANUP_RENDERTARGET")
								CLEANUP_RENDERTARGET(MPTVLocal, property,MPTVClient)
								
								IF IS_AUDIO_SCENE_ACTIVE("MP_APARTMENT_SCENE")
									STOP_AUDIO_SCENE("MP_APARTMENT_SCENE")
									#IF IS_DEBUG_BUILD
									//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === STOP_AUDIO_SCENE(MP_APARTMENT_SCENE)")
									#ENDIF
								ENDIF
								IF IS_AUDIO_SCENE_ACTIVE("MP_GARAGE_SCENE")
									STOP_AUDIO_SCENE("MP_GARAGE_SCENE")
									#IF IS_DEBUG_BUILD
									//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === STOP_AUDIO_SCENE(MP_GARAGE_SCENE)")
									#ENDIF
								ENDIF
								bFinished = TRUE
							ELSE
								IF NOT IS_ANY_PLAYER_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER(SPEC_MODE_TV_CHANNEL)
									#IF IS_DEBUG_BUILD
									//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === NO VALID SPECTATOR TARGET FOUND - bFinished = TRUE")
									#ENDIF
									IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(MPTVLocal.specData.specCamData)
										DEACTIVATE_SPECTATOR_CAM(MPTVLocal.specData)	//, DSCF_DONT_CLEANUP_TIMECYCLES)
									ENDIF
									bFinished = TRUE
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE MP_TV_CHANNEL_NEWS
					
						
							ENABLE_MOVIE_SUBTITLES(FALSE)
						
						IF NATIVE_TO_INT(GlobalServerBD_FM.playerMostWanted) <> -1		//If server target is valid, but not in the same tutorial as player
						AND IS_NET_PLAYER_OK(GlobalServerBD_FM.playerMostWanted)
							IF NOT IS_THIS_SPECTATOR_CAM_ACTIVE(MPTVLocal.specData.specCamData)
								IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)
									CLEAR_FOCUS()
									println("MP_TV_CHANNEL_NEWS - CLEAR_FOCUS")
									CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)
									#IF IS_DEBUG_BUILD
									//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === Clear focus for news CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)")
									#ENDIF
								ENDIF
								ACTIVATE_SPECTATOR_CAM(MPTVLocal.specData, SPEC_MODE_NEWS, GET_PLAYER_PED(GlobalServerBD_FM.playerMostWanted))
							ELSE
								IF IS_THIS_SPECTATOR_CAM_RUNNING(MPTVLocal.specData.specCamData)
								
									MPTVLocal.sfTV = REQUEST_SCALEFORM_MOVIE("BREAKING_NEWS")
									IF HAS_SCALEFORM_MOVIE_LOADED(MPTVLocal.sfTV)
											IF NOT IS_PAUSE_MENU_ACTIVE()
										REFRESH_MP_TV_NEWS_INFO(MPTVLocal)
											ENDIF
										DISPLAY_RADAR(FALSE)
					
										SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLE_EXTERIOR_FOCUS)
										SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_REVEAL_BUILDING_EXTERIOR)
										PRINTLN("CASE MP_TV_CHANNEL_NEWS - calling CLEANUP_RENDERTARGET")
										CLEANUP_RENDERTARGET(MPTVLocal, property,MPTVClient)
										
										IF NOT IS_MP_TV_CHANNEL_CCTV(MPTVLocal.eSwitchChannelFrom)
											SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
										ENDIF
										
										IF IS_AUDIO_SCENE_ACTIVE("MP_APARTMENT_SCENE")
											STOP_AUDIO_SCENE("MP_APARTMENT_SCENE")
											#IF IS_DEBUG_BUILD
											//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === STOP_AUDIO_SCENE(MP_APARTMENT_SCENE)")
											#ENDIF
										ENDIF
										IF IS_AUDIO_SCENE_ACTIVE("MP_GARAGE_SCENE")
											STOP_AUDIO_SCENE("MP_GARAGE_SCENE")
											#IF IS_DEBUG_BUILD
											//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === STOP_AUDIO_SCENE(MP_GARAGE_SCENE)")
											#ENDIF
										ENDIF
										
										bFinished = TRUE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							#IF IS_DEBUG_BUILD
							//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === News target is invalid, bFinished = TRUE")
							#ENDIF
							IF IS_THIS_SPECTATOR_CAM_ACTIVE(MPTVLocal.specData.specCamData)
								FORCE_CLEANUP_SPECTATOR_CAM(MPTVLocal.specData)
							ENDIF
							bFinished = TRUE
						ENDIF
					BREAK
					CASE MP_TV_CHANNEL_CNT
					CASE MP_TV_CHANNEL_WEAZEL
					CASE MP_TV_CHANNEL_ARCADE
						ENABLE_MOVIE_SUBTITLES(TRUE)
					
						IF CLIENT_SETUP_IN_ROOM_CAMERA(MPTVClient, MPTVLocal, property)
							IF IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eSwitchChannelFrom)
								SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_LOAD_SCENE_REQUIRED)
								MPTVLocal.vLoadSceneLocation = GET_CAM_COORD(MPTVLocal.camRoom)
								MPTVLocal.vLoadSceneRot = GET_CAM_ROT(MPTVLocal.camRoom)
								PRINTLN("CASE MP_TV_CHANNEL_CNT - calling CLEANUP_RENDERTARGET")
								CLEANUP_RENDERTARGET(MPTVLocal, property,MPTVClient)
								SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_REQUEST_APARTMENT_AUDIO_SCENE)
							ENDIF
							SET_CAM_ACTIVE(MPTVLocal.camRoom, TRUE)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
							SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(MPTVLocal.sfTV)
							CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
							
							bFinished = TRUE
						ELSE
							PRINTLN("CASE MP_TV_SWITCH_STATE_SETUP_DESIRED - waiting for CLIENT_SETUP_IN_ROOM_CAMERA- 2")
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			IF bFinished
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === finished setup of ", GET_MP_TV_CHANNEL_NAME(MPTVLocal.eSwitchChannelTo))
				#ENDIF
				SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_WAIT_FOR_FILTER_TIME)
			ENDIF
		BREAK
		
		CASE MP_TV_SWITCH_STATE_WAIT_FOR_FILTER_TIME
			IF GET_GAME_TIMER() - MPTVLocal.iChannelSwitchTimer > SPEC_HUD_FILTER_CHANGE_TIME
				IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_LOAD_SCENE_REQUIRED)
					SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_SETUP_LOAD_SCENE)
				ELSE
					SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_FADE_IN)
				ENDIF 
			ENDIF
		BREAK
		
		
		CASE MP_TV_SWITCH_STATE_SETUP_LOAD_SCENE
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === MPTV_BS_LOAD_SCENE_REQUIRED = TRUE, doing load scene")
			#ENDIF
			
			IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_LOAD_SCENE_RUNNING)
				NEW_LOAD_SCENE_STOP()
			ENDIF
			IF MPTVLocal.vLoadSceneRot.x = 0.0
			AND MPTVLocal.vLoadSceneRot.y = 0.0
			AND MPTVLocal.vLoadSceneRot.z = 0.0
				NEW_LOAD_SCENE_START_SPHERE(MPTVLocal.vLoadSceneLocation, 50.0)
			ELSE
				NEW_LOAD_SCENE_START(MPTVLocal.vLoadSceneLocation, MPTVLocal.vLoadSceneRot, 300)
			ENDIF
			SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_LOAD_SCENE_RUNNING)
			
			SET_FOCUS_POS_AND_VEL(MPTVLocal.vLoadSceneLocation, <<0,0,0>>)
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === MP_TV_SWITCH_STATE_SETUP_LOAD_SCENE - SET_FOCUS_POS_AND_VEL ", MPTVLocal.vLoadSceneLocation)
			NETWORK_SET_IN_FREE_CAM_MODE(TRUE)
			SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FOCUS_SET)
			
			MPTVLocal.iChannelSwitchTimer = GET_GAME_TIMER()		//iChannelSwitchTimer now used as safety net for loading
			SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_WAIT_FOR_LOAD_SCENE)
		BREAK
		CASE MP_TV_SWITCH_STATE_WAIT_FOR_LOAD_SCENE
		
			IF IS_NEW_LOAD_SCENE_LOADED()
			OR GET_GAME_TIMER() - MPTVLocal.iChannelSwitchTimer > MP_TV_LOAD_SCENE_TIMEOUT
				IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_LOAD_SCENE_RUNNING)
					NEW_LOAD_SCENE_STOP()
					CLEAR_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_LOAD_SCENE_RUNNING)
				ENDIF
				
				IF MPTVLocal.eSwitchChannelTo = MP_TV_CHANNEL_CCTV_GARAGE
				AND NOT IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === load complete or wait for timer")
					#ENDIF
					MPTVLocal.iChannelSwitchTimer = GET_GAME_TIMER()		//iChannelSwitchTimer now used as safety net for loading
					SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_LOAD_INTERIOR)
				ELIF IS_PLAYER_IN_HANGAR(PLAYER_ID())
				AND (MPTVLocal.eSwitchChannelTo = MP_TV_CHANNEL_CNT
				OR MPTVLocal.eSwitchChannelTo = MP_TV_CHANNEL_WEAZEL
				OR MPTVLocal.eSwitchChannelTo = MP_TV_CHANNEL_ARCADE)
				AND IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eSwitchChannelFrom)
					MPTVLocal.iChannelSwitchTimer = GET_GAME_TIMER()
					SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_LOAD_INTERIOR)
				ELSE
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === load complete or wait for timer")
					#ENDIF
					SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_CLEANUP_LOAD_SCENE)
				ENDIF
			ENDIF
		BREAK
		CASE MP_TV_SWITCH_STATE_LOAD_INTERIOR	
			MPTVLocal.interiorGarage = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_GARAGE_INTERIOR_LOAD_VECTOR(property), 
																		GET_GARAGE_INTERIOR_LOAD_INTERIOR_NAME(property))
			IF GET_GAME_TIMER() - MPTVLocal.iChannelSwitchTimer > MP_TV_LOAD_INTERIOR_TIMEOUT
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === interior load timed out")
				#ENDIF
				SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_CLEANUP_LOAD_SCENE)
			ELSE
				IF IS_VALID_INTERIOR(MPTVLocal.interiorGarage)
					PIN_INTERIOR_IN_MEMORY(MPTVLocal.interiorGarage)
					IF IS_INTERIOR_READY(MPTVLocal.interiorGarage)
						SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME(GET_GARAGE_INTERIOR_LOAD_ROOM_NAME(property))
						IF IS_INTERIOR_SCENE()
							#IF IS_DEBUG_BUILD
							//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === interior load complete")
							#ENDIF
							SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_CLEANUP_LOAD_SCENE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE MP_TV_SWITCH_STATE_CLEANUP_LOAD_SCENE
			bFinished = FALSE
			IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_OFF)
			OR IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_DEACTIVATING)
			OR (IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eSwitchChannelFrom) AND (NOT IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eSwitchChannelTo)))
				IF CLIENT_SETUP_IN_ROOM_CAMERA(MPTVClient, MPTVLocal, property, FALSE)
					SET_CAM_ACTIVE(MPTVLocal.camRoom, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLE_EXTERIOR_FOCUS)
					CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_STATIC_OUTSIDE_CAM_ON)
					SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_REQUEST_APARTMENT_AUDIO_SCENE)
					bFinished = TRUE
				ENDIF
			ELSE
				bFinished = TRUE
			ENDIF
			IF bFinished = TRUE
				CLEAR_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_LOAD_SCENE_REQUIRED)
				MPTVLocal.vLoadSceneLocation = <<0,0,0>>
				MPTVLocal.vLoadSceneRot = <<0,0,0>>
				RESET_ADAPTATION()
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === load scene cleaned up")
				#ENDIF
				SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_SETUP_RENDERTARGET)
			ENDIF
		BREAK
			CASE MP_TV_SWITCH_STATE_SETUP_RENDERTARGET
			bFinished = FALSE
			
			SWITCH MPTVLocal.eSwitchChannelTo
				CASE MP_TV_CHANNEL_CCTV_0
				CASE MP_TV_CHANNEL_CCTV_1
				CASE MP_TV_CHANNEL_CCTV_2
				CASE MP_TV_CHANNEL_SPECTATE
				CASE MP_TV_CHANNEL_NEWS
					IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_OFF)
					OR IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_DEACTIVATING)
						IF NOT IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
						AND NOT IS_PROPERTY_STILT_APARTMENT(property.iIndex)
						AND NOT IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
						AND NOT IS_PROPERTY_OFFICE(property.iIndex)
						AND NOT IS_PROPERTY_CLUBHOUSE(property.iIndex)
						AND NOT IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
						AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
						AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
						AND NOT MPTVLocal.bInCasinoApartment
						#IF FEATURE_CASINO_HEIST
						AND NOT MPTVLocal.bInArcade
						#ENDIF
						#IF FEATURE_FIXER
						AND NOT MPTVLocal.bInFixerHQ
						#ENDIF
						OR IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
							CDEBUG1LN(DEBUG_MP_TV, "MP_TV_SWITCH_STATE_SETUP_RENDERTARGET: Trying to SETUP_RENDERTARGET")
							IF SETUP_RENDERTARGET(MPTVClient, MPTVLocal, property)
								bFinished = TRUE
							ENDIF
						ELSE
							CDEBUG1LN(DEBUG_MP_TV, "MP_TV_SWITCH_STATE_SETUP_RENDERTARGET: Trying to SETUP_RENDERTARGET_FROM_OBJECT")
							IF SETUP_RENDERTARGET_FROM_OBJECT(MPTVClient, MPTVLocal, property)
								bFinished = TRUE
							ENDIF
						ENDIF
					ELSE
						bFinished = TRUE
					ENDIF
				BREAK
				
				CASE MP_TV_CHANNEL_CCTV_GARAGE
				CASE MP_TV_CHANNEL_CNT
				CASE MP_TV_CHANNEL_WEAZEL
				CASE MP_TV_CHANNEL_ARCADE
					IF NOT IS_PROPERTY_YACHT_APARTMENT(property.iIndex)
					AND NOT IS_PROPERTY_STILT_APARTMENT(property.iIndex)
					AND NOT IS_PROPERTY_CUSTOM_APARTMENT(property.iIndex)
					AND NOT IS_PROPERTY_OFFICE(property.iIndex)
					AND NOT IS_PROPERTY_CLUBHOUSE(property.iIndex)
					AND NOT IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
					AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
					AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
					AND NOT MPTVLocal.bInCasinoApartment
					#IF FEATURE_CASINO_HEIST
					AND NOT MPTVLocal.bInArcade
					#ENDIF
					#IF FEATURE_FIXER
					AND NOT MPTVLocal.bInFixerHQ
					#ENDIF
					OR IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
						CDEBUG1LN(DEBUG_MP_TV, "MP_TV_SWITCH_STATE_SETUP_RENDERTARGET: Trying to SETUP_RENDERTARGET")
						IF SETUP_RENDERTARGET(MPTVClient, MPTVLocal, property)
							bFinished = TRUE
						ENDIF
					ELSE
						CDEBUG1LN(DEBUG_MP_TV, "MP_TV_SWITCH_STATE_SETUP_RENDERTARGET: Trying to SETUP_RENDERTARGET_FROM_OBJECT")
						IF SETUP_RENDERTARGET_FROM_OBJECT(MPTVClient, MPTVLocal, property)
							bFinished = TRUE
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			
			IF bFinished
				SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_CLEAR_DISPLAY)
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_CLEAR_DISPLAY)")
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === render target set up")
				#ENDIF
				SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_FADE_IN)
			ENDIF
		BREAK
		
		CASE MP_TV_SWITCH_STATE_FADE_IN
			
			IF (NOT IS_SCREEN_FADED_IN())
				IF (NOT IS_SCREEN_FADING_IN())
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === DO_SCREEN_FADE_IN(0)")
					#ENDIF
					IF bFullscreenToFullscreen
						MP_TV_FADE_IN(MPTVLocal,0)
					ELSE
						IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_OFF)
						OR IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_DEACTIVATING)
						OR (IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eSwitchChannelFrom) AND (NOT IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eSwitchChannelTo)))
							MP_TV_FADE_IN(MPTVLocal,MP_TV_FADE_TIME_POWER_CHANGE)
						ELSE
							MP_TV_FADE_IN(MPTVLocal,MP_TV_FADE_TIME_CHANNEL_CHANGE)
						ENDIF
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_CLEAR_DISPLAY)")
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === fade in")
				#ENDIF
				SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_CLEANUP_PROCESS)
			ENDIF
		BREAK
		CASE MP_TV_SWITCH_STATE_CLEANUP_PROCESS
			
			MP_TV_SOUND_STOP_CCTV_CHANGE_CAM(MPTVLocal)
			
			IF IS_MP_TV_CHANNEL_CCTV(MPTVLocal.eSwitchChannelTo)
			//AND (IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_ON)
			//	OR (IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_ACTIVATING) AND MPTVServer.eStage = MP_TV_SERVER_STAGE_ON))
			//AND MPTVServer.eStage = MP_TV_SERVER_STAGE_ON
			//AND NOT (IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_OFF)
			//		OR IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_DEACTIVATING))
			AND NOT (IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_OFF)
					OR IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_DEACTIVATING)
					OR (
					(NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE) AND MPTVServer.eStage = MP_TV_SERVER_STAGE_OFF )
					OR (IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE) AND MPTVServer.eGarageStage = MP_TV_SERVER_STAGE_OFF))
					)
					
				println("CLEANUP_PROCESS - TIMECYCLE ON")
				SET_TIMECYCLE_MODIFIER("CAMERA_secuirity")
				SET_NOISINESSOVERIDE(0.1)
				
				MP_TV_SOUND_START_CCTV_BACKGROUND(MPTVLocal)
				
			ELSE
				println("CLEANUP_PROCESS - TIMECYCLE OFF")
				CLEAR_TIMECYCLE_MODIFIER()
				SET_NOISINESSOVERIDE(0.0)
				SET_NOISEOVERIDE(FALSE)
			ENDIF
			
			TOGGLE_RENDERPHASES(TRUE)
		
			CLEAR_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_LOAD_SCENE_REQUIRED)
			MPTVLocal.vLoadSceneLocation = <<0,0,0>>
			MPTVLocal.vLoadSceneRot = <<0,0,0>>
						
			SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_UPDATE_BUTTONS)
			
			CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DRAW_LOADING_ICON)
			REFRESH_SCALEFORM_LOADING_ICON(MPTVLocal.loadingIcon, TRUE)
			
			ENABLE_SPECTATOR_FADES()
			SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_UPDATE_BUTTONS)
			
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL SWITCH === set current to desired")
			#ENDIF
			
			SET_MP_TV_SWITCH_STATE(MPTVLocal, MP_TV_SWITCH_STATE_WATCH_CHANNEL)
			
		BREAK
		CASE MP_TV_SWITCH_STATE_WATCH_CHANNEL
			
			IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_FULLSCREEN_TV_TEXT)
				IF IS_MP_TV_CHANNEL_FULLSCREEN(MPTVLocal.eCurrentChannel)
					//MP_MAGDEMO RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention)
					//MP_MAGDEMO PRINT_HELP("MPTV_HLP2")//Some channels are displayed full screen.
					SET_MP_BOOL_CHARACTER_STAT(MP_STAT_FULLSCREEN_TV_TEXT, TRUE)
				ENDIF
			ENDIF
		
			SWITCH MPTVLocal.eCurrentChannel
				CASE MP_TV_CHANNEL_CCTV_0
				CASE MP_TV_CHANNEL_CCTV_1
				CASE MP_TV_CHANNEL_CCTV_2
				CASE MP_TV_CHANNEL_CCTV_GARAGE
					IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
						SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
					ENDIF
				BREAK
				CASE MP_TV_CHANNEL_SPECTATE
					ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPT_SELECT)
				BREAK
				CASE MP_TV_CHANNEL_NEWS
					IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
						SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
					ENDIF
				
					IF IS_THIS_SPECTATOR_CAM_ACTIVE(MPTVLocal.specData.specCamData)
						IF IS_THIS_SPECTATOR_CAM_RUNNING(MPTVLocal.specData.specCamData)
							
							IF NATIVE_TO_INT(GlobalServerBD_FM.playerMostWanted) <> -1
							AND IS_NET_PLAYER_OK(GlobalServerBD_FM.playerMostWanted, FALSE)
								IF GET_SPECTATOR_DESIRED_FOCUS_PED() <> GET_PLAYER_PED(GlobalServerBD_FM.playerMostWanted)
									SET_SPECTATOR_DESIRED_FOCUS_PED(MPTVLocal.specData, GET_PLAYER_PED(GlobalServerBD_FM.playerMostWanted))
								ENDIF
							ENDIF
							
							IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_W_WANTED_PLAYER_TV)
								SET_MP_BOOL_CHARACTER_STAT(MP_STAT_CL_W_WANTED_PLAYER_TV, TRUE)
							ENDIF
						ENDIF
					ELSE
						/*#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV === SWITCH_TO_PREVIOUS_MP_TV_CHANNEL, NOT ACTIVE")
						#ENDIF
						SWITCH_TO_PREVIOUS_MP_TV_CHANNEL(MPTVData, property)*/
					ENDIF
					
					IF NATIVE_TO_INT(GlobalServerBD_FM.playerMostWanted) = -1
					OR NOT IS_NET_PLAYER_OK(GlobalServerBD_FM.playerMostWanted, TRUE)
						SET_BIT(MPSpecGlobals.iBitset, GLOBAL_SPEC_BS_NEED_TO_PROCESS_LIST_INSTANTLY)
					ENDIF
					
					/*IF NOT ARE_SPECTATOR_HUD_TARGET_LISTS_POPULATED(MPTVData.specData.specHUDData)
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV === SWITCH_TO_PREVIOUS_MP_TV_CHANNEL, NOT POPULATED")
						#ENDIF
						SWITCH_TO_PREVIOUS_MP_TV_CHANNEL(MPTVData, property)
					ENDIF*/
					
				BREAK
			ENDSWITCH
		
			
			IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_ON)
			OR IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_ACTIVATING)
				IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
					SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)")
					#ENDIF
				ENDIF
			ENDIF
		
			
			IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_OFF)
			OR IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_DEACTIVATING)
				IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
					CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_WATCHING_MP_TV)")
					#ENDIF
				ENDIF
			ENDIF
			
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PM_APT_INT_HLP2")
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PM_OFC_INT_HLP2")
				CLEAR_HELP()
			ENDIF
			
			bReturnTrue = TRUE
		BREAK
	ENDSWITCH
	
	
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
	
		IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DRAW_LOADING_ICON)
			RUN_SCALEFORM_LOADING_ICON(MPTVLocal.loadingIcon, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(MPTVLocal.loadingIcon))
		ENDIF
	
		IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_SCALEFORM_DRAW)
				
			IF GET_MP_TV_SWITCH_STATE(MPTVLocal) = MP_TV_SWITCH_STATE_CRIMINAL_LOST	//continue to render hud in criminal lost screen
				IF HAS_SCALEFORM_MOVIE_LOADED(MPTVLocal.sfTV)
					THEFEED_HIDE_THIS_FRAME()
					SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
					SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD_PRIORITY_LOW)
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(MPTVLocal.sfTV, 255,255,255,0)
					RESET_SCRIPT_GFX_ALIGN()
				ENDIF
			ELSE
				SWITCH MPTVLocal.eCurrentChannel
					CASE MP_TV_CHANNEL_CCTV_0
					CASE MP_TV_CHANNEL_CCTV_1
					CASE MP_TV_CHANNEL_CCTV_2
					CASE MP_TV_CHANNEL_CCTV_GARAGE
						
						IF HAS_SCALEFORM_MOVIE_LOADED(MPTVLocal.sfTV)
							//SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
							SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD_PRIORITY_LOW)
							//DRAW_SCALEFORM_MOVIE(MPTVLocal.sfTV, 0.463, 0.450, 0.970, 0.970, 255,255,255,0)
							DRAW_SCALEFORM_MOVIE_FULLSCREEN(MPTVLocal.sfTV, 255,255,255,0)
							RESET_SCRIPT_GFX_ALIGN()
						ENDIF
					BREAK
					CASE MP_TV_CHANNEL_NEWS
						
						IF HAS_SCALEFORM_MOVIE_LOADED(MPTVLocal.sfTV)
						
							THEFEED_HIDE_THIS_FRAME()
							
							IF NOT IS_BIT_SET(MPTVServer.iBitset, MPTV_SERVER_BS_NEWS_CHANNEL_LOST_CRIMINAL)
							
								IF SHOULD_REDO_NEWS_HUD_DUE_TO_EVENT()
									REFRESH_MP_TV_NEWS_INFO(MPTVLocal)
								ENDIF
							
								IF GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPTVLocal.timeNewsFilterNextUpdate) >= MP_TV_NEWS_TICKER_TIME_UPDATE
									MPTVLocal.fNewsFilterScrollEntry++
									
									IF MPTVLocal.fNewsFilterScrollEntry >= MP_TV_NUMBER_OF_NEWS_TICKER
										MPTVLocal.fNewsFilterScrollEntry = 0
									ENDIF
									
									BEGIN_SCALEFORM_MOVIE_METHOD(MPTVLocal.sfTV, "DISPLAY_SCROLL_TEXT")
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(0)
										SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(MPTVLocal.fNewsFilterScrollEntry)
									END_SCALEFORM_MOVIE_METHOD()
									
									MPTVLocal.timeNewsFilterNextUpdate = GET_NETWORK_TIME()
								ENDIF
							ENDIF
						
							SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
							SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD_PRIORITY_LOW)
							DRAW_SCALEFORM_MOVIE_FULLSCREEN(MPTVLocal.sfTV, 255,255,255,0)
							RESET_SCRIPT_GFX_ALIGN()
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bReturnTrue
ENDFUNC

/// PURPOSE:
///    Processes player in full-interaction mode with the tv, able to request channel and volume changes
/// PARAMS:
///    MPTVServer - 
///    MPTVClient - 
///    MPTVLocal - 
///    property - 
/// RETURNS:
///    true when player wants to stop interacting with tv
FUNC BOOL CLIENT_MAINTAIN_WATCHING_TV(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property)
	DISABLE_DPADDOWN_THIS_FRAME()
	
	DISPLAY_AMMO_THIS_FRAME(FALSE)			
	HUD_FORCE_WEAPON_WHEEL(FALSE)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
	SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
		DISABLE_SPECTATOR_CONTROL_ACTIONS_GENERAL(MPTVLocal.specData)
		//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
	ENDIF
	
	DISABLE_SELECTOR_THIS_FRAME()
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
	//HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
	//HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
	//HIDE_HELP_TEXT_THIS_FRAME()
	//THEFEED_HIDE_THIS_FRAME()
	
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
	
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	IF NOT (g_sObjectiveTextControlMP.otcIsPaused)
		Pause_Objective_Text()
	ENDIF
	
	
	IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_WATCH_TV_CLEANUP)
//		CPRINTLN(DEBUG_SPECTATOR, "=== MPTV LOCAL === MPTV_LOCAL_BS_WATCH_TV_CLEANUP FALSE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
		CLIENT_SETUP_IN_ROOM_CAMERA(MPTVClient, MPTVLocal, property)
	
		IF CLIENT_MAINTAIN_CURRENT_WATCHED_CHANNEL(MPTVServer, MPTVClient, MPTVLocal, property)
			IF CLIENT_MAINTAIN_MP_TV_INPUT(MPTVServer, MPTVClient, MPTVLocal, property)
			OR bInPropertyOwnerNotPaidLastUtilityBill
			OR g_bCelebrationScreenIsActive
			OR IS_BIT_SET(MPSpecGlobals.iBailBitset, GLOBAL_SPEC_BAIL_BS_STOP_MPTV_SPECTATE)
				CLIENT_SETUP_TV_DEACTIVATING(MPTVLocal, MPTVLocal.eCurrentChannel)
				SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_WATCH_TV_CLEANUP)
				#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_WATCH_TV_CLEANUP)")
				#ENDIF
			ENDIF
		ELSE
			IF MPTVClient.iInputBitset <> 0		//reset control requests when setting up channel
				MPTVClient.iInputBitset = 0
			ENDIF
		ENDIF
	ELSE
//		CPRINTLN(DEBUG_SPECTATOR, "=== MPTV LOCAL === MPTV_LOCAL_BS_WATCH_TV_CLEANUP TRUE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
		IF CLIENT_MAINTAIN_CURRENT_WATCHED_CHANNEL(MPTVServer, MPTVClient, MPTVLocal, property)	//wait for current watched to turn off
			IF IS_PROPERTY_CLUBHOUSE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty, PROPERTY_CLUBHOUSE_1_BASE_A)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(200)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-10)
			ELSE
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			CLIENT_CLEANUP_IN_ROOM_CAMERA(MPTVLocal)
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
			ENDIF
			
			CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_WATCH_TV_CLEANUP)
			#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_WATCH_TV_CAMERA_SETUP)")
				//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV LOCAL === CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_WATCH_TV_CLEANUP)")
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Server sets the channel that the property tv is watching
/// PARAMS:
///    MPTVServer - 
///    thisChannel - desired channel
///    bGarage - true if tv is in garage
PROC SET_SERVER_MP_TV_CHANNEL(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CHANNEL thisChannel, BOOL bGarage)
	IF bGarage
		MPTVServer.eCurrentGarageChannel = thisChannel
		#IF IS_DEBUG_BUILD
		//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV SERVER === MPTVServer.eCurrentGarageChannel = ", GET_MP_TV_CHANNEL_NAME(MPTVServer.eCurrentGarageChannel))
		#ENDIF
	ELSE
		MPTVServer.eCurrentChannel = thisChannel
		
		CDEBUG1LN(DEBUG_MP_TV, "=== MPTV SERVER === MPTVServer.eCurrentChannel = ", GET_MP_TV_CHANNEL_NAME(MPTVServer.eCurrentChannel))
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Server switches to next valid channel
/// PARAMS:
///    MPTVServer - 
///    property - 
///    bGarage - true if in garage
PROC SWITCH_TO_NEXT_MP_TV_CHANNEL(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_PROPERTY_STRUCT &property, BOOL bGarage)
	#IF IS_DEBUG_BUILD
		IF bGarage
			//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV SERVER === SWITCH_TO_NEXT_MP_TV_CHANNEL garage")
		ELSE
			CDEBUG1LN(DEBUG_MP_TV, "=== MPTV SERVER === SWITCH_TO_NEXT_MP_TV_CHANNEL")
		ENDIF
	#ENDIF
	
	INT iCurrentChannelID 
	IF bGarage
		iCurrentChannelID = ENUM_TO_INT(MPTVServer.eCurrentGarageChannel)
	ELSE
		iCurrentChannelID = ENUM_TO_INT(MPTVServer.eCurrentChannel)
	ENDIF
	
	INT iProcess = iCurrentChannelID
	
	INT iSelection = -1
	BOOL bFinished = FALSE
	
	WHILE bFinished = FALSE
		iProcess += 1
		IF iProcess >= NUMBER_OF_MP_TV_CHANNELS
			iProcess = 0
		ENDIF
		IF iProcess = iCurrentChannelID	//If we've looped all the way round
			bFinished = TRUE
		ELSE
			IF CAN_SWITCH_TO_THIS_MP_TV_CHANNEL(MPTVLocal, INT_TO_ENUM(MP_TV_CHANNEL, iProcess), property)
				iSelection = iProcess
				bFinished = TRUE
			ELSE
				CDEBUG1LN(DEBUG_MP_TV, "SWITCH_TO_NEXT_MP_TV_CHANNEL: Skipping Channel: ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, iProcess)))
			ENDIF
		ENDIF
		
	ENDWHILE
	
	IF iSelection <> -1
		SET_SERVER_MP_TV_CHANNEL(MPTVServer, INT_TO_ENUM(MP_TV_CHANNEL, iSelection), bGarage)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Server switches to previous valid channel
/// PARAMS:
///    MPTVServer - 
///    property - 
///    bGarage - true if in garage
PROC SWITCH_TO_PREVIOUS_MP_TV_CHANNEL(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_PROPERTY_STRUCT &property, BOOL bGarage)
	#IF IS_DEBUG_BUILD
		IF bGarage
			CDEBUG1LN(DEBUG_MP_TV, "=== MPTV SERVER === SWITCH_TO_PREVIOUS_MP_TV_CHANNEL garage")
		ELSE
			CDEBUG1LN(DEBUG_MP_TV, "=== MPTV SERVER === SWITCH_TO_PREVIOUS_MP_TV_CHANNEL")
		ENDIF
	#ENDIF
	
	INT iCurrentChannelID 
	IF bGarage
		iCurrentChannelID = ENUM_TO_INT(MPTVServer.eCurrentGarageChannel)
	ELSE
		iCurrentChannelID = ENUM_TO_INT(MPTVServer.eCurrentChannel)
	ENDIF
	
	INT iProcess = iCurrentChannelID
	
	INT iSelection = -1
	BOOL bFinished = FALSE
	
	WHILE bFinished = FALSE
		iProcess -= 1
		IF iProcess < 0
			iProcess = NUMBER_OF_MP_TV_CHANNELS-1
		ENDIF
	
		IF iProcess = iCurrentChannelID	//If we've looped all the way round
			bFinished = TRUE
		ELSE
			IF CAN_SWITCH_TO_THIS_MP_TV_CHANNEL(MPTVLocal, INT_TO_ENUM(MP_TV_CHANNEL, iProcess), property)
				iSelection = iProcess
				bFinished = TRUE
			ENDIF
		ENDIF
		
	ENDWHILE
	
	IF iSelection <> -1
		SET_SERVER_MP_TV_CHANNEL(MPTVServer, INT_TO_ENUM(MP_TV_CHANNEL, iSelection), bGarage)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Server switches to a specific channel if valid
/// PARAMS:
///    MPTVServer - 
///    thisChannel - desired channel
///    property - 
PROC SWITCH_TO_SPECIFIC_MP_TV_CHANNEL(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CHANNEL thisChannel, MP_PROPERTY_STRUCT &property)
	#IF IS_DEBUG_BUILD
	//CDEBUG2LN(DEBUG_MP_TV, "=== MPTV SERVER === SWITCH_TO_SPECIFIC_MP_TV_CHANNEL")
	#ENDIF
	IF CAN_SWITCH_TO_THIS_MP_TV_CHANNEL(MPTVLocal, thisChannel, property)
		SET_SERVER_MP_TV_CHANNEL(MPTVServer, thisChannel)
	ENDIF
ENDPROC

/// PURPOSE:
///    Server checks if the current channel is no longer valid to watch
/// PARAMS:
///    MPTVServer - 
///    MPTVLocal - 
///    property - 
///    bGarage - true if in garage
/// RETURNS:
///    true if channel no longer sutiable
FUNC BOOL SERVER_HAS_CURRENT_MP_TV_CHANNEL_BECOME_UNACCEPTABLE(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property, BOOL bGarage)
	BOOL bNoPlayersToSpectate = FALSE
	MP_TV_CHANNEL thisCurrentServerChannel
	
	IF bGarage
		thisCurrentServerChannel = MPTVServer.eCurrentGarageChannel
	ELSE
		thisCurrentServerChannel = MPTVServer.eCurrentChannel
	ENDIF
	
	IF thisCurrentServerChannel = MP_TV_CHANNEL_NEWS
			
		IF MPTVLocal.iTimeCheckPlayers < GET_GAME_TIMER()
			IF NOT IS_ANY_PLAYER_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER(SPEC_MODE_NEWS)
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV SERVER === News channel, but no-one to watch, SWITCH_TO_PREVIOUS_MP_TV_CHANNEL ")
				#ENDIF
				bNoPlayersToSpectate = TRUE
			ENDIF
			MPTVLocal.iTimeCheckPlayers = GET_GAME_TIMER() + MP_TV_CHECK_PLAYERS_TIME
		ENDIF
	
		IF NOT GlobalServerBD_FM.bMostWantedCriminalSet
		OR bNoPlayersToSpectate
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV SERVER === Wanted player is gone, SWITCH_TO_PREVIOUS_MP_TV_CHANNEL ")
			#ENDIF
			
			SWITCH_TO_PREVIOUS_MP_TV_CHANNEL(MPTVLocal, MPTVServer, property, bGarage)
			
			IF GlobalServerBD_FM.bMostWantedCriminalLost
				IF NOT IS_BIT_SET(MPTVServer.iBitset, MPTV_SERVER_BS_NEWS_CHANNEL_LOST_CRIMINAL)
					SET_BIT(MPTVServer.iBitset, MPTV_SERVER_BS_NEWS_CHANNEL_LOST_CRIMINAL)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV SERVER === SET_BIT(MPTVServer.iBitset, MPTV_SERVER_BS_NEWS_CHANNEL_LOST_CRIMINAL)")
					#ENDIF
				ENDIF
			ENDIF
		
			RETURN TRUE
		ENDIF
		
	ELIF thisCurrentServerChannel = MP_TV_CHANNEL_SPECTATE
	
		IF MPTVLocal.iTimeCheckPlayers < GET_GAME_TIMER()
			IF NOT IS_ANY_PLAYER_ALLOWED_TO_BE_VIEWED_BY_LOCAL_PLAYER(SPEC_MODE_TV_CHANNEL)
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV SERVER === Spectate channel, but no-one to watch, SWITCH_TO_PREVIOUS_MP_TV_CHANNEL ")
				#ENDIF
				bNoPlayersToSpectate = TRUE
			ENDIF
			MPTVLocal.iTimeCheckPlayers = GET_GAME_TIMER() + MP_TV_CHECK_PLAYERS_TIME
		ENDIF
	
		IF bNoPlayersToSpectate
			SWITCH_TO_PREVIOUS_MP_TV_CHANNEL(MPTVLocal, MPTVServer, property, bGarage)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Server sets flags on the frame it recognises the channel has changed
/// PARAMS:
///    MPTVServer - 
///    MPTVLocal - 
///    bGarage - true if in garage
PROC SERVER_HANDLE_MP_TV_CHANNEL_CHANGE_THIS_FRAME(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, BOOL bGarage)
	MP_TV_CHANNEL thisCurrentServerChannel
	
	IF bGarage
		thisCurrentServerChannel = MPTVServer.eCurrentGarageChannel
	ELSE
		thisCurrentServerChannel = MPTVServer.eCurrentChannel
	ENDIF
	
	IF thisCurrentServerChannel = MP_TV_CHANNEL_NEWS
		IF IS_BIT_SET(MPTVServer.iBitset, MPTV_SERVER_BS_NEWS_CHANNEL_LOST_CRIMINAL)
			CLEAR_BIT(MPTVServer.iBitset, MPTV_SERVER_BS_NEWS_CHANNEL_LOST_CRIMINAL)
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV SERVER === CLEAR_BIT(MPTVServer.iBitset, MPTV_SERVER_BS_NEWS_CHANNEL_LOST_CRIMINAL)")
			#ENDIF
		ENDIF
	ENDIF

	IF bGarage
		MPTVLocal.timeServerGarageChannelChange = GET_NETWORK_TIME()
		SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_SERVER_TIMER_GARAGE_CHANNEL_CHANGE_SET)
	ELSE
		MPTVLocal.timeServerChannelChange = GET_NETWORK_TIME()
		SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_SERVER_TIMER_CHANNEL_CHANGE_SET)
	ENDIF
ENDPROC

/// PURPOSE:
///    Server keeps up to date with what the current channel is when the tv is off
/// PARAMS:
///    MPTVServer - 
///    MPTVLocal - 
///    property - 
///    bGarage - 
PROC SERVER_MAINTAIN_CURRENT_MP_TV_CHANNEL_WHEN_OFF(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property, BOOL bGarage)
	IF bGarage
		IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SERVER_TIMER_GARAGE_CHANNEL_CHANGE_SET)
		OR GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPTVLocal.timeServerGarageChannelChange) > MP_TV_CHANNEL_CHANGE_WAIT_TIME
			IF SERVER_HAS_CURRENT_MP_TV_CHANNEL_BECOME_UNACCEPTABLE(MPTVServer, MPTVLocal, property, TRUE)
				SERVER_HANDLE_MP_TV_CHANNEL_CHANGE_THIS_FRAME(MPTVServer, MPTVLocal, TRUE)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SERVER_TIMER_CHANNEL_CHANGE_SET)
		OR GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPTVLocal.timeServerChannelChange) > MP_TV_CHANNEL_CHANGE_WAIT_TIME
			IF SERVER_HAS_CURRENT_MP_TV_CHANNEL_BECOME_UNACCEPTABLE(MPTVServer, MPTVLocal, property, FALSE)
				SERVER_HANDLE_MP_TV_CHANNEL_CHANGE_THIS_FRAME(MPTVServer, MPTVLocal, FALSE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Server keeps a note of what all players want the current tv channel to be and decides which channel should be the current one appropriately
/// PARAMS:
///    MPTVServer - 
///    MPTVStaggerClient - 
///    MPTVLocal - 
///    property - 
///    bGarage - 
PROC SERVER_MAINTAIN_CURRENT_MP_TV_CHANNEL(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVStaggerClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property, BOOL bGarage)
	BOOL bReadyForUpdate = FALSE
	BOOL bChangedChannelThisFrame = FALSE
	MP_TV_CHANNEL thisCurrentServerChannel
	BOOL bIsClientSuitableForChange = FALSE
	
	IF bGarage
		thisCurrentServerChannel = MPTVServer.eCurrentGarageChannel
		IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SERVER_TIMER_GARAGE_CHANNEL_CHANGE_SET)
		OR GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPTVLocal.timeServerGarageChannelChange) > MP_TV_CHANNEL_CHANGE_WAIT_TIME
			bReadyForUpdate = TRUE
			IF IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
				IF MPTVStaggerClient.eTimeOfInputChannel = thisCurrentServerChannel
					bIsClientSuitableForChange = TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		thisCurrentServerChannel = MPTVServer.eCurrentChannel
		IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SERVER_TIMER_CHANNEL_CHANGE_SET)
		OR GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), MPTVLocal.timeServerChannelChange) > MP_TV_CHANNEL_CHANGE_WAIT_TIME
			bReadyForUpdate = TRUE
			IF NOT IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
				IF MPTVStaggerClient.eTimeOfInputChannel = thisCurrentServerChannel
					bIsClientSuitableForChange = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bReadyForUpdate
	
		IF bIsClientSuitableForChange
			IF IS_BIT_SET(MPTVStaggerClient.iInputBitset, MPTV_INPUT_NEXT_CHANNEL)
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV SERVER === SWITCH_TO_NEXT_MP_TV_CHANNEL for ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger)))
				#ENDIF
				SWITCH_TO_NEXT_MP_TV_CHANNEL(MPTVLocal, MPTVServer, property, bGarage)
				bChangedChannelThisFrame = TRUE
			ELIF IS_BIT_SET(MPTVStaggerClient.iInputBitset, MPTV_INPUT_PREVIOUS_CHANNEL)
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV SERVER === SWITCH_TO_PREVIOUS_MP_TV_CHANNEL for ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger)))
				#ENDIF
				SWITCH_TO_PREVIOUS_MP_TV_CHANNEL(MPTVLocal, MPTVServer, property, bGarage)
				bChangedChannelThisFrame = TRUE
			ENDIF
		ENDIF
	
		IF NOT bChangedChannelThisFrame
			IF SERVER_HAS_CURRENT_MP_TV_CHANNEL_BECOME_UNACCEPTABLE(MPTVServer, MPTVLocal, property, bGarage)
				bChangedChannelThisFrame = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bChangedChannelThisFrame
		SERVER_HANDLE_MP_TV_CHANNEL_CHANGE_THIS_FRAME(MPTVServer, MPTVLocal, bGarage)
	ENDIF
	
	bIsClientSuitableForChange = FALSE
	
	IF bGarage
		IF IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
			IF MPTVStaggerClient.fTimeOfInputVolume = GET_MP_TV_SERVER_CURRENT_VOLUME(MPTVServer, MPTVStaggerClient)
				bIsClientSuitableForChange = TRUE
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
			IF MPTVStaggerClient.fTimeOfInputVolume = GET_MP_TV_SERVER_CURRENT_VOLUME(MPTVServer, MPTVStaggerClient)
				bIsClientSuitableForChange = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bIsClientSuitableForChange
		IF IS_BIT_SET(MPTVStaggerClient.iInputBitset, MPTV_INPUT_VOLUME_UP)
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV SERVER === INCREMENT_TV_VOLUME TRUE for ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger)))
			#ENDIF
			INCREMENT_TV_VOLUME(MPTVServer, TRUE, bGarage)
		ELIF IS_BIT_SET(MPTVStaggerClient.iInputBitset, MPTV_INPUT_VOLUME_DOWN)
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV SERVER === INCREMENT_TV_VOLUME FALSE for ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger)))
			#ENDIF
			INCREMENT_TV_VOLUME(MPTVServer, FALSE, bGarage)
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Saves data so that the property owner will keep their last watched channel when they return to property
/// PARAMS:
///    MPTVServer - 
PROC MAINTAIN_TV_SAVE_GAME(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_TV_SERVER_DATA_STRUCT &MPTVServer)
	IF MPTVLocal.bInCasinoApartment
		IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
			IF MPTVServer.eStage > MP_TV_SERVER_STAGE_INIT
				IF MPTVServer.eStage = MP_TV_SERVER_STAGE_ON
					IF NOT g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bPenthouseTVOn
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bTVOn = TRUE")
						#ENDIF
						g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bPenthouseTVOn = TRUE
					ENDIF
				ENDIF
				IF MPTVServer.eStage = MP_TV_SERVER_STAGE_OFF
					IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bPenthouseTVOn
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bTVOn = FALSE")
						#ENDIF
						g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bPenthouseTVOn = FALSE
					ENDIF
				ENDIF
				IF ENUM_TO_INT(MPTVServer.eCurrentChannel) <> g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iPenthouseTVChannelID
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iTVChannelID = ", ENUM_TO_INT(MPTVServer.eCurrentChannel))
					#ENDIF
					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iPenthouseTVChannelID = ENUM_TO_INT(MPTVServer.eCurrentChannel)
				ENDIF
			ENDIF
		ELSE
			IF MPTVServer.eStage > MP_TV_SERVER_STAGE_INIT
				IF MPTVServer.eStage = MP_TV_SERVER_STAGE_ON
					IF NOT g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bMediaRoomTVOn
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bTVOn = TRUE")
						#ENDIF
						g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bMediaRoomTVOn = TRUE
					ENDIF
				ENDIF
				IF MPTVServer.eStage = MP_TV_SERVER_STAGE_OFF
					IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bMediaRoomTVOn
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bTVOn = FALSE")
						#ENDIF
						g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bMediaRoomTVOn = FALSE
					ENDIF
				ENDIF
				IF ENUM_TO_INT(MPTVServer.eCurrentChannel) <> g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iMediaRoomTVChannelID
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iTVChannelID = ", ENUM_TO_INT(MPTVServer.eCurrentChannel))
					#ENDIF
					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iMediaRoomTVChannelID = ENUM_TO_INT(MPTVServer.eCurrentChannel)
				ENDIF
			ENDIF
		ENDIF
	#IF FEATURE_FIXER
	ELIF MPTVLocal.bInFixerHQ
		IF IS_BIT_SET(MPTVLocal.iBitset2, MPTV_LOCAL_BS2_THIRD_TV)
			IF MPTVServer.eStage > MP_TV_SERVER_STAGE_INIT
				IF MPTVServer.eStage = MP_TV_SERVER_STAGE_ON
					IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_HQ_BEDROOM_TV_ON)
						SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_HQ_BEDROOM_TV_ON, TRUE)
					ENDIF
				ENDIF
				
				IF MPTVServer.eStage = MP_TV_SERVER_STAGE_OFF
					IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_HQ_BEDROOM_TV_ON)
						SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_HQ_BEDROOM_TV_ON, FALSE)
					ENDIF
				ENDIF
				
				IF ENUM_TO_INT(MPTVServer.eCurrentChannel) <> GET_PACKED_STAT_INT(PACKED_MP_INT_FIXER_HQ_BEDROOM_TV_CHANNEL_ID)
					SET_PACKED_STAT_INT(PACKED_MP_INT_FIXER_HQ_BEDROOM_TV_CHANNEL_ID, ENUM_TO_INT(MPTVServer.eCurrentChannel))
				ENDIF
			ENDIF
		ELIF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
			IF MPTVServer.eStage > MP_TV_SERVER_STAGE_INIT
				IF MPTVServer.eStage = MP_TV_SERVER_STAGE_ON
					IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_HQ_LOUNGE_TV_ON)
						SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_HQ_LOUNGE_TV_ON, TRUE)
					ENDIF
				ENDIF
				
				IF MPTVServer.eStage = MP_TV_SERVER_STAGE_OFF
					IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_HQ_LOUNGE_TV_ON)
						SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_HQ_LOUNGE_TV_ON, FALSE)
					ENDIF
				ENDIF
				
				IF ENUM_TO_INT(MPTVServer.eCurrentChannel) <> GET_PACKED_STAT_INT(PACKED_MP_INT_FIXER_HQ_LOUNGE_TV_CHANNEL_ID)
					SET_PACKED_STAT_INT(PACKED_MP_INT_FIXER_HQ_LOUNGE_TV_CHANNEL_ID, ENUM_TO_INT(MPTVServer.eCurrentChannel))
				ENDIF
			ENDIF
		ELSE
			IF MPTVServer.eStage > MP_TV_SERVER_STAGE_INIT
				IF MPTVServer.eStage = MP_TV_SERVER_STAGE_ON
					IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_HQ_BOARDROOM_TV_ON)
						SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_HQ_BOARDROOM_TV_ON, TRUE)
					ENDIF
				ENDIF
				
				IF MPTVServer.eStage = MP_TV_SERVER_STAGE_OFF
					IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_HQ_BOARDROOM_TV_ON)
						SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_HQ_BOARDROOM_TV_ON, FALSE)
					ENDIF
				ENDIF
				
				IF ENUM_TO_INT(MPTVServer.eCurrentChannel) <> GET_PACKED_STAT_INT(PACKED_MP_INT_FIXER_HQ_BOARDROOM_TV_CHANNEL_ID)
					SET_PACKED_STAT_INT(PACKED_MP_INT_FIXER_HQ_BOARDROOM_TV_CHANNEL_ID, ENUM_TO_INT(MPTVServer.eCurrentChannel))
				ENDIF
			ENDIF
		ENDIF
	#ENDIF
	ELIF NOT MPTVLocal.bInArcade
		IF MPTVServer.eStage > MP_TV_SERVER_STAGE_INIT
			IF MPTVServer.eStage = MP_TV_SERVER_STAGE_ON
				IF NOT g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bTVOn
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bTVOn = TRUE")
					#ENDIF
					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bTVOn = TRUE
				ENDIF
			ENDIF
			IF MPTVServer.eStage = MP_TV_SERVER_STAGE_OFF
				IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bTVOn
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bTVOn = FALSE")
					#ENDIF
					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bTVOn = FALSE
				ENDIF
			ENDIF
			IF ENUM_TO_INT(MPTVServer.eCurrentChannel) <> g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iTVChannelID
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iTVChannelID = ", ENUM_TO_INT(MPTVServer.eCurrentChannel))
				#ENDIF
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iTVChannelID = ENUM_TO_INT(MPTVServer.eCurrentChannel)
			ENDIF
		ENDIF
		IF MPTVServer.eGarageStage > MP_TV_SERVER_STAGE_INIT
			IF MPTVServer.eGarageStage = MP_TV_SERVER_STAGE_ON
				IF NOT g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bGarageTVOn
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bGarageTVOn = TRUE")
					#ENDIF
					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bGarageTVOn = TRUE
				ENDIF
			ENDIF
			IF MPTVServer.eGarageStage = MP_TV_SERVER_STAGE_OFF
				IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bGarageTVOn
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bGarageTVOn = FALSE")
					#ENDIF
					g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bGarageTVOn = FALSE
				ENDIF
			ENDIF
			IF ENUM_TO_INT(MPTVServer.eCurrentGarageChannel) <> g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iGarageTVChannelID
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iGarageTVChannelID = ", ENUM_TO_INT(MPTVServer.eCurrentGarageChannel))
				#ENDIF
				g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iGarageTVChannelID = ENUM_TO_INT(MPTVServer.eCurrentGarageChannel)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Load the previously watched channels and volume for TV
/// PARAMS:
///    MPTVClient - 
PROC LOAD_PREVIOUS_TV_SAVE_GAME(MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_TV_CLIENT_DATA_STRUCT &MPTVClient)
	IF MPTVLocal.bInCasinoApartment
		IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
			IF NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_SET)
				IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bPenthouseTVOn
					SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)")
					#ENDIF
				ELSE
					CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)")
					#ENDIF
				ENDIF
				MPTVClient.iPreviousSavedChannel = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iPenthouseTVChannelID
				IF MPTVClient.iPreviousSavedChannel > -1
					IF TVCHANNELTYPE_CHANNEL_SPECIAL = GET_MP_TV_CHANNEL_TVCHANNELTYPE(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel))
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedChannel, was a special channel = ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel)), ", so changing ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, 6)))
						MPTVClient.iPreviousSavedChannel = 6		
					ELSE
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedChannel, wasn't a special channel = ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel)))
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedChannel = ", MPTVClient.iPreviousSavedChannel)
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedGarageChannel = ", MPTVClient.iPreviousSavedGarageChannel)
				#ENDIF
				
				SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_SET)
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_SET)
				IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bMediaRoomTVOn
					SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)")
					#ENDIF
				ELSE
					CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)")
					#ENDIF
				ENDIF
				MPTVClient.iPreviousSavedChannel = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iMediaRoomTVChannelID
				IF MPTVClient.iPreviousSavedChannel > -1
					IF TVCHANNELTYPE_CHANNEL_SPECIAL = GET_MP_TV_CHANNEL_TVCHANNELTYPE(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel))
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedChannel, was a special channel = ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel)), ", so changing ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, 6)))
						MPTVClient.iPreviousSavedChannel = 6		
					ELSE
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedChannel, wasn't a special channel = ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel)))
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedChannel = ", MPTVClient.iPreviousSavedChannel)
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedGarageChannel = ", MPTVClient.iPreviousSavedGarageChannel)
				#ENDIF
				
				SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_SET)
			ENDIF
		ENDIF
	#IF FEATURE_FIXER
	ELIF MPTVLocal.bInFixerHQ
		IF IS_BIT_SET(MPTVLocal.iBitset2, MPTV_LOCAL_BS2_THIRD_TV)
			IF NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_SET)
				IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_HQ_BEDROOM_TV_ON)
					SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)
				ELSE
					CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)
				ENDIF
				
				MPTVClient.iPreviousSavedChannel = GET_PACKED_STAT_INT(PACKED_MP_INT_FIXER_HQ_BEDROOM_TV_CHANNEL_ID)
				
				IF MPTVClient.iPreviousSavedChannel > -1
					IF TVCHANNELTYPE_CHANNEL_SPECIAL = GET_MP_TV_CHANNEL_TVCHANNELTYPE(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel))
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedChannel, was a special channel = ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel)), ", so changing ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, 6)))
						
						MPTVClient.iPreviousSavedChannel = 6
					ELSE
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedChannel, wasn't a special channel = ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel)))
					ENDIF
				ENDIF
				
				SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_SET)
			ENDIF
		ELIF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
			IF NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_SET)
				IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_HQ_LOUNGE_TV_ON)
					SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)
				ELSE
					CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)
				ENDIF
				
				MPTVClient.iPreviousSavedChannel = GET_PACKED_STAT_INT(PACKED_MP_INT_FIXER_HQ_LOUNGE_TV_CHANNEL_ID)
				
				IF MPTVClient.iPreviousSavedChannel > -1
					IF TVCHANNELTYPE_CHANNEL_SPECIAL = GET_MP_TV_CHANNEL_TVCHANNELTYPE(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel))
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedChannel, was a special channel = ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel)), ", so changing ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, 6)))
						
						MPTVClient.iPreviousSavedChannel = 6
					ELSE
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedChannel, wasn't a special channel = ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel)))
					ENDIF
				ENDIF
				
				SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_SET)
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_SET)
				IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_FIXER_HQ_BOARDROOM_TV_ON)
					SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)
				ELSE
					CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)
				ENDIF
				
				MPTVClient.iPreviousSavedChannel = GET_PACKED_STAT_INT(PACKED_MP_INT_FIXER_HQ_BOARDROOM_TV_CHANNEL_ID)
				
				IF MPTVClient.iPreviousSavedChannel > -1
					IF TVCHANNELTYPE_CHANNEL_SPECIAL = GET_MP_TV_CHANNEL_TVCHANNELTYPE(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel))
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedChannel, was a special channel = ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel)), ", so changing ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, 6)))
						
						MPTVClient.iPreviousSavedChannel = 6
					ELSE
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedChannel, wasn't a special channel = ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel)))
					ENDIF
				ENDIF
				
				SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_SET)
			ENDIF
		ENDIF
	#ENDIF
	ELSE
		IF NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_SET)
			IF NOT MPTVLocal.bInArcade
				IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bTVOn
					SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)")
					#ENDIF
				ELSE
					CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)")
					#ENDIF
				ENDIF
				IF g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.bGarageTVOn
					SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_GARAGE_PREVIOUS_SAVE_ON)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_GARAGE_PREVIOUS_SAVE_ON)")
					#ENDIF
				ELSE
					CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_GARAGE_PREVIOUS_SAVE_ON)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_GARAGE_PREVIOUS_SAVE_ON)")
					#ENDIF
				ENDIF
				MPTVClient.iPreviousSavedChannel = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iTVChannelID
				MPTVClient.iPreviousSavedGarageChannel = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedProperty.iGarageTVChannelID
				IF MPTVClient.iPreviousSavedChannel > -1
					IF TVCHANNELTYPE_CHANNEL_SPECIAL = GET_MP_TV_CHANNEL_TVCHANNELTYPE(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel))
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedChannel, was a special channel = ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel)), ", so changing ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, 6)))
						MPTVClient.iPreviousSavedChannel = 6		
					ELSE
						CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedChannel, wasn't a special channel = ", GET_MP_TV_CHANNEL_NAME(INT_TO_ENUM(MP_TV_CHANNEL, MPTVClient.iPreviousSavedChannel)))
					ENDIF
				ENDIF
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedChannel = ", MPTVClient.iPreviousSavedChannel)
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iPreviousSavedGarageChannel = ", MPTVClient.iPreviousSavedGarageChannel)
				#ENDIF
			ELSE
				SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)
				MPTVClient.iPreviousSavedChannel = ENUM_TO_INT(MP_TV_CHANNEL_ARCADE)
			ENDIF
			
			SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_SET)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Clears the variables required to pop the local player into a specific seat
/// PARAMS:
///    MPTVClient - 
///    MPTVLocal - 
PROC RESET_FORCE_LOCAL_PLAYER_INTO_TV_SEAT(MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal)
	
	#IF IS_DEBUG_BUILD
	//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === RESET_FORCE_LOCAL_PLAYER_INTO_TV_SEAT")
	#ENDIF
	
	MPTVClient.iSeat = -1
	
	MPTVLocal.iForceIntoTVSeatStage = 0
	
ENDPROC

/// PURPOSE:
///    Pop the local player into any MPTV seat instantly
/// PARAMS:
///    MPTVServer - 
///    MPTVClient - 
///    MPTVLocal - 
///    property - 
/// RETURNS:
///    a result enum indicating if palyer is in seat FORCE_INTO_TV_SEAT_RESULT
FUNC FORCE_INTO_TV_SEAT_RESULT FORCE_LOCAL_PLAYER_INTO_TV_SEAT(	MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, 
																MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property, INT iForcedSeat = -1)
	
	INT i
	
	IF MPTVClient.eStage > MP_TV_CLIENT_STAGE_INIT
	
		SWITCH MPTVLocal.iForceIntoTVSeatStage
			
			CASE 0	//Find empty seat
				
				IF MPTVClient.eStage = MP_TV_CLIENT_STAGE_WALKING
					
					MPTVClient.iSeat = iForcedSeat
					
					
					
					REPEAT MAX_NUMBER_OF_SEATS i
						IF MPTVClient.iSeat = -1
							IF NOT IS_BIT_SET(MPTVServer.sSeat[i].iBitset, MPTV_SEAT_BS_OCCUPIED)
								MPTVClient.iSeat = i
								#IF IS_DEBUG_BUILD
								//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === MPTVClient.iSeat = ", i)
								CPRINTLN(DEBUG_ACT_CELEB,"FORCE_LOCAL_PLAYER_INTO_TV_SEAT: FAIL!! - iSeat = ", i)
								#ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					
					IF MPTVClient.iSeat <> -1
						SET_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)
						MPTVLocal.timeAbandonSeat = GET_NETWORK_TIME()
						MPTVLocal.iForceIntoTVSeatStage++
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === FORCE_LOCAL_PLAYER_INTO_TV_SEAT iForceIntoTVSeatStage = ", MPTVLocal.iForceIntoTVSeatStage)
						#ENDIF
					ELSE
						MPTVLocal.iForceIntoTVSeatStage = 99
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === FORCE_LOCAL_PLAYER_INTO_TV_SEAT FAIL, did not find seat")
						#ENDIF
					ENDIF
				
				ELSE
				
					IF MPTVClient.iSeat != -1
						MPTVLocal.iForceIntoTVSeatStage = 99
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === FORCE_LOCAL_PLAYER_INTO_TV_SEAT FAIL, already sitting")
						#ENDIF
//					ELSE
//						MPTVLocal.iForceIntoTVSeatStage = 99
					ENDIF
					
				ENDIF
				
			BREAK
			
			CASE 1
				IF MPTVClient.iSeat <> -1
				
					SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)
					//CDEBUG2LN(DEBUG_ACT_CELEB,"FORCE_LOCAL_PLAYER_INTO_TV_SEAT: CLIENT_MAINTAIN_GETTING_A_SEAT - iSeat = ", MPTVClient.iSeat)
					IF CLIENT_MAINTAIN_GETTING_A_SEAT(MPTVServer, MPTVClient, MPTVLocal, property, TRUE)
						SET_MP_TV_CLIENT_STAGE(MPTVClient, MP_TV_CLIENT_STAGE_SITTING)
						MPTVLocal.iForceIntoTVSeatStage = 100
						#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === FORCE_LOCAL_PLAYER_INTO_TV_SEAT SUCCESS")
						#ENDIF
					ENDIF
				ELSE
					MPTVLocal.iForceIntoTVSeatStage = 99
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === FORCE_LOCAL_PLAYER_INTO_TV_SEAT FAIL, seat became occupied by someone else")
					#ENDIF
				ENDIF
			BREAK
			
			CASE 98 // Return fail.
				RETURN FORCE_INTO_TV_SEAT_ALREADY_SEATED
			BREAK
			
			CASE 99	//return fail
				RETURN FORCE_INTO_TV_SEAT_FAIL
			BREAK
			
			CASE 100 //return pass
				RETURN FORCE_INTO_TV_SEAT_SUCCESS
			BREAK
			
		ENDSWITCH
	
	ENDIF
	
	
	RETURN FORCE_INTO_TV_SEAT_ATTEMPTING
ENDFUNC

PROC CLIENT_MAINTAIN_SUBTITLES()
	IF IS_CUTSCENE_PLAYING()
		ENABLE_MOVIE_SUBTITLES(FALSE)
	ELSE	
		ENABLE_MOVIE_SUBTITLES(TRUE)
	ENDIF
ENDPROC

// ====================================================================================
// ====================================================================================
//
// MAINTAIN PROCS
//
// ====================================================================================
// ====================================================================================

/// PURPOSE:
///    Every frame server update for MPTV to be called by the owner of the apartment script
/// PARAMS:
///    MPTVServer - 
///    MPTVStaggerClient - 
///    MPTVLocal - 
///    property - 
 PROC SERVER_MAINTAIN_MP_TV(	MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVStaggerClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, 
							MP_PROPERTY_STRUCT &property)
	#IF IS_DEBUG_BUILD
		CDEBUG2LN(DEBUG_MP_TV, " SERVER_MAINTAIN_SEATS()")
	#ENDIF	
	SERVER_MAINTAIN_SEATS(MPTVServer, MPTVStaggerClient, MPTVLocal, property)
	
	
	
	
	SWITCH MPTVServer.eStage
		
		CASE MP_TV_SERVER_STAGE_INIT
			IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBlockLoadingTvForHeistCleanup
				IF NOT bInPropertyOwnerNotPaidLastUtilityBill
					IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger), FALSE, FALSE)
						IF NETWORK_IS_PLAYER_A_PARTICIPANT(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger))
							IF NETWORK_GET_HOST_OF_THIS_SCRIPT() = NETWORK_GET_PARTICIPANT_INDEX(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger))	//We're checking the owner!	
								IF IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_SET)
									MPTVServer.eCurrentChannel = INT_TO_ENUM(MP_TV_CHANNEL, MPTVStaggerClient.iPreviousSavedChannel)
									MPTVServer.fVolume = (MP_TV_MAX_VOLUME + MP_TV_MIN_VOLUME)/2
									
									IF MPTVLocal.bInArcade
										MPTVServer.eCurrentChannel = MP_TV_CHANNEL_ARCADE
									ENDIF
									
									CDEBUG1LN(DEBUG_MP_TV, "MP_TV_SERVER_STAGE_INIT: SETTING PREVIOUSLY SAVED CHANNEL")
									
									IF IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_ON)
									AND NOT bInPropertyOwnerNotPaidLastUtilityBill

										SET_MP_TV_SERVER_STAGE(MPTVServer, MP_TV_SERVER_STAGE_ON, FALSE)
									ELSE
										SET_MP_TV_SERVER_STAGE(MPTVServer, MP_TV_SERVER_STAGE_OFF, FALSE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					MPTVServer.eCurrentChannel = MP_TV_CHANNEL_CNT
					
					MPTVServer.fVolume = (MP_TV_MAX_VOLUME + MP_TV_MIN_VOLUME)/2
					
					SET_MP_TV_SERVER_STAGE(MPTVServer, MP_TV_SERVER_STAGE_OFF, FALSE)
				ENDIF
			ENDIF
		BREAK
		
		CASE MP_TV_SERVER_STAGE_ON
			IF (IS_BIT_SET(MPTVStaggerClient.iInputBitset, MPTV_INPUT_POWER_OFF)
				AND NOT IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE))
			OR bInPropertyOwnerNotPaidLastUtilityBill
				SET_MP_TV_SERVER_STAGE(MPTVServer, MP_TV_SERVER_STAGE_OFF, FALSE)
			ELSE
				SERVER_MAINTAIN_CURRENT_MP_TV_CHANNEL(MPTVServer, MPTVStaggerClient, MPTVLocal, property, FALSE)
			ENDIF
			
			IF IS_TRANSITION_SESSION_RESTARTING() = TRUE
			OR IS_TRANSITION_SESSION_LAUNCHING() = TRUE
			OR IS_SKYSWOOP_MOVING()
			//OR IS_PLAYER_IN_CORONA()
				SET_MP_TV_SERVER_STAGE(MPTVServer, MP_TV_SERVER_STAGE_OFF, FALSE)	
				//CDEBUG2LN(DEBUG_MP_TV, "SERVER_MAINTAIN_MP_TV: IS_TRANSITION_SESSION_RESTARTING:", IS_TRANSITION_SESSION_RESTARTING(), ", IS_TRANSITION_SESSION_LAUNCHING:", IS_TRANSITION_SESSION_LAUNCHING(), ", IS_SKYSWOOP_MOVING:", IS_SKYSWOOP_MOVING(), ", IS_PLAYER_IN_CORONA: ", IS_PLAYER_IN_CORONA(),", turning off TV")
			ENDIF
		BREAK
		
		CASE MP_TV_SERVER_STAGE_OFF
			SERVER_MAINTAIN_CURRENT_MP_TV_CHANNEL_WHEN_OFF(MPTVServer, MPTVLocal, property, FALSE)
			IF IS_BIT_SET(MPTVStaggerClient.iInputBitset, MPTV_INPUT_POWER_ON)
			AND NOT IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
			AND NOT bInPropertyOwnerNotPaidLastUtilityBill
				SET_MP_TV_SERVER_STAGE(MPTVServer, MP_TV_SERVER_STAGE_ON, FALSE)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	SWITCH MPTVServer.eGarageStage
	
		CASE MP_TV_SERVER_STAGE_INIT
			IF NOT bInPropertyOwnerNotPaidLastUtilityBill
				IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger), FALSE, FALSE)
					IF NETWORK_IS_PLAYER_A_PARTICIPANT(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger))
						IF NETWORK_GET_HOST_OF_THIS_SCRIPT() = NETWORK_GET_PARTICIPANT_INDEX(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger))	//We're checking the owner!	
							IF IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_PREVIOUS_SAVE_SET)
								
								MPTVServer.eCurrentGarageChannel = INT_TO_ENUM(MP_TV_CHANNEL, MPTVStaggerClient.iPreviousSavedGarageChannel)
								MPTVServer.fGarageVolume = (MP_TV_MAX_VOLUME + MP_TV_MIN_VOLUME)/2
								
								IF IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_GARAGE_PREVIOUS_SAVE_ON)
								AND NOT bInPropertyOwnerNotPaidLastUtilityBill
									SET_MP_TV_SERVER_STAGE(MPTVServer, MP_TV_SERVER_STAGE_ON, TRUE)
								ELSE
									SET_MP_TV_SERVER_STAGE(MPTVServer, MP_TV_SERVER_STAGE_OFF, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				MPTVServer.eCurrentGarageChannel = MP_TV_CHANNEL_CNT
				MPTVServer.fGarageVolume = (MP_TV_MAX_VOLUME + MP_TV_MIN_VOLUME)/2
				SET_MP_TV_SERVER_STAGE(MPTVServer, MP_TV_SERVER_STAGE_OFF, TRUE)
			ENDIF
			
		BREAK
		
		CASE MP_TV_SERVER_STAGE_ON
			IF (IS_BIT_SET(MPTVStaggerClient.iInputBitset, MPTV_INPUT_POWER_OFF)
				AND IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE))
			OR bInPropertyOwnerNotPaidLastUtilityBill
				SET_MP_TV_SERVER_STAGE(MPTVServer, MP_TV_SERVER_STAGE_OFF, TRUE)
			ELSE
				SERVER_MAINTAIN_CURRENT_MP_TV_CHANNEL(MPTVServer, MPTVStaggerClient, MPTVLocal, property, TRUE)
			ENDIF
		BREAK
		
		CASE MP_TV_SERVER_STAGE_OFF
			SERVER_MAINTAIN_CURRENT_MP_TV_CHANNEL_WHEN_OFF(MPTVServer, MPTVLocal, property, TRUE)
			IF IS_BIT_SET(MPTVStaggerClient.iInputBitset, MPTV_INPUT_POWER_ON)
			AND IS_BIT_SET(MPTVStaggerClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
			AND NOT bInPropertyOwnerNotPaidLastUtilityBill
				SET_MP_TV_SERVER_STAGE(MPTVServer, MP_TV_SERVER_STAGE_ON, TRUE)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	MPTVLocal.iServerPlayerStagger++
	IF MPTVLocal.iServerPlayerStagger >= NUM_NETWORK_PLAYERS
		
		MPTVLocal.iServerPlayerStagger = 0
	ENDIF
ENDPROC


FUNC BOOL DONT89_IS_PLAYER_NEXT_TO_SOFA_IN_OFFICE(MP_PROPERTY_STRUCT &property)
	
	MP_PROP_OFFSET_STRUCT mpBoundingBox1A
	MP_PROP_OFFSET_STRUCT mpBoundingBox1B
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_OFFICE_4TH_TV_STAND_BOUND_1A, mpBoundingBox1A, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_OFFICE_4TH_TV_STAND_BOUND_1B, mpBoundingBox1B, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), mpBoundingBox1A.vLoc, mpBoundingBox1B.vLoc, 1.0)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "DONT89_IS_PLAYER_NEXT_TO_SOFA_IN_OFFICE: Player is in bounding box1")
		RETURN TRUE
	ENDIF
	
	MP_PROP_OFFSET_STRUCT mpBoundingBox2A
	MP_PROP_OFFSET_STRUCT mpBoundingBox2B
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_OFFICE_4TH_TV_STAND_BOUND_2A, mpBoundingBox2A, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_OFFICE_4TH_TV_STAND_BOUND_2B, mpBoundingBox2B, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), mpBoundingBox2A.vLoc, mpBoundingBox2B.vLoc, 1.0)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "DONT89_IS_PLAYER_NEXT_TO_SOFA_IN_OFFICE: Player is in bounding box2")
		RETURN TRUE
	ENDIF
	
	
	MP_PROP_OFFSET_STRUCT mpBoundingBox3A
	MP_PROP_OFFSET_STRUCT mpBoundingBox3B
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_OFFICE_4TH_TV_STAND_BOUND_3A, mpBoundingBox3A, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
	GET_POSITION_AS_OFFSET_FOR_PROPERTY(property.iIndex, MP_PROP_ELEMENT_OFFICE_4TH_TV_STAND_BOUND_3B, mpBoundingBox3B, GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex))
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), mpBoundingBox3A.vLoc, mpBoundingBox3B.vLoc, 1.0)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "DONT89_IS_PLAYER_NEXT_TO_SOFA_IN_OFFICE: Player is in bounding box3")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF FEATURE_CASINO_HEIST
FUNC BOOL DONT89_IS_PLAYER_NEXT_TO_SOFA_IN_ARCADE()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2737.950928, -379.399017, -49.399834>>, <<2737.947754, -380.359253, -47.399834>>, 1.25)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2737.930420, -382.851410, -49.399834>>, <<2737.914307, -383.836914, -47.399834>>, 1.25)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2737.074463, -379.525177, -49.399834>>, <<2737.120117, -383.651245, -47.399834>>, 1.5)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
#ENDIF

/// PURPOSE:
///    Client every frame update to be called by all players in the apartment to handle displaying and interacting with TV
/// PARAMS:
///    MPTVServer - 
///    MPTVClient - 
///    MPTVLocal - 
///    property - 
PROC CLIENT_MAINTAIN_MP_TV(MP_TV_SERVER_DATA_STRUCT &MPTVServer, MP_TV_CLIENT_DATA_STRUCT &MPTVClient, MP_TV_LOCAL_DATA_STRUCT &MPTVLocal, MP_PROPERTY_STRUCT &property)
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTVDebug")
		PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_MP_TV")
	ENDIF
	#ENDIF
	
		//Deal with launching the mini game
	IF NOT IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_RUNNING)
		PRINTLN("[DONT8] - GET_ENTITY_SPEED(PLAYER_PED_ID()) = ", GET_ENTITY_SPEED(PLAYER_PED_ID()))
	
		//If we are near the sofa then set that we can play
		IF NOT g_bCelebrationScreenIsActive
		AND IS_NET_PLAYER_OK(PLAYER_ID(), TRUE)
		AND NOT IS_BIT_SET(MPGlobals.KillYourselfData.iBitSet, biKY_AnimStarted)
		AND NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_RADIO)
		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
		AND NOT IS_ANY_INTERACTION_ANIM_PLAYING()	
		AND NOT IS_PAUSE_MENU_ACTIVE_EX()
		AND ( (IS_PLAYER_IN_OFFICE_PROPERTY(PLAYER_ID()) AND DONT89_IS_PLAYER_NEXT_TO_SOFA_IN_OFFICE(property))
		#IF FEATURE_CASINO_HEIST OR (IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID()) AND DONT89_IS_PLAYER_NEXT_TO_SOFA_IN_ARCADE()) #ENDIF )
		AND NOT IS_BROWSER_OPEN()
		AND NOT IS_OWNER_PREVIEWING_INTERIOR()
		AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		AND NOT IS_CUSTOM_MENU_ON_SCREEN()
		AND NOT NETWORK_IS_ACTIVITY_SESSION()
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND GET_ENTITY_SPEED(PLAYER_PED_ID()) <= 0.001
			INT iPlayerOnSofaCount
			//Cache the return of the function so we don't need to do two loops
			BOOL bDoLaunch = DONT89_LAUNCH_SCRIPT_BECAUSE_REMOTE_PLAYER_WANTS_ME_TO(iPlayerOnSofaCount)
			
			#IF FEATURE_CASINO_HEIST
			IF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
			AND g_iArcadeJimmySpawnLocation = 0 OR g_iArcadeJimmySpawnLocation = 1
				iPlayerOnSofaCount++
			ENDIF
			#ENDIF
			
			//If the sofa is full then allow the player to set his launch flags
			IF iPlayerOnSofaCount >= 3
				DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(TRUE)
				DONT89_SETIS_STANDING_AT_SOFA_BD_STATE(TRUE)
				IF bDoLaunch
					DONT89_SET_LAUNCH_FLAGS(FALSE)
				ENDIF
			//Sofa not full, then clean up
			ELSE
				DONT89_SETIS_STANDING_AT_SOFA_BD_STATE(FALSE)
				DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE, TRUE)		
			ENDIF
		//Set that we need to clean up
		ELIF NOT IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_AVALIABLE)
		AND NOT IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_ON_SOFA)
			DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE, TRUE)		
			DONT89_SETIS_STANDING_AT_SOFA_BD_STATE(FALSE)
		//Just clean up
		ELSE
			DONT89_SETIS_STANDING_AT_SOFA_BD_STATE(FALSE)
		ENDIF
		
		//If it's avaliable
		IF IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_AVALIABLE)
			//And we've not loaded, then load
			IF IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_DO_ASSETS_REQUEST)
			AND LINE_MINIGAME_REQUEST_ALL_ASSETS()
				CLEAR_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_DO_ASSETS_REQUEST)
			ENDIF
		//Clean up the assets
		ELSE
			 LINE_MINIGAME_CLEANUP_ALL_ASSETS_IF_NEEDED()
		ENDIF
		
		IF IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_AVALIABLE)
		AND IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, GET_SPECTATOR_ACCEPT_CANCEL_INPUT(TRUE))
		AND NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
			IF NOT IS_PAUSE_MENU_ACTIVE()
			AND NOT IS_CUSTOM_MENU_ON_SCREEN()
			AND NOT IS_BROWSER_OPEN()
			AND NOT IS_PHONE_ONSCREEN()
			AND MPTVClient.eStage != MP_TV_CLIENT_STAGE_ACTIVATED
			AND NOT IS_OWNER_PREVIEWING_INTERIOR()
			AND NOT IS_WARNING_MESSAGE_ACTIVE()
			AND NOT IS_SELECTOR_ONSCREEN()
			AND NOT IS_REPLAY_RECORDING_FEED_BUTTONS_ONSCREEN()
				
				IF MPTVClient.eStage = MP_TV_CLIENT_STAGE_SITTING
					IF HAS_SEAT_ANIM_FINISHED()
						DONT89_SET_LAUNCH_FLAGS()
						EXIT
					ELSE
						PRINTLN("[DONT89] - HAS_SEAT_ANIM_FINISHED FALSE")
					ENDIF
				ELSE
					DONT89_SET_LAUNCH_FLAGS()
					EXIT
				ENDIF
				PRINTLN("[DONT89] - ciDONT_CROSS_89_RUNNING")
			#IF IS_DEBUG_BUILD
			ELSE
				PRINTLN("[DONT89] - IS_DISABLED_CONTROL_JUST_RELEASED - IS_PAUSE_MENU_ACTIVE")
				#ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_BIT_SET(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_LAUNCHED)
		AND NOT IS_PLAYER_SCTV(PLAYER_ID())
		AND MPTVClient.eStage != MP_TV_CLIENT_STAGE_ACTIVATED
			IF DONT89_LAUNCH_CONTROLLER_SCRIPT()
				SET_BIT(g_sDontCrossTheLine89LaunchVars.iBitSet, ciDONT_CROSS_89_LAUNCHED)
			ENDIF
		ENDIF
		CDEBUG1LN(DEBUG_MP_TV, "CLIENT_MAINTAIN_MP_TV: ciDONT_CROSS_89_RUNNING set")
		IF MPTVLocal.iContextButtonIntention != NEW_CONTEXT_INTENTION
			RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention)		
			MPTVLocal.iContextButtonIntention = NEW_CONTEXT_INTENTION
		ENDIF
		IF IS_PLAYER_IN_OFFICE_PROPERTY(PLAYER_ID())
		OR (MPTVLocal.bInCasinoApartment
		AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
		#IF FEATURE_CASINO_HEIST
		OR (MPTVLocal.bInArcade
		AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
		#ENDIF
		#IF FEATURE_FIXER
		OR (MPTVLocal.bInFixerHQ
		AND IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DCTL_SEAT))
		#ENDIF
			REINIT_NET_TIMER(g_sDontCrossTheLine89LaunchVars.stPromptDelay,TRUE)
			//PRINTLN("CLIENT_MAINTAIN_ACTIVATING_TV: Turning on Delay context for exiting DCTL")
		ENDIF
		EXIT
	ENDIF
	
	INT iInstance = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iVisibilityID
	
	IF IS_PLAYER_IN_ANY_SIMPLE_INTERIOR(PLAYER_ID())
		iInstance = GET_SIMPLE_INTERIOR_INT_SCRIPT_INSTANCE() + ENUM_TO_INT(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()))
	ENDIF
	
	IF NETWORK_IS_SCRIPT_ACTIVE(GET_MP_MISSION_NAME(eAM_DONT_CROSS_THE_LINE), iInstance, TRUE)
		EXIT
	ENDIF
		
	#IF IS_DEBUG_BUILD
		IF MPTVClient.eStage > MP_TV_CLIENT_STAGE_INIT
//			UPDATE_MP_TV_WIDGETS(MPTVLocal)
//			
//			#IF IS_DEBUG_BUILD
//			#IF SCRIPT_PROFILER_ACTIVE
//			ADD_SCRIPT_PROFILE_MARKER("UPDATE_MP_TV_WIDGETS")
//			#ENDIF
//			#ENDIF

		ENDIF
		
		IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_RELATIONSHIP_SEAT)
			//CDEBUG2LN(DEBUG_MP_TV, " MP_TV_CLIENT_STAGE_SITTING +++++++++++++ First player in seat +++++++", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger)))
			//CDEBUG2LN(DEBUG_MP_TV, " MP_TV_CLIENT_STAGE_SITTING +++++++++++++ First player in seat +++++++", MPTVLocal.iServerPlayerStagger)
		ENDIF
		
		IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_RELATIONSHIP_SECOND_SEAT)
			//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT +++++++++++++ Second player in seat +++++++", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger)))
			//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT +++++++++++++ Second player in seat +++++++", MPTVLocal.iServerPlayerStagger)
		ENDIF
	#ENDIF
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
	AND IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_PLAYER_AT_SEAT_NODE_COORD)
		CDEBUG1LN(DEBUG_MP_TV, "CLIENT_MAINTAIN_MP_TV: clearing MPTV_CLIENT_BS_PLAYER_AT_SEAT_NODE_COORD because player isn't going to coord")
		CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_PLAYER_AT_SEAT_NODE_COORD)
	ENDIF
	CLIENT_MAINTAIN_AMBIENT_TV(MPTVServer, MPTVClient, MPTVLocal)
	
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("CLIENT_MAINTAIN_AMBIENT_TV")
//	#ENDIF
//	#ENDIF

	
	IF NETWORK_IS_PLAYER_A_PARTICIPANT(PLAYER_ID())
		IF NETWORK_GET_HOST_OF_THIS_SCRIPT() = NETWORK_GET_PARTICIPANT_INDEX(PLAYER_ID())		//Player is the owner
			MAINTAIN_TV_SAVE_GAME(MPTVLocal, MPTVServer)
			
//			#IF IS_DEBUG_BUILD
//			#IF SCRIPT_PROFILER_ACTIVE
//			ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_TV_SAVE_GAME")
//			#ENDIF
//			#ENDIF
		ENDIF
	ENDIF
	
	IF MPTVClient.eStage <> MP_TV_CLIENT_STAGE_ACTIVATED
		IF MPTVClient.iInputBitset <> 0
			MPTVClient.iInputBitset = 0
		ENDIF
	ENDIF
	
	//MP_MAGDEMO IF NOT GET_MP_BOOL_CHARACTER_STAT(MP_STAT_NEAR_TV_HELP_TEXT)
		//MP_MAGDEMO IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			//MP_MAGDEMO IF VDIST2(GET_MP_TV_POSITION(MPTVLocal, MPTVClient, property), GET_PLAYER_COORDS(PLAYER_ID())) < MP_TV_HELP_TEXT_DISTANCE_SQR
				//MP_MAGDEMO RELEASE_CONTEXT_INTENTION(MPTVLocal.iContextButtonIntention)
				//MP_MAGDEMO PRINT_HELP("MPTV_HLP3")//You can control the TV by sitting on the couch.
				//MP_MAGDEMO SET_MP_BOOL_CHARACTER_STAT(MP_STAT_NEAR_TV_HELP_TEXT, TRUE)
			//MP_MAGDEMO ENDIF
		//MP_MAGDEMO ENDIF
	//MP_MAGDEMO ENDIF
	INT iSeat
	
	IF IS_PROPERTY_OFFICE(property.iIndex)
	OR GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex) = PROPERTY_CLUBHOUSE_7_BASE_B
	OR IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
	OR IS_PLAYER_IN_HANGAR(PLAYER_ID())
	OR IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	OR MPTVLocal.bInCasinoApartment
	#IF FEATURE_CASINO_HEIST
	OR MPTVLocal.bInArcade
	#ENDIF
	#IF FEATURE_FIXER
	OR MPTVLocal.bInFixerHQ
	#ENDIF
		iSeat = 0
	ELIF GET_BASE_PROPERTY_FROM_PROPERTY(property.iIndex) = PROPERTY_CLUBHOUSE_1_BASE_A
		iSeat = 1
	ENDIF
	
	
	
	IF IS_PROPERTY_OFFICE(property.iIndex)
	OR IS_PROPERTY_CLUBHOUSE(property.iIndex)
	OR IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
	OR IS_PLAYER_IN_HANGAR(PLAYER_ID())
	OR IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	OR MPTVLocal.bInCasinoApartment
	#IF FEATURE_CASINO_HEIST
	OR MPTVLocal.bInArcade
	#ENDIF
	#IF FEATURE_FIXER
	OR MPTVLocal.bInFixerHQ
	#ENDIF
		IF IS_BIT_SET(MPTVServer.sSeat[iSeat].iBitset, MPTV_SEAT_BS_OCCUPIED)
			IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_STAGE_SITTING)
				IF IS_SKYSWOOP_AT_GROUND()
					IF MPTVClient.eStage != MP_TV_CLIENT_STAGE_SITTING
						IF HAS_NET_TIMER_EXPIRED(MPTVLocal.timeoutInitStage , 5000)
							SET_MP_TV_CLIENT_STAGE(MPTVClient, MP_TV_CLIENT_STAGE_SITTING)
							SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_FORCE_STAGE_SITTING)
							PRINTLN("Timer out player stuck on stage: ", MPTVClient.eStage , "Setting stage to sitting")
						ENDIF
					ENDIF	
				ENDIF	
			ENDIF	
		ENDIF
	ENDIF
	
	SWITCH MPTVClient.eStage
	
		CASE MP_TV_CLIENT_STAGE_INIT
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTVDebug")
				PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_MP_TV - MP_TV_CLIENT_STAGE_INIT")
			ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD
//			 CDEBUG1LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_MP_TV : MP_TV_CLIENT_STAGE_INIT")
			#ENDIF
			
			IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.bBlockLoadingTvForHeistCleanup
			//IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE,TRUE)
				LOAD_PREVIOUS_TV_SAVE_GAME(MPTVLocal, MPTVClient)
				IF CLIENT_INIT(MPTVServer, MPTVClient, MPTVLocal, property)
				 	CDEBUG1LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_MP_TV : CLIENT_INIT: TRUE")
					
					IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
					OR NOT MPTVLocal.bInCasinoApartment
						SET_TV_AUDIO_FRONTEND(FALSE)
					ELSE
						SET_TV_AUDIO_FRONTEND(TRUE)
					ENDIF
					
					SET_MP_TV_CLIENT_STAGE(MPTVClient, MP_TV_CLIENT_STAGE_WALKING)
				ELSE
					#IF IS_DEBUG_BUILD
					CDEBUG1LN(DEBUG_MP_TV, "=== MP_TV CLIENT === TV Not ready for ambient play, will try again")
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE MP_TV_CLIENT_STAGE_WALKING
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTVDebug")
				PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_MP_TV - MP_TV_CLIENT_STAGE_WALKING")
			ENDIF
			#ENDIF
			
			#IF IS_DEBUG_BUILD
			 	//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_MP_TV : MP_TV_CLIENT_STAGE_WALKING")
			#ENDIF
			IF IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)
			AND NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
				#IF IS_DEBUG_BUILD
				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)")
				#ENDIF
				CLEAR_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)
			ENDIF
			IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLED_SUICIDE)
				ENABLE_KILL_YOURSELF_OPTION()
				CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLED_SUICIDE)
			ENDIF
			IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLED_INTERACTION)
				ENABLE_INTERACTION_MENU()
				CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLED_INTERACTION)
			ENDIF
			IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_AUDIO_FRONTEND)
				//SET_TV_AUDIO_FRONTEND(FALSE)
				CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_AUDIO_FRONTEND)
			ENDIF
			IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
				IF CLIENT_MAINTAIN_ACTIVATING_TV(MPTVClient, MPTVLocal)
					CLIENT_SETUP_TV_ACTIVATING(MPTVLocal, MPTVLocal.eCurrentChannel)
					SET_MP_TV_CLIENT_STAGE(MPTVClient, MP_TV_CLIENT_STAGE_ACTIVATED)
					DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE)
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				//	CDEBUG3LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_MP_TV: NOT IN GARAGE")
				#ENDIF
				IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_ABANDONING_SEAT)
					#IF IS_DEBUG_BUILD
						//CDEBUG2LN(DEBUG_MP_TV, " MPTVClient.iBitset, MPTV_CLIENT_BS_ABANDONING_SEAT: TRUE")
					#ENDIF
					IF CLIENT_MAINTAIN_LEAVING_A_SEAT(MPTVServer, MPTVClient, MPTVLocal, property)
						#IF IS_DEBUG_BUILD
							//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_LEAVING_A_SEAT: TRUE")
						#ENDIF
						CLEAR_BIT(MPTVClient.iBitset, MPTV_CLIENT_BS_ABANDONING_SEAT)
						IF g_bMPTVplayerWatchingTV
						AND IS_NET_PLAYER_OK(PLAYER_ID())
						AND NOT IS_PLAYER_TELEPORT_ACTIVE()
							IF IS_BIT_SET(MPTVLocal.iBitset,MPTV_LOCAL_BS_SET_WATCHING_TV)
								CLEAR_BIT(MPTVLocal.iBitset,MPTV_LOCAL_BS_SET_WATCHING_TV)
							ENDIF
							TIDYUP_MP_TV(MPTVLocal)
							//NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
							CPRINTLN(DEBUG_SPECTATOR, "=== MP_TV LOCAL SWITCH === moving to walking stage SETTING PLAYER CONTROL ON -1")
						ENDIF
						SET_MP_TV_CLIENT_STAGE(MPTVClient, MP_TV_CLIENT_STAGE_WALKING)
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					//	CDEBUG3LN(DEBUG_MP_TV, " MPTVClient.iBitset, MPTV_CLIENT_BS_ABANDONING_SEAT: FALSE")
					#ENDIF
					
					IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_SECOND_TV)
					OR NOT MPTVLocal.bInCasinoApartment
						SET_TV_AUDIO_FRONTEND(FALSE)
					ELSE
						SET_TV_AUDIO_FRONTEND(TRUE)
					ENDIF
					
					IF NOT IS_PROPERTY_OFFICE(property.iIndex)
					AND NOT IS_PROPERTY_CLUBHOUSE(property.iIndex)
					AND NOT IS_PROPERTY_OFFICE_GARAGE(property.iIndex)
					AND NOT IS_PLAYER_IN_HANGAR(PLAYER_ID())
					AND NOT IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
					AND NOT MPTVLocal.bInCasinoApartment
					#IF FEATURE_CASINO_HEIST
					AND NOT MPTVLocal.bInArcade
					#ENDIF
					#IF FEATURE_FIXER
					AND NOT MPTVLocal.bInFixerHQ
					#ENDIF
						IF CLIENT_MAINTAIN_GETTING_A_SEAT(MPTVServer, MPTVClient, MPTVLocal, property)
						
						
							#IF IS_DEBUG_BUILD
								//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT: TRUE")
							#ENDIF
							SET_MP_TV_CLIENT_STAGE(MPTVClient, MP_TV_CLIENT_STAGE_SITTING)
						ENDIF
					ENDIF
					
//					#IF IS_DEBUG_BUILD
//					#IF SCRIPT_PROFILER_ACTIVE
//					ADD_SCRIPT_PROFILE_MARKER("CLIENT_MAINTAIN_GETTING_A_SEAT")
//					#ENDIF
//					#ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE MP_TV_CLIENT_STAGE_SITTING
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTVDebug")
				PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_MP_TV - MP_TV_CLIENT_STAGE_SITTING")
			ENDIF
			#ENDIF
			
//			IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
//				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
//			ENDIF
			
			#IF IS_DEBUG_BUILD
		
			//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_MP_TV : MP_TV_CLIENT_STAGE_SITTING")
			#ENDIF
			
			#IF FEATURE_BENCH_RELATIONSHIPS
			IF NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_RELATIONSHIP_SEAT)
			AND NOT IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_RELATIONSHIP_SECOND_SEAT)
			#ENDIF
		
				IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)
					#IF IS_DEBUG_BUILD
					//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)")
					#ENDIF
					SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)
				ENDIF
				IF NOT IS_KILL_YOURSELF_OPTION_DISABLED()
					DISABLE_KILL_YOURSELF_OPTION()
					SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLED_SUICIDE)
				ENDIF
				IF NOT IS_INTERACTION_MENU_DISABLED()
					DISABLE_INTERACTION_MENU()
					SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLED_INTERACTION)
				ENDIF
				IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_AUDIO_FRONTEND)
					//SET_TV_AUDIO_FRONTEND(FALSE)
					CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_AUDIO_FRONTEND)
				ENDIF
			
				
				
				IF CLIENT_MAINTAIN_ACTIVATING_TV(MPTVClient, MPTVLocal)
				AND NOT IS_REMOTE_PLAYER_RUNNING_DONT89_IN_OFFICE(FALSE)
					SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()
					CDEBUG1LN(DEBUG_MP_TV, "CLIENT_MAINTAIN_MP_TV: MP_TV_CLIENT_STAGE_SITTING: SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED")
					CLIENT_SETUP_TV_ACTIVATING(MPTVLocal, MPTVLocal.eCurrentChannel)
					DISPLAY_CASH(FALSE)
					SET_MP_TV_CLIENT_STAGE(MPTVClient, MP_TV_CLIENT_STAGE_ACTIVATED)
					DONT89_SET_READY_TO_PLAY_GAME_BD_STATE(FALSE)
				ENDIF
				
			#IF FEATURE_BENCH_RELATIONSHIPS
			ELSE
				
//				IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_RELATIONSHIP_SEAT)
//					//CDEBUG2LN(DEBUG_MP_TV, " MP_TV_CLIENT_STAGE_SITTING +++++++++++++ First player in seat +++++++", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger)))
//					//CDEBUG2LN(DEBUG_MP_TV, " MP_TV_CLIENT_STAGE_SITTING +++++++++++++ First player in seat +++++++", MPTVLocal.iServerPlayerStagger)
//				ENDIF
//				
//				IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_RELATIONSHIP_SECOND_SEAT)
//					//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT +++++++++++++ Second player in seat +++++++", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(MPTVLocal.iServerPlayerStagger)))
//					//CDEBUG2LN(DEBUG_MP_TV, " CLIENT_MAINTAIN_GETTING_A_SEAT +++++++++++++ Second player in seat +++++++", MPTVLocal.iServerPlayerStagger)
//				ENDIF
			ENDIF
			#ENDIF
			IF CLIENT_MAINTAIN_LEAVING_A_SEAT(MPTVServer, MPTVClient, MPTVLocal, property)
				IF g_bMPTVplayerWatchingTV
				AND IS_NET_PLAYER_OK(PLAYER_ID())
				AND NOT IS_PLAYER_TELEPORT_ACTIVE()
					IF IS_BIT_SET(MPTVLocal.iBitset,MPTV_LOCAL_BS_SET_WATCHING_TV)
						CLEAR_BIT(MPTVLocal.iBitset,MPTV_LOCAL_BS_SET_WATCHING_TV)
					ENDIF
					TIDYUP_MP_TV(MPTVLocal)
					//NET_SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					CPRINTLN(DEBUG_SPECTATOR, "=== MP_TV LOCAL SWITCH === moving to walking stage SETTING PLAYER CONTROL ON - 2")
				ENDIF
				SET_MP_TV_CLIENT_STAGE(MPTVClient, MP_TV_CLIENT_STAGE_WALKING)
			ENDIF
//			IF NOT IS_BIT_SET(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)
//				#IF IS_DEBUG_BUILD
//				//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV CLIENT === SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)")
//				#ENDIF
//				SET_BIT(MPSpecGlobals.iBitSet, GLOBAL_SPEC_BS_USING_MP_TV_SEAT)
//			ENDIF
//			IF NOT IS_KILL_YOURSELF_OPTION_DISABLED()
//				DISABLE_KILL_YOURSELF_OPTION()
//				SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLED_SUICIDE)
//			ENDIF
//			IF NOT IS_INTERACTION_MENU_DISABLED()
//				DISABLE_INTERACTION_MENU()
//				SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLED_INTERACTION)
//			ENDIF
//			IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_AUDIO_FRONTEND)
//				//SET_TV_AUDIO_FRONTEND(FALSE)
//				CLEAR_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_AUDIO_FRONTEND)
//			ENDIF
//		
//			IF CLIENT_MAINTAIN_LEAVING_A_SEAT(MPTVServer, MPTVClient, MPTVLocal)
//				SET_MP_TV_CLIENT_STAGE(MPTVClient, MP_TV_CLIENT_STAGE_WALKING)
//			ENDIF
//			
//			IF CLIENT_MAINTAIN_ACTIVATING_TV(MPTVClient, MPTVLocal)
//				CLIENT_SETUP_TV_ACTIVATING(MPTVLocal, MPTVLocal.eCurrentChannel)
//				DISPLAY_CASH(FALSE)
//				SET_MP_TV_CLIENT_STAGE(MPTVClient, MP_TV_CLIENT_STAGE_ACTIVATED)
//			ENDIF
		BREAK
		
		CASE MP_TV_CLIENT_STAGE_ACTIVATED
			#IF IS_DEBUG_BUILD
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_ExtraTVDebug")
				PRINTLN("[EXTRA_TV_DEBUG] - CLIENT_MAINTAIN_MP_TV - MP_TV_CLIENT_STAGE_ACTIVATED")
			ENDIF
			#ENDIF
			
			IF NOT g_sMPTunables.bIDLEKICK_TV_JOB 
				IF NETWORK_IS_ACTIVITY_SESSION() 
					SET_IDLE_KICK_DISABLED_THIS_FRAME()
				ENDIF
			ENDIF
			
			IF NOT g_sMPTunables.bIDLEKICK_TV_FM  
				IF NOT NETWORK_IS_ACTIVITY_SESSION() 
					SET_IDLE_KICK_DISABLED_THIS_FRAME()
				ENDIF
			ENDIF
			
			
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
			ENDIF
			IF NOT IS_KILL_YOURSELF_OPTION_DISABLED()
				DISABLE_KILL_YOURSELF_OPTION()
				SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLED_SUICIDE)
			ENDIF
			IF NOT IS_INTERACTION_MENU_DISABLED()
				DISABLE_INTERACTION_MENU()
				SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_DISABLED_INTERACTION)
			ENDIF
			IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_AUDIO_FRONTEND)
				//SET_TV_AUDIO_FRONTEND(TRUE)
				SET_BIT(MPTVLocal.iBitset, MPTV_LOCAL_BS_AUDIO_FRONTEND)
			ENDIF
			
			CLIENT_MAINTAIN_SUBTITLES()
			
			IF NOT IS_BIT_SET(MPTVLocal.iBitset,MPTV_LOCAL_BS_SET_WATCHING_TV)
				g_bMPTVplayerWatchingTV = TRUE
				SET_BIT(MPTVLocal.iBitset,MPTV_LOCAL_BS_SET_WATCHING_TV)
			ENDIF
				MAINTAIN_SPECTATOR(MPTVLocal.specData)
				
//				#IF IS_DEBUG_BUILD
//				#IF SCRIPT_PROFILER_ACTIVE
//				ADD_SCRIPT_PROFILE_MARKER("MAINTAIN_SPECTATOR")
//				#ENDIF
//				#ENDIF
			IF 	g_bStripperHideHUDCutScene
				IF IS_SCREEN_FADED_OUT()
					SET_MP_TV_CLIENT_STAGE(MPTVClient, MP_TV_CLIENT_STAGE_SITTING)
					CLEAR_TIMECYCLE_MODIFIER()
					SET_NOISINESSOVERIDE(0.0)
					SET_NOISEOVERIDE(FALSE)
					CDEBUG1LN(DEBUG_MP_TV, " Stripper cut scene is active set stage to sitting")
				ENDIF	
			ENDIF
				IF CLEAN_UP_ON_CALL_WITH_OUT_WAIT()			//Cleans up any on calls
				
					IF CLIENT_MAINTAIN_WATCHING_TV(MPTVServer, MPTVClient, MPTVLocal, property)
						TIDYUP_MP_TV(MPTVLocal)
						g_bMPTVplayerWatchingTV = FALSE
						IF IS_BIT_SET(MPTVLocal.iBitset,MPTV_LOCAL_BS_SET_WATCHING_TV)
							CLEAR_BIT(MPTVLocal.iBitset,MPTV_LOCAL_BS_SET_WATCHING_TV)
						ENDIF
						IF IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_IN_GARAGE)
						//Prevent getting stuck with player control off
						IF IS_PLAYER_IN_FIRST_PERSON_CAMERA()
							CLEAR_PED_TASKS(PLAYER_PED_ID())
						ENDIF
							SET_MP_TV_CLIENT_STAGE(MPTVClient, MP_TV_CLIENT_STAGE_WALKING)
						ELSE
							SET_MP_TV_CLIENT_STAGE(MPTVClient, MP_TV_CLIENT_STAGE_SITTING)
						ENDIF
					ENDIF
				ENDIF
			SET_CLEAR_ON_CALL_HUD_THIS_FRAME(TRUE)
		BREAK
		
	ENDSWITCH
	
	IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_WATCH_TV_CAMERA_SETUP)
		IF NOT IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_REVEAL_BUILDING_EXTERIOR)	//If we're a camera inside	
			OVERRIDE_LODSCALE_THIS_FRAME(1.0)
		ELSE																			//If we're a camera outside
			IF IS_BIT_SET(MPTVLocal.iBitset, MPTV_LOCAL_BS_STATIC_OUTSIDE_CAM_ON)
				USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, TRUE)
				SET_SCRIPTED_CONVERSION_COORD_THIS_FRAME(GET_FINAL_RENDERED_CAM_COORD())
				THEFEED_HIDE_THIS_FRAME()
			ENDIF
		ENDIF
	ENDIF
	
	IF MPTVClient.eStage = MP_TV_CLIENT_STAGE_SITTING
	OR MPTVClient.eStage = MP_TV_CLIENT_STAGE_ACTIVATED
	OR IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_OCCUPYING_SEAT)
	OR IS_BIT_SET(MPTVClient.iBitset, MPTV_CLIENT_BS_TRYING_TO_OCCUPY_SEAT)
		DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
		SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
	ENDIF
	
	IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_ON)
		IF NOT IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_ON)
			SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_ON)
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_ON)")
			#ENDIF
			g_bTVOn = TRUE
		ENDIF
		IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_OFF)
			CLEAR_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_OFF)
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === CLEAR_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_OFF)")
			#ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNING_OFF)
		IF NOT IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_OFF)
			SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_OFF)
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === SET_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_OFF)")
			#ENDIF
			g_bTVOn = FALSE
		ENDIF
		IF IS_BIT_SET(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_ON)
			CLEAR_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_ON)
			#IF IS_DEBUG_BUILD
			//CDEBUG2LN(DEBUG_MP_TV, "=== MP_TV LOCAL === CLEAR_BIT(MPTVLocal.iSwitchBitset, MPTV_SWITCH_BS_TURNED_ON)")
			#ENDIF
		ENDIF
	ENDIF
	
	
	CLEAR_BIT(MPTVLocal.iBSSettings, ciMPTV_SETTINGS_BS_PREVENT_FADE_IN_THIS_FRAME)
ENDPROC
