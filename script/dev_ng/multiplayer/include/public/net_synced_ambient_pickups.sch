//////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_synced_ambient_pickups.sch											//
// Description: Handles creation of synchronised ambient pickups.						//
// Written by:  David Trenholme															//
// Date:        12/01/2021																//
//////////////////////////////////////////////////////////////////////////////////////////

//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"

USING "mp_globals_fm.sch"
USING "MP_globals_Tunables.sch"
USING "net_events.sch"
USING "net_include.sch"

//----------------------
//	FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD
FUNC STRING GET_SYNCED_AMBIENT_PICKUP_MODEL_NAME(MODEL_NAMES eModel)
	STRING sModelName = "DUMMY_MODEL_FOR_SCRIPT"
	
	IF eModel != DUMMY_MODEL_FOR_SCRIPT
		sModelName = GET_MODEL_NAME_FOR_DEBUG(eModel)
	ENDIF
	
	RETURN sModelName
ENDFUNC
#ENDIF

FUNC BOOL IS_SYNCED_AMBIENT_PICKUP_ID_VALID(SAP_PICKUP_ID& sPickupID)
	IF sPickupID.eType = PICKUP_TYPE_INVALID
	OR IS_VECTOR_ZERO(sPickupID.vInitialLocation)
	OR sPickupID.oiPickup = NULL
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL ARE_SYNCED_AMBIENT_PICKUP_IDS_EQUAL(SAP_PICKUP_ID& sPickupID1, SAP_PICKUP_ID& sPickupID2)
	IF sPickupID1.eType = sPickupID2.eType
	AND ARE_VECTORS_EQUAL(sPickupID1.vInitialLocation, sPickupID2.vInitialLocation)
	AND sPickupID1.oiPickup = sPickupID2.oiPickup
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT FIND_AVAILABLE_SYNCED_AMBIENT_PICKUP_REQUEST_INDEX(INT iPlayer = -1)
	IF iPlayer = -1
		iPlayer = NETWORK_PLAYER_ID_TO_INT()
	ENDIF
	
	INT iAvailableIndex = -1
	INT i
	REPEAT SAP_MAX_SYNCED_AMBIENT_PICKUPS i
		IF NOT IS_SYNCED_AMBIENT_PICKUP_ID_VALID(GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[i].sID)
			iAvailableIndex = i
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iAvailableIndex
ENDFUNC

FUNC INT FIND_SYNCED_AMBIENT_PICKUP_REQUEST_INDEX(SAP_PICKUP_ID& sPickupID, INT iPlayer = -1)
	IF NOT IS_SYNCED_AMBIENT_PICKUP_ID_VALID(sPickupID)
		RETURN -1
	ENDIF
	
	IF iPlayer = -1
		iPlayer = NETWORK_PLAYER_ID_TO_INT()
	ENDIF
	
	INT i
	REPEAT SAP_MAX_SYNCED_AMBIENT_PICKUPS i
		IF ARE_SYNCED_AMBIENT_PICKUP_IDS_EQUAL(sPickupID, GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[i].sID)
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

FUNC INT FIND_AVAILABLE_SYNCED_AMBIENT_PICKUP_INDEX()
	INT iAvailableIndex = -1
	INT i
	REPEAT SAP_MAX_SYNCED_AMBIENT_PICKUPS i
		IF NOT IS_SYNCED_AMBIENT_PICKUP_ID_VALID(GlobalServerBD_BlockC.sSAPServer.sPickups[i].sPickupData.sID)
			iAvailableIndex = i
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iAvailableIndex
ENDFUNC

FUNC INT FIND_SYNCED_AMBIENT_PICKUP_INDEX(SAP_PICKUP_ID& sPickupID)
	IF NOT IS_SYNCED_AMBIENT_PICKUP_ID_VALID(sPickupID)
		RETURN -1
	ENDIF
	
	INT i
	REPEAT SAP_MAX_SYNCED_AMBIENT_PICKUPS i
		IF ARE_SYNCED_AMBIENT_PICKUP_IDS_EQUAL(sPickupID, GlobalServerBD_BlockC.sSAPServer.sPickups[i].sPickupData.sID)
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

FUNC INT FIND_SYNCED_AMBIENT_PICKUP_INDEX_BY_OBJECT_INDEX(OBJECT_INDEX oiPickup)
	IF oiPickup = NULL
		RETURN -1
	ENDIF
	
	INT i
	REPEAT SAP_MAX_SYNCED_AMBIENT_PICKUPS i
		IF g_sSAPPickups[i].oiPickup = oiPickup
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC

FUNC SAP_PICKUP_ID CREATE_SYNCED_AMBIENT_PICKUP(PICKUP_TYPE eType, VECTOR vLocation, INT iPlacementFlags = 0, INT iAmount = -1, MODEL_NAMES eModel = DUMMY_MODEL_FOR_SCRIPT, BOOL bCreateAsScriptObject = FALSE)
	PRINTLN("[SYNCED_AMBIENT_PICKUPS] CREATE_SYNCED_AMBIENT_PICKUP - eType = ", GET_AMBIENT_PICKUP_NAME(eType), ", vLocation = ", vLocation, ", iPlacementFlags = ", iPlacementFlags, ", iAmount = ", iAmount, ", eModel = ", GET_SYNCED_AMBIENT_PICKUP_MODEL_NAME(eModel), ", bCreateAsScriptObject = ", GET_STRING_FROM_BOOL(bCreateAsScriptObject))
	DEBUG_PRINTCALLSTACK()
	
	SAP_PICKUP_ID sInvalidID
	
	//Check whether a pickup can be created.
	IF FIND_AVAILABLE_SYNCED_AMBIENT_PICKUP_INDEX() = -1
		ASSERTLN("[SYNCED_AMBIENT_PICKUPS] CREATE_SYNCED_AMBIENT_PICKUP - iAvailableIndex = -1. Attempting to create too many synced ambient pickups, returning invalid SAP_PICKUP_ID.")
		RETURN sInvalidID
	ENDIF
	
	//Check whether a pickup can be requested.
	INT iAvailableIndex = FIND_AVAILABLE_SYNCED_AMBIENT_PICKUP_REQUEST_INDEX()
	
	IF iAvailableIndex = -1
		ASSERTLN("[SYNCED_AMBIENT_PICKUPS] CREATE_SYNCED_AMBIENT_PICKUP - iAvailableIndex = -1. Attempting to request too many synced ambient pickups, returning invalid SAP_PICKUP_ID.")
		RETURN sInvalidID
	ENDIF
	
	//Create the pickup ID.
	SAP_PICKUP_ID sPickupID
	sPickupID.eType = eType
	sPickupID.vInitialLocation = vLocation
	
	//Check whether a pickup with this ID already exists.
	IF FIND_SYNCED_AMBIENT_PICKUP_INDEX(sPickupID) != -1
		ASSERTLN("[SYNCED_AMBIENT_PICKUPS] CREATE_SYNCED_AMBIENT_PICKUP - FIND_SYNCED_AMBIENT_PICKUP_INDEX(sPickupID) != -1. Attempting to create duplicate synced ambient pickup, returning invalid SAP_PICKUP_ID.")
		RETURN sInvalidID
	ENDIF
	
	//Create the local entity.
	SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
	sPickupID.oiPickup = CREATE_NON_NETWORKED_AMBIENT_PICKUP(eType, vLocation, iPlacementFlags, iAmount, eModel, bCreateAsScriptObject, FALSE)
	
	//Add the pickup request to player BD.
	INT iPlayer = NETWORK_PLAYER_ID_TO_INT()
	
	IF bCreateAsScriptObject
		GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[iAvailableIndex].iScriptNameHash = GET_HASH_OF_THIS_SCRIPT_NAME()
		GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[iAvailableIndex].iInstanceID = NETWORK_GET_POSITION_HASH_OF_THIS_SCRIPT()
		
		IF GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[iAvailableIndex].iInstanceID = 0
			GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[iAvailableIndex].iInstanceID = NETWORK_GET_INSTANCE_ID_OF_THIS_SCRIPT()
			GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[iAvailableIndex].eBehaviour = eSAPPICKUPBEHAVIOUR_SCRIPT_OBJECT
			
			PRINTLN("[SYNCED_AMBIENT_PICKUPS] CREATE_SYNCED_AMBIENT_PICKUP - Creating synced ambient pickup as a script object, iInstanceID = ", GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[iAvailableIndex].iInstanceID)
		ELSE
			GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[iAvailableIndex].eBehaviour = eSAPPICKUPBEHAVIOUR_WORLD_POINT_SCRIPT_OBJECT
			
			PRINTLN("[SYNCED_AMBIENT_PICKUPS] CREATE_SYNCED_AMBIENT_PICKUP - Creating synced ambient pickup as a world point script object, iInstanceID = ", GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[iAvailableIndex].iInstanceID)
		ENDIF
	ENDIF
	
	GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[iAvailableIndex].sID = sPickupID
	GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[iAvailableIndex].vLocation = vLocation
	GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[iAvailableIndex].iPlacementFlags = iPlacementFlags
	GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[iAvailableIndex].iAmount = iAmount
	GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[iAvailableIndex].eModel = eModel
	
	RETURN sPickupID
ENDFUNC

STRUCT SCRIPT_EVENT_DATA_SYNCED_AMBIENT_PICKUP_ID
	STRUCT_EVENT_COMMON_DETAILS Details
	SAP_PICKUP_ID sPickupID
ENDSTRUCT

PROC CLEANUP_SYNCED_AMBIENT_PICKUP(SAP_PICKUP_ID& sPickupID)
	PRINTLN("[SYNCED_AMBIENT_PICKUPS] CLEANUP_SYNCED_AMBIENT_PICKUP - eType = ", GET_AMBIENT_PICKUP_NAME(sPickupID.eType), ", vInitialLocation = ", sPickupID.vInitialLocation)
	DEBUG_PRINTCALLSTACK()
	
	INT iPlayerFlags = ALL_PLAYERS()
	
	IF iPlayerFlags != 0
		SCRIPT_EVENT_DATA_SYNCED_AMBIENT_PICKUP_ID sEventData
		sEventData.Details.Type = SCRIPT_EVENT_CLEANUP_SYNCED_AMBIENT_PICKUP
		sEventData.Details.FromPlayerIndex = PLAYER_ID()
		
		sEventData.sPickupID = sPickupID
		
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] CLEANUP_SYNCED_AMBIENT_PICKUP - iPlayersFlag = ", iPlayerFlags, ". Broadcasting at GET_CLOUD_TIME_AS_INT() = ", GET_CLOUD_TIME_AS_INT())
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sEventData, SIZE_OF(sEventData), iPlayerFlags)
	ENDIF
ENDPROC

PROC DELETE_SYNCED_AMBIENT_PICKUP(SAP_PICKUP_ID& sPickupID)
	PRINTLN("[SYNCED_AMBIENT_PICKUPS] DELETE_SYNCED_AMBIENT_PICKUP - eType = ", GET_AMBIENT_PICKUP_NAME(sPickupID.eType), ", vInitialLocation = ", sPickupID.vInitialLocation)
	DEBUG_PRINTCALLSTACK()
	
	INT iPlayerFlags = ALL_PLAYERS()
	
	IF iPlayerFlags != 0
		SCRIPT_EVENT_DATA_SYNCED_AMBIENT_PICKUP_ID sEventData
		sEventData.Details.Type = SCRIPT_EVENT_DELETE_SYNCED_AMBIENT_PICKUP
		sEventData.Details.FromPlayerIndex = PLAYER_ID()
		
		sEventData.sPickupID = sPickupID
		
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] DELETE_SYNCED_AMBIENT_PICKUP - iPlayerFlags = ", iPlayerFlags, ". Broadcasting at GET_CLOUD_TIME_AS_INT() = ", GET_CLOUD_TIME_AS_INT())
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sEventData, SIZE_OF(sEventData), iPlayerFlags)
	ENDIF
ENDPROC

STRUCT SCRIPT_EVENT_DATA_SYNCED_AMBIENT_PICKUP_COORDS
	STRUCT_EVENT_COMMON_DETAILS Details
	SAP_PICKUP_ID sPickupID
	
	VECTOR vCoords
ENDSTRUCT

PROC SET_SYNCED_AMBIENT_PICKUP_COORDS(SAP_PICKUP_ID& sPickupID, VECTOR vCoords)
	PRINTLN("[SYNCED_AMBIENT_PICKUPS] SET_SYNCED_AMBIENT_PICKUP_COORDS - eType = ", GET_AMBIENT_PICKUP_NAME(sPickupID.eType), ", vInitialLocation = ", sPickupID.vInitialLocation)
	DEBUG_PRINTCALLSTACK()
	
	OBJECT_INDEX oiPickup
	
	INT i = FIND_SYNCED_AMBIENT_PICKUP_REQUEST_INDEX(sPickupID)
	
	IF i != -1
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] SET_SYNCED_AMBIENT_PICKUP_COORDS - Found pickup request at i = ", i)
		
		oiPickup = GlobalplayerBD_FM_4[NETWORK_PLAYER_ID_TO_INT()].sSAPClient.sPickupRequests[i].sID.oiPickup
		GlobalplayerBD_FM_4[NETWORK_PLAYER_ID_TO_INT()].sSAPClient.sPickupRequests[i].vLocation = vCoords
	ELSE
		i = FIND_SYNCED_AMBIENT_PICKUP_INDEX(sPickupID)
			
		IF i != -1
			PRINTLN("[SYNCED_AMBIENT_PICKUPS] SET_SYNCED_AMBIENT_PICKUP_COORDS - Found pickup at i = ", i)
			
			oiPickup = g_sSAPPickups[i].oiPickup
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiPickup)
		#IF IS_DEBUG_BUILD
		VECTOR vCurrentCoords = GET_ENTITY_COORDS(oiPickup)
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] SET_SYNCED_AMBIENT_PICKUP_COORDS - Updating co-ordinates from ", vCurrentCoords, " to ", vCoords)
		#ENDIF
		
		SET_ENTITY_COORDS(oiPickup, vCoords)
	ENDIF
	
	INT iPlayerFlags = ALL_PLAYERS()
	
	IF iPlayerFlags != 0
		SCRIPT_EVENT_DATA_SYNCED_AMBIENT_PICKUP_COORDS sEventData
		sEventData.Details.Type = SCRIPT_EVENT_SET_SYNCED_AMBIENT_PICKUP_COORDS
		sEventData.Details.FromPlayerIndex = PLAYER_ID()
		
		sEventData.sPickupID = sPickupID
		sEventData.vCoords = vCoords
		
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] SET_SYNCED_AMBIENT_PICKUP_COORDS - iPlayerFlags = ", iPlayerFlags, ". Broadcasting at GET_CLOUD_TIME_AS_INT() = ", GET_CLOUD_TIME_AS_INT())
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, sEventData, SIZE_OF(sEventData), iPlayerFlags)
	ENDIF
ENDPROC

FUNC BOOL IS_PICKUP_TYPE_CASH(PICKUP_TYPE eType)
	SWITCH eType
		CASE PICKUP_MONEY_VARIABLE
		CASE PICKUP_MONEY_CASE
		CASE PICKUP_MONEY_WALLET
		CASE PICKUP_MONEY_PURSE
		CASE PICKUP_MONEY_DEP_BAG
		CASE PICKUP_MONEY_MED_BAG
		CASE PICKUP_MONEY_PAPER_BAG
		CASE PICKUP_MONEY_SECURITY_CASE
		CASE PICKUP_GANG_ATTACK_MONEY
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_ENABLE_SYNCED_AMBIENT_PICKUPS()
	RETURN g_sMPTunables.bEnableSyncedAmbientPickups
ENDFUNC

/// PURPOSE:
///    This is a wrapper function for creating networked ambient pickups.
/// PARAMS:
///    Type - The type of pickup to create.
///    VecNewCoors - The world co-ordinates of the pickup.
///    PlacementFlags - Placement options for the pickup (refer to PLACEMENT_FLAG).
///    Amount - An amount that can be specified for some pickups (e.g. money).
///    CustomModel - If specified, this will override the default model for the pickup type.
///    bCreateAsScriptObject - If TRUE, the pickup will persist until it is cleaned up. If SHOULD_ENABLE_SYNCED_AMBIENT_PICKUPS() = FALSE, the pickup will clean up automatically when the creating script terminates.
///    ScriptHostObject = If TRUE, the pickup was created by the script host and will exist regardless of the current host. If FALSE, it was created by a client and can be removed when they leave the script session. Only used if bCreateAsScriptObject = TRUE and SHOULD_ENABLE_SYNCED_AMBIENT_PICKUPS() = FALSE or Type is not a cash pickup.
/// RETURNS:
///    The ID of the created pickup.
FUNC SAP_PICKUP_ID CREATE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(PICKUP_TYPE Type, VECTOR VecNewCoors, INT PlacementFlags = 0, INT Amount = -1, MODEL_NAMES CustomModel = 0, BOOL bCreateAsScriptObject = FALSE, BOOL ScriptHostObject = TRUE)
	SAP_PICKUP_ID sPickupID
	
	IF SHOULD_ENABLE_SYNCED_AMBIENT_PICKUPS()
	AND IS_PICKUP_TYPE_CASH(Type)
		sPickupID = CREATE_SYNCED_AMBIENT_PICKUP(Type, VecNewCoors, PlacementFlags, Amount, CustomModel, bCreateAsScriptObject)
	ELSE
		sPickupID.oiPickup = CREATE_AMBIENT_PICKUP(Type, VecNewCoors, PlacementFlags, Amount, CustomModel, bCreateAsScriptObject, ScriptHostObject)
	ENDIF
	
	RETURN sPickupID
ENDFUNC

/// PURPOSE:
///    This is a wrapper function for setting synced ambient pickups as no longer needed.
/// PARAMS:
///    sPickupID - The synced ambient pickup ID.
PROC CLEANUP_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(SAP_PICKUP_ID& sPickupID)
	IF SHOULD_ENABLE_SYNCED_AMBIENT_PICKUPS()
	AND IS_PICKUP_TYPE_CASH(sPickupID.eType)
		CLEANUP_SYNCED_AMBIENT_PICKUP(sPickupID)
	ELSE
		SET_ENTITY_AS_NO_LONGER_NEEDED(sPickupID.oiPickup)
	ENDIF
ENDPROC

/// PURPOSE:
///    This is a wrapper function for deleting synced ambient pickups.
/// PARAMS:
///    sPickupID - The synced ambient pickup ID.
PROC DELETE_AMBIENT_PICKUP_WITH_SAP_PICKUP_ID(SAP_PICKUP_ID& sPickupID)
	IF SHOULD_ENABLE_SYNCED_AMBIENT_PICKUPS()
	AND IS_PICKUP_TYPE_CASH(sPickupID.eType)
		DELETE_SYNCED_AMBIENT_PICKUP(sPickupID)
	ELSE
		DELETE_OBJECT(sPickupID.oiPickup)
	ENDIF
ENDPROC

/// PURPOSE:
///    This is a wrapper function for setting the co-ordinates of synced ambient pickups.
/// PARAMS:
///    sPickupID - The synced ambient pickup ID.
///    VecNewCoors - The new co-ordinates for the pickup.
PROC SET_AMBIENT_PICKUP_COORDS_WITH_SAP_PICKUP_ID(SAP_PICKUP_ID& sPickupID, VECTOR VecNewCoors)
	IF SHOULD_ENABLE_SYNCED_AMBIENT_PICKUPS()
	AND IS_PICKUP_TYPE_CASH(sPickupID.eType)
		SET_SYNCED_AMBIENT_PICKUP_COORDS(sPickupID, VecNewCoors)
	ELSE
		SET_ENTITY_COORDS(sPickupID.oiPickup, VecNewCoors)
	ENDIF
ENDPROC
