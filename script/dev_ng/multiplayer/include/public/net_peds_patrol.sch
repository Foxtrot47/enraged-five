//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_PATROL.sch																						//
// Description: Header file containing path finding functionality for peds.												//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		26/02/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "net_peds_common.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ SET PATROL DATA ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛


/// PURPOSE:
///    Gets the ped patrol anim dictionaries for a ped location.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    iAnimDictID - ID of the anim dictionary.
/// RETURNS: The ped patrol anim dictionary as a STRING.
FUNC STRING GET_PED_PATROL_ANIM_DICT(PED_LOCATIONS ePedLocation, INT iAnimDictID)
	STRING sAnimDict = ""
	
	SWITCH ePedLocation
		CASE PED_LOCATION_CAR_MEET
			SWITCH iAnimDictID
				CASE 0	sAnimDict = "ANIM@AMB@INSPECT@STAND@MALE_A@BASE"					BREAK
				CASE 1	sAnimDict = "ANIM@AMB@INSPECT@STAND@MALE_A@IDLES"					BREAK
				CASE 2	sAnimDict = "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@ENTER"		BREAK
				CASE 3	sAnimDict = "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@BASE"		BREAK
				CASE 4	sAnimDict = "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@IDLE_A"		BREAK
				CASE 5	sAnimDict = "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@EXIT"		BREAK
				CASE 6	sAnimDict = "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@ENTER"		BREAK
				CASE 7	sAnimDict = "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@BASE"		BREAK
				CASE 8	sAnimDict = "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@IDLE_A"	BREAK
				CASE 9	sAnimDict = "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@EXIT"		BREAK
			ENDSWITCH
		BREAK
		DEFAULT
		BREAK
	ENDSWITCH
	
	RETURN sAnimDict
ENDFUNC

/// PURPOSE:
///    Request and loads the ped patrol anim dictionaries for a ped location.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    LocalData - Local ped data to set.
/// RETURNS: TRUE when all the ped patrol anim dictionaries have been requested and loaded. FALSE otherwise.
FUNC BOOL REQUEST_AND_LOAD_PED_PATROL_ANIM_DICTS(PED_LOCATIONS ePedLocation, SCRIPT_PED_DATA &LocalData)
	
	IF IS_PEDS_BIT_SET(LocalData.iBS, BS_PED_LOCAL_DATA_PATROL_ANIM_DICTS_LOADED)
		RETURN TRUE
	ENDIF
	
	INT iAnimDictID			= 0
	INT iMaxAnimDicts		= 0
	INT iLoadedAnimDicts	= 0
	STRING sAnimDict 		= ""
	BOOL bAnimDictsLoaded	= FALSE
	
	REPEAT MAX_NUM_PED_PATROL_ANIM_DICTS iAnimDictID
		sAnimDict = GET_PED_PATROL_ANIM_DICT(ePedLocation, iAnimDictID)
		IF NOT IS_STRING_NULL_OR_EMPTY(sAnimDict)
			IF REQUEST_AND_LOAD_ANIMATION_DICT(sAnimDict)
				iLoadedAnimDicts++
			ENDIF
			iMaxAnimDicts++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	IF (iLoadedAnimDicts = iMaxAnimDicts)
		SET_PEDS_BIT(LocalData.iBS, BS_PED_LOCAL_DATA_PATROL_ANIM_DICTS_LOADED)
		bAnimDictsLoaded = TRUE
	ENDIF
	
	RETURN bAnimDictsLoaded
ENDFUNC

/// PURPOSE:
///    Unloads the ped patrol anim dictionaries for a ped location.
/// PARAMS:
///    ePedLocation - Which location is being queried.
PROC UNLOAD_PED_PATROL_ANIM_DICTS(PED_LOCATIONS ePedLocation)
	
	INT iAnimDictID		= 0
	STRING sAnimDict 	= ""
	
	REPEAT MAX_NUM_PED_PATROL_ANIM_DICTS iAnimDictID
		sAnimDict = GET_PED_PATROL_ANIM_DICT(ePedLocation, iAnimDictID)
		IF NOT IS_STRING_NULL_OR_EMPTY(sAnimDict)
			REMOVE_ANIM_DICT(sAnimDict)
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Sets the patrol sequence task data for a peds.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    PatrolData - Patrol data to set.
///    iLayout - Layout to set patrol data for.
///    iArrayID - Patrol ped array ID to populate data for.
///    iPatrolPedID - Array that holds the each peds patrol array ID.
///    bNetworkPed - Whether the ped is networked.
PROC SET_PATROL_DATA(PED_LOCATIONS ePedLocation, PATROL_DATA &PatrolData, INT iLayout, INT iArrayID, INT &iPatrolPedID[], INT &iMaxSequence, BOOL bNetworkPed = FALSE)
	
	INT iTask
	REPEAT MAX_NUM_PED_TASK_SEQUENCES iTask
		SET_PED_PATROL_DATA(ePedLocation, PatrolData.seqPatrol[iTask], iLayout, iArrayID, iPatrolPedID, PatrolData.bShowProp, iTask, iMaxSequence, bNetworkPed)
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Sets the local ped patrol data.
///    Populates the local patrol array. Array size: MAX_NUM_LOCAL_PATROL_PEDS
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All local ped data to query.
PROC SET_PED_PATROL_LOCAL_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	INT iArrayID
	REPEAT MAX_NUM_LOCAL_PATROL_PEDS iArrayID
		RESET_PED_PATROL_DATA(LocalData.PatrolData[iArrayID])
		SET_PATROL_DATA(ePedLocation, LocalData.PatrolData[iArrayID], ServerBD.iLayout, iArrayID, LocalData.iPatrolPedID, LocalData.iMaxLocalPatrolSequence[iArrayID])
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Sets the network ped patrol data.
///    Populates the network patrol array. Array size: MAX_NUM_NETWORK_PATROL_PEDS
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
PROC SET_PED_PATROL_NETWORK_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	
	INT iArrayID
	REPEAT MAX_NUM_NETWORK_PATROL_PEDS iArrayID
		RESET_PED_PATROL_DATA(LocalData.NetworkPatrolData[iArrayID])
		SET_PATROL_DATA(ePedLocation, LocalData.NetworkPatrolData[iArrayID], ServerBD.iLayout, iArrayID, LocalData.iNetworkPatrolPedID, LocalData.iMaxNetworkPatrolSequence[iArrayID], TRUE)
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Initialises the ped patrol local array.
///    Sets all elements to -1.
/// PARAMS:
///    LocalData - Local ped data to set.
PROC INITIALISE_PED_PATROL_LOCAL_ARRAY_ID(SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iPed
		LocalData.iPatrolPedID[iPed] = -1
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Initialises the ped patrol network array.
///    Sets all elements to -1.
/// PARAMS:
///    ServerBD - Server ped data to set.
PROC INITIALISE_PED_PATROL_NETWORK_ARRAY_ID(SCRIPT_PED_DATA &LocalData)
	
	INT iPed
	REPEAT MAX_NUM_TOTAL_NETWORK_PEDS iPed
		LocalData.iNetworkPatrolPedID[iPed] = -1
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Sets the patrol sequence task data for all peds.
///    Populates the patrol arrays. 
///    Local array max: MAX_NUM_LOCAL_PATROL_PEDS
///    Network array size: MAX_NUM_NETWORK_PATROL_PEDS
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All local ped data to query.
PROC SET_ALL_PED_PATROL_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData)
	INITIALISE_PED_PATROL_LOCAL_ARRAY_ID(LocalData)
	SET_PED_PATROL_LOCAL_DATA(ePedLocation, ServerBD, LocalData)
	
	INITIALISE_PED_PATROL_NETWORK_ARRAY_ID(LocalData)
	SET_PED_PATROL_NETWORK_DATA(ePedLocation, ServerBD, LocalData)
	
	REQUEST_AND_LOAD_PED_PATROL_ANIM_DICTS(ePedLocation, LocalData)
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ TASK PATROL ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Sets a network patrol peds props visibility state locally.
/// PARAMS:
///    ePedLocation - Ped location.
///    LocalData - Local data to wuery.
///    ServerBD - Server data to query.
PROC MAINTAIN_NETWORK_PED_PATROL_PROPS_VISIBLE_STATE(PED_LOCATIONS ePedLocation, SCRIPT_PED_DATA &LocalData, SERVER_PED_DATA &ServerBD)
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
	OR IS_CUTSCENE_RUNNING_FOR_PED_LOCATION(ePedLocation)
		EXIT
	ENDIF
	
	INT iProp
	INT iPedID
	INT iPatrolTask
	INT iPatrolPedID
	
	REPEAT MAX_NUM_NETWORK_PATROL_PEDS iPedID
		iPatrolPedID = LocalData.iNetworkPatrolPedID[iPedID]
		IF (iPatrolPedID != -1)
			iPatrolTask = ServerBD.iPatrolTask[iPatrolPedID]-1
			IF (iPatrolTask < 0)
				iPatrolTask = LocalData.iMaxNetworkPatrolSequence[iPatrolPedID]
			ENDIF
			REPEAT MAX_NUM_PED_PROPS iProp
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerBD.NetworkPedData[iPatrolPedID].niPropID[iProp])
					// Per frame instructions
					IF LocalData.NetworkPatrolData[iPatrolPedID].bShowProp[iPatrolTask]
						SET_ENTITY_LOCALLY_VISIBLE(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[iPatrolPedID].niPropID[iProp]))
					ELSE
						SET_ENTITY_LOCALLY_INVISIBLE(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[iPatrolPedID].niPropID[iProp]))
					ENDIF
				ELSE
					BREAKLOOP
				ENDIF
			ENDREPEAT
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Changes the walking animation for the patrol peds. This data is filled in the location specific header (GET_PED_ANIM_DATA)
/// PARAMS:
///    ePedLocation - Ped location.
///    LocalData - Local data to wuery.
///    ServerBD - Server data to query.
PROC MAINTAIN_NETWORK_PED_PATROL_ALTERNATIVE_WALKING_ANIMATIONS(PED_LOCATIONS ePedLocation, SCRIPT_PED_DATA &LocalData, SERVER_PED_DATA &ServerBD)
	
//	//IF ANIMATIONS WERE ALREADY SET WE CAN JUST LEAVE
	IF IS_PEDS_BIT_SET(LocalData.iBS, BS_PED_LOCAL_DATA_ALTERNATIVE_WALK_ANIMATIONS_SET)
		EXIT
	ENDIF


	INT iPedID
	INT iPatrolPedID
	PED_ANIM_DATA animData
	REPEAT MAX_NUM_NETWORK_PATROL_PEDS iPedID
		iPatrolPedID = LocalData.iNetworkPatrolPedID[iPedID]
		IF (iPatrolPedID != -1)	
		
			//IF PATROL PED IS SUPPOSED TO USE ALTERNATIVE ANIMS
			IF IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPatrolPedID].Data.iBS, BS_PED_DATA_USE_ALTERNATIVE_WALK_ANIMATION)
			
			
				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerBD.NetworkPedData[iPatrolPedID].niPedID)
					PRINTLN("[PATROL PEDS][ALTERNATIVE_WALKING_ANIMATIONS] PATROL PED ", iPatrolPedID, " doesn't exist yet.")
					EXIT
				ENDIF
				
				IF NOT NETWORK_HAS_CONTROL_OF_ENTITY(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[iPatrolPedID].niPedID))
					PRINTLN("[PATROL PEDS][ALTERNATIVE_WALKING_ANIMATIONS] NETWORK_HAS_CONTROL_OF_ENTITY = FALSE  PATROL PED: ", iPatrolPedID)	
					EXIT
				ENDIF
				
				IF NOT IS_ENTITY_ALIVE(NET_TO_ENT(ServerBD.NetworkPedData[iPatrolPedID].niPedID))
					PRINTLN("[PATROL PEDS][ALTERNATIVE_WALKING_ANIMATIONS] PATROL PED ", iPatrolPedID, " is not alive. (IS_ENTITY_ALIVE = false)")
					EXIT
				ENDIF
				
				//GETTING CLIP INFO FROM PED_ACT
				RESET_PED_ANIM_DATA(animData)
				GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, ServerBD.NetworkPedData[iPatrolPedID].Data.iActivity), animData)
				
				// Make sure the anim dict is in memory
				IF NOT REQUEST_AND_LOAD_ANIMATION_DICT(animData.sAnimDict)
					PRINTLN("[PATROL PEDS][ALTERNATIVE_WALKING_ANIMATIONS] PATROL PED ", iPatrolPedID, " animations are not loaded yet")
					EXIT		
				ENDIF
				
				SET_PED_ALTERNATE_WALK_ANIM(GET_PED_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[iPedID].niPedID)), animData.sAnimDict, animData.sAnimClip)	
				PRINTLN("[PATROL PEDS][ALTERNATIVE_WALKING_ANIMATIONS] PATROL PED ", iPatrolPedID, " SET_PED_ALTERNATE_WALK_ANIM (",animData.sAnimDict,")")
			ENDIF						
		ENDIF
	ENDREPEAT
	
	//if script reached this point it means alternative walking anim was set for every patrolling ped, no need to call any of this again
	SET_PEDS_BIT(LocalData.iBS, BS_PED_LOCAL_DATA_ALTERNATIVE_WALK_ANIMATIONS_SET)
ENDPROC

/// PURPOSE:
///    Tasks a ped to perform the next patrol sequence.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    PedID - Network ped index to task.
///    PatrolData - Patrol data to query.
///    Data - Ped data to query.
///    iPatrolPedID - ID of the ped patrolling.
///    iLayout - Layout to query.
///    bNetworkPed - Whether the ped is networked or not.
PROC TASK_PATROL_PED(PED_INDEX &PedID, PATROL_DATA &PatrolData, PEDS_DATA &Data, INT &iPatrolTask, INT iMaxSequence, BOOL bNetworkPed = FALSE)
	SCRIPTTASKSTATUS TaskStatus = GET_SCRIPT_TASK_STATUS(PedID, SCRIPT_TASK_PERFORM_SEQUENCE)
	
	// Task Status is "FINISHED_TASK" by default on initialise
	IF (TaskStatus = FINISHED_TASK)
		
		// Perform sequence task
		TASK_PERFORM_SEQUENCE(PedID, PatrolData.seqPatrol[iPatrolTask])
		
		// Hide/show props
		IF NOT bNetworkPed
			SET_LOCAL_PED_PROPS_VISIBLE(Data, PatrolData.bShowProp[iPatrolTask])
		ENDIF
		
		iPatrolTask++
		IF (iPatrolTask > iMaxSequence)
			iPatrolTask = 0
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Update a local peds patrol sequence. Plays all patrol tasks from GET_PED_PATROL_DATA() in order. Loops entire sequence.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    LocalData - Local ped data to query.
///    iPedID - ID of the ped to query.
///    iLayout - Layout to query.
PROC UPDATE_LOCAL_PED_PATROL(PED_LOCATIONS ePedLocation, SCRIPT_PED_DATA &LocalData, INT iPedID)
	
	IF NOT IS_PEDS_BIT_SET(LocalData.PedData[iPedID].iBS, BS_PED_DATA_USE_PATROL)
		EXIT
	ENDIF
	
	IF NOT REQUEST_AND_LOAD_PED_PATROL_ANIM_DICTS(ePedLocation, LocalData)
		EXIT
	ENDIF
	
	IF (LocalData.iPatrolPedID[iPedID] != -1)
		TASK_PATROL_PED(LocalData.PedData[iPedID].PedID, LocalData.PatrolData[LocalData.iPatrolPedID[iPedID]], LocalData.PedData[iPedID], LocalData.PatrolData[LocalData.iPatrolPedID[iPedID]].iPatrolTask, LocalData.iMaxLocalPatrolSequence[LocalData.iPatrolPedID[iPedID]])
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Update a network peds patrol sequence. Plays all patrol tasks from GET_PED_PATROL_DATA() in order. Loops entire sequence.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - Local ped data to query.
///    iPedID - ID of the ped to query.
PROC UPDATE_NETWORK_PED_PATROL(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, INT iPedID)
	
	IF NOT IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPedID].Data.iBS, BS_PED_DATA_USE_PATROL)
		EXIT
	ENDIF
	
	IF NOT REQUEST_AND_LOAD_PED_PATROL_ANIM_DICTS(ePedLocation, LocalData)
		EXIT
	ENDIF
		
	IF (LocalData.iNetworkPatrolPedID[iPedID] != -1)
		TASK_PATROL_PED(ServerBD.NetworkPedData[iPedID].Data.PedID, LocalData.NetworkPatrolData[LocalData.iNetworkPatrolPedID[iPedID]], ServerBD.NetworkPedData[iPedID].Data, ServerBD.iPatrolTask[LocalData.iNetworkPatrolPedID[iPedID]], LocalData.iMaxNetworkPatrolSequence[LocalData.iNetworkPatrolPedID[iPedID]], TRUE)
	ENDIF
	
ENDPROC
#ENDIF	// FEATURE_HEIST_ISLAND
