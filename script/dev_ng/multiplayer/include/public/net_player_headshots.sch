USING "rage_builtins.sch"
USING "globals.sch"

USING "net_team_info.sch"
USING "transition_controller.sch"

#IF IS_DEBUG_BUILD
    USING "net_player_headshots_debug.sch"
#ENDIF




// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Player_Headshots.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Contains all Player Headshot Generation and Maintenance routines.
//		NOTES			:	A player headshot takes time to create, although usually only a few frames.
//							Player Headshots can be re-used while they have been registered.
//							A TXD can be retrieved from a Player Headshot as many times as needed while the headshot is registered.
//							Headshots auto-cleanup after a period non-use.
//							Some uses of headshots will need to keep the headshot alive - eg: those used on some cellphone screens.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************

#IF IS_DEBUG_BUILD
	//SPAM SLAM
	//Here for the playthrough build on 05/04/2013 where it was shakey if the headshots would fuck up or not
	INT iSpamSlams = 0
	BOOL bSlamSpam = FALSE
#ENDIF


// ===========================================================================================================
//      Player Headshot Routines
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//      General Headshot Routines
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear a headshot data struct
//
// RETURN PARAMS:			paramHeadshotData		The struct that needs to be cleared out
PROC Clear_One_Set_Of_MP_Headshot_Data(g_structHeadshotDetailsMP &paramHeadshotData)

	// Clear the data
	paramHeadshotData.hsdActive				= FALSE
	paramHeadshotData.hsdPlayerID			= INVALID_PLAYER_INDEX()
	paramHeadshotData.hsdHeadshotID			= NULL
	paramHeadshotData.hsdKeepActive			= FALSE

	IF (NETWORK_IS_GAME_IN_PROGRESS())
		paramHeadshotData.hsdTimeout		= GET_NETWORK_TIME()
	ENDIF

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Generated Headshot Routines
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear all the generated headshots data
//
// INPUT PARAMS:		paramBeingInitialised		[DEFAULT = FALSE] TRUE during initialisation, FALSE during termination, because generated headshots need unregistered
PROC Clear_All_Generated_MP_Headshots(BOOL paramBeingInitialised = FALSE)

	#IF IS_DEBUG_BUILD
	BOOL netGameInProgress = NETWORK_IS_GAME_IN_PROGRESS()
//	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	// KGM 26/9/14 [BUG 1946559]: Need to let code know that the headshot texture is about to be freed - supply a 'default head' replacement
	TEXT_LABEL_63 theDefaultHeadshot = "CHAR_DEFAULT"
	TEXT_LABEL_63 theOldTXD = ""

	INT tempLoop = 0
	REPEAT MAX_MP_HEADSHOTS tempLoop
		IF NOT (paramBeingInitialised)
			IF (tempLoop < g_numGeneratedHeadshotsMP)
				IF (IS_PEDHEADSHOT_VALID(g_sGeneratedHeadshotsMP[tempLoop].hsdHeadshotID))
					// NOTE: In console logs (code+script), search for PedHeadshotManager to get a refcount of registered/unregistered headshots when bug hunting
					// (requires -pedheadshotdebug in command line)
					
					// KGM 26/9/14 [BUG 1946559]: For the Brief screen, need to let code know that the headshot texture is about to be freed - supply a 'default head' replacement
					IF (g_sGeneratedHeadshotsMP[tempLoop].hsdHeadshotID != NULL)
						theOldTXD = GET_PEDHEADSHOT_TXD_STRING(g_sGeneratedHeadshotsMP[tempLoop].hsdHeadshotID)
						CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== CLEAR ALL === This would replace [", theOldTXD, "] with [", theDefaultHeadshot, "] - used by the Brief")
						THEFEED_UPDATE_ITEM_TEXTURE(theOldTXD, theOldTXD, theDefaultHeadshot, theDefaultHeadshot)
					ELSE
						CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== CLEAR ALL === HeadshotID is NULL, so not supplying a CHAR_DEFAULT update to code for the Brief")
					ENDIF
					
					UNREGISTER_PEDHEADSHOT(g_sGeneratedHeadshotsMP[tempLoop].hsdHeadshotID)
	
					#IF IS_DEBUG_BUILD
						IF (netGameInProgress)
						AND (IS_NET_PLAYER_OK(g_sGeneratedHeadshotsMP[tempLoop].hsdPlayerID, FALSE))
							CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== CLEAR ALL === Unregistering Headshot for player: ", 
															GET_PLAYER_NAME(g_sGeneratedHeadshotsMP[tempLoop].hsdPlayerID))
						ELSE
							CPRINTLN(DEBUG_NET_HEADSHOTS, "=== CLEAR ALL === Unregistering Headshot for unknown player, must have left the game")
						ENDIF
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		Clear_One_Set_Of_MP_Headshot_Data(g_sGeneratedHeadshotsMP[tempLoop])
	ENDREPEAT
	
	g_numGeneratedHeadshotsMP = 0
	
	
	// Also start the scheduled maintenance variable on slot 0
	g_nextHeadshotUpdateSlotMP = 0
	
	
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_HEADSHOTS, "=== CLEAR ALL === Cleared All Generated Player Headshot Data")
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Delete the generated headshot in the specified slot and shuffle all entries above it down on eplace to fill the gap
//
// INPUT PARAMS:		paramSlot				The slot where the timeout needs to change
PROC Delete_Generated_MP_Headshot(INT paramSlot)

	IF (paramSlot >= g_numGeneratedHeadshotsMP)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_HEADSHOTS, "=== DELETE === Slot not in use. Slot Requested: ", paramSlot, "   Slots In Use: ", g_numGeneratedHeadshotsMP)
			SCRIPT_ASSERT("Delete_Generated_MP_Headshot(). Requested Slot is not in use. See Console Log. Tell Keith.")
		#ENDIF
		EXIT
	ENDIF
	
	// Unregister the headshot
	IF (IS_PEDHEADSHOT_VALID(g_sGeneratedHeadshotsMP[paramSlot].hsdHeadshotID))
		// NOTE: In console logs (code+script), search for PedHeadshotManager to get a refcount of registered/unregistered headshots when bug hunting
		// (requires -pedheadshotdebug in command line)
		
		// KGM 26/9/14 [BUG 1946559]: For the Brief screen, need to let code know that the headshot texture is about to be freed - supply a 'default head' replacement
		TEXT_LABEL_63 theDefaultHeadshot = "CHAR_DEFAULT"
		IF (g_sGeneratedHeadshotsMP[paramSlot].hsdHeadshotID != NULL)
			TEXT_LABEL_63 theOldTXD = GET_PEDHEADSHOT_TXD_STRING(g_sGeneratedHeadshotsMP[paramSlot].hsdHeadshotID)
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== DELETE === This would replace [", theOldTXD, "] with [", theDefaultHeadshot, "] - used by the Brief")
			THEFEED_UPDATE_ITEM_TEXTURE(theOldTXD, theOldTXD, theDefaultHeadshot, theDefaultHeadshot)
		ELSE
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== DELETE === HeadshotID is NULL, so not supplying a CHAR_DEFAULT update to code for the Brief")
		ENDIF
		
		UNREGISTER_PEDHEADSHOT(g_sGeneratedHeadshotsMP[paramSlot].hsdHeadshotID)
		
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== DELETE === Unregistering Generated Headshot for player: ", 
											GET_PLAYER_NAME(g_sGeneratedHeadshotsMP[paramSlot].hsdPlayerID))
		#ENDIF
	ENDIF

	// Shuffle the rest of the entries down to fill the gap
	INT toPos	= paramSlot
	INT fromPos	= toPos + 1
	
	WHILE (fromPos < g_numGeneratedHeadshotsMP)
		g_sGeneratedHeadshotsMP[toPos] = g_sGeneratedHeadshotsMP[fromPos]
		toPos++
		fromPos++
	ENDWHILE
	
	// Clear the data at the end of the array
	Clear_One_Set_Of_MP_Headshot_Data(g_sGeneratedHeadshotsMP[toPos])
	
	// Decrement the number of registered headshots
	g_numGeneratedHeadshotsMP--

	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== DELETE === Deleted Generated Headshot in Slot: ", paramSlot)
		Debug_Output_MP_Headshots()
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the initial retain time to the timeout in the specified slot if later than the existing timeout
//
// INPUT PARAMS:		paramSlot				The slot where the timeout needs to change
//
// NOTES:	Just overwrite the existing value, also remove the 'keep active' flag - it's not needed if the timeout has been extended
PROC Store_Minimum_Generated_MP_Headshot_Timeout_For_Slot(INT paramSlot)
	g_sGeneratedHeadshotsMP[paramSlot].hsdTimeout		= GET_TIME_OFFSET(GET_NETWORK_TIME(), MP_HEADSHOT_INITIAL_RETAIN_TIME_msec)
	g_sGeneratedHeadshotsMP[paramSlot].hsdKeepActive	= FALSE
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Store the additional retain time to the timeout in the specified slot
//
// INPUT PARAMS:		paramSlot				The slot where the timeout needs to change
//
// NOTES:	Only store the new time if it is later than the existing time
PROC Update_Generated_MP_Headshot_Timeout_For_Slot(INT paramSlot)

	// Get the timeout using the additional time
	TIME_DATATYPE theAdditionalTime = GET_TIME_OFFSET(GET_NETWORK_TIME(), MP_HEADSHOT_ADDITIONAL_RETAIN_TIME_msec)
	
	// Do nothing if the existing timeout is still greater
	IF (IS_TIME_LESS_THAN(theAdditionalTime, g_sGeneratedHeadshotsMP[paramSlot].hsdTimeout))
		EXIT
	ENDIF

	// Additional time is greater, so update the time
	g_sGeneratedHeadshotsMP[paramSlot].hsdTimeout = theAdditionalTime
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a generated headshot has timed out or if the player has quit MP
// NOTES:	If a headshot gets deleted then the same slot will be maintained on the next frame since the data will have changed.
PROC Maintain_Generated_MP_Headshots()

	// Do nothing if there are no stored headshots
	IF (g_numGeneratedHeadshotsMP = 0)
		EXIT
	ENDIF
	
	// Check if we need to wrap the maintenance check
	IF (g_nextHeadshotUpdateSlotMP >= MAX_MP_HEADSHOTS)
		g_nextHeadshotUpdateSlotMP = 0
	ENDIF
	
	// Bomb out if there is no data in the slot - nothing to maintain
	IF NOT (g_sGeneratedHeadshotsMP[g_nextHeadshotUpdateSlotMP].hsdActive)
		g_nextHeadshotUpdateSlotMP++
		EXIT
	ENDIF
	
	IF g_sGeneratedHeadshotsMP[g_nextHeadshotUpdateSlotMP].hsdPlayerID = INVALID_PLAYER_INDEX()
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_HEADSHOTS, "=== MAINTAIN GENERATED === g_sGeneratedHeadshotsMP[g_nextHeadshotUpdateSlotMP].hsdPlayerID = -1 Player is not OK. Slot ", g_nextHeadshotUpdateSlotMP)
		#ENDIF
		
		Delete_Generated_MP_Headshot(g_nextHeadshotUpdateSlotMP)
		
		EXIT
	
	ENDIF
	
	IF NATIVE_TO_INT(g_sGeneratedHeadshotsMP[g_nextHeadshotUpdateSlotMP].hsdPlayerID)= -1
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_HEADSHOTS, "=== MAINTAIN GENERATED === 2 g_sGeneratedHeadshotsMP[g_nextHeadshotUpdateSlotMP].hsdPlayerID = -1 Player is not OK. Slot ", g_nextHeadshotUpdateSlotMP)
		#ENDIF
		
		Delete_Generated_MP_Headshot(g_nextHeadshotUpdateSlotMP)
		
		EXIT
	
	ENDIF
	
	// Does the player still exist?
	IF NOT (IS_NET_PLAYER_OK(g_sGeneratedHeadshotsMP[g_nextHeadshotUpdateSlotMP].hsdPlayerID, FALSE))
		// ...no
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_HEADSHOTS, "=== MAINTAIN GENERATED === Player is not OK. Slot ", g_nextHeadshotUpdateSlotMP)
		#ENDIF
		
		Delete_Generated_MP_Headshot(g_nextHeadshotUpdateSlotMP)
		
		EXIT
	ENDIF
	
	// Does the slot still contain a valid headshot?
	IF NOT (IS_PEDHEADSHOT_VALID(g_sGeneratedHeadshotsMP[g_nextHeadshotUpdateSlotMP].hsdHeadshotID))
		// ...no
		#IF IS_DEBUG_BUILD
			PLAYER_INDEX playerNameForDebug = g_sGeneratedHeadshotsMP[g_nextHeadshotUpdateSlotMP].hsdPlayerID
		#ENDIF
		
		Delete_Generated_MP_Headshot(g_nextHeadshotUpdateSlotMP)
		
		#IF IS_DEBUG_BUILD
			IF (playerNameForDebug != INVALID_PLAYER_INDEX())
				IF (IS_NET_PLAYER_OK(playerNameForDebug, FALSE))
					CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== MAINTAIN GENERATED === Headshot is no longer valid. Slot: ", g_nextHeadshotUpdateSlotMP, " [", 
												GET_PLAYER_NAME(playerNameForDebug), "]")
				ENDIF
				Debug_Output_MP_Headshots()
			ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	// Does the slot still contain a headshot ready to be used?
	IF NOT (IS_PEDHEADSHOT_READY(g_sGeneratedHeadshotsMP[g_nextHeadshotUpdateSlotMP].hsdHeadshotID))
		// ...no
		#IF IS_DEBUG_BUILD
			PLAYER_INDEX playerNameForDebug = g_sGeneratedHeadshotsMP[g_nextHeadshotUpdateSlotMP].hsdPlayerID
		#ENDIF
		
		Delete_Generated_MP_Headshot(g_nextHeadshotUpdateSlotMP)
		
		#IF IS_DEBUG_BUILD
			IF (playerNameForDebug != INVALID_PLAYER_INDEX())
				IF (IS_NET_PLAYER_OK(playerNameForDebug, FALSE))
					CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== MAINTAIN GENERATED === Headshot is no longer ready. Slot: ", g_nextHeadshotUpdateSlotMP, " [", 
												GET_PLAYER_NAME(playerNameForDebug), "]")
				ENDIF
				Debug_Output_MP_Headshots()
			ENDIF
		#ENDIF
		
		EXIT
	ENDIF
	
	// Has the slot timed out?
	IF (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sGeneratedHeadshotsMP[g_nextHeadshotUpdateSlotMP].hsdTimeout))
		// ...yes, but extend the timer if there has been a request to keep the playerID active since the last timeout
		IF (g_sGeneratedHeadshotsMP[g_nextHeadshotUpdateSlotMP].hsdKeepActive)
			// ...yes, so update the timer
			Update_Generated_MP_Headshot_Timeout_For_Slot(g_nextHeadshotUpdateSlotMP)
			
			// Clear the flag
			g_sGeneratedHeadshotsMP[g_nextHeadshotUpdateSlotMP].hsdKeepActive = FALSE
			
			// There's too much console log output - so this doesn't need any
		
			EXIT
		ENDIF
	
		#IF IS_DEBUG_BUILD
			IF (IS_NET_PLAYER_OK(g_sGeneratedHeadshotsMP[g_nextHeadshotUpdateSlotMP].hsdPlayerID, FALSE))
				CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== MAINTAIN GENERATED === Headshot has timed out. Slot: ", g_nextHeadshotUpdateSlotMP, " [", 
											GET_PLAYER_NAME(g_sGeneratedHeadshotsMP[g_nextHeadshotUpdateSlotMP].hsdPlayerID), "]")
			ENDIF
			Debug_Output_MP_Headshots()
		#ENDIF
		
		Delete_Generated_MP_Headshot(g_nextHeadshotUpdateSlotMP)
		
		EXIT
	ENDIF

	// Next frame update next slot
	g_nextHeadshotUpdateSlotMP++

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get the slot containing an existing Headshot for a player
//
// INPUT PARAMS:		paramPlayerID				PlayerID of the player whose headshot is being generated
// RETURN VALUE:		INT							The Generated Headshot Slot, or NO_HEADSHOT_SLOT if the headshot for this player hasn't been generated
FUNC INT Get_Generated_Headshot_Slot_For_Player(PLAYER_INDEX paramPlayerID)

	// Is requested player OK?
	IF NOT (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
		RETURN NO_HEADSHOT_SLOT
	ENDIF
	
	// Are there any registered headshots?
	IF (g_numGeneratedHeadshotsMP = 0)
		RETURN NO_HEADSHOT_SLOT
	ENDIF

	// Find the requested player's headshot if it has been generated
	INT 	tempLoop		= 0
	
	REPEAT g_numGeneratedHeadshotsMP tempLoop
		IF (g_sGeneratedHeadshotsMP[temploop].hsdPlayerID = paramPlayerID)
			// ...found this player as already registered, so return the headshotID if it is still valid and ready
			IF (IS_PEDHEADSHOT_VALID(g_sGeneratedHeadshotsMP[temploop].hsdHeadshotID))
			AND (IS_PEDHEADSHOT_READY(g_sGeneratedHeadshotsMP[temploop].hsdHeadshotID))
				// ...still valid and ready
				RETURN (tempLoop)
			ENDIF
			
			// Headshot ID is no longer valid or ready, so delete it
			Delete_Generated_MP_Headshot(tempLoop)
			
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== GET GENERATED === Headshot is no longer valid or ready so deleting and returning NULL. Slot: ", 
												temploop, " [", GET_PLAYER_NAME(paramPlayerID), "]")
				Debug_Output_MP_Headshots()
			#ENDIF
			
			RETURN NO_HEADSHOT_SLOT
		ENDIF
	ENDREPEAT
	
	// Didn't find an entry for this player
	RETURN NO_HEADSHOT_SLOT

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Get a slot to store the new generated data
//
// RETURN VALUE:		INT			Array Position within the Generated Headshots array, or NO_HEADSHOT_SLOT if full
//
// NOTES:	Place new data at the end of the array.
//			It should never be FULL.
FUNC INT Get_Slot_For_Newly_Generated_MP_Headshot()

	// If the array isn't full, return the array position beyond the current last entry
	IF (g_numGeneratedHeadshotsMP < MAX_MP_HEADSHOTS)
		RETURN g_numGeneratedHeadshotsMP
	ENDIF
	
	// There is no free slot on the array
	RETURN NO_HEADSHOT_SLOT

ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Add the data in the headshot generation data to the Generated Headshot array if not already there, and updating timeout if it is there
//
// RETURN VALUE:			BOOL		TRUE if the headshot ws registered, FALSE if not
//
// NOTES:	Assumes the VALID and READY checks have been performed
FUNC BOOL Add_Headshot_Being_Generated_To_Generated_MP_Headshots()

	// Ensure there is generated headshot data
	IF NOT (g_sHeadshotBeingGeneratedMP.hsdActive)
		RETURN FALSE
	ENDIF
	
	// If the headshot already exists then update the timer since it has been newly requested again?
	INT existingSlot = Get_Generated_Headshot_Slot_For_Player(g_sHeadshotBeingGeneratedMP.hsdPlayerID)
	IF (existingSlot != NO_HEADSHOT_SLOT)
		// ...data was already on the array so update the timeout to the minimum retain time
		Store_Minimum_Generated_MP_Headshot_Timeout_For_Slot(existingSlot)
		
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== ADD GENERATED === Updating existing headshot in slot: ", existingSlot,
											" [", GET_PLAYER_NAME(g_sGeneratedHeadshotsMP[existingSlot].hsdPlayerID), "]")
			Debug_Output_MP_Headshots()
		#ENDIF
		
		RETURN TRUE
	ENDIF
	
	// The data isn't on the registered array, so add it
	INT newSlot = Get_Slot_For_Newly_Generated_MP_Headshot()
	IF (newSlot = NO_HEADSHOT_SLOT)
		// The array is full, it shouldn't be because I should ensure there's space before accepting a headshot for generation
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_HEADSHOTS, "=== ADD GENERATED === ERROR: There is no space on the Generated Headshots array.")
			Debug_Output_MP_Headshots()
			SCRIPT_ASSERT("Add_Headshot_To_Generated_MP_Headshots(): ERROR - Generated Headshot array is full. This shouldn't happen. See Console Log. Tell Keith.")
		#ENDIF
		
		// Failed to register headshot
		RETURN FALSE
	ENDIF
	
	// Store the Headshot Being Generated in the Generated Headshots array
	g_sGeneratedHeadshotsMP[newSlot].hsdActive		= TRUE
	g_sGeneratedHeadshotsMP[newSlot].hsdPlayerID	= g_sHeadshotBeingGeneratedMP.hsdPlayerID
	g_sGeneratedHeadshotsMP[newSlot].hsdHeadshotID	= g_sHeadshotBeingGeneratedMP.hsdHeadshotID
	g_sGeneratedHeadshotsMP[newSlot].hsdKeepActive	= FALSE
	Store_Minimum_Generated_MP_Headshot_Timeout_For_Slot(newSlot)
	
	// Increment the number of Generated Headshots
	g_numGeneratedHeadshotsMP++
	
	// If this is the first generated headsot, restart the scheduled maintenance loop counter
	IF (g_numGeneratedHeadshotsMP = 1)
		g_nextHeadshotUpdateSlotMP = 0
	ENDIF
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== ADD GENERATED === Adding new Generated Headshot in slot: ", newSlot,
										" [", GET_PLAYER_NAME(g_sGeneratedHeadshotsMP[newSlot].hsdPlayerID), "]")
	#ENDIF
	
	// Stored ok
	RETURN TRUE

ENDFUNC




// -----------------------------------------------------------------------------------------------------------
//      Player Headshots Queued For Generation Routines
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if this player already has a headshot on the pending queue
//
// INPUT PARAMS:			paramPlayerID			The Player To Be Generated
// RETURN VALUE:			BOOL					TRUE if already on the pending queue, otherwise FALSE
FUNC BOOL Is_Player_On_Queue_Of_Headshots_To_Be_Generated(PLAYER_INDEX paramPlayerID)

	INT tempLoop = 0
	
	REPEAT g_sNumPendingHeadshotsMP tempLoop
		IF (g_sPendingHeadshotsMP[tempLoop] = paramPlayerID)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	If the player isn't already in the system in any capacity, add it to the 'to be generated' queue
//
// INPUT PARAMS:			paramPlayerID			The Player To Be Generated
//							paramOutputToLog		[DEFAULT = TRUE] TRUE if the details should be output to the console log, FALSE otherwise (to avoid potential spamming)
PROC Add_Player_To_Queue_Of_Headshots_To_Be_Generated(PLAYER_INDEX paramPlayerID, BOOL paramOutputToLog = TRUE)

	// Is the player ok?
	IF NOT (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
		EXIT
	ENDIF
	
	// Has the player already got a Valid and Ready headshot generated?
	IF (Get_Generated_Headshot_Slot_For_Player(paramPlayerID) != NO_HEADSHOT_SLOT)
		// ...yes, so don't add it to the pending queue
		EXIT
	ENDIF
	
	// Is the player having a headshot generated?
	IF (g_sHeadshotBeingGeneratedMP.hsdActive)
		IF (paramPlayerID = g_sHeadshotBeingGeneratedMP.hsdPlayerID)
			// ...yes, so don't add to the pending queue
			EXIT
		ENDIF
	ENDIF

	// Is the player already on the pending queue?
	IF (Is_Player_On_Queue_Of_Headshots_To_Be_Generated(paramPlayerID))
		EXIT
	ENDIF
	
	// Player isn't on the queue so should be added to the queue
	// Ensure there is room on the queue
	IF (g_sNumPendingHeadshotsMP >= MAX_MP_HEADSHOTS)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== ADD GENERATE QUEUE === Attempting to add new Headshot Request to Pending queue, but queue is full.")
			Debug_Output_MP_Headshots()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Add the request to the end of the pending queue
	g_sPendingHeadshotsMP[g_sNumPendingHeadshotsMP] = paramPlayerID
	g_sNumPendingHeadshotsMP++
	
	// Output to log?
	IF (paramOutputToLog)
		#IF IS_DEBUG_BUILD
			IF NOT bSlamSpam
				//spam slam here
				CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== ADD GENERATE QUEUE === Player Headshot Request added to Pending Queue: ", GET_PLAYER_NAME(paramPlayerID),
												"  [", GET_THIS_SCRIPT_NAME(), "]")
				Debug_Output_MP_Headshots()
				DEBUG_PRINTCALLSTACK()
			ENDIF
		#ENDIF
	ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Headshots to be generate queue
PROC Clear_Headshots_To_Be_Generated_Queue()

	INT tempLoop = 0
	
	REPEAT MAX_MP_HEADSHOTS tempLoop
		g_sPendingHeadshotsMP[tempLoop] = INVALID_PLAYER_INDEX()
	ENDREPEAT
	
	g_sNumPendingHeadshotsMP = 0
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_HEADSHOTS, "=== CLEAR GENERATE QUEUE === Cleared Headshots To Be Generated Pending Queue")
	#ENDIF

ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Generate Transparent Headshot Routines
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	If the transparent headshot is valid then unregister it.
// NOTES:	Used to clean up once finished with transparent headshot
PROC Unregister_MP_Transparent_Headshot()

	IF NOT (g_sTransparentHeadshotMP.hsdActive)
		EXIT
	ENDIF
	
	IF NOT (IS_PEDHEADSHOT_VALID(g_sTransparentHeadshotMP.hsdHeadshotID))
		EXIT
	ENDIF
	
	// NOTE: In console logs (code+script), search for PedHeadshotManager to get a refcount of registered/unregistered headshots when bug hunting
	// (requires -pedheadshotdebug in command line)
	UNREGISTER_PEDHEADSHOT(g_sTransparentHeadshotMP.hsdHeadshotID)
	
	#IF IS_DEBUG_BUILD
		IF (NETWORK_IS_GAME_IN_PROGRESS())
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== UNREGISTER TRANSPARENT === Unregister Transparent Headshot: ", 
											GET_PLAYER_NAME(g_sTransparentHeadshotMP.hsdPlayerID))
		ELSE
			CPRINTLN(DEBUG_NET_HEADSHOTS, "=== UNREGISTER TRANSPARENT === Unregister Unknown Transparent Headshot, game is no longer a network game")
		ENDIF
	#ENDIF

ENDPROC

// PURPOSE:	Clear the Transparent Headshot data
PROC Clear_MP_Transparent_Headshot_Data()
		
	Clear_One_Set_Of_MP_Headshot_Data(g_sTransparentHeadshotMP)
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_HEADSHOTS, "=== CLEAR TRANSPARENT DATA === Clearing Transparent Headshot Variables (this does not unregister the headshot)")
	#ENDIF
	
ENDPROC

// PURPOSE:	Cancel the Transparent Headshot and Clear the data
// NOTES:	Use this once finished with the transparent headshot
PROC Cancel_And_Clear_MP_Transparent_Headshot()

	// Is there an active request?
	IF NOT (g_sTransparentHeadshotMP.hsdActive)
		EXIT
	ENDIF

	// Unregister the Headshot Being Generated
	Unregister_MP_Transparent_Headshot()

	// Clear the Transparent Headshot variables
	Clear_MP_Transparent_Headshot_Data()

ENDPROC

// PURPOSE:	Stores the Transpanret Headshot variables
//
// INPUT PARAMS:		paramPlayerID			The Player Index that needs a headshot generated
//						paramHeadshotID			The Player Headshot ID to store
PROC Store_MP_Transparent_Headshot_Data(PLAYER_INDEX paramPlayerID, PEDHEADSHOT_ID paramHeadshotID)

	g_sTransparentHeadshotMP.hsdActive		= TRUE
	g_sTransparentHeadshotMP.hsdPlayerID	= paramPlayerID
	g_sTransparentHeadshotMP.hsdHeadshotID	= paramHeadshotID
	g_sTransparentHeadshotMP.hsdKeepActive	= FALSE
	
	// NOTE: Setting up a Headshot Being Generated timeout for debug purposes only - in practice it should always eventually succeed
	g_sTransparentHeadshotMP.hsdTimeout		= GET_TIME_OFFSET(GET_NETWORK_TIME(), MP_HEADSHOT_GENERATION_TIMEOUT_msec)
	
	// Reset the Code State of the headshot being generated - this will cut down onteh spam by only outputting a change of state
	#IF IS_DEBUG_BUILD
		g_debugTransparentHeadshotState = PHS_INVALID
	#ENDIF

ENDPROC

// PURPOSE:	Flags if Transparent Headshot is Currently Being Generated
FUNC BOOL Is_MP_Transparent_Headshot_Being_Generated()
	
	IF (g_sTransparentHeadshotMP.hsdActive)											//If active
		IF (IS_PEDHEADSHOT_VALID(g_sTransparentHeadshotMP.hsdHeadshotID))			//and valid
			IF (NOT IS_PEDHEADSHOT_READY(g_sTransparentHeadshotMP.hsdHeadshotID))	//but not ready
				RETURN TRUE															//then being generated
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC



// -----------------------------------------------------------------------------------------------------------
//      Player Headshots Being Generated Routines
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	If the headshot being registered is valid then unregister it.
// NOTES:	This will only occur in specific circumstances
PROC Unregister_MP_Headshot_Being_Generated()

	IF NOT (g_sHeadshotBeingGeneratedMP.hsdActive)
		EXIT
	ENDIF
	
	IF NOT (IS_PEDHEADSHOT_VALID(g_sHeadshotBeingGeneratedMP.hsdHeadshotID))
		EXIT
	ENDIF
	
	// NOTE: In console logs (code+script), search for PedHeadshotManager to get a refcount of registered/unregistered headshots when bug hunting
	// (requires -pedheadshotdebug in command line)
	UNREGISTER_PEDHEADSHOT(g_sHeadshotBeingGeneratedMP.hsdHeadshotID)
	
	#IF IS_DEBUG_BUILD
		IF NOT bSlamSpam
			//spam slam here
			IF (NETWORK_IS_GAME_IN_PROGRESS())
				CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== UNREGISTER GENERATE === Unregister Headshot Being Generated for player: ", 
												GET_PLAYER_NAME(g_sHeadshotBeingGeneratedMP.hsdPlayerID))
			ELSE
				CPRINTLN(DEBUG_NET_HEADSHOTS, "=== UNREGISTER GENERATE === Unregister Unknown Generate Player Headshot, game is no longer a network game")
			ENDIF
		ENDIF
	#ENDIF
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Clear the Headshot Being Generated data
PROC Clear_MP_Headshot_Being_Generated_Data()
		
	Clear_One_Set_Of_MP_Headshot_Data(g_sHeadshotBeingGeneratedMP)
	
	#IF IS_DEBUG_BUILD
		IF NOT bSlamSpam
			//spam slam here
			CPRINTLN(DEBUG_NET_HEADSHOTS, "=== CLEAR GENERATE DATA === Clearing Player Headshot Generation Variables (this does not unregister the headshot)")
		ENDIF
	#ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Cancel any Headshot Being Generated and Clear the data
// NOTES:	This only occurs under specific unusual circumstances.
PROC Cancel_And_Clear_Any_MP_Headshot_Being_Generated()

	// Is there an active request?
	IF NOT (g_sHeadshotBeingGeneratedMP.hsdActive)
		EXIT
	ENDIF

	// Unregister the Headshot Being Generated
	Unregister_MP_Headshot_Being_Generated()

	// Clear the Headshot Being Generated variables
	Clear_MP_Headshot_Being_Generated_Data()

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Stores the Headshot Being Generated variables
//
// INPUT PARAMS:		paramPlayerID			The Player Index that needs a headshot generated
//						paramHeadshotID			The Player Headshot ID to store
PROC Store_MP_Headshot_Being_Generated_Data(PLAYER_INDEX paramPlayerID, PEDHEADSHOT_ID paramHeadshotID)

	g_sHeadshotBeingGeneratedMP.hsdActive		= TRUE
	g_sHeadshotBeingGeneratedMP.hsdPlayerID		= paramPlayerID
	g_sHeadshotBeingGeneratedMP.hsdHeadshotID	= paramHeadshotID
	g_sHeadshotBeingGeneratedMP.hsdKeepActive	= FALSE
	
	// NOTE: Setting up a Headshot Being Generated timeout for debug purposes only - in practice it should always eventually succeed
	g_sHeadshotBeingGeneratedMP.hsdTimeout		= GET_TIME_OFFSET(GET_NETWORK_TIME(), MP_HEADSHOT_GENERATION_TIMEOUT_msec)
	
	// Reset the Code State of the headshot being generated - this will cut down onteh spam by only outputting a change of state
	#IF IS_DEBUG_BUILD
		g_debugHeadshotBeingGeneratedState = PHS_INVALID
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Check if a headshot is being generated for the specified player
//
// INPUT PARAMS:		paramPlayerID			The Player Index that needs a headshot generated
// RETURN VALUE:		BOOL					TRUE if the headshot is being generated, FALSE if not
FUNC BOOL Is_MP_Headshot_Being_Generated_For_Player(PLAYER_INDEX paramPlayerIndex)
	
	// Is a headshot being generated?
	IF NOT (g_sHeadshotBeingGeneratedMP.hsdActive)
		RETURN FALSE
	ENDIF
	
	// A headshot is being generated, so check if it is for this player
	RETURN (g_sHeadshotBeingGeneratedMP.hsdPlayerID = paramPlayerIndex)
	
ENDFUNC

FUNC BOOL DOES_PLAYER_OWN_CLUBHOUSE_PEDHEAD(PLAYER_INDEX playerID)
	IF NATIVE_TO_INT(playerID) = -1
		RETURN FALSE
	ENDIF
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].propertyDetails.iBSTwo,PROPERTY_BROADCAST_BS2_OWNS_CLUBHOUSE)
ENDFUNC

FUNC BOOL SHOULD_RUN_HIGH_RES_HEADSHOT(PLAYER_INDEX thePlayer)
	IF (GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), thePlayer)
	AND (GB_GET_PLAYER_GANG_ROLE(thePlayer) != GR_NONE OR GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(thePlayer)))
	AND g_PedShotHighRes_NumberActive < 6
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC
PROC DEBUG_PRINT_SHOULD_RUN_HIGH_RES_HEADSHOT()
	CPRINTLN(DEBUG_NET_HEADSHOTS, "=== GENERATE NEXT === REGISTER_PEDHEADSHOT called ")
	//CPRINTLN(DEBUG_NET_HEADSHOTS, "=== GENERATE NEXT === GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), thePlayer) = ", GB_ARE_PLAYERS_MEMBERS_OF_SAME_GANG(PLAYER_ID(), thePlayer))
	//CPRINTLN(DEBUG_NET_HEADSHOTS, "=== GENERATE NEXT === GB_GET_PLAYER_GANG_ROLE(thePlayer) = ", GB_GET_PLAYER_GANG_ROLE(thePlayer))
	//CPRINTLN(DEBUG_NET_HEADSHOTS, "=== GENERATE NEXT === GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(thePlayer) = ", GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(thePlayer))

ENDPROC

// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Start Generating a new Headshot for the first player on the Headshots To Be Generated queue
PROC Generate_Headshot_For_Next_Player()

	// Are there any headshots to be generated?
	IF (g_sNumPendingHeadshotsMP = 0)
		EXIT
	ENDIF
	
	// There is a headshot to be generated, so retrieve the player from the array
	INT				queuePosition	= 0
	PLAYER_INDEX	thePlayer		= g_sPendingHeadshotsMP[queuePosition]

	// Shuffle the array down to cover the gap
	INT				toPos			= queuePosition
	INT				fromPos			= toPos + 1
	
	WHILE (fromPos < g_sNumPendingHeadshotsMP)
		g_sPendingHeadshotsMP[toPos] = g_sPendingHeadshotsMP[fromPos]
		toPos++
		fromPos++
	ENDWHILE
	
	// Clear the new final entry
	g_sPendingHeadshotsMP[toPos] = INVALID_PLAYER_INDEX()
	
	// Reduce the number of entries in the array
	g_sNumPendingHeadshotsMP--
	
	// Try to initiate the Headshot generation
	// NOTE: If anything fails here then it'll try next frame with the next headshot in the list
	
	// Only Generate a headshot if the headshot player is ok
	IF NOT (IS_NET_PLAYER_OK(thePlayer, FALSE))
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_HEADSHOTS, "=== GENERATE NEXT === Player is not ok. Removing from the Pending list.")
			Debug_Output_MP_Headshots()
		#ENDIF
		
		EXIT
	ENDIF
	
	// Only generate a headshot if the Generated Headshots array isn't full
	IF (g_numGeneratedHeadshotsMP >= MAX_MP_HEADSHOTS)
		// ...there would be no room to store this generated headshot, so try again later - not convinced this should occur, so asserting
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_HEADSHOTS, "=== GENERATE NEXT === ERROR: There would be no space on the Generated Headshots array to store this Headshot. Try again later.")
			Debug_Output_MP_Headshots()
			SCRIPT_ASSERT("Generate_Headshot_For_Next_Player(): ERROR: There would be no space to store this headshot after generation. See Console Log. Tell Keith.")
		#ENDIF
	
		// Don't output the details to the console log to avoid potential spamming
		BOOL outputDetailsToConsoleLog = FALSE
		Add_Player_To_Queue_Of_Headshots_To_Be_Generated(thePlayer, outputDetailsToConsoleLog)
		
		EXIT
	ENDIF
	
	// I can't register the headshot without checking if the headshot player is dead (code asserts - ped dead check) - for now I'll check for dead and delay generation if dead (BUG: 936304)
	// This may mean headshots aren't generated under some circumstances - possible solutions: code may need to bypass the assert, or we could pre-generate headshots for activities.
	IF (IS_ENTITY_DEAD(GET_PLAYER_PED(thePlayer)))
		// ...the player to generate is currently dead, so try again later - we may need a better solution
		#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 150 = 0
				CPRINTLN(DEBUG_NET_HEADSHOTS, "=== GENERATE NEXT === ERROR: The headshot is requested for a dead player. This causes a code assert. Try again later.")
			ENDIF
		#ENDIF
	
		// Don't output the details to the console log to avoid potential spamming
		BOOL outputDetailsToConsoleLog = FALSE
		Add_Player_To_Queue_Of_Headshots_To_Be_Generated(thePlayer, outputDetailsToConsoleLog)
		
		EXIT
	ENDIF
	
	// Register the headshot and setup the generation data
	// NOTE: In console logs (code+script), search for PedHeadshotManager to get a refcount of registered/unregistered headshots when bug hunting
	// 		(requires -pedheadshotdebug in command line)
	PEDHEADSHOT_ID theHeadshotID


	IF SHOULD_RUN_HIGH_RES_HEADSHOT(thePlayer)
		theHeadshotID = REGISTER_PEDHEADSHOT_HIRES(GET_PLAYER_PED(thePlayer))
		CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== GENERATE NEXT === HiRes Headshot ID = ", NATIVE_TO_INT(theHeadshotID))
		
		IF NATIVE_TO_INT(thePlayer) > -1
			IF NOT IS_BIT_SET(g_PedShotHighRes_Bitset, NATIVE_TO_INT(thePlayer))
				SET_BIT(g_PedShotHighRes_Bitset, NATIVE_TO_INT(thePlayer))
			ENDIF
		ENDIF
		
		g_PedShotHighRes_NumberActive = SUM_BITS(g_PedShotHighRes_Bitset)
		CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== HIGH RES === g_PedShotHighRes_NumberActive high res updated to ", g_PedShotHighRes_NumberActive)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_HEADSHOTS, "=== GENERATE NEXT === REGISTER_PEDHEADSHOT_HIRES called g_PedShotHighRes_NumberActive = ",g_PedShotHighRes_NumberActive )
		#ENDIF
		
	ELSE
		theHeadshotID = REGISTER_PEDHEADSHOT(GET_PLAYER_PED(thePlayer))
		CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== GENERATE NEXT === Headshot ID = ", NATIVE_TO_INT(theHeadshotID))
		
		IF NATIVE_TO_INT(thePlayer) > -1
			IF IS_BIT_SET(g_PedShotHighRes_Bitset, NATIVE_TO_INT(thePlayer))
				CLEAR_BIT(g_PedShotHighRes_Bitset, NATIVE_TO_INT(thePlayer))
				CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== HIGH RES === Low Res, skip adding to count ")
			ENDIF
		ENDIF
		
		g_PedShotHighRes_NumberActive = SUM_BITS(g_PedShotHighRes_Bitset)
		CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== HIGH RES === g_PedShotHighRes_NumberActive low res updated to ", g_PedShotHighRes_NumberActive)
		
		#IF IS_DEBUG_BUILD
			DEBUG_PRINT_SHOULD_RUN_HIGH_RES_HEADSHOT()
		#ENDIF
	ENDIF
	g_b_RefreshBikerWall = TRUE
	
	
	
	
	// Store the details (if the headshot ID is not valid, this will get caught in the next frame)
	Store_MP_Headshot_Being_Generated_Data(thePlayer, theHeadshotID)
	
	
	#IF IS_DEBUG_BUILD
		IF NOT bSlamSpam
			//spam slam here
			CPRINTLN(DEBUG_NET_HEADSHOTS, "=== GENERATE NEXT === Headshot Registered with code and now Being Generated: ", GET_PLAYER_NAME(thePlayer))
			Debug_Output_MP_Headshots()
		ENDIF
	#ENDIF

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain a Headshot Being Generated
//
// RETURN VALUE:			BOOL		TRUE if a headshot is being generated, otherwise FALSE
FUNC BOOL Maintain_MP_Headshot_Being_Generated()

	// Is there a headshot being generated?
	IF NOT (g_sHeadshotBeingGeneratedMP.hsdActive)
		// ...no
		RETURN FALSE
	ENDIF
	
	// A Headshot is Being Generated
	PLAYER_INDEX thisPlayer = g_sHeadshotBeingGeneratedMP.hsdPlayerID
	
	// Is the player still ok?
	IF NOT (IS_NET_PLAYER_OK(thisPlayer, FALSE))
		// ...player is no longer ok, so cleanup		
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_NET_HEADSHOTS, "=== MAINTAIN GENERATION === ERROR: Player is no longer OK. Cancelling Headshot Being Generated.")
		#ENDIF
	
		Cancel_And_Clear_Any_MP_Headshot_Being_Generated()
		
		// Return TRUE so that another headshot generation request doesn't start till next frame (probably unnecessary, but it can't hurt)
		RETURN TRUE
	ENDIF
	
	
	// Is the Headshot ID still valid?
	IF NOT (IS_PEDHEADSHOT_VALID(g_sHeadshotBeingGeneratedMP.hsdHeadshotID))
		// ...no longer valid, so cleanup, but add this player back onto the generation list to try again later
		#IF IS_DEBUG_BUILD
			IF NOT bSlamSpam
				//spam slam here
				CPRINTLN(DEBUG_NET_HEADSHOTS, "=== MAINTAIN GENERATION === ERROR: Headshot is no longer valid. Cancelling Headshot Being Generated, but try again later.")
			
				iSpamSlams++
				
				IF iSpamSlams > 20
					CPRINTLN(DEBUG_NET_HEADSHOTS, "=== SPAM SLAM === SPAM SLAMMED")
					bSlamSpam = TRUE
				ENDIF
			
			ENDIF
		#ENDIF
	
		Cancel_And_Clear_Any_MP_Headshot_Being_Generated()
		Add_Player_To_Queue_Of_Headshots_To_Be_Generated(thisPlayer)
		
		// Return TRUE so that another headshot generation request doesn't start till next frame (probably unnecessary, but it can't hurt)
		RETURN TRUE
	ENDIF
	
	// Is the Headshot now ready?
	IF NOT (IS_PEDHEADSHOT_READY(g_sHeadshotBeingGeneratedMP.hsdHeadshotID))
		// ...not yet ready, output some console log details if the headshot state has changed
		#IF IS_DEBUG_BUILD
			IF (GET_PEDHEADSHOT_STATE(g_sHeadshotBeingGeneratedMP.hsdHeadshotID) != g_debugHeadshotBeingGeneratedState)
				// ...headshot state has changed, so output the state
				CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== MAINTAIN GENERATION === Headshot Being Generated is Valid but NOT READY. Current Code Headshot State: ")
				Debug_Output_Append_Player_Headshot_State(g_sHeadshotBeingGeneratedMP.hsdHeadshotID, g_sHeadshotBeingGeneratedMP.hsdPlayerID)
				
				g_debugHeadshotBeingGeneratedState = GET_PEDHEADSHOT_STATE(g_sHeadshotBeingGeneratedMP.hsdHeadshotID)
			ENDIF
		#ENDIF
		
		// Has the attempt being going on for a long time?
		IF (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sHeadshotBeingGeneratedMP.hsdTimeout))
			// ...yes, so cancel the attempt and try again later
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== MAINTAIN GENERATION === ERROR: Headshot Generation has been ongoing for ", 
												MP_HEADSHOT_GENERATION_TIMEOUT_msec, " msec.")
				CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== MAINTAIN GENERATION === Cancelling Headshot Being Generated, but try again later.")
				CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== MAINTAIN GENERATION === Ped Headshot State")
				Debug_Output_Append_Player_Headshot_State(g_sHeadshotBeingGeneratedMP.hsdHeadshotID, g_sHeadshotBeingGeneratedMP.hsdPlayerID)
			#ENDIF
		
			Cancel_And_Clear_Any_MP_Headshot_Being_Generated()
			Add_Player_To_Queue_Of_Headshots_To_Be_Generated(thisPlayer)
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	// Headshot has been generated so store it on the Generated Headshots list and clear the Headshots Being Generated data
	IF NOT (Add_Headshot_Being_Generated_To_Generated_MP_Headshots())
		// ...there was a problem adding the headshot to the Generated Headshots array - this should be impossible so an assert will already have been issued.
		// Best thing to do here is clear the generated headshot and try again later
		Cancel_And_Clear_Any_MP_Headshot_Being_Generated()
		
		// Don't output the details to the console log to avoid potential spamming
		BOOL outputDetailsToConsoleLog = FALSE
		Add_Player_To_Queue_Of_Headshots_To_Be_Generated(thisPlayer, outputDetailsToConsoleLog)
		
		// Return TRUE so that another headshot generation request doesn't start till next frame (probably unnecessary, but it can't hurt)
		RETURN TRUE
	ENDIF
	
	// Headshot is now stored on the Generated Headshots array, so clear the Headshot Being Generated data
	Clear_MP_Headshot_Being_Generated_Data()
	
	#IF IS_DEBUG_BUILD
		Debug_Output_MP_Headshots()
	#ENDIF
	
	// Return TRUE so that another headshot generation request doesn't start till next frame (probably unnecessary, but it can't hurt)
	RETURN TRUE
			
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the current headshot being generated or request the next one to start being generated
PROC Maintain_MP_Headshot_Requests()

	//If a transparent headshot is being generated, cancel out of current generation and do nothing until it is generated
	IF (Is_MP_Transparent_Headshot_Being_Generated())
		Cancel_And_Clear_Any_MP_Headshot_Being_Generated()
		EXIT
	ENDIF
	
	// If a headshot is being uploaded, cancel out of the current generation and do nothing until it is complete
	IF g_sExportPlayerHeadshotStage > 0
		CPRINTLN(DEBUG_NET_HEADSHOTS, "=== MAINTAIN GENERATION === BLOCKED: Export in progress.")
		Cancel_And_Clear_Any_MP_Headshot_Being_Generated()
		EXIT
	ENDIF

	// Maintain an MP Headshot being generated
	IF (Maintain_MP_Headshot_Being_Generated())
		EXIT
	ENDIF

	// There isn't a headshot being generated, so start a new generation request if there are any outstanding
	Generate_Headshot_For_Next_Player()
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//      Pre-Generate Headshot Routines
//		(All player headshots should get generated as they join the game and then remain active throughout)
// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Keeps a Headshot Active if a system still requires it even if it isn't currently on display
//
// INPUT PARAMS:		paramPlayerID				PlayerID of the headshot to be kept active
//						paramInternalRequest		[DEFAULT = FALSE] TRUE if the request came from the headshot system, othrewise FALSE
//
// NOTES:	If the headshot hasn't already been generated then request it
PROC Keep_Player_Headshot_Active(PLAYER_INDEX paramPlayerID, BOOL paramInternalRequest = FALSE)

	// Has the headshot already been generated?
	INT theSlot = Get_Generated_Headshot_Slot_For_Player(paramPlayerID)
	
	// Does the headshot exist?
	IF (theSlot != NO_HEADSHOT_SLOT)
		// ...the headshot exists, so keep it active (this will update the timer during the next maintenance check)
		IF NOT (paramInternalRequest)
			// This is not an internal request, so output some console log details
			#IF IS_DEBUG_BUILD
				IF NOT (g_sGeneratedHeadshotsMP[theSlot].hsdKeepActive)
					CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== KEEP ACTIVE === First Request for a Player Headshot to be kept active for Player: ",
													GET_PLAYER_NAME(paramPlayerID), "  [Requested by: ", GET_THIS_SCRIPT_NAME(), "]")
					Debug_Output_MP_Headshots()
					DEBUG_PRINTCALLSTACK()
				ENDIF
			#ENDIF
		ENDIF
		
		g_sGeneratedHeadshotsMP[theSlot].hsdKeepActive = TRUE
		
		EXIT
	ENDIF
	
	IF (Is_Player_On_Queue_Of_Headshots_To_Be_Generated(paramPlayerID))
		EXIT
	ENDIF
	
	// The headshot hasn't been generated and isn't on the queue to be generated, so need to make a fresh request
	#IF IS_DEBUG_BUILD
		IF GET_FRAME_COUNT() % 150 = 0
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== KEEP ACTIVE === Player Headshot to be kept active but isn't yet generated. Requesting new headshot (ignored if already being generated): ",
											GET_PLAYER_NAME(paramPlayerID))
		ENDIF
	#ENDIF
	
	Add_Player_To_Queue_Of_Headshots_To_Be_Generated(paramPlayerID)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Ensures there is an active headshot generated for all players inthe game and that this headshot remains throughout.
// NOTES:	I can just call the 'keep active' function because that requests new headshots as needed
PROC Maintain_An_Active_Headshot_For_All_Players_In_Game()

	g_scheduledPlayerHeadshotMaintenance++
	IF (g_scheduledPlayerHeadshotMaintenance >= NUM_NETWORK_PLAYERS)
		g_scheduledPlayerHeadshotMaintenance = 0
	ENDIF
	
	PLAYER_INDEX maintainThisPlayer = INT_TO_PLAYERINDEX(g_scheduledPlayerHeadshotMaintenance)
	
	IF (IS_PLAYER_SCTV(maintainThisPlayer))
		// ...don't need headshots for SCTV
		EXIT
	ENDIF
		
	IF NOT (IS_NET_PLAYER_OK(maintainThisPlayer, FALSE))
		EXIT
	ENDIF
	
	// Keep this headshot active (which will request it if not already requested)
	BOOL internalRequest = TRUE
	Keep_Player_Headshot_Active(maintainThisPlayer, internalRequest)
	
ENDPROC


// ===========================================================================================================
//      Export Player Headshot to Social Club
// ===========================================================================================================

FUNC BOOL Reduce_Headshot_Upload_Frequency()
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ReduceHeadshotUploadFrequency")
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN g_bReduceHeadshotUploadFrequency
ENDFUNC

PROC Set_Player_Headshot_Needs_Updating(BOOL bState)
	g_bPlayerHeadshotNeedsUpdating = bState
	SET_MP_BOOL_CHARACTER_STAT(MP_STAT_NEED_TO_UPDATE_HEADSHOT, bState)
ENDPROC

FUNC BOOL Does_Player_Headshot_Needs_Updating()
	IF Reduce_Headshot_Upload_Frequency()
		RETURN g_bPlayerHeadshotNeedsUpdating
	ELSE
		RETURN GET_MP_BOOL_CHARACTER_STAT(MP_STAT_NEED_TO_UPDATE_HEADSHOT)
	ENDIF
ENDFUNC

// PURPOSE:	Clears out the export player headshot variables
PROC Clear_Export_Player_Headshot()
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_HEADSHOTS, "=== CLEAR EXPORT === Clear_Export_Player_Headshot() called!")
	#ENDIF
	//Reset export flag, so will attempt to update
	IF g_sExportPlayerHeadshotStage > 0
		
	ENDIF
	g_sExportPlayerHeadshotRequested = FALSE
	g_sExportPlayerHeadshotStage = -1
ENDPROC

//PURPOSE: Call to start the export now
PROC Setup_Export_Player_Headshot()
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_HEADSHOTS, "=== SETUP EXPORT === Setup_Export_Player_Headshot() called!")
	#ENDIF
	IF NOT g_sExportPlayerHeadshotRequested
		g_sExportPlayerHeadshotRequested = TRUE
		g_sExportPlayerHeadshotStage = 0
		g_sExportPlayerHeadshotTimeout = GET_TIME_OFFSET(GET_NETWORK_TIME(), MP_EXPORT_PLAYER_HEADSHOT_TIMEOUT_msec)
	ENDIF
ENDPROC

//PURPOSE: Starts the countdown to player headshot getting exported.
PROC Start_Export_Player_Headshot_Timer()
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_HEADSHOTS, "=== START EXPORT TIMER === Start_Export_Player_Headshot_Timer() called!")
	#ENDIF
	IF NOT g_sExportPlayerHeadshotRequested		//unless we're already exporting
		Set_Player_Headshot_Needs_Updating(TRUE)	//set stat in case something goes wrong
		g_sExportPlayerHeadshotCountdown = GET_TIME_OFFSET(GET_NETWORK_TIME(), g_sMPTunables.iplayerheadshotuploadtunable)

	#IF IS_DEBUG_BUILD
	ELSE
		CPRINTLN(DEBUG_NET_HEADSHOTS, "=== START EXPORT TIMER === A timer was already running. Didn't set a new export timer.")
	#ENDIF
	ENDIF
ENDPROC

PROC Immediately_Export_Player_Headshot()
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_HEADSHOTS, "=== IMMEDIATELY EXPORT === Immediately_Export_Player_Headshot() called!")
	#ENDIF
	IF NOT g_sExportPlayerHeadshotRequested		//unless we're already exporting
		Set_Player_Headshot_Needs_Updating(TRUE)	//set stat in case something goes wrong
		g_sExportPlayerHeadshotCountdown = GET_NETWORK_TIME()
	ENDIF
ENDPROC

// PURPOSE:	Clears out the export player headshot variables and sets time to export if stat is set
PROC Initialise_Export_Player_Headshot()
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_HEADSHOTS, "=== INITIALISE EXPORT === Initialise_Export_Player_Headshot()")
	#ENDIF
	Clear_Export_Player_Headshot()
	IF g_bNewPlayerHeadshotNeedsCreated
		Immediately_Export_Player_Headshot()
		g_bNewPlayerHeadshotNeedsCreated = FALSE
	ELIF Does_Player_Headshot_Needs_Updating()
		IF Reduce_Headshot_Upload_Frequency()
			Start_Export_Player_Headshot_Timer()
		ELSE
			Immediately_Export_Player_Headshot()
		ENDIF
	ENDIF
ENDPROC


// PURPOSE:	Detects requests from the selector to generate a transparent headshot if the
//			selector failed to download a texture from the cloud.
PROC Maintain_Generate_Initial_Transparent_Headshot()

	IF g_bGenerateInitialTransparentHeadshot
		CPRINTLN(DEBUG_NET_HEADSHOTS, "=== MAINTAIN FRESH TRANSPARENT === Detected a request for a fresh transparent headshot to generate.")
		
		IF IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sExportPlayerHeadshotCountdown)
		AND NOT g_sExportPlayerHeadshotRequested
			Start_Export_Player_Headshot_Timer() 
#IF IS_DEBUG_BUILD
		ELSE
			CPRINTLN(DEBUG_NET_HEADSHOTS, "=== MAINTAIN FRESH TRANSPARENT === A request was already in progress. Skipping starting new request.")
#ENDIF
		ENDIF
		
		// KGM 24/3/15 [2229163]: Always clear the control flag to prevent this spamming continuously if trh transparetn headshot timer is already active
		g_bGenerateInitialTransparentHeadshot = FALSE
	ENDIF
	
ENDPROC


// ===========================================================================================================
//      The Player Headshot Public Functions
// ===========================================================================================================

// PURPOSE:	Get an existing HeadshotID for a player
//
// INPUT PARAMS:		paramPlayerID				PlayerID of the player whose headshot is being generated
// RETURN VALUE:		PEDHEADSHOT_ID				The HeadshotID if it exists, otherwise NULL
FUNC PEDHEADSHOT_ID Get_HeadshotID_For_Player(PLAYER_INDEX paramPlayerID)

	INT theSlot = Get_Generated_Headshot_Slot_For_Player(paramPlayerID)
	
	// Is there stored data?
	IF (theSlot = NO_HEADSHOT_SLOT)
		// ...no, so set up a headshot request
		Add_Player_To_Queue_Of_Headshots_To_Be_Generated(paramPlayerID)
		
		RETURN NULL
	ENDIF

	// Keep the headshot active
	#IF IS_DEBUG_BUILD
		IF NOT (g_sGeneratedHeadshotsMP[theSlot].hsdKeepActive)
		
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== GET HEADSHOT ID === Headshot now set to be Kept Active for player: ",
											GET_PLAYER_NAME(paramPlayerID), "  [Requested by: ", GET_THIS_SCRIPT_NAME(), "]")
			Debug_Output_MP_Headshots()
			DEBUG_PRINTCALLSTACK()
		ENDIF
	#ENDIF
	
	g_sGeneratedHeadshotsMP[theSlot].hsdKeepActive = TRUE
	
	// Return the HeadshotID
	RETURN (g_sGeneratedHeadshotsMP[theSlot].hsdHeadshotID)

ENDFUNC


// -----------------------------------------------------------------------------------------------------------


// PURPOSE:	Generate a HeadshotID with a transparent background for a player
//
// INPUT PARAMS:		paramPlayerID				PlayerID of the player whose headshot is being generated
// RETURN VALUE:		PEDHEADSHOT_ID				The HeadshotID if it exists, otherwise NULL
FUNC PEDHEADSHOT_ID Get_Transparent_HeadshotID_For_Player(PLAYER_INDEX paramPlayerID, BOOL paramForExport = FALSE)
	
	// Is requested player OK?
	IF NOT (IS_NET_PLAYER_OK(paramPlayerID, FALSE))
		IF (g_sTransparentHeadshotMP.hsdPlayerID <> paramPlayerID)
			Cancel_And_Clear_MP_Transparent_Headshot()
		ENDIF
		RETURN NULL
	ENDIF
	
	IF (paramForExport)														//IF export
		IF (g_sTransparentHeadshotMP.hsdPlayerID <> paramPlayerID)			//but something else is using the transparent
		AND (IS_PEDHEADSHOT_VALID(g_sTransparentHeadshotMP.hsdHeadshotID))	
			RETURN NULL														//Bail out!
		ENDIF
	ENDIF
	
	IF IS_NET_PLAYER_OK(paramPlayerID, FALSE)
		IF IS_PLAYER_AN_ANIMAL(paramPlayerID)
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== Get_Transparent_HeadshotID_For_Player === bypassing regeneration player is IS_PLAYER_AN_ANIMAL")
			Cancel_And_Clear_MP_Transparent_Headshot()
			RETURN NULL		
		ENDIF
	ENDIF
	
	PED_INDEX ped = GET_PLAYER_PED(paramPlayerID)
	IF DOES_ENTITY_EXIST(ped)
	AND NOT IS_PED_INJURED(ped)
		MODEL_NAMES eModel = GET_ENTITY_MODEL(ped)
		IF eModel != MP_M_FREEMODE_01
		AND eModel != MP_F_FREEMODE_01
			// Bail out but do not clear, we will wait for the model to change.
			RETURN NULL
		ENDIF
	ENDIF
		
	//Transparent set up with wrong player?
	//or not valid?
	IF (g_sTransparentHeadshotMP.hsdPlayerID <> paramPlayerID)
	OR (NOT IS_PEDHEADSHOT_VALID(g_sTransparentHeadshotMP.hsdHeadshotID))
	
		IF (g_sExportPlayerHeadshotRequested)
			IF g_sExportPlayerHeadshotStage > 0
				IF IS_PEDHEADSHOT_VALID(g_sTransparentHeadshotMP.hsdHeadshotID)
					RELEASE_PEDHEADSHOT_IMG_UPLOAD(g_sTransparentHeadshotMP.hsdHeadshotID)
				ENDIF
				Clear_Export_Player_Headshot()
			ENDIF
		ENDIF
	
		//Clear old transparent
		Cancel_And_Clear_MP_Transparent_Headshot()
		
		// KGM 24/3/15 [BUG 2229163]: The killstrip was the only place that tried to generate a transparent headshot for another player - it doesn't now so nowhere else
		//	is an immediate transparent headshot required to be generated, so it doesn't need to interrupt normal generation, but don't start a new transparent generation
//		//Clear any background generation that might be going on
//		Cancel_And_Clear_Any_MP_Headshot_Being_Generated()

		// KGM 24/3/15 [BUG 2229163]: If a normal headshot generation is in progress then don't start the transparent headshot generation
		IF (g_sHeadshotBeingGeneratedMP.hsdActive)
			RETURN NULL
		ENDIF
		
		//Setup as new transparent
		// Register the headshot and setup the generation data
		// NOTE: In console logs (code+script), search for PedHeadshotManager to get a refcount of registered/unregistered headshots when bug hunting
		// 		(requires -pedheadshotdebug in command line)
		PEDHEADSHOT_ID theHeadshotID = REGISTER_PEDHEADSHOT_TRANSPARENT(GET_PLAYER_PED(paramPlayerID))
		CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== MAINTAIN TRANSPARENT GENERATION === Transparent Headshot ID = ", NATIVE_TO_INT(theHeadshotID))
		
		// Store the details (if the headshot ID is not valid, this will get caught in the next frame)
		Store_MP_Transparent_Headshot_Data(paramPlayerID, theHeadshotID)
		
		RETURN NULL
	ENDIF
	
	IF (NOT IS_PEDHEADSHOT_READY(g_sTransparentHeadshotMP.hsdHeadshotID))
		// ...not ready yet
		// ...not yet ready, output some console log details if the headshot state has changed
		#IF IS_DEBUG_BUILD
			IF (GET_PEDHEADSHOT_STATE(g_sTransparentHeadshotMP.hsdHeadshotID) != g_debugTransparentHeadshotState)
				// ...headshot state has changed, so output the state
				CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== MAINTAIN TRANSPARENT GENERATION === Transparent Headshot Being Generated is Valid but NOT READY. Current Code Headshot State: ")
				Debug_Output_Append_Player_Headshot_State(g_sTransparentHeadshotMP.hsdHeadshotID, g_sTransparentHeadshotMP.hsdPlayerID)
				
				g_debugTransparentHeadshotState = GET_PEDHEADSHOT_STATE(g_sTransparentHeadshotMP.hsdHeadshotID)
			ENDIF
		#ENDIF
		
		// Has the attempt being going on for a long time?
		IF (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sTransparentHeadshotMP.hsdTimeout))
			// ...yes, so cancel the attempt and try again later
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== MAINTAIN TRANSPARENT GENERATION === ERROR: Headshot Generation has been ongoing for ", 
												MP_HEADSHOT_GENERATION_TIMEOUT_msec, " msec.")
				CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== MAINTAIN TRANSPARENT GENERATION ===                        Cancelling Headshot Being Generated, but try again later.")
			#ENDIF
		
			Cancel_And_Clear_MP_Transparent_Headshot()
			IF (g_sExportPlayerHeadshotRequested)
				Clear_Export_Player_Headshot()
			ENDIF
		ENDIF
				
		RETURN NULL
	ENDIF
	
	//Not failed, must be ok!
	RETURN (g_sTransparentHeadshotMP.hsdHeadshotID)
ENDFUNC


// -----------------------------------------------------------------------------------------------------------


// PURPOSE:	Regenerate a headshot ID for the player - intended for after the player has swapped clothes.
//
// INPUT PARAMS:		paramPlayerID				PlayerID of the player whose headshot is being re-generated
//
// NOTES:	Use BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS()) to regenerate the headshot on all machines
PROC Regenerate_Player_Headshot(PLAYER_INDEX paramPlayerID)
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== REGENERATE HEADSHOT === Headshot being re-generated for player: ",
										GET_PLAYER_NAME(paramPlayerID), "  [Requested by: ", GET_THIS_SCRIPT_NAME(), "]")
		Debug_Output_MP_Headshots()
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	IF g_b_OnLeaderboard
	AND IS_PLAYER_ON_A_PLAYLIST(PLAYER_ID())
		CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== REGENERATE HEADSHOT === bypassing regeneration player is on a playlist leaderboard")
		EXIT
	ENDIF
	
	IF IS_NET_PLAYER_OK(paramPlayerID, FALSE)
		IF IS_PLAYER_AN_ANIMAL(paramPlayerID)
			CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== REGENERATE HEADSHOT === bypassing regeneration player is IS_PLAYER_AN_ANIMAL")
			EXIT
		ENDIF
	ENDIF

	// Clear leaderboard headshots so they are regenerated with the new headshot.
	INT iHeadshot
	FOR iHeadshot = 0 TO NUM_NETWORK_PLAYERS - 1
		g_s_HeadshotLbd[iHeadshot] = ""
	ENDFOR
	
	// Remove existing headshot
	INT theSlot = Get_Generated_Headshot_Slot_For_Player(paramPlayerID)
	
	// Is there an existing headshot for this player?
	IF (theSlot != NO_HEADSHOT_SLOT)
		// ...yes, so delete the existing headshot
		Delete_Generated_MP_Headshot(theSlot)
	ENDIF
	
	// Cancel headshot being generated if it is for this player
	IF (Is_MP_Headshot_Being_Generated_For_Player(paramPlayerID))
		Cancel_And_Clear_Any_MP_Headshot_Being_Generated()
	ENDIF
	
	// Keep this headshot active (which will request a new headshot since the previous one no longer exists)
	BOOL internalRequest = TRUE
	Keep_Player_Headshot_Active(paramPlayerID, internalRequest)
	

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Allow a delayed headshot regeneration request broadcast
// NOTES:	KGM 30/7/15 [BUG 2450138]: The request to re-generate the beasts headshot after returning to human can be received by the remote player BEFORE code
//				has updated the model back to human, so the headshot gets re-generated again as the beast. This function introduces a delay.
PROC Add_Delayed_Headshot_Regeneration_Request(INT paramDelay = HEADSHOT_REGENERATION_REQUEST_DEFAULT_DELAY_msec)

	g_delayedHeadshotRegenerationTimeout = GET_GAME_TIMER() + paramDelay
	
	CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== REGENERATE HEADSHOT === Add_Delayed_Headshot_Regeneration_Request() - Now: ", GET_GAME_TIMER(), ".  Timeout: ", g_DelayedHeadshotRegenerationTimeout)
	DEBUG_PRINTCALLSTACK()
		
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: Checks if a delayed headshot generation request is active. If so, broadcasts the request on timeout.
PROC Maintain_Delayed_Headshot_Regeneration()

	IF (g_delayedHeadshotRegenerationTimeout = 0)
		EXIT
	ENDIF
	
	IF (g_delayedHeadshotRegenerationTimeout > GET_GAME_TIMER())
		EXIT
	ENDIF
	
	// Timeout, so broadcast the request to all players to regenerate this player's headshot
	g_delayedHeadshotRegenerationTimeout = 0
	
	CPRINTLN(DEBUG_NET_HEADSHOTS, 	"=== REGENERATE HEADSHOT === Maintain_Delayed_Headshot_Regeneration() - Timeout has expired. Broadcasting request.")

	BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())

ENDPROC



//PURPOSE: Resets the globals required for tracking the delete character headshot request
PROC Reset_Delete_Character_Headshot()

	DEBUG_PRINTCALLSTACK()
	
	g_iDeleteCharacterHeadshotStage = 0
	g_iDeleteCharacterHeadshotID = -1
	g_iDeleteCharacterHeadshotTimer = -1
	
ENDPROC

/// PURPOSE: Returns the character headshot dds file based on character slot and platform
FUNC TEXT_LABEL_63 Get_Headshot_dds_File(INT iCharacterSlot)
	TEXT_LABEL_63 tlResult
	tlResult = "share/gta5/mpchars/"
	tlResult += iCharacterSlot
	
	IF IS_PROSPERO_VERSION()
		tlResult += "_ps5"
	ELIF IS_SCARLETT_VERSION()
		tlResult += "_xboxsx"
	ELIF IS_ORBIS_VERSION()
		tlResult+= "_ps4"
	ELIF IS_DURANGO_VERSION()
		tlResult += "_xboxone"
	ELIF IS_PC_VERSION()
		// May be added later.
	ENDIF
	tlResult += ".dds"
	
	RETURN tlResult
ENDFUNC

// PURPOSE: Deletes the headshot of the passed character slot
//Returns true when complete, returns if successful in bSuccess
FUNC BOOL Delete_Character_Headshot(INT iCharacterSlot)

	TEXT_LABEL_63 tl63Delete = Get_Headshot_dds_File(iCharacterSlot)
	NET_NL()NET_PRINT("Delete_Character_Headshot(")NET_PRINT_INT(iCharacterSlot)NET_PRINT(") tl63Delete = ")NET_PRINT(tl63Delete)
	
	SWITCH g_iDeleteCharacterHeadshotStage
		CASE 0
		
			DEBUG_PRINTCALLSTACK()
			
			g_iDeleteCharacterHeadshotID = CLOUD_DELETE_MEMBER_FILE(tl63Delete)
			g_iDeleteCharacterHeadshotTimer = GET_GAME_TIMER()
			g_iDeleteCharacterHeadshotStage++
		BREAK
		
		CASE 1
			
			NET_NL()NET_PRINT("Delete_Character_Headshot g_iDeleteCharacterHeadshotID = ")NET_PRINT_INT(g_iDeleteCharacterHeadshotID)
			NET_NL()NET_PRINT("Delete_Character_Headshot CLOUD_HAS_REQUEST_COMPLETED(g_iDeleteCharacterHeadshotID) = ")NET_PRINT_BOOL( CLOUD_HAS_REQUEST_COMPLETED(g_iDeleteCharacterHeadshotID))
			
			IF CLOUD_HAS_REQUEST_COMPLETED(g_iDeleteCharacterHeadshotID)
			OR ((GET_GAME_TIMER() - g_iDeleteCharacterHeadshotTimer) > 10000)
				g_iDeleteCharacterHeadshotStage ++
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 2
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Delete_All_Character_Headshot()

	TEXT_LABEL_31 tl31DeleteChar1
	TEXT_LABEL_31 tl31DeleteChar2
	TEXT_LABEL_31 tl31DeleteChar3
	TEXT_LABEL_31 tl31DeleteChar4
	TEXT_LABEL_31 tl31DeleteChar5
	
	tl31DeleteChar1 = "share/gta5/mpchars/1.dds"
	tl31DeleteChar2 = "share/gta5/mpchars/2.dds"
	tl31DeleteChar3 = "share/gta5/mpchars/3.dds"
	tl31DeleteChar4 = "share/gta5/mpchars/4.dds"
	tl31DeleteChar5 = "share/gta5/mpchars/5.dds"
	IF (CLOUD_HAS_REQUEST_COMPLETED(CLOUD_DELETE_MEMBER_FILE(tl31DeleteChar1))
	AND CLOUD_HAS_REQUEST_COMPLETED(CLOUD_DELETE_MEMBER_FILE(tl31DeleteChar2))
	AND CLOUD_HAS_REQUEST_COMPLETED(CLOUD_DELETE_MEMBER_FILE(tl31DeleteChar3))
	AND CLOUD_HAS_REQUEST_COMPLETED(CLOUD_DELETE_MEMBER_FILE(tl31DeleteChar4))
	AND CLOUD_HAS_REQUEST_COMPLETED(CLOUD_DELETE_MEMBER_FILE(tl31DeleteChar5)))
	OR HAS_NET_TIMER_EXPIRED_ONE_FRAME(g_ClearCharacters_Headshots, 15000, TRUE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
 


// ===========================================================================================================
//      The Main MP Player Headshots Control Routines
// ===========================================================================================================

// PURPOSE:	Clears out the export player headshot variables, so will start to update again
PROC Maintain_Export_Player_Headshot()

	#IF IS_DEBUG_BUILD
	IF g_sDebugExportPlayerStartExportPlayerHeadshotTimer
		Start_Export_Player_Headshot_Timer()
		g_sDebugExportPlayerStartExportPlayerHeadshotTimer = FALSE
	ENDIF
	
	IF g_sDebugExportPlayerForceImmediateExportPlayerHeadshot
		Immediately_Export_Player_Headshot()
		g_sDebugExportPlayerForceImmediateExportPlayerHeadshot = FALSE
	ENDIF
	#ENDIF

	IF Does_Player_Headshot_Needs_Updating()
		IF NOT g_sExportPlayerHeadshotRequested
			IF (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sExportPlayerHeadshotCountdown))
				Setup_Export_Player_Headshot()
			ENDIF
		ENDIF
	ENDIF

	IF g_sExportPlayerHeadshotRequested
	
		BOOL bRetry = FALSE
	
		PEDHEADSHOT_ID playerShot = Get_Transparent_HeadshotID_For_Player(PLAYER_ID(), TRUE)
		
		SWITCH g_sExportPlayerHeadshotStage
			CASE 0
				IF playerShot <> NULL
					IF IS_PEDHEADSHOT_IMG_UPLOAD_AVAILABLE()
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_NET_HEADSHOTS, "=== MAINTAIN EXPORT === Export Player Headshot Set up, commencing with upload request for ID = ", NATIVE_TO_INT(playerShot))
						#ENDIF
						REQUEST_PEDHEADSHOT_IMG_UPLOAD(playerShot)
						g_sExportPlayerHeadshotStage++
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF HAS_PEDHEADSHOT_IMG_UPLOAD_FAILED()
					//Failed, release then back to start
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_HEADSHOTS, "=== MAINTAIN EXPORT === Export Player Headshot HAS_PEDHEADSHOT_IMG_UPLOAD_FAILED = TRUE, restarting")
					#ENDIF
					bRetry = TRUE
				ENDIF
				IF HAS_PEDHEADSHOT_IMG_UPLOAD_SUCCEEDED()
					//Success!, release then complete
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_NET_HEADSHOTS, "=== MAINTAIN EXPORT === Export Player Headshot HAS_PEDHEADSHOT_IMG_UPLOAD_SUCCEEDED = TRUE, complete!")
					#ENDIF
					INT iSlot
					iSlot = GET_ACTIVE_CHARACTER_SLOT()
					IF iSlot != -1
						g_bUseCachedTransparentLocalPlayerHeadshot[iSlot] = FALSE	//we've changed the headshot, let Kenneth's selector know
					ENDIF
					Set_Player_Headshot_Needs_Updating(FALSE)		//done, can finally clear stat
					Clear_Export_Player_Headshot()
					
					g_bTriggerCharacterFacebookPostUpdates = TRUE							//Trigger updating facebook with new character
																							//(Will only post first time export success)
					
					IF playerShot <> NULL
						RELEASE_PEDHEADSHOT_IMG_UPLOAD(playerShot)
						Cancel_And_Clear_MP_Transparent_Headshot()
					ENDIF
				ENDIF
			BREAK
			
		ENDSWITCH	
		
		IF (IS_TIME_MORE_THAN(GET_NETWORK_TIME(), g_sExportPlayerHeadshotTimeout))
			//Timed out, bail out
			#IF IS_DEBUG_BUILD
				CPRINTLN(DEBUG_NET_HEADSHOTS, "=== MAINTAIN EXPORT === Export Player Headshot g_sExportPlayerHeadshotTimeout time up!, failed, but move on")
			#ENDIF
			bRetry = TRUE
		ENDIF
		
		IF bRetry
			IF playerShot <> NULL						//Transparent shot ready
				RELEASE_PEDHEADSHOT_IMG_UPLOAD(playerShot)	//release
				Cancel_And_Clear_MP_Transparent_Headshot()
			ENDIF
			Clear_Export_Player_Headshot()
			Start_Export_Player_Headshot_Timer()
		ENDIF
	ENDIF
ENDPROC

// PURPOSE:	Initialise the MP Player Headshots systen
PROC Initialise_MP_Player_Headshots()
	
	BOOL beingInitialised = TRUE
	
	// Clear the player headshot data
	Clear_All_Generated_MP_Headshots(beingInitialised)
	Cancel_And_Clear_Any_MP_Headshot_Being_Generated()
	Clear_Headshots_To_Be_Generated_Queue()
	Cancel_And_Clear_MP_Transparent_Headshot()
	Initialise_Export_Player_Headshot()
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_HEADSHOTS, "=== INITIALISE === Player Headshot Generation Details after initialisation:")
		Debug_Output_MP_Headshots()
	#ENDIF

ENDPROC





// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Maintain the MP Player Headshot system
PROC Maintain_MP_Player_Headshots()
	
	Maintain_Export_Player_Headshot()
	Maintain_Generated_MP_Headshots()
	Maintain_MP_Headshot_Requests()
	Maintain_An_Active_Headshot_For_All_Players_In_Game()
	Maintain_Generate_Initial_Transparent_Headshot()
	Maintain_Delayed_Headshot_Regeneration()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	Terminate the MP Player Headshot system
PROC Terminate_MP_Player_Headshots()
	
	// Ensure Headshot IDs are freed up if in use
	Clear_All_Generated_MP_Headshots()
	Cancel_And_Clear_Any_MP_Headshot_Being_Generated()
	Clear_Headshots_To_Be_Generated_Queue()
	Cancel_And_Clear_MP_Transparent_Headshot()
	Clear_Export_Player_Headshot()
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_NET_HEADSHOTS, "=== TERMINATE === Player Headshot Generation Details after termination:")
		Debug_Output_MP_Headshots()
	#ENDIF
	
ENDPROC

