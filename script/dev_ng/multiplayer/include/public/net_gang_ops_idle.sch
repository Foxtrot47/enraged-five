/* ===================================================================================== *\
		MISSION NAME	:	net_gang_ops_idle.sch
		AUTHOR			:	Alastair Costley - 2017/07/03
		DESCRIPTION		:	Header for the Gang Ops planning board.
\* ===================================================================================== */


// INCLUDE FILES.
USING "globals.sch"
USING "commands_network.sch"
USING "net_include.sch"
USING "net_FPS_cam.sch"
USING "net_gang_ops_helpers.sch"
USING "net_gang_ops_scaleform.sch"
USING "MP_Scaleform_Functions.sch"
USING "hud_drawing.sch"
USING "SVM_MISSION_FLOW.sch"

/* ===================================================================================== *\
			VARIABLES + INITIAL DECLARATIONS + TYPES
\* ===================================================================================== */


/* ===================================================================================== *\
			DEBUG ONLY
\* ===================================================================================== */
#IF IS_DEBUG_BUILD

PROC PRIVATE_PROCESS_GANG_OPS_IDLE_WIDGET_COMMANDS(GANG_OPS_CLIENT_IDLE_DATA &sClientData)

	IF sClientData.sDebugData.bStart
	
		IF sClientData.sStateData.eState != GOBS_IDLE
			sClientData.sStateData.eState = GOBS_IDLE
		ENDIF
		
		sClientData.sStateData.eState = GOBS_INIT
		sClientData.sDebugData.bStart = FALSE
		
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - Debug Starting Planning")
	ENDIF
	
//	IF NOT sClientData.sDebugData.bPlayerVisible
//		SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID(), TRUE)
//	ENDIF
	
//	IF sClientData.sDebugData.bLockToPlayer
//	AND NOT sClientData.sStateData.bInteract
//	
//		VECTOR vBoardPos = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
//		vBoardPos.Z = vBoardPos.Z + 2.0
//		sClientData.sPositionData.vBoardPosition = vBoardPos  
//		
//		VECTOR vBoardRot = GET_ENTITY_ROTATION(PLAYER_PED_ID())
//		sClientData.sPositionData.vBoardRotation = vBoardRot
//	ENDIF
	
	sClientData.sCameraData.ePositionId = INT_TO_ENUM(GANG_OPS_CAMERA_LOCATION, sClientData.sCameraData.iPositionId)
	
ENDPROC

PROC PRIVATE_GANG_OPS_IDLE_WIDGET_UPDATE(GANG_OPS_CLIENT_IDLE_DATA &sClientData)

	TEXT_LABEL_63 sTemp = ""

	IF NOT sClientData.sDebugData.bCreatedWidgets
		IF DOES_WIDGET_GROUP_EXIST(sClientData.sDebugData.wgParentGroup)
			SET_CURRENT_WIDGET_GROUP(sClientData.sDebugData.wgParentGroup)
			
			sClientData.sDebugData.wgActiveGroup = START_WIDGET_GROUP("  Gang Ops (Planning)") 
			
				ADD_WIDGET_STRING(" - DEBUG SETTINGS - ")
				ADD_WIDGET_BOOL("Start", sClientData.sDebugData.bStart)
				ADD_WIDGET_BOOL("Reset", sClientData.sDebugData.bReset)
				ADD_WIDGET_BOOL("Player Visibility", sClientData.sDebugData.bPlayerVisible)
				ADD_WIDGET_BOOL("Lock Rendering to Player", sClientData.sDebugData.bLockToPlayer)
				
				ADD_WIDGET_STRING(" - DEBUG ACTIONS - ")
				ADD_WIDGET_BOOL("Interact", sClientData.sStateData.bInteract)
				
				
				START_WIDGET_GROUP("Component Toggle")
					ADD_WIDGET_BOOL("Component Toggle: Scaleform", sClientData.sScaleformData.bEnableScaleform)
					ADD_WIDGET_BOOL("Component Toggle: Camera", sClientData.sCameraData.bEnableCamera)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Scaleform")
					ADD_WIDGET_BOOL("Verbose Scaleform Logging", sClientData.sScaleformData.bVerboseDebugging)
					
	//				ADD_WIDGET_VECTOR_SLIDER("Main Board - World Pos", sClientData.sScaleformData.vMainBoardPosition, -9999.9, 9999.9, 1.0)
	//				ADD_WIDGET_VECTOR_SLIDER("Main Board - World Rot", sClientData.sScaleformData.vMainBoardRotation, -1000.0, 1000.0, 1.0)
	//				ADD_WIDGET_VECTOR_SLIDER("Main Board - Size", sClientData.sScaleformData.vMainBoardSize, 0.0, 100.0, 1.0)
	//				ADD_WIDGET_VECTOR_SLIDER("Main Board - World Size", sClientData.sScaleformData.vMainBoardWorldSize, 0.0, 100.0, 1.0)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Cameras")
				
					ADD_WIDGET_INT_SLIDER("Cam Position", sClientData.sCameraData.iPositionId, 0, 10, 1)
					ADD_WIDGET_VECTOR_SLIDER("Cam Initial Pos Offset", sClientData.sCameraData.vCamInitialOffset, -100.0, 100.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("Cam Map Pos Offset", sClientData.sCameraData.vCamMapOffset, -100.0, 100.0, 0.1)
					
					ADD_WIDGET_VECTOR_SLIDER("Cam Initial Rot Offset", sClientData.sCameraData.vCamInitialOffsetRot, -180.0, 180.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("Cam Map Rot Offset", sClientData.sCameraData.vCamMapOffsetRot, -180.0, 180.0, 0.1)
				
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Idle Data")
				
					sTemp = "Screen Name: " 
					sTemp += sClientData.sIdleData.tlScreenName
					ADD_WIDGET_STRING(sTemp)
								
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Highlight / Cursor Data") 
					//DEBUG_eFocusPoint = ENUM_TO_INT(			g_HeistPrePlanningClient.sBoardState.eFocusPoint)
					//ADD_WIDGET_INT_SLIDER("eFocusPoint: ", 		DEBUG_eFocusPoint, -100, 100, 1)
					ADD_WIDGET_INT_SLIDER("iHighlight: ", 		sClientData.sCursorData.iHighlight, -100, 100, 1)
					ADD_WIDGET_INT_SLIDER("iSubItemSelection: ",sClientData.sCursorData.iSubItemSelection, -100, 100, 1)
				STOP_WIDGET_GROUP()
				
			STOP_WIDGET_GROUP()
			
			CLEAR_CURRENT_WIDGET_GROUP(sClientData.sDebugData.wgParentGroup)
			
			sClientData.sDebugData.bCreatedWidgets = TRUE
		ENDIF
	ENDIF

	PRIVATE_PROCESS_GANG_OPS_IDLE_WIDGET_COMMANDS(sClientData)

ENDPROC

#ENDIF

/* ===================================================================================== *\
			HELPERS AND MISC WRAPPERS
\* ===================================================================================== */

FUNC INT PRIVATE_GET_SELECTED_STRAND_FROM_SELECTION_ID(INT iSelectionId)
	
	SWITCH iSelectionID
		CASE GANG_OPS_IDLE_SELECTION_ID_REPLAY_1 		RETURN GANG_OPS_MISSION_1
		CASE GANG_OPS_IDLE_SELECTION_ID_REPLAY_2		RETURN GANG_OPS_MISSION_2
		CASE GANG_OPS_IDLE_SELECTION_ID_REPLAY_3		RETURN GANG_OPS_MISSION_3
		CASE GANG_OPS_IDLE_SELECTION_ID_REPLAY_4 		RETURN GANG_OPS_MISSION_4
		CASE GANG_OPS_IDLE_SELECTION_ID_REPLAY_5 		RETURN GANG_OPS_MISSION_5
	ENDSWITCH

	RETURN 0
ENDFUNC

PROC PRIVATE_REQUEST_GANG_OPS_IDLE_SCALEFORM(GANG_OPS_SCALEFORM_DATA &sScaleformData)
	sScaleformData.siMainBoard = REQUEST_SCALEFORM_MOVIE("IAA_HEIST_BOARD")
	sScaleformData.siButtons = REQUEST_SCALEFORM_MOVIE("INSTRUCTIONAL_BUTTONS")
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Request Assets - Requested scaleform assets.")
ENDPROC

/// PURPOSE:
///    Maintain the scaleform instructional movie (bottom right). This handles all of it's own state and is mostly automated. If a new state is added to the
///    planning board, support must also be extended here.
/// PARAMS:
///    sScaleformStruct - 
PROC PRIVATE_UPDATE_GANG_OPS_IDLE_INSTR_BTNS(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	
	SPRITE_PLACEMENT spPlacementLocation = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	
	// Don't render the buttons if the player is typing in network chat.
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF
	
	IF NOT sClientData.sScaleformData.bUpdateInstructions
	
		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sClientData.sScaleformData.siButtons,
											spPlacementLocation,
											sClientData.sScaleformData.sButtons,
											FALSE)
	
		EXIT
	ENDIF
	
	IF NOT GET_IS_WIDESCREEN()
	OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
	      SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sClientData.sScaleformData.sButtons, 0.5)
	ELSE
	      SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sClientData.sScaleformData.sButtons, 0.7)
	ENDIF
	
	REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sClientData.sScaleformData.sButtons)
	
	SWITCH sClientData.sCameraData.ePositionId
	
		CASE GOCAM_MAIN
		
			IF sClientData.sIdleData.iStrandStatus[PRIVATE_GET_SELECTED_STRAND_FROM_SELECTION_ID(sClientData.sCursorData.iHighlight)] = GANG_OPS_MISSION_STATE_AVAILABLE
			
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
					"HPSEL_STRAND", sClientData.sScaleformData.sButtons, TRUE)
				
			ENDIF
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
				"HEIST_IB_QUIT", sClientData.sScaleformData.sButtons, TRUE) // Go back.
							
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
				"HP_OVERVIEW", sClientData.sScaleformData.sButtons, TRUE) // View overview.
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_RB, 
				"HP_SURVEIL", sClientData.sScaleformData.sButtons, TRUE) // View surveillance.
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_LB, 
				"HP_MAP", sClientData.sScaleformData.sButtons, TRUE) // View map.
							
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
					"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
			ELSE
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_TRIGGERS, 
					"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons) // Zoom 
			ENDIF
				
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
				"HEIST_IB_LOOK", sClientData.sScaleformData.sButtons) // Look Around

			IF sClientData.sIdleData.bShowReplayBoard
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
					"HEIST_IB_NAV", sClientData.sScaleformData.sButtons) // Navigate controls.
			ENDIF
			
		BREAK
			
		CASE GOCAM_OVERVIEW
		
			IF sClientData.sIdleData.iStrandStatus[PRIVATE_GET_SELECTED_STRAND_FROM_SELECTION_ID(sClientData.sCursorData.iHighlight)] = GANG_OPS_MISSION_STATE_AVAILABLE
			
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
					"HPSEL_STRAND", sClientData.sScaleformData.sButtons, TRUE)
				
			ENDIF
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
				"HEIST_IB_QUIT", sClientData.sScaleformData.sButtons, TRUE) // Go back.	
		
			SWITCH sClientData.sCameraData.eCachedPositionId
				CASE GOCAM_MAIN
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
					"HP_HEIST", sClientData.sScaleformData.sButtons, TRUE) // View heists
				BREAK
				
				CASE GOCAM_MAP
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
					"HP_MAP", sClientData.sScaleformData.sButtons, TRUE) // View map
				BREAK
				
				CASE GOCAM_IMAGES
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
					"HP_SURVEIL", sClientData.sScaleformData.sButtons, TRUE) // View surveillance
				BREAK
			ENDSWITCH
						
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
					"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
			ELSE
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_RT, 
					"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
			ENDIF
				
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
				"HEIST_IB_LOOK", sClientData.sScaleformData.sButtons) // Look 
		
			IF sClientData.sIdleData.bShowReplayBoard
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
					"HEIST_IB_NAV", sClientData.sScaleformData.sButtons) // Navigate controls.
			ENDIF
			
		BREAK
		
		CASE GOCAM_MAP
		
			IF sClientData.sIdleData.iStrandStatus[PRIVATE_GET_SELECTED_STRAND_FROM_SELECTION_ID(sClientData.sCursorData.iHighlight)] = GANG_OPS_MISSION_STATE_AVAILABLE
			
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
					"HPSEL_STRAND", sClientData.sScaleformData.sButtons, TRUE)
				
			ENDIF
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
				"HEIST_IB_QUIT", sClientData.sScaleformData.sButtons, TRUE)
					
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
				"HP_OVERVIEW", sClientData.sScaleformData.sButtons, TRUE) // View overview.
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_RB, 
				"HP_HEIST", sClientData.sScaleformData.sButtons, TRUE) // View missions.
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_LB, 
				"HP_SURVEIL", sClientData.sScaleformData.sButtons, TRUE) // View surveillance.
												
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
					"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
			ELSE
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_TRIGGERS, 
					"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons) // Zoom 
			ENDIF
				
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
				"HEIST_IB_LOOK", sClientData.sScaleformData.sButtons) // Look Around

			IF sClientData.sIdleData.bShowReplayBoard
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
					"HEIST_IB_NAV", sClientData.sScaleformData.sButtons) // Navigate controls.
			ENDIF
		BREAK
			
		CASE GOCAM_IMAGES
		
			IF sClientData.sIdleData.iStrandStatus[PRIVATE_GET_SELECTED_STRAND_FROM_SELECTION_ID(sClientData.sCursorData.iHighlight)] = GANG_OPS_MISSION_STATE_AVAILABLE
			
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, 
					"HPSEL_STRAND", sClientData.sScaleformData.sButtons, TRUE)
				
			ENDIF
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, 
				"HEIST_IB_QUIT", sClientData.sScaleformData.sButtons, TRUE)
					
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, 
				"HP_OVERVIEW", sClientData.sScaleformData.sButtons, TRUE) // View overview.
			
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_RB, 
				"HP_MAP", sClientData.sScaleformData.sButtons, TRUE) // View map.
		
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_LB, 
				"HP_HEIST", sClientData.sScaleformData.sButtons, TRUE) // View missions.
												
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM, 
					"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons, TRUE) // Zoom
			ELSE
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_TRIGGERS, 
					"HEIST_IB_ZOOM", sClientData.sScaleformData.sButtons) // Zoom 
			ENDIF
				
			ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_LOOK, 
				"HEIST_IB_LOOK", sClientData.sScaleformData.sButtons) // Look Around

			IF sClientData.sIdleData.bShowReplayBoard
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT_GROUP(FRONTEND_CONTROL, INPUTGROUP_MOVE, 
					"HEIST_IB_NAV", sClientData.sScaleformData.sButtons) // Navigate controls.
			ENDIF
		BREAK
	
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Buttons - Updated instructional buttons for position: ", ENUM_TO_INT(sClientData.sCameraData.ePositionId))
	
	RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sClientData.sScaleformData.siButtons,
										spPlacementLocation,
										sClientData.sScaleformData.sButtons,
										sClientData.sScaleformData.bUpdateInstructions)
											
	PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, FALSE)
								
ENDPROC

PROC PRIVATE_RENDER_GANG_OPS_IDLE_SCALEFORM(GANG_OPS_CLIENT_IDLE_DATA &sClientData)

	IF sClientData.sScaleformData.bDrawBoard
	AND g_bPlayingGangOpsCutscene = FALSE
		IF HAS_SCALEFORM_MOVIE_LOADED(sClientData.sScaleformData.siMainBoard)

			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(TRUE)
			SET_TEXT_RENDER_ID(sClientData.sScaleformData.iRenderTargetID)
			SET_SCALEFORM_MOVIE_TO_USE_SUPER_LARGE_RT(sClientData.sScaleformData.siMainBoard, TRUE)
			DRAW_SCALEFORM_MOVIE(sClientData.sScaleformData.siMainBoard, cfHEIST_SCREEN_CENTRE_X, cfHEIST_SCREEN_CENTRE_Y, cfHEIST_SCREEN_WIDTH, cfHEIST_SCREEN_HEIGHT, 255, 255, 255, 255)
			SET_TEXT_RENDER_ID(GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID())
			SET_SCRIPT_GFX_DRAW_BEHIND_PAUSEMENU(FALSE)
						
			IF NOT sClientData.sAudioData.bStartedLoopingBackgroundSound
				sClientData.sAudioData.iLoopingBackgroundSoundId = GET_SOUND_ID()
				IF sClientData.sAudioData.iLoopingBackgroundSoundId != -1
					PLAY_SOUND_FROM_COORD(sClientData.sAudioData.iLoopingBackgroundSoundId, "Background", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Render - Started looping background sound as we've started drawing the screen. Sound ID: ", sClientData.sAudioData.iLoopingBackgroundSoundId)
					sClientData.sAudioData.bStartedLoopingBackgroundSound = TRUE
				ENDIF
			ENDIF
									
			IF sClientData.sScaleformData.bVerboseDebugging
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Render - Index: ", NATIVE_TO_INT(sClientData.sScaleformData.siMainBoard))
			ENDIF
		ENDIF
	ENDIF
	
	IF sClientData.sScaleformData.bDrawInstructions	
		PRIVATE_UPDATE_GANG_OPS_IDLE_INSTR_BTNS(sClientData)
	ENDIF

ENDPROC

PROC PRIVATE_INIT_GANG_OPS_IDLE_CAMERA(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	
	sClientData.sCameraData.vBaseRotation.X = 0.0
	sClientData.sCameraData.vBaseRotation.Y = 0.0
	sClientData.sCameraData.vBaseRotation.Z = -180.0
	
	VECTOR vPosOffset = PUBLIC_GET_GANG_OPS_POSITION_OFFSET(sClientData.sCameraData, GOVEC_CAM_INITIAL)
	VECTOR vCamPos = PUBLIC_GET_GANG_OPS_CAMERA_POSITION_FOR_POSITION_ID(sClientData.sCameraData)
	
	VECTOR vRotOffset = PUBLIC_GET_GANG_OPS_ROTATION_OFFSET(sClientData.sCameraData, GOVEC_CAM_INITIAL)
	VECTOR vCamRot = PUBLIC_GET_GANG_OPS_CAMERA_ROTATION_FOR_POSITION_ID(sClientData.sCameraData)
	
	INIT_FIRST_PERSON_CAMERA(	sClientData.sCameraData.sFPSCam, 
								vCamPos,
								vCamRot, // TODO: Re-add for rotation.
								60, // TODO: This needs to be done properly 
								DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, 0, -1, FALSE)

	PUBLIC_SET_GANG_OPS_FPS_CAM_FAR(sClientData.sCameraData)

	SET_WIDESCREEN_BORDERS(TRUE,0)	
	
	GANG_OPS_VECTORS eNewLoc = PUBLIC_GET_GANG_OPS_POSITION_FOR_CAM_STATE(sClientData.sCameraData.ePositionId)

	vPosOffset = PUBLIC_GET_GANG_OPS_POSITION_OFFSET(sClientData.sCameraData, eNewLoc)
	vRotOffset = PUBLIC_GET_GANG_OPS_ROTATION_OFFSET(sClientData.sCameraData, eNewLoc)

	sClientData.sCameraData.ePositionId = GOCAM_MAIN
	
	sClientData.sCameraData.vCamCurrentPos = PUBLIC_GET_GANG_OPS_CAMERA_POSITION_FOR_POSITION_ID(sClientData.sCameraData)
	sClientData.sCameraData.vCamCurrentRot = PUBLIC_GET_GANG_OPS_CAMERA_ROTATION_FOR_POSITION_ID(sClientData.sCameraData)
	
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Camera - Started FPS camera.")
	CDEBUG3LN(DEBUG_GANG_OPS_PLAN, "   ... Cam Pos: ", vCamPos)
	CDEBUG3LN(DEBUG_GANG_OPS_PLAN, "   ... Cam Rot: ", vCamRot)
	CDEBUG3LN(DEBUG_GANG_OPS_PLAN, "   ... Cam Current Pos: ", sClientData.sCameraData.vCamCurrentPos)
	CDEBUG3LN(DEBUG_GANG_OPS_PLAN, "   ... Cam Current Rot: ", sClientData.sCameraData.vCamCurrentRot)
	CDEBUG3LN(DEBUG_GANG_OPS_PLAN, "   ... Pos Offset: ", vPosOffset)
	CDEBUG3LN(DEBUG_GANG_OPS_PLAN, "   ... Rot Offset: ", vRotOffset)
	CDEBUG3LN(DEBUG_GANG_OPS_PLAN, "   ... Base Rot: ", sClientData.sCameraData.vBaseRotation)

ENDPROC

PROC PRIVATE_CLEAR_GANG_OPS_IDLE_CAMERA(GANG_OPS_CLIENT_IDLE_DATA &sClientData)

	CLEAR_FIRST_PERSON_CAMERA(sClientData.sCameraData.sFPSCam)
	SET_WIDESCREEN_BORDERS(FALSE,0)	

	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Camera - Cleared FPS camera.")
	
ENDPROC


/// PURPOSE:
///    Manage and maintain the UP/DOWN selection indexes driven from by front-end controls.
/// PARAMS:
///    eHighlightType - ENUM - Direction of travel for the highlighter.
PROC PRIVATE_UPDATE_GANG_OPS_IDLE_CONTROL_SELECTIONS(GANG_OPS_CLIENT_IDLE_DATA &sClientData, GANG_OPS_CONTROL_TYPE eControl)

	SWITCH eControl
	
		CASE GOCT_DOWN
		
			PUBLIC_GANG_OPS_SET_INPUT_EVENT(sClientData.sScaleformData.siMainBoard, INPUT_FRONTEND_DOWN)

			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - DOWN.")
			
			SWITCH sClientData.sIdleData.iNumStrandsToShow
				CASE 1
					PLAY_SOUND_FROM_COORD(-1, "Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
				BREAK
				
				CASE 2
					IF sClientData.sCursorData.iHighlight = GANG_OPS_IDLE_SELECTION_ID_REPLAY_2 
						PLAY_SOUND_FROM_COORD(-1, "Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					ELSE
						PLAY_SOUND_FROM_COORD(-1, "Nav_Up_Down_Photo_Change", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					ENDIF
				BREAK
				
				CASE 3
					IF sClientData.sCursorData.iHighlight = GANG_OPS_IDLE_SELECTION_ID_REPLAY_3 
						PLAY_SOUND_FROM_COORD(-1, "Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					ELSE
						PLAY_SOUND_FROM_COORD(-1, "Nav_Up_Down_Photo_Change", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
					ENDIF
				BREAK
			ENDSWITCH

			BREAK
			
		CASE GOCT_UP
		
			PUBLIC_GANG_OPS_SET_INPUT_EVENT(sClientData.sScaleformData.siMainBoard, INPUT_FRONTEND_UP)
			
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - UP.")
			
			IF sClientData.sCursorData.iHighlight = GANG_OPS_IDLE_SELECTION_ID_REPLAY_1
				PLAY_SOUND_FROM_COORD(-1, "Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ELSE
				PLAY_SOUND_FROM_COORD(-1, "Nav_Up_Down_Photo_Change", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ENDIF
			
			BREAK
			
		CASE GOCT_LEFT
		
			PUBLIC_GANG_OPS_SET_INPUT_EVENT(sClientData.sScaleformData.siMainBoard, INPUT_FRONTEND_LEFT)
		
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - LEFT.")
			
			PLAY_SOUND_FROM_COORD(-1, "Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			
			BREAK
			
			
		CASE GOCT_RIGHT
		
			PUBLIC_GANG_OPS_SET_INPUT_EVENT(sClientData.sScaleformData.siMainBoard, INPUT_FRONTEND_RIGHT)
		
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - RIGHT.")
			
			PLAY_SOUND_FROM_COORD(-1, "Nav_Blocked", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			
			BREAK
			
	ENDSWITCH

ENDPROC

PROC PRIVATE_UPDATE_IDLE_CAMERA_AND_FOCUS_FOR_INPUT(GANG_OPS_CLIENT_IDLE_DATA &sClientData, GANG_OPS_CONTROL_TYPE eControl)

	// TODO: Do we want to cancel transitions midway?
	IF sClientData.sCameraData.bTransitioning
		EXIT
	ENDIF

	SWITCH eControl
	
		CASE GOCT_LB

			IF sClientData.sCameraData.ePositionId = GOCAM_MAIN
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_MAP")
				sClientData.sCameraData.ePositionId = GOCAM_MAP
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_MAP)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ELIF sClientData.sCameraData.ePositionId = GOCAM_MAP
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_IMAGES")
				sClientData.sCameraData.ePositionId = GOCAM_IMAGES
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_IMAGES)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ELIF sClientData.sCameraData.ePositionId = GOCAM_IMAGES
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_MAIN")
				sClientData.sCameraData.ePositionId = GOCAM_MAIN
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_MAIN)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ENDIF
		
		BREAK
		
		CASE GOCT_RB
		
			IF sClientData.sCameraData.ePositionId = GOCAM_MAIN
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_IMAGES")
				sClientData.sCameraData.ePositionId = GOCAM_IMAGES
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_IMAGES)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ELIF sClientData.sCameraData.ePositionId = GOCAM_MAP
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_MAIN")
				sClientData.sCameraData.ePositionId = GOCAM_MAIN
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_MAIN)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ELIF sClientData.sCameraData.ePositionId = GOCAM_IMAGES
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_MAP")
				sClientData.sCameraData.ePositionId = GOCAM_MAP
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_MAP)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ENDIF
		
		BREAK
			
		CASE GOCT_Y
		
			IF sClientData.sCameraData.ePositionId != GOCAM_OVERVIEW
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_MAIN")
				sClientData.sCameraData.eCachedPositionId = sClientData.sCameraData.ePositionId
				sClientData.sCameraData.iCachedPositionId = sClientData.sCameraData.iPositionId
				sClientData.sCameraData.ePositionId = GOCAM_OVERVIEW
				sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_OVERVIEW)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ELIF sClientData.sCameraData.ePositionId = GOCAM_OVERVIEW
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Inputs - Setting camera focus to: GOCAM_OVERVIEW")
				sClientData.sCameraData.ePositionId = sClientData.sCameraData.eCachedPositionId
				sClientData.sCameraData.iPositionId = sClientData.sCameraData.iCachedPositionId
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			ENDIF
		
		BREAK
	
	ENDSWITCH

ENDPROC

PROC PRIVATE_TOGGLE_GANG_OPS_IDLE_INTERACTION(GANG_OPS_CLIENT_IDLE_DATA &sClientData, BOOL bEnableInteract)
	sClientData.sStateData.bInteract = bEnableInteract
	sClientData.sCameraData.ePositionId = GOCAM_MAIN
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_TOGGLE_GANG_OPS_INTERACTION - Toggle interact: ", sClientData.sStateData.bInteract)
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_IDLE_NET_DATA(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	IF sClientData.sIdleData.piLeader != PLAYER_ID()
	AND sClientData.sIdleData.piLeader != INVALID_PLAYER_INDEX()
		IF GlobalplayerBD_FM_2[NATIVE_TO_INT(sClientData.sIdleData.piLeader)].GangOps.iSyncId != GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].GangOps.iSyncId
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_PLANNING_NET_DATA - My sync ID is out of sync with the leader, updating")
		
			sClientData.sIdleData.bUpdateMissionStates = TRUE
			GlobalplayerBD_FM_2[NATIVE_TO_INT(PLAYER_ID())].GangOps.iSyncId = GlobalplayerBD_FM_2[NATIVE_TO_INT(sClientData.sIdleData.piLeader)].GangOps.iSyncId
		ENDIF
		
		IF sClientData.sCursorData.iHighlight != GlobalplayerBD_FM_2[NATIVE_TO_INT(sClientData.sIdleData.piLeader)].GangOps.iHighlight
			sClientData.sCursorData.iHighlight = GlobalplayerBD_FM_2[NATIVE_TO_INT(sClientData.sIdleData.piLeader)].GangOps.iHighlight 
			PUBLIC_GANG_OPS_SET_CURRENT_SELECTION(sClientData.sScaleformData.siMainBoard, sClientData.sCursorData.iHighlight)
			PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			PUBLIC_SET_GANG_OPS_MAP_MARKERS_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
			PUBLIC_SET_GANG_OPS_FLAVOUR_IMAGES_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
			CDEBUG1LN(DEBUG_GANG_OPS_PLAN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_IDLE_NET_DATA - leader highlight has changed, new highlight: ", sClientData.sCursorData.iHighlight)
		ENDIF
	ENDIF
ENDPROC

PROC PRIVATE_SET_GANG_OPS_IDLE_NAVIGATION(GANG_OPS_CLIENT_IDLE_DATA &sClientData, BOOL bEnable)
	IF sClientData.sIdleData.piLeader = PLAYER_ID()
		IF bEnable
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_SET_GANG_OPS_NAVIGATION - bEnable")
			CALL_SCALEFORM_MOVIE_METHOD(sClientData.sScaleformData.siMainBoard, "ENABLE_NAVIGATION")
		ELSE
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_SET_GANG_OPS_NAVIGATION - !bEnable")
			CALL_SCALEFORM_MOVIE_METHOD(sClientData.sScaleformData.siMainBoard, "DISABLE_NAVIGATION")
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_SET_GANG_OPS_NAVIGATION - Not leader, disable")
		CALL_SCALEFORM_MOVIE_METHOD(sClientData.sScaleformData.siMainBoard, "DISABLE_NAVIGATION")
	ENDIF
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_IDLE_TUTORIAL(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	IF sClientData.sIdleData.piLeader = PLAYER_ID()
	AND NOT HAS_PLAYER_COMPLETED_GANG_OPS_SETUP_TUTORIAL(PLAYER_ID())
		SWITCH sClientData.sTutorialData.iTutorialStage
			CASE 0
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					// [BOSS ONLY]
					// There is an initial cost to begin a Heist. This fee goes towards buying a portion of the equipment needed for the Heist and to cover Lester's cut.
					PRINT_HELP("HP_SETUP_TUT1", 10000)
					GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.bRunningTutorial = TRUE
					sClientData.sTutorialData.bTutorialInProgress = TRUE
					PRIVATE_SET_GANG_OPS_IDLE_NAVIGATION(sClientData, FALSE)
					PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
					sClientData.sTutorialData.iTutorialStage++
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_IDLE_TUTORIAL - HP_SETUP_TUT1 done, advancing tutorial stage")
				ENDIF
			BREAK
			
			CASE 1
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HP_SETUP_TUT1")
					SET_PLAYER_HAS_COMPLETED_GANG_OPS_SETUP_TUTORIAL()
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_IDLE_TUTORIAL - Tutorial finished!")
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		IF sClientData.sTutorialData.bTutorialInProgress
			sClientData.sTutorialData.bTutorialInProgress = FALSE
			PRIVATE_SET_GANG_OPS_IDLE_NAVIGATION(sClientData, TRUE)
			sClientData.sScaleformData.bDrawInstructions = TRUE
			PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
			GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.bRunningTutorial = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC PRIVATE_SET_GANG_OPS_IDLE_HIGHLIGHT_UPDATE_REQUIRED(GANG_OPS_CLIENT_IDLE_DATA &sClientData, BOOL bUpdate)
	sClientData.sCursorData.bUpdateHighlight = bUpdate
	CDEBUG1LN(DEBUG_GANG_OPS_PLAN, "[GANGOPS_FLOW] PRIVATE_SET_GANG_OPS_IDLE_HIGHLIGHT_UPDATE_REQUIRED - bUpdate = ", GET_STRING_FROM_BOOL(bUpdate))
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_IDLE_HIGHLIGHT(GANG_OPS_CLIENT_IDLE_DATA &sClientData)

	INT iNewHighlight

	IF sClientData.sCursorData.bUpdateHighlight
	AND sClientData.sCursorData.bGetSelectionRequested
		CDEBUG1LN(DEBUG_GANG_OPS_PLAN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_IDLE_HIGHLIGHT - New input, scrap the current get selection query and grab the new one")
		sClientData.sCursorData.bGetSelectionRequested = FALSE
	ENDIF

	IF NOT sClientData.sCursorData.bGetSelectionRequested
		IF sClientData.sCursorData.bUpdateHighlight
			BEGIN_SCALEFORM_MOVIE_METHOD(sClientData.sScaleformData.siMainBoard, "GET_CURRENT_SELECTION")
			sClientData.sCursorData.currentSelectionReturnIndex = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
			sClientData.sCursorData.bUpdateHighlight = FALSE
			sClientData.sCursorData.bGetSelectionRequested = TRUE
			CDEBUG1LN(DEBUG_GANG_OPS_PLAN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_IDLE_HIGHLIGHT - Started to grab current selection, return index: ", NATIVE_TO_INT(sClientData.sCursorData.currentSelectionReturnIndex))
		ENDIF
	ELSE
		IF IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(sClientData.sCursorData.currentSelectionReturnIndex)
			CDEBUG1LN(DEBUG_GANG_OPS_PLAN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_FINALE_HIGHLIGHT - Scaleform movie method return ", NATIVE_TO_INT(sClientData.sCursorData.currentSelectionReturnIndex)," ready!")
			iNewHighlight = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(sClientData.sCursorData.currentSelectionReturnIndex)
			IF sClientData.sCursorData.iHighlight != iNewHighlight
				sClientData.sCursorData.iHighlight = iNewHighlight
				GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iHighlight = iNewHighlight
				DO_INTERACT_ANIM()
//				DO_LOOK_AT_COORD(iNewHighlight)
				PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
				PUBLIC_SET_GANG_OPS_FLAVOUR_IMAGES_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
				PUBLIC_SET_GANG_OPS_MAP_MARKERS_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
				CDEBUG1LN(DEBUG_GANG_OPS_PLAN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_IDLE_HIGHLIGHT - new highlight: ", sClientData.sCursorData.iHighlight, ", selected member: ", sClientData.sCursorData.iSelectedMember)
				sClientData.sCursorData.bGetSelectionRequested = FALSE
			ELSE
				CDEBUG1LN(DEBUG_GANG_OPS_PLAN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_IDLE_HIGHLIGHT - Selection/highlight hasnt updated")
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_GANG_OPS_PLAN, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_IDLE_HIGHLIGHT - Waiting for scaleform movie method return ", NATIVE_TO_INT(sClientData.sCursorData.currentSelectionReturnIndex)," to be ready...")
		ENDIF
	ENDIF
ENDPROC

PROC PRIVATE_SELECT_GANG_OPS_STRAND(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
//	IF sClientData.sCameraData.ePositionId != GOCAM_MAIN
//	AND sClientData.sCameraData.ePositionId != GOCAM_OVERVIEW
//		EXIT
//	ENDIF
	
	INT iStrand = PRIVATE_GET_SELECTED_STRAND_FROM_SELECTION_ID(sClientData.sCursorData.iHighlight)	
	
	IF sClientData.sIdleData.iStrandStatus[iStrand] = GANG_OPS_MISSION_STATE_AVAILABLE
		PLAY_SOUND_FROM_COORD(-1,"Select", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
		sClientData.sIdleData.iWarningScreen = GANG_OPS_WARNING_SCREEN_START_STRAND
		
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_SELECT_GANG_OPS_STRAND - Selected to start ", GET_GANG_OPS_MISSION_STRAND_NAME_FOR_DEBUG(GET_GANG_OPS_MISSION_STRAND_FROM_CONST(sClientData.sIdleData.iStrandId[iStrand])), ". Starting warning screen!")
		
	ELIF sClientData.sIdleData.iStrandStatus[iStrand] = GANG_OPS_MISSION_STATE_ON_COOLDOWN
		PLAY_SOUND_FROM_COORD(-1,"Select", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
		PRINT_HELP("HPSTRAND_ONCD")
		
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_SELECT_GANG_OPS_STRAND - Selected to start ", GET_GANG_OPS_MISSION_STRAND_NAME_FOR_DEBUG(GET_GANG_OPS_MISSION_STRAND_FROM_CONST(sClientData.sIdleData.iStrandId[iStrand])), " which is on cooldown!")
	ENDIF
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_IDLE_INPUTS(GANG_OPS_CLIENT_IDLE_DATA &sClientData)

	IF sClientData.sIdleData.iWarningScreen != -1
	OR sClientData.sIdleData.iWarningScreenBuffer != -1
		EXIT
	ENDIF

	GANG_OPS_CONTROL_TYPE eControlType = PUBLIC_GET_GANG_OPS_INPUTS_THIS_FRAME_ONLY(sClientData.sInputData)
	
	SWITCH eControlType
		CASE GOCT_UP
		CASE GOCT_DOWN
		CASE GOCT_LEFT
		CASE GOCT_RIGHT
		
			IF sClientData.sIdleData.bShowReplayBoard
				PRIVATE_UPDATE_GANG_OPS_IDLE_CONTROL_SELECTIONS(sClientData, eControlType)
				PRIVATE_SET_GANG_OPS_IDLE_HIGHLIGHT_UPDATE_REQUIRED(sClientData, TRUE)
			ENDIF
			
			BREAK
			
		// Camera Pan
		CASE GOCT_LB
		CASE GOCT_RB
		CASE GOCT_Y
		
			PRIVATE_UPDATE_IDLE_CAMERA_AND_FOCUS_FOR_INPUT(sClientData, eControlType)
		
			BREAK
			
		// Camera Cycle (PC only)
//		CASE GOCT_Y
//		
//			// Y is only used for PC to cycle left as if pressing LB on a controller
//			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
//				PRIVATE_UPDATE_IDLE_CAMERA_AND_FOCUS_FOR_INPUT(sClientData, GOCT_LB)
//				PRIVATE_UPDATE_GANG_OPS_IDLE_NAVIGATION(sClientData)
//			ENDIF
//				PRIVATE_UPDATE_IDLE_CAMERA_AND_FOCUS_FOR_INPUT(sClientData, eControlType)
//				PRIVATE_UPDATE_GANG_OPS_IDLE_NAVIGATION(sClientData)
//			BREAK
			
		CASE GOCT_B
		
			PRIVATE_TOGGLE_GANG_OPS_IDLE_INTERACTION(sClientData, FALSE)
			PLAY_SOUND_FROM_COORD(-1, "Back", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			DO_INTERACT_ANIM()
			
			BREAK
			
		CASE GOCT_A
		
			PRIVATE_SELECT_GANG_OPS_STRAND(sClientData)
		
			BREAK
			
		CASE GOCT_X
		
			BREAK
	ENDSWITCH

ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_IDLE_WARNING_SCREENS(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	
	IF sClientData.sIdleData.iWarningScreen = -1
		EXIT
	ENDIF
	
	INT iCashDifference
	BOOL bConfirm
	INT iSelection = PRIVATE_GET_SELECTED_STRAND_FROM_SELECTION_ID(sClientData.sCursorData.iHighlight)
	
	IF NETWORK_CAN_SPEND_MONEY2(g_GangOpsIdle.sIdleData.iStrandCost[iSelection], FALSE, TRUE, FALSE, iCashDifference)
		IF PUBLIC_GANG_OPS_MAINTAIN_WARNING_SCREEN(sClientData.sIdleData.iWarningScreen, bConfirm, GANG_OPS_MISSION_INVALID, GET_GANG_OPS_MISSION_STRAND_FROM_CONST(g_GangOpsIdle.sIdleData.iStrandId[iSelection]), g_GangOpsIdle.sIdleData.iStrandCost[iSelection])
			IF bConfirm
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] - PRIVATE_UPDATE_GANG_OPS_IDLE_WARNING_SCREENS -  Player has triggered context to start the ", GET_GANG_OPS_MISSION_STRAND_NAME_FOR_DEBUG(GET_GANG_OPS_MISSION_STRAND_FROM_CONST(g_GangOpsIdle.sIdleData.iStrandId[iSelection])), " strand!")
				SET_BIT(sClientData.iIdleBitSet, GANG_OPS_BITSET_START_STRAND)
			ENDIF
			
			PRIVATE_SET_GANG_OPS_IDLE_NAVIGATION(sClientData, TRUE)
			sClientData.sIdleData.iWarningScreen = -1
			sClientData.sIdleData.iWarningScreenBuffer = 0
		ENDIF
	ELSE
		IF PUBLIC_GANG_OPS_MAINTAIN_WARNING_SCREEN(GANG_OPS_WARNING_SCREEN_NOT_ENOUGH_CASH_FOR_HEIST, bConfirm, GANG_OPS_MISSION_INVALID, GET_GANG_OPS_MISSION_STRAND_FROM_CONST(g_GangOpsIdle.sIdleData.iStrandId[iSelection]), g_GangOpsIdle.sIdleData.iStrandCost[iSelection])
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_IDLE_WARNING_SCREENS - Not enough cash warning done")
			PRIVATE_SET_GANG_OPS_IDLE_NAVIGATION(sClientData, TRUE)
			sClientData.sIdleData.iWarningScreen = -1
			sClientData.sIdleData.iWarningScreenBuffer = 0
		ENDIF
	ENDIF
	
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_IDLE_START_STRAND(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	IF IS_BIT_SET(sClientData.iIdleBitSet, GANG_OPS_BITSET_START_STRAND)
		INT iMacAddr
		INT iPosixTime
		IF PLAYSTATS_CREATE_MATCH_HISTORY_ID_2(iMacAddr, iPosixTime)
			SET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST2_SESSION_ID_MACADDR, iMacAddr)	
			SET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST2_SESSION_ID_POSTIME, iPosixTime)
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_IDLE_START_STRAND - MP_STAT_HEIST2_SESSION_ID_MACADDR = ",iMacAddr, ", MP_STAT_HEIST2_SESSION_ID_POSTIME = ", iPosixTime)
			
			INT iSelection = PRIVATE_GET_SELECTED_STRAND_FROM_SELECTION_ID(sClientData.sCursorData.iHighlight)
			
			IF USE_SERVER_TRANSACTIONS()
				INT iTransaction
				TRIGGER_FIRE_AND_FORGET_SERVICE_TRANSACTION(SERVICE_SPEND_GANGOPS_START_STRAND, g_GangOpsIdle.sIdleData.iStrandCost[iSelection], iTransaction, FALSE, TRUE)
				g_cashTransactionData[iTransaction].cashInfo.iNumCrates = ENUM_TO_INT(GET_GANG_OPS_MISSION_STRAND_FROM_CONST(g_GangOpsIdle.sIdleData.iStrandId[iSelection]))
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GTA$] PRIVATE_UPDATE_GANG_OPS_IDLE_START_STRAND - Strand started - SERVICE_SPEND_GANGOPS_START_STRAND, $", g_GangOpsIdle.sIdleData.iStrandCost[iSelection])
			ELSE
				NETWORK_SPEND_GANGOPS_START_STRAND(ENUM_TO_INT(GET_GANG_OPS_MISSION_STRAND_FROM_CONST(g_GangOpsIdle.sIdleData.iStrandId[iSelection])), g_GangOpsIdle.sIdleData.iStrandCost[iSelection], FALSE, TRUE)
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [GTA$] PRIVATE_UPDATE_GANG_OPS_IDLE_START_STRAND - Strand started - NETWORK_SPEND_GANGOPS_START_STRAND, $", g_GangOpsIdle.sIdleData.iStrandCost[iSelection])
			ENDIF
			
			PLAY_SOUND_FROM_COORD(-1, "Pay", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			
			// Clear all of the mission completion data for the strand we've just selected
			CANCEL_CURRENT_GANG_OPS_HEIST(FALSE, GET_GANG_OPS_MISSION_STRAND_FROM_CONST(g_GangOpsIdle.sIdleData.iStrandId[iSelection]))
			
			// Then set the strand as active
			SET_CURRENT_GANG_OPS_MISSION_STRAND(GET_GANG_OPS_MISSION_STRAND_FROM_CONST(g_GangOpsIdle.sIdleData.iStrandId[iSelection]))
			
			IF GET_GANG_OPS_MISSION_STRAND_FROM_CONST(g_GangOpsIdle.sIdleData.iStrandId[iSelection]) = GANG_OPS_MISSION_STRAND_SUBMARINE
				IF IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet5, BI_FM_GANG_BOSS_HELP_5_DO_SUBMARINE_READY_REMINDER_TEXT)
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_IDLE_START_STRAND - CLEAR_BIT(MPGlobalsAmbience.iFmGbHelpBitSet5, BI_FM_GANG_BOSS_HELP_5_DO_SUBMARINE_READY_REMINDER_TEXT)")
					CLEAR_BIT(MPGlobalsAmbience.iFmGbHelpBitSet5, BI_FM_GANG_BOSS_HELP_5_DO_SUBMARINE_READY_REMINDER_TEXT)
				ENDIF
			ELIF GET_GANG_OPS_MISSION_STRAND_FROM_CONST(g_GangOpsIdle.sIdleData.iStrandId[iSelection]) = GANG_OPS_MISSION_STRAND_MISSILE_SILO
				IF IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet5, BI_FM_GANG_BOSS_HELP_5_DO_MISSILE_SILO_READY_REMINDER_TEXT)
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_IDLE_START_STRAND - CLEAR_BIT(MPGlobalsAmbience.iFmGbHelpBitSet5, BI_FM_GANG_BOSS_HELP_5_DO_MISSILE_SILO_READY_REMINDER_TEXT)")
					CLEAR_BIT(MPGlobalsAmbience.iFmGbHelpBitSet5, BI_FM_GANG_BOSS_HELP_5_DO_MISSILE_SILO_READY_REMINDER_TEXT)
				ENDIF
				
				IF IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet6, BI_FM_GANG_BOSS_HELP_6_DO_SILO_COMPLETE_CALL)
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_IDLE_START_STRAND - CLEAR BI_FM_GANG_BOSS_HELP_6_DO_SILO_COMPLETE_CALL")
					CLEAR_BIT(MPGlobalsAmbience.iFmGbHelpBitSet6, BI_FM_GANG_BOSS_HELP_6_DO_SILO_COMPLETE_CALL)
				ENDIF
				
				IF IS_BIT_SET(MPGlobalsAmbience.iFmGbHelpBitSet6, BI_FM_GANG_BOSS_HELP_6_SILO_COMPLETE_CALL_DONE)
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_IDLE_START_STRAND - CLEAR BI_FM_GANG_BOSS_HELP_6_SILO_COMPLETE_CALL_DONE")
					CLEAR_BIT(MPGlobalsAmbience.iFmGbHelpBitSet6, BI_FM_GANG_BOSS_HELP_6_SILO_COMPLETE_CALL_DONE)
				ENDIF
			ENDIF
			
			//Check the "Getting started" achievement
			IF DOES_PLAYER_OWN_A_DEFUNCT_BASE(PLAYER_ID()) AND NOT HAS_ACHIEVEMENT_BEEN_AWARDED(ACHGO1)
				SET_GANGOPS_ACHIEVEMENT_DONE(GOPS_ACH_1_DONE)
			ENDIF
			
			IF sClientData.sIdleData.bShowReplayBoard
				INT iStatInt = GET_MP_INT_CHARACTER_STAT(MP_STAT_GANGOPS_HEIST_STATUS)
				IF NOT IS_BIT_SET(iStatInt, GANG_OPS_BD_HEIST_STATUS_BITSET_USED_REPLAY_BOARD_TO_START_HEIST)
					SET_BIT(iStatInt, GANG_OPS_BD_HEIST_STATUS_BITSET_USED_REPLAY_BOARD_TO_START_HEIST)
					SET_MP_INT_CHARACTER_STAT(MP_STAT_GANGOPS_HEIST_STATUS, iStatInt)
					CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_IDLE_START_STRAND - SET_BIT(iStatInt, GANG_OPS_BD_HEIST_STATUS_BITSET_USED_REPLAY_BOARD_TO_START_HEIST)")
				ENDIF
				g_bReplayGangOpsCutscene = TRUE
			ENDIF
			
			#IF FEATURE_GEN9_EXCLUSIVE
			RESUME_DOOMSDAY_HEIST_UDS_ACTIVITY(DEFAULT, TRUE, GET_GANG_OPS_MISSION_STRAND_FROM_CONST(g_GangOpsIdle.sIdleData.iStrandId[iSelection])) 
			#ENDIF
			
			SET_BIT(sClientData.iIdleBitSet, GANG_OPS_BITSET_SELECTED_HEIST)
			CLEAR_BIT(sClientData.iIdleBitSet, GANG_OPS_BITSET_START_STRAND)
		ELSE
			CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] PRIVATE_UPDATE_GANG_OPS_IDLE_START_STRAND - waiting for PLAYSTATS_CREATE_MATCH_HISTORY_ID_2")
		ENDIF
	ENDIF
ENDPROC

PROC PRIVATE_UPDATE_GANG_OPS_IDLE_BOARD(GANG_OPS_CLIENT_IDLE_DATA &sClientData)

	INT index, iStrand, iType, iMarker
	VECTOR vZoomCoords, vMarkerCoords
//	TEXT_LABEL_63 temp

	IF PUBLIC_IS_GANG_OPS_MAP_MARKERS_UPDATE_REQUIRED(sClientData.sScaleformData)
		iStrand = sClientData.sIdleData.iStrandId[PRIVATE_GET_SELECTED_STRAND_FROM_SELECTION_ID(sClientData.sCursorData.iHighlight)]
		
		vZoomCoords = <<350.0, 2000.0, 0.0>>
		
		PUBLIC_GANG_OPS_SET_MAP_DISPLAY(sClientData.sScaleformData.siMainBoard,
										CEIL(vZoomCoords.x),
										CEIL(vZoomCoords.y),
										0.0,
										FALSE)
										
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MAP_MARKER] PRIVATE_UPDATE_GANG_OPS_IDLE_BOARD - Setting up markers for ", GET_GANG_OPS_MISSION_STRAND_NAME_FOR_DEBUG(GET_GANG_OPS_MISSION_STRAND_FROM_CONST(iStrand)))
		
		PUBLIC_GANG_OPS_REMOVE_ALL_MAP_MARKERS(sClientData.sScaleformData.siMainBoard)
		
		FOR iMarker = 0 TO (MAX_GANG_OPS_MAP_MARKERS-1)
			vMarkerCoords = GET_GANG_OPS_STRAND_MAP_MARKER_COORDS(iStrand, iMarker)
			IF NOT IS_VECTOR_ZERO(vMarkerCoords)
				PUBLIC_GANG_OPS_ADD_MAP_MARKER(sClientData.sScaleformData.siMainBoard, iMarker, vMarkerCoords, iMarker)
				CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] [MAP_MARKER] PRIVATE_UPDATE_GANG_OPS_IDLE_BOARD - Adding marker ", iMarker, " at coords: ", vMarkerCoords)
			ENDIF
		ENDFOR
		
		PUBLIC_SET_GANG_OPS_MAP_MARKERS_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
	ENDIF

	IF PUBLIC_IS_GANG_OPS_FLAVOUR_IMAGES_UPDATE_REQUIRED(sClientData.sScaleformData)
				
		iStrand = sClientData.sIdleData.iStrandId[PRIVATE_GET_SELECTED_STRAND_FROM_SELECTION_ID(sClientData.sCursorData.iHighlight)]
		IF sClientData.sIdleData.bShowReplayBoard
			iType = GANG_OPS_FLAVOUR_IMAGE_TYPE_REPLAY
		ELSE
			iType = GANG_OPS_FLAVOUR_IMAGE_TYPE_SETUP
		ENDIF
		PUBLIC_GANG_OPS_UPDATE_FLAVOUR_IMAGES(	sClientData.sScaleformData.siMainBoard,
												GET_TEXTURE_DICT(iStrand),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_TOP, sClientData.sCursorData.iHighlight, iStrand, iType),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_1, sClientData.sCursorData.iHighlight, iStrand, iType),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_2, sClientData.sCursorData.iHighlight, iStrand, iType),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_3, sClientData.sCursorData.iHighlight, iStrand, iType),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_4, sClientData.sCursorData.iHighlight, iStrand, iType),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_LEFT_5, sClientData.sCursorData.iHighlight, iStrand, iType),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_RIGHT_1, sClientData.sCursorData.iHighlight, iStrand, iType),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_RIGHT_2, sClientData.sCursorData.iHighlight, iStrand, iType),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_MID_RIGHT_3, sClientData.sCursorData.iHighlight, iStrand, iType),
												GET_GANG_OPS_FLAVOUR_IMAGE(GANG_OPS_FLAVOUR_IMAGE_INDEX_BOTTOM, sClientData.sCursorData.iHighlight, iStrand, iType))
														
		PUBLIC_SET_GANG_OPS_FLAVOUR_IMAGES_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
	ENDIF
	
	IF sClientData.sIdleData.bShowReplayBoard
	
		IF PUBLIC_IS_GANG_OPS_MISSION_SLOTS_UPDATE_REQUIRED(sClientData.sScaleformData)
				
			FOR index = 0 TO (sClientData.sIdleData.iNumStrandsToShow-1)
				PUBLIC_GANG_OPS_UPDATE_IDLE_STRAND(	sClientData.sScaleformData.siMainBoard,
														index,
														sClientData.sIdleData.tlStrandName[index],
														sClientData.sIdleData.tlStrandDesc[index],
														sClientData.sIdleData.iStrandCost[index],
														sClientData.sIdleData.tlStrandImage[index],
														(sClientData.sIdleData.iStrandStatus[index] = GANG_OPS_MISSION_STATE_ON_COOLDOWN))
			ENDFOR
			
			PUBLIC_SET_GANG_OPS_MISSION_SLOTS_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
		ENDIF

		IF PUBLIC_IS_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData)
			
			PUBLIC_GANG_OPS_UPDATE_REPLAY_SCREEN(	sClientData.sScaleformData.siMainBoard, 
												sClientData.sScaleformData.bSkipLoading,
												sClientData.sIdleData.tlScreenName, 
												sClientData.sIdleData.piLeader)
			
			IF sClientData.sScaleformData.bSkipLoading
				PLAY_SOUND_FROM_COORD(-1, "Draw_Board", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ELSE
				PLAY_SOUND_FROM_COORD(-1, "Bootup", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ENDIF
			
			PUBLIC_SET_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
		ENDIF
		
	ELSE
	
		IF PUBLIC_IS_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData)
			
			PUBLIC_GANG_OPS_UPDATE_SETUP_SCREEN( sClientData.sScaleformData.siMainBoard, 
												sClientData.sScaleformData.bSkipLoading,
												sClientData.sIdleData.tlScreenName,
												sClientData.sIdleData.piLeader,
												sClientData.sIdleData.tlStrandName[0], 
												sClientData.sIdleData.tlStrandDesc[0],
												sClientData.sIdleData.iStrandCost[0],
												sClientData.sIdleData.tlStrandImage[0])
			
			IF sClientData.sScaleformData.bSkipLoading
				PLAY_SOUND_FROM_COORD(-1, "Draw_Board", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ELSE
				PLAY_SOUND_FROM_COORD(-1, "Bootup", GET_SCREEN_COORDS(), GET_GANG_OPS_PLANNING_SCREEN_SOUNDSET())
			ENDIF
			
			PUBLIC_SET_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData, FALSE)
		ENDIF
	
	ENDIF
	
ENDPROC

PROC PRIVATE_UNLOAD_GANG_OPS_IDLE_SCRIPT_RESOURCES(GANG_OPS_CLIENT_IDLE_DATA &sClientData)

	RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_PLANNING_BOARD")
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sClientData.sScaleformData.siMainBoard)
		
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Cleanup - Released all script assets and finished cleanup.")

ENDPROC


/* ===================================================================================== *\
			STATE MACHINE - STATE METHODS AND MAIN UPDATE
\* ===================================================================================== */

PROC PRIVATE_GANG_OPS_IDLE_IDLE_UPDATE(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
ENDPROC



PROC PRIVATE_GANG_OPS_IDLE_INIT_ENTRY(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	UNUSED_PARAMETER(sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - INIT ENTRY")
	IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
		CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
	ENDIF
ENDPROC

PROC PRIVATE_GANG_OPS_IDLE_INIT_UPDATE(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
ENDPROC

PROC PRIVATE_GANG_OPS_IDLE_INIT_EXIT(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - INIT EXIT")
//	PUBLIC_SET_GANG_OPS_HIGHLIGHTER_STATE(sClientData.sCursorData)
	PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
ENDPROC



PROC PRIVATE_GANG_OPS_IDLE_LOADING_ENTRY(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - LOADING ENTRY")
	PRIVATE_REQUEST_GANG_OPS_IDLE_SCALEFORM(sClientData.sScaleformData)
	IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
		CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
	ENDIF
ENDPROC

PROC PRIVATE_GANG_OPS_IDLE_LOADING_UPDATE(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	
	BOOL bReady = TRUE
	
	IF NOT PUBLIC_HAVE_GANG_OPS_ASSETS_LOADED(sClientData.sScaleformData)
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Request Assets - Waiting for assets to load.")
		bReady = FALSE
	ENDIF
		
	IF NOT GET_FM_UGC_INITIAL_HAS_FINISHED()
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Request Assets - Waiting for UGC initial data to load.")
		bReady = FALSE
	ENDIF
	
	IF NOT PUBLIC_HAS_GANG_OPS_AUDIO_LOADED()
		CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] Request Assets - Waiting for audio assets to load.")
		bReady = FALSE
	ENDIF
	
	IF bReady
		PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
	ENDIF
	
ENDPROC

PROC PRIVATE_GANG_OPS_IDLE_LOADING_EXIT(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - LOADING EXIT")
	PRIVATE_RENDER_GANG_OPS_IDLE_SCALEFORM(sClientData)
	PUBLIC_SET_GANG_OPS_SCALEFORM_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
	PUBLIC_SET_GANG_OPS_FLAVOUR_IMAGES_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
	PUBLIC_SET_GANG_OPS_MAP_MARKERS_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
	PUBLIC_SET_GANG_OPS_MISSION_SLOTS_UPDATE_REQUIRED(sClientData.sScaleformData, TRUE)
	sClientData.sCursorData.iHighlight = GANG_OPS_PLANNING_SELECTION_ID_MISSION_1_PREP_MISSION_1
	GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iHighlight = sClientData.sCursorData.iHighlight
ENDPROC



PROC PRIVATE_GANG_OPS_IDLE_DISPLAYING_ENTRY(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - DISPLAYING ENTRY")
	UNUSED_PARAMETER(sClientData)
	IF NOT IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
		SET_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
	ENDIF
ENDPROC

PROC PRIVATE_GANG_OPS_IDLE_DISPLAYING_UPDATE(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	
	PRIVATE_UPDATE_GANG_OPS_IDLE_NET_DATA(sClientData)
	PRIVATE_UPDATE_GANG_OPS_IDLE_BOARD(sClientData)
	PRIVATE_RENDER_GANG_OPS_IDLE_SCALEFORM(sClientData)
	
	IF sClientData.sStateData.bInteract
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
	ENDIF
	
ENDPROC

PROC PRIVATE_GANG_OPS_IDLE_DISPLAYING_EXIT(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - DISPLAYING EXIT")
	UNUSED_PARAMETER(sClientData)
ENDPROC



PROC PRIVATE_GANG_OPS_IDLE_INUSE_ENTRY(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - INUSE ENTRY")
	
	PRIVATE_INIT_GANG_OPS_IDLE_CAMERA(sClientData)
//	PUBLIC_SET_GANG_OPS_HIGHLIGHTER_STATE(sClientData.sCursorData, GANG_OPS_HIGHLIGHT_PLANNING_START_INDEX)
	
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] INUSE ENTRY - Setting camera focus to: GOCAM_MAIN")
	sClientData.sCameraData.ePositionId = GOCAM_MAIN
	sClientData.sCameraData.iPositionId = ENUM_TO_INT(GOCAM_MAIN)
	
	sClientData.sScaleformData.bDrawInstructions = TRUE
	PUBLIC_SET_GANG_OPS_DO_INSTR_BTN_UPDATE(sClientData.sScaleformData, TRUE)
	
	CLEAR_ALL_BIG_MESSAGES()
	
	PUBLIC_GANG_OPS_SET_CURRENT_SELECTION(sClientData.sScaleformData.siMainBoard, GANG_OPS_PLANNING_SELECTION_ID_MISSION_1_PREP_MISSION_1)
	
	PRIVATE_SET_GANG_OPS_IDLE_NAVIGATION(sClientData, TRUE)
	
	PUBLIC_GANG_OPS_FLASH_ACTIVE_ELEMENT(sClientData.sScaleformData.siMainBoard)

ENDPROC

PROC PRIVATE_GANG_OPS_IDLE_INUSE_UPDATE(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	
	PUBLIC_MAINTAIN_EXCLUSIVE_INPUT()
	PRIVATE_UPDATE_GANG_OPS_IDLE_TUTORIAL(sClientData)
	PRIVATE_UPDATE_GANG_OPS_IDLE_NET_DATA(sClientData)
	PRIVATE_UPDATE_GANG_OPS_IDLE_HIGHLIGHT(sClientData)
	PRIVATE_UPDATE_GANG_OPS_IDLE_BOARD(sClientData)
	PRIVATE_RENDER_GANG_OPS_IDLE_SCALEFORM(sClientData)
	PRIVATE_UPDATE_GANG_OPS_IDLE_WARNING_SCREENS(sClientData)
	PRIVATE_UPDATE_GANG_OPS_IDLE_INPUTS(sClientData)
	PRIVATE_UPDATE_GANG_OPS_IDLE_START_STRAND(sClientData)
	PUBLIC_UPDATE_GANG_OPS_CAMERA(sClientData.sCameraData)//, sClientData.sPositionData)
	PUBLIC_GANG_OPS_HIDE_HUD()
//	MAINTAIN_INTERACT_ANIM(sClientData.sInputData)
	sClientData.sIdleData.iWarningScreenBuffer = -1
	
	IF NOT sClientData.sStateData.bInteract
		SET_BIT(sClientData.iIdleBitSet, GANG_OPS_BITSET_SUPPRESS_HELP_SOUND)
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		PRIVATE_CLEAR_GANG_OPS_IDLE_CAMERA(sClientData)
		PUBLIC_REGRESS_GANG_OPS_FSM(sClientData.sStateData)
	ENDIF
	
ENDPROC

PROC PRIVATE_GANG_OPS_IDLE_INUSE_EXIT(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - INUSE EXIT")
	
//	PUBLIC_SET_GANG_OPS_HIGHLIGHTER_STATE(sClientData.sCursorData)
	sClientData.sScaleformData.bDrawInstructions = FALSE
	PRIVATE_SET_GANG_OPS_IDLE_NAVIGATION(sClientData, FALSE)
	
ENDPROC

PROC PRIVATE_GANG_OPS_IDLE_CLEANUP_ENTRY(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	CDEBUG1LN(DEBUG_GANG_OPS, "[GANGOPS_FLOW] FSM - CLEANUP ENTRY")
	
	IF IS_BIT_SET(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
		CLEAR_BIT(GlobalplayerBD_FM_2[NETWORK_PLAYER_ID_TO_INT()].GangOps.iGeneralBitSet, GANG_OPS_BD_GENERAL_BITSET_BOARD_IN_DISPLAYING_STATE)
	ENDIF
	
	PUBLIC_GANG_OPS_DRAW_RECTANGLE(sClientData.sScaleformData)
ENDPROC

PROC PRIVATE_GANG_OPS_IDLE_CLEANUP_UPDATE(GANG_OPS_CLIENT_IDLE_DATA &sClientData)

	PUBLIC_GANG_OPS_DRAW_RECTANGLE(sClientData.sScaleformData)
	PUBLIC_CLEAN_GANG_OPS_SCALEFORM_CACHE()
	
	PRIVATE_CLEAR_GANG_OPS_IDLE_CAMERA(sClientData)
	PUBLIC_PROGRESS_GANG_OPS_FSM(sClientData.sStateData)
	IF IS_SKYSWOOP_AT_GROUND()
	AND NOT IS_PLAYER_TELEPORT_ACTIVE()
	AND NOT IS_PLAYER_IN_CORONA()
	AND IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
	AND NOT IS_NEW_LOAD_SCENE_ACTIVE() 
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
ENDPROC


PROC PRIVATE_GANG_OPS_IDLE_UPDATE(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	
	SWITCH (sClientData.sStateData.eState)
	
		CASE GOBS_IDLE
		
			PRIVATE_GANG_OPS_IDLE_IDLE_UPDATE(sClientData)
		
		BREAK
		
		CASE GOBS_INIT
		
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_ENTRY(sClientData.sStateData)
				PRIVATE_GANG_OPS_IDLE_INIT_ENTRY(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_ENTRY(sClientData.sStateData)
			ENDIF
			
			PRIVATE_GANG_OPS_IDLE_INIT_UPDATE(sClientData)
			
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_EXIT(sClientData.sStateData)
				PRIVATE_GANG_OPS_IDLE_INIT_EXIT(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_EXIT(sClientData.sStateData)
			ENDIF
			
		BREAK
		
		CASE GOBS_LOADING
		
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_ENTRY(sClientData.sStateData)
				PRIVATE_GANG_OPS_IDLE_LOADING_ENTRY(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_ENTRY(sClientData.sStateData)
			ENDIF
			
			PRIVATE_GANG_OPS_IDLE_LOADING_UPDATE(sClientData)
			
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_EXIT(sClientData.sStateData)
				PRIVATE_GANG_OPS_IDLE_LOADING_EXIT(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_EXIT(sClientData.sStateData)
			ENDIF
		
		BREAK
		
		CASE GOBS_DISPLAYING
		
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_ENTRY(sClientData.sStateData)
				PRIVATE_GANG_OPS_IDLE_DISPLAYING_ENTRY(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_ENTRY(sClientData.sStateData)
			ENDIF
			
			PRIVATE_GANG_OPS_IDLE_DISPLAYING_UPDATE(sClientData)
			
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_EXIT(sClientData.sStateData)
				PRIVATE_GANG_OPS_IDLE_DISPLAYING_EXIT(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_EXIT(sClientData.sStateData)
			ENDIF
		
		BREAK
		
		CASE GOBS_INUSE
		
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_ENTRY(sClientData.sStateData)
				PRIVATE_GANG_OPS_IDLE_INUSE_ENTRY(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_ENTRY(sClientData.sStateData)
			ENDIF
			
			PRIVATE_GANG_OPS_IDLE_INUSE_UPDATE(sClientData)
			
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_EXIT(sClientData.sStateData)
				PRIVATE_GANG_OPS_IDLE_INUSE_EXIT(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_EXIT(sClientData.sStateData)
			ENDIF
		
		BREAK
		
		CASE GOBS_CLEANUP
		
			IF NOT PUBLIC_GANG_OPS_HAS_RUN_ENTRY(sClientData.sStateData)
				PRIVATE_GANG_OPS_IDLE_CLEANUP_ENTRY(sClientData)
				PUBLIC_GANG_OPS_SET_COMPLETED_ENTRY(sClientData.sStateData)
			ENDIF
			
			PRIVATE_GANG_OPS_IDLE_CLEANUP_UPDATE(sClientData)
			
		BREAK
		
		CASE GOBS_FINISHED
			PRIVATE_UNLOAD_GANG_OPS_IDLE_SCRIPT_RESOURCES(sClientData)
		BREAK
	
	ENDSWITCH
	
ENDPROC

PROC MAINTAIN_GANG_OPS_IDLE(GANG_OPS_CLIENT_IDLE_DATA &sClientData)
	
	#IF IS_DEBUG_BUILD
	PRIVATE_GANG_OPS_IDLE_WIDGET_UPDATE(sClientData)
	#ENDIF
	
	PRIVATE_GANG_OPS_IDLE_UPDATE(sClientData)
	
ENDPROC


//EOF
