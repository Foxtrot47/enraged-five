USING "rage_builtins.sch"
USING "globals.sch"

USING "net_stat_system.sch"
USING "net_scoring_common.sch"
USING "net_comms_public.sch"
USING "screens_header.sch"
USING "Transition_Saving.sch"
USING "fmmc_cloud_loader.sch"

#IF IS_DEBUG_BUILD
USING "profiler.sch"
#ENDIF


CONST_INT SCRIPT_MAX_INT32       2147483647

CONST_INT THRESHOLD_FOR_LARGE_INT_BADSPORT 10000000


FUNC BOOL IS_PLAYER_A_CHEATER(PLAYER_INDEX aPlayer)
	INT iPlayerID = NATIVE_TO_INT(aPlayer)
	IF iPlayerID > -1
		RETURN IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_CHEATER)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_A_BADSPORT(PLAYER_INDEX aPlayer)
	INT iPlayerID = NATIVE_TO_INT(aPlayer)
	IF iPlayerID > -1
		RETURN IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_BADSPORT)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_AUTOMUTED(PLAYER_INDEX aPlayer)
	INT iPlayerID = NATIVE_TO_INT(aPlayer)
	IF iPlayerID > -1
		RETURN IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_AUTOMUTED)
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_AUTOMUTED_BADSPORT(PLAYER_INDEX aPlayer)
	INT iPlayerID = NATIVE_TO_INT(aPlayer)
	IF iPlayerID > -1
		RETURN IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_AUTOMUTED_BADSPORT)
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC BOOL IGNORE_LAST_DISCREPENCY()

	IF NETWORK_CHECK_ROS_LINK_WENTDOWN_NOT_NET()
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Takes an int and returns a MONTH_OF_YEAR
/// PARAMS:
///    Month - 1- 12 params
/// RETURNS:
///    
FUNC MONTH_OF_YEAR GET_MONTH_OF_YEAR_FROM_INT(INT Month)

	IF Month < 1
		#IF IS_DEBUG_BUILD
		SCRIPT_ASSERT("GET_MONTH_OF_YEAR_FROM_INT - Month < 1 error. ")
		#ENDIF
		RETURN JANUARY
	ENDIF
	IF Month > 12
		#IF IS_DEBUG_BUILD
		SCRIPT_ASSERT("GET_MONTH_OF_YEAR_FROM_INT - Month > 12 error. ")
		#ENDIF
		RETURN JANUARY
	ENDIF

	SWITCH Month
		CASE 1			RETURN JANUARY			BREAK	
		CASE 2			RETURN FEBRUARY			BREAK	
		CASE 3			RETURN MARCH			BREAK	
		CASE 4			RETURN APRIL			BREAK	
		CASE 5			RETURN MAY				BREAK	
		CASE 6			RETURN JUNE				BREAK	
		CASE 7			RETURN JULY				BREAK	
		CASE 8			RETURN AUGUST			BREAK	
		CASE 9			RETURN SEPTEMBER		BREAK	
		CASE 10			RETURN OCTOBER			BREAK	
		CASE 11			RETURN NOVEMBER			BREAK	
		CASE 12			RETURN DECEMBER			BREAK	
	
	ENDSWITCH
	RETURN JANUARY

ENDFUNC

///    Takes an int and returns a MONTH_OF_YEAR
/// PARAMS:
///    Month - 1- 12 params
/// RETURNS:
///    
FUNC INT GET_MONTH_INT_FROM_MONTH_OF_YEAR(MONTH_OF_YEAR Month)


	SWITCH Month
		CASE JANUARY	RETURN 1					BREAK	
		CASE FEBRUARY	RETURN 2					BREAK	
		CASE MARCH		RETURN 3				BREAK	
		CASE APRIL		RETURN 4				BREAK	
		CASE MAY		RETURN 5					BREAK	
		CASE JUNE		RETURN 6					BREAK	
		CASE JULY		RETURN 7					BREAK	
		CASE AUGUST		RETURN 8				BREAK	
		CASE SEPTEMBER	RETURN 9				BREAK	
		CASE OCTOBER	RETURN 10					BREAK	
		CASE NOVEMBER	RETURN 11					BREAK	
		CASE DECEMBER	RETURN 12					BREAK	
	
	ENDSWITCH
	RETURN 1

ENDFUNC



FUNC STRUCT_STAT_DATE GET_CURRENT_TIMEOFDAY_AS_STAT()
	STRUCT_STAT_DATE Result
	TIMEOFDAY aTime =  GET_CURRENT_TIMEOFDAY_POSIX()
	
	Result.Year = GET_TIMEOFDAY_YEAR(aTime)
	Result.Month = GET_MONTH_INT_FROM_MONTH_OF_YEAR(GET_TIMEOFDAY_MONTH(aTime))
	Result.Day = GET_TIMEOFDAY_DAY(aTime)
	Result.Hour = GET_TIMEOFDAY_HOUR(aTime)
	Result.Minute = GET_TIMEOFDAY_MINUTE(aTime)
	Result.Seconds = GET_TIMEOFDAY_SECOND(aTime)
	
	RETURN Result
ENDFUNC


FUNC STRUCT_STAT_DATE CONVERT_TIMEOFDAY_TO_STATDATE(TIMEOFDAY aTime)
	STRUCT_STAT_DATE Result
	
	Result.Year = GET_TIMEOFDAY_YEAR(aTime)
	Result.Month = GET_MONTH_INT_FROM_MONTH_OF_YEAR(GET_TIMEOFDAY_MONTH(aTime))
	Result.Day = GET_TIMEOFDAY_DAY(aTime)
	Result.Hour = GET_TIMEOFDAY_HOUR(aTime)
	Result.Minute = GET_TIMEOFDAY_MINUTE(aTime)
	Result.Seconds = GET_TIMEOFDAY_SECOND(aTime)
	
	RETURN Result
ENDFUNC



FUNC TIMEOFDAY CONVERT_STATDATE_TO_TIMEOFDAY(STRUCT_STAT_DATE aStatDate )
	
	TIMEOFDAY Result  
	
	
	IF aStatDate.Year > 0
		SET_TIMEOFDAY_YEAR(Result, aStatDate.Year )
	ENDIF
	IF aStatDate.Month > 0
		SET_TIMEOFDAY_MONTH(Result, GET_MONTH_OF_YEAR_FROM_INT(aStatDate.Month) )
	ENDIF
	IF aStatDate.Day > 0
		SET_TIMEOFDAY_DAY(Result, aStatDate.Day )
	ENDIF
	IF aStatDate.Hour > 0
		SET_TIMEOFDAY_HOUR(Result, aStatDate.Hour )
	ENDIF
	IF aStatDate.Minute > 0
		SET_TIMEOFDAY_MINUTE(Result, aStatDate.Minute )
	ENDIF
	IF aStatDate.Seconds > 0
		SET_TIMEOFDAY_SECOND(Result, aStatDate.Seconds )
	ENDIF
	RETURN Result

ENDFUNC

FUNC TIMEOFDAY GET_OFFSET_TIMEOFDAY_IN_HOURS(STRUCT_STAT_DATE FirstDate, INT NumHours)
	TIMEOFDAY TOD_First = CONVERT_STATDATE_TO_TIMEOFDAY(FirstDate)
	
	TIMEOFDAY TOD_CutOff = TOD_First
	ADD_TIME_TO_TIMEOFDAY(TOD_CutOff, 0, 0, NumHours, 0, 0, 0)

	RETURN TOD_CutOff

ENDFUNC


FUNC TIMEOFDAY GET_OFFSET_TIMEOFDAY_IN_MINS(STRUCT_STAT_DATE FirstDate, INT NumMins)
	TIMEOFDAY TOD_First = CONVERT_STATDATE_TO_TIMEOFDAY(FirstDate)
	
	TIMEOFDAY TOD_CutOff = TOD_First
	ADD_TIME_TO_TIMEOFDAY(TOD_CutOff, 0, NumMins, 0, 0, 0, 0)

	RETURN TOD_CutOff

ENDFUNC


FUNC BOOL HAS_HOURS_PASSED_BETWEEN_DATES(STRUCT_STAT_DATE FirstDate, STRUCT_STAT_DATE LaterDate, INT NumHours)

	TIMEOFDAY TOD_Later = CONVERT_STATDATE_TO_TIMEOFDAY(LaterDate)
	TIMEOFDAY TOD_CutOff = GET_OFFSET_TIMEOFDAY_IN_HOURS(FirstDate, NumHours)

	IF IS_TIMEOFDAY_AFTER_TIMEOFDAY(TOD_Later,TOD_CutOff ) = TRUE
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL HAS_MINS_PASSED_BETWEEN_DATES(STRUCT_STAT_DATE FirstDate, STRUCT_STAT_DATE LaterDate, INT NumMins)

	TIMEOFDAY TOD_Later = CONVERT_STATDATE_TO_TIMEOFDAY(LaterDate)
	TIMEOFDAY TOD_CutOff = GET_OFFSET_TIMEOFDAY_IN_MINS(FirstDate, NumMins)

	IF IS_TIMEOFDAY_AFTER_TIMEOFDAY(TOD_Later,TOD_CutOff ) = TRUE
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC TIMEOFDAY GET_OFFSET_TIMEOFDAY_IN_DAYS(STRUCT_STAT_DATE FirstDate,  INT NumDays)

	IF IS_DATE_EMPTY(FirstDate) = FALSE
		TIMEOFDAY TOD_First = CONVERT_STATDATE_TO_TIMEOFDAY(FirstDate)
		
		TIMEOFDAY TOD_CutOff = TOD_First
		ADD_TIME_TO_TIMEOFDAY(TOD_CutOff, 0, 0, 0, NumDays, 0, 0)
		
//		NET_NL()NET_PRINT("GET_OFFSET_TIMEOFDAY_IN_DAYS: FirstDate.Year = ")NET_PRINT_INT(FirstDate.YEAR)
//		NET_NL()NET_PRINT("GET_OFFSET_TIMEOFDAY_IN_DAYS: FirstDate.Month = ")NET_PRINT_INT(FirstDate.Month)
//		NET_NL()NET_PRINT("GET_OFFSET_TIMEOFDAY_IN_DAYS: FirstDate.Day = ")NET_PRINT_INT(FirstDate.Day)
//		NET_NL()NET_PRINT("GET_OFFSET_TIMEOFDAY_IN_DAYS: FirstDate.Hour = ")NET_PRINT_INT(FirstDate.Hour)
//		NET_NL()NET_PRINT("GET_OFFSET_TIMEOFDAY_IN_DAYS: FirstDate.Minute = ")NET_PRINT_INT(FirstDate.Minute)
//		NET_NL()NET_PRINT("GET_OFFSET_TIMEOFDAY_IN_DAYS: FirstDate.Seconds = ")NET_PRINT_INT(FirstDate.Seconds)
//
//		TEXT_LABEL_63 TlCutoff 
//		GET_TIMEOFDAY_STRING(TOD_CutOff, TlCutoff, TRUE, TRUE)
//		NET_NL()NET_PRINT(" GET_OFFSET_TIMEOFDAY_IN_DAYS TOD_CutOff = ")NET_PRINT(TlCutoff)
//		TEXT_LABEL_63 TlFirst 
//		GET_TIMEOFDAY_STRING(TOD_First, TlFirst, TRUE, TRUE)
//		NET_NL()NET_PRINT(" GET_OFFSET_TIMEOFDAY_IN_DAYS TOD_First = ")NET_PRINT(TlFirst)

		
		IF NumDays = 0 
			TOD_CutOff = TOD_First
		ENDIF
		RETURN TOD_CutOff
	ENDIF
	
//	NET_NL()NET_PRINT("GET_OFFSET_TIMEOFDAY_IN_DAYS: Returning failsafe ")

	TIMEOFDAY TOD_FailSafe
//	SET_TIMEOFDAY_YEAR(TOD_FailSafe, 0)
//	SET_TIMEOFDAY_MONTH(TOD_FailSafe, JANUARY)
//	SET_TIMEOFDAY_DAY(TOD_FailSafe, 0)
//	SET_TIMEOFDAY_HOUR(TOD_FailSafe, 0)
//	SET_TIMEOFDAY_MINUTE(TOD_FailSafe, 0)
//	SET_TIMEOFDAY_SECOND(TOD_FailSafe, 0)
	
	RETURN TOD_FailSafe
	

ENDFUNC

FUNC BOOL HAS_DAYS_PASSED_BETWEEN_DATES(STRUCT_STAT_DATE FirstDate, STRUCT_STAT_DATE LaterDate, INT NumDays)

	TIMEOFDAY TOD_Later = CONVERT_STATDATE_TO_TIMEOFDAY(LaterDate)
	TIMEOFDAY TOD_CutOff = GET_OFFSET_TIMEOFDAY_IN_DAYS(FirstDate, NumDays)
	
	#IF USE_TU_CHANGES
		IF TOD_CutOff = INVALID_TIMEOFDAY
			NET_NL()NET_PRINT("HAS_DAYS_PASSED_BETWEEN_DATES - returned INVALID_TIMEOFDAY - return false ")
			RETURN FALSE
		ENDIF
	#ENDIF
	
//	#IF IS_DEBUG_BUILD
//	TEXT_LABEL_63 TOD_Later_Str 
//	GET_TIMEOFDAY_STRING(TOD_Later,TOD_Later_Str, TRUE, TRUE )
//	TEXT_LABEL_63 TOD_CutOff_Str
//	GET_TIMEOFDAY_STRING(TOD_CutOff,TOD_CutOff_Str, TRUE, TRUE )
//	
//	PRINTSTRING("HAS_DAYS_PASSED_BETWEEN_DATES: ")
//	PRINTLN("			- TOD_Later = ", TOD_Later_Str)
//	PRINTLN("			- TOD_CutOff = ", TOD_CutOff_Str)
//	#ENDIF
	
	IF IS_TIMEOFDAY_AFTER_TIMEOFDAY(TOD_Later,TOD_CutOff ) = TRUE
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC


FUNC BOOL IS_OFFSET_CLASSED_AS_SMALL(STRUCT_STAT_DATE aStatOffset )
	
	IF aStatOffset.Year > 0
		RETURN FALSE
	ENDIF
	
	IF aStatOffset.Month > 0
		RETURN FALSE
	ENDIF
	

	
	RETURN TRUE

ENDFUNC


FUNC INT CONVERT_OFFSET_TO_MILLISECONDS(STRUCT_STAT_DATE aStatOffset )
	
	INT Result 

	IF IS_OFFSET_CLASSED_AS_SMALL(aStatOffset)
	
		
		IF (Result+86400000*aStatOffset.Day) > SCRIPT_MAX_INT32
			RETURN SCRIPT_MAX_INT32
		ELSE
			Result += 86400000*aStatOffset.Day
		ENDIF
		
		IF (Result+3600000*aStatOffset.Hour) > SCRIPT_MAX_INT32
			RETURN SCRIPT_MAX_INT32
		ELSE
			Result += 3600000*aStatOffset.Hour
		ENDIF
		
		IF (Result+60000*aStatOffset.Minute) > SCRIPT_MAX_INT32
			RETURN SCRIPT_MAX_INT32
		ELSE
			Result += 60000*aStatOffset.Minute
		ENDIF
		
		IF (Result+1000*aStatOffset.Seconds) > SCRIPT_MAX_INT32
			RETURN SCRIPT_MAX_INT32
		ELSE
			Result += 1000*aStatOffset.Seconds
		ENDIF
		
		IF (Result+1*aStatOffset.MilliSeconds) > SCRIPT_MAX_INT32
			RETURN SCRIPT_MAX_INT32
		ELSE
			Result += 1*aStatOffset.MilliSeconds
		ENDIF
	ENDIF
	
	RETURN Result

ENDFUNC


FUNC BOOL HAS_PLAYER_PLAYED_A_WEEK()

	STRUCT_STAT_DATE currentdate
	GET_POSIX_TIME(currentdate.Year, currentdate.Month, currentdate.Day, currentdate.Hour, currentdate.Minute, currentdate.Seconds)
		
	STRUCT_STAT_DATE Started_Stat_date
	Started_Stat_date = GET_MP_DATE_PLAYER_STAT(MPPLY_STARTED_MP)
	
//	NET_NL()NET_PRINT("currentdate.Year = ")NET_PRINT_INT(currentdate.Year)
//	NET_NL()NET_PRINT("currentdate.Month = ")NET_PRINT_INT(currentdate.Month)
//	NET_NL()NET_PRINT("currentdate.Day = ")NET_PRINT_INT(currentdate.Day)
//	NET_NL()NET_PRINT("currentdate.Hour = ")NET_PRINT_INT(currentdate.Hour)
//	NET_NL()NET_PRINT("currentdate.Minute = ")NET_PRINT_INT(currentdate.Minute)
//	NET_NL()NET_PRINT("currentdate.Seconds = ")NET_PRINT_INT(currentdate.Seconds)

	IF HAS_DAYS_PASSED_BETWEEN_DATES(Started_Stat_date, currentdate, 7)
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC


PROC RESET_THIS_MISSION_CHEATS()
	MPGlobals.g_Write_CheatTrackerBitset = 0
ENDPROC

PROC RESET_LAST_MISSION_CHEATS()
	MPGlobals.g_Read_CheatTrackerBitset = 0
ENDPROC

PROC RESET_ALL_CHEATS()
	
	RESET_THIS_MISSION_CHEATS()
	RESET_LAST_MISSION_CHEATS()

ENDPROC

FUNC BOOL HAS_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST aCheat)
	RETURN IS_BIT_SET(MPGlobals.g_Read_CheatTrackerBitset, ENUM_TO_INT(aCheat))
ENDFUNC

PROC SET_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST aCheat, BOOL didHappen)

	IF didHappen 
		IF NOT IS_BIT_SET(MPGlobals.g_Read_CheatTrackerBitset, ENUM_TO_INT(aCheat))
			SET_BIT(MPGlobals.g_Write_CheatTrackerBitset, ENUM_TO_INT(aCheat))
		ENDIF
	ELSE
		IF IS_BIT_SET(MPGlobals.g_Read_CheatTrackerBitset, ENUM_TO_INT(aCheat))
			CLEAR_BIT(MPGlobals.g_Write_CheatTrackerBitset, ENUM_TO_INT(aCheat))
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HAS_CHEAT_HAPPENED_LAST_MISSION(CHEAT_LIST aCheat)
	RETURN IS_BIT_SET(MPGlobals.g_Write_CheatTrackerBitset, ENUM_TO_INT(aCheat))
ENDFUNC

PROC SET_CHEAT_HAPPENED_LAST_MISSION(CHEAT_LIST aCheat, BOOL didHappen)

	IF didHappen 
		IF NOT IS_BIT_SET(MPGlobals.g_Read_CheatTrackerBitset, ENUM_TO_INT(aCheat))
			SET_BIT(MPGlobals.g_Write_CheatTrackerBitset, ENUM_TO_INT(aCheat))
		ENDIF
	ELSE
		IF IS_BIT_SET(MPGlobals.g_Read_CheatTrackerBitset, ENUM_TO_INT(aCheat))
			CLEAR_BIT(MPGlobals.g_Write_CheatTrackerBitset, ENUM_TO_INT(aCheat))
		ENDIF
	ENDIF
	
ENDPROC







/// PURPOSE:
///    Called before the start of mission stat upload. Tracks the cheat progress.
PROC CHEAT_TRACKER_START_OF_DEATHMATCH()
//	BOOL resetPlugPulled
	
	//Mission pulled plug
	MPGlobals.g_Read_CheatTrackerBitset = MPGlobals.g_Write_CheatTrackerBitset
//	
//	IF HAS_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_DM)
//		resetPlugPulled = TRUE
//	ENDIF
//	
//	SET_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_DM, FALSE)
//	
//	IF resetPlugPulled 
//		SET_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_DM, TRUE)
//	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHEAT_BITSET, MPGlobals.g_Read_CheatTrackerBitset)
	
ENDPROC

/// PURPOSE:
///    Called before the start of mission stat upload. Tracks the cheat progress.
PROC CHEAT_TRACKER_START_OF_RACE()
//	BOOL resetPlugPulled
	
	//Mission pulled plug
	MPGlobals.g_Read_CheatTrackerBitset = MPGlobals.g_Write_CheatTrackerBitset
	
//	IF HAS_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_RACE)
//		resetPlugPulled = TRUE
//	ENDIF
//	
//	SET_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_RACE, FALSE)
//	
//	IF resetPlugPulled 
//		SET_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_RACE, TRUE)
//	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHEAT_BITSET, MPGlobals.g_Read_CheatTrackerBitset)
	
ENDPROC

/// PURPOSE:
///    Called before the start of mission stat upload. Tracks the cheat progress.
PROC CHEAT_TRACKER_START_OF_MC_MISSION()
//	BOOL resetPlugPulled
	
	//Mission pulled plug
	MPGlobals.g_Read_CheatTrackerBitset = MPGlobals.g_Write_CheatTrackerBitset
	
//	IF HAS_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_MC_MISSION)
//		resetPlugPulled = TRUE
//	ENDIF
//	
//	SET_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_MC_MISSION, FALSE)
//	
//	IF resetPlugPulled 
//		SET_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_MC_MISSION, TRUE)
//	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_CHEAT_BITSET, MPGlobals.g_Read_CheatTrackerBitset)
	
ENDPROC


PROC DISPLAY_LOCAL_TICKER_FOR_BAD_SPORTER_ENTRY()
	IF HAS_IMPORTANT_STATS_LOADED()
		IF NOT IS_TRANSITION_ACTIVE()
			IF NETWORK_PLAYER_IS_BADSPORT()
				DISPLAY_HELP_TEXT_THIS_FRAME("HUD_BADSWOP", FALSE)
		//		BEGIN_TEXT_COMMAND_DISPLAY_HELP("HUD_BADSPTIME")
		//		END_TEXT_COMMAND_DISPLAY_HELP(TRUE)	
			ENDIF
		ENDIF
	ENDIF
ENDPROC



PROC DISPLAY_LOCAL_TICKER_FOR_CHEATER_ENTRY()
	IF HAS_IMPORTANT_STATS_LOADED()
		IF NOT IS_TRANSITION_ACTIVE()
			IF NETWORK_PLAYER_IS_CHEATER()
				DISPLAY_HELP_TEXT_THIS_FRAME("HUD_CHTSWOP", FALSE)
		//		BEGIN_TEXT_COMMAND_THEFEED_POST("HUD_BADSPTIME")
		//		END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)		
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC DISPLAY_LOCAL_TICKER_FOR_BAD_SPORTER_IMMEDIATE()
	IF HAS_IMPORTANT_STATS_LOADED()
		IF NETWORK_PLAYER_IS_BADSPORT()
			BEGIN_TEXT_COMMAND_THEFEED_POST("BAIL_BADSPORT")
			END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)	
		ENDIF
	ENDIF
ENDPROC



PROC DISPLAY_LOCAL_TICKER_FOR_CHEATER_IMMEDIATE()
	IF HAS_IMPORTANT_STATS_LOADED()
		IF NETWORK_PLAYER_IS_CHEATER()
			BEGIN_TEXT_COMMAND_THEFEED_POST("BAIL_CHEAT")
			END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)		
		ENDIF
	ENDIF
ENDPROC















///////////////////////////////////////
///    Individual Cheats	///////////
///////////////////////////////////////



FUNC TEXT_LABEL_63 GET_PUNISH_REASON_CHEAT(CHEAT_LIST WhichCheat)

	TEXT_LABEL_63 Result 
		SWITCH WhichCheat
			CASE CHEAT_LIST_NONE
				Result = ""	
			BREAK
		ENDSWITCH
	
	
	RETURN Result
ENDFUNC


FUNC BOOL HAS_PLAYER_CHEATED(CHEAT_LIST WhichCheat)

//INT MissionsQuit
	SWITCH WhichCheat
	
		CASE CHEAT_LIST_NONE
		
		BREAK

	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC




PROC RUN_CHEAT_MESSAGE_CHECK()
	BOOL IsAnyTrue
	INT I
	FOR I = 0 TO ENUM_TO_INT(CHEAT_LIST_MAXNUM)-1
		IF MPGlobalsHud.DisplayCheatMessage[I]	
			IsAnyTrue = TRUE
			I = ENUM_TO_INT(CHEAT_LIST_MAXNUM)
		ENDIF
	ENDFOR
	
	
	
	IF IsAnyTrue
	
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("RUN_CHEAT_MESSAGE_CHECK - called ")
		#ENDIF
		
		
		

	ENDIF
	
		


ENDPROC





PROC RUN_DM_CHEAT_CHECK()
	///Works out how many missions you've started. Compares with Over (Valid ending of a mission) and quits(already counted cheating exits)
		
		

//	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_END)
	

//	#IF IS_DEBUG_BUILD
//	NET_PRINT("[BC] - cheat_handler - REMOTE_CHEATER_PLAYER_DETECTED: CHEAT_LIST_PLUG_PULLED_DM set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
//	#ENDIF
	
//	IF HAS_PLAYER_CHEATED(CHEAT_LIST_PLUG_PULLED_DM)
//	
//		SET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_QUIT, MissionsQuit)
//		
//		SET_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_DM, TRUE)
//		
//		MPGlobalsHud.DisplayCheatMessage[CHEAT_LIST_PLUG_PULLED_DM] = TRUE
//		
////		TEXT_LABEL_63 QuitReason =  GET_PUNISH_REASON_CHEAT(CHEAT_LIST_PLUG_PULLED_DM)
//		// Punishments for quitting custody early.
////		BEGIN_TEXT_COMMAND_THEFEED_POST(QuitReason)
////		END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)
//		#IF IS_DEBUG_BUILD
//		NET_PRINT("[BC] - cheat_handler - set this cheat happened: CHEAT_LIST_PLUG_PULLED_DM.")NET_NL()
//		#ENDIF
//
//		FLOAT fNewValue 
////		INT iNewValue 
////		iNewValue= (MissionsQuit-GET_MP_INT_CHARACTER_STAT(MP_STAT_TIMES_CHEATED_SCRIPT) )
////		SET_MP_INT_CHARACTER_STAT(MP_STAT_TIMES_CHEATED_SCRIPT, iNewValue)
//		
//		fNewValue= (TO_FLOAT(MissionsQuit)-GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT))
//		SET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT, fNewValue)
//		
//		REMOTE_CHEATER_PLAYER_DETECTED(PLAYER_ID(), ENUM_TO_INT(CHEAT_LIST_PLUG_PULLED_DM), MissionsQuit)
//		
//	ENDIF
	

ENDPROC


PROC RUN_RACE_CHEAT_CHECK()
	///Works out how many missions you've started. Compares with Over (Valid ending of a mission) and quits(already counted cheating exits)
		
		
//	INT MissionsQuit = GET_MP_INT_CHARACTER_STAT(MP_STAT_RACE_START) - GET_MP_INT_CHARACTER_STAT(MP_STAT_RACE_END)
//	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_END)
	

//	#IF IS_DEBUG_BUILD
//	NET_PRINT("[BC] - cheat_handler - REMOTE_CHEATER_PLAYER_DETECTED: CHEAT_LIST_PLUG_PULLED_RACE set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
//	#ENDIF
	
//	IF HAS_PLAYER_CHEATED(CHEAT_LIST_PLUG_PULLED_RACE)
//	
//	
//		SET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_QUIT, MissionsQuit)
//		
//		SET_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_RACE, TRUE)
//		
//		MPGlobalsHud.DisplayCheatMessage[CHEAT_LIST_PLUG_PULLED_RACE] = TRUE
//		
////		TEXT_LABEL_63 QuitReason =  GET_PUNISH_REASON_CHEAT(CHEAT_LIST_PLUG_PULLED_RACE)
//			
//		// Punishments for quitting custody early.
//		#IF IS_DEBUG_BUILD
//		NET_PRINT("[BC] - cheat_handler - set this cheat happened: CHEAT_LIST_PLUG_PULLED_RACE.")NET_NL()
//		#ENDIF
//		
////		BEGIN_TEXT_COMMAND_THEFEED_POST(QuitReason)
////		END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)
//		
//		FLOAT fNewValue 
////		INT iNewValue 
////		iNewValue= (MissionsQuit-GET_MP_INT_CHARACTER_STAT(MP_STAT_TIMES_CHEATED_SCRIPT) )
////		SET_MP_INT_CHARACTER_STAT(MP_STAT_TIMES_CHEATED_SCRIPT, iNewValue)
//		
//		fNewValue= (TO_FLOAT(MissionsQuit)-GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT))
//		SET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT, fNewValue)
//		
//				
//		REMOTE_CHEATER_PLAYER_DETECTED(PLAYER_ID(), ENUM_TO_INT(CHEAT_LIST_PLUG_PULLED_RACE), MissionsQuit)
//	
//		
//	
//	ENDIF

ENDPROC


PROC RUN_MC_CHEAT_CHECK()
	///Works out how many missions you've started. Compares with Over (Valid ending of a mission) and quits(already counted cheating exits)
		
		
//	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_END)
	
	
//	#IF IS_DEBUG_BUILD
//	NET_PRINT("[BC] - cheat_handler - REMOTE_CHEATER_PLAYER_DETECTED: CHEAT_LIST_PLUG_PULLED_MC_MISSION set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
//	#ENDIF
	
//	IF HAS_PLAYER_CHEATED(CHEAT_LIST_PLUG_PULLED_MC_MISSION)
//	
//		SET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_QUIT, MissionsQuit)
//		
//		SET_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_MC_MISSION, TRUE)
//		
//		MPGlobalsHud.DisplayCheatMessage[CHEAT_LIST_PLUG_PULLED_MC_MISSION] = TRUE
//		
//			
//		// Punishments for quitting custody early.
//		#IF IS_DEBUG_BUILD
//		NET_PRINT("[BC] - cheat_handler - set this cheat happened: CHEAT_LIST_PLUG_PULLED_MC_MISSION.")NET_NL()
//		#ENDIF
//		
//		
//		FLOAT fNewValue 
//		
//		fNewValue= (TO_FLOAT(MissionsQuit)-GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT))
//		SET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT, fNewValue)
//		
//		REMOTE_CHEATER_PLAYER_DETECTED(PLAYER_ID(), ENUM_TO_INT(CHEAT_LIST_PLUG_PULLED_MC_MISSION), MissionsQuit)	
//	ENDIF

ENDPROC

PROC RUN_MINIGAME_CHEAT_CHECK()
	///Works out how many missions you've started. Compares with Over (Valid ending of a mission) and quits(already counted cheating exits)
		
		
//	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_END)
	
	
//	#IF IS_DEBUG_BUILD
//	NET_PRINT("[BC] - cheat_handler - REMOTE_CHEATER_PLAYER_DETECTED: CHEAT_LIST_PLUG_PULLED_MINIGAME set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
//	#ENDIF
//	IF HAS_PLAYER_CHEATED(CHEAT_LIST_PLUG_PULLED_MINIGAME)
//	
//		SET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_QUIT, MissionsQuit)
//		
//		SET_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_MINIGAME, TRUE)
//		
//		MPGlobalsHud.DisplayCheatMessage[CHEAT_LIST_PLUG_PULLED_MINIGAME] = TRUE
//			
//		// Punishments for quitting custody early.
//		#IF IS_DEBUG_BUILD
//		NET_PRINT("[BC] - cheat_handler - set this cheat happened: CHEAT_LIST_PLUG_PULLED_MINIGAME.")NET_NL()
//		#ENDIF
//		
//		FLOAT fNewValue 
//		
//		fNewValue= (TO_FLOAT(MissionsQuit)-GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT))
//		SET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT, fNewValue)
//		
//		REMOTE_CHEATER_PLAYER_DETECTED(PLAYER_ID(), ENUM_TO_INT(CHEAT_LIST_PLUG_PULLED_MINIGAME), MissionsQuit)	
//		
//	ENDIF

ENDPROC

PROC IGNORE_CHEATING_CHECK()

	IF SHOULD_IGNORE_CHEATER_RATING()
//		SET_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_MINIGAME, FALSE)
//		MPGlobalsHud.DisplayCheatMessage[CHEAT_LIST_PLUG_PULLED_MINIGAME] = FALSE
//
//		SET_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_DM, FALSE)
//		MPGlobalsHud.DisplayCheatMessage[CHEAT_LIST_PLUG_PULLED_DM] = FALSE
//
//		SET_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_RACE, FALSE)
//		MPGlobalsHud.DisplayCheatMessage[CHEAT_LIST_PLUG_PULLED_RACE] = FALSE
//
//		SET_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_PLUG_PULLED_MC_MISSION, FALSE)
//		MPGlobalsHud.DisplayCheatMessage[CHEAT_LIST_PLUG_PULLED_MC_MISSION] = FALSE
//
//		SET_CHEAT_HAPPENED_THIS_MISSION(CHEAT_LIST_VOTED_OUT_OF_SESSION, FALSE)
//		MPGlobalsHud.DisplayCheatMessage[CHEAT_LIST_VOTED_OUT_OF_SESSION] = FALSE

		
		SET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT, 0.0)


		SET_HEAVILY_ACCESSED_MP_INT_PLAYER_STAT(MPPLY_BECAME_CHEATER_NUM, 0)

		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("IGNORE_CHEATING - called ")
		#ENDIF
	ENDIF
	
ENDPROC

//PROC RUN_CHEATER_STAT_BOOT_CHECKS()
//
//	IF NETWORK_PLAYER_IS_CHEATER()
//		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_PLAYED_AS_CHEATER)
//		NET_NL()NET_PRINT("RUN_CHEATER_STAT_BOOT_CHECKS - INCREMENT_MP_INT_PLAYER_STAT(MPPLY_PLAYED_AS_CHEATER) ")
//	ENDIF
//		
//	NET_NL()NET_PRINT("RUN_CHEATER_STAT_BOOT_CHECKS - called ")
//
//ENDPROC




FUNC BOOL HAS_PLAYER_PULLED_PLUG()

	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_STARTED) - GET_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_ENDED)
	IF MissionsQuit > 0
		IF MissionsQuit > GET_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_QUIT)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC RUN_GENERAL_PLUG_PULLING_COUNT_AT_BOOT()

	GET_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_STARTED)
		
	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_STARTED) - GET_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_ENDED)
	
	#IF IS_DEBUG_BUILD
	NET_PRINT("[BC] - cheat_handler - RUN_GENERAL_PLUG_PULLING_COUNT_AT_BOOT: MPPLY_ACTIVITY_QUIT set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
	#ENDIF
	
	IF HAS_PLAYER_PULLED_PLUG()
	
		SET_MP_INT_PLAYER_STAT(MPPLY_ACTIVITY_QUIT, MissionsQuit)
	
	ENDIF
	
	
	
ENDPROC


PROC CHECK_CHEAT_TICKER(INT AmountBefore)
	IF (AmountBefore < 10 AND GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT) >= 10)
	OR (AmountBefore < 20 AND GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT) >= 20)
	OR (AmountBefore < 30 AND GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT) >= 30)
	OR (AmountBefore < 40 AND GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT) >= 40)
	
//		SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
//		TickerEventData.TickerEvent = TICKER_EVENT_CHEAT_MILESTONE
//		TickerEventData.dataInt = FLOOR(GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT))
//		BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS(TRUE))		

		PRINT_TICKER("HUD_CHTRISE")


	ENDIF

ENDPROC

PROC RUN_START_OF_GAME_CHEAT_CHECK(BOOL& HasBeenDoneOnce, MP_GAMEMODE agamemode)

	///Works out how many Custody's you've started. Compares with Over (Valid ending of a custody) and quits(already counted cheating exits)
	IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID(), FALSE)
		EXIT
	ENDIF
	
	IF HAVE_STATS_LOADED()
		IF HasBeenDoneOnce = FALSE	
			
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("RUN_START_OF_GAME_CHEAT_CHECK - called ")
			#ENDIF
			
			IF NETWORK_PLAYER_IS_CHEATER()
				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_CHEATER) = FALSE
					SET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_CHEATER_DT, GET_TODAYS_DATE())
					INCREMENT_MP_INT_PLAYER_STAT(MPPLY_BECAME_CHEATER_NUM)
					
				ENDIF
			ELSE
				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_CHEATER)
					SET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_CHEATER, FALSE)
				ENDIF
			ENDIF
			
			IF NETWORK_PLAYER_IS_CHEATER()
			AND GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_CHEATER) = FALSE
				INCREMENT_MP_INT_PLAYER_STAT(MPPLY_BECAME_CHEATER_NUM)
			ENDIF
		
			RUN_GENERAL_PLUG_PULLING_COUNT_AT_BOOT()
			
			
			IF agamemode = GAMEMODE_FM
				RUN_DM_CHEAT_CHECK()
				RUN_RACE_CHEAT_CHECK()
				RUN_MC_CHEAT_CHECK()
				RUN_MINIGAME_CHEAT_CHECK()
				IGNORE_CHEATING_CHECK()
				RUN_CHEAT_MESSAGE_CHECK()
				
			ENDIF
			
			
//			RUN_CHEATER_STAT_BOOT_CHECKS()
			
			
//			MPGlobals.g_Cheat_FRIENDLY_LastCheck = g_MPPLY_FRIENDLY
//			MPGlobals.g_Cheat_OFFENSIVE_LANGUAGE_LastCheck = g_MPPLY_OFFENSIVE_LANGUAGE
//			MPGlobals.g_Cheat_GRIEFING_LastCheck = g_MPPLY_GRIEFING
//			MPGlobals.g_Cheat_HELPFUL_LastCheck = g_MPPLY_HELPFUL
			
			
			
			HasBeenDoneOnce = TRUE
			
		ENDIF
	ENDIF

ENDPROC


//PROC RUN_CHEAT_DEGREDATION(SCRIPT_TIMER& aTimer, INT& NumberOfDays)
//	
////	NumberOfDays = g_sMPTunables.CheaterTimeExponential^GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_A_CHEATER)
//	FLOAT Expon = TO_FLOAT(g_sMPTunables.CheaterTimeExponential)
//	FLOAT Number = TO_FLOAT(g_MPPLY_BECAME_CHEATER_NUM)
//	FLOAT Power = POW(Expon,Number)
//	NumberOfDays = FLOOR(Power)
//
//
//	
//	IF GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT) > 0
//	AND NOT IS_DATE_EMPTY(GET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_CHEATER_DT))
//	
//		IF HAS_DAYS_PASSED_BETWEEN_DATES(GET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_CHEATER_DT), GET_TODAYS_DATE(), NumberOfDays)
//			#IF IS_DEBUG_BUILD
//			NET_NL()NET_PRINT("[BCCHEAT] RUN_CHEAT_DEGREDATION: has passed time, reducing the player and Chars CHEAT to 0 ")
//			#ENDIF
//			SET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT, 0)
//		ENDIF
//		
//	ENDIF
//	
//
//
//	IF NOT NETWORK_PLAYER_IS_BADSPORT()
//	AND NOT NETWORK_PLAYER_IS_CHEATER()
//
//		INT milliseconds = (g_sMPTunables.cheatResetMinutes*60000)
//		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, milliseconds, TRUE) 
//			//Reduce Cheater level
//			IF GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT) > 0
//				#IF IS_DEBUG_BUILD
//				NET_NL()NET_PRINT("[BCCHEAT] RUN_CHEAT_DEGREDATION: has passed time, reducing the player and Chars cheat level by ")NET_PRINT_FLOAT(g_sMPTunables.cheatAmountToForgiveBy)
//				#ENDIF
//				DECREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT, g_sMPTunables.cheatAmountToForgiveBy)
//			ENDIF
//		ENDIF
//	ENDIF
//	
//
//
//	
//	
////	IF MPGlobals.g_Cheat_FRIENDLY_LastCheck <> g_MPPLY_FRIENDLY
////		FLOAT Scale = TO_FLOAT(g_MPPLY_FRIENDLY) - TO_FLOAT(MPGlobals.g_Cheat_FRIENDLY_LastCheck)
////		IF Scale >= 16
////			#IF IS_DEBUG_BUILD
////			NET_NL()NET_PRINT("RUN_CHEAT_DEGREDATION: FRIENDLY Level has changed, reducing cheat level by ")NET_PRINT_FLOAT(g_sMPTunables.cheatAmountToForgiveBy_Friendly)
////			#ENDIF
////			DECREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT, g_sMPTunables.cheatAmountToForgiveBy_Friendly)
////			MPGlobals.g_Cheat_FRIENDLY_LastCheck = g_MPPLY_FRIENDLY
////		ENDIF
////	ENDIF
////	
////	
////	IF MPGlobals.g_Cheat_HELPFUL_LastCheck <> g_MPPLY_HELPFUL
////		FLOAT Scale = TO_FLOAT(g_MPPLY_HELPFUL) - TO_FLOAT(MPGlobals.g_Cheat_HELPFUL_LastCheck)
////		IF Scale >= 16
////			#IF IS_DEBUG_BUILD
////			NET_NL()NET_PRINT("RUN_CHEAT_DEGREDATION: HELPFUL Level has changed, reducing cheat level by ")NET_PRINT_FLOAT( g_sMPTunables.cheatAmountToForgiveBy_Helpful)
////			#ENDIF
////			DECREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT, g_sMPTunables.cheatAmountToForgiveBy_Helpful)
////			MPGlobals.g_Cheat_HELPFUL_LastCheck = g_MPPLY_HELPFUL
////		ENDIF
////	ENDIF
////	
//	
//		
//
//	
//	
//ENDPROC



PROC RUN_CHEATER_PHONECALL()

	SWITCH g_CheaterStages
		CASE 0 
			IF HAVE_STATS_LOADED()
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					IF NETWORK_PLAYER_IS_CHEATER()
					#IF IS_DEBUG_BUILD
					OR g_iCheatPlayPhoneCall
					#ENDIF
						#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("[BCCHEAT] RUN_CHEATER_PHONECALL - called ")
						#ENDIF
						SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD, 1 )
						SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0), FALSE) 

						IF IS_CHARACTER_MALE(GET_ACTIVE_CHARACTER_SLOT())
							set_ped_comp_item_current_mp(PLAYER_PED_ID(), comp_type_berd, BERD_FMM_0_0)
						ELSE
							set_ped_comp_item_current_mp(PLAYER_PED_ID(), comp_type_berd, BERD_FMF_0_0)
						ENDIF
						
						
						
						IF GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_CHEATER) = FALSE
							SET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_CHEATER_DT, GET_TODAYS_DATE())
							INCREMENT_MP_INT_PLAYER_STAT(MPPLY_BECAME_CHEATER_NUM)
						ENDIF
						
						DISPLAY_LOCAL_TICKER_FOR_CHEATER_IMMEDIATE()

						SET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_CHEATER, TRUE)
						BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_IS_A_CHEATER, ALL_PLAYERS())
						BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
						
						#IF IS_DEBUG_BUILD
						g_iCheatPlayPhoneCall = FALSE
						#ENDIF
						g_CheaterStages++
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 1 
			IF (Request_MP_Comms_Txtmsg(CHAR_MP_FM_CONTACT, "MPCHT_1"))
                 g_CheaterStages++
   			 ENDIF
			 
		BREAK
		
		CASE 2 
			IF HAVE_STATS_LOADED()
				IF NOT NETWORK_PLAYER_IS_CHEATER()
				#IF IS_DEBUG_BUILD
				OR g_iCheatPlayPhoneCall
				#ENDIF
				
					PED_VARIATION_STRUCT ClothesConstruct 
					GET_STORED_MP_PLAYER_COMPONENTS(ClothesConstruct)
					SET_PED_VARIATIONS(PLAYER_PED_ID(),ClothesConstruct)
				
					Immediately_Export_Player_Headshot()
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_IS_NO_LONGER_A_CHEATER, ALL_PLAYERS())
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
					#IF IS_DEBUG_BUILD
					g_iCheatPlayPhoneCall = FALSE
					#ENDIF
					g_CheaterStages++
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 3 
			IF (Request_MP_Comms_Txtmsg(CHAR_MP_FM_CONTACT, "MPCHT_2"))
                 g_CheaterStages = 0
   			 ENDIF
		
		
		BREAK
	
	
	ENDSWITCH
	
	



ENDPROC




PROC PEDHEADSHOT_SP_SWITCH_REFRESH_CHECK()


	IF NETWORK_PLAYER_IS_CHEATER() <> g_WasCheaterLastSPSwitchCheck
		Immediately_Export_Player_Headshot()
	ENDIF
	g_WasCheaterLastSPSwitchCheck = NETWORK_PLAYER_IS_CHEATER()
	
	IF NETWORK_PLAYER_IS_BADSPORT() <> g_WasBadSportLastSPSwitchCheck
		Immediately_Export_Player_Headshot()
	ENDIF
	g_WasBadSportLastSPSwitchCheck = NETWORK_PLAYER_IS_BADSPORT()
	


ENDPROC




//PROC DISPLAY_TICKER_FOR_CHEATER(PLAYER_INDEX CheatingplayerID)
//	IF IS_NET_PLAYER_OK(CheatingplayerID, FALSE, TRUE)
//	
//		
//		BEGIN_TEXT_COMMAND_THEFEED_POST("HUD_CHEATYES")
//			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(CheatingplayerID, DEFAULT, TRUE))
//			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_PLAYER_NAME(CheatingplayerID))			
//		END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)		
//		
//	
//	ENDIF
//ENDPROC
//
//PROC DISPLAY_TICKER_FOR_CHEATER_END(PLAYER_INDEX CheatingplayerID)
//	IF IS_NET_PLAYER_OK(CheatingplayerID, FALSE, TRUE)
//	
//		BEGIN_TEXT_COMMAND_THEFEED_POST("HUD_CHEATNO")
//			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(CheatingplayerID, DEFAULT, TRUE))
//			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_PLAYER_NAME(CheatingplayerID))			
//		END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)		
//		
//	
//	ENDIF
//ENDPROC


//PROC DISPLAY_LOCAL_TICKER_FOR_CHEATER_TIME_SMALL(INT MillisecondsLeft, SCRIPT_TIMER& aTimer, INT MessageRepeatTime)
//	IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, MessageRepeatTime)
//		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
//			BEGIN_TEXT_COMMAND_THEFEED_POST("HUD_CHEATIME")
//				ADD_TEXT_COMPONENT_SUBSTRING_TIME(MillisecondsLeft, TIME_FORMAT_DAYS|TIME_FORMAT_HOURS|TIME_FORMAT_MINUTES|TEXT_FORMAT_SHOW_UNIT_DIVIDERS_AS_LETTERS)
//			END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)		
//		ENDIF
//	ENDIF
//ENDPROC
//
//PROC DISPLAY_LOCAL_TICKER_FOR_CHEATER_TIME_LARGE(INT YearsLeft,INT MonthsLeft,INT DaysLeft, SCRIPT_TIMER& aTimer, INT MessageRepeatTime)
//	IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, MessageRepeatTime)
//		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
//			BEGIN_TEXT_COMMAND_THEFEED_POST("HUD_CHEATIME_L")
//				ADD_TEXT_COMPONENT_INTEGER(YearsLeft)
//				ADD_TEXT_COMPONENT_INTEGER(MonthsLeft)
//				ADD_TEXT_COMPONENT_INTEGER(DaysLeft)
//			END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)		
//		ENDIF
//	ENDIF
//ENDPROC




//
//FUNC INT TIME_UNTIL_PLAYER_IS_FORGIVEN_CHEAT(SCRIPT_TIMER& ResetRunningTimer)
//
//	INT ResultMilliseconds
//	
//	FLOAT fPointsToLoseUntilForgiven = (GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT)-TO_FLOAT(g_sMPTunables.NotcheatThreshold))
//
//	FLOAT fTimeMillisecondsPerPoint = (TO_FLOAT(g_sMPTunables.cheatResetMinutes)*60*1000)/(g_sMPTunables.cheatAmountToForgiveBy)
//	
//	INT iTimeMillisecondsPerPoint = FLOOR(fTimeMillisecondsPerPoint)
//	INT iPointsToLoseUntilForgiven = FLOOR(fPointsToLoseUntilForgiven)
//	
//	ResultMilliseconds = (iPointsToLoseUntilForgiven*iTimeMillisecondsPerPoint)
//	
//	ResultMilliseconds -= GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), ResetRunningTimer.Timer)
//
//
//
//	RETURN ResultMilliseconds
//
//ENDFUNC


//
FUNC INT MILLISECONDS_UNTIL_PLAYER_IS_FORGIVEN_CHEAT(INT iNumDays)
	

	
	INT ResultMilliseconds
	
	
	
	TIMEOFDAY Today = GET_CURRENT_TIMEOFDAY_POSIX()
	TIMEOFDAY ForgivenDate = GET_OFFSET_TIMEOFDAY_IN_DAYS(GET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_CHEATER_DT), iNumDays)
	
	
	STRUCT_STAT_DATE TheDifference
	
	GET_DIFFERENCE_BETWEEN_TIMEOFDAYS(Today, ForgivenDate, TheDifference.Seconds, TheDifference.Minute, TheDifference.Hour, TheDifference.Day, TheDifference.Month, TheDifference.Year)



	ResultMilliseconds = CONVERT_OFFSET_TO_MILLISECONDS(TheDifference)

	
//	NET_NL()NET_PRINT("MILLISECONDS_UNTIL_PLAYER_IS_FORGIVEN_CHEAT ")
//	TEXT_LABEL_63 TlToday 
//	GET_TIMEOFDAY_STRING(Today, TlToday, TRUE, TRUE)
//	NET_NL()NET_PRINT(" 		- Today = ")NET_PRINT(TlToday)
//	TEXT_LABEL_63 TlForgive 
//	GET_TIMEOFDAY_STRING(ForgivenDate, TlForgive, TRUE, TRUE)
//	NET_NL()NET_PRINT(" 		- ForgivenDate = ")NET_PRINT(TlForgive)
//	NET_NL()NET_PRINT("			- TheDifference.Year = ") NET_PRINT_INT(TheDifference.Year)
//	NET_NL()NET_PRINT("			- TheDifference.Month = ") NET_PRINT_INT(TheDifference.Month)
//	NET_NL()NET_PRINT("			- TheDifference.Day = ") NET_PRINT_INT(TheDifference.Day)
//	NET_NL()NET_PRINT("			- TheDifference.Hour = ") NET_PRINT_INT(TheDifference.Hour)
//	NET_NL()NET_PRINT("			- TheDifference.Minute = ") NET_PRINT_INT(TheDifference.Minute)
//	NET_NL()NET_PRINT("			- TheDifference.Seconds = ") NET_PRINT_INT(TheDifference.Seconds)
//	NET_NL()NET_PRINT("			- ResultMilliseconds = ")NET_PRINT_INT(ResultMilliseconds)
	
	RETURN ResultMilliseconds
	

ENDFUNC


PROC MHDMY_UNTIL_PLAYER_IS_FORGIVEN_CHEAT(INT iNumDays, INT& Year, INT& Month, INT&Days, INT&Hours, INT&Minutes)
	
	
	TIMEOFDAY Today = GET_CURRENT_TIMEOFDAY_POSIX()
	TIMEOFDAY ForgivenDate = GET_OFFSET_TIMEOFDAY_IN_DAYS(GET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_CHEATER_DT), iNumDays)
	
	
	STRUCT_STAT_DATE TheDifference
	
	GET_DIFFERENCE_BETWEEN_TIMEOFDAYS(Today, ForgivenDate, TheDifference.Seconds, TheDifference.Minute, TheDifference.Hour, TheDifference.Day, TheDifference.Month, TheDifference.Year)

	Year = TheDifference.Year
	Month = TheDifference.Month
	Days = TheDifference.Day+1
	Hours = TheDifference.Hour
	Minutes = TheDifference.Minute
	
	
	
ENDPROC

//PROC DISPLAY_CHEAT_TICKER(INT NumberOfDays, INT RepeatTimer)
//
//	IF NumberOfDays < 14
//		DISPLAY_LOCAL_TICKER_FOR_CHEATER_TIME_SMALL(MILLISECONDS_UNTIL_PLAYER_IS_FORGIVEN_CHEAT(NumberOfDays), MPGlobalsHud.BadSportMessageTimer, RepeatTimer)
//	ELSE
//		STRUCT_STAT_DATE LargeDate
//		MHDMY_UNTIL_PLAYER_IS_FORGIVEN_CHEAT(NumberOfDays, LargeDate.Year, LargeDate.Month, LargeDate.Day, LargeDate.Hour, LargeDate.Minute)
//		DISPLAY_LOCAL_TICKER_FOR_CHEATER_TIME_LARGE( LargeDate.Year, LargeDate.Month, LargeDate.Day, MPGlobalsHud.BadSportMessageTimer, RepeatTimer)
//	ENDIF
//
//ENDPROC




//PROC RUN_CHEATER_POOL_MESSAGES(SCRIPT_TIMER& ResetRunningTimer, INT NumDays)
//
//	
//	SWITCH g_CheaterMessageStages
//	
//		CASE 0
//			IF NETWORK_PLAYER_IS_CHEATER()
//				
//				IF HAS_NET_TIMER_EXPIRED(ResetRunningTimer, 1000)
//				ENDIF
//								
//				g_CheaterMessageStages = 1
//			ENDIF
//		BREAK
//	
//		CASE 1
//		
//	
//				DISPLAY_CHEAT_TICKER(NumDays, 500000)
//			
//			IF NETWORK_PLAYER_IS_CHEATER()
//				IF g_CheaterRatingLastFrame <> GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT)
//					DISPLAY_CHEAT_TICKER(NumDays, -1)
//				ENDIF
//			ENDIF
//			
//			g_CheaterRatingLastFrame = GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_CHEAT)
//			
//			IF NOT NETWORK_PLAYER_IS_CHEATER()
//				
//				
//				g_CheaterMessageStages = 0
//				
//			ENDIF
//		BREAK
//		
//
//	ENDSWITCH
//
//
//
//ENDPROC













///////////////////////////////////////
///    Individual BAD SPORTS	///////////
///////////////////////////////////////




PROC RESET_THIS_MISSION_BAD_SPORTS()
	MPGlobals.g_Write_BAD_SPORTTrackerBitset = 0
ENDPROC

PROC RESET_LAST_MISSION_BAD_SPORTS()
	MPGlobals.g_Read_BAD_SPORTTrackerBitset = 0
ENDPROC

PROC RESET_ALL_BAD_SPORTS()
	
	RESET_THIS_MISSION_BAD_SPORTS()
	RESET_LAST_MISSION_BAD_SPORTS()

ENDPROC

FUNC BOOL HAS_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST aBAD_SPORT)
	RETURN IS_BIT_SET(MPGlobals.g_Read_BAD_SPORTTrackerBitset, ENUM_TO_INT(aBAD_SPORT))
ENDFUNC

PROC SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST aBAD_SPORT, BOOL didHappen)

	IF didHappen 
		IF NOT IS_BIT_SET(MPGlobals.g_Read_BAD_SPORTTrackerBitset, ENUM_TO_INT(aBAD_SPORT))
			SET_BIT(MPGlobals.g_Write_BAD_SPORTTrackerBitset, ENUM_TO_INT(aBAD_SPORT))
		ENDIF
	ELSE
		IF IS_BIT_SET(MPGlobals.g_Read_BAD_SPORTTrackerBitset, ENUM_TO_INT(aBAD_SPORT))
			CLEAR_BIT(MPGlobals.g_Write_BAD_SPORTTrackerBitset, ENUM_TO_INT(aBAD_SPORT))
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HAS_BAD_SPORT_HAPPENED_LAST_MISSION(BAD_SPORT_LIST aBAD_SPORT)
	RETURN IS_BIT_SET(MPGlobals.g_Write_BAD_SPORTTrackerBitset, ENUM_TO_INT(aBAD_SPORT))
ENDFUNC

PROC SET_BAD_SPORT_HAPPENED_LAST_MISSION(BAD_SPORT_LIST aBAD_SPORT, BOOL didHappen)

	IF didHappen 
		IF NOT IS_BIT_SET(MPGlobals.g_Read_BAD_SPORTTrackerBitset, ENUM_TO_INT(aBAD_SPORT))
			SET_BIT(MPGlobals.g_Write_BAD_SPORTTrackerBitset, ENUM_TO_INT(aBAD_SPORT))
		ENDIF
	ELSE
		IF IS_BIT_SET(MPGlobals.g_Read_BAD_SPORTTrackerBitset, ENUM_TO_INT(aBAD_SPORT))
			CLEAR_BIT(MPGlobals.g_Write_BAD_SPORTTrackerBitset, ENUM_TO_INT(aBAD_SPORT))
		ENDIF
	ENDIF
	
ENDPROC







/// PURPOSE:
///    Called before the start of mission stat upload. Tracks the BAD_SPORT progress.
PROC BAD_SPORT_TRACKER_START_OF_DEATHMATCH()
	BOOL resetPlugPulled
	
	//Mission pulled plug
	MPGlobals.g_Read_BAD_SPORTTrackerBitset = MPGlobals.g_Write_BAD_SPORTTrackerBitset
	
	IF HAS_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_DM)
		resetPlugPulled = TRUE
	ENDIF
	
	SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_DM, FALSE)
	
	IF resetPlugPulled 
		SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_DM, TRUE)
	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_BAD_SPORT_BITSET, MPGlobals.g_Read_BAD_SPORTTrackerBitset)
	
ENDPROC

/// PURPOSE:
///    Called before the start of mission stat upload. Tracks the BAD_SPORT progress.
PROC BAD_SPORT_TRACKER_START_OF_RACE()
	BOOL resetPlugPulled
	
	//Mission pulled plug
	MPGlobals.g_Read_BAD_SPORTTrackerBitset = MPGlobals.g_Write_BAD_SPORTTrackerBitset
	
	IF HAS_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_RACE)
		resetPlugPulled = TRUE
	ENDIF
	
	SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_RACE, FALSE)
	
	IF resetPlugPulled 
		SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_RACE, TRUE)
	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_BAD_SPORT_BITSET, MPGlobals.g_Read_BAD_SPORTTrackerBitset)
	
ENDPROC

/// PURPOSE:
///    Called before the start of mission stat upload. Tracks the BAD_SPORT progress.
PROC BAD_SPORT_TRACKER_START_OF_MC_MISSION()
	BOOL resetPlugPulled
	
	//Mission pulled plug
	MPGlobals.g_Read_BAD_SPORTTrackerBitset = MPGlobals.g_Write_BAD_SPORTTrackerBitset
	
	IF HAS_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_MC_MISSION)
		resetPlugPulled = TRUE
	ENDIF
	
	SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_MC_MISSION, FALSE)
	
	IF resetPlugPulled 
		SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_MC_MISSION, TRUE)
	ENDIF
	
	SET_MP_INT_CHARACTER_STAT(MP_STAT_BAD_SPORT_BITSET, MPGlobals.g_Read_BAD_SPORTTrackerBitset)
	
ENDPROC







//FUNC INT GET_PUNISH_AMOUNT()
//	INT LastXP = GET_MP_INT_CHARACTER_STAT(MP_STAT_XP_EARNED_ON_LAST_MISSION)
//	IF LastXP = 0
//		RETURN 100
//	ELSE
//		RETURN FLOOR(LastXP/2)
//	ENDIF
//ENDFUNC

FUNC BOOL HAS_PLAYER_BAD_SPORTED(BAD_SPORT_LIST WhichBAD_SPORT)

INT MissionsQuit
	SWITCH WhichBAD_SPORT
	
				
		CASE BAD_SPORT_LIST_PLUG_PULLED_DM

//			MissionsQuit = GET_MP_INT_CHARACTER_STAT(MP_STAT_DM_START) - GET_MP_INT_CHARACTER_STAT(MP_STAT_DM_END)
			MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_END)
			IF MissionsQuit > 0
//				IF MissionsQuit > GET_MP_INT_CHARACTER_STAT(MP_STAT_DM_BAD_SPORT_QUIT)
				IF MissionsQuit > GET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_QUIT)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE BAD_SPORT_LIST_PLUG_PULLED_RACE

//			MissionsQuit = GET_MP_INT_CHARACTER_STAT(MP_STAT_RACE_START) - GET_MP_INT_CHARACTER_STAT(MP_STAT_RACE_END)
			MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_END)

			IF MissionsQuit > 0
//				IF MissionsQuit > GET_MP_INT_CHARACTER_STAT(MP_STAT_RACE_BAD_SPORT_QUIT)
				IF MissionsQuit > GET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_QUIT)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE BAD_SPORT_LIST_PLUG_PULLED_MC_MISSION

//			MissionsQuit = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MC_STARTED) - GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MC_OVER)
			MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_END)
			IF MissionsQuit > 0
//				IF MissionsQuit > GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MC_BAD_SPORT_QUIT)
				IF MissionsQuit > GET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_QUIT)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE BAD_SPORT_LIST_PLUG_PULLED_MINIGAME

//			MissionsQuit = GET_MP_INT_CHARACTER_STAT(MP_STAT_MINIGAME_START) - GET_MP_INT_CHARACTER_STAT(MP_STAT_MINIGAME_END)
			MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_END)

			IF MissionsQuit > 0
//				IF MissionsQuit > GET_MP_INT_CHARACTER_STAT(MP_STAT_MINIGAME_BAD_SPORT_QUIT)
				IF MissionsQuit > GET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_QUIT)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE BAD_SPORT_LIST_VOTED_OUT_OF_SESSION

			MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_VOTED_OUT) - GET_MP_INT_PLAYER_STAT(MPPLY_VOTED_OUT_DELTA)

			IF MissionsQuit > 0
				IF MissionsQuit > GET_MP_INT_PLAYER_STAT(MPPLY_VOTED_OUT_QUIT)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE BAD_SPORT_LIST_PLUG_PULLED_CAPTURE

			MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_END)
			IF MissionsQuit > 0
				IF MissionsQuit > GET_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_QUIT)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE BAD_SPORT_LIST_PLUG_PULLED_SURVIVAL

			MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_END)
			IF MissionsQuit > 0
				IF MissionsQuit > GET_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_QUIT)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE BAD_SPORT_LIST_PLUG_PULLED_LTS

			MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_END)
			IF MissionsQuit > 0
				IF MissionsQuit > GET_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_QUIT)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE BAD_SPORT_LIST_PLUG_PULLED_PARACHUTE

			MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_PARA_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_PARA_CHEAT_END)
			IF MissionsQuit > 0
				IF MissionsQuit > GET_MP_INT_PLAYER_STAT(MPPLY_PARA_CHEAT_QUIT)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE BAD_SPORT_LIST_PLUG_PULLED_HEIST
			MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_END)
			IF MissionsQuit > 0
				IF MissionsQuit > GET_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_QUIT)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE BAD_SPORT_LIST_PLUG_PULLED_FMEVENT
			MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_FMEVN_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_FMEVN_CHEAT_END)
			IF MissionsQuit > 0
				IF MissionsQuit > GET_MP_INT_PLAYER_STAT(MPPLY_FMEVN_CHEAT_QUIT)
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC




PROC RUN_BAD_SPORT_MESSAGE_CHECK()
	IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID(), FALSE)
		EXIT
	ENDIF
	
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
	AND ARE_ALL_INSTANCED_TUTORIALS_COMPLETE()
	AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
	AND IS_SKYSWOOP_AT_GROUND()
	AND (NATIVE_TO_INT(PLAYER_ID()) > -1 AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].boolReplacementBS,GlobalPlayerBroadcastDataFM_BS_BbNeedsFmCleanup))

		BOOL IsAnyTrue
		INT I
		FOR I = 0 TO ENUM_TO_INT(BAD_SPORT_LIST_MAXNUM)-1
			IF MPGlobalsHud.DisplayBAD_SPORTMessage[I]	
				IsAnyTrue = TRUE
				I = ENUM_TO_INT(BAD_SPORT_LIST_MAXNUM)
			ENDIF
		ENDFOR
		

		
		IF IsAnyTrue
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_MESSAGE_CHECK - called ")
				#ENDIF
				
				TEXT_LABEL_63 QuitReason 
				
				QuitReason = "QUIT_RS_ALL" 
				
				PRINT_HELP(QuitReason)
				
	//		BEGIN_TEXT_COMMAND_THEFEED_POST(QuitReason)
	//		END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)
			
			
			FOR I = 0 TO ENUM_TO_INT(BAD_SPORT_LIST_MAXNUM)-1
				IF MPGlobalsHud.DisplayBAD_SPORTMessage[I]	
					 MPGlobalsHud.DisplayBAD_SPORTMessage[I]	 = FALSE
				ENDIF
			ENDFOR
		
		ENDIF
		
		
		
	ENDIF
	
		


ENDPROC


PROC RUN_START_OF_GAME_MESSAGES()

	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("RUN_START_OF_GAME_MESSAGES - called ")
	#ENDIF
	
	TEXT_LABEL_63 QuitReason 

	IF GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_CHEATER) = TRUE
	AND GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_BAD_SPORT) = FALSE
	AND NETWORK_PLAYER_IS_CHEATER() = FALSE
	AND NETWORK_PLAYER_IS_BADSPORT() = FALSE
		
		//Came from the Cheater Pool into the normal pool
		QuitReason = "CHT_NORM_POOL" 
		BEGIN_TEXT_COMMAND_THEFEED_POST(QuitReason)
		END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)
		EXIT
		
	ENDIF
	
	
	IF GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_BAD_SPORT) = TRUE
	AND GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_CHEATER) = FALSE
	AND NETWORK_PLAYER_IS_CHEATER() = FALSE
	AND NETWORK_PLAYER_IS_BADSPORT() = FALSE
		
		//Came from the Bad Sport Pool into the normal pool
		QuitReason = "BAD_NORM_POOL" 
		BEGIN_TEXT_COMMAND_THEFEED_POST(QuitReason)
		END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)
		EXIT
		
	ENDIF

	IF GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_BAD_SPORT) = FALSE
	AND GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_CHEATER) = FALSE
	AND NETWORK_PLAYER_IS_CHEATER() = TRUE
	AND NETWORK_PLAYER_IS_BADSPORT() = FALSE
		
		//Came from the normal Pool into the Cheater pool
		QuitReason = "NORM_CHT_POOL" 
		BEGIN_TEXT_COMMAND_THEFEED_POST(QuitReason)
		END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)
		EXIT
		
	ENDIF
	
	
	IF GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_BAD_SPORT) = FALSE
	AND GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_CHEATER) = FALSE
	AND NETWORK_PLAYER_IS_CHEATER() = FALSE
	AND NETWORK_PLAYER_IS_BADSPORT() = TRUE
		
		//Came from the normal Pool into the Bad Sport pool
		QuitReason = "NORM_BAD_POOL" 
		BEGIN_TEXT_COMMAND_THEFEED_POST(QuitReason)
		END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)
		EXIT
		
	ENDIF

	IF GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_BAD_SPORT) = TRUE
	AND GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_CHEATER) = FALSE
	AND NETWORK_PLAYER_IS_CHEATER() = TRUE
	AND NETWORK_PLAYER_IS_BADSPORT() = FALSE
		
		//Came from the Bad Sport Pool into the Cheater pool
		QuitReason = "CHT_BAD_POOL" 
		BEGIN_TEXT_COMMAND_THEFEED_POST(QuitReason)
		END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)
		EXIT
		
	ENDIF
	
	
	IF GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_BAD_SPORT) = FALSE
	AND GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_CHEATER) = TRUE
	AND NETWORK_PLAYER_IS_CHEATER() = FALSE
	AND NETWORK_PLAYER_IS_BADSPORT() = TRUE
		
		//Came from the Cheater Pool into the Bad Sport pool
		QuitReason = "BAD_CHT_POOL" 
		BEGIN_TEXT_COMMAND_THEFEED_POST(QuitReason)
		END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)
		EXIT
		
	ENDIF









ENDPROC




PROC RUN_DM_BAD_SPORT_CHECK(BOOL bIgnoreLast)
	///Works out how many missions you've started. Compares with Over (Valid ending of a mission) and quits(already counted BAD_SPORTing exits)
		
		
	INT ThresholdForLargeInt = THRESHOLD_FOR_LARGE_INT_BADSPORT
	IF GET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_START) > ThresholdForLargeInt
	AND GET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_END) > ThresholdForLargeInt
		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_TALLY)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_START, -ThresholdForLargeInt)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_END, -ThresholdForLargeInt)
	ENDIF		
		
//	INT MissionsQuit = GET_MP_INT_CHARACTER_STAT(MP_STAT_DM_START) - GET_MP_INT_CHARACTER_STAT(MP_STAT_DM_END)
	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_END)
	INT BadSportDifference =MissionsQuit 
	

	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - REMOTE_BAD_SPORTER_PLAYER_DETECTED: BAD_SPORT_LIST_PLUG_PULLED_DM set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- MPPLY_DM_CHEAT_START = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_START)) NET_NL()
	NET_PRINT(" 			- MPPLY_DM_CHEAT_END = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_END)) NET_NL()
	NET_PRINT(" 			- MPPLY_DM_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_QUIT)) NET_NL()
	
	#ENDIF
	
	IF HAS_PLAYER_BAD_SPORTED(BAD_SPORT_LIST_PLUG_PULLED_DM)
	
		BadSportDifference -= GET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_QUIT)
	
	
		SET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_QUIT, MissionsQuit)
		
		NET_PRINT(" 			- MPPLY_DM_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_QUIT)) NET_NL()
		NET_PRINT(" 			- MissionsQuit = ")NET_PRINT_INT(MissionsQuit) NET_NL()
		NET_PRINT(" 			- BadSportDifference = ")NET_PRINT_INT(BadSportDifference) NET_NL()
			

		IF bIgnoreLast = FALSE
		
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - BAD_SPORT happened!! *** BAD_SPORT_LIST_PLUG_PULLED_DM *** INCREASE MPPLY_OVERALL_BADSPORT By ")NET_PRINT_INT(BadSportDifference)NET_NL()
			#ENDIF
		
			SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_DM, TRUE)
			MPGlobalsHud.DisplayBAD_SPORTMessage[BAD_SPORT_LIST_PLUG_PULLED_DM] = TRUE
//			FLOAT fNewValue 
//			fNewValue= (TO_FLOAT(MissionsQuit)-GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT))
			INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, TO_FLOAT(BadSportDifference))
		ENDIF
		
	ELSE
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - REMOTE_BAD_SPORTER_PLAYER_DETECTED: BAD_SPORT_LIST_PLUG_PULLED_DM set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
		#ENDIF
	ENDIF
	

ENDPROC


PROC RUN_RACE_BAD_SPORT_CHECK(BOOL bIgnoreLast)
	///Works out how many missions you've started. Compares with Over (Valid ending of a mission) and quits(already counted BAD_SPORTing exits)
		
	INT ThresholdForLargeInt = THRESHOLD_FOR_LARGE_INT_BADSPORT
	IF GET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_START) > ThresholdForLargeInt
	AND GET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_END) > ThresholdForLargeInt
		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_TALLY)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_START, -ThresholdForLargeInt)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_END, -ThresholdForLargeInt)
	ENDIF		
		
	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_END)
	INT BadSportDifference =MissionsQuit 

	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - REMOTE_BAD_SPORTER_PLAYER_DETECTED: BAD_SPORT_LIST_PLUG_PULLED_RACE set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- MPPLY_RACE_CHEAT_START = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_START)) NET_NL()
	NET_PRINT(" 			- MPPLY_RACE_CHEAT_END = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_END)) NET_NL()
	NET_PRINT(" 			- MPPLY_RACE_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_QUIT)) NET_NL()
	
	#ENDIF
	
	IF HAS_PLAYER_BAD_SPORTED(BAD_SPORT_LIST_PLUG_PULLED_RACE)
	
		BadSportDifference -= GET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_QUIT)
	
	
		SET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_QUIT, MissionsQuit)
		
	NET_PRINT(" 			- MPPLY_RACE_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_QUIT)) NET_NL()
	NET_PRINT(" 			- MissionsQuit = ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- BadSportDifference = ")NET_PRINT_INT(BadSportDifference) NET_NL()
		
					

		IF bIgnoreLast = FALSE
		
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - BAD_SPORT happened!! *** BAD_SPORT_LIST_PLUG_PULLED_RACE *** INCREASE MPPLY_OVERALL_BADSPORT By ")NET_PRINT_INT(BadSportDifference)NET_NL()
			#ENDIF
		
			SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_RACE, TRUE)
			MPGlobalsHud.DisplayBAD_SPORTMessage[BAD_SPORT_LIST_PLUG_PULLED_RACE] = TRUE
//			FLOAT fNewValue 
//			fNewValue= (TO_FLOAT(MissionsQuit)-GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT))
			INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, TO_FLOAT(BadSportDifference))
		ENDIF
				
	
		
	
	ENDIF

ENDPROC


PROC RUN_MC_BAD_SPORT_CHECK(BOOL bIgnoreLast)
	///Works out how many missions you've started. Compares with Over (Valid ending of a mission) and quits(already counted BAD_SPORTing exits)
		
		
	INT ThresholdForLargeInt = THRESHOLD_FOR_LARGE_INT_BADSPORT
	IF GET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_START) > ThresholdForLargeInt
	AND GET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_END) > ThresholdForLargeInt
		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_TALLY)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_START, -ThresholdForLargeInt)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_END, -ThresholdForLargeInt)
	ENDIF	
		
//	INT MissionsQuit = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MC_STARTED) - GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_MC_OVER)
	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_END)
	INT BadSportDifference =MissionsQuit 
	
	
	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - REMOTE_BAD_SPORTER_PLAYER_DETECTED: BAD_SPORT_LIST_PLUG_PULLED_MC_MISSION set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- MPPLY_MC_CHEAT_START = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_START)) NET_NL()
	NET_PRINT(" 			- MPPLY_MC_CHEAT_END = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_END)) NET_NL()
	NET_PRINT(" 			- MPPLY_MC_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_QUIT)) NET_NL()
	
	
	
	#ENDIF
	
	IF HAS_PLAYER_BAD_SPORTED(BAD_SPORT_LIST_PLUG_PULLED_MC_MISSION)
	
		BadSportDifference -= GET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_QUIT)
	
	
		SET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_QUIT, MissionsQuit)
		
	NET_PRINT(" 			- MPPLY_MC_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_QUIT)) NET_NL()
	NET_PRINT(" 			- MissionsQuit = ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- BadSportDifference = ")NET_PRINT_INT(BadSportDifference) NET_NL()


		
		IF bIgnoreLast = FALSE
		
		
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - BAD_SPORT happened!! *** BAD_SPORT_LIST_PLUG_PULLED_MC_MISSION *** INCREASE MPPLY_OVERALL_BADSPORT By ")NET_PRINT_INT(BadSportDifference)NET_NL()
			#ENDIF
		
			SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_MC_MISSION, TRUE)
			MPGlobalsHud.DisplayBAD_SPORTMessage[BAD_SPORT_LIST_PLUG_PULLED_MC_MISSION] = TRUE
//			FLOAT fNewValue 
//			fNewValue= (TO_FLOAT(MissionsQuit)-GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT))
			INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, TO_FLOAT(BadSportDifference))
		ENDIF
		
	ENDIF

ENDPROC

PROC RUN_MINIGAME_BAD_SPORT_CHECK(BOOL bIgnoreLast)
	///Works out how many missions you've started. Compares with Over (Valid ending of a mission) and quits(already counted BAD_SPORTing exits)
	
	INT ThresholdForLargeInt = THRESHOLD_FOR_LARGE_INT_BADSPORT
	IF GET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_START) > ThresholdForLargeInt
	AND GET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_END) > ThresholdForLargeInt
		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_TALLY)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_START, -ThresholdForLargeInt)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_END, -ThresholdForLargeInt)
	ENDIF
		
	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_END)
	INT BadSportDifference =MissionsQuit 
	
	
	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - REMOTE_BAD_SPORTER_PLAYER_DETECTED: BAD_SPORT_LIST_PLUG_PULLED_MINIGAME set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- MPPLY_MGAME_CHEAT_START = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_START)) NET_NL()
	NET_PRINT(" 			- MPPLY_MGAME_CHEAT_END = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_END)) NET_NL()
	NET_PRINT(" 			- MPPLY_MGAME_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_QUIT)) NET_NL()
	#ENDIF
	IF HAS_PLAYER_BAD_SPORTED(BAD_SPORT_LIST_PLUG_PULLED_MINIGAME)
	
		BadSportDifference -= GET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_QUIT)
		
		SET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_QUIT, MissionsQuit)
		
		
		
		NET_PRINT(" 			- MPPLY_MGAME_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_QUIT)) NET_NL()
		NET_PRINT(" 			- MissionsQuit = ")NET_PRINT_INT(MissionsQuit) NET_NL()
		NET_PRINT(" 			- BadSportDifference = ")NET_PRINT_INT(BadSportDifference) NET_NL()
			

		IF bIgnoreLast = FALSE
		
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - BAD_SPORT happened!! *** BAD_SPORT_LIST_PLUG_PULLED_MINIGAME *** INCREASE MPPLY_OVERALL_BADSPORT By ")NET_PRINT_INT(BadSportDifference)NET_NL()
			#ENDIF
		
			SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_MINIGAME, TRUE)
			MPGlobalsHud.DisplayBAD_SPORTMessage[BAD_SPORT_LIST_PLUG_PULLED_MINIGAME] = TRUE
//			FLOAT fNewValue 
//			fNewValue= (TO_FLOAT(MissionsQuit)-GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT))
			INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, TO_FLOAT(BadSportDifference))
		ENDIF
		
		
	ENDIF

ENDPROC


PROC RUN_CAPTURE_BAD_SPORT_CHECK(BOOL bIgnoreLast)
	///Works out how many missions you've started. Compares with Over (Valid ending of a mission) and quits(already counted BAD_SPORTing exits)
		
	INT ThresholdForLargeInt = THRESHOLD_FOR_LARGE_INT_BADSPORT
	IF GET_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_START) > ThresholdForLargeInt
	AND GET_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_END) > ThresholdForLargeInt
		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_TALLY)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_START, -ThresholdForLargeInt)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_END, -ThresholdForLargeInt)
	ENDIF		
		
	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_END)
	INT BadSportDifference =MissionsQuit 

	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - REMOTE_BAD_SPORTER_PLAYER_DETECTED: BAD_SPORT_LIST_PLUG_PULLED_CAP set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- MPPLY_CAP_CHEAT_START = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_START)) NET_NL()
	NET_PRINT(" 			- MPPLY_CAP_CHEAT_END = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_END)) NET_NL()
	NET_PRINT(" 			- MPPLY_CAP_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_QUIT)) NET_NL()
	
	#ENDIF
	
	IF HAS_PLAYER_BAD_SPORTED(BAD_SPORT_LIST_PLUG_PULLED_CAPTURE)
	
		BadSportDifference -= GET_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_QUIT)
	
	
		SET_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_QUIT, MissionsQuit)
		
	NET_PRINT(" 			- MPPLY_CAP_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_CAP_CHEAT_QUIT)) NET_NL()
	NET_PRINT(" 			- MissionsQuit = ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- BadSportDifference = ")NET_PRINT_INT(BadSportDifference) NET_NL()
		
					

		
		IF bIgnoreLast = FALSE
		
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - BAD_SPORT happened!! *** BAD_SPORT_LIST_PLUG_PULLED_CAPTURE *** INCREASE MPPLY_OVERALL_BADSPORT By ")NET_PRINT_INT(BadSportDifference)NET_NL()
			#ENDIF
		
			SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_CAPTURE, TRUE)
			MPGlobalsHud.DisplayBAD_SPORTMessage[BAD_SPORT_LIST_PLUG_PULLED_CAPTURE] = TRUE
//			FLOAT fNewValue 
//			fNewValue= (TO_FLOAT(MissionsQuit)-GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT))
			INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, TO_FLOAT(BadSportDifference))
		ENDIF
				
	
		
	
	ENDIF

ENDPROC



PROC RUN_SURVIVAL_BAD_SPORT_CHECK(BOOL bIgnoreLast)
	///Works out how many missions you've started. Compares with Over (Valid ending of a mission) and quits(already counted BAD_SPORTing exits)
		
	INT ThresholdForLargeInt = THRESHOLD_FOR_LARGE_INT_BADSPORT
	IF GET_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_START) > ThresholdForLargeInt
	AND GET_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_END) > ThresholdForLargeInt
		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_TALLY)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_START, -ThresholdForLargeInt)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_END, -ThresholdForLargeInt)
	ENDIF		
		
	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_END)
	INT BadSportDifference =MissionsQuit 

	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - REMOTE_BAD_SPORTER_PLAYER_DETECTED: BAD_SPORT_LIST_PLUG_PULLED_SUR set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- MPPLY_SUR_CHEAT_START = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_START)) NET_NL()
	NET_PRINT(" 			- MPPLY_SUR_CHEAT_END = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_END)) NET_NL()
	NET_PRINT(" 			- MPPLY_SUR_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_QUIT)) NET_NL()
	
	#ENDIF
	
	IF HAS_PLAYER_BAD_SPORTED(BAD_SPORT_LIST_PLUG_PULLED_SURVIVAL)
	
		BadSportDifference -= GET_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_QUIT)
	
	
		SET_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_QUIT, MissionsQuit)
		
	NET_PRINT(" 			- MPPLY_SUR_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_SUR_CHEAT_QUIT)) NET_NL()
	NET_PRINT(" 			- MissionsQuit = ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- BadSportDifference = ")NET_PRINT_INT(BadSportDifference) NET_NL()
		

		
		IF bIgnoreLast = FALSE
		
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - BAD_SPORT happened!! *** BAD_SPORT_LIST_PLUG_PULLED_SURVIVAL *** INCREASE MPPLY_OVERALL_BADSPORT By ")NET_PRINT_INT(BadSportDifference)NET_NL()
			#ENDIF
		
			SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_SURVIVAL, TRUE)
			MPGlobalsHud.DisplayBAD_SPORTMessage[BAD_SPORT_LIST_PLUG_PULLED_SURVIVAL] = TRUE
//			FLOAT fNewValue 
//			fNewValue= (TO_FLOAT(MissionsQuit)-GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT))
			INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, TO_FLOAT(BadSportDifference))
		ENDIF
				
	
		
	
	ENDIF

ENDPROC



PROC RUN_LTS_BAD_SPORT_CHECK(BOOL bIgnoreLast)
	///Works out how many missions you've started. Compares with Over (Valid ending of a mission) and quits(already counted BAD_SPORTing exits)
		
	INT ThresholdForLargeInt = THRESHOLD_FOR_LARGE_INT_BADSPORT
	IF GET_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_START) > ThresholdForLargeInt
	AND GET_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_END) > ThresholdForLargeInt
		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_TALLY)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_START, -ThresholdForLargeInt)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_END, -ThresholdForLargeInt)
	ENDIF		
		
	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_END)
	INT BadSportDifference =MissionsQuit 

	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - REMOTE_BAD_SPORTER_PLAYER_DETECTED: BAD_SPORT_LIST_PLUG_PULLED_LTS set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- MPPLY_LTS_CHEAT_START = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_START)) NET_NL()
	NET_PRINT(" 			- MPPLY_LTS_CHEAT_END = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_END)) NET_NL()
	NET_PRINT(" 			- MPPLY_LTS_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_QUIT)) NET_NL()
	
	#ENDIF
	
	IF HAS_PLAYER_BAD_SPORTED(BAD_SPORT_LIST_PLUG_PULLED_LTS)
	
		BadSportDifference -= GET_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_QUIT)
	
	
		SET_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_QUIT, MissionsQuit)
		
	NET_PRINT(" 			- MPPLY_LTS_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_LTS_CHEAT_QUIT)) NET_NL()
	NET_PRINT(" 			- MissionsQuit = ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- BadSportDifference = ")NET_PRINT_INT(BadSportDifference) NET_NL()
		
					

		
		IF bIgnoreLast = FALSE
		
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - BAD_SPORT happened!! *** BAD_SPORT_LIST_PLUG_PULLED_LTS *** INCREASE MPPLY_OVERALL_BADSPORT By ")NET_PRINT_INT(BadSportDifference)NET_NL()
			#ENDIF
		
			SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_LTS, TRUE)
			MPGlobalsHud.DisplayBAD_SPORTMessage[BAD_SPORT_LIST_PLUG_PULLED_LTS] = TRUE
//			FLOAT fNewValue 
//			fNewValue= (TO_FLOAT(MissionsQuit)-GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT))
			INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, TO_FLOAT(BadSportDifference))
		ENDIF
				
	
		
	
	ENDIF

ENDPROC

PROC RUN_HEIST_BAD_SPORT_CHECK(BOOL bIgnoreLast)
	///Works out how many missions you've started. Compares with Over (Valid ending of a mission) and quits(already counted BAD_SPORTing exits)
		
	INT ThresholdForLargeInt = THRESHOLD_FOR_LARGE_INT_BADSPORT
	IF GET_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_START) > ThresholdForLargeInt
	AND GET_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_END) > ThresholdForLargeInt
		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_TALLY)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_START, -ThresholdForLargeInt)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_END, -ThresholdForLargeInt)
	ENDIF		
		
	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_END)
	INT BadSportDifference =MissionsQuit 

	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - REMOTE_BAD_SPORTER_PLAYER_DETECTED: BAD_SPORT_LIST_PLUG_PULLED_HEIST set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- MPPLY_HEIST_CHEAT_START = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_START)) NET_NL()
	NET_PRINT(" 			- MPPLY_HEIST_CHEAT_END = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_END)) NET_NL()
	NET_PRINT(" 			- MPPLY_HEIST_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_QUIT)) NET_NL()
	
	#ENDIF
	
	IF HAS_PLAYER_BAD_SPORTED(BAD_SPORT_LIST_PLUG_PULLED_HEIST)
	
		BadSportDifference -= GET_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_QUIT)
	
	
		SET_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_QUIT, MissionsQuit)
		
	NET_PRINT(" 			- MPPLY_HEIST_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_HEIST_CHEAT_QUIT)) NET_NL()
	NET_PRINT(" 			- MissionsQuit = ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- BadSportDifference = ")NET_PRINT_INT(BadSportDifference) NET_NL()
		
					

		
		IF bIgnoreLast = FALSE
		
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - BAD_SPORT happened!! *** BAD_SPORT_LIST_PLUG_PULLED_HEIST *** INCREASE MPPLY_OVERALL_BADSPORT By ")NET_PRINT_INT(BadSportDifference)NET_NL()
			#ENDIF
		
			SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_HEIST, TRUE)
			MPGlobalsHud.DisplayBAD_SPORTMessage[BAD_SPORT_LIST_PLUG_PULLED_HEIST] = TRUE
//			FLOAT fNewValue 
//			fNewValue= (TO_FLOAT(MissionsQuit)-GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT))
			INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, TO_FLOAT(BadSportDifference))
		ENDIF
				
	
		
	
	ENDIF

ENDPROC


PROC RUN_FMEVENT_BAD_SPORT_CHECK(BOOL bIgnoreLast)
	///Works out how many missions you've started. Compares with Over (Valid ending of a mission) and quits(already counted BAD_SPORTing exits)
		
	INT ThresholdForLargeInt = THRESHOLD_FOR_LARGE_INT_BADSPORT
	IF GET_MP_INT_PLAYER_STAT(MPPLY_FMEVN_CHEAT_START) > ThresholdForLargeInt
	AND GET_MP_INT_PLAYER_STAT(MPPLY_FMEVN_CHEAT_END) > ThresholdForLargeInt
		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_FMEVN_CHEAT_TALLY)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_FMEVN_CHEAT_START, -ThresholdForLargeInt)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_FMEVN_CHEAT_END, -ThresholdForLargeInt)
	ENDIF		
		
	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_FMEVN_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_FMEVN_CHEAT_END)
	INT BadSportDifference =MissionsQuit 

	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - REMOTE_BAD_SPORTER_PLAYER_DETECTED: BAD_SPORT_LIST_PLUG_PULLED_FMEVN set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- MPPLY_FMEVN_CHEAT_START = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_FMEVN_CHEAT_START)) NET_NL()
	NET_PRINT(" 			- MPPLY_FMEVN_CHEAT_END = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_FMEVN_CHEAT_END)) NET_NL()
	NET_PRINT("				- MPPLY_FMEVN_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_FMEVN_CHEAT_QUIT)) NET_NL()
	
	#ENDIF
	
	IF HAS_PLAYER_BAD_SPORTED(BAD_SPORT_LIST_PLUG_PULLED_FMEVENT)
	
		BadSportDifference -= GET_MP_INT_PLAYER_STAT(MPPLY_FMEVN_CHEAT_QUIT)
	
	
		SET_MP_INT_PLAYER_STAT(MPPLY_FMEVN_CHEAT_QUIT, MissionsQuit)
		
	NET_PRINT(" 			- MPPLY_FMEVN_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_FMEVN_CHEAT_QUIT)) NET_NL()
	NET_PRINT(" 			- MissionsQuit = ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- BadSportDifference = ")NET_PRINT_INT(BadSportDifference) NET_NL()
		
					

		
		IF bIgnoreLast = FALSE
		
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - BAD_SPORT happened!! *** BAD_SPORT_LIST_PLUG_PULLED_FMEVENT *** INCREASE MPPLY_OVERALL_BADSPORT By ")NET_PRINT_INT(BadSportDifference)
			NET_PRINT(" Last Event was ")NET_PRINT(GET_FMMC_BIT_SET_NAME_FOR_LOGS(g_i_BadsportstartedEvent))
			NET_PRINT(" int ")NET_PRINT_INT(g_i_BadsportstartedEvent)NET_NL()
			
			NET_NL()
			#ENDIF
			

			

			PLAYSTATS_AWARD_BAD_SPORT(g_i_BadsportstartedEvent)
		
			SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_FMEVENT, TRUE)
			MPGlobalsHud.DisplayBAD_SPORTMessage[BAD_SPORT_LIST_PLUG_PULLED_FMEVENT] = TRUE
//			FLOAT fNewValue 
//			fNewValue= (TO_FLOAT(MissionsQuit)-GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT))
			INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, TO_FLOAT(BadSportDifference))
		ENDIF
				
	
		
	
	ENDIF

ENDPROC


PROC RUN_PARACHUTE_BAD_SPORT_CHECK(BOOL bIgnoreLast)
	///Works out how many missions you've started. Compares with Over (Valid ending of a mission) and quits(already counted BAD_SPORTing exits)
		
	INT ThresholdForLargeInt = THRESHOLD_FOR_LARGE_INT_BADSPORT
	IF GET_MP_INT_PLAYER_STAT(MPPLY_PARA_CHEAT_START) > ThresholdForLargeInt
	AND GET_MP_INT_PLAYER_STAT(MPPLY_PARA_CHEAT_END) > ThresholdForLargeInt
		INCREMENT_MP_INT_PLAYER_STAT(MPPLY_PARA_CHEAT_TALLY)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_PARA_CHEAT_START, -ThresholdForLargeInt)
		INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_PARA_CHEAT_END, -ThresholdForLargeInt)
	ENDIF		
		
	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_PARA_CHEAT_START) - GET_MP_INT_PLAYER_STAT(MPPLY_PARA_CHEAT_END)
	INT BadSportDifference =MissionsQuit 

	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - REMOTE_BAD_SPORTER_PLAYER_DETECTED: BAD_SPORT_LIST_PLUG_PULLED_PARA set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- MPPLY_PARA_CHEAT_START = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_PARA_CHEAT_START)) NET_NL()
	NET_PRINT(" 			- MPPLY_PARA_CHEAT_END = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_PARA_CHEAT_END)) NET_NL()
	NET_PRINT(" 			- MPPLY_PARA_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_PARA_CHEAT_QUIT)) NET_NL()
	
	#ENDIF
	
	IF HAS_PLAYER_BAD_SPORTED(BAD_SPORT_LIST_PLUG_PULLED_PARACHUTE)
	
		BadSportDifference -= GET_MP_INT_PLAYER_STAT(MPPLY_PARA_CHEAT_QUIT)
	
	
		SET_MP_INT_PLAYER_STAT(MPPLY_PARA_CHEAT_QUIT, MissionsQuit)
		
	NET_PRINT(" 			- MPPLY_PARA_CHEAT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_PARA_CHEAT_QUIT)) NET_NL()
	NET_PRINT(" 			- MissionsQuit = ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- BadSportDifference = ")NET_PRINT_INT(BadSportDifference) NET_NL()
		
					
		
		IF bIgnoreLast = FALSE
		
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - BAD_SPORT happened!! *** BAD_SPORT_LIST_PLUG_PULLED_PARACHUTE *** INCREASE MPPLY_OVERALL_BADSPORT By ")NET_PRINT_INT(BadSportDifference)NET_NL()
			#ENDIF

		
			SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_PARACHUTE, TRUE)
			MPGlobalsHud.DisplayBAD_SPORTMessage[BAD_SPORT_LIST_PLUG_PULLED_PARACHUTE] = TRUE
//			FLOAT fNewValue 
//			fNewValue= (TO_FLOAT(MissionsQuit)-GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT))
			INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, TO_FLOAT(BadSportDifference))
		ENDIF
				
	
		
	
	ENDIF

ENDPROC



PROC RUN_VOTED_OUT_BAD_SPORT_CHECK()
	///Works out how many missions you've started. Compares with Over (Valid ending of a mission) and quits(already counted BAD_SPORTing exits)
		
		
	INT MissionsQuit = GET_MP_INT_PLAYER_STAT(MPPLY_VOTED_OUT) - GET_MP_INT_PLAYER_STAT(MPPLY_VOTED_OUT_DELTA)
//	INT BadSportDifference =MissionsQuit 

	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - REMOTE_BAD_SPORTER_PLAYER_DETECTED: BAD_SPORT_LIST_VOTED_OUT_OF_SESSION set to ")NET_PRINT_INT(MissionsQuit) NET_NL()
	NET_PRINT(" 			- MPPLY_VOTED_OUT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_VOTED_OUT)) NET_NL()
	NET_PRINT(" 			- MPPLY_VOTED_OUT_DELTA = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_VOTED_OUT_DELTA)) NET_NL()
	NET_PRINT(" 			- MPPLY_VOTED_OUT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_VOTED_OUT_QUIT)) NET_NL()
	
	#ENDIF
	
	
	IF HAS_PLAYER_BAD_SPORTED(BAD_SPORT_LIST_VOTED_OUT_OF_SESSION)
	
//		BadSportDifference -= GET_MP_INT_PLAYER_STAT(MPPLY_VOTED_OUT_QUIT)
		
		SET_MP_INT_PLAYER_STAT(MPPLY_VOTED_OUT_QUIT, MissionsQuit)
		
		#IF IS_DEBUG_BUILD
		NET_PRINT(" 			- MPPLY_VOTED_OUT_QUIT = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_VOTED_OUT_QUIT)) NET_NL()
		NET_PRINT(" 			- MissionsQuit = ")NET_PRINT_INT(MissionsQuit) NET_NL()
//		NET_PRINT(" 			- BadSportDifference = ")NET_PRINT_INT(BadSportDifference) NET_NL()
		#ENDIF
		
		SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_VOTED_OUT_OF_SESSION, TRUE)
		
		MPGlobalsHud.DisplayBAD_SPORTMessage[BAD_SPORT_LIST_VOTED_OUT_OF_SESSION] = TRUE
		
		// Punishments for quitting custody early.
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("[BCCHEAT] - BAD_SPORT_handler - set this BAD_SPORT happened: BAD_SPORT_LIST_VOTED_OUT_OF_SESSION.")NET_NL()
		#ENDIF
		
		
//		INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, TO_FLOAT(BadSportDifference))
		
		
	ENDIF

ENDPROC





PROC IGNORE_BAD_SPORTING_CHECK()

	IF SHOULD_IGNORE_BAD_SPORT_RATING()
	
	
	
		SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_MINIGAME, FALSE)
		MPGlobalsHud.DisplayBAD_SPORTMessage[BAD_SPORT_LIST_PLUG_PULLED_MINIGAME] = FALSE

		SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_DM, FALSE)
		MPGlobalsHud.DisplayBAD_SPORTMessage[BAD_SPORT_LIST_PLUG_PULLED_DM] = FALSE

		SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_RACE, FALSE)
		MPGlobalsHud.DisplayBAD_SPORTMessage[BAD_SPORT_LIST_PLUG_PULLED_RACE] = FALSE

		SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_PLUG_PULLED_MC_MISSION, FALSE)
		MPGlobalsHud.DisplayBAD_SPORTMessage[BAD_SPORT_LIST_PLUG_PULLED_MC_MISSION] = FALSE

		SET_BAD_SPORT_HAPPENED_THIS_MISSION(BAD_SPORT_LIST_VOTED_OUT_OF_SESSION, FALSE)
		MPGlobalsHud.DisplayBAD_SPORTMessage[BAD_SPORT_LIST_VOTED_OUT_OF_SESSION] = FALSE

		
//		SET_MP_INT_CHARACTER_STAT(MP_STAT_TIMES_BAD_SPORTED_SCRIPT, 0, 0)
//		SET_MP_INT_CHARACTER_STAT(MP_STAT_TIMES_BAD_SPORTED_SCRIPT, 0, 1)
//		SET_MP_INT_CHARACTER_STAT(MP_STAT_TIMES_BAD_SPORTED_SCRIPT, 0, 2)
//		SET_MP_INT_CHARACTER_STAT(MP_STAT_TIMES_BAD_SPORTED_SCRIPT, 0, 3)
//		SET_MP_INT_CHARACTER_STAT(MP_STAT_TIMES_BAD_SPORTED_SCRIPT, 0, 4)
		SET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, 0.0)
		
		SET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM, 0)

		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("IGNORE_BAD_SPORTING - called ")
		#ENDIF
	ENDIF
	
ENDPROC

PROC CHECK_BADSPORT_TICKER(INT AmountBefore)

	INT Bitset = GET_MP_INT_PLAYER_STAT(MPPLY_BADSPORT_MESSAGE)

	IF (AmountBefore < 10 AND GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT) >= 10 AND IS_BIT_SET(Bitset, 0) = FALSE)
	OR (AmountBefore < 20 AND GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT) >= 20 AND IS_BIT_SET(Bitset, 1) = FALSE)
	OR (AmountBefore < 30 AND GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT) >= 30 AND IS_BIT_SET(Bitset, 2) = FALSE)
	OR (AmountBefore < 40 AND GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT) >= 40 AND IS_BIT_SET(Bitset, 3) = FALSE)
	
	
		
		IF AmountBefore < 10 AND GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT) >= 10
			SET_BIT(Bitset, 0)
		ENDIF
		IF AmountBefore < 20 AND GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT) >= 20
			SET_BIT(Bitset, 1)
		ENDIF
		IF AmountBefore < 30 AND GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT) >= 30
			SET_BIT(Bitset, 2)
		ENDIF
		IF AmountBefore < 40 AND GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT) >= 40
			SET_BIT(Bitset, 3)
		ENDIF
		
		SET_MP_INT_PLAYER_STAT(MPPLY_BADSPORT_MESSAGE, Bitset)
		
	
//		SCRIPT_EVENT_DATA_TICKER_MESSAGE TickerEventData
//		TickerEventData.TickerEvent = TICKER_EVENT_BAD_SPORT_MILESTONE
//		TickerEventData.dataInt = FLOOR(GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT))
//		BROADCAST_TICKER_EVENT(TickerEventData, ALL_PLAYERS(TRUE))		

		PRINT_TICKER("HUD_BADRISE")

	ENDIF

ENDPROC

PROC SYNC_BADSPORT_LAST_FRAME_VARIABLES()

	NET_NL()NET_PRINT("SYNC_BADSPORT_LAST_FRAME_VARIABLES - called ")
	
	ALLOW_EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()
	EXPENSIVE_UPDATE_ALL_HEAVILY_ACCESSED_STATS()

	g_Bad_Sport_FRIENDLY_LastCheck = g_MPPLY_FRIENDLY
	g_Bad_Sport_OFFENSIVE_LANGUAGE_LastCheck = g_MPPLY_OFFENSIVE_LANGUAGE
	g_Bad_Sport_GRIEFING_LastCheck = g_MPPLY_GRIEFING
	g_BAD_SPORT_HELPFUL_LastCheck = g_MPPLY_HELPFUL
	g_Bad_Sport_OFFENSIVE_TAGPLATE_LastCheck = g_MPPLY_OFFENSIVE_TAGPLATE
	g_Bad_Sport_OFFENSIVE_UGC_LastCheck= g_MPPLY_OFFENSIVE_UGC
	g_Bad_Sport_VC_ANNOYING_LastCheck = GET_MP_INT_PLAYER_STAT(MPPLY_VC_ANNOYINGME)
	
	#IF IS_DEBUG_BUILD
	NET_NL()NET_PRINT("[BCCHEAT] SYNC_BADSPORT_LAST_FRAME_VARIABLES - g_MPPLY_FRIENDLY = ")NET_PRINT_INT(g_MPPLY_FRIENDLY)
	NET_NL()NET_PRINT("[BCCHEAT] SYNC_BADSPORT_LAST_FRAME_VARIABLES - g_MPPLY_OFFENSIVE_LANGUAGE = ")NET_PRINT_INT(g_MPPLY_OFFENSIVE_LANGUAGE)
	NET_NL()NET_PRINT("[BCCHEAT] SYNC_BADSPORT_LAST_FRAME_VARIABLES - g_MPPLY_GRIEFING = ")NET_PRINT_INT(g_MPPLY_GRIEFING)
	NET_NL()NET_PRINT("[BCCHEAT] SYNC_BADSPORT_LAST_FRAME_VARIABLES - g_MPPLY_HELPFUL = ")NET_PRINT_INT(g_MPPLY_HELPFUL)
	NET_NL()NET_PRINT("[BCCHEAT] SYNC_BADSPORT_LAST_FRAME_VARIABLES - g_MPPLY_OFFENSIVE_TAGPLATE = ")NET_PRINT_INT(g_MPPLY_OFFENSIVE_TAGPLATE)
	NET_NL()NET_PRINT("[BCCHEAT] SYNC_BADSPORT_LAST_FRAME_VARIABLES - g_MPPLY_OFFENSIVE_UGC = ")NET_PRINT_INT(g_MPPLY_OFFENSIVE_UGC)
	NET_NL()NET_PRINT("[BCCHEAT] SYNC_BADSPORT_LAST_FRAME_VARIABLES - MPPLY_VC_ANNOYINGME = ")NET_PRINT_INT(g_Bad_Sport_VC_ANNOYING_LastCheck)
	#ENDIF

ENDPROC

FUNC BOOL IS_PLAYER_IN_PROCESS_OF_LAUNCHING_CORONA_JOB()
  IF IS_TRANSITION_SESSION_LAUNCHING()
  OR IS_PLAYER_ACTIVE_IN_CORONA()
    RETURN TRUE
  ENDIF
  RETURN FALSE
ENDFUNC



PROC WORK_OUT_DROPOUT_RATE()


	INT TotalEndRate 
	TotalEndRate += GET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_END)
	TotalEndRate += GET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_END)
	TotalEndRate += GET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_END)
	TotalEndRate += GET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_END)

	INT TotalStartRate
	TotalStartRate += GET_MP_INT_PLAYER_STAT(MPPLY_MC_CHEAT_START)
	TotalStartRate += GET_MP_INT_PLAYER_STAT(MPPLY_MGAME_CHEAT_START)
	TotalStartRate += GET_MP_INT_PLAYER_STAT(MPPLY_RACE_CHEAT_START)
	TotalStartRate += GET_MP_INT_PLAYER_STAT(MPPLY_DM_CHEAT_START)

	FLOAT DropoutRate = TO_FLOAT(TotalEndRate)/TO_FLOAT(TotalStartRate)
	
	
	SET_MP_FLOAT_PLAYER_STAT(MPPLY_DROPOUTRATE, DropoutRate )


ENDPROC



PROC RUN_START_OF_GAME_BAD_SPORT_CHECK(BOOL& HasBeenDoneOnce, MP_GAMEMODE agamemode)

	///Works out how many Custody's you've started. Compares with Over (Valid ending of a custody) and quits(already counted BAD_SPORTing exits)
		
	IF IS_PLAYER_ON_ANY_FM_JOB(PLAYER_ID(), FALSE)
		EXIT
	ENDIF
	
	IF HAVE_STATS_LOADED()
		IF  IS_PLAYER_IN_PROCESS_OF_LAUNCHING_CORONA_JOB() = FALSE

			IF HasBeenDoneOnce = FALSE	
				
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[BCCHEAT] RUN_START_OF_GAME_BAD_SPORT_CHECK - called ")
				NET_NL()NET_PRINT("		-  NETWORK_PLAYER_IS_CHEATER = ")NET_PRINT_BOOL(NETWORK_PLAYER_IS_CHEATER())
				NET_NL()NET_PRINT("		-  GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_CHEATER) = ")NET_PRINT_BOOL(GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_CHEATER))
				NET_NL()NET_PRINT("		-  NETWORK_PLAYER_IS_BADSPORT = ")NET_PRINT_BOOL(NETWORK_PLAYER_IS_BADSPORT())
				NET_NL()NET_PRINT("		-  GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_BAD_SPORT) = ")NET_PRINT_BOOL(GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_BAD_SPORT))
				NET_NL()NET_PRINT("		-  GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM))
				NET_NL()NET_PRINT("		-  GET_MP_INT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT) = ")NET_PRINT_FLOAT(GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT))
				
				#ENDIF
				
				FLOAT AmountBefore = GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT)
				
				
				
				
				
				
//				IF NETWORK_PLAYER_IS_BADSPORT()
//				AND GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_BAD_SPORT) = FALSE
//					INCREMENT_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM)
//					NET_NL()NET_PRINT("		-  INCREMENT_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM))
//
//				ENDIF
				
				RUN_GENERAL_PLUG_PULLING_COUNT_AT_BOOT()
				RUN_START_OF_GAME_MESSAGES()
				
				
				IF GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_BAD_SPORT) = TRUE
				AND NETWORK_PLAYER_IS_BADSPORT() = FALSE
					
					FLOAT PunishmentFactor = GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT)
					INT TimersABadSport  = GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM)
					
					NET_NL()NET_PRINT("[BCCHEAT] RUN_START_OF_GAME_BAD_SPORT_CHECK - CALCULATE PunishmentFactor = ")NET_PRINT_FLOAT(PunishmentFactor)

					
					IF TimersABadSport = 0 
						PunishmentFactor += 0
					ELIF TimersABadSport = 1
						PunishmentFactor += 5
					ELIF TimersABadSport = 2
						PunishmentFactor += 10
					ELIF TimersABadSport = 3
						PunishmentFactor += 15
					ELIF TimersABadSport = 4
						PunishmentFactor += 20
					ELSE
						PunishmentFactor += 25
					ENDIF
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[BCCHEAT] RUN_START_OF_GAME_BAD_SPORT_CHECK - CALCULATE TimersABadSport = ")NET_PRINT_INT(TimersABadSport)
					NET_NL()NET_PRINT("[BCCHEAT] RUN_START_OF_GAME_BAD_SPORT_CHECK - APPLYING PunishmentFactor = ")NET_PRINT_FLOAT(PunishmentFactor)
					
					#ENDIF
					
					SET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, PunishmentFactor)
					
				ENDIF
				
				
				
				IF NETWORK_PLAYER_IS_BADSPORT()
					IF GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_BAD_SPORT) = FALSE
						SET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT, GET_TODAYS_DATE())
//						INCREMENT_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM)
//						NET_NL()NET_PRINT("		-  INCREMENT_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM))
						
					ENDIF
				ELSE
					IF GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_BAD_SPORT)
						SET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_BAD_SPORT, FALSE)
						
						STRUCT_STAT_DATE EmptyDate
						SET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT,EmptyDate)
						
					ENDIF
				ENDIF
				
				BOOL bIgnoreLast =  IGNORE_LAST_DISCREPENCY()
		
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("		-  bIgnoreLast = ")NET_PRINT_BOOL(bIgnoreLast)
				#ENDIF
				
				IF agamemode = GAMEMODE_FM
					RUN_DM_BAD_SPORT_CHECK(bIgnoreLast)
					RUN_RACE_BAD_SPORT_CHECK(bIgnoreLast)
					RUN_MC_BAD_SPORT_CHECK(bIgnoreLast)
					RUN_MINIGAME_BAD_SPORT_CHECK(bIgnoreLast)
					RUN_CAPTURE_BAD_SPORT_CHECK(bIgnoreLast)
					RUN_SURVIVAL_BAD_SPORT_CHECK(bIgnoreLast)
					RUN_LTS_BAD_SPORT_CHECK(bIgnoreLast)
					RUN_PARACHUTE_BAD_SPORT_CHECK(bIgnoreLast)
					RUN_HEIST_BAD_SPORT_CHECK(bIgnoreLast)
					RUN_FMEVENT_BAD_SPORT_CHECK(bIgnoreLast)
					RUN_VOTED_OUT_BAD_SPORT_CHECK()
					IGNORE_BAD_SPORTING_CHECK()
					
					WORK_OUT_DROPOUT_RATE()
					
				ENDIF
				
				IF AmountBefore <> GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT)
				AND NOT IS_DATE_EMPTY(GET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT))
					CHECK_BADSPORT_TICKER(FLOOR(AmountBefore))
					
				ENDIF
			
				AmountBefore = GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT)
		
				
				HasBeenDoneOnce = TRUE
				
			ENDIF
		ENDIF
	ENDIF

ENDPROC



PROC RUN_BAD_SPORT_DEGREDATION(SCRIPT_TIMER& aTimer, INT& NumberOfDays, FLOAT& AmountBefore)
	
//	INT TimePerPoint = (g_sMPTunables.BadSportResetMinutes*60000)
//	INT TimesaBadSport = ((GET_MP_INT_LABEL_STAT(MPPLY_BECAME_A_BADSPORT))^g_sMPTunables.BadSportTimeExponential)
//	IF GET_MP_INT_LABEL_STAT(MPPLY_BECAME_A_BADSPORT) = 1
//		
//	ENDIF
//	
//	INT milliseconds =
//
//	IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, milliseconds) 
//		//Reduce BAD_SPORTer level
//		IF GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT) > 0
//			#IF IS_DEBUG_BUILD
//			NET_NL()NET_PRINT("RUN_BAD_SPORT_DEGREDATION: has passed time, reducing the player and Chars BAD_SPORT level by ")NET_PRINT_FLOAT(g_sMPTunables.BadSportAmountToForgiveBy)
//			#ENDIF
//			DECREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, g_sMPTunables.BadSportAmountToForgiveBy)
//		ENDIF
//	ENDIF


	
	IF GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT) > 0
		IF NOT NETWORK_PLAYER_IS_BADSPORT()
		AND NOT NETWORK_PLAYER_IS_CHEATER()
			INT milliseconds = (g_sMPTunables.BadSportResetMinutes*60000)
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, milliseconds, TRUE) 
				//Reduce Cheater level
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: Not a bad sport or cheater forgiveness has passed time, reducing the player and Chars Bad Sport level by ")NET_PRINT_FLOAT(g_sMPTunables.BadSportAmountToForgiveBy)
				#ENDIF
				DECREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, g_sMPTunables.BadSportAmountToForgiveBy)
			ENDIF
		ENDIF
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
		
		IF g_Debug_ApplyDateToShaveOffBecomingBadSport
		AND IS_DATE_EMPTY(g_Debug_DateToShaveOffBecomingBadSport)  = FALSE
		 	
			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: g_Debug_DateToShaveOffBecomingBadSport Adjust MPPLY_BECAME_BADSPORT_DT ")
			
			STRUCT_STAT_DATE BadSportDateStart = GET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT)
			
			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: BEFORE BadSportDateStart = ")NET_PRINT_DATE(BadSportDateStart)
			
			BadSportDateStart.Year -= g_Debug_DateToShaveOffBecomingBadSport.Year
			BadSportDateStart.Month -= g_Debug_DateToShaveOffBecomingBadSport.Month
			BadSportDateStart.Day -= g_Debug_DateToShaveOffBecomingBadSport.Day
			BadSportDateStart.Hour -= g_Debug_DateToShaveOffBecomingBadSport.Hour
			BadSportDateStart.Minute -= g_Debug_DateToShaveOffBecomingBadSport.Minute
			BadSportDateStart.Seconds -= g_Debug_DateToShaveOffBecomingBadSport.Seconds
			BadSportDateStart.Milliseconds -= g_Debug_DateToShaveOffBecomingBadSport.Milliseconds
			
			SET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT, BadSportDateStart)
			
			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: g_Debug_DateToShaveOffBecomingBadSport = ")NET_PRINT_DATE(g_Debug_DateToShaveOffBecomingBadSport)
			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: AFTER BadSportDateStart = ")NET_PRINT_DATE(BadSportDateStart)
			
			g_BadSportRatingLastFrame = 0 //reset to show the ticker again
			g_Debug_ApplyDateToShaveOffBecomingBadSport = FALSE
			SET_DATE_EMPTY(g_Debug_DateToShaveOffBecomingBadSport)
			
		ENDIF
	
	#ENDIF


	
	 
	IF GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT) > 0
	AND NOT IS_DATE_EMPTY(GET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT))
	
		//2^(n-1)
//		NumberOfDays = g_sMPTunables.BadSportTimeExponential^(GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_A_BADSPORT)-1)

//		NumberOfDays = POW(TO_FLOAT(g_sMPTunables.BadSportTimeExponential),TO_FLOAT(GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_A_BADSPORT)))

//		FLOAT Expon = TO_FLOAT(g_sMPTunables.BadSportTimeExponential)
//		FLOAT Number = TO_FLOAT(GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM))
//		FLOAT Power = POW(Expon,Number)
//		NumberOfDays = FLOOR(Power)

		IF GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) = 0 
			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: MPPLY_BECAME_BADSPORT_NUM) = 0 NumberOfDays = ")NET_PRINT_INT(NumberOfDays)
		ENDIF
		IF GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) = 1 
		
		
			#IF IS_DEBUG_BUILD
			IF NumberOfDays <> g_sMPTunables.BadSportNumDays_1stOffense
				NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: MPPLY_BECAME_BADSPORT_NUM) = 1 NumberOfDays = ")NET_PRINT_INT(g_sMPTunables.BadSportNumDays_1stOffense)
			ENDIF
			#ENDIF
			
			NumberOfDays = g_sMPTunables.BadSportNumDays_1stOffense
		

		ENDIF
		IF GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) = 2 
		
		
			#IF IS_DEBUG_BUILD
			IF NumberOfDays <> g_sMPTunables.BadSportNumDays_2ndOffense
				NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: MPPLY_BECAME_BADSPORT_NUM) = 2 NumberOfDays = ")NET_PRINT_INT(g_sMPTunables.BadSportNumDays_2ndOffense)
			ENDIF
			#ENDIF
		
			NumberOfDays = g_sMPTunables.BadSportNumDays_2ndOffense
			
		ENDIF
		IF GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) = 3 

		
			#IF IS_DEBUG_BUILD
			IF NumberOfDays <> g_sMPTunables.BadSportNumDays_3rdOffense
				NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: MPPLY_BECAME_BADSPORT_NUM) = 3 NumberOfDays = ")NET_PRINT_INT(g_sMPTunables.BadSportNumDays_3rdOffense)
			ENDIF
			#ENDIF
		
			NumberOfDays = g_sMPTunables.BadSportNumDays_3rdOffense
			
		ENDIF
		IF GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) = 4 
	
		
			#IF IS_DEBUG_BUILD
			IF NumberOfDays <> g_sMPTunables.BadSportNumDays_4thOffense
				NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: MPPLY_BECAME_BADSPORT_NUM) = 4 NumberOfDays = ")NET_PRINT_INT(g_sMPTunables.BadSportNumDays_4thOffense)
			ENDIF
			#ENDIF
			
			NumberOfDays = g_sMPTunables.BadSportNumDays_4thOffense		
			
		ENDIF
		IF GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) = 5 

			
		
			#IF IS_DEBUG_BUILD
			IF NumberOfDays <> g_sMPTunables.BadSportNumDays_5thOffense
				NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: MPPLY_BECAME_BADSPORT_NUM) = 5 NumberOfDays = ")NET_PRINT_INT(g_sMPTunables.BadSportNumDays_5thOffense)
			ENDIF
			#ENDIF
		
			NumberOfDays = g_sMPTunables.BadSportNumDays_5thOffense		
		ENDIF
		IF GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) = 6 

				
		
			#IF IS_DEBUG_BUILD
			IF NumberOfDays <> g_sMPTunables.BadSportNumDays_6thOffense
				NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: MPPLY_BECAME_BADSPORT_NUM) = 6 NumberOfDays = ")NET_PRINT_INT(g_sMPTunables.BadSportNumDays_6thOffense)
			ENDIF
			#ENDIF
			
			NumberOfDays = g_sMPTunables.BadSportNumDays_6thOffense			
					
		ENDIF
		IF GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) = 7 


			#IF IS_DEBUG_BUILD
			IF NumberOfDays <> g_sMPTunables.BadSportNumDays_7thOffense
				NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: MPPLY_BECAME_BADSPORT_NUM) = 7 NumberOfDays = ")NET_PRINT_INT(g_sMPTunables.BadSportNumDays_7thOffense)
			ENDIF
			#ENDIF
				
			NumberOfDays = g_sMPTunables.BadSportNumDays_7thOffense			
					
		ENDIF
		IF GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) = 8 

			
		
			#IF IS_DEBUG_BUILD
			IF NumberOfDays <> g_sMPTunables.BadSportNumDays_8thOffense
				NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: MPPLY_BECAME_BADSPORT_NUM) = 8 NumberOfDays = ")NET_PRINT_INT(g_sMPTunables.BadSportNumDays_8thOffense)
			ENDIF
			#ENDIF
					
			NumberOfDays = g_sMPTunables.BadSportNumDays_8thOffense				
					
		ENDIF
		IF GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) = 9 


			#IF IS_DEBUG_BUILD
			IF NumberOfDays <> g_sMPTunables.BadSportNumDays_9thOffense
				NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: MPPLY_BECAME_BADSPORT_NUM) = 9 NumberOfDays = ")NET_PRINT_INT(g_sMPTunables.BadSportNumDays_9thOffense)
			ENDIF
			#ENDIF
			
			NumberOfDays = g_sMPTunables.BadSportNumDays_9thOffense		
					
		ENDIF
		IF GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) >= 10 

			
		
			#IF IS_DEBUG_BUILD
			IF NumberOfDays <> g_sMPTunables.BadSportNumDays_10thOffense
				NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: MPPLY_BECAME_BADSPORT_NUM) >= 10 NumberOfDays = ")NET_PRINT_INT(g_sMPTunables.BadSportNumDays_10thOffense)
			ENDIF
			#ENDIF
					
			NumberOfDays = g_sMPTunables.BadSportNumDays_10thOffense				
					
		ENDIF

		IF HAS_NET_TIMER_EXPIRED(aTimer, 1000) 
		ENDIF
		
		IF GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) > 0
			IF IS_DATE_EMPTY(GET_MP_DATE_PLAYER_STAT(MPPLY_BADSPORT_END))

				STRUCT_STAT_DATE BadsportEnd = GET_CURRENT_TIMEOFDAY_AS_STAT()
				TIMEOFDAY TOD_BadsportEnd = GET_OFFSET_TIMEOFDAY_IN_DAYS(BadsportEnd, NumberOfDays)
				BadsportEnd = CONVERT_TIMEOFDAY_TO_STATDATE(TOD_BadsportEnd)
				
				NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: Become a bad sport and saving the MPPLY_BADSPORT_END ")
				NET_PRINT("[BCCHEAT]Year = ")NET_PRINT_INT(BadsportEnd.Year)NET_PRINT(" ")
				NET_PRINT("[BCCHEAT]Month = ")NET_PRINT_INT(BadsportEnd.Month)NET_PRINT(" ")
				NET_PRINT("[BCCHEAT]Day = ")NET_PRINT_INT(BadsportEnd.Day)NET_PRINT(" ")
				NET_PRINT("[BCCHEAT]Hour = ")NET_PRINT_INT(BadsportEnd.Hour)NET_PRINT(" ")
				NET_PRINT("[BCCHEAT]Minute = ")NET_PRINT_INT(BadsportEnd.Minute)NET_PRINT(" ")
				NET_PRINT("[BCCHEAT]Seconds = ")NET_PRINT_INT(BadsportEnd.Seconds)NET_PRINT(" ")

				
				SET_MP_DATE_PLAYER_STAT(MPPLY_BADSPORT_END, BadsportEnd)
			ENDIF
			
			
			IF HAS_DAYS_PASSED_BETWEEN_DATES(GET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT), GET_TODAYS_DATE(), NumberOfDays)
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: has passed time, reducing the player and Chars BAD_SPORT to 0 ")
				NET_NL()NET_PRINT("[BCCHEAT] - NumberOfDays = ")NET_PRINT_INT(NumberOfDays)
	//			NET_NL()NET_PRINT("			- g_sMPTunables.BadSportTimeExponential = ")NET_PRINT_INT(g_sMPTunables.BadSportTimeExponential)
				NET_NL()NET_PRINT("[BCCHEAT]- GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM))
	//			NET_NL()NET_PRINT("			- Power = ")NET_PRINT_FLOAT(Power)
				
				STRUCT_STAT_DATE aDate =  GET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT)
				NET_NL()NET_PRINT("[BCCHEAT]- BECAME_BADSPORT_DT.Year = ")NET_PRINT_INT(aDate.Year)
				NET_NL()NET_PRINT("[BCCHEAT]- BECAME_BADSPORT_DT.Month = ")NET_PRINT_INT(aDate.Month)
				NET_NL()NET_PRINT("[BCCHEAT]- BECAME_BADSPORT_DT.Day = ")NET_PRINT_INT(aDate.Day)
				NET_NL()NET_PRINT("[BCCHEAT]- BECAME_BADSPORT_DT.Hour = ")NET_PRINT_INT(aDate.Hour)
				NET_NL()NET_PRINT("[BCCHEAT]- BECAME_BADSPORT_DT.Minute = ")NET_PRINT_INT(aDate.Minute)
				NET_NL()NET_PRINT("[BCCHEAT]- BECAME_BADSPORT_DT.Seconds = ")NET_PRINT_INT(aDate.Seconds)
				
				STRUCT_STAT_DATE TodaysDate =  GET_TODAYS_DATE()
				NET_NL()NET_PRINT("[BCCHEAT]- TodaysDate.Year = ")NET_PRINT_INT(TodaysDate.Year)
				NET_NL()NET_PRINT("[BCCHEAT]- TodaysDate.Month = ")NET_PRINT_INT(TodaysDate.Month)
				NET_NL()NET_PRINT("[BCCHEAT]- TodaysDate.Day = ")NET_PRINT_INT(TodaysDate.Day)
				NET_NL()NET_PRINT("[BCCHEAT]- TodaysDate.Hour = ")NET_PRINT_INT(TodaysDate.Hour)
				NET_NL()NET_PRINT("[BCCHEAT]- TodaysDate.Minute = ")NET_PRINT_INT(TodaysDate.Minute)
				NET_NL()NET_PRINT("[BCCHEAT]- TodaysDate.Seconds = ")NET_PRINT_INT(TodaysDate.Seconds)
				
				#ENDIF
				SET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, 0)
			ENDIF
		ENDIF
	ENDIF

	
	

	IF g_Bad_Sport_GRIEFING_LastCheck <> g_MPPLY_GRIEFING
		
		FLOAT Scale = TO_FLOAT(g_MPPLY_GRIEFING) - TO_FLOAT(g_Bad_Sport_GRIEFING_LastCheck)

		
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: GRIEFING Level are different ")
		NET_NL()NET_PRINT("			- g_BAD_SPORT_GRIEFING_LastCheck = ")NET_PRINT_INT(g_BAD_SPORT_GRIEFING_LastCheck)
		NET_NL()NET_PRINT("			- g_MPPLY_GRIEFING = ")NET_PRINT_INT(g_MPPLY_GRIEFING)
		NET_NL()NET_PRINT("			- GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH) = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH))
		NET_NL()NET_PRINT("			- Scale = ")NET_PRINT_FLOAT(Scale)
		
		#ENDIF
		
		
		IF Scale >= GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH)  //16
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: GRIEFING Level has changed, increasing BAD_SPORT level by ")NET_PRINT_FLOAT(g_sMPTunables.CheatAmountToPunishBy_Griefing)
			#ENDIF
			IF 	g_sMPTunables.bBSG_1		= TRUE
				INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, g_sMPTunables.CheatAmountToPunishBy_Griefing)
			ENDIF
		ENDIF
		g_Bad_Sport_GRIEFING_LastCheck = g_MPPLY_GRIEFING
	ENDIF
	
	IF g_Bad_Sport_OFFENSIVE_LANGUAGE_LastCheck <> g_MPPLY_OFFENSIVE_LANGUAGE
		
		FLOAT Scale = TO_FLOAT(g_MPPLY_OFFENSIVE_LANGUAGE) - TO_FLOAT(g_Bad_Sport_OFFENSIVE_LANGUAGE_LastCheck)
		
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: OFFENSIVE_LANGUAGE Level are different ")
		NET_NL()NET_PRINT("			- g_BAD_SPORT_OFFENSIVE_LANGUAGE_LastCheck = ")NET_PRINT_INT(g_BAD_SPORT_OFFENSIVE_LANGUAGE_LastCheck)
		NET_NL()NET_PRINT("			- g_MPPLY_OFFENSIVE_LANGUAGE = ")NET_PRINT_INT(g_MPPLY_OFFENSIVE_LANGUAGE)
		NET_NL()NET_PRINT("			- GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH) = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH))
		NET_NL()NET_PRINT("			- Scale = ")NET_PRINT_FLOAT(Scale)
		
		#ENDIF
		
		
		IF Scale >= GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH)  //16
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: OFFENSIVE_LANGUAGE Level has changed, increasing BAD_SPORT level by ")NET_PRINT_FLOAT(g_sMPTunables.CheatAmountToPunishBy_OffensiveLanguage)
			#ENDIF
			INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, g_sMPTunables.CheatAmountToPunishBy_OffensiveLanguage)
			
		ENDIF
		g_Bad_Sport_OFFENSIVE_LANGUAGE_LastCheck = g_MPPLY_OFFENSIVE_LANGUAGE
	ENDIF
	
	
	
	IF g_Bad_Sport_FRIENDLY_LastCheck <> g_MPPLY_FRIENDLY
		
		FLOAT Scale = TO_FLOAT(g_MPPLY_FRIENDLY) - TO_FLOAT(g_Bad_Sport_FRIENDLY_LastCheck)
		
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: FRIENDLY Level are different ")
		NET_NL()NET_PRINT("			- g_BAD_SPORT_FRIENDLY_LastCheck = ")NET_PRINT_INT(g_BAD_SPORT_FRIENDLY_LastCheck)
		NET_NL()NET_PRINT("			- g_MPPLY_FRIENDLY = ")NET_PRINT_INT(g_MPPLY_FRIENDLY)
		NET_NL()NET_PRINT("			- GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH) = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH))
		NET_NL()NET_PRINT("			- Scale = ")NET_PRINT_FLOAT(Scale)
		#ENDIF

		
		IF Scale >= GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH)  //16
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: FRIENDLY Level has changed, reducing BAD_SPORT level by ")NET_PRINT_FLOAT(g_sMPTunables.CheatAmountToForgiveBy_Friendly)
			#ENDIF
			IF GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT) >=  g_sMPTunables.CheatAmountToForgiveBy_Friendly
				DECREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, g_sMPTunables.CheatAmountToForgiveBy_Friendly)
			ELSE
				SET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, 0)
			ENDIF
		ENDIF
		g_Bad_Sport_FRIENDLY_LastCheck = g_MPPLY_FRIENDLY
	ENDIF
	
	
	IF g_BAD_SPORT_HELPFUL_LastCheck <> g_MPPLY_HELPFUL
		
		FLOAT Scale = TO_FLOAT(g_MPPLY_HELPFUL) - TO_FLOAT(g_BAD_SPORT_HELPFUL_LastCheck)
	
		
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: HELPFUL Level are different ")
		NET_NL()NET_PRINT("			- g_BAD_SPORT_HELPFUL_LastCheck = ")NET_PRINT_INT(g_BAD_SPORT_HELPFUL_LastCheck)
		NET_NL()NET_PRINT("			- g_MPPLY_HELPFUL = ")NET_PRINT_INT(g_MPPLY_HELPFUL)
		NET_NL()NET_PRINT("			- GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH) = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH))
		NET_NL()NET_PRINT("			- Scale = ")NET_PRINT_FLOAT(Scale)
		#ENDIF
		
		IF Scale >= GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH)  //16
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: HELPFUL Level has changed, reducing BAD_SPORT level by ")NET_PRINT_FLOAT( g_sMPTunables.CheatAmountToForgiveBy_Helpful)
			#ENDIF
			IF GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT) >=  g_sMPTunables.CheatAmountToForgiveBy_Helpful
				DECREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, g_sMPTunables.CheatAmountToForgiveBy_Helpful)
			ELSE
				SET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, 0)
			ENDIF
			
		ENDIF
		g_BAD_SPORT_HELPFUL_LastCheck = g_MPPLY_HELPFUL
	ENDIF
	
		
	IF g_BAD_SPORT_OFFENSIVE_TAGPLATE_LastCheck <> g_MPPLY_OFFENSIVE_TAGPLATE

		FLOAT Scale = TO_FLOAT(g_MPPLY_OFFENSIVE_TAGPLATE) - TO_FLOAT(g_Bad_Sport_OFFENSIVE_TAGPLATE_LastCheck)
		
		
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: OFFENSIVE_TAGPLATE Level are different ")
		NET_NL()NET_PRINT("			- g_BAD_SPORT_OFFENSIVE_TAGPLATE_LastCheck = ")NET_PRINT_INT(g_BAD_SPORT_OFFENSIVE_TAGPLATE_LastCheck)
		NET_NL()NET_PRINT("			- g_MPPLY_OFFENSIVE_TAGPLATE = ")NET_PRINT_INT(g_MPPLY_OFFENSIVE_TAGPLATE)
		NET_NL()NET_PRINT("			- GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH) = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH))
		NET_NL()NET_PRINT("			- Scale = ")NET_PRINT_FLOAT(Scale)
		
		#ENDIF
		
		
		
		IF Scale >= GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH)
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: OFFENSIVE_TAGPLATE Level has changed, increasing cheat level by ")NET_PRINT_FLOAT(g_sMPTunables.cheatAmountToPunishBy_OffensiveTag)
			#ENDIF
			INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, g_sMPTunables.cheatAmountToPunishBy_OffensiveTag)
			
		ENDIF
		g_BAD_SPORT_OFFENSIVE_TAGPLATE_LastCheck = g_MPPLY_OFFENSIVE_TAGPLATE
	ENDIF
//	


	IF g_BAD_SPORT_OFFENSIVE_UGC_LastCheck <> g_MPPLY_OFFENSIVE_UGC

		FLOAT Scale = TO_FLOAT(g_MPPLY_OFFENSIVE_UGC) - TO_FLOAT(g_BAD_SPORT_OFFENSIVE_UGC_LastCheck)
		
		
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: MPPLY_OFFENSIVE_UGC Level are different ")
		NET_NL()NET_PRINT("			- g_BAD_SPORT_OFFENSIVE_UGC_LastCheck = ")NET_PRINT_INT(g_BAD_SPORT_OFFENSIVE_UGC_LastCheck)
		NET_NL()NET_PRINT("			- g_MPPLY_OFFENSIVE_UGC = ")NET_PRINT_INT(g_MPPLY_OFFENSIVE_UGC)
		NET_NL()NET_PRINT("			- GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH) = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH))
		NET_NL()NET_PRINT("			- Scale = ")NET_PRINT_FLOAT(Scale)
		
		#ENDIF
		
		
		IF Scale >= GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH)
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: OFFENSIVE_UGC Level has changed, increasing cheat level by ")NET_PRINT_FLOAT(g_sMPTunables.cheatAmountToPunishBy_OffensiveUGC)
			#ENDIF
			INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, g_sMPTunables.cheatAmountToPunishBy_OffensiveUGC)
			
		ENDIF
		g_Bad_Sport_OFFENSIVE_UGC_LastCheck = g_MPPLY_OFFENSIVE_UGC
	ENDIF
	
	
	
	IF g_Bad_Sport_VC_ANNOYING_LastCheck <> GET_MP_INT_PLAYER_STAT(MPPLY_VC_ANNOYINGME)

		FLOAT Scale = TO_FLOAT(GET_MP_INT_PLAYER_STAT(MPPLY_VC_ANNOYINGME)) - TO_FLOAT(g_Bad_Sport_VC_ANNOYING_LastCheck)
		
		
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: MPPLY_VC_ANNOYINGME Level are different ")
		NET_NL()NET_PRINT("			- g_Bad_Sport_VC_ANNOYING_LastCheck = ")NET_PRINT_INT(g_Bad_Sport_VC_ANNOYING_LastCheck)
		NET_NL()NET_PRINT("			- GET_MP_INT_PLAYER_STAT(MPPLY_VC_ANNOYINGME) = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_VC_ANNOYINGME))
		NET_NL()NET_PRINT("			- GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH) = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH))
		NET_NL()NET_PRINT("			- Scale = ")NET_PRINT_FLOAT(Scale)
		
		#ENDIF
		
		
		IF Scale >= GET_MP_INT_PLAYER_STAT(MPPLY_REPORT_STRENGTH)
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_DEGREDATION: VC_ANNOYINGME Level has changed, increasing cheat level by ")NET_PRINT_FLOAT(g_sMPTunables.cheatAmountToPunishBy_VC_Annoy)
			#ENDIF
			IF 	g_sMPTunables.bBSAM_1		= TRUE
				INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, g_sMPTunables.cheatAmountToPunishBy_VC_Annoy)
			ENDIF
		ENDIF
		g_Bad_Sport_VC_ANNOYING_LastCheck = GET_MP_INT_PLAYER_STAT(MPPLY_VC_ANNOYINGME)
	ENDIF
	
	
	IF AmountBefore <> GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT)
	AND NOT IS_DATE_EMPTY(GET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT))

		CHECK_BADSPORT_TICKER(FLOOR(AmountBefore))
		
	ENDIF
	AmountBefore = GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT)
	
	
	
	
	
ENDPROC





PROC RUN_BAD_SPORT_PHONECALL()

	
	STRUCT_STAT_DATE EmptyDate
	STRUCT_STAT_DATE StartDate

	SWITCH g_BadSportStages
		CASE 0 
			IF HAS_IMPORTANT_STATS_LOADED()
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					IF NETWORK_PLAYER_IS_BADSPORT()
					#IF IS_DEBUG_BUILD
					OR g_iBadSportPlayPhoneCall
					#ENDIF
						#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_PHONECALL - called ")
						#ENDIF
						
						IF GET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_BAD_SPORT) = FALSE
							INCREMENT_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM)
							NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_PHONECALL - INCREMENT_MP_INT_PLAYER_STAT(MPPLY_BECAME_BADSPORT_NUM) ")
						ENDIF
						
						IF IS_DATE_EMPTY(GET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT))	

							SET_MP_DATE_PLAYER_STAT(MPPLY_BADSPORT_END, EmptyDate)
						
							SET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT, GET_TODAYS_DATE())
							NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_PHONECALL - SET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT, GET_TODAYS_DATE()) ")
						ENDIF
						
						GET_POSIX_TIME(StartDate.Year, StartDate.Month, StartDate.Day, StartDate.Hour, StartDate.Minute, StartDate.Seconds)
						SET_MP_DATE_PLAYER_STAT(MPPLY_BADSPORT_START,StartDate)

						SET_MP_BOOL_PLAYER_STAT(MPPLY_WAS_I_BAD_SPORT, TRUE)
						
			
						REQUEST_SAVE(SSR_REASON_BAD_SPORT_UPDATE, STAT_SAVETYPE_CHEATER_CHANGE)
						#IF IS_DEBUG_BUILD
						g_iBadSportPlayPhoneCall = FALSE
						#ENDIF
						g_BadSportStages++
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		
		CASE 1
			
			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_PHONECALL - CASE 1 called ")
			
			IF (IS_SAVING_HAVING_TROUBLE() 
			OR HAS_ENTERED_OFFLINE_SAVE_FM())
			
				IF IS_SAVING_HAVING_TROUBLE()
					NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_PHONECALL - IS_SAVING_HAVING_TROUBLE() = ")NET_PRINT_BOOL(IS_SAVING_HAVING_TROUBLE())
				ENDIF
				IF HAS_ENTERED_OFFLINE_SAVE_FM()
					NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_PHONECALL - HAS_ENTERED_OFFLINE_SAVE_FM() = ")NET_PRINT_BOOL(HAS_ENTERED_OFFLINE_SAVE_FM())
				ENDIF
				
				g_BadSportStages++
			ENDIF
			
			IF g_BadSportStages = 1
				IF IS_SAVE_IN_PROGRESS() = FALSE

					SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD, 1 )
					SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0), FALSE) 

					IF IS_CHARACTER_MALE(GET_ACTIVE_CHARACTER_SLOT())
						set_ped_comp_item_current_mp(PLAYER_PED_ID(), comp_type_berd, BERD_FMM_0_0)
					ELSE
						set_ped_comp_item_current_mp(PLAYER_PED_ID(), comp_type_berd, BERD_FMF_0_0)
					ENDIF
					

					
					DISPLAY_LOCAL_TICKER_FOR_BAD_SPORTER_IMMEDIATE()
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_IS_A_BAD_SPORT, ALL_PLAYERS())
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
				
					g_BadSportStages++
				
				ENDIF
			ENDIF
		BREAK
		
		
		CASE 2 
		
			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_PHONECALL - CASE 2 called ")

		
			//IF (Request_MP_Comms_Txtmsg(CHAR_MP_FM_CONTACT, "MPBAD_1"))
                 g_BadSportStages++
   			// ENDIF
			 
		BREAK
		
		CASE 3 
		
//			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_PHONECALL - CASE 3 called ")

		
		
			IF HAVE_STATS_LOADED()
				IF NOT NETWORK_PLAYER_IS_BADSPORT()
				#IF IS_DEBUG_BUILD
				OR g_iBadSportPlayPhoneCall
				#ENDIF


					SET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT,EmptyDate)
					
					REQUEST_SAVE(SSR_REASON_BAD_SPORT_UPDATE, STAT_SAVETYPE_CHEATER_CHANGE)
					#IF IS_DEBUG_BUILD
					g_iBadSportPlayPhoneCall = FALSE
					#ENDIF
					g_BadSportStages++
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			
			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_PHONECALL - CASE 4 called ")
			
			IF (IS_SAVING_HAVING_TROUBLE() 
			OR HAS_ENTERED_OFFLINE_SAVE_FM())
			
				IF IS_SAVING_HAVING_TROUBLE()
					NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_PHONECALL - IS_SAVING_HAVING_TROUBLE() = ")NET_PRINT_BOOL(IS_SAVING_HAVING_TROUBLE())
				ENDIF
				IF HAS_ENTERED_OFFLINE_SAVE_FM()
					NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_PHONECALL - HAS_ENTERED_OFFLINE_SAVE_FM() = ")NET_PRINT_BOOL(HAS_ENTERED_OFFLINE_SAVE_FM())
				ENDIF
				
				g_BadSportStages++
			ENDIF
			
			IF g_BadSportStages = 4
				IF IS_SAVE_IN_PROGRESS() = FALSE

					PED_VARIATION_STRUCT ClothesConstruct 
					GET_STORED_MP_PLAYER_COMPONENTS(ClothesConstruct)
					SET_PED_VARIATIONS(PLAYER_PED_ID(),ClothesConstruct)

					Immediately_Export_Player_Headshot()
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_IS_NO_LONGER_A_BAD_SPORT, ALL_PLAYERS())
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
				
					g_BadSportStages++
				
				ENDIF
			ENDIF
		BREAK
		
		CASE 5 
		
			NET_NL()NET_PRINT("[BCCHEAT] RUN_BAD_SPORT_PHONECALL - CASE 5 called ")
		
			//IF (Request_MP_Comms_Txtmsg(CHAR_MP_FM_CONTACT, "MPBAD_2"))
                 g_BadSportStages = 0
   			// ENDIF
		
		
		BREAK
	
	
	ENDSWITCH
	
	



ENDPROC




//
//
//PROC DISPLAY_TICKER_FOR_BAD_SPORTER(PLAYER_INDEX BAD_SPORTingplayerID)
//	IF IS_NET_PLAYER_OK(BAD_SPORTingplayerID, FALSE, TRUE)
//	
//		
//		BEGIN_TEXT_COMMAND_THEFEED_POST("HUD_BADSPYES")
//			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(BAD_SPORTingplayerID, DEFAULT, TRUE))
//			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_PLAYER_NAME(BAD_SPORTingplayerID))			
//		END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)		
//		
//	
//	ENDIF
//ENDPROC
//
//PROC DISPLAY_TICKER_FOR_BAD_SPORTER_END(PLAYER_INDEX BAD_SPORTingplayerID)
//	IF IS_NET_PLAYER_OK(BAD_SPORTingplayerID, FALSE, TRUE)
//	
//		BEGIN_TEXT_COMMAND_THEFEED_POST("HUD_BADSPNO")
//			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(GET_PLAYER_HUD_COLOUR(BAD_SPORTingplayerID, DEFAULT, TRUE))
//			ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(GET_PLAYER_NAME(BAD_SPORTingplayerID))			
//		END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)		
//		
//	
//	ENDIF
//ENDPROC





PROC DISPLAY_LOCAL_TICKER_FOR_BAD_SPORTER_TIME_SMALL(INT MillisecondsLeft, SCRIPT_TIMER& aTimer, INT MessageRepeatTime)
	IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, MessageRepeatTime)
	
		NET_NL()NET_PRINT("[BCCHEAT] DISPLAY_LOCAL_TICKER_FOR_BAD_SPORTER_TIME_SMALL: MillisecondsLeft = ")NET_PRINT_INT(MillisecondsLeft)

	
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			BEGIN_TEXT_COMMAND_THEFEED_POST("HUD_BADSPTIME")
				ADD_TEXT_COMPONENT_SUBSTRING_TIME(MillisecondsLeft, TIME_FORMAT_DAYS|TIME_FORMAT_HOURS|TIME_FORMAT_MINUTES|TEXT_FORMAT_SHOW_UNIT_DIVIDERS_AS_LETTERS)
			END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)		
		ENDIF
	ENDIF
ENDPROC

PROC DISPLAY_LOCAL_TICKER_FOR_BAD_SPORTER_TIME_LARGE(INT YearsLeft,INT MonthsLeft,INT DaysLeft, SCRIPT_TIMER& aTimer, INT MessageRepeatTime)
	IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, MessageRepeatTime)
	
	
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			BEGIN_TEXT_COMMAND_THEFEED_POST("HUD_BADSPTIME_L")
				ADD_TEXT_COMPONENT_INTEGER(YearsLeft)
				ADD_TEXT_COMPONENT_INTEGER(MonthsLeft)
				ADD_TEXT_COMPONENT_INTEGER(DaysLeft)
			END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)		
		ENDIF
	ENDIF
ENDPROC




//FUNC INT TIME_UNTIL_PLAYER_IS_FORGIVEN_BAD_SPORT(SCRIPT_TIMER& ResetRunningTimer)
//
//	INT ResultMilliseconds
//	
//	FLOAT fPointsToLoseUntilForgiven = (GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT)-TO_FLOAT(g_sMPTunables.NotBadSportThreshold))
//
//	FLOAT fTimeMillisecondsPerPoint = (TO_FLOAT(g_sMPTunables.BadSportResetMinutes)*60*1000)/(g_sMPTunables.BadSportAmountToForgiveBy)
//	
//	INT iTimeMillisecondsPerPoint = FLOOR(fTimeMillisecondsPerPoint)
//	INT iPointsToLoseUntilForgiven = FLOOR(fPointsToLoseUntilForgiven)
//	
//	ResultMilliseconds = (iPointsToLoseUntilForgiven*iTimeMillisecondsPerPoint)
//	
//	ResultMilliseconds -= GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), ResetRunningTimer.Timer)
//
//
//
//	RETURN ResultMilliseconds
//
//ENDFUNC

FUNC INT MILLISECONDS_UNTIL_PLAYER_IS_FORGIVEN_BAD_SPORT(INT iNumDays)

	INT ResultMilliseconds
	
	STRUCT_STAT_DATE BecameBadSport = GET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT)
	
	TIMEOFDAY Today = GET_CURRENT_TIMEOFDAY_POSIX()
	TIMEOFDAY ForgivenDate = GET_OFFSET_TIMEOFDAY_IN_DAYS(BecameBadSport, iNumDays)
	
	
	STRUCT_STAT_DATE TheDifference
	
	GET_DIFFERENCE_BETWEEN_TIMEOFDAYS(Today, ForgivenDate, TheDifference.Seconds, TheDifference.Minute, TheDifference.Hour, TheDifference.Day, TheDifference.Month, TheDifference.Year)



	ResultMilliseconds = CONVERT_OFFSET_TO_MILLISECONDS(TheDifference)

	
//	NET_NL()NET_PRINT("MILLISECONDS_UNTIL_PLAYER_IS_FORGIVEN_BAD_SPORT ")
//	TEXT_LABEL_63 TlToday 
//	GET_TIMEOFDAY_STRING(Today, TlToday, TRUE, TRUE)
//	NET_NL()NET_PRINT(" 		- Today = ")NET_PRINT(TlToday)
//	TEXT_LABEL_63 TlForgive 
//	GET_TIMEOFDAY_STRING(ForgivenDate, TlForgive, TRUE, TRUE)
//	NET_NL()NET_PRINT(" 		- ForgivenDate = ")NET_PRINT(TlForgive)
////	NET_NL()NET_PRINT(" 		- Times  = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_A_BADSPORT))
//	NET_NL()NET_PRINT("			- iNumDays = ") NET_PRINT_INT(iNumDays)
//	NET_NL()NET_PRINT("			- TheDifference.Year = ") NET_PRINT_INT(TheDifference.Year)
//	NET_NL()NET_PRINT("			- TheDifference.Month = ") NET_PRINT_INT(TheDifference.Month)
//	NET_NL()NET_PRINT("			- TheDifference.Day = ") NET_PRINT_INT(TheDifference.Day)
//	NET_NL()NET_PRINT("			- TheDifference.Hour = ") NET_PRINT_INT(TheDifference.Hour)
//	NET_NL()NET_PRINT("			- TheDifference.Minute = ") NET_PRINT_INT(TheDifference.Minute)
//	NET_NL()NET_PRINT("			- TheDifference.Seconds = ") NET_PRINT_INT(TheDifference.Seconds)
//	NET_NL()NET_PRINT("			- ResultMilliseconds = ")NET_PRINT_INT(ResultMilliseconds)
//
//	NET_NL()NET_PRINT("			- BecameBadSport.Year = ") NET_PRINT_INT(BecameBadSport.Year)
//	NET_NL()NET_PRINT("			- BecameBadSport.Month = ") NET_PRINT_INT(BecameBadSport.Month)
//	NET_NL()NET_PRINT("			- BecameBadSport.Day = ") NET_PRINT_INT(BecameBadSport.Day)
//	NET_NL()NET_PRINT("			- BecameBadSport.Hour = ") NET_PRINT_INT(BecameBadSport.Hour)
//	NET_NL()NET_PRINT("			- BecameBadSport.Minute = ") NET_PRINT_INT(BecameBadSport.Minute)
//	NET_NL()NET_PRINT("			- BecameBadSport.Seconds = ") NET_PRINT_INT(BecameBadSport.Seconds)
	
	
	RETURN ResultMilliseconds

ENDFUNC

PROC MHDMY_UNTIL_PLAYER_IS_FORGIVEN_BAD_SPORT(INT iNumDays, INT& Year, INT& Month, INT&Days, INT&Hours, INT&Minutes)

	
	
	TIMEOFDAY Today = GET_CURRENT_TIMEOFDAY_POSIX()
	TIMEOFDAY ForgivenDate = GET_OFFSET_TIMEOFDAY_IN_DAYS(GET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT), iNumDays)
	
	
	STRUCT_STAT_DATE TheDifference
	
	GET_DIFFERENCE_BETWEEN_TIMEOFDAYS(Today, ForgivenDate, TheDifference.Seconds, TheDifference.Minute, TheDifference.Hour, TheDifference.Day, TheDifference.Month, TheDifference.Year)

	Year = TheDifference.Year
	Month = TheDifference.Month
	Days = TheDifference.Day
	Hours = TheDifference.Hour
	Minutes = TheDifference.Minute
	
	
//	NET_NL()NET_PRINT("MHDMY_UNTIL_PLAYER_IS_FORGIVEN_BAD_SPORT ")
//	TEXT_LABEL_63 TlToday 
//	GET_TIMEOFDAY_STRING(Today, TlToday, TRUE, TRUE)
//	NET_NL()NET_PRINT(" 		- Today = ")NET_PRINT(TlToday)
//	TEXT_LABEL_63 TlForgive 
//	GET_TIMEOFDAY_STRING(ForgivenDate, TlForgive, TRUE, TRUE)
//	NET_NL()NET_PRINT(" 		- ForgivenDate = ")NET_PRINT(TlForgive)
//	NET_NL()NET_PRINT(" 		- Times  = ")NET_PRINT_INT(GET_MP_INT_PLAYER_STAT(MPPLY_BECAME_A_BADSPORT))
//	NET_NL()NET_PRINT("			- TheDifference.Year = ") NET_PRINT_INT(TheDifference.Year)
//	NET_NL()NET_PRINT("			- TheDifference.Month = ") NET_PRINT_INT(TheDifference.Month)
//	NET_NL()NET_PRINT("			- TheDifference.Day = ") NET_PRINT_INT(TheDifference.Day)
//	NET_NL()NET_PRINT("			- TheDifference.Hour = ") NET_PRINT_INT(TheDifference.Hour)
//	NET_NL()NET_PRINT("			- TheDifference.Minute = ") NET_PRINT_INT(TheDifference.Minute)
//	NET_NL()NET_PRINT("			- TheDifference.Seconds = ") NET_PRINT_INT(TheDifference.Seconds)
	
	
	
ENDPROC

PROC DISPLAY_BADSPORT_TICKER(INT NumberOfDays, INT RepeatTimer)

	IF NumberOfDays < 28
		DISPLAY_LOCAL_TICKER_FOR_BAD_SPORTER_TIME_SMALL(MILLISECONDS_UNTIL_PLAYER_IS_FORGIVEN_BAD_SPORT(NumberOfDays), MPGlobalsHud.BadSportMessageTimer, RepeatTimer)
	ELSE
		STRUCT_STAT_DATE LargeDate
		MHDMY_UNTIL_PLAYER_IS_FORGIVEN_BAD_SPORT(NumberOfDays, LargeDate.Year, LargeDate.Month, LargeDate.Day, LargeDate.Hour, LargeDate.Minute)
		DISPLAY_LOCAL_TICKER_FOR_BAD_SPORTER_TIME_LARGE( LargeDate.Year, LargeDate.Month, LargeDate.Day, MPGlobalsHud.BadSportMessageTimer, RepeatTimer)
	ENDIF

ENDPROC




PROC RUN_BAD_SPORT_POOL_MESSAGES(SCRIPT_TIMER& ResetRunningTimer, INT NumberOfDays)

	
	SWITCH g_BadSportMessageStages
	
		CASE 0
				IF NETWORK_PLAYER_IS_BADSPORT()
				AND NOT IS_DATE_EMPTY(GET_MP_DATE_PLAYER_STAT(MPPLY_BECAME_BADSPORT_DT))
				
					IF HAS_NET_TIMER_EXPIRED(ResetRunningTimer, 1000)
					ENDIF
					
					
					g_BadSportMessageStages = 1
				ENDIF
		BREAK
	
		CASE 1

			DISPLAY_BADSPORT_TICKER(NumberOfDays, 500000)
			
			IF NETWORK_PLAYER_IS_BADSPORT()
			 
						
				IF g_BadSportRatingLastFrame <> GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT)
					DISPLAY_BADSPORT_TICKER(NumberOfDays, -1)
				ENDIF
			ENDIF
			
			g_BadSportRatingLastFrame = GET_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT)
			
			IF NOT NETWORK_PLAYER_IS_BADSPORT()
				
				
				g_BadSportMessageStages = 0
				
			ENDIF
		BREAK
		

	ENDSWITCH



ENDPROC
























































////////////////////////////////////////
////	FORCE	Kicking		////////////////
////////////////////////////////////////



#IF IS_DEBUG_BUILD
	PROC WRITE_PLAYER_LOBBY_BD_WIDGET(INT& NumberVotesToKick)

		INT I, J
		TEXT_LABEL_31 KickText
		
		START_WIDGET_GROUP("KICK PLAYERS")
			ADD_WIDGET_BOOL("TURN ON WIDGET PRINTS", g_BKickingRunWIDGETPRINTS)
			ADD_WIDGET_BOOL("TURN ON WIDGETS WORKINGS ", g_BKickingRunWidgetCHeck)
			ADD_WIDGET_INT_SLIDER("Numbers Needed for Kicking", g_NumberNeededForKicking, -NUM_NETWORK_PLAYERS, NUM_NETWORK_PLAYERS, 1)
			ADD_WIDGET_INT_SLIDER("Numbers Of Votes Cast", NumberVotesToKick, -NUM_NETWORK_PLAYERS, NUM_NETWORK_PLAYERS, 1)
			FOR I = 0 TO NUM_NETWORK_PLAYERS-1
				KickText = "Kick Player "	
				KickText += I
				ADD_WIDGET_BOOL(KickText, GlobalplayerBD_Kicking[NATIVE_TO_INT(PLAYER_ID())].bHaveIVotedToKickPlayer[I]) 
			ENDFOR
			
			FOR I = 0 TO NUM_NETWORK_PLAYERS-1
				KickText = "Voted to Kick me "	
				KickText += I
				ADD_WIDGET_BOOL(KickText, GlobalplayerBD_Kicking[I].bHaveIVotedToKickPlayer[NATIVE_TO_INT(PLAYER_ID())]) 
			ENDFOR
			
			START_WIDGET_GROUP("Who's Kicked Who?")
				FOR I = 0 TO NUM_NETWORK_PLAYERS-1
					FOR J = 0 TO NUM_NETWORK_PLAYERS-1
						KickText = "Player "	
						KickText += I
						KickText += " has kicked "	
						KickText += J
						ADD_WIDGET_BOOL(KickText, GlobalplayerBD_Kicking[I].bHaveIVotedToKickPlayer[J]) 
					ENDFOR
				ENDFOR
				
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		
		
		
	
	ENDPROC
	
	PROC SETUP_KICKING_WIDGETS(INT& iNumberOfVotes)


		WRITE_PLAYER_LOBBY_BD_WIDGET(iNumberOfVotes)


	ENDPROC

#ENDIF


FUNC BOOL HOLD_UP_FORCE_KICK_FOR_EVENT(INT KickedPlayerNum)
	
	IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(INT_TO_NATIVE(PLAYER_INDEX, KickedPlayerNum))
	OR IS_PLAYER_PERMANENT_PARTICIPANT_TO_ANY_EVENT(INT_TO_NATIVE(PLAYER_INDEX, KickedPlayerNum))
		RETURN TRUE
	ENDIF
	
	
	RETURN FALSE
		
ENDFUNC


/// PURPOSE:
///    Need to scan through all players in .bHaveIVotedToKickPlayer to make sure at least one person is in their team. If on a DM. 
/// PARAMS:
///    KickedPlayerNum - Who's possibly being kicked. 
/// RETURNS:
///    
FUNC BOOL IS_FORCE_KICK_VALID(INT KickedPlayerNum)

	
	BOOL Result = FALSE
	INT I

	
	PLAYER_INDEX KickedPlayerIndex = INT_TO_NATIVE(PLAYER_INDEX, KickedPlayerNum)
	INT KickedPlayerTeam = -1
	
	IF KickedPlayerNum > 0
		KickedPlayerTeam = GET_PLAYER_TEAM(KickedPlayerIndex)
	ENDIF
	
	PLAYER_INDEX CheckingPlayerIndex
	INT CheckingPlayerTeam
	
	IF HOLD_UP_FORCE_KICK_FOR_EVENT(KickedPlayerNum) 
		#IF IS_DEBUG_BUILD
		IF KickedPlayerIndex != INVALID_PLAYER_INDEX()
			NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: IS_FORCE_KICK_VALID = FALSE for ")NET_PRINT(GET_PLAYER_NAME(KickedPlayerIndex))NET_PRINT(" player index ")NET_PRINT_INT(KickedPlayerNum)NET_NL()
		ENDIF
		NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: IS_FORCE_KICK_VALID = FALSE - IS_PLAYER_CRITICAL_TO_ANY_EVENT(KickedPlayerIndex) = ")NET_PRINT_BOOL(IS_PLAYER_CRITICAL_TO_ANY_EVENT(KickedPlayerIndex) )
		NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: IS_FORCE_KICK_VALID = FALSE - FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(KickedPlayerIndex) = ")NET_PRINT_BOOL(FM_EVENT_IS_PLAYER_PERMANENT_PARTICIPANT_IN_FM_EVENT(KickedPlayerIndex))
		#ENDIF
		RETURN FALSE
	ENDIF
	
	
	IF g_b_On_Deathmatch = FALSE 

		NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: IS_FORCE_KICK_VALID = TRUE - g_b_On_Deathmatch = ")NET_PRINT_BOOL(g_b_On_Deathmatch)
		RETURN TRUE
	ENDIF
	
	
	
	IF KickedPlayerNum < 0
		NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: IS_FORCE_KICK_VALID = TRUE - KickedPlayerTeam < 0 = ")NET_PRINT_INT(KickedPlayerTeam)
		RETURN TRUE
	ENDIF
	
	
	
	FOR I = 0 TO NUM_NETWORK_PLAYERS-1
		
		CheckingPlayerIndex = INT_TO_NATIVE(PLAYER_INDEX, I)
		
		IF IS_NET_PLAYER_OK(CheckingPlayerIndex, FALSE, TRUE)
		
			IF g_b_On_Deathmatch
				CheckingPlayerTeam = GET_PLAYER_TEAM(CheckingPlayerIndex)
			
				IF GlobalplayerBD_Kicking[I].bHaveIVotedToKickPlayer[KickedPlayerNum] = TRUE
				AND KickedPlayerTeam = CheckingPlayerTeam
				AND CheckingPlayerTeam > 0
					NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: Enough people on their own team have kicked the player, IS_FORCE_KICK_VALID = TRUE ")

					Result = TRUE
				ENDIF
			
			ENDIF
		ENDIF
	ENDFOR
	
	

	RETURN Result

ENDFUNC



PROC RUN_HOST_KICKING_CHECKS(PLAYER_INDEX PlayerID, INT iPlayerNum, INT& PlayerCheckIndex, BOOL bPlayerOkInternalCheck, INT& NumberVotesToKick)

	INT NumberInSession = NETWORK_GET_NUM_CONNECTED_PLAYERS()

		//How many players have voted for PlayerID to be kicked
		
		IF bPlayerOkInternalCheck
		
			IF GlobalplayerBD_Kicking[PlayerCheckIndex].bHaveIVotedToKickPlayer[iPlayerNum] = TRUE
				
				
				IF GlobalplayerBD[iPlayerNum].IsOnlineTVRockstar
				
					GlobalplayerBD_Kicking[PlayerCheckIndex].bHaveIVotedToKickPlayer[iPlayerNum] = FALSE
					#IF IS_DEBUG_BUILD
						IF g_BKickingRunWIDGETPRINTS
							NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: Player ")NET_PRINT(GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, PlayerCheckIndex)))NET_PRINT(" has voted YES to kick off the ROCKSTAR DEV ")NET_PRINT(GET_PLAYER_NAME(PlayerID))NET_PRINT(" very naughty. ")
						ENDIF
					#ENDIF
				ELSE
				
					#IF IS_DEBUG_BUILD
						IF g_BKickingRunWIDGETPRINTS
							NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: Player ")NET_PRINT(GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, PlayerCheckIndex)))NET_PRINT(" has voted YES to kick off player ")NET_PRINT(GET_PLAYER_NAME(PlayerID))
						ENDIF
					#ENDIF
					
					NumberVotesToKick++
				ENDIF
				
			ELSE
				#IF IS_DEBUG_BUILD
					IF g_BKickingRunWIDGETPRINTS
						NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: Player ")NET_PRINT(GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, PlayerCheckIndex)))NET_PRINT(" has NOT voted to kick off player ")NET_PRINT(GET_PLAYER_NAME(PlayerID))
					ENDIF
				#ENDIF
			ENDIF
						
		
		ENDIF
		

	
		PlayerCheckIndex++
		
	
		IF PlayerCheckIndex >= NUM_NETWORK_PLAYERS

			g_NumberNeededForKicking = NumberInSession-1
			FLOAT fNumPlayers = TO_FLOAT(g_NumberNeededForKicking) * g_sMPTunables.kickVotesNeededRatio
						
			g_NumberNeededForKicking = FLOOR(fNumPlayers)
		
			BOOL DoMinimumCap = TRUE
			#IF IS_DEBUG_BUILD
				IF g_BKickingRunWidgetCHeck
					DoMinimumCap = FALSE
				ENDIF
			#ENDIF
		
			IF DoMinimumCap
				IF g_NumberNeededForKicking < 2
					g_NumberNeededForKicking = 2
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF g_BKickingRunWIDGETPRINTS
					NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: g_NumberNeededForKicking ")NET_PRINT_INT(g_NumberNeededForKicking)
					NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: NumberVotesToKick ")NET_PRINT_INT(NumberVotesToKick)
				
				ENDIF
			#ENDIF
			

			IF NumberVotesToKick >= g_NumberNeededForKicking
			AND NumberVotesToKick > 0 AND g_NumberNeededForKicking > 0
				
				#IF IS_DEBUG_BUILD
					IF g_BKickingRunWIDGETPRINTS
						NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: ENOUGH TO KICK player ")NET_PRINT_INT(iPlayerNum)NET_PRINT(" ")NET_PRINT(GET_PLAYER_NAME(PlayerID))
					ENDIF
				#ENDIF
				
				
				
				
				

				IF IS_FORCE_KICK_VALID(iPlayerNum)

					IF GlobalServerBD_Kicking.bHasPlayerBeenToldTheyWillBeKicked[iPlayerNum] = FALSE
					
						#IF IS_DEBUG_BUILD
						IF g_BKickingRunWIDGETPRINTS
							NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: GlobalServerBD_Kicking.bHasPlayerBeenToldTheyWillBeKicked[iPlayerNum] = FALSE for player ")NET_PRINT(GET_PLAYER_NAME(PlayerID))
						ENDIF
						#ENDIF
					
						GlobalServerBD_Kicking.bHasPlayerBeenToldTheyWillBeKicked[iPlayerNum] = TRUE
					ENDIF
					
					IF GlobalServerBD_Kicking.bHasPlayerBeenToldTheyWillBeKicked[iPlayerNum] = TRUE
					
						#IF IS_DEBUG_BUILD
						IF g_BKickingRunWIDGETPRINTS
							NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: GlobalServerBD_Kicking.bHasPlayerBeenToldTheyWillBeKicked[iPlayerNum] = TRUE for player ")NET_PRINT(GET_PLAYER_NAME(PlayerID))
						ENDIF
						#ENDIF
										
						
							
						
						//IF GlobalplayerBD_Kicking[iPlayerNum].bAmIGoingToBeKicked = TRUE
						
						
						#IF IS_DEBUG_BUILD
//								IF g_BKickingRunWIDGETPRINTS
								NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: KICK OFF PLAYER NOW!!! ")NET_PRINT_INT(iPlayerNum)NET_PRINT(" ")NET_PRINT(GET_PLAYER_NAME(PlayerID))
//								ENDIF
						#ENDIF
						
						IF iPlayerNum != NATIVE_TO_INT(PLAYER_ID())
							INT I
							FOR I = 0 TO NUM_NETWORK_PLAYERS-1
								GlobalplayerBD_Kicking[I].bTellMeAboutPlayerBeingKicked[iPlayerNum] = TRUE
							ENDFOR
						ENDIF
						
						#IF IS_DEBUG_BUILD
//							IF g_BKickingRunWIDGETPRINTS
							NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: KICKING player ")NET_PRINT(GET_PLAYER_NAME(PlayerID))
//							ENDIF
						#ENDIF
						
						IF NETWORK_IS_SESSION_STARTED()
						
							//GlobalplayerBD_Kicking[iPlayerNum].bAmIGoingToBeKicked = TRUE
							NETWORK_SESSION_KICK_PLAYER(PlayerID)
							GlobalServerBD_Kicking.bHasPlayerBeenToldTheyWillBeKicked[iPlayerNum] = FALSE
						ENDIF
					ELSE
						NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: TRYING TO KICK player ")NET_PRINT(GET_PLAYER_NAME(PlayerID))
						NET_PRINT(" but NETWORK_IS_SESSION_STARTED = ")NET_PRINT_BOOL(NETWORK_IS_SESSION_STARTED())
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF


ENDPROC


#IF IS_DEBUG_BUILD
PROC KICK_LOCAL_PLAYER()

	IF g_Widget_KickLocalPlayer = TRUE
		IF  NETWORK_IS_SESSION_STARTED()
//			g_Private_Am_Kicked = TRUE
			INT iPlayerNum = NATIVE_TO_INT(PLAYER_ID())
			IF iPlayerNum > -1
				INT I
				FOR I = 0 TO NUM_NETWORK_PLAYERS-1
					GlobalplayerBD_Kicking[I].bTellMeAboutPlayerBeingKicked[iPlayerNum] = TRUE
				ENDFOR
				
			ENDIF
			NETWORK_SESSION_KICK_PLAYER(PLAYER_ID())
			g_Widget_KickLocalPlayer = FALSE
		ENDIF
	ENDIF

ENDPROC
#ENDIF




PROC RUN_HOST_KICKING_CHECKS_OVERALL(INT& iPlayerCheckIndexInternal, INT& iPlayerCheckIndexExternal, INT& NumberVotesToKick)
	
	BOOL bPlayerOkExternal, bPlayerOkInternal
	
	IF NETWORK_IS_HOST()
		IF COMPARE_STRINGS(g_st_LastHostName, GET_PLAYER_NAME(PLAYER_ID())) > 0
			iPlayerCheckIndexInternal = 0
			iPlayerCheckIndexExternal = 0
			NumberVotesToKick = 0
			NET_NL()NET_PRINT("[BCFORCE] RUN_HOST_KICKING_CHECKS: Resetting EVERYTHING Host has changed from ")NET_PRINT(g_st_LastHostName)NET_PRINT(" to ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
		ENDIF
		g_st_LastHostName = GET_PLAYER_NAME(PLAYER_ID())
	ENDIF

	
	PLAYER_INDEX piPlayerExternal =  INT_TO_NATIVE(PLAYER_INDEX, iPlayerCheckIndexExternal)
	IF piPlayerExternal <> INVALID_PLAYER_INDEX()	
	AND IS_NET_PLAYER_OK(piPlayerExternal, FALSE, FALSE)
		bPlayerOkExternal = TRUE
	ELSE
		GlobalServerBD_Kicking.bHasPlayerBeenToldTheyWillBeKicked[iPlayerCheckIndexExternal] = FALSE
	ENDIF
	
	PLAYER_INDEX piPlayerInternal = INT_TO_NATIVE(PLAYER_INDEX, iPlayerCheckIndexInternal)
	IF piPlayerInternal <> INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(piPlayerInternal, FALSE, FALSE)
		bPlayerOkInternal = TRUE
	ELSE
		GlobalServerBD_Kicking.bHasPlayerBeenToldTheyWillBeKicked[iPlayerCheckIndexInternal] = FALSE
	ENDIF


	IF bPlayerOkExternal
		RUN_HOST_KICKING_CHECKS(piPlayerExternal, iPlayerCheckIndexExternal, iPlayerCheckIndexInternal, bPlayerOkInternal, NumberVotesToKick)
	ELSE
		iPlayerCheckIndexInternal++
	ENDIF
	
	IF iPlayerCheckIndexInternal >= NUM_NETWORK_PLAYERS
	
		iPlayerCheckIndexExternal++
		IF iPlayerCheckIndexExternal >= NUM_NETWORK_PLAYERS
			iPlayerCheckIndexExternal = 0			
		ENDIF
		
		NumberVotesToKick = 0
		iPlayerCheckIndexInternal = 0
		
	ENDIF
	
ENDPROC




PROC RUN_KICKING_CHECKS_CLIENT(INT& iPlayerCheckIndexInternal_Client)
	
	
	IF HOLD_UP_FORCE_KICK_FOR_EVENT(NATIVE_TO_INT(PLAYER_ID())) = FALSE
		IF GlobalServerBD_Kicking.bHasPlayerBeenToldTheyWillBeKicked[NATIVE_TO_INT(PLAYER_ID())] = TRUE
			NET_NL()NET_PRINT("[BCFORCE] BC: KICK ME FROM THIS SESSION ")
			g_Private_Am_Kicked = TRUE
			GlobalPlayerBD_Kicking[NATIVE_TO_INT(PLAYER_ID())].bAmIGoingToBeKicked = TRUE
		ELSE
			GlobalPlayerBD_Kicking[NATIVE_TO_INT(PLAYER_ID())].bAmIGoingToBeKicked = FALSE
		ENDIF
	ENDIF
		

	IF INT_TO_NATIVE(PLAYER_INDEX, iPlayerCheckIndexInternal_Client) <> INVALID_PLAYER_INDEX()
	AND IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iPlayerCheckIndexInternal_Client), FALSE, FALSE)
		
		
		
		IF GlobalPlayerBD_Kicking[iPlayerCheckIndexInternal_Client].bHaveIVotedToKickPlayer[NATIVE_TO_INT(PLAYER_ID())] = TRUE
		AND GlobalPlayerBD_Kicking[NATIVE_TO_INT(PLAYER_ID())].bHaveIBeenToldAboutPlayerKickingMe[iPlayerCheckIndexInternal_Client] = FALSE
			
			NET_NL()NET_PRINT("[BCFORCE] WARN PLAYER ABOUT BEING REPORTED ")
			NET_NL()NET_PRINT("[BCFORCE] - ")NET_PRINT(GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iPlayerCheckIndexInternal_Client)))NET_PRINT(" has voted to kick me. ")
			
			STRING KickWarningMessage = "HUD_KICKWARN"
			IF HOLD_UP_FORCE_KICK_FOR_EVENT(NATIVE_TO_INT(PLAYER_ID()))
				KickWarningMessage = "HUD_KICKEVENT"
			ENDIF	
			
			BEGIN_TEXT_COMMAND_THEFEED_POST(KickWarningMessage)
			END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)	
				
			GlobalPlayerBD_Kicking[NATIVE_TO_INT(PLAYER_ID())].bHaveIBeenToldAboutPlayerKickingMe[iPlayerCheckIndexInternal_Client] = TRUE
		ENDIF
	
	
		IF NETWORK_SESSION_GET_KICK_VOTE(INT_TO_NATIVE(PLAYER_INDEX, iPlayerCheckIndexInternal_Client))	
			#IF IS_DEBUG_BUILD
				IF g_BKickingRunWIDGETPRINTS
					NET_NL()NET_PRINT("[BCFORCE] VOTED TO KICK ")
					NET_NL()NET_PRINT("[BCFORCE] 	-	Player Voting = ") NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))NET_PRINT(" ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
					NET_NL()NET_PRINT("[BCFORCE] 	-	Player Voted to kick = ") NET_PRINT_INT(iPlayerCheckIndexInternal_Client)NET_PRINT(" ")NET_PRINT(GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, iPlayerCheckIndexInternal_Client)))

				ENDIF
			#ENDIF
			GlobalPlayerBD_Kicking[NATIVE_TO_INT(PLAYER_ID())].bHaveIVotedToKickPlayer[iPlayerCheckIndexInternal_Client] = TRUE
		ELSE
			GlobalPlayerBD_Kicking[NATIVE_TO_INT(PLAYER_ID())].bHaveIVotedToKickPlayer[iPlayerCheckIndexInternal_Client] = FALSE
		ENDIF
	ENDIF
	
	iPlayerCheckIndexInternal_Client++
	IF iPlayerCheckIndexInternal_Client >= NUM_NETWORK_PLAYERS
		iPlayerCheckIndexInternal_Client = 0
		

	ENDIF


ENDPROC


PROC KICKING_BROADCAST_SETUP()
	
	INT I
	PLAYER_INDEX aplayer 
	FOR I = 0 TO NUM_NETWORK_PLAYERS-1
		aplayer = INT_TO_NATIVE(PLAYER_INDEX, I)
		IF IS_NET_PLAYER_OK(aplayer, FALSE, FALSE)
			IF NETWORK_SESSION_GET_KICK_VOTE(aplayer)	
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("[BCFORCE] KICKING_BROADCAST_SETUP - Coming back into a session and I previously wanted player index ")NET_PRINT_INT(NATIVE_TO_INT(aplayer))
				NET_PRINT(" ")NET_PRINT(GET_PLAYER_NAME(aplayer))NET_PRINT(" kicked. My New Player Id is ")NET_PRINT_INT(NATIVE_TO_INT(PLAYER_ID()))NET_NL()
				#ENDIF
				
				GlobalPlayerBD_Kicking[NATIVE_TO_INT(PLAYER_ID())].bHaveIBeenToldAboutPlayerKickingMe[I] = TRUE
				GlobalPlayerBD_Kicking[NATIVE_TO_INT(PLAYER_ID())].bHaveIVotedToKickPlayer[I] = TRUE
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC


PROC RESET_MY_KICKING_DATA()

	INT I
	FOR I = 0 TO NUM_NETWORK_PLAYERS-1
		//Did I vote to have the player in the session I'm leaving be kicked. Reset this. 
		GlobalplayerBD_Kicking[NATIVE_TO_INT(PLAYER_ID())].bHaveIVotedToKickPlayer[I] = FALSE
		
		
	ENDFOR
	
ENDPROC

PROC RESET_LEAVING_PLAYERS_KICKING_DATA(PLAYER_INDEX& iLeavingPlayerIndex)
	
	INT I
	FOR I = 0 TO NUM_NETWORK_PLAYERS-1
		GlobalplayerBD_Kicking[NATIVE_TO_INT(iLeavingPlayerIndex)].bHaveIVotedToKickPlayer[I] = FALSE
	ENDFOR
	
	//Did I vote to kick a player who is leaving the session. Reset this. 
	GlobalplayerBD_Kicking[NATIVE_TO_INT(PLAYER_ID())].bHaveIVotedToKickPlayer[NATIVE_TO_INT(iLeavingPlayerIndex)] = FALSE
ENDPROC

PROC INCREMENT_KICKED_COUNTERS()

	INCREMENT_MP_INT_PLAYER_STAT(MPPLY_VOTED_OUT)
	INCREMENT_BY_MP_FLOAT_PLAYER_STAT(MPPLY_OVERALL_BADSPORT, g_sMPTunables.cheatAmountToVoted_Out)

ENDPROC



















////////////////////////////////////////
////	Idle	Kicking	////////////////
////////////////////////////////////////


FUNC BOOL IS_IDLE_KICK_DISABLED()
	RETURN MPGlobalsStats.bDisableIdleKick
ENDFUNC

PROC SET_IDLE_KICK_DISABLED(BOOL bDisable)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL()NET_PRINT("<<<< SET_IDLE_KICK_DISABLED = ")NET_PRINT_BOOL(bDisable)NET_PRINT(" >>>>")
	#ENDIF
	MPGlobalsStats.bDisableIdleKick = bDisable
ENDPROC

FUNC BOOL IS_IDLE_KICK_CUTSCENE_CHECK_DISABLE()
	RETURN MPGlobalsStats.bDisableIdleKickCutsceneCheck
ENDFUNC

PROC SET_IDLE_KICK_CUTSCENE_CHECK_DISABLE(BOOL bDisable)
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL()NET_PRINT("<<<< SET_IDLE_KICK_DISABLE_CUTSCENES_CHECK = ")NET_PRINT_BOOL(bDisable)NET_PRINT(" >>>>")
	#ENDIF
	MPGlobalsStats.bDisableIdleKickCutsceneCheck = bDisable
ENDPROC

PROC SET_IDLE_KICK_DISABLED_THIS_FRAME()

	#IF IS_DEBUG_BUILD
	
		BOOL bPrint
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(st_IdleKickThisFramePrint, 6000)
			bPrint = TRUE
		ENDIF
	
		IF bPrint
			DEBUG_PRINTCALLSTACK()
			NET_NL()NET_PRINT("<<<< SET_IDLE_KICK_DISABLED_THIS_FRAME - called >>>>")
		ENDIF
	#ENDIF

	MPGlobalsStats.bDisableIdleKick = TRUE
ENDPROC

PROC RESET_IDLE_KICK_DISABLED_THIS_FRAME()
	MPGlobalsStats.bDisableIdleKick = FALSE
ENDPROC

FUNC BOOL IS_ANY_BUTTON_PRESSED (CONTROL_TYPE aType)
	IF GET_CONTROL_HOW_LONG_AGO(aType) < 2000
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_ANY_CONTROLS_BEING_PRESSED()

	IF IS_IDLE_KICK_DISABLED() 
		#IF IS_DEBUG_BUILD
			IF g_B_IdleKickPrints
				NET_NL()NET_PRINT("[BCIDLE] ARE_ANY_CONTROLS_BEING_PRESSED - IS_IDLE_KICK_DISABLED = TRUE ")
			ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	IF g_b_WasSavingDown 
		#IF IS_DEBUG_BUILD
		IF g_B_IdleKickPrints
			NET_NL()NET_PRINT("[BCIDLE] ARE_ANY_CONTROLS_BEING_PRESSED - g_b_WasSavingDown = TRUE ")
		ENDIF
		#ENDIF
		RETURN TRUE
	ENDIF
	
	BOOL bCutSceneActive = NETWORK_IS_IN_MP_CUTSCENE() AND NOT IS_IDLE_KICK_CUTSCENE_CHECK_DISABLE()
	
	IF (bCutSceneActive = FALSE OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP() = TRUE) 
	AND IS_TRANSITION_ACTIVE() = FALSE
	AND IS_ANY_BUTTON_PRESSED(PLAYER_CONTROL) = FALSE
	AND IS_ANY_BUTTON_PRESSED(FRONTEND_CONTROL) = FALSE
	AND IS_ANY_BUTTON_PRESSED(CAMERA_CONTROL) = FALSE
	AND IS_PLAYER_IN_VEH_WITH_BUDDY(PLAYER_ID()) = FALSE
	AND IS_SOCIAL_CLUB_ACTIVE() = FALSE
	AND IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_RUN_PASSIVE_CUT) = FALSE
	AND PLM_IS_IN_CONSTRAINED_MODE() = FALSE
	
		#IF IS_DEBUG_BUILD
		IF g_B_IdleKickPrints
			NET_NL()NET_PRINT("[BCIDLE] ARE_ANY_CONTROLS_BEING_PRESSED = FALSE ")
		ENDIF
		#ENDIF
		RETURN FALSE
	ENDIF

	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_IdleKickHeartbeat")
		IF GET_FRAME_COUNT() % 10 = 0
			NET_NL()NET_PRINT("[BCIDLE] ARE_ANY_CONTROLS_BEING_PRESSED = TRUE ")
			NET_NL()NET_PRINT("[BCIDLE] NETWORK_IS_IN_MP_CUTSCENE = ")NET_PRINT_BOOL(NETWORK_IS_IN_MP_CUTSCENE())
			NET_NL()NET_PRINT("[BCIDLE] IS_IDLE_KICK_CUTSCENE_CHECK_DISABLE = ")NET_PRINT_BOOL(IS_IDLE_KICK_CUTSCENE_CHECK_DISABLE())
			NET_NL()NET_PRINT("[BCIDLE] IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP = ")NET_PRINT_BOOL(IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP())
			NET_NL()NET_PRINT("[BCIDLE] IS_TRANSITION_ACTIVE = ")NET_PRINT_BOOL(IS_TRANSITION_ACTIVE())
			NET_NL()NET_PRINT("[BCIDLE] IS_ANY_BUTTON_PRESSED(PLAYER_CONTROL) = ")NET_PRINT_BOOL(IS_ANY_BUTTON_PRESSED(PLAYER_CONTROL))
			NET_NL()NET_PRINT("[BCIDLE] IS_ANY_BUTTON_PRESSED(FRONTEND_CONTROL) = ")NET_PRINT_BOOL(IS_ANY_BUTTON_PRESSED(FRONTEND_CONTROL))
			NET_NL()NET_PRINT("[BCIDLE] IS_ANY_BUTTON_PRESSED(CAMERA_CONTROL) = ")NET_PRINT_BOOL(IS_ANY_BUTTON_PRESSED(CAMERA_CONTROL))
			NET_NL()NET_PRINT("[BCIDLE] IS_PLAYER_IN_VEH_WITH_BUDDY(PLAYER_ID())  = ")NET_PRINT_BOOL(IS_PLAYER_IN_VEH_WITH_BUDDY(PLAYER_ID()))
			NET_NL()NET_PRINT("[BCIDLE] IS_SOCIAL_CLUB_ACTIVE = ")NET_PRINT_BOOL(IS_SOCIAL_CLUB_ACTIVE())
			NET_NL()NET_PRINT("[BCIDLE] IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_RUN_PASSIVE_CUT) = ")NET_PRINT_BOOL(IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet7, BI_FM_NMH7_RUN_PASSIVE_CUT))
			NET_NL()NET_PRINT("[BCIDLE] PLM_IS_IN_CONSTRAINED_MODE = ")NET_PRINT_BOOL(PLM_IS_IN_CONSTRAINED_MODE())
		ENDIF
	ENDIF
	#ENDIF
	RETURN TRUE
ENDFUNC


FUNC INT TIME_UNTIL_PLAYER_IS_IDLE_KICKED(SCRIPT_TIMER& iTimeForHold, INT& ResultMilliseconds)


	IF ARE_ANY_CONTROLS_BEING_PRESSED() = FALSE
		                        
        TIME_DATATYPE CurrentTime = GET_NETWORK_TIME()
        
		IF HAS_NET_TIMER_STARTED(iTimeForHold)
       	 	ResultMilliseconds += GET_TIME_DIFFERENCE(CurrentTime, iTimeForHold.Timer)
        ENDIF
		
		REINIT_NET_TIMER(iTimeForHold)

		
	ELSE
		 iTimeForHold.Timer = GET_NETWORK_TIME()
		 ResultMilliseconds = 0
		 MPGlobalsHud.iHowLongIdling = 0
	ENDIF

	RESET_IDLE_KICK_DISABLED_THIS_FRAME()

	RETURN ResultMilliseconds

ENDFUNC



PROC DISPLAY_LOCAL_TICKER_FOR_IDLE_TIME(INT MillisecondsLeft, SCRIPT_TIMER& aTimer, INT MessageRepeatTime)
	IF IS_SKYSWOOP_AT_GROUND()
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, MessageRepeatTime)
			IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
				BEGIN_TEXT_COMMAND_THEFEED_POST("HUD_ILDETIME")
					ADD_TEXT_COMPONENT_SUBSTRING_TIME(MillisecondsLeft, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TEXT_FORMAT_SHOW_UNIT_DIVIDERS_AS_LETTERS)
				g_IdleFeedID = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)		
				NET_NL()NET_PRINT("[BCIDLE] DISPLAY_LOCAL_TICKER_FOR_IDLE_TIME: g_IdleFeedID = ")NET_PRINT_INT(g_IdleFeedID)

			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DISPLAY_LOCAL_TICKER_FOR_IDLE_TIME_FORCED(INT MillisecondsLeft, SCRIPT_TIMER& aTimer, INT MessageRepeatTime)
	IF IS_SKYSWOOP_AT_GROUND()
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, MessageRepeatTime)
			IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
				BEGIN_TEXT_COMMAND_THEFEED_POST("HUD_ILDETIME")
					ADD_TEXT_COMPONENT_SUBSTRING_TIME(MillisecondsLeft, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TEXT_FORMAT_SHOW_UNIT_DIVIDERS_AS_LETTERS)
				g_IdleFeedID = END_TEXT_COMMAND_THEFEED_POST_TICKER_FORCED(TRUE)		
				NET_NL()NET_PRINT("[BCIDLE] RUN_IDLE_KICK_MESSAGES: DISPLAY_LOCAL_TICKER_FOR_IDLE_TIME_FORCED: g_IdleFeedID = ")NET_PRINT_INT(g_IdleFeedID)

			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC TURN_OFF_IDLE_TICKERS()
	IF g_IdleFeedID > -1
		THEFEED_REMOVE_ITEM(g_IdleFeedID) 
		g_IdleFeedID = -1
	ENDIF
ENDPROC

PROC RESET_IDLE_KICK_BEEPS()
	INT I
	MPGlobalsHud_TitleUpdate.iIdleCountdownBeepsCount = 6
	FOR I = 0 TO 11
		MPGlobalsHud.bIdleCountdownBeeps[I] = FALSE
	ENDFOR
	
ENDPROC

PROC RUN_IDLE_KICK_MESSAGES(SCRIPT_TIMER& iTimeForHold)


	

	#IF IS_DEBUG_OR_PROFILE_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_noIdleKick")
	
		#IF IS_DEBUG_BUILD
			IF g_B_IdleKickPrints
				NET_NL()NET_PRINT("[BCIDLE] GET_COMMANDLINE_PARAM_EXISTS(sc_noIdleKick) = TRUE ")
			ENDIF
		#ENDIF
	
		EXIT
	ENDIF
	#ENDIF // IS_DEBUG_OR_PROFILE_BUILD

	#IF IS_DEBUG_BUILD
	IF NETWORK_IS_IN_CODE_DEVELOPMENT_MODE()
	
		#IF IS_DEBUG_BUILD
			IF g_B_IdleKickPrints
				NET_NL()NET_PRINT("[BCIDLE] NETWORK_IS_IN_CODE_DEVELOPMENT_MODE() = TRUE ")
			ENDIF
		#ENDIF
	
		EXIT
	ENDIF
	
	#ENDIF // IS_DEBUG_BUILD
	
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("debug checks")
//	#ENDIF
//	#ENDIF
	
	IF IS_TRANSITION_ACTIVE()
		MPGlobalsHud.iHowLongIdling = 0
		g_IdleCheckMessageStages = 0
		TURN_OFF_IDLE_TICKERS()
		EXIT
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("IS_TRANSITION_ACTIVE")
//	#ENDIF
//	#ENDIF

	IF HAS_KICKED_IDLING_TRANSITION() 
		MPGlobalsHud.iHowLongIdling = 0
		g_IdleCheckMessageStages = 0
		TURN_OFF_IDLE_TICKERS()
		EXIT
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("HAS_KICKED_IDLING_TRANSITION")
//	#ENDIF
//	#ENDIF
	
//	IF IS_PLAYER_SCTV(PLAYER_ID()) 
//		MPGlobalsHud.iHowLongIdling = 0
//		g_IdleCheckMessageStages = 0
//		TURN_OFF_TICKERS()
//		EXIT
//	ENDIF
	
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("IS_PLAYER_SCTV")
//	#ENDIF
//	#ENDIF
	
	IF ARE_CREDITS_RUNNING()
	AND g_ResetIdleTimerWhileViewingCredits
		MPGlobalsHud.iHowLongIdling = 0
		g_IdleCheckMessageStages = 0
		TURN_OFF_IDLE_TICKERS()
		EXIT
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("ARE_CREDITS_RUNNING")
//	#ENDIF
//	#ENDIF
	
	IF IS_PLAYER_USING_A_TAXI()
		MPGlobalsHud.iHowLongIdling = 0
		g_IdleCheckMessageStages = 0
		TURN_OFF_IDLE_TICKERS()
		
		EXIT
	ENDIF
	
	IF IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
		MPGlobalsHud.iHowLongIdling = 0
		g_IdleCheckMessageStages = 0
		TURN_OFF_IDLE_TICKERS()
		EXIT
	ENDIF
	
	
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("IS_PLAYER_USING_A_TAXI")
//	#ENDIF
//	#ENDIF

	
	TIME_UNTIL_PLAYER_IS_IDLE_KICKED(iTimeForHold, MPGlobalsHud.iHowLongIdling)
	
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("TIME_UNTIL_PLAYER_IS_IDLE_KICKED")
//	#ENDIF
//	#ENDIF

	
	IF MPGlobalsHud.iHowLongIdling = 0

		TURN_OFF_IDLE_TICKERS()
		g_IdleCheckMessageStages = 0
	ENDIF
	
//	INT I

	INT iIdleTimeMultiplyer = 1
	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF HAS_ENTERED_GAME_AS_PRIVATE_SCTV(PLAYER_ID())
			#IF IS_DEBUG_BUILD
			IF g_B_IdleKickPrints
				NET_NL()NET_PRINT("[BCIDLE] HAS_ENTERED_GAME_AS_PRIVATE_SCTV() = TRUE ")
			ENDIF
			#ENDIF
			EXIT
		ENDIF
		iIdleTimeMultiplyer = 2
	ENDIF

	INT FinishedTime = (g_sMPTunables.iIdleKick_Kick*iIdleTimeMultiplyer)
	INT Warning1 = (g_sMPTunables.iIdleKick_Warning1*iIdleTimeMultiplyer)
	INT Warning2 = (g_sMPTunables.iIdleKick_Warning2*iIdleTimeMultiplyer)
	INT Warning3 = (g_sMPTunables.iIdleKick_Warning3*iIdleTimeMultiplyer)

	g_i_CurrentIdleKickTime = FinishedTime
	
	IF IS_IDLE_KICK_TIME_OVERRIDDEN()
	
		FinishedTime = GET_IDLE_KICK_TIME_OVERRIDE_MS()

		Warning1 = FLOOR(TO_FLOAT(FinishedTime)/(7.5))
		Warning2 = FLOOR(TO_FLOAT(FinishedTime)/(3.0))
		Warning3 = FLOOR(TO_FLOAT(FinishedTime)/(1.5))
	ENDIF
		
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_QuickIdleKick")
		NET_NL()NET_PRINT("[BCIDLE] RUN_IDLE_KICK_MESSAGES sc_QuickKick")
		
		FinishedTime = 60000 * 3
		
		Warning1 = FLOOR(TO_FLOAT(FinishedTime)/(7.5))
		Warning2 = FLOOR(TO_FLOAT(FinishedTime)/(3.0))
		Warning3 = FLOOR(TO_FLOAT(FinishedTime)/(1.5))
	ENDIF
	#ENDIF
	
		
	SWITCH g_IdleCheckMessageStages
	
		CASE 0
		
			
//			NET_NL()NET_PRINT_TIME()NET_PRINT("RUN_IDLE_KICK_MESSAGES: ")
//			NET_NL()NET_PRINT("			- MPGlobalsHud.iHowLongIdling = ")NET_PRINT_INT(MPGlobalsHud.iHowLongIdling)
//			NET_NL()NET_PRINT("			- MPGlobalsHud.iIdleKick_Warning1 = ")NET_PRINT_INT(g_sMPTunables.iIdleKick_Warning1)
//			
//			FOR I = 1 TO 11
//				NET_NL()NET_PRINT("			- MPGlobalsHud.iIdleKick_Warning1-(1000*I) = ")NET_PRINT_INT(g_sMPTunables.iIdleKick_Warning1-(1000*I))
//
//				IF MPGlobalsHud.bIdleCountdownBeeps[I] = FALSE
//					IF MPGlobalsHud.iHowLongIdling >= g_sMPTunables.iIdleKick_Warning1-(1000*I)
//						NET_NL()NET_PRINT_TIME()NET_PRINT("PLAY_SOUND_FRONTEND MP_IDLE_TIMER ")NET_PRINT_INT(I)
//						PLAY_SOUND_FRONTEND(-1,"MP_IDLE_TIMER","HUD_FRONTEND_DEFAULT_SOUNDSET")
//						MPGlobalsHud.bIdleCountdownBeeps[I] = TRUE
//					ENDIF
//				ENDIF
//			ENDFOR
		
			IF MPGlobalsHud.iHowLongIdling >=  (Warning1) //2m
			
				PLAYSTATS_IDLE_KICK(Warning1)
				DISPLAY_LOCAL_TICKER_FOR_IDLE_TIME(((FinishedTime)-(Warning1)), MPGlobalsHud.IdleMessageTimer, -1)
				
				#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[BCIDLE] RUN_IDLE_KICK_MESSAGES: MPGlobalsHud.iHowLongIdling >= g_sMPTunables.iIdleKick_Warning1  ")
					NET_PRINT_INT(MPGlobalsHud.iHowLongIdling )NET_PRINT(" >= ")NET_PRINT("Warning1 ")
					NET_PRINT(" IS_IDLE_KICK_TIME_OVERRIDDEN = ")NET_PRINT_BOOL(IS_IDLE_KICK_TIME_OVERRIDDEN())
				#ENDIF
				
				
				
				g_IdleCheckMessageStages = 1
			ENDIF
		BREAK
		
		CASE 1
			IF MPGlobalsHud.iHowLongIdling >= (Warning2) //300000 //5m
				
				PLAYSTATS_IDLE_KICK(Warning2)
				DISPLAY_LOCAL_TICKER_FOR_IDLE_TIME((FinishedTime-Warning2), MPGlobalsHud.IdleMessageTimer, -1)
				
				#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[BCIDLE] RUN_IDLE_KICK_MESSAGES: MPGlobalsHud.iHowLongIdling >= g_sMPTunables.iIdleKick_Warning2  ")
					NET_PRINT_INT(MPGlobalsHud.iHowLongIdling )NET_PRINT(" >= ")NET_PRINT("Warning2 ")
					NET_PRINT(" IS_IDLE_KICK_TIME_OVERRIDDEN = ")NET_PRINT_BOOL(IS_IDLE_KICK_TIME_OVERRIDDEN())
				#ENDIF
				
				g_IdleCheckMessageStages = 2
			ENDIF
		BREAK
		
		CASE 2
			
			IF g_b_IdleKickIsFeedPausedChecked = FALSE
				IF MPGlobalsHud.iHowLongIdling >= Warning3-5000
					g_b_IdleKickHidefeed = THEFEED_IS_PAUSED()
					THEFEED_FORCE_RENDER_ON()
					THEFEED_RESUME()
					THEFEED_FLUSH_QUEUE()
					g_b_ReapplyStickySaveFailedFeed = FALSE
					NET_NL()NET_PRINT("[BCIDLE] RUN_IDLE_KICK_MESSAGES: THEFEED_RESUME and THEFEED_FORCE_RENDER_ON ")
					NET_NL()NET_PRINT("[BCIDLE] RUN_IDLE_KICK_MESSAGES: g_b_IdleKickHidefeed = ")NET_PRINT_BOOL(g_b_IdleKickHidefeed)
					g_b_IdleKickIsFeedPausedChecked = TRUE
				ENDIF
				
			ENDIF
		
			IF MPGlobalsHud.iHowLongIdling >= Warning3 //600000 //10m
				
				PLAYSTATS_IDLE_KICK(Warning3)
				
				DISPLAY_LOCAL_TICKER_FOR_IDLE_TIME_FORCED((FinishedTime-Warning3), MPGlobalsHud.IdleMessageTimer, -1)
				
				
				
				#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[BCIDLE] RUN_IDLE_KICK_MESSAGES: MPGlobalsHud.iHowLongIdling >= g_sMPTunables.iIdleKick_Warning3  ")
					NET_PRINT_INT(MPGlobalsHud.iHowLongIdling )NET_PRINT(" >= ")NET_PRINT("Warning3 ")
					NET_PRINT(" IS_IDLE_KICK_TIME_OVERRIDDEN = ")NET_PRINT_BOOL(IS_IDLE_KICK_TIME_OVERRIDDEN())
				#ENDIF
				RESET_IDLE_KICK_BEEPS()
				g_IdleCheckMessageStages = 3
			ENDIF
		BREAK
		
		CASE 3
		
			IF MPGlobalsHud.iHowLongIdling >= Warning3+7000
				IF g_b_IdleKickHidefeed = TRUE	
					THEFEED_PAUSE()
					THEFEED_FORCE_RENDER_OFF()
					THEFEED_FLUSH_QUEUE()
	 				g_b_ReapplyStickySaveFailedFeed = FALSE					
					NET_NL()NET_PRINT("[BCIDLE] RUN_IDLE_KICK_MESSAGES: THEFEED_PAUSE and THEFEED_FORCE_RENDER_OFF ")
					g_b_IdleKickHidefeed = FALSE
				ENDIF
			ENDIF
			
			g_b_IdleKickIsFeedPausedChecked = FALSE
		
//			FOR I = 1 TO 11
			IF MPGlobalsHud_TitleUpdate.iIdleCountdownBeepsCount > 0
				IF MPGlobalsHud.bIdleCountdownBeeps[MPGlobalsHud_TitleUpdate.iIdleCountdownBeepsCount] = FALSE
					IF MPGlobalsHud.iHowLongIdling >= FinishedTime-(1000*MPGlobalsHud_TitleUpdate.iIdleCountdownBeepsCount)
						NET_NL()NET_PRINT(" ******** PLAY_SOUND_FRONTEND(-1,MP_IDLE_TIMER ******")
						PLAY_SOUND_FRONTEND(-1,"MP_IDLE_TIMER","HUD_FRONTEND_DEFAULT_SOUNDSET")
						
						
						
						MPGlobalsHud.bIdleCountdownBeeps[MPGlobalsHud_TitleUpdate.iIdleCountdownBeepsCount] = TRUE
						MPGlobalsHud_TitleUpdate.iIdleCountdownBeepsCount--
					ENDIF
				ENDIF
			ENDIF
//			ENDFOR
		
				
			IF MPGlobalsHud.iHowLongIdling >= FinishedTime //630000 //10m30s Les todo 1204754
				PLAY_SOUND_FRONTEND(-1, "MP_IDLE_KICK","HUD_FRONTEND_DEFAULT_SOUNDSET")
				g_Private_KickedForIdling = TRUE
				
				// fix for url:bugstar:7466473 - Gen9 - The application becomes unresponsive on the ‘Skycam’ after accepting the idle timeout alert message.
				#IF FEATURE_GEN9_EXCLUSIVE
				CLEAR_TRANSITION_SESSION_SRLINK_REQUESTED_JOB()
				PRINTLN("[BCIDLE] RUN_IDLE_KICK_MESSAGES: calling CLEAR_TRANSITION_SESSION_SRLINK_REQUESTED_JOB")
				#ENDIF
				
				PLAYSTATS_IDLE_KICK(FinishedTime)
				
				#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[BCIDLE] RUN_IDLE_KICK_MESSAGES: g_Private_KickedForIdling = TRUE ")
				#ENDIF
				
			ENDIF
		BREAK
	
	ENDSWITCH
	
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("g_IdleCheckMessageStages")
//	#ENDIF
//	#ENDIF
	


//	NET_NL()NET_PRINT("Idle time = ")NET_PRINT_INT(MPGlobalsHud.iHowLongIdling)

ENDPROC


























////////////////////////////////////////
////	Constrained Kicking	////////////////
////////////////////////////////////////





FUNC INT TIME_UNTIL_PLAYER_IS_CONSTRAINED_KICKED( INT& ResultMilliseconds)


	IF PLM_IS_IN_CONSTRAINED_MODE()  
		                        
    
       	ResultMilliseconds = PLM_GET_CONSTRAINED_DURATION_MS()
		

		
	ELSE
		ResultMilliseconds = -1
	ENDIF


	RETURN ResultMilliseconds

ENDFUNC



PROC DISPLAY_LOCAL_TICKER_FOR_CONSTRAINED_TIME(INT MillisecondsLeft, SCRIPT_TIMER& aTimer, INT MessageRepeatTime)
	IF IS_SKYSWOOP_AT_GROUND()
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, MessageRepeatTime)
			IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
				BEGIN_TEXT_COMMAND_THEFEED_POST("HUD_ILDETIME")
					ADD_TEXT_COMPONENT_SUBSTRING_TIME(MillisecondsLeft, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TEXT_FORMAT_SHOW_UNIT_DIVIDERS_AS_LETTERS)
				g_constrainedFeedID = END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE)		
				NET_NL()NET_PRINT("DISPLAY_LOCAL_TICKER_FOR_CONSTRAINED_TIME: g_constrainedFeedID = ")NET_PRINT_INT(g_constrainedFeedID)

			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DISPLAY_LOCAL_TICKER_FOR_CONSTRAINED_TIME_FORCED(INT MillisecondsLeft, SCRIPT_TIMER& aTimer, INT MessageRepeatTime)
	IF IS_SKYSWOOP_AT_GROUND()
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, MessageRepeatTime)
			IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
				BEGIN_TEXT_COMMAND_THEFEED_POST("HUD_ILDETIME")
					ADD_TEXT_COMPONENT_SUBSTRING_TIME(MillisecondsLeft, TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TEXT_FORMAT_SHOW_UNIT_DIVIDERS_AS_LETTERS)
				g_constrainedFeedID = END_TEXT_COMMAND_THEFEED_POST_TICKER_FORCED(TRUE)		
				NET_NL()NET_PRINT("RUN_CONSTRAINED_KICK_MESSAGES: DISPLAY_LOCAL_TICKER_FOR_CONSTRAINED_TIME_FORCED: g_constrainedFeedID = ")NET_PRINT_INT(g_constrainedFeedID)

			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC TURN_OFF_CONSTRAINED_TICKERS()
	IF g_constrainedFeedID > -1
		THEFEED_REMOVE_ITEM(g_constrainedFeedID) 
		g_constrainedFeedID = -1
	ENDIF
ENDPROC

PROC RUN_CONSTRAINED_KICK_MESSAGES()


	

	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_noConstrainedKick")
	
		#IF IS_DEBUG_BUILD
			IF g_B_constrainedKickPrints
				NET_NL()NET_PRINT("[BCCONST] GET_COMMANDLINE_PARAM_EXISTS(sc_noConstrainedKick) = TRUE ")
			ENDIF
		#ENDIF
	
		EXIT
	ENDIF
	
	IF NETWORK_IS_IN_CODE_DEVELOPMENT_MODE()
	
		#IF IS_DEBUG_BUILD
			IF g_B_constrainedKickPrints
				NET_NL()NET_PRINT("[BCCONST] NETWORK_IS_IN_CODE_DEVELOPMENT_MODE() = TRUE ")
			ENDIF
		#ENDIF
	
		EXIT
	ENDIF
	
	#ENDIF

	
	IF IS_TRANSITION_ACTIVE()
		MPGlobalsHud.iHowLongConstrained = 0
		TURN_OFF_CONSTRAINED_TICKERS()
		EXIT
	ENDIF


	IF HAS_KICKED_IDLING_TRANSITION() 
		MPGlobalsHud.iHowLongConstrained = 0
		g_ConstrainedCheckMessageStages = 0
		TURN_OFF_CONSTRAINED_TICKERS()
		EXIT
	ENDIF
	


	
	TIME_UNTIL_PLAYER_IS_CONSTRAINED_KICKED(MPGlobalsHud.iHowLongConstrained)
	
	
	
	IF MPGlobalsHud.iHowLongConstrained = 0
	OR MPGlobalsHud.iHowLongConstrained = -1

		TURN_OFF_CONSTRAINED_TICKERS()
		g_constrainedCheckMessageStages = 0
	ENDIF
	

	INT iconstrainedTimeMultiplyer = 1
	IF IS_PLAYER_SCTV(PLAYER_ID())
		IF HAS_ENTERED_GAME_AS_PRIVATE_SCTV(PLAYER_ID())
			#IF IS_DEBUG_BUILD
			IF g_B_constrainedKickPrints
				NET_NL()NET_PRINT("[BCCONST] HAS_ENTERED_GAME_AS_PRIVATE_SCTV() = TRUE ")
			ENDIF
			#ENDIF
			EXIT
		ENDIF
		iconstrainedTimeMultiplyer = 2
	ENDIF

	INT FinishedTime = (g_sMPTunables.iConstrainedKick_Kick*iconstrainedTimeMultiplyer)
	INT Warning1 = (g_sMPTunables.iConstrainedKick_Warning1*iconstrainedTimeMultiplyer)
	INT Warning2 = (g_sMPTunables.iConstrainedKick_Warning2*iconstrainedTimeMultiplyer)
	INT Warning3 = (g_sMPTunables.iConstrainedKick_Warning3*iconstrainedTimeMultiplyer)

	SWITCH g_constrainedCheckMessageStages
	
		CASE 0
		
			
//			NET_NL()NET_PRINT_TIME()NET_PRINT("RUN_constrained_KICK_MESSAGES: ")
//			NET_NL()NET_PRINT("			- MPGlobalsHud.iHowLongIdling = ")NET_PRINT_INT(MPGlobalsHud.iHowLongIdling)
//			NET_NL()NET_PRINT("			- MPGlobalsHud.iconstrainedKick_Warning1 = ")NET_PRINT_INT(g_sMPTunables.iconstrainedKick_Warning1)
//			
//			FOR I = 1 TO 11
//				NET_NL()NET_PRINT("			- MPGlobalsHud.iconstrainedKick_Warning1-(1000*I) = ")NET_PRINT_INT(g_sMPTunables.iconstrainedKick_Warning1-(1000*I))
//
//				IF MPGlobalsHud.bconstrainedCountdownBeeps[I] = FALSE
//					IF MPGlobalsHud.iHowLongIdling >= g_sMPTunables.iconstrainedKick_Warning1-(1000*I)
//						NET_NL()NET_PRINT_TIME()NET_PRINT("PLAY_SOUND_FRONTEND MP_constrained_TIMER ")NET_PRINT_INT(I)
//						PLAY_SOUND_FRONTEND(-1,"MP_constrained_TIMER","HUD_FRONTEND_DEFAULT_SOUNDSET")
//						MPGlobalsHud.bconstrainedCountdownBeeps[I] = TRUE
//					ENDIF
//				ENDIF
//			ENDFOR
		
			IF MPGlobalsHud.iHowLongConstrained >=  (Warning1) //2m
			
				DISPLAY_LOCAL_TICKER_FOR_constrained_TIME(((FinishedTime)-(Warning1)), MPGlobalsHud.constrainedMessageTimer, -1)
				
				#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[BCCONST] RUN_constrained_KICK_MESSAGES: MPGlobalsHud.iHowLongIdling >= g_sMPTunables.iconstrainedKick_Warning1  ")
				#ENDIF
				
				
				
				g_constrainedCheckMessageStages = 1
			ENDIF
		BREAK
		
		CASE 1
			IF MPGlobalsHud.iHowLongConstrained >= (Warning2) //300000 //5m
				
				DISPLAY_LOCAL_TICKER_FOR_constrained_TIME((FinishedTime-Warning2), MPGlobalsHud.constrainedMessageTimer, -1)
				
				#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[BCCONST] RUN_constrained_KICK_MESSAGES: MPGlobalsHud.iHowLongIdling >= g_sMPTunables.iconstrainedKick_Warning2  ")
				#ENDIF
				
				g_constrainedCheckMessageStages = 2
			ENDIF
		BREAK
		
		CASE 2
			
			IF g_b_constrainedKickIsFeedPausedChecked = FALSE
				IF MPGlobalsHud.iHowLongConstrained >= Warning3-5000
					g_b_constrainedKickHidefeed = THEFEED_IS_PAUSED()
					THEFEED_FORCE_RENDER_ON()
					THEFEED_RESUME()
					THEFEED_FLUSH_QUEUE()
					g_b_ReapplyStickySaveFailedFeed = FALSE
					NET_NL()NET_PRINT("[BCCONST] RUN_constrained_KICK_MESSAGES: THEFEED_RESUME and THEFEED_FORCE_RENDER_ON ")
					NET_NL()NET_PRINT("[BCCONST] RUN_constrained_KICK_MESSAGES: g_b_constrainedKickHidefeed = ")NET_PRINT_BOOL(g_b_constrainedKickHidefeed)
					g_b_constrainedKickIsFeedPausedChecked = TRUE
				ENDIF
				
			ENDIF
		
			IF MPGlobalsHud.iHowLongConstrained >= Warning3 //600000 //10m
				
				DISPLAY_LOCAL_TICKER_FOR_constrained_TIME_FORCED((FinishedTime-Warning3), MPGlobalsHud.constrainedMessageTimer, -1)
				
				#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[BCCONST] RUN_constrained_KICK_MESSAGES: MPGlobalsHud.iHowLongIdling >= g_sMPTunables.iconstrainedKick_Warning3  ")
				#ENDIF
				MPGlobalsHud_TitleUpdate.iconstrainedCountdownBeepsCount = 6
				g_constrainedCheckMessageStages = 3
			ENDIF
		BREAK
		
		CASE 3
		
			IF MPGlobalsHud.iHowLongConstrained >= Warning3+5000
				IF g_b_constrainedKickHidefeed = TRUE	
					THEFEED_PAUSE()
					THEFEED_FORCE_RENDER_OFF()
					THEFEED_FLUSH_QUEUE()
	 				g_b_ReapplyStickySaveFailedFeed = FALSE					
					NET_NL()NET_PRINT("[BCCONST] RUN_constrained_KICK_MESSAGES: THEFEED_PAUSE and THEFEED_FORCE_RENDER_OFF ")
					g_b_constrainedKickHidefeed = FALSE
				ENDIF
			ENDIF
			
			g_b_constrainedKickIsFeedPausedChecked = FALSE
		
//			FOR I = 1 TO 11
			IF MPGlobalsHud_TitleUpdate.iconstrainedCountdownBeepsCount > 0
				IF MPGlobalsHud.bconstrainedCountdownBeeps[MPGlobalsHud_TitleUpdate.iconstrainedCountdownBeepsCount] = FALSE
					IF MPGlobalsHud.iHowLongConstrained >= FinishedTime-(1000*MPGlobalsHud_TitleUpdate.iconstrainedCountdownBeepsCount)
						NET_NL()NET_PRINT(" ******** PLAY_SOUND_FRONTEND(-1,MP_IDLE_TIMER ******")
						PLAY_SOUND_FRONTEND(-1,"MP_IDLE_TIMER","HUD_FRONTEND_DEFAULT_SOUNDSET")
						
						MPGlobalsHud.bconstrainedCountdownBeeps[MPGlobalsHud_TitleUpdate.iconstrainedCountdownBeepsCount] = TRUE
						MPGlobalsHud_TitleUpdate.iconstrainedCountdownBeepsCount--
					ENDIF
				ENDIF
			ENDIF
//			ENDFOR
		
		
			IF MPGlobalsHud.iHowLongConstrained >= FinishedTime //630000 //10m30s Les todo 1204754
				PLAY_SOUND_FRONTEND(-1, "MP_IDLE_KICK","HUD_FRONTEND_DEFAULT_SOUNDSET")
				g_Private_KickedForConstrained = TRUE
								
				#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("[BCCONST] RUN_constrained_KICK_MESSAGES: g_Private_KickedForConstrained = TRUE ")
				#ENDIF
				
			ENDIF
		BREAK
	
	ENDSWITCH
	
//	#IF IS_DEBUG_BUILD
//	#IF SCRIPT_PROFILER_ACTIVE
//	ADD_SCRIPT_PROFILE_MARKER("g_constrainedCheckMessageStages")
//	#ENDIF
//	#ENDIF
	


//	NET_NL()NET_PRINT("constrained time = ")NET_PRINT_INT(MPGlobalsHud.iHowLongIdling)

ENDPROC














////////////////////////////////////////
////		Automute	////////////////
////////////////////////////////////////
///    



FUNC INT GET_TOTAL_NUMBER_OF_REPORTS()

	INT NUM
	NUM += g_MPPLY_OFFENSIVE_LANGUAGE
	NUM += g_MPPLY_GRIEFING
	NUM += g_MPPLY_FRIENDLY
	NUM += g_MPPLY_HELPFUL
	NUM += g_MPPLY_OFFENSIVE_UGC
	NUM += g_MPPLY_OFFENSIVE_TAGPLATE
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_EXPLOITS)
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_VC_ANNOYINGME)
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_VC_HATE)
	
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_BAD_CREW_NAME)
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_BAD_CREW_MOTTO)
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_BAD_CREW_STATUS)
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_BAD_CREW_EMBLEM)
	
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_PLAYERMADE_TITLE)
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_PLAYERMADE_DESC)
	
	
	RETURN NUM 

ENDFUNC

FUNC INT GET_TOTAL_NUMBER_OF_NEGATIVE_REPORTS()

	INT NUM
	NUM += g_MPPLY_OFFENSIVE_LANGUAGE
	NUM += g_MPPLY_GRIEFING
	NUM += g_MPPLY_OFFENSIVE_UGC
	NUM += g_MPPLY_OFFENSIVE_TAGPLATE
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_EXPLOITS)
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_VC_ANNOYINGME)
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_VC_HATE)
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_BAD_CREW_NAME)
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_BAD_CREW_MOTTO)
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_BAD_CREW_STATUS)
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_BAD_CREW_EMBLEM)
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_PLAYERMADE_TITLE)
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_PLAYERMADE_DESC)
	
	RETURN NUM 

ENDFUNC

//Used for Bad sport muting. 
FUNC INT GET_TOTAL_NUMBER_OF_CHAT_NEGATIVE_REPORTS()

	INT NUM
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_VC_ANNOYINGME)
	NUM += GET_MP_INT_PLAYER_STAT(MPPLY_VC_HATE)

	
	RETURN NUM 

ENDFUNC


FUNC INT GET_TOTAL_NUMBER_OF_POSITIVE_REPORTS()

	INT NUM
	NUM += g_MPPLY_FRIENDLY
	NUM += g_MPPLY_HELPFUL
	
	RETURN NUM 

ENDFUNC


PROC DISPLAY_LOCAL_TICKER_FOR_AUTOMUTE()
//	SCRIPT_ASSERT("DISPLAY_LOCAL_TICKER_FOR_AUTOMUTE")
	BEGIN_TEXT_COMMAND_THEFEED_POST("HUD_AUTOMUTE")
	END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)		
ENDPROC


PROC DISPLAY_LOCAL_TICKER_FOR_AUTOMUTE_BADSPORT()
	BEGIN_TEXT_COMMAND_THEFEED_POST("HUD_MUTE_BAD")
	END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)		
ENDPROC

//PROC RUN_AUTO_MUTE_CHECK_AT_BOOT()
//
//	INT MuteCount
//	INT TalkerCount
//	FLOAT MutedPercent
//
//
//	INT I
//	PLAYER_INDEX PlayerCheck
//	FOR I = 0 TO NUM_NETWORK_PLAYERS-1
//		PlayerCheck = INT_TO_NATIVE(PLAYER_INDEX, I)
//		
//		IF PlayerCheck <> INVALID_PLAYER_INDEX()
//			IF IS_NET_PLAYER_OK(PlayerCheck, FALSE, FALSE)
//				NETWORK_GET_MUTE_COUNT_FOR_PLAYER(PlayerCheck, MuteCount, TalkerCount)
//				
//				MutedPercent = (TO_FLOAT(MuteCount)/TO_FLOAT(TalkerCount))*100.0
//				 MutedPercent = 80
//				#IF IS_DEBUG_BUILD 
//				NET_NL()NET_PRINT("RUN_AUTO_MUTE_CHECK_AT_BOOT: - ")
//				NET_NL()NET_PRINT("			- Player Num to check = ")NET_PRINT_INT(I)
//				NET_NL()NET_PRINT("			- Player Name to check = ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
//				NET_NL()NET_PRINT("			- MuteCount = ")NET_PRINT_INT(MuteCount)
//				NET_NL()NET_PRINT("			- TalkerCount = ")NET_PRINT_INT(TalkerCount)
//				NET_NL()NET_PRINT("			- MutedPercent = ")NET_PRINT_FLOAT(MutedPercent)
//				#ENDIF
//				
//				IF MutedPercent > g_sMPTunables.AutoMutePercent
//					g_Private_Display_AutoMute_Feed = TRUE
//					
//					NETWORK_SET_PLAYER_MUTED(PlayerCheck, TRUE)
//					#IF IS_DEBUG_BUILD
//					NET_NL()NET_PRINT("			- NETWORK_SET_PLAYER_MUTED = TRUE ")
//					NET_NL()
//					#ENDIF
//				ELSE
//					#IF IS_DEBUG_BUILD
//					NET_NL()NET_PRINT("			- NETWORK_SET_PLAYER_MUTED = FALSE ")
//					NET_NL()
//					#ENDIF
//				ENDIF
//			ENDIF
//			
//		ENDIF
//		
//		
//	ENDFOR
//	
//	
//
//	IF NETWORK_PLAYER_IS_BADSPORT()
//	OR NETWORK_PLAYER_IS_CHEATER()
//		IF PlayerCheck <> INVALID_PLAYER_INDEX()
//			IF IS_NET_PLAYER_OK(PlayerCheck, FALSE, FALSE)
//				INT NumberOfReports = (GET_TOTAL_NUMBER_OF_REPORTS())
//				INT NumberNegReports = (GET_TOTAL_NUMBER_OF_NEGATIVE_REPORTS())
//				
//				
//				MutedPercent = (TO_FLOAT(NumberNegReports)/TO_FLOAT(NumberOfReports))*100.0
//				
//				#IF IS_DEBUG_BUILD 
//				NET_NL()NET_PRINT("RUN_AUTO_MUTE_CHECK_AT_BOOT: NETWORK_PLAYER_IS_BADSPORT/CHEATER - ")
//				NET_NL()NET_PRINT("			- NumberOfReports = ")NET_PRINT_INT(NumberOfReports)
//				NET_NL()NET_PRINT("			- NumberNegReports = ")NET_PRINT_INT(NumberNegReports)
//				NET_NL()NET_PRINT("			- MutedPercent = ")NET_PRINT_FLOAT(MutedPercent)
//				NET_NL()NET_PRINT("			- g_sMPTunables.AutoMuteCheatBadSportPercent = ")NET_PRINT_FLOAT(g_sMPTunables.AutoMuteCheatBadSportPercent)
//				#ENDIF
//				
//				
//				IF MutedPercent > g_sMPTunables.AutoMuteCheatBadSportPercent
//					g_Private_Display_AutoMute_Feed = TRUE
//					NETWORK_SET_PLAYER_MUTED(PlayerCheck, TRUE)
//					#IF IS_DEBUG_BUILD
//					NET_NL()NET_PRINT("			- NETWORK_SET_PLAYER_MUTED = TRUE ")
//					NET_NL()
//					#ENDIF
//				ELSE
//					#IF IS_DEBUG_BUILD
//					NET_NL()NET_PRINT("			- NETWORK_SET_PLAYER_MUTED = FALSE ")
//					NET_NL()
//					#ENDIF
//				ENDIF
//				
//				
//			ENDIF
//		ENDIF
//	
//	ENDIF
//	
//
//
//ENDPROC

FUNC BOOL HAS_SOMEONE_CHEAT_CHANGED()

	INT I
	FOR I = 0 TO NUM_NETWORK_PLAYERS-1
		IF MPGlobalsHud.iCheatStateBitset_LastFrame[I] <> GlobalplayerBD_FM[I].scoreData.iCheatStateBitset
			NET_NL()NET_PRINT("HAS_SOMEONE_CHEAT_CHANGED: ")
			NET_NL()NET_PRINT("		- I = ")NET_PRINT_INT(I)
			NET_NL()NET_PRINT("		- MPGlobalsHud.iCheatStateBitset_LastFrame[I] = ")NET_PRINT_INT(MPGlobalsHud.iCheatStateBitset_LastFrame[I])
			NET_NL()NET_PRINT("		- GlobalplayerBD_FM[I].scoreData.iCheatStateBitset = ")NET_PRINT_INT(GlobalplayerBD_FM[I].scoreData.iCheatStateBitset)
			RETURN TRUE
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

PROC UPDATE_LOCAL_PLAYER_AUTOMUTE()

	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		INT MuteCount
		INT TalkerCount
		FLOAT MutedPercent
		INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())
		
		
		
		
		
		NETWORK_GET_MUTE_COUNT_FOR_PLAYER(PLAYER_ID(), MuteCount, TalkerCount)
		
		 
		MutedPercent = (TO_FLOAT(MuteCount)/TO_FLOAT(TalkerCount))*100.0
	
		#IF IS_DEBUG_BUILD 
		NET_NL()NET_PRINT("UPDATE_LOCAL_PLAYER_AUTOMUTE: - ")
		NET_NL()NET_PRINT("			- Player Name to check = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
		NET_NL()NET_PRINT("			- iPlayerID = ")NET_PRINT_INT(iPlayerID)
		NET_NL()NET_PRINT("			- MuteCount = ")NET_PRINT_INT(MuteCount)
		NET_NL()NET_PRINT("			- TalkerCount = ")NET_PRINT_INT(TalkerCount)
		NET_NL()NET_PRINT("			- MutedPercent = ")NET_PRINT_FLOAT(MutedPercent)
		NET_NL()NET_PRINT("			- NETWORK_HAS_AUTOMUTE_OVERRIDE = ")NET_PRINT_BOOL(NETWORK_HAS_AUTOMUTE_OVERRIDE())
		NET_NL()NET_PRINT("			- g_sMPTunables.AutoMutePercent = ")NET_PRINT_FLOAT(g_sMPTunables.AutoMutePercent)
		NET_NL()NET_PRINT("			- g_sMPTunables.iLOCAL_PLAYER_AUTOMUTE_TALKER_LIMIT = ")NET_PRINT_INT(g_sMPTunables.iLOCAL_PLAYER_AUTOMUTE_TALKER_LIMIT)

		#ENDIF
		
		IF NETWORK_HAS_AUTOMUTE_OVERRIDE()
			NET_NL()NET_PRINT("			- OVERRIDE NETWORK_SET_SCRIPT_AUTOMUTED(TRUE) ")
			NETWORK_SET_SCRIPT_AUTOMUTED(TRUE)
			EXIT
		ENDIF

		IF iPlayerID > -1
			IF MutedPercent > g_sMPTunables.AutoMutePercent
			AND TalkerCount > g_sMPTunables.iLOCAL_PLAYER_AUTOMUTE_TALKER_LIMIT //3
				IF NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_AUTOMUTED)
					g_AutomuteChecksDone = FALSE
					NET_NL()NET_PRINT("			- NETWORK_SET_SCRIPT_AUTOMUTED(TRUE) ")
					NETWORK_SET_SCRIPT_AUTOMUTED(TRUE)
					SET_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_AUTOMUTED)
				ENDIF
			ELSE
				IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_AUTOMUTED)
					g_AutomuteChecksDone = FALSE
					NET_NL()NET_PRINT("			- NETWORK_SET_SCRIPT_AUTOMUTED(FALSE) ")
					NETWORK_SET_SCRIPT_AUTOMUTED(FALSE)
					CLEAR_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_AUTOMUTED)
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF


ENDPROC


PROC UPDATE_LOCAL_PLAYER_AUTOMUTE_BADSPORT()

	
	IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		FLOAT MutedPercent
		
		INT iPlayerID = NATIVE_TO_INT(PLAYER_ID())

		INT NumberOfReports = (GET_TOTAL_NUMBER_OF_REPORTS())
		INT NumberNegReports = (GET_TOTAL_NUMBER_OF_CHAT_NEGATIVE_REPORTS())
				
				
		MutedPercent = (TO_FLOAT(NumberNegReports)/TO_FLOAT(NumberOfReports))*100.0

		#IF IS_DEBUG_BUILD 
		NET_NL()NET_PRINT("UPDATE_LOCAL_PLAYER_AUTOMUTE_BADSPORT: - ")
		NET_NL()NET_PRINT("			- Player Name to check = ")NET_PRINT(GET_PLAYER_NAME(PLAYER_ID()))
		NET_NL()NET_PRINT("			- iPlayerID = ")NET_PRINT_INT(iPlayerID)
		NET_NL()NET_PRINT("			- NumberOfReports = ")NET_PRINT_INT(NumberOfReports)
		NET_NL()NET_PRINT("			- NumberNegReports = ")NET_PRINT_INT(NumberNegReports)
		NET_NL()NET_PRINT("			- MutedPercent = ")NET_PRINT_FLOAT(MutedPercent)
		NET_NL()NET_PRINT("			- NETWORK_HAS_AUTOMUTE_OVERRIDE = ")NET_PRINT_BOOL(NETWORK_HAS_AUTOMUTE_OVERRIDE())

		#ENDIF
		
		IF NETWORK_HAS_AUTOMUTE_OVERRIDE()
			NET_NL()NET_PRINT("			- OVERRIDE NETWORK_SET_SCRIPT_AUTOMUTED(TRUE) ")
			NETWORK_SET_SCRIPT_AUTOMUTED(TRUE)
			EXIT
		ENDIF

		IF iPlayerID > -1
			
			
			
			IF MutedPercent > g_sMPTunables.AutoMuteCheatBadSportPercent
			AND NumberOfReports >= 64  //Minimum of 8 reports todo 1444244. Plus report weight of 16 for fresh voter. 
				IF NOT IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_AUTOMUTED_BADSPORT)
					g_AutomuteChecksDone = FALSE
					NETWORK_SET_SCRIPT_AUTOMUTED(TRUE)
					NET_NL()NET_PRINT("			- NETWORK_SET_SCRIPT_AUTOMUTED(TRUE) ")
					SET_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_AUTOMUTED_BADSPORT)
				ENDIF
			ELSE
				IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_AUTOMUTED_BADSPORT)
					g_AutomuteChecksDone = FALSE
					NETWORK_SET_SCRIPT_AUTOMUTED(FALSE)
					NET_NL()NET_PRINT("			- NETWORK_SET_SCRIPT_AUTOMUTED(FALSE) ")
					CLEAR_BIT(GlobalplayerBD_FM[iPlayerID].scoreData.iCheatStateBitset, CHEATBITSET_AUTOMUTED_BADSPORT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF


ENDPROC

PROC RUN_AUTOMUTE_CHECKS()
	g_AutomuteChecksDone = FALSE
ENDPROC

PROC APPLY_AUTO_MUTE()

	

	IF g_AutomuteChecksDone = FALSE
	OR HAS_SOMEONE_CHEAT_CHANGED()
	AND IS_PLAYER_IN_CORONA() = FALSE


//		INT I
//		PLAYER_INDEX PlayerCheck
//		FOR I = 0 TO NUM_NETWORK_PLAYERS-1
//		
//		
//		
//			PlayerCheck = INT_TO_NATIVE(PLAYER_INDEX, I)
//			
//			
//			#IF IS_DEBUG_BUILD 
//			IF PlayerCheck <> INVALID_PLAYER_INDEX()
//				IF IS_NET_PLAYER_OK(PlayerCheck, FALSE, TRUE)
//					NET_NL()NET_PRINT("APPLY_AUTO_MUTE: - ")
//					NET_NL()NET_PRINT("			- Player Name to check = ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
//					NET_NL()NET_PRINT("			- iPlayerID = ")NET_PRINT_INT(I)
//					NET_NL()NET_PRINT("			- IS_PLAYER_AUTOMUTED = ")NET_PRINT_BOOL(IS_PLAYER_AUTOMUTED(PlayerCheck))
//					NET_NL()NET_PRINT("			- IS_PLAYER_AUTOMUTED_BADSPORT = ")NET_PRINT_BOOL(IS_PLAYER_AUTOMUTED_BADSPORT(PlayerCheck))
//					NET_NL()NET_PRINT("			- HAS_SOMEONE_CHEAT_CHANGED = ")NET_PRINT_BOOL(HAS_SOMEONE_CHEAT_CHANGED())
//					NET_NL()NET_PRINT("			- NETWORK_PLAYER_IS_CHEATER = ")NET_PRINT_BOOL(NETWORK_PLAYER_IS_CHEATER())
//					NET_NL()NET_PRINT("			- NETWORK_PLAYER_IS_BADSPORT = ")NET_PRINT_BOOL(NETWORK_PLAYER_IS_BADSPORT())
//					NET_NL()NET_PRINT("			- HAS_SOMEONE_CHEAT_CHANGED = ")NET_PRINT_BOOL(HAS_SOMEONE_CHEAT_CHANGED())
//
//					NET_NL()NET_PRINT("			- g_AutomuteChecksDone = ")NET_PRINT_BOOL(g_AutomuteChecksDone)
//				ENDIF
//			ENDIF
//			#ENDIF
//
//			
//			
//			IF PlayerCheck <> INVALID_PLAYER_INDEX()
//				IF IS_NET_PLAYER_OK(PlayerCheck, FALSE, TRUE)
//
//					IF IS_PLAYER_AUTOMUTED(PlayerCheck)
//						
//						NETWORK_SET_PLAYER_MUTED(PlayerCheck, TRUE)
//						#IF IS_DEBUG_BUILD
//						NET_NL()NET_PRINT("			- IS_PLAYER_AUTOMUTED NETWORK_SET_PLAYER_MUTED = TRUE ")
//						NET_NL()
//						#ENDIF
//					ELSE
//						
//						#IF IS_DEBUG_BUILD
//						NET_NL()NET_PRINT("			- IS_PLAYER_AUTOMUTED NETWORK_SET_PLAYER_MUTED = FALSE ")
//						NET_NL()
//						#ENDIF
//					ENDIF
//				ENDIF
//				
//			ENDIF
//			
//			
//			IF IS_PLAYER_AUTOMUTED_BADSPORT(PlayerCheck)
//				IF PlayerCheck <> INVALID_PLAYER_INDEX()
//					IF IS_NET_PLAYER_OK(PlayerCheck, FALSE, FALSE)
//	
//						NETWORK_SET_PLAYER_MUTED(PlayerCheck, TRUE)
//						#IF IS_DEBUG_BUILD
//						NET_NL()NET_PRINT("			- IS_PLAYER_AUTOMUTED_BADSPORT NETWORK_SET_PLAYER_MUTED = TRUE ")
//						NET_NL()
//						#ENDIF
//
//					ENDIF
//				ENDIF
//			ELSE
//				#IF IS_DEBUG_BUILD
//				NET_NL()NET_PRINT("			- IS_PLAYER_AUTOMUTED_BADSPORT = FALSE ")
//				NET_NL()
//				#ENDIF
//			
//			ENDIF
//		
//		
//			MPGlobalsHud.iCheatStateBitset_LastFrame[I] = GlobalplayerBD_FM[I].scoreData.iCheatStateBitset
//		
//		ENDFOR
		
		IF GET_MP_BOOL_PLAYER_STAT(MPPLY_AUTOMUTE_MESSAGE) 
			IF IS_PLAYER_AUTOMUTED_BADSPORT(PLAYER_ID()) = FALSE
			AND IS_PLAYER_AUTOMUTED(PLAYER_ID()) = FALSE
				SET_MP_BOOL_PLAYER_STAT(MPPLY_AUTOMUTE_MESSAGE, FALSE)
			ENDIF
		ENDIF
		
		IF GET_MP_BOOL_PLAYER_STAT(MPPLY_AUTOMUTE_MESSAGE) = FALSE
			IF IS_PLAYER_AUTOMUTED_BADSPORT(PLAYER_ID())
				DISPLAY_LOCAL_TICKER_FOR_AUTOMUTE_BADSPORT()
				SET_MP_BOOL_PLAYER_STAT(MPPLY_AUTOMUTE_MESSAGE, TRUE)
			ELSE
				IF IS_PLAYER_AUTOMUTED(PLAYER_ID())
					DISPLAY_LOCAL_TICKER_FOR_AUTOMUTE()
					SET_MP_BOOL_PLAYER_STAT(MPPLY_AUTOMUTE_MESSAGE, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		
		g_AutomuteChecksDone = TRUE
	
	ENDIF
	


ENDPROC








FUNC BOOL SHOULD_GIVE_SHOP_DISCOUNT()
	
	IF IS_PC_VERSION()
		RETURN FALSE
	ENDIF

	IF HAS_IMPORTANT_STATS_LOADED()
		IF HAS_PLAYER_PLAYED_A_WEEK()
			
			INT TotalReports = GET_TOTAL_NUMBER_OF_REPORTS()
			INT TotalPositiveReports = GET_TOTAL_NUMBER_OF_POSITIVE_REPORTS()
			
			FLOAT TotalPositive = (TO_FLOAT(TotalPositiveReports)/TO_FLOAT(TotalReports))*100.0
			 
			IF TotalPositive > g_sMPTunables.iShopDiscountPercent
			AND TotalReports >= 8
				RETURN TRUE
			ENDIF
			
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


















////////////////////////////////////////////////////////
////	Remote player hacking checks	////////////////
////////////////////////////////////////////////////////
///    




FUNC INT GET_NEW_CRC_REPORT_INDEX( BOOL& IndexChecked[])
	INT I
	BOOL AreAnyChecksLeft = FALSE
	
	
	
	FOR I = 0 TO NUMBER_CRC_REPORT_CHECKS-1
		IF IndexChecked[I] = FALSE
			AreAnyChecksLeft = TRUE
			I = NUMBER_CRC_REPORT_CHECKS
		ENDIF
		
	ENDFOR
	
	IF AreAnyChecksLeft = FALSE
		RETURN 999
	ENDIF

	INT RandomNumToCheck
	
	
	FOR I = 0 TO 50
		
		RandomNumToCheck = GET_RANDOM_INT_IN_RANGE(0, NUMBER_CRC_REPORT_CHECKS)
		IF IndexChecked[RandomNumToCheck] = FALSE
			I = 51
			IndexChecked[RandomNumToCheck] = TRUE
			RETURN RandomNumToCheck
		ENDIF
	ENDFOR
	
	RETURN 999

ENDFUNC


FUNC BOOL RUN_CRC_REPORT_PLAYER_CHEAT_WATCHER(PLAYER_INDEX PlayerCheck, INT& Stages, SCRIPT_TIMER& aTimer, BOOL& IndexChecked[])
	
	
	
	
	
	//TRIGGER_FILE_CRC_HACKER_CHECK(PLAYER_INDEX receiver, STRING fileToHash)
	//fileToHash is the path of the file you want the guy to hash, from either 
	//hdd or disc (if the file is loaded into some structure into memory, and that 
	//structure is compromised, this check won't detect it). One example of fileToHash 
	//param would be "common\data\ai\weapons.meta"
	//does a crc check against an actual file



	//TRIGGER_TUNING_CRC_HACKER_CHECK(PLAYER_INDEX receiver, STRING tuningGroup, STRING tuningInstance)
	//this other function checksums the parsed file into memory instead
	//so it has a more restricted possible values
	//instead of passing a file path, you need to pass the tuning instance and 
	//tuning group (in code, most of this "parse and put into memory" files are 
	//from a class called CTuning, and belong to a CTuningManager)
	//any meta file that contains the <CTuningFile> tags
	//<Name>CAmbientLookAt</Name> inside of defaulttasks.meta
	//X:\gta5\build\dev\common\data\tune\defaulttasks.meta(3):<CTuningFile>
	//{group, instance} tuple





	//TRIGGER_PLAYER_CRC_HACKER_CHECK
//	you'll find the HASHABLE_SYSTEM enum in the same script header, 
//with comments of what subsystem has to be depending on "system" for example
//if you pass CRC_ITEM_INFO_DATA then the other int has to be the hash of the item name, 
//like any weapon or ammo item, like hash(WEAPON_UNARMED) or hash("WEAPON_PISTOL")
//script already has those things as WEAPON_TYPE
//hashes only the parsed PISTOL memory in this example



//Code handles the result, these functions returning true means the player has been checked. 


	INT RandomWaitBetweenChecks = GET_RANDOM_INT_IN_RANGE(g_sMPTunables.CRC_BetweenChecksTime_Start, g_sMPTunables.CRC_BetweenChecksTime_End)

	SWITCH Stages
	
		CASE 0
			IF PlayerCheck <> PLAYER_ID() //Remote players only. 
			AND NETWORK_PLAYER_IS_CHEATER() = FALSE
				#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
					NET_PRINT(" is being checked for CRC Hacking ")
				#ENDIF
				IndexChecked[Stages] = TRUE
				Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
			ELSE
				#IF IS_DEBUG_BUILD
					IF PlayerCheck = PLAYER_ID()
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is me, end chec ")
					ENDIF
					IF NETWORK_PLAYER_IS_CHEATER()
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is already a cheater, end chec ")
					ENDIF
				#ENDIF
				Stages = 999
			ENDIF
		BREAK
		
		CASE 1
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, RandomWaitBetweenChecks)
				IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH("fmmc_launcher"))
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is being checked for CRC Hacking ")
					#ENDIF
					IndexChecked[Stages] = TRUE
					Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(fmmc_launcher)) check done  ")NET_NL()
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(fmmc_launcher)) RUNNING CHECK  ")NET_NL()
					#ENDIF
				ENDIF
			ENDIF
			
			
		BREAK
		
		CASE 2
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, RandomWaitBetweenChecks)
				IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH("fm_deathmatch_controler"))
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is being checked for CRC Hacking ")
					#ENDIF
					IndexChecked[Stages] = TRUE
					Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(fm_deathmatch_controler)) check done  ")NET_NL()
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(fm_deathmatch_controler)) RUNNING CHECK  ")NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 3
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, RandomWaitBetweenChecks)
				IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, FREEMODE_HASH())
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is being checked for CRC Hacking ")
					#ENDIF
					IndexChecked[Stages] = TRUE
					Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(freemode)) check done  ")NET_NL()
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(freemode)) RUNNING CHECK  ")NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		
		CASE 4
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, RandomWaitBetweenChecks)
				IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH("fm_hideout_controler"))
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is being checked for CRC Hacking ")
					#ENDIF
					IndexChecked[Stages] = TRUE
					Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(fm_hideout_controler)) check done  ")NET_NL()
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(fm_hideout_controler)) RUNNING CHECK  ")NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		
		CASE 5
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, RandomWaitBetweenChecks)
				IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH("FM_Horde_Controler"))
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is being checked for CRC Hacking ")
					#ENDIF
					IndexChecked[Stages] = TRUE
					Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(FM_Horde_Controler)) check done  ")NET_NL()
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(FM_Horde_Controler)) RUNNING CHECK  ")NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 6
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, RandomWaitBetweenChecks)
				IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, GET_FM_MISSION_CONTROLLER_SCRIPT_NAME_HASH())
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is being checked for CRC Hacking ")
					#ENDIF
					IndexChecked[Stages] = TRUE
					Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(fm_mission_controler)) check done  ")NET_NL()
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(fm_mission_controler)) RUNNING CHECK  ")NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 7
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, RandomWaitBetweenChecks)
				IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH("fm_race_controler"))
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is being checked for CRC Hacking ")
					#ENDIF
					IndexChecked[Stages] = TRUE
					Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(fm_race_controler)) check done  ")NET_NL()
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(fm_race_controler)) RUNNING CHECK  ")NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 8
		
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, RandomWaitBetweenChecks)
				IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH("am_imp_exp"))
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is being checked for CRC Hacking ")
					#ENDIF
					IndexChecked[Stages] = TRUE
					Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(am_imp_exp)) check done  ")NET_NL()
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(am_imp_exp)) RUNNING CHECK  ")NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 9
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, RandomWaitBetweenChecks)
				IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH("achievement_controller"))
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is being checked for CRC Hacking ")
					#ENDIF
					IndexChecked[Stages] = TRUE
					Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(achievement_controller)) check done  ")NET_NL()
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(achievement_controller)) RUNNING CHECK  ")NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		

		CASE 10
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, RandomWaitBetweenChecks)
				#if USE_CLF_DLC
					IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH("social_controllerCLF"))					
						#IF IS_DEBUG_BUILD
							NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
							NET_PRINT(" is being checked for CRC Hacking ")
						#ENDIF
						IndexChecked[Stages] = TRUE
						Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
					
						#IF IS_DEBUG_BUILD
							NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
							NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(social_controllerAGT)) check done  ")NET_NL()
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
							NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(social_controllerAGT)) RUNNING CHECK  ")NET_NL()
						#ENDIF
					ENDIF
				#endif
				#if USE_NRM_DLC
					IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH("social_controllerNRM"))
					
						#IF IS_DEBUG_BUILD
							NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
							NET_PRINT(" is being checked for CRC Hacking ")
						#ENDIF
						IndexChecked[Stages] = TRUE
						Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
					
						#IF IS_DEBUG_BUILD
							NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
							NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(social_controllerNRM)) check done  ")NET_NL()
						#ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
							NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
							NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(social_controllerNRM)) RUNNING CHECK  ")NET_NL()
						#ENDIF
					ENDIF
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
				IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH("social_controller"))
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is being checked for CRC Hacking ")
					#ENDIF
					IndexChecked[Stages] = TRUE
					Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(social_controller)) check done  ")NET_NL()
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(social_controller)) RUNNING CHECK  ")NET_NL()
					#ENDIF
				ENDIF
				#endif
				#endif
				
			ENDIF
		BREAK

		CASE 11
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, RandomWaitBetweenChecks)
				IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH("stats_controller"))
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is being checked for CRC Hacking ")
					#ENDIF
					IndexChecked[Stages] = TRUE
					Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(stats_controller)) check done  ")NET_NL()
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(stats_controller)) RUNNING CHECK  ")NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		
		
		CASE 12
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, RandomWaitBetweenChecks)
				IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH("main_persistent"))
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is being checked for CRC Hacking ")
					#ENDIF
					IndexChecked[Stages] = TRUE
					Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(main_persistent)) check done  ")NET_NL()
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(main_persistent)) RUNNING CHECK  ")NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 13
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, RandomWaitBetweenChecks)
				IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH("net_cloud_mission_loader"))
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is being checked for CRC Hacking ")
					#ENDIF
					IndexChecked[Stages] = TRUE
					Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(net_cloud_mission_loader)) check done  ")NET_NL()
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(net_cloud_mission_loader)) RUNNING CHECK  ")NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 14
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, RandomWaitBetweenChecks)
				IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH("fm_maintain_cloud_header_data"))
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is being checked for CRC Hacking ")
					#ENDIF
					IndexChecked[Stages] = TRUE
					Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(fm_maintain_cloud_header_data)) check done  ")NET_NL()
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(fm_maintain_cloud_header_data)) RUNNING CHECK  ")NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 15
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, RandomWaitBetweenChecks)
				IF TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH("fm_survival_controller"))
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" is being checked for CRC Hacking ")
					#ENDIF
					IndexChecked[Stages] = TRUE
					Stages = GET_NEW_CRC_REPORT_INDEX( IndexChecked)
				
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(fm_survival_controller)) check done  ")NET_NL()
					#ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
						NET_PRINT(" 	- TRIGGER_PLAYER_CRC_HACKER_CHECK(PlayerCheck, CRC_SCRIPT_RESOURCE, HASH(fm_survival_controller)) RUNNING CHECK  ")NET_NL()
					#ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		
		
		
		CASE 999
			#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("RUN_REPORT_PLAYER_CHEAT_WATCHER ")NET_PRINT(GET_PLAYER_NAME(PlayerCheck))
				NET_PRINT(" 	----------- FINISHED ----------  ")NET_NL()
				#ENDIF
			Stages = 1000
		BREAK
		
		
		CASE 1000
			Stages = 0
			RETURN TRUE
		BREAK
		
		
		DEFAULT 
			Stages = GET_NEW_CRC_REPORT_INDEX(IndexChecked)
		BREAK
	
	
	ENDSWITCH

	

//	REMOTE_CHEATER_PLAYER_DETECTED // Should only be used for wholly script tracking cheating. 


	RETURN FALSE
	

ENDFUNC

PROC RESET_PLAYER_CRC_CHECK_STAGES(BOOL& IndexChecked[])

	INT I
	FOR I = 0 TO NUMBER_CRC_REPORT_CHECKS-1
		IndexChecked[I] = FALSE
	ENDFOR
	
ENDPROC

PROC REFRESH_CRC_REPORT_PLAYERS(BOOL& IndexChecked[])
	PRINTLN("RUN_CRC_REPORT_ALL_PLAYER_CHEAT_WATCHER: REFRESH_CRC_REPORT_PLAYERS - called")
	g_PlayerCRCCheckExternal = 0
	
	RESET_PLAYER_CRC_CHECK_STAGES(IndexChecked)
ENDPROC

PROC RUN_CRC_REPORT_ALL_PLAYER_CHEAT_WATCHER( INT& Stages, SCRIPT_TIMER& aTimer, BOOL& IndexChecked[], INT& LastFrameNumberParticipants)

	INT CurrentNumberOfPart = NETWORK_GET_NUM_PARTICIPANTS()

	IF LastFrameNumberParticipants <> CurrentNumberOfPart
		NET_NL()NET_PRINT("***** RUN_CRC_REPORT_ALL_PLAYER_CHEAT_WATCHER - PLAYER DIFF RESET CHECKING **********")
		Stages = 0
		REFRESH_CRC_REPORT_PLAYERS(IndexChecked)
	ENDIF
	
	IF g_PlayerCRCCheckExternal < CurrentNumberOfPart

		PLAYER_INDEX PedToCheck = INT_TO_NATIVE(PLAYER_INDEX, g_PlayerCRCCheckExternal)
		IF IS_NET_PLAYER_OK(PedToCheck)
			IF RUN_CRC_REPORT_PLAYER_CHEAT_WATCHER(PedToCheck, Stages, aTimer, IndexChecked)
				g_PlayerCRCCheckExternal++
				RESET_NET_TIMER(aTimer)
				RESET_PLAYER_CRC_CHECK_STAGES(IndexChecked)
				
			ENDIF
		ENDIF
	ELSE
		INT RandomWaitBeforeCheckReset = GET_RANDOM_INT_IN_RANGE(g_sMPTunables.CRC_DonePlayersReset_Start, g_sMPTunables.CRC_DonePlayersReset_End)
		IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(aTimer, RandomWaitBeforeCheckReset)
			NET_NL()NET_PRINT("***** RUN_CRC_REPORT_ALL_PLAYER_CHEAT_WATCHER - TIMER RESET CHECKING **********")
			REFRESH_CRC_REPORT_PLAYERS(IndexChecked)
			Stages = 0
		ENDIF
	ENDIF

	LastFrameNumberParticipants = CurrentNumberOfPart

ENDPROC

























////////////////////////////////////////////////////////
////	Playing FM offline	////////////////
////////////////////////////////////////////////////////
///    




PROC DISPLAY_LOCAL_TICKER_OFFLINE_PLAY(BOOL& bDisplayOfflineTicker, SCRIPT_TIMER& DisplayOfflineTimer)
	IF bDisplayOfflineTicker = FALSE
		IF HAS_NET_TIMER_EXPIRED(DisplayOfflineTimer, 5000)
		AND NOT NETWORK_IS_IN_MP_CUTSCENE()
			IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
				IF IS_ANY_MP_CONTENT_REDUCED_DUE_TO_PERMISSIONS() = TRUE
				
					IF  SCRIPT_IS_CLOUD_AVAILABLE() = FALSE
						bDisplayOfflineTicker = TRUE
						EXIT
					ENDIF
				
					STRING Message = "CONT_WORLD"
//					IF ALLOWED_TO_GET_EVERYONE_CONTENT()
//						Message = "CONT_WORLD"
//					IF NETWORK_IS_CLOUD_AVAILABLE() = FALSE
//						Message = "CONT_CLOUDFAIL"
					IF ALLOWED_TO_GET_FRIEND_CONTENT()
						Message = "CONT_FRIEN"
					ELIF ALLOWED_TO_GET_ROCKSTAR_CONTENT()
						
						IF IS_PS3_VERSION() 
							IF NETWORK_HAVE_USER_CONTENT_PRIVILEGES(PC_FRIENDS) = FALSE
								Message = "CONT_ROCK"	//Parental Restrictions (Age)
								
								#IF USE_FINAL_PRINTS
								PRINTLN_FINAL("DISPLAY_LOCAL_TICKER_OFFLINE_PLAY (PS3) - using CONT_ROCK.")
								#ENDIF
								PRINTLN("DISPLAY_LOCAL_TICKER_OFFLINE_PLAY (PS3) - using CONT_ROCK.")
							
							ELIF NETWORK_HAVE_COMMUNICATION_PRIVILEGES(PC_FRIENDS) = FALSE
								Message = "CONT_CHATR"	//Chat Restrictions
							ENDIF
						ELIF IS_PLAYSTATION_PLATFORM()
						
							IF NETWORK_HAVE_USER_CONTENT_PRIVILEGES(PC_FRIENDS) = FALSE
								IF NETWORK_HAVE_COMMUNICATION_PRIVILEGES(PC_FRIENDS) = FALSE
									Message = "CONT_ROCK"	//Parental Restrictions (Age) & CHAT blocked
									
									#IF USE_FINAL_PRINTS
									PRINTLN_FINAL("DISPLAY_LOCAL_TICKER_OFFLINE_PLAY (PS)- using CONT_ROCK.")
									#ENDIF
									PRINTLN("DISPLAY_LOCAL_TICKER_OFFLINE_PLAY (PS)- using CONT_ROCK.")
									
								ELSE
									Message = "CONT_ROCK2"	//Parental Restrictions (Age) & Chat allowed
								ENDIF
							ELIF NETWORK_HAVE_COMMUNICATION_PRIVILEGES(PC_FRIENDS) = FALSE
								Message = "CONT_CHATR"	//Chat Restrictions
							ENDIF
						ELIF IS_XBOX_PLATFROM() 
							Message = "CONT_ROCK"
							
							#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("DISPLAY_LOCAL_TICKER_OFFLINE_PLAY (XB)- using CONT_ROCK.")
							#ENDIF
							PRINTLN("DISPLAY_LOCAL_TICKER_OFFLINE_PLAY (XB)- using CONT_ROCK.")
					
							NETWORK_RESOLVE_PRIVILEGE_USER_CONTENT()
						
						ELSE
						
							#IF USE_FINAL_PRINTS
							PRINTLN_FINAL("DISPLAY_LOCAL_TICKER_OFFLINE_PLAY (default)- using CONT_ROCK.")
							#ENDIF
							PRINTLN("DISPLAY_LOCAL_TICKER_OFFLINE_PLAY (default)- using CONT_ROCK.")
						
							Message = "CONT_ROCK"
						ENDIF
						
					ELIF ALLOWED_TO_GET_PERSONAL_CONTENT()
						Message = "CONT_PERSO"
					ELIF ALLOWED_TO_GET_DISC_CONTENT()
						Message = "CONT_DISC"
					ENDIF
				
		
					BEGIN_TEXT_COMMAND_THEFEED_POST(Message)
					END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)	
					bDisplayOfflineTicker = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC























////////////////////////////////////////////////////////
////	DIG Cheat Tracking	////////////////
////////////////////////////////////////////////////////
///    


#IF IS_DEBUG_BUILD

FUNC STRING GET_REMOTE_PLAYER_CHEAT_LIST_STRING(REMOTE_PLAYER_CHEAT_LIST aCheatList)


	SWITCH aCheatList
		CASE REMOTE_PLAYER_LIST_PLUG_PULLED_DM 		RETURN "REMOTE_PLAYER_LIST_PLUG_PULLED_DM"
		CASE REMOTE_PLAYER_LIST_PLUG_PULLED_RACE 		RETURN "REMOTE_PLAYER_LIST_PLUG_PULLED_RACE"
		CASE REMOTE_PLAYER_LIST_PLUG_PULLED_MINIGAME 		RETURN "REMOTE_PLAYER_LIST_PLUG_PULLED_MINIGAME"
		CASE REMOTE_PLAYER_LIST_PLUG_PULLED_MC_MISSION 		RETURN "REMOTE_PLAYER_LIST_PLUG_PULLED_MC_MISSION"
		CASE REMOTE_PLAYER_LIST_VOTED_OUT_OF_SESSION 		RETURN "REMOTE_PLAYER_LIST_VOTED_OUT_OF_SESSION"

	
	ENDSWITCH
	RETURN ""
ENDFUNC

#ENDIF

FUNC INT GET_PLAYERS_CURRENT_MISSION_TYPE(PLAYER_INDEX PlayerCheating)

	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(PlayerCheating)].iCurrentMissionType


ENDFUNC


FUNC BOOL WAS_PLAYER_KICKED(PLAYER_INDEX PlayerCheating)
	RETURN GlobalPlayerBD_Kicking[NATIVE_TO_INT(PlayerCheating)].bAmIGoingToBeKicked
ENDFUNC


FUNC BOOL DOES_CHEAT_TRACKER_CARE_ABOUT_MISSION_TYPE(PLAYER_INDEX PlayerCheating)
	INT MissionType = GET_PLAYERS_CURRENT_MISSION_TYPE(PlayerCheating)
	NET_NL()NET_PRINT("DOES_CHEAT_TRACKER_CARE_ABOUT_MISSION_TYPE - MissionType = ")NET_PRINT_INT(MissionType)
	SWITCH MissionType
		
		CASE FMMC_TYPE_MISSION	
		CASE FMMC_TYPE_MISSION_CTF
		CASE FMMC_TYPE_DEATHMATCH	
		CASE FMMC_TYPE_IMPROMPTU_DM
		CASE FMMC_TYPE_RACE	
		CASE FMMC_TYPE_RACE_TO_POINT
		CASE FMMC_TYPE_SURVIVAL	
		CASE FMMC_TYPE_GANGHIDEOUT
		CASE FMMC_TYPE_MISSION_LTS
		CASE FMMC_TYPE_MISSION_COOP
		CASE FMMC_TYPE_BASE_JUMP
		CASE FMMC_TYPE_ARMS_SMUGGLING
		
		CASE FMMC_TYPE_MG				
		CASE FMMC_TYPE_MG_GOLF			
		CASE FMMC_TYPE_MG_TENNIS			
		CASE FMMC_TYPE_MG_SHOOTING_RANGE	
		CASE FMMC_TYPE_MG_DARTS			
		CASE FMMC_TYPE_MG_ARM_WRESTLING	
		CASE FMMC_TYPE_MG_RANGE_GRID		
		CASE FMMC_TYPE_MG_RANGE_RANDOM	
		CASE FMMC_TYPE_MG_RANGE_COVERED	
			RETURN TRUE
		BREAK
	
	ENDSWITCH
	RETURN FALSE
			
ENDFUNC

FUNC REMOTE_PLAYER_CHEAT_LIST GET_PLAYERS_CURRENT_CHEAT_BADSPORT_TYPE(PLAYER_INDEX PlayerCheating)
	INT MissionType = GET_PLAYERS_CURRENT_MISSION_TYPE(PlayerCheating)

	SWITCH MissionType
		
		CASE FMMC_TYPE_MISSION	
		CASE FMMC_TYPE_MISSION_CTF
			RETURN REMOTE_PLAYER_LIST_PLUG_PULLED_MC_MISSION	
		BREAK
		
		
		CASE FMMC_TYPE_DEATHMATCH	
		CASE FMMC_TYPE_IMPROMPTU_DM
			RETURN REMOTE_PLAYER_LIST_PLUG_PULLED_DM	
		BREAK
		
		
		CASE FMMC_TYPE_RACE	
		CASE FMMC_TYPE_RACE_TO_POINT
			RETURN REMOTE_PLAYER_LIST_PLUG_PULLED_RACE	
		BREAK
		
		
		CASE FMMC_TYPE_SURVIVAL	
		CASE FMMC_TYPE_GANGHIDEOUT
		//CASE FMMC_TYPE_TRIATHLON
		CASE FMMC_TYPE_BASE_JUMP
		CASE FMMC_TYPE_ARMS_SMUGGLING
		CASE FMMC_TYPE_MISSION_LTS
		CASE FMMC_TYPE_MISSION_COOP
		CASE FMMC_TYPE_MG
		CASE FMMC_TYPE_MG_GOLF			
		CASE FMMC_TYPE_MG_TENNIS			
		CASE FMMC_TYPE_MG_SHOOTING_RANGE	
		CASE FMMC_TYPE_MG_DARTS			
		CASE FMMC_TYPE_MG_ARM_WRESTLING	
		CASE FMMC_TYPE_MG_RANGE_GRID		
		CASE FMMC_TYPE_MG_RANGE_RANDOM	
		CASE FMMC_TYPE_MG_RANGE_COVERED	
		
			RETURN REMOTE_PLAYER_LIST_PLUG_PULLED_MINIGAME
		BREAK
	
	ENDSWITCH
	
	RETURN REMOTE_PLAYER_LIST_NONE

ENDFUNC


PROC RUN_REMOTE_PLAYER_CHEAT_BADSPORT_CHECKS(GAMER_HANDLE &PlayerLeftHandle, REMOTE_PLAYER_CHEAT_LIST Reason, INT secondInt = 0)

	SWITCH Reason
	
		CASE REMOTE_PLAYER_LIST_PLUG_PULLED_DM
			BAD_SPORT_PLAYER_LEFT_DETECTED(PlayerLeftHandle, ENUM_TO_INT(Reason), secondInt)
		BREAK
		CASE REMOTE_PLAYER_LIST_PLUG_PULLED_MC_MISSION
			BAD_SPORT_PLAYER_LEFT_DETECTED(PlayerLeftHandle, ENUM_TO_INT(Reason), secondInt)
		BREAK
		CASE REMOTE_PLAYER_LIST_PLUG_PULLED_MINIGAME
			BAD_SPORT_PLAYER_LEFT_DETECTED(PlayerLeftHandle, ENUM_TO_INT(Reason), secondInt)
		BREAK
		CASE REMOTE_PLAYER_LIST_PLUG_PULLED_RACE
			BAD_SPORT_PLAYER_LEFT_DETECTED(PlayerLeftHandle, ENUM_TO_INT(Reason), secondInt)
		BREAK
		CASE REMOTE_PLAYER_LIST_VOTED_OUT_OF_SESSION
			BAD_SPORT_PLAYER_LEFT_DETECTED(PlayerLeftHandle, ENUM_TO_INT(Reason), secondInt)
		BREAK
	
	ENDSWITCH

ENDPROC


PROC RUN_LEFT_SCRIPT_CHEATER_CHECKS(PLAYER_INDEX PlayerCheating, GAMER_HANDLE &PlayerHandle)

	IF DOES_CHEAT_TRACKER_CARE_ABOUT_MISSION_TYPE(PlayerCheating)
	
		REMOTE_PLAYER_CHEAT_LIST CheatType = GET_PLAYERS_CURRENT_CHEAT_BADSPORT_TYPE(PlayerCheating)
		INT MissionType = GET_PLAYERS_CURRENT_MISSION_TYPE(PlayerCheating)
		
		#IF IS_DEBUG_BUILD
		NET_NL()NET_PRINT("RUN_LEFT_SCRIPT_CHEATER_CHECKS: ")
		NET_NL()NET_PRINT("			- CheatType = ")NET_PRINT(GET_REMOTE_PLAYER_CHEAT_LIST_STRING(CheatType))
		NET_NL()NET_PRINT("			- Player = ")NET_PRINT(GET_PLAYER_NAME(PlayerCheating))
		NET_NL()NET_PRINT("			- MissionType = ")NET_PRINT_INT(MissionType)
		#ENDIF

		RUN_REMOTE_PLAYER_CHEAT_BADSPORT_CHECKS(PlayerHandle, CheatType, MissionType)
		
	ENDIF
	
	
	IF WAS_PLAYER_KICKED(PlayerCheating)
		RUN_REMOTE_PLAYER_CHEAT_BADSPORT_CHECKS(PlayerHandle, REMOTE_PLAYER_LIST_VOTED_OUT_OF_SESSION)
	ENDIF

ENDPROC









