
USING "Net_stat_system.sch"
USING "Weapons_public.sch"
USING "Tattoo_private.sch"
USING "Net_stat_info.sch"
USING "Transition_Controller.sch"

CONST_INT RANK_HEALTH_VALUE_0 238
CONST_INT RANK_HEALTH_VALUE_1 256
CONST_INT RANK_HEALTH_VALUE_2 274
CONST_INT RANK_HEALTH_VALUE_3 292
CONST_INT RANK_HEALTH_VALUE_4 310
CONST_INT RANK_HEALTH_VALUE_5 328



FUNC BOOL IS_CURRENT_HEALTH_LESS_THAN(INT Percentage, INT NewMaxHealthLevel)

	INT Current_Health = (GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_HEALTH)-100) //65
	INT PercentageLimit = FLOOR(TO_FLOAT(NewMaxHealthLevel)-100) //145
	FLOAT PercentageAt = (TO_FLOAT(Current_Health)/TO_FLOAT(PercentageLimit))*100   //44
	

	NET_NL()NET_PRINT("		- Current_Health = ")NET_PRINT_INT(Current_Health)
	NET_NL()NET_PRINT("		- PercentageLimit = ")NET_PRINT_INT(PercentageLimit)
	NET_NL()NET_PRINT("		- PercentageAt = ")NET_PRINT_FLOAT(PercentageAt)


	RETURN (PercentageAt < Percentage) //Less than 50% give 50% at spawn. Les todo 1156558

ENDFUNC



PROC GIVE_PLAYER_HEALTH_AT_SPAWN(INT NewMaxHealthLevel)
	
	
	
//	IF IS_CURRENT_HEALTH_LESS_THAN(50, NewMaxHealthLevel) //Less than 50% give 50% at spawn. Les todo 1156558
//	AND GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_HEALTH) != 0
//		SET_ENTITY_HEALTH(PLAYER_PED_ID(), FLOOR(TO_FLOAT(NewMaxHealthLevel)/2))
//		
//		DEBUG_PRINTCALLSTACK()
//		NET_NL()NET_PRINT("GIVE_PLAYER_HEALTH_AT_SPAWN SET_ENTITY_HEALTH at 50% = ")NET_PRINT_INT(FLOOR(TO_FLOAT(NewMaxHealthLevel)/2))
//		NET_NL()NET_PRINT("		- GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_HEALTH) = ")NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_HEALTH))
//		NET_NL()NET_PRINT("		- FLOOR(TO_FLOAT(MaxHealth)/2) = ")NET_PRINT_INT(FLOOR(TO_FLOAT(NewMaxHealthLevel)/2))
//
//	ELSE
		SET_ENTITY_HEALTH(PLAYER_PED_ID(), NewMaxHealthLevel)
		
		DEBUG_PRINTCALLSTACK()
		NET_NL()NET_PRINT("GIVE_PLAYER_HEALTH_AT_SPAWN SET_ENTITY_HEALTH full health = ")NET_PRINT_INT(NewMaxHealthLevel)
		NET_NL()NET_PRINT("		- GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_HEALTH) = ")NET_PRINT_INT(GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_HEALTH))
		NET_NL()NET_PRINT("		- FLOOR(TO_FLOAT(MaxHealth)/2) = ")NET_PRINT_INT(FLOOR(TO_FLOAT(NewMaxHealthLevel)/2))
//	ENDIF
	
	
ENDPROC


PROC SET_NEW_HEALTH_HUD_LEVEL(INT NewMaxHealthLevel, INT TotalMaximumHealthLevel, BOOL bIsAtSpawn = TRUE)


	FLOAT fTotalMaximumHealthLevel =  TO_FLOAT(TotalMaximumHealthLevel) 
	FLOAT fNewMaxHealthLevel =  TO_FLOAT(NewMaxHealthLevel)
	
	TotalMaximumHealthLevel = CEIL(fTotalMaximumHealthLevel)
	NewMaxHealthLevel = CEIL(fNewMaxHealthLevel)
	
	SET_PED_MAX_HEALTH(PLAYER_PED_ID(), ROUND(NewMaxHealthLevel * g_sMPTunables.fMaxHealthMultiplier))
	
	IF bIsAtSpawn
		GIVE_PLAYER_HEALTH_AT_SPAWN(ROUND(NewMaxHealthLevel * g_sMPTunables.fMaxHealthMultiplier))
	ENDIF
	
	SET_MAX_HEALTH_HUD_DISPLAY(ROUND(TotalMaximumHealthLevel * g_sMPTunables.fMaxHealthMultiplier))
	
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	NET_NL()NET_PRINT("SET_NEW_HEALTH_HUD_LEVEL g_sMPTunables.fMaxHealthMultiplier = ")NET_PRINT_FLOAT(g_sMPTunables.fMaxHealthMultiplier)	
	NET_NL()NET_PRINT("SET_NEW_HEALTH_HUD_LEVEL NewMaxHealthLevel = ")NET_PRINT_INT(NewMaxHealthLevel)
	NET_NL()NET_PRINT("SET_NEW_HEALTH_HUD_LEVEL TotalMaximumHealthLevel = ")NET_PRINT_INT((TotalMaximumHealthLevel))
	#ENDIF
ENDPROC


PROC SET_NEW_ARMOUR_HUD_LEVEL(INT NewMaxArmourLevel, INT TotalMaximumArmourLevel)

	SET_PLAYER_MAX_ARMOUR(PLAYER_ID(), TotalMaximumArmourLevel)
	
	IF NOT g_bFM_ON_TEAM_MISSION // disable during deathmatch 1550260
	AND NOT g_b_On_Deathmatch 
	//and they are not on a mission
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_INVALID
		SET_PED_ARMOUR (PLAYER_PED_ID(), NewMaxArmourLevel)
		NET_NL()NET_PRINT("SET_NEW_ARMOUR_HUD_LEVEL Do not give armor in Deathmatch KW ")NET_PRINT_INT(NewMaxArmourLevel)
	ENDIF
//	FLOAT ScaleformValue = ((TO_FLOAT(NewMaxArmourLevel)/TO_FLOAT(TotalMaximumArmourLevel))*100)
//	INT iScaleformValue = FLOOR(ScaleformValue)
	SET_MAX_ARMOUR_HUD_DISPLAY(TotalMaximumArmourLevel)
	
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	NET_NL()NET_PRINT("SET_NEW_ARMOUR_LEVEL NewMaxArmourLevel = ")NET_PRINT_INT(NewMaxArmourLevel)
	NET_NL()NET_PRINT("SET_NEW_ARMOUR_LEVEL TotalMaximumArmourLevel = ")NET_PRINT_INT((TotalMaximumArmourLevel))
//	NET_NL()NET_PRINT("SET_NEW_ARMOUR_LEVEL iScaleformValue = ")NET_PRINT_INT((iScaleformValue))
	#ENDIF
ENDPROC


FUNC WEAPON_TYPE GET_PLAYER_CURRENT_HELD_WEAPON()
	RETURN gweapon_type_CurrentlyHeldWeapon

ENDFUNC

PROC SET_PLAYER_CURRENT_HELD_WEAPON(WEAPON_TYPE aWeapon)
//	NET_NL()NET_PRINT("SET_PLAYER_CURRENT_HELD_WEAPON SET ")NET_PRINT(GET_WEAPON_NAME(aWeapon))
	#IF IS_DEBUG_BUILD
//	PRINTLN("SET_PLAYER_CURRENT_HELD_WEAPON called with", GET_WEAPON_NAME(aWeapon), " Callstack...")
//	DEBUG_PRINTCALLSTACK()
	#ENDIF
	gweapon_type_CurrentlyHeldWeapon = aWeapon
	
	PED_INDEX piPlayer = PLAYER_PED_ID()
	IF DOES_ENTITY_EXIST(piPlayer)
	AND NOT IS_ENTITY_DEAD(piPlayer)
		IF IS_WEAPON_VALID(aWeapon)
		AND HAS_PED_GOT_WEAPON(piPlayer, gweapon_type_CurrentlyHeldWeapon)
			g_iCurrentlyHeldWeapon_Ammo = GET_AMMO_IN_PED_WEAPON(piPlayer, gweapon_type_CurrentlyHeldWeapon)
		ENDIF
	ENDIF
ENDPROC


/////////////////////////
/////////HEALTH//////////
/////////////////////////
 




/////////////////////////
/////////ARMOUR//////////
/////////////////////////

PROC GIVE_PLAYER_ALL_UNLOCKED_HEALTH()
	
	FLOAT health_value
	SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(PLAYER_ID(), 1.00)
	
	health_value = 1.00
	
	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_1)
		health_value = 1.02

		NET_NL()NET_PRINT("IS_MP_HEALTH_UNLOCKED - PLAYERHEALTH_HEALTHUPGRADE_1")
	ENDIF

	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_2)
		health_value = 1.04

		NET_NL()NET_PRINT("IS_MP_HEALTH_UNLOCKED - PLAYERHEALTH_HEALTHUPGRADE_2")
	ENDIF

	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_3)
		health_value = 1.06

		NET_NL()NET_PRINT("IS_MP_HEALTH_UNLOCKED - PLAYERHEALTH_HEALTHUPGRADE_3")

	ENDIF
	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_4)
		health_value = 1.08

		NET_NL()NET_PRINT("IS_MP_HEALTH_UNLOCKED - PLAYERHEALTH_HEALTHUPGRADE_4")
	ENDIF
	

	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_5)
		health_value = 1.1

		NET_NL()NET_PRINT("IS_MP_HEALTH_UNLOCKED - PLAYERHEALTH_HEALTHUPGRADE_5")

	ENDIF

	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_6)
		health_value = 1.12


	ENDIF

	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_7)
		health_value = 1.14

			
	ENDIF
	
	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_8)
		health_value = 1.16


	ENDIF

	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_9)
		health_value = 1.18

		
	ENDIF

	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_10)
		health_value = 1.2

		NET_NL()NET_PRINT("IS_MP_HEALTH_UNLOCKED - PLAYERHEALTH_HEALTHUPGRADE_10")
	ENDIF
	

	NET_NL()NET_PRINT("GIVE_PLAYER_ALL_UNLOCKED_HEALTH - g_sMPTunables.fHealthRegenMaxMultiplier  = ")
	NET_PRINT_FLOAT(g_sMPTunables.fHealthRegenMaxMultiplier)	

	
	IF g_sMPTunables.fHealthRegenMaxMultiplier != 1.0
		//IF health_value > g_sMPTunables.fHealthRegenMaxMultiplier 
			health_value = g_sMPTunables.fHealthRegenMaxMultiplier 
			NET_NL()NET_PRINT("IS_MP_HEALTH_UNLOCKED - applying max health modifier cap")
		//ENDIF
		
	ENDIF
	
	health_value *= g_sMPTunables.fHealthRegenRateMultiplier
//	g_sMPTunables.fHealthRegenMaxMultiplier
	
	NET_NL()NET_PRINT("GIVE_PLAYER_ALL_UNLOCKED_HEALTH - SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER = ")
	NET_PRINT_FLOAT(health_value)
	SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(PLAYER_ID(), health_value)


ENDPROC

FUNC INT GET_PLAYER_CURRENT_MAX_ARMOUR()
	//FLOAT armour_mulitplier = (g_structMPScriptTunables.fArmourMaxMultiplier) * (g_structMPScriptTunables.fArmourMinMultiplier)

	
		
	INT CurrentLevelArmour 
//	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_ARMOURUPGRADE_1)
//		CurrentLevelArmour = 18
//	ENDIF	
//	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_ARMOURUPGRADE_2)
//		CurrentLevelArmour = 26
//	ENDIF	
//	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_ARMOURUPGRADE_3)
//		CurrentLevelArmour = 34
//	ENDIF	
//	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_ARMOURUPGRADE_4)
//		CurrentLevelArmour = 42
//	ENDIF	
//	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_ARMOURUPGRADE_5)
		CurrentLevelArmour = 50 
//	ENDIF	
	
	RETURN CurrentLevelArmour
	
ENDFUNC


PROC GIVE_PLAYER_ALL_UNLOCKED_HEALTH_FREEMODE(BOOL bIsAtSpawn = FALSE)
	

	
	
	//CurrentLevelArmour = GET_PLAYER_CURRENT_MAX_ARMOUR()
	//  bug 1402344

	FLOAT armour_mulitplier = g_sMPTunables.fArmourMaxMultiplier //
	

	
	//INT CurrentLevelArmour = 50 //* ROUND(armour_mulitplier)
	INT CurrentLevelArmour = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_CURRENT_ARMOUR) //* ROUND(armour_mulitplier)
	INT CurrentLevelHealth = RANK_HEALTH_VALUE_0 
	
	
	#IF IS_DEBUG_BUILD
		

		NET_NL()NET_PRINT("MULTIPLIER VALUES g_sMPTunables.fArmourMaxMultiplier  =")
		NET_PRINT_FLOAT(g_sMPTunables.fArmourMaxMultiplier)
		
	#ENDIF
	
	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL()NET_PRINT("GIVE_PLAYER_ALL_UNLOCKED_HEALTH_FREEMODE: ")
		NET_NL()NET_PRINT(" 		- bIsAtSpawn = ")NET_PRINT_BOOL(bIsAtSpawn)
	#ENDIF
	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_1)
		NET_NL()NET_PRINT(" 		- IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_1) ")
		CurrentLevelHealth = RANK_HEALTH_VALUE_1 
	ENDIF
	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_2)
		NET_NL()NET_PRINT(" 		- IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_2) ")
		CurrentLevelHealth = RANK_HEALTH_VALUE_2 
	ENDIF
	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_3)
	
		NET_NL()NET_PRINT(" 		- IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_3) ")
		CurrentLevelHealth = RANK_HEALTH_VALUE_3 
	ENDIF
	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_4)
		NET_NL()NET_PRINT(" 		- IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_4) ")
		CurrentLevelHealth = RANK_HEALTH_VALUE_4 
	ENDIF
	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_5)
		NET_NL()NET_PRINT(" 		- IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_5) ")
		CurrentLevelHealth = RANK_HEALTH_VALUE_5
	ENDIF
	NET_NL()NET_PRINT(" 		- DONE ")
	
	#IF IS_DEBUG_BUILD
	CurrentLevelHealth += g_StartingHealthChange
	#ENDIF


	INT TotalMaximumArmour =  ROUND(50 *armour_mulitplier)
	INT TotalMaximumHealth = 328
	
	NET_NL()NET_PRINT("TotalMaximumArmour =  " )
	NET_PRINT_INT(TotalMaximumArmour)
	NET_NL()NET_PRINT(" g_sMPTunables.fArmourMaxMultiplier =  ")
	NET_PRINT_FLOAT(armour_mulitplier )
	/*
	IF bIsAtSpawn
		#IF IS_DEBUG_BUILD
			
			NET_NL()NET_PRINT(" GIVE_PLAYER_ALL_UNLOCKED_HEALTH_FREEMODE bIsAtSpawn TRUE ")
			NET_NL()NET_PRINT(" APPLY SPAWN MULTIPLIER  g_sMPTunables.fHealthRespawnMultiplier =")
			NET_PRINT_FLOAT(g_sMPTunables.fHealthRespawnMultiplier)
			NET_NL()NET_PRINT(" TotalMaximumHealth Before multiplier =")
			NET_PRINT_INT(TotalMaximumHealth)
			
			TotalMaximumHealth *=ROUND(g_sMPTunables.fHealthRespawnMultiplier)
			NET_NL()NET_PRINT(" TotalMaximumHealth After multiplier =")
			NET_PRINT_INT(TotalMaximumHealth)
			
			
		#ENDIF
		
	ENDIF
	*/
		
		
	SET_NEW_ARMOUR_HUD_LEVEL(CurrentLevelArmour,TotalMaximumArmour)

	SET_NEW_HEALTH_HUD_LEVEL(CurrentLevelHealth,TotalMaximumHealth, bIsAtSpawn)
	
	
	
	/*
	FLOAT fMaxHealthMultiplier 			= 1.0			// A modiefier used to tune the player's max health value.
	FLOAT fMinHealthMultiplier			= 1.0			// A modiefier used to tune the player's min health value.
	FLOAT fHealthRegenRateMultiplier		= 1.0			// A modiefier used to tune the player's health regen rate.
	FLOAT fHealthRegenMaxMultiplier		= 1.0			// A modiefier used to tune the player's max health value that regen health can reach.
	FLOAT fHealthRespawnMultiplier		= 1.0			// A modiefier used to tune the player's health value when they respawn.
	FLOAT fArmourMaxMultiplier			= 1.0			// A modiefier used to tune the player's max max armor value.
	FLOAT fPAndQsHealthReplenishMultiplier	= 1.0			// A modiefier used to tune the player's health replenish value of the snack Ps&Qs.
	FLOAT fEgoChaserHealthReplenishMultiplier	= 1.0			// A modiefier used to tune the player's health replenish value of the snack Ego Chaser.
	FLOAT fMeteoriteHealthReplenishMultiplier	= 1.0			// A modiefier used to tune the player's health replenish value of the snack Meteorite.
	FLOAT fRedwoodHealthDegenMultiplier		= 1.0			// A modiefier used to tune the player's health deplete value of the Redwood Smokes.
	FLOAT fOrangOTangHealthReplenishMultiplier	= 1.0			// A modiefier used to tune the player's health replenish value of the drink Orang-O-Tang.
	FLOAT fBougeoixHealthReplenishMultiplier	= 1.0			// A modiefier used to tune the player's health replenish value of the snack Bougeoix. 
*/

ENDPROC



PROC GIVE_PLAYER_PROP_UNLOCKED_HEALTH_FREEMODE(FLOAT fPropMax = 1.0)
	

	//CurrentLevelArmour = GET_PLAYER_CURRENT_MAX_ARMOUR()
	//  bug 1402344
	FLOAT health_mulitplier =  (g_sMPTunables.fMaxHealthMultiplier*fPropMax)
	
	PRINTLN("GIVE_PLAYER_PROP_UNLOCKED_HEALTH_FREEMODE(): g_sMPTunables.fMaxHealthMultiplier: ", g_sMPTunables.fMaxHealthMultiplier, ".")
	PRINTLN("GIVE_PLAYER_PROP_UNLOCKED_HEALTH_FREEMODE(): fPropMax: ", fPropMax, ".")
	PRINTLN("GIVE_PLAYER_PROP_UNLOCKED_HEALTH_FREEMODE(): health_mulitplier: ", health_mulitplier, ".")
	
	PRINTLN()
	//INT CurrentLevelArmour = 50 //* ROUND(armour_mulitplier)
	INT CurrentLevelHealth = ROUND((RANK_HEALTH_VALUE_0-100) * (health_mulitplier))
	CurrentLevelHealth = CurrentLevelHealth + 100

	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_1)
		CurrentLevelHealth = ROUND((RANK_HEALTH_VALUE_1-100) * (health_mulitplier))
		CurrentLevelHealth = CurrentLevelHealth + 100
		NET_NL()NET_PRINT(" 	GIVE_PLAYER_PROP_UNLOCKED_HEALTH_FREEMODE	- IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_1) ")NET_PRINT_INT(CurrentLevelHealth)
	ENDIF
	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_2)
		CurrentLevelHealth = ROUND((RANK_HEALTH_VALUE_2-100) * (health_mulitplier))
		CurrentLevelHealth = CurrentLevelHealth + 100
		NET_NL()NET_PRINT(" 	GIVE_PLAYER_PROP_UNLOCKED_HEALTH_FREEMODE	- IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_2) ")NET_PRINT_INT(CurrentLevelHealth)
	ENDIF
	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_3)
		CurrentLevelHealth = ROUND((RANK_HEALTH_VALUE_3-100) * (health_mulitplier))
		CurrentLevelHealth = CurrentLevelHealth + 100
		NET_NL()NET_PRINT(" 	GIVE_PLAYER_PROP_UNLOCKED_HEALTH_FREEMODE	- IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_3) ")NET_PRINT_INT(CurrentLevelHealth)
	ENDIF
	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_4)
		CurrentLevelHealth = ROUND((RANK_HEALTH_VALUE_4-100) * (health_mulitplier))
		CurrentLevelHealth = CurrentLevelHealth + 100
		NET_NL()NET_PRINT(" 	GIVE_PLAYER_PROP_UNLOCKED_HEALTH_FREEMODE	- IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_4) ")NET_PRINT_INT(CurrentLevelHealth)
	ENDIF
	IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_5)
		CurrentLevelHealth = ROUND((RANK_HEALTH_VALUE_5-100) * (health_mulitplier))
		CurrentLevelHealth = CurrentLevelHealth + 100
		NET_NL()NET_PRINT(" 	GIVE_PLAYER_PROP_UNLOCKED_HEALTH_FREEMODE	- IF IS_MP_HEALTH_UNLOCKED(PLAYERHEALTH_HEALTHUPGRADE_5) ")NET_PRINT_INT(CurrentLevelHealth)
	ENDIF
	IF CurrentLevelHealth < 101
		CurrentLevelHealth = 101
	ENDIF
	PRINTLN("GIVE_PLAYER_PROP_UNLOCKED_HEALTH_FREEMODE(): Setting player health to ", CurrentLevelHealth, ".")
	SET_ENTITY_HEALTH(PLAYER_PED_ID(), CurrentLevelHealth)
	
	
ENDPROC

FUNC BOOL IS_RANKUP_BIG_PRINT_RUNNING()
	RETURN MPGlobalsHud.bIsaRankupRunning
ENDFUNC

PROC SET_IS_RANKUP_BIG_PRINT_RUNNING(BOOL isRunning)
	MPGlobalsHud.bIsaRankupRunning = isRunning
ENDPROC



