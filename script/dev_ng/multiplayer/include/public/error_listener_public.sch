/*
	Author: 		Orlando Cazalet-Hyams
	Team: 			Online Technical
	Description:	Some helper functions for the event cache stuff.
					
*/

USING "net_events.sch"
USING "event_enums.sch"
USING "globals.sch"
#IF IS_DEBUG_BUILD
USING "mp_event_enums.sch"
#ENDIF

/// PURPOSE:
///    A safe way to access the g_eventCahce.
/// RETURNS:
///    TRUE if the iCacheIndex was valid.
FUNC BOOL GET_EVENT_FROM_CACHE(INT iCacheIndex, EVENT_CACHE_ITEM& item)
	IF 	iCacheIndex < g_eventCache.iLength 
	AND iCacheIndex < EVENT_CACHE_SIZE
		item = g_eventCache.events[iCacheIndex]
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Convert event name to a friendly string.
FUNC STRING GET_EVENT_NAME(EVENT_NAMES eEvent)
	SWITCH eEvent
		CASE EVENT_ERRORS_ARRAY_OVERFLOW 	RETURN "Array overrun"
		CASE EVENT_ERRORS_INSTRUCTION_LIMIT RETURN "Instr limit"
		CASE EVENT_ERRORS_STACK_OVERFLOW	RETURN "Stack overflow"
		CASE EVENT_ERRORS_UNKNOWN_ERRORS 	RETURN "Unknown error"
	ENDSWITCH
	RETURN "unknown_event"
ENDFUNC

/// PURPOSE:
///    Convert event name to a friendly string without any capital letters.
FUNC STRING GET_EVENT_NAME_NOCAPS(EVENT_NAMES eEvent)
	SWITCH eEvent
		CASE EVENT_ERRORS_ARRAY_OVERFLOW 	RETURN "array overrun"
		CASE EVENT_ERRORS_INSTRUCTION_LIMIT RETURN "instr limit"
		CASE EVENT_ERRORS_STACK_OVERFLOW	RETURN "stack overflow"
		CASE EVENT_ERRORS_UNKNOWN_ERRORS 	RETURN "unknown error"
	ENDSWITCH
	RETURN "unknown_event"
ENDFUNC

FUNC BOOL IS_EVENT_A_FATAL_ERROR(EVENT_NAMES eEvent)
	RETURN eEvent = EVENT_ERRORS_ARRAY_OVERFLOW
		OR eEvent = EVENT_ERRORS_INSTRUCTION_LIMIT
		OR eEvent = EVENT_ERRORS_STACK_OVERFLOW
		OR eEvent = EVENT_ERRORS_UNKNOWN_ERRORS
ENDFUNC

/// PURPOSE:
///    Returns true if the event is one that we care about caching.
///    **Separate from IS_EVENT_A_FATAL_ERROR incase we need to catch other errors later.
FUNC BOOL SHOULD_CACHE_THIS_EVENT(EVENT_NAMES eEvent)
	RETURN IS_EVENT_A_FATAL_ERROR(eEvent)
ENDFUNC 

FUNC BOOL IS_GAME_RESET_REQUIRED_ON_FATAL_ERROR_FOR_SCRIPT(INT iScriptHash)
	SWITCH iScriptHash
		CASE HASH("freemode")
		#IF FEATURE_FREEMODE_CLASSIC
		CASE HASH("freemodeLite")
		#ENDIF
		#IF FEATURE_FREEMODE_ARCADE
		CASE HASH("freemodeArcade")
		CASE HASH("copscrooks")
		CASE HASH("CopsCrooksWrapper")
		#ENDIF
		CASE HASH("FMMC_Launcher")
		CASE HASH("FM_Maintain_Transition_Players")
		CASE HASH("FMMC_PlayList_Controller")
		CASE HASH("fm_Bj_race_controler")
		CASE HASH("FM_Capture_Creator")
		CASE HASH("FM_Deathmatch_Controler")
		CASE HASH("FM_Deathmatch_Creator")
		CASE HASH("FM_Hideout_Controler")
		CASE HASH("FM_Survival_Controller")
		CASE HASH("FM_Horde_Controler")
		CASE HASH("FM_LTS_Creator")
		CASE HASH("FM_Mission_Creator")
		CASE HASH("FM_Race_Controler")
		CASE HASH("FM_Race_Creator")
		CASE HASH("FM_Survival_Creator")
		CASE HASH("Basic_Creator")
		CASE HASH("Freemode_Creator")
		CASE HASH("error_thrower")
			RETURN TRUE
	ENDSWITCH
	
	IF iScriptHash = GET_FM_MISSION_CONTROLLER_SCRIPT_NAME_HASH()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    A safe way to add an event to the g_eventCache.
///    FALSE if the cache was full and the event couldn't be added.
FUNC BOOL PUSH_EVENT_INTO_CACHE(EVENT_CACHE_ITEM& item)
	IF g_eventCache.iLength < EVENT_CACHE_SIZE
		g_eventCache.events[g_eventCache.iLength] = item
		g_eventCache.iLength++ 
		RETURN TRUE
	ENDIF
	
	#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("[error_listener] PUSH_EVENT_INTO_CACHE FAILED : Cache full")
	#ENDIF
	#IF IS_DEBUG_BUILD
		PRINTLN("[error_listener] PUSH_EVENT_INTO_CACHE FAILED : Cache full")
	#ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Clear the g_eventCache
PROC CLEAR_EVENT_CACHE()
	g_eventCache.iLength = 0
ENDPROC

/// PURPOSE:
///    Returns TRUE if the event for a particular script already exists in the g_eventCahce.
FUNC BOOL IS_EVENT_ITEM_DUPLICATE(EVENT_CACHE_ITEM& item, INT& iCacheIndex)
	iCacheIndex = 0
	
	REPEAT g_eventCache.iLength iCacheIndex
		IF g_eventCache.events[iCacheIndex].iEventName = item.iEventName
		AND g_eventCache.events[iCacheIndex].iScriptNameHash = item.iScriptNameHash
			RETURN TRUE
		ENDIF
	ENDREPEAT	
	
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
ENUM ERROR_THROWER_ERROR
	ETE_NONE,
	ETE_ARRAY_OVERRUN,
	ETE_INS_LIMIT,
	ETE_STACK_OVERFLOW
ENDENUM 
STRUCT ERROR_THROWER_ARGS
	ERROR_THROWER_ERROR eError
ENDSTRUCT

STRUCT SCRIPT_EVENT_FATAL_ERROR_IN_IMPORTANT_NET_SCRIPT
	STRUCT_EVENT_COMMON_DETAILS common
	SCRIPT_EVENT_FATAL_ERROR_DATA data
ENDSTRUCT

PROC PROCESS_SCRIPT_EVENT_FATAL_ERROR_IN_IMPORTANT_NET_SCRIPT(INT iEventQueueIndex)
	SCRIPT_EVENT_FATAL_ERROR_IN_IMPORTANT_NET_SCRIPT event
	IF NOT g_bReceivedNetFatalError
	AND GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventQueueIndex, event, SIZE_OF(event))
		PRINTLN("[ERROR_LISTENER][FATAL_ERROR_EVENT] Recieved net fatal error ", GET_EVENT_NAME_NOCAPS(event.data.eEvent) ," in " , event.data.txtScriptName, " from player ", GET_PLAYER_NAME(event.common.FromPlayerIndex), " (", NATIVE_TO_INT(event.common.FromPlayerIndex), ")")
		g_netFatalError.iPlayerIndex 	= event.common.FromPlayerIndex
		g_netFatalError.data			= event.data
		g_netFatalError.iFrameReceived 	= GET_FRAME_COUNT()
		g_bReceivedNetFatalError 		= TRUE
		g_netFatalError.data.txt23FatalPlayer = GET_PLAYER_NAME(g_netFatalError.iPlayerIndex)
	ENDIF
ENDPROC

/// PURPOSE:
///    Broadcast the script name
/// RETURNS:
///    BOOL - TRUE if successful.
FUNC BOOL BROADCAST_SCRIPT_EVENT_FATAL_ERROR_IN_IMPORTANT_NET_SCRIPT()
	INT iPlayerFlags =  ALL_PLAYERS(FALSE, FALSE)
	IF iPlayerFlags <> 0
		// Store the Broadcast data
		SCRIPT_EVENT_FATAL_ERROR_IN_IMPORTANT_NET_SCRIPT event
		event.common.Type 				= SCRIPT_EVENT_DEBUG_FATAL_ERROR_IN_IMPORTANT_NET_SCRIPT
		event.common.FromPlayerIndex	= g_netFatalError.iPlayerIndex
		event.data 						= g_netFatalError.data
		SEND_TU_SCRIPT_EVENT(SCRIPT_EVENT_QUEUE_NETWORK, event, SIZE_OF(event), iPlayerFlags)
		PRINTLN("[ERROR_LISTENER][FATAL_ERROR_EVENT] - BROADCAST_SCRIPT_EVENT_FATAL_ERROR_IN_IMPORTANT_NET_SCRIPT : Broadcasting event")
	ELSE
		PRINTLN("[ERROR_LISTENER][FATAL_ERROR_EVENT] - BROADCAST_SCRIPT_EVENT_FATAL_ERROR_IN_IMPORTANT_NET_SCRIPT : No other players in session.")
	ENDIF
	RETURN TRUE
ENDFUNC
#ENDIF //IS_DEBUG_BUILD
