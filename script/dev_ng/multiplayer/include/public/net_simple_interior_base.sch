USING "commands_interiors.sch"
USING "commands_network.sch"
USING "commands_camera.sch"
USING "MP_globals_interior_instances.sch"
USING "MP_globals_simple_interior_consts.sch"
USING "mp_globals_simple_interior.sch"
USING "mp_globals_common_consts.sch"
USING "mp_globals_nightclub_consts.sch"
USING "mp_globals_autoshop_property_consts.sch"
USING "mp_globals_arcade_propety_consts.sch"
USING "mp_globals_script_timers.sch"

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡      TYPES      ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

ENUM SIMPLE_INTERIORS
	SIMPLE_INTERIOR_INVALID = -1,					

	SIMPLE_INTERIOR_WAREHOUSE_1,										//	0					
	SIMPLE_INTERIOR_WAREHOUSE_2,										//	1					
	SIMPLE_INTERIOR_WAREHOUSE_3,										//	2						
	SIMPLE_INTERIOR_WAREHOUSE_4,										//	3
	SIMPLE_INTERIOR_WAREHOUSE_5,										//	4	
	SIMPLE_INTERIOR_WAREHOUSE_6,										//	5	
	SIMPLE_INTERIOR_WAREHOUSE_7,										//	6	
	SIMPLE_INTERIOR_WAREHOUSE_8,										//	7	
	SIMPLE_INTERIOR_WAREHOUSE_9,										//	8	
	SIMPLE_INTERIOR_WAREHOUSE_10,										//	9
	SIMPLE_INTERIOR_WAREHOUSE_11,										//	10
	SIMPLE_INTERIOR_WAREHOUSE_12,										//	11
	SIMPLE_INTERIOR_WAREHOUSE_13,										//	12
	SIMPLE_INTERIOR_WAREHOUSE_14,										//	13
	SIMPLE_INTERIOR_WAREHOUSE_15,										//	14
	SIMPLE_INTERIOR_WAREHOUSE_16,										//	15
	SIMPLE_INTERIOR_WAREHOUSE_17,										//	16
	SIMPLE_INTERIOR_WAREHOUSE_18,										//	17
	SIMPLE_INTERIOR_WAREHOUSE_19,										//	18
	SIMPLE_INTERIOR_WAREHOUSE_20,										//	19
	SIMPLE_INTERIOR_WAREHOUSE_21,										//	20
	SIMPLE_INTERIOR_WAREHOUSE_22,										//	21

	SIMPLE_INTERIOR_FACTORY_METH_1,										//	22			
	SIMPLE_INTERIOR_FACTORY_WEED_1,										//	23			
	SIMPLE_INTERIOR_FACTORY_CRACK_1,									//	24	
	SIMPLE_INTERIOR_FACTORY_MONEY_1,									//	25		
	SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1,								//	26	

	SIMPLE_INTERIOR_FACTORY_METH_2,										//	27			
	SIMPLE_INTERIOR_FACTORY_WEED_2,										//	28			
	SIMPLE_INTERIOR_FACTORY_CRACK_2,									//	29
	SIMPLE_INTERIOR_FACTORY_MONEY_2,									//	30		
	SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2,								//	31	

	SIMPLE_INTERIOR_FACTORY_METH_3,										//	32		
	SIMPLE_INTERIOR_FACTORY_WEED_3,										//	33			
	SIMPLE_INTERIOR_FACTORY_CRACK_3,									//	34		
	SIMPLE_INTERIOR_FACTORY_MONEY_3,									//	35		
	SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3,								//	36	
	
	SIMPLE_INTERIOR_FACTORY_METH_4,										//	37			
	SIMPLE_INTERIOR_FACTORY_WEED_4,										//	38			
	SIMPLE_INTERIOR_FACTORY_CRACK_4,									//	39		
	SIMPLE_INTERIOR_FACTORY_MONEY_4,									//	40
	SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4,								//	41
	
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_POLICE_STATION,					//	42
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MC_CLUBHOUSE,					//	43
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_ROCKFORD,					//	44
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_PILLBOX,					//	45
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_ALTA,						//	46
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_BURTON,					//	47
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_PALETO,					//	48
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_GRAND_SENORA,				//	49
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_CHUMASH,					//	50
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_ROCKCLUB,						//	51
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_CHICKEN_FACTORY,				//	52
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_CHICKEN_FACTORY_PART_2,			//	53
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_CHICKEN_FACTORY_PART_3,			//	54
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_CHICKEN_FACTORY_PART_4,			//	55
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_FARMHOUSE,						//	56
	
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_HEIST_YACHT,					//	57
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_RECYCLING_PLANT,				//	58
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BIOLAB,							//	59

	SIMPLE_INTERIOR_IE_WAREHOUSE_1,										//	60	// La Puerta
	SIMPLE_INTERIOR_IE_WAREHOUSE_2,										//	61	// La Mesa
	SIMPLE_INTERIOR_IE_WAREHOUSE_3,										//	62	// Davis
	SIMPLE_INTERIOR_IE_WAREHOUSE_4,										//	63	// Strawberry
	SIMPLE_INTERIOR_IE_WAREHOUSE_5,										//	64	// Murrieta Heights
	SIMPLE_INTERIOR_IE_WAREHOUSE_6,										//	65	// Cypress Flats
	SIMPLE_INTERIOR_IE_WAREHOUSE_7,										//	66	// El Burro Heights
	SIMPLE_INTERIOR_IE_WAREHOUSE_8,										//	67	// Elysian Island
	SIMPLE_INTERIOR_IE_WAREHOUSE_9,										//	68	// LSIA
	SIMPLE_INTERIOR_IE_WAREHOUSE_10,									//	69	// LSIA
	
	SIMPLE_INTERIOR_BUNKER_1,											//	70
	SIMPLE_INTERIOR_BUNKER_2,											//	71
	SIMPLE_INTERIOR_BUNKER_3,											//	72
	SIMPLE_INTERIOR_BUNKER_4,											//	73
	SIMPLE_INTERIOR_BUNKER_5,											//	74
	SIMPLE_INTERIOR_BUNKER_6,											//	75
	SIMPLE_INTERIOR_BUNKER_7,											//	76
	SIMPLE_INTERIOR_BUNKER_9,											//	77
	SIMPLE_INTERIOR_BUNKER_10,											//	78
	SIMPLE_INTERIOR_BUNKER_11,											//	79
	SIMPLE_INTERIOR_BUNKER_12,											//	80
	
	SIMPLE_INTERIOR_ARMORY_TRUCK_1,										//	81	// MOC
	SIMPLE_INTERIOR_CREATOR_TRAILER_1,									//	82	// MOC - Creator Mission
	
	SIMPLE_INTERIOR_HANGAR_1,											//	83	// LSIA Hangar 1
	SIMPLE_INTERIOR_HANGAR_2,											//	84	// LSIA Hangar A17
	SIMPLE_INTERIOR_HANGAR_3,											//	85	// Fort Zancudo Hangar A2
	SIMPLE_INTERIOR_HANGAR_4,											//	86	// Fort Zancudo Hangar 3497
	SIMPLE_INTERIOR_HANGAR_5,											//	87	// Fort Zancudo Hangar 3499
	
	SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1,									//	88	// AOC
	
	SIMPLE_INTERIOR_DEFUNCT_BASE_1,										//	89
	SIMPLE_INTERIOR_DEFUNCT_BASE_2,										//	90
	SIMPLE_INTERIOR_DEFUNCT_BASE_3,										//	91
	SIMPLE_INTERIOR_DEFUNCT_BASE_4,										//	92
	SIMPLE_INTERIOR_DEFUNCT_BASE_6,										//	93
	SIMPLE_INTERIOR_DEFUNCT_BASE_7,										//	94
	SIMPLE_INTERIOR_DEFUNCT_BASE_8,										//	95
	SIMPLE_INTERIOR_DEFUNCT_BASE_9,										//	96
	SIMPLE_INTERIOR_DEFUNCT_BASE_10,									//	97
	
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MEDIUM_GARAGE,					//	98
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_LOWEND_STUDIO,					//	99
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MIDEND_APARTMENT,				//	100
	
	SIMPLE_INTERIOR_CREATOR_AIRCRAFT_1,									//	101
	
	SIMPLE_INTERIOR_HUB_LA_MESA,										// 	102
	SIMPLE_INTERIOR_HUB_MISSION_ROW,									//	103
	SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE,							//	104
	SIMPLE_INTERIOR_HUB_WEST_VINEWOOD,									//	105
	SIMPLE_INTERIOR_HUB_CYPRESS_FLATS,									//	106
	SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE,									//	107
	SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND,									//	108
	SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD,								//	109
	SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING,								//	110
	SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS,								//	111
	
	
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_SHERIFF,						// 	112
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_SHERIFF2,						// 	113
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_UNION_DEPOSITORY_CARPARK,		//  114
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_SIMEON_SHOWROOM,				// 	115
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_ABATTOIR,						// 	116
	
	SIMPLE_INTERIOR_HACKER_TRUCK,										// 	117
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_JEWEL_STORE,					// 	118

	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_LIFE_INVADER,					// 	119
	
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_DJ_YACHT,						//	120
	
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MELANOMA_GARAGE,				//	121
	
	SIMPLE_INTERIOR_ARENA_GARAGE_1,										// 122
		
	SIMPLE_INTERIOR_CASINO,												// 123
	SIMPLE_INTERIOR_CASINO_APT,											// 124
	SIMPLE_INTERIOR_CASINO_VAL_GARAGE,									// 125
	
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_HAYES_AUTOS,					// 126
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_METH_LAB,						// 127	
	
	SIMPLE_INTERIOR_ARCADE_PALETO_BAY,									// 128
	SIMPLE_INTERIOR_ARCADE_GRAPESEED,									// 129
	SIMPLE_INTERIOR_ARCADE_DAVIS,										// 130
	SIMPLE_INTERIOR_ARCADE_WEST_VINEWOOD,								// 131
	SIMPLE_INTERIOR_ARCADE_ROCKFORD_HILLS,								// 132
	SIMPLE_INTERIOR_ARCADE_LA_MESA,										// 133
	
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_FIB_BUILDING,					// 134
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BIOLAB_AND_TUNNEL,				// 135
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_FOUNDRY,						// 136
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MAX_RENDA,						// 137
	
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER,				// 138
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_2,		// 139
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_3,		// 140
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_4,		// 141
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_5,		// 142
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_6,		// 143
	
	SIMPLE_INTERIOR_RESTRICTED_INTERIOR_OMEGA,							// 144	
	SIMPLE_INTERIOR_SOLOMONS_OFFICE,									// 145	
	SIMPLE_INTERIOR_CASINO_NIGHTCLUB,									// 146
	SIMPLE_INTERIOR_SUBMARINE,											// 147

	#IF FEATURE_MUSIC_STUDIO
	SIMPLE_INTERIOR_MUSIC_STUDIO,										// 148
	#ENDIF
	
	SIMPLE_INTERIOR_AUTO_SHOP_LA_MESA,									// 149
	SIMPLE_INTERIOR_AUTO_SHOP_STRAWBERRY,								// 150
	SIMPLE_INTERIOR_AUTO_SHOP_BURTON,									// 151
	SIMPLE_INTERIOR_AUTO_SHOP_RANCHO,									// 152
	SIMPLE_INTERIOR_AUTO_SHOP_MISSION_ROW,								// 153
	SIMPLE_INTERIOR_CAR_MEET,											// 154
	
	#IF FEATURE_FIXER
	SIMPLE_INTERIOR_FIXER_HQ_HAWICK,									// 155
	SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD,									// 156
	SIMPLE_INTERIOR_FIXER_HQ_SEOUL,										// 157
	SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI,									// 158
	#ENDIF
	
	#IF FEATURE_DLC_2_2022
	SIMPLE_INTERIOR_ACID_LAB,											// 159
	SIMPLE_INTERIOR_JUGGALO_HIDEOUT,									// 160
	SIMPLE_INTERIOR_MULTISTOREY_GARAGE,									// 161
	#ENDIF
	
	//When adding a new RESTRICTED_INTERIOR be sure to add an apropriate coord to GET_RESTRICTED_INTERIOR_ENTRY_LOCATE to avoid: url:bugstar:6128337
	SIMPLE_INTERIOR_END
ENDENUM

ENUM SIMPLE_INTERIOR_EXT_SCRIPT_STATE
	ENTRANCE_STATE_IDLE,
	ENTRANCE_STATE_IN_LOCATE_AWAITING_CONTEXTUAL_INPUT,
	ENTRANCE_STATE_START_ENTERING,
	ENTRANCE_STATE_WAITING_FOR_ANIM_TO_START,
	ENTRANCE_STATE_ENTERING,
	ENTRANCE_STATE_GOT_INVITED,
	ENTRANCE_STATE_USING_MENU,
	ENTRANCE_STATE_USING_BUZZER_MENU,
	ENTRANCE_STATE_LEAVE_ENTRANCE_LOCATE,
	ENTRANCE_STATE_INT_SCRIPT_AWAITING_NETWORK_CLEANUP,
	ENTRANCE_STATE_INT_SCRIPT_LOADING,
	ENTRANCE_STATE_INT_SCRIPT_STARTING,
	ENTRANCE_STATE_INT_SCRIPT_LOAD_SPECIAL_CUT_SCENE_ASSETS,
	ENTRANCE_STATE_INT_SCRIPT_STARTING_SPECIAL_CUT_SCENE,
	//Custom Cutscene Exterior Enter
	ENTRANCE_STATE_CUTSCENE_ENTER_LOAD_ASSETS,
	ENTRANCE_STATE_CUTSCENE_ENTER_CREATE_ASSETS,
	ENTRANCE_STATE_CUTSCENE_ENTER_STREAM_ASSETS,
	ENTRANCE_STATE_CUTSCENE_ENTER_WAIT_FOR_ALL_PLAYERS,
	ENTRANCE_STATE_CUTSCENE_ENTER_MAINTAIN,	
	ENTRANCE_STATE_INT_SCRIPT_READY,
	ENTRANCE_STATE_PLAYER_IS_DEAD,
	ENTRANCE_STATE_HOLD_UP_PROCESSING,
	ENTRANCE_STATE_ENTERING_SEPERATE_INTERIOR,
	
	ENTRANCE_STATE_COUNT
ENDENUM

ENUM ASYNC_GRID_SPAWN_LOCATIONS
	GRID_SPAWN_LOCATION_INVALID = -1,
	
	#IF IS_DEBUG_BUILD
	_DBG_GRID_SPAWN_LOCATION_CREATOR,
	#ENDIF
	
	GRID_SPAWN_LOCATION_FACTORY_METH_1,
	GRID_SPAWN_LOCATION_FACTORY_CRACK_1,
	GRID_SPAWN_LOCATION_FACTORY_WEED_1,
	GRID_SPAWN_LOCATION_FACTORY_FAKEID_1,
	GRID_SPAWN_LOCATION_FACTORY_CASH_1,
	
	GRID_SPAWN_LOCATION_FACTORY_METH_2,
	GRID_SPAWN_LOCATION_FACTORY_CRACK_2,
	GRID_SPAWN_LOCATION_FACTORY_WEED_2,
	GRID_SPAWN_LOCATION_FACTORY_FAKEID_2,
	GRID_SPAWN_LOCATION_FACTORY_CASH_2,
	
	GRID_SPAWN_LOCATION_FACTORY_METH_3,
	GRID_SPAWN_LOCATION_FACTORY_CRACK_3,
	GRID_SPAWN_LOCATION_FACTORY_WEED_3,
	GRID_SPAWN_LOCATION_FACTORY_FAKEID_3,
	GRID_SPAWN_LOCATION_FACTORY_CASH_3,
	
	GRID_SPAWN_LOCATION_FACTORY_METH_4,
	GRID_SPAWN_LOCATION_FACTORY_CRACK_4,
	GRID_SPAWN_LOCATION_FACTORY_WEED_4,
	GRID_SPAWN_LOCATION_FACTORY_FAKEID_4,
	GRID_SPAWN_LOCATION_FACTORY_CASH_4,
	
	GRID_SPAWN_LOCATION_FACTORY_CRACK_INSIDE,
	GRID_SPAWN_LOCATION_FACTORY_METH_INSIDE,
	GRID_SPAWN_LOCATION_FACTORY_WEED_INSIDE,
	GRID_SPAWN_LOCATION_FACTORY_FAKEID_INSIDE,
	GRID_SPAWN_LOCATION_FACTORY_CASH_INSIDE,
	
	GRID_SPAWN_LOCATION_IE_WAREHOUSE_INSIDE
ENDENUM

// Different systems (like menus) can emit events that will get processed by interior or exterior script.
ENUM SIMPLE_INTERIOR_LOCAL_EVENTS
	SI_EVENT_NONE,
	SI_EVENT_EXIT_MENU_EXIT,
	SI_EVENT_EXIT_MENU_EXIT_ALL,
	SI_EVENT_ENTRY_MENU_ENTER_ALONE,
	SI_EVENT_ENTRY_MENU_ENTER_WITH_NEARBY,
	SI_EVENT_ENTRY_MENU_WAIT_FOR_ASYNC_GROUP_WARP_INSTANCE
ENDENUM

ENUM SIMPLE_INTERIOR_MENU_SETTINGS
	SI_MENU_SETTING_PLAY_SOUND_WHEN_SHOWN,
	SI_MENU_SETTING_USE_CANCEL_ACTION
ENDENUM

ENUM CUTSCENE_HELP_VEHICLE_CLONE_FLAGS
	chVEHICLE_CLONE_FREEZE = BIT0, // Freeze vehicle after it is created.
	chVEHICLE_CLONE_POSTIION_ON_GROUND = BIT1, // Otherwise will be positioned at the same transform as the reference vehicle.
	chVEHICLE_CLONE_TRAILER = BIT2, //Will try to clone trailer if one is attached.
	chVEHICLE_CLONE_FIX_TIRES = BIT3,
	chVEHICLE_CLONE_ONLY_PLAYER_PASSENGERS = BIT4,
	chVEHICLE_CLONE_DO_NOT_CLONE_PASSENGERS = BIT5,
	chVEHICLE_CLONE_DEAD_PED_PASSENGERS = BIT6,
	chVEHICLE_CLONE_WAIT_FOR_HEAD_BLEND = BIT7,
	chVEHICLE_CLONE_DO_NOT_LINK_BLENDS = BIT8
ENDENUM

STRUCT SIMPLE_INTERIOR_MENU
	INT iDesiredState
	INT iCurrentState
	INT iBS
	INT iOptionsVisibleBS
	INT iOptionsSelectableBS
	INT iSelectedItem
	INT iItemsCount
	TEXT_LABEL_63 strTitle
	STRING strOptions[SIMPLE_INTERIOR_MENU_MAX_OPTIONS]
	SIMPLE_INTERIOR_LOCAL_EVENTS eOptionEvents[SIMPLE_INTERIOR_MENU_MAX_OPTIONS]
	TIME_DATATYPE timeScrollDelay
	INT iMoveUp
	INT iSettingsBS
	STRING sMenuHeaderName
ENDSTRUCT

STRUCT SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX
	VECTOR vInsideBBoxMin
	VECTOR vInsideBBoxMax
ENDSTRUCT

STRUCT SIMPLE_INTERIOR_ENTRANCE_DATA
	SIMPLE_INTERIORS eSimpleInteriorID
	INT iID
	INT iType
	INT iSettingsBS
	VECTOR vPosition
	VECTOR vLocatePos1 // Alternatively this is used as a size of the locate if we're not using angled area.
	VECTOR vLocatePos2
	FLOAT fLocateWidth, fEnterHeading
	
	INT iLocateColourR, iLocateColourG, iLocateColourB, iLocateColourA
ENDSTRUCT

CONST_INT ciSIMPLE_INTERIOR_EXIT_COUNT 9

STRUCT SIMPLE_INTERIOR_EXIT_DATA
	VECTOR 	vLocateCoords1[ciSIMPLE_INTERIOR_EXIT_COUNT]
	VECTOR 	vLocateCoords2[ciSIMPLE_INTERIOR_EXIT_COUNT]
	FLOAT 	fLocateWidths[ciSIMPLE_INTERIOR_EXIT_COUNT]
	VECTOR 	vDoorCoords[ciSIMPLE_INTERIOR_EXIT_COUNT]
	FLOAT 	fHeadings[ciSIMPLE_INTERIOR_EXIT_COUNT]
ENDSTRUCT

// Store the cloned vehicle and all its passangers.
STRUCT CUTSCENE_HELP_VEHICLE_CLONE
	VEHICLE_INDEX vehicle
	PED_INDEX vehPassengers[CI_MAX_NUMBER_OF_PASSANGERS]
	VEHICLE_INDEX trailer
ENDSTRUCT

STRUCT EXT_TO_EXT_WARP_DATA
	INT iStage
ENDSTRUCT

STRUCT BASIC_SIMPLE_INTERIOR_ENTRY_DATA
	INT iStage
	FLOAT fFadeInPhase
	INT iFadeInTime
ENDSTRUCT

CONST_INT MAX_ON_ENTER_SOUNDS 2

STRUCT SIMPLE_INTERIOR_ENTRY_ANIM_DATA
	INT iBS
	INT stage
	PED_INDEX pedActor
	STRING dictionary
	STRING dictionaryIdle
	STRING clip
	STRING clipIdle
	STRING clipVehicle
	STRING clipCamera
	STRING clipFace
	INT animEarlyOutTagHash = -1
	CAMERA_INDEX camera
	CAMERA_INDEX camera2
	VECTOR cameraPos
	VECTOR cameraRot
	FLOAT cameraFov
	VECTOR cameraPos2
	VECTOR cameraRot2
	FLOAT cameraFov2
	INT syncSceneID
	VECTOR syncScenePos
	VECTOR syncSceneRot
	FLOAT startingPhase
	FLOAT endingPhase
	FLOAT currentPhase
	INT iDuration = -1
	SCRIPT_TIMER stPlayTime
	FLOAT fCamShake = 0.25
	BOOL bPlayGarageDoorSoundOnExit = FALSE
	BOOL bFadeScreenDuringEntryAnim = FALSE
	
	MODEL_NAMES modelFakeDoor
	VECTOR vFakeDoorPosition
	VECTOR vFakeDoorRotation
	OBJECT_INDEX objFakeDoor
	
	MODEL_NAMES modelFakeFrame
	VECTOR vFakeFramePosition
	VECTOR vFakeFrameRotation
	OBJECT_INDEX objFakeFrame
	
	VECTOR establishingCameraPos
	VECTOR establishingCameraRot
	FLOAT fEstablishingCameraFov
	FLOAT fEstablishingCameraShake = 0.25
	
	// SOUNDS
	INT iOnEnterSoundsPlayedBS
	INT iOnEnterFadeOutStopSoundTime = -1
	STRING strOnEnterSoundNames[MAX_ON_ENTER_SOUNDS]
	FLOAT fOnEnterSoundPhases[MAX_ON_ENTER_SOUNDS]
	STRING strOnEnterPhaseSoundSet
	STRING sOnTransitionAudioScene
	
	STRING strOnEnterFadeOutStartSoundName // Played right before the screen starts fading out on entrance.
	STRING strOnEnterFadeOutStartSoundSet
	
	STRING strOnEnterFadeOutFinishSoundName // Played right after the screen is faded out on entrance.
	STRING strOnEnterFadeOutFinishSoundSet
	
	STRING strOnLeaveFadeInStartSoundName // Played right before fade in on leaving.
	STRING strOnLeaveFadeInStartSoundSet
	
	STRING strOnEnterFadeInFinishSoundName // Played when inside when fade in finishes.
	STRING strOnEnterFadeInFinishSoundSet
	
	STRING strOnLeaveFadeOutStartSoundName // Played right after the screen is faded out on leaving.
	STRING strOnLeaveFadeOutStartSoundSet
	
	STRING strOnEnterGarageFadeOutFinishSoundName // Played right after the screen is faded out on entrance.
	STRING strOnEnterGarageFadeOutFinishSoundSet
	
	INT iExteriorCamShapeTestStage[4]
	INT iContextID = -1
	FLOAT fPanDuration
	SHAPETEST_INDEX sti_ExteriorCam[4]	
	SCRIPT_TIMER sShapeTestTimer
	SCRIPT_TIMER cameraPanTimer
	VEHICLE_INDEX cloneTrailer
	VEHICLE_INDEX cloneTruck
	SHAPETEST_INDEX sti_MobileIntEntryShapeTest
	SHAPETEST_INDEX sti_MobileIntExitSpawnShapeTest
	
	// Custom Cutscene data
	CUTSCENE_HELP_VEHICLE_CLONE sCutsceneVeh
	CUTSCENE_HELP_VEHICLE_CLONE sSecondaryCutsceneVeh
	VEHICLE_INDEX vehicleArray[4]
	PED_INDEX pedArray[30]
	ENTITY_INDEX entityArray[4]
	OBJECT_INDEX objectArray[6]
	FLOAT fStateArray[4]
	INT iSceneBS
	VECTOR vVectorArray[9]
	INT iTruckCutSceneStage
	INT iSoundID = -1
	INT iNumProps = 0
	INT iSceneTimeAtCut
	STRING strPropClipArray[6]
	STRING audioCutsceneSet
	STRING audioCutsceneEnter
	STRING audioCutsceneExit
	
	// Cutscene VFX
	PTFX_ID PtFxId
	STRING strPtFxAsset
	STRING strPtFxEffect
	STRING strPtFxEffectVeh
	VECTOR vPtFxPosOffset
	VECTOR vPtFxRotOffset
	
	STRING audioSceneEntry
	STRING audioSceneInside
	STRING audioSceneExit
	SCRIPT_TIMER stStreamingFailSafe
	SCRIPT_TIMER timer
	// Vehicle and trailer player used to enter with.
	VEHICLE_INDEX vehicle
	VEHICLE_INDEX trailer
	VECTOR vTrailerCoord
	FLOAT fTrailerHeading
	TIME_DATATYPE tdSafeTruckAssetLoadTimer
	// End custom cutscene
	
	#IF IS_DEBUG_BUILD
	BOOL d_bDelayAnimStart
	SCRIPT_TIMER d_timerDelayedStart
	#ENDIF
	
	LOCK_STATE vehDoorLocks
	VEHICLE_INDEX vehEntered
	
	NETWORK_INDEX niExitSceneNetObject
	EXT_TO_EXT_WARP_DATA extToExtWarp
	BASIC_SIMPLE_INTERIOR_ENTRY_DATA basicEntry
	VEHICLE_INDEX vehOverride
	
ENDSTRUCT

//Struct used to group simple interior details bit sets
CONST_INT ciDETAILS_BS_NUM_INTS	2
STRUCT SIMPLE_INTERIOR_DETAILS_BS
	INT iBS[ciDETAILS_BS_NUM_INTS]
ENDSTRUCT

/// PURPOSE: 
///    This structure acts as a "class" for a given simple interior, it gets initialised by a specific init function
///    in GET_SIMPLE_INTERIOR_DETAILS which should come from header files describing given simple interior type.
STRUCT SIMPLE_INTERIOR_DETAILS
	VECTOR vInteriorPosition
	BOOL bInteriorIsSeamless
	FLOAT fInteriorHeading
	TEXT_LABEL_63 strInteriorType
	
	// Extra interior script spawned by SMPL_INTERIOR_INT
	// This is to decouple functionality that might be specific to a given interior from the generic functionality that guards
	// access to the interior, its loading and player spawning.
	STRING strInteriorChildScript
	
	SIMPLE_INTERIOR_ENTRY_ANIM_DATA entryAnim
	// Uses interior space as opposed to world space
	SIMPLE_INTERIOR_EXIT_DATA exitData 
	
	SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX insideBBox
	BOOL bInteriorUsesAngledArea = FALSE
	
	// Bitset used by particular instance of simple interior (so not a generic simple interior but any
	// of the implementations like warehouse) for anything that is required by it (e.g. it is
	// going to be used by warehouses to track if wanted level supression was set).
	INT iSimpleInteriorInstanceBS
	
	//used for on wheel check for hangar but possibly usable for other things in future.
	SCRIPT_TIMER st_GeneralTimer
	
	//Used to block access to another property that is inside the property we're in. MOC in bunker etc.
	BOOL bBlockAccessToSecondaryInterior
	
	//Used to load an IPL necessary for this interior - Used where the interior is above ground
	STRING sInteriorIPL	
	STRING sShadowIPL
	INT iBuzzerEntranceID = 0
	INT iValetEntranceID = -1
	STRING sMenuHeaderName
	
	//Used to declare what functionality a simple interior should have
	SIMPLE_INTERIOR_DETAILS_BS sProperties
	
	//Help text to be used with the contextual input stage of entry
	TEXT_LABEL_15 tlContextHelp
	
	STRING strSeatingScript
	
	//Store which player the active script loaded/hinted an audio bank for in case the owner of the interior cannot be determined on cleanup
	INT iAudioBankLoadedForPlayer = -1
	
	// Used to prevent players from immediately re-entering the interior.
	#IF FEATURE_HEIST_ISLAND
	INT iReEnterDelayTime = -1
	#ENDIF
ENDSTRUCT

STRUCT SIMPLE_INTERIOR_CUSTOM_MENU_IN_DATA
	SIMPLE_INTERIORS eSimpleInteriorID 
	PLAYER_INDEX piOwner
	BOOL bAccepted		= FALSE
	BOOL bCancelled		= FALSE
	INT iCurrentItem	
	BOOl bRebuild		= FALSE
ENDSTRUCT

STRUCT SIMPLE_INTERIOR_CUSTOM_MENU_OUT_DATA
	BOOL bForceRebuildNextFrame
	BOOL bToggleRenderPhaseOnMenuClose = FALSE
	BOOL bContinueWithDefaultProcessing
	BOOL bForceDefaultMenuRebuild
	BOOL bQuitMenu
	BOOL bRequestInteriorLaunchAfterDefaultProcessing
	BOOL bForceInteriorState //Forcing state to loading will fade out & launch interior script
	BOOL bSetupAccessBS = TRUE
	BOOL bUpdateInteriorDetails
	BOOL bHideMenuForForceRebuild // Hide the menu this while rebuilding?
	BOOL bSetExtCamActiveFlag = FALSE
	SIMPLE_INTERIOR_EXT_SCRIPT_STATE eForcedInteriorState
ENDSTRUCT

STRUCT SIMPLE_INTERIOR_CUSTOM_INTERIOR_WARP_PARAMS
	BOOL bIgnoreObstructions = TRUE
	BOOL bIgnoreDistanceChecks = TRUE
	
	FLOAT fPedClearanceRadius = 0.01
	FLOAT fVehClearRadius = 2.0
ENDSTRUCT

CONST_INT NO_FLOOR_INDEX_OVERRIDE	-1
CONST_INT NO_INSTANCE_OVERRIDE 		-1
CONST_INT BLOCK_INSTANCE_OVERRIDE 	-2

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡  DECLARATIONS   ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

//========================== GENERIC FUNCTION POINTERS ==========================
/// PURPOSE:
///    Generic simple interior function pointer - Returns a BOOL - Takes eSimpleInteriorID
TYPEDEF FUNC BOOL _T_SIMPLE_INTERIOR_BOOL_FP_ONLY_ID_PARAM(SIMPLE_INTERIORS eSimpleInteriorID)

/// PURPOSE:
///    Generic simple interior function pointer - Returns an INT - Takes eSimpleInteriorID
TYPEDEF FUNC INT _T_SIMPLE_INTERIOR_INT_FP_ONLY_ID_PARAM(SIMPLE_INTERIORS eSimpleInteriorID)

/// PURPOSE:
///    Generic simple interior function pointer - Returns an INT - Takes eSimpleInteriorID and INT
TYPEDEF FUNC INT _T_SIMPLE_INTERIOR_INT_FP_ID_AND_INT_PARAM(SIMPLE_INTERIORS eSimpleInteriorID, INT iParam = 0)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a STRING - Takes eSimpleInteriorID
TYPEDEF FUNC STRING _T_SIMPLE_INTERIOR_STRING_FP_ONLY_ID_PARAM(SIMPLE_INTERIORS eSimpleInteriorID)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a VECTOR - Takes eSimpleInteriorID
TYPEDEF FUNC VECTOR _T_SIMPLE_INTERIOR_VECTOR_FP_ONLY_ID_PARAM(SIMPLE_INTERIORS eSimpleInteriorID)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a VECTOR - Takes eSimpleInteriorID and INT
TYPEDEF FUNC VECTOR _T_SIMPLE_INTERIOR_VECTOR_FP_ID_AND_INT_PARAM(SIMPLE_INTERIORS eSimpleInteriorID, INT iParam = 0)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a BLIP_SPRITE - Takes eSimpleInteriorID and INT
TYPEDEF FUNC BLIP_SPRITE _T_SIMPLE_INTERIOR_BLIP_SPRITE_FP_ID_AND_INT_PARAM(SIMPLE_INTERIORS eSimpleInteriorID, INT iParam = 0)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a ASYNC_GRID_SPAWN_LOCATIONS - Takes eSimpleInteriorID
TYPEDEF FUNC ASYNC_GRID_SPAWN_LOCATIONS _T_SIMPLE_INTERIOR_ASYNC_FP_ONLY_ID_PARAM(SIMPLE_INTERIORS eSimpleInteriorID)

/// PURPOSE:
///    Generic simple interior function pointer - PROC - Takes details & eSimpleInteriorID
TYPEDEF PROC _T_SIMPLE_INTERIOR_PROC_FP_ID_AND_DETAILS_PARAM(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a BOOL - Takes eSimpleInteriorID and entryAnim
TYPEDEF FUNC BOOL _T_SIMPLE_INTERIOR_BOOL_FP_ID_AND_ENTRYANIM_PARAM(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)

/// PURPOSE:
///    Generic simple interior function pointer - PROC - Takes eSimpleInteriorID and entryAnim
TYPEDEF PROC _T_SIMPLE_INTERIOR_PROC_FP_ID_AND_ENTRYANIM_PARAM(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a BOOL - Takes eSimpleInteriorID and debug only bool
TYPEDEF FUNC BOOL _T_SIMPLE_INTERIOR_BOOL_FP_ID_AND_DEBUGBOOL_PARAM(SIMPLE_INTERIORS eSimpleInteriorID #IF IS_DEBUG_BUILD , BOOL bPrintReason = FALSE #ENDIF)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a BOOL - Takes playerID and eSimpleInteriorID
TYPEDEF FUNC BOOL _T_SIMPLE_INTERIOR_BOOL_FP_PID_AND_ID_PARAM(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a BOOL - Takes playerID and eSimpleInteriorID
TYPEDEF FUNC BOOL _T_SIMPLE_INTERIOR_BOOL_FP_ID_PID_AND_BOOL_PARAM(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX interiorOwner, BOOL bOwnerDrivingOut)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a BOOL - Takes playerID, eSimpleInteriorID and INT
TYPEDEF FUNC BOOL _T_SIMPLE_INTERIOR_BOOL_FP_PID_ID_AND_INT_PARAM(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID, INT iParam)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a BOOL - Takes playerID, eSimpleInteriorID and INT ad bool
TYPEDEF FUNC BOOL _T_SIMPLE_INTERIOR_BOOL_FP_PID_ID_AND_INTAND_BOOL_PARAM(PLAYER_INDEX playerID, SIMPLE_INTERIORS eSimpleInteriorID, INT iParam, BOOL bParam)

TYPEDEF FUNC STRING _T_SIMPLE_INTERIOR_STRING_FP_ID_TL63_AND_INT_PARAM(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, INT iEntranceUsed)

/// PURPOSE:
///    Generic simple interior function pointer - PROC - Takes eSimpleInteriorID and entryMenu
TYPEDEF PROC _T_SIMPLE_INTERIOR_PROC_FP_ID_AND_ENTRYMENU_PARAM(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_MENU &menu)

/// PURPOSE:
///    Generic simple interior function pointer - PROC - Takes params for custom menus
TYPEDEF PROC _T_SIMPLE_INTERIOR_PROC_FP_ID_AND_CUSTOMMENU_PARAMS(SIMPLE_INTERIORS eSimpleInterior, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], SIMPLE_INTERIOR_LOCAL_EVENTS &eventOptions[], INT &iOptionsCount)

/// PURPOSE:
///    Generic simple interior function pointer - PROC - Takes params for custom vehicle entrance menu
TYPEDEF PROC _T_SIMPLE_INTERIOR_PROC_FP_ID_ENTRYMENU_AND_VEHICLE_PARAMS(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_MENU &menu, VEHICLE_INDEX vehEntranceVehicle)

/// PURPOSE:
///    Generic simple interior function pointer - PROC - Takes eSimpleInteriorID and INT by reference
TYPEDEF PROC _T_SIMPLE_INTERIOR_PROC_FP_ID_AND_REF_INT_PARAM(SIMPLE_INTERIORS eSimpleInterior, INT &iCustomInt)

/// PURPOSE:
///    Generic simple interior function pointer - PROC - Takes eSimpleInteriorID and INT
TYPEDEF PROC _T_SIMPLE_INTERIOR_PROC_FP_ID_AND_INT_PARAM(SIMPLE_INTERIORS eSimpleInterior, INT iCustomInt)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a BOOL - Takes eSimpleInteriorID and eModel
TYPEDEF FUNC BOOL _T_SIMPLE_INTERIOR_BOOL_FP_ID_AND_MODEL_PARAM(SIMPLE_INTERIORS eSimpleInteriorID, MODEL_NAMES eModel)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a BOOL - Takes eSimpleInteriorID, Vector and a Float
TYPEDEF FUNC BOOL _T_SIMPLE_INTERIOR_BOOL_FP_ID_INT_VEC_AND_FLOAT_PARAM(SIMPLE_INTERIORS eSimpleInteriorID, INT iIndex, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading)

/// PURPOSE:
///    Generic simple interior function pointer - PROC - Takes eSimpleInteriorID, ref TL15 and a ref STRING array
TYPEDEF PROC _T_SIMPLE_INTERIOR_PROC_FP_ID_TL15_AND_STRING(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_15 &txtTitle, STRING &strOptions[])

/// PURPOSE:
///    Generic simple interior function pointer - PROC - Takes eSimpleInteriorID
TYPEDEF PROC _T_SIMPLE_INTERIOR_PROC_FP_ID(SIMPLE_INTERIORS eSimpleInteriorID)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a BOOL - Takes eSimpleInteriorID & INT
TYPEDEF FUNC BOOL _T_SIMPLE_INTERIOR_BOOL_FP_ID_INT_PARAM(SIMPLE_INTERIORS eSimpleInteriorID, INT iCustomInt = -1)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a BOOL - Takes eSimpleInteriorID & INT & Ref INT
TYPEDEF PROC _T_SIMPLE_INTERIOR_PROC_FP_ID_AND_INT_AND_REF_INT_PARAM(SIMPLE_INTERIORS eSimpleInteriorID, INT iCustomInt, INT &iCustomIntTwo)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a BOOL - Takes eSimpleInteriorID, ref TL63, BOOL & ref Bool
TYPEDEF FUNC BOOL _T_SIMPLE_INTERIOR_BOOL_FP_ID_TL63_BOOLX2_PARAM(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &tlReturn, BOOL bReturnMenuLabel, BOOL &bSelectable)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a BOOL - Takes eSimpleInteriorID, ref bool & ref TL15
TYPEDEF FUNC BOOL _T_SIMPLE_INTERIOR_BOOL_FP_ID_BOOL_TL15_PARAM(SIMPLE_INTERIORS eSimpleInteriorID, BOOL &bSelectable, TEXT_LABEL_15 &tlReturn)

/// PURPOSE:
///    Generic simple interior function pointer - Returns a STRING - Takes eSimpleInteriorID & simple interior text ID
TYPEDEF FUNC STRING _T_SIMPLE_INTERIOR_STRING_FP_ID_AND_GT_PARAM(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_GAME_TEXT eTextID)

/// PURPOSE:
///     Generic simple interior function pointer - PROC - Takes eSimpleInteriorID & player index
TYPEDEF PROC _T_SIMPLE_INTERIOR_PROC_FP_ID_AND_PLYR_PARAM(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX player)

//========================== NON_GENERIC FUNCTION POINTERS ==========================

TYPEDEF PROC _T_GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION					(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtType, VECTOR &vPosition, FLOAT &fHeading , BOOL bUseSecondaryInteriorDetails)
TYPEDEF PROC _T_GET_SIMPLE_INTERIOR_DETAILS										(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS &details, BOOL bUseSecondaryInteriorDetails, BOOL bSMPLIntPreCache)
TYPEDEF PROC _T_GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX							(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX &bbox, BOOL bUseSecondaryInteriorDetails)
TYPEDEF PROC _T_GET_SIMPLE_INTERIOR_ENTRANCE_DATA								(SIMPLE_INTERIORS eSimpleInteriorID, INT iEntranceID, SIMPLE_INTERIOR_ENTRANCE_DATA &data)
TYPEDEF PROC _T_SET_SIMPLE_INTERIOR_ACCESS_BS									(SIMPLE_INTERIORS eSimpleInteriorID, INT iInvitingPlayer)
TYPEDEF PROC _T_GET_SIMPLE_INTERIOR_EXIT_MENU_TITLE_AND_OPTIONS					(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtTitle, STRING &strOptions[], INT &iOptionsCount, INT iCurrentExit)
TYPEDEF PROC _T_MAINTAIN_SIMPLE_INTERIOR_BLIP_EXTRA_FUNCTIONALITY				(SIMPLE_INTERIORS eSimpleInteriorID, BLIP_INDEX &blipIndex, BLIP_INDEX &OverlayBlipIndex, INT &iBlipType, INT &iBlipNameHash, INT iBlipIndex)
TYPEDEF PROC _T_GET_SIMPLE_INTERIOR_CAR_GENS_BLOCK_AREA							(SIMPLE_INTERIORS eSimpleInteriorID, BOOL &bDoBlockCarGen, VECTOR &vMin, VECTOR &vMax)
TYPEDEF PROC _T_GET_SIMPLE_INTERIOR_SCENARIO_PED_REMOVAL_SPHERE					(SIMPLE_INTERIORS eSimpleInteriorID, BOOL &bDoRemoveScenarioPeds, VECTOR &vCoords, FLOAT &fRadius)
TYPEDEF PROC _T_INT_GET_UPDATED_EXIT_LOCATE_DETAILS								(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX pOwner, SIMPLE_INTERIOR_EXIT_DATA &eData)
TYPEDEF PROC _T_SIMPLE_INTERIOR_EXTERIOR_CUTSCENE_CLEANUP						(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim, BOOL bCleanupMPScene = TRUE)
TYPEDEF PROC _T_PROCESS_EXIT_MENU												(SIMPLE_INTERIORS eSimpleInteriorID, INT iExitID, PLAYER_INDEX piOwner, SIMPLE_INTERIOR_MENU& ref_menu)
TYPEDEF PROC _T_GET_SIMPLE_INTERIOR_ENTRANCE_SCENE_DETAILS						(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS& details, INT iEntranceID)
TYPEDEF PROC _T_GET_SIMPLE_INTERIOR_EXTERIOR_OBJECT_COORDS						(SIMPLE_INTERIORS eSimpleInteriorID, VECTOR &vPosition, VECTOR &vRotation, INT iIndex)
TYPEDEF PROC _T_GET_MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_ACCESS			(SIMPLE_INTERIOR_DETAILS& details)
TYPEDEF PROC _T_MAINTAIN_SIMPLE_INTERIOR_OUTSIDE_GAMEPLAY_MODIFIERS				(SIMPLE_INTERIOR_DETAILS &details, SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_EXT_SCRIPT_STATE eState, BOOL &bExtScriptMustRun)
TYPEDEF PROC _T_PROCESS_CUSTOM_ENTRANCE_MENU									(SIMPLE_INTERIOR_CUSTOM_MENU_IN_DATA& ref_inData, SIMPLE_INTERIOR_CUSTOM_MENU_OUT_DATA& ref_outData)
TYPEDEF PROC _T_DO_SML_INT_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT		(SIMPLE_INTERIORS eSimpleInteriorID, VEHICLE_INDEX veh, BOOL bSetOnGround)

TYPEDEF FUNC BOOL _T_GET_SIMPLE_INTERIOR_SPAWN_POINT							(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle, BOOL bUseSecondaryInteriorDetails = FALSE)
TYPEDEF FUNC BOOL _T_GET_SIMPLE_INTERIOR_OUTSIDE_SPAWN_POINT					(SIMPLE_INTERIORS eSimpleInteriorID, INT iSpawnPoint, VECTOR &vSpawnPoint, FLOAT &fSpawnHeading, BOOL bSpawnInVehicle)
TYPEDEF FUNC BOOL _T_GET_SIMPLE_INTERIOR_OUTSIDE_MODEL_HIDE						(SIMPLE_INTERIORS eSimpleInteriorID, INT iIndex, VECTOR &vPos, FLOAT &fRadius, FLOAT &fActivationRadius, MODEL_NAMES &eModel, BOOL &bSurviveMapReload)
TYPEDEF FUNC BOOL _T_CAN_LOCAL_PLAYER_BE_INVITED_TO_SIMPLE_INTERIOR_BY_PLAYER	(SIMPLE_INTERIORS eSimpleInteriorID, PLAYER_INDEX pInvitingPlayer)
TYPEDEF FUNC BOOL _T_CAN_PLAYER_USE_SIMPLE_INTERIOR_ENTRY_IN_CURRENT_VEHICLE	(SIMPLE_INTERIOR_ENTRANCE_DATA &data, SIMPLE_INTERIOR_DETAILS &details, BOOL &bReturnSetExitCoords)
TYPEDEF FUNC BOOL _T_IS_SPECIAL_CUT_SCENE_ASSETS_READY							(SIMPLE_INTERIORS eSimpleInteriorID, SIMPLE_INTERIOR_DETAILS &details)

TYPEDEF FUNC STRING _T_GET_SIMPLE_INTERIOR_KICK_OUT_REASON						(SIMPLE_INTERIORS eSimpleInteriorID, TEXT_LABEL_63 &txtExtraString, TEXT_LABEL_31 &txtPlayerNameString, INT iInvitingPlayer = -1)

TYPEDEF FUNC FLOAT _T_KD_TREE_GET_OBJECT_SORTABLE_VALUE(INT iElementID, INT iAxis = 0)
TYPEDEF FUNC FLOAT _T_KD_TREE_GET_SEARCH_OBJECT_VALUE(INT iAxis = 0)

TYPEDEF FUNC SIMPLE_INTERIOR_DETAILS_BS _T_GET_SIMPLE_INTERIOR_DETAILS_BS(SIMPLE_INTERIORS eSimpleInteriorID)

TYPEDEF FUNC MODEL_NAMES _T_GET_SIMPLE_INTERIOR_EXTERIOR_OBJECT_MODEL(SIMPLE_INTERIORS eSimpleInteriorID, INT iIndex)

TYPEDEF FUNC SIMPLE_INTERIOR_CUSTOM_INTERIOR_WARP_PARAMS _T_GET_SI_CUSTOM_INTERIOR_WARP_PARAMS (SIMPLE_INTERIORS eSimpleInteriorID)

TYPEDEF FUNC SIMPLE_INTERIORS _T_SIMPLE_INTERIOR_ID_FP_2x_INT_PARAMS(PLAYER_INDEX playerID, INT iPropertySlot, INT iFactoryType)

TYPEDEF FUNC BLIP_PRIORITY _T_GET_SIMPLE_INTERIOR_BLIP_PRIORITY(SIMPLE_INTERIORS eSimpleInteriorID, INT iBlipIndex = 0)

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡  DATA STRUCTURES  ╞═══════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

STRUCT SIMPLE_INTERIOR_INTERFACE
	/////////////////////////////////////////////////////////////////
	///     Before adding a new function pointer consider reusing one if possible
	///     The aim of this is to reduce the size of this struct when it's instantiated in a function e.g. every time a simple interior function is called
	///     This will still return the right function via switching on SIMPLE_INTERIOR_INTERFACE_PROCEDURES
	///     
	///     Example:
	///     _T_SIMPLE_INTERIOR_BOOL_FP_ONLY_ID_PARAM simpleInteriorFP_ReturnB_ParamID returns a bool and takes just the simpleinterior ID as a parameter
	///     	Used by: E_DOES_SIMPLE_INTERIOR_USE_EXTERIOR_SCRIPT & E_GET_OWNED_SIMPLE_INTERIOR_OF_TYPE
	/////////////////////////////////////////////////////////////////
	
	//========================== GENERIC FUNCTIONS =====================================
	_T_SIMPLE_INTERIOR_BOOL_FP_ONLY_ID_PARAM 						simpleInteriorFP_ReturnB_ParamID
	_T_SIMPLE_INTERIOR_BOOL_FP_ID_AND_ENTRYANIM_PARAM				simpleInteriorFP_ReturnB_ParamIDAndEA
	_T_SIMPLE_INTERIOR_BOOL_FP_ID_AND_DEBUGBOOL_PARAM				simpleInteriorFP_ReturnB_ParamIDAndDebugB
	_T_SIMPLE_INTERIOR_BOOL_FP_PID_AND_ID_PARAM						simpleInteriorFP_ReturnB_ParamPIDAndID
	_T_SIMPLE_INTERIOR_BOOL_FP_ID_AND_MODEL_PARAM					simpleInteriorFP_ReturnB_ParamPIDAndModel
	_T_SIMPLE_INTERIOR_BOOL_FP_PID_ID_AND_INT_PARAM					simpleInteriorFP_ReturnB_ParamPID_ID_AndInt
	_T_SIMPLE_INTERIOR_BOOL_FP_PID_ID_AND_INTAND_BOOL_PARAM			simpleInteriorFP_ReturnB_ParamPID_ID_AndIntAndBool
	_T_SIMPLE_INTERIOR_BOOL_FP_ID_PID_AND_BOOL_PARAM				simpleInteriorFP_ReturnB_ParamID_PID_AndB
	_T_SIMPLE_INTERIOR_BOOL_FP_ID_INT_PARAM							simpleInteriorFP_ReturnB_ParamIDAndInt
	_T_SIMPLE_INTERIOR_BOOL_FP_ID_TL63_BOOLX2_PARAM					simpleInteriorFP_ReturnB_ParamID_tl63_AndBoolx2
	_T_SIMPLE_INTERIOR_BOOL_FP_ID_BOOL_TL15_PARAM					simpleInteriorFP_ReturnB_ParamID_Bool_Andtl15
	_T_SIMPLE_INTERIOR_INT_FP_ONLY_ID_PARAM 						simpleInteriorFP_ReturnI_ParamID
	_T_SIMPLE_INTERIOR_INT_FP_ID_AND_INT_PARAM						simpleInteriorFP_ReturnI_ParamIDAndInt
	_T_SIMPLE_INTERIOR_STRING_FP_ONLY_ID_PARAM 						simpleInteriorFP_ReturnS_ParamID
	_T_SIMPLE_INTERIOR_VECTOR_FP_ONLY_ID_PARAM						simpleInteriorFP_ReturnV_ParamID
	_T_SIMPLE_INTERIOR_VECTOR_FP_ID_AND_INT_PARAM					simpleInteriorFP_ReturnV_ParamIDAndInt
	_T_SIMPLE_INTERIOR_ASYNC_FP_ONLY_ID_PARAM						simpleInteriorFP_ReturnEGW_ParamID	//ASYNC_GRID_WARP
	_T_SIMPLE_INTERIOR_BLIP_SPRITE_FP_ID_AND_INT_PARAM				simpleInteriorFP_ReturnEBS_ParamIDAndInt
	_T_SIMPLE_INTERIOR_BOOL_FP_ID_INT_VEC_AND_FLOAT_PARAM			simpleInteriorFP_ReturnB_ParamIDAndIntAndVecAndFloat
	_T_SIMPLE_INTERIOR_STRING_FP_ID_TL63_AND_INT_PARAM 				simpleInteriorFP_ReturnS_ParamID_TL63_AndInt
	_T_SIMPLE_INTERIOR_STRING_FP_ID_AND_GT_PARAM					simpleInteriorFP_ReturnS_ParamID_gameTextID
	
	//========================== GENERIC PROCEDURES =====================================
	_T_SIMPLE_INTERIOR_PROC_FP_ID									simpleInteriorFPProc_ParamID
	_T_SIMPLE_INTERIOR_PROC_FP_ID_AND_PLYR_PARAM					simpleInteriorFPProc_ParamIDAndPlayerID
	_T_SIMPLE_INTERIOR_PROC_FP_ID_AND_DETAILS_PARAM 				simpleInteriorFPProc_ParamDetailsAndID
	_T_SIMPLE_INTERIOR_PROC_FP_ID_AND_ENTRYANIM_PARAM				simpleInteriorFPProc_ParamIDAndEA
	_T_SIMPLE_INTERIOR_PROC_FP_ID_AND_ENTRYMENU_PARAM				simpleInteriorFPProc_ParamIDAndEMenu
	_T_SIMPLE_INTERIOR_PROC_FP_ID_AND_CUSTOMMENU_PARAMS				simpleInteriorFPProc_ParamIDAndCustomMenu
	_T_SIMPLE_INTERIOR_PROC_FP_ID_ENTRYMENU_AND_VEHICLE_PARAMS		simpleInteriorFPProc_ParamIDCustomMenuAndVehicle
	_T_SIMPLE_INTERIOR_PROC_FP_ID_TL15_AND_STRING					simpleInteriorFPProc_ParamIDAndTL15AndStringAr
	_T_SIMPLE_INTERIOR_PROC_FP_ID_AND_INT_PARAM						simpleInteriorFPProc_ParamIDAndInt
	_T_SIMPLE_INTERIOR_PROC_FP_ID_AND_REF_INT_PARAM					simpleInteriorFPProc_ParamIDAndRefInt
	_T_SIMPLE_INTERIOR_PROC_FP_ID_AND_INT_AND_REF_INT_PARAM			simpleInteriorFPProc_ParamIDAndIntAndRefInt
	
	//========================== NON_GENERIC FUNCTION POINTERS ==========================
	
	_T_GET_SIMPLE_INTERIOR_INTERIOR_TYPE_AND_POSITION 				getSimpleInteriorInteriorTypeAndPosition
	_T_GET_SIMPLE_INTERIOR_DETAILS 									getSimpleInteriorDetails	
	_T_GET_SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX 						getSimpleInteriorInsideBoundingBox		
	_T_GET_SIMPLE_INTERIOR_ENTRANCE_DATA 							getSimpleInteriorEntranceData
	_T_GET_SIMPLE_INTERIOR_DETAILS_BS								getSimpleInteriorDetailsPropertiesBS
	_T_SIMPLE_INTERIOR_ID_FP_2x_INT_PARAMS							getOwnedSimpleInteriorOfType
	
	_T_GET_SIMPLE_INTERIOR_EXTERIOR_OBJECT_MODEL					getSimpleInteriorExteriorObjectModel
	_T_GET_SIMPLE_INTERIOR_EXTERIOR_OBJECT_COORDS 					getSimpleInteriorExtObjectLocation	
	_T_PROCESS_CUSTOM_ENTRANCE_MENU 								processSimpleInteriorCustomEntranceMenu
	_T_GET_SIMPLE_INTERIOR_KICK_OUT_REASON 							getSimpleInteriorKickOutReason
	_T_SET_SIMPLE_INTERIOR_ACCESS_BS 								setSimpleInteriorAccessBS
	
	_T_GET_SIMPLE_INTERIOR_SPAWN_POINT 								getSimpleInteriorSpawnPoint
	_T_GET_SIMPLE_INTERIOR_OUTSIDE_SPAWN_POINT 						getSimpleInteriorOutsideSpawnPoint
	
	_T_GET_SIMPLE_INTERIOR_EXIT_MENU_TITLE_AND_OPTIONS 				getSimpleInteriorExitMenuTitleAndOptions
	_T_PROCESS_EXIT_MENU 											processSimpleInteriorExitMenu
	
	_T_MAINTAIN_SIMPLE_INTERIOR_BLIP_EXTRA_FUNCTIONALITY 			maintainSimpleInteriorBlipExtraFunctionality
	_T_GET_SIMPLE_INTERIOR_BLIP_PRIORITY							getSimpleInteriorBlipPriority
	
	_T_MAINTAIN_SIMPLE_INTERIOR_OUTSIDE_GAMEPLAY_MODIFIERS 			maintainSimpleInteriorOutsideGameplayModifiers
	
	_T_GET_SIMPLE_INTERIOR_CAR_GENS_BLOCK_AREA 						getSimpleInteriorCarGensBlockArea
	_T_GET_SIMPLE_INTERIOR_OUTSIDE_MODEL_HIDE 						getSimpleInteriorOutsideModelHide
	_T_GET_SIMPLE_INTERIOR_SCENARIO_PED_REMOVAL_SPHERE 				getSimpleInteriorScenarioPedRemovalSphere
	
	_T_IS_SPECIAL_CUT_SCENE_ASSETS_READY 							isSpecialCutSceneAssetsReady
	
	_T_INT_GET_UPDATED_EXIT_LOCATE_DETAILS 							intGetUpdatedExitLocateDetails
	_T_CAN_LOCAL_PLAYER_BE_INVITED_TO_SIMPLE_INTERIOR_BY_PLAYER 	intCanLocalPlayerBeInvitedToSimpleInteriorByPlayer
	
	_T_SIMPLE_INTERIOR_EXTERIOR_CUTSCENE_CLEANUP 					extCutsceneEnterCleanup
	
	_T_CAN_PLAYER_USE_SIMPLE_INTERIOR_ENTRY_IN_CURRENT_VEHICLE 		canPlayerUseEntryInCurrentVeh
	_T_GET_SIMPLE_INTERIOR_ENTRANCE_SCENE_DETAILS 					getSimpleInteriorEntranceSceneDetails
	_T_GET_MAINTAIN_GET_OUT_OF_ENTERING_VEHICLE_IF_NO_ACCESS 		maintainLeaveentryVehIfNoAccess
	
	_T_GET_SI_CUSTOM_INTERIOR_WARP_PARAMS							getSimpleInteriorCustomInteriorWarpParams
	
	_T_DO_SML_INT_CUSTOM_VEH_ACTION_FOR_STATE_WARP_TO_SPAWN_POINT	DoCustomActionForStateWarpToSpawnPoint
	
	///////////////////////////////////////////////////////////////////////////////////
	///     SEE NOTE AT THE TOP OF STRUCT BEFORE ADDING A NEW FUNCTION POINTER     /// 
	/////////////////////////////////////////////////////////////////////////////////
	
	BOOL bUseCustomExitMenuProcess = FALSE
ENDSTRUCT

STRUCT KD_TREE_NODE
	INT iLeftNodePointer = -1
	INT iRightNodePointer = -1
	INT iElementID
ENDSTRUCT

STRUCT KD_TREE
	KD_TREE_NODE nodePool[KD_TREE_MAX_SIZE_OF_ARRAY]
	INT iNodesCount
	INT iDimension
	_T_KD_TREE_GET_OBJECT_SORTABLE_VALUE getValue
ENDSTRUCT

STRUCT ASYNC_GRID_SPAWN_SERVER_BD
	PLAYER_INDEX pAllowedWarpers[COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS)]
	INT iPlayersRequestingWarpsBS[COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS)]
	TIME_DATATYPE timeCurrentWarperStart[COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS)]
	
	// Group Warp
	PLAYER_INDEX pWarpOrder[COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS)][NUM_NETWORK_PLAYERS]
	INT iQueueLength[COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS)]
ENDSTRUCT

STRUCT ASYNC_GRID_SPAWN_LOCATION_DETAILS
	VECTOR vStartCoord
	FLOAT fSpacingX, fSpacingY, fOffsetZ
	FLOAT fProbeRadius
	FLOAT fGridHeading
	INT iGridWidth
	INT iGridHeight
	INT iExcludedPointsBS[8]
	INT iSpawnOriginPointID // Point to which we should be spawning the closest.
	FLOAT fProbeHeight = 1.5
	BOOL bHumanizePos // Set to true to offset poiints positions so they are not uniformly distributed.
	FLOAT fSinSpread, fSinAmp // Parameters of sin used to humanize the coords.
ENDSTRUCT

STRUCT ASYNC_GRID_SPAWN_GLOBAL_DATA
	BOOL bDoingWarp
	
	ASYNC_GRID_SPAWN_LOCATIONS location = GRID_SPAWN_LOCATION_INVALID
	SCRIPT_TIMER requestTimer // For checking if our request timedout and asking server for it again.
	
	INT iCurrentShapeTestPoint
	SHAPETEST_INDEX currentShapeTest
	BOOL bGotNextPoint
	FLOAT fPointsDist[256]
	INT iPointsIndices[256]
	BOOL bSortDone
	BOOL bUsePresortedArray = TRUE
	INT iCurrentSortedIndex
	
	BOOL bGotSpawnPoint
	VECTOR vSpawnPoint
	
	INT iStartTime
	INT iEndTime
	
	INT iUsedPointsBS[8]
	INT iPointsUsedByOthersBS[COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS)][8]
	SCRIPT_TIMER timerPointsUsedByOthers[COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS)]
	
	// Group Warp
	BOOL bRequestingGroupWarp
	BOOL bServerConfirmedGroupWarp
	INT iWarpConfirmedPlayersBS[COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS)]
	INT iWarpFinishedPlayersBS[COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS)]
	
	// Triggering grid warp via event
	BOOL bDoEventWarp
	ASYNC_GRID_SPAWN_LOCATIONS locationFromEvent

	// Server Stuff
	INT iServerPlayerStagger
	
	#IF IS_DEBUG_BUILD
	ASYNC_GRID_SPAWN_LOCATION_DETAILS db_gridCreator
	BOOL db_bGridCreatorActive
	BOOL db_bGrabPlayerPosForCreatorStart
	BOOL db_bDumpCreatedGrid
	BOOL db_bClearCreatorExcludedPoints
	TEXT_WIDGET_ID db_textWidgetID
	
	ASYNC_GRID_SPAWN_LOCATIONS db_processedLocation = GRID_SPAWN_LOCATION_INVALID
	BOOL db_bDrawPoints = TRUE
	INT db_iProcessedPointsCount
	BOOL db_bStartWarp[COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS)]
	BOOL db_bLoadGridToCreator[COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS)]
	INT db_iOverwritePoint[COUNT_OF(ASYNC_GRID_SPAWN_LOCATIONS)]
	
	// Group Warp Test
	INT db_iGroupPlaces[NUM_NETWORK_PLAYERS]
	INT db_iGroupWarpLocation
	BOOL db_bStartGroupWarp
	INT db_iGroupWarpCurrentPlace
	SCRIPT_TIMER db_timer
	
	// Queue Test
	BOOL db_bQueueTestIsOn
	INT db_iQueuePlayerToPush
	BOOL db_bQueueTestPush
	BOOL db_bQueueTestPop
	#ENDIF
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT SIMPLE_INTERIOR_DEBUG_DATA
	WIDGET_GROUP_ID widget
	BOOL bDoTeleport[SIMPLE_INTERIOR_END]
	BOOL bDoAutoWarp[SIMPLE_INTERIOR_END]
	
	INT iCoronaHidden[SIMPLE_INTERIOR_END]
	INT iCoronaColourOverridden[SIMPLE_INTERIOR_END]
	INT iBlipHidden[SIMPLE_INTERIOR_END]
	INT d_iGlobalPlayerBD_RequestingInstance
	INT d_iGlobalPlayerBD_DoingAutowarp
	INT d_iGlobalPlayerBD_AutowarpDone
	
	BOOL bDrawKdTree
	BOOL bBuildKDTree
	
	INT iEventAutowarpSimpleInteriorID
	INT iEventAutowarpSimpleInteriorID2
	INT iEventAutowarpSimpleInteriorID3
	BOOL bEventAutowarpDoAutowarp
	BOOL bEventAutowarpDontUseFades
	BOOL bEventAutowarpUseList2
	BOOL bEventAutowarpUseList3
ENDSTRUCT
#ENDIF

CONST_INT MAX_SIMPLE_INTERIOR_BLIPS			2
CONST_INT SIMPLE_INTERIOR_PRIMARY_BLIP		0
CONST_INT SIMPLE_INTERIOR_SECONDARY_BLIP	1

STRUCT SIMPLE_INTERIOR_BLIP_DATA
	BLIP_INDEX blips[SIMPLE_INTERIOR_END]
	BLIP_INDEX blipOverlays[SIMPLE_INTERIOR_END]
	
	INT iBlipNameHashes[SIMPLE_INTERIOR_END] // For monitoring changes of blip name.
	INT iBlipTypes[SIMPLE_INTERIOR_END]
ENDSTRUCT

STRUCT SIMPLE_INTERIOR_LOCAL_DATA
	INT iBS
	SIMPLE_INTERIORS eExtStagger // Slow loop for launching exterior scripts.
	INT iConcealStagger // Slow loop for concealing players.
	
	SIMPLE_INTERIOR_BLIP_DATA blipsInteriorData[MAX_SIMPLE_INTERIOR_BLIPS]
	
	INT iServerPlayerInstanceStagger
	
	SIMPLE_INTERIOR_INSIDE_BOUNDING_BOX currentStaggerBBox // Being at current stagger this is this interior bbox.
	
	INTERIOR_INSTANCE_INDEX interiorIndices[SIMPLE_INTERIOR_END]
	
	INT iOutsideModelHideSetUpBS[SIMPLE_INTERIOR_BITSET_ARRAY_SIZE]
	INT iAmbientPedsRemovedBS[SIMPLE_INTERIOR_BITSET_ARRAY_SIZE]
	SIMPLE_INTERIORS eSimpleInteriorPlayerWasInLastFrame = SIMPLE_INTERIOR_INVALID
	
	// THIS IS HERE ONLY FOR THE AUGUST FLOWPLAY
	INT iWasFactoryOwnedLastFrameBS
	INT iWasFactoryOwnedLastFrameBS2
	// END OF AUGUST FLOWPLAY
	
	KD_TREE kdTree
	SIMPLE_INTERIORS closestSimpleInteriors[10]
	INT iClosestSimpleInteriorsCount
	
	//Global flag timers that were hiding in net_simple_interior
	SCRIPT_TIMER sMOCExitAllTimer
	SCRIPT_TIMER sHackerEnterFlagTimer
	SCRIPT_TIMER sHackerTruckCanEnterFlagTimer
	SCRIPT_TIMER sPilotShuffleFlagTimer
	SCRIPT_TIMER sPilotEnterFlagTimer
	SCRIPT_TIMER sWarpOutConcealFlagTimer
	SCRIPT_TIMER sConcealFlagSafeTimer
	SCRIPT_TIMER sAvengerExitAllTimer
	SCRIPT_TIMER sWarpOutOfNightClubWithoutIntScript
	SCRIPT_TIMER sAutowarpCleanupTimer
	SCRIPT_TIMER sKickPlayersOutOfVeh
	
	#IF IS_DEBUG_BUILD
	SIMPLE_INTERIOR_DEBUG_DATA debug
	#ENDIF
ENDSTRUCT

// Struct passed to AM_MP_SIMPLE_INTERIOR_EXT and AM_MP_SIMPLE_INTERIOR_INT
STRUCT SIMPLE_INTERIOR_SCRIPT_DATA
	SIMPLE_INTERIORS eSimpleInteriorID
	INT iInvitingPlayer
	INT iScriptInstance
	BOOL bUseSecondaryInteriorData = FALSE
ENDSTRUCT

// Struct passed to any specific script launched by AM_MP_SIMPLE_INTERIOR_INT
STRUCT SIMPLE_INTERIOR_CHILD_SCRIPT_DATA
	SIMPLE_INTERIORS eSimpleInteriorID
	INT iInvitingPlayer
	INT iScriptInstance
	BOOL bWasRelaunched
	
	INTERIOR_INSTANCE_INDEX interiorInstanceIndex
ENDSTRUCT

STRUCT GLOBAL_PLAYER_BD_SIMPLE_INTERIOR_DATA
	INT iBS
	INT iBSTwo
	INT iBSThree
	INT iBS4
	INT iBSFive
	INT iBSSix
	INT iBSSeven
	
	SIMPLE_INTERIORS eCurrentSimpleInterior = SIMPLE_INTERIOR_INVALID
	INT iInstance = -1
	INT iPlayersToJoinBS // Bitset indicating which players this player wants to join when trying to get an instance for interior.

	PLAYER_INDEX propertyOwner // Bitset indicating the owner of the property I want to enter.
	PLAYER_INDEX remotePropertyOwner // Used to broadcast to other players, which remote property we have selected to enter
	VECTOR vMobileInteriorExitSpawnPoint
	FLOAT fMobileInteriorExitHeading
		
	SIMPLE_INTERIORS eCurrentAutoWarpInterior = SIMPLE_INTERIOR_INVALID
	SIMPLE_INTERIORS eCurrentWalkingInOrOutOfSimpleInterior = SIMPLE_INTERIOR_INVALID
	SIMPLE_INTERIORS eSimpleInteriorDriverIsEntering = SIMPLE_INTERIOR_INVALID
	VECTOR vArmoryAircraftExitSpawnPoint
	VECTOR vSpectatorCamCoord
	FLOAT fArmoryAircraftExitHeading
	
	INT iSMPLIntInteriorCrossSessionData	= -1	// Used for cross session heist transitions so we can send this to any player we invite
	INT iEntranceIAmUsing 					= -1	//Int to allow all players entering in a vehicle to sync the interior entry point used
ENDSTRUCT

STRUCT GLOBAL_SERVER_BD_SIMPLE_INTERIOR_DATA
	INT iBS
	
	INT iPlayerInstances[NUM_NETWORK_PLAYERS]
	INT iInstanceTimeStamp[NUM_NETWORK_PLAYERS]
	INT iPlayersRequestingInstancesBS
	INT iPlayersBSPlayersWantsToJoin[NUM_NETWORK_PLAYERS] // BS for each player specifying which players that player wants to join.
	INT iPlayersFreeingScriptInstancesBS
	TIME_DATATYPE timeInstanceServerTimestamps[NUM_NETWORK_PLAYERS] // When an instance for a particular player was calculated, it's a cooldown before we remove that instance if not used.
	SIMPLE_INTERIORS ePlayerInteriorIDs[NUM_NETWORK_PLAYERS] // Interiors players are or want to be in.
	BOOL bInteriorIsPublic[NUM_NETWORK_PLAYERS] // Tells us that the interior we are in is the public instance.
	
	PLAYER_INDEX iOwnerOfPropertyIamIn[NUM_NETWORK_PLAYERS]
ENDSTRUCT

STRUCT SIMPLE_CUTSCENE_SHOT
	INT iDuration
	VECTOR vCamCoordsStart
	VECTOR vCamCoordsFinish
	VECTOR vCamRotStart
	VECTOR vCamRotFinish
	FLOAT fCamFovStart
	FLOAT fCamFovFinish
	FLOAT fCamShake
	TEXT_LABEL_31 strName
	INT iFadeInTime
	INT iFadeOutTime
	INT iHoldLastShotTime
	CAMERA_GRAPH_TYPE graphTypePos = GRAPH_TYPE_LINEAR
	CAMERA_GRAPH_TYPE graphTypeRot = GRAPH_TYPE_LINEAR
	
	BOOL bUseDOF
	FLOAT fDOFStrength, fDOFNearOut, fDOFFarOut, fDOFNearIn, fDOFFarIn
	FLOAT fLodOverride
ENDSTRUCT

STRUCT SIMPLE_CUTSCENE_HELP
	INT iDuration
	INT iStartTime
	TEXT_LABEL_23 txtHelp
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT SIMPLE_CUTSCENE_DEBUG_DATA
	INT iCurrentScene
	INT iPreviousCurrentScene = -1
	BOOL bPlayScene
	BOOL bPlayCutscene
	BOOL bDumpScene
	BOOL bDumpSceneCoords
	BOOL bDumpCutscene
	BOOL bSaveScene
	VECTOR vCoords1, vCoords2, vRot1, vRot2
	FLOAT fFov1, fFov2
	INT iFadeIn, iFadeOut
	FLOAT fShake
	INT iSceneDuration
	BOOL bSetCoords1, bSetCoords2
	BOOL bSetDebugCamCoords1, bSetDebugCamCoords2
	BOOL bCleanupCutsceneCam
	CAMERA_INDEX cutsceneCamera
	SCRIPT_TIMER timerCutscene
	BOOL bFadeOutDone
	TEXT_WIDGET_ID nameWidget
	TEXT_WIDGET_ID helpWidget
	
	INT iCurrentHelp
	INT iPreviousCurrentHelp = -1
	INT iHelpStart
	INT iHelpDuration
	BOOL bSaveHelp
	BOOL bDumpHelp
	BOOL bPlayedViaDebug
	
	BOOL bDontUpdateCamera
	
	BOOL bCurrentSceneUsesDOF
	FLOAT fDOFStrength, fDOFNearOut, fDOFFarOut, fDOFNearIn, fDOFFarIn
ENDSTRUCT
#ENDIF

// Doesn't have fixed scene count, allows to pass around specific scene size.
STRUCT SIMPLE_CUTSCENE_LIGHT
	INT iBS
	TEXT_LABEL_63 strName
	INT iScenesCount
	INT iHelpsCount
	INT iCurrentSceneTimer
	INT iCurrentSceneElapsedTime
	INT iStartTime
	INT iTotalElapsedTime
	INT iTotalDuration
	INT iBSScenesInit
	INT iBSHelpsInit
	INT iBSFadeOutStarted
	INT iCurrentScene
	INT iCurrentHelp
	INT iCurrentHelpTimer
	BOOL bHelpTimerInit
	BOOL bPlaying
	CAMERA_INDEX camera
	
	BOOL bUseRelativePos
	VECTOR vBasePos
	FLOAT fBaseHeading
	
	#IF IS_DEBUG_BUILD
	SIMPLE_CUTSCENE_DEBUG_DATA dbg
	#ENDIF
ENDSTRUCT

STRUCT SIMPLE_CUTSCENE
	INT iBS
	INT iSetupBM
	TEXT_LABEL_63 strName
	SIMPLE_CUTSCENE_SHOT sScenes[SIMPLE_CUTSCENE_MAX_SCENES]
	SIMPLE_CUTSCENE_HELP sHelps[SIMPLE_CUTSCENE_MAX_HELP]
	INT iScenesCount
	INT iHelpsCount
	INT iCurrentSceneTimer
	INT iCurrentSceneElapsedTime
	INT iStartTime
	INT iTotalElapsedTime
	INT iTotalDuration
	INT iBSScenesInit
	INT iBSHelpsInit
	INT iBSFadeOutStarted
	INT iCurrentScene
	INT iCurrentHelp
	INT iCurrentHelpTimer
	BOOL bHelpTimerInit
	BOOL bPlaying
	CAMERA_INDEX camera
	
	SIMPLE_INTERIORS eSimpleInteriorForCutscene // If cutscene is played inside simple interior it will use relative cam coords.
	
	BOOL bUseRelativePos
	VECTOR vBasePos
	FLOAT fBaseHeading
	
	//Animated camera support
	INT iSyncSceneID
	TEXT_LABEL_63 sSceneAnimDict
	TEXT_LABEL_63 sSceneCamAnim
	
	VECTOR vAnimScenePosition
	VECTOR vAnimSceneRotation
	CAM_ANIMATION_FLAGS AnimFlags = CAF_DEFAULT
	EULER_ROT_ORDER RotOrder = EULER_YXZ
	
	#IF IS_DEBUG_BUILD
	SIMPLE_CUTSCENE_DEBUG_DATA dbg
	#ENDIF
	
ENDSTRUCT

CONST_INT NUM_TERMINATING_SIMPLE_EXT_SCRIPTS 15

//BS
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_TERMINATE_ALL_EXT_SCRIPTS 			0
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_TERMINATE_INT_SCRIPT				1
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_ENTRY_TO_ALL_INTERIORS		2
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_MOC_ENTRY					3
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_AVENGER_ENTRY				4
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_BIKER_WH_ENTRY				6
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_BUNKER_ENTRY					7
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_HANGAR_ENTRY					8
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_FACILITY_ENTRY				9
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_ARENA_GARAGE_ENTRY			10
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_BUSINESS_HUB_ENTRY			11
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_TERRORBYTE_ENTRY				12
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_SECONDARY_ENTRY_FLAG_CLEAR	13
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_STATE_DEPENDENCIES_FADE_IN	14
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_DBASE_CLEAR_INT_CLONE_TASKS	15
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_TRANSITION_VAR_PREVENT_KICK	16
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_CRS_SES_KICK_OUT_VAR_SETUP	17
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_DISABLE_EXTERIOR_RESET				18
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_EXT_ENABLE_VEH_ENTRY_CLEANUP_ON_SC	19 //Enable the exterior script cleanup to cleanup vehicle entry data on script cleanup
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_EXT_DISABLE_VEH_REPLACE_PROCESS	20 //Enable the exterior script to process vehicle replace functionality
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_EXT_FORCE_CANCEL_VEH_REPLACE		21 //Enable the exterior script to process vehicle replace functionality
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_INT_DISABLE_OWNER_VEH_ENTRY_CHECK	22
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_CASINO_ENTRY					23
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_CASINO_APT_ENTRY				24
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_CASINO_NIGHTCLUB_ENTRY		25
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_CASINO_VAL_GARAGE_ENTRY		26
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_FORCE_TERMINATE_ST_SPECTATE		27	//Terminate the strike team spectator cam used in the defunct base simple interior
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_AUTOWARP_ENTRANCE_CHECK		28	//Allow the autowarp entrance blocer check to function
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_BLOCK_TERMINATE_INTERIOR_CHECK		29	//Blocks the check for g_SimpleInteriorData.iSimpleInteriorInTerminateProcess
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_DISREGARD_OWNER_ENTRY_ID_SYNC		30	//Allows processing to continue without having to wait for us to sync the entry point used when entering in a vehicle
CONST_INT SIMPLE_INTERIOR_CONTROL_BS_OVERRIDE_AUTOWARP_ENTRY_SAFETY		31	//Overrides an auto warp safety check in the exterior script preventing us from moving to START_ENTERING

//BS2
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_ARCADE_ENTRY				0
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_SOLOMON_OFFICE_ENTRY		1
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_WAIT_IN_EXT_INT_SCRIPT_LOAD	2
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_SUBMARINE_ENTRY				3
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_DESTROYED_VEH_EXIT			4
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_FORCE_DESTROYED_VEH_EXIT			5
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_DISABLE_SUB_DESTROYED_VEH_KICK	6
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_DISABLE_CASINO_CLUB_ENTRY_FOR_PC	7
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_MUSIC_STUDIO_ENTRY		    8
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_AUTO_SHOP_ENTRY			    9
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_CAR_MEET_ENTRY			    10
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_CAR_MEET_INSTANCE_SWITCH    11
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_CAR_MEET_SPRINT_RACE_EXIT   12
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_FIXER_HQ_ENTRY				13
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_INT_ENTER_MAINTAIN_EXIT		14
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_EXT_VEH_FLAG_CLEANUP		15
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_EXT_VEH_ENTRY_CHECK			16
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_NET_INVITE_SUPPRESSION		17
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_CAR_MEET_SPAWN_NO_LS		18
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_SS_MATC_SUPRESSION			19
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_DL_BLOCK_MISSION_LAUNCH		20
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_EXPORT_CARGO_MISSION_LAUNCH	21
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_BAR_SUPPLY_MISSION_LAUNCH	22
#IF FEATURE_DLC_2_2022
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_JUGGALO_HIDEOUT_ENTRY		23
CONST_INT SIMPLE_INTERIOR_CONTROL_BS2_BLOCK_MULTISTOREY_GARAGE_ENTRY	24
#ENDIF

//Struct of variables used to control simple interior functionality via BG script
STRUCT SIMPLE_INTERIOR_CONTROL_DATA
	INT iBS 	= 0
	INT iBS2 	= 0
	
	SIMPLE_INTERIOR_EXT_SCRIPT_STATE 	eCurrentExtState 				= ENTRANCE_STATE_IDLE		//The current state of the specified exterior script (specified using eExteriorMonitorID)
	SIMPLE_INTERIOR_EXT_SCRIPT_STATE 	eCurrentInsideIntEntranceState 	= ENTRANCE_STATE_IDLE		//The current state of the exterior script for the interior that the player is or activley entering/exiting
	SIMPLE_INTERIORS 					eExteriorMonitorID 				= SIMPLE_INTERIOR_INVALID	//Used to specify which interior (Exterior script) to monitor it's state. We have to do it like this as multiple exterior scripts can run simultaneously
	SIMPLE_INTERIORS 					eExteriorControlID 				= SIMPLE_INTERIOR_INVALID	//Used to specify which interior (Exterior script) to shutdown/control
	SIMPLE_INTERIORS 					eExteriorTerminateID 			= SIMPLE_INTERIOR_INVALID	//Used to target a specific exterior to terminate
	SIMPLE_INTERIOR_EXT_SCRIPT_STATE 	eExtScriptStageID 				= ENTRANCE_STATE_COUNT		//Used to specify which stage to set (EXTERIOR ONLY)
	SIMPLE_INTERIOR_INT_STATE 			eIntScriptStageID 				= SIMPLE_INT_STATE_COUNT	//Used to specify which stage to set (INTERIOR ONLY) no need to specify the target interior as there will only ever be one running
	SIMPLE_INTERIORS 					eExteriorIDToReset 				= SIMPLE_INTERIOR_INVALID	// Used to reset exterior script
ENDSTRUCT

STRUCT SIMPLE_INTERIOR_BUZZER_REQUEST_DATA
	PLAYER_INDEX pOwner
	BOOL bAwaitingPlayerResponse = FALSE
	INT iResponse = -1
	INT iPlayerLoop
	//SCRIPT_TIMER stResponseTimeOut
ENDSTRUCT

STRUCT SIMPLE_INTERIOR_BUZZER_REPLY_DATA
	INT iReplayStatus 	= SIMPLE_INTERIOR_BUZZER_REPLY_STATUS_NO_REQUEST
	INT iPlayerHash		= -1
	TIME_DATATYPE responseTime
ENDSTRUCT

CONST_INT ciINTERIOR_SUBMENU_MAX_DEPTH 2

ENUM SMPL_INT_VEH_EXIT_TYPE
	VEH_EXIT_LIMO,
	VEH_EXIT_VALET
ENDENUM

STRUCT SIMPLE_INTERIOR_VALET_EXIT
	SIMPLE_INTERIORS eInteriorID = SIMPLE_INTERIOR_INVALID
	INT iVehicleSaveSlot = -1
	BOOL bLeavingWithSharedGarageVeh = FALSE
	
	BOOL bLeavingAsOwner 			= FALSE
	INT iPlayersBS 					= -1	//Which players did we select to go with us
	INT iOwner 						= -1
	INT iPassengerSlots				= -1
	
	PED_INDEX ownerPed				= NULL
ENDSTRUCT

STRUCT SIMPLE_INTERIOR_LIMO_SERVICE_EXIT	
	INT iDestination 				= -1
	
	SIMPLE_INTERIORS eInteriorID 	= SIMPLE_INTERIOR_INVALID
	BOOL bLeavingAsOwner 			= FALSE
	INT iPlayersBS 					= -1	//Which players did we select to go with us
	INT iOwner 						= -1
ENDSTRUCT

#IF IS_DEBUG_BUILD
ENUM ARCADE_ANIM_TEST_STAGE
	AATS_SELECT_ANIM_DICT,
	AATS_LOAD_DICT,
	AATS_CREATE_ASSETS,
	AATS_START_ANIM,
	AATS_SWITCH_TO_SCRIPTED_CAM,
	AATS_INTERP_TO_GAME_CAM,
	AATS_WAIT_FOR_ANIM_END
ENDENUM

STRUCT ARCADE_HEIST_EXIT_GLOBAL_DEBUG_DATA
	ARCADE_ANIM_TEST_STAGE eArcadeAnimTestStage = AATS_SELECT_ANIM_DICT
	
	BOOL bTestArcadeHeistExitScene
	INT iSceneVariation = 1
	INT iSyncScene
	INT iNumPeds = 1
	INT iPedScenePosition = 1
	INT iNumPedsReserved = 0
	
	FLOAT fCutPhase = 0.0
	
	VECTOR vScriptedScenePos
	VECTOR vScriptedCamPos
	VECTOR vScriptedCamRot
	FLOAT fScriptedCamFOV
	FLOAT fDefinedPitch
	FLOAT fDefinedHeading
	
	FLOAT fPhaseControlReturn
	INT iBlendDuration = 3000
	
	PED_INDEX dummyPeds[3]
	
	CAMERA_INDEX animatedcam
	CAMERA_INDEX scriptedcam
ENDSTRUCT

ENUM FLYING_SUBMARINE_STATE
	FSS_IDLE,
	FSS_FLYING,
	FSS_DESCENDING_STAGE_1,
	FSS_DESCENDING_STAGE_2
ENDENUM

ENUM TUN_ASD_EXT_TEST_SCENE_STATE
	TAETSS_IDLE,
	TAETSS_ASSET_REQUEST,
	TAETSS_LOAD,
	TAETSS_PLAYBACK,
	TAETSS_CLEANUP
ENDENUM

STRUCT TUN_ASD_EXT_TEST_DATA
	BOOL bStartScene
	BOOL bPickedVeh
	BOOL bPlacedSceneVeh
	VEHICLE_INDEX veh
	INT iSceneVariation
	INT iSceneLocation
	VECTOR vSceneOrigin
	FLOAT fSceneHeading
	TUN_ASD_EXT_TEST_SCENE_STATE eSceneState
ENDSTRUCT
#ENDIF

STRUCT SML_FUNC_RETURN_BOOL 
	BOOL ReturnVal
	INT iFC
ENDSTRUCT

STRUCT SMPL_FUNC_RETURN_VAL_CACHE
	SML_FUNC_RETURN_BOOL DoesRivalClubMenuHaveOptions[NIGHTCLUB_ID_COUNT]
	SML_FUNC_RETURN_BOOL DoesRivalAutoShopHaveOptions[AUTO_SHOP_PROPERTY_ID_COUNT]
	SML_FUNC_RETURN_BOOL DoesRivalArcadeHaveOptions[ARCADE_PROPERTY_ID_COUNT]
	SML_FUNC_RETURN_BOOL CanPlayerEnterCommon
ENDSTRUCT

STRUCT SIMPLE_INTERIOR_EXT_ENTRY_SCENE_TRIGGER
	SIMPLE_INTERIORS eSimpleInteriorID = SIMPLE_INTERIOR_INVALID
	BOOL bTriggerCoronaCam
	INT iEntranceID
ENDSTRUCT

#IF IS_DEBUG_BUILD
CONST_INT EXT_DEBUG_PRINT_RESERVED	0
CONST_INT MAX_SMPL_EXT_DEBUG_PRINTS 10

STRUCT SMPL_INT_EXT_SC_DEBUG_PRINT
	TEXT_LABEL_63 tlPrint[MAX_SMPL_EXT_DEBUG_PRINTS]
ENDSTRUCT
#ENDIF

STRUCT SIMPLE_INTERIOR_VEHICLE_ENTRY_DATA
	SIMPLE_INTERIORS eInteriorEntering = SIMPLE_INTERIOR_INVALID	
	SIMPLE_INTERIORS eInteriorEnteringAsPassenger = SIMPLE_INTERIOR_INVALID
	VEHICLE_SEAT seat
	MODEL_NAMES model
	INT playerCount
ENDSTRUCT

#IF FEATURE_DLC_2_2022
STRUCT SIMPLE_INTERIOR_CUSTOMIZATION_PREVIEW_DATA
	//Active preview
	INT iStylePreview = -1
	INT iTintPreview = -1
	
	//Available preview options
	INT iMaxStyles = 0
	INT iMaxTints = 0
	
	//Active Customizations
	INT iCurrentStyle = -1
	INT iCurrentTint = -1
ENDSTRUCT
#ENDIF

STRUCT SIMPLE_INTERIOR_GLOBAL_DATA
	INT iBS
	INT iSecondBS
	INT iThirdBS
	INT iForthBS
	INT iFifthBS
	INT iSixthBS
	INT iSeventhBS
	INT iEighthBS
	INT iNinthBS
	INT iTenthBS
	INT iEleventhBS
	INT iTwelfthBS
	
	INT iClearAreaBS
	INT iSubmarineAudioBS
	
	SIMPLE_INTERIOR_DETAILS_BS sDetailsBS[SIMPLE_INTERIOR_END]	//Bit set to cache the details of the simple interiors rather than calling to a function pointer every time
	
	BOOL bTruckCabOwnerAccess = FALSE
	BOOL bTriggerExitFromArmoryTruckToCab = FALSE
	BOOL bTriggerArmoryTruckExitToCabFreemode = FALSE
	BOOL bTriggerExitFromArmoryTruck = FALSE
	BOOL bRefreshInteriorMenu = FALSE
	BOOL bDontCleanupHeistCrossSessionData = FALSE
	BOOL bAcceptedSimpleIntSameSessionHeistInv = FALSE
	INT iExitMenuOption = -1
	INT iEntrySceneGrassCullSphereID = -1
	INT iMultiPartEntrySceneSoundIDEXT = -1		//Added here to allow the sound to be started in the exterior script and unloaded in the interior script
	INT iMultiPartEntrySceneSoundIDINT = -1		//Added here to allow the sound to be started in the exterior script and unloaded in the interior script
	INT iBSTruckExit
	
	//Used to check if the player has been set as in an interior
	SIMPLE_INTERIORS eCurrentInteriorID = SIMPLE_INTERIOR_INVALID
	//Used to check which int script is running as soon as it initialises
	SIMPLE_INTERIORS eRunningInteriorID = SIMPLE_INTERIOR_INVALID
	INT iAccessBS
	BOOL bChildScriptReady = FALSE
	BOOL bChildScriptInControl = FALSE // Child script is in control of finishing the entry, int script should wait.
	BOOL bChildScriptAllowsForWarp = FALSE // Child script done what it had to do before it lets player warp inside.
	BOOL bChildScriptBlockingFadeIn = FALSE
	BOOL bChildScriptTimedOut = FALSE
	BOOL bForceInteriorScriptToReloadInterior
	BOOL bForceInteriorScriptToRepinInterior
	BOOL bInteriorHasBeenReloaded
	BOOL bDontSimulateInputGaitOnEntrance = FALSE
	BOOL bDontNetworkFadePlayerOnEntrance = FALSE
	BOOL bSafeForChildScriptToTakeControl = FALSE // Can child script safely take control.
	INT iHashOfMostRecentChildScript = 0
	
	BOOL bWalkingInOrOut = FALSE // This is true for the whole length of transition from outside to hte inside.
	BOOL bWalkingIn = FALSE // When the above is true this can be used to determine if we are walking in.
	SIMPLE_INTERIORS eSimpleInteriorPlayerIsWalkingInOrOut = SIMPLE_INTERIOR_INVALID
	BOOL bEntryAnimPlaying = FALSE
	BOOL bSafeToDoInteriorWarp = FALSE // This is set when exterior script is done with all the animation to let interior script know it's now safe to do the warp.
	BOOL bForceTerminateInteriorScript = FALSE // Set it to true to force terminate any int script that might be running, mainly for debug.
	BOOL bRestorePlayerControlOnIntScriptCleanup = FALSE
	BOOL bMaintainForcePlayerLocallyVisibleForFade = FALSE // When int script finishes some other script must switch off force visible, let freemode do it.
	
	BOOL bUseKDTreeDistanceChecks = TRUE
	#IF IS_DEBUG_BUILD
	BOOL bVerboseKDTree = FALSE
	ARCADE_HEIST_EXIT_GLOBAL_DEBUG_DATA sGlobalDebugArcadeHeistExit
	#ENDIF
	
	SCRIPT_TIMER tInteriorWarp
	SCRIPT_TIMER tIntAccessFallback
	INT iIntAccessFallbackTime	= 10000
	
	VECTOR vMidPoints[SIMPLE_INTERIOR_END] // Mid point positions for simple interiors, for use with blip systems.
	
	VECTOR vMainEntryLocates[SIMPLE_INTERIOR_END]
	INT iBSipl
	#IF IS_DEBUG_BUILD
	INT iEntranceVariation = -1
	BOOL bMirrorEntranceVariation = FALSE
	#ENDIF
	
	VECTOR vEstablishingShotRot[SIMPLE_INTERIOR_END]
	VECTOR vEstablishingShotPos[SIMPLE_INTERIOR_END]
	FLOAT fEstablishingShotFov[SIMPLE_INTERIOR_END]
	
	// Auto warp stuff (pulling players into interior by outside scripts)
	INT iAutoWarpPlayerToInteriorBSs[SIMPLE_INTERIOR_BITSET_ARRAY_SIZE]
	INT iOverrideAutoWarpPositionBSs[SIMPLE_INTERIOR_BITSET_ARRAY_SIZE]
	INT iForceAutowarpToInteriorBSs[SIMPLE_INTERIOR_BITSET_ARRAY_SIZE] // If this is set ext script will be launched and warp will happen from anywhere on the map.
	VECTOR vOverriddenAutoWarpPositions[SIMPLE_INTERIOR_END]
	FLOAT fOverridenAutoWarpHeadings[SIMPLE_INTERIOR_END]
	INT iCanFinishAutoWarpBSs[SIMPLE_INTERIOR_BITSET_ARRAY_SIZE]
	INT iAllAutoWarpDoneBSs[SIMPLE_INTERIOR_BITSET_ARRAY_SIZE]
	INT iUseAutowarpPlayerSyncBS[SIMPLE_INTERIOR_BITSET_ARRAY_SIZE] // Whether this autowarp is synced between the players (this disabled timeout on the whole autowarp and syncs players fade in).
	
	INT iOverrideAutoWarpToPlayersProperty = -1 	// Used to allow non gang members to warp to the same interior instance.
	
	BOOL bAllowInstanceOverrideForCrossSession 	= FALSE	// Used for cross session heist transitions where the owner will not be known
	INT iSMPLIntInteriorDataOverrride			= -1	// Used for cross session heist transitions where the interior style will not be known
	
	INT iNoFadesAutowarpBS[SIMPLE_INTERIOR_BITSET_ARRAY_SIZE]
	
	// People get stuck on black screen, adding a timeout.
	INT iAutowarpTimeout = 15000
	SCRIPT_TIMER timerAutowarpTimeout
	BOOL bEnableAutowarpTimeout = TRUE // Can autowarp timeout?
	BOOL bEnableAutowarpSync = TRUE // Sync autowarp with all other players who are being autowarped.
	SCRIPT_TIMER timerAutowarpSyncPlayerTimeout[NUM_NETWORK_PLAYERS] // Each player gets a timer to check if we time out waiting for them.
	
	// Autowarp vehicle saving - If players are autowarped from a helicotper
	// then we want to prevent it from crashing so we run this whole thing to save it
	// and warp it to some safe spot nearby the interior.
	SIMPLE_INTERIORS eSimpleInteriorSavingVehicle // Interior that should handle saving this vehicle, it the one we were autowarped to.
	VEHICLE_INDEX vehToSaveAfterAutowarp // Vehicle that needs saving.
	SCRIPT_TIMER timerForSavingVehicle // Timer for the vehicle to be empty.
	INT iVehSavingStage // Stage of saving.
	
	BOOL bTriggerExit // Triggering exit from the interior, can be done by some outside scripts.
	BOOL bTriggerExitWithHelp
	BOOL bExitAllTriggeredByOwner
	
	SCRIPT_TIMER timerRetryInstanceRequest // If we sent a request to the server and it doesnt respond in time then send it again.
	SCRIPT_TIMER timerRetryInstanceFree
	
	SIMPLE_INTERIORS eRelaunchInteriorScript 		= SIMPLE_INTERIOR_INVALID
	SIMPLE_INTERIORS eKilledScriptForInteriorID 	= SIMPLE_INTERIOR_INVALID // This gets set if int script gets killed by should_this_thread_terminate.
	SIMPLE_INTERIORS eIntKilledBySSMissionLaunch	= SIMPLE_INTERIOR_INVALID // This gets set when we have killed an interior script for a corona transition
	
	#IF IS_DEBUG_BUILD
	INT d_iTransitionStartTime
	BOOL d_bForceTerminateAllSimpleInteriorScripts
	BOOL d_bUseUpdatedDetailsBS
	TUN_ASD_EXT_TEST_DATA sTunAsdExtTest
	#ENDIF
	
	INT iLastInteriorInstanceUsed = -1
	
	// These were added so exit menu can be managed by warehouse script (while main logic still runs in simple interior script).
	BOOL bShouldExitMenuBeVisible
	BOOL bExitMenuOptionAccepted
	INT iExitUsed = -1 // Exit ID player used.
	INT iLastRoomIndex // The room ID the player was last in
	
	INT iCountOfDynamicEntryLocateInteriors
	SIMPLE_INTERIORS listOfDynamicEntryLocateInteriors[SIMPLE_INTERIOR_DYNAMIC_INTERIORS_COUNT] // Allow some interiors to have a dynamic entry locate that changes, keep them here for later reference.
	
	HUD_COLOURS hudColourCoronaOverrides[SIMPLE_INTERIOR_END]
	INT iCoronaColourOverrideBitSet[SIMPLE_INTERIOR_BITSET_ARRAY_SIZE] // Which warehouses should override their colour.
	INT iHiddenEntrancesBitSet[SIMPLE_INTERIOR_BITSET_ARRAY_SIZE] // Sometimes we want to specifically block entrance to a warehouse.
	INT iHiddenBlipsBitSet[SIMPLE_INTERIOR_BITSET_ARRAY_SIZE]
	INT iFlashBlipsBitSet[SIMPLE_INTERIOR_BITSET_ARRAY_SIZE]
	
	INT iExtScriptRunningBitSet[SIMPLE_INTERIOR_BITSET_ARRAY_SIZE]
	
	BOOL bForceUpdateAllBlipsThisFrame
	BOOL bDeleteBlipsBeforeForceUpdate
	
	// The below are used by staggered loop that determines if any warehouse is owned by any friend.
	INT iOwnedByFriendBitSet[SIMPLE_INTERIOR_BITSET_ARRAY_SIZE]
	INT iOwnedByFriendBitSetTemp[SIMPLE_INTERIOR_BITSET_ARRAY_SIZE]
	INT iFriendOwnerStagger
	INT iOwnedByFriendSimpleInteriorStagger
	
	BOOL bPlayGarageDeliverySound
	BOOL bPlayOnFootDeliverySound
	INT iDeliverySoundID = -1
	SCRIPT_TIMER timerDeliverySoundStart
	BOOL bWarehouseShouldWaitForDeliverySound
	
	BOOL bSuppressConcealCheck
	BOOL bForceCCTVCleanup
	BOOL bForceShootingRangeCleanup
	BOOL bForceDrinkActivitiesCleanup
	
	BOOL bForceOrbitalCannonCleanup
	
	// Sometimes autowarp might be triggered by an outside brodcast event, these vars keep track of that.
	BOOL bEventAutowarpActive
	SIMPLE_INTERIORS eEventAutowarpSimpleInterior = SIMPLE_INTERIOR_INVALID
	BOOL bEventAutowarpOverrideCoords
	VECTOR vEventAutowarpOverriddenCoords
	FLOAT fEventAutowarpOverriddenHeading
	BOOL bEventAutowarpSetInInterior
	
	BOOL bEventAutowarpDontUseFades
	
	BOOL bAcceptedInviteIntoSimpleInterior = FALSE
	
	SIMPLE_INTERIORS eSimpleInteriorIDToKeepKilledUntilWarpIsDone = SIMPLE_INTERIOR_INVALID// If we warp from a different simple interior we gotta kil this script and keep it killed.
	SIMPLE_INTERIORS eSimpleInteriorIDToKeepKilledForHeistLaunch = SIMPLE_INTERIOR_INVALID// If we warp from a different simple interior we gotta kil this script and keep it killed.
	INT iRCIDForSimpleInteriorHeistLaunch = SIMPLE_INTERIOR_INVALID// If we warp from a different simple interior we gotta kil this script and keep it killed.
	BOOL bKillCarmodScriptAfterAcceptingInvite
	
	// Quick Travel
	BOOL bOwnerWantsToQuickTravel
	INT iWarehouseToWarpToOnQuickTravel = -1
	
	INT iExteriorCamShapeTestHitSomething[4]
	VECTOR vExitPosition	// If armory truck is destroyed use this point.
	FLOAT fExitHeading		// If armory truck is destroyed use this point.
	PLAYER_INDEX driverPlayer 	// Dirver of vehicle which is entering smplInterior.
	PLAYER_INDEX truckDriverPlayer
	
	// Armory Truck update
	INT iArmoryTruckInteriorBitSet
	BOOL bPedInJugernautSuit = FALSE
	BOOL bAllowInteriorScriptForActivitySession
	INT iBunkerIDTruckIsIn = -1
	INT iBaseIDAvengerIsIn = -1
	PLAYER_INDEX pArmoryVehiclePassanger
	
	SIMPLE_CUTSCENE sCustomCutsceneExt
	SIMPLE_CUTSCENE sCustomCutsceneInt
	PLAYER_INDEX sCutSceneVehEntryPlayers[CI_MAX_NUMBER_OF_PASSANGERS]
	
	// Set when we're in the locate of a specific interior.
	SIMPLE_INTERIORS eInLocateInteriorID = SIMPLE_INTERIOR_INVALID
	
	#IF IS_DEBUG_BUILD
	BOOL bPlayCasinoExitCelebMocap
	BOOL db_bPlayShortSimpleCutscenes
	INT iDebugBS
	SIMPLE_CUTSCENE db_simpleCutscene
	FLYING_SUBMARINE_STATE eFlyingSubmarineState
	
	SMPL_INT_EXT_SC_DEBUG_PRINT sEXTPrint
	#ENDIF
	
	INT iEntitySetTintIndex = 0
	INT iLightColourIndex = 0
	BOOL bDisplayHelpTextInHangar = FALSE
	
	//Used to allow control over simple interior functionality via BG script
	SIMPLE_INTERIOR_CONTROL_DATA sGlobalController	
	SIMPLE_INTERIOR_VEHICLE_ENTRY_DATA sVehicleEntryData
	
	PLAYER_INDEX avengerDriverPlayer
	PLAYER_INDEX playerCasinoCelebMocap	//Player who we are following into the casino celeb mocap
	
	BOOL bShouldCheckForCustomDefunctBaseCutscene = FALSE
	BOOL bPlayingCustomDefunctBaseCutscene = FALSE 
	
	INT iBodyScanStagger // Used in defunct base to trigger body scanner alarm system.

	
	BOOL bToggleFrostedGlass = FALSE
	BOOL bQuartersFrostedGlass = FALSE
	
	BOOL bHoldSkycam = FALSE
	SIMPLE_INTERIORS eSimpleInteriorPlayerIsReentering = SIMPLE_INTERIOR_INVALID
	
	
	//Special INT for entering in a vehicle to ensure the floor index matches the owner
	INT iFloorNumberForPropertyVehEntry = -1
	SIMPLE_INTERIORS eInteriorFloorNumberForPropertyVehEntry
	SIMPLE_INTERIORS eInteriorUsingExteriorExternalShot
	
	// Variables used to keep track of complex entrance menus
	INT iMenuStackTop 
	INT iMenuStack[ciINTERIOR_SUBMENU_MAX_DEPTH]
	INT iStoredGamerHandleHashForMenu[32]
	
	BOOL bNightclubFrostedGlass = FALSE
	BOOL g_bKillElevatorCutscene = FALSE
	
	#IF IS_DEBUG_BUILD
	BOOL bJoinedHeistWithUnknownInterior = FALSE
	BOOL bSkipFloorCleanupOnChildScriptCleanup = FALSE
	#ENDIF
	
	SIMPLE_INTERIORS iSimpleInteriorInTerminateProcess = SIMPLE_INTERIOR_INVALID	
	
	SIMPLE_INTERIORS eSwitchToSecondaryInterior = SIMPLE_INTERIOR_INVALID
	PLAYER_INDEX pSwitchToSecondaryInteriorOwner
	#IF IS_DEBUG_BUILD
	BOOL bTempEnableAllInteriorsToReload
	#ENDIF
	
	SIMPLE_INTERIOR_BUZZER_REQUEST_DATA eBuzzerData
	SIMPLE_INTERIOR_BUZZER_REPLY_DATA	eBuzzerReplyData[NUM_NETWORK_PLAYERS]
	
	BOOL bHideExitMenuThisFrame //Hide exit menu this frame
	BOOL bSkipFadeIn = FALSE
	
	INT iToggleFrostedGlassBitset[2]
	
	SIMPLE_INTERIORS eSimpleInteriorIDToSpawnInDrunk = SIMPLE_INTERIOR_INVALID
	
	SIMPLE_INTERIORS eIntFromWhichSeperateIntSelected 		= SIMPLE_INTERIOR_INVALID
	SIMPLE_INTERIORS eIntToEnterFromSeperateInteriorMenu 	= SIMPLE_INTERIOR_INVALID
	SIMPLE_INTERIORS eInteriorToTransitionToViaElevator 	= SIMPLE_INTERIOR_INVALID
	SIMPLE_INTERIORS eInteriorToPauseRP 					= SIMPLE_INTERIOR_INVALID //Which interior pasued the render phase
	SIMPLE_INTERIORS eInteriorToPauseInvites 				= SIMPLE_INTERIOR_INVALID //Which interior pasued the network invites being delivered to script
	SIMPLE_INTERIORS eInteriorToPauseBlockedEntryHelp		= SIMPLE_INTERIOR_INVALID
	SIMPLE_INTERIORS eInteriorsWithHelipad[NUM_SIMPLE_INTERIORS_WITH_HELIPAD]
	
	SIMPLE_INTERIOR_LIMO_SERVICE_EXIT eLimoService
	SIMPLE_INTERIOR_VALET_EXIT eValetExit
	
	SCRIPT_TIMER stKickDelay
	SCRIPT_TIMER stAvengerPassengerExitDelay
	
	SIMPLE_INTERIOR_TYPE eIntTypeMissionBounsBlock[MAX_BLOCKED_SIMPLE_INTERIOR_TYPES]
	INT iNumInteriorsBlocked = 0
	
	SMPL_FUNC_RETURN_VAL_CACHE sReturnValCache
	
	SIMPLE_INTERIOR_EXT_ENTRY_SCENE_TRIGGER sExternalEntrySceneTrigger
	
	SIMPLE_INTERIORS eExtScriptsTerminating[NUM_TERMINATING_SIMPLE_EXT_SCRIPTS]
	FLOAT fArcadeModelPreloadRange = 25.0//VDIST2
	INT iNumArcadeModelsToPreloadPerFrame = 1
	INT iArcadeModelPreloadIndex
	INT iSubmarineHelmSafeTimer = 120000
	INT iChildScriptLoadSceneMaxTime = 20000
	
	#IF FEATURE_DLC_2_2022
	SIMPLE_INTERIOR_CUSTOMIZATION_PREVIEW_DATA sCustomizationPreview	
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 tlIntPrint
	#ENDIF
ENDSTRUCT

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    FUNCTIONS    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

FUNC SIMPLE_INTERIOR_TYPE GET_SIMPLE_INTERIOR_TYPE(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID
		CASE SIMPLE_INTERIOR_WAREHOUSE_1
		CASE SIMPLE_INTERIOR_WAREHOUSE_2
		CASE SIMPLE_INTERIOR_WAREHOUSE_3
		CASE SIMPLE_INTERIOR_WAREHOUSE_4
		CASE SIMPLE_INTERIOR_WAREHOUSE_5
		CASE SIMPLE_INTERIOR_WAREHOUSE_6
		CASE SIMPLE_INTERIOR_WAREHOUSE_7
		CASE SIMPLE_INTERIOR_WAREHOUSE_8
		CASE SIMPLE_INTERIOR_WAREHOUSE_9
		CASE SIMPLE_INTERIOR_WAREHOUSE_10
		CASE SIMPLE_INTERIOR_WAREHOUSE_11
		CASE SIMPLE_INTERIOR_WAREHOUSE_12
		CASE SIMPLE_INTERIOR_WAREHOUSE_13
		CASE SIMPLE_INTERIOR_WAREHOUSE_14
		CASE SIMPLE_INTERIOR_WAREHOUSE_15
		CASE SIMPLE_INTERIOR_WAREHOUSE_16
		CASE SIMPLE_INTERIOR_WAREHOUSE_17
		CASE SIMPLE_INTERIOR_WAREHOUSE_18
		CASE SIMPLE_INTERIOR_WAREHOUSE_19
		CASE SIMPLE_INTERIOR_WAREHOUSE_20
		CASE SIMPLE_INTERIOR_WAREHOUSE_21
		CASE SIMPLE_INTERIOR_WAREHOUSE_22
			RETURN SIMPLE_INTERIOR_TYPE_WAREHOUSE
		BREAK

		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_1
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_2
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_3
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_4
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_5
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_6
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_7
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_8
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_9
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_10
			RETURN SIMPLE_INTERIOR_TYPE_IE_GARAGE
		BREAK
		
		CASE SIMPLE_INTERIOR_FACTORY_METH_1
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1
		CASE SIMPLE_INTERIOR_FACTORY_METH_2
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2
		CASE SIMPLE_INTERIOR_FACTORY_METH_3
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3
		CASE SIMPLE_INTERIOR_FACTORY_METH_4
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4
			RETURN SIMPLE_INTERIOR_TYPE_FACTORY
		BREAK
		
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MC_CLUBHOUSE
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_POLICE_STATION
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_ROCKFORD
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_PILLBOX
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_ALTA
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_BURTON
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_PALETO
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_GRAND_SENORA
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_CHUMASH
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_ROCKCLUB
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_CHICKEN_FACTORY
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_CHICKEN_FACTORY_PART_2
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_CHICKEN_FACTORY_PART_3
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_CHICKEN_FACTORY_PART_4
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_FARMHOUSE
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_HEIST_YACHT
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_RECYCLING_PLANT
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BIOLAB
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MEDIUM_GARAGE
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_LOWEND_STUDIO
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MIDEND_APARTMENT
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_SHERIFF	
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_SHERIFF2	
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_UNION_DEPOSITORY_CARPARK
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_SIMEON_SHOWROOM
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_LIFE_INVADER
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_ABATTOIR
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_JEWEL_STORE
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_DJ_YACHT
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MELANOMA_GARAGE
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_HAYES_AUTOS
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_METH_LAB
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_FIB_BUILDING
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BIOLAB_AND_TUNNEL
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_FOUNDRY
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MAX_RENDA
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_2
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_3
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_4
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_5
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_6
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_OMEGA
			RETURN SIMPLE_INTERIOR_TYPE_RESTRICTED_INTERIOR
		BREAK
		
		CASE SIMPLE_INTERIOR_BUNKER_1
		CASE SIMPLE_INTERIOR_BUNKER_2
		CASE SIMPLE_INTERIOR_BUNKER_3
		CASE SIMPLE_INTERIOR_BUNKER_4
		CASE SIMPLE_INTERIOR_BUNKER_5
		CASE SIMPLE_INTERIOR_BUNKER_6
		CASE SIMPLE_INTERIOR_BUNKER_7
		CASE SIMPLE_INTERIOR_BUNKER_9
		CASE SIMPLE_INTERIOR_BUNKER_10
		CASE SIMPLE_INTERIOR_BUNKER_11
		CASE SIMPLE_INTERIOR_BUNKER_12
			RETURN SIMPLE_INTERIOR_TYPE_BUNKER
		BREAK
		CASE SIMPLE_INTERIOR_ARMORY_TRUCK_1
			RETURN SIMPLE_INTERIOR_TYPE_ARMORY_TRUCK
		BREAK
		CASE SIMPLE_INTERIOR_CREATOR_TRAILER_1
			RETURN SIMPLE_INTERIOR_TYPE_CREATOR_TRAILER
		BREAK
		
		CASE SIMPLE_INTERIOR_HANGAR_1
		CASE SIMPLE_INTERIOR_HANGAR_2
		CASE SIMPLE_INTERIOR_HANGAR_3
		CASE SIMPLE_INTERIOR_HANGAR_4
		CASE SIMPLE_INTERIOR_HANGAR_5
			RETURN SIMPLE_INTERIOR_TYPE_HANGAR
		BREAK
		
		CASE SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1
			RETURN SIMPLE_INTERIOR_TYPE_ARMORY_AIRCRAFT
		BREAK
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_1
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_2
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_3
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_4
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_6
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_7
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_8
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_9
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_10
			RETURN SIMPLE_INTERIOR_TYPE_DEFUNCT_BASE
		BREAK
		CASE SIMPLE_INTERIOR_CREATOR_AIRCRAFT_1
			RETURN SIMPLE_INTERIOR_TYPE_CREATOR_AIRCRAFT
		BREAK	
		
		CASE SIMPLE_INTERIOR_HUB_LA_MESA
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS
			RETURN SIMPLE_INTERIOR_TYPE_BUSINESS_HUB
		BREAK
		CASE SIMPLE_INTERIOR_HACKER_TRUCK
			RETURN SIMPLE_INTERIOR_TYPE_HACKER_TRUCK
		BREAK
		
		CASE SIMPLE_INTERIOR_ARENA_GARAGE_1
			RETURN SIMPLE_INTERIOR_TYPE_ARENA_GARAGE
		BREAK
				
		CASE SIMPLE_INTERIOR_CASINO
			RETURN SIMPLE_INTERIOR_TYPE_CASINO
		BREAK
		CASE SIMPLE_INTERIOR_CASINO_APT
			RETURN SIMPLE_INTERIOR_TYPE_CASINO_APARTMENT
		BREAK
		CASE SIMPLE_INTERIOR_CASINO_VAL_GARAGE
			RETURN SIMPLE_INTERIOR_TYPE_CASINO_VALET_GARAGE
		BREAK		
		
		CASE SIMPLE_INTERIOR_ARCADE_PALETO_BAY
		CASE SIMPLE_INTERIOR_ARCADE_GRAPESEED
		CASE SIMPLE_INTERIOR_ARCADE_DAVIS
		CASE SIMPLE_INTERIOR_ARCADE_WEST_VINEWOOD
		CASE SIMPLE_INTERIOR_ARCADE_ROCKFORD_HILLS
		CASE SIMPLE_INTERIOR_ARCADE_LA_MESA
			RETURN SIMPLE_INTERIOR_TYPE_ARCADE
		BREAK
		
		CASE SIMPLE_INTERIOR_SOLOMONS_OFFICE
			RETURN SIMPLE_INTERIOR_TYPE_SOLOMON_OFFICE
		BREAK
		
		CASE SIMPLE_INTERIOR_CASINO_NIGHTCLUB
			RETURN SIMPLE_INTERIOR_TYPE_CASINO_NIGHTCLUB
		BREAK
		
		CASE SIMPLE_INTERIOR_SUBMARINE
			RETURN SIMPLE_INTERIOR_TYPE_SUBMARINE
		BREAK
		
		CASE SIMPLE_INTERIOR_MUSIC_STUDIO
			RETURN SIMPLE_INTERIOR_TYPE_MUSIC_STUDIO
		BREAK	
		
		CASE SIMPLE_INTERIOR_AUTO_SHOP_LA_MESA
		CASE SIMPLE_INTERIOR_AUTO_SHOP_BURTON
		CASE SIMPLE_INTERIOR_AUTO_SHOP_MISSION_ROW
		CASE SIMPLE_INTERIOR_AUTO_SHOP_RANCHO
		CASE SIMPLE_INTERIOR_AUTO_SHOP_STRAWBERRY
			RETURN SIMPLE_INTERIOR_TYPE_AUTO_SHOP
		BREAK
		CASE SIMPLE_INTERIOR_CAR_MEET
			RETURN SIMPLE_INTERIOR_TYPE_CAR_MEET
		BREAK
		
		CASE SIMPLE_INTERIOR_FIXER_HQ_HAWICK
		CASE SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD
		CASE SIMPLE_INTERIOR_FIXER_HQ_SEOUL
		CASE SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI
			RETURN SIMPLE_INTERIOR_TYPE_FIXER_HQ
		BREAK

		#IF FEATURE_DLC_2_2022
		CASE SIMPLE_INTERIOR_ACID_LAB
			RETURN SIMPLE_INTERIOR_TYPE_ACID_LAB
		BREAK
		CASE SIMPLE_INTERIOR_JUGGALO_HIDEOUT
			RETURN SIMPLE_INTERIOR_TYPE_JUGGALO_HIDEOUT
		BREAK
		CASE SIMPLE_INTERIOR_MULTISTOREY_GARAGE
			RETURN SIMPLE_INTERIOR_TYPE_MULTISTOREY_GARAGE
		BREAK
		#ENDIF
	ENDSWITCH
	
	ASSERTLN(DEBUG_PROPERTY, " GET_SIMPLE_INTERIOR_TYPE - No simple interior type set for eSimpleInteriorID: ", eSimpleInteriorID)
	
	RETURN SIMPLE_INTERIOR_TYPE_INVALID
ENDFUNC

/// PURPOSE: Used for Telemetry (hashed value of strings) and debug prints (LOOT AT :  GET_PLAYER_LOCATION_NAME() AND GET_LOCATION_HASH_FOR_TELEMETRY())
///    /
/// PARAMS:
///    eSimpleInteriorID - Simple interior enum
/// RETURNS:  This returns the enum names as strings, these are then hashed and used for telemetry location data
FUNC STRING GET_SIMPLE_INTERIOR_NAME_FOR_TELEMETRY(SIMPLE_INTERIORS eSimpleInteriorID)
	SWITCH eSimpleInteriorID																 ///NAME												//HASH VALUE (FOR TELEMETRY)
		CASE SIMPLE_INTERIOR_WAREHOUSE_1										RETURN "SIMPLE_INTERIOR_WAREHOUSE_1"										//	1724876706					
		CASE SIMPLE_INTERIOR_WAREHOUSE_2										RETURN "SIMPLE_INTERIOR_WAREHOUSE_2"										//	1982735967					
		CASE SIMPLE_INTERIOR_WAREHOUSE_3										RETURN "SIMPLE_INTERIOR_WAREHOUSE_3"										//	-1000127800					
		CASE SIMPLE_INTERIOR_WAREHOUSE_4										RETURN "SIMPLE_INTERIOR_WAREHOUSE_4"										//	-1314120358
		CASE SIMPLE_INTERIOR_WAREHOUSE_5										RETURN "SIMPLE_INTERIOR_WAREHOUSE_5"										//	-1088178103	
		CASE SIMPLE_INTERIOR_WAREHOUSE_6										RETURN "SIMPLE_INTERIOR_WAREHOUSE_6"										//	-1434448122	
		CASE SIMPLE_INTERIOR_WAREHOUSE_7										RETURN "SIMPLE_INTERIOR_WAREHOUSE_7"										//	-1191597063
		CASE SIMPLE_INTERIOR_WAREHOUSE_8										RETURN "SIMPLE_INTERIOR_WAREHOUSE_8"										//	104089197
		CASE SIMPLE_INTERIOR_WAREHOUSE_9										RETURN "SIMPLE_INTERIOR_WAREHOUSE_9"										//	343466742	
		CASE SIMPLE_INTERIOR_WAREHOUSE_10										RETURN "SIMPLE_INTERIOR_WAREHOUSE_10"										//	-769698924
		CASE SIMPLE_INTERIOR_WAREHOUSE_11										RETURN "SIMPLE_INTERIOR_WAREHOUSE_11"										//	-966902766
		CASE SIMPLE_INTERIOR_WAREHOUSE_12										RETURN "SIMPLE_INTERIOR_WAREHOUSE_12"										//	-40752519
		CASE SIMPLE_INTERIOR_WAREHOUSE_13										RETURN "SIMPLE_INTERIOR_WAREHOUSE_13"										//	-473172243
		CASE SIMPLE_INTERIOR_WAREHOUSE_14										RETURN "SIMPLE_INTERIOR_WAREHOUSE_14"										//	2032542159
		CASE SIMPLE_INTERIOR_WAREHOUSE_15										RETURN "SIMPLE_INTERIOR_WAREHOUSE_15"										//	-1906095031
		CASE SIMPLE_INTERIOR_WAREHOUSE_16										RETURN "SIMPLE_INTERIOR_WAREHOUSE_16"										//	1437555426
		CASE SIMPLE_INTERIOR_WAREHOUSE_17										RETURN "SIMPLE_INTERIOR_WAREHOUSE_17"										//	1799194110
		CASE SIMPLE_INTERIOR_WAREHOUSE_18										RETURN "SIMPLE_INTERIOR_WAREHOUSE_18"										//	926948868
		CASE SIMPLE_INTERIOR_WAREHOUSE_19										RETURN "SIMPLE_INTERIOR_WAREHOUSE_19"										//	1167833787
		CASE SIMPLE_INTERIOR_WAREHOUSE_20										RETURN "SIMPLE_INTERIOR_WAREHOUSE_20"										//	-705800814
		CASE SIMPLE_INTERIOR_WAREHOUSE_21										RETURN "SIMPLE_INTERIOR_WAREHOUSE_21"										//	-1012354809
		CASE SIMPLE_INTERIOR_WAREHOUSE_22										RETURN "SIMPLE_INTERIOR_WAREHOUSE_22"										//	-1184260983

		CASE SIMPLE_INTERIOR_FACTORY_METH_1										RETURN "SIMPLE_INTERIOR_FACTORY_METH_1"										//	1667011453			
		CASE SIMPLE_INTERIOR_FACTORY_WEED_1										RETURN "SIMPLE_INTERIOR_FACTORY_WEED_1"										//	-1336367741			
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_1									RETURN "SIMPLE_INTERIOR_FACTORY_CRACK_1"									//	461690478	
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_1									RETURN "SIMPLE_INTERIOR_FACTORY_MONEY_1"									//	633602170		
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1								RETURN "SIMPLE_INTERIOR_FACTORY_DOCUMENTS_1"								//	382299359	

		CASE SIMPLE_INTERIOR_FACTORY_METH_2										RETURN "SIMPLE_INTERIOR_FACTORY_METH_2"										//	514984485		
		CASE SIMPLE_INTERIOR_FACTORY_WEED_2										RETURN "SIMPLE_INTERIOR_FACTORY_WEED_2"										//	2026616426			
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_2									RETURN "SIMPLE_INTERIOR_FACTORY_CRACK_2"									//	1937966701
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_2									RETURN "SIMPLE_INTERIOR_FACTORY_MONEY_2"									//	1483138495		
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2								RETURN "SIMPLE_INTERIOR_FACTORY_DOCUMENTS_2"								//	581469341	

		CASE SIMPLE_INTERIOR_FACTORY_METH_3										RETURN "SIMPLE_INTERIOR_FACTORY_METH_3"										//	1367863248		
		CASE SIMPLE_INTERIOR_FACTORY_WEED_3										RETURN "SIMPLE_INTERIOR_FACTORY_WEED_3"										//	-955854113			
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_3									RETURN "SIMPLE_INTERIOR_FACTORY_CRACK_3"									//	1645306762		
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_3									RETURN "SIMPLE_INTERIOR_FACTORY_MONEY_3"									//	1245301093		
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3								RETURN "SIMPLE_INTERIOR_FACTORY_DOCUMENTS_3"								//	1021557011	
		
		CASE SIMPLE_INTERIOR_FACTORY_METH_4										RETURN "SIMPLE_INTERIOR_FACTORY_METH_4"										//	-82296078			
		CASE SIMPLE_INTERIOR_FACTORY_WEED_4										RETURN "SIMPLE_INTERIOR_FACTORY_WEED_4"										//	1025163021			
		CASE SIMPLE_INTERIOR_FACTORY_CRACK_4									RETURN "SIMPLE_INTERIOR_FACTORY_CRACK_4"									//	1356841255	
		CASE SIMPLE_INTERIOR_FACTORY_MONEY_4									RETURN "SIMPLE_INTERIOR_FACTORY_MONEY_4"									//	-290713001
		CASE SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4								RETURN "SIMPLE_INTERIOR_FACTORY_DOCUMENTS_4"								//	1327259012
		
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_POLICE_STATION					RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_POLICE_STATION"					//	-581129697
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MC_CLUBHOUSE					RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MC_CLUBHOUSE"					//	282957256
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_ROCKFORD					RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_ROCKFORD"					//	1829228597
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_PILLBOX					RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_PILLBOX"					//	-873601614
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_ALTA						RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_ALTA"						//	-1409539033
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_BURTON					RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_BURTON"					//	1107449663
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_PALETO					RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_PALETO"					//	-578575258
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_GRAND_SENORA				RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_GRAND_SENORA"				//	-1615949921
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_CHUMASH					RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BANK_CHUMASH"					//	1438364678
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_ROCKCLUB						RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_ROCKCLUB"						//	1894496992
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_CHICKEN_FACTORY				RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_CHICKEN_FACTORY"				//	1779427560
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_CHICKEN_FACTORY_PART_2			RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_CHICKEN_FACTORY_PART_2"			//	-1039460533
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_CHICKEN_FACTORY_PART_3			RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_CHICKEN_FACTORY_PART_3"			//	-195724321
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_CHICKEN_FACTORY_PART_4			RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_CHICKEN_FACTORY_PART_4"			//	500911854
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_FARMHOUSE						RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_FARMHOUSE"						//	811797203
		
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_HEIST_YACHT					RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_HEIST_YACHT"					//	-1666058893
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_RECYCLING_PLANT				RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_RECYCLING_PLANT"				//	-1254748295
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BIOLAB							RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BIOLAB"							//	-74418006

		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_1										RETURN "SIMPLE_INTERIOR_IE_WAREHOUSE_1"										//	-1288625515	// La Puerta
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_2										RETURN "SIMPLE_INTERIOR_IE_WAREHOUSE_2"										//	-563447545	// La Mesa
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_3										RETURN "SIMPLE_INTERIOR_IE_WAREHOUSE_3"										//	-857909779	// Davis
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_4										RETURN "SIMPLE_INTERIOR_IE_WAREHOUSE_4"										//	-100978648	// Strawberry
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_5										RETURN "SIMPLE_INTERIOR_IE_WAREHOUSE_5"										//	-399963004	// Murrieta Heights
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_6										RETURN "SIMPLE_INTERIOR_IE_WAREHOUSE_6"										//	393210641	// Cypress Flats
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_7										RETURN "SIMPLE_INTERIOR_IE_WAREHOUSE_7"										//	95995811	// El Burro Heights
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_8										RETURN "SIMPLE_INTERIOR_IE_WAREHOUSE_8"										//	280616361	// Elysian Island
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_9										RETURN "SIMPLE_INTERIOR_IE_WAREHOUSE_9"										//	1095548622	// LSIA
		CASE SIMPLE_INTERIOR_IE_WAREHOUSE_10									RETURN "SIMPLE_INTERIOR_IE_WAREHOUSE_10"									//	1346361163	// LSIA
		
		CASE SIMPLE_INTERIOR_BUNKER_1											RETURN "SIMPLE_INTERIOR_BUNKER_1"											//	-993934258
		CASE SIMPLE_INTERIOR_BUNKER_2											RETURN "SIMPLE_INTERIOR_BUNKER_2"											//	210686951
		CASE SIMPLE_INTERIOR_BUNKER_3											RETURN "SIMPLE_INTERIOR_BUNKER_3"											//	-1438314667
		CASE SIMPLE_INTERIOR_BUNKER_4											RETURN "SIMPLE_INTERIOR_BUNKER_4"											//	-324692967
		CASE SIMPLE_INTERIOR_BUNKER_5											RETURN "SIMPLE_INTERIOR_BUNKER_5"											//	-10241643
		CASE SIMPLE_INTERIOR_BUNKER_6											RETURN "SIMPLE_INTERIOR_BUNKER_6"											//	-1170821316
		CASE SIMPLE_INTERIOR_BUNKER_7											RETURN "SIMPLE_INTERIOR_BUNKER_7"											//	450916494
		CASE SIMPLE_INTERIOR_BUNKER_9											RETURN "SIMPLE_INTERIOR_BUNKER_9"											//	936323691
		CASE SIMPLE_INTERIOR_BUNKER_10											RETURN "SIMPLE_INTERIOR_BUNKER_10"											//	2057381171
		CASE SIMPLE_INTERIOR_BUNKER_11											RETURN "SIMPLE_INTERIOR_BUNKER_11"											//	1759936958
		CASE SIMPLE_INTERIOR_BUNKER_12											RETURN "SIMPLE_INTERIOR_BUNKER_12"											//	-1640240024
		
		CASE SIMPLE_INTERIOR_ARMORY_TRUCK_1										RETURN "SIMPLE_INTERIOR_ARMORY_TRUCK_1"										//	-1511801648	// MOC
		CASE SIMPLE_INTERIOR_CREATOR_TRAILER_1									RETURN "SIMPLE_INTERIOR_CREATOR_TRAILER_1"									//	-655658513	// MOC - Creator Mission
			
		CASE SIMPLE_INTERIOR_HANGAR_1											RETURN "SIMPLE_INTERIOR_HANGAR_1"											//	795256712	// LSIA Hangar 1
		CASE SIMPLE_INTERIOR_HANGAR_2											RETURN "SIMPLE_INTERIOR_HANGAR_2"											//	480641543	// LSIA Hangar A17
		CASE SIMPLE_INTERIOR_HANGAR_3											RETURN "SIMPLE_INTERIOR_HANGAR_3"											//	-1946001214	// Fort Zancudo Hangar A2
		CASE SIMPLE_INTERIOR_HANGAR_4											RETURN "SIMPLE_INTERIOR_HANGAR_4"											//	-2128262392	// Fort Zancudo Hangar 3497
		CASE SIMPLE_INTERIOR_HANGAR_5											RETURN "SIMPLE_INTERIOR_HANGAR_5"											//	-1871844971	// Fort Zancudo Hangar 3499
		
		CASE SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1									RETURN "SIMPLE_INTERIOR_ARMORY_AIRCRAFT_1"									//	836640245	// AOC
		
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_1										RETURN "SIMPLE_INTERIOR_DEFUNCT_BASE_1"										//	-486180020
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_2										RETURN "SIMPLE_INTERIOR_DEFUNCT_BASE_2"										//	-715989017
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_3										RETURN "SIMPLE_INTERIOR_DEFUNCT_BASE_3"										//	2125083299
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_4										RETURN "SIMPLE_INTERIOR_DEFUNCT_BASE_4"										//	1887344204
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_6										RETURN "SIMPLE_INTERIOR_DEFUNCT_BASE_6"										//	900341924
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_7										RETURN "SIMPLE_INTERIOR_DEFUNCT_BASE_7"										//	855448394
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_8										RETURN "SIMPLE_INTERIOR_DEFUNCT_BASE_8"										//	1700200445
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_9										RETURN "SIMPLE_INTERIOR_DEFUNCT_BASE_9"										//	1415601680
		CASE SIMPLE_INTERIOR_DEFUNCT_BASE_10									RETURN "SIMPLE_INTERIOR_DEFUNCT_BASE_10"									//	-1514610710
		
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MEDIUM_GARAGE					RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MEDIUM_GARAGE"					//	-865660159	
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_LOWEND_STUDIO					RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_LOWEND_STUDIO"					//	-710818203
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MIDEND_APARTMENT				RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MIDEND_APARTMENT"				//	-324709170
		
		CASE SIMPLE_INTERIOR_CREATOR_AIRCRAFT_1									RETURN "SIMPLE_INTERIOR_CREATOR_AIRCRAFT_1"									//	1949842877
		
		CASE SIMPLE_INTERIOR_HUB_LA_MESA										RETURN "SIMPLE_INTERIOR_HUB_LA_MESA"										// 	111057774
		CASE SIMPLE_INTERIOR_HUB_MISSION_ROW									RETURN "SIMPLE_INTERIOR_HUB_MISSION_ROW"									//	1198579449
		CASE SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE							RETURN "SIMPLE_INTERIOR_HUB_STRAWBERRY_WAREHOUSE"							//	74679616
		CASE SIMPLE_INTERIOR_HUB_WEST_VINEWOOD									RETURN "SIMPLE_INTERIOR_HUB_WEST_VINEWOOD"									//	-1559756325
		CASE SIMPLE_INTERIOR_HUB_CYPRESS_FLATS									RETURN "SIMPLE_INTERIOR_HUB_CYPRESS_FLATS"									//	-1872962088
		CASE SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE									RETURN "SIMPLE_INTERIOR_HUB_LSIA_WAREHOUSE"									//	847468838
		CASE SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND									RETURN "SIMPLE_INTERIOR_HUB_ELYSIAN_ISLAND"									//	1336326193
		CASE SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD								RETURN "SIMPLE_INTERIOR_HUB_DOWNTOWN_VINEWOOD"								//	2113184722
		CASE SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING								RETURN "SIMPLE_INTERIOR_HUB_DEL_PERRO_BUILDING"								//	-1608416417
		CASE SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS								RETURN "SIMPLE_INTERIOR_HUB_VESPUCCI_CANALS"								//	-1509790000
		
		
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_SHERIFF						RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_SHERIFF"						// 	-1838126805
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_SHERIFF2						RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_SHERIFF2"						// 	-326940911
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_UNION_DEPOSITORY_CARPARK		RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_UNION_DEPOSITORY_CARPARK"		//  -1495094806
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_SIMEON_SHOWROOM				RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_SIMEON_SHOWROOM"				// 	-1310879307
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_ABATTOIR						RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_ABATTOIR"						// 	-1775580872
		
		CASE SIMPLE_INTERIOR_HACKER_TRUCK										RETURN "SIMPLE_INTERIOR_HACKER_TRUCK"										// 	2142816302
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_JEWEL_STORE					RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_JEWEL_STORE"					// 	407841190

		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_LIFE_INVADER					RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_LIFE_INVADER"					// 	-1174108047
		
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_DJ_YACHT						RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_DJ_YACHT"						//	-56828153
		
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MELANOMA_GARAGE				RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MELANOMA_GARAGE"				//	1049670410
		
		CASE SIMPLE_INTERIOR_ARENA_GARAGE_1										RETURN "SIMPLE_INTERIOR_ARENA_GARAGE_1"										// -1567932215
			
		CASE SIMPLE_INTERIOR_CASINO												RETURN "SIMPLE_INTERIOR_CASINO"												// -84733279
		CASE SIMPLE_INTERIOR_CASINO_APT											RETURN "SIMPLE_INTERIOR_CASINO_APT"											// -2078011960
		CASE SIMPLE_INTERIOR_CASINO_VAL_GARAGE									RETURN "SIMPLE_INTERIOR_CASINO_VAL_GARAGE"									// 587814291
		
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_HAYES_AUTOS					RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_HAYES_AUTOS"					// -1707895473
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_METH_LAB						RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_METH_LAB"						// 	1044377337
		
		CASE SIMPLE_INTERIOR_ARCADE_PALETO_BAY									RETURN "SIMPLE_INTERIOR_ARCADE_PALETO_BAY"									// -10997558
		CASE SIMPLE_INTERIOR_ARCADE_GRAPESEED									RETURN "SIMPLE_INTERIOR_ARCADE_GRAPESEED"									// -1738076238
		CASE SIMPLE_INTERIOR_ARCADE_DAVIS										RETURN "SIMPLE_INTERIOR_ARCADE_DAVIS"										// 1384669232
		CASE SIMPLE_INTERIOR_ARCADE_WEST_VINEWOOD								RETURN "SIMPLE_INTERIOR_ARCADE_WEST_VINEWOOD"								// -272691340
		CASE SIMPLE_INTERIOR_ARCADE_ROCKFORD_HILLS								RETURN "SIMPLE_INTERIOR_ARCADE_ROCKFORD_HILLS"								// 1669090008
		CASE SIMPLE_INTERIOR_ARCADE_LA_MESA										RETURN "SIMPLE_INTERIOR_ARCADE_LA_MESA"										// 1070552029
		
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_FIB_BUILDING					RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_FIB_BUILDING"					// -479073127
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BIOLAB_AND_TUNNEL				RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_BIOLAB_AND_TUNNEL"				//	625933566
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_FOUNDRY						RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_FOUNDRY"						// -1566014872
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MAX_RENDA						RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_MAX_RENDA"						// -1529906855
		
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER				RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER"				// -12273405
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_2		RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_2"		// 676388544
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_3		RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_3"		// -107904702
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_4		RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_4"		// -1390811056
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_5		RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_5"		// -1621078819
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_6		RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_AIRCRAFT_CARRIER_PART_6"		// -911367813
		
		CASE SIMPLE_INTERIOR_RESTRICTED_INTERIOR_OMEGA							RETURN "SIMPLE_INTERIOR_RESTRICTED_INTERIOR_OMEGA"							//	-1326444720
		CASE SIMPLE_INTERIOR_SOLOMONS_OFFICE									RETURN "SIMPLE_INTERIOR_SOLOMONS_OFFICE"									//	-129753564
		CASE SIMPLE_INTERIOR_CASINO_NIGHTCLUB									RETURN "SIMPLE_INTERIOR_CASINO_NIGHTCLUB"									//	-765042571
		CASE SIMPLE_INTERIOR_SUBMARINE											RETURN "SIMPLE_INTERIOR_SUBMARINE"											//	94523690

		#IF FEATURE_MUSIC_STUDIO
		CASE SIMPLE_INTERIOR_MUSIC_STUDIO										RETURN "SIMPLE_INTERIOR_MUSIC_STUDIO"										// 1588517932
		#ENDIF
		
		CASE SIMPLE_INTERIOR_AUTO_SHOP_LA_MESA									RETURN "SIMPLE_INTERIOR_AUTO_SHOP_LA_MESA"									// -1541257485
		CASE SIMPLE_INTERIOR_AUTO_SHOP_STRAWBERRY								RETURN "SIMPLE_INTERIOR_AUTO_SHOP_STRAWBERRY"								// -1094638582
		CASE SIMPLE_INTERIOR_AUTO_SHOP_BURTON									RETURN "SIMPLE_INTERIOR_AUTO_SHOP_BURTON"									// -1302161538
		CASE SIMPLE_INTERIOR_AUTO_SHOP_RANCHO									RETURN "SIMPLE_INTERIOR_AUTO_SHOP_RANCHO"									// 1908505157
		CASE SIMPLE_INTERIOR_AUTO_SHOP_MISSION_ROW								RETURN "SIMPLE_INTERIOR_AUTO_SHOP_MISSION_ROW"								// 1162271008
		CASE SIMPLE_INTERIOR_CAR_MEET											RETURN "SIMPLE_INTERIOR_CAR_MEET"											// -379629017
		
		#IF FEATURE_FIXER
		CASE SIMPLE_INTERIOR_FIXER_HQ_HAWICK									RETURN "SIMPLE_INTERIOR_FIXER_HQ_HAWICK"									// -1829134495
		CASE SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD									RETURN "SIMPLE_INTERIOR_FIXER_HQ_ROCKFORD"									// 1514855350
		CASE SIMPLE_INTERIOR_FIXER_HQ_SEOUL										RETURN "SIMPLE_INTERIOR_FIXER_HQ_SEOUL"										// 546654847
		CASE SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI									RETURN "SIMPLE_INTERIOR_FIXER_HQ_VESPUCCI"									// -962539115
		#ENDIF
	ENDSWITCH
	
	RETURN "SIMPLE_INTERIOR_INVALID"  		//	-1769110197
ENDFUNC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡    INITIALISE   ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

PROC Initialise_Global_Interior_Instances()

	#IF IS_DEBUG_BUILD			
	IF NOT (GET_INDEX_OF_CURRENT_LEVEL() = LEVEL_NET_TEST)
	#ENDIF
		g_Interior_Instance_Strip_Club_Low = GET_INTERIOR_AT_COORDS(<<114.64, -1290.34, 29.68>>)
		
		#IF IS_DEBUG_BUILD
		IF NOT IS_VALID_INTERIOR(g_Interior_Instance_Strip_Club_Low)
			SCRIPT_ASSERT("Initialise_Global_Interior_Instances() - could not get valid interior for g_Interior_Instance_Strip_Club_Low")
		ENDIF
		#ENDIF	
	
	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF
ENDPROC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡     DEBUG     ╞═══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

#IF IS_DEBUG_BUILD
FUNC STRING GET_ASYNC_GRID_SPAWN_LOCATION_NAME(ASYNC_GRID_SPAWN_LOCATIONS location)
	SWITCH location
		CASE _DBG_GRID_SPAWN_LOCATION_CREATOR 				RETURN "_DBG_GRID_SPAWN_LOCATION_CREATOR"
		
		CASE GRID_SPAWN_LOCATION_FACTORY_METH_1 			RETURN "GRID_SPAWN_LOCATION_FACTORY_METH_1"
		CASE GRID_SPAWN_LOCATION_FACTORY_CRACK_1			RETURN "GRID_SPAWN_LOCATION_FACTORY_CRACK_1"
		CASE GRID_SPAWN_LOCATION_FACTORY_WEED_1 			RETURN "GRID_SPAWN_LOCATION_FACTORY_WEED_1"
		CASE GRID_SPAWN_LOCATION_FACTORY_FAKEID_1 			RETURN "GRID_SPAWN_LOCATION_FACTORY_FAKEID_1"
		CASE GRID_SPAWN_LOCATION_FACTORY_CASH_1 			RETURN "GRID_SPAWN_LOCATION_FACTORY_CASH_1"
		
		CASE GRID_SPAWN_LOCATION_FACTORY_METH_2 			RETURN "GRID_SPAWN_LOCATION_FACTORY_METH_2"
		CASE GRID_SPAWN_LOCATION_FACTORY_CRACK_2 			RETURN "GRID_SPAWN_LOCATION_FACTORY_CRACK_2"
		CASE GRID_SPAWN_LOCATION_FACTORY_WEED_2 			RETURN "GRID_SPAWN_LOCATION_FACTORY_WEED_2"
		CASE GRID_SPAWN_LOCATION_FACTORY_FAKEID_2 			RETURN "GRID_SPAWN_LOCATION_FACTORY_FAKEID_2"
		CASE GRID_SPAWN_LOCATION_FACTORY_CASH_2 			RETURN "GRID_SPAWN_LOCATION_FACTORY_CASH_2"
		
		CASE GRID_SPAWN_LOCATION_FACTORY_METH_3 			RETURN "GRID_SPAWN_LOCATION_FACTORY_METH_3"
		CASE GRID_SPAWN_LOCATION_FACTORY_CRACK_3 			RETURN "GRID_SPAWN_LOCATION_FACTORY_CRACK_3"
		CASE GRID_SPAWN_LOCATION_FACTORY_WEED_3 			RETURN "GRID_SPAWN_LOCATION_FACTORY_WEED_3"
		CASE GRID_SPAWN_LOCATION_FACTORY_FAKEID_3 			RETURN "GRID_SPAWN_LOCATION_FACTORY_FAKEID_3"
		CASE GRID_SPAWN_LOCATION_FACTORY_CASH_3 			RETURN "GRID_SPAWN_LOCATION_FACTORY_CASH_3"
		
		CASE GRID_SPAWN_LOCATION_FACTORY_METH_4 			RETURN "GRID_SPAWN_LOCATION_FACTORY_METH_4"
		CASE GRID_SPAWN_LOCATION_FACTORY_CRACK_4 			RETURN "GRID_SPAWN_LOCATION_FACTORY_CRACK_4"
		CASE GRID_SPAWN_LOCATION_FACTORY_WEED_4 			RETURN "GRID_SPAWN_LOCATION_FACTORY_WEED_4"
		CASE GRID_SPAWN_LOCATION_FACTORY_FAKEID_4 			RETURN "GRID_SPAWN_LOCATION_FACTORY_FAKEID_4"
		CASE GRID_SPAWN_LOCATION_FACTORY_CASH_4 			RETURN "GRID_SPAWN_LOCATION_FACTORY_CASH_4"
		
		CASE GRID_SPAWN_LOCATION_FACTORY_CRACK_INSIDE 		RETURN "GRID_SPAWN_LOCATION_FACTORY_CRACK_INSIDE"
		CASE GRID_SPAWN_LOCATION_FACTORY_METH_INSIDE 		RETURN "GRID_SPAWN_LOCATION_FACTORY_METH_INSIDE"
		CASE GRID_SPAWN_LOCATION_FACTORY_WEED_INSIDE 		RETURN "GRID_SPAWN_LOCATION_FACTORY_WEED_INSIDE"
		CASE GRID_SPAWN_LOCATION_FACTORY_FAKEID_INSIDE 		RETURN "GRID_SPAWN_LOCATION_FACTORY_FAKEID_INSIDE"
		CASE GRID_SPAWN_LOCATION_FACTORY_CASH_INSIDE 		RETURN "GRID_SPAWN_LOCATION_FACTORY_CASH_INSIDE"
		
		CASE GRID_SPAWN_LOCATION_IE_WAREHOUSE_INSIDE 		RETURN "GRID_SPAWN_LOCATION_WAREHOUSE_INSIDE"
	ENDSWITCH
	
	RETURN "*** INVALID ***"
ENDFUNC
#ENDIF
