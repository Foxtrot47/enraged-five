/// Online Technical
/// Manages the frosted glass in the player Facility
///    Now the loading/cleanup of the glass objects is handled by the script using this header because
///    there were many different ways the glass was grabbed/created each
///    with their own issues. Really, this head just keeps a global bool synced and toggles visibility on some objects. 

USING "globals.sch"
USING "net_events.sch"
#IF IS_DEBUG_BUILD
USING "net_simple_interior_common.sch"
#ENDIF

TWEAK_INT GLASS_ALPHA_MAX			254

#IF IS_DEBUG_BUILD
PROC FROSTED_GLASS_INIT_WIDGETS()
	START_WIDGET_GROUP("Frosted Glass")
		ADD_WIDGET_INT_SLIDER("Alpha max", GLASS_ALPHA_MAX, 0, 255, 1)
	STOP_WIDGET_GROUP()
ENDPROC
#ENDIF

STRUCT FROSTED_GLASS_DATA
	FLOAT 	fRemainder
	BOOL 	bInTransition
ENDSTRUCT

CONST_FLOAT SAFE_DOOR_STREAM_DISTANCE 79.0

ENUM FROSTED_GLASS_GLASS_TYPE
	FG_GLASS_TYPE_NORMAL,
	FG_GLASS_TYPE_FROSTED
ENDENUM

#IF IS_DEBUG_BUILD
FUNC STRING FROSTED_GLASS_GLASS_TYPE_STRING(FROSTED_GLASS_GLASS_TYPE eValue)
	SWITCH eValue
		CASE FG_GLASS_TYPE_NORMAL RETURN "FG_GLASS_TYPE_NORMAL"
		CASE FG_GLASS_TYPE_FROSTED RETURN "FG_GLASS_TYPE_FROSTED"
	ENDSWITCH
	
	RETURN "FROSTED_GLASS_GLASS_TYPE_STRING *** invalid ***"
ENDFUNC
#ENDIF

ENUM FROSTED_GLASS_ENTITY_TYPE
	FG_ENTITY_TYPE_DOOR,
	FG_ENTITY_TYPE_WINDOW,
	//Do not remove
	FG_ENTITY_TYPE_MAX
ENDENUM

#IF IS_DEBUG_BUILD
FUNC STRING FROSTED_GLASS_ENTITY_TYPE_STRING(FROSTED_GLASS_ENTITY_TYPE eValue)
	SWITCH eValue
		CASE FG_ENTITY_TYPE_DOOR RETURN "FG_ENTITY_TYPE_DOOR"
		CASE FG_ENTITY_TYPE_WINDOW RETURN "FG_ENTITY_TYPE_WINDOW"
	ENDSWITCH
	
	RETURN "FROSTED_GLASS_ENTITY_TYPE_STRING *** invalid ***"
ENDFUNC
#ENDIF

ENUM FROSTED_GLASS_BS_CLIENT
	 FG_SERVER_BS_FROSTED_GLASS_ON
ENDENUM

ENUM FROSTED_GLASS_BS_SERVER
	 FG_CLIENT_FROSTED_GLASS_ON
ENDENUM

STRUCT FROSTED_GLASS_SERVER_DATA
	INT iBS
ENDSTRUCT

STRUCT FROSTED_GLASS_CLIENT_DATA
	INT iBS
ENDSTRUCT

/// PURPOSE:
///  Needs to be called in a loop to check the client BD 
///  for each player. Can be staggered safely.
PROC FROSTED_GLASS_UPDATE_SERVER(FROSTED_GLASS_SERVER_DATA& server, INT &iUpdateBitset)
	IF iUpdateBitset != 0
		IF IS_BIT_SET(iUpdateBitset, 1)
			IF NOT IS_BIT_SET_ENUM(server.iBS, FG_SERVER_BS_FROSTED_GLASS_ON)
				SET_BIT_ENUM(server.iBS, FG_SERVER_BS_FROSTED_GLASS_ON)
				PRINTLN("[FROSTED_GLASS][SERVER] Turning on glass frost.")
			ENDIF
		ELIF IS_BIT_SET(iUpdateBitset, 2)
			IF IS_BIT_SET_ENUM(server.iBS, FG_SERVER_BS_FROSTED_GLASS_ON)
				CLEAR_BIT_ENUM(server.iBS, FG_SERVER_BS_FROSTED_GLASS_ON)
				PRINTLN("[FROSTED_GLASS][SERVER] Turning off glass frost.")
			ENDIF
		ENDIF
		iUpdateBitset = 0
	ENDIF
ENDPROC

ENUM FG_UPDATE_RESULT
	 FG_NO_CHANGE
	,FG_UPDATE_FROSTED_THIS_FRAME
	,FG_UPDATE_UNFROSTED_THIS_FRAME
ENDENUM


ENUM FG_UPDATE_SETTING_MODE
	 FG_UPDATE_SETTING_VISIBILITY
	,FG_UPDATE_SETTING_ALPHA
	,FG_UPDATE_CREATE_MODEL_HIDE
ENDENUM

/// PURPOSE:
///    Handles toggling the visibility of the glass and keeps data synced. Check return result to see if there's been a frosted state change. 
/// PARAMS:
///    bLocalGlassOn - If the server & client are synced and this value is different (i.e. false when synced data says true) the glass state will change. This vlaue 
///    				   is updated if the server & client are out of sync (i.e. set to true if the server says the glass should be frosted).
FUNC FG_UPDATE_RESULT FROSTED_GLASS_UPDATE_CLIENT(FROSTED_GLASS_SERVER_DATA& server, FROSTED_GLASS_CLIENT_DATA& client, ENTITY_INDEX &glassEntities[], BOOL& bLocalGlassOn, INT iGlassIndex, INT iScriptHost, TIME_DATATYPE &Timer, FG_UPDATE_SETTING_MODE eMode = FG_UPDATE_SETTING_VISIBILITY)
	FG_UPDATE_RESULT result = FG_NO_CHANGE
	
	IF IS_BIT_SET_ENUM(client.iBS, FG_CLIENT_FROSTED_GLASS_ON) <> IS_BIT_SET_ENUM(server.iBS, FG_SERVER_BS_FROSTED_GLASS_ON)
		IF IS_BIT_SET_ENUM(server.iBS, FG_SERVER_BS_FROSTED_GLASS_ON)
			PRINTLN("[FROSTED_GLASS][UPDATE_CLIENT]FG_UPDATE_FROSTED_THIS_FRAME. bLocalGlassOn = TRUE.")
			SET_BIT_ENUM(client.iBS, FG_CLIENT_FROSTED_GLASS_ON)
			bLocalGlassOn = TRUE
			result = FG_UPDATE_FROSTED_THIS_FRAME
		ELSE
			PRINTLN("[FROSTED_GLASS][UPDATE_CLIENT] FG_UPDATE_UNFROSTED_THIS_FRAME. bLocalGlassOn = FALSE.")
			CLEAR_BIT_ENUM(client.iBS, FG_CLIENT_FROSTED_GLASS_ON)
			bLocalGlassOn = FALSE
			result = FG_UPDATE_UNFROSTED_THIS_FRAME
		ENDIF
	ELSE
		IF bLocalGlassOn <> IS_BIT_SET_ENUM(server.iBS, FG_SERVER_BS_FROSTED_GLASS_ON)
		AND ABSI(GET_TIME_DIFFERENCE(Timer, GET_NETWORK_TIME())) >= 0250
			BROADCAST_UPDATED_FROSTED_GLASS(bLocalGlassOn, iGlassIndex, iScriptHost)
			Timer = GET_NETWORK_TIME()
		ENDIF
	ENDIF
	
	INT i
	REPEAT COUNT_OF(glassEntities) i
		IF DOES_ENTITY_EXIST(glassEntities[i])
			IF IS_BIT_SET_ENUM(client.iBS, FG_CLIENT_FROSTED_GLASS_ON)
				IF eMode = FG_UPDATE_SETTING_VISIBILITY
					IF NOT IS_ENTITY_VISIBLE(glassEntities[i])
						SET_ENTITY_VISIBLE(glassEntities[i], TRUE)
					ENDIF
				ELIF eMode = FG_UPDATE_SETTING_ALPHA
					IF (GET_ENTITY_ALPHA(glassEntities[i]) != GLASS_ALPHA_MAX)
						SET_ENTITY_ALPHA(glassEntities[i], GLASS_ALPHA_MAX, FALSE)
					ENDIF
				ELIF eMode = FG_UPDATE_CREATE_MODEL_HIDE
					//needs to be handled by parent script
				ENDIF
			ELSE
				IF eMode = FG_UPDATE_SETTING_VISIBILITY
					IF IS_ENTITY_VISIBLE(glassEntities[i])
						SET_ENTITY_VISIBLE(glassEntities[i], FALSE)
					ENDIF
				ELIF eMode = FG_UPDATE_SETTING_ALPHA
					IF (GET_ENTITY_ALPHA(glassEntities[i]) != 0)
						SET_ENTITY_ALPHA(glassEntities[i], 0, FALSE)
					ENDIF
				ELIF eMode = FG_UPDATE_CREATE_MODEL_HIDE
					//needs to be handled by parent script
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN result
ENDFUNC

