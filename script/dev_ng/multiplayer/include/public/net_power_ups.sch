//////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_power_ups.sch														//
// Description: Controls the running of power ups collected by the player.				//
// Written by:  Ryan Baker																//
// Date: 28/02/2013																		//
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "commands_network.sch"
//USING "script_player.sch"
//USING "commands_path.sch"
//USING "building_control_public.sch"

// Network Headers
USING "net_include.sch"
USING "net_events.sch"
USING "net_mission.sch"
USING "net_scoring_common.sch"
USING "net_ambience.sch"
USING "fm_in_corona_header.sch"


//╔═════════════════════════════════════════════════════════════════════════════╗
//║										RAGE																		
//╚═════════════════════════════════════════════════════════════════════════════╝
CONST_INT PU_RAGE_TIME						60000

//GLOBAL RAGE BIT INDEX
CONST_INT gbiPU_RAGE_Running		0
CONST_INT gbiPU_RAGE_RestartTimer	1
CONST_INT gbiPU_RAGE_ForceEnd		2
CONST_INT gbiPU_RAGE_TimedOut		3 //** TWH - RLB END
CONST_INT gbiPU_RAGE_OnSound		4
CONST_INT gbiPU_RAGE_OffSound		5

//RAGE STAGES
CONST_INT PU_RAGE_STAGE_INIT		0
CONST_INT PU_RAGE_STAGE_RUNNING		1
CONST_INT PU_RAGE_STAGE_CLEANUP		2

//LOCAL RAGE BIT INDEX
CONST_INT biPU_RAGE_ScreenActive	0
CONST_INT biPU_RAGE_Initial			1

STRUCT STRUCT_PU_RAGE_DATA
	INT iBitSet
	INT iStage = PU_RAGE_STAGE_INIT
	INT iAudioProgresss
	SCRIPT_TIMER iRageTimer
ENDSTRUCT

//╔═════════════════════════╗
//║		 FX 																	
//╚═════════════════════════╝

CONST_INT FX_REASON_CLEANUP		0
CONST_INT FX_REASON_RAGE 		1
CONST_INT FX_REASON_POWER		2
CONST_INT FX_REASON_KILLSTREAK	3
CONST_INT FX_REASON_DEATHSTREAK	4
CONST_INT FX_REASON_HAD_RAGE	5 // 1723205
CONST_INT FX_REASON_STONED		6

FUNC STRING GET_FX_NAME(INT iEffect, BOOL bOutro)
	SWITCH iEffect
		CASE FX_REASON_RAGE 	
			IF bOutro
				RETURN "MP_Bull_tost_Out"
			ELSE
				RETURN "MP_Bull_tost"
			ENDIF
		BREAK
		CASE FX_REASON_POWER	
			IF bOutro
				RETURN "MP_Powerplay_Out"
			ELSE
				RETURN "MP_Powerplay"
			ENDIF
		BREAK
		CASE FX_REASON_KILLSTREAK	
			IF bOutro
				RETURN "MP_Killstreak_Out"
			ELSE
				RETURN "MP_Killstreak"
			ENDIF
		BREAK
		CASE FX_REASON_DEATHSTREAK	
			IF bOutro
				RETURN "MP_Loser_Streak_Out"
			ELSE
				RETURN "MP_Loser_Streak"
			ENDIF
		BREAK
		CASE FX_REASON_STONED
			IF bOutro
				RETURN "BikerFilterOut"
			ELSE
				RETURN "BikerFilter"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN ""
ENDFUNC

PROC TRIGGER_FX_EFFECT(INT iEffect)

	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_noDMfx")
	
		EXIT
	ENDIF
	#ENDIF

	STRING sEffect = GET_FX_NAME(iEffect, FALSE)
	ANIMPOSTFX_PLAY(sEffect, 0, TRUE)
	PRINTLN("[ DM_FX ] TRIGGER_FX_EFFECT iEffect =  ", GET_FX_NAME(iEffect, FALSE))
ENDPROC

FUNC INT GET_OUTRO_TIME(BOOL bEndNaturally)
	IF NOT bEndNaturally
		
		RETURN 500
	ENDIF

	RETURN 0
ENDFUNC

FUNC BOOL HAS_FX_OUTRO_FINISHED(INT iEffect, BOOL bEndNaturally)
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_noDMfx")
	
		RETURN FALSE
	ENDIF
	#ENDIF
	BOOL bLooped 				= FALSE
	STRING sCurrentActiveEffect = GET_FX_NAME(iEffect, FALSE) // FALSE careful here we are checking an exising fx is playing 
	STRING sOUTroEffect  		= GET_FX_NAME(iEffect, TRUE)
	IF ANIMPOSTFX_IS_RUNNING(sCurrentActiveEffect) 
		ANIMPOSTFX_PLAY(sOUTroEffect, GET_OUTRO_TIME(bEndNaturally), bLooped)
		IF NOT ANIMPOSTFX_IS_RUNNING(sOUTroEffect) 
		
			#IF IS_DEBUG_BUILD
			IF bEndNaturally
				PRINTLN("[ DM_FX ] HAS_FX_OUTRO_FINISHED (bEndNaturally TRUE) iEffect =  ", sOUTroEffect)
			ELSE
				PRINTLN("[ DM_FX ] HAS_FX_OUTRO_FINISHED (bEndNaturally FALSE) iEffect =  ", sOUTroEffect)
			ENDIF
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC STOP_FX_EFFECT(INT iEffect)
	STRING sEffect = GET_FX_NAME(iEffect, FALSE)
	ANIMPOSTFX_STOP(sEffect)
	PRINTLN("[ DM_FX ] STOP_FX_EFFECT iEffect =  ", GET_FX_NAME(iEffect, FALSE))
ENDPROC

//╔═════════════════════════╗
//║		 RAGE - PUBLIC															
//╚═════════════════════════╝

PROC INCREMENT_BULLSHARK_STAT()
	PRINTLN("     ---------->     PU_RAGE - INCREMENT_BULLSHARK_STAT YES")
	INCREMENT_MP_INT_CHARACTER_STAT(MP_STAT_DM_COLLECTED_SHARK_TEST)
ENDPROC

PROC DO_BULLSHARK_HELP()
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_DM_COLLECTED_SHARK_TEST) < 4
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
		ELSE
			PRINTLN("     ---------->     PU_RAGE - DO_BULLSHARK_HELP YES")
			// "Bull Shark Testosterone temporarily boosts your damage and toughness."
			PRINT_HELP("DM_BRU_HLP")
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Forces the Local Player to end their Rage Power Up
PROC PU_FORCE_END_RAGE()
	SET_BIT(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_ForceEnd)
	NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_FORCE_END_RAGE - SET") NET_NL()
ENDPROC

//PURPOSE: Sets the local player to have the Rage Power Up (Inflict Double Damage - Take Half Damage)
PROC PU_GIVE_RAGE()
	IF NOT IS_BIT_SET(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_Running)
		SET_BIT(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_Running)
		INCREMENT_BULLSHARK_STAT()
		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_GIVE_RAGE - gbiPU_RAGE_Running") NET_NL()
	ELSE
		SET_BIT(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_RestartTimer)
		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_GIVE_RAGE - gbiPU_RAGE_RestartTimer") NET_NL()
	ENDIF
ENDPROC

//PURPOSE: Returns TRUE if the Rage Power Up (Bull Shark Testosterone) is active.
FUNC BOOL IS_RAGE_ACTIVE()
	RETURN IS_BIT_SET(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_Running)
ENDFUNC

//╔═════════════════════════╗
//║		RAGE - MISC 																
//╚═════════════════════════╝

// 	** TWH - RLB - START (Speirs)

CONST_INT AUDIO_BEEP_TIME_5 5100
CONST_INT AUDIO_BEEP_TIME_4 4000
CONST_INT AUDIO_BEEP_TIME_3 3000
CONST_INT AUDIO_BEEP_TIME_2 2000
CONST_INT AUDIO_BEEP_TIME_1 1000

// (428108 can we get a 5 second sfx countdown when bullshark is running out (Speirs))
PROC DO_FIVE_SECOND_LEFT_AUDIO(STRUCT_PU_RAGE_DATA &PURageData)
	IF HAS_NET_TIMER_STARTED(PURageData.iRageTimer)
		SWITCH PURageData.iAudioProgresss
			CASE 0
				IF HAS_NET_TIMER_EXPIRED(PURageData.iRageTimer, (PU_RAGE_TIME - AUDIO_BEEP_TIME_5))
					PLAY_SOUND_FRONTEND(-1, "5_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET") 
					PURageData.iAudioProgresss = 1
				ENDIF
			BREAK
			CASE 1
				IF HAS_NET_TIMER_EXPIRED(PURageData.iRageTimer, (PU_RAGE_TIME - AUDIO_BEEP_TIME_4))
					PLAY_SOUND_FRONTEND(-1, "5_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET") 
					PURageData.iAudioProgresss = 2
				ENDIF
			BREAK
			CASE 2
				IF HAS_NET_TIMER_EXPIRED(PURageData.iRageTimer, (PU_RAGE_TIME - AUDIO_BEEP_TIME_3))
					PLAY_SOUND_FRONTEND(-1, "5_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET") 
					PURageData.iAudioProgresss = 3
				ENDIF
			BREAK
			CASE 3
				IF HAS_NET_TIMER_EXPIRED(PURageData.iRageTimer, (PU_RAGE_TIME - AUDIO_BEEP_TIME_2))
					PLAY_SOUND_FRONTEND(-1, "5_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET") 
					PURageData.iAudioProgresss = 4
				ENDIF
			BREAK
			CASE 4
				IF HAS_NET_TIMER_EXPIRED(PURageData.iRageTimer, (PU_RAGE_TIME - AUDIO_BEEP_TIME_1))
					PLAY_SOUND_FRONTEND(-1, "5_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET") 
					PURageData.iAudioProgresss = 5
				ENDIF
			BREAK
			CASE 5

			BREAK
		ENDSWITCH
	ELSE
		PURageData.iAudioProgresss = 0
	ENDIF
ENDPROC

// (1462378 Custom audio for Bull Shark Testosterone (Speirs))
PROC PLAY_START_BULLSHARK_SOUND()
	PLAY_SOUND_FRONTEND(-1, "BULL_SHARK_TESTOSTERONE_START_MASTER", "", FALSE)
	PRINTLN(" [BS_AUDIO] PLAY_START_BULLSHARK_SOUND")
	BROADCAST_RACE_BULL_SHARK_ON(ALL_PLAYERS(FALSE, TRUE))
ENDPROC

FUNC BOOL HAS_BULLSHARK_AUDIO_LOADED()
	REGISTER_SCRIPT_WITH_AUDIO()
	IF REQUEST_SCRIPT_AUDIO_BANK("BULL_SHARK_TESTOSTERONE_01")//, TRUE)
	
		PLAY_START_BULLSHARK_SOUND()
	
		PRINTLN(" [BS_AUDIO] HAS_BULLSHARK_AUDIO_LOADED (TRUE) ")
		RETURN TRUE
	ENDIF
	
	PRINTLN(" [BS_AUDIO] HAS_BULLSHARK_AUDIO_LOADED (FALSE)")
	RETURN FALSE
ENDFUNC

PROC DO_BULLSHARK_AUDIO_STREAM()
	IF LOAD_STREAM("BULL_SHARK_TESTOSTERONE_STREAM_MASTER")
		SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", TRUE)
		PLAY_STREAM_FRONTEND()
		SET_AUDIO_FLAG("DisableReplayScriptStreamRecording", FALSE)
		PRINTLN(" [BS_AUDIO] DO_AUDIO")
	ENDIF
ENDPROC

PROC CLEANUP_AUDIO()
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("BULL_SHARK_TESTOSTERONE_01")
	IF IS_BIT_SET(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_TimedOut)
		PLAY_SOUND_FRONTEND(-1, "BULL_SHARK_TESTOSTERONE_END_MASTER", "", FALSE)
		PRINTLN(" [BS_AUDIO] CLEANUP_AUDIO_NATURALLY")
		BROADCAST_RACE_BULL_SHARK_OFF(ALL_PLAYERS(FALSE, TRUE))
	ENDIF
	STOP_STREAM()
//	RELEASE_SCRIPT_AUDIO_BANK()
	PRINTLN(" [BS_AUDIO] CLEANUP_AUDIO")
ENDPROC
//** TWH - RLB END

//PURPOSE: Returns TRUE if the Rage should end
FUNC BOOL PU_SHOULD_RAGE_END(STRUCT_PU_RAGE_DATA &PURageData)
	IF IS_BIT_SET(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_ForceEnd)
		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_SHOULD_RAGE_END - FORCE END SET") NET_NL()
		RETURN TRUE
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(PURageData.iRageTimer, PU_RAGE_TIME)
		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_SHOULD_RAGE_END - TIME UP") NET_NL()
		SET_BIT(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_TimedOut) //** TWH - RLB END
		RETURN TRUE
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_SHOULD_RAGE_END - PLAYER DEAD") NET_NL()
		RETURN TRUE
	ENDIF
	
	IF IS_TRANSITION_ACTIVE()
		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_SHOULD_RAGE_END - TRANSITION ACTIVE") NET_NL()
		RETURN TRUE
	ENDIF
	
	IF g_bMissionEnding
		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_SHOULD_RAGE_END - g_bMissionEnding") NET_NL()
		RETURN TRUE
	ENDIF
	
	IF IS_CUTSCENE_PLAYING()
		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_SHOULD_RAGE_END - IS_CUTSCENE_PLAYING") NET_NL()
		RETURN TRUE
	ENDIF
	
	//FORCE CLEANUP DONE IN CORONA CHECKS NOW - DUE TO ISSUES WITH GANG ATTACK
	//IF GET_CORONA_STATUS() != CORONA_STATUS_IDLE
	/*IF IS_PLAYER_ACTIVE_IN_CORONA()
		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_SHOULD_RAGE_END - IN CORONA") NET_NL()
		RETURN TRUE
	ENDIF*/
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Controls whether the Rage filter should be displayed or cleaned up mid Rage Mode
PROC PU_CONTROL_RAGE_SCREEN(STRUCT_PU_RAGE_DATA &PURageData)
	IF IS_CELLPHONE_CAMERA_IN_USE()
	OR IS_CUTSCENE_PLAYING()
		IF IS_BIT_SET(PURageData.iBitSet, biPU_RAGE_ScreenActive)
//			IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
//				SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(2.0)
				STOP_FX_EFFECT(FX_REASON_RAGE) 
				CLEAR_BIT(PURageData.iBitSet, biPU_RAGE_ScreenActive)
				CLEAR_BIT(PURageData.iBitSet, biPU_RAGE_Initial)
				NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_CONTROL_RAGE_SCREEN - CLEAR TIMCECYCLE - A") NET_NL()
//			ENDIF
		ENDIF
	ELSE
	
		IF NOT IS_BIT_SET(PURageData.iBitSet, biPU_RAGE_Initial)
//			IF GET_TIMECYCLE_MODIFIER_INDEX() = -1
				//SET_TRANSITION_TIMECYCLE_MODIFIER("MP_Bull_tost", 1.0)	//REDMIST	//"PULSE", fPulseDelay)
				TRIGGER_FX_EFFECT(FX_REASON_RAGE) 
				SET_BIT(PURageData.iBitSet, biPU_RAGE_Initial)
				NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_CONTROL_RAGE_SCREEN - SET TIMCECYCLE - A") NET_NL()
//			ENDIF
		ELSE
			IF NOT IS_BIT_SET(PURageData.iBitSet, biPU_RAGE_ScreenActive)	
				TRIGGER_FX_EFFECT(FX_REASON_RAGE) 
				SET_BIT(PURageData.iBitSet, biPU_RAGE_ScreenActive)
				NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_CONTROL_RAGE_SCREEN - SET biPU_RAGE_ScreenActive - B") NET_NL()
			ENDIF
			
//			IF GET_TIMECYCLE_MODIFIER_INDEX() = -1
//				IF IS_BIT_SET(PURageData.iBitSet, biPU_RAGE_ScreenActive)	
//					//SET_TRANSITION_TIMECYCLE_MODIFIER("MP_Bull_tost", 1.0)
//					TRIGGER_FX_EFFECT(FX_REASON_RAGE) 
//					NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_CONTROL_RAGE_SCREEN - SET TIMCECYCLE - B") NET_NL()
//					CLEAR_BIT(PURageData.iBitSet, biPU_RAGE_ScreenActive)	
//					NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_CONTROL_RAGE_SCREEN - CLEAR biPU_RAGE_ScreenActive - B") NET_NL()
//				ENDIF
//			ELSE
//				IF NOT IS_BIT_SET(PURageData.iBitSet, biPU_RAGE_ScreenActive)	
//					SET_BIT(PURageData.iBitSet, biPU_RAGE_ScreenActive)
//					NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_CONTROL_RAGE_SCREEN - SET biPU_RAGE_ScreenActive - B") NET_NL()
//				ENDIF
//			ENDIF
			
		ENDIF
		
	ENDIF
ENDPROC

//╔═════════════════════════╗
//║		RAGE - STAGES 															
//╚═════════════════════════╝

//PURPOSE: Sets the Rage affects
PROC PU_START_RAGE(STRUCT_PU_RAGE_DATA &PURageData)
	IF HAS_BULLSHARK_AUDIO_LOADED()
		//Initialise Variables
		CLEAR_BIT(PURageData.iBitSet, biPU_RAGE_ScreenActive)
		CLEAR_BIT(PURageData.iBitSet, biPU_RAGE_Initial)
		CLEAR_BIT(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_RestartTimer)
		CLEAR_BIT(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_ForceEnd)
		CLEAR_BIT(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_TimedOut)//** TWH - RLB END
		
		//Do Double Damage
		PRINTLN("CS_DMG], PU_START_RAGE")
		SET_PLAYER_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_WEAPON_DAMAGE*2)
		#IF IS_DEBUG_BUILD
			SET_PLAYER_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), lw_fPlayerWeaponDamageModifier*2)
		#ENDIF
		SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_MELEE_WEAPON_DAMAGE*2)
		
		//Take Half Damage
		IF NATIVE_TO_INT(PLAYER_ID()) != g_i_PowerPlayer
			SET_PLAYER_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_WEAPON_DEFENSE/2)
			SET_PLAYER_MELEE_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_MELEE_WEAPON_DEFENSE/2)
			NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - TAKE HALF DAMAGE") NET_NL()
		ELSE
			SET_PLAYER_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_WEAPON_DEFENSE)
			SET_PLAYER_MELEE_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_MELEE_WEAPON_DEFENSE)
			NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - TAKE NORMAL DAMAGE - PLAYER IS POWER PLAYER") NET_NL()
		ENDIF
			
		//Rage Screen
//		IF GET_TIMECYCLE_MODIFIER_INDEX() = -1
			//SET_TRANSITION_TIMECYCLE_MODIFIER("MP_Bull_tost", 1.0)	//"PULSE", fPulseDelay)
			TRIGGER_FX_EFFECT(FX_REASON_RAGE) 
			NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_START_RAGE - SET TIMCECYCLE - C") NET_NL()
			CLEAR_BIT(PURageData.iBitSet, biPU_RAGE_ScreenActive)	
			NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_START_RAGE - CLEAR biPU_RAGE_ScreenActive - C") NET_NL()
//		ELSE	
//			NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_START_RAGE - ALREADY TIMCECYCLE - C") NET_NL()
//		ENDIF
		SET_BIT(PURageData.iBitSet, biPU_RAGE_ScreenActive)
		
		DO_BULLSHARK_HELP()
		
		RESET_NET_TIMER(PURageData.iRageTimer)
		SETUP_NEW_BIG_MESSAGE(BIG_MESSAGE_GENERIC_MIDSIZED, "STRAP_BULL", "", HUD_COLOUR_WHITE) // 	** TWH - RLB - 
		BROADCAST_BULLSHARK_COLLECTED()
		
		PURageData.iStage = PU_RAGE_STAGE_RUNNING
		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_START_RAGE - PURageData.iStage = PU_RAGE_STAGE_RUNNING") NET_NL()
		
		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_START_RAGE - STARTED") NET_NL()
	ENDIF
ENDPROC

//PURPOSE:
PROC PU_RAGE_RUNNING(STRUCT_PU_RAGE_DATA &PURageData)
	IF (PU_RAGE_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(PURageData.iRageTimer)) >= 0
		DRAW_GENERIC_TIMER((PU_RAGE_TIME-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(PURageData.iRageTimer)), "ABB_TIMER", 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_TOP)
	ELSE
		DRAW_GENERIC_TIMER(0, "ABB_TIMER", 0, TIMER_STYLE_DONTUSEMILLISECONDS, DEFAULT, DEFAULT, HUDORDER_TOP)
	ENDIF
	
	PU_CONTROL_RAGE_SCREEN(PURageData)
	// 	** TWH - RLB - start
	DO_BULLSHARK_AUDIO_STREAM()
	DO_FIVE_SECOND_LEFT_AUDIO(PURageData) 
	// 	** TWH - RLB - end
	
	//Check if Timer should be reset
	IF IS_BIT_SET(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_RestartTimer)
		RESET_NET_TIMER(PURageData.iRageTimer)
		CLEAR_BIT(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_RestartTimer)
		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_RAGE_RUNNING - TIMER RESTARTED") NET_NL()
	ENDIF
	
	IF PU_SHOULD_RAGE_END(PURageData)
		PURageData.iStage = PU_RAGE_STAGE_CLEANUP
		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_RAGE_RUNNING - PURageData.iStage = PU_RAGE_STAGE_CLEANUP") NET_NL()
	ENDIF
ENDPROC

//PURPOSE:
PROC PU_RAGE_CLEANUP(STRUCT_PU_RAGE_DATA &PURageData)
//	IF GET_TIMECYCLE_MODIFIER_INDEX() != -1 	// 	** TWH - RLB - (Speirs commented out to fix PT 1424675, looks like it's already being dealt with below anyway, CL: 4340596)
		//Do Normal Damage
		PRINTLN("CS_DMG], PU_RAGE_CLEANUP")
		SET_PLAYER_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_WEAPON_DAMAGE)
		#IF IS_DEBUG_BUILD
			SET_PLAYER_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), lw_fPlayerWeaponDamageModifier)
		#ENDIF
		SET_PLAYER_MELEE_WEAPON_DAMAGE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_MELEE_WEAPON_DAMAGE)
		
		//Take Normal Damage
		SET_PLAYER_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_WEAPON_DEFENSE)
		SET_PLAYER_MELEE_WEAPON_DEFENSE_MODIFIER(PLAYER_ID(), FREEMODE_PLAYER_MELEE_WEAPON_DEFENSE)
		
		//Normal Screen
		//CLEAR_TIMECYCLE_MODIFIER()
//		IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
//			SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(2.0)
//			NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_RAGE_CLEANUP - CLEAR TIMCECYCLE - B") NET_NL()
//		ELSE
//			CLEAR_TIMECYCLE_MODIFIER()
//			NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_RAGE_CLEANUP - NO TIMCECYCLE - B") NET_NL()
//		ENDIF
		PRINTLN("[ DM_FX ] STOP_FX_EFFECT FX_REASON_RAGE 9RYAN) =  ")
		STOP_FX_EFFECT(FX_REASON_RAGE) 
		
		CLEANUP_AUDIO() //** TWH - RLB 
		
		PURageData.iBitSet = 0
		MPGlobals.PowerUpsData.iRageBitSet = 0
		
		PURageData.iStage = PU_RAGE_STAGE_INIT
		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_RAGE_CLEANUP - PURageData.iStage = PU_RAGE_STAGE_INIT") NET_NL()
		
		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_RAGE_CLEANUP - DONE") NET_NL()
//	ELSE
//		
//		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - PU_RAGE_CLEANUP - WAITING FOR TIMECYCLE MODIFIER TO FINISH BEING SET UP") NET_NL()
//	ENDIF
ENDPROC

//PURPOSE: Play On/Off sound when remote player starts or ends a Bull Shark.
PROC PROCESS_REMOTE_ON_OFF_SOUNDS()
	//On
	IF IS_BIT_SET(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_OnSound)
		REGISTER_SCRIPT_WITH_AUDIO()
		IF REQUEST_SCRIPT_AUDIO_BANK("BULL_SHARK_TESTOSTERONE_01")//, TRUE)
			PLAY_SOUND_FRONTEND(-1, "BULL_SHARK_TESTOSTERONE_START_MASTER", "", FALSE)
			CLEAR_BIT(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_OnSound)
			PRINTLN("  BS_AUDIO   ---------->     PU_RAGE - ON SOUND PLAYED")
			IF IS_BIT_SET(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_Running)
//				RELEASE_SCRIPT_AUDIO_BANK()
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("BULL_SHARK_TESTOSTERONE_01")
			ENDIF
		ENDIF
	//Off
	ELIF IS_BIT_SET(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_OffSound)
		REGISTER_SCRIPT_WITH_AUDIO()
		IF REQUEST_SCRIPT_AUDIO_BANK("BULL_SHARK_TESTOSTERONE_01")//, TRUE)
			PLAY_SOUND_FRONTEND(-1, "BULL_SHARK_TESTOSTERONE_END_MASTER", "", FALSE)
			CLEAR_BIT(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_OffSound)
			PRINTLN("  BS_AUDIO   ---------->     PU_RAGE - OFF SOUND PLAYED")
			IF IS_BIT_SET(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_Running)
//				RELEASE_SCRIPT_AUDIO_BANK()
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("BULL_SHARK_TESTOSTERONE_01")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//╔═════════════════════════╗
//║		RAGE - MAINTAIN 																		
//╚═════════════════════════╝

//PURPOSE: Controls the rage power up given by Brucie Box and DM.
PROC PU_MAINTAIN_RAGE(STRUCT_PU_RAGE_DATA &PURageData)
	IF IS_BIT_SET(MPGlobals.PowerUpsData.iRageBitSet, gbiPU_RAGE_Running)
		
		SWITCH PURageData.iStage
			CASE PU_RAGE_STAGE_INIT
				PU_START_RAGE(PURageData)
			BREAK
			
			CASE PU_RAGE_STAGE_RUNNING
				PU_RAGE_RUNNING(PURageData)
			BREAK
			
			CASE PU_RAGE_STAGE_CLEANUP
				PU_RAGE_CLEANUP(PURageData)
			BREAK
		ENDSWITCH
		
	ENDIF
	
	PROCESS_REMOTE_ON_OFF_SOUNDS()
	
	#IF IS_DEBUG_BUILD
	IF bDebugForceRagePowerUp
		NET_PRINT_TIME() NET_PRINT("     ---------->     PU_RAGE - bDebugForceRagePowerUp SET") NET_NL()
		PU_GIVE_RAGE()
		bDebugForceRagePowerUp = FALSE
	ENDIF
	#ENDIF	
ENDPROC



//╔═════════════════════════════════════════════════════════════════════════════╗
//║									POWER UPS																		
//╚═════════════════════════════════════════════════════════════════════════════╝

STRUCT STRUCT_POWER_UP_DATA
	STRUCT_PU_RAGE_DATA 	RageData
ENDSTRUCT

//PURPOSE: Processes and runs the power ups a player can collect
PROC PROCESS_POWER_UPS(STRUCT_POWER_UP_DATA &PowerUpData)
	
	//Brucie Box/RAGE			-	Deal Double Damage and Take Half Damage
	PU_MAINTAIN_RAGE(PowerUpData.RageData)
	
ENDPROC
