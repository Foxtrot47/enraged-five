USING "globals.sch"
USING "net_include.sch"
USING "fm_mission_flow.sch"
USING "transition_controller.sch"
USING "net_ambience.sch"
USING "net_vehicle_help_text_reg.sch"

CONST_INT DISPLAY_DELAY 3000

STRUCT VEHICLE_HELP_TEXT_STRUCT
	
	BOOL bSameVehicleEntry = FALSE
	
	SCRIPT_TIMER sDisplayDelayTimer
	VEHICLE_INDEX viCurrentVehicle
	MODEL_NAMES eCurrentVehicleModel
		
	VEHICLE_HELP_TEXT_BIT_SETS_STRUCT sHelpTextBitSets
	
ENDSTRUCT


/// PURPOSE:
///    Resets the garage exit delay timer if the player is in a property
/// PARAMS:
///    sVehicleHelpTextInst - instance of the vehicle help text
PROC TRY_RESET_DISPLAY_DELAY_TIMER(VEHICLE_HELP_TEXT_STRUCT& sVehicleHelpTextInst)
	
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
	OR IS_PLAYER_ENTERING_OR_EXITING_PROPERTY(PLAYER_ID())
	OR IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		REINIT_NET_TIMER(sVehicleHelpTextInst.sDisplayDelayTimer)
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Checks if the player is in a property that allows help text.
///    Help text is allowed if not in any property
/// RETURNS:
///    TRUE if help text is allowed in the property or 
///    if the player is not in a property
FUNC BOOL IS_PLAYER_IN_PROPERTY_THAT_ALLOWS_HELP_TEXT()
	
	INT iRoomKey = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
	
	// Arena allows help text
	IF 	iRoomKey = HASH("Arena_Room")
		RETURN TRUE
	ENDIF
	
	// Return True if the player isn't in a property
	RETURN NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
	
ENDFUNC


/// PURPOSE:
///    Displays all the registered vehicle help texts if
///    they are needed
/// PARAMS:
///    sVehicleHelpTextInst - instance of the vehicle help text
PROC DISPLAY_ALL_VEHICLE_HELP_TEXTS(VEHICLE_HELP_TEXT_STRUCT& sVehicleHelpTextInst)
	
	IF DISPLAY_ON_FOOT_VEHICLE_HELP_TEXT(sVehicleHelpTextInst.sHelpTextBitSets,
	sVehicleHelpTextInst.viCurrentVehicle, sVehicleHelpTextInst.eCurrentVehicleModel)
		EXIT
	ENDIF
	
	// Leave if not in a vehicle or help has already displayed for entering the vehicle
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) 
	OR NOT IS_PLAYER_IN_PROPERTY_THAT_ALLOWS_HELP_TEXT()
	OR sVehicleHelpTextInst.bSameVehicleEntry 
		EXIT
	ENDIF	

	IF DISPLAY_GENERAL_VEHICLE_HELP_TEXT(sVehicleHelpTextInst.sHelpTextBitSets,
	sVehicleHelpTextInst.viCurrentVehicle, sVehicleHelpTextInst.eCurrentVehicleModel)
		
		// Flag so messages aren't displayed repeatedly
		sVehicleHelpTextInst.bSameVehicleEntry = TRUE
		EXIT
	ENDIF
	
	IF DISPLAY_VEHICLE_MOD_HELP_TEXT(sVehicleHelpTextInst.sHelpTextBitSets,
	sVehicleHelpTextInst.viCurrentVehicle, sVehicleHelpTextInst.eCurrentVehicleModel)
	
		// Flag so messages aren't displayed repeatedly
		sVehicleHelpTextInst.bSameVehicleEntry = TRUE
		EXIT
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Checks if the player needs help text i.e.
///    player has control
/// RETURNS:
///    TRUE if the player needs help text
FUNC BOOL DOES_PLAYER_NEED_HELP_TEXT()
	
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	// If in a race controls aren't technically disabled at the start although the player can't move.
	// In this case check if the player can exit the vehicle to gage if their controls are "disabled"
	IF (IS_RACE() OR IS_STUNT_RACE()) AND NOT IS_CONTROL_ENABLED(PLAYER_CONTROL, INPUT_VEH_EXIT)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
	
ENDFUNC


/// PURPOSE:
///    Checks if car mod help text can be displayed this frame
/// PARAMS:
///    sVehicleHelpTextInst - instance of the vehicle help text
/// RETURNS:
///    TRUE if we can display car mod help text this frame
FUNC BOOL CAN_VEHICLE_HELP_TEXT_BE_DISPLAYED(VEHICLE_HELP_TEXT_STRUCT& sVehicleHelpTextInst)	
	
	// Restart delay timer if in property
	TRY_RESET_DISPLAY_DELAY_TIMER(sVehicleHelpTextInst)
	
	// Returns true if:
	RETURN 
	NOT IS_TRANSITION_ACTIVE() 										AND
	NOT IS_HELP_MESSAGE_BEING_DISPLAYED() 							AND
	NOT IS_RADAR_HIDDEN() 											AND
	NOT IS_CUTSCENE_PLAYING() 										AND
	DOES_PLAYER_NEED_HELP_TEXT()									AND
	HAS_NET_TIMER_EXPIRED_READ_ONLY(sVehicleHelpTextInst.sDisplayDelayTimer, DISPLAY_DELAY)
	
ENDFUNC


/// PURPOSE:
///    Updates the vehicle help struct with the vehicle the player is currently using
///    and its model
/// PARAMS:
///    sVehicleHelpTextInst - instance of the vehicle help text
PROC UPDATE_CURRENT_VEHICLE_DETAILS(VEHICLE_HELP_TEXT_STRUCT& sVehicleHelpTextInst)
	
	sVehicleHelpTextInst.viCurrentVehicle = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
	
	MODEL_NAMES eModelName = DUMMY_MODEL_FOR_SCRIPT
	
	IF sVehicleHelpTextInst.viCurrentVehicle != NULL
		eModelName = GET_ENTITY_MODEL(sVehicleHelpTextInst.viCurrentVehicle)
	ENDIF
	
	sVehicleHelpTextInst.eCurrentVehicleModel = eModelName
	
ENDPROC


/// PURPOSE:
///    Resets same vehicle entry flag when the player is not in a vehicle
/// PARAMS:
///    sVehicleHelpTextInst - instance of the vehicle help text
PROC TRY_RESET_VEHICLE_ENTRY_FLAG(VEHICLE_HELP_TEXT_STRUCT& sVehicleHelpTextInst)

	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		sVehicleHelpTextInst.bSameVehicleEntry = FALSE
	ENDIF
	
ENDPROC


//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡    ENTRY POINT    ╞═════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Displays any help text for the current vehicle
PROC MAINTAIN_VEHICLE_MODS_HELP_TEXT(VEHICLE_HELP_TEXT_STRUCT& sVehicleHelpTextInst)

	// Reset so messages can be shown when entering a vehicle
	TRY_RESET_VEHICLE_ENTRY_FLAG(sVehicleHelpTextInst)
	
	IF NOT CAN_VEHICLE_HELP_TEXT_BE_DISPLAYED(sVehicleHelpTextInst)
		EXIT
	ENDIF
	
	UPDATE_CURRENT_VEHICLE_DETAILS(sVehicleHelpTextInst)
	DISPLAY_ALL_VEHICLE_HELP_TEXTS(sVehicleHelpTextInst)

ENDPROC

