/* ----------------------------------------------------------------------
* Name: net_betting_drawing.sch
* Author: James Adwick
* Date: 17/08/2012
* Purpose: Header file with commands to draw betting display for clients
* ----------------------------------------------------------------------*/

USING "globals.sch"
USING "net_betting_common.sch"
USING "fm_in_corona_header.sch"
USING "PM_MissionCreator_Public.sch"
USING "MP_Scaleform_functions.sch"


PROC TOGGLE_ODDS_DISPLAY()
	INCREMENT_PAUSE_MENU_SETTING(ENUM_TO_INT(MP_SETTING_BETTING_ODDS), TRUE)
ENDPROC

FUNC INT GET_CASH_DELTA_VALUE()
	RETURN ROUND(g_sMPTunables.fMinBetLimit)
ENDFUNC

FUNC INT GET_CORONA_AVAILABLE_BETTING_CASH() //INT iBettingIndex) //, BOOL bForSelectedPlayer = TRUE)

	INT iActivityCost = 0
	
	IF DO_I_NEED_TO_PAY_TO_PLAY_THIS_FM_ACTIVITY(g_FMMC_STRUCT.iMissionType, g_FMMC_STRUCT.iMissionSubType)
		iActivityCost = GET_FM_ACTIVITY_COST(g_FMMC_STRUCT.iMissionType, g_FMMC_STRUCT.iMissionSubType, TRUE)
	ENDIF

	//The SCRIPT_MAX_INT32 overflow of GET_LOCAL_PLAYER_VC_AMOUNT doesn't need fixing here (see the comment in GET_LOCAL_PLAYER_VC_AMOUNT).
	INT iWalletCashLeft = GET_LOCAL_PLAYER_VC_AMOUNT(FALSE) - GET_TOTAL_CASH_I_HAVE_BET() - iActivityCost  // Activity cost is deducted from wallet first
	
	//IF NOT bForSelectedPlayer
	//iResult -= MPGlobals.g_MPBettingData.iBetOnCoronaPlayer
	//ENDIF
	
	RETURN IMAX(0, iWalletCashLeft)
ENDFUNC


FUNC INT IS_PLAYER_ATTEMPTING_TO_CHANGE_BET(TIME_DATATYPE &buttonTimer, INT iBettingIndex, INT iCurrentSelection = 0)

	INT iOddN
	INT iOddD
	
	IF iBettingIndex >= 0
		GET_PLAYER_BETTING_ODDS(iBettingIndex, iOddN, iOddD)
	ENDIF

	// Exit early if we have yet to get odds for this player
	IF iOddN = 0
	OR (GET_CLIENT_BETTING_STATE() != MP_BETTING_PLACING_BETS)
		PRINTLN("HAS_PLAYER_CHANGED_CURRENT_BET - RETURN FALSE, iBettingIndex = ", iBettingIndex)
		IF iOddN = 0
			PRINTLN("		iOddN = 0")
		ENDIF
		
		RETURN 0
	ENDIF
	
	// If changing betting value, listen for correct inputs	
	// Change to control pressed to quickly up values
	INT iIncrement
	BOOL bButtonPressed

	// If value should change, change it.
	IF SHOULD_SELECTION_BE_INCREMENTED(buttonTimer, iIncrement, bButtonPressed, TRUE, FALSE, 100, TRUE, iCurrentSelection)
		RETURN iIncrement
	ENDIF

	RETURN 0
ENDFUNC

FUNC INT APPLY_BETTING_BOUNDS_TO_VALUE(INT iValue, INT iAvailableCash, BOOL bClampInsteadOfLoop = FALSE,  INT iAvailableCashOnPlayer = -1)

	// LOOP
	IF NOT bClampInsteadOfLoop
		
		// Cap our bet to the tunable bounds
		IF iValue < 0
						
			IF iAvailableCash > ROUND(g_sMPTunables.fMaxBetLimit)
			#IF IS_DEBUG_BUILD
			AND NOT g_bDebugBettingAllowUnlimitedBets
			#ENDIF			
				iValue = ROUND(g_sMPTunables.fMaxBetLimit)
			ELSE
				iValue = FLOOR(iAvailableCash / 100.0) * 100	// have to floor this as this could get rounded up to more than what you actually have
			ENDIF
			
			IF iAvailableCashOnPlayer != -1
				
				IF iValue > iAvailableCashOnPlayer
					iValue = iAvailableCashOnPlayer
				ENDIF
			ENDIF
		ENDIF
		
		// loop back around to zero
		IF (iValue > g_sMPTunables.fMaxBetLimit
		OR iValue > iAvailableCash
		OR iValue > iAvailableCashOnPlayer)
		
		#IF IS_DEBUG_BUILD
		AND NOT g_bDebugBettingAllowUnlimitedBets
		#ENDIF
		
			iValue = 0
		ENDIF
		
	ELSE
	
		INT iUpper
		
		IF iAvailableCash > ROUND(g_sMPTunables.fMaxBetLimit)
			iUpper = ROUND(g_sMPTunables.fMaxBetLimit)
		ELSE
			iUpper = FLOOR(iAvailableCash / 100.0) * 100	// have to floor this as this could get rounded up to more than what you actually have
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF g_bDebugBettingAllowUnlimitedBets
			iUpper = iAvailableCash
		ENDIF
		#ENDIF
	
		iValue = CLAMP_INT(iValue, 0, iUpper)
	
	ENDIF

	RETURN IMAX(0, iValue) // IMAX - never let it go below 0
ENDFUNC

FUNC INT GET_CASH_AVAILABLE_TO_BET_ON_THIS_BETTING_INDEX(INT iBettingIndex)
	RETURN (ROUND(g_sMPTunables.fMaxbettotalamountonasingleplayer) - (GlobalServerBD_Betting.activeBettingData[iBettingIndex].iCashBetOnPlayer - GET_LOCAL_CASH_BET_ON_PLAYER(iBettingIndex)))
ENDFUNC

FUNC BOOL CHANGE_PLAYERS_CURRENT_BET(INT iBettingIndex, INT iIncrement, INT &iFailReason, INT iMyBettingIndex = -1)

	iFailReason = 0

	INT iDeltaBet = GET_CASH_DELTA_VALUE()
	INT iAvailableCash
	INT iCashAvailableOnIndex = GET_CASH_AVAILABLE_TO_BET_ON_THIS_BETTING_INDEX(iBettingIndex)	// How much left to bet on this player
	INT iCurrentBetOnIndex = GET_LOCAL_CASH_BET_ON_PLAYER(iBettingIndex)

	// Cash for betting = Wallet cash - cash bet on others - activity cost
	iAvailableCash = GET_CORONA_AVAILABLE_BETTING_CASH() + GET_LOCAL_CASH_BET_ON_PLAYER(iBettingIndex) //iBettingIndex) //, TRUE)

	// If we are looking at ourselves then increase cash available by the gift if there is one
	IF iMyBettingIndex != -1
		iAvailableCash += GET_BETTING_GIFT_VALUE()
	ENDIF

	iDeltaBet = (iIncrement * GET_CASH_DELTA_VALUE())

	// Calculate the new total
	INT iBetAmount = (MPGlobals.g_MPBettingData.iBetOnCoronaPlayer + iDeltaBet)
	iBetAmount = APPLY_BETTING_BOUNDS_TO_VALUE(iBetAmount, iAvailableCash, FALSE, iCashAvailableOnIndex)

	PRINTLN("HAS_PLAYER_CHANGED_CURRENT_BET **************************************************")
	PRINTLN("HAS_PLAYER_CHANGED_CURRENT_BET - SHOULD_SELECTION_BE_INCREMENTED - button press")
	PRINTLN("HAS_PLAYER_CHANGED_CURRENT_BET - iAvailableCash = $", iAvailableCash)
	PRINTLN("HAS_PLAYER_CHANGED_CURRENT_BET - iCashAvailableOnIndex = $", iCashAvailableOnIndex)
	PRINTLN("HAS_PLAYER_CHANGED_CURRENT_BET - iCurrentBetOnIndex = $", iCurrentBetOnIndex)
	PRINTLN("HAS_PLAYER_CHANGED_CURRENT_BET - iBetAmount = $", iBetAmount)
	PRINTLN("HAS_PLAYER_CHANGED_CURRENT_BET - iBetGiftAmount = $", MPGlobals.g_MPBettingData.iBetGiftAmount)
	
	PRINTLN("HAS_PLAYER_CHANGED_CURRENT_BET **************************************************")
	
	BOOL bCanSpendMoney = TRUE
	
	IF iBetAmount - MPGlobals.g_MPBettingData.iBetGiftAmount > 0
		IF NOT NETWORK_CAN_SPEND_MONEY(iBetAmount - MPGlobals.g_MPBettingData.iBetGiftAmount, FALSE, FALSE, FALSE)
			bCanSpendMoney = FALSE
			PRINTLN("HAS_PLAYER_CHANGED_CURRENT_BET - NETWORK_CAN_SPEND_MONEY(iBetAmount - MPGlobals.g_MPBettingData.iBetGiftAmount, FALSE, FALSE, FALSE) = FALSE")
		ENDIF
	ENDIF
	
	
	// Check that the corrected amount of cash is affordable
	// if they have not paid the job fee then don't allow them to bet
	IF MPGlobals.g_MPBettingData.iBetOnCoronaPlayer != iBetAmount
	AND iBetAmount <= iAvailableCash
	AND (iBetAmount - iCurrentBetOnIndex) <= iCashAvailableOnIndex
	AND bCanSpendMoney
	
		MPGlobals.g_MPBettingData.iBetOnCoronaPlayer = iBetAmount
			
		PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT","HUD_FREEMODE_SOUNDSET")
		RETURN TRUE
	
	#IF IS_DEBUG_BUILD
	ELSE
		
		IF iBetAmount > iAvailableCash
			PRINTLN("HAS_PLAYER_CHANGED_CURRENT_BET - iBetAmount > iAvailableCash")
		ENDIF
		
		IF (iBetAmount - iCurrentBetOnIndex) > iCashAvailableOnIndex
			PRINTLN("HAS_PLAYER_CHANGED_CURRENT_BET - (iBetAmount - iCurrentBetOnIndex) > iCashAvailableOnIndex")
		ENDIF
			
	#ENDIF
		
	ENDIF

	// If we can't place this bet on the player then show why.
	IF iCashAvailableOnIndex = 0
		iFailReason = 1
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_TOGGLED_ODDS()

	IF IS_PAUSE_MENU_ACTIVE()
		RETURN FALSE
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
		TOGGLE_ODDS_DISPLAY()
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE: Resets the betting values
PROC CLEANUP_BETTING_CORONA_SCREEN()	
	MPGlobals.g_MPBettingData.iBetOnCoronaPlayer = 0	
ENDPROC








