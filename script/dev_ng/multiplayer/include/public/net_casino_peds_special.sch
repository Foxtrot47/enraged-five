USING "net_casino_peds_vars.sch"


ENUM CASINO_SPECIAL_PED_AREA
	CASINO_SPECIAL_PED_AREA_INVALID = -1,
	CASINO_SPECIAL_PED_AREA_POKER = 0,
	CASINO_SPECIAL_PED_AREA_BLACKJACK,
	CASINO_SPECIAL_PED_AREA_ROULLETTE,
	CASINO_SPECIAL_PED_AREA_TRACK,
	CASINO_SPECIAL_PED_AREA_SLOTS,
	CASINO_SPECIAL_PED_AREA_HALLWAY,
	CASINO_SPECIAL_PED_AREA_CASHIER,
	CASINO_SPECIAL_PED_AREA_BAR,
	CASINO_SPECIAL_PED_AREA_STORE,
	CASINO_SPECIAL_PED_AREA_LOUNGE,
	CASINO_SPECIAL_PED_AREA_SPIN_WHEEL,
	MAX_CASINO_SPECIAL_PED_AREA
ENDENUM

FUNC STRING GET_CASINO_SPECIAL_PED_NAME_STRING(CASINO_SPECIAL_PED_NAME enumItem)
	SWITCH enumItem
		CASE CASINO_SPECIAL_PED_NAME_CALEB		RETURN "CALEB"	
		CASE CASINO_SPECIAL_PED_NAME_USHI		RETURN "USHI"
		CASE CASINO_SPECIAL_PED_NAME_GABRIEL	RETURN "GABRIEL"
		CASE CASINO_SPECIAL_PED_NAME_VINCE		RETURN "VINCE"
		CASE CASINO_SPECIAL_PED_NAME_DEAN		RETURN "DEAN"
		CASE CASINO_SPECIAL_PED_NAME_CAROL		RETURN "CAROL"
		CASE CASINO_SPECIAL_PED_NAME_BETH		RETURN "BETH"
		CASE CASINO_SPECIAL_PED_NAME_LAUREN		RETURN "LAUREN"
		CASE CASINO_SPECIAL_PED_NAME_TAYLOR		RETURN "TAYLOR"
		CASE CASINO_SPECIAL_PED_NAME_BLANE		RETURN "BLANE"
		CASE CASINO_SPECIAL_PED_NAME_EILEEN		RETURN "EILEEN"
		CASE CASINO_SPECIAL_PED_NAME_CURTIS		RETURN "CURTIS"
		CASE MAX_CASINO_SPECIAL_PED_NAME		RETURN "END"
	ENDSWITCH								
	RETURN " "
ENDFUNC										
	
FUNC STRING GET_CASINO_SPECIAL_PED_AREA_STRING(CASINO_SPECIAL_PED_AREA enumItem)
	SWITCH enumItem
		CASE CASINO_SPECIAL_PED_AREA_POKER		RETURN "POKER"
		CASE CASINO_SPECIAL_PED_AREA_BLACKJACK  RETURN "BLACKJACK"
		CASE CASINO_SPECIAL_PED_AREA_ROULLETTE  RETURN "ROULLETTE"
		CASE CASINO_SPECIAL_PED_AREA_SLOTS		RETURN "SLOTS"
		CASE CASINO_SPECIAL_PED_AREA_TRACK		RETURN "TRACK"
		CASE CASINO_SPECIAL_PED_AREA_HALLWAY	RETURN "HALLWAY"
		CASE CASINO_SPECIAL_PED_AREA_CASHIER	RETURN "CASHIER"
		CASE CASINO_SPECIAL_PED_AREA_BAR		RETURN "BAR"
		CASE CASINO_SPECIAL_PED_AREA_STORE		RETURN "STORE"
		CASE MAX_CASINO_SPECIAL_PED_AREA		RETURN "END"
	ENDSWITCH									
	RETURN " "
ENDFUNC											

FUNC CASINO_SPECIAL_PED_NAME GET_CASINO_SPECIAL_PED_NAME(CASINO_PED_TYPES ePedType)
	SWITCH ePedType
		CASE CASINO_CALEB			RETURN CASINO_SPECIAL_PED_NAME_CALEB
		CASE CASINO_USHI			RETURN CASINO_SPECIAL_PED_NAME_USHI
		CASE CASINO_GABRIEL			RETURN CASINO_SPECIAL_PED_NAME_GABRIEL
		CASE CASINO_VINCE			RETURN CASINO_SPECIAL_PED_NAME_VINCE
		CASE CASINO_DEAN			RETURN CASINO_SPECIAL_PED_NAME_DEAN
		CASE CASINO_CAROL			RETURN CASINO_SPECIAL_PED_NAME_CAROL
		CASE CASINO_BETH			RETURN CASINO_SPECIAL_PED_NAME_BETH
		CASE CASINO_LAUREN			RETURN CASINO_SPECIAL_PED_NAME_LAUREN
		CASE CASINO_TAYLOR			RETURN CASINO_SPECIAL_PED_NAME_TAYLOR
		CASE CASINO_BLANE			RETURN CASINO_SPECIAL_PED_NAME_BLANE
		CASE CASINO_EILEEN			RETURN CASINO_SPECIAL_PED_NAME_EILEEN
		CASE CASINO_CURTIS			RETURN CASINO_SPECIAL_PED_NAME_CURTIS
	ENDSWITCH
	RETURN MAX_CASINO_SPECIAL_PED_NAME
ENDFUNC

PROC GET_CASINO_SPECIAL_PED_VOICE(CASINO_SPECIAL_PED_NAME enumItem, INT &iVoiceNumberID, TEXT_LABEL_23 &sVoiceID)
	SWITCH enumItem
		CASE CASINO_SPECIAL_PED_NAME_CALEB				
			iVoiceNumberID = ConvertSingleCharacter("7")	
			sVoiceID = "CAS_CALEB" 
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_USHI				
			iVoiceNumberID = ConvertSingleCharacter("8")	
			sVoiceID = "CAS_USHI" 
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_GABRIEL				
			iVoiceNumberID = ConvertSingleCharacter("6")	
			sVoiceID = "CAS_GABRIEL" 
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_VINCE				
			iVoiceNumberID = ConvertSingleCharacter("5")	
			sVoiceID = "CAS_VINCE"
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_DEAN				
			iVoiceNumberID = ConvertSingleCharacter("2")	
			sVoiceID = "CAS_DEAN" 
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_CAROL				
			iVoiceNumberID = ConvertSingleCharacter("3")	
			sVoiceID = "CAS_CAROL" 
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_BETH
			iVoiceNumberID = ConvertSingleCharacter("D")	
			sVoiceID = "CAS_BETH"
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_LAUREN //and partner Taylor				
			iVoiceNumberID = ConvertSingleCharacter("B")	
			sVoiceID = "CAS_LAUREN" 
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_TAYLOR				
			iVoiceNumberID = ConvertSingleCharacter("C")	
			sVoiceID = "CAS_TAYLOR" 
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_BLANE				
			iVoiceNumberID = ConvertSingleCharacter("1")	
			sVoiceID = "CAS_BLANE" 
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_EILEEN				
			iVoiceNumberID = ConvertSingleCharacter("4")	
			sVoiceID = "CAS_EILEEN" 
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_CURTIS				
			iVoiceNumberID = ConvertSingleCharacter("A")	
			sVoiceID = "CAS_CURTIS" 
		BREAK
	ENDSWITCH

ENDPROC

CASINO_SPECIAL_PED_SERVER_DATA g_sSpecialPeds[MAX_CASINO_SPECIAL_PED_NAME]

FUNC BOOL IS_CASINO_SPECIAL_PED(INT iPedID)
	INT iSpecialPedLoop
	REPEAT MAX_CASINO_SPECIAL_PED_NAME iSpecialPedLoop
		IF g_sSpecialPeds[iSpecialPedLoop].iPedID = iPedID
			PRINTLN("[CASINO_PEDS] IS_CASINO_SPECIAL_PED - iPedID: ",iPedID," is Special Ped ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPedLoop)))
			RETURN TRUE
		ENDIF
	ENDREPEAT 
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_CASINO_SPECIAL_PED_ACTIVITY(CASINO_ACTIVITY_SLOTS eActivity)
	SWITCH eActivity
		CASE CASINO_SPECIAL_PED_VINCE_BAR
		CASE CASINO_SPECIAL_PED_VINCE_STEPS
		CASE CASINO_SPECIAL_PED_VINCE_WALL
		CASE CASINO_SPECIAL_PED_CAROL_SHOP
		CASE CASINO_SPECIAL_PED_CAROL_SLOTS
		CASE CASINO_SPECIAL_PED_DEAN_SHOP
		CASE CASINO_SPECIAL_PED_DEAN_SLOTS
		CASE CASINO_SPECIAL_PED_BETH_LOUNGE
		CASE CASINO_SPECIAL_PED_BETH_WHEEL
		CASE CASINO_SPECIAL_PED_LAUREN_BAR
		CASE CASINO_SPECIAL_PED_TAYLOR_BAR
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_CASINO_SPECIAL_PED_BE_IN_AREA(CASINO_SPECIAL_PED_NAME ePed, CASINO_SPECIAL_PED_AREA eArea, INT iReservedBlackjackTable, INT iReservedThreeCardPokerTable, INT iReservedRouletteTable)
//	PRINTLN("[CASINO_PEDS] CAN_CASINO_SPECIAL_PED_BE_IN_AREA - ePed: ",GET_CASINO_SPECIAL_PED_NAME_STRING(ePed)," eArea: ",GET_CASINO_SPECIAL_PED_AREA_STRING(eArea))
	SWITCH ePed
		CASE CASINO_SPECIAL_PED_NAME_CALEB
			SWITCH eArea
				CASE CASINO_SPECIAL_PED_AREA_BLACKJACK
					RETURN iReservedBlackjackTable <= 1
				CASE CASINO_SPECIAL_PED_AREA_POKER
					RETURN iReservedThreeCardPokerTable <= 1
			ENDSWITCH
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_USHI
		CASE CASINO_SPECIAL_PED_NAME_GABRIEL
			SWITCH eArea
				CASE CASINO_SPECIAL_PED_AREA_POKER
					RETURN iReservedThreeCardPokerTable >= 2
				CASE CASINO_SPECIAL_PED_AREA_ROULLETTE
					RETURN iReservedRouletteTable >= 2
			ENDSWITCH
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_VINCE
			SWITCH eArea
				CASE CASINO_SPECIAL_PED_AREA_CASHIER
					IF NOT g_sMPTunables.bDisable_Christmas_Tree_Apartment
						RETURN FALSE
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_DEAN
		CASE CASINO_SPECIAL_PED_NAME_CAROL
			SWITCH eArea
				CASE CASINO_SPECIAL_PED_AREA_BLACKJACK
					RETURN iReservedBlackjackTable <= 1
			ENDSWITCH
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_BETH
			SWITCH eArea
				CASE CASINO_SPECIAL_PED_AREA_BLACKJACK
					RETURN iReservedBlackjackTable <= 1
			ENDSWITCH
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_LAUREN
		CASE CASINO_SPECIAL_PED_NAME_TAYLOR
			SWITCH eArea
				CASE CASINO_SPECIAL_PED_AREA_BLACKJACK
					RETURN iReservedBlackjackTable <= 1
				CASE CASINO_SPECIAL_PED_AREA_ROULLETTE
					RETURN iReservedRouletteTable <= 1
			ENDSWITCH
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_BLANE
			SWITCH eArea
				CASE CASINO_SPECIAL_PED_AREA_POKER
					RETURN iReservedThreeCardPokerTable <= 1
				CASE CASINO_SPECIAL_PED_AREA_ROULLETTE
					RETURN iReservedRouletteTable <= 1
			ENDSWITCH
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_EILEEN
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_CURTIS
			SWITCH eArea
				CASE CASINO_SPECIAL_PED_AREA_POKER
					RETURN iReservedThreeCardPokerTable >= 2
				CASE CASINO_SPECIAL_PED_AREA_ROULLETTE
					RETURN iReservedRouletteTable >= 2
				CASE CASINO_SPECIAL_PED_AREA_BLACKJACK
					RETURN iReservedBlackjackTable >= 2
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN TRUE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC GET_CASINO_SPECIAL_PED_SCENARIO_TEXT_LABEL(TEXT_LABEL_63 &tlScenario[], CASINO_SPECIAL_PED_NAME eSpecialPed)
	tlScenario[0] = ""
	tlScenario[1] = ""
	tlScenario[2] = ""
	SWITCH eSpecialPed
		CASE CASINO_SPECIAL_PED_NAME_BETH
			tlScenario[0] = "Scenario 1: Sat Behind Bar"
			tlScenario[1] = "Scenario 2: Looking at Big Wheel"
			tlScenario[2] = "Scenario 3: Regular Blackjack"
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_BLANE
			tlScenario[0] = "Scenario 1: Regular Roulette"
			tlScenario[1] = "Scenario 2: Regular Blackjack"
			tlScenario[2] = "Scenario 3: Inside Track"
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_CALEB
			tlScenario[0] = "Scenario 1: Regular Three Card Poker"
			tlScenario[1] = "Scenario 2: Regular Blackjack"
			tlScenario[2] = "Scenario 3: Slot Machines"
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_CURTIS
			tlScenario[0] = "Scenario 1: High End Three Card Poker"
			tlScenario[1] = "Scenario 2: High End Roulette"
			tlScenario[2] = "Scenario 3: High End Blackjack"
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_EILEEN
			tlScenario[0] = "Scenario 1: Slot Machine Pos 1"
			tlScenario[1] = "Scenario 2: Slot Machine Pos 2"
			tlScenario[2] = "Scenario 3: Slot Machine Pos 3"
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_GABRIEL
			tlScenario[0] = "Scenario 1: High End Three Card Poker"
			tlScenario[1] = "Scenario 2: High End Roulette"
			tlScenario[2] = "Scenario 3: At the Bar"
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_USHI
			tlScenario[0] = "Scenario 1: High End Three Card Poker"
			tlScenario[1] = "Scenario 2: High End Roulette"
			tlScenario[2] = "Scenario 3: Inside Track"
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_VINCE
			tlScenario[0] = "Scenario 1: At the Bar"
			tlScenario[1] = "Scenario 2: Left of Cashier Cage"
			tlScenario[2] = "Scenario 3: Entrance Hallway"
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_DEAN
		CASE CASINO_SPECIAL_PED_NAME_CAROL
			tlScenario[0] = "Scenario 1: Casino Store"
			tlScenario[1] = "Scenario 2: Slot Machines"
			tlScenario[2] = "Scenario 3: Regular Blackjack"
		BREAK
		CASE CASINO_SPECIAL_PED_NAME_LAUREN
		CASE CASINO_SPECIAL_PED_NAME_TAYLOR
			tlScenario[0] = "Scenario 1: At the Bar"
			tlScenario[1] = "Scenario 2: Regular Blackjack"
			tlScenario[2] = "Scenario 3: Regular Roulette"
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF

PROC SET_CASINO_SPECIAL_PEDS_SEATS(INT &iSpecialPeds[], INT iReservedBlackjackTable, INT iReservedThreeCardPokerTable, INT iReservedRouletteTable)
#IF IS_DEBUG_BUILD
	// g_bSpecialPedsKeep value is set on Widget
	// g_iSpecialPeds is persistent, this won't be wiped when the am_casino_peds is cleaned 
	IF g_bCasinoSpecialPedsKeep
		PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Using Special Peds previous configuration")
		INT iPed
		REPEAT MAX_CASINO_SPECIAL_PED_NAME iPed
			iSpecialPeds[iPed] = g_iCasinoSpecialPeds[iPed]
		ENDREPEAT
		EXIT
	ENDIF
#ENDIF
	
	BOOL 					bAreaOcupied[MAX_CASINO_SPECIAL_PED_AREA] // [poker, blackjack, roullete, slots, track, hallway, cashier]
	CASINO_SPECIAL_PED_AREA eAreaAvailable[3]
	INT						iArea
	INT						iAreaLoop
	INT						iPosition
	INT						iPed
	INT 					iSpecialPed
	INT 					iSpecialPedLoop
	INT						iSpecialPedOffset = GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(MAX_CASINO_SPECIAL_PED_NAME)) // this is to not start always by Caleb
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63			tlActiveScenario[3]
	#ENDIF
	
	//	PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - MAX_CASINO_SPECIAL_PED_NAME ",ENUM_TO_INT(MAX_CASINO_SPECIAL_PED_NAME) )
	REPEAT MAX_CASINO_SPECIAL_PED_NAME iSpecialPedLoop
		iSpecialPed = (iSpecialPedLoop+iSpecialPedOffset) % ENUM_TO_INT(MAX_CASINO_SPECIAL_PED_NAME)
		
		SWITCH INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed)
			CASE CASINO_SPECIAL_PED_NAME_CALEB
				eAreaAvailable[0] = CASINO_SPECIAL_PED_AREA_POKER
				eAreaAvailable[1] = CASINO_SPECIAL_PED_AREA_BLACKJACK
				eAreaAvailable[2] = CASINO_SPECIAL_PED_AREA_SLOTS
				iArea = GET_RANDOM_INT_IN_RANGE(0,3)
				
				#IF IS_DEBUG_BUILD
				IF g_bCasinoSpecialPedScenarios[iSpecialPed*3]
					g_bCasinoSpecialPedScenarios[iSpecialPed*3] = FALSE
					iArea = 0
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1] = FALSE
					iArea = 1
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2] = FALSE
					iArea = 2
				ENDIF
				#ENDIF
				
				REPEAT 3 iAreaLoop
					IF  CAN_CASINO_SPECIAL_PED_BE_IN_AREA(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed),eAreaAvailable[iArea], iReservedBlackjackTable, iReservedThreeCardPokerTable, iReservedRouletteTable)
					AND NOT bAreaOcupied[eAreaAvailable[iArea]]
						SWITCH eAreaAvailable[iArea]
							CASE CASINO_SPECIAL_PED_AREA_POKER								// poker [84-87]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,4)
								iPed = 84+iPosition
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_BLACKJACK							// blackjack [72-75]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,4)
								iPed = 72+iPosition
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_SLOTS								// slots [5,77,78]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,3)
								IF iPosition = 0
									iPed = 5
								ELIF iPosition = 1
									iPed = 77
								ELSE
									iPed = 78
								ENDIF
							BREAK
						ENDSWITCH
						IF eAreaAvailable[iArea] <= CASINO_SPECIAL_PED_AREA_TRACK
							bAreaOcupied[eAreaAvailable[iArea]] = TRUE
						ENDIF
//						PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPed))," Area:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea]))," Pos: ",iPed)
						iSpecialPeds[iSpecialPed] = iPed
						BREAKLOOP
					ENDIF
//					PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPed))," Area ocupied:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea])))
					iArea = (iArea + 1) % 3
				ENDREPEAT
				
				#IF IS_DEBUG_BUILD
				GET_CASINO_SPECIAL_PED_SCENARIO_TEXT_LABEL(tlActiveScenario, INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed))
				SWITCH iArea
					CASE 0	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[0]	BREAK
					CASE 1	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[1]	BREAK
					CASE 2	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[2]	BREAK
				ENDSWITCH
				#ENDIF
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_USHI
				eAreaAvailable[0] = CASINO_SPECIAL_PED_AREA_POKER
				eAreaAvailable[1] = CASINO_SPECIAL_PED_AREA_ROULLETTE
				eAreaAvailable[2] = CASINO_SPECIAL_PED_AREA_TRACK
				iArea = GET_RANDOM_INT_IN_RANGE(0,3)
				
				#IF IS_DEBUG_BUILD
				IF g_bCasinoSpecialPedScenarios[iSpecialPed*3]
					g_bCasinoSpecialPedScenarios[iSpecialPed*3] = FALSE
					iArea = 0
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1] = FALSE
					iArea = 1
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2] = FALSE
					iArea = 2
				ENDIF
				#ENDIF
				
				REPEAT 3 iAreaLoop
					IF  CAN_CASINO_SPECIAL_PED_BE_IN_AREA(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed),eAreaAvailable[iArea], iReservedBlackjackTable, iReservedThreeCardPokerTable, iReservedRouletteTable)
					AND NOT bAreaOcupied[eAreaAvailable[iArea]]
						SWITCH eAreaAvailable[iArea]
							CASE CASINO_SPECIAL_PED_AREA_POKER								// poker [84-87]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,4)
								iPed = 84+iPosition
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_ROULLETTE							// roullette [78-81]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,4)
								iPed = 78+iPosition
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_TRACK								// track [48]
								iPed = 48
							BREAK
						ENDSWITCH
						IF eAreaAvailable[iArea] <= CASINO_SPECIAL_PED_AREA_TRACK
							bAreaOcupied[eAreaAvailable[iArea]] = TRUE
						ENDIF
//						PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPed))," Area:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea]))," Pos: ",iPed)
						iSpecialPeds[iSpecialPed] = iPed
						BREAKLOOP
					ENDIF
//					PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPed))," Area ocupied:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea])))
					iArea = (iArea + 1) % 3
				ENDREPEAT
				
				#IF IS_DEBUG_BUILD
				GET_CASINO_SPECIAL_PED_SCENARIO_TEXT_LABEL(tlActiveScenario, INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed))
				SWITCH iArea
					CASE 0	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[0]	BREAK
					CASE 1	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[1]	BREAK
					CASE 2	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[2]	BREAK
				ENDSWITCH
				#ENDIF
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_GABRIEL
				eAreaAvailable[0] = CASINO_SPECIAL_PED_AREA_POKER
				eAreaAvailable[1] = CASINO_SPECIAL_PED_AREA_ROULLETTE
				eAreaAvailable[2] = CASINO_SPECIAL_PED_AREA_BAR
				iArea = GET_RANDOM_INT_IN_RANGE(0,3)
				
				#IF IS_DEBUG_BUILD
				IF g_bCasinoSpecialPedScenarios[iSpecialPed*3]
					g_bCasinoSpecialPedScenarios[iSpecialPed*3] = FALSE
					iArea = 0
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1] = FALSE
					iArea = 1
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2] = FALSE
					iArea = 2
				ENDIF
				#ENDIF
				
				REPEAT 3 iAreaLoop
					IF  CAN_CASINO_SPECIAL_PED_BE_IN_AREA(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed),eAreaAvailable[iArea], iReservedBlackjackTable, iReservedThreeCardPokerTable, iReservedRouletteTable)
					AND NOT bAreaOcupied[eAreaAvailable[iArea]]
						SWITCH eAreaAvailable[iArea]
							CASE CASINO_SPECIAL_PED_AREA_POKER								// poker [84-87]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,4)
								iPed = 84+iPosition
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_ROULLETTE							// roullette [78-81]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,4)
								iPed = 78+iPosition
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_BAR								// bar [56]
								iPed = 56
							BREAK
						ENDSWITCH
						IF eAreaAvailable[iArea] <= CASINO_SPECIAL_PED_AREA_TRACK
							bAreaOcupied[eAreaAvailable[iArea]] = TRUE
						ENDIF
//						PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPed))," Area:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea]))," Pos: ",iPed)
						iSpecialPeds[iSpecialPed] = iPed
						BREAKLOOP
					ENDIF
//					PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPed))," Area ocupied:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea])))
					iArea = (iArea + 1) % 3
				ENDREPEAT
				
				#IF IS_DEBUG_BUILD
				GET_CASINO_SPECIAL_PED_SCENARIO_TEXT_LABEL(tlActiveScenario, INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed))
				SWITCH iArea
					CASE 0	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[0]	BREAK
					CASE 1	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[1]	BREAK
					CASE 2	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[2]	BREAK
				ENDSWITCH
				#ENDIF
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_VINCE
				eAreaAvailable[0] = CASINO_SPECIAL_PED_AREA_BAR
				eAreaAvailable[1] = CASINO_SPECIAL_PED_AREA_CASHIER
				eAreaAvailable[2] = CASINO_SPECIAL_PED_AREA_HALLWAY
				iArea = GET_RANDOM_INT_IN_RANGE(0,3)
				
				#IF IS_DEBUG_BUILD
				IF g_bCasinoSpecialPedScenarios[iSpecialPed*3]
					g_bCasinoSpecialPedScenarios[iSpecialPed*3] = FALSE
					iArea = 0
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1] = FALSE
					iArea = 1
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2] = FALSE
					iArea = 2
				ENDIF
				#ENDIF
				
				REPEAT 3 iAreaLoop
					IF  CAN_CASINO_SPECIAL_PED_BE_IN_AREA(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed),eAreaAvailable[iArea], iReservedBlackjackTable, iReservedThreeCardPokerTable, iReservedRouletteTable)
					AND NOT bAreaOcupied[eAreaAvailable[iArea]]
						SWITCH eAreaAvailable[iArea]
							CASE CASINO_SPECIAL_PED_AREA_BAR								// bar [55]
								iPed = 55
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_CASHIER							// cashier [23]
								iPed = 23
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_HALLWAY							// hallway [67]
								iPed = 67
							BREAK
						ENDSWITCH
						IF eAreaAvailable[iArea] <= CASINO_SPECIAL_PED_AREA_TRACK
							bAreaOcupied[eAreaAvailable[iArea]] = TRUE
						ENDIF
//						PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPed))," Area:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea]))," Pos: ",iPed)
						iSpecialPeds[iSpecialPed] = iPed
						BREAKLOOP
					ENDIF
//					PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPed))," Area ocupied:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea])))
					iArea = (iArea + 1) % 3
				ENDREPEAT
				
				#IF IS_DEBUG_BUILD
				GET_CASINO_SPECIAL_PED_SCENARIO_TEXT_LABEL(tlActiveScenario, INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed))
				SWITCH iArea
					CASE 0	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[0]	BREAK
					CASE 1	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[1]	BREAK
					CASE 2	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[2]	BREAK
				ENDSWITCH
				#ENDIF
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_DEAN
				eAreaAvailable[0] = CASINO_SPECIAL_PED_AREA_STORE
				eAreaAvailable[1] = CASINO_SPECIAL_PED_AREA_SLOTS
				eAreaAvailable[2] = CASINO_SPECIAL_PED_AREA_BLACKJACK
				iArea = GET_RANDOM_INT_IN_RANGE(0,3)
				
				#IF IS_DEBUG_BUILD
				IF g_bCasinoSpecialPedScenarios[iSpecialPed*3]
					g_bCasinoSpecialPedScenarios[iSpecialPed*3] = FALSE
					iArea = 0
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1] = FALSE
					iArea = 1
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2] = FALSE
					iArea = 2
				ENDIF
				#ENDIF
				
				REPEAT 3 iAreaLoop
					IF CAN_CASINO_SPECIAL_PED_BE_IN_AREA(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed),eAreaAvailable[iArea], iReservedBlackjackTable, iReservedThreeCardPokerTable, iReservedRouletteTable)
					AND NOT bAreaOcupied[eAreaAvailable[iArea]]
						SWITCH eAreaAvailable[iArea]
							CASE CASINO_SPECIAL_PED_AREA_STORE								// store [40,98]
								iSpecialPeds[CASINO_SPECIAL_PED_NAME_DEAN] = 98
								iSpecialPeds[CASINO_SPECIAL_PED_NAME_CAROL] = 40
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_SLOTS								// slots [80,37]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,2)
								IF iPosition = 0
									iSpecialPeds[CASINO_SPECIAL_PED_NAME_DEAN] = 80
									iSpecialPeds[CASINO_SPECIAL_PED_NAME_CAROL] = 12
								ELSE
									iSpecialPeds[CASINO_SPECIAL_PED_NAME_DEAN] = 37
									iSpecialPeds[CASINO_SPECIAL_PED_NAME_CAROL] = 12
								ENDIF
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_BLACKJACK							// blackjack [72-75]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,3)
								IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
									iSpecialPeds[CASINO_SPECIAL_PED_NAME_DEAN] = 72+iPosition
									iSpecialPeds[CASINO_SPECIAL_PED_NAME_CAROL] = 72+iPosition+1
								ELSE
									iSpecialPeds[CASINO_SPECIAL_PED_NAME_DEAN] = 72+iPosition+1
									iSpecialPeds[CASINO_SPECIAL_PED_NAME_CAROL] = 72+iPosition
								ENDIF
							BREAK
						ENDSWITCH
						IF eAreaAvailable[iArea] <= CASINO_SPECIAL_PED_AREA_TRACK
							bAreaOcupied[eAreaAvailable[iArea]] = TRUE
						ENDIF
//						PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(CASINO_SPECIAL_PED_NAME_DEAN)," Area:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea]))," Pos: ",iSpecialPeds[CASINO_SPECIAL_PED_NAME_DEAN])
//						PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(CASINO_SPECIAL_PED_NAME_CAROL)," Area:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea]))," Pos: ",iSpecialPeds[CASINO_SPECIAL_PED_NAME_CAROL])
						BREAKLOOP
					ENDIF
//					PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(CASINO_SPECIAL_PED_NAME_DEAN)," Area ocupied:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea])))
//					PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(CASINO_SPECIAL_PED_NAME_CAROL)," Area ocupied:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea])))
					iArea = (iArea + 1) % 3
				ENDREPEAT
				
				#IF IS_DEBUG_BUILD
				GET_CASINO_SPECIAL_PED_SCENARIO_TEXT_LABEL(tlActiveScenario, INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed))
				SWITCH iArea
					CASE 0	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[0]	BREAK
					CASE 1	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[1]	BREAK
					CASE 2	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[2]	BREAK
				ENDSWITCH
				#ENDIF
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_CAROL
				//do nothing, Dean case sets Carol
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_BETH
				eAreaAvailable[0] = CASINO_SPECIAL_PED_AREA_LOUNGE
				eAreaAvailable[1] = CASINO_SPECIAL_PED_AREA_SPIN_WHEEL
				eAreaAvailable[2] = CASINO_SPECIAL_PED_AREA_BLACKJACK
				iArea = GET_RANDOM_INT_IN_RANGE(0,3)
				
				#IF IS_DEBUG_BUILD
				IF g_bCasinoSpecialPedScenarios[iSpecialPed*3]
					g_bCasinoSpecialPedScenarios[iSpecialPed*3] = FALSE
					iArea = 0
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1] = FALSE
					iArea = 1
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2] = FALSE
					iArea = 2
				ENDIF
				#ENDIF
				
				REPEAT 3 iAreaLoop
					IF  CAN_CASINO_SPECIAL_PED_BE_IN_AREA(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed),eAreaAvailable[iArea], iReservedBlackjackTable, iReservedThreeCardPokerTable, iReservedRouletteTable)
					AND NOT bAreaOcupied[eAreaAvailable[iArea]]
						SWITCH eAreaAvailable[iArea]
							CASE CASINO_SPECIAL_PED_AREA_LOUNGE								// lounge [41]
								iPed = 41
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_SPIN_WHEEL							// spin wheel [54]
								iPed = 54
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_BLACKJACK							// blackjack [72-75]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,4)
								iPed = 72+iPosition
							BREAK
						ENDSWITCH
						IF eAreaAvailable[iArea] <= CASINO_SPECIAL_PED_AREA_TRACK
							bAreaOcupied[eAreaAvailable[iArea]] = TRUE
						ENDIF
//						PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPed))," Area:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea]))," Pos: ",iPed)
						iSpecialPeds[iSpecialPed] = iPed
						BREAKLOOP
					ENDIF
//					PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPed))," Area ocupied:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea])))
					iArea = (iArea + 1) % 3
				ENDREPEAT
				
				#IF IS_DEBUG_BUILD
				GET_CASINO_SPECIAL_PED_SCENARIO_TEXT_LABEL(tlActiveScenario, INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed))
				SWITCH iArea
					CASE 0	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[0]	BREAK
					CASE 1	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[1]	BREAK
					CASE 2	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[2]	BREAK
				ENDSWITCH
				#ENDIF
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_LAUREN
				eAreaAvailable[0] = CASINO_SPECIAL_PED_AREA_BAR
				eAreaAvailable[1] = CASINO_SPECIAL_PED_AREA_BLACKJACK
				eAreaAvailable[2] = CASINO_SPECIAL_PED_AREA_ROULLETTE
				iArea = GET_RANDOM_INT_IN_RANGE(0,3)
				
				#IF IS_DEBUG_BUILD
				IF g_bCasinoSpecialPedScenarios[iSpecialPed*3]
					g_bCasinoSpecialPedScenarios[iSpecialPed*3] = FALSE
					iArea = 0
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1] = FALSE
					iArea = 1
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2] = FALSE
					iArea = 2
				ENDIF
				#ENDIF
				
				REPEAT 3 iAreaLoop
					IF  CAN_CASINO_SPECIAL_PED_BE_IN_AREA(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed),eAreaAvailable[iArea], iReservedBlackjackTable, iReservedThreeCardPokerTable, iReservedRouletteTable)
					AND NOT bAreaOcupied[eAreaAvailable[iArea]]
						SWITCH eAreaAvailable[iArea]
							CASE CASINO_SPECIAL_PED_AREA_BAR									// bar [96,109]
								iSpecialPeds[CASINO_SPECIAL_PED_NAME_LAUREN] = 109
								iSpecialPeds[CASINO_SPECIAL_PED_NAME_TAYLOR] = 96
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_BLACKJACK								// blackjack [72-75]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,3)
								IF GET_RANDOM_INT_IN_RANGE(0,2) = 0
									iSpecialPeds[CASINO_SPECIAL_PED_NAME_LAUREN] = 72+iPosition
									iSpecialPeds[CASINO_SPECIAL_PED_NAME_TAYLOR] = 72+iPosition+1
								ELSE
									iSpecialPeds[CASINO_SPECIAL_PED_NAME_LAUREN] = 72+iPosition+1
									iSpecialPeds[CASINO_SPECIAL_PED_NAME_TAYLOR] = 72+iPosition
								ENDIF
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_ROULLETTE								// roulette [78,79]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,2)
								IF iPosition = 0
									iSpecialPeds[CASINO_SPECIAL_PED_NAME_LAUREN] = 79	// Seat 3
									iSpecialPeds[CASINO_SPECIAL_PED_NAME_TAYLOR] = 78	// Seat 4
								ELSE
									iSpecialPeds[CASINO_SPECIAL_PED_NAME_TAYLOR] = 79	// Seat 3
									iSpecialPeds[CASINO_SPECIAL_PED_NAME_LAUREN] = 78	// Seat 4
								ENDIF
							BREAK
						ENDSWITCH
						IF eAreaAvailable[iArea] <= CASINO_SPECIAL_PED_AREA_TRACK
							bAreaOcupied[eAreaAvailable[iArea]] = TRUE
						ENDIF
//						PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(CASINO_SPECIAL_PED_NAME_LAUREN)," Area:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea]))," Pos: ",iSpecialPeds[CASINO_SPECIAL_PED_NAME_LAUREN])
//						PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(CASINO_SPECIAL_PED_NAME_TAYLOR)," Area:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea]))," Pos: ",iSpecialPeds[CASINO_SPECIAL_PED_NAME_TAYLOR])
						BREAKLOOP
					ENDIF
//					PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(CASINO_SPECIAL_PED_NAME_LAUREN)," Area ocupied:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea])))
//					PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(CASINO_SPECIAL_PED_NAME_TAYLOR)," Area ocupied:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea])))
					iArea = (iArea + 1) % 3
				ENDREPEAT
				
				#IF IS_DEBUG_BUILD
				GET_CASINO_SPECIAL_PED_SCENARIO_TEXT_LABEL(tlActiveScenario, INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed))
				SWITCH iArea
					CASE 0	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[0]	BREAK
					CASE 1	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[1]	BREAK
					CASE 2	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[2]	BREAK
				ENDSWITCH
				#ENDIF
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_TAYLOR
				//do nothing, Lauren case sets Taylor
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_BLANE
				eAreaAvailable[0] = CASINO_SPECIAL_PED_AREA_ROULLETTE
				eAreaAvailable[1] = CASINO_SPECIAL_PED_AREA_POKER
				eAreaAvailable[2] = CASINO_SPECIAL_PED_AREA_TRACK
				iArea = GET_RANDOM_INT_IN_RANGE(0,3)
				
				#IF IS_DEBUG_BUILD
				IF g_bCasinoSpecialPedScenarios[iSpecialPed*3]
					g_bCasinoSpecialPedScenarios[iSpecialPed*3] = FALSE
					iArea = 0
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1] = FALSE
					iArea = 1
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2] = FALSE
					iArea = 2
				ENDIF
				#ENDIF
				
				REPEAT 3 iAreaLoop
					IF  CAN_CASINO_SPECIAL_PED_BE_IN_AREA(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed),eAreaAvailable[iArea], iReservedBlackjackTable, iReservedThreeCardPokerTable, iReservedRouletteTable)
					AND NOT bAreaOcupied[eAreaAvailable[iArea]]
						SWITCH eAreaAvailable[iArea]
							CASE CASINO_SPECIAL_PED_AREA_ROULLETTE							// roullette [78-81]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,4)
								iPed = 78+iPosition
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_POKER								// poker [84-87]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,4)
								iPed = 84+iPosition
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_TRACK								// track [49]
								iPed = 49
							BREAK
						ENDSWITCH
						IF eAreaAvailable[iArea] <= CASINO_SPECIAL_PED_AREA_TRACK
							bAreaOcupied[eAreaAvailable[iArea]] = TRUE
						ENDIF
//						PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPed))," Area:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea]))," Pos: ",iPed)
						iSpecialPeds[iSpecialPed] = iPed
						BREAKLOOP
					ENDIF
//					PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPed))," Area ocupied:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea])))
					iArea = (iArea + 1) % 3
				ENDREPEAT
				
				#IF IS_DEBUG_BUILD
				GET_CASINO_SPECIAL_PED_SCENARIO_TEXT_LABEL(tlActiveScenario, INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed))
				SWITCH iArea
					CASE 0	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[0]	BREAK
					CASE 1	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[1]	BREAK
					CASE 2	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[2]	BREAK
				ENDSWITCH
				#ENDIF
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_EILEEN
				eAreaAvailable[0] = CASINO_SPECIAL_PED_AREA_SLOTS
				eAreaAvailable[1] = CASINO_SPECIAL_PED_AREA_SLOTS
				eAreaAvailable[2] = CASINO_SPECIAL_PED_AREA_SLOTS
				iArea = GET_RANDOM_INT_IN_RANGE(0,3)
				
				#IF IS_DEBUG_BUILD
				IF g_bCasinoSpecialPedScenarios[iSpecialPed*3]
					g_bCasinoSpecialPedScenarios[iSpecialPed*3] = FALSE
					iArea = 0
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1] = FALSE
					iArea = 1
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2] = FALSE
					iArea = 2
				ENDIF
				#ENDIF
				
				REPEAT 3 iAreaLoop
					IF  CAN_CASINO_SPECIAL_PED_BE_IN_AREA(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed),eAreaAvailable[iArea], iReservedBlackjackTable, iReservedThreeCardPokerTable, iReservedRouletteTable)
					AND NOT bAreaOcupied[eAreaAvailable[iArea]]
						SWITCH eAreaAvailable[iArea]
							CASE CASINO_SPECIAL_PED_AREA_SLOTS							// roullette [35,79,4]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,3)
								IF iPosition = 0
									iPed = 35
								ELIF iPosition = 1
									iPed = 79
								ELSE
									iPed = 4
								ENDIF
							BREAK
						ENDSWITCH
						IF eAreaAvailable[iArea] <= CASINO_SPECIAL_PED_AREA_TRACK
							bAreaOcupied[eAreaAvailable[iArea]] = TRUE
						ENDIF
//						PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPed))," Area:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea]))," Pos: ",iPed)
						iSpecialPeds[iSpecialPed] = iPed
						BREAKLOOP
					ENDIF
//					PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPed))," Area ocupied:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea])))
					iArea = (iArea + 1) % 3
				ENDREPEAT
				
				#IF IS_DEBUG_BUILD
				GET_CASINO_SPECIAL_PED_SCENARIO_TEXT_LABEL(tlActiveScenario, INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed))
				SWITCH iArea
					CASE 0	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[0]	BREAK
					CASE 1	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[1]	BREAK
					CASE 2	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[2]	BREAK
				ENDSWITCH
				#ENDIF
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_CURTIS
				eAreaAvailable[0] = CASINO_SPECIAL_PED_AREA_POKER
				eAreaAvailable[1] = CASINO_SPECIAL_PED_AREA_ROULLETTE
				eAreaAvailable[2] = CASINO_SPECIAL_PED_AREA_BLACKJACK
				iArea = GET_RANDOM_INT_IN_RANGE(0,3)
				
				#IF IS_DEBUG_BUILD
				IF g_bCasinoSpecialPedScenarios[iSpecialPed*3]
					g_bCasinoSpecialPedScenarios[iSpecialPed*3] = FALSE
					iArea = 0
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+1] = FALSE
					iArea = 1
				ELIF g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2]
					g_bCasinoSpecialPedScenarios[(iSpecialPed*3)+2] = FALSE
					iArea = 2
				ENDIF
				#ENDIF
				
				REPEAT 3 iAreaLoop
					IF  CAN_CASINO_SPECIAL_PED_BE_IN_AREA(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed),eAreaAvailable[iArea], iReservedBlackjackTable, iReservedThreeCardPokerTable, iReservedRouletteTable)
					AND NOT bAreaOcupied[eAreaAvailable[iArea]]
						SWITCH eAreaAvailable[iArea]
							CASE CASINO_SPECIAL_PED_AREA_POKER								// poker [84-87]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,4)
								iPed = 84+iPosition
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_ROULLETTE							// roullette [78-81]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,4)
								iPed = 78+iPosition
							BREAK
							CASE CASINO_SPECIAL_PED_AREA_BLACKJACK							// blackjack [72-75]
								iPosition = GET_RANDOM_INT_IN_RANGE(0,4)
								iPed = 72+iPosition
							BREAK
						ENDSWITCH
						IF eAreaAvailable[iArea] <= CASINO_SPECIAL_PED_AREA_TRACK
							bAreaOcupied[eAreaAvailable[iArea]] = TRUE
						ENDIF
//						PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPed))," Area:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea]))," Pos: ",iPed)
						iSpecialPeds[iSpecialPed] = iPed
						BREAKLOOP
					ENDIF
//					PRINTLN("[CASINO_PEDS] SET_CASINO_SPECIAL_PEDS_SEATS - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPed))," Area ocupied:(",iArea,")(",eAreaAvailable[iArea],") ",GET_CASINO_SPECIAL_PED_AREA_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_AREA,eAreaAvailable[iArea])))
					iArea = (iArea + 1) % 3
				ENDREPEAT
				
				#IF IS_DEBUG_BUILD
				GET_CASINO_SPECIAL_PED_SCENARIO_TEXT_LABEL(tlActiveScenario, INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPed))
				SWITCH iArea
					CASE 0	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[0]	BREAK
					CASE 1	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[1]	BREAK
					CASE 2	g_tlCasinoSpecialPedScenario[iSpecialPed] = tlActiveScenario[2]	BREAK
				ENDSWITCH
				#ENDIF
			BREAK
		ENDSWITCH
	ENDREPEAT
ENDPROC

FUNC BOOL OVERRIDE_CASINO_SPECIAL_PED_TYPE(INT iPedID, INT iLayout, CASINO_AREA eCasinoArea, CASINO_PED_TYPES &pedType)
	UNUSED_PARAMETER(eCasinoArea)
	IF iLayout = 3
#IF IS_DEBUG_BUILD
	OR g_bCasinoSpecialPedsDisable
#ENDIF
		RETURN FALSE
	ENDIF
	INT iSpecialPedLoop
	REPEAT MAX_CASINO_SPECIAL_PED_NAME iSpecialPedLoop
//		PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - iSpecialPedLoop ",iSpecialPedLoop)
		IF g_sSpecialPeds[iSpecialPedLoop].iPedID = iPedID
//			PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - g_sSpecialPeds[iSpecialPedLoop].iPedID ",g_sSpecialPeds[iSpecialPedLoop].iPedID)
			SWITCH INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPedLoop)
				CASE CASINO_SPECIAL_PED_NAME_CALEB
					IF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF (iPedID >= 84 AND iPedID <=87) 
						OR (iPedID >= 72 AND iPedID <=75)
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_CALEB
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_PERMANENT
						IF iPedID = 5
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_CALEB
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
					OR eCasinoArea = CASINO_AREA_SPORTS_BETTING
						IF iPedID = 77 
						OR iPedID = 78
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_CALEB
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_USHI
					IF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF (iPedID >= 84 AND iPedID <=87) 
						OR (iPedID >= 78 AND iPedID <=81)
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_USHI
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_SPORTS_BETTING
						IF iPedID = 48
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_USHI
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_GABRIEL
					IF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF (iPedID >= 84 AND iPedID <=87) 
						OR (iPedID >= 78 AND iPedID <=81)
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_GABRIEL
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
					OR eCasinoArea = CASINO_AREA_SPORTS_BETTING
						IF iPedID = 56
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_GABRIEL
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_VINCE
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
					OR eCasinoArea = CASINO_AREA_SPORTS_BETTING
						IF iPedID = 55
						OR iPedID = 67
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_VINCE
							RETURN TRUE
						ENDIF
					ENDIF				
					IF eCasinoArea = CASINO_AREA_PERMANENT
						IF iPedID = 23
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_VINCE
							RETURN TRUE
						ENDIF
					ENDIF				
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_CAROL
					IF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF (iPedID >= 72 AND iPedID <=75)
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_CAROL
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_PERMANENT
						IF iPedID = 12
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_CAROL
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 40
						OR iPedID = 98
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_CAROL
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_DEAN
					IF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF (iPedID >= 72 AND iPedID <=75)
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_DEAN
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_PERMANENT
						IF iPedID = 37
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_DEAN
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 40
						OR iPedID = 80
						OR iPedID = 98
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_DEAN
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_SPORTS_BETTING
						IF iPedID = 80
						OR iPedID = 98
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_DEAN
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_BETH
					IF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF (iPedID >= 72 AND iPedID <=75)
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_BETH
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 54
						OR iPedID = 41
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_BETH
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_LAUREN
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
					OR eCasinoArea = CASINO_AREA_SPORTS_BETTING
						IF iPedID = 96
						OR iPedID = 109
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_LAUREN
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF (iPedID >= 72 AND iPedID <=75)
						OR (iPedID >= 78 AND iPedID <=79)
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_LAUREN
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_TAYLOR
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
					OR eCasinoArea = CASINO_AREA_SPORTS_BETTING
						IF iPedID = 96
						OR iPedID = 109
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_TAYLOR
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF (iPedID >= 72 AND iPedID <=75)
						OR (iPedID >= 78 AND iPedID <=79)
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_TAYLOR
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_BLANE
					IF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF (iPedID >= 84 AND iPedID <=87)
						OR (iPedID >= 78 AND iPedID <=81)
						OR iPedID = 49
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_BLANE
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_SPORTS_BETTING
						IF iPedID = 49
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_BLANE
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_EILEEN
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
					OR eCasinoArea = CASINO_AREA_SPORTS_BETTING
						IF iPedID = 79
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_EILEEN
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_PERMANENT
						IF iPedID = 35
						OR iPedID = 4
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_EILEEN
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_CURTIS
					IF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF (iPedID >= 84 AND iPedID <=87)
						OR (iPedID >= 78 AND iPedID <=81)
						OR (iPedID >= 72 AND iPedID <=75)
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_TYPE - Special Ped: ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME,iSpecialPedLoop))," Casino Area: ",ENUM_TO_INT(eCasinoArea)," iPedID ",iPedID)
							pedType = CASINO_CURTIS
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL OVERRIDE_CASINO_SPECIAL_PED_POSITION(INT iPedID, INT iLayout, CASINO_AREA eCasinoArea, VECTOR &vSpawnCoords)
	UNUSED_PARAMETER(eCasinoArea)
	IF iLayout = 3
#IF IS_DEBUG_BUILD
	OR g_bCasinoSpecialPedsDisable
#ENDIF
		RETURN FALSE
	ENDIF
	
	FLOAT fPedZAxis
	INT iSpecialPedLoop
	CASINO_ACTIVITY_SLOTS ePedActivity
	
	REPEAT MAX_CASINO_SPECIAL_PED_NAME iSpecialPedLoop
		IF g_sSpecialPeds[iSpecialPedLoop].iPedID = iPedID
			SWITCH INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPedLoop)
				CASE CASINO_SPECIAL_PED_NAME_CALEB
					// Position not modified
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_USHI
					// Position not modified
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_GABRIEL
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 56
							vSpawnCoords =  <<1113.160, 211.426, -49.439>>
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_VINCE
					IF eCasinoArea = CASINO_AREA_PERMANENT
						IF iPedID = 23
							vSpawnCoords =  <<1118.098, 224.60500, -49.635>>
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 67
							vSpawnCoords =  <<1097.503, 207.213, -48.99400>>
							RETURN TRUE
						ENDIF
						IF iPedID = 55
							vSpawnCoords =  <<1115.021, 206.142, -49.419>>
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_DEAN
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 98 // Dean
							vSpawnCoords = <<1100.0807, 198.6786, -49.4567>>
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_CAROL
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 40 // Carol
							vSpawnCoords =  <<1100.7935, 198.6276, -49.4574>>
							RETURN TRUE
						ENDIF
					ENDIF
					IF INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPedLoop) = CASINO_SPECIAL_PED_NAME_CAROL
						IF eCasinoArea = CASINO_AREA_TABLE_GAMES
							IF iPedID >= 72 AND iPedID <= 75
								SWITCH g_iPedReservedBlackjackTable
									CASE 0
										SWITCH iPedID
											CASE 72	vSpawnCoords = << 1149.65, 270.502, -51.840 >>	BREAK
											CASE 73	vSpawnCoords = << 1148.71, 270.505, -51.840 >>	BREAK
											CASE 74	vSpawnCoords = << 1148.08, 269.867, -51.840 >>	BREAK
											CASE 75	vSpawnCoords = << 1148.08, 268.935, -51.840 >>	BREAK
										ENDSWITCH
									BREAK
									CASE 1
										SWITCH iPedID
											CASE 72	vSpawnCoords = << 1151.03, 265.992, -51.840 >>	BREAK
											CASE 73	vSpawnCoords = << 1151.97, 265.989, -51.840 >>	BREAK
											CASE 74	vSpawnCoords = << 1152.6, 266.627, -51.840 >>	BREAK
											CASE 75	vSpawnCoords = << 1152.6, 267.559, -51.840 >>	BREAK
										ENDSWITCH
									BREAK
								ENDSWITCH
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_BETH
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 54
							vSpawnCoords =  <<1113.8896, 227.7491, -49.8408>>
							RETURN TRUE
						ENDIF
						IF iPedID = 41
							vSpawnCoords =  <<1119.5528, 208.7758, -49.9294>>
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF iPedID >= 72 AND iPedID <= 75
							
							ePedActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSpecialPedBethBlackjackActivity)
							SWITCH ePedActivity
								CASE CASINO_AC_SLOT_BLACKJACK_SLOUCHY_FEMALE_NO_HEELS_01A	fPedZAxis = -51.835		BREAK
								CASE CASINO_AC_SLOT_BLACKJACK_SLOUCHY_FEMALE_NO_HEELS_01B	fPedZAxis = -51.835		BREAK
								CASE CASINO_AC_SLOT_BLACKJACK_REGULAR_FEMALE_NO_HEELS_02A	fPedZAxis = -51.818		BREAK
								CASE CASINO_AC_SLOT_BLACKJACK_REGULAR_FEMALE_NO_HEELS_02B	fPedZAxis = -51.818		BREAK
							ENDSWITCH
							
							SWITCH g_iPedReservedBlackjackTable
								CASE 0
									SWITCH iPedID
										CASE 72	vSpawnCoords = << 1149.65, 270.502, fPedZAxis >>	BREAK
										CASE 73	vSpawnCoords = << 1148.71, 270.505, fPedZAxis >>	BREAK
										CASE 74	vSpawnCoords = << 1148.08, 269.867, fPedZAxis >>	BREAK
										CASE 75	vSpawnCoords = << 1148.08, 268.935, fPedZAxis >>	BREAK
									ENDSWITCH
								BREAK
								CASE 1
									SWITCH iPedID
										CASE 72	vSpawnCoords = << 1151.03, 265.992, fPedZAxis >>	BREAK
										CASE 73	vSpawnCoords = << 1151.97, 265.989, fPedZAxis >>	BREAK
										CASE 74	vSpawnCoords = << 1152.6, 266.627, fPedZAxis >>		BREAK
										CASE 75	vSpawnCoords = << 1152.6, 267.559, fPedZAxis >>		BREAK
									ENDSWITCH
								BREAK
							ENDSWITCH
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_TAYLOR
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 96 // Taylor
							vSpawnCoords = <<1109.8335, 205.0403, -49.4400>>
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_POSITION - ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPedLoop))," vSpawnCoords ",GET_STRING_FROM_VECTOR(vSpawnCoords))
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF iPedID >= 72 AND iPedID <= 75
							
							ePedActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSpecialPedTaylorBlackjackActivity)
							SWITCH ePedActivity
								CASE CASINO_AC_SLOT_BLACKJACK_SLOUCHY_FEMALE_NO_HEELS_01A	fPedZAxis = -51.8375	BREAK
								CASE CASINO_AC_SLOT_BLACKJACK_SLOUCHY_FEMALE_NO_HEELS_01B	fPedZAxis = -51.8375	BREAK
								CASE CASINO_AC_SLOT_BLACKJACK_REGULAR_FEMALE_NO_HEELS_02A	fPedZAxis = -51.824		BREAK
								CASE CASINO_AC_SLOT_BLACKJACK_REGULAR_FEMALE_NO_HEELS_02B	fPedZAxis = -51.824		BREAK
							ENDSWITCH
							
							SWITCH g_iPedReservedBlackjackTable
								CASE 0
									SWITCH iPedID
										CASE 72	vSpawnCoords = << 1149.65, 270.502, fPedZAxis >>	BREAK
										CASE 73	vSpawnCoords = << 1148.71, 270.505, fPedZAxis >>	BREAK
										CASE 74	vSpawnCoords = << 1148.08, 269.867, fPedZAxis >>	BREAK
										CASE 75	vSpawnCoords = << 1148.08, 268.935, fPedZAxis >>	BREAK
									ENDSWITCH
								BREAK
								CASE 1
									SWITCH iPedID
										CASE 72	vSpawnCoords = << 1151.03, 265.992, fPedZAxis >>	BREAK
										CASE 73	vSpawnCoords = << 1151.97, 265.989, fPedZAxis >>	BREAK
										CASE 74	vSpawnCoords = << 1152.6, 266.627, fPedZAxis >>		BREAK
										CASE 75	vSpawnCoords = << 1152.6, 267.559, fPedZAxis >>		BREAK
									ENDSWITCH
								BREAK
							ENDSWITCH
							RETURN TRUE
						
						ELIF (iPedID = 79)
							
							ePedActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSpecialPedRouletteSeatThreeActivity)
							SWITCH ePedActivity
								CASE CASINO_AC_SLOT_ROULETTE_SEAT_3_REGULAR_FEMALE_NO_HEELS_03A				fPedZAxis = -51.840		BREAK
								CASE CASINO_AC_SLOT_ROULETTE_SEAT_3_REGULAR_FEMALE_NO_HEELS_03B				fPedZAxis = -51.840		BREAK
							ENDSWITCH
							
							SWITCH g_iPedReservedRouletteTable
								CASE 0
									SWITCH iPedID
										CASE 81	vSpawnCoords = << 1144.34, 269.022, fPedZAxis >>	BREAK
										CASE 80	vSpawnCoords = << 1143.66, 268.335, fPedZAxis >>	BREAK
										CASE 79	vSpawnCoords = << 1143.75, 267.377, fPedZAxis >>	BREAK
										CASE 78	vSpawnCoords = << 1144.72, 267.277, fPedZAxis >>	BREAK
									ENDSWITCH
								BREAK
								CASE 1
									SWITCH iPedID
										CASE 81	vSpawnCoords = << 1150.83, 261.963, fPedZAxis >>	BREAK	
										CASE 80	vSpawnCoords = << 1151.51, 262.651, fPedZAxis >>	BREAK
										CASE 79	vSpawnCoords = << 1151.42, 263.608, fPedZAxis >>	BREAK
										CASE 78	vSpawnCoords = << 1150.45, 263.708, fPedZAxis >>	BREAK
									ENDSWITCH
								BREAK
							ENDSWITCH
							RETURN TRUE
						
						ELIF (iPedID = 78)
							
							ePedActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSpecialPedRouletteSeatFourActivity)
							SWITCH ePedActivity
								CASE CASINO_AC_SLOT_ROULETTE_SEAT_4_SLOUCHY_WITH_DRINK_FEMALE_NO_HEELS_04A	fPedZAxis = -51.844	BREAK
								CASE CASINO_AC_SLOT_ROULETTE_SEAT_4_SLOUCHY_WITH_DRINK_FEMALE_NO_HEELS_04B	fPedZAxis = -51.842	BREAK
							ENDSWITCH
							
							SWITCH g_iPedReservedRouletteTable
								CASE 0
									SWITCH iPedID
										CASE 81	vSpawnCoords = << 1144.34, 269.022, fPedZAxis >>	BREAK
										CASE 80	vSpawnCoords = << 1143.66, 268.335, fPedZAxis >>	BREAK
										CASE 79	vSpawnCoords = << 1143.75, 267.377, fPedZAxis >>	BREAK
										CASE 78	vSpawnCoords = << 1144.72, 267.277, fPedZAxis >>	BREAK
									ENDSWITCH
								BREAK
								CASE 1
									SWITCH iPedID
										CASE 81	vSpawnCoords = << 1150.83, 261.963, fPedZAxis >>	BREAK	
										CASE 80	vSpawnCoords = << 1151.51, 262.651, fPedZAxis >>	BREAK
										CASE 79	vSpawnCoords = << 1151.42, 263.608, fPedZAxis >>	BREAK
										CASE 78	vSpawnCoords = << 1150.45, 263.708, fPedZAxis >>	BREAK
									ENDSWITCH
								BREAK
							ENDSWITCH
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_LAUREN
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 109 // Lauren
							vSpawnCoords = <<1109.3749, 205.5011, -49.4400>>
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_POSITION - ",GET_CASINO_SPECIAL_PED_NAME_STRING(INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPedLoop))," vSpawnCoords ",GET_STRING_FROM_VECTOR(vSpawnCoords))
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF iPedID >= 72 AND iPedID <= 75
							
							ePedActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSpecialPedLaurenBlackjackActivity)
							SWITCH ePedActivity
								CASE CASINO_AC_SLOT_BLACKJACK_SLOUCHY_FEMALE_NO_HEELS_01A	fPedZAxis = -51.8375	BREAK
								CASE CASINO_AC_SLOT_BLACKJACK_SLOUCHY_FEMALE_NO_HEELS_01B	fPedZAxis = -51.8375	BREAK
								CASE CASINO_AC_SLOT_BLACKJACK_REGULAR_FEMALE_NO_HEELS_02A	fPedZAxis = -51.824		BREAK
								CASE CASINO_AC_SLOT_BLACKJACK_REGULAR_FEMALE_NO_HEELS_02B	fPedZAxis = -51.824		BREAK
							ENDSWITCH
							
							SWITCH g_iPedReservedBlackjackTable
								CASE 0
									SWITCH iPedID
										CASE 72	vSpawnCoords = << 1149.65, 270.502, fPedZAxis >>	BREAK
										CASE 73	vSpawnCoords = << 1148.71, 270.505, fPedZAxis >>	BREAK
										CASE 74	vSpawnCoords = << 1148.08, 269.867, fPedZAxis >>	BREAK
										CASE 75	vSpawnCoords = << 1148.08, 268.935, fPedZAxis >>	BREAK
									ENDSWITCH
								BREAK
								CASE 1
									SWITCH iPedID
										CASE 72	vSpawnCoords = << 1151.03, 265.992, fPedZAxis >>	BREAK
										CASE 73	vSpawnCoords = << 1151.97, 265.989, fPedZAxis >>	BREAK
										CASE 74	vSpawnCoords = << 1152.6, 266.627, fPedZAxis >>		BREAK
										CASE 75	vSpawnCoords = << 1152.6, 267.559, fPedZAxis >>		BREAK
									ENDSWITCH
								BREAK
							ENDSWITCH
							RETURN TRUE
							
						ELIF (iPedID = 79)
							
							ePedActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSpecialPedRouletteSeatThreeActivity)
							SWITCH ePedActivity
								CASE CASINO_AC_SLOT_ROULETTE_SEAT_3_REGULAR_FEMALE_NO_HEELS_03A				fPedZAxis = -51.840		BREAK
								CASE CASINO_AC_SLOT_ROULETTE_SEAT_3_REGULAR_FEMALE_NO_HEELS_03B				fPedZAxis = -51.826		BREAK
							ENDSWITCH
							
							SWITCH g_iPedReservedRouletteTable
								CASE 0
									SWITCH iPedID
										CASE 81	vSpawnCoords = << 1144.34, 269.022, fPedZAxis >>	BREAK
										CASE 80	vSpawnCoords = << 1143.66, 268.335, fPedZAxis >>	BREAK
										CASE 79	vSpawnCoords = << 1143.75, 267.377, fPedZAxis >>	BREAK
										CASE 78	vSpawnCoords = << 1144.72, 267.277, fPedZAxis >>	BREAK
									ENDSWITCH
								BREAK
								CASE 1
									SWITCH iPedID
										CASE 81	vSpawnCoords = << 1150.83, 261.963, fPedZAxis >>	BREAK	
										CASE 80	vSpawnCoords = << 1151.51, 262.651, fPedZAxis >>	BREAK
										CASE 79	vSpawnCoords = << 1151.42, 263.608, fPedZAxis >>	BREAK
										CASE 78	vSpawnCoords = << 1150.45, 263.708, fPedZAxis >>	BREAK
									ENDSWITCH
								BREAK
							ENDSWITCH
							RETURN TRUE
						
						ELIF (iPedID = 78)
							
							ePedActivity = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSpecialPedRouletteSeatFourActivity)
							SWITCH ePedActivity
								CASE CASINO_AC_SLOT_ROULETTE_SEAT_4_SLOUCHY_WITH_DRINK_FEMALE_NO_HEELS_04A	fPedZAxis = -51.8385	BREAK
								CASE CASINO_AC_SLOT_ROULETTE_SEAT_4_SLOUCHY_WITH_DRINK_FEMALE_NO_HEELS_04B	fPedZAxis = -51.8350	BREAK
							ENDSWITCH
							
							SWITCH g_iPedReservedRouletteTable
								CASE 0
									SWITCH iPedID
										CASE 81	vSpawnCoords = << 1144.34, 269.022, fPedZAxis >>	BREAK
										CASE 80	vSpawnCoords = << 1143.66, 268.335, fPedZAxis >>	BREAK
										CASE 79	vSpawnCoords = << 1143.75, 267.377, fPedZAxis >>	BREAK
										CASE 78	vSpawnCoords = << 1144.72, 267.277, fPedZAxis >>	BREAK
									ENDSWITCH
								BREAK
								CASE 1
									SWITCH iPedID
										CASE 81	vSpawnCoords = << 1150.83, 261.963, fPedZAxis >>	BREAK	
										CASE 80	vSpawnCoords = << 1151.51, 262.651, fPedZAxis >>	BREAK
										CASE 79	vSpawnCoords = << 1151.42, 263.608, fPedZAxis >>	BREAK
										CASE 78	vSpawnCoords = << 1150.45, 263.708, fPedZAxis >>	BREAK
									ENDSWITCH
								BREAK
							ENDSWITCH
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_BLANE
					// Position not modified
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_EILEEN
					// Position not modified
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_CURTIS
					// Position not modified
				BREAK
			ENDSWITCH
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT(INT iPedID, INT iLayout, CASINO_AREA eCasinoArea, CASINO_ACTIVITY_SLOTS &activitySlots)
	UNUSED_PARAMETER(eCasinoArea)
	IF iLayout = 3
#IF IS_DEBUG_BUILD
	OR g_bCasinoSpecialPedsDisable
#ENDIF
		RETURN FALSE
	ENDIF
	
	INT iSpecialPedLoop
	REPEAT MAX_CASINO_SPECIAL_PED_NAME iSpecialPedLoop
		IF g_sSpecialPeds[iSpecialPedLoop].iPedID = iPedID
			SWITCH INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPedLoop)
				CASE CASINO_SPECIAL_PED_NAME_CALEB
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_USHI
					IF eCasinoArea = CASINO_AREA_SPORTS_BETTING
					OR eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF iPedID = 48
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Ushi CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A")
							activitySlots = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_GABRIEL
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 56
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Gabriel CASINO_AC_SLOT_PROP_HUMAN_SEAT_BAR_M_2")
							activitySlots = CASINO_AC_SLOT_BAR_MALE_SIT_WITH_DRINK_01A
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_VINCE
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 55
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Vince CASINO_AC_SLOT_OUTOFMONEY_M_2A")
							activitySlots =	CASINO_SPECIAL_PED_VINCE_BAR
							RETURN TRUE
						ELIF iPedID = 67
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Vince CASINO_AC_SLOT_OUTOFMONEY_M_1B")
							activitySlots = CASINO_SPECIAL_PED_VINCE_WALL
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_PERMANENT
						IF iPedID = 23
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Vince CASINO_AC_SLOT_OUTOFMONEY_M_1A")
							activitySlots = CASINO_SPECIAL_PED_VINCE_STEPS
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_CAROL
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 40
						OR iPedID = 98
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Carol CASINO_SPECIAL_PED_CAROL_SHOP")
							activitySlots =	CASINO_SPECIAL_PED_CAROL_SHOP
							RETURN TRUE
						ENDIF
						
					ELIF eCasinoArea = CASINO_AREA_PERMANENT
						IF iPedID = 12
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Carol CASINO_SPECIAL_PED_CAROL_SLOTS")
							activitySlots =	CASINO_SPECIAL_PED_CAROL_SLOTS
							RETURN TRUE
						ENDIF
						
					ELIF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF iPedID >= 72 AND iPedID <= 75
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Carol Blackjack")
							activitySlots = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSpecialPedCarolBlackjackActivity)
							#IF IS_DEBUG_BUILD
							IF NOT g_bCasinoPedEditorActive
								g_iCasinoActivitySlot[iPedID] = g_iSpecialPedCarolBlackjackActivity
							ENDIF
							#ENDIF
							RETURN TRUE
						ENDIF
					ENDIF					
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_DEAN
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
					OR eCasinoArea = CASINO_AREA_PERMANENT
						IF iPedID = 40
						OR iPedID = 98
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Dean CASINO_SPECIAL_PED_DEAN_SHOP")
							activitySlots = CASINO_SPECIAL_PED_DEAN_SHOP
							RETURN TRUE
						ENDIF
						IF iPedID = 37
						OR iPedID = 80
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Dean CASINO_SPECIAL_PED_DEAN_SLOTS")
							activitySlots = CASINO_SPECIAL_PED_DEAN_SLOTS
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF iPedID >= 72 AND iPedID <= 75
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Dean blackjack")
							activitySlots = CASINO_AC_SLOT_BLACKJACK_SLOUCHY_01A
							WHILE  activitySlots = CASINO_AC_SLOT_BLACKJACK_SLOUCHY_01A 
								OR activitySlots = CASINO_AC_SLOT_BLACKJACK_SLOUCHY_01B 
								OR activitySlots = CASINO_AC_SLOT_BLACKJACK_SLOUCHY_WITH_DRINK_01A 
								OR activitySlots = CASINO_AC_SLOT_BLACKJACK_SLOUCHY_WITH_DRINK_01B
								activitySlots = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, GET_RANDOM_INT_IN_RANGE(ENUM_TO_INT(CASINO_AC_SLOT_BLACKJACK_ENGAGED_01A), ENUM_TO_INT(CASINO_AC_SLOT_BLACKJACK_SLOUCHY_WITH_DRINK_01B)+1))
							ENDWHILE
							RETURN TRUE
						ENDIF				
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_BETH
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 41
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Beth CASINO_SPECIAL_PED_BETH_LOUNGE")
							activitySlots = CASINO_SPECIAL_PED_BETH_LOUNGE
							RETURN TRUE
						ENDIF
						IF iPedID = 54
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Beth CASINO_SPECIAL_PED_BETH_WHEEL")
							activitySlots = CASINO_SPECIAL_PED_BETH_WHEEL
							RETURN TRUE
						ENDIF
					ELIF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF iPedID >= 72 AND iPedID <= 75
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Beth Blackjack")
							activitySlots = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSpecialPedBethBlackjackActivity)
							#IF IS_DEBUG_BUILD
							IF NOT g_bCasinoPedEditorActive
								g_iCasinoActivitySlot[iPedID] = g_iSpecialPedBethBlackjackActivity
							ENDIF
							#ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_LAUREN
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 96
						OR iPedID = 109
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Lauren CASINO_SPECIAL_PED_LAUREN_BAR")
							activitySlots = CASINO_SPECIAL_PED_LAUREN_BAR
							RETURN TRUE
						ENDIF
					ELIF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF (iPedID >= 72 AND iPedID <= 75)
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Lauren Blackjack")
							activitySlots = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSpecialPedLaurenBlackjackActivity)
							#IF IS_DEBUG_BUILD
							IF NOT g_bCasinoPedEditorActive
								g_iCasinoActivitySlot[iPedID] = g_iSpecialPedLaurenBlackjackActivity
							ENDIF
							#ENDIF
							RETURN TRUE
						
						ELIF (iPedID = 79)
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Lauren Roulette Seat 3")
							activitySlots = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSpecialPedRouletteSeatThreeActivity)
							#IF IS_DEBUG_BUILD
							IF NOT g_bCasinoPedEditorActive
								g_iCasinoActivitySlot[iPedID] = g_iSpecialPedRouletteSeatThreeActivity
							ENDIF
							#ENDIF
							RETURN TRUE
							
						ELIF (iPedID = 78)
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Lauren Roulette Seat 4")
							activitySlots = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSpecialPedRouletteSeatFourActivity)
							#IF IS_DEBUG_BUILD
							IF NOT g_bCasinoPedEditorActive
								g_iCasinoActivitySlot[iPedID] = g_iSpecialPedRouletteSeatFourActivity
							ENDIF
							#ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_TAYLOR
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 96
						OR iPedID = 109
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Taylor CASINO_SPECIAL_PED_TAYLOR_BAR")
							activitySlots = CASINO_SPECIAL_PED_TAYLOR_BAR
							RETURN TRUE
						ENDIF
					ELIF eCasinoArea = CASINO_AREA_TABLE_GAMES
						IF iPedID >= 72 AND iPedID <= 75
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Taylor Blackjack")
							activitySlots = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSpecialPedTaylorBlackjackActivity)
							#IF IS_DEBUG_BUILD
							IF NOT g_bCasinoPedEditorActive
								g_iCasinoActivitySlot[iPedID] = g_iSpecialPedTaylorBlackjackActivity
							ENDIF
							#ENDIF
							RETURN TRUE
							
						ELIF (iPedID = 79)
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Taylor Roulette Seat 3")
							activitySlots = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSpecialPedRouletteSeatThreeActivity)
							#IF IS_DEBUG_BUILD
							IF NOT g_bCasinoPedEditorActive
								g_iCasinoActivitySlot[iPedID] = g_iSpecialPedRouletteSeatThreeActivity
							ENDIF
							#ENDIF
							RETURN TRUE
							
						ELIF (iPedID = 78)
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Taylor  Roulette Seat 4")
							activitySlots = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSpecialPedRouletteSeatFourActivity)
							#IF IS_DEBUG_BUILD
							IF NOT g_bCasinoPedEditorActive
								g_iCasinoActivitySlot[iPedID] = g_iSpecialPedRouletteSeatFourActivity
							ENDIF
							#ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_BLANE
					IF eCasinoArea = CASINO_AREA_SPORTS_BETTING
						IF iPedID = 49
							PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Blane CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A")
							activitySlots = CASINO_AC_SLOT_HANGOUT_WITHDRINK_M_01A
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_EILEEN
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_PERMANENT
						PRINTLN("[CASINO_PEDS] OVERRIDE_CASINO_SPECIAL_PED_ACTIVITY_SLOT - Eileen Slot Machine")
						activitySlots = INT_TO_ENUM(CASINO_ACTIVITY_SLOTS, g_iSpecialPedEileenSlotMachineActivity)
						#IF IS_DEBUG_BUILD
						IF NOT g_bCasinoPedEditorActive
							g_iCasinoActivitySlot[iPedID] = g_iSpecialPedEileenSlotMachineActivity
						ENDIF
						#ENDIF
						RETURN TRUE
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_CURTIS
				BREAK
			ENDSWITCH
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL OVERRIDE_CASINO_SPECIAL_PED_ROOM_KEY(INT iPedID, INT iLayout, CASINO_AREA eCasinoArea, INT &roomKey)
	UNUSED_PARAMETER(eCasinoArea)
	IF iLayout = 3
#IF IS_DEBUG_BUILD
	OR g_bCasinoSpecialPedsDisable
#ENDIF
		RETURN FALSE
	ENDIF
	INT iSpecialPedLoop
	REPEAT MAX_CASINO_SPECIAL_PED_NAME iSpecialPedLoop
		IF g_sSpecialPeds[iSpecialPedLoop].iPedID = iPedID
			SWITCH INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPedLoop)
				CASE CASINO_SPECIAL_PED_NAME_CALEB
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_USHI
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_GABRIEL
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_VINCE
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_CAROL
				CASE CASINO_SPECIAL_PED_NAME_DEAN
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 98
						OR iPedID = 40
							roomKey = CASINO_SHOP_ROOM_KEY
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_BETH
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_LAUREN
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_TAYLOR
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_BLANE
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_EILEEN
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_CURTIS
				BREAK
				BREAK
			ENDSWITCH
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC BOOL OVERRIDE_CASINO_SPECIAL_PED_HEADING(INT iPedID, INT iLayout, CASINO_AREA eCasinoArea, FLOAT &fHeading)
	UNUSED_PARAMETER(eCasinoArea)
	IF iLayout = 3
#IF IS_DEBUG_BUILD
	OR g_bCasinoSpecialPedsDisable
#ENDIF
		RETURN FALSE
	ENDIF
	INT iSpecialPedLoop
	REPEAT MAX_CASINO_SPECIAL_PED_NAME iSpecialPedLoop
		IF g_sSpecialPeds[iSpecialPedLoop].iPedID = iPedID
			SWITCH INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPedLoop)
				CASE CASINO_SPECIAL_PED_NAME_CALEB
					// Position not modified
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_USHI
					// Position not modified
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_GABRIEL
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 56
							fHeading =  140.016
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_VINCE
					IF eCasinoArea = CASINO_AREA_PERMANENT
						IF iPedID = 23
							fHeading =  89.280
							RETURN TRUE
						ENDIF
					ENDIF
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 67
							fHeading =  345.300
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_CAROL
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 40
							fHeading = -15.47
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_DEAN
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 98
							fHeading = -48.96
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_BETH
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 54
							fHeading =  57.1564
							RETURN TRUE
						ENDIF
						IF iPedID = 41
							fHeading =  94.8633
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_LAUREN
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 109 // Lauren
							fHeading = -33.00
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_TAYLOR
					IF eCasinoArea = CASINO_AREA_MAIN_FLOOR
					OR eCasinoArea = CASINO_AREA_MANAGERS_OFFICE
						IF iPedID = 96 // Taylor
							fHeading = -33.00
							RETURN TRUE
						ENDIF
					ENDIF
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_BLANE
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_EILEEN
				BREAK
				CASE CASINO_SPECIAL_PED_NAME_CURTIS
				BREAK
			ENDSWITCH
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

FUNC INT OVERRIDE_CASINO_SPECIAL_PED_SLOT_SPECTATOR(CASINO_SPECIAL_PED_SERVER_DATA &sSpecialPeds[])
#IF IS_DEBUG_BUILD
	IF g_bCasinoSpecialPedsDisable
		RETURN -1
	ENDIF
#ENDIF
	INT iSpecialPedLoop
	REPEAT MAX_CASINO_SPECIAL_PED_NAME iSpecialPedLoop
		SWITCH INT_TO_ENUM(CASINO_SPECIAL_PED_NAME, iSpecialPedLoop)
			CASE CASINO_SPECIAL_PED_NAME_CALEB
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_USHI
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_GABRIEL
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_VINCE
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_DEAN
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_CAROL
				IF sSpecialPeds[iSpecialPedLoop].iPedID = 12
					RETURN sSpecialPeds[CASINO_SPECIAL_PED_NAME_DEAN].iPedID
				ENDIF
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_BETH
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_LAUREN
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_TAYLOR
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_BLANE
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_EILEEN
			BREAK
			CASE CASINO_SPECIAL_PED_NAME_CURTIS
			BREAK
		ENDSWITCH
	ENDREPEAT	
	RETURN -1
ENDFUNC

//FUNC INT GET_NEXT_CONVERSATION(INT iConversationAmount, INT &iBitSetConversation)
//	INT iCounter = 0
//	INT iPrevConvo = -1
//	INT iConversation = GET_RANDOM_INT_IN_RANGE(0, iConversationAmount)
//	
//	REPEAT iConversationAmount iCounter 
//		IF IS_BIT_SET(iBitSetConversation, iConversation)
//			CLEAR_BIT(iBitSetConversation, iConversation)
//			iPrevConvo = iCounter
//		ENDIF
//	ENDREPEAT
//	
//	IF (iConversation = iPrevConvo)
//		iConversation++
//		IF iConversation > iConversationAmount
//			iConversation = 0
//		ENDIF
//	ENDIF
//	
//	RETURN iConversation
//ENDFUNC

FUNC INT GET_NEXT_CONVERSATION(INT iConversationAmount, INT &iBitSetConversation)
	INT iCounter
	iCounter = 0
	INT iConversation = GET_RANDOM_INT_IN_RANGE(0,iConversationAmount)
	WHILE IS_BIT_SET(iBitSetConversation,iConversation)
		iCounter++
		IF iCounter < iConversationAmount
			iConversation = GET_RANDOM_INT_IN_RANGE(0,iConversationAmount)
		ELIF iCounter < iConversationAmount*2
			iConversation = (iConversation + 1) % iConversationAmount
		ELSE
			iBitSetConversation = 0
			iCounter = 0
		ENDIF
	ENDWHILE
	RETURN iConversation
ENDFUNC




PROC GET_CASINO_VINCE_BAR_ANIM_DATA(INT iSequence, INT &iConversation, CASINO_PEDS &casinoPed, TEXT_LABEL_63 &tlPedAnimClip)
	IF g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_VINCE].bWaitingCouple
		iSequence = 0
	ENDIF
	SWITCH iSequence
		CASE 0
		CASE 1
			casinoPed.sPedAnimDict = "ANIM@AMB@CASINO@OUT_OF_MONEY@PED_MALE@02A@BASE"
			tlPedAnimClip = "BASE"
		BREAK
		CASE 2
			iConversation = GET_NEXT_CONVERSATION(10,g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_VINCE].iBitSetConversation)
			SET_BIT(g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_VINCE].iBitSetConversation, iConversation)
			
//			#IF IS_DEBUG_BUILD
//			IF g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_VINCE] > -1
//				iConversation = g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_VINCE]
//			ENDIF
//			#ENDIF
			
			SWITCH iConversation
				CASE 0
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@BAR@CAS_VINCE_IG1"
					tlPedAnimClip = "CAS_VINCE_IG1_CUT_ME_OFF"
				BREAK
				CASE 1
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@BAR@CAS_VINCE_IG1"
					tlPedAnimClip = "CAS_VINCE_IG1_YEAH_WHOS_EMOTIONALLY"
				BREAK
				CASE 2
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@BAR@CAS_VINCE_IG1"
					tlPedAnimClip = "CAS_VINCE_IG1_YOU_WATCH_A"
				BREAK
				CASE 3
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@BAR@CAS_VINCE_IG1"
					tlPedAnimClip = "CAS_VINCE_IG1_WHATS_WRONG_WITH"
				BREAK
				CASE 4
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@BAR@CAS_VINCE_IG1"
					tlPedAnimClip = "CAS_VINCE_IG1_UGH_I_COULD"
				BREAK
				CASE 5
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@BAR@CAS_VINCE_IG1"
					tlPedAnimClip = "CAS_VINCE_IG1_YEAH_DONT_LOOK"
				BREAK
				CASE 6
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@BAR@CAS_VINCE_IG1"
					tlPedAnimClip = "CAS_VINCE_IG1_WAIT_WHERES_BOBBY"
				BREAK
				CASE 7
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@BAR@CAS_VINCE_IG1"
					tlPedAnimClip = "CAS_VINCE_IG1_AWESOME_SAUCE_02"
				BREAK
				CASE 8
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@BAR@CAS_VINCE_IG1"
					tlPedAnimClip = "CAS_VINCE_IG1_ANYBODY_SELLING_ANY"
				BREAK
				CASE 9
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@BAR@CAS_VINCE_IG1"
					tlPedAnimClip = "CAS_VINCE_IG1_WHAT_WITH_YOU"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CASINO_VINCE_STEPS_ANIM_DATA(INT iSequence, INT &iConversation, CASINO_PEDS &casinoPed, TEXT_LABEL_63 &tlPedAnimClip)
	IF g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_VINCE].bWaitingCouple
		iSequence = 0
	ENDIF
	SWITCH iSequence
		CASE 0
		CASE 1
			casinoPed.sPedAnimDict = "ANIM@AMB@CASINO@OUT_OF_MONEY@PED_MALE@01A@BASE"
			tlPedAnimClip = "BASE"
		BREAK
		CASE 2
			iConversation = GET_NEXT_CONVERSATION(10,g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_VINCE].iBitSetConversation)
			SET_BIT(g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_VINCE].iBitSetConversation,iConversation)
			
//			#IF IS_DEBUG_BUILD
//			IF g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_VINCE] > -1
//				iConversation = g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_VINCE]
//			ENDIF
//			#ENDIF
			
			SWITCH iConversation
				CASE 0			
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@STEPS@CAS_VINCE_IG2"
					tlPedAnimClip = "CAS_VINCE_IG2_HEY_WHERE_MY"
				BREAK
				CASE 1
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@STEPS@CAS_VINCE_IG2"
					tlPedAnimClip = "CAS_VINCE_IG2_YEAH_SHELL_COME"
				BREAK
				CASE 2
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@STEPS@CAS_VINCE_IG2"
					tlPedAnimClip = "CAS_VINCE_IG2_ID_GO_HOME"
				BREAK
				CASE 3
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@STEPS@CAS_VINCE_IG2"
					tlPedAnimClip = "CAS_VINCE_IG2_LEAST_I_GET"
				BREAK
				CASE 4
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@STEPS@CAS_VINCE_IG2"
					tlPedAnimClip = "CAS_VINCE_IG2_SHE_BLEW_IT"
				BREAK
				CASE 5
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@STEPS@CAS_VINCE_IG2"
					tlPedAnimClip = "CAS_VINCE_IG2_GOOD_LUCK_FINDING"
				BREAK
				CASE 6
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@STEPS@CAS_VINCE_IG2"
					tlPedAnimClip = "CAS_VINCE_IG2_I_WAS_YOUR"
				BREAK
				CASE 7
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@STEPS@CAS_VINCE_IG2"
					tlPedAnimClip = "CAS_VINCE_IG2_I_SHOULD_GET"
				BREAK
				CASE 8
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@STEPS@CAS_VINCE_IG2"
					tlPedAnimClip = "CAS_VINCE_IG2_I_ACTUALLY_FEEL"
				BREAK
				CASE 9
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@STEPS@CAS_VINCE_IG2"
					tlPedAnimClip = "CAS_VINCE_IG2_JUST_NEED_A"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CASINO_VINCE_WALL_ANIM_DATA(INT iSequence, INT &iConversation, CASINO_PEDS &casinoPed, TEXT_LABEL_63 &tlPedAnimClip)
	IF g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_VINCE].bWaitingCouple
		iSequence = 0
	ENDIF
	SWITCH iSequence
		CASE 0
		CASE 1
			casinoPed.sPedAnimDict = "ANIM@AMB@CASINO@OUT_OF_MONEY@PED_MALE@02B@BASE"
			tlPedAnimClip = "BASE"
		BREAK
		CASE 2
			iConversation = GET_NEXT_CONVERSATION(10,g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_VINCE].iBitSetConversation)
			SET_BIT(g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_VINCE].iBitSetConversation,iConversation)
			
//			#IF IS_DEBUG_BUILD
//			IF g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_VINCE] > -1
//				iConversation = g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_VINCE]
//			ENDIF
//			#ENDIF
			
			SWITCH iConversation
				CASE 0
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@WALL@CAS_VINCE_IG3"
					tlPedAnimClip = "CAS_VINCE_IG3_NEED_ANOTHER_BUMP"
				BREAK
				CASE 1
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@WALL@CAS_VINCE_IG3"
					tlPedAnimClip = "CAS_VINCE_IG3_I_HAVE_MOVED_ON"
				BREAK
				CASE 2
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@WALL@CAS_VINCE_IG3"
					tlPedAnimClip = "CAS_VINCE_IG3_LIVING_THE_DREAM"
				BREAK
				CASE 3
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@WALL@CAS_VINCE_IG3"
					tlPedAnimClip = "CAS_VINCE_IG3_SERVICE_HERE_SUCKS"
				BREAK
				CASE 4
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@WALL@CAS_VINCE_IG3"
					tlPedAnimClip = "CAS_VINCE_IG3_FUNNY_THING_IS"
				BREAK
				CASE 5
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@WALL@CAS_VINCE_IG3"
					tlPedAnimClip = "CAS_VINCE_IG3_KEEP_IT_REAL"
				BREAK
				CASE 6
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@WALL@CAS_VINCE_IG3"
					tlPedAnimClip = "CAS_VINCE_IG3_LUCKY_ESCAPE"
				BREAK
				CASE 7
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@WALL@CAS_VINCE_IG3"
					tlPedAnimClip = "CAS_VINCE_IG3_QUALITY_ME_TIME"
				BREAK
				CASE 8
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@WALL@CAS_VINCE_IG3"
					tlPedAnimClip = "CAS_VINCE_IG3_HASHTAG_BACK_ON_TOP"
				BREAK
				CASE 9
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@VINCE@WALL@CAS_VINCE_IG3"
					tlPedAnimClip = "CAS_VINCE_IG3_BEST_NIGHT_EVER"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CASINO_CAROL_SHOP_ANIM_DATA(INT iSequence, INT &iConversationA, INT iConversationB, CASINO_PEDS &casinoPed, TEXT_LABEL_63 &tlPedAnimClip)
	IF g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_CAROL].bWaitingCouple
		iSequence = 0
	ENDIF
	SWITCH iSequence
		CASE 0
			casinoPed.sPedAnimDict = "ANIM@AMB@CASINO@SHOP@SPECIAL_PEDS@CAROL@01A@BASE"
			tlPedAnimClip = "BASE"
			iConversationA = -1
		BREAK
		CASE 1
			INT iRandomIdle
			iRandomIdle = GET_RANDOM_INT_IN_RANGE(0,3)
			SWITCH iRandomIdle
				CASE 0
					casinoPed.sPedAnimDict = "ANIM@AMB@CASINO@SHOP@SPECIAL_PEDS@CAROL@01A@IDLES"
					tlPedAnimClip = "IDLE_A"
				BREAK
				CASE 1
					casinoPed.sPedAnimDict = "ANIM@AMB@CASINO@SHOP@SPECIAL_PEDS@CAROL@01A@IDLES"
					tlPedAnimClip = "IDLE_B"
				BREAK
				CASE 2
					casinoPed.sPedAnimDict = "ANIM@AMB@CASINO@SHOP@SPECIAL_PEDS@CAROL@01A@IDLES"
					tlPedAnimClip = "IDLE_C"
				BREAK
			ENDSWITCH
			iConversationA = -1
		BREAK
		CASE 2
			IF iConversationB != -1
				iConversationA = iConversationB
			ELSE
				iConversationA = GET_NEXT_CONVERSATION(5,g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_CAROL].iBitSetConversation)
			ENDIF
			SET_BIT(g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_CAROL].iBitSetConversation,iConversationA)
			
//			#IF IS_DEBUG_BUILD
//			IF g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_CAROL] > -1
//				iConversationA = g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_CAROL]
//			ENDIF
//			#ENDIF
			
			SWITCH iConversationA
				CASE 0
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@CAROL@SHOP@"
					tlPedAnimClip = "MAKE_SOUP_SP_CAROL"
				BREAK
				CASE 1
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@CAROL@SHOP@"
					tlPedAnimClip = "DONT_TOUCH_SP_CAROL"
				BREAK
				CASE 2
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@CAROL@SHOP@"
					tlPedAnimClip = "WINGS_SP_CAROL"
				BREAK
				CASE 3
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@CAROL@SHOP@"
					tlPedAnimClip = "DUST_SP_CAROL"
				BREAK
				CASE 4
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@CAROL@SHOP@"
					tlPedAnimClip = "MY_FEET_SP_CAROL"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CASINO_CAROL_SLOTS_ANIM_DATA(INT iSequence, INT &iConversationA, INT iConversationB, CASINO_PEDS &casinoPed, TEXT_LABEL_63 &tlPedAnimClip)
	IF g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_CAROL].bWaitingCouple
		iSequence = 0
	ENDIF
	SWITCH iSequence
		CASE 0
			casinoPed.sPedAnimDict = "ANIM_CASINO_A@AMB@CASINO@GAMES@SPECTATE@SLOTS@SPECIAL_PEDS@CAROL@01A@BASE"
			tlPedAnimClip = "BASE"
			iConversationA = -1
		BREAK
		CASE 1
			INT iRandomIdle
			iRandomIdle = GET_RANDOM_INT_IN_RANGE(0,2)
			SWITCH iRandomIdle
				CASE 0
					casinoPed.sPedAnimDict = "ANIM_CASINO_A@AMB@CASINO@GAMES@SPECTATE@SLOTS@SPECIAL_PEDS@CAROL@01A@IDLES"
					tlPedAnimClip = "IDLE_A"
				BREAK
				CASE 1
					casinoPed.sPedAnimDict = "ANIM_CASINO_A@AMB@CASINO@GAMES@SPECTATE@SLOTS@SPECIAL_PEDS@CAROL@01A@IDLES"
					tlPedAnimClip = "IDLE_B"
				BREAK
			ENDSWITCH
			iConversationA = -1
		BREAK
		CASE 2
			IF iConversationB != -1
				iConversationA = iConversationB
			ELSE
				iConversationA = GET_NEXT_CONVERSATION(5,g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_CAROL].iBitSetConversation)
			ENDIF
			SET_BIT(g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_CAROL].iBitSetConversation,iConversationA)
			
//			#IF IS_DEBUG_BUILD
//			IF g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_CAROL] > -1
//				iConversationA = g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_CAROL]
//			ENDIF
//			#ENDIF
			
			SWITCH iConversationA
				CASE 0
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@CAROL@SLOTS@"
					tlPedAnimClip = "FEELING_LUCKY_SP_CAROL"
				BREAK
				CASE 1
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@CAROL@SLOTS@"
					tlPedAnimClip = "HONEYMOON_SP_CAROL"
				BREAK
				CASE 2
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@CAROL@SLOTS@"
					tlPedAnimClip = "THAT_ACTOR_SP_CAROL"
				BREAK
				CASE 3
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@CAROL@SLOTS@"
					tlPedAnimClip = "WALLET_SP_CAROL"
				BREAK
				CASE 4
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@CAROL@SLOTS@"
					tlPedAnimClip = "THOSE_ROLLS_SP_CAROL"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CASINO_DEAN_SHOP_ANIM_DATA(INT iSequence, INT &iConversationA, INT iConversationB, CASINO_PEDS &casinoPed, TEXT_LABEL_63 &tlPedAnimClip)
	IF g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_DEAN].bWaitingCouple
		iSequence = 0
	ENDIF
	SWITCH iSequence
		CASE 0
			casinoPed.sPedAnimDict = "ANIM@AMB@CASINO@SHOP@SPECIAL_PEDS@DEAN@01A@BASE"
			tlPedAnimClip = "BASE"
			iConversationA = -1
		BREAK
		CASE 1
			INT iRandomIdle
			iRandomIdle = GET_RANDOM_INT_IN_RANGE(0,3)
			SWITCH iRandomIdle
				CASE 0
					casinoPed.sPedAnimDict = "ANIM@AMB@CASINO@SHOP@SPECIAL_PEDS@DEAN@01A@IDLES"
					tlPedAnimClip = "IDLE_A"
				BREAK
				CASE 1
					casinoPed.sPedAnimDict = "ANIM@AMB@CASINO@SHOP@SPECIAL_PEDS@DEAN@01A@IDLES"
					tlPedAnimClip = "IDLE_B"
				BREAK
				CASE 2
					casinoPed.sPedAnimDict = "ANIM@AMB@CASINO@SHOP@SPECIAL_PEDS@DEAN@01A@IDLES"
					tlPedAnimClip = "IDLE_C"
				BREAK
			ENDSWITCH
			iConversationA = -1
		BREAK
		CASE 2
			IF iConversationB != -1
				iConversationA = iConversationB
			ELSE
				iConversationA = GET_NEXT_CONVERSATION(5,g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_DEAN].iBitSetConversation)
			ENDIF
			SET_BIT(g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_DEAN].iBitSetConversation,iConversationA)
			
//			#IF IS_DEBUG_BUILD
//			IF g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_DEAN] > -1
//				iConversationA = g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_DEAN]
//			ENDIF
//			#ENDIF
			
			SWITCH iConversationA
				CASE 0
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@DEAN@SHOP@"
					tlPedAnimClip = "MAKE_SOUP_SP_DEAN"
				BREAK
				CASE 1
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@DEAN@SHOP@"
					tlPedAnimClip = "DONT_TOUCH_SP_DEAN"
				BREAK
				CASE 2
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@DEAN@SHOP@"
					tlPedAnimClip = "WINGS_SP_DEAN"
				BREAK
				CASE 3
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@DEAN@SHOP@"
					tlPedAnimClip = "DUST_SP_DEAN"
				BREAK
				CASE 4
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@DEAN@SHOP@"
					tlPedAnimClip = "MY_FEET_SP_DEAN"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CASINO_DEAN_SLOTS_ANIM_DATA(INT iSequence, INT &iConversationA, INT iConversationB, CASINO_PEDS &casinoPed, TEXT_LABEL_63 &tlPedAnimClip)
	PRINTLN("[CASINO_PEDS] GET_CASINO_DEAN_SLOTS_ANIM_DATA - iSequence ",iSequence)
	DEBUG_PRINTCALLSTACK()
	SWITCH iSequence
		CASE 0
		BREAK
		CASE 1
		BREAK
		CASE 2
			IF iConversationB != -1
				iConversationA = iConversationB
			ELSE
				iConversationA = GET_NEXT_CONVERSATION(5,g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_DEAN].iBitSetConversation)
			ENDIF
			SET_BIT(g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_DEAN].iBitSetConversation,iConversationA)
			
//			#IF IS_DEBUG_BUILD
//			IF g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_DEAN] > -1
//				iConversationA = g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_DEAN]
//			ENDIF
//			#ENDIF
			
			SWITCH iConversationA
				CASE 0
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@DEAN@SLOTS@"
					tlPedAnimClip = "FEELING_LUCKY_SP_DEAN"
				BREAK
				CASE 1
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@DEAN@SLOTS@"
					tlPedAnimClip = "HONEYMOON_SP_DEAN"
				BREAK
				CASE 2
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@DEAN@SLOTS@"
					tlPedAnimClip = "THAT_ACTOR_SP_DEAN"
				BREAK
				CASE 3
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@DEAN@SLOTS@"
					tlPedAnimClip = "WALLET_SP_DEAN"
				BREAK
				CASE 4
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@DEAN@SLOTS@"
					tlPedAnimClip = "THOSE_ROLLS_SP_DEAN"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CASINO_BETH_LOUNGE_ANIM_DATA(INT iSequence, INT &iConversation, CASINO_PEDS &casinoPed, TEXT_LABEL_63 &tlPedAnimClip)
	IF g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_BETH].bWaitingCouple
		iSequence = 0
	ENDIF
	SWITCH iSequence
		CASE 0
		CASE 1
			casinoPed.sPedAnimDict = "AMB@PROP_HUMAN_SEAT_CHAIR@FEMALE@PROPER@BASE"
			tlPedAnimClip = "BASE"
		BREAK
		CASE 2
			iConversation = GET_NEXT_CONVERSATION(10,g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_BETH].iBitSetConversation)
			SET_BIT(g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_BETH].iBitSetConversation,iConversation)
			
//			#IF IS_DEBUG_BUILD
//			IF g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_BETH] > -1
//				iConversation = g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_BETH]
//			ENDIF
//			#ENDIF
			
			SWITCH iConversation
				CASE 0
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@LOUNGE@"
					tlPedAnimClip = "ACTION1_BETH"
				BREAK
				CASE 1
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@LOUNGE@"
					tlPedAnimClip = "ACTION2_BETH"
				BREAK
				CASE 2
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@LOUNGE@"
					tlPedAnimClip = "ACTION3_BETH"
				BREAK
				CASE 3
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@LOUNGE@"
					tlPedAnimClip = "ACTION4_BETH"
				BREAK
				CASE 4
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@LOUNGE@"
					tlPedAnimClip = "ACTION5_BETH"
				BREAK
				CASE 5
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@LOUNGE@"
					tlPedAnimClip = "ACTION6_BETH"
				BREAK
				CASE 6
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@LOUNGE@"
					tlPedAnimClip = "ACTION7_BETH"
				BREAK
				CASE 7
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@LOUNGE@"
					tlPedAnimClip = "ACTION8_BETH"
				BREAK
				CASE 8
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@LOUNGE@"
					tlPedAnimClip = "ACTION9_BETH"
				BREAK
				CASE 9
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@LOUNGE@"
					tlPedAnimClip = "ACTION10_BETH"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CASINO_BETH_WHEEL_ANIM_DATA(INT iSequence, INT &iConversation, CASINO_PEDS &casinoPed, TEXT_LABEL_63 &tlPedAnimClip)
	IF g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_BETH].bWaitingCouple
		iSequence = 0
	ENDIF
	SWITCH iSequence
		CASE 0
		CASE 1
			casinoPed.sPedAnimDict = "ANIM@AMB@CASINO@HANGOUT@PED_FEMALE@STAND@01A@BASE"
			tlPedAnimClip = "BASE"
		BREAK
		CASE 2
			iConversation = GET_NEXT_CONVERSATION(10,g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_BETH].iBitSetConversation)
			SET_BIT(g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_BETH].iBitSetConversation,iConversation)
			
//			#IF IS_DEBUG_BUILD
//			IF g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_BETH] > -1
//				iConversation = g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_BETH]
//			ENDIF
//			#ENDIF
			
			SWITCH iConversation
				CASE 0
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@WHEEL@"
					tlPedAnimClip = "ACTION1_BETH"
				BREAK
				CASE 1
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@WHEEL@"
					tlPedAnimClip = "ACTION2_BETH"
				BREAK
				CASE 2
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@WHEEL@"
					tlPedAnimClip = "ACTION3_BETH"
				BREAK
				CASE 3
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@WHEEL@"
					tlPedAnimClip = "ACTION4_BETH"
				BREAK
				CASE 4
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@WHEEL@"
					tlPedAnimClip = "ACTION5_BETH"
				BREAK
				CASE 5
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@WHEEL@"
					tlPedAnimClip = "ACTION6_BETH"
				BREAK
				CASE 6
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@WHEEL@"
					tlPedAnimClip = "ACTION7_BETH"
				BREAK
				CASE 7
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@WHEEL@"
					tlPedAnimClip = "ACTION8_BETH"
				BREAK
				CASE 8
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@WHEEL@"
					tlPedAnimClip = "ACTION9_BETH"
				BREAK
				CASE 9
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@BETH@WHEEL@"
					tlPedAnimClip = "ACTION10_BETH"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CASINO_LAUREN_BAR_ANIM_DATA(INT iSequence, INT &iConversationA, INT iConversationB, CASINO_PEDS &casinoPed, TEXT_LABEL_63 &tlPedAnimClip)
	IF g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_LAUREN].bWaitingCouple
		iSequence = 0
	ENDIF
	SWITCH iSequence
		CASE 0
		CASE 1
			casinoPed.sPedAnimDict = "ANIM@AMB@CASINO@BAR@SPECIAL_PEDS@LAUREN@01A@BASE@"
			tlPedAnimClip = "BASE_LAUREN"
			iConversationA = -1
		BREAK
		CASE 2
			IF iConversationB != -1
				iConversationA = iConversationB
			ELSE
				iConversationA = GET_NEXT_CONVERSATION(5,g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_LAUREN].iBitSetConversation)
			ENDIF
			SET_BIT(g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_LAUREN].iBitSetConversation,iConversationA)
			
//			#IF IS_DEBUG_BUILD
//			IF g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_LAUREN] > -1
//				iConversationA = g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_LAUREN]
//			ENDIF
//			#ENDIF
			
			SWITCH iConversationA
				CASE 0
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@LAUREN@BAR@"
					tlPedAnimClip = "CAS_LAUTAY_IG1_LAUREN"
				BREAK
				CASE 1
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@LAUREN@BAR@"
					tlPedAnimClip = "CAS_LAUTAY_IG2_LAUREN"
				BREAK
				CASE 2
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@LAUREN@BAR@"
					tlPedAnimClip = "CAS_LAUTAY_IG3_LAUREN"
				BREAK
				CASE 3
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@LAUREN@BAR@"
					tlPedAnimClip = "CAS_LAUTAY_IG4_LAUREN"
				BREAK
				CASE 4
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@LAUREN@BAR@"
					tlPedAnimClip = "CAS_LAUTAY_IG5_LAUREN"
				BREAK	
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CASINO_TAYLOR_BAR_ANIM_DATA(INT iSequence, INT &iConversationA, INT iConversationB, CASINO_PEDS &casinoPed, TEXT_LABEL_63 &tlPedAnimClip)
	IF g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_TAYLOR].bWaitingCouple
		iSequence = 0
	ENDIF
	SWITCH iSequence
		CASE 0
		CASE 1
			casinoPed.sPedAnimDict = "ANIM@AMB@CASINO@BAR@SPECIAL_PEDS@TAYLOR@01A@BASE@"
			tlPedAnimClip = "BASE_TAYLOR"
			iConversationA = -1
		BREAK
		CASE 2
			IF iConversationB != -1
				iConversationA = iConversationB
			ELSE
				iConversationA = GET_NEXT_CONVERSATION(5,g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_TAYLOR].iBitSetConversation)
			ENDIF
			SET_BIT(g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_TAYLOR].iBitSetConversation,iConversationA)
			
//			#IF IS_DEBUG_BUILD
//			IF g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_TAYLOR] > -1
//				iConversationA = g_iCasinoSpecialPedOverrideConversation[CASINO_SPECIAL_PED_NAME_TAYLOR]
//			ENDIF
//			#ENDIF
			
			SWITCH iConversationA
				CASE 0
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@TAYLOR@BAR@"
					tlPedAnimClip = "CAS_LAUTAY_IG1_TAYLOR"
				BREAK
				CASE 1
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@TAYLOR@BAR@"
					tlPedAnimClip = "CAS_LAUTAY_IG2_TAYLOR"
				BREAK
				CASE 2
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@TAYLOR@BAR@"
					tlPedAnimClip = "CAS_LAUTAY_IG3_TAYLOR"
				BREAK
				CASE 3
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@TAYLOR@BAR@"
					tlPedAnimClip = "CAS_LAUTAY_IG4_TAYLOR"
				BREAK
				CASE 4
					casinoPed.sPedAnimDict = "ANIM@SPECIAL_PEDS@CASINO@TAYLOR@BAR@"
					tlPedAnimClip = "CAS_LAUTAY_IG5_TAYLOR"
				BREAK	
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CASINO_SPECIAL_PED_CALEB_DATA(CASINO_PED_DATA &pedData)
	pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("u_m_y_caleb"))
	pedData.ppsProps[ANCHOR_HEAD].iPropIndex = -1
	pedData.ppsProps[ANCHOR_EYES].iPropIndex = -1
	INT j
	REPEAT NUM_PED_COMPONENTS j
		pedData.pcsComponents[j].NewDrawableNumber = 0
		pedData.pcsComponents[j].NewTextureNumber = 0
		pedData.pcsComponents[j].NewPaletteNumber = 0
	ENDREPEAT
ENDPROC

PROC GET_CASINO_SPECIAL_PED_USHI_DATA(CASINO_PED_DATA &pedData)
	pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("u_m_y_ushi"))
	pedData.ppsProps[ANCHOR_HEAD].iPropIndex = -1
	pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0
	INT j
	REPEAT NUM_PED_COMPONENTS j
		pedData.pcsComponents[j].NewDrawableNumber = 0
		pedData.pcsComponents[j].NewTextureNumber = 0
		pedData.pcsComponents[j].NewPaletteNumber = 0
	ENDREPEAT
ENDPROC

PROC GET_CASINO_SPECIAL_PED_GABRIEL_DATA(CASINO_PED_DATA &pedData)
	pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("u_m_y_gabriel"))
	pedData.ppsProps[ANCHOR_HEAD].iPropIndex = -1
	pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0
	INT j
	REPEAT NUM_PED_COMPONENTS j
		pedData.pcsComponents[j].NewDrawableNumber = 0
		pedData.pcsComponents[j].NewTextureNumber = 0
		pedData.pcsComponents[j].NewPaletteNumber = 0
	ENDREPEAT
ENDPROC

PROC GET_CASINO_SPECIAL_PED_VINCE_DATA(CASINO_PED_DATA &pedData)
	pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("u_m_m_vince"))
	pedData.ppsProps[ANCHOR_HEAD].iPropIndex = -1
	pedData.ppsProps[ANCHOR_EYES].iPropIndex = -1
	INT j
	REPEAT NUM_PED_COMPONENTS j
		pedData.pcsComponents[j].NewDrawableNumber = 0
		pedData.pcsComponents[j].NewTextureNumber = 0
		pedData.pcsComponents[j].NewPaletteNumber = 0
	ENDREPEAT
ENDPROC

PROC GET_CASINO_SPECIAL_PED_DEAN_DATA(CASINO_PED_DATA &pedData)
	pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("u_m_o_Dean"))
	pedData.ppsProps[ANCHOR_HEAD].iPropIndex = -1
	pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0
	INT j
	REPEAT NUM_PED_COMPONENTS j
		pedData.pcsComponents[j].NewDrawableNumber = 0
		pedData.pcsComponents[j].NewTextureNumber = 0
		pedData.pcsComponents[j].NewPaletteNumber = 0
	ENDREPEAT
ENDPROC

PROC GET_CASINO_SPECIAL_PED_CAROL_DATA(CASINO_PED_DATA &pedData)
	pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("u_f_o_Carol"))
	pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 0
	pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0
	INT j
	REPEAT NUM_PED_COMPONENTS j
		pedData.pcsComponents[j].NewDrawableNumber = 0
		pedData.pcsComponents[j].NewTextureNumber = 0
		pedData.pcsComponents[j].NewPaletteNumber = 0
	ENDREPEAT

ENDPROC

PROC GET_CASINO_SPECIAL_PED_BETH_DATA(CASINO_PED_DATA &pedData)
	pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("u_f_y_beth"))
	pedData.ppsProps[ANCHOR_HEAD].iPropIndex = -1
	pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0
	INT j
	REPEAT NUM_PED_COMPONENTS j
		pedData.pcsComponents[j].NewDrawableNumber = 0
		pedData.pcsComponents[j].NewTextureNumber = 0
		pedData.pcsComponents[j].NewPaletteNumber = 0
	ENDREPEAT
ENDPROC

PROC GET_CASINO_SPECIAL_PED_LAUREN_DATA(CASINO_PED_DATA &pedData)
	pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("u_f_y_lauren"))
	pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 0
	pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0
	INT j
	REPEAT NUM_PED_COMPONENTS j
		pedData.pcsComponents[j].NewDrawableNumber = 0
		pedData.pcsComponents[j].NewTextureNumber = 0
		pedData.pcsComponents[j].NewPaletteNumber = 0
	ENDREPEAT
ENDPROC

PROC GET_CASINO_SPECIAL_PED_TAYLOR_DATA(CASINO_PED_DATA &pedData)
	pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("u_f_y_taylor"))
	pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 0
	pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0
	INT j
	REPEAT NUM_PED_COMPONENTS j
		pedData.pcsComponents[j].NewDrawableNumber = 0
		pedData.pcsComponents[j].NewTextureNumber = 0
		pedData.pcsComponents[j].NewPaletteNumber = 0
	ENDREPEAT
ENDPROC

PROC GET_CASINO_SPECIAL_PED_BLANE_DATA(CASINO_PED_DATA &pedData)
	pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("u_m_m_blane"))
	pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 0
	pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0
	INT j
	REPEAT NUM_PED_COMPONENTS j
		pedData.pcsComponents[j].NewDrawableNumber = 0
		pedData.pcsComponents[j].NewTextureNumber = 0
		pedData.pcsComponents[j].NewPaletteNumber = 0
	ENDREPEAT
ENDPROC

PROC GET_CASINO_SPECIAL_PED_EILEEN_DATA(CASINO_PED_DATA &pedData)
	pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("u_f_o_eileen"))
	pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 0
	pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0
	INT j
	REPEAT NUM_PED_COMPONENTS j
		pedData.pcsComponents[j].NewDrawableNumber = 0
		pedData.pcsComponents[j].NewTextureNumber = 0
		pedData.pcsComponents[j].NewPaletteNumber = 0
	ENDREPEAT
ENDPROC

PROC GET_CASINO_SPECIAL_PED_CURTIS_DATA(CASINO_PED_DATA &pedData)
	pedData.mnPedModel = INT_TO_ENUM(MODEL_NAMES, HASH("u_m_m_curtis"))
	pedData.ppsProps[ANCHOR_HEAD].iPropIndex = 0
	pedData.ppsProps[ANCHOR_EYES].iPropIndex = 0
	INT j
	REPEAT NUM_PED_COMPONENTS j
		pedData.pcsComponents[j].NewDrawableNumber = 0
		pedData.pcsComponents[j].NewTextureNumber = 0
		pedData.pcsComponents[j].NewPaletteNumber = 0
	ENDREPEAT
ENDPROC

#IF IS_DEBUG_BUILD

FUNC INT GET_CASINO_SPECIAL_PED_ACTIVITY_CONVERSATION_COUNT(CASINO_ACTIVITY_SLOTS eActivity)
	SWITCH eActivity
		CASE CASINO_SPECIAL_PED_VINCE_BAR		RETURN 11
		CASE CASINO_SPECIAL_PED_VINCE_STEPS		RETURN 10
		CASE CASINO_SPECIAL_PED_VINCE_WALL		RETURN 10
		CASE CASINO_SPECIAL_PED_CAROL_SHOP		RETURN 5
		CASE CASINO_SPECIAL_PED_CAROL_SLOTS		RETURN 5
		CASE CASINO_SPECIAL_PED_DEAN_SHOP		RETURN 5
		CASE CASINO_SPECIAL_PED_DEAN_SLOTS		RETURN 5
		CASE CASINO_SPECIAL_PED_BETH_LOUNGE		RETURN 10
		CASE CASINO_SPECIAL_PED_BETH_WHEEL		RETURN 10
		CASE CASINO_SPECIAL_PED_LAUREN_BAR		RETURN 5
		CASE CASINO_SPECIAL_PED_TAYLOR_BAR		RETURN 5
	ENDSWITCH 
	RETURN 0
ENDFUNC

FUNC CASINO_SPECIAL_PED_NAME DEBUG_GET_CASINO_SPECIAL_PED_TYPE(INT iSpecialPedIndex)
	SWITCH iSpecialPedIndex
		CASE 0	RETURN CASINO_SPECIAL_PED_NAME_BETH
		CASE 1	RETURN CASINO_SPECIAL_PED_NAME_BLANE
		CASE 2	RETURN CASINO_SPECIAL_PED_NAME_CALEB
		CASE 3	RETURN CASINO_SPECIAL_PED_NAME_CURTIS
		CASE 4	RETURN CASINO_SPECIAL_PED_NAME_CAROL
		CASE 5	RETURN CASINO_SPECIAL_PED_NAME_DEAN
		CASE 6	RETURN CASINO_SPECIAL_PED_NAME_EILEEN
		CASE 7	RETURN CASINO_SPECIAL_PED_NAME_GABRIEL
		CASE 8	RETURN CASINO_SPECIAL_PED_NAME_LAUREN
		CASE 9	RETURN CASINO_SPECIAL_PED_NAME_TAYLOR
		CASE 10	RETURN CASINO_SPECIAL_PED_NAME_USHI
		CASE 11	RETURN CASINO_SPECIAL_PED_NAME_VINCE
	ENDSWITCH
	RETURN CASINO_SPECIAL_PED_NAME_INVALID
ENDFUNC

FUNC STRING DEBUG_GET_CASINO_SPECIAL_PED_NAME_STRING(CASINO_SPECIAL_PED_NAME enumItem)
	SWITCH enumItem
		CASE CASINO_SPECIAL_PED_NAME_BETH		RETURN "BETH"
		CASE CASINO_SPECIAL_PED_NAME_BLANE		RETURN "BLANE"
		CASE CASINO_SPECIAL_PED_NAME_CALEB		RETURN "CALEB"
		CASE CASINO_SPECIAL_PED_NAME_CURTIS		RETURN "CURTIS"
		CASE CASINO_SPECIAL_PED_NAME_DEAN  		RETURN "DEAN + CAROL"
		CASE CASINO_SPECIAL_PED_NAME_EILEEN		RETURN "EILEEN"
		CASE CASINO_SPECIAL_PED_NAME_GABRIEL	RETURN "GABRIEL"
		CASE CASINO_SPECIAL_PED_NAME_LAUREN		RETURN "LAUREN + TAYLOR"
		CASE CASINO_SPECIAL_PED_NAME_USHI		RETURN "USHI"
		CASE CASINO_SPECIAL_PED_NAME_VINCE		RETURN "VINCE"
	ENDSWITCH								
	RETURN " "
ENDFUNC

FUNC BOOL DEBUG_PROCESS_SPECIAL_PED_WIDGET(CASINO_SPECIAL_PED_NAME eSpecialPed)
	SWITCH eSpecialPed
		CASE CASINO_SPECIAL_PED_NAME_INVALID
		CASE CASINO_SPECIAL_PED_NAME_CAROL
		CASE CASINO_SPECIAL_PED_NAME_TAYLOR
			RETURN FALSE
	ENDSWITCH
	RETURN TRUE
ENDFUNC

FUNC CASINO_SPECIAL_PED_AREA GET_CASINO_SPECIAL_PED_SCENARIO(INT iScenarioID)
	CASINO_SPECIAL_PED_AREA eScenario = CASINO_SPECIAL_PED_AREA_INVALID
	SWITCH iScenarioID
		// CASINO_SPECIAL_PED_NAME_CALEB
		CASE 0	eScenario = CASINO_SPECIAL_PED_AREA_POKER		BREAK
		CASE 1	eScenario = CASINO_SPECIAL_PED_AREA_BLACKJACK	BREAK
		CASE 2	eScenario = CASINO_SPECIAL_PED_AREA_SLOTS		BREAK
		// CASINO_SPECIAL_PED_NAME_USHI
		CASE 3	eScenario = CASINO_SPECIAL_PED_AREA_POKER		BREAK
		CASE 4  eScenario = CASINO_SPECIAL_PED_AREA_ROULLETTE	BREAK
		CASE 5  eScenario = CASINO_SPECIAL_PED_AREA_TRACK		BREAK
		// CASINO_SPECIAL_PED_NAME_GABRIEL
		CASE 6	eScenario = CASINO_SPECIAL_PED_AREA_POKER		BREAK
		CASE 7	eScenario = CASINO_SPECIAL_PED_AREA_ROULLETTE	BREAK
		CASE 8	eScenario = CASINO_SPECIAL_PED_AREA_BAR			BREAK
		// CASINO_SPECIAL_PED_NAME_VINCE
		CASE 9	eScenario = CASINO_SPECIAL_PED_AREA_BAR			BREAK
		CASE 10 eScenario = CASINO_SPECIAL_PED_AREA_CASHIER		BREAK
		CASE 11 eScenario = CASINO_SPECIAL_PED_AREA_HALLWAY		BREAK
		// CASINO_SPECIAL_PED_NAME_DEAN
		CASE 12	eScenario = CASINO_SPECIAL_PED_AREA_STORE		BREAK
		CASE 13 eScenario = CASINO_SPECIAL_PED_AREA_SLOTS		BREAK
		CASE 14 eScenario = CASINO_SPECIAL_PED_AREA_BLACKJACK	BREAK
		// CASINO_SPECIAL_PED_NAME_CAROL
		CASE 15	eScenario = CASINO_SPECIAL_PED_AREA_STORE		BREAK
		CASE 16 eScenario = CASINO_SPECIAL_PED_AREA_SLOTS		BREAK
		CASE 17 eScenario = CASINO_SPECIAL_PED_AREA_BLACKJACK	BREAK
		// CASINO_SPECIAL_PED_NAME_BETH
		CASE 18	eScenario = CASINO_SPECIAL_PED_AREA_BAR			BREAK
		CASE 19 eScenario = CASINO_SPECIAL_PED_AREA_SPIN_WHEEL	BREAK
		CASE 20 eScenario = CASINO_SPECIAL_PED_AREA_BLACKJACK	BREAK
		// CASINO_SPECIAL_PED_NAME_LAUREN
		CASE 21	eScenario = CASINO_SPECIAL_PED_AREA_BAR			BREAK
		CASE 22 eScenario = CASINO_SPECIAL_PED_AREA_BLACKJACK	BREAK
		CASE 23 eScenario = CASINO_SPECIAL_PED_AREA_ROULLETTE	BREAK
		// CASINO_SPECIAL_PED_NAME_TAYLOR
		CASE 24	eScenario = CASINO_SPECIAL_PED_AREA_BAR			BREAK
		CASE 25 eScenario = CASINO_SPECIAL_PED_AREA_BLACKJACK	BREAK
		CASE 26 eScenario = CASINO_SPECIAL_PED_AREA_ROULLETTE	BREAK
		// CASINO_SPECIAL_PED_NAME_BLANE
		CASE 27	eScenario = CASINO_SPECIAL_PED_AREA_ROULLETTE	BREAK
		CASE 28 eScenario = CASINO_SPECIAL_PED_AREA_BLACKJACK	BREAK
		CASE 29 eScenario = CASINO_SPECIAL_PED_AREA_TRACK		BREAK
		// CASINO_SPECIAL_PED_NAME_EILEEN
		CASE 30	eScenario = CASINO_SPECIAL_PED_AREA_SLOTS		BREAK
		CASE 31 eScenario = CASINO_SPECIAL_PED_AREA_SLOTS		BREAK
		CASE 32 eScenario = CASINO_SPECIAL_PED_AREA_SLOTS		BREAK
		// CASINO_SPECIAL_PED_NAME_CURTIS
		CASE 33	eScenario = CASINO_SPECIAL_PED_AREA_POKER		BREAK
		CASE 34 eScenario = CASINO_SPECIAL_PED_AREA_ROULLETTE	BREAK
		CASE 35 eScenario = CASINO_SPECIAL_PED_AREA_BLACKJACK	BREAK
	ENDSWITCH
	RETURN eScenario
ENDFUNC

PROC CREATE_CASINO_SPECIAL_PED_WIDGETS()
	
	START_WIDGET_GROUP("Special Peds")
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			ADD_WIDGET_BOOL("Disable Special Peds for the next run",g_bCasinoSpecialPedsDisable)
			ADD_WIDGET_BOOL("Keep Special Peds for the next run",g_bCasinoSpecialPedsKeep)
		ELSE
			ADD_WIDGET_STRING("Only the script host can modify this.")
		ENDIF
		
		INT iSpecialPedLoop
		TEXT_LABEL_63 tlScenario[3]
		
		START_WIDGET_GROUP("Instructions")
			ADD_WIDGET_STRING("-Tick a scenario and then tick 'Trigger Scenario Change' to update special peds scenario")
			ADD_WIDGET_STRING("-If scenario does not updated then another special ped is using that scenario")
		STOP_WIDGET_GROUP()
		START_WIDGET_GROUP("Notes")
			ADD_WIDGET_STRING("-Do not tick 'Trigger Scenario Change' with the 'Activate Editor' widget also ticked")
			ADD_WIDGET_STRING("-Ped refresh occurs in main floor area. If testing different areas go back to main floor and walk across culling zone")
			ADD_WIDGET_STRING("-'L' key on debug keyboard to see locates (culling zones)")
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Peds")
			REPEAT MAX_CASINO_SPECIAL_PED_NAME iSpecialPedLoop
				CASINO_SPECIAL_PED_NAME eSpecialPed = DEBUG_GET_CASINO_SPECIAL_PED_TYPE(iSpecialPedLoop)
				IF DEBUG_PROCESS_SPECIAL_PED_WIDGET(eSpecialPed)
					STRING sSpecialPedName = DEBUG_GET_CASINO_SPECIAL_PED_NAME_STRING(eSpecialPed)
					IF NOT IS_STRING_NULL_OR_EMPTY(sSpecialPedName)
						g_iCasinoSpecialPeds[eSpecialPed] = g_sSpecialPeds[eSpecialPed].iPedID
						
						START_WIDGET_GROUP(sSpecialPedName)
							SWITCH eSpecialPed
								CASE CASINO_SPECIAL_PED_NAME_DEAN
									ADD_WIDGET_BOOL("Dean Currently Spawned?", g_bCasinoSpecialPedAlive[CASINO_SPECIAL_PED_NAME_CAROL])
									ADD_WIDGET_BOOL("Carol Currently Spawned?", g_bCasinoSpecialPedAlive[CASINO_SPECIAL_PED_NAME_CAROL])
									ADD_WIDGET_INT_READ_ONLY("Dean Ped ID", g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_DEAN].iPedID)
									ADD_WIDGET_INT_READ_ONLY("Carol Ped ID", g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_CAROL].iPedID)
									g_twCasinoSpecialPedAssignedArea[CASINO_SPECIAL_PED_NAME_CAROL] = ADD_TEXT_WIDGET("Area peds are in: ")
									
									ADD_WIDGET_BOOL("Scenario 1: Casino Store", g_bCasinoSpecialPedScenarios[ENUM_TO_INT(CASINO_SPECIAL_PED_NAME_DEAN)*3])
									ADD_WIDGET_BOOL("Scenario 2: Slot Machines", g_bCasinoSpecialPedScenarios[(ENUM_TO_INT(CASINO_SPECIAL_PED_NAME_DEAN)*3)+1])
									ADD_WIDGET_BOOL("Scenario 3: Regular Blackjack", g_bCasinoSpecialPedScenarios[(ENUM_TO_INT(CASINO_SPECIAL_PED_NAME_DEAN)*3)+2])
									g_twCasinoSpecialPedActiveScenario[CASINO_SPECIAL_PED_NAME_DEAN] = ADD_TEXT_WIDGET("Active scenario: ")
									ADD_WIDGET_BOOL("Trigger Scenario Change", g_bCasinoSpecialPedUpdateScenario[eSpecialPed])
								BREAK
								CASE CASINO_SPECIAL_PED_NAME_LAUREN
									ADD_WIDGET_BOOL("Lauren Currently Spawned?", g_bCasinoSpecialPedAlive[CASINO_SPECIAL_PED_NAME_TAYLOR])
									ADD_WIDGET_BOOL("Taylor Currently Spawned?", g_bCasinoSpecialPedAlive[CASINO_SPECIAL_PED_NAME_TAYLOR])
									ADD_WIDGET_INT_READ_ONLY("Lauren Ped ID", g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_LAUREN].iPedID)
									ADD_WIDGET_INT_READ_ONLY("Taylor Ped ID", g_sSpecialPeds[CASINO_SPECIAL_PED_NAME_TAYLOR].iPedID)
									g_twCasinoSpecialPedAssignedArea[CASINO_SPECIAL_PED_NAME_TAYLOR] = ADD_TEXT_WIDGET("Area peds are in: ")
									
									ADD_WIDGET_BOOL("Scenario 1: At the Bar", g_bCasinoSpecialPedScenarios[ENUM_TO_INT(CASINO_SPECIAL_PED_NAME_LAUREN)*3])
									ADD_WIDGET_BOOL("Scenario 2: Regular Blackjack", g_bCasinoSpecialPedScenarios[(ENUM_TO_INT(CASINO_SPECIAL_PED_NAME_LAUREN)*3)+1])
									ADD_WIDGET_BOOL("Scenario 3: Regular Roulette", g_bCasinoSpecialPedScenarios[(ENUM_TO_INT(CASINO_SPECIAL_PED_NAME_LAUREN)*3)+2])
									g_twCasinoSpecialPedActiveScenario[CASINO_SPECIAL_PED_NAME_LAUREN] = ADD_TEXT_WIDGET("Active scenario: ")	
									ADD_WIDGET_BOOL("Trigger Scenario Change", g_bCasinoSpecialPedUpdateScenario[eSpecialPed])
								BREAK
								DEFAULT
									ADD_WIDGET_BOOL("Currently Spawned?", g_bCasinoSpecialPedAlive[eSpecialPed])
									ADD_WIDGET_INT_READ_ONLY("Ped ID", g_iCasinoSpecialPeds[eSpecialPed])
									g_twCasinoSpecialPedAssignedArea[eSpecialPed] = ADD_TEXT_WIDGET("Area ped is in: ")
									GET_CASINO_SPECIAL_PED_SCENARIO_TEXT_LABEL(tlScenario, eSpecialPed)
									
									IF NOT IS_STRING_NULL_OR_EMPTY(tlScenario[0])
										ADD_WIDGET_BOOL(tlScenario[0], g_bCasinoSpecialPedScenarios[ENUM_TO_INT(eSpecialPed)*3])
										ADD_WIDGET_BOOL(tlScenario[1], g_bCasinoSpecialPedScenarios[(ENUM_TO_INT(eSpecialPed)*3)+1])
										ADD_WIDGET_BOOL(tlScenario[2], g_bCasinoSpecialPedScenarios[(ENUM_TO_INT(eSpecialPed)*3)+2])
										g_twCasinoSpecialPedActiveScenario[eSpecialPed] = ADD_TEXT_WIDGET("Active scenario: ")
										ADD_WIDGET_BOOL("Trigger Scenario Change", g_bCasinoSpecialPedUpdateScenario[eSpecialPed])
									ENDIF
								BREAK
							ENDSWITCH
						STOP_WIDGET_GROUP()
					ENDIF
				ENDIF
			ENDREPEAT
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
ENDPROC

#ENDIF //IS_DEBUG_BUILD
