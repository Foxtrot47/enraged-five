// name:        net_mission_CnC.sch
// description: Data on all CnC missions.

USING "globals.sch"
USING "net_include.sch"
USING "net_events.sch"

USING "net_mission_control.sch"
USING "net_scoring_common.sch"
USING "net_mission_info.sch"
USING "FM_Mini_Game_Header.sch"

USING "net_mission_control_broadcasts.sch"		// PROBABLY TEMP: KGM 25/4/12: The 'no join' and 'secondary teams' broadcasts will end up coming from the mission directly
USING "net_mission_trigger_overview.sch"
USING "net_transition_sessions_private.sch"
USING "MP_globals_UGC.sch"
USING "freemode_events_header_private.sch"

#IF IS_DEBUG_BUILD 
USING "net_debug.sch"
#ENDIF



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////// Missions Checks ///////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE: Checks if a mission is of a specified type.
FUNC BOOL IS_MP_MISSION_OF_TYPE(MP_MISSION mission, MP_MISSION_TYPE typeToCheck)
	RETURN GET_MP_MISSION_TYPE(mission) = typeToCheck
ENDFUNC

/// PURPOSE: Checks if a mission is a heist.
FUNC BOOL IS_MP_MISSION_A_PRE_HEIST(MP_MISSION mission)
	RETURN IS_MP_MISSION_OF_TYPE(mission, eTYPE_PRE_HEIST)
ENDFUNC

/// PURPOSE: Gets if the mission has been setup as a random event.
FUNC BOOL IS_MP_MISSION_A_RANDOM_EVENT(MP_MISSION mission)
	RETURN IS_MP_MISSION_OF_TYPE(mission, eTYPE_RANDOM_EVENT)
ENDFUNC

/// PURPOSE: Gets if the mission has been setup as a tutorial.
FUNC BOOL IS_MP_MISSION_A_TUTORIAL(MP_MISSION mission)
	RETURN IS_MP_MISSION_OF_TYPE(mission, eTYPE_TUTORIAL)
ENDFUNC

FUNC BOOL IS_MP_MISSION_A_GM(MP_MISSION mission)
	RETURN IS_MP_MISSION_OF_TYPE(mission, eTYPE_GROUP_MISSION)
ENDFUNC

//Returns true if a blayer is in a FMMC corona
FUNC BOOL IS_PLAYER_IN_A_FMMC_CORONA(PLAYER_INDEX playerId)
	RETURN IS_BIT_SET(GlobalplayerBD_FM_2[NATIVE_TO_INT(playerID)].iBitSet, biIwantToVote)
ENDFUNC

/// PURPOSE: Gets the max number of players per team that can join the mission.
FUNC INT GET_MAX_NUM_PARTICIPANTS_IN_TEAM_FOR_MP_MISSION(INT Team, MP_MISSION mission)

	IF mission = eNULL_MISSION
		RETURN 0
	ENDIF

	
	SWITCH Team
		DEFAULT 
			NET_PRINT("GET_MAX_NUM_PARTICIPANTS_IN_TEAM_FOR_MP_MISSION - called with team = ")
			NET_PRINT_INT(Team)
			NET_NL()
			NET_SCRIPT_ASSERT("GET_MAX_NUM_PARTICIPANTS_IN_TEAM_FOR_MP_MISSION - invalid team!")
		BREAK
	ENDSWITCH
		
	RETURN 0

ENDFUNC

/// PURPOSE:  Clears a player's mission data, so they will no longer be considered as on a mission. 
PROC CLEAR_PLAYER_MP_MISSION_DATA()
	MP_MISSION_DATA EmptyData
	GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].activeMission = EmptyData
ENDPROC

/// PURPOSE:  Gets the mission data for a player.
FUNC MP_MISSION_DATA GET_PLAYER_MP_MISSION_DATA(PLAYER_INDEX playerId)
	IF IS_NET_PLAYER_OK(playerId, FALSE, TRUE)
		RETURN GlobalplayerBD[NATIVE_TO_INT(playerID)].activeMission
	ENDIF	   
	MP_MISSION_DATA missionData
	RETURN missionData
ENDFUNC

/// PURPOSE: Gets the mission the player is on and the varaition 
FUNC MP_MISSION GET_MP_MISSION_PLAYER_IS_ON_WITH_VARAITION(PLAYER_INDEX playerId, INT &missionVariation)
	IF IS_NET_PLAYER_OK(playerId, FALSE, TRUE)
		missionVariation =  GlobalplayerBD[NATIVE_TO_INT(playerID)].activeMission.mdID.idVariation
		RETURN GlobalplayerBD[NATIVE_TO_INT(playerID)].activeMission.mdID.idMission
	ENDIF
	RETURN eNULL_MISSION
ENDFUNC


/// PURPOSE: Gets the mission the player is on.
FUNC MP_MISSION GET_MP_MISSION_PLAYER_IS_ON(PLAYER_INDEX playerId)
	IF IS_NET_PLAYER_OK(playerId, FALSE, TRUE)
		RETURN GlobalplayerBD[NATIVE_TO_INT(playerID)].activeMission.mdID.idMission
	ENDIF
	RETURN eNULL_MISSION
ENDFUNC

FUNC BOOL IS_PLAYER_ON_MP_MISSION(PLAYER_INDEX playerID, MP_MISSION Mission)
	RETURN (Mission = GET_MP_MISSION_PLAYER_IS_ON(playerID))
ENDFUNC


//Set a player is on a Head 2 head or not
PROC SET_PLAYER_ON_A_H2H_PLAYLIST()
	PRINTLN("SET_PLAYER_ON_A_H2H_PLAYLIST")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_ON_A_H2H_PLAYLIST)
ENDPROC
//Clear a player is on a Head 2 head or not
PROC CLEAR_PLAYER_ON_A_H2H_PLAYLIST()
	PRINTLN("CLEAR_PLAYER_ON_A_H2H_PLAYLIST")
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_ON_A_H2H_PLAYLIST)
ENDPROC
//Retunrs if a player is on a Head 2 head or not
FUNC BOOL IS_PLAYER_ON_A_H2H_PLAYLIST(PLAYER_INDEX playerID)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_ON_A_H2H_PLAYLIST)
ENDFUNC

//Set if a player is setting a challenge time on playlist or not!
PROC SET_PLAYER_SETTING_CHALLENGE()
	PRINTLN("SET_PLAYER_SETTING_CHALLENGE")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_SETTING_CHALLENGE_PLAYLIST)
ENDPROC
//Clear if a player is setting a challenge time on playlist or not!
PROC CLEAR_PLAYER_SETTING_CHALLENGE()
	PRINTLN("CLEAR_PLAYER_SETTING_CHALLENGE")
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_SETTING_CHALLENGE_PLAYLIST)
ENDPROC
//Returns if a player is settign a challenge time on playlist or not!
FUNC BOOL IS_PLAYER_SETTING_CHALLENGE(PLAYER_INDEX playerID)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_SETTING_CHALLENGE_PLAYLIST)
ENDFUNC


//Set the local player on a challenge
PROC SET_PLAYER_ON_A_CHALLENGE()
	PRINTLN("SET_PLAYER_ON_A_CHALLENGE")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_ON_A_CHALLENGE_PLAYLIST)
ENDPROC
//Clear the local player on a challenge
PROC CLEAR_PLAYER_ON_A_CHALLENGE()
	PRINTLN("CLEAR_PLAYER_ON_A_CHALLENGE")
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_ON_A_CHALLENGE_PLAYLIST)
ENDPROC
//Retunrs if a player is on a Challenge or not!
FUNC BOOL IS_PLAYER_ON_A_CHALLENGE(PLAYER_INDEX playerID)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_ON_A_CHALLENGE_PLAYLIST)
ENDFUNC

//Returns the current playlist mission file name
FUNC TEXT_LABEL_31 GET_CURRENT_PLAYLIST_MISSION_FILE_NAME()  
	RETURN g_sCurrentPlayListDetails.sLoadedMissionDetails[g_sCurrentPlayListDetails.iCurrentPlayListPosition].tlName
ENDFUNC

//Sets players Playlist state
PROC SET_PLAYER_ON_A_PLAYLIST(BOOL bState)
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	PRINTLN(" SET_PLAYER_ON_A_PLAYLIST bState = ", bState)
	IF bState
		SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_ON_A_PLAYLIST)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListBitSet, ciFMMC_MISSION_DATA_ON_A_PLAYLIST)
	ENDIF
ENDPROC
//Sets players Playlist state
PROC SET_PLAYERS_PLAYLIST_INSTANCE(INT iInstance)
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListInstance = iInstance
	PRINTLN("GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sPlaylistVars.iPlayListInstance = ", iInstance)
ENDPROC

FUNC BOOL IS_THIS_A_MINI_GAME(INT iMissionType)
	SWITCH iMissionType
		CASE FMMC_TYPE_MG_ARM_WRESTLING
		CASE FMMC_TYPE_MG_DARTS
		CASE FMMC_TYPE_MG_GOLF
		CASE FMMC_TYPE_MG_SHOOTING_RANGE
		CASE FMMC_TYPE_MG_TENNIS
		CASE FMMC_TYPE_MG_PILOT_SCHOOL
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC

///// PURPOSE:checks to see if a player is on ANY FM Job (Race, DM, Mission, Survival, Base Jump)
/////    
///// PARAMS:
/////    playerID - the player
///// RETURNS: True if that player is on a Job
//FUNC BOOL IS_PLAYER_ON_ANY_FM_JOB(PLAYER_INDEX playerID, BOOL bDoPlayListCheck = TRUE)
//	IF bDoPlayListCheck
//		IF IS_PLAYER_ON_A_PLAYLIST(playerID)
//			RETURN TRUE
//		ENDIF
//	ENDIF
//	
//	IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_RACE
//	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
//	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_MISSION
//	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_SURVIVAL
//	OR GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].iCurrentMissionType = FMMC_TYPE_BASE_JUMP
//		RETURN TRUE	
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC


//Returns true if a player entered correctly through the corona.
//FUNC BOOL DID_PLAYER_ENTER_CORONA_LATE(PLAYER_INDEX playerId)
//	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_ENTERED_CORONA_LATE)
//ENDFUNC


FUNC BOOL IS_PERSON_LAUNCHING_THE_SAME_MISSION_AS_ME(STRING stOwner, STRING stFile, INT iHostGBD)
	

	IF ARE_STRINGS_EQUAL (stOwner, GlobalplayerBD_FM[iHostGBD].tl23CurrentMissionOwner)
	AND ARE_STRINGS_EQUAL(stFile, GlobalplayerBD_FM[iHostGBD].tl31CurrentMissionFileName)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Returns true if this is a two team mission
FUNC  BOOL IS_THIS_A_TWO_TEAM_MISSION()
	//Not a mission
	IF (g_FMMC_STRUCT.iMissionType != FMMC_TYPE_MISSION)
		RETURN FALSE
	ENDIF
	//Two Teams
	RETURN (g_FMMC_STRUCT.iMaxNumberOfTeams = 2)
ENDFUNC


//Set this is a quick restart corona
PROC SET_SHOW_QUICK_RESTART_OPTION()
	PRINTLN("FMMC EOM [QUICK_RESTART] SET_SHOW_QUICK_RESTART_OPTION")
	g_sFMMCEOM.bShowQuickRestart = TRUE
ENDPROC
//Clear this is a quick restart corona
PROC CLEAR_SHOW_QUICK_RESTART_OPTION()
	PRINTLN("FMMC EOM [QUICK_RESTART] CLEAR_SHOW_QUICK_RESTART_OPTION")
	g_sFMMCEOM.bShowQuickRestart = FALSE
ENDPROC

FUNC BOOL ENOUGH_FOR_QUICK_RESTART()

	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceQuicRestart")
		RETURN TRUE
	ENDIF
	#ENDIF
	IF g_bFMMCCriticalFail
		//Send back to planning board to adjust teams
		RETURN FALSE
	ENDIF
	INT iMinPlayers = g_FMMC_STRUCT.iMinNumParticipants
	IF NOT DOES_CURRENT_MISSION_USE_GANG_BOSS()
		IF IS_BIT_SET(g_FMMC_STRUCT.iBS_MissionTeamRelations, FMMC_TEAM_BALANCING)
		OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetFive, ciSELECTABLE_TEAM_NUMBERS)
			INT iTeam
			iMinPlayers = 0
			REPEAT g_FMMC_STRUCT.iNumberOfTeams iTeam
				iMinPlayers += g_FMMC_STRUCT.iNumPlayersPerTeam[iTeam]
			ENDREPEAT
		ENDIF
	ENDIF
	IF (NETWORK_GET_NUM_PARTICIPANTS() >= iMinPlayers)
		INT iNoneSctvCount
		INT iPartLoop
		PLAYER_INDEX playerID
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPartLoop
			//If they are active
			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPartLoop))
				//Get player ID
				playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iPartLoop))	
				IF NOT IS_PLAYER_SCTV(playerID)
					iNoneSctvCount++
				ENDIF
			ENDIF
		ENDREPEAT	
		IF iNoneSctvCount >= iMinPlayers
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC
//Returns TRUE if this is a quick restart corona
FUNC BOOL SHOULD_SHOW_QUICK_RESTART_OPTION()
	IF ENOUGH_FOR_QUICK_RESTART()
	
		RETURN g_sFMMCEOM.bShowQuickRestart
	ENDIF
	
	RETURN FALSE
ENDFUNC

//Set this is a quick restart corona
PROC SET_CORONA_INITIALISING_A_QUICK_RESTART()
	PRINTLN("SET_CORONA_INITIALISING_A_QUICK_RESTART")
	g_sFMMCEOM.bQuickRestart = TRUE
ENDPROC
//Clear this is a quick restart corona
PROC CLEAR_CORONA_INITIALISING_A_QUICK_RESTART()
	PRINTLN("CLEAR_CORONA_INITIALISING_A_QUICK_RESTART")
	g_sFMMCEOM.bQuickRestart = FALSE
ENDPROC

/// PURPOSE: Returns TRUE if the coroan is a quick restart. NOTE: Only use within corona routines.
///    If you need to check this for a Job please use: IS_THIS_A_QUICK_RESTART_JOB()
FUNC BOOL IS_CORONA_INITIALISING_A_QUICK_RESTART()
	RETURN g_sFMMCEOM.bQuickRestart
ENDFUNC

PROC SET_TIME_OF_DAY_FOR_QUICK_RESTART(INT iTodQuickRestart)
	PRINTLN("SET_TIME_OF_DAY_FOR_QUICK_RESTART - ", iTodQuickRestart)
	g_sFMMCEOM.iTodQuickRestart = iTodQuickRestart 
ENDPROC
PROC CLEAR_TIME_OF_DAY_FOR_QUICK_RESTART()
	#IF IS_DEBUG_BUILD
	IF g_sFMMCEOM.iTodQuickRestart != -1
	PRINTLN("CLEAR_TIME_OF_DAY_FOR_QUICK_RESTART")
	ENDIF
	IF g_sFMMCEOM.iTodQuickRestartHours != -1
	PRINTLN("CLEAR_TIME_OF_DAY_FOR_QUICK_RESTART - Hours")
	ENDIF
	#ENDIF
	g_sFMMCEOM.iTodQuickRestart = -1
	g_sFMMCEOM.iTodQuickRestartHours = -1
	g_sFMMCEOM.iTodQuickRestartMinutes = -1
ENDPROC
FUNC INT GET_TIME_OF_DAY_FOR_QUICK_RESTART()
	RETURN g_sFMMCEOM.iTodQuickRestart 
ENDFUNC

PROC SET_SPECFIC_TIME_OF_DAY_FOR_QUICK_RESTART(INT iTodQuickRestartHours, INT iTodQuickRestartMinutes)
	PRINTLN("SET_SPECFIC_TIME_OF_DAY_FOR_QUICK_RESTART - hours = ", iTodQuickRestartHours, " - minutes = ", iTodQuickRestartMinutes)
	g_sFMMCEOM.iTodQuickRestartHours = iTodQuickRestartHours
	g_sFMMCEOM.iTodQuickRestartMinutes = iTodQuickRestartMinutes
ENDPROC

FUNC INT GET_TIME_OF_DAY_FOR_QUICK_RESTART_FROM_RULE()
	RETURN g_sFMMCEOM.iTodQuickRestartFromRule 
ENDFUNC
PROC SET_TIME_OF_DAY_FOR_QUICK_RESTART_FROM_RULE(INT iTodQuickRestart)
	PRINTLN("SET_TIME_OF_DAY_FOR_QUICK_RESTART_FROM_RULE - ", iTodQuickRestart)
	g_sFMMCEOM.iTodQuickRestartFromRule = iTodQuickRestart 
ENDPROC

//Set this is a quick restart corona
PROC SET_SWAP_TEAMS_ON_QUICK_RESTART()
	PRINTLN("SET_SWAP_TEAMS_ON_QUICK_RESTART")
	g_sFMMCEOM.bQuickRestartSwap = TRUE
ENDPROC
//Clear this is a quick restart corona
PROC CLEAR_SWAP_TEAMS_ON_QUICK_RESTART()
	PRINTLN("CLEAR_SWAP_TEAMS_ON_QUICK_RESTART")
	g_sFMMCEOM.bQuickRestartSwap = FALSE
ENDPROC
//Returns TRUE if this is a quick restart corona
FUNC BOOL SHOULD_SWAP_TEAMS_ON_QUICK_RESTART()
	RETURN g_sFMMCEOM.bQuickRestartSwap
ENDFUNC

//Set my chosen team for a restart
PROC SET_MY_TEAM_FOR_RESTART(INT iTeamChosen)
	PRINTLN("SET_MY_TEAM_FOR_RESTART - iTeamChosen = ", iTeamChosen)
	g_sFMMCEOM.iTeamChosen = iTeamChosen 
ENDPROC
//Clear my chosen team for a restart
PROC CLEAR_MY_TEAM_FOR_RESTART()
	PRINTLN("CLEAR_MY_TEAM_FOR_RESTART")
	g_sFMMCEOM.iTeamChosen = -1
ENDPROC

// 1380652
PROC CLIENT_STORES_LAST_NUM_TEAMS_FOR_RESTART(INT iMaxNumTeams)
	g_sFMMCEOM.iMaxNumTeamsLastMission = iMaxNumTeams
	PRINTLN("CLIENT_STORES_LAST_NUM_TEAMS_FOR_RESTART = ", g_sFMMCEOM.iMaxNumTeamsLastMission)
ENDPROC

PROC CLEAR_LAST_NUM_TEAMS_FOR_RESTART()
	g_sFMMCEOM.iMaxNumTeamsLastMission = -1
	PRINTLN("CLEAR_LAST_NUM_TEAMS_FOR_RESTART = ", g_sFMMCEOM.iMaxNumTeamsLastMission)
ENDPROC

FUNC INT GET_LAST_NUM_TEAMS_FOR_RESTART()
	RETURN g_sFMMCEOM.iMaxNumTeamsLastMission
ENDFUNC

//Get my chosen team for a restart
FUNC INT GET_MY_TEAM_FOR_RESTART()
	RETURN g_sFMMCEOM.iTeamChosen 
ENDFUNC

//Should I use my chosen team for a restart
FUNC BOOL SHOULD_SET_MY_TEAM_FOR_RESTART()
	RETURN (g_sFMMCEOM.iTeamChosen != -1)
ENDFUNC

CONST_INT CORONA_FAILSAFE_TIMEOUT 30000

FUNC BOOL SHOULD_LONE_SPECTATOR_CLEAN_UP(SCRIPT_TIMER& timerFailsafe)
	IF HAS_NET_TIMER_STARTED(timerFailsafe)
		IF HAS_NET_TIMER_EXPIRED(timerFailsafe, CORONA_FAILSAFE_TIMEOUT)
			IF iplayerjoinbitset = 0
				IF DID_I_JOIN_MISSION_AS_SPECTATOR()

					PRINTLN("SHOULD_LONE_SPECTATOR_CLEAN_UP, RETURN TRUE ")

					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAVE_ALL_CORONA_PLAYERS_JOINED(SCRIPT_TIMER& timerFailsafe)

	INT iparticipant
	INT iplayerGBD
	PARTICIPANT_INDEX tempPart
	PLAYER_INDEX tempPlayer
	
	// (933831)
	IF NOT HAS_NET_TIMER_STARTED(timerFailsafe)
		START_NET_TIMER(timerFailsafe)
	ELSE
		IF HAS_NET_TIMER_EXPIRED(timerFailsafe, CORONA_FAILSAFE_TIMEOUT)
			#IF IS_DEBUG_BUILD
//				SCRIPT_ASSERT("net_mission.sch, HAVE_ALL_CORONA_PLAYERS_JOINED, Hit the CORONA_FAILSAFE_TIMEOUT ")
				PRINTLN("[HAVE_ALL_CORONA_PLAYERS_JOINED], net_mission.sch, HAVE_ALL_CORONA_PLAYERS_JOINED, Hit the CORONA_FAILSAFE_TIMEOUT")
			#ENDIF
			
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF iplayerjoinbitset = 0
		iplayerjoinbitset = Get_Bitfield_Of_Players_On_Or_Joining_Mission(Get_UniqueID_For_This_Players_Mission(PLAYER_ID()))
		NET_PRINT("[HAVE_ALL_CORONA_PLAYERS_JOINED], iplayerjoinbitset = ") NET_PRINT_INT(iplayerjoinbitset) NET_NL()
		RETURN FALSE
	ELSE
		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iparticipant
			tempPart = INT_TO_PARTICIPANTINDEX(iparticipant)
			tempPlayer = INT_TO_PLAYERINDEX(iparticipant)
			IF IS_NET_PLAYER_OK(PLAYER_ID(),FALSE,TRUE)
				IF IS_NET_PLAYER_OK(tempPlayer,FALSE,TRUE)
					iplayerGBD = NATIVE_TO_INT(tempPlayer)
					IF IS_BIT_SET(iplayerjoinbitset,iplayerGBD)
						//IF IS_PERSON_LAUNCHING_THE_SAME_MISSION_AS_ME(GlobalplayerBD_FM[iplayerGBD].tl23CurrentMissionOwner, GlobalplayerBD_FM[iplayerGBD].tl31CurrentMissionFileName, iHostGBD)
							IF NOT NETWORK_IS_PARTICIPANT_ACTIVE(tempPart)
								PRINTLN(" [HAVE_ALL_CORONA_PLAYERS_JOINED], WAITING FOR PLAYER TO JOIN: ", iparticipant)
								RETURN FALSE
							ELSE
								PRINTLN(" [HAVE_ALL_CORONA_PLAYERS_JOINED], PARTICIPANT IS ACTIVE: ", iparticipant)
							ENDIF
//						ELSE
//							PRINTLN(" MISSION DATA NOT THE SAME FOR PLAYER: ", iplayerGBD)
//						ENDIF
					ELSE
						PRINTLN(" [HAVE_ALL_CORONA_PLAYERS_JOINED], CORONA BIT IS NOT SET FOR PLAYER: ", iplayerGBD)
					ENDIF
				ELSE
					PRINTLN(" [HAVE_ALL_CORONA_PLAYERS_JOINED], PLAYER NOT OK: ",NATIVE_TO_INT(tempPlayer))
				ENDIF
			ELSE
				PRINTLN(" [HAVE_ALL_CORONA_PLAYERS_JOINED], LOCAL PLAYER NOT OK: ",NATIVE_TO_INT(PLAYER_ID()))
			ENDIF
		ENDREPEAT
	ENDIF
	
	PRINTLN(" [HAVE_ALL_CORONA_PLAYERS_JOINED], HAVE_ALL_CORONA_PLAYERS_JOINED returning true: ") 
	RETURN TRUE
ENDFUNC
/// PURPOSE: Checks if the player is on any mission.
FUNC BOOL IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_INDEX playerId, BOOL bDoPlayListCheck = TRUE)
      IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
            RETURN GET_MP_MISSION_PLAYER_IS_ON(playerId) <> eNULL_MISSION
      ELSE
            RETURN IS_PLAYER_ON_ANY_FM_MISSION(playerId, bDoPlayListCheck)
      ENDIF
ENDFUNC

//PURPOSE: get the number of missions of type running 
FUNC INT GET_NUM_MISSIONS_RUNNING_OF_TYPE(MP_MISSION_TYPE missionType)

	INT iTotal

	INT iPlayer 
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(iPlayer)
		IF IS_PLAYER_ON_ANY_MP_MISSION(playerId)
			IF GET_MP_MISSION_TYPE(GET_MP_MISSION_PLAYER_IS_ON(playerId)) = missionType
				iTotal ++
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN iTotal
ENDFUNC

FUNC BOOL IS_PLAYER_ON_TUTORIAL(PLAYER_INDEX playerID)
	RETURN(IS_MP_MISSION_OF_TYPE(GET_MP_MISSION_PLAYER_IS_ON(playerId), eTYPE_TUTORIAL))
ENDFUNC

FUNC BOOL IS_PLAYER_ON_RANDOM_EVENT(PLAYER_INDEX playerID)
	RETURN(IS_MP_MISSION_OF_TYPE(GET_MP_MISSION_PLAYER_IS_ON(playerId), eTYPE_RANDOM_EVENT))
ENDFUNC

/// PURPOSE: Checks if a player is running a group mission.
FUNC BOOL IS_PLAYER_ON_A_GM( PLAYER_INDEX playerId )
	IF IS_PLAYER_ON_ANY_MP_MISSION(playerId)
		RETURN IS_MP_MISSION_A_GM( GET_MP_MISSION_PLAYER_IS_ON(playerId)  )
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: Gets the mission instance the player is on. If the player is not on any mission, DEFAULT_MISSION_INSTANCE is returned.
FUNC INT GET_MP_MISSION_INSTANCE_PLAYER_IS_ON(PLAYER_INDEX playerId)
	IF IS_NET_PLAYER_OK(playerId, FALSE, TRUE)
		IF NOT IS_PLAYER_ON_ANY_MP_MISSION(playerId)
			RETURN DEFAULT_MISSION_INSTANCE
		ENDIF
		RETURN GlobalplayerBD[NATIVE_TO_INT(playerID)].activeMission.iInstanceId	
	ENDIF
	RETURN DEFAULT_MISSION_INSTANCE
ENDFUNC

/// PURPOSE: Checks if two players are on the same mission instance.
FUNC BOOL ARE_PLAYERS_ON_SAME_MP_MISSION_INSTANCE(PLAYER_INDEX playerId1, PLAYER_INDEX playerId2)
	IF GET_MP_MISSION_PLAYER_IS_ON(playerId1) <> eNULL_MISSION
	AND GET_MP_MISSION_PLAYER_IS_ON(playerId2) <> eNULL_MISSION
		IF GET_MP_MISSION_PLAYER_IS_ON(playerId1) = GET_MP_MISSION_PLAYER_IS_ON(playerId2) 
			RETURN GET_MP_MISSION_INSTANCE_PLAYER_IS_ON(playerId1) = GET_MP_MISSION_INSTANCE_PLAYER_IS_ON(playerId2)
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC



FUNC BOOL IS_PLAYER_IN_SAME_MISSION_STATE_AS_ME(PLAYER_INDEX playerID)
	
	IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
	AND NOT IS_PLAYER_ON_ANY_MP_MISSION(PlayerID)
		RETURN(TRUE)
	ELSE
		IF IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
		AND IS_PLAYER_ON_ANY_MP_MISSION(playerID)
			IF (GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(GET_MP_MISSION_PLAYER_IS_ON(PLAYER_ID())) > 2)
				IF ARE_PLAYERS_ON_SAME_MP_MISSION_INSTANCE(PLAYER_ID(), playerID)
					RETURN(TRUE)
				ENDIF
			ELSE
				RETURN(TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN(FALSE)
	
ENDFUNC


/// PURPOSE: Checks if the current type is a mission
/// RETURNS: true if it is.
FUNC BOOL IS_THIS_A_MISSION(BOOL bIncludePublicCreators = TRUE)
	
	IF NOT bIncludePublicCreators
		RETURN g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
	ENDIF
	
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION 
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION_CTF
	//OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RAMPAGE
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_GANGHIDEOUT
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_SURVIVAL
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_CRATE_DROP
	OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CTF
	OR g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Checks if the current type is a race
/// RETURNS: true if it is.
FUNC BOOL IS_THIS_A_RACE()
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_RACE
	OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_BASE_JUMP
	//OR g_FMMC_STRUCT.iMissionType = FMMC_TYPE_TRIATHLON
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Checks if the current type is a Gang Attack
/// RETURNS: true if it is.
FUNC BOOL IS_THIS_A_GANG_ATTACK()
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_GANGHIDEOUT
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_A_LTS()
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION 
	AND g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_LTS
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_A_CAPTURE()
	RETURN g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION 
	AND g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_CTF
ENDFUNC

FUNC BOOL IS_3D_EXPLODER_CREATOR()
	RETURN g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION 
	AND g_FMMC_STRUCT.iMissionSubType = FMMC_MISSION_TYPE_EXPLODER
	AND IS_ROCKSTAR_DEV()
ENDFUNC

FUNC BOOL IS_THIS_A_DEATHMATCH()
	RETURN g_FMMC_STRUCT.iMissionType = FMMC_TYPE_DEATHMATCH
ENDFUNC

FUNC BOOL IS_THIS_A_SURVIVAL()
	RETURN g_FMMC_STRUCT.iMissionType = FMMC_TYPE_SURVIVAL
ENDFUNC

FUNC BOOL IS_BASIC_CREATOR()
	RETURN GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Basic_Creator")) > 0
ENDFUNC

FUNC BOOL IS_FREEMODE_CREATOR()
	RETURN GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Freemode_Creator")) > 0
ENDFUNC

/// PURPOSE: Checks if any player is running a specific mission instance.
///    True if any player is running "mission" with instance "iInstanceId".
FUNC BOOL IS_ANY_OTHER_PLAYER_RUNNING_MP_MISSION_INSTANCE(MP_MISSION mission, INT iInstanceId)

	IF mission = eNULL_MISSION 
		RETURN FALSE
	ENDIF

	INT iPlayer 
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(iPlayer)
		IF NOT (playerID = PLAYER_ID())
			IF GET_MP_MISSION_PLAYER_IS_ON(playerId) = mission
				IF GET_MP_MISSION_INSTANCE_PLAYER_IS_ON(playerId) = iInstanceId
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

/// PURPOSE: Checks if a player is running a specific mission instance.
FUNC BOOL IS_PLAYER_RUNNING_MP_MISSION_INSTANCE(PLAYER_INDEX playerId, MP_MISSION mission, INT iInstanceId)

	IF mission = eNULL_MISSION 
		RETURN FALSE
	ENDIF
	
	IF playerId = INVALID_PLAYER_INDEX()
		PRINTLN("[JS] IS_PLAYER_RUNNING_MP_MISSION_INSTANCE - invalid player ID passed in")
		RETURN FALSE
	ENDIF
	
	IF GET_MP_MISSION_PLAYER_IS_ON(playerId) = mission
		IF GET_MP_MISSION_INSTANCE_PLAYER_IS_ON(playerId) = iInstanceId
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE: Gets the number of players running a specfic mission.
FUNC INT GET_NUM_PLAYERS_RUNNING_MP_MISSION(MP_MISSION mission)

	IF mission = eNULL_MISSION 
		RETURN 0
	ENDIF
	
	INT iNumPlayersOnMission
	INT iPlayer 
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(iPlayer)
		IF GET_MP_MISSION_PLAYER_IS_ON(playerId) = mission
			iNumPlayersOnMission ++
		ENDIF
	ENDREPEAT

	RETURN iNumPlayersOnMission
ENDFUNC

//PURPOSE: Check if a player is running a mission of type
FUNC BOOL IS_PLAYER_RUNNING_MP_MISSION_OF_TYPE(PLAYER_INDEX PlayerId, MP_MISSION_TYPE missionType)
	IF IS_PLAYER_ON_ANY_MP_MISSION(playerId)
		IF GET_MP_MISSION_TYPE(GET_MP_MISSION_PLAYER_IS_ON(playerId)) = missionType
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE	
ENDFUNC


////////////////// THIS IS COMPLETELY WRONG ITS RETURNING NUM OF PLAYERS NOT DOING A TUTORIAL ///////////
/// PURPOSE: checks if anyone is running a mission (not including tutorials).  
FUNC BOOL IS_ANYONE_ON_A_MISSION(BOOL bIncludeTutorials = FALSE)
	INT iPlayer 
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(iPlayer)
		IF bIncludeTutorials
			IF IS_PLAYER_ON_ANY_MP_MISSION(playerId)
				RETURN TRUE
			ENDIF
		ELSE
			IF NOT IS_PLAYER_RUNNING_MP_MISSION_OF_TYPE(playerId, eTYPE_TUTORIAL)
				IF IS_PLAYER_ON_ANY_MP_MISSION(playerId)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN FALSE
ENDFUNC

/// PURPOSE: Gets the number of players running a specfic mission instance.
FUNC INT GET_NUM_PLAYERS_RUNNING_MP_MISSION_INSTANCE(MP_MISSION mission, INT iInstanceId)

	IF mission = eNULL_MISSION 
		RETURN 0
	ENDIF
	
	INT iNumPlayersOnMission
	INT iPlayer 
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(iPlayer)
		IF GET_MP_MISSION_PLAYER_IS_ON(playerId) = mission
			IF GET_MP_MISSION_INSTANCE_PLAYER_IS_ON(playerId) = iInstanceId
				iNumPlayersOnMission ++
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN iNumPlayersOnMission
ENDFUNC

/// PURPOSE: Gets the number of players running a specfic mission instance.
FUNC INT GET_NUM_PLAYERS_IN_TEAM_RUNNING_MP_MISSION_INSTANCE(INT Team, MP_MISSION mission, INT iInstanceId)

	IF mission = eNULL_MISSION 
		RETURN 0
	ENDIF
	
	INT iNumPlayersOnMission
	INT iPlayer 
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(iPlayer)
		IF GET_MP_MISSION_PLAYER_IS_ON(playerId) = mission
			IF GET_MP_MISSION_INSTANCE_PLAYER_IS_ON(playerId) = iInstanceId
				IF GET_PLAYER_TEAM(playerId) = Team 
					iNumPlayersOnMission ++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN iNumPlayersOnMission
ENDFUNC

///// PURPOSE: Gets the number of players running a specfic mission instance.
//FUNC INT GET_NUM_CRIMINALS_RUNNING_MP_MISSION_INSTANCE(MP_MISSION mission, INT iInstanceId)
//
//	IF mission = eNULL_MISSION 
//		RETURN 0
//	ENDIF
//	
//	INT iNumPlayersOnMission
//	INT iPlayer 
//	REPEAT NUM_NETWORK_PLAYERS iPlayer
//		PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(iPlayer)
//		IF GET_MP_MISSION_PLAYER_IS_ON(playerId) = mission
//			IF GET_MP_MISSION_INSTANCE_PLAYER_IS_ON(playerId) = iInstanceId
//				IF GET_PLAYER_TEAM(playerId) > TEAM_COP
//					IF GET_PLAYER_TEAM(playerId) < MAX_NUM_TEAMS
//						iNumPlayersOnMission ++
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDREPEAT
//
//	RETURN iNumPlayersOnMission
//ENDFUNC

/// PURPOSE:checks to see if the Local player was the host of the last corona
/// RETURNS:true if the local player was
FUNC BOOL WAS_I_HOST_OF_LAST_CORONA()
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	RETURN (GlobalplayerBD_FM[iMyGBD].sClientCoronaData.iLastCoronaLeader = iMyGBD)
ENDFUNC
//Get a players last corona host
FUNC INT GET_PLAYERS_HOST_OF_LAST_CORONA(PLAYER_INDEX piPlayer)
	RETURN (GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].sClientCoronaData.iLastCoronaLeader)
ENDFUNC
//Set the last corona host
PROC SET_GBD_HOST_OF_LAST_CORONA(INT iLastCoronaLeader)
	PRINTLN("SET_GBD_HOST_OF_LAST_CORONA- GlobalplayerBD_FM[iMyGBD].sClientCoronaData.iLastCoronaLeader = ", iLastCoronaLeader)
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iLastCoronaLeader = iLastCoronaLeader
ENDPROC
//Clear the last corona host
PROC CLEAR_GBD_HOST_OF_LAST_CORONA()
	PRINTLN("CLEAR_GBD_HOST_OF_LAST_CORONA")
	GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iLastCoronaLeader = -1
ENDPROC

/// PURPOSE:checks to see if the Local player is on ANY FM misssion
///    
/// PARAMS:
///    playerID - the player
/// RETURNS:true if the local player is on ANY Freemode mission
///    
FUNC BOOL IS_LOCAL_PLAYER_ON_ANY_FM_MISSION()
	//Not is Freemode
	IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
		 RETURN FALSE       
	ENDIF
	//Not in GBD Array
	IF NOT IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
		RETURN FALSE       
	ENDIF
	//Not on a mission
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_INVALID
		RETURN FALSE       
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:checks to see if a player is on ANY FM misssion of a given type
///    playerID 	- the player
///    iMissionType - the type of mission please pick from:
///    					- FMMC_TYPE_MISSION        		
///    					- FMMC_TYPE_MISSION_CTF        		
///    					- FMMC_TYPE_RAMPAGE        		
///    					- FMMC_TYPE_GANGHIDEOUT        		
///    					- FMMC_TYPE_DEATHMATCH          
///    					- FMMC_TYPE_RACE               	
///    					- FMMC_TYPE_SURVIVAL              	
///    					- FMMC_TYPE_MG					
///    					- FMMC_TYPE_MG_GOLF				 
///    					- FMMC_TYPE_MG_TENNIS			
///    					- FMMC_TYPE_MG_SHOOTING_RANGE	
///    					- FMMC_TYPE_MG_DARTS			
///    					- FMMC_TYPE_MG_ARM_WRESTLING	
/// RETURNS:true if a player is on ANY FM misssion of a given type
FUNC BOOL IS_PLAYER_ON_ANY_FM_MISSION_OF_TYPE(PLAYER_INDEX piPassed, INT iMissionType)
	//Not is Freemode
	IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
		 RETURN FALSE       
	ENDIF
	//Not in GBD Array
	IF NOT IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(piPassed)
		RETURN FALSE       
	ENDIF
	
	//Get their GBD slot
	INT iGbdSlot = NATIVE_TO_INT(piPassed)
	
	//On a mission of given type
	//IF  GlobalplayerBD_FM[iGbdSlot].currentMissionData.mdID.idCreator = -1
	IF GlobalplayerBD_FM[iGbdSlot].iCurrentMissionType 			= iMissionType
		RETURN TRUE       
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_INDEX piPassed)
	RETURN GlobalplayerBD_FM[NATIVE_TO_INT(piPassed)].iCurrentMissionType
ENDFUNC

FUNC BOOL SHOULD_POLICE_BE_DISABLED_FM(PLAYER_INDEX piPassed)
	INT iPlayerGBD = NATIVE_TO_INT(piPassed)
	IF GlobalplayerBD_FM[iPlayerGBD].iCurrentMissionType = FMMC_TYPE_MISSION
		IF	g_FMMC_STRUCT.iPolice = DM_POLICE_OFF
			PRINTLN("SHOULD_POLICE_BE_DISABLED_FM - Returning TRUE (1)")
			RETURN TRUE
		ENDIF
	ELIF GlobalplayerBD_FM[iPlayerGBD].iCurrentMissionType = FMMC_TYPE_DEATHMATCH
//		IF	GlobalServerBD_DM.iPolice = DM_POLICE_OFF
			PRINTLN("SHOULD_POLICE_BE_DISABLED_FM - Returning TRUE (2)")
			RETURN TRUE
//		ENDIF
	ELIF GlobalplayerBD_FM[iPlayerGBD].iCurrentMissionType = FMMC_TYPE_RACE
	//OR GlobalplayerBD_FM[iPlayerGBD].iCurrentMissionType = FMMC_TYPE_TRIATHLON
	OR GlobalplayerBD_FM[iPlayerGBD].iCurrentMissionType = FMMC_TYPE_BASE_JUMP
		IF GlobalServerBD_Races.iPolice = DM_POLICE_OFF
			PRINTLN("SHOULD_POLICE_BE_DISABLED_FM - Returning TRUE (3)")
			RETURN TRUE
		ENDIF
	ENDIF
	
	PRINTLN("SHOULD_POLICE_BE_DISABLED_FM - Returning FALSE!")
	RETURN FALSE
ENDFUNC

FUNC  BOOL IS_AREA_TRIGGERED_MISSION(INT iMissionType)
	IF (iMissionType = FMMC_TYPE_MISSION_CTF)
	OR (iMissionType = FMMC_TYPE_GANGHIDEOUT)
	//OR (iMissionType >= FMMC_TYPE_MG) // << --- As described in bug 832909, this line is to be removed now that all minigames have been set up to terminate appropriately
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC  BOOL IS_AREA_TRIGGERED_MISSION_LAUNCHER(INT iMissionType) 
	IF (iMissionType = FMMC_TYPE_MISSION_CTF)
	OR (iMissionType = FMMC_TYPE_GANGHIDEOUT)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_ON_IMPROMPTU_DM()
	RETURN (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_IMPROMPTU_DM)
ENDFUNC


FUNC BOOL IS_PLAYER_ON_BOSSVBOSS_DM()
	RETURN (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_GB_BOSS_DEATHMATCH)
//	#IF FEATURE_BIKER OR IS_PLAYER_ON_BIKER_DM() #ENDIF)
	
ENDFUNC

FUNC BOOL IS_PLAYER_ON_BIKER_DM()
	RETURN IS_PLAYER_ON_BOSSVBOSS_DM()
			AND GB_IS_PLAYER_BOSS_OF_A_BIKER_GANG(GB_GET_LOCAL_PLAYER_MISSION_HOST())
ENDFUNC

FUNC BOOL IS_PLAYER_ON_BOSSVBOSS_ASSAULT()
	RETURN (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType = FMMC_TYPE_GB_ASSAULT)
ENDFUNC


 // FEATURE_GANG_BOSS




/// PURPOSE:checks to see if a player is on the FM misssion that is passed to it
///    
/// PARAMS:
///    playerID - the player
///    iMissionPassed - the mission
///    iVarationPassed - the varation of that mission
/// RETURNS:true is that player is on that mission
///    
FUNC BOOL IS_PLAYER_ON_FM_MISSION(PLAYER_INDEX playerID, INT iMissionPassed, INT iVarationPassed, INT iFMCreatorID)
	IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].currentMissionData.mdID.idCreator	= iMissionPassed
		IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].currentMissionData.mdID.idVariation = iVarationPassed
			IF GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].currentMissionData.mdID.idCreator = iFMCreatorID
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


PROC SET_FM_MISSION_AS_NOT_JOINABLE()
	PRINTLN("Setting SET_FM_MISSION_AS_NOT_JOINABLE")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciMissionIsNotJoinable)
ENDPROC
PROC SET_FM_MISSION_LAUNCHED_SUCESS(INT iCreatorID, INT iVarationPassed = 0, INT iInstancePassed = -10)
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idCreator 		= iCreatorID
	GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idVariation 		= iVarationPassed
	GlobalplayerBD_FM[iMyGBD].currentMissionData.iInstanceId			= iInstancePassed
	PRINTLN("GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idCreator   = ", GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idCreator)
	PRINTLN("GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idVariation = ", GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idVariation)
	PRINTLN("GlobalplayerBD_FM[iMyGBD].currentMissionData.iInstanceId      = ", GlobalplayerBD_FM[iMyGBD].currentMissionData.iInstanceId)
	GlobalplayerBD_FM[iMyGBD].iInstanceToUse							= DEFAULT_MISSION_INSTANCE
	CLEAR_BIT(GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionLaunchInProgress)
	CLEAR_BIT(GlobalplayerBD_FM[iMyGBD].currentMissionData.iBitSet, ciMissionIsNotJoinable)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND NOT IS_PLAYER_SCTV(PLAYER_ID())
	AND NOT IS_PLAYER_SPECTATING(PLAYER_ID())
	AND NOT DID_I_JOIN_MISSION_AS_SPECTATOR()
		PRINTLN("SET_FM_MISSION_LAUNCHED_SUCESS - SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)")
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
	ENDIF
	PRINTLN("SET_FM_MISSION_LAUNCHED_SUCESS called")
ENDPROC

PROC SET_FM_MISSION_AS_JOINABLE()
	PRINTLN("Setting SET_FM_MISSION_AS_JOINABLE")
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciMissionIsNotJoinable)
ENDPROC

PROC SET_NEXT_FM_MISSION_LAUNCH_SUCESS(INT iCreatorID, INT iVarationPassed, VECTOR vMissionStartPos)
	PRINTLN("SET_NEXT_FM_MISSION_LAUNCH_SUCESS called")
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idCreator 		= iCreatorID
	GlobalplayerBD_FM[iMyGBD].currentMissionData.mdID.idVariation 		= iVarationPassed
	GlobalplayerBD_FM[iMyGBD].currentMissionData.mdPrimaryCoords	 	= vMissionStartPos
ENDPROC

PROC CLEAN_UP_FMMC_AT_BLIP_VARS_AFTER_WARP()
	#IF IS_DEBUG_BUILD
		PRINTLN("CLEAN_UP_FMMC_AT_BLIP_VARS_AFTER_WARP()")
		DEBUG_PRINTCALLSTACK()
	#ENDIF
	INT iMyGBD = NATIVE_TO_INT(PLAYER_ID())
	CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biIwantToVote) 
	CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biStartToLoadData) 
	CLEAR_BIT(GlobalplayerBD_FM_2[iMyGBD].iBitSet, biVoteHasPassed)  
	
ENDPROC

/// PURPOSE: Checks if we are already in the process of launching a mission.  
FUNC BOOL IS_FM_MISSION_LAUNCH_IN_PROGRESS()
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionLaunchInProgress)
ENDFUNC



PROC SET_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA()
	PRINTLN("[TS] SET_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA")
	SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_ForceNewCoronaInstance)
ENDPROC
PROC CLEAR_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA()
	PRINTLN("[TS] CLEAR_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA")
	CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_ForceNewCoronaInstance)
ENDPROC
FUNC BOOL IS_PLAYER_FORCING_OWN_INSTANCE_OF_CORONA(PLAYER_INDEX piPassed)
	RETURN IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piPassed)].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_ForceNewCoronaInstance)
ENDFUNC

//FUNC VECTOR GET_PLAYER_FMMC_START_VECTOR(PLAYER_INDEX piPassed, INT iVariationPassed = 0)
//	iVariationPassed = iVariationPassed
//	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(piPassed)
//		RETURN GlobalplayerBD_FM[NATIVE_TO_INT(piPassed)].loadedMissionData.vStartLocation
//	ELSE
//		RETURN <<0.0, 0.0, 0.0>>
//	ENDIF
//ENDFUNC
//FUNC INT GET_PLAYER_FMMC_MAX_START_NUMBER(PLAYER_INDEX piPassed, INT iVariationPassed = 0)
//	iVariationPassed = iVariationPassed
//	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(piPassed)
//		RETURN GlobalplayerBD_FM[NATIVE_TO_INT(piPassed)].loadedMissionData.iMaxStartNumber
//	ELSE
//		RETURN 16
//	ENDIF
//ENDFUNC
//FUNC INT GET_PLAYER_FMMC_MIN_START_NUMBER(PLAYER_INDEX piPassed, INT iVariationPassed = 0)
//	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(piPassed)
//		iVariationPassed = iVariationPassed
//		RETURN GlobalplayerBD_FM[NATIVE_TO_INT(piPassed)].loadedMissionData.iMinStartNumber
//	ELSE
//		RETURN 2
//	ENDIF
//ENDFUNC
//FUNC TEXT_LABEL_31 GET_PLAYER_FMMC_MISSION_TO_LOAD(PLAYER_INDEX piPassed)
//	//iVariationPassed = iVariationPassed
//	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(piPassed)
//		RETURN GlobalplayerBD_FM[NATIVE_TO_INT(piPassed)].loadedMissionData.tl31LoadedMission
//	ELSE
//		TEXT_LABEL_31 tl31
//		RETURN tl31
//	ENDIF
//ENDFUNC
//FUNC TEXT_LABEL_31 GET_PLAYER_FMMC_MISSION_TO_LOAD_PRETTY_NAME(PLAYER_INDEX piPassed)
//	//iVariationPassed = iVariationPassed
//	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(piPassed)
//		RETURN GlobalplayerBD_FM[NATIVE_TO_INT(piPassed)].loadedMissionData.tl31LoadedMissionName
//	ELSE
//		TEXT_LABEL_31 tl31 = ""
//		RETURN tl31
//	ENDIF
//ENDFUNC
//FUNC TEXT_LABEL_23 GET_PLAYER_FMMC_MISSION_TO_LOAD_OWNER(PLAYER_INDEX piPassed)
//	//iVariationPassed = iVariationPassed
//	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(piPassed)
//		RETURN GlobalplayerBD_FM[NATIVE_TO_INT(piPassed)].loadedMissionData.tl31LoadedMissionCreator
//	ELSE
//		TEXT_LABEL_23 tl23 = ""
//		RETURN tl23
//	ENDIF
//ENDFUNC
//
//
//
//FUNC INT GET_PLAYER_FMMC_MISSION_TYPE(PLAYER_INDEX piPassed, INT iVariationPassed = 0)
//	iVariationPassed = iVariationPassed
//	IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(piPassed)
//		RETURN GlobalplayerBD_FM[NATIVE_TO_INT(piPassed)].loadedMissionData.iMissionType
//	ELSE
//		PRINTLN("GET_PLAYER_FMMC_MISSION_TYPE PLAYER IS NOT IN THE GBD ARRAY") 
//		RETURN -1
//	ENDIF
//ENDFUNC

FUNC BOOL IS_FM_MISSION_JOINABLE(INT &iInstance, BOOL &bMissionIsRunning, INT iFMCreatorID) //, INT iMissionType = -1, INT missionToStart = -1, INT missionVariation = -1 ) //, INT iMaxParticipants, INT iFMCreatorID #IF IS_DEBUG_BUILD, BOOL bDoPrint = FALSE #ENDIF)// #IF IS_DEBUG_BUILD, BOOL bUseLaxRules = FALSE #ENDIF)
	//PRINTLN("IS_FM_MISSION_JOINABLE")
	IF iFMCreatorID = FMMC_MINI_GAME_CREATOR_ID
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_SKIP_JOINABLE_CHECKS)
			IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iInstanceToUse != DEFAULT_MISSION_INSTANCE
				iInstance = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iInstanceToUse
				bMissionIsRunning = TRUE
			ENDIF
		ENDIF
		RETURN TRUE	
	ENDIF
	//PRINTLN("IS_FM_MISSION_JOINABLE - bIsMiniGame FALSE")
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_SKIP_JOINABLE_CHECKS)
	AND GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iInstanceToUse != -1
		//PRINTLN("ciFMMC_MISSION_DATA_BitSet_SKIP_JOINABLE_CHECKS")
		iInstance = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iInstanceToUse
		bMissionIsRunning = TRUE
		RETURN TRUE
	ENDIF
	
//	IF bSkipInprogressCheck
//		RETURN TRUE
//	ENDIF
//	IF IS_AREA_TRIGGERED_MISSION_LAUNCHER(iMissionType)
//		INT 			iParticipant
//		REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
//			IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
//				PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
//				IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(piPlayer)
//					IF IS_PLAYER_ON_FM_MISSION(piPlayer, missionToStart, missionVariation, iFMCreatorID)
//						IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].currentMissionData.iBitSet, ciMissionIsNotJoinable)
//							iInstance = GlobalplayerBD_FM[NATIVE_TO_INT(piPlayer)].currentMissionData.iInstanceId
//							//If there is atleast one person on the mission the set the bool to say that there is.
//							IF NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_ENTERED_CORONA_LATE)
//								SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_ENTERED_CORONA_LATE)
//								PRINTLN("SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_ENTERED_CORONA_LATE)")
//							ENDIF
//							bMissionIsRunning = TRUE
//						ELSE
//							RETURN FALSE
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDREPEAT
//	ENDIF
	
	RETURN TRUE
//	IF iFMCreatorID = FMMC_ROCKSTAR_CREATOR_ID
//	OR iFMCreatorID = FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
//	OR iFMCreatorID = FMMC_ROCKSTAR_HIDEOUT_CREATOR_ID
//	
//		IF iPlayerCount < iMaxParticipants
//		OR iMaxParticipants = 0
//			RETURN TRUE
//		ELSE
//			#IF IS_DEBUG_BUILD
//			IF bDoPrint
//				PRINTLN("iPlayerCount > iMaxParticipants")
//				PRINTLN("[", iPlayerCount, "] > [", iMaxParticipants, "]")
//			ENDIF
//			#ENDIF	
//			RETURN FALSE
//		ENDIF
//	ELSE
//		PLAYER_INDEX 	piCreator		= INT_TO_NATIVE(PLAYER_INDEX, missionToStart)
//		IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(piCreator)
//			//iMaxParticipants = GET_PLAYER_FMMC_MAX_START_NUMBER(piCreator, missionVariation)
//			IF iPlayerCount < iMaxParticipants
//			OR iMaxParticipants = 0
//				RETURN TRUE
//			ELSE
//				#IF IS_DEBUG_BUILD
//				IF bDoPrint
//					PRINTLN("iPlayerCount > iMaxParticipants 2")
//					PRINTLN("[", iPlayerCount, "] > [", iMaxParticipants, "]")
//				ENDIF
//				#ENDIF	
//				RETURN FALSE
//			ENDIF
//		ELSE
//			#IF IS_DEBUG_BUILD
//			IF bDoPrint
//				PRINTLN("IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(", NATIVE_TO_INT(piCreator), ") = FALSE")
//			ENDIF
//			#ENDIF	
//			RETURN FALSE
//		ENDIF
//	ENDIF
	
//	RETURN FALSE
	
ENDFUNC

FUNC BOOL IS_ANY_PLAYER_ON_THIS_FM_MISSION(INT missionToStart, INT missionVariation = 0, INT iFMCreatorID = 0)
	INT 			iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PLAYER_INDEX piPlayer = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			IF IS_PLAYER_ON_FM_MISSION(piPlayer, missionToStart, missionVariation, iFMCreatorID)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC
//Returns true if this mission is a heist.
//Takes the sub type
FUNC  BOOL IS_THIS_MISSION_A_HEIST(INT iMissionType, BOOL bHeistMissionFlag)
	//Not a mission then no!
	IF iMissionType != FMMC_TYPE_MISSION
		RETURN FALSE
	ENDIF
	//It's a heist
	RETURN bHeistMissionFlag
ENDFUNC

//Returns true if this mission is a pre planning heist.
//Takes the sub type
FUNC  BOOL IS_THIS_MISSION_A_PRE_PLANNING_HEIST(INT iMissionType, BOOL bPrePlanningHeistFlag)
	//Not a mission then no!
	IF iMissionType != FMMC_TYPE_MISSION
		RETURN FALSE
	ENDIF
	//It's a heist
	RETURN bPrePlanningHeistFlag
ENDFUNC

/// PURPOSE: Returns TRUE if the mission corona is a tutorial cutscene into first strand mission.
FUNC BOOL IS_THIS_MISSION_A_TUTORIAL_CUTSCENE(INT iMissionType, BOOL bHeistPrePlanningTutorialCutscene)

	IF iMissionType != FMMC_TYPE_MISSION
		RETURN FALSE
	ENDIF
	
	RETURN bHeistPrePlanningTutorialCutscene
ENDFUNC

/// PURPOSE: Returns TRUE if the mission corona is a pre planning cutscene
FUNC BOOL IS_THIS_MISSION_A_PRE_PLANNING_CUTSCENE(INT iMissionType, BOOL bHeistPrePlanningCutscene)

	IF iMissionType != FMMC_TYPE_MISSION
		RETURN FALSE
	ENDIF
	
	RETURN bHeistPrePlanningCutscene
ENDFUNC

/// PURPOSE: Returns TRUE if the mission corona is a pre planning cutscene
FUNC BOOL IS_THIS_MISSION_A_PRE_PLANNING_INTRO_CUTSCENE(INT iMissionType, BOOL bHeistPrePlanningCutscene, BOOL bHeistMidStrandCutscene)

	IF iMissionType != FMMC_TYPE_MISSION
		RETURN FALSE
	ENDIF
	
	RETURN (bHeistPrePlanningCutscene AND NOT bHeistMidStrandCutscene)
ENDFUNC

/// PURPOSE: Returns TRUE if the mission corona is a pre planning cutscene
FUNC BOOL IS_THIS_MISSION_A_PRE_PLANNING_MID_STRAND_CUTSCENE(INT iMissionType, BOOL bHeistPrePlanningCutscene, BOOL bHeistMidStrandCutscene)

	IF iMissionType != FMMC_TYPE_MISSION
		RETURN FALSE
	ENDIF
	
	RETURN (bHeistMidStrandCutscene AND bHeistPrePlanningCutscene)
ENDFUNC

/// PURPOSE: Returns TRUE if the mission corona is a contact mission
FUNC BOOL IS_THIS_MISSION_A_CONTACT_MISSION(INT iMissionType, Bool bContactMission)
		
	IF iMissionType != FMMC_TYPE_MISSION
		RETURN FALSE
	ENDIF
	
	RETURN bContactMission
	
ENDFUNC


/// PURPOSE: Returns TRUE if this mission is any of the heist types (Main, planning, cutscene)
FUNC BOOL IS_THIS_MISSION_OF_TYPE_HEIST(INT iMissionType, BOOL bHeistFlag
										, BOOL bHeistPlanningFlag, BOOL bHeistPrePlanningCutscene  )
	RETURN IS_THIS_MISSION_A_HEIST(iMissionType, bHeistFlag)
		   OR IS_THIS_MISSION_A_PRE_PLANNING_HEIST(iMissionType, bHeistPlanningFlag)
		   OR IS_THIS_MISSION_A_PRE_PLANNING_CUTSCENE(iMissionType, bHeistPrePlanningCutscene)
ENDFUNC

////////////////////////////////////////////////////
///    
///  Leave with every body flags
///   
//clear the flag
PROC CLEAR_LEAVE_MISSION_WITH_EVERYBODY()
	#IF IS_DEBUG_BUILD
	PRINTLN("[TS] CLEAR_LEAVE_MISSION_WITH_EVERYBODY")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.sTranToGameVars.bWithPlayers = FALSE
ENDPROC
//Set that flag
PROC SET_LEAVE_MISSION_WITH_EVERYBODY()
	#IF IS_DEBUG_BUILD
	PRINTLN("[TS] SET_LEAVE_MISSION_WITH_EVERYBODY")	
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.sTranToGameVars.bWithPlayers = TRUE	
ENDPROC

////////////////////////////////////////////////////
///    
///  SET WHEN I LEFT THE MISSION
///   
//Left at the time out
PROC SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
	#IF IS_DEBUG_BUILD
	IF NOT g_TransitionSessionNonResetVars.sPostMissionCleanupData.sTranToGameVars.bLeftMissionVeryEnd
		PRINTLN("[TS] SET_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT")	
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.sTranToGameVars.bLeftMissionVeryEnd = TRUE	
ENDPROC
//Left early
PROC CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
	#IF IS_DEBUG_BUILD
	PRINTLN("[TS] CLEAR_ENDED_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT")	
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.sTranToGameVars.bLeftMissionVeryEnd = FALSE	
ENDPROC
//Did I leave at the end
FUNC BOOL DID_I_LEAVE_MISSION_AT_LEADERBOARD_VOTE_OR_TIME_OUT()
	RETURN g_TransitionSessionNonResetVars.sPostMissionCleanupData.sTranToGameVars.bLeftMissionVeryEnd
ENDFUNC

////////////////////////////////////////////////////
///    
///  Set the transition to game to run
///   
//Set it to run   
PROC SET_DO_TRANSITION_TO_GAME()
	#IF IS_DEBUG_BUILD
	PRINTLN("[TS] SET_DO_TRANSITION_TO_GAME")	
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRunDoTransitionToGame = TRUE	
ENDPROC
//Clear it to run   
PROC CLEAR_DO_TRANSITION_TO_GAME()
	#IF IS_DEBUG_BUILD
	PRINTLN("[TS] CLEAR_DO_TRANSITION_TO_GAME")	
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRunDoTransitionToGame = FALSE	
ENDPROC
//Should it to run   
FUNC BOOL SHOULD_DO_TRANSITION_TO_GAME_RUN()
	#IF IS_DEBUG_BUILD
	IF g_TransitionSessionNonResetVars.sPostMissionCleanupData.bDoTransitionToGameEnabled = FALSE
		RETURN FALSE
	ENDIF
	#ENDIF
	RETURN (g_TransitionSessionNonResetVars.sPostMissionCleanupData.bRunDoTransitionToGame)
ENDFUNC

////////////////////////////////////////////////////
///    
/// Should freemode wait for the transition camera
///   
//Set it to run 
PROC SET_SKIP_WAIT_FOR_TRANSITION_CAMERA()
	#IF IS_DEBUG_BUILD
	PRINTLN("[TS] SET_SKIP_WAIT_FOR_TRANSITION_CAMERA")	
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSkipWaitForTransitionCamera = TRUE	
ENDPROC
//Clear it should run 
PROC CLEAR_SKIP_WAIT_FOR_TRANSITION_CAMERA()
	#IF IS_DEBUG_BUILD
	PRINTLN("[TS] CLEAR_SKIP_WAIT_FOR_TRANSITION_CAMERA")	
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSkipWaitForTransitionCamera = FALSE	
ENDPROC
//Should it should run 
FUNC BOOL SHOULD_SKIP_WAIT_FOR_TRANSITION_CAMERA()
	RETURN (g_TransitionSessionNonResetVars.sPostMissionCleanupData.bSkipWaitForTransitionCamera)
ENDFUNC






FUNC BOOL NET_LOAD_AND_LAUNCH_SCRIPT_FM(STRING sScriptNamePassed, INT iStackSizePassed, MP_MISSION_DATA fmmcMissionData, BOOL bDoScreenFadeOutPassed = FALSE, BOOL bIsMission = TRUE, BOOL bClearWantedOnLaunch = FALSE, BOOL bIsAmbiant = FALSE, BOOL bHideHud = TRUE)
		
	IF DOES_SCRIPT_EXIST(sScriptNamePassed)
		
		IF bIsMission
		AND fmmcMissionData.iInstanceId != -1
//			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_Controlled_TurnedOff_For_Launch)	// KGM 18/12/12: looks to be obsolete, checked with Bobby
			PRINTLN("SETTING - ciFMMC_MISSION_DATA_BitSet_SKIP_JOINABLE_CHECKS")
			SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_SKIP_JOINABLE_CHECKS)
			GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iInstanceToUse = fmmcMissionData.iInstanceId
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
			IF bHideHud
				HIDE_HUD_AND_RADAR_THIS_FRAME()
			ENDIF
		ENDIF
		IF bDoScreenFadeOutPassed
			SET_FRONTEND_ACTIVE(FALSE)
			IF  NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(1000)
			ENDIF
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				PRINTLN("NET_LOAD_AND_LAUNCH_SCRIPT_FM  - SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)")
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			ENDIF
		ENDIF
		
		NET_PRINT_TIME() NET_PRINT_STRINGS("NET_LOAD_AND_LAUNCH_SCRIPT_FM: Launching - ", sScriptNamePassed)  NET_NL()
		
		REQUEST_SCRIPT(sScriptNamePassed)
			
		IF HAS_SCRIPT_LOADED(sScriptNamePassed)			
			
			IF bDoScreenFadeOutPassed
				IF NOT IS_SCREEN_FADED_OUT()
					NET_PRINT_TIME() NET_PRINT_STRINGS("NET_LOAD_AND_LAUNCH_SCRIPT_FM:Waiting for IS_SCREEN_FADED_OUT()", sScriptNamePassed)  NET_NL()
					RETURN FALSE
				ENDIF
			ENDIF
			
			IF iStackSizePassed <= 0
				NET_PRINT_TIME() NET_PRINT("NET_LOAD_AND_LAUNCH_SCRIPT_FM iStackSizePassed <= 0 !!!!", ePRINT_NET_ERROR)
				iStackSizePassed = MINI_STACK_SIZE
			ENDIF
			
			// KGM 3/12/12: I can't put a check in here to see if code may still be cleaning up a script (see function below) because
			//				there could genuinely be multiple versions of some scripts required, so the check needs to go around
			//				individual script calls to this function where a problem has arisen.
			#IF IS_DEBUG_BUILD
			THREADID thread = #ENDIF START_NEW_SCRIPT_WITH_ARGS(sScriptNamePassed, fmmcMissionData, SIZE_OF(fmmcMissionData), iStackSizePassed)
			SET_SCRIPT_AS_NO_LONGER_NEEDED(sScriptNamePassed)
			#IF IS_DEBUG_BUILD						
			NET_PRINT_TIME() NET_PRINT_STRINGS("NET_LOAD_AND_LAUNCH_SCRIPT_FM: Launched - ", sScriptNamePassed)
			NET_PRINT_STRING_INT(" with thread ID = ", NATIVE_TO_INT(thread)) 
			NET_PRINT_STRING_INT( " and stack size = ", iStackSizePassed)
			NET_NL()
			#ENDIF
			
			//This is only set if it's a mission as this also launches strip club etc
			IF NOT bIsAmbiant //bIsMission
				SET_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_MissionLaunchInProgress)
			ENDIF
			CLEAR_BIT(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].currentMissionData.iBitSet, ciFMMC_MISSION_DATA_BitSet_SKIP_JOINABLE_CHECKS)
			
			IF bClearWantedOnLaunch
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID())> 0
				
					// JamesA: Prevent wanted level from being awarded if player warps to job
					UPDATE_PLAYER_WANTED_LEVEL_JOINING_MISSION()
				
					PRINTLN("NET_LOAD_AND_LAUNCH_SCRIPT_FM - CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())")
					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
				ENDIF
			ENDIF
			RETURN TRUE

		ENDIF
		
	ELSE	
		NET_PRINT_TIME() NET_PRINT_STRINGS("NET_LOAD_AND_LAUNCH_SCRIPT_FM:  Script does not exist: ", sScriptNamePassed) NET_NL()
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////// Launching Missions ////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//PURPOSE: This FUNC calls REQUEST_SCRIPT, must be called more than once, DOES NOT WAIT for the script to load, only starts the script it is loaded.
FUNC BOOL NET_LOAD_AND_LAUNCH_SCRIPT(STRING paramScriptName, INT paramStackSize)
		
	IF DOES_SCRIPT_EXIST(paramScriptName)
	
		NET_PRINT_TIME() NET_PRINT_STRINGS("NET_LOAD_AND_LAUNCH_SCRIPT_NO_WAIT: Launching - ", paramScriptName)  NET_NL()
		
		REQUEST_SCRIPT(paramScriptName)
			
		IF HAS_SCRIPT_LOADED(paramScriptName)			
			
			IF paramStackSize <= 0
				NET_PRINT_TIME() NET_PRINT("NET_LOAD_AND_LAUNCH_SCRIPT_NO_WAIT paramStackSize <= 0 !!!!", ePRINT_NET_ERROR)
				paramStackSize = MINI_STACK_SIZE
			ENDIF
			
			// KGM 3/12/12: I can't put a check in here to see if code may still be cleaning up a script (see function below) because
			//				there could genuinely be multiple versions of some scripts required, so the check needs to go around
			//				individual script calls to this function where a problem has arisen.
			
			THREADID thread = START_NEW_SCRIPT(paramScriptName, paramStackSize)
			SET_SCRIPT_AS_NO_LONGER_NEEDED(paramScriptName)
									
			NET_PRINT_TIME() NET_PRINT_STRINGS("NET_LOAD_AND_LAUNCH_SCRIPT_NO_WAIT: Launched - ", paramScriptName)
			NET_PRINT_STRING_INT(" with thread ID = ", NATIVE_TO_INT(thread)) 
			NET_PRINT_STRING_INT( " and stack size = ", paramStackSize)
			NET_NL()
			
			RETURN TRUE

		ENDIF
		
	ELSE	
		NET_PRINT_TIME() NET_PRINT_STRINGS("NET_LOAD_AND_LAUNCH_SCRIPT_NO_WAIT:  Script does not exist: ", paramScriptName) NET_NL()
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE: Loads then launches a mission script, passing missionData as args to the launched script.  This will setup a mission and instance.
/// NOTES:	 Added a new temporary variable defaulting to FALSE so that I can tell if this function is called by my new routines or the older stuff.
FUNC BOOL NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS(MP_MISSION_DATA missionData, BOOL paramCalledFromMissionTriggerer = FALSE)

	DEBUG_PRINTCALLSTACK()

	//Bail as we are killing all scripts. 
	IF SHOULD_TRANSITION_SESSION_KILL_ALL_MP_SCRIPTS()
		#IF IS_DEBUG_BUILD
		NET_PRINT(" (NOT called using Mission Triggerer)")
		#ENDIF
		RETURN FALSE
	ENDIF
	
	// KGM 31/8/12: My new parameter doesn't get used in release, so I'm going to fool the compiler - this variable will be removed at some point anyway.
	paramCalledFromMissionTriggerer = paramCalledFromMissionTriggerer

	TEXT_LABEL_31 tl32ScriptName = GET_MP_MISSION_NAME(missionData.mdID.idMission)
	
	IF DOES_SCRIPT_EXIST(tl32ScriptName)
	
		REQUEST_SCRIPT(tl32ScriptName)
	
		#IF IS_DEBUG_BUILD
		NET_PRINT_STRINGS("...KGM MP: NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS: Launching - ", tl32ScriptName)
		IF (paramCalledFromMissionTriggerer)
			NET_PRINT(" (called by Mission Triggerer)")
		ELSE
			NET_PRINT(" (NOT called using Mission Triggerer)")
		ENDIF
		NET_NL()
		#ENDIF
		
		IF HAS_SCRIPT_LOADED(tl32ScriptName)
			// KGM 3/12/12: Don't launch the script if another version with the same instance ID is still running - when script thinks the script is
			//					removed, code can still be cleaning up under-the-hood waiting for other particpants to acknowledge they've quit the script.
			BOOL localCheckOnly = TRUE
			
			// KGM 3/10/14 [BUG 2063716]: For scripts launched through the mission triggerer, ensure ALL previous instances of the script have cleaned up.
			//		This is to ensure a second copy doesn't launch which then also tries to allocate broadcast data when there are no available slots.
			IF (paramCalledFromMissionTriggerer)
				// ...from Mission Triggerer, so check for any previous instances still hanging around
				INT oldInstanceLoop = 0
				REPEAT MAX_NUM_MISSIONS_ALLOWED oldInstanceLoop
					IF (NETWORK_IS_SCRIPT_ACTIVE(tl32ScriptName, oldInstanceLoop, localCheckOnly))
						#IF IS_DEBUG_BUILD
							NET_PRINT("...KGM MP: DELAYING LAUNCH: Code must still be cleaning up a previous instance: ")
							NET_PRINT(tl32ScriptName)
							NET_PRINT(" (instID=")
							NET_PRINT_INT(oldInstanceLoop)
							NET_PRINT(") - Only one Mission Triggerer script can be active at once")
							NET_NL()
						#ENDIF
						
						RETURN FALSE
					ENDIF
				ENDREPEAT
			ELSE
				// ...not from mission triggerer, so just wait for duplicate instance still hanging around
				IF (NETWORK_IS_SCRIPT_ACTIVE(tl32ScriptName, missionData.iInstanceId, localCheckOnly))
					#IF IS_DEBUG_BUILD
						NET_PRINT("...KGM MP: DELAYING LAUNCH: Code must still be cleaning up a previous instance with duplicate details: ")
						NET_PRINT(tl32ScriptName)
						NET_PRINT(" (instID=")
						NET_PRINT_INT(missionData.iInstanceId)
						NET_PRINT(")")
						NET_NL()
					#ENDIF
					
					RETURN FALSE
				ENDIF
			ENDIF
			
			THREADID thread = START_NEW_SCRIPT_WITH_ARGS(tl32ScriptName, missionData, SIZE_OF(missionData), GET_MP_MISSION_STACK_SIZE(missionData.mdID.idMission) )
			SET_SCRIPT_AS_NO_LONGER_NEEDED(tl32ScriptName)
			
			IF NATIVE_TO_INT(thread) > 0 
						
				#IF IS_DEBUG_BUILD
				NET_PRINT_TIME()
				NET_PRINT_STRINGS("NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS: Launched = ", tl32ScriptName)
				NET_PRINT_STRING_INT(" with thread ID = ", NATIVE_TO_INT(thread)) 
				NET_PRINT_STRING_INT(" and instance ID = ", missionData.iInstanceId) 
				NET_NL()
				#ENDIF
				
				RETURN TRUE
			
			ELSE			
				NET_PRINT_STRINGS("...KGM MP: NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS: Bad thread ID: ", tl32ScriptName) NET_NL()
			ENDIF
			
		ENDIF
		
	ELSE	
		NET_PRINT_STRINGS("...KGM MP: NET_LOAD_AND_LAUNCH_SCRIPT_WITH_ARGS:  Script does not exist: ", tl32ScriptName) NET_NL()
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE: Checks to see if a player can launch a specific mission.
FUNC BOOL CAN_PLAYER_LAUNCH_MP_MISSION(PLAYER_INDEX playerId, MP_MISSION mission)
	
	IF IS_NET_PLAYER_OK(playerId)	
	
		// Make sure the player can go on a mission.
		IF IS_PLAYER_ON_ANY_MP_MISSION(playerId)		
			NET_PRINT_TIME() NET_PRINT_THREAD_ID()			
			NET_PRINT("CAN_PLAYER_LAUNCH_MP_MISSION - FALSE - Player on a mission", ePRINT_NET_ERROR)
			RETURN FALSE
		ENDIF
		
		IF IS_MP_MISSION_DISABLED(mission)
				NET_PRINT_TIME() NET_PRINT_THREAD_ID()
				NET_PRINT("CAN_PLAYER_LAUNCH_MP_MISSION - IS_MP_MISSION_DISABLED - TRUE - Mission disabled.", ePRINT_NET_ERROR)
				RETURN FALSE
		ENDIF
		
		IF mission = eNULL_MISSION
			NET_PRINT_TIME() NET_PRINT_THREAD_ID()
			NET_PRINT("CAN_PLAYER_LAUNCH_MP_MISSION - FALSE - Mission is null mission.", ePRINT_NET_ERROR)
			RETURN FALSE	
		ENDIF		
		
		RETURN TRUE
	
	ENDIF

	NET_PRINT_TIME() NET_PRINT_THREAD_ID()
	NET_PRINT("CAN_PLAYER_LAUNCH_MP_MISSION - FALSE - IS_NET_PLAYER_OK = FALSE.", ePRINT_NET_ERROR)	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE: Checks if we are already in the process of launching a mission.  
FUNC BOOL IS_MP_MISSION_LAUNCH_IN_PROGRESS()
	RETURN MPGlobals.g_IsMissionLaunching
ENDFUNC

// PURPOSE: Use this to launch a CnC mission.  This will become your main active mission and is processed in MAINTAIN_MISSION_LAUNCH().
//
// RETURN VALUE:		BOOL			TRUE if the launch is being attempted, FALSE if it failed to launch this mission.
//
// NOTES:	MAINTAIN_MISSION_LAUNCH() will launch the mission.
FUNC BOOL SET_MP_MISSION_TO_LAUNCH(MP_MISSION_DATA missionLaunchData)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: SET_MP_MISSION_TO_LAUNCH(): [Mission: ")
		NET_PRINT(GET_MP_MISSION_NAME(missionLaunchData.mdID.idMission))
		NET_PRINT("]  [Instance: ")
		NET_PRINT_INT(missionLaunchData.iInstanceId)
		NET_PRINT("]")
		NET_NL()
		NET_PRINT("           (called by: ") NET_PRINT(GET_THIS_SCRIPT_NAME()) NET_PRINT(")") NET_NL()
	#ENDIF

	// Check for NULL mission
	IF (missionLaunchData.mdID.idMission = eNULL_MISSION)
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........REJECTED - Mission name is eNULL_Mission") NET_NL()
		#ENDIF
		
		RETURN FALSE
	ENDIF
	
	// Check if a mission is already running
	IF (IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID()))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........A Mission is already in progress: ")
		#ENDIF
			
		IF (IS_PLAYER_RUNNING_MP_MISSION_INSTANCE(PLAYER_ID(), missionLaunchData.mdID.idMission, missionLaunchData.iInstanceId))
			// ...same mission and instance
			#IF IS_DEBUG_BUILD
				NET_PRINT("same mission and instance. Don't request mission again. Return True.") NET_NL()
			#ENDIF
			
			RETURN TRUE
		ENDIF
		
		// A different mission and instance is already running
		#IF IS_DEBUG_BUILD
			NET_PRINT("REJECTED: Different mission and instance. Kill this new request (Return FALSE).") NET_NL()
		#ENDIF
		
		RETURN FALSE		
	ENDIF
	
	// Check if a mission is already being launched
	IF (IS_MP_MISSION_LAUNCH_IN_PROGRESS())
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........Mission Launch already in progress: ")
		#ENDIF
		
		// Check if the launching mission is this mission.
		IF (MPGlobals.g_MyMissionToLaunchInfo.mdID.idMission = missionLaunchData.mdID.idMission)
		AND (MPGlobals.g_MyMissionToLaunchInfo.iInstanceId = missionLaunchData.iInstanceId)
			// ...same mission
			#IF IS_DEBUG_BUILD
				NET_PRINT("same mission and instance. Don't request mission again. Return True.") NET_NL()
			#ENDIF
		
			RETURN TRUE
		ENDIF
		
		// A different mission is already being launched
		#IF IS_DEBUG_BUILD
			NET_PRINT("REJECTED: Different mission and instance. Kill this new request (Return FALSE).") NET_NL()
		#ENDIF
		
		RETURN FALSE		
	ENDIF
	
	// Ensure the player can launch this MP Mission
	IF NOT (CAN_PLAYER_LAUNCH_MP_MISSION(PLAYER_ID(), missionLaunchData.mdID.idMission))
		#IF IS_DEBUG_BUILD
			NET_PRINT("...........REJECTED: CAN_PLAYER_LAUNCH_MP_MISSION() failed. Kill this request (Return FALSE).") NET_NL()
		#ENDIF
		
		RETURN FALSE	
	ENDIF
	
	// Set the globals to be read by MAINTAIN_MISSION_LAUNCH()
	MPGlobals.g_MyMissionToLaunchInfo = missionLaunchData
	MPGlobals.g_IsMissionLaunching = TRUE
	
	#IF IS_DEBUG_BUILD
		NET_PRINT("...........Mission Launch details successfully stored ready for processing.") NET_NL()
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC




/// PURPOSE: Sets a script to be a network script and handles mission instancing.
PROC SETUP_MP_MISSION_FOR_NETWORK(INT iMaxNumMissionParticipants, MP_MISSION_DATA missionScriptArgs )	

	// added by neil 8/11/2011 - if this is called by a script when the network game is not running it should terminate
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		NET_PRINT_TIME() 
		NET_PRINT_THREAD_ID()
		NET_PRINT( GET_THIS_SCRIPT_NAME() )	
		NET_PRINT("SETUP_MP_MISSION_FOR_NETWORK Script to terminate - NETWORK_IS_GAME_IN_PROGRESS = FALSE")
		NET_NL()
		TERMINATE_THIS_MULTIPLAYER_THREAD_NO_ARGS()			
	ENDIF	

	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(iMaxNumMissionParticipants, FALSE, missionScriptArgs.iInstanceId )   
	#IF IS_DEBUG_BUILD
		NET_PRINT_TIME()
		NET_PRINT_THREAD_ID()
		NET_PRINT("SETUP_MP_MISSION_FOR_NETWORK setting net script - ")
		NET_PRINT(GET_MP_MISSION_NAME(missionScriptArgs.mdID.idMission))
		NET_PRINT(" - iMaxNumMissionParticipants - ") NET_PRINT_INT(iMaxNumMissionParticipants)
		NET_PRINT(" - iInstanceId - ") NET_PRINT_INT(missionScriptArgs.iInstanceId)
		NET_NL()
				
		IF iMaxNumMissionParticipants <> GET_MAX_NUM_PARTICIPANTS_FOR_MP_MISSION(missionScriptArgs.mdID.idMission)
			NET_PRINT("SETUP_MP_MISSION_FOR_NETWORK iMaxNumMissionParticipants does not match missionData.iMaxNumParticipants set in GET_MISSION_DATA", ePRINT_NET_ERROR)
			NET_SCRIPT_ASSERT("SETUP_MP_MISSION_FOR_NETWORK iMaxNumMissionParticipants does not match GET_MISSION_DATA!")
		ENDIF

	#ENDIF
ENDPROC


//PURPOSE: Select a random team from the ones available for use in missions
// KGM 27/10/11: bEnsurePlayersAreFree - added so that Witness To Court doesn't pick a team that are all busy (TEMP: this will be handled by my new routines)
FUNC INT GET_RANDOM_TEAM_FOR_MP_MISSION() 

	// TEMP FUNCTIONALITY: The new Broadcast functions that this function calls will get moved into the missions themselves when this routine is removed.
	// NOTE: We'll need the Mission to pass the MP_MISSION_DATA as a parameter
	// NOTE: This new routine will need to pass back the TeamID of the Team that has been pre-selected to join the mission so that the mission continues as before
	#IF IS_DEBUG_BUILD
		NET_PRINT("...KGM MP: Mission Call to GET_RANDOM_TEAM_FOR_MP_MISSION() is being re-directed to new Mission Control Broadcast routines.") NET_NL()
		NET_PRINT("      NOTE: This function will return a TeamID to the calling script which may then also try to make that Team join - resulting in an ignorable double call") NET_NL()
	#ENDIF
	
	// Find the other Team that the new routines have already pre-selected to join the mission so that it can be temporarily returned to the calling script.
	// I'll do this by checking the mission control host broadcast data and if I find the data (which I should unless bad lag) I'll check if I should send it.
	INT thisUniqueID = GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].activeMission.mdUniqueID
	INT tempLoop = 0
	INT theMissionControlSlot = -1
	REPEAT MAX_NUM_MISSIONS_ALLOWED tempLoop
		IF (thisUniqueID = GlobalServerBD_MissionRequest.missionRequests[tempLoop].mrMissionData.mdUniqueID)
			theMissionControlSlot = tempLoop

			#IF IS_DEBUG_BUILD
				NET_PRINT("           Found this mission within the Mission Control host array at slot: ") NET_PRINT_INT(tempLoop) NET_NL()
			#ENDIF
		ENDIF
	ENDREPEAT
	
	// If the mission data wasn't found, return TEAM_INVALID, the calling script should be able to handle that and perhaps try again later
	IF (theMissionControlSlot = -1)
		#IF IS_DEBUG_BUILD
			NET_PRINT("           Failed to find this mission within the Mission Control host array at slot: ") NET_PRINT_INT(tempLoop) NET_PRINT(" - Returning TEAM_INVALID") NET_NL()
		#ENDIF
		
		RETURN TEAM_INVALID
	ENDIF
	
//	INT reservedTeams = GlobalServerBD_MissionRequest.missionRequests[theMissionControlSlot].mrBitsReservedTeams
//	INT joiningTeams = GlobalServerBD_MissionRequest.missionRequests[theMissionControlSlot].mrBitsJoinableTeams
	
//	INT thisPlayersTeam = GET_PLAYER_TEAM(PLAYER_ID())
		
	// Search the mission data looking for a Reserved Team that hasn't Joined
	// As an extra safety check, make sure the team isn't this player's team - just in case the Joining Data has already been cleared for the Team that joined at launch
	// NOTE: If all teams join at launch but the mission still calls for more then this check will fail and return TEAM_INVALID because it won't find a reserved team that hasn't
	//			joined. To fix this I'll store a reserve team of any team that isn't the player's team just in case this happens.
	BOOL allowBroadcast = FALSE
	INT theOtherTeam = TEAM_INVALID
	INT backupTeam = TEAM_INVALID
//	REPEAT MAX_NUM_TEAMS tempLoop
//		IF NOT (thisPlayersTeam = tempLoop)
//			IF (IS_BIT_SET(reservedTeams, tempLoop))
//				IF NOT (IS_BIT_SET(joiningTeams, tempLoop))
//					#IF IS_DEBUG_BUILD
//						NET_PRINT("           Found Team Reserved for mission but not Joinable: ") NET_PRINT(Convert_Team_To_String(tempLoop)) NET_NL()
//					#ENDIF
//					
//					allowBroadcast = TRUE
//					theOtherTeam = tempLoop
//				ELSE
//					// This isn't the player's team but it is both reserved and joined already
//					// Just in case all teams join at launch but the mission still requests more, make a note of a backup team to pass back to the mission
//					IF (backupTeam = TEAM_INVALID)
//						//...store this so there is a valid team to pass back to the mission
//						#IF IS_DEBUG_BUILD
//							NET_PRINT("           Store backup team to pass back to mission (just in case): ") NET_PRINT(Convert_Team_To_String(tempLoop)) NET_NL()
//						#ENDIF
//					
//						backupTeam = tempLoop
//					ENDIF
//				ENDIF
//			ENDIF
//		ELSE
//			#IF IS_DEBUG_BUILD
//				NET_PRINT("           IGNORING this player's team: ") NET_PRINT(Convert_Team_To_String(thisPlayersTeam)) NET_NL()
//			#ENDIF
//		ENDIF
//	ENDREPEAT
	
	IF NOT (allowBroadcast)
		// If a backup team has been stored, then all teams joined the mission at launch, so pass the backup team back to the mission and ignore the broadcast
		IF NOT (backupTeam = TEAM_INVALID)
			#IF IS_DEBUG_BUILD
				NET_PRINT("              FAILED TO FIND A PRE-ALLOCATED SECOND TEAM. There is a BACKUP TEAM stored, so all teams must have joined at launch. Returning Backup Team.") NET_NL()
			#ENDIF
			
			RETURN backupTeam
		ENDIF
		
		#IF IS_DEBUG_BUILD
			NET_PRINT("              FAILED TO FIND A PRE-ALLOCATED SECOND TEAM. Returning TEAM_INVALID") NET_NL()
		#ENDIF
		
		RETURN TEAM_INVALID
	ENDIF
	
	// KGM NOTE: Using the contents of the player's 'active mission' data for now - this should instead be the args supplied to the mission on launch
	IF (allowBroadcast)
		Broadcast_Mission_Ready_For_Secondary_Teams(GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].activeMission)
	ENDIF
	
	// Return the Other Team back to the calling function so it can continue to function
	RETURN theOtherTeam
	
ENDFUNC

/// PURPOSE: Finds a new, unique instance id for a mission.
FUNC INT GET_NEW_UNIQUE_MP_MISSION_INSTANCE_ID(MP_MISSION mission)
	
	INT iHighestInstaceId = DEFAULT_MISSION_INSTANCE
	
	INT iPlayer 
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerId = INT_TO_PLAYERINDEX(iPlayer)
		IF GET_MP_MISSION_PLAYER_IS_ON(playerId) = mission
			IF GET_MP_MISSION_INSTANCE_PLAYER_IS_ON(playerId) >= iHighestInstaceId
				iHighestInstaceId ++
			ENDIF
		ENDIF
	ENDREPEAT
	
	// Just to be sure!
	iHighestInstaceId ++
	
	RETURN iHighestInstaceId
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

//PURPOSE: Check if a mission is already in the Joinable Player List
FUNC BOOL IS_MP_MISSION_IN_JOINABLE_LIST_ALREADY(MP_MISSION Mission, INT iInstanceId, INT &iReturnListSlot)
	INT i 

	REPEAT MAX_NUM_MISSIONS_ALLOWED i
		IF GlobalServerBD_MissionList.MissionList[i].Mission = Mission 
			IF GlobalServerBD_MissionList.MissionList[i].iInstanceId = iInstanceId 
				iReturnListSlot = i
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT 
	RETURN FALSE
ENDFUNC

//PURPOSE: Check if a mission is already joinable by player
FUNC BOOL IS_MP_MISSION_JOINABLE_BY_PLAYER(MP_MISSION Mission, INT iInstanceId, PLAYER_INDEX inPlayer)
	INT iMissionListSlot
	IF IS_MP_MISSION_IN_JOINABLE_LIST_ALREADY(Mission, iInstanceId, iMissionListSlot) 
		IF NOT IS_BIT_SET(GlobalServerBD_MissionList.MissionList[iMissionListSlot].WhichPlayersAreExcluded, NATIVE_TO_INT(inPlayer)) 
			IF IS_BIT_SET(GlobalServerBD_MissionList.MissionList[iMissionListSlot].WhichPlayersCanJoinMission, NATIVE_TO_INT(inPlayer))				
			OR IS_BIT_SET(GlobalServerBD_MissionList.MissionList[iMissionListSlot].WhichTeamsCanJoinMission, GET_PLAYER_TEAM(inPlayer))
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE	
ENDFUNC


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////// Abandoning Missions ///////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/// PURPOSE:
///    Checks if we the crrent active mission has been set to end. 
/// RETURNS:
///    True if the mission is ending, false otherwise.
FUNC BOOL IS_FORCE_ABANDON_CURRENT_MP_MISSION_SET()  	

	RETURN MPGlobals.g_bForceKillActiveMission
	
ENDFUNC

/// PURPOSE:
///    Calling this will force the current active mission to end.
PROC FORCE_ABANDON_CURRENT_MP_MISSION()  

	IF NOT IS_FORCE_ABANDON_CURRENT_MP_MISSION_SET()		
		
		#IF IS_DEBUG_BUILD
			IF NETWORK_IS_GAME_IN_PROGRESS()
				NET_PRINT_TIME()
				NET_PRINT_THREAD_ID()
				NET_PRINT("FORCE_ABANDON_CURRENT_MP_MISSION Set.\n")
			ELSE
				PRINTLN("FORCE_ABANDON_CURRENT_MP_MISSION Set.")
			ENDIF
		#ENDIF
		
		IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
			#IF IS_DEBUG_BUILD
				IF NETWORK_IS_GAME_IN_PROGRESS()
					NET_PRINT_TIME()
					NET_PRINT_THREAD_ID()
					NET_PRINT("FORCE_ABANDON_CURRENT_MP_MISSION Player was not on mission, leave not set.")
				ELSE
					PRINTLN("FORCE_ABANDON_CURRENT_MP_MISSION Player was not on mission, leave not set.")
				ENDIF
			#ENDIF
			EXIT
		ENDIF
		
		MPGlobals.g_bForceKillActiveMission = TRUE
		
	ENDIF

ENDPROC


/// PURPOSE:
///    Clears any abandon mission flags.
PROC CLEAR_ABANDON_MP_MISSION_FLAG()

	IF IS_FORCE_ABANDON_CURRENT_MP_MISSION_SET()
	
		#IF IS_DEBUG_BUILD
		NET_PRINT_TIME()
		NET_PRINT_THREAD_ID()
		NET_PRINT("CLEAR_ABANDON_MP_MISSION_FLAG called.\n")
		#ENDIF
		
		MPGlobals.g_bForceKillActiveMission = FALSE
		
	ENDIF

ENDPROC


/// PURPOSE:
///    Called every frame in mission scripts, checks to see if the player wants to end their current mission.
/// RETURNS:
///    True if the player should leave mission.
FUNC BOOL SHOULD_PLAYER_LEAVE_MP_MISSION(BOOL bDoFrozenBlurredScreen = FALSE) 

	IF IS_FORCE_ABANDON_CURRENT_MP_MISSION_SET()
	
		#IF IS_DEBUG_BUILD
		NET_PRINT_TIME()
		NET_PRINT_THREAD_ID()
		NET_PRINT(">> SHOULD_PLAYER_LEAVE_MP_MISSION Is returning TRUE, you should end your mission.\n")
		#ENDIF	
		
		IF bDoFrozenBlurredScreen
			IF NOT g_bFM_ON_IMPROMPTU_DEATHMATCH
			AND NOT IS_PLAYER_ON_MP_AMBIENT_SCRIPT(PLAYER_ID(), MPAM_TYPE_RACETOPOINT)
				ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
				TOGGLE_RENDERPHASES(FALSE)
				PRINTLN("SHOULD_PLAYER_LEAVE_MP_MISSION called with bDoFrozenBlurredScreen - calling ANIMPOSTFX_PLAY(MinigameTransitionIn) and TOGGLE_RENDERPHASES(FALSE)")
			ENDIF
		ENDIF
		
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE: Called from mission scripts when the player wants to leave the mission.
PROC LEAVE_MY_MP_MISSION(MP_MISSION mission)
	
	#IF IS_DEBUG_BUILD
		NET_PRINT_TIME()
		NET_PRINT_THREAD_ID()
		NET_PRINT_STRINGS("LEAVE_MY_MP_MISSION Called for - ", GET_MP_MISSION_NAME(mission)) NET_NL()
	#ENDIF
	
	IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
		#IF IS_DEBUG_BUILD
		NET_PRINT_TIME()
		NET_PRINT_THREAD_ID()
		NET_PRINT("LEAVE_MY_MP_MISSION Player is not on any mission.", ePRINT_NET_ERROR)
		#ENDIF		
		EXIT
	ENDIF
	
	IF NOT (GET_MP_MISSION_PLAYER_IS_ON(PLAYER_ID()) = mission)
		#IF IS_DEBUG_BUILD
		NET_PRINT_TIME()
		NET_PRINT_THREAD_ID()
		NET_PRINT_STRINGS("LEAVE_MY_MP_MISSION Player is not on this mission ", GET_MP_MISSION_NAME(mission), ePRINT_NET_ERROR)
		#ENDIF	
		EXIT
	ENDIF
		
	//TERMINATE_THIS_THREAD()
	
ENDPROC

FUNC BOOL LOCAL_PLAYER_IS_CLEAR_OF_MP_MISSIONS()
	IF IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
		IF NOT IS_FORCE_ABANDON_CURRENT_MP_MISSION_SET()
			FORCE_ABANDON_CURRENT_MP_MISSION() 
			NET_PRINT("LOCAL_PLAYER_IS_CLEAR_OF_MP_MISSIONS() - calling FORCE_ABANDON_CURRENT_MP_MISSION()") NET_NL()
		ELSE
			NET_PRINT("LOCAL_PLAYER_IS_CLEAR_OF_MP_MISSIONS() - waiting for mission to abandon") NET_NL()
		ENDIF	
	ELSE
		NET_PRINT("LOCAL_PLAYER_IS_CLEAR_OF_MP_MISSIONS() - returning TRUE") NET_NL()
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC SET_MISSION_FINISHED(SCRIPT_TIMER& theTimer)
	IF NOT theTimer.bInitialisedTimer
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			START_NET_TIMER(theTimer)
			NET_PRINT_STRINGS("SET_MISSION_FINISHED has been called by server of script: ", GET_THIS_SCRIPT_NAME()) NET_NL()
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_MISSION_READY_TO_CLEANUP(SCRIPT_TIMER& theTimer)
	IF theTimer.bInitialisedTimer
		IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(),theTimer.timer)) >= 1000
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC




// ===========================================================================================================
//      Mission Timer globals - KGM 15/5/12
//		Helper functions for help text to prompt the player to start a mission.
// ===========================================================================================================

//PURPOSE: (PLACEHOLDER - UNTIL WE CAN DOWNLOAD THE ANGLED AREA) Checks if the a Gang Attack should launch based on an angled area 
FUNC BOOL SHOULD_GANG_ATTACK_LAUNCH(INT iMissionVariation)	//INT iMissionType, VECTOR vStartPoint, 
	
	//g_FMMC_ROCKSTAR_CREATED.vStartPos[iMissionVariation]	
	//g_FMMC_ROCKSTAR_GANG_ATTACKS.vPos[g_FMMC_ROCKSTAR_CREATED.iSubType[iMissionVariation]]
	//g_FMMC_ROCKSTAR_GANG_ATTACKS.fRadius[g_FMMC_ROCKSTAR_CREATED.iSubType[iMissionVariation]]
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), g_FMMC_ROCKSTAR_GANG_ATTACKS.vMin[g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionVariation].iSubType], g_FMMC_ROCKSTAR_GANG_ATTACKS.vMax[g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionVariation].iSubType], g_FMMC_ROCKSTAR_GANG_ATTACKS.fRadius[g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionVariation].iSubType])
	AND ARE_NETWORK_ENTITIES_AVAILABLE_FOR_SCRIPT_LAUNCH(6, 0, 3, FALSE, TRUE)
		NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - TRUE - vMin = ") NET_PRINT_VECTOR(g_FMMC_ROCKSTAR_GANG_ATTACKS.vMin[g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionVariation].iSubType]) NET_NL()
		NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - TRUE - vMax = ") NET_PRINT_VECTOR(g_FMMC_ROCKSTAR_GANG_ATTACKS.vMax[g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionVariation].iSubType]) NET_NL()
		NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - TRUE - fRadius (width) = ") NET_PRINT_FLOAT(g_FMMC_ROCKSTAR_GANG_ATTACKS.fRadius[g_FMMC_ROCKSTAR_CREATED.sMissionHeaderVars[iMissionVariation].iSubType]) NET_NL()
		//NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - vStartPoint = ") NET_PRINT_VECTOR(g_FMMC_ROCKSTAR_CREATED.vStartPos[iMissionVariation]	) NET_NL()
		RETURN TRUE
	ENDIF
	
//	IF iMissionType = FMMC_TYPE_GANGHIDEOUT
//		//ALLEY
//		IF ARE_VECTORS_ALMOST_EQUAL(<<-1120.797485, -1602.936646, 3.398684>>, vStartPoint, 25.0)
//			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1129.435547,-1588.780518,3.398673>>, <<-1064.829468,-1676.151978,23.548561>>, 50.0)
//				NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - ALLEY - vStartPoint = ") NET_PRINT_VECTOR(vStartPoint) NET_NL()
//				RETURN TRUE
//			ENDIF
//			
//		//APARTMENTS
//		ELIF ARE_VECTORS_ALMOST_EQUAL(<<-104.385773, -1602.550781, 30.748196>>, vStartPoint, 25.0)
//			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-112.064445,-1641.133057,28.967455>>, <<-75.639290,-1599.398438,45.962044>>, 36.0)
//				NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - APARTMENTS - vStartPoint = ") NET_PRINT_VECTOR(vStartPoint) NET_NL()
//				RETURN TRUE
//			ENDIF
//			
//		//Hills House
//		ELIF ARE_VECTORS_ALMOST_EQUAL(<<-1872.063599, 192.544022, 83.30809>>, vStartPoint, 25.0)
//			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1881.260620,201.257446,83.290009>>, <<-1844.195435,228.595993,99.404289>>, 55.0)
//				NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Hills House - vStartPoint = ") NET_PRINT_VECTOR(vStartPoint) NET_NL()
//				RETURN TRUE
//			ENDIF
//			
//		//Trailer Park
//		ELIF ARE_VECTORS_ALMOST_EQUAL(<<74.053543, 3643.255127, 38.519753>>, vStartPoint, 25.0)
//			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<78.510849,3640.867676,13.696705>>, <<40.199944,3755.507080,53.622459>>, 110.0)
//				NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Trailer Park - vStartPoint = ") NET_PRINT_VECTOR(vStartPoint) NET_NL()
//				RETURN TRUE
//			ENDIF
//			
//		//Hangar
//		ELIF ARE_VECTORS_ALMOST_EQUAL(<<-984.1312, -2972.7429, 12.94502>>, vStartPoint, 25.0)
//			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1004.507263,-2969.590576,12.944477>>, <<-922.084167,-3017.556152,30.945072>>,108.0 )
//				NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Hangar - vStartPoint = ") NET_PRINT_VECTOR(vStartPoint) NET_NL()
//				RETURN TRUE
//			ENDIF
//			
//		//Plane Junkyard
//		ELIF ARE_VECTORS_ALMOST_EQUAL(<<2380.968018, 3118.08667, 47.117702>>, vStartPoint, 25.0)
//			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2329.593506,3092.940186,47.126545>>, <<2434.994629,3093.640625,67.043114>>, 134.0)
//				NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Plane Junkyard - vStartPoint = ") NET_PRINT_VECTOR(vStartPoint) NET_NL()
//				RETURN TRUE
//			ENDIF
//		
//		//Cargo Ship
//		ELIF ARE_VECTORS_ALMOST_EQUAL(<<1220.402588, -2993.949463, 4.90472>>, vStartPoint, 25.0)
//			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1221.584351,-2965.532471,-5.134671>>, <<1259.952759,-2966.432617,41.361275>>, 185.0)
//				NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Cargo Ship - vStartPoint = ") NET_PRINT_VECTOR(vStartPoint) NET_NL()
//				RETURN TRUE
//			ENDIF
//		
//		//Boat Warehouse
//		ELIF ARE_VECTORS_ALMOST_EQUAL(<<34.360451, -2650.201416, 5.000549>>, vStartPoint, 25.0)
//			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<33.819256,-2649.222412,5.000579>>, <<34.100761,-2759.480225,28.904459>>,56.0 )
//				NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Boat Warehouse - vStartPoint = ") NET_PRINT_VECTOR(vStartPoint) NET_NL()
//				RETURN TRUE
//			ENDIF
//			
//		//Hobo Town
//		ELIF ARE_VECTORS_ALMOST_EQUAL(<<1483.637451, 6407.416992, 21.424854>>, vStartPoint, 25.0)
//			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1483.520752,6406.435547,16.453182>>, <<1454.620972,6299.306152,35.458946>>, 200.0)
//				NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Hobo Town - vStartPoint = ") NET_PRINT_VECTOR(vStartPoint) NET_NL()
//				RETURN TRUE
//			ENDIF
//			
//		//Farmhouse
//		ELIF ARE_VECTORS_ALMOST_EQUAL(<<2455.516602, 4952.11084, 44.116669>>, vStartPoint, 25.0)
//			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2459.457764,4959.946777,29.160881>>, <<2434.458252,4984.021484,75.034569>>, 50.0)
//				NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Farmhouse - vStartPoint = ") NET_PRINT_VECTOR(vStartPoint) NET_NL()
//				RETURN TRUE
//			ENDIF
//			
//		ENDIF
//	ENDIF
	
	RETURN FALSE
ENDFUNC

////PURPOSE: (PLACEHOLDER - UNTIL WE CAN DOWNLOAD THE ANGLED AREA) Checks if the a Gang Attack should launch based on an angled area 
//FUNC VECTOR GET_GANG_ATTACK_VECTOR(TEXT_LABEL_31 thisString)
//	
//	//IF iMissionType = FMMC_TYPE_GANGHIDEOUT
//		//Alley
//		IF ARE_STRINGS_EQUAL(thisString, "Alley")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - ALLEY - Vector = ") NET_PRINT_VECTOR(<<-1064.829468,-1676.151978,23.548561>>) NET_NL()
//			RETURN <<-1064.829468,-1676.151978,23.548561>>
//			
//		//Apartments
//		ELIF ARE_STRINGS_EQUAL(thisString, "Apartments")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - APARTMENTS - Vector = ") NET_PRINT_VECTOR(<<-75.639290,-1599.398438,45.962044>>) NET_NL()
//			RETURN <<-75.639290,-1599.398438,45.962044>>
//			
//		//Hills House
//		ELIF ARE_STRINGS_EQUAL(thisString, "Hills House")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Hills House - Vector = ") NET_PRINT_VECTOR(<<-1844.195435,228.595993,99.404289>>) NET_NL()
//			RETURN <<-1844.195435,228.595993,99.404289>>
//			
//		//Trailer Park
//		ELIF ARE_STRINGS_EQUAL(thisString, "Trailer Park")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Trailer Park - Vector = ") NET_PRINT_VECTOR(<<40.199944,3755.507080,53.622459>>) NET_NL()
//			RETURN <<40.199944,3755.507080,53.622459>>
//			
//		//Hangar
//		ELIF ARE_STRINGS_EQUAL(thisString, "Hangar")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Hangar - Vector = ") NET_PRINT_VECTOR(<<-922.084167,-3017.556152,30.945072>>) NET_NL()
//			RETURN <<-922.084167,-3017.556152,30.945072>>
//			
//		//Plane Junkyard
//		ELIF ARE_STRINGS_EQUAL(thisString, "Plane Junkyard")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Plane Junkyard - Vector = ") NET_PRINT_VECTOR(<<2434.994629,3093.640625,67.043114>>) NET_NL()
//			RETURN <<2434.994629,3093.640625,67.043114>>
//		
//		//Cargo Ship
//		ELIF ARE_STRINGS_EQUAL(thisString, "Cargo Ship")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Cargo Ship - Vector = ") NET_PRINT_VECTOR(<<1259.952759,-2966.432617,41.361275>>) NET_NL()
//			RETURN <<1259.952759,-2966.432617,41.361275>>
//		
//		//Boat Warehouse
//		ELIF ARE_STRINGS_EQUAL(thisString, "Boat Warehouse")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Boat Warehouse - Vector = ") NET_PRINT_VECTOR(<<34.100761,-2759.480225,28.904459>>) NET_NL()
//			RETURN <<34.100761,-2759.480225,28.904459>>
//			
//		//Hobo Town
//		ELIF ARE_STRINGS_EQUAL(thisString, "Hobo Town")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Hobo Town - Vector = ") NET_PRINT_VECTOR( <<1454.620972,6299.306152,35.458946>>) NET_NL()
//			RETURN  <<1454.620972,6299.306152,35.458946>>
//			
//		//Farmhouse
//		ELIF ARE_STRINGS_EQUAL(thisString, "Farmhouse")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Farmhouse - Vector = ") NET_PRINT_VECTOR(<<2434.458252,4984.021484,75.034569>>) NET_NL()
//			RETURN <<2434.458252,4984.021484,75.034569>>
//			
//		ENDIF
//	//ENDIF
//	
//	NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - NONE FOUND - Vector = ") NET_PRINT_VECTOR(<<0, 0, 0>>) NET_NL()
//	RETURN <<0, 0, 0>>
//ENDFUNC

//PURPOSE: (PLACEHOLDER - UNTIL WE CAN DOWNLOAD THE ANGLED AREA) Checks if the a Gang Attack should launch based on an angled area 
//FUNC FLOAT GET_GANG_ATTACK_FLOAT(STRING thisString)
//	
//	//IF iMissionType = FMMC_TYPE_GANGHIDEOUT
//		//Alley
//		IF ARE_STRINGS_EQUAL(thisString, "Alley")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - ALLEY - Float = ") NET_PRINT_FLOAT(50.0) NET_NL()
//			RETURN 50.0
//			
//		//Apartments
//		ELIF ARE_STRINGS_EQUAL(thisString, "Apartments")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - APARTMENTS - Float = ") NET_PRINT_FLOAT(36.0) NET_NL()
//			RETURN 36.0
//			
//		//Hills House
//		ELIF ARE_STRINGS_EQUAL(thisString, "Hills House")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Hills House - Float = ") NET_PRINT_FLOAT(55.0) NET_NL()
//			RETURN 55.0
//			
//		//Trailer Park
//		ELIF ARE_STRINGS_EQUAL(thisString, "Trailer Park")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Trailer Park - Float = ") NET_PRINT_FLOAT(110.0) NET_NL()
//			RETURN 110.0
//			
//		//Hangar
//		ELIF ARE_STRINGS_EQUAL(thisString, "Hangar")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Hangar - Float = ") NET_PRINT_FLOAT(108.0) NET_NL()
//			RETURN 108.0
//			
//		//Plane Junkyard
//		ELIF ARE_STRINGS_EQUAL(thisString, "Plane Junkyard")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Plane Junkyard - Float = ") NET_PRINT_FLOAT(134.0) NET_NL()
//			RETURN 134.0
//		
//		//Cargo Ship
//		ELIF ARE_STRINGS_EQUAL(thisString, "Cargo Ship")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Cargo Ship - Float = ") NET_PRINT_FLOAT(185.0) NET_NL()
//			RETURN 185.0
//		
//		//Boat Warehouse
//		ELIF ARE_STRINGS_EQUAL(thisString, "Boat Warehouse")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Boat Warehouse - Float = ") NET_PRINT_FLOAT(56.0) NET_NL()
//			RETURN 56.0
//			
//		//Hobo Town
//		ELIF ARE_STRINGS_EQUAL(thisString, "Hobo Town")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Hobo Town - Float = ") NET_PRINT_FLOAT(200.0) NET_NL()
//			RETURN 200.0
//			
//		//Farmhouse
//		ELIF ARE_STRINGS_EQUAL(thisString, "Farmhouse")
//			NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - Farmhouse - Float = ") NET_PRINT_FLOAT(50.0) NET_NL()
//			RETURN 50.0
//			
//		ENDIF
//	//ENDIF
//	
//	NET_PRINT(" -[]- SHOULD_GANG_ATTACK_LAUNCH - NONE FOUND - Float = ") NET_PRINT_FLOAT(0) NET_NL()
//	RETURN 0.0
//ENDFUNC

//PURPOSE: (PLACEHOLDER - UNTIL WE CAN DOWNLOAD THE ANGLED AREA) Checks if the a Gang Attack should launch based on an angled area 
FUNC VECTOR GET_GANG_ATTACK_BLIP_VECTOR(TEXT_LABEL_31 thisString)

	//IF iMissionType = FMMC_TYPE_GANGHIDEOUT
		//Alley
		IF ARE_STRINGS_EQUAL(thisString, "Alley")
			NET_PRINT(" -[]- GET_GANG_ATTACK_BLIP_VECTOR - ALLEY - Vector = ") NET_PRINT_VECTOR(<<-1118.6079, -1605.0012, 3.3729>>) NET_NL()
			RETURN <<-1118.6079, -1605.0012, 3.3729>>
			
		//Apartments
		ELIF ARE_STRINGS_EQUAL(thisString, "Apartments")
			NET_PRINT(" -[]- GET_GANG_ATTACK_BLIP_VECTOR - APARTMENTS - Vector = ") NET_PRINT_VECTOR(<<-93.8966, -1611.4363, 31.3183>>) NET_NL()
			RETURN <<-93.8966, -1611.4363, 31.3183>>
			
		//Hills House
		ELIF ARE_STRINGS_EQUAL(thisString, "Hills House")
			NET_PRINT(" -[]- GET_GANG_ATTACK_BLIP_VECTOR - Hills House - Vector = ") NET_PRINT_VECTOR(<<-1871.0354, 193.3095, 83.2945>>) NET_NL()
			RETURN <<-1871.0354, 193.3095, 83.2945>>
			
		//Trailer Park
		ELIF ARE_STRINGS_EQUAL(thisString, "Trailer Park")
			NET_PRINT(" -[]- GET_GANG_ATTACK_BLIP_VECTOR - Trailer Park - Vector = ") NET_PRINT_VECTOR(<<66.6058, 3666.5156, 38.7341>>) NET_NL()
			RETURN <<66.6058, 3666.5156, 38.7341>>
						
		//Plane Junkyard
		ELIF ARE_STRINGS_EQUAL(thisString, "Plane Junkyard")
			NET_PRINT(" -[]- GET_GANG_ATTACK_BLIP_VECTOR - Plane Junkyard - Vector = ") NET_PRINT_VECTOR(<<2398.6956, 3113.6729, 47.1536>>) NET_NL()
			RETURN <<2398.6956, 3113.6729, 47.1536>>
		
		//Cargo Ship
		ELIF ARE_STRINGS_EQUAL(thisString, "Cargo Ship")
			NET_PRINT(" -[]- GET_GANG_ATTACK_BLIP_VECTOR - Cargo Ship - Vector = ") NET_PRINT_VECTOR(<<1241.1349, -2969.2356, 8.3193>>) NET_NL()
			RETURN <<1241.1349, -2969.2356, 8.3193>>
		
		//Boat Warehouse
		ELIF ARE_STRINGS_EQUAL(thisString, "Boat Warehouse")
			NET_PRINT(" -[]- GET_GANG_ATTACK_BLIP_VECTOR - Boat Warehouse - Vector = ") NET_PRINT_VECTOR(<<33.1641, -2677.5869, 5.0111>>) NET_NL()
			RETURN <<33.1641, -2677.5869, 5.0111>>
			
		//Hobo Town
		ELIF ARE_STRINGS_EQUAL(thisString, "Hobo Town")
			NET_PRINT(" -[]- GET_GANG_ATTACK_BLIP_VECTOR - Hobo Town - Vector = ") NET_PRINT_VECTOR( <<1475.2285, 6370.1343, 22.6176>>) NET_NL()
			RETURN  <<1475.2285, 6370.1343, 22.6176>>
			
		//Farmhouse
		ELIF ARE_STRINGS_EQUAL(thisString, "Farmhouse")
			NET_PRINT(" -[]- GET_GANG_ATTACK_BLIP_VECTOR - Farmhouse - Vector = ") NET_PRINT_VECTOR(<<2451.7241, 4967.8848, 45.5767>>) NET_NL()
			RETURN <<2451.7241, 4967.8848, 45.5767>>
			
		ENDIF
	//ENDIF
	
	NET_PRINT(" -[]- GET_GANG_ATTACK_BLIP_VECTOR - NONE FOUND - Vector = ") NET_PRINT_VECTOR(<<0, 0, 0>>) NET_NL()
	RETURN <<0, 0, 0>>
ENDFUNC

PROC CLEAR_LAST_JOB_PLAYERS()
	GAMER_HANDLE emptyHandle
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		lastJobPlayers.Gamers[i] = emptyHandle
		lastJobPlayers.GamersNames[i] = ""
		CLEAR_BIT(lastJobPlayers.iStoredPlayersBS,i)
	ENDREPEAT
	PRINTLN("CLEAR_LAST_JOB_PLAYERS() called from ", GET_THIS_SCRIPT_NAME())
ENDPROC

//PROC STORE_CURRENT_PLAYERS_TO_LAST_JOB_PLAYERS()
//	INT i
//	INT iCounter
//	CLEAR_LAST_JOB_PLAYERS()
//	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() i
//		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(i))
//			IF NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)) != PLAYER_ID()
//				lastJobPlayers.Gamers[iCounter] = GET_GAMER_HANDLE_PLAYER(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
//				lastJobPlayers.GamersNames[iCounter] = GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(i)))
//				PRINTLN("STORE_CURRENT_PLAYERS_TO_LAST_JOB_PLAYERS: lastJobPlayers.Gamers[",iCounter,"] = ",lastJobPlayers.GamersNames[iCounter] )
//				iCounter++
//			ENDIF
//		ENDIF
//	ENDREPEAT
//	PRINTLN("STORE_CURRENT_PLAYERS_TO_LAST_JOB_PLAYERS() called from ", GET_THIS_SCRIPT_NAME())
//ENDPROC

PROC MAINTAIN_LAST_JOB_PLAYERS(PLAYER_INDEX playerID)
	IF NOT IS_PLAYER_IN_CORONA()
		IF NOT IS_BIT_SET(lastJobPlayers.iStoredPlayersBS, native_to_int(playerID))
			IF Get_UniqueID_For_This_Players_Mission(playerID) != NO_UNIQUE_ID 
				IF Get_UniqueID_For_This_Players_Mission(playerID) = Get_UniqueID_For_This_Players_Mission(PLAYER_ID())
					
					GAMER_HANDLE currentHandle = GET_GAMER_HANDLE_PLAYER(playerID)
					STRING playerName = GET_PLAYER_NAME(playerID)
					INT iEmptySlot = -1
					INT iMissingPlayerSlot = -1
					INT i					
					REPEAT NUM_NETWORK_PLAYERS i
						IF lastJobPlayers.Gamers[i].Data1 = currentHandle.Data1
							IF NETWORK_ARE_HANDLES_THE_SAME(lastJobPlayers.Gamers[i],currentHandle)
								SET_BIT(lastJobPlayers.iStoredPlayersBS, native_to_int(playerID))
								EXIT
							ENDIF
						ELSE
							IF NOT IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(i))
								IF iMissingPlayerSlot = -1
									iMissingPlayerSlot = i
								ENDIF
							ELSE
								IF iEmptySlot = -1
									IF NOT IS_GAMER_HANDLE_VALID(lastJobPlayers.Gamers[i])
										iEmptySlot = i
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					INT iStoreSlot
					BOOL bSlotAssigned
					IF iEmptySlot >= 0
						iStoreSlot = iEmptySlot
						bSlotAssigned = TRUE
						PRINTLN("MAINTAIN_LAST_JOB_PLAYERS: empty slot returned for storing player is #",iStoreSlot)
					ELSE
						IF iMissingPlayerSlot >= 0
							iStoreSlot = iMissingPlayerSlot
							bSlotAssigned = TRUE
							PRINTLN("MAINTAIN_LAST_JOB_PLAYERS: missing player slot returned for storing player is #",iStoreSlot)
						#IF IS_DEBUG_BUILD
						ELSE
							PRINTLN("MAINTAIN_LAST_JOB_PLAYERS: trying to add player but no empty or missing slot? See Conor")
							SCRIPT_ASSERT("MAINTAIN_LAST_JOB_PLAYERS: trying to add player but no empty or missing slot? See Conor")
						#ENDIF
						ENDIF
					ENDIF
					PRINTLN("MAINTAIN_LAST_JOB_PLAYERS: slot returned for storing player is #",iStoreSlot)
					IF bSlotAssigned
						lastJobPlayers.Gamers[iStoreSlot] = currentHandle
						lastJobPlayers.GamersNames[iStoreSlot] = playerName
						PRINTLN("MAINTAIN_LAST_JOB_PLAYERS: storing player in slot #",iStoreSlot," name = ",lastJobPlayers.GamersNames[iStoreSlot] )
						SET_BIT(lastJobPlayers.iStoredPlayersBS, native_to_int(playerID))
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR GET_SAFE_WARP_COORD_IN_CIRCLE(INT iPlayerIDToInt,VECTOR vCentre, FLOAT fRadius = 1.90 #IF IS_DEBUG_BUILD, BOOL bDebugOutput = TRUE#ENDIF)
	VECTOR vPlayerPosition
	
	FLOAT fShellDistance = fRadius/3
	INT iPosPerShell[3]
	iPosPerShell[0] = 5
	iPosPerShell[1] = 10
	iPosPerShell[2] = 17
	
	INT iPlayerShell
	INT iPlayerShellPos
	IF iPlayerIDToInt <= iPosPerShell[0]
		iPlayerShell = 0
		iPlayerShellPos = iPlayerIDToInt
	ELIF iPlayerIDToInt <= iPosPerShell[1]+iPosPerShell[0]
		iPlayerShell = 1
		iPlayerShellPos = iPlayerIDToInt-iPosPerShell[0] 
	ELSE
		iPlayerShell = 2
		iPlayerShellPos = iPlayerIDToInt-iPosPerShell[1]-iPosPerShell[0] 
	ENDIF
	
	FLOAT fOffsetForPosShell[3]
	fOffsetForPosShell[0] = TO_FLOAT(360/iPosPerShell[0])
	fOffsetForPosShell[1] = TO_FLOAT(360/iPosPerShell[1])
	fOffsetForPosShell[2] = TO_FLOAT(360/iPosPerShell[2])
	
	FLOAT fRot = fOffsetForPosShell[iPlayerShell]*iPlayerShellPos
	VECTOR vAfterRot
	FLOAT fHeading
	FLOAT CosAngle
     FLOAT SinAngle
	// Rotation about the z axis 
   	CosAngle = COS(fRot)
    SinAngle = SIN(fRot)
    vAfterRot.x = (CosAngle * 0) - (SinAngle * 1)
    vAfterRot.y = (SinAngle * 0) + (CosAngle * 1)
	fHeading = GET_HEADING_FROM_VECTOR_2D( vAfterRot.x,  vAfterRot.y )
	
	VECTOR vOffset = <<0,fShellDistance*(iPlayerShell+1),0>>
	vPlayerPosition = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vCentre,fHeading,vOffset)
	#IF IS_DEBUG_BUILD
		IF bDebugOutput
			PRINTLN("GET_SAFE_WARP_COORD_IN_CIRCLE Was passed in the vector       - [", vCentre ,"]")
			PRINTLN("GET_SAFE_WARP_COORD_IN_CIRCLE Was passed in the radius - [", fRadius ,"]")
			PRINTLN("GET_SAFE_WARP_COORD_IN_CIRCLE returning the following vector - [", vPlayerPosition ,"]")
		ENDIF
	#ENDIF
	
	RETURN vPlayerPosition
ENDFUNC



// ===========================================================================================================
//      Some Additional Mission Triggering PlayerBD variable checking functions
//		(KGM NOTE: Done in a quick and nasty way - not very consistent - should be considered temp)
// ===========================================================================================================

FUNC BOOL Is_Player_Currently_On_MP_Contact_Mission(PLAYER_INDEX paramPlayerID)
	RETURN (GlobalplayerBD[NATIVE_TO_INT(paramPlayerID)].sMissionExtraMP.satmpSpecialActivityType = SATMP_CONTACT_MISSION)
ENDFUNC

FUNC BOOL Is_Player_Currently_On_MP_Heist(PLAYER_INDEX paramPlayerID)
	
	#IF IS_DEBUG_BUILD
	IF g_bFakeFinale
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN (GlobalplayerBD[NATIVE_TO_INT(paramPlayerID)].sMissionExtraMP.satmpSpecialActivityType = SATMP_HEIST)
	
ENDFUNC

FUNC BOOL Is_Player_Currently_On_MP_Heist_Planning(PLAYER_INDEX paramPlayerID)
	
	#IF IS_DEBUG_BUILD
	IF g_bFakePrep
		RETURN TRUE
	ENDIF
	#ENDIF
	
	RETURN (GlobalplayerBD[NATIVE_TO_INT(paramPlayerID)].sMissionExtraMP.satmpSpecialActivityType = SATMP_HEIST_PLANNING)
	
ENDFUNC

FUNC BOOL IS_HEIST_QUICK_RESTART()

	IF Is_Player_Currently_On_MP_Heist(PLAYER_ID())
	OR Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID())	
		IF IS_THIS_A_QUICK_RESTART_JOB()
		OR IS_CORONA_INITIALISING_A_QUICK_RESTART()
		
			RETURN TRUE
		ENDIF
	ENDIF
			
	RETURN FALSE
ENDFUNC

FUNC BOOL RESTART_INTRO_IS_UNARMED(INT iTeam)
	RETURN (g_FMMC_STRUCT.iQuickRestartAnim[iTeam] = ciQUICK_RESTART_ANIM_UNARMED)
ENDFUNC

FUNC BOOL RESTART_INTRO_IS_ARMED(INT iTeam)
	RETURN (g_FMMC_STRUCT.iQuickRestartAnim[iTeam] = ciQUICK_RESTART_ANIM_ARMED)
ENDFUNC

FUNC BOOL SHOULD_RESTART_HAVE_INTRO_ANIM(INT iTeam)
	IF RESTART_INTRO_IS_UNARMED(iTeam)
	OR RESTART_INTRO_IS_ARMED(iTeam)
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_RESTART_HAVE_APARTMENT_ANIM(INT iTeam)
	RETURN (g_FMMC_STRUCT.iQuickRestartAnim[iTeam] = ciQUICK_RESTART_ANIM_APARTMENT)
ENDFUNC

FUNC BOOL Is_Player_Currently_On_MP_Random_Event(PLAYER_INDEX paramPlayerID)
	RETURN (GlobalplayerBD[NATIVE_TO_INT(paramPlayerID)].sMissionExtraMP.satmpSpecialActivityType = SATMP_RANDOM_EVENT)
ENDFUNC

FUNC BOOL Is_Player_Currently_On_MP_Coop_Mission(PLAYER_INDEX paramPlayerID)
	RETURN (GlobalplayerBD[NATIVE_TO_INT(paramPlayerID)].sMissionExtraMP.satmpSpecialActivityType = SATMP_COOP_MISSION)
ENDFUNC

FUNC BOOL Is_Player_Currently_On_MP_Versus_Mission(PLAYER_INDEX paramPlayerID)
	RETURN (GlobalplayerBD[NATIVE_TO_INT(paramPlayerID)].sMissionExtraMP.satmpSpecialActivityType = SATMP_VERSUS_MISSION)
ENDFUNC

FUNC BOOL Is_Player_Currently_On_MP_LTS_Mission(PLAYER_INDEX paramPlayerID)
	RETURN (GlobalplayerBD[NATIVE_TO_INT(paramPlayerID)].sMissionExtraMP.satmpSpecialActivityType = SATMP_LTS_MISSION)
ENDFUNC

FUNC BOOL Is_Player_Currently_On_MP_CTF_Mission(PLAYER_INDEX paramPlayerID)
	RETURN (GlobalplayerBD[NATIVE_TO_INT(paramPlayerID)].sMissionExtraMP.satmpSpecialActivityType = SATMP_CTF_MISSION)
ENDFUNC

FUNC BOOL IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY()
	#IF IS_DEBUG_BUILD
		//PRINTLN("IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY g_FMMC_STRUCT.iAdversaryModeType = ", g_FMMC_STRUCT.iAdversaryModeType)
	#ENDIF
	
	IF g_FMMC_STRUCT.iMissionType = FMMC_TYPE_MISSION
	AND NETWORK_IS_ACTIVITY_SESSION()
	AND (g_FMMC_STRUCT.iMissionTypeBitSetTwo != 0
	OR g_FMMC_STRUCT.iAdversaryModeType > 0
	OR IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_SiegeMentality)
	OR IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HastaLaVista)
	OR IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_ComeOutToPlay)
	OR IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_CrossTheLine)
	OR IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSet, ciROCKSTAR_CREATED_NEW_VS_MISSION_HuntingPack)	
	OR IS_BIT_SET(g_FMMC_STRUCT.iMissionTypeBitSetTwo, ciROCKSTAR_CREATED_2_NEW_VS_SUMO_ZONE))	
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_INDEX paramPlayerID)
	IF Is_Player_Currently_On_MP_Heist(paramPlayerID) 
	OR Is_Player_Currently_On_MP_Heist_Planning(paramPlayerID)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_PLAYER_HEIST_TEAM()	
	INT iTeam	
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		iTeam = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen
	ELSE
		iTeam = g_FMMC_STRUCT.iTestMyTeam
	ENDIF	
	IF iTeam < 0
		NET_PRINT(" GET_PRE_ASSIGNMENT_PLAYER_TEAM - TEAM IS COMING THROUGH AS LESS THAN 0, ADD A BUG FOR ROBERT WRIGHT")
		iTeam = 0
	ENDIF	
	IF IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
	AND iTeam >=FMMC_MAX_TEAMS
		NET_PRINT(" GET_PRE_ASSIGNMENT_PLAYER_TEAM - TEAM IS COMING THROUGH MORE THAN FMMC_MAX_TEAMS, ADD A BUG FOR ROBERT WRIGHT")
		iTeam = 3
	ENDIF	
	RETURN iTeam
ENDFUNC



// ===========================================================================================================
//      Some Additional local player only variables to check while on mission
// ===========================================================================================================

// PURPOSE:	KGM 8/10/13: IF this is a Contact Mission's First Play then give full cash, otherwise give half cash
// NOTES:	A check should be made before calling this to ensure the player is on a Contact Mission
FUNC BOOL Is_This_Contact_Mission_First_Play()

	IF NOT (Is_Player_Currently_On_MP_Contact_Mission(PLAYER_ID()))
		RETURN FALSE
	ENDIF
	
	RETURN (g_cpcmPlaying.cpcmFirstPlay)
	
ENDFUNC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	KGM 13/4/14: FM_Mission_Controler calls this when Full Cash for First Play given
// NOTES:	BUG 1675416: This allows me to detect that I should still allow Full Cash to be given if the first play failed and no cash was given.
PROC Set_Contact_Mission_First_Play_Full_Cash_Given()
	g_cpcmMostRecentFirstPlayFullCashGiven = TRUE
	PRINTLN(".KGM [ActSelect][ContactMission]: FIRST PLAY Full Cash given. Stored CM RootContentID Hash: ", g_cpcmHashMostRecentFirstPlayContentID)
ENDPROC

FUNC TEXT_LABEL_63 CASINO_HEIST_GET_HEIST_MISSION_NAME()

	TEXT_LABEL_63 tl63MissionName = GET_FILENAME_FOR_AUDIO_CONVERSATION("FMMC_CH_DN") //The Diamond Casino Heist
	
	// Casino Heist – Silent & Sneaky
	IF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__STEALTH
		tl63MissionName = GET_FILENAME_FOR_AUDIO_CONVERSATION("FMMC_CH_SS")
		PRINTLN("IS_THIS_CASINO_HEIST_MISSION_A_STEALTH_APPROACH_MISSION - g_FMMC_STRUCT.tl63MissionName = ", tl63MissionName)
		
	// Casino Heist – The Big Con
	ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
		tl63MissionName = GET_FILENAME_FOR_AUDIO_CONVERSATION("FMMC_CH_BC") 
		PRINTLN("IS_THIS_CASINO_HEIST_MISSION_A_SUBTERFUGE_APPROACH_MISSION - g_FMMC_STRUCT.tl63MissionName = ", tl63MissionName)
	
	// Casino Heist – Aggressive	
	ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__DIRECT
		tl63MissionName = GET_FILENAME_FOR_AUDIO_CONVERSATION("FMMC_CH_AG")
		PRINTLN("IS_THIS_CASINO_HEIST_MISSION_A_DIRECT_APPROACH_MISSION - g_FMMC_STRUCT.tl63MissionName = ", tl63MissionName)
	ENDIF
	
	RETURN tl63MissionName
ENDFUNC

FUNC STRING CASINO_HEIST_GET_HEIST_MISSION_DESCRIPTION()
	
	// Casino Heist – Silent & Sneaky
	IF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__STEALTH
		PRINTLN("CASINO_HEIST_GET_HEIST_MISSION_DESCRIPTION - FMMC_CH_SSD = ", GET_FILENAME_FOR_AUDIO_CONVERSATION("FMMC_CH_SSD"))
		RETURN "FMMC_CH_SSD"
		
	// Casino Heist – The Big Con
	ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__SUBTERFUGE
		PRINTLN("CASINO_HEIST_GET_HEIST_MISSION_DESCRIPTION - FMMC_CH_BCD = ", GET_FILENAME_FOR_AUDIO_CONVERSATION("FMMC_CH_BCD"))
		RETURN "FMMC_CH_BCD"
		
	// Casino Heist – Aggressive	
	ELIF g_sCasinoHeistMissionConfigData.eChosenApproachType = CASINO_HEIST_APPROACH_TYPE__DIRECT
		PRINTLN("CASINO_HEIST_GET_HEIST_MISSION_DESCRIPTION - FMMC_CH_AGD = ", GET_FILENAME_FOR_AUDIO_CONVERSATION("FMMC_CH_AGD"))
		RETURN "FMMC_CH_AGD"
	ENDIF
	
	RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION("FMMC_CH_DN")
ENDFUNC

FUNC BOOL SHOULD_SET_CASINO_HEIST_MISSION_NAME()

	IF NETWORK_IS_ACTIVITY_SESSION()
		IF IS_THIS_CASINO_HEIST_MISSION_A_STEALTH_APPROACH_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		OR IS_THIS_CASINO_HEIST_MISSION_A_SUBTERFUGE_APPROACH_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		OR IS_THIS_CASINO_HEIST_MISSION_A_DIRECT_APPROACH_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC TEXT_LABEL_63 GET_ISLAND_HEIST_MISSION_NAME()
	TEXT_LABEL_63 tl63MissionName = GET_FILENAME_FOR_AUDIO_CONVERSATION("IH_TITLE")
	RETURN tl63MissionName
ENDFUNC

FUNC STRING GET_ISLAND_HEIST_MISSION_DESCRIPTION()
	RETURN "IH_DESCR"
ENDFUNC

FUNC BOOL SHOULD_SET_ISLAND_HEIST_MISSION_NAME()
	IF IS_THIS_MISSION_A_HEIST_ISLAND_MISSION(g_FMMC_STRUCT.iRootContentIDHash)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
