//////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_synced_ambient_pickups_private.sch									//
// Description: Handles creation of synchronised ambient pickups.						//
// Written by:  David Trenholme															//
// Date:        12/01/2021																//
//////////////////////////////////////////////////////////////////////////////////////////

//----------------------
//	INCLUDES
//----------------------
USING "rage_builtins.sch"
USING "globals.sch"

USING "net_events.sch"
USING "net_include.sch"
USING "mp_globals_fm.sch"

USING "net_synced_ambient_pickups.sch"

//----------------------
//	ENUMS
//----------------------
#IF IS_DEBUG_BUILD
ENUM SAP_PICKUP_LOCAL_DEBUG_BITSET
	eSAPPICKUPLOCALDEBUGBITSET_ENABLED_DEBUG_LINES_AND_SPHERES,
	
	eSAPPICKUPLOCALDEBUGBITSET_END
ENDENUM
CONST_INT	SAP_PICKUP_LOCAL_DEBUG_BITSET_ARRAY_SIZE	(COUNT_OF(SAP_PICKUP_LOCAL_DEBUG_BITSET) + 30) / 32
#ENDIF

//----------------------
//	STRUCTS
//----------------------
#IF IS_DEBUG_BUILD
STRUCT SAP_DEBUG_VARS_STRUCT
	INT iBitSet[SAP_PICKUP_LOCAL_DEBUG_BITSET_ARRAY_SIZE]
	
	BOOL bEnableDebug
	
	TEXT_WIDGET_ID twIdPlayerName
	TEXT_WIDGET_ID twIdHostPlayerName
	BOOL bRequestToBeHost
	
	VECTOR vLocation
	BOOL bUseGetScriptMousePointer
	BOOL bUseGetSafePickupCoords = TRUE
	INT iAmount = 500
	BOOL bSnapToGround
	BOOL bCreateAsScriptObject
	BOOL bReset
	
	BOOL bCreateSyncedAmbientPickup
	BOOL bDrawDebug
	
	BOOL bCreateAmbientPickup
	BOOL bCreateNonNetworkedAmbientPickup
	BOOL bClearArea
	BOOL bClearAreaBroadcast
	
	INT iSelectedPickup
	BOOL bIsScriptObject
	BOOL bCleanupPickup
	BOOL bDeletePickup
	BOOL bSetPickupCoords
ENDSTRUCT
#ENDIF

STRUCT SAP_VARS_STRUCT
	#IF IS_DEBUG_BUILD
	SAP_DEBUG_VARS_STRUCT sDebug
	#ENDIF
	
	INT iPickupEntityExistsServerBitSet[(SAP_MAX_SYNCED_AMBIENT_PICKUPS + 31) / 32]
	INT iPlayerStaggeredServer
	
	INT iPickupStaggered
ENDSTRUCT

//----------------------
//	FUNCTIONS
//----------------------
#IF IS_DEBUG_BUILD
FUNC STRING SAP_GET_PICKUP_LOCAL_DEBUG_BITSET_NAME(SAP_PICKUP_LOCAL_DEBUG_BITSET eBit)
	SWITCH eBit
		CASE eSAPPICKUPLOCALDEBUGBITSET_ENABLED_DEBUG_LINES_AND_SPHERES	RETURN "eSAPPICKUPLOCALDEBUGBITSET_ENABLED_DEBUG_LINES_AND_SPHERES"
		
		CASE eSAPPICKUPLOCALDEBUGBITSET_END								RETURN "eSAPPICKUPLOCALDEBUGBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING SAP_GET_PICKUP_CLIENT_BITSET_NAME(SAP_PICKUP_CLIENT_BITSET eBit)
	SWITCH eBit
		CASE eSAPPICKUPCLIENTBITSET_NOT_DOES_ENTITY_EXIST	RETURN "eSAPPICKUPCLIENTBITSET_NOT_DOES_ENTITY_EXIST"
		
		CASE eSAPPICKUPCLIENTBITSET_END						RETURN "eSAPPICKUPCLIENTBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC STRING SAP_GET_PICKUP_SERVER_BITSET_NAME(SAP_PICKUP_SERVER_BITSET eBit)
	SWITCH eBit
		CASE eSAPPICKUPSERVERBITSET_NOT_DOES_ENTITY_EXIST	RETURN "eSAPPICKUPSERVERBITSET_NOT_DOES_ENTITY_EXIST"
		
		CASE eSAPPICKUPSERVERBITSET_END						RETURN "eSAPPICKUPSERVERBITSET_END"
	ENDSWITCH
	
	RETURN "***INVALID***"
ENDFUNC

FUNC BOOL SAP_IS_PICKUP_LOCAL_DEBUG_BIT_SET(SAP_VARS_STRUCT& sSAPVarsStruct, SAP_PICKUP_LOCAL_DEBUG_BITSET eBit)
	RETURN IS_ARRAYED_BIT_ENUM_SET(sSAPVarsStruct.sDebug.iBitSet, eBit)
ENDFUNC

PROC SAP_SET_PICKUP_LOCAL_DEBUG_BIT(SAP_VARS_STRUCT& sSAPVarsStruct, SAP_PICKUP_LOCAL_DEBUG_BITSET eBit)
	#IF IS_DEBUG_BUILD
	IF NOT SAP_IS_PICKUP_LOCAL_DEBUG_BIT_SET(sSAPVarsStruct, eBit)
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] [DEBUG] SAP_SET_PICKUP_LOCAL_DEBUG_BIT - ", SAP_GET_PICKUP_LOCAL_DEBUG_BITSET_NAME(eBit))
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	SET_ARRAYED_BIT_ENUM(sSAPVarsStruct.sDebug.iBitSet, eBit)
ENDPROC

PROC SAP_CLEAR_PICKUP_LOCAL_DEBUG_BIT(SAP_VARS_STRUCT& sSAPVarsStruct, SAP_PICKUP_LOCAL_DEBUG_BITSET eBit)
	#IF IS_DEBUG_BUILD
	IF SAP_IS_PICKUP_LOCAL_DEBUG_BIT_SET(sSAPVarsStruct, eBit)
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] [DEBUG] SAP_CLEAR_PICKUP_LOCAL_DEBUG_BIT - ", SAP_GET_PICKUP_LOCAL_DEBUG_BITSET_NAME(eBit))
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	CLEAR_ARRAYED_BIT_ENUM(sSAPVarsStruct.sDebug.iBitSet, eBit)
ENDPROC
#ENDIF

FUNC BOOL SAP_IS_PICKUP_CLIENT_BIT_SET(INT iPickup, PLAYER_INDEX playerID, SAP_PICKUP_CLIENT_BITSET eBit)
	RETURN IS_ARRAYED_BIT_ENUM_SET(GlobalplayerBD_FM_4[NATIVE_TO_INT(playerID)].sSAPClient.sPickups[iPickup].iBitSet, eBit)
ENDFUNC
FUNC BOOL SAP_IS_PICKUP_CLIENT_BIT_SET_INT(INT iPickup, INT iPlayerID, SAP_PICKUP_CLIENT_BITSET eBit)
	RETURN IS_ARRAYED_BIT_ENUM_SET(GlobalplayerBD_FM_4[iPlayerID].sSAPClient.sPickups[iPickup].iBitSet, eBit)
ENDFUNC

PROC SAP_SET_PICKUP_CLIENT_BIT(INT iPickup, SAP_PICKUP_CLIENT_BITSET eBit)
	#IF IS_DEBUG_BUILD
	IF NOT SAP_IS_PICKUP_CLIENT_BIT_SET(iPickup, PLAYER_ID(), eBit)
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_SET_PICKUP_CLIENT_BIT - iPickup = ", iPickup, ", ", SAP_GET_PICKUP_CLIENT_BITSET_NAME(eBit))
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	SET_ARRAYED_BIT_ENUM(GlobalplayerBD_FM_4[NETWORK_PLAYER_ID_TO_INT()].sSAPClient.sPickups[iPickup].iBitSet, eBit)
ENDPROC

PROC SAP_CLEAR_PICKUP_CLIENT_BIT(INT iPickup, SAP_PICKUP_CLIENT_BITSET eBit)
	#IF IS_DEBUG_BUILD
	IF SAP_IS_PICKUP_CLIENT_BIT_SET(iPickup, PLAYER_ID(), eBit)
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_CLEAR_PICKUP_CLIENT_BIT - iPickup = ", iPickup, ", ", SAP_GET_PICKUP_CLIENT_BITSET_NAME(eBit))
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	CLEAR_ARRAYED_BIT_ENUM(GlobalplayerBD_FM_4[NETWORK_PLAYER_ID_TO_INT()].sSAPClient.sPickups[iPickup].iBitSet, eBit)
ENDPROC

FUNC BOOL SAP_IS_PICKUP_SERVER_BIT_SET(INT iPickup, SAP_PICKUP_SERVER_BITSET eBit)
	RETURN IS_ARRAYED_BIT_ENUM_SET(GlobalServerBD_BlockC.sSAPServer.sPickups[iPickup].iBitSet, eBit)
ENDFUNC

PROC SAP_SET_PICKUP_SERVER_BIT(INT iPickup, SAP_PICKUP_SERVER_BITSET eBit)
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ASSERTLN("[SYNCED_AMBIENT_PICKUPS] SAP_SET_PICKUP_SERVER_BIT - Client attempting to set a server bit.")
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT SAP_IS_PICKUP_SERVER_BIT_SET(iPickup, eBit)
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_SET_PICKUP_SERVER_BIT - iPickup = ", iPickup, ", ", SAP_GET_PICKUP_SERVER_BITSET_NAME(eBit))
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	SET_ARRAYED_BIT_ENUM(GlobalServerBD_BlockC.sSAPServer.sPickups[iPickup].iBitSet, eBit)
ENDPROC

PROC SAP_CLEAR_PICKUP_SERVER_BIT(INT iPickup, SAP_PICKUP_SERVER_BITSET eBit)
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ASSERTLN("[SYNCED_AMBIENT_PICKUPS] SAP_CLEAR_PICKUP_SERVER_BIT - Client attempting to clear a server bit.")
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF SAP_IS_PICKUP_SERVER_BIT_SET(iPickup, eBit)
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_CLEAR_PICKUP_SERVER_BIT - iPickup = ", iPickup, ", ", SAP_GET_PICKUP_SERVER_BITSET_NAME(eBit))
		DEBUG_PRINTCALLSTACK()
	ENDIF
	#ENDIF
	
	CLEAR_ARRAYED_BIT_ENUM(GlobalServerBD_BlockC.sSAPServer.sPickups[iPickup].iBitSet, eBit)
ENDPROC

#IF IS_DEBUG_BUILD
PROC SAP_DEBUG_CREATE_WIDGETS(SAP_VARS_STRUCT& sSAPVarsStruct)
	START_WIDGET_GROUP("Synced Ambient Pickups")
		ADD_WIDGET_BOOL("g_sMPTunables.bEnableSyncedAmbientPickups", g_sMPTunables.bEnableSyncedAmbientPickups)
		
		ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
		
		ADD_WIDGET_BOOL("Enable debug", sSAPVarsStruct.sDebug.bEnableDebug)
		
		ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
		
		sSAPVarsStruct.sDebug.twIdPlayerName = ADD_TEXT_WIDGET("Player name")
		sSAPVarsStruct.sDebug.twIdHostPlayerName = ADD_TEXT_WIDGET("Host player name")
		ADD_WIDGET_BOOL("Request to be host", sSAPVarsStruct.sDebug.bRequestToBeHost)
		
		ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
		
		ADD_WIDGET_VECTOR_SLIDER("Location", sSAPVarsStruct.sDebug.vLocation, -10000.0, 10000.0, 0.01)
		ADD_WIDGET_BOOL("Use GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS", sSAPVarsStruct.sDebug.bUseGetScriptMousePointer)
		ADD_WIDGET_BOOL("Use GET_SAFE_PICKUP_COORDS", sSAPVarsStruct.sDebug.bUseGetSafePickupCoords)
		ADD_WIDGET_INT_SLIDER("Amount", sSAPVarsStruct.sDebug.iAmount, 1, 1000, 1)
		ADD_WIDGET_BOOL("Snap to ground", sSAPVarsStruct.sDebug.bSnapToGround)
		ADD_WIDGET_BOOL("Create as script object", sSAPVarsStruct.sDebug.bCreateAsScriptObject)
		ADD_WIDGET_BOOL("Reset", sSAPVarsStruct.sDebug.bReset)
		
		ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
		
		ADD_WIDGET_BOOL("Create synced ambient pickup", sSAPVarsStruct.sDebug.bCreateSyncedAmbientPickup)
		ADD_WIDGET_BOOL("Draw debug", sSAPVarsStruct.sDebug.bDrawDebug)
		
		ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
		
		ADD_WIDGET_BOOL("Create ambient pickup", sSAPVarsStruct.sDebug.bCreateAmbientPickup)
		ADD_WIDGET_BOOL("Create non-networked ambient pickup", sSAPVarsStruct.sDebug.bCreateNonNetworkedAmbientPickup)
		ADD_WIDGET_BOOL("Clear area (local)", sSAPVarsStruct.sDebug.bClearArea)
		ADD_WIDGET_BOOL("Clear area (broadcast)", sSAPVarsStruct.sDebug.bClearAreaBroadcast)
		
		ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
		
		ADD_WIDGET_STRING("Server Data")
		
		ADD_WIDGET_INT_SLIDER("Selected pickup", sSAPVarsStruct.sDebug.iSelectedPickup, 0, SAP_MAX_SYNCED_AMBIENT_PICKUPS - 1, 1)
		ADD_WIDGET_BOOL("Is script object?", sSAPVarsStruct.sDebug.bIsScriptObject)
		ADD_WIDGET_BOOL("Cleanup", sSAPVarsStruct.sDebug.bCleanupPickup)
		ADD_WIDGET_BOOL("Delete", sSAPVarsStruct.sDebug.bDeletePickup)
		ADD_WIDGET_BOOL("Set co-ordinates", sSAPVarsStruct.sDebug.bSetPickupCoords)
		
		TEXT_LABEL_15 tl15Label
		
		INT i
		REPEAT SAP_MAX_SYNCED_AMBIENT_PICKUPS i
			tl15Label = "Pickup: "
			tl15Label += i
			
			ADD_WIDGET_STRING(tl15Label)
			ADD_WIDGET_VECTOR_READ_ONLY("Location", GlobalServerBD_BlockC.sSAPServer.sPickups[i].sPickupData.vLocation)
			
			ADD_WIDGET_STRING("--------------------------------------------------------------------------------------")
		ENDREPEAT
	STOP_WIDGET_GROUP()
ENDPROC

PROC SAP_DEBUG_MAINTAIN_DEBUG(SAP_VARS_STRUCT& sSAPVarsStruct)
	IF NOT sSAPVarsStruct.sDebug.bEnableDebug
		EXIT
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(sSAPVarsStruct.sDebug.twIdPlayerName)
		SET_CONTENTS_OF_TEXT_WIDGET(sSAPVarsStruct.sDebug.twIdPlayerName, GET_PLAYER_NAME(PLAYER_ID()))
	ENDIF
	
	IF DOES_TEXT_WIDGET_EXIST(sSAPVarsStruct.sDebug.twIdHostPlayerName)
		PARTICIPANT_INDEX participantID = NETWORK_GET_HOST_OF_THIS_SCRIPT()
		
		IF NATIVE_TO_INT(participantID) > -1
		AND NETWORK_IS_PARTICIPANT_ACTIVE(participantID)
			SET_CONTENTS_OF_TEXT_WIDGET(sSAPVarsStruct.sDebug.twIdHostPlayerName, GET_PLAYER_NAME(NETWORK_GET_PLAYER_INDEX(participantID)))
		ENDIF
	ENDIF
	
	IF sSAPVarsStruct.sDebug.bRequestToBeHost
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] [DEBUG] SAP_DEBUG_MAINTAIN_DEBUG - bRequestToBeHost = TRUE.")
		sSAPVarsStruct.sDebug.bRequestToBeHost = FALSE
		
		NETWORK_REQUEST_TO_BE_HOST_OF_THIS_SCRIPT()
	ENDIF
	
	IF sSAPVarsStruct.sDebug.bUseGetScriptMousePointer
	AND NOT IS_VECTOR_ZERO(GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS())
		sSAPVarsStruct.sDebug.vLocation = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
	ENDIF
	
	IF sSAPVarsStruct.sDebug.bReset
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] [DEBUG] SAP_DEBUG_MAINTAIN_DEBUG - bReset = TRUE.")
		sSAPVarsStruct.sDebug.bReset = FALSE
		
		sSAPVarsStruct.sDebug.vLocation = <<0.0, 0.0, 0.0>>
		sSAPVarsStruct.sDebug.bUseGetScriptMousePointer = FALSE
		sSAPVarsStruct.sDebug.bUseGetSafePickupCoords = TRUE
		sSAPVarsStruct.sDebug.iAmount = 500
		sSAPVarsStruct.sDebug.bSnapToGround = FALSE
		sSAPVarsStruct.sDebug.bCreateAsScriptObject = FALSE
	ENDIF
	
	IF sSAPVarsStruct.sDebug.bCreateSyncedAmbientPickup
	OR sSAPVarsStruct.sDebug.bCreateAmbientPickup
	OR sSAPVarsStruct.sDebug.bCreateNonNetworkedAmbientPickup
		INT iPlacementFlags
		
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ROTATE))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
		
		IF sSAPVarsStruct.sDebug.bSnapToGround
			SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		ENDIF
		
		IF IS_VECTOR_ZERO(sSAPVarsStruct.sDebug.vLocation)
			sSAPVarsStruct.sDebug.vLocation = GET_PLAYER_COORDS(PLAYER_ID())
		ENDIF
		
		IF sSAPVarsStruct.sDebug.bUseGetSafePickupCoords
			sSAPVarsStruct.sDebug.vLocation = GET_SAFE_PICKUP_COORDS(GET_PLAYER_COORDS(PLAYER_ID()), 2.0, 5.0)
		ELIF IS_VECTOR_ZERO(sSAPVarsStruct.sDebug.vLocation)
			sSAPVarsStruct.sDebug.vLocation = GET_PLAYER_COORDS(PLAYER_ID())
		ENDIF
		
		IF sSAPVarsStruct.sDebug.bCreateSyncedAmbientPickup
			PRINTLN("[SYNCED_AMBIENT_PICKUPS] [DEBUG] SAP_DEBUG_MAINTAIN_DEBUG - bCreateSyncedAmbientPickup = TRUE.")
			
			CREATE_SYNCED_AMBIENT_PICKUP(PICKUP_MONEY_VARIABLE, sSAPVarsStruct.sDebug.vLocation, iPlacementFlags, sSAPVarsStruct.sDebug.iAmount, DEFAULT, sSAPVarsStruct.sDebug.bCreateAsScriptObject)
		ELIF sSAPVarsStruct.sDebug.bCreateAmbientPickup
			PRINTLN("[SYNCED_AMBIENT_PICKUPS] [DEBUG] SAP_DEBUG_MAINTAIN_DEBUG - bCreateAmbientPickup = TRUE.")
			
			CREATE_AMBIENT_PICKUP(PICKUP_MONEY_VARIABLE, sSAPVarsStruct.sDebug.vLocation, iPlacementFlags, sSAPVarsStruct.sDebug.iAmount, DEFAULT, DEFAULT, FALSE)
		ELIF sSAPVarsStruct.sDebug.bCreateNonNetworkedAmbientPickup
			PRINTLN("[SYNCED_AMBIENT_PICKUPS] [DEBUG] SAP_DEBUG_MAINTAIN_DEBUG - bCreateNonNetworkedAmbientPickup = TRUE.")
			
			CREATE_NON_NETWORKED_AMBIENT_PICKUP(PICKUP_MONEY_VARIABLE, sSAPVarsStruct.sDebug.vLocation, iPlacementFlags, sSAPVarsStruct.sDebug.iAmount, DEFAULT, DEFAULT, FALSE)
		ENDIF
		
		sSAPVarsStruct.sDebug.bCreateSyncedAmbientPickup = FALSE
		sSAPVarsStruct.sDebug.bCreateAmbientPickup = FALSE
		sSAPVarsStruct.sDebug.bCreateNonNetworkedAmbientPickup = FALSE
	ENDIF
	
	IF sSAPVarsStruct.sDebug.bClearArea
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] [DEBUG] SAP_DEBUG_MAINTAIN_DEBUG - bClearArea = TRUE.")
		sSAPVarsStruct.sDebug.bClearArea = FALSE
		
		IF IS_VECTOR_ZERO(sSAPVarsStruct.sDebug.vLocation)
			sSAPVarsStruct.sDebug.vLocation = GET_PLAYER_COORDS(PLAYER_ID())
		ENDIF
		
		CLEAR_AREA(sSAPVarsStruct.sDebug.vLocation, 25.0, TRUE)
	ENDIF
	
	IF sSAPVarsStruct.sDebug.bClearAreaBroadcast
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] [DEBUG] SAP_DEBUG_MAINTAIN_DEBUG - bClearAreaBroadcast = TRUE.")
		sSAPVarsStruct.sDebug.bClearAreaBroadcast = FALSE
		
		IF IS_VECTOR_ZERO(sSAPVarsStruct.sDebug.vLocation)
			sSAPVarsStruct.sDebug.vLocation = GET_PLAYER_COORDS(PLAYER_ID())
		ENDIF
		
		CLEAR_AREA(sSAPVarsStruct.sDebug.vLocation, 25.0, TRUE, DEFAULT, DEFAULT, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sSAPPickups[sSAPVarsStruct.sDebug.iSelectedPickup].oiPickup)
		sSAPVarsStruct.sDebug.bIsScriptObject = IS_ENTITY_A_MISSION_ENTITY(g_sSAPPickups[sSAPVarsStruct.sDebug.iSelectedPickup].oiPickup)
	ELSE
		sSAPVarsStruct.sDebug.bIsScriptObject = FALSE
	ENDIF
	
	IF sSAPVarsStruct.sDebug.bCleanupPickup
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] [DEBUG] SAP_DEBUG_MAINTAIN_DEBUG - bCleanupPickup = TRUE.")
		sSAPVarsStruct.sDebug.bCleanupPickup = FALSE
		
		IF IS_SYNCED_AMBIENT_PICKUP_ID_VALID(GlobalServerBD_BlockC.sSAPServer.sPickups[sSAPVarsStruct.sDebug.iSelectedPickup].sPickupData.sID)
			CLEANUP_SYNCED_AMBIENT_PICKUP(GlobalServerBD_BlockC.sSAPServer.sPickups[sSAPVarsStruct.sDebug.iSelectedPickup].sPickupData.sID)
		ENDIF
	ENDIF
	
	IF sSAPVarsStruct.sDebug.bDeletePickup
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] [DEBUG] SAP_DEBUG_MAINTAIN_DEBUG - bDeletePickup = TRUE.")
		sSAPVarsStruct.sDebug.bDeletePickup = FALSE
		
		IF IS_SYNCED_AMBIENT_PICKUP_ID_VALID(GlobalServerBD_BlockC.sSAPServer.sPickups[sSAPVarsStruct.sDebug.iSelectedPickup].sPickupData.sID)
			DELETE_SYNCED_AMBIENT_PICKUP(GlobalServerBD_BlockC.sSAPServer.sPickups[sSAPVarsStruct.sDebug.iSelectedPickup].sPickupData.sID)
		ENDIF
	ENDIF
	
	IF sSAPVarsStruct.sDebug.bSetPickupCoords
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] [DEBUG] SAP_DEBUG_MAINTAIN_DEBUG - bSetPickupCoords = TRUE.")
		sSAPVarsStruct.sDebug.bSetPickupCoords = FALSE
		
		IF IS_SYNCED_AMBIENT_PICKUP_ID_VALID(GlobalServerBD_BlockC.sSAPServer.sPickups[sSAPVarsStruct.sDebug.iSelectedPickup].sPickupData.sID)
			SET_SYNCED_AMBIENT_PICKUP_COORDS(GlobalServerBD_BlockC.sSAPServer.sPickups[sSAPVarsStruct.sDebug.iSelectedPickup].sPickupData.sID, sSAPVarsStruct.sDebug.vLocation)
		ENDIF
	ENDIF
	
	TEXT_LABEL_15 tl15Label
	
	INT i
	REPEAT SAP_MAX_SYNCED_AMBIENT_PICKUPS i
		IF sSAPVarsStruct.sDebug.bDrawDebug
		AND IS_SYNCED_AMBIENT_PICKUP_ID_VALID(g_sSAPPickups[i].sID)
			IF DOES_ENTITY_EXIST(g_sSAPPickups[i].oiPickup)
				IF NOT SAP_IS_PICKUP_LOCAL_DEBUG_BIT_SET(sSAPVarsStruct, eSAPPICKUPLOCALDEBUGBITSET_ENABLED_DEBUG_LINES_AND_SPHERES)
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
					SAP_SET_PICKUP_LOCAL_DEBUG_BIT(sSAPVarsStruct, eSAPPICKUPLOCALDEBUGBITSET_ENABLED_DEBUG_LINES_AND_SPHERES)
				ENDIF
				
				tl15Label = "Pickup: "
				tl15Label += i
				DRAW_DEBUG_TEXT_WITH_OFFSET(tl15Label, GET_ENTITY_COORDS(g_sSAPPickups[i].oiPickup), 0, 50)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
#ENDIF

FUNC BOOL SAP_IS_PICKUP_SCRIPT_OBJECT(SAP_PICKUP_DATA& sPickup)
	RETURN sPickup.eBehaviour != eSAPPICKUPBEHAVIOUR_AMBIENT
ENDFUNC

FUNC BOOL SAP_IS_PICKUP_SCRIPT_ACTIVE(SAP_PICKUP_DATA& sPickup)
	SWITCH sPickup.eBehaviour
		CASE eSAPPICKUPBEHAVIOUR_SCRIPT_OBJECT
			RETURN NETWORK_IS_SCRIPT_ACTIVE_BY_HASH(sPickup.iScriptNameHash, sPickup.iInstanceID)
		
		CASE eSAPPICKUPBEHAVIOUR_WORLD_POINT_SCRIPT_OBJECT
			RETURN NETWORK_IS_SCRIPT_ACTIVE_BY_HASH(sPickup.iScriptNameHash, DEFAULT, DEFAULT, sPickup.iInstanceID)
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC SAP_CREATE_PICKUP_LOCAL(INT iPickup, SAP_PICKUP_DATA& sPickup)
	IF NOT REQUEST_LOAD_MODEL(sPickup.eModel)
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_CREATE_PICKUP_LOCAL - Loading custom model for iPickup = ", iPickup, ", eType = ", GET_AMBIENT_PICKUP_NAME(sPickup.sID.eType), ", vInitialLocation = ", sPickup.sID.vInitialLocation)
		EXIT
	ENDIF
	
	PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_CREATE_PICKUP_LOCAL - Creating iPickup = ", iPickup, ", eType = ", GET_AMBIENT_PICKUP_NAME(sPickup.sID.eType), ", vInitialLocation = ", sPickup.sID.vInitialLocation)
	
	SET_BIT(sPickup.iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
	g_sSAPPickups[iPickup].oiPickup = CREATE_NON_NETWORKED_AMBIENT_PICKUP(sPickup.sID.eType, sPickup.vLocation, sPickup.iPlacementFlags, sPickup.iAmount, sPickup.eModel, SAP_IS_PICKUP_SCRIPT_OBJECT(sPickup), FALSE)
	
	IF sPickup.eModel != DUMMY_MODEL_FOR_SCRIPT
		SET_MODEL_AS_NO_LONGER_NEEDED(sPickup.eModel)
	ENDIF
	
	COPY_SCRIPT_STRUCT(g_sSAPPickups[iPickup].sID, sPickup.sID, SIZE_OF(SAP_PICKUP_ID))
ENDPROC

PROC SAP_COPY_PICKUP_LOCAL(INT iPickup, SAP_PICKUP_DATA& sPickup)
	PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_COPY_PICKUP_LOCAL - Copying iPickup = ", iPickup, ", eType = ", GET_AMBIENT_PICKUP_NAME(sPickup.sID.eType), ", vInitialLocation = ", sPickup.sID.vInitialLocation)
	
	//When copying an existing OBJECT_INDEX, it may have been created by another script. If it was created with bCreateAsScriptObject = TRUE it will be a mission entity, so the system should set it as its own mission entity.
	IF DOES_ENTITY_EXIST(sPickup.sID.oiPickup)
	AND IS_ENTITY_A_MISSION_ENTITY(sPickup.sID.oiPickup)
	AND NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(sPickup.sID.oiPickup, FALSE)
		SET_ENTITY_AS_MISSION_ENTITY(sPickup.sID.oiPickup, FALSE, TRUE)
	ENDIF
	
	g_sSAPPickups[iPickup].oiPickup = sPickup.sID.oiPickup
	COPY_SCRIPT_STRUCT(g_sSAPPickups[iPickup].sID, sPickup.sID, SIZE_OF(SAP_PICKUP_ID))
ENDPROC

PROC SAP_DELETE_PICKUP_LOCAL(INT iPickup, BOOL bDeleteData = TRUE)
	PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_DELETE_PICKUP_LOCAL - iPickup = ", iPickup, ", bDeleteData = ", GET_STRING_FROM_BOOL(bDeleteData))
	
	IF DOES_ENTITY_EXIST(g_sSAPPickups[iPickup].oiPickup)
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_DELETE_PICKUP_LOCAL - Deleting local entity.")
		
		IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_sSAPPickups[iPickup].oiPickup, FALSE)
			SET_ENTITY_AS_MISSION_ENTITY(g_sSAPPickups[iPickup].oiPickup, FALSE, TRUE)
		ENDIF
		
		DELETE_OBJECT(g_sSAPPickups[iPickup].oiPickup)
	ENDIF
	
	IF bDeleteData
		RESET_ARRAYED_BIT_SET(GlobalplayerBD_FM_4[NETWORK_PLAYER_ID_TO_INT()].sSAPClient.sPickups[iPickup].iBitSet)
		
		SAP_PICKUP_LOCAL sEmpty
		COPY_SCRIPT_STRUCT(g_sSAPPickups[iPickup], sEmpty, SIZE_OF(SAP_PICKUP_LOCAL))
	ENDIF
ENDPROC

PROC SAP_CLEANUP_PICKUP_LOCAL(INT iPickup)
	PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_CLEANUP_PICKUP_LOCAL - iPickup = ", iPickup)
	
	OBJECT_INDEX oiPickup = g_sSAPPickups[iPickup].oiPickup
	
	IF DOES_ENTITY_EXIST(oiPickup)
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_CLEANUP_PICKUP_LOCAL - Cleaning up local entity.")
		
		IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(oiPickup, FALSE)
			SET_ENTITY_AS_MISSION_ENTITY(oiPickup, FALSE, TRUE)
		ENDIF
		
		//SET_OBJECT_AS_NO_LONGER_NEEDED sets the OBJECT_INDEX to NULL. We still need to refer to the pickup after it reverts to ambient behaviour, so use a copy of the index.
		SET_OBJECT_AS_NO_LONGER_NEEDED(oiPickup)
	ENDIF
ENDPROC

PROC SAP_DELETE_PICKUP_REQUEST_CLIENT(INT iPickupRequest, BOOL bDeleteEntity = TRUE)
	PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_DELETE_PICKUP_REQUEST_CLIENT - iPickupRequest = ", iPickupRequest, ", bDeleteEntity = ", GET_STRING_FROM_BOOL(bDeleteEntity))
	
	IF bDeleteEntity
		OBJECT_INDEX oiPickup = GlobalplayerBD_FM_4[NETWORK_PLAYER_ID_TO_INT()].sSAPClient.sPickupRequests[iPickupRequest].sID.oiPickup
		
		IF DOES_ENTITY_EXIST(oiPickup)
			PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_DELETE_PICKUP_REQUEST_CLIENT - Deleting local entity.")
		
			IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(oiPickup, FALSE)
				SET_ENTITY_AS_MISSION_ENTITY(oiPickup, FALSE, TRUE)
			ENDIF
			
			DELETE_OBJECT(oiPickup)
		ENDIF
	ENDIF
	
	SAP_PICKUP_DATA sEmpty
	COPY_SCRIPT_STRUCT(GlobalplayerBD_FM_4[NETWORK_PLAYER_ID_TO_INT()].sSAPClient.sPickupRequests[iPickupRequest], sEmpty, SIZE_OF(SAP_PICKUP_DATA))
ENDPROC

PROC SAP_CLEANUP_PICKUP_REQUEST_CLIENT(INT iPickupRequest)
	PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_CLEANUP_PICKUP_REQUEST_CLIENT - iPickupRequest = ", iPickupRequest)
	
	OBJECT_INDEX oiPickup = GlobalplayerBD_FM_4[NETWORK_PLAYER_ID_TO_INT()].sSAPClient.sPickupRequests[iPickupRequest].sID.oiPickup
	
	IF DOES_ENTITY_EXIST(oiPickup)
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_DELETE_PICKUP_REQUEST_CLIENT - Cleaning up local entity.")
		
		IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(oiPickup, FALSE)
			SET_ENTITY_AS_MISSION_ENTITY(oiPickup, FALSE, TRUE)
		ENDIF
		
		//SET_OBJECT_AS_NO_LONGER_NEEDED sets the OBJECT_INDEX to NULL. We still need to refer to the pickup after it reverts to ambient behaviour, so use a copy of the index.
		SET_OBJECT_AS_NO_LONGER_NEEDED(oiPickup)
	ENDIF
	
	GlobalplayerBD_FM_4[NETWORK_PLAYER_ID_TO_INT()].sSAPClient.sPickupRequests[iPickupRequest].eBehaviour = eSAPPICKUPBEHAVIOUR_AMBIENT
ENDPROC

PROC SAP_CREATE_PICKUP_SERVER(INT iPickup, SAP_PICKUP_DATA& sPickup)
	PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_CREATE_PICKUP_SERVER - iPickup = ", iPickup, " eType = ", GET_AMBIENT_PICKUP_NAME(sPickup.sID.eType), ", vInitialLocation = ", sPickup.sID.vInitialLocation)
	
	COPY_SCRIPT_STRUCT(GlobalServerBD_BlockC.sSAPServer.sPickups[iPickup].sPickupData, sPickup, SIZE_OF(SAP_PICKUP_DATA))
ENDPROC

PROC SAP_DELETE_PICKUP_SERVER(INT iPickup)
	PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_DELETE_PICKUP_SERVER - iPickup = ", iPickup)
	
	SAP_PICKUP_SERVER sEmpty
	COPY_SCRIPT_STRUCT(GlobalServerBD_BlockC.sSAPServer.sPickups[iPickup], sEmpty, SIZE_OF(SAP_PICKUP_SERVER))
ENDPROC

PROC SAP_CLEANUP_PICKUP_SERVER(INT iPickup)
	PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_CLEANUP_PICKUP_SERVER - iPickup = ", iPickup)
	
	GlobalServerBD_BlockC.sSAPServer.sPickups[iPickup].sPickupData.eBehaviour = eSAPPICKUPBEHAVIOUR_AMBIENT
ENDPROC

PROC SAP_CLEANUP()
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	INT i
	REPEAT SAP_MAX_SYNCED_AMBIENT_PICKUPS i
		IF IS_SYNCED_AMBIENT_PICKUP_ID_VALID(g_sSAPPickups[i].sID)
			SAP_DELETE_PICKUP_LOCAL(i)
		ENDIF
		
		IF IS_SYNCED_AMBIENT_PICKUP_ID_VALID(GlobalplayerBD_FM_4[NETWORK_PLAYER_ID_TO_INT()].sSAPClient.sPickupRequests[i].sID)
			SAP_DELETE_PICKUP_REQUEST_CLIENT(i)
		ENDIF
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		AND IS_SYNCED_AMBIENT_PICKUP_ID_VALID(GlobalServerBD_BlockC.sSAPServer.sPickups[i].sPickupData.sID)
			SAP_DELETE_PICKUP_SERVER(i)
		ENDIF
	ENDREPEAT
ENDPROC

PROC SAP_PROCESS_EVENT_NETWORK_PLAYER_COLLECTED_AMBIENT_PICKUP(STRUCT_AMBIENT_PICKUP_EVENT& sEventData)
	INT i = FIND_SYNCED_AMBIENT_PICKUP_INDEX_BY_OBJECT_INDEX(INT_TO_NATIVE(OBJECT_INDEX, sEventData.PickupIndex))
	
	IF i != -1
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_PROCESS_EVENT_NETWORK_PLAYER_COLLECTED_AMBIENT_PICKUP - Received collected event from ", GET_PLAYER_NAME(sEventData.PlayerIndex), " with PickupType = ", GET_AMBIENT_PICKUP_NAME(sEventData.PickupType), ", PickupAmount = ", sEventData.PickupAmount, ", PickupModel = ", GET_SYNCED_AMBIENT_PICKUP_MODEL_NAME(sEventData.PickupModel), " and PickupIndex = ", sEventData.PickupIndex, " at GET_CLOUD_TIME_AS_INT() = ", GET_CLOUD_TIME_AS_INT())
		
		DELETE_SYNCED_AMBIENT_PICKUP(g_sSAPPickups[i].sID)
	ENDIF
ENDPROC

PROC SAP_PROCESS_SCRIPT_EVENT_CLEANUP_SYNCED_AMBIENT_PICKUP(INT iCount)
	SCRIPT_EVENT_DATA_SYNCED_AMBIENT_PICKUP_ID sEventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventData, SIZE_OF(sEventData))
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_PROCESS_SCRIPT_EVENT_CLEANUP_SYNCED_AMBIENT_PICKUP - Received cleanup event from ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex), " with eType = ", GET_AMBIENT_PICKUP_NAME(sEventData.sPickupID.eType), " and vInitialLocation = ", sEventData.sPickupID.vInitialLocation, " at GET_CLOUD_TIME_AS_INT() = ", GET_CLOUD_TIME_AS_INT())
		
		//If there is a pickup request with a matching ID, clean it up.
		INT i = FIND_SYNCED_AMBIENT_PICKUP_REQUEST_INDEX(sEventData.sPickupID)
		
		IF i != -1
			SAP_CLEANUP_PICKUP_REQUEST_CLIENT(i)
		ENDIF
		
		//If there is a pickup with a matching ID, clean it up.
		i = FIND_SYNCED_AMBIENT_PICKUP_INDEX(sEventData.sPickupID)
		
		IF i != -1
			SAP_CLEANUP_PICKUP_LOCAL(i)
			
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				SAP_CLEANUP_PICKUP_SERVER(i)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAP_PROCESS_SCRIPT_EVENT_DELETE_SYNCED_AMBIENT_PICKUP(INT iCount)
	SCRIPT_EVENT_DATA_SYNCED_AMBIENT_PICKUP_ID sEventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventData, SIZE_OF(sEventData))
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_PROCESS_SCRIPT_EVENT_DELETE_SYNCED_AMBIENT_PICKUP - Received delete event from ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex), " with eType = ", GET_AMBIENT_PICKUP_NAME(sEventData.sPickupID.eType), " and vInitialLocation = ", sEventData.sPickupID.vInitialLocation, " at GET_CLOUD_TIME_AS_INT() = ", GET_CLOUD_TIME_AS_INT())
		
		//If there is a pickup request with a matching ID, delete it.
		INT i = FIND_SYNCED_AMBIENT_PICKUP_REQUEST_INDEX(sEventData.sPickupID)
		
		IF i != -1
			SAP_DELETE_PICKUP_REQUEST_CLIENT(i)
		ENDIF
		
		//If there is a pickup with a matching ID, delete it.
		i = FIND_SYNCED_AMBIENT_PICKUP_INDEX(sEventData.sPickupID)
		
		IF i != -1
			//Delete the local pickup entity. The local data will be deleted once its corresponding server BD data has been deleted.
			SAP_DELETE_PICKUP_LOCAL(i, FALSE)
			
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				SAP_DELETE_PICKUP_SERVER(i)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAP_PROCESS_SCRIPT_EVENT_SET_SYNCED_AMBIENT_PICKUP_COORDS(INT iCount)
	SCRIPT_EVENT_DATA_SYNCED_AMBIENT_PICKUP_COORDS sEventData
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, sEventData, SIZE_OF(sEventData))
		PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_PROCESS_SCRIPT_EVENT_SET_SYNCED_AMBIENT_PICKUP_COORDS - Received set co-ordinates event from ", GET_PLAYER_NAME(sEventData.Details.FromPlayerIndex), " with eType = ", GET_AMBIENT_PICKUP_NAME(sEventData.sPickupID.eType), ", vInitialLocation = ", sEventData.sPickupID.vInitialLocation, " and vCoords = ", sEventData.vCoords, " at GET_CLOUD_TIME_AS_INT() = ", GET_CLOUD_TIME_AS_INT())
		
		//If there is a pickup with a matching ID, update the location.
		INT iPickup = FIND_SYNCED_AMBIENT_PICKUP_INDEX(sEventData.sPickupID)
		
		IF iPickup != -1
			IF sEventData.Details.FromPlayerIndex != PLAYER_ID()
			AND DOES_ENTITY_EXIST(g_sSAPPickups[iPickup].oiPickup)
				#IF IS_DEBUG_BUILD
				VECTOR vCurrentCoords = GET_ENTITY_COORDS(g_sSAPPickups[iPickup].oiPickup)
				PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_PROCESS_SCRIPT_EVENT_SET_SYNCED_AMBIENT_PICKUP_COORDS - Updating co-ordinates of iPickup = ", iPickup, " from ", vCurrentCoords, " to ", sEventData.vCoords)
				#ENDIF
				
				SET_ENTITY_COORDS(g_sSAPPickups[iPickup].oiPickup, sEventData.vCoords)
			ENDIF
			
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				GlobalServerBD_BlockC.sSAPServer.sPickups[iPickup].sPickupData.vLocation = sEventData.vCoords
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAP_PROCESS_PLAYER_PICKUP_REQUESTS(PLAYER_INDEX playerID, BOOL bPlayerActive)
	IF NOT bPlayerActive
		EXIT
	ENDIF
	
	INT iPlayer = NATIVE_TO_INT(playerID)
	
	//Check whether the current pickup requests have a matching ID in server BD. If not, a new pickup has been requested and we need to add the data to server BD.
	INT i
	REPEAT SAP_MAX_SYNCED_AMBIENT_PICKUPS i
		IF IS_SYNCED_AMBIENT_PICKUP_ID_VALID(GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[i].sID)
		AND FIND_SYNCED_AMBIENT_PICKUP_INDEX(GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[i].sID) = -1
			PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_PROCESS_PLAYER_PICKUP_REQUESTS - ", GET_PLAYER_NAME(playerID), " (iPlayer = ", iPlayer, ") has requested a pickup with eType = ", GET_AMBIENT_PICKUP_NAME(GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[i].sID.eType), " and vInitialLocation = ", GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[i].sID.vInitialLocation)
			
			//Find an available index for the pickup data.
			INT iAvailableIndex = FIND_AVAILABLE_SYNCED_AMBIENT_PICKUP_INDEX()
			
			IF iAvailableIndex != -1
				PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_PROCESS_PLAYER_PICKUP_REQUESTS - Adding to server BD at iAvailableIndex = ", iAvailableIndex)
				SAP_CREATE_PICKUP_SERVER(iAvailableIndex, GlobalplayerBD_FM_4[iPlayer].sSAPClient.sPickupRequests[i])
			ELSE
				ASSERTLN("[SYNCED_AMBIENT_PICKUPS] SAP_PROCESS_PLAYER_PICKUP_REQUESTS - Not adding to server BD as there is no available index.")
				BREAKLOOP
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC PROCESS_SYNCED_AMBIENT_PICKUPS_STAGGERED_PLAYER_LOOP_SERVER(PLAYER_INDEX playerID, BOOL bPlayerActive)
	SAP_PROCESS_PLAYER_PICKUP_REQUESTS(playerID, bPlayerActive)
ENDPROC

PROC SAP_PROCESS_PLAYER_LOOP(SAP_VARS_STRUCT& sSAPVarsStruct)
	IF NOT MPGlobals.sFreemodeCache.bIsHostOfFreemodeScript
		IF sSAPVarsStruct.iPlayerStaggeredServer != 0
			PRINTLN("[SYNCED_AMBIENT_PICKUPS] SAP_PROCESS_PLAYER_LOOP - The local player is no longer the host, setting sSAPVarsStruct.iPlayerStaggeredServer = 0.")
			
			sSAPVarsStruct.iPlayerStaggeredServer = 0
		ENDIF
		
		EXIT
	ENDIF
	
	//Start of staggered loop.
	IF sSAPVarsStruct.iPlayerStaggeredServer = 0
		RESET_ARRAYED_BIT_SET(sSAPVarsStruct.iPickupEntityExistsServerBitSet)
	ENDIF
	
	//Staggered loop.
	INT i
	PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(i)
	
	IF NETWORK_IS_PLAYER_ACTIVE(playerID)
		REPEAT SAP_MAX_SYNCED_AMBIENT_PICKUPS i
			IF NOT SAP_IS_PICKUP_CLIENT_BIT_SET(i, playerID, eSAPPICKUPCLIENTBITSET_NOT_DOES_ENTITY_EXIST)
			AND NOT IS_ARRAYED_BIT_SET(sSAPVarsStruct.iPickupEntityExistsServerBitSet, i)
				SET_ARRAYED_BIT(sSAPVarsStruct.iPickupEntityExistsServerBitSet, i)
			ENDIF
		ENDREPEAT
	ENDIF
	
	sSAPVarsStruct.iPlayerStaggeredServer++
	
	//End of staggered loop.
	IF sSAPVarsStruct.iPlayerStaggeredServer = NUM_NETWORK_PLAYERS
		sSAPVarsStruct.iPlayerStaggeredServer = 0
		
		REPEAT SAP_MAX_SYNCED_AMBIENT_PICKUPS i
			IF NOT SAP_IS_PICKUP_SERVER_BIT_SET(i, eSAPPICKUPSERVERBITSET_NOT_DOES_ENTITY_EXIST)
			AND IS_SYNCED_AMBIENT_PICKUP_ID_VALID(GlobalServerBD_BlockC.sSAPServer.sPickups[i].sPickupData.sID)
			AND NOT IS_ARRAYED_BIT_SET(sSAPVarsStruct.iPickupEntityExistsServerBitSet, i)
				SAP_SET_PICKUP_SERVER_BIT(i, eSAPPICKUPSERVERBITSET_NOT_DOES_ENTITY_EXIST)
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

PROC SAP_PROCESS_CLIENT(SAP_VARS_STRUCT& sSAPVarsStruct)
	INT i
	REPEAT SAP_MAX_SYNCED_AMBIENT_PICKUPS i
		//Check whether there is a mismatch between server and local pickup IDs.
		IF NOT ARE_SYNCED_AMBIENT_PICKUP_IDS_EQUAL(GlobalServerBD_BlockC.sSAPServer.sPickups[i].sPickupData.sID, g_sSAPPickups[i].sID)
			//If the local pickup exists, we need to delete it.
			IF IS_SYNCED_AMBIENT_PICKUP_ID_VALID(g_sSAPPickups[i].sID)
				SAP_DELETE_PICKUP_LOCAL(i)
			ENDIF
			
			//If the server pickup data exists, we need to create a local pickup.
			IF IS_SYNCED_AMBIENT_PICKUP_ID_VALID(GlobalServerBD_BlockC.sSAPServer.sPickups[i].sPickupData.sID)
				//Check whether any current pickup requests in player BD have a matching ID in server BD. If so, we can remove the request from player BD.
				INT iRequestIndex = FIND_SYNCED_AMBIENT_PICKUP_REQUEST_INDEX(GlobalServerBD_BlockC.sSAPServer.sPickups[i].sPickupData.sID)
				
				IF iRequestIndex != -1
					//The local client requested this pickup. Delete the request and copy the OBJECT_INDEX.
					SAP_DELETE_PICKUP_REQUEST_CLIENT(iRequestIndex, FALSE)
					SAP_COPY_PICKUP_LOCAL(i, GlobalServerBD_BlockC.sSAPServer.sPickups[i].sPickupData)
				ELSE
					//A remote client requested this pickup. Create a new local pickup.
					SAP_CREATE_PICKUP_LOCAL(i, GlobalServerBD_BlockC.sSAPServer.sPickups[i].sPickupData)
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Check whether the local pickup entity no longer exists, potentially due to ambient cleanup behaviour.
	IF IS_SYNCED_AMBIENT_PICKUP_ID_VALID(g_sSAPPickups[sSAPVarsStruct.iPickupStaggered].sID)
	AND NOT SAP_IS_PICKUP_CLIENT_BIT_SET(sSAPVarsStruct.iPickupStaggered, PLAYER_ID(), eSAPPICKUPCLIENTBITSET_NOT_DOES_ENTITY_EXIST)
	AND NOT DOES_ENTITY_EXIST(g_sSAPPickups[sSAPVarsStruct.iPickupStaggered].oiPickup)
		SAP_SET_PICKUP_CLIENT_BIT(sSAPVarsStruct.iPickupStaggered, eSAPPICKUPCLIENTBITSET_NOT_DOES_ENTITY_EXIST)
	ENDIF
	
	sSAPVarsStruct.iPickupStaggered++
	
	IF sSAPVarsStruct.iPickupStaggered = SAP_MAX_SYNCED_AMBIENT_PICKUPS
		sSAPVarsStruct.iPickupStaggered = 0
	ENDIF
ENDPROC

PROC SAP_PROCESS_SERVER()
	INT i
	REPEAT SAP_MAX_SYNCED_AMBIENT_PICKUPS i
		IF IS_SYNCED_AMBIENT_PICKUP_ID_VALID(GlobalServerBD_BlockC.sSAPServer.sPickups[i].sPickupData.sID)
			IF SAP_IS_PICKUP_SERVER_BIT_SET(i, eSAPPICKUPSERVERBITSET_NOT_DOES_ENTITY_EXIST)
				SAP_DELETE_PICKUP_SERVER(i)
			ELIF SAP_IS_PICKUP_SCRIPT_OBJECT(GlobalServerBD_BlockC.sSAPServer.sPickups[i].sPickupData)
			AND NOT SAP_IS_PICKUP_SCRIPT_ACTIVE(GlobalServerBD_BlockC.sSAPServer.sPickups[i].sPickupData)
				SAP_CLEANUP_PICKUP_SERVER(i)
				CLEANUP_SYNCED_AMBIENT_PICKUP(GlobalServerBD_BlockC.sSAPServer.sPickups[i].sPickupData.sID)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC MAINTAIN_SYNCED_AMBIENT_PICKUPS(SAP_VARS_STRUCT& sSAPVarsStruct)
	IF NOT SHOULD_ENABLE_SYNCED_AMBIENT_PICKUPS()
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	SAP_DEBUG_MAINTAIN_DEBUG(sSAPVarsStruct)
	#ENDIF
	
	SAP_PROCESS_PLAYER_LOOP(sSAPVarsStruct)
	
	SAP_PROCESS_CLIENT(sSAPVarsStruct)
	
	IF MPGlobals.sFreemodeCache.bIsHostOfFreemodeScript
		SAP_PROCESS_SERVER()
	ENDIF
ENDPROC
