//////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        franklin_supply_stash.sch																	//
// Description: Helper functions for Franklin phone menu Supply Stash feature								//
// Written by:  Martin Grant																				//
// Date: 2016-11-11 (ISO 8601) 																				//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "net_include.sch"
USING "mp_globals_fm.sch"
USING "net_cash_transactions.sch"
USING "net_stat_tracking.sch"

#IF FEATURE_FIXER

///////////////////////////////////////////////////////////
//********************* DATA ****************************//
///////////////////////////////////////////////////////////

CONST_INT NUM_SUPPLY_STASH_LOCATIONS 151

ENUM SUPPLY_STASH_STATE
	SUPPLY_STASH_STATE_IDLE,
	SUPPLY_STASH_STATE_REQUEST,
	SUPPLY_STASH_STATE_CREATE,
	SUPPLY_STASH_STATE_ACTIVE,
	SUPPLY_STASH_STATE_CLEANUP
ENDENUM

ENUM SUPPLY_STASH_ANIM
	GRAB_LOW,
	GRAB_MID
ENDENUM

STRUCT SUPPLY_STASH
	VECTOR vLocation
	VECTOR vRotation
	FLOAT fHeading
	SUPPLY_STASH_ANIM eSupplyStashAnim	
ENDSTRUCT

STRUCT SUPPLY_STASH_DATA
	NETWORK_INDEX niFranklinSupplyStash
	BLIP_INDEX biFranklinSupplyStashBlip	
	BOOL bCachedSpawnLocation = FALSE
	SUPPLY_STASH eSupplyStash
	STRING sAnimDict
	STRING sAnimClip
	
	SUPPLY_STASH_STATE eSUPPLY_STASH_STATE = SUPPLY_STASH_STATE_IDLE			
ENDSTRUCT

///////////////////////////////////////////////////////////
//********************* FUNCTIONS ***********************//
///////////////////////////////////////////////////////////

/// PURPOSE: Get and populate a SUPPLY_STASH struct based on a location index 0 to NUM_SUPPLY_STASH_LOCATIONS
FUNC SUPPLY_STASH GET_SUPPLY_STASH(INT iLocation)
	SUPPLY_STASH tempSupplyStash 

	SWITCH iLocation	
		CASE 0	
			tempSupplyStash.vLocation   = <<-143.8830, 1907.2970, 196.3420>>
			tempSupplyStash.fHeading  = 49.6000
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 49.6000>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 1
			tempSupplyStash.vLocation   = <<1221.7450, 1866.0980, 77.8740>>
			tempSupplyStash.fHeading  = 250.5990
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -109.4010>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 2
			tempSupplyStash.vLocation   = <<1538.0660, 1727.7209, 108.8320>>
			tempSupplyStash.fHeading  = 101.9990
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 101.9990>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 3
			tempSupplyStash.vLocation   = <<2461.1279, 1572.2490, 31.7400>>
			tempSupplyStash.fHeading  = 58.1990
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 58.1990>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 4
			tempSupplyStash.vLocation   = <<2659.4651, 1377.4280, 23.0480>>
			tempSupplyStash.fHeading  = 68.1990
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 68.1990>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 5
			tempSupplyStash.vLocation   = <<2796.6980, 1678.5260, 20.0650>>
			tempSupplyStash.fHeading  = 6.1990
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 6.1990>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 6
			tempSupplyStash.vLocation   = <<-1955.1410, 1776.7440, 182.0580>>
			tempSupplyStash.fHeading  = 116.9990
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 116.9990>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 7
			tempSupplyStash.vLocation   = <<-2722.8000, 1492.9160, 102.9110>>
			tempSupplyStash.fHeading  = 276.5980
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -83.4020>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 8
			tempSupplyStash.vLocation   = <<-3190.4290, 1068.7750, 19.8380>>
			tempSupplyStash.fHeading  = 333.9980
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -26.0020>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 9
			tempSupplyStash.vLocation   = <<-2555.7261, 1916.3680, 167.9260>>
			tempSupplyStash.fHeading  = -26.0020
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -26.0020>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 10
			tempSupplyStash.vLocation   = <<-2541.6699, 2302.9951, 32.2120>>
			tempSupplyStash.fHeading  = 52.1980
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 52.1980>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 11
			tempSupplyStash.vLocation   = <<-1876.1190, 2027.9240, 138.9850>>
			tempSupplyStash.fHeading  = 247.5980
			tempSupplyStash.vRotation = <<-8.0600, -3.8260, -112.6720>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 12	
			tempSupplyStash.vLocation   = <<-1577.7590, 2099.4509, 67.5470>>
			tempSupplyStash.fHeading  = 16.9280
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 16.9280>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 13
			tempSupplyStash.vLocation   = <<-1118.7640, 2714.1440, 18.0860>>
			tempSupplyStash.fHeading  = 234.9280
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -125.0720>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 14
			tempSupplyStash.vLocation   = <<-274.7510, 2210.4900, 128.7920>>
			tempSupplyStash.fHeading  = -125.0720
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -125.0720>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 15
			tempSupplyStash.vLocation   = <<-287.6270, 2534.3059, 73.7050>>
			tempSupplyStash.fHeading  = 68.1280
			tempSupplyStash.vRotation = <<5.1680, 2.2410, 68.0260>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 16
			tempSupplyStash.vLocation   = <<-285.4640, 2839.4021, 53.7760>>
			tempSupplyStash.fHeading  = 150.0260
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 150.0260>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 17
			tempSupplyStash.vLocation   = <<-592.7850, 2095.9529, 130.6180>>
			tempSupplyStash.fHeading  = 238.2260
			tempSupplyStash.vRotation = <<9.5670, 2.8680, -122.0140>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 18
			tempSupplyStash.vLocation   = <<155.8420, 2299.5459, 93.2040>>
			tempSupplyStash.fHeading  = 288.5850
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -71.4150>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 19
			tempSupplyStash.vLocation   = <<264.0500, 2606.5801, 43.9850>>
			tempSupplyStash.fHeading  = 11.3850
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 11.3850>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 20
			tempSupplyStash.vLocation   = <<431.5840, 2994.3660, 39.4800>>
			tempSupplyStash.fHeading  = 249.3850
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -110.6150>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 21
			tempSupplyStash.vLocation   = <<164.7620, 3120.4661, 42.4290>>
			tempSupplyStash.fHeading  = 314.3850
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -45.6150>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 22
			tempSupplyStash.vLocation   = <<372.8360, 3404.6201, 35.4010>>
			tempSupplyStash.fHeading  = 53.5850
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 53.5850>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 23
			tempSupplyStash.vLocation   = <<912.0030, 3556.3770, 33.3860>>
			tempSupplyStash.fHeading  = 42.1830
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 42.1830>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 24
			tempSupplyStash.vLocation   = <<1549.7810, 3801.4609, 33.2500>>
			tempSupplyStash.fHeading  = 110.1830
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 110.1830>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 25
			tempSupplyStash.vLocation   = <<1906.8340, 3886.9819, 32.6170>>
			tempSupplyStash.fHeading  = 126.7830
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 126.7830>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 26
			tempSupplyStash.vLocation   = <<2392.4980, 4022.2290, 35.2540>>
			tempSupplyStash.fHeading  = 154.7820
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 154.7820>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 27
			tempSupplyStash.vLocation   = <<3431.0930, 3756.0090, 29.6630>>
			tempSupplyStash.fHeading  = 170.1360
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 170.1360>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 28
			tempSupplyStash.vLocation   = <<2915.1489, 3628.1160, 43.4070>>
			tempSupplyStash.fHeading  = 274.5360
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -85.4640>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 29
			tempSupplyStash.vLocation   = <<2531.6570, 2843.1780, 38.0820>>
			tempSupplyStash.fHeading  = 90.1360
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 90.1360>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 30
			tempSupplyStash.vLocation   = <<2546.8220, 2576.6670, 37.7840>>
			tempSupplyStash.fHeading  = 9.3350
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 9.3350>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 31
			tempSupplyStash.vLocation   = <<2353.1621, 2612.4780, 46.4500>>
			tempSupplyStash.fHeading  = 324.1350
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -35.8650>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 32
			tempSupplyStash.vLocation   = <<1593.9790, 2201.4890, 79.0020>>
			tempSupplyStash.fHeading  = 135.9350
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 135.9350>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 33
			tempSupplyStash.vLocation   = <<866.9890, 2161.0720, 52.0680>>
			tempSupplyStash.fHeading  = 358.9340
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -1.0660>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 34
			tempSupplyStash.vLocation   = <<650.3470, 2726.6799, 42.2380>>
			tempSupplyStash.fHeading  = 181.3340
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -178.6660>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 35
			tempSupplyStash.vLocation   = <<1198.3760, 2648.8601, 40.9370>>
			tempSupplyStash.fHeading  = 87.1340
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 87.1340>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 36
			tempSupplyStash.vLocation   = <<1591.4740, 2904.8420, 56.6300>>
			tempSupplyStash.fHeading  = 27.3340
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 27.3340>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 37
			tempSupplyStash.vLocation   = <<1988.7350, 3046.6970, 46.9670>>
			tempSupplyStash.fHeading  = 155.5340
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 155.5340>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 38
			tempSupplyStash.vLocation   = <<2354.8149, 3060.3120, 48.2910>>
			tempSupplyStash.fHeading  = 93.9340
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 93.9340>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 39
			tempSupplyStash.vLocation   = <<2681.2261, 3266.2820, 54.3710>>
			tempSupplyStash.fHeading  = 63.0730
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 63.0730>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 40
			tempSupplyStash.vLocation   = <<1703.3280, 3290.6489, 48.6860>>
			tempSupplyStash.fHeading  = 288.8730
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -71.1270>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 41
			tempSupplyStash.vLocation   = <<2190.9910, 3309.7419, 46.0840>>
			tempSupplyStash.fHeading  = -49.1270
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -49.1270>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 42
			tempSupplyStash.vLocation   = <<1106.7600, 3142.4080, 37.0360>>
			tempSupplyStash.fHeading  = 32.0730
			tempSupplyStash.vRotation = <<-8.4090, 0.8030, 32.1320>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 43
			tempSupplyStash.vLocation   = <<66.1760, 3711.5010, 39.3380>>
			tempSupplyStash.fHeading  = 32.1320
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 32.1320>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 44
			tempSupplyStash.vLocation   = <<-196.6920, 3642.3301, 63.4510>>
			tempSupplyStash.fHeading  = 19.7320
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 19.7320>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 45
			tempSupplyStash.vLocation   = <<-1633.9290, 3026.4700, 31.6190>>
			tempSupplyStash.fHeading  = 35.1320
			tempSupplyStash.vRotation = <<-14.2060, 2.5410, 35.4490>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 46
			tempSupplyStash.vLocation   = <<-2512.0601, 3615.1321, 12.6930>>
			tempSupplyStash.fHeading  = 79.2490
			tempSupplyStash.vRotation = <<6.7350, 0.4570, 79.2220>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 47
			tempSupplyStash.vLocation   = <<-2191.5129, 4283.7290, 49.0300>>
			tempSupplyStash.fHeading  = 254.0210
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -105.9790>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 48
			tempSupplyStash.vLocation   = <<-522.7670, 4192.8950, 192.7330>>
			tempSupplyStash.fHeading  = 296.0210
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -63.9790>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 49
			tempSupplyStash.vLocation   = <<727.8220, 4190.0649, 40.4550>>
			tempSupplyStash.fHeading  = 6.2210
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 6.2210>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 50
			tempSupplyStash.vLocation   = <<1312.3610, 4389.5088, 41.0460>>
			tempSupplyStash.fHeading  = 40.4210
			tempSupplyStash.vRotation = <<9.0130, -10.5320, 41.2540>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 51
			tempSupplyStash.vLocation   = <<3818.2380, 4470.4380, 2.9890>>
			tempSupplyStash.fHeading  = 316.4540
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -43.5460>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 52
			tempSupplyStash.vLocation   = <<3313.6509, 5173.3940, 18.2380>>
			tempSupplyStash.fHeading  = 67.4530
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 67.4530>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 53
			tempSupplyStash.vLocation   = <<-1576.1510, 5175.5122, 18.5320>>
			tempSupplyStash.fHeading  = 330.8530
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -29.1470>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 54
			tempSupplyStash.vLocation   = <<1727.1281, 6412.7949, 34.0140>>
			tempSupplyStash.fHeading  = 16.8530
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 16.8530>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 55
			tempSupplyStash.vLocation   = <<1470.4230, 6541.0732, 13.6830>>
			tempSupplyStash.fHeading  = 247.8530
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -112.1470>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 56
			tempSupplyStash.vLocation   = <<450.9550, 5574.2319, 780.1860>>
			tempSupplyStash.fHeading  = 353.4530
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -6.5470>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 57
			tempSupplyStash.vLocation   = <<2224.5879, 5606.1118, 53.6770>>
			tempSupplyStash.fHeading  = 273.2530
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -86.7470>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 58
			tempSupplyStash.vLocation   = <<1818.0520, 3668.7161, 33.2740>>
			tempSupplyStash.fHeading  = 307.0520
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -52.9480>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 59
			tempSupplyStash.vLocation   = <<2483.6799, 3724.4761, 43.0890>>
			tempSupplyStash.fHeading  = 223.2520
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -136.7480>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 60
			tempSupplyStash.vLocation   = <<2727.9839, 4282.4580, 48.1290>>
			tempSupplyStash.fHeading  = 270.8520
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -89.1480>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 61
			tempSupplyStash.vLocation   = <<2935.7510, 4610.7690, 48.8670>>
			tempSupplyStash.fHeading  = 319.4510
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -40.5490>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 62
			tempSupplyStash.vLocation   = <<1708.6980, 4790.3589, 40.9560>>
			tempSupplyStash.fHeading  = 141.2510
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 141.2510>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 63
			tempSupplyStash.vLocation   = <<1969.2970, 5170.9648, 47.7760>>
			tempSupplyStash.fHeading  = 309.2510
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -50.7490>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 64
			tempSupplyStash.vLocation   = <<2294.0940, 4877.9150, 40.8060>>
			tempSupplyStash.fHeading  = 79.8510
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 79.8510>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 65
			tempSupplyStash.vLocation   = <<2580.5081, 4650.5439, 33.0080>>
			tempSupplyStash.fHeading  = 241.8510
			tempSupplyStash.vRotation = <<-4.4390, 1.3470, -118.0970>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 66
			tempSupplyStash.vLocation   = <<2591.4360, 5061.6689, 43.8880>>
			tempSupplyStash.fHeading  = 198.1030
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -161.8970>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 67
			tempSupplyStash.vLocation   = <<1962.2480, 4617.8511, 40.1230>>
			tempSupplyStash.fHeading  = 325.9030
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -34.0970>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 68
			tempSupplyStash.vLocation   = <<-756.8840, 5587.2290, 35.7090>>
			tempSupplyStash.fHeading  = -113.8970
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -113.8970>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 69
			tempSupplyStash.vLocation   = <<-356.1520, 4825.1680, 142.2220>>
			tempSupplyStash.fHeading  = 307.3030
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -52.6970>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 70
			tempSupplyStash.vLocation   = <<426.5090, 6486.7910, 27.7570>>
			tempSupplyStash.fHeading  = 115.7020
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 115.7020>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 71
			tempSupplyStash.vLocation   = <<-10.3980, 6651.9458, 30.6930>>
			tempSupplyStash.fHeading  = 292.1020
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -67.8980>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 72
			tempSupplyStash.vLocation   = <<-443.7330, 5985.2002, 31.8150>>
			tempSupplyStash.fHeading  = 312.5020
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -47.4980>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 73
			tempSupplyStash.vLocation   = <<-28.9570, 6279.0488, 30.3750>>
			tempSupplyStash.fHeading  = 213.9020
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -146.0980>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 74
			tempSupplyStash.vLocation   = <<-292.5250, 6345.1621, 32.3170>>
			tempSupplyStash.fHeading  = 226.5020
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -133.4980>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 75
			tempSupplyStash.vLocation   = <<-889.3720, 4419.2769, 19.8450>>
			tempSupplyStash.fHeading  = -145.4480
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -145.4480>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 76
			tempSupplyStash.vLocation   = <<1381.1810, 1140.5410, 113.9070>>
			tempSupplyStash.fHeading  = 94.3520
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 94.3520>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 77
			tempSupplyStash.vLocation   = <<750.9050, 1291.1680, 360.2460>>
			tempSupplyStash.fHeading  = 335.3520
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -24.6480>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 78
			tempSupplyStash.vLocation   = <<266.8400, 1106.3750, 217.8880>>
			tempSupplyStash.fHeading  = 122.3520
			tempSupplyStash.vRotation = <<-0.3770, -3.6460, 122.3400>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 79
			tempSupplyStash.vLocation   = <<-426.5200, 1214.8630, 325.9090>>
			tempSupplyStash.fHeading  = 158.9380
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 158.9380>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 80
			tempSupplyStash.vLocation   = <<-1549.4620, 1378.0490, 125.4100>>
			tempSupplyStash.fHeading  = 322.7390
			tempSupplyStash.vRotation = <<-0.7890, 5.7860, -37.2210>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 81
			tempSupplyStash.vLocation   = <<1551.5580, 837.1030, 78.3970>>
			tempSupplyStash.fHeading  = 229.1790
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -130.8210>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 82
			tempSupplyStash.vLocation   = <<1912.0601, 576.1810, 174.7850>>
			tempSupplyStash.fHeading  = 343.7790
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -16.2210>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 83
			tempSupplyStash.vLocation   = <<2559.4460, 377.3990, 107.5910>>
			tempSupplyStash.fHeading  = 84.5790
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 84.5790>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 84
			tempSupplyStash.vLocation   = <<878.8850, 535.3720, 125.8020>>
			tempSupplyStash.fHeading  = 232.9780
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -127.0220>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 85
			tempSupplyStash.vLocation   = <<1667.5450, -59.6070, 173.1680>>
			tempSupplyStash.fHeading  = 120.7780
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 120.7780>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 86
			tempSupplyStash.vLocation   = <<2817.1450, -670.4880, 0.6990>>
			tempSupplyStash.fHeading  = 27.7780
			tempSupplyStash.vRotation = <<5.2820, 2.7470, 27.6510>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 87
			tempSupplyStash.vLocation   = <<2575.6069, -299.5960, 92.0760>>
			tempSupplyStash.fHeading  = 95.0510
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 95.0510>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 88
			tempSupplyStash.vLocation   = <<-3094.4751, 753.4880, 28.9830>>
			tempSupplyStash.fHeading  = 170.4000
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 170.4000>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 89
			tempSupplyStash.vLocation   = <<-2963.7610, 429.8030, 15.6750>>
			tempSupplyStash.fHeading  = 266.0000
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -94.0000>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 90
			tempSupplyStash.vLocation   = <<-1662.8051, -1083.4620, 12.1140>>
			tempSupplyStash.fHeading  = 140.2000
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 140.2000>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 91
			tempSupplyStash.vLocation   = <<-1816.7810, 793.8780, 137.0630>>
			tempSupplyStash.fHeading  = 93.6000
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 93.6000>>	
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 92
			tempSupplyStash.vLocation   = <<-2292.4299, 351.2480, 173.5990>>
			tempSupplyStash.fHeading  = 148.8000
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 148.8000>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 93
			tempSupplyStash.vLocation   = <<-2196.6460, -381.1410, 12.9610>>
			tempSupplyStash.fHeading  = 137.9990
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 137.9990>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 94
			tempSupplyStash.vLocation   = <<-1729.4310, -219.4230, 56.2180>>
			tempSupplyStash.fHeading  = 180.9990
			tempSupplyStash.vRotation = <<-1.9250, 5.8840, -178.9020>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 95
			tempSupplyStash.vLocation   = <<-1344.0500, -1532.4080, 4.0890>>
			tempSupplyStash.fHeading  = 168.8980
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 168.8980>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 96
			tempSupplyStash.vLocation   = <<937.0820, 93.1030, 78.0710>>
			tempSupplyStash.fHeading  = 173.8980
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 173.8980>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 97
			tempSupplyStash.vLocation   = <<1718.4790, -1598.9139, 111.5130>>
			tempSupplyStash.fHeading  = 39.8980
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 39.8980>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 98
			tempSupplyStash.vLocation   = <<1510.3180, -2122.0010, 76.1010>>
			tempSupplyStash.fHeading  = 275.4970
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -84.5030>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 99
			tempSupplyStash.vLocation   = <<1282.6400, -2553.9709, 43.7510>>
			tempSupplyStash.fHeading  = 110.4970
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 110.4970>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 100
			tempSupplyStash.vLocation   = <<817.4690, -2970.2319, 5.0180>>
			tempSupplyStash.fHeading  = -0.6060
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -0.6060>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 101
			tempSupplyStash.vLocation   = <<1195.3190, -3247.5330, 5.6550>>
			tempSupplyStash.fHeading  = 178.7940
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 178.7940>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		
		CASE 102
			tempSupplyStash.vLocation   = <<1884.3300, -1027.3840, 77.7480>>
			tempSupplyStash.fHeading  = 7.7930
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 7.7930>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 103
			tempSupplyStash.vLocation   = <<388.9120, 798.8490, 186.6740>>
			tempSupplyStash.fHeading  = 98.9930
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 98.9930>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 104
			tempSupplyStash.vLocation   = <<612.1190, -3060.1709, 5.0670>>
			tempSupplyStash.fHeading  = 328.9930
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -31.0070>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 105
			tempSupplyStash.vLocation   = <<672.0860, -2668.6040, 5.0790>>
			tempSupplyStash.fHeading  = 272.7930
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -87.2070>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 106
			tempSupplyStash.vLocation   = <<119.7230, -3210.2290, 5.0220>>
			tempSupplyStash.fHeading  = 100.3930
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 100.3930>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 107
			tempSupplyStash.vLocation   = <<311.0110, -2860.0371, 5.1550>>
			tempSupplyStash.fHeading  = 217.7920
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -142.2080>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 108
			tempSupplyStash.vLocation   = <<161.9750, -2385.6899, 4.9980>>
			tempSupplyStash.fHeading  = 358.1920
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -1.8080>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 109
			tempSupplyStash.vLocation   = <<-168.4630, -2694.9851, 5.6790>>
			tempSupplyStash.fHeading  = 88.7920
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 88.7920>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 110
			tempSupplyStash.vLocation   = <<1079.6550, -776.6270, 57.2430>>
			tempSupplyStash.fHeading  = 180.5920
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -179.4080>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 111
			tempSupplyStash.vLocation   = <<1185.6169, -415.3000, 67.0260>>
			tempSupplyStash.fHeading  = 76.5920
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 76.5920>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 112
			tempSupplyStash.vLocation   = <<-1027.8340, -2746.9260, 13.3600>>
			tempSupplyStash.fHeading  = 230.1910
			tempSupplyStash.vRotation = <<-3.3080, -2.1040, -129.8690>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 113
			tempSupplyStash.vLocation   = <<-1117.3610, -1992.4900, 12.3310>>
			tempSupplyStash.fHeading  = 318.1300
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -41.8700>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 114
			tempSupplyStash.vLocation   = <<-704.7610, -2283.1609, 12.0410>>
			tempSupplyStash.fHeading  = 49.3300
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 49.3300>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 115
			tempSupplyStash.vLocation   = <<-98.0440, 893.9030, 235.2110>>
			tempSupplyStash.fHeading  = 289.2000
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -70.8000>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 116
			tempSupplyStash.vLocation   = <<171.6380, -944.8790, 30.1270>>
			tempSupplyStash.fHeading  = -70.8000
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -70.8000>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 117
			tempSupplyStash.vLocation   = <<-531.1480, -232.0230, 36.1430>>
			tempSupplyStash.fHeading  = 211.6000
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -148.4000>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 118
			tempSupplyStash.vLocation   = <<-771.7960, 887.2200, 203.3090>>
			tempSupplyStash.fHeading  = 4.6000
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 4.6000>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 119
			tempSupplyStash.vLocation   = <<-1634.7960, 216.9240, 59.6610>>
			tempSupplyStash.fHeading  = 78.6000
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 78.6000>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 120
			tempSupplyStash.vLocation   = <<-628.3200, -1634.8640, 24.9950>>
			tempSupplyStash.fHeading  = 358.8000
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -1.2000>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 121
			tempSupplyStash.vLocation   = <<79.6980, 292.9520, 109.2080>>
			tempSupplyStash.fHeading  = 303.9990
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -56.0010>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 122
			tempSupplyStash.vLocation   = <<-374.5610, 522.7480, 120.4070>>
			tempSupplyStash.fHeading  = 285.9990
			tempSupplyStash.vRotation = <<9.3950, 4.8850, -74.4020>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 123
			tempSupplyStash.vLocation   = <<-1074.3630, 458.7380, 76.5380>>
			tempSupplyStash.fHeading  = 331.7970
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -28.2030>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 124
			tempSupplyStash.vLocation   = <<-907.8328, -1464.8223, 0.5945>>
			tempSupplyStash.fHeading  = 26.9970
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 26.9970>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 125
			tempSupplyStash.vLocation   = <<-36.9950, -2097.5139, 15.7240>>
			tempSupplyStash.fHeading  = 285.9970
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -74.0030>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 126
			tempSupplyStash.vLocation   = <<1155.0861, -1666.1200, 35.5610>>
			tempSupplyStash.fHeading  = 96.3970
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 96.3970>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 127
			tempSupplyStash.vLocation   = <<1246.9860, -1102.1899, 37.5230>>
			tempSupplyStash.fHeading  = 305.1970
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -54.8030>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 128
			tempSupplyStash.vLocation   = <<866.1165, -2364.5547, 30.5075>>
			tempSupplyStash.fHeading  = 84.3959
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 84.3959>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 129
			tempSupplyStash.vLocation   = <<1053.9659, -1951.7990, 31.0970>>
			tempSupplyStash.fHeading  = 59.7960
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 59.7960>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 130
			tempSupplyStash.vLocation   = <<985.2480, -1382.7860, 30.5450>>
			tempSupplyStash.fHeading  = 171.9960
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 171.9960>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 131
			tempSupplyStash.vLocation   = <<-523.3250, 163.6590, 70.0790>>
			tempSupplyStash.fHeading  = 67.9960
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 67.9960>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 132
			tempSupplyStash.vLocation   = <<283.0310, 64.7250, 93.3690>>
			tempSupplyStash.fHeading  = 143.1950
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 143.1950>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 133
			tempSupplyStash.vLocation   = <<542.6700, -175.5650, 53.4840>>
			tempSupplyStash.fHeading  = 276.3950
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -83.6050>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 134
			tempSupplyStash.vLocation   = <<19.9250, -251.2880, 46.6560>>
			tempSupplyStash.fHeading  = 21.9950
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 21.9950>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 135
			tempSupplyStash.vLocation   = <<-1050.7930, -518.6890, 36.0680>>
			tempSupplyStash.fHeading  = 16.3940
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 16.3940>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 136
			tempSupplyStash.vLocation   = <<-936.6358, 104.5082, 52.3728>>
			tempSupplyStash.fHeading  = 16.9940
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 16.9940>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 137
			tempSupplyStash.vLocation   = <<-1474.0110, -653.6160, 28.3750>>
			tempSupplyStash.fHeading  = 134.7940
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 134.7940>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 138
			tempSupplyStash.vLocation   = <<-1150.0330, -1090.6620, 1.1480>>
			tempSupplyStash.fHeading  = 267.9940
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -92.0060>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 139
			tempSupplyStash.vLocation   = <<578.0550, -2288.6941, 4.9850>>
			tempSupplyStash.fHeading  = 177.9940
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 177.9940>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 140
			tempSupplyStash.vLocation   = <<813.4100, -1600.2900, 31.1690>>
			tempSupplyStash.fHeading  = -2.0060
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -2.0060>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 141
			tempSupplyStash.vLocation   = <<441.1680, -1871.1801, 27.0360>>
			tempSupplyStash.fHeading  = 231.9930
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -128.0070>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 142
			tempSupplyStash.vLocation   = <<805.2000, -1061.9200, 27.4230>>
			tempSupplyStash.fHeading  = 306.3930
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -53.6070>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 143
			tempSupplyStash.vLocation   = <<809.4270, -526.3140, 37.0100>>
			tempSupplyStash.fHeading  = 303.3930
			tempSupplyStash.vRotation = <<2.5940, -8.7840, -56.4080>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 144
			tempSupplyStash.vLocation   = <<122.5330, -1731.5470, 29.5440>>
			tempSupplyStash.fHeading  = 147.3920
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 147.3920>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 145
			tempSupplyStash.vLocation   = <<506.0010, -1337.6130, 29.1610>>
			tempSupplyStash.fHeading  = 204.5920
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -155.4080>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 146
			tempSupplyStash.vLocation   = <<31.8810, -1300.3990, 28.2530>>
			tempSupplyStash.fHeading  = 65.7920
			tempSupplyStash.vRotation = <<0.0000, 0.0000, 65.7920>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 147
			tempSupplyStash.vLocation   = <<517.7240, -706.8990, 24.8530>>
			tempSupplyStash.fHeading  = 245.1920
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -114.8080>>
			tempSupplyStash.eSupplyStashAnim = GRAB_MID
		BREAK
		CASE 148
			tempSupplyStash.vLocation   = <<-519.8350, -1208.3440, 17.3540>>
			tempSupplyStash.fHeading  = 197.5920
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -162.4080>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 149
			tempSupplyStash.vLocation   = <<-693.4220, -730.5090, 28.3490>>
			tempSupplyStash.fHeading  = 356.1920
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -3.8080>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
		CASE 150
			tempSupplyStash.vLocation   = <<-99.6810, -609.2020, 35.2610>>
			tempSupplyStash.fHeading  = 348.3910
			tempSupplyStash.vRotation = <<0.0000, 0.0000, -11.6090>>
			tempSupplyStash.eSupplyStashAnim = GRAB_LOW
		BREAK
	ENDSWITCH
	
	RETURN tempSupplyStash
ENDFUNC

/// PURPOSE: Get the closest Supply Stash to the player's current position
PROC SET_NEAREST_SUPPLY_STASH(SUPPLY_STASH_DATA& eSupplyStashData)
	VECTOR vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
	FLOAT fNearestLocationDistance = 9999
	FLOAT fMinimumDistanceToSpawn = 50.0
	
	eSupplyStashData.eSupplyStash = GET_SUPPLY_STASH(0) // Set cached supply stash data to first in list in case loop below fails and we have a safe fall-back location to use
	
	INT i	
	FOR i = 0 TO NUM_SUPPLY_STASH_LOCATIONS - 1
		SUPPLY_STASH eCurrentStash = GET_SUPPLY_STASH(i)
		FLOAT fDistance = GET_DISTANCE_BETWEEN_COORDS(vPlayerPosition, eCurrentStash.vLocation)
							
		IF (fDistance < fNearestLocationDistance)
		AND (fDistance >= fMinimumDistanceToSpawn) // Get the nearest location that is at least fMinimumDistanceToSpawn away
			eSupplyStashData.eSupplyStash = eCurrentStash
			fNearestLocationDistance = fDistance
			
			IF (fDistance < fMinimumDistanceToSpawn + 20.0) // We are probably already at the closest location so just exit here instead of checking all other locations
				BREAKLOOP
			ENDIF
		ENDIF
		
	ENDFOR
	
	PRINTLN("[FRANKLIN_SUPPLY_STASH] - SET_NEAREST_SUPPLY_STASH - Setting nearest stash location to: ", eSupplyStashData.eSupplyStash.vLocation)
ENDPROC

/// PURPOSE: Create a Supply Stash close to the player, blip it and provide help text informing the player
FUNC BOOL CREATE_SUPPLY_STASH(SUPPLY_STASH_DATA& eSupplyStashData)
	IF NOT eSupplyStashData.bCachedSpawnLocation
		SET_NEAREST_SUPPLY_STASH(eSupplyStashData)
		eSupplyStashData.bCachedSpawnLocation = TRUE
	ENDIF
	
	IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(eSupplyStashData.niFranklinSupplyStash)
		MODEL_NAMES mSupplyStashModel = INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_backpack_01a"))
	
		IF REQUEST_LOAD_MODEL(mSupplyStashModel)			
			IF CREATE_NET_OBJ(eSupplyStashData.niFranklinSupplyStash, mSupplyStashModel, eSupplyStashData.eSupplyStash.vLocation, FALSE)
				ENTITY_INDEX stashAsEntity = NET_TO_ENT(eSupplyStashData.niFranklinSupplyStash)
				
				SET_ENTITY_ROTATION(stashAsEntity, eSupplyStashData.eSupplyStash.vRotation)
				SET_ENTITY_HEADING(stashAsEntity, eSupplyStashData.eSupplyStash.fHeading)
				FREEZE_ENTITY_POSITION(stashAsEntity, TRUE)
				
				eSupplyStashData.biFranklinSupplyStashBlip = CREATE_BLIP_FOR_ENTITY(stashAsEntity)
				SET_BLIP_SPRITE(eSupplyStashData.biFranklinSupplyStashBlip, RADAR_TRACE_RUCKSACK)
				SET_BLIP_SCALE(eSupplyStashData.biFranklinSupplyStashBlip, 1.0)
				SET_BLIP_COLOUR(eSupplyStashData.biFranklinSupplyStashBlip, BLIP_COLOUR_WHITE)
				SET_BLIP_FLASHES(eSupplyStashData.biFranklinSupplyStashBlip, TRUE)
            	SET_BLIP_FLASH_TIMER(eSupplyStashData.biFranklinSupplyStashBlip, 10000)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("sf_prop_sf_backpack_01a")))
				
				PRINT_HELP("FRANK_SUPL_STSH", DEFAULT_HELP_TEXT_TIME)
				
				SWITCH eSupplyStashData.eSupplyStash.eSupplyStashAnim
					CASE GRAB_LOW
						IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
							eSupplyStashData.sAnimDict = "anim@scripted@player@freemode@tun_prep_grab_midd_ig3@heeled@"						
						ELSE
							eSupplyStashData.sAnimDict = "anim@scripted@player@freemode@tun_prep_grab_midd_ig3@male@"
						ENDIF
						eSupplyStashData.sAnimClip = "tun_prep_grab_midd_ig3"
					BREAK
					
					CASE GRAB_MID
						IF IS_PED_WEARING_HIGH_HEELS(PLAYER_PED_ID())
							eSupplyStashData.sAnimDict = "anim@scripted@player@freemode@tun_prep_ig1_grab_low@heeled@"						
						ELSE
							eSupplyStashData.sAnimDict = "anim@scripted@player@freemode@tun_prep_ig1_grab_low@male@"
						ENDIF
						eSupplyStashData.sAnimClip = "grab_low"
					BREAK
				ENDSWITCH
			
				PRINTLN("[FRANKLIN_SUPPLY_STASH] - CREATE_SUPPLY_STASH - Created successfully at: ", eSupplyStashData.eSupplyStash.vLocation)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF	

	PRINTLN("[FRANKLIN_SUPPLY_STASH] - CREATE_SUPPLY_STASH - Failed to create")		
	RETURN FALSE
ENDFUNC

/// PURPOSE: Check if the Supply Stash spawn timer is off cooldown (we will spawn the Supply Stash nearby after g_stSupplyStashSpawnTimer has elapsed)
FUNC BOOL HAS_SUPPLY_STASH_SPAWN_TIMER_EXPIRED()	
	IF HAS_NET_TIMER_STARTED_AND_EXPIRED(g_stSupplyStashSpawnTimer, iSupplyStashSpawnTime)
		PRINTLN("[FRANKLIN_SUPPLY_STASH] - HAS_SUPPLY_STASH_SPAWN_TIMER_EXPIRED - g_stSupplyStashSpawnTimer is finished, we can create the Supply Stash nearby")	
		RETURN TRUE		
	ELSE
		PRINTLN("[FRANKLIN_SUPPLY_STASH] - HAS_SUPPLY_STASH_SPAWN_TIMER_EXPIRED - g_stSupplyStashSpawnTimer is not finished, we cannot create the Supply Stash nearby")
		RETURN FALSE		
	ENDIF	
ENDFUNC

/// PURPOSE: Return string representations of SUPPLY_STASH_STATE for useful printing to logs
FUNC STRING GET_SUPPLY_STASH_STATE_AS_STRING(SUPPLY_STASH_STATE eState)
	SWITCH eState
		CASE SUPPLY_STASH_STATE_IDLE		RETURN "SUPPLY_STASH_STATE_IDLE"
		CASE SUPPLY_STASH_STATE_REQUEST		RETURN "SUPPLY_STASH_STATE_REQUEST"
		CASE SUPPLY_STASH_STATE_CREATE		RETURN "SUPPLY_STASH_STATE_CREATE"
		CASE SUPPLY_STASH_STATE_ACTIVE		RETURN "SUPPLY_STASH_STATE_ACTIVE"
		CASE SUPPLY_STASH_STATE_CLEANUP		RETURN "SUPPLY_STASH_STATE_CLEANUP"
	ENDSWITCH	
	
	RETURN "GET_SUPPLY_STASH_STATE_AS_STRING - INVALID PARAMETER"
ENDFUNC

/// PURPOSE: Set the current state of the Supply Stash system
PROC SET_SUPPLY_STASH_STATE(SUPPLY_STASH_DATA& eSupplyStashData, SUPPLY_STASH_STATE eNewState)
	PRINTLN("[FRANKLIN_SUPPLY_STASH] - SET_SUPPLY_STASH_STATE - moving from ", GET_SUPPLY_STASH_STATE_AS_STRING(eSupplyStashData.eSUPPLY_STASH_STATE), " to ", GET_SUPPLY_STASH_STATE_AS_STRING(eNewState))
	eSupplyStashData.eSUPPLY_STASH_STATE = eNewState
ENDPROC

FUNC INT GET_MAX_UNLOCKED_ARMOUR()
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_SUPERHEAVYARMOUR)
		RETURN 5
	ELIF IS_MP_KIT_UNLOCKED(PLAYERKIT_HEAVYARMOUR)
		RETURN 4
	ELIF IS_MP_KIT_UNLOCKED(PLAYERKIT_STANDARDARMOUR)
		RETURN 3
	ELIF IS_MP_KIT_UNLOCKED(PLAYERKIT_LIGHTARMOUR)
		RETURN 2
	ELIF IS_MP_KIT_UNLOCKED(PLAYERKIT_SUPERLIGHTARMOUR)
		RETURN 1
	ENDIF

	RETURN 0
ENDFUNC

FUNC INT GET_MAX_INVENTORY_STORE()
	INT iInventoryMax = 0
	
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L1) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L2) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L3) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L4) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L5) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L6) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L7) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L8) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L9) iInventoryMax++ ENDIF
	IF IS_MP_KIT_UNLOCKED(PLAYERKIT_ARMOUR_STORE_L10) iInventoryMax++ ENDIF
	
	RETURN iInventoryMax
ENDFUNC

/// PURPOSE: Gives specified supplies (health, armour, parachute and ammo) to the player once they collect the Supply Stash
PROC ADD_SUPPLY_STASH_TO_PLAYER()
	/* 
		Specified values in url:bugstar:7276738 - Setup supply stash
		Full health and armour
		+1 body armour of best rank for player
		+1 parachute
		Ammo for specified guns
	*/
			
	SET_ENTITY_HEALTH(PLAYER_PED_ID(), GET_PED_MAX_HEALTH(PLAYER_PED_ID()))
	NETWORK_RESTORE_LOCAL_PLAYER_ARMOUR()
			
	INT iInventoryMax = GET_MAX_INVENTORY_STORE()			
	INT iSlot = GET_ACTIVE_CHARACTER_SLOT()
	
	SWITCH GET_MAX_UNLOCKED_ARMOUR()
		CASE 1		SET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_1_COUNT, iInventoryMax, iSlot)		BREAK
		CASE 2		SET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_2_COUNT, iInventoryMax, iSlot)		BREAK
		CASE 3		SET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_3_COUNT, iInventoryMax, iSlot)		BREAK
		CASE 4		SET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_4_COUNT, iInventoryMax, iSlot)		BREAK
		CASE 5		SET_MP_INT_CHARACTER_STAT(MP_STAT_MP_CHAR_ARMOUR_5_COUNT, iInventoryMax, iSlot)		BREAK
	ENDSWITCH	
	
	GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1, FALSE, FALSE)

	ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 48)
	ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SMG, 64)
	ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, 120)
	ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MG, 216)
	ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, 32)
	ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, 40)
	ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_FLARE, 8)
	ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 2)
	ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, 2)
	ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADELAUNCHER, 2)
	ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_RPG, 2)
	ADD_AMMO_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MINIGUN, 100)
	
	PRINT_TICKER("FRANK_SUPL_CLCT")	
	
	PRINTLN("[FRANKLIN_SUPPLY_STASH] - ADD_SUPPLY_STASH_TO_PLAYER - Giving HP, armour, parachute and ammo to player")
ENDPROC

/// PURPOSE: Check for player collecting the Supply Stash after it has been created
FUNC BOOL HAS_PLAYER_COLLECTED_SUPPLY_STASH(SUPPLY_STASH_DATA& eSupplyStashData)
	IF IS_NET_PLAYER_OK(PLAYER_ID())
	
		PED_INDEX playerPed = PLAYER_PED_ID()
	
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(playerPed), eSupplyStashData.eSupplyStash.vLocation) < 20.0

			IF IS_ENTITY_AT_COORD(playerPed, eSupplyStashData.eSupplyStash.vLocation, <<1.0, 1.0, 2.0>>)
			AND IS_PED_ON_FOOT(playerPed)				
			AND IS_ENTITY_FACING_COORD_WITHIN_RANGE(playerPed, eSupplyStashData.eSupplyStash.vLocation, 60.0)
				REQUEST_ANIM_DICT(eSupplyStashData.sAnimDict)
				IF HAS_ANIM_DICT_LOADED(eSupplyStashData.sAnimDict)	
					SET_PLAYER_FRANKLIN_SUPPLY_STASH_ANIM_LOADED(PLAYER_ID(), TRUE)
					
					IF GET_SCRIPT_TASK_STATUS(playerPed, SCRIPT_TASK_PLAY_ANIM) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(playerPed, SCRIPT_TASK_PLAY_ANIM) != WAITING_TO_START_TASK
						TASK_PLAY_ANIM(playerPed, eSupplyStashData.sAnimDict, eSupplyStashData.sAnimClip)
						PLAY_SOUND_FRONTEND(-1, "PICK_UP", "HUD_FRONTEND_DEFAULT_SOUNDSET")
						RETURN TRUE						
					ENDIF					
				ENDIF					
			ENDIF	
			
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Cleanup assets used for the Supply Stash
PROC CLEANUP_SUPPLY_STASH(SUPPLY_STASH_DATA& eSupplyStashData)
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FRANK_SUPL_STSH")
		CLEAR_HELP()
	ENDIF
	
	DELETE_NET_ID(eSupplyStashData.niFranklinSupplyStash)
	REMOVE_BLIP(eSupplyStashData.biFranklinSupplyStashBlip)
	eSupplyStashData.bCachedSpawnLocation = FALSE
		
	IF HAS_FRANKLIN_SUPPLY_STASH_ANIM_LOADED(PLAYER_ID())
		REMOVE_ANIM_DICT(eSupplyStashData.sAnimDict)
		SET_PLAYER_FRANKLIN_SUPPLY_STASH_ANIM_LOADED(PLAYER_ID(), FALSE)
	ENDIF
	
	SET_PLAYER_RUNNING_SUPPLY_STASH(PLAYER_ID(), FALSE)
	
	PRINTLN("[FRANKLIN_SUPPLY_STASH] - CLEANUP_SUPPLY_STASH")
ENDPROC

/// PURPOSE: Process and update the Supply Stash system
PROC MAINTAIN_SUPPLY_STASH(SUPPLY_STASH_DATA& eSupplyStashData)	
	SWITCH eSupplyStashData.eSUPPLY_STASH_STATE
	
		CASE SUPPLY_STASH_STATE_IDLE
		
			IF HAS_PLAYER_REQUESTED_SUPPLY_STASH(PLAYER_ID())
				SET_PLAYER_REQUESTED_SUPPLY_STASH(PLAYER_ID(), FALSE)							
				SET_PLAYER_RUNNING_SUPPLY_STASH(PLAYER_ID(), TRUE)
				START_NET_TIMER(g_stSupplyStashSpawnTimer)

				SET_SUPPLY_STASH_STATE(eSupplyStashData, SUPPLY_STASH_STATE_REQUEST)
			ELIF IS_PLAYER_RUNNING_SUPPLY_STASH(PLAYER_ID())
				SET_PLAYER_RUNNING_SUPPLY_STASH(PLAYER_ID(), FALSE)
			ENDIF	
			
		BREAK
		
		
		CASE SUPPLY_STASH_STATE_REQUEST	
		
			IF HAS_SUPPLY_STASH_SPAWN_TIMER_EXPIRED()			
				RESET_NET_TIMER(g_stSupplyStashSpawnTimer)
												
				SET_SUPPLY_STASH_STATE(eSupplyStashData, SUPPLY_STASH_STATE_CREATE)
			ENDIF
								
		BREAK
		
		
		CASE SUPPLY_STASH_STATE_CREATE
							
			IF CREATE_SUPPLY_STASH(eSupplyStashData)
				SET_SUPPLY_STASH_STATE(eSupplyStashData, SUPPLY_STASH_STATE_ACTIVE)
			ENDIF
			
		BREAK
		
		
		CASE SUPPLY_STASH_STATE_ACTIVE
								
			IF HAS_PLAYER_REQUESTED_SUPPLY_STASH(PLAYER_ID())
				PRINTLN("[FRANKLIN_SUPPLY_STASH] - MAINTAIN_SUPPLY_STASH - Player has requested a stash while one is already in the world and not collected. Clean up and restart")
				SET_SUPPLY_STASH_STATE(eSupplyStashData, SUPPLY_STASH_STATE_CLEANUP)
			ENDIF
																			
			IF HAS_PLAYER_COLLECTED_SUPPLY_STASH(eSupplyStashData)
				ADD_SUPPLY_STASH_TO_PLAYER()
				SET_SUPPLY_STASH_STATE(eSupplyStashData, SUPPLY_STASH_STATE_CLEANUP)
			ENDIF
								
		BREAK
	
	
		CASE SUPPLY_STASH_STATE_CLEANUP
		
			CLEANUP_SUPPLY_STASH(eSupplyStashData)
	
			SET_SUPPLY_STASH_STATE(eSupplyStashData, SUPPLY_STASH_STATE_IDLE)
			
		BREAK		
	ENDSWITCH							
ENDPROC

#ENDIF
