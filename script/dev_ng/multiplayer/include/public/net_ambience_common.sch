USING "globals.sch"

FUNC BOOL IS_ENTITY_WITHIN_PROPERTY_INTERIOR(ENTITY_INDEX theEntity,INT iProperty, INT iEntranceType, FLOAT extendRadius = 0.0)
	IF iProperty <= 0
		PRINTLN("IS_ENTITY_WITHIN_PROPERTY_INTERIOR- checking an invalid property: ", iProperty)
		RETURN FALSE
	ENDIF
	VECTOR vEntityCoords = GET_ENTITY_COORDS(theEntity)
	IF (iEntranceType = ENTRANCE_TYPE_GARAGE
	OR iEntranceType = -1)
	AND NOT IS_PROPERTY_CLUBHOUSE(iProperty)
		IF GET_DISTANCE_BETWEEN_COORDS(vEntityCoords,mpProperties[iProperty].garage.vMidPoint) <= 30
	
			IF IS_POINT_IN_ANGLED_AREA(vEntityCoords, mpProperties[iProperty].garage.Bounds.vPos1, 
														mpProperties[iProperty].garage.Bounds.vPos2, mpProperties[iProperty].garage.Bounds.fWidth)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	
	IF iEntranceType != ENTRANCE_TYPE_GARAGE
	//OR iEntranceType = -1
		
		IF GET_DISTANCE_BETWEEN_COORDS(vEntityCoords,mpProperties[iProperty].house.vMidPoint) <= 40
			#IF IS_DEBUG_BUILD

			#ENDIF
			IF IS_POINT_IN_ANGLED_AREA( vEntityCoords, mpProperties[iProperty].house.Bounds[0].vPos1, 
															mpProperties[iProperty].house.Bounds[0].vPos2, mpProperties[iProperty].house.Bounds[0].fWidth)
			OR IS_POINT_IN_ANGLED_AREA( vEntityCoords, mpProperties[iProperty].house.Bounds[1].vPos1, 
															mpProperties[iProperty].house.Bounds[1].vPos2, mpProperties[iProperty].house.Bounds[1].fWidth)
			OR IS_POINT_IN_ANGLED_AREA( vEntityCoords, mpProperties[iProperty].house.Bounds[2].vPos1, 
															mpProperties[iProperty].house.Bounds[2].vPos2, mpProperties[iProperty].house.Bounds[2].fWidth)
															

				RETURN TRUE
				
			ELIF GET_DISTANCE_BETWEEN_COORDS(vEntityCoords, mpProperties[iProperty].house.vCircleBoundPosition) < mpProperties[iProperty].house.fCircleBoundRadius + extendRadius
			AND (vEntityCoords.Z > mpProperties[iProperty].house.Bounds[2].vPos1.Z AND vEntityCoords.Z < mpProperties[iProperty].house.Bounds[2].vPos2.Z)
				RETURN TRUE
			ELSE
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "IS_PLAYER_WITHIN_PROPERTY_INTERIOR: player not within angled area")
			ENDIF
		ELSE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ENTITY_IN_CLUBHOUSE(ENTITY_INDEX theEntity)
	IF IS_ENTITY_WITHIN_PROPERTY_INTERIOR(theEntity,PROPERTY_CLUBHOUSE_1_BASE_A, -1)
	OR IS_ENTITY_WITHIN_PROPERTY_INTERIOR(theEntity,PROPERTY_CLUBHOUSE_7_BASE_B, -1)
 		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ENTITY_IN_IMPOUND_YARD(ENTITY_INDEX EntityID)
	IF IS_ENTITY_IN_ANGLED_AREA(EntityID, <<358.970734,-1597.851685,20.000153>>, <<412.824738,-1640.906860,40.542091>>, 33.250000)
	OR IS_ENTITY_IN_ANGLED_AREA(EntityID, <<406.625153,-1644.242188,20.042194>>, <<417.126831,-1652.936768,41.951004>>, 20.250000)
	OR IS_ENTITY_IN_ANGLED_AREA(EntityID, <<414.675842,-1635.666260,20.092776>>, <<423.541534,-1635.644531,41.043156>>, 15.500000)
		RETURN(TRUE)
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL IS_ENTITY_IN_CAR_MEET_PROPERTY(ENTITY_INDEX theEntity)
	IF IS_ENTITY_IN_AREA(theEntity, <<-1831.3330, 980.1857, -29.8460>>, <<-2225.4680, 1241.2166, -9.4235>>)
 		RETURN TRUE
	ENDIF
	
	// sandbox
	IF IS_ENTITY_IN_AREA(theEntity, <<-1831.3330, 972.858, 23.183785>>, <<-2225.4680, 1248.923, 43.606285>>)
 		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ENTITY_IN_AUTO_SHOP_PROPERTY(ENTITY_INDEX theEntity)
	IF IS_ENTITY_IN_AREA(theEntity, <<-1372.786, 135.814, -100.586>>, <<-1319.589, 169.764, -87.918>>)
 		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_VEHICLE_BE_DELETED_ON_CLEANUP(VEHICLE_INDEX VehicleID)
	VECTOR vPosition = GET_ENTITY_COORDS(VehicleID, FALSE)
	
	IF IS_ENTITY_IN_IMPOUND_YARD(VehicleID)
		PRINTLN("SHOULD_VEHICLE_BE_DELETED_ON_CLEANUP - vehicle in impound yard")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_IN_CLUBHOUSE(VehicleID)
		PRINTLN("SHOULD_VEHICLE_BE_DELETED_ON_CLEANUP - vehicle in clubhouse")
		RETURN TRUE
	ENDIF
	
	IF NOT IS_ENTITY_VISIBLE(VehicleID)
		PRINTLN("SHOULD_VEHICLE_BE_DELETED_ON_CLEANUP - vehicle not visible")
		RETURN TRUE
	ENDIF
	
	IF (vPosition.z < -89.0)
		PRINTLN("SHOULD_VEHICLE_BE_DELETED_ON_CLEANUP - vehicle z axis less than -89.0")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_IN_AREA(VehicleID, <<1097.5347, -3016.0115, -40.7658>>, <<1109.2977, -2983.6902, -34.1882>>, FALSE, FALSE)
		PRINTLN("SHOULD_VEHICLE_BE_DELETED_ON_CLEANUP - in ristricted area")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(VehicleID, <<924.536,-18.629,75.715>>, <<942.786, 11.196, 83.665>>, 18.475, FALSE, FALSE) // casino garage entrance
		PRINTLN("SHOULD_VEHICLE_BE_DELETED_ON_CLEANUP - vehicle in casino garage entance")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_IN_AREA(VehicleID,<<1333.9764, 176.982, -51.1537>>,<<1427.7905, 260.917, -43.4137>>,FALSE) //casino valet garage
		PRINTLN("SHOULD_VEHICLE_BE_DELETED_ON_CLEANUP - vehicle in casino valet entrance")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(VehicleID, <<-2179.408447, 1070.749390, -28.909296>>, <<-2178.830811, 1151.052002, -17.204411>>, 95.25) // casino garage entrance
		PRINTLN("SHOULD_VEHICLE_BE_DELETED_ON_CLEANUP - vehicle in casino garage entrance")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_IN_AUTO_SHOP_PROPERTY(VehicleID)
		PRINTLN("SHOULD_VEHICLE_BE_DELETED_ON_CLEANUP - vehicle in auto shop")
		RETURN TRUE
	ENDIF
	
	IF IS_ENTITY_IN_CAR_MEET_PROPERTY(VehicleID)
		PRINTLN("SHOULD_VEHICLE_BE_DELETED_ON_CLEANUP - vehicle in car meet")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

