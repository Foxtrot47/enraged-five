USING "globals.sch"

CONST_INT ADD_WIDGETS_FOR_SPAWNING		0

#IF IS_DEBUG_BUILD

USING "net_debug.sch"

PROC DRAW_DEBUG_SPAWN_TEXT(STRING text, VECTOR VecCoors, int Red = 0, int Green = 0, int Blue = 255, int alpha_param = 255)
	IF NOT (g_SpawnData.bDrawVectorMap)
		DRAW_DEBUG_TEXT(text, VecCoors, Red, Green, Blue, alpha_param)
	ELSE
		DRAW_VECTOR_MAP_TEXT(text, VecCoors, TRUE, Red, Green, Blue, alpha_param) 
	ENDIF
ENDPROC

PROC DRAW_VECTOR_MAP_ARROW(VECTOR vCoord, FLOAT fHeading, FLOAT fSize, INT r, INT g, INT b, INT a)

	fHeading += 180.0

	VECTOR vToForward = <<0.0, fSize, 0.0>>
	VECTOR vToBackRight = <<fSize*0.5, fSize*-0.5, 0.0>>
	VECTOR vToBackLeft = <<fSize*-0.5, fSize*-0.5, 0.0>>
	
	RotateVec(vToForward, <<0.0, 0.0, fHeading*-1.0>>)
	RotateVec(vToBackRight, <<0.0, 0.0, fHeading*-1.0>>)
	RotateVec(vToBackLeft, <<0.0, 0.0, fHeading*-1.0>>)
	
	DRAW_VECTOR_MAP_POLY (vCoord+vToBackLeft, vCoord+vToForward, vCoord+vToBackRight, r,g,b,a)

ENDPROC

PROC DEBUG_DRAW_FACE(VECTOR vCoord1, VECTOR vCoord2, VECTOR vCoord3, VECTOR vCoord4, INT r, INT g, INT b, INT a)
	// draw other side of poly a darker colour
//	INT r2,g2,b2,a2
//	r2 = ROUND(r/2.0)
//	g2 = ROUND(g/2.0)
//	b2 = ROUND(b/2.0)
//	a2 = ROUND(a/2.0)
	
	IF NOT (g_SpawnData.bDrawVectorMap)
		DRAW_DEBUG_POLY (vCoord1, vCoord2, vCoord3 , r,g,b,a)
		DRAW_DEBUG_POLY (vCoord1, vCoord3, vCoord4 , r,g,b,a) 	
		
		DRAW_DEBUG_POLY (vCoord3, vCoord2, vCoord1 , r,g,b,a) 
		DRAW_DEBUG_POLY (vCoord4, vCoord3, vCoord1 , r,g,b,a) 
	
	ELSE
	
		DRAW_VECTOR_MAP_POLY (vCoord1, vCoord2, vCoord3 , r,g,b,a)
		DRAW_VECTOR_MAP_POLY (vCoord1, vCoord3, vCoord4 , r,g,b,a) 	
		
		DRAW_VECTOR_MAP_POLY (vCoord3, vCoord2, vCoord1 , r,g,b,a) 
		DRAW_VECTOR_MAP_POLY (vCoord4, vCoord3, vCoord1 , r,g,b,a)
	ENDIF
	
ENDPROC

PROC DEBUG_DRAW_ANGLED_BOX(VECTOR vCorner1, VECTOR vCorner2, VECTOR vCorner3, VECTOR vCorner4, VECTOR vCorner5, VECTOR vCorner6, VECTOR vCorner7, VECTOR vCorner8, INT r, INT g, INT b, INT a)
	DEBUG_DRAW_FACE(vCorner1, vCorner2, vCorner3, vCorner4, ROUND(r/(1.0 + (0* g_SpawnData.fFaceOffset))), ROUND(g/(1.0 + (0* g_SpawnData.fFaceOffset))), ROUND(b/(1.0 + (0* g_SpawnData.fFaceOffset))), a)
	DEBUG_DRAW_FACE(vCorner2, vCorner6, vCorner7, vCorner3, ROUND(r/(1.0 + (1* g_SpawnData.fFaceOffset))), ROUND(g/(1.0 + (1* g_SpawnData.fFaceOffset))), ROUND(b/(1.0 + (1* g_SpawnData.fFaceOffset))), a)
	DEBUG_DRAW_FACE(vCorner3, vCorner7, vCorner8, vCorner4, ROUND(r/(1.0 + (2* g_SpawnData.fFaceOffset))), ROUND(g/(1.0 + (2* g_SpawnData.fFaceOffset))), ROUND(b/(1.0 + (2* g_SpawnData.fFaceOffset))), a)
	DEBUG_DRAW_FACE(vCorner1, vCorner4, vCorner8, vCorner5, ROUND(r/(1.0 + (3* g_SpawnData.fFaceOffset))), ROUND(g/(1.0 + (3* g_SpawnData.fFaceOffset))), ROUND(b/(1.0 + (3* g_SpawnData.fFaceOffset))), a)
	DEBUG_DRAW_FACE(vCorner1, vCorner5, vCorner6, vCorner2, ROUND(r/(1.0 + (4* g_SpawnData.fFaceOffset))), ROUND(g/(1.0 + (4* g_SpawnData.fFaceOffset))), ROUND(b/(1.0 + (4* g_SpawnData.fFaceOffset))), a)
	DEBUG_DRAW_FACE(vCorner5, vCorner8, vCorner7, vCorner6, ROUND(r/(1.0 + (5* g_SpawnData.fFaceOffset))), ROUND(g/(1.0 + (5* g_SpawnData.fFaceOffset))), ROUND(b/(1.0 + (5* g_SpawnData.fFaceOffset))), a)
ENDPROC

PROC DRAW_DEBUG_ANGLED_AREA(VECTOR vCoord1, VECTOR vCoord2, FLOAT fWidth, INT r, INT g, INT b, INT a)

	IF (vCoord1.z = vCoord2.z)
		vCoord2.z += 0.01
	ENDIF

	// find corner points of angled area

	VECTOR vec = vCoord1 - vCoord2
	VECTOR vCross = CROSS_PRODUCT(vec, <<vec.x, vec.y, 0.0>>)

	vCross /= VMAG(vCross)
	vCross *= fWidth * 0.5
	
	FLOAT fMinZ, fMaxX
	
	IF (vCoord1.z > vCoord2.z)
		fMinZ = vCoord2.z
		fMaxX = vCoord1.z
	ELSE
		fMinZ = vCoord1.z
		fMaxX = vCoord2.z
	ENDIF
	
	VECTOR vCorner[8]
	
	vCorner[0] = <<vCoord1.x, vCoord1.y, fMinZ>> + vCross
	vCorner[1] = <<vCoord1.x, vCoord1.y, fMinZ>> - vCross
	vCorner[2] = <<vCoord1.x, vCoord1.y, fMaxX>> - vCross	
	vCorner[3] = <<vCoord1.x, vCoord1.y, fMaxX>> + vCross	
	vCorner[4] = <<vCoord2.x, vCoord2.y, fMinZ>> + vCross
	vCorner[5] = <<vCoord2.x, vCoord2.y, fMinZ>> - vCross
	vCorner[6] = <<vCoord2.x, vCoord2.y, fMaxX>> - vCross
	vCorner[7] = <<vCoord2.x, vCoord2.y, fMaxX>> + vCross	
	
//	DRAW_DEBUG_SPAWN_TEXT("corner 1", vCorner[0], 255, 255, 255, 255)
//	DRAW_DEBUG_SPAWN_TEXT("corner 2", vCorner[1], 255, 255, 255, 255)
//	DRAW_DEBUG_SPAWN_TEXT("corner 3", vCorner[2], 255, 255, 255, 255)
//	DRAW_DEBUG_SPAWN_TEXT("corner 4", vCorner[3], 255, 255, 255, 255)
//	DRAW_DEBUG_SPAWN_TEXT("corner 5", vCorner[4], 255, 255, 255, 255)
//	DRAW_DEBUG_SPAWN_TEXT("corner 6", vCorner[5], 255, 255, 255, 255)
//	DRAW_DEBUG_SPAWN_TEXT("corner 7", vCorner[6], 255, 255, 255, 255)
//	DRAW_DEBUG_SPAWN_TEXT("corner 8", vCorner[7], 255, 255, 255, 255)
	
	IF NOT (g_SpawnData.bDrawCodeAngleAreas)
	
		
		DRAW_MARKER(MARKER_SPHERE, vCoord1, <<0,0,0>>, <<0,0,0>>, <<g_SpawnData.fDebugSphereScale,g_SpawnData.fDebugSphereScale,g_SpawnData.fDebugSphereScale>>, b,g,r,255)
		DRAW_MARKER(MARKER_SPHERE, vCoord2, <<0,0,0>>, <<0,0,0>>, <<g_SpawnData.fDebugSphereScale,g_SpawnData.fDebugSphereScale,g_SpawnData.fDebugSphereScale>>, g,r,b,255)
	
		DEBUG_DRAW_ANGLED_BOX(vCorner[0], vCorner[1], vCorner[2], vCorner[3], vCorner[4], vCorner[5], vCorner[6], vCorner[7], r, g, b, a)
	ELSE
		IS_POINT_IN_ANGLED_AREA ( vCoord1, vCoord1, vCoord2, fWidth, TRUE, TRUE)
	ENDIF

ENDPROC

/// PURPOSE:
///    Draw a 3-axis rotated debug box at an area with given parameters. 
/// PARAMS:
///    vCoord - centre of the box 
///    vForward - forward vector for the box, use <<0,1,0>> for default
///    vUp - UP vector (for Roll angle), use <<0,0,1>> for default
///    fLength - length (Y-axis aligned, in m)
///    fWidth - width (X-axis aligned, in m)
///    fHeight - height (Z-axis alignend, in m)
///    r - red colour component, 0-255
///    g - green colour component, 0-255
///    b - blue colour component, 0-255
///    a - transparaency component, 0-255
PROC DRAW_DEBUG_ROTATED_AREA(VECTOR vCoord,VECTOR vForward ,VECTOR vUp, FLOAT fLength= 1.0, FLOAT fWidth=1.0, FLOAT fHeight=1.0, INT r = 255, INT g = 255, INT b = 255, INT a=255)
	//Find front and rear points
	VECTOR vCoord1 = vCoord + fLength/2*vForward
	VECTOR vCoord2 = vCoord - fLength/2*vForward
	// find corner points of rotated area
	VECTOR vec = vCoord1 - vCoord2
	VECTOR vCross2 = vUp //CROSS_PRODUCT(vForward,CROSS_PRODUCT(vForward,<<0,0,1>>))
	VECTOR vCross = CROSS_PRODUCT(vec, vUp)
	
	vCross /= VMAG(vCross)
	vCross *= fWidth * 0.5
	
	vCross2 /= VMAG(vCross2)
	vCross2 *= fHeight * 0.5
	
	VECTOR vCorner[8]
	vCorner[0] = <<vCoord1.x, vCoord1.y, vCoord1.z>> + vCross+vCross2
	vCorner[1] = <<vCoord1.x, vCoord1.y, vCoord1.z>> + vCross+-vCross2
	vCorner[2] = <<vCoord1.x, vCoord1.y, vCoord1.z>> - vCross-vCross2
	vCorner[3] = <<vCoord1.x, vCoord1.y, vCoord1.z>> - vCross+vCross2
	vCorner[4] = <<vCoord2.x, vCoord2.y, vCoord2.z>> + vCross+vCross2
	vCorner[5] = <<vCoord2.x, vCoord2.y, vCoord2.z>> + vCross-vCross2
	vCorner[6] = <<vCoord2.x, vCoord2.y, vCoord2.z>> - vCross-vCross2
	vCorner[7] = <<vCoord2.x, vCoord2.y, vCoord2.z>> - vCross+vCross2
	
//	DRAW_DEBUG_SPAWN_TEXT("corner 1", vCorner[0], 255, 255, 255, 255)
//	DRAW_DEBUG_SPAWN_TEXT("corner 2", vCorner[1], 255, 255, 255, 255)
//	DRAW_DEBUG_SPAWN_TEXT("corner 3", vCorner[2], 255, 255, 255, 255)
//	DRAW_DEBUG_SPAWN_TEXT("corner 4", vCorner[3], 255, 255, 255, 255)
//	DRAW_DEBUG_SPAWN_TEXT("corner 5", vCorner[4], 255, 255, 255, 255)
//	DRAW_DEBUG_SPAWN_TEXT("corner 6", vCorner[5], 255, 255, 255, 255)
//	DRAW_DEBUG_SPAWN_TEXT("corner 7", vCorner[6], 255, 255, 255, 255)
//	DRAW_DEBUG_SPAWN_TEXT("corner 8", vCorner[7], 255, 255, 255, 255)
	
	IF NOT (g_SpawnData.bDrawCodeAngleAreas)
		DEBUG_DRAW_ANGLED_BOX(vCorner[0], vCorner[1], vCorner[2], vCorner[3], vCorner[4], vCorner[5], vCorner[6], vCorner[7], r, g, b, a)
	ELSE
		IS_POINT_IN_ANGLED_AREA ( vCoord1, vCoord1, vCoord2, fWidth, TRUE, TRUE)
	ENDIF

ENDPROC

PROC DISPLAY_TEXT_WITH_VECTOR(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pTextLabel, VECTOR VectorToDisplay, INT NumberOfDecimalPlaces)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_FLOAT(VectorToDisplay.x, NumberOfDecimalPlaces)
		ADD_TEXT_COMPONENT_FLOAT(VectorToDisplay.y, NumberOfDecimalPlaces)
		ADD_TEXT_COMPONENT_FLOAT(VectorToDisplay.z, NumberOfDecimalPlaces)
	END_TEXT_COMMAND_DISPLAY_TEXT(DisplayAtX, DisplayAtY)
ENDPROC

PROC DRAW_DEBUG_SPAWN_POINT(VECTOR vPos, FLOAT fHeading, HUD_COLOURS HColour, FLOAT fZOffset=0.0, FLOAT fScale=1.0)
	
	INT r,g,b,a
	
	vPos.z += fZOffset
	
	
	fHeading *= -1.0
	
	GET_HUD_COLOUR(HColour, r,g,b,a)

	fHeading += 180.0
	
	IF NOT (g_SpawnData.bDrawVectorMap)
		DRAW_MARKER(MARKER_CHEVRON_1, vPos, <<0,0,0.0>>, <<90.0, 0.0, fHeading>>, (<<1.0, 1.0, 0.5>> * fScale), r,g,b,a, FALSE, FALSE, INT_TO_ENUM(EULER_ROT_ORDER, g_SpawnData.iEuler) )
	ELSE	
		DRAW_VECTOR_MAP_ARROW(vPos, fHeading, fScale, r,g,b,a)
	ENDIF
	
ENDPROC

PROC DRAW_DEBUG_FLOAT_CIRCLE(VECTOR vCentre, FLOAT fRadius, INT r, INT g, INT b, INT a) //, INT iNumberOfSegments=16)

	FLOAT x1, y1, x2, y2
	FLOAT fAngle
	INT i
	// draw other side of poly a darker colour
	INT r2,g2,b2,a2
	r2 = ROUND(r/2.0)
	g2 = ROUND(g/2.0)
	b2 = ROUND(b/2.0)
	a2 = ROUND(a/2.0)	
	
	fAngle = 360.0 / TO_FLOAT(16)
	
	REPEAT 16 i
		x1 = (SIN((fAngle * TO_FLOAT(i)))) * fRadius
		y1 = (COS((fAngle * TO_FLOAT(i)))) * fRadius
		x2 = (SIN((fAngle * TO_FLOAT(i+1)))) * fRadius
		y2 = (COS((fAngle * TO_FLOAT(i+1)))) * fRadius
		DRAW_DEBUG_POLY (vCentre, <<vCentre.x + x1, vCentre.y + y1, vCentre.z>> , <<vCentre.x + x2, vCentre.y + y2, vCentre.z>>, r,g,b,a)
		DRAW_DEBUG_POLY (vCentre, <<vCentre.x + x2, vCentre.y + y2, vCentre.z>>, <<vCentre.x + x1, vCentre.y + y1, vCentre.z>> , r2,g2,b2,a2)
	ENDREPEAT
	
	IF NOT (g_SpawnData.bDrawVectorMap)
		DRAW_MARKER(MARKER_CYLINDER, vCentre, <<0,0,0>>, <<0,0,0>>, <<fRadius*2.0, fRadius*2.0, 2.0>>, r,g,b,a)
	ELSE
		DRAW_VECTOR_MAP_CIRCLE (vCentre, fRadius, r,g,b,a) 
	ENDIF
	
ENDPROC


PROC SafeMinMax(VECTOR &vMin, VECTOR &vMax)
	VECTOR vStoredMin
	VECTOR vStoredMax
	IF (vMin.x <= vMax.x)
		vStoredMin.x = vMin.x
		vStoredMax.x = vMax.x
	ELSE
		vStoredMin.x = vMax.x
		vStoredMax.x = vMin.x
	ENDIF
	IF (vMin.y <= vMax.y)
		vStoredMin.y = vMin.y
		vStoredMax.y = vMax.y
	ELSE
		vStoredMin.y = vMax.y
		vStoredMax.y = vMin.y
	ENDIF
	IF (vMin.z <= vMax.z)
		vStoredMin.z = vMin.z
		vStoredMax.z = vMax.z
	ELSE
		vStoredMin.z = vMax.z
		vStoredMax.z = vMin.z
	ENDIF	
	vMin = vStoredMin
	vMax = vStoredMax
ENDPROC

PROC DRAW_DEBUG_BOX_FOR_RELEASE(VECTOR vMin, VECTOR vMax, INT r, INT g, INT b, INT a) //, INT iNumberOfSegments=16)

	FLOAT fTemp 
	
	SafeMinMax(vMin, vMax)	
	
	fTemp = vMax.z - vMin.z

	DRAW_MARKER(MARKER_CYLINDER, <<vMin.x, vMin.y, vMin.z>>, <<0,0,0>>, <<0,0,0>>, <<0.05, 0.05, fTemp>>, r,g,b,a)
	DRAW_MARKER(MARKER_CYLINDER, <<vMin.x, vMax.y, vMin.z>>, <<0,0,0>>, <<0,0,0>>, <<0.05, 0.05, fTemp>>, r,g,b,a)
	DRAW_MARKER(MARKER_CYLINDER, <<vMax.x, vMin.y, vMin.z>>, <<0,0,0>>, <<0,0,0>>, <<0.05, 0.05, fTemp>>, r,g,b,a)
	DRAW_MARKER(MARKER_CYLINDER, <<vMax.x, vMax.y, vMin.z>>, <<0,0,0>>, <<0,0,0>>, <<0.05, 0.05, fTemp>>, r,g,b,a)

	
	VECTOR vMid = (vMin + vMax)/2.0
	
	
	
	//DRAW_DEBUG_BOX(vMin, vMax, r,g,b,a)
	
	DRAW_DEBUG_ANGLED_AREA(<<vMin.x, vMid.y, vMin.z>>, <<vMax.x, vMid.y, vMax.z>>, vMax.y-vMin.y, r,g,b,a)
	
ENDPROC


PROC MAKE_SPAWN_AREA_AROUND_PLAYER(SPAWN_AREA &SPArea)
	SWITCH SPArea.iShape
		CASE SPAWN_AREA_SHAPE_CIRCLE 
			SPArea.vCoords1 = GET_PLAYER_COORDS(PLAYER_ID())
			IF (SPArea.fFloat = 0.0)
				SPArea.fFloat = 20.0
			ENDIF
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX		
			SPArea.vCoords1 = GET_PLAYER_COORDS(PLAYER_ID()) - <<20.0, 20.0, 20.0>>
			SPArea.vCoords2 = GET_PLAYER_COORDS(PLAYER_ID()) + <<20.0, 20.0, 20.0>>						
		BREAK
		CASE SPAWN_AREA_SHAPE_ANGLED	
			SPArea.vCoords1 = GET_PLAYER_COORDS(PLAYER_ID()) - <<20.0, 20.0, 20.0>>
			SPArea.vCoords2 = GET_PLAYER_COORDS(PLAYER_ID()) + <<20.0, 20.0, 20.0>>	
			IF (SPArea.fFloat = 0.0)
				SPArea.fFloat = 20.0
			ENDIF
		BREAK
	ENDSWITCH	
ENDPROC

PROC DRAW_DEBUG_SPAWN_AREA(STRING strName, SPAWN_AREA& SpArea, INT r, INT g, INT b, INT a)
	VECTOR vCentre
	SWITCH SpArea.iShape				
		CASE SPAWN_AREA_SHAPE_CIRCLE 	
			DRAW_DEBUG_FLOAT_CIRCLE(SpArea.vCoords1, SpArea.fFloat, r,g,b,a)
			DRAW_DEBUG_SPAWN_TEXT(strName, SpArea.vCoords1, r,g,b,a)
		BREAK
		CASE SPAWN_AREA_SHAPE_BOX		
			DRAW_DEBUG_BOX_FOR_RELEASE(SpArea.vCoords1, SpArea.vCoords2, r,g,b,a)
			vCentre = (SpArea.vCoords1 + SpArea.vCoords2) * 0.5
			DRAW_DEBUG_SPAWN_TEXT(strName, vCentre, r,g,b,a)
		BREAK
		CASE SPAWN_AREA_SHAPE_ANGLED	
			DRAW_DEBUG_ANGLED_AREA(SpArea.vCoords1, SpArea.vCoords2, SpArea.fFloat, r,g,b,a)
			vCentre = (SpArea.vCoords1 + SpArea.vCoords2) * 0.5
			DRAW_DEBUG_SPAWN_TEXT(strName, vCentre, r,g,b,a)
		BREAK
	ENDSWITCH
	
	IF (SpArea.bSetAroundPlayer)
		IF IS_PLAYER_PLAYING(PLAYER_ID())			
			MAKE_SPAWN_AREA_AROUND_PLAYER(SpArea)								
		ENDIF
		SpArea.bSetAroundPlayer = FALSE
	ENDIF		
	
ENDPROC


PROC DRAW_DEBUG_PROBLEM_AREA(STRING strName, PROBLEM_AREA& PrbArea, INT r, INT g, INT b, INT a)
	VECTOR vCentre
	DRAW_DEBUG_ANGLED_AREA(PrbArea.vCoords1, PrbArea.vCoords2, PrbArea.fFloat, r,g,b,a)
	vCentre = (PrbArea.vCoords1 + PrbArea.vCoords2) * 0.5
	DRAW_DEBUG_SPAWN_TEXT(strName, vCentre, r,g,b,a)

	
	IF (PrbArea.bSetAroundPlayer)
		IF IS_PLAYER_PLAYING(PLAYER_ID())			
			PrbArea.vCoords1 = GET_PLAYER_COORDS(PLAYER_ID()) - <<1.0, 1.0, 1.0>>
			PrbArea.vCoords2 = GET_PLAYER_COORDS(PLAYER_ID()) + <<1.0, 1.0, 1.0>>	
			IF (PrbArea.fFloat = 0.0)
				PrbArea.fFloat = 1.0
			ENDIF								
		ENDIF
		PrbArea.bSetAroundPlayer = FALSE
	ENDIF		
	
ENDPROC

PROC DrawMPSpawnPoints(MP_SPAWN_POINT &SpawnPoints[], STRING strName)

	INT i
	TEXT_LABEL_63 str
	HUD_COLOURS hudColArrow = HUD_COLOUR_ORANGE
	
	REPEAT COUNT_OF(SpawnPoints) i
		IF g_SpawnData.bCurrentlyDraggingSpawnPoint
		AND g_SpawnData.iSelectedSpawnPointIndex = i
			hudColArrow = HUD_COLOUR_WHITE
		ENDIF
		DRAW_DEBUG_SPAWN_POINT(SpawnPoints[i].Pos, SpawnPoints[i].Heading, hudColArrow, 1.0)	
		str = strName
		str += " "
		str += i
		IF NOT g_SpawnData.bCurrentlyDraggingSpawnPoint
			DRAW_DEBUG_SPAWN_TEXT(str, SpawnPoints[i].Pos, 255, 255, 255, 255)
		ENDIF
	ENDREPEAT

ENDPROC


PROC DrawMPVehicleSpawnPoints(MP_VEHICLE_SPAWN &VehicleSpawn[], STRING strName)

	INT i
	TEXT_LABEL_63 str
	
	REPEAT COUNT_OF(VehicleSpawn) i
		DRAW_DEBUG_SPAWN_POINT(VehicleSpawn[i].Pos, VehicleSpawn[i].Heading, HUD_COLOUR_PURPLELIGHT, 1.0)	
		str = strName
		str += " "
		str += i
		str += " "
		str += GET_DISPLAY_NAME_FROM_VEHICLE_MODEL (VehicleSpawn[i].Model)
		DRAW_DEBUG_SPAWN_TEXT(str, VehicleSpawn[i].Pos, 255, 255, 255, 255)
	ENDREPEAT

ENDPROC

PROC ToggleSpawningAreaVisible(BOOL bSet)
	INT i
	INT j
	IF (bSet)
		PRINT_HELP("SHOW_SPAWN_ON")
		IF NOT (g_SpawnData.bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)		
			g_SpawnData.bDebugLinesActive = TRUE
		ENDIF				
	ELSE
		PRINT_HELP("SHOW_SPAWN_OFF")
	ENDIF
	
	REPEAT MAX_NUMBER_OF_MISSION_SPAWN_AREAS i	
		g_SpawnData.MissionSpawnDetails.SpawnArea[i].bShow = bSet
	ENDREPEAT
	REPEAT MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS i
		g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i].bShow = bSet
	ENDREPEAT
	
	g_SpawnData.MissionSpawnDetails.bToggleSpawnView = bSet	
	g_SpawnData.bShowCustomSpawnPoints = bSet
	g_SpawnData.bShowAllForbiddenAreas = bSet
	g_SpawnData.bShowAllSpawningSectors = bSet
	g_SpawnData.bRenderSpecificCoords = bSet
	g_SpawnData.bShowAllProblemNodeAreas = bSet
	REPEAT NUMBER_OF_SPAWNING_SECTORS i	
		REPEAT g_iTotalNodesProblemAreasForSector[i] j
			ProblemArea_Nodes[i][j].bShow = bSet
		ENDREPEAT
	ENDREPEAT	
	REPEAT NUMBER_OF_PROBLEM_NODESEARCH_AREAS i	
		ProblemArea_NodeSearch[i].bShow = bSet
	ENDREPEAT
	REPEAT NUMBER_OF_SPAWNING_SECTORS i	
		REPEAT g_iTotalForbiddenProblemAreasForSector[i] j
			ProblemArea_Forbidden[i][j].bShow = bSet
		ENDREPEAT
	ENDREPEAT
	REPEAT MAX_NUMBER_OF_GLOBAL_EXCLUSION_AREAS i
		GlobalExclusionArea[i].ExclusionArea.bShow = bSet
	ENDREPEAT
	REPEAT NUMBER_OF_MIN_AIRPLANE_HEIGHT_AREAS i
		ProblemArea_MinAirplaneHeight[i].bShow = bSet
	ENDREPEAT
	REPEAT NUMBER_OF_CUSTOM_VEHICLE_NODE_AREAS i
		ProblemArea_CustomVehicleNodes[i].bShow = bSet
	ENDREPEAT
	REPEAT TOTAL_NUMBER_OF_RESPAWN_HOSPITALS i
		SpawnAreas_Hospitals[i].bShow = bSet
	ENDREPEAT
	REPEAT TOTAL_NUMBER_OF_RESPAWN_POLICE_STATIONS i
		SpawnAreas_PoliceStation[i].bShow = bSet
	ENDREPEAT
	REPEAT TOTAL_NUMBER_OF_RESPAWN_HOTELS i
		SpawnAreas_Hotels[i].bShow = bSet
	ENDREPEAT
	
ENDPROC


PROC CREATE_GROUP_WARP_WIDGET()

	INT iPlayer
	INT i
	TEXT_LABEL_63 str

	START_WIDGET_GROUP("Group Warping")
		ADD_WIDGET_BOOL("bRenderPositions", g_SpawnData.bRenderGroupWarpPositions)
		
		START_WIDGET_GROUP("g_SpawnData")
			ADD_WIDGET_INT_SLIDER("iGroupWarpState", g_SpawnData.iGroupWarpState, -1, 99, 1)
			ADD_WIDGET_INT_SLIDER("iGroupWarpStatePVCreation", g_SpawnData.iGroupWarpStatePVCreation, -1, 99, 1)
			ADD_WIDGET_INT_SLIDER("iGroupWarpPlayerToProcess", g_SpawnData.iGroupWarpPlayerToProcess, -1, 99, 1)
			ADD_WIDGET_VECTOR_SLIDER("vGroupWarpCoords", g_SpawnData.vGroupWarpCoords, -99999.9, 99999.9, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("fGroupWarpHeading", g_SpawnData.fGroupWarpHeading, -99999.9, 99999.9, 0.01)
			ADD_WIDGET_BOOL("bUseExactGroupWarpCoords", g_SpawnData.bUseExactGroupWarpCoords)
			ADD_WIDGET_INT_SLIDER("iGroupWarp_Bitset", g_SpawnData.iGroupWarp_Bitset, 0, HIGHEST_INT, 1)
			START_WIDGET_GROUP("Debug")
				ADD_WIDGET_BOOL("bFailedToGetGroupWarpArea", g_SpawnData.bFailedToGetGroupWarpArea)
				ADD_WIDGET_INT_SLIDER("iTest_GroupWarpLocation", g_SpawnData.iTest_GroupWarpLocation, -1, 99, 1)
				ADD_WIDGET_BOOL("bStartGroupWarpWithAllPlayersInSession", g_SpawnData.bStartGroupWarpWithAllPlayersInSession)
				ADD_WIDGET_BOOL("bStartDefunctBaseGroupWarp", g_SpawnData.bStartDefunctBaseGroupWarp)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Placement Vehicles")
		
			ADD_WIDGET_BOOL("bCreatePlacementVehicles", g_SpawnData.bCreatePlacementVehicles)
			ADD_WIDGET_BOOL("bOutputPlacementVehicles", g_SpawnData.bOutputPlacementVehicles)
			ADD_WIDGET_BOOL("bOutputPlacementFormation", g_SpawnData.bOutputPlacementFormation)
			ADD_WIDGET_BOOL("bLockTargetVehicle", g_SpawnData.bLockTargetVehicle)
			ADD_WIDGET_INT_SLIDER("iLoadPlacementFormation", g_SpawnData.iLoadPlacementFormation, -1, 99, 1)
			ADD_WIDGET_INT_SLIDER("iPlacementVehicle_GroupWarpLocation", g_SpawnData.iPlacementVehicle_GroupWarpLocation, ENUM_TO_INT(GROUP_WARP_LOCATION_NULL), ENUM_TO_INT(GROUP_WARP_LOCATION_END) - 1, 1)
			ADD_WIDGET_INT_SLIDER("iPlacementVehicle_GroupWarpArea", g_SpawnData.iPlacementVehicle_GroupWarpArea, 0, 0, 1)
			REPEAT 8 i
				str = "vPlacementOffset["
				str += i
				str += "]"
				ADD_WIDGET_VECTOR_SLIDER(str, g_SpawnData.vPlacementOffset[i], -99999.9, 99999.9, 0.01)	
				
				str = "fPlacementHeadingOffset["
				str += i
				str += "]"
				ADD_WIDGET_FLOAT_SLIDER(str, g_SpawnData.fPlacementHeadingOffset[i], -99999.9, 99999.9, 0.01)	
			ENDREPEAT
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Player BD")
			
			REPEAT NUM_NETWORK_PLAYERS iPlayer
								
				str = "Player "
				str += iPlayer
				
				START_WIDGET_GROUP(str)
					
					ADD_WIDGET_BOOL("bReadyToStart", GlobalplayerBD[iPlayer].GroupWarpBD.bReadyToStart)
					ADD_WIDGET_BOOL("bAbandon", GlobalplayerBD[iPlayer].GroupWarpBD.bReadyToStart)
					ADD_WIDGET_INT_SLIDER("iUniqueID", GlobalplayerBD[iPlayer].GroupWarpBD.iUniqueID, LOWEST_INT, HIGHEST_INT, 1)
					//ADD_WIDGET_INT_SLIDER("eGroupWarpLocation", GlobalplayerBD[iPlayer].GroupWarpBD.eGroupWarpLocation, LOWEST_INT, HIGHEST_INT, 1)
					REPEAT NUM_NETWORK_PLAYERS i
						str = "iPlayerNamesHash["
						str += i
						str += "]"
						ADD_WIDGET_INT_SLIDER(str, GlobalplayerBD[iPlayer].GroupWarpBD.iPlayerNamesHash[i], LOWEST_INT, HIGHEST_INT, 1)						
					ENDREPEAT
					
				STOP_WIDGET_GROUP()
				
			ENDREPEAT
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Server BD")

			REPEAT NUM_NETWORK_PLAYERS iPlayer
				
				str = "Player "
				str += iPlayer
				
				START_WIDGET_GROUP(str)
					ADD_WIDGET_INT_SLIDER("iUniqueID", GlobalServerBD.GroupWarpBD.iUniqueID[iPlayer], LOWEST_INT, HIGHEST_INT, 1)
					//ADD_WIDGET_INT_SLIDER("eGroupWarpLocation", GlobalServerBD.GroupWarpBD.eGroupWarpLocation[iPlayer], LOWEST_INT, HIGHEST_INT, 1)
					ADD_WIDGET_INT_SLIDER("iArea", GlobalServerBD.GroupWarpBD.iArea[iPlayer], LOWEST_INT, HIGHEST_INT, 1)
				STOP_WIDGET_GROUP()
				
			ENDREPEAT	
	
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()

ENDPROC


PROC CREATE_MP_SPAWN_POINTS_WIDGET(MP_SPAWN_POINT &SpawnPoints[], STRING strName)
	INT i
	TEXT_LABEL_63 str
	START_WIDGET_GROUP(strName)
		REPEAT COUNT_OF(SpawnPoints) i
			str = "pos "
			str += i
			ADD_WIDGET_VECTOR_SLIDER(str, SpawnPoints[i].Pos, -9999.9, 9999.9, 0.01)
			str = "heading "
			str += i
			ADD_WIDGET_FLOAT_SLIDER(str, SpawnPoints[i].Heading, 0.0, 360.0, 1.0)
		ENDREPEAT
	STOP_WIDGET_GROUP()
ENDPROC
PROC CREATE_MP_VEHICLE_SPAWN_POINTS_WIDGET(MP_VEHICLE_SPAWN &SpawnPoints[], STRING strName)
	INT i
	TEXT_LABEL_63 str
	START_WIDGET_GROUP(strName)
		REPEAT COUNT_OF(SpawnPoints) i
			str = "pos "
			str += i
			ADD_WIDGET_VECTOR_SLIDER(str, SpawnPoints[i].Pos, -9999.9, 9999.9, 0.01)
			str = "heading "
			str += i
			ADD_WIDGET_FLOAT_SLIDER(str, SpawnPoints[i].Heading, 0.0, 360.0, 1.0)
		ENDREPEAT
	STOP_WIDGET_GROUP()
ENDPROC

PROC ADD_WIDGET_FOR_SPAWN_AREA(SPAWN_AREA &SpArea)
	ADD_WIDGET_BOOL("Spawn Area Is Active", SpArea.bIsActive)
	ADD_WIDGET_BOOL("Show Spawn Area", SpArea.bShow)
	ADD_WIDGET_BOOL("Make around player", SpArea.bSetAroundPlayer)		
	ADD_WIDGET_BOOL("bConsiderCentrePointAsValid", SpArea.bConsiderCentrePointAsValid)	
	ADD_WIDGET_VECTOR_SLIDER("Coord1", SpArea.vCoords1, -99999.9, 99999.9, 0.1)
	ADD_WIDGET_VECTOR_SLIDER("Coord2", SpArea.vCoords2, -99999.9, 99999.9, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Float", SpArea.fFloat, -99999.0, 99999.0, 0.1)		
	ADD_WIDGET_FLOAT_SLIDER("Heading", SpArea.fHeading, 0.0, 360.0, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("fIncreaseDist", SpArea.fIncreaseDist, 0.0, 100.0, 1.0)
	START_NEW_WIDGET_COMBO()
		ADD_TO_WIDGET_COMBO("CIRCLE")
		ADD_TO_WIDGET_COMBO("BOX")
		ADD_TO_WIDGET_COMBO("ANGLED AREA")
	STOP_WIDGET_COMBO("Shape", SpArea.iShape)	
ENDPROC

PROC ADD_WIDGET_FOR_PROBLEM_AREA(PROBLEM_AREA &PrbArea)
	ADD_WIDGET_BOOL("Show Problem Area", PrbArea.bShow)
	ADD_WIDGET_BOOL("Make around player", PrbArea.bSetAroundPlayer)		
	ADD_WIDGET_VECTOR_SLIDER("Coord1", PrbArea.vCoords1, -99999.9, 99999.9, 0.1)
	ADD_WIDGET_VECTOR_SLIDER("Coord2", PrbArea.vCoords2, -99999.9, 99999.9, 0.1)
	ADD_WIDGET_FLOAT_SLIDER("Float", PrbArea.fFloat, 0.0, 99999.0, 0.1)		
ENDPROC

FUNC STRING GET_SPAWN_LOCATION_DEBUG_NAME(PLAYER_SPAWN_LOCATION spawnLocation)


	// NOTE :string length must be less than 36 chars

	SWITCH spawnLocation
		CASE SPAWN_LOCATION_AUTOMATIC											RETURN "AUTOMATIC"
		CASE SPAWN_LOCATION_NEAR_DEATH			                                RETURN "NEAR_DEATH"
		CASE SPAWN_LOCATION_NEAR_TEAM_MATES			                            RETURN "NEAR_TEAM_MATES"
		CASE SPAWN_LOCATION_MISSION_AREA			                            RETURN "MISSION_AREA"
		CASE SPAWN_LOCATION_NEAR_OTHER_PLAYERS                                  RETURN "NEAR_OTHER_PLAYERS"
		CASE SPAWN_LOCATION_NEAR_CURRENT_POSITION	                            RETURN "NEAR_CURRENT_POSITION"
		CASE SPAWN_LOCATION_AT_CURRENT_POSITION                                 RETURN "AT_CURRENT_POSITION"
		CASE SPAWN_LOCATION_NET_TEST_BED	                                    RETURN "NET_TEST_BED"
		CASE SPAWN_LOCATION_CUSTOM_SPAWN_POINTS                                 RETURN "CUSTOM_SPAWN_POINTS"
		CASE SPAWN_LOCATION_OUTSIDE_SIMEON_GARAGE                               RETURN "OUTSIDE_SIMEON_GARAGE"
		CASE SPAWN_LOCATION_NEAR_SPECIFIC_COORDS                                RETURN "NEAR_SPECIFIC_COORDS"
		CASE SPAWN_LOCATION_AT_SPECIFIC_COORDS                                  RETURN "AT_SPECIFIC_COORDS"
		CASE SPAWN_LOCATION_AT_AIRPORT_ARRIVALS                                 RETURN "AT_AIRPORT_ARRIVALS"
		CASE SPAWN_LOCATION_AT_SPECIFIC_COORDS_IF_POSSIBLE                      RETURN "AT_SPECIFIC_COORDS_IF_POSSIBLE"
		CASE SPAWN_LOCATION_IN_SPECIFIC_ANGLED_AREA                             RETURN "IN_SPECIFIC_ANGLED_AREA"
		CASE SPAWN_LOCATION_NEAREST_RESPAWN_POINT                               RETURN "NEAREST_RESPAWN_POINT"
		CASE SPAWN_LOCATION_AT_SPECIFIC_COORDS_RACE_CORONA                      RETURN "AT_SPECIFIC_COORDS_RACE_CORONA"
		CASE SPAWN_LOCATION_INSIDE_GARAGE                                       RETURN "INSIDE_GARAGE"
		CASE SPAWN_LOCATION_INSIDE_PROPERTY                                     RETURN "INSIDE_PROPERTY"
		CASE SPAWN_LOCATION_INSIDE_PROPERTY_OR_GARAGE                           RETURN "INSIDE_PROPERTY_OR_GARAGE"
		CASE SPAWN_LOCATION_NEAR_DEATH_IMPROMPTU                                RETURN "NEAR_DEATH_IMPROMPTU"
		CASE SPAWN_LOCATION_NEAR_CURRENT_POSITION_SPREAD_OUT                    RETURN "NEAR_CURRENT_POSITION_SPREAD_OUT"
		CASE SPAWN_LOCATION_NEAREST_RESPAWN_POINT_TO_SPECIFIC_COORDS            RETURN "NEAREST_RESPAWN_POINT_TO_SPECIFIC_C" // truncated to 35 chars
		CASE SPAWN_LOCATION_NEAREST_HOSPITAL                                    RETURN "NEAREST_HOSPITAL"
		CASE SPAWN_LOCATION_NEAREST_POLICE_STATION                              RETURN "NEAREST_POLICE_STATION"
		CASE SPAWN_LOCATION_NEAREST_HOTEL_TO_SPECIFIC_COORDS                    RETURN "NEAREST_HOTEL_TO_SPECIFIC_COORDS"
		CASE SPAWN_LOCATION_MISSION_AREA_NEAR_CURRENT_POSITION                  RETURN "MISSION_AREA_NEAR_CURRENT_POSITION"
		CASE SPAWN_LOCATION_PRIVATE_YACHT                                       RETURN "PRIVATE_YACHT"
		CASE SPAWN_LOCATION_PRIVATE_YACHT_APARTMENT                             RETURN "PRIVATE_YACHT_APARTMENT"
		CASE SPAWN_LOCATION_PRIVATE_FRIEND_YACHT                                RETURN "PRIVATE_FRIEND_YACHT"
		CASE SPAWN_LOCATION_PRIVATE_YACHT_NEAR_SHORE                            RETURN "PRIVATE_YACHT_NEAR_SHORE"
		CASE SPAWN_LOCATION_NEAR_GANG_BOSS                                      RETURN "NEAR_GANG_BOSS"
		CASE SPAWN_LOCATION_NEAR_SPECIFIC_COORDS_WITH_GANG                      RETURN "NEAR_SPECIFIC_COORDS_WITH_GANG"
		CASE SPAWN_LOCATION_GANG_DM                                             RETURN "GANG_DM"
		CASE SPAWN_LOCATION_GANG_BOSS_PRIVATE_YACHT                             RETURN "GANG_BOSS_PRIVATE_YACHT"
		CASE SPAWN_LOCATION_OFFICE                                              RETURN "OFFICE"
		CASE SPAWN_LOCATION_CLUBHOUSE                                           RETURN "CLUBHOUSE"
		CASE SPAWN_LOCATION_NEAR_CURRENT_POSITION_AS_POSSIBLE                   RETURN "NEAR_CURRENT_POSITION_AS_POSSIBLE"
		CASE SPAWN_LOCATION_NEAR_CURRENT_PERCEIVED_POSITION                     RETURN "NEAR_CURRENT_PERCEIVED_POSITION"
		CASE SPAWN_LOCATION_IE_WAREHOUSE                                        RETURN "IE_WAREHOUSE"
		CASE SPAWN_LOCATION_BUNKER                                              RETURN "BUNKER"
		CASE SPAWN_LOCATION_HANGAR                                              RETURN "HANGAR"
		CASE SPAWN_LOCATION_DEFUNCT_BASE                                        RETURN "DEFUNCT_BASE"
		CASE SPAWN_LOCATION_NIGHTCLUB                                           RETURN "NIGHTCLUB"
		CASE SPAWN_LOCATION_ARENA_GARAGE                                        RETURN "ARENA_GARAGE"
		CASE SPAWN_LOCATION_CASINO                                              RETURN "CASINO"
		CASE SPAWN_LOCATION_CASINO_APARTMENT                                    RETURN "CASINO_APARTMENT"
		CASE SPAWN_LOCATION_CASINO_OUTSIDE                                      RETURN "CASINO_OUTSIDE"
		CASE SPAWN_LOCATION_ARCADE                                              RETURN "ARCADE"
		#IF FEATURE_COPS_N_CROOKS                                               
		CASE SPAWN_LOCATION_AFTER_DLC_INTRO_BINK                                RETURN "AFTER_DLC_INTRO_BINK"
		#ENDIF                                                                  
		#IF FEATURE_CASINO_NIGHTCLUB                                            
		CASE SPAWN_LOCATION_CASINO_NIGHTCLUB                                    RETURN "CASINO_NIGHTCLUB"
		#ENDIF                                                                  
		#IF FEATURE_HEIST_ISLAND                                                
		CASE SPAWN_LOCATION_SUBMARINE                                           RETURN "SUBMARINE"
		CASE SPAWN_LOCATION_HEIST_ISLAND_NEAR_DEATH                             RETURN "HEIST_ISLAND_NEAR_DEATH"
		CASE SPAWN_LOCATION_HEIST_ISLAND_BEACH_PARTY                            RETURN "HEIST_ISLAND_BEACH_PARTY"
		CASE SPAWN_LOCATION_LAND_NEAR_SUBMARINE                                 RETURN "LAND_NEAR_SUBMARINE"
		#ENDIF                                                                  
		#IF FEATURE_TUNER                                                       
		CASE SPAWN_LOCATION_CAR_MEET                                            RETURN "CAR_MEET"
		CASE SPAWN_LOCATION_AUTO_SHOP                                           RETURN "AUTO_SHOP"
		#ENDIF                                                                  	
		
		#IF FEATURE_FIXER
		CASE SPAWN_LOCATION_FIXER_HQ											RETURN "FIXER_HQ"
		CASE SPAWN_LOCATION_SITTING_SMOKING										RETURN "SITTING_SMOKING"
		CASE SPAWN_LOCATION_DRUNK_WAKE_UP_MUSIC_STUDIO							RETURN "DRUNK_WAKE_UP_MUSIC_STUDIO"
		#IF FEATURE_MUSIC_STUDIO
		CASE SPAWN_LOCATION_MUSIC_STUDIO										RETURN "MUSIC_STUDIO"
		#ENDIF	
		#ENDIF	                                                                
		#IF FEATURE_GEN9_EXCLUSIVE	                                            	
		CASE SPAWN_LOCATION_TUTORIAL_BUSINESS                                   RETURN "TUTORIAL_BUSINESS"
		#ENDIF		
		#IF FEATURE_DLC_2_2022
		CASE SPAWN_LOCATION_SIMEON_SHOWROOM										RETURN "SIMEON_SHOWROOM"
		CASE SPAWN_LOCATION_LUXURY_SHOWROOM										RETURN "LUXURY_SHOWROOM"
		#ENDIF
	ENDSWITCH

	RETURN "Unknown Spawn Location"

ENDFUNC

PROC AddLocationWidgets()
	INT i
	REPEAT TOTAL_SPAWN_LOCATIONS i
		ADD_TO_WIDGET_COMBO(GET_SPAWN_LOCATION_DEBUG_NAME(INT_TO_ENUM(PLAYER_SPAWN_LOCATION, i)))
	ENDREPEAT
ENDPROC


PROC ADD_WIDGET_FOR_SPAWN_SEARCH_PARAMS(SPAWN_SEARCH_PARAMS &SpawnSearchParams)
	START_WIDGET_GROUP("SPAWN_SEARCH_PARAMS")
		ADD_WIDGET_VECTOR_SLIDER("vFacingCoords", SpawnSearchParams.vFacingCoords, -9999.9, 9999.9, 0.01)
		ADD_WIDGET_BOOL("bConsiderInteriors", SpawnSearchParams.bConsiderInteriors)
		ADD_WIDGET_BOOL("bPreferPointsCloserToRoads", SpawnSearchParams.bPreferPointsCloserToRoads)
		ADD_WIDGET_FLOAT_SLIDER("fMinDistFromPlayer", SpawnSearchParams.fMinDistFromPlayer, 0.0, 500.0, 1.0)
		ADD_WIDGET_BOOL("bCloseToOriginAsPossible", SpawnSearchParams.bCloseToOriginAsPossible)
		ADD_WIDGET_BOOL("bConsiderOriginAsValidPoint", SpawnSearchParams.bConsiderOriginAsValidPoint)
		ADD_WIDGET_BOOL("bSearchVehicleNodesOnly", SpawnSearchParams.bSearchVehicleNodesOnly)
		ADD_WIDGET_BOOL("bUseOnlyBoatNodes", SpawnSearchParams.bUseOnlyBoatNodes)
		ADD_WIDGET_BOOL("bEdgesOnly", SpawnSearchParams.bEdgesOnly)
		ADD_WIDGET_FLOAT_SLIDER("fHeadingForConsideredOrigin", SpawnSearchParams.fHeadingForConsideredOrigin, 0.0, 360.0, 0.1)
		ADD_WIDGET_FLOAT_SLIDER("fMaxZBelowRaw", SpawnSearchParams.fMaxZBelowRaw, -100.0, 100.0, 0.1)	
		INT i
		TEXT_LABEL_63 str
		REPEAT MAX_NUM_AVOID_RADIUS i
			str = "vAvoidCoords["
			str += i
			str += "]"
			ADD_WIDGET_VECTOR_SLIDER(str, SpawnSearchParams.vAvoidCoords[i], -9999.9, 9999.9, 0.01)
			str = "fAvoidRadius["
			str += i
			str += "]"
			ADD_WIDGET_FLOAT_SLIDER(str, SpawnSearchParams.fAvoidRadius[i], 0.0, 1000.0, 0.1)
		ENDREPEAT
		
		ADD_WIDGET_VECTOR_SLIDER("vAvoidAngledAreaPos1", SpawnSearchParams.vAvoidAngledAreaPos1, -9999.9, 9999.9, 0.01)
		ADD_WIDGET_VECTOR_SLIDER("vAvoidAngledAreaPos2", SpawnSearchParams.vAvoidAngledAreaPos2, -9999.9, 9999.9, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("fAvoidAngledAreaWidth", SpawnSearchParams.fAvoidAngledAreaWidth, 0.0, 1000.0, 0.1)

		ADD_WIDGET_BOOL("bUseOffRoadChecking", SpawnSearchParams.bUseOffRoadChecking)
		ADD_WIDGET_FLOAT_SLIDER("fUpperZLimitForNodes", SpawnSearchParams.fUpperZLimitForNodes, -999999.0, 999999.0, 0.1)	
		ADD_WIDGET_BOOL("bAvoidOtherPeds", SpawnSearchParams.bAvoidOtherPeds)
	STOP_WIDGET_GROUP()
ENDPROC

PROC ADD_WIDGET_FOR_PRIVATE_SPAWN_SEARCH_PARAMS(PRIVATE_SPAWN_SEARCH_PARAMS &SpawnSearchParams)
	START_WIDGET_GROUP("PRIVATE_SPAWN_SEARCH_PARAMS")
	
		ADD_WIDGET_VECTOR_SLIDER("vSearchCoord", SpawnSearchParams.vSearchCoord, -9999.9, 9999.9, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("fRawHeading", SpawnSearchParams.fRawHeading, -500.0, 500.0, 1.0)
		ADD_WIDGET_FLOAT_SLIDER("fSearchRadius", SpawnSearchParams.fSearchRadius, 0.0, 500.0, 1.0)
		ADD_WIDGET_BOOL("bIsForLocalPlayerSpawning", SpawnSearchParams.bIsForLocalPlayerSpawning)
		ADD_WIDGET_BOOL("bDoTeamMateVisCheck", SpawnSearchParams.bDoTeamMateVisCheck)
		ADD_WIDGET_INT_SLIDER("iAreaShape", SpawnSearchParams.iAreaShape, 0, 2, 1)
		ADD_WIDGET_VECTOR_SLIDER("vAngledAreaPoint1", SpawnSearchParams.vAngledAreaPoint1, -9999.9, 9999.9, 0.01)
		ADD_WIDGET_VECTOR_SLIDER("vAngledAreaPoint2", SpawnSearchParams.vAngledAreaPoint2, -9999.9, 9999.9, 0.01)
		ADD_WIDGET_FLOAT_SLIDER("fAngledAreaWidth", SpawnSearchParams.fAngledAreaWidth, 0.0, 500.0, 1.0)
		ADD_WIDGET_BOOL("bForAVehicle", SpawnSearchParams.bForAVehicle)
		ADD_WIDGET_BOOL("bDoVisibleChecks", SpawnSearchParams.bDoVisibleChecks)
		ADD_WIDGET_BOOL("bIgnoreTeammatesForMinDistCheck", SpawnSearchParams.bIgnoreTeammatesForMinDistCheck)
		ADD_WIDGET_VECTOR_SLIDER("vOffsetOrigin", SpawnSearchParams.vOffsetOrigin, -9999.9, 9999.9, 0.01)
		ADD_WIDGET_BOOL("bUseOffsetOrigin", SpawnSearchParams.bUseOffsetOrigin)
		ADD_WIDGET_BOOL("bAllowFallbackToUseInteriors", SpawnSearchParams.bAllowFallbackToUseInteriors)
		ADD_WIDGET_BOOL("bAllowFallbackToUseInactiveNodes", SpawnSearchParams.bAllowFallbackToUseInactiveNodes)
		ADD_WIDGET_BOOL("bAllowFallbackToUseNavMesh", SpawnSearchParams.bAllowFallbackToUseNavMesh)
	STOP_WIDGET_GROUP()
ENDPROC

PROC ADD_WIDGET_FOR_MISSION_SPAWN_DETAILS(MISSION_SPAWN_DETAILS &MissionSpawnDetails)
	INT i
	TEXT_LABEL_63 str63
	
	REPEAT MAX_NUMBER_OF_MISSION_SPAWN_AREAS i
		ADD_WIDGET_FOR_SPAWN_AREA(MissionSpawnDetails.SpawnArea[i])
	ENDREPEAT

	ADD_WIDGET_VECTOR_SLIDER("vFacing", MissionSpawnDetails.vFacing, -99999.9, 99999.9, 0.01)
	ADD_WIDGET_BOOL("bFacePoint", MissionSpawnDetails.bFacePoint)
	ADD_WIDGET_BOOL("bFaceAwayFromPoint", MissionSpawnDetails.bFaceAwayFromPoint)
	ADD_WIDGET_BOOL("bUseNearCurrentCoordsForSpawnArea", MissionSpawnDetails.bUseNearCurrentCoordsForSpawnArea)
	ADD_WIDGET_BOOL("bConsiderInteriors", MissionSpawnDetails.bConsiderInteriors)
	ADD_WIDGET_INT_SLIDER("iDeathInteriorGroup", MissionSpawnDetails.iDeathInteriorGroup, LOWEST_INT, HIGHEST_INT, 1)
	ADD_WIDGET_BOOL("bDoNearARoadChecks", MissionSpawnDetails.bDoNearARoadChecks)
	ADD_WIDGET_FLOAT_SLIDER("fSpecificSpawnLocationOverride", MissionSpawnDetails.fSpecificSpawnLocationOverride, -9999999.9, 9999999.9, 0.01)
	ADD_WIDGET_VECTOR_SLIDER("vNextRaceCheckpoint", MissionSpawnDetails.vNextRaceCheckpoint, -99999.9, 99999.9, 0.01)
	ADD_WIDGET_BOOL("bWillNotRespawnCopsInVehicles", MissionSpawnDetails.bWillNotRespawnCopsInVehicles)
	ADD_WIDGET_BOOL("bSpawnInVehicle", MissionSpawnDetails.bSpawnInVehicle)
	ADD_WIDGET_BOOL("bUsePersonalVehicleToSpawn", MissionSpawnDetails.bUsePersonalVehicleToSpawn)
	ADD_WIDGET_BOOL("bSpawnInLastVehicleIfDriveable", MissionSpawnDetails.bSpawnInLastVehicleIfDriveable)
	
	
	//VEHICLE_SETUP_STRUCT_MP SpawnVehicleSetupMP
	
	ADD_WIDGET_BOOL("bSpawnVehicleSetupSaved", MissionSpawnDetails.bSpawnVehicleSetupSaved)
	ADD_WIDGET_BOOL("bDontUseVehicleNodes", MissionSpawnDetails.bDontUseVehicleNodes)
	ADD_WIDGET_BOOL("bSpawningCreatedNewCar", MissionSpawnDetails.bSpawningCreatedNewCar)

	REPEAT MAX_NUM_OF_STORED_LAST_SPAWN_RESULTS i
		str63 = "vLastSpawn["
		str63 += i
		str63 += "]"
		ADD_WIDGET_VECTOR_SLIDER(str63, MissionSpawnDetails.vLastSpawn[i], -99999.9, 99999.9, 0.01)
	ENDREPEAT
	
	REPEAT FMMC_MAX_NUM_RACERS i
		str63 = "vSecondaryRaceRespawn["
		str63 += i
		str63 += "]"
		ADD_WIDGET_VECTOR_SLIDER(str63, MissionSpawnDetails.vSecondaryRaceRespawn[i], -99999.9, 99999.9, 0.01)
	ENDREPEAT
	
	ADD_WIDGET_VECTOR_SLIDER("vRaceRespawnPos", MissionSpawnDetails.vRaceRespawnPos, -99999.9, 99999.9, 0.01)
	ADD_WIDGET_BOOL("bJustRespawnedInRace", MissionSpawnDetails.bJustRespawnedInRace)
	ADD_WIDGET_BOOL("bAbortSpawnInVehicle", MissionSpawnDetails.bAbortSpawnInVehicle)
	ADD_WIDGET_BOOL("bUseRaceRespotAfterRespawns", MissionSpawnDetails.bUseRaceRespotAfterRespawns)

	ADD_WIDGET_INT_SLIDER("iDiedInMissionSpawnAreaCount", MissionSpawnDetails.iDiedInMissionSpawnAreaCount, LOWEST_INT, HIGHEST_INT, 1)
	ADD_WIDGET_FLOAT_SLIDER("fMinDistFromEnemyNearDeath", MissionSpawnDetails.fMinDistFromEnemyNearDeath, -9999999.9, 9999999.9, 0.01)
	ADD_WIDGET_FLOAT_SLIDER("fMinDistFromDeathNearDeath", MissionSpawnDetails.fMinDistFromDeathNearDeath, -9999999.9, 9999999.9, 0.01)
	
	ADD_WIDGET_BOOL("bUseOffRoadChecking", MissionSpawnDetails.bUseOffRoadChecking)
	ADD_WIDGET_BOOL("bUseNavMeshFallback", MissionSpawnDetails.bUseNavMeshFallback)

	ADD_WIDGET_FLOAT_SLIDER("fCarNodeLowerZLimit", MissionSpawnDetails.fCarNodeLowerZLimit, -999999.9, 99999.9, 0.01)

	ADD_WIDGET_BOOL("bDoForcedRespawn", MissionSpawnDetails.bDoForcedRespawn)
	
ENDPROC

PROC ADD_SPAWNING_WIDGETS()                                           

	INT i, j
	
	TEXT_LABEL_63 str

	START_WIDGET_GROUP("Spawning / Warping.")
	
		START_NEW_WIDGET_COMBO()
			AddLocationWidgets()
		STOP_WIDGET_COMBO("TEST SPAWN LOCATION", g_SpawnData.iSpawnLocation)
		
		g_SpawnData.SpawnTextWidgetID = ADD_TEXT_WIDGET("Selected Spawn Location")
		ADD_WIDGET_INT_READ_ONLY("g_SpawnData.iSpawnLocation", g_SpawnData.iSpawnLocation)
			
		START_WIDGET_GROUP("Next spawn location")
			START_NEW_WIDGET_COMBO()
				AddLocationWidgets()
			STOP_WIDGET_COMBO("NEXT SPAWN LOCATION", g_SpawnData.iNextSpawnLocation)	
			ADD_WIDGET_BOOL("bIsPersistant",  g_SpawnData.NextSpawn.bIsPersistant)
			ADD_WIDGET_BOOL("bUseRaceCoronaSpacing", g_SpawnData.NextSpawn.bUseRaceCoronaSpacing)
#IF ADD_WIDGETS_FOR_SPAWNING
			ADD_WIDGET_FLOAT_SLIDER("CORONA_OFFSET_OVERRIDE", CORONA_OFFSET_OVERRIDE, -1.0, 360.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("CORONA_OFFSET_DIST", CORONA_OFFSET_DIST, 0.0, 20.0, 0.1)
#ENDIF	//	ADD_WIDGETS_FOR_SPAWNING
		STOP_WIDGET_GROUP()
			
		//ADD_WIDGET_BOOL("bUseNewSpawningCommands", g_SpawnData.bUseNewSpawningCommands)
			
		ADD_WIDGET_BOOL("Test Spawn/Warp Now",  g_SpawnData.bTestWarp)
		ADD_WIDGET_BOOL("Dont Clear Mission Spawn Data",  g_SpawnData.bDontClearMissionSpawnPersonalVehicleData)
		ADD_WIDGET_BOOL("Leave ped behind", g_SpawnData.bLeavePedBehind)		
		
		
		ADD_WIDGET_BOOL("Enable Use of Spawn Nodes", g_SpawnData.bEnableUseOfSpawnNodes)
		ADD_WIDGET_BOOL("bDoVisibilityChecksOnTeammates", g_SpawnData.bDoVisibilityChecksOnTeammates)
		ADD_WIDGET_INT_SLIDER("iRecRoomPosition", g_SpawnData.iRecRoomPosition, -1, 99, 1)
		ADD_WIDGET_BOOL("bDoQuickWarp", g_SpawnData.bDoQuickWarp)
		
#IF ADD_WIDGETS_FOR_SPAWNING
		ADD_WIDGET_FLOAT_SLIDER("SPAWN_MIN_DIST_FROM_PED_NEAR", SPAWN_MIN_DIST_FROM_PED_NEAR, 0.0, 100.0, 0.1)
#ENDIF	//	ADD_WIDGETS_FOR_SPAWNING
		
		ADD_WIDGET_BOOL("g_SpawnData.bShowAdvancedSpew", g_SpawnData.bShowAdvancedSpew)
		ADD_WIDGET_BOOL("g_SpawnData.bHasDoneInitialSpawn", g_SpawnData.bHasDoneInitialSpawn)
		
		ADD_WIDGET_BOOL("bUseUnsortedSearchResultsSystem", g_SpawnData.bUseUnsortedSearchResultsSystem)
		
		START_WIDGET_GROUP("Custom Player Model")
			ADD_WIDGET_BOOL("g_SpawnData.bCallSetCustomPlayerModel_Franklin", g_SpawnData.bCallSetCustomPlayerModel_Franklin)
			ADD_WIDGET_BOOL("g_SpawnData.bCallSetCustomPlayerModel_Lamar", g_SpawnData.bCallSetCustomPlayerModel_Lamar)
			ADD_WIDGET_BOOL("g_SpawnData.bCallClearCustomPlayerModel", g_SpawnData.bCallClearCustomPlayerModel)
			ADD_WIDGET_BOOL("g_SpawnData.bCallFinalizeHeadBlend", g_SpawnData.bCallFinalizeHeadBlend)
			ADD_WIDGET_BOOL("g_SpawnData.bCallNetSpawnPlayer", g_SpawnData.bCallNetSpawnPlayer)
		STOP_WIDGET_GROUP()
		
		
		START_WIDGET_GROUP("Specific Coords")
		
			ADD_WIDGET_BOOL("bRenderSpecificCoords", g_SpawnData.bRenderSpecificCoords)
			ADD_WIDGET_BOOL("Grab from current Pos", g_SpawnData.bGetSpecificCoordsFromPlayerPos)
			
			ADD_WIDGET_VECTOR_SLIDER("vCoords", g_SpecificSpawnLocation.vCoords, -9999.9, 9999.9, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("fHeading", g_SpecificSpawnLocation.fHeading, -9999.9, 9999.9, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("fMinRadius", g_SpecificSpawnLocation.fMinRadius, -1.0, 9999.9, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("fRadius", g_SpecificSpawnLocation.fRadius, 0.0, 400.0, 0.1)
			ADD_WIDGET_BOOL("bDoVisibleChecks", g_SpecificSpawnLocation.bDoVisibleChecks)			
			ADD_WIDGET_BOOL("bDoNearARoadChecks", g_SpecificSpawnLocation.bDoNearARoadChecks)
			ADD_WIDGET_BOOL("bConsiderInteriors", g_SpecificSpawnLocation.bConsiderInteriors)
			ADD_WIDGET_BOOL("bNearCentrePoint", g_SpecificSpawnLocation.bNearCentrePoint)
			ADD_WIDGET_BOOL("bUseAngledArea", g_SpecificSpawnLocation.bUseAngledArea)			
			ADD_WIDGET_VECTOR_SLIDER("vAngledCoords1", g_SpecificSpawnLocation.vAngledCoords1, -9999.9, 9999.9, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("vAngledCoords2", g_SpecificSpawnLocation.vAngledCoords2, -9999.9, 9999.9, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("fWidth", g_SpecificSpawnLocation.fWidth, -9999.9, 9999.9, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("vFacing", g_SpecificSpawnLocation.vFacing, -9999.9, 9999.9, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("fMinDistFromOtherPlayers", g_SpecificSpawnLocation.fMinDistFromOtherPlayers, -9999.9, 9999.9, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("fPitch", g_SpecificSpawnLocation.fPitch, -9999.9, 9999.9, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("g_SpawnData.fPitch", g_SpawnData.fPitch, -9999.9, 9999.9, 0.01)

			
			
			ADD_WIDGET_FLOAT_SLIDER("fSpecificSpawnLocationOverride", g_SpawnData.MissionSpawnDetails.fSpecificSpawnLocationOverride, -1.0, 99999.9, 1.0)
			ADD_WIDGET_VECTOR_SLIDER("vNextRaceCheckpoint", g_SpawnData.MissionSpawnDetails.vNextRaceCheckpoint, -9999.9, 9999.9, 0.1)			
			ADD_WIDGET_BOOL("bTestMoveSpecificCoords", g_SpawnData.bTestMoveSpecificCoords)
			ADD_WIDGET_BOOL("bMoveOutOfGlobalExclusion", g_SpawnData.bMoveOutOfGlobalExclusion)
			ADD_WIDGET_BOOL("bMoveOutOfMissionExclusion", g_SpawnData.bMoveOutOfMissionExclusion)
			ADD_WIDGET_BOOL("bMoveOutOfGangAttack", g_SpawnData.bMoveOutOfGangAttack)
			ADD_WIDGET_BOOL("bMoveOutOfMissionCorona", g_SpawnData.bMoveOutOfMissionCorona)

			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Race Respawning")
			ADD_WIDGET_BOOL("bIgnoreRaceSecondaryRespawns", g_SpawnData.bIgnoreRaceSecondaryRespawns)
			ADD_WIDGET_BOOL("bTriggerManualRespawn", g_SpawnData.bTriggerManualRespawn)
			ADD_WIDGET_BOOL("render all points", g_SpawnData.bRenderAllRaceCheckpoints)	
			ADD_WIDGET_INT_SLIDER("draw distance", g_SpawnData.iRenderAllRaceCheckpointsDrawDistance, 0, HIGHEST_INT, 1)	
			ADD_WIDGET_BOOL("disable seconary respawning", g_SpawnData.bDontUseSecondaryRespawn)
			ADD_WIDGET_BOOL("bRenderRespotSphere", g_SpawnData.bRenderRespotSphere)
			ADD_WIDGET_BOOL("MissionSpawnDetails.bJustRespawnedInRace", g_SpawnData.MissionSpawnDetails.bJustRespawnedInRace)
			ADD_WIDGET_VECTOR_SLIDER("MissionSpawnDetails.vRaceRespawnPos", g_SpawnData.MissionSpawnDetails.vRaceRespawnPos, -99999.9, 99999.9, 0.001)
		STOP_WIDGET_GROUP()
		
		
		START_WIDGET_GROUP("Last Spawn")
#IF ADD_WIDGETS_FOR_SPAWNING
			ADD_WIDGET_INT_SLIDER("LAST_SPAWN_ACTIVE_TIME", LAST_SPAWN_ACTIVE_TIME, 0, HIGHEST_INT, 1)	
			ADD_WIDGET_FLOAT_SLIDER("LAST_SPAWN_ACTIVE_RADIUS", LAST_SPAWN_ACTIVE_RADIUS, 0.0, 1000.0, 1.0)	
#ENDIF	//	ADD_WIDGETS_FOR_SPAWNING
			REPEAT MAX_NUM_OF_STORED_LAST_SPAWN_RESULTS i
				str = "vLastSpawn["
				str += i
				str += "]"
				ADD_WIDGET_VECTOR_SLIDER(str, g_SpawnData.MissionSpawnDetails.vLastSpawn[i], -9999.9, 9999.9, 0.1)
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Vehicles")
			ADD_WIDGET_INT_SLIDER("g_iSmugglerUniqueMissionID", g_iSmugglerUniqueMissionID, LOWEST_INT, HIGHEST_INT, 1)
			
			START_WIDGET_GROUP("call SET_PLAYER_RESPAWN_IN_VEHICLE")
				ADD_WIDGET_BOOL("call function", g_SpawnData.bUpdate_SET_PLAYER_RESPAWN_IN_VEHICLE)				
				ADD_WIDGET_BOOL("bRespawnInVehicle", g_SpawnData.bRespawnInVehicle)
				ADD_WIDGET_INT_SLIDER("Vehicle Model", g_SpawnData.iSpawnVehicleModel, LOWEST_INT, HIGHEST_INT, 1)
				ADD_WIDGET_BOOL("bUseLastCarIfDriveable", g_SpawnData.bUseLastCarIfDriveable)	
				ADD_WIDGET_BOOL("bDontUseVehicleNodes", g_SpawnData.bDontUseVehicleNodes)	
				ADD_WIDGET_BOOL("bUsePersonalVehicleToSpawn", g_SpawnData.bUsePersonalVehicleIfPossible)	
				ADD_WIDGET_BOOL("bSetVehicleStrong", g_SpawnData.bSetVehicleStrong)
				ADD_WIDGET_INT_SLIDER("iColour", g_SpawnData.iColour, LOWEST_INT, HIGHEST_INT, 1)
				ADD_WIDGET_BOOL("bSetTyresBulletProof", g_SpawnData.bSetTyresBulletProof)
				ADD_WIDGET_BOOL("bSetAsNonSaveable", g_SpawnData.bSetAsNonSaveable)
				ADD_WIDGET_BOOL("bSetAsNonModable", g_SpawnData.bSetAsNonModable)
				ADD_WIDGET_BOOL("bSetFireAfterGangBossMissionEnds", g_SpawnData.bSetFireAfterGangBossMissionEnds)				
			STOP_WIDGET_GROUP()
			g_SpawnData.iSpawnVehicleModel = ENUM_TO_INT(ASTEROPE)
//			START_WIDGET_GROUP("Vehicle delivery timer")
//				ADD_WIDGET_BOOL("Clear delivery timer", g_SpawnData.bClearVehSpawnTimer)			
//			STOP_WIDGET_GROUP()
			ADD_WIDGET_BOOL("Warp into vehicle", g_SpawnData.bWarpIntoCar)
	
			ADD_WIDGET_BOOL("g_SpawnData.MissionSpawnDetails.bSpawnInVehicle", g_SpawnData.MissionSpawnDetails.bSpawnInVehicle)
			ADD_WIDGET_BOOL("g_SpawnData.MissionSpawnDetails.bAbortSpawnInVehicle", g_SpawnData.MissionSpawnDetails.bAbortSpawnInVehicle)
			ADD_WIDGET_BOOL("g_SpawnData.MissionSpawnDetails.bSpawnInLastVehicleIfDriveable", g_SpawnData.MissionSpawnDetails.bSpawnInLastVehicleIfDriveable)
			ADD_WIDGET_BOOL("g_SpawnData.MissionSpawnDetails.bDontUseVehicleNodes", g_SpawnData.MissionSpawnDetails.bDontUseVehicleNodes)
			ADD_WIDGET_BOOL("g_SpawnData.MissionSpawnDetails.bUsePersonalVehicleToSpawn", g_SpawnData.MissionSpawnDetails.bUsePersonalVehicleToSpawn)
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("SAVED_VEHICLE")
			ADD_WIDGET_INT_SLIDER("iFlags", MPGlobals.VehicleData.iFlags, LOWEST_INT, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iFlags2", MPGlobals.VehicleData.iFlags2, LOWEST_INT, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iTruckFlags", MPGlobals.VehicleData.iTruckFlags, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iTruckFlags2", MPGlobals.VehicleData.iTruckFlags2, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iPlaneFlags", MPGlobals.VehicleData.iPlaneFlags, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iPlaneFlags2", MPGlobals.VehicleData.iPlaneFlags2, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iHackerTruckFlags", MPGlobals.VehicleData.iHackerTruckFlags, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iHackerTruckFlags2", MPGlobals.VehicleData.iHackerTruckFlags2, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iTimesImpounded", MPGlobals.VehicleData.iTimesImpounded, -1, HIGHEST_INT, 1)
			ADD_WIDGET_BOOL("bAssignToMainScript", MPGlobals.VehicleData.bAssignToMainScript)
			ADD_WIDGET_BOOL("bAwaitingAssignToMainScript", MPGlobals.VehicleData.bAwaitingAssignToMainScript)
			ADD_WIDGET_BOOL("bWarpNear", MPGlobals.VehicleData.bWarpNear)
			ADD_WIDGET_BOOL("bFixupVehicleAfterWarp", MPGlobals.VehicleData.bFixupVehicleAfterWarp)
			ADD_WIDGET_BOOL("bSwitchWarpProcessed", MPGlobals.VehicleData.bSwitchWarpProcessed)
//			ADD_WIDGET_BOOL("bCrewEmblem", MPGlobals.VehicleData.bCrewEmblem)
//			ADD_WIDGET_BOOL("bLoadingEmblem", MPGlobals.VehicleData.bLoadingEmblem)
			ADD_WIDGET_BOOL("bLockDoor", MPGlobals.VehicleData.bLockDoor)
			ADD_WIDGET_BOOL("bDoorLocked", MPGlobals.VehicleData.bDoorLocked)
			ADD_WIDGET_BOOL("bFadingOutForWarp", MPGlobals.VehicleData.bFadingOutForWarp)
			//ADD_WIDGET_INT_SLIDER("iBitSet", MPGlobals.VehicleData.iBitSet, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iSaveSlot", MPGlobals.VehicleData.iSaveSlot, -1, HIGHEST_INT, 1)
			ADD_WIDGET_BOOL("bDriveableInFreemode", MPGlobals.VehicleData.bDriveableInFreemode)
			ADD_WIDGET_BOOL("bWarpBackIntoGarage", MPGlobals.VehicleData.bWarpBackIntoGarage)
			ADD_WIDGET_INT_SLIDER("iWarpBackIntoGarageResult", MPGlobals.VehicleData.iWarpBackIntoGarageResult, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iWarpBackSlot", MPGlobals.VehicleData.iWarpBackSlot, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iVehAccessLoop", MPGlobals.VehicleData.iVehAccessLoop, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iTruckAccessLoop", MPGlobals.VehicleData.iTruckAccessLoop, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iPlaneAccessLoop", MPGlobals.VehicleData.iPlaneAccessLoop, -1, HIGHEST_INT, 1)
			ADD_WIDGET_INT_SLIDER("iHackerTruckAccessLoop", MPGlobals.VehicleData.iHackerTruckAccessLoop, -1, HIGHEST_INT, 1)
			#IF FEATURE_DLC_2_2022
			ADD_WIDGET_INT_SLIDER("iAcidLabAccessLoop", MPGlobals.VehicleData.iAcidLabAccessLoop, -1, HIGHEST_INT, 1)
			#ENDIF
			ADD_WIDGET_INT_SLIDER("iMissionStartTime", MPGlobals.VehicleData.iMissionStartTime, -1, HIGHEST_INT, 1)
		STOP_WIDGET_GROUP()
		
		
		START_WIDGET_GROUP("GetSafeCoord Test")
			ADD_WIDGET_VECTOR_SLIDER("vTestCoordInput", g_SpawnData.vTestCoordInput, -9999.9, 9999.9, 0.1)
			ADD_WIDGET_VECTOR_SLIDER("vTestCoordOutput", g_SpawnData.vTestCoordOutput, -9999.9, 9999.9, 0.1)			
			//ADD_WIDGET_BOOL("bDoFinalDistanceCheck",  g_SpawnData.bDoFinalDistanceCheckTest)
			//ADD_WIDGET_BOOL("bUseNonPavementCheckTest",  g_SpawnData.bUseNonPavementCheckTest)
			ADD_WIDGET_BOOL("bGetSafeCoord",  g_SpawnData.bGetSafeCoord)	
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Mission Spawn Areas")	
			ADD_WIDGET_INT_SLIDER("iDiedInMissionSpawnAreaCount", g_SpawnData.MissionSpawnDetails.iDiedInMissionSpawnAreaCount, -1, HIGHEST_INT, 1)
			REPEAT MAX_NUMBER_OF_MISSION_SPAWN_AREAS i				
				str = "Spawn Area "
				str += i
				START_WIDGET_GROUP(str)	
					ADD_WIDGET_FOR_SPAWN_AREA(g_SpawnData.MissionSpawnDetails.SpawnArea[i])
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		
		START_WIDGET_GROUP("Facing Direction")			
			ADD_WIDGET_VECTOR_SLIDER("vFacing", g_SpawnData.MissionSpawnDetails.vFacing,  -9999.9, 9999.9, 0.1)
			ADD_WIDGET_BOOL("bFacePoint", g_SpawnData.MissionSpawnDetails.bFacePoint)
			ADD_WIDGET_BOOL("bFaceAwayFromPoint", g_SpawnData.MissionSpawnDetails.bFaceAwayFromPoint)
			ADD_WIDGET_BOOL("Get from Player coords", g_SpawnData.MissionSpawnDetails.bSetFacePointOnPlayer)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Mission Spawn Exclusion Areas")
			REPEAT MAX_NUMBER_OF_MISSION_SPAWN_OCCLUSION_AREAS i
				str = "Exclusion Area "
				str += i
				START_WIDGET_GROUP(str)			
					ADD_WIDGET_FOR_SPAWN_AREA(g_SpawnData.MissionSpawnExclusionAreas.ExclusionArea[i])						
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Off Mission Spawn Exclusion Areas")
			REPEAT MAX_NUM_MISSIONS_ALLOWED i	
				str = "Exclusion Area "
				str += i
				START_WIDGET_GROUP(str)	
					ADD_WIDGET_FOR_SPAWN_AREA(GlobalServerBD_ExclusionAreas.MissionExclusionAreas[i].ExclusionArea)	
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Global Exclusion Areas")
			REPEAT MAX_NUMBER_OF_GLOBAL_EXCLUSION_AREAS i	
				str = "Exclusion Area "
				str += i
				START_WIDGET_GROUP(str)	
					ADD_WIDGET_FOR_SPAWN_AREA(GlobalExclusionArea[i].ExclusionArea)	
					ADD_WIDGET_BOOL("bUseSpecificRespawnArea", GlobalExclusionArea[i].bUseSpecificRespawnArea)
					ADD_WIDGET_VECTOR_SLIDER("vSpecificRespawnArea", GlobalExclusionArea[i].vSpecificRespawnArea, -99999.9, 99999.9, 0.1)
					ADD_WIDGET_BOOL("bAlwaysConsider", GlobalExclusionArea[i].bAlwaysConsider)
				STOP_WIDGET_GROUP()
			ENDREPEAT
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Get Safe Coords - Last Area")				
			START_WIDGET_GROUP("Safe Coords Area")	
				ADD_WIDGET_FOR_SPAWN_AREA(g_SpawnData.LastGetSafeCoordsForCreatingEntity)
			STOP_WIDGET_GROUP()
			REPEAT NUMBER_OF_RESULTS_STORED_FOR_GETTING_SAFE_COORDS_DEBUG i
				str = "Returned Point "
				str += i
				ADD_WIDGET_VECTOR_SLIDER(str, g_SpawnData.LastCoordsReturnedForGetSafeCoords[i].Pos, -999999.9, 999999.9, 0.1)
				str = "Returned Heading "
				str += i
				ADD_WIDGET_FLOAT_SLIDER(str, g_SpawnData.LastCoordsReturnedForGetSafeCoords[i].Heading, -360.0, 360.0, 0.01)
			ENDREPEAT
		STOP_WIDGET_GROUP()		
		
		START_WIDGET_GROUP("NEAR_DEATH")
			ADD_WIDGET_FLOAT_SLIDER("fMinDistFromEnemyNearDeath", g_SpawnData.MissionSpawnDetails.fMinDistFromEnemyNearDeath, -1.0, 1000.0, 0.01)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Fallback Spawn Points")
			ADD_WIDGET_INT_READ_ONLY("iNumberOfFallbackSpawnPoints", g_SpawnData.iNumberOfFallbackSpawnPoints)	
			START_WIDGET_GROUP("Data")
				REPEAT MAX_NUMBER_OF_FALLBACK_SPAWN_POINTS i
					str = "Point "
					str += i	
					str += " coords"
					ADD_WIDGET_VECTOR_SLIDER(str, g_SpawnData.FallbackSpawnPoints[i].vPos, -9999.9, 9999.9, 0.1)
					str = "Point "
					str += i	
					str += " heading"					
					ADD_WIDGET_FLOAT_SLIDER(str, g_SpawnData.FallbackSpawnPoints[i].fHeading, 0.0, 360.0, 1.0)
				ENDREPEAT
			STOP_WIDGET_GROUP()		
			START_WIDGET_GROUP("Debug")
				ADD_WIDGET_VECTOR_SLIDER("pos", g_SpawnData.vFallbackSpawnPointPos, -9999.9, 9999.9, 0.1)	
				ADD_WIDGET_FLOAT_SLIDER("heading", g_SpawnData.fFallbackSpawnPointHeading, 0.0, 360.0, 1.0)
				ADD_WIDGET_BOOL("bAddFallbackSpawnPoint", g_SpawnData.bAddFallbackSpawnPoint)
				ADD_WIDGET_BOOL("bClearFallbackSpawnPoints", g_SpawnData.bClearFallbackSpawnPoints)
				ADD_WIDGET_BOOL("bShowFallbackSpawnPoints", g_SpawnData.bShowFallbackSpawnPoints)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Custom Spawn Points")
		
			ADD_WIDGET_BOOL("bShowCustomSpawnPoints", g_SpawnData.bShowCustomSpawnPoints)
			ADD_WIDGET_BOOL("bShowCustomSpawnPointLabels", g_SpawnData.bShowCustomSpawnPointLabels)
			ADD_WIDGET_BOOL("bClearCustomSpawnPoints", g_SpawnData.bClearCustomSpawnPoints)
			ADD_WIDGET_BOOL("bCustomSpawnPointsIgnoreDistanceCheck", g_SpawnData.bCustomSpawnPointsIgnoreDistanceCheck)
			ADD_WIDGET_INT_SLIDER("iCustomSpawnPointsToGenerate", g_SpawnData.iCustomSpawnPointsToGenerate, -1, MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS, 1)			
			ADD_WIDGET_FLOAT_SLIDER("fGenerateRandomCustomSpawnPointsDist", g_SpawnData.fGenerateRandomCustomSpawnPointsDist, 0.0, 1000.0, 0.1)
			ADD_WIDGET_INT_SLIDER("iGenerateRandomCustomSpawnPointsGridWidth", g_SpawnData.iGenerateRandomCustomSpawnPointsGridWidth, 1, 16, 1)
			ADD_WIDGET_INT_SLIDER("iGenerateRandomCustomSpawnPointsGridAlignment", g_SpawnData.iGenerateRandomCustomSpawnPointsGridAlignment, 0, 2, 1)
			ADD_WIDGET_FLOAT_SLIDER("fGenerateRandomCustomSpawnPointsGridHeading", g_SpawnData.fGenerateRandomCustomSpawnPointsGridHeading, 0.0, 360.0, 0.1)
			
			ADD_WIDGET_BOOL("bGenerateRandomCustomSpawnPoints_Circle", g_SpawnData.bGenerateRandomCustomSpawnPoints_Circle)
			ADD_WIDGET_BOOL("bGenerateRandomCustomSpawnPoints_Cluster", g_SpawnData.bGenerateRandomCustomSpawnPoints_Cluster)
			ADD_WIDGET_BOOL("bGenerateRandomCustomSpawnPoints_Grid", g_SpawnData.bGenerateRandomCustomSpawnPoints_Grid)
			ADD_WIDGET_BOOL("Generate single spawn point", g_SpawnData.bGenerateSingleCustomSpawnPoint)
			ADD_WIDGET_INT_SLIDER("Currently selected spawn point", g_SpawnData.iSelectedSpawnPointIndex, -1, MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS - 1, 1)
			ADD_WIDGET_BOOL("Autoincrement index", g_SpawnData.bAutoincrementGenerateSinglePointIndex)
			ADD_WIDGET_BOOL("Use mouse picking for moving points", g_SpawnData.bAllowPointsMousePicking)
			
			START_WIDGET_GROUP("Swap & remove")
				ADD_WIDGET_INT_SLIDER("Swap selected with this: ", g_SpawnData.iSpawnPointToSwapWith, -1, MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS - 1, 1)
				ADD_WIDGET_BOOL("Swap", g_SpawnData.bSwapCustomSpawnPoints)
				ADD_WIDGET_BOOL("Delete selected", g_SpawnData.bDeleteSelectedSpawnPoint)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_BOOL("bCustomSpawnPointsOutputCoords", g_SpawnData.bCustomSpawnPointsOutputCoords)
			ADD_WIDGET_BOOL("bCustomSpawnPointOutputProperty",g_SpawnData.bCustomSpawnPointOutputProperty)
			ADD_WIDGET_BOOL("Output custom spawn points for simple interior", g_SpawnData.bCustomSpawnPointOutputSimpleInterior)
			ADD_WIDGET_BOOL("Output custom spawn points for simple interior (Interior relative)", g_SpawnData.bCustomSpawnPointOutputSimpleInteriorINT)
			ADD_WIDGET_BOOL("Output custom spawn points for post delivery scene", g_SpawnData.bCustomSpawnPointOutputPostDeliveryScene)
			// read only
			START_WIDGET_GROUP("Internal System Info")
				ADD_WIDGET_INT_READ_ONLY("iNumberOfCustomSpawnPoints", g_SpawnData.CustomSpawnPointInfo.iNumberOfCustomSpawnPoints)
				ADD_WIDGET_BOOL("bUseCustomSpawnPoints", g_SpawnData.bUseCustomSpawnPoints)
			STOP_WIDGET_GROUP()
			
			
			START_WIDGET_GROUP("Custom Spawn Data")
				REPEAT MAX_NUMBER_OF_CUSTOM_SPAWN_POINTS i
					str = "Point "
					str += i	
					str += " coords"
					ADD_WIDGET_VECTOR_SLIDER(str, g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].vPos, -9999.9, 9999.9, 0.1)
					str = "Point "
					str += i	
					str += " heading"					
					ADD_WIDGET_FLOAT_SLIDER(str, g_SpawnData.CustomSpawnPointInfo.CustomSpawnPoints[i].fHeading, 0.0, 360.0, 1.0)
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		

		
		START_WIDGET_GROUP("NET_WARP_TO_COORD")


			ADD_WIDGET_BOOL("bRenderSpecificCoords", g_SpawnData.bRenderSpecificCoords)
			ADD_WIDGET_BOOL("Grab from current Pos", g_SpawnData.bGetSpecificCoordsFromPlayerPos)
			
			ADD_WIDGET_VECTOR_SLIDER("vCoords", g_SpecificSpawnLocation.vCoords, -9999.9, 9999.9, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("fHeading", g_SpecificSpawnLocation.fHeading, -9999.9, 9999.9, 0.1)

			ADD_WIDGET_BOOL("bkeepvehicle", g_SpawnData.b_NET_WARP_TO_COORD_keepvehicle)
			ADD_WIDGET_BOOL("bLeavePedBehind", g_SpawnData.b_NET_WARP_TO_COORD_LeavePedBehind)
			ADD_WIDGET_BOOL("bDontSetCameraBehindPlayer", g_SpawnData.b_NET_WARP_TO_COORD_DontSetCameraBehindPlayer)
			ADD_WIDGET_BOOL("bKillLeftBehindPed", g_SpawnData.b_NET_WARP_TO_COORD_KillLeftBehindPed)
			ADD_WIDGET_BOOL("bKeepPortableObjects", g_SpawnData.b_NET_WARP_TO_COORD_KeepPortableObjects)
			ADD_WIDGET_BOOL("bDoQuickWarp", g_SpawnData.b_NET_WARP_TO_COORD_DoQuickWarp)
			ADD_WIDGET_BOOL("bSnapToGround", g_SpawnData.b_NET_WARP_TO_COORD_SnapToGround)
			ADD_WIDGET_BOOL("bIgnoreAlreadyThereCheck", g_SpawnData.b_NET_WARP_TO_COORD_IgnoreAlreadyThereCheck)
			
			ADD_WIDGET_BOOL("call function", g_SpawnData.bTest_NET_WARP_TO_COORD)
			
			START_WIDGET_GROUP("Debug")
				ADD_WIDGET_BOOL("MPGlobals.g_NetWarpStarted", MPGlobals.g_NetWarpStarted)	
				ADD_WIDGET_BOOL("MPGlobals.g_NetWarpReStarted", MPGlobals.g_NetWarpReStarted)	
				ADD_WIDGET_VECTOR_SLIDER("MPGlobals.g_NetWarpCoords", MPGlobals.g_NetWarpCoords, -99999.9, 99999.9, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("MPGlobals.g_NetWarpHeading", MPGlobals.g_NetWarpHeading, -360, 360, 0.1)
			STOP_WIDGET_GROUP()			
			
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("SEA Safe Spawn")
			ADD_WIDGET_BOOL("bShowAllSafeSea",  g_SpawnData.bShowAllSafeSea)	
			ADD_WIDGET_BOOL("bOutputAllSafeSea",  g_SpawnData.bOutputAllSafeSea)	
			ADD_WIDGET_INT_SLIDER("iWarpToSafeSea", g_SpawnData.iWarpToSafeSea, -1, NUM_SAFE_SPAWN_IN_SEA, 1)	
			ADD_WIDGET_BOOL("bRecordSeaPoints", g_SpawnData.bRecordSeaPoints)
			ADD_WIDGET_INT_SLIDER("iRecordedSeaPoint", g_SpawnData.iRecordedSeaPoint, -1, NUM_SAFE_SPAWN_IN_SEA, 1)
			ADD_WIDGET_VECTOR_SLIDER("vRecordedSeaPoint", g_SpawnData.vRecordedSeaPoint, -10000.0, 10000.0, 0.1)
			ADD_WIDGET_INT_SLIDER("iRecordingSeaDist", g_SpawnData.iRecordingSeaDist, -1, 1000, 1)	
			REPEAT NUM_SAFE_SPAWN_IN_SEA i
				str = "SafeSpawn_Sea["
				str += i
				str += "]"
				ADD_WIDGET_VECTOR_SLIDER(str, SafeSpawn_Sea[i], -10000.0, 10000.0, 0.1)
			ENDREPEAT							
		STOP_WIDGET_GROUP()	
		
//		START_WIDGET_GROUP("AIR Safe Spawn")
//			ADD_WIDGET_BOOL("bShowAllSafeAir",  g_SpawnData.bShowAllSafeAir)	
//			ADD_WIDGET_BOOL("bOutputAllSafeAir",  g_SpawnData.bOutputAllSafeAir)	
//			ADD_WIDGET_INT_SLIDER("iWarpToSafeAir", g_SpawnData.iWarpToSafeAir, -1, NUM_SAFE_SPAWN_IN_AIR, 1)	
//			ADD_WIDGET_BOOL("bGrabAirGroundZ", g_SpawnData.bGrabAirGroundZ)
//			REPEAT NUM_SAFE_SPAWN_IN_AIR i
//				str = "SafeSpawn_Air["
//				str += i
//				str += "]"
//				ADD_WIDGET_VECTOR_SLIDER(str, SafeSpawn_Air[i], -10000.0, 10000.0, 0.1)
//			ENDREPEAT
//		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Hospital Respawns")
			ADD_WIDGET_BOOL("bShowAllHospital",  g_SpawnData.bShowAllHospital)				
			REPEAT TOTAL_NUMBER_OF_RESPAWN_HOSPITALS i
				str = "SpawnAreas_Hospitals[" 
				str += i
				str += "]"
				START_WIDGET_GROUP(str)
					ADD_WIDGET_FOR_SPAWN_AREA(SpawnAreas_Hospitals[i])	
				STOP_WIDGET_GROUP()
			ENDREPEAT	
		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP("Hotel Respawns")
			ADD_WIDGET_BOOL("bShowAllHotels",  g_SpawnData.bShowAllHotels)				
			REPEAT TOTAL_NUMBER_OF_RESPAWN_HOTELS i
				str = "SpawnAreas_Hotels[" 
				str += i
				str += "]"
				START_WIDGET_GROUP(str)
					ADD_WIDGET_FOR_SPAWN_AREA(SpawnAreas_Hotels[i])	
				STOP_WIDGET_GROUP()
			ENDREPEAT	
		STOP_WIDGET_GROUP()
		
//		START_WIDGET_GROUP("Interior Bleeding")
//			ADD_WIDGET_BOOL("bShowAllInteriorBleeds",  g_SpawnData.bShowAllInteriorBleeds)				
//			REPEAT TOTAL_NUMBER_OF_INTERIOR_BLEEDS i
//				str = "InteriorBleed[" 
//				str += i
//				str += "]"
//				START_WIDGET_GROUP(str)
//					ADD_WIDGET_FOR_PROBLEM_AREA(ProblemArea_InteriorBleed[i])	
//				STOP_WIDGET_GROUP()
//			ENDREPEAT				
//		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Forbidden Areas")
			ADD_WIDGET_BOOL("bShowAllForbiddenAreas",  g_SpawnData.bShowAllForbiddenAreas)				
			REPEAT NUMBER_OF_SPAWNING_SECTORS i	
				REPEAT g_iTotalForbiddenProblemAreasForSector[i] j
					str = "Forbidden[" 
					str += i
					str += "]["
					str += j
					str += "]"
					START_WIDGET_GROUP(str)
						ADD_WIDGET_FOR_PROBLEM_AREA(ProblemArea_Forbidden[i][j])	
					STOP_WIDGET_GROUP()
				ENDREPEAT
			ENDREPEAT				
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Spawn sectors")
			ADD_WIDGET_BOOL("bShowAllSpawningSectors",  g_SpawnData.bShowAllSpawningSectors)	
			ADD_WIDGET_FLOAT_SLIDER("g_fSpawningSectorXLine[0]", g_fSpawningSectorXLine[0], LOWEST_INT, HIGHEST_INT, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("g_fSpawningSectorXLine[1]", g_fSpawningSectorXLine[1], LOWEST_INT, HIGHEST_INT, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("g_fSpawningSectorXLine[2]", g_fSpawningSectorXLine[2], LOWEST_INT, HIGHEST_INT, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("g_fSpawningSectorXLine[3]", g_fSpawningSectorXLine[3], LOWEST_INT, HIGHEST_INT, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("g_fSpawningSectorYLine[0]", g_fSpawningSectorYLine[0], LOWEST_INT, HIGHEST_INT, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("g_fSpawningSectorYLine[1]", g_fSpawningSectorYLine[1], LOWEST_INT, HIGHEST_INT, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("g_fSpawningSectorYLine[2]", g_fSpawningSectorYLine[2], LOWEST_INT, HIGHEST_INT, 0.01)			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Problem Node Areas")
			ADD_WIDGET_BOOL("bShowAllProblemNodeAreas",  g_SpawnData.bShowAllProblemNodeAreas)				
			REPEAT NUMBER_OF_SPAWNING_SECTORS i	
				REPEAT g_iTotalNodesProblemAreasForSector[i] j
					str = "ProblemArea_Nodes[" 
					str += i
					str += "]["
					str += j
					str += "]"
					START_WIDGET_GROUP(str)
						ADD_WIDGET_FOR_PROBLEM_AREA(ProblemArea_Nodes[i][j])	
					STOP_WIDGET_GROUP()
				ENDREPEAT
			ENDREPEAT				
		STOP_WIDGET_GROUP()		
		
		START_WIDGET_GROUP("Problem Min Airplane Height")
			ADD_WIDGET_BOOL("bShowAllProblemMinAirplaneHeight",  g_SpawnData.bShowAllProblemMinAirplaneHeight)				
			REPEAT NUMBER_OF_MIN_AIRPLANE_HEIGHT_AREAS i
				str = "ProblemArea_MinAirplaneHeight[" 
				str += i
				str += "]"
				START_WIDGET_GROUP(str)
					ADD_WIDGET_FOR_PROBLEM_AREA(ProblemArea_MinAirplaneHeight[i])	
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()		
		
		START_WIDGET_GROUP("Problem NodeSearch Areas")
			REPEAT NUMBER_OF_PROBLEM_NODESEARCH_AREAS i
				str = "ProblemArea_NodeSearch[" 
				str += i
				str += "]"
				START_WIDGET_GROUP(str)
					ADD_WIDGET_FOR_PROBLEM_AREA(ProblemArea_NodeSearch[i])	
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
		START_WIDGET_GROUP("Custom vehicle node Areas")
			REPEAT NUMBER_OF_CUSTOM_VEHICLE_NODE_AREAS i
				str = "ProblemArea_CustomVehicleNodes[" 
				str += i
				str += "]"
				START_WIDGET_GROUP(str)
					ADD_WIDGET_FOR_PROBLEM_AREA(ProblemArea_CustomVehicleNodes[i])	
				STOP_WIDGET_GROUP()
			ENDREPEAT				
		STOP_WIDGET_GROUP()	
		
		START_WIDGET_GROUP("Transition")
			ADD_WIDGET_BOOL("bForceJoinAtCoords", bForceJoinAtCoords)
			ADD_WIDGET_VECTOR_SLIDER("vForceJoinCoords", vForceJoinCoords, -99999.9, 99999.9, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("fForceJoinHeading", fForceJoinHeading, 0.0, 360.0, 0.001)
			ADD_WIDGET_BOOL("bGrabForceJoinCoords", bGrabForceJoinCoords)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Spawn Activity")
			
			ADD_WIDGET_INT_SLIDER("iJoiningSpawnActivityOverride", iJoiningSpawnActivityOverride, -1, 99, 1)
			ADD_WIDGET_INT_SLIDER("iJoiningSpawnInteractionTypeOverride", iJoiningSpawnInteractionTypeOverride, -1, 99, 1)
			ADD_WIDGET_INT_SLIDER("iJoiningSpawnInteractionAnimOverride", iJoiningSpawnInteractionAnimOverride, -1, 99, 1)			
			ADD_WIDGET_INT_SLIDER("iEntryWantedLevel", g_TransitionSpawnData.iEntryWantedLevel, -1, 99, 1)			
			ADD_WIDGET_BOOL("g_TransitionSpawnData.bIgnoreSpawnActivity", g_TransitionSpawnData.bIgnoreSpawnActivity)			
			ADD_WIDGET_BOOL("bTestSpawnActivity", bTestSpawnActivity)		
			ADD_WIDGET_BOOL("bSpawnInProperty_StartActivity",	g_TransitionSpawnData.bSpawnInProperty_StartActivity)
			ADD_WIDGET_BOOL("bSpawnActivtyObjectScriptReady", g_TransitionSpawnData.bSpawnActivtyObjectScriptReady)
			
			#IF FEATURE_FIXER
			ADD_WIDGET_INT_SLIDER("g_iPostMissionSpawnScenario", g_iPostMissionSpawnScenario, -1, (ENUM_TO_INT(POST_MISSION_SPAWN_TOTAL_NUMBER) - 1), 1)
			ADD_WIDGET_INT_SLIDER("g_iSittingSmokingLocationOverride", g_iSittingSmokingLocationOverride, -1, 15, 1)
			ADD_WIDGET_INT_SLIDER("g_iMusicStudioDrunkSpawnLocation", g_iMusicStudioDrunkSpawnLocation, -1, 10, 1)
			#ENDIF
			
			START_WIDGET_GROUP("Spawn Activity")			
				ADD_WIDGET_INT_SLIDER("iSpawnActivity", g_TransitionSpawnData.iSpawnActivity, -1, 999, 1)				
				ADD_WIDGET_INT_SLIDER("iSpawnActivityState", g_TransitionSpawnData.iSpawnActivityState, -1, 999, 1)
				ADD_WIDGET_BOOL("bSpawnActivityReady", g_TransitionSpawnData.bSpawnActivityReady)
			
				
				START_WIDGET_GROUP("personal vehicle")
					ADD_WIDGET_INT_SLIDER("iSpawnInSavedVehicleState", g_TransitionSpawnData.iSpawnInSavedVehicleState, -1, 999, 1)	
					ADD_WIDGET_BOOL("bSpawnNearbySavedVehicleUseRoadOffset", g_TransitionSpawnData.bSpawnNearbySavedVehicleUseRoadOffset)
					ADD_WIDGET_BOOL("bSpawnNearbySavedVehicle", g_TransitionSpawnData.bSpawnNearbySavedVehicle)
					ADD_WIDGET_BOOL("bWarpPlayerIntoSavedVehicle", g_TransitionSpawnData.bWarpPlayerIntoSavedVehicle)
					ADD_WIDGET_BOOL("bSwitchSavedVehicleEngineOn", g_TransitionSpawnData.bSwitchSavedVehicleEngineOn)
					ADD_WIDGET_BOOL("bSpawnVehicleInExactCoordsIfPossible", g_TransitionSpawnData.bSpawnVehicleInExactCoordsIfPossible)
					ADD_WIDGET_FLOAT_SLIDER("fSpawnVehicleMaxRange", g_TransitionSpawnData.fSpawnVehicleMaxRange, 0.0, 1000.0, 0.01)
					ADD_WIDGET_BOOL("bSpawnNearbyRandomVehicle", g_TransitionSpawnData.bSpawnNearbyRandomVehicle)					
				STOP_WIDGET_GROUP()			
			
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Spawn Interaction")
				ADD_WIDGET_INT_SLIDER("iSpawnInteractionType", g_TransitionSpawnData.iSpawnInteractionType, -1, 999, 1)	
				ADD_WIDGET_INT_SLIDER("iSpawnInteractionAnim", g_TransitionSpawnData.iSpawnInteractionAnim, -1, 999, 1)
				ADD_WIDGET_INT_SLIDER("iSpawnInteractionState", g_TransitionSpawnData.iSpawnInteractionState, -1, 999, 1)
				ADD_WIDGET_BOOL("bSpawnInteractionReady", g_TransitionSpawnData.bSpawnInteractionReady)
			STOP_WIDGET_GROUP()
							

			START_WIDGET_GROUP("camera settings")
				ADD_WIDGET_FLOAT_SLIDER("fSACam1OffsetX", fSACam1OffsetX, -10.0, 10.0, 0.001)	
				ADD_WIDGET_FLOAT_SLIDER("fSACam1OffsetY", fSACam1OffsetY, -10.0, 10.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("fSACam1OffsetZ", fSACam1OffsetZ, -10.0, 10.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("fSACam1PointOffsetX", fSACam1PointOffsetX, -10.0, 10.0, 0.001)	
				ADD_WIDGET_FLOAT_SLIDER("fSACam1PointOffsetY", fSACam1PointOffsetY, -10.0, 10.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("fSACam1PointOffsetZ", fSACam1PointOffsetZ, -10.0, 10.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("fSACam2OffsetX", fSACam2OffsetX, -10.0, 10.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("fSACam2OffsetY", fSACam2OffsetY, -10.0, 10.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("fSACam2OffsetZ", fSACam2OffsetZ, -10.0, 10.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("fSACam2PointOffsetX", fSACam2PointOffsetX, -10.0, 10.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("fSACam2PointOffsetY", fSACam2PointOffsetY, -10.0, 10.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("fSACam2PointOffsetZ", fSACam2PointOffsetZ, -10.0, 10.0, 0.001)
				
				ADD_WIDGET_FLOAT_SLIDER("fSACamFOV", fSACamFOV, 0.0, 100.0, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("fSACamShake", fSACamShake, 0.0, 100.0, 0.001)				
				ADD_WIDGET_INT_SLIDER("iSACamDuration", iSACamDuration, 0, HIGHEST_INT, 1)

			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_BOOL("bStartedTimer", g_TransitionSpawnData.bStartedTimer)
			
		STOP_WIDGET_GROUP()
		

//		START_WIDGET_GROUP("invincible flashing")
//			ADD_WIDGET_INT_SLIDER("INVINCIBLE_FLASH_TIME", INVINCIBLE_FLASH_TIME, 0, HIGHEST_INT, 1)
//			ADD_WIDGET_BOOL("test", g_SpawnData.bTestFlashInvincible)
//		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Shape Test")
			ADD_WIDGET_BOOL("g_SpawnData.bTestShapeTest", g_SpawnData.bTestShapeTest)
			ADD_WIDGET_BOOL("g_SpawnData.bDontResetShapeTestFlag", g_SpawnData.bDontResetShapeTestFlag)
			ADD_WIDGET_BOOL("g_SpawnData.bRenderShapeTest", g_SpawnData.bRenderShapeTest)
			ADD_WIDGET_BOOL("g_SpawnData.bDoShapeTest", g_SpawnData.bDoShapeTest)
			ADD_WIDGET_FLOAT_SLIDER("SPAWN_SHAPETEST_WIDTH", SPAWN_SHAPETEST_WIDTH, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("SPAWN_SHAPETEST_MAX_ROAD_DIST", SPAWN_SHAPETEST_MAX_ROAD_DIST, 0.0, 100.0, 0.1)
			//ADD_WIDGET_FLOAT_SLIDER("SPAWNING_SHAPE_TEST_PROBE_DIST", SPAWNING_SHAPE_TEST_PROBE_DIST, 0.0, 10.0, 0.1)

		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("MissionSpawnDetails")
			ADD_WIDGET_FOR_MISSION_SPAWN_DETAILS(g_SpawnData.MissionSpawnDetails)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("System Debug")

			ADD_WIDGET_BOOL("bForceNodeSearchFail", g_SpawnData.bForceNodeSearchFail)


			START_WIDGET_GROUP("WarpPlayerIntoCar")
				ADD_WIDGET_BOOL("call", g_SpawnData.bCall_WarpPlayerIntoCar)				
				ADD_WIDGET_INT_SLIDER("into seat", g_SpawnData.iCar_seat, -2, 32, 1)
				ADD_WIDGET_BOOL("force", g_SpawnData.bWarpPlayerIntoCar_Force)
				ADD_WIDGET_BOOL("use last car", g_SpawnData.bWarpPlayerIntoCar_LastCar)
				ADD_WIDGET_INT_SLIDER("use partner id", g_SpawnData.iPlayers_car, 0, 32, 1)
			STOP_WIDGET_GROUP()

			START_WIDGET_GROUP("test IS_SPAWN_AREA_ENTIRELY_INSIDE_ANGLED_AREA")
				ADD_WIDGET_BOOL("call", g_SpawnData.bCall_IS_SPAWN_AREA_ENTIRELY_INSIDE_ANGLED_AREA)
				ADD_WIDGET_VECTOR_SLIDER("vCoords1", g_SpawnData.vCoords1_TestSAEIAA, -99999.9, 99999.9, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("vCoords2", g_SpawnData.vCoords2_TestSAEIAA, -99999.9, 99999.9, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fFloat", g_SpawnData.fFloat_TestSAEIAA, -99999.9, 99999.9, 0.01)
			STOP_WIDGET_GROUP()

			ADD_WIDGET_BOOL("bDrawVectorMap", g_SpawnData.bDrawVectorMap)

			ADD_WIDGET_BOOL("bClearPVNetID", g_SpawnData.bClearPVNetID)
	
			ADD_WIDGET_INT_SLIDER("g_SpawnData.iAttempt", g_SpawnData.iAttempt, 0, 99, 1)
			
			ADD_WIDGET_INT_SLIDER("g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.iSpawnVehicleHorn", g_SpawnData.MissionSpawnDetails.SpawnVehicleSetupMP.iSpawnVehicleHorn, LOWEST_INT, HIGHEST_INT, 1) 

			ADD_WIDGET_BOOL("g_SpawnData.bSpawningInProperty", g_SpawnData.bSpawningInProperty)
			ADD_WIDGET_BOOL("g_SpawnData.bSpawningInGarage", g_SpawnData.bSpawningInGarage)
			
			ADD_WIDGET_BOOL("bGiveHelmet", g_SpawnData.bGiveHelmet)
			ADD_WIDGET_BOOL("bDrawCodeAngleAreas", g_SpawnData.bDrawCodeAngleAreas)
#IF ADD_WIDGETS_FOR_SPAWNING
			ADD_WIDGET_FLOAT_SLIDER("GET_SAFE_COORD_FINAL_DISTANCE", GET_SAFE_COORD_FINAL_DISTANCE, 0.0, 1000.0, 1.0)
#ENDIF	//	ADD_WIDGETS_FOR_SPAWNING
//			ADD_WIDGET_BOOL("g_bTellMPHudToQuitForInterpCutscene", g_bTellMPHudToQuitForInterpCutscene)
			
			ADD_WIDGET_BOOL("g_SpawnData.bTellHUDNotToMovePlayer", g_SpawnData.bTellHUDNotToMovePlayer)
			ADD_WIDGET_BOOL("g_SpawnData.bDontBringUpHudWhenDead", g_SpawnData.bDontBringUpHudWhenDead)	
			ADD_WIDGET_BOOL("g_MPCharData.bPlayerWasDead", g_MPCharData.bPlayerWasDead)	
			ADD_WIDGET_BOOL("g_MPCharData.bPerformingTeamSwap", g_MPCharData.bPerformingTeamSwap)
			ADD_WIDGET_BOOL("g_SpawnData.bHasQueriedSpecificPosition", g_SpawnData.bHasQueriedSpecificPosition)
			ADD_WIDGET_INT_SLIDER("g_SpawnData.iQuestSpecificSpawnReply", g_SpawnData.iQuestSpecificSpawnReply, -1, 99, 1) 
			ADD_WIDGET_INT_SLIDER("g_SpawnData.iQuerySpecificSpawnState", g_SpawnData.iQuerySpecificSpawnState, -1, 99, 1) 
			
			ADD_WIDGET_INT_SLIDER("iEuler", g_SpawnData.iEuler, 0, 6, 1) 
			
			ADD_WIDGET_BOOL("g_SpawnData.bPassedOutDrunk", g_SpawnData.bPassedOutDrunk)
			
#IF ADD_WIDGETS_FOR_SPAWNING
			ADD_WIDGET_INT_SLIDER("LOAD_SCENE_TIME_OUT", LOAD_SCENE_TIME_OUT, 0, HIGHEST_INT, 1)
			ADD_WIDGET_FLOAT_SLIDER("RESPAWN_DO_LOAD_SCENE_DIST", RESPAWN_DO_LOAD_SCENE_DIST, 0.0, 1000.0, 1.0)
			
			ADD_WIDGET_FLOAT_SLIDER("CAR_NODE_MIN_DISTANCE", CAR_NODE_MIN_DISTANCE, 0.0, 100.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("CAR_NODE_MAX_DISTANCE", CAR_NODE_MAX_DISTANCE, 0.0, 100.0, 1.0)
			ADD_WIDGET_FLOAT_SLIDER("CAR_NODE_MAX_Z_DISTANCE", CAR_NODE_MAX_Z_DISTANCE, 0.0, 100.0, 1.0)
			
			ADD_WIDGET_FLOAT_SLIDER("AREA_OCCUPIED_RADIUS", AREA_OCCUPIED_RADIUS, 0.0, 5.0, 0.01)
#ENDIF	//	ADD_WIDGETS_FOR_SPAWNING
			
			  
			ADD_WIDGET_BOOL("bDoNearARoadChecks", g_SpawnData.MissionSpawnDetails.bDoNearARoadChecks) 
//			ADD_WIDGET_BOOL("bUseFOVFlag",g_SpawnData.bUseFOVFlag)

			ADD_WIDGET_INT_SLIDER("g_SpawnData.iAlphaValue", g_SpawnData.iAlphaValue, 0, 255, 1)
			ADD_WIDGET_FLOAT_SLIDER("g_SpawnData.fFaceOffset", g_SpawnData.fFaceOffset, 0.0, 5.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("g_SpawnData.fDebugSphereScale", g_SpawnData.fDebugSphereScale, 0.0, 5.0, 0.001)
			
			ADD_WIDGET_BOOL("g_SpawnData.bSpawningInProperty", g_SpawnData.bSpawningInProperty)
			
			
			START_WIDGET_GROUP("g_SpawnData.Params")
				ADD_WIDGET_FOR_SPAWN_SEARCH_PARAMS(g_SpawnData.PublicParams)	
				ADD_WIDGET_FOR_PRIVATE_SPAWN_SEARCH_PARAMS(g_SpawnData.PrivateParams)	
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Interior Spawning")
				ADD_WIDGET_BOOL("bConsiderInteriors", g_SpawnData.MissionSpawnDetails.bConsiderInteriors)
				ADD_WIDGET_INT_SLIDER("iDeathInteriorGroup", g_SpawnData.MissionSpawnDetails.iDeathInteriorGroup, -1, HIGHEST_INT, 1)				
				ADD_WIDGET_BOOL("bTestInteriorCoord", g_SpawnData.bTestInteriorCoord)
				ADD_WIDGET_VECTOR_SLIDER("vTestInteriorCoord", g_SpawnData.vTestInteriorCoord, -99999.9, 99999.9, 0.001)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("SPECIFIC VEHICLE RESPAWNING")
				ADD_WIDGET_BOOL("bSetCurrentCarAsRespawnVehicle", g_SpawnData.bSetCurrentCarAsRespawnVehicle)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("SPAWN VEHICLE")
				ADD_WIDGET_BOOL("g_TransitionSpawnData.bSpawnNearbySavedVehicle", g_TransitionSpawnData.bSpawnNearbySavedVehicle)
				ADD_WIDGET_BOOL("g_TransitionSpawnData.bSpawnNearbySavedVehicleUseRoadOffset", g_TransitionSpawnData.bSpawnNearbySavedVehicleUseRoadOffset)
				ADD_WIDGET_INT_SLIDER("g_SpawnData.iSpawnNearbySavedState", g_SpawnData.iSpawnNearbySavedState, -1, 99, 1)
				ADD_WIDGET_INT_SLIDER("g_TransitionSpawnData.iSpawnInSavedVehicleState", g_TransitionSpawnData.iSpawnInSavedVehicleState, -1, 99, 1)
				ADD_WIDGET_BOOL("g_TransitionSpawnData.bWarpPlayerIntoSavedVehicle", g_TransitionSpawnData.bWarpPlayerIntoSavedVehicle)
				ADD_WIDGET_BOOL("g_TransitionSpawnData.bSwitchSavedVehicleEngineOn", g_TransitionSpawnData.bSwitchSavedVehicleEngineOn)
				ADD_WIDGET_BOOL("g_TransitionSpawnData.bFreezeSavedVehicle", g_TransitionSpawnData.bFreezeSavedVehicle)
				
				ADD_WIDGET_VECTOR_SLIDER("g_SpawnData.vSavedVehicleCoords", g_SpawnData.vSavedVehicleCoords, -99999.9, 99999.9, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("g_SpawnData.fSavedVehicleHeading", g_SpawnData.fSavedVehicleHeading, -99999.9, 99999.9, 0.1)
				
				// random vehicle
				ADD_WIDGET_BOOL("bSpawnNearbyRandomVehicle", g_TransitionSpawnData.bSpawnNearbyRandomVehicle)
				ADD_WIDGET_INT_SLIDER("g_SpawnData.iSpawnNearbyRandomState", g_SpawnData.iSpawnNearbyRandomState, -1, 99, 1)
				//ADD_WIDGET_INT_SLIDER("g_SpawnData.iSpawnInRandomVehicleState", g_SpawnData.iSpawnInRandomVehicleState, -1, 99, 1)

				ADD_WIDGET_BOOL("bCallSpawnRandomVehicleNearPlayer", g_SpawnData.bCallSpawnRandomVehicleNearPlayer)
				ADD_WIDGET_BOOL("bCallSpawnSavedVehicleNearPlayer", g_SpawnData.bCallSpawnSavedVehicleNearPlayer)
				
				ADD_WIDGET_INT_SLIDER("LANDING_GEAR_FRAME_DELAY", LANDING_GEAR_FRAME_DELAY, 0, 100, 1)
				
				//ADD_WIDGET_FLOAT_SLIDER("AREA_OCC_RADIUS_CHECK", AREA_OCC_RADIUS_CHECK, 0.0, 100.0, 0.01)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("SPAWN INFO WINDOW")				
				ADD_WIDGET_FLOAT_SLIDER("SpawnWindowX", g_SpawnData.MissionSpawnDetails.SpawnWindowX, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("SpawnWindowY", g_SpawnData.MissionSpawnDetails.SpawnWindowY, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("SpawnWindowW", g_SpawnData.MissionSpawnDetails.SpawnWindowW, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("SpawnWindowH", g_SpawnData.MissionSpawnDetails.SpawnWindowH, 0.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("SpawnWindowScale", g_SpawnData.MissionSpawnDetails.SpawnWindowScale, -1.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("SpawnWindowColumnX", g_SpawnData.MissionSpawnDetails.SpawnWindowColumnX, -1.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("SpawnWindowRowY", g_SpawnData.MissionSpawnDetails.SpawnWindowRowY, -1.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("SpawnWindowSpacerX", g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerX, -1.0, 1.0, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("SpawnWindowSpacerY", g_SpawnData.MissionSpawnDetails.SpawnWindowSpacerY, -1.0, 1.0, 0.01)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("GET_CLOSEST_VEHICLE")
				ADD_WIDGET_VECTOR_SLIDER("pos", g_SpawnData.vGET_CLOSEST_VEHICLE_coords, -99999.9, 99999.9, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("radius", g_SpawnData.fGET_CLOSEST_VEHICLE_dist, 0.0, 9999.9, 0.1)
				ADD_WIDGET_INT_SLIDER("model hash", g_SpawnData.iGET_CLOSEST_VEHICLE_modelHash, LOWEST_INT, HIGHEST_INT, 1)
				ADD_WIDGET_INT_SLIDER("search flags", g_SpawnData.iGET_CLOSEST_VEHICLE_searchflags, 0, HIGHEST_INT, 1)
				ADD_WIDGET_BOOL("call", g_SpawnData.bTest_GET_CLOSEST_VEHICLE)				
				ADD_WIDGET_VECTOR_SLIDER("return vector", g_SpawnData.vGET_CLOSEST_VEHICLE_return, -99999.9, 99999.9, 0.1)	
			STOP_WIDGET_GROUP()
				
		
			START_WIDGET_GROUP("GET_SPAWN_LOCATION")
#IF ADD_WIDGETS_FOR_SPAWNING
				ADD_WIDGET_INT_SLIDER("MAX_NUM_OF_GET_SPAWN_LOCATION_ATTEMPTS", MAX_NUM_OF_GET_SPAWN_LOCATION_ATTEMPTS, -1, 999, 1)
#ENDIF	//	ADD_WIDGETS_FOR_SPAWNING
				//ADD_WIDGET_INT_SLIDER("MAX_RESPAWN_NODES_CHECKED", MAX_RESPAWN_NODES_CHECKED, -1, 999, 1)
				//ADD_WIDGET_INT_SLIDER("MAX_VEHICLE_NODES_CHECKED", MAX_VEHICLE_NODES_CHECKED, -1, 999, 1)
#IF ADD_WIDGETS_FOR_SPAWNING
				ADD_WIDGET_INT_SLIDER("MAX_NUM_OF_FALLBACK_ATTEMPTS", MAX_NUM_OF_FALLBACK_ATTEMPTS, -1, 999, 1)
#ENDIF	//	ADD_WIDGETS_FOR_SPAWNING
			STOP_WIDGET_GROUP()
		
			START_WIDGET_GROUP("WARP_TO_SPAWN_LOCATION")
				ADD_WIDGET_VECTOR_SLIDER("g_SpawnData.vWarpToLocationCoords", g_SpawnData.vWarpToLocationCoords, -99999.9, 99999.9, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("g_SpawnData.fWarpToLocationHeading", g_SpawnData.fWarpToLocationHeading, -99999.9, 99999.9, 0.001)
				ADD_WIDGET_INT_SLIDER("GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iWarpToLocationState, -1, 999, 1)
			STOP_WIDGET_GROUP()
		
			START_WIDGET_GROUP("Sever Request Info")
				ADD_WIDGET_INT_SLIDER("MAX_NUM_OF_SERVER_WARP_REQUESTS", MAX_NUM_OF_SERVER_WARP_REQUESTS, -1, 999, 1)	
				ADD_WIDGET_INT_SLIDER("iWarpState", g_SpawnData.iWarpState, -1, 999, 1)
				ADD_WIDGET_INT_SLIDER("iWarpServerRequestCount", g_SpawnData.iWarpServerRequestCount, -1, 999, 1)
				//ADD_WIDGET_INT_SLIDER("iWarpRequestTime", g_SpawnData.iWarpRequestTime, -1, 999999999, 1)
				//ADD_WIDGET_VECTOR_SLIDER("vRequestedWarpCoords", g_SpawnData.vRequestedWarpCoords, -9999.9, 9999.9, 0.1)
				//ADD_WIDGET_FLOAT_SLIDER("fRequestedWarpHeading", g_SpawnData.fRequestedWarpHeading, -9999.9, 9999.9, 0.1)
				REPEAT NUM_OF_STORED_SPAWN_RESULTS i 
					str = "RequestedSpawnResults.vCoords["
					str += i
					str += "]"
					ADD_WIDGET_VECTOR_SLIDER(str, g_SpawnData.RequestedSpawnResults.vCoords[i], -9999.9, 9999.9, 0.1)
					str = "RequestedSpawnResults.fHeading["
					str += i
					str += "]"
					ADD_WIDGET_FLOAT_SLIDER(str, g_SpawnData.RequestedSpawnResults.fHeading[i], -9999.9, 9999.9, 0.1)
				ENDREPEAT
				ADD_WIDGET_BOOL("bServerRepliedToWarpRequest", g_SpawnData.bServerRepliedToWarpRequest)
				ADD_WIDGET_BOOL("bServerAgreedToWarpRequest", g_SpawnData.bServerAgreedToWarpRequest)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("CUSTOM VEHICLE NODES")
				ADD_WIDGET_BOOL("bGetCustomVehicleNodesForPlayerPosition", g_SpawnData.bGetCustomVehicleNodesForPlayerPosition)	
				ADD_WIDGET_BOOL("bClearCustomVehicleNodesForPlayerPosition", g_SpawnData.bClearCustomVehicleNodesForPlayerPosition)	
				ADD_WIDGET_BOOL("bRenderCustomVehicleNodesForPlayerPosition", g_SpawnData.bRenderCustomVehicleNodesForPlayerPosition)
				ADD_WIDGET_INT_SLIDER("g_SpawnData.iNumberOfCustomVehicleNodes", g_SpawnData.iNumberOfCustomVehicleNodes, -1, MAX_NUMBER_OF_CUSTOM_VEHICLE_NODES, 1)
			STOP_WIDGET_GROUP()
			
				
			
			START_WIDGET_GROUP("global server bd")				
				REPEAT NUM_NETWORK_PLAYERS i
					str = "bWarpRequestTimeIntialised["
					str += i
					str += "]"
					ADD_WIDGET_BOOL(str, GlobalServerBD.bWarpRequestTimeIntialised[i])	
					
					str = "bHasUpdatedWarpRequestTime["
					str += i
					str += "]"
					ADD_WIDGET_BOOL(str, GlobalServerBD.bHasUpdatedWarpRequestTime[i])
					
					str = "g_vWarpRequest["
					str += i
					str += "]"
					ADD_WIDGET_VECTOR_SLIDER(str, GlobalServerBD.g_vWarpRequest[i], -99999.9, 99999.9, 0.1)
					
					REPEAT NUM_SERVER_STORED_VEH_REQUESTS j
						
						str = "g_VehSpawnReqData["
						str += i
						str += "]["
						str += j
						str += "].vPos"
						ADD_WIDGET_VECTOR_SLIDER(str, GlobalServerBD.g_VehSpawnReqData[i][j].vPos, -99999.9, 99999.9, 0.1)
						
						str = "g_VehSpawnReqData["
						str += i
						str += "]["
						str += j
						str += "].fHeading"
						ADD_WIDGET_FLOAT_SLIDER(str, GlobalServerBD.g_VehSpawnReqData[i][j].fHeading, -99999.9, 99999.9, 0.1)
						
					ENDREPEAT				
														
				ENDREPEAT				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Respawning")
				ADD_WIDGET_INT_SLIDER("iRespawnState", GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iRespawnState, -1, 99999, 1)
				ADD_WIDGET_BOOL("g_SpawnData.bPlayerWantsToRespawn", g_SpawnData.bPlayerWantsToRespawn)
				ADD_WIDGET_BOOL("g_SpawnData.bSpawnLocHadFinalCheck", g_SpawnData.bSpawnLocHadFinalCheck)
				ADD_WIDGET_BOOL("g_SpawnData.bHasSpectatorCamBeenActive", g_SpawnData.bHasSpectatorCamBeenActive)
			STOP_WIDGET_GROUP()
			
#IF ADD_WIDGETS_FOR_SPAWNING
			START_WIDGET_GROUP("Spawn Radii")
				ADD_WIDGET_FLOAT_SLIDER("GANG_HOUSE_SPAWN_RADIUS", GANG_HOUSE_SPAWN_RADIUS, 0.0, 500.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("NEAR_TEAMMATES_SPAWN_RADIUS", NEAR_TEAMMATES_SPAWN_RADIUS, 0.0, 500.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("NEAR_OTHER_PLAYERS_SPAWN_RADIUS", NEAR_OTHER_PLAYERS_SPAWN_RADIUS, 0.0, 500.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("NEAR_CURRENT_POSITION_SPAWN_RADIUS", NEAR_CURRENT_POSITION_SPAWN_RADIUS, 0.0, 500.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("NEAR_BACK_OF_POLICE_STATION_RADIUS", NEAR_BACK_OF_POLICE_STATION_RADIUS, 0.0, 500.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("PARTNER_SPAWN_DISTANCE", PARTNER_SPAWN_DISTANCE, 0.0, 500.0, 0.1)
				ADD_WIDGET_FLOAT_SLIDER("SAFE_RESPAWN_SEARCH_RADIUS", SAFE_RESPAWN_SEARCH_RADIUS, 0.0, 500.0, 1.0)		
				ADD_WIDGET_FLOAT_SLIDER("SPAWNPOINT_SEARCH_MIN_RADIUS", SPAWNPOINT_SEARCH_MIN_RADIUS, 0.0, 20.0, 0.01)
			STOP_WIDGET_GROUP()
#ENDIF	//	ADD_WIDGETS_FOR_SPAWNING
			
			START_WIDGET_GROUP("Get Spawn Location")
				ADD_WIDGET_INT_SLIDER("g_SpawnData.iGetLocationState", g_SpawnData.iGetLocationState, -1, HIGHEST_INT, 1)
				//ADD_WIDGET_INT_SLIDER("g_SpawnData.iGetLocationTime", g_SpawnData.iGetLocationTime, -1, HIGHEST_INT, 1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Viewing Cone")
				ADD_WIDGET_BOOL("bShowViewingCone", g_SpawnData.bShowViewingCone)
#IF ADD_WIDGETS_FOR_SPAWNING
				ADD_WIDGET_FLOAT_SLIDER("SPAWN_VIEWING_RANGE", SPAWN_VIEWING_RANGE, 0.0, 200.0, 1.0)	
				ADD_WIDGET_FLOAT_SLIDER("SPAWN_VIEWING_WIDTH", SPAWN_VIEWING_WIDTH, 0.0, 100.0, 1.0)	
				ADD_WIDGET_FLOAT_SLIDER("SPAWN_VIEWING_MAX_Z_DIFF", SPAWN_VIEWING_MAX_Z_DIFF, 0.0, 20.0, 1.0)
#ENDIF	//	ADD_WIDGETS_FOR_SPAWNING
			STOP_WIDGET_GROUP()
		
			START_WIDGET_GROUP("GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY")				
				// input
				ADD_WIDGET_VECTOR_SLIDER("Coord1", g_SpawnData.vSafeCoordsPoint1, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Coord2", g_SpawnData.vSafeCoordsPoint2, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Float", g_SpawnData.fSafeCoordsFloat, 0.0, 250.0, 0.1)
				ADD_WIDGET_INT_SLIDER("Shape", g_SpawnData.iSafeCoordsShape, SPAWN_AREA_SHAPE_CIRCLE, SPAWN_AREA_SHAPE_ANGLED, 1)
				
				ADD_WIDGET_FOR_SPAWN_SEARCH_PARAMS(g_SpawnData.SafeSpawnSearchParams)				
					
				ADD_WIDGET_BOOL("Test", g_SpawnData.bTestSafeCoordsInArea)
				ADD_WIDGET_VECTOR_SLIDER("Result Coord", g_SpawnData.vSafeCoordsOut, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Result Heading", g_SpawnData.fSafeHeadingOut, 0.0, 360.0, 1.0)
				
				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("create ped with GET_SAFE_COORDS_IN_AREA_FOR_CREATING_ENTITY")
				// input
				ADD_WIDGET_VECTOR_SLIDER("Coord1", g_SpawnData.vSafeCoordsPoint1, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Coord2", g_SpawnData.vSafeCoordsPoint2, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Float", g_SpawnData.fSafeCoordsFloat, 0.0, 250.0, 0.1)
				ADD_WIDGET_INT_SLIDER("Shape", g_SpawnData.iSafeCoordsShape, SPAWN_AREA_SHAPE_CIRCLE, SPAWN_AREA_SHAPE_ANGLED, 1)
				
				ADD_WIDGET_FOR_SPAWN_SEARCH_PARAMS(g_SpawnData.SafeSpawnSearchParams)				
					
				ADD_WIDGET_BOOL("create", g_SpawnData.bCreateMissionPedWithSafeCoords)
				ADD_WIDGET_VECTOR_SLIDER("Result Coord", g_SpawnData.vSafeCoordsOut, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Result Heading", g_SpawnData.fSafeHeadingOut, 0.0, 360.0, 1.0)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("HAS_GOT_VEHICLE_SPAWN_LOCATION_NEAR_COORDS")
			
				ADD_WIDGET_VECTOR_SLIDER("Start coords", g_SpawnData.vSpawnVehicleStartCoords, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("Facing coords", g_SpawnData.vSpawnVehicleFacingCoords, -9999.9, 9999.9, 0.01)
				
				ADD_WIDGET_BOOL("Grab player pos for start", g_SpawnData.bUsePlayerPositionForSpawnVehicle)
				
				ADD_WIDGET_BOOL("Use road offset", g_SpawnData.bSpawnVehicleUseRoadOffset)
				
				ADD_WIDGET_VECTOR_SLIDER("output coords", g_SpawnData.vSpawnVehicleCoords, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("output heading", g_SpawnData.fSpawnVehicleHeading, -9999.9, 9999.9, 0.01)
				
				ADD_WIDGET_FLOAT_SLIDER("min dist from coords", g_SpawnData.fSpawnVehicleMinDistFromCoords, 0.0, 100.0, 0.01)				
				
				ADD_WIDGET_BOOL("test", g_SpawnData.bTestSpawnVehicle)
				
				ADD_WIDGET_BOOL("bSpawnVehicleConsiderHighways", g_SpawnData.bSpawnVehicleConsiderHighways)
				ADD_WIDGET_BOOL("bSpawnVehicleInExactCoordsIfPossible", g_SpawnData.bSpawnVehicleInExactCoordsIfPossible)
				ADD_WIDGET_FLOAT_SLIDER("g_SpawnData.fSpawnVehicleMaxRange", g_SpawnData.fSpawnVehicleMaxRange, -1.0, 10000.0, 0.001)			
				ADD_WIDGET_BOOL("bTestOnlyActiveNodes", g_SpawnData.bTestOnlyActiveNodes)
				ADD_WIDGET_BOOL("bTestAvoidExclusion", g_SpawnData.bTestAvoidExclusion)
				ADD_WIDGET_BOOL("bTestBoatNodesOnly", g_SpawnData.bTestBoatNodesOnly)
				
				ADD_WIDGET_BOOL("render result", g_SpawnData.bRenderSpawnVehicleResult)
			
				START_WIDGET_GROUP("debug")
					ADD_WIDGET_INT_SLIDER("g_SpawnData.iSpawnVehicleState", g_SpawnData.iSpawnVehicleState, -1, 99, 1)
					ADD_WIDGET_INT_SLIDER("g_SpawnData.iSpawnVehicleServerRequestCount", g_SpawnData.iSpawnVehicleServerRequestCount, -1, 99, 1)
					ADD_WIDGET_VECTOR_SLIDER("g_SpawnData.vRequestedSpawnVehicleCoords", g_SpawnData.vRequestedSpawnVehicleCoords, -99999.9, 99999.9, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("g_SpawnData.fRequestedSpawnVehicleHeading", g_SpawnData.fRequestedSpawnVehicleHeading, -99999.9, 99999.9, 0.1)
					ADD_WIDGET_BOOL("g_SpawnData.bServerRepliedToSpawnVehicleRequest", g_SpawnData.bServerRepliedToSpawnVehicleRequest)
					ADD_WIDGET_BOOL("g_SpawnData.bServerAgreedToSpawnVehicleRequest", g_SpawnData.bServerAgreedToSpawnVehicleRequest)	
				STOP_WIDGET_GROUP()
			
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Exclusion radius")
				ADD_WIDGET_VECTOR_SLIDER("vExclusionCoord", g_SpawnData.vExclusionCoord, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fNearExclusionRadius", g_SpawnData.fNearExclusionRadius, 0.0, 250.0, 0.1)				
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Screen blur")
				ADD_WIDGET_BOOL("blur", g_SpawnData.bBlurForSpawn)		
				ADD_WIDGET_BOOL("clear", g_SpawnData.bClearBlurForSpawn)
				ADD_WIDGET_BOOL("g_SpawnData.bIsBlurredOut", g_SpawnData.bIsBlurredOut)
			STOP_WIDGET_GROUP()
			
//			START_WIDGET_GROUP("camera swoop")
//				ADD_WIDGET_BOOL("g_SpawnData.bIsSwoopedUp", g_SpawnData.bIsSwoopedUp)
//				ADD_WIDGET_VECTOR_SLIDER("g_SpawnData.vStartSwoop", g_SpawnData.vStartSwoop, -99999.9, 99999.9, 0.01)
//				ADD_WIDGET_VECTOR_SLIDER("g_SpawnData.vApproxEndSwoop", g_SpawnData.vApproxEndSwoop, -99999.9, 99999.9, 0.01)
//			STOP_WIDGET_GROUP()
		
		
			START_WIDGET_GROUP("Spawn search override")
				ADD_WIDGET_BOOL("bOverrideSpawnSearchResults", bOverrideSpawnSearchResults)	
				ADD_WIDGET_VECTOR_SLIDER("bOverrideSpawnSearchPos", bOverrideSpawnSearchPos, -99999.9, 99999.9, 0.001)
				ADD_WIDGET_FLOAT_SLIDER("fOverrideSpawnSearchHeading", fOverrideSpawnSearchHeading, -99999.9, 99999.9, 0.001)
			STOP_WIDGET_GROUP()

		
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("property spawning")
			ADD_WIDGET_BOOL("bShowPropertySpawnInfo", g_SpawnData.bShowPropertySpawnInfo)
			ADD_WIDGET_INT_SLIDER("iHighlightedProperty", g_SpawnData.iHighlightedProperty, 0, MP_PROPERTY_BUILDING_MAX, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Personal Vehicle Spawning")
			ADD_WIDGET_BOOL("bOverrideValidPVModelCheck", g_SpawnData.bOverrideValidPVModelCheck)
			
			START_WIDGET_GROUP("MissionSpawnPersonalVehicleData.")
				ADD_WIDGET_BOOL("bDisablePersonalVehicleCreation", g_SpawnData.MissionSpawnPersonalVehicleData.bDisablePersonalVehicleCreation)	
				ADD_WIDGET_BOOL("bDisablePersonalVehicleCreationForMissionStartup", g_SpawnData.MissionSpawnPersonalVehicleData.bDisablePersonalVehicleCreationForMissionStartup)	
				ADD_WIDGET_BOOL("bMissionWaitingForPVCreation", g_SpawnData.MissionSpawnPersonalVehicleData.bMissionWaitingForPVCreation)	
				ADD_WIDGET_BOOL("bMissionOKToProceedAfterPVCreation", g_SpawnData.MissionSpawnPersonalVehicleData.bMissionOKToProceedAfterPVCreation)	
				ADD_WIDGET_INT_SLIDER("iPVPropertySpawn", g_SpawnData.MissionSpawnPersonalVehicleData.iPVPropertySpawn, -1, HIGHEST_INT, 1)
				ADD_WIDGET_BOOL("bPVIsQuickRestart", g_SpawnData.MissionSpawnPersonalVehicleData.bPVIsQuickRestart)	
				ADD_WIDGET_BOOL("bHidePersonalVehicles", g_SpawnData.MissionSpawnPersonalVehicleData.bHidePersonalVehicles)	
				ADD_WIDGET_VECTOR_SLIDER("vPersonalVehicleNoSpawnZone_Min", g_SpawnData.MissionSpawnPersonalVehicleData.vPersonalVehicleNoSpawnZone_Min, -99999.9, 99999.9, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("vPersonalVehicleNoSpawnZone_Max", g_SpawnData.MissionSpawnPersonalVehicleData.vPersonalVehicleNoSpawnZone_Max, -99999.9, 99999.9, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fPersonalVehicleNoSpawnFloat", g_SpawnData.MissionSpawnPersonalVehicleData.fPersonalVehicleNoSpawnFloat, -99999.9, 99999.9, 0.01)
				ADD_WIDGET_INT_SLIDER("iPersonalVehicleNoSpawnShape", g_SpawnData.MissionSpawnPersonalVehicleData.iPersonalVehicleNoSpawnShape, -1, HIGHEST_INT, 1)
				ADD_WIDGET_BOOL("bPersonalVehicleNoSpawnZoneEnabled", g_SpawnData.MissionSpawnPersonalVehicleData.bPersonalVehicleNoSpawnZoneEnabled)	
			STOP_WIDGET_GROUP()

		STOP_WIDGET_GROUP()
		
		
		START_WIDGET_GROUP("spawn cam")
			ADD_WIDGET_BOOL("bUpdateSpawnCam", g_SpawnData.bUpdateSpawnCam)

			ADD_WIDGET_FLOAT_SLIDER("SPAWN_CAM_OFFSET_X", SPAWN_CAM_OFFSET_X, -100.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("SPAWN_CAM_OFFSET_Y", SPAWN_CAM_OFFSET_Y, -100.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("SPAWN_CAM_OFFSET_Z", SPAWN_CAM_OFFSET_Z, -100.0, 100.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("SPAWN_CAM_POINT_OFFSET_X", SPAWN_CAM_POINT_OFFSET_X, -100.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("SPAWN_CAM_POINT_OFFSET_Y", SPAWN_CAM_POINT_OFFSET_Y, -100.0, 100.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("SPAWN_CAM_POINT_OFFSET_Z", SPAWN_CAM_POINT_OFFSET_Z, -100.0, 100.0, 0.01)
			
			ADD_WIDGET_FLOAT_SLIDER("SPAWN_CAM_FOV", SPAWN_CAM_FOV, 0.0, 100.0, 0.01)
			
			ADD_WIDGET_BOOL("bSpawnCamIgnoreShapeTest", g_SpawnData.bSpawnCamIgnoreShapeTest)
			ADD_WIDGET_INT_SLIDER("iSpawnCamState", g_SpawnData.iSpawnCamState, -1, 99, 1)
		STOP_WIDGET_GROUP()		
		
		START_WIDGET_GROUP("spawn location setting")
			ADD_WIDGET_BOOL("bCallSetSpawnLocation", g_SpawnData.bCallSetSpawnLocation)	
			ADD_WIDGET_INT_SLIDER("iSpawnLocationToSet", g_SpawnData.iSpawnLocationToSet, -1, 99, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("GetNearestCarNode - TEST")
		
			ADD_WIDGET_BOOL("bCallCarNodeSearch", g_SpawnData.bCallCarNodeSearch)
			ADD_WIDGET_VECTOR_SLIDER("vCarNodeSearchInput", g_SpawnData.vCarNodeSearchInput, -9999.9, 9999.9, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("fCarNodeSearchInputHeading", g_SpawnData.fCarNodeSearchInputHeading, -9999.9, 9999.9, 0.01)
			
			ADD_WIDGET_VECTOR_SLIDER("vCarNodeSearchOutput", g_SpawnData.vCarNodeSearchOutput, -9999.9, 9999.9, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("fCarNodeSearchOutputHeading", g_SpawnData.fCarNodeSearchOutputHeading, -9999.9, 9999.9, 0.01)
			
			START_WIDGET_GROUP("SearchParams")		
				ADD_WIDGET_VECTOR_SLIDER("vFavourFacing", g_SpawnData.CarNodeSearchParams.vFavourFacing, -9999.9, 9999.9, 0.01)				
				ADD_WIDGET_FLOAT_SLIDER("fMinPlayerDist", g_SpawnData.CarNodeSearchParams.fMinPlayerDist, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fMinDistFromCoords", g_SpawnData.CarNodeSearchParams.fMinDistFromCoords, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fMaxDistance", g_SpawnData.CarNodeSearchParams.fMaxDistance, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fUpperZLimit", g_SpawnData.CarNodeSearchParams.fUpperZLimit, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fLowerZLimit", g_SpawnData.CarNodeSearchParams.fLowerZLimit, -9999.9, 9999.9, 0.01)				
				ADD_WIDGET_BOOL("bDoSafeForSpawnCheck", g_SpawnData.CarNodeSearchParams.bDoSafeForSpawnCheck)
				ADD_WIDGET_BOOL("bGetSafeOffset", g_SpawnData.CarNodeSearchParams.bGetSafeOffset)
				ADD_WIDGET_BOOL("bConsiderOnlyActiveNodes", g_SpawnData.CarNodeSearchParams.bConsiderOnlyActiveNodes)
				ADD_WIDGET_BOOL("bWaterNodesOnly", g_SpawnData.CarNodeSearchParams.bWaterNodesOnly)
				ADD_WIDGET_BOOL("bCheckVehicleSpawning", g_SpawnData.CarNodeSearchParams.bCheckVehicleSpawning)
				ADD_WIDGET_BOOL("bConsiderHighways", g_SpawnData.CarNodeSearchParams.bConsiderHighways)
				ADD_WIDGET_BOOL("bConsiderTunnels", g_SpawnData.CarNodeSearchParams.bConsiderTunnels)
				ADD_WIDGET_BOOL("bCheckSpawnExclusionZones", g_SpawnData.CarNodeSearchParams.bCheckSpawnExclusionZones)
				ADD_WIDGET_BOOL("bAllowFallbackToInactiveNodes", g_SpawnData.CarNodeSearchParams.bAllowFallbackToInactiveNodes)
				ADD_WIDGET_BOOL("bStartOfMissionPVSpawn", g_SpawnData.CarNodeSearchParams.bStartOfMissionPVSpawn)
				ADD_WIDGET_BOOL("bCheckInsideArea", g_SpawnData.CarNodeSearchParams.bCheckInsideArea)
				ADD_WIDGET_VECTOR_SLIDER("vAreaCoords1", g_SpawnData.CarNodeSearchParams.vAreaCoords1, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_VECTOR_SLIDER("vAreaCoords2", g_SpawnData.CarNodeSearchParams.vAreaCoords2, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("fAreaFloat", g_SpawnData.CarNodeSearchParams.fAreaFloat, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_INT_SLIDER("iAreaShape", g_SpawnData.CarNodeSearchParams.iAreaShape, -9999, 9999, 1)
				ADD_WIDGET_BOOL("bCouldNotFindWaterNode", g_SpawnData.CarNodeSearchParams.bCouldNotFindWaterNode)
				ADD_WIDGET_BOOL("bAllowReducingVisibility", g_SpawnData.CarNodeSearchParams.bAllowReducingVisibility)
				ADD_WIDGET_BOOL("bReturnRandomGoodNode", g_SpawnData.CarNodeSearchParams.bReturnRandomGoodNode)
				ADD_WIDGET_BOOL("bReturnNearestGoodNode", g_SpawnData.CarNodeSearchParams.bReturnNearestGoodNode)
				ADD_WIDGET_BOOL("bCheckOwnVisibility", g_SpawnData.CarNodeSearchParams.bCheckOwnVisibility)
				ADD_WIDGET_BOOL("bUsingOffsetOrigin", g_SpawnData.CarNodeSearchParams.bUsingOffsetOrigin)
				ADD_WIDGET_INT_SLIDER("iFallbackLevel", g_SpawnData.CarNodeSearchParams.iFallbackLevel, -9999, 9999, 1)
				ADD_WIDGET_VECTOR_SLIDER("vStartCoords", g_SpawnData.CarNodeSearchParams.vStartCoords, -9999.9, 9999.9, 0.01)
				REPEAT MAX_NUM_AVOID_RADIUS i
					str = "vAvoidCoords["
					str += i
					str += "]"
					ADD_WIDGET_VECTOR_SLIDER(str, g_SpawnData.CarNodeSearchParams.vAvoidCoords[i], -9999.9, 9999.9, 0.01)
					str = "fAvoidRadius["
					str += i
					str += "]"
					ADD_WIDGET_FLOAT_SLIDER(str, g_SpawnData.CarNodeSearchParams.fAvoidRadius[i], -9999.9, 9999.9, 0.01)
				ENDREPEAT
				ADD_WIDGET_BOOL("bCheckPVNoSpawnZone", g_SpawnData.CarNodeSearchParams.bCheckPVNoSpawnZone)
				ADD_WIDGET_FLOAT_SLIDER("fVisibleDistance", g_SpawnData.CarNodeSearchParams.fVisibleDistance, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_BOOL("bDoneMoveFallback", g_SpawnData.CarNodeSearchParams.bDoneMoveFallback)
				ADD_WIDGET_BOOL("bIsForFlyingVehicle", g_SpawnData.CarNodeSearchParams.bIsForFlyingVehicle)
				ADD_WIDGET_BOOL("bCheckForLowestEnemiesNearPoint", g_SpawnData.CarNodeSearchParams.bCheckForLowestEnemiesNearPoint)
				ADD_WIDGET_INT_SLIDER("iLowestNumEnemiesNearPoint", g_SpawnData.CarNodeSearchParams.iLowestNumEnemiesNearPoint, -9999, 9999, 1)
				ADD_WIDGET_FLOAT_SLIDER("fCheckForLowestnemiesMaxDist", g_SpawnData.CarNodeSearchParams.fCheckForLowestnemiesMaxDist, -9999.9, 9999.9, 0.01)
				ADD_WIDGET_BOOL("bDoVisibilityChecks", g_SpawnData.CarNodeSearchParams.bDoVisibilityChecks)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("g_NearestCarNodeResults")
				ADD_WIDGET_INT_SLIDER("iNumResults", g_NearestCarNodeResults.iNumResults, -9999, 9999, 1)
				ADD_WIDGET_INT_SLIDER("iNumResultsInsideArea", g_NearestCarNodeResults.iNumResultsInsideArea, -9999, 9999, 1)
				ADD_WIDGET_INT_SLIDER("iLastVehicleNode", g_NearestCarNodeResults.iLastVehicleNode, LOWEST_INT, HIGHEST_INT, 1)
				ADD_WIDGET_VECTOR_SLIDER("vLastVehicleNodePos", g_NearestCarNodeResults.vLastVehicleNodePos, LOWEST_INT, HIGHEST_INT, 0.01)
				REPEAT MAX_VEHICLE_NODES_CHECKED i
					str = "vCoords["
					str += i
					str += "]"
					ADD_WIDGET_VECTOR_SLIDER(str, g_NearestCarNodeResults.vCoords[i], -9999.9, 9999.9, 0.01)	
					
					str = "fHeading["
					str += i
					str += "]"
					ADD_WIDGET_FLOAT_SLIDER(str, g_NearestCarNodeResults.fHeading[i], -9999.9, 9999.9, 0.01)
				ENDREPEAT	
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		
			CREATE_GROUP_WARP_WIDGET()
		
	STOP_WIDGET_GROUP()
	
ENDPROC

#ENDIF

