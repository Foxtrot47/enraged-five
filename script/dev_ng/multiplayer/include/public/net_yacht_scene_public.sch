USING "SceneTool_public.sch"

//USING "cutscene_public.sch"
//USING "rc_helper_functions.sch"

ENUM enumNET_YACHT_SCENEPans
	NET_YACHT_SCENE_PAN_establishing,
	NET_YACHT_SCENE_PAN_MAX
ENDENUM

//ENUM enumNET_YACHT_SCENECuts
//	NET_YACHT_SCENE_CUT_null = 0,
//	NET_YACHT_SCENE_CUT_MAX
//ENDENUM

//ENUM enumNET_YACHT_SCENEMarkers
//	NET_YACHT_SCENE_MARKER_playerWalk0 = 0,
//	NET_YACHT_SCENE_MARKER_playerWalk1,
//	NET_YACHT_SCENE_MARKER_playerWalk2,
//	NET_YACHT_SCENE_MARKER_MAX
//ENDENUM

//ENUM enumNET_YACHT_SCENEPlacers
//	NET_YACHT_SCENE_PLACER_playerPos = 0,
//	NET_YACHT_SCENE_PLACER_MAX
//ENDENUM

STRUCT STRUCT_NET_YACHT_SCENE
	structSceneTool_Pan		mPans[NET_YACHT_SCENE_PAN_MAX]
//	structSceneTool_Cut		mCuts[NET_YACHT_SCENE_CUT_MAX]
//	
//	structSceneTool_Marker	mMarkers[NET_YACHT_SCENE_MARKER_MAX]
//	structSceneTool_Placer	mPlacers[NET_YACHT_SCENE_PLACER_MAX]
	
//	BOOL					bEnablePans[NET_YACHT_SCENE_PAN_MAX]
	FLOAT					fExitDelay
	
	INT						iTimer
	
	#IF IS_DEBUG_BUILD
	BOOL					bPlaceholder
	#ENDIF
ENDSTRUCT

TYPEDEF FUNC BOOL SceneYachtFunc(INT &iCutStage, STRUCT_NET_YACHT_SCENE &scene, CAMERA_INDEX& hCam0, CAMERA_INDEX& hCam1, BOOL bCleanupAtEnd)
#IF IS_DEBUG_BUILD
TYPEDEF FUNC VECTOR SceneYachtFunc1(INT iYachtID, VECTOR vOffset)
TYPEDEF FUNC FLOAT SceneYachtFunc2(INT iYachtID, FLOAT fOffsetHeading)
#ENDIF

FUNC BOOL Private_Get_NET_YACHT_SCENE(INT iYachtID, STRUCT_NET_YACHT_SCENE& scene
		#IF IS_DEBUG_BUILD
		, SceneYachtFunc1 customYachtCoordOffset
		, SceneYachtFunc2 customYachtHeadOffset
		#ENDIF
		)
	SWITCH iYachtID
		CASE -1	FALLTHRU
		
		DEFAULT
			#IF NOT IS_DEBUG_BUILD
			UNUSED_PARAMETER(scene)
			#ENDIF
			#IF IS_DEBUG_BUILD
			scene.mPans[NET_YACHT_SCENE_PAN_establishing].mStart.vPos = CALL customYachtCoordOffset(iYachtID, <<-41.2763, 127.2256, 10.9052>>)
			scene.mPans[NET_YACHT_SCENE_PAN_establishing].mStart.vRot = <<-1.9523, -0.1204, CALL customYachtHeadOffset(iYachtID, 207.0560)>>
			scene.mPans[NET_YACHT_SCENE_PAN_establishing].mEnd.vPos = CALL customYachtCoordOffset(iYachtID, <<-42.3318, 123.0626, 10.8012>>)
			scene.mPans[NET_YACHT_SCENE_PAN_establishing].mEnd.vRot = <<-1.9523, -0.1204, CALL customYachtHeadOffset(iYachtID, 207.8386)>>
			scene.mPans[NET_YACHT_SCENE_PAN_establishing].fFov = 36.8998
			scene.mPans[NET_YACHT_SCENE_PAN_establishing].fShake = 0.3
			scene.mPans[NET_YACHT_SCENE_PAN_establishing].fDuration = 5.0000
			
			scene.fExitDelay = 0.5000
			
			CERRORLN(DEBUG_YACHT, "Private_Get_NET_YACHT_SCENE() invalid ID ", iYachtID)
			
			scene.bPlaceholder = TRUE
			#ENDIF
			RETURN FALSE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD
USING "net_yacht_scene_debug.sch"
#ENDIF
