//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_CAR_MEET.sch																					//
// Description: Functions to implement from net_peds_base.sch and look up tables to return CAR_MEET ped data.			//
// Written by:  Jan Mencfel																								//
// Date:  		03/03/21																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_TUNER
USING "net_peds_base.sch"
USING "website_public.sch"
USING "net_peds_data_common.sch"
USING "net_simple_cutscene_common.sch"




FUNC VECTOR _GET_CAR_MEET_SANDBOX_PED_LOCAL_COORDS_BASE_POSITION(PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	UNUSED_PARAMETER(ePedLocation)
	RETURN <<-2171.6636, 1113.2114, 25.3624>>	// tr_int2_shell interior coords
ENDFUNC

FUNC FLOAT _GET_CAR_MEET_SANDBOX_PED_LOCAL_HEADING_BASE_HEADING(PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	UNUSED_PARAMETER(ePedLocation)
	RETURN 90.0	// tr_int2_shell interior heading
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ PED DATA ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛
PROC _SET_CAR_MEET_SANDBOX_PED_DATA_LAYOUT_0(PEDS_DATA &Data, INT iPed, BOOL bSetPedArea = TRUE)
		SWITCH iPed
		// Local Peds
			CASE 0
				IF bSetPedArea
					Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
				ENDIF

				SWITCH Data.cullingData.ePedArea
					CASE PED_AREA_PERMANENT
					CASE PED_AREA_ONE
					CASE PED_AREA_TWO
						Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C)
						Data.iLevel 										= 0
						Data.iPackedDrawable[0]								= 33816577
						Data.iPackedDrawable[1]								= 131585
						Data.iPackedDrawable[2]								= 0
						Data.iPackedTexture[0] 								= 16842753
						Data.iPackedTexture[1] 								= 131329
						Data.iPackedTexture[2] 								= 0
						Data.vPosition 										= <<-128.9668, -110.0447, 3.3058>>
						Data.vRotation 										= <<0.0000, 0.0000, 263.7999>>
						CLEAR_PEDS_BITSET(Data.iBS)
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					BREAK
				ENDSWITCH
			BREAK
			CASE 1
				IF bSetPedArea
					Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
				ENDIF

				SWITCH Data.cullingData.ePedArea
					CASE PED_AREA_PERMANENT
					CASE PED_AREA_ONE
					CASE PED_AREA_TWO
						Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F)
						Data.iLevel 										= 0
						Data.iPackedDrawable[0]								= 16908292
						Data.iPackedDrawable[1]								= 131074
						Data.iPackedDrawable[2]								= 0
						Data.iPackedTexture[0] 								= 65538
						Data.iPackedTexture[1] 								= 66050
						Data.iPackedTexture[2] 								= 0
						Data.vPosition 										= <<-129.4226, -113.9912, 3.3058>>
						Data.vRotation 										= <<0.0000, 0.0000, 280.8000>>
						CLEAR_PEDS_BITSET(Data.iBS)
					BREAK
				ENDSWITCH
			BREAK
			CASE 2
				IF bSetPedArea
					Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
				ENDIF

				SWITCH Data.cullingData.ePedArea
					CASE PED_AREA_PERMANENT
					CASE PED_AREA_ONE
					CASE PED_AREA_TWO
						Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C)
						Data.iLevel 										= 0
						Data.iPackedDrawable[0]								= 17170436
						Data.iPackedDrawable[1]								= 259
						Data.iPackedDrawable[2]								= 0
						Data.iPackedTexture[0] 								= 33685504
						Data.iPackedTexture[1] 								= 65544
						Data.iPackedTexture[2] 								= 0
						Data.vPosition 										= <<-128.6745, -100.4075, 3.3058>>
						Data.vRotation 										= <<0.0000, 0.0000, 252.7997>>
						CLEAR_PEDS_BITSET(Data.iBS)
					BREAK
				ENDSWITCH
			BREAK
			CASE 3
				IF bSetPedArea
					Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
				ENDIF

				SWITCH Data.cullingData.ePedArea
					CASE PED_AREA_PERMANENT
					CASE PED_AREA_ONE
					CASE PED_AREA_TWO
						Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A)
						Data.iLevel 										= 0
						Data.iPackedDrawable[0]								= 393219
						Data.iPackedDrawable[1]								= 3
						Data.iPackedDrawable[2]								= 0
						Data.iPackedTexture[0] 								= 33619970
						Data.iPackedTexture[1] 								= 519
						Data.iPackedTexture[2] 								= 0
						Data.vPosition 										= <<-128.9790, -101.3176, 3.3058>>
						Data.vRotation 										= <<0.0000, 0.0000, 284.4000>>
						CLEAR_PEDS_BITSET(Data.iBS)
					BREAK
				ENDSWITCH
			BREAK
			CASE 4
				IF bSetPedArea
					Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
				ENDIF

				SWITCH Data.cullingData.ePedArea
					CASE PED_AREA_PERMANENT
					CASE PED_AREA_ONE
					CASE PED_AREA_TWO
						Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_FEMALE_A)
						Data.iLevel 										= 0
						Data.iPackedDrawable[0]								= 50462720
						Data.iPackedDrawable[1]								= 769
						Data.iPackedDrawable[2]								= 0
						Data.iPackedTexture[0] 								= 16842752
						Data.iPackedTexture[1] 								= 131329
						Data.iPackedTexture[2] 								= 0
						Data.vPosition 										= <<-128.9677, -111.1096, 3.3058>>
						Data.vRotation 										= <<0.0000, 0.0000, 252.7997>>
						CLEAR_PEDS_BITSET(Data.iBS)
					BREAK
				ENDSWITCH
			BREAK
			CASE 5
				IF bSetPedArea
					Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
				ENDIF

				SWITCH Data.cullingData.ePedArea
					CASE PED_AREA_PERMANENT
					CASE PED_AREA_ONE
					CASE PED_AREA_TWO
						Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_MALE_A)
						Data.iLevel 										= 0
						Data.iPackedDrawable[0]								= 67174400
						Data.iPackedDrawable[1]								= 65539
						Data.iPackedDrawable[2]								= 0
						Data.iPackedTexture[0] 								= 16777217
						Data.iPackedTexture[1] 								= 11
						Data.iPackedTexture[2] 								= 0
						Data.vPosition 										= <<-129.2502, -118.7561, 3.3058>>
						Data.vRotation 										= <<0.0000, 0.0000, 291.6000>>
						CLEAR_PEDS_BITSET(Data.iBS)
					BREAK
				ENDSWITCH
			BREAK
			CASE 6
				IF bSetPedArea
					Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
				ENDIF

				SWITCH Data.cullingData.ePedArea
					CASE PED_AREA_PERMANENT
					CASE PED_AREA_ONE
					CASE PED_AREA_TWO
						Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
						Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A)
						Data.iLevel 										= 0
						Data.iPackedDrawable[0]								= 33751045
						Data.iPackedDrawable[1]								= 66048
						Data.iPackedDrawable[2]								= 0
						Data.iPackedTexture[0] 								= 65537
						Data.iPackedTexture[1] 								= 66304
						Data.iPackedTexture[2] 								= 0
						Data.vPosition 										= <<-129.1802, -117.6282, 3.3058>>
						Data.vRotation 										= <<0.0000, 0.0000, 252.7997>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 7
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 2
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 65794
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<125.1035, -193.5156, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 108.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 8																			
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777222
					Data.iPackedDrawable[1]								= 131330
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 518
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<125.1826, -194.5066, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 57.6000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 9
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50528261
					Data.iPackedDrawable[1]								= 259
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777218
					Data.iPackedTexture[1] 								= 773
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<124.8086, -201.6896, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 73.3995>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 10
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_STANDING_SMOKING_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65536
					Data.iPackedDrawable[1]								= 65539
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 65545
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<124.8425, -200.5925, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 108.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 11//FROM CAR_MEET LAYOUT 0 CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16908292
					Data.iPackedDrawable[1]								= 131330
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 65538
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-34.7374, -9.8076, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 131.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 12//FROM CAR_MEET LAYOUT 0 CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 17170436
					Data.iPackedDrawable[1]								= 259
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33685504
					Data.iPackedTexture[1] 								= 65544
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-32.2675, -6.9976, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 298.0800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 13//FROM CAR_MEET LAYOUT 0 CASE 20
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50593794
					Data.iPackedDrawable[1]								= 769
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 779
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-8.5485, 2.6414, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 15.8400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 14//FROM CAR_MEET LAYOUT 0 CASE 21
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 196613
					Data.iPackedDrawable[1]								= 65539
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 66309
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-34.7675, -10.4976, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 54.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
				BREAK
			ENDSWITCH
		BREAK
		CASE 15//FROM CAR_MEET LAYOUT 0 CASE 42
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_STANDING_SMOKING_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84082689
					Data.iPackedDrawable[1]								= 132355
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777216
					Data.iPackedTexture[1] 								= 131329
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.5044, 2.4944, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 334.0800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 16//FROM CAR_MEET LAYOUT 0 CASE 43
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131076
					Data.iPackedDrawable[1]								= 65536
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 66048
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-6.0184, 2.9915, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 295.9200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 17//FROM CAR_MEET LAYOUT 0 CASE 45
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67174405
					Data.iPackedDrawable[1]								= 132097
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 66050
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.6919, -13.3291, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 118.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 18//FROM CAR_MEET LAYOUT 0 CASE 49
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50528257
					Data.iPackedDrawable[1]								= 66305
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 65797
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-23.6794, -23.9675, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 148.3200>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 19//FROM CAR_MEET LAYOUT 0 CASE 50
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50659334
					Data.iPackedDrawable[1]								= 65792
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65536
					Data.iPackedTexture[1] 								= 66305
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-25.3324, -23.9705, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 214.5600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 20//FROM CAR_MEET LAYOUT 0 CASE 51
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33816578
					Data.iPackedDrawable[1]								= 131586
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 775
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-24.4495, -23.7527, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 179.2800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 21//FROM CAR_MEET LAYOUT 0 CASE 52
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_H)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 2
					Data.iPackedDrawable[1]								= 65539
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 257
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-36.8254, -19.5405, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 64.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 22//FROM CAR_MEET LAYOUT 0 CASE 55
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84213766
					Data.iPackedDrawable[1]								= 131843
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 66308
					Data.iPackedTexture[2] 								= 1
					Data.vPosition 										= <<-36.6544, -18.4265, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 110.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 23//FROM CAR_MEET LAYOUT 0 CASE 56
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279299
					Data.iPackedDrawable[1]								= 66307
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 33554433
					Data.iPackedTexture[1] 								= 131590
					Data.iPackedTexture[2] 								= 2
					Data.vPosition 										= <<-20.2734, -17.4526, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 12.9600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 24//FROM CAR_MEET LAYOUT 0 CASE 57
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50397189
					Data.iPackedDrawable[1]								= 769
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 518
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-21.2975, -17.1855, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 316.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 25//FROM CAR_MEET LAYOUT 0 CASE 58
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 2
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842752
					Data.iPackedTexture[1] 								= 256
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.3024, -14.3047, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 313.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 26//FROM CAR_MEET LAYOUT 0 CASE 59
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F_TRANS_G)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16908292
					Data.iPackedDrawable[1]								= 65537
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 514
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.3634, -12.9375, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 241.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 27//FROM CAR_MEET LAYOUT 0 CASE 63
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65540
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842754
					Data.iPackedTexture[1] 								= 131584
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<32.2075, -24.4817, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 238.3200>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 28//FROM CAR_MEET LAYOUT 0 CASE 70
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_G)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 83886082
					Data.iPackedDrawable[1]								= 66306
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 16842754
					Data.iPackedTexture[1] 								= 256
					Data.iPackedTexture[2] 								= 1
					Data.vPosition 										= <<21.1926, -16.3945, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 334.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 29//FROM CAR_MEET LAYOUT 0 CASE 71
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279300
					Data.iPackedDrawable[1]								= 1282
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 131080
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<17.4426, -17.1545, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 309.6000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 30//FROM CAR_MEET LAYOUT 0 CASE 72
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_E)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16908292
					Data.iPackedDrawable[1]								= 65536
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 66049
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<29.4205, -19.1626, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 37.4400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 31//FROM CAR_MEET LAYOUT 0 CASE 73
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_C_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33816578
					Data.iPackedDrawable[1]								= 66048
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 775
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<25.0636, -19.2095, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 306.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 32//FROM CAR_MEET LAYOUT 0 CASE 75
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 262146
					Data.iPackedDrawable[1]								= 131074
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65536
					Data.iPackedTexture[1] 								= 131843
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<34.2325, -24.3777, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 127.4400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 33//FROM CAR_MEET LAYOUT 0 CASE 76
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 393219
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 66054
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<33.2275, -24.2856, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 187.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 34//FROM CAR_MEET LAYOUT 0 CASE 77
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_ON_PHONE_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67371009
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 65793
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<18.0455, -16.4705, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 183.6000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 35//FROM CAR_MEET LAYOUT 0 CASE 79
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_STANDING_SMOKING_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67436547
					Data.iPackedDrawable[1]								= 66563
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554434
					Data.iPackedTexture[1] 								= 65536
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<22.0486, -16.4456, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 32.4000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 36//FROM CAR_MEET LAYOUT 0 CASE 81
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_C_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84083717
					Data.iPackedDrawable[1]								= 66817
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33620224
					Data.iPackedTexture[1] 								= 65536
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<3.9481, -28.2766, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 65.6250>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 37//FROM CAR_MEET LAYOUT 0 CASE 82
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A_TRANS_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 6
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16908288
					Data.iPackedTexture[1] 								= 131590
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<3.4061, -29.8950, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 29.3600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 38//FROM CAR_MEET LAYOUT 0 CASE 86
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_PAPARAZZI)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 327686
					Data.iPackedDrawable[1]								= 131075
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619969
					Data.iPackedTexture[1] 								= 131844
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<18.6166, -5.4966, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 224.6400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHANGE_CAPSULE_SIZE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 39//FROM CAR_MEET LAYOUT 0 CASE 89
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_MALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67371009
					Data.iPackedDrawable[1]								= 131843
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777218
					Data.iPackedTexture[1] 								= 65794
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<12.1276, 3.3015, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 324.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 40//FROM CAR_MEET LAYOUT 0 CASE 90
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67108870
					Data.iPackedDrawable[1]								= 66562
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 131586
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<13.2075, 3.2715, 3.3058>>
					Data.vRotation 										= <<0.0000, 0.0000, 22.3200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		// Networked Peds
	ENDSWITCH
	
	
		// Transform the local coords into world coords.
	IF NOT IS_VECTOR_ZERO(Data.vPosition)		
		Data.vPosition = TRANSFORM_LOCAL_COORDS_TO_WORLD_COORDS(_GET_CAR_MEET_SANDBOX_PED_LOCAL_COORDS_BASE_POSITION(), _GET_CAR_MEET_SANDBOX_PED_LOCAL_HEADING_BASE_HEADING(), Data.vPosition)
	ENDIF
	
	// Transform the local heading into world heading.
	IF (Data.vRotation.z != -1.0)
		Data.vRotation.z = TRANSFORM_LOCAL_HEADING_TO_WORLD_HEADING(_GET_CAR_MEET_SANDBOX_PED_LOCAL_HEADING_BASE_HEADING(), Data.vRotation.z)
	ENDIF

ENDPROC

PROC _SET_CAR_MEET_SANDBOX_PED_PROP_INDEXES(PED_INDEX &PedID, INT iPed, INT iLayout, PED_MODELS ePedModel)
	UNUSED_PARAMETER(iLayout)
	UNUSED_PARAMETER(ePedModel)
	UNUSED_PARAMETER(iPed)
		
	SWITCH ePedModel
		CASE PED_MODEL_MOODYMANN
			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
		BREAK
		CASE PED_MODEL_BENNY
			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
		BREAK
		CASE PED_MODEL_HAO
			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,0), 0, 0)
		BREAK
		CASE PED_MODEL_BENNYS_MECHANIC
			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,0), 0, 0)
			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
		BREAK
		CASE PED_MODEL_JOHNNY_JONES
			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,0), 0, 0)
		BREAK
	ENDSWITCH 
ENDPROC
//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ FUNCTIONS TO IMPLEMENT ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _SHOULD_CAR_MEET_SANDBOX_PED_SCRIPT_LAUNCH()
	//RETURN TRUE
	RETURN IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
ENDFUNC

FUNC BOOL _IS_CAR_MEET_SANDBOX_PARENT_A_SIMPLE_INTERIOR
	RETURN TRUE
ENDFUNC

PROC _SET_CAR_MEET_SANDBOX_PED_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, INT iPed, INT iLayout, BOOL bSetPedArea = TRUE)
	UNUSED_PARAMETER(ServerBD)
	UNUSED_PARAMETER(ePedLocation)
	PRINTLN("_SET_CAR_MEET_SANDBOX_PED_DATA   LAYOUT:", iLayout, "  PED:",iPed)	
	UNUSED_PARAMETER(iLayout)
	_SET_CAR_MEET_SANDBOX_PED_DATA_LAYOUT_0(Data, iPed, bSetPedArea)
		
	
ENDPROC

FUNC INT _GET_CAR_MEET_SANDBOX_NETWORK_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	UNUSED_PARAMETER(ePedLocation)
	UNUSED_PARAMETER(ServerBD)
	RETURN 0
ENDFUNC

FUNC INT _GET_CAR_MEET_SANDBOX_LOCAL_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	
	INT iPed
	INT iActivePeds = 0
	PEDS_DATA tempData
	
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iPed
		tempData.vPosition = NULL_VECTOR()
		_SET_CAR_MEET_SANDBOX_PED_DATA(ePedLocation, ServerBD, tempData, iPed, ServerBD.iLayout)
		IF NOT IS_VECTOR_ZERO(tempData.vPosition)
			iActivePeds++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iActivePeds
ENDFUNC

FUNC INT _GET_CAR_MEET_SANDBOX_SERVER_PED_LAYOUT_TOTAL()
	RETURN 1
ENDFUNC

FUNC INT _GET_CAR_MEET_SANDBOX_SERVER_PED_LAYOUT()	
	RETURN 0
ENDFUNC

FUNC INT _GET_CAR_MEET_SANDBOX_SERVER_PED_LEVEL()
	RETURN 0
ENDFUNC

PROC _SET_CAR_MEET_SANDBOX_PED_SERVER_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
	UNUSED_PARAMETER(ePedLocation)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ServerBD.iLayout 			= _GET_CAR_MEET_SANDBOX_SERVER_PED_LAYOUT()
		ServerBD.iLevel 			= _GET_CAR_MEET_SANDBOX_SERVER_PED_LEVEL()
		ServerBD.iMaxLocalPeds 		= _GET_CAR_MEET_SANDBOX_LOCAL_PED_TOTAL(ServerBD)
		ServerBD.iMaxNetworkPeds	= _GET_CAR_MEET_SANDBOX_NETWORK_PED_TOTAL(ServerBD)
	ENDIF
	PRINTLN("[AM_MP_PEDS] _SET_CAR_MEET_SANDBOX_PED_SERVER_DATA - Layout: ", ServerBD.iLayout)
	PRINTLN("[AM_MP_PEDS] _SET_CAR_MEET_SANDBOX_PED_SERVER_DATA - Level: ", ServerBD.iLevel)
	PRINTLN("[AM_MP_PEDS] _SET_CAR_MEET_SANDBOX_PED_SERVER_DATA - Max Local Peds: ", ServerBD.iMaxLocalPeds)
	PRINTLN("[AM_MP_PEDS] _SET_CAR_MEET_SANDBOX_PED_SERVER_DATA - Max Network Peds: ", ServerBD.iMaxNetworkPeds)
ENDPROC

FUNC BOOL _IS_PLAYER_IN_CAR_MEET_SANDBOX_PARENT_PROPERTY(PLAYER_INDEX playerID)
	UNUSED_PARAMETER(playerID)
	RETURN TRUE
	//RETURN IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(playerID)
ENDFUNC

FUNC BOOL _HAS_CAR_MEET_SANDBOX_PED_BEEN_CREATED(PEDS_DATA &Data, INT iLevel)
	
	IF (IS_ENTITY_ALIVE(Data.PedID)
		AND (GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK 
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_START_SCENARIO_IN_PLACE) = PERFORMING_TASK))
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CHILD_PED)
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SKIP_PED)
	OR (Data.iLevel > iLevel)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED MODELS ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_CAR_MEET_SANDBOX_LOCAL_PED_PROPERTIES(PED_INDEX &PedID, INT iPed)
	UNUSED_PARAMETER(iPed)
	SET_ENTITY_CAN_BE_DAMAGED(PedID, FALSE)
	SET_PED_AS_ENEMY(PedID, FALSE)
	SET_CURRENT_PED_WEAPON(PedID, WEAPONTYPE_UNARMED, TRUE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedID, TRUE)
	SET_PED_RESET_FLAG(PedID, PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromExplosions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
	
	SET_PED_CAN_EVASIVE_DIVE(PedID, FALSE)
	SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(PedID, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PedID, FALSE)
	SET_PED_CAN_RAGDOLL(PedID, FALSE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DisableExplosionReactions, TRUE)
	CLEAR_PED_TASKS(PedID)
ENDPROC

PROC _SET_CAR_MEET_SANDBOX_NETWORK_PED_PROPERTIES(NETWORK_INDEX &NetworkPedID, INT &iPedDataBS[PEDS_DATA_BITSET_ARRAY_SIZE])
	//NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_PED(NetworkPedID), TRUE)
	
	IF NOT IS_PEDS_BIT_SET(iPedDataBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, TRUE)
	ENDIF
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED SPEECH ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA_LAYOUT_0(SPEECH_DATA &SpeechData, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	
	PRINTLN("_SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA_LAYOUT_0")
	IF (bNetworkPed)
		SWITCH iArrayID
			CASE 0
			BREAK
		ENDSWITCH
	ELSE
		SWITCH iArrayID
			CASE 0
				SpeechData.iPedID								= 0
				SpeechData.fGreetSpeechDistance					= 3.5000
				SpeechData.fByeSpeechDistance					= 4.7000
				SpeechData.fListenDistance						= 7.0000
				iSpeechPedID[SpeechData.iPedID]					= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC _SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA_LAYOUT_1(SPEECH_DATA &SpeechData, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	PRINTLN("_SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA_LAYOUT_1")
	IF (NOT bNetworkPed)
		SWITCH iArrayID
			CASE 0
				SpeechData.iPedID							= 0	
				SpeechData.fGreetSpeechDistance				= 3.5
				SpeechData.fByeSpeechDistance				= 5.0
				SpeechData.fListenDistance					= 7.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC _SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA_LAYOUT_2(SPEECH_DATA &SpeechData, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	PRINTLN("_SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA_LAYOUT_2")
	IF (NOT bNetworkPed)
		SWITCH iArrayID
			CASE 0
				SpeechData.iPedID							= 0	
				SpeechData.fGreetSpeechDistance				= 3.5
				SpeechData.fByeSpeechDistance				= 5.5
				SpeechData.fListenDistance					= 7.500
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC _SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA_LAYOUT_3(SPEECH_DATA &SpeechData, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	PRINTLN("_SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA_LAYOUT_3")
	IF (NOT bNetworkPed)
		SWITCH iArrayID
			CASE 0
				SpeechData.iPedID							= 0	
				SpeechData.fGreetSpeechDistance				= 3.0
				SpeechData.fByeSpeechDistance				= 5.0
				SpeechData.fListenDistance					= 8.5
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC _SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA_LAYOUT_4(SPEECH_DATA &SpeechData, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	PRINTLN("_SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA_LAYOUT_4")
	IF (NOT bNetworkPed)
		SWITCH iArrayID
			CASE 0
				SpeechData.iPedID							= 0	
				SpeechData.fGreetSpeechDistance				= 2.0
				SpeechData.fByeSpeechDistance				= 3.5
				SpeechData.fListenDistance					= 5.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC _SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA(SPEECH_DATA &SpeechData, INT iLayout, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	SWITCH iLayout
		CASE 0
			_SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA_LAYOUT_0(SpeechData, iArrayID, iSpeechPedID, bNetworkPed)
		BREAK
		CASE 1
			_SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA_LAYOUT_1(SpeechData, iArrayID, iSpeechPedID, bNetworkPed)
		BREAK
		CASE 2
			_SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA_LAYOUT_2(SpeechData, iArrayID, iSpeechPedID, bNetworkPed)
		BREAK
		CASE 3
			_SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA_LAYOUT_3(SpeechData, iArrayID, iSpeechPedID, bNetworkPed)
		BREAK
		CASE 4
			_SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA_LAYOUT_4(SpeechData, iArrayID, iSpeechPedID, bNetworkPed)
		BREAK
			
		DEFAULT
			_SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA_LAYOUT_0(SpeechData, iArrayID, iSpeechPedID, bNetworkPed)
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL _CAN_CAR_MEET_SANDBOX_PED_PLAY_SPEECH(PED_INDEX PedID, INT iPed, INT iLayout, PED_SPEECH ePedSpeech)
	UNUSED_PARAMETER(ePedSpeech)
	UNUSED_PARAMETER(PedID)
	
	// Generic conditions
	IF NOT IS_ENTITY_ALIVE(PedID)
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_SANDBOX_PED_PLAY_SPEECH - Bail Reason: Ped is not alive")
		RETURN FALSE
	ENDIF
	
	IF NOT g_bInitPedsCreated
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_SANDBOX_PED_PLAY_SPEECH - Bail Reason: Waiting for all peds to be created first")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_SANDBOX_PED_PLAY_SPEECH - Bail Reason: Player is walking in or out of interior")
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT()
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_SANDBOX_PED_PLAY_SPEECH - Bail Reason: Screen is fading out")
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_SANDBOX_PED_PLAY_SPEECH - Bail Reason: Browser is open")
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE() OR IS_PLAYER_IN_SIMPLE_CUTSCENE(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_SANDBOX_PED_PLAY_SPEECH - Bail Reason: Cutscene is active")
		RETURN FALSE
	ENDIF
	
	IF (IS_PLAYER_IN_CORONA() OR IS_TRANSITION_SESSION_LAUNCHING() OR IS_TRANSITION_SESSION_RESTARTING())
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_SANDBOX_PED_PLAY_SPEECH - Bail Reason: Player in corona")
		RETURN FALSE
	ENDIF
	
	IF g_bDontCrossRunning
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_SANDBOX_PED_PLAY_SPEECH - Bail Reason: Player is playing Dont Cross The Line")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_INTERACTING_WITH_PLANNING_BOARD()
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_SANDBOX_PED_PLAY_SPEECH - Bail Reason: Player is interacting with the planning board")
		RETURN FALSE
	ENDIF
	
	// Z check
	VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
	VECTOR vPedCoords = GET_ENTITY_COORDS(PedID)
	IF NOT ARE_VECTORS_ALMOST_EQUAL(<<0.0, 0.0, vPlayerCoords.z>>, <<0.0, 0.0, vPedCoords.z>>)
		PRINTLN("[AM_MP_PEDS] _CAN_CASINO_NIGHTCLUB_PED_PLAY_SPEECH - Bail Reason: Z check failed")
		RETURN FALSE
	ENDIF
	
	// Specific conditions
	SWITCH iPed
		CASE 0			
			SWITCH iLayout
				CASE 0
				DEFAULT			
					RETURN TRUE
				BREAK
			
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC PED_SPEECH _GET_CAR_MEET_SANDBOX_PED_SPEECH_TYPE(INT iPed, PED_ACTIVITIES ePedActivity, INT iSpeech)
	PED_SPEECH eSpeech = PED_SPH_INVALID
	
	SWITCH iPed
		CASE 0
			IF (ePedActivity = PED_ACT_PAVEL_SCREENS)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
					CASE 2	eSpeech = PED_SPH_PT_LOITER											BREAK
					CASE 3	eSpeech = PED_SPH_PT_WHERE_TO										BREAK
					CASE 4	eSpeech = PED_SPH_CT_WHATS_UP										BREAK
					CASE 5 	eSpeech = PED_SPH_CT_PAVEL_IG1_SCREENS_PIECE_OF_SHIT				BREAK
					CASE 6 	eSpeech = PED_SPH_CT_PAVEL_IG1_SCREENS_FUCKING_SCREENS				BREAK
					CASE 7 	eSpeech = PED_SPH_CT_PAVEL_IG1_SCREENS_SIX_DEGREES					BREAK
					CASE 8 	eSpeech = PED_SPH_CT_PAVEL_IG1_SCREENS_BIDE_YOUR_TIME				BREAK
					CASE 9 	eSpeech = PED_SPH_CT_PAVEL_IG1_SCREENS_WONDER_WHAT_THAT_MEANS		BREAK
					CASE 10 eSpeech = PED_SPH_CT_PAVEL_IG1_SCREENS_BEEP_TO_YOU_TOO				BREAK
					CASE 11	eSpeech = PED_SPH_CT_PAVEL_IG1_SCREENS_SURE_ITS_NOTHING				BREAK
					CASE 12	eSpeech = PED_SPH_CT_PAVEL_IG1_SCREENS_WHY_LETTER_SO_SMALL			BREAK
					CASE 13	eSpeech = PED_SPH_CT_PAVEL_IG1_SCREENS_PREFER_WHEN_IT_WAS_TV		BREAK
					CASE 14	eSpeech = PED_SPH_CT_PAVEL_IG1_SCREENS_FUCKING_BRIDGE				BREAK
				ENDSWITCH
			ELIF (ePedActivity = PED_ACT_PAVEL_BUTTONS)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
					CASE 2	eSpeech = PED_SPH_PT_LOITER											BREAK
					CASE 3	eSpeech = PED_SPH_PT_WHERE_TO										BREAK
					CASE 4	eSpeech = PED_SPH_CT_WHATS_UP										BREAK
					CASE 5 	eSpeech = PED_SPH_CT_PAVEL_IG2_BUTTONS_OH_YES						BREAK
					CASE 6 	eSpeech = PED_SPH_CT_PAVEL_IG2_BUTTONS_CRUSH_DEPTH					BREAK
					CASE 7 	eSpeech = PED_SPH_CT_PAVEL_IG2_BUTTONS_A_LITTLE_OF_THIS_ONE			BREAK
					CASE 8 	eSpeech = PED_SPH_CT_PAVEL_IG2_BUTTONS_BEAUTIFUL					BREAK
					CASE 9 	eSpeech = PED_SPH_CT_PAVEL_IG2_BUTTONS_SPEAK_TO_ME					BREAK
					CASE 10 eSpeech = PED_SPH_CT_PAVEL_IG2_BUTTONS_I_WILL_FIX_IT				BREAK
					CASE 11	eSpeech = PED_SPH_CT_PAVEL_IG2_BUTTONS_BEARING_TWO_ONE_ZONE			BREAK
					CASE 12	eSpeech = PED_SPH_CT_PAVEL_IG2_BUTTONS_FLUSH_THE_TANK				BREAK
					CASE 13	eSpeech = PED_SPH_CT_PAVEL_IG2_BUTTONS_AND_SILENT					BREAK
					CASE 14	eSpeech = PED_SPH_CT_PAVEL_IG2_BUTTONS_BROUGHT_MY_WRENCH			BREAK
				ENDSWITCH
			ELIF (ePedActivity = PED_ACT_PAVEL_ENGINE)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
					CASE 2	eSpeech = PED_SPH_PT_LOITER											BREAK
					CASE 3	eSpeech = PED_SPH_PT_WHERE_TO										BREAK
					CASE 4	eSpeech = PED_SPH_CT_WHATS_UP										BREAK
					CASE 5	eSpeech = PED_SPH_CT_PAVEL_IG6_ENGINE_PERFECT						BREAK
					CASE 6 	eSpeech = PED_SPH_CT_PAVEL_IG6_ENGINE_GENTLY						BREAK
					CASE 7 	eSpeech = PED_SPH_CT_PAVEL_IG6_ENGINE_DONT_DO_THIS					BREAK
					CASE 8 	eSpeech = PED_SPH_CT_PAVEL_IG6_ENGINE_I_AM_PATIENT					BREAK
					CASE 9 	eSpeech = PED_SPH_CT_PAVEL_IG6_ENGINE_PLAY_NICE						BREAK
					CASE 10	eSpeech = PED_SPH_CT_PAVEL_IG6_ENGINE_UGH_I_AM_SORRY				BREAK
					CASE 11 eSpeech = PED_SPH_CT_PAVEL_IG6_ENGINE_FINE							BREAK
					CASE 12	eSpeech = PED_SPH_CT_PAVEL_IG6_ENGINE_I_WARNED_YOU					BREAK
					CASE 13	eSpeech = PED_SPH_CT_PAVEL_IG6_ENGINE_DONT_TALK_BACK				BREAK
					CASE 14	eSpeech = PED_SPH_CT_PAVEL_IG6_ENGINE_IF_I_HAVE_TO_COMEDOWN			BREAK
				ENDSWITCH
			ELSE
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
					CASE 2	eSpeech = PED_SPH_PT_LOITER											BREAK
					CASE 3	eSpeech = PED_SPH_PT_WHERE_TO										BREAK
					CASE 4	eSpeech = PED_SPH_CT_WHATS_UP										BREAK
				ENDSWITCH
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN eSpeech
ENDFUNC


PROC _GET_CAR_MEET_SANDBOX_PED_CONVO_DATA(PED_CONVO_DATA &convoData, INT iPed, PED_ACTIVITIES ePedActivity, PED_SPEECH ePedSpeech)
	UNUSED_PARAMETER(ePedActivity)
	RESET_PED_CONVO_DATA(convoData)
	
	SWITCH iPed
		CASE 0
			convoData.iSpeakerID						= 2
			convoData.sCharacterVoice					= "HS4_PAVEL"
			convoData.sSubtitleTextBlock				= "HS4PIAU"
			IF (ePedActivity = PED_ACT_PAVEL_SCREENS)
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						convoData.sRootName = "GENERIC_HI"
						IF ARE_STRINGS_EQUAL(convoData.sRootName, "HS4PA_PS_54")
							convoData.sSubtitleTextBlock 	= "HS4PAAU"
							convoData.bPlayAsConvo			= TRUE
						ENDIF
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName = "GENERIC_BYE"
					BREAK
					CASE PED_SPH_PT_LOITER
						convoData.sRootName = "LOITER"
					BREAK
					CASE PED_SPH_PT_WHERE_TO
						convoData.sRootName = "TRAVEL_WHERE_TO"
					BREAK
					CASE PED_SPH_CT_WHATS_UP
						convoData.sRootName = "GENERIC_HOWSITGOING"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG1_SCREENS_PIECE_OF_SHIT
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG1_A"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG1_SCREENS_FUCKING_SCREENS
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG1_B"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG1_SCREENS_SIX_DEGREES
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG1_C"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG1_SCREENS_BIDE_YOUR_TIME
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG1_D"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG1_SCREENS_WONDER_WHAT_THAT_MEANS
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG1_E"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG1_SCREENS_BEEP_TO_YOU_TOO
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG1_F"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG1_SCREENS_SURE_ITS_NOTHING
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG1_G"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG1_SCREENS_WHY_LETTER_SO_SMALL
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG1_H"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG1_SCREENS_PREFER_WHEN_IT_WAS_TV
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG1_I"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG1_SCREENS_FUCKING_BRIDGE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG1_J"
					BREAK
				ENDSWITCH
			ELIF (ePedActivity = PED_ACT_PAVEL_BUTTONS)
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						convoData.sRootName = "GENERIC_HI"
						IF ARE_STRINGS_EQUAL(convoData.sRootName, "HS4PA_PS_54")
							convoData.sSubtitleTextBlock 	= "HS4PAAU"
							convoData.bPlayAsConvo			= TRUE
						ENDIF
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName = "GENERIC_BYE"
					BREAK
					CASE PED_SPH_PT_LOITER
						convoData.sRootName = "LOITER"
					BREAK
					CASE PED_SPH_PT_WHERE_TO
						convoData.sRootName = "TRAVEL_WHERE_TO"
					BREAK
					CASE PED_SPH_CT_WHATS_UP
						convoData.sRootName = "GENERIC_HOWSITGOING"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_OH_YES
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG2_A"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_CRUSH_DEPTH
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG2_B"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_A_LITTLE_OF_THIS_ONE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG2_C"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_BEAUTIFUL
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG2_D"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_SPEAK_TO_ME
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG2_E"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_I_WILL_FIX_IT
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG2_F"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_BEARING_TWO_ONE_ZONE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG2_G"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_FLUSH_THE_TANK
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG2_H"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_AND_SILENT
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG2_I"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG2_BUTTONS_BROUGHT_MY_WRENCH
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG2_J"
					BREAK
				ENDSWITCH
			ELIF (ePedActivity = PED_ACT_PAVEL_ENGINE)
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						convoData.sRootName = "GENERIC_HI"
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName = "GENERIC_BYE"
					BREAK
					CASE PED_SPH_PT_LOITER
						convoData.sRootName = "LOITER"
					BREAK
					CASE PED_SPH_PT_WHERE_TO
						convoData.sRootName = "TRAVEL_WHERE_TO"
					BREAK
					CASE PED_SPH_CT_WHATS_UP
						convoData.sRootName = "GENERIC_HOWSITGOING"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG6_ENGINE_PERFECT
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG6_A"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG6_ENGINE_GENTLY
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG6_B"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG6_ENGINE_DONT_DO_THIS
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG6_C"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG6_ENGINE_I_AM_PATIENT
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG6_D"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG6_ENGINE_PLAY_NICE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG6_E"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG6_ENGINE_UGH_I_AM_SORRY
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG6_F"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG6_ENGINE_FINE
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG6_G"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG6_ENGINE_I_WARNED_YOU
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG6_H"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG6_ENGINE_DONT_TALK_BACK
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG6_I"
					BREAK
					CASE PED_SPH_CT_PAVEL_IG6_ENGINE_IF_I_HAVE_TO_COMEDOWN
						convoData.bConvoRequiresAnimation	= TRUE
						convoData.bPlayAsConvo				= TRUE
						convoData.sRootName					= "HS4PI_IG6_J"
					BREAK
				ENDSWITCH
			ELSE
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						convoData.sRootName = "GENERIC_HI"
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName = "GENERIC_BYE"
					BREAK
					CASE PED_SPH_PT_LOITER
						convoData.sRootName = "LOITER"
					BREAK
					CASE PED_SPH_PT_WHERE_TO
						convoData.sRootName = "TRAVEL_WHERE_TO"
					BREAK
					CASE PED_SPH_CT_WHATS_UP
						convoData.sRootName = "GENERIC_HOWSITGOING"
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC FLOAT _GET_CAR_MEET_SANDBOX_PED_SPEECH_TRIGGER_DISTANCE(INT iPed, PED_SPEECH ePedSpeech)
	FLOAT fDistance = 0.0
	
	SWITCH iPed
		CASE 0
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING		fDistance = 2.0		BREAK
				CASE PED_SPH_PT_BYE				fDistance = 4.0		BREAK
				CASE PED_SPH_LISTEN_DISTANCE	fDistance = 6.0		BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	RETURN fDistance
ENDFUNC

FUNC PED_SPEECH _GET_CAR_MEET_SANDBOX_PED_CONTROLLER_SPEECH(PED_SPEECH &eCurrentSpeech, INT iPed, PED_ACTIVITIES ePedActivity)
	
	INT iSpeech
	INT iAttempts = 0
	INT iMaxSpeech
	INT iRandSpeech
	PED_SPEECH eSpeech
	
	INT iMaxControllerSpeechTypes = 0
	INT iControllerSpeechTypes[PED_SPH_TOTAL]
	
	SWITCH iPed
		CASE 0
			
			// Description: Default simply selects a new speech to play that isn't the same as the previous speech.
			PED_CONVO_DATA convoData
			
			// Populate the iControllerSpeechTypes array with all the controller speech type IDs from _GET_GENREIC_PED_SPEECH_TYPE
			REPEAT PED_SPH_TOTAL iSpeech
				eSpeech = _GET_CAR_MEET_SANDBOX_PED_SPEECH_TYPE(iPed, ePedActivity, iSpeech)
				IF (eSpeech > PED_SPH_PT_TOTAL AND eSpeech < PED_SPH_CT_TOTAL)
					RESET_PED_CONVO_DATA(convoData)
					_GET_CAR_MEET_SANDBOX_PED_CONVO_DATA(convoData, iPed, ePedActivity, eSpeech)
					IF IS_CONVO_DATA_VALID(convoData)
						iControllerSpeechTypes[iMaxControllerSpeechTypes] = iSpeech
						iMaxControllerSpeechTypes++
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF (iMaxControllerSpeechTypes > 1)
				iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
				eSpeech = _GET_CAR_MEET_SANDBOX_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
				
				// Ensure speech type is different from previous
				WHILE (eSpeech = eCurrentSpeech AND iAttempts < 10)
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
					eSpeech = _GET_CAR_MEET_SANDBOX_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
					iAttempts++
				ENDWHILE
				
				// Randomising failed to find new speech type. Manually set it.
				IF (iAttempts >= 10)
					REPEAT iMaxSpeech iSpeech
						eSpeech = _GET_CAR_MEET_SANDBOX_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iSpeech])
						IF (eSpeech != eCurrentSpeech)
							BREAKLOOP
						ENDIF
					ENDREPEAT
				ENDIF
				
			ELSE
				eSpeech = _GET_CAR_MEET_SANDBOX_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[0])
			ENDIF
			
		BREAK
	ENDSWITCH
	
	eCurrentSpeech = eSpeech
	RETURN eSpeech
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PED ANIM DATA ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// Note: Activities with multiple animations have their anim data populated in their own functions below.
//		 If an activity only has one animation associated with it, it is populated within _GET_CAR_MEET_SANDBOX_PED_ANIM_DATA itself.
//
// Note: Each activity either has a _M_ or _F_ in its title to specify which ped gender should be using that activity.
//		 If an activity does not has either an _M_ or _F_ in its title; it is unisex, and can be used by either ped gender.
//
// Note: Some animations have been excluded from activities. 
//		 The excluded anims have Z axis starting positions that dont line up with the other anims in the same dictionary.
//		 This causes a snap that a blend cannot fix. Use the widget 'RAG/Script/AM_MP_PEDS/Animation/Output Initial Activity Anim Data' to see which Z axis anims are broken.

//╒═══════════════════════════════╕
//╞══════╡ LEANING SMOKING ╞══════╡
//╘═══════════════════════════════╛

PROC GET_CAR_MEET_SANDBOX_LEANING_SMOKING_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_C"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_D"
		BREAK
		CASE 5
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_E"
		BREAK
	ENDSWITCH
ENDPROC

//╒═══════════════════════════════╕
//╞═════╡ STANDING SMOKING ╞══════╡
//╘═══════════════════════════════╛

PROC GET_CAR_MEET_SANDBOX_STANDING_SMOKING_M_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_C"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_MALE_A_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@male_a@base"
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@male_a@idles"
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@male_a@idles"
			pedAnimData.sAnimClip 		= "idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@male_a@idles"
			pedAnimData.sAnimClip 		= "idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@male_a@idles"
			pedAnimData.sAnimClip 		= "idle_d"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CAR_MEET_SANDBOX_STANDING_SMOKING_F_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@FEMALE@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@FEMALE@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@FEMALE@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@FEMALE@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_C"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@"
	
	SWITCH iClip
		CASE 0	
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+= letter 		
			pedAnimData.sAnimClip 		+= "_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+= letter 	
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+= letter 	
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+= letter 	
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+= letter 	
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@"
	
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_SMOKE_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_engine@"
	
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 		
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 		
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 		
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 		
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 		
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_SMOKE_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_engine@"
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_engine@"
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@listen_music@"
	SWITCH iClip
		CASE 0	
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@listen_music@"
	SWITCH iClip
		CASE 0		
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@take_photos@"
	SWITCH iClip
		CASE 0					
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@take_photos@"
	SWITCH iClip
		CASE 0		
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_DRINK_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_engine@"
	SWITCH iClip
		CASE 0		
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_DRINK_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_engine@"
	SWITCH iClip
		CASE 0	
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_DRINK_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@"
	SWITCH iClip
		CASE 0		
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_DRINK_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@"
	SWITCH iClip
		CASE 0					
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3			
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_SMOKE_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@"
	SWITCH iClip
		CASE 0	
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_SMOKE_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@"
	SWITCH iClip
		CASE 0	
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_engine@"
	
	SWITCH iClip
		CASE 0		
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, PED_TRANSITION_STATES eTransitionState, BOOL bPedTransition, TEXT_LABEL_23 tlPrefix, TEXT_LABEL_3 tlActivityOneLetter, TEXT_LABEL_3 tlActivityTwoLetter, BOOL bMalePed = TRUE)
	pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
	pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
	
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_TRANSITION_TASK_ANIM)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
	
	SWITCH eTransitionState
		CASE PED_TRANS_STATE_ACTIVITY_ONE
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@checkout_car@", tlPrefix, tlActivityOneLetter, tlActivityTwoLetter)
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ELSE
					GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ENDIF
			ENDIF
		BREAK
		CASE PED_TRANS_STATE_ACTIVITY_TWO
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@checkout_car@", tlPrefix, tlActivityTwoLetter, tlActivityOneLetter)
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ELSE
					GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC


PROC GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, PED_TRANSITION_STATES eTransitionState, BOOL bPedTransition, TEXT_LABEL_23 tlPrefix, TEXT_LABEL_3 tlActivityOneLetter, TEXT_LABEL_3 tlActivityTwoLetter, BOOL bMalePed = TRUE)
	pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
	pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
	
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_TRANSITION_TASK_ANIM)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
	
	SWITCH eTransitionState
		CASE PED_TRANS_STATE_ACTIVITY_ONE
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@checkout_engine@", tlPrefix, tlActivityOneLetter, tlActivityTwoLetter)
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ELSE
					GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ENDIF
			ENDIF
		BREAK
		CASE PED_TRANS_STATE_ACTIVITY_TWO
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@checkout_engine@", tlPrefix, tlActivityTwoLetter, tlActivityOneLetter)
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ELSE
					GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_TRANSITION_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, PED_TRANSITION_STATES eTransitionState, BOOL bPedTransition, TEXT_LABEL_23 tlPrefix, TEXT_LABEL_3 tlActivityOneLetter, TEXT_LABEL_3 tlActivityTwoLetter, BOOL bMalePed = TRUE)
	pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
	pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
	
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_TRANSITION_TASK_ANIM)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
	
	SWITCH eTransitionState
		CASE PED_TRANS_STATE_ACTIVITY_ONE
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@listen_music@", tlPrefix, tlActivityOneLetter, tlActivityTwoLetter)
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ELSE
					GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ENDIF
			ENDIF
		BREAK
		CASE PED_TRANS_STATE_ACTIVITY_TWO
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@listen_music@", tlPrefix, tlActivityTwoLetter, tlActivityOneLetter)
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ELSE
					GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_SMOKE_TRANSITION_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, PED_TRANSITION_STATES eTransitionState, BOOL bPedTransition, TEXT_LABEL_23 tlPrefix, TEXT_LABEL_3 tlActivityOneLetter, TEXT_LABEL_3 tlActivityTwoLetter, BOOL bMalePed = TRUE)
	pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
	pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
	
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_TRANSITION_TASK_ANIM)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
	
	SWITCH eTransitionState
		CASE PED_TRANS_STATE_ACTIVITY_ONE
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@checkout_car@", tlPrefix, tlActivityOneLetter, tlActivityTwoLetter)
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_SMOKE_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ELSE
					GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_SMOKE_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ENDIF
			ENDIF
		BREAK
		CASE PED_TRANS_STATE_ACTIVITY_TWO
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@checkout_car@", tlPrefix, tlActivityTwoLetter, tlActivityOneLetter)
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_SMOKE_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ELSE
					GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_SMOKE_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, PED_TRANSITION_STATES eTransitionState, BOOL bPedTransition, TEXT_LABEL_23 tlPrefix, TEXT_LABEL_3 tlActivityOneLetter, TEXT_LABEL_3 tlActivityTwoLetter, BOOL bMalePed = TRUE)
	pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
	pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
	
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_TRANSITION_TASK_ANIM)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
	
	SWITCH eTransitionState
		CASE PED_TRANS_STATE_ACTIVITY_ONE
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@take_photos@", tlPrefix, tlActivityOneLetter, tlActivityTwoLetter)
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ELSE
					GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ENDIF
			ENDIF
		BREAK
		CASE PED_TRANS_STATE_ACTIVITY_TWO
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@take_photos@", tlPrefix, tlActivityTwoLetter, tlActivityOneLetter)
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ELSE
					GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

//╒═══════════════════════════════╕
//╞════════╡ ANIM LOOKUP ╞════════╡
//╘═══════════════════════════════╛
PROC GET_CAR_MEET_SANDBOX_PAPARAZZI_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
//	VECTOR vPosition = <<17.8506, 20.6003, 2.1004>>	
//	vPosition = TRANSFORM_LOCAL_COORDS_TO_WORLD_COORDS(_GET_CAR_MEET_SANDBOX_PED_LOCAL_COORDS_BASE_POSITION(), _GET_CAR_MEET_SANDBOX_PED_LOCAL_HEADING_BASE_HEADING(),vPosition)
//	START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_rcpap1_camera", pedPaps[1].m_cam,vPosition,  <<0,0,-180.0>>)
//	PRINTLN("playing SHUTTER_FLASH")
//	PLAY_SOUND_FROM_COORD(-1, "SHUTTER_FLASH", GET_ENTITY_COORDS(pedPaps[1].m_cam), "CAMERA_FLASH_SOUNDSET")
	SWITCH iClip
		CASE 0						
			pedAnimData.sAnimDict="amb@world_human_paparazzi@male@base"	
			pedAnimData.sAnimClip = "base"	
		BREAK
		CASE 1	
			pedAnimData.sAnimDict="amb@world_human_paparazzi@male@idle_a"		
			pedAnimData.sAnimClip = "idle_a"													     
		BREAK
		CASE 2
			pedAnimData.sAnimDict="amb@world_human_paparazzi@male@base"	
			pedAnimData.sAnimClip = "base"	
		BREAK
		CASE 3
			pedAnimData.sAnimDict="amb@world_human_paparazzi@male@base"	
			pedAnimData.sAnimClip = "base"	
		BREAK
	ENDSWITCH
ENDPROC
PROC _GET_CAR_MEET_SANDBOX_PED_ANIM_DATA(PED_ACTIVITIES eActivity, PED_ANIM_DATA &pedAnimData, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, INT iPedID = 0, PED_TRANSITION_STATES eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE, BOOL bPedTransition = FALSE)
	UNUSED_PARAMETER(iPedID)
	UNUSED_PARAMETER(eMusicIntensity)
	UNUSED_PARAMETER(ePedMusicIntensity)
	UNUSED_PARAMETER(bDancingTransition)
	RESET_PED_ANIM_DATA(pedAnimData)
	TEXT_LABEL_3 letter
	TEXT_LABEL_3 letterTwo
	TEXT_LABEL_23 prefix
	SWITCH eActivity
		CASE PED_ACT_LEANING_TEXTING_M_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@TEXTING@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_LEANING_TEXTING_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_LEANING@FEMALE@WALL@BACK@TEXTING@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_ON_PHONE_M_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@MOBILE@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_ON_PHONE_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_LEANING@FEMALE@WALL@BACK@MOBILE@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_LEANING_SMOKING_M_01
		CASE PED_ACT_LEANING_SMOKING_F_01
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_CAR_MEET_LEANING_SMOKING_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_STANDING_SMOKING_M_01
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_CAR_MEET_STANDING_SMOKING_M_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_STANDING_SMOKING_F_01
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_CAR_MEET_STANDING_SMOKING_F_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_HANGOUT_M_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_HANG_OUT_STREET@MALE_C@BASE"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE PED_ACT_HANGOUT_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_HANG_OUT_STREET@FEMALE_HOLD_ARM@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE PED_ACT_SMOKING_WEED_M_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_SMOKING_POT@MALE@BASE"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE PED_ACT_SMOKING_WEED_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_SMOKING_POT@FEMALE@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_CAR_MEET_DRINK_COFFEE_M
			pedAnimData.sAnimDict		= "amb@world_human_drinking@coffee@male@base"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_CAR_MEET_DRINK_COFFEE_F
			pedAnimData.sAnimDict		= "amb@world_human_drinking@coffee@female@base"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_CAR_MEET_PHONE_F
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_STAND_MOBILE@FEMALE@TEXT@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE PED_ACT_CAR_MEET_PHONE_M
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@MALE@TEXT@IDLE_A"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE PED_ACT_CAR_MEET_PAPARAZZI				
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SCENARIO_IN_PLACE)	
			pedAnimData.sAnimClip = "WORLD_HUMAN_PAPARAZZI"
		BREAK
		CASE PED_ACT_CAR_MEET_INSPECT
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SCENARIO_IN_PLACE)	
			pedAnimData.sAnimClip = "WORLD_HUMAN_INSPECT_STAND"
		BREAK
		
		
		////////////////////NEW ANIMATIONS//////////////////////////////
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			
			letter = "a"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_DRINK_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_B
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			
			letter = "b"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_DRINK_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_FEMALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "a"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_DRINK_FEMALE_ANIM_CLIP(pedAnimData,iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			letter = "a"			
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			letter = "b"			
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_C
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			letter = "c"			
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_D
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			letter = "d"			
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "a"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip,letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "b"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip,letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "c"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip,letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "d"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip,letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "e"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip,letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_F			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "f"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip,letter)		
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_G		
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "g"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip,letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_H
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "h"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip,letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "a"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_SMOKE_MALE_ANIM_CLIP(pedAnimData,iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_B
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "b"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_SMOKE_MALE_ANIM_CLIP(pedAnimData,iClip, letter)
		BREAK
			CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_FEMALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "a"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_SMOKE_FEMALE_ANIM_CLIP(pedAnimData,iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "a"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "b"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "c"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "d"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_E
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "e"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_F
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "f"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_G
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "g"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_H
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "h"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "a"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_B
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "b"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_C
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "c"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_D
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "d"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_FEMALE_A		
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			letter = "a"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_DRINK_FEMALE_ANIM_CLIP(pedAnimData,iClip,letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_MALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME |  AF_EXPAND_PED_CAPSULE_FROM_SKELETON
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			
			letter = "a"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_DRINK_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			letter = "a"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_SMOKE_MALE_ANIM_CLIP(pedAnimData,iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_B
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			letter = "b"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_SMOKE_MALE_ANIM_CLIP(pedAnimData,iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_C
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			letter = "c"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_SMOKE_MALE_ANIM_CLIP(pedAnimData,iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			letter = "a"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_SMOKE_FEMALE_ANIM_CLIP(pedAnimData,iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME |  AF_EXPAND_PED_CAPSULE_FROM_SKELETON
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "a"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "b"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "c"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "d"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "e"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME |  AF_EXPAND_PED_CAPSULE_FROM_SKELETON
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "f"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_G			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "g"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_H			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "h"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "a"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "b"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_C			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "c"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_D			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "d"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A	
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)		
			letter = "a"	
			GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_MALE_ANIM_CLIP(pedAnimData, iClip, letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B	
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME		
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)		
			letter = "b"	
			GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_MALE_ANIM_CLIP(pedAnimData, iClip, letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_C	
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME		
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)		
			letter = "c"	
			GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_MALE_ANIM_CLIP(pedAnimData, iClip, letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_D			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME		
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)		
			letter = "d"	
			GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_A	
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME		
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)		
			letter = "a"	
			GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_B	
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME		
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)		
			letter = "b"	
			GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "drink_male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_C
			letter 		= "a"
			letterTwo 	= "c"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_D
			letter 		= "a"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_E
			letter 		= "a"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_F
			letter 		= "a"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_G
			letter 		= "a"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_H
			letter 		= "a"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_C
			letter 		= "b"
			letterTwo 	= "c"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_D
			letter 		= "b"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_E
			letter 		= "b"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_F
			letter 		= "b"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_G
			letter 		= "b"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_H
			letter 		= "b"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_D
			letter 		= "c"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_E
			letter 		= "c"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_F
			letter 		= "c"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_G
			letter 		= "c"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_H
			letter 		= "c"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_E
			letter 		= "d"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_F
			letter 		= "d"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_G
			letter 		= "d"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_H
			letter 		= "d"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E_TRANS_F
			letter 		= "e"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E_TRANS_G
			letter 		= "e"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E_TRANS_H
			letter 		= "e"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_F_TRANS_G
			letter 		= "f"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_F_TRANS_H
			letter 		= "f"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_G_TRANS_H
			letter 		= "g"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A_TRANS_C
			letter 		= "a"
			letterTwo 	= "c"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A_TRANS_D
			letter 		= "a"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B_TRANS_C
			letter 		= "b"
			letterTwo 	= "c"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B_TRANS_D
			letter 		= "b"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_C_TRANS_D
			letter 		= "c"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_C
			letter 		= "a"
			letterTwo 	= "c"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_D
			letter 		= "a"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_E
			letter 		= "a"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_F
			letter 		= "a"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_G
			letter 		= "a"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_H
			letter 		= "a"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_C
			letter 		= "b"
			letterTwo 	= "c"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_D
			letter 		= "b"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_E
			letter 		= "b"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_F
			letter 		= "b"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_G
			letter 		= "b"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_H
			letter 		= "b"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_D
			letter 		= "c"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_E
			letter 		= "c"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_F
			letter 		= "c"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_G
			letter 		= "c"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_H
			letter 		= "c"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_E
			letter 		= "d"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_F
			letter 		= "d"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_G
			letter 		= "d"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_H
			letter 		= "d"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_E_TRANS_F
			letter 		= "e"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_E_TRANS_G
			letter 		= "e"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_E_TRANS_H
			letter 		= "e"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_F_TRANS_G
			letter 		= "f"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_F_TRANS_H
			letter 		= "f"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_G_TRANS_H
			letter 		= "g"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A_TRANS_C
			letter 		= "a"
			letterTwo 	= "c"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A_TRANS_D
			letter 		= "a"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_B_TRANS_C
			letter 		= "b"
			letterTwo 	= "c"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_B_TRANS_D
			letter 		= "b"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_C_TRANS_D
			letter 		= "c"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "smoke_male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_SMOKE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A_TRANS_C
			letter 		= "a"
			letterTwo 	= "c"
			prefix 		= "smoke_male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_SMOKE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_B_TRANS_C
			letter 		= "b"
			letterTwo 	= "c"
			prefix 		= "smoke_male_"
			GET_CAR_MEET_SANDBOX_CHECKOUT_CAR_SMOKE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_C
			letter 		= "a"
			letterTwo 	= "c"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_D
			letter 		= "a"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_E
			letter 		= "a"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_F
			letter 		= "a"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_G
			letter 		= "a"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_H
			letter 		= "a"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_C
			letter 		= "b"
			letterTwo 	= "c"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_D
			letter 		= "b"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_E
			letter 		= "b"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_F
			letter 		= "b"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_G
			letter 		= "b"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_H
			letter 		= "b"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_D
			letter 		= "c"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_E
			letter 		= "c"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_F
			letter 		= "c"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_G
			letter 		= "c"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_H
			letter 		= "c"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_E
			letter 		= "d"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_F
			letter 		= "d"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_G
			letter 		= "d"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_H
			letter 		= "d"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_F
			letter 		= "e"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_G
			letter 		= "e"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_H
			letter 		= "e"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F_TRANS_G
			letter 		= "f"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F_TRANS_H
			letter 		= "f"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_G_TRANS_H
			letter 		= "g"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_C
			letter 		= "a"
			letterTwo 	= "c"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_D
			letter 		= "a"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B_TRANS_C
			letter 		= "b"
			letterTwo 	= "c"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B_TRANS_D
			letter 		= "b"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_C_TRANS_D
			letter 		= "c"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_C
			letter 		= "a"
			letterTwo 	= "c"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_D
			letter 		= "a"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B_TRANS_C
			letter 		= "b"
			letterTwo 	= "c"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B_TRANS_D
			letter 		= "b"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_C_TRANS_D
			letter 		= "c"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "female_"
			GET_CAR_MEET_SANDBOX_LISTEN_MUSIC_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
	ENDSWITCH
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PROP ANIM DATA ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _GET_CAR_MEET_SANDBOX_PED_PROP_ANIM_DATA(PED_ACTIVITIES eActivity, PED_PROP_ANIM_DATA &pedPropAnimData, INT iProp = 0, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bHeeledPed = FALSE)
	UNUSED_PARAMETER(eMusicIntensity)
	UNUSED_PARAMETER(ePedMusicIntensity)
	UNUSED_PARAMETER(bDancingTransition)
	UNUSED_PARAMETER(bHeeledPed)
	RESET_PED_PROP_ANIM_DATA(pedPropAnimData)
	
	SWITCH eActivity
		CASE PED_ACT_PAVEL_SCREENS
			pedPropAnimData.sPropAnimDict = "anim@scripted@submarine@special_peds@pavel@hs4_pavel_ig1_screens"
			SWITCH iProp
				CASE 0	// P_AMB_CLIPBOARD_01
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_CLIPBOARD"					BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_CLIPBOARD"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_CLIPBOARD"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_CLIPBOARD"						BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "IDLE_D_CLIPBOARD"						BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "Beep_to_you_too_CLIPBOARD"				BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "Bide_your_time_CLIPBOARD"				BREAK
						CASE 7	pedPropAnimData.sPropAnimClip = "Fucking_bridge_CLIPBOARD"				BREAK
						CASE 8	pedPropAnimData.sPropAnimClip = "Fucking_screens_CLIPBOARD"				BREAK
						CASE 9	pedPropAnimData.sPropAnimClip = "Piece_of_shit_CLIPBOARD"				BREAK
						CASE 10	pedPropAnimData.sPropAnimClip = "Prefer_when_it_was_tv_CLIPBOARD"		BREAK
						CASE 11	pedPropAnimData.sPropAnimClip = "Six_degrees_CLIPBOARD"					BREAK
						CASE 12	pedPropAnimData.sPropAnimClip = "Sure_its_nothing_CLIPBOARD"			BREAK
						CASE 13	pedPropAnimData.sPropAnimClip = "Why_letter_so_small_CLIPBOARD"			BREAK
						CASE 14	pedPropAnimData.sPropAnimClip = "Wonder_what_that_means_CLIPBOARD"		BREAK
					ENDSWITCH
				BREAK
				CASE 1	// PROP_PENCIL_01
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_PENCIL"						BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_PENCIL"							BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_PENCIL"							BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_PENCIL"							BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "IDLE_D_PENCIL"							BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "Beep_to_you_too_PENCIL"				BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "Bide_your_time_PENCIL"					BREAK
						CASE 7	pedPropAnimData.sPropAnimClip = "Fucking_bridge_PENCIL"					BREAK
						CASE 8	pedPropAnimData.sPropAnimClip = "Fucking_screens_PENCIL"				BREAK
						CASE 9	pedPropAnimData.sPropAnimClip = "Piece_of_shit_PENCIL"					BREAK
						CASE 10	pedPropAnimData.sPropAnimClip = "Prefer_when_it_was_tv_PENCIL"			BREAK
						CASE 11	pedPropAnimData.sPropAnimClip = "Six_degrees_PENCIL"					BREAK
						CASE 12	pedPropAnimData.sPropAnimClip = "Sure_its_nothing_PENCIL"				BREAK
						CASE 13	pedPropAnimData.sPropAnimClip = "Why_letter_so_small_PENCIL"			BREAK
						CASE 14	pedPropAnimData.sPropAnimClip = "Wonder_what_that_means_PENCIL"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_PAVEL_TORPEDOS
			pedPropAnimData.sPropAnimDict = "anim@scripted@submarine@special_peds@pavel@hs4_pavel_ig4_torpedos"
			SWITCH iProp
				CASE 0	// PROP_CS_CIGGY_01B
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_PROP_CS_CIGGY_01B"			BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_PROP_CS_CIGGY_01B"				BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_PROP_CS_CIGGY_01B"				BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_PROP_CS_CIGGY_01B"				BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "GESTURE_PROP_CS_CIGGY_01B"				BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_PAVEL_CAVIAR
			pedPropAnimData.sPropAnimDict = "anim@scripted@submarine@special_peds@pavel@hs4_pavel_ig5_caviar_p1"
			SWITCH iProp
				CASE 0	// h4_prop_h4_Caviar_Tin_01a
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_TIN"							BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_TIN"							BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_TIN"							BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_TIN"							BREAK
					ENDSWITCH
				BREAK
				CASE 1	// h4_prop_h4_Caviar_Spoon_01a
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_SPOON"						BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_SPOON"							BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_SPOON"							BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_SPOON"							BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_PAVEL_ENGINE
			pedPropAnimData.sPropAnimDict = "anim@scripted@submarine@special_peds@pavel@hs4_pavel_ig6_engine"
			SWITCH iProp
				CASE 0	// Prop_CS_Wrench
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_WRENCH"						BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_WRENCH"							BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_WRENCH"							BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_WRENCH"							BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "IDLE_D_WRENCH"							BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "DONT_DO_THIS_WRENCH"					BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "DONT_TALK_BACK_WRENCH"					BREAK
						CASE 7	pedPropAnimData.sPropAnimClip = "FINE_WRENCH"							BREAK
						CASE 8	pedPropAnimData.sPropAnimClip = "GENTLY_WRENCH"							BREAK
						CASE 9	pedPropAnimData.sPropAnimClip = "IF_I_HAVE_TO_COMEDOWN_WRENCH"			BREAK
						CASE 10	pedPropAnimData.sPropAnimClip = "I_AM_PATIENT_WRENCH"					BREAK
						CASE 11	pedPropAnimData.sPropAnimClip = "I_WARNED_YOU_WRENCH"					BREAK
						CASE 12	pedPropAnimData.sPropAnimClip = "PERFECT_WRENCH"						BREAK
						CASE 13	pedPropAnimData.sPropAnimClip = "PLAY_NICE_WRENCH"						BREAK
						CASE 14	pedPropAnimData.sPropAnimClip = "UGH_IAM_SORRY_WRENCH"					BREAK
					ENDSWITCH
				BREAK
				CASE 1	// h4_Prop_h4_Engine_FuseBox_01a
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_FUSE"						BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_FUSE"							BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_FUSE"							BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_FUSE"							BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "IDLE_D_FUSE"							BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "DONT_DO_THIS_FUSE"						BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "DONT_TALK_BACK_FUSE"					BREAK
						CASE 7	pedPropAnimData.sPropAnimClip = "FINE_FUSE"								BREAK
						CASE 8	pedPropAnimData.sPropAnimClip = "GENTLY_FUSE"							BREAK
						CASE 9	pedPropAnimData.sPropAnimClip = "IF_I_HAVE_TO_COMEDOWN_FUSE"			BREAK
						CASE 10	pedPropAnimData.sPropAnimClip = "I_AM_PATIENT_FUSE"						BREAK
						CASE 11	pedPropAnimData.sPropAnimClip = "I_WARNED_YOU_FUSE"						BREAK
						CASE 12	pedPropAnimData.sPropAnimClip = "PERFECT_FUSE"							BREAK
						CASE 13	pedPropAnimData.sPropAnimClip = "PLAY_NICE_FUSE"						BREAK
						CASE 14	pedPropAnimData.sPropAnimClip = "UGH_IAM_SORRY_FUSE"					BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ DANCING DATA ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _DOES_CAR_MEET_SANDBOX_PED_LOCATION_USE_DANCING_PEDS()
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ CUTSCENE PEDS ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _SHOULD_HIDE_CAR_MEET_SANDBOX_CUTSCENE_PED(SCRIPT_PED_DATA &LocalData, INT iLayout, INT iPed)
	UNUSED_PARAMETER(LocalData)
	UNUSED_PARAMETER(iLayout)
	UNUSED_PARAMETER(iPed)
	
	RETURN TRUE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ LOOK-UP TABLE ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC BUILD_PED_CAR_MEET_SANDBOX_LOOK_UP_TABLE(PED_INTERFACE &interface, PED_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		// Script Launching
		CASE E_SHOULD_PED_SCRIPT_LAUNCH
			interface.returnShouldPedScriptLaunch = &_SHOULD_CAR_MEET_SANDBOX_PED_SCRIPT_LAUNCH
		BREAK
		CASE E_IS_PARENT_A_SIMPLE_INTERIOR
			interface.returnIsParentASimpleInterior = &_IS_CAR_MEET_SANDBOX_PARENT_A_SIMPLE_INTERIOR
		BREAK
		
		// Ped Data
		CASE E_GET_LOCAL_PED_TOTAL
			interface.returnGetLocalPedTotal = &_GET_CAR_MEET_SANDBOX_LOCAL_PED_TOTAL
		BREAK
		CASE E_GET_NETWORK_PED_TOTAL
			interface.returnGetNetworkPedTotal = &_GET_CAR_MEET_SANDBOX_NETWORK_PED_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT_TOTAL
			interface.returnGetServerPedLayoutTotal= &_GET_CAR_MEET_SANDBOX_SERVER_PED_LAYOUT_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT
			interface.returnGetServerPedLayout = &_GET_CAR_MEET_SANDBOX_SERVER_PED_LAYOUT
		BREAK
		CASE E_GET_SERVER_PED_LEVEL
			interface.returnGetServerPedLevel = &_GET_CAR_MEET_SANDBOX_SERVER_PED_LEVEL
		BREAK
		CASE E_SET_PED_DATA
			interface.setPedData = &_SET_CAR_MEET_SANDBOX_PED_DATA
		BREAK
		CASE E_SET_PED_SERVER_DATA
			interface.setPedServerData = &_SET_CAR_MEET_SANDBOX_PED_SERVER_DATA
		BREAK
		CASE E_GET_PED_ANIM_DATA
			interface.getPedAnimData = &_GET_CAR_MEET_SANDBOX_PED_ANIM_DATA
		BREAK
		CASE E_GET_PED_PROP_ANIM_DATA
			interface.getPedPropAnimData = &_GET_CAR_MEET_SANDBOX_PED_PROP_ANIM_DATA
		BREAK
		CASE E_IS_PLAYER_IN_PARENT_PROPERTY
			interface.returnIsPlayerInParentProperty = &_IS_PLAYER_IN_CAR_MEET_SANDBOX_PARENT_PROPERTY
		BREAK
		CASE E_GET_PED_LOCAL_COORDS_BASE_POSITION
			interface.returnGetPedLocalCoordsBasePosition = &_GET_CAR_MEET_SANDBOX_PED_LOCAL_COORDS_BASE_POSITION
		BREAK
		CASE E_GET_PED_LOCAL_HEADING_BASE_HEADING
			interface.returnGetPedLocalHeadingBaseHeading = &_GET_CAR_MEET_SANDBOX_PED_LOCAL_HEADING_BASE_HEADING
		BREAK
		
		// Ped Creation
		CASE E_SET_LOCAL_PED_PROPERTIES
			interface.setLocalPedProperties = &_SET_CAR_MEET_SANDBOX_LOCAL_PED_PROPERTIES
		BREAK
		CASE E_SET_NETWORK_PED_PROPERTIES
			interface.setNetworkPedProperties = &_SET_CAR_MEET_SANDBOX_NETWORK_PED_PROPERTIES
		BREAK
		CASE E_SET_PED_PROP_INDEXES
			interface.setPedPropIndexes = &_SET_CAR_MEET_SANDBOX_PED_PROP_INDEXES
		BREAK
		CASE E_HAS_PED_BEEN_CREATED
			interface.returnHasPedBeenCreated = &_HAS_CAR_MEET_SANDBOX_PED_BEEN_CREATED
		BREAK
		
		// Ped Speech
		CASE E_SET_PED_SPEECH_DATA
			interface.setPedSpeechData = &_SET_CAR_MEET_SANDBOX_PED_SPEECH_DATA
		BREAK
		CASE E_CAN_PED_PLAY_SPEECH
			interface.returnCanPedPlaySpeech = &_CAN_CAR_MEET_SANDBOX_PED_PLAY_SPEECH
		BREAK
		CASE E_GET_PED_SPEECH_TYPE
			interface.returnGetPedSpeechType = &_GET_CAR_MEET_SANDBOX_PED_SPEECH_TYPE
		BREAK
		CASE E_GET_PED_CONTROLLER_SPEECH
			interface.returnGetPedControllerSpeech = &_GET_CAR_MEET_SANDBOX_PED_CONTROLLER_SPEECH
		BREAK
		CASE E_GET_PED_CONVO_DATA
			interface.getPedConvoData = &_GET_CAR_MEET_SANDBOX_PED_CONVO_DATA
		BREAK
		
		// Dancing Data
		CASE E_DOES_PED_LOCATION_USE_DANCING_PEDS
			interface.returnDoesPedLocationUseDancingPeds = &_DOES_CAR_MEET_SANDBOX_PED_LOCATION_USE_DANCING_PEDS
		BREAK
		
		// Cutscene Peds
		CASE E_SHOULD_HIDE_CUTSCENE_PED
			interface.returnShouldHideCutscenePed = &_SHOULD_HIDE_CAR_MEET_SANDBOX_CUTSCENE_PED
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF	// FEATURE_TUNER
