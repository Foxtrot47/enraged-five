USING "rage_builtins.sch"
USING "globals.sch"

USING "net_mission_info_private.sch"



// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************
//
//      MISSION NAME    :   Net_Mission_Info_Initialiser.sch
//      CREATED         :   Keith
//      DESCRIPTION     :   Stores all the MP Mission info in globals.
//		NOTES			:	This header should only contain the initialisation routines so that it doesn't need to include any other
//								headers. The data access functions will still be stored in net_mission_info.sch.
//
// *************************************************************************************************************************************
// *************************************************************************************************************************************
// *************************************************************************************************************************************




// ===========================================================================================================
//      Mission Data Reset Functions
// ===========================================================================================================

// PURPOSE:	Clears all the mission info array data.
PROC Reset_MP_Mission_Info()

	PRINTSTRING("          Reset_MP_Mission_Info()") PRINTNL()

	INT tempLoop = 0
	
	// Clear the Mission Info array
	REPEAT eMAX_NUM_MP_MISSION tempLoop
		g_sMissionInfo[tempLoop].playersAreInTeams	= FALSE
		g_sMissionInfo[tempLoop].numVariations		= UNDEFINED_MISSION_INFO
		g_sMissionInfo[tempLoop].arrayPosFirstVar	= UNDEFINED_MISSION_INFO
		g_sMissionInfo[tempLoop].instances			= NO_MP_INST
		g_sMissionInfo[tempLoop].teamStorage		= NO_MIT_DATA
		g_sMissionInfo[tempLoop].arrayPosFirstTeam	= UNDEFINED_MISSION_INFO
	ENDREPEAT

	// Clear the Mission Variations array
	REPEAT MAX_MP_MISSION_VARIATIONS tempLoop
		g_sMissionVariations[tempLoop].bitsOptionalFlags	= ALL_MISSION_INFO_BITS_CLEAR
		g_sMissionVariations[tempLoop].optionalRadius		= 0.0
		g_sMissionVariations[tempLoop].locMethod			= NO_MP_LOC
		g_sMissionVariations[tempLoop].teamConfig			= NO_MP_TEAM_CONFIG
		g_sMissionVariations[tempLoop].launchTeams			= NO_MP_LAUNCH_TEAMS
		g_sMissionVariations[tempLoop].arrayPosFirstVarTeam	= UNDEFINED_MISSION_INFO
	ENDREPEAT
	g_numVariationsInUse = 0
	
	// Clear the Mission Team Data array (holds team data that is the same across all variations of a mission)
	REPEAT MAX_MI_TEAMS tempLoop
		g_sTeamInfoPerMission[tempLoop].bitsTeamMissionFlags	= ALL_MISSION_INFO_BITS_CLEAR
	ENDREPEAT
	g_numTeamInfoPerMissionInUse = 0
	
	// Clear the Variation Team Data array (holds team data that can differ for different variations of a mission)
	REPEAT MAX_MI_VAR_TEAMS tempLoop
		g_sTeamInfoPerVariation[tempLoop].bitsTeamVariationFlags	= ALL_MISSION_INFO_BITS_CLEAR
	ENDREPEAT
	g_numTeamInfoPerVariationInUse = 0

ENDPROC




// ===========================================================================================================
//      Mission Data Helper Functions
// ===========================================================================================================

// PURPOSE:	Perform standard error checks on the passed in data, and retrieve the start (inclusive) and end (exclusive) variation array position for the data
//
// INPUT PARAMS:		paramMissionID			The Mission ID
//						paramVariationID		The variation ID
// RETURN PARAMS:		paramStartPos			The first position in the Variations array to get updated (Inclusive - it will change this data location)
//						paramEndPos				The last position in the Variations array to get updated (Exclusive - it'll never change this actual data location)
// RETURN VALUE:		BOOL					TRUE if the start and end array positions have been filled, otherwise FALSE
FUNC BOOL Get_Start_And_End_Variation_ArrayPos(MP_MISSION paramMissionID, INT paramVariationID, INT &paramStartPos, INT &paramEndPos)

	// Ensure the mission has had the basic info setup
	IF (g_sMissionInfo[paramMissionID].numVariations = UNDEFINED_MISSION_INFO)
		PRINTSTRING("...KGM MP [MissionInfo]: Mission has no variations setup. Update Basic Mission Info.") PRINTNL()
		SCRIPT_ASSERT("Get_Start_And_End_Variation_ArrayPos() - Mission Has No Variations Setup. Tell Keith.")
		RETURN FALSE
	ENDIF
	
	// Does this data apply to all variations of the mission?
	BOOL forAllVariations = FALSE
	IF (paramVariationID = ALL_MISSION_VARIATIONS)
		forAllVariations = TRUE
	ENDIF
	
	// Ensure the variation ID is within the known number of variations for the mission
	IF NOT (forAllVariations)
		IF (paramVariationID >= g_sMissionInfo[paramMissionID].numVariations)
			PRINTSTRING("...KGM MP [MissionInfo]: VariationID exceeds number of setup variations for mission. Update Basic Mission Info.") PRINTNL()
			SCRIPT_ASSERT("Get_Start_And_End_Variation_ArrayPos() - Mission Variation is outwith known number of variations for mission. Tell Keith.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	// Retrieve the array position for the mission variation data - initially set start and end positions that covers all variations for a mission
	paramStartPos	= g_sMissionInfo[paramMissionID].arrayPosFirstVar
	paramEndPos		= paramStartPos + g_sMissionInfo[paramMissionID].numVariations
	
	IF NOT (forAllVariations)
		// ...this is just for one variation, so set the first variation position to be the required variation, and the end variation position to be one position after
		paramStartPos	+= paramVariationID
		paramEndPos		= paramStartPos + 1
	ENDIF
	
	// Safety Test, ensure the final variation isn't greater than the max variations (being equal is ok)
	#IF IS_DEBUG_BUILD
		IF (paramEndPos > g_numVariationsInUse)
			PRINTSTRING("...KGM MP [MissionInfo]: Trying to store data in a variation that exceeds the max variations in use.") PRINTNL()
			PRINTSTRING("          MissionID     : ") PRINTINT(ENUM_TO_INT(paramMissionID)) PRINTNL()
			PRINTSTRING("          END VarID     : ") PRINTINT(paramEndPos) PRINTNL()
			PRINTSTRING("          MAX VAR In USE: ") PRINTINT(g_numVariationsInUse) PRINTNL()
			SCRIPT_ASSERT("Get_Start_And_End_Variation_ArrayPos() - Mission Variation is outwith max number of variations in use in the array. Tell Keith.")
			RETURN FALSE
		ENDIF
	#ENDIF
	
	RETURN TRUE
	
ENDFUNC




// ===========================================================================================================
//      Mission Data Fill Generic Functions
// ===========================================================================================================

// PURPOSE:	To store the basic information for one mission.
//
// INPUT PARAMS:		paramMission			The Mission ID
//						paramInTeams			TRUE if players are in teams, FALSE if all players are individuals
//						paramVars				The number of variations of the mission
//						paramTeamStorage		The Team Storage description - used to decide how many team storage structs are required for this mission
//						paramInst				Specifies the number of instances of a mission allowed to be active at the same time
PROC Set_MP_Mission_Basic_Info(MP_MISSION paramMissionID, BOOL paramInTeams, INT paramVars, g_eMITeamStorage paramTeamStorage, g_eMPInstances paramInst)

	// Ensure the number of variations being setup is legal.
	IF (paramVars <= 0)
		PRINTSTRING("...KGM MP [MissionInfo]: Number of Variations of Mission is being set to 0. Should be at least 1.") PRINTNL()
		SCRIPT_ASSERT("Set_MP_Mission_Basic_Info() - Number of Variations of Mission should be at least 1. Tell Keith.")
		EXIT
	ENDIF
	
	// Ensure there is space on the variation array
	IF ((g_numVariationsInUse + paramVars) > MAX_MP_MISSION_VARIATIONS)
		PRINTSTRING("...KGM MP [MissionInfo]: Run out of space on the Mission Variations array. Increase MAX_MP_MISSION_VARIATIONS.") PRINTNL()
		SCRIPT_ASSERT("Set_MP_Mission_Basic_Info() - Run out of space on the Mission Variations array. Increase MAX_MP_MISSION_VARIATIONS. Tell Keith.")
		EXIT
	ENDIF
	
	// Ensure the Instance is legal
	#IF IS_DEBUG_BUILD
		IF (paramInst = NO_MP_INST)
			PRINTSTRING("...KGM MP [MissionInfo]: Allowed Instances has been set to NO_MP_INST.") PRINTNL()
			SCRIPT_ASSERT("Set_MP_Mission_Basic_Info() - Allowed Instances has been set to NO_MP_INST. TEMP SETTING TO MP_INST_ONE. Tell Keith.")
			paramInst = MP_INST_ONE
		ENDIF
	#ENDIF
	
	// If the mission is not TeamBased then all players should use the same Team Storage
	#IF IS_DEBUG_BUILD
		IF NOT (paramInTeams)
		AND (paramTeamStorage != MIT_ALL)
			PRINTSTRING("...KGM MP [MissionInfo]: Mission is not Team-Based, but the team storage requirement is not MIT_ALL.") PRINTNL()
			SCRIPT_ASSERT("Set_MP_Mission_Basic_Info() - Illegal Team Storage setup for mission that is not team-based. TEMP SETTING TO MIT_ALL. Tell Keith.")
			paramTeamStorage = MIT_ALL
		ENDIF
	#ENDIF
	
	// Store the basic information
	g_sMissionInfo[paramMissionID].playersAreInTeams	= paramInTeams
	g_sMissionInfo[paramMissionID].numVariations		= paramVars
	g_sMissionInfo[paramMissionID].arrayPosFirstVar		= g_numVariationsInUse
	g_sMissionInfo[paramMissionID].instances			= paramInst
	
	// Update the total number of variations in use
	g_numVariationsInUse += paramVars
	
	// Team Storage Requirements
	INT teamStorageNeeded = Get_Number_Of_Elements_Of_Team_Storage(paramTeamStorage)
	
	// Ensure the Team storage requirements are legal
	IF (teamStorageNeeded = 0)
		PRINTSTRING("...KGM MP [MissionInfo]: No Team Storage Requirements have been specified. Mission needs to specify the team storage requirements.") PRINTNL()
		SCRIPT_ASSERT("Set_MP_Mission_Basic_Info() - Mission hasn't specified the team storage array requirments. Tell Keith.")
		EXIT
	ENDIF
	
	// Store the Team Storage Requirements ID - used for both Mission Team Data and Variation Team Data
	g_sMissionInfo[paramMissionID].teamStorage	= paramTeamStorage
	
	// Mission Team Storage array
	// ...ensure there is space on the Mission Team Storage array - this array hold team data that is consistent across all variations of a mission
	IF ((g_numTeamInfoPerMissionInUse + teamStorageNeeded) > MAX_MI_TEAMS)
		PRINTSTRING("...KGM MP [MissionInfo]: Run out of space on the Mission Team Storage array. Increase MAX_MI_TEAMS.") PRINTNL()
		SCRIPT_ASSERT("Set_MP_Mission_Basic_Info() - Run out of space on the Mission Team Storage array. Increase MAX_MI_TEAMS. Tell Keith.")
		EXIT
	ENDIF
	
	// ...update the Mission Team Storage Details
	g_sMissionInfo[paramMissionID].arrayPosFirstTeam	= g_numTeamInfoPerMissionInUse
	
	// ...update the total number of Mission Team Storage in use
	g_numTeamInfoPerMissionInUse += teamStorageNeeded
	
	// Variations Team Storage array
	INT thisVariation = g_sMissionInfo[paramMissionID].arrayPosFirstVar
	INT endVariation = thisVariation + g_sMissionInfo[paramMissionID].numVariations
	WHILE (thisVariation < endVariation)
		// ...ensure there is space on the Variations Team Storage array - this array holds team data that can differ on different variations of a mission
		IF ((g_numTeamInfoPerVariationInUse + teamStorageNeeded) > MAX_MI_VAR_TEAMS)
			PRINTSTRING("...KGM MP [MissionInfo]: Run out of space on the Variations Team Storage array. Increase MAX_MI_VAR_TEAMS.") PRINTNL()
			SCRIPT_ASSERT("Set_MP_Mission_Basic_Info() - Run out of space on the Variations Team Storage array. Increase MAX_MI_VAR_TEAMS. Tell Keith.")
			EXIT
		ENDIF
		
		// ...update the Variations Team Storage Details
		g_sMissionVariations[thisVariation].arrayPosFirstVarTeam	= g_numTeamInfoPerVariationInUse
		
		// ...update the total number of Variations Team Storage in use
		g_numTeamInfoPerVariationInUse += teamStorageNeeded
		
		// Next Mission Variation
		thisVariation++
	ENDWHILE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To set the standard details for one or all variations of a mission.
//
// INPUT PARAMS:		paramMissionID			The Mission ID
//						paramVariationID		The Variation ID
//						paramLocMethod			The Location Selection Method ID
//						paramTeamConfig			The Team Configuration for the mission 
//						paramLaunchTeams		The Teams that join the mission at launch
PROC Set_MP_Variation_Control(MP_MISSION paramMissionID, INT paramVariationID, g_eMPLocMethod paramLocMethod, g_eMPTeamConfig paramTeamConfig, g_eMPLaunchTeams paramLaunchTeams)

	INT variationArrayPos		= UNDEFINED_MISSION_INFO
	INT variationsEndArrayPos	= UNDEFINED_MISSION_INFO
	
	// Check the basic data being passed in, and fill in the start and end array positions - the end array position is exclusive (ie: last valid data + 1) so will never get updated
	IF NOT (Get_Start_And_End_Variation_ArrayPos(paramMissionID, paramVariationID, variationArrayPos, variationsEndArrayPos))
		EXIT
	ENDIF
	
	// Loop through all variations that are being changed and set the data
	WHILE (variationArrayPos < variationsEndArrayPos)
		#IF IS_DEBUG_BUILD
			IF NOT (g_sMissionVariations[variationArrayPos].locMethod = NO_MP_LOC)
			OR NOT (g_sMissionVariations[variationArrayPos].teamConfig = NO_MP_TEAM_CONFIG)
			OR NOT (g_sMissionVariations[variationArrayPos].launchTeams = NO_MP_LAUNCH_TEAMS)
				PRINTSTRING("...KGM MP [MissionInfo]: Setting the same data twice for a mission variation.") PRINTNL()
				PRINTSTRING("          MissionID  : ") PRINTINT(ENUM_TO_INT(paramMissionID)) PRINTNL()
				PRINTSTRING("          VariationID: ") PRINTINT((variationArrayPos - g_sMissionInfo[paramMissionID].arrayPosFirstVar)) PRINTNL()
				// ...the data being set twice
				PRINTSTRING("             ")
				IF NOT (g_sMissionVariations[variationArrayPos].locMethod = NO_MP_LOC)
					PRINTSTRING(" [LOCATION METHOD]")
				ENDIF
				IF NOT (g_sMissionVariations[variationArrayPos].teamConfig = NO_MP_TEAM_CONFIG)
					PRINTSTRING(" [TEAM CONFIG]")
				ENDIF
				IF NOT (g_sMissionVariations[variationArrayPos].launchTeams = NO_MP_LAUNCH_TEAMS)
					PRINTSTRING(" [LAUNCH TEAMS]")
				ENDIF
				PRINTNL()
				SCRIPT_ASSERT("Set_MP_Variation_Flag() - Setting the same data twice for a mission variation. Tell Keith.")
			ENDIF
		#ENDIF
		
		// Store the data
		g_sMissionVariations[variationArrayPos].locMethod	= paramLocMethod
		g_sMissionVariations[variationArrayPos].teamConfig	= paramTeamConfig
		g_sMissionVariations[variationArrayPos].launchTeams	= paramLaunchTeams
		
		// Next variation
		variationArrayPos++
	ENDWHILE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE: To store an associated radius with an optional flag
//
// INPUT PARAMS:		paramVariationArrayPos	The array position within the variations array
//						paramRadius				The radius
PROC Set_MP_Variation_Flag_Optional_Radius(INT paramVariationArrayPos, FLOAT paramRadius)

	IF (paramRadius <= 0.0)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("...KGM MP [MissionInfo]: Storing a radius < 0.0 for an associated variation flag. Variation Array Pos: ") PRINTINT(paramVariationArrayPos) PRINTNL()
			SCRIPT_ASSERT("Set_MP_Variation_Flag() - Optional Radius is illegal. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF

	g_sMissionVariations[paramVariationArrayPos].optionalRadius = paramRadius

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To set an optional flag TRUE for one or all variations of a mission.
//
// INPUT PARAMS:		paramMissionID			The Mission ID
//						paramVariationID		The Variation ID.
//						paramFlagID				The Mission Info Flag ID.
//						paramOptionalData		[default = 0] Some flags may require a piece of associated data
PROC Set_MP_Variation_Flag(MP_MISSION paramMissionID, INT paramVariationID, INT paramFlagID, INT paramOptionalData = 0)

	INT variationArrayPos		= UNDEFINED_MISSION_INFO
	INT variationsEndArrayPos	= UNDEFINED_MISSION_INFO
	
	// Check the basic pdata being passed in, and fill in the start and end array positions - the end array position is exclusive (ie: last valid data + 1) so will never get updated
	IF NOT (Get_Start_And_End_Variation_ArrayPos(paramMissionID, paramVariationID, variationArrayPos, variationsEndArrayPos))
		EXIT
	ENDIF
	
	// Loop through all variations that are being changed and set the data
	WHILE (variationArrayPos < variationsEndArrayPos)
		#IF IS_DEBUG_BUILD
			IF (IS_BIT_SET(g_sMissionVariations[variationArrayPos].bitsOptionalFlags, paramFlagID))
				PRINTSTRING("...KGM MP [MissionInfo]: Setting the same flag twice for a mission variation.") PRINTNL()
				PRINTSTRING("          MissionID  : ") PRINTINT(ENUM_TO_INT(paramMissionID)) PRINTNL()
				PRINTSTRING("          VariationID: ") PRINTINT((variationArrayPos - g_sMissionInfo[paramMissionID].arrayPosFirstVar)) PRINTNL()
				PRINTSTRING("          FlagID     : ") PRINTINT(paramFlagID) PRINTNL()
				SCRIPT_ASSERT("Set_MP_Variation_Flag() - Setting the same flag twice for a mission variation. Tell Keith.")
			ENDIF
		#ENDIF
		
		// Set the flag
		SET_BIT(g_sMissionVariations[variationArrayPos].bitsOptionalFlags, paramFlagID)
		
		// Some flags require additional data
		SWITCH (paramFlagID)
			CASE MI_FLAG_INCLUSION_RADIUS
				Set_MP_Variation_Flag_Optional_Radius(variationArrayPos, TO_FLOAT(paramOptionalData))
				BREAK
		ENDSWITCH
		
		// Next variation
		variationArrayPos++
	ENDWHILE

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To set an optional flag TRUE for a team where the flag is consistent across all variations of a mission
//
// INPUT PARAMS:		paramMissionID			The Mission ID
//						paramTeamStorage		The Team Storage ID - describes where the data should be stored
//						paramTeamFlagID			The Team Flag ID that is consistent across all variations of a mission
//
// NOTES:	These flags are stored in the team data structs pointed to by the Mission Info array
PROC Set_MP_Mission_Team_Flag(MP_MISSION paramMissionID, g_eMITeamStorage paramTeamStorage, INT paramTeamFlagID)

	// Retrieve the array position for this Mission Team data
	INT missionTeamArrayPos = Get_MP_Mission_Team_ArrayPos(paramMissionID, paramTeamStorage)
	IF (missionTeamArrayPos = UNDEFINED_MISSION_INFO)
		// This error probably means that the team storage allocation setup in the Basic Details for this mission doesn't support the team storage ID passed in to this function
		#IF IS_DEBUG_BUILD
			PRINTSTRING("...KGM MP [MissionInfo]: Failed to retrieve Mission Team Data Array Position. Possibly TeamStorageID isn't supported by the setup Team Storage Requirements.") PRINTNL()
			PRINTSTRING("          MissionID        : ") PRINTINT(ENUM_TO_INT(paramMissionID)) PRINTNL()
			PRINTSTRING("          TeamStorageID    : ") PRINTINT(ENUM_TO_INT(paramTeamStorage)) PRINTNL()
			PRINTSTRING("          TeamStorage Setup: ") PRINTINT(ENUM_TO_INT(g_sMissionInfo[paramMissionID].teamStorage)) PRINTNL()
			PRINTSTRING("          TeamFlagID       : ") PRINTINT(paramTeamFlagID) PRINTNL()
			SCRIPT_ASSERT("Set_MP_Mission_Team_Flag() - Failed to retrieve Mission Team Data Array Position. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Set the flag
	SET_BIT(g_sTeamInfoPerMission[missionTeamArrayPos].bitsTeamMissionFlags, paramTeamFlagID)

ENDPROC




// ===========================================================================================================
//      Mission Data Fill Mission Specific Functions
// ===========================================================================================================

// PURPOSE:	To fill the Basic MP Mission Info and to set up the number of variations.
// NOTES:	Store in alphabetical order by MissionID by type.
PROC Fill_MP_Mission_Info_Basics()

	PRINTSTRING("          Fill_MP_Mission_Info_Basics()") PRINTNL()
	
	// FIELD DESCRIPTIONS
	//	MissionID		:	The Mission ID ENUM
	//	In Teams		:	TRUE if players are in teams (ie: CnC), FALSE if players are all individuals and team-based decisions are ignored (ie: Freemode)
	//	Num Vars		:	The number of variations of the mission that have been setup - this must be 1 or more
	//	Team Storage	:	The team storage requirements - most CnC missions will require Cop storage and Crook storage (Crook data tends to be the same for all Crook Teams so one element of storage is usually enough)
	//	Instances		:	The number of Instances of a mission that are allowed to be active at the same time


	// Used within Num Vars field when the number of variations haven't been specifically setup, but other data is needed - indicates which missions still have temporary data
	// NOTE: WHILE RE-STRUCTURING ONLY - ALL USES OF THIS WILL NEED ADDRESSED BY THE END OF THE RE-STRUCTURING
	CONST_INT	TMP_1		1

	//													In		Num		Team
	//						  MissionID					Teams	Vars	Storage		Instances
	//						  ---------					-----	----	-------		---------
	// Ambient - Crook
//	Set_MP_Mission_Basic_Info(eAM_CR_CLEANAREA,			TRUE,	TMP_1,	MIT_CROOKS,	MP_INST_ANY)
	
	// Used as both Cop Mission and Crook Mission
//	Set_MP_Mission_Basic_Info(eCOC_Do_Cutscene,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCP_Takeback_Territory,	TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
	
	// Cop Mission
//	Set_MP_Mission_Basic_Info(eCP_InformHeli,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCP_PrisonTrans,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCP_ProWitCourt,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_UNIQUE)
//	Set_MP_Mission_Basic_Info(eCP_vip_tour,				TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_UNIQUE)
//	Set_MP_Mission_Basic_Info(eCP_weapons_ambush,		TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
	
	// Cop Tutorial
//	Set_MP_Mission_Basic_Info(eCP_ARMOURY_TUT,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCP_PARTNER_TUT,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCP_SNITCH_TUT,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCP_STOLEN_TUT,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCP_welcomeTut,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
	
	// Crook Mission
//	Set_MP_Mission_Basic_Info(eCR_Airport_Takeover,		TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_UNIQUE)
//	Set_MP_Mission_Basic_Info(eCR_ASIN,					TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_AttackBoat,			TRUE,	2,		MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_BossMeet,				TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_CarBlowUp,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_UNIQUE)
//	Set_MP_Mission_Basic_Info(eCR_CokeChase,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_Convoy_Steal,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_DefendBase,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_DisposeOfVehicle,		TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCR_escortCrimBoss,		TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_farmhouse,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_UNIQUE)
//	Set_MP_Mission_Basic_Info(eCR_Funeral,				TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_GET_VEHICLE,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_UNIQUE)
//	Set_MP_Mission_Basic_Info(eCR_GOGET,				TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_hostage_steal,		TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_Mayhem,				TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_MilitaryBase,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_PACKAGE_GRAB,			TRUE,	8,		MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCR_parley,				TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_PLANE_DROP,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_PolBustOut,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_PRISON_BREAK,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_Races,				TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_UNIQUE)
//	Set_MP_Mission_Basic_Info(eCR_SHIPMENT_STEAL_1,		TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_UNIQUE)
//	Set_MP_Mission_Basic_Info(eCR_Showroom,				TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_UNIQUE)
//	Set_MP_Mission_Basic_Info(eCR_STEAL_BIKES,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_Steal_Vehicle,		TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCR_van_rescue,			TRUE,	1,		MIT_CP_CR,	MP_INST_ONE)
	
	// Crook Heist
//	Set_MP_Mission_Basic_Info(eCR_BANK_HEIST_1,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_COUNTRY_HEIST_1,		TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_FBI_grab,				TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
//	Set_MP_Mission_Basic_Info(eCR_FBI_HEIST_1,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ONE)
	
	// Crook Tutorial
//	Set_MP_Mission_Basic_Info(eCR_DRUGS_TUT,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCR_GUN_SHOP_TUT,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCR_HOLD_UP_TUT,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCR_HOOKER_TUT,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCR_JOBLIST_TUT,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCR_PROPERTY_TUT,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCR_RIVAL_TUT,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCR_SELL_CARS_TUT,		TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCR_SNITCH_TUT,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCR_TRIGGER_TUT,			TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
//	Set_MP_Mission_Basic_Info(eCR_WELCOME,				TRUE,	TMP_1,	MIT_CP_CR,	MP_INST_ANY)
	
	// Freemode Activities
	Set_MP_Mission_Basic_Info(eFM_ARM_WRESTLING,		FALSE,	10,		MIT_ALL,	MP_INST_ANY)		// KGM 12/11/12: Not Team-based, 10 variations, all players play together, multiple instances
	Set_MP_Mission_Basic_Info(eFM_BASEJUMP_CLOUD,		FALSE,	1,		MIT_ALL,	MP_INST_ANY)		// KGM 12/11/12: Not Team-based, 1 variation (ie: all cloud-loaded BJs will be classed as the same variation), all players play together, multiple instances
	Set_MP_Mission_Basic_Info(eFM_IMPROMPTU_DM,			FALSE,	1,		MIT_ALL,	MP_INST_ANY)		// KGM 12/11/12: Not Team-based, 1 variation (ie: all cloud-loaded BJs will be classed as the same variation), all players play together, multiple instances
	Set_MP_Mission_Basic_Info(eFM_DARTS,				FALSE,	1,		MIT_ALL,	MP_INST_ANY)		// KGM 12/11/12: Not Team-based, 1 variation, all players play together, multiple instances
	Set_MP_Mission_Basic_Info(eFM_PILOT_SCHOOL,			FALSE,	1,		MIT_ALL,	MP_INST_ANY)		// KGM 12/11/12: Not Team-based, 1 variation, all players play together, multiple instances
	Set_MP_Mission_Basic_Info(eFM_DEATHMATCH_CLOUD,		FALSE,	1,		MIT_ALL,	MP_INST_ANY)		// KGM 12/11/12: Not Team-based, 1 variation (ie: all cloud-loaded DMs will be classed as the same variation), all players play together, multiple instances
	Set_MP_Mission_Basic_Info(eFM_GANG_ATTACK_CLOUD,	FALSE,	1,		MIT_ALL,	MP_INST_ANY)		// KGM 12/11/12: Not Team-based, 1 variation (ie: all cloud-loaded GAs will be classed as the same variation), all players play together, multiple instances
	Set_MP_Mission_Basic_Info(eFM_GOLF,					FALSE,	1,		MIT_ALL,	MP_INST_ANY)		// KGM 12/11/12: Not Team-based, 1 variation, all players play together, multiple instances
	Set_MP_Mission_Basic_Info(eFM_MISSION_CLOUD,		FALSE,	1,		MIT_ALL,	MP_INST_ANY)		// KGM 12/11/12: Not Team-based, 1 variation (ie: all cloud-loaded missions will be classed as the same variation), all players play together, multiple instances
	Set_MP_Mission_Basic_Info(eFM_RACE_CLOUD,			FALSE,	1,		MIT_ALL,	MP_INST_ANY)		// KGM 12/11/12: Not Team-based, 1 variation (ie: all cloud-loaded races will be classed as the same variation), all players play together, multiple instances
	Set_MP_Mission_Basic_Info(eFM_SHOOTING_RANGE,		FALSE,	1,		MIT_ALL,	MP_INST_ANY)		// KGM 12/11/12: Not Team-based, 1 variation, all players play together, multiple instances
	Set_MP_Mission_Basic_Info(eFM_SURVIVAL_CLOUD,		FALSE,	1,		MIT_ALL,	MP_INST_ANY)		// KGM 12/11/12: Not Team-based, 1 variation (ie: all cloud-loaded Survivals will be classed as the same variation), all players play together, multiple instances
	Set_MP_Mission_Basic_Info(eFM_TENNIS,				FALSE,	1,		MIT_ALL,	MP_INST_ANY)		// KGM 12/11/12: Not Team-based, 1 variation, all players play together, multiple instances

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To set any standard control data required by each mission variation
// NOTES:	Store in alphabetical order by MissionID by type.
PROC Fill_MP_Mission_Info_Variations_Standard_Data()

	PRINTSTRING("          Fill_MP_Mission_Info_Variations_Standard_Data()") PRINTNL()
	
	// FIELD DESCRIPTIONS
	//	MissionID			:	The Mission ID ENUM
	//	Mission Variation	:	The specific mission variation this data is for (or ALL_MISSION_VARIATIONS)
	//	Location Method		:	The method used to choose a mission location
	//	Team Config			:	The configuration of teams allowed to be on this mission
	//	Launch Teams		:	A description of the teams that should join at launch - other teams will join mid-mission


	//					  	 MissionID					Mission Variation								Location Method		Team Config					Launch Teams
	//						 ---------					-----------------								---------------		-----------					------------
	// Ambient - Crook
//	Set_MP_Variation_Control(eAM_CR_CLEANAREA,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1GANG,				MP_LAUNCH_ONE_GANG)
	
	// Used as both Cop Mission and Crook Mission
//	Set_MP_Variation_Control(eCOC_Do_Cutscene,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1GANG,				MP_LAUNCH_ALL_TEAMS)
	
//	Set_MP_Variation_Control(eCP_Takeback_Territory,	ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1TEAM,				MP_LAUNCH_ONE_TEAM)
	
	// Cop Mission
//	Set_MP_Variation_Control(eCP_InformHeli,			ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1OR2GANGS,	MP_LAUNCH_ALL_COPS)
	
//	Set_MP_Variation_Control(eCP_PrisonTrans,			ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ALL_COPS)
	
//	Set_MP_Variation_Control(eCP_ProWitCourt,			ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ALL_COPS)
	
//	Set_MP_Variation_Control(eCP_vip_tour,				ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1GANG_MIN,	MP_LAUNCH_ALL_COPS)
	
//	Set_MP_Variation_Control(eCP_weapons_ambush,		ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1OR2GANGS,	MP_LAUNCH_ALL_COPS)
	
	// Cop Tutorial
//	Set_MP_Variation_Control(eCP_ARMOURY_TUT,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_COPS,				MP_LAUNCH_ALL_COPS)
	
//	Set_MP_Variation_Control(eCP_PARTNER_TUT,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_COPS,				MP_LAUNCH_ALL_COPS)
	
//	Set_MP_Variation_Control(eCP_SNITCH_TUT,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_COPS,				MP_LAUNCH_ALL_COPS)
	
//	Set_MP_Variation_Control(eCP_STOLEN_TUT,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_COPS,				MP_LAUNCH_ALL_COPS)
	
//	Set_MP_Variation_Control(eCP_welcomeTut,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_COPS,				MP_LAUNCH_ALL_COPS)
	
	// Crook Mission
//	Set_MP_Variation_Control(eCR_Airport_Takeover,		ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_1GANG,				MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_ASIN,					ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1GANG,				MP_LAUNCH_ALL_GANGS)
	
	//Set_MP_Variation_Control(eCR_AttackBoat,			eVARIATION_CR_AttackBoat_CROOK_V_AI,			MP_LOC_FAIR_VAR,	MP_TEAMS_1GANG,				MP_LAUNCH_ONE_GANG)
	//Set_MP_Variation_Control(eCR_AttackBoat,			eVARIATION_CR_AttackBoat_CROOK_V_CROOK,			MP_LOC_FAIR_VAR,	MP_TEAMS_2GANGS,			MP_LAUNCH_ONE_GANG)
	
//	Set_MP_Variation_Control(eCR_BossMeet,				ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_2GANGS,			MP_LAUNCH_ONE_GANG)
	
//	Set_MP_Variation_Control(eCR_CarBlowUp,				ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_XYZ,	MP_TEAMS_1GANG,				MP_LAUNCH_ONE_GANG)
	
//	Set_MP_Variation_Control(eCR_CokeChase,				ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_1GANG,				MP_LAUNCH_ONE_GANG)
	
//	Set_MP_Variation_Control(eCR_Convoy_Steal,			ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_1GANG,				MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_DefendBase,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_2GANGS,			MP_LAUNCH_ONE_GANG)
	
//	Set_MP_Variation_Control(eCR_DisposeOfVehicle,		ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_escortCrimBoss,		ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_farmhouse,				ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_Funeral,				ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_2GANGS,			MP_LAUNCH_ONE_GANG)
	
//	Set_MP_Variation_Control(eCR_GET_VEHICLE,			ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_1GANG,				MP_LAUNCH_ONE_GANG)
	
//	Set_MP_Variation_Control(eCR_GOGET,					ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_2GANGS,			MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_hostage_steal,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ALL_COPS)
	
//	Set_MP_Variation_Control(eCR_Mayhem,				ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_1GANG,				MP_LAUNCH_ONE_GANG)
	
//	Set_MP_Variation_Control(eCR_MilitaryBase,			ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_1GANG,				MP_LAUNCH_ONE_GANG)
	
//	Set_MP_Variation_Control(eCR_PACKAGE_GRAB,			eVARIATION_CR_PACKAGE_GRAB_KEITH_TEMP,			MP_LOC_FAIR_XYZ,	MP_TEAMS_2TEAMS_MIN,		MP_LAUNCH_ALL_GANGS)
//	Set_MP_Variation_Control(eCR_PACKAGE_GRAB,			eVARIATION_CR_PACKAGE_GRAB_TUTORIAL,			MP_LOC_FAIR_XYZ,	MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ALL_GANGS)
//	Set_MP_Variation_Control(eCR_PACKAGE_GRAB,			eVARIATION_CR_PACKAGE_GRAB_MEX_COKE,			MP_LOC_FAIR_XYZ,	MP_TEAMS_2TEAMS_MIN,		MP_LAUNCH_ALL_GANGS)
//	Set_MP_Variation_Control(eCR_PACKAGE_GRAB,			eVARIATION_CR_PACKAGE_GRAB_LOST_METH,			MP_LOC_FAIR_XYZ,	MP_TEAMS_2TEAMS_MIN,		MP_LAUNCH_ALL_GANGS)
//	Set_MP_Variation_Control(eCR_PACKAGE_GRAB,			eVARIATION_CR_PACKAGE_GRAB_SAN_AND,				MP_LOC_FAIR_XYZ,	MP_TEAMS_2TEAMS_MIN,		MP_LAUNCH_ALL_GANGS)
//	Set_MP_Variation_Control(eCR_PACKAGE_GRAB,			eVARIATION_CR_PACKAGE_GRAB_ALAMO_SEA,			MP_LOC_FAIR_XYZ,	MP_TEAMS_2TEAMS_MIN,		MP_LAUNCH_ALL_GANGS)
//	Set_MP_Variation_Control(eCR_PACKAGE_GRAB,			eVARIATION_CR_PACKAGE_GRAB_SENORA_FREE,			MP_LOC_FAIR_XYZ,	MP_TEAMS_2TEAMS_MIN,		MP_LAUNCH_ALL_GANGS)
//	Set_MP_Variation_Control(eCR_PACKAGE_GRAB,			eVARIATION_CR_PACKAGE_GRAB_VINEWOOD_HILL,		MP_LOC_FAIR_XYZ,	MP_TEAMS_2TEAMS_MIN,		MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_parley,				ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_2GANGS,			MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_PLANE_DROP,			ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_PolBustOut,			ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_PRISON_BREAK,			ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_Races,					ALL_MISSION_VARIATIONS,							MP_LOC_RND_VAR,		MP_TEAMS_1GANG_MIN,			MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_SHIPMENT_STEAL_1,		ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_Showroom,				ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_1GANG,				MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_STEAL_BIKES,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_Steal_Vehicle,			ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_van_rescue,			ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ALL_GANGS)
	
	// Crook Heist
//	Set_MP_Variation_Control(eCR_BANK_HEIST_1,			ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ONE_GANG)
	
//	Set_MP_Variation_Control(eCR_COUNTRY_HEIST_1,		ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ONE_GANG)
	
//	Set_MP_Variation_Control(eCR_FBI_grab,				ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ONE_GANG)
	
//	Set_MP_Variation_Control(eCR_FBI_HEIST_1,			ALL_MISSION_VARIATIONS,							MP_LOC_FAIR_VAR,	MP_TEAMS_COPS_1GANG,		MP_LAUNCH_ONE_GANG)
	
	// Crook Tutorial
//	Set_MP_Variation_Control(eCR_DRUGS_TUT,				ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1GANG,				MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_GUN_SHOP_TUT,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1GANG,				MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_HOLD_UP_TUT,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1GANG,				MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_HOOKER_TUT,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1GANG,				MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_JOBLIST_TUT,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1GANG,				MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_PROPERTY_TUT,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1TEAM,				MP_LAUNCH_ONE_TEAM)
	
//	Set_MP_Variation_Control(eCR_RIVAL_TUT,				ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1GANG,				MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_SELL_CARS_TUT,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1GANG,				MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_SNITCH_TUT,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1GANG,				MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_TRIGGER_TUT,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1GANG,				MP_LAUNCH_ALL_GANGS)
	
//	Set_MP_Variation_Control(eCR_WELCOME,				ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1GANG,				MP_LAUNCH_ALL_GANGS)
	
	// Freemode Activities
	Set_MP_Variation_Control(eFM_ARM_WRESTLING,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1TEAM,				MP_LAUNCH_ALL_PLAYERS)

	Set_MP_Variation_Control(eFM_BASEJUMP_CLOUD,		ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1TEAM,				MP_LAUNCH_ALL_PLAYERS)

	Set_MP_Variation_Control(eFM_DARTS,					ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1TEAM,				MP_LAUNCH_ALL_PLAYERS)
	
	Set_MP_Variation_Control(eFM_PILOT_SCHOOL,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1TEAM,				MP_LAUNCH_ALL_PLAYERS)
	
	Set_MP_Variation_Control(eFM_IMPROMPTU_DM,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1TEAM,				MP_LAUNCH_ALL_PLAYERS)

	Set_MP_Variation_Control(eFM_DEATHMATCH_CLOUD,		ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1TEAM,				MP_LAUNCH_ALL_PLAYERS)

	Set_MP_Variation_Control(eFM_GANG_ATTACK_CLOUD,		ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1TEAM,				MP_LAUNCH_ALL_PLAYERS)

	Set_MP_Variation_Control(eFM_GOLF,					ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1TEAM,				MP_LAUNCH_ALL_PLAYERS)

	Set_MP_Variation_Control(eFM_MISSION_CLOUD,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1TEAM,				MP_LAUNCH_ALL_PLAYERS)

	Set_MP_Variation_Control(eFM_RACE_CLOUD,			ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1TEAM,				MP_LAUNCH_ALL_PLAYERS)

	Set_MP_Variation_Control(eFM_SHOOTING_RANGE,		ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1TEAM,				MP_LAUNCH_ALL_PLAYERS)

	Set_MP_Variation_Control(eFM_SURVIVAL_CLOUD,		ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1TEAM,				MP_LAUNCH_ALL_PLAYERS)

	Set_MP_Variation_Control(eFM_TENNIS,				ALL_MISSION_VARIATIONS,							MP_LOC_SPECIAL,		MP_TEAMS_1TEAM,				MP_LAUNCH_ALL_PLAYERS)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To set any optional flags required by a mission variation
// NOTES:	Store in alphabetical order by MissionID by type.
PROC Fill_MP_Mission_Info_Variations_Optional_Flags()

	PRINTSTRING("          Fill_MP_Mission_Info_Variations_Optional_Flags()") PRINTNL()
	
	// FIELD DESCRIPTIONS
	//	MissionID			:	The Mission ID ENUM
	//	Mission Variation	:	The specific mission variation this flag is being set for (or ALL_MISSION_VARIATIONS)
	//	Flag Being Set TRUE	:	The MI_FLAG ID being set to TRUE for this variation of this mission
	//	Optional Data		:	An optional piece of data associated with the flag (hopefully can always pass an INT, even if stored as something else)
	
	// Default Behaviours For A Mission
	//	DON'T USE SECONDARY COMMS (ie: use the same communication whether or not a Crook Gang is first to join a mission or second - Added for AttackBoat - if a Crook Gang is first on the mission they will get a different communication than if they are the second Crook Gang on the mission)
	//	DON'T USE INCLUSION RADIUS (ie: players from anywhere can be put on the mission - an inclusion radius only chooses players within a range of the mission vector - Added for ClaenArea: only players close to the safehouse should be put on the mission)
	//	RESERVED_BY_PLAYER DON'T NEED TO JOIN FIRST (ie: players that choose to join the mission join at the same time as players grabbed for the mission - Added for CleanArea: the first player on the mission must be close to the safehouse to take control of nearby car entities)


	//					  MissionID					Mission Variation							Flag Being Set To TRUE						Optional Data
	//					  ---------					-----------------							----------------------						-------------
	// Ambient - Crook
//	Set_MP_Variation_Flag(eAM_CR_CLEANAREA,			ALL_MISSION_VARIATIONS,						MI_FLAG_INCLUSION_RADIUS,					200)
//	Set_MP_Variation_Flag(eAM_CR_CLEANAREA,			ALL_MISSION_VARIATIONS,						MI_FLAG_RESERVED_BY_PLAYER_JOIN_FIRST)
	
	// Used as both Cop Mission and Crook Mission
	
	// Cop Mission
	
	// Cop Tutorial
	
	// Crook Mission
	//Set_MP_Variation_Flag(eCR_AttackBoat,			eVARIATION_CR_AttackBoat_CROOK_V_CROOK,		MI_FLAG_SECONDARY_COMMS)
	
	// Crook Heist
	
	// Crook Tutorial
	
	// Freemode Activities
	Set_MP_Variation_Flag(eFM_BASEJUMP_CLOUD,		ALL_MISSION_VARIATIONS,						MI_FLAG_DATA_ON_CLOUD)
	
	Set_MP_Variation_Flag(eFM_DEATHMATCH_CLOUD,		ALL_MISSION_VARIATIONS,						MI_FLAG_DATA_ON_CLOUD)
	
	Set_MP_Variation_Flag(eFM_GANG_ATTACK_CLOUD,	ALL_MISSION_VARIATIONS,						MI_FLAG_DATA_ON_CLOUD)
	Set_MP_Variation_Flag(eFM_GANG_ATTACK_CLOUD,	ALL_MISSION_VARIATIONS,						MI_FLAG_ALLOW_REJOIN_AFTER_LEAVING)
	
	Set_MP_Variation_Flag(eFM_MISSION_CLOUD,		ALL_MISSION_VARIATIONS,						MI_FLAG_DATA_ON_CLOUD)
	
	Set_MP_Variation_Flag(eFM_RACE_CLOUD,			ALL_MISSION_VARIATIONS,						MI_FLAG_DATA_ON_CLOUD)
	
	Set_MP_Variation_Flag(eFM_SHOOTING_RANGE,		ALL_MISSION_VARIATIONS,						MI_FLAG_INTERIOR_BLIP_ONLY)
	
	Set_MP_Variation_Flag(eFM_SURVIVAL_CLOUD,		ALL_MISSION_VARIATIONS,						MI_FLAG_DATA_ON_CLOUD)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To set any optional flags required by a team that is consistent across all variations of a mission
// NOTES:	Store in alphabetical order by MissionID by Team.
//			*** THESE FLAGS ARE THE SAME FOR ALL VARIATIONS OF A MISSION ***
PROC Fill_MP_Mission_Team_Data_Optional_Flags()

	PRINTSTRING("          Fill_MP_Mission_Team_Data_Optional_Flags()") PRINTNL()
	
	// FIELD DESCRIPTIONS
	//	MissionID			:	The Mission ID ENUM
	//	Team Storage		:	Uses the Team Requirements ENUM (NOT THE TEAM_COP, etc CONST_INTs) - this is because all Crook Team data for a mission is usually the same
	//	Flag Being Set TRUE	:	The MIT_FLAG ID being set to TRUE for this storage
	
	// Default Behaviours For A Team - Consistent across all variations of a mission
	//	NO PRE-MISSION TEXT MESSAGE
	//	NO PRE-MISSION PHONECALL
	//	NO MISSION DETAILS BOX
	//	NO INVITATION WHEN JOINING MISSION (ie: Players are Forced To Join Mission)
	//	ALL PLAYERS FROM TEAM CAN JOIN MISSION (optionally SPECIFIC PLAYERS ONLY can join the mission - usually those that have reserved themselves at a corona)
	
	// SPECIAL MENTIONS
	//	eAM_CR_CLEANAREA	:	Doesn't use pre-mission phonecall or txtmsg - the mission itself will send txtmsg when the mission launches for a player


	//													Team
	//						 MissionID					Storage			Flag Being Set To TRUE
	//						 ---------					-------			----------------------
	// Ambient - Crook
//	Set_MP_Mission_Team_Flag(eAM_CR_CLEANAREA,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
	
	// Used as both Cop Mission and Crook Mission
//	Set_MP_Mission_Team_Flag(eCOC_Do_Cutscene,			MIT_CROOKS,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCP_Takeback_Territory,	MIT_CROOKS,		MIT_FLAG_PREMISSION_PHONECALL)
//	Set_MP_Mission_Team_Flag(eCP_Takeback_Territory,	MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
	
	// Cop Mission
//	Set_MP_Mission_Team_Flag(eCP_InformHeli,			MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCP_InformHeli,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCP_InformHeli,			MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCP_PrisonTrans,			MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCP_PrisonTrans,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCP_PrisonTrans,			MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCP_ProWitCourt,			MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCP_ProWitCourt,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCP_ProWitCourt,			MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCP_vip_tour,				MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCP_vip_tour,				MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCP_vip_tour,				MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCP_weapons_ambush,		MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCP_weapons_ambush,		MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCP_weapons_ambush,		MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
	// Cop Tutorial
//	Set_MP_Mission_Team_Flag(eCP_ARMOURY_TUT,			MIT_COP,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCP_PARTNER_TUT,			MIT_COP,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCP_SNITCH_TUT,			MIT_COP,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCP_STOLEN_TUT,			MIT_COP,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCP_welcomeTut,			MIT_COP,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
	// Crook Mission
//	Set_MP_Mission_Team_Flag(eCR_Airport_Takeover,		MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_Airport_Takeover,		MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_Airport_Takeover,		MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_ASIN,					MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_ASIN,					MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_ASIN,					MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_AttackBoat,			MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_AttackBoat,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_AttackBoat,			MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_BossMeet,				MIT_CROOKS,		MIT_FLAG_PREMISSION_PHONECALL)
//	Set_MP_Mission_Team_Flag(eCR_BossMeet,				MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
	
//	Set_MP_Mission_Team_Flag(eCR_CarBlowUp,				MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_CarBlowUp,				MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_CarBlowUp,				MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_CokeChase,				MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_CokeChase,				MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_CokeChase,				MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_Convoy_Steal,			MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_Convoy_Steal,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_Convoy_Steal,			MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_DefendBase,			MIT_CROOKS,		MIT_FLAG_PREMISSION_PHONECALL)
//	Set_MP_Mission_Team_Flag(eCR_DefendBase,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
	
//	Set_MP_Mission_Team_Flag(eCR_DisposeOfVehicle,		MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_DisposeOfVehicle,		MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_DisposeOfVehicle,		MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_escortCrimBoss,		MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_escortCrimBoss,		MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_escortCrimBoss,		MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_farmhouse,				MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_farmhouse,				MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_farmhouse,				MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_Funeral,				MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_Funeral,				MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_Funeral,				MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_GET_VEHICLE,			MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_GET_VEHICLE,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_GET_VEHICLE,			MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_GOGET,					MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_GOGET,					MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_GOGET,					MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_hostage_steal,			MIT_CROOKS,		MIT_FLAG_PREMISSION_PHONECALL)
//	Set_MP_Mission_Team_Flag(eCR_hostage_steal,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
	
//	Set_MP_Mission_Team_Flag(eCR_Mayhem,				MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_Mayhem,				MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_Mayhem,				MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_MilitaryBase,			MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_MilitaryBase,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_MilitaryBase,			MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_oceandrop,				MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_oceandrop,				MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_oceandrop,				MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_PACKAGE_GRAB,			MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_PACKAGE_GRAB,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_PACKAGE_GRAB,			MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_parley,				MIT_CROOKS,		MIT_FLAG_PREMISSION_PHONECALL)
//	Set_MP_Mission_Team_Flag(eCR_parley,				MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
	
//	Set_MP_Mission_Team_Flag(eCR_PLANE_DROP,			MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_PLANE_DROP,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_PLANE_DROP,			MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_PolBustOut,			MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_PolBustOut,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_PolBustOut,			MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_PRISON_BREAK,			MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_PRISON_BREAK,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_PRISON_BREAK,			MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_Races,					MIT_CROOKS,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCR_SHIPMENT_STEAL_1,		MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_SHIPMENT_STEAL_1,		MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_SHIPMENT_STEAL_1,		MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_Showroom,				MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_Showroom,				MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_Showroom,				MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_STEAL_BIKES,			MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_STEAL_BIKES,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_STEAL_BIKES,			MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_Steal_Vehicle,			MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_Steal_Vehicle,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_Steal_Vehicle,			MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
//	Set_MP_Mission_Team_Flag(eCR_van_rescue,			MIT_CROOKS,		MIT_FLAG_PREMISSION_TXTMSG)
//	Set_MP_Mission_Team_Flag(eCR_van_rescue,			MIT_CROOKS,		MIT_FLAG_MISSION_DETAILS_BOX)
//	Set_MP_Mission_Team_Flag(eCR_van_rescue,			MIT_CROOKS,		MIT_FLAG_INVITE_ONTO_MISSION)
	
	// Crook Heist
//	Set_MP_Mission_Team_Flag(eCR_BANK_HEIST_1,			MIT_CROOKS,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCR_COUNTRY_HEIST_1,		MIT_CROOKS,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCR_FBI_grab,				MIT_CROOKS,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCR_FBI_HEIST_1,			MIT_CROOKS,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
	// Crook Tutorial
//	Set_MP_Mission_Team_Flag(eCR_DRUGS_TUT,				MIT_CROOKS,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCR_GUN_SHOP_TUT,			MIT_CROOKS,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCR_HOLD_UP_TUT,			MIT_CROOKS,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCR_HOOKER_TUT,			MIT_CROOKS,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCR_JOBLIST_TUT,			MIT_CROOKS,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCR_RIVAL_TUT,				MIT_CROOKS,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCR_SELL_CARS_TUT,			MIT_CROOKS,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCR_SNITCH_TUT,			MIT_CROOKS,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
//	Set_MP_Mission_Team_Flag(eCR_TRIGGER_TUT,			MIT_CROOKS,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)

//	Set_MP_Mission_Team_Flag(eCR_WELCOME,				MIT_CROOKS,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
	// Freemode Activities
	Set_MP_Mission_Team_Flag(eFM_ARM_WRESTLING,			MIT_ALL,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)

	Set_MP_Mission_Team_Flag(eFM_BASEJUMP_CLOUD,		MIT_ALL,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)

	Set_MP_Mission_Team_Flag(eFM_DARTS,					MIT_ALL,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
	Set_MP_Mission_Team_Flag(eFM_PILOT_SCHOOL,			MIT_ALL,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)
	
	Set_MP_Mission_Team_Flag(eFM_IMPROMPTU_DM,			MIT_ALL,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)

	Set_MP_Mission_Team_Flag(eFM_DEATHMATCH_CLOUD,		MIT_ALL,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)

	Set_MP_Mission_Team_Flag(eFM_GANG_ATTACK_CLOUD,		MIT_ALL,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)

	Set_MP_Mission_Team_Flag(eFM_GOLF,					MIT_ALL,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)

	Set_MP_Mission_Team_Flag(eFM_MISSION_CLOUD,			MIT_ALL,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)

	Set_MP_Mission_Team_Flag(eFM_RACE_CLOUD,			MIT_ALL,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)

	Set_MP_Mission_Team_Flag(eFM_SHOOTING_RANGE,		MIT_ALL,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)

	Set_MP_Mission_Team_Flag(eFM_SURVIVAL_CLOUD,		MIT_ALL,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)

	Set_MP_Mission_Team_Flag(eFM_TENNIS,				MIT_ALL,		MIT_FLAG_SPECIFIC_PLAYERS_ONLY)

ENDPROC


// -----------------------------------------------------------------------------------------------------------

// PURPOSE:	To fill the MP Mission Info arrays.
PROC Fill_MP_Mission_Info()

	// Mission Info
	// ...basic information for the mission
	Fill_MP_Mission_Info_Basics()
	
	// Mission Variations
	// ...standard control data needed for each variation
	Fill_MP_Mission_Info_Variations_Standard_Data()
	
	// ...optional flags (with optional associated data) being set for each variation
	Fill_MP_Mission_Info_Variations_Optional_Flags()
	
	// Team Details
	// ...optional flags being set for team data that is consistent across all variations of a mission
	Fill_MP_Mission_Team_Data_Optional_Flags()
	
	// ...(TO DO: setup Variation Team Data)
	PRINTSTRING("          TEMP: Variation Team Data setup function") PRINTNL()

	PRINTSTRING("          *** All MP Mission Info Data Setup ***") PRINTNL()
	
	// Output some data usage statistics
	#IF IS_DEBUG_BUILD
		// ...mission info
		PRINTSTRING("              MISSION INFO STRUCT: [NUMBER OF: ")
		PRINTINT(ENUM_TO_INT(MP_MISSIONS_ARRAY_MAX))
		PRINTSTRING("]  [Bytes Per Mission: ")
		PRINTINT((SIZE_OF(g_structMissionInfo) * 4))
		PRINTSTRING("]  [Total Bytes in use: ")
		PRINTINT((ENUM_TO_INT(eMAX_NUM_MP_MISSION) * SIZE_OF(g_structMissionInfo) * 4))
		PRINTSTRING(" from ")
		PRINTINT((SIZE_OF(g_sMissionInfo) * 4))
		PRINTSTRING("]")
		PRINTNL()

		// ...mission team data
		PRINTSTRING("              MISSION TEAM DATA STRUCT: [NUMBER OF: ")
		PRINTINT(g_numTeamInfoPerMissionInUse)
		PRINTSTRING(" from ")
		PRINTINT(MAX_MI_TEAMS)
		PRINTSTRING("]  [Bytes Per Team: ")
		PRINTINT((SIZE_OF(g_structTeamInfoPerMission) * 4))
		PRINTSTRING("]  [Total Bytes In Use: ")
		PRINTINT((g_numTeamInfoPerMissionInUse * SIZE_OF(g_structTeamInfoPerMission) * 4))
		PRINTSTRING(" from ")
		PRINTINT((SIZE_OF(g_sTeamInfoPerMission) * 4))
		PRINTSTRING("]")
		PRINTNL()

		// ...mission info variations
		PRINTSTRING("              MISSION INFO VARIATIONS STRUCT: [NUMBER OF: ")
		PRINTINT(g_numVariationsInUse)
		PRINTSTRING(" from ")
		PRINTINT(MAX_MP_MISSION_VARIATIONS)
		PRINTSTRING("]  [Bytes Per Variation: ")
		PRINTINT((SIZE_OF(g_structMissionInfoForVariations) * 4))
		PRINTSTRING("]  [Total Bytes In Use: ")
		PRINTINT((g_numVariationsInUse * SIZE_OF(g_structMissionInfoForVariations) * 4))
		PRINTSTRING(" from ")
		PRINTINT((SIZE_OF(g_sMissionVariations) * 4))
		PRINTSTRING("]")
		PRINTNL()

		// ...variations team data
		PRINTSTRING("              VARIATIONS TEAM DATA STRUCT: [NUMBER OF: ")
		PRINTINT(g_numTeamInfoPerVariationInUse)
		PRINTSTRING(" from ")
		PRINTINT(MAX_MI_VAR_TEAMS)
		PRINTSTRING("]  [Bytes Per Team: ")
		PRINTINT((SIZE_OF(g_structTeamInfoPerVariation) * 4))
		PRINTSTRING("]  [Total Bytes In Use: ")
		PRINTINT((g_numTeamInfoPerVariationInUse * SIZE_OF(g_structTeamInfoPerVariation) * 4))
		PRINTSTRING(" from ")
		PRINTINT((SIZE_OF(g_sTeamInfoPerVariation) * 4))
		PRINTSTRING("]")
		PRINTNL()
		
		// ...overall totals
		PRINTSTRING("              OVERALL TOTAL BYTES ALLOCATED FOR MISSION INFO DATA: ")
		PRINTINT((SIZE_OF(g_sMissionInfo) * 4) + (SIZE_OF(g_sMissionVariations) * 4) + (SIZE_OF(g_sTeamInfoPerMission) * 4) + (SIZE_OF(g_sTeamInfoPerVariation) * 4))
		PRINTNL()
	#ENDIF
	
	// TO DO - Check that all the data is setup
	#IF IS_DEBUG_BUILD
	// ........Special debug only check - ensure pre-mission phonecall and pre-mission txtmsg aren't both set for the same team for the same mission
	#ENDIF
	
// KGM 9/2/13: Prevent console log output when joining game
//	#IF IS_DEBUG_BUILD
//		// TEMP: Output all arrays
//		INT tempLoop = 0
//		PRINTNL()
//		PRINTSTRING("...KGM MP [MissionInfo]: Mission Info Array") PRINTNL()
//		REPEAT eMAX_NUM_MP_MISSION tempLoop
//			IF NOT (g_sMissionInfo[tempLoop].numVariations = UNDEFINED_MISSION_INFO)
//				PRINTSTRING("         ")
//				PRINTINT(tempLoop)
//				IF (g_sMissionInfo[tempLoop].playersAreInTeams)
//					PRINTSTRING("  [IN TEAMS]")
//				ELSE
//					PRINTSTRING("            ")
//				ENDIF
//				PRINTSTRING("   NumVars: ") PRINTINT(g_sMissionInfo[tempLoop].numVariations)
//				PRINTSTRING("   FirstVar: ") PRINTINT(g_sMissionInfo[tempLoop].arrayPosFirstVar)
//				PRINTSTRING("   LastVar: ") PRINTINT((g_sMissionInfo[tempLoop].arrayPosFirstVar + g_sMissionInfo[tempLoop].numVariations - 1))
//				PRINTSTRING("   Team Data: ") PRINTINT(Get_Number_Of_Elements_Of_Team_Storage(g_sMissionInfo[tempLoop].teamStorage))
//				PRINTSTRING("   FirstTeam: ") PRINTINT(g_sMissionInfo[tempLoop].arrayPosFirstTeam)
//				PRINTSTRING("   LastTeam: ") PRINTINT((g_sMissionInfo[tempLoop].arrayPosFirstTeam + Get_Number_Of_Elements_Of_Team_Storage(g_sMissionInfo[tempLoop].teamStorage) - 1))
//				// Errors
//				IF (g_sMissionInfo[tempLoop].instances = NO_MP_INST)
//					PRINTSTRING(" [UNDEFINED INSTANCES]")
//				ENDIF
//				PRINTNL()
//			ENDIF
//		ENDREPEAT
//
//		PRINTNL()
//		PRINTSTRING("...KGM MP [MissionInfo]: Mission Team Data Array") PRINTNL()
//		REPEAT g_numTeamInfoPerMissionInUse tempLoop
//			PRINTSTRING("         ")
//			PRINTINT(tempLoop)
//			IF (IS_BIT_SET(g_sTeamInfoPerMission[tempLoop].bitsTeamMissionFlags, MIT_FLAG_PREMISSION_TXTMSG))
//				PRINTSTRING(" [INTRO TXTMSG]")
//			ENDIF
//			IF (IS_BIT_SET(g_sTeamInfoPerMission[tempLoop].bitsTeamMissionFlags, MIT_FLAG_PREMISSION_PHONECALL))
//				PRINTSTRING(" [INTRO PHONECALL]")
//			ENDIF
//			IF (IS_BIT_SET(g_sTeamInfoPerMission[tempLoop].bitsTeamMissionFlags, MIT_FLAG_MISSION_DETAILS_BOX))
//				PRINTSTRING(" [DETAILS BOX]")
//			ENDIF
//			IF (IS_BIT_SET(g_sTeamInfoPerMission[tempLoop].bitsTeamMissionFlags, MIT_FLAG_INVITE_ONTO_MISSION))
//				PRINTSTRING(" [INVITATION]")
//			ENDIF
//			IF (IS_BIT_SET(g_sTeamInfoPerMission[tempLoop].bitsTeamMissionFlags, MIT_FLAG_SPECIFIC_PLAYERS_ONLY))
//				PRINTSTRING(" [SPECIFIC PLAYERS]")
//			ENDIF
//			PRINTNL()
//		ENDREPEAT
//
//		PRINTNL()
//		PRINTSTRING("...KGM MP [MissionInfo]: Mission Variations Array") PRINTNL()
//		REPEAT g_numVariationsInUse tempLoop
//			PRINTSTRING("         ")
//			PRINTINT(tempLoop)
//			PRINTSTRING("   FirstTeam: ") PRINTINT(g_sMissionVariations[tempLoop].arrayPosFirstVarTeam)
//			IF (IS_BIT_SET(g_sMissionVariations[tempLoop].bitsOptionalFlags, MI_FLAG_SECONDARY_COMMS))
//				PRINTSTRING(" [SECONDARY COMMS]")
//			ENDIF
//			IF (IS_BIT_SET(g_sMissionVariations[tempLoop].bitsOptionalFlags, MI_FLAG_INCLUSION_RADIUS))
//				PRINTSTRING(" [INCLUSION RADIUS: ")
//				PRINTFLOAT(g_sMissionVariations[tempLoop].optionalRadius)
//				PRINTSTRING("]")
//			ENDIF
//			IF (IS_BIT_SET(g_sMissionVariations[tempLoop].bitsOptionalFlags, MI_FLAG_RESERVED_BY_PLAYER_JOIN_FIRST))
//				PRINTSTRING(" [RESERVED BY PLAYER JOINS FIRST]")
//			ENDIF
//			IF (IS_BIT_SET(g_sMissionVariations[tempLoop].bitsOptionalFlags, MI_FLAG_DATA_ON_CLOUD))
//				PRINTSTRING(" [DATA ON CLOUD]")
//			ENDIF
//			IF (IS_BIT_SET(g_sMissionVariations[tempLoop].bitsOptionalFlags, MI_FLAG_ALLOW_REJOIN_AFTER_LEAVING))
//				PRINTSTRING(" [ALLOW REJOIN]")
//			ENDIF
//			IF (IS_BIT_SET(g_sMissionVariations[tempLoop].bitsOptionalFlags, MI_FLAG_INTERIOR_BLIP_ONLY))
//				PRINTSTRING(" [INTERIOR BLIP ONLY]")
//			ENDIF
//			// Errors
//			IF (g_sMissionVariations[tempLoop].locMethod = NO_MP_LOC)
//				PRINTSTRING(" [UNDEFINED LOCATION METHOD]")
//			ENDIF
//			IF (g_sMissionVariations[tempLoop].teamConfig = NO_MP_TEAM_CONFIG)
//				PRINTSTRING(" [UNDEFINED TEAM CONFIG]")
//			ENDIF
//			IF (g_sMissionVariations[tempLoop].launchTeams = NO_MP_LAUNCH_TEAMS)
//				PRINTSTRING(" [UNDEFINED LAUNCH TEAMS]")
//			ENDIF
//			PRINTNL()
//		ENDREPEAT
//
//		PRINTNL()
//		PRINTSTRING("...KGM MP [MissionInfo]: Variations Team Data Array") PRINTNL()
//		REPEAT g_numTeamInfoPerVariationInUse tempLoop
//			PRINTSTRING("         ")
//			PRINTINT(tempLoop)
//			PRINTNL()
//		ENDREPEAT
//	#ENDIF

ENDPROC




// ===========================================================================================================
//      Mission Data Initialisation - Public Functions
// ===========================================================================================================

// PURPOSE:	To setup all the MP Mission Data in globals.
// NOTES:	This will be called from all modes and mission data for all modes will be stored in the one array
PROC Initialise_MP_Mission_Info()

	// Using PrintStrings to avoid including other script headers
	PRINTNL()
	PRINTSTRING("...KGM MP [MissionInfo]: Initialise_MP_Mission_Info() called") PRINTNL()
	
	// Ensure the current number of missions doesn't break the mission array maximum
	INT numMissions	= ENUM_TO_INT(eMAX_NUM_MP_MISSION)
	IF (numMissions >= MP_MISSIONS_ARRAY_MAX)
		#IF IS_DEBUG_BUILD
			PRINTSTRING("...KGM MP [MissionInfo]: The number of missions (eMAX_NUM_MP_MISSION) is larger than the array size for missions (MP_MISSIONS_ARRAY_MAX)") PRINTNL()
			PRINTSTRING("      eMAX_NUM_MP_MISSION: ")		PRINTINT(numMissions) 			PRINTNL()
			PRINTSTRING("      MP_MISSIONS_ARRAY_MAX: ")	PRINTINT(MP_MISSIONS_ARRAY_MAX)	PRINTNL()
			SCRIPT_ASSERT("Initialise_MP_Mission_Info() - FATAL ERROR: THERE ARE TOO MANY MISSIONS - none are being setup. See Console log. Tell Keith.")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Clear the Mission Info arrays
	Reset_MP_Mission_Info()
	
	// Fill the Mission Info arrays
	Fill_MP_Mission_Info()

ENDPROC





