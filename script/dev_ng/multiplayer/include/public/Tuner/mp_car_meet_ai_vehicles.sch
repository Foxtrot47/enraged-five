//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        mp_car_meet_ai_vehicles.sch																			//
// Description: This is implementation of the car meet ai vehicle spawner . Consists of both private and public API //
//				for other scripts to use.																			//
// Written by:  Jan Mencfel																							//
// Date:  		30/03/2021																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    

USING "globals.sch"
USING "mp_globals.sch"
USING "script_debug.sch"
USING "net_include.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "rc_helper_functions.sch"
USING "net_realty_new.sch"
USING "net_simple_cutscene.sch"
USING "Cutscene_help.sch"
USING "net_simple_interior_cs_header_include.sch"
USING "net_collectables_pickups.sch"
USING "net_simple_interior_car_meet_property.sch"

BOOL b_HAVE_ALL_AI_VEHICLES_BEEN_CREATED = FALSE
BOOL b_HALE_ALL_AL_VEHICLES_BEEN_CULLED = TRUE
BOOL b_SHOULD_CHECK_FOR_VISIBILITTY = FALSE

// ***************************************************************************************
// 					@CONSTS@
// ***************************************************************************************

CONST_INT NUM_OF_UNIQUE_VEHICLE_MODELS      	10
CONST_INT NUM_OF_VEHICLE_VARIANTS          		 5
CONST_INT NUM_VEHICLES_WITH_OPEN_HOOD			 9
CONST_INT NUM_VEHICLES_WITH_BOOT_SPEAKERS		 5
CONST_INT NUM_VEHICLES_WITH_HEADLIGHTS_ON		 8
CONST_INT NUM_VEHICLE_CULLING_LEVELS			 3
CONST_INT NUM_FRAMES_TO_WAIT_TO_COUNT_PLAYERS	300


CONST_INT VEHICLE_INDEX_WITH_OPEN_HOOD_0 		 0
CONST_INT VEHICLE_INDEX_WITH_OPEN_HOOD_1 		 4
CONST_INT VEHICLE_INDEX_WITH_OPEN_HOOD_2		 7
CONST_INT VEHICLE_INDEX_WITH_OPEN_HOOD_3		12
CONST_INT VEHICLE_INDEX_WITH_OPEN_HOOD_4 		13
CONST_INT VEHICLE_INDEX_WITH_OPEN_HOOD_5 		15
CONST_INT VEHICLE_INDEX_WITH_OPEN_HOOD_6 		35
CONST_INT VEHICLE_INDEX_WITH_OPEN_HOOD_7 		39
CONST_INT VEHICLE_INDEX_WITH_OPEN_HOOD_8 		48

CONST_INT VEHICLE_INDEX_WITH_OPEN_BOOT_0 		18
CONST_INT VEHICLE_INDEX_WITH_OPEN_BOOT_1 		20
CONST_INT VEHICLE_INDEX_WITH_OPEN_BOOT_2		26
CONST_INT VEHICLE_INDEX_WITH_OPEN_BOOT_3		41
CONST_INT VEHICLE_INDEX_WITH_OPEN_BOOT_4 		42

//BITSET
CONST_INT BS_AI_VEHICLES_MOODYMAN_PRESENT 					1
CONST_INT BS_AI_VEHICLES_MOODYMAN_TRUNK_PROPS_SPAWNED		2
CONST_INT BS_AI_VEHICLES_MOODYMAN_TRUNK_HELP_DISPLAYED		3
CONST_INT BS_AI_VEHICLES_MOODYMAN_TRUNK_COLLECTION_STARTED	4
CONST_INT BS_AI_VEHICLES_BLOCK_FROM_RESPAWNING				5
CONST_INT BS_AI_VEHICLES_HYDRAULICS_VEHICLE_PRESENT 		6	

// ***************************************************************************************
// 					@ENUMS@
// ***************************************************************************************
ENUM CAR_MEET_AI_VEHICLE_LIST_CREATION_STATE
	CAR_MEET_AI_VEHICLE_LIST_NOT_STARTED,
	CAR_MEET_AI_VEHICLE_LIST_LOADING,
	CAR_MEET_AI_VEHICLE_LIST_DONE
ENDENUM
// ***************************************************************************************
// 					@LOOKUP_TABLES@
// ***************************************************************************************
FUNC STRING CAR_MEET_VEHICLE_ENUM_TO_STRING(CAR_MEET_AI_VEHICLE_ENUM vehicle)
	SWITCH vehicle
		CASE CM_VEH_ASBO           		   RETURN "ASBO"
		CASE CM_VEH_SULTAN2                RETURN "SULTAN2"
		CASE CM_VEH_BANSHEE2               RETURN "BANSHEE2"
		CASE CM_VEH_SULTANRS               RETURN "SULTANRS"
		CASE CM_VEH_JESTER3                RETURN "JESTER3"
		CASE CM_VEH_BUCCANEER2             RETURN "BUCCANEER2"
		CASE CM_VEH_PEYOTE2                RETURN "PEYOTE2"
		CASE CM_VEH_CHEBUREK               RETURN "CHEBUREK"
		CASE CM_VEH_CHEETAH2               RETURN "CHEETAH2"
		CASE CM_VEH_CLUB                   RETURN "CLUB"
		CASE CM_VEH_COMET5                 RETURN "COMET5"
		CASE CM_VEH_COQUETTE4              RETURN "COQUETTE4"
		CASE CM_VEH_DEVIANT                RETURN "DEVIANT"
		CASE CM_VEH_DOMINATOR3             RETURN "DOMINATOR3"
		CASE CM_VEH_ELEGY                  RETURN "ELEGY"
		CASE CM_VEH_ELLIE                  RETURN "ELLIE"
		CASE CM_VEH_HUSTLER                RETURN "HUSTLER"
		CASE CM_VEH_FACTION3               RETURN "FACTION3"
		CASE CM_VEH_KANJO                  RETURN "KANJO"
		CASE CM_VEH_KOMODA                 RETURN "KOMODA"
		CASE CM_VEH_GAUNTLET5              RETURN "GAUNTLET5"
		CASE CM_VEH_GB200                  RETURN "GB200"
		CASE CM_VEH_ELEGY2                 RETURN "ELEGY2"
		CASE CM_VEH_ISSI7                  RETURN "ISSI7"
		CASE CM_VEH_ITALIGTO               RETURN "ITALIGTO"
		CASE CM_VEH_JESTER                 RETURN "JESTER"
		CASE CM_VEH_HELLION                RETURN "HELLION"
		CASE CM_VEH_LYNX                   RETURN "LYNX"
		CASE CM_VEH_MAMBA                  RETURN "MAMBA"
		CASE CM_VEH_MANANA2                RETURN "MANANA2"
		CASE CM_VEH_MICHELLI               RETURN "MICHELLI"
		CASE CM_VEH_MOONBEAM2              RETURN "MOONBEAM2"
		CASE CM_VEH_NIGHTSHADE             RETURN "NIGHTSHADE"
		CASE CM_VEH_PENUMBRA2              RETURN "PENUMBRA2"
		CASE CM_VEH_RAPIDGT3               RETURN "RAPIDGT3"
		CASE CM_VEH_RETINUE2               RETURN "RETINUE2"
		CASE CM_VEH_SABREGT2               RETURN "SABREGT2"
		CASE CM_VEH_SAVESTRA               RETURN "SAVESTRA"
		CASE CM_VEH_SCHLAGEN               RETURN "SCHLAGEN"
		CASE CM_VEH_SENTINEL3              RETURN "SENTINEL3"
		CASE CM_VEH_SLAMVAN3               RETURN "SLAMVAN3"
		CASE CM_VEH_SPECTER2               RETURN "SPECTER2"
		CASE CM_VEH_SUGOI                  RETURN "SUGOI"
		CASE CM_VEH_INFERNUS2              RETURN "INFERNUS2"
		CASE CM_VEH_TORERO                 RETURN "TORERO"
		CASE CM_VEH_YOSEMITE3              RETURN "YOSEMITE3"
		CASE CM_VEH_TURISMO2               RETURN "TURISMO2"
		CASE CM_VEH_VERLIERER2             RETURN "VERLIERER2"
		CASE CM_VEH_VISERIS                RETURN "VISERIS"
		CASE CM_VEH_FLASHGT                RETURN "FLASHGT"
		CASE CM_VEH_COUNT				   RETURN "SLAMTRUCK"
		CASE CM_VEH_COUNT2				   RETURN "SANDKING"
	ENDSWITCH
	RETURN ""
ENDFUNC

FUNC BOOL DOES_VEHICLE_HAVE_HYDRAULICS(CAR_MEET_AI_VEHICLE_ENUM vehicle)
	SWITCH vehicle
		CASE CM_VEH_BUCCANEER2
		CASE CM_VEH_FACTION3
		CASE CM_VEH_MANANA2
		CASE CM_VEH_MOONBEAM2
		CASE CM_VEH_SABREGT2
		CASE CM_VEH_SLAMVAN3
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//ALSO ADDING MIMI'S CAR TOO
PROC GET_AMBIENT_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_COLOURS(VEHICLE_SETUP_STRUCT_MP &sData, INT iVehicleIndex)
	// IF IS PLAYER IN CAR MEET INTERIOR
	SWITCH iVehicleIndex
		CASE 0
			sData.VehicleSetup.iColour1 								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_COLOUR_1
			sData.VehicleSetup.iColour2 								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_COLOUR_2
			sData.VehicleSetup.iColourExtra1 							= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_EXTRA_COLOUR_1
			sData.VehicleSetup.iColourExtra2 							= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_EXTRA_COLOUR_2
			sData.iColour5 												= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_COLOUR_5
			sData.iColour6 												= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_COLOUR_6
			sData.VehicleSetup.iModIndex[MOD_LIVERY] 					= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_LIVERY
			sData.VehicleSetup.iWheelType								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_WHEEL_TYPE
			sData.VehicleSetup.iModIndex[MOD_WHEELS]					= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_WHEEL_ID
			sData.VehicleSetup.iWindowTintColour						= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_WINDOW_TINT_COLOUR
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] 		= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_LIGHTS_COLOUR
		BREAK
		CASE 1
			sData.VehicleSetup.iColour1 								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_COLOUR_1
			sData.VehicleSetup.iColour2 								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_COLOUR_2
			sData.VehicleSetup.iColourExtra1 							= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_EXTRA_COLOUR_1
			sData.VehicleSetup.iColourExtra2 							= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_EXTRA_COLOUR_2
			sData.iColour5 												= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_COLOUR_5
			sData.iColour6 												= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_COLOUR_6
			sData.VehicleSetup.iModIndex[MOD_LIVERY] 					= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_LIVERY
			sData.VehicleSetup.iWheelType								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_WHEEL_TYPE
			sData.VehicleSetup.iModIndex[MOD_WHEELS]					= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_WHEEL_ID
			sData.VehicleSetup.iWindowTintColour						= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_WINDOW_TINT_COLOUR
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] 		= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_LIGHTS_COLOUR
		BREAK
		CASE 2
			sData.VehicleSetup.iColour1 								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_COLOUR_1
			sData.VehicleSetup.iColour2 								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_COLOUR_2
			sData.VehicleSetup.iColourExtra1 							= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_EXTRA_COLOUR_1
			sData.VehicleSetup.iColourExtra2 							= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_EXTRA_COLOUR_2
			sData.iColour5 												= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_COLOUR_5
			sData.iColour6 												= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_COLOUR_6
			sData.VehicleSetup.iModIndex[MOD_LIVERY] 					= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_LIVERY
			sData.VehicleSetup.iWheelType								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_WHEEL_TYPE
			sData.VehicleSetup.iModIndex[MOD_WHEELS]					= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_WHEEL_ID
			sData.VehicleSetup.iWindowTintColour						= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_WINDOW_TINT_COLOUR
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] 		= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_LIGHTS_COLOUR
		BREAK
		CASE 3
			sData.VehicleSetup.eModel = minivan2 // MINIVAN2
			sData.VehicleSetup.tlPlateText = "68HYO893"
			sData.VehicleSetup.iColour1 = 145
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 70
			sData.VehicleSetup.iColourExtra2 = 145
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 8
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 67
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 3
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 1
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 2
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 12
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 22
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 3
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
		BREAK
		DEFAULT
			ASSERTLN("[TEST_DRIVE_SANDBOX][GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_COORDS] Invalid vehicle Index. iVehicleIndex = ", iVehicleIndex)
		BREAK
	ENDSWITCH
	// ENDIF
ENDPROC

PROC GET_CAR_MEET_CUTSCENE_VEHICLE_DATA(VEHICLE_SETUP_STRUCT_MP &sData, INT iVehicleIndex)
	SWITCH iVehicleIndex
		CASE 0  //===JESTER4 = P8_Car_1===
			sData.VehicleSetup.eModel = JESTER4
			sData.VehicleSetup.iColour1 = 112
			sData.VehicleSetup.iColour2 = 99
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 99
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 12
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 9
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 6
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 6
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 24
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 1
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 2
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 3
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
		BREAK
		CASE 1 //===ZR350 = P8_Car_2===
			sData.VehicleSetup.eModel = ZR350
			sData.VehicleSetup.iColour1 = 73
			sData.VehicleSetup.iColour2 = 27
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 12
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 13
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 6
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 7
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 9
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 5
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 5
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 3
			sData.VehicleSetup.iModIndex[MOD_DOOR_R] = 2
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 15
		BREAK
		CASE 2 //===TAMPA2 = P9_Car===
			sData.VehicleSetup.eModel = TAMPA2
			sData.VehicleSetup.iColour1 = 111
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
		BREAK
		CASE 3//===COQUETTE4 = P5_Car===
			sData.VehicleSetup.eModel = COQUETTE4
			sData.VehicleSetup.iColour1 = 73
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 74
			sData.VehicleSetup.iColourExtra2 = 1
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 2
			sData.VehicleSetup.iNeonG = 21
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		DEFAULT
			ASSERTLN("[MP_CAR_MEET_AI_VEHICLES][GET_CAR_MEET_CUTSCENE_VEHICLE_DATA] Invalid vehicle Index. iVehicleIndex = ", iVehicleIndex)
		BREAK
	ENDSWITCH
ENDPROC
FUNC VECTOR GET_AMBIENT_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_COORDS(INT iVehicleIndex)
	// IF IS PLAYER IN CAR MEET INTERIOR
	
		SWITCH iVehicleIndex
			CASE 0		RETURN <<-2143.9045, 1114.7545, 30.7814>>
			CASE 1		RETURN <<-2144.2776, 1119.2275, 30.7000>>
			CASE 2		RETURN <<-2144.2976, 1123.8685, 30.5620>>
			CASE 3		RETURN <<-2178.1646, 1107.6230, 24.6364>>
		ENDSWITCH	
	ASSERTLN("[MP_CAR_MEET_AI_VEHICLES][GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_COORDS] Invalid vehicle Index. iVehicleIndex = ", iVehicleIndex)
	RETURN <<0, 0, 0>>
ENDFUNC
FUNC FLOAT GET_AMBIENT_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_HEADING(INT iVehicleIndex)
	
	SWITCH iVehicleIndex
		CASE 0		RETURN 116.6498
		CASE 1		RETURN  89.9025
		CASE 2		RETURN 61.1236
		CASE 3		RETURN 116.679
	ENDSWITCH

	RETURN 0.0
ENDFUNC
FUNC MODEL_NAMES GET_AMBIENT_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_MODEL(INT iVehicleIndex)
	MODEL_NAMES eReturnModel = DUMMY_MODEL_FOR_SCRIPT
	
	SWITCH iVehicleIndex
		CASE 0		eReturnModel = INT_TO_ENUM(MODEL_NAMES, g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_MODEL_HASH)	BREAK
		CASE 1		eReturnModel = INT_TO_ENUM(MODEL_NAMES, g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_MODEL_HASH)	BREAK
		CASE 2		eReturnModel = INT_TO_ENUM(MODEL_NAMES, g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_MODEL_HASH)	BREAK
		CASE 3		eReturnModel = minivan2																			BREAK
	ENDSWITCH
	
	IF eReturnModel != DUMMY_MODEL_FOR_SCRIPT
		IF IS_MODEL_VALID(eReturnModel)
			PRINTLN("[MP_CAR_MEET_AI_VEHICLES][GET_AMBIENT_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_MODEL] Returning: ", GET_MODEL_NAME_FOR_DEBUG(eReturnModel))
			RETURN eReturnModel
		ELSE
			ASSERTLN("[MP_CAR_MEET_AI_VEHICLES][GET_AMBIENT_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_MODEL] Invalid Model!")
		ENDIF
	ELSE
		ASSERTLN("[MP_CAR_MEET_AI_VEHICLES][GET_AMBIENT_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_MODEL] Invalid vehicle Index. iVehicleIndex = ", iVehicleIndex)
	ENDIF
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC
FUNC BOOL DOES_VEHICLE_HAVE_UNCONVENTIONAL_HEADLIGHTS(CAR_MEET_AI_VEHICLE_ENUM vehicle)
	SWITCH vehicle
		CASE CM_VEH_ELEGY
		CASE CM_VEH_GAUNTLET5
		CASE CM_VEH_TORERO
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC
FUNC INT GET_NUMBER_OF_VEHICLES_TO_BE_HIDDEN()
	RETURN 4
ENDFUNC
FUNC INT GET_VEHCILE_INDEX_TO_BE_HIDDEN_IN_CUTSCENE(int i)
	SWITCH i
		CASE 0 RETURN 4
		CASE 1 RETURN 12 
		CASE 2 RETURN 13
		CASE 3 RETURN 45		
	ENDSWITCH
	RETURN -1
ENDFUNC
FUNC BOOL SHOULD_VEHICLE_BE_HIDDEN_IN_CUTSCENE(int index)
	SWITCH index
		CASE 4 		RETURN TRUE 
		CASE 12 	RETURN TRUE 
		CASE 13 	RETURN TRUE 
		CASE 45 	RETURN TRUE 
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_VEHICLE_BE_ROTATED_BY_180(INT index)
	SWITCH index
		CASE 1
		CASE 2
		CASE 5
		CASE 9
		CASE 10
		CASE 14
		CASE 16
		CASE 17
		CASE 22
		CASE 24
		CASE 25
		CASE 28
		CASE 34
		CASE 38
		CASE 44
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC
FUNC INT GET_VEHICLE_WITH_LIGHTS_ON(INT index)
	SWITCH index	
		CASE 0 RETURN 4
		CASE 1 RETURN 12
		CASE 2 RETURN 13
		CASE 3 RETURN 15
		CASE 4 RETURN 39
		CASE 5 RETURN 40
		CASE 6 RETURN 47
		CASE 7 RETURN 48
	ENDSWITCH
	RETURN -1
ENDFUNC
FUNC BOOL SHOULD_VEHICLE_HAVE_LIGHTS_ON(INT index)
	SWITCH index	
		CASE 4
		CASE 12
		CASE 13
		CASE 15
		CASE 39
		CASE 40
		CASE 47
		CASE 48
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

FUNC STRING CAR_MEET_VEHICLE_INT_TO_STRING(INT vehicle)
	RETURN CAR_MEET_VEHICLE_ENUM_TO_STRING(INT_TO_ENUM(CAR_MEET_AI_VEHICLE_ENUM, vehicle))
ENDFUNC
/// PURPOSE:
///    NEEDED FOR SPECIAL PED SPAWNING TO EITHER SPAWN THEM IN A SPECIFIED LOCATION OUTSIDE OF 50 AI VEHICLES OR REPLACE ONE OF THE 50 AI VEHICLES
/// PARAMS:
///    ped - ENUM FOR SPECIAL PED
/// RETURNS:
///     TRUE IF SHOULD BE SPAWNNED IN THEIR OWN SPECIAL LOCATION ( FALSE IF SHOULD REPLACE ONE OF THE 50 AI VEH LOCATIONS)
FUNC BOOL DOES_SPECIAL_PED_HAVE_PREFEDINED_LOCATION(CAR_MEET_SPECIAL_PED_VEHICLES_ENUM ped)
	SWITCH ped
		CASE PED_RACE_ORGANIZER
		CASE PED_MIMI
			RETURN TRUE
		BREAK
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC
FUNC VECTOR GET_SPECIAL_PED_VEHICLE_POSITION(CAR_MEET_SPECIAL_PED_VEHICLES_ENUM ped)
	SWITCH ped
		CASE PED_RACE_ORGANIZER
			RETURN <<-2207.8730, 1132.7607, -23.934>>
		BREAK
		CASE PED_MIMI
			RETURN <<-2178.1646, 1107.623, -25.3636>>
		BREAK
	ENDSWITCH
	RETURN <<0,0,0>>
ENDFUNC
FUNC FLOAT GET_SPECIAL_PED_VEHICLE_HEADING(CAR_MEET_SPECIAL_PED_VEHICLES_ENUM ped)
	SWITCH ped
		CASE PED_RACE_ORGANIZER
			RETURN 74.7026
		BREAK
		CASE PED_MIMI
			RETURN 116.4826
		BREAK
	ENDSWITCH
	RETURN 0.0
ENDFUNC

FUNC STRING CAR_MEET_SPECIAL_PED_ENUM_TO_STRING(CAR_MEET_SPECIAL_PED_VEHICLES_ENUM ped)
	SWITCH ped
		CASE PED_RACE_ORGANIZER 		RETURN "PED_RACE_ORGANIZER"					BREAK
		CASE PED_MIMI					RETURN "PED_MIMI"							BREAK
		CASE PED_MOODYMAN				RETURN "PED_MOODYMAN"						BREAK
		CASE PED_HAO					RETURN "PED_HAO"							BREAK
		CASE PED_BENNY					RETURN "PED_BENNY"							BREAK	
		CASE PED_BENNYS_MECHANIC		RETURN "PED_BENNYS_MECHANIC"				BREAK	
		CASE PED_JOHNNY_JONES			RETURN "PED_JOHNNY_JONES"					BREAK	
		CASE PED_LS_CUSTOMS_MECHANIC	RETURN "PED_LS_CUSTOMS_MECHANIC"			BREAK	
		CASE PED_WAREHOUSE_MECHANIC		RETURN "PED_WAREHOUSE_MECHANIC"				BREAK	
		CASE NO_PED_VEH_BANSHEE			RETURN "NO_PED_VEH_BANSHEE"					BREAK	
		CASE NO_PED_VEH_EMPEROR			RETURN "NO_PED_VEH_EMPEROR"					BREAK	
		CASE NO_PED_VEH_FURIA			RETURN "NO_PED_VEH_FURIA"					BREAK	
		CASE NO_PED_VEH_TYRANT			RETURN "NO_PED_VEH_TYRANT"					BREAK	
		CASE NO_PED_VEH_WINKY			RETURN "NO_PED_VEH_WINKY"					BREAK	
	ENDSWITCH
	RETURN ""
ENDFUNC
PROC GET_SPECIAL_PED_VEHICLE_DATA(CAR_MEET_SPECIAL_PED_VEHICLES_ENUM ped, VEHICLE_SETUP_STRUCT_MP &sData)
	SWITCH ped
		CASE PED_RACE_ORGANIZER
			sData.VehicleSetup.eModel = ZR350
			sData.VehicleSetup.tlPlateText = "01KQX761"
			sData.VehicleSetup.iColour1 = 73
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 73
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 12
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 10
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 54
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 8
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 5
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 7
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 11
			sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 4
			sData.VehicleSetup.iModIndex[MOD_DOOR_R] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 11
		BREAK
		CASE PED_MIMI
			sData.VehicleSetup.eModel = minivan2 // MINIVAN2
			sData.VehicleSetup.tlPlateText = "68HYO893"
			sData.VehicleSetup.iColour1 = 145
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 70
			sData.VehicleSetup.iColourExtra2 = 145
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 8
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 67
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 3
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 1
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 2
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 12
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 22
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 3
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
		BREAK
		CASE PED_MOODYMAN		
			sData.VehicleSetup.eModel =gauntlet4 // GAUNTLET4
			sData.VehicleSetup.tlPlateText = "MOODYMAN"
			sData.VehicleSetup.iColour1 = 111
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 15
		BREAK
		CASE PED_HAO
			sData.VehicleSetup.eModel = PENUMBRA // PENUMBRA
			sData.VehicleSetup.tlPlateText = "60HSW264"
			sData.VehicleSetup.iColour1 = 38
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 138
			sData.VehicleSetup.iColourExtra2 = 41
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 49
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
		BREAK
		CASE PED_BENNY
			sData.VehicleSetup.eModel = hermes // HERMES
			sData.VehicleSetup.tlPlateText = "B3NNY"
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 141
			sData.VehicleSetup.iColourExtra1 = 10
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 14
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE PED_BENNYS_MECHANIC
			sData.VehicleSetup.eModel = peyote3 // PEYOTE3
			sData.VehicleSetup.tlPlateText = "09AIO297"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 53
			sData.VehicleSetup.iColour2 = 147
			sData.VehicleSetup.iColourExtra1 = 53
			sData.VehicleSetup.iColourExtra2 = 37
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE PED_JOHNNY_JONES
			sData.VehicleSetup.eModel = FUTO // FUTO
			sData.VehicleSetup.tlPlateText = "ONDASP0T"
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 112
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 6
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE PED_LS_CUSTOMS_MECHANIC
			sData.VehicleSetup.eModel = Yosemite2 // YOSEMITE2
			sData.VehicleSetup.tlPlateText = "LSC2"
			sData.VehicleSetup.iColour1 = 69
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 74
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 11
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
		BREAK
		CASE PED_WAREHOUSE_MECHANIC
			sData.VehicleSetup.eModel = impaler // IMPALER
			sData.VehicleSetup.tlPlateText = "62CSB145"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 12
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 94
			sData.VehicleSetup.iNeonG = 255
			sData.VehicleSetup.iNeonB = 1
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 7
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 10
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE NO_PED_VEH_BANSHEE
			sData.VehicleSetup.eModel = BANSHEE
			sData.VehicleSetup.tlPlateText = "D481TCH"
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 88
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 42
		BREAK
		CASE NO_PED_VEH_EMPEROR
			sData.VehicleSetup.eModel = EMPEROR
			sData.VehicleSetup.tlPlateText = "LAMAR G"
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 53
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 3
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE NO_PED_VEH_FURIA
			sData.VehicleSetup.eModel =  FURIA
			sData.VehicleSetup.tlPlateText = "ANCESTOR"
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 138
			sData.VehicleSetup.iColour2 = 138
			sData.VehicleSetup.iColourExtra1 = 89
			sData.VehicleSetup.iColourExtra2 = 21
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 8
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
		BREAK
		CASE NO_PED_VEH_TYRANT
			sData.VehicleSetup.eModel =  TYRANT
			sData.VehicleSetup.tlPlateText = "MADRA20"
			sData.VehicleSetup.iColour1 = 141
			sData.VehicleSetup.iColour2 = 158
			sData.VehicleSetup.iColourExtra1 = 70
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 25
		BREAK
		CASE NO_PED_VEH_WINKY
			sData.VehicleSetup.eModel = winky
			sData.VehicleSetup.tlPlateText = "42OYK235"
			sData.VehicleSetup.iColour1 = 38
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 37
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 8
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 11
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 28
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Vehicle coordinates is based on the position in the array.
/// PARAMS:
///    index 
FUNC VECTOR GET_AI_VEHICLE_POSITON(INT index)
	SWITCH index
		CASE 0		RETURN <<-2207.9800, 1086.2830, -23.8210>>
		CASE 1		RETURN <<-2207.7542, 1089.4821, -23.6248>>
		CASE 2		RETURN <<-2207.2810, 1115.8967, -23.9970>>
		CASE 3		RETURN <<-2199.3850, 1117.9756, -23.7819>>
		CASE 4		RETURN <<-2186.7360, 1105.1770, -23.6080>>//<<-2185.6111, 1105.2025, -23.5652>>
		CASE 5		RETURN <<-2186.7571, 1090.0436, -23.9985>>
		CASE 6		RETURN <<-2186.8542, 1093.0175, -23.7006>>
		CASE 7		RETURN <<-2165.5908, 1096.1431, -25.0151>>
		CASE 8		RETURN <<-2165.7937, 1089.5665, -25.0063>>
		CASE 9		RETURN <<-2187.2397, 1116.9534, -23.9684>>
		CASE 10		RETURN <<-2199.1458, 1089.3051, -23.9967>>
		CASE 11		RETURN <<-2207.5715, 1112.8409, -23.6101>>
		CASE 12		RETURN <<-2207.4939, 1124.0216, -23.9006>>
		CASE 13		RETURN <<-2207.4924, 1128.5042, -23.8219>>
		CASE 14		RETURN <<-2157.5881, 1085.9642, -24.7143>>
		CASE 15		RETURN <<-2178.5085, 1138.2255, -24.7220>>
		CASE 16		RETURN <<-2178.3352, 1134.1493, -24.7390>>
		CASE 17		RETURN <<-2199.7878, 1126.0371, -23.9109>>
		CASE 18		RETURN <<-2196.2087, 1139.1560, -23.8270>>
		CASE 19		RETURN <<-2187.0869, 1082.9451, -23.7004>>
		CASE 20		RETURN <<-2199.9299, 1073.3486, -23.8247>>
		CASE 21		RETURN <<-2186.9036, 1120.1157, -23.9684>>
		CASE 22		RETURN <<-2186.9236, 1126.9691, -23.6264>>
		CASE 23		RETURN <<-2157.6660, 1088.9771, -25.0154>>
		CASE 24		RETURN <<-2165.5559, 1122.9930, -24.9359>>
		CASE 25		RETURN <<-2177.7668, 1082.6068, -24.7161>>
		CASE 26		RETURN <<-2178.3313, 1095.9530, -24.9317>>
		CASE 27		RETURN <<-2177.4417, 1103.5594, -24.8084>>
		CASE 28		RETURN <<-2177.8621, 1112.4559, -24.8130>>
		CASE 29		RETURN <<-2177.9956, 1115.8110, -24.8131>>
		CASE 30		RETURN <<-2177.9092, 1126.1991, -24.5806>>
		CASE 31		RETURN <<-2157.6177, 1122.9877, -24.9197>>
		CASE 32		RETURN <<-2165.8689, 1082.9851, -25.1011>>
		CASE 33		RETURN <<-2165.8579, 1099.1241, -25.0166>>
		CASE 34		RETURN <<-2165.7327, 1116.0560, -24.7375>>
		CASE 35		RETURN <<-2165.7139, 1113.0128, -24.7202>>
		CASE 36		RETURN <<-2157.4143, 1092.6615, -25.0158>>
		CASE 37		RETURN <<-2157.4890, 1099.3881, -24.5680>>
		CASE 38		RETURN <<-2157.7695, 1119.5001, -24.7377>>
		CASE 39		RETURN <<-2153.2402, 1073.6912, -24.9994>>
		CASE 40		RETURN <<-2144.7417, 1078.3011, -24.8055>>
		CASE 41		RETURN <<-2144.2424, 1146.9443, -24.9388>>
		CASE 42		RETURN <<-2144.3511, 1088.6233, -24.9323>>
		CASE 43		RETURN <<-2144.5117, 1091.5623, -24.8513>>
		CASE 44		RETURN <<-2178.0149, 1092.9044, -24.9227>>
		CASE 45		RETURN <<-2154.1758, 1140.5280, -25.0145>>
		CASE 46		RETURN <<-2157.5161, 1112.6079, -25.0091>>
		CASE 47		RETURN <<-2169.7104, 1073.6605, -25.0005>>
		CASE 48		RETURN <<-2215.4480, 1095.9480, -23.8653>>
		CASE 49		RETURN <<-2186.9919, 1130.1017, -23.4614>>
	ENDSWITCH
	RETURN <<0,0,0>>
ENDFUNC
/// PURPOSE:
///    Vehicle heading is based on the position in the array.
/// PARAMS:
///    index 
FUNC FLOAT GET_AI_VEHICLE_HEADING(INT index)
	SWITCH index	
		CASE 0		RETURN 89.2257
		CASE 1		RETURN 275.1232
		CASE 2		RETURN 90.0392
		CASE 3		RETURN 116.4836
		CASE 4		RETURN 150.1042
		CASE 5		RETURN 270.5697
		CASE 6		RETURN 92.3375
		CASE 7		RETURN 90.9944
		CASE 8		RETURN 92.7410
		CASE 9		RETURN 264.4242
		CASE 10		RETURN 267.0066
		CASE 11		RETURN 85.8560
		CASE 12		RETURN 49.9012
		CASE 13		RETURN 132.7019
		CASE 14		RETURN 270.5836
		CASE 15		RETURN 245.2490
		CASE 16		RETURN 74.8831
		CASE 17		RETURN 92.9992
		CASE 18		RETURN 278.2801
		CASE 19		RETURN 81.7416
		CASE 20		RETURN 250.0139
		CASE 21		RETURN 91.5535
		CASE 22		RETURN 92.3546
		CASE 23		RETURN 269.2820
		CASE 24		RETURN 86.2415
		CASE 25		RETURN 269.6463
		CASE 26		RETURN 108.3571
		CASE 27		RETURN 285.1019
		CASE 28		RETURN 279.0025
		CASE 29		RETURN 269.9982
		CASE 30		RETURN 270.4960
		CASE 31		RETURN 294.5404
		CASE 32		RETURN 89.8625
		CASE 33		RETURN 90.3754
		CASE 34		RETURN 92.4518
		CASE 35		RETURN 84.7794
		CASE 36		RETURN 273.0755
		CASE 37		RETURN 269.7734
		CASE 38		RETURN 87.7933
		CASE 39		RETURN 346.3212
		CASE 40		RETURN 99.4436
		CASE 41		RETURN 282.9675
		CASE 42		RETURN 267.4824
		CASE 43		RETURN 87.3259
		CASE 44		RETURN 90.0027
		CASE 45		RETURN 353.2417
		CASE 46		RETURN 275.9479
		CASE 47		RETURN 12.1427
		CASE 48		RETURN 0.0046
		CASE 49		RETURN 93.5280
	ENDSWITCH
	RETURN 0.0
ENDFUNC
PROC GET_AI_VEHICLE_DATA_0(CAR_MEET_AI_VEHICLE_ENUM eVehicle , VEHICLE_SETUP_STRUCT_MP &sData)
	SWITCH eVehicle
		CASE CM_VEH_ASBO     	
			sData.VehicleSetup.eModel = asbo // ASBO
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 88
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 88
			sData.VehicleSetup.iColourExtra2 = 37
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 7
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 7
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 10
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 4
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 176
		BREAK
		
		CASE CM_VEH_SULTAN2         	
			sData.VehicleSetup.eModel = SULTAN2 // SULTAN2
			sData.VehicleSetup.iColour1 = 29
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 28
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 9
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 9
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 14
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 48
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK

		CASE CM_VEH_BANSHEE2        
			sData.VehicleSetup.eModel = BANSHEE2
			sData.VehicleSetup.tlPlateText = "65GRZ071"
			sData.VehicleSetup.iColour1 = 27
			sData.VehicleSetup.iColour2 = 27
			sData.VehicleSetup.iColourExtra1 = 27
			sData.VehicleSetup.iColourExtra2 = 2
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 7
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 3
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 5
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 13
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
		BREAK
		
		CASE CM_VEH_SULTANRS        
			sData.VehicleSetup.eModel = SULTANRS
			sData.VehicleSetup.tlPlateText = "86CVG072"
			sData.VehicleSetup.iColour1 = 28
			sData.VehicleSetup.iColour2 = 28
			sData.VehicleSetup.iColourExtra1 = 28
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 11
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 6
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 3
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 12
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
		BREAK
		
		CASE CM_VEH_JESTER3         	
			sData.VehicleSetup.eModel = jester3 // JESTER3
			sData.VehicleSetup.iColour1 = 62
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 68
			sData.VehicleSetup.iColourExtra2 = 109
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 16
		BREAK
		CASE CM_VEH_BUCCANEER2        
			sData.VehicleSetup.eModel = BUCCANEER2 // BUCCANEE2
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 74
			sData.VehicleSetup.iColour2 = 4
			sData.VehicleSetup.iColourExtra1 = 74
			sData.VehicleSetup.iColourExtra2 = 138
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 8
			sData.VehicleSetup.iTyreG = 174
			sData.VehicleSetup.iTyreB = 239
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 1
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 1
			sData.VehicleSetup.iModIndex[MOD_HORN] = 43
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 67
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 6
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 4
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 1
			sData.VehicleSetup.iModIndex[MOD_ICE] = 2
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 6
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 5
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 2
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE CM_VEH_PEYOTE2         
			sData.VehicleSetup.eModel = peyote2 // PEYOTE2
			sData.VehicleSetup.iColour1 = 151
			sData.VehicleSetup.iColour2 = 52
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 16
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
		BREAK
		CASE CM_VEH_CHEBUREK         
			sData.VehicleSetup.eModel = cheburek // CHEBUREK
			sData.VehicleSetup.iColour1 = 8
			sData.VehicleSetup.iColour2 = 8
			sData.VehicleSetup.iColourExtra1 = 5
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 6
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 11
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
		BREAK
		CASE CM_VEH_CHEETAH2        
			sData.VehicleSetup.eModel = cheetah2 // CHEETAH2
			sData.VehicleSetup.iColour1 = 112
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 87
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 17
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 8
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 148
		BREAK
		CASE CM_VEH_CLUB        
			sData.VehicleSetup.eModel = club // CLUB
			sData.VehicleSetup.iColour1 = 91
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 91
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 8
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 7
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 12
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
		BREAK
		CASE CM_VEH_COMET5         
			sData.VehicleSetup.eModel = comet5 // COMET5
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 64
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 73
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 2
			sData.VehicleSetup.iNeonG = 21
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 13
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_COQUETTE4        
			sData.VehicleSetup.eModel = coquette4 // COQUETTE4
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 72
			sData.VehicleSetup.iColour2 = 72
			sData.VehicleSetup.iColourExtra1 = 63
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 19
		BREAK
		CASE CM_VEH_DEVIANT         
			sData.VehicleSetup.eModel = deviant // DEVIANT
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 2
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 7
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 8
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_DOMINATOR3        
			sData.VehicleSetup.eModel = dominator3 // DOMINATOR3
			sData.VehicleSetup.iColour1 = 38
			sData.VehicleSetup.iColour2 = 38
			sData.VehicleSetup.iColourExtra1 = 89
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 15
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 7
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 7
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 14
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 17
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_ELEGY      
			sData.VehicleSetup.eModel = elegy // ELEGY
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 89
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 88
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 8
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 19
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 9
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 12
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 6
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 6
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 212
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 3
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 5
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 15
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 7
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 14
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 16
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 8
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 7
			sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 6
			sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 11
			sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 1
			sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 2
		BREAK
		CASE CM_VEH_ELLIE 
			sData.VehicleSetup.eModel = ellie // ELLIE
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 111
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 26
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 10
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_HUSTLER         
			sData.VehicleSetup.eModel = hustler // HUSTLER
			sData.VehicleSetup.iColour1 = 88
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 1
		BREAK
		CASE CM_VEH_FACTION3
			sData.VehicleSetup.eModel = faction3 // FACTION3
			sData.VehicleSetup.iColour1 = 36
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 88
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 9
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 73
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 4
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 4
		BREAK
		CASE CM_VEH_KANJO
			sData.VehicleSetup.eModel = kanjo // KANJO
			sData.VehicleSetup.iColour1 = 6
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 4
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 9
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 22
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
		BREAK
		CASE CM_VEH_KOMODA  
			sData.VehicleSetup.eModel = komoda // KOMODA
			sData.VehicleSetup.iColour1 = 31
			sData.VehicleSetup.iColour2 = 147
			sData.VehicleSetup.iColourExtra1 = 27
			sData.VehicleSetup.iColourExtra2 = 30
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 23
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 11
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 17
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 16
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 10
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 19
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_GAUNTLET5   
			sData.VehicleSetup.eModel = gauntlet5 // GAUNTLET5
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 5
			sData.VehicleSetup.iColour2 = 4
			sData.VehicleSetup.iColourExtra1 = 145
			sData.VehicleSetup.iColourExtra2 = 30
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 9
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 2
			sData.VehicleSetup.iNeonG = 21
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 17
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 95
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 1
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 1
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 1
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE CM_VEH_GB200     
			sData.VehicleSetup.eModel = gb200 // GB200
			sData.VehicleSetup.iColour1 = 7
			sData.VehicleSetup.iColour2 = 28
			sData.VehicleSetup.iColourExtra1 = 5
			sData.VehicleSetup.iColourExtra2 = 158
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 95
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE CM_VEH_ELEGY2 
			sData.VehicleSetup.eModel = ELEGY2 // ELEGY2
			sData.VehicleSetup.iColour1 = 4
			sData.VehicleSetup.iColour2 = 7
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 5
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_ISSI7 
			sData.VehicleSetup.eModel = issi7 // ISSI7
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 150
			sData.VehicleSetup.iColour2 = 33
			sData.VehicleSetup.iColourExtra1 = 42
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 6
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 14
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
		BREAK
		CASE CM_VEH_ITALIGTO      
			sData.VehicleSetup.eModel = italigto // ITALIGTO
			sData.VehicleSetup.iColour1 = 34
			sData.VehicleSetup.iColour2 = 34
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 39
		BREAK
		CASE CM_VEH_JESTER     
			sData.VehicleSetup.eModel = Jester // JESTER
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 138
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 89
			sData.VehicleSetup.iColourExtra2 = 1
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 14
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_HELLION  
			sData.VehicleSetup.eModel = hellion // HELLION
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 42
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 94
			sData.VehicleSetup.iNeonG = 255
			sData.VehicleSetup.iNeonB = 1
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 14
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 8
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 7
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 4
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 10
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 7
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 24
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_LYNX  
			sData.VehicleSetup.eModel = lynx // LYNX
			sData.VehicleSetup.iColour1 = 62
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 68
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 16
		BREAK
		CASE CM_VEH_MAMBA
			sData.VehicleSetup.eModel = MAMBA // MAMBA
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 75
			sData.VehicleSetup.iColour2 = 75
			sData.VehicleSetup.iColourExtra1 = 70
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 21
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_MANANA2
			sData.VehicleSetup.eModel = manana2 // MANANA2
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 112
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 5
			sData.VehicleSetup.iNeonB = 190
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 26
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 1
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 12
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 8
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 12
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 12
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 12
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 2
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 7
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_MICHELLI
			sData.VehicleSetup.eModel = michelli // MICHELLI
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 27
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 5
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 22
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 10
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_MOONBEAM2  
			sData.VehicleSetup.eModel = moonbeam2 // MOONBEAM2
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 131
			sData.VehicleSetup.iColour2 = 39
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 27
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 9
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 3
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
			sData.VehicleSetup.iModIndex[MOD_HORN] = 28
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 106
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 3
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 1
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 3
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 1
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 1
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 1
		BREAK
		CASE CM_VEH_NIGHTSHADE 
			sData.VehicleSetup.eModel = nightshade // NITESHAD
			sData.VehicleSetup.tlPlateText = "09RMJ931"
			sData.VehicleSetup.iColour1 = 5
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
		BREAK
		CASE CM_VEH_PENUMBRA2  
			sData.VehicleSetup.eModel = penumbra2 // PENUMBRA2
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 6
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 10
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 28
		BREAK
		CASE CM_VEH_RAPIDGT3 
			sData.VehicleSetup.eModel = rapidgt3 // RAPIDGT3
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 31
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 29
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 21
		BREAK
		CASE CM_VEH_RETINUE2 
			sData.VehicleSetup.eModel = retinue2 // RETINUE2
			sData.VehicleSetup.iColour1 = 112
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 12
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
		BREAK
		CASE CM_VEH_SABREGT2
			sData.VehicleSetup.eModel = sabregt2 // SABREGT2
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 142
			sData.VehicleSetup.iColour2 = 4
			sData.VehicleSetup.iColourExtra1 = 81
			sData.VehicleSetup.iColourExtra2 = 88
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 9
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 31
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 5
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 19
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 11
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 12
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 15
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 19
			sData.VehicleSetup.iModIndex[MOD_ICE] = 2
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 5
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 7
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 4
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
		BREAK
		CASE CM_VEH_SAVESTRA 
			sData.VehicleSetup.eModel = savestra // SAVESTRA
			sData.VehicleSetup.tlPlateText = "68FTY342"
			sData.VehicleSetup.iColour1 = 50
			sData.VehicleSetup.iColour2 = 15
			sData.VehicleSetup.iColourExtra1 = 7
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 6
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 7
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 46
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_SCHLAGEN
			sData.VehicleSetup.eModel = schlagen // SCHLAGEN
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 64
			sData.VehicleSetup.iColour2 = 38
			sData.VehicleSetup.iColourExtra1 = 68
			sData.VehicleSetup.iColourExtra2 = 41
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 6
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 4
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 23
		BREAK
		CASE CM_VEH_SENTINEL3
			sData.VehicleSetup.eModel = sentinel3 // sentinel3
			sData.VehicleSetup.iColour1 = 3
			sData.VehicleSetup.iColour2 = 5
			sData.VehicleSetup.iColourExtra1 = 2
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 11
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
		BREAK
		CASE CM_VEH_SLAMVAN3
			sData.VehicleSetup.eModel = slamvan3 // SLAMVAN3
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 34
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 9
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 30
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 2
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE CM_VEH_SPECTER2
			sData.VehicleSetup.eModel = specter2 // SPECTER2
			sData.VehicleSetup.tlPlateText = "24SAN574"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 146
			sData.VehicleSetup.iColour2 = 5
			sData.VehicleSetup.iColourExtra1 = 145
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 15
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 8
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 5
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 23
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 3
		BREAK
		CASE CM_VEH_SUGOI
			sData.VehicleSetup.eModel = sugoi // Sugoi
			sData.VehicleSetup.iColour1 = 70
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 70
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 8
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 6
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 22
		BREAK
		CASE CM_VEH_INFERNUS2
			sData.VehicleSetup.eModel = infernus2 // INFERNUS2
			sData.VehicleSetup.iColour1 = 119
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 5
			sData.VehicleSetup.iColourExtra2 = 112
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 5
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 6
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 4
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_TORERO
			sData.VehicleSetup.eModel = torero // TORERO
			sData.VehicleSetup.iColour1 = 131
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 4
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 94
		BREAK
		CASE CM_VEH_YOSEMITE3 
			sData.VehicleSetup.eModel = yosemite3 // yosemite3
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 34
			sData.VehicleSetup.iColour2 = 11
			sData.VehicleSetup.iColourExtra1 = 47
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 5
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 5
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 8
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 2
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 2
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 6
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 1
			sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 1
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_TURISMO2
			sData.VehicleSetup.eModel = turismo2 // TURISMO2
			sData.VehicleSetup.iColour1 = 29
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 1
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 1
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 23
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_VERLIERER2
			sData.VehicleSetup.eModel = verlierer2 // VERLIER
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 32
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 1
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 226
			sData.VehicleSetup.iTyreG = 6
			sData.VehicleSetup.iTyreB = 6
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 1
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_VISERIS
			sData.VehicleSetup.eModel = viseris // VISERIS
			sData.VehicleSetup.iColour1 = 27
			sData.VehicleSetup.iColour2 = 11
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 112
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iTyreR = 226
			sData.VehicleSetup.iTyreG = 6
			sData.VehicleSetup.iTyreB = 6
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 3
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_HORN] = 18
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 4
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK	
		CASE CM_VEH_FLASHGT
			sData.VehicleSetup.eModel = flashgt // FLASHGT
			sData.VehicleSetup.iColour1 = 38
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 37
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 7
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_AI_VEHICLE_DATA_1(CAR_MEET_AI_VEHICLE_ENUM eVehicle , VEHICLE_SETUP_STRUCT_MP &sData)

	sData.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT //safety net to compensate for car variations that aren't ready
	
	SWITCH eVehicle
		CASE CM_VEH_ASBO       
			sData.VehicleSetup.eModel = asbo // ASBO
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 145
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 74
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 62
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 6
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 36
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 10
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_SULTAN2        
			sData.VehicleSetup.eModel = sultan2 // SULTAN2
			sData.VehicleSetup.iColour1 = 53
			sData.VehicleSetup.iColour2 = 89
			sData.VehicleSetup.iColourExtra1 = 59
			sData.VehicleSetup.iColourExtra2 = 89
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 14
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 6
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 8
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 12
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 10
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_BANSHEE2        
			sData.VehicleSetup.eModel = BANSHEE2
			sData.VehicleSetup.tlPlateText = "06NZQ185"
			sData.VehicleSetup.iColour1 = 38
			sData.VehicleSetup.iColour2 = 38
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 26
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 3
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 5
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 13
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 6
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
		BREAK
		CASE CM_VEH_SULTANRS       
			sData.VehicleSetup.eModel = SULTANRS
			sData.VehicleSetup.tlPlateText = "15HBW365"
			sData.VehicleSetup.iColour1 = 92
			sData.VehicleSetup.iColour2 = 92
			sData.VehicleSetup.iColourExtra1 = 92
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 11
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 9
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 6
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 49
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 5
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 4
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 5
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 6
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 6
			sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
		BREAK
		CASE CM_VEH_JESTER3
			sData.VehicleSetup.eModel = jester3 // JESTER3
			sData.VehicleSetup.iColour1 = 50
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 53
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 9
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 21
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE CM_VEH_BUCCANEER2    
			sData.VehicleSetup.eModel = BUCCANEER2 // BUCCANEE2	sData.VehicleSetup.iColour1 = 34
			sData.VehicleSetup.iColour2 = 32
			sData.VehicleSetup.iColourExtra1 = 35
			sData.VehicleSetup.iColourExtra2 = 90
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 62
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 10
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 21
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 3
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 10
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 3
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 19
			sData.VehicleSetup.iModIndex[MOD_ICE] = 4
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
		BREAK
		CASE CM_VEH_PEYOTE2     
			sData.VehicleSetup.eModel = peyote2 // PEYOTE2
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 11
			sData.VehicleSetup.iColour2 = 39
			sData.VehicleSetup.iColourExtra1 = 2
			sData.VehicleSetup.iColourExtra2 = 30
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 8
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 12
			sData.VehicleSetup.iModIndex[MOD_HORN] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 11
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 10
		BREAK
		CASE CM_VEH_CHEBUREK     
			sData.VehicleSetup.eModel = cheburek // CHEBUREK
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 63
			sData.VehicleSetup.iColour2 = 63
			sData.VehicleSetup.iColourExtra1 = 87
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 8
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 7
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 46
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
		BREAK
		CASE CM_VEH_CHEETAH2     
			sData.VehicleSetup.eModel = cheetah2 // CHEETAH2
			sData.VehicleSetup.iColour1 = 29
			sData.VehicleSetup.iColour2 = 29
			sData.VehicleSetup.iColourExtra1 = 28
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 13
		BREAK
		CASE CM_VEH_CLUB      
			sData.VehicleSetup.eModel = club // CLUB
			sData.VehicleSetup.tlPlateText = "24VDD860"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 105
			sData.VehicleSetup.iColour2 = 99
			sData.VehicleSetup.iColourExtra1 = 105
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 7
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 13
		BREAK
		CASE CM_VEH_COMET5      
			sData.VehicleSetup.eModel = comet5 // COMET5
			sData.VehicleSetup.iColour1 = 4
			sData.VehicleSetup.iColour2 = 42
			sData.VehicleSetup.iColourExtra1 = 88
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 22
		BREAK
		CASE CM_VEH_COQUETTE4  
			sData.VehicleSetup.eModel = coquette4 // COQUETTE4
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 29
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 29
			sData.VehicleSetup.iColourExtra2 = 27
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 6
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 19
	sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_DEVIANT      
			sData.VehicleSetup.eModel = deviant // DEVIANT	
			sData.VehicleSetup.iColour1 = 111
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 10
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 7
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_DOMINATOR3    
			sData.VehicleSetup.eModel = dominator3 // DOMINATOR3
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 64
			sData.VehicleSetup.iColour2 = 131
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 2
			sData.VehicleSetup.iNeonG = 21
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 11
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 17
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 15
		BREAK
		CASE CM_VEH_ELEGY     
			sData.VehicleSetup.eModel = elegy // ELEGY
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 38
			sData.VehicleSetup.iColour2 = 38
			sData.VehicleSetup.iColourExtra1 = 61
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 62
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 18
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 6
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 1
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 5
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 1
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 12
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 6
			sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 5
			sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_ELLIE     
			sData.VehicleSetup.eModel = ellie // ELLIE
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 3
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 4
			sData.VehicleSetup.iColourExtra2 = 2
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 24
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE CM_VEH_HUSTLER
			sData.VehicleSetup.eModel = hustler // HUSTLER
			sData.VehicleSetup.iColour1 = 101
			sData.VehicleSetup.iColour2 = 106
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 6
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 5
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
		BREAK
		CASE CM_VEH_FACTION3     
			sData.VehicleSetup.eModel = faction3 // FACTION3
			sData.VehicleSetup.iColour1 = 51
			sData.VehicleSetup.iColour2 = 51
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 8
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 67
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 5
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
		BREAK
		CASE CM_VEH_KANJO       
			sData.VehicleSetup.eModel = kanjo // KANJO
			sData.VehicleSetup.iColour1 = 27
			sData.VehicleSetup.iColour2 = 36
			sData.VehicleSetup.iColourExtra1 = 38
			sData.VehicleSetup.iColourExtra2 = 2
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 9
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 9
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 6
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 13
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1sData.VehicleSetup.iModIndex[MOD_WHEELS] = 30
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
			sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 2
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE CM_VEH_KOMODA    
			sData.VehicleSetup.eModel = komoda // KOMODA
			sData.VehicleSetup.iColour1 = 38
			sData.VehicleSetup.iColour2 = 104
			sData.VehicleSetup.iColourExtra1 = 37
			sData.VehicleSetup.iColourExtra2 = 27
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 24
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 15
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 17
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 7
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_GAUNTLET5     
			sData.VehicleSetup.eModel = gauntlet5 // GAUNTLET5
			sData.VehicleSetup.iColour1 = 32
			sData.VehicleSetup.iColour2 = 5
			sData.VehicleSetup.iColourExtra1 = 25
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 13
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 8
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 10
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 12
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 6
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 17
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 5
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 5
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 5
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 4
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 6
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 6
			sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 5
			sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 4
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 10
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_GB200      
			sData.VehicleSetup.eModel = gb200 // GB200
			sData.VehicleSetup.iColour1 = 30
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 3
			sData.VehicleSetup.iColourExtra2 = 30
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 6
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 119
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 7
		BREAK
		CASE CM_VEH_ELEGY2     
			sData.VehicleSetup.eModel = ELEGY2 // ELEGY2	
			sData.VehicleSetup.iColour1 = 28
			sData.VehicleSetup.iColour2 = 28
			sData.VehicleSetup.iColourExtra1 = 28
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 27
		BREAK
		CASE CM_VEH_ISSI7     
			sData.VehicleSetup.eModel = issi7 // ISSI7
			sData.VehicleSetup.iColour1 = 135
			sData.VehicleSetup.iColour2 = 135
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 50
			sData.VehicleSetup.iNeonB = 100
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 26
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_ITALIGTO      
			sData.VehicleSetup.eModel = italigto // ITALIGTO
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 112
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 6
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 5
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 17
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_JESTER       
			sData.VehicleSetup.eModel = Jester // JESTER
			sData.VehicleSetup.iColour1 = 9
			sData.VehicleSetup.iColour2 = 11
			sData.VehicleSetup.iColourExtra1 = 107
			sData.VehicleSetup.iColourExtra2 = 1
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 25
		BREAK
		CASE CM_VEH_HELLION     
			sData.VehicleSetup.eModel = hellion // HELLION
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 53
			sData.VehicleSetup.iColourExtra1 = 10
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 94
			sData.VehicleSetup.iNeonG = 255
			sData.VehicleSetup.iNeonB = 1
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 10
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_LYNX    
			sData.VehicleSetup.eModel = lynx // LYNX
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 50
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 55
			sData.VehicleSetup.iColourExtra2 = 53
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 94
			sData.VehicleSetup.iNeonG = 255
			sData.VehicleSetup.iNeonB = 1
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 5
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_MAMBA       
			sData.VehicleSetup.eModel = MAMBA // MAMBA
			sData.VehicleSetup.tlPlateText = "46IQG452"
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 120
			sData.VehicleSetup.iColourExtra1 = 10
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 3
			sData.VehicleSetup.iNeonG = 83
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 30
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_MANANA2   
			sData.VehicleSetup.eModel = manana2 // MANANA2
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 145
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 74
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 9
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 15
			sData.VehicleSetup.iNeonG = 3
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 217
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 1
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 3
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 9
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 9
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 13
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 9
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 20
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 2
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 5
			sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 2
		BREAK
		CASE CM_VEH_MICHELLI      
			sData.VehicleSetup.eModel = michelli // MICHELLI
			sData.VehicleSetup.tlPlateText = "88SOU906"
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 49
			sData.VehicleSetup.iColour2 = 1
			sData.VehicleSetup.iColourExtra1 = 52
			sData.VehicleSetup.iColourExtra2 = 2
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 6
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 26
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_MOONBEAM2    
			sData.VehicleSetup.eModel = moonbeam2 // MOONBEAM2	sData.VehicleSetup.iColour1 = 145
			sData.VehicleSetup.iColour2 = 145
			sData.VehicleSetup.iColourExtra1 = 137
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 9
			sData.VehicleSetup.iTyreR = 132
			sData.VehicleSetup.iTyreG = 102
			sData.VehicleSetup.iTyreB = 226
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 3
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
			sData.VehicleSetup.iModIndex[MOD_HORN] = 33
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 206
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 5
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 7
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 2
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 1
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 10
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 4
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 6
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 2
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 1
		BREAK
		CASE CM_VEH_NIGHTSHADE       
			sData.VehicleSetup.eModel = nightshade // NITESHAD
			sData.VehicleSetup.iColour1 = 53
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 59
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 26
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_PENUMBRA2      
			sData.VehicleSetup.eModel = penumbra2 // PENUMBRA2
			sData.VehicleSetup.iColour1 = 136
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 50
			sData.VehicleSetup.iNeonB = 100
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 10
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 15
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 7
		BREAK
		CASE CM_VEH_RAPIDGT3      
			sData.VehicleSetup.eModel = rapidgt3 // RAPIDGT3
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 146
			sData.VehicleSetup.iColour2 = 146
			sData.VehicleSetup.iColourExtra1 = 145
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 132
			sData.VehicleSetup.iTyreG = 102
			sData.VehicleSetup.iTyreB = 226
			sData.VehicleSetup.iNeonR = 35
			sData.VehicleSetup.iNeonG = 1
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 16
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 29
		BREAK
		CASE CM_VEH_RETINUE2     
			sData.VehicleSetup.eModel = retinue2 // RETINUE2
			sData.VehicleSetup.iColour1 = 61
			sData.VehicleSetup.iColour2 = 62
			sData.VehicleSetup.iColourExtra1 = 73
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 5
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 47
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
		BREAK
		CASE CM_VEH_SABREGT2      
			sData.VehicleSetup.eModel = sabregt2 // SABREGT2
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 27
			sData.VehicleSetup.iColour2 = 147
			sData.VehicleSetup.iColourExtra1 = 146
			sData.VehicleSetup.iColourExtra2 = 27
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 8
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 144
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 2
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 6
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 12
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 12
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 14
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 15
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 9
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 3
			sData.VehicleSetup.iModIndex[MOD_ICE] = 1
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 2
		BREAK
		CASE CM_VEH_SAVESTRA   
			sData.VehicleSetup.eModel = savestra // SAVESTRA
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 138
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 9
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 9
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 5
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 4
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 11
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 6
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
		BREAK
		CASE CM_VEH_SCHLAGEN    
			sData.VehicleSetup.eModel = schlagen // SCHLAGEN
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 28
			sData.VehicleSetup.iColourExtra1 = 3
			sData.VehicleSetup.iColourExtra2 = 12
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 21
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE CM_VEH_SENTINEL3      
			sData.VehicleSetup.eModel = sentinel3 // sentinel3
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 138
			sData.VehicleSetup.iColour2 = 1
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 138
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 15
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 8
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 22
		BREAK
		CASE CM_VEH_SLAMVAN3    
			sData.VehicleSetup.eModel = slamvan3 // SLAMVAN3
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 82
			sData.VehicleSetup.iColour2 = 23
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 3
			sData.VehicleSetup.iNeonG = 83
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 17
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 5
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 19
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 9
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 9
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 1
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 22
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 4
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
		BREAK
		CASE CM_VEH_SPECTER2      
			sData.VehicleSetup.eModel = specter2 // SPECTER2
			sData.VehicleSetup.tlPlateText = "69AHH012"
			sData.VehicleSetup.iColour1 = 39
			sData.VehicleSetup.iColour2 = 3
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 4
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 20
			sData.VehicleSetup.iTyreG = 20
			sData.VehicleSetup.iTyreB = 20
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 8
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 9
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 1
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 1
			sData.VehicleSetup.iModIndex[MOD_HORN] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_SUBWOOFER] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 1
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 2
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 2
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_SUGOI     
			sData.VehicleSetup.eModel = sugoi // Sugoi
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 149
			sData.VehicleSetup.iColour2 = 138
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 9
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 13
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 7
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 5
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 16
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_INFERNUS2      
			sData.VehicleSetup.eModel = infernus2 // INFERNUS2
			sData.VehicleSetup.tlPlateText = "88TNX911"
			sData.VehicleSetup.iColour1 = 145
			sData.VehicleSetup.iColour2 = 64
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 3
			sData.VehicleSetup.iNeonG = 83
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 1
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 1
			sData.VehicleSetup.iModIndex[MOD_HORN] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 138
		BREAK
		CASE CM_VEH_TORERO        
			sData.VehicleSetup.eModel = torero // TORERO
			sData.VehicleSetup.iColour1 = 29
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 5
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
		BREAK
		CASE CM_VEH_YOSEMITE3      
			sData.VehicleSetup.eModel = yosemite3 // yosemite3
			sData.VehicleSetup.iColour1 = 155
			sData.VehicleSetup.iColour2 = 13
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 6
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 12
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 23
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 20
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 13
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 1
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 5
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 14
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 8
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 6
			sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 5
			sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 4
			sData.VehicleSetup.iModIndex[MOD_DOOR_R] = 2
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 17
		BREAK
		CASE CM_VEH_TURISMO2       
			sData.VehicleSetup.eModel = turismo2 // TURISMO2
			sData.VehicleSetup.iColour1 = 38
			sData.VehicleSetup.iColour2 = 88
			sData.VehicleSetup.iColourExtra1 = 88
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 74
		BREAK
		CASE CM_VEH_VERLIERER2     
			sData.VehicleSetup.eModel = verlierer2 // VERLIER
			sData.VehicleSetup.tlPlateText = "07BQV306"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 64
			sData.VehicleSetup.iColour2 = 64
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 247
			sData.VehicleSetup.iTyreG = 44
			sData.VehicleSetup.iTyreB = 210
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
		BREAK
		CASE CM_VEH_VISERIS       
			sData.VehicleSetup.eModel = viseris // VISERIS
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 73
			sData.VehicleSetup.iColour2 = 62
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 7
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 1
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 49
		BREAK	
		CASE CM_VEH_FLASHGT
			sData.VehicleSetup.eModel = flashgt // FLASHGT
			sData.VehicleSetup.iColour1 = 10
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 7
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 6
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 6
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 141
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_AI_VEHICLE_DATA_2(CAR_MEET_AI_VEHICLE_ENUM eVehicle , VEHICLE_SETUP_STRUCT_MP &sData)
	
	sData.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT //safety net to compensate for car variations that aren't ready
	
	SWITCH eVehicle
		CASE CM_VEH_ASBO        
			sData.VehicleSetup.eModel = asbo // ASBO
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 135
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 135
			sData.VehicleSetup.iColourExtra2 = 81
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 13
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 9
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 8
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 4
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 5
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_SULTAN2   	
			sData.VehicleSetup.eModel = sultan2 // SULTAN2
			sData.VehicleSetup.iColour1 = 73
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 73
			sData.VehicleSetup.iColourExtra2 = 37
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 11
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 11
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
		BREAK
		CASE CM_VEH_BANSHEE2         
			sData.VehicleSetup.eModel =  BANSHEE2
			sData.VehicleSetup.tlPlateText = "68KWK927"
			sData.VehicleSetup.iColour1 = 64
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 70
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 2
			sData.VehicleSetup.iNeonG = 21
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 36
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 3
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 12
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 6
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
		BREAK
		CASE CM_VEH_SULTANRS         
			sData.VehicleSetup.eModel = sultanrs // SULTANRS
			sData.VehicleSetup.tlPlateText = "84TFX949"
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 138
			sData.VehicleSetup.iColour2 = 138
			sData.VehicleSetup.iColourExtra1 = 88
			sData.VehicleSetup.iColourExtra2 = 88
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 10
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 12
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 5
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 4
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 10
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 5
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 17
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 5
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 7
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 6
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 16
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
		BREAK
		CASE CM_VEH_JESTER3
			sData.VehicleSetup.eModel = jester3 // JESTER3
			sData.VehicleSetup.iColour1 = 38
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 62
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 11
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 26
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 10
		BREAK
		CASE CM_VEH_BUCCANEER2    				
			sData.VehicleSetup.eModel = BUCCANEER2 // BUCCANEE2
			sData.VehicleSetup.tlPlateText = "01FYK005"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 147
			sData.VehicleSetup.iColour2 = 10
			sData.VehicleSetup.iColourExtra1 = 4
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 9
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 16
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 9
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 4
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 3
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 1
			sData.VehicleSetup.iModIndex[MOD_ICE] = 2
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 3
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 2
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 7
		BREAK
		CASE CM_VEH_PEYOTE2     
			sData.VehicleSetup.eModel = peyote2 // PEYOTE2	sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 73
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 73
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_3)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 11
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3	sData.VehicleSetup.iModIndex[MOD_HORN] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 31
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE CM_VEH_CHEBUREK   
			sData.VehicleSetup.eModel = cheburek // CHEBUREK
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 42
			sData.VehicleSetup.iColour2 = 42
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 28
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
		BREAK
		CASE CM_VEH_CHEETAH2     
			sData.VehicleSetup.eModel = cheetah2 // CHEETAH2
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 92
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 92
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 21
		BREAK
		CASE CM_VEH_CLUB     
			sData.VehicleSetup.eModel = club // CLUB
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 70
			sData.VehicleSetup.iColour2 = 120
			sData.VehicleSetup.iColourExtra1 = 70
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 3
			sData.VehicleSetup.iNeonG = 83
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 10
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 5
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 7
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 7
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 4
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 33
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_COMET5   
			sData.VehicleSetup.eModel = comet5 // COMET5
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 38
			sData.VehicleSetup.iColour2 = 1
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 41
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 1
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_COQUETTE4  
			sData.VehicleSetup.eModel = coquette4 // COQUETTE4
			sData.VehicleSetup.tlPlateText = "47CYL696"
			sData.VehicleSetup.iColour1 = 49
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 52
			sData.VehicleSetup.iColourExtra2 = 11
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 21
		BREAK
		CASE CM_VEH_DEVIANT    
			sData.VehicleSetup.eModel = deviant // DEVIANT
			sData.VehicleSetup.iColour1 = 33
			sData.VehicleSetup.iColour2 = 33
			sData.VehicleSetup.iColourExtra1 = 47
			sData.VehicleSetup.iColourExtra2 = 2
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 62
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 7
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 1
		BREAK
		CASE CM_VEH_DOMINATOR3   
			sData.VehicleSetup.eModel = dominator3 // DOMINATOR3
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 53
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 89
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 94
			sData.VehicleSetup.iNeonG = 255
			sData.VehicleSetup.iNeonB = 1
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 11
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 15
		BREAK
		CASE CM_VEH_ELEGY      
			sData.VehicleSetup.eModel = elegy // ELEGY	sData.VehicleSetup.iColour1 = 61
			sData.VehicleSetup.iColour2 = 61
			sData.VehicleSetup.iColourExtra1 = 67
			sData.VehicleSetup.iColourExtra2 = 158
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 7
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 15
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 2
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 3
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 3
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 5
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 8
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 12
			sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 5
			sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 2
		BREAK
		CASE CM_VEH_ELLIE     
			sData.VehicleSetup.eModel = ellie // ELLIE
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 74
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 74
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 7
		BREAK
		CASE CM_VEH_HUSTLER
			sData.VehicleSetup.eModel = hustler // HUSTLER
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 143
			sData.VehicleSetup.iColour2 = 143
			sData.VehicleSetup.iColourExtra1 = 31
			sData.VehicleSetup.iColourExtra2 = 33
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 6
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 9
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK

		CASE CM_VEH_FACTION3    
			sData.VehicleSetup.eModel = faction3 // FACTION3	sData.VehicleSetup.iColour1 = 73
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 135
			sData.VehicleSetup.iColourExtra2 = 88
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 5
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 5
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 1
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 19
			sData.VehicleSetup.iModIndex[MOD_ICE] = 2
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 5
		BREAK
		CASE CM_VEH_KANJO     
			sData.VehicleSetup.eModel = kanjo // KANJO
			sData.VehicleSetup.iColour1 = 13
			sData.VehicleSetup.iColour2 = 13
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 9
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 9
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 6
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 6
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1sData.VehicleSetup.iModIndex[MOD_WHEELS] = 16
			sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_KOMODA    
			sData.VehicleSetup.eModel = komoda // KOMODA
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 112
			sData.VehicleSetup.iColour2 = 38
			sData.VehicleSetup.iColourExtra1 = 30
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
		BREAK
		CASE CM_VEH_GAUNTLET5    
			sData.VehicleSetup.eModel = gauntlet5 // GAUNTLET5
			sData.VehicleSetup.iColour1 = 68
			sData.VehicleSetup.iColour2 = 42
			sData.VehicleSetup.iColourExtra1 = 68
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 9
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 8
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 6
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 7
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 33
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 6
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 4
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 4
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 12
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 5
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 5
			sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 6
			sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 3
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_GB200       
			sData.VehicleSetup.eModel = gb200 // GB200
			sData.VehicleSetup.iColour1 = 53
			sData.VehicleSetup.iColour2 = 52
			sData.VehicleSetup.iColourExtra1 = 59
			sData.VehicleSetup.iColourExtra2 = 158
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 4
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 7
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
		BREAK
		CASE CM_VEH_ELEGY2    
			sData.VehicleSetup.eModel = ELEGY2 // ELEGY2
			sData.VehicleSetup.iColour1 = 5
			sData.VehicleSetup.iColour2 = 38
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 41
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_10)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 2
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_ISSI7     
			sData.VehicleSetup.eModel = issi7 // ISSI7
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 38
			sData.VehicleSetup.iColourExtra1 = 10
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 62
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
		BREAK
		CASE CM_VEH_ITALIGTO     
			sData.VehicleSetup.eModel = italigto // ITALIGTO
			sData.VehicleSetup.iColour1 = 147
			sData.VehicleSetup.iColour2 = 41
			sData.VehicleSetup.iColourExtra1 = 4
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 62
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 8
		BREAK
		CASE CM_VEH_JESTER      
			sData.VehicleSetup.eModel = Jester // JESTER
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 53
			sData.VehicleSetup.iColourExtra1 = 93
			sData.VehicleSetup.iColourExtra2 = 53
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_HELLION  
			sData.VehicleSetup.eModel = hellion // HELLION
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 132
			sData.VehicleSetup.iColour2 = 28
			sData.VehicleSetup.iColourExtra1 = 18
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 7
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 6
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 8
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 6
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_LYNX     
			sData.VehicleSetup.eModel = lynx // LYNX
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 88
			sData.VehicleSetup.iColour2 = 2
			sData.VehicleSetup.iColourExtra1 = 5
			sData.VehicleSetup.iColourExtra2 = 1
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 2
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_MAMBA       
			sData.VehicleSetup.eModel = MAMBA // MAMBA
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 27
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 36
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 19
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_MANANA2      
			sData.VehicleSetup.eModel = manana2 // MANANA2
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 27
			sData.VehicleSetup.iColourExtra1 = 10
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 9
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 64
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 2
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 13
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 14
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 1
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 2
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 1
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 3
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
		BREAK
		CASE CM_VEH_MICHELLI      
			sData.VehicleSetup.eModel = michelli // MICHELLI
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 73
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 73
			sData.VehicleSetup.iColourExtra2 = 1
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 3
			sData.VehicleSetup.iNeonG = 83
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 12
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_MOONBEAM2     
			sData.VehicleSetup.eModel = moonbeam2 // MOONBEAM2
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 33
			sData.VehicleSetup.iColour2 = 107
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 33
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 8
			sData.VehicleSetup.iTyreR = 226
			sData.VehicleSetup.iTyreG = 6
			sData.VehicleSetup.iTyreB = 6
			sData.VehicleSetup.iNeonR = 15
			sData.VehicleSetup.iNeonG = 3
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 3
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
			sData.VehicleSetup.iModIndex[MOD_HORN] = 35
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 92
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 7
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 6
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 10
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 8
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 3
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 1
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 13
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 5
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 8
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 3
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 2
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
		BREAK
		CASE CM_VEH_NIGHTSHADE    
			sData.VehicleSetup.eModel = nightshade // NITESHAD
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 132
		BREAK
		CASE CM_VEH_PENUMBRA2     
			sData.VehicleSetup.eModel = penumbra2 // PENUMBRA2
			sData.VehicleSetup.iColour1 = 131
			sData.VehicleSetup.iColour2 = 2
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 14
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 12
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 6
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 11
		BREAK
		CASE CM_VEH_RAPIDGT3      
			sData.VehicleSetup.eModel = rapidgt3 // RAPIDGT3
			sData.VehicleSetup.iColour1 = 63
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 36
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_RETINUE2    
			sData.VehicleSetup.eModel = retinue2 // RETINUE2
			sData.VehicleSetup.iColour1 = 38
			sData.VehicleSetup.iColour2 = 28
			sData.VehicleSetup.iColourExtra1 = 88
			sData.VehicleSetup.iColourExtra2 = 1
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 6
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 4
		BREAK
		CASE CM_VEH_SABREGT2
			sData.VehicleSetup.eModel = sabregt2 // SABREGT2
			sData.VehicleSetup.iColour1 = 137
			sData.VehicleSetup.iColour2 = 25
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 88
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 9
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 27
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 3
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 8
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 13
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 21
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 2
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 13
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 10
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 9
			sData.VehicleSetup.iModIndex[MOD_ICE] = 2
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 4
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE CM_VEH_SAVESTRA     
			sData.VehicleSetup.eModel = savestra // SAVESTRA
			sData.VehicleSetup.tlPlateText = "29DBU105"
			sData.VehicleSetup.iColour1 = 28
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 28
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 6
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 6
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
		BREAK
		CASE CM_VEH_SCHLAGEN     
			sData.VehicleSetup.eModel = schlagen // SCHLAGEN
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 137
			sData.VehicleSetup.iColour2 = 38
			sData.VehicleSetup.iColourExtra1 = 3
			sData.VehicleSetup.iColourExtra2 = 12
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 9
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
		BREAK
		CASE CM_VEH_SENTINEL3     
			sData.VehicleSetup.eModel = sentinel3 // sentinel3
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 83
			sData.VehicleSetup.iColour2 = 82
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 11
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 9
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 6
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 44
		BREAK
		CASE CM_VEH_SLAMVAN3     
			sData.VehicleSetup.eModel = slamvan3 // SLAMVAN3
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 141
			sData.VehicleSetup.iColour2 = 120
			sData.VehicleSetup.iColourExtra1 = 37
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 9
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 16
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 2
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 12
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 1
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 1
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 4
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 1
		BREAK
		CASE CM_VEH_SPECTER2     
			sData.VehicleSetup.eModel = specter2 // SPECTER2
			sData.VehicleSetup.tlPlateText = "53AYE372"
			sData.VehicleSetup.iColour1 = 70
			sData.VehicleSetup.iColour2 = 3
			sData.VehicleSetup.iColourExtra1 = 70
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 250
			sData.VehicleSetup.iTyreG = 117
			sData.VehicleSetup.iTyreB = 212
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 10
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 5
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 1
			sData.VehicleSetup.iModIndex[MOD_HORN] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_NITROUS] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 11
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 7
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 1
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 6
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 13
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE CM_VEH_SUGOI      
			sData.VehicleSetup.eModel = sugoi // Sugoi
			sData.VehicleSetup.iColour1 = 111
			sData.VehicleSetup.iColour2 = 27
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 27
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 5
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 4
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 17
		BREAK
		CASE CM_VEH_INFERNUS2    
			sData.VehicleSetup.eModel = infernus2 // INFERNUS2
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 50
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 92
			sData.VehicleSetup.iColourExtra2 = 112
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 94
			sData.VehicleSetup.iNeonG = 255
			sData.VehicleSetup.iNeonB = 1
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
		BREAK
		CASE CM_VEH_TORERO
			sData.VehicleSetup.eModel = torero // TORERO
			sData.VehicleSetup.tlPlateText = "62JDZ684"
			sData.VehicleSetup.iColour1 = 62
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 3
			sData.VehicleSetup.iNeonG = 83
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 10
		BREAK
		CASE CM_VEH_YOSEMITE3   
			sData.VehicleSetup.eModel = yosemite3 // yosemite3
			sData.VehicleSetup.iColour1 = 30
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 5
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 7
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 11
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 22
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 8
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 3
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 10
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 3
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_TURISMO2    
			sData.VehicleSetup.eModel = turismo2 // TURISMO2
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 51
			sData.VehicleSetup.iColour2 = 147
			sData.VehicleSetup.iColourExtra1 = 92
			sData.VehicleSetup.iColourExtra2 = 11
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonG = 255
			sData.VehicleSetup.iNeonB = 140
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 10
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 6
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 23
		BREAK
		CASE CM_VEH_VERLIERER2   
			sData.VehicleSetup.eModel = verlierer2 // VERLIER
			sData.VehicleSetup.iColour1 = 38
			sData.VehicleSetup.iColour2 = 3
			sData.VehicleSetup.iColourExtra1 = 89
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 27
		BREAK
		CASE CM_VEH_VISERIS     
			sData.VehicleSetup.eModel = viseris // VISERIS
			sData.VehicleSetup.iColour1 = 8
			sData.VehicleSetup.iColour2 = 52
			sData.VehicleSetup.iColourExtra1 = 134
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 114
			sData.VehicleSetup.iTyreG = 204
			sData.VehicleSetup.iTyreB = 114
			sData.VehicleSetup.iNeonG = 255
			sData.VehicleSetup.iNeonB = 140
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 6
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 1
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 199
		BREAK
		CASE CM_VEH_FLASHGT
			sData.VehicleSetup.eModel = flashgt // FLASHGT
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 111
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 11
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 10
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
	ENDSWITCH
ENDPROC

PROC GET_AI_VEHICLE_DATA_3(CAR_MEET_AI_VEHICLE_ENUM eVehicle , VEHICLE_SETUP_STRUCT_MP &sData)

	sData.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT //safety net to compensate for car variations that aren't ready
	
	SWITCH eVehicle
		CASE CM_VEH_ASBO  
			sData.VehicleSetup.eModel = asbo // ASBO
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 112
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 15
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 11
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 5
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 6
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_SULTAN2        		
			sData.VehicleSetup.eModel = sultan2 // SULTAN2
			sData.VehicleSetup.iColour1 = 112
			sData.VehicleSetup.iColour2 = 135
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 135
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 9
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 10
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 19
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_BANSHEE2        
			sData.VehicleSetup.eModel = banshee2 // BANSHEE2
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 137
			sData.VehicleSetup.iColour2 = 137
			sData.VehicleSetup.iColourExtra1 = 136
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 48
		BREAK
		CASE CM_VEH_SULTANRS       
			sData.VehicleSetup.eModel = sultanrs // SULTANRS
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 111
			sData.VehicleSetup.iColour2 = 135
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 8
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 6
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 7
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 13
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 6
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 4
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 2
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 12
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 12
			sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 6
			sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
			sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 3
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
		BREAK
		CASE CM_VEH_JESTER3
			sData.VehicleSetup.eModel = jester3 // JESTER3
			sData.VehicleSetup.iColour1 = 29
			sData.VehicleSetup.iColour2 = 131
			sData.VehicleSetup.iColourExtra1 = 28
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 62
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 4
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 135
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
		BREAK
		CASE CM_VEH_BUCCANEER2        
			sData.VehicleSetup.eModel = BUCCANEER2 // BUCCANEE2
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 51
			sData.VehicleSetup.iColour2 = 159
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 9
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_3)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 1
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 14
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 13
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 15
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 10
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 2
			sData.VehicleSetup.iModIndex[MOD_ICE] = 1
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 7
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
		BREAK
		CASE CM_VEH_PEYOTE2
			sData.VehicleSetup.eModel = peyote2 // PEYOTE2
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 89
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 88
			sData.VehicleSetup.iColourExtra2 = 30
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 8
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 6
			sData.VehicleSetup.iModIndex[MOD_HORN] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 31
		BREAK
		CASE CM_VEH_CHEBUREK     
			sData.VehicleSetup.eModel = cheburek // CHEBUREK
			sData.VehicleSetup.tlPlateText = "07LWB585"
			sData.VehicleSetup.iColour1 = 95
			sData.VehicleSetup.iColour2 = 95
			sData.VehicleSetup.iColourExtra1 = 97
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 7
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 44
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE CM_VEH_CHEETAH2     
			sData.VehicleSetup.eModel = cheetah2 // CHEETAH2
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 61
			sData.VehicleSetup.iColour2 = 131
			sData.VehicleSetup.iColourExtra1 = 63
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 9
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
		BREAK
		CASE CM_VEH_CLUB       
			sData.VehicleSetup.eModel = club // CLUB
			sData.VehicleSetup.tlPlateText = "05SBZ255"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 111
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 2
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 8
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 8
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 7
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 34
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_COMET5      
			sData.VehicleSetup.eModel = comet5 // COMET5
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 146
			sData.VehicleSetup.iColour2 = 158
			sData.VehicleSetup.iColourExtra1 = 145
			sData.VehicleSetup.iColourExtra2 = 142
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 15
			sData.VehicleSetup.iNeonG = 3
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 6
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_COQUETTE4    
			sData.VehicleSetup.eModel = coquette4 // COQUETTE4
			sData.VehicleSetup.iColour1 = 73
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 74
			sData.VehicleSetup.iColourExtra2 = 1
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 2
			sData.VehicleSetup.iNeonG = 21
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_DEVIANT      
			sData.VehicleSetup.eModel = deviant // DEVIANT
			sData.VehicleSetup.iColour1 = 50
			sData.VehicleSetup.iColour2 = 120
			sData.VehicleSetup.iColourExtra1 = 53
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 10
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_DOMINATOR3    
			sData.VehicleSetup.eModel = dominator3 // DOMINATOR3
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 31
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 9
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 7
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 11
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 11
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
		BREAK
		CASE CM_VEH_ELEGY      
			sData.VehicleSetup.eModel = elegy // ELEGY
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 4
			sData.VehicleSetup.iColour2 = 6
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 3
			sData.VehicleSetup.iNeonG = 83
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 16
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 8
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 6
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 4
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 16
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 1
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 11
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 7
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 12
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 11
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 5
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 7
			sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 5
			sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
		BREAK
		CASE CM_VEH_ELLIE      
			sData.VehicleSetup.eModel = ellie // ELLIE
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 28
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 28
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 6
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 5
		BREAK
		CASE CM_VEH_HUSTLER
			sData.VehicleSetup.eModel = hustler // HUSTLER
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 66
			sData.VehicleSetup.iColour2 = 66
			sData.VehicleSetup.iColourExtra1 = 60
			sData.VehicleSetup.iColourExtra2 = 30
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 24
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
		BREAK
		CASE CM_VEH_FACTION3     
			sData.VehicleSetup.eModel = faction3 // FACTION3
			sData.VehicleSetup.iColour1 = 142
			sData.VehicleSetup.iColour2 = 159
			sData.VehicleSetup.iColourExtra1 = 81
			sData.VehicleSetup.iColourExtra2 = 142
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 9
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 15
			sData.VehicleSetup.iNeonG = 3
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 216
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 11
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 10
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 10
			sData.VehicleSetup.iModIndex[MOD_ICE] = 1
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 7
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
		BREAK
		CASE CM_VEH_KANJO       
			sData.VehicleSetup.eModel = kanjo // KANJO
			sData.VehicleSetup.iColour1 = 89
			sData.VehicleSetup.iColour2 = 88
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 8
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 90
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
		BREAK
		CASE CM_VEH_KOMODA     
			sData.VehicleSetup.eModel = komoda // KOMODA
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 52
			sData.VehicleSetup.iColour2 = 64
			sData.VehicleSetup.iColourExtra1 = 33
			sData.VehicleSetup.iColourExtra2 = 27
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 12
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 6
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 12
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 5
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 3
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 10
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_GAUNTLET5    
			sData.VehicleSetup.eModel = gauntlet5 // GAUNTLET5
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 13
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 9
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 5
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 7
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 6
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 2
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 2
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 9
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 6
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 8
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 4
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE CM_VEH_GB200    
			sData.VehicleSetup.eModel = gb200 // GB200
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 68
			sData.VehicleSetup.iColour2 = 65
			sData.VehicleSetup.iColourExtra1 = 68
			sData.VehicleSetup.iColourExtra2 = 87
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 3
			sData.VehicleSetup.iNeonG = 83
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 34
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9
		BREAK
		CASE CM_VEH_ELEGY2    
			sData.VehicleSetup.eModel = ELEGY2 // ELEGY2
			sData.VehicleSetup.iColour1 = 2
			sData.VehicleSetup.iColour2 = 4
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 15
			sData.VehicleSetup.iNeonG = 3
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 17
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_ISSI7      
			sData.VehicleSetup.eModel = issi7 // ISSI7
			sData.VehicleSetup.iColour1 = 111
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_ITALIGTO      		
			sData.VehicleSetup.eModel = italigto // ITALIGTO
			sData.VehicleSetup.iColour1 = 42
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 7
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 36
		BREAK
		CASE CM_VEH_JESTER      
			sData.VehicleSetup.eModel = Jester // JESTER
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 31
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 41
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 1
			sData.VehicleSetup.iNeonB = 1
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 8
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_HELLION     
			sData.VehicleSetup.eModel = hellion // HELLION
			sData.VehicleSetup.iColour1 = 52
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 59
			sData.VehicleSetup.iColourExtra2 = 1
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 5
		BREAK
		CASE CM_VEH_LYNX     
			sData.VehicleSetup.eModel = lynx // LYNX
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 28
			sData.VehicleSetup.iColour2 = 28
			sData.VehicleSetup.iColourExtra1 = 41
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 32
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_MAMBA    
			sData.VehicleSetup.eModel = MAMBA // MAMBA
			sData.VehicleSetup.iColour1 = 49
			sData.VehicleSetup.iColour2 = 147
			sData.VehicleSetup.iColourExtra1 = 52
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 25
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_MANANA2    
			sData.VehicleSetup.eModel = manana2 // MANANA2
			sData.VehicleSetup.iColour1 = 53
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 59
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 94
			sData.VehicleSetup.iNeonG = 255
			sData.VehicleSetup.iNeonB = 1
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_3)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 3
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 6
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 15
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 16
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 7
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 21
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 4
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 4
		BREAK
		CASE CM_VEH_MICHELLI    
			sData.VehicleSetup.eModel = michelli // MICHELLI
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 89
			sData.VehicleSetup.iColourExtra1 = 89
			sData.VehicleSetup.iColourExtra2 = 89
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 6
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 3
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
			sData.VehicleSetup.iModIndex[MOD_HORN] = 45
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 16
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_MOONBEAM2   
			sData.VehicleSetup.eModel = moonbeam2 // MOONBEAM2
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 42
			sData.VehicleSetup.iColour2 = 42
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 81
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 8
			sData.VehicleSetup.iTyreR = 252
			sData.VehicleSetup.iTyreG = 238
			sData.VehicleSetup.iNeonR = 2
			sData.VehicleSetup.iNeonG = 21
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 3
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
			sData.VehicleSetup.iModIndex[MOD_HORN] = 43
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 217
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 10
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 13
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 11
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 12
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 4
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 5
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 10
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 20
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 4
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
		BREAK
		CASE CM_VEH_NIGHTSHADE   
			sData.VehicleSetup.eModel = nightshade // NITESHAD
			sData.VehicleSetup.iColour1 = 111
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 49
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_PENUMBRA2    
			sData.VehicleSetup.eModel = penumbra2 // PENUMBRA2
			sData.VehicleSetup.iColour1 = 62
			sData.VehicleSetup.iColour2 = 6
			sData.VehicleSetup.iColourExtra1 = 74
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 2
			sData.VehicleSetup.iNeonG = 21
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 6
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 10
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 9
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 10
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 50
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_RAPIDGT3     
			sData.VehicleSetup.eModel = rapidgt3 // RAPIDGT3
			sData.VehicleSetup.iColour1 = 88
			sData.VehicleSetup.iColour2 = 88
			sData.VehicleSetup.iColourExtra1 = 87
			sData.VehicleSetup.iColourExtra2 = 2
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 6
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 172
		BREAK
		CASE CM_VEH_RETINUE2     
			sData.VehicleSetup.eModel = retinue2 // RETINUE2
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 5
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 6
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 7
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_SABREGT2     
			sData.VehicleSetup.eModel = sabregt2 // SABREGT2
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 104
			sData.VehicleSetup.iColour2 = 3
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 154
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 8
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 24
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 1
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 16
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 12
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 2
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 14
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 17
			sData.VehicleSetup.iModIndex[MOD_ICE] = 1
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 3
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 5
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
		BREAK
		CASE CM_VEH_SAVESTRA     
			sData.VehicleSetup.eModel = savestra // SAVESTRA
			sData.VehicleSetup.iColour1 = 2
			sData.VehicleSetup.iColour2 = 120
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 10
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 29
		BREAK
		CASE CM_VEH_SCHLAGEN     
			sData.VehicleSetup.eModel = schlagen // SCHLAGEN
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 159
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 48
		BREAK
		CASE CM_VEH_SENTINEL3   
			sData.VehicleSetup.eModel = sentinel3 // sentinel3
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 112
			sData.VehicleSetup.iColour2 = 147
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 11
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 16
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 7
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 16
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_SLAMVAN3    
			sData.VehicleSetup.eModel = slamvan3 // SLAMVAN3
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 152
			sData.VehicleSetup.iColour2 = 107
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 7
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 1
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 2
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_SPECTER2   
			sData.VehicleSetup.eModel = specter2 // SPECTER2
			sData.VehicleSetup.tlPlateText = "03PTF712"
			sData.VehicleSetup.iColour1 = 88
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 173
			sData.VehicleSetup.iTyreG = 163
			sData.VehicleSetup.iTyreB = 154
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 16
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 8
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 7
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 10
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 3
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 1
			sData.VehicleSetup.iModIndex[MOD_HORN] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_NITROUS] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 1
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 1
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 16
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
		BREAK
		CASE CM_VEH_SUGOI      
			sData.VehicleSetup.eModel = sugoi // Sugoi
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 42
			sData.VehicleSetup.iColour2 = 150
			sData.VehicleSetup.iColourExtra1 = 4
			sData.VehicleSetup.iColourExtra2 = 27
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 1
			sData.VehicleSetup.iNeonB = 1
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 11
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 6
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 10
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 3
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE CM_VEH_INFERNUS2   
			sData.VehicleSetup.eModel = infernus2 // INFERNUS2
			sData.VehicleSetup.iColour1 = 64
			sData.VehicleSetup.iColour2 = 70
			sData.VehicleSetup.iColourExtra1 = 68
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreG = 174
			sData.VehicleSetup.iTyreB = 239
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 1
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 23
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_TORERO      
			sData.VehicleSetup.eModel = torero // TORERO
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 61
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 54
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 23
		BREAK
		CASE CM_VEH_YOSEMITE3    
			sData.VehicleSetup.eModel = yosemite3 // yosemite3
			sData.VehicleSetup.iColour1 = 74
			sData.VehicleSetup.iColour2 = 74
			sData.VehicleSetup.iColourExtra1 = 74
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 8
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 3
			sData.VehicleSetup.iNeonG = 83
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 8
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 202
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 1
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 9
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
		BREAK
		CASE CM_VEH_TURISMO2     
			sData.VehicleSetup.eModel = turismo2 // TURISMO2
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 72
			sData.VehicleSetup.iColour2 = 72
			sData.VehicleSetup.iColourExtra1 = 70
			sData.VehicleSetup.iColourExtra2 = 112
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_VERLIERER2     
			sData.VehicleSetup.eModel = verlierer2 // VERLIER
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 61
			sData.VehicleSetup.iColour2 = 3
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 45
		BREAK
		CASE CM_VEH_VISERIS    
			sData.VehicleSetup.eModel = viseris // VISERIS
			sData.VehicleSetup.iColour1 = 89
			sData.VehicleSetup.iColour2 = 89
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 31
		BREAK		
		CASE CM_VEH_FLASHGT
			sData.VehicleSetup.eModel = flashgt // FLASHGT
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 73
			sData.VehicleSetup.iColour2 = 73
			sData.VehicleSetup.iColourExtra1 = 73
			sData.VehicleSetup.iColourExtra2 = 1
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 3
			sData.VehicleSetup.iNeonG = 83
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 6
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 107
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_AI_VEHICLE_DATA_4(CAR_MEET_AI_VEHICLE_ENUM eVehicle , VEHICLE_SETUP_STRUCT_MP &sData)

	sData.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT //safety net to compensate for car variations that aren't ready
	
	SWITCH eVehicle
		CASE CM_VEH_ASBO    
			sData.VehicleSetup.eModel = asbo // ASBO
			sData.VehicleSetup.iColour1 = 30
			sData.VehicleSetup.iColour2 = 89
			sData.VehicleSetup.iColourExtra1 = 36
			sData.VehicleSetup.iColourExtra2 = 89
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 6
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
		BREAK
		
		CASE CM_VEH_SULTAN2   
			sData.VehicleSetup.eModel = sultan2 // SULTAN2
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 37
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 10
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 7
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 12
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 5
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 19
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		
		CASE CM_VEH_BANSHEE2   
			sData.VehicleSetup.eModel = banshee2 // BANSHEE2
			sData.VehicleSetup.iPlateIndex = 3
			sData.VehicleSetup.iColour1 = 119
			sData.VehicleSetup.iColour2 = 119
			sData.VehicleSetup.iColourExtra1 = 5
			sData.VehicleSetup.iColourExtra2 = 45
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 15
		BREAK
		
		CASE CM_VEH_SULTANRS   
			sData.VehicleSetup.eModel = sultanrs // SULTANRS
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 73
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 70
			sData.VehicleSetup.iColourExtra2 = 64
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 15
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 6
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 23
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 5
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 2
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 7
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 5
			sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 2
			sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_JESTER3 
			sData.VehicleSetup.eModel = jester3 // JESTER3
			sData.VehicleSetup.iColour1 = 92
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 92
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 9
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 4
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 8
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
		BREAK
		CASE CM_VEH_BUCCANEER2     
			sData.VehicleSetup.eModel = BUCCANEER2 // BUCCANEE2
			sData.VehicleSetup.iColour1 = 145
			sData.VehicleSetup.iColour2 = 147
			sData.VehicleSetup.iColourExtra1 = 136
			sData.VehicleSetup.iColourExtra2 = 90
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 8
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_3)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
			sData.VehicleSetup.iModIndex[MOD_HORN] = 17
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 1
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 3
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 13
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 6
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 1
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 2
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 1
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 15
			sData.VehicleSetup.iModIndex[MOD_ICE] = 3
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 4
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 2
		BREAK
		CASE CM_VEH_PEYOTE2       
			sData.VehicleSetup.eModel = peyote2 // PEYOTE2
			sData.VehicleSetup.tlPlateText = "67NQH763"
			sData.VehicleSetup.iColour1 = 5
			sData.VehicleSetup.iColour2 = 33
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 29
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
		BREAK
		CASE CM_VEH_CHEBUREK      
			sData.VehicleSetup.eModel = cheburek // CHEBUREK
			sData.VehicleSetup.iColour1 = 36
			sData.VehicleSetup.iColour2 = 36
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4	sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 20
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
		BREAK
		CASE CM_VEH_CHEETAH2      
			sData.VehicleSetup.eModel = cheetah2 // CHEETAH2
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 72
			sData.VehicleSetup.iColour2 = 72
			sData.VehicleSetup.iColourExtra1 = 64
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 14
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 8
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 17
		BREAK
		CASE CM_VEH_CLUB      
			sData.VehicleSetup.eModel = club // CLUB
			sData.VehicleSetup.tlPlateText = "02ZLN386"
			sData.VehicleSetup.iColour1 = 104
			sData.VehicleSetup.iColour2 = 98
			sData.VehicleSetup.iColourExtra1 = 104
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_COMET5      
			sData.VehicleSetup.eModel = comet5 // COMET5
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 31
			sData.VehicleSetup.iColour2 = 1
			sData.VehicleSetup.iColourExtra1 = 88
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 21
		BREAK
		CASE CM_VEH_COQUETTE4     
			sData.VehicleSetup.eModel = coquette4 // COQUETTE4
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 147
			sData.VehicleSetup.iColour2 = 138
			sData.VehicleSetup.iColourExtra1 = 4
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 150
			sData.VehicleSetup.iNeonB = 5
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 23
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_DEVIANT       
			sData.VehicleSetup.eModel = deviant // DEVIANT
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 38
			sData.VehicleSetup.iColour2 = 38
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 25
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_DOMINATOR3    
			sData.VehicleSetup.eModel = dominator3 // DOMINATOR3
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 13
			sData.VehicleSetup.iColour2 = 13
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 1
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 10
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 10
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 6
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 19
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_ELEGY       
			sData.VehicleSetup.eModel = elegy // ELEGY
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 92
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 92
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 3
			sData.VehicleSetup.iNeonG = 83
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 10
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 4
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 6
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 15
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 3
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 11
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 1
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 5
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 7
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY3] = 6
			sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 3
			sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 2
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
		BREAK
		CASE CM_VEH_ELLIE       
			sData.VehicleSetup.eModel = ellie // ELLIE
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 27
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 15
		BREAK
		CASE CM_VEH_HUSTLER 
			sData.VehicleSetup.eModel = hustler // HUSTLER
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 151
			sData.VehicleSetup.iColour2 = 151
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 11
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE CM_VEH_FACTION3   
			sData.VehicleSetup.eModel = faction3 // FACTION3
			sData.VehicleSetup.iColour1 = 28
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 28
			sData.VehicleSetup.iColourExtra2 = 27
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 8
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 140
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 1
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 9
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 5
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 10
			sData.VehicleSetup.iModIndex[MOD_ICE] = 2
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 4
		BREAK
		CASE CM_VEH_KANJO      
			sData.VehicleSetup.eModel = kanjo // KANJO
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 10
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 3
			sData.VehicleSetup.iNeonG = 83
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 18
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 12
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 13
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 6
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 7
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 13
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 8
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 17
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 3
			sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 8
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_KOMODA     
			sData.VehicleSetup.eModel = komoda // KOMODA
			sData.VehicleSetup.iColour1 = 68
			sData.VehicleSetup.iColour2 = 137
			sData.VehicleSetup.iColourExtra1 = 68
			sData.VehicleSetup.iColourExtra2 = 125
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 17
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 9
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 14
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 17
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 11
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 2
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 7
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_GAUNTLET5   
			sData.VehicleSetup.eModel = gauntlet5 // GAUNTLET5
			sData.VehicleSetup.iColour1 = 112
			sData.VehicleSetup.iColour2 = 6
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 13
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 15
			sData.VehicleSetup.iNeonG = 3
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 4
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 7
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 6
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 6
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 9
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR3] = 3
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 4
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 3
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 12
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 5
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 4
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
		BREAK
		CASE CM_VEH_GB200      
			sData.VehicleSetup.eModel = gb200 // GB200
			sData.VehicleSetup.iColour1 = 89
			sData.VehicleSetup.iColour2 = 53
			sData.VehicleSetup.iColourExtra1 = 88
			sData.VehicleSetup.iColourExtra2 = 158
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 4
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 5
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 7
		BREAK
		CASE CM_VEH_ELEGY2     
			sData.VehicleSetup.eModel = ELEGY2 // ELEGY2
			sData.VehicleSetup.iColour1 = 159
			sData.VehicleSetup.iColour2 = 158
			sData.VehicleSetup.iColourExtra1 = 160
			sData.VehicleSetup.iColourExtra2 = 90
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonG = 62
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 1
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_ISSI7      
			sData.VehicleSetup.eModel = issi7 // ISSI7
			sData.VehicleSetup.iColour1 = 92
			sData.VehicleSetup.iColour2 = 92
			sData.VehicleSetup.iColourExtra1 = 92
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 94
			sData.VehicleSetup.iNeonG = 255
			sData.VehicleSetup.iNeonB = 1
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 5
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 7
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 40
		BREAK
		CASE CM_VEH_ITALIGTO    
			sData.VehicleSetup.eModel = italigto // ITALIGTO
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 64
			sData.VehicleSetup.iColour2 = 73
			sData.VehicleSetup.iColourExtra1 = 68
			sData.VehicleSetup.iColourExtra2 = 140
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 6
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 18
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_JESTER     
			sData.VehicleSetup.eModel = Jester // JESTER
			sData.VehicleSetup.tlPlateText = "69YWO799"
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 62
			sData.VehicleSetup.iColour2 = 62
			sData.VehicleSetup.iColourExtra1 = 68
			sData.VehicleSetup.iColourExtra2 = 87
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 19
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_HELLION    
			sData.VehicleSetup.eModel = hellion // HELLION
			sData.VehicleSetup.iPlateIndex = 2
			sData.VehicleSetup.iColour1 = 79
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 4
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 4
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 2
			sData.VehicleSetup.iNeonG = 21
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 10
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 9
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 8
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 13
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_LYNX      
			sData.VehicleSetup.eModel = lynx // LYNX
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 10
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 13
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_MAMBA     
			sData.VehicleSetup.eModel = MAMBA // MAMBA
			sData.VehicleSetup.tlPlateText = "62EBS542"
			sData.VehicleSetup.iColour1 = 3
			sData.VehicleSetup.iColour2 = 6
			sData.VehicleSetup.iColourExtra1 = 6
			sData.VehicleSetup.iColourExtra2 = 1
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 10
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 2
		BREAK
		CASE CM_VEH_MANANA2      
			sData.VehicleSetup.eModel = manana2 // MANANA2
			sData.VehicleSetup.iPlateIndex = 4
			sData.VehicleSetup.iColour1 = 6
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 4
			sData.VehicleSetup.iColourExtra2 = 30
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 8
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_2)
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 153
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 21
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 14
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 7
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 15
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 5
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 5
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 5
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 5
		BREAK
		CASE CM_VEH_MICHELLI     
			sData.VehicleSetup.eModel = michelli // MICHELLI
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 88
			sData.VehicleSetup.iColour2 = 88
			sData.VehicleSetup.iColourExtra1 = 88
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 5
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 12
		BREAK
		CASE CM_VEH_MOONBEAM2    
			sData.VehicleSetup.eModel = moonbeam2 // MOONBEAM2
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 53
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 9
			sData.VehicleSetup.iTyreR = 252
			sData.VehicleSetup.iTyreG = 238
			sData.VehicleSetup.iNeonR = 15
			sData.VehicleSetup.iNeonG = 3
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 2
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 4
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 3
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 3
			sData.VehicleSetup.iModIndex[MOD_HORN] = 45
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 5
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 62
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 13
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 16
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 4
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 21
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 14
			sData.VehicleSetup.iModIndex[MOD_INTERIOR5] = 5
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 1
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 16
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 15
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 22
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 5
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 3
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 9
		BREAK
		CASE CM_VEH_NIGHTSHADE   
			sData.VehicleSetup.eModel = nightshade // NITESHAD
			sData.VehicleSetup.iColour1 = 27
			sData.VehicleSetup.iColour2 = 33
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 3
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 43
		BREAK
		CASE CM_VEH_PENUMBRA2    
			sData.VehicleSetup.eModel = penumbra2 // PENUMBRA2
			sData.VehicleSetup.iColour1 = 29
			sData.VehicleSetup.iColour2 = 111
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 8
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 6
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 10
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
		BREAK
		CASE CM_VEH_RAPIDGT3    
			sData.VehicleSetup.eModel = rapidgt3 // RAPIDGT3
			sData.VehicleSetup.tlPlateText = "69TPF037"
			sData.VehicleSetup.iColour1 = 0
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 10
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 4
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 44
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 6
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_RETINUE2    
			sData.VehicleSetup.eModel = retinue2 // RETINUE2
			sData.VehicleSetup.iColour1 = 29
			sData.VehicleSetup.iColour2 = 4
			sData.VehicleSetup.iColourExtra1 = 138
			sData.VehicleSetup.iColourExtra2 = 27
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 4
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 5
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
		BREAK
		CASE CM_VEH_SABREGT2    
			sData.VehicleSetup.eModel = sabregt2 // SABREGT2
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 138
			sData.VehicleSetup.iColour2 = 25
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 88
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 2
			sData.VehicleSetup.iWheelType = 9
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 24
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 4
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 1
			sData.VehicleSetup.iModIndex[MOD_PLTVANITY] = 2
			sData.VehicleSetup.iModIndex[MOD_INTERIOR2] = 17
			sData.VehicleSetup.iModIndex[MOD_INTERIOR4] = 8
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 5
			sData.VehicleSetup.iModIndex[MOD_KNOB] = 13
			sData.VehicleSetup.iModIndex[MOD_PLAQUE] = 10
			sData.VehicleSetup.iModIndex[MOD_ICE] = 2
			sData.VehicleSetup.iModIndex[MOD_TRUNK] = 4
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 6
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 4
		BREAK
		CASE CM_VEH_SAVESTRA    
			sData.VehicleSetup.eModel = savestra // SAVESTRA
			sData.VehicleSetup.iColour1 = 74
			sData.VehicleSetup.iColour2 = 14
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 4
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 9
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 4
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 12
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		CASE CM_VEH_SCHLAGEN     
			sData.VehicleSetup.eModel = schlagen // SCHLAGEN
			sData.VehicleSetup.tlPlateText = "23WKN123"
			sData.VehicleSetup.iColour1 = 131
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 6
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 5
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 23
		BREAK
		CASE CM_VEH_SENTINEL3     
			sData.VehicleSetup.eModel = sentinel3 // sentinel3
			sData.VehicleSetup.iColour1 = 1
			sData.VehicleSetup.iColour2 = 12
			sData.VehicleSetup.iColourExtra1 = 5
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 11
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 6
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 7
		BREAK
		CASE CM_VEH_SLAMVAN3      
			sData.VehicleSetup.eModel = slamvan3 // SLAMVAN3
			sData.VehicleSetup.iColour1 = 28
			sData.VehicleSetup.iColour2 = 138
			sData.VehicleSetup.iColourExtra1 = 88
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 2
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 1
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 3
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 16
			sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS] = 3
			sData.VehicleSetup.iModIndex[MOD_PLTHOLDER] = 7
			sData.VehicleSetup.iModIndex[MOD_INTERIOR1] = 2
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 9
			sData.VehicleSetup.iModIndex[MOD_STEERING] = 15
			sData.VehicleSetup.iModIndex[MOD_HYDRO] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 1
			sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 1
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 3
		BREAK
		CASE CM_VEH_SPECTER2       
			sData.VehicleSetup.eModel = specter2 // SPECTER2
			sData.VehicleSetup.tlPlateText = "26LFV544"
			sData.VehicleSetup.iColour1 = 2
			sData.VehicleSetup.iColour2 = 3
			sData.VehicleSetup.iColourExtra1 = 92
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 1
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 2
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 2
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 2
		BREAK
		CASE CM_VEH_SUGOI       
			sData.VehicleSetup.eModel = sugoi // Sugoi
			sData.VehicleSetup.iColour1 = 8
			sData.VehicleSetup.iColour2 = 31
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 122
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 7
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 8
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 7
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 2
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 7
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 4
		BREAK
		CASE CM_VEH_INFERNUS2     
			sData.VehicleSetup.eModel = infernus2 // INFERNUS2
			sData.VehicleSetup.iColour1 = 28
			sData.VehicleSetup.iColour2 = 31
			sData.VehicleSetup.iColourExtra1 = 28
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 20
			sData.VehicleSetup.iTyreG = 20
			sData.VehicleSetup.iTyreB = 20
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 5
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 6
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 6
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 4
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 2
			sData.VehicleSetup.iModIndex[MOD_GEARBOX] = 1
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 3
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TURBO] = 1
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_TYRE_SMOKE] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 24
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_TORERO        
			sData.VehicleSetup.eModel = torero // TORERO
			sData.VehicleSetup.iColour1 = 89
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 2
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 5
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 3
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 5
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 14
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_YOSEMITE3      
			sData.VehicleSetup.eModel = yosemite3 // yosemite3
			sData.VehicleSetup.iColour1 = 11
			sData.VehicleSetup.iColour2 = 3
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 0
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 8
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 5
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 11
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R] = 4
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 13
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 5
			sData.VehicleSetup.iModIndex[MOD_WING_R] = 7
			sData.VehicleSetup.iModIndex[MOD_ROOF] = 5
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 204
			sData.VehicleSetup.iModIndex[MOD_SEATS] = 9
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY1] = 3
			sData.VehicleSetup.iModIndex[MOD_ENGINEBAY2] = 5
			sData.VehicleSetup.iModIndex[MOD_CHASSIS2] = 5
			sData.VehicleSetup.iModIndex[MOD_CHASSIS3] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS4] = 1
			sData.VehicleSetup.iModIndex[MOD_CHASSIS5] = 7
			sData.VehicleSetup.iModIndex[MOD_DOOR_L] = 4
		BREAK
		CASE CM_VEH_TURISMO2       
			sData.VehicleSetup.eModel = turismo2 // TURISMO2
			sData.VehicleSetup.iColour1 = 70
			sData.VehicleSetup.iColour2 = 138
			sData.VehicleSetup.iColourExtra1 = 70
			sData.VehicleSetup.iColourExtra2 = 138
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWheelType = 5
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 8
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 1
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 2
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 7
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_VERLIERER2     
			sData.VehicleSetup.eModel = verlierer2 // VERLIER
			sData.VehicleSetup.iPlateIndex = 1
			sData.VehicleSetup.iColour1 = 4
			sData.VehicleSetup.iColour2 = 73
			sData.VehicleSetup.iColourExtra1 = 111
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 255
			sData.VehicleSetup.iNeonB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 1
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 21
			sData.VehicleSetup.iModVariation[0] = 1
		BREAK
		CASE CM_VEH_VISERIS      
			sData.VehicleSetup.eModel = viseris // VISERIS
			sData.VehicleSetup.iColour1 = 112
			sData.VehicleSetup.iColour2 = 0
			sData.VehicleSetup.iColourExtra1 = 0
			sData.VehicleSetup.iColourExtra2 = 112
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 3
			sData.VehicleSetup.iWheelType = 1
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iNeonR = 222
			sData.VehicleSetup.iNeonG = 222
			sData.VehicleSetup.iNeonB = 255
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 6
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 3
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 3
			sData.VehicleSetup.iModIndex[MOD_CHASSIS] = 3
			sData.VehicleSetup.iModIndex[MOD_GRILL] = 6
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_WING_L] = 1
			sData.VehicleSetup.iModIndex[MOD_ENGINE] = 3
			sData.VehicleSetup.iModIndex[MOD_BRAKES] = 2
			sData.VehicleSetup.iModIndex[MOD_ARMOUR] = 4
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] = 1
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 3
		BREAK
		CASE CM_VEH_FLASHGT
			sData.VehicleSetup.eModel = flashgt // FLASHGT
			sData.VehicleSetup.iColour1 = 53
			sData.VehicleSetup.iColour2 = 53
			sData.VehicleSetup.iColourExtra1 = 59
			sData.VehicleSetup.iColourExtra2 = 156
			sData.iColour5 = 1
			sData.iColour6 = 132
			sData.iLivery2 = 0
			sData.VehicleSetup.iWindowTintColour = 1
			sData.VehicleSetup.iWheelType = 7
			sData.VehicleSetup.iTyreR = 255
			sData.VehicleSetup.iTyreG = 255
			sData.VehicleSetup.iTyreB = 255
			sData.VehicleSetup.iModIndex[MOD_SPOILER] = 3
			sData.VehicleSetup.iModIndex[MOD_SKIRT] = 2
			sData.VehicleSetup.iModIndex[MOD_EXHAUST] = 4
			sData.VehicleSetup.iModIndex[MOD_BONNET] = 2
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION] = 4
			sData.VehicleSetup.iModIndex[MOD_WHEELS] = 40
			sData.VehicleSetup.iModIndex[MOD_LIVERY] = 1
		BREAK
		
	ENDSWITCH
ENDPROC
FUNC INT GET_SANDBOX_AMBIENT_VEHICLE_INDEX(int i)
	SWITCH i
		CASE 0 RETURN 4
		CASE 1 RETURN 7
		CASE 2 RETURN 8
		CASE 3 RETURN 9
		CASE 4 RETURN 12
		CASE 5 RETURN 14
		CASE 6 RETURN 20
		CASE 7 RETURN 22
		CASE 8 RETURN 23
		CASE 9 RETURN 26
		CASE 10 RETURN 28
		CASE 11 RETURN 30
		CASE 12 RETURN 31
		CASE 13 RETURN 32
		CASE 14 RETURN 33
		CASE 15 RETURN 34
		CASE 16 RETURN 35
		CASE 17 RETURN 36
		CASE 18 RETURN 37
		CASE 19 RETURN 38
		CASE 20 RETURN 39
		CASE 21 RETURN 40
		CASE 22 RETURN 41
		CASE 23 RETURN 42
		CASE 24 RETURN 43
		CASE 25 RETURN 45
		CASE 26 RETURN 46
		CASE 27 RETURN 47
	ENDSWITCH
	RETURN -1
ENDFUNC
///////////////
// @CULLING@ //
///////////////   
FUNC INT GET_AI_VEHICLE_CULLING_LEVEL(INT INDEX)
	//only needs to be populated with vehicles other than 0
	SWITCH index				
		CASE 9		RETURN 1
		CASE 17	 	RETURN 1
		CASE 22		RETURN 1			
		CASE 31		RETURN 1	
		CASE 43		RETURN 1	
				
		CASE 1		RETURN 2
		CASE 11		RETURN 2
		CASE 14		RETURN 2
		CASE 25		RETURN 2
		CASE 28		RETURN 2	
	ENDSWITCH
	RETURN 0
ENDFUNC
FUNC INT GET_AI_VEHICLE_ADDITIONAL_CULLING_PRIORITY(INT INDEX)
	SWITCH INDEX		
		CASE 8	RETURN 1
		CASE 16	RETURN 2
		CASE 34	RETURN 3
		CASE 38	RETURN 4
		CASE 44	RETURN 5
	ENDSWITCH
	RETURN 100
ENDFUNC
FUNC INT GET_PLAYER_COUNT()	
	INT iPlayerCount
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			iPlayerCount++
		ENDIF
	ENDREPEAT
	
	RETURN iPlayerCount
ENDFUNC

FUNC INT GET_AMBIENT_VEHICLE_LEVEL_THRESHOLD(INT iLevel)
	INT iThreshold = -1
	SWITCH iLevel
		// Vehicles have level 0, 1 & 2. 
		// Level 2 Vehicles are culled if the player count > 10
		// Level 1 Vehicles are culled if the player count > 20
		// Level 0 Vehicles are base and always remain
		CASE 0	iThreshold = 20	BREAK
		CASE 1	iThreshold = 10	BREAK
		CASE 2	iThreshold = 0	BREAK
	ENDSWITCH
	RETURN iThreshold
ENDFUNC

PROC MAINTAIN_AMBIENT_VEHICLE_CULLING_LEVELS(AI_VEHICLE_DATA sData)
	
	IF NOT ((GET_FRAME_COUNT()%NUM_FRAMES_TO_WAIT_TO_COUNT_PLAYERS) = 0)
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
		
	INT iPlayerCount = GET_PLAYER_COUNT()
	
	INT iLevel
	INT iThreshold
	REPEAT NUM_VEHICLE_CULLING_LEVELS iLevel
		iThreshold = GET_AMBIENT_VEHICLE_LEVEL_THRESHOLD(iLevel)
		IF (iThreshold = -1)
			BREAKLOOP
		ELSE
			IF (iPlayerCount > iThreshold)
				IF (sData.iCullingLevel != iLevel)
					sData.iCullingLevel = iLevel
					PRINTLN("[MP_CAR_MEET_AI_VEHICLES] MAINTAIN_AMBIENT_VEHICLE_CULLING_LEVELS - Updating Level: ", iLevel)
				ENDIF
				BREAKLOOP
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

FUNC BOOL DOES_VEHICLE_HAVE_BOOT_SPEAKERS(CAR_MEET_AI_VEHICLE_ENUM eVehicle)
	SWITCH eVehicle
		CASE CM_VEH_ASBO
		CASE CM_VEH_BUCCANEER2
		CASE CM_VEH_MANANA2
		CASE CM_VEH_SABREGT2	
			RETURN TRUE
		BREAK
		DEFAULT
			RETURN FALSE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Checks if a certain model of a car has the engine in the front (Needed for animations)
/// PARAMS:
///    eVehicle - enum representing models
FUNC BOOL IS_VEHICLE_ENGINE_IN_FRONT_AND_VIABLE_FOR_PED_ANIMATIONS(CAR_MEET_AI_VEHICLE_ENUM eVehicle )
	PRINTLN("[IS_VEHICLE_ENGINE_IN_FRONT_AND_VIABLE_FOR_PED_ANIMATIONS]" , CAR_MEET_VEHICLE_ENUM_TO_STRING(eVehicle))
	SWITCH eVehicle
		CASE CM_VEH_ASBO 			// ENSURES THERE IS ALWAYS ENOUGH OF VEHICLES WITH TRUNK SPEAKERS
		CASE CM_VEH_BUCCANEER2 		// ENSURES THERE IS ALWAYS ENOUGH OF VEHICLES WITH TRUNK SPEAKERS
		CASE CM_VEH_FACTION3 		// ENSURES THERE IS ALWAYS ENOUGH OF VEHICLES WITH TRUNK SPEAKERS
		CASE CM_VEH_MANANA2 		// ENSURES THERE IS ALWAYS ENOUGH OF VEHICLES WITH TRUNK SPEAKERS
		CASE CM_VEH_MOONBEAM2 		// ENSURES THERE IS ALWAYS ENOUGH OF VEHICLES WITH TRUNK SPEAKERS
		CASE CM_VEH_SABREGT2	
		CASE CM_VEH_BANSHEE2
		CASE CM_VEH_COMET5
		CASE CM_VEH_ELEGY	//HAS FRONT ENGINE BUT ALSO WONKY LIGHTS SO REMOVING
		CASE CM_VEH_GAUNTLET5	//HAS FRONT ENGINE BUT ALSO WONKY LIGHTS SO REMOVING
		CASE CM_VEH_COQUETTE4
		CASE CM_VEH_CHEETAH2
		CASE CM_VEH_HELLION
		CASE CM_VEH_GB200
		CASE CM_VEH_JESTER
		CASE CM_VEH_PEYOTE2
		CASE CM_VEH_SPECTER2
		CASE CM_VEH_INFERNUS2
		CASE CM_VEH_MAMBA
		CASE CM_VEH_MICHELLI
		CASE CM_VEH_SAVESTRA
		CASE CM_VEH_SENTINEL3
		CASE CM_VEH_TORERO
		CASE CM_VEH_TURISMO2
		CASE CM_VEH_VISERIS
			RETURN FALSE
		BREAK
		DEFAULT
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN TRUE
ENDFUNC
/// PURPOSE:
///    Returns data about vehicle models and mods (different for each variation)
/// PARAMS:
///    eVehicle - enum representing the model
///    iVariation - (0-4)
///    sData - struct to hold data
PROC GET_AI_VEHICLE_DATA(CAR_MEET_AI_VEHICLE_ENUM eVehicle, INT iVariation, VEHICLE_SETUP_STRUCT_MP &sData)

	SWITCH iVariation
		CASE 0		GET_AI_VEHICLE_DATA_0(eVehicle, sData) 		 BREAK
		CASE 1		GET_AI_VEHICLE_DATA_1(eVehicle, sData) 		 BREAK
		CASE 2		GET_AI_VEHICLE_DATA_2(eVehicle, sData) 		 BREAK
		CASE 3		GET_AI_VEHICLE_DATA_3(eVehicle, sData) 		 BREAK
		CASE 4		GET_AI_VEHICLE_DATA_4(eVehicle, sData) 		 BREAK
	ENDSWITCH
	
	IF sData.VehicleSetup.eModel = DUMMY_MODEL_FOR_SCRIPT
	AND iVariation > 0 
		GET_AI_VEHICLE_DATA_0(eVehicle, sData)
	ENDIF
ENDPROC

//// PURPOSE:
 ///    RETURNS index where vehicle with open hood is located. Needed for loops to convert values 0-7 to indices in the list
 /// PARAMS:
 ///    index - 0-7
 /// RETURNS:
 ///    index on the vehicle list (list of 50 vehicles with assigned locations)
FUNC INT GET_FRONT_ENGINE_INDEX(INT index)
	SWITCH index
			CASE 0 RETURN  VEHICLE_INDEX_WITH_OPEN_HOOD_0
			CASE 1 RETURN  VEHICLE_INDEX_WITH_OPEN_HOOD_1
			CASE 2 RETURN  VEHICLE_INDEX_WITH_OPEN_HOOD_2
			CASE 3 RETURN  VEHICLE_INDEX_WITH_OPEN_HOOD_3
			CASE 4 RETURN  VEHICLE_INDEX_WITH_OPEN_HOOD_4
			CASE 5 RETURN  VEHICLE_INDEX_WITH_OPEN_HOOD_5
			CASE 6 RETURN  VEHICLE_INDEX_WITH_OPEN_HOOD_6
			CASE 7 RETURN  VEHICLE_INDEX_WITH_OPEN_HOOD_7
			CASE 8 RETURN  VEHICLE_INDEX_WITH_OPEN_HOOD_8
	ENDSWITCH
	RETURN -1
ENDFUNC

FUNC INT GET_TRUNK_SPEAKERS_INDEX(INT index)
	SWITCH index
			CASE 0 RETURN  VEHICLE_INDEX_WITH_OPEN_BOOT_0
			CASE 1 RETURN  VEHICLE_INDEX_WITH_OPEN_BOOT_1
			CASE 2 RETURN  VEHICLE_INDEX_WITH_OPEN_BOOT_2
			CASE 3 RETURN  VEHICLE_INDEX_WITH_OPEN_BOOT_3
			CASE 4 RETURN  VEHICLE_INDEX_WITH_OPEN_BOOT_4		
	ENDSWITCH
	RETURN -1
ENDFUNC
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
//
// Car meet hydraulics, adapted from mission controller.	
//
//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
CONST_FLOAT cfCAR_MEET_HYDRAULICS_DANCE_MAX_STATE 	0.7

CONST_INT HDSA_ACTION_TRIGGERED_1		0
CONST_INT HDSA_ACTION_TRIGGERED_2		1
CONST_INT HDSA_ACTION_TRIGGERED_3		2
CONST_INT HDSA_ACTION_TRIGGERED_4		3
CONST_INT HDSA_ACTION_TRIGGERED_5		4
CONST_INT HDSA_ACTION_TRIGGERED_6		5
CONST_INT HDSA_ACTION_TRIGGERED_7		6
CONST_INT HDSA_ACTION_TRIGGERED_8		7
CONST_INT HDSA_ACTION_TRIGGERED_9		8
CONST_INT HDSA_ACTION_DOWNTIME			9

// +1 TO THE AMOUNT
CONST_INT HDCFG_MAX_RANDOM_EVENTS		5
CONST_INT HDCFG_REENTER_EVENT_CHANCE	250
CONST_INT HDCFG_ENTER_EVENT_CHANCE		450

PROC private_RAISE_WHEEL(CAR_MEET_AI_VEHICLE_CREATION_DATA &sData, PED_INDEX &tempPed, SC_WHEEL_LIST wheel, INT bit = -1 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), wheel, HS_WHEEL_FREE, cfCAR_MEET_HYDRAULICS_DANCE_MAX_STATE, 1.0 )
	IF bit >= 0 AND bit < 32
		SET_BIT( sData.sHydraulics.iHydraulicDanceBitSet, bit )
	ENDIF
ENDPROC

PROC private_LOWER_WHEEL(CAR_MEET_AI_VEHICLE_CREATION_DATA &sData, PED_INDEX &tempPed, SC_WHEEL_LIST wheel, INT bit = -1 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), wheel, HS_WHEEL_FREE, 0.0, 1.0 )
	IF bit >= 0 AND bit < 32
		SET_BIT( sData.sHydraulics.iHydraulicDanceBitSet, bit )
	ENDIF
ENDPROC

PROC private_RAISE_ALL_WHEELS(CAR_MEET_AI_VEHICLE_CREATION_DATA &sData, PED_INDEX &tempPed, INT bit = -1 )
	SET_HYDRAULIC_VEHICLE_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), HS_ALL_BOUNCE )
	
	IF bit >= 0 AND bit < 32
		SET_BIT( sData.sHydraulics.iHydraulicDanceBitSet, bit )
	ENDIF
ENDPROC

PROC private_LOWER_ALL_WHEELS(CAR_MEET_AI_VEHICLE_CREATION_DATA &sData, PED_INDEX &tempPed, INT bit = -1 )
	SET_HYDRAULIC_VEHICLE_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), HS_ALL_BOUNCE )
	
	IF bit >= 0 AND bit < 32
		SET_BIT( sData.sHydraulics.iHydraulicDanceBitSet, bit )
	ENDIF
ENDPROC

PROC private_RAISE_FRONT_WHEELS(CAR_MEET_AI_VEHICLE_CREATION_DATA &sData, PED_INDEX &tempPed, INT bit = -1 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), SC_WHEEL_CAR_FRONT_LEFT, HS_WHEEL_FREE, cfCAR_MEET_HYDRAULICS_DANCE_MAX_STATE, 1.0 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), SC_WHEEL_CAR_FRONT_RIGHT, HS_WHEEL_FREE, cfCAR_MEET_HYDRAULICS_DANCE_MAX_STATE, 1.0 )
	
	IF bit >= 0 AND bit < 32
		SET_BIT( sData.sHydraulics.iHydraulicDanceBitSet, bit )
	ENDIF
ENDPROC

PROC private_LOWER_FRONT_WHEELS(CAR_MEET_AI_VEHICLE_CREATION_DATA &sData, PED_INDEX &tempPed, INT bit = -1 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), SC_WHEEL_CAR_FRONT_LEFT, HS_WHEEL_FREE, 0.0, 1.0 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), SC_WHEEL_CAR_FRONT_RIGHT, HS_WHEEL_FREE, 0.0, 1.0 )
	IF bit >= 0 AND bit < 32
		SET_BIT( sData.sHydraulics.iHydraulicDanceBitSet, bit )
	ENDIF
ENDPROC

PROC private_RAISE_BACK_WHEELS(CAR_MEET_AI_VEHICLE_CREATION_DATA &sData, PED_INDEX &tempPed, INT bit = -1 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), SC_WHEEL_CAR_REAR_LEFT, HS_WHEEL_FREE, cfCAR_MEET_HYDRAULICS_DANCE_MAX_STATE, 1.0 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), SC_WHEEL_CAR_REAR_RIGHT, HS_WHEEL_FREE, cfCAR_MEET_HYDRAULICS_DANCE_MAX_STATE, 1.0 )
	
	IF bit >= 0 AND bit < 32
		SET_BIT( sData.sHydraulics.iHydraulicDanceBitSet, bit )
	ENDIF
ENDPROC

PROC private_LOWER_BACK_WHEELS(CAR_MEET_AI_VEHICLE_CREATION_DATA &sData, PED_INDEX &tempPed, INT bit = -1 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), SC_WHEEL_CAR_REAR_LEFT, HS_WHEEL_FREE, 0.0, 1.0 )
	SET_HYDRAULIC_WHEEL_STATE( GET_VEHICLE_PED_IS_IN( tempPed ), SC_WHEEL_CAR_REAR_RIGHT, HS_WHEEL_FREE, 0.0, 1.0 )
	
	IF bit >= 0 AND bit < 32
		SET_BIT( sData.sHydraulics.iHydraulicDanceBitSet, bit )
	ENDIF
ENDPROC

PROC private_CLEAR_HYDRAULIC_DATA(CAR_MEET_AI_VEHICLE_CREATION_DATA &sData)
	sData.sHydraulics.iHydraulicDanceBitSet = 0
	sData.sHydraulics.iHydraulicDanceCurrentRandomEvent = 0
	sData.sHydraulics.iHydraulicDanceTimer = 0
ENDPROC

PROC private_TEST_REENTER_HYDRAULIC_EVENT(CAR_MEET_AI_VEHICLE_CREATION_DATA &sData)
	IF GET_RANDOM_INT_IN_RANGE( 0, 1000 ) < HDCFG_REENTER_EVENT_CHANCE
		sData.sHydraulics.iHydraulicDanceCurrentRandomEvent = GET_RANDOM_INT_IN_RANGE( 1, HDCFG_MAX_RANDOM_EVENTS )
	ENDIF
ENDPROC

CONST_INT HYDRAULIC_UPDATE_FRAME_LIMIT 3

PROC private_PROCESS_RANDOM_HYDRAULIC_DANCING_EVENT(CAR_MEET_AI_VEHICLE_CREATION_DATA &sData, PED_INDEX &tempPed )
	
	PRINTLN( "[JJT][Hydraulics] private_PROCESS_RANDOM_HYDRAULIC_DANCING_EVENT. Event = ", sData.sHydraulics.iHydraulicDanceCurrentRandomEvent )
	
	SWITCH( sData.sHydraulics.iHydraulicDanceCurrentRandomEvent )
	
		//RANDOM MODIFIER 1 - ANTI-CLOCKWISE WAVE
		CASE 1
			IF sData.sHydraulics.iHydraulicDanceTimer > HYDRAULIC_UPDATE_FRAME_LIMIT
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
					private_LOWER_BACK_WHEELS(sData, tempPed )
					private_LOWER_FRONT_WHEELS(sData, tempPed )
					
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_LEFT, HDSA_ACTION_TRIGGERED_1 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_RIGHT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > HYDRAULIC_UPDATE_FRAME_LIMIT*2
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_LEFT, HDSA_ACTION_TRIGGERED_2 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_LEFT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > HYDRAULIC_UPDATE_FRAME_LIMIT*3
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_3 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_RIGHT, HDSA_ACTION_TRIGGERED_3 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_LEFT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > HYDRAULIC_UPDATE_FRAME_LIMIT*4
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_4 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_RIGHT, HDSA_ACTION_TRIGGERED_4 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_RIGHT )				
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > HYDRAULIC_UPDATE_FRAME_LIMIT*5
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_5 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_LEFT, HDSA_ACTION_TRIGGERED_5 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_RIGHT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > HYDRAULIC_UPDATE_FRAME_LIMIT*6
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_6 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_LEFT, HDSA_ACTION_TRIGGERED_6 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_LEFT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > HYDRAULIC_UPDATE_FRAME_LIMIT*7
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_7 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_RIGHT, HDSA_ACTION_TRIGGERED_7 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_LEFT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > HYDRAULIC_UPDATE_FRAME_LIMIT*8
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_8)
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_RIGHT, HDSA_ACTION_TRIGGERED_8 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_RIGHT )
					private_CLEAR_HYDRAULIC_DATA(sData)
					private_TEST_REENTER_HYDRAULIC_EVENT(sData)					
				ENDIF
			ENDIF
		BREAK
		//RANDOM MODIFIER 2 - CLOCKWISE WAVE
		CASE 2
			IF sData.sHydraulics.iHydraulicDanceTimer > HYDRAULIC_UPDATE_FRAME_LIMIT
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
					private_LOWER_BACK_WHEELS(sData, tempPed )
					private_LOWER_FRONT_WHEELS(sData, tempPed )
					
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_RIGHT, HDSA_ACTION_TRIGGERED_1 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_LEFT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > HYDRAULIC_UPDATE_FRAME_LIMIT*2
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_RIGHT, HDSA_ACTION_TRIGGERED_2 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_RIGHT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > HYDRAULIC_UPDATE_FRAME_LIMIT*3
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_3 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_LEFT, HDSA_ACTION_TRIGGERED_3 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_RIGHT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > HYDRAULIC_UPDATE_FRAME_LIMIT*4
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_4 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_LEFT, HDSA_ACTION_TRIGGERED_4 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_LEFT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > HYDRAULIC_UPDATE_FRAME_LIMIT*5
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_5 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_RIGHT, HDSA_ACTION_TRIGGERED_5 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_LEFT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > HYDRAULIC_UPDATE_FRAME_LIMIT*6
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_6 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_RIGHT, HDSA_ACTION_TRIGGERED_6 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_RIGHT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > HYDRAULIC_UPDATE_FRAME_LIMIT*7
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_7 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_LEFT, HDSA_ACTION_TRIGGERED_7 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_RIGHT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > HYDRAULIC_UPDATE_FRAME_LIMIT*8
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_8 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_LEFT, HDSA_ACTION_TRIGGERED_8 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_LEFT )
					private_CLEAR_HYDRAULIC_DATA(sData)
					private_TEST_REENTER_HYDRAULIC_EVENT(sData)
				ENDIF
			ENDIF
		BREAK
		//RANDOM MODIFIER 3 - TWO BUNNY HOPS
//		CASE 3
//			IF sData.sHydraulics.iHydraulicDanceTimer > 0
//				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
//					private_LOWER_BACK_WHEELS(sData, tempPed )
//					private_LOWER_FRONT_WHEELS(sData, tempPed )
//					
//					private_RAISE_ALL_WHEELS(sData, tempPed, HDSA_ACTION_TRIGGERED_1 )
//				ENDIF
//			ENDIF
//			
//			IF sData.sHydraulics.iHydraulicDanceTimer > 10
//				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
//					private_LOWER_ALL_WHEELS(sData, tempPed, HDSA_ACTION_TRIGGERED_2 )
//				ENDIF
//			ENDIF
//			
//			IF sData.sHydraulics.iHydraulicDanceTimer > 50
//				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_3 )
//					private_RAISE_ALL_WHEELS(sData, tempPed, HDSA_ACTION_TRIGGERED_3 )
//				ENDIF
//			ENDIF
//			
//			IF sData.sHydraulics.iHydraulicDanceTimer > 60
//				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_4 )
//					private_LOWER_ALL_WHEELS(sData, tempPed, HDSA_ACTION_TRIGGERED_4 )
//				ENDIF
//			ENDIF
//			
//			IF sData.sHydraulics.iHydraulicDanceTimer > 100
//				private_CLEAR_HYDRAULIC_DATA(sData)
//				private_TEST_REENTER_HYDRAULIC_EVENT(sData)
//			ENDIF
//		BREAK
//		//RANDOM MODIFIER 4 - BACK OFFSET BUNNY HOP
//		CASE 4
//			IF sData.sHydraulics.iHydraulicDanceTimer > 0
//				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
//					private_LOWER_BACK_WHEELS(sData, tempPed )
//					private_LOWER_FRONT_WHEELS(sData, tempPed )
//					
//					private_RAISE_BACK_WHEELS(sData, tempPed, HDSA_ACTION_TRIGGERED_1 )
//				ENDIF
//			ENDIF
//			
//			IF sData.sHydraulics.iHydraulicDanceTimer > 10
//				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
//					private_RAISE_FRONT_WHEELS(sData, tempPed, HDSA_ACTION_TRIGGERED_2 )
//					private_LOWER_BACK_WHEELS(sData, tempPed )
//				ENDIF
//			ENDIF
//			
//			IF sData.sHydraulics.iHydraulicDanceTimer > 20
//				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_3 )
//					private_LOWER_FRONT_WHEELS(sData, tempPed, HDSA_ACTION_TRIGGERED_3 )
//				ENDIF
//			ENDIF
//			
//			IF sData.sHydraulics.iHydraulicDanceTimer > 30
//				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_4 )
//					private_RAISE_BACK_WHEELS(sData, tempPed, HDSA_ACTION_TRIGGERED_4 )
//				ENDIF
//			ENDIF
//			
//			IF sData.sHydraulics.iHydraulicDanceTimer > 40
//				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_5 )
//					private_RAISE_FRONT_WHEELS(sData, tempPed, HDSA_ACTION_TRIGGERED_5 )
//					private_LOWER_BACK_WHEELS(sData, tempPed )
//				ENDIF
//			ENDIF
//			
//			IF sData.sHydraulics.iHydraulicDanceTimer > 50
//				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_6 )
//					private_LOWER_FRONT_WHEELS(sData, tempPed, HDSA_ACTION_TRIGGERED_6 )
//				ENDIF
//			ENDIF
//						
//			IF sData.sHydraulics.iHydraulicDanceTimer > 55
//				private_CLEAR_HYDRAULIC_DATA(sData)
//				private_TEST_REENTER_HYDRAULIC_EVENT(sData)
//			ENDIF
//		BREAK
//		//RANDOM MODIFIER 5 - BACK OFFSET BUNNY HOP
//		CASE 5
//			IF sData.sHydraulics.iHydraulicDanceTimer > 0
//				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
//					private_LOWER_BACK_WHEELS(sData, tempPed )
//					private_LOWER_FRONT_WHEELS(sData, tempPed )
//					
//					private_RAISE_FRONT_WHEELS(sData, tempPed, HDSA_ACTION_TRIGGERED_1 )
//				ENDIF
//			ENDIF
//			
//			IF sData.sHydraulics.iHydraulicDanceTimer > 10
//				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
//					private_RAISE_BACK_WHEELS(sData, tempPed, HDSA_ACTION_TRIGGERED_2 )
//					private_LOWER_FRONT_WHEELS(sData, tempPed )
//				ENDIF
//			ENDIF
//			
//			IF sData.sHydraulics.iHydraulicDanceTimer > 20
//				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_3 )
//					private_LOWER_BACK_WHEELS(sData, tempPed, HDSA_ACTION_TRIGGERED_3 )
//				ENDIF
//			ENDIF
//			
//			IF sData.sHydraulics.iHydraulicDanceTimer > 30
//				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_4 )
//					private_RAISE_FRONT_WHEELS(sData, tempPed, HDSA_ACTION_TRIGGERED_4 )
//				ENDIF
//			ENDIF
//			
//			IF sData.sHydraulics.iHydraulicDanceTimer > 40
//				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_5 )
//					private_RAISE_BACK_WHEELS(sData, tempPed, HDSA_ACTION_TRIGGERED_5 )
//					private_LOWER_FRONT_WHEELS(sData, tempPed )
//				ENDIF
//			ENDIF
//			
//			IF sData.sHydraulics.iHydraulicDanceTimer > 50
//				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_6 )
//					private_LOWER_BACK_WHEELS(sData, tempPed, HDSA_ACTION_TRIGGERED_6 )
//				ENDIF
//			ENDIF
//						
//			IF sData.sHydraulics.iHydraulicDanceTimer > 55
//				private_CLEAR_HYDRAULIC_DATA(sData)
//				private_TEST_REENTER_HYDRAULIC_EVENT(sData)
//			ENDIF
//		BREAK
		//RANDOM MODIFIER 6 - LEFT CORNER BUMP COUNTER
		CASE 3
			IF sData.sHydraulics.iHydraulicDanceTimer > 0
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
					private_LOWER_BACK_WHEELS(sData, tempPed )
					private_LOWER_FRONT_WHEELS(sData, tempPed )
					
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_LEFT, HDSA_ACTION_TRIGGERED_1 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_RIGHT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > 15
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_RIGHT, HDSA_ACTION_TRIGGERED_2 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_LEFT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > 30
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_3 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_LEFT, HDSA_ACTION_TRIGGERED_3 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_RIGHT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > 45
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_4 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_RIGHT, HDSA_ACTION_TRIGGERED_4 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_LEFT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > 60
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_5 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_LEFT, HDSA_ACTION_TRIGGERED_5 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_RIGHT )
				ENDIF
			ENDIF
		
			IF sData.sHydraulics.iHydraulicDanceTimer > 75
				private_LOWER_BACK_WHEELS(sData, tempPed )
				private_LOWER_FRONT_WHEELS(sData, tempPed )
				private_CLEAR_HYDRAULIC_DATA(sData)
				private_TEST_REENTER_HYDRAULIC_EVENT(sData)
			ENDIF
		BREAK
		
		//RANDOM MODIFIER 7 - RIGHT CORNER BUMP COUNTER
		CASE 4
			IF sData.sHydraulics.iHydraulicDanceTimer > 0
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
					private_LOWER_BACK_WHEELS(sData, tempPed )
					private_LOWER_FRONT_WHEELS(sData, tempPed )
					
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_RIGHT, HDSA_ACTION_TRIGGERED_1 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_LEFT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > 15
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_LEFT, HDSA_ACTION_TRIGGERED_2 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_RIGHT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > 30
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_3 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_RIGHT, HDSA_ACTION_TRIGGERED_3 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_LEFT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > 45
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_4 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_LEFT, HDSA_ACTION_TRIGGERED_4 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_RIGHT )
				ENDIF
			ENDIF
			
			IF sData.sHydraulics.iHydraulicDanceTimer > 60
				IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_5 )
					private_RAISE_WHEEL(sData, tempPed, SC_WHEEL_CAR_REAR_RIGHT, HDSA_ACTION_TRIGGERED_5 )
					private_LOWER_WHEEL(sData, tempPed, SC_WHEEL_CAR_FRONT_LEFT )
				ENDIF
			ENDIF
		
			IF sData.sHydraulics.iHydraulicDanceTimer > 75
				private_LOWER_BACK_WHEELS(sData, tempPed )
				private_LOWER_FRONT_WHEELS(sData, tempPed )
				private_CLEAR_HYDRAULIC_DATA(sData)
				private_TEST_REENTER_HYDRAULIC_EVENT(sData)
			ENDIF
		BREAK
	
	ENDSWITCH
	
ENDPROC

PROC PROCESS_HYDRAULIC_DANCING(CAR_MEET_AI_VEHICLE_CREATION_DATA &sData)

	VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_IN( sData.piDancingHydraulicsPed )
	
	IF NOT NETWORK_HAS_ENTITY_BEEN_REGISTERED_WITH_THIS_THREAD(tempVeh)
		EXIT
	ENDIF
	
	IF NOT NETWORK_DOES_NETWORK_ID_EXIST( VEH_TO_NET( tempVeh ) )
		EXIT
	ENDIF
	
	IF NOT NETWORK_HAS_CONTROL_OF_ENTITY( tempVeh )
		EXIT
	ENDIF
	
	IF sData.sHydraulics.iHydraulicDanceDowntimeTimer > 240 + sData.sHydraulics.iHydraulicDowntimerVariance
		IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_DOWNTIME )
			SET_BIT( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_DOWNTIME )
			private_LOWER_BACK_WHEELS(sData, sData.piDancingHydraulicsPed )
			private_LOWER_FRONT_WHEELS(sData, sData.piDancingHydraulicsPed )
		ENDIF
		
		IF sData.sHydraulics.iHydraulicDanceDowntimeTimer > 330 + sData.sHydraulics.iHydraulicDowntimerVariance
			CLEAR_BIT( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_DOWNTIME )
			sData.sHydraulics.iHydraulicDanceDowntimeTimer = 0
			sData.sHydraulics.iHydraulicDowntimerVariance = GET_RANDOM_INT_IN_RANGE( -35, 35 )
			private_CLEAR_HYDRAULIC_DATA(sData)
		ENDIF
		
		PRINTLN( "[JJT][Hydraulics] In downtime." )
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF (GET_FRAME_COUNT() % 10) = 0
			PRINTLN( "[JJT][Hydraulics] Processing Hydraulics Animation. sData.sHydraulics.iHydraulicDanceCurrentLoop = ", sData.sHydraulics.iHydraulicDanceCurrentLoop,
																	  ", sData.sHydraulics.iHydraulicDanceCurrentRandomEvent = ", sData.sHydraulics.iHydraulicDanceCurrentRandomEvent, 
																	  ", sData.sHydraulics.iHydraulicDanceTimer = ", sData.sHydraulics.iHydraulicDanceTimer )
		ENDIF
	#ENDIF

	SWITCH( sData.sHydraulics.iHydraulicDanceCurrentLoop )
		//BASE LOOP - FRONT POP UP
		CASE 1 
			//IF WE WANT TO DO THE BASE LOOP
			IF sData.sHydraulics.iHydraulicDanceCurrentRandomEvent = 0
				IF sData.sHydraulics.iHydraulicDanceTimer > 20
					IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
						private_RAISE_FRONT_WHEELS(sData, sData.piDancingHydraulicsPed, HDSA_ACTION_TRIGGERED_1 )
					ENDIF
				ENDIF
				
				IF sData.sHydraulics.iHydraulicDanceTimer > 35
					IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
						private_LOWER_FRONT_WHEELS(sData, sData.piDancingHydraulicsPed )
						private_CLEAR_HYDRAULIC_DATA(sData)
						
						//CHANCE OF GOING INTO A RANDOM EVENT
						IF GET_RANDOM_INT_IN_RANGE( 0, 1000 ) < HDCFG_ENTER_EVENT_CHANCE
							sData.sHydraulics.iHydraulicDanceCurrentRandomEvent = GET_RANDOM_INT_IN_RANGE( 1, HDCFG_MAX_RANDOM_EVENTS )
							PRINTLN( "[JJT][Hydraulics] Going into random event: ", sData.sHydraulics.iHydraulicDanceCurrentRandomEvent )
						ENDIF
						
						//5% CHANCE OF SWITCHING BASE LOOP
						IF GET_RANDOM_INT_IN_RANGE( 0, 1000 ) < 100
							PRINTLN( "[JJT][Hydraulics] Switching to base loop 2" )
							sData.sHydraulics.iHydraulicDanceCurrentLoop = 2
						ENDIF
					ENDIF
				ENDIF
			ELSE	
				private_PROCESS_RANDOM_HYDRAULIC_DANCING_EVENT(sData, sData.piDancingHydraulicsPed )
			ENDIF
		BREAK
		//BASE LOOP - BACK POP UP
		CASE 2 
			//IF WE WANT TO DO THE BASE LOOP
			IF sData.sHydraulics.iHydraulicDanceCurrentRandomEvent = 0
				IF sData.sHydraulics.iHydraulicDanceTimer > 20
					IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_1 )
						private_RAISE_BACK_WHEELS(sData, sData.piDancingHydraulicsPed, HDSA_ACTION_TRIGGERED_1 )
					ENDIF
				ENDIF
				
				IF sData.sHydraulics.iHydraulicDanceTimer > 35
					IF NOT IS_BIT_SET( sData.sHydraulics.iHydraulicDanceBitSet, HDSA_ACTION_TRIGGERED_2 )
						private_LOWER_BACK_WHEELS(sData, sData.piDancingHydraulicsPed )
						private_CLEAR_HYDRAULIC_DATA(sData)
						
						//CHANCE OF GOING INTO A RANDOM EVENT
						IF GET_RANDOM_INT_IN_RANGE( 0, 1000 ) < HDCFG_ENTER_EVENT_CHANCE
							sData.sHydraulics.iHydraulicDanceCurrentRandomEvent = GET_RANDOM_INT_IN_RANGE( 1, HDCFG_MAX_RANDOM_EVENTS )
						ENDIF
						
						//5% CHANCE OF SWITCHING BASE LOOP
						IF GET_RANDOM_INT_IN_RANGE( 0, 1000 ) < 100
							PRINTLN( "[JJT][Hydraulics] Switching to base loop 1" )
							sData.sHydraulics.iHydraulicDanceCurrentLoop = 1
						ENDIF
					ENDIF
				ENDIF
			ELSE	
				private_PROCESS_RANDOM_HYDRAULIC_DANCING_EVENT(sData, sData.piDancingHydraulicsPed )
			ENDIF
		BREAK
		
		CASE 3
			
		BREAK
		
	ENDSWITCH
ENDPROC

// ***************************************************************************************
// 					@FUNCTIONS@
// ***************************************************************************************
FUNC BOOL IS_SPECIAL_PED_VEHICLE_INDEX(int index)
	RETURN (index = g_sCarMeetSpecialPedPositionData.iVehicleIndex[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS]) OR (index = g_sCarMeetSpecialPedPositionData.iVehicleIndex[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1])		
ENDFUNC
PROC CLEANUP_CAR_MEET_SANDBOX_AI_VEHICLES(CAR_MEET_SANDBOX_AMBIENT_VEHICLE_CREATION_DATA &sAIVehicleCreationData)	
	//CLEAN UP VEHICLES
	INT i
	REPEAT ciCAR_MEET_NUMBER_OF_SANDBOX_AMBIENT_VEHICLES i		
		IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viVehicles[i])				
			DELETE_VEHICLE(sAIVehicleCreationData.viVehicles[i])
		ENDIF
	ENDREPEAT
	REPEAT ciCAR_MEET_NUMBER_OF_PROMO_VEHS i
		IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viTestVehicles[i])				
			DELETE_VEHICLE(sAIVehicleCreationData.viTestVehicles[i])
		ENDIF
	ENDREPEAT
ENDPROC 
PROC CLEANUP_CAR_MEET_AI_VEHICLES(CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData, BOOL bForceNotRespawn = FALSE)
	INT i = 0
	IF bForceNotRespawn
		IF NOT IS_BIT_SET(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_BLOCK_FROM_RESPAWNING)
			SET_BIT(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_BLOCK_FROM_RESPAWNING)
		ELSE
			CLEAR_BIT(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_BLOCK_FROM_RESPAWNING)
			sAIVehicleCreationData.iBS = 0
		ENDIF
	ENDIF
	//CLEAN UP VEHICLES
	REPEAT ciCAR_MEET_NUMER_OF_AI_VEHICLES i		
		IF IS_ENTITY_ALIVE(sAIVehicleCreationData.viVehicles[i])	
		AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(sAIVehicleCreationData.viVehicles[i])
			DELETE_VEHICLE(sAIVehicleCreationData.viVehicles[i])
		ENDIF
	ENDREPEAT
	REPEAT ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS i		
		IF IS_ENTITY_ALIVE(sAIVehicleCreationData.viSpecialPedVehicles[i])	
		AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(sAIVehicleCreationData.viSpecialPedVehicles[i])
			DELETE_VEHICLE(sAIVehicleCreationData.viSpecialPedVehicles[i])
		ENDIF
	ENDREPEAT
	b_HAVE_ALL_AI_VEHICLES_BEEN_CREATED = FALSE
	b_HALE_ALL_AL_VEHICLES_BEEN_CULLED = TRUE
	//RELEASE AUDIO IF USB NOT COLLECTED
	IF DOES_ENTITY_EXIST(sAIVehicleCreationData.oiTrunkProps[4])
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_TUNER/DLC_Tuner_Collectibles")
	ENDIF
	//CLEAN UP PROPS
	REPEAT ciCAR_MEET_NUMBER_OF_MOODYMAN_TRUNK_PROPS i
		IF IS_ENTITY_ALIVE(sAIVehicleCreationData.oiTrunkProps[i])	
		AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(sAIVehicleCreationData.oiTrunkProps[i])
			DELETE_OBJECT(sAIVehicleCreationData.oiTrunkProps[i])
		ENDIF
	ENDREPEAT
	IF NOT bForceNotRespawn
		sAIVehicleCreationData.iBS = 0
	ENDIF
	sAIVehicleCreationData.iMoodymanIndex = -1
ENDPROC 

PROC TOGGLE_VEHICLE_VISIBILITY_FOR_CUTSCENE(CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData, BOOL bVisible)
	
	INT iCurrentVehicle = 0
	INT i = 0
	
	//AMBIENT VEHS
	WHILE iCurrentVehicle != -1
		iCurrentVehicle = GET_VEHCILE_INDEX_TO_BE_HIDDEN_IN_CUTSCENE(i)
		i++
		IF iCurrentVehicle != -1
			IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viVehicles[iCurrentVehicle])
				IF IS_ENTITY_ALIVE(sAIVehicleCreationData.viVehicles[iCurrentVehicle])
					SET_ENTITY_VISIBLE(sAIVehicleCreationData.viVehicles[iCurrentVehicle], bVisible)
				ENDIF
			ELSE
				IF g_sCarMeetSpecialPedPositionData.iVehicleIndex[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS] = iCurrentVehicle
					IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viSpecialPedVehicles[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS])
						IF IS_ENTITY_ALIVE(sAIVehicleCreationData.viSpecialPedVehicles[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS])
							SET_ENTITY_VISIBLE(sAIVehicleCreationData.viSpecialPedVehicles[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS], bVisible)
						ENDIF
					ENDIF
				ELIF g_sCarMeetSpecialPedPositionData.iVehicleIndex[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1] = iCurrentVehicle
					IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viSpecialPedVehicles[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1])
						IF IS_ENTITY_ALIVE(sAIVehicleCreationData.viSpecialPedVehicles[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1])
							SET_ENTITY_VISIBLE(sAIVehicleCreationData.viSpecialPedVehicles[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1], bVisible)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDWHILE
	
	//SPECIAL PED MIMI's VEH
	IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viSpecialPedVehicles[ENUM_TO_INT(PED_MIMI)])
		IF IS_ENTITY_ALIVE(sAIVehicleCreationData.viSpecialPedVehicles[ENUM_TO_INT(PED_MIMI)])
			SET_ENTITY_VISIBLE(sAIVehicleCreationData.viSpecialPedVehicles[ENUM_TO_INT(PED_MIMI)], bVisible)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL HAVE_ALL_AI_VEHICLES_BEEN_CREATED()
	RETURN b_HAVE_ALL_AI_VEHICLES_BEEN_CREATED
ENDFUNC

PROC SET_ALL_VEHICLES_AS_CREATED()
	b_HAVE_ALL_AI_VEHICLES_BEEN_CREATED = TRUE
ENDPROC
/// PURPOSE:
///    Initially created list contains 5 of each car model in sequence with increasing variant int (0-4), this needs to be shuffeled to avoid sawning 5 of the same car one next to another
/// PARAMS:
///    sVehicleData - List of vehicles to be spawned. Will contain 10 car models and each instance of the same model will have a different variant
PROC SHUFFLE_VEHICLE_LIST(AI_VEHICLE_DATA &sVehicleData, INT iListSize)

	INT i, j
	AI_VEHICLE sSwap
	
	//Shuffle the list
	REPEAT iListSize i
		j = GET_RANDOM_INT_IN_RANGE(0, iListSize)
		
		sSwap = sVehicleData.sAIVehicle[i]
		sVehicleData.sAIVehicle[i] = sVehicleData.sAIVehicle[j]
		sVehicleData.sAIVehicle[j] = sSwap
	ENDREPEAT
	
ENDPROC
/// PURPOSE:
///    ENSURES THAT AT LEAST ONE OF THE MODELS ON THE LIST WITH A FRONT ENGINE TO OPEN THE HOOD
/// PARAMS:
///    iModelsToSpawn - list of 10 unique numbers representing the indices of models that will be spawned
PROC CHECK_FOR_AT_LEAST_ONE_VEHICLE_WITH_FRONT_ENGINE(INT &iModelsToSpawn[])
	INT i
	INT iCount=0
	REPEAT NUM_OF_UNIQUE_VEHICLE_MODELS i
		IF IS_VEHICLE_ENGINE_IN_FRONT_AND_VIABLE_FOR_PED_ANIMATIONS(INT_TO_ENUM(CAR_MEET_AI_VEHICLE_ENUM, iModelsToSpawn[i]))
			iCount++
		ENDIF
		IF iCount>1
			EXIT
		ENDIF
	ENDREPEAT
	//IF THE PROC DIDN'T EXIT ALL 10 VEHS HAVE ENGINE IN REAR, REPLACING MODEL WITH ONE THAT HAS FRONT ENGINE
	PRINTLN("[AM_MP_CAR_MEET_PROPERTY][MP_CAR_MEET_AI_VEHICLES] CHECK_FOR_AT_LEAST_ONE_VEHICLE_WITH_FRONT_ENGINE ALL VEHICLES ON THE LIST HAVE REAR ENGINE. REPLACING ONE")
	//SETTING DEFAULT VALUE TO REAR ENGINE FOR THE WHILE LOOP TO PROCESS AT LEAST ONCE
	int iModelIndex = ENUM_TO_INT(CM_VEH_CHEETAH2)
	WHILE NOT IS_VEHICLE_ENGINE_IN_FRONT_AND_VIABLE_FOR_PED_ANIMATIONS(INT_TO_ENUM(CAR_MEET_AI_VEHICLE_ENUM, iModelIndex))
			iModelIndex = GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CM_VEH_COUNT))
	ENDWHILE
	PRINTLN("[AM_MP_CAR_MEET_PROPERTY][MP_CAR_MEET_AI_VEHICLES] CHECK_FOR_AT_LEAST_ONE_VEHICLE_WITH_FRONT_ENGINE MODEL INDEX ", iModelsToSpawn[0], " replaced with ", iModelIndex)
	//REPLACE FIRST VEHICLE ON THE LIST WITH NEW MODEL
	iModelsToSpawn[0] = iModelIndex
ENDPROC
/// PURPOSE:
///    ENSURES THAT AT LEAST ONE OF THE MODELS ON THE LIST HAS BOOT SPEAKERS
/// PARAMS:
///    iModelsToSpawn - list of 10 unique numbers representing the indices of models that will be spawned
PROC CHECK_FOR_AT_LEAST_ONE_VEHICLE_WITH_BOOT_SPEAKERS(INT &iModelsToSpawn[])
	INT i
	REPEAT NUM_OF_UNIQUE_VEHICLE_MODELS i
		IF DOES_VEHICLE_HAVE_BOOT_SPEAKERS(INT_TO_ENUM(CAR_MEET_AI_VEHICLE_ENUM, iModelsToSpawn[i]))
			EXIT
		ENDIF
	ENDREPEAT
	//IF THE PROC DIDN'T EXIT ALL 10 VEHS HAVE ENGINE IN REAR, REPLACING MODEL WITH ONE THAT HAS FRONT ENGINE
	PRINTLN("[AM_MP_CAR_MEET_PROPERTY][MP_CAR_MEET_AI_VEHICLES] CHECK_FOR_AT_LEAST_ONE_VEHICLE_WITH_BOOT_SPEAKERS NONE OF THE VEHICLES HAVE BOOT SPEAKERS. REPLACING ONE")
	//SETTING DEFAULT VALUE TO REAR ENGINE FOR THE WHILE LOOP TO PROCESS AT LEAST ONCE
	int iModelIndex = ENUM_TO_INT(CM_VEH_CHEETAH2)
	WHILE NOT DOES_VEHICLE_HAVE_BOOT_SPEAKERS(INT_TO_ENUM(CAR_MEET_AI_VEHICLE_ENUM, iModelIndex))
			iModelIndex = GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CM_VEH_COUNT))
	ENDWHILE
	PRINTLN("[AM_MP_CAR_MEET_PROPERTY][MP_CAR_MEET_AI_VEHICLES] CHECK_FOR_AT_LEAST_ONE_VEHICLE_WITH_BOOT_SPEAKERS MODEL INDEX ", iModelsToSpawn[0], " replaced with ", iModelIndex)
	//REPLACE LAST VEHICLE ON THE LIST WITH NEW MODEL
	iModelsToSpawn[NUM_OF_UNIQUE_VEHICLE_MODELS-1] = iModelIndex
ENDPROC
/// PURPOSE:
///    Finds the first vehicle model on the list to have a front engine and places all instances of that vehicle at pre-selected locations for
///    ped animations. Also ensures car boot speaker vehicles are not on these spots to avoid locks in the function that rearranges them
/// PARAMS:
///    sVehicleData - list generated by the host
PROC POSITION_FRONT_ENGINE_VEHICLE_MODELS_FOR_PED_ANIMATIONS(AI_VEHICLE_DATA &sVehicleData)
	
	INT i, j=0
	AI_VEHICLE sSwap	
	BOOL bDidSwapOccur = FALSE
	//IF A VEHICLE IN OPEN HOOD LOCATION IS NOT VIABLE FOR PEDS TO VIEW SWAP WITH FIRST ONE ON THE LIST THAT IS VIABLE
	REPEAT NUM_VEHICLES_WITH_OPEN_HOOD i
		IF NOT IS_VEHICLE_ENGINE_IN_FRONT_AND_VIABLE_FOR_PED_ANIMATIONS(sVehicleData.sAIVehicle[GET_FRONT_ENGINE_INDEX(i)].eModel)
			PRINTLN("[MP_CAR_MEET_AI_VEHICLES][POSITION_FRONT_ENGINE_VEHICLE_MODELS_FOR_PED_ANIMATIONS] VEHICLE ", GET_FRONT_ENGINE_INDEX(i), " IS NOT VIABLE FOR FRONT ENGINE RELATED ANIMATIONS.")
			bDidSwapOccur = FALSE
			REPEAT ciCAR_MEET_NUMER_OF_AI_VEHICLES j
				//DO NOT accidentally replace current openhood veh with previous openhood veh, later iterations could ruin previous iterations
				IF j = VEHICLE_INDEX_WITH_OPEN_HOOD_0
				OR j = VEHICLE_INDEX_WITH_OPEN_HOOD_1
				OR j = VEHICLE_INDEX_WITH_OPEN_HOOD_2
				OR j = VEHICLE_INDEX_WITH_OPEN_HOOD_3
				OR j = VEHICLE_INDEX_WITH_OPEN_HOOD_4
				OR j = VEHICLE_INDEX_WITH_OPEN_HOOD_5
				OR j = VEHICLE_INDEX_WITH_OPEN_HOOD_6
				OR j = VEHICLE_INDEX_WITH_OPEN_HOOD_7
				OR j = VEHICLE_INDEX_WITH_OPEN_HOOD_8
					RELOOP
				ENDIF
				//any vehicle with front facing engine that is not already placed at open hood places, can be swapped
				IF IS_VEHICLE_ENGINE_IN_FRONT_AND_VIABLE_FOR_PED_ANIMATIONS(sVehicleData.sAIVehicle[j].eModel)
				PRINTLN("[MP_CAR_MEET_AI_VEHICLES][POSITION_FRONT_ENGINE_VEHICLE_MODELS_FOR_PED_ANIMATIONS] SWAPPING PLACES OF VEHICLES : ", GET_FRONT_ENGINE_INDEX(i)," AND ", j)
					sSwap = sVehicleData.sAIVehicle[j]
					bDidSwapOccur = TRUE
					sVehicleData.sAIVehicle[j] = sVehicleData.sAIVehicle[GET_FRONT_ENGINE_INDEX(i)]
					sVehicleData.sAIVehicle[GET_FRONT_ENGINE_INDEX(i)] = sSwap
					BREAKLOOP
				ENDIF
			ENDREPEAT
			IF NOT bDidSwapOccur
				PRINTLN("[MP_CAR_MEET_AI_VEHICLES][POSITION_FRONT_ENGINE_VEHICLE_MODELS_FOR_PED_ANIMATIONS] NOT ENOUGH FRON ENGINES FOR ANIMATIONS!")
			ENDIF
		ENDIF
		PRINTLN("[MP_CAR_MEET_AI_VEHICLES][POSITION_FRONT_ENGINE_VEHICLE_MODELS_FOR_PED_ANIMATIONS][MODEL] VEHICLE(", GET_FRONT_ENGINE_INDEX(i), "):" , CAR_MEET_VEHICLE_ENUM_TO_STRING(sVehicleData.sAIVehicle[GET_FRONT_ENGINE_INDEX(i)].eModel))
	ENDREPEAT
	
	
	
ENDPROC
FUNC BOOL IS_INDEX_A_RESTRICTED_LOCATION_FOR_MOODYMANN(int index)
	SWITCH index
		CASE 45
		CASE 47
			RETURN TRUE
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_INDEX_OF_A_SPECIAL_LOCATION(int index)
	IF index = VEHICLE_INDEX_WITH_OPEN_HOOD_0
	OR index = VEHICLE_INDEX_WITH_OPEN_HOOD_1
	OR index = VEHICLE_INDEX_WITH_OPEN_HOOD_2
	OR index = VEHICLE_INDEX_WITH_OPEN_HOOD_3
	OR index = VEHICLE_INDEX_WITH_OPEN_HOOD_4
	OR index = VEHICLE_INDEX_WITH_OPEN_HOOD_5
	OR index = VEHICLE_INDEX_WITH_OPEN_HOOD_6
	OR index = VEHICLE_INDEX_WITH_OPEN_HOOD_7
	OR index = VEHICLE_INDEX_WITH_OPEN_HOOD_8
	OR index = VEHICLE_INDEX_WITH_OPEN_BOOT_0
	OR index = VEHICLE_INDEX_WITH_OPEN_BOOT_1
	OR index = VEHICLE_INDEX_WITH_OPEN_BOOT_2
	OR index = VEHICLE_INDEX_WITH_OPEN_BOOT_3
	OR index = VEHICLE_INDEX_WITH_OPEN_BOOT_4
		RETURN TRUE
	ENDIF	
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Finds the first vehicle model on the list to have a front engine and places all instances of that vehicle at pre-selected locations for ped animations
/// PARAMS:
///    sVehicleData - list generated by the host
PROC POSITION_VEHICLES_WITH_BOOT_SPEAKERS_FOR_PED_ANIMATIONS(AI_VEHICLE_DATA &sVehicleData)
	
	INT i, j=0
	AI_VEHICLE sSwap	
	//IF A VEHICLE IN OPEN BOOT LOCATION IS NOT VIABLE FOR PEDS TO VIEW SWAP WITH FIRST ONE ON THE LIST THAT IS VIABLE
	REPEAT NUM_VEHICLES_WITH_BOOT_SPEAKERS i
		IF NOT DOES_VEHICLE_HAVE_BOOT_SPEAKERS(sVehicleData.sAIVehicle[GET_TRUNK_SPEAKERS_INDEX(i)].eModel)
			REPEAT ciCAR_MEET_NUMER_OF_AI_VEHICLES j
				//DO NOT accidentally replace current open BOOT/HOOD veh with previous openboot veh, later iterations could ruin previous iterations
				IF IS_INDEX_OF_A_SPECIAL_LOCATION(j)
					RELOOP
				ENDIF
				//any vehicle with front facing engine that is not already placed at open boot places, can be swapped
				IF DOES_VEHICLE_HAVE_BOOT_SPEAKERS(sVehicleData.sAIVehicle[j].eModel)
					sSwap = sVehicleData.sAIVehicle[j]
					sVehicleData.sAIVehicle[j] = sVehicleData.sAIVehicle[GET_TRUNK_SPEAKERS_INDEX(i)]
					sVehicleData.sAIVehicle[GET_TRUNK_SPEAKERS_INDEX(i)] = sSwap
					BREAKLOOP
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
	
ENDPROC
PROC POSITION_VEHICLES_WITHOUT_HEADLIGHTS_AWAY_FROM_HEADLIGHT_SPOTS(AI_VEHICLE_DATA &sVehicleData)
	
	INT i, j=0
	AI_VEHICLE sSwap	
	//IF A VEHICLE HEADLIGHTS ON LOCATION HAS WONKY LIGHTS REPLACE WITH FIRST THAT DOESNT
	REPEAT NUM_VEHICLES_WITH_HEADLIGHTS_ON i
		IF DOES_VEHICLE_HAVE_UNCONVENTIONAL_HEADLIGHTS(sVehicleData.sAIVehicle[GET_VEHICLE_WITH_LIGHTS_ON(i)].eModel)
			REPEAT ciCAR_MEET_NUMER_OF_AI_VEHICLES j
				//DO NOT accidentally replace openhood veh/trunk speakers veh
				IF IS_INDEX_OF_A_SPECIAL_LOCATION(j)
				OR SHOULD_VEHICLE_HAVE_LIGHTS_ON(j)
					RELOOP
				ENDIF
				//any vehicle with normal headlights that is not in any special spots can be used
				IF NOT DOES_VEHICLE_HAVE_UNCONVENTIONAL_HEADLIGHTS(sVehicleData.sAIVehicle[j].eModel)
					sSwap = sVehicleData.sAIVehicle[j]
					sVehicleData.sAIVehicle[j] = sVehicleData.sAIVehicle[GET_VEHICLE_WITH_LIGHTS_ON(i)]
					sVehicleData.sAIVehicle[GET_VEHICLE_WITH_LIGHTS_ON(i)] = sSwap
					BREAKLOOP
				ENDIF
			ENDREPEAT
		ENDIF
	ENDREPEAT
	
	
ENDPROC
FUNC INT GET_RANDOM_VEHICLE_WITH_FRONT_ENGINE()
	INT i = GET_RANDOM_INT_IN_RANGE(0,24)
	  SWITCH  i
		CASE 0       RETURN ENUM_TO_INT(CM_VEH_SULTAN2) 		BREAK
		CASE 1       RETURN ENUM_TO_INT(CM_VEH_SULTANRS) 		BREAK
		CASE 2       RETURN ENUM_TO_INT(CM_VEH_JESTER3) 	    BREAK
		CASE 3       RETURN ENUM_TO_INT(CM_VEH_CLUB) 			BREAK
		CASE 4       RETURN ENUM_TO_INT(CM_VEH_DEVIANT) 		BREAK
		CASE 5       RETURN ENUM_TO_INT(CM_VEH_DOMINATOR3) 		BREAK
		CASE 6       RETURN ENUM_TO_INT(CM_VEH_FLASHGT) 		BREAK
		CASE 7       RETURN ENUM_TO_INT(CM_VEH_ELLIE)			BREAK
		CASE 8       RETURN ENUM_TO_INT(CM_VEH_HUSTLER) 		BREAK
		CASE 9       RETURN ENUM_TO_INT(CM_VEH_KANJO) 			BREAK
		CASE 10      RETURN ENUM_TO_INT(CM_VEH_KOMODA) 			BREAK
		CASE 11      RETURN ENUM_TO_INT(CM_VEH_VERLIERER2)		BREAK
		CASE 12      RETURN ENUM_TO_INT(CM_VEH_ELEGY2) 			BREAK
		CASE 13      RETURN ENUM_TO_INT(CM_VEH_ISSI7) 			BREAK
		CASE 14      RETURN ENUM_TO_INT(CM_VEH_ITALIGTO)		BREAK
		CASE 15      RETURN ENUM_TO_INT(CM_VEH_YOSEMITE3)		BREAK		
		CASE 16      RETURN ENUM_TO_INT(CM_VEH_LYNX) 			BREAK
		CASE 17      RETURN ENUM_TO_INT(CM_VEH_NIGHTSHADE) 		BREAK
		CASE 18      RETURN ENUM_TO_INT(CM_VEH_PENUMBRA2) 	    BREAK
		CASE 19      RETURN ENUM_TO_INT(CM_VEH_RAPIDGT3) 		BREAK
		CASE 20      RETURN ENUM_TO_INT(CM_VEH_RETINUE2) 		BREAK
		CASE 21      RETURN ENUM_TO_INT(CM_VEH_SCHLAGEN)		BREAK
		CASE 22      RETURN ENUM_TO_INT(CM_VEH_SLAMVAN3) 		BREAK
		CASE 23      RETURN ENUM_TO_INT(CM_VEH_SUGOI) 			BREAK	
	ENDSWITCH
	RETURN -1
ENDFUNC
	
FUNC INT GET_RANDOM_VEHICLE_WITH_TRUNK_SPEAKERS()
	INT i = GET_RANDOM_INT_IN_RANGE(0,5)
	  SWITCH  i
		CASE 0       RETURN ENUM_TO_INT(CM_VEH_BUCCANEER2) 		BREAK
		CASE 1       RETURN ENUM_TO_INT(CM_VEH_FACTION3) 		BREAK
		CASE 2       RETURN ENUM_TO_INT(CM_VEH_MANANA2) 	    BREAK
		CASE 3       RETURN ENUM_TO_INT(CM_VEH_MOONBEAM2) 		BREAK
		CASE 4       RETURN ENUM_TO_INT(CM_VEH_SABREGT2) 		BREAK
	ENDSWITCH
	RETURN -1
ENDFUNC



/// PURPOSE:
///    SETS SPECIAL PROPERTies 
PROC SET_AI_VEHICLE_RANDOM_PROPERTIES(AI_VEHICLE_DATA &sVehicleData)
	INT i
	
	INT iNumberOfVehiclesWithHydraulics = 0
	
	INT iRevingEngine[ciCAR_MEET_NUMBER_OF_REVING_ENGINE_CARS]
	
	iRevingEngine[0] = GET_VEHICLE_WITH_LIGHTS_ON(GET_RANDOM_INT_IN_RANGE(0,8))
	iRevingEngine[1] = GET_VEHICLE_WITH_LIGHTS_ON(GET_RANDOM_INT_IN_RANGE(0,8))
	iRevingEngine[2] = GET_VEHICLE_WITH_LIGHTS_ON(GET_RANDOM_INT_IN_RANGE(0,8))
	
	WHILE ((iRevingEngine[1] = iRevingEngine[0]) OR (iRevingEngine[2] = iRevingEngine[0]) OR (iRevingEngine[1] = iRevingEngine[2]))
		iRevingEngine[1] = GET_VEHICLE_WITH_LIGHTS_ON(GET_RANDOM_INT_IN_RANGE(0,8))
		iRevingEngine[2] = GET_VEHICLE_WITH_LIGHTS_ON(GET_RANDOM_INT_IN_RANGE(0,8))
	ENDWHILE
	
	REPEAT ciCAR_MEET_NUMER_OF_AI_VEHICLES i
		
	//DANCING HYDRAULICS
		IF DOES_VEHICLE_HAVE_HYDRAULICS(sVehicleData.sAIVehicle[i].eModel)
		AND NOT IS_INDEX_OF_A_SPECIAL_LOCATION(i)			
		AND i != iRevingEngine[0]
		AND i != iRevingEngine[1]
		AND i != iRevingEngine[2]
			iNumberOfVehiclesWithHydraulics++
		ENDIF		
		
		//VEHICLES THAT CAN BE ROTATED
		IF CAN_VEHICLE_BE_ROTATED_BY_180(i)
			INT iChance = GET_RANDOM_INT_IN_RANGE(1,101)
		
			IF iChance <= 20 //20% chance to rotate
				SET_BITMASK_ENUM_AS_ENUM(sVehicleData.sAIVehicle[i].eSpecialProperty, AI_VEHICLE_SPECIAL_PROPERTY_ROTATE_180)
				PRINTLN("[MP_CAR_MEET_AI_VEHICLES][SET_AI_VEHICLE_ROTATE_180_PROPERTY] VEHICLE CHOSEN: ", i )
			ENDIF	
		ENDIF	
		
		//REVING ENGINE PEDS
		IF i = iRevingEngine[0]
		OR i = iRevingEngine[1]
		OR i = iRevingEngine[2]
			SET_BITMASK_ENUM_AS_ENUM(sVehicleData.sAIVehicle[i].eSpecialProperty, AI_VEHICLE_SPECIAL_PROPERTY_REVING_PED)
			PRINTLN("[MP_CAR_MEET_AI_VEHICLES][SET_AI_VEHICLE_RANDOM_PROPERTIES] AI_VEHICLE_SPECIAL_PROPERTY_REVING_PED on i: ", i)
		ENDIF
	ENDREPEAT

	IF iNumberOfVehiclesWithHydraulics>0
		iNumberOfVehiclesWithHydraulics = GET_RANDOM_INT_IN_RANGE(0, iNumberOfVehiclesWithHydraulics)
		REPEAT ciCAR_MEET_NUMER_OF_AI_VEHICLES i
			IF DOES_VEHICLE_HAVE_HYDRAULICS(sVehicleData.sAIVehicle[i].eModel)
			AND NOT IS_INDEX_OF_A_SPECIAL_LOCATION(i)	
			AND i != iRevingEngine[0]
			AND i != iRevingEngine[1]
			AND i != iRevingEngine[2]
				iNumberOfVehiclesWithHydraulics--
				IF iNumberOfVehiclesWithHydraulics<0
					SET_BITMASK_ENUM_AS_ENUM(sVehicleData.sAIVehicle[i].eSpecialProperty, AI_VEHICLE_SPECIAL_PROPERTY_HYDRAULICS_DANCE)
					BREAKLOOP
				ENDIF
				
			ENDIF
		ENDREPEAT
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Host of the script / first person to enter the interior will generate a semi-random list of vehicles to be used as an archetype for spawning cars for all players
/// PARAMS:
////   eState - supplied from the main script to ensure other players don't attempt co create the list and wait for the host to supply one
///    sVehicleData - array of vehicles shared with all players so all models and variations are synchronised despite being randomised and local
PROC CREATE_VEHICLE_LIST(CAR_MEET_AI_VEHICLE_LIST_CREATION_STATE &eState, AI_VEHICLE_DATA &sVehicleData)
	INT i, j
	INT iModelsToSpawn[NUM_OF_UNIQUE_VEHICLE_MODELS] 
	BOOL bIsModelAlreadyPresent = FALSE
	INT iModelIndex
	REPEAT NUM_OF_UNIQUE_VEHICLE_MODELS i
	
		//PICK A RANDOM MODEL INDEX FROM AVAILABLE POOL		
		IF i<3 //FIRST ASSIGN Three MODELS WITH FRONTAL ENGINE
			iModelIndex = GET_RANDOM_VEHICLE_WITH_FRONT_ENGINE()
		ELIF i < 4 // SEDOND ASSING ONE MODEL WITH BOOT SPEAKERS
			iModelIndex = GET_RANDOM_VEHICLE_WITH_TRUNK_SPEAKERS()
		ELSE // REMAINING MODELS ARE RANDOM
			iModelIndex = GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CM_VEH_COUNT))
		ENDIF
		
		//CHECK IF THE MODEL INDEX IS AREADY ON THE LIST (we only need to check from 0 to i, as other spaces in the array are not filled yet)
		bIsModelAlreadyPresent = FALSE
		j = 0
		REPEAT i j
			IF iModelIndex = iModelsToSpawn[j]
				bIsModelAlreadyPresent = TRUE
			ENDIF
		ENDREPEAT
			
		IF bIsModelAlreadyPresent//IF THE MODEL IS ALREADY ON THE LIST DONT INREASE COUNTER
			i--
			RELOOP	
		ELSE 			//ONLY IF THE MODEL IS NOT ON THE LIST THEN WE CAN SAFELY ADD IT
			iModelsToSpawn[i] = iModelIndex
		ENDIF	
	ENDREPEAT
	
	//Need at least one model with boot speakers
	CHECK_FOR_AT_LEAST_ONE_VEHICLE_WITH_BOOT_SPEAKERS(iModelsToSpawn)
	
	//Need at least one model with front engine for some ped animations in the car meet proprty
	CHECK_FOR_AT_LEAST_ONE_VEHICLE_WITH_FRONT_ENGINE(iModelsToSpawn)
	
	//AFTER ACQUIRING A LIST OF 10 UNIQUE MODEL INICES WE POPULATE AI_VEHICLE_DATA array form AM_MP_CAR_MEET_PROPERTY 
	// with 5 of each model each with a different variation index
	REPEAT NUM_OF_UNIQUE_VEHICLE_MODELS i		
		REPEAT NUM_OF_VEHICLE_VARIANTS j
		 	sVehicleData.sAIVehicle[i*5+j].eModel =  INT_TO_ENUM(CAR_MEET_AI_VEHICLE_ENUM, iModelsToSpawn[i])
			sVehicleData.sAIVehicle[i*5+j].iVariation = j
		ENDREPEAT
	ENDREPEAT
	
	SHUFFLE_VEHICLE_LIST(sVehicleData, ciCAR_MEET_NUMER_OF_AI_VEHICLES)	//WE MAKE SURE LIST ORDER IS RANDOMIZED SO VEHICLES WITH THE SAME MODEL DONT APPEAR NEXT TO ONE ANOTHER
	
	//reorders the list to ensure that front engine models are at locations with peds watching them
	POSITION_FRONT_ENGINE_VEHICLE_MODELS_FOR_PED_ANIMATIONS(sVehicleData)
	//reorders the list to ensure that models with boot speakers are at locations with peds listening to music (makes sure not to touch front engine vehs)
	POSITION_VEHICLES_WITH_BOOT_SPEAKERS_FOR_PED_ANIMATIONS(sVehicleData)
	
	//reorders the list to ensure that vehs with wonky headlights don't appear in spots that have headlights on by default. (ELLEGY, TORRERO, GAUNTLET5)
	POSITION_VEHICLES_WITHOUT_HEADLIGHTS_AWAY_FROM_HEADLIGHT_SPOTS(sVehicleData)
	
	//20% chance(per vehicle) to rotate certain vehicles 180 degrees
	SET_AI_VEHICLE_RANDOM_PROPERTIES(sVehicleData)
	
	sVehicleData.sAIVehicle[g_sCarMeetSpecialPedPositionData.iVehicleIndex[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS]].eModel = CM_VEH_INVALID
	sVehicleData.sAIVehicle[g_sCarMeetSpecialPedPositionData.iVehicleIndex[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1]].eModel = CM_VEH_INVALID
	eState = CAR_MEET_AI_VEHICLE_LIST_DONE
ENDPROC

PROC CREATE_SANDBOX_VEHICLE_LIST(CAR_MEET_AI_VEHICLE_LIST_CREATION_STATE &eState, AI_VEHICLE_DATA &sVehicleData)
	INT i, j
	INT iModelsToSpawn[NUM_OF_UNIQUE_VEHICLE_MODELS] 
	BOOL bIsModelAlreadyPresent = FALSE
	INT iModelIndex
	REPEAT NUM_OF_UNIQUE_VEHICLE_MODELS i
	
		iModelIndex = GET_RANDOM_INT_IN_RANGE(0, ENUM_TO_INT(CM_VEH_COUNT))
		
		
		//CHECK IF THE MODEL INDEX IS AREADY ON THE LIST (we only need to check from 0 to i, as other spaces in the array are not filled yet)
		bIsModelAlreadyPresent = FALSE
		j = 0
		REPEAT i j
			IF iModelIndex = iModelsToSpawn[j]
				bIsModelAlreadyPresent = TRUE
			ENDIF
		ENDREPEAT
			
		IF bIsModelAlreadyPresent//IF THE MODEL IS ALREADY ON THE LIST DONT INREASE COUNTER
			i--
			RELOOP	
		ELSE 			//ONLY IF THE MODEL IS NOT ON THE LIST THEN WE CAN SAFELY ADD IT
			iModelsToSpawn[i] = iModelIndex
		ENDIF	
	ENDREPEAT
	
	REPEAT NUM_OF_UNIQUE_VEHICLE_MODELS i		
		REPEAT (NUM_OF_VEHICLE_VARIANTS-2) j ///adding only 3 variants per model as there is only 28 ai vehs while in sandbox
			IF (i*3+j) < ciCAR_MEET_NUMBER_OF_SANDBOX_AMBIENT_VEHICLES
		 		sVehicleData.sAIVehicle[i*3+j].eModel =  INT_TO_ENUM(CAR_MEET_AI_VEHICLE_ENUM, iModelsToSpawn[i])
				sVehicleData.sAIVehicle[i*3+j].iVariation = j
			ELSE
				BREAKLOOP
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	SHUFFLE_VEHICLE_LIST(sVehicleData, ciCAR_MEET_NUMBER_OF_SANDBOX_AMBIENT_VEHICLES)	//WE MAKE SURE LIST ORDER IS RANDOMIZED SO VEHICLES WITH THE SAME MODEL DONT APPEAR NEXT TO ONE ANOTHER
	
	eState = CAR_MEET_AI_VEHICLE_LIST_DONE
ENDPROC
/// PURPOSE:
///    Boots of 5 cars at specific locations will have speakers in them to play music
PROC OPEN_RELEVANT_CAR_BOOTS(CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData)
	INT i 
	FOR i = 0 TO 4
		IF IS_ENTITY_ALIVE(sAIVehicleCreationData.viVehicles[GET_TRUNK_SPEAKERS_INDEX(i)])
			SET_VEHICLE_ENGINE_ON(sAIVehicleCreationData.viVehicles[GET_TRUNK_SPEAKERS_INDEX(i)], true, true)
			SET_VEHICLE_DOOR_OPEN(sAIVehicleCreationData.viVehicles[GET_TRUNK_SPEAKERS_INDEX(i)], SC_DOOR_BOOT)
			SET_VEHICLE_LIGHTS(sAIVehicleCreationData.viVehicles[GET_TRUNK_SPEAKERS_INDEX(i)], FORCE_VEHICLE_LIGHTS_ON)
			SET_VEHICLE_RADIO_ENABLED(sAIVehicleCreationData.viVehicles[GET_TRUNK_SPEAKERS_INDEX(i)], FALSE)
		ENDIF
	ENDFOR	
ENDPROC

FUNC BOOL IS_MIMIS_TRUNK_OPEN(CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData)
	IF IS_ENTITY_ALIVE(sAIVehicleCreationData.viSpecialPedVehicles[ENUM_TO_INT(PED_MIMI)])
		RETURN (IS_VEHICLE_DOOR_FULLY_OPEN(sAIVehicleCreationData.viSpecialPedVehicles[ENUM_TO_INT(PED_MIMI)], SC_DOOR_BOOT))
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC SET_MIMI_VEHICLE_SETTINGS(CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData)
	///OPENING SPECIAL PED MIMI'S VAN TRUNK
	IF IS_ENTITY_ALIVE(sAIVehicleCreationData.viSpecialPedVehicles[ENUM_TO_INT(PED_MIMI)])
		SET_VEHICLE_DOOR_OPEN(sAIVehicleCreationData.viSpecialPedVehicles[ENUM_TO_INT(PED_MIMI)], SC_DOOR_BOOT, FALSE, TRUE)
		SET_VEHICLE_LIGHTS(sAIVehicleCreationData.viSpecialPedVehicles[ENUM_TO_INT(PED_MIMI)], FORCE_VEHICLE_LIGHTS_ON)
		FREEZE_ENTITY_POSITION(sAIVehicleCreationData.viSpecialPedVehicles[ENUM_TO_INT(PED_MIMI)], TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Hoods of 5 cars at specific locations will be opened with peds gathered around them , watching the engine
PROC OPEN_RELEVENT_CAR_HOODS(CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData)
	
	INT i
	FOR i = 0 TO NUM_VEHICLES_WITH_OPEN_HOOD-1
		IF IS_ENTITY_ALIVE(sAIVehicleCreationData.viVehicles[GET_FRONT_ENGINE_INDEX(i)])
			IF DOES_VEHICLE_HAVE_DOOR(sAIVehicleCreationData.viVehicles[GET_FRONT_ENGINE_INDEX(i)], SC_DOOR_BONNET)
				SET_VEHICLE_DOOR_OPEN(sAIVehicleCreationData.viVehicles[GET_FRONT_ENGINE_INDEX(i)], SC_DOOR_BONNET)
			ENDIF
		ENDIF
	ENDFOR

ENDPROC
PROC MAINTAIN_RACE_ORGANISER_VEHICLE(CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData)
	PRINTLN("MAINTAIN_RACE_ORGANISER_VEHICLE")
	IF NOT DOES_ENTITY_EXIST(sAIVehicleCreationData.viSpecialPedVehicles[0])	
		VEHICLE_SETUP_STRUCT_MP sData	
		VECTOR vPosition
		FLOAT fHeading
		GET_SPECIAL_PED_VEHICLE_DATA(PED_RACE_ORGANIZER, sData)
		vPosition = GET_SPECIAL_PED_VEHICLE_POSITION(PED_RACE_ORGANIZER)
		fHeading  = GET_SPECIAL_PED_VEHICLE_HEADING(PED_RACE_ORGANIZER)
		PRINTLN("MAINTAIN_RACE_ORGANISER_VEHICLE CREATING VEH")
		sAIVehicleCreationData.viSpecialPedVehicles[0] = CREATE_VEHICLE(sData.VehicleSetup.eModel, vPosition, fHeading, FALSE)
		SET_VEHICLE_SETUP_MP(sAIVehicleCreationData.viSpecialPedVehicles[0], sData)			
		SET_VEHICLE_DOORS_LOCKED(sAIVehicleCreationData.viSpecialPedVehicles[0], VEHICLELOCK_CANNOT_ENTER)
		SET_MODEL_AS_NO_LONGER_NEEDED(sData.VehicleSetup.eModel)
	ENDIF
ENDPROC

FUNC BOOL CREATE_SPECIAL_PED_VEHICLES(CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData, AI_VEHICLE_DATA &sVehicleData)

	INT i = 0
	VEHICLE_SETUP_STRUCT_MP sData	
	VECTOR vPosition
	FLOAT fHeading
	BOOL bWereVehiclesSkipped = FALSE
	REPEAT ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS i	
	
		//IF CULLING IS ENABLED SKIP VEHICLES THAT HAVE CULLING LEVEL LOWER THAN CURRENT CULLING LEVEL
		IF g_sMPTunables.iTUNER_CARMEET_ENABLE_AMBIENT_VEHICLE_CULLING
			IF NOT DOES_SPECIAL_PED_HAVE_PREFEDINED_LOCATION(g_sCarMeetSpecialPedPositionData.ePedsToSpawn[i])
				IF sVehicleData.iCullingLevel<GET_AI_VEHICLE_CULLING_LEVEL(g_sCarMeetSpecialPedPositionData.iVehicleIndex[i])
					PRINTLN("[AM_MP_CAR_MEET_PROPERTY][MP_CAR_MEET_AI_VEHICLES] DIDN'T CREATE SPECIAL PED VEHICLE ID: ",i," AT ", vPosition, " POSITION IS CULLED")					
					RELOOP
				ENDIF
			ENDIF
		ENDIF	
		
		//CULLION OF ADDITIONAL VEHICLES
		IF g_sMPTunables.iTUNER_CARMEET_NUMBER_OF_ADDITIONAL_VEHICLES_TO_CULL >= GET_AI_VEHICLE_ADDITIONAL_CULLING_PRIORITY(g_sCarMeetSpecialPedPositionData.iVehicleIndex[i])
			PRINTLN("[AM_MP_CAR_MEET_PROPERTY][MP_CAR_MEET_AI_VEHICLES] DIDN'T CREATE SPECIAL PED VEHICLE ID: ",i," AT ", vPosition, " POSITION IS CULLED")	
			RELOOP
		ENDIF
	
		IF NOT DOES_ENTITY_EXIST(sAIVehicleCreationData.viSpecialPedVehicles[i])	
			GET_SPECIAL_PED_VEHICLE_DATA(INT_TO_ENUM(CAR_MEET_SPECIAL_PED_VEHICLES_ENUM, g_sCarMeetSpecialPedPositionData.ePedsToSpawn[i]), sData)
			
			IF g_sCarMeetSpecialPedPositionData.ePedsToSpawn[i] = PED_MOODYMAN				
				sAIVehicleCreationData.iMoodymanIndex = i
			ENDIF
			IF REQUEST_LOAD_MODEL(sData.VehicleSetup.eModel)
				IF DOES_SPECIAL_PED_HAVE_PREFEDINED_LOCATION(g_sCarMeetSpecialPedPositionData.ePedsToSpawn[i])
					vPosition = GET_SPECIAL_PED_VEHICLE_POSITION(INT_TO_ENUM(CAR_MEET_SPECIAL_PED_VEHICLES_ENUM, i))
					fHeading  = GET_SPECIAL_PED_VEHICLE_HEADING(INT_TO_ENUM(CAR_MEET_SPECIAL_PED_VEHICLES_ENUM, i))
				ELSE
					vPosition = GET_AI_VEHICLE_POSITON(g_sCarMeetSpecialPedPositionData.iVehicleIndex[i])
					fHeading = GET_AI_VEHICLE_HEADING(g_sCarMeetSpecialPedPositionData.iVehicleIndex[i])
				ENDIF
				
				//WE ONLY CHECK VISIBILITY IF CREATING VEHICLES AFTER THEY WERE CULLED
				IF b_SHOULD_CHECK_FOR_VISIBILITTY
				AND IS_SPHERE_VISIBLE(vPosition, 3.5)
				//THIS ENSURES THAT WE DON'T HOLD SPAWNING FOR ONE VEHICLE, BUT THIS FRAME CONTINUE TO THE NEXT, WILL TRY AGAIN NEXT FRAME
					bWereVehiclesSkipped = TRUE
					RELOOP
				ENDIF
				
				IF IS_ANY_VEHICLE_NEAR_POINT(vPosition, 0.5)
					PRINTLN("[AM_MP_CAR_MEET_PROPERTY][MP_CAR_MEET_AI_VEHICLES] CAN'T CREATE SPECIAL PED VEHICLE ID: ",i," AT ", vPosition, " THERE'S A VEHICLE ALREADY THERE")
					bWereVehiclesSkipped = TRUE
					RELOOP
				ENDIF
				sAIVehicleCreationData.viSpecialPedVehicles[i] = CREATE_VEHICLE(sData.VehicleSetup.eModel, vPosition, fHeading, FALSE)
				
				SET_VEHICLE_SETUP_MP(sAIVehicleCreationData.viSpecialPedVehicles[i], sData)				
				SET_VEHICLE_DOORS_LOCKED(sAIVehicleCreationData.viSpecialPedVehicles[i], VEHICLELOCK_CANNOT_ENTER)				
				#IF IS_DEBUG_BUILD 
				IF GET_COMMANDLINE_PARAM_EXISTS("ForceCarMeetHeadlights") 
					//SET_VEHICLE_LIGHTS(sAIVehicleCreationData.viSpecialPedVehicles[i], FORCE_VEHICLE_LIGHTS_ON)
					SET_VEHICLE_ENGINE_ON(sAIVehicleCreationData.viSpecialPedVehicles[i], TRUE, TRUE)
					SET_VEHICLE_LIGHTS(sAIVehicleCreationData.viSpecialPedVehicles[i], SET_VEHICLE_LIGHTS_ON)
				ENDIF	
				#ENDIF
				IF SHOULD_VEHICLE_HAVE_LIGHTS_ON(g_sCarMeetSpecialPedPositionData.iVehicleIndex[i])
					SET_VEHICLE_LIGHTS(sAIVehicleCreationData.viSpecialPedVehicles[i], FORCE_VEHICLE_LIGHTS_ON)
				ENDIF	
				
				SET_ENTITY_INVINCIBLE(sAIVehicleCreationData.viSpecialPedVehicles[i], TRUE)
				SET_ENTITY_CAN_BE_DAMAGED(sAIVehicleCreationData.viSpecialPedVehicles[i], FALSE)
				SET_VEHICLE_FIXED(sAIVehicleCreationData.viSpecialPedVehicles[i])
				SET_ENTITY_HEALTH(sAIVehicleCreationData.viSpecialPedVehicles[i], 1000)
				SET_VEHICLE_ENGINE_HEALTH(sAIVehicleCreationData.viSpecialPedVehicles[i], 1000)
				SET_VEHICLE_PETROL_TANK_HEALTH(sAIVehicleCreationData.viSpecialPedVehicles[i], 1000)
				SET_VEHICLE_DIRT_LEVEL(sAIVehicleCreationData.viSpecialPedVehicles[i], 0.0)
				SET_VEHICLE_ON_GROUND_PROPERLY(sAIVehicleCreationData.viSpecialPedVehicles[i])		
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sAIVehicleCreationData.viSpecialPedVehicles[i], TRUE)
				
				
				SET_MODEL_AS_NO_LONGER_NEEDED(sData.VehicleSetup.eModel)
				PRINTLN("[AM_MP_CAR_MEET_PROPERTY][MP_CAR_MEET_AI_VEHICLES] CREATING SPECIAL PED VEHICLE VEHICLE ID: ",i," AT ", vPosition)
				RETURN FALSE //TO MAKE SURE WE CREATE ONE MODEL PER FRAME
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
		
	//opens trunk + turns on the headlights
	SET_MIMI_VEHICLE_SETTINGS(sAIVehicleCreationData)

	IF NOT bWereVehiclesSkipped
		RETURN TRUE //ALL CARS SPAWNED
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC MAINTAIN_ENGINE_REVING(CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData)
	INT i
 	REPEAT ciCAR_MEET_NUMBER_OF_REVING_ENGINE_CARS i
		IF IS_ENTITY_ALIVE(sAIVehicleCreationData.piRevingEnginePeds[i])
			IF IS_PED_IN_ANY_VEHICLE(sAIVehicleCreationData.piRevingEnginePeds[i])
				VEHICLE_INDEX viVeh = GET_VEHICLE_PED_IS_IN(sAIVehicleCreationData.piRevingEnginePeds[i])
				IF IS_VEHICLE_DRIVEABLE(viVeh)	
					IF HAS_NET_TIMER_EXPIRED(sAIVehicleCreationData.stRevingEngineTimers[i], sAIVehicleCreationData.iRevingEngineActualDelay[i])
						PRINTLN("[MP_CAR_MEET_AI_VEHICLES][MAINTAIN_ENGINE_REVING] TIMER EXPIRED STARTING REV")
						sAIVehicleCreationData.iRevingEngineActualDelay[i] = GET_RANDOM_INT_IN_RANGE(ciCAR_MEET_ENGINE_REV_MIN_DELAY, ciCAR_MEET_ENGINE_REV_MAX_DELAY)
						RESET_NET_TIMER(sAIVehicleCreationData.stRevingEngineTimers[i])
						TASK_VEHICLE_TEMP_ACTION(sAIVehicleCreationData.piRevingEnginePeds[i], viVeh, TEMPACT_REV_ENGINE, GET_RANDOM_INT_IN_RANGE(ciCAR_MEET_ENGINE_REV_MIN_LENGTH, ciCAR_MEET_ENGINE_REV_MAX_LENGTH))
					ENDIF
					
					IF NOT HAS_NET_TIMER_STARTED(sAIVehicleCreationData.stRevingEngineTimers[i])	
						PRINTLN("[MP_CAR_MEET_AI_VEHICLES][MAINTAIN_ENGINE_REVING] TIMER NOT STARTED. Starting timer")
						sAIVehicleCreationData.iRevingEngineActualDelay[i] = GET_RANDOM_INT_IN_RANGE(ciCAR_MEET_ENGINE_REV_MIN_DELAY, ciCAR_MEET_ENGINE_REV_MAX_DELAY)
						START_NET_TIMER(sAIVehicleCreationData.stRevingEngineTimers[i])
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC
PROC MAINTAIN_DANCING_HYDRAULICS(CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData)
	IF IS_BIT_SET(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_HYDRAULICS_VEHICLE_PRESENT)
		IF IS_ENTITY_ALIVE(sAIVehicleCreationData.piDancingHydraulicsPed)
			IF IS_PED_IN_ANY_VEHICLE(sAIVehicleCreationData.piDancingHydraulicsPed)
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(sAIVehicleCreationData.piDancingHydraulicsPed))	
				sAIVehicleCreationData.sHydraulics.iHydraulicDanceTimer++
				sAIVehicleCreationData.sHydraulics.iHydraulicDanceDowntimeTimer++
				PROCESS_HYDRAULIC_DANCING(sAIVehicleCreationData)
			ENDIF
		ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL CREATE_HYDRAULICS_DANCING_PED(CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData)
	IF IS_BIT_SET(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_HYDRAULICS_VEHICLE_PRESENT)
		IF NOT DOES_ENTITY_EXIST(sAIVehicleCreationData.piDancingHydraulicsPed)
			MODEL_NAMES eModel
			eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_CarClub_01"))	
			IF REQUEST_LOAD_MODEL(eModel)
				VEHICLE_INDEX vehID
				vehID = sAIVehicleCreationData.viVehicles[sAIVehicleCreationData.sHydraulics.iHydraulicsDanceIndex]
				IF IS_ENTITY_ALIVE(vehID)			
				AND IS_VEHICLE_SEAT_FREE(vehID, VS_DRIVER)
					sAIVehicleCreationData.piDancingHydraulicsPed = CREATE_PED_INSIDE_VEHICLE(vehID, PEDTYPE_CIVMALE, eModel, VS_DRIVER, FALSE, FALSE)
					PRINTLN("[MP_CAR_MEET_AI_VEHICLES][CREATE_HYDRAULICS_DANCING_PED] CREATED DANCING HYDRAULICS PED.")					
					RETURN FALSE
				ELSE
					PRINTLN("[MP_CAR_MEET_AI_VEHICLES][CREATE_HYDRAULICS_DANCING_PED] FAILED TO CREATE PED. VEHICLE SEAT IS ALREADY TAKEN OR ENTITY NOT ALIVE. ABANDONING")
					CLEAR_BIT(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_HYDRAULICS_VEHICLE_PRESENT)
					RETURN FALSE
				ENDIF				
			ELSE
				PRINTLN("[MP_CAR_MEET_AI_VEHICLES][CREATE_HYDRAULICS_DANCING_PED] FAILED TO CREATE PED. Model not loaded")
			ENDIF
			
		ELSE		
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

FUNC MODEL_NAMES GET_TRUNK_PROP_MODEL(INT index)
	SWITCH index
	
		CASE 0
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_bag_djlp_01a"))
		BREAK
		
		CASE 1
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_tool_box_04"))
		BREAK
		
		CASE 2
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("prop_cs_cardbox_01"))
		BREAK
		
		CASE 3
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("h4_prop_h4_lp_01a"))
		BREAK
		
		CASE 4
			RETURN INT_TO_ENUM(MODEL_NAMES, HASH("tr_prop_tr_usb_drive_02a"))
		BREAK
	ENDSWITCH
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC
FUNC VECTOR GET_TRUNK_PROP_POSITION(INT index, VECTOR vPosition, VECTOR vRotation)
	
	VECTOR vOffset
	SWITCH index
		CASE 0
			vOffset = <<-0.35, -1.70 , 0.2>>
			ROTATE_VECTOR_FMMC(vOffset, vRotation)
			RETURN vPosition + vOffset	
		CASE 1
			vOffset = <<0.25, -1.65 , 0.2>>
			ROTATE_VECTOR_FMMC(vOffset, vRotation)
			RETURN vPosition + vOffset	
		CASE 2
			vOffset = <<0.17, -2.1 , 0.4>>
			ROTATE_VECTOR_FMMC(vOffset, vRotation)
			RETURN vPosition + vOffset	
		CASE 3
			vOffset = <<0.2, -2.15 , 0.585>>
			ROTATE_VECTOR_FMMC(vOffset, vRotation)
			RETURN vPosition + vOffset		
		BREAK	
		CASE 4
			vOffset = <<0.3, -2.15 , 0.595>>
			ROTATE_VECTOR_FMMC(vOffset, vRotation)
			RETURN vPosition + vOffset		
		BREAK	
	ENDSWITCH
	RETURN <<0.0,0.0,0.0>>
ENDFUNC
FUNC VECTOR GET_TRUNK_PROP_OFFSET(INT index)
	
	SWITCH index
		CASE 0
			RETURN  <<-0.35, -1.70 , 0.2>>
		CASE 1
			RETURN <<0.25, -1.65 , 0.2>>
		CASE 2
			RETURN <<0.17, -2.1 , 0.4>>			
		CASE 3
			RETURN <<0.2, -2.15 , 0.585>>			
		BREAK	
		CASE 4
			RETURN <<0.3, -2.15 , 0.595>>		
		BREAK	
	ENDSWITCH
	RETURN <<0.0,0.0,0.0>>
ENDFUNC

FUNC VECTOR GET_TRUNK_PROP_ROTATION(int index , VECTOR vRotation)

	VECTOR vAdjustedRotation = <<0,0,0>>
	
	SWITCH index	
		CASE 0
			vAdjustedRotation = (vRotation + <<0.0  ,0.0  ,40.0>>)
		BREAK
		
		CASE 1
			vAdjustedRotation = (vRotation + <<0.0  ,0.0  ,-110.0>>)
		BREAK
		
		CASE 2
			vAdjustedRotation = (vRotation + <<0.0  ,0.0  , 10.0>>)
		BREAK
		
		CASE 3
			vAdjustedRotation = (vRotation + <<0.0  ,0.0  , 25.0>>)
		BREAK
		
		CASE 4
			 vAdjustedRotation = (vRotation + <<0.0  ,0.0  , 55.0>>)
		BREAK		
	ENDSWITCH
	
	//KEEP Z between -180 and 180
	IF vAdjustedRotation.z > 180.0
		vAdjustedRotation.z -=360.0
	ELIF vAdjustedRotation.z < -180.0
		vAdjustedRotation.z +=360.0
	ENDIF
	
	RETURN vAdjustedRotation
ENDFUNC
FUNC BOOL CAN_PLAYER_COLLECT_USB(ENTITY_INDEX object_item, BOOL bskip_heading_check,BOOL bskip_weapon_check = FALSE )
	IF  NOT IS_PHONE_ONSCREEN() 
	AND NOT IS_CELLPHONE_CAMERA_IN_USE() 
	AND NOT IS_BROWSER_OPEN()	
	AND NOT IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT (IS_PLAYER_FREE_AIMING(PLAYER_ID())	AND NOT bskip_weapon_check)
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_ENTITY_ALIVE(object_item)
	AND IS_PLAYER_FACING_ENTITY(object_item, bskip_heading_check)
	AND NOT IS_PED_RUNNING(PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_PHONE_ONSCREEN()
		PRINTLN("CAN_PLAYER_OPEN_COLLECTABLE IS_PHONE_ONSCREEN")
	ENDIF	
	IF IS_CELLPHONE_CAMERA_IN_USE() 
		PRINTLN("CAN_PLAYER_OPEN_COLLECTABLE IS_CELLPHONE_CAMERA_IN_USE")
	ENDIF
	IF IS_BROWSER_OPEN()
		PRINTLN("CAN_PLAYER_OPEN_COLLECTABLE IS_BROWSER_OPEN")
	ENDIF
	IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)
		PRINTLN("CAN_PLAYER_OPEN_COLLECTABLE IS_HUD_COMPONENT_ACTIVE")
	ENDIF
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		PRINTLN("CAN_PLAYER_OPEN_COLLECTABLE IS_PED_IN_ANY_VEHICLE")
	ENDIF
	IF IS_PLAYER_FREE_AIMING(PLAYER_ID())	
		PRINTLN("CAN_PLAYER_OPEN_COLLECTABLE IS_PLAYER_FREE_AIMING")
	ENDIF
	IF IS_PLAYER_FACING_ENTITY(object_item, bskip_heading_check)
		PRINTLN("CAN_PLAYER_OPEN_COLLECTABLE IS_PLAYER_FACING_ENTITY")
	ENDIF
	IF NOT IS_PED_RUNNING(PLAYER_PED_ID())
		PRINTLN("CAN_PLAYER_OPEN_COLLECTABLE IS_PED_RUNNING")
	ENDIF
	#ENDIF 
	
	RETURN FALSE
ENDFUNC
FUNC BOOL IS_GET_USB_BUTTON_PRESSED()	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
	OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
		PRINTLN("[KW Collectables] IS_ACKNOWLEDGE_BUTTON_PRESSED() TRUE")
		RETURN TRUE
	ENDIF		
	RETURN FALSE
ENDFUNC
PROC MAINTAIN_COLLECTIBLE_PROMPT_FOR_USB(CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData)//)(VECTOR vPosition, VECTOR vRotation)
	IF DOES_ENTITY_EXIST(sAIVehicleCreationData.oiTrunkProps[4])
	STRING sAnimDict = "anim@scripted@player@freemode@tun_prep_grab_midd_ig3@male@"
		IF REQUEST_AND_LOAD_ANIM_DICT(sAnimDict)
			STRING sClip = "tun_prep_grab_midd_ig3"
			VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			VECTOR vCollectiblePos = GET_ENTITY_COORDS(sAIVehicleCreationData.oiTrunkProps[4])
			IF NOT IS_BIT_SET(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_MOODYMAN_TRUNK_HELP_DISPLAYED)
			AND CAN_PLAYER_COLLECT_USB(sAIVehicleCreationData.viSpecialPedVehicles[sAIVehicleCreationData.iMoodymanIndex], FALSE)
				IF  GET_DISTANCE_BETWEEN_COORDS(vPlayerPos,vCollectiblePos )<= 1.5
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP(TRUE)
					ENDIF
					PRINT_HELP_FOREVER("CONTEXT_MOODY")   //Press ~D-Pad Right~ to collect the weapon components.
					SET_BIT(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_MOODYMAN_TRUNK_HELP_DISPLAYED)
				ENDIF
			ELIF (GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vCollectiblePos)> 1.5
			OR NOT CAN_PLAYER_COLLECT_USB(sAIVehicleCreationData.viSpecialPedVehicles[sAIVehicleCreationData.iMoodymanIndex], FALSE ))
			AND IS_BIT_SET(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_MOODYMAN_TRUNK_HELP_DISPLAYED)
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CONTEXT_MOODY")
							CLEAR_HELP()
						ENDIF
					ENDIF
					CLEAR_BIT(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_MOODYMAN_TRUNK_HELP_DISPLAYED)
					
			ENDIF
			
			IF  GET_DISTANCE_BETWEEN_COORDS(vPlayerPos, vCollectiblePos)<= 1.5
			AND CAN_PLAYER_COLLECT_USB(sAIVehicleCreationData.viSpecialPedVehicles[sAIVehicleCreationData.iMoodymanIndex], FALSE)
				MAINTAIN_COLLECTABLE_RUMBLE(vPlayerPos, vCollectiblePos, sAIVehicleCreationData.rumbleTimer)
				IF IS_GET_USB_BUTTON_PRESSED()								
				AND NOT IS_BIT_SET(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_MOODYMAN_TRUNK_COLLECTION_STARTED)
					IF (GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PLAY_ANIM) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_PLAY_ANIM) != WAITING_TO_START_TASK)
						SET_BIT(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_MOODYMAN_TRUNK_COLLECTION_STARTED)
						TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDict, sClip)				
					ENDIF			
				ENDIF
			ENDIF
			IF IS_BIT_SET(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_MOODYMAN_TRUNK_COLLECTION_STARTED)
				sAIVehicleCreationData.fUSBCollectionDelay-=GET_FRAME_TIME()
				IF sAIVehicleCreationData.fUSBCollectionDelay <= 0
					SET_COLLECTABLE_COLLECTED(COLLECTABLE_USB_PIRATE_RADIO, ENUM_TO_INT(USB_COLLECTABLE_LOCATIONS_MOODY_MAN), TRUE, TRUE)
					CLEAR_BIT(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_MOODYMAN_TRUNK_COLLECTION_STARTED)
					DELETE_OBJECT(sAIVehicleCreationData.oiTrunkProps[4])
					CLEAR_HELP()
					
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
PROC MAINTAN_MOODYMAN_TRUNK_PROPRS(CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData)	
	IF NOT IS_BIT_SET(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_MOODYMAN_TRUNK_PROPS_SPAWNED)
	AND sAIVehicleCreationData.iMoodymanIndex != -1	
		
		IF IS_ENTITY_ALIVE(sAIVehicleCreationData.viSpecialPedVehicles[sAIVehicleCreationData.iMoodymanIndex])
			IF NOT IS_ENTITY_IN_AIR(sAIVehicleCreationData.viSpecialPedVehicles[sAIVehicleCreationData.iMoodymanIndex])
				BOOL bAreAllPropsCreated = TRUE
				INT i
				SET_VEHICLE_DOOR_OPEN(sAIVehicleCreationData.viSpecialPedVehicles[sAIVehicleCreationData.iMoodymanIndex], SC_DOOR_BOOT)
				VECTOR vRotation = GET_ENTITY_ROTATION(sAIVehicleCreationData.viSpecialPedVehicles[sAIVehicleCreationData.iMoodymanIndex])
				VECTOR vPosition = GET_ENTITY_COORDS(sAIVehicleCreationData.viSpecialPedVehicles[sAIVehicleCreationData.iMoodymanIndex])
				REPEAT ciCAR_MEET_NUMBER_OF_MOODYMAN_TRUNK_PROPS i
					PRINTLN("[MP_CAR_MEET_AI_VEHICLES][MAINTAN_MOODYMAN_TRUNK_PROPRS] CHECKING IF PROP ", i, " EXISTS")
					IF HAS_LOCAL_PLAYER_COLLECTED_USB_MIX(USB_MUSIC_MIX_4_MOODY_MAN)
					AND i >= 4
						RELOOP
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(sAIVehicleCreationData.oiTrunkProps[i])
						bAreAllPropsCreated = FALSE
						sAIVehicleCreationData.oiTrunkProps[i] = CREATE_OBJECT_NO_OFFSET(GET_TRUNK_PROP_MODEL(i), GET_TRUNK_PROP_POSITION(i,vPosition, vRotation), FALSE)

						ATTACH_ENTITY_TO_ENTITY(sAIVehicleCreationData.oiTrunkProps[i], sAIVehicleCreationData.viSpecialPedVehicles[sAIVehicleCreationData.iMoodymanIndex], 0, GET_TRUNK_PROP_OFFSET(i), GET_TRUNK_PROP_ROTATION(i, vRotation))
						PRINTLN("[MP_CAR_MEET_AI_VEHICLES][MAINTAN_MOODYMAN_TRUNK_PROPRS] CREATING MOODYMAN TRUNK PROP ", i)
						
					ENDIF
			    ENDREPEAT		
				IF bAreAllPropsCreated
					REQUEST_SCRIPT_AUDIO_BANK("DLC_TUNER/DLC_Tuner_Collectibles")
					SET_BIT(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_MOODYMAN_TRUNK_PROPS_SPAWNED)
				ENDIF	
			ELSE
				PRINTLN("[MP_CAR_MEET_AI_VEHICLES][MAINTAN_MOODYMAN_TRUNK_PROPRS] VEHICLE STILL IN AIR. WAITING")
			ENDIF
		ENDIF
	ENDIF
	IF NOT HAS_LOCAL_PLAYER_COLLECTED_USB_MIX(USB_MUSIC_MIX_4_MOODY_MAN)
		MAINTAIN_COLLECTIBLE_PROMPT_FOR_USB(sAIVehicleCreationData)	
	ENDIF
ENDPROC
FUNC BOOL CULL_AI_VEHICLES(AI_VEHICLE_DATA &sVehicleData, CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData)

	IF NOT g_sMPTunables.iTUNER_CARMEET_ENABLE_AMBIENT_VEHICLE_CULLING
	AND g_sMPTunables.iTUNER_CARMEET_NUMBER_OF_ADDITIONAL_VEHICLES_TO_CULL = 0
		RETURN TRUE
	ENDIF
	
	INT i	
	REPEAT ciCAR_MEET_NUMER_OF_AI_VEHICLES i	
		IF sVehicleData.iCullingLevel<GET_AI_VEHICLE_CULLING_LEVEL(i)
		OR  g_sMPTunables.iTUNER_CARMEET_NUMBER_OF_ADDITIONAL_VEHICLES_TO_CULL >= GET_AI_VEHICLE_ADDITIONAL_CULLING_PRIORITY(i)			
			IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viVehicles[i])
				IF NOT IS_ENTITY_ON_SCREEN(sAIVehicleCreationData.viVehicles[i])
					PRINTLN("[MP_CAR_MEET_AT_VEHICLES][CULL_AI_VEHICLES] CULLING / DELETING VEHICLE ID ", i)
					IF IS_ENTITY_ALIVE(sAIVehicleCreationData.viVehicles[i])
						IF NOT IS_VEHICLE_EMPTY(sAIVehicleCreationData.viVehicles[i])
							PED_INDEX driver =  GET_PED_IN_VEHICLE_SEAT(sAIVehicleCreationData.viVehicles[i])
							IF DOES_ENTITY_EXIST(driver)
								SAFE_DELETE_PED(driver)
								RETURN FALSE	
							ENDIF
						ENDIF
						SAFE_DELETE_VEHICLE(sAIVehicleCreationData.viVehicles[i])
					ENDIF
				ENDIF	
				RETURN FALSE			
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS i	
		IF DOES_SPECIAL_PED_HAVE_PREFEDINED_LOCATION(g_sCarMeetSpecialPedPositionData.ePedsToSpawn[i])
			RELOOP
		ENDIF
		IF sVehicleData.iCullingLevel<GET_AI_VEHICLE_CULLING_LEVEL(g_sCarMeetSpecialPedPositionData.iVehicleIndex[i])
		OR g_sMPTunables.iTUNER_CARMEET_NUMBER_OF_ADDITIONAL_VEHICLES_TO_CULL >= GET_AI_VEHICLE_ADDITIONAL_CULLING_PRIORITY(g_sCarMeetSpecialPedPositionData.iVehicleIndex[i])			
			IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viSpecialPedVehicles[i])
				IF NOT IS_ENTITY_ON_SCREEN(sAIVehicleCreationData.viSpecialPedVehicles[i])
					PRINTLN("[MP_CAR_MEET_AT_VEHICLES][CULL_AI_VEHICLES] CULLING / DELETING SPECIAL PED VEHICLE ID ", i)
					SAFE_DELETE_VEHICLE(sAIVehicleCreationData.viSpecialPedVehicles[i])
					RETURN FALSE
				ENDIF
			ENDIF	
			
			///IF MOODYMANN ALSO CLEAN UP PROPS
			IF g_sCarMeetSpecialPedPositionData.ePedsToSpawn[i] = PED_MOODYMAN
				INT j
				REPEAT ciCAR_MEET_NUMBER_OF_MOODYMAN_TRUNK_PROPS j
					IF DOES_ENTITY_EXIST(sAIVehicleCreationData.oiTrunkProps[j])	
						IF NOT IS_ENTITY_ON_SCREEN(sAIVehicleCreationData.oiTrunkProps[j])					
							DELETE_OBJECT(sAIVehicleCreationData.oiTrunkProps[j])
						ENDIF
					ENDIF
				ENDREPEAT
				CLEAR_BIT(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_MOODYMAN_PRESENT)
				CLEAR_BIT(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_MOODYMAN_TRUNK_PROPS_SPAWNED)
				CLEAR_BIT(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_MOODYMAN_TRUNK_HELP_DISPLAYED)
				CLEAR_BIT(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_MOODYMAN_TRUNK_COLLECTION_STARTED)
			ENDIF
			
		ENDIF
	ENDREPEAT
	//ALL VEHICLES DELETED
	RETURN TRUE
ENDFUNC
/// PURPOSE:
///    created 50 vehicles based on list supplied by the host of the sctipt
/// PARAMS:
///    sVehicleData - array of vehicles shared with all players so all models and variations are synchronised despite being randomised and local
/// RETURNS:
///    FALSE - more cars need to be created , TRUE - all cars created
FUNC BOOL CREATE_AI_VEHICLES(AI_VEHICLE_DATA &sVehicleData, CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData)

	INT i = 0
	VEHICLE_SETUP_STRUCT_MP sData	
	VECTOR vPosition
	FLOAT fHeading
	INT iCurrentPed = 0
	BOOL bWereVehiclesSkipped = FALSE	
	
	REPEAT ciCAR_MEET_NUMER_OF_AI_VEHICLES i	
	
		//IF CULLING IS ENABLED SKIP VEHICLES THAT HAVE CULLING LEVEL LOWER THAN CURRENT CULLING LEVEL
		IF g_sMPTunables.iTUNER_CARMEET_ENABLE_AMBIENT_VEHICLE_CULLING
			IF sVehicleData.iCullingLevel<GET_AI_VEHICLE_CULLING_LEVEL(i)
				RELOOP
			ENDIF
		ENDIF		
		
		//CULLING OF ADDITIONAL VEHICLES  (UP TO 5)
		IF g_sMPTunables.iTUNER_CARMEET_NUMBER_OF_ADDITIONAL_VEHICLES_TO_CULL >= GET_AI_VEHICLE_ADDITIONAL_CULLING_PRIORITY(i)
			PRINTLN("[AM_MP_CAR_MEET_PROPERTY][MP_CAR_MEET_AI_VEHICLES] DIDN'T CREATE VEHICLE ID: ",i," AT ", vPosition, " POSITION IS CULLED")	
			RELOOP
		ENDIF
		
		#IF FEATURE_GEN9_EXCLUSIVE
		IF (i = 41)
			PRINTLN("[AM_MP_CAR_MEET_PROPERTY][MP_CAR_MEET_AI_VEHICLES] Skipping veh index ", i, " for Gen 9")
			RELOOP
		ENDIF
		#ENDIF
		
		IF NOT DOES_ENTITY_EXIST(sAIVehicleCreationData.viVehicles[i])
			IF sVehicleData.sAIVehicle[i].eModel = CM_VEH_INVALID
				PRINTLN("[MP_CAR_MEET_AI_VEHICLES][CREATE_AI_VEHICLES] VEHICLE INDEX: ", i , " IS RESERVED FOR SPECIAL PED")
				RELOOP
			ENDIF
			GET_AI_VEHICLE_DATA(sVehicleData.sAIVehicle[i].eModel,sVehicleData.sAIVehicle[i].iVariation, sData)	
			
			IF NOT IS_MODEL_VALID(sData.VehicleSetup.eModel)
				RELOOP
			ENDIF
			
			IF REQUEST_LOAD_MODEL(sData.VehicleSetup.eModel)
				vPosition = GET_AI_VEHICLE_POSITON(i)
				fHeading = GET_AI_VEHICLE_HEADING(i)
				
				//WE ONLY CHECK VISIBILITY IF CREATING VEHICLES AFTER THEY WERE CULLED
				IF b_SHOULD_CHECK_FOR_VISIBILITTY
				AND IS_SPHERE_VISIBLE(vPosition, 3.5)
					//THIS ENSURES THAT WE DON'T HOLD SPAWNING FOR ONE VEHICLE, BUT THIS FRAME CONTINUE TO THE NEXT, WILL TRY AGAIN NEXT FRAME
					bWereVehiclesSkipped = TRUE
					RELOOP
				ENDIF
				IF IS_ANY_VEHICLE_NEAR_POINT(vPosition, 0.5)
					PRINTLN("[AM_MP_CAR_MEET_PROPERTY][MP_CAR_MEET_AI_VEHICLES] CAN'T CREATE VEHICLE ID: ",i," AT ", vPosition, " THERE'S A VEHICLE ALREADY THERE")
					//THIS ENSURES THAT WE DON'T HOLD SPAWNING FOR ONE VEHICLE, BUT THIS FRAME CONTINUE TO THE NEXT, WILL TRY AGAIN NEXT FRAME
					bWereVehiclesSkipped = TRUE
					RELOOP
				ENDIF
				
				///SPECIAL PROPERTY FLIP 180
				IF IS_BITMASK_ENUM_AS_ENUM_SET(sVehicleData.sAIVehicle[i].eSpecialProperty, AI_VEHICLE_SPECIAL_PROPERTY_ROTATE_180)
					IF fHeading<=180
						fHeading+=180.0
					ELSE
						fHeading-=180.0
					ENDIF
					PRINTLN("[MP_CAR_MEET_AI_VEHICLES][CREATE_AI_VEHICLES] ROTATING VEHICLE: ", i )
				ENDIF
				sAIVehicleCreationData.viVehicles[i] = CREATE_VEHICLE(sData.VehicleSetup.eModel, vPosition, fHeading, FALSE)
				SET_VEHICLE_SETUP_MP(sAIVehicleCreationData.viVehicles[i], sData)				
				SET_VEHICLE_DOORS_LOCKED(sAIVehicleCreationData.viVehicles[i], VEHICLELOCK_CANNOT_ENTER)			
				
				IF NOT IS_BIT_SET(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_HYDRAULICS_VEHICLE_PRESENT)
					IF IS_BITMASK_ENUM_AS_ENUM_SET(sVehicleData.sAIVehicle[i].eSpecialProperty, AI_VEHICLE_SPECIAL_PROPERTY_HYDRAULICS_DANCE)
						SET_BIT(sAIVehicleCreationData.iBS, BS_AI_VEHICLES_HYDRAULICS_VEHICLE_PRESENT)
						sAIVehicleCreationData.sHydraulics.iHydraulicsDanceIndex = i		
						PRINTLN("[MP_CAR_MEET_AI_VEHICLES][CREATE_AI_VEHICLES] VEHICLE INDEX: ", i , " WILL HAVE A DANCING HYDRAULICS PED")
					ENDIF
				ENDIF
				///SPECIAL PROPERTY HEADLIGHTS ON
				IF SHOULD_VEHICLE_HAVE_LIGHTS_ON(i)
				#IF IS_DEBUG_BUILD OR GET_COMMANDLINE_PARAM_EXISTS("ForceCarMeetHeadlights") #ENDIF
					SET_VEHICLE_LIGHTS(sAIVehicleCreationData.viVehicles[i], FORCE_VEHICLE_LIGHTS_ON)
					PRINTLN("[MP_CAR_MEET_AI_VEHICLES][CREATE_AI_VEHICLES] SWITCHING ON HEADLIGHTS FOR VEHICLE: ", i )
				ENDIF	
				
				SET_VEHICLE_RADIO_ENABLED(sAIVehicleCreationData.viVehicles[i], FALSE)
				
				PRINTLN("[AM_MP_CAR_MEET_PROPERTY][MP_CAR_MEET_AI_VEHICLES] CREATING AI VEHICLE ID: ",i," AT ", vPosition)
				
				SET_ENTITY_INVINCIBLE(sAIVehicleCreationData.viVehicles[i], TRUE)
				SET_ENTITY_CAN_BE_DAMAGED(sAIVehicleCreationData.viVehicles[i], FALSE)
				SET_VEHICLE_FIXED(sAIVehicleCreationData.viVehicles[i])
				SET_ENTITY_HEALTH(sAIVehicleCreationData.viVehicles[i], 1000)
				SET_VEHICLE_ENGINE_HEALTH(sAIVehicleCreationData.viVehicles[i], 1000)
				SET_VEHICLE_PETROL_TANK_HEALTH(sAIVehicleCreationData.viVehicles[i], 1000)
				SET_VEHICLE_DIRT_LEVEL(sAIVehicleCreationData.viVehicles[i], 0.0)
				SET_VEHICLE_ON_GROUND_PROPERLY(sAIVehicleCreationData.viVehicles[i])		
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sAIVehicleCreationData.viVehicles[i], TRUE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(sData.VehicleSetup.eModel)
				RETURN FALSE //TO MAKE SURE WE CREATE ONE MODEL PER FRAME
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(sAIVehicleCreationData.viVehicles[i])	
		AND IS_BITMASK_ENUM_AS_ENUM_SET(sVehicleData.sAIVehicle[i].eSpecialProperty, AI_VEHICLE_SPECIAL_PROPERTY_REVING_PED)
			IF NOT DOES_ENTITY_EXIST(sAIVehicleCreationData.piRevingEnginePeds[iCurrentPed])
				MODEL_NAMES eModel
				eModel = INT_TO_ENUM(MODEL_NAMES, HASH("A_M_Y_CarClub_01"))	
				IF REQUEST_LOAD_MODEL(eModel)
					IF IS_ENTITY_ALIVE(sAIVehicleCreationData.viVehicles[i])			
					AND IS_VEHICLE_SEAT_FREE(sAIVehicleCreationData.viVehicles[i], VS_DRIVER)
						sAIVehicleCreationData.piRevingEnginePeds[iCurrentPed] = CREATE_PED_INSIDE_VEHICLE(sAIVehicleCreationData.viVehicles[i], PEDTYPE_CIVMALE, eModel, VS_DRIVER, FALSE, FALSE)
						PRINTLN("[AM_MP_CAR_MEET_PROPERTY][MP_CAR_MEET_AI_VEHICLES] SPAWNED REVING PED IN VEH ", i)
						SET_PED_DEFAULT_COMPONENT_VARIATION(sAIVehicleCreationData.piRevingEnginePeds[iCurrentPed])	
						IF NOT HAS_NET_TIMER_STARTED(sAIVehicleCreationData.stRevingEngineTimers[iCurrentPed])	
							sAIVehicleCreationData.iRevingEngineActualDelay[iCurrentPed] = GET_RANDOM_INT_IN_RANGE(ciCAR_MEET_ENGINE_REV_MIN_DELAY, ciCAR_MEET_ENGINE_REV_MAX_DELAY)
							START_NET_TIMER(sAIVehicleCreationData.stRevingEngineTimers[iCurrentPed])
						ENDIF
						RETURN FALSE
					ELSE
					
					PRINTLN("[AM_MP_CAR_MEET_PROPERTY][MP_CAR_MEET_AI_VEHICLES] VEHICLE SEAT TAKEN")
					ENDIF	
				
				ENDIF
			ENDIF
			iCurrentPed++
		ENDIF
	ENDREPEAT		
	
	//FOR SPEAKERS MUSIC
	OPEN_RELEVANT_CAR_BOOTS(sAIVehicleCreationData)
	
	//FOR PEDS WATCHING THE ENGINES
	OPEN_RELEVENT_CAR_HOODS(sAIVehicleCreationData)
	
	IF NOT bWereVehiclesSkipped
		RETURN TRUE // ALL VEHICLES SPAWNED
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC
FUNC BOOL CREATE_SANDBOX_AI_VEHICLES(AI_VEHICLE_DATA &sVehicleData, CAR_MEET_SANDBOX_AMBIENT_VEHICLE_CREATION_DATA &sAIVehicleCreationData)

	INT i = 0
	VEHICLE_SETUP_STRUCT_MP sData	
	VECTOR vPosition
	FLOAT fHeading
	REPEAT ciCAR_MEET_NUMBER_OF_SANDBOX_AMBIENT_VEHICLES i		
		IF NOT DOES_ENTITY_EXIST(sAIVehicleCreationData.viVehicles[i])	
			GET_AI_VEHICLE_DATA(sVehicleData.sAIVehicle[i].eModel,sVehicleData.sAIVehicle[i].iVariation, sData)		
			IF REQUEST_LOAD_MODEL(sData.VehicleSetup.eModel)
				vPosition = GET_AI_VEHICLE_POSITON(GET_SANDBOX_AMBIENT_VEHICLE_INDEX(i))
				//NEED TO TAKE INTERIOR Z INTO CONSIDERATION
				vPosition.z += 50.0
				fHeading = GET_AI_VEHICLE_HEADING(GET_SANDBOX_AMBIENT_VEHICLE_INDEX(i))	
				sAIVehicleCreationData.viVehicles[i] = CREATE_VEHICLE(sData.VehicleSetup.eModel, vPosition, fHeading, FALSE)
				SET_VEHICLE_SETUP_MP(sAIVehicleCreationData.viVehicles[i], sData)	
				
				SET_VEHICLE_ON_GROUND_PROPERLY(sAIVehicleCreationData.viVehicles[i])
				FREEZE_ENTITY_POSITION(sAIVehicleCreationData.viVehicles[i], TRUE)
				SET_ENTITY_COLLISION(sAIVehicleCreationData.viVehicles[i], FALSE)
				SET_ENTITY_CAN_BE_DAMAGED(sAIVehicleCreationData.viVehicles[i], FALSE)
				SET_ENTITY_DYNAMIC(sAIVehicleCreationData.viVehicles[i], FALSE)
				SET_ENTITY_HAS_GRAVITY(sAIVehicleCreationData.viVehicles[i], FALSE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(sData.VehicleSetup.eModel)
				
				///SPECIAL PROPERTY HEADLIGHTS ON
				IF SHOULD_VEHICLE_HAVE_LIGHTS_ON(GET_SANDBOX_AMBIENT_VEHICLE_INDEX(i))
				#IF IS_DEBUG_BUILD OR GET_COMMANDLINE_PARAM_EXISTS("ForceCarMeetHeadlights") #ENDIF
					SET_VEHICLE_LIGHTS(sAIVehicleCreationData.viVehicles[i], FORCE_VEHICLE_LIGHTS_ON)
					PRINTLN("[MP_CAR_MEET_AI_VEHICLES][CREATE_AI_VEHICLES] SWITCHING ON HEADLIGHTS FOR VEHICLE: ", i )
				ENDIF				
				
				PRINTLN("[AM_MP_CAR_MEET_PROPERTY][MP_CAR_MEET_AI_VEHICLES] CREATING AI VEHICLE ID: ",i," AT ", vPosition)
				RETURN FALSE //TO MAKE SURE WE CREATE ONE MODEL PER FRAME
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF		
	ENDREPEAT			
	RETURN TRUE // ALL VEHICLES SPAWNED
	
ENDFUNC
FUNC BOOL CREATE_SANDBOX_PROMO_VEHICLE_CLONES(CAR_MEET_SANDBOX_AMBIENT_VEHICLE_CREATION_DATA &sAIVehicleCreationData)

	INT i = 0
	VEHICLE_SETUP_STRUCT_MP sData
	REPEAT ciCAR_MEET_NUMBER_OF_PROMO_VEHS i		
		IF NOT DOES_ENTITY_EXIST(sAIVehicleCreationData.viTestVehicles[i])
			MODEL_NAMES _eVehModel = GET_AMBIENT_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_MODEL(i)	
			PRINTLN("[CREATE_SANDBOX_PROMO_VEHICLE_CLONES] LOADING MODEL")
			IF REQUEST_LOAD_MODEL(_eVehModel)
				PRINTLN("[CREATE_SANDBOX_PROMO_VEHICLE_CLONES] CREATING VEH ", i)
				sAIVehicleCreationData.viTestVehicles[i] = CREATE_VEHICLE(_eVehModel, GET_AMBIENT_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_COORDS(i), GET_AMBIENT_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_HEADING(i), FALSE, FALSE)
				IF i<3
					SET_VEHICLE_ON_GROUND_PROPERLY(sAIVehicleCreationData.viTestVehicles[i])
					FREEZE_ENTITY_POSITION(sAIVehicleCreationData.viTestVehicles[i], TRUE)
				ENDIF
				GET_AMBIENT_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_COLOURS(sData, i)
				SET_VEHICLE_SETUP_MP(sAIVehicleCreationData.viTestVehicles[i], sData, FALSE, TRUE, TRUE)
				RETURN FALSE
			ELSE
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT		
	IF IS_ENTITY_ALIVE(sAIVehicleCreationData.viTestVehicles[3])
		SET_VEHICLE_DOOR_OPEN(sAIVehicleCreationData.viTestVehicles[3], SC_DOOR_BOOT, FALSE, TRUE)
		SET_VEHICLE_LIGHTS(sAIVehicleCreationData.viTestVehicles[3], FORCE_VEHICLE_LIGHTS_ON)
	ENDIF
	RETURN TRUE // ALL VEHICLES SPAWNED
	
ENDFUNC
/// PURPOSE:
///    
/// PARAMS:
///    eState - supplied from the main script to ensure other players don't attempt co create the list and wait for the host to supply one
///    sVehicleData - array of vehicles shared with all players so all models and variations are synchronised despite being randomised and local
PROC MAINTAIN_LOADING_CAR_MEET_AI_VEHICLES(CAR_MEET_AI_VEHICLE_LIST_CREATION_STATE &eState, AI_VEHICLE_DATA &sVehicleData, CAR_MEET_AI_VEHICLE_CREATION_DATA &sAIVehicleCreationData)
	
	//HOST CHECKS HOW MANY PLAYERS ARE IN THE INTERIOR AND ADJUSTS CULLING LEVELS
	MAINTAIN_AMBIENT_VEHICLE_CULLING_LEVELS(sVehicleData)
	
	SWITCH eState
		CASE CAR_MEET_AI_VEHICLE_LIST_NOT_STARTED  // FIRST PLAYER TO ENTER IMMEDIATELY STARTS CREATING THE LIST AND CHANGES THE STATE
			IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
				eState = CAR_MEET_AI_VEHICLE_LIST_LOADING
				CREATE_VEHICLE_LIST(eState, sVehicleData)
			ENDIF
		BREAK
		CASE CAR_MEET_AI_VEHICLE_LIST_LOADING // OTHER PLAYERS DO NOTHING UNTIL LIST IS CREATED
				EXIT	
		BREAK
		CASE CAR_MEET_AI_VEHICLE_LIST_DONE  //THIS IS CHANGED FROM WITHIN CREATE_VEHICLE_LIST		
				//SPAWN VEHICLES
			IF NOT b_HAVE_ALL_AI_VEHICLES_BEEN_CREATED
				b_HAVE_ALL_AI_VEHICLES_BEEN_CREATED = CREATE_AI_VEHICLES(sVehicleData, sAIVehicleCreationData) AND CREATE_SPECIAL_PED_VEHICLES(sAIVehicleCreationData, sVehicleData) AND CREATE_HYDRAULICS_DANCING_PED(sAIVehicleCreationData)
				IF b_HAVE_ALL_AI_VEHICLES_BEEN_CREATED
					b_SHOULD_CHECK_FOR_VISIBILITTY = FALSE
				ENDIF
			ENDIF
				//REMOVE VEHICLES (CULLING)
			IF NOT b_HALE_ALL_AL_VEHICLES_BEEN_CULLED	
				b_HALE_ALL_AL_VEHICLES_BEEN_CULLED = CULL_AI_VEHICLES(sVehicleData, sAIVehicleCreationData)
			ENDIF		
			
			MAINTAIN_DANCING_HYDRAULICS(sAIVehicleCreationData)
			MAINTAIN_ENGINE_REVING(sAIVehicleCreationData)
			MAINTAN_MOODYMAN_TRUNK_PROPRS(sAIVehicleCreationData) 
			
			
			
			//IF PLAYERS LEFT AND LEVEL INCREASED - RESPAWN MISSING VEHICLES
			IF sVehicleData.iCullingLevel > sAIVehicleCreationData.iPrevousCullingLevel
			OR sAIVehicleCreationData.iNUMBER_OF_ADDITIONAL_VEHICLES_TO_CULL > g_sMPTunables.iTUNER_CARMEET_NUMBER_OF_ADDITIONAL_VEHICLES_TO_CULL
				sAIVehicleCreationData.iNUMBER_OF_ADDITIONAL_VEHICLES_TO_CULL = g_sMPTunables.iTUNER_CARMEET_NUMBER_OF_ADDITIONAL_VEHICLES_TO_CULL
				sAIVehicleCreationData.iPrevousCullingLevel = sVehicleData.iCullingLevel
				b_HAVE_ALL_AI_VEHICLES_BEEN_CREATED = FALSE
				b_SHOULD_CHECK_FOR_VISIBILITTY = TRUE
				PRINTLN("[MP_CAR_MEET_AT_VEHICLES][MAINTAIN_LOADING_CAR_MEET_AI_VEHICLES] CULLING LEVEL INCREASED, RESPAWNING SOME VEHICLES")
			ENDIF
										
			//IF PLAYERS JOINED AND LEVEL DECREASED - REMOVE VEHICLES
			IF sVehicleData.iCullingLevel < sAIVehicleCreationData.iPrevousCullingLevel
			OR sAIVehicleCreationData.iNUMBER_OF_ADDITIONAL_VEHICLES_TO_CULL < g_sMPTunables.iTUNER_CARMEET_NUMBER_OF_ADDITIONAL_VEHICLES_TO_CULL
				sAIVehicleCreationData.iNUMBER_OF_ADDITIONAL_VEHICLES_TO_CULL = g_sMPTunables.iTUNER_CARMEET_NUMBER_OF_ADDITIONAL_VEHICLES_TO_CULL
				sAIVehicleCreationData.iPrevousCullingLevel = sVehicleData.iCullingLevel
				b_HALE_ALL_AL_VEHICLES_BEEN_CULLED = FALSE
				PRINTLN("[MP_CAR_MEET_AT_VEHICLES][MAINTAIN_LOADING_CAR_MEET_AI_VEHICLES] CULLING LEVEL DECREASED, DELETING SOME VEHICLES")
			ENDIF
		BREAK
	ENDSWITCH		
		
ENDPROC
PROC MAINTAIN_LOADING_CAR_MEET_SANDBOX_AI_VEHICLES(CAR_MEET_AI_VEHICLE_LIST_CREATION_STATE &eState, AI_VEHICLE_DATA &sVehicleData, CAR_MEET_SANDBOX_AMBIENT_VEHICLE_CREATION_DATA &sAIVehicleCreationData)
		SWITCH eState
			CASE CAR_MEET_AI_VEHICLE_LIST_NOT_STARTED  // FIRST PLAYER TO ENTER IMMEDIATELY STARTS CREATING THE LIST AND CHANGES THE STATEm
				IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					eState = CAR_MEET_AI_VEHICLE_LIST_LOADING
					CREATE_SANDBOX_VEHICLE_LIST(eState, sVehicleData)
				ENDIF
			BREAK
			CASE CAR_MEET_AI_VEHICLE_LIST_LOADING // OTHER PLAYERS DO NOTHING UNTIL LIST IS CREATED
					EXIT	
			BREAK
			CASE CAR_MEET_AI_VEHICLE_LIST_DONE  //THIS IS CHANGED FROM WITHIN CREATE_VEHICLE_LIST
				IF NOT b_HAVE_ALL_AI_VEHICLES_BEEN_CREATED
					b_HAVE_ALL_AI_VEHICLES_BEEN_CREATED = CREATE_SANDBOX_AI_VEHICLES(sVehicleData, sAIVehicleCreationData) AND CREATE_SANDBOX_PROMO_VEHICLE_CLONES(sAIVehicleCreationData)
				ENDIF
			
			BREAK
		ENDSWITCH		
ENDPROC
