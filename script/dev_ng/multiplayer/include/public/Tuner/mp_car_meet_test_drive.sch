//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        mp_car_meet_test_drive.sch																			//
// Description: This is implementation of the car meet test drive. Consists of both private and public API			//
//				for other scripts to use.																			//
// Written by:  Joe Davis																							//
// Date:  		26/02/2021																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////    

USING "globals.sch"
USING "mp_globals.sch"
USING "script_debug.sch"
USING "net_include.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "rc_helper_functions.sch"
USING "net_realty_new.sch"
USING "net_simple_cutscene.sch"
USING "Cutscene_help.sch"
USING "net_simple_interior_cs_header_include.sch"
USING "net_purchase_vehicle.sch"

#IF FEATURE_TUNER

// ***************************************************************************************
// 					CONSTS
// ***************************************************************************************

#IF NOT FEATURE_GEN9_EXCLUSIVE
CONST_INT NUM_PROMO_TEST_DRIVE_VEHICLES							3
#ENDIF

#IF FEATURE_GEN9_EXCLUSIVE
CONST_INT NUM_PROMO_TEST_DRIVE_VEHICLES							4 // + HAO'S SPECIAL VEHICLE
CONST_INT HAO_PREMIUM_TEST_VEH_INDEX							3
#ENDIF

CONST_INT TEST_DRIVE_INSTANCE_MENU_SELECTION_PUBLIC				0
CONST_INT TEST_DRIVE_INSTANCE_MENU_SELECTION_ENTER_ALONE		1
CONST_INT TEST_DRIVE_INSTANCE_MENU_SELECTION_JOIN				2

CONST_INT TEST_DRIVE_EXIT_MENU_SELECTION_ENTER_MEET				0
CONST_INT TEST_DRIVE_EXIT_MENU_SELECTION_EXIT					1

CONST_INT TEST_DRIVE_EXIT_SANDBOX_MENU_SELECTION_ENTER_MEET		0
CONST_INT TEST_DRIVE_EXIT_SANDBOX_MENU_SELECTION_EXIT_MEET		1

CONST_INT KICK_PLAYER_OUT_OF_PROMO_VEH_TIME						10000 // 10 seconds
CONST_INT EXIT_TEST_VEH_HOLD_TIME								3000 // 3 seconds
CONST_INT WARP_BACK_INTO_TEST_DRIVE_PV_TIME						5000
CONST_INT NUM_SANDBOX_EXIT_MENU_VARIANTS						7

CONST_FLOAT NEAR_PROMO_TEST_DRIVE_VEHICLE_RANGE_DEFAULT			5.0

// ----- Cutscene consts -----
CONST_INT CUTSCENE_VEC_ARRAY_TD_PRIMARY_VEH_MODEL_MIN			0
CONST_INT CUTSCENE_VEC_ARRAY_TD_PRIMARY_VEH_MODEL_MAX			1
CONST_INT CUTSCENE_VEC_ARRAY_TD_SAVED_VEH_COORD					2

CONST_INT CUTSCENE_FLOAT_ARRAY_TD_VEH_Z_COORD					0
CONST_INT CUTSCENE_FLOAT_TD_MOVE_VEH_DISTANCE					1
CONST_INT CUTSCENE_FLOAT_ARRAY_TD_VEH_SPEED						2

CONST_FLOAT TD_SANDBOX_SCENE_VEH_MAX_HEIGHT 					4.9
CONST_FLOAT TD_SANDBOX_SCENE_VEH_MAX_LENGTH 					7.0

// entryAnim.iSceneBS
CONST_INT CUTSCENE_BS_TD_SANDBOX_ONE_SHOT_BIKE_SCENE						1
CONST_INT CUTSCENE_BS_TD_SANDBOX_TALL_VEH_SCENE								2
CONST_INT CUTSCENE_BS_TD_SANDBOX_TASKED_MOTORBIKE							3
CONST_INT CUTSCENE_BS_TD_SANDBOX_FROZEN_CLONE								4

// serverBD.iBS
CONST_INT SERVER_TEST_DRIVE_BD_DATA_FORCED_ROOM_FOR_PROMO_VEHICLES			0

// playerBD[player].iBS
CONST_INT PLAYER_TEST_DRIVE_BD_DATA_DRIVER_TRIGGERED_START_TEST_DRIVE				0
CONST_INT PLAYER_TEST_DRIVE_BD_DATA_DRIVER_TRIGGERED_STOP_TEST_DRIVE				1
CONST_INT PLAYER_TEST_DRIVE_BD_DATA_DRIVER_IS_IN_NEW_TEST_DRIVE_VEHICLE				2
CONST_INT PLAYER_TEST_DRIVE_BD_DATA_CHECKED_PASSENGER_IS_OKAY_TO_START_TEST_DRIVE	3

// sTestDriveData.iBS
CONST_INT TEST_DRIVE_DATA_BS_STARTING_TRANSITION_TO_SANDBOX_IN_PV			0
CONST_INT TEST_DRIVE_DATA_BS_STARTING_TRANSITION_IN_PV_AS_PASSENGER			1
CONST_INT TEST_DRIVE_DATA_BS_LOCKED_TEST_DRIVE_VEHICLE						2
CONST_INT TEST_DRIVE_DATA_BS_MENU_DELAY_TIMER_ACTIVE						3
CONST_INT TEST_DRIVE_DATA_BS_REBUILD_MENU									4
CONST_INT TEST_DRIVE_DATA_BS_MENU_OPTION_ACCEPTED							5
CONST_INT TEST_DRIVE_DATA_BS_MENU_OPTION_BACK								6
CONST_INT TEST_DRIVE_DATA_BS_SANDBOX_VEH_RESERVED							7
CONST_INT TEST_DRIVE_DATA_BS_SANDBOX_VEH_CREATED							8
CONST_INT TEST_DRIVE_DATA_BS_SETUP_FIRST_VEHICLE_SCENE						9
CONST_INT TEST_DRIVE_DATA_BS_PLAYED_ENTER_PROMO_VEH_SCENE					10
CONST_INT TEST_DRIVE_DATA_BS_FADING_OUT_PLAYER_FOR_CUTSCENE					11
CONST_INT TEST_DRIVE_DATA_BS_START_TRANSITION_FROM_SANDBOX_TO_SHOWROOM		13
CONST_INT TEST_DRIVE_DATA_BS_PASSENGER_IS_READY_TO_TRANSITION_TO_SANDBOX	14
CONST_INT TEST_DRIVE_DATA_BS_UNLOCKED_PV_I_AM_WARPING_BACK_TO				15
CONST_INT TEST_DRIVE_DATA_BS_SET_TEST_DRIVE_PED_PROPERTIES					16
CONST_INT TEST_DRIVE_DATA_BS_SHOULD_DISPLAY_EXIT_SANDBOX_MENU				17
CONST_INT TEST_DRIVE_DATA_BS_PURCHASE_PROMO_VEH_CONTROL_PRESSED				18
CONST_INT TEST_DRIVE_DATA_BS_WARPED_NEAR_PV_AFTER_TEST_DRIVE				19
CONST_INT TEST_DRIVE_DATA_BS_REQUESTED_AUDIO_BANK							20
CONST_INT TEST_DRIVE_DATA_BS_RELEASED_AUDIO_BANK							21
CONST_INT TEST_DRIVE_DATA_BS_DISABLE_SANDBOX_EXIT_RAMP						22

// ***************************************************************************************
// 					ENUMS
// ***************************************************************************************

ENUM TEST_DRIVE_STATE
	TDS_NULL,
	
	TDS_ENTER_SANDBOX_CUTSCENE
ENDENUM

ENUM TEST_DRIVE_CUTSCENE_STATE
	TDCS_NULL,
	
	TDCS_LOAD_ASSETS,
	TDCS_CREATE_ASSETS,
	TDCS_UPDATE,
	TDCS_CLEANUP
ENDENUM

ENUM TEST_DRIVE_INSTANCE_MENU_LAYER
	TDML_SELECT_OPTIONS,
	TDML_SELECT_FRIEND_TO_JOIN
ENDENUM

ENUM TEST_DRIVE_MENU_GRAPHIC
	#IF FEATURE_GEN9_EXCLUSIVE
	TDMG_HSW,
	#ENDIF
	
	TDMG_DEFAULT
ENDENUM

// ***************************************************************************************
// 					STRUCTS
// ***************************************************************************************

STRUCT SERVER_TEST_DRIVE_BD_DATA
	INT iPlayerTestDriving = -1
	INT iBS
	INT iVehReservedBS
	INT iVehGroundedBS
	NETWORK_INDEX niTestDriveVehicle[NUM_PROMO_TEST_DRIVE_VEHICLES]
ENDSTRUCT

STRUCT PLAYER_TEST_DRIVE_BD_DATA
	INT iBS
	INT iInstance = -99
ENDSTRUCT

STRUCT TEST_DRIVE_MENU_DATA
	INT iCurrentSelection
	INT iNumberOfFriendOptions = 0
	BOOL bJoinOptionShouldBeActive
	
	PLAYER_INDEX playerInThisSelection[NUM_NETWORK_PLAYERS]
	CAMERA_INDEX camera
	
	TEST_DRIVE_INSTANCE_MENU_LAYER eLayer = TDML_SELECT_OPTIONS
	SCRIPT_TIMER tAnalogueDelayTimer
	
	INT iShotVariant = -1
	FLOAT fShotCamShake 
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT TEST_DRIVE_VEHICLE_DEBUG_DATA
	VECTOR vCoords
	VECTOR vCachedCoords
	VECTOR vRotation
	VECTOR vCachedRotation
ENDSTRUCT

STRUCT TEST_DRIVE_DEBUG_DATA
	BOOL bDebugDrawCamOffsetToPromoVeh
	
	BOOL bUpdatePromoVehCoords
	BOOL bOutputPromoVehCoords
	
	BOOL bDrawInstanceMenuDebug
	
	// Sandbox area
	BOOL bDisableRamp
	BOOL bManuallySetExitMenuShot
	INT iShotVariant = -1
	FLOAT fShotCamShake
	
	TEST_DRIVE_VEHICLE_DEBUG_DATA sDebugVehData[NUM_PROMO_TEST_DRIVE_VEHICLES]
	
	#IF FEATURE_GEN9_EXCLUSIVE
	BOOL bBypassHSWPurchaseRequirements
	#ENDIF
ENDSTRUCT
#ENDIF

STRUCT TEST_DRIVE_DATA
	INT iBS, iPlayerStagger, iPromoVehIAmNearAsInt
	VECTOR vNewTestVehCoords
	
	NETWORK_INDEX niSandboxVeh
	VEHICLE_INDEX viPromoVehIAmNear
	VEHICLE_INDEX viVehToTestDrive
	
	// Structs
	PLAYER_TEST_DRIVE_BD_DATA playerBD[NUM_NETWORK_PLAYERS]
	TEST_DRIVE_MENU_DATA sInstanceMenu
	TEST_DRIVE_MENU_DATA sExitSandboxMenu
	SCRIPT_TIMER sKickOutOfPromoVehTimer
	SIMPLE_INTERIOR_ENTRY_ANIM_DATA promoVehAnim
	SIMPLE_INTERIOR_ENTRY_ANIM_DATA entryAnim
	SCRIPT_CONTROL_HOLD_TIMER sExitVehHoldTimer
	
	#IF IS_DEBUG_BUILD
	TEST_DRIVE_DEBUG_DATA sDebugData
	#ENDIF
	
	// Enums
	TEST_DRIVE_STATE eState 					= TDS_NULL
	TEST_DRIVE_CUTSCENE_STATE eCutsceneState 	= TDCS_NULL
	SCRIPT_TIMER timerWarpBackIntoPV
	SCRIPT_TIMER timerPVNetworkRequestControl
	SCRIPT_TIMER blockTestDriveMenu
	TEST_DRIVE_MENU_GRAPHIC eMenuGraphic		= TDMG_DEFAULT
	SCRIPT_TIMER timerPassengerBail
	SCRIPT_TIMER timerCutsceneBail
ENDSTRUCT

// ***************************************************************************************
// 					DEBUG
// ***************************************************************************************

#IF IS_DEBUG_BUILD

DEBUGONLY PROC ADD_WIDGET_TEST_VEHICLE_DATA(TEST_DRIVE_DATA &sTestDriveData)
	INT i
	REPEAT NUM_PROMO_TEST_DRIVE_VEHICLES i
		TEXT_LABEL_15 tlVeh = "Vehicle - "
		tlVeh += GET_STRING_FROM_INT(i+1)
		
		START_WIDGET_GROUP(tlVeh)
			ADD_WIDGET_VECTOR_SLIDER("Coords", sTestDriveData.sDebugData.sDebugVehData[i].vCoords, -99999.9, 99999.9, 0.01)
			ADD_WIDGET_VECTOR_SLIDER("Rotation", sTestDriveData.sDebugData.sDebugVehData[i].vRotation, -360.9, 360.9, 0.1)
		STOP_WIDGET_GROUP()
	ENDREPEAT
ENDPROC

DEBUGONLY PROC DEBUG_TEST_DRIVE_ADD_WIDGETS(TEST_DRIVE_DATA &sTestDriveData)	
	PRINTLN("[TEST_DRIVE_SANDBOX][DEBUG_TEST_DRIVE_ADD_WIDGETS] Adding debug widgets.")
	
	START_WIDGET_GROUP("Test Drive Sandbox")
		START_WIDGET_GROUP("Instance Menu Debug")
			ADD_WIDGET_BOOL("Draw On Screen Debug", sTestDriveData.sDebugData.bDrawInstanceMenuDebug)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Promo Test Vehicles")
			START_WIDGET_GROUP("Placement")
				ADD_WIDGET_BOOL("Update Test Cars Coords", sTestDriveData.sDebugData.bUpdatePromoVehCoords)
				ADD_WIDGET_BOOL("Output Test Cars Coords", sTestDriveData.sDebugData.bOutputPromoVehCoords)
				ADD_WIDGET_TEST_VEHICLE_DATA(sTestDriveData)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Cutscene Cam")
				ADD_WIDGET_BOOL("Draw Cam Offset To Promo Veh", sTestDriveData.sDebugData.bDebugDrawCamOffsetToPromoVeh)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		#IF FEATURE_GEN9_EXCLUSIVE
		START_WIDGET_GROUP("HSW Premium Test Vehicle")
			ADD_WIDGET_BOOL("Bypass Purchase Requirements.", sTestDriveData.sDebugData.bBypassHSWPurchaseRequirements)
		STOP_WIDGET_GROUP()
		#ENDIF
		
	STOP_WIDGET_GROUP()
ENDPROC

DEBUGONLY PROC DEBUG_TEST_DRIVE_ADD_WIDGETS_SANDBOX_AREA(TEST_DRIVE_DATA &sTestDriveData)
	PRINTLN("[TEST_DRIVE_SANDBOX][DEBUG_TEST_DRIVE_SANDBOX_AREA_ADD_WIDGETS] Adding debug widgets.")
	
	START_WIDGET_GROUP("Test Drive Sandbox")
		ADD_WIDGET_BOOL("Disable Ramp", sTestDriveData.sDebugData.bDisableRamp)
		ADD_WIDGET_BOOL("Set Exit Menu Shot Variant", sTestDriveData.sDebugData.bManuallySetExitMenuShot)
		ADD_WIDGET_INT_SLIDER("Shot Variant", sTestDriveData.sDebugData.iShotVariant, -1, NUM_SANDBOX_EXIT_MENU_VARIANTS - 1, 1)
		ADD_WIDGET_FLOAT_SLIDER("Shot Cam Shake", sTestDriveData.sDebugData.fShotCamShake, 0, 1.0, 0.01)
	STOP_WIDGET_GROUP()
ENDPROC

DEBUGONLY PROC DEBUG_TEST_DRIVE_SANDBOX_DRAW_KICK_OUT_PROMO_VEH_TIMER(SCRIPT_TIMER sTimer)
	TEXT_LABEL_63 tlText
	VECTOR vPos = <<0.5, 0.5, 0.0>>
	INT iTimeDiff = GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sTimer)
	INT iDisplayTime = (KICK_PLAYER_OUT_OF_PROMO_VEH_TIME - iTimeDiff) / 1000
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	
	tlText = "Debug: Kicking player out of promo vehicle in - "
	tlText += GET_STRING_FROM_INT(iDisplayTime)
	DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vPos, 0, 255, 0)
ENDPROC

DEBUGONLY PROC OUTPUT_TEST_DRIVE_VEHICLE_DEBUG_POSITION(TEST_DRIVE_DEBUG_DATA &sDebugData)
	INT i
	
	OPEN_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("-------- TEST CARS COORDS ----------")
		REPEAT NUM_PROMO_TEST_DRIVE_VEHICLES i
			TEXT_LABEL_15 tlVeh = "Vehicle - "
			tlVeh += GET_STRING_FROM_INT(i)
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE(tlVeh)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("Coords = ") SAVE_VECTOR_TO_DEBUG_FILE(sDebugData.sDebugVehData[i].vCoords)
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("Rotation = ") SAVE_VECTOR_TO_DEBUG_FILE(sDebugData.sDebugVehData[i].vRotation)
		ENDREPEAT
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("-------- END TEST CARS COORDS ----------")
	CLOSE_DEBUG_FILE()
ENDPROC

DEBUGONLY PROC UPDATE_TEST_DRIVE_VEHICLE_DEBUG_POSITION(TEST_DRIVE_DEBUG_DATA &sDebugData, SERVER_TEST_DRIVE_BD_DATA &sServerData)
	INT i
	REPEAT NUM_PROMO_TEST_DRIVE_VEHICLES i
		IF IS_ENTITY_ALIVE(NET_TO_VEH(sServerData.niTestDriveVehicle[i]))
			IF IS_ENTITY_ATTACHED(NET_TO_VEH(sServerData.niTestDriveVehicle[i]))
				DETACH_ENTITY(NET_TO_VEH(sServerData.niTestDriveVehicle[i]))
			ENDIF
			
			IF NOT ARE_VECTORS_EQUAL(sDebugData.sDebugVehData[i].vCoords, sDebugData.sDebugVehData[i].vCachedCoords)
				sDebugData.sDebugVehData[i].vCachedCoords = sDebugData.sDebugVehData[i].vCoords
				SET_ENTITY_COORDS_NO_OFFSET(NET_TO_VEH(sServerData.niTestDriveVehicle[i]), sDebugData.sDebugVehData[i].vCoords)
			ENDIF
			
			IF NOT ARE_VECTORS_EQUAL(sDebugData.sDebugVehData[i].vRotation, sDebugData.sDebugVehData[i].vCachedRotation)
				sDebugData.sDebugVehData[i].vCachedRotation = sDebugData.sDebugVehData[i].vRotation
				SET_ENTITY_ROTATION(NET_TO_VEH(sServerData.niTestDriveVehicle[i]), sDebugData.sDebugVehData[i].vRotation)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

DEBUGONLY PROC INIT_TEST_DRIVE_VEHICLE_DEBUG_POSITION(TEST_DRIVE_VEHICLE_DEBUG_DATA &sDebugVehData, VEHICLE_INDEX Vehicle)
	IF IS_ENTITY_ALIVE(Vehicle)
		sDebugVehData.vCoords = GET_ENTITY_COORDS(Vehicle)
		sDebugVehData.vRotation = GET_ENTITY_ROTATION(Vehicle)
	ENDIF
ENDPROC

DEBUGONLY PROC DEBUG_TEST_DRIVE_SANDBOX_UPDATE(TEST_DRIVE_DATA &sTestDriveData, SERVER_TEST_DRIVE_BD_DATA &sServerData)
	TEXT_LABEL_63 tlText
	VECTOR vTextPos = <<0.5, 0.1, 0.0>>
	VECTOR vTextOffset = << 0.0, 0.02, 0.0>>
	INT i
	INT iRed 	= 0
	INT iGreen 	= 255
	INT iBlue 	= 0
	
	// ----- Instance Menu Debug -----
	IF (sTestDriveData.sDebugData.bDrawInstanceMenuDebug)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		tlText = "----- Instance Menu Debug -----"
		vTextPos += vTextOffset
		
		tlText = "iCurrentSelection: "
		tlText += GET_STRING_FROM_INT(sTestDriveData.sInstanceMenu.iCurrentSelection)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		
		tlText = "iNumberOfFriendOptions: "
		tlText += GET_STRING_FROM_INT(sTestDriveData.sInstanceMenu.iNumberOfFriendOptions)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		
		tlText = "bJoinOptionShouldBeActive: "
		tlText += GET_STRING_FROM_BOOL(sTestDriveData.sInstanceMenu.bJoinOptionShouldBeActive)
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
		
		REPEAT NUM_NETWORK_PLAYERS i
			tlText = "playerInThisSelection["
			tlText += GET_STRING_FROM_INT(i)
			tlText += "]: "
			
			IF NETWORK_IS_PLAYER_ACTIVE(sTestDriveData.sInstanceMenu.playerInThisSelection[i])
				tlText += GET_PLAYER_NAME(sTestDriveData.sInstanceMenu.playerInThisSelection[i])
				DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
				vTextPos += vTextOffset
			ENDIF
		ENDREPEAT
	ENDIF
	
	// ----- Cam offset to veh -----
	IF sTestDriveData.sDebugData.bDebugDrawCamOffsetToPromoVeh
	AND sTestDriveData.viPromoVehIAmNear != NULL
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		tlText = "OFFSET_FROM_VEH: "
		tlText += GET_STRING_FROM_VECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(sTestDriveData.viPromoVehIAmNear, GET_FINAL_RENDERED_CAM_COORD()))
		DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlText), vTextPos, iRed, iGreen, iBlue)
		vTextPos += vTextOffset
	ENDIF
	
	// ----- Promo veh positions.. url:bugstar:7006136 -----
	IF (sTestDriveData.sDebugData.bUpdatePromoVehCoords)
		UPDATE_TEST_DRIVE_VEHICLE_DEBUG_POSITION(sTestDriveData.sDebugData, sServerData)
	ENDIF
	
	IF (sTestDriveData.sDebugData.bOutputPromoVehCoords)
		OUTPUT_TEST_DRIVE_VEHICLE_DEBUG_POSITION(sTestDriveData.sDebugData)
		sTestDriveData.sDebugData.bOutputPromoVehCoords = FALSE
	ENDIF
ENDPROC

DEBUGONLY FUNC STRING GET_TEST_DRIVE_CUTSCENE_STATE_AS_STRING(TEST_DRIVE_CUTSCENE_STATE eState)
	SWITCH eState
		CASE TDCS_NULL								RETURN "TDCS_NULL"
		CASE TDCS_LOAD_ASSETS						RETURN "TDCS_LOAD_ASSETS"
		CASE TDCS_CREATE_ASSETS						RETURN "TDCS_CREATE_ASSETS"
		CASE TDCS_UPDATE							RETURN "TDCS_UPDATE"
		CASE TDCS_CLEANUP							RETURN "TDCS_CLEANUP"
	ENDSWITCH
	
	RETURN "INVALID STATE"
ENDFUNC

/// PURPOSE:
///    Gets the debug name test drive player broadcast data bit set as string 
DEBUGONLY FUNC STRING DEBUG_GET_TEST_DRIVE_SANDBOX_PLAYER_BD_BS_AS_STRING(INT iBS)
	UNUSED_PARAMETER(iBS)
	SWITCH iBS
		CASE PLAYER_TEST_DRIVE_BD_DATA_DRIVER_TRIGGERED_START_TEST_DRIVE				RETURN "PLAYER_TEST_DRIVE_BD_DATA_DRIVER_TRIGGERED_START_TEST_DRIVE"
		CASE PLAYER_TEST_DRIVE_BD_DATA_DRIVER_TRIGGERED_STOP_TEST_DRIVE					RETURN "PLAYER_TEST_DRIVE_BD_DATA_DRIVER_TRIGGERED_STOP_TEST_DRIVE"
		CASE PLAYER_TEST_DRIVE_BD_DATA_DRIVER_IS_IN_NEW_TEST_DRIVE_VEHICLE				RETURN "PLAYER_TEST_DRIVE_BD_DATA_DRIVER_IS_IN_NEW_TEST_DRIVE_VEHICLE"
		CASE PLAYER_TEST_DRIVE_BD_DATA_CHECKED_PASSENGER_IS_OKAY_TO_START_TEST_DRIVE	RETURN "PLAYER_TEST_DRIVE_BD_DATA_CHECKED_PASSENGER_IS_OKAY_TO_START_TEST_DRIVE"
	ENDSWITCH

	ASSERTLN("[TEST_DRIVE_SANDBOX][DEBUG_GET_TEST_DRIVE_SANDBOX_PLAYER_BD_BS_AS_STRING] - Missing name from lookup: ", iBS)
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets the debug name test drive player broadcast data bit set as string 
DEBUGONLY FUNC STRING DEBUG_GET_TEST_DRIVE_SANDBOX_LOCAL_BD_BS_AS_STRING(INT iBS)
	SWITCH iBS
		CASE TEST_DRIVE_DATA_BS_LOCKED_TEST_DRIVE_VEHICLE						RETURN "TEST_DRIVE_DATA_BS_LOCKED_TEST_DRIVE_VEHICLE"
		CASE TEST_DRIVE_DATA_BS_MENU_DELAY_TIMER_ACTIVE							RETURN "TEST_DRIVE_DATA_BS_MENU_DELAY_TIMER_ACTIVE"
		CASE TEST_DRIVE_DATA_BS_REBUILD_MENU									RETURN "TEST_DRIVE_DATA_BS_REBUILD_MENU"
		CASE TEST_DRIVE_DATA_BS_MENU_OPTION_ACCEPTED							RETURN "TEST_DRIVE_DATA_BS_MENU_OPTION_ACCEPTED"
		CASE TEST_DRIVE_DATA_BS_MENU_OPTION_BACK								RETURN "TEST_DRIVE_DATA_BS_MENU_OPTION_BACK"
		CASE TEST_DRIVE_DATA_BS_SANDBOX_VEH_RESERVED							RETURN "TEST_DRIVE_DATA_BS_SANDBOX_VEH_RESERVED"
		CASE TEST_DRIVE_DATA_BS_SANDBOX_VEH_CREATED								RETURN "TEST_DRIVE_DATA_BS_SANDBOX_VEH_CREATED"
		CASE TEST_DRIVE_DATA_BS_SETUP_FIRST_VEHICLE_SCENE						RETURN "TEST_DRIVE_DATA_BS_SETUP_FIRST_VEHICLE_SCENE"
		CASE TEST_DRIVE_DATA_BS_PLAYED_ENTER_PROMO_VEH_SCENE					RETURN "TEST_DRIVE_DATA_BS_PLAYED_ENTER_PROMO_VEH_SCENE"
		CASE TEST_DRIVE_DATA_BS_FADING_OUT_PLAYER_FOR_CUTSCENE					RETURN "TEST_DRIVE_DATA_BS_FADING_OUT_PLAYER_FOR_CUTSCENE"
		CASE TEST_DRIVE_DATA_BS_START_TRANSITION_FROM_SANDBOX_TO_SHOWROOM		RETURN "TEST_DRIVE_DATA_BS_START_TRANSITION_FROM_SANDBOX_TO_SHOWROOM"
		CASE TEST_DRIVE_DATA_BS_PASSENGER_IS_READY_TO_TRANSITION_TO_SANDBOX		RETURN "TEST_DRIVE_DATA_BS_PASSENGER_IS_READY_TO_TRANSITION_TO_SANDBOX"
		CASE TEST_DRIVE_DATA_BS_UNLOCKED_PV_I_AM_WARPING_BACK_TO				RETURN "TEST_DRIVE_DATA_BS_UNLOCKED_PV_I_AM_WARPING_BACK_TO"
		CASE TEST_DRIVE_DATA_BS_SET_TEST_DRIVE_PED_PROPERTIES					RETURN "TEST_DRIVE_DATA_BS_SET_TEST_DRIVE_PED_PROPERTIES"
		CASE TEST_DRIVE_DATA_BS_SHOULD_DISPLAY_EXIT_SANDBOX_MENU				RETURN "TEST_DRIVE_DATA_BS_SHOULD_DISPLAY_EXIT_SANDBOX_MENU"
		CASE TEST_DRIVE_DATA_BS_PURCHASE_PROMO_VEH_CONTROL_PRESSED				RETURN "TEST_DRIVE_DATA_BS_PURCHASE_PROMO_VEH_CONTROL_PRESSED"
		CASE TEST_DRIVE_DATA_BS_WARPED_NEAR_PV_AFTER_TEST_DRIVE					RETURN "TEST_DRIVE_DATA_BS_WARPED_NEAR_PV_AFTER_TEST_DRIVE"
		CASE TEST_DRIVE_DATA_BS_REQUESTED_AUDIO_BANK							RETURN "TEST_DRIVE_DATA_BS_REQUESTED_AUDIO_BANK"
		CASE TEST_DRIVE_DATA_BS_RELEASED_AUDIO_BANK								RETURN "TEST_DRIVE_DATA_BS_RELEASED_AUDIO_BANK"
		CASE TEST_DRIVE_DATA_BS_DISABLE_SANDBOX_EXIT_RAMP						RETURN "TEST_DRIVE_DATA_BS_DISABLE_SANDBOX_EXIT_RAMP"
		CASE TEST_DRIVE_DATA_BS_STARTING_TRANSITION_TO_SANDBOX_IN_PV			RETURN "TEST_DRIVE_DATA_BS_STARTING_TRANSITION_TO_SANDBOX_IN_PV"
		CASE TEST_DRIVE_DATA_BS_STARTING_TRANSITION_IN_PV_AS_PASSENGER			RETURN "TEST_DRIVE_DATA_BS_STARTING_TRANSITION_IN_PV_AS_PASSENGER"
	ENDSWITCH

	ASSERTLN("[TEST_DRIVE_SANDBOX][DEBUG_GET_TEST_DRIVE_SANDBOX_LOCAL_BD_BS_AS_STRING] - Missing name from lookup: ", iBS)
	RETURN ""
ENDFUNC

/// PURPOSE:
///    Gets the debug name test drive player broadcast data bit set as string 
DEBUGONLY FUNC STRING DEBUG_GET_TEST_DRIVE_SANDBOX_SERVER_BD_BS_AS_STRING(INT iBS)
	SWITCH iBS
		CASE SERVER_TEST_DRIVE_BD_DATA_FORCED_ROOM_FOR_PROMO_VEHICLES	RETURN "SERVER_TEST_DRIVE_BD_DATA_FORCED_ROOM_FOR_PROMO_VEHICLES"
	ENDSWITCH

	ASSERTLN("[TEST_DRIVE_SANDBOX][DEBUG_GET_TEST_DRIVE_SANDBOX_SERVER_BD_BS_AS_STRING] - Missing name from lookup: ", iBS)
	RETURN ""
ENDFUNC

#ENDIF // #IF IS_DEBUG_BUILD

// ***************************************************************************************
// 					CONFIG DATA
// ***************************************************************************************

/// PURPOSE:
///    Once the driver has started the test drive transition, we store what seat each ped in the vehicle is in. 
/// PARAMS:
///    eSeat - Current seat ped is in.
PROC SET_TEST_DRIVE_SANDBOX_VEHICLE_SEAT(VEHICLE_SEAT eSeat)
	#IF IS_DEBUG_BUILD
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[TEST_DRIVE_SANDBOX][SET_TEST_DRIVE_SANDBOX_VEHICLE_SEAT] Setting vehicle seat to: ", ENUM_TO_INT(eSeat))
	#ENDIF
	
	g_eTestDriveVehicleSeat = eSeat
ENDPROC

/// PURPOSE:
///    Returns the seat for the local player set in SET_TEST_DRIVE_SANDBOX_VEHICLE_SEAT. Used to warp the player in the sandbox area.
/// RETURNS:
///    Seat of local player. 
FUNC VEHICLE_SEAT GET_TEST_DRIVE_SANDBOX_VEHICLE_SEAT()
	RETURN g_eTestDriveVehicleSeat
ENDFUNC

PROC RESET_GLOBAL_TEST_DRIVE_PV_I_NEED_TO_WARP_BACK_TO()
	IF g_vehTestDrivePVINeedToWarpBackTo != NULL
		g_vehTestDrivePVINeedToWarpBackTo = NULL
	ENDIF
ENDPROC
FUNC INT GET_TUNER_SANDBOX_RADIO_MIX_START_MS()
	INT iStartPoint = GET_RANDOM_INT_IN_RANGE(0,5)
	SWITCH iStartPoint
		CASE 0 RETURN 69100
		CASE 1 RETURN 353200
		CASE 2 RETURN 951400
		CASE 3 RETURN 1093000
		CASE 4 RETURN 1654300
	ENDSWITCH
	
	RETURN 69100
ENDFUNC

#IF FEATURE_GEN9_EXCLUSIVE
FUNC BOOL IS_THIS_VEHICLE_INDEX_HAOS_PREMIUM_TEST_VEHICLE(INT iVehIndex)
	RETURN (iVehIndex = HAO_PREMIUM_TEST_VEH_INDEX)
ENDFUNC

FUNC BOOL IS_PLAYER_NEAR_HAO_PREMIUM_TEST_VEHICLE(TEST_DRIVE_DATA &sTestDriveData)
	RETURN IS_THIS_VEHICLE_INDEX_HAOS_PREMIUM_TEST_VEHICLE(sTestDriveData.iPromoVehIAmNearAsInt)
ENDFUNC
#ENDIF

/// PURPOSE:
///    Returns the models for the promo vehicles. 
/// PARAMS:
///    iVehicleIndex - vehicle index
/// RETURNS:
///    Model of vehicle. 
FUNC MODEL_NAMES GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_MODEL(INT iVehicleIndex)
	MODEL_NAMES eReturnModel = DUMMY_MODEL_FOR_SCRIPT
	
	SWITCH iVehicleIndex
		CASE 0		eReturnModel = INT_TO_ENUM(MODEL_NAMES, g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_MODEL_HASH)				BREAK
		CASE 1		eReturnModel = INT_TO_ENUM(MODEL_NAMES, g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_MODEL_HASH)				BREAK
		CASE 2		eReturnModel = INT_TO_ENUM(MODEL_NAMES, g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_MODEL_HASH)				BREAK
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE HAO_PREMIUM_TEST_VEH_INDEX		eReturnModel = INT_TO_ENUM(MODEL_NAMES, g_sMPTunables.iHSW_TEST_RIDE_MODEL_HASH)	BREAK
		#ENDIF
	ENDSWITCH
	
	IF eReturnModel != DUMMY_MODEL_FOR_SCRIPT
		IF IS_MODEL_VALID(eReturnModel)
			PRINTLN("[TEST_DRIVE_SANDBOX][GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_MODEL] Returning: ", GET_MODEL_NAME_FOR_DEBUG(eReturnModel))
			RETURN eReturnModel
		ELSE
			ASSERTLN("[TEST_DRIVE_SANDBOX][GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_MODEL] Invalid Model!")
		ENDIF
	ELSE
		ASSERTLN("[TEST_DRIVE_SANDBOX][GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_MODEL] Invalid vehicle Index. iVehicleIndex = ", iVehicleIndex)
	ENDIF
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE:
///    Returns the colours, mods and livery of the chosen vehicle.
/// PARAMS:
///    sData - data struct
///    iVehicleIndex - vehicle index
PROC GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_SETUP(VEHICLE_SETUP_STRUCT_MP &sData, INT iVehicleIndex, MODEL_NAMES eVehModel)
	SWITCH iVehicleIndex
		CASE 0
			sData.VehicleSetup.iColour1 								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_COLOUR_1
			sData.VehicleSetup.iColour2 								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_COLOUR_2
			sData.VehicleSetup.iColourExtra1 							= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_EXTRA_COLOUR_1
			sData.VehicleSetup.iColourExtra2 							= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_EXTRA_COLOUR_2
			sData.iColour5 												= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_COLOUR_5
			sData.iColour6 												= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_COLOUR_6
			sData.VehicleSetup.iModIndex[MOD_LIVERY] 					= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_LIVERY
			sData.VehicleSetup.iWheelType								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_WHEEL_TYPE
			sData.VehicleSetup.iModIndex[MOD_WHEELS]					= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_WHEEL_ID
			sData.VehicleSetup.iWindowTintColour						= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_WINDOW_TINT_COLOUR
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] 		= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_1_LIGHTS_COLOUR
		BREAK
		CASE 1
			sData.VehicleSetup.iColour1 								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_COLOUR_1
			sData.VehicleSetup.iColour2 								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_COLOUR_2
			sData.VehicleSetup.iColourExtra1 							= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_EXTRA_COLOUR_1
			sData.VehicleSetup.iColourExtra2 							= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_EXTRA_COLOUR_2
			sData.iColour5 												= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_COLOUR_5
			sData.iColour6 												= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_COLOUR_6
			sData.VehicleSetup.iModIndex[MOD_LIVERY] 					= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_LIVERY
			sData.VehicleSetup.iWheelType								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_WHEEL_TYPE
			sData.VehicleSetup.iModIndex[MOD_WHEELS]					= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_WHEEL_ID
			sData.VehicleSetup.iWindowTintColour						= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_WINDOW_TINT_COLOUR
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] 		= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_2_LIGHTS_COLOUR
		BREAK
		CASE 2
			sData.VehicleSetup.iColour1 								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_COLOUR_1
			sData.VehicleSetup.iColour2 								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_COLOUR_2
			sData.VehicleSetup.iColourExtra1 							= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_EXTRA_COLOUR_1
			sData.VehicleSetup.iColourExtra2 							= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_EXTRA_COLOUR_2
			sData.iColour5 												= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_COLOUR_5
			sData.iColour6 												= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_COLOUR_6
			sData.VehicleSetup.iModIndex[MOD_LIVERY] 					= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_LIVERY
			sData.VehicleSetup.iWheelType								= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_WHEEL_TYPE
			sData.VehicleSetup.iModIndex[MOD_WHEELS]					= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_WHEEL_ID
			sData.VehicleSetup.iWindowTintColour						= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_WINDOW_TINT_COLOUR
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] 		= g_sMPTunables.iPROMO_TEST_DRIVE_VEHICLE_3_LIGHTS_COLOUR
		BREAK
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE HAO_PREMIUM_TEST_VEH_INDEX
			sData.VehicleSetup.iColour1 								= g_sMPTunables.iHSW_TEST_RIDE_COLOUR_1
			sData.VehicleSetup.iColour2 								= g_sMPTunables.iHSW_TEST_RIDE_COLOUR_2
			sData.VehicleSetup.iColourExtra1 							= g_sMPTunables.iHSW_TEST_RIDE_EXTRA_COLOUR_1
			sData.VehicleSetup.iColourExtra2 							= g_sMPTunables.iHSW_TEST_RIDE_EXTRA_COLOUR_2
			sData.iColour5 												= g_sMPTunables.iHSW_TEST_RIDE_COLOUR_5
			sData.iColour6 												= g_sMPTunables.iHSW_TEST_RIDE_COLOUR_6
			sData.VehicleSetup.iModIndex[MOD_LIVERY] 					= g_sMPTunables.iHSW_TEST_RIDE_LIVERY
			sData.VehicleSetup.iWheelType								= g_sMPTunables.iHSW_TEST_RIDE_WHEEL_TYPE
			sData.VehicleSetup.iModIndex[MOD_WHEELS]					= g_sMPTunables.iHSW_TEST_RIDE_WHEEL_ID
			
			IF IS_THIS_MODEL_A_BIKE(eVehModel)
				sData.VehicleSetup.iModIndex[MOD_REAR_WHEELS]			= g_sMPTunables.iHSW_TEST_RIDE_WHEEL_ID
			ENDIF
			
			sData.VehicleSetup.iWindowTintColour						= g_sMPTunables.iHSW_TEST_RIDE_WINDOW_TINT_COLOUR
			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS] 		= g_sMPTunables.iHSW_TEST_RIDE_XENON_LIGHTS
			
			sData.VehicleSetup.iModIndex[MOD_BRAKES]					= g_sMPTunables.iHSW_TEST_RIDE_BREAKS
			sData.VehicleSetup.iModIndex[MOD_ENGINE]					= g_sMPTunables.iHSW_TEST_RIDE_ENGINE
			sData.VehicleSetup.iModIndex[MOD_SUSPENSION]				= g_sMPTunables.iHSW_TEST_RIDE_SUSPENSION
			sData.VehicleSetup.iModIndex[MOD_GEARBOX]					= g_sMPTunables.iHSW_TEST_RIDE_TRANSMISSION
			sData.VehicleSetup.iModIndex[MOD_SPOILER]					= g_sMPTunables.iHSW_TEST_RIDE_SPOILER

			sData.VehicleSetup.iModIndex[MOD_ICE] 						= g_sMPTunables.iHSW_TEST_RIDE_MOD_ICE
			sData.VehicleSetup.iModIndex[MOD_KNOB]						= g_sMPTunables.iHSW_TEST_RIDE_HYBRID_TURBO
			
			sData.VehicleSetup.iModIndex[MOD_BUMPER_F]					= g_sMPTunables.iHSW_TEST_RIDE_MOD_BUMPER_F
			sData.VehicleSetup.iModIndex[MOD_BUMPER_R]					= g_sMPTunables.iHSW_TEST_RIDE_MOD_BUMPER_R
			sData.VehicleSetup.iModIndex[MOD_SKIRT]						= g_sMPTunables.iHSW_TEST_RIDE_MOD_SKIRT
			sData.VehicleSetup.iModIndex[MOD_EXHAUST]					= g_sMPTunables.iHSW_TEST_RIDE_MOD_EXHAUST
			sData.VehicleSetup.iModIndex[MOD_CHASSIS]					= g_sMPTunables.iHSW_TEST_RIDE_MOD_CHASSIS
			sData.VehicleSetup.iModIndex[MOD_GRILL]						= g_sMPTunables.iHSW_TEST_RIDE_MOD_GRILL
			sData.VehicleSetup.iModIndex[MOD_BONNET]					= g_sMPTunables.iHSW_TEST_RIDE_MOD_BONNET
			sData.VehicleSetup.iModIndex[MOD_WING_L]					= g_sMPTunables.iHSW_TEST_RIDE_MOD_WING_L
			sData.VehicleSetup.iModIndex[MOD_WING_R]					= g_sMPTunables.iHSW_TEST_RIDE_MOD_WING_R
			sData.VehicleSetup.iModIndex[MOD_ROOF]						= g_sMPTunables.iHSW_TEST_RIDE_MOD_ROOF
			sData.VehicleSetup.iNeonR									= g_sMPTunables.iHSW_TEST_RIDE_NEON_R
			sData.VehicleSetup.iNeonG									= g_sMPTunables.iHSW_TEST_RIDE_NEON_G
			sData.VehicleSetup.iNeonB									= g_sMPTunables.iHSW_TEST_RIDE_NEON_B
			
			IF g_sMPTunables.bHSW_TEST_RIDE_NEON_FRONT
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
			ENDIF
			
			IF g_sMPTunables.bHSW_TEST_RIDE_NEON_BACK
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
			ENDIF
			
			IF g_sMPTunables.bHSW_TEST_RIDE_NEON_LEFT
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
			ENDIF
			
			IF g_sMPTunables.bHSW_TEST_RIDE_NEON_RIGHT
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
			ENDIF
			
			IF g_sMPTunables.bHSW_TEST_RIDE_TIRES
				SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
			ENDIF
			
			IF g_sMPTunables.bHSW_TEST_RIDE_CUSTOM_TIRE_DESIGN
				sData.VehicleSetup.iModVariation[0] = 1
				
				IF IS_THIS_MODEL_A_BIKE(eVehModel)
					sData.VehicleSetup.iModVariation[1] = 1
				ENDIF
			ENDIF
			
			// Speicifc vehicles
			IF eVehModel = BANSHEE
				IF  g_sMPTunables.bBANSHEE_TEST_RIDE_TOPLESS
					// No Roof
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
					CLEAR_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
					PRINTLN("[TEST_DRIVE_SANDBOX][GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_COORDS] banshee no roof")
				ELSE
					// Roof
					CLEAR_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0)
					SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_1)
					PRINTLN("[TEST_DRIVE_SANDBOX][GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_COORDS] banshee roof")
				ENDIF
			ENDIF
		BREAK
		#ENDIF
		DEFAULT
			ASSERTLN("[TEST_DRIVE_SANDBOX][GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_COORDS] Invalid vehicle Index. iVehicleIndex = ", iVehicleIndex)
		BREAK
	ENDSWITCH
	
	ADD_PURCHASE_VEHICLE_EXTRAS(eVehModel, sData.VehicleSetup)
ENDPROC

/// PURPOSE:
///    Returns the coords of the promo vehicles on display. 
/// PARAMS:
///    iVehicleIndex - vehcile index
/// RETURNS:
///    Coords of vehicle. 
FUNC VECTOR GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_COORDS(INT iVehicleIndex)
	//IF IS PLAYER IN CAR MEET INTERIOR
	
		SWITCH iVehicleIndex
			CASE 0		RETURN <<-2143.9045, 1114.7545, -24.7814>>
			CASE 1		RETURN <<-2144.2776, 1119.2275, -24.7000>>
			CASE 2		RETURN <<-2144.2976, 1123.8685, -24.5620>>
			#IF FEATURE_GEN9_EXCLUSIVE
			CASE HAO_PREMIUM_TEST_VEH_INDEX		RETURN <<-2144.8108, 1148.2618, -25.0481>>
			#ENDIF
		ENDSWITCH
	
	// ENDIF
	
	ASSERTLN("[TEST_DRIVE_SANDBOX][GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_COORDS] Invalid vehicle Index. iVehicleIndex = ", iVehicleIndex)
	RETURN <<0, 0, 0>>
ENDFUNC

/// PURPOSE:
///    Returns the rotation of the promo vehicles on display. 
/// PARAMS:
///    iVehicleIndex - vehcile index
/// RETURNS:
///    Rotation of vehicle. 
FUNC VECTOR GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_ROTATION(INT iVehicleIndex)
	// IF IS PLAYER IN CAR MEET INTERIOR
	SWITCH iVehicleIndex
		CASE 0		RETURN <<0.8102, 0.0018, 116.6498>>
		CASE 1		RETURN <<-0.3616, 0.0296, 89.9025>>
		CASE 2		RETURN <<-0.0032, -0.0007, 61.1236>>
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE HAO_PREMIUM_TEST_VEH_INDEX		RETURN <<-0.0050, 0.0009, 125.0000>>
		#ENDIF
	ENDSWITCH
	// ENDIF
	
	ASSERTLN("[TEST_DRIVE_SANDBOX][GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_ROTATION] Invalid vehicle Index. iVehicleIndex = ", iVehicleIndex)
	RETURN <<0, 0, 0>>
ENDFUNC

FUNC VECTOR GET_INDIVIDUAL_TEST_DRIVE_SANDBOX_VEHICLE_SPAWN_COORD(INT iSpawnIndex)
	SWITCH iSpawnIndex
		CASE 0		RETURN <<-2121.0701, 1103.7826, 24.6638>>
		CASE 1		RETURN <<-2121.3042, 1100.0680, 24.6637>>
		CASE 2		RETURN <<-2121.5190, 1095.7183, 24.6637>>
		CASE 3		RETURN <<-2121.3826, 1091.1252, 24.6637>>
		CASE 4		RETURN <<-2122.5291, 1082.5858, 24.6636>>
		CASE 5		RETURN <<-2122.7393, 1078.3519, 24.6635>>
		CASE 6		RETURN <<-2122.8813, 1073.8705, 24.6635>>
		CASE 7		RETURN <<-2122.7500, 1069.9944, 24.6635>>
		CASE 8		RETURN <<-2121.8899, 1066.0487, 24.6636>>
		CASE 9		RETURN <<-2121.5513, 1107.5614, 24.6637>>
		CASE 10		RETURN <<-2121.5488, 1114.9553, 24.6637>>
		CASE 11		RETURN <<-2121.2864, 1119.5732, 24.6637>>
		CASE 12		RETURN <<-2120.8547, 1124.4540, 24.6638>>
		CASE 13		RETURN <<-2121.4114, 1128.7777, 24.6637>>
		CASE 14		RETURN <<-2121.3337, 1133.1927, 24.6637>>
		CASE 15		RETURN <<-2121.6921, 1139.3795, 24.6637>>
		CASE 16		RETURN <<-2121.6833, 1143.0996, 24.6637>>
		CASE 17		RETURN <<-2121.1499, 1147.1212, 24.6638>>
		CASE 18		RETURN <<-2121.6252, 1151.3705, 24.6637>>
		CASE 19		RETURN <<-2122.2344, 1156.9202, 24.6636>>
		CASE 20		RETURN <<-2122.1516, 1163.8248, 24.6640>>
		CASE 21		RETURN <<-2121.8650, 1168.1184, 24.6644>>
		CASE 22		RETURN <<-2121.6331, 1172.1259, 24.6648>>
		CASE 23		RETURN <<-2121.8472, 1176.4264, 24.6653>>
		CASE 24		RETURN <<-2121.6428, 1180.8553, 24.6657>>
		CASE 25		RETURN <<-2122.3799, 1056.4399, 24.6641>>
		CASE 26		RETURN <<-2122.4680, 1052.6310, 24.6645>>
		CASE 27		RETURN <<-2122.0840, 1048.7321, 24.6649>>
		CASE 28		RETURN <<-2125.3809, 1044.5676, 24.6653>>
		CASE 29		RETURN <<-2127.1084, 1039.3708, 24.6659>>
		CASE 30		RETURN <<-2129.0000, 1039.3708, 24.6659>>
		CASE 31		RETURN <<-2130.1084, 1039.3708, 24.6659>>
	ENDSWITCH
	
	ASSERTLN("[TEST_DRIVE_SANDBOX][GET_INDIVIDUAL_TEST_DRIVE_SANDBOX_VEHICLE_SPAWN_COORD] Invalid vspawn Index. iSpawnIndex = ", iSpawnIndex)
	RETURN <<-2124.0701, 1103.7826, 24.6638>>
ENDFUNC

FUNC FLOAT GET_INDIVIDUAL_TEST_DRIVE_SANDBOX_VEHICLE_SPAWN_HEADING(INT iSpawnIndex)
	SWITCH iSpawnIndex
		CASE 0		RETURN 269.9998
		CASE 1		RETURN 266.5996
		CASE 2		RETURN 266.5996
		CASE 3		RETURN 267.7996
		CASE 4		RETURN 267.7995
		CASE 5		RETURN 267.7995
		CASE 6		RETURN 267.7995
		CASE 7		RETURN 267.7995
		CASE 8		RETURN 267.7995
		CASE 9		RETURN 272.5992
		CASE 10		RETURN 272.5992
		CASE 11		RETURN 272.5992
		CASE 12		RETURN 272.5992
		CASE 13		RETURN 268.3991
		CASE 14		RETURN 268.3991
		CASE 15		RETURN 269.5991
		CASE 16		RETURN 269.5991
		CASE 17		RETURN 269.5991
		CASE 18		RETURN 273.7990
		CASE 19		RETURN 268.1989
		CASE 20		RETURN 272.3988
		CASE 21		RETURN 272.3988
		CASE 22		RETURN 272.3988
		CASE 23		RETURN 272.3988
		CASE 24		RETURN 270.3987
		CASE 25		RETURN 270.3987
		CASE 26		RETURN 270.3987
		CASE 27		RETURN 270.3987
		CASE 28		RETURN 270.3987
		CASE 29		RETURN 270.3987
		CASE 30		RETURN 270.3987
		CASE 31		RETURN 270.3987
	ENDSWITCH
	
	ASSERTLN("[TEST_DRIVE_SANDBOX][GET_TEST_DRIVE_SANDBOX_VEHICLE_SPAWN_HEADING] Invalid vspawn Index. iSpawnIndex = ", iSpawnIndex)
	RETURN 269.9998
ENDFUNC

/// PURPOSE:
///    Finds a spawn point for the test drive vehicle, created when entering the sandbox area.
/// PARAMS:
///    ref_vCoord - Spawn Coord
///    ref_fHeading - Spawn heading
/// RETURNS:
///    TRUE when a safe spawn point is found.
FUNC BOOL GET_TEST_DRIVE_SANDBOX_VEHICLE_SPAWN_COORDS(VECTOR &ref_vCoord, FLOAT &ref_fHeading)
	INT iSpawnPoint
	REPEAT NUM_NETWORK_PLAYERS iSpawnPoint
		VECTOR _vCoord = GET_INDIVIDUAL_TEST_DRIVE_SANDBOX_VEHICLE_SPAWN_COORD(iSpawnPoint)
		FLOAT _fHeading = GET_INDIVIDUAL_TEST_DRIVE_SANDBOX_VEHICLE_SPAWN_HEADING(iSpawnPoint)
		
		IF IS_POINT_OK_FOR_NET_ENTITY_CREATION(_vCoord, 3.5, DEFAULT, DEFAULT, DEFAULT, FALSE, FALSE, FALSE, DEFAULT, TRUE, DEFAULT, FALSE, DEFAULT, TRUE, DEFAULT, DEFAULT, DEFAULT)
			ref_vCoord = _vCoord
			ref_fHeading = _fHeading
			
			PRINTLN("[GET_TEST_DRIVE_SANDBOX_VEHICLE_SPAWN_COORDS] *** Found spawn point ***")
			PRINTLN("[GET_TEST_DRIVE_SANDBOX_VEHICLE_SPAWN_COORDS] Spawn Index: ", iSpawnPoint)
			PRINTLN("[GET_TEST_DRIVE_SANDBOX_VEHICLE_SPAWN_COORDS] Spawn Coord: ", GET_STRING_FROM_VECTOR(ref_vCoord))
			PRINTLN("[GET_TEST_DRIVE_SANDBOX_VEHICLE_SPAWN_COORDS] Spawn Heading: ", GET_STRING_FROM_FLOAT(ref_fHeading))
			
			RETURN TRUE
		ELIF iSpawnPoint = (NUM_NETWORK_PLAYERS - 1)
			PRINTLN("[GET_TEST_DRIVE_SANDBOX_VEHICLE_SPAWN_COORDS] None of the spawn points are safe.")
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

// ***************************************************************************************
// 					HELPERS
// ***************************************************************************************

PROC SET_TEST_DRIVE_MENU_GRAPHIC(TEST_DRIVE_DATA &sTestDriveData, TEST_DRIVE_MENU_GRAPHIC eNewGraphic)
	IF sTestDriveData.eMenuGraphic != eNewGraphic
		sTestDriveData.eMenuGraphic = eNewGraphic
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets test drive player broadcast data bit set
/// PARAMS:
///    iBit - bit to set
///    bSet - true to set bit or false to clear
PROC SET_TEST_DRIVE_SANDBOX_PLAYER_BROADCAST_DATA_BS(TEST_DRIVE_DATA &sTestDriveData, INT iBit, BOOL bSet)
	IF PLAYER_ID() = INVALID_PLAYER_INDEX()
		PRINTLN("[TEST_DRIVE_SANDBOX][SET_TEST_DRIVE_SANDBOX_PLAYER_BROADCAST_DATA_BS] Invalid player ID")
		EXIT
	ENDIF
	
	IF bSet
		IF NOT IS_BIT_SET(sTestDriveData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			SET_BIT(sTestDriveData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			PRINTLN("[TEST_DRIVE_SANDBOX][SET_TEST_DRIVE_SANDBOX_PLAYER_BROADCAST_DATA_BS] ", DEBUG_GET_TEST_DRIVE_SANDBOX_PLAYER_BD_BS_AS_STRING(iBit), " - TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(sTestDriveData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			CLEAR_BIT(sTestDriveData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iBS, iBit)
			PRINTLN("[TEST_DRIVE_SANDBOX][SET_TEST_DRIVE_SANDBOX_PLAYER_BROADCAST_DATA_BS] ", DEBUG_GET_TEST_DRIVE_SANDBOX_PLAYER_BD_BS_AS_STRING(iBit), " - FALSE")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if test drive player broadcast data bit is set
/// PARAMS:
///    iBit - bit to set
FUNC BOOL IS_TEST_DRIVE_SANDBOX_PLAYER_BROADCAST_DATA_BIT_SET(TEST_DRIVE_DATA &sTestDriveData, PLAYER_INDEX playerTocheck, INT iBit)
	IF playerTocheck = INVALID_PLAYER_INDEX()
		PRINTLN("[TEST_DRIVE_SANDBOX][IS_TEST_DRIVE_SANDBOX_PLAYER_BROADCAST_DATA_BIT_SET] invalid playerTocheck")
		RETURN FALSE
	ENDIF
	
	RETURN IS_BIT_SET(sTestDriveData.playerBD[NATIVE_TO_INT(playerTocheck)].iBS, iBit)
ENDFUNC

/// PURPOSE:
///    Sets test drive local bit set
/// PARAMS:
///    iBit - bit to set
///    bSet - true to set bit or false to clear
PROC SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(TEST_DRIVE_DATA &sTestDriveData, INT iBit, BOOL bSet)
	IF bSet
		IF NOT IS_BIT_SET(sTestDriveData.iBS, iBit)
			SET_BIT(sTestDriveData.iBS, iBit)
			PRINTLN("[TEST_DRIVE_SANDBOX][SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS] ", DEBUG_GET_TEST_DRIVE_SANDBOX_LOCAL_BD_BS_AS_STRING(iBit), " TRUE")
		ENDIF
	ELSE
		IF IS_BIT_SET(sTestDriveData.iBS, iBit)
			CLEAR_BIT(sTestDriveData.iBS, iBit)
			PRINTLN("[TEST_DRIVE_SANDBOX][SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS] ", DEBUG_GET_TEST_DRIVE_SANDBOX_LOCAL_BD_BS_AS_STRING(iBit), " FALSE")
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if test drive sandbox player broadcast data bit is set
/// PARAMS:
///    iBit - bit to set
FUNC BOOL IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(TEST_DRIVE_DATA &sTestDriveData, INT iBit)
	RETURN IS_BIT_SET(sTestDriveData.iBS, iBit)
ENDFUNC

/// PURPOSE:
///    Sets test drive server bit set
/// PARAMS:
///    iBit - bit to set
///    bSet - true to set bit or false to clear
PROC SET_TEST_DRIVE_SANDBOX_SERVER_DATA_BS(SERVER_TEST_DRIVE_BD_DATA &sTestDriveData, INT iBit, BOOL bSet)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		IF bSet
			IF NOT IS_BIT_SET(sTestDriveData.iBS, iBit)
				SET_BIT(sTestDriveData.iBS, iBit)
				PRINTLN("[TEST_DRIVE_SANDBOX][SET_TEST_DRIVE_SANDBOX_SERVER_DATA_BS] ", DEBUG_GET_TEST_DRIVE_SANDBOX_SERVER_BD_BS_AS_STRING(iBit), " TRUE")
			ENDIF
		ELSE
			IF IS_BIT_SET(sTestDriveData.iBS, iBit)
				CLEAR_BIT(sTestDriveData.iBS, iBit)
				PRINTLN("[TEST_DRIVE_SANDBOX][SET_TEST_DRIVE_SANDBOX_SERVER_DATA_BS] ", DEBUG_GET_TEST_DRIVE_SANDBOX_SERVER_BD_BS_AS_STRING(iBit), " FALSE")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Check if test drive sandbox server broadcast data bit is set
/// PARAMS:
///    iBit - bit to set
FUNC BOOL IS_TEST_DRIVE_SANDBOX_SERVER_DATA_BIT_SET(SERVER_TEST_DRIVE_BD_DATA &sTestDriveData, INT iBit)
	RETURN IS_BIT_SET(sTestDriveData.iBS, iBit)
ENDFUNC

/// PURPOSE:
///    Clears all test drive bit sets
PROC CLEAR_ALL_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(TEST_DRIVE_DATA &sTestDriveData)
	sTestDriveData.iBS = 0
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[TEST_DRIVE_SANDBOX][CLEAR_ALL_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS] Called. ")
ENDPROC

/// PURPOSE:
///    Set a new test drive cutscene state while printing out debug
/// PARAMS:
///    sTestDriveData - data struct
///    eNewState - New state
PROC SET_TEST_DRIVE_SANDBOX_CUTSCENE_STATE(TEST_DRIVE_DATA &sTestDriveData, TEST_DRIVE_CUTSCENE_STATE eNewState)
	IF sTestDriveData.eCutsceneState != eNewState
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[TEST_DRIVE_SANDBOX][SET_TEST_DRIVE_SANDBOX_CUTSCENE_STATE] Setting state from ", GET_TEST_DRIVE_CUTSCENE_STATE_AS_STRING(sTestDriveData.eCutsceneState), " to ", GET_TEST_DRIVE_CUTSCENE_STATE_AS_STRING(eNewState))
		sTestDriveData.eCutsceneState = eNewState
	ENDIF
ENDPROC

/// PURPOSE:
///    Usually used by the driver, to tell passengers what lobby instance to join.
/// PARAMS:
///    sTestDriveData - Data struct 
///    iSelection - Selection
PROC SET_TEST_DRIVE_SANDBOX_PLAYER_BROADCAST_DATA_INSTANCE(TEST_DRIVE_DATA &sTestDriveData, INT iNewInstance)
	IF sTestDriveData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iInstance != iNewInstance
		sTestDriveData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iInstance = iNewInstance
		PRINTLN("[TEST_DRIVE_SANDBOX][SET_TEST_DRIVE_SANDBOX_PLAYER_BROADCAST_DATA_INSTANCE] Setting new instance: ", iNewInstance)
	ENDIF
ENDPROC

/// PURPOSE:
///    Usually used by passengers to see what menu selection the driver has chosen
/// PARAMS:
///    sTestDriveData - data struct
///    playerTocheck - player
/// RETURNS:
///    Menu selection of driver.
FUNC INT GET_PLAYER_BROADCAST_DATA_INSTANCE(TEST_DRIVE_DATA &sTestDriveData, PLAYER_INDEX playerTocheck)
	IF playerTocheck = INVALID_PLAYER_INDEX()
		ASSERTLN("[TEST_DRIVE_SANDBOX][GET_PLAYER_BROADCAST_DATA_INSTANCE] invalid playerTocheck. Returning -1")
		RETURN -1
	ENDIF
	
	RETURN sTestDriveData.playerBD[NATIVE_TO_INT(playerTocheck)].iInstance
ENDFUNC

/// PURPOSE:
///    Cleans up the instance set by the driver.
/// PARAMS:
///    sTestDriveData - Data struct 
///    iSelection - Selection
PROC REINIT_TEST_DRIVE_SANDBOX_PLAYER_BROADCAST_DATA_INSTANCE(TEST_DRIVE_DATA &sTestDriveData)
	IF sTestDriveData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iInstance != -99
		sTestDriveData.playerBD[NATIVE_TO_INT(PLAYER_ID())].iInstance = -99
		PRINTLN("[REINIT_TEST_DRIVE_SANDBOX_PLAYER_BROADCAST_DATA_INSTANCE] Called")
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the driver of the new test vehicle to warp in.
/// PARAMS:
///    pDriver - player
PROC SET_DRIVER_OF_NEW_TEST_DRIVE_VEHICLE(PLAYER_INDEX pDriver)
	IF pDriver != INVALID_PLAYER_INDEX()
		PRINTLN("[SET_DRIVER_OF_TEST_DRIVE_VEHICLE] Driver: ", GET_PLAYER_NAME(pDriver))
		g_piDriverOfTestDriveVehicle = pDriver
	ELSE
		ASSERTLN("[SET_DRIVER_OF_TEST_DRIVE_VEHICLE] Invalid player id.")
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the driver of the new test vehicle to warp in.
/// RETURNS:
///    PLayer / driver.
FUNC PLAYER_INDEX GET_DRIVER_OF_NEW_TEST_DRIVE_VEHICLE()
	RETURN g_piDriverOfTestDriveVehicle
ENDFUNC

/// PURPOSE:
///    Register car meet player broadcast data
/// PARAMS:
///    sTestDriveData - car meet player broadcast data struct
PROC REGISTER_TEST_DRIVE_SANDBOX_PLAYER_BROADCAST_DATA(TEST_DRIVE_DATA &sTestDriveData)
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(sTestDriveData.playerBD, SIZE_OF(sTestDriveData.playerBD))
	PRINTLN("[TEST_DRIVE_SANDBOX][REGISTER_TEST_DRIVE_SANDBOX_PLAYER_BROADCAST_DATA] Registered player broadcast data")
ENDPROC

/// PURPOSE:
///    Register car meet server broadcast data
/// PARAMS:
///    serverBD - car meet server broadcast data struct
PROC REGISTER_TEST_DRIVE_SANDBOX_SERVER_BROADCAST_DATA(SERVER_TEST_DRIVE_BD_DATA &serverBD)
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	PRINTLN("[TEST_DRIVE_SANDBOX][REGISTER_TEST_DRIVE_SANDBOX_SERVER_BROADCAST_DATA] Registered server broadcast data")
ENDPROC

/// PURPOSE:
///    Gets the control action for starting the test drive sandbox.
/// RETURNS:
///    Control action for keyboard and mouse.
FUNC CONTROL_ACTION GET_START_TEST_DRIVE_SANDBOX_CONTROL_ACTION()
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		RETURN INPUT_SCRIPT_RDOWN
	ENDIF
	
	RETURN INPUT_FRONTEND_ACCEPT
ENDFUNC

/// PURPOSE:
///    Gets the control action for starting the test drive sandbox.
/// RETURNS:
///    Control action for keyboard and mouse.
FUNC CONTROL_ACTION GET_START_TEST_DRIVE_SANDBOX_PROMO_VEHICLE_CONTROL_ACTION()
	//IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	//	RETURN INPUT_CURSOR_ACCEPT
	//ENDIF
	
	RETURN INPUT_ENTER
ENDFUNC

/// PURPOSE:
///    Detects when the player has pressed the prompt to start a test drive in a players vehicle.
/// RETURNS:
///    TRUE if prompt is pressed.
FUNC BOOL IS_START_TEST_DRIVE_SANDBOX_CONTROL_PRESSED()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	
		CONTROL_ACTION _eControlAction
		BOOL bUseJustReleased = FALSE
		
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			_eControlAction = GET_START_TEST_DRIVE_SANDBOX_CONTROL_ACTION()
			bUseJustReleased = TRUE
		ELSE
			_eControlAction = GET_START_TEST_DRIVE_SANDBOX_PROMO_VEHICLE_CONTROL_ACTION()
		ENDIF
		
		IF (bUseJustReleased)
			IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, _eControlAction)
			OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, _eControlAction)
				RETURN TRUE
			ENDIF
		ELSE
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, _eControlAction)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, _eControlAction)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC GET_PROMO_VEH_PURCHASE_CONTROLS(CONTROL_TYPE &eControlType, CONTROL_ACTION &eControlAction)
	eControlType 	= PLAYER_CONTROL
	eControlAction 	= INPUT_SCRIPT_RLEFT
ENDPROC

FUNC BOOL IS_PURCHASE_TEST_DRIVE_VEHICLE_CONTROL_PRESSED()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			CONTROL_TYPE eControlType
			CONTROL_ACTION eControlAction
			
			GET_PROMO_VEH_PURCHASE_CONTROLS(eControlType, eControlAction)
			IF IS_CONTROL_JUST_PRESSED(eControlType, eControlAction)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Disables all the controls for test drive vehicles in the show room.
PROC DISABLE_PLAYER_TEST_DRIVE_VEHICLE_CONTROLS_THIS_FRAME()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HORN)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_NEXT_RADIO)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PREV_RADIO)
ENDPROC

/// PURPOSE:
///    Draws a progress meter while the player is holding VEH_EXIT.
/// PARAMS:
///    sTestDriveData - data struct 
/// RETURNS:
///    TRUE when control is held for EXIT_TEST_VEH_HOLD_TIME milliseconds.
FUNC BOOL IS_EXIT_TEST_DRIVE_CONTROL_HELD(TEST_DRIVE_DATA &sTestDriveData, CONTROL_TYPE eControlType, CONTROL_ACTION eControlAction)
	sTestDriveData.sExitVehHoldTimer.control = eControlType
	sTestDriveData.sExitVehHoldTimer.action = eControlAction
	
	IF IS_CONTROL_HELD(sTestDriveData.sExitVehHoldTimer, TRUE, EXIT_TEST_VEH_HOLD_TIME)
		RETURN TRUE
	ELIF IS_DISABLED_CONTROL_PRESSED(eControlType, eControlAction)
		INT iTimeHeld = GET_ABS_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(sTestDriveData.sExitVehHoldTimer.sTimer)
		DRAW_GENERIC_METER(iTimeHeld, EXIT_TEST_VEH_HOLD_TIME, "TDR_SANDBOX_1", DEFAULT, DEFAULT, HUDORDER_TOP)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_APPROACHED_TEST_AREA_RAMP(TEST_DRIVE_DATA &sTestDriveData)
	UNUSED_PARAMETER(sTestDriveData)
	
	#IF IS_DEBUG_BUILD
	IF sTestDriveData.sDebugData.bDisableRamp
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		VECTOR vRampMin = <<-2148.613281,1106.073975,31.213045>>
		VECTOR vRampMax = <<-2132.121338,1106.088745,24.412294>>
		FLOAT fRampWidth = 10.000000
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vRampMin, vRampMax, fRampWidth)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PLAYER_MANUALLY_EXIT_TEST_DRIVE()

	IF GB_GET_CONTRABAND_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = ENUM_TO_INT(SAV_CHECKPOINT_DASH)
		PRINTLN("[CAN_PLAYER_MANUALLY_EXIT_TEST_DRIVE] FALSE - SAV_CHECKPOINT_DASH")
		RETURN FALSE
	ENDIF
	
	IF GB_GET_CONTRABAND_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = ENUM_TO_INT(SAV_HEAD_TO_HEAD)
		PRINTLN("[CAN_PLAYER_MANUALLY_EXIT_TEST_DRIVE] FALSE - SAV_HEAD_TO_HEAD")
		RETURN FALSE
	ENDIF
	
	IF GB_IS_GLOBAL_NON_BD_BIT1_SET(eGB_GLOBAL_NON_BD_BITSET_1_DISABLE_QUIT_MID_MISSION)
		PRINTLN("[CAN_PLAYER_MANUALLY_EXIT_TEST_DRIVE] FALSE - eGB_GLOBAL_NON_BD_BITSET_1_DISABLE_QUIT_MID_MISSION")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		PRINTLN("[CAN_PLAYER_MANUALLY_EXIT_TEST_DRIVE] FALSE - IS_PLAYER_IN_CORONA")
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		PRINTLN("[CAN_PLAYER_MANUALLY_EXIT_TEST_DRIVE] FALSE - IS_PHONE_ONSCREEN")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// **********************************************
// 					VEHICLES
// **********************************************

/// PURPOSE:
///    Used in MAINTAIN_TEST_DRIVE_WARP_BACK_INTO_PV so the driver can request control of their PV properly.
PROC WARP_LOCAL_PLAYER_TO_THEIR_PV_AFTER_TEST_DRIVE(TEST_DRIVE_DATA &sTestDriveData, VEHICLE_INDEX personalVeh)
	IF NOT IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(sTestDriveData, TEST_DRIVE_DATA_BS_WARPED_NEAR_PV_AFTER_TEST_DRIVE)
		IF IS_VEHICLE_OK(personalVeh)
		AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
			VECTOR vPVCoords = GET_ENTITY_COORDS(personalVeh)
			VECTOR vWarpCoords = vPVCoords + <<0, 0, 1>>
			
			IF IS_NET_PLAYER_OK(PLAYER_ID())
				IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vWarpCoords, <<1,1,1>>)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpCoords)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_WARPED_NEAR_PV_AFTER_TEST_DRIVE, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the model of the vehicle set in GET_TEST_DRIVE_SANDBOX_VEHICLE_SETUP_MP
/// RETURNS:
///    Model of the vehicle. 
FUNC MODEL_NAMES GET_TEST_DRIVE_SANDBOX_VEHICLE_MODEL()
	RETURN g_eTestDriveVehicleModel
ENDFUNC

/// PURPOSE:
///    Sets the model of the new test drive. vehicle
/// RETURNS:
///    Model of the vehicle. 
PROC SET_TEST_DRIVE_SANDBOX_VEHICLE_MODEL(MODEL_NAMES eModel)
	IF NOT IS_MODEL_A_VEHICLE(eModel)
	AND (eModel != DUMMY_MODEL_FOR_SCRIPT)
		ASSERTLN("[TEST_DRIVE_SANDBOX][SET_TEST_DRIVE_SANDBOX_VEHICLE_MODEL] Model is not a vehicle: ", GET_MODEL_NAME_FOR_DEBUG(eModel))
		EXIT
	ENDIF
	
	g_eTestDriveVehicleModel = eModel
ENDPROC

/// PURPOSE:
///    Gets the setup of the vehicle (created by script A) which the player is about to test drive. Call for 1 frame
/// PARAMS:
///    vehToClone - Vehicle to clone.
PROC GET_TEST_DRIVE_SANDBOX_VEHICLE_SETUP_MP(VEHICLE_INDEX vehToClone)
	IF IS_ENTITY_ALIVE(vehToClone)
		RESET_VEHICLE_SETUP_STRUCT_MP(g_sTestDriveVehicleSetup)
		GET_VEHICLE_SETUP_MP(vehToClone, g_sTestDriveVehicleSetup)
		SET_TEST_DRIVE_SANDBOX_VEHICLE_MODEL(GET_ENTITY_MODEL(vehToClone))
		
		DEBUG_PRINTCALLSTACK()
		PRINTLN("[TEST_DRIVE_SANDBOX][GET_TEST_DRIVE_SANDBOX_VEHICLE_SETUP_MP] Called.")
	ELSE
		PRINTLN("[TEST_DRIVE_SANDBOX][GET_TEST_DRIVE_SANDBOX_VEHICLE_SETUP_MP] Vehicle is not alive.")
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets up the new vehcile (created by script B) which the player is about to test drive. Call for 1 frame.
/// PARAMS:
///    newVehicle - Newly created vehicle.
PROC SET_TEST_DRIVE_SANDBOX_VEHICLE_SETUP_MP(VEHICLE_INDEX &newVehicle)
	IF IS_ENTITY_ALIVE(newVehicle)
		IF (GET_TEST_DRIVE_SANDBOX_VEHICLE_MODEL() != DUMMY_MODEL_FOR_SCRIPT)
			SET_VEHICLE_SETUP_MP(newVehicle, g_sTestDriveVehicleSetup)
			
			DEBUG_PRINTCALLSTACK()
			PRINTLN("[TEST_DRIVE_SANDBOX][SET_TEST_DRIVE_SANDBOX_VEHICLE_SETUP_MP] Called.")
		ELSE
			ASSERTLN("[TEST_DRIVE_SANDBOX][SET_TEST_DRIVE_SANDBOX_VEHICLE_SETUP_MP] Dummy model detected. Be sure to call GET_TEST_DRIVE_SANDBOX_VEHICLE_SETUP_MP first.")
		ENDIF
	ELSE
		ASSERTLN("[TEST_DRIVE_SANDBOX][SET_TEST_DRIVE_SANDBOX_VEHICLE_SETUP_MP] Vehicle is not alive.")
	ENDIF
ENDPROC

FUNC VEHICLE_INDEX GET_TEST_DRIVE_PROMO_VEH_I_AM_NEAR(TEST_DRIVE_DATA &sTestDriveData)
	RETURN sTestDriveData.viPromoVehIAmNear
ENDFUNC

/// PURPOSE:
///    Detects when the local player is near any of the promo test drive vehicles. 
/// PARAMS:
///    serverBD - server struct
/// RETURNS:
///    TRUE if player is near.
FUNC BOOL IS_PLAYER_NEAR_ANY_PROMO_TEST_DRIVE_SANDBOX_VEHICLE(TEST_DRIVE_DATA &sTestDriveData, SERVER_TEST_DRIVE_BD_DATA &serverBD, FLOAT fRange = NEAR_PROMO_TEST_DRIVE_VEHICLE_RANGE_DEFAULT)
	IF NETWORK_IS_PLAYER_A_PARTICIPANT(PLAYER_ID())
	AND NETWORK_IS_PLAYER_ACTIVE(PLAYER_ID())
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			INT iVeh
			REPEAT NUM_PROMO_TEST_DRIVE_VEHICLES iVeh
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTestDriveVehicle[iVeh])
					IF IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.niTestDriveVehicle[iVeh]))
						
						IF (g_sTestDriveBGScript.fNearTestVehRange > 0)
							fRange = g_sTestDriveBGScript.fNearTestVehRange
						ENDIF
						
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(NET_TO_VEH(serverBD.niTestDriveVehicle[iVeh]), FALSE)) < fRange
							IF sTestDriveData.viPromoVehIAmNear != NET_TO_VEH(serverBD.niTestDriveVehicle[iVeh])
								sTestDriveData.viPromoVehIAmNear = NET_TO_VEH(serverBD.niTestDriveVehicle[iVeh])
								sTestDriveData.iPromoVehIAmNearAsInt = iVeh
							ENDIF
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	//IF sTestDriveData.viPromoVehIAmNear != NULL
	//	sTestDriveData.viPromoVehIAmNear = NULL
	//ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Used for the help text that displays when the player is near a promo test vehicle.
/// RETURNS:
///    Help text label.
FUNC STRING GET_TEST_DRIVE_SANDBOX_PROMO_VEH_START_HELP_TEXT(TEST_DRIVE_DATA &sTestDriveData)
	VEHICLE_INDEX vehIAmNear = GET_TEST_DRIVE_PROMO_VEH_I_AM_NEAR(sTestDriveData)
	BOOL bSafeToPurchase = IS_THIS_VEHICLE_SAFE_TO_PURCHASE(vehIAmNear)
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_NEAR_HAO_PREMIUM_TEST_VEHICLE(sTestDriveData)
	
		#IF IS_DEBUG_BUILD
		IF sTestDriveData.sDebugData.bBypassHSWPurchaseRequirements
			RETURN "TDR_SANDBOX_0"
		ENDIF
		#ENDIF
		
		IF NOT (bSafeToPurchase)
			RETURN "TDR_SANDBOX_0a" // ~INPUT_ENTER~ Test Drive. This vehicle will be available for purchase coming soon.
		ELIF NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HSW_RACE_DONE)
			RETURN "TDR_SANDBOX_0b" // ~INPUT_ENTER~ Test Drive. This vehicle will be available for purchase after beating the par time for Hao's race in Vinewood.
		ELSE
			RETURN "TDR_SANDBOX_0" // ~INPUT_ENTER~ Test Drive~n~ ~INPUT_SCRIPT_RLEFT~ Purchase Vehicle
		ENDIF
	ENDIF
	#ENDIF
	
	IF (bSafeToPurchase)
		RETURN "TDR_SANDBOX_0"
	ELSE
		RETURN "TDR_SANDBOX_0a"
	ENDIF
	
	RETURN ""
ENDFUNC

#IF FEATURE_GEN9_EXCLUSIVE
FUNC BOOL CAN_PLAYER_PURCHASE_TEST_DRIVE_VEHICLE(TEST_DRIVE_DATA &sTestDriveData)
	IF IS_PLAYER_NEAR_HAO_PREMIUM_TEST_VEHICLE(sTestDriveData)
		#IF IS_DEBUG_BUILD
		IF sTestDriveData.sDebugData.bBypassHSWPurchaseRequirements
			PRINTLN("CAN_PLAYER_PURCHASE_TEST_DRIVE_VEHICLE - TRUE - DEBUG - bBypassHSWPurchaseRequirements")
			RETURN TRUE
		ENDIF
		#ENDIF
		
		RETURN GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_HSW_RACE_DONE)
	ENDIF
	
	RETURN TRUE
ENDFUNC
#ENDIF

/// PURPOSE:
///    Detects when the player is entering a promo test drive vehicle. 
/// PARAMS:
///    serverBD - server struct
/// RETURNS:
///    TRUE if local player is entering promo vehicle. 
FUNC BOOL IS_PLAYER_ENTERING_A_PROMO_TEST_DRIVE_SANDBOX_VEHICLE(SERVER_TEST_DRIVE_BD_DATA &serverBD)
	IF NETWORK_IS_PLAYER_A_PARTICIPANT(PLAYER_ID())
	AND NETWORK_IS_PLAYER_ACTIVE(PLAYER_ID())
	
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		
			// Exit now to prevent going through a loop every frame. 
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
				RETURN FALSE
			ENDIF
			
			INT iVeh
			REPEAT NUM_PROMO_TEST_DRIVE_VEHICLES iVeh
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTestDriveVehicle[iVeh])
					IF IS_ENTITY_ALIVE(NET_TO_VEH(serverBD.niTestDriveVehicle[iVeh]))
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niTestDriveVehicle[iVeh]), FALSE)
							RETURN FALSE
						ELIF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), NET_TO_VEH(serverBD.niTestDriveVehicle[iVeh]), TRUE)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the driver and all passengers are ready to start test drive.
/// PARAMS:
///    theVeh - Vehicle to check
/// RETURNS:
///    TRUE if all players in vehicle are ready.
FUNC BOOL ARE_ALL_PEDS_IN_TEST_DRIVE_SANDBOX_VEHICLE_READY(TEST_DRIVE_DATA &sTestDriveData, VEHICLE_INDEX theVeh, BOOL bCheckBit = FALSE)
	IF MPGlobals.bStartedMPCutscene
		RETURN TRUE
	ENDIF
	
	PLAYER_INDEX thePlayer
	
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		thePlayer = INT_TO_PLAYERINDEX(i)
		
		IF thePlayer != PLAYER_ID()
		AND thePlayer != INVALID_PLAYER_INDEX()
		AND NETWORK_IS_PLAYER_A_PARTICIPANT(thePlayer)
			IF IS_NET_PLAYER_OK(thePlayer)
				IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), theVeh, FALSE)
					IF GET_SCRIPT_TASK_STATUS(GET_PLAYER_PED(thePlayer), SCRIPT_TASK_LEAVE_VEHICLE) = PERFORMING_TASK
					OR GET_SCRIPT_TASK_STATUS(GET_PLAYER_PED(thePlayer), SCRIPT_TASK_LEAVE_ANY_VEHICLE) = PERFORMING_TASK
					OR IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(thePlayer)
						PRINTLN("[TEST_DRIVE_SANDBOX][ARE_ALL_PEDS_IN_TEST_DRIVE_SANDBOX_VEHICLE_READY] Player in vehicle is not ready: ", i)
						RETURN FALSE
					ENDIF
					
					IF (bCheckBit)
						IF NOT IS_TEST_DRIVE_SANDBOX_PLAYER_BROADCAST_DATA_BIT_SET(sTestDriveData, thePlayer, PLAYER_TEST_DRIVE_BD_DATA_CHECKED_PASSENGER_IS_OKAY_TO_START_TEST_DRIVE)
							PRINTLN("[TEST_DRIVE_SANDBOX][ARE_ALL_PEDS_IN_TEST_DRIVE_SANDBOX_VEHICLE_READY] Player in vehicle is not okay to start: ", i)
							RETURN FALSE
						ENDIF
					ENDIF
				ELSE
					IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(thePlayer), theVeh, TRUE)
					OR IS_ENTITY_ATTACHED_TO_ENTITY(GET_PLAYER_PED(thePlayer), theVeh)
						PRINTLN("[TEST_DRIVE_SANDBOX][ARE_ALL_PEDS_IN_TEST_DRIVE_SANDBOX_VEHICLE_READY] Player is not ready: ", i)
						RETURN FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Grabs the model of the vehicle the player is currently in.
/// RETURNS:
///    Vehicle model.
FUNC MODEL_NAMES GET_MODEL_OF_CURRENT_VEHICLE()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX _currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_ENTITY_ALIVE(_currentVeh)
				RETURN GET_ENTITY_MODEL(_currentVeh)
			ENDIF
		ENDIF
	ENDIF
	
	RETURN DUMMY_MODEL_FOR_SCRIPT
ENDFUNC

/// PURPOSE:
///    Creates all the promo vehicles that appear in the interior.
/// PARAMS:
///    sTestDriveData - data struct
///    sServerBD - server struct
/// RETURNS:
///    TRUE if all vehicles are created/able to be created.
FUNC BOOL CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES(TEST_DRIVE_DATA &sTestDriveData, SERVER_TEST_DRIVE_BD_DATA &sServerBD)
	UNUSED_PARAMETER(sTestDriveData)
	
	VEHICLE_SETUP_STRUCT_MP sData
	
	INT iVeh
	REPEAT NUM_PROMO_TEST_DRIVE_VEHICLES iVeh
		IF NOT IS_NET_VEHICLE_DRIVEABLE(sServerBD.niTestDriveVehicle[iVeh])
		AND GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_MODEL(iVeh) != DUMMY_MODEL_FOR_SCRIPT
			MODEL_NAMES _eVehModel = GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_MODEL(iVeh)
			IF IS_MODEL_IN_CDIMAGE(_eVehModel)
			AND IS_MODEL_A_VEHICLE(_eVehModel)
				IF REQUEST_LOAD_MODEL(_eVehModel)
					IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_VEHICLES(FALSE, RESERVATION_LOCAL_ONLY) + 1, FALSE, TRUE)
						IF NOT IS_BIT_SET(sServerBD.iVehReservedBS, iVeh)
							RESERVE_LOCAL_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES(FALSE, RESERVATION_LOCAL_ONLY) + 1)
							
							PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Vehicle Reserved: ", iVeh)
							SET_BIT(sServerBD.iVehReservedBS, iVeh)
						ENDIF
						
						//SERVER_CHECK_IF_ANY_PLAYERS_ARE_IN_THE_WAY_OF_VEHICLES(GET_VEHICLE_POSITION(i))
					
						//IF NOT IS_IT_SAFE_TO_CREATE_A_VEHICLE()
						//	PRINTLN("[TEST_DRIVE_SANDBOX][TEST_DRIVE_GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_COORDS] Not safe to create: ", i)
							
						//	RETURN FALSE
						//ENDIF
					
						IF IS_BIT_SET(sServerBD.iVehReservedBS, iVeh)
							IF CAN_REGISTER_MISSION_VEHICLES(1)
								
								// url:bugstar:7206911
								IF NETWORK_IS_IN_MP_CUTSCENE()
									SET_NETWORK_CUTSCENE_ENTITIES(TRUE)
								ENDIF
								
								VEHICLE_INDEX _tempVehicle
								_tempVehicle = CREATE_VEHICLE(_eVehModel, GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_COORDS(iVeh), 90.0000, TRUE, TRUE)
								sServerBD.niTestDriveVehicle[iVeh] = VEH_TO_NET(_tempVehicle)
								
								SET_ENTITY_COORDS_NO_OFFSET(_tempVehicle, GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_COORDS(iVeh))
								SET_ENTITY_ROTATION(_tempVehicle, GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_ROTATION(iVeh))
								
								SET_ENTITY_INVINCIBLE(_tempVehicle, TRUE)
								NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(_tempVehicle, TRUE)
								SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(_tempVehicle, TRUE)
								SET_ENTITY_COLLISION(_tempVehicle, TRUE)
								SET_VEHICLE_FULLBEAM(_tempVehicle, FALSE)							
								TOGGLE_VEHICLE_MOD(_tempVehicle, MOD_TOGGLE_XENON_LIGHTS, TRUE)
								SET_VEHICLE_DOORS_LOCKED(_tempVehicle, VEHICLELOCK_UNLOCKED)
								SET_ENTITY_CAN_BE_DAMAGED(_tempVehicle, FALSE)
								SET_VEHICLE_ENGINE_ON(_tempVehicle, TRUE, TRUE)
								SET_VEHICLE_LIGHTS(_tempVehicle, FORCE_VEHICLE_LIGHTS_ON)
								SET_VEHICLE_FIXED(_tempVehicle)
						        SET_ENTITY_HEALTH(_tempVehicle, 1000)
						        SET_VEHICLE_ENGINE_HEALTH(_tempVehicle, 1000)
						        SET_VEHICLE_PETROL_TANK_HEALTH(_tempVehicle, 1000)
								SET_VEHICLE_DIRT_LEVEL(_tempVehicle, 0.0)
								SET_ENTITY_CAN_BE_DAMAGED(_tempVehicle, FALSE)
								SET_ENTITY_VISIBLE(_tempVehicle, TRUE)
							
						
								SET_VEHICLE_ON_GROUND_PROPERLY(_tempVehicle)
								SET_ENTITY_VISIBLE_IN_CUTSCENE(_tempVehicle, FALSE)
								FREEZE_ENTITY_POSITION(_tempVehicle, TRUE)
								
								SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(_tempVehicle, PLAYER_ID(), TRUE)
								SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(_tempVehicle, TRUE)
								SET_DONT_ALLOW_PLAYER_TO_ENTER_VEHICLE_IF_LOCKED_FOR_PLAYER(_tempVehicle, TRUE)
								SET_VEHICLE_DOORS_LOCKED(_tempVehicle, VEHICLELOCK_CANNOT_ENTER)
								
								#IF IS_DEBUG_BUILD
								INIT_TEST_DRIVE_VEHICLE_DEBUG_POSITION(sTestDriveData.sDebugData.sDebugVehData[iVeh], _tempVehicle)
								#ENDIF
								
								#IF FEATURE_GEN9_EXCLUSIVE
								IF IS_THIS_VEHICLE_INDEX_HAOS_PREMIUM_TEST_VEHICLE(iVeh)
									IF UPGRADE_VEHICLE_TO_HSW(_tempVehicle)
										PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Upgrading to HSW! iVeh: ", iVeh)
									ELSE
										PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Trying to upgrade a non-HSW vehicle to HSW. iVeh: ", iVeh)
									ENDIF
								ENDIF
								#ENDIF
								
								// Colours, Livery and lights.
								GET_PROMO_TEST_DRIVE_SANDBOX_VEHICLE_SETUP(sData, iVeh, _eVehModel)
								SET_VEHICLE_SETUP_MP(_tempVehicle, sData, FALSE, TRUE, TRUE)
								
								#IF IS_DEBUG_BUILD
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] ********************")
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Creating Vehicle: ", iVeh)
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Model: ", 			GET_MODEL_NAME_FOR_DEBUG(_eVehModel))
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Colour 1: ", 		sData.VehicleSetup.iColour1)
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Colour 2: ", 		sData.VehicleSetup.iColour2)
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Colour Extra 1: ", 	sData.VehicleSetup.iColourExtra1)
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Colour Extra 2: ", 	sData.VehicleSetup.iColourExtra2)
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Colour 5: ", 		sData.iColour5)
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Colour 6: ", 		sData.iColour6)
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Livery: ", 			sData.VehicleSetup.iModIndex[MOD_LIVERY])
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Wheel Type: ", 		sData.VehicleSetup.iWheelType)
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Wheel ID: ", 		sData.VehicleSetup.iModIndex[MOD_WHEELS])
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Window Tint: ", 	sData.VehicleSetup.iWindowTintColour)
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Lights: ", 			sData.VehicleSetup.iModIndex[MOD_TOGGLE_XENON_LIGHTS])
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] ********************")
								#ENDIF
								
								SET_VEHICLE_RADIO_ENABLED(_tempVehicle, TRUE)
								LOCK_RADIO_STATION("RADIO_36_AUDIOPLAYER", FALSE)
								SET_VEH_RADIO_STATION(_tempVehicle, "RADIO_36_AUDIOPLAYER")
								UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_MIX3_PARTD")
								FORCE_MUSIC_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_MIX3_PARTD" , GET_TUNER_SANDBOX_RADIO_MIX_START_MS())
								
								SET_MODEL_AS_NO_LONGER_NEEDED(_eVehModel)
								
								// url:bugstar:7206911
								IF NETWORK_IS_IN_MP_CUTSCENE()
									SET_NETWORK_CUTSCENE_ENTITIES(FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			RETURN FALSE
		#IF FEATURE_GEN9_EXCLUSIVE
		ELIF NOT IS_BIT_SET(sServerBD.iVehGroundedBS, iVeh)
		AND IS_THIS_VEHICLE_INDEX_HAOS_PREMIUM_TEST_VEHICLE(iVeh)
		AND IS_NET_VEHICLE_DRIVEABLE(sServerBD.niTestDriveVehicle[iVeh])
		AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(sServerBD.niTestDriveVehicle[iVeh])
		    IF NOT SET_VEHICLE_ON_GROUND_PROPERLY(NET_TO_VEH(sServerBD.niTestDriveVehicle[iVeh]))
				PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] Setting HSW Test Ride vehicle on ground properly")
				RETURN FALSE
		    ENDIF
			PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES] HSW Test Ride vehicle ",iVeh," is on ground properly. Setting sServerBD.iVehGroundedBS")
			SET_BIT(sServerBD.iVehGroundedBS, iVeh)		
		#ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Forces a single vehicle into the given room.
/// PARAMS:
///    vehicleToForce - Vehicle
///    InteriorInstanceIndex - Interior INstance INdex
///    iRoomKey - Room Key
/// RETURNS:
///    TRUE - Forced vehicle into room.
FUNC BOOL FORCE_TEST_DRIVE_SANDBOX_VEHICLE_INTO_ROOM(VEHICLE_INDEX vehicleToForce, INTERIOR_INSTANCE_INDEX InteriorInstanceIndex, INT iRoomKey)
	IF IS_ENTITY_ALIVE(vehicleToForce)
		INT iCurrentRoom = GET_ROOM_KEY_FROM_ENTITY(vehicleToForce)
		
		PRINTLN("[TEST_DRIVE_SANDBOX][FORCE_VEHICLE_INTO_ROOM] iCurrentRoom:", iCurrentRoom, " iRoomKey: ", iRoomKey)
		
		IF (iCurrentRoom = iRoomKey)
			IF IS_ENTITY_ALIVE(vehicleToForce)
				IF IS_VALID_INTERIOR(InteriorInstanceIndex)
					IF IS_INTERIOR_READY(InteriorInstanceIndex)
						FORCE_ROOM_FOR_ENTITY(vehicleToForce, InteriorInstanceIndex, iRoomKey)
						PRINTLN("[TEST_DRIVE_SANDBOX][FORCE_VEHICLE_INTO_ROOM] Forcing vehicle into room. iRoomKey: ", iRoomKey)
					ELSE
						RETURN FALSE
					ENDIF
				ELSE
					PRINTLN("[TEST_DRIVE_SANDBOX][FORCE_VEHICLE_INTO_ROOM] Interior isn't valid. returning true.")
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Forces all the promo vehicles into the given room.
/// PARAMS:
///    sServerBD - Vehicle
///    InteriorInstanceIndex - Insterior Instance Index
///    iRoomKey - Room key
/// RETURNS:
///    TRUE - Forced all the vehicles into the room.
FUNC BOOL FORCE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES_INTO_ROOM(SERVER_TEST_DRIVE_BD_DATA &sServerBD, INTERIOR_INSTANCE_INDEX InteriorInstanceIndex, INT iRoomKey)
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		INT iVeh
		REPEAT NUM_PROMO_TEST_DRIVE_VEHICLES iVeh
			IF IS_NET_VEHICLE_DRIVEABLE(sServerBD.niTestDriveVehicle[iVeh])
				INT iCurrentRoom = GET_ROOM_KEY_FROM_ENTITY(NET_TO_VEH(sServerBD.niTestDriveVehicle[iVeh]))
				PRINTLN("[TEST_DRIVE_SANDBOX][FORCE_PROMO_TEST_DRIVE_SANDBOX_VEHICLES_INTO_ROOM] iVeh: ", iVeh, " iCurrentRoom: ", iCurrentRoom, " iRoomKey: ", iRoomKey)
				
				IF (iCurrentRoom = iRoomKey)
					IF NOT FORCE_TEST_DRIVE_SANDBOX_VEHICLE_INTO_ROOM(NET_TO_VEH(sServerBD.niTestDriveVehicle[iVeh]), InteriorInstanceIndex, iRoomKey)
						RETURN FALSE
					ENDIF
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Creates the vehicle the player will drive around the sandbox area.
/// PARAMS:
///    sTestDriveData - server struct
/// RETURNS:
///    TRUE if vehicle is created
FUNC BOOL CREATE_TEST_DRIVE_SANDBOX_VEHICLE(TEST_DRIVE_DATA &sTestDriveData)
	IF NOT IS_NET_VEHICLE_DRIVEABLE(sTestDriveData.niSandboxVeh)
	AND GET_TEST_DRIVE_SANDBOX_VEHICLE_MODEL() != DUMMY_MODEL_FOR_SCRIPT
		MODEL_NAMES _eVehModel = GET_TEST_DRIVE_SANDBOX_VEHICLE_MODEL()
		IF IS_MODEL_IN_CDIMAGE(_eVehModel)
		AND IS_MODEL_A_VEHICLE(_eVehModel)
			IF REQUEST_LOAD_MODEL(_eVehModel)
				IF CAN_RESERVE_NETWORK_VEHICLES_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_VEHICLES(FALSE, RESERVATION_LOCAL_ONLY) + 1, FALSE, TRUE)
				AND NOT IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(sTestDriveData, TEST_DRIVE_DATA_BS_SANDBOX_VEH_RESERVED)
					RESERVE_LOCAL_NETWORK_MISSION_VEHICLES(GET_NUM_RESERVED_MISSION_VEHICLES(FALSE, RESERVATION_LOCAL_ONLY) + 1)
					
					PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_TEST_DRIVE_SANDBOX_VEHICLE] Vehicle Reserved")
					SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_SANDBOX_VEH_RESERVED, TRUE)
				ELIF IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(sTestDriveData, TEST_DRIVE_DATA_BS_SANDBOX_VEH_RESERVED)
					
					VECTOR _vVehCoords
					FLOAT _fVehHeading
					
					IF NOT GET_TEST_DRIVE_SANDBOX_VEHICLE_SPAWN_COORDS(_vVehCoords, _fVehHeading)
						PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_TEST_DRIVE_SANDBOX_VEHICLE] Waiting to find safe creation point...")
						RETURN FALSE
					ENDIF
					
					sTestDriveData.vNewTestVehCoords = _vVehCoords
					
					IF CAN_REGISTER_MISSION_VEHICLES(1)
						VEHICLE_INDEX _tempVehicle
						_tempVehicle = CREATE_VEHICLE(_eVehModel, _vVehCoords, _fVehHeading, TRUE, FALSE)
						sTestDriveData.niSandboxVeh = VEH_TO_NET(_tempVehicle)
						
						SET_ENTITY_COORDS_NO_OFFSET(_tempVehicle, _vVehCoords)
						SET_ENTITY_ROTATION(_tempVehicle, <<0.0, 0.0, _fVehHeading>>)
						
						SET_ENTITY_INVINCIBLE(_tempVehicle, TRUE)
						NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(_tempVehicle, TRUE)
						SET_VEHICLE_NOT_STEALABLE_AMBIENTLY(_tempVehicle, TRUE)
						SET_ENTITY_COLLISION(_tempVehicle, TRUE)
						SET_VEHICLE_FULLBEAM(_tempVehicle, FALSE)
						SET_VEHICLE_LIGHTS(_tempVehicle, SET_VEHICLE_LIGHTS_OFF)
						SET_ENTITY_CAN_BE_DAMAGED(_tempVehicle, FALSE)
						SET_VEHICLE_ENGINE_ON(_tempVehicle, TRUE, TRUE)
						SET_VEHICLE_FIXED(_tempVehicle)
				        SET_ENTITY_HEALTH(_tempVehicle, 1000)
				        SET_VEHICLE_ENGINE_HEALTH(_tempVehicle, 1000)
				        SET_VEHICLE_PETROL_TANK_HEALTH(_tempVehicle, 1000)
						SET_VEHICLE_DIRT_LEVEL(_tempVehicle, 0.0)
						SET_ENTITY_CAN_BE_DAMAGED(_tempVehicle, FALSE)
						SET_ENTITY_VISIBLE(_tempVehicle, TRUE)
						
						DECOR_SET_BOOL(_tempVehicle, "TestDrive", TRUE)	
						SET_VEHICLE_ON_GROUND_PROPERLY(_tempVehicle)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(_tempVehicle, TRUE)
						//SET_RADIO_TO_MY_CHOSEN()
						
						#IF FEATURE_GEN9_EXCLUSIVE
						IF IS_PLAYER_TEST_DRIVING_HAOS_PREMIUM_VEHICLE(PLAYER_ID())
							IF UPGRADE_VEHICLE_TO_HSW(_tempVehicle)
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_TEST_DRIVE_SANDBOX_VEHICLE] Upgrading to HSW!")
							ELSE
								PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_TEST_DRIVE_SANDBOX_VEHICLE] Trying to upgrade a non-HSW vehicle to HSW.")
							ENDIF
						ENDIF
						#ENDIF
						
						// Colours and Livery
						SET_TEST_DRIVE_SANDBOX_VEHICLE_SETUP_MP(_tempVehicle)
						
						#IF FEATURE_TUNER
						IF CAN_VEHICLE_USE_PV_STANCE(GET_ENTITY_MODEL(_tempVehicle))
						AND GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_PERSONAL_VEHICLE_STANCE) = TRUE
							SET_REDUCED_SUSPENSION_FORCE(_tempVehicle, TRUE)
						ENDIF
						#ENDIF
						
						SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(_tempVehicle, PLAYER_ID(), FALSE)
						SET_VEHICLE_DOORS_LOCKED_FOR_ALL_PLAYERS(_tempVehicle, FALSE)
						SET_VEHICLE_DOORS_LOCKED(_tempVehicle, VEHICLELOCK_UNLOCKED)
						
						SET_VEHICLE_CANNOT_BE_STORED_IN_PROPERTIES(_tempVehicle)
						
						#IF IS_DEBUG_BUILD
						PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_TEST_DRIVE_SANDBOX_VEHICLE] ********************")
						PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_TEST_DRIVE_SANDBOX_VEHICLE] Creating Vehicle")
						PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_TEST_DRIVE_SANDBOX_VEHICLE] Model: ", 			GET_MODEL_NAME_FOR_DEBUG(_eVehModel))
						PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_TEST_DRIVE_SANDBOX_VEHICLE] ********************")
						#ENDIF
						
						SET_VEHICLE_RADIO_ENABLED(_tempVehicle, TRUE)
						LOCK_RADIO_STATION("RADIO_36_AUDIOPLAYER", FALSE)
						//SET_VEH_RADIO_STATION(_tempVehicle, "RADIO_36_AUDIOPLAYER")
						UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_MIX3_PARTD")
						FORCE_MUSIC_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_MIX3_PARTD" , GET_TUNER_SANDBOX_RADIO_MIX_START_MS())
						SET_VEH_RADIO_STATION(_tempVehicle, "HIDDEN_RADIO_09_HIPHOP_OLD")
						SET_MODEL_AS_NO_LONGER_NEEDED(_eVehModel)	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		RETURN FALSE
	ENDIF
	
	PRINTLN("[TEST_DRIVE_SANDBOX][CREATE_TEST_DRIVE_SANDBOX_VEHICLE] Sandbox vehicle created!")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks to see if the driver has set a bit in IS_TEST_DRIVE_SANDBOX_PLAYER_BROADCAST_DATA_BIT_SET
/// PARAMS:
///    sTestDriveData - data struct
///    iBit - bit
/// RETURNS:
///    TRUE if driver has set bit.
FUNC BOOL HAS_DRIVER_SET_PLAYER_BD_BS_IN_VEHICLE_I_AM_IN(TEST_DRIVE_DATA &sTestDriveData, INT iBit)
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX _currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) != VS_DRIVER
			AND NOT IS_VEHICLE_SEAT_FREE(_currentVeh, VS_DRIVER)
				PED_INDEX pedDriver = GET_PED_IN_VEHICLE_SEAT(_currentVeh, VS_DRIVER)
				PLAYER_INDEX pDriver = NETWORK_GET_PLAYER_INDEX_FROM_PED(pedDriver)
				
				IF IS_TEST_DRIVE_SANDBOX_PLAYER_BROADCAST_DATA_BIT_SET(sTestDriveData, pDriver, iBit)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Kicks all of the players out of the current vehicle.
/// PARAMS:
///    Veh - vehicle
/// RETURNS:
///    TRUE when vehicle is empty.
FUNC BOOL KICK_ALL_PLAYERS_FROM_TEST_DRIVE_VEHICLE(VEHICLE_INDEX Veh)
	IF IS_ENTITY_ALIVE(Veh)
		INT i
		PLAYER_INDEX tempPlayer
		
		REPEAT NUM_NETWORK_PLAYERS i
			tempPlayer = INT_TO_PLAYERINDEX(i)
			
			IF IS_NET_PLAYER_OK(tempPlayer, FALSE)
				IF IS_PED_IN_VEHICLE(GET_PLAYER_PED(tempPlayer), Veh)
					BROADCAST_LEAVE_VEHICLE(SPECIFIC_PLAYER(tempPlayer), FALSE, 0, 0, FALSE, FALSE)
					SET_VEHICLE_DOORS_LOCKED_FOR_PLAYER(Veh, tempPlayer, TRUE)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL CAN_TEST_DRIVE_PROMO_VEHICLE(TEST_DRIVE_DATA &sTestDriveData)
	
	#IF NOT FEATURE_GEN9_EXCLUSIVE
	UNUSED_PARAMETER(sTestDriveData)
	#ENDIF
	
	IF IS_CUSTOM_MENU_ON_SCREEN()
		RETURN FALSE
	ENDIF
		
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_NEAR_HAO_PREMIUM_TEST_VEHICLE(sTestDriveData)
	AND g_sMPTunables.bHSW_TEST_RIDE_DISABLE
		RETURN FALSE
	ENDIF
	#ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks to see if the local player is safe to start a test drive
/// PARAMS:
///    sTestDriveData - data struct
///    serverBD - server struct
/// RETURNS:
///    TRUE if player is safe to start a test drive.
FUNC BOOL CAN_PLAYER_START_TEST_DRIVE_SANDBOX(TEST_DRIVE_DATA &sTestDriveData)
	UNUSED_PARAMETER(sTestDriveData)
	
	IF SHOULD_LOCAL_PLAYER_WARP_DIRECTLY_INTO_TEST_DRIVE_AREA()
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] TRUE - SHOULD_LOCAL_PLAYER_WARP_DIRECTLY_INTO_TEST_DRIVE_AREA")
		RETURN TRUE
	ENDIF
	
	IF (g_sTestDriveBGScript.bDisableStartTestDrive)
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] g_sTestDriveBGScript.bDisableStartTestDrive")
		RETURN FALSE
	ENDIF
	
	CONTROL_ACTION eModShopControlAction = INPUT_SCRIPT_PAD_RIGHT
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		eModShopControlAction = INPUT_THROW_GRENADE
	ENDIF
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, eModShopControlAction)
		REINIT_NET_TIMER(sTestDriveData.blockTestDriveMenu)
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] we are in main help and mod shop control pressed. START_NET_TIMER(carMeetTestDriveMenu)")
		RETURN FALSE
	ENDIF
	
	IF HAS_NET_TIMER_STARTED(sTestDriveData.blockTestDriveMenu)
	AND NOT HAS_NET_TIMER_EXPIRED(sTestDriveData.blockTestDriveMenu, 3000)
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] waiting for 3 sec before letting player to use test drive")
		RETURN FALSE
	ENDIF
	
	IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX _currentVeh = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
		IF GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IsSeatShuffling)
			PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] Player is shuffling seats.")
			RETURN FALSE
		ENDIF
		
		// Only car club members can start the test drive. Passengers can still be dragged in regardless.
		IF GET_SEAT_PED_IS_IN(PLAYER_PED_ID()) = VS_DRIVER
			
			IF NOT ARE_ALL_PEDS_IN_TEST_DRIVE_SANDBOX_VEHICLE_READY(sTestDriveData, _currentVeh)
				PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] ARE_ALL_PEDS_IN_TEST_DRIVE_SANDBOX_VEHICLE_READY is FALSE.")
				RETURN FALSE
			ENDIF
			
			IF NOT HAS_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(PLAYER_ID())
			#IF IS_DEBUG_BUILD
			AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_TestDriveSkipMembershipRequirement")
			#ENDIF
				PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] Driver has not purchased car club membership.")
				RETURN FALSE
			ENDIF
			
			IF DOES_ENTITY_EXIST(_currentVeh)
			AND IS_VEHICLE_DRIVEABLE(_currentVeh)
			AND IS_VEHICLE_A_PERSONAL_VEHICLE(_currentVeh)
				PLAYER_INDEX pOwner = GET_OWNER_OF_PERSONAL_VEHICLE(_currentVeh)
				
				IF pOwner != INVALID_PLAYER_INDEX()
				AND pOwner != PLAYER_ID()
				AND IS_BIT_SET(GlobalplayerBD[NATIVE_TO_INT(pOwner)].SimpleInteriorBD.iBSFive, BS_SIMPLE_INTERIOR_GLOBAL_BS_FIVE_USING_VEHICLE_MANAGEMENT)
					PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] Vehicle owner is using Vehicle Management.")
					
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT HAS_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(PLAYER_ID()) // url:bugstar:7023205
		#IF IS_DEBUG_BUILD
		AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_TestDriveSkipMembershipRequirement")
		#ENDIF
			PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] Player has not purchased car club membership.")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD(PLAYER_ID())
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] IS_PLAYER_STARTED_TO_USE_PERSONAL_CAR_MOD is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF PLAYER_ID() != INVALID_PLAYER_INDEX()
		IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCarMeetPropertyData.iBS, PROPERTY_BROADCAST_BS_I_AM_REQUESTING_TO_USE_PERSONAL_CAR_MOD)
			PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] PROPERTY_BROADCAST_BS_I_AM_REQUESTING_TO_USE_PERSONAL_CAR_MOD is TRUE.")
			RETURN FALSE
		ENDIF
	ENDIF	
		
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] IS_PLAYER_SWITCH_IN_PROGRESS is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_SELECTOR_ONSCREEN()
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] IS_SELECTOR_ONSCREEN is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF PROPERTY_HAS_JUST_ACCEPTED_A_MISSION()
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] PROPERTY_HAS_JUST_ACCEPTED_A_MISSION is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF DOES_SCRIPT_EXIST("appMPJobListNEW")
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appMPJobListNEW")) > 0
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] appMPJobListNEW is running.")
		RETURN FALSE
	ENDIF
	
	IF DOES_SCRIPT_EXIST("appJIPMP")
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appJIPMP")) > 0
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] appJIPMP is running.")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SCTV(PLAYER_ID())
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] IS_PLAYER_SCTV is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_PAUSE_MENU_ACTIVE_EX()
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] IS_PAUSE_MENU_ACTIVE_EX is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_COMMERCE_STORE_OPEN()
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] IS_COMMERCE_STORE_OPEN is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN()
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] IS_STORE_PENDING_NETWORK_SHUTDOWN_TO_OPEN is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_CELLPHONE_CAMERA_IN_USE()
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] IS_CELLPHONE_CAMERA_IN_USE is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] FALSE - Browser is open.")
		RETURN FALSE
	ENDIF
	
	IF IS_PHONE_ONSCREEN()
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] FALSE - Browser is open.")
		RETURN FALSE
	ENDIF
	
	IF SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER()
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADING_IN()
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] IS_SCREEN_FADING_IN is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_INTERACTION_MENU_OPEN()
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] IS_INTERACTION_MENU_OPEN is TRUE.")
		RETURN FALSE
	ENDIF
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_THROW_GRENADE)
			RETURN FALSE
		ENDIF
	ELSE				
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF GB_IS_LOCAL_PLAYER_JOINING_THIS_GANG_BOSS_MISSION(FMMC_TYPE_SANDBOX_ACTIVITY, ENUM_TO_INT(SAV_SPRINT)) // Don't let player enter test area if they are joining a Sprint Race 
	OR GB_GET_CONTRABAND_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = ENUM_TO_INT(SAV_SPRINT)
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] SAV_SPRINT")
		RETURN FALSE
	ENDIF
	
	IF IS_PI_MENU_OPEN() // If player selects LS Car Meet in PIM, it would also open the Test Drive menu when in their PV, causing two overlapping menus
		PRINTLN("[CAN_PLAYER_START_TEST_DRIVE_SANDBOX] IS_PI_MENU_OPEN is TRUE.")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

// ***************************************************************************************
// 					CUTSCENES
// ***************************************************************************************

FUNC INT GET_SANDBOX_MENU_CAMERA_VARIANT()
	RETURN GET_RANDOM_INT_IN_RANGE(0, NUM_SANDBOX_EXIT_MENU_VARIANTS)
ENDFUNC

FUNC VECTOR GET_EXIT_SANDBOX_MENU_CAMERA_COORDS(INT iShotVariant)
	SWITCH iShotVariant
		CASE 0		RETURN <<-2139.5696, 1117.0297, 28.7612>>
		CASE 1		RETURN <<-2139.7341, 1114.7706, 28.8857>>
		CASE 2		RETURN <<-2139.8022, 1086.6510, 28.8391>>
		CASE 3		RETURN <<-2138.5171, 1124.1162, 28.8781>>
		CASE 4		RETURN <<-2139.5569, 1114.2887, 28.8886>>
		CASE 5		RETURN <<-2139.3337, 1150.7338, 28.9131>>
		CASE 6		RETURN <<-2138.9824, 1088.6503, 28.9118>>
	ENDSWITCH
	
	RETURN <<-2139.5696, 1117.0297, 28.7612>>
ENDFUNC

FUNC VECTOR GET_EXIT_SANDBOX_MENU_CAMERA_ROT(INT iShotVariant)
	SWITCH iShotVariant
		CASE 0		RETURN <<-1.5965, 0.1167, 104.5397>>
		CASE 1		RETURN <<-1.6269, 0.1167, 71.8699>>
		CASE 2		RETURN <<-2.7073, -0.0201, 54.9734>>
		CASE 3		RETURN <<-1.0543, 0.1166, 104.4969>>
		CASE 4		RETURN <<-2.6093, 0.1107, 27.5694>>
		CASE 5		RETURN <<-1.2991, 0.1006, 120.5073>>
		CASE 6		RETURN <<-1.2476, 0.1006, 80.9185>>
	ENDSWITCH
	
	RETURN <<-1.5965, 0.1167, 104.5397>>
ENDFUNC

FUNC FLOAT GET_EXIT_SANDBOX_MENU_CAMERA_FOV(INT iShotVariant)
	SWITCH iShotVariant
		CASE 0		RETURN 7.7533
		CASE 1		RETURN 20.2132
		CASE 2		RETURN 21.4896
		CASE 3		RETURN 10.6434
		CASE 4		RETURN 8.5651
		CASE 5		RETURN 11.6216
		CASE 6		RETURN 11.6216
	ENDSWITCH
	
	RETURN 7.7533
ENDFUNC

FUNC FLOAT GET_EXIT_SANDBOX_MENU_CAMERA_SHAKE(INT iShotVariant)
	SWITCH iShotVariant
		CASE 0		RETURN 0.1
		CASE 1		RETURN 0.1
		CASE 2		RETURN 0.1
		CASE 3		RETURN 0.1
		CASE 4		RETURN 0.1
		CASE 5		RETURN 0.1
		CASE 6		RETURN 0.1
	ENDSWITCH
	
	RETURN 0.1
ENDFUNC

#IF IS_DEBUG_BUILD
PROC DEBUG_UPDATE_EXIT_SANDBOX_MENU_CAMERA_VALUES(TEST_DRIVE_MENU_DATA &sMenuData, TEST_DRIVE_DEBUG_DATA &sDebugData)
	IF sDebugData.bManuallySetExitMenuShot
		
		IF DOES_CAM_EXIST(sMenuData.camera)
			IF IS_CAM_ACTIVE(sMenuData.camera)
				SET_CAM_ACTIVE(sMenuData.camera, FALSE)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
			ENDIF
		ENDIF
		
		sMenuData.iShotVariant = sDebugData.iShotVariant
		sMenuData.fShotCamShake = sDebugData.fShotCamShake
		sDebugData.bManuallySetExitMenuShot = FALSE
	ENDIF
ENDPROC
#ENDIF

PROC RENDER_EXIT_SANDBOX_MENU_CAMERA(TEST_DRIVE_MENU_DATA &sMenuData)
	IF NOT DOES_CAM_EXIST(sMenuData.camera)
		sMenuData.camera = CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)
	ELIF NOT IS_CAM_ACTIVE(sMenuData.camera)
		SET_CAM_ACTIVE(sMenuData.camera, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		
		IF sMenuData.iShotVariant = -1
			sMenuData.iShotVariant = GET_SANDBOX_MENU_CAMERA_VARIANT()
		ENDIF
		
		IF sMenuData.fShotCamShake = 0
			sMenuData.fShotCamShake = GET_EXIT_SANDBOX_MENU_CAMERA_SHAKE(sMenuData.iShotVariant)
		ENDIF
		
		PRINTLN("[RENDER_EXIT_SANDBOX_MENU_CAMERA] sMenuData.iShotVariant = ", sMenuData.iShotVariant)
		
		SET_CAM_PARAMS(sMenuData.camera, GET_EXIT_SANDBOX_MENU_CAMERA_COORDS(sMenuData.iShotVariant), GET_EXIT_SANDBOX_MENU_CAMERA_ROT(sMenuData.iShotVariant), GET_EXIT_SANDBOX_MENU_CAMERA_FOV(sMenuData.iShotVariant))
		STOP_CAM_SHAKING(sMenuData.camera, TRUE)
		SHAKE_CAM(sMenuData.camera, "Hand_shake", sMenuData.fShotCamShake)
		START_MP_CUTSCENE()
		
		sMenuData.iShotVariant = -1
		sMenuData.fShotCamShake = 0
	ENDIF
ENDPROC

PROC CLEANUP_EXIT_SANDBOX_MENU_CAMERA(TEST_DRIVE_MENU_DATA &sMenuData)
	IF DOES_CAM_EXIST(sMenuData.camera)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		IF IS_CAM_ACTIVE(sMenuData.camera)
			SET_CAM_ACTIVE(sMenuData.camera, FALSE)
		ENDIF
		DESTROY_CAM(sMenuData.camera)
		CLEANUP_MP_CUTSCENE(FALSE, FALSE, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Grabs the camera shot for both the start and end of the cutscene (interps).
/// PARAMS:
///    vEstablishingPos - pos
///    vEstablishingRot - rot
///    fEstablishingFOV - fov
///    bStart - Start of cutscene.
PROC GET_TD_SANDBOX_VEH_SCENE_ESTABLISHING_SHOT(VECTOR &vEstablishingPos, VECTOR &vEstablishingRot, FLOAT &fEstablishingFOV, BOOL bStart, BOOL bExiting)
	IF (bExiting)
		IF bStart
			vEstablishingPos 	= <<-2127.6323, 1112.7855, 25.6922>>
			vEstablishingRot	= <<0.8826, -0.0047, 131.1553>>
			fEstablishingFOV	= 45.0000
		ELSE
			vEstablishingPos 	= <<-2128.5227, 1112.0637, 25.7099>>
			vEstablishingRot	= <<4.6338, -0.0047, 126.0645>>
			fEstablishingFOV	= 45.0000
		ENDIF
	ELSE
		IF bStart
			vEstablishingPos 	= <<-2149.4707, 1114.5287, -23.0937>>
			vEstablishingRot	= <<-9.5889, 0.0001, 152.6064>>
			fEstablishingFOV	= 45.0000
		ELSE
			vEstablishingPos 	= <<-2149.4143, 1113.6300, -23.2240>>
			vEstablishingRot	= <<-10.7635, 0.0001, 160.1306>>
			fEstablishingFOV	= 45.0000
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the top-down shot of the cutscene (cam interps from start to end)
/// PARAMS:
///    vTDShot - pos
///    vTDRot - rot
///    fTDFOV - fov
///    bStart - Start or end of cutscene.
PROC GET_TD_SANDBOX_VEH_SCENE_TOP_DOWN_SHOT(VECTOR &vTDShot, VECTOR &vTDRot, FLOAT &fTDFOV, BOOL bStart, BOOL bExiting)
	IF (bExiting)
		IF bStart
			vTDShot = <<-2137.1475, 1105.9199, 31.4112>>
			vTDRot	= <<-84.4625, -0.0000, -89.4472>>
			fTDFOV	= 49.1648
		ELSE
			vTDShot = <<-2137.1475, 1105.9199, 31.4112>>
			vTDRot	= <<-84.4625, -0.0000, -89.4472>>
			fTDFOV	= 47.3068
		ENDIF
	ELSE
		IF bStart
			vTDShot = <<-2150.5542, 1106.0448, -22.2766>>
			vTDRot	= <<-77.5101, -0.0159, 90.5013>>
			fTDFOV	= 61.8783
		ELSE
			vTDShot = <<-2150.5542, 1106.0448, -22.2766>>
			vTDRot	= <<-89.5003, -0.0159, 90.5013>>
			fTDFOV	= 61.8783
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Grab the vehicle start position in the cutscene.
/// RETURNS:
///    Coords of start position.
FUNC VECTOR TD_SANDBOX_VEH_ENTRY_VEHICLE_START_POSITION(BOOL bExiting, MODEL_NAMES eVehModel)
	UNUSED_PARAMETER(eVehModel)
	
	IF (bExiting)
		RETURN <<-2135.4390, 1106.1417, 25.2541>>
	ENDIF
	
	RETURN <<-2155.6726, 1106.1189, -24.7763>>
ENDFUNC

/// PURPOSE:
///    Grab the vehicle start heading in the cutscene.
/// RETURNS:
///    heading.
FUNC FLOAT TD_SANDBOX_VEH_ENTRY_VEHICLE_START_HEADING(BOOL bExiting, MODEL_NAMES eVehModel)
	UNUSED_PARAMETER(eVehModel)
	
	IF (bExiting)
		RETURN 90.000
	ENDIF
	
	RETURN 270.0000
ENDFUNC

/// PURPOSE:
///    Grab the vehicle end position in the cutscene.
/// RETURNS:
///    Coords of end position.
FUNC VECTOR TD_SANDBOX_VEH_ENTRY_VEHICLE_END_POSITION(BOOL bExiting, MODEL_NAMES eVehModel)
	UNUSED_PARAMETER(eVehModel)
	
	IF (bExiting)
		RETURN <<-2154.9294, 1106.1417, 27.6627>>
	ENDIF
	
	RETURN <<-2110.7891, 1106.2903, -28.3645>>
ENDFUNC

/// PURPOSE:
///    Gets the area limit of which the cutscene will end if the vehicle reaches. 
/// PARAMS:
///    vecA - angled area a
///    vecB - angles area b
///    fWidth - width.
PROC TD_SANDBOX_VEH_ENTRY_GET_VEHICLE_DRIVE_FORWARD_LIMIT_AREA(VECTOR &vecA, VECTOR &vecB, FLOAT &fWidth, BOOL bExiting)
	IF (bExiting)
		vecA = <<-2144.500, 1105.9036, 25.3400>>
		vecB = <<-2150.2661, 1105.9036, 30.6690>>
		fWidth = 10.0000
	ELSE
		vecA = <<-2136.4829, 1106.5000, -28.3829>>
		vecB = <<-2149.0000, 1106.5000, -21.7090>>
		fWidth = 3.5000
	ENDIF
ENDPROC

FUNC INT GET_TEST_DRIVE_CUTSCENE_FADE_OUT_TIME(BOOL bExiting)

	IF bExiting
		RETURN 1300
	ENDIF

	RETURN DEFAULT_FADE_TIME
ENDFUNC

/// PURPOSE:
///    Controls the vehicle in the cutscene, tasking it to drive forward.
/// PARAMS:
///    entryAnim - entryanim struct
PROC TD_SANDBOX_VEH_ENTRY_CONTROL_VEH_CLONE(SIMPLE_INTERIOR_ENTRY_ANIM_DATA &entryAnim, BOOL bExiting, MODEL_NAMES eVehModel)
	SIMPLE_INTERIOR_VEH_ENTRY_SCENE_DATA sEntrySceneData
	
	sEntrySceneData.bVehAboveLengthLimit 	= FALSE
	sEntrySceneData.fStartHeading 			= TD_SANDBOX_VEH_ENTRY_VEHICLE_START_HEADING(bExiting, eVehModel)
	sEntrySceneData.vVehStartPosition		= TD_SANDBOX_VEH_ENTRY_VEHICLE_START_POSITION(bExiting, eVehModel)
	sEntrySceneData.vVehEndPosition			= TD_SANDBOX_VEH_ENTRY_VEHICLE_END_POSITION(bExiting, eVehModel)
	
	//Indexes
	sEntrySceneData.iVecArrayIndexPrimaryVehModelMax 	= CUTSCENE_VEC_ARRAY_TD_PRIMARY_VEH_MODEL_MAX
	sEntrySceneData.iVecArrayIndexSavedVehCoords		= CUTSCENE_VEC_ARRAY_TD_SAVED_VEH_COORD
	
	sEntrySceneData.iFloatArrayIndexSavedVehSpeed		= CUTSCENE_FLOAT_ARRAY_TD_VEH_SPEED
	sEntrySceneData.iFloatArrayIndexVehMoveDistance		= CUTSCENE_FLOAT_TD_MOVE_VEH_DISTANCE
	sEntrySceneData.iFloatArrayIndexVehZCoord			= CUTSCENE_FLOAT_ARRAY_TD_VEH_Z_COORD
	
	//BS Ints
	sEntrySceneData.iTaskedVeh							= CUTSCENE_BS_TD_SANDBOX_TASKED_MOTORBIKE
	sEntrySceneData.iFrozenVehClone						= CUTSCENE_BS_TD_SANDBOX_FROZEN_CLONE
	sEntrySceneData.bSingleShotBikeScene 				= IS_BIT_SET(entryAnim.iSceneBS, CUTSCENE_BS_TD_SANDBOX_ONE_SHOT_BIKE_SCENE)
	
	TD_SANDBOX_VEH_ENTRY_GET_VEHICLE_DRIVE_FORWARD_LIMIT_AREA(sEntrySceneData.vLimitAreaA, sEntrySceneData.vLimitAreaB, sEntrySceneData.fLimitAreaWidth, bExiting)
	
	SIMPLE_INTERIOR_VEH_ENTRY_CONTROL_VEH_CLONE(entryAnim, sEntrySceneData, GET_TEST_DRIVE_CUTSCENE_FADE_OUT_TIME(bExiting))
ENDPROC

/// PURPOSE:
///    Cleans up the promo veh clone.
/// PARAMS:
///    sTestDriveData - data struct
/// RETURNS:
///    TRUE if vehicle is cleaned up.
PROC CLEANUP_PROMO_VEH_CUTSCENE_CLONES(TEST_DRIVE_DATA &sTestDriveData)
	INT i
	REPEAT NUM_PROMO_TEST_DRIVE_VEHICLES i
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		AND IS_ENTITY_ALIVE(sTestDriveData.promoVehAnim.vehicleArray[i])
			IF IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), sTestDriveData.promoVehAnim.vehicleArray[i])
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != WAITING_TO_START_TASK
					PRINTLN("[CLEANUP_PROMO_VEH_CUTSCENE_CLONES] - Tasking player ped to leave vehicle.")
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP | ECF_WARP_IF_DOOR_IS_BLOCKED | ECF_WARP_IF_SHUFFLE_LINK_IS_BLOCKED)
				ENDIF
			ELSE
				DELETE_VEHICLE(sTestDriveData.promoVehAnim.vehicleArray[i])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

FUNC STRING GET_TEST_DRIVE_CUTSCENE_AUDIO_SCENE()
	RETURN "DLC_Tuner_Car_Test_Area_Transition_Scene"
ENDFUNC

PROC CLEANUP_EXT_ENTER_SANDBOX_CUTSCENE(TEST_DRIVE_DATA &sTestDriveData)
	IF IS_BIT_SET(sTestDriveData.entryAnim.iBS, BS_SIMPLE_INTERIOR_CUTSCENE_STARTED)
		CLEAR_BIT(sTestDriveData.entryAnim.iBS, BS_SIMPLE_INTERIOR_CUTSCENE_STARTED)
	ENDIF
	
	IF IS_BIT_SET(sTestDriveData.entryAnim.iBS, BS_SIMPLE_INTERIOR_CUSTOM_CUTSCENE_ON_START_CLEAR_AREA)
		CLEAR_BIT(sTestDriveData.entryAnim.iBS, BS_SIMPLE_INTERIOR_CUSTOM_CUTSCENE_ON_START_CLEAR_AREA)
	ENDIF
	
	SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_SETUP_FIRST_VEHICLE_SCENE, FALSE)
	CLEANUP_CUSTOM_CUTSCENE_ASSETS(sTestDriveData.entryAnim)
	SIMPLE_CUTSCENE_STOP(g_SimpleInteriorData.sCustomCutsceneExt, TRUE, FALSE, TRUE)
	SIMPLE_CUTSCENE_CLEAR_ALL_DATA(g_SimpleInteriorData.sCustomCutsceneExt)
	CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_CLEANUP(sTestDriveData.entryAnim.sCutsceneVeh)
	CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_CLEANUP(sTestDriveData.entryAnim.sSecondaryCutsceneVeh)
	CLEANUP_PROMO_VEH_CUTSCENE_CLONES(sTestDriveData)
	VEHICLE_SET_JET_WASH_FORCE_ENABLED(TRUE)
	NETWORK_SET_IN_MP_CUTSCENE(FALSE, FALSE)
	DISPLAY_RADAR(TRUE)
	CLEAR_FOCUS()
	
	IF DOES_ENTITY_EXIST(sTestDriveData.entryAnim.pedActor)
		DELETE_PED(sTestDriveData.entryAnim.pedActor)
	ENDIF
	
	STOP_AUDIO_SCENE(GET_TEST_DRIVE_CUTSCENE_AUDIO_SCENE())
	
	PRINTLN("[PLAY_EXT_ENTER_SANDBOX_CUTSCENE] Cleaned up.")
ENDPROC

/// PURPOSE:
///    Main update for playing the 'enter sandbox' cutscene.
/// PARAMS:
///    sTestDriveData - data struct
///    vehToClone - Vehicle to clone.
/// RETURNS:
///    TRUE if cutscene is finished playing.
FUNC BOOL PLAY_EXT_ENTER_SANDBOX_CUTSCENE(TEST_DRIVE_DATA &sTestDriveData, VEHICLE_INDEX vehToClone, BOOL bExiting = FALSE, BOOL bInPV = FALSE)
	VECTOR vVehiclePosition
	FLOAT fVehHeading
	BOOL bSkipSecondCut
	BOOL bShortenSecondCut
	FLOAT fClearAreaDist
	
	IF SHOULD_LOCAL_PLAYER_WARP_DIRECTLY_INTO_TEST_DRIVE_AREA()
		PRINTLN("[PLAY_EXT_ENTER_SANDBOX_CUTSCENE] - Warping player directly into test drive area. Skipping cutscene.")
		RETURN TRUE
	ENDIF
	
	IF (g_sTestDriveBGScript.bSkipEnterSandboxCutscene)
		PRINTLN("[PLAY_EXT_ENTER_SANDBOX_CUTSCENE] - g_sTestDriveBGScript.bSkipEnterSandboxCutscene. Skipping cutscene.")
		RETURN TRUE
	ENDIF
	
	SWITCH sTestDriveData.eCutsceneState
		CASE TDCS_NULL
			IF NOT IS_SCREEN_FADED_OUT()
				CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_CLEANUP(sTestDriveData.entryAnim.sCutsceneVeh)
				SIMPLE_CUTSCENE_CLEAR_ALL_DATA(g_SimpleInteriorData.sCustomCutsceneExt)
				SET_TEST_DRIVE_SANDBOX_CUTSCENE_STATE(sTestDriveData, TDCS_CREATE_ASSETS)
			ENDIF
			RETURN FALSE
		BREAK
		CASE TDCS_CREATE_ASSETS
			IF NOT DOES_ENTITY_EXIST(sTestDriveData.entryAnim.sCutsceneVeh.vehicle)
			AND IS_ENTITY_ALIVE(vehToClone)
				PRINTLN("[PLAY_EXT_ENTER_SANDBOX_CUTSCENE] - created clone of the player vehicle.")
				IF CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS(vehToClone, sTestDriveData.entryAnim.sCutsceneVeh, chVEHICLE_CLONE_TRAILER | chVEHICLE_CLONE_FIX_TIRES)
					CUTSCENE_HELP_DISABLE_VEHICLE_AUTOMATIC_CRASH(sTestDriveData.entryAnim.sCutsceneVeh.vehicle)
				ELSE
					RETURN FALSE
				ENDIF
				
				RETURN FALSE
			ENDIF
			
			//Grab the model min and max for use later
			GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(sTestDriveData.entryAnim.sCutsceneVeh.vehicle), 
								sTestDriveData.entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_TD_PRIMARY_VEH_MODEL_MIN], 
								sTestDriveData.entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_TD_PRIMARY_VEH_MODEL_MAX])
			
			VECTOR vEstablishingShot, vEstablishingShotEnd, vEstablishingRot, vEstablishingEndRot, vTDShot, vTDEndShot, vTDRot, vTDEndRot
			FLOAT fEstablishingFOV, fEstablishingEndFOV, fTDFOV, fTDEndFOV
			
			//Scene 1
			GET_TD_SANDBOX_VEH_SCENE_ESTABLISHING_SHOT(vEstablishingShot, vEstablishingRot, fEstablishingFOV, TRUE, bExiting)
			GET_TD_SANDBOX_VEH_SCENE_ESTABLISHING_SHOT(vEstablishingShotEnd, vEstablishingEndRot, fEstablishingEndFOV, FALSE, bExiting)
			
			//Scene 2
			GET_TD_SANDBOX_VEH_SCENE_TOP_DOWN_SHOT(vTDShot, vTDRot, fTDFOV, TRUE, bExiting)
			GET_TD_SANDBOX_VEH_SCENE_TOP_DOWN_SHOT(vTDEndShot, vTDEndRot, fTDEndFOV, FALSE, bExiting)
			
			IF (bExiting)
				bSkipSecondCut = TRUE
				bShortenSecondCut = FALSE
			ELSE
				IF IS_ENTITY_ALIVE(sTestDriveData.entryAnim.sCutsceneVeh.vehicle)
					bSkipSecondCut = IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(sTestDriveData.entryAnim.sCutsceneVeh.vehicle))
				ENDIF
				
				//If the vehicle does not qualify as extra long then use a shorter second cut
				bShortenSecondCut = !((sTestDriveData.entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_TD_PRIMARY_VEH_MODEL_MAX].y - sTestDriveData.entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_TD_PRIMARY_VEH_MODEL_MIN].y) > TD_SANDBOX_SCENE_VEH_MAX_LENGTH)
			ENDIF
			
			IF bSkipSecondCut
				SET_BIT(sTestDriveData.entryAnim.iSceneBS, CUTSCENE_BS_TD_SANDBOX_ONE_SHOT_BIKE_SCENE)
			ELSE
				CLEAR_BIT(sTestDriveData.entryAnim.iSceneBS, CUTSCENE_BS_TD_SANDBOX_ONE_SHOT_BIKE_SCENE)
			ENDIF		
			
			SIMPLE_INTERIOR_VEH_ENTRY_SCENE_CREATE_SCENE_NO_ZOOM(g_SimpleInteriorData.sCustomCutsceneExt, vEstablishingShot, vEstablishingRot, fEstablishingFOV, vEstablishingShotEnd, vEstablishingEndRot, fEstablishingEndFOV, bSkipSecondCut, vTDShot, vTDRot, fTDFOV, vTDEndShot, vTDEndRot, fTDEndFOV, bShortenSecondCut)
			CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_TOGGLE_READY(sTestDriveData.entryAnim.sCutsceneVeh, FALSE)
			
			SIMPLE_CUTSCENE_SETUP eCutsceneBitMasks
			IF (bExiting)
			OR (bInPV)
				eCutsceneBitMasks = SCS_DO_NOT_RETURN_PLAYER_CONTROL | SCS_PREVENT_VISIBLITY_CHANGES_START | SCS_LEAVE_PLAYER_REMOTELY_VISIBLE | SCS_PREVENT_FROZEN_CHANGES_START | SCS_HIDE_PROJECTILES
			ELSE
				eCutsceneBitMasks = SCS_DO_NOT_RETURN_PLAYER_CONTROL | SCS_PREVENT_VISIBLITY_CHANGES_START | SCS_PREVENT_FROZEN_CHANGES_START | SCS_HIDE_PROJECTILES
			ENDIF
			
			SIMPLE_CUTSCENE_START(g_SimpleInteriorData.sCustomCutsceneExt, eCutsceneBitMasks)
			SET_BIT(sTestDriveData.entryAnim.iBS, BS_SIMPLE_INTERIOR_CUTSCENE_STARTED)
			
			IF IS_BIT_SET(g_SimpleInteriorData.sCustomCutsceneExt.iBSFadeOutStarted, 1)
				CLEAR_BIT(g_SimpleInteriorData.sCustomCutsceneExt.iBSFadeOutStarted, 1)
			ENDIF
	
			SET_TEST_DRIVE_SANDBOX_CUTSCENE_STATE(sTestDriveData, TDCS_UPDATE)
			RETURN FALSE
		BREAK
		CASE TDCS_UPDATE
			IF (bExiting)
				CLEANUP_EXIT_SANDBOX_MENU_CAMERA(sTestDriveData.sExitSandboxMenu)
			ENDIF
			
			SIMPLE_CUTSCENE_MAINTAIN(g_SimpleInteriorData.sCustomCutsceneExt)
			
			IF NOT IS_BIT_SET(sTestDriveData.entryAnim.iBS, BS_SIMPLE_INTERIOR_CUSTOM_CUTSCENE_ON_START_CLEAR_AREA)
				IF g_SimpleInteriorData.sCustomCutsceneExt.bPlaying
					fClearAreaDist = 1000.0
					//CUTSCENE_HELP_PREPARE_AREA_FOR_CUTSCENE(sTestDriveData.entryAnim.syncScenePos, fClearAreaDist)
					REMOVE_PARTICLE_FX_IN_RANGE(sTestDriveData.entryAnim.syncScenePos, fClearAreaDist)
					SET_BIT(sTestDriveData.entryAnim.iBS, BS_SIMPLE_INTERIOR_CUSTOM_CUTSCENE_ON_START_CLEAR_AREA)
				ENDIF
			ENDIF
			
			IF SIMPLE_CUTSCENE_IS_SCENE_RUNNING(g_SimpleInteriorData.sCustomCutsceneExt, 0)
			OR SIMPLE_CUTSCENE_IS_SCENE_RUNNING(g_SimpleInteriorData.sCustomCutsceneExt, 1)
				//FIRST FRAME
				IF NOT IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(sTestDriveData, TEST_DRIVE_DATA_BS_SETUP_FIRST_VEHICLE_SCENE)
				
					CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE(sTestDriveData.entryAnim.sCutsceneVeh.vehicle, TRUE)
					CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_TOGGLE_READY(sTestDriveData.entryAnim.sCutsceneVeh, TRUE)
					
					IF IS_ENTITY_ALIVE(sTestDriveData.entryAnim.sCutsceneVeh.vehicle)
						
					
						IF (sTestDriveData.entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_TD_PRIMARY_VEH_MODEL_MAX].z - sTestDriveData.entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_TD_PRIMARY_VEH_MODEL_MIN].z) > TD_SANDBOX_SCENE_VEH_MAX_HEIGHT	
							SET_BIT(sTestDriveData.entryAnim.iSceneBS, CUTSCENE_BS_TD_SANDBOX_TALL_VEH_SCENE)
						ENDIF
						
						vVehiclePosition = TD_SANDBOX_VEH_ENTRY_VEHICLE_START_POSITION(bExiting, GET_ENTITY_MODEL(sTestDriveData.entryAnim.sCutsceneVeh.vehicle))
						fVehHeading = TD_SANDBOX_VEH_ENTRY_VEHICLE_START_HEADING(bExiting, GET_ENTITY_MODEL(sTestDriveData.entryAnim.sCutsceneVeh.vehicle))
						
						SET_ENTITY_COORDS_NO_OFFSET(sTestDriveData.entryAnim.sCutsceneVeh.vehicle, vVehiclePosition)
						SET_ENTITY_HEADING(sTestDriveData.entryAnim.sCutsceneVeh.vehicle, fVehHeading)
						
						FREEZE_ENTITY_POSITION(sTestDriveData.entryAnim.sCutsceneVeh.vehicle, FALSE)
						SET_VEHICLE_ON_GROUND_PROPERLY(sTestDriveData.entryAnim.sCutsceneVeh.vehicle)
						SET_ENTITY_COLLISION(sTestDriveData.entryAnim.sCutsceneVeh.vehicle, TRUE)
						SET_ENTITY_VISIBLE(sTestDriveData.entryAnim.sCutsceneVeh.vehicle, TRUE)
						SET_VEHICLE_ENGINE_ON(sTestDriveData.entryAnim.sCutsceneVeh.vehicle, TRUE, TRUE)
						PLAY_SOUND_FROM_ENTITY(-1, "Exit_Engine_Blips", sTestDriveData.entryAnim.sCutsceneVeh.vehicle, "Lowrider_Super_Mod_Garage_Sounds")
						
						SET_FOCUS_POS_AND_VEL(vVehiclePosition, <<0.0, 0.0, 0.0>>)
						
						sTestDriveData.entryAnim.fStateArray[CUTSCENE_FLOAT_TD_MOVE_VEH_DISTANCE] = (sTestDriveData.entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_TD_PRIMARY_VEH_MODEL_MAX].y - sTestDriveData.entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_TD_PRIMARY_VEH_MODEL_MIN].y)
						sTestDriveData.entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_TD_PRIMARY_VEH_MODEL_MAX].x = 0.0
						sTestDriveData.entryAnim.vVectorArray[CUTSCENE_VEC_ARRAY_TD_PRIMARY_VEH_MODEL_MAX].z = 0.0
						
						IF (bExiting)
							SET_VEHICLE_RADIO_ENABLED(sTestDriveData.entryAnim.sCutsceneVeh.vehicle, TRUE)
							LOCK_RADIO_STATION("RADIO_36_AUDIOPLAYER", FALSE)
							SET_VEH_RADIO_STATION(sTestDriveData.entryAnim.sCutsceneVeh.vehicle, "RADIO_36_AUDIOPLAYER")
							UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_MIX3_PARTD")
							FORCE_MUSIC_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_MIX3_PARTD" , GET_TUNER_SANDBOX_RADIO_MIX_START_MS())
							SET_VEH_RADIO_STATION(sTestDriveData.entryAnim.sCutsceneVeh.vehicle, "RADIO_36_AUDIOPLAYER")
						ELSE
							SET_VEHICLE_RADIO_ENABLED(sTestDriveData.entryAnim.sCutsceneVeh.vehicle, FALSE)
						ENDIF
						
						START_AUDIO_SCENE(GET_TEST_DRIVE_CUTSCENE_AUDIO_SCENE())
						
						//Setup complete
						SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_SETUP_FIRST_VEHICLE_SCENE, TRUE)			
					ENDIF
				ENDIF
				
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				IF IS_ENTITY_ALIVE(sTestDriveData.entryAnim.sCutsceneVeh.vehicle)
					IF (bExiting)
						IF NOT IS_VEHICLE_RADIO_ON(sTestDriveData.entryAnim.sCutsceneVeh.vehicle)
							SET_VEHICLE_RADIO_ENABLED(sTestDriveData.entryAnim.sCutsceneVeh.vehicle, TRUE)
							LOCK_RADIO_STATION("RADIO_36_AUDIOPLAYER", FALSE)
							SET_VEH_RADIO_STATION(sTestDriveData.entryAnim.sCutsceneVeh.vehicle, "RADIO_36_AUDIOPLAYER")
							UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_MIX3_PARTD")
							FORCE_MUSIC_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_MIX3_PARTD" , GET_TUNER_SANDBOX_RADIO_MIX_START_MS())
							SET_VEH_RADIO_STATION(sTestDriveData.entryAnim.sCutsceneVeh.vehicle, "RADIO_36_AUDIOPLAYER")
						ENDIF
					ELSE
						IF IS_VEHICLE_RADIO_ON(sTestDriveData.entryAnim.sCutsceneVeh.vehicle)
							SET_RADIO_TO_STATION_NAME("OFF")
						ENDIF
					ENDIF
					
					TD_SANDBOX_VEH_ENTRY_CONTROL_VEH_CLONE(sTestDriveData.entryAnim, bExiting, GET_ENTITY_MODEL(sTestDriveData.entryAnim.sCutsceneVeh.vehicle))
				ENDIF
				
				IF IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(sTestDriveData, TEST_DRIVE_DATA_BS_PLAYED_ENTER_PROMO_VEH_SCENE)
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
			PRINTLN("[EXT_ENTER_SANDBOX_CUTSCENE] ===== CURRENT SCENE =====")	// url:bugstar:7069420
			PRINTLN("[EXT_ENTER_SANDBOX_CUTSCENE] bPlaying: ", GET_STRING_FROM_BOOL(g_SimpleInteriorData.sCustomCutsceneExt.bPlaying))
			PRINTLN("[EXT_ENTER_SANDBOX_CUTSCENE] IS_BIT_SET(iBSFadeOutStarted): ", GET_STRING_FROM_BOOL(IS_BIT_SET(g_SimpleInteriorData.sCustomCutsceneExt.iBSFadeOutStarted, g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene)))
			PRINTLN("[EXT_ENTER_SANDBOX_CUTSCENE] iCurrentScene: ", g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene)
			PRINTLN("[EXT_ENTER_SANDBOX_CUTSCENE] iScenesCount: ", g_SimpleInteriorData.sCustomCutsceneExt.iScenesCount)
			PRINTLN("[EXT_ENTER_SANDBOX_CUTSCENE] iDuration: ", g_SimpleInteriorData.sCustomCutsceneExt.sScenes[g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene].iDuration)
			PRINTLN("[EXT_ENTER_SANDBOX_CUTSCENE] iCurrentSceneElapsedTime: ",g_SimpleInteriorData.sCustomCutsceneExt.iCurrentSceneElapsedTime)
			PRINTLN("[EXT_ENTER_SANDBOX_CUTSCENE] iDuration - iCurrentSceneElapsedTime: ", g_SimpleInteriorData.sCustomCutsceneExt.sScenes[g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene].iDuration - g_SimpleInteriorData.sCustomCutsceneExt.iCurrentSceneElapsedTime)
			PRINTLN("[EXT_ENTER_SANDBOX_CUTSCENE] iFadeOutTime: ", g_SimpleInteriorData.sCustomCutsceneExt.sScenes[g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene].iFadeOutTime)
			#ENDIF
			
			IF (g_SimpleInteriorData.sCustomCutsceneExt.bPlaying
			AND g_SimpleInteriorData.sCustomCutsceneExt.iScenesCount > 0
			AND g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene = g_SimpleInteriorData.sCustomCutsceneExt.iScenesCount - 1
			AND (g_SimpleInteriorData.sCustomCutsceneExt.sScenes[g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene].iDuration - g_SimpleInteriorData.sCustomCutsceneExt.iCurrentSceneElapsedTime) <= 400)
			OR NOT g_SimpleInteriorData.sCustomCutsceneExt.bPlaying
			OR (g_SimpleInteriorData.sCustomCutsceneExt.sScenes[g_SimpleInteriorData.sCustomCutsceneExt.iCurrentScene].iFadeOutTime > 0 AND IS_SCREEN_FADED_OUT())
				IF IS_SCREEN_FADED_IN()
					DO_SCREEN_FADE_OUT(GET_TEST_DRIVE_CUTSCENE_FADE_OUT_TIME(bExiting))
				ENDIF
				SET_TEST_DRIVE_SANDBOX_CUTSCENE_STATE(sTestDriveData, TDCS_CLEANUP)
			ENDIF
			
			RETURN FALSE
		BREAK
		CASE TDCS_CLEANUP
			IF NOT IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_OUT()
				PRINTLN("[PLAY_EXT_ENTER_SANDBOX_CUTSCENE] Forcing screen fade out.")
				DO_SCREEN_FADE_OUT(GET_TEST_DRIVE_CUTSCENE_FADE_OUT_TIME(bExiting))
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
				CLEANUP_EXT_ENTER_SANDBOX_CUTSCENE(sTestDriveData)
			ELSE
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	SET_TEST_DRIVE_SANDBOX_CUTSCENE_STATE(sTestDriveData, TDCS_NULL)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Return the camera pos of the promo veh shot.
/// PARAMS:
///    vehClone - vehicle for cam to look at.
/// RETURNS:
///    Real world coords of cam pos.
FUNC VECTOR GET_EXT_ENTER_PROMO_VEH_CAM_POS(INT iVehIndex, VEHICLE_INDEX vehClone)
	UNUSED_PARAMETER(vehClone)
	UNUSED_PARAMETER(iVehIndex)
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_THIS_VEHICLE_INDEX_HAOS_PREMIUM_TEST_VEHICLE(iVehIndex)
		RETURN <<-2145.9214, 1143.8986, -24.7351>>
	ENDIF
	#ENDIF
	
	RETURN <<-2153.3835, 1113.2800, -23.0181>>
	
	// --- Offset to the vehicle ---
	/*
	IF IS_ENTITY_ALIVE(vehClone)
		VECTOR vOffset = <<2.0812, 2.8213, 0.3916>>
		VECTOR vReturn = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehClone, vOffset)
		
		//PRINTLN("[GET_TEST_DRIVE_SCENE_CAM_POS] vCamCoords: ", GET_STRING_FROM_VECTOR(vCamCoords))
		PRINTLN("[GET_TEST_DRIVE_SCENE_CAM_POS] vOffset: ", GET_STRING_FROM_VECTOR(vOffset))
		PRINTLN("[GET_TEST_DRIVE_SCENE_CAM_POS] vReturn: ", GET_STRING_FROM_VECTOR(vReturn))
		RETURN vReturn
	ENDIF
	
	RETURN <<-1987.5835, 1259.8546, 51.4949>>
	*/
ENDFUNC

/// PURPOSE:
///    Return the camera rot of the promo veh shot.
/// RETURNS:
///    Real world coords of cam rot.
FUNC VECTOR GET_EXT_ENTER_PROMO_VEH_CAM_ROT(INT iVehIndex)
	UNUSED_PARAMETER(iVehIndex)
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_THIS_VEHICLE_INDEX_HAOS_PREMIUM_TEST_VEHICLE(iVehIndex)
		RETURN <<3.6375, 0.0151, -3.7125>>
	ENDIF
	#ENDIF

	RETURN <<-7.3884, -0.0000, -62.3180>>
ENDFUNC

/// PURPOSE:
///    Return the camera fov of the promo veh shot.
/// RETURNS:
///    Real world coords of cam fov.
FUNC FLOAT GET_EXT_ENTER_PROMO_VEH_CAM_FOV(INT iVehIndex)
	UNUSED_PARAMETER(iVehIndex)
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_THIS_VEHICLE_INDEX_HAOS_PREMIUM_TEST_VEHICLE(iVehIndex)
		RETURN 49.8890
	ENDIF
	#ENDIF

	RETURN 40.6900
ENDFUNC

/// PURPOSE:
///    If the ped AI is forced to leave the cutscene vehicle, skip the engine check to prevent loop. 
FUNC BOOL SKIP_ENGINE_RUNNING_CHECK_FOR_CUTSCENE()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
		IF GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_EXIT_VEHICLE)
			PRINTLN("[SKIP_ENGINE_RUNNING_CHECK_FOR_CUTSCENE] - TRUE")
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Main update for playing the 'enter promo veh' cutscene shot.
/// PARAMS:
///    sTestDriveData - data struct
///    vehToClone - vehicle to clone.
/// RETURNS:
///    TRUE if cutscene has been played.
FUNC BOOL PLAY_EXT_ENTER_PROMO_VEH_CUTSCENE(TEST_DRIVE_DATA &sTestDriveData, SERVER_TEST_DRIVE_BD_DATA &serverBD)
	IF IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(sTestDriveData, TEST_DRIVE_DATA_BS_PLAYED_ENTER_PROMO_VEH_SCENE)
		RETURN TRUE
	ENDIF
	
	IF (g_sTestDriveBGScript.bSkipEnterTestVehCutscene)
		PRINTLN("[PLAY_EXT_ENTER_SANDBOX_CUTSCENE] - g_sTestDriveBGScript.bSkipEnterTestVehCutscene. Skipping cutscene.")
		RETURN TRUE
	ENDIF
	
	VECTOR vCloneVehCoords
	FLOAT fCloneVehHeading
	INT iVehToEnter, iOn, iFullBeam, iVeh
	VEHICLE_SETUP_STRUCT_MP vehicleSetup
	VEHICLE_INDEX tempVeh
	
	SWITCH sTestDriveData.eCutsceneState
		CASE TDCS_NULL
			SET_TEST_DRIVE_SANDBOX_CUTSCENE_STATE(sTestDriveData, TDCS_CREATE_ASSETS)
			RETURN FALSE
		BREAK
		CASE TDCS_CREATE_ASSETS
			// ----- All 3 promo vehicles -----
			REPEAT NUM_PROMO_TEST_DRIVE_VEHICLES iVeh
				#IF IS_DEBUG_BUILD
				IF iVeh >= 4
					ASSERTLN("[PLAY_EXT_ENTER_PROMO_VEH_CUTSCENE] - Increase size of sTestDriveData.promoVehAnim.vehicleArray.")
					BREAKLOOP
				ENDIF
				#ENDIF
				
				IF NOT DOES_ENTITY_EXIST(sTestDriveData.promoVehAnim.vehicleArray[iVeh])
				AND IS_NET_VEHICLE_DRIVEABLE(serverBD.niTestDriveVehicle[iVeh])
				
					tempVeh = NET_TO_VEH(serverBD.niTestDriveVehicle[iVeh])
					
					vCloneVehCoords = GET_ENTITY_COORDS(tempVeh)
					fCloneVehHeading = GET_ENTITY_HEADING(tempVeh)
					
					IF CREATE_VEHICLE_CLONE(sTestDriveData.promoVehAnim.vehicleArray[iVeh], tempVeh, vCloneVehCoords, fCloneVehHeading, FALSE, FALSE)
						PRINTLN("[PLAY_EXT_ENTER_PROMO_VEH_CUTSCENE] - created clone of the player vehicle. iVeh: ", iVeh)
						
						IF DOES_ENTITY_EXIST(sTestDriveData.promoVehAnim.vehicleArray[iVeh])
							GET_VEHICLE_SETUP_MP(tempVeh, vehicleSetup)
							SET_VEHICLE_SETUP_MP(sTestDriveData.promoVehAnim.vehicleArray[iVeh], vehicleSetup)
							VEHICLE_COPY_EXTRAS(tempVeh, sTestDriveData.promoVehAnim.vehicleArray[iVeh])
							SET_ENTITY_COLLISION(sTestDriveData.promoVehAnim.vehicleArray[iVeh], FALSE)
							SET_ENTITY_VISIBLE(sTestDriveData.promoVehAnim.vehicleArray[iVeh], FALSE)
							SET_ENTITY_INVINCIBLE(sTestDriveData.promoVehAnim.vehicleArray[iVeh], TRUE)
							SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(sTestDriveData.promoVehAnim.vehicleArray[iVeh], FALSE)
							SET_VEHICLE_ON_GROUND_PROPERLY(sTestDriveData.promoVehAnim.vehicleArray[iVeh])
							FREEZE_ENTITY_POSITION(sTestDriveData.promoVehAnim.vehicleArray[iVeh], TRUE)
							SET_ENTITY_COORDS_NO_OFFSET(sTestDriveData.promoVehAnim.vehicleArray[iVeh], vCloneVehCoords)
							SET_ENTITY_HEADING(sTestDriveData.promoVehAnim.vehicleArray[iVeh], fCloneVehHeading)
				
							//LIGHTS
							GET_VEHICLE_LIGHTS_STATE(tempVeh, iOn, iFullBeam)
							IF iOn != 0
								SET_VEHICLE_LIGHTS(sTestDriveData.promoVehAnim.vehicleArray[iVeh], FORCE_VEHICLE_LIGHTS_ON)
							ENDIF
							
							IF iFullBeam != 0
								SET_VEHICLE_FULLBEAM(sTestDriveData.promoVehAnim.vehicleArray[iVeh], TRUE)
							ENDIF
							
							SET_VEHICLE_ENGINE_ON(sTestDriveData.promoVehAnim.vehicleArray[iVeh], FALSE, TRUE)
							CUTSCENE_HELP_CLONE_VEH_TRAILER(tempVeh, sTestDriveData.promoVehAnim.trailer)
							CUTSCENE_HELP_DISABLE_VEHICLE_AUTOMATIC_CRASH(sTestDriveData.promoVehAnim.vehicleArray[iVeh])
						ENDIF
					ENDIF
					
					RETURN FALSE
				ENDIF
				
			ENDREPEAT
			
			// ----- Camera -----
			IF NOT DOES_CAM_EXIST(sTestDriveData.promoVehAnim.camera)
				sTestDriveData.promoVehAnim.camera = CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)
			ENDIF
			
			SET_TEST_DRIVE_SANDBOX_CUTSCENE_STATE(sTestDriveData, TDCS_UPDATE)
			RETURN FALSE
		BREAK
		CASE TDCS_UPDATE
			iVehToEnter = sTestDriveData.iPromoVehIAmNearAsInt
			
			IF (iVehToEnter < 0 OR iVehToEnter >= NUM_PROMO_TEST_DRIVE_VEHICLES)
				ASSERTLN("[PLAY_EXT_ENTER_PROMO_VEH_CUTSCENE] - Invalid sTestDriveData.iPromoVehIAmNearAsInt: ", sTestDriveData.iPromoVehIAmNearAsInt)
				iVehToEnter = 0
			ENDIF
			
			IF IS_ENTITY_ALIVE(sTestDriveData.promoVehAnim.vehicleArray[iVehToEnter])
			AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
				// SETUP SCENE
				IF NOT IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(sTestDriveData, TEST_DRIVE_DATA_BS_SETUP_FIRST_VEHICLE_SCENE)			
					START_MP_CUTSCENE(TRUE)
					CUTSCENE_HELP_PREPARE_VEHICLE_FOR_SCENE(sTestDriveData.promoVehAnim.vehicleArray[iVehToEnter], TRUE)
					
					// ----- Cutscene Veh -----
					REPEAT NUM_PROMO_TEST_DRIVE_VEHICLES iVeh
						IF IS_ENTITY_ALIVE(sTestDriveData.promoVehAnim.vehicleArray[iVeh])
							FREEZE_ENTITY_POSITION(sTestDriveData.promoVehAnim.vehicleArray[iVeh], FALSE)
							SET_VEHICLE_ON_GROUND_PROPERLY(sTestDriveData.promoVehAnim.vehicleArray[iVeh])
							SET_ENTITY_COLLISION(sTestDriveData.promoVehAnim.vehicleArray[iVeh], TRUE)
							SET_ENTITY_VISIBLE(sTestDriveData.promoVehAnim.vehicleArray[iVeh], TRUE)
						ENDIF
					ENDREPEAT
					
					// ----- Camera -----
					IF DOES_CAM_EXIST(sTestDriveData.promoVehAnim.camera)
					AND NOT IS_CAM_ACTIVE(sTestDriveData.promoVehAnim.camera)
						SET_CAM_COORD(sTestDriveData.promoVehAnim.camera, GET_EXT_ENTER_PROMO_VEH_CAM_POS(iVehToEnter, sTestDriveData.promoVehAnim.vehicleArray[iVehToEnter]))
						SET_CAM_ROT(sTestDriveData.promoVehAnim.camera, GET_EXT_ENTER_PROMO_VEH_CAM_ROT(iVehToEnter))
						SET_CAM_FOV(sTestDriveData.promoVehAnim.camera, GET_EXT_ENTER_PROMO_VEH_CAM_FOV(iVehToEnter))
						//POINT_CAM_AT_COORD(sTestDriveData.promoVehAnim.camera, GET_ENTITY_COORDS(sTestDriveData.promoVehAnim.sCutsceneVeh.vehicle))
						SET_CAM_ACTIVE(sTestDriveData.promoVehAnim.camera, TRUE)
						SHAKE_CAM(sTestDriveData.promoVehAnim.camera, "HAND_SHAKE", 0.25)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF
					
					//Setup complete
					SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_SETUP_FIRST_VEHICLE_SCENE, TRUE)
				ENDIF
				
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_IgnoreVehicleEntryCollisionTests, TRUE)
				
				// End the cutscene if player is taking too long to enter vehicle. 
				IF HAS_NET_TIMER_STARTED_AND_EXPIRED(sTestDriveData.timerCutsceneBail, 8000)
					PRINTLN("[PLAY_EXT_ENTER_PROMO_VEH_CUTSCENE] - Timer expired. Forcing cutscene cleanup.")
					RESET_NET_TIMER(sTestDriveData.timerCutsceneBail)
					SET_TEST_DRIVE_SANDBOX_CUTSCENE_STATE(sTestDriveData, TDCS_CLEANUP)
				ENDIF
				
				IF IS_PED_SITTING_IN_THIS_VEHICLE(PLAYER_PED_ID(), sTestDriveData.promoVehAnim.vehicleArray[iVehToEnter])
				OR IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), sTestDriveData.promoVehAnim.vehicleArray[iVehToEnter])
					IF GET_IS_VEHICLE_ENGINE_RUNNING(sTestDriveData.promoVehAnim.vehicleArray[iVehToEnter])
					OR IS_PED_IN_THIS_VEHICLE(PLAYER_PED_ID(), sTestDriveData.promoVehAnim.vehicleArray[iVehToEnter])
					OR SKIP_ENGINE_RUNNING_CHECK_FOR_CUTSCENE()
						SET_TEST_DRIVE_SANDBOX_CUTSCENE_STATE(sTestDriveData, TDCS_CLEANUP)
					ELSE
						SET_VEHICLE_ENGINE_ON(sTestDriveData.promoVehAnim.vehicleArray[iVehToEnter], TRUE, TRUE)
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK
						PRINTLN("[PLAY_EXT_ENTER_PROMO_VEH_CUTSCENE] - Tasking player ped to enter vehicle.")
						TASK_ENTER_VEHICLE(PLAYER_PED_ID(), sTestDriveData.promoVehAnim.vehicleArray[iVehToEnter], 5000, VS_DRIVER, PEDMOVEBLENDRATIO_WALK, ECF_RESUME_IF_INTERRUPTED)
						REINIT_NET_TIMER(sTestDriveData.timerCutsceneBail)
					ENDIF
				ENDIF
				
				RETURN FALSE
			ELSE
				SET_TEST_DRIVE_SANDBOX_CUTSCENE_STATE(sTestDriveData, TDCS_CLEANUP)
				RETURN FALSE
			ENDIF
		BREAK
		CASE TDCS_CLEANUP
			SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_SETUP_FIRST_VEHICLE_SCENE, FALSE)
			SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_PLAYED_ENTER_PROMO_VEH_SCENE, TRUE)
			//CLEANUP_CUSTOM_CUTSCENE_ASSETS(entryAnim)
			//CUTSCENE_HELP_CLONE_VEH_AND_PASSENGERS_CLEANUP(entryAnim.sCutsceneVeh)
			
			//CLEANUP_MP_CUTSCENE(DEFAULT, FALSE)
			CLEAR_FOCUS()
			
			SET_TEST_DRIVE_SANDBOX_CUTSCENE_STATE(sTestDriveData, TDCS_NULL)
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

// ***************************************************************************************
// 					INSTANCE MENU
// ***************************************************************************************

FUNC STRING GET_TEST_DRIVE_MENU_GRAPHIC(TEST_DRIVE_MENU_GRAPHIC eGraphic = TDMG_DEFAULT)
	SWITCH eGraphic
		CASE TDMG_DEFAULT	RETURN "ShopUI_Title_Los_Santos_Car_Meet"
		#IF FEATURE_GEN9_EXCLUSIVE
		CASE TDMG_HSW		RETURN "ShopUI_Title_Hao"
		#ENDIF
	ENDSWITCH
	
	RETURN "ShopUI_Title_Los_Santos_Car_Meet"
ENDFUNC

FUNC BOOL CAN_I_JOIN_THIS_PLAYERS_TEST_DRIVE_SANDBOX(PLAYER_INDEX playerToJoin)	
	IF IS_NET_PLAYER_OK(playerToJoin)
	AND NOT IS_PLAYER_DEAD(playerToJoin)
	
		INT iLocalCrewID
		NETWORK_CLAN_DESC eClan
	
		PRINTLN("[CAN_I_JOIN_THIS_PLAYERS_TEST_DRIVE_SANDBOX] Called. Player I'm trying to join: ", GET_PLAYER_NAME(playerToJoin))
		
		// ----- Player to join is not in a joinable session -----
		IF NOT IS_PLAYER_IN_A_JOINABLE_TEST_DRIVE_SANDBOX(playerToJoin)
			PRINTLN("[CAN_I_JOIN_THIS_PLAYERS_TEST_DRIVE_SANDBOX] Player is not in a joinable sandbox! Returning FALSE")
			RETURN FALSE
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF GET_COMMANDLINE_PARAM_EXISTS("sc_TestDriveIgnoreInteractionMenuRestrictions")
			PRINTLN("[CAN_I_JOIN_THIS_PLAYERS_TEST_DRIVE_SANDBOX] Debug - sc_TestDriveIgnoreInteractionMenuRestrictions is TRUE")
			RETURN TRUE
		ENDIF
		#ENDIF
		
		// ----- Everyone -----
		IF IS_TEST_DRIVE_SANDBOX_ACCESS_EVERYONE(playerToJoin)
			PRINTLN("[CAN_I_JOIN_THIS_PLAYERS_TEST_DRIVE_SANDBOX] Everyone can enter! Returning TRUE")
			RETURN TRUE
		ENDIF
		
		// ----- Crew -----
		IF IS_TEST_DRIVE_SANDBOX_ACCESS_CREW_ONLY(playerToJoin)
			iLocalCrewID 	= GET_LOCAL_PLAYER_CLAN_ID()
			eClan 			= CHECK_PLAYER_CREW(playerToJoin) 
			
			IF NOT (iLocalCrewID = eClan.Id AND eClan.Id != 0)
				PRINTLN("[CAN_I_JOIN_THIS_PLAYERS_TEST_DRIVE_SANDBOX] I am not in the active clan. Returning FALSE")
				RETURN FALSE
			ENDIF
		ENDIF
		
		// ----- Friend -----
		IF IS_TEST_DRIVE_SANDBOX_ACCESS_FRIENDS_ONLY(playerToJoin)
			IF NOT IS_PLAYER_FRIEND(playerToJoin)
				PRINTLN("[CAN_I_JOIN_THIS_PLAYERS_TEST_DRIVE_SANDBOX] I am not friends. Returning FALSE")
				RETURN FALSE
			ENDIF
		ENDIF
		
		// ----- Crew + Friends -----
		IF IS_TEST_DRIVE_SANDBOX_ACCESS_CREW_AND_FRIENDS_ONLY(playerToJoin)
			IF NOT IS_PLAYER_FRIEND(playerToJoin)
			AND NOT GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), playerToJoin)
				PRINTLN("[CAN_I_JOIN_THIS_PLAYERS_TEST_DRIVE_SANDBOX] I'm not friends or in a crew. Returning FALSE")
				RETURN FALSE
			ENDIF
		ENDIF
		
		// ----- You shall not paaass -----
		IF IS_TEST_DRIVE_SANDBOX_ACCESS_NO_ONE(playerToJoin)
			PRINTLN("[CAN_I_JOIN_THIS_PLAYERS_TEST_DRIVE_SANDBOX] No one can enter! Returning FALSE")
			RETURN FALSE
		ENDIF
		
		// ----- Organization -----
		IF IS_TEST_DRIVE_SANDBOX_ACCESS_ORGANIZATION(playerToJoin)
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
			AND NOT GB_ARE_PLAYERS_IN_SAME_GANG(playerToJoin, PLAYER_ID())
				PRINTLN("[CAN_I_JOIN_THIS_PLAYERS_TEST_DRIVE_SANDBOX] Not in same organization! Returning FALSE")
				RETURN FALSE				
			ENDIF			
		ENDIF
		
		// ----- Associates -----
		IF IS_TEST_DRIVE_SANDBOX_ACCESS_ASSOCIATES(playerToJoin)
			BOOL bInSameGang, bFriends, bSameCrew
			GAMER_HANDLE playerHandle 		= GET_GAMER_HANDLE_PLAYER(playerToJoin)
			iLocalCrewID 		= GET_LOCAL_PLAYER_CLAN_ID()
			eClan 				= CHECK_PLAYER_CREW(playerToJoin)
			bFriends 			= NETWORK_IS_FRIEND(playerHandle)
			bSameCrew			= (iLocalCrewID = eClan.Id AND eClan.Id != 0)
			
			IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG()
				bInSameGang = GB_ARE_PLAYERS_IN_SAME_GANG(playerToJoin, PLAYER_ID())
			ENDIF
			
			IF NOT bInSameGang
			AND NOT bFriends
			AND NOT bSameCrew
				PRINTLN("[CAN_I_JOIN_THIS_PLAYERS_TEST_DRIVE_SANDBOX] I'm not an associate. Returning FALSE")
				RETURN FALSE
			ENDIF						
		ENDIF
		
		PRINTLN("[CAN_I_JOIN_THIS_PLAYERS_TEST_DRIVE_SANDBOX] I can join! Returning TRUE!")
		RETURN TRUE
		
	ENDIF
	
	PRINTLN("[CAN_I_JOIN_THIS_PLAYERS_TEST_DRIVE_SANDBOX] Returning FALSE. Player ID: ", NATIVE_TO_INT(playerToJoin))
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Detects when applicable players are in a private test drive session, and are able to be joined.
/// RETURNS:
///    TRUE if  the conditions in CAN_I_JOIN_THIS_PLAYERS_TEST_DRIVE_SANDBOX are met.
FUNC BOOL ARE_ANY_FRIENDS_OR_CREW_MEMBERS_IN_A_JOINABLE_TEST_DRIVE_SANDBOX()
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(iPlayer)
		IF NETWORK_IS_PLAYER_ACTIVE(playerID)
			IF CAN_I_JOIN_THIS_PLAYERS_TEST_DRIVE_SANDBOX(playerID)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles the PC controls for the menu. Call every frame. 
/// PARAMS:
///    sTestDriveData - data struct
///    bPrevious - previous choice
///    bNext - next choice
///    bAccept - accept 
///    bBack -  exit menu
PROC HANDLE_TEST_DRIVE_SANDBOX_MENU_PC_CONTROL(TEST_DRIVE_DATA &sTestDriveData, BOOL &bPrevious, BOOL &bNext, BOOL  &bAccept, BOOL &bBack)
	IF IS_PC_VERSION()
	AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
		IF IS_USING_CURSOR(FRONTEND_CONTROL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
			SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CURSOR_ACCEPT)
			SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CURSOR_CANCEL)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_UP)
			ENABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
			
			IF ((g_iMenuCursorItem = MENU_CURSOR_NO_ITEM) OR (g_iMenuCursorItem = MENU_CURSOR_NO_CAMERA_MOVE) OR (g_iMenuCursorItem = MENU_CURSOR_DRAG_CAM))
				ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
				ENABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
			ELSE
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_LOOK_UD)
			ENDIF
			
			HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
			
			HANDLE_MENU_CURSOR(FALSE)
		ENDIF
		
		IF IS_MENU_CURSOR_ACCEPT_PRESSED()
			IF g_iMenuCursorItem = sTestDriveData.sInstanceMenu.iCurrentSelection
				bAccept = TRUE
			ELSE
				sTestDriveData.sInstanceMenu.iCurrentSelection = g_iMenuCursorItem
				SET_CURRENT_MENU_ITEM(sTestDriveData.sInstanceMenu.iCurrentSelection)
			ENDIF
		ENDIF
		
		IF IS_MENU_CURSOR_CANCEL_PRESSED()
			bBack = TRUE
		ENDIF
		
		IF IS_MENU_CURSOR_SCROLL_UP_PRESSED()
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			bPrevious = TRUE
		ENDIF
		
		IF IS_MENU_CURSOR_SCROLL_DOWN_PRESSED()
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			bNext = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC CONTAIN_FRIENDS_LIST_CURRENT_SELECTION(TEST_DRIVE_DATA &sTestDriveData)
	IF sTestDriveData.sInstanceMenu.iCurrentSelection < 0
		sTestDriveData.sInstanceMenu.iCurrentSelection = 0
	ENDIF
	
	IF sTestDriveData.sInstanceMenu.iCurrentSelection >= sTestDriveData.sInstanceMenu.iNumberOfFriendOptions
		sTestDriveData.sInstanceMenu.iCurrentSelection = (sTestDriveData.sInstanceMenu.iNumberOfFriendOptions - 1)
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the input for the instance menu.
/// PARAMS:
///    sTestDriveData - data struct
PROC HANDLE_TEST_DRIVE_SANDBOX_MENU_INPUT(TEST_DRIVE_DATA &sTestDriveData, TEST_DRIVE_MENU_DATA &sMenuData, INT iNumberOfMenuOptions)
	BOOL bPrevious, bNext
	
	INT iLeftY
	INT iMenuInputDelay = 150
	
	IF IS_USING_CURSOR(FRONTEND_CONTROL)
		iMenuInputDelay = 110
	ENDIF
	
	SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
	HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
	
	/*
	IF (NOT IS_SYSTEM_UI_BEING_DISPLAYED())
	AND (NOT IS_PAUSE_MENU_ACTIVE())
		DISABLE_FRONTEND_THIS_FRAME()
		DISABLE_ALL_CONTROL_ACTIONS(PLAYER_CONTROL)
		ENABLE_ALL_CONTROL_ACTIONS(CAMERA_CONTROL)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_ACCEPT)
		SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_FRONTEND_CANCEL)
	ENDIF
	
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MOVE_LR)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_MOVE_UD)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_X)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	*/
	
	BOOL bAccept = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
	BOOL bBack = (IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
	iLeftY = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_MOVE_UD)
	
	IF IS_PLAYER_IN_CORONA()
	OR IS_PHONE_ONSCREEN()
	OR g_sTestDriveBGScript.bDisableExitMenuInputs
		bAccept = FALSE
	ENDIF
	
	IF NOT IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(sTestDriveData, TEST_DRIVE_DATA_BS_MENU_DELAY_TIMER_ACTIVE)
		IF NOT g_sTestDriveBGScript.bDisableExitMenuInputs
			IF IS_USING_CURSOR(FRONTEND_CONTROL)
				HANDLE_TEST_DRIVE_SANDBOX_MENU_PC_CONTROL(sTestDriveData, bPrevious, bNext, bAccept, bBack)
			ELSE
				bPrevious = ((iLeftY < 100) OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP))
				bNext = ((iLeftY > 150) OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN))
			ENDIF
			
			IF bPrevious OR bNext
				START_NET_TIMER(sMenuData.tAnalogueDelayTimer)
				SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_MENU_DELAY_TIMER_ACTIVE, TRUE)
			ENDIF
		ENDIF
	ELIF HAS_NET_TIMER_EXPIRED(sMenuData.tAnalogueDelayTimer, iMenuInputDelay)
		RESET_NET_TIMER(sMenuData.tAnalogueDelayTimer)
		SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_MENU_DELAY_TIMER_ACTIVE, FALSE)
	ENDIF
	
	IF bNext
		sMenuData.iCurrentSelection++
		
		IF sMenuData.eLayer = TDML_SELECT_OPTIONS
			IF sMenuData.iCurrentSelection >= iNumberOfMenuOptions
				sMenuData.iCurrentSelection = 0
			ENDIF
		ELIF sMenuData.eLayer = TDML_SELECT_FRIEND_TO_JOIN
			IF sMenuData.iCurrentSelection >= sMenuData.iNumberOfFriendOptions
				sMenuData.iCurrentSelection = 0
			ENDIF
		ENDIF
		
		PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	ENDIF
	
	IF bPrevious
		sMenuData.iCurrentSelection--
		
		IF sMenuData.eLayer = TDML_SELECT_OPTIONS
			IF sMenuData.iCurrentSelection < 0
				sMenuData.iCurrentSelection = (iNumberOfMenuOptions - 1)
			ENDIF
		ELIF sMenuData.eLayer = TDML_SELECT_FRIEND_TO_JOIN
			IF sMenuData.iCurrentSelection < 0
				sMenuData.iCurrentSelection = (sMenuData.iNumberOfFriendOptions - 1)
			ENDIF
		ENDIF
		
		PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	ENDIF
	
	IF bAccept
		PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_MENU_OPTION_ACCEPTED, TRUE)
	ENDIF
	
	IF bBack
	OR SHOULD_LAUNCH_CRIMINAL_STARTER_PACK_BROWSER()
	OR IS_BROWSER_OPEN()
		PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET")
		sMenuData.iCurrentSelection = 0
		SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_MENU_OPTION_BACK, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Adds the menu choices. Call every frame. 
/// PARAMS:
///    sTestDriveData - data struct
PROC BUILD_TEST_DRIVE_SANDBOX_INSTANCE_MENU(TEST_DRIVE_DATA &sTestDriveData)	
	SETUP_MENU_WITH_TITLE("INST_MENU_TTL")
	
	REQUEST_STREAMED_TEXTURE_DICT(GET_TEST_DRIVE_MENU_GRAPHIC(sTestDriveData.eMenuGraphic))
	IF HAS_STREAMED_TEXTURE_DICT_LOADED(GET_TEST_DRIVE_MENU_GRAPHIC(sTestDriveData.eMenuGraphic))
		SET_MENU_USES_HEADER_GRAPHIC(TRUE, GET_TEST_DRIVE_MENU_GRAPHIC(sTestDriveData.eMenuGraphic), GET_TEST_DRIVE_MENU_GRAPHIC(sTestDriveData.eMenuGraphic))	
	ENDIF
	
	IF sTestDriveData.sInstanceMenu.eLayer = TDML_SELECT_OPTIONS
			ADD_MENU_ITEM_TEXT(TEST_DRIVE_INSTANCE_MENU_SELECTION_PUBLIC, "INST_MENU_O_0", 0, TRUE)
			ADD_MENU_ITEM_TEXT(TEST_DRIVE_INSTANCE_MENU_SELECTION_ENTER_ALONE, "INST_MENU_O_1", 0, TRUE)
			ADD_MENU_ITEM_TEXT(TEST_DRIVE_INSTANCE_MENU_SELECTION_JOIN, "INST_MENU_O_2", 0, sTestDriveData.sInstanceMenu.bJoinOptionShouldBeActive)
			SET_CURRENT_MENU_ITEM(sTestDriveData.sInstanceMenu.iCurrentSelection)
	ELIF sTestDriveData.sInstanceMenu.eLayer = TDML_SELECT_FRIEND_TO_JOIN
		LANGUAGE_TYPE eLang = GET_CURRENT_LANGUAGE()
		BOOL bUseCondensedText = eLang <> LANGUAGE_CHINESE_SIMPLIFIED AND eLang <> LANGUAGE_CHINESE AND eLang <> LANGUAGE_KOREAN AND eLang <> LANGUAGE_JAPANESE
		
		INT iOptionIndex 
		REPEAT NUM_NETWORK_PLAYERS iOptionIndex
			IF sTestDriveData.sInstanceMenu.playerInThisSelection[iOptionIndex] != INVALID_PLAYER_INDEX()
				ADD_MENU_ITEM_TEXT(iOptionIndex, "INST_MENU_O_X", 1)
				
				STRING strPlayerName = GET_PLAYER_NAME(sTestDriveData.sInstanceMenu.playerInThisSelection[iOptionIndex])
				
				IF bUseCondensedText 
					ADD_MENU_ITEM_TEXT_COMPONENT_PLAYER_NAME(strPlayerName)		
				ELSE
					ADD_MENU_ITEM_TEXT_COMPONENT_LITERAL(strPlayerName)
				ENDIF
			ENDIF
		ENDREPEAT
		
		CONTAIN_FRIENDS_LIST_CURRENT_SELECTION(sTestDriveData)
	ENDIF
	
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
ENDPROC

/// PURPOSE:
///    Updates the description of the menu choices. Called every frame. 
/// PARAMS:
///    sTestDriveData - data struct
PROC MAINTAIN_TEST_DRIVE_SANDBOX_INSTANCE_MENU_DESCRIPTION(TEST_DRIVE_DATA &sTestDriveData)
	SWITCH sTestDriveData.sInstanceMenu.eLayer
		CASE TDML_SELECT_OPTIONS
			SWITCH sTestDriveData.sInstanceMenu.iCurrentSelection
				CASE TEST_DRIVE_INSTANCE_MENU_SELECTION_PUBLIC
					SET_CURRENT_MENU_ITEM_DESCRIPTION("INST_MENU_D_0")
				BREAK
				CASE TEST_DRIVE_INSTANCE_MENU_SELECTION_ENTER_ALONE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("INST_MENU_D_1")
				BREAK
				CASE TEST_DRIVE_INSTANCE_MENU_SELECTION_JOIN
					IF sTestDriveData.sInstanceMenu.bJoinOptionShouldBeActive
						SET_CURRENT_MENU_ITEM_DESCRIPTION("INST_MENU_D_2A")
					ELSE
						SET_CURRENT_MENU_ITEM_DESCRIPTION("INST_MENU_D_2B")
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE TDML_SELECT_FRIEND_TO_JOIN
			// Descriptions for each friend? Nah
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL CAN_JOIN_PLAYERS_TEST_TRACK(PLAYER_INDEX playerID)
	IF NETWORK_IS_PLAYER_ACTIVE(playerID)
	AND IS_PLAYER_TEST_DRIVING_A_VEHICLE(playerID)
	AND CAN_I_JOIN_THIS_PLAYERS_TEST_DRIVE_SANDBOX(playerID)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEAR_PLAYER_FROM_JOIN_LIST(TEST_DRIVE_DATA &sTestDriveData, PLAYER_INDEX playerID)
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		IF sTestDriveData.sInstanceMenu.playerInThisSelection[i] = playerID
			sTestDriveData.sInstanceMenu.playerInThisSelection[i] = INVALID_PLAYER_INDEX()
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Add conditions here to determine if the menu should be rebuilt. Used to hide / display menu options.
/// PARAMS:
///    sTestDriveData - data struct
/// RETURNS:
///    TRUE if below conditions are met.
FUNC BOOL SHOULD_INSTANCE_MENU_BE_REBUILT(TEST_DRIVE_DATA &sTestDriveData)
	IF IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(sTestDriveData, TEST_DRIVE_DATA_BS_REBUILD_MENU)
		RETURN FALSE
	ENDIF
	
	IF (sTestDriveData.sInstanceMenu.eLayer = TDML_SELECT_OPTIONS)
		
		// ----- Friends are available to join -----
		IF (sTestDriveData.sInstanceMenu.bJoinOptionShouldBeActive)
			IF NOT ARE_ANY_FRIENDS_OR_CREW_MEMBERS_IN_A_JOINABLE_TEST_DRIVE_SANDBOX()
				PRINTLN("[TEST_DRIVE_SANDBOX][SHOULD_INSTANCE_MENU_BE_REBUILT] TRUE - No friends are in a joinable session.")
				sTestDriveData.sInstanceMenu.bJoinOptionShouldBeActive = FALSE
				RETURN TRUE
			ENDIF
		ELSE
			IF ARE_ANY_FRIENDS_OR_CREW_MEMBERS_IN_A_JOINABLE_TEST_DRIVE_SANDBOX()
				PRINTLN("[TEST_DRIVE_SANDBOX][SHOULD_INSTANCE_MENU_BE_REBUILT] TRUE - A friend or crew member in a joinable session.")
				sTestDriveData.sInstanceMenu.bJoinOptionShouldBeActive = TRUE
				RETURN TRUE
			ENDIF
		ENDIF
		
		// ----- Display Friends menu -----
		IF IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(sTestDriveData, TEST_DRIVE_DATA_BS_MENU_OPTION_ACCEPTED)
			IF sTestDriveData.sInstanceMenu.iCurrentSelection = TEST_DRIVE_INSTANCE_MENU_SELECTION_JOIN
				IF sTestDriveData.sInstanceMenu.bJoinOptionShouldBeActive
					PRINTLN("[TEST_DRIVE_SANDBOX][SHOULD_INSTANCE_MENU_BE_REBUILT] TRUE - Changing from TDML_SELECT_OPTIONS to TDML_SELECT_FRIEND_TO_JOIN.")
					sTestDriveData.sInstanceMenu.eLayer = TDML_SELECT_FRIEND_TO_JOIN
					sTestDriveData.sInstanceMenu.iCurrentSelection = 0
					SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_MENU_OPTION_ACCEPTED, FALSE)
					RETURN TRUE
				ELSE
					SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_MENU_OPTION_ACCEPTED, FALSE)
				ENDIF
			ENDIF
		ENDIF
	ELIF (sTestDriveData.sInstanceMenu.eLayer = TDML_SELECT_FRIEND_TO_JOIN)
		// ----- Go back to options menu -----
		IF IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(sTestDriveData, TEST_DRIVE_DATA_BS_MENU_OPTION_BACK)
			PRINTLN("[TEST_DRIVE_SANDBOX][SHOULD_INSTANCE_MENU_BE_REBUILT] TRUE - Changing from TDML_SELECT_FRIEND_TO_JOIN to TDML_SELECT_OPTIONS.")
			sTestDriveData.sInstanceMenu.eLayer = TDML_SELECT_OPTIONS
			SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_MENU_OPTION_BACK, FALSE)
			RETURN TRUE
		ENDIF
		
		// ----- Friend availability has changed -----
		BOOL bShouldRebuild = FALSE
		
		INT iPlayerIndex
		INT iOptionIndex
		
		sTestDriveData.sInstanceMenu.iNumberOfFriendOptions = 0
		
		REPEAT NUM_NETWORK_PLAYERS iPlayerIndex
			PLAYER_INDEX playerID = INT_TO_PLAYERINDEX(iPlayerIndex)
			
			IF (playerID != PLAYER_ID())
				IF CAN_JOIN_PLAYERS_TEST_TRACK(playerID)
					IF sTestDriveData.sInstanceMenu.playerInThisSelection[iOptionIndex] != playerID
						PRINTLN("[TEST_DRIVE_SANDBOX][SHOULD_INSTANCE_MENU_BE_REBUILT] TRUE - Adding Player: ", GET_PLAYER_NAME(playerID), " to option ", iOptionIndex)
						CLEAR_PLAYER_FROM_JOIN_LIST(sTestDriveData, playerID)
						sTestDriveData.sInstanceMenu.playerInThisSelection[iOptionIndex] = playerID
						iOptionIndex++
						sTestDriveData.sInstanceMenu.iNumberOfFriendOptions++
						bShouldRebuild = TRUE
					ELIF sTestDriveData.sInstanceMenu.playerInThisSelection[iOptionIndex] != INVALID_PLAYER_INDEX()
						// Slot assigned
						iOptionIndex++
						sTestDriveData.sInstanceMenu.iNumberOfFriendOptions++
					ENDIF
				ELIF sTestDriveData.sInstanceMenu.playerInThisSelection[iOptionIndex] = playerID
					PRINTLN("[TEST_DRIVE_SANDBOX][SHOULD_INSTANCE_MENU_BE_REBUILT] TRUE - Clearing option slot ", iOptionIndex)
					sTestDriveData.sInstanceMenu.playerInThisSelection[iOptionIndex] = INVALID_PLAYER_INDEX()
					bShouldRebuild = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF (sTestDriveData.sInstanceMenu.iNumberOfFriendOptions = 0)
			PRINTLN("[TEST_DRIVE_SANDBOX][SHOULD_INSTANCE_MENU_BE_REBUILT] TRUE - No friend options.")
			sTestDriveData.sInstanceMenu.eLayer = TDML_SELECT_OPTIONS
			bShouldRebuild = TRUE
		ENDIF
		
		RETURN bShouldRebuild
		// ----- End of friend availability -----
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Main update for the sanbox instance menu. Called every frame. 
/// PARAMS:
///    sTestDriveData - data struct
PROC DRAW_TEST_DRIVE_SANDBOX_INSTANCE_MENU(TEST_DRIVE_DATA &sTestDriveData)
	IF NOT IS_PAUSE_MENU_ACTIVE_EX()
		HANDLE_TEST_DRIVE_SANDBOX_MENU_INPUT(sTestDriveData, sTestDriveData.sInstanceMenu, 3)
		
		IF SHOULD_INSTANCE_MENU_BE_REBUILT(sTestDriveData)
			SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_REBUILD_MENU, TRUE)
		ENDIF
		
		REQUEST_STREAMED_TEXTURE_DICT(GET_TEST_DRIVE_MENU_GRAPHIC(sTestDriveData.eMenuGraphic))
		
		IF LOAD_MENU_ASSETS()
		AND HAS_STREAMED_TEXTURE_DICT_LOADED(GET_TEST_DRIVE_MENU_GRAPHIC(sTestDriveData.eMenuGraphic))
			IF IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(sTestDriveData, TEST_DRIVE_DATA_BS_REBUILD_MENU)
				CLEAR_MENU_DATA()
				BUILD_TEST_DRIVE_SANDBOX_INSTANCE_MENU(sTestDriveData)
				SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_REBUILD_MENU, FALSE)
			ELSE
				SET_CURRENT_MENU_ITEM(sTestDriveData.sInstanceMenu.iCurrentSelection)
			ENDIF
			
			MAINTAIN_TEST_DRIVE_SANDBOX_INSTANCE_MENU_DESCRIPTION(sTestDriveData)
			DRAW_MENU()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Main method for initialising the instance menu data. Call for 1 frame. 
/// PARAMS:
///    sTestDriveData - data struct
///    bResetMenuSelection - reset the current selection int
PROC TEST_DRIVE_SANDBOX_INSTANCE_MENU_INIT(TEST_DRIVE_DATA &sTestDriveData, BOOL bResetMenuSelection = FALSE)
	IF bResetMenuSelection
		sTestDriveData.sInstanceMenu.iCurrentSelection = 0
	ENDIF
	
	// --- Player firends list -----
	INT i
	REPEAT NUM_NETWORK_PLAYERS i
		sTestDriveData.sInstanceMenu.playerInThisSelection[i] = INVALID_PLAYER_INDEX()
	ENDREPEAT
	
	RESET_NET_TIMER(sTestDriveData.sInstanceMenu.tAnalogueDelayTimer)
	SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_REBUILD_MENU, TRUE)
ENDPROC

/// PURPOSE:
///    Main method for cleaning up the instabnce menu data. Call for 1 frame. 
/// PARAMS:
///    sTestDriveData - data struct
PROC TEST_DRIVE_SANDBOX_INSTANCE_MENU_CLEANUP(TEST_DRIVE_DATA &sTestDriveData)
	TEST_DRIVE_SANDBOX_INSTANCE_MENU_INIT(sTestDriveData, TRUE)
	CLEANUP_MENU_ASSETS()
	
	IF ARE_STRINGS_EQUAL(g_sMenuData.tl15Title, "INST_MENU_TTL")
		g_sMenuData.tl15Title = ""
	ENDIF
	
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(GET_TEST_DRIVE_MENU_GRAPHIC(sTestDriveData.eMenuGraphic))
	
	SET_TEST_DRIVE_MENU_GRAPHIC(sTestDriveData, TDMG_DEFAULT)
	PRINTLN("[TEST_DRIVE_SANDBOX][TEST_DRIVE_SANDBOX_INSTANCE_MENU_CLEANUP] Called")
ENDPROC

// ***************************************************************************************
// 					EXIT SANDBOX MENU
// ***************************************************************************************

PROC MAINTAIN_TEST_DRIVE_SANDBOX_EXIT_MENU_DESCRIPTION(TEST_DRIVE_DATA &sTestDriveData)
	SWITCH sTestDriveData.sExitSandboxMenu.iCurrentSelection
		CASE TEST_DRIVE_EXIT_SANDBOX_MENU_SELECTION_EXIT_MEET
			IF NOT SHOULD_PLAYER_BE_WARPING_BACK_INTO_PV_AFTER_TEST_DRIVE(PLAYER_ID())
				SET_CURRENT_MENU_ITEM_DESCRIPTION("EXIT_MENU_D_1A")
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC BUILD_TEST_DRIVE_SANDBOX_EXIT_MENU(TEST_DRIVE_DATA &sTestDriveData)	
	SETUP_MENU_WITH_TITLE("EXIT_MENU_TTL")
	
	REQUEST_STREAMED_TEXTURE_DICT(GET_TEST_DRIVE_MENU_GRAPHIC(sTestDriveData.eMenuGraphic))
	IF HAS_STREAMED_TEXTURE_DICT_LOADED(GET_TEST_DRIVE_MENU_GRAPHIC(sTestDriveData.eMenuGraphic))
		SET_MENU_USES_HEADER_GRAPHIC(TRUE, GET_TEST_DRIVE_MENU_GRAPHIC(sTestDriveData.eMenuGraphic), GET_TEST_DRIVE_MENU_GRAPHIC(sTestDriveData.eMenuGraphic))	
	ENDIF
	
	ADD_MENU_ITEM_TEXT(TEST_DRIVE_EXIT_SANDBOX_MENU_SELECTION_ENTER_MEET, "EXIT_MENU_O_0", 0, TRUE)
	ADD_MENU_ITEM_TEXT(TEST_DRIVE_EXIT_SANDBOX_MENU_SELECTION_EXIT_MEET, "EXIT_MENU_O_1", 0, SHOULD_PLAYER_BE_WARPING_BACK_INTO_PV_AFTER_TEST_DRIVE(PLAYER_ID()))
	SET_CURRENT_MENU_ITEM(sTestDriveData.sExitSandboxMenu.iCurrentSelection)
	
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
ENDPROC

PROC DRAW_TEST_DRIVE_SANDBOX_EXIT_SANDBOX_MENU(TEST_DRIVE_DATA &sTestDriveData)
	IF NOT IS_PAUSE_MENU_ACTIVE_EX()
		HANDLE_TEST_DRIVE_SANDBOX_MENU_INPUT(sTestDriveData, sTestDriveData.sExitSandboxMenu, 2)
		REQUEST_STREAMED_TEXTURE_DICT(GET_TEST_DRIVE_MENU_GRAPHIC(sTestDriveData.eMenuGraphic))
		
		IF LOAD_MENU_ASSETS("SNK_MNU")
		AND HAS_STREAMED_TEXTURE_DICT_LOADED(GET_TEST_DRIVE_MENU_GRAPHIC(sTestDriveData.eMenuGraphic))
			IF IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(sTestDriveData, TEST_DRIVE_DATA_BS_REBUILD_MENU)
				CLEAR_MENU_DATA()
				BUILD_TEST_DRIVE_SANDBOX_EXIT_MENU(sTestDriveData)
				SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_REBUILD_MENU, FALSE)
			ELSE
				SET_CURRENT_MENU_ITEM(sTestDriveData.sExitSandboxMenu.iCurrentSelection)
			ENDIF
			
			MAINTAIN_TEST_DRIVE_SANDBOX_EXIT_MENU_DESCRIPTION(sTestDriveData)
			DRAW_MENU()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
		ENDIF
	ENDIF
ENDPROC

PROC TEST_DRIVE_SANDBOX_EXIT_SANDBOX_MENU_INIT(TEST_DRIVE_DATA &sTestDriveData)
	sTestDriveData.sExitSandboxMenu.iCurrentSelection = 0
	RESET_NET_TIMER(sTestDriveData.sInstanceMenu.tAnalogueDelayTimer)
	SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_REBUILD_MENU, TRUE)
ENDPROC

PROC TEST_DRIVE_SANDBOX_EXIT_SANDBOX_MENU_CLEANUP(TEST_DRIVE_DATA &sTestDriveData)
	TEST_DRIVE_SANDBOX_EXIT_SANDBOX_MENU_INIT(sTestDriveData)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(GET_TEST_DRIVE_MENU_GRAPHIC(sTestDriveData.eMenuGraphic))
	CLEANUP_MENU_ASSETS()
	PRINTLN("[TEST_DRIVE_SANDBOX][TEST_DRIVE_SANDBOX_EXIT_SANDBOX_MENU_CLEANUP] Called")
ENDPROC


// ***************************************************************************************
// 					END OF EXIT SANDBOX MENU
// ***************************************************************************************

/// PURPOSE:
///     Set the properties of the local ped whilst test driving a vehicle
PROC SET_TEST_DRIVE_PED_PROPERTIES(TEST_DRIVE_DATA &sTestDriveData)
	IF IS_PLAYER_TEST_DRIVING_A_VEHICLE(PLAYER_ID())
	OR IS_PLAYER_TEST_DRIVING_A_VEHICLE_AS_PASSENGER(PLAYER_ID())
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_HARD)
		ENDIF
		SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_SET_TEST_DRIVE_PED_PROPERTIES, TRUE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up the properties set in SET_TEST_DRIVE_PED_PROPERTIES
PROC CLEANUP_TEST_DRIVE_PED_PROPERTIES(TEST_DRIVE_DATA &sTestDriveData)
	IF IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(sTestDriveData, TEST_DRIVE_DATA_BS_SET_TEST_DRIVE_PED_PROPERTIES)
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_DEFAULT)
		ENDIF
	ENDIF
ENDPROC

PROC REQUEST_ENTER_TEST_DRIVE_AUDIO_BANK(TEST_DRIVE_DATA &sTestDriveData)
	IF NOT IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(sTestDriveData, TEST_DRIVE_DATA_BS_REQUESTED_AUDIO_BANK)
		REQUEST_SCRIPT_AUDIO_BANK("DLC_TUNER/DLC_Tuner_Car_Meet_Test_Area")
		SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_REQUESTED_AUDIO_BANK, TRUE)
	ENDIF
ENDPROC

PROC RELEASE_ENTER_TEST_DRIVE_AUDIO_BANK(TEST_DRIVE_DATA &sTestDriveData)
	IF NOT IS_TEST_DRIVE_SANDBOX_LOCAL_DATA_BIT_SET(sTestDriveData, TEST_DRIVE_DATA_BS_RELEASED_AUDIO_BANK)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("DLC_TUNER/DLC_Tuner_Car_Meet_Test_Area")
		SET_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData, TEST_DRIVE_DATA_BS_RELEASED_AUDIO_BANK, TRUE)
	ENDIF
ENDPROC


// ***************************************************************************************
// 					MAIN
// ***************************************************************************************

/// PURPOSE:
///    Main update for display help text for test drive. Called every frame.
/// PARAMS:
///    serverBD - server struct
PROC UPDATE_TEST_DRIVE_SANDBOX_HELP_TEXT(TEST_DRIVE_DATA &sTestDriveData, SERVER_TEST_DRIVE_BD_DATA &serverBD)
	IF NETWORK_IS_PLAYER_A_PARTICIPANT(PLAYER_ID())
	AND IS_ENTITY_ALIVE(PLAYER_PED_ID())
	
		// Prompt to enter a promo test drive vehicle. 
		IF IS_PLAYER_NEAR_ANY_PROMO_TEST_DRIVE_SANDBOX_VEHICLE(sTestDriveData, serverBD)
		AND CAN_PLAYER_START_TEST_DRIVE_SANDBOX(sTestDriveData)
		AND CAN_TEST_DRIVE_PROMO_VEHICLE(sTestDriveData)
		AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		AND NOT MPGlobals.bStartedMPCutscene
		AND NOT IS_PLAYER_MOVING_FROM_SHOWROOM_TO_SANDBOX_AREA(PLAYER_ID())
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP_FOREVER(GET_TEST_DRIVE_SANDBOX_PROMO_VEH_START_HELP_TEXT(sTestDriveData))
			ENDIF
		ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(GET_TEST_DRIVE_SANDBOX_PROMO_VEH_START_HELP_TEXT(sTestDriveData))
			CLEAR_HELP()
		ENDIF
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Cleans up the everything to do with the test drive functionality. Call for 1 frame.
/// PARAMS:
///    sTestDriveData - struct data
///    serverBD - server data
PROC TEST_DRIVE_SANDBOX_CLEANUP(TEST_DRIVE_DATA &sTestDriveData, SERVER_TEST_DRIVE_BD_DATA &serverBD)
	UNUSED_PARAMETER(serverBD)
	
	TEST_DRIVE_SANDBOX_INSTANCE_MENU_CLEANUP(sTestDriveData)
	CLEAR_ALL_TEST_DRIVE_SANDBOX_LOCAL_DATA_BS(sTestDriveData)
	CLEANUP_PROMO_VEH_CUTSCENE_CLONES(sTestDriveData)
	RESET_NET_TIMER(sTestDriveData.sKickOutOfPromoVehTimer)
ENDPROC

/// PURPOSE:
///    Init data for test drive functionality. Call for 1 frame.
/// PARAMS:
///    sTestDriveData - struct data
///    serverBD - server data
PROC TEST_DRIVE_SANDBOX_INIT(TEST_DRIVE_DATA &sTestDriveData, SERVER_TEST_DRIVE_BD_DATA &serverBD)
	REGISTER_TEST_DRIVE_SANDBOX_PLAYER_BROADCAST_DATA(sTestDriveData)
	REGISTER_TEST_DRIVE_SANDBOX_SERVER_BROADCAST_DATA(serverBD)
	
	TEST_DRIVE_SANDBOX_INSTANCE_MENU_INIT(sTestDriveData, TRUE)
ENDPROC

FUNC INT GET_TIME_REMAINING_FOR_SPRINT_RACE_LAUNCH()
	INT i
	
	IF MPGlobalsAmbience.sMagnateGangBossData.bSprintRaceEnabledStartTimerUI	
		REPEAT MAX_NUMBER_HUD_ELEMENT i
			IF NOT IS_STRING_NULL_OR_EMPTY(MPGlobalsScoreHud.ElementHud_TIMER.sGenericTimer_TimerTitle[i])
			AND GET_HASH_KEY(MPGlobalsScoreHud.ElementHud_TIMER.sGenericTimer_TimerTitle[i]) = HASH("TSA_HUD_START")
				RETURN MPGlobalsScoreHud.ElementHud_TIMER.iGenericTimer_Timer[i]
			ENDIF
		ENDREPEAT
	ENDIF
	
	RETURN -1
ENDFUNC

FUNC BOOL IS_SPRINT_RACE_LAUNCHING_SOON(INT iTimeBeforeLaunch = 5000)
	IF NOT IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	INT iTimeRemaining = GET_TIME_REMAINING_FOR_SPRINT_RACE_LAUNCH()
	
	IF iTimeRemaining != -1
	AND iTimeRemaining <= iTimeBeforeLaunch
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

#ENDIF // #IF FEATURE_TUNER



