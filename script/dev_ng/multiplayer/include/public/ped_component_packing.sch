
USING "globals.sch"

CONST_INT MAX_NUM_OF_SETTABLE_COMPONENTS 10

STRUCT PED_COMPONENT_DATA
	INT iDrawable
	INT iTexture
ENDSTRUCT


STRUCT PED_COMPONENT_CONFIG
	PED_COMPONENT_DATA ComponentData[MAX_NUM_OF_SETTABLE_COMPONENTS]
ENDSTRUCT

//STRUCT SERVER_BROADCAST_DATA2
//	PED_COMPONENT_CONFIG PedComponentConfig[MAX_NUMBER_OF_NIGHTCLUB_PED_DATA]
//ENDSTRUCT

PROC Pack3BitData(INT &iBitSet, INT iValue, INT iIndex)

#IF IS_DEBUG_BUILD
	CDEBUG1LN(DEBUG_NET_PED_DANCING, "Pack3BitData: iValue ", iValue, " iIndex = ", iIndex)

	IF (iValue < 0)
	OR (iValue > 7)
		SCRIPT_ASSERT("Pack3BitData check iValue")
	ENDIF
	
	IF (iIndex < 0)
	OR (iIndex > 29)
		SCRIPT_ASSERT("Pack3BitData check iIndex")
	ENDIF	
#ENDIF

	SWITCH iValue
		CASE 0
			CLEAR_BIT(iBitSet, iIndex)
			CLEAR_BIT(iBitSet, iIndex+1)
			CLEAR_BIT(iBitSet, iIndex+2)
		BREAK
		CASE 1
			SET_BIT(iBitSet, iIndex)
			CLEAR_BIT(iBitSet, iIndex+1)
			CLEAR_BIT(iBitSet, iIndex+2)
		BREAK
		CASE 2
			CLEAR_BIT(iBitSet, iIndex)
			SET_BIT(iBitSet, iIndex+1)
			CLEAR_BIT(iBitSet, iIndex+2)
		BREAK
		CASE 3
			SET_BIT(iBitSet, iIndex)
			SET_BIT(iBitSet, iIndex+1)
			CLEAR_BIT(iBitSet, iIndex+2)
		BREAK
		CASE 4
			CLEAR_BIT(iBitSet, iIndex)
			CLEAR_BIT(iBitSet, iIndex+1)
			SET_BIT(iBitSet, iIndex+2)
		BREAK
		CASE 5
			SET_BIT(iBitSet, iIndex)
			CLEAR_BIT(iBitSet, iIndex+1)
			SET_BIT(iBitSet, iIndex+2)
		BREAK
		CASE 6
			CLEAR_BIT(iBitSet, iIndex)
			SET_BIT(iBitSet, iIndex+1)
			SET_BIT(iBitSet, iIndex+2)
		BREAK
		CASE 7
			SET_BIT(iBitSet, iIndex)
			SET_BIT(iBitSet, iIndex+1)
			SET_BIT(iBitSet, iIndex+2)
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT UnPack3BitData(INT &iBitSet, INT iIndex)
	INT iValue = 0
	IF IS_BIT_SET(iBitSet, iIndex)
		iValue += 1
	ENDIF
	IF IS_BIT_SET(iBitSet, iIndex+1)
		iValue += 2
	ENDIF
	IF IS_BIT_SET(iBitSet, iIndex+2)
		iValue += 4
	ENDIF	
	RETURN iValue
ENDFUNC

PROC PACK_PED_COMPONENT_CONFIG(PED_COMPONENT_CONFIG ComponentConfig, INT &iPackedDrawable, INT &iPackedTexture)
	INT i
	INT iIndex
	REPEAT MAX_NUM_OF_SETTABLE_COMPONENTS i
		iIndex = i * 3 // 3 bits each
		Pack3BitData(iPackedDrawable, ComponentConfig.ComponentData[i].iDrawable, iIndex) 
		Pack3BitData(iPackedTexture, ComponentConfig.ComponentData[i].iTexture, iIndex) 
	ENDREPEAT	
ENDPROC

PROC GET_PACKED_PED_COMPONENT_CONFIG(PED_INDEX PedID, INT &iPackedDrawable, INT &iPackedTexture)

	INT i
	PED_COMPONENT_CONFIG ComponentConfig
	INT iDrawable, iTexture
	
	REPEAT NUM_PED_COMPONENTS i
		iDrawable = GET_PED_DRAWABLE_VARIATION(PedID, INT_TO_ENUM(PED_COMPONENT, i))
		iTexture = GET_PED_TEXTURE_VARIATION(PedID, INT_TO_ENUM(PED_COMPONENT, i))
		IF (i < MAX_NUM_OF_SETTABLE_COMPONENTS)																				
			ComponentConfig.ComponentData[i].iDrawable = iDrawable
			ComponentConfig.ComponentData[i].iTexture = iTexture
		ENDIF									
	ENDREPEAT
	
	PACK_PED_COMPONENT_CONFIG(ComponentConfig, iPackedDrawable, iPackedTexture)
	
ENDPROC

PROC UNPACK_PED_COMPONENT_CONFIG(PED_COMPONENT_CONFIG &ComponentConfig, INT iPackedDrawable, INT iPackedTexture)
	INT i
	INT iIndex
	REPEAT MAX_NUM_OF_SETTABLE_COMPONENTS i
		iIndex = i * 3 // 3 bits each
		ComponentConfig.ComponentData[i].iDrawable = UnPack3BitData(iPackedDrawable, iIndex)
		ComponentConfig.ComponentData[i].iTexture = UnPack3BitData(iPackedTexture, iIndex)
	ENDREPEAT	
ENDPROC

FUNC BOOL IS_PED_COMPONENT_DATA_VALID(INT iDrawable, INT iTexture)
	RETURN (iDrawable > 0 OR iTexture > 0)
ENDFUNC

PROC APPLY_PED_STYLE_RANDOM_COMPONENTS(PED_INDEX PedID, PED_COMPONENT_CONFIG ComponentConfig)
	INT iPedComponent
	REPEAT MAX_NUM_OF_SETTABLE_COMPONENTS iPedComponent
		SET_PED_COMPONENT_VARIATION(PedID, INT_TO_ENUM(PED_COMPONENT, iPedComponent), ComponentConfig.ComponentData[iPedComponent].iDrawable, ComponentConfig.ComponentData[iPedComponent].iTexture) 
	ENDREPEAT
ENDPROC

PROC APPLY_PACKED_PED_COMPONENTS(PED_INDEX PedID, INT iPackedDrawable, INT iPackedTexture)
	
	PED_COMPONENT_CONFIG ComponentConfig
	UNPACK_PED_COMPONENT_CONFIG(ComponentConfig, iPackedDrawable, iPackedTexture)
	APPLY_PED_STYLE_RANDOM_COMPONENTS(PedID, ComponentConfig)

ENDPROC



