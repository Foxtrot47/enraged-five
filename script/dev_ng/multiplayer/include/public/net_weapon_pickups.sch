USING "rage_builtins.sch"
USING "globals.sch"
USING "net_include.sch"
//USING "pm_missioncreator_public.sch"
USING "net_spawn_debug.sch"
#IF IS_DEBUG_BUILD
USING "shared_debug.sch"
#ENDIF
USING "fmmc_header.sch"
USING "net_common_functions.sch"


ENUM FM_WEAPON_PICKUP_TYPES
	FM_WPT_OTHER,
	FM_WPT_MELEE,
	FM_WPT_PROJECTILE,
	FM_WPT_GUN
ENDENUM

PROC INIT_FM_WEAPON_PICKUP_TOTAL_VALUES_FROM_TUNABLES()

	CPRINTLN(DEBUG_FMPICKUPS, "INIT_FM_WEAPON_PICKUP_TOTAL_VALUES_FROM_TUNABLES - called")

	// these will be replaced when we get actual tunables
	INT iNumberOfWeaponPickups = g_sMPTunables.iNumberOfWeaponPickups
	INT iPercentageOfGuns = g_sMPTunables.iPercentageOfGuns
	INT iPercentageOfProjectiles = g_sMPTunables.iPercentageOfProjectiles
	INT iPercentageOfMelee = g_sMPTunables.iPercentageOfMelee
	
	// check percentage total
	INT iPercentageTotal
	iPercentageTotal = iPercentageOfGuns + iPercentageOfProjectiles + iPercentageOfMelee
	IF (iPercentageTotal > 100)
		
		CASSERTLN(DEBUG_FMPICKUPS, "INIT_FM_WEAPON_PICKUP_TOTAL_VALUES_FROM_TUNABLES - tunable percentages > 100")	
		
		// we need to scale down the values
		iPercentageOfGuns = FLOOR((TO_FLOAT(iPercentageOfGuns) / TO_FLOAT(iPercentageTotal)) * 100.0)
		iPercentageOfProjectiles = FLOOR((TO_FLOAT(iPercentageOfProjectiles) / TO_FLOAT(iPercentageTotal)) * 100.0)
		iPercentageOfMelee =  FLOOR((TO_FLOAT(iPercentageOfMelee) / TO_FLOAT(iPercentageTotal)) * 100.0)
		
	ENDIF
	
	// work out percentage of Other
	INT iPercentageOfOther 
	iPercentageOfOther = 100 - (iPercentageOfGuns + iPercentageOfProjectiles + iPercentageOfMelee)
	IF (iPercentageOfOther < 0)
		iPercentageOfOther = 0
	ENDIF
	
	FLOAT fTemp
	
	// check total number of weapon pickups
	g_iTotalNumberOfWeaponPickupsToBeCreated = iNumberOfWeaponPickups
	IF (g_iTotalNumberOfWeaponPickupsToBeCreated > FM_MAX_NUMBER_OF_WEAPON_PICKUPS)
		CASSERTLN(DEBUG_FMPICKUPS, "INIT_FM_WEAPON_PICKUP_TOTAL_VALUES_FROM_TUNABLES - g_iTotalNumberOfWeaponPickupsToBeCreated > FM_MAX_NUMBER_OF_WEAPON_PICKUPS - check tunables values")	
		g_iTotalNumberOfWeaponPickupsToBeCreated = FM_MAX_NUMBER_OF_WEAPON_PICKUPS
	ENDIF
	
	// other
	fTemp = TO_FLOAT(g_iTotalNumberOfWeaponPickupsToBeCreated) * (TO_FLOAT(iPercentageOfOther) * 0.01)
	fTemp += 0.01 // to combat rounding errors
	CPRINTLN(DEBUG_FMPICKUPS, "g_iNumberOfWeaponPickupsToBeCreated_Other = ", fTemp)
	g_iNumberOfWeaponPickupsToBeCreated_Other = FLOOR(fTemp)
	IF (g_iNumberOfWeaponPickupsToBeCreated_Other > FM_WEAPON_PICKUPS_MAX_OTHERS)
		CASSERTLN(DEBUG_FMPICKUPS, "INIT_FM_WEAPON_PICKUP_TOTAL_VALUES_FROM_TUNABLES - g_iNumberOfWeaponPickupsToBeCreated_Other > FM_WEAPON_PICKUPS_MAX_OTHERS - check tunables values")
		g_iNumberOfWeaponPickupsToBeCreated_Other = FM_WEAPON_PICKUPS_MAX_OTHERS		
	ENDIF
	
	// melee
	fTemp = TO_FLOAT(g_iTotalNumberOfWeaponPickupsToBeCreated) * (TO_FLOAT(iPercentageOfMelee) * 0.01)
	fTemp += 0.01 // to combat rounding errors
	CPRINTLN(DEBUG_FMPICKUPS, "g_iNumberOfWeaponPickupsToBeCreated_Melee = ", fTemp)
	g_iNumberOfWeaponPickupsToBeCreated_Melee = FLOOR(fTemp)
	IF (g_iNumberOfWeaponPickupsToBeCreated_Melee > FM_WEAPON_PICKUPS_MAX_MELEE)
		CASSERTLN(DEBUG_FMPICKUPS, "INIT_FM_WEAPON_PICKUP_TOTAL_VALUES_FROM_TUNABLES - g_iNumberOfWeaponPickupsToBeCreated_Melee > FM_WEAPON_PICKUPS_MAX_MELEE - check tunables values")
		g_iNumberOfWeaponPickupsToBeCreated_Melee = FM_WEAPON_PICKUPS_MAX_MELEE		
	ENDIF
	
	// projectile
	fTemp = TO_FLOAT(g_iTotalNumberOfWeaponPickupsToBeCreated) * (TO_FLOAT(iPercentageOfProjectiles) * 0.01)
	fTemp += 0.01 // to combat rounding errors
	CPRINTLN(DEBUG_FMPICKUPS, "g_iNumberOfWeaponPickupsToBeCreated_Projectile = ", fTemp)
	g_iNumberOfWeaponPickupsToBeCreated_Projectile = FLOOR(fTemp)
	IF (g_iNumberOfWeaponPickupsToBeCreated_Projectile > FM_WEAPON_PICKUPS_MAX_PROJECTILE)
		CASSERTLN(DEBUG_FMPICKUPS, "INIT_FM_WEAPON_PICKUP_TOTAL_VALUES_FROM_TUNABLES - g_iNumberOfWeaponPickupsToBeCreated_Projectile > FM_WEAPON_PICKUPS_MAX_PROJECTILE - check tunables values")
		g_iNumberOfWeaponPickupsToBeCreated_Projectile = FM_WEAPON_PICKUPS_MAX_PROJECTILE		
	ENDIF
	
	// gun
	fTemp = TO_FLOAT(g_iTotalNumberOfWeaponPickupsToBeCreated) * (TO_FLOAT(iPercentageOfGuns) * 0.01)
	fTemp += 0.01 // to combat rounding errors
	CPRINTLN(DEBUG_FMPICKUPS, "g_iNumberOfWeaponPickupsToBeCreated_Gun = ", fTemp)
	g_iNumberOfWeaponPickupsToBeCreated_Gun = FLOOR(fTemp)
	IF (g_iNumberOfWeaponPickupsToBeCreated_Gun > FM_WEAPON_PICKUPS_MAX_GUN)
		CASSERTLN(DEBUG_FMPICKUPS, "INIT_FM_WEAPON_PICKUP_TOTAL_VALUES_FROM_TUNABLES - g_iNumberOfWeaponPickupsToBeCreated_Gun > FM_WEAPON_PICKUPS_MAX_GUN - check tunables values")
		g_iNumberOfWeaponPickupsToBeCreated_Gun = FM_WEAPON_PICKUPS_MAX_GUN		
	ENDIF	
	
	// check total
	INT iTotal 
	iTotal = g_iNumberOfWeaponPickupsToBeCreated_Other + g_iNumberOfWeaponPickupsToBeCreated_Melee + g_iNumberOfWeaponPickupsToBeCreated_Projectile + g_iNumberOfWeaponPickupsToBeCreated_Gun
	CPRINTLN(DEBUG_FMPICKUPS, "iTotal = ", iTotal)
	CPRINTLN(DEBUG_FMPICKUPS, "g_iTotalNumberOfWeaponPickupsToBeCreated = ", g_iTotalNumberOfWeaponPickupsToBeCreated)
	IF (iTotal > g_iTotalNumberOfWeaponPickupsToBeCreated)
		CASSERTLN(DEBUG_FMPICKUPS, "INIT_FM_WEAPON_PICKUP_TOTAL_VALUES_FROM_TUNABLES - iTotal > g_iTotalNumberOfWeaponPickupsToBeCreated")	
	ENDIF
	

ENDPROC

PROC INIT_FM_WEAPON_PICKUP_SERVER_DATA()
	
	CPRINTLN(DEBUG_FMPICKUPS, "INIT_FM_WEAPON_PICKUP_SPAWN_DATA - called")
	
	INT i
	REPEAT FM_MAX_NUMBER_OF_WEAPON_PICKUPS i
		GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[i] = -1
		GlobalServerBD_FM.WeaponPickupData.iCollector[i] = -1
	ENDREPEAT
	
	GlobalServerBD_FM.WeaponPickupData.iNumberInitiallyCreated = 0

ENDPROC

FUNC INT GET_GLOBAL_ARRAY_INDEX_FROM_PICKUP_ID(PICKUP_INDEX PickupID)
	
	INT i
	
	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_FMPICKUPS, "GET_GLOBAL_ARRAY_INDEX_FROM_PICKUP_ID called with ", NATIVE_TO_INT(PickupID))	
		REPEAT FM_MAX_NUMBER_OF_WEAPON_PICKUPS i
			IF (PickupID = g_FMWeaponPickup[i])
				CPRINTLN(DEBUG_FMPICKUPS, "GET_GLOBAL_ARRAY_INDEX_FROM_PICKUP_ID - g_FMWeaponPickup[", i, "] = ", NATIVE_TO_INT(g_FMWeaponPickup[i]), " ** Matches !! **")
			ELSE
				CPRINTLN(DEBUG_FMPICKUPS, "GET_GLOBAL_ARRAY_INDEX_FROM_PICKUP_ID - g_FMWeaponPickup[", i, "] = ", NATIVE_TO_INT(g_FMWeaponPickup[i]))	
			ENDIF
		ENDREPEAT
	#ENDIF

	
	REPEAT FM_MAX_NUMBER_OF_WEAPON_PICKUPS i
		IF (PickupID = g_FMWeaponPickup[i])
			RETURN(i)
		ENDIF	
	ENDREPEAT
	CASSERTLN(DEBUG_FMPICKUPS, "GET_GLOBAL_ARRAY_INDEX_FROM_PICKUP_ID could not find pickup id in global list")
	RETURN(-1)
ENDFUNC

PROC REMOVE_FM_WEAPON_PICKUP(INT iPickupToRemove)
	IF iPickupToRemove < 0
	OR iPickupToRemove >= COUNT_OF(g_FMWeaponPickup)
		CPRINTLN(DEBUG_FMPICKUPS, "REMOVE_FM_WEAPON_PICKUP [VALIDATE] - invalid value in iPickupToRemove, expected value between 0 and (FM_MAX_NUMBER_OF_WEAPON_PICKUPS-1), actual value: ", iPickupToRemove)
		SCRIPT_ASSERT("REMOVE_FM_WEAPON_PICKUP [VALIDATE] - invalid value in iPickupToRemove")
		EXIT
	ENDIF
	
	CPRINTLN(DEBUG_FMPICKUPS, "REMOVE_FM_WEAPON_PICKUP - ", iPickupToRemove)	
	IF DOES_PICKUP_EXIST(g_FMWeaponPickup[iPickupToRemove])
		REMOVE_PICKUP(g_FMWeaponPickup[iPickupToRemove])
	ENDIF
	g_FMWeaponPickup[iPickupToRemove] = NULL
	IF DOES_BLIP_EXIST(g_FMWeaponPickupBlipID[iPickupToRemove])
		REMOVE_BLIP(g_FMWeaponPickupBlipID[iPickupToRemove])
	ENDIF
	g_FMWeaponPickupBlipID[iPickupToRemove] = NULL
ENDPROC

PROC SET_FM_WEAPON_PICKUP_BITSET(INT iPlayerID,INT iPickup,BOOL bSet)
	INT iBS_ARRAY = iPickup/32
	INT iBS_INDEX = iPickup%32
	#IF IS_DEBUG_BUILD
	IF iBS_ARRAY >= FM_MAX_NUMBER_OF_WEAPON_PICKUPS_BITSETS
		SCRIPT_ASSERT("FM_MAX_NUMBER_OF_WEAPON_PICKUPS_BITSETS needs to be increased!")
		PRINTLN("SET_FM_WEAPON_PICKUP_BITSET: FM_MAX_NUMBER_OF_WEAPON_PICKUPS_BITSETS needs to be increased! to ",iBS_ARRAY)
	ENDIF
	#ENDIF
	IF bSet
		SET_BIT(GlobalplayerBD_FM[iPlayerID].WeaponPickupData.iClaimingPickupBS[iBS_ARRAY],iBS_INDEX)
	ELSE
		CLEAR_BIT(GlobalplayerBD_FM[iPlayerID].WeaponPickupData.iClaimingPickupBS[iBS_ARRAY],iBS_INDEX)	
	ENDIF
ENDPROC

FUNC BOOL IS_FM_WEAPON_PICKUP_BITSET_SET(INT iPlayerID,INT iPickup)
	INT iBS_ARRAY = iPickup/32
	INT iBS_INDEX = iPickup%32
	#IF IS_DEBUG_BUILD
	IF iBS_ARRAY >= FM_MAX_NUMBER_OF_WEAPON_PICKUPS_BITSETS
		SCRIPT_ASSERT("FM_MAX_NUMBER_OF_WEAPON_PICKUPS_BITSETS needs to be increased!")
		PRINTLN("IS_FM_WEAPON_PICKUP_BITSET_SET: FM_MAX_NUMBER_OF_WEAPON_PICKUPS_BITSETS needs to be increased! to ",iBS_ARRAY)
	ENDIF
	#ENDIF
	RETURN IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].WeaponPickupData.iClaimingPickupBS[iBS_ARRAY],iBS_INDEX)
ENDFUNC

PROC REMOVE_MY_CLAIM_FOR_PICKUP(INT iPickup)
	CPRINTLN(DEBUG_FMPICKUPS, "REMOVE_MY_CLAIM_FOR_PICKUP - ", iPickup)	
	IF g_iFMWeaponPickupState[iPickup] = FM_WEAPON_PICKUP_STATE_CLAIM_PENDING
		g_iFMWeaponPickupState[iPickup] = FM_WEAPON_PICKUP_STATE_NULL
	ENDIF
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].WeaponPickupData.bClaimingPickup[iPickup] = FALSE
	SET_FM_WEAPON_PICKUP_BITSET(NATIVE_TO_INT(PLAYER_ID()),iPickup,FALSE)
ENDPROC

PROC CLAIM_FM_WEAPON_PICKUP(INT iPickup)

	CPRINTLN(DEBUG_FMPICKUPS, "Player is staking a claim for pickup ", iPickup)

	// send broadcast event (this is quicker than just using the broadcast data, but the broadcast data is used as backup as events sometimes go AWOL.
	BROADCAST_REQUEST_TO_CLAIM_FM_WEAPON_PICKUP(iPickup)
	
	// also set broadcast data bit stating claim.
	//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].WeaponPickupData.bClaimingPickup[iPickup] = TRUE
	SET_FM_WEAPON_PICKUP_BITSET(NATIVE_TO_INT(PLAYER_ID()),iPickup,TRUE)
	
	// set the local flag to say we're waiting on a claim
	g_iFMWeaponPickupState[iPickup] = FM_WEAPON_PICKUP_STATE_CLAIM_PENDING
ENDPROC

PROC REMOVE_AND_RESET_FM_WEAPON_PICKUP(INT iPickupToRemove)
	CPRINTLN(DEBUG_FMPICKUPS, "REMOVE_AND_RESET_FM_WEAPON_PICKUP - ", iPickupToRemove)	
	REMOVE_FM_WEAPON_PICKUP(iPickupToRemove)
	g_iFMWeaponPickupSpawnLocation[iPickupToRemove] = -1
	REMOVE_MY_CLAIM_FOR_PICKUP(iPickupToRemove)
ENDPROC

PROC REMOVE_AND_RESET_ALL_FM_WEAPON_PICKUPS()
	CPRINTLN(DEBUG_FMPICKUPS, "REMOVE_AND_RESET_ALL_FM_WEAPON_PICKUPS - called")
	DEBUG_PRINTCALLSTACK()
	INT i
	REPEAT FM_MAX_NUMBER_OF_WEAPON_PICKUPS i
		REMOVE_AND_RESET_FM_WEAPON_PICKUP(i)
	ENDREPEAT
ENDPROC

PROC INIT_FM_WEAPON_PICKUP_SPAWN_DATA()

	REMOVE_AND_RESET_ALL_FM_WEAPON_PICKUPS()

	CPRINTLN(DEBUG_FMPICKUPS, "INIT_FM_WEAPON_PICKUP_SPAWN_DATA - called.")

	// -------------- OTHER ------------------------
	
	// petrol cans
	g_PickupSpawnLocations_Other[0].vPos = <<207.21,6630.832,30.7327>>
	g_PickupSpawnLocations_Other[0].ptPickUpType = PICKUP_WEAPON_PETROLCAN
	
	g_PickupSpawnLocations_Other[1].vPos = <<2663.8608,3274.759,54.2405>>
	g_PickupSpawnLocations_Other[1].ptPickUpType = PICKUP_WEAPON_PETROLCAN
	
	g_PickupSpawnLocations_Other[2].vPos = <<-2555.2629,2334.5645,32.078>>
	g_PickupSpawnLocations_Other[2].ptPickUpType = PICKUP_WEAPON_PETROLCAN
	
	g_PickupSpawnLocations_Other[3].vPos = <<-724.005,-937.6594,18.0347>>
	g_PickupSpawnLocations_Other[3].ptPickUpType = PICKUP_WEAPON_PETROLCAN
	
	g_PickupSpawnLocations_Other[4].vPos = <<1737.3549,-1634.2474,111.4911>>
	g_PickupSpawnLocations_Other[4].ptPickUpType = PICKUP_WEAPON_PETROLCAN
	
	g_PickupSpawnLocations_Other[5].vPos = <<237.2382,-2497.1448,5.5711>>
	g_PickupSpawnLocations_Other[5].ptPickUpType = PICKUP_WEAPON_PETROLCAN
	
	g_PickupSpawnLocations_Other[6].vPos = <<-1805.0782,796.3972,137.6858>>
	g_PickupSpawnLocations_Other[6].ptPickUpType = PICKUP_WEAPON_PETROLCAN
	
	g_PickupSpawnLocations_Other[7].vPos = <<592.425,2927.0291,39.9188>>
	g_PickupSpawnLocations_Other[7].ptPickUpType = PICKUP_WEAPON_PETROLCAN
	
	g_PickupSpawnLocations_Other[8].vPos = <<1693.5399,4920.416,41.0781>>
	g_PickupSpawnLocations_Other[8].ptPickUpType = PICKUP_WEAPON_PETROLCAN
	
	g_PickupSpawnLocations_Other[9].vPos = <<2580.8818,361.7214,107.4688>>
	g_PickupSpawnLocations_Other[9].ptPickUpType = PICKUP_WEAPON_PETROLCAN
	

	// -------------- MELEE ------------------------
	
	// knife
	g_PickupSpawnLocations_Melee[0].vPos = <<1439.5936,6331.7124,22.9575>>
	g_PickupSpawnLocations_Melee[0].ptPickUpType = PICKUP_WEAPON_KNIFE
	
	g_PickupSpawnLocations_Melee[1].vPos = <<1531.4744,3590.7239,37.7715>>
	g_PickupSpawnLocations_Melee[1].ptPickUpType = PICKUP_WEAPON_KNIFE
	
	g_PickupSpawnLocations_Melee[2].vPos = <<-60.3809,-1218.3936,27.7019>>
	g_PickupSpawnLocations_Melee[2].ptPickUpType = PICKUP_WEAPON_KNIFE
	
	g_PickupSpawnLocations_Melee[3].vPos = <<-1633.1589,-1053.6359,3.8816>>
	g_PickupSpawnLocations_Melee[3].ptPickUpType = PICKUP_WEAPON_KNIFE
	
	g_PickupSpawnLocations_Melee[4].vPos = <<975.5866,-1810.923,30.4854>>
	g_PickupSpawnLocations_Melee[4].ptPickUpType = PICKUP_WEAPON_KNIFE
	
	// nightstick
	g_PickupSpawnLocations_Melee[5].vPos = <<850.1597,-1317.4041,25.4525>>
	g_PickupSpawnLocations_Melee[5].ptPickUpType = PICKUP_WEAPON_NIGHTSTICK
	
	g_PickupSpawnLocations_Melee[6].vPos = <<-442.715,5996.535,30.4901>>
	g_PickupSpawnLocations_Melee[6].ptPickUpType = PICKUP_WEAPON_NIGHTSTICK
	
	g_PickupSpawnLocations_Melee[7].vPos = <<-1104.75,-831.6023,36.6754>>
	g_PickupSpawnLocations_Melee[7].ptPickUpType = PICKUP_WEAPON_NIGHTSTICK
	
	g_PickupSpawnLocations_Melee[8].vPos = <<1838.4863,3691.7878,33.267>>
	g_PickupSpawnLocations_Melee[8].ptPickUpType = PICKUP_WEAPON_NIGHTSTICK
	
	g_PickupSpawnLocations_Melee[9].vPos = <<1831.2114,2602.1096,44.6046>>
	g_PickupSpawnLocations_Melee[9].ptPickUpType = PICKUP_WEAPON_NIGHTSTICK
	
	// hammer
	g_PickupSpawnLocations_Melee[10].vPos = <<-813.6743,-798.7653,18.5765>>
	g_PickupSpawnLocations_Melee[10].ptPickUpType = PICKUP_WEAPON_HAMMER
	
	g_PickupSpawnLocations_Melee[11].vPos = <<-338.8805,6295.0732,34.2573>>
	g_PickupSpawnLocations_Melee[11].ptPickUpType = PICKUP_WEAPON_HAMMER
	
	g_PickupSpawnLocations_Melee[12].vPos = <<2675.5161,3500.4485,52.3>>
	g_PickupSpawnLocations_Melee[12].ptPickUpType = PICKUP_WEAPON_HAMMER
	
	g_PickupSpawnLocations_Melee[13].vPos = <<-82.1495,935.0021,232.0286>>
	g_PickupSpawnLocations_Melee[13].ptPickUpType = PICKUP_WEAPON_HAMMER
	
	g_PickupSpawnLocations_Melee[14].vPos = <<120.4866,-3182.4385,4.9906>>
	g_PickupSpawnLocations_Melee[14].ptPickUpType = PICKUP_WEAPON_HAMMER

	// baseball bat
	g_PickupSpawnLocations_Melee[15].vPos = <<-317.6021,-1647.4132,30.8532>>
	g_PickupSpawnLocations_Melee[15].ptPickUpType = PICKUP_WEAPON_BAT
	
	g_PickupSpawnLocations_Melee[16].vPos = <<-359.91019,-1866.58777,19.52816>>
	g_PickupSpawnLocations_Melee[16].ptPickUpType = PICKUP_WEAPON_BAT
	
	g_PickupSpawnLocations_Melee[17].vPos = <<-1755.5251,185.8265,63.4436>>
	g_PickupSpawnLocations_Melee[17].ptPickUpType = PICKUP_WEAPON_BAT
	
	g_PickupSpawnLocations_Melee[18].vPos = <<1736.1443,6419.3442,34.0372>>
	g_PickupSpawnLocations_Melee[18].ptPickUpType = PICKUP_WEAPON_BAT
	
	g_PickupSpawnLocations_Melee[19].vPos = <<245.6425,3166.3223,41.8399>>
	g_PickupSpawnLocations_Melee[19].ptPickUpType = PICKUP_WEAPON_BAT
	
	// crowbar
	g_PickupSpawnLocations_Melee[20].vPos = <<-553.0112,5325.9663,72.5996>>
	g_PickupSpawnLocations_Melee[20].ptPickUpType = PICKUP_WEAPON_CROWBAR
	
	g_PickupSpawnLocations_Melee[21].vPos = <<-1810.52,3104.2776,31.8418>>
	g_PickupSpawnLocations_Melee[21].ptPickUpType = PICKUP_WEAPON_CROWBAR
	
	g_PickupSpawnLocations_Melee[22].vPos = <<-2947.5195,438.6648,14.2617>>
	g_PickupSpawnLocations_Melee[22].ptPickUpType = PICKUP_WEAPON_CROWBAR
	
	g_PickupSpawnLocations_Melee[23].vPos = <<1346.2872,4390.3511,43.3438>>
	g_PickupSpawnLocations_Melee[23].ptPickUpType = PICKUP_WEAPON_CROWBAR
	
	g_PickupSpawnLocations_Melee[24].vPos = <<1238.7391,-2969.334,8.3193>>
	g_PickupSpawnLocations_Melee[24].ptPickUpType = PICKUP_WEAPON_CROWBAR
	
	// golf club
	g_PickupSpawnLocations_Melee[25].vPos = <<-1602.416,3.309,59.9076>>
	g_PickupSpawnLocations_Melee[25].ptPickUpType = PICKUP_WEAPON_GOLFCLUB
	
	g_PickupSpawnLocations_Melee[26].vPos = <<-1341.0416,48.6275,54.2456>>
	g_PickupSpawnLocations_Melee[26].ptPickUpType = PICKUP_WEAPON_GOLFCLUB
	
	g_PickupSpawnLocations_Melee[27].vPos = <<1330.4628,-608.6297,73.5131>>
	g_PickupSpawnLocations_Melee[27].ptPickUpType = PICKUP_WEAPON_GOLFCLUB
	
	g_PickupSpawnLocations_Melee[28].vPos = <<-1029.9465,-2737.7361,19.1693>>
	g_PickupSpawnLocations_Melee[28].ptPickUpType = PICKUP_WEAPON_GOLFCLUB
	
	g_PickupSpawnLocations_Melee[29].vPos = <<-2167.0891,5197.0737,15.8854>>
	g_PickupSpawnLocations_Melee[29].ptPickUpType = PICKUP_WEAPON_GOLFCLUB
	
	// broken bottle
	g_PickupSpawnLocations_Melee[30].vPos = <<2526.4631,2583.6506,36.9449>>
	g_PickupSpawnLocations_Melee[30].ptPickUpType = PICKUP_WEAPON_DLC_BOTTLE
	
	g_PickupSpawnLocations_Melee[31].vPos = <<1073.3335,-260.834,58.0789>>
	g_PickupSpawnLocations_Melee[31].ptPickUpType = PICKUP_WEAPON_DLC_BOTTLE
	
	g_PickupSpawnLocations_Melee[32].vPos = <<63.1339,3671.7244,38.8075>>
	g_PickupSpawnLocations_Melee[32].ptPickUpType = PICKUP_WEAPON_DLC_BOTTLE
	
	g_PickupSpawnLocations_Melee[33].vPos = <<-131.3041,6378.7451,31.18>>
	g_PickupSpawnLocations_Melee[33].ptPickUpType = PICKUP_WEAPON_DLC_BOTTLE
	
	g_PickupSpawnLocations_Melee[34].vPos = <<2220.467,5611.5112,53.6791>>
	g_PickupSpawnLocations_Melee[34].ptPickUpType = PICKUP_WEAPON_DLC_BOTTLE
	
	// cavalry dagger
	g_PickupSpawnLocations_Melee[35].vPos = <<2955.1389, 3134.4436, 170.5797>>
	g_PickupSpawnLocations_Melee[35].ptPickUpType = PICKUP_WEAPON_DLC_DAGGER
	
	g_PickupSpawnLocations_Melee[36].vPos = <<-572.4406,-613.0344,29.4478>>
	g_PickupSpawnLocations_Melee[36].ptPickUpType = PICKUP_WEAPON_DLC_DAGGER
	
	g_PickupSpawnLocations_Melee[37].vPos = <<-446.4161,2013.0691,122.5453>>
	g_PickupSpawnLocations_Melee[37].ptPickUpType = PICKUP_WEAPON_DLC_DAGGER

	// ----------------  PROJECTILES -----------------------------------------

	// grenade
	g_PickupSpawnLocations_Projectile[0].vPos = <<-2055.09,3238.8672,30.4989>>
	g_PickupSpawnLocations_Projectile[0].ptPickUpType = PICKUP_WEAPON_GRENADE
	
	g_PickupSpawnLocations_Projectile[1].vPos = <<-1937.8043,2051.439,139.8329>>
	g_PickupSpawnLocations_Projectile[1].ptPickUpType = PICKUP_WEAPON_GRENADE
	
	g_PickupSpawnLocations_Projectile[2].vPos = <<713.5492,4093.6765,33.7329>>
	g_PickupSpawnLocations_Projectile[2].ptPickUpType = PICKUP_WEAPON_GRENADE
	
	g_PickupSpawnLocations_Projectile[3].vPos = <<1525.5714,1710.3022,109.007>>
	g_PickupSpawnLocations_Projectile[3].ptPickUpType = PICKUP_WEAPON_GRENADE
	
	g_PickupSpawnLocations_Projectile[4].vPos = <<2815.865,-668.2856,0.1915>>
	g_PickupSpawnLocations_Projectile[4].ptPickUpType = PICKUP_WEAPON_GRENADE
	
	g_PickupSpawnLocations_Projectile[5].vPos = <<1024.3884,134.75,89.2405>>
	g_PickupSpawnLocations_Projectile[5].ptPickUpType = PICKUP_WEAPON_GRENADE
	
	g_PickupSpawnLocations_Projectile[6].vPos = <<-444.1653,-892.3921,46.9889>>
	g_PickupSpawnLocations_Projectile[6].ptPickUpType = PICKUP_WEAPON_GRENADE
	
	g_PickupSpawnLocations_Projectile[7].vPos = <<1255.2072,-1571.801,77.9696>>
	g_PickupSpawnLocations_Projectile[7].ptPickUpType = PICKUP_WEAPON_GRENADE
	
	g_PickupSpawnLocations_Projectile[8].vPos = <<-1.6907,-1822.6652,28.5482>>
	g_PickupSpawnLocations_Projectile[8].ptPickUpType = PICKUP_WEAPON_GRENADE
	
	g_PickupSpawnLocations_Projectile[9].vPos = <<-1065.1025,-1163.2686,1.1586>>
	g_PickupSpawnLocations_Projectile[9].ptPickUpType = PICKUP_WEAPON_GRENADE
	
	g_PickupSpawnLocations_Projectile[10].vPos = <<-1002.3256,729.7162,163.0705>> 
	g_PickupSpawnLocations_Projectile[10].ptPickUpType = PICKUP_WEAPON_GRENADE
	
	g_PickupSpawnLocations_Projectile[11].vPos = <<-102.7766,2806.6714,52.0369>> 
	g_PickupSpawnLocations_Projectile[11].ptPickUpType = PICKUP_WEAPON_GRENADE
	
	g_PickupSpawnLocations_Projectile[12].vPos = <<2491.1414,4966.5854,43.6179>> 
	g_PickupSpawnLocations_Projectile[12].ptPickUpType = PICKUP_WEAPON_GRENADE
	
	g_PickupSpawnLocations_Projectile[13].vPos = <<1362.7847,6549.6685,13.5391>> 
	g_PickupSpawnLocations_Projectile[13].ptPickUpType = PICKUP_WEAPON_GRENADE
	
	g_PickupSpawnLocations_Projectile[14].vPos = <<-838.8696,4186.1382,214.295>> 
	g_PickupSpawnLocations_Projectile[14].ptPickUpType = PICKUP_WEAPON_GRENADE
	
	// molotov
	g_PickupSpawnLocations_Projectile[15].vPos = <<-1101.0573,2723.0371,17.8004>>
	g_PickupSpawnLocations_Projectile[15].ptPickUpType = PICKUP_WEAPON_MOLOTOV
	
	g_PickupSpawnLocations_Projectile[16].vPos = <<1598.0739,3586.7898,37.7715>>
	g_PickupSpawnLocations_Projectile[16].ptPickUpType = PICKUP_WEAPON_MOLOTOV
	
	g_PickupSpawnLocations_Projectile[17].vPos = <<88.6023,3743.1826,39.7783>>
	g_PickupSpawnLocations_Projectile[17].ptPickUpType = PICKUP_WEAPON_MOLOTOV
	
	g_PickupSpawnLocations_Projectile[18].vPos = <<2318.1799,2551.3799,46.6955>>
	g_PickupSpawnLocations_Projectile[18].ptPickUpType = PICKUP_WEAPON_MOLOTOV
	
	g_PickupSpawnLocations_Projectile[19].vPos = <<-1598.3097,5188.9946,3.3151>>
	g_PickupSpawnLocations_Projectile[19].ptPickUpType = PICKUP_WEAPON_MOLOTOV
	
	g_PickupSpawnLocations_Projectile[20].vPos = <<1321.4521,4307.7173,37.0253>>
	g_PickupSpawnLocations_Projectile[20].ptPickUpType = PICKUP_WEAPON_MOLOTOV
	
	g_PickupSpawnLocations_Projectile[21].vPos = <<3613.571,5025.4907,10.3525>>
	g_PickupSpawnLocations_Projectile[21].ptPickUpType = PICKUP_WEAPON_MOLOTOV
	
	g_PickupSpawnLocations_Projectile[22].vPos = <<256.3964,-1109.3451,28.7002>>
	g_PickupSpawnLocations_Projectile[22].ptPickUpType = PICKUP_WEAPON_MOLOTOV
	
	g_PickupSpawnLocations_Projectile[23].vPos = <<1419.4525,-2604.1194,46.9843>>
	g_PickupSpawnLocations_Projectile[23].ptPickUpType = PICKUP_WEAPON_MOLOTOV
	
	g_PickupSpawnLocations_Projectile[24].vPos = <<1330.6736,-1660.2426,50.2364>>
	g_PickupSpawnLocations_Projectile[24].ptPickUpType = PICKUP_WEAPON_MOLOTOV
	
	g_PickupSpawnLocations_Projectile[25].vPos = <<-3262.5361,960.1833,7.3522>>
	g_PickupSpawnLocations_Projectile[25].ptPickUpType = PICKUP_WEAPON_MOLOTOV
	
	g_PickupSpawnLocations_Projectile[26].vPos = <<658.4307,1278.8306,359.296>>
	g_PickupSpawnLocations_Projectile[26].ptPickUpType = PICKUP_WEAPON_MOLOTOV
	
	g_PickupSpawnLocations_Projectile[27].vPos = <<664.42,551.9471,128.4458>>
	g_PickupSpawnLocations_Projectile[27].ptPickUpType = PICKUP_WEAPON_MOLOTOV	
	
	g_PickupSpawnLocations_Projectile[28].vPos = <<147.3479,6865.8984,27.3152>>
	g_PickupSpawnLocations_Projectile[28].ptPickUpType = PICKUP_WEAPON_MOLOTOV
	
	g_PickupSpawnLocations_Projectile[29].vPos = <<-174.7206,297.1453,92.775>>
	g_PickupSpawnLocations_Projectile[29].ptPickUpType = PICKUP_WEAPON_MOLOTOV	

	// sticky bomb
	g_PickupSpawnLocations_Projectile[30].vPos = <<-2141.6,3312.4211,37.7325>>
	g_PickupSpawnLocations_Projectile[30].ptPickUpType = PICKUP_WEAPON_STICKYBOMB	
	
	g_PickupSpawnLocations_Projectile[31].vPos = <<734.3641,2592.9387,72.8021>>
	g_PickupSpawnLocations_Projectile[31].ptPickUpType = PICKUP_WEAPON_STICKYBOMB
	
	g_PickupSpawnLocations_Projectile[32].vPos = <<1662.0681,-26.2036,172.7748>>
	g_PickupSpawnLocations_Projectile[32].ptPickUpType = PICKUP_WEAPON_STICKYBOMB
	
	g_PickupSpawnLocations_Projectile[33].vPos = <<-2295.8901,198.9769,166.6>>
	g_PickupSpawnLocations_Projectile[33].ptPickUpType = PICKUP_WEAPON_STICKYBOMB
	
	g_PickupSpawnLocations_Projectile[34].vPos = <<2945.4866,2746.6614,42.3869>>
	g_PickupSpawnLocations_Projectile[34].ptPickUpType = PICKUP_WEAPON_STICKYBOMB
	
	g_PickupSpawnLocations_Projectile[35].vPos = <<1559.9653,-2165.7207,76.4832>>
	g_PickupSpawnLocations_Projectile[35].ptPickUpType = PICKUP_WEAPON_STICKYBOMB
	
	g_PickupSpawnLocations_Projectile[36].vPos = <<1297.4019,-3349.2051,4.9016>>
	g_PickupSpawnLocations_Projectile[36].ptPickUpType = PICKUP_WEAPON_STICKYBOMB
	
	g_PickupSpawnLocations_Projectile[37].vPos = <<-153.1925,-1099.0458,12.117>>
	g_PickupSpawnLocations_Projectile[37].ptPickUpType = PICKUP_WEAPON_STICKYBOMB
	
	g_PickupSpawnLocations_Projectile[38].vPos = <<-1911.8456,-3032.7227,22.5878>>
	g_PickupSpawnLocations_Projectile[38].ptPickUpType = PICKUP_WEAPON_STICKYBOMB
	
	g_PickupSpawnLocations_Projectile[39].vPos = <<-465.1981,-2273.8735,7.5208>>
	g_PickupSpawnLocations_Projectile[39].ptPickUpType = PICKUP_WEAPON_STICKYBOMB
	
	g_PickupSpawnLocations_Projectile[40].vPos = <<85.2738,-436.2608,35.0055>>
	g_PickupSpawnLocations_Projectile[40].ptPickUpType = PICKUP_WEAPON_STICKYBOMB
	
	g_PickupSpawnLocations_Projectile[41].vPos = <<-263.0924,4729.4927,137.3357>>
	g_PickupSpawnLocations_Projectile[41].ptPickUpType = PICKUP_WEAPON_STICKYBOMB
	
	g_PickupSpawnLocations_Projectile[42].vPos = <<2809.9783,5985.1953,349.6869>>
	g_PickupSpawnLocations_Projectile[42].ptPickUpType = PICKUP_WEAPON_STICKYBOMB
	
	g_PickupSpawnLocations_Projectile[43].vPos = <<-1548.1443,-272.6125,45.7184>>
	g_PickupSpawnLocations_Projectile[43].ptPickUpType = PICKUP_WEAPON_STICKYBOMB
	
	g_PickupSpawnLocations_Projectile[44].vPos = <<-438.7823,1597.3129,357.4732>>
	g_PickupSpawnLocations_Projectile[44].ptPickUpType = PICKUP_WEAPON_STICKYBOMB

	// tear gas
	g_PickupSpawnLocations_Projectile[45].vPos = <<433.8869,-994.8528,24.7952>>
	g_PickupSpawnLocations_Projectile[45].ptPickUpType = PICKUP_WEAPON_SMOKEGRENADE
	
	g_PickupSpawnLocations_Projectile[46].vPos = <<870.1877,-1347.6156,25.3143>>
	g_PickupSpawnLocations_Projectile[46].ptPickUpType = PICKUP_WEAPON_SMOKEGRENADE
	
	g_PickupSpawnLocations_Projectile[47].vPos = <<402.9637,-1626.4093,28.2919>>
	g_PickupSpawnLocations_Projectile[47].ptPickUpType = PICKUP_WEAPON_SMOKEGRENADE
	
	g_PickupSpawnLocations_Projectile[48].vPos = <<-964.0244,-2798.8674,13.257>>
	g_PickupSpawnLocations_Projectile[48].ptPickUpType = PICKUP_WEAPON_SMOKEGRENADE
	
	g_PickupSpawnLocations_Projectile[49].vPos = <<-1029.41,-843.2015,9.8502>>
	g_PickupSpawnLocations_Projectile[49].ptPickUpType = PICKUP_WEAPON_SMOKEGRENADE
	
	g_PickupSpawnLocations_Projectile[50].vPos = <<-319.5805,6085.437,30.4459>>
	g_PickupSpawnLocations_Projectile[50].ptPickUpType = PICKUP_WEAPON_SMOKEGRENADE
	
	g_PickupSpawnLocations_Projectile[51].vPos = <<3426.0957,3761.8767,29.6426>>
	g_PickupSpawnLocations_Projectile[51].ptPickUpType = PICKUP_WEAPON_SMOKEGRENADE
	
	g_PickupSpawnLocations_Projectile[52].vPos = <<2661.0298,1642.7609,23.874>>
	g_PickupSpawnLocations_Projectile[52].ptPickUpType = PICKUP_WEAPON_SMOKEGRENADE
	
	g_PickupSpawnLocations_Projectile[53].vPos = <<9.0143,-2531.992,5.1515>>
	g_PickupSpawnLocations_Projectile[53].ptPickUpType = PICKUP_WEAPON_SMOKEGRENADE
	
	g_PickupSpawnLocations_Projectile[54].vPos = <<639.5297,11.7998,81.7956>>
	g_PickupSpawnLocations_Projectile[54].ptPickUpType = PICKUP_WEAPON_SMOKEGRENADE
	
	g_PickupSpawnLocations_Projectile[55].vPos = <<-1706.16,187.5831,62.926>>
	g_PickupSpawnLocations_Projectile[55].ptPickUpType = PICKUP_WEAPON_SMOKEGRENADE
	
	g_PickupSpawnLocations_Projectile[56].vPos = <<-563.0706,-134.6288,37.0794>>
	g_PickupSpawnLocations_Projectile[56].ptPickUpType = PICKUP_WEAPON_SMOKEGRENADE
	
	g_PickupSpawnLocations_Projectile[57].vPos = <<831.0158,-2195.54,29.2622>>
	g_PickupSpawnLocations_Projectile[57].ptPickUpType = PICKUP_WEAPON_SMOKEGRENADE
	
	g_PickupSpawnLocations_Projectile[58].vPos = <<485.1099,-3112.01,5.2944>>
	g_PickupSpawnLocations_Projectile[58].ptPickUpType = PICKUP_WEAPON_SMOKEGRENADE
	
	g_PickupSpawnLocations_Projectile[59].vPos = <<-1327.5778,-1520.6383,3.4322>>
	g_PickupSpawnLocations_Projectile[59].ptPickUpType = PICKUP_WEAPON_SMOKEGRENADE

	// proximity mines
	g_PickupSpawnLocations_Projectile[60].vPos = <<574.8237,-3126.324,17.7686>>
	g_PickupSpawnLocations_Projectile[60].ptPickUpType = PICKUP_WEAPON_DLC_PROXMINE
	
	g_PickupSpawnLocations_Projectile[61].vPos = <<-2349.4,3267.0493,31.8107>>
	g_PickupSpawnLocations_Projectile[61].ptPickUpType = PICKUP_WEAPON_DLC_PROXMINE

	// ----------------------------- GUNS ---------------------------------------------
	
	// combat pistol
	g_PickupSpawnLocations_Gun[0].vPos = <<-1592.5,2796.7466,15.9301>>
	g_PickupSpawnLocations_Gun[0].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[1].vPos = <<-2646.4453,1874.0111,159.134>>
	g_PickupSpawnLocations_Gun[1].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[2].vPos = <<1665.3607,1.32,165.1181>>
	g_PickupSpawnLocations_Gun[2].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[3].vPos = <<-1504.4316,-39.491,53.5484>>
	g_PickupSpawnLocations_Gun[3].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[4].vPos = <<-1086.262,-2403.7236,12.9452>>
	g_PickupSpawnLocations_Gun[4].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[5].vPos = <<4.0018,-1215.0494,25.708>>	
	g_PickupSpawnLocations_Gun[5].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[6].vPos = <<726.014,-749.1634,24.6943>>
	g_PickupSpawnLocations_Gun[6].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[7].vPos = <<785.92,2165.6587,52.0979>>
	g_PickupSpawnLocations_Gun[7].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[8].vPos = <<1717.8431,4680.0562,42.6558>>
	g_PickupSpawnLocations_Gun[8].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[9].vPos = <<-366.4723,6336.6079,28.8537>>
	g_PickupSpawnLocations_Gun[9].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[10].vPos = <<-1230.7507,-922.8118,1.1502>> 
	g_PickupSpawnLocations_Gun[10].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[11].vPos = <<789.6778,-3167.8694,4.9941>> 
	g_PickupSpawnLocations_Gun[11].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[12].vPos = <<261.5213,204.2065,109.2873>> 
	g_PickupSpawnLocations_Gun[12].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[13].vPos = <<-1051.675,-477.2152,35.9871>>
	g_PickupSpawnLocations_Gun[13].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[14].vPos = <<2577.3792,-295.4593,92.0782>> 
	g_PickupSpawnLocations_Gun[14].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[15].vPos = <<1271.5552,-1706.6456,53.655>> 
	g_PickupSpawnLocations_Gun[15].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[16].vPos = <<259.4435,-1357.1798,29.5518>> 
	g_PickupSpawnLocations_Gun[16].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	g_PickupSpawnLocations_Gun[17].vPos = <<-783.2446,187.8259,71.8353>> 
	g_PickupSpawnLocations_Gun[17].ptPickUpType = PICKUP_WEAPON_COMBATPISTOL
	
	// micro smg
	g_PickupSpawnLocations_Gun[18].vPos = <<1532.3042,3796.3684,32.5178>>
	g_PickupSpawnLocations_Gun[18].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[19].vPos = <<-387.8509,1134.9889,321.6291>>
	g_PickupSpawnLocations_Gun[19].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[20].vPos = <<3313.1255,5177.8506,18.6196>>
	g_PickupSpawnLocations_Gun[20].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[21].vPos = <<156.7099,3130.2332,42.5891>>
	g_PickupSpawnLocations_Gun[21].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[22].vPos = <<-311.3634,-1199.7723,23.7682>>
	g_PickupSpawnLocations_Gun[22].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[23].vPos = <<304.044,-3250.7358,4.8007>>
	g_PickupSpawnLocations_Gun[23].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[24].vPos = <<115.3294,-1973.7297,19.9264>>
	g_PickupSpawnLocations_Gun[24].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[25].vPos = <<-1072.8646,-1675.74,3.5128>>
	g_PickupSpawnLocations_Gun[25].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[26].vPos = <<-926.1714,-2939.1809,12.9451>>
	g_PickupSpawnLocations_Gun[26].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[27].vPos = <<-105.4509,0.7935,77.4424>>
	g_PickupSpawnLocations_Gun[27].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[28].vPos = <<-1068.8027,4893.8994,213.2765>>
	g_PickupSpawnLocations_Gun[28].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[29].vPos = <<2289.7578,1719.2854,67.0406>>
	g_PickupSpawnLocations_Gun[29].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[30].vPos = <<455.9319,5572.0142,780.1841>>
	g_PickupSpawnLocations_Gun[30].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[31].vPos = <<753.1926,6463.0679,30.0616>>
	g_PickupSpawnLocations_Gun[31].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[32].vPos = <<1902.835,613.0169,189.4283>>
	g_PickupSpawnLocations_Gun[32].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[33].vPos = <<1861.5521,-1105.0264,83.2897>>
	g_PickupSpawnLocations_Gun[33].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[34].vPos = <<-1513.4462,1524.1227,110.6261>>
	g_PickupSpawnLocations_Gun[34].ptPickUpType = PICKUP_WEAPON_MICROSMG
	
	g_PickupSpawnLocations_Gun[35].vPos = <<-1616.8297,762.6611,188.2431>>
	g_PickupSpawnLocations_Gun[35].ptPickUpType = PICKUP_WEAPON_MICROSMG

	// assault rifle
	g_PickupSpawnLocations_Gun[36].vPos = <<-1147.0228,4939.1704,221.2736>>
	g_PickupSpawnLocations_Gun[36].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[37].vPos = <<-2788.4299,1418.0519,99.907>>
	g_PickupSpawnLocations_Gun[37].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[38].vPos = <<1406.9309,1138.0573,113.5431>>
	g_PickupSpawnLocations_Gun[38].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[39].vPos = <<-263.2222,2196.0779,129.4037>>
	g_PickupSpawnLocations_Gun[39].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[40].vPos = <<-1757.3544,427.6137,126.685>>
	g_PickupSpawnLocations_Gun[40].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[41].vPos = <<1942.2854,4657.1523,39.5475>>
	g_PickupSpawnLocations_Gun[41].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[42].vPos = <<1133.0952,-2263.3994,30.9256>>
	g_PickupSpawnLocations_Gun[42].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[43].vPos = <<2852.4331,-1347.0917,14.9853>>
	g_PickupSpawnLocations_Gun[43].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[44].vPos = <<-282.8943,-2780.49,3.372>>
	g_PickupSpawnLocations_Gun[44].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[45].vPos = <<2339.2483,3124.6101,47.2087>>
	g_PickupSpawnLocations_Gun[45].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[46].vPos = <<902.6002,3565.8726,32.7947>>
	g_PickupSpawnLocations_Gun[46].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[47].vPos = <<3825.3328,4439.7261,1.8011>>
	g_PickupSpawnLocations_Gun[47].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[48].vPos = <<-569.1706,-1632.2709,18.4124>>
	g_PickupSpawnLocations_Gun[48].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[49].vPos = <<-13.0164,6668.3599,30.9201>>
	g_PickupSpawnLocations_Gun[49].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[50].vPos = <<-35.289,820.8724,230.3325>>
	g_PickupSpawnLocations_Gun[50].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[51].vPos = <<73.9799,-877.1083,29.4405>>
	g_PickupSpawnLocations_Gun[51].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[52].vPos = <<983.5856,-102.6713,73.8538>>
	g_PickupSpawnLocations_Gun[52].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	g_PickupSpawnLocations_Gun[53].vPos = <<3063.9592,2219.7651,2.0363>>
	g_PickupSpawnLocations_Gun[53].ptPickUpType = PICKUP_WEAPON_ASSAULTRIFLE
	
	// pump shotgun
	g_PickupSpawnLocations_Gun[54].vPos = <<2943.0244,4625.2295,47.7259>>
	g_PickupSpawnLocations_Gun[54].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[55].vPos = <<731.0479,2530.4236,72.2271>>
	g_PickupSpawnLocations_Gun[55].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[56].vPos = <<56.8977,3690.6294,38.9213>>
	g_PickupSpawnLocations_Gun[56].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[57].vPos = <<1486.6232,1131.2119,113.3367>>
	g_PickupSpawnLocations_Gun[57].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[58].vPos = <<2354.291,2610.5532,45.6676>>
	g_PickupSpawnLocations_Gun[58].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[59].vPos = <<-678.2736,5792.8359,16.331>>
	g_PickupSpawnLocations_Gun[59].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[60].vPos = <<-2868.6614,-12.333,10.6082>>
	g_PickupSpawnLocations_Gun[60].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[61].vPos = <<161.8364,-561.1632,20.9956>>
	g_PickupSpawnLocations_Gun[61].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[62].vPos = <<1280.3527,304.2236,80.9909>>
	g_PickupSpawnLocations_Gun[62].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[63].vPos = <<-60.6782,-1440.5056,31.1225>>
	g_PickupSpawnLocations_Gun[63].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[64].vPos = <<1581.3345,2910.53,55.9464>>
	g_PickupSpawnLocations_Gun[64].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[65].vPos = <<-2172.4741,4293.4375,48.0238>>
	g_PickupSpawnLocations_Gun[65].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[66].vPos = <<742.8358,4170.1504,40.0929>>
	g_PickupSpawnLocations_Gun[66].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[67].vPos = <<1591.9875,6581.9863,12.9679>>
	g_PickupSpawnLocations_Gun[67].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[68].vPos = <<-1890.4219,2073.8352,139.9977>>
	g_PickupSpawnLocations_Gun[68].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[69].vPos = <<944.5846,-678.1459,57.4548>>
	g_PickupSpawnLocations_Gun[69].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[70].vPos = <<-666.053,-2003.2303,6.5173>>
	g_PickupSpawnLocations_Gun[70].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	g_PickupSpawnLocations_Gun[71].vPos = <<2727.2925,4143.9268,43.2929>>
	g_PickupSpawnLocations_Gun[71].ptPickUpType = PICKUP_WEAPON_PUMPSHOTGUN
	
	// sniper rifle
	g_PickupSpawnLocations_Gun[72].vPos = <<1989.8857,5026.9463,60.6082>>
	g_PickupSpawnLocations_Gun[72].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[73].vPos = <<2729.0454,1577.5116,65.5431>>
	g_PickupSpawnLocations_Gun[73].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[74].vPos = <<-2507.8,3299.0608,90.969>>
	g_PickupSpawnLocations_Gun[74].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[75].vPos = <<1815.5082,3906.4258,36.2175>>
	g_PickupSpawnLocations_Gun[75].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[76].vPos = <<424.9106,5614.0283,765.529>>
	g_PickupSpawnLocations_Gun[76].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[77].vPos = <<1004.3945,-2880.2158,38.1619>>
	g_PickupSpawnLocations_Gun[77].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[78].vPos = <<352.8205,170.5852,126.7566>>
	g_PickupSpawnLocations_Gun[78].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[79].vPos = <<-1272.3586,-2444.9873,72.0491>>
	g_PickupSpawnLocations_Gun[79].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[80].vPos = <<-1175.8099,-472.4921,59.1132>>
	g_PickupSpawnLocations_Gun[80].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[81].vPos = <<1611.9698,-2245.3513,131.7942>>
	g_PickupSpawnLocations_Gun[81].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[82].vPos = <<763.5873,1185.7682,348.0852>>
	g_PickupSpawnLocations_Gun[82].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[83].vPos = <<-275.6897,-636.3749,47.4426>>
	g_PickupSpawnLocations_Gun[83].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[84].vPos = <<-2041.7185,-373.5576,47.1062>>
	g_PickupSpawnLocations_Gun[84].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[85].vPos = <<-521.8686,4196.3467,192.736>>
	g_PickupSpawnLocations_Gun[85].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[86].vPos = <<529.0732,-2358.5586,48.9981>>
	g_PickupSpawnLocations_Gun[86].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[87].vPos = <<172.0419,2220.2917,89.7872>>
	g_PickupSpawnLocations_Gun[87].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[88].vPos = <<25.5916,7644.835,17.9513>>
	g_PickupSpawnLocations_Gun[88].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	
	g_PickupSpawnLocations_Gun[89].vPos = <<70.5065, 1224.1641, 272.0053>>
	g_PickupSpawnLocations_Gun[89].ptPickUpType = PICKUP_WEAPON_SNIPERRIFLE
	

	INIT_FM_WEAPON_PICKUP_TOTAL_VALUES_FROM_TUNABLES()
	
	INIT_FM_WEAPON_PICKUP_SERVER_DATA()
	
ENDPROC


FUNC PICKUP_TYPE GET_PICKUP_TYPE_FOR_SPAWN_LOCATION(FM_WEAPON_PICKUP_TYPES eType, INT iSpawnLocation)
	SWITCH eType
		CASE FM_WPT_OTHER
			RETURN g_PickupSpawnLocations_Other[iSpawnLocation].ptPickUpType
		BREAK
		CASE FM_WPT_MELEE
			RETURN g_PickupSpawnLocations_Melee[iSpawnLocation].ptPickUpType
		BREAK
		CASE FM_WPT_PROJECTILE
			RETURN g_PickupSpawnLocations_Projectile[iSpawnLocation].ptPickUpType
		BREAK
		CASE FM_WPT_GUN		
			RETURN g_PickupSpawnLocations_Gun[iSpawnLocation].ptPickUpType
		BREAK		
	ENDSWITCH
	
	CASSERTLN(DEBUG_FMPICKUPS, "GET_PICKUP_TYPE_FOR_SPAWN_LOCATION - invalid. eType ", eType, ", iSpawnLocation ", iSpawnLocation)	
	RETURN PICKUP_TYPE_INVALID
	
ENDFUNC

FUNC WEAPON_TYPE GET_WEAPON_TYPE_FOR_SPAWN_LOCATION(FM_WEAPON_PICKUP_TYPES eType, INT iSpawnLocation)

	RETURN GET_WEAPON_TYPE_FROM_PICKUP_TYPE(GET_PICKUP_TYPE_FOR_SPAWN_LOCATION(eType, iSpawnLocation) )
	
ENDFUNC

FUNC VECTOR GET_PICKUP_COORDS_FOR_SPAWN_LOCATION(FM_WEAPON_PICKUP_TYPES eType, INT iSpawnLocation)
	SWITCH eType
		CASE FM_WPT_OTHER
			RETURN g_PickupSpawnLocations_Other[iSpawnLocation].vPos
		BREAK
		CASE FM_WPT_MELEE
			RETURN g_PickupSpawnLocations_Melee[iSpawnLocation].vPos
		BREAK
		CASE FM_WPT_PROJECTILE
			RETURN g_PickupSpawnLocations_Projectile[iSpawnLocation].vPos
		BREAK
		CASE FM_WPT_GUN		
			RETURN g_PickupSpawnLocations_Gun[iSpawnLocation].vPos
		BREAK		
	ENDSWITCH
	
	CASSERTLN(DEBUG_FMPICKUPS, "GET_PICKUP_COORDS_FOR_SPAWN_LOCATION - invalid. eType ", eType, ", iSpawnLocation ", iSpawnLocation)	
	RETURN <<0.0, 0.0, 0.0>>	
ENDFUNC

FUNC FM_WEAPON_PICKUP_TYPES GET_FM_WEAPON_PICKUP_TYPE_FROM_NETWORK_PICKUP_INDEX(INT iNetworkIndex)
	
	IF (iNetworkIndex < g_iNumberOfWeaponPickupsToBeCreated_Other)
		RETURN FM_WPT_OTHER
	ELIF (iNetworkIndex < g_iNumberOfWeaponPickupsToBeCreated_Other + g_iNumberOfWeaponPickupsToBeCreated_Melee)
		RETURN FM_WPT_MELEE
	ELIF (iNetworkIndex < g_iNumberOfWeaponPickupsToBeCreated_Other + g_iNumberOfWeaponPickupsToBeCreated_Melee + g_iNumberOfWeaponPickupsToBeCreated_Projectile)
		RETURN FM_WPT_PROJECTILE
	ELIF (iNetworkIndex < g_iNumberOfWeaponPickupsToBeCreated_Other + g_iNumberOfWeaponPickupsToBeCreated_Melee + g_iNumberOfWeaponPickupsToBeCreated_Projectile + g_iNumberOfWeaponPickupsToBeCreated_Gun)
		RETURN FM_WPT_GUN
	ENDIF
	
	CASSERTLN(DEBUG_FMPICKUPS, "GET_FM_WEAPON_PICKUP_TYPE_FROM_NETWORK_PICKUP_INDEX - invalid network index ", iNetworkIndex)	
	RETURN FM_WPT_OTHER
	
ENDFUNC

FUNC INT GET_NETWORK_PICKUP_INDEX_FOR_FM_WEAPON_PICKUP_TYPE(FM_WEAPON_PICKUP_TYPES eType, INT iIndex)
	
	SWITCH eType
		CASE FM_WPT_OTHER
			IF (iIndex < g_iNumberOfWeaponPickupsToBeCreated_Other)
				RETURN (iIndex)
			ENDIF
		BREAK
		CASE FM_WPT_MELEE
			IF (iIndex < g_iNumberOfWeaponPickupsToBeCreated_Melee)
				RETURN (iIndex + g_iNumberOfWeaponPickupsToBeCreated_Other)
			ENDIF
		BREAK
		CASE FM_WPT_PROJECTILE
			IF (iIndex < g_iNumberOfWeaponPickupsToBeCreated_Projectile)
				RETURN (iIndex + g_iNumberOfWeaponPickupsToBeCreated_Other + g_iNumberOfWeaponPickupsToBeCreated_Melee)
			ENDIF
		BREAK
		CASE FM_WPT_GUN		
			IF (iIndex < g_iNumberOfWeaponPickupsToBeCreated_Gun)
				RETURN (iIndex + g_iNumberOfWeaponPickupsToBeCreated_Other + g_iNumberOfWeaponPickupsToBeCreated_Melee + g_iNumberOfWeaponPickupsToBeCreated_Projectile)
			ENDIF	
		BREAK
	ENDSWITCH
	
	CASSERTLN(DEBUG_FMPICKUPS, "GET_NETWORK_PICKUP_INDEX_FOR_FM_WEAPON_PICKUP_TYPE - invalide index for type. eType ", eType, ", iIndex ", iIndex)	
	RETURN(-1)

ENDFUNC

FUNC INT GET_TOTAL_WEAPON_PICKUPS_TO_BE_CREATED_FOR_FM_WEAPON_PICKUP_TYPE(FM_WEAPON_PICKUP_TYPES eType)
	SWITCH eType
		CASE FM_WPT_OTHER 		RETURN g_iNumberOfWeaponPickupsToBeCreated_Other 		BREAK
		CASE FM_WPT_MELEE		RETURN g_iNumberOfWeaponPickupsToBeCreated_Melee 		BREAK	
		CASE FM_WPT_PROJECTILE	RETURN g_iNumberOfWeaponPickupsToBeCreated_Projectile	BREAK
		CASE FM_WPT_GUN			RETURN g_iNumberOfWeaponPickupsToBeCreated_Gun			BREAK
	ENDSWITCH	
	CASSERTLN(DEBUG_FMPICKUPS, "GET_TOTAL_WEAPON_PICKUPS_TO_BE_CREATED_FOR_FM_WEAPON_PICKUP_TYPE - invalid eType ", eType)	
	RETURN -1
ENDFUNC

FUNC BOOL IS_SPAWN_LOCATION_ALREADY_USED_FOR_FM_WEAPON_PICKUP_TYPE(FM_WEAPON_PICKUP_TYPES eType, INT iSpawnLocation)
	INT i
	INT iTotal = GET_TOTAL_WEAPON_PICKUPS_TO_BE_CREATED_FOR_FM_WEAPON_PICKUP_TYPE(eType)
	REPEAT iTotal i
		IF (GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[GET_NETWORK_PICKUP_INDEX_FOR_FM_WEAPON_PICKUP_TYPE(eType, i)]	= iSpawnLocation)
			RETURN(TRUE)
		ENDIF
	ENDREPEAT
	RETURN(FALSE)
ENDFUNC

FUNC INT GET_MAX_NUMBER_OF_SPAWN_LOCATIONS_FOR_FM_WEAPON_PICKUP_TYPE(FM_WEAPON_PICKUP_TYPES eType)
	SWITCH eType
		CASE FM_WPT_OTHER 			RETURN FM_WEAPON_PICKUPS_MAX_OTHERS 	BREAK
		CASE FM_WPT_MELEE			RETURN FM_WEAPON_PICKUPS_MAX_MELEE 		BREAK
		CASE FM_WPT_PROJECTILE		RETURN FM_WEAPON_PICKUPS_MAX_PROJECTILE BREAK
		CASE FM_WPT_GUN				RETURN FM_WEAPON_PICKUPS_MAX_GUN 		BREAK	
	ENDSWITCH
	CASSERTLN(DEBUG_FMPICKUPS, "GET_MAX_NUMBER_OF_SPAWN_LOCATIONS_FOR_FM_WEAPON_PICKUP_TYPE - invalid eType ", eType)	
	RETURN (-1)
ENDFUNC

FUNC INT GET_FREE_RANDOM_SPAWN_LOCATION_FOR_FM_WEAPON_PICKUP_TYPE(FM_WEAPON_PICKUP_TYPES eType)

	INT iSpawnLocation
	INT iMaxValue = GET_MAX_NUMBER_OF_SPAWN_LOCATIONS_FOR_FM_WEAPON_PICKUP_TYPE(eType)	
	iSpawnLocation = GET_RANDOM_INT_IN_RANGE(0, iMaxValue)
	
	INT iAttempt = 1
	INT iNewSpawnLocation
	INT iPlayers
	INT iNumPlayers
	INT iStoredSpawnLocation = -1
	INT iStoredNumPlayers = 999
	
	IF NOT IS_SPAWN_LOCATION_ALREADY_USED_FOR_FM_WEAPON_PICKUP_TYPE(eType, iSpawnLocation)
		RETURN(iSpawnLocation)
	ELSE
		// if it's not free then keep incrementing until we find a free slot
		WHILE iAttempt < iMaxValue
			
			iNewSpawnLocation = iSpawnLocation + iAttempt
			IF (iNewSpawnLocation >= iMaxValue)
				iNewSpawnLocation -= iMaxValue
			ENDIF
			
			IF NOT IS_SPAWN_LOCATION_ALREADY_USED_FOR_FM_WEAPON_PICKUP_TYPE(eType, iNewSpawnLocation)
				iStoredSpawnLocation = iNewSpawnLocation
				
				// if no player is within 100m of this location then return it, if not try another.
				IF NOT NETWORK_IS_ANY_PLAYER_NEAR(iPlayers, iNumPlayers, GET_PICKUP_COORDS_FOR_SPAWN_LOCATION(eType, iNewSpawnLocation), 100.0, FALSE)				
					CPRINTLN(DEBUG_FMPICKUPS, "GET_FREE_RANDOM_SPAWN_LOCATION_FOR_FM_WEAPON_PICKUP_TYPE - no player near. returning eType ", eType, " iNewSpawnLocation ", iNewSpawnLocation)
					RETURN(iNewSpawnLocation)				
				ELSE
					IF (iNumPlayers < iStoredNumPlayers)
						iStoredSpawnLocation = iNewSpawnLocation
						iStoredNumPlayers = iNumPlayers
						CPRINTLN(DEBUG_FMPICKUPS, "GET_FREE_RANDOM_SPAWN_LOCATION_FOR_FM_WEAPON_PICKUP_TYPE - players nearby storing as reserve. etype  ", eType, " iSpawnLocation ", iStoredSpawnLocation, " iPlayers ", iPlayers, " iNumPlayers ", iNumPlayers)						
					ELSE
						CPRINTLN(DEBUG_FMPICKUPS, "GET_FREE_RANDOM_SPAWN_LOCATION_FOR_FM_WEAPON_PICKUP_TYPE - players nearby NOT storing as reserve. etype  ", eType, " iSpawnLocation ", iStoredSpawnLocation, " iPlayers ", iPlayers, " iNumPlayers ", iNumPlayers)	
					ENDIF
				ENDIF
			ENDIF
		
			iAttempt++
		ENDWHILE
	ENDIF
	
	IF NOT (iStoredSpawnLocation = -1)
		CPRINTLN(DEBUG_FMPICKUPS, "GET_FREE_RANDOM_SPAWN_LOCATION_FOR_FM_WEAPON_PICKUP_TYPE - returning reserve value for ", eType, " iStoredSpawnLocation ", iStoredSpawnLocation)
		RETURN(iStoredSpawnLocation)
	ENDIF
	
	CASSERTLN(DEBUG_FMPICKUPS, "GET_FREE_RANDOM_SPAWN_LOCATION_FOR_FM_WEAPON_PICKUP_TYPE - could not find free slot for type ", eType)	
	RETURN -1

ENDFUNC

FUNC TIME_DATATYPE GET_COOL_DOWN_TIME_FOR_FM_WEAPON_PICKUP_TYPE(FM_WEAPON_PICKUP_TYPES eType)
	SWITCH eType
		CASE FM_WPT_OTHER 			RETURN GlobalServerBD_FM.WeaponPickupData.TimerOther 		BREAK
		CASE FM_WPT_MELEE			RETURN GlobalServerBD_FM.WeaponPickupData.TimerMelee  		BREAK
		CASE FM_WPT_PROJECTILE		RETURN GlobalServerBD_FM.WeaponPickupData.TimerProjectile  	BREAK
		CASE FM_WPT_GUN				RETURN GlobalServerBD_FM.WeaponPickupData.TimerGun	 		BREAK	
	ENDSWITCH
	CASSERTLN(DEBUG_FMPICKUPS, "GET_COOL_DOWN_TIME_FOR_FM_WEAPON_PICKUP_TYPE - invalid etype ", eType)	
	RETURN GET_NETWORK_TIME()
ENDFUNC

FUNC BOOL HAS_COOL_DOWN_TIMER_EXPIRED_FOR_FM_WEAPON_PICKUP_TYPE(FM_WEAPON_PICKUP_TYPES eType)
	
	TIME_DATATYPE storedTime = GET_COOL_DOWN_TIME_FOR_FM_WEAPON_PICKUP_TYPE(eType)
	
	INT iTunableCooldownTime = g_sMPTunables.iWeaponPickupCoolDownTime // tunable 
	iTunableCooldownTime *= 1000
	
	IF (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), storedTime)) > iTunableCooldownTime)
		RETURN(TRUE)	
	ENDIF
	RETURN(FALSE)

ENDFUNC

PROC RESET_COOL_DOWN_TIMER_FOR_FM_WEAPON_PICKUP_TYPE(FM_WEAPON_PICKUP_TYPES eType)
	
	SWITCH eType
		CASE FM_WPT_OTHER 			
			GlobalServerBD_FM.WeaponPickupData.TimerOther = GET_NETWORK_TIME()	
		BREAK
		CASE FM_WPT_MELEE			
			GlobalServerBD_FM.WeaponPickupData.TimerMelee = GET_NETWORK_TIME()  		
		BREAK
		CASE FM_WPT_PROJECTILE		
			GlobalServerBD_FM.WeaponPickupData.TimerProjectile = GET_NETWORK_TIME()  	
		BREAK
		CASE FM_WPT_GUN				
			GlobalServerBD_FM.WeaponPickupData.TimerGun = GET_NETWORK_TIME()	 		
		BREAK	
	ENDSWITCH

	CPRINTLN(DEBUG_FMPICKUPS, "RESET_COOL_DOWN_TIMER_FOR_FM_WEAPON_PICKUP_TYPE - reset for ", eType)

ENDPROC

FUNC INT GET_WEAPON_AMOUNT_FOR_FM_WEAPON_PICKUP_TYPE(FM_WEAPON_PICKUP_TYPES eType, INT iSpawnLocation)
	
	SWITCH eType
		CASE FM_WPT_OTHER 			
			RETURN 50 * 100 // 1 unit of petrol can liquid appear to be 50 ammo
		BREAK
		CASE FM_WPT_MELEE			
			RETURN 1		
		BREAK
		CASE FM_WPT_PROJECTILE		
			RETURN g_sMPTunables.iNumberOfProjectilesGifted	
		BREAK
		CASE FM_WPT_GUN				
			RETURN g_sMPTunables.iNumberOfAmmoClipsGifted * GET_WEAPON_CLIP_SIZE(GET_WEAPON_TYPE_FOR_SPAWN_LOCATION(eType, iSpawnLocation))
		BREAK	
	ENDSWITCH
	
	CASSERTLN(DEBUG_FMPICKUPS, "GET_WEAPON_AMOUNT_FOR_FM_WEAPON_PICKUP_TYPE - invalid etype ", eType, ", iSpawnLocation = ", iSpawnLocation)	
	RETURN -1
ENDFUNC

FUNC STRING GET_PICKUP_SOUND_NAME_FOR_WEAPON_TYPE(WEAPON_TYPE eWeaponType)
	
	SWITCH eWeaponType
		// other
		CASE WEAPONTYPE_PETROLCAN 					RETURN 		"PICKUP_WEAPON_PETROLCAN"
		
		// melee
		CASE WEAPONTYPE_KNIFE						RETURN		"PICKUP_WEAPON_KNIFE"
		CASE WEAPONTYPE_NIGHTSTICK					RETURN 		"PICKUP_WEAPON_NIGHTSTICK"
		CASE WEAPONTYPE_HAMMER						RETURN 		"PICKUP_WEAPON_HAMMER"
		CASE WEAPONTYPE_BAT							RETURN 		"PICKUP_WEAPON_BAT"
		CASE WEAPONTYPE_CROWBAR						RETURN 		"PICKUP_WEAPON_CROWBAR"
		CASE WEAPONTYPE_GOLFCLUB					RETURN 		"PICKUP_WEAPON_GOLFCLUB"
		CASE WEAPONTYPE_DLC_BOTTLE					RETURN		"PICKUP_WEAPON_DEFAULT"
		CASE WEAPONTYPE_DLC_DAGGER					RETURN		"PICKUP_WEAPON_DEFAULT"
		
		// proj
		CASE WEAPONTYPE_GRENADE 					RETURN		"PICKUP_WEAPON_GRENADE"
		CASE WEAPONTYPE_MOLOTOV						RETURN 		"PICKUP_WEAPON_MOLOTOV"
		CASE WEAPONTYPE_STICKYBOMB 					RETURN 		"PICKUP_WEAPON_STICKYBOMB"
		CASE WEAPONTYPE_SMOKEGRENADE				RETURN 		"PICKUP_WEAPON_SMOKEGRENADE"
		CASE WEAPONTYPE_DLC_PROXMINE				RETURN		"PICKUP_WEAPON_DEFAULT"
		
		// gun
		CASE WEAPONTYPE_COMBATPISTOL				RETURN 		"PICKUP_WEAPON_COMBATPISTOL"
		CASE WEAPONTYPE_MICROSMG					RETURN		"PICKUP_WEAPON_MICROSMG"
		CASE WEAPONTYPE_ASSAULTRIFLE				RETURN 		"PICKUP_WEAPON_ASSAULTRIFLE"
		CASE WEAPONTYPE_PUMPSHOTGUN					RETURN		"PICKUP_WEAPON_PUMPSHOTGUN"
		CASE WEAPONTYPE_SNIPERRIFLE					RETURN		"PICKUP_WEAPON_SNIPERRIFLE"
	ENDSWITCH
	
	// full list of sounds here:
//	PICKUP_WEAPON_PISTOL		
//	PICKUP_WEAPON_COMBATPISTOL		
//	PICKUP_WEAPON_PISTOL50		
//	PICKUP_WEAPON_APPISTOL		
//	PICKUP_WEAPON_MICROSMG		
//	PICKUP_WEAPON_SMG		
//	PICKUP_WEAPON_ASSAULTSMG		
//	PICKUP_WEAPON_ASSAULTRIFLE		
//	PICKUP_WEAPON_CARBINERIFLE		
//	PICKUP_WEAPON_HEAVYRIFLE		
//	PICKUP_WEAPON_ADVANCEDRIFLE		
//	PICKUP_WEAPON_MG		
//	PICKUP_WEAPON_COMBATMG		
//	PICKUP_WEAPON_ASSAULTMG		
//	PICKUP_WEAPON_PUMPSHOTGUN		
//	PICKUP_WEAPON_SAWNOFFSHOTGUN		
//	PICKUP_WEAPON_BULLPUPSHOTGUN		
//	PICKUP_WEAPON_ASSAULTSHOTGUN		
//	PICKUP_WEAPON_SNIPERRIFLE		
//	PICKUP_WEAPON_ASSAULTSNIPER		
//	PICKUP_WEAPON_HEAVYSNIPER		
//	PICKUP_WEAPON_GRENADELAUNCHER		
//	PICKUP_WEAPON_RPG		
//	PICKUP_WEAPON_MINIGUN		
//	PICKUP_WEAPON_GRENADE		
//	PICKUP_WEAPON_SMOKEGRENADE		
//	PICKUP_WEAPON_STICKYBOMB		
//	PICKUP_WEAPON_MOLOTOV		
//	PICKUP_WEAPON_STUNGUN		
//	PICKUP_WEAPON_RUBBERGUN		
//	PICKUP_WEAPON_PROGRAMMABLEAR		
//	PICKUP_WEAPON_FIREEXTINGUISHER		
//	PICKUP_WEAPON_PETROLCAN		
//	PICKUP_WEAPON_LOUDHAILER		
//	PICKUP_WEAPON_LASSO		
//	PICKUP_WEAPON_KNIFE		
//	PICKUP_WEAPON_NIGHTSTICK		
//	PICKUP_WEAPON_CROWBAR		
//	PICKUP_WEAPON_HAMMER		
//	PICKUP_WEAPON_BAT		
//	PICKUP_WEAPON_GOLFCLUB		
//	PICKUP_WEAPON_DEFAULT		
//	WEAPON_PURCHASE		
//	PICKUP_WEAPON_BALL
	
	
	CASSERTLN(DEBUG_FMPICKUPS, "GET_PICKUP_SOUND_NAME_FOR_WEAPON_TYPE - weapon type not listed ", eWeaponType)
	RETURN "PICKUP_WEAPON_DEFAULT"

ENDFUNC

#IF IS_DEBUG_BUILD

FUNC STRING GET_WEAPON_NAME_FOR_DEBUG(WEAPON_TYPE eWeaponType)
	
	SWITCH eWeaponType
		// Pistols
		CASE WEAPONTYPE_PISTOL					RETURN "PISTOL"
		CASE WEAPONTYPE_COMBATPISTOL			RETURN "CMBPISTOL"
		CASE WEAPONTYPE_APPISTOL				RETURN "APPISTOL"
		CASE WEAPONTYPE_DLC_SNSPISTOL			RETURN "SNSPISTOL"
		CASE WEAPONTYPE_DLC_HEAVYPISTOL			RETURN "HVPISTOL"
		CASE WEAPONTYPE_DLC_VINTAGEPISTOL		RETURN "VNTPISTOL"
		CASE WEAPONTYPE_DLC_MARKSMANPISTOL		RETURN "MARKSMAN"

		// Shotguns
		CASE WEAPONTYPE_PUMPSHOTGUN				RETURN "PUMPSHOTG"
		CASE WEAPONTYPE_SAWNOFFSHOTGUN			RETURN "SOFFSHOTG"
		CASE WEAPONTYPE_ASSAULTSHOTGUN 			RETURN "ASSSHOTG"
		CASE WEAPONTYPE_DLC_HEAVYSHOTGUN 		RETURN "HVSHOTG"
		
		// Light machine guns
		CASE WEAPONTYPE_MICROSMG				RETURN "MICROSMG"
		CASE WEAPONTYPE_SMG						RETURN "SMG"
		CASE WEAPONTYPE_MG						RETURN "MG"
		CASE WEAPONTYPE_COMBATMG				RETURN "COMBATMG"
		CASE WEAPONTYPE_DLC_GUSENBERG			RETURN "GUSSPR"
		CASE WEAPONTYPE_DLC_COMBATPDW			RETURN "COMPATPDW"
		
		// Assault rifles
		CASE WEAPONTYPE_ASSAULTRIFLE			RETURN "ASSRIFLE"
		CASE WEAPONTYPE_CARBINERIFLE			RETURN "CRBRIFLE"
		CASE WEAPONTYPE_ADVANCEDRIFLE			RETURN "ADVRIFLE"			
		CASE WEAPONTYPE_DLC_SPECIALCARBINE 		RETURN "SPCRB"
		CASE WEAPONTYPE_DLC_BULLPUPRIFLE		RETURN "BULLPRIFLE"	
		CASE WEAPONTYPE_DLC_MUSKET				RETURN "MUSKET"	
		
		// Sniper Rifles
		CASE WEAPONTYPE_SNIPERRIFLE				RETURN "SNIPERRFL"	
		CASE WEAPONTYPE_HEAVYSNIPER				RETURN "HVSNIPER"
		CASE WEAPONTYPE_DLC_MARKSMANRIFLE		RETURN "MARKSMAN"	
		
		// Special
		CASE WEAPONTYPE_GRENADELAUNCHER 		RETURN "GRENLNCH"
		CASE WEAPONTYPE_RPG						RETURN "RPG"
		CASE WEAPONTYPE_MINIGUN					RETURN "MINIGUN"
		CASE WEAPONTYPE_DLC_FIREWORK			RETURN "FIREWLNCH"
		CASE WEAPONTYPE_DLC_HOMINGLAUNCHER		RETURN "HOMINGLAUNCHER"
		
		// Thrown
		CASE WEAPONTYPE_GRENADE					RETURN "GRENADE"		
		CASE WEAPONTYPE_STICKYBOMB				RETURN "STICKYBOMB"
		CASE WEAPONTYPE_SMOKEGRENADE			RETURN "SMOKEGRN"
		CASE WEAPONTYPE_MOLOTOV 				RETURN "MOLOTOV"
		CASE WEAPONTYPE_PETROLCAN				RETURN "PETROLCAN"	
		CASE WEAPONTYPE_DLC_PROXMINE			RETURN "PROXMINE"	
		
		// Melee
		CASE WEAPONTYPE_KNIFE					RETURN "KNIFE"
		CASE WEAPONTYPE_NIGHTSTICK				RETURN "NIGHTSTICK"
		CASE WEAPONTYPE_BAT						RETURN "BAT"
		CASE WEAPONTYPE_CROWBAR 				RETURN "CROWBAR"
		CASE WEAPONTYPE_GOLFCLUB				RETURN "GOLFCLUB"	
		CASE WEAPONTYPE_DLC_BOTTLE				RETURN "BOTTLE"	
		CASE WEAPONTYPE_DLC_DAGGER				RETURN "DAGGER"	
		CASE WEAPONTYPE_DLC_KNUCKLE				RETURN "KNUCKLE"
		CASE WEAPONTYPE_HAMMER					RETURN "HAMMER"
		
		// Misc
		CASE WEAPONTYPE_INVALID					RETURN "ARMOUR"	 	//Armour???
		CASE WEAPONTYPE_UNARMED					RETURN "HEALTH" //Health???
		CASE WEAPONTYPE_FALL					RETURN "PARACHUTE"	//Parachute???			
	ENDSWITCH
	
	CASSERTLN(DEBUG_FMPICKUPS, "GET_WEAPON_NAME_FOR_DEBUG - weapon type not listed ", eWeaponType)
	RETURN ""

ENDFUNC

#ENDIF

FUNC INT GET_ITEM_ID_FROM_WEAPON_TYPE(WEAPON_TYPE eWeaponType)

	SWITCH eWeaponType
	
		// other
		CASE WEAPONTYPE_PETROLCAN 					RETURN 		HASH("WP_WT_PETROL_t5_v1")
		
		// melee
		CASE WEAPONTYPE_KNIFE						RETURN		HASH("WP_WT_KNIFE_t1_v1")
		CASE WEAPONTYPE_NIGHTSTICK					RETURN 		HASH("WP_WT_NGTSTK_t1_v1")
		CASE WEAPONTYPE_HAMMER						RETURN 		HASH("WP_WT_HAMMER_t1_v1")
		CASE WEAPONTYPE_BAT							RETURN -1
		CASE WEAPONTYPE_CROWBAR						RETURN -1
		CASE WEAPONTYPE_GOLFCLUB					RETURN -1
		CASE WEAPONTYPE_DLC_BOTTLE					RETURN		HASH("WP_WT_BOTTLE_t1_v1")
		CASE WEAPONTYPE_DLC_DAGGER					RETURN		HASH("WP_WT_DAGGER_t1_v1")
		
		// proj
		CASE WEAPONTYPE_GRENADE 					RETURN		HASH("WP_WT_GNADE_t2_v2")		
		CASE WEAPONTYPE_MOLOTOV						RETURN -1
		CASE WEAPONTYPE_STICKYBOMB 					RETURN 		HASH("WP_WT_GNADE_STK_t2_v2")
		CASE WEAPONTYPE_SMOKEGRENADE				RETURN 		HASH("WP_WT_GNADE_SMK_t2_v2")
		CASE WEAPONTYPE_DLC_PROXMINE				RETURN		HASH("WP_WT_PRXMINE_t2_v2")
		
		// gun
		CASE WEAPONTYPE_COMBATPISTOL				RETURN 		HASH("WP_WT_PIST_CBT_t0_v1")
		CASE WEAPONTYPE_MICROSMG					RETURN		HASH("WP_WT_SMG_MCR_t0_v1")
		CASE WEAPONTYPE_ASSAULTRIFLE				RETURN 		HASH("WP_WT_RIFLE_ASL_t0_v1")
		CASE WEAPONTYPE_PUMPSHOTGUN					RETURN		HASH("WP_WT_SG_PMP_t0_v1")
		CASE WEAPONTYPE_SNIPERRIFLE					RETURN		HASH("WP_WT_SNIP_RIF_t0_v1")

	ENDSWITCH

	CASSERTLN(DEBUG_FMPICKUPS, "GET_ITEM_ID_FROM_WEAPON_TYPE - weapon type not listed ", eWeaponType)
	RETURN -1

ENDFUNC

FUNC STRING GET_WEAPON_TEXT_LABEL_FROM_WEAPON_TYPE(WEAPON_TYPE eWeaponType)

	SWITCH eWeaponType
	
		// other
		CASE WEAPONTYPE_PETROLCAN 					RETURN 		"WT_PETROL"
		
		// melee
		CASE WEAPONTYPE_KNIFE						RETURN		"WT_KNIFE"
		CASE WEAPONTYPE_NIGHTSTICK					RETURN 		"WT_NGTSTK"
		CASE WEAPONTYPE_HAMMER						RETURN 		"WT_HAMMER"
		CASE WEAPONTYPE_BAT							RETURN 		"WT_BAT"
		CASE WEAPONTYPE_CROWBAR						RETURN 		"WT_CROWBAR"
		CASE WEAPONTYPE_GOLFCLUB					RETURN 		"WT_GOLFCLUB"
		CASE WEAPONTYPE_DLC_BOTTLE					RETURN		"WT_BOTTLE"
		CASE WEAPONTYPE_DLC_DAGGER					RETURN		"WT_DAGGER"
		
		// proj
		CASE WEAPONTYPE_GRENADE 					RETURN		"WT_GNADE"		
		CASE WEAPONTYPE_MOLOTOV						RETURN 		"WT_MOLOTOV"
		CASE WEAPONTYPE_STICKYBOMB 					RETURN 		"WT_GNADE_STK"
		CASE WEAPONTYPE_SMOKEGRENADE				RETURN 		"WT_GNADE_SMK"
		CASE WEAPONTYPE_DLC_PROXMINE				RETURN		"WT_PROXIM"
		
		// gun
		CASE WEAPONTYPE_COMBATPISTOL				RETURN 		"WT_PIST_CBT"
		CASE WEAPONTYPE_MICROSMG					RETURN		"WT_SMG_MCR"
		CASE WEAPONTYPE_ASSAULTRIFLE				RETURN 		"WT_RIFLE_ASL"
		CASE WEAPONTYPE_PUMPSHOTGUN					RETURN		"WT_SG_PMP"
		CASE WEAPONTYPE_SNIPERRIFLE					RETURN		"WT_SNIP_RIF"

	ENDSWITCH

	CASSERTLN(DEBUG_FMPICKUPS, "GET_WEAPON_TEXT_LABEL_FROM_WEAPON_TYPE - weapon type not listed ", eWeaponType)
	RETURN ""

ENDFUNC

FUNC WEAPON_TYPE GET_WEAPON_TYPE_FROM_MODEL_NAME(MODEL_NAMES ModelNameHash)
	
	SWITCH ENUM_TO_INT(ModelNameHash)
		// other
		CASE 242383520		RETURN WEAPONTYPE_PETROLCAN		BREAK
		
		// melee
		CASE -1982443329	RETURN WEAPONTYPE_KNIFE			BREAK
		CASE -1634978236	RETURN WEAPONTYPE_NIGHTSTICK	BREAK
		CASE 64104227		RETURN WEAPONTYPE_HAMMER		BREAK
		CASE 32653987		RETURN WEAPONTYPE_BAT			BREAK
		CASE 1862268168		RETURN WEAPONTYPE_CROWBAR		BREAK
		CASE -580196246		RETURN WEAPONTYPE_GOLFCLUB		BREAK
		CASE 1150762982		RETURN WEAPONTYPE_DLC_BOTTLE	BREAK
		CASE 601713565		RETURN WEAPONTYPE_DLC_DAGGER	BREAK
		
		// proj
		CASE 290600267 		RETURN WEAPONTYPE_GRENADE 		BREAK
		CASE -880609331 	RETURN WEAPONTYPE_MOLOTOV		BREAK
		CASE -1110203649 	RETURN WEAPONTYPE_STICKYBOMB 	BREAK
		CASE 1591549914		RETURN WEAPONTYPE_SMOKEGRENADE	BREAK
		CASE 1876445962		RETURN WEAPONTYPE_DLC_PROXMINE	BREAK
		
		// guns		
		CASE 403140669		RETURN WEAPONTYPE_COMBATPISTOL	BREAK
		CASE -1056713654	RETURN WEAPONTYPE_MICROSMG		BREAK
		CASE 273925117		RETURN WEAPONTYPE_ASSAULTRIFLE	BREAK		
		CASE 689760839		RETURN WEAPONTYPE_PUMPSHOTGUN	BREAK
		CASE 346403307		RETURN WEAPONTYPE_SNIPERRIFLE	BREAK
				
	ENDSWITCH
	
	CASSERTLN(DEBUG_FMPICKUPS, "GET_WEAPON_TYPE_FROM_MODEL_NAME - hash not in list ", ModelNameHash)
	
	RETURN WEAPONTYPE_INVALID
	
ENDFUNC

FUNC BOOL CREATE_FM_WEAPON_PICKUP_AT_SPAWN_LOCATION(PICKUP_INDEX &PickupID, BLIP_INDEX &BlipID, FM_WEAPON_PICKUP_TYPES eType, INT iSpawnLocation)
	

	WEAPON_TYPE WeaponType = GET_WEAPON_TYPE_FOR_SPAWN_LOCATION(eType, iSpawnLocation)
	MODEL_NAMES WeaponModel = GET_WEAPONTYPE_MODEL(WeaponType)
	#IF IS_DEBUG_BUILD
		STRING strModelName = GET_MODEL_NAME_FOR_DEBUG(WeaponModel)
		STRING strWeaponName = GET_WEAPON_NAME_FOR_DEBUG(WeaponType)	
	#ENDIF	
	VECTOR vPos = GET_PICKUP_COORDS_FOR_SPAWN_LOCATION(eType, iSpawnLocation)
	
	REQUEST_MODEL(WeaponModel)
	IF HAS_MODEL_LOADED(WeaponModel)
		
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_FMPICKUPS, "CREATE_FM_WEAPON_PICKUP_AT_SPAWN_LOCATION - WeaponModel ", strModelName, ", hash of ", WeaponModel)
			
			WEAPON_TYPE WeaponTypeCheck = GET_WEAPON_TYPE_FROM_MODEL_NAME(WeaponModel)
			IF NOT (WeaponTypeCheck = WeaponType)
				CASSERTLN(DEBUG_FMPICKUPS, "CREATE_FM_WEAPON_PICKUP_AT_SPAWN_LOCATION - model mismatch")	
			ENDIF
		#ENDIF

		INT iPlacementFlags = 0
		//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_MAP))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_LOCAL_ONLY))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
		SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ROTATE))

		VECTOR vMin, vMax
		GET_MODEL_DIMENSIONS(WeaponModel, vMin, vMax)
		
		vPos.z += ((vMax.z - vMin.z) * 0.5)
		vPos.z += FM_WEAPON_PICKUP_Z_OFFSET
		
		PICKUP_TYPE PickupType = GET_PICKUP_TYPE_FOR_SPAWN_LOCATION(eType, iSpawnLocation)

		PickupID = CREATE_PICKUP_ROTATE(PICKUP_CUSTOM_SCRIPT, vPos, <<0,360,0>>, iPlacementFlags, GlobalServerBD_FM.WeaponPickupData.iUniqueKey, EULER_YZX, FALSE, GET_WEAPONTYPE_MODEL(WeaponType))
		SET_CUSTOM_PICKUP_WEAPON_HASH(WeaponType, PickupID)

		// add blip
		IF (g_sMPTunables.bShowFMWeaponPickupBlips)
			BlipID  = ADD_BLIP_FOR_COORD(vPos)
			SET_BLIP_SPRITE(BlipID , GET_CORRECT_BLIP_SPRITE_FMMC(PickupType))
			SET_BLIP_SCALE(BlipID , BLIP_SIZE_NETWORK_PICKUP)
			SET_BLIP_NAME_FROM_TEXT_FILE(BlipID, GET_WEAPON_TEXT_LABEL_FROM_WEAPON_TYPE(WeaponType))
			SET_BLIP_AS_SHORT_RANGE(BlipID, TRUE)
			SHOW_HEIGHT_ON_BLIP(BlipID, TRUE)
			SET_BLIP_DISPLAY(BlipID, DISPLAY_RADAR_ONLY)
		ENDIF
		
		SET_MODEL_AS_NO_LONGER_NEEDED(WeaponModel)
		
		#IF IS_DEBUG_BUILD					
			CPRINTLN(DEBUG_FMPICKUPS, "CREATE_FM_WEAPON_PICKUP_AT_SPAWN_LOCATION - created ", strWeaponName, " at ", vPos, " for etype ", eType, ", iSpawnLocation ", iSpawnLocation, " PickupID = ", NATIVE_TO_INT(PickupID))
		#ENDIF

		RETURN(TRUE)
	ELSE
		CPRINTLN(DEBUG_FMPICKUPS, "CREATE_FM_WEAPON_PICKUP_AT_SPAWN_LOCATION - waiting on model loading. Model ", strModelName, " for weapon ", strWeaponName, " at ", vPos, " for etype ", eType, ", iSpawnLocation ", iSpawnLocation )	
	ENDIF
	RETURN(FALSE)
ENDFUNC

FUNC BOOL SHOULD_FM_WEAPON_PICKUPS_BE_DISABLED()
	IF IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID())
	OR NETWORK_IS_SCRIPT_ACTIVE(GET_FM_MISSION_CONTROLLER_SCRIPT_NAME(),-1, TRUE )	
	OR IS_ON_DEATHMATCH_GLOBAL_SET()
	OR (g_Private_Players_FM_SESSION_Menu_Choice_PERSIST = FM_SESSION_MENU_CHOICE_JOIN_CLOSED_INVITE_ONLY)
	OR NETWORK_SESSION_IS_CLOSED_FRIENDS()
	OR NETWORK_SESSION_IS_CLOSED_CREW()
	OR NETWORK_SESSION_IS_SOLO()
	OR NETWORK_SESSION_IS_PRIVATE()
	OR (g_b_On_Race = TRUE)
	OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_BIKER_DEAL_GONE_WRONG
		RETURN TRUE
	ENDIF
	RETURN(FALSE)
ENDFUNC

PROC GIVE_FM_WEAPON_PICKUP_TO_LOCAL_PLAYER(INT iGlobalArrayIndex)
	
	IF iGlobalArrayIndex < 0
	OR iGlobalArrayIndex >= COUNT_OF(g_iFMWeaponPickupState)
		CPRINTLN(DEBUG_FMPICKUPS, "GIVE_FM_WEAPON_PICKUP_TO_LOCAL_PLAYER [VALIDATE] - invalid value in iGlobalArrayIndex, expected value between 0 and (FM_MAX_NUMBER_OF_WEAPON_PICKUPS-1), actual value: ", iGlobalArrayIndex)
		SCRIPT_ASSERT("GIVE_FM_WEAPON_PICKUP_TO_LOCAL_PLAYER [VALIDATE] - invalid value in iGlobalArrayIndex")
		EXIT
	ENDIF
	
	INT iItemID
	TEXT_LABEL_15 labelTemp

	IF (g_iFMWeaponPickupState[iGlobalArrayIndex] = FM_WEAPON_PICKUP_STATE_CLAIM_PENDING)
					
		FM_WEAPON_PICKUP_TYPES eType = GET_FM_WEAPON_PICKUP_TYPE_FROM_NETWORK_PICKUP_INDEX(iGlobalArrayIndex)
		INT iSpawnLocation  = g_iFMWeaponPickupSpawnLocation[iGlobalArrayIndex]		
		
		IF (iSpawnLocation > -1)
			INT iAmmo = GET_WEAPON_AMOUNT_FOR_FM_WEAPON_PICKUP_TYPE(eType, iSpawnLocation)
			WEAPON_TYPE eWeaponType = GET_WEAPON_TYPE_FOR_SPAWN_LOCATION(eType, iSpawnLocation)
			
			#IF IS_DEBUG_BUILD		
				STRING strWeaponName = GET_WEAPON_NAME_FOR_DEBUG(eWeaponType)			
				CPRINTLN(DEBUG_FMPICKUPS, "GIVE_FM_WEAPON_PICKUP_TO_LOCAL_PLAYER - Giving local player ", strWeaponName, " iAmmo ", iAmmo, " iSpawnLocation ", iSpawnLocation, ", iGlobalArrayIndex ", iGlobalArrayIndex)
			#ENDIF
			
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), eWeaponType, iAmmo, FALSE, FALSE)	
			
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 100, 100)
			
			//PLAY_SOUND_FRONTEND(-1,GET_PICKUP_SOUND_NAME_FOR_WEAPON_TYPE(eWeaponType),"HUD_FRONTEND_WEAPONS_PICKUPS_SOUNDSET")
			
			// if player doesn't already own the weapon them give them it.
			IF NOT IS_MP_WEAPON_PURCHASED(eWeaponType)
				
				IF USE_SERVER_TRANSACTIONS()
					iItemID = GET_ITEM_ID_FROM_WEAPON_TYPE(eWeaponType)	
					IF (iItemID != -1)
					#IF IS_DEBUG_BUILD
					AND NOT (g_bSkipCashTransaction)
					#ENDIF
						IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_WEAPON, iItemID, NET_SHOP_ACTION_SPEND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, DEFAULT, CTPF_AUTO_RETRY|CTPF_FAIL_ALERT|CTPF_AUTO_PROCESS_REPLY)
							NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
						ENDIF
					ENDIF
				ENDIF
				
				SET_MP_WEAPON_PURCHASED(eWeaponType, TRUE)
				
				// melee weapons might need to have their ammo set
				IF (eType = FM_WPT_MELEE)
					IF GET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(eWeaponType)) =  0
						CPRINTLN(DEBUG_FMPICKUPS, "GIVE_FM_WEAPON_PICKUP_TO_LOCAL_PLAYER - setting ammo bought stat for weapon ", eType)
						SET_MP_INT_CHARACTER_STAT(GET_AMMO_BOUGHT_FOR_WEAPON(eWeaponType), 1)
					ENDIF
				ENDIF
				
			ENDIF
					
			labelTemp = GET_CORRECT_TICKER_STRING(GET_PICKUP_TYPE_FOR_SPAWN_LOCATION(eType, iSpawnLocation))
	       	PRINT_WEAPON_TICKER(labelTemp) 
			
			g_bRewardedAmmo = TRUE
			
			REMOVE_AND_RESET_FM_WEAPON_PICKUP(iGlobalArrayIndex)
		ELSE
			CASSERTLN(DEBUG_FMPICKUPS, "GIVE_FM_WEAPON_PICKUP_TO_LOCAL_PLAYER - spawn location is -1.")	
		ENDIF
		
	ELSE
	
		CPRINTLN(DEBUG_FMPICKUPS, "GIVE_FM_WEAPON_PICKUP_TO_LOCAL_PLAYER - trying to give weapon to local player, but player no longer staking a claim. iGlobalArrayIndex ", iGlobalArrayIndex)	
	
		//CASSERTLN(DEBUG_FMPICKUPS, "GIVE_FM_WEAPON_PICKUP_TO_LOCAL_PLAYER - trying to give weapon to player who has not staked a claim.")		
		
	ENDIF

ENDPROC

PROC UPDATE_FM_WEAPON_PICKUP(INT iIndex)

	// the server has removed this pickup
	IF NOT (g_iFMWeaponPickupSpawnLocation[iIndex] = GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[iIndex])
	AND NOT (g_iFMWeaponPickupSpawnLocation[iIndex] = -1)
		
		CPRINTLN(DEBUG_FMPICKUPS, "UPDATE_FM_WEAPON_PICKUP - values differ.  g_iFMWeaponPickupSpawnLocation[", iIndex, "] = ", g_iFMWeaponPickupSpawnLocation[iIndex], ", GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[", iIndex, "] = ", GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[iIndex])
		
		IF NOT (g_iFMWeaponPickupState[iIndex] = FM_WEAPON_PICKUP_STATE_CLAIM_PENDING)
			CPRINTLN(DEBUG_FMPICKUPS, "UPDATE_FM_WEAPON_PICKUP - server has removed this pickup ", iIndex)
			REMOVE_AND_RESET_FM_WEAPON_PICKUP(iIndex)
			EXIT
		ELSE
			CPRINTLN(DEBUG_FMPICKUPS, "UPDATE_FM_WEAPON_PICKUP - server has removed this pickup, but i still have a claim pending. ", iIndex)
			
			// did the server set the broadcast bit to award it to me?
			IF (GlobalServerBD_FM.WeaponPickupData.iCollector[iIndex] = -1)
				CPRINTLN(DEBUG_FMPICKUPS, "UPDATE_FM_WEAPON_PICKUP - i have a claim on this pickup, waiting for server bd. ", iIndex)	
			ELSE
				IF (GlobalServerBD_FM.WeaponPickupData.iCollector[iIndex] = NATIVE_TO_INT(PLAYER_ID()))
					CPRINTLN(DEBUG_FMPICKUPS, "UPDATE_FM_WEAPON_PICKUP - server has awarded this pickup to me! ", iIndex)	
					GIVE_FM_WEAPON_PICKUP_TO_LOCAL_PLAYER(iIndex)
					REMOVE_AND_RESET_FM_WEAPON_PICKUP(iIndex)
					EXIT
				ELSE
					CPRINTLN(DEBUG_FMPICKUPS, "UPDATE_FM_WEAPON_PICKUP - server has awarded this pickup to player ", GlobalServerBD_FM.WeaponPickupData.iCollector[iIndex], " for pickup ", iIndex)	
					REMOVE_AND_RESET_FM_WEAPON_PICKUP(iIndex)
					EXIT
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF	
	
	IF NOT DOES_PICKUP_EXIST(g_FMWeaponPickup[iIndex])
		// the server spawn location is different from what we currently have then create the pickup.
		IF NOT (g_iFMWeaponPickupSpawnLocation[iIndex] = GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[iIndex])
		AND NOT (GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[iIndex] = -1)		
		//AND NOT (GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].WeaponPickupData.bClaimingPickup[iIndex])
		AND NOT IS_FM_WEAPON_PICKUP_BITSET_SET(NATIVE_TO_INT(PLAYER_ID()),iIndex)
		AND (GlobalServerBD_FM.WeaponPickupData.iUniqueKey > 0)
		AND NOT (g_iFMWeaponPickupState[iIndex] = FM_WEAPON_PICKUP_STATE_CLAIM_PENDING)
			FM_WEAPON_PICKUP_TYPES eType = GET_FM_WEAPON_PICKUP_TYPE_FROM_NETWORK_PICKUP_INDEX(iIndex)
			CPRINTLN(DEBUG_FMPICKUPS, "UPDATE_FM_WEAPON_PICKUP - creating pickup ", iIndex)			
			IF CREATE_FM_WEAPON_PICKUP_AT_SPAWN_LOCATION(g_FMWeaponPickup[iIndex], g_FMWeaponPickupBlipID[iIndex], eType, GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[iIndex])	
				g_iFMWeaponPickupSpawnLocation[iIndex] = GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[iIndex]
			ENDIF
		ELSE
//			#IF IS_DEBUG_BUILD
//				CPRINTLN(DEBUG_FMPICKUPS, "UPDATE_FM_WEAPON_PICKUP - not creating pickup  ", iIndex)	
//				CPRINTLN(DEBUG_FMPICKUPS, "UPDATE_FM_WEAPON_PICKUP - not creating pickup - g_iFMWeaponPickupSpawnLocation[iIndex] = ", g_iFMWeaponPickupSpawnLocation[iIndex])
//				CPRINTLN(DEBUG_FMPICKUPS, "UPDATE_FM_WEAPON_PICKUP - not creating pickup - GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[iIndex] = ", g_iFMWeaponPickupSpawnLocation[iIndex])
//				CPRINTLN(DEBUG_FMPICKUPS, "UPDATE_FM_WEAPON_PICKUP - not creating pickup - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].WeaponPickupData.bClaimingPickup[iIndex] = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].WeaponPickupData.bClaimingPickup[iIndex])
//				CPRINTLN(DEBUG_FMPICKUPS, "UPDATE_FM_WEAPON_PICKUP - not creating pickup - GlobalServerBD_FM.WeaponPickupData.iUniqueKey = ", GlobalServerBD_FM.WeaponPickupData.iUniqueKey)
//				CPRINTLN(DEBUG_FMPICKUPS, "UPDATE_FM_WEAPON_PICKUP - not creating pickup - g_iFMWeaponPickupState[iIndex]  = ", g_iFMWeaponPickupState[iIndex] )
//			#ENDIF			
		ENDIF	
	ELSE
		
//		#IF IS_DEBUG_BUILD
//			IF (g_iFMWeaponPickupSpawnLocation[iIndex] = -1)
//				CPRINTLN(DEBUG_FMPICKUPS, "UPDATE_FM_WEAPON_PICKUP - pickup exists but  g_iFMWeaponPickupSpawnLocation[", iIndex, "] = -1 ")	
//			ENDIF
//		#ENDIF
	
//		// has this been picked up locally?
//		IF HAS_PICKUP_BEEN_COLLECTED(g_FMWeaponPickup[iIndex])
//			BROADCAST_COLLECTED_FM_WEAPON_PICKUP(iIndex)
//			SET_FM_WEAPON_PICKUP_AS_COLLECTED(iIndex, TRUE)
//			CPRINTLN(DEBUG_FMPICKUPS, "UPDATE_FM_WEAPON_PICKUP - pickup has been collected ", iIndex)
//		ENDIF
	ENDIF
	

	
ENDPROC

PROC UPDATE_ALL_FM_WEAPON_PICKUPS_NOW()
	CPRINTLN(DEBUG_FMPICKUPS, "UPDATE_ALL_FM_WEAPON_PICKUPS_NOW - called")
	INT i
	REPEAT g_iTotalNumberOfWeaponPickupsToBeCreated i 
		UPDATE_FM_WEAPON_PICKUP(i)
	ENDREPEAT
ENDPROC

PROC CLIENT_UPDATE_FM_WEAPON_PICKUPS(INT &iStagger)
	
	IF (g_iTotalNumberOfWeaponPickupsToBeCreated = 0)
		// if no weapons are to be created then quit out
		EXIT
	ENDIF	
	
	// should it be disabled?
	IF SHOULD_FM_WEAPON_PICKUPS_BE_DISABLED()
		IF NOT (g_bFMWeaponPickupsDisabled)
			REMOVE_AND_RESET_ALL_FM_WEAPON_PICKUPS()	
			g_bFMWeaponPickupsDisabled = TRUE
			CPRINTLN(DEBUG_FMPICKUPS, "CLIENT_UPDATE_FM_WEAPON_PICKUPS - removed and disabled fm weapon pickups")
		ENDIF
		EXIT
	ELSE
		IF (g_bFMWeaponPickupsDisabled)
			CPRINTLN(DEBUG_FMPICKUPS, "CLIENT_UPDATE_FM_WEAPON_PICKUPS - re-enabled fm weapon pickups")
			g_bFMWeaponPickupsDisabled = FALSE
		ENDIF
	ENDIF
	
	UPDATE_FM_WEAPON_PICKUP(iStagger)
	
	iStagger++
	IF (iStagger >= g_iTotalNumberOfWeaponPickupsToBeCreated)
		iStagger = 0
	ENDIF	

ENDPROC


PROC SERVER_FM_WEAPON_PICKUP_HAS_BEEN_CLAIMED(INT iLocation)
	GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[iLocation] = -1
	CPRINTLN(DEBUG_FMPICKUPS, "SERVER_FM_WEAPON_PICKUP_HAS_BEEN_CLAIMED - pickup has been claimed and removed. iLocation ", iLocation)	
	
	// should we reset the cooldown timer?
	FM_WEAPON_PICKUP_TYPES eType
	eType = GET_FM_WEAPON_PICKUP_TYPE_FROM_NETWORK_PICKUP_INDEX(iLocation)
	IF HAS_COOL_DOWN_TIMER_EXPIRED_FOR_FM_WEAPON_PICKUP_TYPE(eType)
		CPRINTLN(DEBUG_FMPICKUPS, "SERVER_FM_WEAPON_PICKUP_HAS_BEEN_CLAIMED - cool down timer should be reset. eType ", eType)	
		RESET_COOL_DOWN_TIMER_FOR_FM_WEAPON_PICKUP_TYPE(eType)
	ELSE
		CPRINTLN(DEBUG_FMPICKUPS, "SERVER_FM_WEAPON_PICKUP_HAS_BEEN_CLAIMED - dont reset cool down timer. eType ", eType)		
	ENDIF	
	
ENDPROC


PROC SERVER_UPDATE_FM_WEAPON_PICKUPS(INT &iStagger, INT &iPlayerStagger)

	IF (g_iTotalNumberOfWeaponPickupsToBeCreated = 0)
		// if no weapons are to be created then quit out
		EXIT
	ENDIF

	FM_WEAPON_PICKUP_TYPES eType
	eType = GET_FM_WEAPON_PICKUP_TYPE_FROM_NETWORK_PICKUP_INDEX(iStagger)
	
	BOOL bCooldownTimerExpired
	
	
	// set unique key
	IF (GlobalServerBD_FM.WeaponPickupData.iUniqueKey <= 0)
		GlobalServerBD_FM.WeaponPickupData.iUniqueKey = GET_RANDOM_INT_IN_RANGE(1, 1000)
		CPRINTLN(DEBUG_FMPICKUPS, "SERVER_UPDATE_FM_WEAPON_PICKUPS - set iUniqueKey to ", GlobalServerBD_FM.WeaponPickupData.iUniqueKey)								
	ENDIF
	
	// is this player staking a claim?
	//IF (GlobalplayerBD_FM[iPlayerStagger].WeaponPickupData.bClaimingPickup[iStagger])
	IF IS_FM_WEAPON_PICKUP_BITSET_SET(iPlayerStagger,iStagger)
		CPRINTLN(DEBUG_FMPICKUPS, "SERVER_UPDATE_FM_WEAPON_PICKUPS - player ", iPlayerStagger, " is staking a claim for pickup ", iStagger)
		IF (GlobalServerBD_FM.WeaponPickupData.iCollector[iStagger] = -1)
			CPRINTLN(DEBUG_FMPICKUPS, "SERVER_UPDATE_FM_WEAPON_PICKUPS - allowing player ", iPlayerStagger, " to claim pickup ", iStagger)
			GlobalServerBD_FM.WeaponPickupData.iCollector[iStagger] = iPlayerStagger
			BROADCAST_RESPONSE_TO_CLAIM_FM_WEAPON_PICKUP(iStagger, INT_TO_NATIVE(PLAYER_INDEX, iPlayerStagger))
		ELSE
			CPRINTLN(DEBUG_FMPICKUPS, "SERVER_UPDATE_FM_WEAPON_PICKUPS - player ", GlobalServerBD_FM.WeaponPickupData.iCollector[iStagger], " has already claimed pickup ", iStagger)
		ENDIF
	ELSE
		// was this player an old collector of pickup?
		IF (GlobalServerBD_FM.WeaponPickupData.iCollector[iStagger] = iPlayerStagger)
			CPRINTLN(DEBUG_FMPICKUPS, "SERVER_UPDATE_FM_WEAPON_PICKUPS - player ", iPlayerStagger, " no longer claiming pickup ", iStagger, " setting collector back to -1")
			GlobalServerBD_FM.WeaponPickupData.iCollector[iStagger] = -1
		ENDIF
	ENDIF

	// the pickup might not exist because this local player is doing a dm or something, but we'll want to assign a 
	IF (GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[iStagger] = -1)

		bCooldownTimerExpired = HAS_COOL_DOWN_TIMER_EXPIRED_FOR_FM_WEAPON_PICKUP_TYPE(eType)
	
		// should we create it?
		IF (GlobalServerBD_FM.WeaponPickupData.iNumberInitiallyCreated < g_iTotalNumberOfWeaponPickupsToBeCreated)
		OR bCooldownTimerExpired
		
			IF bCooldownTimerExpired
				CPRINTLN(DEBUG_FMPICKUPS, "SERVER_UPDATE_FM_WEAPON_PICKUPS - timer expired! - eType ", eType, ", pickup ", iStagger, " assigned spawn location ", GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[iStagger])						
				RESET_COOL_DOWN_TIMER_FOR_FM_WEAPON_PICKUP_TYPE(eType)
			ELSE
				CPRINTLN(DEBUG_FMPICKUPS, "SERVER_UPDATE_FM_WEAPON_PICKUPS - iNumberInitiallyCreated not reached max value - ", GlobalServerBD_FM.WeaponPickupData.iNumberInitiallyCreated)							
			ENDIF
			
			// set spawn location
			GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[iStagger] = GET_FREE_RANDOM_SPAWN_LOCATION_FOR_FM_WEAPON_PICKUP_TYPE(eType)
			GlobalServerBD_FM.WeaponPickupData.iCollector[iStagger] = -1
			CPRINTLN(DEBUG_FMPICKUPS, "SERVER_UPDATE_FM_WEAPON_PICKUPS - eType ", eType, ", pickup ", iStagger, " assigned spawn location ", GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[iStagger])			
			
			// increment initial creation amount
			IF (GlobalServerBD_FM.WeaponPickupData.iNumberInitiallyCreated < g_iTotalNumberOfWeaponPickupsToBeCreated)
				GlobalServerBD_FM.WeaponPickupData.iNumberInitiallyCreated++
				CPRINTLN(DEBUG_FMPICKUPS, "SERVER_UPDATE_FM_WEAPON_PICKUPS - new GlobalServerBD_FM.WeaponPickupData.iNumberInitiallyCreated ", GlobalServerBD_FM.WeaponPickupData.iNumberInitiallyCreated)			
			ENDIF
			
		ENDIF
		
	ELSE
		//CPRINTLN(DEBUG_FMPICKUPS, "SERVER_UPDATE_FM_WEAPON_PICKUPS - pickup doesn't exist, but spawn location is valid. eType ", eType, ", pickup ", iStagger, " assigned spawn location ", GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[iStagger])			
		
		// has anyone collected a pickup? we can stagger this as in theory all players (who dont have pickups disabled) should set their bit, so we only need 1 to be true
		IF IS_NET_PLAYER_OK(INT_TO_NATIVE(PLAYER_INDEX, iPlayerStagger), FALSE, FALSE)
		AND IS_FM_WEAPON_PICKUP_BITSET_SET(iPlayerStagger,iStagger)
		//(GlobalplayerBD_FM[iPlayerStagger].WeaponPickupData.bClaimingPickup[iStagger] = TRUE)
			
			SERVER_FM_WEAPON_PICKUP_HAS_BEEN_CLAIMED(iStagger)

		ENDIF	

	ENDIF



	iStagger++
	IF (iStagger >= g_iTotalNumberOfWeaponPickupsToBeCreated)
		iStagger = 0
	ENDIF
	
	// only increment the player stagger when the first stagger hits 0
	IF (iStagger = 0)
		iPlayerStagger++
		IF (iPlayerStagger >= NUM_NETWORK_PLAYERS)
			iPlayerStagger = 0	
		ENDIF
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD

PROC ADD_WIDGETS_FOR_FM_WEAPON_PICKUPS()
	
	INT i//, j
	TEXT_LABEL_63 str

	START_WIDGET_GROUP("FM WEAPON PICKUPS")
		
		// locations
		START_WIDGET_GROUP("Locations")
			
			START_WIDGET_GROUP("g_PickupSpawnLocations_Other")
				REPEAT FM_WEAPON_PICKUPS_MAX_OTHERS i
					str = "["
					str += i
					str += "]"
					ADD_WIDGET_VECTOR_SLIDER(str, g_PickupSpawnLocations_Other[i].vPos, -99999.9, 99999.9, 0.01)
				ENDREPEAT
			STOP_WIDGET_GROUP()

			START_WIDGET_GROUP("g_PickupSpawnLocations_Melee")
				REPEAT FM_WEAPON_PICKUPS_MAX_MELEE i
					str = "["
					str += i
					str += "]"
					ADD_WIDGET_VECTOR_SLIDER(str, g_PickupSpawnLocations_Melee[i].vPos, -99999.9, 99999.9, 0.01)
				ENDREPEAT
			STOP_WIDGET_GROUP()

			START_WIDGET_GROUP("g_PickupSpawnLocations_Projectile")
				REPEAT FM_WEAPON_PICKUPS_MAX_PROJECTILE i
					str = "["
					str += i
					str += "]"
					ADD_WIDGET_VECTOR_SLIDER(str, g_PickupSpawnLocations_Projectile[i].vPos, -99999.9, 99999.9, 0.01)
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("g_PickupSpawnLocations_Gun")
				REPEAT FM_WEAPON_PICKUPS_MAX_GUN i
					str = "["
					str += i
					str += "]"
					ADD_WIDGET_VECTOR_SLIDER(str, g_PickupSpawnLocations_Gun[i].vPos, -99999.9, 99999.9, 0.01)
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("globals")
			ADD_WIDGET_INT_SLIDER("g_iNumberOfWeaponPickupsToBeCreated_Other", g_iNumberOfWeaponPickupsToBeCreated_Other, -1, 999, 1)
			ADD_WIDGET_INT_SLIDER("g_iNumberOfWeaponPickupsToBeCreated_Melee", g_iNumberOfWeaponPickupsToBeCreated_Melee, -1, 999, 1)
			ADD_WIDGET_INT_SLIDER("g_iNumberOfWeaponPickupsToBeCreated_Projectile", g_iNumberOfWeaponPickupsToBeCreated_Projectile, -1, 999, 1)
			ADD_WIDGET_INT_SLIDER("g_iNumberOfWeaponPickupsToBeCreated_Gun", g_iNumberOfWeaponPickupsToBeCreated_Gun, -1, 999, 1)
			ADD_WIDGET_INT_SLIDER("g_iTotalNumberOfWeaponPickupsToBeCreated", g_iTotalNumberOfWeaponPickupsToBeCreated, -1, 999, 1)
			
			START_WIDGET_GROUP("g_iFMWeaponPickupSpawnLocation")
				REPEAT FM_MAX_NUMBER_OF_WEAPON_PICKUPS i	
					str = "["
					str += i
					str += "]"
					ADD_WIDGET_INT_SLIDER(str, g_iFMWeaponPickupSpawnLocation[i], -1, 999, 1)
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("g_iFMWeaponPickupState")
				REPEAT FM_MAX_NUMBER_OF_WEAPON_PICKUPS i	
					str = "["
					str += i
					str += "]"
					ADD_WIDGET_INT_SLIDER(str, g_iFMWeaponPickupState[i], -1, 999, 1)
				ENDREPEAT
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_FLOAT_SLIDER("FM_WEAPON_PICKUP_Z_OFFSET", FM_WEAPON_PICKUP_Z_OFFSET, -1.0, 1.0, 0.01)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("tunables")
			ADD_WIDGET_INT_SLIDER("g_sMPTunables.iNumberOfWeaponPickups", g_sMPTunables.iNumberOfWeaponPickups, -1, 80, 1)
			ADD_WIDGET_INT_SLIDER("g_sMPTunables.iPercentageOfGuns", g_sMPTunables.iPercentageOfGuns, -1, 100, 1)
			ADD_WIDGET_INT_SLIDER("g_sMPTunables.iPercentageOfProjectiles", g_sMPTunables.iPercentageOfProjectiles, -1, 100, 1)
			ADD_WIDGET_INT_SLIDER("g_sMPTunables.iPercentageOfMelee", g_sMPTunables.iPercentageOfMelee, -1, 100, 1)
			ADD_WIDGET_INT_SLIDER("g_sMPTunables.iWeaponPickupCoolDownTime", g_sMPTunables.iWeaponPickupCoolDownTime, -1, 99999, 1)
			ADD_WIDGET_INT_SLIDER("g_sMPTunables.iNumberOfAmmoClipsGifted", g_sMPTunables.iNumberOfAmmoClipsGifted, -1, 99999, 1)
			ADD_WIDGET_INT_SLIDER("g_sMPTunables.iNumberOfProjectilesGifted", g_sMPTunables.iNumberOfProjectilesGifted, -1, 99999, 1)
			ADD_WIDGET_BOOL("g_sMPTunables.bShowFMWeaponPickupBlips", g_sMPTunables.bShowFMWeaponPickupBlips)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("server bd")
			START_WIDGET_GROUP("GlobalServerBD_FM.WeaponPickupData.iSpawnLocation")
				REPEAT FM_MAX_NUMBER_OF_WEAPON_PICKUPS i	
					str = "["
					str += i
					str += "]"
					ADD_WIDGET_INT_SLIDER(str, GlobalServerBD_FM.WeaponPickupData.iSpawnLocation[i], -1, 999, 1)
				ENDREPEAT
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("GlobalServerBD_FM.WeaponPickupData.iCollector")
				REPEAT FM_MAX_NUMBER_OF_WEAPON_PICKUPS i	
					str = "["
					str += i
					str += "]"
					ADD_WIDGET_INT_SLIDER(str, GlobalServerBD_FM.WeaponPickupData.iCollector[i], -1, 999, 1)
				ENDREPEAT
			STOP_WIDGET_GROUP()
			ADD_WIDGET_INT_SLIDER("GlobalServerBD_FM.WeaponPickupData.iNumberInitiallyCreated", GlobalServerBD_FM.WeaponPickupData.iNumberInitiallyCreated, -1, 999, 1)
			ADD_WIDGET_INT_SLIDER("GlobalServerBD_FM.WeaponPickupData.iUniqueKey", GlobalServerBD_FM.WeaponPickupData.iUniqueKey, -1, HIGHEST_INT, 1)
		STOP_WIDGET_GROUP()
		
//		START_WIDGET_GROUP("PlayerBD")
//			REPEAT FM_MAX_NUMBER_OF_WEAPON_PICKUPS i
//				str = "bClaimingPickup["
//				str += i
//				str += "]"	
//				START_WIDGET_GROUP(str)
//					REPEAT NUM_NETWORK_PLAYERS j
//						str = "player "
//						str += j
//						ADD_WIDGET_BOOL(str, GlobalplayerBD_FM[j].WeaponPickupData.bClaimingPickup[i])
//					ENDREPEAT
//				STOP_WIDGET_GROUP()
//			ENDREPEAT
//		STOP_WIDGET_GROUP()

		START_WIDGET_GROUP("debug")
			ADD_WIDGET_BOOL("g_bSkipCashTransaction", g_bSkipCashTransaction)
			ADD_WIDGET_BOOL("g_bFMWeaponPickup_ResetEverythingNow",g_bFMWeaponPickup_ResetEverythingNow)
			ADD_WIDGET_BOOL("g_bFMWeaponPickup_DeleteAllClientPickupsNow", g_bFMWeaponPickup_DeleteAllClientPickupsNow)
			ADD_WIDGET_BOOL("g_bRenderAllFMWeaponPickupLocations_Other", g_bRenderAllFMWeaponPickupLocations_Other)
			ADD_WIDGET_BOOL("g_bRenderAllFMWeaponPickupLocations_Melee", g_bRenderAllFMWeaponPickupLocations_Melee)	
			ADD_WIDGET_BOOL("g_bRenderAllFMWeaponPickupLocations_Projectile", g_bRenderAllFMWeaponPickupLocations_Projectile)	
			ADD_WIDGET_BOOL("g_bRenderAllFMWeaponPickupLocations_Gun", g_bRenderAllFMWeaponPickupLocations_Gun)	
			ADD_WIDGET_BOOL("g_bRenderAllFMWeaponPickupLocations_Active", g_bRenderAllFMWeaponPickupLocations_Active)	
		STOP_WIDGET_GROUP()
	
	STOP_WIDGET_GROUP()

ENDPROC



PROC UPDATE_FW_WEAPON_PICKUPS_DEBUG()
	INT i
	TEXT_LABEL_63 str
	FM_WEAPON_PICKUP_TYPES eWeaponPickupType
	VECTOR vPos
		
	IF (g_bFMWeaponPickup_ResetEverythingNow)
		
		REMOVE_AND_RESET_ALL_FM_WEAPON_PICKUPS()
		
		INIT_FM_WEAPON_PICKUP_TOTAL_VALUES_FROM_TUNABLES()
		
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
			INIT_FM_WEAPON_PICKUP_SERVER_DATA()
		ENDIF
		
		g_bFMWeaponPickup_ResetEverythingNow = FALSE
	ENDIF
	
	IF (g_bFMWeaponPickup_DeleteAllClientPickupsNow)
		REMOVE_AND_RESET_ALL_FM_WEAPON_PICKUPS()
		g_bFMWeaponPickup_DeleteAllClientPickupsNow = FALSE
	ENDIF
	
	IF (g_bRenderAllFMWeaponPickupLocations_Other)
		
		IF NOT (g_bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			g_bDebugLinesActive = TRUE
		ENDIF
	
		REPEAT FM_WEAPON_PICKUPS_MAX_OTHERS i
			str = "Other "
			str += i
			DRAW_DEBUG_TEXT(str, <<g_PickupSpawnLocations_Other[i].vPos.x, g_PickupSpawnLocations_Other[i].vPos.y, g_PickupSpawnLocations_Other[i].vPos.z + 0.3>>, 255, 0, 0, 255)
			DRAW_DEBUG_SPHERE(g_PickupSpawnLocations_Other[i].vPos, 0.1, 255, 0, 0, 255)
			IF NOT DOES_BLIP_EXIST(g_DebugWeaponBlip_Other[i])
				g_DebugWeaponBlip_Other[i] = ADD_BLIP_FOR_COORD(g_PickupSpawnLocations_Other[i].vPos)
				IF IS_SPAWN_LOCATION_ALREADY_USED_FOR_FM_WEAPON_PICKUP_TYPE(FM_WPT_OTHER, i)
					SET_BLIP_COLOUR(g_DebugWeaponBlip_Other[i], BLIP_COLOUR_GREEN)
				ELSE
					SET_BLIP_COLOUR(g_DebugWeaponBlip_Other[i], BLIP_COLOUR_RED)
				ENDIF
				SET_BLIP_SCALE(g_DebugWeaponBlip_Other[i], 0.4)
			ELSE
				SET_BLIP_COORDS(g_DebugWeaponBlip_Other[i], g_PickupSpawnLocations_Other[i].vPos)
			ENDIF
		ENDREPEAT
		g_bFMWeaponPickupLocations_BlipsCreated_Other = TRUE
	ELSE
		IF (g_bFMWeaponPickupLocations_BlipsCreated_Other)
			REPEAT FM_WEAPON_PICKUPS_MAX_OTHERS i
				IF DOES_BLIP_EXIST(g_DebugWeaponBlip_Other[i])	
					REMOVE_BLIP(g_DebugWeaponBlip_Other[i])
				ENDIF
			ENDREPEAT
			g_bFMWeaponPickupLocations_BlipsCreated_Other = FALSE
		ENDIF
	ENDIF
	
	IF (g_bRenderAllFMWeaponPickupLocations_Melee)
	
		IF NOT (g_bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			g_bDebugLinesActive = TRUE
		ENDIF
		
		REPEAT FM_WEAPON_PICKUPS_MAX_MELEE i
			str = "Melee "
			str += i
			DRAW_DEBUG_TEXT(str, <<g_PickupSpawnLocations_Melee[i].vPos.x, g_PickupSpawnLocations_Melee[i].vPos.y, g_PickupSpawnLocations_Melee[i].vPos.z + 0.3>>, 0, 255, 0, 255)
			DRAW_DEBUG_SPHERE(g_PickupSpawnLocations_Melee[i].vPos, 0.1, 0, 255, 0, 255)
			IF NOT DOES_BLIP_EXIST(g_DebugWeaponBlip_Melee[i])
				g_DebugWeaponBlip_Melee[i] = ADD_BLIP_FOR_COORD(g_PickupSpawnLocations_Melee[i].vPos)
				IF IS_SPAWN_LOCATION_ALREADY_USED_FOR_FM_WEAPON_PICKUP_TYPE(FM_WPT_MELEE, i)
					SET_BLIP_COLOUR(g_DebugWeaponBlip_Melee[i], BLIP_COLOUR_GREEN)
				ELSE
					SET_BLIP_COLOUR(g_DebugWeaponBlip_Melee[i], BLIP_COLOUR_RED)
				ENDIF
				SET_BLIP_SCALE(g_DebugWeaponBlip_Melee[i], 0.4)
			ELSE
				SET_BLIP_COORDS(g_DebugWeaponBlip_Melee[i], g_PickupSpawnLocations_Melee[i].vPos)
			ENDIF
			g_bFMWeaponPickupLocations_BlipsCreated_Melee = TRUE
		ENDREPEAT
	ELSE
		IF (g_bFMWeaponPickupLocations_BlipsCreated_Melee)
			REPEAT FM_WEAPON_PICKUPS_MAX_MELEE i
				IF DOES_BLIP_EXIST(g_DebugWeaponBlip_Melee[i])	
					REMOVE_BLIP(g_DebugWeaponBlip_Melee[i])
				ENDIF
			ENDREPEAT
			g_bFMWeaponPickupLocations_BlipsCreated_Melee = FALSE
		ENDIF		
	ENDIF
		
	IF (g_bRenderAllFMWeaponPickupLocations_Projectile)
		IF NOT (g_bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			g_bDebugLinesActive = TRUE
		ENDIF	
		REPEAT FM_WEAPON_PICKUPS_MAX_PROJECTILE i
			str = "Projectile "
			str += i
			DRAW_DEBUG_TEXT(str, <<g_PickupSpawnLocations_Projectile[i].vPos.x, g_PickupSpawnLocations_Projectile[i].vPos.y, g_PickupSpawnLocations_Projectile[i].vPos.z + 0.3>>, 0, 255, 255, 255)
			DRAW_DEBUG_SPHERE(g_PickupSpawnLocations_Projectile[i].vPos, 0.1, 0, 255, 255, 255)
			IF NOT DOES_BLIP_EXIST(g_DebugWeaponBlip_Projectile[i])
				g_DebugWeaponBlip_Projectile[i] = ADD_BLIP_FOR_COORD(g_PickupSpawnLocations_Projectile[i].vPos)
				IF IS_SPAWN_LOCATION_ALREADY_USED_FOR_FM_WEAPON_PICKUP_TYPE(FM_WPT_PROJECTILE, i)
					SET_BLIP_COLOUR(g_DebugWeaponBlip_Projectile[i], BLIP_COLOUR_GREEN)
				ELSE
					SET_BLIP_COLOUR(g_DebugWeaponBlip_Projectile[i], BLIP_COLOUR_RED)
				ENDIF
				SET_BLIP_SCALE(g_DebugWeaponBlip_Projectile[i], 0.4)
			ELSE
				SET_BLIP_COORDS(g_DebugWeaponBlip_Projectile[i], g_PickupSpawnLocations_Projectile[i].vPos)
			ENDIF
			g_bFMWeaponPickupLocations_BlipsCreated_Projectile = TRUE			
		ENDREPEAT
	ELSE
		IF (g_bFMWeaponPickupLocations_BlipsCreated_Projectile)
			REPEAT FM_WEAPON_PICKUPS_MAX_PROJECTILE i
				IF DOES_BLIP_EXIST(g_DebugWeaponBlip_Projectile[i])	
					REMOVE_BLIP(g_DebugWeaponBlip_Projectile[i])
				ENDIF
			ENDREPEAT
			g_bFMWeaponPickupLocations_BlipsCreated_Projectile = FALSE
		ENDIF		
	ENDIF
		
	IF (g_bRenderAllFMWeaponPickupLocations_Gun)
		IF NOT (g_bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			g_bDebugLinesActive = TRUE
		ENDIF	
		REPEAT FM_WEAPON_PICKUPS_MAX_GUN i
			str = "Gun "
			str += i
			DRAW_DEBUG_TEXT(str, <<g_PickupSpawnLocations_Gun[i].vPos.x, g_PickupSpawnLocations_Gun[i].vPos.y, g_PickupSpawnLocations_Gun[i].vPos.z + 0.3>>, 0, 0, 255, 255)
			DRAW_DEBUG_SPHERE(g_PickupSpawnLocations_Gun[i].vPos, 0.1, 0, 0, 255, 255)
			IF NOT DOES_BLIP_EXIST(g_DebugWeaponBlip_Gun[i])
				g_DebugWeaponBlip_Gun[i] = ADD_BLIP_FOR_COORD(g_PickupSpawnLocations_Gun[i].vPos)
				IF IS_SPAWN_LOCATION_ALREADY_USED_FOR_FM_WEAPON_PICKUP_TYPE(FM_WPT_GUN, i)
					SET_BLIP_COLOUR(g_DebugWeaponBlip_Gun[i], BLIP_COLOUR_GREEN)
				ELSE
					SET_BLIP_COLOUR(g_DebugWeaponBlip_Gun[i], BLIP_COLOUR_RED)
				ENDIF
				SET_BLIP_SCALE(g_DebugWeaponBlip_Gun[i], 0.4)
			ELSE
				SET_BLIP_COORDS(g_DebugWeaponBlip_Gun[i], g_PickupSpawnLocations_Gun[i].vPos)
			ENDIF
			g_bFMWeaponPickupLocations_BlipsCreated_Gun = TRUE					
		ENDREPEAT
	ELSE
		IF (g_bFMWeaponPickupLocations_BlipsCreated_Gun)
			REPEAT FM_WEAPON_PICKUPS_MAX_GUN i
				IF DOES_BLIP_EXIST(g_DebugWeaponBlip_Gun[i])	
					REMOVE_BLIP(g_DebugWeaponBlip_Gun[i])
				ENDIF
			ENDREPEAT
			g_bFMWeaponPickupLocations_BlipsCreated_Gun = FALSE
		ENDIF			
	ENDIF
	
	IF (g_bRenderAllFMWeaponPickupLocations_Active)
		IF NOT (g_bDebugLinesActive)
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			g_bDebugLinesActive = TRUE
		ENDIF	
		REPEAT g_iTotalNumberOfWeaponPickupsToBeCreated i
			
			IF DOES_PICKUP_EXIST(g_FMWeaponPickup[i])
			AND NOT (g_iFMWeaponPickupSpawnLocation[i] = -1)
			
				eWeaponPickupType = GET_FM_WEAPON_PICKUP_TYPE_FROM_NETWORK_PICKUP_INDEX(i)
				vPos = GET_PICKUP_COORDS_FOR_SPAWN_LOCATION(eWeaponPickupType, g_iFMWeaponPickupSpawnLocation[i])
				
				IF NOT DOES_BLIP_EXIST(g_DebugWeaponBlip_Active[i])
					g_DebugWeaponBlip_Active[i] = ADD_BLIP_FOR_COORD(vPos)
					SET_BLIP_SCALE(g_DebugWeaponBlip_Active[i], 0.4)
				ENDIF

				str = "eType "
				str += ENUM_TO_INT(eWeaponPickupType)
				str += ", loc "
				str += g_iFMWeaponPickupSpawnLocation[i]
				str += ", index "
				str += i	
				
//				IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].WeaponPickupData.bClaimingPickup[i]
				IF IS_FM_WEAPON_PICKUP_BITSET_SET(NATIVE_TO_INT(PLAYER_ID()),i)
					DRAW_DEBUG_TEXT(str, <<vPos.x, vPos.y, vPos.z + 0.3>>, 255, 50, 50, 255)
					DRAW_DEBUG_SPHERE(vPos, 0.1, 255, 50, 50, 255)
					IF DOES_BLIP_EXIST(g_DebugWeaponBlip_Active[i])
						SET_BLIP_COLOUR(g_DebugWeaponBlip_Active[i], BLIP_COLOUR_RED)
					ENDIF				
				ELSE
					DRAW_DEBUG_TEXT(str, <<vPos.x, vPos.y, vPos.z + 0.3>>, 50, 255, 50, 255)
					DRAW_DEBUG_SPHERE(vPos, 0.1, 50, 255, 50, 255)
					IF DOES_BLIP_EXIST(g_DebugWeaponBlip_Active[i])
						SET_BLIP_COLOUR(g_DebugWeaponBlip_Active[i], BLIP_COLOUR_GREEN)
					ENDIF	
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(g_DebugWeaponBlip_Active[i])
					REMOVE_BLIP(g_DebugWeaponBlip_Active[i])
				ENDIF
			ENDIF
		ENDREPEAT
		
		g_bFMWeaponPickupLocations_BlipsCreated_Active = TRUE
		
	ELSE
	
		IF (g_bFMWeaponPickupLocations_BlipsCreated_Active)
			REPEAT g_iTotalNumberOfWeaponPickupsToBeCreated i
				IF DOES_BLIP_EXIST(g_DebugWeaponBlip_Active[i])
					REMOVE_BLIP(g_DebugWeaponBlip_Active[i])
				ENDIF
			ENDREPEAT		
			g_bFMWeaponPickupLocations_BlipsCreated_Active = FALSE
		ENDIF
	
	ENDIF

ENDPROC

#ENDIF






