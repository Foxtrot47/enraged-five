//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        net_simeon_showroom_public.sch																		//
// Description: Contains public functions for maintaining Simeons showroom in regards to access, purchaseable		//
//				vehicles and test drive launch. - url:bugstar:7565143												//
// Written by:  Joe Davis																							//
// Date:  		08/03/2022																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "net_include.sch"
USING "rc_helper_functions.sch"
USING "net_blips.sch"
USING "freemode_header.sch"

#IF FEATURE_DLC_1_2022

CONST_INT ciSIMEON_SHOWROOM_ROOM_KEY_MAIN_HALL		-51694438
CONST_INT ciSIMEON_SHOWROOM_ROOM_KEY_OFFICE_RIGHT	1646471835
CONST_INT ciSIMEON_SHOWROOM_ROOM_KEY_OFFICE_LEFT	-1159701476
CONST_INT ciSIMEON_SHOWROOM_ROOM_KEY_GARAGE			-1767991270

/// PURPOSE:
///    Returns the coords of simeons showroom, used for blip and launching. 
FUNC VECTOR GET_SIMEON_SHOWROOM_BLIP_COORDS()
	RETURN <<-42.6786, -1097.2642, 34.1977>>
ENDFUNC

/// PURPOSE:
///    Returns the script name string.
FUNC STRING GET_SIMEON_SHOWROOM_SCRIPT_NAME()
	RETURN "am_mp_simeon_showroom"
ENDFUNC

FUNC STRING GET_SIMEON_SHOWROOM_MENU_TXD()
	RETURN "ShopUI_Title_Premium_Deluxe_Motorsport"
ENDFUNC

/// PURPOSE:
///    Determines if the child script "am_mp_simeon_showroom" should not launch / terminate.
FUNC BOOL SHOULD_TERMINATE_SIMEON_SHOWROOM_SCRIPT()
	IF g_sMPTunables.bDISABLE_SIMEON_SHOWROOM
		PRINTLN("SHOULD_TERMINATE_SIMEON_SHOWROOM_SCRIPT - g_sMPTunables.bDISABLE_SIMEON_SHOWROOM is TRUE")
		RETURN TRUE
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		PRINTLN("SHOULD_TERMINATE_SIMEON_SHOWROOM_SCRIPT - NETWORK_IS_GAME_IN_PROGRESS is FALSE")
		RETURN TRUE
	ENDIF
	
	IF FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_BUSINESS_BATTLES
	AND GB_GET_FMBB_VARIATION_PLAYER_IS_ON(PLAYER_ID()) = ENUM_TO_INT(BBV_SHOWROOM)
		PRINTLN("SHOULD_TERMINATE_SIMEON_SHOWROOM_SCRIPT - Player is in a Business Battle.")
		RETURN TRUE
	ENDIF
	
	IF (IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID()) AND IS_SCREEN_FADED_OUT())
		PRINTLN("SHOULD_TERMINATE_SIMEON_SHOWROOM_SCRIPT - IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR is TRUE")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Stops ambient peds and vehicles from roaming around the seamless interior.
PROC CLEAR_SIMEON_SHOWROOM_OF_PEDS_AND_VEHICLES(BOOL bPeds, BOOL bVehicles)
	FLOAT fRadius 		= 4.0
	VECTOR vLeftArea 	= <<-48.9566, -1095.7876, 25.4223>>
	VECTOR vRightArea 	= <<-39.2317, -1100.3231, 25.4223>>
	VECTOR vGarageArea 	= <<-32.3635, -1090.1152, 25.4222>>
	
	IF bPeds
		CLEAR_AREA_OF_PEDS(vLeftArea, fRadius)
		CLEAR_AREA_OF_PEDS(vRightArea, fRadius)
		CLEAR_AREA_OF_PEDS(vGarageArea, fRadius)
	ENDIF
	
	IF bVehicles
		CLEAR_AREA_OF_VEHICLES(vLeftArea, fRadius)
		CLEAR_AREA_OF_VEHICLES(vRightArea, fRadius)
		CLEAR_AREA_OF_VEHICLES(vGarageArea, fRadius)
	ENDIF
	
	PRINTLN("CLEAR_SIMEON_SHOWROOM_OF_PEDS_AND_VEHICLES - Called.")
ENDPROC

PROC ADD_SIMEON_SHOWROOM_SCENARIO_BLOCKING_AREA(SCENARIO_BLOCKING_INDEX &BlockIndex, BOOL bAdd)

	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	IF NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	VECTOR vMin 		= <<-62.2120, -1110.0520, 25.2403>>
	VECTOR vMax 		= <<-18.2150, -1078.4771, 32.7980>>
	
	IF bAdd
		IF NOT DOES_SCENARIO_BLOCKING_AREA_EXISTS(vMin, vMax)
			BlockIndex = ADD_SCENARIO_BLOCKING_AREA(vMin, vMax, TRUE, DEFAULT, FALSE, TRUE)
		ENDIF
	ELIF NETWORK_GET_NUM_PARTICIPANTS() = 1 // last player on script
		IF DOES_SCENARIO_BLOCKING_AREA_EXISTS(vMin, vMax)
			REMOVE_SCENARIO_BLOCKING_AREA(BlockIndex, TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC MODEL_NAMES GET_SIMEON_SHOWROOM_GARAGE_DOOR_MODEL()
	RETURN INT_TO_ENUM(MODEL_NAMES, HASH("reh_prop_reh_door_gar_02a"))
ENDFUNC

FUNC VECTOR GET_SIMEON_SHOWROOM_GARAGE_DOOR_COORDS()
	RETURN <<-29.47945, -1086.614, 26.955>>
ENDFUNC

FUNC VECTOR GET_SIMEON_SHOWROOM_GARAGE_DOOR_ROTATION()
	RETURN <<0.0000, 0.0000, -20.0000>>
ENDFUNC

#ENDIF // FEATURE_DLC_1_2022
