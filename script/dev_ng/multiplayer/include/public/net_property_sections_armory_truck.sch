///////////////////////////////////////////////////////////////////////////////////
// Name:        net_property_sections_armory_truck.sch                           //
// Description: Description for interior swap of the armory truck				 //
// Written By:  Aleksej Leskin                                    				 //
///////////////////////////////////////////////////////////////////////////////////

USING "model_enums.sch"
USING "commands_streaming.sch"
USING "commands_debug.sch"
USING "commands_entity.sch"
USING "commands_misc.sch"
USING "net_property_sections.sch"
USING "net_sliding_doors_lib.sch"
USING "net_property_sections_lib.sch"
USING "script_conversion.sch"
USING "net_property_sections_armory_truck_data.sch"
USING "stats_enums.sch"
USING "net_include.sch"

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════════╡ DATA ╞══════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//Information about doors, their position and what section they are linked to.
STRUCT ARMORY_TRUCK_DOOR_INFO_STRUCT
	ARMORY_TRUCK_SECTIONS_ENUM eOwnerSection = ATS_INVALID_SECTION
	BOOL bDoorSideA = FALSE
ENDSTRUCT

STRUCT ARMORY_TRUCK_SECTIONS_STRUCT
	PROPERTY_SECTION_STRUCT sSectionsArray[ATS_MAX_SECTIONS]
	INT iActiveSectionsNum = 0
	
	ARMORY_TRUCK_DOOR_INFO_STRUCT sDoorInfoArray[ARMORY_TRUCK_MAX_DOORS]
	SLIDING_DOOR_STRUCT sDoorsArray[ARMORY_TRUCK_MAX_DOORS]
	INT iActiveDoorsNum = 0
	
	OBJECT_INDEX obTV[3]
	OBJECT_INDEX obSeat[9]
	OBJECT_INDEX obComConsole[3]
	OBJECT_INDEX obComMonitor[3]
	OBJECT_INDEX obDoorGeom[2]
	OBJECT_INDEX obTinsel[3]
	
	BOOL bTruckRebuilt = FALSE
ENDSTRUCT

ENUM ARMORY_TRUCK_MONITOR_STATES
	ATP_MONITOR_STATE_DETECT_PROP = 0,
	ATP_MONITOR_STATE_LINK_RT,
	ATP_MONITOR_STATE_UPDATE_SCALEFORM
ENDENUM

STRUCT ARMORY_TRUCK_MONITOR_DATA
	INT iRenderTargetIDs[5]
	PLAYER_INDEX pOwnerID
	SCALEFORM_INDEX sMonitorScaleform
	ARMORY_TRUCK_MONITOR_STATES eMonitorStates = ATP_MONITOR_STATE_DETECT_PROP
ENDSTRUCT

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ FUNCTIONS ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

//╞═════════════════════════════════╡ TIME CYCLE ╞═══════════════════════════════╡

PROC ARMORY_TRUCK_CLEAR_TIMECYCLE()
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR][TCM]ARMORY_TRUCK_CLEAR_TIMECYCLE - CLEAR_TIMECYCLE")
	REMOVE_TCMODIFIER_OVERRIDE("mp_gr_int01_black")
ENDPROC

PROC ARMORY_TRUCK_SET_TIMECYCLE(STRING sNewTimeCycle)
	IF IS_STRING_NULL_OR_EMPTY(sNewTimeCycle)
		ASSERTLN("[AT-INTERIOR]ARMORY_TRUCK_SET_TIMECYCLE - Unable to set a new modifier, passed in emptry")
		EXIT
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]ARMORY_TRUCK_SET_TIMECYCLE - setting new timecycle: ", sNewTimeCycle)
	
	IF NOT DOES_ENTITY_EXIST(PLAYER_PED_ID())
	OR IS_ENTITY_DEAD(PLAYER_PED_ID())
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR][TCM]ARMORY_TRUCK_SET_TIMECYCLE - unable to set timecycle due to ped state, Exists: ", DOES_ENTITY_EXIST(PLAYER_PED_ID()), " Dead: ", IS_ENTITY_DEAD(PLAYER_PED_ID()))
		EXIT
	ENDIF
	
	IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = GET_HASH_KEY("GtaMloRoom001")
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR][TCM]ARMORY_TRUCK_SET_TIMECYCLE - already inside the room, dont set the modifier!")
	ELSE
		ADD_TCMODIFIER_OVERRIDE("mp_gr_int01_black", sNewTimeCycle)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR][TCM]ARMORY_TRUCK_SET_TIMECYCLE - not inside the room yet, set the timecycle.")
	ENDIF
ENDPROC

//╞═════════════════════════════════╡ LIGHT RIG ╞═══════════════════════════════╡
//light rig is stored on section cleaned up by section cleanup.

FUNC BOOL SET_ARMORY_TRUCK_SECTION_LIGHTRIG(PROPERTY_SECTION_STRUCT &sSection, MODEL_NAMES newLightRigName)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_LIGHTRIG - Setting new lightrig, name: ", GET_MODEL_NAME_FOR_DEBUG(newLightRigName), " SectionID: ", sSection.iSectionID, " SectionType: ", sSection.iSectionType)
	IF DOES_ENTITY_EXIST(sSection.lightRig)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_LIGHTRIG - old light rig deleted.")
		DELETE_ENTITY(sSection.lightRig)
	ENDIF
	
	sSection.lightRig = CREATE_OBJECT(newLightRigName, sSection.vPosition, FALSE)
	IF DOES_ENTITY_EXIST(sSection.lightRig)
		SET_ENTITY_COORDS_NO_OFFSET(sSection.lightRig, sSection.vPosition)				
		FREEZE_ENTITY_POSITION(sSection.lightRig, TRUE)
		SET_ENTITY_COLLISION(sSection.lightRig, FALSE)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_LIGHTRIG - Lightrig Created at: ", sSection.vPosition, " Frozen , collision disabled.")
	ENDIF
	IF DOES_ENTITY_EXIST(sSection.lightRig)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_LIGHTRIG - Lightrig updated.")
		RETURN TRUE
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_LIGHTRIG - Was unable to update light rig.")
	RETURN FALSE
ENDFUNC

//╞═════════════════════════════════╡ STATS ╞═══════════════════════════════╡

FUNC BOOL GET_ARMORY_TRUCK_SECTION_STAT(ARMORY_TRUCK_SECTIONS_ENUM eSection, ARMORY_TRUCK_STATS eStat, STATS_PACKED& OutStatName)
	BOOL bReturn = TRUE

	SWITCH eSection
		CASE ATS_FIRST_SECTION
			SWITCH eStat
				CASE AT_STAT_SECTION_TYPE 
					OutStatName = PACKED_MP_INT_TRUCK_SECTION_1_TYPE
				BREAK
				CASE AT_STAT_SECTION_TINT 
					OutStatName = PACKED_MP_INT_TRUCK_SECTION_1_NUM
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ATS_SECOND_SECTION
			SWITCH eStat
				CASE AT_STAT_SECTION_TYPE
					OutStatName = PACKED_MP_INT_TRUCK_SECTION_2_TYPE
				BREAK
				CASE AT_STAT_SECTION_TINT
					OutStatName = PACKED_MP_INT_TRUCK_SECTION_1_NUM
				BREAK
			ENDSWITCH
		BREAK
		
		CASE ATS_THIRD_SECTION
			SWITCH eStat
				CASE AT_STAT_SECTION_TYPE
					OutStatName = PACKED_MP_INT_TRUCK_SECTION_3_TYPE
				BREAK
				CASE AT_STAT_SECTION_TINT
					OutStatName = PACKED_MP_INT_TRUCK_SECTION_1_NUM
				BREAK
			ENDSWITCH
		BREAK
		
		DEFAULT 
			bReturn = FALSE
		BREAK 
	ENDSWITCH
	
	RETURN bReturn
ENDFUNC

//////////
/// PURPOSE:
///    Check if a section exists in players stats, passes back section position
/// PARAMS:
///    eSectionToFind - 
///    iOutSectionPos - Position of the section (0-2)
FUNC BOOL DOES_ARMORY_TRUCK_SECTION_EXIST(ARMORY_TRUCK_SECTION_TYPE_ENUM eSectionToFind, ARMORY_TRUCK_SECTIONS_ENUM &iOutSectionPos)
	STATS_PACKED eStat
	INT iSection
	
	REPEAT ATS_MAX_SECTIONS iSection
		IF GET_ARMORY_TRUCK_SECTION_STAT(INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, iSection), AT_STAT_SECTION_TYPE, eStat)
			IF GET_PACKED_STAT_INT(eStat) = ENUM_TO_INT(eSectionToFind)
				iOutSectionPos = INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, iSection)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the player stat. Truck serction position is assigned with a new section type.
FUNC BOOL SET_ARMORY_TRUCK_SECTION_TYPE_STAT(ARMORY_TRUCK_SECTIONS_ENUM eSectionPos, ARMORY_TRUCK_SECTION_TYPE_ENUM eNewSectionType)
	STATS_PACKED eStat
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-STAT][AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_STAT")
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-STAT][AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_STAT - eSectionPos: ", ENUM_TO_INT(eSectionPos), " eNewSectionType: ", GET_ARMORY_TRUCK_SECTION_TYPE_NAME(eNewSectionType), " ID: ", ENUM_TO_INT(eNewSectionType))
	IF GET_ARMORY_TRUCK_SECTION_STAT(INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, eSectionPos), AT_STAT_SECTION_TYPE, eStat)
		SET_PACKED_STAT_INT(eStat, ENUM_TO_INT(eNewSectionType))
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-STAT][AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_STAT - stat set: ", GET_PACKED_STAT_INT(eStat))
		SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_SECTIONS_REFRESH_REQUIRED)
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-STAT][AT-INTERIOR] - Summary, One: ", GET_PACKED_STAT_INT(PACKED_MP_INT_TRUCK_SECTION_1_TYPE), " Two: ", GET_PACKED_STAT_INT(PACKED_MP_INT_TRUCK_SECTION_2_TYPE), " Three: ", GET_PACKED_STAT_INT(PACKED_MP_INT_TRUCK_SECTION_3_TYPE))
		
		BROADCAST_PLAYER_TRUCK_PURCHASED_SECTIONS()
		RETURN TRUE
	ELSE
		ASSERTLN("[AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_STAT - Was unable to update stat, SectionPos: ", ENUM_TO_INT(eSectionPos),  " eNewSectionType: ", GET_ARMORY_TRUCK_SECTION_TYPE_NAME(eNewSectionType))
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sets the player stat. Truck serction position is assigned with a new section type.
FUNC ARMORY_TRUCK_SECTION_TYPE_ENUM GET_ARMORY_TRUCK_SECTION_TYPE_STAT(ARMORY_TRUCK_SECTIONS_ENUM eSectionPos)
	STATS_PACKED eStat
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]GET_ARMORY_TRUCK_SECTION_TYPE_STAT - eSectionPos: ", GET_ARMORY_TRUCK_SECTION_NAME(eSectionPos))
	IF GET_ARMORY_TRUCK_SECTION_STAT(eSectionPos, AT_STAT_SECTION_TYPE, eStat)
		RETURN INT_TO_ENUM(ARMORY_TRUCK_SECTION_TYPE_ENUM, GET_PACKED_STAT_INT(eStat))
	ELSE
		ASSERTLN("[AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_STAT - Was unable to get section type stat for: ", GET_ARMORY_TRUCK_SECTION_NAME(eSectionPos))
	ENDIF
	
	RETURN AT_ST_UNDEFINED
ENDFUNC

/// PURPOSE:
///    Update Tint Stat, does not auto update the interior.
FUNC BOOL SET_ARMORY_TRUCK_SECTION_TINT_STAT(ARMORY_TRUCK_SECTIONS_ENUM eSectionPos, ARMORY_TRUCK_SECTION_TINT_ENUM eNewTintIndex)
	STATS_PACKED eStat
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-STAT][AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_TINT_STAT - eSectionPos: ", ENUM_TO_INT(eSectionPos), " eNewTintIndex: ", GET_ARMORY_TRUCK_SECTION_TINT_NAME(eNewTintIndex))
	IF GET_ARMORY_TRUCK_SECTION_STAT(eSectionPos, AT_STAT_SECTION_TINT, eStat)
		SET_PACKED_STAT_INT(eStat, ENUM_TO_INT(eNewTintIndex))
		SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESH_REQUIRED)
		BROADCAST_PLAYER_TRUCK_PURCHASED_SECTION_TINT()
		RETURN TRUE
	ELSE
		ASSERTLN("[AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_TINT_STAT - Was unable to update stat, SectionPos: ", ENUM_TO_INT(eSectionPos),  " eNewTintIndex: ", GET_ARMORY_TRUCK_SECTION_TINT_NAME(eNewTintIndex))
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Update Tint Stat, does not auto update the interior.
FUNC BOOL SET_ARMORY_TRUCK_TINT_STAT(ARMORY_TRUCK_SECTION_TINT_ENUM eNewTintIndex)
	STATS_PACKED eStat
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-STAT][AT-INTERIOR]SET_ARMORY_TRUCK_TINT_STAT - Updating truck overall interior colour: ", GET_ARMORY_TRUCK_SECTION_TINT_NAME(eNewTintIndex)) 
	IF GET_ARMORY_TRUCK_SECTION_STAT(ATS_FIRST_SECTION, AT_STAT_SECTION_TINT, eStat)
		SET_PACKED_STAT_INT(eStat, ENUM_TO_INT(eNewTintIndex))
		SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESH_REQUIRED)
		BROADCAST_PLAYER_TRUCK_PURCHASED_SECTION_TINT()
	ELSE
		ASSERTLN("[AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_TINT_STAT - Was unable to update stat, SectionPos1:  eNewTintIndex: ", GET_ARMORY_TRUCK_SECTION_TINT_NAME(eNewTintIndex))
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Get Tint Stat
FUNC ARMORY_TRUCK_SECTION_TINT_ENUM GET_ARMORY_TRUCK_SECTION_TINT_STAT(ARMORY_TRUCK_SECTIONS_ENUM eSectionPos)
	ARMORY_TRUCK_SECTION_TINT_ENUM eOutTintIndex = AT_STI_INVALID_INDEX
	STATS_PACKED eStat
	
	//CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_ARMORY_TRUCK_SECTION_TINT_STAT - eSectionPos: ", GET_ARMORY_TRUCK_SECTION_NAME(eSectionPos))
	IF GET_ARMORY_TRUCK_SECTION_STAT(eSectionPos, AT_STAT_SECTION_TINT, eStat)
		eOutTintIndex = INT_TO_ENUM(ARMORY_TRUCK_SECTION_TINT_ENUM, GET_PACKED_STAT_INT(eStat))
		RETURN eOutTintIndex
	ELSE
		ASSERTLN("[AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_TINT_STAT - Was unable to get stat, SectionPos: ", GET_ARMORY_TRUCK_SECTION_NAME(eSectionPos))
	ENDIF
	
	RETURN eOutTintIndex
ENDFUNC

/// PURPOSE:
///    Get the section taht is at a iSectionPos Position (0-2)
/// PARAMS:
///    eNewSectionType - 
///    iSectionPos - 
/// RETURNS: True if valid position was passed
///    
FUNC ARMORY_TRUCK_SECTION_TYPE_ENUM GET_ARMORY_TRUCK_SECTION_TYPE_FROM_POS(ARMORY_TRUCK_SECTIONS_ENUM eSectionPos)
	IF eSectionPos = ATS_FIRST_SECTION
		RETURN INT_TO_ENUM(ARMORY_TRUCK_SECTION_TYPE_ENUM, GET_PACKED_STAT_INT(PACKED_MP_INT_TRUCK_SECTION_1_TYPE))
	ELIF eSectionPos = ATS_SECOND_SECTION
		RETURN INT_TO_ENUM(ARMORY_TRUCK_SECTION_TYPE_ENUM, GET_PACKED_STAT_INT(PACKED_MP_INT_TRUCK_SECTION_2_TYPE))
	ELIF eSectionPos = ATS_THIRD_SECTION
		RETURN INT_TO_ENUM(ARMORY_TRUCK_SECTION_TYPE_ENUM, GET_PACKED_STAT_INT(PACKED_MP_INT_TRUCK_SECTION_3_TYPE))
	ENDIF
	
	RETURN AT_ST_UNDEFINED
ENDFUNC
//╞═════════════════════════════════╡ STATS END ╞═══════════════════════════════╡
 
//╞═════════════════════════════════╡ POSITIONS ╞═══════════════════════════════╡
/// PURPOSE:
///    Get all anchor pitns availible for a specific interior.
/// PARAMS:
///    eSimpleInterior - which interior
///    sAnchorPoints - Array to hold all the anchors
///    ActiveAnchorPoints - Number of anchors added to array.
///
FUNC BOOL GET_ARMORY_TRUCK_INTERIOR_ANCHOR_POINTS(SIMPLE_INTERIORS eSimpleInterior, ANCHOR_POINT_STRUCT &sAnchorPoints[], INT &ActiveAnchorPoints)
	SWITCH eSimpleInterior
		CASE SIMPLE_INTERIOR_ARMORY_TRUCK_1
			CLEAR_INTERIOR_ANCHORS(sAnchorPoints, ActiveAnchorPoints)
			MP_PROP_OFFSET_STRUCT sSectionCoords
			
			GET_ARMORY_TRUCK_SECTION_COORDS(ATS_FIRST_SECTION, sSectionCoords)
			_ADD_INTERIOR_ANCHOR_(sAnchorPoints, ActiveAnchorPoints, sSectionCoords.vLoc, INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_IntTruck_Anchor")))
			
			GET_ARMORY_TRUCK_SECTION_COORDS(ATS_SECOND_SECTION, sSectionCoords)
			_ADD_INTERIOR_ANCHOR_(sAnchorPoints, ActiveAnchorPoints, sSectionCoords.vLoc, INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_IntTruck_Anchor")))
			
			GET_ARMORY_TRUCK_SECTION_COORDS(ATS_THIRD_SECTION, sSectionCoords)
			_ADD_INTERIOR_ANCHOR_(sAnchorPoints, ActiveAnchorPoints, sSectionCoords.vLoc, INT_TO_ENUM(MODEL_NAMES, HASH("gr_Prop_IntTruck_Anchor")))
		RETURN TRUE
		
		DEFAULT
			ASSERTLN("[AT-INTERIOR]GET_INTERIOR_ANCHOR_POINTS_ARMORY_TRUCK - Unable to get anchor points for this Interior ID: ", ENUM_TO_INT(eSimpleInterior))
		RETURN FALSE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Get door locations based on the section.
/// PARAMS:
///    Desc - door positions returned.
/// RETURNS:
///    
FUNC BOOL GET_ARMORY_TRUCK_SECTION_DOOR_COORDS(PROPERTY_SECTION_STRUCT sPropertySection, SLIDING_DOOR_DESCRIPTION_STRUCT &sDoorDescription, BOOL bEntryDoor = TRUE)
	SWITCH INT_TO_ENUM(ARMORY_TRUCK_SECTION_TYPE_ENUM, sPropertySection.iSectionType)
		CASE AT_ST_UNDEFINED
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]GET_SECTION_DOOR_TRANSFORM - Can not get section door coords for : AT_STUNDEFINED")
		RETURN FALSE
		DEFAULT
			MP_PROP_OFFSET_STRUCT sPropTransform
			
			IF sPropertySection.iSectionID = ENUM_TO_INT(ATS_FIRST_SECTION)
			AND sPropertySection.iSectionType != ENUM_TO_INT(AT_ST_LIVING_ROOM)
				IF sDoorDescription.bStatic
					GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_DOOR_BLOCKER, INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, sPropertySection.iSectionID), sPropTransform)
					sDoorDescription.vOpenedPosition = sPropTransform.vLoc
					sDoorDescription.vClosedPosition = sPropTransform.vLoc
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF bEntryDoor
				GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_SECTION_ENTRY_DOOR_OPENED, INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, sPropertySection.iSectionID), sPropTransform)
				sDoorDescription.vOpenedPosition = sPropTransform.vLoc
				GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_SECTION_ENTRY_DOOR_CLOSED, INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, sPropertySection.iSectionID), sPropTransform)
				sDoorDescription.vClosedPosition = sPropTransform.vLoc
			ELSE
				GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_SECTION_EXIT_DOOR_OPENED, INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, sPropertySection.iSectionID), sPropTransform)
				sDoorDescription.vOpenedPosition = sPropTransform.vLoc
				GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_SECTION_EXIT_DOOR_CLOSED, INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, sPropertySection.iSectionID), sPropTransform)
				sDoorDescription.vClosedPosition = sPropTransform.vLoc
			ENDIF
			
			//CDEBUG1LN(DEBUG_SAFEHOUSE, "GET_SECTION_DOOR_TRANSFORM - vDoorOpenedPosition: ", Desc.vOpenedPosition, " vDoorClosedPosition: ", Desc.vClosedPosition)
		RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC FLOAT GET_CLOSEST_ARMORY_TRUCK_WARDROBE(PLAYER_INDEX pOwner, ARMORY_TRUCK_SECTIONS_ENUM &eSection)
	FLOAT fDistance = 1000.0
	MP_PROP_OFFSET_STRUCT tempOffset
	ARMORY_TRUCK_SECTIONS_ENUM iSection
	
	INT i
	REPEAT 3 i
		iSection = INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, i)
		
		IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(iSection, pOwner) = AT_ST_LIVING_ROOM
		OR GET_PLAYER_TRUCK_PURCHASED_SECTIONS(iSection, pOwner) = AT_ST_COMMAND_CENTER
			GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_SECTION_WARDROBE, iSection, tempOffset)
			
			FLOAT fTemp = GET_DISTANCE_BETWEEN_COORDS(tempOffset.vLoc, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
			
			IF fTemp < fDistance
				fDistance = fTemp
				eSection = iSection
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN fDistance
ENDFUNC

//╞═════════════════════════════════╡ POSITIONS END ╞═══════════════════════════════╡

/// PURPOSE:
///    Populate every truck section with a door between the sections.
/// PARAMS:
///    iActiveDoorsNum - how many doors were created
PROC ADD_ARMORY_TRUCK_SLIDING_DOORS(ARMORY_TRUCK_SECTIONS_STRUCT& sTruckSections)
	SLIDING_DOOR_DESCRIPTION_STRUCT sDoorDescription
	
	//Custom door description
	sDoorDescription.fInteractRadius = 1.9 // Ped interaction radius with the door.
	sDoorDescription.audioSoundSetDoorOpen = "DLC_GR_Sliding_Door_Sounds"
	sDoorDescription.audioSoundSetDoorClose = "DLC_GR_Sliding_Door_Sounds"
	sDoorDescription.audioSoundDoorOpen = "Open"
	sDoorDescription.audioSoundDoorClose = "Close"
	sDoorDescription.fDoorSpeed = 2.5
	//Grab Transform , spawn door
	INT iSectionID
	REPEAT sTruckSections.iActiveSectionsNum iSectionID
		//First section should have a static entry door.
		IF iSectionID = ENUM_TO_INT(ATS_FIRST_SECTION)
			IF sTruckSections.sSectionsArray[iSectionID].iSectionType != ENUM_TO_INT(AT_ST_LIVING_ROOM)
				sDoorDescription.Model = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_inttruck_doorblocker"))
			ELSE
				sDoorDescription.Model = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_inttruck_door_01"))
			ENDIF
			
			sDoorDescription.bStatic = TRUE
		ELSE
			sDoorDescription.Model = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_inttruck_door_01"))
			sDoorDescription.bStatic = FALSE
		ENDIF
		
		IF GET_ARMORY_TRUCK_SECTION_DOOR_COORDS(sTruckSections.sSectionsArray[iSectionID], sDoorDescription)
			sTruckSections.sDoorInfoArray[sTruckSections.iActiveDoorsNum].bDoorSideA = TRUE
			sTruckSections.sDoorInfoArray[sTruckSections.iActiveDoorsNum].eOwnerSection = INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, iSectionID)
			ADD_SLIDING_DOOR(sTruckSections.sDoorsArray, sTruckSections.iActiveDoorsNum, sDoorDescription)
		ENDIF
	ENDREPEAT
	
	//Check if last section needs a door.
	SWITCH INT_TO_ENUM(ARMORY_TRUCK_SECTION_TYPE_ENUM, sTruckSections.sSectionsArray[ATS_THIRD_SECTION].iSectionType)
		CASE AT_ST_LIVING_ROOM
		CASE AT_ST_COMMAND_CENTER
		CASE AT_ST_GUNMOD
			sDoorDescription.bStatic = TRUE
			IF GET_ARMORY_TRUCK_SECTION_DOOR_COORDS(sTruckSections.sSectionsArray[ATS_THIRD_SECTION], sDoorDescription, FALSE)
				sTruckSections.sDoorInfoArray[sTruckSections.iActiveDoorsNum].bDoorSideA = FALSE
				sTruckSections.sDoorInfoArray[sTruckSections.iActiveDoorsNum].eOwnerSection = ATS_THIRD_SECTION
				ADD_SLIDING_DOOR(sTruckSections.sDoorsArray, sTruckSections.iActiveDoorsNum, sDoorDescription)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC REPOSITION_TRUCK_DOOR_BLOCKER_COLLISION_GEOMETRY(ARMORY_TRUCK_SECTION_TYPE_ENUM eSectionType, ARMORY_TRUCK_SECTIONS_STRUCT& sTruckSections, ARMORY_TRUCK_SECTIONS_ENUM eTruckSection, INT iIndex)
	MP_PROP_OFFSET_STRUCT sPropPosition
	MODEL_NAMES eModel
	
	IF eSectionType = AT_ST_LIVING_ROOM
		GET_ARMORY_TRUCK_PROP_TRANSFORM((ATP_LIVING_QUARTERS_BLOCK_0 + iIndex), eTruckSection, sPropPosition)
		eModel = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_inttruck_doorblocker"))
	ELSE
		CASSERTLN(DEBUG_SAFEHOUSE, "[AT_INTERIOR] REPOSITION_TRUCK_DOOR_BLOCKER_COLLISION_GEOMETRY - Passed invalid truck section.")
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(sTruckSections.obDoorGeom[iIndex])
		PRINTLN("[AT_INTERIOR] REPOSITION_TRUCK_DOOR_BLOCKER_COLLISION_GEOMETRY - Creating door blocker collision geometry - ", iIndex)
		
		sTruckSections.obDoorGeom[iIndex] = CREATE_OBJECT(eModel, sPropPosition.vLoc, FALSE)
		
		SET_ENTITY_COORDS(sTruckSections.obDoorGeom[iIndex], sPropPosition.vLoc)
		SET_ENTITY_ROTATION(sTruckSections.obDoorGeom[iIndex], sPropPosition.vRot)
		SET_ENTITY_VISIBLE(sTruckSections.obDoorGeom[iIndex], FALSE)
		FREEZE_ENTITY_POSITION(sTruckSections.obDoorGeom[iIndex], TRUE)
	ENDIF
ENDPROC

PROC UPDATE_ARMORY_TRUCK_DOOR_BLOCKER_COLLISION_GEOMETRY(ARMORY_TRUCK_SECTIONS_STRUCT &sTruckSections, PLAYER_INDEX piOwner)
	IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, piOwner) = AT_ST_LIVING_ROOM
		REPOSITION_TRUCK_DOOR_BLOCKER_COLLISION_GEOMETRY(AT_ST_LIVING_ROOM, sTruckSections, ATS_FIRST_SECTION, 0)
	ELSE
		IF DOES_ENTITY_EXIST(sTruckSections.obDoorGeom[0])
			DELETE_OBJECT(sTruckSections.obDoorGeom[0])
		ENDIF
	ENDIF
	
	IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, piOwner) = AT_ST_LIVING_ROOM
		REPOSITION_TRUCK_DOOR_BLOCKER_COLLISION_GEOMETRY(AT_ST_LIVING_ROOM, sTruckSections, ATS_THIRD_SECTION, 1)
	ELSE
		IF DOES_ENTITY_EXIST(sTruckSections.obDoorGeom[1])
			DELETE_OBJECT(sTruckSections.obDoorGeom[1])
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates the touch screen TVs used for launching missions if they don't exist
///    If they do exist then it just moves them
/// PARAMS:
///    eSectionType - Which truck section to create the TV in
///    oiTV			- Object index
PROC REPOSITION_ARMORY_TRUCK_TV(ARMORY_TRUCK_SECTION_TYPE_ENUM eSectionType, OBJECT_INDEX &oiTV, ARMORY_TRUCK_SECTIONS_ENUM eTruckSection)
	
	MP_PROP_OFFSET_STRUCT sPropPosition
	MODEL_NAMES eModel
		
	IF eSectionType = AT_ST_COMMAND_CENTER
		GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_TV_1, eTruckSection, sPropPosition)
		eModel = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_trailer_tv_02"))
		PRINTLN("[AT-INTERIOR]REPOSITION_ARMORY_TRUCK_TV - Creating comunication centre TV at coords: ", sPropPosition.vLoc, " eTruckSection = ", eTruckSection)
	ELIF eSectionType = AT_ST_LIVING_ROOM
		GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_TV_2, eTruckSection, sPropPosition)
		eModel = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_trailer_tv"))
		PRINTLN("[AT-INTERIOR]REPOSITION_ARMORY_TRUCK_TV - Creating living area TV at coords: ", sPropPosition.vLoc, " eTruckSection = ", eTruckSection)
	ELSE
		CASSERTLN(DEBUG_SAFEHOUSE, "REPOSITION_ARMORY_TRUCK_TV passed invalid truck section to create tv prop")
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(oiTV)
		oiTV = CREATE_OBJECT_NO_OFFSET(eModel, sPropPosition.vLoc, FALSE)
		PRINTLN("[AT-INTERIOR]REPOSITION_ARMORY_TRUCK_TV - Created tv")
	ELSE
		SET_ENTITY_COORDS_NO_OFFSET(oiTV, sPropPosition.vLoc)
		PRINTLN("[AT-INTERIOR]REPOSITION_ARMORY_TRUCK_TV - Repositioned existing TV object for section: ", eTruckSection)
	ENDIF
ENDPROC

PROC UPDATE_ARMOURY_TRUCK_TVS(ARMORY_TRUCK_SECTIONS_STRUCT &sTruckSections, PLAYER_INDEX piOwner)
	
	ARMORY_TRUCK_SECTIONS_ENUM eTruckSection
	INT i
	
	IF DOES_ENTITY_EXIST(sTruckSections.obTV[0])
		DELETE_OBJECT(sTruckSections.obTV[0])
	ENDIF
	
	IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, piOwner) = AT_ST_COMMAND_CENTER
		REPOSITION_ARMORY_TRUCK_TV(AT_ST_COMMAND_CENTER, sTruckSections.obTV[0], eTruckSection)
	ENDIF
	
	REPEAT ATS_MAX_SECTIONS i
		IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, i), piOwner) = AT_ST_LIVING_ROOM
			REPOSITION_ARMORY_TRUCK_TV(AT_ST_LIVING_ROOM, sTruckSections.obTV[i], INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, i))
		ELSE
			IF DOES_ENTITY_EXIST(sTruckSections.obTV[i])
			AND i != 0
				//We will always have 1 tv if not the living area then the comand centre in section 1
				DELETE_OBJECT(sTruckSections.obTV[i])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC REPOSITION_ARMORY_TRUCK_TINSELS(ARMORY_TRUCK_SECTION_TYPE_ENUM eSectionType, OBJECT_INDEX &oiTinsels, ARMORY_TRUCK_SECTIONS_ENUM eTruckSection)
	
	MP_PROP_OFFSET_STRUCT sPropPosition
	MODEL_NAMES eModel
	
	SWITCH 	eSectionType
		
	CASE AT_ST_COMMAND_CENTER
		GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_TINSEL_TRUCK_COMMAD, eTruckSection, sPropPosition)
		eModel = INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Truck_Command"))
		PRINTLN("[AT-INTERIOR]REPOSITION_ARMORY_TRUCK_TINSELS - Creating command tinsel at coords: ", sPropPosition.vLoc, " eTruckSection = ", eTruckSection)
	BREAK
	CASE AT_ST_LIVING_ROOM
		GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_TINSEL_TRUCK_LIVING, eTruckSection, sPropPosition)
		eModel = INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Truck_Living"))
		PRINTLN("[AT-INTERIOR]REPOSITION_ARMORY_TRUCK_TINSELS - Creating living tinsel at coords: ", sPropPosition.vLoc, " eTruckSection = ", eTruckSection)
	BREAK
	CASE AT_ST_GUNMOD
		GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_TINSEL_TRUCK_GUNMOD, eTruckSection, sPropPosition)
		eModel = INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Truck_GunMod"))
		PRINTLN("[AT-INTERIOR]REPOSITION_ARMORY_TRUCK_TINSELS - Creating gunmod tinsel at coords: ", sPropPosition.vLoc, " eTruckSection = ", eTruckSection)
	BREAK
	CASE AT_ST_CARMOD
		GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_TINSEL_TRUCK_CARMOD, eTruckSection, sPropPosition)
		eModel = INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Truck_CarMod"))
		PRINTLN("[AT-INTERIOR]REPOSITION_ARMORY_TRUCK_TINSELS - Creating carmod tinsel at coords: ", sPropPosition.vLoc, " eTruckSection = ", eTruckSection)
	BREAK
	CASE AT_ST_EMPTY_DOUBLE
	CASE AT_ST_EMPTY_SINGLE
		GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_TINSEL_TRUCK_DOUBLE_EMPTY, eTruckSection, sPropPosition)
		eModel = INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Truck_GunMod"))
		PRINTLN("[AT-INTERIOR]REPOSITION_ARMORY_TRUCK_TINSELS - Creating main tinsel for double empty section at coords: ", sPropPosition.vLoc, " eTruckSection = ", eTruckSection)
	BREAK
	DEFAULT 
		GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_TINSEL_TRUCK_MAIN, eTruckSection, sPropPosition)
		eModel = INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Truck_Main"))
		PRINTLN("[AT-INTERIOR]REPOSITION_ARMORY_TRUCK_TINSELS - Creating main tinsel at coords: ", sPropPosition.vLoc, " eTruckSection = ", eTruckSection)
	BREAK
	ENDSWITCH
	IF NOT DOES_ENTITY_EXIST(oiTinsels)
		oiTinsels = CREATE_OBJECT_NO_OFFSET(eModel, sPropPosition.vLoc, FALSE)
		PRINTLN("[AT-INTERIOR]REPOSITION_ARMORY_TRUCK_TINSELS - Created tinsel")
	ELSE
		SET_ENTITY_COORDS_NO_OFFSET(oiTinsels, sPropPosition.vLoc)
		PRINTLN("[AT-INTERIOR]REPOSITION_ARMORY_TRUCK_TINSELS - Repositioned existing tinsel object for section: ", eTruckSection)
	ENDIF
ENDPROC

PROC UPDATE_ARMOURY_TRUCK_TINSELS(ARMORY_TRUCK_SECTIONS_STRUCT &sTruckSections, PLAYER_INDEX piOwner)
	IF g_sMPTunables.bEnable_XMAS2017_Vehicle_Tinsel
		IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, piOwner) = AT_ST_COMMAND_CENTER
			REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_COMMAND_CENTER, sTruckSections.obTinsel[0], ATS_FIRST_SECTION)
		ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, piOwner) = AT_ST_LIVING_ROOM	
			REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_LIVING_ROOM, sTruckSections.obTinsel[0], ATS_FIRST_SECTION)
		ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, piOwner) = AT_ST_LIVING_ROOM	
			REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_LIVING_ROOM, sTruckSections.obTinsel[0], ATS_FIRST_SECTION)
		ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, piOwner) = AT_ST_GUNMOD	
			REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_GUNMOD, sTruckSections.obTinsel[0], ATS_FIRST_SECTION)	
		ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, piOwner) = AT_ST_CARMOD
			REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_CARMOD, sTruckSections.obTinsel[0], ATS_FIRST_SECTION)	
		ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, piOwner) = AT_ST_EMPTY_SINGLE
			REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_EMPTY_SINGLE, sTruckSections.obTinsel[0], ATS_FIRST_SECTION)	
		ENDIF
		
		IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, piOwner) = AT_ST_COMMAND_CENTER
			REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_COMMAND_CENTER, sTruckSections.obTinsel[1], ATS_SECOND_SECTION)
		ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, piOwner) = AT_ST_LIVING_ROOM	
			REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_LIVING_ROOM, sTruckSections.obTinsel[1], ATS_SECOND_SECTION)
		ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, piOwner) = AT_ST_LIVING_ROOM	
			REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_LIVING_ROOM, sTruckSections.obTinsel[1], ATS_SECOND_SECTION)
		ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, piOwner) = AT_ST_GUNMOD	
			REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_GUNMOD, sTruckSections.obTinsel[1], ATS_SECOND_SECTION)	
		ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, piOwner) = AT_ST_CARMOD
			REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_CARMOD, sTruckSections.obTinsel[1], ATS_SECOND_SECTION)	
		ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, piOwner) = AT_ST_EMPTY_SINGLE
			REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_EMPTY_SINGLE, sTruckSections.obTinsel[1], ATS_SECOND_SECTION)	
		ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, piOwner) = AT_ST_EMPTY_DOUBLE
			REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_EMPTY_DOUBLE, sTruckSections.obTinsel[1], ATS_SECOND_SECTION)	
			REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_EMPTY_DOUBLE, sTruckSections.obTinsel[2], ATS_THIRD_SECTION)	
		ENDIF
		
		IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, piOwner) != AT_ST_EMPTY_DOUBLE
		AND GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, piOwner) != AT_ST_CARMOD
			IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, piOwner) = AT_ST_COMMAND_CENTER
				REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_COMMAND_CENTER, sTruckSections.obTinsel[2], ATS_THIRD_SECTION)
			ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, piOwner) = AT_ST_LIVING_ROOM	
				REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_LIVING_ROOM, sTruckSections.obTinsel[2], ATS_THIRD_SECTION)
			ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, piOwner) = AT_ST_LIVING_ROOM	
				REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_LIVING_ROOM, sTruckSections.obTinsel[2], ATS_THIRD_SECTION)
			ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, piOwner) = AT_ST_GUNMOD	
				REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_GUNMOD, sTruckSections.obTinsel[2], ATS_THIRD_SECTION)	
			ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, piOwner) = AT_ST_CARMOD
				REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_CARMOD, sTruckSections.obTinsel[2], ATS_THIRD_SECTION)	
			ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, piOwner) = AT_ST_EMPTY_SINGLE
				REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_EMPTY_SINGLE, sTruckSections.obTinsel[2], ATS_THIRD_SECTION)
			ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, piOwner) = AT_ST_VEHICLE_STORAGE
				REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_VEHICLE_STORAGE, sTruckSections.obTinsel[2], ATS_THIRD_SECTION)
			ELIF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, piOwner) = AT_ST_EMPTY_DOUBLE
				REPOSITION_ARMORY_TRUCK_TINSELS(AT_ST_EMPTY_DOUBLE, sTruckSections.obTinsel[2], ATS_THIRD_SECTION)	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_ARMORY_TRUCK_SEATING(OBJECT_INDEX objIndex)
	IF DOES_ENTITY_EXIST(objIndex)
		DELETE_OBJECT(objIndex)
	ENDIF
ENDPROC

PROC REPOSITION_ARMORY_TRUCK_SEATING(ARMORY_TRUCK_SECTION_TYPE_ENUM eSectionType, OBJECT_INDEX &objIndex, ARMORY_TRUCK_SECTIONS_ENUM eTruckSection, INT iPropID)
	MP_PROP_OFFSET_STRUCT sPropTransform
	MODEL_NAMES eModel
	
	IF eSectionType = AT_ST_LIVING_ROOM
		GET_ARMORY_TRUCK_PROP_TRANSFORM(iPropID, eTruckSection, sPropTransform, eTruckSection, TRUE)
		eModel = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_highendchair_gr_01a"))
		PRINTLN("REPOSITION_ARMORY_TRUCK_SEATING - Creating living area seat at coords: ", sPropTransform.vLoc, " eTruckSection = ", eTruckSection)
	ELSE
		CASSERTLN(DEBUG_SAFEHOUSE, "REPOSITION_ARMORY_TRUCK_SEATING - Passed invalid truck section, cannot create seating prop.")
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(objIndex)
		objIndex = CREATE_OBJECT_NO_OFFSET(eModel, sPropTransform.vLoc, FALSE)
		SET_ENTITY_ROTATION(objIndex, sPropTransform.vRot)	
		FREEZE_ENTITY_POSITION(objIndex, TRUE)
	ELSE
		SET_ENTITY_COORDS_NO_OFFSET(objIndex, sPropTransform.vLoc)
		SET_ENTITY_ROTATION(objIndex, sPropTransform.vRot)
		FREEZE_ENTITY_POSITION(objIndex, TRUE)
	ENDIF
ENDPROC

PROC UPDATE_ARMORY_TRUCK_SEATING(ARMORY_TRUCK_SECTIONS_STRUCT &sTruckSections, PLAYER_INDEX piOwner)

	IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, piOwner) = AT_ST_LIVING_ROOM
		REPOSITION_ARMORY_TRUCK_SEATING(AT_ST_LIVING_ROOM, sTruckSections.obSeat[0], ATS_FIRST_SECTION, 0)
		REPOSITION_ARMORY_TRUCK_SEATING(AT_ST_LIVING_ROOM, sTruckSections.obSeat[1], ATS_FIRST_SECTION, 1)
		REPOSITION_ARMORY_TRUCK_SEATING(AT_ST_LIVING_ROOM, sTruckSections.obSeat[2], ATS_FIRST_SECTION, 2)
	ELSE
		CLEANUP_ARMORY_TRUCK_SEATING(sTruckSections.obSeat[0])
		CLEANUP_ARMORY_TRUCK_SEATING(sTruckSections.obSeat[1])
		CLEANUP_ARMORY_TRUCK_SEATING(sTruckSections.obSeat[2])
	ENDIF
	
	IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, piOwner) = AT_ST_LIVING_ROOM
		REPOSITION_ARMORY_TRUCK_SEATING(AT_ST_LIVING_ROOM, sTruckSections.obSeat[3], ATS_SECOND_SECTION, 3)
		REPOSITION_ARMORY_TRUCK_SEATING(AT_ST_LIVING_ROOM, sTruckSections.obSeat[4], ATS_SECOND_SECTION, 4)
		REPOSITION_ARMORY_TRUCK_SEATING(AT_ST_LIVING_ROOM, sTruckSections.obSeat[5], ATS_SECOND_SECTION, 5)
	ELSE
		CLEANUP_ARMORY_TRUCK_SEATING(sTruckSections.obSeat[3])
		CLEANUP_ARMORY_TRUCK_SEATING(sTruckSections.obSeat[4])
		CLEANUP_ARMORY_TRUCK_SEATING(sTruckSections.obSeat[5])
	ENDIF
	
	IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, piOwner) = AT_ST_LIVING_ROOM
		REPOSITION_ARMORY_TRUCK_SEATING(AT_ST_LIVING_ROOM, sTruckSections.obSeat[6], ATS_THIRD_SECTION, 6)
		REPOSITION_ARMORY_TRUCK_SEATING(AT_ST_LIVING_ROOM, sTruckSections.obSeat[7], ATS_THIRD_SECTION, 7)
		REPOSITION_ARMORY_TRUCK_SEATING(AT_ST_LIVING_ROOM, sTruckSections.obSeat[8], ATS_THIRD_SECTION, 8)
	ELSE
		CLEANUP_ARMORY_TRUCK_SEATING(sTruckSections.obSeat[6])
		CLEANUP_ARMORY_TRUCK_SEATING(sTruckSections.obSeat[7])
		CLEANUP_ARMORY_TRUCK_SEATING(sTruckSections.obSeat[8])
	ENDIF
ENDPROC

PROC REPOSITION_ARMORY_TRUCK_COMMAND_MONITORS(ARMORY_TRUCK_SECTION_TYPE_ENUM eSectionType, OBJECT_INDEX &objIndex, ARMORY_TRUCK_SECTIONS_ENUM eTruckSection, INT iElement)
	MP_PROP_OFFSET_STRUCT sPropPosition
	MODEL_NAMES eModel
	
	IF eSectionType = AT_ST_COMMAND_CENTER
		IF iElement = ATP_MONITOR_1
			GET_ARMORY_TRUCK_PROP_TRANSFORM(iElement, eTruckSection, sPropPosition)
			eModel = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_trailer_monitor_01"))
			PRINTLN("[AT-INTERIOR]REPOSITION_ARMORY_TRUCK_COMMAND_MONITORS - Creating command monitor at coords: ", sPropPosition.vLoc, " sTruckSection = ", eTruckSection)
		ELIF iElement = ATP_MONITOR_2
			GET_ARMORY_TRUCK_PROP_TRANSFORM(iElement, eTruckSection, sPropPosition)
			eModel = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_trailer_monitor_02"))
			PRINTLN("[AT-INTERIOR]REPOSITION_ARMORY_TRUCK_COMMAND_MONITORS - Creating command monitor at coords: ", sPropPosition.vLoc, " sTruckSection = ", eTruckSection)
		ELIF iElement = ATP_MONITOR_3
			GET_ARMORY_TRUCK_PROP_TRANSFORM(iElement, eTruckSection, sPropPosition)
			eModel = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_trailer_monitor_03"))
			PRINTLN("[AT-INTERIOR]REPOSITION_ARMORY_TRUCK_COMMAND_MONITORS - Creating command monitor at coords: ", sPropPosition.vLoc, " sTruckSection = ", eTruckSection)
		ENDIF
	ELSE
		CASSERTLN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]REPOSITION_ARMORY_TRUCK_COMMAND_MONITORS - Passed invalid truck section, cannot create command monitor prop.")
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(objIndex)
		objIndex = CREATE_OBJECT_NO_OFFSET(eModel, sPropPosition.vLoc, FALSE)
	ELSE
		SET_ENTITY_COORDS_NO_OFFSET(objIndex, sPropPosition.vLoc)
	ENDIF
ENDPROC
	
PROC REPOSITION_ARMORY_TRUCT_COMMAND_CONSOLES(ARMORY_TRUCK_SECTION_TYPE_ENUM eSectionType, OBJECT_INDEX &objIndex, ARMORY_TRUCK_SECTIONS_ENUM eTruckSection, INT iElement)
	MP_PROP_OFFSET_STRUCT sPropPosition
	MODEL_NAMES eModel
	
	IF eSectionType = AT_ST_COMMAND_CENTER
		GET_ARMORY_TRUCK_PROP_TRANSFORM(iElement, eTruckSection, sPropPosition)
		eModel = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_console_01"))
		PRINTLN("[AT-INTERIOR]REPOSITION_ARMORY_TRUCT_COMMAND_CONSOLES - Creating command center console at coords: ", sPropPosition.vLoc, " sTruckSection = ", eTruckSection)
	ELSE
		CASSERTLN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]REPOSITION_ARMORY_TRUCT_COMMAND_CONSOLES - Passed invalid truck section, cannot create command console prop.")
		EXIT
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(objIndex)
		objIndex = CREATE_OBJECT_NO_OFFSET(eModel, sPropPosition.vLoc, FALSE)
		SET_ENTITY_ROTATION(objIndex, sPropPosition.vRot)
	ELSE
		SET_ENTITY_COORDS_NO_OFFSET(objIndex, sPropPosition.vLoc)
		SET_ENTITY_ROTATION(objIndex, sPropPosition.vRot)
	ENDIF
ENDPROC

PROC UPDATE_ARMORY_TRUCK_PROPS(ARMORY_TRUCK_SECTIONS_STRUCT &sTruckSections, PLAYER_INDEX piOwner)
	ARMORY_TRUCK_SECTIONS_ENUM eTruckSection 
	
	INT i
	REPEAT ATS_MAX_SECTIONS i
		IF HAS_PLAYER_PURCHASED_SECTION_TYPE_FOR_TRUCK(piOwner, AT_ST_COMMAND_CENTER, eTruckSection)
//			REPOSITION_ARMORY_TRUCT_COMMAND_CONSOLES(AT_ST_COMMAND_CENTER, sTruckSections.obComConsole[i], eTruckSection, (ATP_TURRET_NO_1_CONSOLE + i))
			REPOSITION_ARMORY_TRUCK_COMMAND_MONITORS(AT_ST_COMMAND_CENTER, sTruckSections.obComMonitor[i], eTruckSection, (ATP_MONITOR_1 + i))
		ELSE
			IF DOES_ENTITY_EXIST(sTruckSections.obComConsole[i])
				DELETE_OBJECT(sTruckSections.obComConsole[i])
			ENDIF

			IF DOES_ENTITY_EXIST(sTruckSections.obComMonitor[i])
				DELETE_OBJECT(sTruckSections.obComMonitor[i])
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Set tint for a specific section of the truck.
/// PARAMS:
///    iSection - 
///    bRefreshInterior - 
/// RETURNS:
///    
FUNC BOOL SET_ARMORY_TRUCK_SECTION_TINT(ARMORY_TRUCK_SECTIONS_ENUM eSectionPos, ARMORY_TRUCK_SECTION_TINT_ENUM iTintIndex, ARMORY_TRUCK_SECTION_TYPE_ENUM sSectionType)
	//ARMORY_TRUCK_SECTION_TYPE_ENUM sSectionType
	PROPERTY_SECTION_STRUCT sSectionDesc
	MP_PROP_OFFSET_STRUCT sSectionTransform
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]****************** SET_ARMORY_TRUCK_SECTION_TINT START ******************")
	
	IF NOT GET_ARMORY_TRUCK_SECTION_COORDS(eSectionPos, sSectionTransform)
		ASSERTLN("[AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_TINT - Can not set tint for iSection: ", ENUM_TO_INT(eSectionPos), " Unable to get section position sSectionTransform", ENUM_TO_INT(sSectionType))
		RETURN FALSE
	ENDIF
	
	//sSectionType = GET_ARMORY_TRUCK_SECTION_TYPE_FROM_POS(eSectionPos)
	IF sSectionType = AT_ST_UNDEFINED
		ASSERTLN("[AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_TINT - Can not set tint for iSection: ", ENUM_TO_INT(eSectionPos), " Unable to get section type from isectionPos ", ENUM_TO_INT(sSectionType))
		RETURN FALSE
	ENDIF

	IF NOT GET_ARMORY_TRUCK_SECTION_DESCRIPTION(sSectionType, sSectionDesc)
		ASSERTLN("[AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_TINT - Can not set tint for iSection: ", ENUM_TO_INT(eSectionPos), " Unable to get section description for sSectionType: ", ENUM_TO_INT(sSectionType), " vLoc: ", sSectionDesc.vPosition, " iSecID: ", sSectionDesc.iSectionID, " sType: ", ENUM_TO_INT(sSectionDesc.iSectionType))
		RETURN FALSE
	ENDIF

	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_TINT - TRYING TO SET TINT FOR iSectionPos: ", ENUM_TO_INT(eSectionPos), " iTintIndex: ", ENUM_TO_INT(iTintIndex), " sSectionTransform.vLoc: ", sSectionTransform.vLoc)
	IF SET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(sSectionTransform.vLoc, 10.0, sSectionDesc.mSection, ENUM_TO_INT(iTintIndex))
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_TINT - Tint Update Successfull") 
		RETURN TRUE
	ELSE
		#IF IS_DEBUG_BUILD
		VECTOR vPos
		vPos =  GET_ENTITY_COORDS(PLAYER_PED_ID())
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]SET_ARMORY_TRUCK_SECTION_TINT - Was unable to update the tint, player pos: ", vPos) 
		#ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Get tint index from a model.
/// PARAMS:
///    eSectionPos - 
///    eOutValue - 
/// RETURNS:
///    
FUNC ARMORY_TRUCK_SECTION_TINT_ENUM GET_ARMORY_TRUCK_TINT_INDEX_MODEL(ARMORY_TRUCK_SECTIONS_ENUM eSectionPos, ARMORY_TRUCK_SECTION_TYPE_ENUM sSectionType)
	ARMORY_TRUCK_SECTION_TINT_ENUM eOutValue = AT_STI_INVALID_INDEX
	PROPERTY_SECTION_STRUCT sSectionDesc
	MP_PROP_OFFSET_STRUCT sSectionTransform
		
	IF NOT GET_ARMORY_TRUCK_SECTION_COORDS(eSectionPos, sSectionTransform)
		ASSERTLN("[AT-INTERIOR]_GET_ARMORY_TRUCK_TINT_INDEX - Can not get tint for iSection: ", ENUM_TO_INT(eSectionPos), " Unable to get section position sSectionTransform", ENUM_TO_INT(sSectionType))
		RETURN eOutValue
	ENDIF
	
	IF sSectionType = AT_ST_UNDEFINED
		ASSERTLN("[AT-INTERIOR]_GET_ARMORY_TRUCK_TINT_INDEX - Can not get tint for iSection: ", ENUM_TO_INT(eSectionPos), " Unable to get section type from isectionPos ", ENUM_TO_INT(sSectionType))
		RETURN eOutValue
	ENDIF
	
	IF NOT GET_ARMORY_TRUCK_SECTION_DESCRIPTION(sSectionType, sSectionDesc)
		ASSERTLN("[AT-INTERIOR]_GET_ARMORY_TRUCK_TINT_INDEX - Can not get tint for iSection: ", ENUM_TO_INT(eSectionPos), " Unable to get section description for sSectionType: ", ENUM_TO_INT(sSectionType), " vLoc: ", sSectionDesc.vPosition, " iSecID: ", sSectionDesc.iSectionID, " sType: ", ENUM_TO_INT(sSectionDesc.iSectionType))
		RETURN eOutValue
	ENDIF
	
	//CDEBUG1LN(DEBUG_SAFEHOUSE, "_GET_ARMORY_TRUCK_TINT_INDEX - TRYING TO GET TINT FOR iSectionPos: ", ENUM_TO_INT(eSectionPos), " sSectionTransform.vLoc: ", sSectionTransform.vLoc, " MODEL: ", GET_MODEL_NAME_FOR_DEBUG(sSectionDesc.mSection))
	
	INT iOut
	iOut = GET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(sSectionTransform.vLoc, 10.0, sSectionDesc.mSection)
	
	IF iOut = -1
		ASSERTLN("[AT-INTERIOR]_GET_ARMORY_TRUCK_TINT_INDEX - NO object found: Can not get tint for iSection: ", ENUM_TO_INT(eSectionPos), " Pos: ", sSectionTransform.vLoc, " Model: ", GET_MODEL_NAME_FOR_DEBUG(sSectionDesc.mSection))
		eOutValue =  AT_STI_INVALID_INDEX
	ELSE
		eOutValue = INT_TO_ENUM(ARMORY_TRUCK_SECTION_TINT_ENUM, iOut)
	ENDIF
	
	RETURN eOutValue
ENDFUNC

/// PURPOSE:
///    Simplified to assemble all 3 sections, also used to rebuild the truck sections.
///    
FUNC BOOL CREATE_ARMORY_TRUCK_SECTIONS(SIMPLE_INTERIORS eSimpleInteriorID, ARMORY_TRUCK_SECTIONS_STRUCT& sTruckSections,
	ARMORY_TRUCK_SECTION_TYPE_ENUM eSectionOne = AT_ST_COMMAND_CENTER, ARMORY_TRUCK_SECTION_TYPE_ENUM eSectionTwo = AT_ST_LIVING_ROOM, ARMORY_TRUCK_SECTION_TYPE_ENUM eSectionThree = AT_ST_EMPTY_SINGLE)

	//Store anchors for Truck sections.
	ANCHOR_POINT_STRUCT vAnchorsArray[ATS_MAX_SECTIONS]
	INT iActiveAnchorsNum = 0
	
	//Destroy current sections/doors
	CLEANUP_PROPERTY_SECTIONS(sTruckSections.sSectionsArray, sTruckSections.iActiveSectionsNum)
	DESTROY_SLIDING_DOORS(sTruckSections.sDoorsArray, sTruckSections.iActiveDoorsNum)
	
	IF	GET_ARMORY_TRUCK_INTERIOR_ANCHOR_POINTS(eSimpleInteriorID, vAnchorsArray, iActiveAnchorsNum)
		//Create Property sections
		PRINT_INTERIOR_ANCHORS(vAnchorsArray, iActiveAnchorsNum)
		
		PROPERTY_SECTION_STRUCT sNewPropertySection

		IF eSectionOne <> AT_ST_UNDEFINED
			IF GET_ARMORY_TRUCK_SECTION_DESCRIPTION(eSectionOne, sNewPropertySection)
				sNewPropertySection.iSectionType = ENUM_TO_INT(eSectionOne)
				ADD_PROPERTY_SECTION(sTruckSections.sSectionsArray, sTruckSections.iActiveSectionsNum, vAnchorsArray, iActiveAnchorsNum, sNewPropertySection, ENUM_TO_INT(ATS_FIRST_SECTION))
			ENDIF
		ENDIF
		IF eSectionTwo <> AT_ST_UNDEFINED
			IF GET_ARMORY_TRUCK_SECTION_DESCRIPTION(eSectionTwo, sNewPropertySection)
				sNewPropertySection.iSectionType = ENUM_TO_INT(eSectionTwo)
				ADD_PROPERTY_SECTION(sTruckSections.sSectionsArray, sTruckSections.iActiveSectionsNum, vAnchorsArray, iActiveAnchorsNum, sNewPropertySection, ENUM_TO_INT(ATS_SECOND_SECTION))
			ENDIF
		ENDIF
		IF eSectionThree <> AT_ST_UNDEFINED
			IF GET_ARMORY_TRUCK_SECTION_DESCRIPTION(eSectionThree, sNewPropertySection)
				sNewPropertySection.iSectionType = ENUM_TO_INT(eSectionThree)
				ADD_PROPERTY_SECTION(sTruckSections.sSectionsArray, sTruckSections.iActiveSectionsNum, vAnchorsArray, iActiveAnchorsNum, sNewPropertySection, ENUM_TO_INT(ATS_THIRD_SECTION))
			ENDIF
		ENDIF
		
		PRINT_PROPERTY_SECTIONS(sTruckSections.sSectionsArray, sTruckSections.iActiveSectionsNum)
		CREATE_PROPERTY_SECTIONS(sTruckSections.sSectionsArray, sTruckSections.iActiveSectionsNum)
		
		ADD_ARMORY_TRUCK_SLIDING_DOORS(sTruckSections)
		CREATE_SLIDING_DOORS(sTruckSections.sDoorsArray, sTruckSections.iActiveDoorsNum)
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]CREATE_ARMORY_TRUCK_SECTIONS - Sections Section Creation Finished..")
		SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_SECTIONS_CREATED)
		RETURN TRUE
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]CREATE_ARMORY_TRUCK_SECTIONS - Was unable to grab Anchor Points.")
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]CREATE_ARMORY_TRUCK_SECTIONS - Sections were not created.")
	SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_SECTIONS_CREATED, FALSE)
	RETURN FALSE
ENDFUNC

//Update tint for specific entity array.
PROC _OBJECT_ARRAY_SET_TINT(OBJECT_INDEX &ObjectArray[], ARMORY_TRUCK_SECTION_TINT_ENUM eNewTint)
	INT index
	CDEBUG1LN(DEBUG_SAFEHOUSE, "") 
	
	REPEAT COUNT_OF(ObjectArray) index
		IF DOES_ENTITY_EXIST(ObjectArray[index])
			SET_OBJECT_TINT_INDEX(ObjectArray[index], ENUM_TO_INT(eNewTint))
			#IF IS_DEBUG_BUILD
			VECTOR vPos
			vPos = GET_ENTITY_COORDS(ObjectArray[index])
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]_ENTITY_ARRAY_SET_TINT - index: ", index , " Position: ", vPos, " eNewTint: ", ENUM_TO_INT(eNewTint), " TintName: ", GET_ARMORY_TRUCK_SECTION_TINT_NAME(eNewTint)) 
			#ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Update tint off all custom entities created for the truck.
PROC _ARMORY_TRUCK_UPDATE_OBJECT_TINT(ARMORY_TRUCK_SECTIONS_STRUCT &sTruckSections, ARMORY_TRUCK_SECTION_TINT_ENUM eNewTint)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "") 
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& _ARMORY_TRUCK_UPDATE_OBJECT_TINT &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&") 
		
	//Add new arrays here
	//Tvs dont have a tint.
	//CDEBUG1LN(DEBUG_SAFEHOUSE, "obTV") 
	//_OBJECT_ARRAY_SET_TINT(sTruckSections.obTV, eNewTint)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "obComConsole") 
	_OBJECT_ARRAY_SET_TINT(sTruckSections.obComConsole, eNewTint)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "obComMonitor") 
	_OBJECT_ARRAY_SET_TINT(sTruckSections.obComMonitor, eNewTint)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "obSeat") 
	_OBJECT_ARRAY_SET_TINT(sTruckSections.obSeat, eNewTint)
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]&&&&&&&&&&&&&&&&&&&&&&&&&&&&&& _ARMORY_TRUCK_UPDATE_OBJECT_TINT &&&&&&&&&&&&&&&&&&&&&&&&&&&&&&") 
	CDEBUG1LN(DEBUG_SAFEHOUSE, "")
ENDPROC

/// PURPOSE:
///    Check current tint with Stat tint , update to the one from stat.
///    Dont call per frame , just use whenever stat is changed.
FUNC BOOL UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS(ARMORY_TRUCK_SECTIONS_STRUCT & sTruckSections, PLAYER_INDEX pOwner)
	BOOL bReturn = TRUE
	
	ARMORY_TRUCK_SECTION_TINT_ENUM eCurrentStat//, eCurrentSet
	ARMORY_TRUCK_SECTIONS_ENUM eTruckSection
	ARMORY_TRUCK_SECTION_TYPE_ENUM eSectionType
	INT iSection
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "") 
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]############################  UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS ############################") 
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]SET_ARMORY_TRUCK - g_SimpleInteriorData.bInteriorHasBeenReloaded: ", g_SimpleInteriorData.bInteriorHasBeenReloaded)
	//SECTION TINT
	REPEAT ATS_MAX_SECTIONS iSection
	
		IF INT_TO_ENUM(ARMORY_TRUCK_SECTION_TYPE_ENUM, sTruckSections.sSectionsArray[iSection].iSectionType) <> AT_ST_UNDEFINED
			CDEBUG1LN(DEBUG_SAFEHOUSE, "")
			eTruckSection = INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, iSection)
			eSectionType = GET_PLAYER_TRUCK_PURCHASED_SECTIONS(eTruckSection, pOwner)
			eCurrentStat = GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT(pOwner) //All sections will be same color now.
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - via owner, Section tint eCurrentStat: ", GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT(pOwner), 
			" PlayerName: ", GET_PLAYER_NAME(pOwner),
			" iSectionPos: ", iSection,
			" eSectionPos: ", GET_ARMORY_TRUCK_SECTION_NAME(eTruckSection),
			" eSectionType: ", GET_ARMORY_TRUCK_SECTION_TYPE_NAME(eSectionType),
			" scriptSectionType: ", GET_ARMORY_TRUCK_SECTION_TYPE_NAME(INT_TO_ENUM(ARMORY_TRUCK_SECTION_TYPE_ENUM, sTruckSections.sSectionsArray[iSection].iSectionType)))
			
			IF eCurrentStat <> AT_STI_INVALID_INDEX
				CDEBUG1LN(DEBUG_SAFEHOUSE, "") 
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]### Section Tint")
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - Updating Section: ", GET_ARMORY_TRUCK_SECTION_NAME(eTruckSection), 
				" <> eCurrentSet: ", GET_ARMORY_TRUCK_SECTION_TINT_NAME(eCurrentStat))
				
				IF NOT SET_ARMORY_TRUCK_SECTION_TINT(eTruckSection, eCurrentStat, eSectionType)
					bReturn = FALSE
				ENDIF
				//LightRig
				CDEBUG1LN(DEBUG_SAFEHOUSE, "") 
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]### Light Rig")
				MODEL_NAMES LightRigModel = GET_ARMORY_TRUCK_LIGHT_RIG(eSectionType, eCurrentStat)
				IF LightRigModel <> DUMMY_MODEL_FOR_SCRIPT
					SET_ARMORY_TRUCK_SECTION_LIGHTRIG(sTruckSections.sSectionsArray[iSection], LightRigModel)
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - was unable to update the light right, invald model: ")
				ENDIF
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - Section Type undefined, iSection: ", iSection) 
		ENDIF
	ENDREPEAT
		
	IF INT_TO_ENUM(ARMORY_TRUCK_SECTION_TYPE_ENUM, sTruckSections.sSectionsArray[ATS_FIRST_SECTION].iSectionType) <> AT_ST_UNDEFINED
		CDEBUG1LN(DEBUG_SAFEHOUSE, "") 
//		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]### TimeCycle")
//		//TimeCycle
//		STRING sTimeCycleModifier = GET_ARMORY_TRUCK_TIMECYCLE_MODIFIER(GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT(pOwner))
//		IF NOT IS_STRING_NULL_OR_EMPTY(sTimeCycleModifier)
//			SET_TIMECYCLE(sTimeCycleModifier)
//		ENDIF
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "") 
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]### Section Doors")
						
		//SECTION DOORS
		INT iDoor
		ARMORY_TRUCK_SECTION_TINT_ENUM eSectionSideA, eSectionSideB
		ARMORY_TRUCK_SECTIONS_ENUM eLastSection = INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, sTruckSections.iActiveSectionsNum - 1)
	
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - sTruckSections.iActiveSectionsNum: ", sTruckSections.iActiveSectionsNum, " eLastSection: ", GET_ARMORY_TRUCK_SECTION_NAME(eLastSection))
		REPEAT ARMORY_TRUCK_MAX_DOORS iDoor
			CDEBUG1LN(DEBUG_SAFEHOUSE, " ")
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - DOOR: ", iDoor)
			IF sTruckSections.sDoorInfoArray[iDoor].eOwnerSection <> ATS_INVALID_SECTION
				IF DOES_ENTITY_EXIST(sTruckSections.sDoorsArray[iDoor].Entity)
					//If first section , and entry door.
					IF sTruckSections.sDoorInfoArray[iDoor].eOwnerSection = ATS_FIRST_SECTION
					AND sTruckSections.sDoorInfoArray[iDoor].bDoorSideA
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - Section 1 Door Entry Found, iDoor: ", iDoor, " Owner: ", GET_ARMORY_TRUCK_SECTION_NAME(sTruckSections.sDoorInfoArray[iDoor].eOwnerSection))
						eSectionSideB = GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT(pOwner)
						eSectionSideA = eSectionSideB
					
					//If this is the last section , and this is the door that closes the last section
					ELIF sTruckSections.sDoorInfoArray[iDoor].eOwnerSection = eLastSection
					AND NOT sTruckSections.sDoorInfoArray[iDoor].bDoorSideA
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - Section Last Door Close Found, iDoor: ", iDoor, " Owner: ", GET_ARMORY_TRUCK_SECTION_NAME(sTruckSections.sDoorInfoArray[iDoor].eOwnerSection))
						eSectionSideA = GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT(pOwner)
						eSectionSideB = eSectionSideA
					
					//Door Closer to first section, and not the start/end sections
					ELIF sTruckSections.sDoorInfoArray[iDoor].bDoorSideA
						//try get previous section
						eTruckSection = GET_JOINT_SECTION(sTruckSections.sDoorInfoArray[iDoor].eOwnerSection, AT_SO_PREVIOUS)
						IF eTruckSection <> ATS_INVALID_SECTION
							eSectionSideA = GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT(pOwner) //Prev Section.
						ELSE
							CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - was unable to get previous section")
						ENDIF
						eSectionSideB = GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT(pOwner) //Section Door is in.
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - Else, iDoor: ", iDoor, " SideA?: ", sTruckSections.sDoorInfoArray[iDoor].bDoorSideA, 
						" Owner: ", GET_ARMORY_TRUCK_SECTION_NAME(sTruckSections.sDoorInfoArray[iDoor].eOwnerSection), 
						" Prev: ", GET_ARMORY_TRUCK_SECTION_NAME(eTruckSection))
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - Else Unacounted, iDoor: ", iDoor, " SideA?: ", sTruckSections.sDoorInfoArray[iDoor].bDoorSideA, " Owner: ", GET_ARMORY_TRUCK_SECTION_NAME(sTruckSections.sDoorInfoArray[iDoor].eOwnerSection))
						eSectionSideB = GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT(pOwner) //Section Door is in.
						eSectionSideA = eSectionSideB
					ENDIF
				ENDIF
				
				//Work out door tint index.
				IF eSectionSideA <> AT_STI_INVALID_INDEX
				AND eSectionSideB <> AT_STI_INVALID_INDEX
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - Tint sections valid, Updating Door: ", iDoor, 
					" eSectionSideA: ", GET_ARMORY_TRUCK_SECTION_TINT_NAME(eSectionSideA),
					" eSectionSideB: ", GET_ARMORY_TRUCK_SECTION_TINT_NAME(eSectionSideB))
					
					ARMORY_TRUCK_DOOR_TINT_ENUM eNewDoorTint = GET_ARMORY_TRUCK_DOOR_TINT(eSectionSideA, eSectionSideB)
					IF eNewDoorTint <> AT_DTI_INVALID_INDEX
						IF DOES_ENTITY_EXIST(sTruckSections.sDoorsArray[iDoor].Entity)
							//For blocker just use the tint of the property, doors use old setup.
							IF GET_ENTITY_MODEL(sTruckSections.sDoorsArray[iDoor].Entity) = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_inttruck_doorblocker"))
								SET_OBJECT_TINT_INDEX(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(sTruckSections.sDoorsArray[iDoor].Entity), ENUM_TO_INT(GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT(pOwner)))
							ELSE
								SET_OBJECT_TINT_INDEX(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(sTruckSections.sDoorsArray[iDoor].Entity), ENUM_TO_INT(eNewDoorTint))
							ENDIF
						ENDIF
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - Door: ", iDoor, " Tint Updated To: ", GET_ARMORY_TRUCK_DOOR_TINT_NAME(eNewDoorTint))
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - Unable to update door - invalid eNewDoorTint, tint iDoor: ", iDoor)
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - Unable to update door tint SECTION TINT INVALID, iDoor: ", iDoor, 
					" eSectionSideA: ", GET_ARMORY_TRUCK_SECTION_TINT_NAME(eSectionSideA),
					" eSectionSideB: ", GET_ARMORY_TRUCK_SECTION_TINT_NAME(eSectionSideB))
				ENDIF
				
			ENDIF
		ENDREPEAT
		CDEBUG1LN(DEBUG_SAFEHOUSE, " ")	
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR] UPDATE DOOR TINTS FINISH")
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - Unable to update timecycle, and tint due to 1st section being invalid.") 
	ENDIF
	
	//Extra Entity Tint
	IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT(pOwner) <> AT_STI_INVALID_INDEX
		_ARMORY_TRUCK_UPDATE_OBJECT_TINT(sTruckSections, GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT(pOwner))
	ELSE
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - Unable to update object tint index, invalid tint.")
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS - Finished")
	RETURN bReturn
ENDFUNC

//╞═════════════════════════════════╡ LOADING / UNLOADING ╞═══════════════════════════════╡

FUNC BOOL ARMORY_TRUCK_HAVE_SECTION_MODELS_LOADED(PLAYER_INDEX pOwner)
	ARMORY_TRUCK_SECTION_TYPE_ENUM eSectionOwner
	BOOL bAllSectionsLoaded = TRUE
	INT iSection
	CDEBUG1LN(DEBUG_SAFEHOUSE, "")
	PROPERTY_SECTION_STRUCT sDescriptionOut
	REPEAT ATS_MAX_SECTIONS iSection
		eSectionOwner = GET_PLAYER_TRUCK_PURCHASED_SECTIONS(INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, iSection), pOwner)
		IF eSectionOwner <> AT_ST_UNDEFINED
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]ARMORY_TRUCK_HAVE_SECTION_MODELS_LOADED - SectionID ", iSection, " Name: ", GET_ARMORY_TRUCK_SECTION_TYPE_NAME(eSectionOwner))
			IF GET_ARMORY_TRUCK_SECTION_DESCRIPTION(eSectionOwner, sDescriptionOut)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]ARMORY_TRUCK_HAVE_SECTION_MODELS_LOADED - description grabbed ok, model: ", GET_MODEL_NAME_FOR_DEBUG(sDescriptionOut.mSection))
				REQUEST_LOAD_MODEL(sDescriptionOut.mSection)
				IF NOT HAS_MODEL_LOADED(sDescriptionOut.mSection)
					bAllSectionsLoaded = FALSE
				ENDIF
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]ARMORY_TRUCK_HAVE_SECTION_MODELS_LOADED - description grabbed failed.")
			ENDIF
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]ARMORY_TRUCK_HAVE_SECTION_MODELS_LOADED, undefined - SectionID ", iSection)
		ENDIF
	ENDREPEAT
	
	RETURN bAllSectionsLoaded
ENDFUNC

FUNC BOOL REQUEST_ARMORY_TRUCK_SECTION_ASSETS(PLAYER_INDEX pOwner)
	
	IF NOT ARMORY_TRUCK_HAVE_SECTION_MODELS_LOADED(pOwner)
		RETURN FALSE
	ENDIF
	
	MODEL_NAMES eDoorModel = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_inttruck_door_01"))
	
	REQUEST_MODEL(eDoorModel)
	IF NOT HAS_MODEL_LOADED(eDoorModel)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]REQUEST_ARMORY_TRUCK_SECTION_ASSETS - Not loaded yet: ", GET_MODEL_NAME_FOR_DEBUG(eDoorModel))
		RETURN FALSE
	ENDIF
	
	MODEL_NAMES eComTVModel = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_trailer_tv_02"))
	
	REQUEST_MODEL(eComTVModel)
	IF NOT HAS_MODEL_LOADED(eComTVModel)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]REQUEST_ARMORY_TRUCK_SECTION_ASSETS - Not loaded yet: ", GET_MODEL_NAME_FOR_DEBUG(eComTVModel))
		RETURN FALSE
	ENDIF
	
	MODEL_NAMES eLivTVModel = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_trailer_tv"))
	
	REQUEST_MODEL(eLivTVModel)
	IF NOT HAS_MODEL_LOADED(eLivTVModel)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]REQUEST_ARMORY_TRUCK_SECTION_ASSETS - Not loaded yet: ", GET_MODEL_NAME_FOR_DEBUG(eLivTVModel))
		RETURN FALSE
	ENDIF
	
	MODEL_NAMES eLivDoorBlocker = INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_inttruck_doorblocker"))
	
	REQUEST_MODEL(eLivDoorBlocker)
	IF NOT HAS_MODEL_LOADED(eLivDoorBlocker)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]REQUEST_ARMORY_TRUCK_SECTION_ASSETS - Not loaded yet: ", GET_MODEL_NAME_FOR_DEBUG(eLivDoorBlocker))
		RETURN FALSE
	ENDIF
	
	IF g_sMPTunables.bEnable_XMAS2017_Vehicle_Tinsel
		MODEL_NAMES eXmasPropMain = INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Truck_Main"))
		
		REQUEST_MODEL(eXmasPropMain)
		IF NOT HAS_MODEL_LOADED(eXmasPropMain)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]REQUEST_ARMORY_TRUCK_SECTION_ASSETS - Not loaded yet: ", GET_MODEL_NAME_FOR_DEBUG(eXmasPropMain))
			RETURN FALSE
		ENDIF
		
		MODEL_NAMES eXmasPropCarMod = INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Truck_CarMod"))
		
		REQUEST_MODEL(eXmasPropCarMod)
		IF NOT HAS_MODEL_LOADED(eXmasPropCarMod)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]REQUEST_ARMORY_TRUCK_SECTION_ASSETS - Not loaded yet: ", GET_MODEL_NAME_FOR_DEBUG(eXmasPropCarMod))
			RETURN FALSE
		ENDIF
		
		MODEL_NAMES eXmasPropGunMod = INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Truck_GunMod"))
		
		REQUEST_MODEL(eXmasPropGunMod)
		IF NOT HAS_MODEL_LOADED(eXmasPropGunMod)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]REQUEST_ARMORY_TRUCK_SECTION_ASSETS - Not loaded yet: ", GET_MODEL_NAME_FOR_DEBUG(eXmasPropGunMod))
			RETURN FALSE
		ENDIF
		
		MODEL_NAMES eXmasPropLiving = INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Truck_Living"))
		
		REQUEST_MODEL(eXmasPropLiving)
		IF NOT HAS_MODEL_LOADED(eXmasPropLiving)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]REQUEST_ARMORY_TRUCK_SECTION_ASSETS - Not loaded yet: ", GET_MODEL_NAME_FOR_DEBUG(eXmasPropLiving))
			RETURN FALSE
		ENDIF
		
		MODEL_NAMES eXmasPropCommand = INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Truck_Command"))
		
		REQUEST_MODEL(eXmasPropCommand)
		IF NOT HAS_MODEL_LOADED(eXmasPropCommand)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]REQUEST_ARMORY_TRUCK_SECTION_ASSETS - Not loaded yet: ", GET_MODEL_NAME_FOR_DEBUG(eXmasPropCommand))
			RETURN FALSE
		ENDIF
	ENDIF	
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]REQUEST_ARMORY_TRUCK_SECTION_ASSETS - All assets loaded successfully: ")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Remove all the sections, destroy doors
///
PROC CLEANUP_ARMORY_TRUCK_SECTIONS(ARMORY_TRUCK_SECTIONS_STRUCT & sTruckSections)
	CLEANUP_PROPERTY_SECTIONS(sTruckSections.sSectionsArray, sTruckSections.iActiveSectionsNum)
	DESTROY_SLIDING_DOORS(sTruckSections.sDoorsArray, sTruckSections.iActiveDoorsNum)
	
	INT index
	REPEAT ARMORY_TRUCK_MAX_DOORS index
		sTruckSections.sDoorInfoArray[index].eOwnerSection = ATS_INVALID_SECTION
	ENDREPEAT
		
	//CLEAR_TIMECYCLE()
	g_SimpleInteriorData.iArmoryTruckInteriorBitSet = 0
	
	REPEAT ATS_MAX_SECTIONS index
		IF DOES_ENTITY_EXIST(sTruckSections.obTV[index])
			DELETE_OBJECT(sTruckSections.obTV[index])
		ENDIF
		
		IF DOES_ENTITY_EXIST(sTruckSections.obComConsole[index])
			DELETE_OBJECT(sTruckSections.obComConsole[index])
		ENDIF
		
		IF DOES_ENTITY_EXIST(sTruckSections.obComMonitor[index])
			DELETE_OBJECT(sTruckSections.obComMonitor[index])
		ENDIF
	ENDREPEAT
	
	INT i
	REPEAT 9 i 
		IF DOES_ENTITY_EXIST(sTruckSections.obSeat[i])
			DELETE_OBJECT(sTruckSections.obSeat[i])
		ENDIF
	ENDREPEAT 
	
	IF DOES_ENTITY_EXIST(sTruckSections.obDoorGeom[0])
		DELETE_OBJECT(sTruckSections.obDoorGeom[0])
	ENDIF
	
	IF DOES_ENTITY_EXIST(sTruckSections.obDoorGeom[1])
		DELETE_OBJECT(sTruckSections.obDoorGeom[1])
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_trailer_tv_02")))
	SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_gr_trailer_tv")))
	SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_highendchair_gr_01a")))	
	SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("gr_prop_inttruck_doorblocker")))
	
	IF g_sMPTunables.bEnable_XMAS2017_Vehicle_Tinsel
		SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Truck_Main")))
		SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Truck_CarMod")))
		SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Truck_GunMod")))
		SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Truck_Living")))
		SET_MODEL_AS_NO_LONGER_NEEDED(INT_TO_ENUM(MODEL_NAMES, HASH("xm_Int_Prop_Tinsel_Truck_Command")))
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]CLEANUP_ARMORY_TRUCK_SECTIONS - All cleaned up!")
ENDPROC

//╞═════════════════════════════════╡ GLOBAL CHECKS ╞═══════════════════════════════╡
/// PURPOSE:
///    Check if all section of the truck were loaded and created.
FUNC BOOL HAVE_ARMORY_TRUCK_SECTIONS_LOADED()
	RETURN GET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_SECTIONS_CREATED)
ENDFUNC

//Check if Truck Sections/Tints were rebuilt this frame.
FUNC BOOL HAVE_ARMORY_TRUCK_SECTIONS_UPDATED()
	RETURN GET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_SECTIONS_REFRESHED)
ENDFUNC

FUNC BOOL HAVE_ARMORY_TRUCK_TINTS_UPDATED()
	RETURN GET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESHED)
ENDFUNC

/// PURPOSE:
///    Request to rebuild the truck interior
/// PARAMS:
///    bForceUpdate - will force the update, -> added for remote palyers, because the tint stat changes only for owners.
PROC ARMORY_TRUCK_REQUEST_INTERIOR_UPDATE(BOOL bForceUpdate = FALSE)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]ARMORY_TRUCK_REQUEST_INTERIOR_UPDATE - Truck update requested.")
	SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_REFRESH_REQUESTED)
	
//	g_bRefreshTruckActivities = TRUE
	
	IF bForceUpdate
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]ARMORY_TRUCK_REQUEST_INTERIOR_UPDATE - Force refresh the interior.")
		SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_SECTIONS_REFRESH_REQUIRED)
		SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESH_REQUIRED)
	ENDIF
ENDPROC

/// PURPOSE:
///    Update all logic related to sections.
/// PARAMS:
///    sTruckSections - 
PROC UPDATE_ARMORY_TRUCK_SECTIONS(ARMORY_TRUCK_SECTIONS_STRUCT & sTruckSections, PLAYER_INDEX pOwner)	

	//Stop refresh calls.
	IF GET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_SECTIONS_REFRESHED)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_SECTIONS - AT_UB_SECTIONS_REFRESHED -> Stopped")
		SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_SECTIONS_REFRESHED, FALSE)
	ENDIF
	
	IF GET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESHED)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_SECTIONS - AT_UB_TINTS_REFRESHED -> Stopped")
		SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESHED, FALSE)
	ENDIF
	
	//Check if script request to update is set.
	IF GET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_REFRESH_REQUESTED)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "") 
		CDEBUG1LN(DEBUG_SAFEHOUSE, "") 
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ARMORY_TRUCK REFRESH - START @@@@@@@@@@@@@@@@@@@@@@@@@@@@@") 
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_SECTIONS - Truck refresh was requested")
		CDEBUG1LN(DEBUG_SAFEHOUSE, "")
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_SECTIONS - OWNER: ", GET_PLAYER_NAME(pOwner))
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_SECTIONS - ATS_FIRST_SECTION: ", GET_ARMORY_TRUCK_SECTION_TYPE_NAME(GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, pOwner))
						, " | ATS_SECOND_SECTION: ", GET_ARMORY_TRUCK_SECTION_TYPE_NAME(GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, pOwner))
						, " | ATS_THIRD_SECTION: " , GET_ARMORY_TRUCK_SECTION_TYPE_NAME(GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, pOwner)))
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_SECTIONS - GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT ATS_FIRST_SECTION, ID: ", GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT(pOwner),
		" NAME: ", GET_ARMORY_TRUCK_SECTION_TINT_NAME(GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT(pOwner)))
		CDEBUG1LN(DEBUG_SAFEHOUSE, "")
		// SECTIONS
		IF GET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_SECTIONS_REFRESH_REQUIRED)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_SECTIONS - truck refresh sections.")
			
			CREATE_ARMORY_TRUCK_SECTIONS(SIMPLE_INTERIOR_ARMORY_TRUCK_1, sTruckSections, 
				GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, pOwner), 
				GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, pOwner), 
				GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, pOwner))
				
			//When rebuilding the sections need to refresh the tint aswell.
			IF NOT GET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESH_REQUIRED)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_SECTIONS - truck refresh tints, due to sections refresh")
				SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESH_REQUIRED)
			ENDIF
			
			//Extra Entities.
			UPDATE_ARMOURY_TRUCK_TVS(sTruckSections, pOwner)
			UPDATE_ARMORY_TRUCK_PROPS(sTruckSections, pOwner)
			UPDATE_ARMORY_TRUCK_SEATING(sTruckSections, pOwner)
			UPDATE_ARMORY_TRUCK_DOOR_BLOCKER_COLLISION_GEOMETRY(sTruckSections, pOwner)
			UPDATE_ARMOURY_TRUCK_TINSELS(sTruckSections, pOwner)
			SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_SECTIONS_REFRESHED)
			SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_SECTIONS_REFRESH_REQUIRED, FALSE)
		ENDIF
		
		//TINTS
		IF GET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESH_REQUIRED)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_SECTIONS - truck refresh tints.")
			IF UPDATE_ARMORY_TRUCK_TINT_ALL_SECTIONS(sTruckSections, pOwner)
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_SECTIONS - truck refresh tints - was successfull.")
				SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESH_REQUIRED, FALSE)
				SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESHED)
			ELSE
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_SECTIONS - truck refresh tints - was UN-successfull.")
			ENDIF
		ENDIF
		
		IF NOT GET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESH_REQUIRED)
			SET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_REFRESH_REQUESTED, FALSE)
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_SECTIONS - Removing refresh request.")
		ELSE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]UPDATE_ARMORY_TRUCK_SECTIONS - tints were not refreshed succesfully, allowing refresh request to continue.")
		ENDIF
		
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR]@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ ARMORY_TRUCK REFRESH - FINISH @@@@@@@@@@@@@@@@@@@@@@@@@@@@@") 
		CDEBUG1LN(DEBUG_SAFEHOUSE, "") 
		CDEBUG1LN(DEBUG_SAFEHOUSE, "") 
	ENDIF
	
	INT iPlayer
	PLAYER_INDEX piPlayer
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iPlayer
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iPlayer))
			piPlayer = INT_TO_PLAYERINDEX(iPlayer)
			
			IF NETWORK_IS_PLAYER_A_PARTICIPANT_ON_SCRIPT(piPlayer, "am_mp_armory_truck", globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance)
			OR NETWORK_IS_PLAYER_A_PARTICIPANT_ON_SCRIPT(piPlayer, "AM_MP_CREATOR_TRAILER", globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance)
				UPDATE_SLIDING_DOORS(sTruckSections.sDoorsArray, sTruckSections.iActiveDoorsNum, piPlayer, TRUE)
			ELSE
				#IF IS_DEBUG_BUILD
				IF GET_FRAME_COUNT() % 60 = 0
					CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR] not a script participat, instance: ", globalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.iInstance) 
				ENDIF
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			IF GET_FRAME_COUNT() % 60 = 0
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR] NETWORK_IS_PARTICIPANT_ACTIVE not active: ", iPlayer)
			ENDIF
			#ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

//PROC ARMORY_TRUCK_CHECK_SECTIONS_CREATED(ARMORY_TRUCK_SECTIONS_STRUCT & sTruckSections, PLAYER_INDEX pOwner)
//	//Stop refresh calls.
//	IF NOT GET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESH_REQUIRED)
//	AND NOT GET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESH_REQUIRED)
//		CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR][AT-CREATED]ARMORY_TRUCK_CHECK_SECTIONS_CREATED, Owner: ", GET_PLAYER_NAME(pOwner), 
//		" SectionOne: ", GET_ARMORY_TRUCK_SECTION_NAME(GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, pOwner)),
//		" SectionTwo: ", GET_ARMORY_TRUCK_SECTION_NAME(GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, pOwner)),
//		" SectionThree: ", GET_ARMORY_TRUCK_SECTION_NAME(GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, pOwner)))
//		
//		
//		MP_PROP_OFFSET_STRUCT sSectionTransform
//		PROPERTY_SECTION_STRUCT sSectionDescription
//		INT index
//		
//		INT iSectionsValid, iSectionsFound
//		
//		FOR index = ATS_FIRST_SECTION TO ATS_THIRD_SECTION
//			IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, index), pOwner) = AT_ST_UNDEFINED
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR][AT-CREATED] ARMORY_TRUCK_CHECK_SECTIONS_CREATED - section: ", index, " is undefined")
//			ELSE
//				iSectionsValid++
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR][AT-CREATED] ARMORY_TRUCK_CHECK_SECTIONS_CREATED - section: ", index, " is defined")
//				IF NOT GET_ARMORY_TRUCK_SECTION_COORDS(ATS_FIRST_SECTION, sSectionTransform)
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR][AT-CREATED]ARMORY_TRUCK_CHECK_SECTIONS_CREATED - was unable to grab section transform")
//				ENDIF
//				IF NOT GET_ARMORY_TRUCK_SECTION_DESCRIPTION(GET_PLAYER_TRUCK_PURCHASED_SECTIONS(INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, index), pOwner))
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR][AT-CREATED]ARMORY_TRUCK_CHECK_SECTIONS_CREATED - was unable to grab section description")
//				ENDIF
//				
//				IF GET_TINT_INDEX_CLOSEST_BUILDING_OF_TYPE(sSectionTransform.vLoc, 5.0, sSectionDescription.mSection) <> - 1
//					iSectionsFound++
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR][AT-CREATED] ARMORY_TRUCK_CHECK_SECTIONS_CREATED - section: ", index, " found object.")
//				ELSE
//					CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR][AT-CREATED] ARMORY_TRUCK_CHECK_SECTIONS_CREATED - section: ", index, " was unable to find object.")
//				ENDIF
//			ENDIF
//		ENDFOR
//			
//		IF iSectionsValid = iSectionsFound
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR][AT-CREATED] ARMORY_TRUCK_CHECK_SECTIONS_CREATED - all sections found, iSectionsValid: ", iSectionsValid, " iSectionsFound: ", iSectionsFound)
//			RETURN TRUE
//		ELSE
//			CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR][AT-CREATED] ARMORY_TRUCK_CHECK_SECTIONS_CREATED - not all sections are found, iSectionsValid: ", iSectionsValid, " iSectionsFound: ", iSectionsFound)
//		ENDIF
//	ELSE
//		#IF IS_DEBUG_BUILD
//			IF GET_FRAME_COUNT() % 30 = 0
//				CDEBUG1LN(DEBUG_SAFEHOUSE, "[AT-INTERIOR][AT-CREATED]ARMORY_TRUCK_CHECK_SECTIONS_CREATED - can't cehck if sections are created, refreshReq: ", GET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESH_REQUIRED),
//				" refreshTintsReq: ", GET_ARMORY_TRUCK_UPDATE_BIT(AT_UB_TINTS_REFRESH_REQUIRED))		
//			ENDIF
//		#ENDIF
//	ENDIF
//	
//	RETURN FALSE
//ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════════╡ DEBUG ╞═════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛
#IF IS_DEBUG_BUILD

PROC PRINT_ARMORY_TRUCK_SETUP(ARMORY_TRUCK_SECTIONS_STRUCT & sTruckSections, PLAYER_INDEX pOwner)
	CDEBUG1LN(DEBUG_SAFEHOUSE, "")
	CDEBUG1LN(DEBUG_SAFEHOUSE, "")
	CDEBUG1LN(DEBUG_SAFEHOUSE, "**************** ARMORY TRUCK SETUP PRINT ****************")
	
	INT iSection
	INT iDoor
	
	REPEAT ATS_MAX_SECTIONS iSection

		CDEBUG1LN(DEBUG_SAFEHOUSE, "")
		CDEBUG1LN(DEBUG_SAFEHOUSE, "	################ SECTION ", iSection)
			CDEBUG1LN(DEBUG_SAFEHOUSE, " Section Type: ", GET_ARMORY_TRUCK_SECTION_TYPE_NAME(INT_TO_ENUM(ARMORY_TRUCK_SECTION_TYPE_ENUM, sTruckSections.sSectionsArray[iSection].iSectionType)))
			CDEBUG1LN(DEBUG_SAFEHOUSE, " Section Tint: ", GET_ARMORY_TRUCK_SECTION_TINT_NAME(GET_PLAYER_TRUCK_PURCHASED_SECTIONS_TINT(pOwner)))
			BOOL bLightRighCreated = DOES_ENTITY_EXIST(sTruckSections.sSectionsArray[iSection].lightRig)
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, " Section LightRig: Exists: ", bLightRighCreated)
			IF bLightRighCreated
				CDEBUG1LN(DEBUG_SAFEHOUSE, " Section LightRig: Name: ", GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(sTruckSections.sSectionsArray[iSection].lightRig)))
			ENDIF
			REPEAT ARMORY_TRUCK_MAX_DOORS iDoor
				IF sTruckSections.sDoorInfoArray[iDoor].eOwnerSection = INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, iSection)
					IF DOES_ENTITY_EXIST(sTruckSections.sDoorsArray[iDoor].Entity)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "")
						CDEBUG1LN(DEBUG_SAFEHOUSE, "		Door: ", iDoor, " EntrySideA: ", sTruckSections.sDoorInfoArray[iDoor].bDoorSideA, 
						" Static: ", sTruckSections.sDoorsArray[iDoor].bStatic,
						" Tint: ", GET_ARMORY_TRUCK_DOOR_TINT_NAME(INT_TO_ENUM(ARMORY_TRUCK_DOOR_TINT_ENUM, GET_OBJECT_TINT_INDEX(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(sTruckSections.sDoorsArray[iDoor].Entity)))))
					ELSE
						CDEBUG1LN(DEBUG_SAFEHOUSE, " DOOR DOES NOT EXIST: ", iDoor)
					ENDIF
				ENDIF
			ENDREPEAT
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "**********************************************************")
	CDEBUG1LN(DEBUG_SAFEHOUSE, "")
	CDEBUG1LN(DEBUG_SAFEHOUSE, "")
ENDPROC

PROC CREATE_SECTION_DEBUG_WIDGET(PROPERTY_SECTIONS_DEBUG_STRUCT & sDebug)
	START_WIDGET_GROUP("ARMORY TRUCK SECTIONS")
		INT iSectionID
		TEXT_LABEL_31 txSection, txID
		REPEAT ATS_MAX_SECTIONS iSectionID
			//SECTION SELECTION
			START_NEW_WIDGET_COMBO()
				//INT iBitID
				//REPEAT 31 iBitID
				//	IF IS_BIT_SET(iPossibleSectionTypes, iBitID)
				INT iSectionType
				REPEAT AT_ST_MAX_SECTION_TYPES iSectionType
						ADD_TO_WIDGET_COMBO(GET_ARMORY_TRUCK_SECTION_TYPE_NAME(INT_TO_ENUM(ARMORY_TRUCK_SECTION_TYPE_ENUM, iSectionType)))//   GET_SECTION_TYPE_FROM_DEBUG_ID(iBitID)))
				ENDREPEAT
				//	ENDIF
				//ENDREPEAT
				
				txSection = "Section # "
				txID = iSectionID
				txSection += txID
			CDEBUG1LN(DEBUG_SAFEHOUSE, "CREATE_SECTION_DEBUG_WIDGET iSectionID: ", iSectionID)  
			STOP_WIDGET_COMBO(txSection, sDebug.iSectionTypeArray[iSectionID])
								
			ADD_WIDGET_STRING("=====================================================")
			sDebug.iSectionTypeArray[iSectionID] = 0
			sDebug.iSectionTypePrevArray[iSectionID] = 0
			sDebug.iSectionTintArray[iSectionID] = 0
			sDebug.iSectionTintPrevArray[iSectionID] = 0
		ENDREPEAT
		
		//All sections same color
		START_NEW_WIDGET_COMBO()
			INT iTintIndex = 0
			REPEAT AT_STI_INDEX_MAX iTintIndex
				CDEBUG1LN(DEBUG_SAFEHOUSE, "$CREATE_SECTION_DEBUG_WIDGET iTintIndex: ", iTintIndex, " AT_STI_INDEX_MAX: ", AT_STI_INDEX_MAX) 
				ADD_TO_WIDGET_COMBO(GET_ARMORY_TRUCK_SECTION_TINT_NAME(INT_TO_ENUM(ARMORY_TRUCK_SECTION_TINT_ENUM, iTintIndex+1)))
			ENDREPEAT
			
		STOP_WIDGET_COMBO("Truck Interior Tint", sDebug.iSectionTintArray[ATS_FIRST_SECTION])
		
		ADD_WIDGET_BOOL("Object Placement", sDebug.bObjectPlacement)
		ADD_WIDGET_BOOL("Print Truck Info", sDebug.bPrintTruckInfo)
	STOP_WIDGET_GROUP()
	
	sDebug.bObjectPlacement = FALSE
	sDebug.bPrevObjectPlacement = FALSE
ENDPROC

/// PURPOSE:
///    Frame update to check if sections parameters were changed via RAG
/// PARAMS:
///    sDebug - 
///    eSimpleInteriorID - 
///    sTruckSections - 
PROC UPDATE_SECTIONS_DEBUG_WIDGET(PROPERTY_SECTIONS_DEBUG_STRUCT & sDebug, ARMORY_TRUCK_SECTIONS_STRUCT & sTruckSections, PLAYER_INDEX pOwner)
	BOOL bUpdate = FALSE, bUpdateTint = FALSE
	INT iSection = 0
	INT iTintIndex = 0
	
	IF sDebug.bPrintTruckInfo
		PRINT_ARMORY_TRUCK_SETUP(sTruckSections, pOwner)
		sDebug.bPrintTruckInfo = FALSE
	ENDIF
	
	REPEAT ATS_MAX_SECTIONS iSection
		//Section Update
		IF sDebug.iSectionTypeArray[iSection] <> sDebug.iSectionTypePrevArray[iSection]
			//If debug placement any section selection will set first section only,
			IF sDebug.bObjectPlacement
				sDebug.iSectionTypeArray[ATS_FIRST_SECTION] = sDebug.iSectionTypeArray[iSection]
				sDebug.iSectionTypePrevArray[iSection] = sDebug.iSectionTypeArray[iSection]
			ELSE
				sDebug.iSectionTypePrevArray[iSection] = sDebug.iSectionTypeArray[iSection]
			ENDIF
			bUpdate = TRUE
		ENDIF
		
		//TINT UPDATE
		REPEAT AT_STI_INDEX_MAX iTintIndex
			IF sDebug.iSectionTintArray[iSection] <> sDebug.iSectionTintPrevArray[iSection]
				sDebug.iSectionTintPrevArray[iSection] = sDebug.iSectionTintArray[iSection]
				bUpdateTint = TRUE
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	IF sDebug.bObjectPlacement <> sDebug.bPrevObjectPlacement
		sDebug.bPrevObjectPlacement = sDebug.bObjectPlacement
		bUpdate = TRUE
	ENDIF
	
	IF bUpdate
		IF sDebug.bObjectPlacement
			sDebug.iSectionTypeArray[ATS_SECOND_SECTION] = ENUM_TO_INT(AT_ST_UNDEFINED)
			sDebug.iSectionTypeArray[ATS_THIRD_SECTION] = ENUM_TO_INT(AT_ST_UNDEFINED)
			sDebug.iSectionTypePrevArray[ATS_SECOND_SECTION] = ENUM_TO_INT(AT_ST_UNDEFINED)
			sDebug.iSectionTypePrevArray[ATS_THIRD_SECTION] = ENUM_TO_INT(AT_ST_UNDEFINED)
		ELSE
			IF sDebug.iSectionTypeArray[ATS_FIRST_SECTION] = ENUM_TO_INT(AT_ST_UNDEFINED)
				sDebug.iSectionTypeArray[ATS_SECOND_SECTION] = ENUM_TO_INT(AT_ST_UNDEFINED)
				sDebug.iSectionTypeArray[ATS_THIRD_SECTION] = ENUM_TO_INT(AT_ST_UNDEFINED)
			ELIF sDebug.iSectionTypeArray[ATS_SECOND_SECTION] = ENUM_TO_INT(AT_ST_UNDEFINED)
				sDebug.iSectionTypeArray[ATS_THIRD_SECTION] = ENUM_TO_INT(AT_ST_UNDEFINED)
			ENDIF
			
			IF sDebug.iSectionTypeArray[ATS_SECOND_SECTION] = ENUM_TO_INT(AT_ST_EMPTY_DOUBLE)
			OR sDebug.iSectionTypeArray[ATS_SECOND_SECTION] = ENUM_TO_INT(AT_ST_CARMOD)
				sDebug.iSectionTypeArray[ATS_THIRD_SECTION] = ENUM_TO_INT(AT_ST_UNDEFINED)
			ENDIF
		ENDIF
		
		SET_ARMORY_TRUCK_SECTION_TYPE_STAT(ATS_FIRST_SECTION, INT_TO_ENUM(ARMORY_TRUCK_SECTION_TYPE_ENUM, sDebug.iSectionTypeArray[ATS_FIRST_SECTION]))
		SET_ARMORY_TRUCK_SECTION_TYPE_STAT(ATS_SECOND_SECTION, INT_TO_ENUM(ARMORY_TRUCK_SECTION_TYPE_ENUM, sDebug.iSectionTypeArray[ATS_SECOND_SECTION]))
		SET_ARMORY_TRUCK_SECTION_TYPE_STAT(ATS_THIRD_SECTION, INT_TO_ENUM(ARMORY_TRUCK_SECTION_TYPE_ENUM, sDebug.iSectionTypeArray[ATS_THIRD_SECTION]))
		ARMORY_TRUCK_REQUEST_INTERIOR_UPDATE()
	ENDIF
	
	IF bUpdateTint
		SET_ARMORY_TRUCK_TINT_STAT(INT_TO_ENUM(ARMORY_TRUCK_SECTION_TINT_ENUM, sDebug.iSectionTintArray[ATS_FIRST_SECTION] + 1))
		//SET_ARMORY_TRUCK_SECTION_TINT_STAT(ATS_FIRST_SECTION, INT_TO_ENUM(ARMORY_TRUCK_SECTION_TINT_ENUM, sDebug.iSectionTintArray[ATS_FIRST_SECTION] + 1))
		//SET_ARMORY_TRUCK_SECTION_TINT_STAT(ATS_SECOND_SECTION, INT_TO_ENUM(ARMORY_TRUCK_SECTION_TINT_ENUM, sDebug.iSectionTintArray[ATS_FIRST_SECTION] + 1))
		//SET_ARMORY_TRUCK_SECTION_TINT_STAT(ATS_THIRD_SECTION, INT_TO_ENUM(ARMORY_TRUCK_SECTION_TINT_ENUM, sDebug.iSectionTintArray[ATS_FIRST_SECTION] + 1))
		ARMORY_TRUCK_REQUEST_INTERIOR_UPDATE()
	ENDIF
	
	IF GET_FRAME_COUNT() % 30 = 0
		ARMORY_TRUCK_SECTION_TYPE_ENUM sSectionType
		ARMORY_TRUCK_SECTIONS_ENUM eTruckSection
		//Update debug section type if changed inthe background.
		REPEAT ATS_MAX_SECTIONS iSection
			eTruckSection = INT_TO_ENUM(ARMORY_TRUCK_SECTIONS_ENUM, iSection)
			sSectionType = INT_TO_ENUM(ARMORY_TRUCK_SECTION_TYPE_ENUM, sTruckSections.sSectionsArray[iSection].iSectionType)
			sDebug.iSectionTypeArray[eTruckSection] = ENUM_TO_INT(sSectionType)
			sDebug.iSectionTypePrevArray[eTruckSection] = ENUM_TO_INT(sSectionType)
						
			//Update debug tint type id changed in the background.
			ARMORY_TRUCK_SECTION_TINT_ENUM eTintIndex
			eTintIndex = AT_STI_INVALID_INDEX
			
			IF sSectionType <> AT_ST_UNDEFINED
			AND sSectionType <> AT_ST_EMPTY_DOUBLE
			AND sSectionType <> AT_ST_EMPTY_SINGLE
			AND sSectionType <> AT_ST_EMPTY_SINGLE_DOOR
				IF HAVE_ARMORY_TRUCK_SECTIONS_LOADED()
					eTintIndex = GET_ARMORY_TRUCK_TINT_INDEX_MODEL(eTruckSection, sSectionType) - INT_TO_ENUM(ARMORY_TRUCK_SECTION_TINT_ENUM, 1)
				ENDIF
			ENDIF
			
			IF eTintIndex <> AT_STI_INVALID_INDEX
				sDebug.iSectionTintArray[iSection] = ENUM_TO_INT(eTintIndex)
				sDebug.iSectionTintPrevArray[iSection] = ENUM_TO_INT(eTintIndex)
			ENDIF
		ENDREPEAT
	ENDIF

ENDPROC

PROC DEBUG_UPDATE_TRUCK_MODULE(ARMORY_TRUCK_SECTIONS_ENUM eSectionPos, ARMORY_TRUCK_SECTION_TYPE_ENUM eNewSectionType)
	IF NOT IS_GUNRUNNING_TRUCK_PURCHASED()
		IF GET_RANDOM_BOOL()
			SET_MP_INT_CHARACTER_STAT(MP_STAT_INV_TRUCK_MODEL_0, ENUM_TO_INT(HAULER2))
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[TRUCK] DEBUG_UPDATE_TRUCK_MODULE - force player owns Hauler2")
		ELSE
			SET_MP_INT_CHARACTER_STAT(MP_STAT_INV_TRUCK_MODEL_0, ENUM_TO_INT(PHANTOM3))
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[TRUCK] DEBUG_UPDATE_TRUCK_MODULE - force player owns Phantom3")
		ENDIF
	ENDIF
	
	CDEBUG1LN(DEBUG_SAFEHOUSE, "[TRUCK] DEBUG_UPDATE_TRUCK_MODULE: iSectionPos:", GET_ARMORY_TRUCK_SECTION_NAME(eSectionPos), " eNewSectionType:", GET_ARMORY_TRUCK_SECTION_TYPE_NAME(eNewSectionType))
	
	SET_ARMORY_TRUCK_SECTION_TYPE_STAT(eSectionPos, eNewSectionType)
	SET_ARMORY_TRUCK_TINT_STAT(AT_STI_GREY_DARK_GREY)
	SWITCH GET_ARMORY_TRUCK_SECTION_TYPE_STAT(ATS_SECOND_SECTION)
		CASE AT_ST_CARMOD
		CASE AT_ST_EMPTY_DOUBLE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[TRUCK] DEBUG_UPDATE_TRUCK_MODULE: REMOVING SECTION THREE")
			SET_ARMORY_TRUCK_SECTION_TYPE_STAT(ATS_THIRD_SECTION, AT_ST_UNDEFINED)
		BREAK
	ENDSWITCH

	IF eSectionPos = ATS_SECOND_SECTION
	OR eSectionPos = ATS_THIRD_SECTION
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[TRUCK] DEBUG_UPDATE_TRUCK_MODULE: Section 2 or 3")
		IF GET_ARMORY_TRUCK_SECTION_TYPE_STAT(ATS_SECOND_SECTION) <> AT_ST_CARMOD
		AND GET_ARMORY_TRUCK_SECTION_TYPE_STAT(ATS_SECOND_SECTION) <> AT_ST_EMPTY_DOUBLE
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[TRUCK] DEBUG_UPDATE_TRUCK_MODULE: MOLULDE 2 is not a carmod or empty double")
			IF GET_ARMORY_TRUCK_SECTION_TYPE_STAT(ATS_THIRD_SECTION) = AT_ST_UNDEFINED
				CDEBUG1LN(DEBUG_SAFEHOUSE, "[TRUCK] DEBUG_UPDATE_TRUCK_MODULE: MOLULDE 3 is undefined set it to single empty.")
				SET_ARMORY_TRUCK_SECTION_TYPE_STAT(ATS_THIRD_SECTION, AT_ST_EMPTY_SINGLE)
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_PACKED_STAT_INT(PACKED_MP_INT_TRUCK_SECTION_1_NUM) = ENUM_TO_INT(AT_STI_INVALID_INDEX)
		CDEBUG1LN(DEBUG_SAFEHOUSE, "[TRUCK] DEBUG_UPDATE_TRUCK_MODULE: tint stat is undefined white ligth grey.")
		SET_ARMORY_TRUCK_TINT_STAT(AT_STI_WHITE_LIGTH_GREY)
	ENDIF
	
	ARMORY_TRUCK_REQUEST_INTERIOR_UPDATE()
ENDPROC

#ENDIF //debug

 //gunrunning
