//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_CAR_MEET.sch																					//
// Description: Functions to implement from net_peds_base.sch and look up tables to return CAR_MEET ped data.			//
// Written by:  Jan Mencfel																								//
// Date:  		03/03/21																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_TUNER
USING "net_peds_base.sch"
USING "website_public.sch"
USING "net_peds_data_common.sch"
USING "net_simple_cutscene_common.sch"




FUNC VECTOR _GET_CAR_MEET_PED_LOCAL_COORDS_BASE_POSITION(PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	UNUSED_PARAMETER(ePedLocation)
	RETURN <<-2171.6636, 1113.2114, -25.3624>>	// tr_int2_shell interior coords
ENDFUNC

FUNC FLOAT _GET_CAR_MEET_PED_LOCAL_HEADING_BASE_HEADING(PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	UNUSED_PARAMETER(ePedLocation)
	RETURN 90.0	// tr_int2_shell interior heading
ENDFUNC

PROC GET_SPECIAL_PED_LOCATION_AND_ANIMATION(INT iGeneralLayout,INT iPed, VECTOR &vPos, FLOAT &fHead, INT &iActivity)
	
	INT iLayout
	INT iNumHundreds = FLOOR(iGeneralLayout/100.0)
	iLayout = iGeneralLayout - 100*iNumHundreds
	PRINTLN("GET_SPECIAL_PED_LOCATION PED:", iPed, " GEN LAYOUT:", iGeneralLayout, " SPECIAL PEDS LAYOUT: ", iLayout)
	SWITCH iPed
		CASE 0
			SWITCH iLayout
				CASE 0
				CASE 1
				CASE 2
				CASE 3
				CASE 4
					//LOC 1
					vPos = <<-2210.978, 1087.570, -23.262>>
					fHead = 210.240
					iActivity = ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A)
				BREAK
				CASE 5
				CASE 6
				CASE 7
				CASE 8
				CASE 9	
					//LOC 2
					vPos = <<-2174.8560, 1095.6829, -24.362>>
					fHead = 74.9529
					iActivity = ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_C)
				BREAK
				CASE 10
				CASE 11
				CASE 12
				CASE 13
				CASE 14
					//LOC 3
					vPos = <<-2161.1113, 1130.4016, -24.362>>
					fHead = 7.9766
					iActivity = ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D)
				BREAK
				CASE 15
				CASE 16
				CASE 17	
				CASE 18
				CASE 19
					//LOC 4
					vPos = <<-2168.6770, 1114.3773, -24.362>>
					fHead = 238.0675
					iActivity = ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D)
				BREAK
				CASE 20
				CASE 21
				CASE 22
				CASE 23
				CASE 24
					//LOC 5
					vPos = <<-2196.9785, 1116.8539, -23.262>>
					fHead = 47.8405
					iActivity = ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F)
				BREAK
				CASE 25
				CASE 26
				CASE 27
				CASE 28
				CASE 29
					//LOC 6
					vPos = <<-2147.9866, 1078.2937, -24.362>>
					fHead = 266.3582
					iActivity = ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			SWITCH iLayout
				CASE 5
				CASE 10
				CASE 15
				CASE 20
				CASE 25
					//LOC 1
					vPos = <<-2210.978, 1087.570, -23.262>>
					fHead = 210.240
					iActivity = ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A)
				BREAK
				CASE 0
				CASE 11
				CASE 16
				CASE 21
				CASE 26
					//LOC 2
					vPos = <<-2174.8560, 1095.6829, -24.362>>
					fHead = 74.9529
					iActivity = ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_C)
				BREAK
				CASE 1
				CASE 6
				CASE 17
				CASE 22
				CASE 27
					//LOC 3
					vPos = <<-2161.1113, 1130.4016, -24.362>>
					fHead = 7.9766
					iActivity = ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D)
				BREAK
				CASE 2
				CASE 7
				CASE 12
				CASE 23
				CASE 28
					//LOC 4
					vPos = <<-2168.6770, 1114.3773, -24.362>>
					fHead = 238.0675
					iActivity = ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D)
				BREAK
				CASE 3
				CASE 8
				CASE 13
				CASE 18
				CASE 29
					//LOC 5
					vPos = <<-2196.9785, 1116.8539, -23.262>>
					fHead = 47.8405
					iActivity = ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F)
				BREAK
				CASE 4
				CASE 9
				CASE 14
				CASE 19
				CASE 24
					//LOC 6
					vPos = <<-2147.9866, 1078.2937, -24.362>>
					fHead = 266.3582
					iActivity = ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D)
				BREAK
			ENDSWITCH
		BREAK	
	ENDSWITCH
ENDPROC

#IF FEATURE_GEN9_EXCLUSIVE
FUNC BOOL DOES_FIRST_SPECIAL_VEHICLE_INCLUDE_A_PED()
	IF g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-2] = PED_HAO
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC
#ENDIF

FUNC BOOL DOES_SECOND_SPECIAL_VEHICLE_INCLUDE_A_PED()
	#IF FEATURE_GEN9_EXCLUSIVE
		IF g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-1] = PED_HAO
			RETURN FALSE
		ENDIF
	#ENDIF
	RETURN (ENUM_TO_INT(g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-1])<ENUM_TO_INT(CAR_MEET_SPECIAL_PED_COUNT))
ENDFUNC

PROC GET_SPECIAL_PED_INFO(INT iLayout, PED_INDEX iPedIndex, int iSpecialPed, int &iModel, VECTOR &vPosition, FLOAT &fHeading, INT &iActivity)
	UNUSED_PARAMETER(iPedIndex)
	SWITCH g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+iSpecialPed]
		CASE PED_MOODYMAN 								
			iModel = ENUM_TO_INT(PED_MODEL_CAR_MEET_MOODYMANN) 
		BREAK
		CASE PED_HAO	 								
			iModel = ENUM_TO_INT(PED_MODEL_HAO)
		BREAK
		CASE PED_BENNY 									
			iModel = ENUM_TO_INT(PED_MODEL_BENNY)	
		BREAK
		CASE PED_BENNYS_MECHANIC 						
			iModel = ENUM_TO_INT(PED_MODEL_BENNYS_MECHANIC)    
		BREAK
		CASE PED_JOHNNY_JONES 					 		
			iModel = ENUM_TO_INT(PED_MODEL_JOHNNY_JONES)    
		BREAK
		CASE PED_LS_CUSTOMS_MECHANIC 					
			iModel = ENUM_TO_INT(PED_MODEL_LSC_MECHANIC)   
		BREAK
		CASE PED_WAREHOUSE_MECHANIC 					
			iModel = ENUM_TO_INT(PED_MODEL_WAREHOUSE_MECHANIC) 
		BREAK
	ENDSWITCH
	GET_SPECIAL_PED_LOCATION_AND_ANIMATION(iLayout, iSpecialPed, vPosition, fHeading, iActivity)

	#IF IS_DEBUG_BUILD
		PRINTLN("[NET_PEDS_CAR_MEET][GET_SPECIAL_PED_INFO] SPAWNING SPECIAL PED :",ENUM_TO_INT(g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+iSpecialPed])," Next to vehicle index: ", g_sCarMeetSpecialPedPositionData.iVehicleIndex[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+iSpecialPed])
	#ENDIF	
ENDPROC

FUNC BOOL IS_SPECIAL_PED_INDEX(int iPed)
	IF iPed = 94 OR iPed = 95
		RETURN TRUE
	ENDIF
	RETURN FALSE		
ENDFUNC

PROC GET_JACKER_PED_DATA(PEDS_DATA &Data, INT index)
	SWITCH index 
		CASE 0
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_CAR_MEET_JACKER_FEMALE)
			Data.iPackedDrawable[0]	= 0
			Data.iPackedDrawable[1]	= 0
			Data.iPackedDrawable[2]	= 0
			Data.iPackedTexture[0] 	= 0
			Data.iPackedTexture[1] 	= 0
			Data.iPackedTexture[2] 	= 0
		BREAK
		CASE 1
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_CAR_MEET_JACKER_FEMALE)
			Data.iPackedDrawable[0]	= 16842753
			Data.iPackedDrawable[1]	= 1
			Data.iPackedDrawable[2]	= 1
			Data.iPackedTexture[0] 	= 16777216
			Data.iPackedTexture[1] 	= 0
			Data.iPackedTexture[2] 	= 0
		BREAK
		CASE 2
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_CAR_MEET_JACKER_FEMALE)
			Data.iPackedDrawable[0]	= 33685506
			Data.iPackedDrawable[1]	= 2
			Data.iPackedDrawable[2]	= 2
			Data.iPackedTexture[0] 	= 0
			Data.iPackedTexture[1] 	= 0
			Data.iPackedTexture[2] 	= 0
		BREAK
		CASE 3
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_CAR_MEET_JACKER_FEMALE)
			Data.iPackedDrawable[0]	= 50528259
			Data.iPackedDrawable[1]	= 3
			Data.iPackedDrawable[2]	= 3
			Data.iPackedTexture[0] 	= 50331648
			Data.iPackedTexture[1] 	= 3
			Data.iPackedTexture[2] 	= 0
		BREAK
		CASE 4
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_CAR_MEET_JACKER_FEMALE)
			Data.iPackedDrawable[0]	= 67371012
			Data.iPackedDrawable[1]	= 4
			Data.iPackedDrawable[2]	= 4
			Data.iPackedTexture[0] 	= 16777216
			Data.iPackedTexture[1] 	= 2
			Data.iPackedTexture[2] 	= 0
		BREAK
		CASE 5
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_CAR_MEET_JACKER_MALE)
			Data.iPackedDrawable[0]	= 0
			Data.iPackedDrawable[1]	= 0
			Data.iPackedDrawable[2]	= 0
			Data.iPackedTexture[0] 	= 0
			Data.iPackedTexture[1] 	= 2
			Data.iPackedTexture[2] 	= 0
		BREAK
		CASE 6
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_CAR_MEET_JACKER_MALE)
			Data.iPackedDrawable[0]	= 16842753
			Data.iPackedDrawable[1]	= 1
			Data.iPackedDrawable[2]	= 0
			Data.iPackedTexture[0] 	= 0
			Data.iPackedTexture[1] 	= 0
			Data.iPackedTexture[2] 	= 0
		BREAK
		CASE 7
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_CAR_MEET_JACKER_MALE)
			Data.iPackedDrawable[0]	= 33685506
			Data.iPackedDrawable[1]	= 2
			Data.iPackedDrawable[2]	= 0
			Data.iPackedTexture[0] 	= 0
			Data.iPackedTexture[1] 	= 0
			Data.iPackedTexture[2] 	= 0
		BREAK
		CASE 8
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_CAR_MEET_JACKER_MALE)
			Data.iPackedDrawable[0]	= 50528259
			Data.iPackedDrawable[1]	= 3
			Data.iPackedDrawable[2]	= 0
			Data.iPackedTexture[0] 	= 0
			Data.iPackedTexture[1] 	= 0
			Data.iPackedTexture[2] 	= 0
		BREAK
		CASE 9
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_CAR_MEET_JACKER_MALE)
			Data.iPackedDrawable[0]	= 67371012
			Data.iPackedDrawable[1]	= 4
			Data.iPackedDrawable[2]	= 0
			Data.iPackedTexture[0] 	= 0
			Data.iPackedTexture[1] 	= 0
			Data.iPackedTexture[2] 	= 0
		BREAK
	ENDSWITCH
ENDPROC
PROC SET_JACKER_PROP_DATA(PED_INDEX PedID, INT index)
	PRINTLN("SETTING PROP INFO FOR PED ", index)
	SWITCH index
		CASE 5	SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION, 0), 0)		BREAK
		CASE 6	SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION, 1), 0)		BREAK
		CASE 9	SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION, 0), 1)		BREAK	
	ENDSWITCH	
ENDPROC
PROC SET_JACKER_PED_DATA(PEDS_DATA &Data, INT iPed, INT iLayout)
	IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup != -1
		SWITCH iLayout
			CASE 0
				SWITCH iPed			
					//GROUP 1
					CASE 16
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 0
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[0])
						ENDIF
					BREAK
					CASE 17
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 0
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[1])
						ENDIF
					BREAK
					CASE 18
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 0
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[2])
						ENDIF
					BREAK
					//GROUP 2
					CASE 19
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 1
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[0])
						ENDIF
					BREAK
					CASE 61
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 1
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[1])
						ENDIF
					BREAK
					CASE 62
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 1
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[2])
						ENDIF
					BREAK
					//GROUP 3
					CASE 52
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 2
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[0])
						ENDIF
					BREAK
					CASE 53
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 2
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[1])
						ENDIF
					BREAK
					CASE 54
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 2
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[2])
						ENDIF
					BREAK
				ENDSWITCH
			BREAK
			CASE 1
				SWITCH iPed		
					//GROUP 1
					CASE 44
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 0
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[0])
						ENDIF
					BREAK
					CASE 48
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 0
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[1])
						ENDIF
					BREAK
					CASE 84
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 0
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[2])
						ENDIF
					BREAK
					//GROUP 2
					CASE 64
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 1
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[0])
						ENDIF
					BREAK
					CASE 65
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 1
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[1])
						ENDIF
					BREAK
					CASE 66
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 1
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[2])
						ENDIF
					BREAK
					//GROUP 3
					CASE 11
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 2
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[0])
						ENDIF
					BREAK
					CASE 13
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 2
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[1])
						ENDIF
					BREAK
					CASE 14
						IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 2
							GET_JACKER_PED_DATA(Data, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[2])
						ENDIF
					BREAK				
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC GET_CAR_MEET_PED_PATROL_DATA(PEDS_DATA &Data, INT iPed, INT iLayout)
	
	IF (iPed = NETWORK_PED_ID_0)
		IF (iLayout >= 0 AND iLayout <= 29)			// Ambient Layout 0
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
			Data.iPackedDrawable[0]	= 4
			Data.iPackedDrawable[1]	= 0
			Data.iPackedDrawable[2]	= 0
			Data.iPackedTexture[0] 	= 0
			Data.iPackedTexture[1] 	= 0
			Data.iPackedTexture[2] 	= 0
			
		ELIF (iLayout >= 100 AND iLayout <= 129)	// Ambient Layout 1
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
			Data.iPackedDrawable[0]	= 33619973
			Data.iPackedDrawable[1]	= 769
			Data.iPackedDrawable[2]	= 0
			Data.iPackedTexture[0] 	= 33619970
			Data.iPackedTexture[1] 	= 518
			Data.iPackedTexture[2] 	= 0
		ENDIF
		
	ELIF (iPed = NETWORK_PED_ID_1)
		IF (iLayout >= 0 AND iLayout <= 29)			// Ambient Layout 0
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
			Data.iPackedDrawable[0]	= 33619973
			Data.iPackedDrawable[1]	= 769
			Data.iPackedDrawable[2]	= 0
			Data.iPackedTexture[0] 	= 33619970
			Data.iPackedTexture[1] 	= 518
			Data.iPackedTexture[2] 	= 0
			
		ELIF (iLayout >= 100 AND iLayout <= 129)	// Ambient Layout 1
			Data.iPedModel 			= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
			Data.iPackedDrawable[0]	= 4
			Data.iPackedDrawable[1]	= 0
			Data.iPackedDrawable[2]	= 0
			Data.iPackedTexture[0] 	= 0
			Data.iPackedTexture[1] 	= 0
			Data.iPackedTexture[2] 	= 0
		ENDIF
	ENDIF
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ PED DATA ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛
PROC _SET_CAR_MEET_PED_DATA_LAYOUT_0(PEDS_DATA &Data, INT iPed, INT iLayout, BOOL bSetPedArea = TRUE)
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_ON_PHONE_M_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 33816577
					Data.iPackedDrawable[1]								= 131585
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842753
					Data.iPackedTexture[1] 								= 131329
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<27.7905, 44.3623, 2.1071>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16908292
					Data.iPackedDrawable[1]								= 131330
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 65538
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-34.7374, -9.8076, 0.9944>>
					Data.vRotation 										= <<0.0000, 0.0000, 131.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_A	
					Data.iPackedDrawable[0]								= 17170436
					Data.iPackedDrawable[1]								= 259
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33685504
					Data.iPackedTexture[1] 								= 65544
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-32.2675, -6.9976, 0.9944>>
					Data.vRotation 										= <<0.0000, 0.0000, 298.0800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A_TRANS_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 393219
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<9.9675, -2.9985, 0.9944>>
					Data.vRotation 										= <<0.0000, 0.0000, 153.3600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 4
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_STANDING_SMOKING_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50462720
					Data.iPackedDrawable[1]								= 769
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842752
					Data.iPackedTexture[1] 								= 131329
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<21.5920, 35.6499, 2.1116>>
					Data.vRotation 										= <<0.0000, 0.0000, 26.7950>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 5
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67174400
					Data.iPackedDrawable[1]								= 65539
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 11
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<20.8430, 35.3689, 2.1032>>
					Data.vRotation 										= <<0.0000, 0.0000, 2.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 6
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F_TRANS_H)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 33751045
					Data.iPackedDrawable[1]								= 66048
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 66304
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<24.6536, 27.9143, 2.1002>>
					Data.vRotation 										= <<0.0000, 0.0000, 223.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 7
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_E)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 2
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 65794
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<1.9255, 2.9695, 1.0004>>
					Data.vRotation 										= <<0.0000, 0.0000, 334.0800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 8
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777222
					Data.iPackedDrawable[1]								= 131330
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 518
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.9355, 3.0095, 1.0004>>
					Data.vRotation 										= <<0.0000, 0.0000, 2.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 9
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_G_TRANS_H)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50528261
					Data.iPackedDrawable[1]								= 257
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777218
					Data.iPackedTexture[1] 								= 770
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<6.9996, 28.7405, 2.1004>>
					Data.vRotation 										= <<0.0000, 0.0000, 113.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 10
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65536
					Data.iPackedDrawable[1]								= 65537
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 65536
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<6.3766, 29.6794, 2.1007>>
					Data.vRotation 										= <<0.0000, 0.0000, 138.2400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 11
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279300
					Data.iPackedDrawable[1]								= 1281
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842754
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<11.4310, 39.0688, 2.1083>>
					Data.vRotation 										= <<0.0000, 0.0000, 186.4800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 12
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67371009
					Data.iPackedDrawable[1]								= 65537
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 256
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<14.6660, 38.6223, 2.1014>>
					Data.vRotation 										= <<0.0000, 0.0000, 165.6000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 13
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 2
					Data.iPackedDrawable[1]								= 131072
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842753
					Data.iPackedTexture[1] 								= 257
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<12.4196, 38.8613, 2.1029>>
					Data.vRotation 										= <<0.0000, 0.0000, 165.6000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 14
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_FEMALE_A)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 50593794
					Data.iPackedDrawable[1]								= 131841
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554433
					Data.iPackedTexture[1] 								= 131847
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<10.5521, 38.5610, 2.1021>>
					Data.vRotation 										= <<0.0000, 0.0000, 228.2400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 15
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_F_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 33619973
					Data.iPackedDrawable[1]								= 66049
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16908288
					Data.iPackedTexture[1] 								= 522
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<8.6035, 38.4873, 2.0284>>
					Data.vRotation 										= <<0.0000, 0.0000, 228.2400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 16
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_C)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 17104902
					Data.iPackedDrawable[1]								= 65536
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554432
					Data.iPackedTexture[1] 								= 66305
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-18.4445, 41.9395, 2.1017>>
					Data.vRotation 										= <<0.0000, 0.0000, 336.2400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 17
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_F_TRANS_H)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84017156
					Data.iPackedDrawable[1]								= 131842
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 16777216
					Data.iPackedTexture[1] 								= 131584
					Data.iPackedTexture[2] 								= 1
					Data.vPosition 										= <<-14.2804, 44.2295, 2.1005>>
					Data.vRotation 										= <<0.0000, 0.0000, 118.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 18
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_A_TRANS_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50724867
					Data.iPackedDrawable[1]								= 258
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 514
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-14.3784, 43.0425, 2.1010>>
					Data.vRotation 										= <<0.0000, 0.0000, 62.6400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 19
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_G_TRANS_H)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 33554434
					Data.iPackedDrawable[1]								= 131586
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 257
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-2.4844, -5.7107, 1.0004>>
					Data.vRotation 										= <<0.0000, 0.0000, 243.3600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 20
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_B)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 50593794
					Data.iPackedDrawable[1]								= 769
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 779
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-8.5485, 2.6414, 0.9914>>
					Data.vRotation 										= <<0.0000, 0.0000, 15.8400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 21
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 196613
					Data.iPackedDrawable[1]								= 65539
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 66309
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-34.7675, -10.4976, 0.9944>>
					Data.vRotation 										= <<0.0000, 0.0000, 54.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
				BREAK
			ENDSWITCH
		BREAK
		CASE 22
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_DRINK_COFFEE_F)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 131072
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 65797
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-21.3544, 38.5544, 2.1126>>
					Data.vRotation 										= <<0.0000, 0.0000, 40.3200>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 23
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_ON_PHONE_M_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 50659334
					Data.iPackedDrawable[1]								= 257
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 66306
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-22.1575, 38.4924, 2.0984>>
					Data.vRotation 										= <<0.0000, 0.0000, 2.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 24
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84213763
					Data.iPackedDrawable[1]								= 132352
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 65544
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-22.9204, 24.5483, 2.1088>>
					Data.vRotation 										= <<0.0000, 0.0000, 15.1200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 25
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_G_TRANS_H)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 83886082
					Data.iPackedDrawable[1]								= 769
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 131328
					Data.iPackedTexture[2] 								= 2
					Data.vPosition 										= <<-26.7744, 39.5054, 2.1018>>
					Data.vRotation 										= <<0.0000, 0.0000, 170.6400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 26
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 83951616
					Data.iPackedDrawable[1]								= 66305
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 65536
					Data.iPackedTexture[1] 								= 131073
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-27.8984, 39.3074, 2.1003>>
					Data.vRotation 										= <<0.0000, 0.0000, 205.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 27
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E_TRANS_G)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16973829
					Data.iPackedDrawable[1]								= 131073
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777216
					Data.iPackedTexture[1] 								= 769
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-10.2729, 17.1770, 2.0986>>
					Data.vRotation 										= <<0.0000, 0.0000, 210.9600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 28
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_A_TRANS_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67305473
					Data.iPackedDrawable[1]								= 66560
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842754
					Data.iPackedTexture[1] 								= 131337
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<26.6746, 28.0305, 2.1120>>
					Data.vRotation 										= <<0.0000, 0.0000, 136.0800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 29
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A_TRANS_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16842757
					Data.iPackedDrawable[1]								= 131328
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 66058
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<25.7606, 28.2534, 2.1076>>
					Data.vRotation 										= <<0.0000, 0.0000, 179.2800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 30
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_PAPARAZZI)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50462724
					Data.iPackedDrawable[1]								= 131330
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 131585
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<17.3706, 20.5803, 2.1004>>
					Data.vRotation 										= <<0.0000, 0.0000, 172.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHANGE_CAPSULE_SIZE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 31
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 196609
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33685505
					Data.iPackedTexture[1] 								= 65797
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<7.8945, 18.2524, 2.1024>>
					Data.vRotation 										= <<0.0000, 0.0000, 150.4800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 32
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B_TRANS_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67239936
					Data.iPackedDrawable[1]								= 132096
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 131329
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<6.9656, 18.1675, 2.1016>>
					Data.vRotation 										= <<0.0000, 0.0000, 179.2800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 33
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E_TRANS_H)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67239940
					Data.iPackedDrawable[1]								= 131073
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 66048
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-39.9924, 31.7075, 2.1084>>
					Data.vRotation 										= <<0.0000, 0.0000, 218.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 34
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_C_TRANS_D)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 33816577
					Data.iPackedDrawable[1]								= 66048
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 256
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-37.6184, 30.6013, 2.1084>>
					Data.vRotation 										= <<0.0000, 0.0000, 89.2800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 35
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A_TRANS_B)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 67436550
					Data.iPackedDrawable[1]								= 131072
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 131841
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-37.9534, 31.5764, 2.1084>>
					Data.vRotation 										= <<0.0000, 0.0000, 118.0800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 36
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131076
					Data.iPackedDrawable[1]								= 131330
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 257
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-38.9924, 31.9585, 2.1084>>
					Data.vRotation 										= <<0.0000, 0.0000, 182.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 37
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_B_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33947652
					Data.iPackedDrawable[1]								= 131329
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33685504
					Data.iPackedTexture[1] 								= 65544
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-38.1060, 0.1365, 0.9966>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 38
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_C_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33554438
					Data.iPackedDrawable[1]								= 131587
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33685504
					Data.iPackedTexture[1] 								= 514
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-10.7780, 15.9790, 2.0984>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 39
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50593793
					Data.iPackedDrawable[1]								= 66306
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 131330
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-39.1949, 0.0925, 0.9974>>
					Data.vRotation 										= <<0.0000, 0.0000, 209.5200>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 40
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33619968
					Data.iPackedDrawable[1]								= 515
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 11
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-16.4974, 3.2444, 0.9966>>
					Data.vRotation 										= <<0.0000, 0.0000, 355.6800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 41
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B_TRANS_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16973829
					Data.iPackedDrawable[1]								= 65536
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 770
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-15.5314, 3.5024, 0.9973>>
					Data.vRotation 										= <<0.0000, 0.0000, 45.3600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 42
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_STANDING_SMOKING_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84082689
					Data.iPackedDrawable[1]								= 132355
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777216
					Data.iPackedTexture[1] 								= 131329
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.5044, 2.4944, 0.9981>>
					Data.vRotation 										= <<0.0000, 0.0000, 334.0800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 43
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131076
					Data.iPackedDrawable[1]								= 65536
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 66048
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-6.0184, 2.9915, 0.9919>>
					Data.vRotation 										= <<0.0000, 0.0000, 295.9200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 44
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777222
					Data.iPackedDrawable[1]								= 65794
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 522
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-16.9724, -2.7815, 1.0069>>
					Data.vRotation 										= <<0.0000, 0.0000, 190.0800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 45
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67174405
					Data.iPackedDrawable[1]								= 132097
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 66050
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.6919, -13.3291, 0.9967>>
					Data.vRotation 										= <<0.0000, 0.0000, 118.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 46
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_D)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 33685508
					Data.iPackedDrawable[1]								= 131586
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 513
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.6869, -14.4290, 0.9940>>
					Data.vRotation 										= <<0.0000, 0.0000, 54.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 47
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_E)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 67371009
					Data.iPackedDrawable[1]								= 131073
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 65792
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-12.0385, -6.3765, 0.9944>>
					Data.vRotation 										= <<0.0000, 0.0000, 79.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 48
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E_TRANS_F)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 67502083
					Data.iPackedDrawable[1]								= 131074
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 131586
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-17.7975, -3.0916, 0.9999>>
					Data.vRotation 										= <<0.0000, 0.0000, 221.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 49
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50528257
					Data.iPackedDrawable[1]								= 66305
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 65797
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-23.6794, -23.9675, 1.0069>>
					Data.vRotation 										= <<0.0000, 0.0000, 148.3200>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 50
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50659334
					Data.iPackedDrawable[1]								= 65792
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65536
					Data.iPackedTexture[1] 								= 66305
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-25.3324, -23.9705, 1.0066>>
					Data.vRotation 										= <<0.0000, 0.0000, 214.5600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 51
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33816578
					Data.iPackedDrawable[1]								= 131586
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 775
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-24.4495, -23.7527, 1.0066>>
					Data.vRotation 										= <<0.0000, 0.0000, 179.2800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 52
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_H)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 2
					Data.iPackedDrawable[1]								= 65538
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 257
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-36.4910, -19.5581, 1.0022>>
					Data.vRotation 										= <<0.0000, 0.0000, 64.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 53
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 17039361
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554434
					Data.iPackedTexture[1] 								= 258
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-37.4744, -20.1707, 0.9956>>
					Data.vRotation 										= <<0.0000, 0.0000, 24.4800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 54
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 83951621
					Data.iPackedDrawable[1]								= 66816
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33685504
					Data.iPackedTexture[1] 								= 131586
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-37.2764, -17.5776, 1.0080>>
					Data.vRotation 										= <<0.0000, 0.0000, 140.4000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 55
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84213766
					Data.iPackedDrawable[1]								= 131843
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 66308
					Data.iPackedTexture[2] 								= 1
					Data.vPosition 										= <<-36.6544, -18.4265, 0.9963>>
					Data.vRotation 										= <<0.0000, 0.0000, 110.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 56
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279299
					Data.iPackedDrawable[1]								= 66306
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 33554433
					Data.iPackedTexture[1] 								= 131585
					Data.iPackedTexture[2] 								= 2
					Data.vPosition 										= <<-20.4235, -17.7427, 1.0021>>
					Data.vRotation 										= <<0.0000, 0.0000, 12.9600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 57
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50397189
					Data.iPackedDrawable[1]								= 769
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 518
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-21.4574, -17.4956, 0.9936>>
					Data.vRotation 										= <<0.0000, 0.0000, 316.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 58
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 2
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842752
					Data.iPackedTexture[1] 								= 256
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.3024, -14.3047, 0.9896>>
					Data.vRotation 										= <<0.0000, 0.0000, 313.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 59
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F_TRANS_G)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16908292
					Data.iPackedDrawable[1]								= 65537
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 514
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-3.3634, -12.9375, 0.9944>>
					Data.vRotation 										= <<0.0000, 0.0000, 241.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 60
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_LEANING_TEXTING_F_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 84082689
					Data.iPackedDrawable[1]								= 132352
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 261
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<7.8971, -3.2690, 0.9940>>
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 61
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_B_TRANS_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33554438
					Data.iPackedDrawable[1]								= 66051
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 522
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<0.1855, -2.8335, 1.0006>>
					Data.vRotation 										= <<0.0000, 0.0000, 154.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 62
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 17104899
					Data.iPackedDrawable[1]								= 259
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842753
					Data.iPackedTexture[1] 								= 65540
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-0.8044, -3.0505, 1.0001>>
					Data.vRotation 										= <<0.0000, 0.0000, 208.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
				BREAK
			ENDSWITCH
		BREAK
		CASE 63
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65540
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842754
					Data.iPackedTexture[1] 								= 131584
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<32.2075, -24.4817, 0.9966>>
					Data.vRotation 										= <<0.0000, 0.0000, 238.3200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					
					#IF FEATURE_GEN9_EXCLUSIVE
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					#ENDIF	// Hide in Gen9 for Hao Special Vehicle.
				BREAK
			ENDSWITCH
		BREAK
		CASE 64
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_D)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 84082693
					Data.iPackedDrawable[1]								= 771
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 66307
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<22.5631, 5.2573, 0.9878>>
					Data.vRotation 										= <<0.0000, 0.0000, 293.0400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 65
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279300
					Data.iPackedDrawable[1]								= 66817
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554433
					Data.iPackedTexture[1] 								= 65536
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<23.1080, 4.2290, 0.9914>>
					Data.vRotation 										= <<0.0000, 0.0000, 324.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 66
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_G)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 83886082
					Data.iPackedDrawable[1]								= 66306
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 33554432
					Data.iPackedTexture[1] 								= 131328
					Data.iPackedTexture[2] 								= 2
					Data.vPosition 										= <<24.1906, 3.5730, 0.9909>>
					Data.vRotation 										= <<0.0000, 0.0000, 2.8800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 67
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_STANDING_SMOKING_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 327683
					Data.iPackedDrawable[1]								= 131074
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33685504
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<21.5156, -3.4436, 0.9963>>
					Data.vRotation 										= <<0.0000, 0.0000, 193.6800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 68
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_D)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 50397184
					Data.iPackedDrawable[1]								= 259
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842752
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<19.0526, -3.4006, 0.9875>>
					Data.vRotation 										= <<0.0000, 0.0000, 200.8800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 69
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_B)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 16973829
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842754
					Data.iPackedTexture[1] 								= 131845
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<28.7426, -5.9937, 0.9878>>
					Data.vRotation 										= <<0.0000, 0.0000, 104.4000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 70
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_G)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 83886082
					Data.iPackedDrawable[1]								= 66306
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 16842754
					Data.iPackedTexture[1] 								= 256
					Data.iPackedTexture[2] 								= 1
					Data.vPosition 										= <<21.1926, -16.3945, 0.9886>>
					Data.vRotation 										= <<0.0000, 0.0000, 334.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 71
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_F_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 84279300
					Data.iPackedDrawable[1]								= 1282
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 131080
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<17.4426, -17.1545, 1.0002>>
					Data.vRotation 										= <<0.0000, 0.0000, 309.6000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 72
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_E)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 16908292
					Data.iPackedDrawable[1]								= 65536
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 66049
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<29.5806, -19.8425, 0.9937>>
					Data.vRotation 										= <<0.0000, 0.0000, 28.0800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 73
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_C_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33816578
					Data.iPackedDrawable[1]								= 66048
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 775
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<25.0636, -19.2095, 0.9953>>
					Data.vRotation 										= <<0.0000, 0.0000, 306.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 74
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_F_TRANS_G)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 50659334
					Data.iPackedDrawable[1]								= 131328
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65536
					Data.iPackedTexture[1] 								= 770
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<30.3265, -19.2085, 0.9956>>
					Data.vRotation 										= <<0.0000, 0.0000, 56.8800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 75
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_B)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 262146
					Data.iPackedDrawable[1]								= 131074
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65536
					Data.iPackedTexture[1] 								= 131843
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<34.2325, -24.3777, 0.9994>>
					Data.vRotation 										= <<0.0000, 0.0000, 127.4400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					
					#IF FEATURE_GEN9_EXCLUSIVE
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					#ENDIF	// Hide in Gen9 for Hao Special Vehicle.
				BREAK
			ENDSWITCH
		BREAK
		CASE 76
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 393219
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 66054
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<33.2275, -24.2856, 0.9879>>
					Data.vRotation 										= <<0.0000, 0.0000, 187.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					
					#IF FEATURE_GEN9_EXCLUSIVE
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					#ENDIF	// Hide in Gen9 for Hao Special Vehicle.
				BREAK
			ENDSWITCH
		BREAK
		CASE 77
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_ON_PHONE_M_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 67371009
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 65793
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<18.0455, -16.4705, 0.9884>>
					Data.vRotation 										= <<0.0000, 0.0000, 183.6000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 78
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33619968
					Data.iPackedDrawable[1]								= 515
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777218
					Data.iPackedTexture[1] 								= 131082
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<28.9896, -7.0105, 0.9882>>
					Data.vRotation 										= <<0.0000, 0.0000, 79.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 79
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_STANDING_SMOKING_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67436547
					Data.iPackedDrawable[1]								= 66563
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554434
					Data.iPackedTexture[1] 								= 65536
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<22.0486, -16.4456, 0.9997>>
					Data.vRotation 										= <<0.0000, 0.0000, 32.4000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 80
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777218
					Data.iPackedDrawable[1]								= 65538
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 131330
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-20.8774, 18.4224, 2.0984>>
					Data.vRotation 										= <<0.0000, 0.0000, 214.5600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 81
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_C_TRANS_D)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_B	
					Data.iPackedDrawable[0]								= 84083717
					Data.iPackedDrawable[1]								= 66817
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33620224
					Data.iPackedTexture[1] 								= 65536
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<3.9481, -28.2766, 1.1985>>
					Data.vRotation 										= <<0.0000, 0.0000, 65.6250>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 82
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A_TRANS_C)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 6
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16908288
					Data.iPackedTexture[1] 								= 131590
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<3.4061, -29.8950, 1.1985>>
					Data.vRotation 										= <<0.0000, 0.0000, 29.3600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 83
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50528257
					Data.iPackedDrawable[1]								= 66306
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16908289
					Data.iPackedTexture[1] 								= 131333
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-19.8374, 18.5425, 2.0984>>
					Data.vRotation 										= <<0.0000, 0.0000, 159.8400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 84
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33947651
					Data.iPackedDrawable[1]								= 512
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 131584
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-16.0325, -3.0215, 0.9974>>
					Data.vRotation 										= <<0.0000, 0.0000, 146.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 85
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_G_TRANS_H)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 16842752
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842754
					Data.iPackedTexture[1] 								= 65537
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-29.7074, 18.3125, 2.0944>>
					Data.vRotation 										= <<0.0000, 0.0000, 154.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 86
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_PAPARAZZI)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 327686
					Data.iPackedDrawable[1]								= 131075
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619969
					Data.iPackedTexture[1] 								= 131844
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<18.6166, -5.4966, 0.9883>>
					Data.vRotation 										= <<0.0000, 0.0000, 224.6400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHANGE_CAPSULE_SIZE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 87
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50659334
					Data.iPackedDrawable[1]								= 131329
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554434
					Data.iPackedTexture[1] 								= 66306
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<22.2375, -3.4006, 0.9998>>
					Data.vRotation 										= <<0.0000, 0.0000, 169.9200>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 88
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_A)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 67239936
					Data.iPackedDrawable[1]								= 1026
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131073
					Data.iPackedTexture[1] 								= 265
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<25.2330, 4.1919, 0.9890>>
					Data.vRotation 										= <<0.0000, 0.0000, 33.1200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 89
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_MALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67371009
					Data.iPackedDrawable[1]								= 131843
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777218
					Data.iPackedTexture[1] 								= 65794
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<12.1276, 3.3015, 0.9944>>
					Data.vRotation 										= <<0.0000, 0.0000, 324.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 90
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67108870
					Data.iPackedDrawable[1]								= 66562
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 131586
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<13.2075, 3.2715, 0.9944>>
					Data.vRotation 										= <<0.0000, 0.0000, 22.3200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 91
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_STANDING_SMOKING_F_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 33816578
					Data.iPackedDrawable[1]								= 66049
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65536
					Data.iPackedTexture[1] 								= 66307
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-22.2184, 39.4065, 2.1093>>
					Data.vRotation 										= <<0.0000, 0.0000, 216.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 92
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_F_TRANS_G)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67502083
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 131585
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<13.5040, 38.2859, 2.1014>>
					Data.vRotation 										= <<-15.1200, 0.0000, 221.0400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 93
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_ON_PHONE_M_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 33619968
					Data.iPackedDrawable[1]								= 66051
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554434
					Data.iPackedTexture[1] 								= 131082
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<8.4835, 37.6785, 2.1047>>
					Data.vRotation 										= <<0.0000, 0.0000, 275.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 94		
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					GET_SPECIAL_PED_INFO(iLayout, Data.PedID, 0, Data.iPedModel, Data.vPosition, Data.vRotation.z, Data.iActivity)
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					#IF FEATURE_GEN9_EXCLUSIVE
					IF NOT DOES_FIRST_SPECIAL_VEHICLE_INCLUDE_A_PED()
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
					#ENDIF
				BREAK
			ENDSWITCH
		
		BREAK
		CASE 95
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					GET_SPECIAL_PED_INFO(iLayout, Data.PedID, 1, Data.iPedModel, Data.vPosition, Data.vRotation.z, Data.iActivity)
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					IF NOT DOES_SECOND_SPECIAL_VEHICLE_INCLUDE_A_PED()
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
			
		BREAK
		
		// Network Peds
		CASE NETWORK_PED_ID_0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_PATROL)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0] 								= 4
					Data.iPackedDrawable[1] 								= 0
					Data.iPackedDrawable[2] 								= 0
					Data.iPackedTexture[0] 									= 0
					Data.iPackedTexture[1] 									= 0
					Data.iPackedTexture[2] 									= 0
					Data.vPosition 										= <<15.4336, -0.8333, 0.9998>>
					Data.vRotation 										= <<0.0000, 0.0000, 266.3013>>
					GET_CAR_MEET_PED_PATROL_DATA(Data, iPed, iLayout)
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_NO_ANIMATION)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_PATROL)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
		CASE NETWORK_PED_ID_1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_PATROL)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0] 								= 33619973
					Data.iPackedDrawable[1] 								= 769
					Data.iPackedDrawable[2] 								= 0
					Data.iPackedTexture[0] 									= 33619970
					Data.iPackedTexture[1] 									= 518
					Data.iPackedTexture[2] 									= 0
					Data.vPosition 										= <<-13.6299, 16.6377, 1.1037>>
					Data.vRotation 										= <<0.0000, 0.0000, 177.5264>>
					GET_CAR_MEET_PED_PATROL_DATA(Data, iPed, iLayout)
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_NO_ANIMATION)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_PATROL)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	
	IF NOT IS_SPECIAL_PED_INDEX(iPed)
			// Transform the local coords into world coords.
		IF NOT IS_VECTOR_ZERO(Data.vPosition)
			Data.vPosition = TRANSFORM_LOCAL_COORDS_TO_WORLD_COORDS(_GET_CAR_MEET_PED_LOCAL_COORDS_BASE_POSITION(), _GET_CAR_MEET_PED_LOCAL_HEADING_BASE_HEADING(), Data.vPosition)
		ENDIF
		
		// Transform the local heading into world heading.
		IF (Data.vRotation.z != -1.0)
			Data.vRotation.z = TRANSFORM_LOCAL_HEADING_TO_WORLD_HEADING(_GET_CAR_MEET_PED_LOCAL_HEADING_BASE_HEADING(), Data.vRotation.z)
		ENDIF
	ENDIF
ENDPROC
PROC _SET_CAR_MEET_PED_DATA_LAYOUT_1(PEDS_DATA &Data, INT iPed, INT iLayout, BOOL bSetPedArea = TRUE)
	SWITCH iPed
		// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_ON_PHONE_M_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 33816577
					Data.iPackedDrawable[1]								= 131585
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842753
					Data.iPackedTexture[1] 								= 131329
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<27.7905, 44.3623, 2.1071>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_F)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16908292
					Data.iPackedDrawable[1]								= 131330
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 65538
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<10.5455, -2.9966, 0.9904>>
					Data.vRotation 										= <<0.0000, 0.0000, 164.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 17170436
					Data.iPackedDrawable[1]								= 259
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33685504
					Data.iPackedTexture[1] 								= 65544
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<9.6655, -2.9966, 0.9904>>
					Data.vRotation 										= <<0.0000, 0.0000, 196.5600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 393218
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 519
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<12.9255, 2.6934, 0.9904>>
					Data.vRotation 										= <<0.0000, 0.0000, 353.5200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 4
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_STANDING_SMOKING_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50462720
					Data.iPackedDrawable[1]								= 769
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842752
					Data.iPackedTexture[1] 								= 131329
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<21.5920, 35.6499, 2.1116>>
					Data.vRotation 										= <<0.0000, 0.0000, 26.7950>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 5
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67174400
					Data.iPackedDrawable[1]								= 65539
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 11
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<20.8430, 35.3689, 2.1032>>
					Data.vRotation 										= <<0.0000, 0.0000, 2.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 6
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_C)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 33751045
					Data.iPackedDrawable[1]								= 66048
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 66304
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<24.4336, 27.5444, 2.1002>>
					Data.vRotation 										= <<0.0000, 0.0000, 231.1200>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 7
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_G)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 2
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 65794
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<1.8156, 3.2395, 0.9904>>
					Data.vRotation 										= <<0.0000, 0.0000, 337.6800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 8
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777222
					Data.iPackedDrawable[1]								= 131330
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 518
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.8256, 3.1394, 0.9904>>
					Data.vRotation 										= <<0.0000, 0.0000, 0.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 9
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_F)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50528261
					Data.iPackedDrawable[1]								= 257
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777218
					Data.iPackedTexture[1] 								= 769
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<7.0295, 28.4805, 2.1004>>
					Data.vRotation 										= <<0.0000, 0.0000, 99.3600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 10
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65536
					Data.iPackedDrawable[1]								= 65539
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 65545
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<6.4565, 29.5095, 2.1007>>
					Data.vRotation 										= <<0.0000, 0.0000, 135.3600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 11
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279300
					Data.iPackedDrawable[1]								= 1281
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842754
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<11.4511, 38.7930, 2.1083>>
					Data.vRotation 										= <<0.0000, 0.0000, 186.4800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 12
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67371009
					Data.iPackedDrawable[1]								= 65537
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 256
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<14.8921, 38.4724, 2.1014>>
					Data.vRotation 										= <<0.0000, 0.0000, 148.3200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 13
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E_TRANS_G)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 2
					Data.iPackedDrawable[1]								= 131072
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842753
					Data.iPackedTexture[1] 								= 257
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<12.2861, 38.3179, 2.1029>>
					Data.vRotation 										= <<0.0000, 0.0000, 158.6550>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 14
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A_TRANS_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50593794
					Data.iPackedDrawable[1]								= 131841
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554433
					Data.iPackedTexture[1] 								= 131847
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<10.5980, 38.1160, 2.1121>>
					Data.vRotation 										= <<0.0000, 0.0000, 231.1200>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 15
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_F_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 33619973
					Data.iPackedDrawable[1]								= 66049
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16908288
					Data.iPackedTexture[1] 								= 522
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<8.6035, 38.4873, 2.0284>>
					Data.vRotation 										= <<0.0000, 0.0000, 228.2400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 16
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_G)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 17104902
					Data.iPackedDrawable[1]								= 65536
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554432
					Data.iPackedTexture[1] 								= 66305
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-20.7844, 42.2295, 2.1017>>
					Data.vRotation 										= <<0.0000, 0.0000, 293.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 17
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84017156
					Data.iPackedDrawable[1]								= 131842
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 16777216
					Data.iPackedTexture[1] 								= 131584
					Data.iPackedTexture[2] 								= 1
					Data.vPosition 										= <<-14.3904, 44.3994, 2.1005>>
					Data.vRotation 										= <<0.0000, 0.0000, 137.5200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 18
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_H)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50724867
					Data.iPackedDrawable[1]								= 258
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 514
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-14.2885, 43.1423, 2.1010>>
					Data.vRotation 										= <<0.0000, 0.0000, 65.5200>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 19
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_D)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 33554434
					Data.iPackedDrawable[1]								= 131586
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 257
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<2.5736, 39.0674, 2.0817>>
					Data.vRotation 										= <<0.0000, 0.0000, 191.5200>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 20
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_D)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 50593794
					Data.iPackedDrawable[1]								= 769
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 779
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<3.4736, 39.0674, 2.1028>>
					Data.vRotation 										= <<0.0000, 0.0000, 145.4400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 21
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F_TRANS_G)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 196613
					Data.iPackedDrawable[1]								= 65539
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 66309
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-29.1294, 18.6804, 2.1004>>
					Data.vRotation 										= <<0.0000, 0.0000, 149.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 22
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_F_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 131072
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 65797
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-21.3544, 38.5544, 2.1126>>
					Data.vRotation 										= <<0.0000, 0.0000, 40.3200>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 23
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_ON_PHONE_M_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 50659334
					Data.iPackedDrawable[1]								= 257
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 66306
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-22.1514, 38.4885, 2.1008>>
					Data.vRotation 										= <<0.0000, 0.0000, 2.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 24
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_F_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 84213763
					Data.iPackedDrawable[1]								= 132352
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 65544
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-21.5804, 39.5085, 2.1088>>
					Data.vRotation 										= <<0.0000, 0.0000, 169.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 25
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_H)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 83886082
					Data.iPackedDrawable[1]								= 769
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 131328
					Data.iPackedTexture[2] 								= 2
					Data.vPosition 										= <<-26.7759, 39.6528, 2.1018>>
					Data.vRotation 										= <<0.0000, 0.0000, 169.2500>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 26
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_F)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 83951616
					Data.iPackedDrawable[1]								= 66306
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 65536
					Data.iPackedTexture[1] 								= 131073
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-28.1869, 39.3669, 2.1003>>
					Data.vRotation 										= <<0.0000, 0.0000, 220.7950>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 27
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_E)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16973829
					Data.iPackedDrawable[1]								= 131074
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777216
					Data.iPackedTexture[1] 								= 769
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-10.1635, 17.0764, 2.0986>>
					Data.vRotation 										= <<0.0000, 0.0000, 210.9600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 28
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67305473
					Data.iPackedDrawable[1]								= 66560
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842754
					Data.iPackedTexture[1] 								= 131337
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<26.3146, 27.9805, 2.1120>>
					Data.vRotation 										= <<0.0000, 0.0000, 144.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 29
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16842757
					Data.iPackedDrawable[1]								= 131328
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 66058
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<25.4205, 28.0435, 2.1076>>
					Data.vRotation 										= <<0.0000, 0.0000, 185.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 30
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_PAPARAZZI)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50462724
					Data.iPackedDrawable[1]								= 131330
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 131585
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-23.3295, 20.6504, 2.1004>>
					Data.vRotation 										= <<0.0000, 0.0000, 180.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHANGE_CAPSULE_SIZE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 31
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_B_TRANS_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 196609
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33685505
					Data.iPackedTexture[1] 								= 65797
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<8.4346, 17.7324, 2.1124>>
					Data.vRotation 										= <<0.0000, 0.0000, 115.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 32
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67239936
					Data.iPackedDrawable[1]								= 132096
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 131329
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<7.4956, 18.2874, 2.1116>>
					Data.vRotation 										= <<0.0000, 0.0000, 158.4000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 33
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67239940
					Data.iPackedDrawable[1]								= 131073
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 66048
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-39.5024, 31.5173, 2.1084>>
					Data.vRotation 										= <<0.0000, 0.0000, 199.4400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 34
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_E)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 33816577
					Data.iPackedDrawable[1]								= 66048
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 256
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-38.2384, 26.9214, 2.1084>>
					Data.vRotation 										= <<0.0000, 0.0000, 48.9600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 35
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_C)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 67436550
					Data.iPackedDrawable[1]								= 131073
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 131841
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-37.9435, 27.9863, 2.1084>>
					Data.vRotation 										= <<0.0000, 0.0000, 88.5600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 36
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_H)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50462724
					Data.iPackedDrawable[1]								= 131330
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554433
					Data.iPackedTexture[1] 								= 513
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-38.2024, 31.1885, 2.1084>>
					Data.vRotation 										= <<0.0000, 0.0000, 138.2400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 37
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279300
					Data.iPackedDrawable[1]								= 132353
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131072
					Data.iPackedTexture[1] 								= 65544
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-39.3544, 0.0764, 1.0091>>
					Data.vRotation 										= <<0.0000, 0.0000, 218.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 38
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33554438
					Data.iPackedDrawable[1]								= 131587
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33685504
					Data.iPackedTexture[1] 								= 514
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-10.7134, 16.0364, 2.0984>>
					Data.vRotation 										= <<0.0000, 0.0000, 270.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 39
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50593793
					Data.iPackedDrawable[1]								= 65794
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 131330
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-38.6874, 0.5925, 0.9942>>
					Data.vRotation 										= <<0.0000, 0.0000, 195.1200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 40
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33619968
					Data.iPackedDrawable[1]								= 513
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 2
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-16.8074, 3.5044, 0.9966>>
					Data.vRotation 										= <<0.0000, 0.0000, 355.6800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 41
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_G)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16973829
					Data.iPackedDrawable[1]								= 65536
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 770
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-15.6115, 3.6924, 0.9973>>
					Data.vRotation 										= <<0.0000, 0.0000, 45.3600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 42
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_STANDING_SMOKING_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84082689
					Data.iPackedDrawable[1]								= 132355
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777216
					Data.iPackedTexture[1] 								= 131329
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-5.5044, 2.4944, 0.9981>>
					Data.vRotation 										= <<0.0000, 0.0000, 334.0800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 43
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 131076
					Data.iPackedDrawable[1]								= 65536
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65537
					Data.iPackedTexture[1] 								= 66048
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-6.0184, 2.9915, 0.9919>>
					Data.vRotation 										= <<0.0000, 0.0000, 295.9200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 44
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 16777222
					Data.iPackedDrawable[1]								= 65794
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 522
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-17.0840, -2.7815, 1.0069>>
					Data.vRotation 										= <<0.0000, 0.0000, 190.0800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 45
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67174405
					Data.iPackedDrawable[1]								= 132097
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 66050
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.9485, -14.2876, 0.9967>>
					Data.vRotation 										= <<0.0000, 0.0000, 62.6400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 46
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_F)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33685508
					Data.iPackedDrawable[1]								= 131586
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 513
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.8064, -13.1885, 0.9940>>
					Data.vRotation 										= <<0.0000, 0.0000, 123.8400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 47
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_E)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_B	
					Data.iPackedDrawable[0]								= 67371009
					Data.iPackedDrawable[1]								= 131073
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 65792
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-13.4285, -17.0767, 0.9944>>
					Data.vRotation 										= <<0.0000, 0.0000, 10.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 48
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_H)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_A
					Data.iPackedDrawable[0]								= 67502083
					Data.iPackedDrawable[1]								= 131074
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 131586
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-18.1189, -3.2012, 0.9999>>
					Data.vRotation 										= <<0.0000, 0.0000, 221.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 49
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_A_TRANS_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50528257
					Data.iPackedDrawable[1]								= 66305
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 65797
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-23.6794, -23.9675, 1.0069>>
					Data.vRotation 										= <<0.0000, 0.0000, 148.3200>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 50
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_H)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 50659334
					Data.iPackedDrawable[1]								= 65792
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65536
					Data.iPackedTexture[1] 								= 66305
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-25.3899, -23.9202, 1.0066>>
					Data.vRotation 										= <<0.0000, 0.0000, 214.5600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 51
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33816578
					Data.iPackedDrawable[1]								= 131586
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 775
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-24.4495, -23.7527, 1.0066>>
					Data.vRotation 										= <<0.0000, 0.0000, 179.2800>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 52
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 2
					Data.iPackedDrawable[1]								= 65539
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 257
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-36.6654, -19.0605, 1.0022>>
					Data.vRotation 										= <<0.0000, 0.0000, 64.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 53
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_H)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_A	
					Data.iPackedDrawable[0]								= 17039361
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554434
					Data.iPackedTexture[1] 								= 258
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-37.0679, -20.0210, 0.9956>>
					Data.vRotation 										= <<0.0000, 0.0000, 36.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 54
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 83951621
					Data.iPackedDrawable[1]								= 66816
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33685504
					Data.iPackedTexture[1] 								= 131586
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-37.7465, -17.1711, 1.0080>>
					Data.vRotation 										= <<0.0000, 0.0000, 165.6000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 55
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84213766
					Data.iPackedDrawable[1]								= 131842
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 66306
					Data.iPackedTexture[2] 								= 1
					Data.vPosition 										= <<-36.6210, -17.6760, 0.9963>>
					Data.vRotation 										= <<0.0000, 0.0000, 115.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 56
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF	

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_E)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279299
					Data.iPackedDrawable[1]								= 66306
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 33554433
					Data.iPackedTexture[1] 								= 131586
					Data.iPackedTexture[2] 								= 2
					Data.vPosition 										= <<-20.1079, -17.6521, 1.0021>>
					Data.vRotation 										= <<0.0000, 0.0000, 12.9600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 57
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50397189
					Data.iPackedDrawable[1]								= 769
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 518
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-21.1675, -17.3057, 0.9936>>
					Data.vRotation 										= <<0.0000, 0.0000, 316.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 58
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_A	
					Data.iPackedDrawable[0]								= 2
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842752
					Data.iPackedTexture[1] 								= 258
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.3524, -6.5046, 0.9996>>
					Data.vRotation 										= <<0.0000, 0.0000, 113.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 59
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_E)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_A	
					Data.iPackedDrawable[0]								= 16908292
					Data.iPackedDrawable[1]								= 65537
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 514
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-11.4534, -7.3477, 0.9944>>
					Data.vRotation 										= <<0.0000, 0.0000, 47.5200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 60
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_PAPARAZZI)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84082689
					Data.iPackedDrawable[1]								= 132352
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 257
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-24.4225, -19.7107, 0.9940>>
					Data.vRotation 										= <<0.0000, 0.0000, 0.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHANGE_CAPSULE_SIZE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 61
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33554438
					Data.iPackedDrawable[1]								= 66051
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 522
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-0.6919, -3.0781, 0.9906>>
					Data.vRotation 										= <<0.0000, 0.0000, 214.5600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 62
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_LEANING_TEXTING_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 17104899
					Data.iPackedDrawable[1]								= 259
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842753
					Data.iPackedTexture[1] 								= 65540
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-1.5209, -3.8950, 1.0001>>
					Data.vRotation 										= <<0.0000, 0.0000, 274.7900>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
				BREAK
			ENDSWITCH
		BREAK
		CASE 63
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B_TRANS_C)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 65540
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842754
					Data.iPackedTexture[1] 								= 131584
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<32.2476, -24.4717, 0.9966>>
					Data.vRotation 										= <<0.0000, 0.0000, 243.3600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					
					#IF FEATURE_GEN9_EXCLUSIVE
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					#ENDIF	// Hide in Gen9 for Hao Special Vehicle.
				BREAK
			ENDSWITCH
		BREAK
		CASE 64
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_B)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 84082693
					Data.iPackedDrawable[1]								= 771
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 66307
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<22.9155, 5.2175, 0.9878>>
					Data.vRotation 										= <<0.0000, 0.0000, 263.5200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 65
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84279300
					Data.iPackedDrawable[1]								= 66817
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554433
					Data.iPackedTexture[1] 								= 65536
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<24.1051, 3.8269, 0.9914>>
					Data.vRotation 										= <<0.0000, 0.0000, 0.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 66
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_F)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 83886082
					Data.iPackedDrawable[1]								= 66306
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 33554432
					Data.iPackedTexture[1] 								= 131328
					Data.iPackedTexture[2] 								= 2
					Data.vPosition 										= <<25.2921, 4.0220, 0.9909>>
					Data.vRotation 										= <<0.0000, 0.0000, 36.7200>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 67
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_STANDING_SMOKING_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 327683
					Data.iPackedDrawable[1]								= 131074
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33685504
					Data.iPackedTexture[1] 								= 4
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<21.5856, -3.4436, 0.9863>>
					Data.vRotation 										= <<0.0000, 0.0000, 173.7858>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 68
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50397184
					Data.iPackedDrawable[1]								= 259
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842752
					Data.iPackedTexture[1] 								= 9
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<19.0526, -3.4006, 0.9875>>
					Data.vRotation 										= <<0.0000, 0.0000, 208.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 69
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_DRINK_COFFEE_M)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 16973829
					Data.iPackedDrawable[1]								= 3
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842754
					Data.iPackedTexture[1] 								= 131845
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<27.1826, -2.8735, 0.9878>>
					Data.vRotation 										= <<0.0000, 0.0000, 138.2400>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 70
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 83886082
					Data.iPackedDrawable[1]								= 66306
					Data.iPackedDrawable[2]								= 1
					Data.iPackedTexture[0] 								= 16842754
					Data.iPackedTexture[1] 								= 256
					Data.iPackedTexture[2] 								= 1
					Data.vPosition 										= <<21.1926, -16.3945, 0.9886>>
					Data.vRotation 										= <<0.0000, 0.0000, 334.8000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 71
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_F_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 84279300
					Data.iPackedDrawable[1]								= 1282
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65538
					Data.iPackedTexture[1] 								= 131080
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<17.4525, -17.1545, 1.0002>>
					Data.vRotation 										= <<0.0000, 0.0000, 309.6000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 72
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_C)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 16908292
					Data.iPackedDrawable[1]								= 65536
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619968
					Data.iPackedTexture[1] 								= 66049
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<30.3105, -17.7227, 0.9937>>
					Data.vRotation 										= <<0.0000, 0.0000, 91.4400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 73
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A_TRANS_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33816578
					Data.iPackedDrawable[1]								= 66048
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131074
					Data.iPackedTexture[1] 								= 775
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<23.9136, -17.6397, 0.9953>>
					Data.vRotation 										= <<0.0000, 0.0000, 266.4000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 74
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_F_TRANS_G)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50659334
					Data.iPackedDrawable[1]								= 131328
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65536
					Data.iPackedTexture[1] 								= 770
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<24.4166, -18.6287, 0.9956>>
					Data.vRotation 										= <<0.0000, 0.0000, 324.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 75
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 262146
					Data.iPackedDrawable[1]								= 131074
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65536
					Data.iPackedTexture[1] 								= 131843
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<34.1826, -24.1377, 0.9994>>
					Data.vRotation 										= <<0.0000, 0.0000, 138.9600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					
					#IF FEATURE_GEN9_EXCLUSIVE
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					#ENDIF	// Hide in Gen9 for Hao Special Vehicle.
				BREAK
			ENDSWITCH
		BREAK
		CASE 76
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_F)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 393219
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 66048
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<33.1376, -23.9255, 0.9879>>
					Data.vRotation 										= <<0.0000, 0.0000, 187.2000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					
					#IF FEATURE_GEN9_EXCLUSIVE
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					#ENDIF	// Hide in Gen9 for Hao Special Vehicle.
				BREAK
			ENDSWITCH
		BREAK
		CASE 77
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_ON_PHONE_M_01)
					Data.iLevel 										= 1
					Data.iPackedDrawable[0]								= 67371009
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777217
					Data.iPackedTexture[1] 								= 65793
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<18.0455, -16.4705, 0.9884>>
					Data.vRotation 										= <<0.0000, 0.0000, 183.6000>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 78
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_F)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33619968
					Data.iPackedDrawable[1]								= 515
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777218
					Data.iPackedTexture[1] 								= 131082
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<28.9896, -7.0105, 0.9882>>
					Data.vRotation 										= <<0.0000, 0.0000, 114.4800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 79
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67436547
					Data.iPackedDrawable[1]								= 66563
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554434
					Data.iPackedTexture[1] 								= 65536
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<22.0486, -16.4456, 0.9997>>
					Data.vRotation 										= <<0.0000, 0.0000, 17.2800>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 80
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_F)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_B	
					Data.iPackedDrawable[0]								= 16777218
					Data.iPackedDrawable[1]								= 65577
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 1
					Data.iPackedTexture[1] 								= 131330
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<12.7506, -25.9465, 1.1948>>
					Data.vRotation 										= <<0.0000, 0.0000, 128.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 81
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_STANDING_SMOKING_F_01)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 84083717
					Data.iPackedDrawable[1]								= 66817
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33620224
					Data.iPackedTexture[1] 								= 65536
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-23.1494, 24.4805, 2.1004>>
					Data.vRotation 										= <<0.0000, 0.0000, 48.2400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 82
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_B)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 6
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16908288
					Data.iPackedTexture[1] 								= 131590
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-24.0094, 24.3105, 2.1004>>
					Data.vRotation 										= <<0.0000, 0.0000, 347.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 83
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B_TRANS_C)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_B
					Data.iPackedDrawable[0]								= 50528257
					Data.iPackedDrawable[1]								= 66306
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16908289
					Data.iPackedTexture[1] 								= 131333
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<12.6356, -26.8357, 1.2004>>
					Data.vRotation 										= <<0.0000, 0.0000, 69.1200>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 84
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 33947651
					Data.iPackedDrawable[1]								= 512
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 131585
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-15.9280, -3.1750, 0.9974>>
					Data.vRotation 										= <<0.0000, 0.0000, 146.1600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 85
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_HANGOUT_M_01)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_A	
					Data.iPackedDrawable[0]								= 16842752
					Data.iPackedDrawable[1]								= 1
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842754
					Data.iPackedTexture[1] 								= 65537
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-34.7574, -9.9475, 0.9944>>
					Data.vRotation 										= <<0.0000, 0.0000, 95.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_A
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 86
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_PAPARAZZI)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 327686
					Data.iPackedDrawable[1]								= 131075
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619969
					Data.iPackedTexture[1] 								= 131844
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<19.5966, -11.1265, 0.9883>>
					Data.vRotation 										= <<0.0000, 0.0000, 302.4000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_CHANGE_CAPSULE_SIZE)
				BREAK
			ENDSWITCH
		BREAK
		CASE 87
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_H)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 50659334
					Data.iPackedDrawable[1]								= 131329
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554434
					Data.iPackedTexture[1] 								= 66306
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<22.3976, -3.3306, 0.9898>>
					Data.vRotation 										= <<0.0000, 0.0000, 176.4000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 88
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_A)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67239936
					Data.iPackedDrawable[1]								= 1026
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 131073
					Data.iPackedTexture[1] 								= 265
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<23.1576, 4.3445, 0.9890>>
					Data.vRotation 										= <<0.0000, 0.0000, 318.9600>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 89
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_FEMALE_A)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_B	
					Data.iPackedDrawable[0]								= 50593793
					Data.iPackedDrawable[1]								= 131843
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 2
					Data.iPackedTexture[1] 								= 65794
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-0.1575, -16.9246, 0.9954>>
					Data.vRotation 										= <<0.0000, 0.0000, 21.6000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 90
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_H)
					Data.iLevel 										= g_sMPTunables.iTUNER_CARMEET_PEDS_CULLING_LEVEL_BLOCK_B	
					Data.iPackedDrawable[0]								= 67108870
					Data.iPackedDrawable[1]								= 66562
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33619970
					Data.iPackedTexture[1] 								= 131586
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-1.2074, -16.9846, 1.0004>>
					Data.vRotation 										= <<0.0000, 0.0000, 336.2400>>
					CLEAR_PEDS_BITSET(Data.iBS)
					IF g_sMPTunables.bTUNER_CARMEET_PEDS_REMOVE_BLOCK_B
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 91
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_FEMALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_STANDING_SMOKING_F_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 33816578
					Data.iPackedDrawable[1]								= 66049
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 65536
					Data.iPackedTexture[1] 								= 66307
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<-22.2184, 39.4065, 2.0893>>
					Data.vRotation 										= <<0.0000, 0.0000, 216.0000>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEELED_PED)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
				BREAK
			ENDSWITCH
		BREAK
		CASE 92
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_D)
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 67502083
					Data.iPackedDrawable[1]								= 2
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 131585
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<13.7711, 38.5288, 2.1014>>
					Data.vRotation 										= <<-15.1200, 0.0000, 215.6150>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 93
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_CAR_MEET_MALE)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_ON_PHONE_M_01)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 33619968
					Data.iPackedDrawable[1]								= 66051
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554434
					Data.iPackedTexture[1] 								= 131082
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<8.4835, 37.6785, 2.1047>>
					Data.vRotation 										= <<0.0000, 0.0000, 275.7600>>
					CLEAR_PEDS_BITSET(Data.iBS)
				BREAK
			ENDSWITCH
		BREAK
		CASE 94
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					GET_SPECIAL_PED_INFO(iLayout, Data.PedID, 0, Data.iPedModel, Data.vPosition, Data.vRotation.z, Data.iActivity)
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					#IF FEATURE_GEN9_EXCLUSIVE
					IF NOT DOES_FIRST_SPECIAL_VEHICLE_INCLUDE_A_PED()
						PRINTLN("[FEATURE_GEN9_EXCLUSIVE][SKIPPING SPAWNING OF HAO]")
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
					#ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 95
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iLevel 										= 0
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					GET_SPECIAL_PED_INFO(iLayout, Data.PedID, 1, Data.iPedModel, Data.vPosition, Data.vRotation.z, Data.iActivity)
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					IF NOT DOES_SECOND_SPECIAL_VEHICLE_INCLUDE_A_PED()
						PRINTLN("[FEATURE_GEN9_EXCLUSIVE][SKIPPING SPAWNING OF HAO]")
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		// Network Peds
		CASE NETWORK_PED_ID_0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_PATROL)
					Data.iLevel 										= 0
					Data.vPosition 										= <<15.4336, -0.8333, 0.9998>>
					Data.vRotation 										= <<0.0000, 0.0000, 266.3013>>
					GET_CAR_MEET_PED_PATROL_DATA(Data, iPed, iLayout)
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_NO_ANIMATION)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_PATROL)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
		CASE NETWORK_PED_ID_1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_CAR_MEET_PATROL)
					Data.iLevel 										= 0
					Data.vPosition 										= <<-13.6299, 16.6377, 1.1037>>
					Data.vRotation 										= <<0.0000, 0.0000, 177.5264>>
					GET_CAR_MEET_PED_PATROL_DATA(Data, iPed, iLayout)
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_NO_ANIMATION)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_PATROL)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF NOT IS_SPECIAL_PED_INDEX(iPed)
			// Transform the local coords into world coords.
		IF NOT IS_VECTOR_ZERO(Data.vPosition)
			Data.vPosition = TRANSFORM_LOCAL_COORDS_TO_WORLD_COORDS(_GET_CAR_MEET_PED_LOCAL_COORDS_BASE_POSITION(), _GET_CAR_MEET_PED_LOCAL_HEADING_BASE_HEADING(), Data.vPosition)
		ENDIF
		
		// Transform the local heading into world heading.
		IF (Data.vRotation.z != -1.0)
			Data.vRotation.z = TRANSFORM_LOCAL_HEADING_TO_WORLD_HEADING(_GET_CAR_MEET_PED_LOCAL_HEADING_BASE_HEADING(), Data.vRotation.z)
		ENDIF
	ENDIF
ENDPROC
PROC _SET_CAR_MEET_PED_CHANGED_CAPSULE_SIZE_DATA_LAYOUT_0(CHANGED_CAPSULE_SIZE_DATA &ChangedCapsuleSizeData, INT iArrayID, INT &iChangedCapsuleSizePedID[], BOOL bNetworkPed = FALSE)
	
	IF (bNetworkPed)
//		SWITCH iArrayID
//		ENDSWITCH
	ELSE
		SWITCH iArrayID
			CASE 0
				ChangedCapsuleSizeData.iPedID							= 30
				ChangedCapsuleSizeData.fCapsuleRadius					= 0.7
				iChangedCapsuleSizePedID[ChangedCapsuleSizeData.iPedID]	= iArrayID
			BREAK
			CASE 1
				ChangedCapsuleSizeData.iPedID							= 86
				ChangedCapsuleSizeData.fCapsuleRadius					= 0.7
				iChangedCapsuleSizePedID[ChangedCapsuleSizeData.iPedID]	= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC
PROC _SET_CAR_MEET_PED_CHANGED_CAPSULE_SIZE_DATA_LAYOUT_1(CHANGED_CAPSULE_SIZE_DATA &ChangedCapsuleSizeData, INT iArrayID, INT &iChangedCapsuleSizePedID[], BOOL bNetworkPed = FALSE)
	
	IF (bNetworkPed)
//		SWITCH iArrayID
//		ENDSWITCH
	ELSE
		SWITCH iArrayID
			CASE 0
				ChangedCapsuleSizeData.iPedID							= 30
				ChangedCapsuleSizeData.fCapsuleRadius					= 0.7
				iChangedCapsuleSizePedID[ChangedCapsuleSizeData.iPedID]	= iArrayID
			BREAK
			CASE 1
				ChangedCapsuleSizeData.iPedID							= 60
				ChangedCapsuleSizeData.fCapsuleRadius					= 0.7
				iChangedCapsuleSizePedID[ChangedCapsuleSizeData.iPedID]	= iArrayID
			BREAK
			CASE 2
				ChangedCapsuleSizeData.iPedID							= 86
				ChangedCapsuleSizeData.fCapsuleRadius					= 0.7
				iChangedCapsuleSizePedID[ChangedCapsuleSizeData.iPedID]	= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC _SET_CAR_MEET_PED_CHANGED_CAPSULE_SIZE_DATA(CHANGED_CAPSULE_SIZE_DATA &ChangedCapsuleSize, INT iLayout, INT iArrayID, INT &iChangedCapsuleSizePedID[], BOOL bNetworkPed = FALSE)
	IF iLayout <100
	_SET_CAR_MEET_PED_CHANGED_CAPSULE_SIZE_DATA_LAYOUT_0(ChangedCapsuleSize, iArrayID, iChangedCapsuleSizePedID, bNetworkPed)
	ELSE
	_SET_CAR_MEET_PED_CHANGED_CAPSULE_SIZE_DATA_LAYOUT_1(ChangedCapsuleSize, iArrayID, iChangedCapsuleSizePedID, bNetworkPed)
	ENDIF	
ENDPROC
PROC _SET_CAR_MEET_PED_PROP_INDEXES(PED_INDEX &PedID, INT iPed, INT iLayout, PED_MODELS ePedModel)
	UNUSED_PARAMETER(ePedModel)
	UNUSED_PARAMETER(iPed)
	
	INT iMainLayout = FLOOR(iLayout/100.0)

	SWITCH ePedModel
		CASE PED_MODEL_CAR_MEET_MOODYMANN
			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
		BREAK
		CASE PED_MODEL_BENNY
			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
		BREAK
//		CASE PED_MODEL_HAO
//			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,0), 0, 0)
//		BREAK
		CASE PED_MODEL_BENNYS_MECHANIC
			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,0), 0, 0)
			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
		BREAK
		CASE PED_MODEL_JOHNNY_JONES
			SET_PED_PROP_INDEX(PedID, INT_TO_ENUM(PED_PROP_POSITION,0), 0, 0)
		BREAK
		CASE PED_MODEL_CAR_MEET_JACKER_MALE
			IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup != -1			
			SWITCH iMainLayout 
				CASE 0
					SWITCH iPed
						//GROUP 1
						CASE 16
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 0
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[0])
							ENDIF
						BREAK
						CASE 17
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 0
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[1])
							ENDIF
						BREAK
						CASE 18
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 0
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[2])
							ENDIF
						BREAK
						//GROUP 2
						CASE 19
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 1
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[0])
							ENDIF
						BREAK
						CASE 61
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 1
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[1])
							ENDIF
						BREAK
						CASE 62
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 1
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[2])
							ENDIF
						BREAK
						//GROUP 3
						CASE 52
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 2
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[0])
							ENDIF
						BREAK
						CASE 53
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 2
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[1])
							ENDIF
						BREAK
						CASE 54
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 2
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[2])
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
				CASE 1
					SWITCH iPed
						//GROUP 0
						CASE 44
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 0
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[0])
							ENDIF
						BREAK
						CASE 48
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 0
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[1])
							ENDIF
						BREAK
						CASE 84
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 0
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[2])
							ENDIF
						BREAK
						//GROUP 2
						CASE 64
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 1
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[0])
							ENDIF
						BREAK
						CASE 65
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 1
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[1])
							ENDIF
						BREAK
						CASE 66
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 1
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[2])
							ENDIF
						BREAK
						//GROUP 3
						CASE 11
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 2
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[0])
							ENDIF
						BREAK
						CASE 13
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 2
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[1])
							ENDIF
						BREAK
						CASE 14
							IF g_sCarMeetSpecialPedPositionData.iCarJackerGroup = 2
								SET_JACKER_PROP_DATA(PedID, g_sCarMeetSpecialPedPositionData.iJackerPedsIndex[2])
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
			ENDIF
		BREAK
	ENDSWITCH 
ENDPROC
//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ FUNCTIONS TO IMPLEMENT ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _SHOULD_CAR_MEET_PED_SCRIPT_LAUNCH()
	RETURN IS_PLAYER_IN_CAR_MEET_PROPERTY(PLAYER_ID()) AND NOT IS_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX(PLAYER_ID())
ENDFUNC

FUNC BOOL _IS_CAR_MEET_PARENT_A_SIMPLE_INTERIOR
	RETURN TRUE
ENDFUNC

PROC _SET_CAR_MEET_PED_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, INT iPed, INT iLayout, BOOL bSetPedArea = TRUE)
	UNUSED_PARAMETER(ServerBD)
	UNUSED_PARAMETER(ePedLocation)
	INT iSelectedLayout =  FLOOR(iLayout/100.0)
	SWITCH iSelectedLayout
		CASE 0
			PRINTLN("_SET_CAR_MEET_PED_DATA   USING LAYOUT 0, PED: ", iPed)	
			_SET_CAR_MEET_PED_DATA_LAYOUT_0(Data, iPed,iLayout, bSetPedArea)
		BREAK
		CASE 1
			PRINTLN("_SET_CAR_MEET_PED_DATA   USING LAYOUT 1, PED: ", iPed)		
			_SET_CAR_MEET_PED_DATA_LAYOUT_1(Data, iPed,iLayout, bSetPedArea)
		BREAK
	ENDSWITCH
	
	//OVERRIDES PEDS WITH CAR JACKERS IF APPLICABLE
	SET_JACKER_PED_DATA(Data, iPed, iSelectedLayout)
	
ENDPROC

FUNC INT _GET_CAR_MEET_NETWORK_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	UNUSED_PARAMETER(ServerBD)
	UNUSED_PARAMETER(ePedLocation)
	RETURN 2
ENDFUNC

FUNC INT _GET_CAR_MEET_LOCAL_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	
	INT iPed
	INT iActivePeds = 0
	PEDS_DATA tempData
	
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iPed
		tempData.vPosition = NULL_VECTOR()
		_SET_CAR_MEET_PED_DATA(ePedLocation, ServerBD, tempData, iPed, ServerBD.iLayout)
		IF NOT IS_VECTOR_ZERO(tempData.vPosition)
			iActivePeds++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iActivePeds
ENDFUNC

FUNC INT _GET_CAR_MEET_SERVER_PED_LAYOUT_TOTAL()
	RETURN 130
ENDFUNC

FUNC INT _GET_CAR_MEET_SERVER_PED_LAYOUT()	
	INT iSpecialPedLayout = GET_RANDOM_INT_IN_RANGE(0,30)
	INT iGeneralLayout = GET_RANDOM_INT_IN_RANGE(0,2)
	RETURN 100*iGeneralLayout + iSpecialPedLayout
ENDFUNC

FUNC INT _GET_CAR_MEET_SERVER_PED_LEVEL()
	RETURN 2  // Ped levels 0, 1 & 2 spawn by default
ENDFUNC

PROC _SET_CAR_MEET_PED_SERVER_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
	UNUSED_PARAMETER(ePedLocation)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ServerBD.iLayout 			= _GET_CAR_MEET_SERVER_PED_LAYOUT()
		ServerBD.iLevel 			= _GET_CAR_MEET_SERVER_PED_LEVEL()
		ServerBD.iMaxLocalPeds 		= _GET_CAR_MEET_LOCAL_PED_TOTAL(ServerBD)
		ServerBD.iMaxNetworkPeds	= _GET_CAR_MEET_NETWORK_PED_TOTAL(ServerBD)
	ENDIF
	PRINTLN("[AM_MP_PEDS] _SET_CAR_MEET_PED_SERVER_DATA - Layout: ", ServerBD.iLayout)
	PRINTLN("[AM_MP_PEDS] _SET_CAR_MEET_PED_SERVER_DATA - Level: ", ServerBD.iLevel)
	PRINTLN("[AM_MP_PEDS] _SET_CAR_MEET_PED_SERVER_DATA - Max Local Peds: ", ServerBD.iMaxLocalPeds)
	PRINTLN("[AM_MP_PEDS] _SET_CAR_MEET_PED_SERVER_DATA - Max Network Peds: ", ServerBD.iMaxNetworkPeds)
ENDPROC

FUNC BOOL _IS_PLAYER_IN_CAR_MEET_PARENT_PROPERTY(PLAYER_INDEX playerID)
	RETURN IS_PLAYER_IN_CAR_MEET_PROPERTY(playerID)
ENDFUNC

FUNC BOOL _HAS_CAR_MEET_PED_BEEN_CREATED(PEDS_DATA &Data, INT iLevel)
	
	IF (IS_ENTITY_ALIVE(Data.PedID)
		AND (GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK 
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_START_SCENARIO_IN_PLACE) = PERFORMING_TASK))
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CHILD_PED)
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SKIP_PED)
	OR (Data.iLevel > iLevel)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED MODELS ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_CAR_MEET_LOCAL_PED_PROPERTIES(PED_INDEX &PedID, INT iPed)
	UNUSED_PARAMETER(iPed)
	SET_ENTITY_CAN_BE_DAMAGED(PedID, FALSE)
	SET_PED_AS_ENEMY(PedID, FALSE)
	SET_CURRENT_PED_WEAPON(PedID, WEAPONTYPE_UNARMED, TRUE)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedID, TRUE)
	SET_PED_RESET_FLAG(PedID, PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromExplosions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
	
	SET_PED_CAN_EVASIVE_DIVE(PedID, FALSE)
	SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(PedID, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PedID, FALSE)
	SET_PED_CAN_RAGDOLL(PedID, FALSE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DisableExplosionReactions, TRUE)
	CLEAR_PED_TASKS(PedID)
ENDPROC

PROC _SET_CAR_MEET_NETWORK_PED_PROPERTIES(NETWORK_INDEX &NetworkPedID, INT &iPedDataBS[PEDS_DATA_BITSET_ARRAY_SIZE])
	NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NETWORK_GET_ENTITY_FROM_NETWORK_ID(NetworkPedID), TRUE)
	SET_NETWORK_ID_CAN_MIGRATE(NetworkPedID, FALSE)
	
	IF IS_PEDS_BIT_SET(iPedDataBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, FALSE)
	ELSE
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, TRUE)
	ENDIF
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED SPEECH ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_CAR_MEET_PED_SPEECH_DATA(SPEECH_DATA &SpeechData, INT iLayout, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	UNUSED_PARAMETER(iLayout)
	
	IF (NOT bNetworkPed)
		SWITCH iArrayID
			CASE 0
				SpeechData.iPedID							= 94
				SpeechData.fGreetSpeechDistance				= 3.5
				SpeechData.fByeSpeechDistance				= 5.0
				SpeechData.fListenDistance					= 7.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
			CASE 1
				SpeechData.iPedID							= 95
				SpeechData.fGreetSpeechDistance				= 3.5
				SpeechData.fByeSpeechDistance				= 5.0
				SpeechData.fListenDistance					= 7.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

FUNC BOOL _CAN_CAR_MEET_PED_PLAY_SPEECH(PED_INDEX PedID, INT iPed, INT iLayout, PED_SPEECH ePedSpeech)
	UNUSED_PARAMETER(ePedSpeech)
	UNUSED_PARAMETER(PedID)
	UNUSED_PARAMETER(iLayout)
	
	// Generic conditions
	IF NOT IS_ENTITY_ALIVE(PedID)
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_PED_PLAY_SPEECH - Bail Reason: Ped is not alive")
		RETURN FALSE
	ENDIF
	
	IF NOT g_bInitPedsCreated
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_PED_PLAY_SPEECH - Bail Reason: Waiting for all peds to be created first")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_PED_PLAY_SPEECH - Bail Reason: Player is walking in or out of interior")
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT()
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_PED_PLAY_SPEECH - Bail Reason: Screen is fading out")
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_PED_PLAY_SPEECH - Bail Reason: Browser is open")
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE() OR IS_PLAYER_IN_SIMPLE_CUTSCENE(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_PED_PLAY_SPEECH - Bail Reason: Cutscene is active")
		RETURN FALSE
	ENDIF
	
	IF (IS_PLAYER_IN_CORONA() OR IS_TRANSITION_SESSION_LAUNCHING() OR IS_TRANSITION_SESSION_RESTARTING())
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_PED_PLAY_SPEECH - Bail Reason: Player in corona")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_INTERACTING_WITH_PLANNING_BOARD()
		PRINTLN("[AM_MP_PEDS] _CAN_CAR_MEET_PED_PLAY_SPEECH - Bail Reason: Player is interacting with the planning board")
		RETURN FALSE
	ENDIF
	
	// Specific conditions
	SWITCH iPed
		CASE -1
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
FUNC PED_SPEECH _GET_CAR_MEET_PED_SPEECH_TYPE(INT iPed, PED_ACTIVITIES ePedActivity, INT iSpeech)
	UNUSED_PARAMETER(ePedActivity)
	PED_SPEECH eSpeech = PED_SPH_INVALID
	
	SWITCH iPed
		CASE 94
			IF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS] = PED_HAO)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
					CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
					CASE 3	eSpeech = PED_SPH_PT_LOITER											BREAK
				ENDSWITCH
			ELIF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS] = PED_MOODYMAN)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
					CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
					CASE 3	eSpeech = PED_SPH_PT_LOITER											BREAK
				ENDSWITCH
			ELIF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS] = PED_BENNY)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				ENDSWITCH
			ELIF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS] = PED_BENNYS_MECHANIC)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
					CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
				ENDSWITCH
			ELIF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS] = PED_LS_CUSTOMS_MECHANIC)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
					CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
				ENDSWITCH
			ELIF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS] = PED_WAREHOUSE_MECHANIC)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
					CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 95
			IF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1] = PED_HAO)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
					CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
					CASE 3	eSpeech = PED_SPH_PT_LOITER											BREAK
				ENDSWITCH
			ELIF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1] = PED_MOODYMAN)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
					CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
					CASE 3	eSpeech = PED_SPH_PT_LOITER											BREAK
				ENDSWITCH
			ELIF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1] = PED_BENNY)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				ENDSWITCH
			ELIF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1] = PED_BENNYS_MECHANIC)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
					CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
				ENDSWITCH
			ELIF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1] = PED_LS_CUSTOMS_MECHANIC)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
					CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
					CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
				ENDSWITCH
			ELIF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1] = PED_WAREHOUSE_MECHANIC)
				SWITCH iSpeech
					CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
					CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
				ENDSWITCH
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN eSpeech
ENDFUNC
FUNC BOOL MOODYMANN_USE_SPECIAL_SPEECH(PED_CONVO_DATA &convoData)

	PLAYER_INDEX playerID = PLAYER_ID()
	BOOL bShouldUseSpecialSpeech = FALSE
	
	// TRAIN COMPLETE
	IF HAS_PLAYER_COMPLETED_TUNER_ROBBERY(playerID, TR_FREIGHT_TRAIN)
	AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MOODYMANN_DIALOGUE_TRAIN_COMPLETE)
		convoData.sRootName = "TRAIN_COMPLETE"
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SESSANTA_DIALOGUE_TRAIN_SCHEDULE, TRUE)
		bShouldUseSpecialSpeech = TRUE
	///ROBBERY_IN_PROGRESS
	ELIF GET_PLAYER_ACTIVE_TUNER_ROBBERY(playerID) != TR_NONE
	AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MOODYMANN_DIALOGUE_ROBBERY_IN_PROGRESS)
		convoData.sRootName = "ROBBERY_IN_PROGRESS"
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MOODYMANN_DIALOGUE_ROBBERY_IN_PROGRESS, TRUE)
		bShouldUseSpecialSpeech = TRUE
	///ROBBERY_COMPLETE
	ELIF GET_PLAYER_ACTIVE_TUNER_ROBBERY(playerID) = TR_NONE
	AND HAS_PLAYER_COMPLETED_ANY_TUNER_ROBBERY(playerID)
	AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MOODYMANN_DIALOGUE_ROBBERY_COMPLETE)
		convoData.sRootName = "ROBBERY_COMPLETE"
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_MOODYMANN_DIALOGUE_ROBBERY_COMPLETE, TRUE)
	ENDIF
		

	RETURN bShouldUseSpecialSpeech = TRUE
	
ENDFUNC
PROC _GET_CAR_MEET_PED_CONVO_DATA(PED_CONVO_DATA &convoData, INT iPed, PED_ACTIVITIES ePedActivity, PED_SPEECH ePedSpeech)
	UNUSED_PARAMETER(ePedActivity)
	RESET_PED_CONVO_DATA(convoData)
	
	INT iRandSpeech
	SWITCH iPed
		CASE 94
			IF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS] = PED_HAO)
				convoData.sCharacterVoice = "HAO"
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
						convoData.sRootName = "GENERIC_HI"
						IF (iRandSpeech = 1)
							convoData.sRootName = "GENERIC_HOWSITGOING"
						ENDIF
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName = "GENERIC_BYE"
					BREAK
					CASE PED_SPH_PT_BUMP
						convoData.sRootName = "BUMP"
					BREAK
					CASE PED_SPH_PT_LOITER
						iRandSpeech = GET_RANDOM_INT_IN_RANGE(1, 101)
						IF (iRandSpeech <= 40)
							convoData.sRootName = "IDLE"
						ELIF (iRandSpeech >= 41 AND iRandSpeech <= 80)
							convoData.sRootName = "IDLE"
							#IF FEATURE_GEN9_STANDALONE
							convoData.sRootName = "IDLE_HSW"
							#ENDIF
						ELSE
							convoData.sRootName = "GENERAL_CHAT"
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			IF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS] = PED_MOODYMAN)
				convoData.sCharacterVoice = "TUN_KDJ"
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						IF NOT MOODYMANN_USE_SPECIAL_SPEECH(convoData)
							IF DOES_PLAYER_OWN_A_AUTO_SHOP(PLAYER_ID())
								iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 3)
							ELSE
								iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
							ENDIF
							convoData.sRootName = "GENERIC_HI"
							IF (iRandSpeech = 1)
								convoData.sRootName = "GENERIC_HOWSITGOING"
							ENDIF
							IF (iRandSpeech = 2)							
								convoData.sRootName = "GENERAL_AUTO_SHOP"							
							ENDIF
						ENDIF
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName = "GENERIC_BYE"
					BREAK
					CASE PED_SPH_PT_BUMP
						convoData.sRootName = "BUMP"
					BREAK
					CASE PED_SPH_PT_LOITER
						iRandSpeech = GET_RANDOM_INT_IN_RANGE(1, 101)
						IF (iRandSpeech <= 50)
							convoData.sRootName = "IDLE"
						ELSE
							convoData.sRootName = "GENERAL_CHAT"
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			IF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS] = PED_BENNY)
				convoData.sCharacterVoice = "BENNY"
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						IF IS_PV_IN_CAR_MEET_GAR()
							convoData.sRootName = "SHOP_NICE_VEHICLE"
						ENDIF
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName = "SHOP_GOODBYE"
					BREAK
				ENDSWITCH
			ENDIF
			IF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS] = PED_BENNYS_MECHANIC)
				convoData.sCharacterVoice = "A_F_Y_EastSA_03_Latino_MINI_01"
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 3)
						IF iRandSpeech = 0
							convoData.sRootName = "GENERIC_HI"
						ELIF iRandSpeech = 1
							convoData.sRootName = "GENERIC_HOWS_IT_GOING"
						ELIF iRandSpeech = 2
							convoData.sRootName = "GREET_ACROSS_STREET"
						ENDIF

					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName = "GOODBYE_ACROSS_STREET"
					BREAK
					CASE PED_SPH_PT_BUMP
						iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
						IF iRandSpeech = 0
							convoData.sRootName = "BUMP"
						ELIF iRandSpeech = 1
							convoData.sRootName = "SEE_WEIRDO"
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			IF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS] = PED_LS_CUSTOMS_MECHANIC)
				convoData.sCharacterVoice = "S_M_M_AUTOSHOP_01_WHITE_01"
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						convoData.sRootName = "GENERIC_HI"					
					BREAK
					CASE PED_SPH_PT_BYE					
						convoData.sRootName = "GENERIC_BYE"						
					BREAK
					CASE PED_SPH_PT_BUMP
						convoData.sRootName = "BUMP"
					BREAK
				ENDSWITCH
			ENDIF
			IF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS] = PED_WAREHOUSE_MECHANIC)
				convoData.sCharacterVoice = "SECUROMECH"
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						convoData.sRootName = "SECMECH_ASSOGREET"				
					BREAK					
					CASE PED_SPH_PT_BUMP
						convoData.sRootName = "BUMP"						
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 95
			IF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1] = PED_HAO)
				convoData.sCharacterVoice = "HAO"
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
						convoData.sRootName = "GENERIC_HI"
						IF (iRandSpeech = 1)
							convoData.sRootName = "GENERIC_HOWSITGOING"
						ENDIF
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName = "GENERIC_BYE"
					BREAK
					CASE PED_SPH_PT_BUMP
						convoData.sRootName = "BUMP"
					BREAK
					CASE PED_SPH_PT_LOITER
						iRandSpeech = GET_RANDOM_INT_IN_RANGE(1, 101)
						IF (iRandSpeech <= 40)
							convoData.sRootName = "IDLE"
						ELIF (iRandSpeech >= 41 AND iRandSpeech <= 80)
							convoData.sRootName = "IDLE"
							#IF FEATURE_GEN9_STANDALONE
							convoData.sRootName = "IDLE_HSW"
							#ENDIF
						ELSE
							convoData.sRootName = "GENERAL_CHAT"
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			IF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1] = PED_MOODYMAN)
				convoData.sCharacterVoice = "TUN_KDJ"
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						IF NOT MOODYMANN_USE_SPECIAL_SPEECH(convoData)
							iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 3)
							convoData.sRootName = "GENERIC_HI"
							IF (iRandSpeech = 1)
								convoData.sRootName = "GENERIC_HOWSITGOING"
							ENDIF
							IF (iRandSpeech = 2)
								IF DOES_PLAYER_OWN_A_AUTO_SHOP(PLAYER_ID())
									convoData.sRootName = "GENERAL_AUTO_SHOP"
								ELSE
									convoData.sRootName = "GENERIC_HI"
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName = "GENERIC_BYE"
					BREAK
					CASE PED_SPH_PT_BUMP
						convoData.sRootName = "BUMP"
					BREAK
					CASE PED_SPH_PT_LOITER
						iRandSpeech = GET_RANDOM_INT_IN_RANGE(1, 101)
						IF (iRandSpeech <= 50)
							convoData.sRootName = "IDLE"
						ELSE
							convoData.sRootName = "GENERAL_CHAT"
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			IF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1] = PED_BENNY)
				convoData.sCharacterVoice = "BENNY"
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						IF IS_PV_IN_CAR_MEET_GAR()
							convoData.sRootName = "SHOP_NICE_VEHICLE"
						ENDIF
					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName = "SHOP_GOODBYE"
					BREAK
				ENDSWITCH
			ENDIF
			IF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1] = PED_BENNYS_MECHANIC)
				convoData.sCharacterVoice = "A_F_Y_EastSA_03_Latino_MINI_01"
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 3)
						IF iRandSpeech = 0
							convoData.sRootName = "GENERIC_HI"
						ELIF iRandSpeech = 1
							convoData.sRootName = "GENERIC_HOWS_IT_GOING"
						ELIF iRandSpeech = 2
							convoData.sRootName = "GREET_ACROSS_STREET"
						ENDIF

					BREAK
					CASE PED_SPH_PT_BYE
						convoData.sRootName = "GOODBYE_ACROSS_STREET"
					BREAK
					CASE PED_SPH_PT_BUMP
						iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, 2)
						IF iRandSpeech = 0
							convoData.sRootName = "BUMP"
						ELIF iRandSpeech = 1
							convoData.sRootName = "SEE_WEIRDO"
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			IF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1] = PED_LS_CUSTOMS_MECHANIC)
				convoData.sCharacterVoice = "S_M_M_AUTOSHOP_01_WHITE_01"
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						convoData.sRootName = "GENERIC_HI"					
					BREAK
					CASE PED_SPH_PT_BYE					
						convoData.sRootName = "GENERIC_BYE"						
					BREAK
					CASE PED_SPH_PT_BUMP
						convoData.sRootName = "BUMP"
					BREAK
				ENDSWITCH
			ENDIF
			IF (g_sCarMeetSpecialPedPositionData.ePedsToSpawn[ciCAR_MEET_NUMBER_OF_SPECIAL_PEDS-ciCAR_MEET_NUMBER_CONSTANT_SPEC_PEDS+1] = PED_WAREHOUSE_MECHANIC)
				convoData.sCharacterVoice = "SECUROMECH"
				SWITCH ePedSpeech
					CASE PED_SPH_PT_GREETING
						convoData.sRootName = "SECMECH_ASSOGREET"				
					BREAK					
					CASE PED_SPH_PT_BUMP
						convoData.sRootName = "BUMP"						
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

FUNC PED_SPEECH _GET_CAR_MEET_PED_CONTROLLER_SPEECH(PED_SPEECH &eCurrentSpeech, INT iPed, PED_ACTIVITIES ePedActivity)
	
	INT iSpeech
	INT iAttempts = 0
	INT iMaxSpeech
	INT iRandSpeech
	PED_SPEECH eSpeech
	
	INT iMaxControllerSpeechTypes = 0
	INT iControllerSpeechTypes[PED_SPH_TOTAL]
	
	SWITCH iPed
		CASE 0			
			// Description: Default simply selects a new speech to play that isn't the same as the previous speech.
			PED_CONVO_DATA convoData
			
			// Populate the iControllerSpeechTypes array with all the controller speech type IDs from _GET_GENREIC_PED_SPEECH_TYPE
			REPEAT PED_SPH_TOTAL iSpeech
				eSpeech = _GET_CAR_MEET_PED_SPEECH_TYPE(iPed, ePedActivity, iSpeech)
				IF (eSpeech > PED_SPH_PT_TOTAL AND eSpeech < PED_SPH_CT_TOTAL)
					RESET_PED_CONVO_DATA(convoData)
					_GET_CAR_MEET_PED_CONVO_DATA(convoData, iPed, ePedActivity, eSpeech)
					IF IS_CONVO_DATA_VALID(convoData)
						iControllerSpeechTypes[iMaxControllerSpeechTypes] = iSpeech
						iMaxControllerSpeechTypes++
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF (iMaxControllerSpeechTypes > 1)
				iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
				eSpeech = _GET_CAR_MEET_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
				
				// Ensure speech type is different from previous
				WHILE (eSpeech = eCurrentSpeech AND iAttempts < 10)
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
					eSpeech = _GET_CAR_MEET_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
					iAttempts++
				ENDWHILE
				
				// Randomising failed to find new speech type. Manually set it.
				IF (iAttempts >= 10)
					REPEAT iMaxSpeech iSpeech
						eSpeech = _GET_CAR_MEET_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iSpeech])
						IF (eSpeech != eCurrentSpeech)
							BREAKLOOP
						ENDIF
					ENDREPEAT
				ENDIF
				
			ELSE
				eSpeech = _GET_CAR_MEET_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[0])
			ENDIF
			
		BREAK
	ENDSWITCH
	
	eCurrentSpeech = eSpeech
	RETURN eSpeech
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PED ANIM DATA ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

// Note: Activities with multiple animations have their anim data populated in their own functions below.
//		 If an activity only has one animation associated with it, it is populated within _GET_CAR_MEET_PED_ANIM_DATA itself.
//
// Note: Each activity either has a _M_ or _F_ in its title to specify which ped gender should be using that activity.
//		 If an activity does not has either an _M_ or _F_ in its title; it is unisex, and can be used by either ped gender.
//
// Note: Some animations have been excluded from activities. 
//		 The excluded anims have Z axis starting positions that dont line up with the other anims in the same dictionary.
//		 This causes a snap that a blend cannot fix. Use the widget 'RAG/Script/AM_MP_PEDS/Animation/Output Initial Activity Anim Data' to see which Z axis anims are broken.

//╒═══════════════════════════════╕
//╞══════╡ LEANING SMOKING ╞══════╡
//╘═══════════════════════════════╛

PROC GET_CAR_MEET_LEANING_SMOKING_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_C"
		BREAK
		CASE 4
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_D"
		BREAK
		CASE 5
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@SMOKING@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_E"
		BREAK
	ENDSWITCH
ENDPROC

//╒═══════════════════════════════╕
//╞═════╡ STANDING SMOKING ╞══════╡
//╘═══════════════════════════════╛

PROC GET_CAR_MEET_STANDING_SMOKING_M_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@MALE@MALE_A@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_C"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CAR_MEET_CHECKOUT_CAR_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@"
	
	SWITCH iClip
		CASE 0	
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+= letter 		
			pedAnimData.sAnimClip 		+= "_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+= letter 	
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+= letter 	
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+= letter 	
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+= letter 	
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_CHECKOUT_CAR_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@"
	
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	pedAnimData.bVFXAnimClip			= TRUE
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_engine@"
	
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 		
			pedAnimData.sAnimClip 		+="_base"
			pedAnimData.bVFXAnimClip	= FALSE
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 		
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 		
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 		
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 		
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
	
	pedAnimData.sFacialAnimClip = pedAnimData.sAnimClip
	pedAnimData.sFacialAnimClip += "_facial"
ENDPROC
PROC GET_CAR_MEET_CHECKOUT_ENGINE_SMOKE_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	pedAnimData.bVFXAnimClip			= TRUE
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_engine@"
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
			pedAnimData.bVFXAnimClip	= FALSE
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
	
	pedAnimData.sFacialAnimClip = pedAnimData.sAnimClip
	pedAnimData.sFacialAnimClip += "_facial"
ENDPROC
PROC GET_CAR_MEET_CHECKOUT_ENGINE_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_engine@"
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_TAKE_LISTEN_MUSIC_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@listen_music@"
	SWITCH iClip
		CASE 0	
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_TAKE_LISTEN_MUSIC_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@listen_music@"
	SWITCH iClip
		CASE 0		
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_TAKE_PHOTOS_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	pedAnimData.bVFXAnimClip			= TRUE
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@take_photos@"
	SWITCH iClip
		CASE 0					
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
			pedAnimData.bVFXAnimClip	= FALSE
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_TAKE_PHOTOS_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	pedAnimData.bVFXAnimClip			= TRUE
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@take_photos@"
	SWITCH iClip
		CASE 0		
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
			pedAnimData.bVFXAnimClip	= FALSE
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC
PROC GET_CAR_MEET_CHECKOUT_ENGINE_DRINK_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_engine@"
	SWITCH iClip
		CASE 0		
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
	
	pedAnimData.sFacialAnimClip = pedAnimData.sAnimClip
	pedAnimData.sFacialAnimClip += "_facial"
ENDPROC
PROC GET_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_engine@"
	SWITCH iClip
		CASE 0	
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
	
	pedAnimData.sFacialAnimClip = pedAnimData.sAnimClip
	pedAnimData.sFacialAnimClip += "_facial"
ENDPROC
PROC GET_CAR_MEET_CHECKOUT_CAR_DRINK_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@"
	SWITCH iClip
		CASE 0		
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "drink_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
	
	pedAnimData.sFacialAnimClip = pedAnimData.sAnimClip
	pedAnimData.sFacialAnimClip += "_facial"
ENDPROC
PROC GET_CAR_MEET_CHECKOUT_CAR_DRINK_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@"
	SWITCH iClip
		CASE 0					
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3			
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "drink_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
	
	pedAnimData.sFacialAnimClip = pedAnimData.sAnimClip
	pedAnimData.sFacialAnimClip += "_facial"
ENDPROC
PROC GET_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	pedAnimData.bVFXAnimClip			= TRUE
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@"
	SWITCH iClip
		CASE 0	
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
			pedAnimData.bVFXAnimClip	= FALSE
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "smoke_male_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
	
	pedAnimData.sFacialAnimClip = pedAnimData.sAnimClip
	pedAnimData.sFacialAnimClip += "_facial"
ENDPROC
PROC GET_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	pedAnimData.bVFXAnimClip			= TRUE
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_car@"
	SWITCH iClip
		CASE 0	
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
			pedAnimData.bVFXAnimClip	= FALSE
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "smoke_female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
	
	pedAnimData.sFacialAnimClip = pedAnimData.sAnimClip
	pedAnimData.sFacialAnimClip += "_facial"
ENDPROC
PROC GET_CAR_MEET_CHECKOUT_ENGINE_FEMALE_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, TEXT_LABEL_3 letter)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	pedAnimData.sAnimDict 		= "anim@amb@carmeet@checkout_engine@"
	
	SWITCH iClip
		CASE 0		
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_base"
		BREAK
		CASE 1
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_a"
		BREAK
		CASE 2
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_b"
		BREAK
		CASE 3
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_c"
		BREAK
		CASE 4
			pedAnimData.sAnimClip 		= "female_"
			pedAnimData.sAnimClip 		+=letter 
			pedAnimData.sAnimClip 		+="_idle_d"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CAR_MEET_STANDING_SMOKING_F_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	pedAnimData.bVFXAnimClip			= TRUE
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@FEMALE@BASE"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE 1
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@FEMALE@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE 2
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@FEMALE@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE 3
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_SMOKING@FEMALE@IDLE_A"
			pedAnimData.sAnimClip 		= "IDLE_C"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, PED_TRANSITION_STATES eTransitionState, BOOL bPedTransition, TEXT_LABEL_23 tlPrefix, TEXT_LABEL_3 tlActivityOneLetter, TEXT_LABEL_3 tlActivityTwoLetter, BOOL bMalePed = TRUE, ANIMATION_FLAGS eAnimFlags = AF_DEFAULT)
	pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
	pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
	
	IF (eAnimFlags != AF_DEFAULT)
		pedAnimData.animFlags = eAnimFlags
	ENDIF
	
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_TRANSITION_TASK_ANIM)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
	
	SWITCH eTransitionState
		CASE PED_TRANS_STATE_ACTIVITY_ONE
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@checkout_car@", tlPrefix, tlActivityOneLetter, tlActivityTwoLetter)
				pedAnimData.bVFXAnimClip = TRUE
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ELSE
					GET_CAR_MEET_CHECKOUT_CAR_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ENDIF
			ENDIF
		BREAK
		CASE PED_TRANS_STATE_ACTIVITY_TWO
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@checkout_car@", tlPrefix, tlActivityTwoLetter, tlActivityOneLetter)
				pedAnimData.bVFXAnimClip = TRUE
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ELSE
					GET_CAR_MEET_CHECKOUT_CAR_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC
PROC GET_CAR_MEET_CHECKOUT_ENGINE_DRINK_TRANSITION_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, PED_TRANSITION_STATES eTransitionState, BOOL bPedTransition, TEXT_LABEL_23 tlPrefix, TEXT_LABEL_3 tlActivityOneLetter, TEXT_LABEL_3 tlActivityTwoLetter, BOOL bMalePed = TRUE)
	pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
	pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
	
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_TRANSITION_TASK_ANIM)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
	
	SWITCH eTransitionState
		CASE PED_TRANS_STATE_ACTIVITY_ONE
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@checkout_engine@", tlPrefix, tlActivityOneLetter, tlActivityTwoLetter)
				pedAnimData.sFacialAnimClip = pedAnimData.sAnimClip
				pedAnimData.sFacialAnimClip += "_facial"
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ELSE
					GET_CAR_MEET_CHECKOUT_ENGINE_DRINK_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ENDIF
			ENDIF
		BREAK
		CASE PED_TRANS_STATE_ACTIVITY_TWO
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@checkout_engine@", tlPrefix, tlActivityTwoLetter, tlActivityOneLetter)
				pedAnimData.sFacialAnimClip = pedAnimData.sAnimClip
				pedAnimData.sFacialAnimClip += "_facial"
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ELSE
					GET_CAR_MEET_CHECKOUT_ENGINE_DRINK_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC
PROC GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, PED_TRANSITION_STATES eTransitionState, BOOL bPedTransition, TEXT_LABEL_23 tlPrefix, TEXT_LABEL_3 tlActivityOneLetter, TEXT_LABEL_3 tlActivityTwoLetter, BOOL bMalePed = TRUE)
	pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
	pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
	
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_TRANSITION_TASK_ANIM)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
	
	SWITCH eTransitionState
		CASE PED_TRANS_STATE_ACTIVITY_ONE
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@checkout_engine@", tlPrefix, tlActivityOneLetter, tlActivityTwoLetter)
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ELSE
					GET_CAR_MEET_CHECKOUT_ENGINE_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ENDIF
			ENDIF
		BREAK
		CASE PED_TRANS_STATE_ACTIVITY_TWO
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@checkout_engine@", tlPrefix, tlActivityTwoLetter, tlActivityOneLetter)
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ELSE
					GET_CAR_MEET_CHECKOUT_ENGINE_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC GET_CAR_MEET_LISTEN_MUSIC_TRANSITION_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, PED_TRANSITION_STATES eTransitionState, BOOL bPedTransition, TEXT_LABEL_23 tlPrefix, TEXT_LABEL_3 tlActivityOneLetter, TEXT_LABEL_3 tlActivityTwoLetter, BOOL bMalePed = TRUE)
	pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
	pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
	
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_TRANSITION_TASK_ANIM)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
	
	SWITCH eTransitionState
		CASE PED_TRANS_STATE_ACTIVITY_ONE
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@listen_music@", tlPrefix, tlActivityOneLetter, tlActivityTwoLetter)
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_TAKE_LISTEN_MUSIC_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ELSE
					GET_CAR_MEET_TAKE_LISTEN_MUSIC_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ENDIF
			ENDIF
		BREAK
		CASE PED_TRANS_STATE_ACTIVITY_TWO
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@listen_music@", tlPrefix, tlActivityTwoLetter, tlActivityOneLetter)
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_TAKE_LISTEN_MUSIC_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ELSE
					GET_CAR_MEET_TAKE_LISTEN_MUSIC_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC GET_CAR_MEET_CHECKOUT_CAR_SMOKE_TRANSITION_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, PED_TRANSITION_STATES eTransitionState, BOOL bPedTransition, TEXT_LABEL_23 tlPrefix, TEXT_LABEL_3 tlActivityOneLetter, TEXT_LABEL_3 tlActivityTwoLetter, BOOL bMalePed = TRUE)
	pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
	pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
	
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_TRANSITION_TASK_ANIM)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
	
	SWITCH eTransitionState
		CASE PED_TRANS_STATE_ACTIVITY_ONE
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@checkout_car@", tlPrefix, tlActivityOneLetter, tlActivityTwoLetter)
				pedAnimData.sFacialAnimClip = pedAnimData.sAnimClip
				pedAnimData.sFacialAnimClip += "_facial"
				pedAnimData.bVFXAnimClip = TRUE
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ELSE
					GET_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ENDIF
			ENDIF
		BREAK
		CASE PED_TRANS_STATE_ACTIVITY_TWO
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@checkout_car@", tlPrefix, tlActivityTwoLetter, tlActivityOneLetter)
				pedAnimData.sFacialAnimClip = pedAnimData.sAnimClip
				pedAnimData.sFacialAnimClip += "_facial"
				pedAnimData.bVFXAnimClip = TRUE
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ELSE
					GET_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip, PED_TRANSITION_STATES eTransitionState, BOOL bPedTransition, TEXT_LABEL_23 tlPrefix, TEXT_LABEL_3 tlActivityOneLetter, TEXT_LABEL_3 tlActivityTwoLetter, BOOL bMalePed = TRUE, ANIMATION_FLAGS eAnimFlags = AF_DEFAULT)
	pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
	pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
	
	IF (eAnimFlags != AF_DEFAULT)
		pedAnimData.animFlags = eAnimFlags
	ENDIF
	
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_TRANSITION_TASK_ANIM)
	SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
	
	SWITCH eTransitionState
		CASE PED_TRANS_STATE_ACTIVITY_ONE
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@take_photos@", tlPrefix, tlActivityOneLetter, tlActivityTwoLetter)
				pedAnimData.bVFXAnimClip = TRUE
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ELSE
					GET_CAR_MEET_TAKE_PHOTOS_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityOneLetter)
				ENDIF
			ENDIF
		BREAK
		CASE PED_TRANS_STATE_ACTIVITY_TWO
			IF (bPedTransition)
				GET_CAR_MEET_TRANS_ANIM_CLIP(pedAnimData, iClip, "anim@amb@carmeet@take_photos@", tlPrefix, tlActivityTwoLetter, tlActivityOneLetter)
				pedAnimData.bVFXAnimClip = TRUE
			ELSE
				IF (bMalePed)
					GET_CAR_MEET_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ELSE
					GET_CAR_MEET_TAKE_PHOTOS_FEMALE_ANIM_CLIP(pedAnimData, iClip, tlActivityTwoLetter)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

//╒═══════════════════════════════╕
//╞════════╡ ANIM LOOKUP ╞════════╡
//╘═══════════════════════════════╛
PROC _GET_CAR_MEET_PED_ANIM_DATA(PED_ACTIVITIES eActivity, PED_ANIM_DATA &pedAnimData, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, INT iPedID = 0, PED_TRANSITION_STATES eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE, BOOL bPedTransition = FALSE)
	UNUSED_PARAMETER(iPedID)
	UNUSED_PARAMETER(eMusicIntensity)
	UNUSED_PARAMETER(ePedMusicIntensity)
	UNUSED_PARAMETER(bDancingTransition)
	RESET_PED_ANIM_DATA(pedAnimData)
	TEXT_LABEL_3 letter
	TEXT_LABEL_3 letterTwo
	TEXT_LABEL_23 prefix
	SWITCH eActivity
		CASE PED_ACT_LEANING_TEXTING_M_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@TEXTING@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_LEANING_TEXTING_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_LEANING@FEMALE@WALL@BACK@TEXTING@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_ON_PHONE_M_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_LEANING@MALE@WALL@BACK@MOBILE@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_ON_PHONE_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_LEANING@FEMALE@WALL@BACK@MOBILE@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_LEANING_SMOKING_M_01
		CASE PED_ACT_LEANING_SMOKING_F_01
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_CAR_MEET_LEANING_SMOKING_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_STANDING_SMOKING_M_01
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_CAR_MEET_STANDING_SMOKING_M_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_STANDING_SMOKING_F_01
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.2, 0, DEFAULT, TRUE)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_CAR_MEET_STANDING_SMOKING_F_ANIM_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_HANGOUT_M_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_HANG_OUT_STREET@MALE_C@BASE"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE PED_ACT_HANGOUT_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_HANG_OUT_STREET@FEMALE_HOLD_ARM@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_B"
		BREAK
		CASE PED_ACT_SMOKING_WEED_M_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_SMOKING_POT@MALE@BASE"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE PED_ACT_SMOKING_WEED_F_01
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_SMOKING_POT@FEMALE@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimClip 		= "IDLE_A"
		BREAK
		CASE PED_ACT_CAR_MEET_DRINK_COFFEE_M
			pedAnimData.sAnimDict		= "amb@world_human_drinking@coffee@male@base"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_CAR_MEET_DRINK_COFFEE_F
			pedAnimData.sAnimDict		= "amb@world_human_drinking@coffee@female@base"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_CAR_MEET_PHONE_F
			pedAnimData.sAnimDict		= "AMB@WORLD_HUMAN_STAND_MOBILE@FEMALE@TEXT@IDLE_A"
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE PED_ACT_CAR_MEET_PHONE_M
			pedAnimData.sAnimDict 		= "AMB@WORLD_HUMAN_STAND_MOBILE@MALE@TEXT@IDLE_A"
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE PED_ACT_CAR_MEET_PAPARAZZI				
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SCENARIO_IN_PLACE)	
			pedAnimData.sAnimClip = "WORLD_HUMAN_PAPARAZZI"
		BREAK
		CASE PED_ACT_CAR_MEET_INSPECT
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SCENARIO_IN_PLACE)	
			pedAnimData.sAnimClip = "WORLD_HUMAN_INSPECT_STAND"
		BREAK
		
		
		////////////////////NEW ANIMATIONS//////////////////////////////
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			
			letter = "a"
			GET_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_B
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			
			letter = "b"
			GET_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_FEMALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "a"
			GET_CAR_MEET_CHECKOUT_ENGINE_DRINK_FEMALE_ANIM_CLIP(pedAnimData,iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			letter = "a"			
			GET_CAR_MEET_CHECKOUT_ENGINE_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			letter = "b"			
			GET_CAR_MEET_CHECKOUT_ENGINE_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_C
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			letter = "c"			
			GET_CAR_MEET_CHECKOUT_ENGINE_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_D
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			letter = "d"			
			GET_CAR_MEET_CHECKOUT_ENGINE_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "a"
			GET_CAR_MEET_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip,letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "b"
			GET_CAR_MEET_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip,letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "c"
			GET_CAR_MEET_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip,letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "d"
			GET_CAR_MEET_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip,letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "e"
			GET_CAR_MEET_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip,letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_F			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "f"
			GET_CAR_MEET_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip,letter)		
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_G		
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "g"
			GET_CAR_MEET_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip,letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_H
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "h"
			GET_CAR_MEET_CHECKOUT_ENGINE_MALE_ANIM_CLIP(pedAnimData, iClip,letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_A
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_meet_cig_smoke_exhale_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_meet_cig_smoke_exhale_2"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_mouth", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.65, 0, DEFAULT, TRUE, DEFAULT, TRUE)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "a"
			GET_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_ANIM_CLIP(pedAnimData,iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_B
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_meet_cig_smoke_exhale_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_meet_cig_smoke_exhale_2"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_mouth", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.65, 0, DEFAULT, TRUE, DEFAULT, TRUE)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "b"
			GET_CAR_MEET_CHECKOUT_ENGINE_SMOKE_MALE_ANIM_CLIP(pedAnimData,iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_SMOKE_FEMALE_A
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_meet_cig_smoke_exhale_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_meet_cig_smoke_exhale_2"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_nose", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.65, 0, DEFAULT, TRUE, DEFAULT, TRUE)
				
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "a"
			GET_CAR_MEET_CHECKOUT_ENGINE_SMOKE_FEMALE_ANIM_CLIP(pedAnimData,iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "a"
			GET_CAR_MEET_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "b"
			GET_CAR_MEET_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "c"
			GET_CAR_MEET_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "d"
			GET_CAR_MEET_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_E
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "e"
			GET_CAR_MEET_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_F
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "f"
			GET_CAR_MEET_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_G
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "g"
			GET_CAR_MEET_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_H
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "h"
			GET_CAR_MEET_CHECKOUT_CAR_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "a"
			GET_CAR_MEET_CHECKOUT_CAR_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_B
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "b"
			GET_CAR_MEET_CHECKOUT_CAR_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_C
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "c"
			GET_CAR_MEET_CHECKOUT_CAR_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_D
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			letter = "d"
			GET_CAR_MEET_CHECKOUT_CAR_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_FEMALE_A		
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			letter = "a"
			GET_CAR_MEET_CHECKOUT_CAR_DRINK_FEMALE_ANIM_CLIP(pedAnimData,iClip,letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_DRINK_MALE_A
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			
			letter = "a"
			GET_CAR_MEET_CHECKOUT_CAR_DRINK_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_meet_cig_smoke_exhale_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_meet_cig_smoke_exhale_2"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_nose", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.65, 0, DEFAULT, TRUE, DEFAULT, TRUE)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			letter = "a"
			GET_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_ANIM_CLIP(pedAnimData,iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_B
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_meet_cig_smoke_exhale_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_meet_cig_smoke_exhale_2"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_nose", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.65, 0, DEFAULT, TRUE, DEFAULT, TRUE)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			letter = "b"
			GET_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_ANIM_CLIP(pedAnimData,iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_C
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_meet_cig_smoke_exhale_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_meet_cig_smoke_exhale_2"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_mouth", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.65, 0, DEFAULT, TRUE, DEFAULT, TRUE)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			letter = "c"
			GET_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_ANIM_CLIP(pedAnimData,iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_A
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_meet_cig_smoke_exhale_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_meet_cig_smoke_exhale_2"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_mouth", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.65, 0, DEFAULT, TRUE, DEFAULT, TRUE)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
			letter = "a"
			GET_CAR_MEET_CHECKOUT_CAR_SMOKE_FEMALE_ANIM_CLIP(pedAnimData,iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A			
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "a"
			GET_CAR_MEET_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B	
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "b"
			GET_CAR_MEET_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C	
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "c"
			GET_CAR_MEET_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D		
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "d"
			GET_CAR_MEET_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E		
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "e"
			GET_CAR_MEET_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F	
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "f"
			GET_CAR_MEET_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_G			
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "g"
			GET_CAR_MEET_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_H	
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "h"
			GET_CAR_MEET_TAKE_PHOTOS_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "a"
			GET_CAR_MEET_TAKE_PHOTOS_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "b"
			GET_CAR_MEET_TAKE_PHOTOS_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_C		
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "c"
			GET_CAR_MEET_TAKE_PHOTOS_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_D			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON)
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			letter = "d"
			GET_CAR_MEET_TAKE_PHOTOS_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A	
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME	
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)		
			letter = "a"	
			GET_CAR_MEET_TAKE_LISTEN_MUSIC_MALE_ANIM_CLIP(pedAnimData, iClip, letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B	
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)		
			letter = "b"	
			GET_CAR_MEET_TAKE_LISTEN_MUSIC_MALE_ANIM_CLIP(pedAnimData, iClip, letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_C	
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME		
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)		
			letter = "c"	
			GET_CAR_MEET_TAKE_LISTEN_MUSIC_MALE_ANIM_CLIP(pedAnimData, iClip, letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_D			
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME		
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)		
			letter = "d"	
			GET_CAR_MEET_TAKE_LISTEN_MUSIC_MALE_ANIM_CLIP(pedAnimData, iClip, letter)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_A	
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME		
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)		
			letter = "a"	
			GET_CAR_MEET_TAKE_LISTEN_MUSIC_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_B	
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_HOLD_LAST_FRAME		
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)		
			letter = "b"	
			GET_CAR_MEET_TAKE_LISTEN_MUSIC_FEMALE_ANIM_CLIP(pedAnimData, iClip, letter)	
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_DRINK_MALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "drink_male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_DRINK_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_C
			letter 		= "a"
			letterTwo 	= "c"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_D
			letter 		= "a"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_E
			letter 		= "a"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_F
			letter 		= "a"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_G
			letter 		= "a"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_A_TRANS_H
			letter 		= "a"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_C
			letter 		= "b"
			letterTwo 	= "c"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_D
			letter 		= "b"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_E
			letter 		= "b"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_F
			letter 		= "b"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_G
			letter 		= "b"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_B_TRANS_H
			letter 		= "b"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_D
			letter 		= "c"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_E
			letter 		= "c"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_F
			letter 		= "c"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_G
			letter 		= "c"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_C_TRANS_H
			letter 		= "c"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_E
			letter 		= "d"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_F
			letter 		= "d"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_G
			letter 		= "d"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_D_TRANS_H
			letter 		= "d"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E_TRANS_F
			letter 		= "e"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E_TRANS_G
			letter 		= "e"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_E_TRANS_H
			letter 		= "e"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_F_TRANS_G
			letter 		= "f"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_F_TRANS_H
			letter 		= "f"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_MALE_G_TRANS_H
			letter 		= "g"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "female_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A_TRANS_C
			letter 		= "a"
			letterTwo 	= "c"
			prefix 		= "female_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_A_TRANS_D
			letter 		= "a"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B_TRANS_C
			letter 		= "b"
			letterTwo 	= "c"
			prefix 		= "female_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_B_TRANS_D
			letter 		= "b"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_ENGINE_FEMALE_C_TRANS_D
			letter 		= "c"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_CHECKOUT_ENGINE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_C
			letter 		= "a"
			letterTwo 	= "c"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_D
			letter 		= "a"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_E
			letter 		= "a"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_F
			letter 		= "a"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_G
			letter 		= "a"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_A_TRANS_H
			letter 		= "a"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_C
			letter 		= "b"
			letterTwo 	= "c"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_D
			letter 		= "b"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_E
			letter 		= "b"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_F
			letter 		= "b"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_G
			letter 		= "b"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_B_TRANS_H
			letter 		= "b"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_D
			letter 		= "c"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_E
			letter 		= "c"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_F
			letter 		= "c"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_G
			letter 		= "c"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_C_TRANS_H
			letter 		= "c"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_E
			letter 		= "d"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_F
			letter 		= "d"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_G
			letter 		= "d"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_D_TRANS_H
			letter 		= "d"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_E_TRANS_F
			letter 		= "e"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_E_TRANS_G
			letter 		= "e"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_E_TRANS_H
			letter 		= "e"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_F_TRANS_G
			letter 		= "f"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_F_TRANS_H
			letter 		= "f"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_MALE_G_TRANS_H
			letter 		= "g"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "female_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A_TRANS_C
			letter 		= "a"
			letterTwo 	= "c"
			prefix 		= "female_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_A_TRANS_D
			letter 		= "a"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE, (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON))
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_B_TRANS_C
			letter 		= "b"
			letterTwo 	= "c"
			prefix 		= "female_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_B_TRANS_D
			letter 		= "b"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE, (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON))
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_FEMALE_C_TRANS_D
			letter 		= "c"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_CHECKOUT_CAR_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE, (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON))
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A_TRANS_B
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_meet_cig_smoke_exhale_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_meet_cig_smoke_exhale_2"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_nose", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.65, 0, DEFAULT, TRUE, DEFAULT, TRUE)
			
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "smoke_male_"
			GET_CAR_MEET_CHECKOUT_CAR_SMOKE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_A_TRANS_C
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_meet_cig_smoke_exhale_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_meet_cig_smoke_exhale_2"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_mouth", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.65, 0, DEFAULT, TRUE, DEFAULT, TRUE)
			
			letter 		= "a"
			letterTwo 	= "c"
			prefix 		= "smoke_male_"
			GET_CAR_MEET_CHECKOUT_CAR_SMOKE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_CHECKOUT_CAR_SMOKE_MALE_B_TRANS_C
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_meet_cig_smoke_exhale_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_meet_cig_smoke_exhale_2"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_mouth", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "FOREVER", "FOREVER", "scr_tn_meet", "scr_tn_meet_cig_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 0.65, 0, DEFAULT, TRUE, DEFAULT, TRUE)
			
			letter 		= "b"
			letterTwo 	= "c"
			prefix 		= "smoke_male_"
			GET_CAR_MEET_CHECKOUT_CAR_SMOKE_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_B
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_C
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "a"
			letterTwo 	= "c"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_D
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "a"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_E
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "a"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, DEFAULT, (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON))
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_F
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "a"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_G
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "a"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_A_TRANS_H
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "a"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_C
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "b"
			letterTwo 	= "c"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_D
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "b"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_E
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "b"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, DEFAULT, (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON))
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_F
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "b"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_G
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "b"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_B_TRANS_H
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "b"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_D
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "c"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_E
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "c"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, DEFAULT, (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON))
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_F
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "c"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_G
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "c"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_C_TRANS_H
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "c"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_E
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "d"
			letterTwo 	= "e"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, DEFAULT, (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON))
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_F
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "d"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_G
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "d"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_D_TRANS_H
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "d"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_F
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "e"
			letterTwo 	= "f"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, DEFAULT, (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON))
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_G
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "e"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, DEFAULT, (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON))
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_E_TRANS_H
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "e"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, DEFAULT, (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON))
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F_TRANS_G
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "f"
			letterTwo 	= "g"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_F_TRANS_H
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "f"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_MALE_G_TRANS_H
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "g"
			letterTwo 	= "h"
			prefix 		= "male_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_B
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "female_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE, (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON))
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_C
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "a"
			letterTwo 	= "c"
			prefix 		= "female_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_A_TRANS_D
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "a"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B_TRANS_C
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "b"
			letterTwo 	= "c"
			prefix 		= "female_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE, (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON))
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_B_TRANS_D
			letter 		= "b"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE, (AF_HOLD_LAST_FRAME | AF_EXPAND_PED_CAPSULE_FROM_SKELETON))
		BREAK
		CASE PED_ACT_CAR_MEET_TAKE_PHOTOS_FEMALE_C_TRANS_D
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_tun_carmeet_phone_camera_flash_trigger_1"
			pedAnimData.VFXOneShotData.sAnimEvent[1] = "vfx_tun_carmeet_phone_camera_flash_trigger_2"
			pedAnimData.VFXOneShotData.sAnimEvent[2] = "vfx_tun_carmeet_phone_camera_flash_trigger_3"
			pedAnimData.VFXOneShotData.sAnimEvent[3] = "vfx_tun_carmeet_phone_camera_flash_trigger_4"
			pedAnimData.VFXOneShotData.sAnimEvent[4] = "vfx_tun_carmeet_phone_camera_flash_trigger_5"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_phone_camera_flash", "DLC_Tuner_Car_Meet_Scripted_Sounds", "Ped_Take_Picture", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, BONETAG_PH_R_HAND, FALSE, TRUE, FALSE, 15)
			letter 		= "c"
			letterTwo 	= "d"
			prefix 		= "female_"
			GET_CAR_MEET_TAKE_PHOTOS_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "male_"
			GET_CAR_MEET_LISTEN_MUSIC_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_C
			letter 		= "a"
			letterTwo 	= "c"
			prefix 		= "male_"
			GET_CAR_MEET_LISTEN_MUSIC_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_A_TRANS_D
			letter 		= "a"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_LISTEN_MUSIC_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B_TRANS_C
			letter 		= "b"
			letterTwo 	= "c"
			prefix 		= "male_"
			GET_CAR_MEET_LISTEN_MUSIC_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_B_TRANS_D
			letter 		= "b"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_LISTEN_MUSIC_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_MALE_C_TRANS_D
			letter 		= "c"
			letterTwo 	= "d"
			prefix 		= "male_"
			GET_CAR_MEET_LISTEN_MUSIC_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo)
		BREAK
		CASE PED_ACT_CAR_MEET_LISTEN_MUSIC_FEMALE_A_TRANS_B
			letter 		= "a"
			letterTwo 	= "b"
			prefix 		= "female_"
			GET_CAR_MEET_LISTEN_MUSIC_TRANSITION_ANIM_CLIP(pedAnimData, iClip, eTransitionState, bPedTransition, prefix, letter, letterTwo, FALSE)
		BREAK
	ENDSWITCH
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PROP ANIM DATA ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _GET_CAR_MEET_PED_PROP_ANIM_DATA(PED_ACTIVITIES eActivity, PED_PROP_ANIM_DATA &pedPropAnimData, INT iProp = 0, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bHeeledPed = FALSE)
	UNUSED_PARAMETER(eMusicIntensity)
	UNUSED_PARAMETER(ePedMusicIntensity)
	UNUSED_PARAMETER(bDancingTransition)
	UNUSED_PARAMETER(bHeeledPed)
	RESET_PED_PROP_ANIM_DATA(pedPropAnimData)
	
	SWITCH eActivity
		CASE PED_ACT_PAVEL_SCREENS
			pedPropAnimData.sPropAnimDict = "anim@scripted@submarine@special_peds@pavel@hs4_pavel_ig1_screens"
			SWITCH iProp
				CASE 0	// P_AMB_CLIPBOARD_01
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_CLIPBOARD"					BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_CLIPBOARD"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_CLIPBOARD"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_CLIPBOARD"						BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "IDLE_D_CLIPBOARD"						BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "Beep_to_you_too_CLIPBOARD"				BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "Bide_your_time_CLIPBOARD"				BREAK
						CASE 7	pedPropAnimData.sPropAnimClip = "Fucking_bridge_CLIPBOARD"				BREAK
						CASE 8	pedPropAnimData.sPropAnimClip = "Fucking_screens_CLIPBOARD"				BREAK
						CASE 9	pedPropAnimData.sPropAnimClip = "Piece_of_shit_CLIPBOARD"				BREAK
						CASE 10	pedPropAnimData.sPropAnimClip = "Prefer_when_it_was_tv_CLIPBOARD"		BREAK
						CASE 11	pedPropAnimData.sPropAnimClip = "Six_degrees_CLIPBOARD"					BREAK
						CASE 12	pedPropAnimData.sPropAnimClip = "Sure_its_nothing_CLIPBOARD"			BREAK
						CASE 13	pedPropAnimData.sPropAnimClip = "Why_letter_so_small_CLIPBOARD"			BREAK
						CASE 14	pedPropAnimData.sPropAnimClip = "Wonder_what_that_means_CLIPBOARD"		BREAK
					ENDSWITCH
				BREAK
				CASE 1	// PROP_PENCIL_01
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_PENCIL"						BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_PENCIL"							BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_PENCIL"							BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_PENCIL"							BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "IDLE_D_PENCIL"							BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "Beep_to_you_too_PENCIL"				BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "Bide_your_time_PENCIL"					BREAK
						CASE 7	pedPropAnimData.sPropAnimClip = "Fucking_bridge_PENCIL"					BREAK
						CASE 8	pedPropAnimData.sPropAnimClip = "Fucking_screens_PENCIL"				BREAK
						CASE 9	pedPropAnimData.sPropAnimClip = "Piece_of_shit_PENCIL"					BREAK
						CASE 10	pedPropAnimData.sPropAnimClip = "Prefer_when_it_was_tv_PENCIL"			BREAK
						CASE 11	pedPropAnimData.sPropAnimClip = "Six_degrees_PENCIL"					BREAK
						CASE 12	pedPropAnimData.sPropAnimClip = "Sure_its_nothing_PENCIL"				BREAK
						CASE 13	pedPropAnimData.sPropAnimClip = "Why_letter_so_small_PENCIL"			BREAK
						CASE 14	pedPropAnimData.sPropAnimClip = "Wonder_what_that_means_PENCIL"			BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_PAVEL_TORPEDOS
			pedPropAnimData.sPropAnimDict = "anim@scripted@submarine@special_peds@pavel@hs4_pavel_ig4_torpedos"
			SWITCH iProp
				CASE 0	// PROP_CS_CIGGY_01B
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_PROP_CS_CIGGY_01B"			BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_PROP_CS_CIGGY_01B"				BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_PROP_CS_CIGGY_01B"				BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_PROP_CS_CIGGY_01B"				BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "GESTURE_PROP_CS_CIGGY_01B"				BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_PAVEL_CAVIAR
			pedPropAnimData.sPropAnimDict = "anim@scripted@submarine@special_peds@pavel@hs4_pavel_ig5_caviar_p1"
			SWITCH iProp
				CASE 0	// h4_prop_h4_Caviar_Tin_01a
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_TIN"							BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_TIN"							BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_TIN"							BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_TIN"							BREAK
					ENDSWITCH
				BREAK
				CASE 1	// h4_prop_h4_Caviar_Spoon_01a
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_SPOON"						BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_SPOON"							BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_SPOON"							BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_SPOON"							BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_PAVEL_ENGINE
			pedPropAnimData.sPropAnimDict = "anim@scripted@submarine@special_peds@pavel@hs4_pavel_ig6_engine"
			SWITCH iProp
				CASE 0	// Prop_CS_Wrench
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_WRENCH"						BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_WRENCH"							BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_WRENCH"							BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_WRENCH"							BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "IDLE_D_WRENCH"							BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "DONT_DO_THIS_WRENCH"					BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "DONT_TALK_BACK_WRENCH"					BREAK
						CASE 7	pedPropAnimData.sPropAnimClip = "FINE_WRENCH"							BREAK
						CASE 8	pedPropAnimData.sPropAnimClip = "GENTLY_WRENCH"							BREAK
						CASE 9	pedPropAnimData.sPropAnimClip = "IF_I_HAVE_TO_COMEDOWN_WRENCH"			BREAK
						CASE 10	pedPropAnimData.sPropAnimClip = "I_AM_PATIENT_WRENCH"					BREAK
						CASE 11	pedPropAnimData.sPropAnimClip = "I_WARNED_YOU_WRENCH"					BREAK
						CASE 12	pedPropAnimData.sPropAnimClip = "PERFECT_WRENCH"						BREAK
						CASE 13	pedPropAnimData.sPropAnimClip = "PLAY_NICE_WRENCH"						BREAK
						CASE 14	pedPropAnimData.sPropAnimClip = "UGH_IAM_SORRY_WRENCH"					BREAK
					ENDSWITCH
				BREAK
				CASE 1	// h4_Prop_h4_Engine_FuseBox_01a
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "BASE_IDLE_FUSE"						BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "IDLE_A_FUSE"							BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "IDLE_B_FUSE"							BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "IDLE_C_FUSE"							BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "IDLE_D_FUSE"							BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "DONT_DO_THIS_FUSE"						BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "DONT_TALK_BACK_FUSE"					BREAK
						CASE 7	pedPropAnimData.sPropAnimClip = "FINE_FUSE"								BREAK
						CASE 8	pedPropAnimData.sPropAnimClip = "GENTLY_FUSE"							BREAK
						CASE 9	pedPropAnimData.sPropAnimClip = "IF_I_HAVE_TO_COMEDOWN_FUSE"			BREAK
						CASE 10	pedPropAnimData.sPropAnimClip = "I_AM_PATIENT_FUSE"						BREAK
						CASE 11	pedPropAnimData.sPropAnimClip = "I_WARNED_YOU_FUSE"						BREAK
						CASE 12	pedPropAnimData.sPropAnimClip = "PERFECT_FUSE"							BREAK
						CASE 13	pedPropAnimData.sPropAnimClip = "PLAY_NICE_FUSE"						BREAK
						CASE 14	pedPropAnimData.sPropAnimClip = "UGH_IAM_SORRY_FUSE"					BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ DANCING DATA ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _DOES_CAR_MEET_PED_LOCATION_USE_DANCING_PEDS()
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ CUTSCENE PEDS ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _SHOULD_HIDE_CAR_MEET_CUTSCENE_PED(SCRIPT_PED_DATA &LocalData, INT iLayout, INT iPed)
	UNUSED_PARAMETER(LocalData)
	
	IF (iLayout >= 0 AND iLayout <= 29)  // Ambient Layout 0
		SWITCH iPed
			CASE 20
				RETURN TRUE
		ENDSWITCH
	ENDIF
	
	// These peds are the same for all layouts
	SWITCH iPed
		CASE 27
		CASE 38
		CASE 42
		CASE 43
			RETURN TRUE
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PATROL DATA ╞═══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC GET_RANDOM_CAR_MEET_PED_PATROL_INSPECT_SEQUENCE()
	
	INT iRandSequence
	iRandSequence = GET_RANDOM_INT_IN_RANGE(0, 4)
	
	IF (iRandSequence = 0)
		TASK_PLAY_ANIM(NULL, "ANIM@AMB@INSPECT@STAND@MALE_A@BASE", 	"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
		TASK_PLAY_ANIM(NULL, "ANIM@AMB@INSPECT@STAND@MALE_A@IDLES", "IDLE_A", 	WALK_BLEND_IN, WALK_BLEND_OUT)
		TASK_PLAY_ANIM(NULL, "ANIM@AMB@INSPECT@STAND@MALE_A@IDLES", "IDLE_B", 	WALK_BLEND_IN, WALK_BLEND_OUT)
		TASK_PLAY_ANIM(NULL, "ANIM@AMB@INSPECT@STAND@MALE_A@BASE", 	"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
		
	ELIF (iRandSequence = 1)
		TASK_PLAY_ANIM(NULL, "ANIM@AMB@INSPECT@STAND@MALE_A@BASE", 	"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
		TASK_PLAY_ANIM(NULL, "ANIM@AMB@INSPECT@STAND@MALE_A@IDLES", "IDLE_B", 	WALK_BLEND_IN, WALK_BLEND_OUT)
		TASK_PLAY_ANIM(NULL, "ANIM@AMB@INSPECT@STAND@MALE_A@IDLES", "IDLE_C", 	WALK_BLEND_IN, WALK_BLEND_OUT)
		TASK_PLAY_ANIM(NULL, "ANIM@AMB@INSPECT@STAND@MALE_A@BASE", 	"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
	
	ELIF (iRandSequence = 2)
		TASK_PLAY_ANIM(NULL, "ANIM@AMB@INSPECT@STAND@MALE_A@BASE", 	"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
		TASK_PLAY_ANIM(NULL, "ANIM@AMB@INSPECT@STAND@MALE_A@IDLES", "IDLE_C", 	WALK_BLEND_IN, WALK_BLEND_OUT)
		TASK_PLAY_ANIM(NULL, "ANIM@AMB@INSPECT@STAND@MALE_A@IDLES", "IDLE_D", 	WALK_BLEND_IN, WALK_BLEND_OUT)
		TASK_PLAY_ANIM(NULL, "ANIM@AMB@INSPECT@STAND@MALE_A@BASE", 	"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
		
	ELIF (iRandSequence = 3)
		TASK_PLAY_ANIM(NULL, "ANIM@AMB@INSPECT@STAND@MALE_A@BASE", 	"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
		TASK_PLAY_ANIM(NULL, "ANIM@AMB@INSPECT@STAND@MALE_A@IDLES", "IDLE_D", 	WALK_BLEND_IN, WALK_BLEND_OUT)
		TASK_PLAY_ANIM(NULL, "ANIM@AMB@INSPECT@STAND@MALE_A@IDLES", "IDLE_A", 	WALK_BLEND_IN, WALK_BLEND_OUT)
		TASK_PLAY_ANIM(NULL, "ANIM@AMB@INSPECT@STAND@MALE_A@BASE", 	"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
	ENDIF
	
ENDPROC

PROC GET_RANDOM_CAR_MEET_PED_PATROL_MOBILE_FILM_SHOCKED_SEQUENCE(BOOL bFemalePed = FALSE)
	
	INT iRandSequence
	iRandSequence = GET_RANDOM_INT_IN_RANGE(0, 3)
	
	IF (bFemalePed)
		IF (iRandSequence = 0)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@ENTER", 	"ENTER", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@BASE", 	"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@IDLE_A",	"IDLE_A", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@IDLE_A",	"IDLE_B", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@IDLE_A",	"IDLE_C", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@BASE", 	"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@EXIT", 	"EXIT", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			
		ELIF (iRandSequence = 1)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@ENTER", 	"ENTER", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@BASE", 	"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@IDLE_A",	"IDLE_B", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@IDLE_A",	"IDLE_C", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@IDLE_A",	"IDLE_A", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@BASE", 	"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@EXIT", 	"EXIT", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			
		ELIF (iRandSequence = 2)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@ENTER", 	"ENTER", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@BASE", 	"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@IDLE_A",	"IDLE_C", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@IDLE_A",	"IDLE_A", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@IDLE_A",	"IDLE_B", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@BASE", 	"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@FEMALE@EXIT", 	"EXIT", 	WALK_BLEND_IN, WALK_BLEND_OUT)
		ENDIF
	ELSE
		IF (iRandSequence = 0)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@ENTER", 	"ENTER", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@BASE", 		"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@IDLE_A",	"IDLE_A", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@IDLE_A",	"IDLE_B", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@IDLE_A",	"IDLE_C", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@BASE", 		"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@EXIT", 		"EXIT", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			
		ELIF (iRandSequence = 1)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@ENTER", 	"ENTER", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@BASE", 		"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@IDLE_A",	"IDLE_B", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@IDLE_A",	"IDLE_C", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@IDLE_A",	"IDLE_A", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@BASE", 		"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@EXIT", 		"EXIT", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			
		ELIF (iRandSequence = 2)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@ENTER", 	"ENTER", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@BASE", 		"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@IDLE_A",	"IDLE_C", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@IDLE_A",	"IDLE_A", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@IDLE_A",	"IDLE_B", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@BASE", 		"BASE", 	WALK_BLEND_IN, WALK_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, "AMB@WORLD_HUMAN_MOBILE_FILM_SHOCKING@MALE@EXIT", 		"EXIT", 	WALK_BLEND_IN, WALK_BLEND_OUT)
		ENDIF
	ENDIF
	
ENDPROC

PROC _SET_CAR_MEET_PED_PATROL_DATA(SEQUENCE_INDEX &SequenceIndex, INT iLayout, INT iArrayID, INT &iPatrolPedID[], BOOL &bShowProp[], INT iTask, INT &iMaxSequence, BOOL bNetworkPed = FALSE)
	
	// Max tasks in a sequence is 10 (code limit).
	SequenceIndex = NULL
	iMaxSequence = 0
	
	FLOAT fRadius = 0.4
	BOOL bFemalePed = FALSE
	
	IF (bNetworkPed)
		SWITCH iArrayID
			CASE 0
				iPatrolPedID[iArrayID] = iArrayID
				iMaxSequence = 5
				
				IF (iLayout >= 100 AND iLayout <= 129)	// Ambeint Layout 1
					bFemalePed = TRUE
				ENDIF
				
				bShowProp[iTask] = FALSE
				
				SWITCH iTask
					CASE 0
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2170.8303, 1133.2251, -24.3711>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2169.382, 1137.3483, -24.3711>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_ACHIEVE_HEADING(NULL, 265.0589)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK
					CASE 1
						OPEN_SEQUENCE_TASK(SequenceIndex)
							GET_RANDOM_CAR_MEET_PED_PATROL_INSPECT_SEQUENCE()
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK
					CASE 2
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2196.7383, 1144.6038, -24.2981>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2212.8970, 1134.4181, -24.2590>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2213.217, 1096.826, -24.258>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_ACHIEVE_HEADING(NULL, 88.7544)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK
					CASE 3
						OPEN_SEQUENCE_TASK(SequenceIndex)
							GET_RANDOM_CAR_MEET_PED_PATROL_INSPECT_SEQUENCE()
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK
					CASE 4
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2212.0586, 1079.6754, -24.2579>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2170.868, 1077.340, -24.3627>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_ACHIEVE_HEADING(NULL, 190.9344)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK
					CASE 5
						OPEN_SEQUENCE_TASK(SequenceIndex)
							GET_RANDOM_CAR_MEET_PED_PATROL_MOBILE_FILM_SHOCKED_SEQUENCE(bFemalePed)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						bShowProp[iTask] = TRUE
					BREAK
				ENDSWITCH
			BREAK
			CASE 1
				iPatrolPedID[iArrayID] = iArrayID
				iMaxSequence = 5
				
				IF (iLayout >= 0 AND iLayout <= 29)	// Ambeint Layout 0
					bFemalePed = TRUE
				ENDIF
				
				bShowProp[iTask] = FALSE
				
				SWITCH iTask
					CASE 0
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2188.3013, 1099.5815, -24.2587>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2173.9863, 1099.5784, -25.3642>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2169.2776, 1125.7380, -25.3709>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2155.1316, 1126.2002, -25.3709>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2149.6907, 1122.2703, -25.3708>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_ACHIEVE_HEADING(NULL, 234.7078)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK
					CASE 1
						OPEN_SEQUENCE_TASK(SequenceIndex)
							GET_RANDOM_CAR_MEET_PED_PATROL_MOBILE_FILM_SHOCKED_SEQUENCE(bFemalePed)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
						bShowProp[iTask] = TRUE
					BREAK
					CASE 2
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2153.0474, 1086.528, -25.3628>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_ACHIEVE_HEADING(NULL, 89.4733)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK
					CASE 3
						OPEN_SEQUENCE_TASK(SequenceIndex)
							GET_RANDOM_CAR_MEET_PED_PATROL_INSPECT_SEQUENCE()
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK
					CASE 4
						OPEN_SEQUENCE_TASK(SequenceIndex)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2153.8242, 1079.1854, -25.3622>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2156.9231, 1079.1349, -25.3621>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2161.7393, 1079.0138, -25.3620>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2167.3345, 1078.7189, -25.3621>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2178.1152, 1077.3325, -24.2215>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2201.4321, 1080.0991, -24.2575>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-2191.670, 1087.751, -24.258>>, PEDMOVEBLENDRATIO_WALK, -1, fRadius, ENAV_NO_STOPPING)
							TASK_ACHIEVE_HEADING(NULL, 301.320)
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK
					CASE 5
						OPEN_SEQUENCE_TASK(SequenceIndex)
							GET_RANDOM_CAR_MEET_PED_PATROL_INSPECT_SEQUENCE()
						CLOSE_SEQUENCE_TASK(SequenceIndex)
					BREAK
				ENDSWITCH
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

FUNC INT _GET_CAR_MEET_ACTIVE_PED_LEVEL_THRESHOLD(INT iLevel)
	INT iThreshold = -1
	SWITCH iLevel
		// Peds have level 0, 1 & 2. 
		// Level 2 peds are culled if the player count > 10
		// Level 1 peds are culled if the player count > 20
		// Level 0 peds are base and always remain
		CASE 0	iThreshold = 20	BREAK
		CASE 1	iThreshold = 10	BREAK
		CASE 2	iThreshold = 0	BREAK
	ENDSWITCH
	RETURN iThreshold
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ LOOK-UP TABLE ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC BUILD_PED_CAR_MEET_LOOK_UP_TABLE(PED_INTERFACE &interface, PED_INTERFACE_PROCEDURES eProc)
	SWITCH eProc
		// Script Launching
		CASE E_SHOULD_PED_SCRIPT_LAUNCH
			interface.returnShouldPedScriptLaunch = &_SHOULD_CAR_MEET_PED_SCRIPT_LAUNCH
		BREAK
		CASE E_IS_PARENT_A_SIMPLE_INTERIOR
			interface.returnIsParentASimpleInterior = &_IS_CAR_MEET_PARENT_A_SIMPLE_INTERIOR
		BREAK
		
		// Ped Data
		CASE E_GET_LOCAL_PED_TOTAL
			interface.returnGetLocalPedTotal = &_GET_CAR_MEET_LOCAL_PED_TOTAL
		BREAK
		CASE E_GET_NETWORK_PED_TOTAL
			interface.returnGetNetworkPedTotal = &_GET_CAR_MEET_NETWORK_PED_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT_TOTAL
			interface.returnGetServerPedLayoutTotal= &_GET_CAR_MEET_SERVER_PED_LAYOUT_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT
			interface.returnGetServerPedLayout = &_GET_CAR_MEET_SERVER_PED_LAYOUT
		BREAK
		CASE E_GET_SERVER_PED_LEVEL
			interface.returnGetServerPedLevel = &_GET_CAR_MEET_SERVER_PED_LEVEL
		BREAK
		CASE E_SET_PED_DATA
			interface.setPedData = &_SET_CAR_MEET_PED_DATA
		BREAK
		CASE E_SET_PED_SERVER_DATA
			interface.setPedServerData = &_SET_CAR_MEET_PED_SERVER_DATA
		BREAK
		CASE E_GET_PED_ANIM_DATA
			interface.getPedAnimData = &_GET_CAR_MEET_PED_ANIM_DATA
		BREAK
		CASE E_GET_PED_PROP_ANIM_DATA
			interface.getPedPropAnimData = &_GET_CAR_MEET_PED_PROP_ANIM_DATA
		BREAK
		CASE E_IS_PLAYER_IN_PARENT_PROPERTY
			interface.returnIsPlayerInParentProperty = &_IS_PLAYER_IN_CAR_MEET_PARENT_PROPERTY
		BREAK
		CASE E_GET_PED_LOCAL_COORDS_BASE_POSITION
			interface.returnGetPedLocalCoordsBasePosition = &_GET_CAR_MEET_PED_LOCAL_COORDS_BASE_POSITION
		BREAK
		CASE E_GET_PED_LOCAL_HEADING_BASE_HEADING
			interface.returnGetPedLocalHeadingBaseHeading = &_GET_CAR_MEET_PED_LOCAL_HEADING_BASE_HEADING
		BREAK
		CASE E_GET_ACTIVE_PED_LEVEL_THRESHOLD
			interface.getActivePedLevelThreshold = &_GET_CAR_MEET_ACTIVE_PED_LEVEL_THRESHOLD
		BREAK
		
		// Ped Creation
		CASE E_SET_LOCAL_PED_PROPERTIES
			interface.setLocalPedProperties = &_SET_CAR_MEET_LOCAL_PED_PROPERTIES
		BREAK
		CASE E_SET_NETWORK_PED_PROPERTIES
			interface.setNetworkPedProperties = &_SET_CAR_MEET_NETWORK_PED_PROPERTIES
		BREAK
		CASE E_SET_PED_PROP_INDEXES
			interface.setPedPropIndexes = &_SET_CAR_MEET_PED_PROP_INDEXES
		BREAK
		CASE E_HAS_PED_BEEN_CREATED
			interface.returnHasPedBeenCreated = &_HAS_CAR_MEET_PED_BEEN_CREATED
		BREAK
		CASE E_SET_PED_CHANGED_CAPSULE_SIZE_DATA
			interface.setPedChangedCapsuleSizeData = &_SET_CAR_MEET_PED_CHANGED_CAPSULE_SIZE_DATA
		BREAK
		
		// Ped Speech
		CASE E_SET_PED_SPEECH_DATA
			interface.setPedSpeechData = &_SET_CAR_MEET_PED_SPEECH_DATA
		BREAK
		CASE E_CAN_PED_PLAY_SPEECH
			interface.returnCanPedPlaySpeech = &_CAN_CAR_MEET_PED_PLAY_SPEECH
		BREAK
		CASE E_GET_PED_SPEECH_TYPE
			interface.returnGetPedSpeechType = &_GET_CAR_MEET_PED_SPEECH_TYPE
		BREAK
		CASE E_GET_PED_CONTROLLER_SPEECH
			interface.returnGetPedControllerSpeech = &_GET_CAR_MEET_PED_CONTROLLER_SPEECH
		BREAK
		CASE E_GET_PED_CONVO_DATA
			interface.getPedConvoData = &_GET_CAR_MEET_PED_CONVO_DATA
		BREAK
		
		// Dancing Data
		CASE E_DOES_PED_LOCATION_USE_DANCING_PEDS
			interface.returnDoesPedLocationUseDancingPeds = &_DOES_CAR_MEET_PED_LOCATION_USE_DANCING_PEDS
		BREAK
		
		// Cutscene Peds
		CASE E_SHOULD_HIDE_CUTSCENE_PED
			interface.returnShouldHideCutscenePed = &_SHOULD_HIDE_CAR_MEET_CUTSCENE_PED
		BREAK
		
		// Patrol Data
		CASE E_SET_PED_PATROL_DATA
			interface.setPedPatrolData = &_SET_CAR_MEET_PED_PATROL_DATA
		BREAK
	ENDSWITCH
ENDPROC
#ENDIF	// FEATURE_TUNER
