//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_ANIMATION.sch																					//
// Description: Header file containing animation functionality for peds.												//
// Written by:  Online Technical Team: Scott Ranken																		//
// Date:  		26/02/20																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_HEIST_ISLAND
USING "net_peds_speech.sch"

//╒══════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════╡ SYNCED SCENES ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Gets the anim max phase to play until.
/// PARAMS:
///    Data - Ped data to query.
/// RETURNS: The anim max phase to play until.
FUNC FLOAT GET_ANIM_MAX_PHASE(PEDS_DATA &Data)
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PLAY_ANIM_FULL_PHASE)
		RETURN FULL_PED_ANIM_PHASE
	ENDIF
	RETURN MAX_PED_ANIM_PHASE
ENDFUNC

/// PURPOSE:
///    Checks whether a sync scene has finished its current clip, or hasn't started playing.
/// PARAMS:
///    Data - Ped data to query.
/// RETURNS: TRUE if a sync scene should be played on a ped.
FUNC BOOL SHOULD_PED_SYNC_SCENE_PLAY(PEDS_DATA &Data)
		
	SCRIPTTASKSTATUS TaskStatus
	INT iSyncSceneID = Data.animData.iSyncSceneID
	
	IF IS_ENTITY_ALIVE(Data.PedID)
		TaskStatus = GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_SYNCHRONIZED_SCENE)
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_NETWORK_ANIMS)
		iSyncSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(Data.animData.iSyncSceneID)
	ENDIF
	
	// DJs update animations based on time
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_DJ_PED)
		IF (Data.animData.iSumAnimDurationMS = 0)
		OR ((Data.animData.iSumAnimDurationMS - g_clubMusicData.iCurrentMixTimeMS <= 0)
			AND ((iSyncSceneID != -1 AND IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneID) >= GET_ANIM_MAX_PHASE(Data))))
			RETURN TRUE
		ENDIF
	ENDIF
	
	IF (iSyncSceneID != -1 AND IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneID) AND IS_SYNCHRONIZED_SCENE_LOOPED(iSyncSceneID))
		RETURN FALSE
	ENDIF
	
	IF (TaskStatus != WAITING_TO_START_TASK AND TaskStatus != PERFORMING_TASK)
	OR (iSyncSceneID != -1 AND IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneID) >= GET_ANIM_MAX_PHASE(Data))
	OR (iSyncSceneID = -1)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_UPDATE_DJ_SYNC_SCENE_SPEED(PEDS_DATA &Data)
		
	SCRIPTTASKSTATUS TaskStatus
	INT iSyncSceneID = Data.animData.iSyncSceneID
	
	IF IS_ENTITY_ALIVE(Data.PedID)
		TaskStatus = GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_SYNCHRONIZED_SCENE)
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_NETWORK_ANIMS)
		iSyncSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(Data.animData.iSyncSceneID)
	ENDIF
			
	IF (TaskStatus = PERFORMING_TASK)
	OR (iSyncSceneID != -1 AND IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneID) AND GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneID) < GET_ANIM_MAX_PHASE(Data))
	OR (iSyncSceneID = -1)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the starting clip and phase for the DJ based on the current track time.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    eActivity - Ped activity to query.
///    fSumAnimDurationMS - The running sum of all the animation durations up to the current animation.
///    fStartPhase - How far into the animation does the DJ need to start.
/// RETURNS: The starting DJ clip.
FUNC INT GET_DJ_SYNC_SCENE_STARTING_CLIP(PED_LOCATIONS ePedLocation, PED_ACTIVITIES eActivity, INT &iSumAnimDurationMS, FLOAT &fStartPhase)
	iSumAnimDurationMS = 0
	
	INT iClip = 0
	PED_ANIM_DATA pedAnimData
	
	REPEAT MAX_NUM_ANIMS_IN_ACTIVITY iClip
		RESET_PED_ANIM_DATA(pedAnimData)
		GET_PED_ANIM_DATA(ePedLocation, eActivity, pedAnimData, iClip)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimClip)
		AND NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
			iSumAnimDurationMS += FLOOR(GET_ANIM_DURATION(pedAnimData.sAnimDict, pedAnimData.sAnimClip)*1000.0)
			IF (iSumAnimDurationMS > g_clubMusicData.iCurrentMixTimeMS)
				
				INT iAnimStartingTimeMS 	= iSumAnimDurationMS-FLOOR(GET_ANIM_DURATION(pedAnimData.sAnimDict, pedAnimData.sAnimClip)*1000.0)
				INT iMillisecondsIntoClip 	= (g_clubMusicData.iCurrentMixTimeMS - iAnimStartingTimeMS)
				FLOAT fSecondsIntoClip		= TO_FLOAT(iMillisecondsIntoClip)/1000.0
				
				fStartPhase = (fSecondsIntoClip/GET_ANIM_DURATION(pedAnimData.sAnimDict, pedAnimData.sAnimClip))
				IF (fStartPhase < 0)
					fStartPhase = 0
				ELIF (fStartPhase > 1)
					fStartPhase = 1
				ENDIF
				
				RETURN iClip
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Calculate the DJ sync scene rate +/-20% to counteract any animation drifting.
///    The animation uses the running sum duration total to calculate when the next animation should play.
///    This is diffed against the current audio track time to see if they match. If they differ, the rate is calulated to slightly speed up/slow down the sync scene.
/// PARAMS:
///    iSumAnimDurationMS - The running sum total of animation durations in milliseconds. Does not include the current animation duration.
///    fAnimDurationMS - The current animation duration in milliseconds.
/// RETURNS: The DJ sync scene rate.
FUNC FLOAT GET_DJ_SYNC_SCENE_RATE(FLOAT fSumAnimDurationMS, FLOAT fAnimDurationMS)
	
	FLOAT fSumAnimDurationTotalMS = (fSumAnimDurationMS+fAnimDurationMS)
	FLOAT fCurrentMixTimeTotalMS = TO_FLOAT(g_clubMusicData.iCurrentMixTimeMS)+fAnimDurationMS
	
	FLOAT fTimeOffsetMS = fCurrentMixTimeTotalMS - fSumAnimDurationTotalMS
	FLOAT fRate = (fAnimDurationMS / (fAnimDurationMS-fTimeOffsetMS))
	
	// Cap the rate +/-20%
	IF fRate > 1.2
		fRate = 1.2
	ELIF fRate < 0.8
		fRate = 0.8
	ENDIF
	
	RETURN fRate
ENDFUNC

/// PURPOSE:
///    Sets all sync scene props visibility based on anim phase times. 
/// PARAMS:
///    Data - Ped data to query.
///    pedAnimData - Struct to populate with ped anim data.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
PROC MAINTAIN_SYNC_SCENE_PROP_VISIBILITY_FOR_DANCING_ACTIVITY(PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL)
	
	IF NOT IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
	AND NOT IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_DJ_DANCER)
		EXIT
	ENDIF
	
	INT iSyncSceneID = Data.animData.iSyncSceneID
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_NETWORK_ANIMS)
		iSyncSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(Data.animData.iSyncSceneID)
	ENDIF
	
	IF (iSyncSceneID = -1)
		EXIT
	ENDIF
	
	INT iProp
	FLOAT fPhaseTime
	FLOAT fVisibleEndPhase = -1.0
	FLOAT fVisibleStartPhase = -1.0
	REPEAT MAX_NUM_PED_PROPS iProp
		IF DOES_ENTITY_EXIST(Data.PropID[iProp])
			GET_ANIM_PROP_VISIBLE_PHASES(fVisibleStartPhase, fVisibleEndPhase, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), iProp, Data.animData.iCurrentClip, eMusicIntensity, ePedMusicIntensity, IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_DANCING_TRANSITION_ACTIVE))
			
			IF (fVisibleStartPhase != -1.0)
			AND (fVisibleEndPhase != -1.0)
				fPhaseTime = GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneID)
				IF (fPhaseTime >= fVisibleEndPhase)
					IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HIDE_PROP)
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PROP)
						SET_ENTITY_ALPHA(Data.PropID[iProp], 0, FALSE)
					ENDIF
				ELIF (fPhaseTime >= fVisibleStartPhase)
					IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SHOW_PROP)
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SHOW_PROP)
						SET_ENTITY_ALPHA(Data.PropID[iProp], 255, FALSE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    FORCE_PED_AI_AND_ANIMATION_UPDATE can only be called once a frame.
///    This function calls FORCE_PED_AI_AND_ANIMATION_UPDATE on the next available frame if it couldn't on the frame the ped animation began.
/// PARAMS:
///    LocalData - Local ped data to query.
///    Data - Ped data to query.
///    iPedID - ID of current ped.
PROC MAINTAIN_SYNC_SCENE_FORCE_ANIM_UPDATE(SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, INT iPedID)
	UNUSED_PARAMETER(iPedID)
	
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
	OR NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_RETRY_FORCE_ANIM_UPDATE)
		EXIT
	ENDIF
	
	IF NOT IS_PEDS_BIT_SET(LocalData.iBS, BS_PED_LOCAL_DATA_FORCED_ANIM_UPDATE_THIS_FRAME)
		SET_PEDS_BIT(LocalData.iBS, BS_PED_LOCAL_DATA_FORCED_ANIM_UPDATE_THIS_FRAME)
		CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_RETRY_FORCE_ANIM_UPDATE)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(Data.PedID, TRUE, TRUE)
		PRINTLN("[AM_MP_PEDS] MAINTAIN_SYNC_SCENE_FORCE_ANIM_UPDATE - Ped ID: ", iPedID, " Calling: FORCE_PED_AI_AND_ANIMATION_UPDATE")
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Loops through the ped anim data and counts the number of valid clips in a ped activity.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    eActivity - Ped activity to query.
///    pedAnimData - Struct to populate with ped anim data.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
///    eTransitionState - Used if a ped can transition between two ped transition states.
/// RETURNS: Returns the number of anim clips in a ped activity.
FUNC INT GET_MAX_ACTIVITY_ANIMS_MAIN(PED_LOCATIONS ePedLocation, PED_ACTIVITIES eActivity, PED_ANIM_DATA pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, PED_TRANSITION_STATES eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE)
	
	INT iClip
	INT iMaxClips = 0
	
	REPEAT MAX_NUM_ANIMS_IN_ACTIVITY iClip
		GET_PED_ANIM_DATA(ePedLocation, eActivity, pedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, DEFAULT, eTransitionState)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimClip)
			iMaxClips++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iMaxClips
ENDFUNC

/// PURPOSE:
///    Caches the max number of clips in an activity for later use.
///    Dancing activites need to check each time as max clips can change based on intensities and transtions.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    iMaxClips - Max clips var to cache.
///    eActivity - Ped activity to query.
///    pedAnimData - Struct to populate with ped anim data.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
///    eTransitionState - Used if a ped can transition between two ped transition states.
/// RETURNS: Returns the number of anim clips in a ped activity.
FUNC INT GET_MAX_ACTIVITY_ANIMS(PED_LOCATIONS ePedLocation, INT &iMaxClips, PED_ACTIVITIES eActivity, PED_ANIM_DATA pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bCacheMaxClips = TRUE, PED_TRANSITION_STATES eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE)
	
	INT iReturnMaxClips = 0
	
	IF (IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY) OR NOT bCacheMaxClips)
		iReturnMaxClips = GET_MAX_ACTIVITY_ANIMS_MAIN(ePedLocation, eActivity, pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition, eTransitionState)
	ELSE
		IF (iMaxClips = 0)
			iMaxClips = GET_MAX_ACTIVITY_ANIMS_MAIN(ePedLocation, eActivity, pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition, eTransitionState)
		ENDIF
		iReturnMaxClips = iMaxClips
	ENDIF
	
	RETURN iReturnMaxClips
ENDFUNC

/// PURPOSE:
///    Loops through the ped anim data and counts the number of valid looping clips in a ped activity.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    eActivity - Ped activity to query.
///    pedAnimData - Struct to populate with ped anim data.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
/// RETURNS: Returns the number of anim clips in a ped activity.
FUNC INT GET_MAX_ACTIVITY_LOOPING_ANIMS(PED_LOCATIONS ePedLocation, PED_ACTIVITIES eActivity, PED_ANIM_DATA pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	INT iClip
	INT iLoopingClips
	
	REPEAT MAX_NUM_ANIMS_IN_ACTIVITY iClip
		GET_PED_ANIM_DATA(ePedLocation, eActivity, pedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimClip)
			IF (pedAnimData.bLoopingAnim)
				iLoopingClips++
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iLoopingClips
ENDFUNC

/// PURPOSE:
///    Finds the maximum number of anim clips in an activity of a certain duration length.
///    Stores all these clips in the iClipArray array.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    eActivity - Ped activity to query.
///    iClipLength - Length of the desired clip in seconds.
///    iClipArray - Stores the clips of the desired length.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
/// RETURNS: The maximum number of anim clips in an activity of a certain duration length.
FUNC INT GET_MAX_DANCING_ANIMS_OF_CLIP_LENGTH(PED_LOCATIONS ePedLocation, PED_ACTIVITIES eActivity, INT iClipLength, INT &iClipArray[], CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	INT iClip
	INT iMaxClips = 0
	PED_ANIM_DATA pedAnimData
	
	REPEAT MAX_NUM_ANIMS_IN_ACTIVITY iClip
		RESET_PED_ANIM_DATA(pedAnimData)
		GET_PED_ANIM_DATA(ePedLocation, eActivity, pedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimClip)
			INT iAnimDuration = FLOOR(GET_ANIM_DURATION(pedAnimData.sAnimDict, pedAnimData.sAnimClip))
			
			IF (iClipLength = iAnimDuration)
				iClipArray[iMaxClips] = iClip
				iMaxClips++
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iMaxClips
ENDFUNC

/// PURPOSE:
///    Sets the dancing transition clip ID for the next transition.
///    The dancing transition clip is selected ahead of the transition so we can query its
///	   "Drop_Trans" anim tag to line up the transition animation with the music intensity change.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to query.
///    pedAnimData - Struct to populate with ped anim data.
///    eNextMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
PROC SET_DANCING_TRANSITION_CLIP_ID(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, CLUB_MUSIC_INTENSITY eNextMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL)
	
	// iDancingTransitionClipID is reset to -1 inside UPDATE_PED_DANCING_TRANSITION() after each transition
	IF (Data.dancingData.iDancingTransitionClipID = -1)
		INT iMaxClips = GET_MAX_ACTIVITY_ANIMS(ePedLocation, Data.animData.iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, eNextMusicIntensity, eMusicIntensity, TRUE)
		
		// For back to back transitions: 
		// The ped intensity should match the current intensity before transitioning. If not, use the current intensity to transition too and not the next intensity.
		IF (ePedMusicIntensity != eMusicIntensity)
			iMaxClips = GET_MAX_ACTIVITY_ANIMS(ePedLocation, Data.animData.iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, eMusicIntensity, ePedMusicIntensity, TRUE)
		ENDIF
		
		IF (iMaxClips > 1)
			Data.dancingData.iDancingTransitionClipID = GET_RANDOM_INT_IN_RANGE(0, iMaxClips)
		ELSE
			Data.dancingData.iDancingTransitionClipID = 0
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the time until the next dancing transition in seconds.
///    This is based off the global g_iTimeUntilPedDanceTransitionSecs and the "Drop_Trans" 
///    anim event so we can better line up the transition animation with the music intensity change.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to query.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
/// RETURNS: The time until the next dancing transition in seconds.
FUNC INT GET_TIME_UNTIL_DANCING_TRANSITION_SECONDS(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL)
	
	INT iTimeUntilDanceTransitionSecs = g_iTimeUntilPedDanceTransitionSecs
	
	PED_ANIM_DATA pedAnimData
	GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, Data.dancingData.iDancingTransitionClipID, eMusicIntensity, ePedMusicIntensity, TRUE)
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
	AND NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimClip)
		
		FLOAT fEndPhase			= 1.0
		FLOAT fStartPhase		= 0.0
		FLOAT fDurationSeconds 	= GET_ANIM_DURATION(pedAnimData.sAnimDict, pedAnimData.sAnimClip)
		
		IF FIND_ANIM_EVENT_PHASE(pedAnimData.sAnimDict, pedAnimData.sAnimClip, "Drop_Trans", fStartPhase, fEndPhase)
			fDurationSeconds *= fStartPhase
			iTimeUntilDanceTransitionSecs -= ROUND(fDurationSeconds)
		ENDIF
	ENDIF
	
	IF (iTimeUntilDanceTransitionSecs < 0)
		iTimeUntilDanceTransitionSecs = 0
	ENDIF
	
	RETURN iTimeUntilDanceTransitionSecs
ENDFUNC

/// PURPOSE:
///    Includes dancing transition logic to try and fit the music playing based on the time until the next music intensity change.
///    Function taken from GET_TIME_UNTIL_DANCE_TRANSITION from NightClubPedsHeader. This is essentially the Coin Change problem,
///    but implementation attempts all resulted in using nested looped. Hard coded values aren't ideal but does keeps instructions per frame low.
///    Forces the clip leangth to be either: 9 11 13 15 17
/// PARAMS:
///    iTimeUntilDanceTransitionSecs - The time until the next dancing transition in seconds.
/// RETURNS: The length of the dancing clip to play.
FUNC INT GET_INTENSITY_DANCING_ANIM_LENGTH(INT iTimeUntilDanceTransitionSecs)
	
	INT iClipLength = 0
	
	// Find the range the time remaining falls into
	INT iRange = FLOOR((iTimeUntilDanceTransitionSecs/2.0)-8.0)
	IF iRange > 5
		iRange = 5
	ENDIF
	
	IF iRange > 0
		// Get random choice in (restricted) range if there is one
		INT iRand = GET_RANDOM_INT_IN_RANGE(0, iRange)
		
		IF iRange > 1
			IF iRange > 4
				iRange = 4
			ENDIF
			iRand = GET_RANDOM_INT_IN_RANGE(1, iRange)
		ENDIF
		
		iClipLength = (iRand*2)+9
	ELSE
		// Get choice that will fit in the remaining time slot
		iClipLength = (ROUND((iTimeUntilDanceTransitionSecs+1)/2.0)*2)-1
		IF iClipLength < 9
			iClipLength = 9
		ENDIF
	ENDIF
	
	RETURN iClipLength
ENDFUNC

/// PURPOSE:
///    Includes dancing transition logic to try and fit the music playing based on the time until the next music intensity change.
///    Function taken from GET_TIME_UNTIL_DANCE_TRANSITION from NightClubPedsHeader. This is essentially the Coin Change problem,
///    but implementation attempts all resulted in using nested looped. Hard coded values aren't ideal but does keeps instructions per frame low.
///    Forces the clip leangth to be either: 6 9 10 11 12 13
/// PARAMS:
///    iTimeUntilDanceTransitionSecs - The time until the next dancing transition in seconds..
/// RETURNS: The length of the dancing clip to play.
FUNC INT GET_AMBIENT_DANCING_ANIM_LENGTH(INT iTimeUntilDanceTransitionSecs)
	
	INT iClipLength = 0
	
	// Restrict random choice range below 19s, to prevent having too little time left for the last anim to fit
	INT iRange = FLOOR(iTimeUntilDanceTransitionSecs-13.0)
	IF iRange > 6
		iRange = 6
	ENDIF
	
	IF iRange > 0
		// Get random choice in (restricted) range if there is one
		INT iRand = GET_RANDOM_INT_IN_RANGE(0, iRange)
		
		iClipLength = (iRand+8)
		IF iClipLength < 9
			iClipLength = 6
		ENDIF
	ELSE
		// Get choice that will fit in the remaining time slot
		iClipLength = iTimeUntilDanceTransitionSecs
		
		IF iClipLength < 9
			IF iClipLength = 8
				iClipLength = 9
			ELSE
				iClipLength = 6
			ENDIF
		ELIF iClipLength > 13
			iClipLength = 13
		ENDIF
	ENDIF
	
	RETURN iClipLength
ENDFUNC

/// PURPOSE:
///    Includes dancing transition logic to try and fit the music playing based on the time until the next music intensity change.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    eActivity - Ped activity to query.
///    pedAnimData - Struct to populate with ped anim data.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
/// RETURNS: Dancing activity clip to play.
FUNC INT GET_DANCING_ACTIVITY_ANIM(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	INT iTimeUntilDanceTransitionSecs = GET_TIME_UNTIL_DANCING_TRANSITION_SECONDS(ePedLocation, Data, eMusicIntensity, ePedMusicIntensity)
	
	// Logic for dancing activities when getting close to a transition change
	// Restrict the dancing clip pool to try and fit the time until the next dancing transition
	INT iClipLength = 0
	
	IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
	OR IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
	OR IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
		iClipLength = GET_INTENSITY_DANCING_ANIM_LENGTH(iTimeUntilDanceTransitionSecs)
	
	ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
		iClipLength = GET_AMBIENT_DANCING_ANIM_LENGTH(iTimeUntilDanceTransitionSecs)
	ENDIF
	
	// Get the max number of clips of the desired length
	INT iClipsArray[MAX_NUM_DANCING_ANIMS_IN_ACTIVITY]
	INT iMaxClipsOfLength = GET_MAX_DANCING_ANIMS_OF_CLIP_LENGTH(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), iClipLength, iClipsArray, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	
	// We have failed to find any clips so play random
	IF (iMaxClipsOfLength = 0)
		RETURN -1
	ENDIF
	
	// Select a random clip if we have more than one anim of the desired length
	INT iRandClipID = GET_RANDOM_INT_IN_RANGE(0, iMaxClipsOfLength)
	
	// Return the clip ID of the desired clip length version
	RETURN iClipsArray[iRandClipID]
ENDFUNC

/// PURPOSE:
///    Gets the time we should start lining up the dancing clips to sync with the dancint transition.
/// PARAMS:
///    pedAnimData - Struct to populate with ped anim data.
/// RETURNS: The time we should start lining up the dancing clips to sync with the dancint transition.
FUNC INT GET_PROCESS_SECONDS_TO_DANCING_TRANSITION(PED_ANIM_DATA pedAnimData)
	
	IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
	OR IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
	OR IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
		RETURN MAX_NUM_PROCESS_SECONDS_TO_INTENSITY_DANCING_TRANSITION 
	
	ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
		RETURN MAX_NUM_PROCESS_SECONDS_TO_AMBIENT_DANCING_TRANSITION
	ENDIF
	
	RETURN 0
ENDFUNC

/// PURPOSE:
///    Randomly selects an anim clip from a ped activity. Doesn't care if it's not different from Data.animData.iPrevClip
///    Use GET_RAND_UNIQUE_ACTIVITY_ANIM to get different random anim clips each time.
///    Includes dancing transition logic to try and fit the music playing based on the time until the next music intensity change.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to query.
///    pedAnimData - Struct to populate with ped anim data.
///    eNextMusicIntensity - For dancing peds. Gets the correct transition data based on the next music intensity.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
/// RETURNS: Random anim clip from a ped activity.
FUNC INT GET_RAND_ACTIVITY_ANIM(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, CLUB_MUSIC_INTENSITY eNextMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	// Selective clips for dancing activities
	IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_FACEDJ)
	OR IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_SINGLE_PROP)
	OR IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
	OR IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_AMBIENT)
		
		// The dancing transition clip is selected ahead of the transition so we can query its
		// "Drop_Trans" anim event to line up the transition animation with the music intensity change.
		IF (bDancingTransition)
			// Calling SET_DANCING_TRANSITION_CLIP_ID as there may be back to back transitions
			SET_DANCING_TRANSITION_CLIP_ID(ePedLocation, Data, pedAnimData, eNextMusicIntensity, eMusicIntensity, ePedMusicIntensity)
			RETURN Data.dancingData.iDancingTransitionClipID
		ELSE
			// Dancing transition logic to try and fit the music playing based on the time until the next music intensity change. Only queried if the intensity change is close.
			IF (g_iTimeUntilPedDanceTransitionSecs <= GET_PROCESS_SECONDS_TO_DANCING_TRANSITION(pedAnimData))
				
				// Set the next dancing transition clip
				SET_DANCING_TRANSITION_CLIP_ID(ePedLocation, Data, pedAnimData, eNextMusicIntensity, eMusicIntensity, ePedMusicIntensity)
				
				// Attempt to get best fitting dancing clip based on the time until the next intensity transition
				INT iAnimClip = GET_DANCING_ACTIVITY_ANIM(ePedLocation, Data, pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
				IF (iAnimClip != -1)
					RETURN iAnimClip
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	INT iMaxClips = GET_MAX_ACTIVITY_ANIMS(ePedLocation, Data.animData.iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	RETURN GET_RANDOM_INT_IN_RANGE(0, iMaxClips)
ENDFUNC

/// PURPOSE:
///    Randomly selects an anim clip from a ped activity. Will try and make it different from Data.animData.iPrevClip
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to query.
///    pedAnimData - Struct to populate with ped anim data.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
/// RETURNS: Random anim clip from a ped activity.
FUNC INT GET_RAND_UNIQUE_ACTIVITY_ANIM(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	INT iMaxClips = GET_MAX_ACTIVITY_ANIMS(ePedLocation, Data.animData.iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	INT iClip = GET_RANDOM_INT_IN_RANGE(0, iMaxClips)
	
	IF (iMaxClips > 1)
		INT iAttempts = 0
		WHILE (iClip = Data.animData.iPrevClip AND iAttempts < 10)
			iClip = GET_RANDOM_INT_IN_RANGE(0, iMaxClips)		// Ensure random clip is different from current clip
			iAttempts++											// Ten attemps at this
		ENDWHILE
	ENDIF
	
	RETURN iClip
ENDFUNC

/// PURPOSE:
///    Percentage selects a one off anim clip to play. 
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to query.
///    pedAnimData - Struct to populate with ped anim data.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
/// RETURNS: Random anim clip from a ped activity.
FUNC INT GET_RAND_PERCENTAGE_ACTIVITY_ANIM(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	INT iClip
	INT iMaxClips 		= 0
	INT iLoopingClips	= 0
	
	// Count the number of looping clips and max number of clips
	REPEAT MAX_NUM_ANIMS_IN_ACTIVITY iClip
		GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimClip)
			IF (pedAnimData.bLoopingAnim)
				iLoopingClips++
			ENDIF
			iMaxClips++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	IF (bDancingTransition)
		RETURN GET_RANDOM_INT_IN_RANGE(0, iMaxClips)
	ENDIF
	
	INT iActivityPercentage = GET_ANIM_VARIATION_CLIP_PERCENTAGE(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), ePedMusicIntensity)
	INT iRandPercentage 	= GET_RANDOM_INT_IN_RANGE(1, 101)
	INT iRandAnim			= 0
	
	// Play one off animation
	IF (iRandPercentage <= iActivityPercentage)
		iRandAnim = GET_RANDOM_INT_IN_RANGE(iLoopingClips, iMaxClips)
	ELSE
		// Play looping animation
		IF (iLoopingClips > 1)
			iRandAnim = GET_RANDOM_INT_IN_RANGE(0, iLoopingClips)
		ELSE
			iRandAnim = 0
		ENDIF
	ENDIF
	
	RETURN iRandAnim
ENDFUNC

/// PURPOSE:
///    Randomly selects which activity anim to play. It can play a transition anim based on a percentage check. Will play anims from either ped transition state.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to query.
///    pedAnimData - Struct to populate with ped anim data.
///    bPedTransition - For non dancing peds. Whether a ped should play a transition animation.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
/// RETURNS: Random anim clip from a ped activity.
FUNC INT GET_RAND_TRANSITION_ACTIVITY_ANIM(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, BOOL &bPedTransition, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	bPedTransition = FALSE
	
	INT iRandAnim = 0
	INT iMaxClips = 0
	BOOL bIncludeTransitionAnims = TRUE
	
	// Transition clip has just finished
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_PED_TRANSITION)
		CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_PED_TRANSITION)
		
		// Update ped transition state
		IF (Data.eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE)
			Data.eTransitionState = PED_TRANS_STATE_ACTIVITY_TWO
		ELIF (Data.eTransitionState = PED_TRANS_STATE_ACTIVITY_TWO)
			Data.eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE
		ENDIF
		
		// Do not play two transition clips in a row
		bIncludeTransitionAnims = FALSE
	ENDIF
	
	IF (bIncludeTransitionAnims)
		INT iTransitionPercentage 	= GET_ANIM_VARIATION_CLIP_PERCENTAGE(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), ePedMusicIntensity)
		INT iRandPercentage 		= GET_RANDOM_INT_IN_RANGE(1, 101)
	
		IF (iRandPercentage <= iTransitionPercentage)
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_PED_TRANSITION)
			bPedTransition = TRUE
			iRandAnim = 0
		ELSE
			iMaxClips = GET_MAX_ACTIVITY_ANIMS(ePedLocation, Data.animData.iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition, FALSE, Data.eTransitionState)
			iRandAnim = GET_RANDOM_INT_IN_RANGE(0, iMaxClips)
		ENDIF
	ELSE
		iMaxClips = GET_MAX_ACTIVITY_ANIMS(ePedLocation, Data.animData.iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition, FALSE, Data.eTransitionState)
		iRandAnim = GET_RANDOM_INT_IN_RANGE(0, iMaxClips)
	ENDIF
	
	RETURN iRandAnim
ENDFUNC

/// PURPOSE:
///    Partner method selects a random animation to play that may include a partner ped. 
///    Works similarly to the parent/child peds, except each ped is independant and controls which anims they play.
///    Anims are selected via percentages. Mainly plays loops and includes one off variants. These variants can be stand-alone or inlcude another ped (partner).
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All ped data to query.
///    Data - Ped data to query.
///    pedAnimData - Struct to populate with ped anim data.
///    iPedID - ID of current ped.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
/// RETURNS: Random anim clip from a ped activity.
FUNC INT GET_RAND_PARTNER_ACTIVITY_ANIM(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, INT iPedID, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	INT iClip
	INT iMaxClips 		= 0
	INT iLoopingClips	= 0
	INT iPartnerClips	= 0
	
	// Count the number of looping clips and max number of clips
	REPEAT MAX_NUM_ANIMS_IN_ACTIVITY iClip
		GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimClip)
			IF (pedAnimData.bLoopingAnim)
				iLoopingClips++
			ENDIF
			IF (pedAnimData.bPartnerAnim)
				iPartnerClips++
			ENDIF
			iMaxClips++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	IF (bDancingTransition)
		RETURN GET_RANDOM_INT_IN_RANGE(0, iMaxClips)
	ENDIF
	
	INT iActivityPercentage = GET_ANIM_VARIATION_CLIP_PERCENTAGE(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), ePedMusicIntensity)
	INT iRandPercentage 	= GET_RANDOM_INT_IN_RANGE(1, 101)
	INT iRandAnim			= 0
	
	// Play one off animation
	IF (iRandPercentage <= iActivityPercentage)
		
		BOOL bFoundClip = FALSE
		
		// Select random one off clip
		iRandAnim = GET_RANDOM_INT_IN_RANGE(iLoopingClips, iMaxClips)
		GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, iRandAnim, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		
		// Does animation need a partner ped
		IF (pedAnimData.bPartnerAnim)
			
			// Network peds
			IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_NETWORK_ANIMS)
				IF (ServerBD.iParentPedID[iPedID] != -1 AND pedAnimData.iPartnerPed != -1)
					
					INT iPartnerPedID = ServerBD.ParentPedData[ServerBD.iParentPedID[iPedID]].iChildID[pedAnimData.iPartnerPed]
					IF (iPartnerPedID != -1 AND IS_ENTITY_ALIVE(ServerBD.NetworkPedData[iPartnerPedID].Data.PedID))
						
						// Check data for current partner ped animation
						GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, ServerBD.NetworkPedData[iPartnerPedID].Data.iActivity), pedAnimData, ServerBD.NetworkPedData[iPartnerPedID].Data.animData.iCurrentClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
						
						// Check if the partner ped is playing a looping animation
						IF (pedAnimData.bLoopingAnim)
							// The current clip (iRandAnim) is fine to play
							bFoundClip = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				// Local peds
				// Check if partner ped is available (e.g. they are playing a looping animation and can blend out of it)
				IF (LocalData.iParentPedID[iPedID] != -1 AND pedAnimData.iPartnerPed != -1)
					
					INT iPartnerPedID = LocalData.ParentPedData[LocalData.iParentPedID[iPedID]].iChildID[pedAnimData.iPartnerPed]
					IF (iPartnerPedID != -1 AND IS_ENTITY_ALIVE(LocalData.PedData[iPartnerPedID].PedID))
						
						// Check data for current partner ped animation
						GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[iPartnerPedID].iActivity), pedAnimData, LocalData.PedData[iPartnerPedID].animData.iCurrentClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
						
						// Check if the partner ped is playing a looping animation
						IF (pedAnimData.bLoopingAnim)
							// The current clip (iRandAnim) is fine to play
							bFoundClip = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF (NOT bFoundClip)
			// Select a stand-alone one off clip
			iRandAnim = GET_RANDOM_INT_IN_RANGE(iLoopingClips, iMaxClips-iPartnerClips)
		ENDIF
	ELSE
		// Play looping animation
		IF (iLoopingClips > 1)
			iRandAnim = GET_RANDOM_INT_IN_RANGE(0, iLoopingClips)
		ELSE
			iRandAnim = 0
		ENDIF
	ENDIF
	
	RETURN iRandAnim
ENDFUNC

/// PURPOSE:
///    Checks if a convo waiting to be played requires a linked animation.
///    If so this animation is played. Otherwise a random looping animation is played.
///    This is only for networked peds that have animations and dialogue synced by host.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All ped data to query.
///    Data - Ped data to query.
///    pedAnimData - Struct to populate with ped anim data.
///    iPedID - ID of current ped.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
///    bNetworkPed - Whether the ped is networked or not.
/// RETURNS: Random anim clip from a ped activity.
FUNC INT GET_DIALOGUE_ACTIVITY_ANIM(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, INT iPedID, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bNetworkPed = FALSE)
	UNUSED_PARAMETER(ServerBD)
	
	// Cache max and looping clips to save calculating each time anim triggered
	IF (Data.animData.iMaxClips = 0)
		GET_MAX_ACTIVITY_ANIMS(ePedLocation, Data.animData.iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	ENDIF
	IF (Data.animData.iLoopingClips = 0)
		Data.animData.iLoopingClips = GET_MAX_ACTIVITY_LOOPING_ANIMS(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	ENDIF
	
	// Check if a convo needs to be triggered with an animation
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CONVO_REQUIRES_ANIMATION)
		
		INT iClip
		BOOL bFoundConvoAnimClip = FALSE
		REPEAT MAX_NUM_ANIMS_IN_ACTIVITY iClip
			GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimClip)
				IF (bNetworkPed)
					IF (pedAnimData.ePedSpeech = LocalData.eNetworkPedSpeech)
						bFoundConvoAnimClip = TRUE
						BREAKLOOP
					ENDIF
				ELSE
					IF (pedAnimData.ePedSpeech = LocalData.eLocalPedSpeech)
						bFoundConvoAnimClip = TRUE
						BREAKLOOP
					ENDIF
				ENDIF
			ELSE
				BREAKLOOP
			ENDIF
		ENDREPEAT
		
		// Return the convo anim clip if the ped is free to play dialogue
		IF (bFoundConvoAnimClip)
			IF (bNetworkPed)
				IF (LocalData.iNetworkSpeechPed != -1)
				AND (GET_PED_NETWORK_ARRAY_ID(LocalData.iNetworkSpeechPed) = iPedID)
					IF (LocalData.iNetworkSpeechPedID[GET_PED_NETWORK_ARRAY_ID(LocalData.iNetworkSpeechPed)] != -1)
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(ServerBD.NetworkPedData[iPedID].niPedID)
						AND CAN_SEND_CONTROLLER_SPEECH_EVENT(Data, LocalData.NetworkSpeechData[LocalData.iNetworkSpeechPedID[GET_PED_NETWORK_ARRAY_ID(LocalData.iNetworkSpeechPed)]], GET_PED_INDEX_FROM_ENTITY_INDEX(NETWORK_GET_ENTITY_FROM_NETWORK_ID(ServerBD.NetworkPedData[iPedID].niPedID)), TRUE)
							RETURN iClip
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF (LocalData.iLocalSpeechPed != -1)
				AND (LocalData.iLocalSpeechPed = iPedID)
					IF (LocalData.iLocalSpeechPedID[LocalData.iLocalSpeechPed] != -1)
						IF CAN_SEND_CONTROLLER_SPEECH_EVENT(Data, LocalData.LocalSpeechData[LocalData.iLocalSpeechPedID[LocalData.iLocalSpeechPed]], Data.PedID)
							RETURN iClip
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	// Ped is not free to play dialogue
	CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_CONVO_REQUIRES_ANIMATION)
	
	// Return a random looping clip
	RETURN GET_RANDOM_INT_IN_RANGE(0, Data.animData.iLoopingClips)
ENDFUNC

/// PURPOSE:
///    Excludes any animations tagged as partner anims when selecting clips.
///    Based on percentages. Plays the base anim if the percentage check fails. Variation if it succeeds.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All ped data to query.
///    Data - Ped data to query.
///    pedAnimData - Struct to populate with ped anim data.
///    iPedID - ID of current ped.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
/// RETURNS: Random anim clip from a ped activity.
FUNC INT GET_EXCLUDE_PARTNER_ACTIVITY_ANIM(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	INT iClip
	INT iMaxClips = 0
	INT iLoopingClips = 0
	INT iPartnerClips = 0
	
	REPEAT MAX_NUM_ANIMS_IN_ACTIVITY iClip
		GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		
		IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimClip)
			IF (pedAnimData.bLoopingAnim)
				iLoopingClips++
			ENDIF
			IF (pedAnimData.bPartnerAnim)
				iPartnerClips++
			ENDIF
			iMaxClips++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	IF (bDancingTransition)
		RETURN GET_RANDOM_INT_IN_RANGE(0, iMaxClips)
	ENDIF
	
	INT iActivityPercentage = GET_ANIM_VARIATION_CLIP_PERCENTAGE(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), ePedMusicIntensity)
	INT iRandPercentage 	= GET_RANDOM_INT_IN_RANGE(1, 101)
	INT iRandAnim			= 0
	
	// Play one off animation
	IF (iRandPercentage <= iActivityPercentage)
		iRandAnim = GET_RANDOM_INT_IN_RANGE(iLoopingClips, iMaxClips-iPartnerClips)
	ELSE
		// Play looping animation
		IF (iLoopingClips > 1)
			iRandAnim = GET_RANDOM_INT_IN_RANGE(0, iLoopingClips)
		ELSE
			iRandAnim = 0
		ENDIF
	ENDIF
	
	RETURN iRandAnim
ENDFUNC

/// PURPOSE:
///    Loops over the anim dictionaries in a peds activity to request and loads them.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to query.
///    pedAnimData - Struct to populate with ped anim data.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
/// RETURNS: TRUE when all anim dictionaries in a peds activity have been requested and loaded.
FUNC BOOL REQUEST_AND_LOAD_ALL_ACTIVITY_ANIMATION_DICTS(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	INT iClip
	INT iAnimDictsLoaded = 0
	INT iMaxClips = GET_MAX_ACTIVITY_ANIMS(ePedLocation, Data.animData.iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	
	REPEAT iMaxClips iClip
		GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		
		IF REQUEST_AND_LOAD_ANIMATION_DICT(pedAnimData.sAnimDict)
			iAnimDictsLoaded++
		ENDIF
	ENDREPEAT
	
	RETURN (iAnimDictsLoaded = iMaxClips)
ENDFUNC

#IF IS_DEBUG_BUILD
PROC STORE_SEQUENCED_PED_CLIP_DETAIL(PEDS_DATA &Data, INT iClip, PED_ANIM_DATA pedAnimData)
	INT i
	FOR i = 0 TO 2
		IF g_DebugAnimSeqDetail.pedActivity[i] = Data.iActivity
			g_DebugAnimSeqDetail.iClip[i] = iClip
			g_DebugAnimSeqDetail.sClipName[i] = pedAnimData.sAnimClip
			g_DebugAnimSeqDetail.sDictName[i] = pedAnimData.sAnimDict
			g_DebugAnimSeqDetail.iSyncSceneID[i] = Data.animData.iSyncSceneID
		ENDIF
	ENDFOR
ENDPROC
#ENDIF

/// PURPOSE:
///    Add all the local ped props associated with the ped activity to the synced scene.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to query.
///    pedAnimData - Struct to populate with ped anim data.
///    iSyncSceneID - Sync scene ID to add props too.
///    iClip - The prop clip to play. Should match the ped clip to play.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - The current music intensity. Used for dancing peds.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
///    bHeeledPed - Wheather the ped is heeled or not. Determined via ped editor widgets (bit set).
PROC ADD_PED_LOCAL_PROPS_TO_SYNCED_SCENE(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, INT iSyncSceneID, INT iClip, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bHeeledPed = FALSE)
	
	INT iProp
	PED_PROP_ANIM_DATA pedPropAnimData
	
	REPEAT MAX_NUM_PED_PROPS iProp
		IF DOES_ENTITY_EXIST(Data.PropID[iProp])
		AND DOES_ENTITY_HAVE_DRAWABLE(Data.PropID[iProp])
			GET_PED_PROP_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedPropAnimData, iProp, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, bHeeledPed)

			IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_HIDE_PROPS_WITHOUT_ANIMATION)
				IF NOT IS_STRING_NULL_OR_EMPTY(pedPropAnimData.sPropAnimDict)
				AND NOT IS_STRING_NULL_OR_EMPTY(pedPropAnimData.sPropAnimClip)
				AND NOT IS_ENTITY_VISIBLE(Data.PropID[iProp])
					SET_ENTITY_VISIBLE(Data.PropID[iProp], TRUE)
				ENDIF
				
				IF IS_STRING_NULL_OR_EMPTY(pedPropAnimData.sPropAnimDict)
				OR IS_STRING_NULL_OR_EMPTY(pedPropAnimData.sPropAnimClip)
				AND IS_ENTITY_VISIBLE(Data.PropID[iProp])
					SET_ENTITY_VISIBLE(Data.PropID[iProp], FALSE)
				ENDIF
			ENDIF
			
			PRINTLN("SHOULD BE PLAYING CLIP: ", pedPropAnimData.sPropAnimClip)
			IF NOT IS_STRING_NULL_OR_EMPTY(pedPropAnimData.sPropAnimDict)
			AND NOT IS_STRING_NULL_OR_EMPTY(pedPropAnimData.sPropAnimClip)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(Data.PropID[iProp], iSyncSceneID, pedPropAnimData.sPropAnimClip, pedPropAnimData.sPropAnimDict, pedPropAnimData.fBlendInDelta, pedPropAnimData.fBlendOutDelta, pedPropAnimData.iFlags, pedPropAnimData.fMoverBlendInDelta)
			ENDIF
			
			IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			OR IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_DJ_DANCER)
				SET_ENTITY_ALPHA(Data.PropID[iProp], 0, FALSE)
			ENDIF
			
			IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(Data.PropID[iProp])
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	// Clear prop visible bits for new anim
	CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_SHOW_PROP)
	CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PROP)
	
ENDPROC

/// PURPOSE:
///    Add all the networked ped props associated with the ped activity to the synced scene.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to query.
///    pedAnimData - Struct to populate with ped anim data.
///    iSyncSceneID - Sync scene ID to add props too.
///    iClip - The prop clip to play. Should match the ped clip to play.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - The current music intensity. Used for dancing peds.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
///    bHeeledPed - Wheather the ped is heeled or not. Determined via ped editor widgets (bit set).
PROC ADD_PED_NETWORKED_PROPS_TO_SYNCED_SCENE(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, INT iSyncSceneID, INT iClip, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bHeeledPed = FALSE)
	
	INT iProp
	PED_PROP_ANIM_DATA pedPropAnimData
	
	REPEAT MAX_NUM_PED_PROPS iProp
		IF DOES_ENTITY_EXIST(Data.PropID[iProp])
			GET_PED_PROP_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedPropAnimData, iProp, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, bHeeledPed)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(pedPropAnimData.sPropAnimDict)
			AND NOT IS_STRING_NULL_OR_EMPTY(pedPropAnimData.sPropAnimClip)
				NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(Data.PropID[iProp], iSyncSceneID, pedPropAnimData.sPropAnimDict, pedPropAnimData.sPropAnimClip, pedPropAnimData.fBlendInDelta, pedPropAnimData.fBlendOutDelta, pedAnimData.syncSceneFlags)
			ENDIF
			
			IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_GROUPS)
			OR IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY_DJ_DANCER)
				SET_ENTITY_ALPHA(Data.PropID[iProp], 0, FALSE)
			ENDIF
			
			IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(Data.PropID[iProp])
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	// Clear prop visible bits for new anim
	CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_SHOW_PROP)
	CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PROP)
	
ENDPROC

/// PURPOSE:
///    Adds a partner ped to the local synced scene.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    LocalData - All ped data to query.
///    Data - Current ped data to query.
///    pedAnimData - Anim data to query.
///    iPedID - ID of current ped.
///    iClip - The clip to play.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
PROC ADD_PARTNER_PED_TO_LOCAL_SYNCED_SCENE(PED_LOCATIONS ePedLocation, SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, INT iPedID, INT iClip, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PARTNER_PED)
	OR (LocalData.iParentPedID[iPedID] = -1)  // Partner uses parent ped vars
	OR (pedAnimData.iPartnerPed = -1)
		EXIT
	ENDIF
	
	INT iPartnerPedID = LocalData.ParentPedData[LocalData.iParentPedID[iPedID]].iChildID[pedAnimData.iPartnerPed]
	
	IF (iPartnerPedID != -1)
		PED_ANIM_DATA partnerPedAnimData
		GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[iPartnerPedID].iActivity), partnerPedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		
		IF DOES_ENTITY_EXIST(LocalData.PedData[iPartnerPedID].PedID)
		AND NOT IS_STRING_NULL_OR_EMPTY(partnerPedAnimData.sAnimDict)
		AND NOT IS_STRING_NULL_OR_EMPTY(partnerPedAnimData.sAnimClip)
		AND IS_ENTITY_A_PED(LocalData.PedData[iPartnerPedID].PedID)
			TASK_SYNCHRONIZED_SCENE(LocalData.PedData[iPartnerPedID].PedID, Data.animData.iSyncSceneID, partnerPedAnimData.sAnimDict, partnerPedAnimData.sAnimClip, partnerPedAnimData.fBlendInDelta, partnerPedAnimData.fBlendOutDelta, partnerPedAnimData.syncSceneFlags, partnerPedAnimData.ragdollFlags, partnerPedAnimData.fMoverBlendInDelta, partnerPedAnimData.ikFlags)
			LocalData.PedData[iPartnerPedID].animData.iSyncSceneID = Data.animData.iSyncSceneID
			LocalData.PedData[iPartnerPedID].animData.iCurrentClip = iClip
			
			IF NOT IS_STRING_NULL_OR_EMPTY(partnerPedAnimData.sFacialAnimClip)
				IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					SET_FACIAL_CLIPSET(LocalData.PedData[iPartnerPedID].PedID, pedAnimData.sMoodAnimDict)
					SET_FACIAL_IDLE_ANIM_OVERRIDE(LocalData.PedData[iPartnerPedID].PedID, pedAnimData.sFacialAnimClip)
				ELIF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_FACIAL_ANIM_IN_SEPARATE_DICTIONARY)
					PLAY_FACIAL_ANIM(LocalData.PedData[iPartnerPedID].PedID, partnerPedAnimData.sFacialAnimClip, partnerPedAnimData.sMoodAnimDict)
				ELSE
					PLAY_FACIAL_ANIM(LocalData.PedData[iPartnerPedID].PedID, partnerPedAnimData.sFacialAnimClip, partnerPedAnimData.sAnimDict)
				ENDIF	
			ENDIF
			
			RESET_LOCAL_PED_VFX_DATA(LocalData, partnerPedAnimData, iPartnerPedID)
		ENDIF
		
		// Add partner props to sync scene
		ADD_PED_LOCAL_PROPS_TO_SYNCED_SCENE(ePedLocation, LocalData.PedData[iPartnerPedID], partnerPedAnimData, Data.animData.iSyncSceneID, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, IS_PEDS_BIT_SET(LocalData.PedData[iPartnerPedID].iBS, BS_PED_DATA_HEELED_PED))
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Adds a partner ped to the local synced scene.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    Data - Current ped data to query.
///    pedAnimData - Anim data to query.
///    iPedID - ID of current ped.
///    iClip - The clip to play.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
PROC ADD_PARTNER_PED_TO_NETWORK_SYNCED_SCENE(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, INT iPedID, INT iClip, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PARTNER_PED)
	OR (ServerBD.iParentPedID[iPedID] = -1)  // Partner uses parent ped vars
	OR (pedAnimData.iPartnerPed = -1)
		EXIT
	ENDIF
	
	INT iPartnerPedID = ServerBD.ParentPedData[ServerBD.iParentPedID[iPedID]].iChildID[pedAnimData.iPartnerPed]
	
	IF (iPartnerPedID != -1)
		PED_ANIM_DATA partnerPedAnimData
		GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, ServerBD.NetworkPedData[iPartnerPedID].Data.iActivity), partnerPedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		
		IF DOES_ENTITY_EXIST(ServerBD.NetworkPedData[iPartnerPedID].Data.PedID)
		AND NOT IS_STRING_NULL_OR_EMPTY(partnerPedAnimData.sAnimDict)
		AND NOT IS_STRING_NULL_OR_EMPTY(partnerPedAnimData.sAnimClip)
		AND IS_ENTITY_A_PED(ServerBD.NetworkPedData[iPartnerPedID].Data.PedID)
			NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(ServerBD.NetworkPedData[iPartnerPedID].Data.PedID, Data.animData.iSyncSceneID, partnerPedAnimData.sAnimDict, partnerPedAnimData.sAnimClip, partnerPedAnimData.fBlendInDelta, partnerPedAnimData.fBlendOutDelta, partnerPedAnimData.syncSceneFlags, partnerPedAnimData.ragdollFlags, partnerPedAnimData.fMoverBlendInDelta, partnerPedAnimData.ikFlags)
			ServerBD.NetworkPedData[iPartnerPedID].Data.animData.iSyncSceneID = Data.animData.iSyncSceneID
			ServerBD.NetworkPedData[iPartnerPedID].Data.animData.iCurrentClip = iClip
			
			IF NOT IS_STRING_NULL_OR_EMPTY(partnerPedAnimData.sFacialAnimClip)
				IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
					SET_FACIAL_CLIPSET(ServerBD.NetworkPedData[iPartnerPedID].Data.PedID, pedAnimData.sMoodAnimDict)
					SET_FACIAL_IDLE_ANIM_OVERRIDE(ServerBD.NetworkPedData[iPartnerPedID].Data.PedID, pedAnimData.sFacialAnimClip)
				ELIF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_FACIAL_ANIM_IN_SEPARATE_DICTIONARY)
					PLAY_FACIAL_ANIM(ServerBD.NetworkPedData[iPartnerPedID].Data.PedID, partnerPedAnimData.sFacialAnimClip, partnerPedAnimData.sMoodAnimDict)
				ELSE
					PLAY_FACIAL_ANIM(ServerBD.NetworkPedData[iPartnerPedID].Data.PedID, partnerPedAnimData.sFacialAnimClip, partnerPedAnimData.sAnimDict)
				ENDIF	
			ENDIF
			
			RESET_NETWORK_PED_VFX_DATA(ServerBD, partnerPedAnimData, iPartnerPedID)
		ENDIF
		
		// Add partner props to sync scene
		ADD_PED_NETWORKED_PROPS_TO_SYNCED_SCENE(ePedLocation, ServerBD.NetworkPedData[iPartnerPedID].Data, partnerPedAnimData, Data.animData.iSyncSceneID, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iPartnerPedID].Data.iBS, BS_PED_DATA_HEELED_PED))
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Adds any child peds to the parents local synced scene.
///    If the parent is a local ped the children have to be local too.
///    Networked peds cannot play local sync scenes and vice versa.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    LocalData - All ped data to query. Needed to query child peds.
///    ParentData - Parent ped data to query.
///    iPedID - ID of current ped.
///    iClip - The clip to play.
///    bChildPedUsed - Returns if child ped is being used.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
///    bDJInteraction - Sets flags if DJ dancer interaction.
PROC ADD_CHILD_PEDS_TO_LOCAL_SYNCED_SCENE(PED_LOCATIONS ePedLocation, SCRIPT_PED_DATA &LocalData, PEDS_DATA &ParentData, INT iPedID, INT iClip, BOOL &bChildPedUsed, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bDJInteraction = FALSE, INT iChildPed = -1)
	
	IF NOT IS_PEDS_BIT_SET(ParentData.iBS, BS_PED_DATA_PARENT_PED)
	OR IS_PEDS_BIT_SET(ParentData.iBS, BS_PED_DATA_PARTNER_PED)
	OR (LocalData.iParentPedID[iPedID] = -1)
		EXIT
	ENDIF
	
	IF IS_PEDS_BIT_SET(ParentData.iBS, BS_PED_DATA_DJ_DANCER_INTERACTION)
	AND NOT bDJInteraction
		EXIT
	ENDIF
	
	IF (bDJInteraction)
		bDancingTransition = FALSE
	ENDIF
	
	PED_ANIM_DATA childPedAnimData
	
	INT iChild
	INT iChildPedID
	REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChild
		iChildPedID = LocalData.ParentPedData[LocalData.iParentPedID[iPedID]].iChildID[iChild]
		IF (iChildPedID != -1)
			IF ((NOT bDJInteraction) OR (bDJInteraction AND iChildPedID = iChildPed))
				RESET_PED_ANIM_DATA(childPedAnimData)
				GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[iChildPedID].iActivity), childPedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
				
				IF IS_ENTITY_ALIVE(LocalData.PedData[iChildPedID].PedID)
				AND NOT IS_STRING_NULL_OR_EMPTY(childPedAnimData.sAnimDict)
				AND NOT IS_STRING_NULL_OR_EMPTY(childPedAnimData.sAnimClip)
				AND IS_ENTITY_A_PED(LocalData.PedData[iChildPedID].PedID)
					TASK_SYNCHRONIZED_SCENE(LocalData.PedData[iChildPedID].PedID, ParentData.animData.iSyncSceneID, childPedAnimData.sAnimDict, childPedAnimData.sAnimClip, childPedAnimData.fBlendInDelta, childPedAnimData.fBlendOutDelta, childPedAnimData.syncSceneFlags, childPedAnimData.ragdollFlags, childPedAnimData.fMoverBlendInDelta, childPedAnimData.ikFlags)
					LocalData.PedData[iChildPedID].animData.iSyncSceneID = ParentData.animData.iSyncSceneID
					LocalData.PedData[iChildPedID].animData.iCurrentClip = iClip
					bChildPedUsed = TRUE
					
					IF (bDJInteraction)
						SET_PEDS_BIT(LocalData.PedData[iChildPedID].iBS, BS_PED_DATA_REALLY_SLOW_BLEND_IN)
					ENDIF
					
					// Add child props to sync scene
					IF (childPedAnimData.bAddChildPedProps)
						ADD_PED_LOCAL_PROPS_TO_SYNCED_SCENE(ePedLocation, LocalData.PedData[iChildPedID], childPedAnimData, ParentData.animData.iSyncSceneID, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, IS_PEDS_BIT_SET(LocalData.PedData[iChildPedID].iBS, BS_PED_DATA_HEELED_PED))
					ENDIF
					
					RESET_LOCAL_PED_VFX_DATA(LocalData, childPedAnimData, iChildPedID)
				ENDIF
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Adds any child peds to the parents network synced scene.
///    If the parent is a networked ped the children have to be networked too.
///    Local peds cannot play network sync scenes and vice versa.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    ParentData - Parent ped data to query.
///    iPedID - ID of current ped.
///    iClip - The clip to play.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
///    bDJInteraction - Sets flags if DJ dancer interaction.
PROC ADD_CHILD_PEDS_TO_NETWORK_SYNCED_SCENE(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &ParentData, INT iPedID, INT iClip, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bDJInteraction = FALSE)
	
	IF NOT IS_PEDS_BIT_SET(ParentData.iBS, BS_PED_DATA_PARENT_PED)
	OR IS_PEDS_BIT_SET(ParentData.iBS, BS_PED_DATA_PARTNER_PED)
	OR (ServerBD.iParentPedID[iPedID] = -1)
		EXIT
	ENDIF
	
	IF (bDJInteraction)
		bDancingTransition = FALSE
	ENDIF
	
	PED_ANIM_DATA childPedAnimData
	
	INT iChild
	INT iChildPedID
	REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChild
		iChildPedID = ServerBD.ParentPedData[ServerBD.iParentPedID[iPedID]].iChildID[iChild]
		IF (iChildPedID != -1)
			RESET_PED_ANIM_DATA(childPedAnimData)
			GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, ServerBD.NetworkPedData[iChildPedID].Data.iActivity), childPedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			
			IF (IS_ENTITY_ALIVE(ServerBD.NetworkPedData[iChildPedID].Data.PedID) OR IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iChildPedID].Data.iBS, BS_PED_DATA_PROPS_ONLY_NO_PED))
			AND NOT IS_STRING_NULL_OR_EMPTY(childPedAnimData.sAnimDict)
			AND NOT IS_STRING_NULL_OR_EMPTY(childPedAnimData.sAnimClip)
			AND (IS_ENTITY_A_PED(ServerBD.NetworkPedData[iChildPedID].Data.PedID) OR IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iChildPedID].Data.iBS, BS_PED_DATA_PROPS_ONLY_NO_PED))
				IF NOT IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iChildPedID].Data.iBS, BS_PED_DATA_PROPS_ONLY_NO_PED)
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(ServerBD.NetworkPedData[iChildPedID].Data.PedID, ParentData.animData.iSyncSceneID, childPedAnimData.sAnimDict, childPedAnimData.sAnimClip, childPedAnimData.fBlendInDelta, childPedAnimData.fBlendOutDelta, childPedAnimData.syncSceneFlags, childPedAnimData.ragdollFlags, childPedAnimData.fMoverBlendInDelta, childPedAnimData.ikFlags)
				ENDIF
				ServerBD.NetworkPedData[iChildPedID].Data.animData.iSyncSceneID = ParentData.animData.iSyncSceneID
				ServerBD.NetworkPedData[iChildPedID].Data.animData.iCurrentClip = iClip
				
				IF (bDJInteraction)
					SET_PEDS_BIT(ServerBD.NetworkPedData[iChildPedID].Data.iBS, BS_PED_DATA_REALLY_SLOW_BLEND_IN)
				ENDIF
				
				// Add child props to sync scene
				IF (childPedAnimData.bAddChildPedProps)
				OR IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iChildPedID].Data.iBS, BS_PED_DATA_PROPS_ONLY_NO_PED)
					ADD_PED_NETWORKED_PROPS_TO_SYNCED_SCENE(ePedLocation, ServerBD.NetworkPedData[iChildPedID].Data, childPedAnimData, ParentData.animData.iSyncSceneID, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iChildPedID].Data.iBS, BS_PED_DATA_HEELED_PED))
				ENDIF
				
				RESET_NETWORK_PED_VFX_DATA(ServerBD, childPedAnimData, iChildPedID)
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
ENDPROC

/// PURPOSE:
///    Checks if all the local child peds of a parent are alive.
///    Sets the bit BS_PED_DATA_PARENT_CHILD_PEDS_ALIVE if so.
/// PARAMS:
///    LocalData - All ped data to query. Needed to query child peds.
///    Data - Ped data to query. This will be the parent ped if child peds exist.
///    iPedID - ID of current ped.
/// RETURNS: TRUE if all a parents child peds are alive.
FUNC BOOL ARE_LOCAL_CHILD_PEDS_ALIVE(SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, INT iPedID)
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PARENT_CHILD_PEDS_ALIVE)
		RETURN TRUE
	ENDIF
	
	INT iChild
	INT iChildPedID
	INT iTotalChildPeds
	INT iAliveChildPeds
	REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChild
		iChildPedID = LocalData.ParentPedData[LocalData.iParentPedID[iPedID]].iChildID[iChild]
		IF (iChildPedID != -1)
			iTotalChildPeds++
			IF IS_ENTITY_ALIVE(LocalData.PedData[iChildPedID].PedID)
				iAliveChildPeds++
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	IF (iAliveChildPeds = iTotalChildPeds)
		SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_CHILD_PEDS_ALIVE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if all the networked child peds of a parent are alive.
///    Sets the bit BS_PED_DATA_PARENT_CHILD_PEDS_ALIVE if so.
/// PARAMS:
///    ServerBD - Server ped data to query.
///    Data - Ped data to query. This will be the parent ped if child peds exist.
///    iPedID - ID of current ped.
/// RETURNS: TRUE if all a parents child peds are alive.
FUNC BOOL ARE_NETWORKED_CHILD_PEDS_ALIVE(SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, INT iPedID)
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PARENT_CHILD_PEDS_ALIVE)
		RETURN TRUE
	ENDIF
	
	INT iChild
	INT iChildPedID
	INT iTotalChildPeds
	INT iAliveChildPeds
	REPEAT MAX_NUM_TOTAL_CHILD_PEDS iChild
		iChildPedID = ServerBD.ParentPedData[ServerBD.iParentPedID[iPedID]].iChildID[iChild]
		IF (iChildPedID != -1)
			iTotalChildPeds++
			IF IS_ENTITY_ALIVE(ServerBD.NetworkPedData[iChildPedID].Data.PedID)
			OR IS_PEDS_BIT_SET(ServerBD.NetworkPedData[iChildPedID].Data.iBS, BS_PED_DATA_PROPS_ONLY_NO_PED)
				iAliveChildPeds++
			ENDIF
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	IF (iAliveChildPeds = iTotalChildPeds)
		SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PARENT_CHILD_PEDS_ALIVE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Converts the DJ clip ID into the correct clip for the DJ dancer.
///    If this function returns -1 then the current DJ clip is not a DJ dancer interaction clip.
/// PARAMS:
///    pedAnimData - Ped anim data to query.
///    eActivity - The DJ activity to query.
///    ePedMusicIntensity - Ped intensity to query.
/// RETURNS: 
FUNC INT GET_LOCAL_DJ_DANCER_INTERACTION_ANIM_CLIP(PED_LOCATIONS ePedLocation, SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, INT& iChildPed, INT iPedID, INT iClipHash, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL)
	
	INT iChildPedSlot
	INT iDancerClipHash = GET_DJ_DANCER_INTERACTION_CLIP_HASH(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), iClipHash, iChildPedSlot)
	
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PARENT_PED)
	OR (LocalData.iParentPedID[iPedID] = -1)
	OR (iDancerClipHash = -1)
		RETURN -1
	ENDIF
	
	INT iClip = -1
	INT iChildPedID = -1
	BOOL bFoundClip = FALSE
	PED_ANIM_DATA childPedAnimData
	
	iChildPedID = LocalData.ParentPedData[LocalData.iParentPedID[iPedID]].iChildID[iChildPedSlot]
	IF (iChildPedID != -1)
		iChildPed = iChildPedID
		REPEAT MAX_NUM_ANIMS_IN_ACTIVITY iClip
			RESET_PED_ANIM_DATA(childPedAnimData)
			GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, LocalData.PedData[iChildPedID].iActivity), childPedAnimData, iClip, ePedMusicIntensity, ePedMusicIntensity, FALSE)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(childPedAnimData.sAnimClip)
				IF (GET_HASH_KEY(childPedAnimData.sAnimClip) = iDancerClipHash)
					bFoundClip = TRUE
					BREAKLOOP
				ENDIF
			ELSE
				BREAKLOOP
			ENDIF
		ENDREPEAT
	ENDIF
							
	IF (bFoundClip)
		RETURN iClip
	ENDIF
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Converts the DJ clip ID into the correct clip for the DJ dancer.
///    If this function returns -1 then the current DJ clip is not a DJ dancer interaction clip.
/// PARAMS:
///    pedAnimData - Ped anim data to query.
///    eActivity - The DJ activity to query.
///    ePedMusicIntensity - Ped intensity to query.
/// RETURNS: 
FUNC INT GET_NETWORK_DJ_DANCER_INTERACTION_ANIM_CLIP(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, INT iPedID, INT iClipHash, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL)
	
	INT iChildPed = 0
	INT iDancerClipHash = GET_DJ_DANCER_INTERACTION_CLIP_HASH(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), iClipHash, iChildPed)
	
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PARENT_PED)
	OR (ServerBD.iParentPedID[iPedID] = -1)
	OR (iDancerClipHash = -1)
		RETURN -1
	ENDIF
	
	INT iClip = -1
	INT iChildPedID = -1
	BOOL bFoundClip = FALSE
	PED_ANIM_DATA childPedAnimData
	
	iChildPedID = ServerBD.ParentPedData[ServerBD.iParentPedID[iPedID]].iChildID[iChildPed]
	IF (iChildPedID != -1)
		REPEAT MAX_NUM_ANIMS_IN_ACTIVITY iClip
			RESET_PED_ANIM_DATA(childPedAnimData)
			GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, ServerBD.NetworkPedData[iChildPedID].Data.iActivity), childPedAnimData, iClip, ePedMusicIntensity, ePedMusicIntensity, FALSE)
			
			IF NOT IS_STRING_NULL_OR_EMPTY(childPedAnimData.sAnimClip)
				IF (GET_HASH_KEY(childPedAnimData.sAnimClip) = iDancerClipHash)
					bFoundClip = TRUE
					BREAKLOOP
				ENDIF
			ELSE
				BREAKLOOP
			ENDIF
		ENDREPEAT
	ENDIF
							
	IF (bFoundClip)
		RETURN iClip
	ENDIF
	
	RETURN -1
ENDFUNC

/// PURPOSE:
///    Creates a local synced scene. To be used within PLAY_PED_SYNCED_SCENE()
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    LocalData - All ped data to query. Needed to query child peds.
///    Data - Ped data to query. This will be the parent ped if child peds exist.
///    pedAnimData - Struct to populate with ped anim data.
///    iPedID - ID of current ped.
///    iClip - The clip to play.
///    bLoopClip - Rand sync scenes will pass in different clips each time. Looped sync scenes will loop over all clips in activity.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
PROC CREATE_PED_LOCAL_SYNCED_SCENE(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, INT iPedID, INT iClip, BOOL bLoopClip = FALSE, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	FLOAT fStartPhase = -1.0
	FLOAT fPlaybackRate = -1.0
	
	// Parent peds need to make sure all their child peds are alive first
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PARENT_PED)
		IF NOT ARE_LOCAL_CHILD_PEDS_ALIVE(LocalData, Data, iPedID)
			EXIT
		ENDIF
	ENDIF
	
	// Does the starting phase need randomised
	IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
		IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_RAND_START_PHASE)
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_RAND_START_PHASE)
			fStartPhase = GET_RANDOM_FLOAT_IN_RANGE(0.1, 0.8)
		ENDIF
	ENDIF
	
	// Starting DJ clip
	BOOL bInitDJAnimSet = FALSE
	IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DJ_START_PHASE)
		IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_DJ_START_PHASE)
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DJ_START_PHASE)
			iClip = GET_DJ_SYNC_SCENE_STARTING_CLIP(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), Data.animData.iSumAnimDurationMS, fStartPhase)
			bInitDJAnimSet = TRUE
		ENDIF
	ENDIF
	
	Data.animData.iPrevClip = iClip																																// Update prev clip var to diff against when randomly selecting new clip
	GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)	// Get data for newly selected clip
	
	
	// Exit if synced scene is already set up and set to looping
	IF (Data.animData.iSyncSceneID != -1 AND bLoopClip)
		IF (IS_SYNCHRONIZED_SCENE_RUNNING(Data.animData.iSyncSceneID) AND IS_SYNCHRONIZED_SCENE_LOOPED(Data.animData.iSyncSceneID))
			EXIT
		ENDIF
	ENDIF
	
	// Make sure the anim dict is in memory
	IF NOT REQUEST_AND_LOAD_ANIMATION_DICT(pedAnimData.sAnimDict)
		EXIT
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_FACIAL_ANIM_IN_SEPARATE_DICTIONARY)
		IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sMoodAnimDict)
			IF NOT REQUEST_AND_LOAD_ANIMATION_DICT(pedAnimData.sMoodAnimDict)
				EXIT
			ENDIF
		ENDIF	
	ENDIF
	
	// Slow blend in
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_REALLY_SLOW_BLEND_IN)
		CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_REALLY_SLOW_BLEND_IN)
		pedAnimData.fBlendInDelta = REALLY_SLOW_BLEND_IN
	ENDIF
	
	IF DOES_ENTITY_EXIST(Data.PedID)
	AND NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
	AND NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimClip)
		
		// Create the synced scene
		Data.animData.iSyncSceneID = CREATE_SYNCHRONIZED_SCENE(Data.vPosition, Data.vRotation)
		
		#IF IS_DEBUG_BUILD
		STORE_SEQUENCED_PED_CLIP_DETAIL(Data, iClip, pedAnimData)
		#ENDIF
		
		TASK_SYNCHRONIZED_SCENE(Data.PedID, Data.animData.iSyncSceneID, pedAnimData.sAnimDict, pedAnimData.sAnimClip, pedAnimData.fBlendInDelta, pedAnimData.fBlendOutDelta, pedAnimData.syncSceneFlags, pedAnimData.ragdollFlags, pedAnimData.fMoverBlendInDelta, pedAnimData.ikFlags)
		Data.animData.iCurrentClip = iClip
		
		// DJ playback rate
		IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_DJ_PED) AND NOT bInitDJAnimSet
			
			// Temp var to calculate rate based on sum anim duration
			FLOAT fAnimDurationMS = GET_ANIM_DURATION(pedAnimData.sAnimDict, pedAnimData.sAnimClip)*1000.0
			FLOAT fSumAnimDurationMS = TO_FLOAT(Data.animData.iSumAnimDurationMS)
			
			// Calculate rate
			fPlaybackRate = GET_DJ_SYNC_SCENE_RATE(fSumAnimDurationMS, fAnimDurationMS)
			
			// Update sum of animation durations for DJs
			Data.animData.iSumAnimDurationMS += FLOOR(fAnimDurationMS)
			Data.animData.fLastPhase = 0.0
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sFacialAnimClip)
			IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
				SET_FACIAL_CLIPSET(Data.PedID, pedAnimData.sMoodAnimDict)
				SET_FACIAL_IDLE_ANIM_OVERRIDE(Data.PedID, pedAnimData.sFacialAnimClip)
			ELIF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_FACIAL_ANIM_IN_SEPARATE_DICTIONARY)
				PLAY_FACIAL_ANIM(Data.PedID, pedAnimData.sFacialAnimClip, pedAnimData.sMoodAnimDict)
			ELSE
				PLAY_FACIAL_ANIM(Data.PedID, pedAnimData.sFacialAnimClip, pedAnimData.sAnimDict)
			ENDIF	
		ENDIF
		
		// Can only call FORCE_PED_AI_AND_ANIMATION_UPDATE once per frame.
		IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
			IF NOT IS_PEDS_BIT_SET(LocalData.iBS, BS_PED_LOCAL_DATA_FORCED_ANIM_UPDATE_THIS_FRAME)
				SET_PEDS_BIT(LocalData.iBS, BS_PED_LOCAL_DATA_FORCED_ANIM_UPDATE_THIS_FRAME)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(Data.PedID, TRUE, TRUE)
				PRINTLN("[AM_MP_PEDS] CREATE_PED_LOCAL_SYNCED_SCENE - Ped ID: ", iPedID, " Calling: FORCE_PED_AI_AND_ANIMATION_UPDATE")
			ELSE
				SET_PEDS_BIT(Data.iBS, BS_PED_DATA_RETRY_FORCE_ANIM_UPDATE)
				PRINTLN("[AM_MP_PEDS] CREATE_PED_LOCAL_SYNCED_SCENE - Ped ID: ", iPedID, " Setting Bit: BS_PED_DATA_RETRY_FORCE_ANIM_UPDATE")
			ENDIF
		ENDIF
		
		// DJ Dancer interactions
		INT iChildPed = -1
		INT iChildClip = iClip
		BOOL bDJInteraction = FALSE
		IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_DJ_DANCER_INTERACTION)
			iChildClip = GET_LOCAL_DJ_DANCER_INTERACTION_ANIM_CLIP(ePedLocation, LocalData, Data, iChildPed, iPedID, GET_HASH_KEY(pedAnimData.sAnimClip), ePedMusicIntensity)
			IF (iChildClip != -1)
				bDJInteraction = TRUE
			ELSE
				iChildClip = iClip
			ENDIF
		ENDIF
		
		// Add any partner peds to the synced scene
		ADD_PARTNER_PED_TO_LOCAL_SYNCED_SCENE(ePedLocation, LocalData, Data, pedAnimData, iPedID, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		
		// Add any child peds to the synced scene
		BOOL bChildPedUsed = FALSE
		ADD_CHILD_PEDS_TO_LOCAL_SYNCED_SCENE(ePedLocation, LocalData, Data, iPedID, iChildClip, bChildPedUsed, eMusicIntensity, ePedMusicIntensity, bDancingTransition, bDJInteraction, iChildPed)
		
		// Add any props to the synced scene
		ADD_PED_LOCAL_PROPS_TO_SYNCED_SCENE(ePedLocation, Data, pedAnimData, Data.animData.iSyncSceneID, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HEELED_PED))
		
		// Clear transition bit now that transition is tasked to play
		IF (bDancingTransition)
			CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_DANCING_TRANSITION)
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DANCING_TRANSITION_ACTIVE)
		ELSE
			CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_DANCING_TRANSITION_ACTIVE)
		ENDIF
		
		// Set starting phase
		IF (fStartPhase != -1.0)
			SET_SYNCHRONIZED_SCENE_PHASE(Data.animData.iSyncSceneID, fStartPhase)
		ENDIF
		
		// Set playback rate
		IF (fPlaybackRate != -1.0)
			SET_SYNCHRONIZED_SCENE_RATE(Data.animData.iSyncSceneID, fPlaybackRate)
		ENDIF
		
		// Loop the synced scene
		IF bLoopClip
			SET_SYNCHRONIZED_SCENE_LOOPED(Data.animData.iSyncSceneID, TRUE)
		ENDIF
		
		// Hold the last frame of the synced scene - True by default
		IF (bChildPedUsed AND IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_DJ_DANCER_INTERACTION))
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(Data.animData.iSyncSceneID, FALSE)
		ENDIF
		
		// Convo Animation - Controller Speech - Play speech as already checked if ped can do so when selecting the clip 
		IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CONVO_REQUIRES_ANIMATION)
			CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_CONVO_REQUIRES_ANIMATION)
			
			IF CAN_PED_PLAY_SPEECH(ePedLocation, LocalData.PedData[iPedID].PedID, iPedID, ServerBD.iLayout, LocalData.eLocalPedSpeech)
				PLAY_PED_SPEECH(ePedLocation, ServerBD, LocalData, LocalData.PedData[iPedID], LocalData.PedData[iPedID].PedID, iPedID, LocalData.eLocalPedSpeech, LocalData.LocalPedSpeechConversation)
				SET_SPEECH_DATA(LocalData.PedData[iPedID], LocalData.LocalSpeechData[LocalData.iLocalSpeechPedID[iPedID]], LocalData.eLocalPedSpeech, LocalData.PedData[iPedID].PedID)
			ENDIF
			
			RESET_NET_TIMER(LocalData.stLocalSpeechTimer)
		ENDIF
		
		RESET_LOCAL_PED_VFX_DATA(LocalData, pedAnimData, iPedID)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Data.PedID, TRUE)
		SET_PED_KEEP_TASK(Data.PedID, TRUE)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Creates a networked networked synced scene. To be used within PLAY_PED_SYNCED_SCENE()
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - Local ped data to query.
///    Data - Ped data to query.
///    pedAnimData - Struct to populate with ped anim data.
///    iPedID - ID of current ped.
///    iClip - The clip to play.
///    bLoopClip - Rand sync scenes will pass in different clips each time. Looped sync scenes will loop over all clips in activity.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
PROC CREATE_PED_NETWORKED_SYNCED_SCENE(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, INT iPedID, INT iClip, BOOL bLoopClip = FALSE, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	// Parent peds need to make sure all their child peds are alive first
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PARENT_PED)
		IF NOT ARE_NETWORKED_CHILD_PEDS_ALIVE(ServerBD, Data, iPedID)
			EXIT
		ENDIF
	ENDIF
	
	Data.animData.iPrevClip = iClip																																// Update prev clip var to diff against when randomly selecting new clip
	GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)	// Get data for newly selected clip
	
	// Exit if synced scene is already set up and set to looping
	IF (Data.animData.iSyncSceneID != -1 AND bLoopClip)
		EXIT
	ENDIF
	
	// Make sure the anim dict is in memory
	IF NOT REQUEST_AND_LOAD_ANIMATION_DICT(pedAnimData.sAnimDict)
		EXIT
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_FACIAL_ANIM_IN_SEPARATE_DICTIONARY)
		IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sMoodAnimDict)
			IF NOT REQUEST_AND_LOAD_ANIMATION_DICT(pedAnimData.sMoodAnimDict)
				EXIT
			ENDIF
		ENDIF	
	ENDIF
	
	// Slow blend in
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_REALLY_SLOW_BLEND_IN)
		CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_REALLY_SLOW_BLEND_IN)
		pedAnimData.fBlendInDelta = REALLY_SLOW_BLEND_IN
	ENDIF
	
	IF DOES_ENTITY_EXIST(Data.PedID)
	AND NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
	AND NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimClip)
		
		// Create the synced scene
		Data.animData.iSyncSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(Data.vPosition, Data.vRotation, DEFAULT, IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME), bLoopClip, pedAnimData.fStopPhase, pedAnimData.fStartPhase, pedAnimData.fStartRate)
		
		NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(Data.PedID, Data.animData.iSyncSceneID, pedAnimData.sAnimDict, pedAnimData.sAnimClip, pedAnimData.fBlendInDelta, pedAnimData.fBlendOutDelta, pedAnimData.syncSceneFlags, pedAnimData.ragdollFlags, pedAnimData.fMoverBlendInDelta, pedAnimData.ikFlags)
		Data.animData.iCurrentClip = iClip
		
		IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sFacialAnimClip)
			IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
				SET_FACIAL_CLIPSET(Data.PedID, pedAnimData.sMoodAnimDict)
				SET_FACIAL_IDLE_ANIM_OVERRIDE(Data.PedID, pedAnimData.sFacialAnimClip)
			ELIF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_FACIAL_ANIM_IN_SEPARATE_DICTIONARY)
				PLAY_FACIAL_ANIM(Data.PedID, pedAnimData.sFacialAnimClip, pedAnimData.sMoodAnimDict)
			ELSE
				PLAY_FACIAL_ANIM(Data.PedID, pedAnimData.sFacialAnimClip, pedAnimData.sAnimDict)
			ENDIF	
		ENDIF
		
		// Can only call FORCE_PED_AI_AND_ANIMATION_UPDATE once per frame.
		IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_FORCE_ANIM_UPDATE)
			IF NOT IS_PEDS_BIT_SET(LocalData.iBS, BS_PED_LOCAL_DATA_FORCED_ANIM_UPDATE_THIS_FRAME)
				SET_PEDS_BIT(LocalData.iBS, BS_PED_LOCAL_DATA_FORCED_ANIM_UPDATE_THIS_FRAME)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(Data.PedID, TRUE, TRUE)
				PRINTLN("[AM_MP_PEDS] CREATE_PED_NETWORKED_SYNCED_SCENE - Ped ID: ", iPedID, " Calling: FORCE_PED_AI_AND_ANIMATION_UPDATE")
			ELSE
				SET_PEDS_BIT(Data.iBS, BS_PED_DATA_RETRY_FORCE_ANIM_UPDATE)
				PRINTLN("[AM_MP_PEDS] CREATE_PED_NETWORKED_SYNCED_SCENE - Ped ID: ", iPedID, " Setting Bit: BS_PED_DATA_RETRY_FORCE_ANIM_UPDATE")
			ENDIF
		ENDIF
		
		// DJ Dancer interactions
		INT iChildClip = iClip
		BOOL bDJInteraction = FALSE
		IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_DJ_DANCER_INTERACTION)
			iChildClip = GET_NETWORK_DJ_DANCER_INTERACTION_ANIM_CLIP(ePedLocation, ServerBD, Data, iPedID, GET_HASH_KEY(pedAnimData.sAnimClip), ePedMusicIntensity)
			IF (iChildClip != -1)
				bDJInteraction = TRUE
			ELSE
				iChildClip = iClip
			ENDIF
		ENDIF
		
		// Add any partner peds to the synced scene
		ADD_PARTNER_PED_TO_NETWORK_SYNCED_SCENE(ePedLocation, ServerBD, Data, pedAnimData, iPedID, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		
		// Add any child peds to the synced scene
		ADD_CHILD_PEDS_TO_NETWORK_SYNCED_SCENE(ePedLocation, ServerBD, Data, iPedID, iChildClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, bDJInteraction)
		
		// Add any props to the synced scene
		ADD_PED_NETWORKED_PROPS_TO_SYNCED_SCENE(ePedLocation, Data, pedAnimData, Data.animData.iSyncSceneID, iClip, eMusicIntensity, ePedMusicIntensity, IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HEELED_PED))
		
		// Clear transition bit now that transition is tasked to play
		IF (bDancingTransition)
			CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_DANCING_TRANSITION)
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_DANCING_TRANSITION_ACTIVE)
		ELSE
			CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_DANCING_TRANSITION_ACTIVE)
		ENDIF
		
		// Convo Animation - Controller Speech - Play speech as already checked if ped can do so when selecting the clip 
		IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CONVO_REQUIRES_ANIMATION)
			CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_CONVO_REQUIRES_ANIMATION)
			BROADCAST_PED_SPEECH_DATA(GET_PED_NETWORK_ID(iPedID), LocalData.eNetworkPedSpeech)
			RESET_NET_TIMER(LocalData.stNetworkSpeechTimer)
		ENDIF
		
		RESET_NETWORK_PED_VFX_DATA(ServerBD, pedAnimData, iPedID)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Data.PedID, TRUE)
		SET_PED_KEEP_TASK(Data.PedID, TRUE)
		NETWORK_START_SYNCHRONISED_SCENE(Data.animData.iSyncSceneID)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Wrapper function to call CREATE_PED_NETWORKED_SYNCED_SCENE or CREATE_PED_LOCAL_SYNCED_SCENE depending on if the BS_PED_DATA_USE_NETWORK_ANIMS bit is set.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All ped data to query. Needed to query child peds.
///    Data - Ped data to query.
///    pedAnimData - Struct to populate with ped anim data.
///    iPedID - ID of current ped.
///    iClip - The clip to play.
///    bLoopClip - Rand sync scenes will pass in different clips each time. Looped sync scenes will loop over all clips in activity.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
PROC CREATE_PED_SYNCED_SCENE(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, INT iPedID, INT iClip, BOOL bLoopClip = FALSE, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_NETWORK_ANIMS)
		CREATE_PED_NETWORKED_SYNCED_SCENE(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, iClip, bLoopClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	ELSE
		CREATE_PED_LOCAL_SYNCED_SCENE(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, iClip, bLoopClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	ENDIF
ENDPROC

PROC MAINTAIN_SYNCED_SCENE_PROP_VISIBILITY(PED_LOCATIONS ePedLocation, PEDS_DATA &Data)
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		EXIT
	ENDIF
	
	
	IF NETWORK_IS_IN_MP_CUTSCENE()
	OR IS_CUTSCENE_RUNNING_FOR_PED_LOCATION(ePedLocation)		
		IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PED_HIDDEN)
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_PED_HIDDEN)
			IF DOES_ENTITY_EXIST(Data.PedID)
			OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PROPS_ONLY_NO_PED)
				IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PROPS_ONLY_NO_PED)
					SET_ENTITY_VISIBLE(Data.PedID, FALSE)
				ENDIF
				INT iProp
				REPEAT MAX_NUM_PED_PROPS iProp
					IF DOES_ENTITY_EXIST(Data.PropID[iProp])
						SET_ENTITY_ALPHA(Data.PropID[iProp], 0, FALSE)		
					ELSE
						BREAKLOOP
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ELSE
		IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PED_HIDDEN)
			CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_PED_HIDDEN)
			IF DOES_ENTITY_EXIST(Data.PedID)
			OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PROPS_ONLY_NO_PED)
				IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PROPS_ONLY_NO_PED)
					SET_ENTITY_VISIBLE(Data.PedID, TRUE)
				ENDIF
				INT iProp
				REPEAT MAX_NUM_PED_PROPS iProp
					IF DOES_ENTITY_EXIST(Data.PropID[iProp])
						SET_ENTITY_ALPHA(Data.PropID[iProp], 255, FALSE)		
					ELSE
						BREAKLOOP
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Plays a synced scene based on the data populated from GET_PED_ANIM_DATA().
///    The sync scenes are continuously looped by default.
///    Rand sync scenes will pass in randomised clips each time that may not differ from the previous clip.
///    Rand unique sync scenes will pass in different randomised clips each time. 
///    By default, the sync scenes will play all clips in the activity sequentially.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All ped data to query. Needed to query child peds.
///    Data - Ped data to query.
///    pedAnimData - Populated ped anim data.
///    iPedID - ID of current ped.
///    eNextMusicIntensity - For dancing peds. Gets the correct transition data based on the next music intensity.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
///    bNetworkPed - Whether the ped is networked or not.
PROC PLAY_PED_SYNCED_SCENE(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, INT iPedID, CLUB_MUSIC_INTENSITY eNextMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bNetworkPed = FALSE)
	
	MAINTAIN_SYNC_SCENE_PROP_VISIBILITY_FOR_DANCING_ACTIVITY(Data, pedAnimData, eMusicIntensity, ePedMusicIntensity)
	MAINTAIN_SYNC_SCENE_FORCE_ANIM_UPDATE(LocalData, Data, iPedID)
	MAINTAIN_SYNCED_SCENE_PROP_VISIBILITY(ePedLocation, Data)	
	
	IF  SHOULD_PED_SYNC_SCENE_PLAY(Data)	
		INT iClip
		INT iMaxClips
		
		IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			iClip = GET_RAND_ACTIVITY_ANIM(ePedLocation, Data, pedAnimData, eNextMusicIntensity, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			CREATE_PED_SYNCED_SCENE(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, iClip, DEFAULT, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			
		ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_UNIQUE_SYNC_SCENE)
			iClip = GET_RAND_UNIQUE_ACTIVITY_ANIM(ePedLocation, Data, pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			CREATE_PED_SYNCED_SCENE(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, iClip, DEFAULT, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			
		ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_SYNC_SCENE)
			iClip = GET_RAND_PERCENTAGE_ACTIVITY_ANIM(ePedLocation, Data, pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			CREATE_PED_SYNCED_SCENE(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, iClip, DEFAULT, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			
		ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PARTNER_SYNC_SCENE)
			iClip = GET_RAND_PARTNER_ACTIVITY_ANIM(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			CREATE_PED_SYNCED_SCENE(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, iClip, DEFAULT, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			
		ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DIALOGUE_SYNC_SCENE)
			iClip = GET_DIALOGUE_ACTIVITY_ANIM(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, eMusicIntensity, ePedMusicIntensity, bDancingTransition, bNetworkPed)
			CREATE_PED_SYNCED_SCENE(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, iClip, DEFAULT, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			
		ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_EXCLUDE_PARTNER_SYNC_SCENE)
			iClip = GET_EXCLUDE_PARTNER_ACTIVITY_ANIM(ePedLocation, Data, pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			CREATE_PED_SYNCED_SCENE(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, iClip, DEFAULT, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			
		ELSE
			iMaxClips = GET_MAX_ACTIVITY_ANIMS(ePedLocation, Data.animData.iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			
			IF (iMaxClips > 1)
				
				// Ensure iPrevClip is valid
				IF (Data.animData.iPrevClip > iMaxClips-1)
					Data.animData.iPrevClip = 0
				ENDIF
				
				CREATE_PED_SYNCED_SCENE(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, Data.animData.iPrevClip, DEFAULT, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
				
				// Increment iPrevClip to play next clip after current one finishes
				IF (Data.animData.iPrevClip < iMaxClips-1)
					Data.animData.iPrevClip++
				ELSE
					Data.animData.iPrevClip = 0
				ENDIF
			ELSE
				CREATE_PED_SYNCED_SCENE(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, 0, TRUE, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			ENDIF
		ENDIF
	ENDIF
	
	// DJ playback rate
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SYNC_DJ_ANIM_OFTEN)
	AND GET_FRAME_COUNT()%60 = 0
	AND SHOULD_UPDATE_DJ_SYNC_SCENE_SPEED(Data)

		GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, Data.animData.iCurrentClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)	// Get data for newly selected clip
		
		//url:bugstar:7450974
		//only checking if it's loaded, we don't want to request it here, as this is handled separately
		IF NOT REQUEST_AND_LOAD_ANIMATION_DICT(pedAnimData.sAnimDict) 
			EXIT
		ENDIF
		
		// Temp var to calculate rate based on sum anim duration
		FLOAT fCurrentPhase = GET_SYNCHRONIZED_SCENE_PHASE(Data.animData.iSyncSceneID)			
		FLOAT fAnimElapsed = fCurrentPhase*GET_ANIM_DURATION(pedAnimData.sAnimDict, pedAnimData.sAnimClip)*1000.0		
		FLOAT fSumAnimDurationMS = TO_FLOAT(Data.animData.iSumAnimDurationMS)
		INT iCurrentAnimTime = FLOOR(GET_ANIM_DURATION(pedAnimData.sAnimDict, pedAnimData.sAnimClip)*1000.0)
		
		#IF IS_DEBUG_BUILD
		PRINTLN("[PLAY_PED_SYNCED_SCENE][BS_PED_DATA_SYNC_DJ_ANIM_OFTEN] fCurrentPhase = " ,fCurrentPhase)	
		PRINTLN("[PLAY_PED_SYNCED_SCENE][BS_PED_DATA_SYNC_DJ_ANIM_OFTEN] fAnimElapsed = " ,fAnimElapsed)
		PRINTLN("[PLAY_PED_SYNCED_SCENE][BS_PED_DATA_SYNC_DJ_ANIM_OFTEN] GET_ANIM_DURATION = " ,GET_ANIM_DURATION(pedAnimData.sAnimDict, pedAnimData.sAnimClip)*1000.0)
		PRINTLN("[PLAY_PED_SYNCED_SCENE][BS_PED_DATA_SYNC_DJ_ANIM_OFTEN]m fSumAnimDurationMS = " ,fSumAnimDurationMS)
		PRINTLN("[PLAY_PED_SYNCED_SCENE][BS_PED_DATA_SYNC_DJ_ANIM_OFTEN]m fSumAnimDurationMS +  fAnimElapsed - iCurrentAnimTime= " ,Data.animData.iSumAnimDurationMS+fAnimElapsed -iCurrentAnimTime)
		PRINTLN("[PLAY_PED_SYNCED_SCENE][BS_PED_DATA_SYNC_DJ_ANIM_OFTEN] g_clubMusicData.iCurrentTrackTimeMS= " ,g_clubMusicData.iCurrentTrackTimeMS)
		#ENDIF
		// Calculate rate
		FLOAT fPlaybackRate = GET_DJ_SYNC_SCENE_RATE(Data.animData.iSumAnimDurationMS+fAnimElapsed-iCurrentAnimTime, fAnimElapsed)
		
		// Update sum of animation durations for DJs			
		Data.animData.fLastPhase = fCurrentPhase
		
		// Set playback rate
		
		SET_SYNCHRONIZED_SCENE_RATE(Data.animData.iSyncSceneID, fPlaybackRate)
		PRINTLN("[PLAY_PED_SYNCED_SCENE][BS_PED_DATA_SYNC_DJ_ANIM_OFTEN] RATE: ", fPlaybackRate)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Stops a synced scene a ped is playing.
/// PARAMS:
///    Data - Ped data to query.
PROC STOP_PED_SYNCED_SCENE(PEDS_DATA &Data)
	INT iSyncSceneID = Data.animData.iSyncSceneID
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_NETWORK_ANIMS)
		iSyncSceneID = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(Data.animData.iSyncSceneID)
	ENDIF
	
	IF (iSyncSceneID != -1)
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneID)
			DETACH_SYNCHRONIZED_SCENE(iSyncSceneID)
			IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_NETWORK_ANIMS)
				NETWORK_STOP_SYNCHRONISED_SCENE(iSyncSceneID)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Loops through all the peds and stops/detaches any active synced scenes.
/// PARAMS:
///    iMaxLocalPeds - Total local peds.
///    iMaxNetworkPeds - Total network peds.
PROC CLEANUP_ALL_PED_SYNCED_SCENES(NETWORK_PED_DATA &NetworkPedData[], PEDS_DATA &Data[], INT iMaxLocalPeds, INT iMaxNetworkPeds)
	
	INT iPed
	REPEAT iMaxLocalPeds iPed
		STOP_PED_SYNCED_SCENE(Data[iPed])
	ENDREPEAT
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR NOT NETWORK_IS_HOST_OF_THIS_SCRIPT()
		EXIT
	ENDIF
	
	REPEAT iMaxNetworkPeds iPed
		STOP_PED_SYNCED_SCENE(NetworkPedData[iPed].Data)
	ENDREPEAT
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════╡ ANIM SEQUENCES ╞═════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Plays a task sequence on a ped.
///    Does not play any prop anims, props need to be attached to bones for this approach to work (use synced scenes for playing prop anims with ped anims).
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    Data - Ped data to query.
///    pedAnimData - Populated ped anim data.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
PROC PLAY_PED_TASK_SEQUENCE(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE)
	
	SCRIPTTASKSTATUS TaskStatus
	
	IF IS_ENTITY_ALIVE(Data.PedID)
		TaskStatus = GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PERFORM_SEQUENCE)
	ENDIF
	
	IF (TaskStatus != WAITING_TO_START_TASK AND TaskStatus != PERFORMING_TASK)
		IF NOT REQUEST_AND_LOAD_ALL_ACTIVITY_ANIMATION_DICTS(ePedLocation, Data, pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			EXIT
		ENDIF
		
		SEQUENCE_INDEX seq
		OPEN_SEQUENCE_TASK(seq)
			INT iMaxClips = GET_MAX_ACTIVITY_ANIMS(ePedLocation, Data.animData.iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			
			IF (iMaxClips > 1)
				INT iTask[MAX_NUM_ANIMS_IN_SEQUENCE]
				
				INT iClip
				REPEAT MAX_NUM_ANIMS_IN_SEQUENCE iClip
					iTask[iClip] = iClip
				ENDREPEAT
				
				IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_SHUFFLE_ANIM_SEQUENCE)
					INT iStoredClip, iClip1, iClip2
					REPEAT MAX_NUM_ANIMS_TASK_SEQUENCE_SHUFFLES iClip
						iClip1 = GET_RANDOM_INT_IN_RANGE(0, iMaxClips)
						iClip2 = GET_RANDOM_INT_IN_RANGE(0, iMaxClips)
						iStoredClip = iTask[iClip1]
						iTask[iClip1] = iTask[iClip2] 
						iTask[iClip2] = iStoredClip
					ENDREPEAT
				ENDIF
				
				REPEAT MAX_NUM_ANIMS_IN_SEQUENCE iClip
					GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, iTask[iClip], eMusicIntensity, ePedMusicIntensity, bDancingTransition)
					IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
					AND NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimClip)
						TASK_PLAY_ANIM_ADVANCED(NULL, pedAnimData.sAnimDict, pedAnimData.sAnimClip, Data.vPosition, Data.vRotation, pedAnimData.fBlendInDelta, pedAnimData.fBlendOutDelta, pedAnimData.iTimeToPlay, pedAnimData.animFlags, pedAnimData.fStartPhase, pedAnimData.rotOrder, pedAnimData.ikFlags)
					ENDIF
				ENDREPEAT
			ELSE
				IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
				AND NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimClip)
					TASK_PLAY_ANIM_ADVANCED(NULL, pedAnimData.sAnimDict, pedAnimData.sAnimClip, Data.vPosition, Data.vRotation, pedAnimData.fBlendInDelta, pedAnimData.fBlendOutDelta, pedAnimData.iTimeToPlay, pedAnimData.animFlags, pedAnimData.fStartPhase, pedAnimData.rotOrder, pedAnimData.ikFlags)
				ENDIF
			ENDIF
			
			IF (bDancingTransition)
				CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_DANCING_TRANSITION)
			ENDIF
			
			IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_LOOP_ANIM_SEQUENCE)
				SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
			ENDIF
			
		CLOSE_SEQUENCE_TASK(seq)				
		TASK_PERFORM_SEQUENCE(Data.PedID, seq)	
		CLEAR_SEQUENCE_TASK(seq)
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ TASK ANIMS ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Checks whether a task anim has finished its current clip, or hasn't started playing.
/// PARAMS:
///    ePedLocation - Location to query.
///    Data - Ped data to query.
///    pedAnimData - Populated ped anim data.
///    iPedID - Ped ID to query.
///    iLayout - Layout to query.
/// RETURNS: TRUE if a task anim should be played on a ped.
FUNC BOOL SHOULD_PED_TASK_ANIM_PLAY(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, PED_ANIM_DATA &pedAnimData, INT iPedID)
	
	IF SHOULD_ANIM_PLAY_INSTANTLY(ePedLocation, iPedID)
		RETURN TRUE
	ENDIF
	
	IF (Data.animData.iTaskAnimStartTimeMS = -1)
		RETURN TRUE
	ENDIF
	
	IF (pedAnimData.animFlags = AF_LOOPING)
		RETURN FALSE
	ENDIF
	
	IF (GET_CURRENT_TASK_ANIM_PHASE(Data) >= GET_ANIM_MAX_PHASE(Data))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets an animations duration time in milliseconds.
///    Factors in if an animation was started at a start phase other than 0.0
/// PARAMS:
///    sAnimDict - The animation dictionary to query.
///    sAnimClip - The animation clip to query.
///    fStartPhase - The start phase of the animation.
///    iTimeToPlay - Time to play the animation in milliseconds.
/// RETURNS: An animations duration time in milliseconds.
FUNC INT GET_ANIM_DURATION_TIME_MS(STRING sAnimDict, STRING sAnimClip, FLOAT fStartPhase, INT iTimeToPlay = -1)
	
	FLOAT fAnimDurationMS = GET_ANIM_DURATION(sAnimDict, sAnimClip)*1000.0
	
	IF (fStartPhase > 0.0)
		FLOAT fTimeRemainingMS = (fAnimDurationMS*fStartPhase)
		fAnimDurationMS -= fTimeRemainingMS
		
	ELIF (iTimeToPlay != -1)
		fAnimDurationMS = TO_FLOAT(iTimeToPlay)
	ENDIF
	
	RETURN FLOOR(fAnimDurationMS)
ENDFUNC

/// PURPOSE:
///    Tasks a ped to play a task anim.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All ped data to query.
///    Data - Ped data to query.
///    pedAnimData - Populated ped anim data.
///    iPedID - Ped ID to query.
///    iClip - The clip to play.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
///    bNetworkPed - Whether the ped is networked or not.
///    bPedTransition - For non dancing peds. Whether a ped should play a transition animation.
PROC TASK_PED_ANIM(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, INT iPedID, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bNetworkPed = FALSE, BOOL bPedTransition = FALSE)

	GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, iPedID, Data.eTransitionState, bPedTransition)
	
	// Make sure the anim dict is in memory
	IF NOT REQUEST_AND_LOAD_ANIMATION_DICT(pedAnimData.sAnimDict)
		EXIT
	ENDIF
	
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_FACIAL_ANIM_IN_SEPARATE_DICTIONARY)
		IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sMoodAnimDict)
			IF NOT REQUEST_AND_LOAD_ANIMATION_DICT(pedAnimData.sMoodAnimDict)
				EXIT
			ENDIF
		ENDIF	
	ENDIF
	
	// Does the starting phase need randomised
	IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
		IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_RAND_START_PHASE)
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_RAND_START_PHASE)
			pedAnimData.fStartPhase = GET_RANDOM_FLOAT_IN_RANGE(0.1, 0.8)
		ENDIF
	ENDIF
	
	// Dancing blends
	IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_DANCING_ACTIVITY)
		IF (pedAnimData.iTimeToPlay > -1)
			IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_REALLY_SLOW_BLEND_IN)
				SET_PEDS_BIT(Data.iBS, BS_PED_DATA_REALLY_SLOW_BLEND_IN)
			ELSE
				pedAnimData.fBlendInDelta = REALLY_SLOW_BLEND_IN
			ENDIF
		ELSE
			IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_REALLY_SLOW_BLEND_IN)
				CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_REALLY_SLOW_BLEND_IN)
				pedAnimData.fBlendInDelta = REALLY_SLOW_BLEND_IN
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimDict)
	AND NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sAnimClip)
		TASK_PLAY_ANIM_ADVANCED(Data.PedID, pedAnimData.sAnimDict, pedAnimData.sAnimClip, Data.vPosition, Data.vRotation, pedAnimData.fBlendInDelta, pedAnimData.fBlendOutDelta, pedAnimData.iTimeToPlay, pedAnimData.animFlags, pedAnimData.fStartPhase, pedAnimData.rotOrder, pedAnimData.ikFlags)
		Data.animData.iTaskAnimStartTimeMS 	= GET_GAME_TIMER()
		Data.animData.iTaskAnimDurationMS 	= GET_ANIM_DURATION_TIME_MS(pedAnimData.sAnimDict, pedAnimData.sAnimClip, pedAnimData.fStartPhase, pedAnimData.iTimeToPlay)
		Data.animData.iCurrentClip 			= iClip
		
		IF NOT IS_STRING_NULL_OR_EMPTY(pedAnimData.sFacialAnimClip)
			//PRINTLN("TASK_PED_ANIM activity: ", DEBUG_GET_PED_ACTIVITY_STRING(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity)))
			IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_PLAY_FACE_AS_MOOD)
				SET_FACIAL_CLIPSET(Data.PedID, pedAnimData.sMoodAnimDict)
				SET_FACIAL_IDLE_ANIM_OVERRIDE(Data.PedID, pedAnimData.sFacialAnimClip)				
			ELIF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_FACIAL_ANIM_IN_SEPARATE_DICTIONARY)
				PLAY_FACIAL_ANIM(Data.PedID, pedAnimData.sFacialAnimClip, pedAnimData.sMoodAnimDict)
			ELSE
				PLAY_FACIAL_ANIM(Data.PedID, pedAnimData.sFacialAnimClip, pedAnimData.sAnimDict)
			ENDIF	
		ENDIF
		
		IF (pedAnimData.bRemoveAnimDict)
			REMOVE_ANIM_DICT(pedAnimData.sAnimDict)
		ENDIF
		
		CLEAR_ANIM_INSTANTLY_FLAGS(ePedLocation, iPedID)
		
		IF (bNetworkPed)
			RESET_NETWORK_PED_VFX_DATA(ServerBD, pedAnimData, iPedID)
		ELSE
			RESET_LOCAL_PED_VFX_DATA(LocalData, pedAnimData, iPedID)
		ENDIF
	ENDIF

	IF (bDancingTransition)
		CLEAR_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_DANCING_TRANSITION)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Plays a task anim based on the data populated from GET_PED_ANIM_DATA().
///    Does not play any prop anims, props need to be attached to bones for this approach to work (use synced scenes for playing prop anims with ped anims).
///    The task anims are continuously looped by default.
///    Rand task anims will pass in randomised clips each time that may not differ from the previous clip.
///    Rand task anims will pass in different randomised clips each time. 
///    By default, the task anims will play all clips in the activity sequentially.
/// PARAMS:
///    ePedLocation - Which location is being queried.
///    ServerBD - Server ped data to query.
///    LocalData - All ped data to query.
///    Data - Ped data to query.
///    pedAnimData - Populated ped anim data.
///    iPedID - Ped ID to query.
///    eNextMusicIntensity - For dancing peds. Gets the correct transition data based on the next music intensity.
///    eMusicIntensity - For dancing peds. Gets the correct data based on the music intensity.
///    ePedMusicIntensity - For dancing peds. The peds currently tracked intensity. Used for dancing transitions.
///    bDancingTransition - For dancing peds. Transition clips that link activities together.
///    bNetworkPed - Whether the ped is networked or not.
///    bPedTransition - For non dancing peds. Whether a ped should play a transition animation.
PROC PLAY_PED_TASK_ANIM(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, PED_ANIM_DATA pedAnimData, INT iPedID, CLUB_MUSIC_INTENSITY eNextMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bNetworkPed = FALSE, BOOL bPedTransition = FALSE)
	
	MAINTAIN_SYNCED_SCENE_PROP_VISIBILITY(ePedLocation, Data)
	
	IF NOT SHOULD_PED_TASK_ANIM_PLAY(ePedLocation, Data, pedAnimData, iPedID)
		EXIT
	ENDIF
	
	INT iClip
	INT iMaxClips
	
	IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
		iClip = GET_RAND_ACTIVITY_ANIM(ePedLocation, Data, pedAnimData, eNextMusicIntensity, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		TASK_PED_ANIM(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, bNetworkPed)
		
	ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_UNIQUE_ANIM_TASK)
		iClip = GET_RAND_UNIQUE_ACTIVITY_ANIM(ePedLocation, Data, pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		TASK_PED_ANIM(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, bNetworkPed)
		
	ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_PERCENTAGE_TASK_ANIM)
		iClip = GET_RAND_PERCENTAGE_ACTIVITY_ANIM(ePedLocation, Data, pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		TASK_PED_ANIM(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, bNetworkPed)
	
	ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_TRANSITION_TASK_ANIM)
		iClip = GET_RAND_TRANSITION_ACTIVITY_ANIM(ePedLocation, Data, pedAnimData, bPedTransition, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		TASK_PED_ANIM(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, iClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, bNetworkPed, bPedTransition)
		
	ELSE
		iMaxClips = GET_MAX_ACTIVITY_ANIMS(ePedLocation, Data.animData.iMaxClips, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
		
		IF (iMaxClips > 1)
			
			// Ensure iPrevClip is valid
			IF (Data.animData.iPrevClip > iMaxClips-1)
				Data.animData.iPrevClip = 0
			ENDIF
			
			TASK_PED_ANIM(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, Data.animData.iPrevClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition, bNetworkPed)
			
			// Increment iPrevClip to play next clip after current one finishes
			IF (Data.animData.iPrevClip < iMaxClips-1)
				Data.animData.iPrevClip++
			ELSE
				Data.animData.iPrevClip = 0
			ENDIF
		ELSE
			TASK_PED_ANIM(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, 0, eMusicIntensity, ePedMusicIntensity, bDancingTransition, bNetworkPed)
		ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Plays a task start scenario in place
PROC PLAY_PED_TASK_SCENARIO_IN_PLACE(/*PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData,*/ PEDS_DATA &Data, PED_ANIM_DATA pedAnimData)//, INT iPedID, CLUB_MUSIC_INTENSITY eNextMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bNetworkPed = FALSE)
	
	IF (GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_START_SCENARIO_IN_PLACE) != PERFORMING_TASK
	AND GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_START_SCENARIO_IN_PLACE) != WAITING_TO_START_TASK)
		TASK_START_SCENARIO_IN_PLACE(Data.PedID, pedAnimData.sAnimClip, DEFAULT, !IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_SCENARIO_DONT_PLAY_INTRO_CLIP)	)	
	ENDIF
	
ENDPROC

//╒══════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════╡ GLOBAL DATA ╞══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Maintains any globals need for specific ped types e.g. DJs.
/// PARAMS:
///    Data - Ped data to query.
///    iPedID - Ped ID to query.
PROC MAINTAIN_PED_ANIM_GLOBALS(PEDS_DATA &Data, INT iPedID)
	
	// DJ Anim Data
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_DJ_DANCER_INTERACTION)
		IF (iPedID < MAX_PED_ON_STAGE_DJS)
			
			// Next clip hash
			IF (g_PedDJAnimData[iPedID].iNextClipHash = -1)
			OR (g_PedDJAnimData[iPedID].iNextClipHash != GET_HASH_KEY(GET_PED_DJ_ANIM_CLIP_BASE(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), Data.animData.iCurrentClip+1)))
				g_PedDJAnimData[iPedID].iNextClipHash = GET_HASH_KEY(GET_PED_DJ_ANIM_CLIP_BASE(INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), Data.animData.iCurrentClip+1))
			ENDIF
			
			// Time until next clip in seconds
			g_PedDJAnimData[iPedID].iTimeUntilNextClipSeconds = GET_TIME_UNTIL_END_OF_CURRENT_ANIM_IN_SECONDS(Data)
			
		ENDIF
	ENDIF
	
ENDPROC


//╒══════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ MAINTAIN ╞═══════════════════════════════╡
//╘══════════════════════════════════════════════════════════════════════════╛

PROC UPDATE_PED_ANIMATION(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, SCRIPT_PED_DATA &LocalData, PEDS_DATA &Data, PED_ANIM_DATA &pedAnimData, INT iPedID, CLUB_MUSIC_INTENSITY eNextMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bNetworkPed = FALSE, BOOL bPedTransition = FALSE)
		
	IF IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_USE_PATROL)
	OR IS_PEDS_BIT_SET(data.iBS, BS_PED_DATA_NO_ANIMATION)
	OR NOT IS_ENTITY_A_PED(Data.PedID)
		EXIT
	ENDIF
	
	RESET_PED_ANIM_DATA(pedAnimData)
	GET_PED_ANIM_DATA(ePedLocation, INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity), pedAnimData, DEFAULT, eMusicIntensity, ePedMusicIntensity, bDancingTransition, iPedID, Data.eTransitionState, bPedTransition)
	
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CHILD_PED)
		IF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			PLAY_PED_SYNCED_SCENE(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, eNextMusicIntensity, eMusicIntensity, ePedMusicIntensity, bDancingTransition, bNetworkPed)
			
		ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_SEQUENCE)
			PLAY_PED_TASK_SEQUENCE(ePedLocation, Data, pedAnimData, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
			
		ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			PLAY_PED_TASK_ANIM(ePedLocation, ServerBD, LocalData, Data, pedAnimData, iPedID, eNextMusicIntensity, eMusicIntensity, ePedMusicIntensity, bDancingTransition, bNetworkPed, bPedTransition)
			
		ELIF IS_PEDS_BIT_SET(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SCENARIO_IN_PLACE)
			PLAY_PED_TASK_SCENARIO_IN_PLACE(Data, pedAnimData)
		ENDIF
	ENDIF
	
	
	IF (bNetworkPed)
		MAINTAIN_NETWORK_PED_VFX(ePedLocation, ServerBD, pedAnimData, iPedID, Data.animData.iCurrentClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	ELSE
		MAINTAIN_LOCAL_PED_VFX(ePedLocation, LocalData, pedAnimData, iPedID, Data.animData.iCurrentClip, eMusicIntensity, ePedMusicIntensity, bDancingTransition)
	ENDIF
	IF NOT IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CHILD_PED)
		MAINTAIN_PED_ANIM_GLOBALS(Data, iPedID)
	ENDIF
ENDPROC
#ENDIF	// FEATURE_HEIST_ISLAND
