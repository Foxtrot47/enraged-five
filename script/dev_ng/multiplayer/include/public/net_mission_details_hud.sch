
//|=======================================================================================|
//|                 Author:  Brenda Carey				 Date: 19/10/2011                 |
//|=======================================================================================|
//| 																					  |
//|                             net_mission_details_hud    	                              |
//|                a) the box that displays in the top left corner			              |
//|                             	 								                      |
//| 																					  |
//|=======================================================================================|




//USING "net_include.sch"
//USING "rage_builtins.sch"
//USING "globals.sch"
USING "Screen_placements.sch"
USING "shared_hud_displays.sch"
USING "Screens_header.sch"
USING "net_hud_activating.sch"
USING "net_hud_displays.sch"
USING "Transition_Crews.sch"
USING "MP_Scaleform_Functions.sch"
USING "stats_helper_packed.sch"
USING "net_transition_sessions_private.sch"
USING "Freemode_Header.sch"
USING "freemode_events_header_private.sch"
#IF FEATURE_FREEMODE_ARCADE 
USING "lobby_public.sch"
#ENDIF

FUNC TRANSITION_FM_SESSION_MENU_CHOICE GET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE()
	RETURN g_Private_Players_FM_SESSION_Menu_Choice
ENDFUNC

PROC SET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE(TRANSITION_FM_SESSION_MENU_CHOICE Selected)

	#IF IS_DEBUG_BUILD
		DEBUG_PRINTCALLSTACK()
		NET_NL()NET_PRINT("<<<SET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE = ")NET_PRINT(GET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE_STRING(Selected))NET_PRINT(" >>>> ")
	#ENDIF

	g_Private_Players_FM_SESSION_Menu_Choice = Selected
ENDPROC






FUNC BOOL IS_SAFE_TO_QUIT_BAIL_SCREEN_EXTERNAL_SCREENS()
	IF NOT NETWORK_IS_SESSION_BUSY()
		
		#IF FEATURE_GEN9_EXCLUSIVE
		BOOL bBypassTransitionStateCheck
		IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_SP_SWOOP_UP
		OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_PRE_HUD_CHECKS
			IF GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY
			AND GET_JOINING_GAMEMODE() = GAMEMODE_FM
			AND g_Private_BailWarningScreenEventParam > -1
			AND INT_TO_ENUM(JOIN_RESPONSE_CODE, g_Private_BailWarningScreenEventParam)  = RESPONSE_DENY_GROUP_FULL
			AND g_bAllowSafeToQuitBailScreenBypass
				PRINTLN("IS_SAFE_TO_QUIT_BAIL_SCREEN_EXTERNAL_SCREENS = bypasssing check on transition state")
				bBypassTransitionStateCheck = TRUE
			ENDIF
		ENDIF
		#ENDIF
		
		IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_HUD_EXIT
		OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_TERMINATE_SESSION_AND_HOLD
		OR GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_MP_SWOOP_UP //Only for external screens, due to ARE_ANY_EXTERNAL_WARNING_SCREENS_RUNNING()		
		OR IS_TRANSITION_ACTIVE() = FALSE
		#IF FEATURE_GEN9_EXCLUSIVE
		OR bBypassTransitionStateCheck
		#ENDIF
		
			IF CLEAN_UP_TRANSITION_SESSION_AND_WAIT()
				IF (IS_SKYSWOOP_IN_SKY() AND GET_IS_LOADING_SCREEN_ACTIVE())
				OR (IS_SKYCAM_PAST_FIRST_CUT() AND GET_IS_LOADING_SCREEN_ACTIVE() = FALSE)
					RETURN TRUE
				ELSE
					NET_NL()NET_PRINT("IS_SAFE_TO_QUIT_BAIL_SCREEN_EXTERNAL_SCREENS = FALSE - IS_SKYSWOOP_IN_SKY = ")NET_PRINT_BOOL(IS_SKYSWOOP_IN_SKY())
					NET_NL()NET_PRINT("IS_SAFE_TO_QUIT_BAIL_SCREEN_EXTERNAL_SCREENS = FALSE - GET_IS_LOADING_SCREEN_ACTIVE = ")NET_PRINT_BOOL(GET_IS_LOADING_SCREEN_ACTIVE())
					NET_NL()NET_PRINT("IS_SAFE_TO_QUIT_BAIL_SCREEN_EXTERNAL_SCREENS = FALSE - IS_SKYCAM_PAST_FIRST_CUT = ")NET_PRINT_BOOL(IS_SKYCAM_PAST_FIRST_CUT())
				ENDIF
			ELSE
				NET_NL()NET_PRINT("IS_SAFE_TO_QUIT_BAIL_SCREEN_EXTERNAL_SCREENS = FALSE - CLEAN_UP_TRANSITION_SESSION_AND_WAIT = FALSE ")
			ENDIF
		ELSE
			NET_NL()NET_PRINT("IS_SAFE_TO_QUIT_BAIL_SCREEN_EXTERNAL_SCREENS = FALSE - GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAIT_HUD_EXIT ")
		ENDIF
	ELSE
		NET_NL()NET_PRINT("IS_SAFE_TO_QUIT_BAIL_SCREEN_EXTERNAL_SCREENS = FALSE - NETWORK_IS_SESSION_BUSY = TRUE ")
	ENDIF
	RETURN FALSE
ENDFUNC


FUNC MATCHMAKING_GROUP GET_MATCHMAKING_GROUP_FROM_TEAM(INT iTeam, MP_GAMEMODE aGamemode)
	MATCHMAKING_GROUP Result
	
	SWITCH aGamemode
		CASE GAMEMODE_FM
			SWITCH iTeam
				CASE TEAM_SCTV
					NET_NL()NET_PRINT("GET_MATCHMAKING_GROUP_FROM_TEAM: TEAM_SCTV ")
					Result = MM_GROUP_SCTV
				BREAK
				DEFAULT
					IF IS_INVITED_TO_SCTV()
					OR IS_PAUSE_MENU_SELECT_TO_SCTV()
						NET_NL()NET_PRINT("GET_MATCHMAKING_GROUP_FROM_TEAM: IS_INVITED_TO_SCTV = ")NET_PRINT_BOOL(IS_INVITED_TO_SCTV())
						NET_NL()NET_PRINT("GET_MATCHMAKING_GROUP_FROM_TEAM: IS_PAUSE_MENU_SELECT_TO_SCTV = ")NET_PRINT_BOOL(IS_PAUSE_MENU_SELECT_TO_SCTV())
						Result = MM_GROUP_SCTV
					ELSE
						Result = MM_GROUP_FREEMODER
					ENDIF
				BREAK
				
			ENDSWITCH
		
		
		BREAK
	ENDSWITCH

	RETURN Result
ENDFUNC


FUNC INT GET_SCALED_MENTALNESS()
	
	//MENTAL_STATE_BUILD
	FLOAT MentalPercentage
	FLOAT m
	FLOAT fScaledMentalness
	INT iScaledMentalness
	
	//MentalPercentage = GET_MP_FLOAT_PLAYER_STAT(MPPLY_PLAYER_FMENTAL_STATE) 
	MentalPercentage = GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_PLAYER_MENTAL_STATE)
			
	//MENTAL_STATE_BUILD
	//Scale 0-100 to within 0-7
	
	m = 0.07 //(7.0/100.0)
	
	fScaledMentalness = MentalPercentage*m

	iScaledMentalness = ROUND(fScaledMentalness)
	
	NET_NL()NET_PRINT("GET_SCALED_MENTALNESS: MentalPercentage = ")NET_PRINT_FLOAT(MentalPercentage)
	NET_NL()NET_PRINT("GET_SCALED_MENTALNESS: m = ")NET_PRINT_FLOAT(m)
	NET_NL()NET_PRINT("GET_SCALED_MENTALNESS: fScaledMentalness = ")NET_PRINT_FLOAT(fScaledMentalness)
	NET_NL()NET_PRINT("GET_SCALED_MENTALNESS: iScaledMentalness = ")NET_PRINT_INT(iScaledMentalness)
	
	RETURN iScaledMentalness
	
ENDFUNC



PROC SETUP_MATCHMAKING_RULES(MP_GAMEMODE aGamemode, INT iCurrentlySelected = -1, BOOL HeadingToAnotherMission = FALSE)

	DEBUG_PRINTCALLSTACK()

	IF IS_CLOUD_DOWN_CLOUD_LOADER() 
	OR IS_PLAYER_KICKED_TO_GO_OFFLINE()
		//UGC_SET_USING_OFFLINE_CONTENT(TRUE)
		//NET_NL()NET_PRINT("SETUP_MATCHMAKING_RULES: UGC_SET_USING_OFFLINE_CONTENT(TRUE) ")
	ELSE
		UGC_SET_USING_OFFLINE_CONTENT(FALSE)
		NET_NL()NET_PRINT("SETUP_MATCHMAKING_RULES: UGC_SET_USING_OFFLINE_CONTENT(FALSE) ")
	ENDIF



	g_b_HasJoinedSessionHappened = TRUE
	NETWORK_SESSION_SET_SCRIPT_VALIDATE_JOIN()

	#IF FEATURE_FREEMODE_ARCADE
	IF IS_FREEMODE_ARCADE()
	OR aGamemode = GAMEMODE_ARCADE
		NETWORK_SESSION_SET_MATCHMAKING_GROUP(MM_GROUP_FREEMODER)
		NETWORK_SESSION_ADD_ACTIVE_MATCHMAKING_GROUP(MM_GROUP_FREEMODER)		
		NETWORK_SESSION_SET_MATCHMAKING_GROUP_MAX(MM_GROUP_FREEMODER, NUM_NETWORK_PLAYERS)

		#IF IS_DEBUG_BUILD
			PRINTLN("[AIMPREF] SETUP_MATCHMAKING_RULES - Using freemode arcade setup")
		#ENDIF	
		#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("[AIMPREF] SETUP_MATCHMAKING_RULES - Using freemode arcade setup ")
		#ENDIF
		EXIT
	ENDIF
	#ENDIF
	
	
	
	MATCHMAKING_GROUP MyGroup = GET_MATCHMAKING_GROUP_FROM_TEAM(GET_STAT_CHARACTER_TEAM(iCurrentlySelected), aGamemode)
	
	NETWORK_SESSION_SET_MATCHMAKING_GROUP(MyGroup)

	IF MyGroup = MM_GROUP_SCTV
		NET_NL()NET_PRINT("SETUP_MATCHMAKING_RULES: SET_PLAYER_TEAM(PLAYER_ID(), TEAM_SCTV)")
		SET_ENTERED_GAME_AS_SCTV(TRUE, iCurrentlySelected)
		SET_PLAYER_TEAM(PLAYER_ID(), TEAM_SCTV)
	ENDIF
	
	
	INT SCTV_Max 
	INT FMMax
	INT iOwnedProperty
	SWITCH aGamemode
		CASE GAMEMODE_FM
	
			IF IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
			AND NETWORK_IS_SIGNED_ONLINE()	
			AND HAS_IMPORTANT_STATS_LOADED()
				REFRESH_LOCAL_PLAYER_RANK(iCurrentlySelected)
			ENDIF
			
			IF NOT GET_CONFIRM_INVITE_INTO_GAME_STATE()
			AND NOT HeadingToAnotherMission 
			AND NOT AM_I_ACCEPTING_TRANSITION_SESSIONS_INVITE_FROM_SP()

				IF GET_PREVIOUS_AIM_STATE_BEFORE_CHANGE() = 0
//					SET_PLAYER_TARGETING_MODE(INT_TO_ENUM(TARGETING_MODE_TYPE, 0))
					SET_PREVIOUS_AIM_STATE_HAS_CHANGED(TRUE)
					#IF IS_DEBUG_BUILD
						PRINTLN("[AIMPREF] SETUP_MATCHMAKING_RULES - Changing Aim type back to Trad GTA ")
					#ENDIF	
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[AIMPREF] SETUP_MATCHMAKING_RULES - Changing Aim type back to Trad GTA ")
					#ENDIF
					
				ELIF GET_PREVIOUS_AIM_STATE_BEFORE_CHANGE()  = 1
//					SET_PLAYER_TARGETING_MODE(INT_TO_ENUM(TARGETING_MODE_TYPE,1))
					SET_PREVIOUS_AIM_STATE_HAS_CHANGED(TRUE)
					#IF IS_DEBUG_BUILD
						PRINTLN("[AIMPREF] SETUP_MATCHMAKING_RULES - Changing Aim type back to Ass Aim ")
					#ENDIF	
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[AIMPREF] SETUP_MATCHMAKING_RULES - Changing Aim type back to Ass Aim ")
					#ENDIF
				ELIF GET_PREVIOUS_AIM_STATE_BEFORE_CHANGE() = 2
//					SET_PLAYER_TARGETING_MODE(INT_TO_ENUM(TARGETING_MODE_TYPE,2))
					SET_PREVIOUS_AIM_STATE_HAS_CHANGED(TRUE)
					#IF IS_DEBUG_BUILD
						PRINTLN("[AIMPREF] SETUP_MATCHMAKING_RULES - Changing Aim type back to Free Aim ")
					#ENDIF	
					#IF USE_FINAL_PRINTS
					PRINTLN_FINAL("[AIMPREF] SETUP_MATCHMAKING_RULES - Changing Aim type back to Free Aim ")
					#ENDIF
				ENDIF



			ENDIF
						
	
			NETWORK_SESSION_ADD_ACTIVE_MATCHMAKING_GROUP(MM_GROUP_SCTV)
			NETWORK_SESSION_ADD_ACTIVE_MATCHMAKING_GROUP(MM_GROUP_FREEMODER)
			iOwnedProperty = GET_LAST_USED_PROPERTY()
			IF iOwnedProperty > 0
				NETWORK_SESSION_SET_MATCHMAKING_PROPERTY_ID(iOwnedProperty)
			ENDIF
			
			NETWORK_SESSION_SET_MATCHMAKING_MENTAL_STATE(GET_SCALED_MENTALNESS())
	
			FMMax = GET_MAX_NUM_PLAYERS_FOR_TEAM(TEAM_FREEMODE, GAMEMODE_FM)
			SCTV_Max = GET_MAX_NUM_PLAYERS_FOR_TEAM(TEAM_SCTV, GAMEMODE_FM)
			
		
			
			IF NUM_NETWORK_PLAYERS != g_sMPTunables.iMaxNgMainSessionSize
			
				FMMax = g_sMPTunables.iMaxNgMainSessionSize-SCTV_Max
				
				IF FMMax < 0
					FMMax = 0
				ENDIF
				

				#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("SETUP_MATCHMAKING_RULES - Tunable difference detected!! ")
					NET_NL()NET_PRINT("SETUP_MATCHMAKING_RULES - g_sMPTunables.iMaxNgMainSessionSize = ")NET_PRINT_INT(g_sMPTunables.iMaxNgMainSessionSize)
				#ENDIF	
			
			ENDIF


			#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("SETUP_MATCHMAKING_RULES - NUM_NETWORK_PLAYERS = ")NET_PRINT_INT(NUM_NETWORK_PLAYERS)
				NET_NL()NET_PRINT("SETUP_MATCHMAKING_RULES - FMMax = ")NET_PRINT_INT(FMMax)					
				NET_NL()NET_PRINT("SETUP_MATCHMAKING_RULES - SCTV_Max = ")NET_PRINT_INT(SCTV_Max)
			#ENDIF	
			

			NETWORK_SESSION_SET_MATCHMAKING_GROUP_MAX(MM_GROUP_FREEMODER, FMMax)
			NETWORK_SESSION_SET_MATCHMAKING_GROUP_MAX(MM_GROUP_SCTV, SCTV_Max)


			SWITCH GET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE()
				
				CASE FM_SESSION_MENU_CHOICE_JOIN_CLOSED_CREW_2_ONLY_SESSION
					NET_NL()NET_PRINT("SETUP_MATCHMAKING_RULES: FM_SESSION_MENU_CHOICE_JOIN_CLOSED_CREW_2_ONLY_SESSION")
					NETWORK_SESSION_SET_UNIQUE_CREW_LIMIT(2)
				BREAK
				CASE FM_SESSION_MENU_CHOICE_JOIN_CLOSED_CREW_3_ONLY_SESSION
					NET_NL()NET_PRINT("SETUP_MATCHMAKING_RULES: FM_SESSION_MENU_CHOICE_JOIN_CLOSED_CREW_3_ONLY_SESSION")
				
					NETWORK_SESSION_SET_UNIQUE_CREW_LIMIT(3)
				BREAK
				CASE FM_SESSION_MENU_CHOICE_JOIN_CLOSED_CREW_4_ONLY_SESSION
					NET_NL()NET_PRINT("SETUP_MATCHMAKING_RULES: FM_SESSION_MENU_CHOICE_JOIN_CLOSED_CREW_4_ONLY_SESSION")
				
					NETWORK_SESSION_SET_UNIQUE_CREW_LIMIT(4)
				BREAK
				
				CASE FM_SESSION_MENU_CHOICE_JOIN_CLOSED_MY_CREW_ONLY_SESSION
					NET_NL()NET_PRINT("SETUP_MATCHMAKING_RULES: FM_SESSION_MENU_CHOICE_JOIN_CLOSED_MY_CREW_ONLY_SESSION")
				
					NETWORK_SESSION_SET_UNIQUE_CREW_LIMIT(1)
				BREAK

			ENDSWITCH
			
		BREAK
	
	ENDSWITCH




ENDPROC



PROC IGNORE_INVITE()
	NET_NL()NET_PRINT("IGNORE_INVITE -called  ")NET_NL()

	SET_CONFIRM_PRESENCE_INVITE_INTO_GAME_STATE(FALSE)
	SET_INVITED_TO_SCTV(FALSE)
	g_PresenceInviteUniqueId = 0
	SET_CONFIRM_INVITE_INTO_GAME_STATE(FALSE)
	SET_NEED_TO_JOIN_SESSION_FROM_INVITE(FALSE)
	#IF FEATURE_COPS_N_CROOKS
	IF NOT IS_TRANSITION_SESSION_JOINING_ARCADE_MODE_FROM_INVITE()
	AND NOT IS_TRANSITION_SESSION_REJOINING_ARCADE_MODE()
	#ENDIF
		NETWORK_SESSION_FORCE_CANCEL_INVITE()
	#IF FEATURE_COPS_N_CROOKS
	ENDIF
	#ENDIF
	
//	NETWORK_SESSION_CANCEL_INVITE()
	RESET_EVENT_APP_LAUNCH_FLAG_RECIEVED()
	RESET_TRANSITION_SESSION_NON_RESET_VARS()
	RESET_TRANSITION_SESSION_CORONA_BISET()
	SET_JOINING_BOOT_INVITE(FALSE)

ENDPROC


FUNC STRING GET_MONTH_LABEL(int number)

	TEXT_LABEL_15 result = "MONTH_"
	result += number
	RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(result)
ENDFUNC

FUNC STRING GET_MONTH_LABEL_LOWERCASE(int number)

	TEXT_LABEL_15 result = "MONTH_"
	result += number
	result += "L"
	RETURN GET_FILENAME_FOR_AUDIO_CONVERSATION(result)
ENDFUNC

FUNC TEXT_LABEL_31 GET_DATE_FOR_BANNED_ACCT_CREATION_DATE(UGC_DATE& sDate)
	TEXT_LABEL_31 tlReturn
	/*
	IF LOCALIZATION_GET_SYSTEM_DATE_TYPE() = DATE_FORMAT_MDY
		tlReturn = sDate.nMonth 
		tlReturn += "/" 
		tlReturn += sDate.nDay  
		tlReturn += "/" 
		tlReturn += sDate.nYear
	ELIF LOCALIZATION_GET_SYSTEM_DATE_TYPE() = DATE_FORMAT_DMY
		tlReturn = sDate.nDay 
		tlReturn += "/" 
		tlReturn += sDate.nMonth  
		tlReturn += "/" 
		tlReturn += sDate.nYear
	ELIF LOCALIZATION_GET_SYSTEM_DATE_TYPE() = DATE_FORMAT_YMD
		tlReturn = sDate.nYear 
		tlReturn += "/" 
		tlReturn += sDate.nMonth  
		tlReturn += "/" 
		tlReturn += sDate.nDay
	ELSE
		tlReturn = sDate.nMonth  
		tlReturn += "/" 
		tlReturn += sDate.nDay 
		tlReturn += "/" 
		tlReturn += sDate.nYear
	ENDIF
	*/
	
	TEXT_LABEL_31 month = GET_MONTH_LABEL(sDate.nMonth) 
	
	
	SWITCH GET_CURRENT_LANGUAGE()
		
		
		CASE LANGUAGE_PORTUGUESE
			month = GET_MONTH_LABEL_LOWERCASE(sDate.nMonth)
			tlReturn = sDate.nDay 
			tlReturn += " " 
			tlReturn += GET_FILENAME_FOR_AUDIO_CONVERSATION("POSTDAYCHAR")
			tlReturn += " " 
			tlReturn += month
			tlReturn += " " 
			tlReturn += GET_FILENAME_FOR_AUDIO_CONVERSATION("POSTMONTHCHAR")
			tlReturn += " " 
			tlReturn += sDate.nYear
		BREAK
		
		CASE LANGUAGE_CHINESE
		CASE LANGUAGE_CHINESE_SIMPLIFIED
			tlReturn = sDate.nYear 
			tlReturn += " " 
			tlReturn += GET_FILENAME_FOR_AUDIO_CONVERSATION("POSTYEARCHAR")
			tlReturn += " " 
			tlReturn += month
			tlReturn += " " 
			tlReturn += sDate.nDay
			tlReturn += " " 
			tlReturn += GET_FILENAME_FOR_AUDIO_CONVERSATION("POSTDAYCHAR")
		BREAK
		
		CASE LANGUAGE_FRENCH
			month = GET_MONTH_LABEL_LOWERCASE(sDate.nMonth)
			tlReturn = sDate.nDay 
			IF sDate.nDay = 1
				tlReturn += GET_FILENAME_FOR_AUDIO_CONVERSATION("POSTDAYCHAR")
			ENDIF
			tlReturn += " " 
			tlReturn += month
			tlReturn += " " 
			tlReturn += sDate.nYear
		BREAK
		
		CASE LANGUAGE_GERMAN
			tlReturn = sDate.nDay 
			tlReturn += ". " 
			tlReturn += month
			tlReturn += " " 
			tlReturn += sDate.nYear
		BREAK
		
		CASE LANGUAGE_ITALIAN
			month = GET_MONTH_LABEL_LOWERCASE(sDate.nMonth)
			tlReturn = sDate.nDay 
			tlReturn += " " 
			tlReturn += month
			tlReturn += " " 
			tlReturn += sDate.nYear
		BREAK
		
		CASE LANGUAGE_JAPANESE
			tlReturn = sDate.nYear 
			tlReturn += GET_FILENAME_FOR_AUDIO_CONVERSATION("POSTYEARCHAR")
			tlReturn += " " 
			tlReturn += month
			tlReturn += GET_FILENAME_FOR_AUDIO_CONVERSATION("POSTMONTHCHAR")
			tlReturn += " " 
			tlReturn += sDate.nDay
			tlReturn += GET_FILENAME_FOR_AUDIO_CONVERSATION("POSTDAYCHAR")
		BREAK
		
		CASE LANGUAGE_KOREAN
			tlReturn = sDate.nYear 
			tlReturn += GET_FILENAME_FOR_AUDIO_CONVERSATION("POSTYEARCHAR")
			tlReturn += " " 
			tlReturn += month
			tlReturn += " " 
			tlReturn += sDate.nDay
			tlReturn += GET_FILENAME_FOR_AUDIO_CONVERSATION("POSTDAYCHAR")
		BREAK
		
		CASE LANGUAGE_MEXICAN
			month = GET_MONTH_LABEL_LOWERCASE(sDate.nMonth)
			tlReturn = sDate.nDay 
			tlReturn += " " 
			tlReturn += GET_FILENAME_FOR_AUDIO_CONVERSATION("POSTDAYCHAR")
			tlReturn += " " 
			tlReturn += month
			tlReturn += " " 
			tlReturn += GET_FILENAME_FOR_AUDIO_CONVERSATION("POSTMONTHCHAR")
			tlReturn += " " 
			tlReturn += sDate.nYear
		BREAK
		
		CASE LANGUAGE_POLISH
			month = GET_MONTH_LABEL_LOWERCASE(sDate.nMonth)
			tlReturn = sDate.nDay 
			tlReturn += " " 
			tlReturn += month
			tlReturn += " " 
			tlReturn += sDate.nYear
		BREAK
		
		CASE LANGUAGE_RUSSIAN
			month = GET_MONTH_LABEL_LOWERCASE(sDate.nMonth)
			tlReturn = sDate.nDay 
			tlReturn += " " 
			tlReturn += month
			tlReturn += " " 
			tlReturn += sDate.nYear
			tlReturn += " " 
			tlReturn += GET_FILENAME_FOR_AUDIO_CONVERSATION("POSTYEARCHAR")
		BREAK
		
		CASE LANGUAGE_SPANISH
			month = GET_MONTH_LABEL_LOWERCASE(sDate.nMonth)
			tlReturn = sDate.nDay 
			tlReturn += " " 
			tlReturn += GET_FILENAME_FOR_AUDIO_CONVERSATION("POSTDAYCHAR")
			tlReturn += " " 
			tlReturn += month
			tlReturn += " " 
			tlReturn += GET_FILENAME_FOR_AUDIO_CONVERSATION("POSTMONTHCHAR")
			tlReturn += " " 
			tlReturn += sDate.nYear
		BREAK
		
		CASE LANGUAGE_ENGLISH
			tlReturn = month
			tlReturn += " " 
			tlReturn += sDate.nDay 
			tlReturn += " " 
			tlReturn += sDate.nYear
		BREAK
		
		
		DEFAULT
			tlReturn = month
			tlReturn += " " 
			tlReturn += sDate.nDay 
			tlReturn += " " 
			tlReturn += sDate.nYear
		BREAK
		
	ENDSWITCH

	
	PRINTLN("[SAVETRANS] GET_DATE_FOR_BANNED_ACCT_CREATION_DATE = ", tlReturn)
	RETURN tlReturn
ENDFUNC

/////////////////////////////////////////////
/////////MISSION START CORONA////////////////
/////////////////////////////////////////////


PROC DRAW_NUMBER_MISSION_START(INT NUMBER, INT iTeam, VECTOR Position, FLOAT fWidthPassed = 3.5)

	

	IF NUMBER > 0
		INT r, g, b
		GET_TEAM_CONTRAST_COLOUR(iTeam, r, g, b)
		IF NUMBER = 0
		ENDIF
		IF NUMBER < 10

			Position.z += (fWidthPassed/2.000)
			fWidthPassed -= fWidthPassed/1.500 
			
			VECTOR CamPos
			CAMERA_INDEX RenderCam =  GET_RENDERING_CAM() 
			IF DOES_CAM_EXIST(RenderCam)
				IF IS_GAMEPLAY_CAM_RENDERING()
					CamPos = GET_GAMEPLAY_CAM_COORD()
				ELSE
					CamPos = GET_CAM_COORD(RenderCam)
				ENDIF
			ENDIF
						
			VECTOR PosToCam = CamPos-Position
			
			MARKER_TYPE Marker
			SWITCH NUMBER
				CASE 0
					Marker = MARKER_NUM_0
				BREAK
				CASE 1
					Marker = MARKER_NUM_1
				BREAK
				CASE 2
					Marker = MARKER_NUM_2
				BREAK
				CASE 3
					Marker = MARKER_NUM_3
				BREAK
				CASE 4
					Marker = MARKER_NUM_4
				BREAK
				CASE 5
					Marker = MARKER_NUM_5
				BREAK
				CASE 6
					Marker = MARKER_NUM_6
				BREAK
				CASE 7
					Marker = MARKER_NUM_7
				BREAK
				CASE 8
					Marker = MARKER_NUM_8
				BREAK
				CASE 9
					Marker = MARKER_NUM_9
				BREAK
			ENDSWITCH
			
			DRAW_MARKER(Marker, Position, PosToCam, <<0,0,0>>, <<fWidthPassed, fWidthPassed, fWidthPassed>>, r, g, b, 100)
		ELSE
			FLOAT XScaler = fWidthPassed/3.5
//			
//			FLOAT XShiftTen = MissionStartSingleNumberWidth
//			FLOAT XShiftUnits = -MissionStartSingleNumberWidth
//			
//			FLOAT YShiftTen = MissionStartSingleNumberWidth
//			FLOAT YShiftUnits = -MissionStartSingleNumberWidth
			
			
			MARKER_TYPE MarkerUnits
			MARKER_TYPE MarkerTens
			SWITCH NUMBER
				CASE 10
					MarkerUnits = MARKER_NUM_0
					MarkerTens = MARKER_NUM_1
				BREAK
				CASE 11
					MarkerUnits = MARKER_NUM_1
					MarkerTens = MARKER_NUM_1
				BREAK
				CASE 12
					MarkerUnits = MARKER_NUM_2
					MarkerTens = MARKER_NUM_1
				BREAK
				CASE 13
					MarkerUnits = MARKER_NUM_3
					MarkerTens = MARKER_NUM_1
				BREAK
				CASE 14
					MarkerUnits = MARKER_NUM_4
					MarkerTens = MARKER_NUM_1
				BREAK
				CASE 15
					MarkerUnits = MARKER_NUM_5
					MarkerTens = MARKER_NUM_1
				BREAK
				CASE 16
					MarkerUnits = MARKER_NUM_6
					MarkerTens = MARKER_NUM_1
				BREAK
			ENDSWITCH
			
			VECTOR CamPos
			CAMERA_INDEX RenderCam =  GET_RENDERING_CAM() 
			IF DOES_CAM_EXIST(RenderCam)
				IF IS_GAMEPLAY_CAM_RENDERING()
					CamPos = GET_GAMEPLAY_CAM_COORD()
				ELSE
					CamPos = GET_CAM_COORD(RenderCam)
				ENDIF
			ENDIF
						
			VECTOR PosToCam = CamPos-Position	
			PosToCam = NORMALISE_VECTOR(PosToCam)
			VECTOR RightDir =   <<PosToCam.y, -PosToCam.x, 0>>
			VECTOR LeftDir =  <<-PosToCam.y, PosToCam.x, 0>>
			
			RightDir = NORMALISE_VECTOR(RightDir)
			LeftDir = NORMALISE_VECTOR(LeftDir)
			
			VECTOR RightPos = Position+(RightDir*(XScaler/2))
			VECTOR LeftPos = Position+(LeftDir*(XScaler/2))
		
			
//			XShiftTen /= XScaler
//			XShiftUnits /= XScaler
//			
//			Position1.x += XShiftTen
//			Position2.x += XShiftUnits
//			
//			Position1.y += XShiftTen
//			Position2.y += XShiftUnits
//			
			
			RightPos.z += (fWidthPassed/1.5)
			LeftPos.z += (fWidthPassed/1.5)
			fWidthPassed  -= fWidthPassed/2.00
			
			
//			#IF IS_DEBUG_BUILD
//				fWidthPassed += MissionStartDoubleNumberWidth
////				Position1 += MissionStartDOUBLENumberPos1
////				Position2 += MissionStartDOUBLENumberPos2
//			#ENDIF
				
			DRAW_MARKER(MarkerTens, RightPos, PosToCam, <<0,0,0>>, <<fWidthPassed, fWidthPassed, fWidthPassed>>, r, g, b, 100)
			DRAW_MARKER(MarkerUnits, LeftPos, PosToCam, <<0,0,0>>, <<fWidthPassed, fWidthPassed, fWidthPassed>>, r, g, b, 100)
		ENDIF

	ENDIF
	
ENDPROC




PROC RUN_MISSION_START_CORONA(VECTOR Position, INT iTeam, /*INT NumberRemaining,*/ FLOAT fWidthPassed = 3.5)

		
//	MPGlobalsHudPosition.fMissionCorona_CoronaHeight = MissionStartSingleNumberWidth	
	MPGlobalsHudPosition.fMissionCorona_CoronaWidth = fWidthPassed
//	MPGlobalsHudPosition.fMissionCorona_Scale = fWidthPassed-0.500//-1.0
	
	//VECTOR vDirection = <<0.0, 0.0, -1>>
	
	//VECTOR vSide = NORMALISE_VECTOR(<<1.0, 0.5, 0.0>>) 
		
	VECTOR CoronaPos = (Position + <<0.0, 0.0, 1.5>>)
	GET_GROUND_Z_FOR_3D_COORD(CoronaPos, CoronaPos.z)	
	//CoronaPos.z += (fWidthPassed/13.810)
		
//	GET_GROUND_Z_FOR_3D_COORD(Position, Position.z)	

	VECTOR Scale = <<MPGlobalsHudPosition.fMissionCorona_CoronaWidth, MPGlobalsHudPosition.fMissionCorona_CoronaWidth, MPGlobalsHudPosition.fMissionCorona_CoronaWidth/7.000>>
	INT R, G, B
	SWITCH iTeam
		CASE FMMC_ROCKSTAR_CREATOR_ID
		CASE FMMC_ROCKSTAR_CANDIDATE_CREATOR_ID
		CASE FMMC_ROCKSTAR_HIDEOUT_CREATOR_ID
			GET_TEAM_RGB_COLOUR(-1, R, G, B)
			//DRAW_NUMBER_MISSION_START(NumberRemaining,iTeam, Position, fWidthPassed)
			DRAW_MARKER(MARKER_CYLINDER, CoronaPos, <<0,0,0>>, <<0,0,0>>, Scale, R, G, B )
		//	REGISTER_MARKER_DECAL(DECAL_RSID_LOGO_LOST_WHITE, Position, vDirection, vSide, MPGlobalsHudPosition.fMissionCorona_Scale, MPGlobalsHudPosition.fMissionCorona_Scale, 1.0)
		BREAK
	
	ENDSWITCH
	
ENDPROC






//
//BOOL InitRankDistance
//FLOAT RankNumXOffsetDistance, RankNumYOffsetDistance
//////
//////
//TEXT_STYLE RankStyle
//FLOAT WidgetDistanceAmount
//SPRITE_PLACEMENT RankSizeOffsetDistance
//
//FLOAT DISTANCE_NUMBERMULT_Y_TENS = 0.159
//FLOAT DISTANCE_NUMBERMULT_W_TENS = 1
//FLOAT DISTANCE_NUMBERMULT_Y_UNITS = 0.150
//FLOAT DISTANCE_NUMBERMULT_W_UNITS = 1
//
//FLOAT DISTANCE_NUMBERDIV_Y_TENS = 1.000
//FLOAT DISTANCE_NUMBERDIV_W_TENS = 0.186
//FLOAT DISTANCE_NUMBERDIV_Y_UNITS = 1.00
//FLOAT DISTANCE_NUMBERDIV_W_UNITS = 0.179













ENUM TERRITORYBOX_RECT
	TERRITORYBOX_RECT_COLOUREDTITLE = 0,
	TERRITORYBOX_RECT_FIRSTDIVIDER,
	TERRITORYBOX_RECT_TERRITORYNAME,
	TERRITORYBOX_RECT_SECONDDIVIDER,
	TERRITORYBOX_RECT_DESCRIPTION
ENDENUM

ENUM TERRITORYBOX_TEXT
	TERRITORYBOX_TEXT_COLOUREDTITLE = 0,
	TERRITORYBOX_TEXT_TERRITORYNAME,
	TERRITORYBOX_TEXT_DESCRIPTION
ENDENUM

STRUCT TERRITORY_BATTLE_BOX
	TEXT_PLACEMENT TerritoryBoxText[3]
	RECT TerrtoriyBoxRect[5]
	TEXT_STYLE MainTitleStyle
	TEXT_STYLE NameTextStyle
	TEXT_STYLE DescriptionTextStyle

	#IF IS_DEBUG_BUILD
		BOOL tb_InitialiseVariables
		TEXT_PLACEMENT TerritoryBoxTextWidget[3]
		RECT TerrtoriyBoxRectWidget[5]

		FLOAT iDescriptionWrapEND, iDescriptionWrapSTART, TextTitleScale
	#ENDIF
ENDSTRUCT


ENUM BEFORERACEBOX_RECT
	BEFORERACEBOX_RECT_TITLE = 0,
	BEFORERACEBOX_RECT_TIME
ENDENUM

ENUM BEFORERACEBOX_TEXT
	BEFORERACEBOX_TEXT_TITLE = 0,
	BEFORERACEBOX_TEXT_PLAYERNAME,
	BEFORERACEBOX_TEXT_PLAYERTIME
ENDENUM

ENUM BEFORERACEBOX_SPRITE
	BEFORERACEBOX_SPRITE_CATEGORY = 0,
	BEFORERACEBOX_SPRITE_SOCIALCLUB
ENDENUM

ENUM BESTRACETIMEBOX
	BESTRACETIMEBOX_NONE,
	BESTRACETIMEBOX_PERSONALBEST,
	BESTRACETIMEBOX_CREWBEST,
	BESTRACETIMEBOX_WORLDBEST,
	BESTRACETIMEBOX_FULL_SCREEN
ENDENUM

STRUCT BEFORERACESTRUCT

	TEXT_PLACEMENT RaceBox_Text[3]
	SPRITE_PLACEMENT RaceBox_Sprite[2]
	RECT RaceBox_Rect[2]

	TEXT_STYLE aTextStyle
	
	SCALEFORM_INDEX aMovie
	SCALEFORM_CREWTAG aCrewDetails

	#IF IS_DEBUG_BUILD
		BOOL rb_InitialiseVariables
		TEXT_PLACEMENT RaceBoxTextWidget[3]
		RECT RaceBoxRectWidget[2]
		SPRITE_PLACEMENT RaceBoxSpriteWidget[2]

		FLOAT iDescriptionWrapEND, iDescriptionWrapSTART, TextTitleScale
	#ENDIF
ENDSTRUCT

PROC UNLOAD_RACE_BEST_TIME_BOX(BEFORERACESTRUCT& Details)

	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(Details.aMovie)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPLeaderboard")

ENDPROC

PROC DRAW_RACE_BEST_TIME_BOX(BESTRACETIMEBOX BoxType, INT iTime,STRING Name, BEFORERACESTRUCT& Details, FLOAT OffsetX = 0.0, FLOAT OffsetY = 0.0)

	
//	#IF IS_DEBUG_BUILD
//		IF Details.rb_InitialiseVariables = FALSE
//			
//			START_WIDGET_GROUP("BEFORE RACE BOX")
//				CREATE_A_TEXT_PLACEMENT_WIDGET(Details.RaceBoxTextWidget[BEFORERACEBOX_TEXT_TITLE], "BEFORERACEBOX_TEXT_TITLE")
//				CREATE_A_TEXT_PLACEMENT_WIDGET(Details.RaceBoxTextWidget[BEFORERACEBOX_TEXT_PLAYERNAME], "BEFORERACEBOX_TEXT_PLAYERNAME")
//				CREATE_A_TEXT_PLACEMENT_WIDGET(Details.RaceBoxTextWidget[BEFORERACEBOX_TEXT_PLAYERTIME], "BEFORERACEBOX_TEXT_PLAYERTIME")
//			
//				CREATE_A_RECT_PLACEMENT_WIDGET(Details.RaceBoxRectWidget[BEFORERACEBOX_RECT_TITLE], "BEFORERACEBOX_RECT_TITLE")
//				CREATE_A_RECT_PLACEMENT_WIDGET(Details.RaceBoxRectWidget[BEFORERACEBOX_RECT_TIME], "BEFORERACEBOX_RECT_TIME")
//				
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(Details.RaceBoxSpriteWidget[BEFORERACEBOX_SPRITE_CATEGORY], "BEFORERACEBOX_SPRITE_CATEGORY")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(Details.RaceBoxSpriteWidget[BEFORERACEBOX_SPRITE_SOCIALCLUB], "BEFORERACEBOX_SPRITE_SOCIALCLUB")
//
//				ADD_WIDGET_FLOAT_SLIDER("iDescriptionWrapEND", Details.iDescriptionWrapEND, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("iDescriptionWrapSTART", Details.iDescriptionWrapSTART, -1, 1, 0.001)
//				ADD_WIDGET_FLOAT_SLIDER("TextTitleScale", Details.TextTitleScale, -1, 1, 0.001)
//			STOP_WIDGET_GROUP()
//			
//			
//			
//			
//			
//			Details.rb_InitialiseVariables = TRUE
//		ENDIF
//	#ENDIF
//	
	

	
	
	REQUEST_STREAMED_TEXTURE_DICT("MPLeaderboard")
	IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPLeaderboard")
	
		SET_STANDARD_SMALL_HUD_TEXT(Details.aTextStyle)
		Details.aTextStyle.wrapEndX = 0.250
		#IF IS_DEBUG_BUILD
		Details.aTextStyle.YScale += Details.TextTitleScale
		Details.aTextStyle.WrapEndX += Details.iDescriptionWrapEND
		Details.aTextStyle.WrapStartX += Details.iDescriptionWrapSTART
		#ENDIF
		SET_TEXT_STYLE(Details.aTextStyle)
		
		GAMER_HANDLE gamerHandle
		NETWORK_CLAN_DESC PlayerClan
		TEXT_LABEL_7 CrewTag
		
		IF OffsetY = 0.0
			OffsetY = GET_CUSTOM_MENU_FINAL_Y_COORD()
			OffsetY += -0.040
		ENDIF
		
		SWITCH BoxType
			CASE BESTRACETIMEBOX_PERSONALBEST
				SET_TEXT_PLACEMENT(Details.RaceBox_Text[BEFORERACEBOX_TEXT_TITLE], 0.071+OffsetX, 0.057, OffsetY)
				SET_TEXT_PLACEMENT(Details.RaceBox_Text[BEFORERACEBOX_TEXT_PLAYERNAME], 0.056+OffsetX, 0.091, OffsetY)
				SET_TEXT_PLACEMENT(Details.RaceBox_Text[BEFORERACEBOX_TEXT_PLAYERTIME], 0.060+OffsetX, 0.091, OffsetY)

				SET_RECT_PLACEMENT(Details.RaceBox_Rect[BEFORERACEBOX_RECT_TITLE], GENERIC_BOX_X_POSITION+OffsetX, 0.068, GENERIC_SLOT_WIDTH, GENERIC_SLOT_HEIGHT, 0,0,0,255, OffsetY)
				SET_RECT_PLACEMENT(Details.RaceBox_Rect[BEFORERACEBOX_RECT_TIME], GENERIC_BOX_X_POSITION+OffsetX, 0.103, GENERIC_SLOT_WIDTH, GENERIC_SLOT_HEIGHT, 50,50,50,255, OffsetY)
				
				SET_SPRITE_PLACEMENT(Details.RaceBox_Sprite[BEFORERACEBOX_SPRITE_CATEGORY], 0.062+OffsetX, 0.067, 0.022, 0.033, 255,255,255,255, OffsetY)
				SET_SPRITE_PLACEMENT(Details.RaceBox_Sprite[BEFORERACEBOX_SPRITE_SOCIALCLUB], 0.24+OffsetX, 0.067, 0.029, 0.055, 255,255,255,255, OffsetY)
			BREAK
			CASE BESTRACETIMEBOX_CREWBEST
				
				
				gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
				CrewTag = GET_PLAYER_CLAN_TAG(gamerHandle)
	
				PRIVATE_ADD_SCALEFORM_CREWTAG_CREW_STRING(CrewTag , Details.aCrewDetails)
				PRIVATE_ADD_SCALEFORM_CREWTAG_IS_PRIVATE(IS_PLAYER_CLAN_PRIVATE(gamerHandle), Details.aCrewDetails)
				PlayerClan = GET_PLAYER_CREW(PLAYER_ID())
				PRIVATE_ADD_SCALEFORM_CREWTAG_IS_ROCKSTAR(IS_CREW_A_ROCKSTAR_CREW(PlayerClan) , Details.aCrewDetails)
				PRIVATE_ADD_SCALEFORM_CREWTAG_IS_FOUNDER(IS_PLAYER_CLAN_LEADER(gamerHandle) , Details.aCrewDetails)
			
				SET_TEXT_PLACEMENT(Details.RaceBox_Text[BEFORERACEBOX_TEXT_TITLE], 0.097+OffsetX, 0.057, OffsetY)
				SET_TEXT_PLACEMENT(Details.RaceBox_Text[BEFORERACEBOX_TEXT_PLAYERNAME], 0.056+OffsetX, 0.091, OffsetY)
				SET_TEXT_PLACEMENT(Details.RaceBox_Text[BEFORERACEBOX_TEXT_PLAYERTIME], 0.060+OffsetX, 0.091, OffsetY)

				SET_RECT_PLACEMENT(Details.RaceBox_Rect[BEFORERACEBOX_RECT_TITLE], GENERIC_BOX_X_POSITION+OffsetX, 0.068, GENERIC_SLOT_WIDTH, GENERIC_SLOT_HEIGHT, 0,0,0,255, OffsetY)
				SET_RECT_PLACEMENT(Details.RaceBox_Rect[BEFORERACEBOX_RECT_TIME], GENERIC_BOX_X_POSITION+OffsetX, 0.103, GENERIC_SLOT_WIDTH, GENERIC_SLOT_HEIGHT, 50,50,50,255, OffsetY)
				
				SET_SPRITE_PLACEMENT(Details.RaceBox_Sprite[BEFORERACEBOX_SPRITE_CATEGORY], 0.087+OffsetX, 0.071, 0.058, 0.026, 255,255,255,255, OffsetY)
				SET_SPRITE_PLACEMENT(Details.RaceBox_Sprite[BEFORERACEBOX_SPRITE_SOCIALCLUB], 0.24+OffsetX, 0.067, 0.029, 0.055, 255,255,255,255, OffsetY)
			BREAK
			CASE BESTRACETIMEBOX_WORLDBEST
				SET_TEXT_PLACEMENT(Details.RaceBox_Text[BEFORERACEBOX_TEXT_TITLE], 0.071+OffsetX, 0.057, OffsetY)
				SET_TEXT_PLACEMENT(Details.RaceBox_Text[BEFORERACEBOX_TEXT_PLAYERNAME], 0.056+OffsetX, 0.091, OffsetY)
				SET_TEXT_PLACEMENT(Details.RaceBox_Text[BEFORERACEBOX_TEXT_PLAYERTIME], 0.060+OffsetX, 0.091, OffsetY)

				SET_RECT_PLACEMENT(Details.RaceBox_Rect[BEFORERACEBOX_RECT_TITLE], GENERIC_BOX_X_POSITION+OffsetX, 0.068, GENERIC_SLOT_WIDTH, GENERIC_SLOT_HEIGHT, 0,0,0,255, OffsetY)
				SET_RECT_PLACEMENT(Details.RaceBox_Rect[BEFORERACEBOX_RECT_TIME], GENERIC_BOX_X_POSITION+OffsetX, 0.103, GENERIC_SLOT_WIDTH, GENERIC_SLOT_HEIGHT, 50,50,50,255, OffsetY)
				
				SET_SPRITE_PLACEMENT(Details.RaceBox_Sprite[BEFORERACEBOX_SPRITE_CATEGORY], 0.062+OffsetX, 0.067, 0.022, 0.033, 255,255,255,255, OffsetY)
				SET_SPRITE_PLACEMENT(Details.RaceBox_Sprite[BEFORERACEBOX_SPRITE_SOCIALCLUB], 0.24+OffsetX, 0.067, 0.029, 0.055, 255,255,255,255, OffsetY)
			BREAK
			CASE BESTRACETIMEBOX_FULL_SCREEN
				SET_SPRITE_PLACEMENT(Details.RaceBox_Sprite[BEFORERACEBOX_SPRITE_CATEGORY], 0.5, 0.45, 0.75, 0.75, 255,255,255,255)
				DRAW_2D_SPRITE("MPLeaderboard", "DEBUG_BOX_lead_B_LARGE", Details.RaceBox_Sprite[BEFORERACEBOX_SPRITE_CATEGORY])
				HIDE_HUD_AND_RADAR_THIS_FRAME()
			BREAK
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
		UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(Details.RaceBox_Text[BEFORERACEBOX_TEXT_TITLE], Details.RaceBoxTextWidget[BEFORERACEBOX_TEXT_TITLE])
		UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(Details.RaceBox_Text[BEFORERACEBOX_TEXT_PLAYERNAME], Details.RaceBoxTextWidget[BEFORERACEBOX_TEXT_PLAYERNAME])
		UPDATE_TEXT_PLACEMENT_WIDGET_VALUE(Details.RaceBox_Text[BEFORERACEBOX_TEXT_PLAYERTIME], Details.RaceBoxTextWidget[BEFORERACEBOX_TEXT_PLAYERTIME])

		UPDATE_RECT_WIDGET_VALUE(Details.RaceBox_Rect[BEFORERACEBOX_RECT_TITLE], Details.RaceBoxRectWidget[BEFORERACEBOX_RECT_TITLE])
		UPDATE_RECT_WIDGET_VALUE(Details.RaceBox_Rect[BEFORERACEBOX_RECT_TIME], Details.RaceBoxRectWidget[BEFORERACEBOX_RECT_TIME])
	
		UPDATE_SPRITE_WIDGET_VALUE(Details.RaceBox_Sprite[BEFORERACEBOX_SPRITE_CATEGORY], Details.RaceBoxSpriteWidget[BEFORERACEBOX_SPRITE_CATEGORY])
		UPDATE_SPRITE_WIDGET_VALUE(Details.RaceBox_Sprite[BEFORERACEBOX_SPRITE_SOCIALCLUB], Details.RaceBoxSpriteWidget[BEFORERACEBOX_SPRITE_SOCIALCLUB])

		#ENDIF
		
		
		
		
		
		IF BoxType != BESTRACETIMEBOX_FULL_SCREEN
			
			DRAW_RECTANGLE(Details.RaceBox_Rect[BEFORERACEBOX_RECT_TITLE])
			DRAW_RECTANGLE(Details.RaceBox_Rect[BEFORERACEBOX_RECT_TIME])
			
			TEXT_COMPONENT_TIME_FORMAT aFormat = TIME_FORMAT_MINUTES|TIME_FORMAT_SECONDS|TIME_FORMAT_MILLISECONDS
			
			DRAW_TEXT_TIMER(Details.RaceBox_Text[BEFORERACEBOX_TEXT_PLAYERTIME], Details.aTextStyle, iTime, aFormat, "", FALSE, TRUE)


			DRAW_TEXT_WITH_PLAYER_NAME(Details.RaceBox_Text[BEFORERACEBOX_TEXT_PLAYERNAME], Details.aTextStyle, Name,"", HUD_COLOUR_WHITE, FONT_LEFT)
			DRAW_TEXT_WITH_ALIGNMENT(Details.RaceBox_Text[BEFORERACEBOX_TEXT_TITLE], Details.aTextStyle, "TIMER_BESLAP", FALSE, FALSE)

			
			
			DRAW_2D_SPRITE("MPLeaderboard", "Leaderboard_SocialClub_Icon", Details.RaceBox_Sprite[BEFORERACEBOX_SPRITE_SOCIALCLUB])
			
			SWITCH BoxType
				CASE BESTRACETIMEBOX_PERSONALBEST
					DRAW_2D_SPRITE("MPLeaderboard", "Leaderboard_Players_Icon", Details.RaceBox_Sprite[BEFORERACEBOX_SPRITE_CATEGORY])
				BREAK
				CASE BESTRACETIMEBOX_CREWBEST
					
					RUN_SCALEFORM_CREWTAG(Details.aMovie, Details.RaceBox_Sprite[BEFORERACEBOX_SPRITE_CATEGORY], Details.aCrewDetails, SHOULD_REFRESH_SCALEFORM_CREWTAG(Details.aCrewDetails))

				BREAK
				CASE BESTRACETIMEBOX_WORLDBEST
					DRAW_2D_SPRITE("MPLeaderboard", "Leaderboard_Globe_Icon", Details.RaceBox_Sprite[BEFORERACEBOX_SPRITE_CATEGORY])
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	

ENDPROC

ENUM CREW_RANK_ORDER
	CREW_RANK_NONE  = -1,

	CREW_RANK_0  = 0,
	CREW_RANK_1,
	CREW_RANK_2,
	CREW_RANK_3,
	CREW_RANK_4,
	CREW_RANK_5
ENDENUM



//#IF IS_DEBUG_BUILD
//BOOL CrewTagWidget 
//TEXT_PLACEMENT WidgetCrewPlacement
//TEXT_PLACEMENT WidgetCrewIconPlacement
//TEXT_PLACEMENT WidgetCrewRankCharacterPlacement
//TEXT_STYLE WidgetCrewStyle
//TEXT_STYLE WidgetCrewIconStyle
//TEXT_STYLE WidgetCrewIconBackgroundStyle
//TEXT_STYLE WidgetCrewRankCharacter
//
//BOOL bUseWidgetLogic
//BOOL WidgetIsRockstar
//BOOL WidgetIsSystem
//INT iRankOrder
//FLOAT PositionalFactorX
//FLOAT PositionalRankFactorX, PositionalRankFactorY
//
//FLOAT ScaleFactor
//#ENDIF

PROC DRAW_CREW_TAG_GAMER(GAMER_HANDLE& aPlayer, FLOAT ScreenX, FLOAT ScreenY, FLOAT ScaleMultiplyer = 1.0)

	BOOL IsRockstar
	BOOL IsSystem
	INT iRankPosition
	
	
	
	TEXT_LABEL_31 tl31_CrewIcon = ""
	TEXT_LABEL_31 tl31_CrewIconBackground = ""
	TEXT_LABEL_31 tl31_CrewText = ""
	TEXT_LABEL_31 tl31_CrewRank = ""
	
	BOOL UseDebug = FALSE

//	#IF IS_DEBUG_BUILD
//		IF bUseWidgetLogic = TRUE
//			UseDebug = TRUE	
//		ENDIF
//	#ENDIF
	
	IF UseDebug = FALSE
		IF IS_PLAYER_IN_ACTIVE_CLAN(aPlayer) = FALSE
			EXIT
		ENDIF
	ENDIF
	
	
	
	TEXT_STYLE TSCrewStyle
	TEXT_STYLE TSCrewIconStyle
	TEXT_STYLE TSCrewIconStyleBackground
	TEXT_STYLE TSCrewRankCharacter
	TEXT_PLACEMENT CrewPlacement
	TEXT_PLACEMENT CrewIconPlacement
	TEXT_PLACEMENT CrewRankCharacter

//	#IF IS_DEBUG_BUILD
//	IF CrewTagWidget = FALSE
//		START_WIDGET_GROUP("DRAW_CREW_TAG")
//			ADD_WIDGET_BOOL("bUseWidgetLogic", bUseWidgetLogic)
//			CREATE_A_TEXT_PLACEMENT_WIDGET(WidgetCrewPlacement, "WidgetCrewPlacement")
////			CREATE_A_TEXT_PLACEMENT_WIDGET(WidgetCrewtagCrewIconPlacement, "WidgetCrewIconPlacement")
//			CREATE_A_TEXT_PLACEMENT_WIDGET(WidgetCrewRankCharacterPlacement, "WidgetCrewRankCharacterPlacement")
//			CREATE_A_TEXT_STYLE_WIGET(WidgetCrewStyle, "WidgetCrewStyle")
//			CREATE_A_TEXT_STYLE_WIGET(WidgetCrewIconStyle, "WidgetCrewIconStyle")
//			CREATE_A_TEXT_STYLE_WIGET(WidgetCrewIconBackgroundStyle, "WidgetCrewIconBackgroundStyle")
//			CREATE_A_TEXT_STYLE_WIGET(WidgetCrewRankCharacter, "WidgetCrewRankCharacter")
//			ADD_WIDGET_BOOL("Is a Rockstar", WidgetIsRockstar)
//			ADD_WIDGET_BOOL("Is System", WidgetIsSystem)
//			ADD_WIDGET_INT_SLIDER("Rank Level", iRankOrder, 0, 6, 1)
//			ADD_WIDGET_FLOAT_SLIDER("PositionalFactorX", PositionalFactorX, -100, 100, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("ScaleFactor", ScaleFactor, -10, 10, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("PositionalRankFactorX", PositionalRankFactorX, -100, 100, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("PositionalRankFactorY", PositionalRankFactorY, -100, 100, 0.001)
//
//
//		STOP_WIDGET_GROUP()
//	
//		CrewTagWidget = TRUE
//	ENDIF
//	#ENDIF
	
	SET_TEXT_PLACEMENT(CrewPlacement, ScreenX, ScreenY)
	SET_TEXT_PLACEMENT(CrewIconPlacement, ScreenX, ScreenY)
	SET_TEXT_PLACEMENT(CrewRankCharacter, ScreenX, ScreenY)
	
	
//	#IF IS_DEBUG_BUILD
//	ScaleMultiplyer += ScaleFactor
//	#ENDIF
	
	SET_STANDARD_SMALL_HUD_TEXT(TSCrewStyle)
	SET_STANDARD_SMALL_HUD_TEXT(TSCrewIconStyle)
	SET_STANDARD_SMALL_HUD_TEXT(TSCrewIconStyleBackground)
	SET_STANDARD_SMALL_HUD_TEXT(TSCrewRankCharacter)

	TSCrewStyle.YScale += 0.24*ScaleMultiplyer
	TSCrewStyle.WrapEndX += 0
	TSCrewStyle.WrapStartX += 0
	TSCrewStyle.r += 0
	TSCrewStyle.g += 0
	TSCrewStyle.b += 0
	TSCrewStyle.a += 0
	
	TSCrewIconStyle.YScale += 0.24*ScaleMultiplyer
	TSCrewIconStyle.WrapEndX += 0
	TSCrewIconStyle.WrapStartX += 0
	TSCrewIconStyle.r += 0
	TSCrewIconStyle.g += 0
	TSCrewIconStyle.b += 0
	TSCrewIconStyle.a += 0
	
	TSCrewIconStyleBackground.YScale += 0.24*ScaleMultiplyer
	TSCrewIconStyleBackground.WrapEndX +=0
	TSCrewIconStyleBackground.WrapStartX +=0
	TSCrewIconStyleBackground.r += 0
	TSCrewIconStyleBackground.g += 0
	TSCrewIconStyleBackground.b += 0
	TSCrewIconStyleBackground.a += 0
	
	TSCrewRankCharacter.YScale += 0.24*ScaleMultiplyer
	TSCrewRankCharacter.WrapEndX += 0
	TSCrewRankCharacter.WrapStartX += 0
	TSCrewRankCharacter.r += 0
	TSCrewRankCharacter.g += 0
	TSCrewRankCharacter.b += 0
	TSCrewRankCharacter.a += 0
	
	

	SET_TEXT_BLACK(TSCrewStyle)
	TSCrewStyle.aFont = FONT_ROCKSTAR_TAG
	TSCrewIconStyle.aFont = FONT_ROCKSTAR_TAG
	TSCrewIconStyleBackground.aFont = FONT_ROCKSTAR_TAG
	TSCrewRankCharacter.aFont = FONT_ROCKSTAR_TAG
//	
//	#IF IS_DEBUG_BUILD
//	TSCrewStyle.YScale += WidgetCrewStyle.YScale
//	TSCrewStyle.WrapEndX += WidgetCrewStyle.WrapEndX
//	TSCrewStyle.WrapStartX += WidgetCrewStyle.WrapStartX
//	TSCrewStyle.r += WidgetCrewStyle.r
//	TSCrewStyle.g += WidgetCrewStyle.g
//	TSCrewStyle.b += WidgetCrewStyle.b
//	TSCrewStyle.a += WidgetCrewStyle.a
//	
//	TSCrewIconStyle.YScale += WidgetCrewIconStyle.YScale
//	TSCrewIconStyle.WrapEndX += WidgetCrewIconStyle.WrapEndX
//	TSCrewIconStyle.WrapStartX += WidgetCrewIconStyle.WrapStartX
//	TSCrewIconStyle.r += WidgetCrewIconStyle.r
//	TSCrewIconStyle.g += WidgetCrewIconStyle.g
//	TSCrewIconStyle.b += WidgetCrewIconStyle.b
//	TSCrewIconStyle.a += WidgetCrewIconStyle.a
//	
//	TSCrewIconStyleBackground.YScale += WidgetCrewIconBackgroundStyle.YScale
//	TSCrewIconStyleBackground.WrapEndX += WidgetCrewIconBackgroundStyle.WrapEndX
//	TSCrewIconStyleBackground.WrapStartX += WidgetCrewIconBackgroundStyle.WrapStartX
//	TSCrewIconStyleBackground.r += WidgetCrewIconBackgroundStyle.r
//	TSCrewIconStyleBackground.g += WidgetCrewIconBackgroundStyle.g
//	TSCrewIconStyleBackground.b += WidgetCrewIconBackgroundStyle.b
//	TSCrewIconStyleBackground.a += WidgetCrewIconBackgroundStyle.a
//	
//	TSCrewRankCharacter.YScale += WidgetCrewRankCharacter.YScale
//	TSCrewRankCharacter.WrapEndX += WidgetCrewRankCharacter.WrapEndX
//	TSCrewRankCharacter.WrapStartX += WidgetCrewRankCharacter.WrapStartX
//	TSCrewRankCharacter.r += WidgetCrewRankCharacter.r
//	TSCrewRankCharacter.g += WidgetCrewRankCharacter.g
//	TSCrewRankCharacter.b += WidgetCrewRankCharacter.b
//	TSCrewRankCharacter.a += WidgetCrewRankCharacter.a
//	
//	#ENDIF
//	
	
	
//	CrewPlacement.x += 0.017*PositionalFactorX
	CrewPlacement.x += ((0.005*ScaleMultiplyer)+0.005)
	CrewPlacement.y += 0.0
	CrewIconPlacement.x += 0
	CrewIconPlacement.y +=  0
	CrewRankCharacter.x += 0
	CrewRankCharacter.y += 0//0.500
	
//	#IF IS_DEBUG_BUILD
//		CrewPlacement.x += WidgetCrewPlacement.x
//		CrewPlacement.y += WidgetCrewPlacement.y
//		CrewIconPlacement.x += WidgetCrewIconPlacement.x
//		CrewIconPlacement.y += WidgetCrewIconPlacement.y
//		CrewRankCharacter.x += WidgetCrewRankCharacterPlacement.x
//		CrewRankCharacter.y += WidgetCrewRankCharacterPlacement.y	
//	#ENDIF
	
	

	IF UseDebug
	
//		#IF IS_DEBUG_BUILD
//	
//		IF WidgetIsRockstar
//			tl31_CrewText = "@" 
//			tl31_CrewText += "EDI"
//
//		ELSE
//			tl31_CrewText = "NOOB"
//		ENDIF
//		
//		CREW_RANK_ORDER CrewIndex = INT_TO_ENUM(CREW_RANK_ORDER, iRankOrder)
//		
//		SWITCH CrewIndex
//		
//			
//		
//			CASE CREW_RANK_0
//				
//				IF WidgetIsSystem = FALSE
//					tl31_CrewIcon += "." 
//					tl31_CrewIconBackground += "/"
//					tl31_CrewRank += "€"
//				ELSE
//					tl31_CrewIcon += ","
//					tl31_CrewIconBackground +=   "-" 
//				ENDIF
//			BREAK
//			CASE CREW_RANK_1
//
//				IF WidgetIsSystem = FALSE
//					tl31_CrewIcon += "." 
//					tl31_CrewIconBackground += "/"
//					tl31_CrewRank += "£"
//				ELSE
//					tl31_CrewIcon += ","
//					tl31_CrewIconBackground +=   "-" 
//				ENDIF
//			BREAK
//			CASE CREW_RANK_2
//				
//				
//				IF WidgetIsSystem = FALSE
//					tl31_CrewIcon += "." 
//					tl31_CrewIconBackground += "/"
//					tl31_CrewRank += "}"
//				ELSE
//					tl31_CrewIcon += ","
//					tl31_CrewIconBackground +=   "-" 
//				ENDIF
//			BREAK
//			CASE CREW_RANK_3
//				
//				
//				IF WidgetIsSystem = FALSE
//					tl31_CrewIcon += "." 
//					tl31_CrewIconBackground += "/"
//					tl31_CrewRank += "|"
//				ELSE
//					tl31_CrewIcon += ","
//					tl31_CrewIconBackground +=   "-"  
//				ENDIF
//			BREAK
//			CASE CREW_RANK_4			
//				IF WidgetIsSystem = FALSE
//					tl31_CrewIcon += "." 
//					tl31_CrewIconBackground += "/"
//					tl31_CrewRank += "{"
//					
//				ELSE
//					tl31_CrewIcon += ","
//					tl31_CrewIconBackground +=   "-" 
//				ENDIF
//			BREAK
//			CASE CREW_RANK_5
//				IF WidgetIsSystem = FALSE
//					tl31_CrewIcon += "." 
//					tl31_CrewIconBackground += "/"
//					tl31_CrewRank += ""
//					
//				ELSE
//					tl31_CrewIcon += ","
//					tl31_CrewIconBackground +=   "-" 
//				ENDIF
//			BREAK
//			
//			
//		
//		ENDSWITCH
//		
//		#ENDIF

	ELSE
		
		IF IsRockstar
			tl31_CrewText = "@"
			
		ELSE
			tl31_CrewText += "" 
		ENDIF
		
		

		
		IsSystem = IS_PLAYER_CLAN_SYSTEM(aPlayer)
		
		
		iRankPosition = GET_PLAYER_CLAN_RANK_LEVEL(aPlayer)
		
		IsRockstar = IS_PLAYER_IN_A_ROCKSTAR_CREW(aPlayer)
		TEXT_LABEL_7 tl7_ClanTag =  GET_PLAYER_CLAN_TAG(aPlayer)
		tl31_CrewText  +=tl7_ClanTag

		
		
		CREW_RANK_ORDER CrewIndex = INT_TO_ENUM(CREW_RANK_ORDER, iRankPosition)
		
		SWITCH CrewIndex
		
			CASE CREW_RANK_0
				
				IF IsSystem = FALSE
					tl31_CrewIcon += "." 
					tl31_CrewIconBackground += "/"
					tl31_CrewRank += "€"
				ELSE
					tl31_CrewIcon += ","
					tl31_CrewIconBackground +=   "-" 
				ENDIF
			BREAK
			CASE CREW_RANK_1

				IF IsSystem = FALSE
					tl31_CrewIcon += "." 
					tl31_CrewIconBackground += "/"
					tl31_CrewRank += "£"
				ELSE
					tl31_CrewIcon += ","
					tl31_CrewIconBackground +=   "-" 
				ENDIF
			BREAK
			CASE CREW_RANK_2
				
				
				IF IsSystem = FALSE
					tl31_CrewIcon += "." 
					tl31_CrewIconBackground += "/"
					tl31_CrewRank += "}"
				ELSE
					tl31_CrewIcon += ","
					tl31_CrewIconBackground +=   "-" 
				ENDIF
			BREAK
			CASE CREW_RANK_3
				
				
				IF IsSystem = FALSE
					tl31_CrewIcon += "." 
					tl31_CrewIconBackground += "/"
					tl31_CrewRank += "|"
				ELSE
					tl31_CrewIcon += ","
					tl31_CrewIconBackground +=   "-"  
				ENDIF
			BREAK
			CASE CREW_RANK_4			
				IF IsSystem = FALSE
					tl31_CrewIcon += "." 
					tl31_CrewIconBackground += "/"
					tl31_CrewRank += "{"
					
				ELSE
					tl31_CrewIcon += ","
					tl31_CrewIconBackground +=   "-" 
				ENDIF
			BREAK
			CASE CREW_RANK_5
				IF IsSystem = FALSE
					tl31_CrewIcon += "." 
					tl31_CrewIconBackground += "/"
					tl31_CrewRank += ""
					
				ELSE
					tl31_CrewIcon += ","
					tl31_CrewIconBackground +=   "-" 
				ENDIF
			BREAK
		
		ENDSWITCH
	
	ENDIF

		


	SET_TEXT_STYLE(TSCrewIconStyleBackground)
	DRAW_TEXT_WITH_PLAYER_NAME(CrewIconPlacement, TSCrewIconStyleBackground, tl31_CrewIconBackground, "", HUD_COLOUR_WHITE)
	
		
		
	SET_TEXT_STYLE(TSCrewIconStyle)
	DRAW_TEXT_WITH_PLAYER_NAME(CrewIconPlacement, TSCrewIconStyle, tl31_CrewIcon, "",HUD_COLOUR_BLACK )

	
	SET_TEXT_STYLE(TSCrewStyle)
	DRAW_TEXT_WITH_PLAYER_NAME(CrewPlacement, TSCrewStyle, tl31_CrewText, "", HUD_COLOUR_BLACK)

	SET_TEXT_STYLE(TSCrewRankCharacter)
	DRAW_TEXT_WITH_PLAYER_NAME(CrewRankCharacter, TSCrewRankCharacter, tl31_CrewRank, "", HUD_COLOUR_BLACK)


ENDPROC



PROC DRAW_CREW_TAG_PLAYER(PLAYER_INDEX aPlayer, FLOAT ScreenX, FLOAT ScreenY, FLOAT ScaleMultiplyer = 1.0)
	
	GAMER_HANDLE aGamer = GET_GAMER_HANDLE_PLAYER(aPlayer)
	DRAW_CREW_TAG_GAMER(aGamer, ScreenX, ScreenY,ScaleMultiplyer )
	
ENDPROC




//#IF IS_DEBUG_BUILD
//
//BOOL RankBadge_WIDGET
//TEXT_STYLE TSBadgeTextStyle_WIDGET
//TEXT_PLACEMENT RankBadgeTextPlacement_WIDGET
//SPRITE_PLACEMENT RankBadgeSpritePlacement_WIDGET
//SPRITE_PLACEMENT RankBadgeSpritePlacement_BG_WIDGET
//
//
//#ENDIF



PROC DRAW_RANK_BADGE_FONT_LEADERBOARD(INT RankLevel, FLOAT ScreenX, FLOAT ScreenY, RANKDISPLAYTYPE aType = RANKDISPLAYTYPE_FULL)		///, FLOAT fRankScale = 0.77)


	
	
	TEXT_STYLE TSBadgeTextStyle
	TEXT_PLACEMENT RankBadgeTextPlacement

	SPRITE_PLACEMENT RankBadgeSpritePlacement
	SPRITE_PLACEMENT RankBadgeSpritePlacement_BG


//	#IF IS_DEBUG_BUILD
//	IF RankBadge_WIDGET = FALSE
//		START_WIDGET_GROUP("DRAW RANK BADGE")
//			CREATE_A_TEXT_PLACEMENT_WIDGET(RankBadgeTextPlacement_WIDGET, "RankBadgeTextPlacement_WIDGET")
//			CREATE_A_TEXT_STYLE_WIGET(TSBadgeTextStyle_WIDGET, "TSBadgeTextStyle_WIDGET")
//			
//			CREATE_A_SPRITE_PLACEMENT_WIDGET(RankBadgeSpritePlacement_WIDGET, "RankBadgeSpritePlacement_WIDGET")
//			CREATE_A_SPRITE_PLACEMENT_WIDGET(RankBadgeSpritePlacement_BG_WIDGET, "RankBadgeSpritePlacement_BG_WIDGET")
//
//		STOP_WIDGET_GROUP()
//	
//		RankBadge_WIDGET = TRUE
//	ENDIF
//	#ENDIF
	
	SET_TEXT_PLACEMENT(RankBadgeTextPlacement, ScreenX, ScreenY)

	RankBadgeSpritePlacement.x = ScreenX
	RankBadgeSpritePlacement.y = ScreenY
	RankBadgeSpritePlacement.w = 0.031-0.007-0.002+0.003-0.003
	RankBadgeSpritePlacement.h = 0.052-0.004-0.006+0.001-0.003
	RankBadgeSpritePlacement.r = 0
	RankBadgeSpritePlacement.g = 0
	RankBadgeSpritePlacement.b = 0
	RankBadgeSpritePlacement.a = 255
	
	RankBadgeSpritePlacement.y += 0.0335
	

	
	SET_STANDARD_SMALL_HUD_TEXT(TSBadgeTextStyle)
	TSBadgeTextStyle.aFont = FONT_CONDENSED


	FLOAT Rank_TEXT_SCALE 
	
	

	IF RankLevel < 10
	
		Rank_TEXT_SCALE = 0.111+0.023+0.200+0.051-0.110
		
		IF aType = RANKDISPLAYTYPE_JUST_NUMBER
			Rank_TEXT_SCALE += 0.500
		ENDIF
		
//		#IF IS_DEBUG_BUILD
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(TSBadgeTextStyle, TSBadgeTextStyle_WIDGET)
//		UPDATE_SPRITE_WIDGET_VALUE(RankBadgeSpritePlacement, RankBadgeSpritePlacement_WIDGET)
//		
//		#ENDIF
		
		
	ELIF RankLevel < 100
	
		Rank_TEXT_SCALE = 0.113+0.200-0.040
		
		IF aType = RANKDISPLAYTYPE_JUST_NUMBER
			Rank_TEXT_SCALE += 0.500
		ENDIF
		
//		#IF IS_DEBUG_BUILD
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(TSBadgeTextStyle, TSBadgeTextStyle_WIDGET)
//		#ENDIF
		
	ELIF RankLevel < 1000
	
		Rank_TEXT_SCALE = 0.038-0.010+0.250-0.092
	
		IF aType = RANKDISPLAYTYPE_JUST_NUMBER
			Rank_TEXT_SCALE += 0.400
		ENDIF
		
//		#IF IS_DEBUG_BUILD
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(TSBadgeTextStyle, TSBadgeTextStyle_WIDGET)
//		#ENDIF
	
	ELSE
	
		Rank_TEXT_SCALE = 0.003+0.160-0.081
	
		IF aType = RANKDISPLAYTYPE_JUST_NUMBER
			Rank_TEXT_SCALE += 0.300
		ENDIF
		
		
		
//		#IF IS_DEBUG_BUILD
//		UPDATE_TEXT_STYLE_WIDGET_VALUE(TSBadgeTextStyle, TSBadgeTextStyle_WIDGET)
//		#ENDIF
	
	ENDIF

	TSBadgeTextStyle.YScale += Rank_TEXT_SCALE //(RankMultiplyer_TEXT*ScaleMultiplyer)+RankAddition_TEXT
	TSBadgeTextStyle.WrapEndX += 0
	TSBadgeTextStyle.WrapStartX += 0
	TSBadgeTextStyle.r += 0
	TSBadgeTextStyle.g += 0
	TSBadgeTextStyle.b += 0
	TSBadgeTextStyle.a += 0	

	SET_TEXT_WHITE(TSBadgeTextStyle)

		
	FLOAT Rank_X_POS
	FLOAT Rank_Y_POS
	
	IF RankLevel < 10
	
		Rank_X_POS = -0.006+0.0015+0.004
		Rank_Y_POS = 0.017-0.005-0.002+0.003
			
		
	ELIF RankLevel < 100
		
		Rank_X_POS = -0.0075+0.001+0.006
		Rank_Y_POS = 0.020-0.002-0.006+0.002
		
		
	ELIF RankLevel < 1000
		Rank_X_POS = -0.0085+0.001+0.007
		Rank_Y_POS = 0.022-0.001-0.008+0.003
		
		
	
	ELSE
		Rank_X_POS = -0.009+0.0005+0.008+0.0005
		Rank_Y_POS = 0.024-0.002-0.005+0.003
	ENDIF
	
		
	RankBadgeTextPlacement.x += Rank_X_POS
	RankBadgeTextPlacement.y += Rank_Y_POS
	
	
	SET_SPRITE_HUD_COLOUR(RankBadgeSpritePlacement, HUD_COLOUR_FREEMODE)	

	
	
	RankBadgeSpritePlacement_BG = RankBadgeSpritePlacement
	RankBadgeSpritePlacement_BG.w += 0.003
	RankBadgeSpritePlacement_BG.h += 0.003
	
//	#IF IS_DEBUG_BUILD
//		UPDATE_SPRITE_WIDGET_VALUE(RankBadgeSpritePlacement_BG, RankBadgeSpritePlacement_BG_WIDGET)
//	#ENDIF
	
	
	
	SET_SPRITE_HUD_COLOUR(RankBadgeSpritePlacement_BG, HUD_COLOUR_BLACK)	
	RankBadgeSpritePlacement_BG.a = 255 //23 //9%
	RankBadgeSpritePlacement.a = 178

		

	
//	#IF IS_DEBUG_BUILD
//		RankBadgeTextPlacement.x += RankBadgeTextPlacement_WIDGET.x
//		RankBadgeTextPlacement.y += RankBadgeTextPlacement_WIDGET.y
//	#ENDIF
	

	
	

	REQUEST_STREAMED_TEXTURE_DICT("MPRankBadge")

	
	SWITCH aType

		
		CASE RANKDISPLAYTYPE_FULL_WITH_BOX			
			
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPRankBadge")
				DRAW_2D_SPRITE("MPRankBadge", "Globe_BG",RankBadgeSpritePlacement_BG, DEFAULT, DEFAULT, GFX_ORDER_AFTER_FADE)

				DRAW_2D_SPRITE("MPRankBadge", "Globe",RankBadgeSpritePlacement, DEFAULT, DEFAULT, GFX_ORDER_AFTER_FADE)

			ENDIF
			SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_FADE)
		BREAK
		
		CASE RANKDISPLAYTYPE_JUST_NUMBER
			SET_TEXT_BLACK(TSBadgeTextStyle)
		BREAK
	
	ENDSWITCH
	
	SET_TEXT_STYLE(TSBadgeTextStyle)
	DRAW_TEXT_WITH_NUMBER(RankBadgeTextPlacement, TSBadgeTextStyle,  "NUMBER", RankLevel, FONT_CENTRE)
	
ENDPROC


PROC RESET_BIRDS_ALPHA()
	DEBUG_PRINTCALLSTACK()
	NET_NL()NET_PRINT("RESET_BIRDS_ALPHA - CALLED ")
	g_i_private_BirdsAndBeesAlphaStages = 0
	g_i_Private_BirdsAndBeesAlpha = 0
	g_i_Private_BirdsAndBeesRectAlpha = 0
	RESET_NET_TIMER(g_st_Private_BirdsAndBeesTimer)
ENDPROC


FUNC BOOL RUN_BIRDS_ALPHA()


	
	
	
	SWITCH g_i_private_BirdsAndBeesAlphaStages
	
		CASE 0
		
			 
		
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(g_st_Private_BirdsAndBeesTimer, 350)
				g_i_private_BirdsAndBeesAlphaStages++
			ELSE
			
				//FadeInRect
			
				g_i_Private_BirdsAndBeesRectAlpha += g_i_Private_BirdsAndBeesAlphaRate
				IF g_i_Private_BirdsAndBeesRectAlpha > 255
					g_i_Private_BirdsAndBeesRectAlpha = 255
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(g_st_Private_BirdsAndBeesTimer, 350)
				g_i_private_BirdsAndBeesAlphaStages++
			ELSE
			
				//FadeInTexture
			
				g_i_Private_BirdsAndBeesAlpha += g_i_Private_BirdsAndBeesAlphaRate
				IF g_i_Private_BirdsAndBeesAlpha > 255
					g_i_Private_BirdsAndBeesAlpha = 255
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF HAS_NET_TIMER_EXPIRED_ONE_FRAME(g_st_Private_BirdsAndBeesTimer, 150)
				g_i_private_BirdsAndBeesAlphaStages++
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE 3
			RETURN TRUE
		BREAK
		
	
	ENDSWITCH
	
	
	RETURN FALSE
	

ENDFUNC

PROC FULLSCREEN_BIRDS_BACKGROUND()
	g_i_Private_BirdsAndBeesAlpha = 255
	g_i_private_BirdsAndBeesAlphaStages = 3
	
ENDPROC



//BOOL aWidgetBirdBool
//SPRITE_PLACEMENT aWidgetBirdSprite[4]

PROC DRAW_BACKGROUND_BIRDS(GFX_DRAW_ORDER drawOrder = GFX_ORDER_AFTER_HUD)

	SPRITE_PLACEMENT aSprite[4]
	
	RECT aRect
	
	aSprite[0].x += 0.125
	
	aSprite[1].x += 0.125+0.250
	
	aSprite[2].x += 0.125+0.5
	
	aSprite[3].x += 0.125+0.75
	
	INT I 
	FOR I = 0 TO 3
		aSprite[I].y += 0.5
		aSprite[I].w += 0.25
		aSprite[I].h += 1.0
		aSprite[I].r += 255
		aSprite[I].g += 255
		aSprite[I].b += 255
		aSprite[I].a += g_i_Private_BirdsAndBeesAlpha
		SET_SPRITE_HUD_COLOUR(aSprite[I], HUD_COLOUR_FREEMODE )

	ENDFOR
	
	aRect.x = 0.5
	aRect.y = 0.5
	aRect.w = 1.0
	aRect.h = 1.0
	aRect.r = 255
	aRect.g = 255
	aRect.b = 255
	aRect.a = 0
	SET_RECT_TO_THIS_HUD_COLOUR(aRect, HUD_COLOUR_FREEMODE)
	
	aRect.r -= g_i_Private_BirdsAndBeesColourDiff
	aRect.g -= g_i_Private_BirdsAndBeesColourDiff
	aRect.b -= g_i_Private_BirdsAndBeesColourDiff
	
	aRect.a = g_i_Private_BirdsAndBeesRectAlpha

	
	
	
//	#IF IS_DEBUG_BUILD
//	
//	
//		IF aWidgetBirdBool = FALSE
//			
//			START_WIDGET_GROUP("BIRDS AND BEES")
//			
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(aWidgetBirdSprite[0], "aWidgetBirdSprite[0]")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(aWidgetBirdSprite[1], "aWidgetBirdSprite[1]")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(aWidgetBirdSprite[2], "aWidgetBirdSprite[2]")
//				CREATE_A_SPRITE_PLACEMENT_WIDGET(aWidgetBirdSprite[3], "aWidgetBirdSprite[3]")
//			
//			
//			STOP_WIDGET_GROUP()
//				
//		
//			aWidgetBirdBool = TRUE
//		ENDIF
//	
//		UPDATE_SPRITE_WIDGET_VALUE(aSprite[0], aWidgetBirdSprite[0])
//		UPDATE_SPRITE_WIDGET_VALUE(aSprite[1], aWidgetBirdSprite[1])
//		UPDATE_SPRITE_WIDGET_VALUE(aSprite[2], aWidgetBirdSprite[2])
//		UPDATE_SPRITE_WIDGET_VALUE(aSprite[3], aWidgetBirdSprite[3])
//	
//	#ENDIF
	
	IF GET_CURRENT_HUD_STATE() = HUD_STATE_SELECT_CHARACTER_FM
		SET_SCRIPT_GFX_DRAW_ORDER(drawOrder)
		DRAW_RECTANGLE(aRect)
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_AFTER_HUD)
	ENDIF
	
	REQUEST_STREAMED_TEXTURE_DICT("MPEntry", TRUE)
	IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPEntry")
		DRAW_2D_SPRITE("MPEntry", "BirdsAndBees_flat", aSprite[0], DEFAULT, DEFAULT, drawOrder)
		DRAW_2D_SPRITE("MPEntry", "BirdsAndBees_flat", aSprite[1], DEFAULT, DEFAULT, drawOrder)
		DRAW_2D_SPRITE("MPEntry", "BirdsAndBees_flat", aSprite[2], DEFAULT, DEFAULT, drawOrder)
		DRAW_2D_SPRITE("MPEntry", "BirdsAndBees_flat", aSprite[3], DEFAULT, DEFAULT, drawOrder)
		g_AreBirdsDisplaying = TRUE
	ENDIF
	
	
	
	
	
ENDPROC



FUNC INT DAYS_UNTIL_RELEASE_SELECTOR()
	INT iReleaseTime =  g_sMPTunables.iMpCountDownTimer
	INT iCurrentTime = GET_CLOUD_TIME_AS_INT()
	
	INT iTimeDifference = (iReleaseTime-iCurrentTime)
	INT iDays
			
	IF iTimeDifference > 0
		// DAYS
		IF (iTimeDifference/86400) > 0
			iDays = (iTimeDifference/86400)
			iTimeDifference -= (iDays*86400) 
		ENDIF
	ENDIF
	RETURN iDays
ENDFUNC

FUNC STRING GET_GTAO_BLOCK_REASON(INT & iDaysTilRelease)

	MULTIPLAYER_ACCESS_CODE accessFailReason
	STRING reasonString
	iDaysTilRelease = DAYS_UNTIL_RELEASE_SELECTOR()
	IF NOT SCRIPT_NETWORK_CAN_ACCESS_MULTIPLAYER(accessFailReason)
	
		
	
		PRINTLN("BC: - GET_GTAO_BLOCK_REASON NOT SCRIPT_NETWORK_CAN_ACCESS_MULTIPLAYER")
		#IF USE_FINAL_PRINTS
		PRINTLN_FINAL("BC: - GET_GTAO_BLOCK_REASON NOT SCRIPT_NETWORK_CAN_ACCESS_MULTIPLAYER ")								
		#ENDIF
		
		SWITCH accessFailReason
			CASE ACCESS_DENIED_TUNABLE_NOT_FOUND
				PRINTLN("BC: - GET_GTAO_BLOCK_REASON  NOT ACCESS_DENIED_TUNABLE_NOT_FOUND")
				
				reasonString = "PM_LAUNCH"
//				// Release date not reached
//				IF iDaysTilRelease > 1
//					reasonString = "PM_DODAYS"
//				ELSE
//					IF iDaysTilRelease = 1
//						reasonString = "PM_DODAY"
//					ELSE
//						reasonString = "PM_DOTDY"
//						iDaysTilRelease = -1
//					ENDIF
//				ENDIF
			BREAK
		    CASE ACCESS_DENIED_TUNABLE_FALSE // Release date not reached
				PRINTLN("BC: - GET_GTAO_BLOCK_REASON  NOT ACCESS_DENIED_TUNABLE_FALSE")
				// Release date not reached
				reasonString = "PM_LAUNCH"
//				IF iDaysTilRelease > 1
//					reasonString = "PM_DODAYS"
//				ELSE
//					IF iDaysTilRelease = 1
//						reasonString = "PM_DODAY"
//					ELSE
//						reasonString = "PM_DOTDY"
//						iDaysTilRelease = -1
//					ENDIF
//				ENDIF
			BREAK
		    CASE ACCESS_DENIED_NETWORK_LOCKED // No Online Pass
				PRINTLN("BC: - GET_GTAO_BLOCK_REASON NOT ACCESS_DENIED_NETWORK_LOCKED")
				reasonString = "PM_DODLCB"
			BREAK
		    CASE ACCESS_DENIED_INVALID_PROFILE_SETTINGS
				PRINTLN("BC: - GET_GTAO_BLOCK_REASON NOT ACCESS_DENIED_INVALID_PROFILE_SETTINGS")
				// Something very bad has happened, should never get here
			BREAK
		    CASE ACCESS_DENIED_PROLOGUE_INCOMPLETE // Prologue incomplete	
				PRINTLN("BC: - GET_GTAO_BLOCK_REASON NOT ACCESS_DENIED_PROLOGUE_INCOMPLETE")
				reasonString = "PM_DOPROB"
			BREAK
			CASE ACCESS_DENIED_NO_BACKGROUND_SCRIPT
				PRINTLN("BC: - GET_GTAO_BLOCK_REASON NOT ACCESS_DENIED_NO_BACKGROUND_SCRIPT")
				reasonString = "HUD_NOBACKGRN"
			BREAK
			CASE ACCESS_DENIED_NO_TUNABLES
				PRINTLN("BC: - GET_GTAO_BLOCK_REASON NOT ACCESS_DENIED_NO_TUNABLES")
				reasonString = "HUD_NOTUNE"
			BREAK
			DEFAULT
				PRINTLN("BC: - GET_GTAO_BLOCK_REASON NO REASON")
			BREAK
		ENDSWITCH

	ELSE 
		#if USE_CLF_DLC
			IF g_savedGlobalsClifford.sFlow.missionSavedData[SP_MISSION_CLF_TRAIN].completed = FALSE
				PRINTLN("BC: - GET_GTAO_BLOCK_REASON NOT g_savedGlobalsClifford.sFlow.missionSavedData[SP_MISSION_CLF_TRAIN].completed = FALSE")
				reasonString = "PM_DOPROB"
			ELSE
				PRINTLN("BC: - GET_GTAO_BLOCK_REASON SCRIPT_NETWORK_CAN_ACCESS_MULTIPLAYER = TRUE ")
			ENDIF
		#ENDIF
		#if USE_NRM_DLC
			IF g_savedGlobalsnorman.sFlow.missionSavedData[SP_MISSION_NRM_SUR_START].completed = FALSE
				PRINTLN("BC: - GET_GTAO_BLOCK_REASON NOT g_savedGlobalsnorman.sFlow.missionSavedData[SP_MISSION_NRM_SUR_A].completed = FALSE ")
				reasonString = "PM_DOPROB"
			ELSE
				PRINTLN("BC: - GET_GTAO_BLOCK_REASON SCRIPT_NETWORK_CAN_ACCESS_MULTIPLAYER = TRUE ")
			ENDIF
		#ENDIF
		#if not USE_CLF_DLC
		#if not USE_NRM_DLC
			IF g_savedGlobals.sFlow.missionSavedData[SP_MISSION_PROLOGUE].completed = FALSE
				PRINTLN("BC: - GET_GTAO_BLOCK_REASON NOT g_savedGlobals.sFlow.missionSavedData[SP_MISSION_PROLOGUE].completed = FALSE ")
				reasonString = "PM_DOPROB"
			ELSE
				PRINTLN("BC: - GET_GTAO_BLOCK_REASON SCRIPT_NETWORK_CAN_ACCESS_MULTIPLAYER = TRUE ")
			ENDIF
		#ENDIF
		#ENDIF
	ENDIF
	
	
	RETURN reasonString


ENDFUNC







FUNC BOOL HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(CONTROL_ACTION anAction)

	
	INT Index = ENUM_TO_INT(anAction)
	INT Bitset =  GET_WEAPON_BITSET(Index) 
	INT BitsetIndex = GET_WEAPON_INDEX_BITSET(Index)
	
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, anAction)
	OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, anAction)
	OR IS_ANALOGUE_STICK_MOVED(anAction, MPGlobalsHud.iDelayInt, TRUE)
		
		IF NOT IS_BIT_SET(MPGlobalsHud.iHudButtonPressed[Bitset], BitsetIndex)
			SET_BIT(MPGlobalsHud.iHudButtonPressed[Bitset], BitsetIndex)
			RETURN TRUE
		ENDIF
	ELSE
		IF IS_BIT_SET(MPGlobalsHud.iHudButtonPressed[Bitset], BitsetIndex)
			CLEAR_BIT(MPGlobalsHud.iHudButtonPressed[Bitset], BitsetIndex)
		ENDIF
	ENDIF
	
	
	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(CONTROL_ACTION anAction)

	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, anAction) 
	OR IS_DISABLED_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, anAction) 
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL SHOULD_FADE_IN_SCREEN_WITH_MESSAGE()
	IF g_BGScript_PreventScreenFadeAfterMessage
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_TRIGGERED_ENTER_TO_BASE_FROM_AIRCRAFT_WITH_VEH(PLAYER_ID())
	OR IS_ENTER_BASE_FROM_AIRCRAFT_WITH_VEH_TRIGGERED_IN_PROGRESS()
	OR (IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID()) AND IS_DRIVER_ENTERING_SIMPLE_INTERIOR(PLAYER_ID()) AND GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_DRIVER_IS_ENTERING(PLAYER_ID())) = SIMPLE_INTERIOR_TYPE_DEFUNCT_BASE)
	OR (IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID()) AND IS_DRIVER_ENTERING_SIMPLE_INTERIOR(PLAYER_ID()) AND GET_SIMPLE_INTERIOR_TYPE(GET_SIMPLE_INTERIOR_DRIVER_IS_ENTERING(PLAYER_ID())) = SIMPLE_INTERIOR_TYPE_ARMORY_TRUCK)
		PRINTLN("SHOULD_FADE_IN_SCREEN_WITH_MESSAGE - transition between avenger and facility")
		RETURN FALSE
	ENDIF
	
	IF HAS_PLAYER_START_EXIT_TO_BUNKER_FROM_TRUCK()
		PRINTLN("SHOULD_FADE_IN_SCREEN_WITH_MESSAGE - HAS_PLAYER_START_EXIT_TO_BUNKER_FROM_TRUCK TRUE")
		RETURN FALSE
	ENDIF
	
	IF HAS_PLAYER_START_EXIT_TO_BASE_FROM_AIRCRAFT()
		PRINTLN("SHOULD_FADE_IN_SCREEN_WITH_MESSAGE - HAS_PLAYER_START_EXIT_TO_BASE_FROM_AIRCRAFT TRUE")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK()
		PRINTLN("SHOULD_FADE_IN_SCREEN_WITH_MESSAGE - IS_PLAYER_MOVING_FROM_BUNKER_TO_TRUCK TRUE")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_MOVING_FROM_BASE_TO_AIRCRAFT()
		PRINTLN("SHOULD_FADE_IN_SCREEN_WITH_MESSAGE - IS_PLAYER_MOVING_FROM_BASE_TO_AIRCRAFT TRUE")
		RETURN FALSE
	ENDIF
	
	IF g_bForceToHackerTruck
		PRINTLN("SHOULD_FADE_IN_SCREEN_WITH_MESSAGE - g_bForceToHackerTruck TRUE")
		RETURN FALSE
	ENDIF
	
	IF HAS_EXIT_TO_BASE_FROM_AIRCRAFT_TRIGGERED()
		PRINTLN("SHOULD_FADE_IN_SCREEN_WITH_MESSAGE - HAS_EXIT_TO_BASE_FROM_AIRCRAFT_TRIGGERED TRUE")
		RETURN FALSE
	ENDIF
	
	IF g_bUpdateCustomApt
		PRINTLN("SHOULD_FADE_IN_SCREEN_WITH_MESSAGE - g_bUpdateCustomApt TRUE")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_MOVING_TO_ARMORY_AIRCRAFT_PILOT_SEAT_IN_BASE(PLAYER_ID())
		PRINTLN("SHOULD_FADE_IN_SCREEN_WITH_MESSAGE - IS_PLAYER_MOVING_TO_ARMORY_AIRCRAFT_PILOT_SEAT_IN_BASE TRUE")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_MOVING_TO_HACKER_TRUCK_CAB_SEAT_IN_HUB(PLAYER_ID())
		PRINTLN("SHOULD_FADE_IN_SCREEN_WITH_MESSAGE - IS_PLAYER_MOVING_TO_HACKER_TRUCK_CAB_SEAT_IN_HUB TRUE")
		RETURN FALSE
	ENDIF
	
	IF _SIMPLE_INTERIOR_IS_PLAYER_SWITCHING_BETWEEN_INTERIORS()
		PRINTLN("SHOULD_FADE_IN_SCREEN_WITH_MESSAGE - _SIMPLE_INTERIOR_IS_PLAYER_SWITCHING_BETWEEN_INTERIORS TRUE")
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_SimpleInteriorData.iForthBS, BS4_SIMPLE_INTERIOR_AUTOWARP_BETWEEN_TWO_INTERIORS)
		PRINTLN("SHOULD_FADE_IN_SCREEN_WITH_MESSAGE - BS4_SIMPLE_INTERIOR_AUTOWARP_BETWEEN_TWO_INTERIORS TRUE")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	
	IF IS_WARNING_MESSAGE_ACTIVE()
		IF IS_SCREEN_FADED_OUT()
			IF SHOULD_FADE_IN_SCREEN_WITH_MESSAGE()
				DO_SCREEN_FADE_IN(0)
			ENDIF	
		ENDIF
	ENDIF
	IF GET_IS_LOADING_SCREEN_ACTIVE()
		SHUTDOWN_LOADING_SCREEN()
	ENDIF

ENDPROC



ENUM WARNING_SCREEN_RETURN
	WARNING_SCREEN_RETURN_NONE = 0,
	WARNING_SCREEN_RETURN_ACCEPT,
	WARNING_SCREEN_RETURN_CANCEL
ENDENUM



FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED_SCREEN_ONLY()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED_SCREEN_ONLY - running ")
	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("[BCWHEEL]  RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED_SCREEN_ONLY - running ")								
	#ENDIF

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

//	IF g_b_Private_NumberofAttemptsToChangeAimType != 2
		IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
			SET_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
		ENDIF
//	ENDIF

	

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 

			NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED_SCREEN_ONLY - INPUT_FRONTEND_ACCEPT ")
		
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED_SCREEN_ONLY, FALSE)
			SET_TRANSITION_SESSIONS_RECIEVED_FAILED_TO_LAUNCH()
			
			SET_LOADING_ICON_INACTIVE()			
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	
	
	 

	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge =  "TRAN_JOINFAIL" 
	STRING sLineTwoMessge = "TRAN_RETNFM"
	
	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED_SCREEN_ONLY - GET_CURRENT_GAMEMODE() = ")NET_PRINT(GET_GAMEMODE_STRING(GET_CURRENT_GAMEMODE())) 
	
	
	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED_SCREEN_ONLY - g_Private_BailWarningScreenEventParam = ")NET_PRINT_INT(g_Private_BailWarningScreenEventParam) 
	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED_SCREEN_ONLY - ENUM_TO_INT(g_JoinResponseEvent.nResponseCode) = ")NET_PRINT_INT(ENUM_TO_INT(g_JoinResponseEvent.nResponseCode)) 
	
	
	IF (ENUM_TO_INT(g_JoinResponseEvent.nResponseCode)) > -1
		sLineOneMessge = JOIN_SESSION_FAILED_REASON_STRING(ENUM_TO_INT(g_JoinResponseEvent.nResponseCode), TEAM_FREEMODE) 
	endif
	
	IF g_Private_BailWarningScreenEventParam > -1

		sLineOneMessge = JOIN_SESSION_FAILED_REASON_STRING(g_Private_BailWarningScreenEventParam, TEAM_FREEMODE) 
	ENDIF
	
	IF IS_STRING_EMPTY_HUD(sLineOneMessge)
		sLineOneMessge =  "TRAN_JOINFAIL" 
	ENDIF
	
	FE_WARNING_FLAGS Buttons = FE_WARNING_CONTINUE


	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, Buttons , sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC





FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED()
	
	BOOL bRunGroupFullChangeTeam = FALSE
	BOOL bIsSCTV = FALSE
	IF IS_PLAYER_SCTV(PLAYER_ID())
	OR IS_INVITED_TO_SCTV()
	OR IS_PAUSE_MENU_SELECT_TO_SCTV()
		bIsSCTV = TRUE
	ENDIF
	
	IF g_Private_BailWarningScreenEventParam > -1
		IF bIsSCTV = FALSE
			IF INT_TO_ENUM(JOIN_RESPONSE_CODE, g_Private_BailWarningScreenEventParam)  = RESPONSE_DENY_GROUP_FULL
				bRunGroupFullChangeTeam = TRUE //when activating this. 
			ENDIF
		ENDIF
	ENDIF
	
	//NG Turn on Both QUEUE & Spectate
	IF g_sMPTunables.bdisable_queuing_system = FALSE
		IF NETWORK_CAN_QUEUE_FOR_PREVIOUS_SESSION_JOIN()
			SET_BIT(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_JOIN_QUEUE))
		ELSE
			NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - NETWORK_CAN_QUEUE_FOR_PREVIOUS_SESSION_JOIN = FALSE ")

		ENDIF
	ENDIF
	
	IF g_smpTunables.bdisable_SCTV_friends_spectate = FALSE
		IF g_sMPTunables.bTurnOnSpectateAfterJoiningFullSession = TRUE
			SET_BIT(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_SPECTATE_PLAYER))
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_SPECTATE_PLAYER)) = FALSE
	AND IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_JOIN_QUEUE)) = FALSE
		bRunGroupFullChangeTeam = FALSE
	ENDIF
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1
	CONST_INT X 2
	
	PRINTLN("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - running g_Private_BailWarningScreenEventParam = ",g_Private_BailWarningScreenEventParam, " bRunGroupFullChangeTeam = ",bRunGroupFullChangeTeam)


	BOOL bFreezeScreen = FALSE

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

//	IF g_b_Private_NumberofAttemptsToChangeAimType != 2
		IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
			SET_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
		ENDIF
//	ENDIF

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_X)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, X)
	ENDIF

	BOOL ShouldThisPullcameraDown = FALSE

	IF bRunGroupFullChangeTeam = FALSE
		IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			 
			
				SET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE(FM_SESSION_MENU_CHOICE_JOIN_PUBLIC_SESSION)

				
				NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - INPUT_FRONTEND_ACCEPT")
			
				PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET") 
				SET_BAIL_STILL_VALID(FALSE)
				TELL_TRANSITION_BAIL_HAPPENED(FALSE)
				IGNORE_INVITE()
				
				
					
				IF HAS_AIMTYPE_ALTERED_BY_INVITE()
					PUT_AIMTYPE_BACK_TO_WHERE_IT_WAS()
					SET_AIMTYPE_ALTERED_BY_INVITE(FALSE)
					PRINTLN("[AIMPREF] RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - put aimtype back ")
				ENDIF
		
				
			
				CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
				
				IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				OR INT_TO_ENUM(JOIN_RESPONSE_CODE, g_Private_BailWarningScreenEventParam)  = RESPONSE_DENY_GROUP_FULL
				
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - RESPONSE_DENY_GROUP_FULL or NOT NETWORK_IS_GAME_IN_PROGRESS ")
					#ENDIF
					
					IF INT_TO_ENUM(JOIN_RESPONSE_CODE, g_Private_BailWarningScreenEventParam)  = RESPONSE_DENY_GROUP_FULL
					AND (SHOULD_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE() OR DID_TRANSITION_SESSION_PUT_CAM_IN_SKY_FOR_SESSION_CHANGE())
					AND (IS_SKYSWOOP_IN_SKY() OR IS_SKYSWOOP_MOVING())
					
						#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - ShouldThisPullcameraDown = TRUE ")
						#ENDIF
						
						SET_SCRIPT_BAILS_PROCESSING(TRUE)
						SET_JOINING_GAMEMODE(GAMEMODE_FM)
						SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM)
						TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)	
						
					ENDIF
					RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
				ENDIF
				IF GET_CORONA_STATUS() != CORONA_STATUS_IDLE
					SET_TRANSITION_SESSIONS_RECIEVED_FAILED_TO_LAUNCH()
				ENDIF
				g_iExternalWarningScreenStages = 0
				g_Private_BailWarningScreenButtonBitset = 0
				
				IF IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA()

					#IF IS_DEBUG_BUILD
						NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - IS_LOCAL_PLAYER_IN_ACTIVITY_TUTORIAL_CORONA = TRUE calling NETWORK_BAIL() ")
					#ENDIF
					NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_ACTIVITY_TUTORIAL_CORONA_JOIN_FAILED))
					
					
				ENDIF
				

				SET_BAILED_HAPPENED_SOMEHOW(FALSE) //Already dealt with the bail, if we enter teh transition don't show again.
				
				IF TRANSITION_SESSIONS_RECIEVED_JOIN_FAILED_WITH_DEAD_FREEMODE()
				OR TRANSITION_SESSIONS_STARTED_EVENT_FAILED()
					
					#IF IS_DEBUG_BUILD
						IF TRANSITION_SESSIONS_RECIEVED_JOIN_FAILED_WITH_DEAD_FREEMODE()
							NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - TRANSITION_SESSIONS_RECIEVED_JOIN_FAILED_WITH_DEAD_FREEMODE - Bail ")
						ENDIF
						IF TRANSITION_SESSIONS_STARTED_EVENT_FAILED()
							NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - TRANSITION_SESSIONS_STARTED_EVENT_FAILED - Bail ")
						ENDIF
					#ENDIF
					
					SET_FRONTEND_ACTIVE(FALSE)
//					SET_SKYFREEZE_FROZEN()
					bFreezeScreen = TRUE
					
					IF TRANSITION_SESSIONS_RECIEVED_JOIN_FAILED_WITH_DEAD_FREEMODE()
						SET_HAS_JOIN_FAILED_WITH_DEAD_FREEMODE(TRUE)
					ENDIF
					
					SET_QUIT_CURRENT_GAME(TRUE)
					CLEAR_TRANSITION_SESSIONS_RECIEVED_JOIN_FAILED_WITH_DEAD_FREEMODE()
					CLEAR_TRANSITION_SESSIONS_STARTED_EVENT_FAILED()

					IF IS_TRANSITION_ACTIVE() = FALSE
						TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)
						SET_EMERGENCY_SKYCAM_UP_RUNNING(TRUE)
					ENDIF
					
				ENDIF
				
				
				
				
				IF GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR

					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED -  - warp to FM GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR ")

					SET_FRONTEND_ACTIVE(FALSE)
//					SET_SKYFREEZE_FROZEN()
					bFreezeScreen = TRUE
					SET_QUIT_CURRENT_GAME(TRUE)

					SET_PAUSE_MENU_WARP_FROM_MODE(GAMEMODE_CREATOR)
					SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM)

					IF IS_TRANSITION_ACTIVE() = FALSE
						TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)	
						SET_EMERGENCY_SKYCAM_UP_RUNNING(TRUE)
						
					ENDIF
					
				ENDIF
				
				IF GET_CURRENT_GAMEMODE() = GAMEMODE_SP
				OR GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY
				
					
					
						SET_QUIT_CURRENT_GAME(TRUE)
					
				
					IF IS_TRANSITION_ACTIVE()
					
						SET_CURRENT_TRANSITION_BAILING_OPTIONS(BAILING_OPTIONS_EMPTY)
						SET_JOINING_GAMEMODE(GAMEMODE_FM)
						TRANSITION_CHANGE_STATE(TRANSITION_STATE_PRE_HUD_CHECKS)
						HUD_CHANGE_STATE(HUD_STATE_PRE_HUD_CHECKS)
			
						
					
					ELSE
						SET_FRONTEND_ACTIVE(FALSE)
//						SET_SKYFREEZE_FROZEN()
						bFreezeScreen = TRUE
						SET_PAUSE_MENU_WARP_FROM_MODE(GAMEMODE_SP)
						SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM)

						TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)				
					ENDIF
					
				ENDIF
				
				BOOL AlreadySetJoiningGamemode = FALSE
				
				IF GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY
				AND GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_IS_FM_AND_TRANSITION_READY
				
					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED -  - Midway through the transition, bailing from MP now.")

				
					SET_FRONTEND_ACTIVE(FALSE)
//					SET_SKYFREEZE_FROZEN()
					bFreezeScreen = TRUE
					SET_QUIT_CURRENT_GAME(TRUE)
					AlreadySetJoiningGamemode = TRUE
					REQUEST_TRANSITION_TERMINATE_MP_SESSION()
				ENDIF
				
				
				IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
				AND HAS_SCRIPT_TIMEDOUT_TRANSITION()
					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED -  - Midway through the transition and transition sessions probably timed out, bailing from MP now.")
					SET_FRONTEND_ACTIVE(FALSE)
					AlreadySetJoiningGamemode = TRUE
					SET_QUIT_CURRENT_GAME(TRUE)
					SET_SCRIPT_TIMEDOUT_TRANSITION(FALSE)
					REQUEST_TRANSITION_TERMINATE_MP_SESSION()
				ENDIF
				
				IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
				AND INT_TO_ENUM(JOIN_RESPONSE_CODE, g_Private_BailWarningScreenEventParam)  = RESPONSE_DENY_BLOCKING
					PRINTLN(" RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - head back to FM - fully - RESPONSE_DENY_BLOCKING ")
					
					SET_FRONTEND_ACTIVE(FALSE)
					bFreezeScreen = TRUE
					SET_PAUSE_MENU_WARP_FROM_MODE(GAMEMODE_SP)
					SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM)

					TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)				
					
				ENDIF
				

				
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF
				
				IF NOT IS_SKYSWOOP_AT_GROUND()
				AND SHOULD_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
					PRINTLN(" RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - DON'T TURN PLAYER CONTROL ON WE ARE IN A SKYSWOOP")
					SET_TRANSITION_SESSION_RETURN_PLAYER_CONTROL_AFTER_CAM_DOWN()
				ELIF NETWORK_IS_GAME_IN_PROGRESS()
					//Don't turn player control back if we got a failed to launch, that corona will deal with it
					IF NOT TRANSITION_SESSIONS_RECIEVED_FAILED_TO_LAUNCH()
						IF NOT IS_SKYSWOOP_AT_GROUND()
						AND (DID_TRANSITION_SESSION_PUT_CAM_IN_SKY_FOR_SESSION_CHANGE()
						OR SHOULD_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE())
							PRINTLN(" RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - NET SET_TRANSITION_SESSION_RETURN_PLAYER_CONTROL_AFTER_CAM_DOWN ON")
							SET_TRANSITION_SESSION_RETURN_PLAYER_CONTROL_AFTER_CAM_DOWN()
						ELSE
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							PRINTLN(" RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - NET SET PLAYER CONTROL ON")
						ENDIF
					ENDIF
				ELSE
					SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
							SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						ENDIF
					ENDIF
					PRINTLN(" RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - SET PLAYER CONTROL ON")
				ENDIF
				
				
				IF GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY
				AND GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_IS_FM_AND_TRANSITION_READY					
					REQUEST_TRANSITION(GET_CURRENT_TRANSITION_STATE(), GAMEMODE_SP, GET_CURRENT_HUD_STATE())	
				ELSE
					IF AlreadySetJoiningGamemode = FALSE
						SET_JOINING_GAMEMODE(GAMEMODE_FM)
						PRINTLN(" RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - head back to FM ")
						
						
						
					ENDIF
				ENDIF
				
				
				//Put the cam on the ground
				IF DID_TRANSITION_SESSION_PUT_CAM_IN_SKY_FOR_SESSION_CHANGE()
				OR SHOULD_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
				OR ShouldThisPullcameraDown
					CLEAR_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
					CLEAR_TRANSITION_SESSION_PUT_CAM_IN_SKY_FOR_SESSION_CHANGE()
					IF NOT IS_TRANSITION_MENU_TRIGGERED() //The maintransition will handle bringing the camera down. bug 2088667
						SET_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
						IF NOT IS_SKYSWOOP_AT_GROUND()
						AND NOT TRANSITION_SESSIONS_RECIEVED_FAILED_TO_LAUNCH()
							SET_TRANSITION_SESSION_RETURN_PLAYER_CONTROL_AFTER_CAM_DOWN()
						ENDIF
					ENDIF
				ENDIF
				
				IF bFreezeScreen = TRUE
					SET_SKYFREEZE_FROZEN(TRUE)
				ELSE
					SET_SKYFREEZE_CLEAR(TRUE, TRUE, FALSE)
				ENDIF
									
				SWITCH_SCTV_MODE(SCTV_MODE_INIT)
				MPSpecGlobals.SCTVData.stage = SCTV_STAGE_CLEANUP
				PRINTLN(" RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - MPSpecGlobals.SCTVData.stage = SCTV_STAGE_CLEANUP")
				SET_LOADING_ICON_INACTIVE()			
				SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED, FALSE)
				
				
				
				
				RETURN WARNING_SCREEN_RETURN_ACCEPT
			ENDIF
		ENDIF
	ELSE
		IF IS_SAFE_TO_QUIT_BAIL_SCREEN_EXTERNAL_SCREENS()	
			IF IS_BIT_SET(g_bitset_SessionFullQuestion_Answer, ENUM_TO_INT(SESSION_FULL_QUESTION_SPECTATE_PLAYER)) = FALSE
				IF IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_SPECTATE_PLAYER))
				AND NOT IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_JOIN_QUEUE))
				
					IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
						IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
							CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
							PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET") 
							NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED: INPUT_FRONTEND_ACCEPT - SESSION_FULL_QUESTION_SPECTATE_PLAYER")
							SET_BIT(g_bitset_SessionFullQuestion_Answer, ENUM_TO_INT(SESSION_FULL_QUESTION_SPECTATE_PLAYER))
						ENDIF
					ENDIF
					
				ELIF IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_SPECTATE_PLAYER))
				AND IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_JOIN_QUEUE))
					
					IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, X)
						IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_X) 
							CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, X)
							PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET") 
							NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED: INPUT_FRONTEND_X - SESSION_FULL_QUESTION_SPECTATE_PLAYER")
				
							SET_BIT(g_bitset_SessionFullQuestion_Answer, ENUM_TO_INT(SESSION_FULL_QUESTION_SPECTATE_PLAYER))
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(g_bitset_SessionFullQuestion_Answer, ENUM_TO_INT(SESSION_FULL_QUESTION_JOIN_QUEUE)) = FALSE
				IF IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_JOIN_QUEUE))
				AND NOT IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_SPECTATE_PLAYER))

					IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
						IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
							NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED: INPUT_FRONTEND_ACCEPT - SESSION_FULL_QUESTION_JOIN_QUEUE")
		
							CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
							PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET") 
							SET_BIT(g_bitset_SessionFullQuestion_Answer, ENUM_TO_INT(SESSION_FULL_QUESTION_JOIN_QUEUE))
						ENDIF
					ENDIF
					
				ELIF IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_JOIN_QUEUE))
				AND IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_SPECTATE_PLAYER))
					IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
						IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
							CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
							PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET") 
							NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED: INPUT_FRONTEND_ACCEPT - SESSION_FULL_QUESTION_JOIN_QUEUE")
							SET_BIT(g_bitset_SessionFullQuestion_Answer, ENUM_TO_INT(SESSION_FULL_QUESTION_JOIN_QUEUE))
						ENDIF
					ENDIF
				
				ENDIF
			ENDIF
			
			
			IF IS_BIT_SET(g_bitset_SessionFullQuestion_Answer, ENUM_TO_INT(SESSION_FULL_QUESTION_CANCELLED)) = FALSE
				IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, CANCEL)
					IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_CANCEL) 
						CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
						PLAY_SOUND_FRONTEND(-1, "CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET") 
						NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED: INPUT_FRONTEND_CANCEL - SESSION_FULL_QUESTION_CANCELLED ")					
						SET_BIT(g_bitset_SessionFullQuestion_Answer, ENUM_TO_INT(SESSION_FULL_QUESTION_CANCELLED))
					ENDIF
				ENDIF
			
			ENDIF
		ELSE
			g_Private_BailWarningScreenButtonBitset = 0
		ENDIF
	ENDIF
	
	
	IF IS_BIT_SET(g_bitset_SessionFullQuestion_Answer, ENUM_TO_INT(SESSION_FULL_QUESTION_JOIN_QUEUE))
		IF SET_SKYSWOOP_UP()
			IF IS_SAFE_TO_QUIT_BAIL_SCREEN_EXTERNAL_SCREENS()
				NET_NL()NET_PRINT("BC: RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - INPUT_FRONTEND_X going to Join queue, NETWORK_SEND_QUEUED_JOIN_REQUEST() called ")
				SET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE(FM_SESSION_MENU_CHOICE_JOIN_PUBLIC_SESSION)
		
				NETWORK_SEND_QUEUED_JOIN_REQUEST()
				SET_CURRENT_TRANSITION_BAILING_OPTIONS(BAILING_OPTIONS_EMPTY)
				TELL_TRANSITION_BAIL_HAPPENED(FALSE)
				
				SET_JOINED_QUEUE_HEAD_TO_GTAO(TRUE)				
				IF NOT IS_TRANSITION_ACTIVE()
					SET_PAUSE_MENU_WARP_FROM_MODE(GAMEMODE_SP)
					SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM)

					IF IS_TRANSITION_ACTIVE() = FALSE
						TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)				
					ENDIF
				ELSE
					TRANSITION_CHANGE_STATE(TRANSITION_STATE_PRE_HUD_CHECKS)
					HUD_CHANGE_STATE(HUD_STATE_PRE_HUD_CHECKS)
					SET_PAUSE_MENU_WARP_FROM_MODE(GAMEMODE_SP)
					SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM)
				ENDIF
				
				//Removed so we don't keep trying to enter the session we were blocked from bug 2461578
				IF g_Private_Players_FM_SESSION_Menu_Choice_PERSIST != FM_SESSION_MENU_CHOICE_JOIN_FRIEND_SESSION 
					SET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE(g_Private_Players_FM_SESSION_Menu_Choice_PERSIST)
				ENDIF
			
				RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
				SET_BAIL_STILL_VALID(FALSE)
				CLEAR_TRANSITION_SESSIONS_ACTIVITY_TYPE()
				CLEAR_TRANSITION_SESSIONS_ACTIVITY_ID()
				IGNORE_INVITE()
				SET_TRANSITION_SESSIONS_RECIEVED_FAILED_TO_LAUNCH()
				g_iExternalWarningScreenStages = 0
				g_Private_BailWarningScreenButtonBitset = 0
				g_bitset_SessionFullQuestion_Available = 0
				g_bitset_SessionFullQuestion_Answer = 0
				SET_LOADING_ICON_INACTIVE()			
				SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED, FALSE)
			
				CLEAR_BIT(g_bitset_SessionFullQuestion_Answer, ENUM_TO_INT(SESSION_FULL_QUESTION_JOIN_QUEUE))
				RETURN WARNING_SCREEN_RETURN_ACCEPT
			
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_bitset_SessionFullQuestion_Answer, ENUM_TO_INT(SESSION_FULL_QUESTION_SPECTATE_PLAYER))
		IF SET_SKYSWOOP_UP()

			IF IS_SAFE_TO_QUIT_BAIL_SCREEN_EXTERNAL_SCREENS()

				CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
				NET_NL()NET_PRINT("BC: RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - INPUT_FRONTEND_ACCEPT going to spectate ")

				SET_CONFIRM_INVITE_FROM_SAME_SESSION_DIFFERENT_TEAMS(FALSE)
				SET_TRANSITION_TO_IGNORE_LAUNCH_SCRIPT_START(FALSE) 
				SET_CURRENT_TRANSITION_BAILING_OPTIONS(BAILING_OPTIONS_EMPTY)
				TELL_TRANSITION_BAIL_HAPPENED(FALSE)
				SET_CONFIRM_INVITE_INTO_GAME_STATE(TRUE)
				SET_INVITED_TO_SCTV(TRUE)
				SET_ENTERED_GAME_AS_SCTV(TRUE)
				SETUP_MATCHMAKING_RULES(GAMEMODE_FM)
				
				
				NET_SESSION_SET_GAMEMODE(GAMEMODE_FM)
				g_bitset_SessionFullQuestion_Available = 0
				g_bitset_SessionFullQuestion_Answer = 0
				CLEAR_TRANSITION_SESSIONS_STARTED_EVENT_FAILED()
				CLEAR_TRANSITION_SESSIONS_CORONA_CONTROLLER_MAINTAIN_CAMERA()
				SET_BAILED_HAPPENED_SOMEHOW(FALSE) //Already dealt with the bail, if we enter teh transition don't show again.
			
				IF IS_INVITE_IS_TRANSITION_INVITE()
				OR TRANSITION_SESSIONS_STARTED_EVENT_FAILED()
					PRINTLN(" RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - NETWORK_JOIN_PREVIOUSLY_FAILED_TRANSITION - called ")
					NETWORK_JOIN_PREVIOUSLY_FAILED_TRANSITION()
				ELSE
					PRINTLN(" RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - NETWORK_JOIN_PREVIOUSLY_FAILED_SESSION - called ")
					NETWORK_JOIN_PREVIOUSLY_FAILED_SESSION()
				ENDIF
				SET_MAINTRANSITION_KILLING_MULTIPLAYER_SCRIPTS(TRUE)
				SET_TRANSITION_TO_IGNORE_LAUNCH_SCRIPT_START(FALSE)
				
				SET_NEED_TO_JOIN_SESSION_FROM_INVITE(FALSE)
				
				IF TRANSITION_SESSIONS_RECIEVED_JOIN_FAILED_WITH_DEAD_FREEMODE()
					SET_HAS_JOIN_FAILED_WITH_DEAD_FREEMODE(TRUE)
				ENDIF
				
				SET_JOINING_FULL_SESSION_AS_SPECTATOR(TRUE)
				
				IF IS_INVITE_IS_TRANSITION_INVITE()
					
					SET_MAINTRANSITION_PROCESSING_AN_SCTV_TRANSITION_INVITE(TRUE)
					NETWORK_SESSION_SET_MATCHMAKING_GROUP(MM_GROUP_SCTV)
					PRINTLN("[TS] BC: NETWORK_SESSION_SET_MATCHMAKING_GROUP(MM_GROUP_SCTV) called when changing to a spectate invite.  ")
				ENDIF
				
				
				SET_BAIL_STILL_VALID(FALSE)
			
				SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED, FALSE)
				g_iExternalWarningScreenStages = 0
				g_Private_BailWarningScreenButtonBitset = 0
				
				SET_FRONTEND_ACTIVE(FALSE)
				SET_SKYFREEZE_FROZEN()
				
				SET_QUIT_CURRENT_GAME(TRUE)
				
				
				
				SWITCH_SCTV_MODE(SCTV_MODE_INIT)
				MPSpecGlobals.SCTVData.stage = SCTV_STAGE_CLEANUP
				
				

				
				
				IF NOT IS_TRANSITION_ACTIVE()
					SET_PAUSE_MENU_WARP_FROM_MODE(GAMEMODE_SP)
					SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM)

					IF IS_TRANSITION_ACTIVE() = FALSE
						TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)				
					ENDIF
				ELSE
					TRANSITION_CHANGE_STATE(TRANSITION_STATE_WAIT_JOIN_FM_SESSION)
					HUD_CHANGE_STATE(HUD_STATE_LOADING)
				ENDIF
				PRINTLN(" RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - INPUT_FRONTEND_ACCEPT bRunGroupFullChangeTeam = TRUE")
				CLEAR_BIT(g_bitset_SessionFullQuestion_Answer, ENUM_TO_INT(SESSION_FULL_QUESTION_SPECTATE_PLAYER))
				RETURN WARNING_SCREEN_RETURN_ACCEPT
			ENDIF
		ENDIF
	ENDIF
	
	
	
	IF IS_BIT_SET(g_bitset_SessionFullQuestion_Answer, ENUM_TO_INT(SESSION_FULL_QUESTION_CANCELLED))
	

		IF SET_SKYSWOOP_UP()
		AND IS_AUTO_SAVE_IN_PROGRESS() = FALSE
		AND SHOULD_END_SINGLEPLAYER() = FALSE
		AND IS_SAFE_TO_QUIT_BAIL_SCREEN_EXTERNAL_SCREENS()
		
			g_bitset_SessionFullQuestion_Available = 0
			g_bitset_SessionFullQuestion_Answer = 0
			
			SET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE(FM_SESSION_MENU_CHOICE_JOIN_PUBLIC_SESSION)
		
			SET_QUIT_CURRENT_GAME(TRUE)
			PRINTLN(" RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - INPUT_FRONTEND_CANCEL g_b_HaveCancelledToEnterFullSession = TRUE so heading into normal FM ")
			
			IF IS_TRANSITION_ACTIVE()
			
				TRANSITION_CHANGE_STATE(TRANSITION_STATE_TERMINATE_SESSION)
				HUD_CHANGE_STATE(HUD_STATE_LOADING)
				SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM)
				SET_JOINING_GAMEMODE(GAMEMODE_FM)
				
				
				#IF IS_DEBUG_BUILD
					DEBUG_PRINTCALLSTACK()
					NET_NL()NET_PRINT("<<<SET_CURRENT_TRANSITION_FM_MENU_CHOICE - set to FM_MENU_CHOICE_JOIN_NEW_SESSION , IS_TRANSITION_ACTIVE = TRUE ")
					DEBUG_PRINTCALLSTACK()
					NET_NL()NET_PRINT("<<<SET_CURRENT_TRANSITION_FM_MENU_CHOICE - set to ")NET_PRINT(GET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE_STRING(g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY))
				#ENDIF
				g_Private_Players_FM_Menu_Choice = FM_MENU_CHOICE_JOIN_NEW_SESSION

				//Removed so we don't keep trying to enter the session we were blocked from bug 2461578
				IF g_Private_Players_FM_SESSION_Menu_Choice_PERSIST != FM_SESSION_MENU_CHOICE_JOIN_FRIEND_SESSION 
					SET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE(g_Private_Players_FM_SESSION_Menu_Choice_PERSIST)
				ENDIF
			ELSE
			
				#IF IS_DEBUG_BUILD
					DEBUG_PRINTCALLSTACK()
					NET_NL()NET_PRINT("<<<SET_CURRENT_TRANSITION_FM_MENU_CHOICE - set to FM_MENU_CHOICE_JOIN_NEW_SESSION , IS_TRANSITION_ACTIVE = FALSE ")
					DEBUG_PRINTCALLSTACK()
					NET_NL()NET_PRINT("<<<SET_CURRENT_TRANSITION_FM_MENU_CHOICE - set to ")NET_PRINT(GET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE_STRING(g_Private_Players_FM_SESSION_Menu_Choice_LAST_ENTRY))
				#ENDIF
				g_Private_Players_FM_Menu_Choice = FM_MENU_CHOICE_JOIN_NEW_SESSION

				SET_CURRENT_TRANSITION_FM_SESSION_MENU_CHOICE(g_Private_Players_FM_SESSION_Menu_Choice_PERSIST)
			
				CLEAR_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE() //Starting the maintransition so that script will pull it back down. bug 2229436
				SET_PAUSE_MENU_REQUESTING_A_WARP()
				SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM)
				SET_JOINING_GAMEMODE(GAMEMODE_FM)
				TRIGGER_TRANSITION_MENU_ACTIVE(TRUE)	
			ENDIF
					
			
			
			
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
			ENDIF
			CLEAR_TRANSITION_SESSIONS_ACTIVITY_TYPE()
			CLEAR_TRANSITION_SESSIONS_ACTIVITY_ID()
			IGNORE_INVITE()
						
			SET_TRANSITION_SESSIONS_RECIEVED_FAILED_TO_LAUNCH()
			g_iExternalWarningScreenStages = 0
			g_Private_BailWarningScreenButtonBitset = 0
			SET_LOADING_ICON_INACTIVE()			
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED, FALSE)
			SET_BAIL_HAPPENED_DURING_JOIN(FALSE)

			//Put the cam on the ground
			IF DID_TRANSITION_SESSION_PUT_CAM_IN_SKY_FOR_SESSION_CHANGE()
			OR SHOULD_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
				CLEAR_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
				CLEAR_TRANSITION_SESSION_PUT_CAM_IN_SKY_FOR_SESSION_CHANGE()
				IF NOT IS_TRANSITION_MENU_TRIGGERED()  //Transition session doesn't need to handle this, the maintransition will 2316858. 
					SET_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
					IF NETWORK_IS_GAME_IN_PROGRESS()
					AND NOT NETWORK_IS_ACTIVITY_SESSION()
						SET_TRANSITION_SESSION_RETURN_PLAYER_CONTROL_AFTER_CAM_DOWN()
					ENDIF
				ENDIF
			ENDIF
			SET_BAIL_STILL_VALID(FALSE)
			g_sPlayerPedRequest.eState = PR_STATE_PROCESSING
			#if not USE_CLF_DLC
			#if not USE_NRM_DLC		
				IF g_MissionLeftSPIn != SP_MISSION_PROLOGUE
				AND g_MissionLeftSPIn != SP_MISSION_MICHAEL_1
				AND g_MissionLeftSPIn != SP_MISSION_ARMENIAN_1	
					IF IS_SCREEN_FADED_OUT()
					OR IS_SCREEN_FADING_OUT()			
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
				ENDIF
			#endif
			#endif
			
			CLEAR_BIT(g_bitset_SessionFullQuestion_Answer, ENUM_TO_INT(SESSION_FULL_QUESTION_CANCELLED))
			RETURN WARNING_SCREEN_RETURN_CANCEL
		ELSE
			#IF IS_DEBUG_BUILD
				IF IS_AUTO_SAVE_IN_PROGRESS()
					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - CANCEL - IS_AUTO_SAVE_IN_PROGRESS = TRUE, waiting  ")
				ENDIF
				IF SHOULD_END_SINGLEPLAYER()
					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - CANCEL - SHOULD_END_SINGLEPLAYER = TRUE, waiting  ")
				ENDIF
			#ENDIF
		ENDIF
	
	ENDIF

	
	
	IF INT_TO_ENUM(JOIN_RESPONSE_CODE, g_Private_BailWarningScreenEventParam)  = RESPONSE_DENY_AIM_PREFERENCE
	OR g_Private_BailWarningScreenEventParam = ENUM_TO_INT(RESPONSE_DENY_AIM_PREFERENCE)
//	IF g_b_Private_NumberofAttemptsToChangeAimType = 0
		IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, CANCEL)
			IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_CANCEL) 
				CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)

				PLAY_SOUND_FRONTEND(-1, "CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET") 
				
				#IF IS_DEBUG_BUILD
				IF bRunGroupFullChangeTeam
					PRINTLN(" RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - INPUT_FRONTEND_CANCEL bRunGroupFullChangeTeam = TRUE")
				ENDIF
				#ENDIF
				
				IF bRunGroupFullChangeTeam
				AND IS_TRANSITION_ACTIVE()
				
					PRINTLN(" RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - INPUT_FRONTEND_CANCEL bRunGroupFullChangeTeam = TRUE so heading back to prehud checks ")
					TRANSITION_CHANGE_STATE(TRANSITION_STATE_PRE_HUD_CHECKS)
					HUD_CHANGE_STATE(HUD_STATE_PRE_HUD_CHECKS)
					SET_PAUSE_MENU_WARP_FROM_MODE(GAMEMODE_SP)
					SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM)
				ENDIF
				
				IF NOT NETWORK_IS_GAME_IN_PROGRESS()
					RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
				ENDIF
				CLEAR_TRANSITION_SESSIONS_ACTIVITY_TYPE()
				CLEAR_TRANSITION_SESSIONS_ACTIVITY_ID()
				IGNORE_INVITE()
				SET_TRANSITION_SESSIONS_RECIEVED_FAILED_TO_LAUNCH()
				g_iExternalWarningScreenStages = 0
				g_Private_BailWarningScreenButtonBitset = 0
				SET_LOADING_ICON_INACTIVE()			
				SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED, FALSE)
				
				//Put the cam on the ground
				IF DID_TRANSITION_SESSION_PUT_CAM_IN_SKY_FOR_SESSION_CHANGE()
				OR SHOULD_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
					CLEAR_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
					CLEAR_TRANSITION_SESSION_PUT_CAM_IN_SKY_FOR_SESSION_CHANGE()
					SET_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
					IF NETWORK_IS_GAME_IN_PROGRESS()
					AND NOT NETWORK_IS_ACTIVITY_SESSION()
						SET_TRANSITION_SESSION_RETURN_PLAYER_CONTROL_AFTER_CAM_DOWN()
					ENDIF
				ENDIF
				
				RETURN WARNING_SCREEN_RETURN_CANCEL
			ENDIF
		ENDIF
	ENDIF
	 

	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge =  "TRAN_JOINFAIL"
	STRING sLineTwoMessge = "TRAN_RETNFM"
	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - GET_CURRENT_GAMEMODE() = ")NET_PRINT(GET_GAMEMODE_STRING(GET_CURRENT_GAMEMODE())) 
	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
	OR IS_INVITE_ACCEPTED_IN_GTAO()
	OR GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR
	OR GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY
	OR GET_CURRENT_GAMEMODE() = GAMEMODE_SP
		sLineTwoMessge = "TRAN_RETNFM"
	
	ENDIF
	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_EMPTY
	AND GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_IS_FM_AND_TRANSITION_READY
		sLineTwoMessge = "HUD_RETURNSP"
	ENDIF
	
	IF g_Private_BailWarningScreenEventParam > -1
		STRING JoinFailSubReason = JOIN_SESSION_FAILED_REASON_STRING(g_Private_BailWarningScreenEventParam, TEAM_FREEMODE)  
		IF IS_STRING_EMPTY_HUD(JoinFailSubReason) = FALSE
			sLineOneMessge = JoinFailSubReason
		ENDIF
		NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED - g_Private_BailWarningScreenEventParam = ")NET_PRINT_INT(g_Private_BailWarningScreenEventParam) 
	endif
	
	
	FE_WARNING_FLAGS Buttons = FE_WARNING_CONTINUE
	
	IF INT_TO_ENUM(JOIN_RESPONSE_CODE, g_Private_BailWarningScreenEventParam)  = RESPONSE_DENY_AIM_PREFERENCE
	OR g_Private_BailWarningScreenEventParam = ENUM_TO_INT(RESPONSE_DENY_AIM_PREFERENCE)
	
		sLineTwoMessge = "HUD_CNGSET"
		Buttons = FE_WARNING_YESNO
	ENDIF
	
	
	IF bRunGroupFullChangeTeam
		IF IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_SPECTATE_PLAYER))
		AND IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_JOIN_QUEUE))
			sLineTwoMessge = "HUD_CNGQUEUSP"
		ELIF IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_JOIN_QUEUE))
			sLineTwoMessge = "HUD_CNGQUEU"
		ELIF IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_SPECTATE_PLAYER))
			sLineTwoMessge = "HUD_CNGSPEC"
		ENDIF
		
		IF SET_SKYSWOOP_UP()
		AND IS_SAFE_TO_QUIT_BAIL_SCREEN_EXTERNAL_SCREENS()
			IF IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_SPECTATE_PLAYER))
			AND IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_JOIN_QUEUE))
				Buttons = FE_WARNING_FULL_SESSION_BOTH
			ELIF IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_JOIN_QUEUE))
				Buttons = FE_WARNING_YESNO
			ELIF IS_BIT_SET(g_bitset_SessionFullQuestion_Available, ENUM_TO_INT(SESSION_FULL_QUESTION_SPECTATE_PLAYER))
				Buttons = FE_WARNING_YESNO
			ENDIF
		ELSE
			Buttons = FE_WARNING_SPINNER_ONLY
		ENDIF
	ENDIF
	

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, Buttons , sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC




FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF()
	
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	
	
	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE")NET_PRINT_BOOL(NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE())
	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: NETWORK_HAS_PENDING_INVITE")NET_PRINT_BOOL(NETWORK_HAS_PENDING_INVITE())

	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("[BCWHEEL]  RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF - NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE ", NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE())								
	PRINTLN_FINAL("[BCWHEEL]  RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF - NETWORK_HAS_PENDING_INVITE ", NETWORK_HAS_PENDING_INVITE())								
	#ENDIF

	IF NETWORK_HAS_PENDING_INVITE() = FALSE
	
		NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: No longer has a valid invite. Leaving this warning screen ")

	
		g_iExternalWarningScreenStages = 0
		g_Private_BailWarningScreenButtonBitset = 0
		SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF, FALSE)
		RETURN WARNING_SCREEN_RETURN_NONE
	ENDIF
	
	

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

//	IF g_b_Private_NumberofAttemptsToChangeAimType != 2
		IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
			SET_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
		ENDIF
//	ENDIF


	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	
			NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF - ACCEPT PRESSED ")
			PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET") 
			TARGETING_MODE_TYPE Aimtype
			
			SET_PREVIOUS_AIM_STATE_BEFORE_CHANGE(GET_LOCAL_PLAYER_AIM_STATE())
			
			IF GET_ACCEPTED_INVITE_AIM_STATE() = ENUM_TO_INT(TARGETING_OPTION_GTA_TRADITIONAL) 
			OR GET_ACCEPTED_INVITE_AIM_STATE() = ENUM_TO_INT(TARGETING_OPTION_ASSISTED_AIM) 
				Aimtype = TARGETING_OPTION_ASSISTED_AIM 
			ELIF GET_ACCEPTED_INVITE_AIM_STATE() = ENUM_TO_INT(TARGETING_OPTION_ASSISTED_FREEAIM)
				Aimtype = TARGETING_OPTION_ASSISTED_FREEAIM 
			ELSE
				Aimtype = TARGETING_OPTION_FREEAIM
			ENDIF
		
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[AIMPREF] RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: GET_LOCAL_PLAYER_AIM_STATE() BEFORE the call = ")NET_PRINT_INT(GET_LOCAL_PLAYER_AIM_STATE())
			
			NET_NL()NET_PRINT("[AIMPREF] RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: GET_CONFIRM_INVITE_AIM_STATE() = ")NET_PRINT_INT(GET_CONFIRM_INVITE_AIM_STATE())
			IF Aimtype = TARGETING_OPTION_FREEAIM
				NET_NL()NET_PRINT("[AIMPREF] RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: Changed aim Type to Aimtype = TARGETING_OPTION_FREEAIM")
			ELIF Aimtype = TARGETING_OPTION_ASSISTED_AIM
				NET_NL()NET_PRINT("[AIMPREF] RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: Changed aim Type to Aimtype = TARGETING_OPTION_ASSISTED_AIM")
			ELIF Aimtype = TARGETING_OPTION_ASSISTED_FREEAIM 
				NET_NL()NET_PRINT("[AIMPREF] RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: Changed aim Type to Aimtype = TARGETING_OPTION_ASSISTED_FREEAIM")
			ENDIF
			#ENDIF
			
			#IF USE_FINAL_PRINTS
				PRINTLN_FINAL("[AIMPREF] RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: GET_LOCAL_PLAYER_AIM_STATE() BEFORE the call = ", GET_LOCAL_PLAYER_AIM_STATE())
				PRINTLN_FINAL("[AIMPREF] RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: GET_CONFIRM_INVITE_AIM_STATE() = ", GET_CONFIRM_INVITE_AIM_STATE())
				IF Aimtype = TARGETING_OPTION_FREEAIM
					PRINTLN_FINAL("[AIMPREF] RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: Changed aim Type to Aimtype = TARGETING_OPTION_FREEAIM")
				ELIF Aimtype = TARGETING_OPTION_ASSISTED_AIM
					PRINTLN_FINAL("[AIMPREF] RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: Changed aim Type to Aimtype = TARGETING_OPTION_ASSISTED_AIM")
				ELIF Aimtype = TARGETING_OPTION_ASSISTED_FREEAIM 
					PRINTLN_FINAL("[AIMPREF] RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: Changed aim Type to Aimtype = TARGETING_OPTION_ASSISTED_FREEAIM")
				ENDIF
			#ENDIF
		
			//0 = hardlock, 1 = softlock, 2 = assisted freeaim 3 = free aim
			SET_PLAYER_TARGETING_MODE(Aimtype)
			
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[AIMPREF] RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: GET_LOCAL_PLAYER_AIM_STATE() aFTER the call = ")NET_PRINT_INT(GET_LOCAL_PLAYER_AIM_STATE())
			#ENDIF
			
			//Recheck the aim types will be accepted by the join invite. 				
			IF DO_INVITE_AND_PLAYERS_AIMTYPE_MATCH_FROM_INVITE_DIRECTLY(GET_ACCEPTED_INVITE_AIM_STATE())
			
				#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: Proceed MODES MATCH ")
				#ENDIF
				
				IF NETWORK_HAS_CONFIRMED_INVITE()
					NETWORK_REQUEST_INVITE_CONFIRMED_EVENT()
				ELSE
					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: NETWORK_HAS_CONFIRMED_INVITE = FALSE - can't confirm invite here. ")
				ENDIF
			
				RESET_EVENT_APP_LAUNCH_FLAG_RECIEVED()


//				g_TransitionSessionNonResetVars.sTransVars.bCleanedUpAndWait = FALSE	
//				SET_TRANSITION_SESSIONS_ACCEPTING_INVITE_WAIT_FOR_CLEAN()	
				
				SET_AIMTYPE_ALTERED_BY_INVITE(TRUE)
								
				g_iExternalWarningScreenStages = 0
				g_Private_BailWarningScreenButtonBitset = 0
				SET_LOADING_ICON_INACTIVE()			
				SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF, FALSE)				
								
				RETURN WARNING_SCREEN_RETURN_ACCEPT				
								
			ELSE
			

			
//				IF g_b_Private_NumberofAttemptsToChangeAimType = 0
//					#IF IS_DEBUG_BUILD
//					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_JOIN_FAILED: Fail once  ")
//					#ENDIF
//				
//					g_b_Private_NumberofAttemptsToChangeAimType =1
//					RETURN WARNING_SCREEN_RETURN_NONE
//				
//				ELSE
					
					
					#IF IS_DEBUG_BUILD
					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF: Fail again, accept to go back to session  ")
					#ENDIF
				
					IF NOT NETWORK_IS_GAME_IN_PROGRESS()
						RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
					ENDIF
					CLEAR_TRANSITION_SESSIONS_ACTIVITY_TYPE()
					CLEAR_TRANSITION_SESSIONS_ACTIVITY_ID()
					IGNORE_INVITE()
					IF GET_CORONA_STATUS() != CORONA_STATUS_IDLE
						SET_TRANSITION_SESSIONS_RECIEVED_FAILED_TO_LAUNCH()
					ENDIF
					g_iExternalWarningScreenStages = 0
					g_Private_BailWarningScreenButtonBitset = 0
					SET_LOADING_ICON_INACTIVE()			
					SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF, FALSE)
					
					//Put the cam on the ground
					IF DID_TRANSITION_SESSION_PUT_CAM_IN_SKY_FOR_SESSION_CHANGE()
					OR SHOULD_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
						CLEAR_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
						CLEAR_TRANSITION_SESSION_PUT_CAM_IN_SKY_FOR_SESSION_CHANGE()
						SET_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
					ENDIF
//					g_b_Private_NumberofAttemptsToChangeAimType = 0
					RETURN WARNING_SCREEN_RETURN_CANCEL
					
//				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	
//	IF g_b_Private_NumberofAttemptsToChangeAimType = 0
		IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, CANCEL)
			IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_CANCEL) 
				CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
				PLAY_SOUND_FRONTEND(-1, "CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET") 

				IF NOT NETWORK_IS_GAME_IN_PROGRESS()
					RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
				ENDIF
				IF IS_PLAYER_SCTV(PLAYER_ID())
					SET_EXTERNAL_SCTV_QUIT_HAS_BEEN_CANCELLED(TRUE)
				ENDIF
				CLEAR_TRANSITION_SESSIONS_ACTIVITY_TYPE()
				CLEAR_TRANSITION_SESSIONS_ACTIVITY_ID()
				IGNORE_INVITE()
				SET_TRANSITION_SESSIONS_RECIEVED_FAILED_TO_LAUNCH()
				g_iExternalWarningScreenStages = 0
				g_Private_BailWarningScreenButtonBitset = 0
				SET_LOADING_ICON_INACTIVE()			
				SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF, FALSE)
				
				NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF - CANCEL PRESSED ")

				IF NETWORK_IS_ACTIVITY_SESSION()
					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF - CANCEL PRESSED but in an activity session, totally broke so calling NETWORK_BAIL ")
					NETWORK_BAIL(ENUM_TO_INT(NETWORK_BAIL_CANCELLED_AIMTYPE_WHEN_ALREADY_IN_ACTIVITY))
				ENDIF

				
				//Put the cam on the ground
				IF DID_TRANSITION_SESSION_PUT_CAM_IN_SKY_FOR_SESSION_CHANGE()
				OR SHOULD_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
					CLEAR_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
					CLEAR_TRANSITION_SESSION_PUT_CAM_IN_SKY_FOR_SESSION_CHANGE()
					SET_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
				ENDIF
				
				RETURN WARNING_SCREEN_RETURN_CANCEL
			ENDIF
		ENDIF
//	ENDIF

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge =  "HUD_CNGSETTIN"
	STRING sLineTwoMessge = "HUD_RETURNSP" //"TRAN_RETNFM"
	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF - GET_CURRENT_GAMEMODE() = ")NET_PRINT(GET_GAMEMODE_STRING(GET_CURRENT_GAMEMODE())) 
	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
	OR IS_INVITE_ACCEPTED_IN_GTAO()
	
		sLineTwoMessge = "TRAN_RETNFM"
	
	ENDIF
	
	
	FE_WARNING_FLAGS Buttons = FE_WARNING_CONTINUE
	
//	IF g_b_Private_NumberofAttemptsToChangeAimType = 0
		sLineTwoMessge = "HUD_CNGSET"
		Buttons = FE_WARNING_YESNO
//	ELSE
//		sLineOneMessge = "HUD_CNGSETFAIL"
//		sLineTwoMessge = "HUD_RETURNSP"
//		IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
//		OR IS_INVITE_ACCEPTED_IN_GTAO()
//	
//			sLineTwoMessge = "TRAN_RETNFM"
//		ENDIF
//	ENDIF
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, Buttons , sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC

FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1	
	
	NET_NL()NET_PRINT("[BOSSMM] RUN_WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES: NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE")NET_PRINT_BOOL(NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE())
	NET_NL()NET_PRINT("[BOSSMM] RUN_WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES: NETWORK_HAS_PENDING_INVITE")NET_PRINT_BOOL(NETWORK_HAS_PENDING_INVITE())

	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("[BOSSMM] [BCWHEEL]  RUN_WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES - NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE ", NETWORK_SESSION_IS_AWAITING_INVITE_RESPONSE())								
	PRINTLN_FINAL("[BOSSMM] [BCWHEEL]  RUN_WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES - NETWORK_HAS_PENDING_INVITE ", NETWORK_HAS_PENDING_INVITE())								
	#ENDIF
	
	IF g_FMMC_STRUCT.iRootContentIDHash != 0
		g_FMMC_STRUCT.iRootContentIDHash = 0
		PRINTLN("RUN_WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES - g_FMMC_STRUCT.iRootContentIDHash = 0")
	ENDIF
	
	IF NETWORK_HAS_PENDING_INVITE() = FALSE
	
		NET_NL()NET_PRINT("[BOSSMM] RUN_WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES: No longer has a valid invite. Leaving this warning screen ")
	
		g_iExternalWarningScreenStages = 0
		g_Private_BailWarningScreenButtonBitset = 0
		SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES, FALSE)
		RETURN WARNING_SCREEN_RETURN_NONE
	ENDIF

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
	ENDIF
	
	//Accept - Join session
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	
			NET_NL()NET_PRINT("[BOSSMM]  RUN_WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES - ACCEPT PRESSED ")
			PLAY_SOUND_FRONTEND(-1, "SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			g_sTransitionSessionData.bRemoveBossPriviligesOnEntryToFM = TRUE
			
			#IF IS_DEBUG_BUILD
			NET_NL()NET_PRINT("[BOSSMM] RUN_WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES: Proceed Boss priviliges revoked.")
			#ENDIF
		
			NETWORK_REQUEST_INVITE_CONFIRMED_EVENT()		

							
			g_iExternalWarningScreenStages = 0
			g_Private_BailWarningScreenButtonBitset = 0
			SET_LOADING_ICON_INACTIVE()			
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES, FALSE)				
							
			RETURN WARNING_SCREEN_RETURN_ACCEPT	
		ENDIF
	ENDIF
	
	//Cancel - Return to my session
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, CANCEL)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_CANCEL) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
			PLAY_SOUND_FRONTEND(-1, "CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET") 

			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				RESET_EVERYTHING_TRANSITION_SESSION_FOR_SP_BAIL()
			ENDIF
			IF IS_PLAYER_SCTV(PLAYER_ID())
				SET_EXTERNAL_SCTV_QUIT_HAS_BEEN_CANCELLED(TRUE)
			ENDIF
			CLEAR_TRANSITION_SESSIONS_ACTIVITY_TYPE()
			CLEAR_TRANSITION_SESSIONS_ACTIVITY_ID()
			IGNORE_INVITE()
			SET_TRANSITION_SESSIONS_RECIEVED_FAILED_TO_LAUNCH()
			g_iExternalWarningScreenStages = 0
			g_Private_BailWarningScreenButtonBitset = 0
			SET_LOADING_ICON_INACTIVE()			
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES, FALSE)
			
			NET_NL()NET_PRINT("[BOSSMM] WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES - CANCEL PRESSED ")
			
			//Put the cam on the ground
			IF DID_TRANSITION_SESSION_PUT_CAM_IN_SKY_FOR_SESSION_CHANGE()
			OR SHOULD_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
				CLEAR_TRANSITION_SESSION_PULL_CAM_UP_FOR_SESSION_CHANGE()
				CLEAR_TRANSITION_SESSION_PUT_CAM_IN_SKY_FOR_SESSION_CHANGE()
				SET_TRANSITION_SESSION_PULL_CAM_DOWN_FOR_SESSION_CHANGE()
			ENDIF
			
			RETURN WARNING_SCREEN_RETURN_CANCEL
		ENDIF
	ENDIF
	
	STRING sStatementText = "HUD_CONNPROB"
	STRING sLineOneMessge = "HUD_BOSSWARN"
	STRING sLineTwoMessge = "HUD_CNGBOSS"
	NET_NL()NET_PRINT("[BOSSMM] RUN_WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES - GET_CURRENT_GAMEMODE() = ")NET_PRINT(GET_GAMEMODE_STRING(GET_CURRENT_GAMEMODE())) 
	
	FE_WARNING_FLAGS Buttons = FE_WARNING_CONTINUEBACK
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, Buttons , sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE
ENDFUNC


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_BLOCKED_UNDERAGE()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_BLOCKED_UNDERAGE - running ")
	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("[BCWHEEL]  RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_BLOCKED_UNDERAGE - running  ")								
	#ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			g_iExternalWarningScreenStages = 0
			CLEAR_INVITE_BLOCKED_DUE_TO_BEING_ON_TUTORIAL()
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
			  NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ELSE
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ENDIF
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE				
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			SET_LOADING_ICON_INACTIVE()				
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_UNDERAGE, FALSE)
			RETURN WARNING_SCREEN_RETURN_ACCEPT
			
		ENDIF
	ENDIF
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_PERM"
	STRING sLineTwoMessge = "HUD_RETURNSP"
	
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE , sLineTwoMessge)
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_DROPINVITE_NO_TUNEABLE()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_NO_TUNEABLE - running ")
	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("[BCWHEEL]  RUN_WARNINGSCREEN_DROPINVITE_NO_TUNEABLE - running  ")								
	#ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			g_iExternalWarningScreenStages = 0
			CLEAR_INVITE_BLOCKED_DUE_TO_BEING_ON_TUTORIAL()
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
			  NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ELSE
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ENDIF
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			IGNORE_INVITE()
			SET_LOADING_ICON_INACTIVE()				
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_NO_TUNEABLE, FALSE)
			RETURN WARNING_SCREEN_RETURN_ACCEPT
			
		ENDIF
	ENDIF
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_NOTUNE"
	STRING sLineTwoMessge = "HUD_SPRETRNFRSH"
	
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE , sLineTwoMessge)
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC

FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_DROPINVITE_NO_BACKGROUND()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_NO_BACKGROUND - running ")
	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("[BCWHEEL]  RUN_WARNINGSCREEN_DROPINVITE_NO_BACKGROUND - running  ")								
	#ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			g_iExternalWarningScreenStages = 0
			CLEAR_INVITE_BLOCKED_DUE_TO_BEING_ON_TUTORIAL()
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
			  NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ELSE
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			NETWORK_REQUEST_CLOUD_BACKGROUND_SCRIPTS()
			NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_NO_BACKGROUND - NETWORK_REQUEST_CLOUD_BACKGROUND_SCRIPTS() called ")

			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			IGNORE_INVITE()
			SET_LOADING_ICON_INACTIVE()				
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_NO_BACKGROUND, FALSE)
			RETURN WARNING_SCREEN_RETURN_ACCEPT
			
		ENDIF
	ENDIF
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_NOBACKGRN"
	STRING sLineTwoMessge = "HUD_SPRETRNFRSH"
	
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE , sLineTwoMessge)
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC



FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_DROPINVITE_SAVE_TRANSFER_IN_PROGRESS()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_SAVE_TRANSFER_IN_PROGRESS - running ")
	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("[BCWHEEL]  RUN_WARNINGSCREEN_DROPINVITE_SAVE_TRANSFER_IN_PROGRESS - running  ")								
	#ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			g_iExternalWarningScreenStages = 0
			CLEAR_INVITE_BLOCKED_DUE_TO_BEING_ON_TUTORIAL()
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
			  NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ELSE
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ENDIF
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			IGNORE_INVITE()
			SET_LOADING_ICON_INACTIVE()				
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SAVE_TRANSFER_IN_PROGRESS, FALSE)
			RETURN WARNING_SCREEN_RETURN_ACCEPT
			
		ENDIF
	ENDIF
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_ST_INVITE"
	STRING sLineTwoMessge = "HUD_QURETSP"
	
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE , sLineTwoMessge)
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC


CONST_INT ciDROPINVITE_COMPATFAIL_STAGE_INT 			0
CONST_INT ciDROPINVITE_COMPATFAIL_STAGE_STORE_OPEN 		1
CONST_INT ciDROPINVITE_COMPATFAIL_STAGE_STORE_CLOSED 	2
CONST_INT ciDROPINVITE_COMPATFAIL_STAGE_WAIT_FOR_CLOUD	3
CONST_INT ciDROPINVITE_COMPATFAIL_STAGE_FAILED			4

PROC RUN_COMPAT_SCREEN_CANCEL()

	g_iExternalWarningScreenStages = 0
	CLEAR_INVITE_BLOCKED_DUE_TO_BEING_ON_TUTORIAL()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	  NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ELSE
		SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
				SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			ENDIF
		ENDIF
	ENDIF
	IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
		REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
	ENDIF
	IGNORE_INVITE()
	g_iExternalWarningScreen_Compat_stages = ciDROPINVITE_COMPATFAIL_STAGE_INT
	SET_LOADING_ICON_INACTIVE()			
	g_bAlreadyAskedForCompatInvite = FALSE
	SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_COMPATFAIL, FALSE)
	
ENDPROC

FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL - running ")
	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("[BCWHEEL]  RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL - running  ")								
	#ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
	ENDIF
	
	STRING sStatementText 
	STRING sLineOneMessge
	
	SWITCH g_iExternalWarningScreen_Compat_stages
		
		CASE ciDROPINVITE_COMPATFAIL_STAGE_INT		
			sStatementText = "HUD_CONNPROB" 
			sLineOneMessge = "HUD_COMBATPACK"
			NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL: CASE 0 waiting for input ")
			SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_YESNO)	
			RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
		
			IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
				IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
					CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
					CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL: ACCEPT OPEN_COMMERCE_STORE - called ")
					SHUTDOWN_LOADING_SCREEN()
					OPEN_COMMERCE_STORE("", "COMPAT_PACKS") 
					g_iExternalWarningScreen_Compat_stages = ciDROPINVITE_COMPATFAIL_STAGE_STORE_OPEN			
				ENDIF
			ENDIF
		
			IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, CANCEL)
				IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_CANCEL) 
					CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
					CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL: CANCEL WARNING_SCREEN_RETURN_CANCEL - called ")
					RUN_COMPAT_SCREEN_CANCEL()
					RETURN WARNING_SCREEN_RETURN_CANCEL
				ENDIF
			ENDIF
			
			
			
		BREAK
		
		CASE ciDROPINVITE_COMPATFAIL_STAGE_STORE_OPEN
			NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL: CASE 1 waiting for IS_COMMERCE_STORE_OPEN ")			
			//IF IS_COMMERCE_STORE_OPEN()			
				g_iExternalWarningScreen_Compat_stages = ciDROPINVITE_COMPATFAIL_STAGE_STORE_CLOSED		
				NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL: IS_COMMERCE_STORE_OPEN = TRUE ")			
			//ENDIF
		BREAK
		
		CASE ciDROPINVITE_COMPATFAIL_STAGE_STORE_CLOSED		
			NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL: CASE 1 waiting for NOT IS_COMMERCE_STORE_OPEN ")

			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(0)
			ENDIF
		
			IF NOT IS_COMMERCE_STORE_OPEN()
				
				NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL: NOT OPEN_COMMERCE_STORE - called ")
				
				CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
				CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
				g_iExternalWarningScreen_Compat_stages = ciDROPINVITE_COMPATFAIL_STAGE_WAIT_FOR_CLOUD
			ENDIF		
		BREAK
		
		CASE ciDROPINVITE_COMPATFAIL_STAGE_WAIT_FOR_CLOUD		
			NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL: CASE 2 waiting for HAS_CLOUD_REQUESTS_FINISHED ")
			BOOL bTimedOut
			IF HAS_CLOUD_REQUESTS_FINISHED(bTimedOut, 0)
				g_iExternalWarningScreen_Compat_stages = ciDROPINVITE_COMPATFAIL_STAGE_INT
				g_iExternalWarningScreenStages = 0
				SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_COMPATFAIL, FALSE)
				
				IF DLC_CHECK_COMPAT_PACK_CONFIGURATION() = FALSE
				#IF IS_DEBUG_BUILD
				OR GET_COMMANDLINE_PARAM_EXISTS("sc_Compatfailed_MissingPack")
				#ENDIF
					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL - DOWNLOADED but PLAYER QUIT THE STORE, keep listening. ")
					g_bAlreadyAskedForCompatInvite = FALSE
					RUN_COMPAT_SCREEN_CANCEL()
					RETURN WARNING_SCREEN_RETURN_CANCEL
				ELSE
					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL - DOWNLOADED EVERYTHING FINE, Move on ")
				ENDIF
				
				RETURN WARNING_SCREEN_RETURN_ACCEPT
			ENDIF
			IF bTimedOut
			#IF IS_DEBUG_BUILD
			//Or we should force the fail
			OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceClocudRequestsFailStore")
			#ENDIF
				CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
				CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
				g_iExternalWarningScreen_Compat_stages = ciDROPINVITE_COMPATFAIL_STAGE_FAILED
				NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL - g_iForceDLCStoreSetUp = HAS_CLOUD_REQUESTS_FINISHED bTimedOut = TRUE ")
			ENDIF
		BREAK
		
		CASE ciDROPINVITE_COMPATFAIL_STAGE_FAILED		
			NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL: CASE 3 waiting for OK to be pressed. ")
			sStatementText = "HUD_CONNPROB"
			sLineOneMessge = "HUD_COMBATPACKT" //Timed out when checking current compatibility pack configuration. Please return to Grand Theft Auto V.
			SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK)	
			IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
				IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
					CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
					CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL: ACCEPT that the store cloud failed.  ")
					g_iExternalWarningScreen_Compat_stages = ciDROPINVITE_COMPATFAIL_STAGE_INT
					RUN_COMPAT_SCREEN_CANCEL()		
					RETURN WARNING_SCREEN_RETURN_ACCEPT
				ENDIF
			ENDIF
		BREAK	
	ENDSWITCH
	

	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC



FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_UNDERAGE()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_UNDERAGE - running ")
	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("[BCWHEEL]  RUN_WARNINGSCREEN_TRANSITION_UNDERAGE - running  ")								
	#ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			g_iExternalWarningScreenStages = 0
			
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_UNDERAGE, FALSE)
			RETURN WARNING_SCREEN_RETURN_ACCEPT
			
		ENDIF
	ENDIF
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_PERM"
	STRING sLineTwoMessge = "HUD_RETURNSP"
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE , sLineTwoMessge)
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC




FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN()
	
	CONST_INT ACCEPT 0
//	CONST_INT CANCEL 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN - running ")
	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("[BCWHEEL]  RUN_WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN - running  ")								
	#ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
//	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
//		SET_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
//	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			g_iExternalWarningScreenStages = 0
//			NETWORK_SHOW_ACCOUNT_UPGRADE_UI()
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN, FALSE)
			RETURN WARNING_SCREEN_RETURN_ACCEPT
			
		ENDIF
	ENDIF
	
//	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, CANCEL)
//		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_CANCEL) 
//			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
//			g_iExternalWarningScreenStages = 0
//			
//			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN, FALSE)
//			RETURN WARNING_SCREEN_RETURN_CANCEL
//			
//		ENDIF
//	ENDIF
	
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_PROFILECHNG"
	
//	IF IS_PLAYSTATION_PLATFORM()
//		NETWORK_SHOW_ACCOUNT_UPGRADE_UI()
//	ENDIF

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE)
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC



FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_GAME_UPDATE()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_GAME_UPDATE - running ")

	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_GAME_UPDATE, FALSE)
			RETURN WARNING_SCREEN_RETURN_ACCEPT
			
		ENDIF
	ENDIF
	
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_GAMEUPD"
	
	
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(0)
	ENDIF
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE)
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC




FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_SYSTEM_UPDATE()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_SYSTEM_UPDATE - running ")

	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_SYSTEM_UPDATE, FALSE)
			RETURN WARNING_SCREEN_RETURN_ACCEPT
			
		ENDIF
	ENDIF
	
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_SYSTUPD"
	

	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(0)
	ENDIF
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE)
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC

FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_DROPINVITE_REPLAY_ACTIVE()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_DROPINVITE_REPLAY_ACTIVE - running ")

	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_DROPINVITE_REPLAY_ACTIVE, FALSE)
			RETURN WARNING_SCREEN_RETURN_ACCEPT
			
		ENDIF
	ENDIF
	
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_REPLYINVITE"

	
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(0)
	ENDIF
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE)
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_SAVE_MIGRATION_IN_PROGRESS()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_SAVE_MIGRATION_IN_PROGRESS - running ")

	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	
	IF IS_SKYSWOOP_IN_SKY()
		IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
				CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
				g_iExternalWarningScreenStages = 0
				SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_SAVE_MIGRATION_IN_PROGRESS, FALSE)
				IF HAS_IMPORTANT_STATS_LOADED() = FALSE
					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_SAVE_MIGRATION_IN_PROGRESS - The save transfer bumped the stats from memory, now we should shut down the invites.  ")
					SET_BEEN_IN_GTAO_THIS_BOOT(FALSE)
				ENDIF				
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), HUD_STATE_LOADING)
				
				RETURN WARNING_SCREEN_RETURN_ACCEPT
				
			ENDIF
		ENDIF
	ENDIF
	
	SET_SKYSWOOP_UP()
	
	
	
	STRING sStatementText = "HUD_TRANSPEND" 
	STRING sLineOneMessge = "HUD_SAVEPROGRES"
	STRING sLineTwoMessge = ""

	FE_WARNING_FLAGS Buttons = FE_WARNING_OK
	IF NOT IS_SKYSWOOP_IN_SKY()
		Buttons = FE_WARNING_SPINNER_ONLY
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(0)
	ENDIF
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, Buttons, sLineTwoMessge)
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_NP_CONNECTION()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_NP_CONNECTION - running ")

	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_SYSTEM_UPDATE, FALSE)
			RETURN WARNING_SCREEN_RETURN_ACCEPT
			
		ENDIF
	ENDIF

	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_DISCON" //Disconnected from the Platform servers
	

	
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(0)
	ENDIF
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE)
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_MODS_INSTALLED()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_MODS_INSTALLED - running ")

	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_MOD_INSTALLED, FALSE)
			PLAYSTATS_BAN_ALERT(1)
			RETURN WARNING_SCREEN_RETURN_ACCEPT
			
		ENDIF
	ENDIF

	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_MODINSTAL" //You are attempting to access GTA Online servers with an altered version of the game.
	STRING sLineTwoMessge = "HUD_RETURNSP"

	
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(0)
	ENDIF
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE, sLineTwoMessge)
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC



FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_NO_PS_PLUS()
	
	CONST_INT ACCEPT 0
//	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_NO_PS_PLUS - running ")

	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
//	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
//		SET_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
//	ENDIF

	IF IS_SYSTEM_UI_BEING_DISPLAYED() = FALSE
		IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
				CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
				g_iExternalWarningScreenStages = 0
				
				SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN, FALSE)
				SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_SUBSCRIPTION, FALSE)
				SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD, FALSE)
				
				RETURN WARNING_SCREEN_RETURN_ACCEPT
				
			ENDIF
		ENDIF
	ENDIF
	
//	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, CANCEL)
//		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_CANCEL) 
//			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
//			g_iExternalWarningScreenStages = 0
//			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_SUBSCRIPTION, FALSE)
//			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN, FALSE)
//			RETURN WARNING_SCREEN_RETURN_CANCEL
//			
//		ENDIF
//	ENDIF
	
	IF NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = TRUE
		NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_NO_PS_PLUS - NETWORK_HAVE_PLATFORM_SUBSCRIPTION = TRUE ")
		RETURN WARNING_SCREEN_RETURN_ACCEPT
	ENDIF
	
	IF GET_IS_LOADING_SCREEN_ACTIVE()
		IF IS_SKYSWOOP_IN_SKY()
			SHUTDOWN_LOADING_SCREEN()
			NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_NO_PS_PLUS - SHUTDOWN_LOADING_SCREEN called ")
		ENDIF
	ENDIF
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_PSPLUS"
	
//	IF IS_PLAYSTATION_PLATFORM()
//		NETWORK_SHOW_ACCOUNT_UPGRADE_UI()
//	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(0)
	ENDIF
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	IF NETWORK_IS_SHOWING_SYSTEM_UI_OR_RECENTLY_REQUESTED_UPSELL() = FALSE
		SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE)
	ELSE
		DRAW_RECT(0.0, 0.0, 1.0, 1.0, 0,0,0,255)
		NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_NO_PS_PLUS - DRAW_RECT ")
	ENDIF
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC



FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD - running ")

	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	

	IF IS_SYSTEM_UI_BEING_DISPLAYED() = FALSE
		IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
				CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
				g_iExternalWarningScreenStages = 0
				
				SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN, FALSE)
				SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_SUBSCRIPTION, FALSE)
				SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD, FALSE)
				
				RETURN WARNING_SCREEN_RETURN_ACCEPT
				
			ENDIF
		ENDIF
	ENDIF
	
	
	IF NETWORK_HAVE_PLATFORM_SUBSCRIPTION() = TRUE
		NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD - NETWORK_HAVE_PLATFORM_SUBSCRIPTION = TRUE ")
		RETURN WARNING_SCREEN_RETURN_ACCEPT
	ENDIF
	
	IF GET_IS_LOADING_SCREEN_ACTIVE()
		IF IS_SKYSWOOP_IN_SKY()
			SHUTDOWN_LOADING_SCREEN()
			NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD - SHUTDOWN_LOADING_SCREEN called ")
		ENDIF
	ENDIF
	
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_XBGOLD"
	
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(0)
	ENDIF
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	IF IS_SYSTEM_UI_BEING_DISPLAYED() = FALSE
		SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE)
	ELSE
		DRAW_RECT(0.0, 0.0, 1.0, 1.0, 0,0,0,255)
		NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD - DRAW_RECT ")
	ENDIF
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_PERMISSION()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_PERMISSION - running ")
	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("[BCWHEEL]  RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_PERMISSION - running  ")								
	#ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			g_iExternalWarningScreenStages = 0
			
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_XBOX_PERMISSIONS, FALSE)
			RETURN WARNING_SCREEN_RETURN_ACCEPT
			
		ENDIF
	ENDIF
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_PERM"
	STRING sLineTwoMessge = "HUD_RETURNSP"
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE , sLineTwoMessge)
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC






FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED - running ")

	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			g_iExternalWarningScreenStages = 0
			
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED, FALSE)
			RETURN WARNING_SCREEN_RETURN_ACCEPT
			
		ENDIF
	ENDIF
	
	
	INT isGranted = 0
	UGC_DATE aDate
	BOOL isNotPermanent
	IF NETWORK_HAS_VALID_ROS_CREDENTIALS() = TRUE
	 	NETWORK_ROS_PRIVILEGEID PrivID = RLROS_PRIVILEGEID_BANNED 
		isNotPermanent = NETWORK_HAS_ROS_PRIVILEGE_END_DATE(PrivID, isGranted, aDate)
	ENDIF
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_SCSBANNED"
	STRING sLineTwoMessge = "HUD_RETURNSP"
	
	IF isNotPermanent = FALSE //IS Perm
		sLineOneMessge = "HUD_SCSBANPERM" 
		SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE, sLineTwoMessge)
	ELIF isGranted = 1
		sLineOneMessge =  "HUD_SCSBANX"
		TEXT_LABEL_23 aTlBan =   GET_DATE_FOR_BANNED_ACCT_CREATION_DATE(aDate)
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE, "", FALSE, -1, WARNING_MESSAGE_FIRST_SUBSTRING_IS_LITERAL, aTlBan)
	ELSE
		SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE, sLineTwoMessge)
	ENDIF
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()

	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC





PROC RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_BLOCKED_ON_TUTORIAL()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1
	
	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_BLOCKED_ON_TUTORIAL - running ")

	
	BOOL IsPlayerInMultiplayer  = FALSE
	
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
	OR IS_INVITE_ACCEPTED_IN_GTAO()
		IsPlayerInMultiplayer = TRUE
	ENDIF

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IsPlayerInMultiplayer = FALSE
	AND NOT IS_TRANSITION_ACTIVE()
		IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
			SET_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
		ENDIF
	ENDIF


	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			CLEAR_INVITE_BLOCKED_DUE_TO_BEING_ON_TUTORIAL()
			g_iExternalWarningScreenStages = 0
			
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
			  NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ELSE
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ENDIF
			SET_LOADING_ICON_INACTIVE()	
			
			
			
			IF IsPlayerInMultiplayer = FALSE
				SET_PAUSE_MENU_REQUESTING_A_WARP()
				SET_PAUSE_MENU_REQUESTING_A_NEW_SESSION()
				SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM)
				SET_LOADING_ICON_INACTIVE()
				g_iExternalWarningScreenStages = 0
				SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_STILL_ON_TUTORIAL, FALSE)
			
			ENDIF
			
			
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_STILL_ON_TUTORIAL, FALSE)
		ENDIF
	ENDIF
	
	IF IsPlayerInMultiplayer = FALSE
	AND NOT IS_TRANSITION_ACTIVE()
		IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, CANCEL)
			IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_CANCEL) 
				CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
				CLEAR_INVITE_BLOCKED_DUE_TO_BEING_ON_TUTORIAL()
				
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
				  NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ELSE
					SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
							SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
							FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						ENDIF
					ENDIF
				ENDIF
				SET_LOADING_ICON_INACTIVE()	
				
				IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE					
					REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
				ENDIF
				
				g_iExternalWarningScreenStages = 0
				SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_STILL_ON_TUTORIAL, FALSE)
				
			ENDIF
		ENDIF
	ENDIF
	
	
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "TRAN_ONTUTOR"
	STRING sLineTwoMessge = "HUD_RETURNSP"
	IF IsPlayerInMultiplayer 
		sLineTwoMessge = "TRAN_RETNFM" ///Return to GTA Online. 
	ENDIF
	
	FE_WARNING_FLAGS warningFlags = FE_WARNING_CONTINUE
	IF IsPlayerInMultiplayer = FALSE
	AND NOT IS_TRANSITION_ACTIVE()
		sLineTwoMessge = "TRAN_ENTMP" 
		warningFlags = FE_WARNING_CONTINUERETURNSP
	ENDIF
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, warningFlags , sLineTwoMessge)
	

ENDPROC


PROC RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_NO_CHARACTERS()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_NO_CHARACTERS - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			CLEAR_INVITE_BLOCKED_DUE_TO_BEING_ON_TUTORIAL()
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
			  NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ELSE
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ENDIF
			SET_LOADING_ICON_INACTIVE()				
			
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE		
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF	
			
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_NO_CHARACTERS, FALSE)
		ENDIF
	ENDIF
		
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "TRAN_NOCHAR"
	STRING sLineTwoMessge = "HUD_RETURNSP"
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE , sLineTwoMessge)
	

ENDPROC


PROC RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_DIRTY_READ()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_DIRTY_READ - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
			  NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ELSE
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ENDIF
			SET_LOADING_ICON_INACTIVE()				
			
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF	
			
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_DIRTY_READ, FALSE)
		ENDIF
	ENDIF
	
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "TRAN_DIRTY"
	STRING sLineTwoMessge = "HUD_RETURNSP"
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE , sLineTwoMessge)
	

ENDPROC


PROC RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_SESSION_FULL()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_SESSION_FULL - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
			  NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ELSE
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			SET_LOADING_ICON_INACTIVE()	
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				SET_JOINING_GAMEMODE(GAMEMODE_FM)
				HUD_CHANGE_STATE(HUD_STATE_PRE_HUD_CHECKS)
				TRANSITION_CHANGE_STATE(TRANSITION_STATE_PRE_HUD_CHECKS)
			ENDIF

			
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SESSION_FULL, FALSE)
		ENDIF
	ENDIF
	
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "BAIL_CORONAFUL"
	STRING sLineTwoMessge = "TRAN_RETNFM"
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE , sLineTwoMessge)
	

ENDPROC



PROC RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_CLOUD_DOWN()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_CLOUD_DOWN - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
			  NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ELSE
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			
			
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_CLOUD_DOWN, FALSE)
		ENDIF
	ENDIF
	
	
	
	
	STRING sStatementText = "HUD_CONNPROB" 
// KGM 21/4/14 - changing to an existing message that doesn't mention crating a temporary character
//	STRING sLineOneMessge = "BAIL_CLOUDDN" //Mentions create a new character 
	STRING sLineOneMessge = "HUD_STATFAIL"
	
	STRING sLineTwoMessge = "HUD_RETURNSP"
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		sLineTwoMessge = "TRAN_RETNFM"
	ENDIF
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE , sLineTwoMessge)
	

ENDPROC



FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_QUIT_SINGLEPLAYER()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_QUIT_SINGLEPLAYER - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
			  NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ELSE
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ENDIF
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, CANCEL)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_CANCEL) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
	
			RETURN WARNING_SCREEN_RETURN_CANCEL
		ENDIF
	ENDIF
	
	STRING sStatementText = "PM_QUIT_WARN" 
	STRING sLineOneMessge = "PM_QUIT_WARN2"
	STRING sLineTwoMessge = "PM_QUIT_WARN5"
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_YESNO , sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC 



FUNC WARNING_SCREEN_RETURN WARNING_SCREEN_ACCEPT_NEW_CHARACTER_RANK_BOOST(STRING CharName, INT Slot, INT Rank, INT FixedRank)
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("WARNING_SCREEN_ACCEPT_NEW_CHARACTER_RANK_BOOST - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, CANCEL)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_CANCEL) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
			RETURN WARNING_SCREEN_RETURN_CANCEL
		ENDIF
	ENDIF
	
	STRING sStatementText = "PM_INF_QMFT" 
	
	
	
	STRING sLineOneMessge = "RP_BOOSTER"
	IF FixedRank > -1
		sLineOneMessge = "RP_BOOSTERFIX"
	ENDIF
	
	STRING slotString
	IF Slot = 0
		slotString = "SLOT1"
	ELSE
		slotString = "SLOT2"
	ENDIF
	
	IF IS_STRING_EMPTY_HUD(CharName)
		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(sStatementText, "RP_BOOSTERNO", FE_WARNING_YESNO,DEFAULT, TRUE, Rank, 
														WARNING_MESSAGE_DEFAULT, slotString)
	ELSE
		IF FixedRank = -1
			SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(sStatementText, sLineOneMessge, FE_WARNING_YESNO,DEFAULT, TRUE, Rank, 
														WARNING_MESSAGE_SECOND_SUBSTRING_IS_LITERAL, slotString, CharName)
		ELSE
			SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_YESNO, DEFAULT, TRUE, FixedRank)
		ENDIF
	ENDIF
													
													
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC 



FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_ENTER_SP_UPSELL()

	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_ENTER_SP_UPSELL - running ")

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, CANCEL)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_CANCEL) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
			RETURN WARNING_SCREEN_RETURN_CANCEL
		ENDIF
	ENDIF

	STRING sStatementText = "PM_QUIT_WARN" 
	STRING sLineOneMessge = "UI_ALERT_ENTRSTORE"
	STRING sLineTwoMessge = "PM_QUIT_WARN11"

	IF GET_CURRENT_HUD_STATE() != HUD_STATE_SELECT_CHARACTER_FM
		RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	ENDIF
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_YESNO, sLineTwoMessge)

	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC



FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_QUIT_GTAONLINE()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("[BCWARN] RUN_WARNINGSCREEN_QUIT_GTAONLINE - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND IS_SKYSWOOP_AT_GROUND()
			AND NOT IS_PLAYER_TELEPORT_ACTIVE()
			  NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			ELSE
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, CANCEL)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_CANCEL) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
	
			RETURN WARNING_SCREEN_RETURN_CANCEL
		ENDIF
	ENDIF
	
	IF g_bMissionEnding  
		NET_NL()NET_PRINT("[BCWARN][RCC MISSION] RUN_WARNINGSCREEN_QUIT_GTAONLINE - about to enter the end of the mission, put this screen away due to Player Scripted Kill - g_bMissionEnding = TRUE ")
		RETURN WARNING_SCREEN_RETURN_CANCEL
	ENDIF
	
	IF g_bForceCloseWarningScreen
		NET_NL()NET_PRINT("[BCWARN][RCC MISSION] RUN_WARNINGSCREEN_QUIT_GTAONLINE - forcing player off warning screen - g_bForceCloseWarningScreen = TRUE ")
		g_bForceCloseWarningScreen = FALSE
		RETURN WARNING_SCREEN_RETURN_CANCEL
	ENDIF
	
	STRING sStatementText = "PM_QUIT_WARN" 
	STRING sLineOneMessge = "PM_QUIT_WARN3"
	STRING sLineTwoMessge = "PM_QUIT_WARN11"
	
	IF WILL_MP_AUTOSAVE_BE_SKIPPED()
		
		sLineTwoMessge = "PM_QUIT_WARN5"
	ENDIF
	
	IF g_b_WasSavingDown
	OR SCRIPT_IS_CLOUD_AVAILABLE() = FALSE
	 	sLineOneMessge = "PM_QUIT_WARN3"
		sLineTwoMessge = "PM_QUIT_WARN16"
	ENDIF
	
	IF Is_Player_Currently_On_MP_Heist_Planning(PLAYER_ID()) 
	OR Is_Player_Currently_On_MP_Heist(PLAYER_ID())
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			sLineOneMessge = "HEIST_QUITWARNING"
			sLineTwoMessge = "CONT_SURE"
		ENDIF
	ENDIF
	
		IF NOT IS_PLAYER_SCTV(PLAYER_ID())
			IF FM_EVENT_IS_PLAYER_LEAVING_EVENT_SEVERE()
				sLineOneMessge = "FMEVNT_SUREQUIT"	//Are you sure you want to quit during this event?
			ENDIF
			IF FM_EVENT_IS_PLAYER_LEAVING_EVENT_SUPER_SEVERE()	
				sLineOneMessge = "FMEVNT_SUREQUIT" 	//Are you sure you want to quit during this event?
				sLineTwoMessge = "FMEVNT_QUITWARNING"	//If you quit now, the event will fail for everyone.
			ENDIF
		ENDIF
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_YESNO , sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC 



FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_CLOUD_DOWN()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_CLOUD_DOWN - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_CLOUDFAILMSG"
	STRING sLineTwoMessge = "HUD_SPRETRNFRSH"

	IF GET_CURRENT_HUD_STATE() != HUD_STATE_SELECT_CHARACTER_FM
		RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	ENDIF
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK, sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC 

FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_SELECTOR_TRANSITION_CLOUD_DOWN()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1


	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_SELECTOR_TRANSITION_CLOUD_DOWN - running ")

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
	ENDIF

	
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, CANCEL)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_CANCEL) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
	
			RETURN WARNING_SCREEN_RETURN_CANCEL
		ENDIF
	ENDIF

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "TRAN_CLOUDDWN"
	STRING sLineTwoMessge = "HUD_OFFLINEMODE"

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_YESNO, sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC 




FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_CLOUD_UP()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_CLOUD_UP - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_CANCEL)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, CANCEL)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_CANCEL) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, CANCEL)
			
			
			
			RETURN WARNING_SCREEN_RETURN_CANCEL
		ENDIF
	ENDIF
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "TRAN_CLOUDUP"
	STRING sLineTwoMessge = "HUD_RETRYSTAT"
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_YESRETURNSP , sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC 




FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_GTAO_NOT_AVAIL()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_GTAO_NOT_AVAIL - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			SET_LOADING_ICON_INACTIVE()				
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_GTAO_NOT_AVAIL, FALSE)
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "BAIL_NOGTAO"
	STRING sLineTwoMessge = "HUD_RETURNSP"

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK, sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC 

FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_NOT_AVAIL()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_NOT_AVAIL - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_GTAO_UNAVAILABLE, FALSE)
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			SET_LOADING_ICON_INACTIVE()				
			g_iExternalWarningScreenStages = 0
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "PM_LAUNCH"
	STRING sLineTwoMessge = "HUD_RETURNSP"

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK, sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC 


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_SELECTOR_NO_CABLE()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_SELECTOR_NO_CABLE - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_CABLE, FALSE)
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			SET_LOADING_ICON_INACTIVE()				
			g_iExternalWarningScreenStages = 0
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_NOCONNECT"
	STRING sLineTwoMessge = "HUD_RETURNSP"

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK, sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_NO_TUNABLES()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("[BCWHEEL] RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_NO_TUNABLES - running ")
	#IF USE_FINAL_PRINTS
	PRINTLN_FINAL("[BCWHEEL] RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_NO_TUNABLES - running ")								
	#ENDIF

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			SET_LOADING_ICON_INACTIVE()				
			g_iExternalWarningScreenStages = 0
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_NOTUNE"
	STRING sLineTwoMessge = "HUD_RETURNSP"

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK, sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC 

FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_NO_BACKGROUND()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_NO_BACKGROUND - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_NO_BACKGROUND - NETWORK_REQUEST_CLOUD_BACKGROUND_SCRIPTS() called ")
			NETWORK_REQUEST_CLOUD_BACKGROUND_SCRIPTS()
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			SET_LOADING_ICON_INACTIVE()				
			g_iExternalWarningScreenStages = 0
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_NOBACKGRN"
	STRING sLineTwoMessge = "HUD_RETURNSP"

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK, sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC 


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_HEAVY_LOAD()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_HEAVY_LOAD - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			SET_LOADING_ICON_INACTIVE()				
			g_iExternalWarningScreenStages = 0
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_LOADDOWN"
	STRING sLineTwoMessge = "HUD_RETURNSP"

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK, sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC 


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_MAINTENANCE()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_MAINTENANCE - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_MAINTENANCE, FALSE)

			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			
			IGNORE_INVITE()
			SET_LOADING_ICON_INACTIVE()				
			g_iExternalWarningScreenStages = 0
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_LOADMAIN"
	STRING sLineTwoMessge = "HUD_RETURNSP"
	
	//Check if MP is disabled by script
	IF HAS_SCRIPT_BLOCKED_MULTIPLAYER_ACCESS()
		sLineOneMessge = "HUD_LOADSCR_D"
		sLineTwoMessge = "HUD_RETURNSP"
	ENDIF

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK, sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC 

FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SP_PROLOGUE_NOT_DONE()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SP_PROLOGUE_NOT_DONE - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			SET_LOADING_ICON_INACTIVE()				
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SP_PROLOGUE_NOT_DONE, FALSE)
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "BAIL_SPPRO"
	STRING sLineTwoMessge = "HUD_RETURNSP"

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK, sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_SELECTOR_SP_PROLOGUE_NOT_DONE()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_SELECTOR_SP_PROLOGUE_NOT_DONE - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			SET_LOADING_ICON_INACTIVE()				
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_SELECTOR_SP_PROLOGUE_NOT_DONE, FALSE)
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "PM_DOPROB"
	STRING sLineTwoMessge = "HUD_RETURNSP"

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK, sLineTwoMessge)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SAME_SESSION()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SAME_SESSION - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			
			IF HAS_AIMTYPE_ALTERED_BY_INVITE()
				PUT_AIMTYPE_BACK_TO_WHERE_IT_WAS()
				SET_AIMTYPE_ALTERED_BY_INVITE(FALSE)
				PRINTLN("[AIMPREF] RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SAME_SESSION - put aimtype back ")
			ENDIF
			
			SET_LOADING_ICON_INACTIVE()	
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SAME_SESSION, FALSE)
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_BAILSAME"

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC

FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SAVE_ISSUE()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SAVE_ISSUE - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			IGNORE_INVITE()
			RESET_NET_TIMER(g_st_SigningOutSelectorTimer)
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			SET_LOADING_ICON_INACTIVE()				
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SAVE_ISSUE, FALSE)
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_BAILSAVEISS" //Unable to join the game as your save game failed to load. The Rockstar game services are unavailable right now, please try again later.
//	STRING sLineTwoMessge = "HUD_RETURNSP" //Plesae try again later is in the message above. 

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC

FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_NO_POLICY()
	
	CONST_INT ACCEPT 0
	CONST_INT CANCEL 1

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_NO_POLICY - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			RESET_NET_TIMER(g_st_SigningOutSelectorTimer)
			
			SET_LOADING_ICON_INACTIVE()				
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_NO_POLICY, FALSE)
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "BUD_BAILPOLC"
//	STRING sLineTwoMessge = "HUD_RETURNSP" //Plesae try again later is in the message above. 

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC

FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN(BOOL CalledDirectly = FALSE, BOOL bActionInvite = TRUE)
	
	CONST_INT ACCEPT 0
	
	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN - running ")
	
	IF g_Private_BailWarningScreenEventParam = -2
		bActionInvite = FALSE
	ENDIF
	
	IF NOT IS_SYSTEM_UI_BEING_DISPLAYED()
	OR bActionInvite = FALSE
	
		NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN: IS_SYSTEM_UI_BEING_DISPLAYED = FALSE")
		IF HAS_NET_TIMER_EXPIRED(g_st_SigningOutSelectorTimer, 3000)
		OR bActionInvite = FALSE
			NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN: HAS_NET_TIMER_EXPIRED = TRUE")
		
			IF CalledDirectly 
				SET_GAME_PAUSED(TRUE)
			ENDIF
		
			IF NOT NETWORK_IS_SIGNED_ONLINE()
			OR bActionInvite = FALSE
				NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN: NETWORK_IS_SIGNED_ONLINE = FALSE ")
				
				IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
					SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
				ENDIF
				
				
				
				IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
					IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
						CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
						PRINTLN("[BCWARN] RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN - g_Private_BailWarningScreenEventParam = ", -1)
						g_Private_BailWarningScreenEventParam = -1
						SET_LOADING_ICON_INACTIVE()
						RESET_NET_TIMER(g_st_SigningOutSelectorTimer)
						g_iExternalWarningScreenStages = 0
						SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SIGNIN, FALSE)
						
						RETURN WARNING_SCREEN_RETURN_ACCEPT
					ENDIF
				ENDIF
				
				

				
				STRING sStatementText = "HUD_CONNPROB" 
				STRING sLineOneMessge = "HUD_CONNT"
				
				IF IS_PLAYSTATION_PLATFORM()
					sLineOneMessge = "HUD_CONNTPS4SI"
				ENDIF

				IF IS_PLAYSTATION_PLATFORM()
				AND NETWORK_IS_CABLE_CONNECTED() = FALSE
					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN: NETWORK_IS_CABLE_CONNECTED = FALSE ")

					sLineOneMessge = "HUD_PLUGPU"
				ENDIF
				
				STRING sLineTwoMessge = "HUD_RETURNSP"
				RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
				
				SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK, sLineTwoMessge)
			ELSE
				IF bActionInvite
					NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN: NOT DISPLAYING BECAUSE bActionInvite = TRUE")
					SET_PAUSE_MENU_REQUESTING_A_WARP()
					SET_PAUSE_MENU_REQUESTING_A_NEW_SESSION()
					SET_PAUSE_MENU_WARP_TO_MODE(GAMEMODE_FM)
					SET_LOADING_ICON_INACTIVE()
					PRINTLN("[BCWARN] RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN - g_Private_BailWarningScreenEventParam = ", -1)
					g_Private_BailWarningScreenEventParam = -1
					g_iExternalWarningScreenStages = 0
					SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SIGNIN, FALSE)
					RETURN WARNING_SCREEN_RETURN_CANCEL
				ENDIF
			ENDIF
		ELSE
			IF CalledDirectly 
				SET_GAME_PAUSED(FALSE)
			ENDIF
			#IF IS_DEBUG_BUILD
				NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN: NOT DISPLAYING BECAUSE waiting for 3 seconds first to make sure it's a correct sign out. ")
			#ENDIF
			
		ENDIF 
	ELSE
		#IF IS_DEBUG_BUILD
			IF IS_SYSTEM_UI_BEING_DISPLAYED()
				NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN: NOT DISPLAYING BECAUSE IS_SYSTEM_UI_BEING_DISPLAYED = TRUE")
			ENDIF
			IF bActionInvite
				NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN: NOT DISPLAYING BECAUSE bActionInvite = TRUE")
			ENDIF
		#ENDIF
	ENDIF
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_GTAO_ACCESS_DENIED()
	
	CONST_INT ACCEPT 0
	
	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_GTAO_ACCESS_DENIED - running ")
	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			
			SET_LOADING_ICON_INACTIVE()
			MULTIPLAYER_ACCESS_CODE accessFailReason
			SCRIPT_NETWORK_CAN_ACCESS_MULTIPLAYER(accessFailReason)
			IF accessFailReason = ACCESS_DENIED_NO_BACKGROUND_SCRIPT
				NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_GTAO_ACCESS_DENIED - NETWORK_REQUEST_CLOUD_BACKGROUND_SCRIPTS() called ")
				NETWORK_REQUEST_CLOUD_BACKGROUND_SCRIPTS()
			ENDIF
			
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_GTAO_ACCESS_DENIED, FALSE)
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	INT iNumberOfDays
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = GET_GTAO_BLOCK_REASON(iNumberOfDays)
	STRING sLineTwoMessge = "HUD_RETURNSP"
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	
//	IF iNumberOfDays = -1
		SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK, sLineTwoMessge)
//	ELSE
//		SET_WARNING_MESSAGE_WITH_HEADER_AND_SUBSTRING_FLAGS(sStatementText, sLineOneMessge, FE_WARNING_OK, sLineTwoMessge, TRUE,iNumberOfDays )
//	ENDIF

	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_CREATOR_SAVES_FAILED()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_CREATOR_SAVES_FAILED - running ")

	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
			
		ENDIF
	ENDIF
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_CRSAVEFAIL"
	STRING sLineTwoMessge = ""
	
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE , sLineTwoMessge)
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_CREATOR_NO_NETWORKING_SHARING_PRIV()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_CREATOR_NO_NETWORKING_SHARING_PRIV - running ")

	
	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF

	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
			
		ENDIF
	ENDIF
	
	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "HUD_CRSAVENNSP"
	STRING sLineTwoMessge = ""
	
	
	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_CONTINUE , sLineTwoMessge)
	
	
	RETURN WARNING_SCREEN_RETURN_NONE


ENDFUNC

FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_PRESENCE_INVITE_DROPPED_TIMED_OUT()
	
	CONST_INT ACCEPT 0

	

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_PRESENCE_INVITE_DROPPED_TIMED_OUT - running ")

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			RESET_NET_TIMER(g_st_SigningOutSelectorTimer)
			SET_LOADING_ICON_INACTIVE()				
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_PRESENCE_INVITE_DROPPED_TIMED_OUT, FALSE)
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "TRAN_J_FL" //Failed to join session, please try again later. 

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC

FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_PRESENCE_INVITE_DROPPED_REJECTED()
	
	CONST_INT ACCEPT 0
	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_PRESENCE_INVITE_DROPPED_REJECTED - running ")

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			RESET_NET_TIMER(g_st_SigningOutSelectorTimer)
			SET_LOADING_ICON_INACTIVE()				
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_PRESENCE_INVITE_DROPPED_REJECTED, FALSE)
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "TRAN_J_FL" //Failed to join session, please try again later. 

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_PRESENCE_INVITE_DROPPED_OLDEST()
	
	CONST_INT ACCEPT 0
	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_PRESENCE_INVITE_DROPPED_OLDEST - running ")

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			RESET_NET_TIMER(g_st_SigningOutSelectorTimer)
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			SET_LOADING_ICON_INACTIVE()				
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_PRESENCE_INVITE_DROPPED_OLDEST, FALSE)
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "TRAN_J_FL" //Failed to join session, please try again later. 

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC

FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_PRESENCE_INVITE_DROPPED_ACCEPTED()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_PRESENCE_INVITE_DROPPED_ACCEPTED - running ")


	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			RESET_NET_TIMER(g_st_SigningOutSelectorTimer)
			IF GET_CURRENT_TRANSITION_STATE() = TRANSITION_STATE_WAIT_ON_INVITE
				REQUEST_TRANSITION(TRANSITION_STATE_RETURN_TO_SINGLEPLAYER, GET_CURRENT_GAMEMODE(), GET_CURRENT_HUD_STATE())
			ENDIF
			SET_LOADING_ICON_INACTIVE()				
			g_iExternalWarningScreenStages = 0
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_PRESENCE_INVITE_DROPPED_ACCEPTED, FALSE)
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "TRAN_J_FL" //Failed to join session, please try again later. 

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC


FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_PLAYER_CHOSEN_TO_SPECTATE_GONE()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_PLAYER_CHOSEN_TO_SPECTATE_GONE - running ")

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "NO_INDIV_SPEC" //The player chosen to spectate is no longer available. 

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC

FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_NO_SPECTATE_SPACES()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_NO_SPECTATE_SPACES - running ")

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_NO_SPECTATE_SPACES, FALSE)
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	

	
	STRING sStatementText = "HUD_CONNPROB" 
	STRING sLineOneMessge = "NO_INDIV_SPEC" //The player chosen to spectate is no longer available. 

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC



FUNC WARNING_SCREEN_RETURN RUN_WARNINGSCREEN_TRANSITION_MIGRATION_QUEUE_FINISHED()
	
	CONST_INT ACCEPT 0

	NET_NL()NET_PRINT("RUN_WARNINGSCREEN_TRANSITION_MIGRATION_QUEUE_FINISHED - running ")

	IF HAS_HUD_CONTROL_BEEN_PRESSED_PAUSE(INPUT_FRONTEND_ACCEPT)
		SET_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
	ENDIF
	
	IF IS_BIT_SET(g_Private_BailWarningScreenButtonBitset, ACCEPT)
		IF HAS_HUD_CONTROL_BEEN_RELEASED_PAUSE(  INPUT_FRONTEND_ACCEPT) 
			CLEAR_BIT(g_Private_BailWarningScreenButtonBitset, ACCEPT)
			
			SET_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_MIGRATE_QUEUE_FINISHED, FALSE)
			
			RETURN WARNING_SCREEN_RETURN_ACCEPT
		ENDIF
	ENDIF
	


	STRING sStatementText = "HUD_COMPTITLE" 
	STRING sLineOneMessge = "HUD_IMPORFIN" //GTA Online data import complete.

	RUN_LOADING_SCREEN_SHUTDOWN_WITH_MESSAGE()
	
	SET_WARNING_MESSAGE_WITH_HEADER(sStatementText, sLineOneMessge, FE_WARNING_OK)
	
	RETURN WARNING_SCREEN_RETURN_NONE

ENDFUNC



PROC DISPLAY_EXTERNAL_WARNING_SCREENS()

	
	SWITCH g_iExternalWarningScreenStages
		CASE 0			
			IF IS_ANY_WARNING_SCREEN_BAIL_ACTIVE()		
			AND IS_WARNING_MESSAGE_ACTIVE() = FALSE
			
				#IF IS_DEBUG_BUILD
				
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED >>>")
				
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED_SCREEN_ONLY)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED_SCREEN_ONLY >>>")
				
					ENDIF
					
					
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_UNDERAGE)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROPINVITE_UNDERAGE >>>")
				
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_STILL_ON_TUTORIAL)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROPINVITE_STILL_ON_TUTORIAL >>>")
				
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_NO_CHARACTERS)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROPINVITE_NO_CHARACTERS >>>")
				
				
					ENDIF
										
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_DIRTY_READ)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROPINVITE_DIRTY_READ >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SESSION_FULL)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROPINVITE_SESSION_FULL >>>")
				
				
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_CLOUD_DOWN)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROPINVITE_CLOUD_DOWN >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_GTAO_NOT_AVAIL)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROPINVITE_GTAO_NOT_AVAIL >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SP_PROLOGUE_NOT_DONE)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROPINVITE_SP_PROLOGUE_NOT_DONE >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SAME_SESSION)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROPINVITE_SAME_SESSION >>>")
					ENDIF
						
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SAVE_ISSUE)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROPINVITE_SAVE_ISSUE >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SIGNIN)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROPINVITE_SIGNIN >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_GTAO_ACCESS_DENIED)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_GTAO_ACCESS_DENIED >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_PRESENCE_INVITE_DROPPED_TIMED_OUT)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_PRESENCE_INVITE_DROPPED_TIMED_OUT >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_PRESENCE_INVITE_DROPPED_REJECTED)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_PRESENCE_INVITE_DROPPED_REJECTED >>>")
					ENDIF

					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_PRESENCE_INVITE_DROPPED_OLDEST)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_PRESENCE_INVITE_DROPPED_OLDEST >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_PRESENCE_INVITE_DROPPED_ACCEPTED)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_PRESENCE_INVITE_DROPPED_ACCEPTED >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_NO_POLICY)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROPINVITE_NO_POLICY >>>")
					ENDIF
				
				
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_UNDERAGE)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_TRANSITION_UNDERAGE >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_COMPATFAIL)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROPINVITE_COMPATFAIL >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_NO_TUNEABLE)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROPINVITE_NO_TUNEABLE >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_NO_BACKGROUND)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROPINVITE_NO_BACKGROUND >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_NO_SPECTATE_SPACES)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_NO_SPECTATE_SPACES >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_SYSTEM_UPDATE)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_TRANSITION_SYSTEM_UPDATE >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_GAME_UPDATE)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_TRANSITION_GAME_UPDATE >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_DROPINVITE_REPLAY_ACTIVE)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_TRANSITION_DROPINVITE_REPLAY_ACTIVE >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_SAVE_MIGRATION_IN_PROGRESS)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_TRANSITION_SAVE_MIGRATION_IN_PROGRESS >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_SUBSCRIPTION)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_TRANSITION_NO_SUBSCRIPTION >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_MIGRATE_QUEUE_FINISHED)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_TRANSITION_MIGRATE_QUEUE_FINISHED >>>")
					ENDIF
					
					IF  IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_MAINTENANCE)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_TRANSITION_MAINTENANCE >>>")
					ENDIF	
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_GTAO_UNAVAILABLE)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_TRANSITION_GTAO_UNAVAILABLE >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_CABLE)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_TRANSITION_NO_CABLE >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SAVE_TRANSFER_IN_PROGRESS)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROPINVITE_SAVE_TRANSFER_IN_PROGRESS >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES >>>")
					ENDIF
		
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_MOD_INSTALLED)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_BAIL_DROP_MOD_INSTALLED >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD >>>")
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_XBOX_PERMISSIONS)
						NET_NL()NET_PRINT("<<< BC: DISPLAY_EXTERNAL_WARNING_SCREENS = WARNINGSCREEN_TRANSITION_NO_XBOX_PERMISSIONS >>>")
					ENDIF
				#ENDIF
			
				IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_UNDERAGE)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_STILL_ON_TUTORIAL)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_NO_CHARACTERS)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SESSION_FULL)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_CLOUD_DOWN)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_GTAO_NOT_AVAIL)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SP_PROLOGUE_NOT_DONE)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SAME_SESSION)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SAVE_ISSUE)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SIGNIN)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_GTAO_ACCESS_DENIED)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_PRESENCE_INVITE_DROPPED_TIMED_OUT)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_PRESENCE_INVITE_DROPPED_REJECTED)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_PRESENCE_INVITE_DROPPED_OLDEST)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_PRESENCE_INVITE_DROPPED_ACCEPTED)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_NO_POLICY)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_UNDERAGE)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_DIRTY_READ)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_COMPATFAIL)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_NO_TUNEABLE)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_NO_BACKGROUND)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_NO_SPECTATE_SPACES)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_SYSTEM_UPDATE)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_GAME_UPDATE)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_DROPINVITE_REPLAY_ACTIVE)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_SAVE_MIGRATION_IN_PROGRESS)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_SUBSCRIPTION)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_MIGRATE_QUEUE_FINISHED)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_MAINTENANCE)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_GTAO_UNAVAILABLE)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_CABLE)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SAVE_TRANSFER_IN_PROGRESS)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED_SCREEN_ONLY)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_MOD_INSTALLED)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD)
				OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_XBOX_PERMISSIONS)
	
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SIGNIN)
					AND g_Private_BailWarningScreenEventParam != -2
						NET_NL()NET_PRINT("<<< DISPLAY_EXTERNAL_WARNING_SCREENS = DISPLAY_SYSTEM_SIGNIN_UI  = TRUE >>>")
						
						DISPLAY_SYSTEM_SIGNIN_UI(TRUE)
						RESET_NET_TIMER(g_st_SigningOutSelectorTimer)
					ENDIF
					
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_SUBSCRIPTION)
						NET_NL()NET_PRINT("DISPLAY_EXTERNAL_WARNING_SCREENS = NETWORK_SHOW_ACCOUNT_UPGRADE_UI - called ")
					
						NETWORK_SHOW_ACCOUNT_UPGRADE_UI()
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD)
						NET_NL()NET_PRINT("DISPLAY_EXTERNAL_WARNING_SCREENS = DOES_PLAYER_HAVE_XBOX_GOLD_MEMBERSHIP - called ")
						DOES_PLAYER_HAVE_XBOX_GOLD_MEMBERSHIP(TRUE)
					ENDIF
					
					IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_XBOX_PERMISSIONS)
						NET_NL()NET_PRINT("WARNINGSCREEN_TRANSITION_NO_XBOX_PERMISSIONS = DOES_PLAYER_HAVE_XBOX_PERMISSIONS - called ")
						DOES_PLAYER_HAVE_XBOX_PERMISSIONS(TRUE)
					ENDIF
					
					CANCEL_ALL_SP_ANIMPOSTFX_WHEN_ENTERING_MP()
//					g_b_Private_NumberofAttemptsToChangeAimType = 0
					
					g_b_LoadingInProgress = FALSE
					g_iExternalWarningScreenStages = 1
				ENDIF
			ENDIF
		BREAK
		

		
		CASE 1
		
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_SUBSCRIPTION) = FALSE
			OR IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD) = FALSE
				IF GET_IS_LOADING_SCREEN_ACTIVE()
	//				SET_EMERGENCY_SKYCAM_UP_RUNNING(TRUE)
					NET_NL()NET_PRINT("<<< DISPLAY_EXTERNAL_WARNING_SCREENS = SHUTDOWN_LOADING_SCREEN >>>")
					SHUTDOWN_LOADING_SCREEN()				
				ENDIF
			ELSE
				IF GET_IS_LOADING_SCREEN_ACTIVE()
					SET_EMERGENCY_SKYCAM_UP_RUNNING(TRUE)
				ENDIF
			ENDIF
				
		
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED)
				RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED()
				
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED_SCREEN_ONLY)
				RUN_WARNINGSCREEN_BAIL_TRANSITION_JOIN_FAILED_SCREEN_ONLY()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_UNDERAGE)
				RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_BLOCKED_UNDERAGE()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_STILL_ON_TUTORIAL)
				RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_BLOCKED_ON_TUTORIAL()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_NO_CHARACTERS)
				RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_NO_CHARACTERS()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_DIRTY_READ)
				RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_DIRTY_READ()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SESSION_FULL)
				RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_SESSION_FULL()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_CLOUD_DOWN)
				RUN_WARNINGSCREEN_BAIL_TRANSITION_INVITE_CLOUD_DOWN()
			ENDIF

				
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_GTAO_NOT_AVAIL)
				RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_GTAO_NOT_AVAIL()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SP_PROLOGUE_NOT_DONE)
				RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SP_PROLOGUE_NOT_DONE()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SAME_SESSION)
				RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SAME_SESSION()
			ENDIF

			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SAVE_ISSUE)
				RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SAVE_ISSUE()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SIGNIN)
				RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_SIGNIN()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_GTAO_ACCESS_DENIED)
				RUN_WARNINGSCREEN_TRANSITION_GTAO_ACCESS_DENIED()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF)
				RUN_WARNINGSCREEN_BAIL_DROP_TRANSITION_INVITE_AIM_DIFF()
			ENDIF
			
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_PRESENCE_INVITE_DROPPED_TIMED_OUT)
				RUN_WARNINGSCREEN_PRESENCE_INVITE_DROPPED_TIMED_OUT()
			ENDIF
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_PRESENCE_INVITE_DROPPED_REJECTED)
				RUN_WARNINGSCREEN_PRESENCE_INVITE_DROPPED_REJECTED()
			ENDIF
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_PRESENCE_INVITE_DROPPED_OLDEST)
				RUN_WARNINGSCREEN_PRESENCE_INVITE_DROPPED_OLDEST()
			ENDIF
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_PRESENCE_INVITE_DROPPED_ACCEPTED)
				RUN_WARNINGSCREEN_PRESENCE_INVITE_DROPPED_ACCEPTED()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_NO_POLICY)
				RUN_WARNINGSCREEN_TRANSITION_BAILINVITE_NO_POLICY()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_UNDERAGE)
				RUN_WARNINGSCREEN_TRANSITION_UNDERAGE()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED)
				RUN_WARNINGSCREEN_TRANSITION_SOCIAL_CLUB_BANNED()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_COMPATFAIL)
				RUN_WARNINGSCREEN_DROPINVITE_COMPATFAIL()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_NO_TUNEABLE)
				RUN_WARNINGSCREEN_DROPINVITE_NO_TUNEABLE()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_NO_BACKGROUND)
				RUN_WARNINGSCREEN_DROPINVITE_NO_BACKGROUND()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_NO_SPECTATE_SPACES)
				RUN_WARNINGSCREEN_NO_SPECTATE_SPACES()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN)
				RUN_WARNINGSCREEN_TRANSITION_UNDERAGE_NEXTGEN()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_GAME_UPDATE)
				RUN_WARNINGSCREEN_TRANSITION_GAME_UPDATE()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_SYSTEM_UPDATE)
				RUN_WARNINGSCREEN_TRANSITION_SYSTEM_UPDATE()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_DROPINVITE_REPLAY_ACTIVE)
				RUN_WARNINGSCREEN_TRANSITION_DROPINVITE_REPLAY_ACTIVE()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_SAVE_MIGRATION_IN_PROGRESS)
				RUN_WARNINGSCREEN_TRANSITION_SAVE_MIGRATION_IN_PROGRESS()
			ENDIF
			
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_SUBSCRIPTION)
				RUN_WARNINGSCREEN_TRANSITION_NO_PS_PLUS()
			ENDIF	
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_MIGRATE_QUEUE_FINISHED)
				RUN_WARNINGSCREEN_TRANSITION_MIGRATION_QUEUE_FINISHED()
			ENDIF	
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_MAINTENANCE)
				RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_MAINTENANCE()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_GTAO_UNAVAILABLE)
				RUN_WARNINGSCREEN_TRANSITION_SELECTOR_GTAO_NOT_AVAIL()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_CABLE)
				RUN_WARNINGSCREEN_TRANSITION_SELECTOR_NO_CABLE()
			ENDIF
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROPINVITE_SAVE_TRANSFER_IN_PROGRESS)
				RUN_WARNINGSCREEN_DROPINVITE_SAVE_TRANSFER_IN_PROGRESS()
			ENDIF
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES)
				RUN_WARNINGSCREEN_BAIL_DROP_TOO_MANY_BOSSES()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_BAIL_DROP_MOD_INSTALLED)
				RUN_WARNINGSCREEN_TRANSITION_MODS_INSTALLED()
			ENDIF
			
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD)
				RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_GOLD()
			ENDIF
			IF IS_WARNING_SCREEN_BAIL(WARNINGSCREEN_TRANSITION_NO_XBOX_PERMISSIONS)
				RUN_WARNINGSCREEN_TRANSITION_NO_XBOX_PERMISSION()
			ENDIF
			
		BREAK
	
	ENDSWITCH


	
	

ENDPROC




ENUM EXTERNAL_SKYCAM
	EXTERNAL_SKYCAM_NONE = 0,
	EXTERNAL_SKYCAM_EMERGENCY, 
	EXTERNAL_SKYCAM_SP_SELECTOR,
	EXTERNAL_SKYCAM_SPECTATOR_INVITE,
	EXTERNAL_SKYCAM_SP_PAUSE_MENU_UP,
	EXTERNAL_SKYCAM_SP_PAUSE_MENU_UP_DURING_MISSION,
	
	EXTERNAL_SKYCAM_MP_PAUSE_MENU_UP,
	EXTERNAL_SKYCAM_MP_SWAP_CHARACTERS_UP,
	
	EXTERNAL_SKYCAM_CLEANUP

ENDENUM

FUNC BOOL IS_ANY_EXTERNAL_EMERGENCY_SKYCAM_UP_ACTIVE()


			
	IF IS_EMERGENCY_SKYCAM_UP_RUNNING()
	OR IS_SP_SELECTOR_SKYCAM_UP()
	OR IS_EMERGENCY_SKYCAM_UP_SPECTATOR_INVITE()
	OR IS_SP_PAUSE_MENU_SKYCAM_UP()
	OR IS_SP_PAUSE_MENU_DURING_MISSIONS_SKYCAM_UP()
	OR IS_MP_PAUSE_MENU_SKYCAM_UP()
	OR IS_MP_SWAP_CHARACTERS_SKYCAM_UP()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


PROC RUN_EXTERNAL_EMERGENCY_SKYCAM_UP()

	INT iTimeSinceCalled = GET_GAME_TIMER() - g_i_SetSkyswoopUpLastCalledTime
	BOOL bSkipIntro = FALSE
	
		SWITCH INT_TO_ENUM(EXTERNAL_SKYCAM, g_iExternalSkycamUpStages)
			CASE EXTERNAL_SKYCAM_NONE 
			
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS() 
				AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_RETURN_TO_SINGLEPLAYER
				AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_WAIT_FOR_SINGLEPLAYER_TO_START
				AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_TERMINATE_MAINTRANSITION
				AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_SP_SWOOP_DOWN
				AND GET_CURRENT_TRANSITION_STATE() != TRANSITION_STATE_FM_SWOOP_DOWN
				#IF FEATURE_FREEMODE_ARCADE 
				AND NOT IS_LOBBY_PREVIEW_RUNNING()
				#ENDIF
				
			
				
					IF IS_EMERGENCY_SKYCAM_UP_RUNNING()
						#IF IS_DEBUG_BUILD
						
						NET_NL()NET_PRINT("<<< BC: RUN_EXTERNAL_EMERGENCY_SKYCAM_UP: IS_EMERGENCY_SKYCAM_UP_RUNNING >>>")
						
						#ENDIF
						IF NOT IS_TRANSITION_FADE_OUT_FOR_INTERIOR_SWITCH()
							DO_SCREEN_FADE_OUT(0)
						ENDIF
						DISABLE_KILL_YOURSELF_OPTION()
						
						g_iExternalSkycamUpStages = ENUM_TO_INT(EXTERNAL_SKYCAM_EMERGENCY)
					ENDIF
					
					IF IS_SP_SELECTOR_SKYCAM_UP()
						#IF IS_DEBUG_BUILD
						
						NET_NL()NET_PRINT("<<< BC: RUN_EXTERNAL_EMERGENCY_SKYCAM_UP: IS_SP_SELECTOR_SKYCAM_UP >>>")
						
						#ENDIF
						
						g_iExternalSkycamUpStages = ENUM_TO_INT(EXTERNAL_SKYCAM_SP_SELECTOR)
					ENDIF
				
					IF IS_EMERGENCY_SKYCAM_UP_SPECTATOR_INVITE()
						#IF IS_DEBUG_BUILD
						
						NET_NL()NET_PRINT("<<< BC: RUN_EXTERNAL_EMERGENCY_SKYCAM_UP: IS_EMERGENCY_SKYCAM_UP_SPECTATOR_INVITE >>>")
						
						#ENDIF
						

						g_iExternalSkycamUpStages = ENUM_TO_INT(EXTERNAL_SKYCAM_SPECTATOR_INVITE)
					
					ENDIF
					
					IF IS_SP_PAUSE_MENU_SKYCAM_UP()
					
						#IF IS_DEBUG_BUILD
						
						NET_NL()NET_PRINT("<<< BC: RUN_EXTERNAL_EMERGENCY_SKYCAM_UP: IS_SP_PAUSE_MENU_SKYCAM_UP >>>")
						
						#ENDIF
						

						g_iExternalSkycamUpStages = ENUM_TO_INT(EXTERNAL_SKYCAM_SP_PAUSE_MENU_UP)
					
					ENDIF
					
					
					IF IS_SP_PAUSE_MENU_DURING_MISSIONS_SKYCAM_UP()
					
						#IF IS_DEBUG_BUILD
						
						NET_NL()NET_PRINT("<<< BC: RUN_EXTERNAL_EMERGENCY_SKYCAM_UP: IS_SP_PAUSE_MENU_DURING_MISSIONS_SKYCAM_UP >>>")
						
						#ENDIF
						
						IF SHOULD_END_SINGLEPLAYER() = FALSE
							g_iExternalSkycamUpStages = ENUM_TO_INT(EXTERNAL_SKYCAM_SP_PAUSE_MENU_UP_DURING_MISSION)
						ENDIF
					ENDIF
					
					IF  IS_MP_PAUSE_MENU_SKYCAM_UP()
						#IF IS_DEBUG_BUILD
						
						NET_NL()NET_PRINT("<<< BC: RUN_EXTERNAL_EMERGENCY_SKYCAM_UP: IS_MP_PAUSE_MENU_SKYCAM_UP >>>")
						
						#ENDIF
						
						g_iExternalSkycamUpStages = ENUM_TO_INT(EXTERNAL_SKYCAM_MP_PAUSE_MENU_UP)
					ENDIF

					IF IS_MP_SWAP_CHARACTERS_SKYCAM_UP()
						#IF IS_DEBUG_BUILD
						
						NET_NL()NET_PRINT("<<< BC: RUN_EXTERNAL_EMERGENCY_SKYCAM_UP: IS_MP_SWAP_CHARACTERS_SKYCAM_UP >>>")
						
						#ENDIF
						

						g_iExternalSkycamUpStages = ENUM_TO_INT(EXTERNAL_SKYCAM_MP_SWAP_CHARACTERS_UP)
					
					ENDIF
					
					IF IS_TRANSITION_FADE_OUT_FOR_INTERIOR_SWITCH()
						PRINTLN("RUN_EXTERNAL_EMERGENCY_SKYCAM_UP - iTimeSinceCalled = ", iTimeSinceCalled)
						IF (iTimeSinceCalled > 1000) // to make sure that another script isnt in the process of calling a skyswoop_up and depending on this	. see url:bugstar:6142044 
							CLEAR_TRANSITION_FADE_OUT_FOR_INTERIOR_SWITCH() 
						ENDIF
					ENDIF
				
				ENDIF
			BREAK
			
			CASE EXTERNAL_SKYCAM_EMERGENCY
			
				IF IS_SKYCAM_PAST_FIRST_CUT()			
					DO_SCREEN_FADE_IN_FOR_TRANSITION(0)
				ENDIF
			
				IF SET_SKYSWOOP_UP(FALSE, FALSE, TRUE, SWITCH_TYPE_LONG, TRUE)
	//				SET_EMERGENCY_SKYCAM_UP_RUNNING(FALSE)
					g_iExternalSkycamUpStages = ENUM_TO_INT(EXTERNAL_SKYCAM_CLEANUP)
				ENDIF
				
			BREAK
			
			CASE EXTERNAL_SKYCAM_SP_SELECTOR
				
				IF IS_SKYCAM_PAST_FIRST_CUT()	
					DO_SCREEN_FADE_IN_FOR_TRANSITION(0)
				ENDIF
				
				HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
				IF SET_SKYSWOOP_UP(FALSE, FALSE, FALSE, SWITCH_TYPE_LONG, FALSE)
	//				SET_SP_SELECTOR_SKYCAM_UP(FALSE)
					g_iExternalSkycamUpStages = ENUM_TO_INT(EXTERNAL_SKYCAM_CLEANUP)
				ENDIF
				
			BREAK
			
			CASE EXTERNAL_SKYCAM_SPECTATOR_INVITE
				
				HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
				IF SET_SKYSWOOP_UP(TRUE, FALSE, FALSE, SWITCH_TYPE_LONG, FALSE)
	//				SET_SP_SELECTOR_SKYCAM_UP(FALSE)
					g_iExternalSkycamUpStages = ENUM_TO_INT(EXTERNAL_SKYCAM_CLEANUP)
				ENDIF
				
			BREAK
			
			CASE EXTERNAL_SKYCAM_SP_PAUSE_MENU_UP
				
				HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
				IF SET_SKYSWOOP_UP(TRUE, TRUE, TRUE, SWITCH_TYPE_LONG, FALSE)
	//				SET_SP_SELECTOR_SKYCAM_UP(FALSE)
					g_iExternalSkycamUpStages = ENUM_TO_INT(EXTERNAL_SKYCAM_CLEANUP)
				ENDIF
				
			BREAK
			
			CASE EXTERNAL_SKYCAM_SP_PAUSE_MENU_UP_DURING_MISSION
				NET_NL()NET_PRINT("RUNNING EXTERNAL_SKYCAM_SP_PAUSE_MENU_UP_DURING_MISSION")
				IF IS_SKYCAM_PAST_FIRST_CUT()
					NET_NL()NET_PRINT("IS_SKYCAM_PAST_FIRST_CUT = TRUE")
					SET_SKYFREEZE_CLEAR(TRUE)
				ENDIF
				
					HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
					
					IF SET_SKYSWOOP_UP(FALSE, TRUE, FALSE, SWITCH_TYPE_LONG, TRUE)
		//				SET_SP_SELECTOR_SKYCAM_UP(FALSE)
						g_iExternalSkycamUpStages = ENUM_TO_INT(EXTERNAL_SKYCAM_CLEANUP)
					ENDIF
				
			BREAK
			
			CASE EXTERNAL_SKYCAM_MP_PAUSE_MENU_UP
			
				 
				IF IS_SKYCAM_PAST_FIRST_CUT()
					NET_NL()NET_PRINT("IS_SKYCAM_PAST_FIRST_CUT = TRUE")
					SET_SKYFREEZE_CLEAR(TRUE)
				ENDIF
			
				// url:bugstar:5479282 - Arena - Carnage I NG - Spectate - Quitting out of spectating a player during  the vehicle select screen caused the local players PED to fall through the world.
				// Tried extending initial fade or using renderpause but too many overlapping systems were stomping it. This was unfortunately the only fix that worked - RyanE
				IF GET_JOINING_GAMEMODE() != GAMEMODE_EMPTY
				AND CONTENT_IS_USING_ARENA()
					DO_SCREEN_FADE_OUT(0)
					
					#IF IS_DEBUG_BUILD
					IF NOT IS_SCREEN_FADED_OUT()
						PRINTLN("EXTERNAL_SKYCAM_MP_PAUSE_MENU_UP - DO_SCREEN_FADE_OUT(0)")
					ENDIF
					#ENDIF
				ENDIF
			
				HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
				IF SET_SKYSWOOP_UP(FALSE, FALSE, FALSE, SWITCH_TYPE_LONG, TRUE, 0, 0, 0, FALSE)
				
					g_iExternalSkycamUpStages = ENUM_TO_INT(EXTERNAL_SKYCAM_CLEANUP)
				ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
					NET_NL()NET_PRINT("EXTERNAL_SKYCAM_MP_PAUSE_MENU_UP SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)")
				ENDIF
			BREAK
			
			CASE EXTERNAL_SKYCAM_MP_SWAP_CHARACTERS_UP
				HIDE_SCRIPTED_HUD_COMPONENT_THIS_FRAME(HUD_MP_RANK_BAR)
				
				IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP() 
					bSkipIntro = TRUE
				ENDIF
				
				IF SET_SKYSWOOP_UP(FALSE, FALSE, TRUE, SWITCH_TYPE_LONG, bSkipIntro, 0, 0, 0, FALSE)
				
					g_iExternalSkycamUpStages = ENUM_TO_INT(EXTERNAL_SKYCAM_CLEANUP)
				ENDIF
			BREAK
			
			
		
			CASE EXTERNAL_SKYCAM_CLEANUP
				IF IS_ANY_EXTERNAL_EMERGENCY_SKYCAM_UP_ACTIVE() = FALSE
					g_iExternalSkycamUpStages =ENUM_TO_INT(EXTERNAL_SKYCAM_NONE)
				ENDIF
		
		ENDSWITCH
	

	
	

ENDPROC

PROC RUN_EXTERNAL_EMERGENCY_SKYCAM_DOWN()

	IF g_b_Enable_RUN_EXTERNAL_EMERGENCY_SKYCAM_DOWN
	
		INT iTimeSinceCalled = GET_GAME_TIMER() - g_i_SetSkyswoopDownLastCalledTime
		
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
		
			IF NOT IS_SKYSWOOP_AT_GROUND()
			
				// time sine set_skyswoop_down was called
				#IF IS_DEBUG_BUILD
				IF NOT (g_b_SetSkyswoopDownCompleted)
					PRINTLN("RUN_EXTERNAL_EMERGENCY_SKYCAM_DOWN - iTimeSinceCalled = ", iTimeSinceCalled)					
				ENDIF
				#ENDIF

			
				IF (iTimeSinceCalled > 1000)
			   		IF IS_SWITCH_READY_FOR_DESCENT()
						PRINTLN("RUN_EXTERNAL_EMERGENCY_SKYCAM_DOWN - IS_SWITCH_READY_FOR_DESCENT = TRUE")
					
			     		ALLOW_PLAYER_SWITCH_DESCENT()
					ELSE
						#IF IS_DEBUG_BUILD
						IF NOT (g_b_SetSkyswoopDownCompleted)
							PRINTLN("RUN_EXTERNAL_EMERGENCY_SKYCAM_DOWN - IS_SWITCH_READY_FOR_DESCENT = FALSE")					
						ENDIF
						#ENDIF					
					ENDIF
					
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
ENDPROC








