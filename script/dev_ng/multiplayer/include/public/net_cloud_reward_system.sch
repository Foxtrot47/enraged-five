/**********************************************************
/ Name: 		net_cloud_reward_system.sch
/ Author(s): 	James Adwick
/ Purpose: 		Generic header containing script functions to
/				control the unpacking and rewarding of in-game
/				items from cloud tuneables
***********************************************************/

USING "globals.sch"
USING "net_include.sch"
USING "net_reward_transactions.sch"
USING "net_common_functions.sch"

ENUM NET_CLOUD_REWARD_CLOTHING_RESULT
	NET_CLOUD_REWARD_CLOTHING_INVALID,
	NET_CLOUD_REWARD_CLOTHING_SHIRT_W_DECAL,
	NET_CLOUD_REWARD_CLOTHING_VALID_NOT_OWNED
ENDENUM



//// PURPOSE:
 ///    Takes the tunable passed from the server and looks up data to find matching value
 /// PARAMS:
 ///    playersModel - the player's model used for validation
 ///    iClothingTunable - contains the stored clothing data
 ///    eContentsClothingType - the returned clothing data type
 ///    eContentsClothingName - the returned clothing data name
 /// RETURNS:
 ///    Returns TRUE if an item is found successfully.
FUNC BOOL GET_MP_REWARD_CLOTHING_FROM_TUNABLE(MODEL_NAMES playersModel, INT iClothingTunable, PED_COMP_TYPE_ENUM &eContentsClothingType, PED_COMP_NAME_ENUM &eContentsClothingName)
	IF iClothingTunable != -1
	AND iClothingTunable != 0
	AND iClothingTunable != 1849449579 // 1849449579 = "0"
	
		INT iPed
		TATTOO_FACTION_ENUM eFaction
		
		IF playersModel = MP_M_FREEMODE_01
			iPed = 3
			eFaction = TATTOO_MP_FM
		ELSE
			iPed = 4
			eFaction = TATTOO_MP_FM_F
		ENDIF
	
		// Component namehash
		scrShopPedComponent sCompItemData
		GET_SHOP_PED_COMPONENT(iClothingTunable, sCompItemData)
		IF sCompItemData.m_nameHash = iClothingTunable
			eContentsClothingType = INT_TO_ENUM(PED_COMP_TYPE_ENUM, sCompItemData.m_eCompType)
			eContentsClothingName = GET_PED_COMP_ITEM_FROM_NAME_HASH(playersModel, iClothingTunable, eContentsClothingType, iPed)
			PRINTLN("     -----> GET_MP_REWARD_CLOTHING_FROM_TUNABLE - Found item by component namehash")
			RETURN TRUE
		ENDIF
		
		// Prop namehash
		scrShopPedProp sPropItemData
		GET_SHOP_PED_PROP(iClothingTunable, sPropItemData)
		IF sPropItemData.m_nameHash = iClothingTunable
			eContentsClothingType = COMP_TYPE_PROPS
			eContentsClothingName = GET_PED_COMP_ITEM_FROM_NAME_HASH(playersModel, iClothingTunable, eContentsClothingType, iPed)
			PRINTLN("     -----> GET_MP_REWARD_CLOTHING_FROM_TUNABLE - Found item by prop namehash")
			RETURN TRUE
		ENDIF
		
		// Outfit namehash
		scrShopPedOutfit sOutfitItemData
		GET_SHOP_PED_OUTFIT(iClothingTunable, sOutfitItemData)
		IF sOutfitItemData.m_nameHash = iClothingTunable
			eContentsClothingType = COMP_TYPE_OUTFIT
			eContentsClothingName = GET_PED_COMP_ITEM_FROM_NAME_HASH(playersModel, sOutfitItemData.m_nameHash, eContentsClothingType, iPed)
			PRINTLN("     -----> GET_MP_REWARD_CLOTHING_FROM_TUNABLE - Found item by outfit namehash")
			RETURN TRUE
		ENDIF
		
		// Overlay namehash
		TATTOO_NAME_ENUM eTattoo = GET_TATTOO_ENUM_FROM_DLC_HASH(iClothingTunable, eFaction)
		IF eTattoo != INVALID_TATTOO
			// Store the tattoo as a ped component just now and we will convert later.
			eContentsClothingName = INT_TO_ENUM(PED_COMP_NAME_ENUM, ENUM_TO_INT(eTattoo))
			eContentsClothingType = COMP_TYPE_DECL
			PRINTLN("     -----> GET_MP_REWARD_CLOTHING_FROM_TUNABLE - Found item by overlay namehash")
			RETURN TRUE
		ENDIF
		
		// For now we will not search by text label as this involves iterating over all DLC items and is not a cheap process.
		// Refer to previous iteration of this function to see how text label searches can be done with current support.
		// If we want to add text label searching in future we should consider additional code support to obtain a list of DLC items that have specific labels set in the shop meta.
	ENDIF

	NET_PRINT_TIME() NET_PRINT("     -----> GET_MP_REWARD_CLOTHING_FROM_TUNABLE - failed to find an item with iClothingTunable: ") NET_PRINT_INT(iClothingTunable) NET_NL()
	RETURN FALSE
ENDFUNC

FUNC NET_CLOUD_REWARD_CLOTHING_RESULT GIVE_MP_REWARD_CLOTHING(PED_COMP_TYPE_ENUM compType, PED_COMP_NAME_ENUM compName, TEXT_LABEL_31 &tlRewardLabel)
	
	NET_CLOUD_REWARD_CLOTHING_RESULT eMPRewardClothingResult = NET_CLOUD_REWARD_CLOTHING_INVALID
	STATS_PACKED eUnlockStat
	TATTOO_DATA_STRUCT sTattooData
	PED_COMP_ITEM_DATA_STRUCT ItemData
	ItemData = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_PLAYER_MODEL(), compType, compName)
	
	BOOL bFoundShirtWithDecalItem
	bFoundShirtWithDecalItem = FALSE
	
	// T-shirts that use decals use the tattoo system to unlock so do separate checks.
	IF compType = COMP_TYPE_DECL
	AND compName != INT_TO_ENUM(PED_COMP_NAME_ENUM, -1)
	AND ENUM_TO_INT(compName) > ENUM_TO_INT(TATTOO_MP_FM_DLC) // We only ever deal with DLC decls.
	
		IF IS_PLAYER_MALE(PLAYER_ID())
			IF GET_TATTOO_DATA(sTattooData, INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(compName)), TATTOO_MP_FM, PLAYER_PED_ID())
				bFoundShirtWithDecalItem = TRUE
			ENDIF
		ELSE
			IF GET_TATTOO_DATA(sTattooData, INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(compName)), TATTOO_MP_FM_F, PLAYER_PED_ID())
				bFoundShirtWithDecalItem = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bFoundShirtWithDecalItem
		
		eUnlockStat = GET_EVENT_ITEM_PACKED_STAT(sTattooData.iPreset)
		IF eUnlockStat != INT_TO_ENUM(STATS_PACKED, -1)
			
			SET_PACKED_STAT_BOOL(eUnlockStat, TRUE)
			
			tlRewardLabel = sTattooData.sLabel
			eMPRewardClothingResult = NET_CLOUD_REWARD_CLOTHING_SHIRT_W_DECAL
			
			NET_PRINT_TIME() NET_PRINT("     ----------> GIVE_MP_REWARD_CLOTHING - CD_CONTENTS_DLC  - CLOTHING ITEM GIVEN WITH DECAL = ") NET_PRINT(tlRewardLabel) NET_NL()
		ENDIF
	
	// Valid clothing item found, that the player doesn't already own.
	ELIF compType != INT_TO_ENUM(PED_COMP_TYPE_ENUM, -1)
	AND (compType != COMP_TYPE_DECL OR NOT bFoundShirtWithDecalItem)
	AND compName != INT_TO_ENUM(PED_COMP_NAME_ENUM, -1)
	AND compName != DUMMY_PED_COMP
	AND IS_PED_COMP_ITEM_AVAILABLE_MP(GET_PLAYER_MODEL(), compType, compName)
	AND NOT IS_STRING_NULL_OR_EMPTY(ItemData.sLabel)
	AND IS_BIT_SET(ItemData.iProperties, PED_COMPONENT_IS_DLC_BIT)
	
		eUnlockStat = GET_EVENT_ITEM_PACKED_STAT(g_iLastDLCItemNameHash)
		IF eUnlockStat != INT_TO_ENUM(STATS_PACKED, -1)
			IF NOT GET_PACKED_STAT_BOOL(eUnlockStat)
				SET_PACKED_STAT_BOOL(eUnlockStat, TRUE)
				
				tlRewardLabel = ItemData.sLabel
				eMPRewardClothingResult = NET_CLOUD_REWARD_CLOTHING_VALID_NOT_OWNED
				
				NET_PRINT_TIME() NET_PRINT("     ----------> GIVE_MP_REWARD_CLOTHING - CD_CONTENTS_DLC  - CLOTHING ITEM GIVEN = ") NET_PRINT(tlRewardLabel) NET_NL()
			ELSE
				NET_PRINT_TIME() NET_PRINT("     ----------> GIVE_MP_REWARD_CLOTHING - CD_CONTENTS_DLC  - ALREADY OWNED") NET_NL()
				eMPRewardClothingResult = NET_CLOUD_REWARD_CLOTHING_INVALID
			ENDIF
		ENDIF
	
	// Invalid component, or empty do not include clothing item, or already owned 
	ELSE
		
		//DEBUG PRINT REASON
		#IF IS_DEBUG_BUILD
		IF compType = INT_TO_ENUM(PED_COMP_TYPE_ENUM, -1)
			PRINTLN("     ----------> GIVE_MP_REWARD_CLOTHING - CD_CONTENTS_DLC - CLOTHING FAILED ON - compType = INT_TO_ENUM(PED_COMP_TYPE_ENUM, -1)")
		ENDIF
		IF compType = COMP_TYPE_DECL
			PRINTLN("     ----------> GIVE_MP_REWARD_CLOTHING - CD_CONTENTS_DLC - CLOTHING FAILED ON - compType = COMP_TYPE_DECL")
		ENDIF
		IF compName = INT_TO_ENUM(PED_COMP_NAME_ENUM, -1)
			PRINTLN("     ----------> GIVE_MP_REWARD_CLOTHING - CD_CONTENTS_DLC - CLOTHING FAILED ON - compName = INT_TO_ENUM(PED_COMP_NAME_ENUM, -1)")
		ENDIF
		IF compName = DUMMY_PED_COMP
			PRINTLN("     ----------> GIVE_MP_REWARD_CLOTHING - CD_CONTENTS_DLC - CLOTHING FAILED ON - compName = DUMMY_PED_COMP")
		ENDIF
		IF NOT IS_PED_COMP_ITEM_AVAILABLE_MP(GET_PLAYER_MODEL(), compType, compName)
			PRINTLN("     ----------> GIVE_MP_REWARD_CLOTHING - CD_CONTENTS_DLC - CLOTHING FAILED ON - NOT IS_PED_COMP_ITEM_AVAILABLE_MP")
		ENDIF
		IF IS_STRING_NULL_OR_EMPTY(ItemData.sLabel)
			PRINTLN("     ----------> GIVE_MP_REWARD_CLOTHING - CD_CONTENTS_DLC - CLOTHING FAILED ON - IS_STRING_NULL_OR_EMPTY")
		ENDIF
		#ENDIF
			
		NET_PRINT_TIME() NET_PRINT("     ----------> GIVE_MP_REWARD_CLOTHING - CD_CONTENTS_DLC  - NO CLOTHING") NET_NL()
		
		eMPRewardClothingResult = NET_CLOUD_REWARD_CLOTHING_INVALID
	ENDIF
	
	RETURN eMPRewardClothingResult
ENDFUNC

FUNC BOOL IS_MP_REWARD_CLOTHING_GIVEN(PED_COMP_TYPE_ENUM compType, PED_COMP_NAME_ENUM compName, TEXT_LABEL_31 &tlRewardLabel)
	
//	NET_CLOUD_REWARD_CLOTHING_RESULT eMPRewardClothingResult = NET_CLOUD_REWARD_CLOTHING_INVALID
	TATTOO_DATA_STRUCT sTattooData
	PED_COMP_ITEM_DATA_STRUCT ItemData
	ItemData = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_PLAYER_MODEL(), compType, compName)
	
	BOOL bFoundShirtWithDecalItem
	bFoundShirtWithDecalItem = FALSE
	
	// T-shirts that use decals use the tattoo system to unlock so do separate checks.
	IF compType = COMP_TYPE_DECL
	AND compName != INT_TO_ENUM(PED_COMP_NAME_ENUM, -1)
	AND ENUM_TO_INT(compName) > ENUM_TO_INT(TATTOO_MP_FM_DLC) // We only ever deal with DLC decls.
		
		IF IS_PLAYER_MALE(PLAYER_ID())
			IF GET_TATTOO_DATA(sTattooData, INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(compName)), TATTOO_MP_FM, PLAYER_PED_ID())
				bFoundShirtWithDecalItem = TRUE
			ENDIF
		ELSE
			IF GET_TATTOO_DATA(sTattooData, INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(compName)), TATTOO_MP_FM_F, PLAYER_PED_ID())
				bFoundShirtWithDecalItem = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF bFoundShirtWithDecalItem
		
	//	SET_MP_TATTOO_UNLOCKED(INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(compName)), TRUE, FALSE, TRUE, FALSE)
		
	//	SET_REWARD_ITEM_UNLOCKED(GET_HASH_KEY(sTattooData.sLabel))
				
		NET_PRINT_TIME() NET_PRINT("     ----------> IS_MP_REWARD_CLOTHING_GIVEN - CD_CONTENTS_DLC  - CLOTHING ITEM GIVEN WITH DECAL = ") NET_PRINT(sTattooData.sLabel) NET_NL()
		
		tlRewardLabel = sTattooData.sLabel		
	//	eMPRewardClothingResult = NET_CLOUD_REWARD_CLOTHING_SHIRT_W_DECAL
		
		RETURN IS_MP_TATTOO_UNLOCKED(INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(compName)))
		
	// Valid clothing item found, that the player doesn't already own.
	ELIF compType != INT_TO_ENUM(PED_COMP_TYPE_ENUM, -1)
	AND (compType != COMP_TYPE_DECL OR NOT bFoundShirtWithDecalItem)
	AND compName != INT_TO_ENUM(PED_COMP_NAME_ENUM, -1)
	AND compName != DUMMY_PED_COMP
	AND IS_PED_COMP_ITEM_AVAILABLE_MP(GET_PLAYER_MODEL(), compType, compName)
	AND NOT IS_STRING_NULL_OR_EMPTY(ItemData.sLabel)
//			AND NOT ARE_STRINGS_EQUAL(ItemData.sLabel, "NO_LABEL")
		
	//	SET_PED_COMP_ITEM_ACQUIRED_MP(GET_PLAYER_MODEL(), compType, compName, TRUE)
		
	//	SET_REWARD_ITEM_UNLOCKED(GET_HASH_KEY(ItemData.sLabel))
		
		tlRewardLabel = ItemData.sLabel	
		NET_PRINT_TIME() NET_PRINT("     ----------> IS_MP_REWARD_CLOTHING_GIVEN - CD_CONTENTS_DLC  - CLOTHING ITEM GIVEN = ") NET_PRINT(ItemData.sLabel) NET_NL()
		
	//	REWARD_ITEM_THROUGH_TRANSACTION(GET_HASH_KEY(ItemData.sLabel))
	
	//	eMPRewardClothingResult = NET_CLOUD_REWARD_CLOTHING_VALID_NOT_OWNED
		
		RETURN IS_PED_COMP_ITEM_ACQUIRED_MP(GET_PLAYER_MODEL(), compType, compName)
		
	// Invalid component, or empty do not include clothing item, or already owned 
	ELSE
		
		//DEBUG PRINT REASON
		#IF IS_DEBUG_BUILD
		IF compType = INT_TO_ENUM(PED_COMP_TYPE_ENUM, -1)
			PRINTLN("     ----------> IS_MP_REWARD_CLOTHING_GIVEN - CD_CONTENTS_DLC - CLOTHING FAILED ON - compType = INT_TO_ENUM(PED_COMP_TYPE_ENUM, -1)")
		ENDIF
		IF compType = COMP_TYPE_DECL
			PRINTLN("     ----------> IS_MP_REWARD_CLOTHING_GIVEN - CD_CONTENTS_DLC - CLOTHING FAILED ON - compType = COMP_TYPE_DECL")
		ENDIF
		IF compName = INT_TO_ENUM(PED_COMP_NAME_ENUM, -1)
			PRINTLN("     ----------> IS_MP_REWARD_CLOTHING_GIVEN - CD_CONTENTS_DLC - CLOTHING FAILED ON - compName = INT_TO_ENUM(PED_COMP_NAME_ENUM, -1)")
		ENDIF
		IF compName = DUMMY_PED_COMP
			PRINTLN("     ----------> IS_MP_REWARD_CLOTHING_GIVEN - CD_CONTENTS_DLC - CLOTHING FAILED ON - compName = DUMMY_PED_COMP")
		ENDIF
		IF NOT IS_PED_COMP_ITEM_AVAILABLE_MP(GET_PLAYER_MODEL(), compType, compName)
			PRINTLN("     ----------> IS_MP_REWARD_CLOTHING_GIVEN - CD_CONTENTS_DLC - CLOTHING FAILED ON - NOT IS_PED_COMP_ITEM_AVAILABLE_MP")
		ENDIF
		IF IS_STRING_NULL_OR_EMPTY(ItemData.sLabel)
			PRINTLN("     ----------> IS_MP_REWARD_CLOTHING_GIVEN - CD_CONTENTS_DLC - CLOTHING FAILED ON - IS_STRING_NULL_OR_EMPTY")
		ENDIF
		#ENDIF
			
		NET_PRINT_TIME() NET_PRINT("     ----------> IS_MP_REWARD_CLOTHING_GIVEN - CD_CONTENTS_DLC  - NO CLOTHING/ALREADY OWNED") NET_NL()
		
		RETURN FALSE
	ENDIF
	
	
ENDFUNC

//returns the tunable for the amount of ammo given to a player at the end of a playlist
FUNC INT GET_AMMO_FOR_GIFT_WEAPON_FROM_TUNABLE(WEAPON_TYPE eWeapon)
	SWITCH eWeapon
		CASE 	WEAPONTYPE_GRENADE			RETURN 	g_sMPTunables.iGRENADE_AMMO_GIFTING
		CASE WEAPONTYPE_SMOKEGRENADE		RETURN 	g_sMPTunables.iSMKGRENADE_AMMO_GIFTING
		CASE WEAPONTYPE_STICKYBOMB			RETURN 	g_sMPTunables.iSTKYBMB_AMMO_GIFTING
		CASE WEAPONTYPE_MOLOTOV				RETURN 	g_sMPTunables.iMOLOTOV_AMMO_GIFTING
		CASE WEAPONTYPE_PISTOL				RETURN 	g_sMPTunables.iPISTOL_AMMO_GIFTING
		CASE WEAPONTYPE_COMBATPISTOL		RETURN 	g_sMPTunables.iPISTOL_AMMO_GIFTING
		CASE WEAPONTYPE_DLC_PISTOL50		RETURN 	g_sMPTunables.iPISTOL_AMMO_GIFTING
		CASE WEAPONTYPE_APPISTOL			RETURN 	g_sMPTunables.iPISTOL_AMMO_GIFTING
		CASE WEAPONTYPE_MICROSMG			RETURN  g_sMPTunables.iSMG_AMMO_GIFTING
		CASE WEAPONTYPE_SMG					RETURN 	g_sMPTunables.iSMG_AMMO_GIFTING
		CASE WEAPONTYPE_DLC_ASSAULTSMG		RETURN  g_sMPTunables.iSMG_AMMO_GIFTING
		CASE WEAPONTYPE_ASSAULTRIFLE		RETURN 	g_sMPTunables.iRIFLE_AMMO_GIFTING
		CASE WEAPONTYPE_CARBINERIFLE		RETURN 	g_sMPTunables.iRIFLE_AMMO_GIFTING
		CASE WEAPONTYPE_ADVANCEDRIFLE		RETURN 	g_sMPTunables.iRIFLE_AMMO_GIFTING
		CASE WEAPONTYPE_MG					RETURN 	g_sMPTunables.iMG_AMMO_GIFTING
		CASE WEAPONTYPE_DLC_ASSAULTMG		RETURN 	g_sMPTunables.iMG_AMMO_GIFTING
		CASE WEAPONTYPE_PUMPSHOTGUN			RETURN 	g_sMPTunables.iSHOTGUN_AMMO_GIFTING
		CASE WEAPONTYPE_SAWNOFFSHOTGUN		RETURN 	g_sMPTunables.iSHOTGUN_AMMO_GIFTING
		CASE WEAPONTYPE_DLC_BULLPUPSHOTGUN	RETURN 	g_sMPTunables.iSHOTGUN_AMMO_GIFTING
		CASE WEAPONTYPE_ASSAULTSHOTGUN		RETURN 	g_sMPTunables.iSHOTGUN_AMMO_GIFTING
		CASE WEAPONTYPE_SNIPERRIFLE			RETURN 	g_sMPTunables.iSNIPER_AMMO_GIFTING
		CASE WEAPONTYPE_HEAVYSNIPER			RETURN 	g_sMPTunables.iSNIPER_AMMO_GIFTING
		CASE WEAPONTYPE_GRENADELAUNCHER		RETURN 	g_sMPTunables.iGRNLAUNCH_AMMO_GIFTING
		CASE WEAPONTYPE_RPG					RETURN 	g_sMPTunables.iRPG_AMMO_GIFTING
		CASE WEAPONTYPE_MINIGUN				RETURN 	g_sMPTunables.iMINIGUNS_AMMO_GIFTING
		CASE WEAPONTYPE_PETROLCAN			RETURN 	g_sMPTunables.iPETROLCAN_AMMO_GIFTING
		CASE WEAPONTYPE_DLC_ASSAULTSNIPER	RETURN 	g_sMPTunables.iASLTSNIP_AMMO_GIFTING
		CASE WEAPONTYPE_COMBATMG			RETURN 	g_sMPTunables.iMG_AMMO_GIFTING
		
		CASE WEAPONTYPE_DLC_SPECIALCARBINE	RETURN 	g_sMPTunables.iRIFLE_AMMO_GIFTING
		CASE WEAPONTYPE_DLC_SNSPISTOL		RETURN 	g_sMPTunables.iPISTOL_AMMO_GIFTING
		CASE WEAPONTYPE_DLC_HEAVYPISTOL		RETURN 	g_sMPTunables.iPISTOL_AMMO_GIFTING
		CASE WEAPONTYPE_DLC_BULLPUPRIFLE	RETURN 	g_sMPTunables.iSHOTGUN_AMMO_GIFTING
		CASE WEAPONTYPE_DLC_GUSENBERG		RETURN 	g_sMPTunables.iRIFLE_AMMO_GIFTING
		
		CASE WEAPONTYPE_DLC_VINTAGEPISTOL	RETURN 	g_sMPTunables.iPISTOL_AMMO_GIFTING
		CASE WEAPONTYPE_DLC_FIREWORK		RETURN 	g_sMPTunables.iFIREWRK_AMMO_GIFTING
		CASE WEAPONTYPE_DLC_MUSKET			RETURN 	g_sMPTunables.iSNIPER_AMMO_GIFTING
		CASE WEAPONTYPE_DLC_FLAREGUN		RETURN 	g_sMPTunables.iFLAREGUN_AMMO_GIFTING
		CASE WEAPONTYPE_DLC_PROXMINE		RETURN 	g_sMPTunables.iPROXMI_AMMO_GIFTING
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC INT GET_AMMO_FOR_GIFT_FROM_TUNABLE(AMMO_TYPE eAmmo)
	SWITCH eAmmo
		CASE AMMOTYPE_PISTOL					RETURN g_sMPTunables.iPISTOL_AMMO_GIFTING
		CASE AMMOTYPE_SMG						RETURN g_sMPTunables.iSMG_AMMO_GIFTING
		CASE AMMOTYPE_RIFLE						RETURN g_sMPTunables.iRIFLE_AMMO_GIFTING
		CASE AMMOTYPE_MG						RETURN g_sMPTunables.iMG_AMMO_GIFTING
		CASE AMMOTYPE_SHOTGUN					RETURN g_sMPTunables.iSHOTGUN_AMMO_GIFTING
		CASE AMMOTYPE_SNIPER					RETURN g_sMPTunables.iSNIPER_AMMO_GIFTING
		CASE AMMOTYPE_PETROL_CAN				RETURN g_sMPTunables.iPETROLCAN_AMMO_GIFTING
		CASE AMMOTYPE_MINIGUN					RETURN g_sMPTunables.iMINIGUNS_AMMO_GIFTING
		CASE AMMOTYPE_GRENADE_LAUNCHER			RETURN g_sMPTunables.iGRNLAUNCH_AMMO_GIFTING
		CASE AMMOTYPE_RPG						RETURN g_sMPTunables.iRPG_AMMO_GIFTING
		CASE AMMOTYPE_STICKY_BOMB				RETURN g_sMPTunables.iSTKYBMB_AMMO_GIFTING
		CASE AMMOTYPE_SMOKE_GRENADE				RETURN g_sMPTunables.iSMKGRENADE_AMMO_GIFTING
		CASE AMMOTYPE_MOLOTOV					RETURN g_sMPTunables.iMOLOTOV_AMMO_GIFTING
		CASE AMMOTYPE_DLC_FLAREGUN				RETURN g_sMPTunables.iFLAREGUN_AMMO_GIFTING
		CASE AMMOTYPE_DLC_FIREWORK				RETURN g_sMPTunables.iFIREWRK_AMMO_GIFTING 
		CASE AMMOTYPE_DLC_PROXMINE				RETURN 	g_sMPTunables.iPROXMI_AMMO_GIFTING
	ENDSWITCH
	RETURN 0
ENDFUNC

FUNC BOOL IS_AMMO_TYPE_VALID_FOR_GIFT(AMMO_TYPE eAmmo)
	SWITCH eAmmo
		CASE AMMOTYPE_PISTOL			
		CASE AMMOTYPE_SMG				
		CASE AMMOTYPE_RIFLE				
		CASE AMMOTYPE_MG				
		CASE AMMOTYPE_SHOTGUN			
		CASE AMMOTYPE_SNIPER			
		CASE AMMOTYPE_PETROL_CAN		
		CASE AMMOTYPE_MINIGUN			
		CASE AMMOTYPE_GRENADE_LAUNCHER	
		CASE AMMOTYPE_RPG				
		CASE AMMOTYPE_STICKY_BOMb		
		CASE AMMOTYPE_SMOKE_GRENADE		
		CASE AMMOTYPE_MOLOTOV			
		CASE AMMOTYPE_DLC_FLAREGUN		
		CASE AMMOTYPE_DLC_FIREWORK		
		CASE AMMOTYPE_DLC_PROXMINE	
			RETURN TRUE
	ENDSWITCH
	RETURN FALSE
ENDFUNC
		

