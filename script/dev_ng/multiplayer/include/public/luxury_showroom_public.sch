//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        luxury_showroom_public.sch																			//
// Description: Contains public functions for maintaining the luxury showroom located opposite the music studio.	//
// 				url:bugstar:7447898 																				//
// Written by:  Joe Davis																							//
// Date:  		17/02/2022																							//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "net_wait_zero.sch"
USING "commands_network.sch"
USING "script_player.sch"
USING "commands_event.sch"
USING "net_include.sch"
USING "net_events.sch"
USING "net_spawn.sch"
USING "net_ambience.sch"
USING "net_GameDir.sch"
USING "net_gang_boss.sch"

#IF FEATURE_DLC_1_2022

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ ENUMS ╞══════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ STRUCTS ╞════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

STRUCT LUXURY_SHOWROOM_LAUNCH_DATA
	BLIP_INDEX blipLuxuryShowroom
	INT iBS
ENDSTRUCT

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ CONSTS ╞═════════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

// LUXURY_SHOWROOM_LAUNCH_DATA.iBS
CONST_INT ciLUXURY_SHOWROOM_LAUNCH_IBS_LAUNCHED_SCRIPT		0

CONST_FLOAT	cfLUXURY_SHOWROOM_LAUNCH_DISTANCE				180.0
CONST_FLOAT	cfLUXURY_SHOWROOM_KILL_DISTANCE					210.0

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ HELPERS / DATA ╞═════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

FUNC VECTOR GET_LUXURY_SHOWROOM_COORDS()
	RETURN <<-788.7220, -239.5369, 37.3525>>
ENDFUNC

FUNC STRING GET_LUXURY_SHOWROOM_SCRIPT_NAME()
	RETURN "am_luxury_showroom"
ENDFUNC

FUNC BOOL IS_PLAYER_NEAR_LUXURY_SHOWROOM(PLAYER_INDEX player)
	RETURN GET_DISTANCE_BETWEEN_COORDS(GET_PLAYER_COORDS(player), GET_LUXURY_SHOWROOM_COORDS()) < cfLUXURY_SHOWROOM_LAUNCH_DISTANCE
ENDFUNC

FUNC STRING GET_LUXURY_SHOWROOM_MENU_TXD()
	RETURN "ShopUI_Title_Luxury_Autos"
ENDFUNC

//╒═══════════════════════════════════════════════════════════════════════════════════╕
//╞════════════════════════════════════╡ FUNCTIONS ╞══════════════════════════════════╡
//╘═══════════════════════════════════════════════════════════════════════════════════╛

/// PURPOSE:
///    Determines if the luxury showroom script should be launched. 
FUNC BOOL SHOULD_LAUNCH_LUXURY_SHOWROOM_SCRIPT(LUXURY_SHOWROOM_LAUNCH_DATA &sData)
	
	IF IS_BIT_SET(sData.iBS, ciLUXURY_SHOWROOM_LAUNCH_IBS_LAUNCHED_SCRIPT)
		RETURN FALSE
	ENDIF
	
	IF g_sMPTunables.bDISABLE_LUXURY_SHOWROOM
		RETURN FALSE
	ENDIF
	
	IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT IS_ENTITY_ALIVE(PLAYER_PED_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_IN_ANY_SIMPLE_INTERIOR() 
	AND NOT IS_PLAYER_WALKING_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	// @TODO: Add to main freemode stagger for performance optimisation.
	IF NOT IS_PLAYER_NEAR_LUXURY_SHOWROOM(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MAINTAIN_LUXURY_SHOWROOM_COMING_SOON()
	IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
	AND IS_SCREEN_FADED_IN()
	AND IS_GAMEPLAY_CAM_RENDERING()
	AND IS_SKYSWOOP_AT_GROUND()
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-796.621948,-234.644302,35.883450>>, <<-788.396729,-249.307037,46.035660>>, 10.000000, DEFAULT, DEFAULT, TM_ON_FOOT)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-779.243042,-248.043655,35.573826>>, <<-791.992004,-253.064865,46.234215>>, 10.000000, DEFAULT, DEFAULT, TM_ON_FOOT)
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_SRM_SOON")
				PRINT_HELP_FOREVER("LUX_SRM_SOON")
			ENDIF
		ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_SRM_SOON")
			CLEAR_HELP()
		ENDIF
	ELIF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_SRM_SOON")
		CLEAR_HELP()
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the launching of the am_luxury_showroom script.
PROC MAINTAIN_LUXURY_SHOWROOM_LAUNCHING(LUXURY_SHOWROOM_LAUNCH_DATA &sData)
	IF SHOULD_LAUNCH_LUXURY_SHOWROOM_SCRIPT(sData)
		REQUEST_SCRIPT(GET_LUXURY_SHOWROOM_SCRIPT_NAME())
		IF HAS_SCRIPT_LOADED(GET_LUXURY_SHOWROOM_SCRIPT_NAME())
			START_NEW_SCRIPT(GET_LUXURY_SHOWROOM_SCRIPT_NAME(), SHOP_STACK_SIZE)
			SET_SCRIPT_AS_NO_LONGER_NEEDED(GET_LUXURY_SHOWROOM_SCRIPT_NAME())
			SET_BIT(sData.iBS, ciLUXURY_SHOWROOM_LAUNCH_IBS_LAUNCHED_SCRIPT)
		ENDIF
	ELIF IS_BIT_SET(sData.iBS, ciLUXURY_SHOWROOM_LAUNCH_IBS_LAUNCHED_SCRIPT)
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(GET_LUXURY_SHOWROOM_SCRIPT_NAME())) = 0
		AND NOT NETWORK_IS_SCRIPT_ACTIVE(GET_LUXURY_SHOWROOM_SCRIPT_NAME(), DEFAULT, TRUE)
			CLEAR_BIT(sData.iBS, ciLUXURY_SHOWROOM_LAUNCH_IBS_LAUNCHED_SCRIPT)
		ENDIF
	ELIF g_sMPTunables.bDISABLE_LUXURY_SHOWROOM
		MAINTAIN_LUXURY_SHOWROOM_COMING_SOON()
	ENDIF
ENDPROC

PROC MAINTAIN_LUXURY_SHOWROOM_INTRO_HELP_TEXT(LUXURY_SHOWROOM_LAUNCH_DATA &sData)
	IF IS_BIT_SET(g_sLuxuryShowroom.iBS, BS_LUXURY_SHOWROOM_GLOBAL_DISPLAYED_INTRO_HELP_TEXT)
		IF NOT IS_BIT_SET(g_sLuxuryShowroom.iBS, BS_LUXURY_SHOWROOM_GLOBAL_CHECKED_FLASHING_BLIP)
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("LUX_SRM_INIT")
			
				IF IS_BLIP_FLASHING(sData.blipLuxuryShowroom)
					SET_BLIP_FLASHES(sData.blipLuxuryShowroom, FALSE)
				ENDIF
				
				SET_BIT(g_sLuxuryShowroom.iBS, BS_LUXURY_SHOWROOM_GLOBAL_CHECKED_FLASHING_BLIP)
			ENDIF
		ENDIF
		
		EXIT
	ENDIF
	
	IF GET_PACKED_STAT_INT(PACKED_MP_INT_DISPLAYED_LUXURY_SHOWROOM_INTRO_HELP) >= 3
		PRINTLN("MAINTAIN_LUXURY_SHOWROOM_INTRO_HELP_TEXT - Displayed 3 times already.")
		SET_BIT(g_sLuxuryShowroom.iBS, BS_LUXURY_SHOWROOM_GLOBAL_DISPLAYED_INTRO_HELP_TEXT)
		EXIT
	ENDIF
	
	IF NOT IS_PAUSE_MENU_ACTIVE()
	AND IS_LUXURY_SHOWROOM_SCRIPT_RUNNING()
	AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
	AND IS_BLIP_ON_MINIMAP(sData.blipLuxuryShowroom)
	AND IS_SCREEN_FADED_IN()
		PRINT_HELP("LUX_SRM_INIT")
		
		IF NOT IS_BLIP_FLASHING(sData.blipLuxuryShowroom)
			SET_BLIP_FLASHES(sData.blipLuxuryShowroom, TRUE)
		ENDIF
		
		INT iTimesDisplayed = GET_PACKED_STAT_INT(PACKED_MP_INT_DISPLAYED_LUXURY_SHOWROOM_INTRO_HELP)
		iTimesDisplayed++
		SET_PACKED_STAT_INT(PACKED_MP_INT_DISPLAYED_LUXURY_SHOWROOM_INTRO_HELP, iTimesDisplayed)
		
		PRINTLN("MAINTAIN_LUXURY_SHOWROOM_INTRO_HELP_TEXT - Displayed ", iTimesDisplayed, " times.")
		
		SET_BIT(g_sLuxuryShowroom.iBS, BS_LUXURY_SHOWROOM_GLOBAL_DISPLAYED_INTRO_HELP_TEXT)
	ENDIF
ENDPROC

/// PURPOSE:
///    Maintains the freemode blip for luxury showroom.
PROC MAINTAIN_LUXURY_SHOWROOM_BLIP(LUXURY_SHOWROOM_LAUNCH_DATA &sData)
	IF SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_LUXURY_AUTO)
	OR g_sMPTunables.bDISABLE_LUXURY_SHOWROOM
	OR IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
	OR HIDE_BLIP_BECAUSE_PLAYER_IS_STARTING_RACE()
	OR NOT HAS_PLAYER_COMPLETED_RACE_AND_DM_TUTORIAL(PLAYER_ID())
	OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
	OR SHOULD_HIDE_ALL_MISC_BLIPS_FOR_FM_EVENT()
	#IF FEATURE_GEN9_EXCLUSIVE
	OR IS_PLAYER_ON_MP_INTRO()
	#ENDIF
	OR IS_PLAYER_IN_MP_NON_SIMPLE_INTEIROR_PROPERTY(PLAYER_ID(), FALSE)
		IF DOES_BLIP_EXIST(sData.blipLuxuryShowroom)
			REMOVE_BLIP(sData.blipLuxuryShowroom)
		ENDIF
	ELSE
		IF NOT DOES_BLIP_EXIST(sData.blipLuxuryShowroom)
			sData.blipLuxuryShowroom = CREATE_BLIP_FOR_COORD(GET_LUXURY_SHOWROOM_COORDS())
			SET_BLIP_SPRITE(sData.blipLuxuryShowroom, RADAR_TRACE_LUXURY_CAR_SHOWROOM)
			SET_BLIP_NAME_FROM_TEXT_FILE(sData.blipLuxuryShowroom, "LUX_SRM_BLIP")
			SET_BLIP_AS_SHORT_RANGE(sData.blipLuxuryShowroom, TRUE)
			PRINTLN("MAINTAIN_LUXURY_SHOWROOM_BLIP: added blip for luxury showroom")
		ELSE
			// Bobby Yotov: Suppressing help text without changing the blip functionality (url:bugstar:7787545)
			IF NOT IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
			AND NOT GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION (PLAYER_ID()) 
				MAINTAIN_LUXURY_SHOWROOM_INTRO_HELP_TEXT(sData)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#ENDIF // #IF FEATURE_DLC_1_2022
