// name:        network_ticker_ui.sch
// description: Functionality for drawing the ticker system. 
//				Ticker drawing is completely separate from ticker logic. 
//				Key ticker data held in globals so we can easily add 
//				tickers from multiple scripts.

USING "globals.sch"

USING "commands_network.sch"
USING "net_include.sch"

USING "net_ticker_logic.sch"

USING "net_include.sch"
USING "cellphone_public.sch"
USING "net_stat_tracking.sch"
USING "net_common_functions.sch"

USING "net_blips.sch"

#IF IS_DEBUG_BUILD
	USING "profiler.sch"
#ENDIF

//BOOL bEnableFeedTickers = TRUE

ENUM DISPLAY_TICKER_TYPE
	DISPLAY_TICKER_TO_FEED, 
	DISPLAY_TICKER_AS_GOD_TEXT
ENDENUM

	
#IF IS_DEBUG_BUILD
	
	PROC ADD_TICKER_WIDGETS()
	
		START_WIDGET_GROUP("Tickers")			
			
			//ADD_WIDGET_BOOL("bEnableFeedTickers", bEnableFeedTickers)
			
			START_WIDGET_GROUP("Ticker Test")
				ADD_WIDGET_BOOL("bPrintTestTicker", bPrintTestTicker)
				ADD_WIDGET_BOOL("bPrintTestCashTicker", bPrintTestCashTicker)
				ADD_WIDGET_BOOL("bPrintTest1PlayerTicker", bPrintTest1PlayerTicker)
				ADD_WIDGET_BOOL("bPrintTest2PlayerTicker", bPrintTest2PlayerTicker)
				ADD_WIDGET_BOOL("bPrintTest3PlayerTicker", bPrintTest3PlayerTicker)
				ADD_WIDGET_BOOL("bPrintTest4PlayerTicker", bPrintTest4PlayerTicker)
//				ADD_WIDGET_BOOL("bPrintTestWithTeamName", bPrintTestWithTeamName)
				ADD_WIDGET_BOOL("bPrintTestWithCrewName", bPrintTestWithCrewName)
				ADD_WIDGET_BOOL("bTickerTestDontUseCodeNames", bTickerTestDontUseCodeNames)
				ADD_WIDGET_BOOL("bUseOldCrewTicker", bUseOldCrewTicker)								
			STOP_WIDGET_GROUP()				
			
			START_WIDGET_GROUP("Ticker Time")
				ADD_WIDGET_INT_SLIDER("Start fade out", TICKER_FADE_OUT_START_TIME,-HIGHEST_INT, HIGHEST_INT, 1)	
				ADD_WIDGET_INT_SLIDER("End fade out", TICKER_MAX_DISPLAY_TIME,-HIGHEST_INT, HIGHEST_INT, 1)	
				ADD_WIDGET_INT_SLIDER("End fade in", TICKER_FADE_IN_TIME,-HIGHEST_INT, HIGHEST_INT, 1)	
				ADD_WIDGET_INT_SLIDER("Ticker Fade out time", TICKET_MAX_FADE_OUT_TIME,-HIGHEST_INT, HIGHEST_INT, 1)	
			STOP_WIDGET_GROUP()


			
		STOP_WIDGET_GROUP()
	
	ENDPROC
	
		
	PROC UPDATE_TICKER_WIDGETS()
	
		IF (bPrintTestTicker)

			bPrintTestTicker = FALSE
		ENDIF
		
		IF (bPrintTest1PlayerTicker)
			PRINT_TICKER_WITH_PLAYER_NAME("TICK_TEST_1", PLAYER_ID(), bTickerTestDontUseCodeNames)
			bPrintTest1PlayerTicker = FALSE
		ENDIF
		
		IF (bPrintTest2PlayerTicker)
			PRINT_TICKER_WITH_TWO_PLAYER_NAMES("TICK_TEST_2", PLAYER_ID(), PLAYER_ID(), bTickerTestDontUseCodeNames)
			bPrintTest2PlayerTicker = FALSE
		ENDIF
		
		IF (bPrintTest3PlayerTicker)
			PRINT_TICKER_WITH_THREE_PLAYER_NAMES("TICK_TEST_3", PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), bTickerTestDontUseCodeNames)
			bPrintTest3PlayerTicker = FALSE
		ENDIF
		
		IF (bPrintTest4PlayerTicker)
			PRINT_TICKER_WITH_FOUR_PLAYER_NAMES("TICK_TEST_4", PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), PLAYER_ID(), bTickerTestDontUseCodeNames)
			bPrintTest4PlayerTicker = FALSE
		ENDIF
		
//		IF (bPrintTestWithTeamName)
//			PRINT_TICKER_WITH_TEAM_NAME("TICK_TEST_1", TEAM_COP)
//			bPrintTestWithTeamName = FALSE
//		ENDIF
		
		IF (bPrintTestWithCrewName)
			PRINT_TICKER_WITH_PLAYER_NAME("TICK_TEST_1", PLAYER_ID(), FALSE, FALSE, TRUE )
			bPrintTestWithCrewName = FALSE
		ENDIF
		
		IF (bPrintTestCashTicker)
			PRINT_CASH_TICKER(100)
			bPrintTestCashTicker = FALSE
		ENDIF

	ENDPROC	
	
#ENDIF






//INFO: 
//PARAM NOTES: Duration is an INT in millisecs
//PURPOSE: Overwrites any current message with a string inside another string string. More info..
PROC PRINT_COLOURED_STRING_IN_STRING_NOW(STRING pTextLabel, STRING pShortTextLabel, INT Duration, HUD_COLOURS Colour)
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(pShortTextLabel)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

//INFO: 
//PARAM NOTES: Duration is an INT in millisecs
//PURPOSE: Overwrites any current message with a string inside another string string. More info..
PROC PRINT_STRINGS_WITH_TWO_COLOURED_STRINGS_NOW(STRING pTextLabel, STRING pShortTextLabel1, STRING pShortTextLabel2, INT Duration, HUD_COLOURS Colour1, HUD_COLOURS Colour2)

#IF IS_DEBUG_BUILD
	NET_PRINT("PRINT_STRINGS_WITH_TWO_COLOURED_STRINGS_NOW called with '") 
	NET_PRINT(GET_STRING_FROM_TEXT_FILE(pTextLabel ))
	NET_PRINT("'")
	NET_NL()
#ENDIF

	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
		IF NOT (Colour1 = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour1)
		ENDIF
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(pShortTextLabel1)
		IF NOT (Colour2 = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour2)
		ENDIF
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(pShortTextLabel2)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

//INFO: 
//PARAM NOTES: Duration is an INT in millisecs
//PURPOSE: Overwrites any current message with a string inside another string string. More info..
PROC PRINT_STRINGS_WITH_TWO_COLOURED_STRINGS_AND_FLOAT_NOW(STRING pTextLabel, STRING pShortTextLabel1, STRING pShortTextLabel2, FLOAT fFloat, INT Duration, HUD_COLOURS Colour1, HUD_COLOURS Colour2)
	
#IF IS_DEBUG_BUILD
	NET_PRINT("PRINT_STRINGS_WITH_TWO_COLOURED_STRINGS_AND_FLOAT_NOW called with '") 
	NET_PRINT(GET_STRING_FROM_TEXT_FILE(pTextLabel ))
	NET_PRINT("'")
	NET_NL()
#ENDIF
	
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
		IF NOT (Colour1 = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour1)
		ENDIF
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(pShortTextLabel1)
		IF NOT (Colour2 = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour2)
		ENDIF
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(pShortTextLabel2)
		ADD_TEXT_COMPONENT_FLOAT(fFloat, 1)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC



PROC PRINT_STRING_WITH_PLAYER_NAME_NOW(STRING pTextLabel, STRING PlayerName, INT Duration, HUD_COLOURS Colour)
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
		IF NOT (Colour = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour)
		ENDIF
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

PROC PRINT_STRING_WITH_PLAYER_NAME_AND_STRING_NOW(STRING pTextLabel, STRING PlayerName, STRING SubString, INT Duration, HUD_COLOURS Colour1, HUD_COLOURS Colour2)
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
		IF NOT (Colour1 = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour1)
		ENDIF
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName)
		IF NOT (Colour2 = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour2)
		ENDIF	
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubString)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

PROC PRINT_STRING_WITH_STRING_AND_PLAYER_NAME_NOW(STRING pTextLabel, STRING SubString, STRING PlayerName, INT Duration, HUD_COLOURS Colour1, HUD_COLOURS Colour2)
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
		IF NOT (Colour1 = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour1)
		ENDIF
		ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(SubString)
		IF NOT (Colour2 = HUD_COLOUR_PURE_WHITE)
			SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour2)
		ENDIF	
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC

PROC PRINT_STRING_WITH_TWO_PLAYER_NAMES_NOW(STRING pTextLabel, STRING PlayerName1, STRING PlayerName2, INT Duration, HUD_COLOURS Colour1, HUD_COLOURS Colour2)
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour1)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName1)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour2)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName2)
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC


PROC PRINT_STRING_WITH_THREE_PLAYER_NAMES_NOW(STRING pTextLabel, STRING PlayerName1, STRING PlayerName2, STRING PlayerName3, INT Duration, HUD_COLOURS Colour1, HUD_COLOURS Colour2, HUD_COLOURS Colour3)
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour1)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName1)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour2)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName2)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour3)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName3)		
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC


PROC PRINT_STRING_WITH_FOUR_PLAYER_NAMES_NOW(STRING pTextLabel, STRING PlayerName1, STRING PlayerName2, STRING PlayerName3, STRING PlayerName4, INT Duration, HUD_COLOURS Colour1, HUD_COLOURS Colour2, HUD_COLOURS Colour3, HUD_COLOURS Colour4)
	BEGIN_TEXT_COMMAND_PRINT(pTextLabel)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour1)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName1)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour2)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName2)
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour3)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName3)		
		SET_COLOUR_OF_NEXT_TEXT_COMPONENT(Colour4)
		ADD_TEXT_COMPONENT_SUBSTRING_PLAYER_NAME(PlayerName4)			
	END_TEXT_COMMAND_PRINT(Duration, TRUE)
ENDPROC
