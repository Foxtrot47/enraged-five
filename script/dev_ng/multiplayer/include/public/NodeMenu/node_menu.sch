//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        	node_menu.sch																	
/// Description: 	The main header file to include to run a node menu
///	
/// Written by:  Online Technical Team: Tom Turner,															
/// Date created:  		27/08/2019
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "Core\node_menu_struct.sch"
USING "Core\node_menu_item_struct.sch"
USING "Core\node_menu_ui.sch"
USING "Core\node_menu_input.sch"
USING "Core\node_menu_item.sch"
USING "Core\node_menu_flags.sch"
USING "Factories\node_menu_factory_simple.sch"
USING "Factories\node_menu_factory_scaleform.sch"

/// PURPOSE:
///    Tells the node menu to rebuild
/// PARAMS:
///    sInst - node menu instance
PROC NODE_MENU_REBUILD(NODE_MENU_STRUCT &sInst)
	NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_REBUILD_MENU)
ENDPROC

/// PURPOSE:
///    Closes the node menu, this will prevent NODE_MENU_UPDATE_AND_DRAW 
///    from updating and displaying the menu
/// PARAMS:
///    sInst - node menu instance
///    bResetOnClose - if the menu should go back to starting node and fpOnReset current the selection
PROC NODE_MENU_CLOSE(NODE_MENU_STRUCT &sInst)
	NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_CLOSE_MENU)
ENDPROC

/// PURPOSE:
///    Opens the node menu, this will allow NODE_MENU_UPDATE_AND_DRAW 
///    to update and display the menu
/// PARAMS:
///    sInst - the node menu instance
PROC NODE_MENU_OPEN(NODE_MENU_STRUCT &sInst)
	NODE_MENU_BS_CLEAR_FLAG(sInst, NODE_MENU_BS_CLOSE_MENU)
ENDPROC

/// PURPOSE:
///    Checks if the node menu is in an open state
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    TRUE if the menu is open
FUNC BOOL NODE_MENU_IS_OPEN(NODE_MENU_STRUCT &sInst)
	RETURN NOT NODE_MENU_BS_IS_FLAG_SET(sInst, NODE_MENU_BS_CLOSE_MENU)
ENDFUNC

/// PURPOSE:
///    Updates the node menus input and draws the menu, 
///    will not draw on the frame the user closes the menu OR if
///    NODE_MENU_CLOSE has been called
/// PARAMS:
///    sInst - node menu instance
/// RETURNS:
///    TRUE if the menu is closed either by the user or NODE_MENU_CLOSE
FUNC BOOL NODE_MENU_UPDATE_AND_DRAW(NODE_MENU_STRUCT &sInst)
	IF NOT NODE_MENU_IS_OPEN(sInst)
		RETURN TRUE
	ENDIF
	
	IF NODE_MENU_UPDATE(sInst)
		RETURN TRUE
	ENDIF
	
	NODE_MENU_DRAW(sInst)
	RETURN FALSE
ENDFUNC

PROC NODE_MENU_CLEANUP(NODE_MENU_STRUCT &sInst)
	NODE_MENU_CLEAN_UP_ASSETS(sInst)
	NODE_MENU_RESET(sInst, TRUE)
ENDPROC

