//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        	node_bitset.sch																	
/// Description: 	All the bitsets used by the node menu system with helper functions				
///	
/// Written by:  Online Technical Team: Tom Turner,															
/// Date created:  		27/08/2019
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "Core\node_menu_struct.sch"

/// PURPOSE: 
///    Bitsets used by the node menu system
ENUM NODE_MENU_BITS
	NODE_MENU_BS_IS_MENU_SETUP
	,NODE_MENU_BS_REBUILD_MENU
	,NODE_MENU_BS_CLOSE_MENU
	,NODE_MENU_BS_AWAITING_CONFIRMATION
	,NODE_MENU_BS_ALWAYS_HIGHLIGHT_FIRST_ITEM
	,NODE_MENU_BS_AWAITING_TEXT_INPUT_CONFIRMATION
	// REMEMBER TO FILL OUT THE LOOKUP BELLOW
ENDENUM

/// PURPOSE:
///    Gets the name of the node menu bitset as a string
/// PARAMS:
///    eBitSetNameToGet - the bitset to get the name of
/// RETURNS:
///    The debug name of the bitset, or MISSING_BS_NAME if it hasn't been defined
DEBUGONLY FUNC STRING NODE_MENU_BS_GET_NAME(NODE_MENU_BITS eBitSetNameToGet)
	SWITCH eBitSetNameToGet
		CASE NODE_MENU_BS_IS_MENU_SETUP							RETURN "NODE_MENU_BS_IS_MENU_SETUP"
		CASE NODE_MENU_BS_REBUILD_MENU							RETURN "NODE_MENU_BS_REBUILD_MENU"
		CASE NODE_MENU_BS_CLOSE_MENU							RETURN "NODE_MENU_BS_CLOSE_MENU"
		CASE NODE_MENU_BS_AWAITING_CONFIRMATION					RETURN "NODE_MENU_BS_AWAITING_CONFIRMATION"
		CASE NODE_MENU_BS_ALWAYS_HIGHLIGHT_FIRST_ITEM			RETURN "NODE_MENU_BS_ALWAYS_HIGHLIGHT_FIRST_ITEM"
		CASE NODE_MENU_BS_AWAITING_TEXT_INPUT_CONFIRMATION		RETURN "NODE_MENU_BS_AWAITING_TEXT_INPUT_CONFIRMATION"
	ENDSWITCH
	
	CASSERTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_BS_GET_NAME - No name assigned to bit: ",
	ENUM_TO_INT(eBitSetNameToGet), " - Make sure any new bits have their name defined in the lookup")
	RETURN "MISSING_BS_NAME"
ENDFUNC

/// PURPOSE:
///    Checks if a node menu bit is set for the given instance
/// PARAMS:
///    sInst - node menu instance to check
///    eBitSet - bit to check
/// RETURNS:
///    TRUE if the bit is set
FUNC BOOL NODE_MENU_BS_IS_FLAG_SET(NODE_MENU_STRUCT &sInst, NODE_MENU_BITS eBitSet)
	RETURN IS_BIT_SET_ENUM(sInst.iBS, eBitSet)
ENDFUNC

/// PURPOSE:
///    Enables a bit on the given node menu instance
/// PARAMS:
///    sInst - node menu instance to enable the bit on
///    eBitSet - the bit to enable
///    bEnable - if the bit should be enabled or disabled
PROC NODE_MENU_BS_ENABLE_FLAG(NODE_MENU_STRUCT &sInst, NODE_MENU_BITS eBitSet, BOOL bEnable)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_BS_ENABLE_FLAG - Enabling to ", bEnable,
	" ", NODE_MENU_BS_GET_NAME(eBitSet))
	ENABLE_BIT_ENUM(sInst.iBS, eBitSet, bEnable)
ENDPROC

/// PURPOSE:
///    Sets the bit on the given node menu instance
/// PARAMS:
///    sInst - node menu instance to set the bit on
///    eBitSet - the bit to set
PROC NODE_MENU_BS_SET_FLAG(NODE_MENU_STRUCT &sInst, NODE_MENU_BITS eBitSet)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_BS_SET_FLAG - Setting ",
	NODE_MENU_BS_GET_NAME(eBitSet))
	SET_BIT_ENUM(sInst.iBS, eBitSet)
ENDPROC

/// PURPOSE:
///    Clears the bit on the given node menu instance
/// PARAMS:
///    sInst - node menu instance to clear the bit on
///    eBitSet - the bit to clear
PROC NODE_MENU_BS_CLEAR_FLAG(NODE_MENU_STRUCT &sInst, NODE_MENU_BITS eBitSet)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_BS_CLEAR_FLAG - Clearing ",
	NODE_MENU_BS_GET_NAME(eBitSet))
	CLEAR_BIT_ENUM(sInst.iBS, eBitSet)
ENDPROC

/// PURPOSE:
///    Clears all flags on the node menu instance
/// PARAMS:
///    sInst - node menu instance to clear all flags on
PROC NODE_MENU_BS_CLEAR_ALL_FLAGS(NODE_MENU_STRUCT &sInst)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_BS_CLEAR_ALL_FLAGS - Clearing all flags")
	sInst.iBS = 0
ENDPROC
