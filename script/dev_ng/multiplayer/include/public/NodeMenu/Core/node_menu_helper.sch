//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        	node_menu_helper.sch																	
/// Description: 	Helper for node menu system e.g. node getters			
///	
/// Written by:  Online Technical Team: Tom Turner,															
/// Date created:  		27/08/2019
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "Globals.sch"

USING "Core\node_menu_struct.sch"
USING "Core\node_menu_flags.sch"
USING "Core\node_menu_item.sch"

///----------------------------------------
///    MENU ITEM HELPERS
///----------------------------------------



