USING "Core\node_menu_item_struct.sch"

/// PURPOSE:
///    Gets the child count on the menu item
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC INT NODE_MENU_ITEM_GET_MAX_CHILD_COUNT(NODE_MENU_ITEM_STRUCT &sMenuItem)
	T_NODE_MENU_ITEM_GET_FROM_LOOKUP fpChildLookup = NODE_MENU_ITEM_GET_CHILDREN_CONNECTION(sMenuItem)
	
	IF fpChildLookup = NULL
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_ITEM_GET_CHILD_COUNT - No lookup assigned")
		RETURN 0
	ENDIF
	
	// Count for the first time
	IF sMenuItem.iChildCount = NODE_MENU_ITEM_COUNT_CHILD_DYNAMICALLY
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_ITEM_GET_CHILD_COUNT - Counting all possible children")
		sMenuItem.iChildCount = 0
		NODE_MENU_ITEM_STRUCT sCurrentChild
		NODE_MENU_ITEM_LOOKUP_STATUS eStatus = NODE_MENU_ITEM_LOOKUP_STATUS_INVALID
		
		/// Hey you, call stack overflow in this loop? you forgot to return NODE_MENU_ITEM_LOOKUP_STATUS_END_OF_LOOKUP at the end of your lookup
		WHILE eStatus != NODE_MENU_ITEM_LOOKUP_STATUS_END_OF_LOOKUP
			eStatus = CALL fpChildLookup(sMenuItem.iChildCount, sCurrentChild)
			
			// We count the child regardless of return result (except for end of lookup) because this is the max possible child count
			IF eStatus != NODE_MENU_ITEM_LOOKUP_STATUS_END_OF_LOOKUP
				sMenuItem.iChildCount++
				CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_ITEM_GET_CHILD_COUNT - counting child")
			ENDIF
		ENDWHILE
	ENDIF
	
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_ITEM_GET_CHILD_COUNT - child count: ", sMenuItem.iChildCount)
	RETURN sMenuItem.iChildCount
ENDFUNC

/// PURPOSE:
///    Gets the max child index of the menu item
/// PARAMS:
///    sMenuItem - the menu item instance  
FUNC INT NODE_MENU_ITEM_GET_MAX_CHILD_INDEX(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN IMAX(NODE_MENU_ITEM_GET_MAX_CHILD_COUNT(sMenuItem) - 1, 0)
ENDFUNC

/// PURPOSE:
///    Checks if the node is currently hidden
/// PARAMS:
///    sInst - the node to check
/// RETURNS:
///    TRUE if the node is hidden
FUNC BOOL NODE_MENU_ITEM_IS_NODE_HIDDEN(NODE_MENU_ITEM_STRUCT &sInst)
	// Is hidden check is assigned, default to not hidden
	IF sInst.isHidden = NULL
		RETURN FALSE
	ENDIF

	RETURN CALL sInst.isHidden()
ENDFUNC

/// PURPOSE:
///    Counts how many of the menu items children are hidden
/// PARAMS:
///    sMenuItem - the menu item to check
/// RETURNS:
///    The count of hidden children
FUNC INT NODE_MENU_ITEM_GET_MAX_HIDDEN_CHILD_COUNT(NODE_MENU_ITEM_STRUCT &sMenuItem)
	T_NODE_MENU_ITEM_GET_FROM_LOOKUP fpChildLookup = NODE_MENU_ITEM_GET_CHILDREN_CONNECTION(sMenuItem)
	
	IF fpChildLookup = NULL
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_ITEM_GET_MAX_VISIBLE_CHILD_COUNT - No lookup assigned")
		RETURN 0
	ENDIF
	
	INT iHiddenCount = 0
	INT iItemIndex = 0
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_ITEM_GET_MAX_VISIBLE_CHILD_COUNT - Counting all possible children")
	NODE_MENU_ITEM_STRUCT sCurrentChild
	NODE_MENU_ITEM_LOOKUP_STATUS eStatus = NODE_MENU_ITEM_LOOKUP_STATUS_INVALID
	
	/// Hey you, call stack overflow in this loop? you forgot to return NODE_MENU_ITEM_LOOKUP_STATUS_END_OF_LOOKUP at the end of your lookup
	WHILE eStatus != NODE_MENU_ITEM_LOOKUP_STATUS_END_OF_LOOKUP
		eStatus = CALL fpChildLookup(iItemIndex, sCurrentChild)
		
		IF eStatus != NODE_MENU_ITEM_LOOKUP_STATUS_END_OF_LOOKUP
		AND NODE_MENU_ITEM_IS_NODE_HIDDEN(sCurrentChild)
			iHiddenCount++
			CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_ITEM_GET_MAX_VISIBLE_CHILD_COUNT - counting child")
		ENDIF
		
		iItemIndex++
	ENDWHILE

	RETURN IMAX(0, iHiddenCount - 1)
ENDFUNC

/// PURPOSE:
///    Checks if the node is highlightable
/// PARAMS:
///    sInst - the node to check
/// RETURNS:
///    TRUE if the node is highlightable
FUNC BOOL NODE_MENU_ITEM_IS_HIGHLIGHTABLE(NODE_MENU_ITEM_STRUCT &sInst)
	// Is highlightable check not assigned, default to is highlightable
	IF sInst.isHighlightable = NULL
		RETURN TRUE
	ENDIF

	RETURN CALL sInst.isHighlightable()
ENDFUNC

/// PURPOSE:
///    Checks if the node is currently selectable
/// PARAMS:
///    sInst - the node to check
/// RETURNS:
///    TRUE if the node is selectable
FUNC BOOL NODE_MENU_ITEM_IS_NODE_SELECTABLE(NODE_MENU_ITEM_STRUCT &sInst)
	// Is selectable check not assigned, default to is selectable
	IF sInst.isSelectable = NULL
		RETURN TRUE
	ENDIF

	RETURN CALL sInst.isSelectable()
ENDFUNC

/// PURPOSE:
///    Gets the menu items parent if it has one
/// PARAMS:
///    sInst - the menu item to get the parent of
///    sParentOut - the retrieved
/// RETURNS:
///    TRUE if the parent was retrieved successfully
FUNC BOOL NODE_MENU_ITEM_GET_PARENT(NODE_MENU_ITEM_STRUCT &sInst, NODE_MENU_ITEM_STRUCT &sParentOut)
	IF sInst.getParent = NULL
		PRINTLN("[NODE_MENU] GET_PARENT - Parent not assigned to item")
		RETURN FALSE
	ENDIF	
	
	// Success
	CALL sInst.getParent(sParentOut)
	PRINTLN("[NODE_MENU] GET_PARENT - retrieved successfully")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Gets the child at the given index of the menu item if it has children
/// PARAMS:
///    sInst - the menu item to grab the child from
///    iChildIndex - the index of the child to grab
///    sChildOut - the retrieved child
/// RETURNS:
///    TRUE if the child was successfully retieved or we hit the end of the lookup because there are no children
FUNC BOOL NODE_MENU_ITEM_GET_CHILD(NODE_MENU_ITEM_STRUCT &sInst, INT iChildIndex, NODE_MENU_ITEM_STRUCT &sChildOut)	
	// Has no children getter set
	IF sInst.getChildren = NULL
		PRINTLN("[NODE_MENU] GET_CHILD - Children lookup not assigned to item")
		RETURN FALSE
	ENDIF
	
	SWITCH CALL sInst.getChildren(iChildIndex, sChildOut)
		CASE NODE_MENU_ITEM_LOOKUP_STATUS_SUCCESS 
			PRINTLN("[NODE_MENU] GET_CHILD - Child ", iChildIndex, " retrieved successfully")
			RETURN TRUE
		CASE NODE_MENU_ITEM_LOOKUP_STATUS_END_OF_LOOKUP
			PRINTLN("[NODE_MENU] GET_CHILD - hit end of lookup, actual child count in lookup: ", NODE_MENU_ITEM_GET_MAX_CHILD_COUNT(sInst))
			RETURN TRUE
		CASE NODE_MENU_ITEM_LOOKUP_STATUS_FAILED
			PRINTLN("[NODE_MENU] GET_CHILD - Child ", iChildIndex, " lookup returned fail")
			RETURN FALSE
	ENDSWITCH
	
	// Failed, invalid return status
	CASSERTLN(DEBUG_NODE_MENU, "[NODE_MENU] GET_CHILD - ",
		"Failed to get current selected child at index: ", iChildIndex,
		" from menu item ", Get_String_From_TextLabel(NODE_MENU_ITEM_GET_DISPLAY_NAME(sInst)), 
		" make sure the item returns NODE_MENU_ITEM_LOOKUP_STATUS_SUCCESS in the look up")
	DEBUG_PRINTCALLSTACK()
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Triggers the menu items add event, allowing it to add itself to whichever menu frontend is being used
/// PARAMS:
///    sInst - the menu item to add
///    iParamRow - the row the item will be insserted into
PROC NODE_MENU_ITEM_TRIGGER_ON_ADD_EVENT(NODE_MENU_ITEM_STRUCT &sInst, INT iParamRow)
	// No add item defined so don't attempt to call it
	IF sInst.addItem = NULL
		EXIT
	ENDIF
	
	CALL sInst.addItem(sInst, iParamRow)
ENDPROC

/// PURPOSE:
///   	Triggers the menu items on highlight event
/// PARAMS:
///    sInst - the menu item to trigger the on highlight event on
/// RETURNS:
///    TRUE if the highlight event wants the menu to rebuild
///    FALSE if the menu shouldn't rebuild or there is no on highlight event assigned
FUNC BOOL NODE_MENU_ITEM_TRIGGER_ON_HIGHLIGHT_EVENT(NODE_MENU_ITEM_STRUCT &sInst)
	IF sInst.onHighlight = NULL
		RETURN FALSE
	ENDIF
	
	RETURN CALL sInst.onHighlight()
ENDFUNC

/// PURPOSE:
///    Triggers the menu items on highlight update event, to be called when the menu item is 
///    highlighted
/// PARAMS:
///    sInst - the menu item to trigger the highlight update for
/// RETURNS:
///    TRUE if the highlight update wants the menu to rebuild
FUNC BOOL NODE_MENU_ITEM_TRIGGER_ON_HIGHLIGHT_UPDATE_EVENT(NODE_MENU_ITEM_STRUCT &sInst)
	// No highlight update assigned so no need to rebuild menu
	IF sInst.onHighlightUpdate = NULL
		RETURN FALSE
	ENDIF
	
	RETURN CALL sInst.onHighlightUpdate()
ENDFUNC

/// PURPOSE:
///   	Triggers the menu items on unhighlight event
/// PARAMS:
///    sInst - the menu item to trigger the on unhighlight event on
/// RETURNS:
///    TRUE if the unhighlight event wants the menu to rebuild
///    FALSE if the menu shouldn't rebuild or there is no on unhighlight event assigned 
FUNC BOOL NODE_MENU_ITEM_TRIGGER_ON_UNHIGHLIGHT_EVENT(NODE_MENU_ITEM_STRUCT &sInst)
	IF sInst.onUnHighlight = NULL
		RETURN FALSE
	ENDIF
	
	RETURN CALL sInst.onUnHighlight()
ENDFUNC

/// PURPOSE:
///    Triggers the menu items on press event
/// PARAMS:
///    sInst - the menu item to trigger the on press event for
PROC NODE_MENU_ITEM_TRIGGER_ON_PRESS_EVENT(NODE_MENU_ITEM_STRUCT &sInst)
	IF sInst.onPress = NULL
		EXIT
	ENDIF
	
	CALL sInst.onPress()
ENDPROC

/// PURPOSE:
///    Triggers the menu items on back event, to be triggered when we press back when hovering
///    over the item
/// PARAMS:
///    sInst - the menu item to trigger the on back event for
PROC NODE_MENU_ITEM_TRIGGER_ON_BACK_EVENT(NODE_MENU_ITEM_STRUCT &sInst)
	IF sInst.onBack = NULL
		EXIT
	ENDIF
	
	CALL sInst.onBack()
ENDPROC

/// PURPOSE:
///    Triggers the menu items on become parent event, to be triggered when the menu item
///    becomes a parent/submenu
/// PARAMS:
///    sInst - the menu item to trigger the on become parent event for
PROC NODE_MENU_ITEM_TRIGGER_ON_BECOME_PARENT_EVENT(NODE_MENU_ITEM_STRUCT &sInst)
	IF sInst.onBecomeParent = NULL
		EXIT
	ENDIF
	
	CALL sInst.onBecomeParent()
ENDPROC

/// PURPOSE:
///    Triggers the menu items on value change event
/// PARAMS:
///    sInst - the menu item to trigger the on value change event for
PROC NODE_MENU_ITEM_TRIGGER_ON_VALUE_CHANGE_EVENT(NODE_MENU_ITEM_STRUCT &sInst)
	IF sInst.onValueChange = NULL
		EXIT
	ENDIF
	
	CALL sInst.onValueChange()
ENDPROC

/// PURPOSE:
///    Triggers the menu items on value next event
/// PARAMS:
///    sInst - the menu item to trigger the on value next event
PROC NODE_MENU_ITEM_TRIGGER_ON_VALUE_NEXT_EVENT(NODE_MENU_ITEM_STRUCT &sInst)
	IF sInst.onValueNext = NULL
		EXIT	
	ENDIF
	
	CALL sInst.onValueNext()
ENDPROC

/// PURPOSE:
///    Triggers the menu items on value previous event
/// PARAMS:
///    sInst - the menu item to trigger the on value previous event
PROC NODE_MENU_ITEM_TRIGGER_ON_VALUE_PREVIOUS_EVENT(NODE_MENU_ITEM_STRUCT &sInst)
	IF sInst.onValuePrevious = NULL
		EXIT
	ENDIF
	
	CALL sInst.onValuePrevious()
ENDPROC

/// PURPOSE:
///    Triggers the load assets event for the menu item
/// PARAMS:
///    sInst - the menu item to load assets for
/// RETURNS:
///    TRUE if the load assets update has finished loading the assets or there is no load assets
///    update event assigned
FUNC BOOL NODE_MENU_ITEM_TRIGGER_ON_LOAD_ASSETS_UPDATE_EVENT(NODE_MENU_ITEM_STRUCT &sInst)
	// No asset loading assigned so return true for finished
	IF sInst.onLoadAssetsUpdate = NULL
		RETURN TRUE
	ENDIF
	
	RETURN CALL sInst.onLoadAssetsUpdate()
ENDFUNC

/// PURPOSE:
///    Adds the defined help keys for the menu item
/// PARAMS:
///    sInst - the menu item to trigger the add help keys on
///    sItemToPass - a menu item you to pass to the on add help key event, typically the current highlighted menu item
///    				 so that we can check if its selectable before adding button help keys
PROC NODE_MENU_ITEM_ADD_HELP_KEYS(NODE_MENU_ITEM_STRUCT &sInst, NODE_MENU_ITEM_STRUCT &sItemToPass)
	T_NODE_MENU_ITEM_CONDITION_GET fpAddHelpKeys = NODE_MENU_ITEM_GET_ADD_HELP_KEYS(sInst)
	
	IF fpAddHelpKeys = NULL
		EXIT
	ENDIF
	
	CALL fpAddHelpKeys(sItemToPass)
ENDPROC

/// PURPOSE:
///    Adds the defined submenu help keys, that are to be added when the menu item becomes a parent submenu
/// PARAMS:
///    sInst - the menu item to trigger the add submenu help keys on
///    sItemToPass - a menu item you to pass to the on add help key event, typically the current highlighted menu item
///    				 so that we can check if its selectable before adding button help keys
PROC NODE_MENU_ITEM_ADD_SUBMENU_HELP_KEYS(NODE_MENU_ITEM_STRUCT &sInst, NODE_MENU_ITEM_STRUCT &sItemToPass)
	T_NODE_MENU_ITEM_CONDITION_GET fpAddSubmenuHelpKeys = NODE_MENU_ITEM_GET_ADD_PARENT_HELP_KEYS(sInst)
	
	IF fpAddSubmenuHelpKeys = NULL
		EXIT
	ENDIF
	
	CALL fpAddSubmenuHelpKeys(sItemToPass)
ENDPROC

/// PURPOSE:
///    Updates the custom input for the menu item if it has been defined
/// PARAMS:
///    sMenuItem - the menu item
///    sNodeMenu - the node menu instance (needs to be passed to input processing)
/// RETURNS:
///    TRUE if the menu needs to refresh
FUNC BOOL NODE_MENU_ITEM_UPDATE_CUSTOM_INPUT(NODE_MENU_ITEM_STRUCT &sMenuItem, NODE_MENU_STRUCT &sNodeMenu)
	T_NODE_MENU_ITEM_INPUT_UPDATE fpInputUpdate = NODE_MENU_ITEM_GET_INPUT_UPDATE(sMenuItem)
	
	// Not asigned so no update needed
	IF fpInputUpdate = NULL
		RETURN FALSE
	ENDIF
	
	RETURN CALL fpInputUpdate(sNodeMenu, sMenuItem)
ENDFUNC

/// PURPOSE:
///    Updates the custom parent input for the menu item if it has been defined
/// PARAMS:
///    sMenuItem - the menu item
///    sNodeMenu - the node menu instance (needs to be passed to input processing)
/// RETURNS:
///    TRUE if the menu needs to refresh
FUNC BOOL NODE_MENU_ITEM_UPDATE_SUBMENU_CUSTOM_INPUT(NODE_MENU_ITEM_STRUCT &sMenuItem, NODE_MENU_STRUCT &sNodeMenu)
	T_NODE_MENU_ITEM_INPUT_UPDATE fpParentInputUpdate = NODE_MENU_ITEM_GET_PARENT_INPUT_UPDATE(sMenuItem)
	
	// Not asigned so no update needed
	IF fpParentInputUpdate = NULL
		RETURN FALSE
	ENDIF
	
	RETURN CALL fpParentInputUpdate(sNodeMenu, sMenuItem)
ENDFUNC

/// PURPOSE:
///    Checks if the menu item requires keyboard text input when pressed
/// PARAMS:
///    sMenuItem - the menu item instance
/// RETURNS:
///    TRUE if the menu item has a text input confirmation
FUNC BOOL NODE_MENU_ITEM_DOES_REQUIRE_TEXT_INPUT_CONFIRMATION(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN NODE_MENU_ITEM_GET_TEXT_INPUT_RESULT_EVENT(sMenuItem) != NULL
ENDFUNC

PROC NODE_MENU_ITEM_TRIGGER_TEXT_INPUT_RESULT_PROCESS(NODE_MENU_ITEM_STRUCT &sMenuItem, OSK_STATUS eResult, STRING sInputText)
	T_NODE_MENU_ITEM_PROCESS_INPUT_RESULT fpTextInputProcess = NODE_MENU_ITEM_GET_TEXT_INPUT_RESULT_EVENT(sMenuItem)
	
	IF fpTextInputProcess = NULL
		EXIT
	ENDIF
	
	CALL fpTextInputProcess(eResult, sInputText)
ENDPROC
