//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        	node_menu_struct.sch																	
/// Description: 	Struct of the data used for running a node menu instance			
///	
/// Written by:  Online Technical Team: Tom Turner,															
/// Date created:  		27/08/2019
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "Core\node_menu_item.sch"
USING "Core\node_menu_typedefs.sch"

/// PURPOSE: 
///    Data for a node menu instance
STRUCT NODE_MENU_STRUCT
	INT iBS = 0
	INT iCurrentSelection = 0
	INT iCurrentMaxChildIndex = 0
	
	OSK_STATUS eKeyboardStatus 
	INT	iProfanityToken
	INT iKeyboardState
	
	NODE_MENU_ITEM_STRUCT sCurrentParentItem
	NODE_MENU_ITEM_STRUCT sRootItem
	NODE_MENU_ITEM_STRUCT sCurrentHighlightedItem
	
	T_NODE_MENU_ITEM_RETURN fpItemPrototype
	T_NODE_MENU_EVENT_CONDITION fpLoadAssetsUpdate
	T_NODE_MENU_EVENT_CONDITION_SELF fpBuild
	T_NODE_MENU_EVENT_STRING fpSetDescription
	T_NODE_MENU_EVENT_INT fpSetHighlighted
	T_NODE_MENU_EVENT fpCleanupAssets
	T_NODE_MENU_EVENT fpDrawHeader
	T_NODE_MENU_EVENT fpDrawMenu
	T_NODE_MENU_EVENT fpOnReset
ENDSTRUCT

///----------------------------
///    SETTERS
///----------------------------    

/// PURPOSE:
///    Sets the load assets behaviour, use this to load in assets that the menu needs.
///    You are responsible for loading the base assets used by the menu you are using!
/// PARAMS:
///    sInst - the node menu instance
///    fpLoadAssetsUpdate - the load assets behaviour to set, should return TRUE when assets have loaded
PROC NODE_MENU_SET_LOAD_ASSETS(NODE_MENU_STRUCT &sInst, T_NODE_MENU_EVENT_CONDITION fpLoadAssetsUpdate)
	sInst.fpLoadAssetsUpdate = fpLoadAssetsUpdate
ENDPROC

/// PURPOSE:
///    Sets the clean up assets behaviour, use this to clean up any assets loaded by the menu
/// PARAMS:
///    sInst - the node menu instance
///    fpCleanupAssets - the clean up assets behaviour to set
PROC NODE_MENU_SET_CLEAN_UP_ASSETS(NODE_MENU_STRUCT &sInst, T_NODE_MENU_EVENT fpCleanupAssets)
	sInst.fpCleanupAssets = fpCleanupAssets
ENDPROC

/// PURPOSE:
///    Sets the header draw behaviour, use this to change how the header at the top of the menu is drawn
/// PARAMS:
///    sInst - the node menu instance
///    fpDrawHeader - the header draw behaviour to set
PROC NODE_MENU_SET_HEADER_DRAW(NODE_MENU_STRUCT &sInst, T_NODE_MENU_EVENT fpDrawHeader)
	sInst.fpDrawHeader = fpDrawHeader
ENDPROC

/// PURPOSE:
///    Sets the on fpOnReset behaviour, use this to trigger anything that needs to happen when the menu fpOnResets
///    useful for fpOnReseting custom menu items to their starting state
/// PARAMS:
///    sInst - the node menu instance
///    fpOnReset - the fpOnReset behaviour to set
PROC NODE_MENU_SET_ON_RESET(NODE_MENU_STRUCT &sInst, T_NODE_MENU_EVENT fpOnReset)
	sInst.fpOnReset = fpOnReset
ENDPROC

/// PURPOSE:
///    Sets the draw method for the menu, this allows you to swap between using DRAW_MENU
///    or drawing scaleform. You could use both if you want to get fancy.
/// PARAMS:
///    sInst - the node menu instance
///    fpDraw - the draw method
PROC NODE_MENU_SET_DRAW_METHOD(NODE_MENU_STRUCT &sInst, T_NODE_MENU_EVENT fpDrawMenu)
	sInst.fpDrawMenu = fpDrawMenu
ENDPROC

/// PURPOSE:
///    Sets the build method for the menu?
/// PARAMS:
///    sInst - the node menu instance
///    fpBuild - the build method
PROC NODE_MENU_SET_BUILD_METHOD(NODE_MENU_STRUCT &sInst, T_NODE_MENU_EVENT_CONDITION_SELF fpBuild)
	sInst.fpBuild = fpBuild
ENDPROC

/// PURPOSE:
///    Sets the method responsible for setting the highlights. This will vary based on if
///    you are using a sacleform menu or the default menu system.
/// PARAMS:
///    sInst - the node menu instance
///    fpSetHighlighted - the highlight method
PROC NODE_MENU_SET_HIGHLIGHT_METHOD(NODE_MENU_STRUCT &sInst, T_NODE_MENU_EVENT_INT fpSetHighlighted)
	sInst.fpSetHighlighted = fpSetHighlighted
ENDPROC

/// PURPOSE:
///    Sets the method used for displaying the description, 
///    note this will also be used to display the confirmation message
/// PARAMS:
///    sInst - the node menu instance
///    fpSetDescription - the description method
PROC NODE_MENU_SET_DESCRIPTION_METHOD(NODE_MENU_STRUCT &sInst, T_NODE_MENU_EVENT_STRING fpSetDescription)
	sInst.fpSetDescription = fpSetDescription
ENDPROC

/// PURPOSE:
///    Sets the protoype to be used for initializing nodes in the menu
/// PARAMS:
///    sInst - the node menu instance
///    fpItemPrototype - the description method
PROC NODE_MENU_SET_MENU_ITEM_PROTOTYPE(NODE_MENU_STRUCT &sInst, T_NODE_MENU_ITEM_RETURN fpItemPrototype)
	sInst.fpItemPrototype = fpItemPrototype
ENDPROC

/// PURPOSE:
///    Sets the current highlighted menu item to the given index according 
///    to the preconfigured highlight method
/// PARAMS:
///    sInst - the node menu instance
///    iHighlightedIndex - the item index to highlight
PROC NODE_MENU_UI_SET_HIGHLIGHTED(NODE_MENU_STRUCT &sInst, INT iHighlightedIndex)
	IF sInst.fpSetHighlighted = NULL
		PRINTLN("[NODE_MENU] SET_HIGHLIGHTED - No highlight method has been assigned to the menu")
		EXIT
	ENDIF
	
	PRINTLN("[NODE_MENU] SET_HIGHLIGHTED - Set highlight to ", iHighlightedIndex, " successfully")
	CALL sInst.fpSetHighlighted(iHighlightedIndex)
ENDPROC

/// PURPOSE:
///    Sets the current description to the given string according to the preconfigured set description method
/// PARAMS:
///    sInst - the node menu instance
///    strDescription - the description to set
PROC NODE_MENU_SET_DESCRIPTION(NODE_MENU_STRUCT &sInst, STRING strDescription)
	IF sInst.fpSetDescription = NULL
		PRINTLN("[NODE_MENU] SET_DESCRIPTION - No description method has been assigned to the menu")
		EXIT
	ENDIF
	
	PRINTLN("[NODE_MENU] SET_HIGHLIGHTED - Set description to ", strDescription, " successfully")
	CALL sInst.fpSetDescription(strDescription)
ENDPROC

PROC _NODE_MENU_SET_CURRENT_MAX_CHILD_INDEX(NODE_MENU_STRUCT &sInst, INT iCurrentMaxChildIndex)
	sInst.iCurrentMaxChildIndex = iCurrentMaxChildIndex
ENDPROC

PROC _NODE_MENU_INCREMENT_CURRENT_MAX_CHILD_INDEX(NODE_MENU_STRUCT &sInst, INT iIncrement)
	sInst.iCurrentMaxChildIndex += iIncrement
ENDPROC

///----------------------------
///    GETTERS
///----------------------------    

/// PURPOSE:
///    Gets the load assets behaviour
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    The current load assets behaviour
FUNC T_NODE_MENU_EVENT_CONDITION _NODE_MENU_GET_LOAD_ASSETS(NODE_MENU_STRUCT &sInst)
	RETURN sInst.fpLoadAssetsUpdate
ENDFUNC

/// PURPOSE:
///    Gets the clean up assets behaviour
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    The clean up assets behaviour
FUNC T_NODE_MENU_EVENT _NODE_MENU_GET_CLEAN_UP_ASSETS(NODE_MENU_STRUCT &sInst)
	RETURN sInst.fpCleanupAssets
ENDFUNC

/// PURPOSE:
///   Gets the header draw behaviour
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    The header draw behaviour
FUNC T_NODE_MENU_EVENT _NODE_MENU_GET_HEADER_DRAW(NODE_MENU_STRUCT &sInst)
	RETURN sInst.fpDrawHeader
ENDFUNC

/// PURPOSE:
///    Gets the on fpOnReset behaviour
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    The on fpOnReset behaviour
FUNC T_NODE_MENU_EVENT _NODE_MENU_GET_ON_RESET(NODE_MENU_STRUCT &sInst)
	RETURN sInst.fpOnReset
ENDFUNC

/// PURPOSE:
///    Gets the draw method for the menu
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    The draw menu method
FUNC T_NODE_MENU_EVENT _NODE_MENU_GET_DRAW_METHOD(NODE_MENU_STRUCT &sInst)
	RETURN sInst.fpDrawMenu
ENDFUNC

/// PURPOSE:
///    Gets the build menu method for the menu
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    The build menu method
FUNC T_NODE_MENU_EVENT_CONDITION_SELF _NODE_MENU_GET_BUILD_METHOD(NODE_MENU_STRUCT &sInst)
	RETURN sInst.fpBuild
ENDFUNC

/// PURPOSE:
///    Gets the parent node of the current sub menu
/// PARAMS:
///    sInst - the node menu instance
///    sParentItem - the parent info to fill
/// RETURNS:
///    TRUE if a parent was successfully retrieved
FUNC BOOL NODE_MENU_GET_CURRENT_MENUS_PARENT_MENU_ITEM(NODE_MENU_STRUCT &sInst, NODE_MENU_ITEM_STRUCT &sParentItem)
	RETURN NODE_MENU_ITEM_GET_PARENT(sInst.sCurrentParentItem, sParentItem) 
ENDFUNC

/// PURPOSE:
///    Checks if the current node is currently hidden
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    TRUE if the current node is hidden
FUNC BOOL NODE_MENU_IS_CURRENT_NODE_HIDDEN(NODE_MENU_STRUCT &sInst)
	RETURN NODE_MENU_ITEM_IS_NODE_HIDDEN(sInst.sCurrentHighlightedItem)
ENDFUNC

/// PURPOSE:
///    Checks if the current node is currently selectable
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    TRUE if the current node is selectable
FUNC BOOL NODE_MENU_IS_CURRENT_NODE_SELECTABLE(NODE_MENU_STRUCT &sInst)
	RETURN NODE_MENU_ITEM_IS_NODE_SELECTABLE(sInst.sCurrentHighlightedItem)
ENDFUNC

/// PURPOSE:
///    Gets the node menu item prototype assigned to this menu
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    The configured prototype menu item for this menu or a blank one if it has'nt been assigned
FUNC NODE_MENU_ITEM_STRUCT NODE_MENU_GET_MENU_ITEM_PROTOTYPE(NODE_MENU_STRUCT &sInst)
	IF sInst.fpItemPrototype != NULL
		RETURN CALL sInst.fpItemPrototype()
	ENDIF
	
	// No protoype assigned so just use a default node
	NODE_MENU_ITEM_STRUCT sBlankNode
	RETURN sBlankNode
ENDFUNC

/// PURPOSE:
///    Gets the current highlighted item index
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///     The current highlighted item index
FUNC INT NODE_MENU_GET_CURRENT_HIGHLIGHTED_ITEM_INDEX(NODE_MENU_STRUCT &sInst)
	RETURN sInst.iCurrentSelection
ENDFUNC

FUNC INT NODE_MENU_GET_CURRENT_MAX_CHILD_INDEX(NODE_MENU_STRUCT &sInst)
	RETURN sInst.iCurrentMaxChildIndex
ENDFUNC
