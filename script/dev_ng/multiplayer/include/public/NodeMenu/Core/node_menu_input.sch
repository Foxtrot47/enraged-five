//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        	node_input.sch																	
/// Description: 	Header for updating the input control of a node menu				
///	
/// Written by:  Online Technical Team: Tom Turner,															
/// Date created:  		27/08/2019
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "Core\node_menu_struct.sch"
USING "Core\node_menu_helper.sch"
USING "Core\node_menu_flags.sch"
USING "Core\node_menu_item_flags.sch"
USING "fmmc_keyboard.sch"

/// PURPOSE:
///    Checks if the given selection index is valid according to the 
///    sub menu currently being displayed in the given node menu
/// PARAMS:
///    sInst - the node menu instance
///    iSelectionIndex - the selection index to check
/// RETURNS:
///    TRUE if valid
FUNC BOOL NODE_MENU_IS_CURRENT_SELECTION_INDEX_VALID(NODE_MENU_STRUCT &sInst, INT iSelectionIndex)
	INT iIndexMin = 0
	INT iIndexMax = NODE_MENU_ITEM_GET_MAX_CHILD_INDEX(sInst.sCurrentParentItem)
	
	// For mouse cursor selection can go negative to denote
	// no selection
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		iIndexMin = -1
	ENDIF
	
	IF iSelectionIndex < iIndexMin OR iSelectionIndex > iIndexMax
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_IS_CURRENT_SELECTION_INDEX_VALID -",
		" Index invalid - index: ", iSelectionIndex,	 
		" current child max index: ",  iIndexMax, 
		" menu item: ",	Get_String_From_TextLabel(NODE_MENU_ITEM_GET_DISPLAY_NAME(sInst.sCurrentParentItem)))
		DEBUG_PRINTCALLSTACK()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC _NODE_MENU_ADD_MENU_ITEMS_DESCRIPTION(NODE_MENU_STRUCT &sInst, NODE_MENU_ITEM_STRUCT &sMenuItem)
	IF IS_BIT_SET_ENUM(sInst.iBS, NODE_MENU_BS_AWAITING_CONFIRMATION)
		NODE_MENU_SET_DESCRIPTION(sInst, NODE_MENU_ITEM_GET_CONFIRMATION(sMenuItem))
		EXIT
	ENDIF
	
	T_NODE_MENU_ITEM_ADD_EVENT fpAddDescription = NODE_MENU_ITEM_GET_CUSTOM_DESCRIPTION(sMenuItem)
	
	IF fpAddDescription != NULL
		CALL fpAddDescription(sMenuItem, sInst.iCurrentSelection)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] _NODE_MENU_ADD_MENU_ITEMS_DESCRIPTION - adding custom description")
		EXIT
	ENDIF
		
	STRING strDesc = NODE_MENU_ITEM_GET_DESCRIPTION(sMenuItem)
	IF IS_STRING_NULL_OR_EMPTY(strDesc)
		EXIT
	ENDIF
	
	NODE_MENU_SET_DESCRIPTION(sInst, strDesc)
ENDPROC

/// PURPOSE:
///    Gets the currently selected node in the node menu for the default menu system (not scaleform)
/// PARAMS:
///    sInst - the node menu instance
///    sCurrentItem - the current node info to fill
/// RETURNS:
///    TRUE is the currently selected item was retrieved successfully
FUNC BOOL NODE_MENU_SET_HIGHLIGHTED_MENU_ITEM(NODE_MENU_STRUCT &sInst, INT iSelection)
	IF NOT NODE_MENU_IS_CURRENT_SELECTION_INDEX_VALID(sInst, iSelection)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_SET_HIGHLIGHTED_MENU_ITEM - invalid selection")
		RETURN FALSE
	ENDIF	

	sInst.iCurrentSelection = iSelection
	
	// Clear current highlighted child
	NODE_MENU_ITEM_STRUCT sEmpty = NODE_MENU_GET_MENU_ITEM_PROTOTYPE(sInst)
	COPY_SCRIPT_STRUCT(sInst.sCurrentHighlightedItem, sEmpty, SIZE_OF(NODE_MENU_ITEM_STRUCT))
	
	IF NOT NODE_MENU_ITEM_GET_CHILD(sInst.sCurrentParentItem, 
	iSelection, sInst.sCurrentHighlightedItem)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] SET_HIGHLIGHTED_MENU_ITEM - No child to highlight")
		RETURN FALSE
	ENDIF		
	
	IF NOT NODE_MENU_ITEM_IS_NODE_HIDDEN(sInst.sCurrentHighlightedItem) 
	AND NODE_MENU_ITEM_IS_HIGHLIGHTABLE(sInst.sCurrentHighlightedItem)
		NODE_MENU_UI_SET_HIGHLIGHTED(sInst, sInst.iCurrentSelection)
		NODE_MENU_ITEM_TRIGGER_ON_HIGHLIGHT_EVENT(sInst.sCurrentHighlightedItem)
	ELSE
		// don't highlight if its invisible or unhighlightable
		NODE_MENU_UI_SET_HIGHLIGHTED(sInst, -1)
	ENDIF
	
	_NODE_MENU_ADD_MENU_ITEMS_DESCRIPTION(sInst, sInst.sCurrentHighlightedItem)
	
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_GET_CURRENT_SELECTED_MENU_ITEM - ",
	"Sucessfully got current selected child at index: ", iSelection,
	" from menu ", Get_String_From_TextLabel(NODE_MENU_ITEM_GET_DISPLAY_NAME(sInst.sCurrentParentItem)))
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Disables the weapon wheel selection controls for when ta node menu is in use
PROC _NODE_MENU_INPUT_DISABLE_WEAPON_SELECTION()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL) 
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)    
ENDPROC

/// PURPOSE:
///    Disables various combat controls for when a node menu is in use
PROC _NODE_MENU_INPUT_DISABLE_COMBAT_CONTROLS()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)   
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DIVE)  
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
ENDPROC

/// PURPOSE:
///    Disables various vehicle controls for when a node menu is in use 
PROC _NODE_MENU_INPUT_DISABLE_VEHICLE_CONTROLS()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PUSHBIKE_SPRINT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PUSHBIKE_PEDAL)
ENDPROC

/// PURPOSE:
///    Disables character wheel controls for when a node menu is in use 
PROC _NODE_MENU_INPUT_DISABLE_CHARACTER_WHEEL()
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_FRANKLIN)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_MICHAEL)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_TREVOR)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_MULTIPLAYER)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)
ENDPROC

/// PURPOSE:
///    Maintains the required control set up for when a node menu is in use
PROC _NODE_MENU_INPUT_MAINTAIN_CONTROL_SCHEME()
   	_NODE_MENU_INPUT_DISABLE_WEAPON_SELECTION()
    _NODE_MENU_INPUT_DISABLE_COMBAT_CONTROLS()
	_NODE_MENU_INPUT_DISABLE_VEHICLE_CONTROLS()
    _NODE_MENU_INPUT_DISABLE_CHARACTER_WHEEL()
ENDPROC

/// PURPOSE:
///    Gets the next valid highlightable and selectable item
///    if the current highlight item passed in is valid it will return that
/// PARAMS:
///    sInst - the node menu instance
///    sHighlightedItem - the current highlighted item to validate
///    iIncrement - the direction to try getting the next valid
PROC _NODE_MENU_INPUT_GET_NEXT_CHILD(NODE_MENU_STRUCT &sInst, INT iIncrement)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] _NODE_MENU_INPUT_GET_NEXT_ITEM - sInst.iCurrentSelection: ", sInst.iCurrentSelection)
	INT i
	INT iNewSelection = sInst.iCurrentSelection	
	INT iChildCount = NODE_MENU_ITEM_GET_MAX_CHILD_COUNT(sInst.sCurrentParentItem)
	NODE_MENU_ITEM_STRUCT sNewItem
	
	REPEAT iChildCount i
		sNewItem = NODE_MENU_GET_MENU_ITEM_PROTOTYPE(sInst)
		
		iNewSelection += iIncrement
		iNewSelection = IWRAP_INDEX(iNewSelection, iChildCount)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] _NODE_MENU_INPUT_GET_NEXT_ITEM - iNewSelection: ", iNewSelection)
		
		IF NOT NODE_MENU_ITEM_GET_CHILD(sInst.sCurrentParentItem, iNewSelection, sNewItem)
			CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] _NODE_MENU_INPUT_GET_NEXT_ITEM - failed to get: ", iNewSelection)
			EXIT
		ENDIF
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] _NODE_MENU_INPUT_GET_NEXT_ITEM - sNewItem: ", sNewItem.tlDisplayName)
		
		IF NOT NODE_MENU_ITEM_IS_NODE_HIDDEN(sNewItem) 
		AND NODE_MENU_ITEM_IS_HIGHLIGHTABLE(sNewItem)
			CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] _NODE_MENU_INPUT_GET_NEXT_ITEM - Break Loop, found valid: ", sNewItem.tlDisplayName)
			BREAKLOOP
		ENDIF		
	ENDREPEAT
	
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] _NODE_MENU_INPUT_GET_NEXT_ITEM - Setting item to ", iNewSelection, " ", sNewItem.tlDisplayName)
	NODE_MENU_SET_HIGHLIGHTED_MENU_ITEM(sInst, iNewSelection)
ENDPROC

/// PURPOSE:
///    Validates the current highlighted item, if not found to be valid it will get the next valid one
/// PARAMS:
///    sInst - the node menu instance
PROC _NODE_MENU_INPUT_VALIDATE_CURRENT_HIGHLIGHTED_ITEM(NODE_MENU_STRUCT &sInst)
	IF NODE_MENU_ITEM_IS_NODE_HIDDEN(sInst.sCurrentHighlightedItem) 
	OR NOT NODE_MENU_ITEM_IS_HIGHLIGHTABLE(sInst.sCurrentHighlightedItem)
		_NODE_MENU_INPUT_GET_NEXT_CHILD(sInst, 1)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the current node as the current parent in the menu, this will show its children it does NOT select the node
/// PARAMS:
///    sInst - the node menu instance
///    sCurrentNode - the node to set as current parent
PROC _NODE_MENU_SET_CURRENT_PARENT_NODE(NODE_MENU_STRUCT &sInst, NODE_MENU_ITEM_STRUCT& sCurrentNode, BOOL bHighlightChild = TRUE, BOOL bTriggerOnBecomeParentEvent = TRUE)
	COPY_SCRIPT_STRUCT(sInst.sCurrentParentItem, sCurrentNode, SIZE_OF(NODE_MENU_ITEM_STRUCT))
	
	IF bTriggerOnBecomeParentEvent
		NODE_MENU_ITEM_TRIGGER_ON_BECOME_PARENT_EVENT(sInst.sCurrentParentItem)
	ENDIF
	
	IF bHighlightChild
		INT iDefaultHighlight = NODE_MENU_ITEM_GET_DEFAULT_HIGHLIGHTED_CHILD_INDEX(sInst.sCurrentParentItem)
		NODE_MENU_SET_HIGHLIGHTED_MENU_ITEM(sInst, iDefaultHighlight)
		_NODE_MENU_INPUT_VALIDATE_CURRENT_HIGHLIGHTED_ITEM(sInst)
	ENDIF
	
	NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_REBUILD_MENU)
ENDPROC

/// PURPOSE:
///    Resets the menu back to its starting node
/// PARAMS:
///    sInst - the node menu instance
PROC _NODE_MENU_RESET_TO_START_NODE(NODE_MENU_STRUCT &sInst)
	_NODE_MENU_SET_CURRENT_PARENT_NODE(sInst, sInst.sRootItem)
ENDPROC

/// PURPOSE:
///    Triggers the reset event on the menu that users can hook into
/// PARAMS:
///    sInst - the node menu instance
PROC _NODE_MENU_INPUT_TRIGGER_RESET_EVENT(NODE_MENU_STRUCT &sInst)
	IF sInst.fpOnReset = NULL
		EXIT
	ENDIF
	
	CALL sInst.fpOnReset()
ENDPROC

/// PURPOSE:
///    Cleans up all the variables associated with running the text input keyboard
/// PARAMS:
///    sInst - the node menu instance
PROC _NODE_MENU_INPUT_CLEANUP_KEYBOARD_INPUT(NODE_MENU_STRUCT &sInst)
	// If keyboard input was pending, send a failed result
	IF NODE_MENU_BS_IS_FLAG_SET(sInst, NODE_MENU_BS_AWAITING_TEXT_INPUT_CONFIRMATION)
	AND NODE_MENU_ITEM_DOES_REQUIRE_TEXT_INPUT_CONFIRMATION(sInst.sCurrentHighlightedItem)
		NODE_MENU_ITEM_TRIGGER_TEXT_INPUT_RESULT_PROCESS(sInst.sCurrentHighlightedItem, OSK_FAILED, "")
	ENDIF
	
	NODE_MENU_BS_CLEAR_FLAG(sInst, NODE_MENU_BS_AWAITING_TEXT_INPUT_CONFIRMATION)
	sInst.eKeyboardStatus = OSK_PENDING
	sInst.iProfanityToken = 0
	sInst.iKeyboardState = 0
ENDPROC

/// PURPOSE:
///    Tells the node menu to fpOnReset to as if it has just been opened
///    this will also clear the menus closed state
/// PARAMS:
///    sInst - the node menu instance
PROC NODE_MENU_RESET(NODE_MENU_STRUCT &sInst, BOOL bClearClosedFlag = TRUE)
	IF bClearClosedFlag
		NODE_MENU_BS_CLEAR_FLAG(sInst, NODE_MENU_BS_CLOSE_MENU)
	ENDIF
	
	_NODE_MENU_INPUT_CLEANUP_KEYBOARD_INPUT(sInst)
	NODE_MENU_BS_CLEAR_FLAG(sInst, NODE_MENU_BS_AWAITING_CONFIRMATION)
	_NODE_MENU_RESET_TO_START_NODE(sInst)
	NODE_MENU_SET_HIGHLIGHTED_MENU_ITEM(sInst, 0)
	_NODE_MENU_INPUT_TRIGGER_RESET_EVENT(sInst)
ENDPROC

/// PURPOSE:
///    Checks if the currently highlighted menu item has finished loading
///    the assets it requires
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    TRUE if all assets have loaded
FUNC BOOL NODE_MENU_INPUT_HAS_CURRENT_HIGHLIGHTED_FINISHED_LOADING_ASSETS(NODE_MENU_STRUCT &sInst)
	RETURN NODE_MENU_ITEM_TRIGGER_ON_LOAD_ASSETS_UPDATE_EVENT(sInst.sCurrentHighlightedItem)
ENDFUNC

/// PURPOSE:
///    Triggers the on press event on the menu item the menu is currently highlighting
/// PARAMS:
///    sInst - the node menu instance
PROC NODE_MENU_TRIGGER_PRESS_EVENT_ON_CURRENT_HIGHLIGHTED_NODE(NODE_MENU_STRUCT &sInst)
	NODE_MENU_ITEM_TRIGGER_ON_PRESS_EVENT(sInst.sCurrentHighlightedItem)
ENDPROC

/// PURPOSE:
///    Gets the current parents index
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    The current parents index
FUNC INT NODE_MENU_GET_CURRENT_PARENTS_INDEX(NODE_MENU_STRUCT &sInst)
	RETURN NODE_MENU_ITEM_GET_ID(sInst.sCurrentParentItem)
ENDFUNC

/// PURPOSE:
///    Processes a pending menu select confirmation
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    TRUE if confirmation has finished or there is no confirmation pending
///    FALSE if a confirmation is pending
FUNC BOOL _NODE_MENU_PROCESS_WAIT_FOR_CONFIRMATION(NODE_MENU_STRUCT &sInst)
	// Wait for confirmation
	IF NOT NODE_MENU_BS_IS_FLAG_SET(sInst, NODE_MENU_BS_AWAITING_CONFIRMATION)
	AND NOT IS_STRING_NULL_OR_EMPTY(NODE_MENU_ITEM_GET_CONFIRMATION(sInst.sCurrentHighlightedItem))
		NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_AWAITING_CONFIRMATION)
		NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_REBUILD_MENU)
		RETURN FALSE
	ENDIF
	
	// flag is set so a confirmation must have been completed
	NODE_MENU_BS_CLEAR_FLAG(sInst, NODE_MENU_BS_AWAITING_CONFIRMATION)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    COnsumes the wait for text input confirmation, if text input is active it clears it
///    if it has not yet started and is required it starts it
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    TRUE if running text input confirmation
FUNC BOOL _NODE_MENU_PROCESS_WAIT_FOR_TEXT_INPUT_CONFIRMATION(NODE_MENU_STRUCT &sInst)
	IF NOT NODE_MENU_BS_IS_FLAG_SET(sInst, NODE_MENU_BS_AWAITING_TEXT_INPUT_CONFIRMATION)
	AND NODE_MENU_ITEM_DOES_REQUIRE_TEXT_INPUT_CONFIRMATION(sInst.sCurrentHighlightedItem)
		NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_AWAITING_TEXT_INPUT_CONFIRMATION)
		RETURN FALSE
	ENDIF
	
	NODE_MENU_BS_CLEAR_FLAG(sInst, NODE_MENU_BS_AWAITING_TEXT_INPUT_CONFIRMATION)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Handles selection of a menu item ensuring its events are called
///    and entering it if it is a submenu (has child menu items)
/// PARAMS:
///    sInst - the node menu instance
FUNC BOOL NODE_MENU_ENTER_HIGHLIGHTED_NODE(NODE_MENU_STRUCT &sInst, BOOL bTriggerPressEvent = TRUE, BOOL bTriggerOnBecomeParentEvent = TRUE)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_ENTER_HIGHLIGHTED_NODE - Called")
	DEBUG_PRINTCALLSTACK()
	
	IF NOT NODE_MENU_INPUT_HAS_CURRENT_HIGHLIGHTED_FINISHED_LOADING_ASSETS(sInst)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_ENTER_HIGHLIGHTED_NODE - Still running load assets update")
		RETURN FALSE
	ENDIF
	
	IF NOT NODE_MENU_ITEM_IS_NODE_SELECTABLE(sInst.sCurrentHighlightedItem)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_ENTER_HIGHLIGHTED_NODE - Item is disabled")
		RETURN FALSE
	ENDIF	
	
	// Refresh the current item so any confirmation setting is up to date
	NODE_MENU_SET_HIGHLIGHTED_MENU_ITEM(sInst, sInst.iCurrentSelection)
	
	// We may need to wait for confirmation to enter this node
	IF NOT _NODE_MENU_PROCESS_WAIT_FOR_CONFIRMATION(sInst)
		RETURN TRUE
	ENDIF
	
	// We may need to wait for keyboard text input
	IF NOT _NODE_MENU_PROCESS_WAIT_FOR_TEXT_INPUT_CONFIRMATION(sInst)
		RETURN TRUE
	ENDIF
	
	IF bTriggerPressEvent
		NODE_MENU_TRIGGER_PRESS_EVENT_ON_CURRENT_HIGHLIGHTED_NODE(sInst)
	ENDIF
	
	IF IS_BIT_SET_ENUM(sInst.sCurrentHighlightedItem.iFlags, NODE_MENU_ITEM_FLAGS_CLOSE_MENU_ON_SELECT)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_ENTER_HIGHLIGHTED_NODE - Closing on select")
		NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_CLOSE_MENU)
	ENDIF
	
	// Leave if this child isn't a sub menu
	IF NODE_MENU_ITEM_GET_CHILDREN_CONNECTION(sInst.sCurrentHighlightedItem) = NULL
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_ENTER_HIGHLIGHTED_NODE - Current highlighted is not a sub menu")
		RETURN FALSE
	ENDIF
	
	// If it is a sub menu copy it into the current parent menu
	_NODE_MENU_SET_CURRENT_PARENT_NODE(sInst, sInst.sCurrentHighlightedItem, DEFAULT, bTriggerOnBecomeParentEvent)
	
	
	
	// If the node we now highlight is a tab we should automatically enter it to show its children
	IF IS_BIT_SET_ENUM(sInst.sCurrentHighlightedItem.iFlags, NODE_MENU_ITEM_FLAGS_IS_TAB)
		// WARNING: if too many tabs are one after the other, this recursion could blow the call stack
		// TODO: Add safety count to limit how many tabs can be one after the other
		NODE_MENU_ENTER_HIGHLIGHTED_NODE(sInst)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_ENTER_HIGHLIGHTED_NODE - Is tab, entering to show its children")
	ENDIF
	
	// Only rebuild of theres a sub menu
	NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_REBUILD_MENU)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_ENTER_HIGHLIGHTED_NODE - Finished")
	RETURN TRUE
ENDFUNC

PROC NODE_MENU_JUMP_TO_SUBMENU(NODE_MENU_STRUCT &sInst, NODE_MENU_ITEM_STRUCT &sNewParent, INT iHighlightItem = 0, BOOL bTriggerBecomeParentEvent = TRUE)
	_NODE_MENU_SET_CURRENT_PARENT_NODE(sInst, sNewParent, FALSE, bTriggerBecomeParentEvent)
	NODE_MENU_SET_HIGHLIGHTED_MENU_ITEM(sInst, iHighlightItem)
	
	IF NODE_MENU_ITEM_GET_CHILDREN_CONNECTION(sNewParent) = NULL
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_JUMP_TO_SUBMENU - Current highlighted is not a sub menu")
		EXIT
	ENDIF
	
	// If the node we now highlight is a tab we should automatically enter it to show its children
	IF IS_BIT_SET_ENUM(sInst.sCurrentHighlightedItem.iFlags, NODE_MENU_ITEM_FLAGS_IS_TAB)
		NODE_MENU_ENTER_HIGHLIGHTED_NODE(sInst, FALSE)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_JUMP_TO_SUBMENU - Is tab, entering to show its children")
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the child count of the current parent i.e. how many items to display
/// PARAMS:
///    sInst - the node menu instance 
/// RETURNS:
///    The child count of the current parent
FUNC INT NODE_MENU_GET_CURRENT_PARENTS_CHILD_COUNT(NODE_MENU_STRUCT &sInst)
	RETURN NODE_MENU_ITEM_GET_MAX_CHILD_COUNT(sInst.sCurrentParentItem)
ENDFUNC

/// PURPOSE:
///    Increments the current selected item of the menu items currently being shown
/// PARAMS:
///    sInst - the node menu instance
///    iIncrement - increment direction of the selction movement
FUNC BOOL NODE_MENU_INCREMENT_CURRENT_SELECTION(NODE_MENU_STRUCT &sInst, INT iIncrement)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_INCREMENT_CURRENT_SELECTION - Called")
	
	INT iParentsChildCount = NODE_MENU_GET_CURRENT_PARENTS_CHILD_COUNT(sInst)
	
	// Don't bother incrementing, this parent item only has one child to select
	IF iParentsChildCount <= 1
		RETURN FALSE
	ENDIF
	
	IF NODE_MENU_ITEM_TRIGGER_ON_UNHIGHLIGHT_EVENT(sInst.sCurrentHighlightedItem)
		NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_REBUILD_MENU)
	ENDIF
	
	_NODE_MENU_INPUT_GET_NEXT_CHILD(sInst, iIncrement)
	
	// Invoke on highlight event for the item
	IF NODE_MENU_ITEM_TRIGGER_ON_HIGHLIGHT_EVENT(sInst.sCurrentHighlightedItem)
		NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_REBUILD_MENU)
	ENDIF
	
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_INCREMENT_CURRENT_SELECTION - Called")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Handles backing out of a menu item
///    if at the root menu it will close the entire menu
///    WARNING: this calls the On Back event for menu items, you should never call
///    this from a menu items On Back event unless you pass bTriggerOnBackEvent as FALSE
///    you will get stuck in a recursive loop otherwise
/// PARAMS:
///    sInst - the node menu instance
///    bIgnoreTab - Ignore the entering a tab behaviour on pressing back
///    bTriggerOnBackEvent - determines if the on back event for the current menu item should be called
/// RETURNS:
///    TRUE if back has been pressed on the root menu i.e close menu
PROC NODE_MENU_TRIGGER_BACK_OUT_OF_CURRENT_NODE(NODE_MENU_STRUCT &sInst, BOOL bIgnoreTab = FALSE, BOOL bTriggerOnBackEvent = TRUE)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_TRIGGER_BACK_OUT_OF_CURRENT_NODE - Called")
	
	IF bTriggerOnBackEvent
		NODE_MENU_ITEM_TRIGGER_ON_BACK_EVENT(sInst.sCurrentHighlightedItem)
	ENDIF
	
	NODE_MENU_ITEM_STRUCT sCurrentMenusParent = NODE_MENU_GET_MENU_ITEM_PROTOTYPE(sInst)
	
	IF NOT NODE_MENU_GET_CURRENT_MENUS_PARENT_MENU_ITEM(sInst, sCurrentMenusParent)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_TRIGGER_BACK_OUT_OF_CURRENT_NODE - No parent closing menu")
		// If no parent we are at root so exit menu
		NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_CLOSE_MENU)	
		EXIT
	ENDIF
	
	INT iOldParentID = NODE_MENU_ITEM_GET_ID(sInst.sCurrentParentItem)
	BOOL bOldParentIsTab = IS_BIT_SET_ENUM(sInst.sCurrentParentItem.iFlags, NODE_MENU_ITEM_FLAGS_IS_TAB)
	
	// Ignore setting back to previous selected
	IF NODE_MENU_BS_IS_FLAG_SET(sInst, NODE_MENU_BS_ALWAYS_HIGHLIGHT_FIRST_ITEM)
		iOldParentID = 0
	ENDIF
	
	/// Store current parents highlight event and id as it will become the new 
	/// current child
	T_NODE_MENU_ITEM_CONDITION oldOnHighlight = NODE_MENU_ITEM_GET_ON_HIGHLIGHT(sInst.sCurrentParentItem)
	_NODE_MENU_SET_CURRENT_PARENT_NODE(sInst, sCurrentMenusParent, FALSE)
	
	// Try use the id of this child to fake remembering the selection in its parent
	IF NODE_MENU_IS_CURRENT_SELECTION_INDEX_VALID(sInst, iOldParentID)
		IF oldOnHighlight != NULL
			CALL oldOnHighlight()
		ENDIF
		
		NODE_MENU_SET_HIGHLIGHTED_MENU_ITEM(sInst, iOldParentID)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_TRIGGER_BACK_OUT_OF_CURRENT_NODE - Highlighting: ", iOldParentID)
	ELSE
		// Couldn't find old parent as child in new parent so 
		// use first child or user defined highlight
		NODE_MENU_SET_HIGHLIGHTED_MENU_ITEM(sInst, 0)
		NODE_MENU_ITEM_TRIGGER_ON_HIGHLIGHT_EVENT(sInst.sCurrentHighlightedItem)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_TRIGGER_BACK_OUT_OF_CURRENT_NODE - Highlight invalid, highlighting 0")
	ENDIF
	
	// If the old parent is a tab then its parent must have been a tab group so we need to go up one more parent
	IF bOldParentIsTab AND NOT bIgnoreTab
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_TRIGGER_BACK_OUT_OF_CURRENT_NODE - Backing out again because its a tab")
		NODE_MENU_TRIGGER_BACK_OUT_OF_CURRENT_NODE(sInst)
	
	// Else if the new parent holds tabs, enter it
	ELIF IS_BIT_SET_ENUM(sInst.sCurrentHighlightedItem.iFlags, NODE_MENU_ITEM_FLAGS_IS_TAB) AND NOT bIgnoreTab
		NODE_MENU_ENTER_HIGHLIGHTED_NODE(sInst)
		NODE_MENU_SET_HIGHLIGHTED_MENU_ITEM(sInst, iOldParentID)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_TRIGGER_BACK_OUT_OF_CURRENT_NODE - Entering highlighted because its a tab then highlighting: ", iOldParentID)
	ENDIF
	
	NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_REBUILD_MENU)	
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_TRIGGER_BACK_OUT_OF_CURRENT_NODE - Finished")
ENDPROC

FUNC INT NODE_MENU_GET_MAX_TAB_INDEX(NODE_MENU_STRUCT &sInst)
	// Not on a tab
	IF NOT IS_BIT_SET_ENUM(sInst.sCurrentParentItem.iFlags, NODE_MENU_ITEM_FLAGS_IS_TAB)
		RETURN 0
	ENDIF
	
	NODE_MENU_ITEM_STRUCT sTabGroup = NODE_MENU_GET_MENU_ITEM_PROTOTYPE(sInst)
	
	IF NOT NODE_MENU_ITEM_GET_PARENT(sInst.sCurrentParentItem, sTabGroup)
		ASSERTLN("[NODE_MENU] NODE_MENU_GET_CURRENT_TAB_COUNT - FAiled to get tab group parent")
		RETURN 0
	ENDIF
	
	// The max tab index is the max child index the current tabs parent has
	RETURN NODE_MENU_ITEM_GET_MAX_CHILD_INDEX(sTabGroup)
ENDFUNC

FUNC INT NODE_MENU_GET_CURRENT_TAB_COUNT(NODE_MENU_STRUCT &sInst)
	// Not on a tab
	IF NOT IS_BIT_SET_ENUM(sInst.sCurrentParentItem.iFlags, NODE_MENU_ITEM_FLAGS_IS_TAB)
		RETURN 0
	ENDIF
	
	NODE_MENU_ITEM_STRUCT sTabGroup = NODE_MENU_GET_MENU_ITEM_PROTOTYPE(sInst)
	
	IF NOT NODE_MENU_ITEM_GET_PARENT(sInst.sCurrentParentItem, sTabGroup)
		ASSERTLN("[NODE_MENU] NODE_MENU_GET_CURRENT_TAB_COUNT - FAiled to get tab group parent")
		RETURN 0
	ENDIF
	
	// The tab count is how many children the current tabs parents has
	RETURN NODE_MENU_ITEM_GET_MAX_CHILD_COUNT(sTabGroup)
ENDFUNC

FUNC INT NODE_MENU_GET_CURRENT_TAB_INDEX(NODE_MENU_STRUCT &sInst)
	// Not on a tab
	IF NOT IS_BIT_SET_ENUM(sInst.sCurrentParentItem.iFlags, NODE_MENU_ITEM_FLAGS_IS_TAB)
		RETURN 0
	ENDIF
	
	RETURN NODE_MENU_GET_CURRENT_PARENTS_INDEX(sInst)
ENDFUNC

/// PURPOSE:
///    Increments to the next menu tab if the current parent is itself a tab
/// PARAMS:
///    sInst - the node menu instance
///    iIncrement - the direction and step size in which to get the next tab
FUNC BOOL NODE_MENU_INCREMENT_TAB(NODE_MENU_STRUCT &sInst, INT iIncrement)
	IF NOT IS_BIT_SET_ENUM(sInst.sCurrentParentItem.iFlags, NODE_MENU_ITEM_FLAGS_IS_TAB)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_INCREMENT_TAB - Not a tab")
		RETURN FALSE
	ENDIF
	
	BOOL bClampTab = IS_BIT_SET_ENUM(sInst.sCurrentParentItem.iFlags, NODE_MENU_ITEM_FLAGS_CLAMP_TAB)
	
	IF bClampTab
		INT iCurrentTab = NODE_MENU_GET_CURRENT_TAB_INDEX(sInst)
		INT iMaxTab = NODE_MENU_GET_MAX_TAB_INDEX(sInst)
		
		/// If the current selection is about to over shoot the valid range
		/// don't increment or decremtent as we are clamping
		IF (iIncrement < 0 AND iCurrentTab <= 0)
		OR (iIncrement > 0 AND iCurrentTab >= iMaxTab)
			CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_INCREMENT_TAB - Clamping, not incrementing tab")
			RETURN FALSE
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_INCREMENT_TAB - Backing out")
	
	// Jump back to parent
	NODE_MENU_TRIGGER_BACK_OUT_OF_CURRENT_NODE(sInst, TRUE)
	
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_INCREMENT_TAB - Incrementing to next tab")

	// Increment to next child in parent which should be another tab
	NODE_MENU_INCREMENT_CURRENT_SELECTION(sInst, iIncrement)
	
	// Enter that tab
	NODE_MENU_ENTER_HIGHLIGHTED_NODE(sInst)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Increments the current selected item to its next value
///    triggers an event so values can be handled externally
/// PARAMS:
///    sInst - the node menu instance
PROC NODE_MENU_TRIGGER_VALUE_NEXT_ON_HIGHLIGHTED_NODE(NODE_MENU_STRUCT &sInst)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_TRIGGER_VALUE_NEXT_ON_HIGHLIGHTED_NODE - Called")
	
	IF NOT NODE_MENU_INPUT_HAS_CURRENT_HIGHLIGHTED_FINISHED_LOADING_ASSETS(sInst)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_TRIGGER_VALUE_NEXT_ON_HIGHLIGHTED_NODE - Still running highlight update")
		EXIT
	ENDIF
	
	IF NOT NODE_MENU_ITEM_IS_NODE_SELECTABLE(sInst.sCurrentHighlightedItem)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_TRIGGER_VALUE_NEXT_ON_HIGHLIGHTED_NODE - Item is disabled")
		EXIT
	ENDIF
	
	NODE_MENU_ITEM_TRIGGER_ON_VALUE_CHANGE_EVENT(sInst.sCurrentHighlightedItem)
	NODE_MENU_ITEM_TRIGGER_ON_VALUE_NEXT_EVENT(sInst.sCurrentHighlightedItem)
	NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_REBUILD_MENU)
	
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_TRIGGER_VALUE_NEXT_ON_HIGHLIGHTED_NODE - Finished")
ENDPROC

/// PURPOSE:
///    Decrements the current selected item to its previous value
///    triggers an event so values can be handled externally
/// PARAMS:
///    sInst - the node menu instance
PROC NODE_MENU_TRIGGER_VALUE_PREVIOUS_ON_HIGHLIGHTED_NODE(NODE_MENU_STRUCT &sInst)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_TRIGGER_VALUE_PREVIOUS_ON_HIGHLIGHTED_NODE - Called")
	
	IF NOT NODE_MENU_INPUT_HAS_CURRENT_HIGHLIGHTED_FINISHED_LOADING_ASSETS(sInst)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_TRIGGER_VALUE_PREVIOUS_ON_HIGHLIGHTED_NODE - Still running highlight update")
		EXIT
	ENDIF
	
	IF NOT NODE_MENU_ITEM_IS_NODE_SELECTABLE(sInst.sCurrentHighlightedItem)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_TRIGGER_VALUE_PREVIOUS_ON_HIGHLIGHTED_NODE - Item is disabled")
		EXIT
	ENDIF
	
	// Invoke events for value change
	NODE_MENU_ITEM_TRIGGER_ON_VALUE_CHANGE_EVENT(sInst.sCurrentHighlightedItem)
	NODE_MENU_ITEM_TRIGGER_ON_VALUE_PREVIOUS_EVENT(sInst.sCurrentHighlightedItem)
	
	// Redraw this item so its values can be updated
	NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_REBUILD_MENU)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_TRIGGER_VALUE_PREVIOUS_ON_HIGHLIGHTED_NODE - Finished")
ENDPROC

/// PURPOSE:
///    Sets the root node of the menu this is the node that the menu will start at
/// PARAMS:
///    sInst - the node menu instance
///    sRootNode - the node to set as root
PROC NODE_MENU_SET_ROOT_NODE(NODE_MENU_STRUCT &sInst, NODE_MENU_ITEM_STRUCT& sRootNode)
	COPY_SCRIPT_STRUCT(sInst.sRootItem, sRootNode, SIZE_OF(NODE_MENU_ITEM_STRUCT))
ENDPROC

/// PURPOSE:
///    Handles the input processing for a confirmation message
/// PARAMS:
///    sInst - the node menu instance
PROC _NODE_MENU_INPUT_PROCESS_CONFIRMATION(NODE_MENU_STRUCT &sInst)
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
	OR IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) 
	AND IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		NODE_MENU_ENTER_HIGHLIGHTED_NODE(sInst)
		EXIT
	ENDIF
	
	IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CELLPHONE_CANCEL)
		NODE_MENU_BS_CLEAR_FLAG(sInst, NODE_MENU_BS_AWAITING_CONFIRMATION)	
		NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_REBUILD_MENU)	
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates processing a keyboard input confirmation, this will only run if the highlighted menu item
///    has requested text input when it is pressed
/// PARAMS:
///    sInst - the node menu instance
PROC _NODE_MENU_INPUT_PROCESS_KEYBOARD_INPUT(NODE_MENU_STRUCT &sInst)
	IF NOT NODE_MENU_ITEM_DOES_REQUIRE_TEXT_INPUT_CONFIRMATION(sInst.sCurrentHighlightedItem)
		NODE_MENU_BS_CLEAR_FLAG(sInst, NODE_MENU_BS_AWAITING_TEXT_INPUT_CONFIRMATION)
		EXIT
	ENDIF
	
	IF NOT RUN_ON_SCREEN_KEYBOARD(sInst.eKeyboardStatus, sInst.iProfanityToken, sInst.iKeyboardState)
		EXIT
	ENDIF
		
	NODE_MENU_ITEM_TRIGGER_TEXT_INPUT_RESULT_PROCESS(sInst.sCurrentHighlightedItem, sInst.eKeyboardStatus, GET_ONSCREEN_KEYBOARD_RESULT())
	_NODE_MENU_INPUT_CLEANUP_KEYBOARD_INPUT(sInst)// This also clears the awaiting text input flag
	NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_REBUILD_MENU)	
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] INPUT_PROCESS_KEYBOARD_INPUT - Keyboard input finished")
ENDPROC

/// PURPOSE:
///    Updates the defined input for the currently highlighted item
/// PARAMS:
///    sInst - the node menu instance
PROC _NODE_MENU_INPUT_PROCESS_MENU_NAVIGATION(NODE_MENU_STRUCT &sInst)
	// Only update parent submenu input if the current highlighted isn't blocking it
	IF NOT NODE_MENU_ITEM_GET_SHOULD_BLOCK_PARENT_INPUT(sInst.sCurrentHighlightedItem)
		IF NODE_MENU_ITEM_UPDATE_SUBMENU_CUSTOM_INPUT(sInst.sCurrentParentItem, sInst)
			NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_REBUILD_MENU)	
			CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] INPUT_PROCESS_CURRENT_ITEMS_CUSTOM_INPUT - Submenu update returned true, rebuilding menu")
		ENDIF
	ENDIF
	
	// Process input according to the current highlighted nodes input method
	IF NODE_MENU_ITEM_UPDATE_CUSTOM_INPUT(sInst.sCurrentHighlightedItem, sInst)
		NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_REBUILD_MENU)	
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] INPUT_PROCESS_CURRENT_ITEMS_CUSTOM_INPUT - menu item input update returned true, rebuilding menu")
	ENDIF	
ENDPROC

/// PURPOSE:
///    Processes the highlight update for the current highlighted item
/// PARAMS:
///    sInst - the node menu instance
PROC _NODE_MENU_INPUT_PROCESS_CURRENT_ITEMS_HIGHLIGHT_UPDATE(NODE_MENU_STRUCT &sInst)
	// Process highlighted menu item
	IF NODE_MENU_ITEM_TRIGGER_ON_HIGHLIGHT_UPDATE_EVENT(sInst.sCurrentHighlightedItem)
		NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_REBUILD_MENU)	
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] INPUT_PROCESS_CURRENT_ITEMS_HIGHLIGHT_UPDATE - Highlight update returned true, rebuilding menu")
	ENDIF	
ENDPROC

/// PURPOSE:
///    Cancels any on going text input currently being processed by the menu
///    a fail result will be sent to the items waiting for the input result
/// PARAMS:
///    sInst - the node menu instance
PROC NODE_MENU_CANCEL_KEYBOARD_INPUT(NODE_MENU_STRUCT &sInst)
	_NODE_MENU_INPUT_CLEANUP_KEYBOARD_INPUT(sInst)
	NODE_MENU_BS_CLEAR_FLAG(sInst, NODE_MENU_BS_AWAITING_TEXT_INPUT_CONFIRMATION)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] CANCEL_KEYBOARD_INPUT - Cancelling current keyboard input")
ENDPROC

/// PURPOSE:
///    Restarts keyboard input if the current menu item requires keyboard input but isn't currently running the keyboard
/// PARAMS:
///    sInst - the node menu instance
PROC NODE_MENU_RETRY_KEYBOARD_INPUT(NODE_MENU_STRUCT &sInst)
	IF NODE_MENU_ITEM_DOES_REQUIRE_TEXT_INPUT_CONFIRMATION(sInst.sCurrentHighlightedItem)
		NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_AWAITING_TEXT_INPUT_CONFIRMATION)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] RETRY_KEYBOARD_INPUT - Setting keyboard up for a retry")
	ENDIF
	
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] RETRY_KEYBOARD_INPUT - Failed, highlighted item does not require input")
ENDPROC

/// PURPOSE:
///    Updates the highlighted menu item, allwoing any of its on update events to trigger
/// PARAMS:
///    sInst - the node menu instance
PROC _NODE_MENU_INPUT_UPDATE_CURRENT_HIGHLIGHTED_ITEM(NODE_MENU_STRUCT &sInst)
	_NODE_MENU_INPUT_PROCESS_CURRENT_ITEMS_HIGHLIGHT_UPDATE(sInst)
	
	IF NODE_MENU_ITEM_GET_SHOULD_DESCRIPTION_UPDATE_EVERY_FRAME(sInst.sCurrentHighlightedItem)
		_NODE_MENU_ADD_MENU_ITEMS_DESCRIPTION(sInst, sInst.sCurrentHighlightedItem)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Processes any active menu item confirmations such as press A to confirm or keyboard input
///    confirmation
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    TRUE if a confirmation is active
FUNC BOOL _NODE_MENU_INPUT_PROCESS_CONFIRMATIONS(NODE_MENU_STRUCT &sInst)
	// Process any pending confirmations
	IF NODE_MENU_BS_IS_FLAG_SET(sInst, NODE_MENU_BS_AWAITING_CONFIRMATION)
		_NODE_MENU_INPUT_PROCESS_CONFIRMATION(sInst)
		RETURN TRUE
	ENDIF
	
	IF NODE_MENU_BS_IS_FLAG_SET(sInst, NODE_MENU_BS_AWAITING_TEXT_INPUT_CONFIRMATION)
		_NODE_MENU_INPUT_PROCESS_KEYBOARD_INPUT(sInst)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Updates the input handling for the node menu
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    TRUE if the user has closed the menu
FUNC BOOL NODE_MENU_UPDATE(NODE_MENU_STRUCT &sInst)
	_NODE_MENU_INPUT_VALIDATE_CURRENT_HIGHLIGHTED_ITEM(sInst)
	_NODE_MENU_INPUT_MAINTAIN_CONTROL_SCHEME()
	
	// Confirmations need to complete
	IF _NODE_MENU_INPUT_PROCESS_CONFIRMATIONS(sInst)
		RETURN FALSE
	ENDIF
	
	IF NOT NODE_MENU_BS_IS_FLAG_SET(sInst, NODE_MENU_BS_REBUILD_MENU)
		// Only process input if there are no confirmations pending and the menu is not rebuilding
		_NODE_MENU_INPUT_PROCESS_MENU_NAVIGATION(sInst)	
	ENDIF
	
	_NODE_MENU_INPUT_UPDATE_CURRENT_HIGHLIGHTED_ITEM(sInst)
	
	// Reset the menu on close
	IF NODE_MENU_BS_IS_FLAG_SET(sInst, NODE_MENU_BS_CLOSE_MENU)
		NODE_MENU_RESET(sInst, FALSE)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC
