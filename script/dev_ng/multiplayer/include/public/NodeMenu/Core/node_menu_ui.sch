//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        	node_menu_ui.sch																	
/// Description: 	Header for drawing the node menu ui			
///	
/// Written by:  Online Technical Team: Tom Turner,															
/// Date created:  		27/08/2019
//////////////////////////////////////////////////////////////////////////////////////////////////////////////
   
USING "Globals.sch"
USING "menu_public.sch"

USING "Core\node_menu_struct.sch"
USING "Core\node_menu_helper.sch"
USING "Core\node_menu_flags.sch"

///-------------------------------
///		UI DEFAULT EVENTS
///-------------------------------    

/// PURPOSE:
///    Handles drawing a custom header for the menu
/// PARAMS:
///    sInst - the node menu instance 
PROC NODE_MENU_UI_DRAW_HEADER(NODE_MENU_STRUCT &sInst)
	IF sInst.fpDrawHeader = NULL
		EXIT
	ENDIF
	
	CALL sInst.fpDrawHeader()
ENDPROC

/// PURPOSE:
///    Handles loading the menu assets according to it
/// PARAMS:
///    sInst - the node menu instance 
/// RETURNS:
///    TRUE when the assets have finished loading
FUNC BOOL NODE_MENU_LOAD_ASSETS(NODE_MENU_STRUCT &sInst)
	IF sInst.fpLoadAssetsUpdate = NULL
		RETURN TRUE
	ENDIF
	
	RETURN CALL sInst.fpLoadAssetsUpdate()
ENDFUNC

/// PURPOSE:
///    Cleans up the menu assets according to its set clean up method
/// PARAMS:
///    sInst - the node menu instance
PROC NODE_MENU_UI_CLEANUP_ASSETS(NODE_MENU_STRUCT &sInst)
	IF sInst.fpCleanupAssets = NULL
		EXIT
	ENDIF
	
	CALL sInst.fpCleanupAssets()
ENDPROC

/// PURPOSE:
///    Adds the menu item to the menu and increments the count of added items internally,
///    it won't add the item if it is currently set to hidden
/// PARAMS:
///    sInst -  the node menu instance
///    sMenuItem - the menu item to add
/// RETURNS:
///    TRUE if it was added
FUNC BOOL NODE_MENU_UI_ADD_MENU_ITEM(NODE_MENU_STRUCT &sInst, INT iIndex, NODE_MENU_ITEM_STRUCT &sMenuItem)
	IF NODE_MENU_ITEM_IS_NODE_HIDDEN(sMenuItem)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] ADD_CHILD_MENU_ITEMS - ",
		"Hiding child at index: ", iIndex)
		RETURN FALSE
	ENDIF
	
	NODE_MENU_ITEM_TRIGGER_ON_ADD_EVENT(sMenuItem, iIndex)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] ADD_CHILD_MENU_ITEMS - Child count updated: ", NODE_MENU_GET_CURRENT_MAX_CHILD_INDEX(sInst))
	
	/// Override added menu items selectable flag according to its defined condition
	///	a little risky but allows us to control selectable state internally
	///      TODO: split out from core, should be for simple menu only
	g_sMenuData.bIsSelectable[iIndex] = NODE_MENU_ITEM_IS_NODE_SELECTABLE(sMenuItem)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Adds all the children of the current submenu node
/// PARAMS:
///    sInst - the node menu instance
PROC NODE_MENU_UI_ADD_CHILD_MENU_ITEMS(NODE_MENU_STRUCT &sInst)	
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] ADD_CHILD_MENU_ITEMS - Called")
	
	_NODE_MENU_SET_CURRENT_MAX_CHILD_INDEX(sInst, 0)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] ADD_CHILD_MENU_ITEMS - Start child count: 0")

	T_NODE_MENU_ITEM_GET_FROM_LOOKUP fpChildLookup = NODE_MENU_ITEM_GET_CHILDREN_CONNECTION(sInst.sCurrentParentItem)
	
	IF fpChildLookup = NULL
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] ADD_CHILD_MENU_ITEMS - No children")
		EXIT
	ENDIF
	
	INT iCurrentChildIndex = 0
	INT iChildrenAddedCount = 0
	NODE_MENU_ITEM_STRUCT sCurrentChild
	NODE_MENU_ITEM_LOOKUP_STATUS eStatus = NODE_MENU_ITEM_LOOKUP_STATUS_INVALID
	
	/// Hey you, call stack overflow in this loop? you forgot to return NODE_MENU_ITEM_LOOKUP_STATUS_END_OF_LOOKUP at the end of your lookup
	WHILE eStatus != NODE_MENU_ITEM_LOOKUP_STATUS_END_OF_LOOKUP	
		// Clear for next child  using the default prototype
		sCurrentChild = NODE_MENU_GET_MENU_ITEM_PROTOTYPE(sInst)
		
		// Get the status of the child
		eStatus = CALL fpChildLookup(iCurrentChildIndex, sCurrentChild)
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] ADD_CHILD_MENU_ITEMS - Grabbing: ", iCurrentChildIndex)
		
		// We should only add if we got the child successfully
		IF eStatus != NODE_MENU_ITEM_LOOKUP_STATUS_SUCCESS
			iCurrentChildIndex++
			RELOOP
		ENDIF
		
		CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] ADD_CHILD_MENU_ITEMS - Adding: ", iCurrentChildIndex)
		
		// If added successfully increment the max child index
		IF NODE_MENU_UI_ADD_MENU_ITEM(sInst, iCurrentChildIndex, sCurrentChild)
			iChildrenAddedCount++
		ENDIF
		
		iCurrentChildIndex++
	ENDWHILE
	
	// We don't want the count because we want it to start from 0
	INT iMaxIndex = IMAX(0, iChildrenAddedCount - 1)
	_NODE_MENU_SET_CURRENT_MAX_CHILD_INDEX(sInst, iMaxIndex)
	
	// Re-highlight the child item
	NODE_MENU_UI_SET_HIGHLIGHTED(sInst, sInst.iCurrentSelection)
	NODE_MENU_ITEM_TRIGGER_ON_HIGHLIGHT_EVENT(sInst.sCurrentHighlightedItem)
	
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] ADD_CHILD_MENU_ITEMS - End selection index: ", sInst.iCurrentSelection)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] ADD_CHILD_MENU_ITEMS - End child max index: ", iMaxIndex)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] ADD_CHILD_MENU_ITEMS - Finished")
ENDPROC

/// PURPOSE:
///    Updates the menus title with the current parent nodes title.
///    If the node doesn't have a title it will default to using its display name.
/// PARAMS:
///    sInst - the node menu instance
PROC _NODE_MENU_UI_UPDATE_TITLE(NODE_MENU_STRUCT &sInst)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] _NODE_MENU_UI_UPDATE_TITLE - Called")
	
	IF IS_STRING_NULL_OR_EMPTY(Get_String_From_TextLabel(NODE_MENU_ITEM_GET_TITLE(sInst.sCurrentParentItem)))
		SET_MENU_TITLE(NODE_MENU_ITEM_GET_DISPLAY_NAME(sInst.sCurrentParentItem))
		EXIT
	ENDIF
	
	// Only use title if defined, otherwise use items display name
	// TODO: Split out
	SET_MENU_TITLE(NODE_MENU_ITEM_GET_TITLE(sInst.sCurrentParentItem))	
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] _NODE_MENU_UI_UPDATE_TITLE - Finished")
ENDPROC

/// PURPOSE:
///    Adds the currently highlighted items help keys
/// PARAMS:
///    sInst - the node menu instance 
PROC _NODE_MENU_UI_ADD_HIGHLIGHTED_ITEMS_HELP_KEYS(NODE_MENU_STRUCT &sInst)
	NODE_MENU_ITEM_ADD_HELP_KEYS(sInst.sCurrentHighlightedItem, sInst.sCurrentHighlightedItem)
ENDPROC

/// PURPOSE:
///    Adds the current parents, parent help keys that should show when its a submenu
/// PARAMS:
///    sInst - the node menu instance
PROC _NODE_MENU_UI_ADD_CURRENT_PARENTS_SUBMENU_HELP_KEYS(NODE_MENU_STRUCT &sInst)
	NODE_MENU_ITEM_ADD_SUBMENU_HELP_KEYS(sInst.sCurrentParentItem,  sInst.sCurrentHighlightedItem)
ENDPROC

/// PURPOSE:
///    Adds the menus required help keys taking into account the base inputs
///    and any custom input specific to the highlighted menu item
/// PARAMS:
///    sInst - the node menu instance
PROC NODE_MENU_ADD_HELP_KEYS(NODE_MENU_STRUCT &sInst)		
	IF NODE_MENU_BS_IS_FLAG_SET(sInst, NODE_MENU_BS_AWAITING_CONFIRMATION)
		ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_CANCEL, "PIMK2_CANCEL") // TODO: this should be done through an abstraction
		ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_ACCEPT, "PIMK2_ACCEPT")
		EXIT
	ENDIF
	
	// The on screen keyboard system has its own way of adding help keys
	IF NODE_MENU_BS_IS_FLAG_SET(sInst, NODE_MENU_BS_AWAITING_TEXT_INPUT_CONFIRMATION)
		EXIT
	ENDIF
	
	// Only add submenus help keys if the current highlighted item isn't blocking its input
	IF NOT NODE_MENU_ITEM_GET_SHOULD_BLOCK_PARENT_INPUT(sInst.sCurrentHighlightedItem)
		_NODE_MENU_UI_ADD_CURRENT_PARENTS_SUBMENU_HELP_KEYS(sInst)
	ENDIF
	
	_NODE_MENU_UI_ADD_HIGHLIGHTED_ITEMS_HELP_KEYS(sInst)
ENDPROC

/// PURPOSE:
///    Builds the menu according to the user defined build method
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    TRUE if the menu has finished building
FUNC BOOL NODE_MENU_UI_BUILD_MENU(NODE_MENU_STRUCT &sInst)
	IF sInst.fpBuild = NULL
		RETURN TRUE
	ENDIF
	
	RETURN CALL sInst.fpBuild(sInst)
ENDFUNC

/// PURPOSE:
///    Sets up the player interaction menu mk2 according to its current state
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    TRUE if the menu has finished setting up
FUNC BOOL _NODE_MENU_UI_SETUP_MENU(NODE_MENU_STRUCT &sInst)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] _NODE_MENU_UI_SETUP_MENU - Called")
	
	IF NOT NODE_MENU_LOAD_ASSETS(sInst)
		RETURN FALSE
	ENDIF
	
	// Call the assigned build method
	IF NOT NODE_MENU_UI_BUILD_MENU(sInst)
		RETURN FALSE
	ENDIF
	
	NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_IS_MENU_SETUP)
	NODE_MENU_BS_CLEAR_FLAG(sInst, NODE_MENU_BS_REBUILD_MENU)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] _NODE_MENU_UI_SETUP_MENU - Finished")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Checks if the menu needs to be setup or a refresh has been requested
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    TRUE if the menu needs rebuilding due to setup or refresh
FUNC BOOL _NODE_MENU_UI_SHOULD_REBUILD_MENU(NODE_MENU_STRUCT &sInst)
	RETURN NODE_MENU_BS_IS_FLAG_SET(sInst, NODE_MENU_BS_REBUILD_MENU)
	OR NOT NODE_MENU_BS_IS_FLAG_SET(sInst, NODE_MENU_BS_IS_MENU_SETUP)
ENDFUNC

/// PURPOSE:
///    Draws the menu according to its set draw method
/// PARAMS:
///    sInst - the node menu instance
PROC _NODE_MENU_UI_DRAW_MENU(NODE_MENU_STRUCT &sInst)
	IF sInst.fpDrawMenu = NULL
		EXIT
	ENDIF
	
	CALL sInst.fpDrawMenu()
ENDPROC

/// PURPOSE:
///    Draws the node menu in its current state
/// PARAMS:
///    sInst - the node menu instance data
/// RETURNS:
///    TRUE is successfully drawn, FALSE if still setting up or failed
FUNC BOOL NODE_MENU_DRAW(NODE_MENU_STRUCT &sInst)	
	BOOL bRebuilding = _NODE_MENU_UI_SHOULD_REBUILD_MENU(sInst)
	
	IF bRebuilding
		IF NOT _NODE_MENU_UI_SETUP_MENU(sInst)	
			RETURN FALSE
		ENDIF
	ENDIF
	
	_NODE_MENU_UI_DRAW_MENU(sInst)
	
	// If the menu is rebuilding we don't want to overdraw on the custom header
	IF NOT bRebuilding
		NODE_MENU_UI_DRAW_HEADER(sInst)
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Cleans up the assets used by the node menu instance
/// PARAMS:
///    sInst - the node menu instance
PROC NODE_MENU_CLEAN_UP_ASSETS(NODE_MENU_STRUCT &sInst)	
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_CLEAN_UP_ASSETS - Called")
	NODE_MENU_UI_CLEANUP_ASSETS(sInst)	
	NODE_MENU_BS_CLEAR_FLAG(sInst, NODE_MENU_BS_IS_MENU_SETUP)
	NODE_MENU_BS_CLEAR_FLAG(sInst, NODE_MENU_BS_REBUILD_MENU)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] NODE_MENU_CLEAN_UP_ASSETS - Finished")
ENDPROC
