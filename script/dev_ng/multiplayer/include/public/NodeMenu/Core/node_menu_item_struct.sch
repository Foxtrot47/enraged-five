//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        	node_menu_item.sch																	
/// Description: 	Node menu item struct that is used within a node menu, 
///    				only getter and setter methods should be in this file				
///	
/// Todo:			- Rename events so they don't have ON prefixed
///    				- prefix all fp events with fp
///    
/// Written by:  Online Technical Team: Tom Turner,															
/// Date created:  		26/08/2019
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "menu_public.sch"
USING "Core\node_menu_input_override_method_enum.sch"
USING "Core\node_menu_item_typedefs.sch"
USING "Core\node_menu_item_flags.sch"

CONST_INT NODE_MENU_ITEM_COUNT_CHILD_DYNAMICALLY -1

/// PURPOSE: 
///    A menu item used in the node menu, used in a tree 
///    like structure to traverse linked parents and children.
///    Uses events to define behaviour
STRUCT NODE_MENU_ITEM_STRUCT
	/// NOTE FOR USERS 
	///     You should not be setting/getting these directly
	///     please use getters and setters NODE_MENU_ITEM_SET_... or NODE_MENU_ITEM_GET_...
	
	INT iID	= 0
	STRING tlTitle
	STRING tlSubTitle
	STRING tlDisplayName
	STRING tlDescription
	STRING tlConfirmation
	INT iDefaultHighlightIndex
	INT iFlags = NODE_MENU_ITEM_FLAGS_NONE
	
	INT iChildCount = NODE_MENU_ITEM_COUNT_CHILD_DYNAMICALLY
	T_NODE_MENU_ITEM_GET_FROM_LOOKUP getChildren
	T_NODE_MENU_ITEM_GET getParent
	
	
	T_NODE_MENU_ITEM_ADD_EVENT fpAddDescription
	BOOL bUpdateDescriptionEveryFrame = FALSE
	
	/// Condition used to check if the item can be selected
	/// if not it will show as greyed out
	T_NODE_MENU_ITEM_CONDITION isSelectable
	
	/// Condition used for checking if the item is hidden and should
	/// not be displayed
	T_NODE_MENU_ITEM_CONDITION isHidden
	
	/// Condition used to check if an item can be highlighted
	T_NODE_MENU_ITEM_CONDITION isHighlightable
	
	/// Event called for adding the menu item, use to configure the menu item type
	/// Called whenever the menu refreshes and the item needs to be shown
	T_NODE_MENU_ITEM_ADD_EVENT addItem
	
	/// Event called when the item is highlighted in the menu
	T_NODE_MENU_ITEM_CONDITION onHighlight
	
	/// Event called when an item is unhighlighted
	/// Usefull for when you need to release assets
	/// loaded by the menu item
	T_NODE_MENU_ITEM_CONDITION onUnHighlight
	
	/// Event called when the item is highlighted in the menu and the player
	/// presses select
	T_NODE_MENU_ITEM_EVENT onPress
	
	/// Event called when the item is highlighted in the menu and the player
	/// presses back
	T_NODE_MENU_ITEM_EVENT onBack
	
	/// Event called when the item becomes the parent in the menu (when it displays its children)
	T_NODE_MENU_ITEM_EVENT onBecomeParent
	
	/// Custom update event for when the item is highlighted
	/// can be used to handle custom controls for a specific item
	T_NODE_MENU_ITEM_CONDITION onHighlightUpdate
	
	// TODO: only add one event with the direction passed as a parameter
	/// Event called when the value on an item is changed regardless off increment
	T_NODE_MENU_ITEM_EVENT onValueChange
	
	/// Event called when the menu items value is incremented to the next one
	/// e.g. right on the dpad is pressed by default
	T_NODE_MENU_ITEM_EVENT onValueNext
	
	/// Event called when the menu items value is decremented to the previous one
	/// e.g. left on the dpad is pressed by default
	T_NODE_MENU_ITEM_EVENT onValuePrevious
	
	/// Event that blocks using the menu item until it returns true
	/// use this for loading the assets the menu item needs
	/// e.g. models for a menu item that swaps clothing
	T_NODE_MENU_ITEM_CONDITION onLoadAssetsUpdate
	
		/// Event called when adding buttons to the UI override this 
	/// to override the input help buttons for this specific menu item
	T_NODE_MENU_ITEM_CONDITION_GET fpAddHelpKeys
	
	// Used for processing custom input
	T_NODE_MENU_ITEM_INPUT_UPDATE fpInputUpdate
	
	/// If true the custom input for this node will ignore
	/// the input of the submenu
	BOOL bBlockParentInput = FALSE
	
	/// Event called when adding buttons to the UI override this 
	/// to override the input help buttons for this specific menu item
	T_NODE_MENU_ITEM_CONDITION_GET fpAddSubmenuHelpKeys
	
	// Used for processing custom input when the menu item is the current submenu
	T_NODE_MENU_ITEM_INPUT_UPDATE fpSubmenuInputUpdate
	
	T_NODE_MENU_ITEM_PROCESS_INPUT_RESULT fpTextInputResult
ENDSTRUCT

PROC NODE_MENU_ITEM_CLEAR(NODE_MENU_ITEM_STRUCT &sInst)
	NODE_MENU_ITEM_STRUCT sEmpty
	COPY_SCRIPT_STRUCT(sInst, sEmpty, SIZE_OF(NODE_MENU_ITEM_STRUCT))
ENDPROC

///------------------------------------
///    GETTERS
///------------------------------------

FUNC BOOL NODE_MENU_ITEM_IS_FLAG_SET(NODE_MENU_ITEM_STRUCT &sMenuItem, NODE_MENU_ITEM_FLAGS eFlag)
	RETURN IS_BIT_SET_ENUM(sMenuItem.iFlags, eFlag)
ENDFUNC

/// PURPOSE:
///    Gets the child index that should be highlighted when this item becomes a submenu
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC INT NODE_MENU_ITEM_GET_DEFAULT_HIGHLIGHTED_CHILD_INDEX(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.iDefaultHighlightIndex
ENDFUNC

/// PURPOSE:
///    Gets the id of the node menu item
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC INT NODE_MENU_ITEM_GET_ID(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.iID
ENDFUNC

/// PURPOSE:
///    Gets the title of this node menu item, this is what is shown at the top of the menu
///    when showing this items children. 
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC STRING NODE_MENU_ITEM_GET_TITLE(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.tlTitle
ENDFUNC

/// PURPOSE:
///    Gets the name of the tab assigned to this menu item, 
///    this indicates the name of the tab the user is on
/// PARAMS:
///    sMenuItem - the menu item instance  
FUNC STRING NODE_MENU_ITEM_GET_SUBTITLE(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.tlSubTitle
ENDFUNC

/// PURPOSE:
///    Gets the display name of the menu item, this is what is shown in the menu
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC STRING NODE_MENU_ITEM_GET_DISPLAY_NAME(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.tlDisplayName
ENDFUNC

/// PURPOSE:
///    Gets the menu items description, this is what is shown at the bottom of the menu
///    when the item is highlighted
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC STRING NODE_MENU_ITEM_GET_DESCRIPTION(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.tlDescription
ENDFUNC

/// PURPOSE:
///    Gets the menu items add description, this allows the user to add a descripition with any parameters they require
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_ADD_EVENT NODE_MENU_ITEM_GET_CUSTOM_DESCRIPTION(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.fpAddDescription
ENDFUNC

/// PURPOSE:
///    Gets if the menu items description should update every frame
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC BOOL NODE_MENU_ITEM_GET_SHOULD_DESCRIPTION_UPDATE_EVERY_FRAME(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.bUpdateDescriptionEveryFrame
ENDFUNC

/// PURPOSE:
///    Gets the menu items confirmation label, empty means no confirmation set
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC STRING NODE_MENU_ITEM_GET_CONFIRMATION(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.tlConfirmation
ENDFUNC

/// PURPOSE:
///    Gets the menu items flags
/// PARAMS:
///    sMenuItem - the menu item to set the flag on
FUNC INT NODE_MENU_ITEM_GET_FLAGS(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.iFlags
ENDFUNC

/// PURPOSE:
///    Gets the menu items connection to its children
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_GET_FROM_LOOKUP NODE_MENU_ITEM_GET_CHILDREN_CONNECTION(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.getChildren
ENDFUNC

/// PURPOSE:
///    Gets the menu items connection to its parent
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_GET NODE_MENU_ITEM_GET_PARENT_CONNECTION(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.getParent
ENDFUNC

/// PURPOSE:
///    Gets the is selectable condition that defines if the item can be selected
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_CONDITION NODE_MENU_ITEM_GET_IS_SELECTABLE_CONDITION(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.isSelectable
ENDFUNC

/// PURPOSE:
///    Gets the is hidden condition for the menu item
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_CONDITION NODE_MENU_ITEM_GET_IS_HIDDEN_CONDITION(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.isHidden
ENDFUNC

/// PURPOSE:
///    Gets the is highlightable condition
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_CONDITION NODE_MENU_ITEM_GET_IS_HIGHLIGHTABLE_CONDITION(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.isHighlightable
ENDFUNC

/// PURPOSE:
///    Gets the on add functionality of the menu item
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_ADD_EVENT NODE_MENU_ITEM_GET_ON_ADD(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.addItem
ENDFUNC

/// PURPOSE:
///    Gets the menu items on highlight behaviour 
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_CONDITION NODE_MENU_ITEM_GET_ON_HIGHLIGHT(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.onHighlight
ENDFUNC

/// PURPOSE:
///    Gets the menu items on un-highlight behaviour
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_CONDITION NODE_MENU_ITEM_GET_ON_UNHIGHLIGHT(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.onUnHighlight
ENDFUNC

/// PURPOSE:
///    Gets the menu items on press behaviour 
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_EVENT NODE_MENU_ITEM_GET_ON_PRESS(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.onPress
ENDFUNC

/// PURPOSE:
///    Gets the menu items on back behaviour 
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_EVENT NODE_MENU_ITEM_GET_ON_BACK(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.onBack
ENDFUNC

/// PURPOSE:
///    Gets the menu items on become parent 
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_EVENT NODE_MENU_ITEM_GET_ON_BECOME_PARENT(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.onBecomeParent
ENDFUNC

/// PURPOSE:
///    Gets the on value change behaviour
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_EVENT NODE_MENU_ITEM_GET_ON_VALUE_CHANGE(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.onValueChange
ENDFUNC

/// PURPOSE:
///    Gets the on value next behaviour
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_EVENT NODE_MENU_ITEM_GET_ON_VALUE_NEXT(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.onValueNext
ENDFUNC

/// PURPOSE:
///    Gets the on value previous behaviour
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_EVENT NODE_MENU_ITEM_GET_ON_VALUE_PREVIOUS(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.onValuePrevious
ENDFUNC

/// PURPOSE:
///    Gets the on load update
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_CONDITION NODE_MENU_ITEM_GET_ON_LOAD_ASSETS_UPDATE(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.onLoadAssetsUpdate
ENDFUNC

/// PURPOSE:
///    Gets the on highlight update behaviour
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_CONDITION NODE_MENU_ITEM_GET_ON_HIGHLIGHT_UPDATE(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.onHighlightUpdate
ENDFUNC

/// PURPOSE:
///    Gets the add help keys
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_CONDITION_GET NODE_MENU_ITEM_GET_ADD_HELP_KEYS(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.fpAddHelpKeys
ENDFUNC

/// PURPOSE:
///    Gets the custom input behaviour, this determines how the menu items input works
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_INPUT_UPDATE NODE_MENU_ITEM_GET_INPUT_UPDATE(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.fpInputUpdate 
ENDFUNC

/// PURPOSE:
///    Gets the add parent help keys
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_CONDITION_GET NODE_MENU_ITEM_GET_ADD_PARENT_HELP_KEYS(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.fpAddSubmenuHelpKeys
ENDFUNC

/// PURPOSE:
///    Gets the custom input behaviour for when the menu item is a submenu, this determines how the menu items input works
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_INPUT_UPDATE NODE_MENU_ITEM_GET_PARENT_INPUT_UPDATE(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.fpSubmenuInputUpdate 
ENDFUNC

/// PURPOSE:
///    Gets if the parents input should be blocked in favour of this menu items
///    custom input
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC BOOL NODE_MENU_ITEM_GET_SHOULD_BLOCK_PARENT_INPUT(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.bBlockParentInput
ENDFUNC

/// PURPOSE:
///    Gets the event that process an on screen keyboard text input result
/// PARAMS:
///    sMenuItem - the menu item instance
FUNC T_NODE_MENU_ITEM_PROCESS_INPUT_RESULT NODE_MENU_ITEM_GET_TEXT_INPUT_RESULT_EVENT(NODE_MENU_ITEM_STRUCT &sMenuItem)
	RETURN sMenuItem.fpTextInputResult
ENDFUNC

///------------------------------------
///    SETTERS
///------------------------------------

/// PURPOSE:
///    Sets a flag on the menu item
/// PARAMS:
///    sMenuItem - the menu item to set the flag on
///    eFlags - flag to set, currently CANNOT or flags together (TODO)
PROC NODE_MENU_ITEM_SET_FLAGS(NODE_MENU_ITEM_STRUCT &sMenuItem, NODE_MENU_ITEM_FLAGS eFlags)
	SET_BIT_ENUM(sMenuItem.iFlags, eFlags)
ENDPROC

PROC NODE_MENU_ITEM_SET_DEFAULT_HIGHLIGHTED_CHILD_INDEX(NODE_MENU_ITEM_STRUCT &sMenuItem, INT iIndex)
	sMenuItem.iDefaultHighlightIndex = iIndex
ENDPROC

/// PURPOSE:
///    Clears a flag on the menu item
/// PARAMS:
///    sMenuItem - the menu item to set the flag on
///    eFlags - flag to clear/remove
PROC NODE_MENU_ITEM_CLEAR_FLAG(NODE_MENU_ITEM_STRUCT &sMenuItem, NODE_MENU_ITEM_FLAGS eFlags)
	CLEAR_BIT_ENUM(sMenuItem.iFlags, eFlags)
ENDPROC

/// PURPOSE:
///    Sets if the menu item flag i enabled or disabled
/// PARAMS:
///    sMenuItem - the menu item to enable the flag for
///    eFlags - the flag to enable
///    bEnable - if the flag is on or off
PROC NODE_MENU_ITEM_ENABLE_FLAG(NODE_MENU_ITEM_STRUCT &sMenuItem, NODE_MENU_ITEM_FLAGS eFlags, BOOL bEnable)
	IF bEnable
		SET_BIT_ENUM(sMenuItem.iFlags, eFlags)
	ELSE
		CLEAR_BIT_ENUM(sMenuItem.iFlags, eFlags)
	ENDIF
ENDPROC

/// PURPOSE:
///    Sets the id of the node menu item in its sub menu, used internally
///    to determine this items index in the submenu
/// PARAMS:
///    sMenuItem - the menu item instance
///    iID - the id to set
PROC NODE_MENU_ITEM_SET_ID(NODE_MENU_ITEM_STRUCT &sMenuItem, INT iID)
	sMenuItem.iID = iID
ENDPROC

/// PURPOSE:
///    Sets the title of this node menu item, this is shown at the top of the menu
///    when showing this items children. This is typically shown in all upper case.
///    If left empty it will show the display name instead
/// PARAMS:
///    sMenuItem - the menu item instance
///    tlTitle - the title text label to set
PROC NODE_MENU_ITEM_SET_TITLE(NODE_MENU_ITEM_STRUCT &sMenuItem, STRING tlTitle, BOOL bUseLiteralString = FALSE)
	sMenuItem.tlTitle = tlTitle
	NODE_MENU_ITEM_ENABLE_FLAG(sMenuItem, NODE_MENU_ITEM_FLAGS_USE_LITERAL_STRING_FOR_TITLE, bUseLiteralString)
ENDPROC

/// PURPOSE:
///    Sets the display name of the menu item, this is what is shown in the menu
/// PARAMS:
///    sMenuItem - the menu item instance
///    tlDisplayName - the display name text label to set
PROC NODE_MENU_ITEM_SET_DISPLAY_NAME(NODE_MENU_ITEM_STRUCT &sMenuItem, STRING tlDisplayName, BOOL bUseLiteralString = FALSE)
	sMenuItem.tlDisplayName = tlDisplayName
	NODE_MENU_ITEM_ENABLE_FLAG(sMenuItem, NODE_MENU_ITEM_FLAGS_USE_LITERAL_STRING_FOR_DISPLAY_NAME, bUseLiteralString)
ENDPROC

/// PURPOSE:
///    Sets the tab name for this menu item
/// PARAMS:
///    sMenuItem - the menu item instance
///    tlSubTitle - the name of the tab to indicate to the user which tab they are on
PROC NODE_MENU_ITEM_SET_SUBTITLE(NODE_MENU_ITEM_STRUCT &sMenuItem, STRING tlSubTitle, BOOL bUseLiteralString = FALSE)
	sMenuItem.tlSubTitle = tlSubTitle
	NODE_MENU_ITEM_ENABLE_FLAG(sMenuItem, NODE_MENU_ITEM_FLAGS_USE_LITERAL_STRING_FOR_SUBTITLE, bUseLiteralString)
ENDPROC

/// PURPOSE:
///    Sets the menu items description, this is shown at the bottom of the menu
///    when the item is highlighted
/// PARAMS:
///    sMenuItem - the menu item instance
///    tlDescription - the description text label to set
PROC NODE_MENU_ITEM_SET_DESCRIPTION(NODE_MENU_ITEM_STRUCT &sMenuItem, STRING tlDescription, BOOL bUseLiteralString = FALSE)
	sMenuItem.fpAddDescription = NULL
	sMenuItem.bUpdateDescriptionEveryFrame = FALSE
	sMenuItem.tlDescription = tlDescription
	NODE_MENU_ITEM_ENABLE_FLAG(sMenuItem, NODE_MENU_ITEM_FLAGS_USE_LITERAL_STRING_FOR_DESCRIPTION, bUseLiteralString)
ENDPROC

/// PURPOSE:
///    Sets the menu items add description event, this is so you can define a function that adds any params you need in teh description
/// PARAMS:
///    sMenuItem - the menu item instance
PROC NODE_MENU_ITEM_SET_CUSTOM_DESCRIPTION(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_ADD_EVENT fpAddDescriptionEvent, BOOL bUpdateEveryFrame = FALSE)
	sMenuItem.tlDescription = ""
	sMenuItem.bUpdateDescriptionEveryFrame = bUpdateEveryFrame
	sMenuItem.fpAddDescription = fpAddDescriptionEvent
ENDPROC

/// PURPOSE:
///    Sets the menu item to use a confirmation that will appear when the item is pressed,
///    leaving it blank means no confirmation is set
/// PARAMS:
///    sMenuItem - the menu item instance
///    tlConfirmation - the text label of the confirmation message
PROC NODE_MENU_ITEM_SET_CONFIRMATION(NODE_MENU_ITEM_STRUCT &sMenuItem, STRING tlConfirmation)
	sMenuItem.tlConfirmation = tlConfirmation
ENDPROC

/// PURPOSE:
///    Removes any set confirmation from the menu item
/// PARAMS:
///    sMenuItem - the menu item instance
PROC NODE_MENU_ITEM_REMOVE_CONFIRMATION(NODE_MENU_ITEM_STRUCT &sMenuItem)
	sMenuItem.tlConfirmation = ""
ENDPROC

/// PURPOSE:
///    Sets if the user needs to input valid text before the menu item can be pressed, the given
///    function pointer will be called when the keyboard has finished gathering input and should be used
///    to process the result
/// PARAMS:
///    sMenuItem - the menu item instance
///    fpInputConfirmationResultProcess - the function to process the keyboard input result
PROC NODE_MENU_ITEM_SET_KEYBOARD_INPUT_CONFIRMATION(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_PROCESS_INPUT_RESULT fpInputConfirmationResultProcess)
	sMenuItem.fpTextInputResult = fpInputConfirmationResultProcess
ENDPROC

/// PURPOSE:
///    Removes an input confirmation requirement from the menu item
/// PARAMS:
///    sMenuItem -  the menu item instance
PROC NODE_MENU_ITEM_REMOVE_KEYBOARD_INPUT_CONFIRMATION(NODE_MENU_ITEM_STRUCT &sMenuItem)
	sMenuItem.fpTextInputResult = NULL
ENDPROC

/// PURPOSE:
///    Sets the menu items connection to its children
/// PARAMS:
///    sMenuItem - the menu item instance
///    iParentID - the menu items id as a parent, this should be its index in its submenu
///    getChildren - the lookup containing the menu items children to set with signature:
///    				 FUNC BOOL T_NODE_MENU_ITEM_GET_FROM_LOOKUP(INT iIndex, NODE_MENU_ITEM_STRUCT &sNode)
PROC NODE_MENU_ITEM_SET_CHILDREN(NODE_MENU_ITEM_STRUCT &sMenuItem, INT iParentID, 
T_NODE_MENU_ITEM_GET_FROM_LOOKUP getChildren, INT iChildCount = NODE_MENU_ITEM_COUNT_CHILD_DYNAMICALLY)
	sMenuItem.iID = iParentID
	sMenuItem.getChildren = getChildren
	sMenuItem.iChildCount = iChildCount	
ENDPROC

/// PURPOSE:
///    Sets the menu items connection to its parent
/// PARAMS:
///    sMenuItem - the menu item instance
///    getParent - the parent getter to set
PROC NODE_MENU_ITEM_SET_PARENT(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_GET getParent)
	sMenuItem.getParent = getParent
ENDPROC

/// PURPOSE:
///    Sets the is selectable condition that defines if the item can be selected,
///    when the condition returns false the item is greyed out
/// PARAMS:
///    sMenuItem - the menu item instance
///    isSelectable - the condition to set
PROC NODE_MENU_ITEM_SET_IS_SELECTABLE_CONDITION(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_CONDITION isSelectable)
	sMenuItem.isSelectable = isSelectable
ENDPROC

/// PURPOSE:
///    Sets if the item is selectable, if set to false the item will be greyed out
/// PARAMS:
///    sMenuItem - the menu item instance
///    isSelectable - if the item is selectable
PROC NODE_MENU_ITEM_SET_IS_SELECTABLE(NODE_MENU_ITEM_STRUCT &sMenuItem, BOOL isSelectable)	
	IF isSelectable
		sMenuItem.isSelectable = &_NODE_MENU_ITEM_CONDITION_DEFAULT_TRUE
		EXIT
	ENDIF
	
	sMenuItem.isSelectable = &_NODE_MENU_ITEM_CONDITION_DEFAULT_FALSE
ENDPROC

/// PURPOSE:
///    Sets the is hidden condition for the menu item
/// PARAMS:
///    sMenuItem - the menu item instance
///    isHidden - the condition to set
PROC NODE_MENU_ITEM_SET_IS_HIDDEN_CONDITION(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_CONDITION isHidden)
	sMenuItem.isHidden = isHidden
ENDPROC

/// PURPOSE:
///    Sets if the item is hidden
/// PARAMS:
///    sMenuItem - the menu item instance
///    isHidden - if the item is hidden
PROC NODE_MENU_ITEM_SET_IS_HIDDEN(NODE_MENU_ITEM_STRUCT &sMenuItem, BOOL isHidden)
	IF isHidden
		sMenuItem.isHidden = &_NODE_MENU_ITEM_CONDITION_DEFAULT_TRUE
		EXIT
	ENDIF
	
	sMenuItem.isHidden = &_NODE_MENU_ITEM_CONDITION_DEFAULT_FALSE
ENDPROC

/// PURPOSE:
///    Sets the is highlightable condition, if it returns false the menu item will be skipped
///    over, good for info items that don't do anything
/// PARAMS:
///    sMenuItem - the menu item instance
///    isHighlightable - the condition to set
PROC NODE_MENU_ITEM_SET_IS_HIGHLIGHTABLE_CONDITION(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_CONDITION isHighlightable)
	sMenuItem.isHighlightable = isHighlightable
ENDPROC

/// PURPOSE:
///    Sets if the menu item is highlightable, if false the menu item will be skipped
///    over; Good for info items that don't do anything
/// PARAMS:
///    sMenuItem - the menu item instance
///    isHighlightable - if the item ishighlightable
PROC NODE_MENU_ITEM_SET_IS_HIGHLIGHTABLE(NODE_MENU_ITEM_STRUCT &sMenuItem, BOOL isHighlightable)	
	IF isHighlightable
		sMenuItem.isHighlightable = &_NODE_MENU_ITEM_CONDITION_DEFAULT_TRUE
		EXIT
	ENDIF
	
	sMenuItem.isHighlightable = &_NODE_MENU_ITEM_CONDITION_DEFAULT_FALSE
ENDPROC

/// PURPOSE:
///    Sets the on add functionality, use this to define the menu items
///    format using ADD_MENU_ITEM_...
/// PARAMS:
///    sMenuItem - the menu item instance
///    onAdd - the on add to set
PROC NODE_MENU_ITEM_SET_ON_ADD(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_ADD_EVENT onAdd)
	sMenuItem.addItem = onAdd
ENDPROC

/// PURPOSE:
///    Sets the menu item on highlight behaviour which fires once when the menu item is highlighted.
///    A common use is to use SET_MENU_ITEM_TOGGLEABLE, as toggles should only appear
///    on highlight as part of the games design
/// PARAMS:
///    sMenuItem - the menu item instance
///    onHighlight - the on highlight event to set
PROC NODE_MENU_ITEM_SET_ON_HIGHLIGHT(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_CONDITION onHighlight)
	sMenuItem.onHighlight = onHighlight
ENDPROC

/// PURPOSE:
///    Sets the menu item on un-highlight behaviour which fires once when the menu item is un-highlighted.
/// PARAMS:
///    sMenuItem - the menu item instance
///    onHighlight - the on un-highlight event to set
PROC NODE_MENU_ITEM_SET_ON_UNHIGHLIGHT(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_CONDITION onUnHighlight)
	sMenuItem.onUnHighlight = onUnHighlight
ENDPROC

/// PURPOSE:
///    Sets the on press behaviour which fires when the menu item is selected in the menu
/// PARAMS:
///    sMenuItem - the menu item instance
///    onPress - the on press event to set
PROC NODE_MENU_ITEM_SET_ON_PRESS(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_EVENT onPress)
	sMenuItem.onPress = onPress
ENDPROC

/// PURPOSE:
///    Sets the on back behaviour which fires when the menu item is highlighted in the menu and back is pressed
/// PARAMS:
///    sMenuItem - the menu item instance
///    onBack - the on back event to set
PROC NODE_MENU_ITEM_SET_ON_BACK(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_EVENT onBack)
	sMenuItem.onBack = onBack
ENDPROC

/// PURPOSE:
///    Sets the on become parent behaviour which fires when the menu item becomes the current parent (displays its children)
/// PARAMS:
///    sMenuItem - the menu item instance
///    onBack - the on back event to set
PROC NODE_MENU_ITEM_SET_ON_BECOME_PARENT(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_EVENT onBecomeParent)
	sMenuItem.onBecomeParent = onBecomeParent
ENDPROC

/// PURPOSE:
///    Sets the on value change behaviour which fires when the value is changed in either direction
/// PARAMS:
///    sMenuItem - the menu item instance
///    onValueChange - the on value change event to set
PROC NODE_MENU_ITEM_SET_ON_VALUE_CHANGE(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_EVENT onValueChange)
	sMenuItem.onValueChange = onValueChange
ENDPROC

/// PURPOSE:
///    Sets the on value next behaviour which fires when the value is changed to the right
/// PARAMS:
///    sMenuItem - the menu item instance
///    onValueNext - the on next behaviour to set
PROC NODE_MENU_ITEM_SET_ON_VALUE_NEXT(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_EVENT onValueNext)
	sMenuItem.onValueNext = onValueNext
ENDPROC

/// PURPOSE:
///    Sets the on value previous behaviour which fires when the value is changed to the left
/// PARAMS:
///    sMenuItem - the menu item instance
///    onValuePrevious - the on previous behaviour to set
PROC NODE_MENU_ITEM_SET_ON_VALUE_PREVIOUS(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_EVENT onValuePrevious)
	sMenuItem.onValuePrevious = onValuePrevious
ENDPROC

/// PURPOSE:
///    Sets the on load update, use this to load assets that the menu item needs. 
///    The load is performed when the item is highlighted; during the load, interaction with the item is blocked.
///    The function should retrun true when the load has finished
/// PARAMS:
///    sMenuItem - the menu item instance
///    onLoadAssetsUpdate - the on load assets update behaviour to set
PROC NODE_MENU_ITEM_SET_ON_LOAD_ASSETS_UPDATE(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_CONDITION onLoadAssetsUpdate)
	sMenuItem.onLoadAssetsUpdate = onLoadAssetsUpdate
ENDPROC

/// PURPOSE:
///    Sets the on highlight update behaviour, this is called while the menu item is highlighted.
///    On return true it will rebuild the menu which is necessary for updating menu data.
///    Useful for processing custom inputs for the menu item. 
/// PARAMS:
///    sMenuItem - the menu item instance
///    onHighlightUpdate - the on highlight update to set
PROC NODE_MENU_ITEM_SET_ON_HIGHLIGHT_UPDATE(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_CONDITION onHighlightUpdate)
	sMenuItem.onHighlightUpdate = onHighlightUpdate
ENDPROC

/// PURPOSE:
///    Sets the add help keys, this defines the inputs shown at the bottom right for this menu item
/// PARAMS:
///    sMenuItem - the menu item instance
///    fpAddHelpKeys - the help key add to set
PROC NODE_MENU_ITEM_SET_ADD_HELP_KEYS(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_CONDITION_GET fpAddHelpKeys)
	sMenuItem.fpAddHelpKeys = fpAddHelpKeys
ENDPROC	

/// PURPOSE:
///    Sets if the menu item should block recieving input from the parent (sub menu)
///    in favour of its own
/// PARAMS:
///    sMenuItem - the menu item instace
///    bBlockParentInput - if the parents input should be blocked
PROC NODE_MENU_ITEM_SET_BLOCK_PARENT_INPUT(NODE_MENU_ITEM_STRUCT &sMenuItem, BOOL bBlockParentInput)
	sMenuItem.bBlockParentInput = bBlockParentInput
ENDPROC

/// PURPOSE:
///    Sets the custom input behaviour, use this to change how input works for this menu item.
///    Remember you will be responsible for both PC and console input
/// PARAMS:
///    sMenuItem - the menu item instance
///    fpInputUpdate - the input behaviour
PROC NODE_MENU_ITEM_SET_INPUT_UPDATE(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_INPUT_UPDATE fpInputUpdate, BOOL bBlockParentInput = FALSE)
	sMenuItem.fpInputUpdate = fpInputUpdate
	NODE_MENU_ITEM_SET_BLOCK_PARENT_INPUT(sMenuItem, bBlockParentInput)
ENDPROC

PROC NODE_MENU_ITEM_CLEAR_INPUT_UPDATE(NODE_MENU_ITEM_STRUCT &sMenuItem)
	sMenuItem.fpInputUpdate = NULL
ENDPROC

/// PURPOSE:
///    Sets the add help keys for when the item is a submenu, this defines the inputs shown at the bottom right for this menu item
/// PARAMS:
///    sMenuItem - the menu item instance
///    fpAddHelpKeys - the help key add to set
PROC NODE_MENU_ITEM_SET_SUBMENU_HELP_KEYS(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_CONDITION_GET fpAddSubmenuHelpKeys)
	sMenuItem.fpAddSubmenuHelpKeys = fpAddSubmenuHelpKeys
ENDPROC	

/// PURPOSE:
///    Sets the custom input behaviour for when this menu item is a submenu, allowing it to use the same input
///    for all its children (unless the child is blocking the input).
/// PARAMS:
///    sMenuItem - the menu item instance
///    fpSubmenuInputUpdate - the parent input behaviour
PROC NODE_MENU_ITEM_SET_SUBMENU_INPUT_UPDATE(NODE_MENU_ITEM_STRUCT &sMenuItem, T_NODE_MENU_ITEM_INPUT_UPDATE fpSubmenuInputUpdate)
	sMenuItem.fpSubmenuInputUpdate = fpSubmenuInputUpdate
ENDPROC
