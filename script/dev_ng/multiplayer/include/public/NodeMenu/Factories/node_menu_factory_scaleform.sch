USING "Core\node_menu_struct.sch"
USING "Core\node_menu_ui.sch"
USING "Core\node_menu_input.sch"
USING "Core\node_menu_item.sch"
USING "Core\node_menu_flags.sch"
USING "Factories\node_menu_factory_shared.sch"
USING "script_usecontext.sch"

TYPEDEF PROC T_NODE_MENU_SCALEFORM_ON_NAVIGATION_PROCESSED(BOOL bNewHighlight, NODE_MENU_BASE_INPUT_EVENT eInputEvent)

STRUCT VECTOR2D
	INT x
	INT y
ENDSTRUCT

/// PURPOSE: 
///    Extra data needed to run a Scaleform node menu
STRUCT NODE_MENU_SCALEFORM_STRUCT
	SCALEFORM_INDEX siScaleform
	INT iScreenType = 0
	
	INT iHighlight = -1
	INT iLastFrameHighlight = -1
	SCALEFORM_RETURN_INDEX sriHighlightRequest
	BOOL bHighlightRequested = FALSE
	
	NODE_MENU_BASE_INPUT_EVENT eInputEvent
	T_NODE_MENU_SCALEFORM_ON_NAVIGATION_PROCESSED onNavigationProcessed
	VECTOR2D vLeftJoystick
	SCRIPT_TIMER sLeftStickDelay
	NODE_MENU_STRUCT sNodeMenu
	
	SCALEFORM_INDEX sfButtons
	SCALEFORM_INSTRUCTIONAL_BUTTONS sButtons
ENDSTRUCT

CONST_INT _ciNODE_MENU_SCALEFORM_LEFT_STICK_DELAY_TIME 100
CONST_INT ANALOGUE_STICK_NEUTRAL 127
CONST_INT ANALOGUE_STICK_DEADZONE ANALOGUE_STICK_NEUTRAL / 2

/// PURPOSE:
///    Sets the scaleform index
/// PARAMS:
///    sInst - the node menu scaleform
///    siScaleform - the scaleform index to set
PROC NODE_MENU_SCALEFORM_SET_SCALEFORM_INDEX(SCALEFORM_INDEX &sfMovie, SCALEFORM_INDEX siScaleform)
	sfMovie = siScaleform
ENDPROC

/// PURPOSE:
///    Sets the screen to show when the menu is built
/// PARAMS:
///    sInst - the node menu scaleform
///    eScreenType - Any enum of the screen ID you want to show
PROC NODE_MENU_SCALEFORM_SET_CURRENT_SCREEN_TYPE(NODE_MENU_SCALEFORM_STRUCT &sInst, ENUM_TO_INT eScreenType)
	sInst.iScreenType = ENUM_TO_INT(eScreenType)
ENDPROC

/// PURPOSE:
///    Returns the scaleform index currently used by the menu
/// PARAMS:
///    sInst - the node menu scaleform
/// RETURNS:
///    The scaleform index currently used by the menu
FUNC SCALEFORM_INDEX NODE_MENU_SCALEFORM_GET_SCALEFORM_INDEX(NODE_MENU_SCALEFORM_STRUCT &sInst)
	RETURN sInst.siScaleform
ENDFUNC

/// PURPOSE:
///    Returns the current scaleform screen type
/// PARAMS:
///    sInst - the node menu scaleform
/// RETURNS:
///    The current scaleform screen type
FUNC INT NODE_MENU_SCALEFORM_GET_CURRENT_SCREEN_TYPE(NODE_MENU_SCALEFORM_STRUCT &sInst)
	RETURN sInst.iScreenType
ENDFUNC

/// PURPOSE:
///    Sets the scaleforms current selected highlight to the given selection index
/// PARAMS:
///    sInst - the node menu scaleform
///    iSelection - The item index to select
PROC NODE_MENU_SCALEFORM_SET_HIGHLIGHT_ON_SCALEFORM(NODE_MENU_SCALEFORM_STRUCT &sInst, INT iSelection)
	BEGIN_SCALEFORM_MOVIE_METHOD(NODE_MENU_SCALEFORM_GET_SCALEFORM_INDEX(sInst), "SET_CURRENT_SELECTION")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iSelection)
	END_SCALEFORM_MOVIE_METHOD()
	PRINTLN("[NODE_MENU][SCALEFORM][SELECTION] NODE_MENU_SCALEFORM_SET_HIGHLIGHT_ON_SCALEFORM - SET_CURRENT_SELECTION: ", iSelection)
ENDPROC

/// PURPOSE:
///    Clears the help keys on the scaleform node menu
/// PARAMS:
///    sInst - the node menu scaleform
PROC _NODE_MENU_SCALEFORM_CLEAR_HELP_KEYS(NODE_MENU_SCALEFORM_STRUCT &sInst)
	REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sInst.sButtons)
ENDPROC

/// PURPOSE:
///    Appends the current parents subtitle if one is defined, this does not call the
///    scaleform to method to set the subtitle it only adds the string
/// PARAMS:
///    sInst - the node menu instance
PROC _NODE_MENU_SCALEFORM_APPEND_CURRENT_PARENTS_SUBTITLE(NODE_MENU_SCALEFORM_STRUCT &sInst)
	STRING strSubtitleName = NODE_MENU_ITEM_GET_SUBTITLE(sInst.sNodeMenu.sCurrentParentItem)
	
	IF IS_STRING_NULL(strSubtitleName)
		EXIT
	ENDIF
	
	IF IS_BIT_SET_ENUM(sInst.sNodeMenu.sCurrentParentItem.iFlags, NODE_MENU_ITEM_FLAGS_USE_LITERAL_STRING_FOR_SUBTITLE)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(strSubtitleName)
	ELSE
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strSubtitleName)
	ENDIF
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)// @TODO: Add is new param to node menu
ENDPROC

/// PURPOSE:
///    Adds all node menu tabs as subtitles, this only calls SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING
///    it does not start the scaleform method to set the sub headings
/// PARAMS:
///    sInst - the scaleform node menu instance
PROC _NODE_MENU_SCALEFORM_APPEND_ALL_TAB_SUBTITLES(NODE_MENU_SCALEFORM_STRUCT &sInst)
	NODE_MENU_ITEM_STRUCT sTabGroup = NODE_MENU_GET_MENU_ITEM_PROTOTYPE(sInst.sNodeMenu)
	
	// Get parent of current parent, because its children are tabs
	IF NOT NODE_MENU_ITEM_GET_PARENT(sInst.sNodeMenu.sCurrentParentItem, sTabGroup)
		EXIT
	ENDIF
	
	INT iChild = 0
	INT iMaxChild = NODE_MENU_ITEM_GET_MAX_CHILD_COUNT(sTabGroup)
	STRING strSubtitleName = ""
	NODE_MENU_ITEM_STRUCT sTab
	
	// Loop through the tabs, adding their tab names
	REPEAT iMaxChild iChild
		sTab = NODE_MENU_GET_MENU_ITEM_PROTOTYPE(sInst.sNodeMenu)
		
		// If we got the tab and it is visible, add it to the tab headings
		IF NODE_MENU_ITEM_GET_CHILD(sTabGroup, iChild, sTab)
		AND NOT NODE_MENU_ITEM_IS_NODE_HIDDEN(sTab)
			strSubtitleName = NODE_MENU_ITEM_GET_SUBTITLE(sTab)
			
			IF NOT IS_STRING_NULL(strSubtitleName)		
				IF IS_BIT_SET_ENUM(sTab.iFlags, NODE_MENU_ITEM_FLAGS_USE_LITERAL_STRING_FOR_SUBTITLE)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(strSubtitleName)
				ELSE
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strSubtitleName)
				ENDIF
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(FALSE)// @TODO: Add is new param to node menu
			ENDIF
			
			PRINTLN("[NODE_MENU][SCALEFORM] _NODE_MENU_SCALEFORM_ADD_ALL_SUB_TITLES - SET_HEADINGS: ", strSubtitleName)
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Adds all the required tabs to the scaleforms sub headings
/// PARAMS:
///    sInst - the node menu scaleform
PROC _NODE_MENU_SCALEFORM_ADD_ALL_SUB_TITLES(NODE_MENU_SCALEFORM_STRUCT &sInst)
	IF IS_BIT_SET_ENUM(sInst.sNodeMenu.sCurrentParentItem.iFlags, NODE_MENU_ITEM_FLAGS_IS_TAB)
		// A tab so add all tab headings
		_NODE_MENU_SCALEFORM_APPEND_ALL_TAB_SUBTITLES(sInst)
	ELSE
		// Not a tab so just add defined subtitle
		_NODE_MENU_SCALEFORM_APPEND_CURRENT_PARENTS_SUBTITLE(sInst)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the scaleform heading according to the current parent in the node menu
/// PARAMS:
///    sInst - the node menu scaleform
PROC _NODE_MENU_SCALEFORM_UPDATE_HEADING(NODE_MENU_SCALEFORM_STRUCT &sInst)
	STRING strHeading = "" 
	strHeading = NODE_MENU_ITEM_GET_TITLE(sInst.sNodeMenu.sCurrentParentItem)
	BOOL bUseLiteralString = IS_BIT_SET_ENUM(sInst.sNodeMenu.sCurrentParentItem.iFlags, NODE_MENU_ITEM_FLAGS_USE_LITERAL_STRING_FOR_TITLE)
				
	// If the parent didn't have a title use its display name instead
	IF IS_STRING_NULL_OR_EMPTY(strHeading) 
		strHeading = NODE_MENU_ITEM_GET_DISPLAY_NAME(sInst.sNodeMenu.sCurrentParentItem)
		bUseLiteralString = IS_BIT_SET_ENUM(sInst.sNodeMenu.sCurrentParentItem.iFlags, NODE_MENU_ITEM_FLAGS_USE_LITERAL_STRING_FOR_DISPLAY_NAME)
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(strHeading) 
		strHeading = ""
	ENDIF
	
	BEGIN_SCALEFORM_MOVIE_METHOD(NODE_MENU_SCALEFORM_GET_SCALEFORM_INDEX(sInst), "SET_HEADINGS")
		IF bUseLiteralString
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_LITERAL_STRING(strHeading)
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strHeading)
		ENDIF
		
		PRINTLN("[NODE_MENU][SCALEFORM] _NODE_MENU_SCALEFORM_UPDATE_HEADING - SET_HEADINGS: ", strHeading)
		_NODE_MENU_SCALEFORM_ADD_ALL_SUB_TITLES(sInst)
	END_SCALEFORM_MOVIE_METHOD()	
ENDPROC

/// PURPOSE:
///    Updates the subheading to highlight the current tab
/// PARAMS:
///    sInst - the node menu scaleform
PROC _NODE_MENU_SCALEFORM_UPDATE_HIGHLIGHTED_TAB(NODE_MENU_SCALEFORM_STRUCT &sInst)
	// Get parent tab group
	NODE_MENU_ITEM_STRUCT sTabGroup
	NODE_MENU_ITEM_GET_PARENT(sInst.sNodeMenu.sCurrentParentItem, sTabGroup)
	
	// Get tab counts from the tab group
	INT iHiddenTabs = NODE_MENU_ITEM_GET_MAX_HIDDEN_CHILD_COUNT(sTabGroup)
	PRINTLN("[NODE_MENU][SCALEFORM] _NODE_MENU_SCALEFORM_UPDATE_HIGHLIGHTED_TAB - iTotalTabs: ", NODE_MENU_ITEM_GET_MAX_CHILD_COUNT(sTabGroup), " iHiddenTabs: ", iHiddenTabs)
	
	/// The node menu treats tabs as existing even if they are hidden, so that their indexs
	/// stay consistent. But because they were never added to the scaleform we have to correct the
	/// index for the scaleform heading
	INT iTabIndex = NODE_MENU_ITEM_GET_ID(sInst.sNodeMenu.sCurrentParentItem) 
	iTabIndex = IMAX(0, iTabIndex - iHiddenTabs)
	
	BEGIN_SCALEFORM_MOVIE_METHOD(NODE_MENU_SCALEFORM_GET_SCALEFORM_INDEX(sInst), "SET_SUBHEADING_HIGHLIGHT")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTabIndex)
	END_SCALEFORM_MOVIE_METHOD()
	PRINTLN("[NODE_MENU][SCALEFORM] _NODE_MENU_SCALEFORM_UPDATE_HIGHLIGHTED_TAB - SET_SUBHEADING_HIGHLIGHT: ", iTabIndex)
ENDPROC

/// PURPOSE:
///    Requests the current selection on the scaleform, it will not make a request
///    if one is already pending
/// PARAMS:
///    sInst - the node menu instance
PROC NODE_MENU_SCALEFORM_REQUEST_CURRENT_SELECTION(NODE_MENU_SCALEFORM_STRUCT &sInst)
	// Already requesting don't request again
	IF sInst.bHighlightRequested
		EXIT
	ENDIF
	
	SCALEFORM_INDEX scaleformMovie = NODE_MENU_SCALEFORM_GET_SCALEFORM_INDEX(sInst)
	BEGIN_SCALEFORM_MOVIE_METHOD(scaleformMovie, "GET_CURRENT_SELECTION")
	sInst.sriHighlightRequest = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
	sInst.bHighlightRequested = TRUE
	
	PRINTLN("[NODE_MENU][SCALEFORM][SELECTION] NODE_MENU_SCALEFORM_REQUEST_CURRENT_SELECTION  - Requesting")
	DEBUG_PRINTCALLSTACK()
ENDPROC

/// PURPOSE:
///    Processes requesting the current selection from the scaleform menu
///    scaleform is in a seperate thread so we have to wait a few frames for return values
///    this waits for the value to be ready then updates the node menu to match what is selected in
///    the scaleform
/// PARAMS:
///    sInst - the scaleform node menu instance
/// RETURNS:
///    TRUE if the request is finished or if there are no pending requests
FUNC BOOL NODE_MENU_SCALEFORM_PROCESS_CURRENT_SELECTION_REQUEST(NODE_MENU_SCALEFORM_STRUCT &sInst)
	IF NOT sInst.bHighlightRequested
		RETURN TRUE
	ENDIF
	
	IF NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(sInst.sriHighlightRequest)
		PRINTLN("[NODE_MENU][SCALEFORM][SELECTION] NODE_MENU_SCALEFORM_PROCESS_CURRENT_SELECTION_REQUEST  - Not ready")
		RETURN FALSE	
	ENDIF
	
	INT iHighlight = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(sInst.sriHighlightRequest)

	IF iHighlight > -1
		BOOL bNewHighlight = iHighlight != NODE_MENU_GET_CURRENT_HIGHLIGHTED_ITEM_INDEX(sInst.sNodeMenu)
		IF sInst.onNavigationProcessed != NULL
			CALL sInst.onNavigationProcessed(bNewHighlight, sInst.eInputEvent)
		ENDIF
		
		// Update the node menu with the retrieved value
		NODE_MENU_SET_HIGHLIGHTED_MENU_ITEM(sInst.sNodeMenu, iHighlight)
		PRINTLN("[NODE_MENU][SCALEFORM][SELECTION] NODE_MENU_SCALEFORM_PROCESS_CURRENT_SELECTION_REQUEST  - setting node menu selection to: ", iHighlight)
	ENDIF
	
	sInst.bHighlightRequested = FALSE
	PRINTLN("[NODE_MENU][SCALEFORM][SELECTION] NODE_MENU_SCALEFORM_PROCESS_CURRENT_SELECTION_REQUEST  - Retrieved: ", iHighlight)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Updates the scaleform to highlight what the node menu thinks should be highlighted
/// PARAMS:
///    sInst - the node menu scaleform
PROC _NODE_MENU_SCALEFORM_UPDATE_HIGHLIGHTED_ITEM(NODE_MENU_SCALEFORM_STRUCT &sInst)
	// Make sure the scaleform highlights what the node menu thinks is currently highlighted
	INT iCurrentNodeMenuHighlighted = NODE_MENU_GET_CURRENT_HIGHLIGHTED_ITEM_INDEX(sInst.sNodeMenu)

	// Only try to set the scaleforms index if we have just entered a sub menu
	IF NOT sInst.bHighlightRequested	
		NODE_MENU_SCALEFORM_SET_HIGHLIGHT_ON_SCALEFORM(sInst, iCurrentNodeMenuHighlighted)
		sInst.iLastFrameHighlight = iCurrentNodeMenuHighlighted
		PRINTLN("[NODE_MENU][SCALEFORM][SELECTION] _NODE_MENU_SCALEFORM_UPDATE_HIGHLIGHTED_ITEM - setting scaleform highlighted to: ", iCurrentNodeMenuHighlighted)
	ENDIF
ENDPROC

/// PURPOSE:
///    Tells the scaleform to show the current screen type
/// PARAMS:
///    sInst - the node menu scaleform 
PROC _NODE_MENU_SCALEFORM_SHOW_CURRENT_SCREEN(NODE_MENU_SCALEFORM_STRUCT &sInst)
	INT iScreen = NODE_MENU_SCALEFORM_GET_CURRENT_SCREEN_TYPE(sInst)
	BEGIN_SCALEFORM_MOVIE_METHOD(NODE_MENU_SCALEFORM_GET_SCALEFORM_INDEX(sInst), "SHOW_SCREEN")
	SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iScreen)
	END_SCALEFORM_MOVIE_METHOD()
	PRINTLN("[NODE_MENU][SCALEFORM] _NODE_MENU_SCALEFORM_SHOW_CURRENT_SCREEN - SHOW_SCREEN: ", iScreen)
ENDPROC

/// PURPOSE:
///    Refreshes the menus help keys to show the help keys of the current highlighted menu item
///    and submenu
/// PARAMS:
///    sInst - the node menu scaleform
PROC NODE_MENU_SCALEFORM_REFRESH_HELP_KEYS(NODE_MENU_SCALEFORM_STRUCT &sInst)
	_NODE_MENU_SCALEFORM_CLEAR_HELP_KEYS(sInst)
	NODE_MENU_ADD_HELP_KEYS(sInst.sNodeMenu)
ENDPROC

/// PURPOSE:
///    Builds the scaleform
/// PARAMS:
///    sInst - the node menu scaleform 
/// RETURNS:
///    TRUE if the menu has finished building
FUNC BOOL NODE_MENU_SCALEFORM_BUILD_SCALEFORM(NODE_MENU_SCALEFORM_STRUCT &sInst)
	// We need to wait for the scaleform to finish the seleciton request before we rebuild the menu
	IF sInst.bHighlightRequested
		NODE_MENU_SCALEFORM_PROCESS_CURRENT_SELECTION_REQUEST(sInst)
		RETURN FALSE
	ENDIF
	
	PRINTLN("[NODE_MENU][SCALEFORM] ")
	PRINTLN("[NODE_MENU][SCALEFORM] NODE_MENU_SCALEFORM_BUILD_SCALEFORM - BUILDING ----------------")
	
	// Add child menu items
	PRINTLN("[NODE_MENU][SCALEFORM] NODE_MENU_SCALEFORM_BUILD_SCALEFORM - Adding items ----------------")
	NODE_MENU_UI_ADD_CHILD_MENU_ITEMS(sInst.sNodeMenu)
	PRINTLN("[NODE_MENU][SCALEFORM] NODE_MENU_SCALEFORM_BUILD_SCALEFORM - Finished Adding items ----------------")
	
	// Add the help keys assigned by the current selected child
	NODE_MENU_SCALEFORM_REFRESH_HELP_KEYS(sInst)
	
	// Update headings and current screen
	_NODE_MENU_SCALEFORM_UPDATE_HEADING(sInst)
	_NODE_MENU_SCALEFORM_SHOW_CURRENT_SCREEN(sInst)
	
	_NODE_MENU_SCALEFORM_UPDATE_HIGHLIGHTED_ITEM(sInst)
	NODE_MENU_SCALEFORM_PROCESS_CURRENT_SELECTION_REQUEST(sInst)
	_NODE_MENU_SCALEFORM_UPDATE_HIGHLIGHTED_TAB(sInst)
	
	PRINTLN("[NODE_MENU][SCALEFORM] NODE_MENU_SCALEFORM_BUILD_SCALEFORM - FINISHED BUILDING ----------------")
	PRINTLN("[NODE_MENU][SCALEFORM] ")
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Loads the scaleform of the given name
/// PARAMS:
///    sInst - the node menu scaleform
///    strScaleformName - the name of the scaleform to load
/// RETURNS:
///    TRUE when finished loading
FUNC BOOL NODE_MENU_SCALEFORM_LOAD_SCALEFORM(SCALEFORM_INDEX &sfMovie, STRING strScaleformName)
	SCALEFORM_INDEX siScaleform = REQUEST_SCALEFORM_MOVIE(strScaleformName)
	NODE_MENU_SCALEFORM_SET_SCALEFORM_INDEX(sfMovie, siScaleform)
	BOOL bFinishedLoading = HAS_SCALEFORM_MOVIE_LOADED(siScaleform)	
	PRINTLN("[NODE_MENU][SCALEFORM] NODE_MENU_SCALEFORM_LOAD_SCALEFORM - Loaded: ", bFinishedLoading)
	RETURN bFinishedLoading
ENDFUNC

/// PURPOSE:
///    Cleans up the current scaleform
/// PARAMS:
///    sInst - the node menu scaleform
PROC NODE_MENU_SCALEFORM_CLEANUP(NODE_MENU_SCALEFORM_STRUCT &sInst)
	PRINTLN("[NODE_MENU][SCALEFORM] NODE_MENU_SCALEFORM_CLEANUP - Called")
	RESET_SCALEFORM_INSTRUCTIONAL_BUTTON(sInst.sButtons)
	SCALEFORM_INDEX siScaleform = NODE_MENU_SCALEFORM_GET_SCALEFORM_INDEX(sInst)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(siScaleform)
ENDPROC

#IF IS_DEBUG_BUILD
DEBUGONLY PROC DEBUG_DRAW_NODE_MENU_SCALEFORM_DATA(NODE_MENU_SCALEFORM_STRUCT &sInst)
	IF NOT GET_COMMANDLINE_PARAM_EXISTS("sc_NodeMenuScaleformDebug")
		EXIT
	ENDIF
	
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)	
	VECTOR vPos = << 0.6, 0.2, 0.0>>
	//VECTOR vOffset = << 0.0, 0.025, 0.0>>

	TEXT_LABEL_63 tlNodeMenuIndex = "Node Menu Index: "
	tlNodeMenuIndex += NODE_MENU_GET_CURRENT_HIGHLIGHTED_ITEM_INDEX(sInst.sNodeMenu)
	DRAW_DEBUG_TEXT_2D(Get_String_From_TextLabel(tlNodeMenuIndex), vPos, 0, 255, 0)
ENDPROC
#ENDIF

/// PURPOSE:
///    Draws the scaleform
/// PARAMS:
///    sInst - the node menu scaleform
PROC NODE_MENU_SCALEFORM_DRAW_SCALEFORM(NODE_MENU_SCALEFORM_STRUCT &sInst)
	// Just to make sure we are always processing selection requests
	NODE_MENU_SCALEFORM_PROCESS_CURRENT_SELECTION_REQUEST(sInst)
	
	// Draw the scaleform
	SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(NODE_MENU_SCALEFORM_GET_SCALEFORM_INDEX(sInst), 255, 255, 255, 0)
	
	IF NOT NODE_MENU_BS_IS_FLAG_SET(sInst.sNodeMenu, NODE_MENU_BS_AWAITING_TEXT_INPUT_CONFIRMATION)
		// Draw help keys on top
		SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sInst.sButtons, 1.0)
		SPRITE_PLACEMENT spritePlacement = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(sInst.sfButtons, spritePlacement, sInst.sButtons, SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sInst.sButtons))
	ENDIF
	
	#IF IS_DEBUG_BUILD
	DEBUG_DRAW_NODE_MENU_SCALEFORM_DATA(sInst)
	#ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the analogue axis value is neutral taking into account the deadzone
/// PARAMS:
///    iStickAxisValue - the analogue stick axis value to checks
/// RETURNS:
///    TRUE if the snalogue stick axis is in a neutral position
FUNC BOOL _NODE_MENU_SCALEFORM_IS_ANALOGUE_STICK_AXIS_NEUTRAL(INT iStickAxisValue)
	IF iStickAxisValue > ANALOGUE_STICK_NEUTRAL + ANALOGUE_STICK_DEADZONE
		RETURN FALSE
	ENDIF
	
	IF iStickAxisValue < ANALOGUE_STICK_NEUTRAL - ANALOGUE_STICK_DEADZONE
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Handles navigating the scaleform from the 
/// PARAMS:
///    sInst - the node menu instance
FUNC NODE_MENU_BASE_INPUT_EVENT _NODE_MENU_SCALFORM_ANALOGUE_NAVIGATION(NODE_MENU_SCALEFORM_STRUCT &sInst)
	NODE_MENU_SCALEFORM_PROCESS_CURRENT_SELECTION_REQUEST(sInst)
	SCALEFORM_INDEX siScaleform = NODE_MENU_SCALEFORM_GET_SCALEFORM_INDEX(sInst)
	CONTROL_ACTION eStickX = INPUT_FRONTEND_AXIS_X
	CONTROL_ACTION eStickY = INPUT_FRONTEND_AXIS_Y
	
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, eStickX) 
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, eStickY)
	
	INT x = GET_CONTROL_VALUE(FRONTEND_CONTROL, eStickX)// Will need to switch if south paw
	INT y = GET_CONTROL_VALUE(FRONTEND_CONTROL, eStickY)
	
	BOOL bLastXAxisNeutral = _NODE_MENU_SCALEFORM_IS_ANALOGUE_STICK_AXIS_NEUTRAL(sInst.vLeftJoystick.x)
	BOOL bLastYAxisNeutral = _NODE_MENU_SCALEFORM_IS_ANALOGUE_STICK_AXIS_NEUTRAL(sInst.vLeftJoystick.y)
	BOOL bCurrentXAxisNeutral = _NODE_MENU_SCALEFORM_IS_ANALOGUE_STICK_AXIS_NEUTRAL(x)
	BOOL bCurrentYAxisNeutral = _NODE_MENU_SCALEFORM_IS_ANALOGUE_STICK_AXIS_NEUTRAL(y)
	
	NODE_MENU_BASE_INPUT_EVENT eInputEvent = NODE_MENU_BASE_INPUT_EVENT_NONE
	
	IF bCurrentXAxisNeutral OR NOT bLastXAxisNeutral
	OR bCurrentYAxisNeutral OR NOT bLastYAxisNeutral		
		// Only do delay if an axis is held
		IF NOT bLastXAxisNeutral OR NOT bLastYAxisNeutral
			// If delay hasn't expired exit, but only if we have started the delay timer
			IF NOT HAS_NET_TIMER_EXPIRED(sInst.sLeftStickDelay, _ciNODE_MENU_SCALEFORM_LEFT_STICK_DELAY_TIME)
			AND HAS_NET_TIMER_STARTED(sInst.sLeftStickDelay)
				// Remember last frame input
				sInst.vLeftJoystick.x = x
				sInst.vLeftJoystick.y = y
				RETURN NODE_MENU_BASE_INPUT_EVENT_NONE
			ENDIF
		ENDIF
		
		IF x > ANALOGUE_STICK_NEUTRAL + ANALOGUE_STICK_DEADZONE
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siScaleform, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_RIGHT)))
			PRINTLN("[NODE_MENU][SCALEFORM] _NODE_MENU_SCALFORM_ANALOGUE_NAVIGATION - INPUT_FRONTEND_RIGHT pressed")
			NODE_MENU_SCALEFORM_REQUEST_CURRENT_SELECTION(sInst)
			eInputEvent = NODE_MENU_BASE_INPUT_EVENT_MOVED_RIGHT
			
		ELIF x < ANALOGUE_STICK_NEUTRAL - ANALOGUE_STICK_DEADZONE
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siScaleform, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_LEFT)))
			PRINTLN("[NODE_MENU][SCALEFORM] _NODE_MENU_SCALFORM_ANALOGUE_NAVIGATION - INPUT_FRONTEND_LEFT pressed")
			NODE_MENU_SCALEFORM_REQUEST_CURRENT_SELECTION(sInst)
			eInputEvent = NODE_MENU_BASE_INPUT_EVENT_MOVED_LEFT
		ENDIF
		
		IF y > ANALOGUE_STICK_NEUTRAL + ANALOGUE_STICK_DEADZONE
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siScaleform, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_DOWN)))
			NODE_MENU_SCALEFORM_REQUEST_CURRENT_SELECTION(sInst)
			PRINTLN("[NODE_MENU][SCALEFORM] _NODE_MENU_SCALFORM_ANALOGUE_NAVIGATION - INPUT_FRONTEND_DOWN pressed")
			eInputEvent = NODE_MENU_BASE_INPUT_EVENT_MOVED_DOWN
		ELIF y < ANALOGUE_STICK_NEUTRAL - ANALOGUE_STICK_DEADZONE
			CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siScaleform, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_UP)))
			PRINTLN("[NODE_MENU][SCALEFORM] _NODE_MENU_SCALFORM_ANALOGUE_NAVIGATION - INPUT_FRONTEND_UP pressed")
			NODE_MENU_SCALEFORM_REQUEST_CURRENT_SELECTION(sInst)
			eInputEvent = NODE_MENU_BASE_INPUT_EVENT_MOVED_UP
		ENDIF	
		
		RESET_NET_TIMER(sInst.sLeftStickDelay)
		START_NET_TIMER(sInst.sLeftStickDelay)
	ENDIF
	
	// Remember last frame input
	sInst.vLeftJoystick.x = x
	sInst.vLeftJoystick.y = y
	RETURN eInputEvent
ENDFUNC

/// PURPOSE:
///    The standard base navigation controls for navigating a scaleform node menu
///    making sure the inputs are passed to the node menu itself in case it has special functionality like
///    scroll bars
/// PARAMS:
///    sInst - the scaleform node menu instance
/// RETURNS:
///    TRUE if the a navigation button was pressed
FUNC NODE_MENU_BASE_INPUT_EVENT NODE_MENU_SCALEFORM_BASE_NAVIGATION_CONTROLS(NODE_MENU_SCALEFORM_STRUCT &sInst)
	NODE_MENU_SCALEFORM_PROCESS_CURRENT_SELECTION_REQUEST(sInst)
	SCALEFORM_INDEX siScaleform = NODE_MENU_SCALEFORM_GET_SCALEFORM_INDEX(sInst)
	
	IF IS_WARNING_MESSAGE_ACTIVE()
	OR IS_SYSTEM_UI_BEING_DISPLAYED()
	OR (GET_PAUSE_MENU_STATE() != PM_INACTIVE)
		PRINTLN("[NODE_MENU][SCALEFORM] NODE_MENU_SCALEFORM_BASE_CONTROLS - IS_WARNING_MESSAGE_ACTIVE")
		RETURN NODE_MENU_BASE_INPUT_EVENT_NONE
	ENDIF
	
	
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siScaleform, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_UP)))
		PRINTLN("[NODE_MENU][SCALEFORM] NODE_MENU_SCALEFORM_BASE_CONTROLS - INPUT_FRONTEND_UP pressed")
		NODE_MENU_SCALEFORM_REQUEST_CURRENT_SELECTION(sInst)
		sInst.eInputEvent = NODE_MENU_BASE_INPUT_EVENT_MOVED_UP
		RETURN NODE_MENU_BASE_INPUT_EVENT_MOVED_UP
	ENDIF
	
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siScaleform, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_DOWN)))
		PRINTLN("[NODE_MENU][SCALEFORM] NODE_MENU_SCALEFORM_BASE_CONTROLS - INPUT_FRONTEND_DOWN pressed")
		NODE_MENU_SCALEFORM_REQUEST_CURRENT_SELECTION(sInst)
		sInst.eInputEvent = NODE_MENU_BASE_INPUT_EVENT_MOVED_DOWN
		RETURN NODE_MENU_BASE_INPUT_EVENT_MOVED_DOWN
	ENDIF
	
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siScaleform, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_LEFT)))
		NODE_MENU_SCALEFORM_REQUEST_CURRENT_SELECTION(sInst)
		PRINTLN("[NODE_MENU][SCALEFORM] NODE_MENU_SCALEFORM_BASE_CONTROLS - INPUT_FRONTEND_LEFT pressed")
		sInst.eInputEvent = NODE_MENU_BASE_INPUT_EVENT_MOVED_LEFT
		RETURN NODE_MENU_BASE_INPUT_EVENT_MOVED_LEFT
	ENDIF
	
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
		CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(siScaleform, "SET_INPUT_EVENT", TO_FLOAT(ENUM_TO_INT(INPUT_FRONTEND_RIGHT)))
		NODE_MENU_SCALEFORM_REQUEST_CURRENT_SELECTION(sInst)
		PRINTLN("[NODE_MENU][SCALEFORM] NODE_MENU_SCALEFORM_BASE_CONTROLS - INPUT_FRONTEND_RIGHT pressed")
		sInst.eInputEvent = NODE_MENU_BASE_INPUT_EVENT_MOVED_RIGHT
		RETURN NODE_MENU_BASE_INPUT_EVENT_MOVED_RIGHT
	ENDIF
	
	
	RETURN _NODE_MENU_SCALFORM_ANALOGUE_NAVIGATION(sInst)
ENDFUNC

/// PURPOSE:
///    The base controls for the menu, this includes navigation and pressing buttons
/// PARAMS:
///    sInst - the node menu scaleform
/// RETURNS:
///    TRUE if a button has been pressed and the menu needs rebuilding
FUNC BOOL NODE_MENU_SCALEFORM_BASE_CONTROLS(NODE_MENU_SCALEFORM_STRUCT &sInst)
	NODE_MENU_SCALEFORM_PROCESS_CURRENT_SELECTION_REQUEST(sInst)
	
	IF NODE_MENU_SCALEFORM_BASE_NAVIGATION_CONTROLS(sInst) != NODE_MENU_BASE_INPUT_EVENT_NONE
		RETURN FALSE
	ENDIF
	
	IF NODE_MENU_BASE_INPUT_PROCESS_TABBING(sInst.sNodeMenu) != NODE_MENU_BASE_INPUT_EVENT_NONE
		RETURN TRUE
	ENDIF
	
	IF NODE_MENU_BASE_INPUT_PROCESS_BACK(sInst.sNodeMenu) != NODE_MENU_BASE_INPUT_EVENT_NONE
		RETURN TRUE
	ENDIF
	
	IF NODE_MENU_BASE_INPUT_PROCESS_CHANGE_VALUE(sInst.sNodeMenu) != NODE_MENU_BASE_INPUT_EVENT_NONE
		RETURN TRUE
	ENDIF

 	// If we have successfully entered a node on selection, rebuild the menu as it has children that need displaying
	RETURN NODE_MENU_BASE_INPUT_PROCESS_SELECTING(sInst.sNodeMenu) != NODE_MENU_BASE_INPUT_EVENT_NONE
ENDFUNC

/// PURPOSE:
///    Creates a scaleform node menu item with its default behaviour
/// RETURNS:
///    The default scaleform menu item behaviour
FUNC NODE_MENU_ITEM_STRUCT CREATE_SCALEFORM_NODE_MENU_ITEM()
	NODE_MENU_ITEM_STRUCT sMenuItem
	// Set any default scaleform node behaviour here
	PRINTLN("[NODE_MENU][SCALEFORM] Used prototype")
	RETURN sMenuItem
ENDFUNC

STRUCT SCALEFORM_NODE_MENU_METHOD_WRAPPERS
	T_NODE_MENU_ITEM_RETURN fpMenuItemPrototype
	T_NODE_MENU_EVENT_CONDITION_SELF fpBuildMethod
	T_NODE_MENU_EVENT fpDrawMethod
	T_NODE_MENU_EVENT_CONDITION fpLoadAssetsMethod
	T_NODE_MENU_EVENT fpCleanupAssetsMethod
ENDSTRUCT

/// PURPOSE:
///    Creates the scaleform node menu
/// PARAMS:
///    sRoot - the starting parent of the menu
/// RETURNS:
///    A new scaleform node menu struct
FUNC NODE_MENU_STRUCT CREATE_SCALEFORM_NODE_MENU(NODE_MENU_ITEM_STRUCT &sRoot, SCALEFORM_NODE_MENU_METHOD_WRAPPERS &sWrappedMethods, INT iStartingHighlight = 0)
	NODE_MENU_STRUCT sMenu
	
	NODE_MENU_SET_MENU_ITEM_PROTOTYPE(sMenu, sWrappedMethods.fpMenuItemPrototype)
	NODE_MENU_SET_BUILD_METHOD(sMenu, sWrappedMethods.fpBuildMethod)
	NODE_MENU_SET_DRAW_METHOD(sMenu, sWrappedMethods.fpDrawMethod)
	NODE_MENU_SET_LOAD_ASSETS(sMenu, sWrappedMethods.fpLoadAssetsMethod)
	NODE_MENU_SET_CLEAN_UP_ASSETS(sMenu, sWrappedMethods.fpCleanupAssetsMethod)	
	
	NODE_MENU_SET_ROOT_NODE(sMenu, sRoot)
	NODE_MENU_JUMP_TO_SUBMENU(sMenu, sRoot, iStartingHighlight, FALSE)
	RETURN sMenu
ENDFUNC
