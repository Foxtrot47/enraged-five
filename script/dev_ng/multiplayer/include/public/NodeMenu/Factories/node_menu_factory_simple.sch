
USING "Core\node_menu_struct.sch"
USING "Core\node_menu_ui.sch"
USING "Core\node_menu_input.sch"
USING "Core\node_menu_item.sch"
USING "Core\node_menu_flags.sch"
USING "Factories\node_menu_factory_shared.sch"

/// PURPOSE:
///    Sets up the standard layout of the menu, adding the header and setting justification
PROC _NODE_MENU_SIMPLE_SETUP_MENU_LAYOUT(NODE_MENU_STRUCT &sInst)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] _NODE_MENU_UI_SETUP_MENU_LAYOUT - Called")	
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
    SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	NODE_MENU_UI_DRAW_HEADER(sInst)
	CPRINTLN(DEBUG_NODE_MENU, "[NODE_MENU] _NODE_MENU_UI_SETUP_MENU_LAYOUT - Finished")
ENDPROC

/// PURPOSE:
///    Builds a simple node menu according to its current menu configuration
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    TRUE is the build has finished
FUNC BOOL NODE_MENU_SIMPLE_BUILD_MENU(NODE_MENU_STRUCT &sInst)	
	REMOVE_MENU_HELP_KEYS()
	CLEAR_MENU_DATA(TRUE)
	_NODE_MENU_SIMPLE_SETUP_MENU_LAYOUT(sInst)
	_NODE_MENU_UI_UPDATE_TITLE(sInst)	
	/// Don't make items toggleable, this is up to the item configs highlight update
	SET_MENU_ITEM_TOGGLEABLE()	
	NODE_MENU_UI_ADD_CHILD_MENU_ITEMS(sInst)		
	NODE_MENU_ADD_HELP_KEYS(sInst)
	NODE_MENU_SET_HIGHLIGHTED_MENU_ITEM(sInst, sInst.iCurrentSelection)
	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Draws a simple node menu, mainly acts as a wrapped for the old DRAW_MENU so that its signature
///    matches the one node menu needs
PROC NODE_MENU_SIMPLE_DRAW_METHOD()
	DRAW_MENU()
ENDPROC

/// PURPOSE:
///    The simple menu method for setting which item is highlighted
/// PARAMS:
///    iSelection - the index of the menu item to highlight 
PROC NODE_MENU_SIMPLE_HIGHLIGHT_METHOD(INT iSelection)
	SET_CURRENT_MENU_ITEM(iSelection, TRUE)	
ENDPROC

/// PURPOSE:
///    The simple menu method for setting a description
/// PARAMS:
///    strDesc - the description label to set
PROC NODE_MENU_SIMPLE_DESCRIPTION_METHOD(STRING strDesc)
	SET_CURRENT_MENU_ITEM_DESCRIPTION(strDesc)
ENDPROC

///------------------------------
///    SIMPLE MENU INPUT
///------------------------------    

/// PURPOSE:
///    Checks if the mouse cursor is on the node menu
/// RETURNS:
///    TRUE if the mouse cursor is on the menu
FUNC BOOL NODE_MENU_IS_MOUSE_CURSOR_ON_MENU_ITEM()
	RETURN g_iMenuCursorItem > MENU_CURSOR_NO_ITEM
ENDFUNC

/// PURPOSE:
///    Checks if the mouse cursor is on top of the currently highlighted item
///    in the node menu
/// PARAMS:
///    sInst - the node menu instance
/// RETURNS:
///    TRUE if the cursor is on the node menu
FUNC BOOL NODE_MENU_IS_MOUSE_CURSOR_ON_CURRENTLY_HIGHLIGHTED_ITEM(NODE_MENU_STRUCT &sInst)
	RETURN g_iMenuCursorItem = sInst.iCurrentSelection
ENDFUNC

/// PURPOSE:
///    Updates the currently slected node item index to the index that the cursor is currently 
///    highlighting according to the menu api
/// PARAMS:
///    sInst - the node menu instance
PROC _NODE_MENU_INPUT_UPDATE_CURRENT_SELECTED_TO_MOUSE_CURSOR_SELECTED(NODE_MENU_STRUCT &sInst)
	// May need to take into account hidden items
	NODE_MENU_SET_HIGHLIGHTED_MENU_ITEM(sInst, g_iMenuCursorItem)
ENDPROC

/// PURPOSE:
///    Checks if the mouse cursor is on the scroll up button
///    according to the menu api
/// RETURNS:
///    TRUE if on the scroll up button
FUNC BOOL NODE_MENU_IS_MOUSE_CURSOR_ON_SCROLL_UP_BUTTON()
	RETURN g_iMenuCursorItem = MENU_CURSOR_SCROLL_UP
ENDFUNC

/// PURPOSE:
///    Checks if the mouse cursor is on the scroll down button
///    according to the menu api
/// RETURNS:
///    TRUE if on the scroll up button
FUNC BOOL NODE_MENU_IS_MOUSE_CURSOR_ON_SCROLL_DOWN_BUTTON()
	RETURN g_iMenuCursorItem = MENU_CURSOR_SCROLL_DOWN
ENDFUNC

FUNC BOOL NODE_MENU_IS_MOUSE_CURSOR_ON_MENU()
	RETURN NODE_MENU_IS_MOUSE_CURSOR_ON_MENU_ITEM()
	OR NODE_MENU_IS_MOUSE_CURSOR_ON_SCROLL_DOWN_BUTTON()
	OR NODE_MENU_IS_MOUSE_CURSOR_ON_SCROLL_UP_BUTTON()
ENDFUNC

/// PURPOSE:
///    Checks if the scroll up button has been pressed
/// RETURNS:
///    TRUE if the scroll up button has been pressed
FUNC BOOL NODE_MENU_HAS_SCROLL_UP_BUTTON_BEEN_PRESSED()
	RETURN IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
	AND NODE_MENU_IS_MOUSE_CURSOR_ON_SCROLL_UP_BUTTON()
ENDFUNC

/// PURPOSE:
///    Checks if the scroll down button has been pressed
/// RETURNS:
///    TRUE if the scroll down button has been pressed
FUNC BOOL NODE_MENU_HAS_SCROLL_DOWN_BUTTON_BEEN_PRESSED()
	RETURN IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT) 
	AND NODE_MENU_IS_MOUSE_CURSOR_ON_SCROLL_DOWN_BUTTON()
ENDFUNC

/// PURPOSE:
///    Updates the selection of a menu items arrow buttons
///    using the mouse cursor
/// PARAMS:
///    sInst - the node menu instance
PROC _NODE_MENU_SIMPLE_UPDATE_CURSOR_ARROW_BUTTONS(NODE_MENU_STRUCT &sInst)
	IF NOT NODE_MENU_IS_MOUSE_CURSOR_ON_MENU_ITEM()
		EXIT
	ENDIF
	
	IF NOT IS_CURSOR_ACCEPT_JUST_PRESSED()
		EXIT
	ENDIF
	
	IF NOT NODE_MENU_IS_MOUSE_CURSOR_ON_CURRENTLY_HIGHLIGHTED_ITEM(sInst)
		EXIT
	ENDIF
	
	// The following code is pulled from the original pi menu
	CONST_FLOAT	LEFT_SIDE_ACCEPT_SIZE 0.045
	INT iIncrement = GET_CURSOR_MENU_ITEM_VALUE_CHANGE(LEFT_SIDE_ACCEPT_SIZE)
	
	IF iIncrement > 0
		NODE_MENU_TRIGGER_VALUE_NEXT_ON_HIGHLIGHTED_NODE(sInst)
	ELIF iIncrement < 0
		NODE_MENU_TRIGGER_VALUE_PREVIOUS_ON_HIGHLIGHTED_NODE(sInst)
	ELIF iIncrement = -999// value has been clicked so count it as a select
		NODE_MENU_ENTER_HIGHLIGHTED_NODE(sInst)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the cursor selecting a menu item
///    if the menu is clicked off of close the menu
/// PARAMS:
///    sInst - 
PROC _NODE_MENU_SIMPLE_UPDATE_CURSOR_SELECT(NODE_MENU_STRUCT &sInst)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
    ENABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
		
	IF NOT IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		EXIT
	ENDIF
	
	IF NOT NODE_MENU_IS_MOUSE_CURSOR_ON_MENU()
		/// @TODO: we need a flag check to enable/disable this
		/// e.g a menu item that has camera dragging controls wouldn't 
		/// want to quit
		NODE_MENU_BS_SET_FLAG(sInst, NODE_MENU_BS_CLOSE_MENU)
		EXIT
	ENDIF
	
	IF NODE_MENU_IS_MOUSE_CURSOR_ON_CURRENTLY_HIGHLIGHTED_ITEM(sInst)
		NODE_MENU_ENTER_HIGHLIGHTED_NODE(sInst)
	ELSE
		_NODE_MENU_INPUT_UPDATE_CURRENT_SELECTED_TO_MOUSE_CURSOR_SELECTED(sInst)
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the menus scrolling behaviour for pc controls
///    including scroll wheel and scroll button detection
/// PARAMS:
///    sInst - the node menu instance
PROC _NODE_MENU_SIMPLE_UPDATE_SCROLL(NODE_MENU_STRUCT &sInst)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
    SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
	BOOL bCursorOnMenu = NODE_MENU_IS_MOUSE_CURSOR_ON_MENU_ITEM()
	
	IF NODE_MENU_HAS_SCROLL_UP_BUTTON_BEEN_PRESSED()
	OR (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP) AND bCursorOnMenu)
		NODE_MENU_INCREMENT_CURRENT_SELECTION(sInst, -1)
		EXIT
	ENDIF
	
	IF NODE_MENU_HAS_SCROLL_DOWN_BUTTON_BEEN_PRESSED()
	OR (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN) AND bCursorOnMenu)
		NODE_MENU_INCREMENT_CURRENT_SELECTION(sInst, 1)
		EXIT
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates the menu going back when the cursor back button is pressed
/// PARAMS:
///    sInst - the node menu instance 
PROC _NODE_MENU_SIMPLE_UPDATE_CURSOR_BACK(NODE_MENU_STRUCT &sInst)
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
	
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		NODE_MENU_TRIGGER_BACK_OUT_OF_CURRENT_NODE(sInst)
	ENDIF
ENDPROC

/// PURPOSE:
///    Shows the cursor with the correct style for a simple node menu
PROC _NODE_MENU_SIMPLE_SHOW_CURSOR()
	SET_MOUSE_CURSOR_THIS_FRAME()
	SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
ENDPROC

/// PURPOSE:
///    Updates the node menu according to cursor controls
/// PARAMS:
///    sInst - the node menu instance
PROC NODE_MENU_SIMPLE_CURSOR_CONTROLS(NODE_MENU_STRUCT &sInst)
	IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		EXIT
	ENDIF
	
	SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_X)
    SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_Y)
	HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()		
	_NODE_MENU_SIMPLE_SHOW_CURSOR()
	_NODE_MENU_SIMPLE_UPDATE_CURSOR_ARROW_BUTTONS(sInst)
	_NODE_MENU_SIMPLE_UPDATE_CURSOR_SELECT(sInst)
	_NODE_MENU_SIMPLE_UPDATE_SCROLL(sInst)
	_NODE_MENU_SIMPLE_UPDATE_CURSOR_BACK(sInst)
ENDPROC

/// PURPOSE:
///    Adds the standard A to select and B to go back help keys to the menu
/// PARAMS:
///    sMenuItem - the current highlighted menu item
/// RETURNS:
///    TRUE when added
FUNC BOOL NODE_MENU_SIMPLE_BASE_HELP_KEYS(NODE_MENU_ITEM_STRUCT &sMenuItem)
	ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_CANCEL, "PIM_CEXI")
	
	IF NODE_MENU_ITEM_IS_NODE_SELECTABLE(sMenuItem)
		ADD_MENU_HELP_KEY_INPUT(INPUT_FRONTEND_ACCEPT, "PIM_CSEL")
	ENDIF
	
	RETURN TRUE
ENDFUNC

///------------------------------------------
///    SIMPLE MENU DEFAULT ITEM BEHAVIOURS
///------------------------------------------    

/// PURPOSE:
///    Default on add behaviour for a simple node menu item, it
///    adds the item as an item with it sgiven name
/// PARAMS:
///    sMenuItem - the menu item being added
///    iParamRow - the row the item is being inserted into
PROC NODE_MENU_ITEM_SIMPLE_ON_ADD_EVENT(NODE_MENU_ITEM_STRUCT &sMenuItem, INT iParamRow)
	ADD_MENU_ITEM_TEXT(iParamRow, NODE_MENU_ITEM_GET_DISPLAY_NAME(sMenuItem))
	PRINTLN("[NODE_MENU][SIMPLE] Used prototype on add event")
ENDPROC

/// PURPOSE:
///    Basic input for navigating a simple node menu including both PC and console controls
/// PARAMS:
///    sNodeMenu - the node menu instance
///    sMenuItem - the menu item currently highlighted
/// RETURNS:
///    TRUE if a controller bitton has been pressed
FUNC BOOL NODE_MENU_SIMPLE_BASE_CONTROLS(NODE_MENU_STRUCT &sNodeMenu, NODE_MENU_ITEM_STRUCT &sMenuItem)
	UNUSED_PARAMETER(sMenuItem)
	NODE_MENU_SIMPLE_CURSOR_CONTROLS(sNodeMenu)	
	RETURN NODE_MENU_BASE_BUTTON_CONTROLS(sNodeMenu) != NODE_MENU_BASE_INPUT_EVENT_NONE
ENDFUNC

/// PURPOSE:
///    Creates a simple node menu item with its default behaviour
/// RETURNS:
///    A new simple node menu item with its default behaviour
FUNC NODE_MENU_ITEM_STRUCT CREATE_SIMPLE_NODE_MENU_ITEM()
	NODE_MENU_ITEM_STRUCT sMenuItem
	NODE_MENU_ITEM_SET_ON_ADD(sMenuItem, &NODE_MENU_ITEM_SIMPLE_ON_ADD_EVENT)
	NODE_MENU_ITEM_SET_INPUT_UPDATE(sMenuItem, &NODE_MENU_SIMPLE_BASE_CONTROLS)
	NODE_MENU_ITEM_SET_ADD_HELP_KEYS(sMenuItem, &NODE_MENU_SIMPLE_BASE_HELP_KEYS)
	PRINTLN("[NODE_MENU][SIMPLE] Used prototype")
	RETURN sMenuItem
ENDFUNC

/// PURPOSE:
///    Cleanup for a simple node menu
PROC NODE_MENU_SIMPLE_CLEANUP_ASSETS()
	CLEAR_MENU_DATA(TRUE)
	CLEANUP_MENU_ASSETS()
	PRINTLN("[NODE_MENU][SIMPLE] NODE_MENU_SIMPLE_CLEANUP_ASSETS - Called")
ENDPROC

FUNC BOOL NODE_MENU_SIMPLE_LOAD_MENU_ASSETS()
	RETURN LOAD_MENU_ASSETS(DEFAULT, DEFAULT, TRUE)
ENDFUNC

///------------------------------
///    SIMPLE MENU CREATION
///------------------------------    

/// PURPOSE:
///    Creates a simple node menu with its default behaviour
/// PARAMS:
///    sRoot - the starting parent item (its children will be displayed first)
/// RETURNS:
///    A new simple node menu with its default behaviour
FUNC NODE_MENU_STRUCT CREATE_SIMPLE_NODE_MENU(NODE_MENU_ITEM_STRUCT &sRoot, INT iStartingHighlight = 0)
	NODE_MENU_STRUCT sMenu
	
	// Configure node menu for using the old simple menu
	NODE_MENU_SET_BUILD_METHOD(sMenu, &NODE_MENU_SIMPLE_BUILD_MENU)
	NODE_MENU_SET_DRAW_METHOD(sMenu, &NODE_MENU_SIMPLE_DRAW_METHOD)
	NODE_MENU_SET_HIGHLIGHT_METHOD(sMenu, &NODE_MENU_SIMPLE_HIGHLIGHT_METHOD)
	NODE_MENU_SET_DESCRIPTION_METHOD(sMenu, &NODE_MENU_SIMPLE_DESCRIPTION_METHOD)
	NODE_MENU_SET_MENU_ITEM_PROTOTYPE(sMenu, &CREATE_SIMPLE_NODE_MENU_ITEM)
	NODE_MENU_SET_LOAD_ASSETS(sMenu, &NODE_MENU_SIMPLE_LOAD_MENU_ASSETS)
	NODE_MENU_SET_CLEAN_UP_ASSETS(sMenu, &NODE_MENU_SIMPLE_CLEANUP_ASSETS)
	
	NODE_MENU_SET_ROOT_NODE(sMenu, sRoot)
	NODE_MENU_JUMP_TO_SUBMENU(sMenu, sRoot, iStartingHighlight, FALSE)
	RETURN sMenu
ENDFUNC
