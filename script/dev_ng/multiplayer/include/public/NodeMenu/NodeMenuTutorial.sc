USING "node_menu.sch"

///---------------------------------------------------------------------
///    MENU DEFINITIONS
///---------------------------------------------------------------------    

/// PURPOSE: 
///    Defines the items in the root menu
ENUM ROOT_MENU_ITEMS
	ROOT_MENU_ITEM_ONE,
	ROOT_MENU_ITEM_TWO
ENDENUM

/// PURPOSE: 
///    Defines the sub items under menu item one
ENUM MENU_ITEM_ONE_SUB_ITEMS
	SUB_ITEM_ONE,
	SUB_ITEM_TWO
ENDENUM

///---------------------------------------------------------------------
///    MENU DATA
///---------------------------------------------------------------------    

NODE_MENU_STRUCT sMyMenu
BOOL bHaveAssetsLoaded = FALSE
T_NODE_MENU_ITEM_GET fpRootNodeConnection
T_NODE_MENU_ITEM_GET fpMenuItemOneConnection
INT iMyInt = 0

///---------------------------------------------------------------------
///    MENU CONFIG
///---------------------------------------------------------------------

///----------------------------
///    Menu item one sub items
///----------------------------  

/// PURPOSE:
///    Gets the first sub item
PROC GET_SUB_ITEM_ONE(NODE_MENU_ITEM_STRUCT &sMenuItem)
	NODE_MENU_ITEM_SET_DISPLAY_NAME(sMenuItem, "TUT_SUB_M2")
	NODE_MENU_ITEM_SET_DESCRIPTION(sMenuItem, "TUT_SUB_M2_D")
ENDPROC

/// PURPOSE:
///    Gets the second sub item
PROC GET_SUB_ITEM_TWO(NODE_MENU_ITEM_STRUCT &sMenuItem)
	NODE_MENU_ITEM_SET_DISPLAY_NAME(sMenuItem, "TUT_SUB_M2")
	NODE_MENU_ITEM_SET_DESCRIPTION(sMenuItem, "TUT_SUB_M2_D")
ENDPROC

///----------------------------
///    Menu item one
///----------------------------  

/// PURPOSE:
///    Defines the on add behaviour for menu item one.
///    Displays an integer on the menu item.
PROC MENU_ITEM_ONE_ON_ADD(NODE_MENU_ITEM_STRUCT &sMenuItem, INT iParamIndex)
	ADD_MENU_ITEM_TEXT(iParamIndex, NODE_MENU_ITEM_GET_DISPLAY_NAME(sMenuItem))
	ADD_MENU_ITEM_TEXT(iParamIndex, "NUMBER", 1)
	ADD_MENU_ITEM_TEXT_COMPONENT_INT(iMyInt)
ENDPROC

/// PURPOSE:
///    Defines the on highlight behaviour for menu item one.
///    Makes the menu item have arrows when highlighted.
PROC MENU_ITEM_ONE_ON_HIGHLIGHT()
	SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
ENDPROC

/// PURPOSE:
///    Defines the on next behaviour for menu item one.
///    Increments the integer displayed on the item.
PROC MENU_ITEM_ONE_ON_NEXT()
	++iMyInt
ENDPROC

/// PURPOSE:
///    Defines the on previous behaviour for menu item one.
///    Decrements the integer displayed on the item.
PROC MENU_ITEM_ONE_ON_PREVIOUS()
	--iMyInt
ENDPROC

/// PURPOSE:
///    Defines the on press behaviour for menu item one.
///    Sets the integer displayed on the item to 100.
PROC MENU_ITEM_ONE_ON_PRESS()
	iMyInt = 100
ENDPROC

/// PURPOSE:
///    Gets menu item ones child at the given index.
///    Used for displaying menu items ones sub menu items. 
FUNC NODE_MENU_ITEM_LOOKUP_STATUS GET_MENU_ITEM_ONE_CHILD_ITEMS(INT iChildIndex, NODE_MENU_ITEM_STRUCT &sChild)
	NODE_MENU_ITEM_SET_PARENT(sChild, fpMenuItemOneConnection)
	
	SWITCH INT_TO_ENUM(MENU_ITEM_ONE_SUB_ITEMS, iChildIndex)
		CASE SUB_ITEM_ONE	GET_SUB_ITEM_ONE(sChild)	RETURN NODE_MENU_ITEM_LOOKUP_STATUS_SUCCESS
		CASE SUB_ITEM_TWO	GET_SUB_ITEM_TWO(sChild)	RETURN NODE_MENU_ITEM_LOOKUP_STATUS_SUCCESS
	ENDSWITCH	
	
	RETURN NODE_MENU_ITEM_LOOKUP_STATUS_END_OF_LOOKUP // Get child success
ENDFUNC

/// PURPOSE:
///    Gets menu item one in the root menu.
PROC GET_ROOT_MENU_ITEM_ONE(NODE_MENU_ITEM_STRUCT &sMenuItem)
	NODE_MENU_ITEM_SET_TITLE(sMenuItem, "TUT_ROOT_M1T")
	NODE_MENU_ITEM_SET_DISPLAY_NAME(sMenuItem, "TUT_ROOT_M1")
	NODE_MENU_ITEM_SET_DESCRIPTION(sMenuItem, "TUT_ROOT_M1_D")
	NODE_MENU_ITEM_SET_CHILDREN(sMenuItem, ENUM_TO_INT(ROOT_MENU_ITEM_ONE), &GET_MENU_ITEM_ONE_CHILD_ITEMS, COUNT_OF(MENU_ITEM_ONE_SUB_ITEMS))
ENDPROC

///----------------------------
///    Menu item two
///---------------------------- 

/// PURPOSE:
///    Gets menu item two in the root menu.
PROC GET_ROOT_MENU_ITEM_TWO(NODE_MENU_ITEM_STRUCT &sMenuItem)
	NODE_MENU_ITEM_SET_DISPLAY_NAME(sMenuItem, "TUT_ROOT_M2")
	NODE_MENU_ITEM_SET_DESCRIPTION(sMenuItem, "TUT_ROOT_M2_D")
ENDPROC

///----------------------------
///   Root menu item
///---------------------------- 

/// PURPOSE:
///    Gets the root menu items child at the given index.
///    Used for displaying the root menu items. 
FUNC NODE_MENU_ITEM_LOOKUP_STATUS GET_ROOT_CHILD_ITEMS(INT iChildIndex, NODE_MENU_ITEM_STRUCT &sChild)
	NODE_MENU_ITEM_SET_PARENT(sChild, fpRootNodeConnection)
	
	SWITCH INT_TO_ENUM(ROOT_MENU_ITEMS, iChildIndex)
		CASE ROOT_MENU_ITEM_ONE	GET_ROOT_MENU_ITEM_ONE(sChild) 	RETURN NODE_MENU_ITEM_LOOKUP_STATUS_SUCCESS
		CASE ROOT_MENU_ITEM_TWO	GET_ROOT_MENU_ITEM_TWO(sChild)	RETURN NODE_MENU_ITEM_LOOKUP_STATUS_SUCCESS
	ENDSWITCH	
	
	RETURN NODE_MENU_ITEM_LOOKUP_STATUS_END_OF_LOOKUP // Get child success
ENDFUNC

/// PURPOSE:
///    Gets the root menu item
PROC GET_ROOT_NODE(NODE_MENU_ITEM_STRUCT &sRoot)
	NODE_MENU_ITEM_SET_TITLE(sRoot, "TUT_ROOT_TITLE")
	NODE_MENU_ITEM_SET_CHILDREN(sRoot, 0, &GET_ROOT_CHILD_ITEMS, COUNT_OF(ROOT_MENU_ITEMS))
ENDPROC

///---------------------------------------------------------------------
///   CORE LOOP
///--------------------------------------------------------------------- 

/// PURPOSE:
///    Pre-loop Initialization
PROC PROCESS_PREGAME()
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()// Allow script to run in multiplayer
	
	// Init the menu, must be done before use
	NODE_MENU_ITEM_STRUCT sRoot 
	GET_ROOT_NODE(sRoot)
	sMyMenu = CREATE_SIMPLE_NODE_MENU(sRoot)
	
	// Establish parent item connections
	fpRootNodeConnection = &GET_ROOT_NODE
	fpMenuItemOneConnection = &GET_ROOT_MENU_ITEM_ONE
ENDPROC

/// PURPOSE:
///    Cleans up the script and menu assets
PROC CLEANUP()
	NODE_MENU_CLEAN_UP_ASSETS(sMyMenu)
	TERMINATE_THIS_THREAD()// Terminate the script
ENDPROC

/// PURPOSE:
///    Updates and draws the menu
PROC UPDATE()
	IF NOT bHaveAssetsLoaded
		bHaveAssetsLoaded = NODE_MENU_LOAD_ASSETS(sMyMenu)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF IS_DEBUG_KEY_PRESSED(KEY_R, KEYBOARD_MODIFIER_ALT, "Show menu")
		NODE_MENU_OPEN(sMyMenu)
	ENDIF
	#ENDIF
	
	// Must be in the open state for it to draw, returns true when the player quits the menu
	NODE_MENU_UPDATE_AND_DRAW(sMyMenu)
ENDPROC

SCRIPT
	PROCESS_PREGAME()
	
	WHILE TRUE
		IF SHOULD_THIS_MULTIPLAYER_THREAD_TERMINATE()
			CLEANUP()
			EXIT
		ENDIF
	
		UPDATE()
		WAIT(0)
	ENDWHILE
ENDSCRIPT
