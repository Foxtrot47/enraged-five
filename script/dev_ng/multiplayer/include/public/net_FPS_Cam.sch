//MP FPS camera 
//Author: Conor McGuire
//Date: 1/7/11
USING "rage_builtins.sch"
USING "globals.sch"

// Game Headers
USING "script_buttons.sch"
USING "net_prints.sch"
USING "commands_recording.sch"

CONST_FLOAT	FPC_DONT_UPDATE_FOV		-1.0

//Interpolation rates.
CONST_FLOAT FPC_ROT_INTERP			0.050
CONST_FLOAT FPC_FOV_INTERP			0.060

//Camera effect values.
CONST_FLOAT FPC_HANDSHAKE			0.19

CONST_FLOAT FPC_PC_ZOOM_SPEED		5.0

//Focus release timer
CONST_INT FPC_FOCUS_RESET			4000

PROC GET_VECTOR_FROM_ROTATION(VECTOR &InVec, VECTOR rotation)
      
      FLOAT CosAngle
      FLOAT SinAngle
      VECTOR ReturnVec

      // Rotation about the x axis 
      CosAngle = COS(rotation.x)
      SinAngle = SIN(rotation.x)
      ReturnVec.x = InVec.x
      ReturnVec.y = (CosAngle * InVec.y) - (SinAngle * InVec.z)
      ReturnVec.z = (SinAngle * InVec.y) + (CosAngle * InVec.z)
      InVec = ReturnVec

      // Rotation about the y axis
      CosAngle = COS(rotation.y)
      SinAngle = SIN(rotation.y)
      ReturnVec.x = (CosAngle * InVec.x) + (SinAngle * InVec.z)
      ReturnVec.y = InVec.y
      ReturnVec.z = (CosAngle * InVec.z) - (SinAngle * InVec.x) 
      InVec = ReturnVec
      
      // Rotation about the z axis 
      CosAngle = COS(rotation.z)
      SinAngle = SIN(rotation.z)
      ReturnVec.x = (CosAngle * InVec.x) - (SinAngle * InVec.y)
      ReturnVec.y = (SinAngle * InVec.x) + (CosAngle * InVec.y)
      ReturnVec.z = InVec.z
      InVec = ReturnVec
ENDPROC


FUNC VECTOR GET_CAMERA_INIT_FORWARD_VECTOR(FIRST_PERSON_CAM_STRUCT& fpCam)
	VECTOR vEnd
	//end vector - pointing north
	vEnd	= <<0.0, 1.0, 0.0>>
	//point it in the same direction as the init camera
	GET_VECTOR_FROM_ROTATION(vEnd,fpCam.vInitCamRot)
	RETURN vEnd
ENDFUNC


FUNC VECTOR GET_FIRST_PERSON_NEW_FOCUS_ROTATION(FIRST_PERSON_CAM_STRUCT& fpCam, VECTOR vNewFocusPos)
	//Get camera forward vector
	VECTOR vCamForward  = GET_CAMERA_INIT_FORWARD_VECTOR(fpCam)
	
	//Get vector to new focus
	VECTOR vNewFocusDirection
	vNewFocusDirection.x = vNewFocusPos.x - fpCam.vInitCamPos.x
	vNewFocusDirection.y = vNewFocusPos.y - fpCam.vInitCamPos.y
	vNewFocusDirection.z = vNewFocusPos.z - fpCam.vInitCamPos.z
	
	//Get XY angle between initial camera forward vector and new focus vector.
	FLOAT fXYAngle = GET_ANGLE_BETWEEN_2D_VECTORS(vCamForward.x,vCamForward.y,vNewFocusDirection.x, vNewFocusDirection.y)
	
	vCamForward = NORMALISE_VECTOR(vCamForward)
	vNewFocusDirection = NORMALISE_VECTOR(vNewFocusDirection)
	FLOAT fFowardVectorPitch = ASIN(vCamForward.z)
	FLOAT fNewFocusVectorPitch = ASIN(vNewFocusDirection.z) 
	
	FLOAT fYZAngle = fNewFocusVectorPitch - fFowardVectorPitch
	
	FLOAT fRollAngle = fXYAngle*fpCam.iRollLimit/fpCam.iLookXLimit

	RETURN <<fYZAngle, fRollAngle, fXYAngle>>
ENDFUNC


/// PURPOSE:
///    Setups up parameters for a new first person camera.
/// PARAMS:
///    fpCam 		- Struct representing an uninitialised first person camera.
///    vInitPos 	- Initial camera position
///    vInitRot 	- Initial camera rotation
///    fInitFov 	- Initial camera FOV
///    iLookXLimit 	- Left/Right look limit
///    iLookYLimit 	- Up/Down look limit
///    iRollLimit 	- Roll limit (leave as default)
///    fMaxZoom 	- Max zoom
///    bLockMiniMap - True if you want to lock mini-map angle while using FP cam
///    iLockAngle 	- Angle to lock mini-map to
///    fNearClip 	- Near clip of the camera (set very low if you have things close to player i.e. stripper ped)
///    
PROC INIT_FIRST_PERSON_CAMERA(FIRST_PERSON_CAM_STRUCT& fpCam, VECTOR vInitPos, VECTOR vInitRot, FLOAT fInitFov, INT iLookXLimit = 20, INT iLookYLimit = 10, INT iRollLimit = 3, FLOAT fMaxZoom = 20.0, BOOL bLockMiniMap = FALSE, INT iLockAngle = 0, FLOAT fNearClip = -1.0, BOOL bDisableHandshake = FALSE)
										
	#IF IS_DEBUG_BUILD
		IF g_bInMultiplayer
			NET_PRINT("Initialising new FP cam.") NET_NL()
		ELSE
			CPRINTLN(DEBUG_SYSTEM, "<FP-CAM> Initialising new FP cam.")
			CDEBUG1LN(DEBUG_SYSTEM, "<FP-CAM> Base pos: ", vInitPos)
			CDEBUG1LN(DEBUG_SYSTEM, "<FP-CAM> Base rot: ", vInitRot)
			CDEBUG1LN(DEBUG_SYSTEM, "<FP-CAM> Base fov: ", fInitFov)
			CDEBUG1LN(DEBUG_SYSTEM, "<FP-CAM> X look angle limit: ", iLookXLimit)
			CDEBUG1LN(DEBUG_SYSTEM, "<FP-CAM> Y look angle limit: ", iLookXLimit)
			CDEBUG1LN(DEBUG_SYSTEM, "<FP-CAM> Applied X roll limit: ", iRollLimit)
			CDEBUG1LN(DEBUG_SYSTEM, "<FP-CAM> Zoom limit: ", fMaxZoom)
			CDEBUG1LN(DEBUG_SYSTEM, "<FP-CAM> Minimap locked: ", PICK_STRING(bLockMiniMap, "Yes", "No"))
			IF bLockMiniMap
				CDEBUG1LN(DEBUG_SYSTEM, "<FP-CAM> Minimap locked angle: ", iLockAngle)
			ENDIF
			CDEBUG1LN(DEBUG_SYSTEM, "<FP-CAM> Near clip: ", fNearClip)
			CDEBUG1LN(DEBUG_SYSTEM, "<FP-CAM> Handshake disabled: ", PICK_STRING(bDisableHandshake, "Yes", "No"))
		ENDIF
	#ENDIF
	
	fpCam.vInitCamPos = vInitPos 
	fpCam.vInitCamRot = vInitRot
	fpCam.fBaseCamFov = fInitFov
	fpCam.iLookXLimit = iLookXLimit
	fpCam.iLookYLimit = iLookYLimit
	fpCam.iRollLimit = iRollLimit

	fpCam.vCamRotStickOffsetTarget = <<0,0,0>>
	fpCam.vCamRotFocusOffsetTarget = <<0,0,0>>
	fpCam.vCamRotOffsetCurrent = <<0,0,0>>
	fpCam.fCamFOVTarget = fInitFov
	fpCam.fCamFOVCurrent = fInitFov
	
	fpCam.bLockMiniMapAngle = bLockMiniMap
	fpCam.fMaxZoom = fMaxZoom
	
	fpCam.theCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	SET_CAM_ACTIVE(fpCam.theCam,TRUE)
	SET_CAM_PARAMS(	fpCam.theCam,
					fpCam.vInitCamPos,
					fpCam.vInitCamRot,
					fpCam.fBaseCamFov)
					
	IF NOT bDisableHandshake
		SHAKE_CAM(fpCam.theCam, "HAND_SHAKE", FPC_HANDSHAKE)
	ENDIF
	 
	RENDER_SCRIPT_CAMS(TRUE,FALSE)
	IF fNearClip > 0
		SET_CAM_NEAR_CLIP(fpCam.theCam,fNearClip)
	ENDIF
	IF fpCam.bLockMiniMapAngle
		LOCK_MINIMAP_ANGLE(iLockAngle)
	ENDIF
	
	fpCam.iOldStickRX = 0
	fpCam.iOldStickRY = 0
	
	fpCam.fPrevMouseX = 0.0
	fpCam.fPrevMouseY = 0.0
	
	fpCam.bCappedFOV = FALSE
	fpCam.bResetZoom = FALSE
	fpCam.iFocusResetTime = 0
	
	//MPGlobalsAmbience.bInFPView = TRUE
ENDPROC


/// PURPOSE:
///    Set an initialised first person camera to center its focus in a new direction and with a new base FOV.
/// PARAMS:
///    fpCam 		- Struct representing an initialised first person camera.
///    vNewFocusRot - The new base direction to center focus on.
///    fNewBaseFOV	- The new base field of view.
///    
PROC REFOCUS_FIRST_PERSON_CAMERA_WITH_ROTATION(FIRST_PERSON_CAM_STRUCT& fpCam, VECTOR vNewFocusRot, FLOAT fNewBaseFOV = FPC_DONT_UPDATE_FOV)
	//CPRINTLN(DEBUG_SYSTEM, "<FP-CAM> Focusing FP cam with rotation ", vNewFocusRot, ".")
	fpCam.vCamRotFocusOffsetTarget = vNewFocusRot
	IF fNewBaseFOV != FPC_DONT_UPDATE_FOV
		//CPRINTLN(DEBUG_SYSTEM, "<FP-CAM> Setting FP cam base FOV to ", fNewBaseFOV, ".")
		fpCam.fBaseCamFov = fNewBaseFOV
	ENDIF
ENDPROC


/// PURPOSE:
///    Set an initialised first person camera to center its focus on a new world position and with a new base FOV.
/// PARAMS:
///    fpCam 		- Struct representing an initialised first person camera.
///    vNewFocusPos - The new world positon to center focus on.
///    fNewBaseFOV	- The new base field of view.
///    
PROC REFOCUS_FIRST_PERSON_CAMERA_ON_WORLD_POSITION(FIRST_PERSON_CAM_STRUCT& fpCam, VECTOR vNewFocusPos, FLOAT fNewBaseFOV = FPC_DONT_UPDATE_FOV)
	//CPRINTLN(DEBUG_SYSTEM, "<FP-CAM> Focusing FP cam on world position ", vNewFocusPos, ".")
	REFOCUS_FIRST_PERSON_CAMERA_WITH_ROTATION(fpCam, GET_FIRST_PERSON_NEW_FOCUS_ROTATION(fpCam, vNewFocusPos), fNewBaseFOV)
ENDPROC


/// PURPOSE:
///    Update a first person camera this frame. Read control input and change camera facing and zoom accordingly.
/// PARAMS:
///    fpCam 				- Struct representing an initialised first person camera.
///    bZoomAvailable		- Allow zoom controls or not.
///    bStickLookAvailable 	- Allow the player to pull the view around with the right stick or not.
///  
PROC UPDATE_FIRST_PERSON_CAMERA(FIRST_PERSON_CAM_STRUCT& fpCam, BOOL bZoomAvailable = TRUE, BOOL bStickLookAvailable = TRUE ,BOOL bZoomInOnly = FALSE, BOOL bMouseGrabMode = FALSE, FLOAT fMouseSensitivity = 0.2, BOOL bMouseFrontEndZoomControls = FALSE, FLOAT fInterpSpeed = 1.0 , BOOL bUseLTRTZoom = FALSE)
	//Analogue stick axis indexes.
	CONST_INT FPC_LEFT_X	0
	CONST_INT FPC_LEFT_Y	1
	CONST_INT FPC_RIGHT_X	2
	CONST_INT FPC_RIGHT_Y	3
	INT iStickPosition[4]
	
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	
	//Get analogue stick positions.
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(iStickPosition[FPC_LEFT_X],
									 	 iStickPosition[FPC_LEFT_Y],
									 	 iStickPosition[FPC_RIGHT_X],
									 	 iStickPosition[FPC_RIGHT_Y])
										 
	// Invert gamepad stick if look is inverted in settings.
	IF IS_LOOK_INVERTED()
		 iStickPosition[FPC_RIGHT_Y] *= -1
	ENDIF
										 							 
	// Apply PC movements cumulatively (clamped to normal min/max of 127)
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
	
		FLOAT fMouseX
		FLOAT fMouseY
		FLOAT fMouseDistX
		FLOAT fMouseDistY
		
		fMouseX = GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X) 
		fMouseY = GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y)
		fMouseDistX = fMouseX - fpCam.fPrevMouseX
		fMouseDistY = fMouseY - fpCam.fPrevMouseY
		
		fpCam.fPrevMouseX = fMouseX
		fpCam.fPrevMouseY = fMouseY

		// This is for cameras like the planning board which on PC use a grab camera using cursor distance moved rather than a look camera.
		IF bMouseGrabMode
			iStickPosition[FPC_RIGHT_X] = -ROUND((fMouseDistX * fMouseSensitivity) * 127.0)
			iStickPosition[FPC_RIGHT_Y] = -ROUND((fMouseDistY * fMouseSensitivity) * 127.0)
		ELSE
			// Get look input direct from LR/UD inputs, so they take account of mouse sensitivity and all inversions
			iStickPosition[FPC_RIGHT_X] = ROUND((GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCALED_LOOK_LR) * fMouseSensitivity) * 127.0)
			iStickPosition[FPC_RIGHT_Y] = ROUND((GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCALED_LOOK_UD) * fMouseSensitivity) * 127.0)
		ENDIF
		
		iStickPosition[FPC_RIGHT_X] = CLAMP_INT(iStickPosition[FPC_RIGHT_X] + fpCam.iOldStickRX,-127,127)
		iStickPosition[FPC_RIGHT_Y] = CLAMP_INT(iStickPosition[FPC_RIGHT_Y] + fpCam.iOldStickRY,-127,127)
	
	ENDIF
	
	//Check if moved from old focus position 
	IF fpCam.iOldStickRX = iStickPosition[FPC_RIGHT_X] AND fpCam.iOldStickRY = iStickPosition[FPC_RIGHT_Y]
		//Do nothing if not moved but not time to reset yet
		IF fpCam.iFocusResetTime < GET_GAME_TIMER()
			fpCam.iOldStickRX = 0
			fpCam.iOldStickRY = 0
			//Reset mouse position if on Kb&M
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				iStickPosition[FPC_RIGHT_X] = 0
				iStickPosition[FPC_RIGHT_Y] = 0
				fpCam.bResetZoom = TRUE
			ENDIF
		ENDIF
	ELSE	
	//Register new positions
		fpCam.iOldStickRX = iStickPosition[FPC_RIGHT_X]
		fpCam.iOldStickRY = iStickPosition[FPC_RIGHT_Y]
		fpCam.iFocusResetTime = GET_GAME_TIMER() + FPC_FOCUS_RESET
		fpCam.bresetZoom = FALSE
	ENDIF
										 
	//Update camera rotation stick target based on right stick.
	IF bStickLookAvailable 
		
		fpCam.vCamRotStickOffsetTarget.z = -(TO_FLOAT(iStickPosition[FPC_RIGHT_X])/127)*fpCam.iLookXLimit
		fpCam.vCamRotStickOffsetTarget.y = -fpCam.vCamRotStickOffsetTarget.z*fpCam.iRollLimit/fpCam.iLookXLimit
		
		fpCam.vCamRotStickOffsetTarget.x = -(TO_FLOAT(iStickPosition[FPC_RIGHT_Y])/127)*fpCam.iLookYLimit

	ELSE	//Reset Kb&M focus when look unavailable
		fpCam.vCamRotStickOffsetTarget = <<0,0,0>>
		fpCam.iOldStickRX = 0
		fpCam.iOldStickRY = 0
	ENDIF
	//*** Steps all camera interpolated values towards their target values. ***
	//Scale by timestep to ensure this is frame rate independent.
	FLOAT fFrameRateModifier = 30.0 * TIMESTEP()
	
	VECTOR vCamRotOffsetTarget = fpCam.vCamRotStickOffsetTarget + fpCam.vCamRotFocusOffsetTarget
	
	//Old mouse code
	/*IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND bStickLookAvailable
	// No interpolation or auto-centering if using mouse and keyboard.
		//CPRINTLN(debug_heist,"using M&Kb")
		FLOAT fXLimit = TO_FLOAT(fpCam.iLookXLimit)
		FLOAT fYLimit = TO_FLOAT(fpCam.iLookYLimit)
		FLOAT fRollLimit = TO_FLOAT(fpCam.iRollLimit)//fpCam.ILookYLimit)
		//CPRINTLN(debug_heist,"M interp:",fpCam.vCamRotOffsetCurrent.x,"+",fpCam.vCamRotStickOffsetTarget.x, " ",fpCam.vCamRotOffsetCurrent.y,"+",fpCam.vCamRotStickOffsetTarget.y )
		fpCam.vCamRotOffsetCurrent.x = CLAMP((fpCam.vCamRotOffsetCurrent.x + (fpCam.vCamRotStickOffsetTarget.x )), -fYLimit, fYLimit)
		fpCam.vCamRotOffsetCurrent.y = CLAMP((fpCam.vCamRotOffsetCurrent.y + (fpCam.vCamRotStickOffsetTarget.y )), -fRollLimit, fRollLimit)
		fpCam.vCamRotOffsetCurrent.z = CLAMP((fpCam.vCamRotOffsetCurrent.z + (fpCam.vCamRotStickOffsetTarget.z )), -fXLimit, fXLimit)
	
	ELSE*/
	
	//Interpolate camera rotation offset. Limit rotation to max 3 degrees per frame.
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL) AND bStickLookAvailable AND NOT fpCam.bResetZoom
		fpCam.vCamRotOffsetCurrent.x = vCamRotOffsetTarget.x
		fpCam.vCamRotOffsetCurrent.y = vCamRotOffsetTarget.y
		fpCam.vCamRotOffsetCurrent.z = vCamRotOffsetTarget.z
	ELSE
		fpCam.vCamRotOffsetCurrent.x += CLAMP((vCamRotOffsetTarget.x - fpCam.vCamRotOffsetCurrent.x) * FPC_ROT_INTERP * fFrameRateModifier * fInterpSpeed, -3.0, 3.0)
		fpCam.vCamRotOffsetCurrent.y += CLAMP((vCamRotOffsetTarget.y - fpCam.vCamRotOffsetCurrent.y) * FPC_ROT_INTERP * fFrameRateModifier * fInterpSpeed, -3.0, 3.0)
		fpCam.vCamRotOffsetCurrent.z += CLAMP((vCamRotOffsetTarget.z - fpCam.vCamRotOffsetCurrent.z) * FPC_ROT_INTERP * fFrameRateModifier * fInterpSpeed, -3.0, 3.0)
	ENDIF
		//Clamp values of current offset, if needed
		IF fpCam.bCappedFOV
			fpCam.vCamRotOffsetCurrent.x = CLAMP(fpCam.vCamRotOffsetCurrent.x,TO_FLOAT(-fpCam.iLookYLimit),TO_FLOAT(fpCam.iLookYLimit))
			fpCam.vCamRotOffsetCurrent.y = CLAMP(fpCam.vCamRotOffsetCurrent.y,TO_FLOAT(-fpCam.iRollLimit),TO_FLOAT(fpCam.iRollLimit))
			fpCam.vCamRotOffsetCurrent.z = CLAMP(fpCam.vCamRotOffsetCurrent.z,TO_FLOAT(-fpCam.iLookXLimit),TO_FLOAT(fpCam.iLookXLimit))
		ENDIF
//	ENDIF
	
	//Interpolate camera FOV.
	//Reset FOV for non-PC 
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) AND bZoomAvailable
		//Don't reset mouse zoom level if the player can zoom in and has moved recently
		IF fpCam.bResetZoom
			fpCam.fCamFOVTarget = fpCam.fBaseCamFov
		ENDIF
	ELSE
		fpCam.fCamFOVTarget = fpCam.fBaseCamFov
	ENDIF
	
	IF bZoomAvailable
		
		
		// PC zoom controls using sniper zoom 
		// Handled differently as we don't have access to analogue input for zoom on keyboard and mouse.
		// We keep track of the current FOV and use the sniper zoom controls to increment or decrement the target fov.
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)

			CONTROL_ACTION eInputZoomIn		= INPUT_SNIPER_ZOOM_IN_ONLY
			CONTROL_ACTION eInputZoomOut	= INPUT_SNIPER_ZOOM_OUT_ONLY
			
			IF bMouseFrontEndZoomControls
				eInputZoomIn	= INPUT_CURSOR_SCROLL_UP
				eInputZoomOut	= INPUT_CURSOR_SCROLL_DOWN
			ENDIF
			
			IF IS_DISABLED_CONTROL_JUST_PRESSED( PLAYER_CONTROL, eInputZoomIn )
				fpCam.fCamFOVTarget -= FPC_PC_ZOOM_SPEED
				//Reset focus timer and zoom reset
				fpCam.iFocusResetTime = GET_GAME_TIMER() + FPC_FOCUS_RESET
				fpCam.bResetZoom  = false
			ELIF IS_DISABLED_CONTROL_JUST_PRESSED( PLAYER_CONTROL, eInputZoomOut )
				fpCam.fCamFOVTarget += FPC_PC_ZOOM_SPEED
				//Reset focus timer and zoom reset
				fpCam.iFocusResetTime = GET_GAME_TIMER() + FPC_FOCUS_RESET
				fpCam.bResetZoom = false
			ENDIF
			
			IF bZoomInOnly
				 fpCam.fCamFOVTarget = CLAMP( fpCam.fCamFOVTarget, fpCam.fBaseCamFov - fpCam.fMaxZoom, fpCam.fBaseCamFov )
			ELSE
				 fpCam.fCamFOVTarget = CLAMP(  fpCam.fCamFOVTarget, fpCam.fBaseCamFov - fpCam.fMaxZoom , fpCam.fBaseCamFov + fpCam.fMaxZoom )
			ENDIF
			
		ELSE
			IF bUseLTRTZoom
				iStickPosition[FPC_LEFT_Y] = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
				iStickPosition[FPC_RIGHT_Y] = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
				
				PRINTLN("TRIGGERCHECK LEFT - ", iStickPosition[FPC_LEFT_Y])
				PRINTLN("TRIGGERCHECK RIGHT - ", iStickPosition[FPC_RIGHT_Y])
				
				IF bZoomInOnly
					IF TO_FLOAT(iStickPosition[FPC_RIGHT_Y]) > 127 
						fpCam.fCamFOVTarget -= ROUND(TO_FLOAT(iStickPosition[FPC_RIGHT_Y])/128.0*(fpCam.fMaxZoom/2))
					ENDIF
				ELSE
					fpCam.fCamFOVTarget += ROUND(TO_FLOAT(iStickPosition[FPC_LEFT_Y])/128.0*fpCam.fMaxZoom)
					fpCam.fCamFOVTarget -= ROUND(TO_FLOAT(iStickPosition[FPC_RIGHT_Y])/128.0*fpCam.fMaxZoom)
				ENDIF
				
			ELSE
		
				IF bZoomInOnly
					IF TO_FLOAT(iStickPosition[FPC_LEFT_Y]) < 0 
						fpCam.fCamFOVTarget += ROUND(TO_FLOAT(iStickPosition[FPC_LEFT_Y])/128.0*fpCam.fMaxZoom)
					ENDIF
				ELSE
					fpCam.fCamFOVTarget += ROUND(TO_FLOAT(iStickPosition[FPC_LEFT_Y])/128.0*fpCam.fMaxZoom)
				ENDIF
				
					//fpCam.fCamFOVTarget += ROUND(TO_FLOAT(iStickPosition[FPC_LEFT_Y])/128.0*fpCam.fMaxZoom)
			ENDIF
		ENDIF
		
	
	ENDIF
	
	// Not sure about this on PC - we can't assume a 30fps frame rate, but it seems to work.
	//This needs to be outside the IF bZoomAvailable block since planning boards deactivate zoom when no buttons pressed 
	//Should fix B* 1790213
	fpCam.fCamFOVCurrent += (fpCam.fCamFOVTarget - fpCam.fCamFOVCurrent) * FPC_FOV_INTERP * fFrameRateModifier
	
	
	//Update the camera with interpolated values.
	SET_CAM_PARAMS(	fpCam.theCam,
					fpCam.vInitCamPos,
					fpCam.vInitCamRot + fpCam.vCamRotOffsetCurrent,
					fpCam.fCamFOVCurrent)
	
	IF DOES_CAM_EXIST(fpCam.theCam)
		IF IS_CAM_ACTIVE(fpCam.theCam)
			IF IS_CAM_RENDERING(fpCam.theCam)
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2170341
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Clean up an initialised first person camera.
/// PARAMS:
///    fpCam	- Struct representing an initialised first person camera.
///    
PROC CLEAR_FIRST_PERSON_CAMERA(FIRST_PERSON_CAM_STRUCT& fpCam #IF USE_TU_CHANGES , BOOL bApplyAcrossAllThreads = FALSE , BOOL bStopScriptedCams = TRUE #ENDIF)
	#IF IS_DEBUG_BUILD
		IF g_bInMultiplayer
			DEBUG_PRINTCALLSTACK()
			NET_PRINT("Cleaning up FP cam.") NET_NL()
		ELSE
			CPRINTLN(DEBUG_SYSTEM, "<FP-CAM> Cleaning up FP cam.")
		ENDIF
	#ENDIF

	IF DOES_CAM_EXIST(fpCam.theCam)
	
		#IF USE_TU_CHANGES
			IF bStopScriptedCams
				RENDER_SCRIPT_CAMS(FALSE, FALSE, DEFAULT, DEFAULT, bApplyAcrossAllThreads)
			ENDIF
		#ENDIF
		
		#IF NOT USE_TU_CHANGES
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		#ENDIF
		
		IF IS_CAM_ACTIVE(fpCam.theCam)
			SET_CAM_ACTIVE(fpCam.theCam,FALSE)
		ENDIF

		#IF USE_TU_CHANGES
			DESTROY_CAM(fpCam.theCam, bApplyAcrossAllThreads)
		#ENDIF

		#IF NOT USE_TU_CHANGES
			DESTROY_CAM(fpCam.theCam)
		#ENDIF
		
		//MPGlobalsAmbience.bInFPView = FALSE
		
		#IF IS_DEBUG_BUILD
			IF g_bInMultiplayer
				DEBUG_PRINTCALLSTACK()
				NET_PRINT("Active FP cam was deactivated.") NET_NL()
			ELSE
				CPRINTLN(DEBUG_SYSTEM, "<FP-CAM> Active FP cam was deactivated.")
			ENDIF
		#ENDIF
	ENDIF
	
	IF fpCam.bLockMiniMapAngle
		UNLOCK_MINIMAP_ANGLE()
		fpCam.bLockMiniMapAngle = FALSE
	ENDIF

	fpCam.vInitCamPos = <<0,0,0>>
	fpCam.vInitCamRot = <<0,0,0>>
	fpCam.fBaseCamFov = 0
	fpCam.iLookXLimit = 0
	fpCam.iLookYLimit = 0
	fpCam.iRollLimit = 0

	fpCam.vCamRotStickOffsetTarget = <<0,0,0>>
	fpCam.vCamRotFocusOffsetTarget = <<0,0,0>>
	fpCam.vCamRotOffsetCurrent = <<0,0,0>>
	fpCam.fCamFOVTarget = 0
	fpCam.fCamFOVCurrent = 0
ENDPROC

//eof
