//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Name:        NET_PEDS_FIXER_HQ.sch																				//
// Description: Functions to implement from net_peds_base.sch and look up tables to return FEATURE_FIXER ped data.		//
// Written by:  Paul Nettleton																							//
// Date:  		19/08/21																								//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#IF FEATURE_FIXER
USING "net_peds_base.sch"
USING "website_public.sch"
USING "net_peds_data_common.sch"
USING "net_simple_cutscene_common.sch"
USING "net_simple_interior.sch"

CONST_INT NET_PEDS_FIXER_HQ_IS_FRANKLIN_PRESENT				0
CONST_INT NET_PEDS_FIXER_HQ_IS_LAMAR_PRESENT				1

CONST_INT NET_PEDS_FIXER_LAMAR_VARIATION_0					0
CONST_INT NET_PEDS_FIXER_LAMAR_VARIATION_1					1


CONST_INT NET_PEDS_FIXER_HQ_CHANCE_OF_FRANKLIN_APPEARING 	100

ENUM FIXER_GUARD_LOCATION
	LOCATION_1,
	LOCATION_2
ENDENUM

FUNC VECTOR _GET_FIXER_HQ_PED_LOCAL_COORDS_BASE_POSITION(PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	SWITCH ePedLocation
		CASE PED_LOCATION_FIXER_HQ_HAWICK	RETURN <<384.813995, -60.727001, 102.362999>>	BREAK
		CASE PED_LOCATION_FIXER_HQ_ROCKFORD	RETURN <<-1020.286377, -427.301758, 62.861137>>	BREAK
		CASE PED_LOCATION_FIXER_HQ_SEOUL	RETURN <<-589.475708, -716.525940, 112.005058>>	BREAK
		CASE PED_LOCATION_FIXER_HQ_VESPUCCI	RETURN <<-1003.911011, -759.604004, 60.894192>>	BREAK
	ENDSWITCH
	RETURN <<388.902771, -61.801605, 106.577713>>
ENDFUNC

FUNC FLOAT _GET_FIXER_HQ_PED_LOCAL_HEADING_BASE_HEADING(PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	SWITCH ePedLocation
		CASE PED_LOCATION_FIXER_HQ_HAWICK		RETURN -110.0	BREAK
		CASE PED_LOCATION_FIXER_HQ_ROCKFORD		RETURN -63.05	BREAK
		CASE PED_LOCATION_FIXER_HQ_SEOUL		RETURN  89.85	BREAK
		CASE PED_LOCATION_FIXER_HQ_VESPUCCI		RETURN    0.0 	BREAK
	ENDSWITCH
	RETURN -110.15
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═════════════════════════════════╡ PED DATA ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC INT GET_LAMARS_VARIATION(INT iLayout)
	SWITCH iLayout
		CASE 0 
		CASE 1
		CASE 2
		CASE 3
		CASE 4
		CASE 5
		CASE 6
		CASE 7
			RETURN NET_PEDS_FIXER_LAMAR_VARIATION_0
		CASE 8
		CASE 9
		CASE 10
		CASE 11
		CASE 12
		CASE 13
		CASE 14
		CASE 15
			RETURN NET_PEDS_FIXER_LAMAR_VARIATION_1
		BREAK
	ENDSWITCH
	
	RETURN NET_PEDS_FIXER_LAMAR_VARIATION_0
ENDFUNC

PROC GET_RANDOM_STAFF_LOCATION_1_POSITION_AND_HEADING(VECTOR &vPosition, FLOAT &fHeading, INT &iActivity, SERVER_PED_DATA &ServerBD)

	INT i = ServerBD.iLayout
	SWITCH i
		CASE 0
		CASE 1
			vPosition = <<-6.4989, 2.0799, 9.0900>>
			fHeading = 352.1500
		BREAK
		CASE 2
		CASE 3
			vPosition = <<-6.4170, 4.1655, 9.1000>>
			fHeading = 177.9500
		BREAK
		CASE 4
		CASE 5
			vPosition = <<-8.4519, 4.1221, 9.0900>> 
			fHeading = 177.9500
		BREAK
		CASE 6		
		CASE 7
			vPosition = <<-8.5360, -2.5734, 9.0900>>
			fHeading = 5.0
		BREAK
		CASE 8
		CASE 9
			vPosition = <<-6.4920, -2.5910, 9.0900>>
			fHeading = 356.4500
		BREAK
		CASE 10
		CASE 11
		CASE 12
			vPosition 		= <<-0.5727, 5.2458, 9.5800>>
			fHeading 		= 90.9800
			iActivity = 	ENUM_TO_INT(PED_ACT_FIXER_TALK_ON_PHONE_FEMALE)
		BREAK
		CASE 13
		CASE 14
		CASE 15
			vPosition = <<-4.4740, -4.3750, 9.5800>>
			fHeading = 274.6500
			iActivity = 	ENUM_TO_INT(PED_ACT_FIXER_TALK_ON_PHONE_FEMALE)
		BREAK
			
	ENDSWITCH
ENDPROC

PROC GET_RANDOM_STAFF_LOCATION_2_POSITION_AND_HEADING(VECTOR &vPosition, FLOAT &fHeading, INT &iActivity, SERVER_PED_DATA &ServerBD)

	INT i = ServerBD.iLayout
	SWITCH i
		CASE 0
		CASE 1
			vPosition 		= <<-0.5727, 5.2458, 9.5800>>
			fHeading 		= 90.9800
			iActivity = 	ENUM_TO_INT(PED_ACT_FIXER_TALK_ON_PHONE)
		BREAK
		CASE 2
		CASE 3
			vPosition = <<-4.4740, -4.3750, 9.5800>>
			fHeading = 274.6500		
			iActivity = 	ENUM_TO_INT(PED_ACT_FIXER_TALK_ON_PHONE)
		BREAK
		CASE 4
		CASE 5
			vPosition = <<-6.4989, 2.0799, 9.0900>>
			fHeading = 352.1500			
		BREAK
		CASE 6		
		CASE 7
			vPosition = <<-6.4170, 4.1655, 9.1000>>
			fHeading = 177.9500			
		BREAK
		CASE 8
		CASE 9
			vPosition = <<-8.4519, 4.1221, 9.0900>> 
			fHeading = 177.9500						
		BREAK
		CASE 10
		CASE 11
		CASE 12
			vPosition = <<-8.5360, -2.5734, 9.0900>>
			fHeading = 5.0			
		BREAK
		CASE 13
		CASE 14
		CASE 15
			vPosition = <<-6.4920, -2.5910, 9.0900>>
			fHeading = 356.4500				
		BREAK
			
	ENDSWITCH
ENDPROC

PROC GET_RANDOM_STAFF_LOCATION_3_POSITION_AND_HEADING(VECTOR &vPosition, FLOAT &fHeading, INT &iActivity, SERVER_PED_DATA &ServerBD)
	INT i = ServerBD.iLayout
	SWITCH i
		CASE 0
		CASE 1
			vPosition = <<-8.5360, -2.5734, 9.0900>>
			fHeading = 5.0		
		BREAK
		CASE 2
		CASE 3
			vPosition = <<-6.4920, -2.5910, 9.0900>>
			fHeading = 356.4500			
		BREAK
		CASE 4
		CASE 5
			vPosition 		= <<-0.5727, 5.2458, 9.5800>>
			fHeading 		= 90.9800			
			iActivity = 	ENUM_TO_INT(PED_ACT_FIXER_TALK_ON_PHONE_FEMALE)			
		BREAK
		CASE 6		
		CASE 7
			vPosition = <<-4.4740, -4.3750, 9.5800>>
			fHeading = 274.6500		
			iActivity = 	ENUM_TO_INT(PED_ACT_FIXER_TALK_ON_PHONE_FEMALE)			
		BREAK
		CASE 8
		CASE 9
			vPosition = <<-6.4989, 2.0799, 9.0900>>
			fHeading = 352.1500								
		BREAK
		CASE 10
		CASE 11
		CASE 12
			vPosition = <<-6.4170, 4.1655, 9.1000>>
			fHeading = 177.9500			
				
		BREAK
		CASE 13
		CASE 14
		CASE 15
			vPosition = <<-8.4519, 4.1221, 9.0900>> 
			fHeading = 177.9500				
		BREAK
			
	ENDSWITCH
ENDPROC

PROC GET_RANDOM_STAFF_LOCATION_4_POSITION_AND_HEADING(VECTOR &vPosition, FLOAT &fHeading, INT &iActivity, SERVER_PED_DATA &ServerBD)
	INT i = ServerBD.iLayout
	SWITCH i
		CASE 0
		CASE 1				
			vPosition = <<-8.4519, 4.1221, 9.0900>> 
			fHeading = 177.9500		
		BREAK
		CASE 2
		CASE 3
			vPosition = <<-8.5360, -2.5734, 9.0900>>
			fHeading = 5.0					
		BREAK
		CASE 4
		CASE 5
			vPosition = <<-6.4920, -2.5910, 9.0900>>
			fHeading = 356.4500	
		BREAK
		CASE 6		
		CASE 7
			vPosition 		= <<-0.5727, 5.2458, 9.5800>>
			fHeading 		= 90.9800			
			iActivity = 	ENUM_TO_INT(PED_ACT_FIXER_TALK_ON_PHONE)		
		BREAK
		CASE 8
		CASE 9
			vPosition = <<-4.4740, -4.3750, 9.5800>>
			fHeading = 274.6500		
			iActivity = 	ENUM_TO_INT(PED_ACT_FIXER_TALK_ON_PHONE)								
		BREAK
		CASE 10
		CASE 11
		CASE 12
			vPosition = <<-6.4989, 2.0799, 9.0900>>
			fHeading = 352.1500			
		BREAK
		CASE 13
		CASE 14
		CASE 15
			vPosition = <<-6.4170, 4.1655, 9.1000>>
			fHeading = 177.9500					
		BREAK
			
	ENDSWITCH
ENDPROC


PROC GET_RANDOM_GUARD_LOCATION_1_POSITION_AND_HEADING(VECTOR &vPosition, FLOAT &fHeading, SERVER_PED_DATA &ServerBD)
	INT i = ServerBD.iLayout
	SWITCH i
		CASE 0
		CASE 2
		CASE 4
		CASE 6
		CASE 8
		CASE 10
		CASE 12
		CASE 14			
			vPosition = <<2.2630, 4.1013, 4.8050>>
			fHeading = 318.0800
		BREAK
		CASE 1
		CASE 3
		CASE 5
		CASE 7
		CASE 9
		CASE 11
		CASE 13
		CASE 15
			vPosition = <<3.0730, -1.3787, 0.9948>>
			fHeading =228.380
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_RANDOM_GUARD_LOCATION_2_POSITION_AND_HEADING(VECTOR &vPosition, FLOAT &fHeading, SERVER_PED_DATA &ServerBD)
	INT i = ServerBD.iLayout
	SWITCH i
		CASE 0
		CASE 1
		CASE 4
		CASE 5
		CASE 8
		CASE 9
		CASE 12
		CASE 13			
			vPosition 		= <<10.8030, -7.2787, 0.9950>>
			fHeading 		= 268.2800
		BREAK
		CASE 2
		CASE 3
		CASE 6
		CASE 7
		CASE 10
		CASE 11
		CASE 14
		CASE 15
			vPosition = <<-1.4570, -16.2687, 4.7850>>
			fHeading = 207.1800
		BREAK
	ENDSWITCH
ENDPROC

FUNC FIXER_GUARD_LOCATION GET_RANDOM_GUARD_LOCATION_3_LOCATION(SERVER_PED_DATA &ServerBD)
	INT i = ServerBD.iLayout
	SWITCH i
		CASE 0
		CASE 1
		CASE 2
		CASE 3
		CASE 8
		CASE 9
		CASE 10
		CASE 11			
			RETURN LOCATION_1
		BREAK
		CASE 4
		CASE 5
		CASE 6
		CASE 7
		CASE 12
		CASE 13
		CASE 14
		CASE 15
			RETURN LOCATION_2
		BREAK
	ENDSWITCH
	
	RETURN LOCATION_1
ENDFUNC

FUNC PED_ACTIVITIES GET_RANDOM_GUARD_LOCATION_3_ACTIVITY(SERVER_PED_DATA &ServerBD)
	IF GET_RANDOM_GUARD_LOCATION_3_LOCATION(ServerBD) = LOCATION_1
		RETURN PED_ACT_FIXER_TALK_ON_PHONE
	ENDIF
	RETURN PED_ACT_FIXER_LEAN_ON_RAIL
ENDFUNC

PROC GET_RANDOM_GUARD_LOCATION_3_POSITION_AND_HEADING(VECTOR &vPosition, FLOAT &fHeading, SERVER_PED_DATA &ServerBD)
	IF GET_RANDOM_GUARD_LOCATION_3_LOCATION(ServerBD) = LOCATION_1
		vPosition 		= <<2.9230, 8.2713, 0.1750>>
		fHeading 		= 263.6800
	ELSE
		vPosition = <<9.0930, -5.9987, 3.7950>>
		fHeading = 92.180
	ENDIF
ENDPROC

PROC GET_RANDOM_GUARD_LOCATION_4_POSITION_AND_HEADING(VECTOR &vPosition, FLOAT &fHeading, SERVER_PED_DATA &ServerBD)
	INT i = ServerBD.iLayout
	SWITCH i
		CASE 0
		CASE 1
		CASE 2
		CASE 3
		CASE 4
		CASE 5
		CASE 6
		CASE 7			
			vPosition = <<0.3530, 0.0113, 4.7950>>
			fHeading = 311.9800
		BREAK
		CASE 8
		CASE 9
		CASE 10
		CASE 11
		CASE 12
		CASE 13
		CASE 14
		CASE 15
			vPosition = <<1.7030, -1.1887, 4.7950>>
			fHeading = 160.00
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_FRANKLIN_ACTIVITY(INT iLayout, INT &iActivity)
	SWITCH iLayout
		CASE 0
		CASE 1
		CASE 2
		CASE 3
		CASE 4
			iActivity 		 = ENUM_TO_INT(PED_ACT_FIXER_FRANKLIN_0)
		BREAK
		CASE 5
		CASE 6
		CASE 7
		CASE 8
		CASE 9
			iActivity 		=  ENUM_TO_INT(PED_ACT_FIXER_FRANKLIN_1)
		BREAK
		CASE 10
		CASE 11
		CASE 12
		CASE 13
		CASE 14
		CASE 15
			iActivity 		 =  ENUM_TO_INT(PED_ACT_FIXER_FRANKLIN_2)
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_RANDOM_FRANKLIN_POSITION_HEADING_AND_ACTIVITY(PEDS_DATA &Data, SERVER_PED_DATA &ServerBD)

	INT iLayout =  ServerBD.iLayout
	
	IF GET_PLAYER_NUM_FIXER_SECURITY_CONTRACTS_COMPLETED(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()) = 0
		iLayout = 10 //with 0 contracts, default to keyboard position
	ENDIF
	
	GET_FRANKLIN_ACTIVITY(iLayout, Data.iActivity)
	
	SWITCH INT_TO_ENUM(PED_ACTIVITIES, Data.iActivity)
		CASE PED_ACT_FIXER_FRANKLIN_0		
			Data.vPosition		 = <<-3.8070, 9.9713, 3.8250>>
			Data.vRotation.z	 = 277.7000
		BREAK
		CASE PED_ACT_FIXER_FRANKLIN_1
			Data.vPosition 		= <<2.1860, 9.6232, 3.8150>>
			Data.vRotation.z	= 90.0
			SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
		BREAK
		CASE PED_ACT_FIXER_FRANKLIN_2
			Data.vPosition		 = <<-3.2170, 9.8613, 3.8250>>
			Data.vRotation.z 	 = 270.0
		BREAK
	ENDSWITCH
	
ENDPROC

PROC GET_RANDOM_LAMAR_POSITION_AND_HEADING(VECTOR &vPosition, FLOAT &fHeading, SERVER_PED_DATA &ServerBD)
	INT i = ServerBD.iLayout
	SWITCH GET_LAMARS_VARIATION(i)
		CASE NET_PEDS_FIXER_LAMAR_VARIATION_0
			vPosition = <<-5.0200, 9.4700, 8.5958>>
			fHeading = 90.0
		BREAK
		CASE NET_PEDS_FIXER_LAMAR_VARIATION_1
			vPosition = <<-7.5100, 7.3200, 8.6000>>
			fHeading = 351.30
		BREAK
	ENDSWITCH
ENDPROC

FUNC CHOP_CREATION_STATE GET_RANDOM_CHOP_POSITION_AND_HEADING(SERVER_PED_DATA &ServerBD)
	INT i = ServerBD.iLayout
	SWITCH i
		CASE 0
		CASE 1
		CASE 2
		CASE 3
		CASE 4
		CASE 5				
			RETURN POSITION_1
		BREAK
		CASE 6
		CASE 7
		CASE 8
		CASE 9
		CASE 10	
			RETURN POSITION_4
		BREAK
		CASE 11
		CASE 12
		CASE 13
		CASE 14
		CASE 15
			RETURN POSITION_7
		BREAK
	ENDSWITCH
	
	RETURN POSITION_1
ENDFUNC

FUNC CHOP_CREATION_STATE GET_RANDOM_CHOP_POSITION_AND_HEADING_WITH_PERSONAL_QUARTERS(SERVER_PED_DATA &ServerBD)
	INT i = ServerBD.iLayout
	SWITCH i
		CASE 0
		CASE 1
		CASE 2
		CASE 3		
			RETURN POSITION_1
		BREAK
		CASE 4
		CASE 5
		CASE 6	
			RETURN POSITION_4
		BREAK
		CASE 7
		CASE 8
		CASE 9			
			RETURN POSITION_5
		BREAK
		CASE 10
		CASE 11
		CASE 12	
			RETURN POSITION_6
		BREAK
		CASE 13
		CASE 14
		CASE 15
			RETURN POSITION_7
		BREAK
	ENDSWITCH
	
	RETURN POSITION_1
ENDFUNC

FUNC CHOP_CREATION_STATE GET_RANDOM_CHOP_WIH_LAMAR_POSITION_AND_HEADING(SERVER_PED_DATA &ServerBD)
	INT i = ServerBD.iLayout
	SWITCH i
		CASE 0
		CASE 1
		CASE 2
		CASE 3
		CASE 4
		CASE 5
		CASE 6
		CASE 7
			RETURN POSITION_1
		BREAK
		CASE 8
		CASE 9
		CASE 10
		CASE 11
			RETURN POSITION_3
		BREAK
		CASE 12
		CASE 13
			RETURN POSITION_4
		BREAK
		CASE 14
		CASE 15
			RETURN POSITION_7
		BREAK
	ENDSWITCH
	
	RETURN POSITION_1
ENDFUNC

FUNC CHOP_CREATION_STATE GET_RANDOM_CHOP_WIH_LAMAR_POSITION_AND_HEADING_WITH_PERSONAL_QUARTERS(SERVER_PED_DATA &ServerBD)
	INT i = ServerBD.iLayout
	SWITCH i
		CASE 0
		CASE 1
		CASE 2
		CASE 3
			RETURN POSITION_1
		BREAK
		CASE 4
		CASE 5
		CASE 6
			RETURN POSITION_3
		BREAK
		CASE 7
		CASE 8
		CASE 9
			RETURN POSITION_4
		BREAK
		CASE 10
		CASE 11
		CASE 12
			RETURN POSITION_5
		BREAK
		CASE 13
		CASE 14
		CASE 15
			RETURN POSITION_6
		BREAK
	ENDSWITCH
	
	RETURN POSITION_1
ENDFUNC

PROC _SET_FIXER_HQ_PED_DATA_LAYOUT(PED_LOCATIONS ePedLocation, PEDS_DATA &Data, SERVER_PED_DATA &ServerBD, INT iPed, BOOL bSetPedArea = TRUE)
	
	SWITCH iPed
	// Local Peds
		CASE 0
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_HQ_RECEPTIONIST)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_FIXER_HQ_RECEPTION)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 16842752
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16908288
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<5.8590, 5.9914, 0.5850>>
					Data.vRotation 										= <<0.0000, 0.0000, 15.0950>>
					CLEAR_PEDS_BITSET(Data.iBS)
					
					IF g_bHasPlayerCompletedAnySecurityContracts
					#IF IS_DEBUG_BUILD
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceReceptionistInFixerHQ")
					#ENDIF
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					ELSE
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
					
					IF NOT (GET_INTERIOR_FLOOR_INDEX() = ciFIXER_HQ_FLOOR_OFFICE)
						//Skip if we are not in the office floor.
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 1
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_IMANI)  
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_IMANI)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= -1
					Data.iPackedTexture[1] 								= -1
					Data.iPackedTexture[2] 								= -1
					Data.vPosition 										= <<-7.3487, -0.3719, 8.6058>>
					Data.vRotation 										= <<0.0000, 0.0000, 0.0>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					//SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEAD_TRACKING_ONLY_WITH_BASE_ANIM)
					IF NOT (GET_INTERIOR_FLOOR_INDEX() = ciFIXER_HQ_FLOOR_OFFICE)
						//Skip if we are not in the office floor.
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 2
		IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_OFFICE_FEMALE_1)  
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_FIXER_DESK_WORKER_FEMALE)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 16973825
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16842752
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					GET_RANDOM_STAFF_LOCATION_1_POSITION_AND_HEADING(Data.vPosition, Data.vRotation.z, Data.iActivity, ServerBD)
					CLEAR_PEDS_BITSET(Data.iBS)
					
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					
					IF g_bHasPlayerCompletedAnySecurityContracts
					#IF IS_DEBUG_BUILD
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceAmbientWorkers1InFixerHQ")
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceAmbientWorkers2InFixerHQ")
					#ENDIF
						//Setup any ped bits here.
					ELSE
						//Skip the ped if we have not done any contracts.
						SET_PEDS_BIT(Data.iBs, BS_PED_DATA_SKIP_PED)
					ENDIF
					
					IF NOT (GET_INTERIOR_FLOOR_INDEX() = ciFIXER_HQ_FLOOR_OFFICE)
						//Skip if we are not in the office floor.
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 3
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_OFFICE_MALE)  
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_FIXER_DESK_WORKER_MALE)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 33554432
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					GET_RANDOM_STAFF_LOCATION_2_POSITION_AND_HEADING(Data.vPosition, Data.vRotation.z, Data.iActivity, ServerBD)
					CLEAR_PEDS_BITSET(Data.iBS)
					
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					
					IF g_bHasPlayerCompletedAnySecurityContracts
					#IF IS_DEBUG_BUILD
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceAmbientWorkers1InFixerHQ")
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceAmbientWorkers2InFixerHQ")
					#ENDIF
						//Setup any ped bits here.
					ELSE
						//Skip the ped if we have not done any contracts.
						SET_PEDS_BIT(Data.iBs, BS_PED_DATA_SKIP_PED)
					ENDIF
					
					IF NOT (GET_INTERIOR_FLOOR_INDEX() = ciFIXER_HQ_FLOOR_OFFICE)
						//Skip if we are not in the office floor.
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 4
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_OFFICE_FEMALE_2)  
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_FIXER_DESK_WORKER_FEMALE)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 16842752
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16908290
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					GET_RANDOM_STAFF_LOCATION_3_POSITION_AND_HEADING(Data.vPosition, Data.vRotation.z, Data.iActivity, ServerBD)
					CLEAR_PEDS_BITSET(Data.iBS)
					
					//Hide this worker as they are in the middle of a scene.
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					
					IF HAS_PLAYER_COMPLETED_FIXER_STORY_MISSION(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN(), FSM_INTRO) //One contract should be done if this is true as well.
					#IF IS_DEBUG_BUILD
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceAmbientWorkers2InFixerHQ")
					#ENDIF
						//Setup any ped bits here.
					ELSE
						//Skip the ped if we have not done any contracts.
						SET_PEDS_BIT(Data.iBs, BS_PED_DATA_SKIP_PED)
					ENDIF
					
					IF NOT (GET_INTERIOR_FLOOR_INDEX() = ciFIXER_HQ_FLOOR_OFFICE)
						//Skip if we are not in the office floor.
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 5
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_OFFICE_MALE)  
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_FIXER_DESK_WORKER_MALE)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 16842753
					Data.iPackedDrawable[1]								= 65793
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777216
					Data.iPackedTexture[1] 								= 1
					Data.iPackedTexture[2] 								= 0
					GET_RANDOM_STAFF_LOCATION_4_POSITION_AND_HEADING(Data.vPosition, Data.vRotation.z, Data.iActivity, ServerBD)
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					
					IF HAS_PLAYER_COMPLETED_FIXER_STORY_MISSION(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN(), FSM_INTRO) //One contract should be done if this is true as well.
					#IF IS_DEBUG_BUILD
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceAmbientWorkers2InFixerHQ")
					#ENDIF
						//Setup any ped bits here.
					ELSE
						//Skip the ped if we have not done any contracts.
						SET_PEDS_BIT(Data.iBs, BS_PED_DATA_SKIP_PED)
					ENDIF
					
					IF NOT (GET_INTERIOR_FLOOR_INDEX() = ciFIXER_HQ_FLOOR_OFFICE)
						//Skip if we are not in the office floor.
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 6
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_FRANKLIN)  
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 256
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 65536
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0					
					CLEAR_PEDS_BITSET(Data.iBS)
					GET_RANDOM_FRANKLIN_POSITION_HEADING_AND_ACTIVITY(Data,  ServerBD)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)		
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEAD_TRACKING_ONLY_WITH_BASE_ANIM)
					
					
					IF NOT IS_BIT_SET(ServerBD.iBS, NET_PEDS_FIXER_HQ_IS_FRANKLIN_PRESENT)
						//Skip the ped if we have decided he should not appear.
						SET_PEDS_BIT(Data.iBs, BS_PED_DATA_SKIP_PED)
					ENDIF
					
					IF NOT (GET_INTERIOR_FLOOR_INDEX() = ciFIXER_HQ_FLOOR_OFFICE)
						//Skip if we are not in the office floor.
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 7
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_LAMAR)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_FIXER_LAMAR)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					
					GET_RANDOM_LAMAR_POSITION_AND_HEADING(Data.vPosition, Data.vRotation.z, ServerBD)
					
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEAD_TRACKING_ONLY_WITH_BASE_ANIM)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_VFX)
					
					IF NOT IS_BIT_SET(ServerBD.iBS, NET_PEDS_FIXER_HQ_IS_LAMAR_PRESENT)
					#IF IS_DEBUG_BUILD
					AND NOT GET_COMMANDLINE_PARAM_EXISTS("sc_ForceLamarInFixerHQ")
					#ENDIF
						SET_PEDS_BIT(Data.iBs, BS_PED_DATA_SKIP_PED)
					ENDIF
					
					IF NOT (GET_INTERIOR_FLOOR_INDEX() = ciFIXER_HQ_FLOOR_OFFICE)
						//Skip if we are not in the office floor.
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 8
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_HQ_SECURITY)
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_FIXER_HQ_SECURITY_DESK)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 0
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 0
					Data.iPackedTexture[1] 								= 0
					Data.iPackedTexture[2] 								= 0
					Data.vPosition 										= <<9.1190, 2.6278, 0.6150>>
					Data.vRotation 										= <<0.0000, 0.0000, 317.2050>>
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_HEAD_TRACKING_ENTITY)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HEAD_TRACKING_ONLY_WITH_BASE_ANIM)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_USE_SPEECH)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)					
					IF NOT (GET_INTERIOR_FLOOR_INDEX() = ciFIXER_HQ_FLOOR_OFFICE)
						//Skip if we are not in the office floor.
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 9
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_HQ_GUARD)  
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_FIXER_TEXTING)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 16777219
					Data.iPackedDrawable[1]								= 65536
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 50331648
					Data.iPackedTexture[1] 								= 771
					Data.iPackedTexture[2] 								= 0
					
					GET_RANDOM_GUARD_LOCATION_1_POSITION_AND_HEADING(Data.vPosition, Data.vRotation.z, ServerBD)
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					
					IF (GET_PLAYER_NUM_FIXER_SECURITY_CONTRACTS_COMPLETED(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()) > 4)
					#IF IS_DEBUG_BUILD
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceSecurityGuard1InHQ")
					#ENDIF
						//Setup any ped bits here.
					ELSE
						//Skip the ped if we have not done any contracts.
						SET_PEDS_BIT(Data.iBs, BS_PED_DATA_SKIP_PED)
					ENDIF
					
					IF NOT (GET_INTERIOR_FLOOR_INDEX() = ciFIXER_HQ_FLOOR_OFFICE)
						//Skip if we are not in the office floor.
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 10
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_HQ_GUARD)  
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_FIXER_STAND_ON_PHONE)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 33751041
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 16777216
					Data.iPackedTexture[0] 								= 50331648
					Data.iPackedTexture[1] 								= 514
					Data.iPackedTexture[2] 								= 0
					
					GET_RANDOM_GUARD_LOCATION_2_POSITION_AND_HEADING(Data.vPosition, Data.vRotation.z, ServerBD)
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					
					IF (GET_PLAYER_NUM_FIXER_SECURITY_CONTRACTS_COMPLETED(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()) > 9)
					#IF IS_DEBUG_BUILD
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceSecurityGuard2InHQ")
					#ENDIF
						//Setup any ped bits here.
					ELSE
						//Skip the ped if we have not done any contracts.
						SET_PEDS_BIT(Data.iBs, BS_PED_DATA_SKIP_PED)
					ENDIF
					
					IF NOT (GET_INTERIOR_FLOOR_INDEX() = ciFIXER_HQ_FLOOR_OFFICE)
						//Skip if we are not in the office floor.
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 11
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_HQ_GUARD)  
					Data.iActivity 										= ENUM_TO_INT(GET_RANDOM_GUARD_LOCATION_3_ACTIVITY(ServerBD))
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 16777216
					Data.iPackedDrawable[1]								= 0
					Data.iPackedDrawable[2]								= 0
					Data.iPackedTexture[0] 								= 16777216
					Data.iPackedTexture[1] 								= 3
					Data.iPackedTexture[2] 								= 16777216
					
					GET_RANDOM_GUARD_LOCATION_3_POSITION_AND_HEADING(Data.vPosition, Data.vRotation.z, ServerBD)
					
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					
					IF (GET_PLAYER_NUM_FIXER_SECURITY_CONTRACTS_COMPLETED(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()) > 24)
					#IF IS_DEBUG_BUILD
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceSecurityGuard3InHQ")
					#ENDIF
						//Setup any ped bits here.
					ELSE
						//Skip the ped if we have not done any contracts.
						SET_PEDS_BIT(Data.iBs, BS_PED_DATA_SKIP_PED)
					ENDIF
					
					IF NOT (GET_INTERIOR_FLOOR_INDEX() = ciFIXER_HQ_FLOOR_OFFICE)
						//Skip if we are not in the office floor.
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 12
			IF bSetPedArea
				Data.cullingData.ePedArea 								= PED_AREA_PERMANENT
			ENDIF

			SWITCH Data.cullingData.ePedArea
				CASE PED_AREA_PERMANENT
				CASE PED_AREA_ONE
				CASE PED_AREA_TWO
					Data.iPedModel 										= ENUM_TO_INT(PED_MODEL_FIXER_HQ_GUARD)  
					Data.iActivity 										= ENUM_TO_INT(PED_ACT_FIXER_DRINK)
					Data.iLevel 										= 2
					Data.iPackedDrawable[0]								= 33619970
					Data.iPackedDrawable[1]								= 65536
					Data.iPackedDrawable[2]								= 16777216
					Data.iPackedTexture[0] 								= 16777216
					Data.iPackedTexture[1] 								= 512
					Data.iPackedTexture[2] 								= 0
					
					GET_RANDOM_GUARD_LOCATION_4_POSITION_AND_HEADING(Data.vPosition, Data.vRotation.z, ServerBD)
					CLEAR_PEDS_BITSET(Data.iBS)
					SET_PEDS_BIT(Data.iBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
					
					IF (GET_PLAYER_NUM_FIXER_SECURITY_CONTRACTS_COMPLETED(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()) > 49)
					#IF IS_DEBUG_BUILD
					OR GET_COMMANDLINE_PARAM_EXISTS("sc_ForceSecurityGuard4InHQ")
					#ENDIF
						//Setup any ped bits here.
					ELSE
						//Skip the ped if we have not done any contracts.
						SET_PEDS_BIT(Data.iBs, BS_PED_DATA_SKIP_PED)
					ENDIF
					
					IF NOT (GET_INTERIOR_FLOOR_INDEX() = ciFIXER_HQ_FLOOR_OFFICE)
						//Skip if we are not in the office floor.
						SET_PEDS_BIT(Data.iBS, BS_PED_DATA_SKIP_PED)
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH	
	
	IF NOT IS_VECTOR_ZERO(Data.vPosition)
		Data.vPosition = TRANSFORM_LOCAL_COORDS_TO_WORLD_COORDS(_GET_FIXER_HQ_PED_LOCAL_COORDS_BASE_POSITION(ePedLocation), _GET_FIXER_HQ_PED_LOCAL_HEADING_BASE_HEADING(ePedLocation), Data.vPosition)
	ENDIF
	
	// Transform the local heading into world heading.
	IF (Data.vRotation.z != -1.0)
		Data.vRotation.z = TRANSFORM_LOCAL_HEADING_TO_WORLD_HEADING(_GET_FIXER_HQ_PED_LOCAL_HEADING_BASE_HEADING(ePedLocation), Data.vRotation.z)
	ENDIF

ENDPROC

PROC _SET_FIXER_HQ_PED_PROP_INDEXES(PED_INDEX &PedID, INT iPed, INT iLayout, PED_MODELS ePedModel)
	UNUSED_PARAMETER(ePedModel)
	UNUSED_PARAMETER(iLayout)
	SWITCH iPed	
		CASE 3
			SET_PED_PROP_INDEX(PedID, ANCHOR_EYES, 0, 0)
		BREAK
	ENDSWITCH 
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════╡ FUNCTIONS TO IMPLEMENT ╞═════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

FUNC BOOL _SHOULD_FIXER_HQ_PED_SCRIPT_LAUNCH()
	RETURN IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
ENDFUNC

FUNC BOOL _IS_FIXER_HQ_PARENT_A_SIMPLE_INTERIOR
	RETURN TRUE
ENDFUNC

PROC _SET_FIXER_HQ_PED_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD, PEDS_DATA &Data, INT iPed, INT iLayout, BOOL bSetPedArea = TRUE)
	UNUSED_PARAMETER(ServerBD)
	UNUSED_PARAMETER(ePedLocation)
	UNUSED_PARAMETER(iLayout)
	PRINTLN("_SET_FIXER_HQ_PED_DATA PED ", iPed)
	PRINTLN("_SET_FIXER_HQ_PED_DATA USING LAYOUT ", iLayout)
	_SET_FIXER_HQ_PED_DATA_LAYOUT(ePedLocation, Data, ServerBD, iPed, bSetPedArea)
ENDPROC

FUNC INT _GET_FIXER_HQ_NETWORK_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	UNUSED_PARAMETER(ePedLocation)
	UNUSED_PARAMETER(ServerBD)
	RETURN 0
ENDFUNC

FUNC INT _GET_FIXER_HQ_LOCAL_PED_TOTAL(SERVER_PED_DATA &ServerBD, PED_LOCATIONS ePedLocation = PED_LOCATION_INVALID)
	
	INT iPed
	INT iActivePeds = 0
	PEDS_DATA tempData
	
	REPEAT MAX_NUM_TOTAL_LOCAL_PEDS iPed
		tempData.vPosition = NULL_VECTOR()
		_SET_FIXER_HQ_PED_DATA(ePedLocation, ServerBD, tempData, iPed, ServerBD.iLayout)
		IF NOT IS_VECTOR_ZERO(tempData.vPosition)
			iActivePeds++
		ELSE
			BREAKLOOP
		ENDIF
	ENDREPEAT
	
	RETURN iActivePeds
ENDFUNC

FUNC INT _GET_FIXER_HQ_SERVER_PED_LAYOUT_TOTAL()
	RETURN 16
ENDFUNC

FUNC INT _GET_FIXER_HQ_SERVER_PED_LAYOUT()	
	RETURN GET_RANDOM_INT_IN_RANGE(0, 16)
ENDFUNC

FUNC INT _GET_FIXER_HQ_SERVER_PED_LEVEL()
	RETURN 2  // Ped levels 0, 1 & 2 spawn by default
ENDFUNC

PROC _SET_FIXER_HQ_PED_SERVER_DATA(PED_LOCATIONS ePedLocation, SERVER_PED_DATA &ServerBD)
	UNUSED_PARAMETER(ePedLocation)
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		ServerBD.iLayout 			= _GET_FIXER_HQ_SERVER_PED_LAYOUT()
		ServerBD.iLevel 			= _GET_FIXER_HQ_SERVER_PED_LEVEL()
		ServerBD.iMaxLocalPeds 		= _GET_FIXER_HQ_LOCAL_PED_TOTAL(ServerBD)
		ServerBD.iMaxNetworkPeds	= _GET_FIXER_HQ_NETWORK_PED_TOTAL(ServerBD)
		
		//Do we want Franklin? 
		INT iPercentageChanceFranklinAppears = NET_PEDS_FIXER_HQ_CHANCE_OF_FRANKLIN_APPEARING //Default 100
		IF (GET_PLAYER_NUM_FIXER_SECURITY_CONTRACTS_COMPLETED(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()) > 0)
			iPercentageChanceFranklinAppears = g_sMPTunables.iFIXER_HQ_CHANCE_FRANKLIN_APPEARS
		ENDIF
		
		IF (GET_PLAYER_CURRENT_FIXER_STRAND(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()) = FS_BILLIONAIRE_GAMES
		AND HAS_PLAYER_COMPLETED_FIXER_STORY_MISSION(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN(), FSM_BILLIONAIRE_GAMES_INVESTIGATION_2)
		AND NOT HAS_PLAYER_COMPLETED_FIXER_STORY_MISSION(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN(), FSM_BILLIONAIRE_GAMES_MISSION))
			iPercentageChanceFranklinAppears = 0
		ENDIF

		IF (GET_RANDOM_INT_IN_RANGE(0, 100) < iPercentageChanceFranklinAppears)
			SET_BIT(ServerBD.iBS, NET_PEDS_FIXER_HQ_IS_FRANKLIN_PRESENT)
			
			//Lamar only appears in the interior if Franklin is there
			IF GET_RANDOM_INT_IN_RANGE(0,100) < g_sMPTunables.iFIXER_HQ_CHANCE_LAMAR_APPEARS   
			AND HAS_PLAYER_VIEWED_FIXER_HQ_POST_DATALEAK_MOCAP(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN())
				SET_BIT(ServerBD.iBS, NET_PEDS_FIXER_HQ_IS_LAMAR_PRESENT)
			ENDIF
		ENDIF
		
		IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_EXIT_INTERIOR_FRANKLIN_PRESENT)
			CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_EXIT_INTERIOR_FRANKLIN_PRESENT)
			SET_BIT(ServerBD.iBS, NET_PEDS_FIXER_HQ_IS_FRANKLIN_PRESENT) 
		ENDIF
		IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_EXIT_INTERIOR_LAMAR_PRESENT)
			CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_EXIT_INTERIOR_LAMAR_PRESENT)
			SET_BIT(ServerBD.iBS, NET_PEDS_FIXER_HQ_IS_LAMAR_PRESENT)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(ServerBD.iBS, NET_PEDS_FIXER_HQ_IS_FRANKLIN_PRESENT)
		//Save Franklin's creation location. 
		IF GET_PLAYER_NUM_FIXER_SECURITY_CONTRACTS_COMPLETED(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN()) = 0
			g_iFixerFranklinActivity = ENUM_TO_INT(PED_ACT_FIXER_FRANKLIN_2)
		ELSE
			GET_FRANKLIN_ACTIVITY(ServerBD.iLayout, g_iFixerFranklinActivity)	
		ENDIF
		SET_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_FRANKLIN_PRESENT)
	ELSE
		CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_FRANKLIN_PRESENT)
		g_iFixerFranklinActivity = -1
	ENDIF
	
	IF IS_BIT_SET(ServerBD.iBS, NET_PEDS_FIXER_HQ_IS_LAMAR_PRESENT)
		//Save Lamar's creation location. 
		SWITCH GET_LAMARS_VARIATION(ServerBD.iLayout)
			CASE NET_PEDS_FIXER_LAMAR_VARIATION_0
				g_eFHQLamarCreationState = FHQ_LAMAR_POSITION_1
			BREAK
			CASE NET_PEDS_FIXER_LAMAR_VARIATION_1
				g_eFHQLamarCreationState = FHQ_LAMAR_POSITION_2
			BREAK
		ENDSWITCH
		SET_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_LAMAR_PRESENT)
	ELSE
		CLEAR_BIT(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_LAMAR_PRESENT)
		g_eFHQLamarCreationState = FHQ_LAMAR_NO_CREATED
	ENDIF
	
	g_iAnimLayout = ServerBD.iLayout //Save our anim layout.
	IF IS_BIT_SET(ServerBD.iBS, NET_PEDS_FIXER_HQ_IS_LAMAR_PRESENT)
		IF HAS_PLAYER_PURCHASED_FIXER_HQ_UPGRADE_PERSONAL_QUARTERS(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN())
			g_eChopCreationState = GET_RANDOM_CHOP_WIH_LAMAR_POSITION_AND_HEADING_WITH_PERSONAL_QUARTERS(ServerBD)
		ELSE
			g_eChopCreationState = GET_RANDOM_CHOP_WIH_LAMAR_POSITION_AND_HEADING(ServerBD)
		ENDIF
	ELIF IS_BIT_SET(ServerBD.iBS, NET_PEDS_FIXER_HQ_IS_FRANKLIN_PRESENT)
		IF HAS_PLAYER_PURCHASED_FIXER_HQ_UPGRADE_PERSONAL_QUARTERS(GET_OWNER_OF_SIMPLE_INTERIOR_LOCAL_PLAYER_IS_IN())
			g_eChopCreationState = GET_RANDOM_CHOP_POSITION_AND_HEADING_WITH_PERSONAL_QUARTERS(ServerBD)
		ELSE
			g_eChopCreationState = GET_RANDOM_CHOP_POSITION_AND_HEADING(ServerBD)
		ENDIF
	ELSE
		g_eChopCreationState = NO_CREATED
	ENDIF
	
	#IF IS_DEBUG_BUILD
	//Force chop under the desk.
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceChopOfficeFarRight")
		g_eChopCreationState = POSITION_1
	ENDIF
//	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceChopOfficeLeft")
//		g_eChopCreationState = POSITION_2
//	ENDIF
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceChopWithLamar")
		g_eChopCreationState = POSITION_3
	ENDIF
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceChopUnderDesk")
		g_eChopCreationState = POSITION_4
	ENDIF
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceChopOnBed")
		g_eChopCreationState = POSITION_5	
	ENDIF
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceChopOnCoach")
		g_eChopCreationState = POSITION_6	
	ENDIF
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceChopOnDogBed")
		g_eChopCreationState = POSITION_7
	ENDIF
	
	#ENDIF
		
	PRINTLN("[AM_MP_PEDS] _SET_FIXER_HQ_PED_SERVER_DATA - Layout: ", ServerBD.iLayout)
	PRINTLN("[AM_MP_PEDS] _SET_FIXER_HQ_PED_SERVER_DATA - Level: ", ServerBD.iLevel)
	PRINTLN("[AM_MP_PEDS] _SET_FIXER_HQ_PED_SERVER_DATA - Max Local Peds: ", ServerBD.iMaxLocalPeds)
	PRINTLN("[AM_MP_PEDS] _SET_FIXER_HQ_PED_SERVER_DATA - Max Network Peds: ", ServerBD.iMaxNetworkPeds)
ENDPROC

FUNC BOOL _IS_PLAYER_IN_FIXER_HQ_PARENT_PROPERTY(PLAYER_INDEX playerID)
	RETURN IS_PLAYER_IN_FIXER_HQ(playerID)
ENDFUNC

FUNC BOOL _HAS_FIXER_HQ_PED_BEEN_CREATED(PEDS_DATA &Data, INT iLevel)
	
	IF (IS_ENTITY_ALIVE(Data.PedID)
		AND (GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK 
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK
		OR GET_SCRIPT_TASK_STATUS(Data.PedID, SCRIPT_TASK_START_SCENARIO_IN_PLACE) = PERFORMING_TASK))
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_CHILD_PED)
	OR IS_PEDS_BIT_SET(Data.iBS, BS_PED_DATA_SKIP_PED)
	OR (Data.iLevel > iLevel)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED MODELS ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _SET_FIXER_HQ_LOCAL_PED_PROPERTIES(PED_INDEX &PedID, INT iPed)
	UNUSED_PARAMETER(iPed)
	SET_ENTITY_CAN_BE_DAMAGED(PedID, FALSE)
	SET_PED_AS_ENEMY(PedID, FALSE)
	
	IF GET_ENTITY_MODEL(PedID) != A_C_CHOP
		SET_CURRENT_PED_WEAPON(PedID, WEAPONTYPE_UNARMED, TRUE)
	ENDIF
		
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedID, TRUE)
	SET_PED_RESET_FLAG(PedID, PRF_DisablePotentialBlastReactions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_UseKinematicModeWhenStationary, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromExplosions, TRUE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
	
	SET_PED_CAN_EVASIVE_DIVE(PedID, FALSE)
	SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(PedID, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PedID, FALSE)
	SET_PED_CAN_RAGDOLL(PedID, FALSE)
	SET_PED_CONFIG_FLAG(PedID, PCF_DisableExplosionReactions, TRUE)
	CLEAR_PED_TASKS(PedID)
ENDPROC

PROC _SET_FIXER_HQ_NETWORK_PED_PROPERTIES(NETWORK_INDEX &NetworkPedID, INT &iPedDataBS[PEDS_DATA_BITSET_ARRAY_SIZE])
	NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NETWORK_GET_ENTITY_FROM_NETWORK_ID(NetworkPedID), TRUE)
	SET_NETWORK_ID_CAN_MIGRATE(NetworkPedID, FALSE)
	
	IF IS_PEDS_BIT_SET(iPedDataBS, BS_PED_DATA_HIDE_PED_IN_CUTSCENE)
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, FALSE)
	ELSE
		SET_NETWORK_ID_VISIBLE_IN_CUTSCENE(NetworkPedID, TRUE)
	ENDIF
ENDPROC


//╒═════════════════════════════════════════════════════════════════════════════╕
//╞═══════════════════════════════╡ PED SPEECH ╞════════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛
	
PROC _SET_FIXER_HQ_PED_SPEECH_DATA(SPEECH_DATA &SpeechData, INT iLayout, INT iArrayID, INT &iSpeechPedID[], BOOL bNetworkPed = FALSE)
	UNUSED_PARAMETER(iLayout)
	
	IF (NOT bNetworkPed)
		SWITCH iArrayID
			CASE 0
				SpeechData.iPedID							= 1
				SpeechData.fGreetSpeechDistance				= 2.4
				SpeechData.fByeSpeechDistance				= 3.9
				SpeechData.fListenDistance					= 7.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
				SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_TIMED_GREETING)
			BREAK
			CASE 1
				SpeechData.iPedID							= 6
				SpeechData.fGreetSpeechDistance				= 4.0
				SpeechData.fByeSpeechDistance				= 5.5
				SpeechData.fListenDistance					= 7.0
				
				SpeechData.vAreaPos1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_FIXER_HQ_INTERIOR_POSITION(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()), GET_INTERIOR_FLOOR_INDEX()), GET_FIXER_HQ_INTERIOR_HEADING(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()), GET_INTERIOR_FLOOR_INDEX()), <<-1008.614197,-749.760986,64.727982>> - <<-1003.911011, -759.604004, 60.894192>>)
				SpeechData.vAreaPos2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_FIXER_HQ_INTERIOR_POSITION(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()), GET_INTERIOR_FLOOR_INDEX()), GET_FIXER_HQ_INTERIOR_HEADING(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()), GET_INTERIOR_FLOOR_INDEX()), <<-1001.227722,-749.933777,66.586678>> - <<-1003.911011, -759.604004, 60.894192>>)
				SpeechData.fWidth = 6.750000
				
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
				SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_TIMED_GREETING)
				SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_USE_AREA)
			BREAK
			CASE 2
				SpeechData.iPedID							= 7
				SpeechData.fGreetSpeechDistance				= 4.3
				SpeechData.fByeSpeechDistance				= 5.0
				SpeechData.fListenDistance					= 7.0
				
				SpeechData.vAreaPos1 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_FIXER_HQ_INTERIOR_POSITION(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()), GET_INTERIOR_FLOOR_INDEX()), GET_FIXER_HQ_INTERIOR_HEADING(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()), GET_INTERIOR_FLOOR_INDEX()), <<-1012.001160,-747.814331,69.494141>> - <<-1003.911011, -759.604004, 60.894192>>)
				SpeechData.vAreaPos2 = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_FIXER_HQ_INTERIOR_POSITION(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()), GET_INTERIOR_FLOOR_INDEX()), GET_FIXER_HQ_INTERIOR_HEADING(GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID()), GET_INTERIOR_FLOOR_INDEX()), <<-1011.833252,-752.742798,71.268089>> - <<-1003.911011, -759.604004, 60.894192>>)
				SpeechData.fWidth = 5.750000
				
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
				SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_TIMED_GREETING)
				SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_USE_AREA)
			BREAK
			CASE 3
				SpeechData.iPedID							= 8
				SpeechData.fGreetSpeechDistance				= 2.3
				SpeechData.fByeSpeechDistance				= 5.0
				SpeechData.fListenDistance					= 7.0
				iSpeechPedID[SpeechData.iPedID]				= iArrayID
				SET_PEDS_BIT(SpeechData.iSpeechBS, BS_PED_SPEECH_DATA_PT_TIMED_GREETING)
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

FUNC BOOL _CAN_FIXER_HQ_PED_PLAY_SPEECH(PED_INDEX PedID, INT iPed, INT iLayout , PED_SPEECH ePedSpeech)
	UNUSED_PARAMETER(ePedSpeech)
	UNUSED_PARAMETER(PedID)
	UNUSED_PARAMETER(iLayout)
	
	// Generic conditions
	IF NOT IS_ENTITY_ALIVE(PedID)
		PRINTLN("[AM_MP_PEDS] _CAN_FIXER_HQ_PED_PLAY_SPEECH - Bail Reason: Ped is not alive")
		RETURN FALSE
	ENDIF
	
	IF NOT g_bInitPedsCreated
		PRINTLN("[AM_MP_PEDS] _CAN_FIXER_HQ_PED_PLAY_SPEECH - Bail Reason: Waiting for all peds to be created first")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_FIXER_HQ_PED_PLAY_SPEECH - Bail Reason: Player is walking in or out of interior")
		RETURN FALSE
	ENDIF
	
	IF IS_SCREEN_FADING_OUT() OR IS_SCREEN_FADED_OUT()
		PRINTLN("[AM_MP_PEDS] _CAN_FIXER_HQ_PED_PLAY_SPEECH - Bail Reason: Screen is fading out")
		RETURN FALSE
	ENDIF
	
	IF IS_BROWSER_OPEN()
		PRINTLN("[AM_MP_PEDS] _CAN_FIXER_HQ_PED_PLAY_SPEECH - Bail Reason: Browser is open")
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE() OR IS_PLAYER_IN_SIMPLE_CUTSCENE(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_FIXER_HQ_PED_PLAY_SPEECH - Bail Reason: Cutscene is active")
		RETURN FALSE
	ENDIF
	
	IF (IS_PLAYER_IN_CORONA() OR IS_TRANSITION_SESSION_LAUNCHING() OR IS_TRANSITION_SESSION_RESTARTING())
		PRINTLN("[AM_MP_PEDS] _CAN_FIXER_HQ_PED_PLAY_SPEECH - Bail Reason: Player in corona")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_INTERACTING_WITH_PLANNING_BOARD()
		PRINTLN("[AM_MP_PEDS] _CAN_FIXER_HQ_PED_PLAY_SPEECH - Bail Reason: Player is interacting with the planning board")
		RETURN FALSE
	ENDIF
	
	// Specific conditions
	SWITCH iPed
		CASE 7 //Lamar.
			IF g_bBlockLamarSpeech = TRUE
				RETURN FALSE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC

FUNC PED_SPEECH _GET_FIXER_HQ_PED_SPEECH_TYPE(INT iPed, PED_ACTIVITIES ePedActivity, INT iSpeech)
	UNUSED_PARAMETER(ePedActivity)
	PED_SPEECH eSpeech = PED_SPH_INVALID	
	SWITCH iPed
		CASE 1			
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
				CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
				CASE 3	eSpeech = PED_SPH_PT_LOITER											BREAK
			ENDSWITCH
		BREAK
		CASE 6			
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
				CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
				CASE 3	eSpeech = PED_SPH_PT_LOITER											BREAK
			ENDSWITCH
		BREAK
		CASE 7			
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
				CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
				CASE 3	eSpeech = PED_SPH_PT_LOITER											BREAK
			ENDSWITCH
		BREAK
		CASE 8			
			SWITCH iSpeech
				CASE 0  eSpeech = PED_SPH_PT_GREETING										BREAK
				CASE 1 	eSpeech = PED_SPH_PT_BYE											BREAK
				CASE 2	eSpeech = PED_SPH_PT_BUMP											BREAK
				CASE 3	eSpeech = PED_SPH_PT_LOITER											BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	RETURN eSpeech
ENDFUNC

PROC _GET_FIXER_HQ_PED_CONVO_DATA(PED_CONVO_DATA &convoData, INT iPed, PED_ACTIVITIES ePedActivity, PED_SPEECH ePedSpeech)
	UNUSED_PARAMETER(ePedActivity)
	RESET_PED_CONVO_DATA(convoData)
	
	SWITCH iPed
		CASE 1
			convoData.sCharacterVoice = "FIX_IMANI"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())
						IF GET_RANDOM_BOOL()
							convoData.sRootName = "GREET_OWNER"
						ELSE
							convoData.sRootName = "HOWSITGOING_OWNER"
						ENDIF
					ELSE
						IF GET_RANDOM_BOOL()
							convoData.sRootName = "GREET_OTHER_PLAYER"
						ELSE
							convoData.sRootName = "HOWSITGOING_OTHER_PLAYER"
						ENDIF
					ENDIF
				BREAK
				CASE PED_SPH_PT_BYE
					IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())
						convoData.sRootName = "BYE_OWNER"
					ELSE
						convoData.sRootName = "BYE_OTHER_PLAYER" 
					ENDIF
				BREAK
				CASE PED_SPH_PT_BUMP
					IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())
						convoData.sRootName = "BUMP_OWNER"
					ELSE
						convoData.sRootName = "BUMP_OTHER_PLAYER" 
					ENDIF
				BREAK
				CASE PED_SPH_PT_LOITER
					SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)
						CASE 0
							IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())
								convoData.sRootName = "IDLE_OWNER"
							ELSE
								convoData.sRootName = "IDLE_OTHER_PLAYER"
							ENDIF
						BREAK
						CASE 1
							convoData.sRootName = "COMMENT_FRANKLIN"
						BREAK
						CASE 2
							//Is Lamar here?
							IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_LAMAR_PRESENT)
								convoData.sRootName = "COMMENT_LAMAR"
							ELSE
								IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())
									convoData.sRootName = "IDLE_OWNER"
								ELSE
									convoData.sRootName = "IDLE_OTHER_PLAYER"
								ENDIF
							ENDIF
						BREAK
						CASE 3
							convoData.sRootName = "COMMENT_CHOP"
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 6
			convoData.sCharacterVoice = "FIX_FRANKLIN"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())
						IF GET_RANDOM_BOOL()
							convoData.sRootName = "GREET_OWNER_OFFICE"
						ELSE
							convoData.sRootName = "HOWSITGOING_OWNER"
						ENDIF
					ELSE
						IF GET_RANDOM_BOOL()
							convoData.sRootName = "GREET_OTHER_PLAYER"
						ELSE
							convoData.sRootName = "HOWSITGOING_OTHER_PLAYER"
						ENDIF
					ENDIF
				BREAK
				CASE PED_SPH_PT_BYE
					IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())
						convoData.sRootName = "BYE_OWNER"
					ELSE
						convoData.sRootName = "BYE_OTHER_PLAYER" 
					ENDIF
					
					
				BREAK
				CASE PED_SPH_PT_BUMP
					convoData.sRootName = "BUMP"
				BREAK
				CASE PED_SPH_PT_LOITER
					
					//First, see if we want to say somethign to do with other things the player has done if the player owns the HQ.
					IF ( (GET_RANDOM_INT_IN_RANGE(0, 100) < 50) AND (IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())) )
						SWITCH GET_RANDOM_INT_IN_RANGE(0, 9)
							CASE 0	
								IF DOES_PLAYER_OWN_A_BUNKER(PLAYER_ID())
									convoData.sRootName = "IDLE_OWNER_BUNKER_OWNED"
									EXIT
								ENDIF
							BREAK
							CASE 1	
								IF DOES_PLAYER_OWN_A_NIGHTCLUB(PLAYER_ID())
									convoData.sRootName = "IDLE_OWNER_NIGHTCLUB_OWNED"
									EXIT
								ENDIF
							BREAK
							CASE 2	
								IF DOES_PLAYER_OWN_A_HANGER(PLAYER_ID())
									convoData.sRootName = "IDLE_OWNER_HANGAR_OWNED"
									EXIT
								ENDIF
							BREAK
							CASE 3	
								IF DOES_PLAYER_OWN_A_AUTO_SHOP(PLAYER_ID())
									convoData.sRootName = "IDLE_OWNER_AUTOSHOP_OWNED"
									EXIT
								ENDIF 
							BREAK
							CASE 4	
								IF DOES_PLAYER_OWN_A_WAREHOUSE(PLAYER_ID())
									convoData.sRootName = "IDLE_OWNER_VEHICLE_WAREHOUSE_OWNED"
									EXIT
								ENDIF 
							BREAK
							CASE 5	
								IF DOES_PLAYER_OWN_A_BUNKER(PLAYER_ID())
									convoData.sRootName = "IDLE_OWNER_ISLAND_HEIST_COMPLETED"
									EXIT
								ENDIF 
							BREAK
							CASE 6	
								IF HAS_PLAYER_COMPLETED_CASINO_HEIST(PLAYER_ID())
									convoData.sRootName = "IDLE_OWNER_CASINO_HEIST_COMPLETED"
									EXIT
								ENDIF 
							BREAK
							CASE 7	
								IF (GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(Get_Ornate_Heist_RCID_Hash(), TRUE) > 0)
									convoData.sRootName = "IDLE_OWNER_SERIES_A_COMPLETED"
									EXIT
								ENDIF 
							BREAK
							CASE 8	
								IF HAS_LOCAL_PLAYER_COMPLETED_FIXER_STORY_AS_LEADER()
									IF GET_RANDOM_BOOL()
										convoData.sRootName = "IDLE_OWNER_STORY_COMPLETE"
									ELSE
										convoData.sRootName = "OFFICE_COLLECT_GIFT"
									ENDIF
									EXIT
								ENDIF 
							BREAK
						ENDSWITCH
					ENDIF
					
					//If not, then choose another line.
					SWITCH GET_RANDOM_INT_IN_RANGE(0, 6)
						CASE 0
							IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())
								convoData.sRootName = "IDLE_OWNER"
							ELSE
								convoData.sRootName = "IDLE_OTHER_PLAYER"
							ENDIF
						BREAK
						CASE 1 convoData.sRootName = "OFFICE_BANTER" BREAK
						CASE 2 convoData.sRootName = "COMMENT_CHOP" BREAK
						CASE 3 convoData.sRootName = "COMMENT_IMANI" BREAK
						CASE 4 convoData.sRootName = "COMMENT_LAMAR" BREAK
						CASE 5 convoData.sRootName = "COMMENT_REQUISITIONS_OFFICER" BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
			
		BREAK
		CASE 7
			convoData.sCharacterVoice = "FIX_LAMAR"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())
						IF GET_RANDOM_BOOL()
							convoData.sRootName = "GREET_OWNER_OFFICE"
						ELSE
							convoData.sRootName = "HOWSITGOING_OWNER"
						ENDIF
					ELSE
						IF GET_RANDOM_BOOL()
							convoData.sRootName = "GREET_OTHER_PLAYER"
						ELSE
							convoData.sRootName = "HOWSITGOING_OTHER_PLAYER"
						ENDIF
					ENDIF
				BREAK
				CASE PED_SPH_PT_BYE
					IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())
						convoData.sRootName = "BYE_OWNER"
					ELSE
						convoData.sRootName = "BYE_OTHER_PLAYER" 
					ENDIF
				BREAK
				CASE PED_SPH_PT_BUMP
					convoData.sRootName = "BUMP"
				BREAK
				CASE PED_SPH_PT_LOITER
					//Small chance we check that the player has done the lowrider missions.
					IF ( (GET_RANDOM_INT_IN_RANGE(0, 100) < 10) )
						IF FM_LOW_FLOW_HAS_BEEN_COMPLETED()
							convoData.sRootName = "IDLE_OTHER_PLAYER_LOWRIDER_COMPLETE"
							EXIT
						ENDIF
					ENDIF
					
					SWITCH GET_RANDOM_INT_IN_RANGE(0, 2)
						CASE 0
							IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())
								convoData.sRootName = "IDLE_OWNER"
							ELSE
								convoData.sRootName = "IDLE_OTHER_PLAYER"
							ENDIF
						BREAK
						CASE 1
							convoData.sRootName = "OFFICE_BANTER"
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE 8
			convoData.sCharacterVoice = "FIX_SECURE1"
			SWITCH ePedSpeech
				CASE PED_SPH_PT_GREETING
					IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())
						IF GET_RANDOM_BOOL()
							convoData.sRootName = "GREET_OWNER"
						ELSE
							convoData.sRootName = "HOWSITGOING_OWNER"
						ENDIF
					ELSE
						IF GET_RANDOM_BOOL()
							convoData.sRootName = "GREET_OTHER_PLAYER"
						ELSE
							convoData.sRootName = "HOWSITGOING_OTHER_PLAYER"
						ENDIF
					ENDIF
				BREAK
				CASE PED_SPH_PT_BYE
					IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())
						convoData.sRootName = "BYE_OWNER"
					ELSE
						convoData.sRootName = "BYE_OTHER_PLAYER" 
					ENDIF
				BREAK
				CASE PED_SPH_PT_BUMP
					IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())
						convoData.sRootName = "BUMP_OWNER"
					ELSE
						convoData.sRootName = "BUMP_OTHER_PLAYER" 
					ENDIF
				BREAK
				CASE PED_SPH_PT_LOITER
					SWITCH GET_RANDOM_INT_IN_RANGE(0, 5)
						CASE 0
							IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())
								convoData.sRootName = "IDLE_OWNER"
							ELSE
								convoData.sRootName = "IDLE_OTHER_PLAYER"
							ENDIF
						BREAK
						CASE 1
							convoData.sRootName = "COMMENT_FRANKLIN"
						BREAK
						CASE 2
							//Is Lamar here?
							IF IS_BIT_SET(g_SimpleInteriorData.iEleventhBS, BS11_SIMPLE_INTERIOR_FIXER_HQ_LAMAR_PRESENT)
								convoData.sRootName = "COMMENT_LAMAR"
							ELSE
								IF IS_PLAYER_IN_FIXER_HQ_THEY_OWN(PLAYER_ID())
									convoData.sRootName = "IDLE_OWNER"
								ELSE
									convoData.sRootName = "IDLE_OTHER_PLAYER"
								ENDIF
							ENDIF
						BREAK
						CASE 3
							convoData.sRootName = "COMMENT_CHOP"
						BREAK
						CASE 4
							convoData.sRootName = "COMMENT_IMANI"
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
ENDPROC

FUNC PED_SPEECH _GET_FIXER_HQ_PED_CONTROLLER_SPEECH(PED_SPEECH &eCurrentSpeech, INT iPed, PED_ACTIVITIES ePedActivity)
	
	INT iSpeech
	INT iAttempts = 0
	INT iMaxSpeech
	INT iRandSpeech
	PED_SPEECH eSpeech
	
	INT iMaxControllerSpeechTypes = 0
	INT iControllerSpeechTypes[PED_SPH_TOTAL]
	
	SWITCH iPed
		CASE -1
		BREAK
		
		DEFAULT
			// Description: Default simply selects a new speech to play that isn't the same as the previous speech.
			PED_CONVO_DATA convoData
			
			// Populate the iControllerSpeechTypes array with all the controller speech type IDs from _GET_GENREIC_PED_SPEECH_TYPE
			REPEAT PED_SPH_TOTAL iSpeech
				eSpeech = _GET_FIXER_HQ_PED_SPEECH_TYPE(iPed, ePedActivity, iSpeech)
				IF (eSpeech > PED_SPH_PT_TOTAL AND eSpeech < PED_SPH_CT_TOTAL)
					RESET_PED_CONVO_DATA(convoData)
					_GET_FIXER_HQ_PED_CONVO_DATA(convoData, iPed, ePedActivity, eSpeech)
					IF IS_CONVO_DATA_VALID(convoData)
						iControllerSpeechTypes[iMaxControllerSpeechTypes] = iSpeech
						iMaxControllerSpeechTypes++
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF (iMaxControllerSpeechTypes > 1)
				iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
				eSpeech = _GET_FIXER_HQ_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
				
				// Ensure speech type is different from previous
				WHILE (eSpeech = eCurrentSpeech AND iAttempts < 10)
					iRandSpeech = GET_RANDOM_INT_IN_RANGE(0, iMaxControllerSpeechTypes)
					eSpeech = _GET_FIXER_HQ_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iRandSpeech])
					iAttempts++
				ENDWHILE
				
				// Randomising failed to find new speech type. Manually set it.
				IF (iAttempts >= 10)
					REPEAT iMaxSpeech iSpeech
						eSpeech = _GET_FIXER_HQ_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[iSpeech])
						IF (eSpeech != eCurrentSpeech)
							BREAKLOOP
						ENDIF
					ENDREPEAT
				ENDIF
				
			ELSE
				eSpeech = _GET_FIXER_HQ_PED_SPEECH_TYPE(iPed, ePedActivity, iControllerSpeechTypes[0])
			ENDIF
			
		BREAK
	ENDSWITCH
	
	eCurrentSpeech = eSpeech
	RETURN eSpeech
ENDFUNC
//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ PROP ANIM DATA ╞═════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC _GET_FIXER_HQ_PED_PROP_ANIM_DATA(PED_ACTIVITIES eActivity, PED_PROP_ANIM_DATA &pedPropAnimData, INT iProp = 0, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, BOOL bHeeledPed = FALSE)
	UNUSED_PARAMETER(eMusicIntensity)
	UNUSED_PARAMETER(ePedMusicIntensity)
	UNUSED_PARAMETER(bDancingTransition)
	UNUSED_PARAMETER(bHeeledPed)
	RESET_PED_PROP_ANIM_DATA(pedPropAnimData)
	
	pedPropAnimData.fBlendInDelta						= SLOW_BLEND_IN
	pedPropAnimData.fBlendOutDelta						= SLOW_BLEND_OUT
	pedPropAnimData.fMoverBlendInDelta					= SLOW_BLEND_IN
	
	SWITCH eActivity
		CASE PED_ACT_FIXER_HQ_SECURITY_DESK
			pedPropAnimData.sPropAnimDict = "anim@amb@office@pa@male@"
			SWITCH iProp
				CASE 0	// P_AMB_CLIPBOARD_01
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "base_chair"						BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "idle_a_chair"						BREAK		
						CASE 2	pedPropAnimData.sPropAnimClip = "idle_b_chair"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "idle_c_chair"						BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "idle_d_chair"						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_FIXER_HQ_RECEPTION
			pedPropAnimData.sPropAnimDict = "anim@amb@office@pa@female@"
			SWITCH iProp
				CASE 0	// P_AMB_CLIPBOARD_01
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "base_chair"						BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "idle_a_chair"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "idle_b_chair"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "idle_c_chair"						BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "idle_d_chair"						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_IMANI
			pedPropAnimData.sPropAnimDict = "anim@scripted@freemode_npc@fix_agy_ig3_imani@"
			SWITCH iProp
				CASE 0	//  Prop_Mug_01
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "base_mug"							BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "idle_01_mug"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "idle_02_mug"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "idle_03_mug"						BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "idle_04_mug"						BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "idle_05_mug"						BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "idle_06_mug"						BREAK
						CASE 7	pedPropAnimData.sPropAnimClip = "idle_07_mug"						BREAK
					ENDSWITCH
					IF g_bShouldImaniHeadtrack
						pedPropAnimData.sPropAnimDict 		= "anim@scripted@freemode_npc@fix_agy_ig3_imani@"
						pedPropAnimData.sPropAnimClip 		= "look_at_player_r_mug"
					ENDIF
				BREAK
				CASE 1	//  HASH("vw_Prop_VW_OffChair_03")
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "base_chair"						BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "idle_01_chair"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "idle_02_chair"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "idle_03_chair"						BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "idle_04_chair"						BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "idle_05_chair"						BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "idle_06_chair"						BREAK
						CASE 7	pedPropAnimData.sPropAnimClip = "idle_07_chair"						BREAK
					ENDSWITCH
					IF g_bShouldImaniHeadtrack
						pedPropAnimData.sPropAnimDict 		= "anim@scripted@freemode_npc@fix_agy_ig3_imani@"
						pedPropAnimData.sPropAnimClip 		= "look_at_player_r_chair"
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_FIXER_FRANKLIN_0
			pedPropAnimData.sPropAnimDict = "anim@scripted@freemode_npc@fix_agy_ig1_franklin@"		
			SWITCH iProp
				CASE 0	//  chair
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "chair_base_chair"							BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "chair_idle_01_chair"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "chair_idle_02_chair"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "chair_idle_03_chair"						BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "chair_idle_04_chair"						BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "chair_idle_05_chair"						BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "chair_idle_06_chair"						BREAK
					ENDSWITCH
				BREAK
				CASE 1	//  phone
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "chair_base_phone"							BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "chair_idle_01_phone"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "chair_idle_02_phone"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "chair_idle_03_phone"						BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "chair_idle_04_phone"						BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "chair_idle_05_phone"						BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "chair_idle_06_phone"						BREAK
					ENDSWITCH
				BREAK					
			ENDSWITCH
		BREAK
		CASE PED_ACT_FIXER_FRANKLIN_1
			pedPropAnimData.sPropAnimDict = "anim@scripted@freemode_npc@fix_agy_ig1_franklin@"		
			SWITCH iProp
				CASE 0	//  ashtray
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "couch_base_ashtray"						BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "couch_idle_01_ashtray"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "couch_idle_02_ashtray"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "couch_idle_03_ashtray"						BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "couch_idle_04_ashtray"						BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "couch_idle_05_ashtray"						BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "couch_idle_06_ashtray"						BREAK
					ENDSWITCH
				BREAK
				CASE 1	//  joint
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "couch_base_joint"							BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "couch_idle_01_joint"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "couch_idle_02_joint"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "couch_idle_03_joint"						BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "couch_idle_04_joint"						BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "couch_idle_05_joint"						BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "couch_idle_06_joint"						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_FIXER_FRANKLIN_2
			pedPropAnimData.sPropAnimDict = "anim@scripted@freemode_npc@fix_agy_ig1_franklin@"	
			SWITCH iProp
				CASE 0	//  chair
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "typing_base_chair"						BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "typing_idle_01_chair"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "typing_idle_02_chair"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "typing_idle_03_chair"						BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "typing_idle_04_chair"						BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "typing_idle_05_chair"						BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "typing_idle_06_chair"						BREAK
					ENDSWITCH
				BREAK
				CASE 1	//  keyboard
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "typing_base_keyboard"							BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "typing_idle_01_keyboard"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "typing_idle_02_keyboard"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "typing_idle_03_keyboard"						BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "typing_idle_04_keyboard"						BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "typing_idle_05_keyboard"						BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "typing_idle_06_keyboard"						BREAK
					ENDSWITCH
				BREAK
				CASE 2	//  mouse
					SWITCH iClip
						CASE 0	pedPropAnimData.sPropAnimClip = "typing_base_mouse"							BREAK
						CASE 1	pedPropAnimData.sPropAnimClip = "typing_idle_01_mouse"						BREAK
						CASE 2	pedPropAnimData.sPropAnimClip = "typing_idle_02_mouse"						BREAK
						CASE 3	pedPropAnimData.sPropAnimClip = "typing_idle_03_mouse"						BREAK
						CASE 4	pedPropAnimData.sPropAnimClip = "typing_idle_04_mouse"						BREAK
						CASE 5	pedPropAnimData.sPropAnimClip = "typing_idle_05_mouse"						BREAK
						CASE 6	pedPropAnimData.sPropAnimClip = "typing_idle_06_mouse"						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_FIXER_LAMAR
			pedPropAnimData.sPropAnimDict = "anim@scripted@freemode_npc@fix_agy_ig4_lamar@"
			SWITCH GET_LAMARS_VARIATION(g_iAnimLayout)
				CASE NET_PEDS_FIXER_LAMAR_VARIATION_0
					SWITCH iProp
						CASE 0	
							SWITCH iClip //joint
								CASE 0	pedPropAnimData.sPropAnimClip = "lean_wall_base_joint"					BREAK
								CASE 1	pedPropAnimData.sPropAnimClip = "lean_wall_idle_01_joint"				BREAK
								CASE 2	pedPropAnimData.sPropAnimClip = "lean_wall_idle_02_joint"				BREAK
								CASE 3	pedPropAnimData.sPropAnimClip = "lean_wall_idle_03_joint"				BREAK
								CASE 4	pedPropAnimData.sPropAnimClip = "lean_wall_idle_04_joint"				BREAK
								CASE 5	pedPropAnimData.sPropAnimClip = "lean_wall_idle_05_joint"				BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
				CASE NET_PEDS_FIXER_LAMAR_VARIATION_1
					SWITCH iProp
						CASE 0
							SWITCH iClip //joint
								CASE 0	pedPropAnimData.sPropAnimClip = "sit_base_joint"					BREAK
								CASE 1	pedPropAnimData.sPropAnimClip = "sit_idle_01_joint"					BREAK
								CASE 2	pedPropAnimData.sPropAnimClip = "sit_idle_02_joint"					BREAK
								CASE 3	pedPropAnimData.sPropAnimClip = "sit_idle_03_joint"					BREAK
								CASE 4	pedPropAnimData.sPropAnimClip = "sit_idle_04_joint"					BREAK
								CASE 5	pedPropAnimData.sPropAnimClip = "sit_idle_05_joint"					BREAK
								CASE 6	pedPropAnimData.sPropAnimClip = "sit_idle_06_joint"					BREAK
							ENDSWITCH
						BREAK
						CASE 1
							SWITCH iClip //phone
								CASE 0	pedPropAnimData.sPropAnimClip = "sit_base_phone"					BREAK
								CASE 1	pedPropAnimData.sPropAnimClip = "sit_idle_01_phone"					BREAK
								CASE 2	pedPropAnimData.sPropAnimClip = "sit_idle_02_phone"					BREAK
								CASE 3	pedPropAnimData.sPropAnimClip = "sit_idle_03_phone"					BREAK
								CASE 4	pedPropAnimData.sPropAnimClip = "sit_idle_04_phone"					BREAK
								CASE 5	pedPropAnimData.sPropAnimClip = "sit_idle_05_phone"					BREAK
								CASE 6	pedPropAnimData.sPropAnimClip = "sit_idle_06_phone"					BREAK
							ENDSWITCH
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
	ENDSWITCH
	
ENDPROC
//╒═══════════════════════════════╕
//╞═════╡ 	RECEPTIONIST   ╞══════╡
//╘═══════════════════════════════╛

PROC GET_FIXER_HQ_DESK_ANIM(PED_ANIM_DATA &pedAnimData, INT iClip, BOOL bIsFemale = TRUE)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	IF bIsFemale
		SWITCH iClip
			CASE 0
				pedAnimData.sAnimDict 		= "anim@amb@office@pa@female@"
				pedAnimData.sAnimClip 		= "pa_base"
			BREAK
			CASE 1
				pedAnimData.sAnimDict 		= "anim@amb@office@pa@female@"
				pedAnimData.sAnimClip 		= "pa_idle_a"
			BREAK
			CASE 2
				pedAnimData.sAnimDict 		= "anim@amb@office@pa@female@"
				pedAnimData.sAnimClip 		= "pa_idle_b"
			BREAK
			CASE 3
				pedAnimData.sAnimDict 		= "anim@amb@office@pa@female@"
				pedAnimData.sAnimClip 		= "pa_idle_c"
			BREAK
			CASE 4
				pedAnimData.sAnimDict 		= "anim@amb@office@pa@female@"
				pedAnimData.sAnimClip 		= "pa_idle_d"
			BREAK
		ENDSWITCH
	ELSE
		SWITCH iClip
			CASE 0
				pedAnimData.sAnimDict 		= "anim@amb@office@pa@male@"
				pedAnimData.sAnimClip 		= "pa_base"
			BREAK
			CASE 1
				pedAnimData.sAnimDict 		= "anim@amb@office@pa@male@"
				pedAnimData.sAnimClip 		= "pa_idle_a"
			BREAK
			CASE 2
				pedAnimData.sAnimDict 		= "anim@amb@office@pa@male@"
				pedAnimData.sAnimClip 		= "pa_idle_b"
			BREAK
			CASE 3
				pedAnimData.sAnimDict 		= "anim@amb@office@pa@male@"
				pedAnimData.sAnimClip 		= "pa_idle_c"
			BREAK
			CASE 4
				pedAnimData.sAnimDict 		= "anim@amb@office@pa@male@"
				pedAnimData.sAnimClip 		= "pa_idle_d"
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

//╒═══════════════════════════════╕
//╞════════╡ ANIM LOOKUP ╞════════╡
//╘═══════════════════════════════╛

PROC GET_FIXER_HQ_IMANI_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	
	SWITCH iClip
		CASE 0 //Type
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_agy_ig3_imani@"
			pedAnimData.sAnimClip 		= "BASE_IMANI"
		BREAK
		CASE 1 //Drink coffee
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_agy_ig3_imani@"
			pedAnimData.sAnimClip 		= "IDLE_01_IMANI"
		BREAK
		CASE 2 //Drink coffee longer
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_agy_ig3_imani@"
			pedAnimData.sAnimClip 		= "IDLE_02_IMANI"
		BREAK
		CASE 3 //Look on left screen
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_agy_ig3_imani@"
			pedAnimData.sAnimClip 		= "IDLE_03_IMANI"
		BREAK
		CASE 4 //Look on right screen
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_agy_ig3_imani@"
			pedAnimData.sAnimClip 		= "IDLE_04_IMANI"
		BREAK
		CASE 5 //Chair sit back
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_agy_ig3_imani@"
			pedAnimData.sAnimClip 		= "IDLE_05_IMANI"
		BREAK
		CASE 6 //Angry at screen
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_agy_ig3_imani@"
			pedAnimData.sAnimClip 		= "IDLE_06_IMANI"
		BREAK
		CASE 7 //Stretch hands
			pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_agy_ig3_imani@"
			pedAnimData.sAnimClip 		= "IDLE_07_IMANI"
		BREAK
	ENDSWITCH
	
	IF g_bShouldImaniHeadtrack
		pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_agy_ig3_imani@"
		pedAnimData.sAnimClip 		= "look_at_player_r_imani"
	ENDIF
ENDPROC

PROC GET_FIXER_HQ_LAMAR_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	UNUSED_PARAMETER(iClip)
	pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_agy_ig4_lamar@"
	
	g_bBlockLamarSpeech = FALSE //Set to be false by default unless we want specific anims not to trigger speech.
	
	SWITCH (GET_LAMARS_VARIATION(g_iAnimLayout)) 
		CASE NET_PEDS_FIXER_LAMAR_VARIATION_0
			SWITCH iClip
				CASE 0
					pedAnimData.sAnimClip 		= "lean_wall_base_lamar"
				BREAK
				CASE 1
					pedAnimData.sAnimClip 		= "lean_wall_idle_01_lamar"
				BREAK
				CASE 2
					pedAnimData.sAnimClip 		= "lean_wall_idle_02_lamar"
				BREAK
				CASE 3
					pedAnimData.sAnimClip 		= "lean_wall_idle_03_lamar"
				BREAK
				CASE 4
					pedAnimData.sAnimClip 		= "lean_wall_idle_04_lamar"
				BREAK
				CASE 5
					pedAnimData.sAnimClip 		= "lean_wall_idle_05_lamar"
					g_bBlockLamarSpeech = TRUE
				BREAK
			ENDSWITCH
			
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_sec_weed_smoke_exhale_start"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_mouth", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)	
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "vfx_sec_weed_smoke_start", "vfx_sec_weed_smoke_stop", "scr_sec", "scr_sec_weed_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, DEFAULT, TRUE)				
			pedAnimData.bVFXAnimClip	= TRUE
		BREAK
		CASE NET_PEDS_FIXER_LAMAR_VARIATION_1
			SWITCH iClip
				CASE 0
					pedAnimData.sAnimClip 		= "sit_base_lamar"
				BREAK
				CASE 1
					pedAnimData.sAnimClip 		= "sit_idle_01_lamar"
				BREAK
				CASE 2
					pedAnimData.sAnimClip 		= "sit_idle_02_lamar"
					g_bBlockLamarSpeech = TRUE
				BREAK
				CASE 3
					pedAnimData.sAnimClip 		= "sit_idle_03_lamar"
				BREAK
				CASE 4
					pedAnimData.sAnimClip 		= "sit_idle_04_lamar"
				BREAK
				CASE 5
					pedAnimData.sAnimClip 		= "sit_idle_05_lamar"
				BREAK
				CASE 6
					pedAnimData.sAnimClip 		= "sit_idle_06_lamar"
				BREAK
			ENDSWITCH
			
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_sec_weed_smoke_exhale_start"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_mouth", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)	
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "vfx_sec_weed_smoke_start", "vfx_sec_weed_smoke_stop", "scr_sec", "scr_sec_weed_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 0, DEFAULT, TRUE)				
			pedAnimData.bVFXAnimClip	= TRUE
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_FIXER_HQ_FRANKLIN_CLIP(PED_ANIM_DATA &pedAnimData, PED_ACTIVITIES eActivity,  INT iClip)
	
	pedAnimData.sAnimDict 		= "anim@scripted@freemode_npc@fix_agy_ig1_franklin@"
	SWITCH eActivity
		CASE PED_ACT_FIXER_FRANKLIN_0
			SWITCH iClip
				CASE 0
					pedAnimData.sAnimClip 		= "chair_base_franklin"
				BREAK
				CASE 1
					pedAnimData.sAnimClip 		= "chair_idle_01_franklin"
				BREAK
				CASE 2
					pedAnimData.sAnimClip 		= "chair_idle_02_franklin"
				BREAK
				CASE 3
					pedAnimData.sAnimClip 		= "chair_idle_03_franklin"
				BREAK
				CASE 4
					pedAnimData.sAnimClip 		= "chair_idle_04_franklin"
				BREAK
				CASE 5
					pedAnimData.sAnimClip 		= "chair_idle_05_franklin"
				BREAK
				CASE 6
					pedAnimData.sAnimClip 		= "chair_idle_06_franklin"
				BREAK
			ENDSWITCH
		BREAK
		CASE PED_ACT_FIXER_FRANKLIN_1
			SWITCH iClip
				CASE 0
					pedAnimData.sAnimClip 		= "couch_base_franklin"
				BREAK
				CASE 1
					pedAnimData.sAnimClip 		= "couch_idle_01_franklin"
				BREAK
				CASE 2
					pedAnimData.sAnimClip 		= "couch_idle_02_franklin"
				BREAK
				CASE 3
					pedAnimData.sAnimClip 		= "couch_idle_03_franklin"
				BREAK
				CASE 4
					pedAnimData.sAnimClip 		= "couch_idle_04_franklin"
				BREAK
				CASE 5
					pedAnimData.sAnimClip 		= "couch_idle_05_franklin"
				BREAK
				CASE 6
					pedAnimData.sAnimClip 		= "couch_idle_06_franklin"
				BREAK
			ENDSWITCH
			
			pedAnimData.VFXOneShotData.sAnimEvent[0] = "vfx_sec_weed_smoke_exhale_start"
			SET_ONE_SHOT_VFX_DATA(pedAnimData.VFXOneShotData, pedAnimData.VFXOneShotData.sAnimEvent, "scr_tn_meet", "scr_tn_meet_cig_exhale_mouth", "", "", <<-0.02, 0.13, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, DEFAULT, BONETAG_HEAD, DEFAULT, TRUE)	
			SET_LOOPING_VFX_DATA(pedAnimData.VFXLoopingData, "vfx_sec_weed_smoke_start", "vfx_sec_weed_smoke_stop", "scr_sec", "scr_sec_weed_smoke", "", "", <<-0.07, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.0, 1, DEFAULT, TRUE)				
			pedAnimData.bVFXAnimClip	= TRUE
		BREAK
		CASE PED_ACT_FIXER_FRANKLIN_2
			SWITCH iClip
				CASE 0
					pedAnimData.sAnimClip 		= "typing_base_franklin"
				BREAK
				CASE 1
					pedAnimData.sAnimClip 		= "typing_idle_01_franklin"
				BREAK
				CASE 2
					pedAnimData.sAnimClip 		= "typing_idle_02_franklin"
				BREAK
				CASE 3
					pedAnimData.sAnimClip 		= "typing_idle_03_franklin"
				BREAK
				CASE 4
					pedAnimData.sAnimClip 		= "typing_idle_04_franklin"
				BREAK
				CASE 5
					pedAnimData.sAnimClip 		= "typing_idle_05_franklin"
				BREAK
				CASE 6
					pedAnimData.sAnimClip 		= "typing_idle_06_franklin"
				BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
ENDPROC
			
//PROC GET_FIXER_HQ_DESK_WORKER_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
//	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
//	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
//	SWITCH iClip
//		CASE 0
//			pedAnimData.sAnimDict 		= "amb@prop_human_seat_computer@male@base"
//			pedAnimData.sAnimClip 		= "BASE"
//		BREAK
//		CASE 1
//			pedAnimData.sAnimDict 		= "amb@prop_human_seat_computer@male@idle_a"
//			pedAnimData.sAnimClip 		= "IDLE_A"
//		BREAK
//		CASE 2
//			pedAnimData.sAnimDict 		= "amb@prop_human_seat_computer@male@idle_a"
//			pedAnimData.sAnimClip 		= "IDLE_B"
//		BREAK
//		CASE 3
//			pedAnimData.sAnimDict 		= "amb@prop_human_seat_computer@male@idle_a"
//			pedAnimData.sAnimClip 		= "IDLE_C"
//		BREAK
//		CASE 4
//			pedAnimData.sAnimDict 		= "amb@prop_human_seat_computer@male@idle_b"
//			pedAnimData.sAnimClip 		= "IDLE_D"
//		BREAK
//		CASE 5
//			pedAnimData.sAnimDict 		= "amb@prop_human_seat_computer@male@idle_b"
//			pedAnimData.sAnimClip 		= "IDLE_E"
//		BREAK
//	ENDSWITCH
//ENDPROC

PROC GET_FIXER_HQ_TALK_ON_PHONE_CLIP(PED_ANIM_DATA &pedAnimData, INT iClip)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	SWITCH iClip
		CASE 0
			pedAnimData.sAnimDict 		= "amb@world_human_leaning@male@wall@back@mobile@base"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
	ENDSWITCH
ENDPROC

PROC GET_FIXER_HQ_TALK_ON_PHONE_FEMALE_CLIP(PED_ANIM_DATA &pedAnimData)
	pedAnimData.fBlendInDelta 			= SLOW_BLEND_IN
	pedAnimData.fBlendOutDelta 			= SLOW_BLEND_OUT
	pedAnimData.sAnimDict 		= "amb@world_human_leaning@female@wall@back@mobile@base"
	pedAnimData.sAnimClip 		= "BASE"
ENDPROC


PROC _GET_FIXER_HQ_PED_ANIM_DATA(PED_ACTIVITIES eActivity, PED_ANIM_DATA &pedAnimData, INT iClip = 0, CLUB_MUSIC_INTENSITY eMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, CLUB_MUSIC_INTENSITY ePedMusicIntensity = CLUB_MUSIC_INTENSITY_NULL, BOOL bDancingTransition = FALSE, INT iPedID = 0, PED_TRANSITION_STATES eTransitionState = PED_TRANS_STATE_ACTIVITY_ONE, BOOL bPedTransition = FALSE)
	UNUSED_PARAMETER(iPedID)
	UNUSED_PARAMETER(eMusicIntensity)
	UNUSED_PARAMETER(ePedMusicIntensity)
	UNUSED_PARAMETER(bDancingTransition)
	UNUSED_PARAMETER(eTransitionState)
	UNUSED_PARAMETER(bPedTransition)
	RESET_PED_ANIM_DATA(pedAnimData)
	
	UNUSED_PARAMETER(iClip)
	
	
	SWITCH eActivity		
		CASE PED_ACT_FIXER_HQ_RECEPTION
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_FIXER_HQ_DESK_ANIM(pedAnimData, iClip)
		BREAK		
		CASE PED_ACT_FIXER_HQ_SECURITY_DESK
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_FIXER_HQ_DESK_ANIM(pedAnimData, iClip, FALSE)
		BREAK		
		CASE PED_ACT_FIXER_TEXTING
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimDict 		= "amb@world_human_leaning@male@wall@back@texting@base"
			pedAnimData.sAnimClip 		= "BASE"
		BREAK
		CASE PED_ACT_FIXER_STAND_ON_PHONE
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimDict 		= "amb@world_human_stand_mobile@male@standing@call@idle_a"
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE PED_ACT_FIXER_LEAN_ON_RAIL
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimDict 		= "anim@amb@yacht@rail@standing@male@variant_01@"
			pedAnimData.sAnimClip 		= "base"
		BREAK
		CASE PED_ACT_FIXER_DRINK
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimDict 		= "amb@world_human_drinking@coffee@male@idle_a"
			pedAnimData.sAnimClip 		= "idle_a"
		BREAK
		CASE PED_ACT_IMANI
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_EXTRACT_INITIAL_OFFSET | AF_HOLD_LAST_FRAME
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_FIXER_HQ_IMANI_CLIP(pedAnimData, iClip)
			
		BREAK
		CASE PED_ACT_FIXER_DESK_WORKER_FEMALE
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_LOOPING
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimDict 		= "anim@amb@prop_human_seat_computer@female@base"
			pedAnimData.sAnimClip 		= "BASE"
			
			//GET_FIXER_HQ_DESK_WORKER_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_FIXER_DESK_WORKER_MALE
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			
			pedAnimData.animFlags 		= AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION | AF_LOOPING
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			pedAnimData.sAnimDict 		= "anim@amb@prop_human_seat_computer@male@base"
			pedAnimData.sAnimClip 		= "BASE"
			
			//GET_FIXER_HQ_DESK_WORKER_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_FIXER_TALK_ON_PHONE
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_FIXER_HQ_TALK_ON_PHONE_CLIP(pedAnimData, iClip)
		BREAK
		CASE PED_ACT_FIXER_TALK_ON_PHONE_FEMALE
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= AF_LOOPING
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_ANIM_TASK)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_FIXER_HQ_TALK_ON_PHONE_FEMALE_CLIP(pedAnimData)
		BREAK
		CASE PED_ACT_FIXER_FRANKLIN_0
		CASE PED_ACT_FIXER_FRANKLIN_1
		CASE PED_ACT_FIXER_FRANKLIN_2
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_FIXER_HQ_FRANKLIN_CLIP(pedAnimData, eActivity, iClip)
		BREAK
		CASE PED_ACT_FIXER_LAMAR
			pedAnimData.syncSceneFlags 	= (SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS)
			pedAnimData.animFlags 		= (AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION)
			pedAnimData.ikFlags 		= AIK_DISABLE_LEG_IK
			
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_TYPE_SYNCED_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_SYNC_SCENE)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_HOLD_LAST_FRAME)
			SET_PEDS_BIT(pedAnimData.iBS, BS_PED_ANIM_DATA_RAND_START_PHASE)
			
			GET_FIXER_HQ_LAMAR_CLIP(pedAnimData, iClip)
		BREAK
	ENDSWITCH
ENDPROC

FUNC INT _GET_FIXER_HQ_ACTIVE_PED_LEVEL_THRESHOLD(INT iLevel)
	INT iThreshold = -1
	SWITCH iLevel
		// Peds have level 0, 1 & 2. 
		// Level 2 peds are culled if the player count > 10
		// Level 1 peds are culled if the player count > 20
		// Level 0 peds are base and always remain
		CASE 0	iThreshold = 20	BREAK
		CASE 1	iThreshold = 10	BREAK
		CASE 2	iThreshold = 0	BREAK
	ENDSWITCH
	RETURN iThreshold
ENDFUNC

PROC _SET_FIXER_HQ_PED_HEAD_TRACKING_DATA_LAYOUT(HEAD_TRACKING_DATA &HeadtrackingData, INT iArrayID, INT &iHeadTrackingPedID[], BOOL bNetworkPed = FALSE)
	
	IF (bNetworkPed)
//		SWITCH iArrayID
//		ENDSWITCH
	ELSE
		SWITCH iArrayID
			CASE 0
				HeadtrackingData.iPedID							= 0
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 7.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
			CASE 1
				HeadtrackingData.iPedID							= 1
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 7.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
			CASE 2
				HeadtrackingData.iPedID							= 6
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 7.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
			CASE 3
				HeadtrackingData.iPedID							= 7
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 7.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
			CASE 4
				HeadtrackingData.iPedID							= 8
				HeadtrackingData.iHeadTrackingTime				= -1
				HeadtrackingData.fHeadTrackingRange				= 7.0000
				HeadtrackingData.vHeadTrackingCoords			= <<0.0000, 0.0000, 0.0000>>
				HeadtrackingData.eHeadTrackingLookFlag 			= SLF_DEFAULT
				HeadtrackingData.eHeadTrackingLookPriority 		= SLF_LOOKAT_HIGH
				iHeadTrackingPedID[HeadtrackingData.iPedID]		= iArrayID
			BREAK
			
		ENDSWITCH
	ENDIF
	
ENDPROC

FUNC BOOL _CAN_FIXER_HQ_PED_HEAD_TRACK(PED_INDEX PedID, INT iPed, INT iLayout)
	UNUSED_PARAMETER(PedID)
	UNUSED_PARAMETER(iLayout)
	
	// Generic conditions
	IF NOT IS_ENTITY_ALIVE(PedID)
		PRINTLN("[AM_MP_PEDS] _CAN_FIXER_HQ_PED_HEAD_TRACK - Bail Reason: Ped is not alive")
		RETURN FALSE
	ENDIF
	
	IF NOT g_bInitPedsCreated
		PRINTLN("[AM_MP_PEDS] _CAN_FIXER_HQ_PED_HEAD_TRACK - Bail Reason: Waiting for all peds to be created first")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_FIXER_HQ_PED_HEAD_TRACK - Bail Reason: Player is walking in or out of interior")
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_IN_MP_CUTSCENE() OR IS_PLAYER_IN_SIMPLE_CUTSCENE(PLAYER_ID())
		PRINTLN("[AM_MP_PEDS] _CAN_FIXER_HQ_PED_HEAD_TRACK - Bail Reason: Cutscene is active")
		RETURN FALSE
	ENDIF
	
	IF (IS_PLAYER_IN_CORONA() OR IS_TRANSITION_SESSION_LAUNCHING() OR IS_TRANSITION_SESSION_RESTARTING())
		PRINTLN("[AM_MP_PEDS] _CAN_FIXER_HQ_PED_HEAD_TRACK - Bail Reason: Player in corona")
		RETURN FALSE
	ENDIF
	
	IF IS_LOCAL_PLAYER_INTERACTING_WITH_PLANNING_BOARD()
		PRINTLN("[AM_MP_PEDS] _CAN_FIXER_HQ_PED_HEAD_TRACK - Bail Reason: Player is interacting with the planning board")
		RETURN FALSE
	ENDIF
	
	// Specific conditions
	SWITCH iPed
		CASE 1 //Imani.		Only head track when player is on the right side of the desk
			RETURN g_bShouldImaniHeadtrack			
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC _SET_FIXER_HQ_PED_HEAD_TRACKING_DATA(HEAD_TRACKING_DATA &HeadtrackingData, INT iLayout, INT iArrayID, INT &iHeadTrackingPedID[], BOOL bNetworkPed = FALSE)
	UNUSED_PARAMETER(iLayout)
	
	_SET_FIXER_HQ_PED_HEAD_TRACKING_DATA_LAYOUT(HeadtrackingData, iArrayID, iHeadTrackingPedID, bNetworkPed)
	
ENDPROC

//╒═════════════════════════════════════════════════════════════════════════════╕
//╞══════════════════════════════╡ LOOK-UP TABLE ╞══════════════════════════════╡
//╘═════════════════════════════════════════════════════════════════════════════╛

PROC BUILD_PED_FIXER_HQ_LOOK_UP_TABLE(PED_INTERFACE &interface, PED_INTERFACE_PROCEDURES eProc)
	
	SWITCH eProc
		// Script Launching
		CASE E_SHOULD_PED_SCRIPT_LAUNCH
			interface.returnShouldPedScriptLaunch = &_SHOULD_FIXER_HQ_PED_SCRIPT_LAUNCH
		BREAK
		CASE E_IS_PARENT_A_SIMPLE_INTERIOR
			interface.returnIsParentASimpleInterior = &_IS_FIXER_HQ_PARENT_A_SIMPLE_INTERIOR
		BREAK
		
		// Ped Data
		CASE E_GET_LOCAL_PED_TOTAL
			interface.returnGetLocalPedTotal = &_GET_FIXER_HQ_LOCAL_PED_TOTAL
		BREAK
		CASE E_GET_NETWORK_PED_TOTAL
			interface.returnGetNetworkPedTotal = &_GET_FIXER_HQ_NETWORK_PED_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT_TOTAL
			interface.returnGetServerPedLayoutTotal= &_GET_FIXER_HQ_SERVER_PED_LAYOUT_TOTAL
		BREAK
		CASE E_GET_SERVER_PED_LAYOUT
			interface.returnGetServerPedLayout = &_GET_FIXER_HQ_SERVER_PED_LAYOUT
		BREAK
		CASE E_GET_SERVER_PED_LEVEL
			interface.returnGetServerPedLevel = &_GET_FIXER_HQ_SERVER_PED_LEVEL
		BREAK
		CASE E_SET_PED_DATA
			interface.setPedData = &_SET_FIXER_HQ_PED_DATA
		BREAK
		CASE E_SET_PED_SERVER_DATA
			interface.setPedServerData = &_SET_FIXER_HQ_PED_SERVER_DATA
		BREAK
		CASE E_GET_PED_ANIM_DATA
			interface.getPedAnimData = &_GET_FIXER_HQ_PED_ANIM_DATA
		BREAK
		CASE E_IS_PLAYER_IN_PARENT_PROPERTY
			interface.returnIsPlayerInParentProperty = &_IS_PLAYER_IN_FIXER_HQ_PARENT_PROPERTY
		BREAK
		CASE E_GET_PED_LOCAL_COORDS_BASE_POSITION
			interface.returnGetPedLocalCoordsBasePosition = &_GET_FIXER_HQ_PED_LOCAL_COORDS_BASE_POSITION
		BREAK
		CASE E_GET_PED_LOCAL_HEADING_BASE_HEADING
			interface.returnGetPedLocalHeadingBaseHeading = &_GET_FIXER_HQ_PED_LOCAL_HEADING_BASE_HEADING
		BREAK
		CASE E_GET_ACTIVE_PED_LEVEL_THRESHOLD
			interface.getActivePedLevelThreshold = &_GET_FIXER_HQ_ACTIVE_PED_LEVEL_THRESHOLD
		BREAK
		
		// Ped Creation
		CASE E_SET_LOCAL_PED_PROPERTIES
			interface.setLocalPedProperties = &_SET_FIXER_HQ_LOCAL_PED_PROPERTIES
		BREAK
		CASE E_SET_NETWORK_PED_PROPERTIES
			interface.setNetworkPedProperties = &_SET_FIXER_HQ_NETWORK_PED_PROPERTIES
		BREAK
		CASE E_SET_PED_PROP_INDEXES
			interface.setPedPropIndexes = &_SET_FIXER_HQ_PED_PROP_INDEXES
		BREAK
		CASE E_HAS_PED_BEEN_CREATED
			interface.returnHasPedBeenCreated = &_HAS_FIXER_HQ_PED_BEEN_CREATED
		BREAK
		
		//Head tracking
		CASE E_SET_PED_HEAD_TRACKING_DATA
			interface.setPedHeadTrackingData = &_SET_FIXER_HQ_PED_HEAD_TRACKING_DATA
		BREAK
		CASE E_CAN_PED_HEAD_TRACK
			interface.returnCanPedHeadTrack = &_CAN_FIXER_HQ_PED_HEAD_TRACK
		BREAK
		
		// Ped Speech
		CASE E_SET_PED_SPEECH_DATA
			interface.setPedSpeechData = &_SET_FIXER_HQ_PED_SPEECH_DATA
		BREAK
		CASE E_CAN_PED_PLAY_SPEECH
			interface.returnCanPedPlaySpeech = &_CAN_FIXER_HQ_PED_PLAY_SPEECH
		BREAK
		CASE E_GET_PED_SPEECH_TYPE
			interface.returnGetPedSpeechType = &_GET_FIXER_HQ_PED_SPEECH_TYPE
		BREAK
		CASE E_GET_PED_CONTROLLER_SPEECH
			interface.returnGetPedControllerSpeech = &_GET_FIXER_HQ_PED_CONTROLLER_SPEECH
		BREAK
		CASE E_GET_PED_CONVO_DATA
			interface.getPedConvoData = &_GET_FIXER_HQ_PED_CONVO_DATA
		BREAK
		
		CASE E_GET_PED_PROP_ANIM_DATA
			interface.getPedPropAnimData = &_GET_FIXER_HQ_PED_PROP_ANIM_DATA
		BREAK
				
	ENDSWITCH
	
ENDPROC


#ENDIF	// FEATURE_FIXER
