/* ===================================================================================== *\
		MISSION NAME	:	net_heists_planning.sch
		AUTHOR			:	Alastair Costley - 2013/12/03
		DESCRIPTION		:	Contains the code for managing the heist finale planning board.
\* ===================================================================================== */


// INCLUDE FILES.
USING "globals.sch"
USING "commands_network.sch"

USING "net_include.sch"
USING "context_control_public.sch"
USING "net_ambience.sch"
USING "net_heists_hardcoded.sch"
USING "MP_Scaleform_Functions.sch"
USING "fmmc_corona_controller.sch"
USING "net_heists_event_control.sch"
USING "net_heists_scaleform_header.sch"
USING "net_fps_cam.sch"
USING "net_heists_common.sch"
USING "ped_component_public.sch"
USING "fmmc_vars.sch"
USING "menu_cursor_3d_public.sch"



CONST_INT ci_HEIST_LEADER							0
CONST_INT ci_INVALID_PLAYER							-1

CONST_INT ci_HEIST_TARGET_VEHICLE 					1
CONST_INT ci_HEIST_TARGET_PED 						2
CONST_INT ci_HEIST_TARGET_OBJECT 					3
CONST_INT ci_HEIST_TARGET_LOCATION 					4

CONST_INT ci_MAX_MAP_POSTITS_PER_RULE				5		//	Can be amended to allow for more map postit's per rule. Priority order: Peds, Locs, Vehs, Objs - RowanJ.
CONST_INT ci_MIN_WORLD_SPACE_DIST_BETWEEN_POSITS	1000	//	Meters (World space) between locations - If under this dist postit not shown on map for same rule. - RowanJ.

// PC Mouse menu support
CONST_FLOAT cf_HEIST_MOUSE_PLAYER_ORIGIN_X	0.326
CONST_FLOAT cf_HEIST_MOUSE_PLAYER_ORIGIN_Y	0.108
CONST_FLOAT cf_HEIST_MOUSE_PLAYER_HEIGHT	0.070
CONST_FLOAT cf_HEIST_MOUSE_PLAYER_WIDTH		0.658
CONST_FLOAT cf_HEIST_MOUSE_PLAYER_GAP		0.012
TWEAK_FLOAT cf_HEIST_MOUSE_TODO_ORIGIN_X	0.005
TWEAK_FLOAT cf_HEIST_MOUSE_TODO_ORIGIN_Y	0.43
TWEAK_FLOAT cf_HEIST_MOUSE_TODO_ITEM_HEIGHT	0.027
TWEAK_FLOAT cf_HEIST_MOUSE_TODO_ITEM_WIDTH	0.265
TWEAK_FLOAT cf_HEIST_MOUSE_TODO_ITEM_GAP	0.007
//TWEAK_FLOAT cf_HEIST_MOUSE_TODO_BOARD_HEIGHT 0.25


// Mouse clickable areas on the board.
ENUM eHEIST_PLANNING_MOUSE_AREA
	
	HEIST_PLANNING_MOUSE_ROLE,
	HEIST_PLANNING_MOUSE_ROLE_LEFT,
	HEIST_PLANNING_MOUSE_ROLE_RIGHT,
	HEIST_PLANNING_MOUSE_CUT,
	HEIST_PLANNING_MOUSE_CUT_LEFT,
	HEIST_PLANNING_MOUSE_CUT_RIGHT,
	HEIST_PLANNING_MOUSE_STATS,
	HEIST_PLANNING_MOUSE_STATS_PLAYER,
	HEIST_PLANNING_MOUSE_STATS_PLAYER_NEXT,
	HEIST_PLANNING_MOUSE_STATS_PLAYER_PREV,
	HEIST_PLANNING_MOUSE_RULES
	
ENDENUM


#IF IS_DEBUG_BUILD
//WIDGET_GROUP_ID DEBUG_PlanningWidgets
BOOL bRenderPlanningDebugText 	= FALSE
BOOL bFakeAllReady				= FALSE
WIDGET_GROUP_ID DEBUG_PlanningWidgets
WIDGET_GROUP_ID DEBUG_ParentWidgets
#ENDIF

/* ======================================================================= *\
	HEIST - TRIGGERING AND BOARD POPULATION
\* ======================================================================= */


/// PURPOSE:
///    Clean all graphics and data for the heist map.
PROC CLEAN_HEIST_MAP()

	PRINTLN("[AMEC][HEIST_LAUNCH] - CLEAN_HEIST_MAP - Cleaning heist map, call stack:")
	DEBUG_PRINTCALLSTACK()

	SET_HEIST_MAP_REMOVE_ALL_PINS(g_HeistSharedClient.PlanningMapIndex)
	SET_HEIST_MAP_REMOVE_ALL_TEXT(g_HeistSharedClient.PlanningMapIndex)
	SET_HEIST_MAP_REMOVE_ALL_ARROWS(g_HeistSharedClient.PlanningMapIndex)
	SET_HEIST_MAP_REMOVE_ALL_AREAS(g_HeistSharedClient.PlanningMapIndex)
	SET_HEIST_MAP_REMOVE_ALL_HIGHLIGHTS(g_HeistSharedClient.PlanningMapIndex)
	SET_HEIST_MAP_REMOVE_ALL_POSTITS(g_HeistSharedClient.PlanningMapIndex)
	
	INT index
	
	FOR index = 0 TO (FMMC_MAX_RULES-1)	
			
		g_HeistPlanningClient.sBoardData.iMapPins[index] 		= 0
		g_HeistPlanningClient.sBoardData.iMapText[index] 		= 0
		g_HeistPlanningClient.sBoardData.iMapArrows[index] 		= 0
		g_HeistPlanningClient.sBoardData.iMapAreas[index] 		= 0
		g_HeistPlanningClient.sBoardData.iMapHighlights[index] 	= 0
		
		g_HeistPlanningClient.sCumPedLocations[index].X 		= INVALID_HEIST_DATA
		g_HeistPlanningClient.sCumPedLocations[index].Y 		= INVALID_HEIST_DATA
		
		g_HeistPlanningClient.sMapRuleColours[index].iRed		= INVALID_HEIST_DATA
		g_HeistPlanningClient.sMapRuleColours[index].iGreen		= INVALID_HEIST_DATA
		g_HeistPlanningClient.sMapRuleColours[index].iBlue		= INVALID_HEIST_DATA
		
	ENDFOR
	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_LAUNCH] - CLEAN_HEIST_MAP - Heist map cleaned successfully.")
//	#ENDIF
	
ENDPROC





/// PURPOSE:
///    DEBUG - Bobby's dodgy flaot to string function.
/// PARAMS:
///    fPos - Float to convert.
/// RETURNS:
///    Text label float.
#IF IS_DEBUG_BUILD
FUNC TEXT_LABEL_63 FLOAT_TO_STRING_F9(FLOAT fPos) 
	TEXT_LABEL_63 tl63 
	tl63 += ROUND(fPos) 
	tl63 += "." 
	FLOAT fTemp = fPos%1 
	fTemp *= 10 
	INT iTemp = ROUND(fTemp) 
	tl63 += iTemp 
	RETURN tl63 
ENDFUNC
#ENDIF


/// PURPOSE:
///    Fill the passed array with the team labels, and null the slots where there isn't a team. This is then passed into
///    scaleform which demarkates non-existant teams via null values.
/// PARAMS:
///    sHeistTeams - String array to fill.
PROC GET_TEAM_ARRAY_WITH_NULLS(TEXT_LABEL_15 &sHeistTeams[])

	INT i
	
	// Set these the undefined so that scaleform knows what is trash and what is real.
	FOR i = 0 TO (g_FMMC_STRUCT.iNumberOfTeams-1)
		sHeistTeams[i] = ""
	ENDFOR
	
	FOR i = 0 TO (g_FMMC_STRUCT.iNumberOfTeams-1)
		sHeistTeams[i] = GET_CORONA_TEAM_NAME(i)
	ENDFOR

ENDPROC


#IF IS_DEBUG_BUILD


PROC PRINT_AVAILABLE_GEAR()
	PRINTLN("[AMEC][HEIST_LAUNCH] - PRINT_AVAILABLE_GEAR - PRINTING AVAILABLE HEIST GEAR: ")
	INT index
	FOR index = 0 TO (g_FMMC_STRUCT.iNumberOfTeams-1)
		IF g_FMMC_STRUCT.iGearDefault[index] = INVALID_HEIST_DATA
			PRINTLN("[AMEC][HEIST_LAUNCH] ... WARNING! Team: ",index," has no default gear!")
		ELSE
			PRINTLN("[AMEC][HEIST_LAUNCH] ... Gear name: ", GET_MP_HEIST_GEAR_NAME(INT_TO_ENUM(MP_HEIST_GEAR_ENUM, g_FMMC_STRUCT.iGearDefault[index])),", team: ", index, ", enum: ", g_FMMC_STRUCT.iGearDefault[index])
		ENDIF
	ENDFOR
ENDPROC

#ENDIF


/// PURPOSE:
///    Display 4 different strings on screen via placeholder.
/// PARAMS:pilot i
///    DisplayAtX - FLOAT X
///    DisplayAtY - FLOAT Y
///    pTextLabel - STRING Placeholder
///    pLiteralString1 - STRING Text1
///    pLiteralString2 - STRING Text2
///    pLiteralString3 - STRING Text3
///    pLiteralString4 - STRING Text4
#IF IS_DEBUG_BUILD
PROC DISPLAY_TEXT_WITH_FOUR_LITERAL_STRINGS(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pTextLabel, STRING pLiteralString1, STRING pLiteralString2, STRING pLiteralString3, STRING pLiteralString4)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(pLiteralString1)
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(pLiteralString2)
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(pLiteralString3)
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(pLiteralString4)
	END_TEXT_COMMAND_DISPLAY_TEXT(DisplayAtX, DisplayAtY)
ENDPROC
#ENDIF


/// PURPOSE:
///    DEBUG - Display 3 different strings on screen via placeholder.
/// PARAMS:
///    DisplayAtX - FLOAT X
///    DisplayAtY - FLOAT Y
///    pTextLabel - STRING Placeholder
///    pLiteralString1 - STRING Text1
///    pLiteralString2 - STRING Text2
///    pLiteralString3 - STRING Text3
#IF IS_DEBUG_BUILD
PROC DISPLAY_TEXT_WITH_THREE_LITERAL_STRINGS(FLOAT DisplayAtX, FLOAT DisplayAtY, STRING pTextLabel, STRING pLiteralString1, STRING pLiteralString2, STRING pLiteralString3)
	BEGIN_TEXT_COMMAND_DISPLAY_TEXT(pTextLabel)
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(pLiteralString1)
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(pLiteralString2)
		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(pLiteralString3)
	END_TEXT_COMMAND_DISPLAY_TEXT(DisplayAtX, DisplayAtY)
ENDPROC
#ENDIF


/// PURPOSE:
///    DEBUG - Set text colour, scale and wrap values.
#IF IS_DEBUG_BUILD
PROC SET_TEXT_COLOR_SCALE_WRAP()
	SET_TEXT_COLOUR(255, 255, 1, 255)
	SET_TEXT_SCALE(0.0000, 0.4200)
	SET_TEXT_WRAP(0.0, 1.0)
ENDPROC
#ENDIF


/// PURPOSE:
///    Get the current number of players on this heist.
/// RETURNS:
///    INT - The number of connected players to this heist.
FUNC INT NUMBER_OF_PLAYERS_ON_HEIST()
	INT iCount
	INT index
	PLAYER_INDEX thisPlayer
	REPEAT NUM_NETWORK_PLAYERS index
		thisPlayer = INT_TO_NATIVE(PLAYER_INDEX, index)
		IF NOT IS_THIS_PLAYER_SCTV_OR_JOINING_AS_SCTV(thisPlayer)
			INT i
			FOR i = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
				IF GlobalServerBD_HeistPlanning.iPlayerOrder[i] = NATIVE_TO_INT(thisPlayer)
					IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, i)
						iCount++	
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
	ENDREPEAT	
	RETURN(iCount)
ENDFUNC


/// PURPOSE:
///    Get the number of players for the heist mission. If it is over the maximum slots, log the error and force to a legal value.
/// RETURNS:
///    INT - The size of the heist mission (player slots).
FUNC INT MAX_NUMBER_OF_PLAYERS_FOR_THIS_HEIST()

	INT iTotal = g_FMMC_STRUCT.iNumParticipants
	
	IF iTotal > MAX_HEIST_PLAYER_SLOTS
		PRINTLN("[AMEC][HEIST_LAUNCH] - MAX_NUMBER_OF_PLAYERS_FOR_THIS_HEIST - ERROR! Max number of mission participants (", iTotal, ") EXCEEDS maximum heist capacity (", MAX_HEIST_PLAYER_SLOTS,").")
		iTotal = MAX_HEIST_PLAYER_SLOTS
	ENDIF
	
	RETURN iTotal

ENDFUNC


/// PURPOSE:
///    Get a specific player's slot on the heist (as listed in iPlayerOrder).
/// PARAMS:
///    PlayerID - The player to check.
/// RETURNS:
///    INT - The index of the player. -1 if falls through.
FUNC INT GET_PLAYER_HEIST_SLOT(PLAYER_INDEX PlayerID)
	INT index
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] = NATIVE_TO_INT(PlayerID)
			RETURN index
		ENDIF
	ENDFOR

	RETURN -1
ENDFUNC


/// PURPOSE:
///    Get role enum for specific player index.
/// PARAMS:
///    PlayerID - Player whose role to get.
/// RETURNS:
///    ENUM - Player role as enum.
FUNC HEIST_ROLES GET_PLAYER_ROLE_ON_HEIST(PLAYER_INDEX PlayerID)
	IF (NATIVE_TO_INT(PlayerID) > -1)	
		INT i
		FOR i = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
			IF GlobalServerBD_HeistPlanning.iPlayerOrder[i] = NATIVE_TO_INT(PlayerID)
				IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, i)
				
					INT iHeistSlot = GET_PLAYER_HEIST_SLOT(PlayerID)
					IF (iHeistSlot > -1)
						RETURN GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[iHeistSlot]
					ENDIF	
					
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	RETURN(HEIST_ROLE_UNASSIGNED)
ENDFUNC


/// PURPOSE:
///    Get outfit enum as INT for specific player index.
/// PARAMS:
///    PlayerID - Player whose outfit to get.
/// RETURNS:
///    INT - Player outfit enum as int.
//FUNC INT GET_PLAYER_OUTFIT_ON_HEIST(PLAYER_INDEX PlayerID)
//
//	IF (NATIVE_TO_INT(PlayerID) > -1)	
//		INT i
//		FOR i = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
//			IF GlobalServerBD_HeistPlanning.iPlayerOrder[i] = NATIVE_TO_INT(PlayerID)
//				IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, i)
//				
//					INT iHeistSlot = GET_PLAYER_HEIST_SLOT(PlayerID)
//					IF (iHeistSlot > -1)
//						RETURN GlobalServerBD_HeistPlanning.iSelectedOutfit[iHeistSlot]
//					ENDIF	
//					
//				ENDIF
//			ENDIF
//		ENDFOR
//	ENDIF
//	
//	RETURN(INVALID_HEIST_DATA)
//
//ENDFUNC


/// PURPOSE:
///    Returns the string to pass to the board to display the gear for the heist.
FUNC STRING GET_HEIST_BOARD_GEAR_IMAGE(INT iContentID)
	SWITCH GET_CURRENT_HEIST_STRAND(iContentID)
		DEFAULT RETURN ""
		CASE HEIST_STRAND_BIOLAB	RETURN "MPHEIST_GEAR_BIOLAB"
		CASE HEIST_STRAND_ORNATE	RETURN "MPHEIST_GEAR_ORNATE"
		CASE HEIST_STRAND_PRISON	RETURN "MPHEIST_GEAR_PRISON"
		CASE HEIST_STRAND_NARCOTICS	RETURN "MPHEIST_GEAR_NARCOTICS"
		CASE HEIST_STRAND_TUTORIAL	RETURN "MPHEIST_GEAR_TUTORIAL"
	ENDSWITCH
ENDFUNC

/// PURPOSE:
///    Check if a specific player is ready to launch the heist. (DUPLICATE?)
/// PARAMS:
///    playerID - the player to check.
/// RETURNS:
///    INT - index value of status.
FUNC INT GET_HEIST_MEMBER_PLANNING_STATE(PLAYER_INDEX playerID)

	INT index
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] != ci_INVALID_PLAYER
			IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] = NATIVE_TO_INT(PlayerID)
				IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
				
					INT iReturnValue = GlobalplayerBD[NATIVE_TO_INT(playerID)].iHeistReadyState
					INT i
					
					FOR i = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
						IF GlobalServerBD_HeistPlanning.iPlayerOrder[i] = NATIVE_TO_INT(PlayerID)
							g_HeistPlanningClient.iPlayerStatuses[i] = iReturnValue
							RETURN iReturnValue
						ENDIF
					ENDFOR
					
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN(-1)
ENDFUNC


/// PURPOSE:
///    Check if a specific player is ready to launch the heist. (DUPLICATE?)
/// PARAMS:
///    playerID - the player to check.
/// RETURNS:
///    BOOLEAN - True if ready to launch.
FUNC BOOL IS_PLAYER_IN_HEIST_READY_TO_LAUNCH(PLAYER_INDEX playerID)

	IF (GET_HEIST_MEMBER_PLANNING_STATE(playerID) = ENUM_TO_INT(STATUS_READY))
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Check the index of player order to see if it has changed. Each client has a local copy of the most up-to-date player order array, this is used
///    to make sure that the server updates correctly before restarting net status checks.
/// PARAMS:
///    index - The index to compare
/// RETURNS:
///    BOOL - True if not different (I.E. both updated).
FUNC BOOL HAS_PLAYER_ROLES_UPDATED()

	INT index
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] != GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index]
			RETURN FALSE
		ENDIF
	ENDFOR

	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Get the currently assigned team for the selected heist member.
/// PARAMS:
///    iMemberIndex - 0-7 which heist member.
/// RETURNS:
///    INT member's team. -1 for unassigned, -2 for fail.
FUNC INT GET_HEIST_MEMBER_TEAM_NUMBER(INT iMemberIndex)

	IF iMemberIndex < 0
	OR iMemberIndex > (MAX_HEIST_PLAYER_SLOTS-1)
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - GET_HEIST_MEMBER_TEAM_NUMBER - ERROR! Member index out of range! Value: ", iMemberIndex)
		#ENDIF
		
		RETURN INVALID_HEIST_DATA
	ENDIF

	IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
		IF HAS_PLAYER_ROLES_UPDATED()
			RETURN ENUM_TO_INT(GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[iMemberIndex])
		ELSE
			RETURN ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[iMemberIndex])
		ENDIF
	ELSE
		RETURN ENUM_TO_INT(GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[iMemberIndex])
	ENDIF 
	
ENDFUNC


/// PURPOSE:
///    Check if a specific player is ready to launch the heist. (DUPLICATE?)
/// PARAMS:
///    playerID - the player to check.
/// RETURNS:
///    BOOLEAN - True if ready to launch.
FUNC INT GET_PLAYER_IN_HEIST_READY_TO_LAUNCH(PLAYER_INDEX playerID)

	IF (GET_HEIST_MEMBER_PLANNING_STATE(playerID) = ENUM_TO_INT(STATUS_READY))
		RETURN ENUM_TO_INT(STATUS_READY)
	ENDIF
	
	RETURN ENUM_TO_INT(STATUS_NOT_READY)
ENDFUNC


/// PURPOSE:
///    Get the total number of players whose role is X.
/// PARAMS:
///    eRole - The role to count.
/// RETURNS:
///    INT - Total number of players marked with that role.
PROC GET_ROLE_TOTAL_FROM_TEAMS(INT &iTeamNumbers[], BOOL bIsLeader = FALSE)

	INT index

	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] != -1
			// Check the player net status.
			IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
				IF bIsLeader
					SWITCH ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index])
						CASE 0 iTeamNumbers[0]++ BREAK
						CASE 1 iTeamNumbers[1]++ BREAK
						CASE 2 iTeamNumbers[2]++ BREAK
						CASE 3 iTeamNumbers[3]++ BREAK
					ENDSWITCH
				ELSE
					SWITCH ENUM_TO_INT(GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index])
						CASE 0 iTeamNumbers[0]++ BREAK
						CASE 1 iTeamNumbers[1]++ BREAK
						CASE 2 iTeamNumbers[2]++ BREAK
						CASE 3 iTeamNumbers[3]++ BREAK
					ENDSWITCH
				ENDIF
			ENDIF	
		ENDIF
	ENDFOR
	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_LAUNCH] - GET_ROLE_TOTAL_FROM_TEAMS - Team Details:  0:",iTeamNumbers[0],"  1:",iTeamNumbers[1],"  2:",iTeamNumbers[2],"  3:",iTeamNumbers[3])
//	#ENDIF

ENDPROC



/// PURPOSE:
///    Get the remaining number of roles for the provided team.
/// PARAMS:
///    iTeam - Team to check.
/// RETURNS:
///    INT number of slots available on the team.
FUNC INT GET_REMAINING_ROLES_FOR_TEAM(INT iTeam)

	IF iTeam < 0
	OR iTeam > (FMMC_MAX_TEAMS-1)
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_LAUNCH] - GET_REMAINING_ROLES_FOR_TEAM - iTeam is INVALID, exiting. Value: ", iTeam)
//		#ENDIF
		RETURN 0
	ENDIF

	// Check for the minimum team requirements.
	INT iTeamTotals[FMMC_MAX_TEAMS]
	INT iRemainingTeam, index
	
	GET_ROLE_TOTAL_FROM_TEAMS(iTeamTotals, IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID()))
	
	FOR index = 0 TO (FMMC_MAX_TEAMS-1)
		IF index < g_FMMC_STRUCT.iNumberOfTeams
			IF index = iTeam
				// Found the players team, it is also valid.
				iRemainingTeam = (g_FMMC_STRUCT.iNumPlayersPerTeam[index] - iTeamTotals[iTeam])
			ENDIF
		ENDIF
	ENDFOR
	
	IF iRemainingTeam < 0
		iRemainingTeam = 0
	ENDIF
	
	RETURN iRemainingTeam

ENDFUNC


FUNC INT GET_MY_TEAM_VALID_STATUS(INT iPlayerTeam)
	
	IF iPlayerTeam < 0
	OR iPlayerTeam > (FMMC_MAX_TEAMS-1)
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_LAUNCH] - GET_REMAINING_ROLES_FOR_TEAM - iTeam is INVALID, exiting. Value: ", iTeam)
//		#ENDIF
		RETURN ci_HEIST_TEAM_STATUS_INVALID
	ENDIF
	
	// Check for the minimum team requirements.
	INT iTeamTotals[FMMC_MAX_TEAMS]
	
	GET_ROLE_TOTAL_FROM_TEAMS(iTeamTotals, IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID()))
	
	IF iTeamTotals[iPlayerTeam] <= g_FMMC_STRUCT.iNumPlayersPerTeam[iPlayerTeam]
		RETURN ci_HEIST_TEAM_STATUS_VALID
	ELSE
		RETURN ci_HEIST_TEAM_STATUS_INVALID
	ENDIF

ENDFUNC 


/// PURPOSE:
///    Check if a player has been assigned an outfit.
/// PARAMS:
///    iSelectedPlayer - 
/// RETURNS:
///    
FUNC BOOL HAS_PLAYER_BEEN_ASSIGNED_OUTFIT(INT iSelectedPlayer)

	IF iSelectedPlayer < 0
	OR iSelectedPlayer > MAX_HEIST_PLAYER_SLOTS
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - HAS_PLAYER_BEEN_ASSIGNED_OUTFIT - ERROR! iSelectedPlayer is out-of-bounds. Value: ", iSelectedPlayer)
		#ENDIF
		
		RETURN FALSE
	
	ENDIF

	IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
		IF g_HeistPlanningClient.iSelectedOutfit[iSelectedPlayer] != INVALID_HEIST_DATA
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		IF GlobalServerBD_HeistPlanning.iSelectedOutfit[iSelectedPlayer] != INVALID_HEIST_DATA
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ENDIF

ENDFUNC


/// PURPOSE:
///    Get the status label (and icon) for a specific player.
/// PARAMS:
///    playerID - ID of the player
///    iStatusIcon - BYREF status icon enum.
/// RETURNS:
///    STRING - StringTable label for the specific player's status.
FUNC STRING GET_STATUS_INFO_FOR_PLAYER(PLAYER_INDEX playerID, INT &iStatusIcon)

	IF IS_PLAYER_LEADER_OF_HEIST(playerID)
		iStatusIcon = ENUM_TO_INT(STATUS_READY)
		RETURN "HEIST_RDY_YES"
	ENDIF
	
//	IF NOT IS_MP_HEIST_FINALE_PLANNING_CURRENTLY_IN_USE()
//		iStatusIcon = ENUM_TO_INT(STATUS_BLANK)
//		RETURN ""
//	ENDIF

	IF IS_PLAYER_IN_HEIST_READY_TO_LAUNCH(playerID)
		iStatusIcon = ENUM_TO_INT(STATUS_READY)
		RETURN "HEIST_RDY_YES"
	ELSE
		iStatusIcon = ENUM_TO_INT(STATUS_NOT_READY)
		RETURN "HEIST_RDY_NO"
	ENDIF

ENDFUNC


/// PURPOSE:
///    Get the role (and icon) for the provided player.
/// PARAMS:
///    index - Index of the selected player.
///    playerID - ID of the selected player.
///    iRoleIcon - BYREF role icon enum.
/// RETURNS:
///    TEXT_LABEL_15 - StringTable label for the specific player's role.
FUNC TEXT_LABEL_15 GET_ROLE_INFO_FOR_PLAYER(PLAYER_INDEX playerID, INT &iRoleIcon, INT index)
	
	//INT iPlayerTeam = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].sClientCoronaData.iTeamChosen
	INT iPlayerTeam
	INT iTeamTotals[FMMC_MAX_TEAMS]
	BOOL bUseSingular = TRUE
	
	IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
		iPlayerTeam = ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index])
		GET_ROLE_TOTAL_FROM_TEAMS(iTeamTotals, TRUE)
	ELSE
		iPlayerTeam = ENUM_TO_INT(GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index])
		GET_ROLE_TOTAL_FROM_TEAMS(iTeamTotals, FALSE)
	ENDIF
	
	IF iPlayerTeam != INVALID_HEIST_DATA
	AND iPlayerTeam < FMMC_MAX_TEAMS
		IF iTeamTotals[iPlayerTeam] > 1
			bUseSingular = FALSE
		ENDIF
	ENDIF
	
	TEXT_LABEL_15 sRoleLabel = GET_CORONA_TEAM_NAME(iPlayerTeam, bUseSingular)
	
	IF IS_PLAYER_LEADER_OF_HEIST(playerID)
		iRoleIcon = ENUM_TO_INT(ROLE_LEADER)
	ELSE
		iRoleIcon = ENUM_TO_INT(ROLE_NONE)
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_LAUNCH] - GET_ROLE_INFO_FOR_PLAYER - Player: ",GET_PLAYER_NAME(playerID)," | Icon = ",iRoleIcon," Label = ",sRoleLabel)
//	#ENDIF
	
	RETURN sRoleLabel
ENDFUNC


/// PURPOSE:
///    Get the cut percentage for a specific player.
/// PARAMS:
///    index - Index for the player whose cut to get (0 = Leader, etc).
/// RETURNS:
///    INT - Cut percent.
FUNC INT GET_CUT_INFO_FOR_PLAYER(INT index)

	INT iCutPercent

	IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
		iCutPercent = g_HeistPlanningClient.sHeistCut.iCutPercent[index]
	ELSE
		iCutPercent = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[index]
	ENDIF

	RETURN iCutPercent
	
ENDFUNC





//FUNC TEXT_LABEL_15 GET_PICTURE_NAME_FROM_HEIST_MEMBER_OUTFIT(INT iSelectedMember)
//
//	TEXT_LABEL_15 	tlPictureName = "m_"
//	TEXT_LABEL_7 	tlExtension = ".jpg"
//	
//	IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
//		tlPictureName += g_HeistPlanningClient.iSelectedOutfit[iSelectedMember]
//	ELSE
//		tlPictureName += GlobalServerBD_HeistPlanning.iSelectedOutfit[iSelectedMember]
//	ENDIF
//	
//	tlPictureName += tlExtension
//	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_LAUNCH_PHOTO] - GET_PICTURE_NAME_FROM_HEIST_MEMBER_OUTFIT - Getting picture for member ",iSelectedMember,". Name: ",tlPictureName)
//	#ENDIF
//	
//	RETURN tlPictureName
//
//ENDFUNC




FUNC TEXT_LABEL_23 GET_ACTIVE_HEIST_NAME()

	TEXT_LABEL_23 tlTemp = "ornate/"

	RETURN tlTemp

ENDFUNC

//heist_planning/finale_-1072870761/		//The_Flecca_Job
//heist_planning/finale_-2043623611/		//The_Prison_Break
//heist_planning/finale_-1096986654/		//The_Humane_Labs_Raid  
//heist_planning/finale_164435858/			//Series_A_Funding  
//heist_planning/finale_-231973569/			//The_Pacific_Standard_Job

//get the pictures based on the root content ID of the mission we are currently on
FUNC TEXT_LABEL_63 GET_HEIST_PICTURE_ROOT_DIRECTORY(INT iHeistHash, INT iLoop = -1)

	TEXT_LABEL_63 tlPath = "finale_"
	tlPath += iHeistHash
	IF iLoop != -1
		tlPath += iLoop
	ENDIF	

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH_PHOTO] - GET_HEIST_PICTURE_ROOT_DIRECTORY - Completed dir string: ", tlPath)
	#ENDIF

	RETURN tlPath
	
ENDFUNC
FUNC TEXT_LABEL_63 GET_HEIST_PICTURE_DOWNLOAD_DIRECTORY(INT iHeistHash)

	TEXT_LABEL_63 tlPath = "heist_planning/"
	TEXT_LABEL_63 tlPathtemp = GET_HEIST_PICTURE_ROOT_DIRECTORY(iHeistHash)
	tlPath += tlPathtemp
	tlPath += "/"
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH_PHOTO] - GET_HEIST_PICTURE_DOWNLOAD_DIRECTORY - Completed dir string: ", tlPath)
	#ENDIF

	RETURN tlPath
	
ENDFUNC

//Get the imnage file name (Alastair, it just adds .jpg to an INT)
FUNC TEXT_LABEL_15 GET_PICTURE_NAME_FOR_HEIST_PLANNING_FROM_IMAGE_NUMBER(INT iSelectedMember)
	TEXT_LABEL_15 tlPictureName = iSelectedMember
	tlPictureName += ".jpg"
	RETURN tlPictureName
ENDFUNC


//Downloads and draws a picture
FUNC BOOL DOWNLOAD_PHOTO_FOR_HEIST_PLANNING_BOARD(STRUCT_DL_PHOTO_VARS_LITE &sDownLoadPhotoVars, TEXT_LABEL_63 cloudPath, TEXT_LABEL_63 imageNameForDL, TEXT_LABEL_15 textureName)
	
	TEXT_LABEL_63 tl63
	
	SWITCH sDownLoadPhotoVars.iDownLoadPorgress
	
		CASE ciDOWNLOAD_PHOTO_FOR_FMMC_INT
			TEXTURE_DOWNLOAD_RELEASE(sDownLoadPhotoVars.iTextureDownloadHandle)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH_PHOTO] - DOWNLOAD_PHOTO_FOR_HEIST_PLANNING_BOARD - TEXTURE_DOWNLOAD_RELEASE")
			#ENDIF
			
			sDownLoadPhotoVars.iTextureDownloadHandle = 0
			sDownLoadPhotoVars.iDownLoadPorgress = ciDOWNLOAD_PHOTO_FOR_FMMC_SET_UP
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH_PHOTO] - DOWNLOAD_PHOTO_FOR_HEIST_PLANNING_BOARD - sDownLoadPhotoVars.iDownLoadPorgress = ciDOWNLOAD_PHOTO_FOR_FMMC_SET_UP")
			#ENDIF
			
		BREAK
		
		//Set up the download
		CASE ciDOWNLOAD_PHOTO_FOR_FMMC_SET_UP
			tl63 = cloudPath
			tl63 += textureName
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH_PHOTO] - DOWNLOAD_PHOTO_FOR_HEIST_PLANNING_BOARD - TITLE_TEXTURE_DOWNLOAD_REQUEST(", tl63, ")")
			#ENDIF
			
			sDownLoadPhotoVars.iTextureDownloadHandle = TITLE_TEXTURE_DOWNLOAD_REQUEST(tl63, imageNameForDL)	
			//Move stage
			sDownLoadPhotoVars.iDownLoadPorgress = ciDOWNLOAD_PHOTO_FOR_FMMC_DOWNLOAD
		BREAK
		
		//wait for it to finis
		CASE ciDOWNLOAD_PHOTO_FOR_FMMC_DOWNLOAD		
		
			//Wait to get
			SWITCH  GET_STATUS_OF_TEXTURE_DOWNLOAD(sDownLoadPhotoVars.iTextureDownloadHandle)
		
				CASE PHOTO_OPERATION_IN_PROGRESS
					//PRINTLN("DOWNLOAD_PHOTO_FOR_FMMC - GET_STATUS_OF_TEXTURE_DOWNLOAD - PHOTO_OPERATION_IN_PROGRESS = (", sDownLoadPhotoVars.iTextureDownloadHandle, ")")
					RETURN FALSE
					
				CASE PHOTO_OPERATION_SUCCEEDED
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH_PHOTO] - DOWNLOAD_PHOTO_FOR_HEIST_PLANNING_BOARD - PHOTO_OPERATION_SUCCEEDED = (", sDownLoadPhotoVars.iTextureDownloadHandle, ")")
					#ENDIF
					
					sDownLoadPhotoVars.bSucess = TRUE
					
					SET_HEIST_UPDATE_AVAILABLE(TRUE)
					
					//Get out
					RETURN TRUE
				
				CASE PHOTO_OPERATION_FAILED
					sDownLoadPhotoVars.iFailCount++
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH_PHOTO] - DOWNLOAD_PHOTO_FOR_HEIST_PLANNING_BOARD - PHOTO_OPERATION_FAILED = (", sDownLoadPhotoVars.iTextureDownloadHandle, ")")
					#ENDIF
					
					IF sDownLoadPhotoVars.iFailCount > 2
						IF sDownLoadPhotoVars.iTextureDownloadHandle != 0
						
							#IF IS_DEBUG_BUILD
								PRINTLN("[AMEC][HEIST_LAUNCH_PHOTO] - DOWNLOAD_PHOTO_FOR_HEIST_PLANNING_BOARD - TEXTURE_DOWNLOAD_RELEASE(", sDownLoadPhotoVars.iTextureDownloadHandle, ")")
							#ENDIF
						
							TEXTURE_DOWNLOAD_RELEASE(sDownLoadPhotoVars.iTextureDownloadHandle)
						ENDIF
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_LAUNCH_PHOTO] - DOWNLOAD_PHOTO_FOR_HEIST_PLANNING_BOARD - PHOTO_OPERATION_FAILED more than 2 times")
						#ENDIF

						sDownLoadPhotoVars.bSucess = FALSE	// - MAKE TRUE SO WE JUST STOP TRYING AND DISPLAY EMPTY???
						
						SET_HEIST_UPDATE_AVAILABLE(TRUE)
						
						//Get out
						RETURN TRUE
					ENDIF
				BREAK
					
			ENDSWITCH
		BREAK
		
	ENDSWITCH
	RETURN FALSE
ENDFUNC


//Download all (2) the images for the planning board
FUNC BOOL DOWNLOAD_ALL_PHOTOS_FOR_HEIST_PLANNING_BOARD()
	INT iLoop
	BOOL bReturn = TRUE
	//Loop all two of the imaged
	REPEAT ciHEIST_PLANNING_BOARD_IMAGE_MAX iLoop
		//Not already downloaded
		IF NOT g_HeistPlanningClient.bImagesDown[iLoop]
			//then get
			IF DOWNLOAD_PHOTO_FOR_HEIST_PLANNING_BOARD(g_HeistPlanningClient.sPhotoVars[iLoop], GET_HEIST_PICTURE_DOWNLOAD_DIRECTORY(g_FMMC_STRUCT.iRootContentIDHash),GET_HEIST_PICTURE_ROOT_DIRECTORY(g_FMMC_STRUCT.iRootContentIDHash, iLoop), GET_PICTURE_NAME_FOR_HEIST_PLANNING_FROM_IMAGE_NUMBER(iLoop))
				g_HeistPlanningClient.bImagesDown[iLoop] = TRUE
			//not ready yet
			ELSE
				bReturn = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN bReturn
ENDFUNC


/// PURPOSE:
///    Find available slot to load headshot into - index corresponds to value passed to scaleform
/// PARAMS:
///    playerID - Player to store ready for a headshot.
/// RETURNS:
///   	INT - The index of the stored player. -1 if it falls through.
FUNC INT STORE_PLAYER_FOR_HEADSHOT(PLAYER_INDEX playerID)
	INT i
	REPEAT MAX_HEIST_PLAYER_SLOTS i
		IF g_HeistPlanningClient.sHeistHeadshots[i].playerID = INVALID_PLAYER_INDEX()
			g_HeistPlanningClient.sHeistHeadshots[i].playerID = playerID
			g_HeistPlanningClient.sHeistHeadshots[i].eHeadshotState = HEADSHOT_NULL
			RETURN i
		ENDIF
	ENDREPEAT
	
	RETURN -1
ENDFUNC


/// PURPOSE:
///    Check to see if we are already requesting and loading a ped headshot.
/// RETURNS:
///    BOOLEAN - True if requesting or loading.
FUNC BOOL ARE_ANY_HEADSHOTS_BEING_LOADED()

	INT i
	FOR i = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
	
		IF g_HeistPlanningClient.sHeistHeadshots[i].eHeadshotState = HEADSHOT_REQUESTING
			RETURN TRUE
		ENDIF
	
	ENDFOR

	RETURN FALSE
ENDFUNC



FUNC BOOL ARE_ALL_MEMBER_HEADSHOTS_READY(BOOL bInCorona = TRUE)

	INT index
	BOOL bAllReady = TRUE
	
//	#IF IS_DEBUG_BUILD
//		IF isLeader
//			PRINTLN("[AMEC][HEIST_LAUNCH_HEADSHOT] - ARE_ALL_MEMBER_HEADSHOTS_READY - **** Starting headshot status printout (callstack below) **** ")
//			DEBUG_PRINTCALLSTACK()
//		ENDIF
//	#ENDIF
	
	IF bInCorona
		IF NOT g_HeistPlanningClient.bPlayerNetStatusInSync
		
			#IF IS_DEBUG_BUILD
				CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - ARE_ALL_MEMBER_HEADSHOTS_READY - bPlayerNetStatusInSync = FALSE, wait for this to be TRUE before starting the headshot monitor.")
			#ENDIF
		
			RETURN FALSE
		ENDIF
	ENDIF

	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
	
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] != -1
		
			// Check the player net status.
			IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
			
				IF IS_NET_PLAYER_OK(g_HeistPlanningClient.sHeistHeadshots[index].playerID)
			
					IF g_HeistPlanningClient.sHeistHeadshots[index].eHeadshotState != HEADSHOT_AVAILABLE
					
						#IF IS_DEBUG_BUILD
							CDEBUG2LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - ARE_ALL_MEMBER_HEADSHOTS_READY - WARNING! Headshot display is being held up by: [",index,", ", GET_PLAYER_NAME(g_HeistPlanningClient.sHeistHeadshots[index].playerID),"]. State: ", g_HeistPlanningClient.sHeistHeadshots[index].eHeadshotState)
						#ENDIF
					
						bAllReady = FALSE
					ENDIF
					
				ENDIF
			
			ENDIF
			
		ENDIF
		
	ENDFOR
	
	IF bAllReady
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_LAUNCH_HEADSHOT] - ARE_ALL_MEMBER_HEADSHOTS_READY - All heist member headshots returned HEADSHOT_AVAILABLE. Load time: ", NATIVE_TO_INT(g_HeistPlanningClient.sHeadshotLoadTimer.Timer))
//		#ENDIF
		
		IF HAS_NET_TIMER_STARTED(g_HeistPlanningClient.sHeadshotLoadTimer)
			RESET_NET_TIMER(g_HeistPlanningClient.sHeadshotLoadTimer)
		ENDIF
		
		g_HeistPlanningClient.bPlanningImagesReady = TRUE
		
	ENDIF
	
	RETURN bAllReady

ENDFUNC

FUNC BOOL HAVE_REACHED_HEADSHOT_TIMEOUT()

	IF HAS_NET_TIMER_EXPIRED(g_HeistPlanningClient.sHeadshotLoadTimer, ci_HEIST_HEADSHOT_LOAD_MAX_DURATION)
	
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_LAUNCH_HEADSHOT] - !! HAVE_REACHED_HEADSHOT_TIMEOUT - WARNING! Headshot loading timer expired, falling back to default placeholders.")
//		#ENDIF
		
		RETURN TRUE
	
	ENDIF
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Loads ped headshot into hesitHeadshots array. Script requests texture first before loading it into the scaleform
/// PARAMS:
///    index - Member number (0 = leader, etc).
/// RETURNS:
///    BOOLEAN - True when headshot is ready for use. 
FUNC BOOL IS_HEIST_HEADSHOT_READY(INT index)	

	STRING 	texture
	INT 	tempLoop
	BOOL 	bHasFound = FALSE

	SWITCH g_HeistPlanningClient.sHeistHeadshots[index].eHeadshotState
	
		CASE HEADSHOT_NULL
			IF NOT ARE_ANY_HEADSHOTS_BEING_LOADED()
				g_HeistPlanningClient.sHeistHeadshots[index].eHeadshotState = HEADSHOT_REQUESTING
				
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - IS_HEIST_HEADSHOT_READY - Init loading headshot for: [",index,", ", GET_PLAYER_NAME(g_HeistPlanningClient.sHeistHeadshots[index].playerID),"]")
				#ENDIF
			ENDIF
			
			BREAK
		
		// Script must initially request the headshot
		CASE HEADSHOT_REQUESTING
			IF NOT IS_PED_INJURED(GET_PLAYER_PED(g_HeistPlanningClient.sHeistHeadshots[index].playerID))
				IF g_HeistPlanningClient.sHeistHeadshots[index].pedHeadshotID = NULL
					// Get headshot ID - this method handles all of the validation to ensure it is a valid ID.
					g_HeistPlanningClient.sHeistHeadshots[index].pedHeadshotID = Get_HeadshotID_For_Player(g_HeistPlanningClient.sHeistHeadshots[index].playerID)
					
					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - IS_HEIST_HEADSHOT_READY - Performing request for headshot for: [",index,", ", GET_PLAYER_NAME(g_HeistPlanningClient.sHeistHeadshots[index].playerID),"]")
					#ENDIF
				ELSE
					g_HeistPlanningClient.sHeistHeadshots[index].sHeadshotText = GET_PEDHEADSHOT_TXD_STRING(g_HeistPlanningClient.sHeistHeadshots[index].pedHeadshotID)					
										
					REPEAT g_numGeneratedHeadshotsMP tempLoop
						IF (g_sGeneratedHeadshotsMP[temploop].hsdPlayerID = g_HeistPlanningClient.sHeistHeadshots[index].playerID)
							IF g_HeistPlanningClient.sHeistHeadshots[index].pedHeadshotID != g_sGeneratedHeadshotsMP[temploop].hsdHeadshotID
								CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - IS_HEIST_HEADSHOT_READY - Error! - Player doesn't have a valid Headshot ID.")
								CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - IS_HEIST_HEADSHOT_READY - index = ",index)
								CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - IS_HEIST_HEADSHOT_READY - player = ", GET_PLAYER_NAME(g_HeistPlanningClient.sHeistHeadshots[index].playerID))
								g_HeistPlanningClient.sHeistHeadshots[index].pedHeadshotID = NULL
								RETURN FALSE
							ENDIF
							
							bHasFound = TRUE
						ENDIF
					ENDREPEAT
					
					IF NOT bHasFound
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - IS_HEIST_HEADSHOT_READY - Error! - Player doesn't exist in headshot queue.")
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - IS_HEIST_HEADSHOT_READY - index = ",index)
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - IS_HEIST_HEADSHOT_READY - player = ", GET_PLAYER_NAME(g_HeistPlanningClient.sHeistHeadshots[index].playerID))
						g_HeistPlanningClient.sHeistHeadshots[index].pedHeadshotID = NULL	
						RETURN FALSE
					ENDIF
								
					IF IS_STRING_NULL_OR_EMPTY(g_HeistPlanningClient.sHeistHeadshots[index].sHeadshotText)
					OR ARE_STRINGS_EQUAL(" ",g_HeistPlanningClient.sHeistHeadshots[index].sHeadshotText)
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - IS_HEIST_HEADSHOT_READY - GET_PEDHEADSHOT_TXD_STRING returned emptystring or 'space'")
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - IS_HEIST_HEADSHOT_READY - g_HeistPlanningClient.sHeistHeadshots[index].sHeadshotText = (",g_HeistPlanningClient.sHeistHeadshots[index].sHeadshotText,")")
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - IS_HEIST_HEADSHOT_READY - index = ",index)
						CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - IS_HEIST_HEADSHOT_READY - player = ", GET_PLAYER_NAME(g_HeistPlanningClient.sHeistHeadshots[index].playerID))
						//attempt to get headshot again
						g_HeistPlanningClient.sHeistHeadshots[index].pedHeadshotID = NULL
						RETURN FALSE
					ELSE
						g_HeistPlanningClient.sHeistHeadshots[index].eHeadshotState = HEADSHOT_AVAILABLE
						
						#IF IS_DEBUG_BUILD
							CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - IS_HEIST_HEADSHOT_READY - Headshot now available for: [",index,", ", GET_PLAYER_NAME(g_HeistPlanningClient.sHeistHeadshots[index].playerID),"]. String: ", g_HeistPlanningClient.sHeistHeadshots[index].sHeadshotText)
						#ENDIF
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - IS_HEIST_HEADSHOT_READY - Nullifying headshot for: [",index,", ", GET_PLAYER_NAME(g_HeistPlanningClient.sHeistHeadshots[index].playerID),"]")
				#ENDIF
				g_HeistPlanningClient.sHeistHeadshots[index].eHeadshotState = HEADSHOT_NULL
			ENDIF
			
			BREAK
			
		// Now we can use the headshot
		CASE HEADSHOT_AVAILABLE
			//B* 2107175
			//double check correct texture string is grabbed.
			texture = GET_PEDHEADSHOT_TXD_STRING(g_HeistPlanningClient.sHeistHeadshots[index].pedHeadshotID)
			IF NOT IS_STRING_NULL_OR_EMPTY(texture)
			AND NOT ARE_STRINGS_EQUAL(" ",texture)
				g_HeistPlanningClient.sHeistHeadshots[index].sHeadshotText = texture
			ENDIF
			
			RETURN TRUE
	ENDSWITCH

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Check if this heist finale is for the tutorial strand.
/// RETURNS:
///    TRUE if tutorial.
FUNC BOOL IS_THIS_A_TUTORIAL_HEIST()
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwo, ciHEIST_TUTORIAL_STRAND)
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC


/// PURPOSE:
///    Get headshot for a heist member.
/// PARAMS:
///    playerID - The member whose headshot to retrieve.
/// RETURNS:
///    Headshot TXD string (already preloaded).
FUNC TEXT_LABEL_23 GET_HEIST_MEMBER_HEADSHOT(PLAYER_INDEX playerID, BOOL bInCorona = TRUE)

	TEXT_LABEL_23 sPortraitSlot

	IF bInCorona
		IF NOT g_HeistPlanningClient.bPlayerNetStatusInSync
			#IF IS_DEBUG_BUILD
				CDEBUG2LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - GET_HEIST_MEMBER_HEADSHOT - bPlayerNetStatusInSync = FALSE, waiting for network sync before requesting headshots.")
			#ENDIF
			
			RETURN sPortraitSlot
		ENDIF
	ENDIF
	
	//sPortraitSlot = "CHAR_DEFAULT"
				
	INT index = ci_INVALID_PLAYER
	INT i
	FOR i = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF g_HeistPlanningClient.sHeistHeadshots[i].playerID = playerID
			index = i
			i = MAX_HEIST_PLAYER_SLOTS
		ENDIF
	ENDFOR
	
	IF index = ci_INVALID_PLAYER
		index = STORE_PLAYER_FOR_HEADSHOT(playerID)
		#IF IS_DEBUG_BUILD
			CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - GET_HEIST_MEMBER_HEADSHOT - Stored headshot for player [",index,", ", GET_PLAYER_NAME(playerID),"]")
		#ENDIF
	ENDIF

	IF index != ci_INVALID_PLAYER	
		IF IS_HEIST_HEADSHOT_READY(index)
			IF ARE_ALL_MEMBER_HEADSHOTS_READY(bInCorona)
								
				sPortraitSlot = g_HeistPlanningClient.sHeistHeadshots[index].sHeadshotText
				
				IF g_HeistPlanningClient.bSuppressHeadshotScaleformLoading
					PRINTLN("[RBJ][HEIST_LAUNCH_HEADSHOT] - net_heist_planning - GET_HEIST_MEMBER_HEADSHOT - Resetting suppress and cache data for headshots.")
					g_HeistPlanningClient.bSuppressHeadshotScaleformLoading = FALSE
					
					INT x
					FOR x = 0 TO MAX_HEIST_PLAYER_SLOTS - 1
						g_HeistPlanningClient.sHeistDataCache.sPlayerListCached[x].sPlayerPortrait = 0
					ENDFOR
				ENDIF
				
				#IF IS_DEBUG_BUILD
					CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - GET_HEIST_MEMBER_HEADSHOT - ARE_ALL_MEMBER_HEADSHOTS_READY = TRUE, portrait for player: ",GET_PLAYER_NAME(playerID), " is string: ", sPortraitSlot)
				#ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - ARE_ALL_MEMBER_HEADSHOTS_READY = FALSE, cannot display headshots.")
				#ENDIF
			ENDIF
		ELSE
			IF HAVE_REACHED_HEADSHOT_TIMEOUT()
				#IF IS_DEBUG_BUILD
					CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH_HEADSHOT] - HAVE_REACHED_HEADSHOT_TIMEOUT = TRUE, checking for empty string.")
				#ENDIF
				IF IS_STRING_NULL_OR_EMPTY(sPortraitSlot)
					sPortraitSlot = "CHAR_DEFAULT"
				ENDIF
				
				g_HeistPlanningClient.bPlanningImagesReady = TRUE
			ENDIF
		ENDIF
	
	ENDIF
	
	RETURN sPortraitSlot
ENDFUNC


/// PURPOSE:
///    Due to there being no way of completely setting all of the values in an array at once, we must do this manually.
/// PARAMS:
///    targetArray - The array to modify.
///    iValue - The desired value.
PROC SET_ALL_VALUES_IN_ARRAY_INT(INT &targetArray[], INT iValue)

	INT arraySize, index 
	
	arraySize = COUNT_OF(targetArray)
	
	FOR index = 0 TO (arraySize-1)
		targetArray[index] = iValue
	ENDFOR

ENDPROC


/// PURPOSE:
///    Check the index of player order to see if it has changed. Each client has a local copy of the most up-to-date player order array, this is used
///    to make sure that the server updates correctly before restarting net status checks.
/// PARAMS:
///    index - The index to compare
/// RETURNS:
///    BOOL - True if not different (I.E. both updated).
//FUNC BOOL HAS_PLAYER_ORDER_UPDATED()
//
//	INT index
//	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
//		IF g_HeistPlanningClient.iPlayerOrder[index] != GlobalServerBD_HeistPlanning.iPlayerOrder[index]
//			RETURN FALSE
//		ENDIF
//	ENDFOR
//	
//	RETURN TRUE
//
//ENDFUNC


/// PURPOSE:
///    Check if the heist teams are all full. This is used to know when to modify team selection behaviour.
/// RETURNS:
///    TRUE if each team is equal to maximum potential value.
FUNC BOOL ARE_ALL_TEAMS_FULL()

	INT iTeamTotals[FMMC_MAX_TEAMS]
	GET_ROLE_TOTAL_FROM_TEAMS(iTeamTotals)
	
	BOOL bTeamsFull = TRUE
	
	INT index
	FOR index = 0 TO (FMMC_MAX_TEAMS-1)
		IF iTeamTotals[index] != g_FMMC_STRUCT.iMaxNumPlayersPerTeam[index]
			bTeamsFull = FALSE
		ENDIF
	ENDFOR
	
	RETURN bTeamsFull

ENDFUNC


/// PURPOSE:
///    For the heist leader to update their cuts data in the same way that the global data is updated.
/// PARAMS:
///    index - Player index to modify.
///    iModifier - Value to modify by.
PROC PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL(INT index, INT iModifier, INT iSource)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
		PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - Updating local cuts. Modifier: ", iModifier)
	#ENDIF
	
	IF iModifier < 0 	
		// DECREMENT TARGET, INCREASE SOURCE.
		INT iTargetCut = g_HeistPlanningClient.sHeistCut.iCutPercent[index]
		IF iTargetCut < ABSI(iModifier)
			iModifier = iTargetCut
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - Decrease target cut. iModifier: ", iModifier)
		#ENDIF
		
		IF g_HeistPlanningClient.sHeistCut.iCutPercent[iSource] > g_sMPTUNABLES.imax_heist_cut_amount
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - Cannot decrease target cut! Source already greater cut than allowed maximum! Source cut: ", g_HeistPlanningClient.sHeistCut.iCutPercent[iSource], ", Maximum: ", g_sMPTUNABLES.imax_heist_cut_amount)
			#ENDIF
			
			// Because the source has somehow ended up with more than the maximum, calculate how much over they are and add it back onto the target.
			INT iRem
			iRem = g_HeistPlanningClient.sHeistCut.iCutPercent[iSource] - g_sMPTUNABLES.imax_heist_cut_amount
			g_HeistPlanningClient.sHeistCut.iCutPercent[index] += iRem
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - Added remainder: ", iRem, ", to target total: ", (g_HeistPlanningClient.sHeistCut.iCutPercent[index] - iRem), " to rebalance player cuts.")
			#ENDIF
			
		ELIF g_HeistPlanningClient.sHeistCut.iCutPercent[iSource] = g_sMPTUNABLES.imax_heist_cut_amount
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - Cannot decrease target cut, source already at maximum: ", g_sMPTUNABLES.imax_heist_cut_amount)
			#ENDIF
			
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - DECREASE - Pre cut modifications: ")
				PRINTLN("[AMEC][HEIST_LAUNCH] - iSource(%)	: 	", g_HeistPlanningClient.sHeistCut.iCutPercent[iSource])
				PRINTLN("[AMEC][HEIST_LAUNCH] - Target(%)	: 	", g_HeistPlanningClient.sHeistCut.iCutPercent[index])
			#ENDIF
			
			// Validate that the values are legal (i.e. within 0 and 100).
			IF g_HeistPlanningClient.sHeistCut.iCutPercent[iSource] <= (100 - ABSI(iModifier))
				g_HeistPlanningClient.sHeistCut.iCutPercent[iSource] = g_HeistPlanningClient.sHeistCut.iCutPercent[iSource] + ABSI(iModifier)
				g_HeistPlanningClient.sHeistCut.iCutPercent[index] = g_HeistPlanningClient.sHeistCut.iCutPercent[index] - ABSI(iModifier)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - DECREASE - Post cut modifications: ")
				PRINTLN("[AMEC][HEIST_LAUNCH] - iSource(%)	: 	", g_HeistPlanningClient.sHeistCut.iCutPercent[iSource])
				PRINTLN("[AMEC][HEIST_LAUNCH] - Target(%)	: 	", g_HeistPlanningClient.sHeistCut.iCutPercent[index])
			#ENDIF
			
			IF iModifier = 0
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - Playing Highlight_Error.")
				#ENDIF
				PLAY_SOUND_FRONTEND(-1,"Highlight_Error","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - Playing Highlight_Move.")
				#ENDIF
				PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			ENDIF
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_5")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_6")
				CLEAR_HELP()
			ENDIF
			
		ENDIF
		
		IF g_HeistPlanningClient.sHeistCut.iCutPercent[iSource] >= g_sMPTUNABLES.imax_heist_cut_amount
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_6")
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - Print heist cut warning message: HEIST_NOTE_6")
				#ENDIF
				CLEAR_HELP()
				PRINT_HELP("HEIST_NOTE_6", ci_HEIST_CUT_WARN_TIMER)
			ENDIF
		ENDIF
		
	ELSE 			
		// INCREMENT TARGET, DECREASE SOURCE.
		INT iSourceCut = g_HeistPlanningClient.sHeistCut.iCutPercent[iSource]
		IF iSourceCut < ABSI(iModifier)
			iModifier = iSourceCut
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - Increase target cut. iModifier: ", iModifier)
		#ENDIF
		
		IF g_HeistPlanningClient.sHeistCut.iCutPercent[index] > g_sMPTUNABLES.imax_heist_cut_amount
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - Cannot increase target cut! Already greater cut than allowed maximum! Target cut: ", g_HeistPlanningClient.sHeistCut.iCutPercent[index], ", Maximum: ", g_sMPTUNABLES.imax_heist_cut_amount)
			#ENDIF
			
			// Because the target has somehow ended up with more than the maximum, calculate how much over they are and add it back onto the pot.
			INT iRem
			iRem = g_HeistPlanningClient.sHeistCut.iCutPercent[index] - g_sMPTUNABLES.imax_heist_cut_amount
			g_HeistPlanningClient.sHeistCut.iCutPercent[iSource] += iRem
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - Added remainder: ", iRem, ", to source total: ", (g_HeistPlanningClient.sHeistCut.iCutPercent[iSource] - iRem), " to rebalance player cuts.")
			#ENDIF
			
		ELIF g_HeistPlanningClient.sHeistCut.iCutPercent[index] = g_sMPTUNABLES.imax_heist_cut_amount
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - Cannot increase target cut, already at maximum: ", g_sMPTUNABLES.imax_heist_cut_amount)
			#ENDIF
			
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - INCREASE - Pre cut modifications: ")
				PRINTLN("[AMEC][HEIST_LAUNCH] - iSource(%)	: 	", g_HeistPlanningClient.sHeistCut.iCutPercent[iSource])
				PRINTLN("[AMEC][HEIST_LAUNCH] - Target(%)	: 	", g_HeistPlanningClient.sHeistCut.iCutPercent[index])
			#ENDIF
		
			// Validate that the values are legal (i.e. within 0 and 100).
			IF g_HeistPlanningClient.sHeistCut.iCutPercent[iSource] >= ABSI(iModifier)
				g_HeistPlanningClient.sHeistCut.iCutPercent[iSource] = g_HeistPlanningClient.sHeistCut.iCutPercent[iSource] - ABSI(iModifier)
				g_HeistPlanningClient.sHeistCut.iCutPercent[index] = g_HeistPlanningClient.sHeistCut.iCutPercent[index] + ABSI(iModifier)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - INCREASE - Post cut modifications: ")
				PRINTLN("[AMEC][HEIST_LAUNCH] - iSource(%)	: 	", g_HeistPlanningClient.sHeistCut.iCutPercent[iSource])
				PRINTLN("[AMEC][HEIST_LAUNCH] - Target(%)	: 	", g_HeistPlanningClient.sHeistCut.iCutPercent[index])
			#ENDIF
			
			IF iModifier = 0
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - Playing Highlight_Error.")
				#ENDIF
				PLAY_SOUND_FRONTEND(-1,"Highlight_Error","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - Playing Highlight_Move.")
				#ENDIF
				PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			ENDIF
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_5")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_6")
				CLEAR_HELP()
			ENDIF
			
		ENDIF
		
		IF g_HeistPlanningClient.sHeistCut.iCutPercent[index] >= g_sMPTUNABLES.imax_heist_cut_amount
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_5")
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL - Print heist cut warning message: HEIST_NOTE_5")
				#ENDIF
				CLEAR_HELP()
				PRINT_HELP("HEIST_NOTE_5", ci_HEIST_CUT_WARN_TIMER)
			ENDIF
		ENDIF

	ENDIF

ENDPROC


/// PURPOSE:
///    For the heist leader to update their cuts data in the same way that the global data is updated.
/// PARAMS:
///    index - Player index to modify.
///    iModifier - Value to modify by.
PROC PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA(INT index, INT iModifier)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - $$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
		PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Updating local cuts. Modifier: ", iModifier)
	#ENDIF
	
	INT iMinimumCut
	
	IF g_HeistPlanningClient.iPlayerOrder[index] = g_HeistSharedClient.iCurrentPropertyOwnerIndex
		iMinimumCut = g_sMPTUNABLES.iLeader_Min_Heist_Finale_Take_percentage
		PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Minimum cut percent for leader: ", iMinimumCut)
	ELSE
		iMinimumCut = g_sMPTUNABLES.iMember_Min_Heist_Finale_Take_percentage
		PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Minimum cut percent for member: ", iMinimumCut)
	ENDIF
	
	IF iModifier < 0 	
		// DECREMENT TARGET, INCREASE POT.
		INT iTargetCut = g_HeistPlanningClient.sHeistCut.iCutPercent[index]
		IF iTargetCut < ABSI(iModifier)
			iModifier = iTargetCut
		ENDIF
		
		IF g_HeistPlanningClient.sHeistCut.iCutPercent[index] < iMinimumCut
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Cannot decrease target cut! Target already less cut than allowed minimum! Target cut: ", g_HeistPlanningClient.sHeistCut.iCutPercent[index], ", Minimum: ", iMinimumCut)
			#ENDIF
			
			// Because the target has somehow ended up with less than the minimum, calculate how much under they are and add it back onto the target.
			INT iRem
			iRem = iMinimumCut - g_HeistPlanningClient.sHeistCut.iCutPercent[index]
			g_HeistPlanningClient.sHeistCut.iCutPercent[index] += iRem
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Added missing remainder: ", iRem, ", to target total: ", (g_HeistPlanningClient.sHeistCut.iCutPercent[index] - iRem), " to rebalance player cuts.")
			#ENDIF
			
		ELIF g_HeistPlanningClient.sHeistCut.iCutPercent[index] = iMinimumCut
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Cannot decrease target cut, target already at minimum: ", iMinimumCut)
			#ENDIF
			
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Decrease target cut. iModifier: ", iModifier)
			#ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - DECREASE - Pre cut modifications: ")
				PRINTLN("[AMEC][HEIST_LAUNCH] - Pot(%): 	", g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS])
				PRINTLN("[AMEC][HEIST_LAUNCH] - Target(%): 	", g_HeistPlanningClient.sHeistCut.iCutPercent[index])
			#ENDIF
			
			// Validate that the values are legal (i.e. within 0 and 100).
			IF g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] <= (100 - ABSI(iModifier))
				g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] = g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] + ABSI(iModifier)
				g_HeistPlanningClient.sHeistCut.iCutPercent[index] = g_HeistPlanningClient.sHeistCut.iCutPercent[index] - ABSI(iModifier)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - DECREASE - Post cut modifications: ")
				PRINTLN("[AMEC][HEIST_LAUNCH] - Pot(%): 	", g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS])
				PRINTLN("[AMEC][HEIST_LAUNCH] - Target(%): 	", g_HeistPlanningClient.sHeistCut.iCutPercent[index])
			#ENDIF
			
			IF iModifier = 0
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Playing Highlight_Error.")
				#ENDIF
				PLAY_SOUND_FRONTEND(-1,"Highlight_Error","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Playing Highlight_Move.")
				#ENDIF
				PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			ENDIF
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_5")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_6")
				CLEAR_HELP()
			ENDIF

		ENDIF
		
		IF g_HeistPlanningClient.sHeistCut.iCutPercent[index] <= iMinimumCut
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_6")
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Print heist cut warning message: HEIST_NOTE_6")
				#ENDIF
				CLEAR_HELP()
				PRINT_HELP("HEIST_NOTE_6", ci_HEIST_CUT_WARN_TIMER)
			ENDIF
		ENDIF
		
	ELSE 			
		// INCREMENT TARGET, DECREASE POT.
		INT iPotCut = g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS]
		IF iPotCut < ABSI(iModifier)
			iModifier = iPotCut
		ENDIF
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Increase target cut. iModifier: ", iModifier)
		#ENDIF
		
		IF g_HeistPlanningClient.sHeistCut.iCutPercent[index] > g_sMPTUNABLES.imax_heist_cut_amount
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Cannot increase target cut! Already greater cut than allowed maximum! Target cut: ", g_HeistPlanningClient.sHeistCut.iCutPercent[index], ", Maximum: ", g_sMPTUNABLES.imax_heist_cut_amount)
			#ENDIF
			
			// Because the target has somehow ended up with more than the maximum, calculate how much over they are and add it back onto the pot.
			INT iRem
			iRem = g_HeistPlanningClient.sHeistCut.iCutPercent[index] - g_sMPTUNABLES.imax_heist_cut_amount
			g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] += iRem
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Added remainder: ", iRem, ", to pot total: ", (g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] - iRem), " to rebalance player cuts.")
			#ENDIF
			
		ELIF g_HeistPlanningClient.sHeistCut.iCutPercent[index] = g_sMPTUNABLES.imax_heist_cut_amount
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Cannot increase target cut, already at maximum: ", g_sMPTUNABLES.imax_heist_cut_amount)
			#ENDIF
			
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - INCREASE - Pre cut modifications: ")
				PRINTLN("[AMEC][HEIST_LAUNCH] - Pot(%): 	", g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS])
				PRINTLN("[AMEC][HEIST_LAUNCH] - Target(%): 	", g_HeistPlanningClient.sHeistCut.iCutPercent[index])
			#ENDIF
		
			// Validate that the values are legal (i.e. within 0 and 100).
			IF g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] >= ABSI(iModifier)
				g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] = g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] - ABSI(iModifier)
				g_HeistPlanningClient.sHeistCut.iCutPercent[index] = g_HeistPlanningClient.sHeistCut.iCutPercent[index] + ABSI(iModifier)
			ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - INCREASE - Post cut modifications: ")
				PRINTLN("[AMEC][HEIST_LAUNCH] - Pot(%): 	", g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS])
				PRINTLN("[AMEC][HEIST_LAUNCH] - Target(%): 	", g_HeistPlanningClient.sHeistCut.iCutPercent[index])
			#ENDIF
			
			IF iModifier = 0
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Playing Highlight_Error.")
				#ENDIF
				PLAY_SOUND_FRONTEND(-1,"Highlight_Error","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Playing Highlight_Move.")
				#ENDIF
				PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			ENDIF
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_5")
			OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_6")
				CLEAR_HELP()
			ENDIF
			
		ENDIF
		
		IF g_HeistPlanningClient.sHeistCut.iCutPercent[index] >= g_sMPTUNABLES.imax_heist_cut_amount
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_5")
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA - Print heist cut warning message: HEIST_NOTE_5")
				#ENDIF
				CLEAR_HELP()
				PRINT_HELP("HEIST_NOTE_5", ci_HEIST_CUT_WARN_TIMER)
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC


/// PURPOSE:
///    Update the entire local copy of the player cuts array with the passed in array.
/// PARAMS:
///    iPlayerCuts - New player cuts array data.
PROC UPDATE_LOCAL_HEIST_CUTS(INT &iPlayerCuts[])

// Debugging code for validation purposes.
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_LOCAL_HEIST_CUTS - Updating client local cuts data (all).")
	#ENDIF
	
	INT index
	FOR index = 0 TO MAX_HEIST_PLAYER_SLOTS
		g_HeistPlanningClient.sHeistCut.iCutPercent[index] = iPlayerCuts[index]
	ENDFOR

ENDPROC


/// PURPOSE:
///    Store the value of the selected player's option into the cache incase the user selectes "cancel".
/// PARAMS:
///    iRow - Player order index.
///    iColumn - Option to save (role, cut, etc)
PROC SAVE_SELECTED_VARIABLE_OPTION(INT iRow, INT iColumn)

//	// Sanity check - Make sure that you can't save the leader's cut.
//	IF iRow = ci_HEIST_LEADER
//	AND	iColumn = ci_HEIST_COLUMN_CUT
//		PRINTLN("[AMEC][HEIST_LAUNCH] - SAVE_SELECTED_VARIABLE_OPTION - Saving leader's cut, something went wrong here! Exit before any damage can occur.")
//		EXIT
//	ENDIF
	IF iRow = iRow
	ENDIF
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - SAVE_SELECTED_VARIABLE_OPTION - Saving selected variable option. Player[",iRow,"] Option:",iColumn)
	#ENDIF
	
	INT index

	SWITCH iColumn
	
		CASE ci_HEIST_COLUMN_ROLE
		
			FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
				g_HeistPlanningClient.sHeistDataCache.sMiscCached.sSavedRoles[index] = GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index]
//				g_HeistPlanningClient.sHeistDataCache.sMiscCached.iSavedOutfits[index] = GlobalServerBD_HeistPlanning.iSelectedOutfit[index] // Role is now also tied with outfit.
			ENDFOR
		
			BREAK
			
		CASE ci_HEIST_COLUMN_CUT
			
			FOR index = 0 TO MAX_HEIST_PLAYER_SLOTS
				g_HeistPlanningClient.sHeistDataCache.sMiscCached.iSavedCuts[index] = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[index]
			ENDFOR
		
			BREAK
			
		CASE ci_HEIST_COLUMN_OUTFIT
			
//			FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
//				g_HeistPlanningClient.sHeistDataCache.sMiscCached.iSavedOutfits[index] = GlobalServerBD_HeistPlanning.iSelectedOutfit[index]
//			ENDFOR
			
			BREAK
	
	ENDSWITCH

ENDPROC


/// PURPOSE:
///    Completely clear a heist member's cut. This is used to quickly re-collect all of the distributed cut.
PROC CLEAR_CURRENTLY_SELECTED_MEMBERS_CUT()

	IF IS_THIS_A_TUTORIAL_HEIST()
	
		INT iModifier = (g_HeistPlanningClient.sHeistCut.iCutPercent[g_HeistPlanningClient.iSelectedHeistMember] - (HEIST_CUT_PERCENT_TOTAL - g_sMPTUNABLES.imax_heist_cut_amount)) * (-1)
		INT i, iPointer
		
		FOR i = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
			IF g_HeistPlanningClient.iPlayerOrder[i] != INVALID_HEIST_DATA
				IF g_HeistPlanningClient.iPlayerOrder[i] != g_HeistPlanningClient.iPlayerOrder[g_HeistPlanningClient.iSelectedHeistMember]
					iPointer = i
				ENDIF
			ENDIF
		ENDFOR
		
		// Update local data.
		PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL(g_HeistPlanningClient.iSelectedHeistMember, iModifier, iPointer)
				
		// Update global data.
		BROADCST_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL(g_HeistPlanningClient.iSelectedHeistMember, iModifier, g_HeistPlanningClient.sHeistCut.iCutPercent[g_HeistPlanningClient.iSelectedHeistMember], iPointer)
	
	ELSE

		INT iModifier = g_HeistPlanningClient.sHeistCut.iCutPercent[g_HeistPlanningClient.iSelectedHeistMember] * (-1)
		
		// Update local data.
		PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA(g_HeistPlanningClient.iSelectedHeistMember, iModifier)
				
		// Update global data.
		BROADCST_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON(g_HeistPlanningClient.iSelectedHeistMember, iModifier, g_HeistPlanningClient.sHeistCut.iCutPercent[g_HeistPlanningClient.iSelectedHeistMember])
	
	ENDIF

ENDPROC


/// PURPOSE:
///    Completely revoke a heist member's outfit.
//PROC CLEAR_CURRENTLY_SELECTED_MEMBERS_OUTFIT()
//	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_LAUNCH] - CLEAR_CURRENTLY_SELECTED_MEMBERS_OUTFIT - Player #",g_HeistPlanningClient.iSelectedHeistMember," - Clearing outfit.")
//	#ENDIF
//	
//	// Update local data.
//	g_HeistPlanningClient.iSelectedOutfit[g_HeistPlanningClient.iSelectedHeistMember] = INVALID_HEIST_DATA
//	
//	SET_HEIST_PLANNING_UPDATE_PREVIEW(g_HeistSharedClient.PlanningBoardIndex, ci_HEIST_PREVIEW_WINDOW_OUTFIT, "HEIST_ER_OUTFT", "")
//	
//	INT iTempNewOutfit = INVALID_HEIST_DATA
//	
//	// Update global data.
//	BROADCST_SCRIPT_EVENT_HEIST_UPDATE_OUTFIT_SINGLETON(g_HeistPlanningClient.iSelectedHeistMember, iTempNewOutfit)
//
//ENDPROC


/// PURPOSE:
///    Completely revoke a heist member's team AND outfit. This is used when the leader wants to force a member back to "unassigned".
PROC REVOKE_CURRENTLY_SELECTED_MEMBERS_TEAM()
	
	HEIST_ROLES tempRole
	
	tempRole = INT_TO_ENUM(HEIST_ROLES, INVALID_HEIST_DATA)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - REVOKE_CURRENTLY_SELECTED_MEMBER_TEAM - Player #",g_HeistPlanningClient.iSelectedHeistMember," - Revoking role.")
	#ENDIF
	
	// Update local data.
	g_HeistPlanningClient.sHeistRoles.ePlayerRoles[g_HeistPlanningClient.iSelectedHeistMember] = tempRole
	
	// Update global data.
	BROADCST_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON(g_HeistPlanningClient.iSelectedHeistMember, tempRole, g_HeistPlanningClient.iPlayerOrder[g_HeistPlanningClient.iSelectedHeistMember])
	
	CLEAN_HEIST_MAP()
	
//	// Role clearing always requires an outfit clear too.
//	CLEAR_CURRENTLY_SELECTED_MEMBERS_OUTFIT()

ENDPROC


/// PURPOSE:
///    Incrememnt or decrement a selected player's role. Used for recursive team alloc.
PROC STEP_SELECTED_HEIST_ROLE(INT &iPlayerRole, HEIST_HIGHLIGHT_TYPE eHighlightType)

	#IF IS_DEBUG_BUILD
		INT iOriginalValue = iPlayerRole
	#ENDIF

	// Calculate the progression for the selection. By clicking right, they want to increase, left decrease etc.
	IF eHighlightType = HEIST_HIGHLIGHT_SUB_LEFT
	
		IF iPlayerRole <= 0
			iPlayerRole = (g_FMMC_STRUCT.iNumberOfTeams-1)
		ELSE
			iPlayerRole--
		ENDIF
		
	ELIF eHighlightType = HEIST_HIGHLIGHT_SUB_RIGHT
		
		IF iPlayerRole < INVALID_HEIST_DATA
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - STEP_SELECTED_HEIST_ROLE - WARNING! Selected player's role was INVALID, moving to UNASSIGNED as a precaution!")
			#ENDIF
			iPlayerRole = INVALID_HEIST_DATA
		ENDIF
		
		IF iPlayerRole >= (g_FMMC_STRUCT.iNumberOfTeams-1)
			iPlayerRole = 0
		ELSE
			iPlayerRole++
		ENDIF
		
	ENDIF 
	
	PRINTLN("[AMEC][HEIST_LAUNCH] - STEP_SELECTED_HEIST_ROLE - Stepping role value. Original: ", iOriginalValue, ", New: ", iPlayerRole)

ENDPROC


/// PURPOSE:
///    Get the total number of team members for the provided team.
/// PARAMS:
///    iTeam - Team to check.
///    iTeamShift - OPTIONAL - Change this team's total by -1 (useful for proposed changes).
/// RETURNS:
///    INT amount.
FUNC INT GET_PREVIEW_NUM_PLAYERS_IN_TEAM(INT iTeam, INT iTeamShift = INVALID_HEIST_DATA)
	
	INT iTeamTotals[FMMC_MAX_TEAMS]
	GET_ROLE_TOTAL_FROM_TEAMS(iTeamTotals)
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - GET_PREVIEW_NUM_PLAYERS_IN_TEAM - Team[",iTeam,"] CurrTotal: ", iTeamTotals[iTeam], ", PreviewTotal: ", iTeamTotals[iTeam]+1)
	#ENDIF
	
	IF iTeamShift > INVALID_HEIST_DATA
	AND iTeamShift < FMMC_MAX_TEAMS
		PRINTLN("[AMEC][HEIST_LAUNCH] - GET_PREVIEW_NUM_PLAYERS_IN_TEAM - iTeamShift is VALID, value: ", iTeamShift, ", shifting this team back one slot. Original: ",iTeamTotals[iTeamShift],", Shifted: ",(iTeamTotals[iTeamShift] - 1))
		iTeamTotals[iTeamShift] = iTeamTotals[iTeamShift] - 1
	ENDIF
	
	RETURN iTeamTotals[iTeam] + 1
	
ENDFUNC


/// PURPOSE:
///    Get the total number of team members for the provided team (LEADER ONLY).
/// PARAMS:
///    iTeam - Team to check.
/// RETURNS:
///    INT amount.
FUNC INT GET_PREVIEW_NUM_PLAYERS_IN_TEAM_LEADER_ONLY(INT iTeam, BOOL bUseLocalDataOnly = FALSE)
	
	INT iTeamTotals[FMMC_MAX_TEAMS]
	INT index
	
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF bUseLocalDataOnly
			IF g_HeistPlanningClient.iPlayerOrder[index] != -1
				SWITCH ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index])
					CASE 0 iTeamTotals[0]++ BREAK
					CASE 1 iTeamTotals[1]++ BREAK
					CASE 2 iTeamTotals[2]++ BREAK
					CASE 3 iTeamTotals[3]++ BREAK
				ENDSWITCH
			ENDIF
		ELSE
			IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] != -1
				IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
					SWITCH ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index])
						CASE 0 iTeamTotals[0]++ BREAK
						CASE 1 iTeamTotals[1]++ BREAK
						CASE 2 iTeamTotals[2]++ BREAK
						CASE 3 iTeamTotals[3]++ BREAK
					ENDSWITCH
				ENDIF
			ENDIF	
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - GET_PREVIEW_NUM_PLAYERS_IN_TEAM_LEADER_ONLY - Team[",iTeam,"] CurrTotal: ", iTeamTotals[iTeam], ", PreviewTotal: ", iTeamTotals[iTeam]+1, ", bUseLocalDataOnly: ",bUseLocalDataOnly)
	#ENDIF
	
	RETURN iTeamTotals[iTeam]
	
ENDFUNC


/// PURPOSE:
///    Used for monitoring when the heist planning participant count drops below the minimum.
/// RETURNS:
///    
FUNC INT GET_NUMBER_OF_ACTIVE_HEIST_MEMBERS()

	INT iTotalMembers, index

	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
			iTotalMembers++
		ENDIF
	ENDFOR
	
	RETURN iTotalMembers

ENDFUNC


/// PURPOSE:
///    Check the remaining connected members of the heist board to see if the leader is among them.
/// RETURNS:
///    TRUE is no leader is found.
FUNC BOOL HAS_HEIST_LEADER_LEFT()

	// If i'm the leader, don't check if i exist.
	IF AM_I_LEADER_OF_HEIST()
		RETURN FALSE
	ENDIF

	INT index

	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
			IF IS_PLAYER_LEADER_OF_HEIST(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index]))
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR

	PRINTLN("[AMEC][HEIST_LAUNCH] - HAS_HEIST_LEADER_LEFT - Leader was not found in iPlayerOrder.")

	RETURN TRUE

ENDFUNC


FUNC INT GET_HEIST_MEMBER_MEDAL_DATA_FOR_MISSION(INT iMissionIndex, INT iPlayerNameHash)

	INT iRootContentID = GET_STRAND_ROOT_CONTENT_ID_HASH()
	IF iRootContentID = 0
		iRootContentID = g_FMMC_STRUCT.iRootContentIdHash
	ENDIF

	// TODO: Remove this hardcoded in favour of dynamic stuff.
	IF iMissionIndex >= TEMP_GET_CURRENT_HEIST_STRAND_SETUP_MSSION_COUNT(iRootContentID) 
		RETURN INVALID_HEIST_DATA
	ENDIF

	IF iPlayerNameHash = g_HeistPlanningClient.sMedalData[iMissionIndex].iPlayerOne
		RETURN ci_HEIST_MEDAL_PLATINUM
	ELIF iPlayerNameHash = g_HeistPlanningClient.sMedalData[iMissionIndex].iPlayerTwo
		RETURN ci_HEIST_MEDAL_GOLD
	ELIF iPlayerNameHash = g_HeistPlanningClient.sMedalData[iMissionIndex].iPlayerThree
		RETURN ci_HEIST_MEDAL_SILVER
	ELIF iPlayerNameHash = g_HeistPlanningClient.sMedalData[iMissionIndex].iPlayerFour
		RETURN ci_HEIST_MEDAL_BRONZE
	ENDIF
	
	RETURN ci_HEIST_MEDAL_NONE
	
ENDFUNC


///// PURPOSE:
/////    Get the first available outfit.
//FUNC INT GET_START_OF_OUTFIT_LIST(INT iTeam)
//	INT iLoop
//	FOR iLoop = 0 TO ENUM_TO_INT(OUTFIT_MAX_AMOUNT)-1
//		// Is the loop index a set bit in the total outfit selection.
//		IF IS_BIT_SET(g_FMMC_STRUCT.biOutFitsAvailableBitset[iTeam][iLoop/32], iLoop%32)
//			PRINTLN("[AMEC][HEIST_LAUNCH] - GET_START_OF_OUTFIT_LIST - Valid outfit list starts at: ", iLoop, ". iTeam: ",iTeam)
//			RETURN iLoop
//		ENDIF
//	ENDFOR
//	
//	PRINTLN("[AMEC][HEIST_LAUNCH] - GET_START_OF_OUTFIT_LIST - CRITICAL ERROR! Could not find any valid outfits. Returning -1. iTeam: ",iTeam)
//	RETURN INVALID_HEIST_DATA
//ENDFUNC
//
//
///// PURPOSE:
/////    Get the far right end of the available outfits.
//FUNC INT GET_END_OF_OUTFIT_LIST(INT iTeam)
//	INT iLoop, iCount
//	FOR iLoop = 0 TO ENUM_TO_INT(OUTFIT_MAX_AMOUNT)-1
//		// Is the loop index a set bit in the total outfit selection.
//		IF IS_BIT_SET(g_FMMC_STRUCT.biOutFitsAvailableBitset[iTeam][iLoop/32], iLoop%32)
//			iCount = iLoop
//		ENDIF
//	ENDFOR
//	
//	PRINTLN("[AMEC][HEIST_LAUNCH] - GET_END_OF_OUTFIT_LIST - Valid outfit list ends at: ", iCount, ". iTeam: ",iTeam)
//	RETURN iCount
//ENDFUNC
//
//
///// PURPOSE:
/////    Get the previous valid outfit (when pressing left).
//FUNC INT GET_PREVIOUS_AVAILABLE_OUTFIT(INT iCurrentOutfit, INT iTeam)
//	INT iLoop
//	INT iPrevious = -1
//	FOR iLoop = 0 TO ENUM_TO_INT(OUTFIT_MAX_AMOUNT)-1
//		IF IS_BIT_SET(g_FMMC_STRUCT.biOutFitsAvailableBitset[iTeam][iLoop/32], iLoop%32)
//		
//			IF iLoop < iCurrentOutfit
//				iPrevious = iLoop
//			ENDIF
//		ENDIF
//	ENDFOR
//	
//	PRINTLN("[AMEC][HEIST_LAUNCH] - GET_PREVIOUS_AVAILABLE_OUTFIT - iCurrentOutfit: ",iCurrentOutfit,", iPrevious: ",iPrevious,", iTeam: ", iTeam)
//	RETURN iPrevious
//ENDFUNC
//
//
///// PURPOSE:
/////    Get the next valid outfit (when pressing left).
//FUNC INT GET_NEXT_AVAILABLE_OUTFIT(INT iCurrentOutfit, INT iTeam)
//	INT iLoop
//	FOR iLoop = 0 TO ENUM_TO_INT(OUTFIT_MAX_AMOUNT)-1
//		IF IS_BIT_SET(g_FMMC_STRUCT.biOutFitsAvailableBitset[iTeam][iLoop/32], iLoop%32)
//			IF iLoop > iCurrentOutfit
//				PRINTLN("[AMEC][HEIST_LAUNCH] - GET_NEXT_AVAILABLE_OUTFIT - iCurrentOutfit: ",iCurrentOutfit,", iLoop(Next Outfit): ",iLoop,", iTeam: ", iTeam)
//				RETURN iLoop
//			ENDIF
//		ENDIF
//	ENDFOR
//	
//	PRINTLN("[AMEC][HEIST_LAUNCH] - GET_NEXT_AVAILABLE_OUTFIT - ERROR! Couldn't find next outfit, returning -1.")
//	RETURN -1  //returns default outfit if error
//ENDFUNC
//
//
///// PURPOSE:
/////    Get the first available outfit for the passed in team.
///// PARAMS:
/////    iTeam - Team number.
//FUNC INT GET_DEFAULT_OUTFIT_FOR_TEAM(INT iTeam)
//
//	IF iTeam < 0
//	OR iTeam > (FMMC_MAX_TEAMS-1)
//		PRINTLN("[AMEC][HEIST_LAUNCH] - GET_DEFAULT_OUTFIT_FOR_TEAM - ERROR! iTeam is INVALID. Value: ", iTeam)
//		RETURN INVALID_HEIST_DATA
//	ENDIF
//	
//	INT iDefaultOutfit = g_FMMC_STRUCT.iOutfitDefault[iTeam]
//	
//	IF iDefaultOutfit < 0 
//		PRINTLN("[AMEC][HEIST_LAUNCH] - GET_DEFAULT_OUTFIT_FOR_TEAM - ERROR! iDefaultOutfit is INVALID. Value: ", iDefaultOutfit)
//		RETURN INVALID_HEIST_DATA
//	ENDIF
//	
//	PRINTLN("[AMEC][HEIST_LAUNCH] - GET_DEFAULT_OUTFIT_FOR_TEAM - Locating iDefaultOutfit in bitsets. Value: ", iDefaultOutfit)
//
//	RETURN iDefaultOutfit
//	
//ENDFUNC
//
//
///// PURPOSE:
/////    Get the total number of outfits available for the passed in team.
///// PARAMS:
/////    iTeam - Team index.
//FUNC INT GET_TOTAL_OUTFITS_FOR_TEAM(INT iTeam)
//
//	// Validate our team. If not within the range, exit
//	IF iTeam < 0
//	OR iTeam >= FMMC_MAX_TEAMS
//		PRINTLN("[AMEC][HEIST_LAUNCH] - GET_TOTAL_OUTFITS_FOR_TEAM - Invalid team iTeam: ", iTeam)
//		RETURN 0
//	ENDIF
//
//	INT iLoop, iTotal
//	FOR iLoop = 0 TO ENUM_TO_INT(OUTFIT_MAX_AMOUNT)-1
//		IF IS_BIT_SET(g_FMMC_STRUCT.biOutFitsAvailableBitset[iTeam][iLoop/32], iLoop%32)
//			iTotal++
//		ENDIF
//	ENDFOR
//	PRINTLN("[AMEC][HEIST_LAUNCH] - GET_TOTAL_OUTFITS_FOR_TEAM - iTeam: ",iTeam,", iTotal: ",iTotal)
//	RETURN iTotal
//ENDFUNC


/// PURPOSE:
///    LEADER ONLY - Automatically allocate out free roles to connected heist members. Do not call every frame.
PROC AUTO_ASSIGN_ROLES_TO_HEIST_MEMBERS(BOOL bUseLocalDataOnly = FALSE)

	IF NOT IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
		EXIT
	ENDIF
	
	DEBUG_PRINTCALLSTACK()
	PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_ROLES_TO_HEIST_MEMBERS - Automatically assigning free heist roles to connected members.")

	HEIST_ROLES tempHeistRoles[MAX_HEIST_PLAYER_SLOTS]
	INT index
				
	// Make sure that the local ePlayerRoles data is correct.
	IF NOT bUseLocalDataOnly
		PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_ROLES_TO_HEIST_MEMBERS - Auto assigning teams using global and local data.")
	
		FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
			g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index]
		ENDFOR
			
		FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
			IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] != INVALID_HEIST_DATA
				IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
					IF g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = HEIST_ROLE_UNASSIGNED
						INT iPlayerRoleEnumAsInt, iOriginalRole, i
						BOOL bFoundRole = FALSE
						HEIST_ROLES tempRole
						
						iPlayerRoleEnumAsInt = ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index])
						iOriginalRole = iPlayerRoleEnumAsInt
						IF iOriginalRole = iOriginalRole
						ENDIF
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_ROLES_TO_HEIST_MEMBERS - Selected player [",index,", ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index])),"] role enum as int: ", iPlayerRoleEnumAsInt)
						#ENDIF
					
						// Step the role in the selected direction. This is an indescriminate step (as in, it just steps without doing any min/max checks).
						STEP_SELECTED_HEIST_ROLE(iPlayerRoleEnumAsInt, HEIST_HIGHLIGHT_SUB_RIGHT)
						g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = INT_TO_ENUM(HEIST_ROLES, iPlayerRoleEnumAsInt)
					
						// If the stepped role given originally is already at maximum value, we need to attempt to find another.
						FOR i = 0 TO (FMMC_MAX_TEAMS-1)
							IF GET_PREVIEW_NUM_PLAYERS_IN_TEAM_LEADER_ONLY(iPlayerRoleEnumAsInt) > g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iPlayerRoleEnumAsInt]
								#IF IS_DEBUG_BUILD
									PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_ROLES_TO_HEIST_MEMBERS - WARNING! Stepped role is already at maximum! Stepping again...")
								#ENDIF
								STEP_SELECTED_HEIST_ROLE(iPlayerRoleEnumAsInt, HEIST_HIGHLIGHT_SUB_RIGHT)
								
								g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = INT_TO_ENUM(HEIST_ROLES, iPlayerRoleEnumAsInt)
							ELSE
								bFoundRole = TRUE
							ENDIF
						ENDFOR

						IF NOT bFoundRole
							PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_ROLES_TO_HEIST_MEMBERS - WARNING! Could not locate a free slot in any teams, keeping current role: ", iOriginalRole)
							g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = INT_TO_ENUM(HEIST_ROLES, iOriginalRole)
						ELSE
							tempRole = INT_TO_ENUM(HEIST_ROLES, iPlayerRoleEnumAsInt)
							tempHeistRoles[index] = tempRole // Save the players new role ready for the all-in-one update at the end of the loop.
							PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_ROLES_TO_HEIST_MEMBERS - Player #",index," - New role: ", tempRole)
						ENDIF
					ELSE
						tempHeistRoles[index] = g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index]
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_ROLES_TO_HEIST_MEMBERS - Skipping player[",index,"] as they are already in team: ", ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index]))
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_ROLES_TO_HEIST_MEMBERS - Auto assigning teams using local data only.")	
		FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
			IF g_HeistPlanningClient.iPlayerOrder[index] != INVALID_HEIST_DATA
				IF g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = HEIST_ROLE_UNASSIGNED
					INT iPlayerRoleEnumAsInt, iOriginalRole, i
					BOOL bFoundRole = FALSE
					HEIST_ROLES tempRole
					
					iPlayerRoleEnumAsInt = ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index])
					iOriginalRole = iPlayerRoleEnumAsInt
				
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_ROLES_TO_HEIST_MEMBERS - LOCAL - Selected player [",index,", ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(g_HeistPlanningClient.iPlayerOrder[index])),"] role enum as int: ", iPlayerRoleEnumAsInt)
					#ENDIF
				
					// Step the role in the selected direction. This is an indescriminate step (as in, it just steps without doing any min/max checks).
					STEP_SELECTED_HEIST_ROLE(iPlayerRoleEnumAsInt, HEIST_HIGHLIGHT_SUB_RIGHT)
					g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = INT_TO_ENUM(HEIST_ROLES, iPlayerRoleEnumAsInt)
				
					// If the stepped role given originally is already at maximum value, we need to attempt to find another.
					FOR i = 0 TO (FMMC_MAX_TEAMS-1)
						IF GET_PREVIEW_NUM_PLAYERS_IN_TEAM_LEADER_ONLY(iPlayerRoleEnumAsInt, TRUE) > g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iPlayerRoleEnumAsInt]
							#IF IS_DEBUG_BUILD
								PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_ROLES_TO_HEIST_MEMBERS - LOCAL - WARNING! Stepped role is already at maximum! Stepping again...")
							#ENDIF
							STEP_SELECTED_HEIST_ROLE(iPlayerRoleEnumAsInt, HEIST_HIGHLIGHT_SUB_RIGHT)
							
							g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = INT_TO_ENUM(HEIST_ROLES, iPlayerRoleEnumAsInt)
						ELSE
							bFoundRole = TRUE
						ENDIF
					ENDFOR

					IF NOT bFoundRole
						PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_ROLES_TO_HEIST_MEMBERS - LOCAL - WARNING! Could not locate a free slot in any teams, keeping current role: ", iOriginalRole)
						g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = INT_TO_ENUM(HEIST_ROLES, iOriginalRole)
					ELSE
						tempRole = INT_TO_ENUM(HEIST_ROLES, iPlayerRoleEnumAsInt)
						tempHeistRoles[index] = tempRole // Save the players new role ready for the all-in-one update at the end of the loop.
						PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_ROLES_TO_HEIST_MEMBERS - LOCAL - Player #",index," - New role: ", tempRole)
					ENDIF
				ELSE
					tempHeistRoles[index] = g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index]
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_ROLES_TO_HEIST_MEMBERS - LOCAL - Skipping player[",index,"] as they are already in team: ", ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index]))
					#ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = tempHeistRoles[index]
	ENDFOR	

	// Now that the roles array has been built, send it in one go to the host.
	BROADCST_SCRIPT_EVENT_HEIST_UPDATE_ROLES(tempHeistRoles)

ENDPROC


/// PURPOSE:
///    LEADER ONLY - Automatically allocate out free roles to connected heist members. Do not call every frame.
//PROC AUTO_ASSIGN_OUTFITS_TO_HEIST_MEMBERS()
//
//	IF NOT IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
//		EXIT
//	ENDIF
//	
//	DEBUG_PRINTCALLSTACK()
//	PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_OUTFITS_TO_HEIST_MEMBERS - Automatically assigning default outfits to assigned roles using local data only.")
//	
//	INT index
//				
//	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
//		IF g_HeistPlanningClient.iPlayerOrder[index] != INVALID_HEIST_DATA
//			IF g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] != HEIST_ROLE_UNASSIGNED
//				
//				INT iPlayerOutfitEnumAsInt, iPlayerTeam
//		
//				// Get the int value of the selected player's current role.
//				iPlayerOutfitEnumAsInt = g_HeistPlanningClient.iSelectedOutfit[index]
//				iPlayerTeam = ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index])
//				
//				#IF IS_DEBUG_BUILD
//					PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_OUTFITS_TO_HEIST_MEMBERS - Selected player [",index,", ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(g_HeistPlanningClient.iPlayerOrder[index])),"] Old Outfit: ", iPlayerOutfitEnumAsInt, " on team: ",iPlayerTeam)
//				#ENDIF
//				
//				iPlayerOutfitEnumAsInt = GET_DEFAULT_OUTFIT_FOR_TEAM(iPlayerTeam)
//				
//				#IF IS_DEBUG_BUILD
//					PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_OUTFITS_TO_HEIST_MEMBERS - Player #",index," - New outfit(default): ", iPlayerOutfitEnumAsInt)
//				#ENDIF
//
//				// Update local data to reduce input lag for the heist leader.
//				//g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = tempRole
//				g_HeistPlanningClient.iSelectedOutfit[index] = iPlayerOutfitEnumAsInt
//				
//				// Update global data.
//				BROADCST_SCRIPT_EVENT_HEIST_UPDATE_OUTFIT_SINGLETON(index, iPlayerOutfitEnumAsInt)
//				
//			ELSE
//				#IF IS_DEBUG_BUILD
//					PRINTLN("[AMEC][HEIST_LAUNCH] - AUTO_ASSIGN_OUTFITS_TO_HEIST_MEMBERS - ERROR! Cannot assign default outfit to player [",index,", ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(g_HeistPlanningClient.iPlayerOrder[index])),"] due to INVALID team, value: ", ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index]))
//				#ENDIF
//			ENDIF
//		ENDIF
//	ENDFOR
//
//ENDPROC



/// PURPOSE:
///    Maintain the count-down timer for auto-launch.
PROC UPDATE_HEIST_COUNTDOWN_TIMER(SCALEFORM_INSTRUCTIONAL_BUTTONS &sScaleformStruct)

	UNUSED_PARAMETER(sScaleformStruct)

	IF GlobalServerBD_HeistPlanning.tsStartPlanningTime = NULL
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_COUNTDOWN_TIMER - Start timestamp is NULL, waiting for global event to set timestamp.")
		#ENDIF
		EXIT
	ENDIF

	INT iTimerUp, iTimerDown, iTimerDownSec
	
	IF NOT HAS_NET_TIMER_STARTED(g_HeistPlanningClient.sLaunchCountdown)
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_COUNTDOWN_TIMER - Launch timer not yet started, forcing start timer.")
		#ENDIF
		START_NET_TIMER(g_HeistPlanningClient.sLaunchCountdown)
	ENDIF

	//If there is a count down timer then calculate it as seconds.
	IF g_HeistPlanningClient.sLaunchCountdown.Timer != NULL
        //Get the timer as seconds
        iTimerUp = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), GlobalServerBD_HeistPlanning.tsStartPlanningTime))
		
		IF iTimerUp >= GlobalServerBD_HeistPlanning.iLaunchTime
			iTimerUp = INVALID_HEIST_DATA
		ENDIF
		
        // Count downwards 
        iTimerDown = ABSI(GlobalServerBD_HeistPlanning.iLaunchTime - iTimerUp)
		iTimerDownSec = iTimerDown/1000
		
	ELSE
	  	iTimerUp = INVALID_HEIST_DATA
	ENDIF
	
	
	IF iTimerUp > INVALID_HEIST_DATA
		IF g_HeistPlanningClient.iStoredCountDownTime != iTimerDownSec
			g_HeistPlanningClient.iStoredCountDownTime = iTimerDownSec
			g_HeistPlanningClient.bUpdateInstButtons = TRUE
			PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_COUNTDOWN_TIMER - Timer changed, update scaleform buttons. Value: ", iTimerDownSec)
		ENDIF
	ELSE
		IF NOT g_HeistPlanningClient.bLaunchTimerExpired
		
			g_HeistPlanningClient.bLaunchTimerExpired = TRUE
			
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHeistReadyState = ENUM_TO_INT(STATUS_READY)
			
			#IF IS_DEBUG_BUILD // UPDATE_HEIST_COUNTDOWN_TIMER - Timer expired, forcing ready state.
				PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_COUNTDOWN_TIMER - !!!!! LAUNCH TIMER EXPIRED !!!!!")
			#ENDIF
			
			IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
			
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_COUNTDOWN_TIMER - I am leader, move to timer elapsed auto-exec...")
				#ENDIF
			
				// When the timer expires, every member sets their ready status. The leader must also automatically assign teams
				// to all of the members in the heist. Make sure that the leader hasn't unassigned everyone's roles.
				AUTO_ASSIGN_ROLES_TO_HEIST_MEMBERS()
				
//				AUTO_ASSIGN_OUTFITS_TO_HEIST_MEMBERS()

			ENDIF
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE:
///    Restore the saved variable for the selected option. In other words, when the heist leader selectes the "cancel" option
///    when changing a player's details, call this method to restore the data to the previous value.
/// PARAMS:
///    iRow - Player order index.
///    iColumn - Option to restore.
PROC RESTORE_SAVED_HEIST_CONFIG_OPTION(INT iRow, INT iColumn)

//	// Sanity check - Make sure that you can't change the leader's cut.
//	IF iRow = ci_HEIST_LEADER
//	AND	iColumn = ci_HEIST_COLUMN_CUT
//		PRINTLN("[AMEC][HEIST_LAUNCH] - RESTORE_SAVED_HEIST_CONFIG_OPTION - Loading leader's cut, something went wrong here! Exit before any damage can occur.")
//		EXIT
//	ENDIF
	
	INT index
	IF iRow = iRow
	ENDIF
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - RESTORE_SAVED_HEIST_CONFIG_OPTION - Loading selected variable option. Player[",iRow,"] Option:",iColumn)
	#ENDIF

	SWITCH iColumn
	
		CASE ci_HEIST_COLUMN_ROLE
				
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - RESTORE_SAVED_HEIST_CONFIG_OPTION - Restoring previous player roles and outfit arrays.")
			#ENDIF
			
			FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
				g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = g_HeistPlanningClient.sHeistDataCache.sMiscCached.sSavedRoles[index]
			ENDFOR
			
//			FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
//				g_HeistPlanningClient.iSelectedOutfit[index] = g_HeistPlanningClient.sHeistDataCache.sMiscCached.iSavedOutfits[index]
//			ENDFOR

			// Update global data.
			BROADCST_SCRIPT_EVENT_HEIST_UPDATE_ROLES(g_HeistPlanningClient.sHeistRoles.ePlayerRoles)
//			BROADCST_SCRIPT_EVENT_HEIST_UPDATE_OUTFITS(g_HeistPlanningClient.iSelectedOutfit)

			BREAK
			
		CASE ci_HEIST_COLUMN_CUT
		
			// Slight deviation from the norm here. Normally when a cut is updated, a modifier value is sent to the host; the host then uses
			// this modifier on the target and himself to achieve the desired changes. This varies here, as i am restoring the entire array to how it was before.
			// This is the cut down on the issues caused by the singleton modifier update method, and a part of the process of removing protential
			// exploits with the cut percentages.
			
			// Update local data.
			UPDATE_LOCAL_HEIST_CUTS(g_HeistPlanningClient.sHeistDataCache.sMiscCached.iSavedCuts)
			
			// Update global data.
			BROADCST_SCRIPT_EVENT_HEIST_UPDATE_CUTS(g_HeistPlanningClient.sHeistDataCache.sMiscCached.iSavedCuts)

			BREAK
			
		CASE ci_HEIST_COLUMN_OUTFIT
		
//			// Simple restore of the original saved outfit array.
//			
//			#IF IS_DEBUG_BUILD
//				PRINTLN("[AMEC][HEIST_LAUNCH] - RESTORE_SAVED_HEIST_CONFIG_OPTION - Player #",iRow," - Restoring outfit: ", g_HeistPlanningClient.sHeistDataCache.sMiscCached.iSavedOutfits[iRow])
//			#ENDIF
//			
//			FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
//				g_HeistPlanningClient.iSelectedOutfit[index] = g_HeistPlanningClient.sHeistDataCache.sMiscCached.iSavedOutfits[index]
//			ENDFOR
//			
//			// Update global data.
//			BROADCST_SCRIPT_EVENT_HEIST_UPDATE_OUTFITS(g_HeistPlanningClient.iSelectedOutfit)
//		
			BREAK
	
	ENDSWITCH

ENDPROC


/// PURPOSE:
///    Monitor the heist planning board main data for corruption and missed events. If the values are off, this will also lock them down to something legal.
PROC CHECK_ALL_HEIST_PLANNING_VALUES_ARE_LEGAL()

	// Only the heist leader should have the power to change all members' data.
	IF NOT IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
		EXIT
	ENDIF
	
	// Disable sanity checking while the heist leader is in configuration mode.
	IF g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
		EXIT
	ENDIF
	
	// Initialise the sync variables to nothing. This is so that no accidental "0" value updates get through.
	INT iHeistTotalPay 		= INVALID_HEIST_DATA
	INT iHeistTotalTeams	= INVALID_HEIST_DATA
	INT iHeistTotalRules	= INVALID_HEIST_DATA
	INT iPlayerOrder[MAX_HEIST_PLAYER_SLOTS]
	INT index = 0
	
	BOOL bHaveUpdatedArray 	= FALSE
	
	SET_ALL_VALUES_IN_ARRAY_INT(iPlayerOrder, INVALID_HEIST_DATA) 
	
	// ***** Local globals ***** \\

	// Total Pay LOCAL.
	IF g_HeistPlanningClient.iHeistTotalPay > g_sMPTunables.iheist_planning_sanity_ceiling
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_ALL_HEIST_PLANNING_VALUES_ARE_LEGAL - WARNING! iHeistTotalPay (",g_HeistPlanningClient.iHeistTotalPay,") > MAX (",g_sMPTunables.iheist_planning_sanity_ceiling,") ")
		#ENDIF
		
		g_HeistPlanningClient.iHeistTotalPay = g_sMPTunables.iheist_planning_sanity_ceiling 
		
	ELIF g_HeistPlanningClient.iHeistTotalPay < 0
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_ALL_HEIST_PLANNING_VALUES_ARE_LEGAL - WARNING! iHeistTotalPay (",g_HeistPlanningClient.iHeistTotalPay,") < 0")
		#ENDIF
	
		g_HeistPlanningClient.iHeistTotalPay = 0 
		
	ENDIF
	
	// Total Teams LOCAL.
	IF g_HeistPlanningClient.iTotalTeams > MAX_HEIST_TEAMS
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_ALL_HEIST_PLANNING_VALUES_ARE_LEGAL - WARNING! iTotalTeams (",g_HeistPlanningClient.iTotalTeams,") > MAX (",MAX_HEIST_TEAMS,") ")
		#ENDIF
	
		g_HeistPlanningClient.iTotalTeams = MAX_HEIST_TEAMS
		
	ELIF g_HeistPlanningClient.iTotalTeams < 0 
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_ALL_HEIST_PLANNING_VALUES_ARE_LEGAL - WARNING! iTotalTeams (",g_HeistPlanningClient.iTotalTeams,") < 0")
		#ENDIF
	
		g_HeistPlanningClient.iTotalTeams = 0 
		
	ENDIF
	
	// Total Rules/Tasks LOCAL.
	IF g_HeistPlanningClient.iTotalRules > MAX_HEIST_TASKS
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_ALL_HEIST_PLANNING_VALUES_ARE_LEGAL - WARNING! iTotalRules (",g_HeistPlanningClient.iTotalRules,") > MAX (",MAX_HEIST_TASKS,") ")
		#ENDIF
	
		g_HeistPlanningClient.iTotalRules = MAX_HEIST_TASKS
		
	ELIF g_HeistPlanningClient.iTotalRules < 0
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_ALL_HEIST_PLANNING_VALUES_ARE_LEGAL - WARNING! iTotalRules (",g_HeistPlanningClient.iTotalRules,") < 0")
		#ENDIF
	
		g_HeistPlanningClient.iTotalRules = 0 
		
	ENDIF
	
	// Sync iPlayerOrder GLOBAL and CLIENT.
	IF NOT ARE_TWO_INT_ARRAYS_IDENTICAL(GlobalServerBD_HeistPlanning.iPlayerOrder, g_HeistPlanningClient.iPlayerOrder)
	//IF GlobalServerBD_HeistPlanning.iPlayerOrder[0] != g_HeistPlanningClient.iPlayerOrder[0]
		// Big problem - the iPlayerOrder array is corrupt (the first entry is the leader). This should seldom happen, if ever. 
		// We need to fix this ASAP or other parts of the heist planning board will start to crumble. If the player order changes,
		// this might cause more issues than it fixes....
		
		PLAYER_INDEX thisPlayer = INVALID_PLAYER_INDEX()
		INT iPointer = 0 // This is the array tracker - it has to be different to the index because this manages the order of who's in the heist.

		FOR index = 0 TO (NUM_NETWORK_PLAYERS-1)
		    thisPlayer = INT_TO_PLAYERINDEX(index)
			IF NOT IS_THIS_PLAYER_SCTV_OR_JOINING_AS_SCTV(thisPlayer)
			    IF (IS_NET_PLAYER_OK(thisPlayer, FALSE, FALSE))
					IF thisPlayer <> INVALID_PLAYER_INDEX()
						IF iPointer > (MAX_HEIST_PLAYER_SLOTS-1)
							SCRIPT_ASSERT("[AMEC][HEIST_LAUNCH] - CHECK_ALL_HEIST_PLANNING_VALUES_ARE_LEGAL - ERROR! Too many players exist in the planning lobby! Tell Alastair.")
						ELSE
							#IF IS_DEBUG_BUILD
								PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_ALL_HEIST_PLANNING_VALUES_ARE_LEGAL - Re-synchronising player: ", GET_PLAYER_NAME(thisPlayer), " ID: ", NATIVE_TO_INT(thisPlayer), " IN SLOT: ", iPointer)
							#ENDIF
							
							// Also sync the client side here as we know these values are good 'n fresh (this also saves cycles later).
							g_HeistPlanningClient.iPlayerOrder[iPointer] = NATIVE_TO_INT(thisPlayer)
							
					        iPlayerOrder[iPointer] = NATIVE_TO_INT(thisPlayer)
						ENDIF
				    ENDIF
					iPointer++ // Player added to array, manually increment the pointer.
				ENDIF
			ENDIF
		ENDFOR
		
		bHaveUpdatedArray = TRUE
		
	ENDIF
	
	// Total Pay GLOBAL.
	IF GlobalServerBD_HeistPlanning.iHeistTotalPay != g_HeistPlanningClient.iHeistTotalPay
		iHeistTotalPay = g_HeistPlanningClient.iHeistTotalPay
	ENDIF
	
	// Total Teams GLOBAL.
	IF GlobalServerBD_HeistPlanning.iHeistTotalTeams != g_HeistPlanningClient.iTotalTeams
		iHeistTotalTeams = g_HeistPlanningClient.iTotalTeams
	ENDIF
	
	// Total Rules/Tasks GLOBAL.
	IF GlobalServerBD_HeistPlanning.iHeistTotalRules != g_HeistPlanningClient.iTotalRules
		iHeistTotalRules = g_HeistPlanningClient.iTotalRules
	ENDIF
		
	// Now that all of the required local variables have been set, bradcast them to all clients. Only broadcast a resync if
	// the values actually change.
	IF iHeistTotalPay 	!= INVALID_HEIST_DATA
	OR iHeistTotalTeams != INVALID_HEIST_DATA
	OR iHeistTotalRules != INVALID_HEIST_DATA
	OR bHaveUpdatedArray
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_ALL_HEIST_PLANNING_VALUES_ARE_LEGAL - Broadcast new server data:")
			PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_ALL_HEIST_PLANNING_VALUES_ARE_LEGAL - ... TotalPay:	", iHeistTotalPay)
			PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_ALL_HEIST_PLANNING_VALUES_ARE_LEGAL - ... TotalTeams:	", iHeistTotalTeams)
			PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_ALL_HEIST_PLANNING_VALUES_ARE_LEGAL - ... TotalRules:	", iHeistTotalRules)
		#ENDIF
		
		BROADCST_SCRIPT_EVENT_HEIST_SYNC_GLOBAL_DATA(iHeistTotalPay, iHeistTotalRules, iHeistTotalTeams, iPlayerOrder)
	ENDIF
	
	
	// Make sure that the total cut percent never exceeds 100%.
	
	INT iCumCuts
	
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		iCumCuts += GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[index]
	ENDFOR
	
	IF iCumCuts > HEIST_CUT_PERCENT_TOTAL
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_ALL_HEIST_PLANNING_VALUES_ARE_LEGAL - ERROR! Player cuts exceed maximum total! Value: ",iCumCuts,". Maximum: ",HEIST_CUT_PERCENT_TOTAL)
		#ENDIF
		
		FOR index = 0 TO MAX_HEIST_PLAYER_SLOTS
			g_HeistPlanningClient.sHeistCut.iCutPercent[index] = 0
		ENDFOR
		
		g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] = HEIST_CUT_PERCENT_TOTAL
		
		BROADCST_SCRIPT_EVENT_HEIST_UPDATE_CUTS(g_HeistPlanningClient.sHeistCut.iCutPercent)
	
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Set the planning state to the parameter provided.
/// PARAMS:
///    iNewState - INT - The new state to be set to.
PROC GO_TO_HEIST_PLANNING_STATE(INT iNewState)
	INT index
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] = NATIVE_TO_INT(PLAYER_ID())
			IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
			
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHeistReadyState = iNewState
			
				IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
					BROADCST_SCRIPT_EVENT_HEIST_UPDATE_STATUS(iNewState)
				ENDIF
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH] - GO_TO_HEIST_PLANNING_STATE - Going to new ready state: ", iNewState)
					DEBUG_PRINTCALLSTACK()
				#ENDIF

			ENDIF
		ENDIF
	ENDFOR
ENDPROC


/// PURPOSE:
///    Set the local player's is bad sport value.
PROC SET_PLAYER_BAD_SPORT_VALUE()
	INT index
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] = NATIVE_TO_INT(PLAYER_ID())
			IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
			
				GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].bIsBadSport = NETWORK_PLAYER_IS_BADSPORT()

			ENDIF
		ENDIF
	ENDFOR
ENDPROC


/// PURPOSE:
///    Get the local player's is bad sport value.
FUNC BOOL GET_PLAYER_BAD_SPORT_VALUE(PLAYER_INDEX playerID)
	
	INT index
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] = NATIVE_TO_INT(PLAYER_ID())
			IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
			
				RETURN GlobalplayerBD[NATIVE_TO_INT(playerID)].bIsBadSport

			ENDIF
		ENDIF
	ENDFOR	
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Check the saved array for members that already exist. Also purge missing members.
PROC CHECK_FOR_ALREADY_SAVED_PLAYERS(INT &index, PLAYER_INDEX &playerID, GAMER_HANDLE &currentHandle, INT &iMissingPlayerSlot, INT &iEmptySlot)

	IF lastHeistPlayers.Gamers[index].Data1 = currentHandle.Data1
		IF NETWORK_IS_HANDLE_VALID(lastHeistPlayers.Gamers[index], SIZE_OF(lastHeistPlayers.Gamers[index]))
		AND NETWORK_IS_HANDLE_VALID(currentHandle, SIZE_OF(currentHandle))
			IF NETWORK_ARE_HANDLES_THE_SAME(lastHeistPlayers.Gamers[index],currentHandle)
				//PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_FOR_ALREADY_SAVED_PLAYERS - CurrentHandle: ",currentHandle," and saved handle: ",lastHeistPlayers.Gamers[index]," are the same. Dropping to next member...")
				PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_FOR_ALREADY_SAVED_PLAYERS - Found handle in lastHeistPlayers at [",index,"]. Dropping out..")
				SET_BIT(lastHeistPlayers.iStoredPlayersBS, NATIVE_TO_INT(playerID))
				EXIT
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(index))
			IF iMissingPlayerSlot = -1
				iMissingPlayerSlot = index
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_FOR_ALREADY_SAVED_PLAYERS - IS_NET_PLAYER_OK[",index,"] = FALSE, Setting iMissingPlayerSlot to index: ", index)
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_FOR_ALREADY_SAVED_PLAYERS - IS_NET_PLAYER_OK[",index,"] = TRUE, Name: ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(index)))
			#ENDIF
		
			IF iEmptySlot = -1
				IF NOT IS_GAMER_HANDLE_VALID(lastHeistPlayers.Gamers[index])
				
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH] - CHECK_FOR_ALREADY_SAVED_PLAYERS - IS_GAMER_HANDLE_VALID = FALSE, Setting iEmptySlot to index: ", index)
					#ENDIF
					
					iEmptySlot = index
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE:
///    Calculate some non-transition vars for the post heist stats stuff used by William.
///    Leader only.
PROC CALCULATE_STATS_FOR_POST_HEIST()

	IF NOT IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
		EXIT
	ENDIF

	INT iTotalMembers, index

	g_TransitionSessionNonResetVars.bAllHeistCutsEven = TRUE
	
	IF (g_HeistPlanningClient.sHeistCut.iCutPercent[ci_HEIST_LEADER] = HEIST_CUT_PERCENT_TOTAL)
		g_TransitionSessionNonResetVars.bAllHeistCutsEven = FALSE
		PRINTLN("[AMEC][HEIST_LAUNCH] - CALCULATE_STATS_FOR_POST_HEIST - Heist leader has full cut, exiting.")
		EXIT
	ENDIF

	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF g_HeistPlanningClient.iPlayerOrder[index] != -1
			IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
				iTotalMembers++ 
			ENDIF
		ENDIF
	ENDFOR
	
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF g_HeistPlanningClient.iPlayerOrder[index] != -1
			IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
				IF (g_HeistPlanningClient.sHeistCut.iCutPercent[index] != (HEIST_CUT_PERCENT_TOTAL / iTotalMembers))
					g_TransitionSessionNonResetVars.bAllHeistCutsEven = FALSE
					PRINTLN("[AMEC][HEIST_LAUNCH] - CALCULATE_STATS_FOR_POST_HEIST - Heist member cuts are NOT even. iPlayerOrder[",index,"]'s cut of ",g_HeistPlanningClient.sHeistCut.iCutPercent[index]," is different to average of ",(HEIST_CUT_PERCENT_TOTAL / iTotalMembers))
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[AMEC][HEIST_LAUNCH] - CALCULATE_STATS_FOR_POST_HEIST - All heist member cuts are even.")

ENDPROC


/// PURPOSE:
///    Save heist finale board participant details for quick restore. LEADER ONLY.
/// PARAMS:
///    index - Index of player in iPlayerOrder.
PROC SAVE_HEIST_MEMBER_DATA_FOR_QUICK_RESTART(INT index)
	PRINTLN("[AMEC][HEIST_LAUNCH] - SAVE_HEIST_MEMBER_DATA_FOR_QUICK_RESTART - Saving heist member data for player [",index,", ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(g_HeistPlanningClient.iPlayerOrder[index])),"] ")
	g_TransitionSessionNonResetVars.sMemberData[index].ghPlayer = GET_GAMER_HANDLE_PLAYER(INT_TO_PLAYERINDEX(g_HeistPlanningClient.iPlayerOrder[index]))
	g_TransitionSessionNonResetVars.sMemberData[index].iCutPercent = g_HeistPlanningClient.sHeistCut.iCutPercent[index]
	g_TransitionSessionNonResetVars.sMemberData[index].iTeam = ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index])
//	g_TransitionSessionNonResetVars.sMemberData[index].iOutfit = g_HeistPlanningClient.iSelectedOutfit[index]
ENDPROC

PROC LOAD_HEIST_MEMBER_DATA_FOR_QUICK_RESTART(INT index)
	PRINTLN("[AMEC][HEIST_LAUNCH] - LOAD_HEIST_MEMBER_DATA_FOR_QUICK_RESTART - Loading saved heist member data for player [",index,", ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(g_HeistPlanningClient.iPlayerOrder[index])),"] ")
	PRINTLN("[AMEC][HEIST_LAUNCH] - LOAD_HEIST_MEMBER_DATA_FOR_QUICK_RESTART ...  iCutPercent: 	", g_TransitionSessionNonResetVars.sMemberData[index].iCutPercent)
	PRINTLN("[AMEC][HEIST_LAUNCH] - LOAD_HEIST_MEMBER_DATA_FOR_QUICK_RESTART ...  iTeam: 		", g_TransitionSessionNonResetVars.sMemberData[index].iTeam)
//	PRINTLN("[AMEC][HEIST_LAUNCH] - LOAD_HEIST_MEMBER_DATA_FOR_QUICK_RESTART ...  iOutfit: 		", g_TransitionSessionNonResetVars.sMemberData[index].iOutfit)
	
	g_HeistPlanningClient.sHeistCut.iCutPercent[index] = g_TransitionSessionNonResetVars.sMemberData[index].iCutPercent
	g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = INT_TO_ENUM(HEIST_ROLES, g_TransitionSessionNonResetVars.sMemberData[index].iTeam)
//	g_HeistPlanningClient.iSelectedOutfit[index] = g_TransitionSessionNonResetVars.sMemberData[index].iOutfit
ENDPROC


FUNC BOOL ARE_HEIST_FINALE_MEMBERS_ALREADY_SAVED()

	INT index, subIndex, iEmptyCount
	INT iPlayersFound[MAX_HEIST_PLAYER_SLOTS]
	GAMER_HANDLE ghPlayerTemp
	
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		FOR subIndex = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
			IF g_HeistPlanningClient.iPlayerOrder[index] != -1
				
				ghPlayerTemp = GET_GAMER_HANDLE_PLAYER(INT_TO_PLAYERINDEX(g_HeistPlanningClient.iPlayerOrder[index]))
				
				IF NETWORK_IS_HANDLE_VALID(ghPlayerTemp, SIZE_OF(ghPlayerTemp))
				AND NETWORK_IS_HANDLE_VALID(g_TransitionSessionNonResetVars.sMemberData[subIndex].ghPlayer, SIZE_OF(g_TransitionSessionNonResetVars.sMemberData[subIndex].ghPlayer))
					IF NETWORK_ARE_HANDLES_THE_SAME(g_TransitionSessionNonResetVars.sMemberData[subIndex].ghPlayer, ghPlayerTemp)
						PRINTLN("[AMEC][HEIST_LAUNCH] - ARE_HEIST_FINALE_MEMBERS_ALREADY_SAVED - Found previous finale participant: slot=",subIndex," : [",index,", ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(g_HeistPlanningClient.iPlayerOrder[index])),"] ")
						iPlayersFound[index] = ci_HEIST_SAVED_PARTICIPANT_MATCH
						subIndex = (MAX_HEIST_PLAYER_SLOTS-1) // This is where I would use my loop break, IF I HAD ONE.
					ELSE
						PRINTLN("[AMEC][HEIST_LAUNCH] - ARE_HEIST_FINALE_MEMBERS_ALREADY_SAVED - Setting ",index," to MISMATCH.")
						iPlayersFound[index] = ci_HEIST_SAVED_PARTICIPANT_MISMATCH
					ENDIF
				ELSE
					PRINTLN("[AMEC][HEIST_LAUNCH] - ARE_HEIST_FINALE_MEMBERS_ALREADY_SAVED - Setting ",index," to INVALID due to INVALID HANDLE.")
					iPlayersFound[index] = ci_HEIST_SAVED_PARTICIPANT_INVALID
				ENDIF
				
			ELSE
				iPlayersFound[index] = ci_HEIST_SAVED_PARTICIPANT_EMPTY
			ENDIF
		ENDFOR
	ENDFOR
	
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF iPlayersFound[index] != ci_HEIST_SAVED_PARTICIPANT_EMPTY
			IF iPlayersFound[index] != ci_HEIST_SAVED_PARTICIPANT_INVALID
				IF iPlayersFound[index] = ci_HEIST_SAVED_PARTICIPANT_MISMATCH
					PRINTLN("[AMEC][HEIST_LAUNCH] - ARE_HEIST_FINALE_MEMBERS_ALREADY_SAVED - Player slot ",index," was not found in saved data, not using saved.")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	// Make sure they're not all empty - fringe case but we don't want to take any chances.
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF iPlayersFound[index] = ci_HEIST_SAVED_PARTICIPANT_EMPTY
		OR iPlayersFound[index] = ci_HEIST_SAVED_PARTICIPANT_INVALID
			iEmptyCount++
		ENDIF
	ENDFOR
	
	IF iEmptyCount = MAX_HEIST_PLAYER_SLOTS
		PRINTLN("[AMEC][HEIST_LAUNCH] - ARE_HEIST_FINALE_MEMBERS_ALREADY_SAVED - iEmptyCount: ",iEmptyCount," is equal to maximum players, do not restore saved data.")
		RETURN FALSE
	ELIF iEmptyCount > 0
		PRINTLN("[AMEC][HEIST_LAUNCH] - ARE_HEIST_FINALE_MEMBERS_ALREADY_SAVED - iEmptyCount: ",iEmptyCount," is greater than 0, do not restore saved data.")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    If the local struct has not yet been initialised then fill our struct, grabbing gamer tags ready for corona
PROC MAINTAIN_LAST_HEIST_PLAYERS(INT &iLastHeistGamerToProcess, INT &iLastHeistNameProgress)
	
	IF NOT lastHeistPlayers.bGamersInitialised
		
		GAMER_HANDLE savedGamer = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.gamerFromLastJob[iLastHeistGamerToProcess]
		
		BOOL bIncrementPlayer
		IF IS_GAMER_HANDLE_VALID(savedGamer)
			
			IF NOT IS_XBOX360_VERSION()
			AND NOT IS_XBOX_PLATFORM()
			AND NOT IS_PLAYSTATION_PLATFORM()
				lastHeistPlayers.Gamers[iLastHeistGamerToProcess] 		= savedGamer
				
				IF IS_PC_VERSION()
					lastHeistPlayers.GamersNames[iLastHeistGamerToProcess] = g_savedMPGlobalsNew.g_savedMPGlobals[GET_SAVE_GAME_ARRAY_SLOT()].MpSavedGeneral.gamerFromLastJobTL[iLastHeistGamerToProcess]
				ELSE
					lastHeistPlayers.GamersNames[iLastHeistGamerToProcess]	= NETWORK_GET_GAMERTAG_FROM_HANDLE(savedGamer)
				ENDIF
				
				bIncrementPlayer = TRUE
				PRINTLN("[CORONA] MAINTAIN_LAST_HEIST_PLAYERS - Saved game data has player: ", lastHeistPlayers.GamersNames[iLastHeistGamerToProcess], " available.")
				#IF IS_DEBUG_BUILD
					DEBUG_PRINT_GAMER_HANDLE(savedGamer)
					NETWORK_LOG_GAMER_HANDLE(savedGamer)
				#ENDIF
			ELIF IS_PLAYSTATION_PLATFORM()
				GAMER_HANDLE gamerHandleArray[1]
				gamerHandleArray[0] = savedGamer
				TEXT_LABEL_63 tlGamerNames[1]
			
				SWITCH iLastHeistNameProgress
					CASE 0
						lastHeistPlayers.iGamertagRequestID = NETWORK_DISPLAYNAMES_FROM_HANDLES_START(gamerHandleArray, 1)
						
						IF lastHeistPlayers.iGamertagRequestID >= 0
							iLastHeistNameProgress += 1
							PRINTLN("[CORONA] MAINTAIN_LAST_HEIST_PLAYERS - lastHeistPlayers.iGamertagRequestID = ", lastHeistPlayers.iGamertagRequestID, ". Progressing to pending (1).")
						ENDIF
					BREAK
					
					CASE 1
						INT iNameState
						// Grab our status for pending gamertags / names
						iNameState = NETWORK_GET_DISPLAYNAMES_FROM_HANDLES(lastHeistPlayers.iGamertagRequestID, tlGamerNames, 1)
						
						SWITCH iNameState
							// Fail
							CASE -1
								bIncrementPlayer = TRUE
								iLastHeistNameProgress = 0
							BREAK
							
							// Success
							CASE 0
								lastHeistPlayers.Gamers[iLastHeistGamerToProcess] 		= gamerHandleArray[0]
								lastHeistPlayers.GamersNames[iLastHeistGamerToProcess]	= tlGamerNames[0]
								PRINTLN("[CORONA] MAINTAIN_LAST_HEIST_PLAYERS - Saved game data has player (#2): ", lastHeistPlayers.GamersNames[iLastHeistGamerToProcess], " available.")
								
								bIncrementPlayer = TRUE
								iLastHeistNameProgress = 0
							BREAK
							
							// Pending
							CASE 1
								#IF IS_DEBUG_BUILD
								IF GET_GAME_TIMER() % 1000 < 75
									PRINTLN("[CORONA] MAINTAIN_LAST_HEIST_PLAYERS - Pending for ", iLastHeistGamerToProcess, " - NETWORK_GET_DISPLAYNAMES_FROM_HANDLES() = 1")
								ENDIF
								#ENDIF
							BREAK
						ENDSWITCH
					BREAK
				ENDSWITCH
			ELSE
				SWITCH iLastHeistNameProgress
					CASE 0
						IF NOT NETWORK_GAMERTAG_FROM_HANDLE_PENDING()
							NETWORK_GAMERTAG_FROM_HANDLE_START(savedGamer)
							iLastHeistNameProgress++
						ENDIF
					BREAK
					
					CASE 1
						IF NOT NETWORK_GAMERTAG_FROM_HANDLE_PENDING()
							IF NETWORK_GAMERTAG_FROM_HANDLE_SUCCEEDED()
								
								lastHeistPlayers.Gamers[iLastHeistGamerToProcess] 		= savedGamer
								lastHeistPlayers.GamersNames[iLastHeistGamerToProcess]	= NETWORK_GET_GAMERTAG_FROM_HANDLE(savedGamer)
				
								PRINTLN("[CORONA] MAINTAIN_LAST_HEIST_PLAYERS - Saved game data has player (#3): ", lastHeistPlayers.GamersNames[iLastHeistGamerToProcess], " available.")
							ENDIF
							
							bIncrementPlayer = TRUE
							iLastHeistNameProgress = 0
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ELSE
			bIncrementPlayer = TRUE
		ENDIF
		
		// If we have successfully processed a player, move on.
		IF bIncrementPlayer
			iLastHeistGamerToProcess++
			
			// Once all players have been process, set flag to stop processing
			IF iLastHeistGamerToProcess >= MAX_LAST_JOB_GAMERS
				lastHeistPlayers.bGamersInitialised = TRUE
				PRINTLN("[CORONA] MAINTAIN_LAST_HEIST_PLAYERS - Process all saved game data player for Last Job: ", MAX_LAST_JOB_GAMERS)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


/// PURPOSE:
///    When the leader re-enters the property, they need to re-cache a local copy of the global data. They ONLY use local data for
///    speed, so it would be easier to just use this system when re-loading it outside the corona flow.
PROC COPY_GLOBAL_HEIST_DATA_TO_LOCAL()

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - COPY_GLOBAL_HEIST_DATA_TO_LOCAL - LEADER - Copying global data into local variables.")
	#ENDIF
	
	INT index
	
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index]
		g_HeistPlanningClient.sHeistCut.iCutPercent[index] = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[index]
//		g_HeistPlanningClient.iSelectedOutfit[index] = GlobalServerBD_HeistPlanning.iSelectedOutfit[index]
	ENDFOR

ENDPROC


/// PURPOSE:
///    Maintain the scaleform instructional movie (bottom right). This handles all of its own state and is mostly automated. If a new state is added to the
///    planning board, support must also be extended here.
/// PARAMS:
///    sScaleformStruct - 
PROC UPDATE_HEIST_SCALEFORM_INSTRUCTIONS(SCALEFORM_INSTRUCTIONAL_BUTTONS &sScaleformStruct)
									//INT								iSeconds)
	
	// This is to make the map/board switching control more
	// friendly on PC.
	CONTROL_ACTION caGoToMapInput	= INPUT_FRONTEND_LB
	CONTROL_ACTION caGoToBoardInput = INPUT_FRONTEND_RB
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		caGoToMapInput = INPUT_FRONTEND_X
		caGoToBoardInput = INPUT_FRONTEND_CANCEL
	ENDIF
	
	// Don't update if the text chat box is on-screen.
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT g_HeistPlanningClient.bTimerOverride
	#ENDIF
		UPDATE_HEIST_COUNTDOWN_TIMER(sScaleformStruct)
	#IF IS_DEBUG_BUILD
	ELSE
		IF HAS_NET_TIMER_STARTED(g_HeistPlanningClient.sLaunchCountdown)
			RESET_NET_TIMER(g_HeistPlanningClient.sLaunchCountdown)
		ENDIF
	ENDIF
	#ENDIF
	
	INT iSelectedMember = g_HeistPlanningClient.sBoardState.iHighlight
	
	SPRITE_PLACEMENT spPlacementLocation = GET_SCALEFORM_INSTRUCTIONAL_BUTTON_POSITION()
	
	IF g_HeistPlanningClient.bUpdateInstButtons
	
		REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sScaleformStruct)
		
		BUSYSPINNER_OFF()
		
		//Set the max width
		IF NOT GET_IS_WIDESCREEN()
		OR GET_CURRENT_LANGUAGE() = LANGUAGE_JAPANESE
		      SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sScaleformStruct, 0.5)
		ELSE
		      SET_SCALEFORM_INSTRUCTIONAL_BUTTON_WRAP(sScaleformStruct, 0.7)
		ENDIF
//					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X), "HEIST_IB_CONF", sScaleformStruct)
//					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_X), "HEIST_IB_VIEW", sScaleformStruct)
//				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "HEIST_IB_UNRDY", sScaleformStruct)
		
		ADD_SCALEFORM_INSTRUCTIONAL_BUTTON_WITH_INT_AS_TIME("", "HEIST_IB_CONT", (g_HeistPlanningClient.iStoredCountDownTime * 1000), sScaleformStruct) 
		
		SWITCH g_HeistPlanningClient.sBoardState.eFocusPoint
		
			// Camera focus point is the overview of the whole planning board.
			CASE HEIST_FOCUS_OVERVIEW

				BREAK
		
			// Camera focus point is the map.
			CASE HEIST_FOCUS_MAP
			
				IF g_HeistPlanningClient.bLaunchTimerExpired 
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_ICON_SPINNER_STRING(), "HEIST_IB_WAIT", sScaleformStruct) // Waiting for leader
				ENDIF

				// Only gamepad can quit here.
				IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HEIST_IB_QUIT", sScaleformStruct, TRUE)
				ENDIF
				
				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_UD), "HEIST_IB_NAV", sScaleformStruct) // Navigate controls.
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, caGoToBoardInput, "HEIST_IB_BOARD", sScaleformStruct, TRUE) // View player list.
				
				// Can't look around when using keyboard and mouse.
				IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_RSTICK_ALL), "HEIST_IB_LOOK", sScaleformStruct)
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y), "HEIST_IB_ZOOM", sScaleformStruct)
				ENDIF
				
				IF NOT IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
					IF NOT g_HeistPlanningClient.bLaunchTimerExpired
						IF IS_PLAYER_IN_HEIST_READY_TO_LAUNCH(PLAYER_ID())
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, "HEIST_IB_UNRDY", sScaleformStruct, TRUE)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, "HEIST_IB_RDY", sScaleformStruct, TRUE)
						ENDIF
					ENDIF
				ENDIF
			
				BREAK
				
			// Camera focus point is he general board / player list.	
			CASE HEIST_FOCUS_BOARD
			
				IF g_HeistPlanningClient.bLaunchTimerExpired 
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_ICON_SPINNER_STRING(), "HEIST_IB_WAIT", sScaleformStruct) // Waiting for leader
				ENDIF

				IF NOT IS_PLAYER_IN_HEIST_READY_TO_LAUNCH(PLAYER_ID())
				OR g_HeistPlanningClient.bLaunchTimerExpired
				
					IF IS_PLAYER_LEADER_OF_HEIST(PlAYER_ID())
					
						SWITCH iSelectedMember
						
							DEFAULT
								ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, "HEIST_IB_CONFPL", sScaleformStruct, TRUE)
							

								BREAK
								
							CASE ci_HEIST_ROW_LAUNCH
								
								ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, "HEIST_IB_ACCPT", sScaleformStruct, TRUE)
							
								BREAK
						
						ENDSWITCH
						
					ELSE
					
						IF NOT g_HeistPlanningClient.bLaunchTimerExpired
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, "HEIST_IB_RDY", sScaleformStruct, TRUE)
						ENDIF
					ENDIF
					
				ELSE
				
					IF NOT IS_PLAYER_LEADER_OF_HEIST(PlAYER_ID())
						IF NOT g_HeistPlanningClient.bLaunchTimerExpired
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, "HEIST_IB_UNRDY", sScaleformStruct, TRUE)
						ENDIF
					ENDIF 
				ENDIF
				
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HEIST_IB_QUIT", sScaleformStruct, TRUE) // Go back.
				
				IF iSelectedMember != ci_HEIST_ROW_LAUNCH
					ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, "HEIST_IB_STAT", sScaleformStruct, TRUE) // view player card.
				ENDIF
			
				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_UD), "HEIST_IB_NAV", sScaleformStruct) // Navigate controls.
				
				IF g_HeistPlanningClient.sBoardState.iHighlight != ci_HEIST_ROW_LAUNCH
					IF IS_PS3_VERSION() OR IS_PLAYSTATION_PLATFORM() OR IS_PC_VERSION()
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT, "HEIST_IB_PSN", sScaleformStruct, TRUE)
					ELIF IS_XBOX360_VERSION() OR IS_XBOX_PLATFORM()
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT), "HEIST_IB_XBL", sScaleformStruct)
					ELSE
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT), "HEIST_IB_PSNXBL", sScaleformStruct)
					ENDIF
				ENDIF
			
				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, caGoToMapInput, "HEIST_IB_MAP", sScaleformStruct, TRUE) // View map.
				//ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_RB), "HEIST_IB_DESC", sScaleformStruct) // View description / email.
				
				// Can't look around when using keyboard and mouse.
				IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_RSTICK_ALL), "HEIST_IB_LOOK", sScaleformStruct)
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y), "HEIST_IB_ZOOM", sScaleformStruct)
				ENDIF
				
				BREAK
			
			// Camera focus point is the player's specific details (e.g. cut, role)
			CASE HEIST_FOCUS_BOARD_SUB
			
				SWITCH g_HeistPlanningClient.sBoardState.iSubHighlight
				
					CASE ci_HEIST_COLUMN_ROLE
					
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, "HEIST_IB_SAVE", sScaleformStruct, TRUE)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HEIST_IB_UNSAVE", sScaleformStruct, TRUE)
						
						IF GET_HEIST_MEMBER_TEAM_NUMBER(g_HeistPlanningClient.iSelectedHeistMember) != INVALID_HEIST_DATA
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, "HEIST_IB_REVOKE", sScaleformStruct, TRUE)
						ENDIF

						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_LR), "HEIST_IB_NAV2", sScaleformStruct)
						
						// Can't look around when using keyboard and mouse.
						IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_RSTICK_ALL), "HEIST_IB_LOOK", sScaleformStruct)
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y), "HEIST_IB_ZOOM", sScaleformStruct)
						ENDIF
			
						BREAK
						
					CASE ci_HEIST_COLUMN_CUT
					
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, "HEIST_IB_SAVE", sScaleformStruct, TRUE)
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HEIST_IB_UNSAVE", sScaleformStruct, TRUE)
						
						IF GET_CUT_INFO_FOR_PLAYER(g_HeistPlanningClient.iSelectedHeistMember) != 0
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_Y, "HEIST_IB_REVOKE", sScaleformStruct, TRUE)
						ENDIF
						
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_LR), "HEIST_IB_NAV2", sScaleformStruct)
						
						// Can't look around when using keyboard and mouse.
						IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_RSTICK_ALL), "HEIST_IB_LOOK", sScaleformStruct)
							ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y), "HEIST_IB_ZOOM", sScaleformStruct)
						ENDIF
					
						BREAK
						
				ENDSWITCH
				
				BREAK
				
			// Camera focus point is the heist description email.
//			CASE HEIST_FOCUS_DESC
//				
////					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_LR), "HEIST_IB_NAV2", sScaleformStruct)
////					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL), "HEIST_IB_UNSAVE", sScaleformStruct)
////					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT), "HEIST_IB_SAVE", sScaleformStruct)
//			
//				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, caGoToMapInput, "HEIST_IB_BOARD", sScaleformStruct, TRUE) // View player list.
//				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HEIST_IB_QUIT", sScaleformStruct, TRUE)
//				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_RSTICK_ALL), "HEIST_IB_LOOK", sScaleformStruct)
//				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y), "HEIST_IB_ZOOM", sScaleformStruct)
//				IF NOT IS_PLAYER_LEADER_OF_HEIST(PlAYER_ID())
//					IF NOT g_HeistPlanningClient.bLaunchTimerExpired
//						IF IS_PLAYER_IN_HEIST_READY_TO_LAUNCH(PLAYER_ID())
//							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, "HEIST_IB_UNRDY", sScaleformStruct, TRUE)
//						ELSE
//							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, "HEIST_IB_RDY", sScaleformStruct, TRUE)
//						ENDIF
//					ENDIF
//				ENDIF
//			
//				BREAK
				
			CASE HEIST_FOCUS_STATS
			
				IF g_HeistPlanningClient.bLaunchTimerExpired 
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_ICON_SPINNER_STRING(), "HEIST_IB_WAIT", sScaleformStruct) // Waiting for leader
				ENDIF

				ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL, "HEIST_IB_BACK", sScaleformStruct, TRUE)
				ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_DPAD_UD), "HEIST_IB_NAV", sScaleformStruct) // Navigate controls.
			
				IF g_HeistPlanningClient.sBoardState.iHighlight != ci_HEIST_ROW_LAUNCH
					IF IS_PS3_VERSION() OR IS_PLAYSTATION_PLATFORM() OR IS_PC_VERSION()
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT), "HEIST_IB_PSN", sScaleformStruct)
					ELIF IS_XBOX360_VERSION() OR IS_XBOX_PLATFORM()
						ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT), "HEIST_IB_XBL", sScaleformStruct)
					ELSE
						ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT, "HEIST_IB_PSNXBL", sScaleformStruct, TRUE)
					ENDIF
				ENDIF
			
				
				// Can't look around if using keyboard and mouse
				IF NOT IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_GROUP_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUTGROUP_FRONTEND_RSTICK_ALL), "HEIST_IB_LOOK", sScaleformStruct)
					ADD_SCALEFORM_INSTRUCTIONAL_BUTTON(GET_CONTROL_INSTRUCTIONAL_BUTTONS_STRING(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y), "HEIST_IB_ZOOM", sScaleformStruct)
				ENDIF
				
				IF NOT IS_PLAYER_LEADER_OF_HEIST(PlAYER_ID())
					IF NOT g_HeistPlanningClient.bLaunchTimerExpired
						IF IS_PLAYER_IN_HEIST_READY_TO_LAUNCH(PLAYER_ID())
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, "HEIST_IB_UNRDY", sScaleformStruct, TRUE)
						ELSE
							ADD_SCALEFORM_INSTRUCTIONAL_INPUT(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT, "HEIST_IB_RDY", sScaleformStruct, TRUE)
						ENDIF
					ENDIF
				ENDIF
			
				BREAK
		
		ENDSWITCH
		
		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(g_HeistSharedClient.PlanningInstButtonsIndex,
											spPlacementLocation,
											sScaleformStruct,
											SHOULD_REFRESH_SCALEFORM_INSTRUCTIONAL_BUTTONS(sScaleformStruct))
											
		g_HeistPlanningClient.bUpdateInstButtons = FALSE
		
	ELSE
		
		RUN_SCALEFORM_INSTRUCTIONAL_BUTTONS(g_HeistSharedClient.PlanningInstButtonsIndex,
											spPlacementLocation,
											sScaleformStruct,
											FALSE)
	ENDIF
	
											
ENDPROC


#IF IS_DEBUG_BUILD
PROC DRAW_SCALEFORM_WIDGETS()

	INT index, i
	TEXT_LABEL_63 tlTemp1
	TEXT_LABEL_63 tlTemp2
	
	TEXT_LABEL_3 tlTempStr1 = "]["
	TEXT_LABEL_3 tlTempStr2 = "]: "

	DEBUG_PlanningWidgets = DEBUG_PlanningWidgets
	SET_CURRENT_WIDGET_GROUP(DEBUG_ParentWidgets)
	DEBUG_PlanningWidgets = START_WIDGET_GROUP("Heist Planning") 
	
		ADD_WIDGET_BOOL("Draw debug text on-screen", bRenderPlanningDebugText)
		ADD_WIDGET_BOOL("FAKE - All clients ready", bFakeAllReady)
		
		START_WIDGET_GROUP("Todo list mouse support")
			ADD_WIDGET_FLOAT_SLIDER("cf_HEIST_MOUSE_TODO_ORIGIN_X", cf_HEIST_MOUSE_TODO_ORIGIN_X, 0.0, 1.0, 0.005)
			ADD_WIDGET_FLOAT_SLIDER("cf_HEIST_MOUSE_TODO_ORIGIN_Y", cf_HEIST_MOUSE_TODO_ORIGIN_Y, 0.0, 1.0, 0.42)
			ADD_WIDGET_FLOAT_SLIDER("cf_HEIST_MOUSE_TODO_ITEM_HEIGHT", cf_HEIST_MOUSE_TODO_ITEM_HEIGHT, 0.0, 1.0, 0.024) 
			ADD_WIDGET_FLOAT_SLIDER("cf_HEIST_MOUSE_TODO_ITEM_WIDTH", cf_HEIST_MOUSE_TODO_ITEM_WIDTH, 0.0, 1.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("cf_HEIST_MOUSE_TODO_ITEM_GAP", cf_HEIST_MOUSE_TODO_ITEM_GAP,  0.0, 1.0, 0.01)
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("Camera Transition.")
			ADD_WIDGET_INT_SLIDER("Speed", ci_HEIST_BOARD_TRANSITION_SPEED_STD, 0, 5000, 1)
			ADD_WIDGET_INT_SLIDER("Type" , ci_HEIST_BOARD_TRANSITION_CAMERA_GRAPH, 0, COUNT_OF(CAMERA_GRAPH_TYPE) - 2, 1)
		STOP_WIDGET_GROUP()
	
		START_WIDGET_GROUP("Variables - Server") 
			FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
			
				tlTemp1 = " ----- MEMBER: "
				tlTemp2	= " ----- "
				tlTemp1 += index
				tlTemp1 += tlTemp2
				ADD_WIDGET_STRING(tlTemp1)
			
				tlTemp1 = "iPlayerOrder["
				tlTemp1 += index
				tlTemp1 += tlTempStr2
				ADD_WIDGET_INT_READ_ONLY(tlTemp1, GlobalServerBD_HeistPlanning.iPlayerOrder[index])
				
				tlTemp1 = "iHeistPlanningState["
				tlTemp1 += index
				tlTemp1 += tlTempStr2
				ADD_WIDGET_INT_READ_ONLY(tlTemp1, GlobalServerBD_HeistPlanning.iHeistPlanningState[index])
				
				tlTemp1 = "iCutPercent["
				tlTemp1 += index
				tlTemp1 += tlTempStr2
				ADD_WIDGET_INT_READ_ONLY(tlTemp1, GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[index])
			ENDFOR
			
			ADD_WIDGET_STRING(" --- END MEMBERS --- ")
			
			ADD_WIDGET_INT_READ_ONLY("iHeistTotalRules: ", 		GlobalServerBD_HeistPlanning.iHeistTotalRules)
			ADD_WIDGET_INT_READ_ONLY("iHeistTotalTeams: ", 		GlobalServerBD_HeistPlanning.iHeistTotalTeams)
			ADD_WIDGET_INT_READ_ONLY("iHeistTotalPay: ", 		GlobalServerBD_HeistPlanning.iHeistTotalPay)
			ADD_WIDGET_INT_READ_ONLY("iHeistSetupCosts: ", 		GlobalServerBD_HeistPlanning.iHeistSetupCosts)
			
			ADD_WIDGET_BOOL("(READ ONLY) bDoLaunchHeist", 				GlobalServerBD_HeistPlanning.bDoLaunchHeist)
			ADD_WIDGET_BOOL("(READ ONLY) bForceExitPlanning", 			GlobalServerBD_HeistPlanning.bForceExitPlanning)
//			ADD_WIDGET_BOOL("(READ ONLY) DEBUG - bKickHeistPlayers",	GlobalServerBD_HeistPlanning.DEBUG_bKickHeistPlayers)
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Variables - Local") 
			ADD_WIDGET_STRING(" ---------- INTEGERS ---------- ")
			
			ADD_WIDGET_INT_SLIDER("Button Press Delay", g_tiBUTTON_PRESS_DELAY, 0, 2500, 1)
			
			FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
				tlTemp1 = "iPlayerStatuses["
				tlTemp1 += index
				tlTemp1 += tlTempStr2
				ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPlanningClient.iPlayerStatuses[index], -10, 10, 1)
			ENDFOR
			
//			FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
//				tlTemp1 = "iSelectedOutfit["
//				tlTemp1 += index
//				tlTemp1 += tlTempStr2
//				ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPlanningClient.iSelectedOutfit[index], -1, 128, 1)
//			ENDFOR
			
			START_WIDGET_GROUP("iTarget & iRules") 
				FOR index = 0 TO (FMMC_MAX_TEAMS-1)
					
					tlTemp1 = " --- TEAM "
					tlTemp2	= " --- "
					tlTemp1 += index
					tlTemp1 += tlTemp2
					ADD_WIDGET_STRING(tlTemp1)
					
					FOR i = 0 TO (FMMC_MAX_RULES-1)
						tlTemp1 = "iObjectiveTarget["
						tlTemp1 += index
						tlTemp1 += tlTempStr1 // "]["
						tlTemp1 += i
						tlTemp1 += tlTempStr2 // "]: "
						ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPlanningClient.iObjectiveTarget[index][i], -64, 64, 1)
						
						tlTemp1 = "iObjectiveRule["
						tlTemp1 += index
						tlTemp1 += tlTempStr1 // "]["
						tlTemp1 += i
						tlTemp1 += tlTempStr2 // "]: "
						ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPlanningClient.iObjectiveRule[index][i], -64, 64, 1)
					ENDFOR
				ENDFOR
			STOP_WIDGET_GROUP()
			
			FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
			
				tlTemp1 = " --- MEMBER: "
				tlTemp2	= " --- "
				tlTemp1 += index
				tlTemp1 += tlTemp2
				ADD_WIDGET_STRING(tlTemp1)
			
				tlTemp1 = "iPlayerOrder["
				tlTemp1 += index
				tlTemp1 += tlTempStr2
				ADD_WIDGET_INT_READ_ONLY(tlTemp1, g_HeistPlanningClient.iPlayerOrder[index])
				
				tlTemp1 = "iCutPercent["
				tlTemp1 += index
				tlTemp1 += tlTempStr2
				ADD_WIDGET_INT_READ_ONLY(tlTemp1, g_HeistPlanningClient.sHeistCut.iCutPercent[index])
			ENDFOR
			
			ADD_WIDGET_STRING(" --- END MEMBERS --- ")

			ADD_WIDGET_INT_SLIDER("iHeistTotalPay: ", 				g_HeistPlanningClient.iHeistTotalPay, 0, 9999999, 1)
			ADD_WIDGET_INT_SLIDER("iHeistSetupCosts: ", 			g_HeistPlanningClient.iHeistSetupCosts, 0, 9999999, 1)
			ADD_WIDGET_INT_SLIDER("iTotalRules: ", 					g_HeistPlanningClient.iTotalRules, -1, 20, 1)
			ADD_WIDGET_INT_SLIDER("iTotalTeams: ", 					g_HeistPlanningClient.iTotalTeams, -1, 8, 1)
			ADD_WIDGET_INT_SLIDER("iSelectedTeam: ", 				g_HeistPlanningClient.iSelectedTeam, -1, 8, 1)
			ADD_WIDGET_INT_SLIDER("iSelectedRule: ", 				g_HeistPlanningClient.iSelectedRule, -1, 20, 1)
			ADD_WIDGET_INT_SLIDER("iCameraLocationIndex: ", 		g_HeistPlanningClient.iCameraLocationIndex, -50, 50, 1)
			ADD_WIDGET_INT_SLIDER("iPlayerLoopIndex: ", 			g_HeistPlanningClient.iPlayerLoopIndex, -1, 8, 1)
			ADD_WIDGET_INT_SLIDER("iHeistLeaderIndex: ", 			g_HeistPlanningClient.iHeistLeaderIndex, -1, 20, 1)
			ADD_WIDGET_INT_SLIDER("iSelectedHeistMember: ", 		g_HeistPlanningClient.iSelectedHeistMember, -1, 20, 1)
			ADD_WIDGET_INT_READ_ONLY("iHeistPlanningBitset: ", 		g_HeistPlanningClient.iHeistPlanningBitset)
			ADD_WIDGET_INT_READ_ONLY("iPlayerNetStatusBitset: ", 	g_HeistPlanningClient.iPlayerNetStatusBitset)
			ADD_WIDGET_INT_READ_ONLY("iWarningScreenButtonBitset: ",g_HeistPlanningClient.iWarningScreenButtonBitset)
			
			
			
			ADD_WIDGET_STRING(" ---------- BOOLEANS ---------- ")
			
			ADD_WIDGET_BOOL("bPlayerCardsInSync", 			g_HeistPlanningClient.bPlayerCardsInSync)
			ADD_WIDGET_BOOL("bPlayerNetStatusInSync", 		g_HeistPlanningClient.bPlayerNetStatusInSync)
			ADD_WIDGET_BOOL("bDoPurgePlayerList", 			g_HeistPlanningClient.bDoPurgePlayerList)
			ADD_WIDGET_BOOL("bHaveSentLaunchTrigger", 		g_HeistPlanningClient.bHaveSentLaunchTrigger)
			ADD_WIDGET_BOOL("bHaveDoneHelp", 				g_HeistPlanningClient.bHaveDoneHelp)
			ADD_WIDGET_BOOL("bForceHeistExit", 				g_HeistPlanningClient.bForceHeistExit)
			ADD_WIDGET_BOOL("bHaveDoneScreenCleanup", 		g_HeistPlanningClient.bHaveDoneScreenCleanup)
			ADD_WIDGET_BOOL("bForceHeistPlanningComplete", 	g_HeistPlanningClient.bForceHeistPlanningComplete)
			ADD_WIDGET_BOOL("bExtendedLaunchTime",			g_HeistPlanningClient.bExtendedLaunchTime)
//			ADD_WIDGET_BOOL("bNoOutfitsAvailable",			g_HeistPlanningClient.bNoOutfitsAvailable)
			ADD_WIDGET_BOOL("bTimerOverride",				g_HeistPlanningClient.bTimerOverride)
			
			ADD_WIDGET_BOOL("DEBUG - bSkipPlanningHelpText",g_HeistPlanningClient.DEBUG_bSkipPlanningHelpText)
			
			ADD_WIDGET_STRING(" ---------- STRINGS ---------- ")
			
			FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
				FOR i = 0 TO (MAX_HEIST_PLAYERS_PER_SETUP-1)
					tlTemp1 = "tlMissionPlayerName["
					tlTemp1 += index
					tlTemp1 += tlTempStr1 // "]["
					tlTemp1 += i
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_STRING(tlTemp1)
					ADD_WIDGET_STRING(g_HeistSharedClient.tlMissionPlayerName[index][i])
				ENDFOR
			ENDFOR
			
			FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
				FOR i = 0 TO (MAX_HEIST_PLAYERS_PER_SETUP-1)
					tlTemp1 = "tlMissionPlayerUID["
					tlTemp1 += index
					tlTemp1 += tlTempStr1 // "]["
					tlTemp1 += i
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_STRING(tlTemp1)
					ADD_WIDGET_STRING(g_HeistSharedClient.tlMissionPlayerUID[index][i])
				ENDFOR
			ENDFOR
			
			ADD_WIDGET_STRING(" ---------- CUSTOM STRUCTS ---------- ")
			
			START_WIDGET_GROUP("HEIST_BOARD_DATA") 
				ADD_WIDGET_VECTOR_SLIDER("vBoardPosition: ", 	g_HeistSharedClient.vBoardPosition, -5000, 5000, 1)
				ADD_WIDGET_VECTOR_SLIDER("vBoardRotation: ", 	g_HeistSharedClient.vBoardRotation, -5000, 5000, 1)
				ADD_WIDGET_VECTOR_SLIDER("vBoardSize: ", 		g_HeistSharedClient.vBoardSize, -50, 50, 1)
				ADD_WIDGET_VECTOR_SLIDER("vWorldSize: ", 		g_HeistSharedClient.vWorldSize, -50, 50, 1)
				ADD_WIDGET_VECTOR_SLIDER("vMapPosition: ", 		g_HeistSharedClient.vMapPosition, -5000, 5000, 1)
				ADD_WIDGET_VECTOR_SLIDER("vMapRotation: ", 		g_HeistSharedClient.vMapRotation, -5000, 5000, 1)
				ADD_WIDGET_VECTOR_SLIDER("vMapSize: ", 			g_HeistSharedClient.vMapSize, -50, 50, 1)
				ADD_WIDGET_VECTOR_SLIDER("vMapSize: ", 			g_HeistSharedClient.vMapSize, -50, 50, 1)
				
				FOR index = 0 TO (FMMC_MAX_RULES-1)
					tlTemp1 = " - RULE "
					tlTemp2	= " - "
					tlTemp1 += index
					tlTemp1 += tlTemp2
					ADD_WIDGET_STRING(tlTemp1)
				
					tlTemp1 = "iMapPins[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPlanningClient.sBoardData.iMapPins[index], -1, ((index * 100) + 99), 1)
					
					tlTemp1 = "iMapText[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPlanningClient.sBoardData.iMapText[index], -1, ((index * 100) + 99), 1)
					
					tlTemp1 = "iMapPostIt[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPlanningClient.sBoardData.iMapPostIt[index], -1, ((index * 100) + 99), 1)
					
					tlTemp1 = "iMapArrows[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPlanningClient.sBoardData.iMapArrows[index], -1, ((index * 100) + 99), 1)
					
					tlTemp1 = "iMapAreas[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPlanningClient.sBoardData.iMapAreas[index], -1, ((index * 100) + 99), 1)
					
					tlTemp1 = "iMapHighlights[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1, g_HeistPlanningClient.sBoardData.iMapHighlights[index], -1, ((index * 100) + 99), 1)
					
					tlTemp1 = " - END RULE "
					tlTemp2	= " - "
					tlTemp1 += index
					tlTemp1 += tlTemp2
					ADD_WIDGET_STRING(tlTemp1)
					ADD_WIDGET_STRING(" ")
				ENDFOR
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("HEIST_BOARD_CAMERA_DATA") 
				ADD_WIDGET_BOOL("bTransitionInProgress", 	g_HeistPlanningClient.sBoardCamData.bTransitionInProgress)
				ADD_WIDGET_VECTOR_SLIDER("vCamCurrPos: ", 	g_HeistPlanningClient.sBoardCamData.vCamCurrPos, -5000, 5000, 1)
				ADD_WIDGET_VECTOR_SLIDER("vCamCurrRot: ", 	g_HeistPlanningClient.sBoardCamData.vCamCurrRot, -180, 180, 1)
				ADD_WIDGET_VECTOR_SLIDER("vCamStartPos: ", 	g_HeistPlanningClient.sBoardCamData.vCamStartPos, -5000, 5000, 1)
				ADD_WIDGET_VECTOR_SLIDER("vCamStartRot: ", 	g_HeistPlanningClient.sBoardCamData.vCamStartRot, -180, 180, 1)
				ADD_WIDGET_VECTOR_SLIDER("vCamEndPos: ", 	g_HeistPlanningClient.sBoardCamData.vCamEndPos, -5000, 5000, 1)
				ADD_WIDGET_VECTOR_SLIDER("vCamEndRot: ", 	g_HeistPlanningClient.sBoardCamData.vCamEndRot, -180, 180, 1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("HEIST_BOARD_STATE") 
				ADD_WIDGET_INT_SLIDER("iHighlight: ", 		g_HeistPlanningClient.sBoardState.iHighlight, -100, 100, 1)
				ADD_WIDGET_INT_SLIDER("iSubHighlight: ", 	g_HeistPlanningClient.sBoardState.iSubHighlight, -100, 100, 1)
				ADD_WIDGET_INT_SLIDER("iRightArrow: ", 		g_HeistPlanningClient.sBoardState.iRightArrow, -100, 100, 1)
				ADD_WIDGET_INT_SLIDER("iLeftArrow: ", 		g_HeistPlanningClient.sBoardState.iLeftArrow, -100, 100, 1)
				ADD_WIDGET_INT_SLIDER("iSubItemSelection: ",g_HeistPlanningClient.sBoardState.iSubItemSelection, -100, 100, 1)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("HEIST_MAP_VECTOR") 
				FOR index = 0 TO (FMMC_MAX_RULES-1)
					tlTemp1 = "X[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_FLOAT_SLIDER(tlTemp1, 			g_HeistPlanningClient.sCumPedLocations[index].X, -10000, 10000, 1)
					
					tlTemp1 = "Y[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_FLOAT_SLIDER(tlTemp1, 			g_HeistPlanningClient.sCumPedLocations[index].Y, -10000, 10000, 1)
					
					tlTemp1 = "fDiameter[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_FLOAT_SLIDER(tlTemp1, 	g_HeistPlanningClient.sCumPedLocations[index].fDiameter, -1, 10000, 1)
					
					tlTemp1 = "iCount[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1,		g_HeistPlanningClient.sCumPedLocations[index].iCount, -1, 100, 1)
				ENDFOR
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("HEIST_MAP_RULE_COLOUR")
				FOR index = 0 TO (FMMC_MAX_RULES-1)
					tlTemp1 = "iRed[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1,		g_HeistPlanningClient.sMapRuleColours[index].iRed, -1, 255, 1)
					
					tlTemp1 = "iGreen[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1,		g_HeistPlanningClient.sMapRuleColours[index].iGreen, -1, 255, 1)
					
					tlTemp1 = "iBlue[" 
					tlTemp1 += index
					tlTemp1 += tlTempStr2 // "]: "
					ADD_WIDGET_INT_SLIDER(tlTemp1,		g_HeistPlanningClient.sMapRuleColours[index].iBlue, -1, 255, 1)
				ENDFOR
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Animation Positioning") 
		
			START_WIDGET_GROUP("Old high-end 1") 
			
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_PHONE") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_PHONE_X", 			HEIST_ANIM_PHONE_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_PHONE_Y", 			HEIST_ANIM_PHONE_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_PHONE_Z", 			HEIST_ANIM_PHONE_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_PHONE_ROT_X", 		HEIST_ANIM_PHONE_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_PHONE_ROT_Y", 		HEIST_ANIM_PHONE_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_PHONE_ROT_Z", 		HEIST_ANIM_PHONE_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
		
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_WHISKY_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WHISKY_1_X", 		HEIST_ANIM_WHISKY_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WHISKY_1_Y", 		HEIST_ANIM_WHISKY_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WHISKY_1_Z", 		HEIST_ANIM_WHISKY_1_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WHISKY_1_ROT_X", 	HEIST_ANIM_WHISKY_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WHISKY_1_ROT_Y", 	HEIST_ANIM_WHISKY_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WHISKY_1_ROT_Z", 	HEIST_ANIM_WHISKY_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_WHISKY_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WHISKY_2_X", 		HEIST_ANIM_WHISKY_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WHISKY_2_Y", 		HEIST_ANIM_WHISKY_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WHISKY_2_Z", 		HEIST_ANIM_WHISKY_2_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WHISKY_2_ROT_X", 	HEIST_ANIM_WHISKY_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WHISKY_2_ROT_Y", 	HEIST_ANIM_WHISKY_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WHISKY_2_ROT_Z", 	HEIST_ANIM_WHISKY_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_WINE_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINE_1_X", 			HEIST_ANIM_WINE_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINE_1_Y", 			HEIST_ANIM_WINE_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINE_1_Z", 			HEIST_ANIM_WINE_1_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINE_1_ROT_X", 		HEIST_ANIM_WINE_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINE_1_ROT_Y", 		HEIST_ANIM_WINE_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINE_1_ROT_Z", 		HEIST_ANIM_WINE_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_WINE_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINE_2_X", 			HEIST_ANIM_WINE_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINE_2_Y", 			HEIST_ANIM_WINE_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINE_2_Z", 			HEIST_ANIM_WINE_2_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINE_2_ROT_X", 		HEIST_ANIM_WINE_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINE_2_ROT_Y", 		HEIST_ANIM_WINE_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINE_2_ROT_Z", 		HEIST_ANIM_WINE_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_COUCH_M_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_M_1_X", 		HEIST_ANIM_COUCH_M_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_M_1_Y", 		HEIST_ANIM_COUCH_M_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_M_1_Z", 		HEIST_ANIM_COUCH_M_1_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_M_1_ROT_X", 	HEIST_ANIM_COUCH_M_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_M_1_ROT_Y", 	HEIST_ANIM_COUCH_M_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_M_1_ROT_Z", 	HEIST_ANIM_COUCH_M_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_COUCH_M_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_M_2_X", 		HEIST_ANIM_COUCH_M_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_M_2_Y", 		HEIST_ANIM_COUCH_M_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_M_2_Z", 		HEIST_ANIM_COUCH_M_2_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_M_2_ROT_X", 	HEIST_ANIM_COUCH_M_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_M_2_ROT_Y", 	HEIST_ANIM_COUCH_M_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_M_2_ROT_Z", 	HEIST_ANIM_COUCH_M_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_COUCH_F_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_F_1_X", 		HEIST_ANIM_COUCH_F_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_F_1_Y", 		HEIST_ANIM_COUCH_F_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_F_1_Z", 		HEIST_ANIM_COUCH_F_1_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_F_1_ROT_X", 	HEIST_ANIM_COUCH_F_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_F_1_ROT_Y", 	HEIST_ANIM_COUCH_F_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_F_1_ROT_Z", 	HEIST_ANIM_COUCH_F_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_COUCH_F_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_F_2_X", 		HEIST_ANIM_COUCH_F_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_F_2_Y", 		HEIST_ANIM_COUCH_F_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_F_2_Z", 		HEIST_ANIM_COUCH_F_2_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_F_2_ROT_X", 	HEIST_ANIM_COUCH_F_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_F_2_ROT_Y", 	HEIST_ANIM_COUCH_F_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_COUCH_F_2_ROT_Z", 	HEIST_ANIM_COUCH_F_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_TV_M_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_M_1_X", 			HEIST_ANIM_TV_M_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_M_1_Y", 			HEIST_ANIM_TV_M_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_M_1_Z", 			HEIST_ANIM_TV_M_1_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_M_1_ROT_X", 		HEIST_ANIM_TV_M_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_M_1_ROT_Y", 		HEIST_ANIM_TV_M_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_M_1_ROT_Z", 		HEIST_ANIM_TV_M_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_TV_M_2")	 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_M_2_X", 			HEIST_ANIM_TV_M_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_M_2_Y", 			HEIST_ANIM_TV_M_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_M_2_Z", 			HEIST_ANIM_TV_M_2_Z, -1000.0, 1000.0, 0.001)
					                                                   	                 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_M_2_ROT_X", 		HEIST_ANIM_TV_M_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_M_2_ROT_Y", 		HEIST_ANIM_TV_M_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_M_2_ROT_Z", 		HEIST_ANIM_TV_M_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_TV_F_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_F_1_X", 			HEIST_ANIM_TV_F_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_F_1_Y", 			HEIST_ANIM_TV_F_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_F_1_Z", 			HEIST_ANIM_TV_F_1_Z, -1000.0, 1000.0, 0.001)
					                                                   	                
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_F_1_ROT_X", 		HEIST_ANIM_TV_F_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_F_1_ROT_Y", 		HEIST_ANIM_TV_F_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_F_1_ROT_Z", 		HEIST_ANIM_TV_F_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_TV_F_2")	 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_F_2_X", 			HEIST_ANIM_TV_F_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_F_2_Y", 			HEIST_ANIM_TV_F_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_F_2_Z", 			HEIST_ANIM_TV_F_2_Z, -1000.0, 1000.0, 0.001)
					                                                   	               
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_F_2_ROT_X", 		HEIST_ANIM_TV_F_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_F_2_ROT_Y", 		HEIST_ANIM_TV_F_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_TV_F_2_ROT_Z", 		HEIST_ANIM_TV_F_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_M_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_M_1_X", 		HEIST_ANIM_WINDOW_M_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_M_1_Y", 		HEIST_ANIM_WINDOW_M_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_M_1_Z", 		HEIST_ANIM_WINDOW_M_1_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_M_1_ROT_X", 	HEIST_ANIM_WINDOW_M_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_M_1_ROT_Y", 	HEIST_ANIM_WINDOW_M_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_M_1_ROT_Z", 	HEIST_ANIM_WINDOW_M_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_M_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_M_2_X", 		HEIST_ANIM_WINDOW_M_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_M_2_Y", 		HEIST_ANIM_WINDOW_M_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_M_2_Z", 		HEIST_ANIM_WINDOW_M_2_Z, -1000.0, 1000.0, 0.001)
					                                                                    
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_M_2_ROT_X", 	HEIST_ANIM_WINDOW_M_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_M_2_ROT_Y", 	HEIST_ANIM_WINDOW_M_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_M_2_ROT_Z", 	HEIST_ANIM_WINDOW_M_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_F_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_F_1_X", 		HEIST_ANIM_WINDOW_F_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_F_1_Y", 		HEIST_ANIM_WINDOW_F_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_F_1_Z", 		HEIST_ANIM_WINDOW_F_1_Z, -1000.0, 1000.0, 0.001)
					                                                                   
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_F_1_ROT_X", 	HEIST_ANIM_WINDOW_F_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_F_1_ROT_Y", 	HEIST_ANIM_WINDOW_F_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_F_1_ROT_Z", 	HEIST_ANIM_WINDOW_F_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_F_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_F_2_X", 		HEIST_ANIM_WINDOW_F_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_F_2_Y", 		HEIST_ANIM_WINDOW_F_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_F_2_Z", 		HEIST_ANIM_WINDOW_F_2_Z, -1000.0, 1000.0, 0.001)
					                                                                  
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_F_2_ROT_X", 	HEIST_ANIM_WINDOW_F_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_F_2_ROT_Y", 	HEIST_ANIM_WINDOW_F_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_WINDOW_F_2_ROT_Z", 	HEIST_ANIM_WINDOW_F_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_M_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_M_1_X", 	HEIST_ANIM_KITCHEN_M_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_M_1_Y", 	HEIST_ANIM_KITCHEN_M_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_M_1_Z", 	HEIST_ANIM_KITCHEN_M_1_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_M_1_ROT_X", HEIST_ANIM_KITCHEN_M_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_M_1_ROT_Y", HEIST_ANIM_KITCHEN_M_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_M_1_ROT_Z", HEIST_ANIM_KITCHEN_M_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_M_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_M_2_X", 	HEIST_ANIM_KITCHEN_M_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_M_2_Y", 	HEIST_ANIM_KITCHEN_M_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_M_2_Z", 	HEIST_ANIM_KITCHEN_M_2_Z, -1000.0, 1000.0, 0.001)
					                                                                    
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_M_2_ROT_X", HEIST_ANIM_KITCHEN_M_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_M_2_ROT_Y", HEIST_ANIM_KITCHEN_M_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_M_2_ROT_Z", HEIST_ANIM_KITCHEN_M_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_F_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_F_1_X", 	HEIST_ANIM_KITCHEN_F_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_F_1_Y", 	HEIST_ANIM_KITCHEN_F_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_F_1_Z", 	HEIST_ANIM_KITCHEN_F_1_Z, -1000.0, 1000.0, 0.001)
					                                                                   
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_F_1_ROT_X", HEIST_ANIM_KITCHEN_F_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_F_1_ROT_Y", HEIST_ANIM_KITCHEN_F_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_F_1_ROT_Z", HEIST_ANIM_KITCHEN_F_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_F_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_F_2_X", 	HEIST_ANIM_KITCHEN_F_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_F_2_Y", 	HEIST_ANIM_KITCHEN_F_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_F_2_Z", 	HEIST_ANIM_KITCHEN_F_2_Z, -1000.0, 1000.0, 0.001)
					                                                                  
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_F_2_ROT_X", HEIST_ANIM_KITCHEN_F_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_F_2_ROT_Y", HEIST_ANIM_KITCHEN_F_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_KITCHEN_F_2_ROT_Z", HEIST_ANIM_KITCHEN_F_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
								
				
		
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Buisness high-end 1") 
			
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_PHONE") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_PHONE_X", 			HEIST_ANIM_BUS_PHONE_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_PHONE_Y", 			HEIST_ANIM_BUS_PHONE_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_PHONE_Z", 			HEIST_ANIM_BUS_PHONE_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_PHONE_ROT_X", 		HEIST_ANIM_BUS_PHONE_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_PHONE_ROT_Y", 		HEIST_ANIM_BUS_PHONE_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_PHONE_ROT_Z", 		HEIST_ANIM_BUS_PHONE_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
		
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_WHISKY_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WHISKY_1_X", 		HEIST_ANIM_BUS_WHISKY_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WHISKY_1_Y", 		HEIST_ANIM_BUS_WHISKY_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WHISKY_1_Z", 		HEIST_ANIM_BUS_WHISKY_1_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WHISKY_1_ROT_X", 	HEIST_ANIM_BUS_WHISKY_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WHISKY_1_ROT_Y", 	HEIST_ANIM_BUS_WHISKY_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WHISKY_1_ROT_Z", 	HEIST_ANIM_BUS_WHISKY_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_WHISKY_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WHISKY_2_X", 		HEIST_ANIM_BUS_WHISKY_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WHISKY_2_Y", 		HEIST_ANIM_BUS_WHISKY_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WHISKY_2_Z", 		HEIST_ANIM_BUS_WHISKY_2_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WHISKY_2_ROT_X", 	HEIST_ANIM_BUS_WHISKY_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WHISKY_2_ROT_Y", 	HEIST_ANIM_BUS_WHISKY_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WHISKY_2_ROT_Z", 	HEIST_ANIM_BUS_WHISKY_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_WINE_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINE_1_X", 			HEIST_ANIM_BUS_WINE_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINE_1_Y", 			HEIST_ANIM_BUS_WINE_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINE_1_Z", 			HEIST_ANIM_BUS_WINE_1_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINE_1_ROT_X", 		HEIST_ANIM_BUS_WINE_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINE_1_ROT_Y", 		HEIST_ANIM_BUS_WINE_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINE_1_ROT_Z", 		HEIST_ANIM_BUS_WINE_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_WINE_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINE_2_X", 			HEIST_ANIM_BUS_WINE_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINE_2_Y", 			HEIST_ANIM_BUS_WINE_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINE_2_Z", 			HEIST_ANIM_BUS_WINE_2_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINE_2_ROT_X", 		HEIST_ANIM_BUS_WINE_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINE_2_ROT_Y", 		HEIST_ANIM_BUS_WINE_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINE_2_ROT_Z", 		HEIST_ANIM_BUS_WINE_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_COUCH_M_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_M_1_X", 		HEIST_ANIM_BUS_COUCH_M_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_M_1_Y", 		HEIST_ANIM_BUS_COUCH_M_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_M_1_Z", 		HEIST_ANIM_BUS_COUCH_M_1_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_M_1_ROT_X", 	HEIST_ANIM_BUS_COUCH_M_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_M_1_ROT_Y", 	HEIST_ANIM_BUS_COUCH_M_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_M_1_ROT_Z", 	HEIST_ANIM_BUS_COUCH_M_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_COUCH_M_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_M_2_X", 		HEIST_ANIM_BUS_COUCH_M_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_M_2_Y", 		HEIST_ANIM_BUS_COUCH_M_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_M_2_Z", 		HEIST_ANIM_BUS_COUCH_M_2_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_M_2_ROT_X", 	HEIST_ANIM_BUS_COUCH_M_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_M_2_ROT_Y", 	HEIST_ANIM_BUS_COUCH_M_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_M_2_ROT_Z", 	HEIST_ANIM_BUS_COUCH_M_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_COUCH_F_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_F_1_X", 		HEIST_ANIM_BUS_COUCH_F_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_F_1_Y", 		HEIST_ANIM_BUS_COUCH_F_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_F_1_Z", 		HEIST_ANIM_BUS_COUCH_F_1_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_F_1_ROT_X", 	HEIST_ANIM_BUS_COUCH_F_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_F_1_ROT_Y", 	HEIST_ANIM_BUS_COUCH_F_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_F_1_ROT_Z", 	HEIST_ANIM_BUS_COUCH_F_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_COUCH_F_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_F_2_X", 		HEIST_ANIM_BUS_COUCH_F_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_F_2_Y", 		HEIST_ANIM_BUS_COUCH_F_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_F_2_Z", 		HEIST_ANIM_BUS_COUCH_F_2_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_F_2_ROT_X", 	HEIST_ANIM_BUS_COUCH_F_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_F_2_ROT_Y", 	HEIST_ANIM_BUS_COUCH_F_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_COUCH_F_2_ROT_Z", 	HEIST_ANIM_BUS_COUCH_F_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_TV_M_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_M_1_X", 			HEIST_ANIM_BUS_TV_M_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_M_1_Y", 			HEIST_ANIM_BUS_TV_M_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_M_1_Z", 			HEIST_ANIM_BUS_TV_M_1_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_M_1_ROT_X", 		HEIST_ANIM_BUS_TV_M_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_M_1_ROT_Y", 		HEIST_ANIM_BUS_TV_M_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_M_1_ROT_Z", 		HEIST_ANIM_BUS_TV_M_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_TV_M_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_M_2_X", 			HEIST_ANIM_BUS_TV_M_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_M_2_Y", 			HEIST_ANIM_BUS_TV_M_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_M_2_Z", 			HEIST_ANIM_BUS_TV_M_2_Z, -1000.0, 1000.0, 0.001)
					                                                       	             
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_M_2_ROT_X", 		HEIST_ANIM_BUS_TV_M_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_M_2_ROT_Y", 		HEIST_ANIM_BUS_TV_M_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_M_2_ROT_Z", 		HEIST_ANIM_BUS_TV_M_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_TV_F_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_F_1_X", 			HEIST_ANIM_BUS_TV_F_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_F_1_Y", 			HEIST_ANIM_BUS_TV_F_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_F_1_Z", 			HEIST_ANIM_BUS_TV_F_1_Z, -1000.0, 1000.0, 0.001)
					                                                       	            
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_F_1_ROT_X", 		HEIST_ANIM_BUS_TV_F_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_F_1_ROT_Y", 		HEIST_ANIM_BUS_TV_F_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_F_1_ROT_Z", 		HEIST_ANIM_BUS_TV_F_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_TV_F_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_F_2_X", 			HEIST_ANIM_BUS_TV_F_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_F_2_Y", 			HEIST_ANIM_BUS_TV_F_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_F_2_Z", 			HEIST_ANIM_BUS_TV_F_2_Z, -1000.0, 1000.0, 0.001)
					                                                       	           
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_F_2_ROT_X", 		HEIST_ANIM_BUS_TV_F_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_F_2_ROT_Y", 		HEIST_ANIM_BUS_TV_F_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_TV_F_2_ROT_Z", 		HEIST_ANIM_BUS_TV_F_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_M_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_M_1_X", 		HEIST_ANIM_BUS_WINDOW_M_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_M_1_Y", 		HEIST_ANIM_BUS_WINDOW_M_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_M_1_Z", 		HEIST_ANIM_BUS_WINDOW_M_1_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_M_1_ROT_X", 	HEIST_ANIM_BUS_WINDOW_M_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_M_1_ROT_Y", 	HEIST_ANIM_BUS_WINDOW_M_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_M_1_ROT_Z", 	HEIST_ANIM_BUS_WINDOW_M_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_M_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_M_2_X", 		HEIST_ANIM_BUS_WINDOW_M_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_M_2_Y", 		HEIST_ANIM_BUS_WINDOW_M_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_M_2_Z", 		HEIST_ANIM_BUS_WINDOW_M_2_Z, -1000.0, 1000.0, 0.001)
					                                                                    
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_M_2_ROT_X", 	HEIST_ANIM_BUS_WINDOW_M_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_M_2_ROT_Y", 	HEIST_ANIM_BUS_WINDOW_M_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_M_2_ROT_Z", 	HEIST_ANIM_BUS_WINDOW_M_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_F_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_F_1_X", 		HEIST_ANIM_BUS_WINDOW_F_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_F_1_Y", 		HEIST_ANIM_BUS_WINDOW_F_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_F_1_Z", 		HEIST_ANIM_BUS_WINDOW_F_1_Z, -1000.0, 1000.0, 0.001)
					                                                                   
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_F_1_ROT_X", 	HEIST_ANIM_BUS_WINDOW_F_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_F_1_ROT_Y", 	HEIST_ANIM_BUS_WINDOW_F_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_F_1_ROT_Z", 	HEIST_ANIM_BUS_WINDOW_F_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_WINDOW_F_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_F_2_X", 		HEIST_ANIM_BUS_WINDOW_F_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_F_2_Y", 		HEIST_ANIM_BUS_WINDOW_F_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_F_2_Z", 		HEIST_ANIM_BUS_WINDOW_F_2_Z, -1000.0, 1000.0, 0.001)
					                                                                  
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_F_2_ROT_X", 	HEIST_ANIM_BUS_WINDOW_F_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_F_2_ROT_Y", 	HEIST_ANIM_BUS_WINDOW_F_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_WINDOW_F_2_ROT_Z", 	HEIST_ANIM_BUS_WINDOW_F_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_M_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_M_1_X", 	HEIST_ANIM_BUS_KITCHEN_M_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_M_1_Y", 	HEIST_ANIM_BUS_KITCHEN_M_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_M_1_Z", 	HEIST_ANIM_BUS_KITCHEN_M_1_Z, -1000.0, 1000.0, 0.001)
					
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_M_1_ROT_X", HEIST_ANIM_BUS_KITCHEN_M_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_M_1_ROT_Y", HEIST_ANIM_BUS_KITCHEN_M_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_M_1_ROT_Z", HEIST_ANIM_BUS_KITCHEN_M_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_M_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_M_2_X", 	HEIST_ANIM_BUS_KITCHEN_M_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_M_2_Y", 	HEIST_ANIM_BUS_KITCHEN_M_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_M_2_Z", 	HEIST_ANIM_BUS_KITCHEN_M_2_Z, -1000.0, 1000.0, 0.001)
					                                                                    
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_M_2_ROT_X", HEIST_ANIM_BUS_KITCHEN_M_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_M_2_ROT_Y", HEIST_ANIM_BUS_KITCHEN_M_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_M_2_ROT_Z", HEIST_ANIM_BUS_KITCHEN_M_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_F_1") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_F_1_X", 	HEIST_ANIM_BUS_KITCHEN_F_1_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_F_1_Y", 	HEIST_ANIM_BUS_KITCHEN_F_1_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_F_1_Z", 	HEIST_ANIM_BUS_KITCHEN_F_1_Z, -1000.0, 1000.0, 0.001)
					                                                                   
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_F_1_ROT_X", HEIST_ANIM_BUS_KITCHEN_F_1_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_F_1_ROT_Y", HEIST_ANIM_BUS_KITCHEN_F_1_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_F_1_ROT_Z", HEIST_ANIM_BUS_KITCHEN_F_1_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()

				START_WIDGET_GROUP("MP_PROP_ELEMENT_HEIST_ANIM_KITCHEN_F_2") 
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_F_2_X", 	HEIST_ANIM_BUS_KITCHEN_F_2_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_F_2_Y", 	HEIST_ANIM_BUS_KITCHEN_F_2_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_F_2_Z", 	HEIST_ANIM_BUS_KITCHEN_F_2_Z, -1000.0, 1000.0, 0.001)
					                                                                  
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_F_2_ROT_X", HEIST_ANIM_BUS_KITCHEN_F_2_ROT_X, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_F_2_ROT_Y", HEIST_ANIM_BUS_KITCHEN_F_2_ROT_Y, -1000.0, 1000.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("HEIST_ANIM_BUS_KITCHEN_F_2_ROT_Z", HEIST_ANIM_BUS_KITCHEN_F_2_ROT_Z, -1000.0, 1000.0, 0.001)
				STOP_WIDGET_GROUP()
		
			STOP_WIDGET_GROUP()
		
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	
	CLEAR_CURRENT_WIDGET_GROUP(DEBUG_ParentWidgets)
ENDPROC
#ENDIF


/// PURPOSE:
///    Check if the passed in heist team number has the minimum required amount of players or more.
/// PARAMS:
///    iTeam - Team to check.
/// RETURNS:
///    TRUE if minimum requirements have been met.
FUNC BOOL HAS_MET_MINIMUM_TEAM_REQUIREMENTS(INT iTeam)

	INT iTeamTotals[FMMC_MAX_TEAMS]
	GET_ROLE_TOTAL_FROM_TEAMS(iTeamTotals)	
	
	IF iTeamTotals[iTeam] < g_FMMC_STRUCT.iNumPlayersPerTeam[iTeam]
		RETURN FALSE
	ENDIF 
	
	RETURN TRUE

ENDFUNC 


/// PURPOSE:
///    Get the camera index from the help text state.
/// RETURNS:
///    
FUNC INT GET_HEIST_PLANNING_CAMERA_LOCATION_INDEX()

	SWITCH g_HeistPlanningClient.sHelpTextData.eHelpFlowState
		DEFAULT								RETURN INVALID_HEIST_DATA
		CASE HEIST_HELP_FLOW_OVERVIEW 		RETURN ci_HEIST_CAMERA_POS_OVERVIEW
		CASE HEIST_HELP_FLOW_PIE_CHART 		RETURN ci_HEIST_CAMERA_POS_PIE_CHART
		CASE HEIST_HELP_FLOW_TABLE 			RETURN ci_HEIST_CAMERA_POS_BOARD_CLOSE
		CASE HEIST_HELP_FLOW_PLAYER_CARD	RETURN ci_HEIST_CAMERA_POS_STATS
		CASE HEIST_HELP_FLOW_TODO_LIST	 	RETURN ci_HEIST_CAMERA_POS_TODO_LIST
		CASE HEIST_HELP_FLOW_MAP_TUTORIAL	RETURN ci_HEIST_CAMERA_POS_MAP_TUTORIAL
	ENDSWITCH

ENDFUNC


/// PURPOSE:
///    Update the camera only. This can be used as a "read only" mode for the help text introduction.
PROC UPDATE_HEIST_PLANNING_CAMERA()

	// Ensure the camera keeps moving smoothly to the target destination.
	MAINTAIN_SMOOTH_CAM_TRANSITION(g_HeistPlanningClient.sBoardCamData, FALSE, FALSE)
	
ENDPROC


/// PURPOSE:
///    Clear the map highlights. Map highlights are used to signify the heist task being examined (such as changing a pin colour to something different).
PROC CLEAR_ALL_MAP_HIGHLIGHTS()

	// Firstly reset all previous highlighting / selection indicators.
	INT iTotalPins, iPinPointer, index
	
	FOR index = 0 TO (FMMC_MAX_RULES-1)	
			
		iTotalPins = g_HeistPlanningClient.sBoardData.iMapPins[index] 
		
		FOR iPinPointer = (index * ci_HEIST_MAP_ITEM_ID_MODIFIER) TO iTotalPins
		
			IF iTotalPins - iPinPointer > 0

				SET_HEIST_MAP_COLOUR_PIN(g_HeistSharedClient.PlanningMapIndex, iPinPointer, 255, 255, 255, 100) 
				
			ENDIF
			
		ENDFOR

	ENDFOR
	
//	#IF IS_DEBUG_BUILD
//		PRINTLN("[AMEC][HEIST_LAUNCH] - CLEAR_ALL_MAP_HIGHLIGHTS - Cleared all highlights on the planning map.")
//	#ENDIF

ENDPROC


PROC RESET_PLAYER_BITSET_SYNC_MONITOR()

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - RESET_PLAYER_BITSET_SYNC_MONITOR - Resetting net status sync progress.")
	#ENDIF

	g_HeistPlanningClient.bPlayerNetStatusInSync = FALSE
	g_HeistPlanningClient.iPlayerLoopIndex = 0
	
ENDPROC


/// PURPOSE:
///    Reaplce and existing playerOrder array with fresh data.
/// PARAMS:
///    iTargetArray - Old array to replace.
PROC BUILD_PLAYER_ORDER_LIST(INT &iTargetArray[])

	PLAYER_INDEX thisPlayer = INVALID_PLAYER_INDEX()
	INT index
	
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		iTargetArray[index] = -1
	ENDFOR
	
	INT iPointer = 0 // This is the array tracker - it has to be different to the index because this manages the order of who's in the heist.

	FOR index = 0 TO (NUM_NETWORK_PLAYERS-1)
	    thisPlayer = INT_TO_PLAYERINDEX(index)
	    IF (IS_NET_PLAYER_OK(thisPlayer))
			IF thisPlayer <> INVALID_PLAYER_INDEX()
				IF NOT IS_THIS_PLAYER_SCTV_OR_JOINING_AS_SCTV(thisPlayer) 
					IF iPointer > (MAX_HEIST_PLAYER_SLOTS-1)
						SCRIPT_ASSERT("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_SERVER_CONFIG - ERROR! Too many players are attempting to enter the heist planning board. Tell Alastair.")
					ELSE
						// Add player to the player order array.
				        iTargetArray[iPointer] = NATIVE_TO_INT(thisPlayer)
						iPointer++ // Player added to array, manually increment the pointer.
					ENDIF
				ENDIF
		    ENDIF
		ENDIF
	ENDFOR

ENDPROC


/// PURPOSE:
///    Create player 0 player card. This is the only card that is required to be initialised, all others only require updates after this init.
PROC HEIST_CREATE_INITIAL_PLAYER_CARD()

	TEXT_LABEL_15	tlPlayerRole
	TEXT_LABEL_23 	tlPortrait
	TEXT_LABEL_31	tlPlayerTitle
	STRING			sPlayerRating, sPlayerName
	INT				iPlayerStamina, iPlayerShooting, iPlayerStealth, iPlayerFlying, iPlayerDriving, iPlayerMentalState, iPlayerRank, iPlayerSlot
	BOOL			bAccessPlane, bAccessHeli, bAccessBoat, bAccessCar
	FLOAT			fPlayerKillDeathRatio

	// Initialise the variables in the order in which they are passed into the set player card procedure.

	// Because this is the INIT of the player card, we only init the leader.
	iPlayerSlot = 0
	
	// Leader's name.
	sPlayerName = GET_PLAYER_NAME(GlobalServerBD_HeistPlanning.LeaderIndex)

	// Get the players respect level.
	iPlayerRank = GET_PLAYER_RANK(PLAYER_ID())
	
	// Retrieve role informations
	INT i = 0 // Empty int, not used.
	tlPlayerRole = GET_ROLE_INFO_FOR_PLAYER(PLAYER_ID(), i, ci_HEIST_LEADER)
	
	// Retrieve the portrait for the player.
	tlPortrait = GET_HEIST_MEMBER_HEADSHOT(PLAYER_ID())
	
	// HARDWARE ACCESS
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].scoreData.iVehicleAccess, STAT_CARD_PLANE_UNLOCK)
		bAccessPlane = TRUE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].scoreData.iVehicleAccess, STAT_CARD_HELI_UNLOCK)
		bAccessHeli = TRUE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].scoreData.iVehicleAccess, STAT_CARD_BOAT_UNLOCK)
		bAccessBoat = TRUE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].scoreData.iVehicleAccess, STAT_CARD_PERSONAL_CAR_UNLOCK)
		bAccessCar = TRUE
	ENDIF
	
	// KDR as float.
	fPlayerKillDeathRatio = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].scoreData.fKDRatio
	
	// To get the player title, we need to get the corrected rank number.
	INT iTempRank = GET_RANK_AS_NUMBER_FROM_0_TO_20(iPlayerRank)
	// Now build the localised text label using the corrected rank as the incrementor.
	tlPlayerTitle = RETURN_PERSONAL_RANK_STRING(iTempRank, TEAM_FREEMODE)
	
	IF NOT GET_PLAYER_BAD_SPORT_VALUE(PLAYER_ID())
		sPlayerRating = "PCARD_CLEAN_PLAYER"
	ELSE
		sPlayerRating = "PCARD_BAD_SPORT"
	ENDIF
	
	iPlayerStamina = GlobalplayerBD_Stats[NATIVE_TO_INT(PLAYER_ID())].iStatStamina
	iPlayerShooting = GlobalplayerBD_Stats[NATIVE_TO_INT(PLAYER_ID())].iStatShooting
	iPlayerStealth = GlobalplayerBD_Stats[NATIVE_TO_INT(PLAYER_ID())].iStatStealth
	iPlayerFlying = GlobalplayerBD_Stats[NATIVE_TO_INT(PLAYER_ID())].iStatFlying
	iPlayerDriving = GlobalplayerBD_Stats[NATIVE_TO_INT(PLAYER_ID())].iStatDriving
	iPlayerMentalState = GlobalplayerBD_Stats[NATIVE_TO_INT(PLAYER_ID())].iStatMentalState

	SET_HEIST_PLANNING_PLAYER_CARD(	g_HeistSharedClient.PlanningBoardIndex, 
									iPlayerSlot, sPlayerName, iPlayerRank, tlPlayerRole, 
									tlPortrait, bAccessPlane, bAccessHeli, bAccessBoat,
									bAccessCar, fPlayerKillDeathRatio, tlPlayerTitle, sPlayerRating, 
									iPlayerStamina, iPlayerShooting, iPlayerStealth, iPlayerFlying, 
									iPlayerDriving, iPlayerMentalState)

ENDPROC


/// PURPOSE:
///    Create the heist member withactual data. This oly happens once for each member when the board inits.
/// PARAMS:
///    index - The number for the player list. 0 = leader. 
PROC HEIST_CREATE_INITIAL_MEMBER(INT index)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_CREATE_INITIAL_MEMBER - Creating initial heist member. No.: ", index)
	#ENDIF

	PLAYER_INDEX playerID

	STRING sStatus
	TEXT_LABEL_15 tlCodeName
	TEXT_LABEL_15 tlPlayerRole
	TEXT_LABEL_23 tlPortrait
	
	INT iRoleIcon
	INT iStatusIcon
	INT iCutPercent
	INT iRank

	playerID = INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index])
	

	// Get the players respect level.
	iRank = GET_PLAYER_RANK(playerID)
	// Retrieve the portrait for the player.
	tlPortrait = GET_HEIST_MEMBER_HEADSHOT(playerID)
	// Retrieve role informations
	tlPlayerRole = GET_ROLE_INFO_FOR_PLAYER(playerID, iRoleIcon, index)
	// Retrieve status of player (either READY or NOT READY);
	sStatus = GET_STATUS_INFO_FOR_PLAYER(playerID, iStatusIcon)
	// Get specific player cut percentage.
	iCutPercent = GET_CUT_INFO_FOR_PLAYER(index)
	
//	// Get if the heist member gender
//	bIsMale = GET_HEIST_MEMBER_IS_MALE(playerID)
//	// Get the slot's specific codename.
//	tlCodeName = GET_CODENAME_LABEL(INT_TO_ENUM(HEIST_CODENAMES, index), bIsMale)  // 1865660 - Remove codenames.
	
	// Feed all of the data just gathered into the scaleform update method.
	SET_HEIST_PLANNING_CREW_SLOT(g_HeistSharedClient.PlanningBoardIndex, index, GET_PLAYER_NAME(playerID), iRank, tlPortrait, tlPlayerRole, iRoleIcon, sStatus, iStatusIcon, iCutPercent, tlCodeName)

ENDPROC


PROC GET_COLOUR_FOR_RULE(HEIST_MAP_RULE_COLOUR &sRuleColour, INT iTeam, INT iRuleNumber)
	
//	sRuleColour.iRed = 200
//	sRuleColour.iGreen = 200
//	sRuleColour.iBlue = 50
	
	SWITCH g_HeistPlanningClient.iObjectiveRule[iTeam][iRuleNumber]
	
		DEFAULT
		
			sRuleColour.iRed = 255
			sRuleColour.iGreen = 255
			sRuleColour.iBlue = 255
		
			BREAK
	
		CASE FMMC_OBJECTIVE_LOGIC_GET_AND_DELIVER
		
			IF g_HeistPlanningClient.iObjectiveTarget[iTeam][iRuleNumber] = INT_TARGET_VEHICLE
			OR g_HeistPlanningClient.iObjectiveTarget[iTeam][iRuleNumber] = INT_TARGET_PED
				sRuleColour.iRed = 70
				sRuleColour.iGreen = 70
				sRuleColour.iBlue = 255
			ELIF g_HeistPlanningClient.iObjectiveTarget[iTeam][iRuleNumber] = INT_TARGET_OBJECT
				sRuleColour.iRed = 70
				sRuleColour.iGreen = 255
				sRuleColour.iBlue = 70
			ENDIF
			
			BREAK
		CASE FMMC_OBJECTIVE_LOGIC_KILL 
		
			sRuleColour.iRed = 255
			sRuleColour.iGreen = 70
			sRuleColour.iBlue = 70
			
			BREAK 
		CASE FMMC_OBJECTIVE_LOGIC_DAMAGE
		CASE FMMC_OBJECTIVE_LOGIC_LEAVE_ENTITY
		
			sRuleColour.iRed = 255
			sRuleColour.iGreen = 70
			sRuleColour.iBlue = 70
			
			BREAK 
		
		CASE FMMC_OBJECTIVE_LOGIC_PED_GOTO_LOCATION
			sRuleColour.iRed = 70
			sRuleColour.iGreen = 70
			sRuleColour.iBlue = 255
		BREAK
		
		CASE FMMC_OBJECTIVE_LOGIC_PROTECT	

			IF g_HeistPlanningClient.iObjectiveTarget[iTeam][iRuleNumber] = INT_TARGET_VEHICLE
			OR g_HeistPlanningClient.iObjectiveTarget[iTeam][iRuleNumber] = INT_TARGET_PED
				sRuleColour.iRed = 70
				sRuleColour.iGreen = 70
				sRuleColour.iBlue = 255
			ELIF g_HeistPlanningClient.iObjectiveTarget[iTeam][iRuleNumber] = INT_TARGET_OBJECT
				sRuleColour.iRed = 70
				sRuleColour.iGreen = 255
				sRuleColour.iBlue = 70
			ENDIF
			
			BREAK

		CASE FMMC_OBJECTIVE_LOGIC_GO_TO	
		
			IF g_HeistPlanningClient.iObjectiveTarget[iTeam][iRuleNumber] = INT_TARGET_VEHICLE
			OR g_HeistPlanningClient.iObjectiveTarget[iTeam][iRuleNumber] = INT_TARGET_PED
				sRuleColour.iRed = 70
				sRuleColour.iGreen = 70
				sRuleColour.iBlue = 255
			ELIF g_HeistPlanningClient.iObjectiveTarget[iTeam][iRuleNumber] = INT_TARGET_OBJECT
				sRuleColour.iRed = 70
				sRuleColour.iGreen = 255
				sRuleColour.iBlue = 70
			ELIF g_HeistPlanningClient.iObjectiveTarget[iTeam][iRuleNumber] = INT_TARGET_LOCATION
				sRuleColour.iRed = 200
				sRuleColour.iGreen = 200
				sRuleColour.iBlue = 50
			ENDIF
		
			BREAK
		CASE FMMC_OBJECTIVE_LOGIC_CAPTURE	
		
			sRuleColour.iRed = 200
			sRuleColour.iGreen = 200
			sRuleColour.iBlue = 50
		
			BREAK

		CASE FMMC_OBJECTIVE_LOGIC_GET_AND_HOLD	
		
			IF g_HeistPlanningClient.iObjectiveTarget[iTeam][iRuleNumber] = INT_TARGET_VEHICLE
			OR g_HeistPlanningClient.iObjectiveTarget[iTeam][iRuleNumber] = INT_TARGET_PED
				sRuleColour.iRed = 70
				sRuleColour.iGreen = 70
				sRuleColour.iBlue = 255
			ELIF g_HeistPlanningClient.iObjectiveTarget[iTeam][iRuleNumber] = INT_TARGET_OBJECT
				sRuleColour.iRed = 70
				sRuleColour.iGreen = 255
				sRuleColour.iBlue = 70
			ENDIF
		
			BREAK
		CASE FMMC_OBJECTIVE_LOGIC_PHOTO	
		
			IF g_HeistPlanningClient.iObjectiveTarget[iTeam][iRuleNumber] = INT_TARGET_VEHICLE
			OR g_HeistPlanningClient.iObjectiveTarget[iTeam][iRuleNumber] = INT_TARGET_PED
				sRuleColour.iRed = 70
				sRuleColour.iGreen = 70
				sRuleColour.iBlue = 255
			ELIF g_HeistPlanningClient.iObjectiveTarget[iTeam][iRuleNumber] = INT_TARGET_OBJECT
				sRuleColour.iRed = 70
				sRuleColour.iGreen = 255
				sRuleColour.iBlue = 70
			ENDIF
		
			BREAK
		CASE FMMC_OBJECTIVE_LOGIC_MINIGAME	

			sRuleColour.iRed = 70
			sRuleColour.iGreen = 255
			sRuleColour.iBlue = 70
	
			BREAK // green
	ENDSWITCH
	
ENDPROC


FUNC INT GET_HEIST_MULTI_RULE_ENTITY_DETAILS(INT iObjectiveType, INT iObjectiveID, INT iTeam, INT iRuleNumber)
	
	INT iextraObjectiveNum, index

	// Loop through all potential entites that have multiple rules
	FOR iextraObjectiveNum = 0 TO ( MAX_NUM_EXTRA_OBJECTIVE_ENTITIES-1 )
		// If the type matches the one passed
		IF g_FMMC_STRUCT.iExtraObjectiveEntityType[iextraObjectiveNum] = iObjectiveType
		AND g_FMMC_STRUCT.iExtraObjectiveEntityId[iextraObjectiveNum] = iObjectiveID
	
			FOR index = 0 TO (MAX_NUM_EXTRA_OBJECTIVE_PER_ENTITY-1)
	
				IF g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][index][iTeam] < FMMC_MAX_RULES
				AND g_FMMC_STRUCT.iExtraObjectivePriority[iextraObjectiveNum][index][iTeam] >= 0 // and it's not -1
					
					INT iAdditionalObjectivePriority = g_FMMC_STRUCT.iExtraObjectivePriority[ iextraObjectiveNum ][ index ][ iTeam ]
						
//					PRINTLN("[AMEC][HEIST_LAUNCH] - GET_HEIST_MULTI_RULE_ENTITY_DETAILS - Found entity on rule: ", iAdditionalObjectivePriority," on team: ",iTeam,". iextraObjectiveNum: ", iextraObjectiveNum, ", param iRuleNumber: ",iRuleNumber)
					
					IF iAdditionalObjectivePriority = iRuleNumber
						PRINTLN("[AMEC][HEIST_LAUNCH] - GET_HEIST_MULTI_RULE_ENTITY_DETAILS - !! Found additional entity on passed in rule, returning iAdditionalObjectivePriority: ", iAdditionalObjectivePriority)
						RETURN iAdditionalObjectivePriority
					ENDIF
					
				ENDIF
				
			ENDFOR
		ENDIF
	ENDFOR

	RETURN FMMC_PRIORITY_IGNORE

ENDFUNC



/// PURPOSE:
///    Using heist cloud data, create the map graphics. This is completely automated - do not attempt to modifty these at a later point
///    without the correct graphic ID or it will cause issues.
FUNC BOOL HEIST_START_CREATE_MAP_GRAPHICS(INT iTeam)

	// Firstly we have to clean all of the crap left on the board, and then clean all of the references held script side.
	CLEAN_HEIST_MAP()

	// Bounds check.
	IF iTeam < 0
	OR iTeam > (FMMC_MAX_TEAMS-1)
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_CREATE_MAP_GRAPHICS - WARNING! Team value passed in is out of bounds! Value: ", iTeam)
//		#ENDIF
		RETURN FALSE
	ENDIF

	INT currItem, index, iRuleIndex, iRulePointer, iMultiObjectivePointer, i
	FLOAT fX, fY
	VECTOR vPlayerPos
	
	// Offset each rule with their index * ci_HEIST_MAP_ITEM_ID_MODIFIER. If the modifier = 100, it gives 0, 100, 200, 300 etc.
	// This is used to create a unique ID for each item added thereafter. E.G. task 7's 3rd pin would be 603.
	FOR index = 0 TO (FMMC_MAX_RULES-1)
		g_HeistPlanningClient.sBoardData.iMapPins[index] = index * ci_HEIST_MAP_ITEM_ID_MODIFIER
		g_HeistPlanningClient.sBoardData.iMapText[index] = index * ci_HEIST_MAP_ITEM_ID_MODIFIER
		g_HeistPlanningClient.sBoardData.iMapPostIt[index] = index * ci_HEIST_MAP_ITEM_ID_MODIFIER
		g_HeistPlanningClient.sBoardData.iMapArrows[index] = index * ci_HEIST_MAP_ITEM_ID_MODIFIER
		g_HeistPlanningClient.sBoardData.iMapAreas[index] = index * ci_HEIST_MAP_ITEM_ID_MODIFIER
		g_HeistPlanningClient.sBoardData.iMapHighlights[index] = index * ci_HEIST_MAP_ITEM_ID_MODIFIER
		
		g_HeistPlanningClient.sCumPedLocations[index].X = INVALID_HEIST_DATA
		g_HeistPlanningClient.sCumPedLocations[index].Y = INVALID_HEIST_DATA
		g_HeistPlanningClient.sCumPedLocations[index].iCount = 0
		
		GET_COLOUR_FOR_RULE(g_HeistPlanningClient.sMapRuleColours[index], iTeam, index)
	
	ENDFOR
	
	PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS - ## Pre-process all mission ped vectors ##")

	// For each potential maximum of a specific item, extract the relevant locations for the heist board.
	REPEAT FMMC_MAX_PEDS currItem
		IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[currItem].iPriority[iTeam] < FMMC_MAX_RULES 
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[g_FMMC_STRUCT_ENTITIES.sPlacedPed[currItem].iPriority[iTeam]], ciBS_RULE_HIDE_ON_PLANNING_BOARD) 
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[currItem].iRule[iTeam]!= FMMC_OBJECTIVE_LOGIC_NONE
					IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedPed[currItem].vPos)
					
						iRuleIndex = g_FMMC_STRUCT_ENTITIES.sPlacedPed[currItem].iPriority[iTeam]
						PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS - Saving PED at index: ", currItem, ", iRuleIndex: ", iRuleIndex)
						// Get an average 2D vector for ped positions.
						g_HeistPlanningClient.sCumPedLocations[iRuleIndex].X += g_FMMC_STRUCT_ENTITIES.sPlacedPed[currItem].vPos.X
						g_HeistPlanningClient.sCumPedLocations[iRuleIndex].Y += g_FMMC_STRUCT_ENTITIES.sPlacedPed[currItem].vPos.Y
						g_HeistPlanningClient.sCumPedLocations[iRuleIndex].iCount++		
						PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS - Current number of peds for rule ",iRuleIndex,": ", g_HeistPlanningClient.sCumPedLocations[iRuleIndex].iCount)
					
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS - ## Start build map graphics for each rule ##")
	
	VECTOR 	vRulePriorityLocations[ci_MAX_MAP_POSTITS_PER_RULE]		//	Local array per rule to hold upto 5 postits
	INT 	iCurrentIndex	=	0									//	Current index of stored postit locs
	INT 	iPos			=	0									//	Counter

	FOR index = 0 TO (FMMC_MAX_RULES-1)
	
		REPEAT ci_MAX_MAP_POSTITS_PER_RULE iPos
			vRulePriorityLocations[iPos] = <<0.0, 0.0, 0.0>>
		ENDREPEAT
		iCurrentIndex	=	0
		iPos			=	0
		
		IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[index], ciBS_RULE_HIDE_ON_PLANNING_BOARD)
		
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitSetTwo[index], ciBS_RULE2_HIDE_ON_MAP)
		
				PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS - SHOW RULE: ", index)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[index])
				
					PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ... Textual description found, moving to add to map.")
					PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ... Text: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].tl63Objective[index])
					
					FLOAT fAvgX, fAvgY

					IF g_HeistPlanningClient.sCumPedLocations[index].X != INVALID_HEIST_DATA
					AND g_HeistPlanningClient.sCumPedLocations[index].Y != INVALID_HEIST_DATA
					
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ... Processing cumulative ped positions for rule: ", index, ". iCount: ", g_HeistPlanningClient.sCumPedLocations[index].iCount)
							PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ... CumValues: X=",g_HeistPlanningClient.sCumPedLocations[index].X, ", Y=",g_HeistPlanningClient.sCumPedLocations[index].Y)
						#ENDIF
						
						fAvgX = g_HeistPlanningClient.sCumPedLocations[index].X / g_HeistPlanningClient.sCumPedLocations[index].iCount
						fAvgY = g_HeistPlanningClient.sCumPedLocations[index].Y / g_HeistPlanningClient.sCumPedLocations[index].iCount
												
						BOOL	bTooClose	=	FALSE
						VECTOR	vCurrentLoc	=	<<fAvgX, fAvgY, 0.0>>
						IF iCurrentIndex   !=	0
							REPEAT 	iCurrentIndex	iPos
								IF FLAT_VDIST2(vCurrentLoc, vRulePriorityLocations[iPos]) < TO_SQUARED_FLOAT(ci_MIN_WORLD_SPACE_DIST_BETWEEN_POSITS)
									bTooClose = TRUE	//	If current location is too close to prev within rule set flag
								ENDIF
							ENDREPEAT
						ENDIF
						
						IF NOT bTooClose	//	If flag not set and not max num of postits per rule hit- add postit to map
						AND iCurrentIndex	<	ci_MAX_MAP_POSTITS_PER_RULE 
						
							PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ...... Adding PED for RULE: ",index)
							// ADD AREA.
							SET_HEIST_MAP_ADD_AREA(	g_HeistSharedClient.PlanningMapIndex, 
													g_HeistPlanningClient.sBoardData.iMapAreas[iRulePointer],
													fAvgX, fAvgY, cf_HEIST_MAP_PED_ZONE_RADIUS,
													g_HeistPlanningClient.sMapRuleColours[index].iRed, 
													g_HeistPlanningClient.sMapRuleColours[index].iGreen, 
													g_HeistPlanningClient.sMapRuleColours[index].iBlue, 50)
													
							g_HeistPlanningClient.sBoardData.iMapAreas[iRulePointer]++
							
							// ADD POST-IT
							SET_HEIST_MAP_ADD_POSTIT(	g_HeistSharedClient.PlanningMapIndex, 
														g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer],
														iRulePointer+1,
														fAvgX, fAvgY)
										
							g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer]++
							
							vRulePriorityLocations[iCurrentIndex]	=	vCurrentLoc		//	If valid location add it to the list of locations
							iCurrentIndex ++											//	Up the curr index ready for next loc
								
						ENDIF
					
					ENDIF
					
					REPEAT FMMC_MAX_GO_TO_LOCATIONS currItem
					
						iMultiObjectivePointer = GET_HEIST_MULTI_RULE_ENTITY_DETAILS(ciRULE_TYPE_GOTO, currItem, iTeam, index)
					
						IF iMultiObjectivePointer != FMMC_PRIORITY_IGNORE
						
							vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
									
							IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[index] = ciFMMC_DROP_OFF_TYPE_PROPERTY
							OR g_FMMC_STRUCT.sFMMCEndConditions[iteam].iDropOffType[index] = ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
								fX = vPlayerPos.X
								fY = vPlayerPos.Y
								PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ...... Player Coords - fX: ",fX,", fY: ",fY)
							ELSE
								fX = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[currItem].vLoc[0].X
								fY = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[currItem].vLoc[0].Y
								PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ...... Struct Coords - fX: ",fX,", fY: ",fY)
							ENDIF
													
							BOOL	bTooClose	=	FALSE
							VECTOR	vCurrentLoc	=	<<fX, fY, 0.0>>
							IF iCurrentIndex   !=	0
								REPEAT 	iCurrentIndex	iPos
									IF FLAT_VDIST2(vCurrentLoc, vRulePriorityLocations[iPos]) < TO_SQUARED_FLOAT(ci_MIN_WORLD_SPACE_DIST_BETWEEN_POSITS)
										bTooClose = TRUE
									ENDIF
								ENDREPEAT
							ENDIF
							
							IF NOT bTooClose
							AND iCurrentIndex	<	ci_MAX_MAP_POSTITS_PER_RULE 
						
								PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ...... Adding MULTI-RULE LOCATION for RULE: ",index,", Location index: ",currItem, ", multi-pointer: ", iMultiObjectivePointer)
								
								SET_HEIST_MAP_ADD_PIN(	g_HeistSharedClient.PlanningMapIndex, 
														g_HeistPlanningClient.sBoardData.iMapPins[iRulePointer], 
														0, 
														fX, fY,
														255, 255, 255, 100, 100)
														
								SET_HEIST_MAP_ADD_POSTIT(g_HeistSharedClient.PlanningMapIndex, 
														g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer], 
														iRulePointer+1,
														fX, fY)
														
								g_HeistPlanningClient.sBoardData.iMapPins[iRulePointer]++
								g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer]++
								
								fX = 0.0
								fY = 0.0
								
								vRulePriorityLocations[iCurrentIndex]	=	vCurrentLoc		//	If valid location add it to the list of locations
								iCurrentIndex ++											//	Up the curr index ready for next loc
									
							ENDIF
							
						ENDIF
					
						IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[currItem].iPriority[iTeam] = index
							IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[currItem].iRule[iTeam]!= FMMC_OBJECTIVE_LOGIC_NONE
								IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[currItem].vLoc[0])
								
									vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
									
									IF g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[index] = ciFMMC_DROP_OFF_TYPE_PROPERTY
									OR g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[index] = ciFMMC_DROP_OFF_TYPE_LEADER_GARAGE
										fX = vPlayerPos.X
										fY = vPlayerPos.Y
										PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ...... Player Coords - fX: ",fX,", fY: ",fY, ", Type: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[index])
									ELSE
										fX = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[currItem].vLoc[0].X
										fY = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[currItem].vLoc[0].Y
										PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ...... Struct Coords - fX: ",fX,", fY: ",fY, ", Type: ", g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iDropOffType[index])
									ENDIF
															
									BOOL	bTooClose	=	FALSE
									VECTOR	vCurrentLoc	=	<<fX, fY, 0.0>>
									IF iCurrentIndex   !=	0
										REPEAT 	iCurrentIndex	iPos
											IF FLAT_VDIST2(vCurrentLoc, vRulePriorityLocations[iPos]) < TO_SQUARED_FLOAT(ci_MIN_WORLD_SPACE_DIST_BETWEEN_POSITS)
												bTooClose = TRUE
											ENDIF
										ENDREPEAT
									ENDIF
									
									IF NOT bTooClose
									AND iCurrentIndex	<	ci_MAX_MAP_POSTITS_PER_RULE 
								
										PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ...... Adding LOCATION for RULE: ",index,", Location index: ",currItem)
										
										SET_HEIST_MAP_ADD_PIN(	g_HeistSharedClient.PlanningMapIndex, 
																g_HeistPlanningClient.sBoardData.iMapPins[iRulePointer], 
																0, 
																fX, fY,
																255, 255, 255, 100, 100)
																
										SET_HEIST_MAP_ADD_POSTIT(g_HeistSharedClient.PlanningMapIndex, 
																g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer], 
																iRulePointer+1,
																fX, fY)
																
										g_HeistPlanningClient.sBoardData.iMapPins[iRulePointer]++
										g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer]++
										
										fX = 0.0
										fY = 0.0
									
										vRulePriorityLocations[iCurrentIndex]	=	vCurrentLoc		//	If valid location add it to the list of locations
										iCurrentIndex ++											//	Up the curr index ready for next loc
										
									ENDIF										
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
			
					REPEAT FMMC_MAX_VEHICLES currItem
					
						iMultiObjectivePointer = GET_HEIST_MULTI_RULE_ENTITY_DETAILS(ciRULE_TYPE_VEHICLE, currItem, iTeam, index)
					
						IF iMultiObjectivePointer != FMMC_PRIORITY_IGNORE
													
							BOOL	bTooClose	=	FALSE
							VECTOR	vCurrentLoc	=	<<g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[currItem].vPos.X, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[currItem].vPos.Y, 0.0>>
							IF iCurrentIndex   !=	0
								REPEAT 	iCurrentIndex	iPos
									IF FLAT_VDIST2(vCurrentLoc, vRulePriorityLocations[iPos]) < TO_SQUARED_FLOAT(ci_MIN_WORLD_SPACE_DIST_BETWEEN_POSITS)
										bTooClose = TRUE
									ENDIF
								ENDREPEAT
							ENDIF
							
							IF NOT bTooClose
							AND iCurrentIndex	<	ci_MAX_MAP_POSTITS_PER_RULE 
						
								PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ...... Adding MULTI-RULE VEHICLE for RULE: ",index,", Vehicle index: ",currItem, ", multi-pointer: ", iMultiObjectivePointer)
								
								SET_HEIST_MAP_ADD_PIN(	g_HeistSharedClient.PlanningMapIndex, 
														g_HeistPlanningClient.sBoardData.iMapPins[iRulePointer], 
														0, 
														g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[currItem].vPos.X, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[currItem].vPos.Y,
														255, 255, 255, 100, 100)
														
								SET_HEIST_MAP_ADD_POSTIT(g_HeistSharedClient.PlanningMapIndex, 
														g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer],
														iRulePointer+1, // Derive base ID from unique.
														g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[currItem].vPos.X, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[currItem].vPos.Y)
							
								g_HeistPlanningClient.sBoardData.iMapPins[iRulePointer]++
								g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer]++
							
								vRulePriorityLocations[iCurrentIndex]	=	vCurrentLoc		//	If valid location add it to the list of locations
								iCurrentIndex ++											//	Up the curr index ready for next loc
								
							ENDIF	
						ENDIF
					
						IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[currItem].iPriority[iTeam] = index
							IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[currItem].iRule[iTeam]!= FMMC_OBJECTIVE_LOGIC_NONE
								IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[currItem].vPos) 
										
									BOOL	bTooClose	=	FALSE
									VECTOR	vCurrentLoc	=	<<g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[currItem].vPos.X, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[currItem].vPos.Y, 0.0>>
									IF iCurrentIndex   !=	0
										REPEAT 	iCurrentIndex	iPos
											IF FLAT_VDIST2(vCurrentLoc, vRulePriorityLocations[iPos]) < TO_SQUARED_FLOAT(ci_MIN_WORLD_SPACE_DIST_BETWEEN_POSITS)
												bTooClose = TRUE
											ENDIF
										ENDREPEAT
									ENDIF
									
									IF NOT bTooClose
									AND iCurrentIndex	<	ci_MAX_MAP_POSTITS_PER_RULE 
								
										PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ...... Adding VEHICLE for RULE: ",index,", Vehicle index: ",currItem)
										
										SET_HEIST_MAP_ADD_PIN(	g_HeistSharedClient.PlanningMapIndex, 
																g_HeistPlanningClient.sBoardData.iMapPins[iRulePointer], 
																0, 
																g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[currItem].vPos.X, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[currItem].vPos.Y,
																255, 255, 255, 100, 100)
																
										SET_HEIST_MAP_ADD_POSTIT(g_HeistSharedClient.PlanningMapIndex, 
																g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer],
																iRulePointer+1, // Derive base ID from unique.
																g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[currItem].vPos.X, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[currItem].vPos.Y)
									
										g_HeistPlanningClient.sBoardData.iMapPins[iRulePointer]++
										g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer]++
										
										vRulePriorityLocations[iCurrentIndex]	=	vCurrentLoc		//	If valid location add it to the list of locations
										iCurrentIndex ++											//	Up the curr index ready for next loc
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					REPEAT FMMC_MAX_NUM_OBJECTS currItem
						
						iMultiObjectivePointer = GET_HEIST_MULTI_RULE_ENTITY_DETAILS(ciRULE_TYPE_OBJECT, currItem, iTeam, index)
					
						IF iMultiObjectivePointer != FMMC_PRIORITY_IGNORE
								
							BOOL	bTooClose	=	FALSE
							VECTOR	vCurrentLoc	=	<<g_FMMC_STRUCT_ENTITIES.sPlacedObject[currItem].vPos.X, g_FMMC_STRUCT_ENTITIES.sPlacedObject[currItem].vPos.Y, 0.0>>
							IF iCurrentIndex   !=	0
								REPEAT 	iCurrentIndex	iPos
									IF FLAT_VDIST2(vCurrentLoc, vRulePriorityLocations[iPos]) < TO_SQUARED_FLOAT(ci_MIN_WORLD_SPACE_DIST_BETWEEN_POSITS)
										bTooClose = TRUE
									ENDIF
								ENDREPEAT
							ENDIF
							
							IF NOT bTooClose
							AND iCurrentIndex	<	ci_MAX_MAP_POSTITS_PER_RULE 
						
								PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ...... Adding MULTI-RULE OBJECT for RULE: ",index,", Object index: ",currItem, ", multi-pointer: ", iMultiObjectivePointer)
								
								SET_HEIST_MAP_ADD_PIN(	g_HeistSharedClient.PlanningMapIndex, 
														g_HeistPlanningClient.sBoardData.iMapPins[iRulePointer], 
														0, 
														g_FMMC_STRUCT_ENTITIES.sPlacedObject[currItem].vPos.X, g_FMMC_STRUCT_ENTITIES.sPlacedObject[currItem].vPos.Y,
														255, 255, 255, 100, 100)
														
								SET_HEIST_MAP_ADD_POSTIT(g_HeistSharedClient.PlanningMapIndex, 
														g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer], 
														iRulePointer+1, // Derive base ID from unique.
														g_FMMC_STRUCT_ENTITIES.sPlacedObject[currItem].vPos.X, g_FMMC_STRUCT_ENTITIES.sPlacedObject[currItem].vPos.Y)
														
								g_HeistPlanningClient.sBoardData.iMapPins[iRulePointer]++
								g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer]++
								
								vRulePriorityLocations[iCurrentIndex]	=	vCurrentLoc		//	If valid location add it to the list of locations
								iCurrentIndex ++											//	Up the curr index ready for next loc
									
							ENDIF
						ENDIF
					
						IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[currItem].iPriority[iTeam] = index
							IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[currItem].iRule[iTeam]!= FMMC_OBJECTIVE_LOGIC_NONE
								IF NOT IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sPlacedObject[currItem].vPos)
									
									BOOL	bTooClose	=	FALSE
									VECTOR	vCurrentLoc	=	<<g_FMMC_STRUCT_ENTITIES.sPlacedObject[currItem].vPos.X, g_FMMC_STRUCT_ENTITIES.sPlacedObject[currItem].vPos.Y, 0.0>>
									IF iCurrentIndex   !=	0
										REPEAT 	iCurrentIndex	iPos
											IF FLAT_VDIST2(vCurrentLoc, vRulePriorityLocations[iPos]) < TO_SQUARED_FLOAT(ci_MIN_WORLD_SPACE_DIST_BETWEEN_POSITS)
												bTooClose = TRUE
											ENDIF
										ENDREPEAT
									ENDIF
									
									IF NOT bTooClose
									AND iCurrentIndex	<	ci_MAX_MAP_POSTITS_PER_RULE 
								
										PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ...... Adding OBJECT for RULE: ",index,", Object index: ",currItem)
										
										SET_HEIST_MAP_ADD_PIN(	g_HeistSharedClient.PlanningMapIndex, 
																g_HeistPlanningClient.sBoardData.iMapPins[iRulePointer], 
																0, 
																g_FMMC_STRUCT_ENTITIES.sPlacedObject[currItem].vPos.X, g_FMMC_STRUCT_ENTITIES.sPlacedObject[currItem].vPos.Y,
																255, 255, 255, 100, 100)
																
										SET_HEIST_MAP_ADD_POSTIT(g_HeistSharedClient.PlanningMapIndex, 
																g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer], 
																iRulePointer+1, // Derive base ID from unique.
																g_FMMC_STRUCT_ENTITIES.sPlacedObject[currItem].vPos.X, g_FMMC_STRUCT_ENTITIES.sPlacedObject[currItem].vPos.Y)
																
										g_HeistPlanningClient.sBoardData.iMapPins[iRulePointer]++
										g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer]++
										
										vRulePriorityLocations[iCurrentIndex]	=	vCurrentLoc		//	If valid location add it to the list of locations
										iCurrentIndex ++											//	Up the curr index ready for next loc
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
										
					iRulePointer++
					
				ELSE
					PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ... ERROR! Rule is marked as SHOW, but contains NO TEXT! Skipping...")
//					SCRIPT_ASSERT("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS ... ERROR! Rule is marked as SHOW ON PLANNING, but contains NO OBJECTIVE TEXT! Please enter a bug for the mission owner.")
				ENDIF
			ELSE
				PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS - HIDE POST-IT: ", index)
			ENDIF
		ELSE
			PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS - HIDE RULE: ", index)
		ENDIF
	ENDFOR
		
	IF g_sHeistStrandMissionRules.bStrandMissionForHeistBoard	
	
		PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS - STRAND - ## starting additional map graphics placement ##")
	
		FOR index = 0 TO (MAX_HEIST_ADDITIONAL_RULES-1)
		
			FOR i = 0 TO (MAX_HEIST_ADDITIONAL_ENTITIES-1)
			
				IF g_HeistPlanningClient.sHeistExtaMapData[index].iType[iTeam][i] > 0
		
					PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS - STRAND - parsing map data for index[",index,", ",i,"]")
					
					IF ((index + iRulePointer) > (FMMC_MAX_RULES-1))
						SCRIPT_ASSERT("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS - STRAND - ERROR! Cannot parse additional map data as there is no more space in the graphics tracking array! Please enter a bug for the mission owner.")
						PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS - STRAND - ERROR! Index(",index,") + iRulePointer(",iRulePointer,") is > FMMC_MAX_RULES. No mmore space available to build graphics, exiting...")
						RETURN TRUE
					ELSE
						iRuleIndex = (index + iRulePointer)
					ENDIF
				
					BOOL	bTooClose	=	FALSE
					VECTOR	vCurrentLoc	=	<<g_HeistPlanningClient.sHeistExtaMapData[index].vLocation[iTeam][i].X, g_HeistPlanningClient.sHeistExtaMapData[index].vLocation[iTeam][i].Y, 0.0>>
					IF iCurrentIndex   !=	0
						REPEAT 	iCurrentIndex	iPos
							IF FLAT_VDIST2(vCurrentLoc, vRulePriorityLocations[iPos]) < TO_SQUARED_FLOAT(ci_MIN_WORLD_SPACE_DIST_BETWEEN_POSITS)
								bTooClose = TRUE
							ENDIF
						ENDREPEAT
					ENDIF
					
					IF NOT bTooClose
					AND iCurrentIndex	<	ci_MAX_MAP_POSTITS_PER_RULE 
				
						PRINTLN("[RBJ][POSTITS] - net_heist_planning - HEIST_START_CREATE_MAP_GRAPHICS - Init rule: ", index, " - DRAWING POSTIT AND PIN for: OBJ at: ", vCurrentLoc)
						
						SWITCH g_HeistPlanningClient.sHeistExtaMapData[index].iType[iTeam][i]
						
							CASE ci_HEIST_MAP_PIN_AND_POSTIT
							
								PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS - STRAND - ...... Adding additional PIN_AND_POSTIT data from rule: ",index,", entity index: ", i)
							
								// ADD PIN.
								SET_HEIST_MAP_ADD_PIN(	g_HeistSharedClient.PlanningMapIndex, 
														g_HeistPlanningClient.sBoardData.iMapPins[iRuleIndex], 
														0, 
														g_HeistPlanningClient.sHeistExtaMapData[index].vLocation[iTeam][i].X, 
														g_HeistPlanningClient.sHeistExtaMapData[index].vLocation[iTeam][i].Y,
														255, 255, 255, 100, 100)
														
								g_HeistPlanningClient.sBoardData.iMapPins[iRuleIndex]++
								
								// ADD POST-IT
								SET_HEIST_MAP_ADD_POSTIT(	g_HeistSharedClient.PlanningMapIndex, 
															g_HeistPlanningClient.sBoardData.iMapPostIt[iRuleIndex], 
															iRuleIndex + 1,
															g_HeistPlanningClient.sHeistExtaMapData[index].vLocation[iTeam][i].X, 
															g_HeistPlanningClient.sHeistExtaMapData[index].vLocation[iTeam][i].Y)

								g_HeistPlanningClient.sBoardData.iMapPostIt[iRuleIndex]++
								
								BREAK
							
							CASE ci_HEIST_MAP_ZONE_AND_POSTIT
							
								PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS - STRAND - ...... Adding additional ZONE_AND_POSTIT data from rule: ",index,", entity index: ", i)
						
								// ADD AREA.
								SET_HEIST_MAP_ADD_AREA(	g_HeistSharedClient.PlanningMapIndex, 
														g_HeistPlanningClient.sBoardData.iMapAreas[iRuleIndex],
														g_HeistPlanningClient.sHeistExtaMapData[index].vLocation[iTeam][i].X, 
														g_HeistPlanningClient.sHeistExtaMapData[index].vLocation[iTeam][i].Y,
														cf_HEIST_MAP_PED_ZONE_RADIUS,
														g_HeistPlanningClient.sHeistExtaMapData[index].sRuleColour[iTeam][i].iRed, 
														g_HeistPlanningClient.sHeistExtaMapData[index].sRuleColour[iTeam][i].iGreen,
														g_HeistPlanningClient.sHeistExtaMapData[index].sRuleColour[iTeam][i].iBlue,
														50)
														
								g_HeistPlanningClient.sBoardData.iMapAreas[iRuleIndex]++
								
								// ADD POST-IT
								SET_HEIST_MAP_ADD_POSTIT(	g_HeistSharedClient.PlanningMapIndex, 
															g_HeistPlanningClient.sBoardData.iMapPostIt[iRuleIndex],
															iRuleIndex + 1,
															g_HeistPlanningClient.sHeistExtaMapData[index].vLocation[iTeam][i].X, 
															g_HeistPlanningClient.sHeistExtaMapData[index].vLocation[iTeam][i].Y)
											
								g_HeistPlanningClient.sBoardData.iMapPostIt[iRuleIndex]++
								
								BREAK
								
						ENDSWITCH
						
						vRulePriorityLocations[iCurrentIndex]	=	vCurrentLoc		//	If valid location add it to the list of locations
						iCurrentIndex ++											//	Up the curr index ready for next loc
						
					ENDIF
				
				ENDIF
				
			ENDFOR
	
		ENDFOR
		
		PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS - STRAND - ## finished additional map graphics placement ##")
	
	ENDIF
	
	PRINTLN("[AMEC][HEIST_LAUNCH_MAP] - HEIST_START_CREATE_MAP_GRAPHICS - ## finished map graphics for each rule ##")
		
	RETURN TRUE
	
ENDFUNC


/// PURPOSE:
///    Create the board viewing camera, move the user to the correct location and disable interaction.
PROC CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA(INT iCameraPos, BOOL bDisableShake)
	
	SET_HEIST_FPS_CAM_FAR(g_HeistPlanningClient.sBoardCamData)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA - Creating FPS cam FAR. bDisableShake: ", bDisableShake)
		PRINTLN("[AMEC][HEIST_LAUNCH] - CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA ... iLookXLimit:	", g_HeistPlanningClient.sBoardCamData.sFPSCamData.iLookXLimit)
		PRINTLN("[AMEC][HEIST_LAUNCH] - CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA ... iLookYLimit:	", g_HeistPlanningClient.sBoardCamData.sFPSCamData.iLookYLimit)
		PRINTLN("[AMEC][HEIST_LAUNCH] - CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA ... iRollLimit:	", g_HeistPlanningClient.sBoardCamData.sFPSCamData.iRollLimit)
		PRINTLN("[AMEC][HEIST_LAUNCH] - CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA ... fMaxZoom:		", g_HeistPlanningClient.sBoardCamData.sFPSCamData.fMaxZoom)
	#ENDIF

	IF NOT DOES_CAM_EXIST(g_HeistPlanningClient.sBoardCamData.sFPSCamData.theCam)
		
		VECTOR vTemp = GET_CAMERA_POSITION_FROM_INT(iCameraPos)
		
		PRINTLN("[AMEC][HEIST_LAUNCH] - CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA - Creating FPS cam")
		PRINTLN("[AMEC][HEIST_LAUNCH] - CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA ... Position		: ", iCameraPos)
		PRINTLN("[AMEC][HEIST_LAUNCH] - CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA ... Position(v)	: ", vTemp)
		PRINTLN("[AMEC][HEIST_LAUNCH] - CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA ... FOV			: ", cf_HEIST_DEFAULT_CAMERA_FOV)
		PRINTLN("[AMEC][HEIST_LAUNCH] - CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA ... bDisableShake	: ", bDisableShake)
	
		INIT_FIRST_PERSON_CAMERA(	g_HeistPlanningClient.sBoardCamData.sFPSCamData, 
									GET_CAMERA_POSITION_FROM_INT(iCameraPos), 
									GET_CAMERA_ROTATION_FROM_INT(ci_HEIST_CAMERA_ROT_GENERIC), 
									GET_CAMERA_FOV_FROM_INT(iCameraPos, GET_IS_WIDESCREEN(), g_HeistPlanningClient.bHaveDoneHelp), 
									g_HeistPlanningClient.sBoardCamData.sFPSCamData.iLookXLimit, 
									g_HeistPlanningClient.sBoardCamData.sFPSCamData.iLookYLimit, 
									g_HeistPlanningClient.sBoardCamData.sFPSCamData.iRollLimit, 
									g_HeistPlanningClient.sBoardCamData.sFPSCamData.fMaxZoom,
									FALSE, 0, -1, bDisableShake)
	ELSE
		PRINTLN("[AMEC][HEIST_LAUNCH] - CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA - NOT Creating FPS cam, already exists")
		
		IF NOT bDisableShake
			PRINTLN("[AMEC][HEIST_LAUNCH] - CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA - Triggering hand shake")
			SHAKE_CAM(g_HeistPlanningClient.sBoardCamData.sFPSCamData.theCam, "HAND_SHAKE", FPC_HANDSHAKE)
		ENDIF
		
		IF NOT IS_CAM_RENDERING(g_HeistPlanningClient.sBoardCamData.sFPSCamData.theCam)
			PRINTLN("[AMEC][HEIST_LAUNCH] - CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA - FPS Cam not rendering, force cam render.")
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
		ENDIF
	ENDIF
								
	SET_HEIST_CAM_LOCATION_INDEX(iCameraPos)

ENDPROC

PROC CREATE_HEIST_FINALE_BOARD_CAMERA()

	IF NOT g_HeistPlanningClient.bHaveDoneHelp
		
		// Always start with overview.
		g_HeistPlanningClient.sHelpTextData.eHelpFlowState = HEIST_HELP_FLOW_OVERVIEW
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_FINALE_PLANNING_MODE - bHaveDoneHelp is FALSE, setting help text mode to ACTIVE.")
		#ENDIF
		CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA(ci_HEIST_CAMERA_POS_OVERVIEW, TRUE)
	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_FINALE_PLANNING_MODE - bHaveDoneHelp is TRUE, setting help text mode to INACTIVE and creating shake cam.")
		#ENDIF
		CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA(ci_HEIST_CAMERA_POS_OVERVIEW, FALSE)
	ENDIF

ENDPROC

PROC RESET_ALL_MAP_POSTIT_SCALE_TO_DEFAULT()

	INT iPostitID, iTeam
	
	iTeam = GET_HEIST_MEMBER_TEAM_NUMBER(g_HeistPlanningClient.iSelectedHeistMember)
	
	// Fix for 2317279.
	IF iTeam < 0
	OR iTeam >= FMMC_MAX_TEAMS
		EXIT
	ENDIF
	
	FOR iPostitID = 0 TO g_HeistPlanningClient.iPostitRulesForTeam[iTeam]
		PRINTLN("[RBJ] - net_heist_planning - RESET_ALL_MAP_POSTIT_SCALE_TO_DEFAULT - iPostitID: ", ( iPostitID * 100 ), "  set to scale: ", ci_HEIST_MAP_POSIT_SCALE_NORMAL, ".")
		SET_HEIST_MAP_POSTIT_SCALE(g_HeistSharedClient.PlanningMapIndex, iPostitID * 100, ci_HEIST_MAP_POSIT_SCALE_NORMAL)
	ENDFOR

ENDPROC

/// PURPOSE:
///    Update everything related to the heist planning map (E.G. pins, text, post-its, arrows etc). This can safely be called every frame.
PROC SET_HEIST_PLANNING_UPDATE_HEIST_MAP()
	
	IF g_HeistPlanningClient.sHeistDataCache.bDoUpdateMapLayers
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - ****************** UPDATING MAP ******************")
		#ENDIF
	
		// Firstly reset all previous highlighting / selection indicators.
		INT iTotal, iPointer, index, iRulePointer, iTeam//, iSelectedItem
		
		iTeam = GET_HEIST_MEMBER_TEAM_NUMBER(g_HeistPlanningClient.iSelectedHeistMember)
		
		//iSelectedItem = g_HeistPlanningClient.sBoardState.iSubHighlight
		
		IF iTeam < 0
		OR iTeam > (FMMC_MAX_RULES-1)
			PRINTLN("[AMEC][HEIST_LAUNCH] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - WARNING! iTeam for selected member is INVALID, not updating map.")
			g_HeistPlanningClient.sHeistDataCache.bDoUpdateMapLayers = FALSE
			EXIT
		ENDIF
		
		FOR index = 0 TO (FMMC_MAX_RULES-1)	
		
			PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - !!! - RULE [",index,"] - !!!")
		
			IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitset[index], ciBS_RULE_HIDE_ON_PLANNING_BOARD)

				IF NOT IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitSetTwo[index], ciBS_RULE2_HIDE_ON_MAP)
				
//					PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - iRulePointer: ",iRulePointer,", iSelectedItem(HL): ",iSelectedItem, ", Selected rule: ", iSelectedItem+1)
					iTotal = g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer]
					FOR iPointer = (iRulePointer * 100) TO g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer] 
						IF iTotal - iPointer > 0
							PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - ** Resetting scale for POST-IT: ",iPointer," for SELECTION: ", iRulePointer+1)
							SET_HEIST_MAP_POSTIT_SCALE(g_HeistSharedClient.PlanningMapIndex, iPointer, ci_HEIST_MAP_POSIT_SCALE_NORMAL)
						ENDIF
					ENDFOR
			
					//	Changed to reflect the postit index without the dashes being included.
					IF iRulePointer = g_HeistPlanningClient.iPostitNumber //	iSelectedItem
					
						PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - iRulePointer and iPostitNumber are EQUAL, adjust selected rule's graphics...")
						
						// Update map AREAS.
						iTotal = g_HeistPlanningClient.sBoardData.iMapAreas[iRulePointer]
						
						PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - AREA: iTotal = ", iTotal, ". Corrected total: ", iTotal - (iRulePointer * 100))
					
						FOR iPointer = (iRulePointer * 100) TO iTotal
							IF iTotal - iPointer > 0
								PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - ** Bringing forward AREA: ",iPointer," for SELECTION: ", iRulePointer+1)
								BRING_HEIST_MAP_AREA_TO_FRONT(g_HeistSharedClient.PlanningMapIndex, iPointer)
							ENDIF
						ENDFOR
						
						// Update map ARROWS.
						iTotal = g_HeistPlanningClient.sBoardData.iMapArrows[iRulePointer] 
						
						PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - ARROWS: iTotal = ", iTotal, ". Corrected total: ", iTotal - (iRulePointer * 100))
					
						FOR iPointer = (iRulePointer * 100) TO iTotal
							IF iTotal - iPointer > 0
								PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - ** Bringing forward ARROW: ",iPointer," for SELECTION: ", iRulePointer+1)
								BRING_HEIST_MAP_ARROW_TO_FRONT(g_HeistSharedClient.PlanningMapIndex, iPointer)
							ENDIF
						ENDFOR
						
						// Update map HIGHLIGHTS.
						iTotal = g_HeistPlanningClient.sBoardData.iMapHighlights[iRulePointer] 
						
						PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - IGHLIGHT: iTotal = ", iTotal, ". Corrected total: ", iTotal - (iRulePointer * 100))
					
						FOR iPointer = (iRulePointer * 100) TO iTotal
							IF iTotal - iPointer > 0
								PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - ** Bringing forward HIGHLIGHT: ",iPointer," for SELECTION: ", iRulePointer+1)
								BRING_HEIST_MAP_HIGHLIGHT_TO_FRONT(g_HeistSharedClient.PlanningMapIndex, iPointer)
							ENDIF
						ENDFOR
						
						// Update map PINS.
						iTotal = g_HeistPlanningClient.sBoardData.iMapPins[iRulePointer] 
						
						PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - PINS: iTotal = ", iTotal, ". Corrected total: ", iTotal - (iRulePointer * 100))
					
						FOR iPointer = (iRulePointer * 100) TO iTotal
							IF iTotal - iPointer > 0
								PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - ** Bringing forward PIN: ",iPointer," for SELECTION: ", iRulePointer+1)
								BRING_HEIST_MAP_PIN_TO_FRONT(g_HeistSharedClient.PlanningMapIndex, iPointer)
							ENDIF
						ENDFOR
						
						// Update map TEXT.
						iTotal = g_HeistPlanningClient.sBoardData.iMapText[iRulePointer]  
					
						PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - TEXT: iTotal = ", iTotal, ". Corrected total: ", iTotal - (iRulePointer * 100))
					
						FOR iPointer = (iRulePointer * 100) TO iTotal
							IF iTotal - iPointer > 0
								PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - ** Bringing forward TEXT: ",iPointer," for SELECTION: ", iRulePointer+1)
								BRING_HEIST_MAP_TEXT_TO_FRONT(g_HeistSharedClient.PlanningMapIndex, iPointer)
							ENDIF
						ENDFOR
						
						// Update map POST-IT notes.
						iTotal = g_HeistPlanningClient.sBoardData.iMapPostIt[iRulePointer] 
						
						PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - POST-IT: iTotal = ", iTotal, ". Corrected total: ", iTotal - (iRulePointer * 100))
					
						FOR iPointer = (iRulePointer * 100) TO iTotal
							IF iTotal - iPointer > 0
								PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - SET_HEIST_PLANNING_UPDATE_HEIST_MAP - LAYERS - ** Bringing forward POST-IT: ",iPointer," for SELECTION: ", iRulePointer+1)
								BRING_HEIST_MAP_POSTIT_TO_FRONT(g_HeistSharedClient.PlanningMapIndex, iPointer)
								SET_HEIST_MAP_POSTIT_SCALE(g_HeistSharedClient.PlanningMapIndex, iPointer, ci_HEIST_MAP_POSIT_SCALE_LARGE)
							ENDIF
						ENDFOR
						
					ENDIF
					
					iRulePointer++
					
				ENDIF
				
			ENDIF
			
		ENDFOR
		
		// Update complete, reset flag to stop unnecessary updating.
		g_HeistPlanningClient.sHeistDataCache.bDoUpdateMapLayers = FALSE
	
	ELIF GET_HEIST_UPDATE_MAP_ALL()
		// An update all flag will be set when the selected player changes, and the newly selected player has a different team assigned	
		IF HEIST_START_CREATE_MAP_GRAPHICS(GET_HEIST_MEMBER_TEAM_NUMBER(g_HeistPlanningClient.iSelectedHeistMember))
			SET_HEIST_UPDATE_MAP_ALL(FALSE)
			TOGGLE_ZOOM_MAP_TO_HEIST_AREA(g_HeistSharedClient.PlanningMapIndex, FALSE)
		ENDIF
	ELSE
		EXIT
	ENDIF

ENDPROC 


/// PURPOSE:
///    The post staggered update procedure. This is called every X frames, where X is the heist member count.
PROC PLAYER_MANAGEMENT_POST_PROCESS()
	
	// Make sure that the server has all of the hard data created at the start. This is incase the init event
	// gets missed or the server drops and the data is corrupted.
	CHECK_ALL_HEIST_PLANNING_VALUES_ARE_LEGAL()
	
	IF NOT g_HeistPlanningClient.bPlayerNetStatusInSync
		IF g_HeistPlanningClient.iNetworkSyncAttempts = 0
			g_HeistPlanningClient.bPlayerNetStatusInSync = TRUE
		ELSE
			PRINTLN("[AMEC][HEIST_LAUNCH] - PLAYER_MANAGEMENT_POST_PROCESS - Bypassing SET bPlayerNetStatusInSync because full status flush is still in progress! Attempts: ", g_HeistPlanningClient.iNetworkSyncAttempts)
		ENDIF
	ENDIF
	
ENDPROC



PROC UPDATE_HEIST_PLAYER_NET_STATUS_IMMEDIATE(INT iMinPlayers)

	INT index
	#IF IS_DEBUG_BUILD
	INT iTracker
	#ENDIF
	BOOL bWaitForData = TRUE
	
	FOR index = 0 TO (iMinPlayers-1)
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] != INVALID_HEIST_DATA
			bWaitForData = FALSE
		ELSE
			bWaitForData = TRUE
			#IF IS_DEBUG_BUILD
			iTracker = index
			#ENDIF
		ENDIF
	ENDFOR
	
	IF bWaitForData
		
		PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYER_NET_STATUS_IMMEDIATE - Participant ID: ",iTracker," of minimum total ",iMinPlayers," has reported INVALID_HEIST_DATA in iPlayerOrder.")
	
		g_HeistPlanningClient.iNetworkSyncAttempts++
		
		IF g_HeistPlanningClient.iNetworkSyncAttempts > MAX_HEIST_FINALE_SYNC_ATTEMPTS_HIGH
			PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYER_NET_STATUS_IMMEDIATE - ERROR! Did not receive network data in ", MAX_HEIST_FINALE_SYNC_ATTEMPTS_HIGH, " attempts! Drop to net checks (that will probably fail).")
		ELSE
			PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYER_NET_STATUS_IMMEDIATE - Waiting for network data sync. Attempt: ", g_HeistPlanningClient.iNetworkSyncAttempts)
			EXIT
		ENDIF
		
	ENDIF
	
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
	
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] != INVALID_HEIST_DATA
			// Now that this has been fully set up before, only check 1 player per frame.
			IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index]), FALSE, FALSE)
				IF NOT IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYER_NET_STATUS_IMMEDIATE - BITSET - Setting NetStatus Bit for player[",index,", ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index])),"]")
					#ENDIF
					SET_BIT(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
				ENDIF
			ELSE
				IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYER_NET_STATUS_IMMEDIATE - BITSET - Clearing NetStatus Bit for player[",index,", ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index])),"]")
					#ENDIF
					CLEAR_BIT(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
				ELSE
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYER_NET_STATUS_IMMEDIATE - BITSET - ERROR! Player[",index,", ", GET_PLAYER_NAME(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index])),"] is NOT OK, and never was.")
					#ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDFOR
	
	g_HeistPlanningClient.iNetworkSyncAttempts = 0
	g_HeistPlanningClient.bPlayerNetStatusInSync = TRUE
	g_HeistPlanningClient.bNetStatusUpdateRequired = FALSE

ENDPROC


/// PURPOSE:
///    Update a member of the heist planning board's net status. This is staggered to reduce the incured lag, and minimise processing overhead.
PROC UPDATE_HEIST_PLAYER_NET_STATUS()

//	IF HAS_PLAYER_ORDER_UPDATED()
		
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[g_HeistPlanningClient.iPlayerLoopIndex] != INVALID_HEIST_DATA
		
			// Now that this has been fully set up before, only check 1 player per frame.
			IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[g_HeistPlanningClient.iPlayerLoopIndex]), FALSE, FALSE)
				IF NOT IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, g_HeistPlanningClient.iPlayerLoopIndex)
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYER_NET_STATUS - BITSET - Setting NetStatus Bit for player[",g_HeistPlanningClient.iPlayerLoopIndex,", ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[g_HeistPlanningClient.iPlayerLoopIndex])),"]")
					#ENDIF
					SET_BIT(g_HeistPlanningClient.iPlayerNetStatusBitset, g_HeistPlanningClient.iPlayerLoopIndex)
				ENDIF
			ELSE
				IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, g_HeistPlanningClient.iPlayerLoopIndex)
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYER_NET_STATUS - BITSET - Clearing NetStatus Bit for player[",g_HeistPlanningClient.iPlayerLoopIndex,", ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[g_HeistPlanningClient.iPlayerLoopIndex])),"]")
					#ENDIF
					CLEAR_BIT(g_HeistPlanningClient.iPlayerNetStatusBitset, g_HeistPlanningClient.iPlayerLoopIndex)
				ENDIF
			ENDIF
		
		ENDIF

		IF g_HeistPlanningClient.iPlayerLoopIndex >= (MAX_HEIST_PLAYER_SLOTS-1)
			g_HeistPlanningClient.iPlayerLoopIndex = 0
			PLAYER_MANAGEMENT_POST_PROCESS()
		ELSE
			g_HeistPlanningClient.iPlayerLoopIndex++
		ENDIF

//	ENDIF

ENDPROC


/// PURPOSE:
///    Update everything to do with the player on the heist planning board. This can safely be called every frame.
PROC UPDATE_HEIST_PLAYERS()

	PLAYER_INDEX playerID
	TEXT_LABEL_23 tlPortrait
	TEXT_LABEL_15 tlPlayerRole
	TEXT_LABEL_15 tlCodeName
	STRING sStatus //, sOutfitName
	INT iRoleIcon, iStatusIcon, iCutPercent, iRank, index, iRemainingSlots, iVoiceState, iInvalidTeam
	INT iRemovalQueueBitset
	BOOL bResetHeadshots = FALSE
	
	// Update the selected player where the selected player is the current staggered frame index.
	UPDATE_HEIST_PLAYER_NET_STATUS()
	
	IF g_HeistPlanningClient.bNetStatusUpdateRequired
		PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYERS - bNetStatusUpdateRequired = TRUE, force immediate net state sync.")
		UPDATE_HEIST_PLAYER_NET_STATUS_IMMEDIATE(g_FMMC_STRUCT.iMinNumParticipants)
		EXIT
	ENDIF
	
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
	
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] != -1
		
			// Check the player net status.
			IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)

				playerID = INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index])
				
				// Get the players respect level.
				iRank = GET_PLAYER_RANK(playerID)
				
				// Retrieve the portrait for the player.
				tlPortrait = GET_HEIST_MEMBER_HEADSHOT(playerID)
				
				// Retrieve role information
				tlPlayerRole = GET_ROLE_INFO_FOR_PLAYER(playerID, iRoleIcon, index)
				
				// Retrieve status of player (either READY or NOT READY);
				sStatus = GET_STATUS_INFO_FOR_PLAYER(playerID, iStatusIcon)
				
				// Get specific player cut percentage.
				iCutPercent = GET_CUT_INFO_FOR_PLAYER(index)
				
				// Get the status of the player's headset.
				iVoiceState = GET_HEIST_PLANNING_PLAYER_HEADSET_STATUS(playerID)
				
				// Get the number of remaining team slots for the passed in team number.
				iRemainingSlots = GET_REMAINING_ROLES_FOR_TEAM(GET_HEIST_MEMBER_TEAM_NUMBER(index))
				
				// A player's team is INVALID if there is more than the maximum allowed.
				iInvalidTeam = GET_MY_TEAM_VALID_STATUS(GET_HEIST_MEMBER_TEAM_NUMBER(index))
				
				//B* 2057640 - Avoid displaying invalid player name, before player net status has updated.
				IF NOT ARE_STRINGS_EQUAL(GET_PLAYER_NAME(playerID),"**Invalid**")
					// Feed all of the data just gathered into the scaleform update method.
					SET_HEIST_PLANNING_UPDATE_CREW_SLOT(g_HeistSharedClient.PlanningBoardIndex, 
														index, 			GET_PLAYER_NAME(playerID), 
														iRank, 			tlPortrait, 
														tlPlayerRole, 	iRoleIcon, 
														sStatus, 		iStatusIcon, 
														iCutPercent, 	tlCodeName, 
														"", 			iRemainingSlots, 
														iVoiceState, 	iInvalidTeam,
														DEFAULT,
														g_HeistPlanningClient.bSuppressHeadshotScaleformLoading) 
				ELSE
					PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYERS - ARE_STRINGS_EQUAL(GET_PLAYER_NAME(playerID),**Invalid**), dont update scaleform")
				ENDIF
				
				IF Get_HeadshotID_For_Player(playerID) = NULL
					PRINTLN("[RBJ][HEIST_LAUNCH_HEADSHOT] - net_heist_planning - UPDATE_HEIST_PLAYERS - PlayerIndex value: ", index, " headshot is invalid. Setting flag to reload headshots.")
					bResetHeadshots = TRUE
				ENDIF
				
			// Player is NOT ok. This could mean that the have desconnected / aren't a valid player anymore.
			ELSE
//				IF HAS_PLAYER_ORDER_UPDATED()
					IF g_HeistPlanningClient.bPlayerNetStatusInSync
						// Mark this player for removal. This is delayed on purpose as not to impact other players above/below until the update is complete.
						// Additional lag incurred by this should be unnoticable.
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYERS - Adding player: ",index," to the removal queue.")
						#ENDIF
						
						SET_BIT(iRemovalQueueBitset, index)
						
					ENDIF
//				ELSE
//					#IF IS_DEBUG_BUILD
//						PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYERS - WARNING! iPlayerOrder is not yet synchronised, not adding player: ",index," to removal queue.")
//					#ENDIF
//				ENDIF
			ENDIF
			
		ENDIF
		
	ENDFOR
	
	IF bResetHeadshots
		PRINTLN("[RBJ][HEIST_LAUNCH_HEADSHOT] - net_heist_planning - UPDATE_HEIST_PLAYERS - Resetting playerID's to Invalid for headshot reset.")
		g_HeistPlanningClient.bSuppressHeadshotScaleformLoading = TRUE
		INT i
		FOR i = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
			g_HeistPlanningClient.sHeistHeadshots[i].playerID = INVALID_PLAYER_INDEX()
		ENDFOR
	ENDIF
	
	
//	IF NOT g_HeistPlanningClient.bPlayerNetStatusInSync
//	
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYERS - WARNING! Net status bitset not yet synchronised, exiting before playerOrder gets screwed by early cleanup.")
//		#ENDIF
//	
//		EXIT
//	ENDIF
//	
//	IF NOT HAS_PLAYER_ORDER_UPDATED()
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYERS - WARNING! iPlayerOrder is not yet synchronised, exiting before playerOrder gets screwed by early cleanup.")
//		#ENDIF
//		
//		EXIT
//	ENDIF
//	
//	// Process removals after updating all players.
//	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
//	
//		// This player has been marked for removal; begin processing the removal.
//		IF IS_BIT_SET(iRemovalQueueBitset, index)
//		
//			g_HeistPlanningClient.bDoPurgePlayerList = TRUE
//
//			IF IS_PLAYER_LEADER_OF_HEIST(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index]))
//					
//				#IF IS_DEBUG_BUILD
//					PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYERS - Heist leader net check returned fail; triggering heist shutdown.")
//				#ENDIF
//			
//				//g_HeistPlanningClient.iPlayerOrder[0] = -1
//				g_HeistPlanningClient.bForceHeistExit = TRUE
//					
//				EXIT
//			ELSE
//			
//				#IF IS_DEBUG_BUILD
//					PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYERS - Heist member net check returned fail for [",index,"]; shifting affected members.")
//				#ENDIF
//				
//				IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
//				
//					// Remove the heist member from globals, and connected clients.
//					IF BROADCST_SCRIPT_EVENT_HEIST_REMOVE_MEMBER(index)
//					
//						// Remove the heist member from locals. Start the loop from 1 not 0 because we don't want to wipe out the leader.
//						INT i = 1
//						INT iCutModifier = 0
//						
//						FOR i = 1 TO (MAX_HEIST_PLAYER_SLOTS-1)
//						
//							// Find the element in the array that needs to be removed, and only shift from there.
//							IF i >= (MAX_HEIST_PLAYER_SLOTS-1)
//								PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYERS - LEADER - Reached end of array.")
//								// The end of the array needs to be set to nothing, consdering that everything before has been shifted left.
//								g_HeistPlanningClient.iPlayerOrder[i] = -1
//								g_HeistPlanningClient.sHeistRoles.ePlayerRoles[i] = HEIST_ROLE_INVALID
//								CLEANUP_PLAYER_HEADSHOT(i)
//								
//							ELSE
//							
//								// Shift all array entries (from target onwards) to the left by one.
//								PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYERS - LEADER - iPlayerOder[",i,"]: ", g_HeistPlanningClient.iPlayerOrder[i], " = iPlayerOrder[",i,"+1]: ", g_HeistPlanningClient.iPlayerOrder[i+1])
//								g_HeistPlanningClient.iPlayerOrder[i] = g_HeistPlanningClient.iPlayerOrder[i+1]
//								g_HeistPlanningClient.sHeistRoles.ePlayerRoles[i] = g_HeistPlanningClient.sHeistRoles.ePlayerRoles[i+1]
//								g_HeistPlanningClient.iPlayerStatuses[i] = g_HeistPlanningClient.iPlayerStatuses[i+1]
//								g_HeistPlanningClient.sHeistHeadshots[i] = g_HeistPlanningClient.sHeistHeadshots[i+1]
//								
//							ENDIF
//							
//						ENDFOR
//						
//						// Recalculate the cuts now that everyone has shifted. We need to find to total players left in the heist
//						// planning board, and then every cut above that number gets added back onto the leader. This is done after
//						// the all other data has been shifted because it makes things easier.
//						FOR i = 1 TO (MAX_HEIST_PLAYER_SLOTS-1)
//						
//							IF g_HeistPlanningClient.iPlayerOrder[i] = -1
//							
//								IF g_HeistPlanningClient.sHeistCut.iCutPercent[i] > 0
//								
//									iCutModifier = (g_HeistPlanningClient.sHeistCut.iCutPercent[i] * (-1))
//
//									#IF IS_DEBUG_BUILD
//										PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYERS - LEADER - iCutModifier for [",i,"] = ",iCutModifier)
//									#ENDIF
//									
//									PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA(i, iCutModifier)
//									BROADCST_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON(i, iCutModifier, g_HeistPlanningClient.sHeistCut.iCutPercent[i])
//		
//								ENDIF
//								
//							ENDIF
//							
//						ENDFOR
//					
//					ENDIF
//					
//				ELSE
//					
//					// Remove the heist member from locals. Start the loop from 1 not 0 because we don't want to wipe out the leader.
//					INT i = 1
//					FOR i = 1 TO (MAX_HEIST_PLAYER_SLOTS-1)
//					
//						// Find the element in the array that needs to be removed, and only shift from there.
//						IF i >= (MAX_HEIST_PLAYER_SLOTS-1)
//							PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYERS - CLIENT - Reached end of array.")
//							// The end of the array needs to be set to nothing, consdering that everything before has been shifted left.
//							g_HeistPlanningClient.iPlayerOrder[i] = -1
//							CLEANUP_PLAYER_HEADSHOT(i)
//						ELSE
//							// Shift all array entries (from target onwards) to the left by one.
//							PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYERS - CLIENT - iPlayerOder[",i,"]: ", g_HeistPlanningClient.iPlayerOrder[i], " = iPlayerOrder[",i,"+1]: ", g_HeistPlanningClient.iPlayerOrder[i+1])
//							g_HeistPlanningClient.iPlayerOrder[i] = g_HeistPlanningClient.iPlayerOrder[i+1]
//						ENDIF
//		
//					ENDFOR
//					
//				ENDIF
//				
//				SET_ALL_HEIST_HIGHLIGHTS(g_HeistPlanningClient.sBoardState, ci_HEIST_LEADER, IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID()))
//
//			ENDIF
//			
//			// Mark the active/inactive player bitset for re-synchronisation.
//			RESET_PLAYER_BITSET_SYNC_MONITOR()
//			
//		ENDIF
//	
//	ENDFOR
//	
//	
//	// Update any left over player slots with blank ones. This has to be done last so that only superflous players are destroyed and not soon
//	// to be active slots. E.G. if player 2 leaves, player 3 takes their slot. We don't want to delete slot 2 before 3 has taken it, and after
//	// slot 3 is vacant, only then do we need to clear it.
//	
//	IF g_HeistPlanningClient.bDoPurgePlayerList
//	
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYERS - Waiting for playerOrder to be updated to continue player list purge...")
//		#ENDIF
//	
//		//IF HAS_PLAYER_ORDER_UPDATED()
//			FOR index = 1 TO (MAX_HEIST_PLAYER_SLOTS-1)
//				IF g_HeistPlanningClient.iPlayerOrder[index] = -1
//				
//					#IF IS_DEBUG_BUILD
//						PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYERS - Setting blank crew slot at index: ",index)
//					#ENDIF
//					
//					SET_HEIST_PLANNING_UPDATE_CREW_SLOT_BLANK(g_HeistSharedClient.PlanningBoardIndex, index)
//				ENDIF
//			ENDFOR
//			
//			g_HeistPlanningClient.bDoPurgePlayerList = FALSE
//		//ENDIF
//	ENDIF
	
ENDPROC


/// PURPOSE:
///    Update the player cards with the player role, and remove player who disconnect.
PROC UPDATE_HEIST_PLAYER_CARDS()

	IF NOT g_HeistPlanningClient.bPlayerCardsInSync

		TEXT_LABEL_15	tlPlayerRole
		TEXT_LABEL_23 	tlPortrait
		TEXT_LABEL_31	tlPlayerTitle
		STRING			sPlayerRating, sPlayerName
		INT				iPlayerStamina, iPlayerShooting, iPlayerStealth, iPlayerFlying, iPlayerDriving, iPlayerMentalState, iPlayerRank, iPlayerSlot, index, iPlayerID, iPlayerStrenth
		BOOL			bAccessPlane, bAccessHeli, bAccessBoat, bAccessCar
		FLOAT			fPlayerKillDeathRatio
		PLAYER_INDEX	playerID
		
//		#IF IS_DEBUG_BUILD
//			PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLAYER_CARD - Updating all heist member's player cards.")
//		#ENDIF

		FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
			
			IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] != -1

				playerID = INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index])
				iPlayerID = GlobalServerBD_HeistPlanning.iPlayerOrder[index] // Reduce the amound of boxing/unboxing requred.

				// Because this is the INIT of the player card, we only init the leader.
				iPlayerSlot = index
				
				// Leader's name.
				sPlayerName = GET_PLAYER_NAME(playerID)

				// Get the players respect level.
				iPlayerRank = GET_PLAYER_RANK(playerID)
				
				// Retrieve role informations
				INT i = 0 // Empty int, not used.
				tlPlayerRole = GET_ROLE_INFO_FOR_PLAYER(playerID, i, index)
				
				// Retrieve the portrait for the player.
				tlPortrait = GET_HEIST_MEMBER_HEADSHOT(playerID)
				
				// HARDWARE ACCESS
				IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_PLANE_UNLOCK)
					bAccessPlane = TRUE
				ENDIF
				
				IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_HELI_UNLOCK)
					bAccessHeli = TRUE
				ENDIF
				
				IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_BOAT_UNLOCK)
					bAccessBoat = TRUE
				ENDIF
				
				IF IS_BIT_SET(GlobalplayerBD_FM[iPlayerID].scoreData.iVehicleAccess, STAT_CARD_PERSONAL_CAR_UNLOCK)
					bAccessCar = TRUE
				ENDIF
				
				// KDR as float.
				fPlayerKillDeathRatio = GlobalplayerBD_FM[iPlayerID].scoreData.fKDRatio
				
				// To get the player title, we need to get the corrected rank number.
				INT iTempRank = GET_RANK_AS_NUMBER_FROM_0_TO_20(iPlayerRank)
				// Now build the localised text label using the corrected rank as the incrementor.
				tlPlayerTitle = RETURN_PERSONAL_RANK_STRING(iTempRank, TEAM_FREEMODE)
				
				IF NOT GET_PLAYER_BAD_SPORT_VALUE(playerID)
					sPlayerRating = "PCARD_CLEAN_PLAYER"
				ELSE
					sPlayerRating = "PCARD_BAD_SPORT"
				ENDIF
				
				iPlayerStamina = GlobalplayerBD_Stats[iPlayerID].iStatStamina
				iPlayerShooting = GlobalplayerBD_Stats[iPlayerID].iStatShooting
				iPlayerStealth = GlobalplayerBD_Stats[iPlayerID].iStatStealth
				iPlayerFlying = GlobalplayerBD_Stats[iPlayerID].iStatFlying
				iPlayerDriving = GlobalplayerBD_Stats[iPlayerID].iStatDriving
				iPlayerMentalState = GlobalplayerBD_Stats[iPlayerID].iStatMentalState
				iPlayerStrenth = GlobalplayerBD_Stats[iPlayerID].iStatStrength

				SET_HEIST_PLANNING_UPDATE_PLAYER_CARD(	g_HeistSharedClient.PlanningBoardIndex, 
														iPlayerSlot, sPlayerName, iPlayerRank, tlPlayerRole, 
														tlPortrait, bAccessPlane, bAccessHeli, bAccessBoat,
														bAccessCar, fPlayerKillDeathRatio, tlPlayerTitle, sPlayerRating, 
														iPlayerStamina, iPlayerShooting, iPlayerStealth, iPlayerFlying, 
														iPlayerDriving, iPlayerMentalState, iPlayerStrenth)	
				
				IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
					SET_HEIST_PLANNING_UPDATE_PLAYERCARD_MEDALS(g_HeistSharedClient.PlanningBoardIndex, 
																iPlayerSlot, 
																GET_GAMERTAG_ORDER_FOR_MISSION_INDEX(0, GET_PLAYER_NAME(playerID)),
																GET_GAMERTAG_ORDER_FOR_MISSION_INDEX(1, GET_PLAYER_NAME(playerID)),
																GET_GAMERTAG_ORDER_FOR_MISSION_INDEX(2, GET_PLAYER_NAME(playerID)),
																GET_GAMERTAG_ORDER_FOR_MISSION_INDEX(3, GET_PLAYER_NAME(playerID)),
																GET_GAMERTAG_ORDER_FOR_MISSION_INDEX(4, GET_PLAYER_NAME(playerID)))
				ELSE
					SET_HEIST_PLANNING_UPDATE_PLAYERCARD_MEDALS(g_HeistSharedClient.PlanningBoardIndex, 
																iPlayerSlot, 
																GET_HEIST_MEMBER_MEDAL_DATA_FOR_MISSION(0, GET_HASH_KEY(GET_PLAYER_NAME(playerID))),
																GET_HEIST_MEMBER_MEDAL_DATA_FOR_MISSION(1, GET_HASH_KEY(GET_PLAYER_NAME(playerID))),
																GET_HEIST_MEMBER_MEDAL_DATA_FOR_MISSION(2, GET_HASH_KEY(GET_PLAYER_NAME(playerID))),
																GET_HEIST_MEMBER_MEDAL_DATA_FOR_MISSION(3, GET_HASH_KEY(GET_PLAYER_NAME(playerID))),
																GET_HEIST_MEMBER_MEDAL_DATA_FOR_MISSION(4, GET_HASH_KEY(GET_PLAYER_NAME(playerID))))
				ENDIF
														
				
														
			ENDIF
		ENDFOR
	
		g_HeistPlanningClient.bPlayerCardsInSync = TRUE

	ENDIF
	
ENDPROC


/// PURPOSE:
///    Update all of the heist detals that are not DIRECTLY related to the player. This can safely be called every frame.
/// PARAMS:
///    sScaleformStruct - Instructional Button struct from FMMC_LAUNCHER.
PROC UPDATE_HEIST_MISC()

	IF NOT AM_I_LEADER_OF_HEIST()
		IF g_HeistPlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_MAP
			IF g_HeistPlanningClient.sBoardState.iSubHighlight != INVALID_HEIST_INDEX
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_MISC - GUEST - iSubHighlight != ",INVALID_HEIST_INDEX,", forcing value to: ", INVALID_HEIST_INDEX)
				#ENDIF
				g_HeistPlanningClient.sBoardState.iSubHighlight = INVALID_HEIST_INDEX
			ENDIF
		ENDIF
	ENDIF

	SET_HEIST_PLANNING_HIGHLIGHT_ITEM(	g_HeistSharedClient.PlanningBoardIndex,
										g_HeistPlanningClient.sBoardState.iHighlight,
										g_HeistPlanningClient.sBoardState.iSubHighlight,
										g_HeistPlanningClient.sBoardState.iLeftArrow,
										g_HeistPlanningClient.sBoardState.iRightArrow)

	// Bounds limit to avoid array overruns caused by the new "Launch Heist" button being on index 8.
	IF g_HeistPlanningClient.iSelectedHeistMember <= INVALID_HEIST_DATA
	OR g_HeistPlanningClient.iSelectedHeistMember > MAX_HEIST_PLAYER_SLOTS
		g_HeistPlanningClient.iSelectedHeistMember = ci_HEIST_LEADER
	ENDIF

	INT index
	BOOL bNewTextures
	
	FOR index = 0 TO (ciHEIST_PLANNING_BOARD_IMAGE_MAX-1)
		IF g_HeistPlanningClient.sPhotoVars[index].bSucess
			PRINTLN("[AMEC][HEIST_LAUNCH_PHOTO] - UPDATE_HEIST_MISC - Found an unclaimed texture in sPhotoVars index: ", index)
			bNewTextures = TRUE
		ENDIF
	ENDFOR
	
	IF bNewTextures
		STRUCT_DL_PHOTO_VARS_LITE sTempStruct // Shut the compiler up about loop initialized variables.
		
		FOR index = 0 TO (ciHEIST_PLANNING_BOARD_IMAGE_MAX-1)
			IF g_HeistPlanningClient.sPhotoVars[index].bSucess
				g_HeistPlanningClient.tlFinaleImages[index] = TEXTURE_DOWNLOAD_GET_NAME(g_HeistPlanningClient.sPhotoVars[index].iTextureDownloadHandle)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH_PHOTO] - UPDATE_HEIST_MISC - Extracting photo textures from download cache. Index: ", index, ", Texture: ", g_HeistPlanningClient.tlFinaleImages[index])
				#ENDIF
			
				g_HeistPlanningClient.sPhotoVars[index] = sTempStruct
			ENDIF
		ENDFOR
	ENDIF
	
	SET_HEIST_PLANNING_UPDATE_PREVIEW(g_HeistSharedClient.PlanningBoardIndex, ci_HEIST_PREVIEW_WINDOW_GEAR, "HEIST_GEAR", g_HeistPlanningClient.tlFinaleImages[ciHEIST_PLANNING_BOARD_IMAGE_THING])
	SET_HEIST_PLANNING_UPDATE_PREVIEW(g_HeistSharedClient.PlanningBoardIndex, ci_HEIST_PREVIEW_WINDOW_OUTFIT, "HEIST_VEH", g_HeistPlanningClient.tlFinaleImages[ciHEIST_PLANNING_BOARD_IMAGE_VEHCILE])
	
	INT iRootContentID = GET_STRAND_ROOT_CONTENT_ID_HASH()
	IF iRootContentID = 0
		iRootContentID = g_FMMC_STRUCT.iRootContentIdHash
	ENDIF
	
//	SET_HEIST_PLANNING_UPDATE_PREVIEW(g_HeistSharedClient.PlanningBoardIndex, ci_HEIST_PREVIEW_WINDOW_GEAR, "HEIST_GEAR", GET_HEIST_BOARD_GEAR_IMAGE(iRootContentID))
		
	SET_HEIST_PLANNING_UPDATE_TASK_LIST(g_HeistSharedClient.PlanningBoardIndex, GET_HEIST_MEMBER_TEAM_NUMBER(g_HeistPlanningClient.iSelectedHeistMember))

	// Update the piechart with the latest details.	
	IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
		SET_HEIST_PLANNING_UPDATE_PIECHART(	g_HeistSharedClient.PlanningBoardIndex,
											g_HeistPlanningClient.iHeistTotalPay,
											g_HeistPlanningClient.sHeistCut)
	ELSE
		SET_HEIST_PLANNING_UPDATE_PIECHART(	g_HeistSharedClient.PlanningBoardIndex,
											GlobalServerBD_HeistPlanning.iHeistTotalPay,
											GlobalServerBD_HeistPlanning.sHeistCut)
	ENDIF
	
//	IF g_HeistPlanningClient.sBoardState.iHighlight = ci_HEIST_ROW_LAUNCH
//		SET_HEIST_LAUNCH_BUTTON_HIGHLIGHTED(g_HeistSharedClient.PlanningBoardIndex, TRUE)
//	ELSE
//		SET_HEIST_LAUNCH_BUTTON_HIGHLIGHTED(g_HeistSharedClient.PlanningBoardIndex, FALSE)
//	ENDIF

	// Get the todo item info for mouse interaction.
	// Does this asynchronously as we have to wait for scaleform to return the data.
	PROCESS_MOUSE_TODO_ITEM_INFO( GET_HEIST_MEMBER_TEAM_NUMBER(g_HeistPlanningClient.iSelectedHeistMember))
																	
	// Update the planning map.
	SET_HEIST_PLANNING_UPDATE_HEIST_MAP()
		
ENDPROC

/// PURPOSE: Returns FALSE if a player involved in the heist still needs to see the cut 
FUNC BOOL HAVE_ALL_PLAYERS_SEEN_HEIST_FINALE_CUT()
	
	INT i
	PLAYER_INDEX playerID
	
	REPEAT NUM_NETWORK_PLAYERS i
		
		playerID = INT_TO_PLAYERINDEX(i)
		
		IF playerID != INVALID_PLAYER_INDEX()
		AND IS_NET_PLAYER_OK(playerID, FALSE)
			IF NOT IS_THIS_PLAYER_SCTV_OR_JOINING_AS_SCTV(playerID)
				IF NOT IS_BIT_SET(GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(playerID)].iBSPlayerHeistSettings, ciHEIST_PLAYER_BD_DONE_FINALE_CUTSCENE)
					PRINTLN("[AMEC][CUTSCENE] HAVE_ALL_PLAYERS_SEEN_HEIST_FINALE_CUT - Player ", GET_PLAYER_NAME(playerID), " has not seen CUT")
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Should we display the cutscene
FUNC BOOL SHOULD_HEIST_FINALE_CUTSCENE_PLAY(INT iRootContentID)
	
	IF NOT IS_CORONA_BIT_SET(CORONA_HEIST_CUTSCENE_HAS_BEEN_VALIDATED)
		
		// We can always show the finale
		SET_CORONA_BIT(CORONA_HEIST_FINALE_CUTSCENE_CAN_PLAY)
		
		// First check if the cutscene exists
		TEXT_LABEL_23 heistCutsceneName = TEMP_Get_Mocap_Name_From_FinaleRCID_Hash(iRootContentID, FALSE, TRUE, FALSE, FALSE, FALSE)
		
		IF IS_STRING_NULL_OR_EMPTY(heistCutsceneName)
			PRINTLN("[AMEC][CUTSCENE] SHOULD_HEIST_FINALE_CUTSCENE_PLAY - There is no heist cutscene for this strand. Skip stage.")
			CLEAR_CORONA_BIT(CORONA_HEIST_FINALE_CUTSCENE_CAN_PLAY)
		ENDIF
		
//		// Then check if all players have seen the cut
//		IF HAVE_ALL_PLAYERS_SEEN_HEIST_FINALE_CUT()
//		
//			//...yes, skip the cutscene
//			PRINTLN("[AMEC][CUTSCENE] SHOULD_HEIST_FINALE_CUTSCENE_PLAY - HAVE_ALL_PLAYERS_SEEN_HEIST_FINALE_CUT() = TRUE, Skip stage.")
//			CLEAR_CORONA_BIT(CORONA_HEIST_FINALE_CUTSCENE_CAN_PLAY)
//		ENDIF

		PRINTLN("[AMEC][CUTSCENE] SHOULD_HEIST_FINALE_CUTSCENE_PLAY - Forcing finale cutscene to play.")
		
		SET_CORONA_BIT(CORONA_HEIST_CUTSCENE_HAS_BEEN_VALIDATED)
	ENDIF
	
	RETURN IS_CORONA_BIT_SET(CORONA_HEIST_FINALE_CUTSCENE_CAN_PLAY)	
ENDFUNC

/// PURPOSE:
///    Returns the correct help text to display based on number of players on tutorial and if its the leader
FUNC STRING GET_HEIST_HELP_TEXT_FOR_PLAYERS_ON_FINALE_TUTORIAL(INT iTotalOnTutorial, BOOL bLeaderOnTutorial)
	
	// If only one player is on the tutorial, check if its the leader or a member
	IF iTotalOnTutorial = 1
		IF bLeaderOnTutorial
			RETURN "HEIST_HELP_22"
		ELSE
			RETURN "HEIST_HELP_23"
		ENDIF
	ELSE
		IF iTotalOnTutorial != MAX_HEIST_PLAYER_SLOTS
			RETURN "HEIST_HELP_24"
		ENDIF
	ENDIF
	
	RETURN ""
ENDFUNC


/// PURPOSE:
///    This updates the help text for a player NOT on the tutorial if some of the other members are.
PROC UPDATE_HEIST_PLANNING_PLAYER_STATUS_HELP()
	
	// Have we completed our check for the players being on the finale tutorial flow
	IF NOT g_HeistPlanningClient.bPlayersCheckedForFinaleTutorial
	
		//...no, every second poll (for max of 10x - no need to do this for any longer)
		IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_HeistPlanningClient.tsPlayerStatusHelpTextTimer)) >= 1000
		
			// Update our variabled for the next iteration
			g_HeistPlanningClient.iChecksForFinaleTutorialStatus++
			g_HeistPlanningClient.tsPlayerStatusHelpTextTimer = GET_NETWORK_TIME()
		
			INT i
			BOOL bTutorialActive
			BOOL bLeaderOnTutorial
			INT iTotalOnTutorial
			PLAYER_INDEX playerID
			
			// Loop over the players check if anyone is on the tutorial.
			REPEAT MAX_HEIST_PLAYER_SLOTS i 
				
				playerID = INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[i])
				
				// Is the player on the finale tutorial
				IF IS_PLAYER_ON_HEIST_FINALE_TUTORIAL(playerID)
					
					bTutorialActive = TRUE
					iTotalOnTutorial++
					
					// First index is always the leader.
					IF i = 0
						bLeaderOnTutorial = TRUE
					ENDIF					
				ENDIF
			ENDREPEAT
			
			// If someone is on the tutorial, show the help text forever
			IF bTutorialActive
				
				STRING strHelpText = GET_HEIST_HELP_TEXT_FOR_PLAYERS_ON_FINALE_TUTORIAL(iTotalOnTutorial, bLeaderOnTutorial)
			
				IF NOT IS_STRING_NULL_OR_EMPTY(strHelpText)
					PRINT_HELP_FOREVER(strHelpText)
					PRINTLN("[AMEC][HEIST_LAUNCH] UPDATE_HEIST_PLANNING_PLAYER_STATUS_HELP - print the help text: ", strHelpText, ". Total On Tutorial: ", iTotalOnTutorial, ", Leader? ", PICK_STRING(bLeaderOnTutorial, "TRUE", "FALSE"))
					
					g_HeistPlanningClient.bPlayersOnTutorialHelpDisplayed = TRUE
				ENDIF
				
				g_HeistPlanningClient.bPlayersCheckedForFinaleTutorial = TRUE
			ENDIF			
		
			IF g_HeistPlanningClient.iChecksForFinaleTutorialStatus >= 10
				PRINTLN("[AMEC][HEIST_LAUNCH] UPDATE_HEIST_PLANNING_PLAYER_STATUS_HELP - we have checked for players being on the tutorial help 10x. Stop.")
				g_HeistPlanningClient.bPlayersCheckedForFinaleTutorial = TRUE
			ENDIF
		ENDIF
	ELSE
		
		// If we have help text displaying then we should monitor for the players finishing
		IF g_HeistPlanningClient.bPlayersOnTutorialHelpDisplayed
			
			IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_HeistPlanningClient.tsPlayerStatusHelpTextTimer)) >= 1000
				
				g_HeistPlanningClient.tsPlayerStatusHelpTextTimer = GET_NETWORK_TIME()
			
				INT iPlayer
				BOOL bCleanUpHelp = TRUE
				PLAYER_INDEX playerID
				REPEAT MAX_HEIST_PLAYER_SLOTS iPlayer
				
					playerID = INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[iPlayer])
					
					// Is the player on the finale tutorial then we need to leave the help text on screen
					IF IS_PLAYER_ON_HEIST_FINALE_TUTORIAL(playerID)
						bCleanUpHelp = FALSE
					ENDIF
				ENDREPEAT
			
				// If we are safe to clean up the help text then do so.
				IF bCleanUpHelp
					g_HeistPlanningClient.bPlayersOnTutorialHelpDisplayed = FALSE
					PRINTLN("[AMEC][HEIST_LAUNCH] UPDATE_HEIST_PLANNING_PLAYER_STATUS_HELP - Safe to clean up help text now as no one is on tutorial: CLEAR_HELP()")
					CLEAR_HELP()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Manage the help text prompts displaysed to the user the first time they enter the planning board.
PROC UPDATE_HEIST_PLANNING_HELP_TEXT_PROMPTS()
	
	IF NOT SHOULD_HEIST_FINALE_CUTSCENE_PLAY(g_FMMC_STRUCT.iRootContentIDHash)
		IF g_HeistPlanningClient.bHaveDoneScreenCleanup = FALSE
			PRINTLN("[RBJ][HEIST_LAUNCH] - net_heists_planning - UPDATE_HEIST_PLANNING_HELP_TEXT_PROMPTS - Cutscene not played AND screen not cleaned up. Dropping out of func to delay help text.")
			EXIT
		ENDIF
		IF GET_SKYFREEZE_STAGE() = SKYFREEZE_FROZEN
			PRINTLN("[RBJ][HEIST_LAUNCH] - net_heists_planning - UPDATE_HEIST_PLANNING_HELP_TEXT_PROMPTS - Cutscene not played AND screen still frozen. Dropping out of func to delay help text.")
			EXIT
		ENDIF
		PRINTLN("[RBJ][HEIST_LAUNCH] - net_heists_planning - UPDATE_HEIST_PLANNING_HELP_TEXT_PROMPTS - Cutscene not played BUT screen cleared, and not frozen so moving on to help text prompts.")
	ENDIF
	IF ENUM_TO_INT(g_HeistPlanningClient.sHelpTextData.eHelpFlowState) != g_HeistPlanningClient.sHeistDataCache.sMiscCached.iHelpTextStateCached
		g_HeistPlanningClient.sHeistDataCache.sMiscCached.iHelpTextStateCached = ENUM_TO_INT(g_HeistPlanningClient.sHelpTextData.eHelpFlowState)
		
		g_HeistPlanningClient.sBoardState.iHighlight = INVALID_HEIST_INDEX
		
		TEXT_LABEL_23 tlHelpLabel = GET_HEIST_HELP_TEXT_LABEL(g_HeistPlanningClient.sHelpTextData, FALSE, IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID()))
		
		IF NOT IS_STRING_NULL_OR_EMPTY(tlHelpLabel)
		
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(tlHelpLabel)
				PRINT_HELP_FOREVER(tlHelpLabel)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLANNING_HELP_TEXT_PROMPTS - Printing new help text. Label: ", tlHelpLabel)
				#ENDIF
			ENDIF
			
			INT iCamLocation = GET_HEIST_PLANNING_CAMERA_LOCATION_INDEX()
			
			IF iCamLocation != INVALID_HEIST_DATA
			
				SET_HEIST_CAM_LOCATION_INDEX(iCamLocation)
				TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPlanningClient.sBoardCamData, iCamLocation)
				
			ENDIF
			
		ELSE
		
			IF GET_HEIST_CAM_LOCATION_INDEX() != ci_HEIST_CAMERA_POS_BOARD
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLANNING_HELP_TEXT_PROMPTS - Help-text label is INVALID, clearing help and moving to non-helptext state.")
				#ENDIF
			
				CLEAR_HELP()
				
				SET_HEIST_CAM_LOCATION_INDEX(ci_HEIST_CAMERA_POS_BOARD)
				g_HeistPlanningClient.sBoardState.iHighlight	= ci_HEIST_LEADER
				TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPlanningClient.sBoardCamData, ci_HEIST_CAMERA_POS_BOARD)
				
				// Make sure that non leader members never end up with the sub highlighter visible.
				IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
					g_HeistPlanningClient.sBoardState.iSubHighlight = ENUM_TO_INT(SUB_HIGHLIGHT_BOARD_ROLE)
				ELSE
					g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_PLANNING_HIGHLIGHT_NULL
				ENDIF
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_PLANNING_HELP_TEXT_PROMPTS - Reset camera to default position ready for interaction.")
				#ENDIF
			ENDIF
			
		ENDIF

	ENDIF
	
ENDPROC


PROC SET_HIGHLIGHTER_TO_LOCAL_PLAYER()

	INT index
	
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] != -1
			IF INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index]) = PLAYER_ID()
				PRINTLN("[AMEC][HEIST_LAUNCH] - SET_HIGHLIGHTER_TO_LOCAL_PLAYER - Setting main highlighter to value: ", index)
				g_HeistPlanningClient.sBoardState.iHighlight = index
				g_HeistPlanningClient.iSelectedHeistMember = index
				g_HeistPlanningClient.sBoardState.iSubHighlight = INVALID_HEIST_INDEX
				SET_HEIST_UPDATE_MAP_ALL(TRUE)
				EXIT
			ENDIF
		ENDIF
	ENDFOR

ENDPROC


/// PURPOSE:
///    Keep checking the launch status. If all players return ready, send a launch trigger event. LEADER ONLY.
PROC MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY()
	
	// This has to be executed solely by the leader, otherwise we will have 4x the amount of noise getting sent.
	IF NOT IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
		EXIT
	ENDIF
	
	BOOL bCanLaunch = TRUE
	BOOL bAllReady = TRUE
	INT index
	
	#IF IS_DEBUG_BUILD
	IF g_HeistPlanningClient.bForceHeistPlanningComplete
		IF NOT g_HeistPlanningClient.bHaveSentLaunchTrigger
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY - Detected leader force progress, firing launch event.")
			#ENDIF
			
			BROADCAST_GENERAL_EVENT(GENERAL_EVENT_HEIST_TRIGGER_LAUNCH, ALL_PLAYERS())
			
			EXIT
		ENDIF
	ENDIF
	#ENDIF
	

	FOR index = 1 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
			// Monitor each player's ready status (EXCUDING LEADER).
			IF NOT IS_PLAYER_IN_HEIST_READY_TO_LAUNCH(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index]))
				bCanLaunch = FALSE
				bAllReady = FALSE
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY - WAIT CONDITION - Player[",index,"] is NOT READY.")
				#ENDIF
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_3")
					PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY - Clearing help label: HEIST_NOTE_3")
					CLEAR_HELP()
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
			IF ENUM_TO_INT(GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index]) = INVALID_HEIST_DATA
			
	//			IF bCanLaunch // Can launch will be FALSE if 1 or more members are not ready.
	//				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_4")
	//					PRINT_HELP_FOREVER("HEIST_NOTE_4")
	//				ENDIF
	//			ENDIF

				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY - WAIT CONDITION - Player[",index,"]'s team is INVALID. Value: ", ENUM_TO_INT(GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index]))
				#ENDIF
			
				bCanLaunch = FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	#IF IS_DEBUG_BUILD
	IF DEBUG_IS_ALLOW_ALL_MISSION_WITH_TWO_PLAYERS_ENABLED()
	OR IS_TRANSITION_DEBUG_LAUNCH_VAR_SET()
		PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY - sc_AllowAllMissionsWithTwoPlayers = TRUE, ignoring minimum team checks.")
	ELSE
	#ENDIF
	
	IF g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY - WAIT CONDITION - sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB, cannot progress yet.")
		#ENDIF
		bCanLaunch = FALSE
	ENDIF
	
	// Check for the minimum team requirements.
	INT iTeamTotals[FMMC_MAX_TEAMS]
	GET_ROLE_TOTAL_FROM_TEAMS(iTeamTotals, TRUE)
	
	FOR index = 0 TO (FMMC_MAX_TEAMS-1)
		IF index < g_FMMC_STRUCT.iNumberOfTeams
			IF iTeamTotals[index] < g_FMMC_STRUCT.iNumPlayersPerTeam[index]
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY - WAIT CONDITION - iTeamTotals[",index,"]: ",iTeamTotals[index]," < Min: ", g_FMMC_STRUCT.iNumPlayersPerTeam[index])
				#ENDIF
				
				IF bAllReady
					IF g_HeistPlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_BOARD_SUB
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_3")
							PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY - Displaying help message: HEIST_NOTE_3")
							PRINT_HELP_FOREVER("HEIST_NOTE_3")
						ENDIF
					ENDIF
				ENDIF
			
				bCanLaunch = FALSE
				
			ELIF iTeamTotals[index] > g_FMMC_STRUCT.iMaxNumPlayersPerTeam[index]
			
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY - WAIT CONDITION - iTeamTotals[",index,"]: ",iTeamTotals[index]," > Max: ", g_FMMC_STRUCT.iMaxNumPlayersPerTeam[index])
				#ENDIF
				
				IF bAllReady
					IF g_HeistPlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_BOARD_SUB
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_3")
							PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY - Displaying help message: HEIST_NOTE_3")
							PRINT_HELP_FOREVER("HEIST_NOTE_3")
						ENDIF
					ENDIF
				ENDIF
			
				bCanLaunch = FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	IF g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] > 0
		PRINTLN("[AMEC][HEIST_LAUNCH_SPEW] - MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY - WAIT CONDITION - Kitty still has cash in it! Remainder value: ", g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS])
		bCanLaunch = FALSE
	ENDIF

	#IF IS_DEBUG_BUILD
	ENDIF
	#ENDIF

	
	IF bCanLaunch
	#IF IS_DEBUG_BUILD
	OR bFakeAllReady
	#ENDIF
		IF NOT g_HeistPlanningClient.bHaveSentLaunchTrigger
		
			// If the timer has expired, we don't want to wait for the leader to press "Launch Heist" before starting.
			IF g_HeistPlanningClient.bLaunchTimerExpired
			
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
				ENDIF
		
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY - bLaunchTimerExpired = TRUE, forcing all clients returned READY and leader has hit LAUNCH HEIST, firing launch event.")
				#ENDIF
				
				BROADCAST_GENERAL_EVENT(GENERAL_EVENT_HEIST_TRIGGER_LAUNCH, ALL_PLAYERS())
				
				EXIT	
			
			ENDIF
		
			SET_HEIST_LAUNCH_BUTTON_VISIBILITY(g_HeistSharedClient.PlanningBoardIndex, TRUE)
			SET_HEIST_LAUNCH_BUTTON_ENABLED(g_HeistSharedClient.PlanningBoardIndex, TRUE)
				
			IF NOT IS_PLAYER_IN_HEIST_READY_TO_LAUNCH(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[ci_HEIST_LEADER]))
			
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY - All clients returned READY, waiting for leader to hit launch...")
				#ENDIF
			
				EXIT
			ENDIF
		
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
			ENDIF
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY - All clients returned READY and leader has hit LAUNCH HEIST, firing launch event.")
			#ENDIF
			
			BROADCAST_GENERAL_EVENT(GENERAL_EVENT_HEIST_TRIGGER_LAUNCH, ALL_PLAYERS())
		
		ENDIF
		
	ELSE
		// Refused launch, conditions not met. Make sure that the LAUNCH HEIST button is disabled and the leader's status is reset.
		SET_HEIST_LAUNCH_BUTTON_VISIBILITY(g_HeistSharedClient.PlanningBoardIndex, FALSE)
		SET_HEIST_LAUNCH_BUTTON_ENABLED(g_HeistSharedClient.PlanningBoardIndex, FALSE)
		
		IF g_HeistPlanningClient.sBoardState.iHighlight = ci_HEIST_ROW_LAUNCH
			PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY - Setting highlighter index back to 0 from launch button.")
			g_HeistPlanningClient.sBoardState.iHighlight = ci_HEIST_ROW_ONE
		ENDIF
		
		IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHeistReadyState = ENUM_TO_INT(STATUS_READY)
			GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHeistReadyState = ENUM_TO_INT(STATUS_NOT_READY)				
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY - Re-setting leader's status to NOT READY due to invalid heist configuration.")
			#ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Check the global flag for heist launch. 
FUNC BOOL IS_HEIST_READY_TO_LAUNCH()

	IF NOT GlobalServerBD_HeistPlanning.bDoLaunchHeist
//		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_4")
//			CLEAR_HELP()
//		ENDIF
		
		RETURN FALSE
	ENDIF
	
	PRINTLN("[AMEC][HEIST_LAUNCH] - IS_HEIST_READY_TO_LAUNCH - Heist global launch is set to TRUE. Heist IS READY to launch at: ", GET_CLOUD_TIME_AS_INT())
	PRINTLN("[RBJ] - net_heist_planning - IS_HEIST_READY_TO_LAUNCH - Playing continue sound - 'Continue_Accepted'.")
	//CLEAR_HELP()
	PLAY_SOUND_FRONTEND(-1,"Continue_Accepted", "DLC_HEIST_PLANNING_BOARD_SOUNDS")
	RETURN TRUE
ENDFUNC


/// LAUNCH HEIST FINALE
/// PURPOSE:
///    Monitor the launch. Used by both the leader and all members. Needs to be called every frame.
 PROC MONITOR_MP_HEIST_LAUNCH(BOOL& bDoLaunch)

	// make the leader update everyone's launch status, and then pass on the appropriate details to the host.
	MONITOR_MP_HEIST_LAUNCH_LEADER_ONLY()

	IF IS_HEIST_READY_TO_LAUNCH()
				
		PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH - Heist is ready to launch! Progressing to cleanup routines.")
		
		INT index
		FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
			IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] = NATIVE_TO_INT(PLAYER_ID())
				GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ENUM_TO_INT(GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index])
				PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH - Setting local iTeamChosen to: ", ENUM_TO_INT(GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index]), " at time: ", GET_CLOUD_TIME_AS_INT(), " - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen)
			ENDIF
		ENDFOR
		
		FOR index = 0 TO (NUM_NETWORK_PLAYERS-1)
			PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH - SAVED PLAYER [",index,"]: ", lastHeistPlayers.GamersNames[index])
		ENDFOR
		
		IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
			SET_HEIST_AM_I_HEIST_LEADER_GLOBAL(TRUE)
			GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].iPreviousPropertyIndex = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
			
			TEXT_LABEL_23 tlContID = GET_HEIST_FINALE_STAT_DATA()
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH - Heist leader: ",GET_PLAYER_NAME(PLAYER_ID())," got rContID: ",tlContID," from stats. Putting into playerDB...")
			#ENDIF
			GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].tlHeistFinaleRootContID = tlContID

			SET_FM_JOB_ENTERY_TYPE(ciMISSION_ENTERY_TYPE_HEIST_PLANNING_BOARD)
			PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH - Heist leader: Set job entry type ciMISSION_ENTERY_TYPE_HEIST_PLANNING_BOARD")
		
		ELSE
			SET_HEIST_AM_I_HEIST_LEADER_GLOBAL(FALSE)
			GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].iPreviousPropertyIndex = INVALID_HEIST_DATA
		ENDIF
		
		PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH - iPreviousPropertyIndex = ", GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].iPreviousPropertyIndex)
		
		// Check if outfit has been set for your local player. If not, pick the default one for your current team.
		
		FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
			IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] != -1
				IF INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index]) = PLAYER_ID()
					IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
						IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
							
							g_HeistPlanningClient.m_ishost = TRUE // Telemetry / stat tracking.
							g_HeistPlanningClient.m_role = GET_HEIST_MEMBER_TEAM_NUMBER(ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index])) // Telemetry / stat tracking.
							g_HeistPlanningClient.m_cashcutpercentage = g_HeistPlanningClient.sHeistCut.iCutPercent[index] // Telemetry / stat tracking.
							PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH - LEADER - Saving cut value: ", g_HeistPlanningClient.sHeistCut.iCutPercent[index], " to NonTransitionVars.")
							g_TransitionSessionNonResetVars.iHeistCutPercent = g_HeistPlanningClient.sHeistCut.iCutPercent[index]
						
						ELSE
						
							g_HeistPlanningClient.m_ishost = FALSE // Telemetry / stat tracking.
							g_HeistPlanningClient.m_role = GET_HEIST_MEMBER_TEAM_NUMBER(ENUM_TO_INT(GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index])) // Telemetry / stat tracking.
							g_HeistPlanningClient.m_cashcutpercentage = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[index] // Telemetry / stat tracking.
							PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH - GUEST - Saving cut value: ", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[index], " to NonTransitionVars.")
							g_TransitionSessionNonResetVars.iHeistCutPercent = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[index]
						
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
					SAVE_HEIST_MEMBER_DATA_FOR_QUICK_RESTART(index)
				ENDIF
			ENDIF
		ENDFOR
		
		PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH - Setting iLocalPlayerTeam to value: ", g_HeistPlanningClient.m_role)
		g_HeistPlanningClient.iLocalPlayerTeam = g_HeistPlanningClient.m_role
		
		CALCULATE_STATS_FOR_POST_HEIST()
		
		// Get the time spent on the finale planning board.
		INT iTimeOnBoard
		iTimeOnBoard = ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), g_HeistPlanningClient.tsBoardTotalPlanningTime))
		g_HeistPlanningClient.m_TimePlanningBoard = iTimeOnBoard
		PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH - Setting m_TimePlanningBoard to: ", iTimeOnBoard)
		
		#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(DEBUG_PlanningWidgets)
			DELETE_WIDGET_GROUP(DEBUG_PlanningWidgets)
		ENDIF
		
		PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH - Debug spewing team data: ")
		
		IF AM_I_LEADER_OF_HEIST()
			FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
				IF g_HeistPlanningClient.iPlayerOrder[index] != INVALID_HEIST_DATA
					PRINTLN("[AMEC][HEIST_LAUNCH] ...  L - Player: [",
					index,
					", ",
					GET_PLAYER_NAME(INT_TO_PLAYERINDEX(g_HeistPlanningClient.iPlayerOrder[index])),
					"] saved heist team: ",
					GET_HEIST_MEMBER_TEAM_NUMBER(ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index])),
					", iTeamChosen: ",
					GlobalplayerBD_FM[g_HeistPlanningClient.iPlayerOrder[index]].sClientCoronaData.iTeamChosen)
				ENDIF
			ENDFOR
		ELSE
			FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
				IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] != INVALID_HEIST_DATA
					PRINTLN("[AMEC][HEIST_LAUNCH] ...  M - Player: [", index, ", ",
					GET_PLAYER_NAME(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index])),
					"] saved heist team: ",
					GET_HEIST_MEMBER_TEAM_NUMBER(ENUM_TO_INT(GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index])),
					", iTeamChosen: ",
					GlobalplayerBD_FM[GlobalServerBD_HeistPlanning.iPlayerOrder[index]].sClientCoronaData.iTeamChosen)
				ENDIF
			ENDFOR
		ENDIF
		
		#ENDIF
		
		IF IS_CURRENT_GEN_HEIST()
			PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH - Current gen launch detected, clean heist planning data.")
			TOGGLE_RENDERPHASES(FALSE)
			CLEAN_HEIST_FINALE_ALL(TRUE, FALSE, TRUE)
			CLEAN_HEIST_SHARED_DATA(TRUE, TRUE) 
			SET_HEIST_FINALE_STATE(HEIST_FLOW_IDLE)
		ELSE
			PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH - Next gen launch detected, DONT clean heist planning data.")
			TOGGLE_RENDERPHASES(FALSE)
			SET_HIGHLIGHTER_TO_LOCAL_PLAYER()
			SET_HEIST_PLANNING_UPDATE_TASK_LIST(g_HeistSharedClient.PlanningBoardIndex, GET_HEIST_MEMBER_TEAM_NUMBER(g_HeistPlanningClient.iSelectedHeistMember))
			SET_HEIST_FINALE_STATE(HEIST_FLOW_UPDATE_BACKGROUND)
		ENDIF
		
		PRINTLN("[AMEC][HEIST_LAUNCH] - MONITOR_MP_HEIST_LAUNCH - Cleanup executed successfully, returning TRUE back to FMMC LAUNCHER.")
		
		bDoLaunch = TRUE
		
		EXIT
	
	ENDIF

ENDPROC


/// PURPOSE:
///    Begin the process of smoothly moving the camera to the specified location.
PROC HEIST_CAM_MOVE_IN()
	SWITCH GET_HEIST_CAM_LOCATION_INDEX()
	
		/******************** LAYER 1 ********************/ // AKA OVERVIEW 
		CASE ci_HEIST_CAMERA_POS_OVERVIEW
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - CAMERA SWOOP - HEIST_CAM_MOVE_IN - Source: ci_HEIST_CAMERA_POS_OVERVIEW / Target: ci_HEIST_CAMERA_POS_BOARD")
			#ENDIF
		
			SET_HEIST_CAM_LOCATION_INDEX(ci_HEIST_CAMERA_POS_BOARD)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - HEIST_CAM_MOVE_IN - Playing Highlight_Move.")
			#ENDIF
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
			g_HeistPlanningClient.bUpdateInstButtons = TRUE
			
			TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPlanningClient.sBoardCamData, ci_HEIST_CAMERA_POS_BOARD)
			BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Begin the process of smoothly moving the camera to the specified location.
PROC HEIST_CAM_MOVE_OUT()
	SWITCH GET_HEIST_CAM_LOCATION_INDEX()
		/******************** LAYER 2 ********************/
		CASE ci_HEIST_CAMERA_POS_MAP_TUTORIAL
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - CAMERA SWOOP - HEIST_CAM_MOVE_OUT - Source: ci_HEIST_CAMERA_POS_MAP_TUTORIAL / Target: ci_HEIST_CAMERA_POS_OVERVIEW")
			#ENDIF
		
			SET_HEIST_CAM_LOCATION_INDEX(ci_HEIST_CAMERA_POS_OVERVIEW)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - HEIST_CAM_MOVE_OUT - Playing Highlight_Move.")
			#ENDIF
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_OVERVIEW
			g_HeistPlanningClient.bUpdateInstButtons = TRUE
			
			TOGGLE_ZOOM_MAP_TO_HEIST_AREA(g_HeistSharedClient.PlanningMapIndex, FALSE)
			
			TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPlanningClient.sBoardCamData, ci_HEIST_CAMERA_POS_OVERVIEW)
			BREAK
			
		CASE ci_HEIST_CAMERA_POS_BOARD
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - CAMERA SWOOP - HEIST_CAM_MOVE_OUT - Source: ci_HEIST_CAMERA_POS_BOARD / Target: ci_HEIST_CAMERA_POS_OVERVIEW")
			#ENDIF
		
			SET_HEIST_CAM_LOCATION_INDEX(ci_HEIST_CAMERA_POS_OVERVIEW)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - HEIST_CAM_MOVE_OUT - Playing Highlight_Move.")
			#ENDIF
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_OVERVIEW
			g_HeistPlanningClient.bUpdateInstButtons = TRUE
			
			TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPlanningClient.sBoardCamData, ci_HEIST_CAMERA_POS_OVERVIEW)
			BREAK
			
		CASE ci_HEIST_CAMERA_POS_DESC
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - CAMERA SWOOP - HEIST_CAM_MOVE_OUT - Source: ci_HEIST_CAMERA_POS_DESC / Target: ci_HEIST_CAMERA_POS_OVERVIEW")
			#ENDIF
		
			SET_HEIST_CAM_LOCATION_INDEX(ci_HEIST_CAMERA_POS_OVERVIEW)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - HEIST_CAM_MOVE_OUT - Playing Highlight_Move.")
			#ENDIF
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_OVERVIEW
			g_HeistPlanningClient.bUpdateInstButtons = TRUE
			
			TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPlanningClient.sBoardCamData, ci_HEIST_CAMERA_POS_OVERVIEW)
			BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Begin the process of smoothly moving the camera to the specified location.
PROC HEIST_CAM_MOVE_LEFT()

	// We don't want to pan the camera anywhere when the leader is in sub configuration mode. This is
	// to keep focus on the actual modification, and force the process of "saving" the changes.
	IF g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
		EXIT
	ENDIF
	
	FLOAT fPreviousBaseFOV = g_HeistPlanningClient.sBoardCamData.sFPSCamData.fBaseCamFov	// Save local copy to pass into Trigger.	
			
	SWITCH GET_HEIST_CAM_LOCATION_INDEX()
	
		/******************** LAYER 2 ********************/
		CASE ci_HEIST_CAMERA_POS_BOARD
		CASE ci_HEIST_CAMERA_POS_OVERVIEW
		
			SET_HEIST_CAM_LOCATION_INDEX(ci_HEIST_CAMERA_POS_MAP_TUTORIAL)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - CAMERA SWOOP - HEIST_CAM_MOVE_LEFT - Source: ci_HEIST_CAMERA_POS_BOARD / Target: ci_HEIST_CAMERA_POS_MAP_TUTORIAL")
			#ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - CAMERA SWOOP - HEIST_CAM_MOVE_LEFT - HEIST_CONTROL_LB - Playing Zoom_Left.")
			#ENDIF
			PLAY_SOUND_FRONTEND(-1,"Zoom_Left","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - CAMERA SWOOP - HEIST_CAM_MOVE_LEFT - HEIST_CONTROL_LB - Playing Map_Roll_Down.")
			#ENDIF
			PLAY_SOUND_FRONTEND(-1,"Map_Roll_Down","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			// Set the highlighting so that that TODO list is selected.
			g_HeistPlanningClient.sBoardState.iHighlight = ENUM_TO_INT(HIGHLIGHT_TODO_LIST)			
			g_HeistPlanningClient.sBoardState.iSubHighlight = g_HeistPlanningClient.sBoardState.iSelectedRule 						
			g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_MAP
			g_HeistPlanningClient.sHeistDataCache.bDoUpdateMapLayers = TRUE		//	This is set from ..eFocusPoint = HEIST_FOCUS_MAP but needed this frame so set manually. RowanJ
			g_HeistPlanningClient.bUpdateInstButtons = TRUE
		
			SET_HEIST_FPS_CAM_NEAR(g_HeistPlanningClient.sBoardCamData)
			
			TOGGLE_ZOOM_MAP_TO_HEIST_AREA(g_HeistSharedClient.PlanningMapIndex, TRUE)
				
			TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPlanningClient.sBoardCamData, ci_HEIST_CAMERA_POS_MAP_TUTORIAL, ci_HEIST_CAMERA_ROT_MAP_TUTORIAL, fPreviousBaseFOV, cf_HEIST_CAM_MAX_ZOOM_FAR)
			
			SET_HEIST_PLANNING_UPDATE_HEIST_MAP()	//	Called here so the postits refresh as it detects that the rule index is on the selected item to enlarge the first rules postit. RowanJ
			
			BREAK

	ENDSWITCH
	
ENDPROC


/// PURPOSE:
///    Begin the process of smoothly moving the camera to the specified location.
PROC HEIST_CAM_MOVE_RIGHT()

	// We don't want to pan the camera anywhere when the leader is in sub configuration mode. This is
	// to keep focus on the actual modification, and force the process of "saving" the changes.
	IF g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
		EXIT
	ENDIF

	FLOAT fPreviousBaseFOV = g_HeistPlanningClient.sBoardCamData.sFPSCamData.fBaseCamFov	// Save local copy to pass into Trigger.
	
	SWITCH GET_HEIST_CAM_LOCATION_INDEX()
	
		/******************** LAYER 2 ********************/
		CASE ci_HEIST_CAMERA_POS_MAP_TUTORIAL
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - CAMERA SWOOP - HEIST_CAM_MOVE_RIGHT - Source: ci_HEIST_CAMERA_POS_MAP_TUTORIAL / Target: ci_HEIST_CAMERA_POS_BOARD")
			#ENDIF
		
			SET_HEIST_CAM_LOCATION_INDEX(ci_HEIST_CAMERA_POS_OVERVIEW)  //ci_HEIST_CAMERA_POS_BOARD
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - CAMERA SWOOP - HEIST_CAM_MOVE_RIGHT - Playing Zoom_Right.")
			#ENDIF
			PLAY_SOUND_FRONTEND(-1,"Zoom_Right","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - CAMERA SWOOP - HEIST_CAM_MOVE_RIGHT - Playing Map_Roll_Up.")
			#ENDIF
			PLAY_SOUND_FRONTEND(-1,"Map_Roll_Up","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			g_HeistPlanningClient.sBoardState.iSelectedRule = g_HeistPlanningClient.sBoardState.iSubHighlight
			g_HeistPlanningClient.sBoardState.iHighlight = g_HeistPlanningClient.iSelectedHeistMember
			g_HeistPlanningClient.sBoardState.iLeftArrow = 0
			g_HeistPlanningClient.sBoardState.iRightArrow = 0
			g_HeistPlanningClient.bUpdateInstButtons = TRUE
			
			IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
				g_HeistPlanningClient.sBoardState.iSubHighlight = ENUM_TO_INT(SUB_HIGHLIGHT_BOARD_ROLE)
			ELSE
				g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_PLANNING_HIGHLIGHT_NULL
			ENDIF
			
			SET_HEIST_PLANNING_HIGHLIGHT_ITEM(	g_HeistSharedClient.PlanningBoardIndex, 
												g_HeistPlanningClient.sBoardState.iHighlight,
												g_HeistPlanningClient.sBoardState.iSubHighlight, 
												g_HeistPlanningClient.sBoardState.iLeftArrow, 
												g_HeistPlanningClient.sBoardState.iRightArrow, 
												FALSE, TRUE)
		
			g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD  
			
			SET_HEIST_FPS_CAM_FAR(g_HeistPlanningClient.sBoardCamData)
			
			TOGGLE_ZOOM_MAP_TO_HEIST_AREA(g_HeistSharedClient.PlanningMapIndex, FALSE)
			RESET_ALL_MAP_POSTIT_SCALE_TO_DEFAULT()
			
			TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPlanningClient.sBoardCamData, ci_HEIST_CAMERA_POS_OVERVIEW, DEFAULT, fPreviousBaseFOV, cf_HEIST_CAM_MAX_ZOOM_NEAR) //ci_HEIST_CAMERA_POS_BOARD)
					
			BREAK
			
	ENDSWITCH
	
ENDPROC

PROC HEIST_CAM_MOVE_TO_STATS()

	SWITCH GET_HEIST_CAM_LOCATION_INDEX()
	
		/******************** LAYER 2 ********************/
		CASE ci_HEIST_CAMERA_POS_OVERVIEW //ci_HEIST_CAMERA_POS_BOARD
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - CAMERA SWOOP - HEIST_CAM_MOVE_TO_STATS - Source: ci_HEIST_CAMERA_POS_BOARD / Target: ci_HEIST_CAMERA_POS_STATS")
			#ENDIF
		
			SET_HEIST_CAM_LOCATION_INDEX(ci_HEIST_CAMERA_POS_STATS)
			g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_STATS
			g_HeistPlanningClient.bUpdateInstButtons = TRUE
			TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPlanningClient.sBoardCamData, ci_HEIST_CAMERA_POS_STATS, DEFAULT, g_HeistPlanningClient.sBoardCamData.sFPSCamData.fBaseCamFov, g_HeistPlanningClient.sBoardCamData.sFPSCamData.fMaxZoom)
			
			BREAK
	ENDSWITCH

ENDPROC 

PROC HEIST_CAM_MOVE_TO_BOARD()

	SWITCH GET_HEIST_CAM_LOCATION_INDEX()
	
		/******************** LAYER 2 ********************/
		CASE ci_HEIST_CAMERA_POS_STATS
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - CAMERA SWOOP - HEIST_CAM_MOVE_TO_BOARD - Source: ci_HEIST_CAMERA_POS_STATS / Target: ci_HEIST_CAMERA_POS_BOARD")
			#ENDIF
		
			SET_HEIST_CAM_LOCATION_INDEX(ci_HEIST_CAMERA_POS_OVERVIEW) //(ci_HEIST_CAMERA_POS_BOARD)
			g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
			g_HeistPlanningClient.bUpdateInstButtons = TRUE
			TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPlanningClient.sBoardCamData, ci_HEIST_CAMERA_POS_OVERVIEW, DEFAULT, g_HeistPlanningClient.sBoardCamData.sFPSCamData.fBaseCamFov, g_HeistPlanningClient.sBoardCamData.sFPSCamData.fMaxZoom) //(ci_HEIST_CAMERA_POS_BOARD) - B* 2193575 RowanJ
			
			BREAK
	ENDSWITCH

ENDPROC


PROC HEIST_RESTORE_DISPLACED_ROLES_EXCLUDING_TEAM(INT iTeam)
	INT index
	
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index]) != iTeam
			IF g_HeistPlanningClient.sHeistDataCache.sMiscCached.sDisplacedRole[index] != HEIST_ROLE_UNASSIGNED
				IF ENUM_TO_INT(g_HeistPlanningClient.sHeistDataCache.sMiscCached.sDisplacedRole[index]) != iTeam
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_RESTORE_DISPLACED_ROLES_EXCLUDING_TEAM - Restoring displaced role: ",ENUM_TO_INT(g_HeistPlanningClient.sHeistDataCache.sMiscCached.sDisplacedRole[index])," for player: ",index, " excluding team: ",iTeam)
					#ENDIF
					g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = g_HeistPlanningClient.sHeistDataCache.sMiscCached.sDisplacedRole[index]
//					g_HeistPlanningClient.iSelectedOutfit[index] = GET_DEFAULT_OUTFIT_FOR_TEAM(ENUM_TO_INT(g_HeistPlanningClient.sHeistDataCache.sMiscCached.sDisplacedRole[index]))
					g_HeistPlanningClient.sHeistDataCache.sMiscCached.sDisplacedRole[index] = HEIST_ROLE_UNASSIGNED
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC


/// PURPOSE:
///    Remove member 0 from the passed in team; also removes that member's outfit.
/// PARAMS:
///    iTeam - Team to reduce by 1.
/// RETURNS:
///    TRUE if member found in team and removed.
FUNC BOOL HEIST_UNASSIGN_A_MEMBER_FROM_TEAM(INT iTeam)

	INT index

	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] != -1
			IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
				IF ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index]) = iTeam
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_UNASSIGN_A_MEMBER_FROM_TEAM - Unassigning member: ", index," from team: ", ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index]))
					#ENDIF
					g_HeistPlanningClient.sHeistDataCache.sMiscCached.sDisplacedRole[index] = g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index]
					g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = HEIST_ROLE_UNASSIGNED
					
//					#IF IS_DEBUG_BUILD
//						PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_UNASSIGN_A_MEMBER_FROM_TEAM - Unassigning member: ", index, "'s outfit: ", g_HeistPlanningClient.iSelectedOutfit[index])
//					#ENDIF
					// Update local data.
//					g_HeistPlanningClient.iSelectedOutfit[index] = INVALID_HEIST_DATA
					
					RETURN TRUE
				ENDIF
			ENDIF	
		ENDIF
	ENDFOR

	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Remove member 0 from the passed in team; also removes that member's outfit.
/// PARAMS:
///    iTeam - Team to reduce by 1.
/// RETURNS:
///    TRUE if member found in team and removed.
FUNC BOOL HEIST_REASSIGN_A_MEMBER_FROM_TEAM(INT iTeam, HEIST_HIGHLIGHT_TYPE eHighlightType, INT iOriginalRole)

	// First we need to find a member from the team ath is ripe for the picking. We can then use that pointer
	// to relocate them to a team with space.

	INT index, iPointer, iPlayerRoleEnumAsInt
	BOOL bFoundRole
	
	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] != -1
			IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
				IF ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index]) = iTeam
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_REASSIGN_A_MEMBER_FROM_TEAM - Found member: ",index," from team: ", ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index]))
					#ENDIF
					
					iPointer = index
					g_HeistPlanningClient.sHeistRoles.ePlayerRoles[iPointer] = HEIST_ROLE_UNASSIGNED
					index = (MAX_HEIST_PLAYER_SLOTS-1)
					
				ENDIF
			ENDIF	
		ENDIF
	ENDFOR
	
	iPlayerRoleEnumAsInt = ENUM_TO_INT(GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[iPointer])
	
	STEP_SELECTED_HEIST_ROLE(iPlayerRoleEnumAsInt, eHighlightType)
	
	// If the stepped role given originally is already at maximum value, we need to attempt to find another. This is also
	// extended here because we don't want to re-occupy to original role.
	FOR index = 0 TO (FMMC_MAX_TEAMS-1)
		IF NOT bFoundRole
			IF iPlayerRoleEnumAsInt != iTeam 
			
				// We're pretty deep into the "hypothetical" team changes by this point, so we need to take into account the leader's
				// original role as it could potentially become available. This requires a backwards shift on the index of
				// said original team/role so that the proposed teams aren't deadlocked as full (when they're not).
				
				IF GET_PREVIEW_NUM_PLAYERS_IN_TEAM(iPlayerRoleEnumAsInt, iOriginalRole) > g_FMMC_STRUCT.iMaxNumPlayersPerTeam[iPlayerRoleEnumAsInt]
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_REASSIGN_A_MEMBER_FROM_TEAM - WARNING! Stepped role: ",iPlayerRoleEnumAsInt," is already at maximum! Re-stepping role...")
					#ENDIF
					STEP_SELECTED_HEIST_ROLE(iPlayerRoleEnumAsInt, eHighlightType)
				ELSE
					// SUCCESS! Found a role isn't the original, and isn't full.
					g_HeistPlanningClient.sHeistRoles.ePlayerRoles[iPointer] = INT_TO_ENUM(HEIST_ROLES, iPlayerRoleEnumAsInt)
					bFoundRole = TRUE
				ENDIF
			ELSE
				PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_REASSIGN_A_MEMBER_FROM_TEAM - iPlayerRoleEnumAsInt is equal to displaced team, cannot put member back in. Re-stepping role...")
				STEP_SELECTED_HEIST_ROLE(iPlayerRoleEnumAsInt, eHighlightType)
			ENDIF
		ENDIF
	ENDFOR
	
	PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_REASSIGN_A_MEMBER_FROM_TEAM - Returning bFoundRole: ", bFoundRole)

	RETURN bFoundRole

ENDFUNC


/// PURPOSE:
///    Crawl through the lists of items for the selected detail. E.G., get the next enumerator for the heist role.
/// PARAMS:
///    eHighlightType - ENUM - Direction of travel along the imaginary list.
PROC UPDATE_SELECTED_PLAYER_PROPERTIES(HEIST_HIGHLIGHT_TYPE eHighlightType)

	INT index = g_HeistPlanningClient.iSelectedHeistMember
	INT i

	SWITCH g_HeistPlanningClient.sBoardState.iSubHighlight
	
		CASE ci_HEIST_COLUMN_ROLE

			INT iPlayerRoleEnumAsInt

			IF index < 0
			OR index > (MAX_HEIST_PLAYER_SLOTS-1)
				PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_SELECTED_PLAYER_PROPERTIES - ERROR! Selected member value is out of bounds! Value: ", index)
				EXIT
			ENDIF
			
			g_HeistPlanningClient.sBoardState.iSelectedRule = 0		//	Reset the current task when the players role changes.		B* 2164062 RowanJ.
			g_HeistPlanningClient.iPostitNumber = 0					//	Reset the postit to default to the top one on a role change.
			
			// Get the int value of the selected player's current role.
			iPlayerRoleEnumAsInt = ENUM_TO_INT(GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index])
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_SELECTED_PLAYER_PROPERTIES - Selected player [",index,", ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index])),"] role enum as int: ", iPlayerRoleEnumAsInt)
			#ENDIF
			
			IF eHighlightType = HEIST_HIGHLIGHT_SUB_LEFT
	
				IF iPlayerRoleEnumAsInt <= 0
					iPlayerRoleEnumAsInt = (g_FMMC_STRUCT.iNumberOfTeams-1)
				ELSE
					iPlayerRoleEnumAsInt--
				ENDIF
				
			ELIF eHighlightType = HEIST_HIGHLIGHT_SUB_RIGHT
				
				IF iPlayerRoleEnumAsInt < INVALID_HEIST_DATA
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH] - STEP_SELECTED_HEIST_ROLE - WARNING! Selected player's role was INVALID, moving to UNASSIGNED as a precaution!")
					#ENDIF
					iPlayerRoleEnumAsInt = INVALID_HEIST_DATA
				ENDIF
				
				IF iPlayerRoleEnumAsInt >= (g_FMMC_STRUCT.iNumberOfTeams-1)
					iPlayerRoleEnumAsInt = 0
				ELSE
					iPlayerRoleEnumAsInt++
				ENDIF
				
			ENDIF
			
			// Force the map to refresh.
			SET_HEIST_UPDATE_MAP_ALL(TRUE)
			
			// Hack around the decleration / assignment limitaion.
			HEIST_ROLES tempRole
			tempRole = INT_TO_ENUM(HEIST_ROLES, iPlayerRoleEnumAsInt)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_SELECTED_PLAYER_PROPERTIES - Player #",index," - New role: ", tempRole) //, " and new outfit: ",iNewOutfit)
			#ENDIF
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_SELECTED_PLAYER_PROPERTIES - ci_HEIST_COLUMN_ROLE - Playing Highlight_Move.")
			#ENDIF
			PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
			
			// Update local data to reduce input lag for the heist leader.
			g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = tempRole

			// Changed recently to update all roles instead of the singleton. This is to support the unassignment when a team is full.
			BROADCST_SCRIPT_EVENT_HEIST_UPDATE_ROLES(g_HeistPlanningClient.sHeistRoles.ePlayerRoles)
			
			BREAK
			
		CASE ci_HEIST_COLUMN_CUT
			
			INT iModifier, iPointer
			
			FOR i = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
				IF g_HeistPlanningClient.iPlayerOrder[i] != INVALID_HEIST_DATA
					IF g_HeistPlanningClient.iPlayerOrder[i] != g_HeistPlanningClient.iPlayerOrder[index]
						iPointer = i
					ENDIF
				ENDIF
			ENDFOR

			// Calculate the progression for the selection. By clicking right, they want to increase, left decrease etc.
			IF eHighlightType = HEIST_HIGHLIGHT_SUB_LEFT 
			
				// DECREASE
				// Remove cut from the selected player and add it to the pot.
//				IF NOT IS_PLAYER_LEADER_OF_HEIST(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[g_HeistPlanningClient.sBoardState.iHighlight]))
				iModifier = (ci_HEIST_PLANNING_CUT_STEP) * (-1) // flip to negative for decrement.
//				ENDIF
				
			ELIF eHighlightType = HEIST_HIGHLIGHT_SUB_RIGHT 
			
				IF IS_THIS_A_TUTORIAL_HEIST()

					IF g_HeistPlanningClient.sHeistCut.iCutPercent[iPointer] >= ci_HEIST_PLANNING_CUT_STEP
						iModifier = (ci_HEIST_PLANNING_CUT_STEP)
					ENDIF
					
				ELSE
				
					// INCREASE	 					
					// Check that the pot [0,1,2,3,4] has at least positive percent for the amount that will be added to the highlighted player.
					//							   ^
					IF g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] >= ci_HEIST_PLANNING_CUT_STEP
						iModifier = (ci_HEIST_PLANNING_CUT_STEP)
					ENDIF
				
				ENDIF
				
			ENDIF
			
			IF IS_THIS_A_TUTORIAL_HEIST()
				// Update local data to reduce input lag for the heist leader (tutorial only).
				PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA_TUTORIAL(index, iModifier, iPointer)
				
				// Update global data.
				BROADCST_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON_TUTORIAL(index, iModifier, g_HeistPlanningClient.sHeistCut.iCutPercent[index], iPointer)
			ELSE
				// Update local data to reduce input lag for the heist leader.
				PROCESS_CUT_MODIFIER_FOR_LOCAL_CUT_DATA(index, iModifier)
				
				// Update global data.
				BROADCST_SCRIPT_EVENT_HEIST_UPDATE_CUT_SINGLETON(index, iModifier, g_HeistPlanningClient.sHeistCut.iCutPercent[index])
			ENDIF
				
			BREAK	
			
		CASE ci_HEIST_COLUMN_OUTFIT
	
			// It's dead, Jim..
			
			BREAK
			
		DEFAULT
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_SELECTED_PLAYER_PROPERTIES - ERROR! Configure index is invalid, value: ", g_HeistPlanningClient.sBoardState.iSubHighlight)
			#ENDIF
			BREAK
	ENDSWITCH
ENDPROC


/// PURPOSE:
///    Manage and maintain the LEFT/RIGHT arrows that appear when a value can be changed (E.G. Heist role & Cut). Also disables those
///    arrows for anyone who isn't leader. 
PROC UPDATE_SUB_HIGHLIGHT_ARROWS()

	INT iPlayerNumber = g_HeistPlanningClient.sBoardState.iHighlight
	IF iPlayerNumber < 0 // If this is the case, it means that the camera is not focused on the board and there is no need to update.
		EXIT
	ELIF iPlayerNumber >= MAX_HEIST_PLAYER_SLOTS // Highlight index is not pointing to a player, move on.
		EXIT
	ENDIF	
	
	IF NOT AM_I_LEADER_OF_HEIST(TRUE)
		EXIT
	ENDIF
	
	INT iCutPercent, iCutMin, iCutMax, iCutOne, iCutTwo, iProblemPlayer
	
	IF g_HeistPlanningClient.iPlayerOrder[iPlayerNumber] = g_HeistSharedClient.iCurrentPropertyOwnerIndex
		iCutMin = g_sMPTUNABLES.iLeader_Min_Heist_Finale_Take_percentage
	ELSE
		iCutMin = g_sMPTUNABLES.iMember_Min_Heist_Finale_Take_percentage
	ENDIF

	iCutMax = g_sMPTUNABLES.imax_heist_cut_amount
	
	IF IS_THIS_A_TUTORIAL_HEIST()
	
		IF g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
			IF g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_COLUMN_CUT // If the player is on the cut selection.
				
				iCutOne = g_HeistPlanningClient.sHeistCut.iCutPercent[ci_HEIST_ROW_ONE]
				iCutTwo = g_HeistPlanningClient.sHeistCut.iCutPercent[ci_HEIST_ROW_TWO]
				
				IF ((iCutOne <= iCutMin) OR (iCutTwo <= iCutMin))
				
					IF (iCutOne <= iCutMin)
						iProblemPlayer = ci_HEIST_ROW_ONE
					ELSE
						iProblemPlayer = ci_HEIST_ROW_TWO
					ENDIF
					
					IF iPlayerNumber = iProblemPlayer
				
						IF g_HeistPlanningClient.sBoardState.iLeftArrow != ci_HEIST_DISABLED
							g_HeistPlanningClient.sBoardState.iLeftArrow = ci_HEIST_DISABLED
							PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_SUB_HIGHLIGHT_ARROWS - iCutOne(",iCutOne,") <= iCutMin(",iCutMin,") OR iCutTwo(",iCutTwo,") <= iCutMin (",iCutMin,") - LEFT ARROW DISABLE. Selected player(",iPlayerNumber,").")
						ENDIF
						IF g_HeistPlanningClient.sBoardState.iRightArrow != ci_HEIST_ENABLED
							g_HeistPlanningClient.sBoardState.iRightArrow = ci_HEIST_ENABLED
							PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_SUB_HIGHLIGHT_ARROWS - iCutOne(",iCutOne,") <= iCutMin(",iCutMin,") OR iCutTwo(",iCutTwo,") <= iCutMin (",iCutMin,") - RIGHT ARROW ENABLE. Selected player(",iPlayerNumber,").")
						ENDIF
						
					ELSE
					
						IF g_HeistPlanningClient.sBoardState.iLeftArrow != ci_HEIST_ENABLED
							g_HeistPlanningClient.sBoardState.iLeftArrow = ci_HEIST_ENABLED
							PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_SUB_HIGHLIGHT_ARROWS - iCutOne(",iCutOne,") <= iCutMin(",iCutMin,") OR iCutTwo(",iCutTwo,") <= iCutMin (",iCutMin,") - LEFT ARROW ENABLE. Selected player(",iPlayerNumber,").")
						ENDIF
						IF g_HeistPlanningClient.sBoardState.iRightArrow != ci_HEIST_DISABLED
							g_HeistPlanningClient.sBoardState.iRightArrow = ci_HEIST_DISABLED
							PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_SUB_HIGHLIGHT_ARROWS - iCutOne(",iCutOne,") <= iCutMin(",iCutMin,") OR iCutTwo(",iCutTwo,") <= iCutMin (",iCutMin,") - RIGHT ARROW DISABLE. Selected player(",iPlayerNumber,").")
						ENDIF
					
					ENDIF
					
				ELIF ((iCutOne >= iCutMax) OR (iCutTwo >= iCutMax))
				
					IF (iCutOne >= iCutMax)
						iProblemPlayer = ci_HEIST_ROW_ONE
					ELSE
						iProblemPlayer = ci_HEIST_ROW_TWO
					ENDIF
					
					IF iPlayerNumber = iProblemPlayer
					
						IF g_HeistPlanningClient.sBoardState.iLeftArrow != ci_HEIST_ENABLED
							g_HeistPlanningClient.sBoardState.iLeftArrow = ci_HEIST_ENABLED
							PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_SUB_HIGHLIGHT_ARROWS - iCutOne(",iCutOne,") >= iCutMax(",iCutMax,") OR iCutTwo(",iCutTwo,") >= iCutMax (",iCutMax,") - LEFT ARROW ENABLE. Selected player(",iPlayerNumber,").")
						ENDIF
						IF g_HeistPlanningClient.sBoardState.iRightArrow != ci_HEIST_DISABLED
							g_HeistPlanningClient.sBoardState.iRightArrow = ci_HEIST_DISABLED
							PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_SUB_HIGHLIGHT_ARROWS - iCutOne(",iCutOne,") >= iCutMax(",iCutMax,") OR iCutTwo(",iCutTwo,") >= iCutMax (",iCutMax,") - RIGHT ARROW DISABLE. Selected player(",iPlayerNumber,").")
						ENDIF
						
					ELSE
					
						IF g_HeistPlanningClient.sBoardState.iLeftArrow != ci_HEIST_DISABLED
						g_HeistPlanningClient.sBoardState.iLeftArrow = ci_HEIST_DISABLED
						PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_SUB_HIGHLIGHT_ARROWS - iCutOne(",iCutOne,") >= iCutMax(",iCutMax,") OR iCutTwo(",iCutTwo,") >= iCutMax (",iCutMax,") - LEFT ARROW DISABLE. Selected player(",iPlayerNumber,").")
						ENDIF
						IF g_HeistPlanningClient.sBoardState.iRightArrow != ci_HEIST_ENABLED
							g_HeistPlanningClient.sBoardState.iRightArrow = ci_HEIST_ENABLED
							PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_SUB_HIGHLIGHT_ARROWS - iCutOne(",iCutOne,") >= iCutMax(",iCutMax,") OR iCutTwo(",iCutTwo,") >= iCutMax (",iCutMax,") - RIGHT ARROW ENABLE. Selected player(",iPlayerNumber,").")
						ENDIF
					
					ENDIF
					
				ELSE
					IF g_HeistPlanningClient.sBoardState.iLeftArrow != ci_HEIST_ENABLED
						g_HeistPlanningClient.sBoardState.iLeftArrow = ci_HEIST_ENABLED
						PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_SUB_HIGHLIGHT_ARROWS - ELSE - RIGHT ARROW ENABLE. Selected player(",iPlayerNumber,").")
					ENDIF
					IF g_HeistPlanningClient.sBoardState.iRightArrow != ci_HEIST_ENABLED
						g_HeistPlanningClient.sBoardState.iRightArrow = ci_HEIST_ENABLED
						PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_SUB_HIGHLIGHT_ARROWS - ELSE - LEFT ARROW ENABLE. Selected player(",iPlayerNumber,").")
					ENDIF
				ENDIF
			ELSE
				g_HeistPlanningClient.sBoardState.iLeftArrow = ci_HEIST_ENABLED
				g_HeistPlanningClient.sBoardState.iRightArrow = ci_HEIST_ENABLED
			ENDIF
		ELSE
			g_HeistPlanningClient.sBoardState.iLeftArrow = ci_HEIST_DISABLED
			g_HeistPlanningClient.sBoardState.iRightArrow = ci_HEIST_DISABLED
		ENDIF
		
	ELSE
	
		IF g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
			IF g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_COLUMN_CUT // If the player is on the cut selection.
				
				iCutPercent = g_HeistPlanningClient.sHeistCut.iCutPercent[iPlayerNumber]

				IF iCutPercent >= iCutMax
					g_HeistPlanningClient.sBoardState.iLeftArrow = ci_HEIST_ENABLED
					g_HeistPlanningClient.sBoardState.iRightArrow = ci_HEIST_DISABLED
				ELIF iCutPercent <= iCutMin
					g_HeistPlanningClient.sBoardState.iLeftArrow = ci_HEIST_DISABLED
					g_HeistPlanningClient.sBoardState.iRightArrow = ci_HEIST_ENABLED
				ELSE
					g_HeistPlanningClient.sBoardState.iLeftArrow = ci_HEIST_ENABLED
					g_HeistPlanningClient.sBoardState.iRightArrow = ci_HEIST_ENABLED
				ENDIF
			ELSE
				g_HeistPlanningClient.sBoardState.iLeftArrow = ci_HEIST_ENABLED
				g_HeistPlanningClient.sBoardState.iRightArrow = ci_HEIST_ENABLED
			ENDIF
		ELSE
			g_HeistPlanningClient.sBoardState.iLeftArrow = ci_HEIST_DISABLED
			g_HeistPlanningClient.sBoardState.iRightArrow = ci_HEIST_DISABLED
		ENDIF
		
	ENDIF

	// Check if the player is the leader (don't want clients somehow changing other player's values locally).
	IF GlobalServerBD_HeistPlanning.LeaderIndex != PLAYER_ID() 
		g_HeistPlanningClient.sBoardState.iLeftArrow = ci_HEIST_DISABLED
		g_HeistPlanningClient.sBoardState.iRightArrow = ci_HEIST_DISABLED
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Manage and maintain the UP/DOWN/LEFT/RIGHT selection indexes driven from by front-end controls.
/// PARAMS:
///    eHighlightType - ENUM - Direction of travel for the highlighter.
PROC UPDATE_PLAYER_SELECTION_DETAILS(HEIST_HIGHLIGHT_TYPE eHighlightType = HEIST_HIGHLIGHT_NONE)
	
	// The leader is the only one with SUB highlight access, normal members can only use the basic player highlight.
	SWITCH eHighlightType

		CASE HEIST_HIGHLIGHT_DOWN
		
			// Change the highlighter behavious depending on region.
			SWITCH g_HeistPlanningClient.sBoardState.eFocusPoint
			
				CASE HEIST_FOCUS_BOARD
			
					// PLAYER highlight region.
					IF g_HeistPlanningClient.sBoardState.iHighlight >= (NUMBER_OF_PLAYERS_ON_HEIST()-1)
					
						g_HeistPlanningClient.bUpdateInstButtons = TRUE
						IF g_HeistPlanningClient.sBoardState.iHighlight = ci_HEIST_ROW_LAUNCH
						
							g_HeistPlanningClient.sBoardState.iHighlight = ci_HEIST_LEADER
							
						ELSE
						
							IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
							
								// Make sure the launch button is enabled, otherwise we don't want to allow people to highlight it.
								IF g_HeistPlanningClient.sHeistDataCache.sMiscCached.bLaunchButtonEnabled
									g_HeistPlanningClient.sBoardState.iHighlight = ci_HEIST_ROW_LAUNCH
								ELSE
									g_HeistPlanningClient.sBoardState.iHighlight = ci_HEIST_LEADER
								ENDIF
							
							ELSE	
								g_HeistPlanningClient.sBoardState.iHighlight = ci_HEIST_LEADER
							ENDIF
						ENDIF	
						
					ELSE
						g_HeistPlanningClient.sBoardState.iHighlight++
					ENDIF
					
					IF g_HeistPlanningClient.sBoardState.iHighlight != ci_HEIST_ROW_LAUNCH
						g_HeistPlanningClient.iSelectedHeistMember = g_HeistPlanningClient.sBoardState.iHighlight
					ENDIF
	
					g_HeistPlanningClient.sBoardState.iSelectedRule = ENUM_TO_INT(SUB_HIGHLIGHT_MAP_TASK_1)
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_PLAYER_SELECTION_DETAILS - DOWN - HEIST_FOCUS_BOARD - Playing Paper_Shuffle.")
					#ENDIF
					PLAY_SOUND_FRONTEND(-1,"Paper_Shuffle","DLC_HEIST_PLANNING_BOARD_SOUNDS")
				
					BREAK
					
				CASE HEIST_FOCUS_STATS
				
					IF g_HeistPlanningClient.sBoardState.iHighlight >= (NUMBER_OF_PLAYERS_ON_HEIST()-1)
						g_HeistPlanningClient.sBoardState.iHighlight = 0
					ELSE
						g_HeistPlanningClient.sBoardState.iHighlight++
					ENDIF
					
					IF g_HeistPlanningClient.sBoardState.iHighlight != ci_HEIST_ROW_LAUNCH
						g_HeistPlanningClient.iSelectedHeistMember = g_HeistPlanningClient.sBoardState.iHighlight
					ENDIF
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_PLAYER_SELECTION_DETAILS - DOWN - HEIST_FOCUS_STATS - Playing Paper_Shuffle.")
					#ENDIF
					PLAY_SOUND_FRONTEND(-1,"Paper_Shuffle","DLC_HEIST_PLANNING_BOARD_SOUNDS")
				
					BREAK
					
				CASE HEIST_FOCUS_MAP
				
					INT iPlayerTeam
					iPlayerTeam = GET_HEIST_MEMBER_TEAM_NUMBER(g_HeistPlanningClient.iSelectedHeistMember)
					
					IF iPlayerTeam < 0
					OR iPlayerTeam > (FMMC_MAX_TEAMS-1)
						PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_PLAYER_SELECTION_DETAILS - DOWN - HEIST_FOCUS_MAP - WARNING! iPlayerTeam was INVALID, value: ", iPlayerTeam)
						EXIT
					ENDIF
				
					IF g_HeistPlanningClient.sBoardState.iSubHighlight >= (g_HeistPlanningClient.iVisibleRulesForTeam[iPlayerTeam]-1)
						g_HeistPlanningClient.sBoardState.iSubHighlight = 0
						g_HeistPlanningClient.iPostitNumber = 0
					ELSE
						IF IS_BIT_SET(g_HeistPlanningClient.iTodoListBitset, g_HeistPlanningClient.sBoardState.iSubHighlight + 1)
							g_HeistPlanningClient.iPostitNumber ++
						ENDIF
						g_HeistPlanningClient.sBoardState.iSubHighlight++
					ENDIF
					
					g_HeistPlanningClient.sHeistDataCache.bDoUpdateMapLayers = TRUE	//	Removed if wrapper to ensure map updates on dashes, not just postit tasks. RowanJ B* 2164062
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_PLAYER_SELECTION_DETAILS - DOWN - HEIST_FOCUS_MAP - Playing Highlight_Move.")
					#ENDIF
					PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
				
					BREAK
			
			ENDSWITCH
			
			BREAK
			
		CASE HEIST_HIGHLIGHT_UP
		
			// Change the highlighter behavious depending on region.
			SWITCH g_HeistPlanningClient.sBoardState.eFocusPoint
			
				CASE HEIST_FOCUS_BOARD
				
					// PLAYER highlight region.
					IF g_HeistPlanningClient.sBoardState.iHighlight <= 0
						
						g_HeistPlanningClient.bUpdateInstButtons = TRUE
						IF g_HeistPlanningClient.sBoardState.iHighlight = ci_HEIST_ROW_LAUNCH
							g_HeistPlanningClient.sBoardState.iHighlight = (NUMBER_OF_PLAYERS_ON_HEIST()-1)
						ELSE
							IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
							
								IF g_HeistPlanningClient.sHeistDataCache.sMiscCached.bLaunchButtonEnabled
								AND g_HeistPlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_STATS
									g_HeistPlanningClient.sBoardState.iHighlight = ci_HEIST_ROW_LAUNCH
								ELSE
									g_HeistPlanningClient.sBoardState.iHighlight = (NUMBER_OF_PLAYERS_ON_HEIST()-1)
								ENDIF
								
							ELSE	
								g_HeistPlanningClient.sBoardState.iHighlight = (NUMBER_OF_PLAYERS_ON_HEIST()-1)
							ENDIF
						ENDIF
						
					ELSE
					
						IF g_HeistPlanningClient.sBoardState.iHighlight > (NUMBER_OF_PLAYERS_ON_HEIST()-1)
							g_HeistPlanningClient.sBoardState.iHighlight = (NUMBER_OF_PLAYERS_ON_HEIST()-1)
						ELSE
							g_HeistPlanningClient.sBoardState.iHighlight--
						ENDIF 
						
					ENDIF
					
					IF g_HeistPlanningClient.sBoardState.iHighlight != ci_HEIST_ROW_LAUNCH
						g_HeistPlanningClient.iSelectedHeistMember = g_HeistPlanningClient.sBoardState.iHighlight
					ENDIF
					
					g_HeistPlanningClient.sBoardState.iSelectedRule = ENUM_TO_INT(SUB_HIGHLIGHT_MAP_TASK_1)
				
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_PLAYER_SELECTION_DETAILS - UP - HEIST_FOCUS_BOARD - Playing Paper_Shuffle.")
					#ENDIF
					PLAY_SOUND_FRONTEND(-1,"Paper_Shuffle","DLC_HEIST_PLANNING_BOARD_SOUNDS")
				
					BREAK
					
				CASE HEIST_FOCUS_STATS
				
					// PLAYER highlight region.
					IF g_HeistPlanningClient.sBoardState.iHighlight <= 0
						g_HeistPlanningClient.sBoardState.iHighlight = (NUMBER_OF_PLAYERS_ON_HEIST()-1)
					ELSE
						IF g_HeistPlanningClient.sBoardState.iHighlight > (NUMBER_OF_PLAYERS_ON_HEIST()-1)
							g_HeistPlanningClient.sBoardState.iHighlight = (NUMBER_OF_PLAYERS_ON_HEIST()-1)
						ELSE
							g_HeistPlanningClient.sBoardState.iHighlight--
						ENDIF 
					ENDIF
					
					IF g_HeistPlanningClient.sBoardState.iHighlight != ci_HEIST_ROW_LAUNCH
						g_HeistPlanningClient.iSelectedHeistMember = g_HeistPlanningClient.sBoardState.iHighlight
					ENDIF
				
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_PLAYER_SELECTION_DETAILS - UP - HEIST_FOCUS_STATS - Playing Paper_Shuffle.")
					#ENDIF
					PLAY_SOUND_FRONTEND(-1,"Paper_Shuffle","DLC_HEIST_PLANNING_BOARD_SOUNDS")
				
					BREAK
				
				CASE HEIST_FOCUS_MAP
				
					INT iPlayerTeam
					iPlayerTeam = GET_HEIST_MEMBER_TEAM_NUMBER(g_HeistPlanningClient.iSelectedHeistMember)
					
					IF iPlayerTeam < 0
					OR iPlayerTeam > (FMMC_MAX_TEAMS-1)
						PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_PLAYER_SELECTION_DETAILS - UP - HEIST_FOCUS_MAP - WARNING! iPlayerTeam was INVALID, value: ", iPlayerTeam)
						EXIT
					ENDIF
				
					IF g_HeistPlanningClient.sBoardState.iSubHighlight <= 0
						g_HeistPlanningClient.sBoardState.iSubHighlight = (g_HeistPlanningClient.iVisibleRulesForTeam[iPlayerTeam]-1)
						g_HeistPlanningClient.iPostitNumber = g_HeistPlanningClient.iPostitRulesForTeam[iPlayerTeam] - 1
					ELSE
						IF IS_BIT_SET(g_HeistPlanningClient.iTodoListBitset, g_HeistPlanningClient.sBoardState.iSubHighlight) 
							g_HeistPlanningClient.iPostitNumber --
						ENDIF
						g_HeistPlanningClient.sBoardState.iSubHighlight--
					ENDIF
					
					g_HeistPlanningClient.sHeistDataCache.bDoUpdateMapLayers = TRUE		//	Removed if wrapper to ensure map updates on dashes, not just postit tasks. RowanJ B* 2164062
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_PLAYER_SELECTION_DETAILS - UP - HEIST_FOCUS_MAP - Playing Highlight_Move.")
					#ENDIF
					PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
				
					BREAK
				
			ENDSWITCH
			
			BREAK
			
		CASE HEIST_HIGHLIGHT_SUB_LEFT
		
			IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())	
			
				// Change the highlighter behavious depending on region.
				SWITCH g_HeistPlanningClient.sBoardState.eFocusPoint
				
					CASE HEIST_FOCUS_BOARD
					
						// No sub-items have been selected, move the cursor.
						IF g_HeistPlanningClient.sBoardState.iSubHighlight <= 0
							g_HeistPlanningClient.sBoardState.iSubHighlight = (MAX_NUM_SUB_INDEXES-1)
						ELSE
							g_HeistPlanningClient.sBoardState.iSubHighlight--
						ENDIF
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_PLAYER_SELECTION_DETAILS - LEFT - HEIST_FOCUS_BOARD - Playing Highlight_Move.")
						#ENDIF
						PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						
						BREAK
						
					CASE HEIST_FOCUS_BOARD_SUB
					
						// A sub-item been selected, don't move cursor left and right, but instead iterate through the selected item's contents.
						UPDATE_SELECTED_PLAYER_PROPERTIES(HEIST_HIGHLIGHT_SUB_LEFT)
						
						BREAK
				
				ENDSWITCH
				
			ELSE // Not leader...
				g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_PLANNING_HIGHLIGHT_NULL
			ENDIF
			
			BREAK
			
		CASE HEIST_HIGHLIGHT_SUB_RIGHT
		
			IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
			
				// Change the highlighter behavious depending on region.
				SWITCH g_HeistPlanningClient.sBoardState.eFocusPoint
				
					CASE HEIST_FOCUS_BOARD
					
						// No sub-items have been selected, move the cursor.
						IF g_HeistPlanningClient.sBoardState.iSubHighlight >= (MAX_NUM_SUB_INDEXES-1)
							g_HeistPlanningClient.sBoardState.iSubHighlight = 0
						ELSE
							g_HeistPlanningClient.sBoardState.iSubHighlight++
						ENDIF
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_PLAYER_SELECTION_DETAILS - RIGHT - HEIST_FOCUS_BOARD - Playing Highlight_Move.")
						#ENDIF
						PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
					
						BREAK
						
					CASE HEIST_FOCUS_BOARD_SUB
						
						// A sub-item been selected, don't move cursor left and right, but instead iterate through the selected item's contents.
						UPDATE_SELECTED_PLAYER_PROPERTIES(HEIST_HIGHLIGHT_SUB_RIGHT)	
					
						BREAK
						
				ENDSWITCH

			ELSE // Not leader...
				g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_PLANNING_HIGHLIGHT_NULL
			ENDIF
			
			BREAK
	ENDSWITCH
	
	// Make sure the item modification arrows get updated if the player focus is in the appropriate region.
	IF g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
	OR g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
		UPDATE_SUB_HIGHLIGHT_ARROWS()
	ENDIF
	
ENDPROC

//// PURPOSE:
 ///    Works out which todo list item (rule) is being highlighted by the mouse.
 /// RETURNS:
 ///    The index of the highlighted item, or -1 if none.
 ///     
FUNC INT GET_HEIST_PLANNING_MOUSE_HIGHLIGHTED_RULE()
	
	INT i = 0
	FLOAT fYPos
			
	VECTOR vBoardDimensions =  g_HeistSharedClient.vWorldSize
	fYPos = cf_HEIST_MOUSE_TODO_ORIGIN_Y
	
	INT iVisibleRules = iNumTodoItems
			
	vBoardDimensions.x *= 0.60 // Adjust size for heist planning board.
		
	FLOAT fTodoItemHeight
	
	WHILE i < iVisibleRules
	
		IF iTodoItemHeight[i]  > 0
		
			fTodoItemHeight = (cf_HEIST_MOUSE_TODO_ITEM_HEIGHT * iTodoItemHeight[i]) 
				
			IF IS_CURSOR_INSIDE_3D_MENU_ITEM( cf_HEIST_MOUSE_TODO_ORIGIN_X, fYPos, cf_HEIST_MOUSE_TODO_ITEM_WIDTH, fTodoItemHeight, g_HeistSharedClient.vBoardPosition, g_HeistSharedClient.vBoardRotation.z, vBoardDimensions )
				//DISPLAY_TEXT_WITH_NUMBER( 0.1, 0.1, "NUMBER", i )
				RETURN i
			ENDIF
			
		ENDIF
		
		fYPos += (fTodoItemHeight + cf_HEIST_MOUSE_TODO_ITEM_GAP)
		
		++i
			
	ENDWHILE

	RETURN -1

ENDFUNC


/// PURPOSE:
///    Gets the index of the player that's being highlighted by the mouse.
/// RETURNS:
///    The index of the player, ci_HEIST_ROW_LAUNCH if the launch row is highlighted, or -1 if no valid row highlighted
FUNC INT GET_HEIST_PLANNING_MOUSE_HIGHLIGHTED_PLAYER()

	INT i = 0
	FLOAT fYPos
	
	VECTOR vBoardDimensions =  g_HeistSharedClient.vWorldSize
			
	vBoardDimensions.x *= 0.60 // Adjust size for heist planning board.
	
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	

	fYPos = cf_HEIST_MOUSE_PLAYER_ORIGIN_Y
	
	// Handle players
	WHILE i < NUMBER_OF_PLAYERS_ON_HEIST()
	
		IF IS_CURSOR_INSIDE_3D_MENU_ITEM( cf_HEIST_MOUSE_PLAYER_ORIGIN_X, fYPos, cf_HEIST_MOUSE_PLAYER_WIDTH, cf_HEIST_MOUSE_PLAYER_HEIGHT, g_HeistSharedClient.vBoardPosition, g_HeistSharedClient.vBoardRotation.z, vBoardDimensions )
			
		//	DISPLAY_TEXT_WITH_NUMBER( 0.1, 0.1, "NUMBER", i)
			RETURN i
		
		ENDIF
		
		fYPos += cf_HEIST_MOUSE_PLAYER_HEIGHT + cf_HEIST_MOUSE_PLAYER_GAP
		
		i++
	
	ENDWHILE
	
	
	// Handle confirm row.
	IF g_HeistPlanningClient.sHeistDataCache.sMiscCached.bLaunchButtonEnabled
	AND i = NUMBER_OF_PLAYERS_ON_HEIST()
	
		fYPos += cf_HEIST_MOUSE_PLAYER_GAP

		IF IS_CURSOR_INSIDE_3D_MENU_ITEM( cf_HEIST_MOUSE_PLAYER_ORIGIN_X, fYPos, cf_HEIST_MOUSE_PLAYER_WIDTH, cf_HEIST_MOUSE_PLAYER_HEIGHT, g_HeistSharedClient.vBoardPosition, g_HeistSharedClient.vBoardRotation.z, vBoardDimensions )
			//DISPLAY_TEXT_WITH_NUMBER( 0.1, 0.1, "NUMBER", ci_HEIST_ROW_LAUNCH )
			RETURN ci_HEIST_ROW_LAUNCH
		ENDIF
	
	ENDIF

	RETURN -1

ENDFUNC

/// PURPOSE:
///    Checks if the mouse is selecting a specified area of the heist planning board.
/// PARAMS:
///    eMouseArea - Enum defining which area is selected.
///    iPlayer - Player number in the player list (-1 if no player needed)
/// RETURNS:
///    TRUE if the area is being selected.
FUNC BOOL IS_MOUSE_INSIDE_HEIST_PLANNING_AREA(eHEIST_PLANNING_MOUSE_AREA eMouseArea, INT iPlayer = -1)

	FLOAT fXPos, fYPos
	FLOAT fWidth, fHeight
	
	VECTOR vBoardDimensions =  g_HeistSharedClient.vWorldSize	
	vBoardDimensions.x *= 0.60 // Adjust size for heist planning board.
	
	// Handle main menu
	IF iPlayer != -1
	
		fYPos = cf_HEIST_MOUSE_PLAYER_ORIGIN_Y + (cf_HEIST_MOUSE_PLAYER_HEIGHT * iPlayer)
		
		IF iPlayer > 0 
			fYPos += cf_HEIST_MOUSE_PLAYER_GAP
		ENDIF
		
		fHeight = cf_HEIST_MOUSE_PLAYER_HEIGHT
				
	ENDIF
	
	SWITCH eMouseArea
	
		CASE HEIST_PLANNING_MOUSE_ROLE
		
			fXPos = 0.4
			fWidth = 0.204	
			
			BREAK
			
		CASE HEIST_PLANNING_MOUSE_ROLE_LEFT
			
			fXPos = 0.4
			fWidth = 0.03	
		
			BREAK
			
		CASE HEIST_PLANNING_MOUSE_ROLE_RIGHT
			
			fXPos = 0.43
			fWidth = 0.174
		
			BREAK
			
		CASE HEIST_PLANNING_MOUSE_CUT
		
			fXPos = 0.605
			fWidth = 0.177	
			
			BREAK
			
		CASE HEIST_PLANNING_MOUSE_CUT_LEFT
			
			fXPos = 0.605
			fWidth = 0.03
		
			BREAK
			
		CASE HEIST_PLANNING_MOUSE_CUT_RIGHT
			
			fXPos = 0.635
			fWidth = 0.148
		
			BREAK
			
		CASE HEIST_PLANNING_MOUSE_STATS
			
			fXPos = 0.356
			fYPos = 0.553
			fWidth = 0.617
			fHeight = 0.399
			
			BREAK
			
		CASE HEIST_PLANNING_MOUSE_STATS_PLAYER
			
			fXPos = 0.771
			fYPos = 0.591
			fWidth = 0.194
			fHeight = 0.295
			
			BREAK
		
		CASE HEIST_PLANNING_MOUSE_STATS_PLAYER_NEXT
			
			fXPos = 0.966
			fYPos = 0.591
			fWidth = 0.074
			fHeight = 0.295
			
			BREAK
			
		CASE HEIST_PLANNING_MOUSE_STATS_PLAYER_PREV
		
			fXPos = 0.697
			fYPos = 0.591
			fWidth = 0.074
			fHeight = 0.295
			
			BREAK
			
		CASE HEIST_PLANNING_MOUSE_RULES
			
			fXPos = 0.0
			fYPos = 0.378
			fWidth = 0.271
			fHeight = 0.479
			
			BREAK
			
	ENDSWITCH
		
	
	IF IS_CURSOR_INSIDE_3D_MENU_ITEM( fXPos, fYPos, fWidth, fHeight, g_HeistSharedClient.vBoardPosition, g_HeistSharedClient.vBoardRotation.z, vBoardDimensions )
		RETURN TRUE
	ENDIF 
	
	RETURN FALSE

ENDFUNC

/// PURPOSE:
///    Handles the mouse control for the board section of the planning board.
/// RETURNS:
///    The heist control input.
FUNC HEIST_CONTROL_TYPE HANDLE_HEIST_PLANNING_MOUSE_BOARD( INT iMousePlayerHighlight )

	// Don't process if the board isn't visible.
	IF g_HeistPlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_BOARD
	AND g_HeistPlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_BOARD_SUB
	AND g_HeistPlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_OVERVIEW
		RETURN HEIST_CONTROL_NONE
	ENDIF
		
	// Only process if the accept button is pressed
	IF NOT IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		RETURN HEIST_CONTROL_NONE
	ENDIF
		
	// Main selection 
		
	//CPRINTLN( DEBUG_HEIST, "iMousePlayerHightlight = ", iMousePlayerHighlight  )	
	
	// Player is highlighting on of the players in the menu.
	IF iMousePlayerHighlight != -1
	
		g_HeistPlanningClient.iSelectedHeistMember = iMousePlayerHighlight
							
		// New player selected
	 	IF iMousePlayerHighlight != g_HeistPlanningClient.sBoardState.iHighlight
		
			g_HeistPlanningClient.sBoardState.iHighlight = iMousePlayerHighlight
			
			// Only the leader can change options
			IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
			
				// Mouse is clicking on Role - Go directly into the sub-menu.
				IF IS_MOUSE_INSIDE_HEIST_PLANNING_AREA( HEIST_PLANNING_MOUSE_ROLE, iMousePlayerHighlight )
					UPDATE_SUB_HIGHLIGHT_ARROWS()
					//RETURN 	HEIST_CONTROL_A
					
				// Mouse is clicking on Cut - Go directly into the sub menu
				ELIF IS_MOUSE_INSIDE_HEIST_PLANNING_AREA( HEIST_PLANNING_MOUSE_CUT, iMousePlayerHighlight )
					g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_COLUMN_CUT
					g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
					UPDATE_SUB_HIGHLIGHT_ARROWS()
					//RETURN 	HEIST_CONTROL_A
				ELSE
					
					// Player is clicking on the slot, but not on the role or cuts - Default to role.
					g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_COLUMN_ROLE
					UPDATE_SUB_HIGHLIGHT_ARROWS()
					
				ENDIF
				
			ENDIF
			
		// Same player, so we're changing values.
		ELSE
		
			// Only leader can change values
			IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
					
				// Launch heist if button is enabled
				IF iMousePlayerHighlight = ci_HEIST_ROW_LAUNCH
					g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
					g_HeistPlanningClient.sBoardState.iHighlight = ci_HEIST_ROW_LAUNCH
					g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_PLANNING_HIGHLIGHT_NULL
					RETURN HEIST_CONTROL_A
				ENDIF
								
				// Role
				IF g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_COLUMN_ROLE
				
					// Directly change to cut
					IF IS_MOUSE_INSIDE_HEIST_PLANNING_AREA( HEIST_PLANNING_MOUSE_CUT, iMousePlayerHighlight )
						g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_COLUMN_CUT
						g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
						UPDATE_SUB_HIGHLIGHT_ARROWS()
						CPRINTLN( DEBUG_HEIST, "Mouse: Role active, Cut clicked" )
						RETURN 	HEIST_CONTROL_A
					ENDIF
				
					// We're already in the role sub-menu
					IF g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
				
						// Decrement
						IF IS_MOUSE_INSIDE_HEIST_PLANNING_AREA( HEIST_PLANNING_MOUSE_ROLE_LEFT, iMousePlayerHighlight )
						AND g_HeistPlanningClient.sBoardState.iLeftArrow = ci_HEIST_ENABLED
							CPRINTLN( DEBUG_HEIST, "Mouse: Role left click" )
							UPDATE_SUB_HIGHLIGHT_ARROWS()
							RETURN 	HEIST_CONTROL_LEFT
						ENDIF
						
						// Increment
						IF IS_MOUSE_INSIDE_HEIST_PLANNING_AREA( HEIST_PLANNING_MOUSE_ROLE_RIGHT, iMousePlayerHighlight )
						AND g_HeistPlanningClient.sBoardState.iRightArrow = ci_HEIST_ENABLED
							CPRINTLN( DEBUG_HEIST, "Mouse: Role right click" )
							UPDATE_SUB_HIGHLIGHT_ARROWS()
							RETURN 	HEIST_CONTROL_RIGHT
						ENDIF
					
					ELSE
					
						// Go into the sub menu
						RETURN HEIST_CONTROL_A
				
					ENDIF
				
				// We're in the CUT column.
				ELIF g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_COLUMN_CUT 
				
					// Player has selected role, so change to that.
					IF IS_MOUSE_INSIDE_HEIST_PLANNING_AREA( HEIST_PLANNING_MOUSE_ROLE, iMousePlayerHighlight )
						g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_COLUMN_ROLE
						g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
						UPDATE_SUB_HIGHLIGHT_ARROWS()
						RETURN 	HEIST_CONTROL_A
						CPRINTLN( DEBUG_HEIST, "Mouse: Cut active, role clicked." )
					ENDIF
			
					// We're already in the sub-menu
					IF g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
					
						// Decrement
						IF IS_MOUSE_INSIDE_HEIST_PLANNING_AREA( HEIST_PLANNING_MOUSE_CUT_LEFT, iMousePlayerHighlight )
							AND g_HeistPlanningClient.sBoardState.iLeftArrow = ci_HEIST_ENABLED
							CPRINTLN( DEBUG_HEIST, "Mouse: Cut left click" )
							UPDATE_SUB_HIGHLIGHT_ARROWS()
							RETURN 	HEIST_CONTROL_LEFT
						ENDIF
						
						// Increment
						IF IS_MOUSE_INSIDE_HEIST_PLANNING_AREA( HEIST_PLANNING_MOUSE_CUT_RIGHT, iMousePlayerHighlight )
							g_HeistPlanningClient.sBoardState.iRightArrow = ci_HEIST_ENABLED
							CPRINTLN( DEBUG_HEIST, "Mouse: Cut right click" )
							UPDATE_SUB_HIGHLIGHT_ARROWS()
							RETURN 	HEIST_CONTROL_RIGHT
						ENDIF
						
					ELSE
						// Go into sub menu.
						UPDATE_SUB_HIGHLIGHT_ARROWS()
						RETURN HEIST_CONTROL_A
					ENDIF
											
				ELSE
					// Nothing selected.
				
					// Change to Role
					IF IS_MOUSE_INSIDE_HEIST_PLANNING_AREA( HEIST_PLANNING_MOUSE_ROLE, iMousePlayerHighlight )
						g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_COLUMN_ROLE
						g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
						UPDATE_SUB_HIGHLIGHT_ARROWS()
						CPRINTLN( DEBUG_HEIST, "Mouse: Role click from none" )
						RETURN 	HEIST_CONTROL_A
					ENDIF
					
					// Directly change to cut
					IF IS_MOUSE_INSIDE_HEIST_PLANNING_AREA( HEIST_PLANNING_MOUSE_CUT, iMousePlayerHighlight )
						g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_COLUMN_CUT
						g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
						UPDATE_SUB_HIGHLIGHT_ARROWS()
						CPRINTLN( DEBUG_HEIST, "Mouse: Cut click from none" )
						RETURN 	HEIST_CONTROL_A
					ENDIF
			
				ENDIF
				
			// Non-leader selects ready/unready
			ELSE
				CPRINTLN( DEBUG_HEIST, "Mouse: Click player - ready/unready" )
				RETURN HEIST_CONTROL_A
			ENDIF
			
		ENDIF
											
	ENDIF
			
	RETURN HEIST_CONTROL_NONE
	
ENDFUNC


/// PURPOSE:
///    Handles map interactions for mouse heist controls
/// RETURNS:
///    
FUNC HEIST_CONTROL_TYPE HANDLE_HEIST_PLANNING_MOUSE_MAP()

	IF g_HeistPlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_MAP
		RETURN HEIST_CONTROL_NONE
	ENDIF
	
	IF eMouseTodoItemState != MOUSE_TODO_ITEMS_STATE_COMPLETE
		CPRINTLN(DEBUG_INTERNET, "HANDLE_HEIST_PLANNING_MOUSE_MAP - Can't do mouse support for TODO items, rules not initialised yet")
		RETURN HEIST_CONTROL_NONE
	ENDIF
	
	//DISPLAY_TEXT_WITH_NUMBER( 0.2, 0.4, "NUMBER", g_HeistPlanningClient.iPostitNumber )
	

	INT iTodoItem =	GET_HEIST_PLANNING_MOUSE_HIGHLIGHTED_RULE()
	
	
	IF iTodoItem != -1 
		SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
		
		//DISPLAY_TEXT_WITH_NUMBER( 0.2, 0.5, "NUMBER", iTodoItemPostItIndex[iTodoItem] )
	
		IF IS_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
		AND g_HeistPlanningClient.sBoardState.iSubHighlight != iTodoItem
			g_HeistPlanningClient.sBoardState.iSubHighlight = iTodoItem
			
			IF iTodoItemPostItIndex[iTodoItem] != -1
				g_HeistPlanningClient.iPostitNumber = iTodoItemPostItIndex[iTodoItem]
			ENDIF
			
			g_HeistPlanningClient.sHeistDataCache.bDoUpdateMapLayers = TRUE	
			
		ENDIF
				
	ENDIF
	
	RETURN HEIST_CONTROL_NONE

ENDFUNC

/// PURPOSE:
///     Handles detecting when the mouse is in the right area to switch between the planning board and the map, and vice-versa
/// PARAMS:
///    fSplit - offset from the border between the two boards.
///    bLeft - detect left or right board.
/// RETURNS:
///    
FUNC BOOL IS_HEIST_PLANNING_MOUSE_BOARD_SWAP_HIGHLIGHTED( FLOAT fSplit, BOOL bLeft )
	
	VECTOR	vSplitter = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(g_HeistSharedClient.vBoardPosition, g_HeistSharedClient.vBoardRotation.z, <<fSplit, 0.0, -g_HeistSharedClient.vWorldSize.y / 2 >> )
	VECTOR vSplitterOnScreenPos
	
	IF NOT GET_SCREEN_COORD_FROM_WORLD_COORD(vSplitter, vSplitterOnScreenPos.x, vSplitterOnScreenPos.y)
		vSplitterOnScreenPos.x = -1
	ENDIF
	
	
	//DRAW_DEBUG_SPHERE(vSplitter, 0.1)
		
	IF vSplitterOnScreenPos.x >= 0.0 
		IF (g_fMenuCursorX < vSplitterOnScreenPos.x AND bLeft = TRUE)	
		OR (g_fMenuCursorX > vSplitterOnScreenPos.x AND bLeft = FALSE)	
			IF g_fMenuCursorY < 0.9
			//IF NOT IS_MOUSE_ROLLED_OVER_INSTRUCTIONAL_BUTTONS()
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
		
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Handles map interactions for mouse heist controls
/// RETURNS:
///    Heist control type used by the main script
FUNC HEIST_CONTROL_TYPE HANDLE_HEIST_PLANNING_MOUSE_STATS()

	IF g_HeistPlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_STATS
		RETURN HEIST_CONTROL_NONE
	ENDIF
	
	// Scroll wheel changes character up
	IF IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
		RETURN HEIST_CONTROL_UP
	ENDIF
	
	// Scrollwheel changes character down
	IF IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
		RETURN HEIST_CONTROL_DOWN
	ENDIF
	
	// Clicking on player changes character (equiv to next)
	IF IS_MOUSE_INSIDE_HEIST_PLANNING_AREA( HEIST_PLANNING_MOUSE_STATS_PLAYER )
		SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			RETURN HEIST_CONTROL_DOWN
		ENDIF
	ENDIF
	
	// Clicking right of player gets next player
	IF IS_MOUSE_INSIDE_HEIST_PLANNING_AREA( HEIST_PLANNING_MOUSE_STATS_PLAYER_NEXT )
		SET_MOUSE_CURSOR_STYLE( MOUSE_CURSOR_STYLE_ARROW_RIGHT)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			RETURN HEIST_CONTROL_DOWN
		ENDIF
		
	ENDIF
	
	// Clicking left of player gets previous character
	IF IS_MOUSE_INSIDE_HEIST_PLANNING_AREA( HEIST_PLANNING_MOUSE_STATS_PLAYER_PREV )
		SET_MOUSE_CURSOR_STYLE( MOUSE_CURSOR_STYLE_ARROW_LEFT)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			RETURN HEIST_CONTROL_UP
		ENDIF
	ENDIF
		
	// Going back
	IF g_fMenuCursorY < 0.13
		SET_MOUSE_CURSOR_STYLE( MOUSE_CURSOR_STYLE_ARROW_UP)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			RETURN HEIST_CONTROL_B
		ENDIF
	ENDIF
	
	// Going back
	IF g_fMenuCursorX < 0.1
		SET_MOUSE_CURSOR_STYLE( MOUSE_CURSOR_STYLE_ARROW_LEFT)
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
			RETURN HEIST_CONTROL_B
		ENDIF
	ENDIF
	
	// Back on right mouse click
	IF IS_DISABLED_CONTROL_PRESSED( FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		RETURN HEIST_CONTROL_B
	ENDIF
	
	RETURN HEIST_CONTROL_NONE

ENDFUNC


/// PURPOSE:
///    Handles mouse interaction on the heist planning menu
/// RETURNS:
///    The heist control type used by the main script
FUNC HEIST_CONTROL_TYPE HANDLE_HEIST_PLANNING_MOUSE()

	INT iMousePlayerHighlight = -1
	
	IF IS_WARNING_MESSAGE_ACTIVE()
		RETURN HEIST_CONTROL_NONE
	ENDIF
			
	//HEIST_CONTROL_TYPE hctInput = HEIST_CONTROL_NONE
	
	// We only want the mouse cursor to work unless we say otherwise
	SET_INPUT_EXCLUSIVE( FRONTEND_CONTROL, INPUT_CURSOR_X)
	SET_INPUT_EXCLUSIVE( FRONTEND_CONTROL, INPUT_CURSOR_Y)
	
	DISABLE_CONTROL_ACTION( FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
	DISABLE_CONTROL_ACTION( FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
	DISABLE_CONTROL_ACTION( PLAYER_CONTROL, INPUT_SNIPER_ZOOM)
	DISABLE_CONTROL_ACTION( FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
	DISABLE_CONTROL_ACTION( FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
	
	SET_MOUSE_CURSOR_THIS_FRAME()
	UPDATE_MENU_CURSOR_GLOBALS()
	
	SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW)
	
//	DISPLAY_TEXT_WITH_NUMBER( 0.2, 0.1, "NUMBER", g_HeistPlanningClient.sBoardState.iHighlight )
//	DISPLAY_TEXT_WITH_NUMBER( 0.2, 0.2, "NUMBER", g_HeistPlanningClient.sBoardState.iSubHighlight )
//	DISPLAY_TEXT_WITH_NUMBER( 0.2, 0.3, "NUMBER", ENUM_TO_INT(g_HeistPlanningClient.sBoardState.eFocusPoint) )
	
	// Mouse cursors when focusing on the planning board
	IF g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
	OR g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
	OR g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_OVERVIEW
	
		// Scrollwheel up
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
			g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
			g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_PLANNING_HIGHLIGHT_NULL
			RETURN HEIST_CONTROL_UP
		ENDIF
		
		// Scrollwheel down
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
			g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
			g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_PLANNING_HIGHLIGHT_NULL
			RETURN HEIST_CONTROL_DOWN
		ENDIF
	
		iMousePlayerHighlight = GET_HEIST_PLANNING_MOUSE_HIGHLIGHTED_PLAYER()
				
		// Launch button
		IF iMousePlayerHighlight = ci_HEIST_ROW_LAUNCH
		AND g_HeistPlanningClient.sBoardState.iHighlight = ci_HEIST_ROW_LAUNCH
		AND g_HeistPlanningClient.sHeistDataCache.sMiscCached.bLaunchButtonEnabled

			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				RETURN HEIST_CONTROL_A
			ENDIF

		// Stats
		ELIF IS_MOUSE_INSIDE_HEIST_PLANNING_AREA( HEIST_PLANNING_MOUSE_STATS )
		
			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
				RETURN HEIST_CONTROL_Y
			ENDIF
		
		// Player Menu
		ELIF iMousePlayerHighlight > -1
			
			SET_MOUSE_CURSOR_STYLE( MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER )
			
			RETURN HANDLE_HEIST_PLANNING_MOUSE_BOARD( iMousePlayerHighlight)
			
		// Go to map
		ELIF IS_HEIST_PLANNING_MOUSE_BOARD_SWAP_HIGHLIGHTED(0.1, TRUE)
			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_ARROW_LEFT)
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
				g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_PLANNING_HIGHLIGHT_NULL
				RETURN HEIST_CONTROL_LB
			ENDIF			
		
		ELIF IS_MOUSE_INSIDE_HEIST_PLANNING_AREA( HEIST_PLANNING_MOUSE_RULES )
			SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
				g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_PLANNING_HIGHLIGHT_NULL
				RETURN HEIST_CONTROL_LB
			ENDIF			
		ENDIF
		
		// Map key pressed - different input on PC so we can use the same key to do both.
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
			g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
			g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_PLANNING_HIGHLIGHT_NULL
			RETURN HEIST_CONTROL_LB
		ENDIF
	
	// Map 
	ELIF g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_MAP
		
		// Mouse click
		IF IS_HEIST_PLANNING_MOUSE_BOARD_SWAP_HIGHLIGHTED(-0.6, FALSE)
			SET_MOUSE_CURSOR_STYLE( MOUSE_CURSOR_STYLE_ARROW_RIGHT)
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
				RETURN HEIST_CONTROL_RB
			ENDIF
		ENDIF
		
		// Keyboard press - different input on PC
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
		OR IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
			RETURN HEIST_CONTROL_RB
		ENDIF
		
		// Scrollwheel up
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
			RETURN HEIST_CONTROL_UP
		ENDIF
		
		// Scrollwheel down
		IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
			RETURN HEIST_CONTROL_DOWN
		ENDIF
		
		// Handle mouse click selection of the map items.
		HANDLE_HEIST_PLANNING_MOUSE_MAP()
	
	// Stats interactions
	ELIF g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_STATS
					
		RETURN HANDLE_HEIST_PLANNING_MOUSE_STATS()
		
	ENDIF
	
	// Go back is disabled on the map to prevent the alert screen from appearing.
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
	AND g_HeistPlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_MAP
		RETURN HEIST_CONTROL_B
	ENDIF
			 
	RETURN HEIST_CONTROL_NONE

ENDFUNC


/// PURPOSE:
///    Maintain the inputs (controller), maintain the camera positioning (interp to target location, etc) and general state management driven
///    by front-end controls.
PROC UPDATE_HEIST_INPUTS_AND_CAMERA()

	
	// Ensure the camera keeps moving smoothly to it's target destination.
	
	// No camera or zoom for keyboard and mouse - mouse movement is used for UI support.
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		MAINTAIN_SMOOTH_CAM_TRANSITION(g_HeistPlanningClient.sBoardCamData, FALSE, FALSE)
	ELSE
		MAINTAIN_SMOOTH_CAM_TRANSITION(g_HeistPlanningClient.sBoardCamData, TRUE, TRUE)
	ENDIF
	
	// You have to check that the leader is in configuration mode before forcing an arrow update. This is because
	// when the camera zooms out, the highlight value is set to -1. The highlight value is required for the below method
	// to work.
	IF g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
	OR g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
		UPDATE_SUB_HIGHLIGHT_ARROWS()
	ENDIF
	
	IF NOT g_HeistPlanningClient.bHaveDoneHelp
		EXIT
	ENDIF
	
	HEIST_CONTROL_TYPE hctInput = HEIST_CONTROL_NONE
			
	//DISPLAY_TEXT_WITH_NUMBER( 0.2, 0.1, "NUMBER", g_HeistPlanningClient.sBoardState.iHighlight )
	//DISPLAY_TEXT_WITH_NUMBER( 0.2, 0.2, "NUMBER", g_HeistPlanningClient.sBoardState.iSubHighlight )
	//DISPLAY_TEXT_WITH_NUMBER( 0.2, 0.3, "NUMBER", ENUM_TO_INT(g_HeistPlanningClient.sBoardState.eFocusPoint) )
			
	// Mouse and keyboard support
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		hctInput = HANDLE_HEIST_PLANNING_MOUSE()
	ENDIF
	
	// Gamepad control if no mouse detected.
	IF hctInput = HEIST_CONTROL_NONE
		hctInput = GET_HEIST_INPUTS_THIS_FRAME_ONLY(g_HeistPlanningClient.sBoardInputData)
	ENDIF

	SWITCH hctInput
	
		DEFAULT
		
			//SCRIPT_ASSERT("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_INPUTS_AND_CAMERA - Heist control returned was not marked as valid in the HEIST_CONTROL_TYPE enum!")
			//PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_INPUTS_AND_CAMERA - Heist control returned was not marked as valid in the HEIST_CONTROL_TYPE enum! Value: ", ENUM_TO_INT(GET_HEIST_INPUTS_THIS_FRAME_ONLY(g_HeistPlanningClient.sBoardInputData)))
		
			BREAK
	
		CASE HEIST_CONTROL_UP
		
			//IF g_HeistPlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_OVERVIEW
				// Scroll up an entry.  
				UPDATE_PLAYER_SELECTION_DETAILS(HEIST_HIGHLIGHT_UP)
			//ENDIF
		
			BREAK
			
		CASE HEIST_CONTROL_DOWN
		
			//IF g_HeistPlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_OVERVIEW
				// Scroll down an entry.  
				UPDATE_PLAYER_SELECTION_DETAILS(HEIST_HIGHLIGHT_DOWN)
			//ENDIF
		
			BREAK
			
		CASE HEIST_CONTROL_LEFT
		
			IF g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
			OR g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
				// Scroll left across a specific player's details. 
				UPDATE_PLAYER_SELECTION_DETAILS(HEIST_HIGHLIGHT_SUB_LEFT)
			ENDIF
			
			BREAK
			
		CASE HEIST_CONTROL_RIGHT
		
			IF g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
			OR g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
				// Scroll right across a specific player's details. 
				UPDATE_PLAYER_SELECTION_DETAILS(HEIST_HIGHLIGHT_SUB_RIGHT)
			ENDIF
		
			BREAK
			
		CASE HEIST_CONTROL_LB
		
			//IF g_HeistPlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_OVERVIEW
				HEIST_CAM_MOVE_LEFT()
			//ENDIF
		
			BREAK
			
		CASE HEIST_CONTROL_RB
		
			//IF g_HeistPlanningClient.sBoardState.eFocusPoint != HEIST_FOCUS_OVERVIEW
				HEIST_CAM_MOVE_RIGHT()
			//ENDIF
		
			BREAK
			
		CASE HEIST_CONTROL_X
		
		
			SWITCH g_HeistPlanningClient.sBoardState.eFocusPoint
		
				CASE HEIST_FOCUS_OVERVIEW
				
					IF NOT IS_PLAYER_IN_HEIST_READY_TO_LAUNCH(PLAYER_ID())
						
						IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
							SET_ALL_HEIST_HIGHLIGHTS(g_HeistPlanningClient.sBoardState, 0, IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())) // Set the highlighting to look at the leader.
						ELSE
							// Manually set the highlighting here as it's not a simple case of all x or all y.
							g_HeistPlanningClient.sBoardState.iHighlight = 0
							g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_PLANNING_HIGHLIGHT_NULL
							g_HeistPlanningClient.sBoardState.iLeftArrow = 0
							g_HeistPlanningClient.sBoardState.iRightArrow = 0
						ENDIF
						
						HEIST_CAM_MOVE_IN() // Force the camera position inward a step.
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_HEIST_INPUTS_AND_CAMERA - HEIST_CONTROL_X - HEIST_FOCUS_OVERVIEW - Playing Zoom_In.")
						#ENDIF
						PLAY_SOUND_FRONTEND(-1,"Zoom_In","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						
					ENDIF
				
					BREAK
					
			ENDSWITCH
		
			BREAK
			
		CASE HEIST_CONTROL_Y
		
			SWITCH g_HeistPlanningClient.sBoardState.eFocusPoint
			
				CASE HEIST_FOCUS_BOARD
				
					IF g_HeistPlanningClient.sBoardState.iHighlight != ci_HEIST_ROW_LAUNCH
				
						HEIST_CAM_MOVE_TO_STATS()
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_HEIST_INPUTS_AND_CAMERA - HEIST_CONTROL_Y - HEIST_FOCUS_BOARD - Playing Zoom_In.")
						#ENDIF
						PLAY_SOUND_FRONTEND(-1,"Zoom_In","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						
					ELSE
					
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_HEIST_INPUTS_AND_CAMERA - HEIST_CONTROL_Y - HEIST_FOCUS_BOARD - Attempted to view player stats card while on launch button.")
						#ENDIF
						
					ENDIF
						
					BREAK
					
				CASE HEIST_FOCUS_BOARD_SUB
				
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_HEIST_INPUTS_AND_CAMERA - HEIST_CONTROL_Y - HEIST_FOCUS_BOARD_SUB - Playing Highlight_Cancel.")
					#ENDIF
					PLAY_SOUND_FRONTEND(-1,"Highlight_Cancel","DLC_HEIST_PLANNING_BOARD_SOUNDS")
				
					SWITCH g_HeistPlanningClient.sBoardState.iSubHighlight
					
						CASE ci_HEIST_COLUMN_ROLE
						
							IF GET_HEIST_MEMBER_TEAM_NUMBER(g_HeistPlanningClient.iSelectedHeistMember) != INVALID_HEIST_DATA
				
								#IF IS_DEBUG_BUILD
									PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_INPUTS_AND_CAMERA - Unassigning team member: ", g_HeistPlanningClient.iSelectedHeistMember)
								#ENDIF
								
								REVOKE_CURRENTLY_SELECTED_MEMBERS_TEAM()
							
							ENDIF
								
							BREAK
							
						CASE ci_HEIST_COLUMN_CUT
							
							IF GET_CUT_INFO_FOR_PLAYER(g_HeistPlanningClient.iSelectedHeistMember) != 0
							
								#IF IS_DEBUG_BUILD
									PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_INPUTS_AND_CAMERA - Clearing cut for team member: ", g_HeistPlanningClient.iSelectedHeistMember)
								#ENDIF
							
								CLEAR_CURRENTLY_SELECTED_MEMBERS_CUT()
							ENDIF
						
							BREAK
							
						CASE ci_HEIST_COLUMN_OUTFIT
						
//							#IF IS_DEBUG_BUILD
//								PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_INPUTS_AND_CAMERA - Clearing outfit on member: ", g_HeistPlanningClient.iSelectedHeistMember)
//							#ENDIF
//								
//							CLEAR_CURRENTLY_SELECTED_MEMBERS_OUTFIT()

							BREAK
	
					ENDSWITCH
					
					BREAK
			
			ENDSWITCH
			
			BREAK
			
		CASE HEIST_CONTROL_B
		
			SWITCH g_HeistPlanningClient.sBoardState.eFocusPoint
				
				CASE HEIST_FOCUS_OVERVIEW
				
					// Overview has been disabled, no use for this currently.	

					BREAK
					
				// Cancel changes made.	
				CASE HEIST_FOCUS_BOARD_SUB  
					
					RESTORE_SAVED_HEIST_CONFIG_OPTION(g_HeistPlanningClient.sBoardState.iHighlight, g_HeistPlanningClient.sBoardState.iSubHighlight)
				
					CLEAN_SAVED_HEIST_CONFIG_OPTION(g_HeistPlanningClient.sBoardState.iHighlight, ci_HEIST_COLUMN_ROLE)
					CLEAN_SAVED_HEIST_CONFIG_OPTION(g_HeistPlanningClient.sBoardState.iHighlight, ci_HEIST_COLUMN_CUT)
					
					SET_HEIST_UPDATE_MAP_ALL(TRUE)
				                                                                                                    
					g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
					g_HeistPlanningClient.bUpdateInstButtons = TRUE
										
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_HEIST_INPUTS_AND_CAMERA - HEIST_CONTROL_B - HEIST_FOCUS_BOARD_SUB - Playing Highlight_Cancel.")
					#ENDIF
					PLAY_SOUND_FRONTEND(-1,"Highlight_Cancel","DLC_HEIST_PLANNING_BOARD_SOUNDS")
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_5")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_6")
						CLEAR_HELP()
					ENDIF
				
					BREAK 
										
				// Back out of stats view.
				CASE HEIST_FOCUS_STATS 
				
					HEIST_CAM_MOVE_TO_BOARD()
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_HEIST_INPUTS_AND_CAMERA - HEIST_CONTROL_B - HEIST_FOCUS_STATS - Playing Zoom_Out.")
					#ENDIF
					PLAY_SOUND_FRONTEND(-1,"Zoom_Out","DLC_HEIST_PLANNING_BOARD_SOUNDS")
				
					BREAK 
					
				DEFAULT
				
					// Are you sure you want to exit th heist planning board?
					SET_HEIST_FINALE_STATE(HEIST_FLOW_EXIT_PENDING)
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_HEIST_INPUTS_AND_CAMERA - HEIST_CONTROL_B - DEFAULT - Playing Highlight_Move.")
					#ENDIF
					PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
				
					BREAK
			
			ENDSWITCH
		
			BREAK
			
		CASE HEIST_CONTROL_A
		
			SWITCH g_HeistPlanningClient.sBoardState.eFocusPoint
		
				DEFAULT
				
					IF NOT IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
					
						IF NOT g_HeistPlanningClient.bLaunchTimerExpired
						
							IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHeistReadyState = ENUM_TO_INT(STATUS_NOT_READY)
								GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHeistReadyState = ENUM_TO_INT(STATUS_READY)
								
								#IF IS_DEBUG_BUILD
									PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_HEIST_INPUTS_AND_CAMERA - HEIST_CONTROL_A - DEFAULT - Playing Pen_Tick.")
								#ENDIF
								PLAY_SOUND_FRONTEND(-1,"Pen_Tick","DLC_HEIST_PLANNING_BOARD_SOUNDS")
								
								#IF IS_DEBUG_BUILD
									PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_INPUTS_AND_CAMERA - Member flipped ready state. New value: READY")
								#ENDIF
							ELSE
								GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHeistReadyState = ENUM_TO_INT(STATUS_NOT_READY)
								
								#IF IS_DEBUG_BUILD
									PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_HEIST_INPUTS_AND_CAMERA - HEIST_CONTROL_A - DEFAULT - Playing Highlight_Cancel.")
								#ENDIF
								PLAY_SOUND_FRONTEND(-1,"Highlight_Cancel","DLC_HEIST_PLANNING_BOARD_SOUNDS")
								
								#IF IS_DEBUG_BUILD
									PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_INPUTS_AND_CAMERA - Member flipped ready state. New value: NOT READY")
								#ENDIF
							ENDIF
						
						ENDIF
					
					ENDIF
				
					BREAK

				CASE HEIST_FOCUS_BOARD
				
					IF NOT IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
					
						IF NOT g_HeistPlanningClient.bLaunchTimerExpired
					
							IF GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHeistReadyState = ENUM_TO_INT(STATUS_NOT_READY)
								GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHeistReadyState = ENUM_TO_INT(STATUS_READY)
								
								#IF IS_DEBUG_BUILD
									PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_HEIST_INPUTS_AND_CAMERA - HEIST_CONTROL_A - HEIST_FOCUS_BOARD - Playing Pen_Tick.")
								#ENDIF
								PLAY_SOUND_FRONTEND(-1,"Pen_Tick","DLC_HEIST_PLANNING_BOARD_SOUNDS")
								
								#IF IS_DEBUG_BUILD
									PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_INPUTS_AND_CAMERA - Member flipped ready state. New value: READY")
								#ENDIF
							ELSE
								GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHeistReadyState = ENUM_TO_INT(STATUS_NOT_READY)
								
								#IF IS_DEBUG_BUILD
									PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_HEIST_INPUTS_AND_CAMERA - HEIST_CONTROL_A - HEIST_FOCUS_BOARD - Playing Highlight_Cancel.")
								#ENDIF
								PLAY_SOUND_FRONTEND(-1,"Highlight_Cancel","DLC_HEIST_PLANNING_BOARD_SOUNDS")
								
								#IF IS_DEBUG_BUILD
									PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_INPUTS_AND_CAMERA - Member flipped ready state. New value: NOT READY")
								#ENDIF
							ENDIF
						
						ENDIF
						
						// This needs to be here to stop members dropping to leader only functions.
						EXIT
					
					ENDIF
					
					// Check if LAUNCH HEIST before entering edit mode.
					IF g_HeistPlanningClient.sBoardState.iHighlight = ci_HEIST_ROW_LAUNCH
					
						IF g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] > 0
							#IF IS_DEBUG_BUILD
								PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_INPUTS_AND_CAMERA - WARNING! Leader has clicked LAUNCH HEIST without first assigning all of the cut.")
							#ENDIF
							GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHeistReadyState = ENUM_TO_INT(STATUS_NOT_READY)
							SET_HEIST_FINALE_STATE(HEIST_FLOW_CUT_PENDING)
							EXIT
						ENDIF
					
						GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].iHeistReadyState = ENUM_TO_INT(STATUS_READY)
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_INPUTS_AND_CAMERA - WARNING! Leader has clicked LAUNCH HEIST. Progressing leader's status to ready to auto-launch detection.")
						#ENDIF
						
						EXIT
					ENDIF
					
//					IF g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_COLUMN_OUTFIT
//						IF g_HeistPlanningClient.bNoOutfitsAvailable
//						
//							#IF IS_DEBUG_BUILD
//								PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_INPUTS_AND_CAMERA - WARNING! Leader attempted to modify outfit when there was no additional outfits available!")
//							#ENDIF
//						
//							EXIT
//						ENDIF
//					ENDIF
					
					// CLEAR_HELP() // AMEC - Not sure why this was ever added?
				
					IF NOT IS_PLAYER_IN_HEIST_READY_TO_LAUNCH(PLAYER_ID())
					OR g_HeistPlanningClient.bLaunchTimerExpired
					
						g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD_SUB
						g_HeistPlanningClient.bUpdateInstButtons = TRUE
						
						// Save the existing index value for selected team here.
						SAVE_SELECTED_VARIABLE_OPTION(g_HeistPlanningClient.sBoardState.iHighlight, g_HeistPlanningClient.sBoardState.iSubHighlight)
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_HEIST_INPUTS_AND_CAMERA - HEIST_CONTROL_A - HEIST_FOCUS_BOARD - Playing Highlight_Move.")
						#ENDIF
						PLAY_SOUND_FRONTEND(-1,"Highlight_Move","DLC_HEIST_PLANNING_BOARD_SOUNDS")
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_INPUTS_AND_CAMERA - LEADER is ENTERING edit mode. SubIndex: ", g_HeistPlanningClient.sBoardState.iSubHighlight)
						#ENDIF
						
						CLEAR_HELP()
					
					ENDIF
				
					BREAK
					
				// Accept the selected value.
				CASE HEIST_FOCUS_BOARD_SUB
					
					CLEAN_SAVED_HEIST_CONFIG_OPTION(g_HeistPlanningClient.sBoardState.iHighlight, ci_HEIST_COLUMN_ROLE)
					CLEAN_SAVED_HEIST_CONFIG_OPTION(g_HeistPlanningClient.sBoardState.iHighlight, ci_HEIST_COLUMN_CUT)
//					CLEAN_SAVED_HEIST_CONFIG_OPTION(g_HeistPlanningClient.sBoardState.iHighlight, ci_HEIST_COLUMN_OUTFIT)
				
					g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD
					g_HeistPlanningClient.bUpdateInstButtons = TRUE
					
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH_AUDIO] - UPDATE_HEIST_INPUTS_AND_CAMERA - HEIST_CONTROL_A - HEIST_FOCUS_BOARD_SUB - Playing Highlight_Accept.")
					#ENDIF
					PLAY_SOUND_FRONTEND(-1,"Highlight_Accept","DLC_HEIST_PLANNING_BOARD_SOUNDS")
				
					#IF IS_DEBUG_BUILD
						PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_INPUTS_AND_CAMERA - LEADER is EXITING edit mode.")
					#ENDIF
					
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_5")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_NOTE_6")
						CLEAR_HELP()
					ENDIF
				
					BREAK
					
			ENDSWITCH
		
			BREAK
			
		CASE HEIST_CONTROL_SELECT
		
			SWITCH g_HeistPlanningClient.sBoardState.eFocusPoint
				
				// Only allow stats card view when on the board view mode.
				CASE HEIST_FOCUS_BOARD  
				CASE HEIST_FOCUS_STATS
				
					IF g_HeistPlanningClient.sBoardState.iHighlight != ci_HEIST_ROW_LAUNCH
				
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_INPUTS_AND_CAMERA - HEIST_CONTROL_SELECT - HEIST_FOCUS_BOARD/STATS - Triggered inspect selected player.")
						#ENDIF
						
						PLAYER_INDEX playerID
						GAMER_HANDLE ghSelectedPlayer
						
						IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
							playerID = INT_TO_PLAYERINDEX(g_HeistPlanningClient.iPlayerOrder[g_HeistPlanningClient.iSelectedHeistMember])
						ELSE
							playerID = INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[g_HeistPlanningClient.iSelectedHeistMember])
						ENDIF
						
	                    ghSelectedPlayer = GET_GAMER_HANDLE_PLAYER(playerID)
	                    PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET")
	                    NETWORK_SHOW_PROFILE_UI(ghSelectedPlayer)
						
					ELSE
						
						#IF IS_DEBUG_BUILD
							PRINTLN("[AMEC][HEIST_LAUNCH] - UPDATE_HEIST_INPUTS_AND_CAMERA - HEIST_CONTROL_SELECT - HEIST_FOCUS_BOARD/STATS - Triggered inspect selected player on LAUNCH HEIST button, do nothing.")
						#ENDIF
					
					ENDIF
				
					BREAK
					
			ENDSWITCH
		
			BREAK
	
	ENDSWITCH
	
ENDPROC


/// PURPOSE:
///    Download the heist cloud data, and store it locally for rapid parsing.
PROC HEIST_START_GET_INIT_DATA()

	IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
//		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_GET_INIT_DATA - Waiting for player to re-enter property...")
		EXIT
	ENDIF
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty < 0
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_GET_INIT_DATA - Waiting for player BD inside property index to be valid. Value: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		EXIT
	ENDIF

	PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_GET_INIT_DATA - Player in property index: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)

	DISPLAY_RADAR(FALSE) 
	
	// Pre-load audio banks.
	IF NOT REQUEST_SCRIPT_AUDIO_BANK("DLC_MPHEIST/HEIST_PLANNING_BOARD")
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_GET_INIT_DATA - Waiting for heist audio bank to be loaded...")
		EXIT
	ENDIF
	
//	IF NOT DOWNLOAD_ALL_PHOTOS_FOR_HEIST_PLANNING_BOARD()
//		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_GET_INIT_DATA - Waiting for DOWNLOAD_ALL_PHOTOS_FOR_HEIST_PLANNING_BOARD()...")
//		EXIT
//	ENDIF
	
	#IF IS_DEBUG_BUILD
	// DEBUG ONLY - Print the list of available outfits for this heist.
//	PRINT_AVAILABLE_OUTFITS()
	PRINT_AVAILABLE_GEAR()
	DEBUG_PRINTCALLSTACK()
	#ENDIF

	// Process mission details.
	// Get the coords for TEAM 0 start location, and place them on the map.
	
	INT iTeam, iVeh, iPed, iObj, iLoc
	
	FOR iTeam = 0 TO (FMMC_MAX_TEAMS-1)
		
		FOR iVeh = 0 TO (FMMC_MAX_VEHICLES-1)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iPriority[iTeam] < FMMC_MAX_RULES 
				IF g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRule[iTeam]!= FMMC_OBJECTIVE_LOGIC_NONE 
					IF NOT IS_BIT_SET(g_HeistPlanningClient.iHeistPlanningBitset, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iPriority[iTeam])
						SET_BIT(g_HeistPlanningClient.iHeistPlanningBitset, g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iPriority[iTeam])
					ENDIF
				    g_HeistPlanningClient.iObjectiveTarget[iTeam][g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iPriority[iTeam]] = ci_HEIST_TARGET_VEHICLE
				    g_HeistPlanningClient.iObjectiveRule[iTeam][g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iPriority[iTeam]] = g_FMMC_STRUCT_ENTITIES.sPlacedVehicle[iVeh].iRule[iTeam]
					 
				ENDIF
			ENDIF
		ENDFOR
		
		FOR iPed = 0 TO (FMMC_MAX_PEDS-1)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPriority[iTeam] < FMMC_MAX_RULES 
				IF g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iRule[iTeam]!= FMMC_OBJECTIVE_LOGIC_NONE 
					IF NOT IS_BIT_SET(g_HeistPlanningClient.iHeistPlanningBitset, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPriority[iTeam])
						SET_BIT(g_HeistPlanningClient.iHeistPlanningBitset, g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPriority[iTeam])
					ENDIF
				    g_HeistPlanningClient.iObjectiveTarget[iTeam][g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPriority[iTeam]] = ci_HEIST_TARGET_PED
				    g_HeistPlanningClient.iObjectiveRule[iTeam][g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iPriority[iTeam]] = g_FMMC_STRUCT_ENTITIES.sPlacedPed[iPed].iRule[iTeam]
					
				ENDIF
			ENDIF
		ENDFOR
		
		FOR iObj = 0 TO (FMMC_MAX_NUM_OBJECTS-1)
			IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPriority[iTeam] < FMMC_MAX_RULES 
				IF g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iRule[iTeam]!= FMMC_OBJECTIVE_LOGIC_NONE
					IF NOT IS_BIT_SET(g_HeistPlanningClient.iHeistPlanningBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPriority[iTeam])
						SET_BIT(g_HeistPlanningClient.iHeistPlanningBitset, g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPriority[iTeam])
					ENDIF
				    g_HeistPlanningClient.iObjectiveTarget[iTeam][g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPriority[iTeam]] = ci_HEIST_TARGET_OBJECT
				    g_HeistPlanningClient.iObjectiveRule[iTeam][g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iPriority[iTeam]] = g_FMMC_STRUCT_ENTITIES.sPlacedObject[iObj].iRule[iTeam]
					
				ENDIF
			ENDIF
		ENDFOR
		
		FOR iLoc = 0 TO (FMMC_MAX_GO_TO_LOCATIONS-1)
			IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iPriority[iTeam] < FMMC_MAX_RULES
				IF g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iRule[iTeam]!= FMMC_OBJECTIVE_LOGIC_NONE 
				
//					IF IS_VECTOR_ZERO(g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].vLoc[iTeam])
//						#IF IS_DEBUG_BUILD
//							PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_GET_INIT_DATA - ERROR! Mission 'GoTo Location' was parsed as VALID but contained no coordinates!")
//						#ENDIF
//					ENDIF
					
					IF NOT IS_BIT_SET(g_HeistPlanningClient.iHeistPlanningBitset, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iPriority[iTeam])
						SET_BIT(g_HeistPlanningClient.iHeistPlanningBitset, g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iPriority[iTeam])
					ENDIF
				    g_HeistPlanningClient.iObjectiveTarget[iTeam][g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iPriority[iTeam]] = ci_HEIST_TARGET_LOCATION
				    g_HeistPlanningClient.iObjectiveRule[iTeam][g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iPriority[iTeam]] = g_FMMC_STRUCT_ENTITIES.sGotoLocationData[iLoc].iRule[iTeam]
				ENDIF
			ENDIF
		ENDFOR

	ENDFOR
	
	PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_GET_INIT_DATA - Mission data was successfully parsed.")
	SET_HEIST_FINALE_STATE(HEIST_FLOW_SERVER_INIT)


ENDPROC

/// PURPOSE:
///    Sets any local settings about the heist finale from the local player to be read by remote players (ie. have we seen the cutscene).
PROC SET_HEIST_FINALE_PLAYER_SETTINGS()
	
	INT iRootContentID = GET_STRAND_ROOT_CONTENT_ID_HASH()
	IF iRootContentID = 0
		iRootContentID = g_FMMC_STRUCT.iRootContentIdHash
	ENDIF
	
	IF HAS_LOCAL_PLAYER_COMPLETED_FINALE_CUTSCENE(g_FMMC_STRUCT.iRootContentIDHash)
		PRINTLN("[AMEC][HEIST_LAUNCH] SET_HEIST_FINALE_PLAYER_SETTINGS - Local player has seen the cutscene")
		SET_BIT(GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(PLAYER_ID())].iBSPlayerHeistSettings, ciHEIST_PLAYER_BD_DONE_FINALE_CUTSCENE)
	ELSE
		PRINTLN("[AMEC][HEIST_LAUNCH] SET_HEIST_FINALE_PLAYER_SETTINGS - Local player has NOT seen the cutscene")
		CLEAR_BIT(GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(PLAYER_ID())].iBSPlayerHeistSettings, ciHEIST_PLAYER_BD_DONE_FINALE_CUTSCENE)
	ENDIF
ENDPROC

/// PURPOSE:
///    Setup the server variables to distribute to all heist members. Also builds the extremely useful iPlayerOrder array here (note: do not try to use it before).
PROC HEIST_START_INIT_SERVER_CONFIG()

	#IF IS_DEBUG_BUILD
	PRINTLN("[AMEC][HEIST_LAUNCH] ********************************** ")
	PRINTLN("[AMEC][HEIST_LAUNCH] 	STARTING HEIST FINALE PLANNING   ")
	PRINTLN("[AMEC][HEIST_LAUNCH] 		LEADER: ",GET_PLAYER_NAME(INT_TO_PLAYERINDEX(g_HeistPlanningClient.iHeistLeaderIndex)))
	PRINTLN("[AMEC][HEIST_LAUNCH] ********************************** ")
	DEBUG_PRINTCALLSTACK()
	#ENDIF
	
	SET_HEIST_FINALE_PLAYER_SETTINGS()
	g_HeistPlanningClient.sHeistDataCache.sMiscCached.iSelectedTeam = INVALID_HEIST_DATA
	
	IF g_HeistPlanningClient.iHeistLeaderIndex != NATIVE_TO_INT(PLAYER_ID()) 
		// Build an initial playerorder list for the clients. This is required for when the clients need to update the
		// other player's net statuses but they cannot rely on the globals being updated in time.
		BUILD_PLAYER_ORDER_LIST(g_HeistPlanningClient.iPlayerOrder)
		SET_HEIST_FINALE_STATE(HEIST_FLOW_CREATE_BOARD)
		
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_SERVER_CONFIG - Heist init config called, I am NOT host. Exiting.")
		#ENDIF
		
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_SERVER_CONFIG - Heist init config called, I am host...")
	#ENDIF
	
	// REQUEST SERVER BD DATA CHANGE FOR BASIC CONFIG
	// using the initial data parsed before, calculate the total number of rules for this heist.
	INT iTotalRules, iTotalTeams, index, iHeistTotalPay
	FLOAT fDifficultyMod
	
	FOR index = 0 TO (FMMC_MAX_RULES-1)
		IF IS_BIT_SET(g_HeistPlanningClient.iHeistPlanningBitset, index)
			iTotalRules++
		ENDIF
	ENDFOR

	iTotalTeams = g_FMMC_STRUCT.iNumberOfTeams
	
	// Declare temporary array and make all values invalid.
	INT iPlayerOrder[MAX_HEIST_PLAYER_SLOTS]
	
	BUILD_PLAYER_ORDER_LIST(g_HeistPlanningClient.iPlayerOrder)
	BUILD_PLAYER_ORDER_LIST(iPlayerOrder)

	iHeistTotalPay = GET_CASH_VALUE_FROM_CREATOR(g_FMMC_STRUCT.iCashReward, TRUE)
	
	IF IS_PLAYER_IN_CORONA()
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_SERVER_CONFIG - IS_PLAYER_IN_CORONA = TRUE, use corona difficulty.")
		SWITCH g_sTransitionSessionOptions.iSelection[ciMISSION_HOST_OPTION_DIFFICULTY]
			CASE DIFF_EASY 		fDifficultyMod = g_sMPTunables.fheist_difficulty_easy 		BREAK
			CASE DIFF_NORMAL 	fDifficultyMod = g_sMPTunables.fheist_difficulty_normal 	BREAK
			CASE DIFF_HARD 		fDifficultyMod = g_sMPTunables.fheist_difficulty_hard 		BREAK
		ENDSWITCH
	ELSE
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_SERVER_CONFIG - IS_PLAYER_IN_CORONA = FALSE, use g_FMMC_STRUCT difficulty.")
		SWITCH g_FMMC_STRUCT.iDifficulity
			CASE DIFF_EASY 		fDifficultyMod = g_sMPTunables.fheist_difficulty_easy 		BREAK
			CASE DIFF_NORMAL 	fDifficultyMod = g_sMPTunables.fheist_difficulty_normal 	BREAK
			CASE DIFF_HARD 		fDifficultyMod = g_sMPTunables.fheist_difficulty_hard 		BREAK
		ENDSWITCH
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_SERVER_CONFIG - Base pay: ", iHeistTotalPay, ", Adjusted for difficulty: ", (iHeistTotalPay * fDifficultyMod), " using modifier: ", fDifficultyMod)
	#ENDIF
	
	iHeistTotalPay = ROUND(iHeistTotalPay * fDifficultyMod)

	FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
		g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index] = HEIST_ROLE_UNASSIGNED
//		g_HeistPlanningClient.iSelectedOutfit[index] = INVALID_HEIST_DATA
		g_HeistPlanningClient.sHeistDataCache.sMiscCached.sDisplacedRole[index] = HEIST_ROLE_UNASSIGNED
	ENDFOR
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_SERVER_CONFIG - Initialised all local data arrays to invalid.")
	#ENDIF
	
	// Set values locally for the host (to reduce input lag later on).
	g_HeistPlanningClient.iHeistTotalPay = iHeistTotalPay
	//g_HeistPlanningClient.iHeistSetupCosts = iHeistTotalCosts
	g_HeistPlanningClient.iTotalRules = iTotalRules
	g_HeistPlanningClient.iTotalTeams = iTotalTeams
	
	HEIST_MEDAL_DATA sMedalData[MAX_HEIST_PLANNING_ROWS]
	
	#IF IS_DEBUG_BUILD
		DEBUG_DUMP_HEIST_STATS_TO_LOG()
	#ENDIF
	
	FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
		sMedalData[index].iPlayerOne 	= GET_MEDAL_DATA_FOR_PLAYER_FROM_MISSION_INDEX(index, ci_HEIST_PRE_PLAN_PLAYER_0)
		sMedalData[index].iPlayerTwo 	= GET_MEDAL_DATA_FOR_PLAYER_FROM_MISSION_INDEX(index, ci_HEIST_PRE_PLAN_PLAYER_1)
		sMedalData[index].iPlayerThree 	= GET_MEDAL_DATA_FOR_PLAYER_FROM_MISSION_INDEX(index, ci_HEIST_PRE_PLAN_PLAYER_2)
		sMedalData[index].iPlayerFour 	= GET_MEDAL_DATA_FOR_PLAYER_FROM_MISSION_INDEX(index, ci_HEIST_PRE_PLAN_PLAYER_3)
	ENDFOR
	
	INT iTotalClients
	PLAYER_INDEX thisPlayer
	
	FOR index = 0 TO (NUM_NETWORK_PLAYERS-1)
	    thisPlayer = INT_TO_PLAYERINDEX(index)
	    IF (IS_NET_PLAYER_OK(thisPlayer, FALSE, FALSE))
			IF thisPlayer <> INVALID_PLAYER_INDEX()
				IF NOT IS_THIS_PLAYER_SCTV_OR_JOINING_AS_SCTV(thisPlayer) 
					iTotalClients++
				ENDIF
		    ENDIF
		ENDIF
	ENDFOR
	
	IF iTotalClients <= 1
		g_HeistPlanningClient.sHeistCut.iCutPercent[ci_HEIST_LEADER] = HEIST_CUT_PERCENT_TOTAL
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_SERVER_CONFIG - iTotalClients: ",iTotalClients,", iPlayerOrder[",ci_HEIST_LEADER,"] cut: ", HEIST_CUT_PERCENT_TOTAL)
		#ENDIF
	ELSE
		IF IS_THIS_A_TUTORIAL_HEIST()
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_SERVER_CONFIG - Setting Fleeca default cut for LEADER to: ", g_sMPTunables.ileader_default_heist_tutorial_finale_cut)
			#ENDIF
			g_HeistPlanningClient.sHeistCut.iCutPercent[ci_HEIST_LEADER] = g_sMPTunables.ileader_default_heist_tutorial_finale_cut
		ELSE
			g_HeistPlanningClient.sHeistCut.iCutPercent[ci_HEIST_LEADER] = g_sMPTunables.iHeists_Leader_Default_Heist_Finale_Cut
		ENDIF

		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_SERVER_CONFIG - iTotalClients: ",iTotalClients,", iPlayerOrder[",ci_HEIST_LEADER,"] cut: ", g_HeistPlanningClient.sHeistCut.iCutPercent[ci_HEIST_LEADER])
		#ENDIF

		FOR index = 1 TO (iTotalClients-1)
			INT iCutPercent = ((HEIST_CUT_PERCENT_TOTAL - g_HeistPlanningClient.sHeistCut.iCutPercent[ci_HEIST_LEADER]) / (iTotalClients-1))
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_SERVER_CONFIG - iTotalClients: ",iTotalClients,", iPlayerOrder[",index,"] cut: ", iCutPercent)
			#ENDIF
		    g_HeistPlanningClient.sHeistCut.iCutPercent[index] = iCutPercent
		ENDFOR
	ENDIF
	
	//B* 2100067, set to 0
	g_HeistPlanningClient.sHeistCut.iCutPercent[MAX_HEIST_PLAYER_SLOTS] = 0
	
	// Only do these things if we're entering the planning board from the corona. This is needed because you can re-eneter the property
	// whilst on a finale mission.
	IF IS_PLAYER_IN_CORONA()
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_SERVER_CONFIG - IS_PLAYER_IN_CORONA() = TRUE, sending new data to HOST.")
		#ENDIF
		
		// Final step: send data to the host.
		BROADCST_SCRIPT_EVENT_HEIST_SETUP_INIT_DATA(iHeistTotalPay, iTotalRules, iTotalTeams, iPlayerOrder) //, g_HeistPlanningClient.iSelectedOutfit)
		BROADCAST_SCRIPT_EVENT_HEIST_MEDAL_DATA(sMedalData)
		BROADCST_SCRIPT_EVENT_HEIST_UPDATE_CUTS(g_HeistPlanningClient.sHeistCut.iCutPercent)

	ELSE
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_SERVER_CONFIG - IS_PLAYER_IN_CORONA() = FALSE, not sending new data to HOST.")
		#ENDIF
	ENDIF
	
	SET_HEIST_FINALE_STATE(HEIST_FLOW_CREATE_BOARD)

ENDPROC

/// PURPOSE:
///    Function to update all players with the medal data for finale planning (from staggered loop)
PROC PROCESS_RESEND_OF_HEIST_MEDAL_DATA()
	
	IF GET_HEIST_FINALE_STATE() >= HEIST_FLOW_CREATE_BOARD
	
		PRINTLN("[AMEC][HEIST_LAUNCH] - PROCESS_RESEND_OF_HEIST_MEDAL_DATA - Sharing the data of players medals from host to clients now download is complete")
		HEIST_MEDAL_DATA sMedalData[MAX_HEIST_PLANNING_ROWS]
	
		INT index
		FOR index = 0 TO (MAX_HEIST_PLANNING_ROWS-1)
			sMedalData[index].iPlayerOne 	= GET_MEDAL_DATA_FOR_PLAYER_FROM_MISSION_INDEX(index, ci_HEIST_PRE_PLAN_PLAYER_0)
			sMedalData[index].iPlayerTwo 	= GET_MEDAL_DATA_FOR_PLAYER_FROM_MISSION_INDEX(index, ci_HEIST_PRE_PLAN_PLAYER_1)
			sMedalData[index].iPlayerThree 	= GET_MEDAL_DATA_FOR_PLAYER_FROM_MISSION_INDEX(index, ci_HEIST_PRE_PLAN_PLAYER_2)
			sMedalData[index].iPlayerFour 	= GET_MEDAL_DATA_FOR_PLAYER_FROM_MISSION_INDEX(index, ci_HEIST_PRE_PLAN_PLAYER_3)
		ENDFOR
		
		BROADCAST_SCRIPT_EVENT_HEIST_MEDAL_DATA(sMedalData)
	ENDIF
ENDPROC


/// PURPOSE:
///    Create the initial heist planning board with placeholder data. Also sets some useful local global variables and other initialisation items.
PROC HEIST_START_CREATE_BOARD()

	IF NOT LOAD_HEIST_SCALEFORM_MOVIES()
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_CREATE_BOARD - Waiting on heist scaleform movies...")
		#ENDIF
		
		EXIT
	ENDIF
	
	// Beyond this point, all reqired assets should be loaded.
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_CREATE_BOARD - Building heist finale planning board.")
	#ENDIF
	
	//NET_SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,FALSE,FALSE,TRUE,TRUE,TRUE,FALSE,TRUE,TRUE)
	IF IS_PLAYER_IN_CORONA()
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_CREATE_BOARD - Player is in corona, making sure that player control is disabled.")
		NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_SET_INVISIBLE | NSPC_NO_COLLISION | NSPC_FREEZE_POSITION ) 
	ELSE
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_CREATE_BOARD - Player not in corona, don't set player control.")
	ENDIF
	
	// Make sure that no headshot remnants interfere with the board.
	CLEAN_ALL_HEIST_HEADSHOTS(TRUE)

	// Set local globals to default values.
	
	GET_PLAYER_PROPERTY_HEIST_LOCATION(g_HeistSharedClient.vBoardPosition, MP_PROP_ELEMENT_HEIST_BOARD_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	GET_PLAYER_PROPERTY_HEIST_ROTATION(g_HeistSharedClient.vBoardRotation, MP_PROP_ELEMENT_HEIST_BOARD_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	g_HeistSharedClient.vBoardSize 		= <<1.0, 1.0, 1.0>>
	g_HeistSharedClient.vWorldSize 		= <<4.2, 2.4, 1.0>>
	
	GET_PLAYER_PROPERTY_HEIST_LOCATION(g_HeistSharedClient.vMapPosition, MP_PROP_ELEMENT_HEIST_MAP_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	GET_PLAYER_PROPERTY_HEIST_ROTATION(g_HeistSharedClient.vMapRotation, MP_PROP_ELEMENT_HEIST_MAP_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	g_HeistSharedClient.vMapSize = <<HEIST_MAP_SIZE_X, HEIST_MAP_SIZE_Y, HEIST_MAP_SIZE_Z>>
	g_HeistSharedClient.vMapWorldSize = <<HEIST_MAP_WORLD_SIZE_X, HEIST_MAP_WORLD_SIZE_Y, HEIST_MAP_WORLD_SIZE_Z>>
	
	INT index 
	
	g_HeistPlanningClient.sBoardState.eFocusPoint = HEIST_FOCUS_BOARD

	// ******************** SETUP MP HEIST BOARD DATA ******************** \\
	
	IF NOT IS_PLAYER_IN_CORONA()
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_CREATE_BOARD - NON-Corona create, use cache configuration.")
		#ENDIF
		
		IF AM_I_LEADER_OF_HEIST()
			COPY_GLOBAL_HEIST_DATA_TO_LOCAL()
		ENDIF
		
		CLEAN_ONLY_HEIST_PLANNING_CACHED_DATA()
	ENDIF

	// Call first to setup the board labels.
	SET_HEIST_PLANNING_LABELS(	g_HeistSharedClient.PlanningBoardIndex, 
								"HEIST_LAUN_0", "HEIST_TAKE", 
								"HEIST_PLAN", 	"HEIST_TOTAL", 
								"HEIST_COSTS", 	"HEIST_ROLE", 
								"HEIST_CUT", 	"HEIST_STATUS",
								"")
	
	// Set the name of the heist from the global data.
	SET_HEIST_PLANNING_NAME(g_HeistSharedClient.PlanningBoardIndex, 
							g_FMMC_STRUCT.tl63MissionName)
							//GET_PLAYER_NAME(INT_TO_PLAYERINDEX(g_HeistPlanningClient.iPlayerOrder[ci_HEIST_LEADER]))) // Removed as part of 1845891.
		
	// Setup blank placeholders for all players.
	FOR index = 0 TO (NUMBER_OF_PLAYERS_ON_HEIST()-1) 
		SET_HEIST_PLANNING_CREW_SLOT_EMPTY(g_HeistSharedClient.PlanningBoardIndex, index)
		
		HEIST_CREATE_INITIAL_MEMBER(index)
		
		IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index]), FALSE, FALSE)
			SET_BIT(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
		ENDIF
							
		// Initialise headshot array to be blank
		g_HeistPlanningClient.sHeistHeadshots[index].playerID = INVALID_PLAYER_INDEX()
		g_HeistPlanningClient.sHeistHeadshots[index].pedHeadshotID = NULL
	ENDFOR
	
	// Build the player card for slot 0. This only needs to be build for slot 0 initially, and then all of the player's respective
	// cards can be updated afterwards.
	HEIST_CREATE_INITIAL_PLAYER_CARD()
	
//	TEXT_LABEL_63 tlTempOutfit
	
	// Configure the pie chart.
	IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
		SET_HEIST_PLANNING_PIECHART(	g_HeistSharedClient.PlanningBoardIndex,
										g_HeistPlanningClient.iHeistTotalPay,
										g_HeistPlanningClient.sHeistCut)
	ELSE
		SET_HEIST_PLANNING_PIECHART(	g_HeistSharedClient.PlanningBoardIndex,
										GlobalServerBD_HeistPlanning.iHeistTotalPay,
										GlobalServerBD_HeistPlanning.sHeistCut)
	ENDIF
	
	// Setup the TODO list (bottom left).
	SET_HEIST_PLANNING_TASK_LIST(g_HeistSharedClient.PlanningBoardIndex, INVALID_HEIST_DATA)
	
	// Initialise the board with the above data.
	SET_HEIST_PLANNING_INITIALISE(g_HeistSharedClient.PlanningBoardIndex) 
	
	SET_HEIST_PLANNING_LEADER_COST(	g_HeistSharedClient.PlanningBoardIndex, 
									"HEIST_LCOST",
									GET_HEIST_ENTRY_FEE_FROM_ROOT_CONTENT_ID(g_FMMC_STRUCT.iRootContentIDHash))

						
	SET_HEIST_PLANNING_UPDATE_PREVIEW(	g_HeistSharedClient.PlanningBoardIndex, 
										ci_HEIST_PREVIEW_WINDOW_OUTFIT, 
										"",//GET_HEIST_BOARD_OUTFIT_LABEL(ci_HEIST_LEADER),
										//g_HeistPlanningClient.tlOutfitTextures[0])
										"")//tlTempOutfit)
	
	// Highlight nothing.
	SET_ALL_HEIST_HIGHLIGHTS(g_HeistPlanningClient.sBoardState, INVALID_HEIST_INDEX, IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID()))

	g_HeistPlanningClient.iSelectedHeistMember = ci_HEIST_LEADER
	
	IF GET_CORONA_STATUS() != CORONA_STATUS_TEAM_DM
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_CREATE_BOARD - Setting local ready state to: STATUS_NOT_READY")
		#ENDIF
		GO_TO_HEIST_PLANNING_STATE(ENUM_TO_INT(STATUS_NOT_READY))
	ENDIF
	
	// Set the player's bad sport rating in the global player data.
	SET_PLAYER_BAD_SPORT_VALUE()
	
	// Only do these things if we're entering the planning board from the corona. This is needed because you can re-eneter the property
	// whilst on a finale mission.
	IF IS_PLAYER_IN_CORONA()
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_CREATE_BOARD - Corona create, use leader's configuration.")
		#ENDIF
		
		IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
			IF NOT ARE_HEIST_FINALE_MEMBERS_ALREADY_SAVED()
				// Give every heist member a default team.
				AUTO_ASSIGN_ROLES_TO_HEIST_MEMBERS(TRUE)

				FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
					IF g_HeistPlanningClient.iPlayerOrder[index] != -1
						BROADCST_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON(index, g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index], g_HeistPlanningClient.iPlayerOrder[index])
					ENDIF
				ENDFOR
				
			ELSE
			
				FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
					IF g_HeistPlanningClient.iPlayerOrder[index] != -1
						LOAD_HEIST_MEMBER_DATA_FOR_QUICK_RESTART(index)
						BROADCST_SCRIPT_EVENT_HEIST_UPDATE_ROLE_SINGLETON(index, g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index], g_HeistPlanningClient.iPlayerOrder[index])
					ENDIF
				ENDFOR
				
				BROADCST_SCRIPT_EVENT_HEIST_UPDATE_CUTS(g_HeistPlanningClient.sHeistCut.iCutPercent)
			ENDIF
		ENDIF
		
		// Start the timer for headshot loading.
		IF NOT HAS_NET_TIMER_STARTED(g_HeistPlanningClient.sHeadshotLoadTimer)
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - IS_HEIST_HEADSHOT_READY - Headshot timer not yet started, forcing start timer.")
			#ENDIF
			START_NET_TIMER(g_HeistPlanningClient.sHeadshotLoadTimer)
		ENDIF
	ENDIF
	
	// Added for 1909537 to make sure that all highlights are reset upon entering.
	g_HeistPlanningClient.sBoardState.iHighlight = ci_HEIST_PLANNING_HIGHLIGHT_NULL
	g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_PLANNING_HIGHLIGHT_NULL
	g_HeistPlanningClient.sBoardState.iLeftArrow = 0
	g_HeistPlanningClient.sBoardState.iRightArrow = 0
	
	// Create RAG widgets
	#IF IS_DEBUG_BUILD
		DRAW_SCALEFORM_WIDGETS()
	#ENDIF
	
	// Removed for 1865657 - this is handled within the cutscene or planning corona status
	//CORONA_CLEAN_UP_SCREEN_STATE()
	
	SET_HEIST_FINALE_STATE(HEIST_FLOW_UPDATE_BACKGROUND)
ENDPROC


/// PURPOSE:
///    Only update the scaleform for the members in the current session. This is used for when you want to see the board in the background, but not interact with it.
/// PARAMS:
///    bAllowHeistConfig - TRUE to take player cam, FALSE to keep the finale heist board in the background.
PROC HEIST_START_UPDATE_FINALE_BACKGROUND(BOOL bAllowHeistConfig)

	// Don't bother wasting cycles if the local player isn't close to the board.
	IF NOT IS_LOCAL_PLAYER_NEAR_HEIST_PLANNING_BOARD(ci_HEIST_BOARD_LOAD_IN_RANGE)
		EXIT
	ENDIF
	
	IF NOT DOWNLOAD_ALL_PHOTOS_FOR_HEIST_PLANNING_BOARD()
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_UPDATE_FINALE_BACKGROUND - Picture download still in progress.")
	ENDIF
	
	// Update all data related to the player (E.G. cut, status, role)
	UPDATE_HEIST_PLAYERS()
	
	// Update the playercard on the right of the heist planning board.
	UPDATE_HEIST_PLAYER_CARDS()
	
	// Update misc heist planning details (E.G. open roles, description, instructional buttons)
	UPDATE_HEIST_MISC()
	
	// Render scaleform movie.
	DRAW_HEIST_SCALEFORM()
	
	// Check for pass-thru. This is for when the heist board gets called from the corona state for heist planning.
	IF bAllowHeistConfig
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_UPDATE_FINALE_BACKGROUND - bAllowHeistConfig = TRUE, moving to UPDATE_ALL.")
		#ENDIF
		SET_HEIST_FINALE_STATE(HEIST_FLOW_INIT_FINALE)
	ENDIF
	
	IF GET_HEIST_UPDATE_AVAILABLE()
		SET_HEIST_UPDATE_AVAILABLE(FALSE)
	ENDIF

ENDPROC


/// PURPOSE:
///    Init the configuration mode of the planning board. This is used when going from background to foreground.
PROC HEIST_START_INIT_FINALE_PLANNING_MODE()

	HIDE_HUD_AND_RADAR_THIS_FRAME()

	IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
		g_HeistPlanningClient.bHaveDoneHelp = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_PLANNING_DONE_HELP_0)
		g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_LEADER
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_FINALE_PLANNING_MODE - LEADER Setting iSubHighlight to: ", ci_HEIST_LEADER)
		#ENDIF
	ELSE
		g_HeistPlanningClient.bHaveDoneHelp = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_PLANNING_DONE_HELP_1)
		g_HeistPlanningClient.sBoardState.iSubHighlight = INVALID_HEIST_INDEX
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_INIT_FINALE_PLANNING_MODE - CLIENT Setting iSubHighlight to: ", INVALID_HEIST_INDEX)
		#ENDIF
	ENDIF
	
	//B* 2135283
	IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID()) AND NOT g_HeistPlanningClient.bHaveDoneHelp
		g_HeistPlanningClient.sBoardState.iHighlight 	= INVALID_HEIST_INDEX
	ELSE
		g_HeistPlanningClient.sBoardState.iHighlight 	= ci_HEIST_LEADER
	ENDIF	
	
	g_HeistPlanningClient.sBoardState.iSubHighlight 	= ci_HEIST_COLUMN_ROLE
	g_HeistPlanningClient.sBoardState.iRightArrow 		= 0
	g_HeistPlanningClient.sBoardState.iLeftArrow 		= 0
	g_HeistPlanningClient.tsPlayerStatusHelpTextTimer 	= GET_TIME_OFFSET(GET_NETWORK_TIME(), 3000)			// Initialise the timer to check for tutorial Xs later
	g_HeistPlanningClient.tsBoardTotalPlanningTime 		= GET_NETWORK_TIME()
	g_HeistPlanningClient.bUpdateInstButtons			= TRUE
	
	SET_HEIST_SUPPRESS_HUD_FOR_PLANNING(TRUE)
	
	// Initialise our source camera target
	g_HeistPlanningClient.sBoardCamData.iCamCurrentTarget = ci_HEIST_CAMERA_POS_BOARD
	
	// All of the loading is complete, start the help timer if required.
	CREATE_HEIST_FINALE_BOARD_CAMERA()

	IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
		BROADCAST_GENERAL_EVENT(GENERAL_EVENT_HEIST_SET_LAUNCH_TIMESTAMP, ALL_PLAYERS())
		SET_HEIST_LAUNCH_BUTTON_VISIBILITY(g_HeistSharedClient.PlanningBoardIndex, FALSE)
		SET_HEIST_LAUNCH_BUTTON_ENABLED(g_HeistSharedClient.PlanningBoardIndex, FALSE)
//		SET_HEIST_LAUNCH_BUTTON_HIGHLIGHTED(g_HeistSharedClient.PlanningBoardIndex, FALSE)
	ENDIF

	RESET_NET_TIMER(g_HeistPlanningClient.sLaunchCountdown)
	
	//SET_ALL_HEIST_HIGHLIGHTS(g_HeistPlanningClient.sBoardState, ci_HEIST_LEADER, IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID()))
	
	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_CREATE_CAMERA - Planning cam successfully created.")
	#ENDIF
	
	// Render scaleform movie.
	DRAW_HEIST_SCALEFORM()
		
#IF FEATURE_GEN9_STANDALONE
	HEIST_UDS_ACTIVITY_ENTER_FINALE_PLANNING_BOARD()
#ENDIF // FEATURE_GEN9_STANDALONE
	
	SET_HEIST_FINALE_STATE(HEIST_FLOW_UPDATE_ALL)

ENDPROC


/// PURPOSE:
///    Update the heist planning board, and associated items. This also draws the heist board scaleform movie(s).
/// PARAMS:
///    sScaleformStruct - 
///    iCoronaTime - 
PROC HEIST_START_UPDATE_FINALE_ALL(SCALEFORM_INSTRUCTIONAL_BUTTONS &sScaleformStruct, BOOL bAllowHeistConfig, BOOL& bDoLaunch)
	
	IF IS_SCREEN_FADED_OUT()
		IF NOT IS_SCREEN_FADING_IN()
			DO_SCREEN_FADE_IN(500)
		ENDIF
	ENDIF 
	
	// If configuration is not allowed, clean up interaction and move to background update.
	IF NOT bAllowHeistConfig
		IF NOT Is_Player_Currently_On_MP_Heist(PLAYER_ID())
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_UPDATE_FINALE_ALL - bAllowHeistConfig = FALSE; moving from configure to background update.")
			CLEAN_HEIST_FINALE_LITE()
			EXIT
		ELSE
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_UPDATE_FINALE_ALL - WARNING! bAllowHeistConfig = FALSE but Is_Player_Currently_On_MP_Heist() = TRUE! Moving to background WITHOUT cleanup.")
			SET_HEIST_FINALE_STATE(HEIST_FLOW_UPDATE_BACKGROUND)
			EXIT
		ENDIF
	ENDIF
			
	IF NOT GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bPlanningBoardReady
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_UPDATE_FINALE_ALL - Setting local player BD bPlanningBoardReady to TRUE")
		GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bPlanningBoardReady = TRUE
	ENDIF
	
	IF NOT DOWNLOAD_ALL_PHOTOS_FOR_HEIST_PLANNING_BOARD()
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_UPDATE_FINALE_ALL - Picture download still in progress.")
	ENDIF
								
	// Make sure we're not interrupted in the planning phase.
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	// Update all data related to the player (E.G. cut, status, role)
	UPDATE_HEIST_PLAYERS()
	
	// Update the playercard on the right of the heist planning board.
	UPDATE_HEIST_PLAYER_CARDS()
	
	// Update misc heist planning details (E.G. open roles, description, instructional buttons)
	UPDATE_HEIST_MISC()
	
	IF NOT g_HeistPlanningClient.bHaveDoneHelp
	
		// Make sure that the help-text has finished. If not, extend the launch timer.
		IF NOT g_HeistPlanningClient.bExtendedLaunchTime
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_UPDATE_FINALE_ALL - Requesting additional launch time to cover help-text display.")
			BROADCAST_GENERAL_EVENT(GENERAL_EVENT_HEIST_EXTEND_TIMER, ALL_PLAYERS())
			g_HeistPlanningClient.bExtendedLaunchTime = TRUE
		ENDIF
		
		IF MANAGE_HEIST_HELP_TEXT_STATE(g_HeistPlanningClient.sHelpTextData)
		AND NOT g_HeistPlanningClient.sBoardCamData.bTransitionInProgress
		
			// Make sure the help-text is totally up-to-date before exiting. This also catches the finished state and lets the camera
			// finish interoplating back to the centre of the planning board before destroying the camera.
			UPDATE_HEIST_PLANNING_HELP_TEXT_PROMPTS()
		
			g_HeistPlanningClient.bHaveDoneHelp = TRUE
			
			IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_PLANNING_DONE_HELP_0, g_HeistPlanningClient.bHaveDoneHelp)
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_PLANNING_DONE_HELP_1, g_HeistPlanningClient.bHaveDoneHelp)
			ELSE
				SET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_PLANNING_DONE_HELP_1, g_HeistPlanningClient.bHaveDoneHelp)
			ENDIF
			
			CLEAR_FIRST_PERSON_CAMERA(g_HeistPlanningClient.sBoardCamData.sFPSCamData)
			
			CLEAR_HEIST_CAM_OPERATING_MODE(g_HeistPlanningClient.sBoardCamData)
			
			CREATE_HEIST_PLANNING_FIRST_PERSON_CAMERA(ci_HEIST_CAMERA_POS_OVERVIEW, FALSE)
			
			// Clean up that we are on the finale tutorial so players can remove the help text
			CLEAR_PLAYER_ON_HEIST_FINALE_TUTORIAL()
			
			g_HeistPlanningClient.sBoardState.iHighlight 		= ci_HEIST_LEADER
			g_HeistPlanningClient.sBoardState.iSubHighlight 	= ci_HEIST_COLUMN_ROLE
			g_HeistPlanningClient.sBoardState.iRightArrow 		= 0
			g_HeistPlanningClient.sBoardState.iLeftArrow 		= 0
			
			IF GET_HEIST_HELP_SEQUENCE_IN_PROGRESS()
				SET_HEIST_HELP_SEQUENCE_IN_PROGRESS(FALSE)
			ENDIF
			
		ELSE
		
			IF NOT GET_HEIST_HELP_SEQUENCE_IN_PROGRESS()
				SET_HEIST_HELP_SEQUENCE_IN_PROGRESS(TRUE)
			ENDIF
			
			// Update the text prompts.
			UPDATE_HEIST_PLANNING_HELP_TEXT_PROMPTS()
			
			// We only want to update the camera at this point, as the user doesn't have contol.
			UPDATE_HEIST_PLANNING_CAMERA()
		ENDIF
	ELSE
		UPDATE_HEIST_PLANNING_PLAYER_STATUS_HELP()	
	
		// Update the scaleform instructional buttons to tell the user how to navigate.
		UPDATE_HEIST_SCALEFORM_INSTRUCTIONS(sScaleformStruct)
		
		// Update the camera movement, and various navigationsal inputs.
		UPDATE_HEIST_INPUTS_AND_CAMERA()
	ENDIF

	// Update the launch condition monitor.
	MONITOR_MP_HEIST_LAUNCH(bDoLaunch)
	
	// Render scaleform movie.
	DRAW_HEIST_SCALEFORM()
	
	IF GET_HEIST_UPDATE_AVAILABLE()
		SET_HEIST_UPDATE_AVAILABLE(FALSE)
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Ask the user to confirm that they want to exit the heist planning board.
PROC HEIST_START_CONFIRM_EXIT(SCALEFORM_INSTRUCTIONAL_BUTTONS &sScaleformStruct, BOOL &bDoLaunch)

	// To stop the board visibly disappearing when entering the conrifm exit state, keep drawing the background elements but don't do any updating.	
	// Render scaleform movie.
	DRAW_HEIST_SCALEFORM()
	
	// Update the scaleform instructional buttons to tell the user how to navigate.
	UPDATE_HEIST_SCALEFORM_INSTRUCTIONS(sScaleformStruct)
	
	// Update the launch condition monitor. We have to pull you through if you have not yet fully quit.
	MONITOR_MP_HEIST_LAUNCH(bDoLaunch)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_CONFIRM_EXIT - Waiting for confirmation of exit.")
	#ENDIF

	BOOL bQuit

	IF CONTROL_FM_MISSION_WARNING_SCREEN(bQuit, g_HeistPlanningClient.iWarningScreenButtonBitset)

		IF bQuit
		
			SET_HEIST_FINALE_STATE(HEIST_FLOW_CLEANUP)
			SET_HEIST_SKIP_FIRST_SKYSWOOP_UP_CUT(TRUE)
			
			IF AM_I_LEADER_OF_HEIST(TRUE)
				PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_CONFIRM_EXIT - I'm leader, setting go to apartment flag.")
				SET_TRANSITION_SESSION_HOLD_CAM_FOR_APARTMENT_SCRIPT()
			ENDIF
			
			TOGGLE_RENDERPHASES(FALSE)

#IF FEATURE_GEN9_STANDALONE
			HEIST_UDS_ACTIVITY_EXIT_FINALE_PLANNING_BOARD()
#ENDIF // FEATURE_GEN9_STANDALONE

			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_CONFIRM_EXIT - Quit confirmed, moving to cleanup.")
			#ENDIF
			
		ELSE
		
			SET_HEIST_FINALE_STATE(HEIST_FLOW_UPDATE_ALL)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_CONFIRM_EXIT - Quit cancelled, moving back to update.")
			#ENDIF
			
		ENDIF

	ENDIF

ENDPROC


/// PURPOSE:
///    Ask the user to confirm that they want to exit the heist planning board.
PROC HEIST_START_CONFIRM_CUTS(SCALEFORM_INSTRUCTIONAL_BUTTONS &sScaleformStruct)

	// To stop the board visibly disappearing when entering the conrifm exit state, keep drawing the background elements but don't do any updating.	
	// Render scaleform movie.
	DRAW_HEIST_SCALEFORM()
	
	// Update the scaleform instructional buttons to tell the user how to navigate.
	UPDATE_HEIST_SCALEFORM_INSTRUCTIONS(sScaleformStruct)
	
//	// Update the launch condition monitor. We have to pull you through if you have not yet fully quit.
//	MONITOR_MP_HEIST_LAUNCH(bDoLaunch)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_CONFIRM_CUTS - Waiting for acceptance of cut requirement.")
	#ENDIF

	BOOL bAccept

	IF CONTROL_HEIST_ASSIGN_CUT_SCREEN(bAccept, g_HeistPlanningClient.iWarningScreenButtonBitset)

		IF bAccept
		
			SET_HEIST_FINALE_STATE(HEIST_FLOW_UPDATE_ALL)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_CONFIRM_CUTS - Accepted requirement to alocate cut, return to planning.")
			#ENDIF
			
		ENDIF
			
	ENDIF

ENDPROC


/// PURPOSE:
///    Trigger the cleanup of the heist planning board.
/// PARAMS:
///    scaleformStruct - Instructional buttons.
PROC HEIST_START_CLEAN_ALL(CORONA_MENU_DATA& coronaMenuData, MISSION_TO_LAUNCH_DETAILS& sLaunchMissionDetails)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_CLEAN_ALL - Heist dropped into cleanup state due to player dropout or exiting early.")
	#ENDIF
	
	CLEAN_HEIST_FINALE_ALL(Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
	CLEANUP_BEFORE_WALK_OUT(sLaunchMissionDetails, coronaMenuData)
ENDPROC



// ===========================================================================================================
//      FAKE FINALE PLANNING BOARD (shown when all setup missions are complete but not yet in corona)
// ===========================================================================================================

/// PURPOSE:
///    Create the heist leader as the only valid player on the heist board.
PROC HEIST_CREATE_INITIAL_MEMBER_FAKE(INT iFakeRewardTotal)

	#IF IS_DEBUG_BUILD
		PRINTLN("[AMEC][HEIST_FAKE] - HEIST_CREATE_INITIAL_MEMBER_FAKE - Creating heist leader on fake finale board.")
	#ENDIF

	PLAYER_INDEX playerID

	STRING sStatus
	TEXT_LABEL_15 tlCodeName
	TEXT_LABEL_15 tlPlayerRole
	TEXT_LABEL_23 tlPortrait
	
	INT iRoleIcon
	INT iStatusIcon
	INT iCutPercent
	INT iRank
	
	IF AM_I_LEADER_OF_HEIST()
		playerID = PLAYER_ID()
	ELSE
		IF IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
			playerID = INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex)
		ENDIF
	ENDIF

	// Get the players respect level.
	iRank = GET_PLAYER_RANK(playerID)
	// Retrieve the portrait for the player.
	tlPortrait = GET_HEIST_MEMBER_HEADSHOT(playerID, FALSE)
	// Retrieve role informations
	tlPlayerRole = "HEIST_RL_NONE"
	// Get the little leader's crown.
	iRoleIcon = ENUM_TO_INT(ROLE_LEADER)
	// Retrieve status of player (either READY or NOT READY);
	sStatus = ""
	// The icon type for that status (blank because you know, leader...)
	iStatusIcon = ENUM_TO_INT(STATUS_BLANK)
	// The cut is only ever 100% because the leader is the only one on the board.
	iCutPercent = HEIST_CUT_PERCENT_TOTAL
	
	// Feed all of the data just gathered into the scaleform update method.
	SET_HEIST_PLANNING_CREW_SLOT(g_HeistSharedClient.PlanningBoardIndex, ci_HEIST_ROW_ONE, GET_PLAYER_NAME(playerID), iRank, tlPortrait, tlPlayerRole, iRoleIcon, sStatus, iStatusIcon, iCutPercent, tlCodeName, iFakeRewardTotal)

ENDPROC


/// PURPOSE:
///    Create player 0 player card. This is the only card that is required to be initialised, all others only require updates after this init.
PROC HEIST_CREATE_INITIAL_PLAYER_CARD_FAKE()

	TEXT_LABEL_15	tlPlayerRole
	TEXT_LABEL_23 	tlPortrait
	TEXT_LABEL_31	tlPlayerTitle
	STRING			sPlayerRating, sPlayerName
	INT				iPlayerStamina, iPlayerShooting, iPlayerStealth, iPlayerFlying, iPlayerDriving, iPlayerMentalState, iPlayerRank, iPlayerSlot
	BOOL			bAccessPlane, bAccessHeli, bAccessBoat, bAccessCar
	FLOAT			fPlayerKillDeathRatio
	PLAYER_INDEX	playerID

	IF AM_I_LEADER_OF_HEIST()
		playerID = PLAYER_ID()
	ELSE
		IF IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
			playerID = INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex)
		ENDIF
	ENDIF
	
	// Initialise the variables in the order in which they are passed into the set player card procedure.

	// Because this is the INIT of the player card, we only init the leader.
	iPlayerSlot = 0
	
	// Leader's name.
	sPlayerName = GET_PLAYER_NAME(playerID)

	// Get the players respect level.
	iPlayerRank = GET_PLAYER_RANK(playerID)
	
	// Retrieve role informations
	tlPlayerRole = "HEIST_RL_NONE"
	
	// Retrieve the portrait for the player.
	tlPortrait = GET_HEIST_MEMBER_HEADSHOT(playerID, FALSE)
	
	// HARDWARE ACCESS
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].scoreData.iVehicleAccess, STAT_CARD_PLANE_UNLOCK)
		bAccessPlane = TRUE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].scoreData.iVehicleAccess, STAT_CARD_HELI_UNLOCK)
		bAccessHeli = TRUE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].scoreData.iVehicleAccess, STAT_CARD_BOAT_UNLOCK)
		bAccessBoat = TRUE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].scoreData.iVehicleAccess, STAT_CARD_PERSONAL_CAR_UNLOCK)
		bAccessCar = TRUE
	ENDIF
	
	// KDR as float.
	fPlayerKillDeathRatio = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].scoreData.fKDRatio
	
	// To get the player title, we need to get the corrected rank number.
	INT iTempRank = GET_RANK_AS_NUMBER_FROM_0_TO_20(iPlayerRank)
	// Now build the localised text label using the corrected rank as the incrementor.
	tlPlayerTitle = RETURN_PERSONAL_RANK_STRING(iTempRank, TEAM_FREEMODE)
	
	IF NOT GET_PLAYER_BAD_SPORT_VALUE(playerID)
		sPlayerRating = "PCARD_CLEAN_PLAYER"
	ELSE
		sPlayerRating = "PCARD_BAD_SPORT"
	ENDIF
	
	iPlayerStamina = GlobalplayerBD_Stats[NATIVE_TO_INT(playerID)].iStatStamina
	iPlayerShooting = GlobalplayerBD_Stats[NATIVE_TO_INT(playerID)].iStatShooting
	iPlayerStealth = GlobalplayerBD_Stats[NATIVE_TO_INT(playerID)].iStatStealth
	iPlayerFlying = GlobalplayerBD_Stats[NATIVE_TO_INT(playerID)].iStatFlying
	iPlayerDriving = GlobalplayerBD_Stats[NATIVE_TO_INT(playerID)].iStatDriving
	iPlayerMentalState = GlobalplayerBD_Stats[NATIVE_TO_INT(playerID)].iStatMentalState

	SET_HEIST_PLANNING_PLAYER_CARD(	g_HeistSharedClient.PlanningBoardIndex, 
									iPlayerSlot, sPlayerName, iPlayerRank, tlPlayerRole, 
									tlPortrait, bAccessPlane, bAccessHeli, bAccessBoat,
									bAccessCar, fPlayerKillDeathRatio, tlPlayerTitle, sPlayerRating, 
									iPlayerStamina, iPlayerShooting, iPlayerStealth, iPlayerFlying, 
									iPlayerDriving, iPlayerMentalState)

ENDPROC


/// PURPOSE:
///    Update the heist leader on the fake planning board. Only really used to download the heist leader's headshot after init.
PROC HEIST_UPDATE_FINALE_MEMBER_FAKE()

	PLAYER_INDEX playerID

	STRING sStatus
	STRING sOutfitName
	TEXT_LABEL_15 tlCodeName
	TEXT_LABEL_15 tlPlayerRole
	TEXT_LABEL_23 tlPortrait
	
	INT iRoleIcon, iStatusIcon, iCutPercent, iRank, iVoiceState
	
	IF AM_I_LEADER_OF_HEIST()
		playerID = PLAYER_ID()
	ELSE
		IF IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
			playerID = INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex)
		ENDIF
	ENDIF
	
	// Get the players respect level.
	iRank = GET_PLAYER_RANK(playerID)
	// Retrieve the portrait for the player.
	tlPortrait = GET_HEIST_MEMBER_HEADSHOT(playerID, FALSE)
	// Retrieve role informations
	tlPlayerRole = "HEIST_RL_NONE"
	// Get the little leader's crown.
	iRoleIcon = ENUM_TO_INT(ROLE_LEADER)
	// Retrieve status of player (either READY or NOT READY);
	sStatus = ""
	// The icon type for that status (blank because you know, leader...)
	iStatusIcon = ENUM_TO_INT(STATUS_BLANK)
	// The cut is only ever 100% because the leader is the only one on the board.
	iCutPercent = HEIST_CUT_PERCENT_TOTAL
	// UNASSIGNED outfit
	sOutfitName = "HEIST_RL_NONE"
	// The players headset status
	iVoiceState = ENUM_TO_INT(ICON_EMPTY)
	
	// Feed all of the data just gathered into the scaleform update method.
	SET_HEIST_PLANNING_UPDATE_CREW_SLOT(g_HeistSharedClient.PlanningBoardIndex, ci_HEIST_ROW_ONE, GET_PLAYER_NAME(playerID), iRank, tlPortrait, tlPlayerRole, iRoleIcon, sStatus, iStatusIcon, iCutPercent, tlCodeName, sOutfitName, 0, iVoiceState, ci_HEIST_TEAM_STATUS_VALID, g_HeistPlanningClient.iTotalRewardFake)

ENDPROC


/// PURPOSE:
///    Update the heist leader's stats on the fake planning board. Only really used to download the heist leader's headshot after init.
PROC HEIST_UPDATE_FINALE_MEMBER_CARD_FAKE()

	TEXT_LABEL_15	tlPlayerRole
	TEXT_LABEL_23 	tlPortrait
	TEXT_LABEL_31	tlPlayerTitle
	STRING			sPlayerRating, sPlayerName
	INT				iPlayerStamina, iPlayerShooting, iPlayerStealth, iPlayerFlying, iPlayerDriving, iPlayerMentalState, iPlayerRank, iPlayerSlot, iPlayerStrenth
	BOOL			bAccessPlane, bAccessHeli, bAccessBoat, bAccessCar
	FLOAT			fPlayerKillDeathRatio
	PLAYER_INDEX	playerID

	IF AM_I_LEADER_OF_HEIST()
		playerID = PLAYER_ID()
	ELSE
		IF IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
			playerID = INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex)
		ENDIF
	ENDIF
	
	// Initialise the variables in the order in which they are passed into the set player card procedure.

	// Because this is the INIT of the player card, we only init the leader.
	iPlayerSlot = 0
	
	// Leader's name.
	sPlayerName = GET_PLAYER_NAME(playerID)

	// Get the players respect level.
	iPlayerRank = GET_PLAYER_RANK(playerID)
	
	// Retrieve role informations
	tlPlayerRole = "HEIST_RL_NONE"
	
	// Retrieve the portrait for the player.
	tlPortrait = GET_HEIST_MEMBER_HEADSHOT(playerID, FALSE)
	
	// HARDWARE ACCESS
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].scoreData.iVehicleAccess, STAT_CARD_PLANE_UNLOCK)
		bAccessPlane = TRUE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].scoreData.iVehicleAccess, STAT_CARD_HELI_UNLOCK)
		bAccessHeli = TRUE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].scoreData.iVehicleAccess, STAT_CARD_BOAT_UNLOCK)
		bAccessBoat = TRUE
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].scoreData.iVehicleAccess, STAT_CARD_PERSONAL_CAR_UNLOCK)
		bAccessCar = TRUE
	ENDIF
	
	// KDR as float.
	fPlayerKillDeathRatio = GlobalplayerBD_FM[NATIVE_TO_INT(playerID)].scoreData.fKDRatio
	
	// To get the player title, we need to get the corrected rank number.
	INT iTempRank = GET_RANK_AS_NUMBER_FROM_0_TO_20(iPlayerRank)
	// Now build the localised text label using the corrected rank as the incrementor.
	tlPlayerTitle = RETURN_PERSONAL_RANK_STRING(iTempRank, TEAM_FREEMODE)
	
	IF NOT GET_PLAYER_BAD_SPORT_VALUE(playerID)
		sPlayerRating = "PCARD_CLEAN_PLAYER"
	ELSE
		sPlayerRating = "PCARD_BAD_SPORT"
	ENDIF
	
	iPlayerStamina = GlobalplayerBD_Stats[NATIVE_TO_INT(playerID)].iStatStamina
	iPlayerShooting = GlobalplayerBD_Stats[NATIVE_TO_INT(playerID)].iStatShooting
	iPlayerStealth = GlobalplayerBD_Stats[NATIVE_TO_INT(playerID)].iStatStealth
	iPlayerFlying = GlobalplayerBD_Stats[NATIVE_TO_INT(playerID)].iStatFlying
	iPlayerDriving = GlobalplayerBD_Stats[NATIVE_TO_INT(playerID)].iStatDriving
	iPlayerMentalState = GlobalplayerBD_Stats[NATIVE_TO_INT(playerID)].iStatMentalState
	iPlayerStrenth = GlobalplayerBD_Stats[NATIVE_TO_INT(playerID)].iStatStrength

	SET_HEIST_PLANNING_UPDATE_PLAYER_CARD(	g_HeistSharedClient.PlanningBoardIndex, 
											iPlayerSlot, sPlayerName, iPlayerRank, tlPlayerRole, 
											tlPortrait, bAccessPlane, bAccessHeli, bAccessBoat,
											bAccessCar, fPlayerKillDeathRatio, tlPlayerTitle, sPlayerRating, 
											iPlayerStamina, iPlayerShooting, iPlayerStealth, iPlayerFlying, 
											iPlayerDriving, iPlayerMentalState, iPlayerStrenth)

ENDPROC


/// PURPOSE:
///    Build the fake heist finale board. No actual data is downloaded about this mission outside of what is available in
///    the mission header data. 
PROC HEIST_START_GET_INIT_DATA_FAKE()

	IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
		PRINTLN("[AMEC][HEIST_FAKE] - HEIST_START_GET_INIT_DATA_FAKE - Waiting for player to enter property...")
		EXIT
	ENDIF
	
	IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty < 0
		PRINTLN("[AMEC][HEIST_FAKE] - HEIST_START_GET_INIT_DATA_FAKE - Waiting for player BD inside property index to be valid. Value: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		EXIT
	ENDIF
	
	IF NOT LOAD_HEIST_SCALEFORM_MOVIES()
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_FAKE] - HEIST_START_GET_INIT_DATA_FAKE - Waiting on heist scaleform movies.")
		#ENDIF
		EXIT
	ENDIF
	
	IF g_HeistSharedClient.iCurrentPropertyOwnerIndex > INVALID_HEIST_DATA
	AND g_HeistSharedClient.iCurrentPropertyOwnerIndex < NUM_NETWORK_PLAYERS
		IF NOT AM_I_LEADER_OF_HEIST()
			IF GlobalplayerBD_FM_HeistPlanning[g_HeistSharedClient.iCurrentPropertyOwnerIndex].iHeistPayBandCosmetic = INVALID_HEIST_DATA
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_FAKE] - HEIST_START_GET_INIT_DATA_FAKE - BD for iHeistPayBandCosmetic is INVALID, waiting for data from leader...")
				#ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[AMEC][HEIST_FAKE] - HEIST_START_GET_INIT_DATA_FAKE - Player in property index: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	
	INT iFinaleRootContID, iRewardBand, index
	TEXT_LABEL_63 tlTempMissionName = ""
	
	IF AM_I_LEADER_OF_HEIST()
		g_HeistPrePlanningClient.tlHeistFinaleRootContID = GET_HEIST_FINALE_STAT_DATA()
		iFinaleRootContID = GET_HASH_KEY(g_HeistPrePlanningClient.tlHeistFinaleRootContID)
		iRewardBand = GET_MP_INT_CHARACTER_STAT(MP_STAT_HEIST_TOTAL_REWARD_COSMETIC) 
		GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(PLAYER_ID())].iHeistPayBandCosmetic = iRewardBand
	ELSE
		IF IS_PLAYER_IN_ANOTHER_PLAYERS_PROPERTY(PLAYER_ID())
			IF g_HeistSharedClient.iCurrentPropertyOwnerIndex > INVALID_HEIST_DATA
				g_HeistPrePlanningClient.tlHeistFinaleRootContID = GlobalplayerBD_FM_HeistPlanning[g_HeistSharedClient.iCurrentPropertyOwnerIndex].tlHeistFinaleRootContID
				iFinaleRootContID = GET_HASH_KEY(g_HeistPrePlanningClient.tlHeistFinaleRootContID)
				iRewardBand = GlobalplayerBD_FM_HeistPlanning[g_HeistSharedClient.iCurrentPropertyOwnerIndex].iHeistPayBandCosmetic
			ELSE
				PRINTLN("[AMEC][HEIST_FAKE] - HEIST_START_GET_INIT_DATA_FAKE - CRITICAL ERROR! iCurrentPropertyOwnerIndex is INVALID, cannot build fake planning board!")
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	// Set the leader's team to team HEIST_ROLE_ZERO.
	g_HeistPlanningClient.sHeistRoles.ePlayerRoles[ci_HEIST_LEADER] = HEIST_ROLE_UNASSIGNED
	
	// Set the leader's cut to 100%.
	g_HeistPlanningClient.sHeistCut.iCutPercent[ci_HEIST_LEADER] = HEIST_CUT_PERCENT_TOTAL
	
	GET_PLAYER_PROPERTY_HEIST_LOCATION(g_HeistSharedClient.vBoardPosition, MP_PROP_ELEMENT_HEIST_BOARD_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	GET_PLAYER_PROPERTY_HEIST_ROTATION(g_HeistSharedClient.vBoardRotation, MP_PROP_ELEMENT_HEIST_BOARD_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	g_HeistSharedClient.vBoardSize 		= <<1.0, 1.0, 1.0>>
	g_HeistSharedClient.vWorldSize 		= <<4.2, 2.4, 1.0>>
			
	GET_PLAYER_PROPERTY_HEIST_LOCATION(g_HeistSharedClient.vMapPosition, MP_PROP_ELEMENT_HEIST_MAP_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	GET_PLAYER_PROPERTY_HEIST_ROTATION(g_HeistSharedClient.vMapRotation, MP_PROP_ELEMENT_HEIST_MAP_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
	g_HeistSharedClient.vMapSize = <<HEIST_MAP_SIZE_X, HEIST_MAP_SIZE_Y, HEIST_MAP_SIZE_Z>>
	g_HeistSharedClient.vMapWorldSize = <<HEIST_MAP_WORLD_SIZE_X, HEIST_MAP_WORLD_SIZE_Y, HEIST_MAP_WORLD_SIZE_Z>>
			
	// Call first to setup the board labels.
	SET_HEIST_PLANNING_LABELS(	g_HeistSharedClient.PlanningBoardIndex, 
								"HEIST_LAUN_1", "HEIST_TAKE", 
								"HEIST_PLAN", 	"HEIST_TOTAL", 
								"HEIST_COSTS", 	"HEIST_ROLE", 
								"HEIST_CUT", 	"HEIST_STATUS",
								"")

	// Make sure that no headshot remnants interfere with the board.
	CLEAN_ALL_HEIST_HEADSHOTS(TRUE)
	
	FOR index = 0 TO (g_numLocalMPHeists-1)
		IF (g_sLocalMPHeists[index].lhsHashFinaleRCID = iFinaleRootContID)
		    tlTempMissionName = Get_Mission_Name_For_FM_Cloud_Loaded_Activity(g_sLocalMPHeists[index].lhsMissionIdData)
		ENDIF
	ENDFOR
			
	// Set the name of the heist from the global data.
	SET_HEIST_PLANNING_NAME(g_HeistSharedClient.PlanningBoardIndex, 
							tlTempMissionName)
	
	IF iRewardBand = INVALID_HEIST_DATA
		g_HeistPlanningClient.iTotalRewardFake = GET_HEIST_TOTAL_REWARD(iFinaleRootContID)
		PRINTLN("[AMEC][HEIST_FAKE] - HEIST_START_GET_INIT_DATA_FAKE - ERROR! MP_STAT_HEIST_TOTAL_REWARD_COSMETIC = -1, reverting to hard-coded reward values!")
		SCRIPT_ASSERT("[AMEC][HEIST_FAKE] - HEIST_START_GET_INIT_DATA_FAKE - ERROR! MP_STAT_HEIST_TOTAL_REWARD_COSMETIC = -1, reverting to hard-coded reward values! Please enter a bug for Alastair.")
		#IF IS_DEBUG_BUILD
		DEBUG_DUMP_HEIST_STATS_TO_LOG()
		#ENDIF
	ELSE
		g_HeistPlanningClient.iTotalRewardFake = GET_HEIST_TOTAL_REWARD_FROM_BAND(iFinaleRootContID, iRewardBand)
		PRINTLN("[AMEC][HEIST_FAKE] - HEIST_START_GET_INIT_DATA_FAKE - Got reward band: ", iRewardBand, " for a value of: ", g_HeistPlanningClient.iTotalRewardFake)
	ENDIF
											
	SET_HEIST_PLANNING_CREW_SLOT_EMPTY(g_HeistSharedClient.PlanningBoardIndex, ci_HEIST_LEADER)
							
	HEIST_CREATE_INITIAL_MEMBER_FAKE(g_HeistPlanningClient.iTotalRewardFake)
	
	HEIST_CREATE_INITIAL_PLAYER_CARD_FAKE()
	
	g_HeistPlanningClient.sHeistHeadshots[ci_HEIST_LEADER].playerID = INVALID_PLAYER_INDEX()
	g_HeistPlanningClient.sHeistHeadshots[ci_HEIST_LEADER].pedHeadshotID = NULL		

	SET_HEIST_PLANNING_PIECHART(	g_HeistSharedClient.PlanningBoardIndex,
									//GET_HEIST_TOTAL_REWARD(iFinaleRootContID),
									g_HeistPlanningClient.iTotalRewardFake,
									g_HeistPlanningClient.sHeistCut)

	// Setup the TODO list (bottom left).
	SET_HEIST_PLANNING_TASK_LIST(g_HeistSharedClient.PlanningBoardIndex, INVALID_HEIST_DATA)
			
	// Initialise the board with the above data.
	SET_HEIST_PLANNING_INITIALISE(g_HeistSharedClient.PlanningBoardIndex) 
			
	// Highlight nothing.
	SET_ALL_HEIST_HIGHLIGHTS(g_HeistPlanningClient.sBoardState, INVALID_HEIST_DATA, TRUE)

	g_HeistPlanningClient.iSelectedHeistMember = ci_HEIST_LEADER
			
	GO_TO_HEIST_PLANNING_STATE(ENUM_TO_INT(STATUS_NOT_READY))
			
	// Set the player's bad sport rating in the global player data.
	SET_PLAYER_BAD_SPORT_VALUE()	
	
	IF NOT g_HeistPrePlanningClient.bLaunchFromPhone
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_FAKE] - HEIST_START_GET_INIT_DATA_FAKE - bLaunchFromPhone = FALSE, cleaning anims.")
		#ENDIF
		CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimFirst)
		CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimSecond)
	ENDIF
	
	CLEAN_ALL_HEIST_ANIM_DETAILS(g_HeistSharedClient.sHeistAnimThird)
			
	// Start the timer for headshot loading.
	IF NOT HAS_NET_TIMER_STARTED(g_HeistPlanningClient.sHeadshotLoadTimer)
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_FAKE] - IS_HEIST_HEADSHOT_READY - Headshot timer not yet started, forcing start timer.")
		#ENDIF
		START_NET_TIMER(g_HeistPlanningClient.sHeadshotLoadTimer)
	ENDIF

	PRINTLN("[AMEC][HEIST_FAKE] - HEIST_START_GET_INIT_DATA_FAKE - Mission data was successfully parsed.")
	SET_HEIST_FINALE_STATE(HEIST_FLOW_FAKE_UPDATE)


	GlobalplayerBD_FM_HeistPlanning[NATIVE_TO_INT(PLAYER_ID())].bHeistPlanningDataReady = TRUE
	CDEBUG1LN(DEBUG_HEIST_PLAN_PROP, "HEIST_START_GET_INIT_DATA_FAKE - Setting BD for bHeistPlanningDataReady to: TRUE")
	PRINTLN("[AMEC][HEIST_PREPLAN] - HEIST_START_GET_INIT_DATA_FAKE - Setting BD for bHeistPlanningDataReady to: TRUE")

ENDPROC


/// PURPOSE:
///    Check if any strand-board specific help-text is active. Used for distance based culling.
/// RETURNS:
///    TRUE if no strand messages are active.
FUNC BOOL IS_A_HEIST_FINALE_HELP_MESSAGE_CURRENTLY_DISPLAYING()

	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_WNTED")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_SOLO")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Make sure that there is space for help-text before forcing an update. Prevents rapid
///    fire help-text popping.
/// RETURNS:
///    TRUE when safe to print a new help message.
FUNC BOOL IS_IT_SAFE_TO_DISPLAY_HEIST_FINALE_BOARD_HELP()

	IF IS_A_HEIST_FINALE_HELP_MESSAGE_CURRENTLY_DISPLAYING()
		RETURN FALSE
	ENDIF
	
	// Make sure to defer to any active help-text.
	IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		RETURN FALSE
	ENDIF
	
	RETURN TRUE

ENDFUNC


/// PURPOSE:
///    Get the target context state based on a number of prerequisites. Allows for diffing current state with
///    desired state (aka dynamic label updates depending on environmental factors).
/// RETURNS:
///    INT state value.
FUNC INT GET_HEIST_FINALE_DESIRED_CONTEXT_REGISTRATION_STATE()

	// KGM 23/2/15 [BUG 2244469]: Check if a delay is needed for a one-off piece of special help text if the replay board has just become unlocked while there is an active heist
	BOOL delayForOneOffReplayHelp = FALSE
	IF (g_hasReplayBoardBeenUnlocked)
	AND NOT (IS_BIT_SET(MPGlobalsAmbience.iFmNmhBitSet9, BI_FM_NMH9_HEIST_RPLY_BOARD))
		PRINTLN(".KGM [Heist][AMEC][HEIST_FAKE] - GET_HEIST_FINALE_DESIRED_CONTEXT_REGISTRATION_STATE - DELAY INTERACTION: Replay Board Unlocked and one-off help text still needs to be displsyed.")
		delayForOneOffReplayHelp = TRUE
	ENDIF

	INT iReturnState

	IF NOT IS_PLAYER_ON_ANY_MP_MISSION(PLAYER_ID())
	AND NOT IS_PLAYER_IN_CORONA()
	AND NOT (Does_This_Player_Have_An_Active_Heist_Corona())
	AND NOT IS_THIS_A_SOLO_SESSION()
	AND NOT ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE()
	AND NOT (delayForOneOffReplayHelp)
	AND NOT IS_LOCAL_PLAYER_DOING_ANY_AMBIENT_TUTORIAL()
	AND NOT IS_HEIST_ACTIONING_A_QUICK_JOIN_TRANSITION()
	AND GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Appinternet")) < 1 // Fix for 1860441.
	AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <= 0
	AND (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), Get_Heist_Corona_Coords_For_Players_Currently_Occupied_Apartment()) < HEIST_CUTSCENE_TRIGGER_m)
		
		IF GET_HEIST_BACKGROUND_MODE_ACTIVE()
			iReturnState = ci_HEIST_REG_STATE_BACKGROUND
		ELIF (AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL() AND NOT SHOULD_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW())
		OR g_sMPTUNABLES.bDisable_Heist_On_Call_From_Planning_Board
		OR Is_Ped_Drunk(PLAYER_PED_ID())
			iReturnState = ci_HEIST_REG_STATE_ONCALL
		ELSE
			iReturnState = ci_HEIST_REG_STATE_DEFAULT
		ENDIF
		
	ELSE
		iReturnState = ci_HEIST_REG_STATE_NONE
	ENDIF
				
	RETURN iReturnState
				
ENDFUNC
					
					
/// PURPOSE:
///    Monitor for input, and launch the heist finale when the player interacts.
PROC MONITOR_FAKE_HEIST_FINALE_BOARD_INTERACTION()
					
	INT iTargetContextState = GET_HEIST_FINALE_DESIRED_CONTEXT_REGISTRATION_STATE()
						
	SWITCH iTargetContextState

		CASE ci_HEIST_REG_STATE_DEFAULT
						
			CLEAR_GENERIC_HEIST_HELP_TEXT()
			g_HeistSharedClient.bSuppressPlanningBoardContextIntention = IS_HELP_MESSAGE_BEING_DISPLAYED()
						
			IF GET_HEIST_FINALE_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_DEFAULT
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_FAKE_HEIST_FINALE_BOARD_INTERACTION - Unregister old context intention, prev state: ", GET_HEIST_FINALE_CONTEXT_REGISTRATION_STATE())
				// This takes a few frames.
				RELEASE_CONTEXT_INTENTION(g_HeistPlanningClient.iHeistFinalePlanContext)
						
				IF GET_HEIST_FINALE_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_NONE
					SET_HEIST_FINALE_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_NONE)
				ENDIF
			ENDIF
				
			IF g_HeistPlanningClient.iHeistFinalePlanContext = NEW_CONTEXT_INTENTION
			AND NOT g_HeistSharedClient.bSuppressPlanningBoardContextIntention
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_FAKE_HEIST_FINALE_BOARD_INTERACTION - Register new context intention, target state: ci_HEIST_REG_STATE_DEFAULT")
				REGISTER_CONTEXT_INTENTION(g_HeistPlanningClient.iHeistFinalePlanContext, CP_MAXIMUM_PRIORITY, "HEIST_PRE_DONE2")
				SET_HEIST_FINALE_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_DEFAULT)
			ENDIF
				
		BREAK
		
		
		CASE ci_HEIST_REG_STATE_ONCALL
		
			CLEAR_GENERIC_HEIST_HELP_TEXT()
			g_HeistSharedClient.bSuppressPlanningBoardContextIntention = IS_HELP_MESSAGE_BEING_DISPLAYED()
			
			IF GET_HEIST_FINALE_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_ONCALL
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_FAKE_HEIST_FINALE_BOARD_INTERACTION - Unregister old context intention, prev state: ", GET_HEIST_FINALE_CONTEXT_REGISTRATION_STATE())
				// This takes a few frames.
				RELEASE_CONTEXT_INTENTION(g_HeistPlanningClient.iHeistFinalePlanContext)
				 
				IF GET_HEIST_FINALE_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_NONE
					SET_HEIST_FINALE_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_NONE)
				ENDIF
			ENDIF
				
			IF g_HeistPlanningClient.iHeistFinalePlanContext = NEW_CONTEXT_INTENTION
			AND NOT g_HeistSharedClient.bSuppressPlanningBoardContextIntention
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_FAKE_HEIST_FINALE_BOARD_INTERACTION - Register new context intention, target state: ci_HEIST_REG_STATE_ONCALL")
				REGISTER_CONTEXT_INTENTION(g_HeistPlanningClient.iHeistFinalePlanContext, CP_MAXIMUM_PRIORITY, "HEIST_PRE_DONE")
				SET_HEIST_FINALE_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_ONCALL)
			ENDIF
				
		BREAK
			
		
		CASE ci_HEIST_REG_STATE_NONE
		
			IF GET_HEIST_FINALE_CONTEXT_REGISTRATION_STATE() != ci_HEIST_REG_STATE_NONE
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_FAKE_HEIST_FINALE_BOARD_INTERACTION - Resetting saved context intention state, value: ", GET_HEIST_FINALE_CONTEXT_REGISTRATION_STATE())
				SET_HEIST_FINALE_CONTEXT_REGISTRATION_STATE(ci_HEIST_REG_STATE_NONE)
			ENDIF
		
			IF g_HeistPlanningClient.iHeistFinalePlanContext != NEW_CONTEXT_INTENTION
				PRINTLN("[AMEC][HEIST_PREP] - MONITOR_FAKE_HEIST_FINALE_BOARD_INTERACTION - Resetting context intention.")
				RELEASE_CONTEXT_INTENTION(g_HeistPlanningClient.iHeistFinalePlanContext)
			ENDIF
			
			IF (GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), Get_Heist_Corona_Coords_For_Players_Currently_Occupied_Apartment()) < HEIST_CUTSCENE_TRIGGER_m)
		
				IF IS_IT_SAFE_TO_DISPLAY_HEIST_FINALE_BOARD_HELP()
		
					IF ARE_SAVES_AND_HEIST_PROGRESS_IN_TROUBLE()
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD")
							CLEAR_HELP()
						ENDIF
						PRINT_HELP_FOREVER("FM_COR_HCLOUD")
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FM_COR_HCLOUD")
							CLEAR_HELP()
						ENDIF
					ENDIF
					
					IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0)
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_WNTED")
							CLEAR_HELP()
						ENDIF
						PRINT_HELP_FOREVER("HEIST_ER_WNTED")
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_WNTED")
							CLEAR_HELP()
						ENDIF
					ENDIF
					
					IF IS_THIS_A_SOLO_SESSION()
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_SOLO")
							CLEAR_HELP()
						ENDIF
						PRINT_HELP_FOREVER("HEIST_ER_SOLO")
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HEIST_ER_SOLO")
							CLEAR_HELP()
						ENDIF
					ENDIF
				
				ENDIF
				
			ELSE
			
				IF IS_A_HEIST_FINALE_HELP_MESSAGE_CURRENTLY_DISPLAYING()
					CPRINTLN(DEBUG_MP_HEISTS_AMEC, "[HEIST_PREP] - MONITOR_FAKE_HEIST_FINALE_BOARD_INTERACTION - Finale message is ACTIVE but distance is too far, culling help.")
					CLEAR_HELP()
				ENDIF
				
			ENDIF
		
		EXIT
			
	ENDSWITCH

			
	IF NOT IS_PAUSE_MENU_ACTIVE() // Fix for 2165189, make sure we don't listen for inputs if the pause menu is up.
	AND NOT IS_PAUSE_MENU_RESTARTING()
	
		IF IS_CONTEXT_INTENTION_HELP_DISPLAYING(g_HeistPlanningClient.iHeistFinalePlanContext) 
		AND HAS_CONTEXT_BUTTON_TRIGGERED(g_HeistPlanningClient.iHeistFinalePlanContext, TRUE)
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_FAKE] - MONITOR_FAKE_HEIST_FINALE_BOARD_INTERACTION - Content intention fired, building finale corona.")
			#ENDIF
		
			FORCE_PLAYER_LOCALLY_VISIBLE_FOR_FADE(TRUE)
		
			RELEASE_CONTEXT_INTENTION(g_HeistPlanningClient.iHeistFinalePlanContext)
			
			SET_TRANSITION_SESSIONS_CLEAN_UP_ON_CALL_NOW_IF_NEEDED()

			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimFirst, DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE)
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond, TRUE, DEFAULT, DEFAULT, DEFAULT, TRUE)
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimThird, TRUE, DEFAULT, DEFAULT, DEFAULT, TRUE)
			
			SET_HEIST_SUPPRESS_HUD_FOR_PLANNING(TRUE)
			
			SET_HEIST_FINALE_STATE(HEIST_FLOW_FAKE_LAUNCH)
			
			SET_HEIST_CORONA_FROM_PLANNING_BOARD(TRUE)
			
		ELIF IS_CONTEXT_INTENTION_HELP_DISPLAYING(g_HeistPlanningClient.iHeistFinalePlanContext) 
		AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT_SECONDARY)
		AND NOT g_sMPTUNABLES.bDisable_Heist_On_Call_From_Planning_Board
		AND NOT Is_Ped_Drunk(PLAYER_PED_ID())
			IF REGISTER_FOR_HEIST_QUICK_MATCH_FROM_BOARD()
				RELEASE_CONTEXT_INTENTION(g_HeistPlanningClient.iHeistFinalePlanContext)
			ENDIF
		ENDIF
		
	ENDIF
		
ENDPROC


/// PURPOSE:
///    Only update the scaleform for the members in the current session. This is used for when you want to see the board in the background, but not interact with it.
PROC HEIST_START_UPDATE_FINALE_FAKE()
	
	// Don't bother wasting cycles if the local player isn't close to the board.
	IF NOT IS_LOCAL_PLAYER_NEAR_HEIST_PLANNING_BOARD(ci_HEIST_BOARD_LOAD_IN_RANGE)
	AND NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
		SET_HEIST_FINALE_STATE(HEIST_FLOW_FAKE_CLEANUP)
		CLEAN_HEIST_SHARED_DATA()
		EXIT
	ENDIF
	
	IF NOT GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bPlanningBoardReady
		PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_START_UPDATE_FINALE_FAKE - Setting local player BD bPlanningBoardReady to TRUE")
		GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].bPlanningBoardReady = TRUE
	ENDIF
	
	SET_HEIST_PLANNING_UPDATE_PREVIEW(	g_HeistSharedClient.PlanningBoardIndex, 
										ci_HEIST_PREVIEW_WINDOW_GEAR, 
										"", 
										"")
										
	SET_HEIST_PLANNING_UPDATE_PREVIEW(	g_HeistSharedClient.PlanningBoardIndex, 
										ci_HEIST_PREVIEW_WINDOW_OUTFIT, 
										"", 
										"")
										
	HEIST_UPDATE_FINALE_MEMBER_FAKE()
	
	HEIST_UPDATE_FINALE_MEMBER_CARD_FAKE()
	
	IF AM_I_LEADER_OF_HEIST()
	
		MONITOR_FAKE_HEIST_FINALE_BOARD_INTERACTION()
	
		g_enumActiveHeistAnim eHeistAnimSecond
		g_enumActiveHeistAnim eHeistAnimThird
		TEXT_LABEL_23 tlTemp = GET_CONTENTID_FOR_SLOT(ci_HEIST_CONTENT_INDEX_FINALE)
		INT iHash = GET_HASH_KEY(tlTemp)
					
		eHeistAnimSecond 	= GET_HEIST_INTO_ANIM_PRE(	iHash, 
														(NOT IS_PLAYER_PED_FEMALE(PLAYER_ID())),
														FALSE,
														FALSE,
														TRUE)
		
		eHeistAnimThird		= GET_HEIST_INTO_ANIM_DURING(iHash, 
														(NOT IS_PLAYER_PED_FEMALE(PLAYER_ID())),
														FALSE,
														FALSE,
														TRUE)

		PRELOAD_HEIST_ANIM_ASSETS(HEIST_SELECTED_ANIM_PHONE, eHeistAnimSecond, eHeistAnimThird)
		
	ENDIF

	DRAW_HEIST_SCALEFORM()

ENDPROC


/// PURPOSE:
///    Trigger the cleanup of the heist planning board.
PROC HEIST_FINALE_LAUNCH_FINALE_FROM_FAKE()
		
	IF NOT IS_CORONA_BIT_SET(CORONA_PLAYER_FULLY_ON_INTRO_SCREEN)
		SUPPRESS_FRONTEND_RENDERING_THIS_FRAME()
	ENDIF
	
	DRAW_HEIST_SCALEFORM()
		
	IF NOT g_HeistPlanningClient.bHeistCoronaActive
	
		TEXT_LABEL_23 tlMissionRootContID = GET_CONTENTID_FOR_SLOT(ci_HEIST_CONTENT_INDEX_FINALE)
		TEXT_LABEL_23 contentID = GET_CONTENTID_FROM_ROOT_CONTENTID(GET_HASH_KEY(tlMissionRootContID))
		
		IF Setup_My_Heist_Corona_From_ContentID(contentID, TRUE)
		
			// Move the player to the corona location.
			VECTOR vHeistCoronaLocation
			GET_PLAYER_PROPERTY_HEIST_LOCATION(vHeistCoronaLocation, MP_PROP_ELEMENT_HEIST_PLAN_LOC, GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vHeistCoronaLocation)
			
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_FAKE] - HEIST_FINALE_LAUNCH_FINALE_FROM_FAKE - bHeistCoronaActive = FALSE, setting up corona. Warped player to: SET_ENTITY_COORDS(",vHeistCoronaLocation,")")
			#ENDIF
			
			g_HeistPlanningClient.bHeistCoronaActive = TRUE
			
		ENDIF
		
	ENDIF


	IF g_HeistPrePlanningClient.bLaunchFromPhone
	
		IF NOT IS_SKYSWOOP_AT_GROUND()
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_FAKE] - HEIST_FINALE_LAUNCH_FINALE_FROM_FAKE - Triggering launch of heist finale mission from PHONE. SkySwoop NOT at ground.")
			#ENDIF
		
			g_enumActiveHeistAnim eHeistAnimSecond
			g_enumActiveHeistAnim eHeistAnimThird
			TEXT_LABEL_23 tlTemp = GET_CONTENTID_FOR_SLOT(ci_HEIST_CONTENT_INDEX_FINALE)
			INT iHash = GET_HASH_KEY(tlTemp)
			
			eHeistAnimSecond 	= GET_HEIST_INTO_ANIM_PRE(	iHash, 
															(NOT IS_PLAYER_PED_FEMALE(INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex))),
															FALSE,
															FALSE,
															TRUE)
	
			eHeistAnimThird		= GET_HEIST_INTO_ANIM_DURING(iHash, 
															(NOT IS_PLAYER_PED_FEMALE(INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex))),
															FALSE,
															FALSE,
															TRUE)

			IF NOT IS_ANIM_CONTROLLER_ALREADY_ACTIVE(g_HeistSharedClient.sHeistAnimSecond) 	OR NOT ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimSecond)
			OR NOT IS_ANIM_CONTROLLER_ALREADY_ACTIVE(g_HeistSharedClient.sHeistAnimThird) 	OR NOT ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimThird)
				
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_FAKE] - HEIST_FINALE_LAUNCH_FINALE_FROM_FAKE - WARNING! Attempted launch of finale without building anim controllers; doing last minuite build.")
				#ENDIF
				
				PRELOAD_HEIST_ANIM_ASSETS_2(g_HeistSharedClient.sHeistAnimSecond, eHeistAnimSecond, 1,
											g_HeistSharedClient.sHeistAnimThird, eHeistAnimThird, 2)
			ENDIF
			
			IF NOT HAS_HEIST_STRAND_ANIM_FINISHED(g_HeistSharedClient.sHeistAnimSecond)
				MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond, DEFAULT, DEFAULT, FALSE, FALSE, TRUE, TRUE)
			ENDIF
			
			EXIT
		
		ELSE
		
			#IF IS_DEBUG_BUILD
				PRINTLN("[AMEC][HEIST_FAKE] - HEIST_FINALE_LAUNCH_FINALE_FROM_FAKE - Triggering launch of heist finale mission from PHONE. SkySwoop at ground.")
			#ENDIF
		
			IF g_HeistSharedClient.sHeistAnimSecond.bAtLastFrame
				//	---	B* 2145099 RowanJ	-	Stop MenuMGHeistIntro when corona comes on and change to MenuMGHeistTint
				IF ANIMPOSTFX_IS_RUNNING("MenuMGHeistIntro")
					PRINTLN("[AMEC][HEIST_MISC] - HEIST_FINALE_LAUNCH_FINALE_FROM_FAKE - Stopping heist PostFX: MenuMGHeistIntro.")
					ANIMPOSTFX_STOP("MenuMGHeistIntro")
				ENDIF
				
				IF NOT ANIMPOSTFX_IS_RUNNING("MenuMGHeistIn")
					PRINTLN("[AMEC][HEIST_MISC] - HEIST_FINALE_LAUNCH_FINALE_FROM_FAKE - Triggering heist PostFX: MenuMGHeistIn.")
					ANIMPOSTFX_PLAY("MenuMGHeistIn", 0, TRUE)
					TOGGLE_RENDERPHASES(FALSE, TRUE)
				ENDIF
			ENDIF
			
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond, DEFAULT, DEFAULT, FALSE, DEFAULT, TRUE)
		
			IF NOT HAS_HEIST_STRAND_ANIM_FINISHED(g_HeistSharedClient.sHeistAnimSecond)
			OR g_HeistSharedClient.sHeistAnimSecond.bAtLastFrame	
				//PRINTLN("[AMEC][HEIST_MISC] - HEIST_FINALE_LAUNCH_FINALE_FROM_FAKE - Exit conditions: ANIM FINISHED? ", PICK_STRING(HAS_HEIST_STRAND_ANIM_FINISHED(g_HeistSharedClient.sHeistAnimSecond),"TRUE","FALSE"),", bAtLastFrame? ", PICK_STRING(g_HeistSharedClient.sHeistAnimSecond.bAtLastFrame,"TRUE","FALSE"))
				EXIT
			ENDIF
		ENDIF
		
	ELSE
	
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_FAKE] - HEIST_FINALE_LAUNCH_FINALE_FROM_FAKE - Triggering launch of heist finale mission from ROOM.")
		#ENDIF
	
		IF NOT HAS_HEIST_STRAND_ANIM_FINISHED(g_HeistSharedClient.sHeistAnimFirst)
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimFirst, DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE)
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond, TRUE, DEFAULT, DEFAULT, DEFAULT, TRUE)
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimThird, TRUE, DEFAULT, DEFAULT, DEFAULT, TRUE)
			EXIT
		ELSE
			
			IF g_HeistSharedClient.sHeistAnimSecond.bAtLastFrame
				//	---	B* 2145099 RowanJ	-	Stop MenuMGHeistIntro when corona comes on and change to MenuMGHeistTint
				IF ANIMPOSTFX_IS_RUNNING("MenuMGHeistIntro")
					PRINTLN("[AMEC][HEIST_MISC] - HEIST_FINALE_LAUNCH_FINALE_FROM_FAKE - Stopping heist PostFX: MenuMGHeistIntro.")
					ANIMPOSTFX_STOP("MenuMGHeistIntro")
				ENDIF
				
				IF NOT ANIMPOSTFX_IS_RUNNING("MenuMGHeistIn")
					PRINTLN("[AMEC][HEIST_MISC] - HEIST_FINALE_LAUNCH_FINALE_FROM_FAKE - Triggering heist PostFX: MenuMGHeistIn.")
					ANIMPOSTFX_PLAY("MenuMGHeistIn", 0, TRUE)
					TOGGLE_RENDERPHASES(FALSE, TRUE)
				ENDIF
			ENDIF
		
			IF NOT g_HeistSharedClient.sHeistAnimFirst.bBasicCleanupFinished
				MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimFirst, FALSE)
			ENDIF
			
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE)
			MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimThird, TRUE, DEFAULT, DEFAULT, DEFAULT, TRUE)
			
			IF NOT HAS_HEIST_STRAND_ANIM_FINISHED(g_HeistSharedClient.sHeistAnimSecond)
			OR g_HeistSharedClient.sHeistAnimSecond.bAtLastFrame
				PRINTLN("[AMEC][HEIST_MISC] - HEIST_FINALE_LAUNCH_FINALE_FROM_FAKE - Exit conditions: ANIM FINISHED? ", PICK_STRING(HAS_HEIST_STRAND_ANIM_FINISHED(g_HeistSharedClient.sHeistAnimSecond),"TRUE","FALSE"),", bAtLastFrame? ", PICK_STRING(g_HeistSharedClient.sHeistAnimSecond.bAtLastFrame,"TRUE","FALSE"))
				EXIT
			ENDIF
		ENDIF
		
	ENDIF
	
	g_HeistPrePlanningClient.bLaunchFromPhone = FALSE
	
	SET_PLAYER_INVISIBLE_LOCALLY(PLAYER_ID())
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
		
	g_HeistPlanningClient.bHeistCoronaActive = FALSE
	
	MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimSecond)
	MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimThird, DEFAULT, FALSE, DEFAULT, TRUE, TRUE)
		
	SET_HEIST_FINALE_STATE(HEIST_FLOW_FAKE_IDLE)

ENDPROC


/// PURPOSE:
///    Maintain the background idle animation.
PROC HEIST_FINALE_IDLE_FAKE()

	IF NOT IS_CORONA_BIT_SET(CORONA_PLAYER_FULLY_ON_INTRO_SCREEN)
		SUPPRESS_FRONTEND_RENDERING_THIS_FRAME()
	ENDIF

	IF NOT ARE_HEIST_ANIM_ASSETS_LOADED(g_HeistSharedClient.sHeistAnimThird)
		#IF IS_DEBUG_BUILD
			PRINTLN("[AMEC][HEIST_LAUNCH] - HEIST_FINALE_IDLE_FAKE - ERROR! Attempted launch of idle anim without building anim controller; doing last minuite build.")
		#ENDIF
		
		g_enumActiveHeistAnim eHeistAnim
		
		IF (IS_PLAYER_PED_FEMALE(INT_TO_PLAYERINDEX(g_HeistSharedClient.iCurrentPropertyOwnerIndex)))
			eHeistAnim = HEIST_SELECTED_ANIM_WINE_IDLE
		ELSE
			eHeistAnim = HEIST_SELECTED_ANIM_WHISKY_IDLE
		ENDIF

		PRELOAD_HEIST_ANIM_ASSET_SINGLE(g_HeistSharedClient.sHeistAnimThird, eHeistAnim, FALSE)
	ENDIF

	MAINTAIN_HEIST_STRAND_INTRO_ANIMATIONS(g_HeistSharedClient.sHeistAnimThird, DEFAULT, FALSE, DEFAULT, TRUE, TRUE)

ENDPROC


#IF IS_DEBUG_BUILD
PROC DEBUG_DRAW_MISSION_DATA()

	IF bRenderPlanningDebugText

		TEXT_LABEL_63 tempDebug1 = "FlowState: "
		TEXT_LABEL_63 tempDebug2 = ENUM_TO_INT(GET_HEIST_FINALE_STATE())
		tempDebug1 += tempDebug2
		SET_TEXT_COLOUR(255, 1, 1, 255)
		SET_TEXT_SCALE(0.0000, 0.4300)
		SET_TEXT_WRAP(0.0, 1.0)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.7350, "STRING", tempDebug1)
		
		tempDebug1 = "Highlighting(M/S/L/R): "
		tempDebug2 = g_HeistPlanningClient.sBoardState.iHighlight
		TEXT_LABEL_63 tempDebug3 = " : "
		tempDebug1 += tempDebug2
		tempDebug1 += tempDebug3
		
		tempDebug2 = g_HeistPlanningClient.sBoardState.iSubHighlight
		tempDebug1 += tempDebug2
		tempDebug1 += tempDebug3
		
		tempDebug2 = g_HeistPlanningClient.sBoardState.iLeftArrow
		tempDebug1 += tempDebug2
		tempDebug1 += tempDebug3
		
		tempDebug2 = g_HeistPlanningClient.sBoardState.iRightArrow
		tempDebug1 += tempDebug2
		SET_TEXT_COLOUR(255, 1, 1, 255)
		SET_TEXT_SCALE(0.0000, 0.4300)
		SET_TEXT_WRAP(0.0, 1.0)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.7600, "STRING", tempDebug1)	
		
		tempDebug1 = "CameraState: "
		tempDebug2 = ENUM_TO_INT(g_HeistPlanningClient.sBoardState.eFocusPoint)
		tempDebug1 += tempDebug2
		SET_TEXT_COLOUR(255, 1, 1, 255)
		SET_TEXT_SCALE(0.0000, 0.4300)
		SET_TEXT_WRAP(0.0, 1.0)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.05, 0.7850, "STRING", tempDebug1)

	ENDIF

ENDPROC
#ENDIF


/// PURPOSE:
///    Main fake heist planning maintenance procedure. Runs every frame, and handles the states for the faked finale
///    planning board that is shown in the background in the leader's apartment.
/// PARAMS:
///    sFakeMissionData - Struct containing the faked mission data.
///    scaleformStruct - Scaleform instr. btns. struct from FMMC_LAUNCHER.
PROC MANAGE_PLAYER_START_HEIST_PLANNING_FAKE()
	
	SWITCH GET_HEIST_FINALE_STATE()
	
		CASE HEIST_FLOW_NONE
		CASE HEIST_FLOW_INIT
		CASE HEIST_FLOW_SERVER_INIT
		CASE HEIST_FLOW_CREATE_BOARD
		CASE HEIST_FLOW_UPDATE_BACKGROUND
		CASE HEIST_FLOW_INIT_FINALE
		CASE HEIST_FLOW_UPDATE_ALL
		CASE HEIST_FLOW_EXIT_PENDING
		CASE HEIST_FLOW_CLEANUP
		CASE HEIST_FLOW_FAKE_INIT
		
			HEIST_START_GET_INIT_DATA_FAKE()
		
			BREAK
			
		CASE HEIST_FLOW_FAKE_UPDATE
		
			HEIST_START_UPDATE_FINALE_FAKE()
		
			BREAK
			
		CASE HEIST_FLOW_FAKE_LAUNCH
		
			HEIST_FINALE_LAUNCH_FINALE_FROM_FAKE()
		
			BREAK
			
		CASE HEIST_FLOW_FAKE_IDLE
		
			HEIST_FINALE_IDLE_FAKE()
			
			BREAK
			
		CASE HEIST_FLOW_FAKE_CLEANUP	
		
			HEIST_FINALE_CLEAN_ALL_FAKE()
		
			BREAK
			
		DEFAULT
		
			PRINTLN("[AMEC][HEIST_FAKE] - MANAGE_PLAYER_START_HEIST_PLANNING_FAKE - ERROR! GET_HEIST_FINALE_STATE() was set to DEFAULT. Value: ", ENUM_TO_INT(GET_HEIST_FINALE_STATE()))
		
			BREAK

	ENDSWITCH
												
ENDPROC


/// PURPOSE:
///    Main heist planning maintenance procedure. Runs every frame, and handles the states for the entire planning process.
/// PARAMS:
///    scaleformStruct - Scaleform instructional button struct.
///    sLaunchMissionDetails - Mission details.
///    bAllowHeistConfig - Allow configuration of the heist board.
///    bDoLaunch - ByRef launch flag.
PROC MANAGE_PLAYER_START_HEIST_PLANNING(	CORONA_MENU_DATA&					coronaMenuData,
											SCALEFORM_INSTRUCTIONAL_BUTTONS&	scaleformStruct,
											MISSION_TO_LAUNCH_DETAILS&			sLaunchMissionDetails,
											BOOL								bAllowHeistConfig,
											BOOL&								bDoLaunch)
	
	SWITCH GET_HEIST_FINALE_STATE()
		CASE HEIST_FLOW_INIT
			HEIST_START_GET_INIT_DATA()
			BREAK
			
		CASE HEIST_FLOW_SERVER_INIT
			HEIST_START_INIT_SERVER_CONFIG()
			BREAK
			
		CASE HEIST_FLOW_CREATE_BOARD
			HEIST_START_CREATE_BOARD()
			BREAK
			
		CASE HEIST_FLOW_UPDATE_BACKGROUND
			HEIST_START_UPDATE_FINALE_BACKGROUND(bAllowHeistConfig)
			BREAK
			
		CASE HEIST_FLOW_INIT_FINALE
			HEIST_START_INIT_FINALE_PLANNING_MODE()
			BREAK
			
		CASE HEIST_FLOW_UPDATE_ALL
			HEIST_START_UPDATE_FINALE_ALL(scaleformStruct, bAllowHeistConfig, bDoLaunch)
			BREAK
			
		CASE HEIST_FLOW_CUT_PENDING
			HEIST_START_CONFIRM_CUTS(scaleformStruct)
			BREAK
			
		CASE HEIST_FLOW_EXIT_PENDING
			HEIST_START_CONFIRM_EXIT(scaleformStruct, bDoLaunch)
			BREAK
			
		CASE HEIST_FLOW_CLEANUP
			HEIST_START_CLEAN_ALL(coronaMenuData, sLaunchMissionDetails)
			BREAK
			
		CASE HEIST_FLOW_IDLE
			// Do nothing
			CDEBUG3LN(DEBUG_MP_HEISTS_AMEC, "[HEIST_LAUNCH] - MANAGE_PLAYER_START_HEIST_PLANNING - In HEIST_FLOW_IDLE, do nothing.")
			BREAK
			
		CASE HEIST_FLOW_FAKE_INIT
		CASE HEIST_FLOW_FAKE_UPDATE
		CASE HEIST_FLOW_FAKE_LAUNCH
		CASE HEIST_FLOW_FAKE_CLEANUP
			CLEAN_HEIST_FINALE_ALL(Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
			PRINTLN("[AMEC][HEIST_LAUNCH] - MANAGE_PLAYER_START_HEIST_PLANNING - ERROR! Heist finale state machine has been set to a fake value: ", ENUM_TO_INT(GET_HEIST_FINALE_STATE()))
			BREAK
			
		DEFAULT
			SET_HEIST_FINALE_STATE(HEIST_FLOW_INIT)
			BREAK
	ENDSWITCH

// Draw the mission data as text labels on the screen.
#IF IS_DEBUG_BUILD
	DEBUG_DRAW_MISSION_DATA()	
#ENDIF	

ENDPROC


// HEIST FINALE LAUNCH DEBUG
/// PURPOSE:
///    Monitor debug inputs.
PROC MAINTAIN_HEIST_PLANNING_DEBUG_INPUTS(BOOL& bDoLaunch)
	IF bDoLaunch = bDoLaunch
	ENDIF
	#IF IS_DEBUG_BUILD
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD6)
			
		GAMER_HANDLE emptyHandle
		INT i
		REPEAT NUM_NETWORK_PLAYERS i
			lastHeistPlayers.Gamers[i] = emptyHandle
			lastHeistPlayers.GamersNames[i] = ""
			CLEAR_BIT(lastHeistPlayers.iStoredPlayersBS,i)
		ENDREPEAT
		
		PRINTLN("[AMEC][HEIST_LAUNCH] - MAINTAIN_HEIST_PLANNING_DEBUG_INPUTS - Cleaned saved heist members.")
	
	ENDIF
	#ENDIF
	
	IF GET_HEIST_FINALE_STATE() = HEIST_FLOW_UPDATE_ALL
		#IF IS_DEBUG_BUILD
		IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD1, KEYBOARD_MODIFIER_SHIFT, "bAllowMissionsWithOnePlayer")
		
			IF NOT g_HeistPlanningClient.bHaveDoneHelp
				PRINTLN("[AMEC][HEIST_LAUNCH] - MAINTAIN_HEIST_PLANNING_DEBUG_INPUTS - Skip heist planning help text called, clearing current help text.")
				
				SET_HEIST_CAM_LOCATION_INDEX(ci_HEIST_CAMERA_POS_BOARD)
				g_HeistPlanningClient.sBoardState.iHighlight	= ci_HEIST_LEADER
				TRIGGER_SMOOTH_CAMERA_TRANSITION(g_HeistPlanningClient.sBoardCamData, ci_HEIST_CAMERA_POS_BOARD)
				g_HeistPlanningClient.DEBUG_bSkipPlanningHelpText = TRUE
				CLEAR_HELP()
				EXIT
			ENDIF
		
			PRINTLN("[AMEC][HEIST_LAUNCH] - MAINTAIN_HEIST_PLANNING_DEBUG_INPUTS - Force launching heist...")
			
			INT index
			FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
				IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] = NATIVE_TO_INT(PLAYER_ID())
					#IF IS_DEBUG_BUILD
					IF (DEBUG_IS_ALLOW_ALL_MISSION_WITH_TWO_PLAYERS_ENABLED())
						GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = 0
						PRINTLN("MAINTAIN_HEIST_PLANNING_DEBUG_INPUTS - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen	= 0")
					ELSE
					#ENDIF
					
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ENUM_TO_INT(GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index])
					PRINTLN("MAINTAIN_HEIST_PLANNING_DEBUG_INPUTS - GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].sClientCoronaData.iTeamChosen = ", ENUM_TO_INT(GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index]))
					
					#IF IS_DEBUG_BUILD
					ENDIF
					#ENDIF
				ENDIF
			ENDFOR			
			
			FOR index = 0 TO (NUM_NETWORK_PLAYERS-1)
				PRINTLN("[AMEC][HEIST_LAUNCH] - MAINTAIN_HEIST_PLANNING_DEBUG_INPUTS - SAVED PLAYER [",index,"]: ", lastHeistPlayers.GamersNames[index])
			ENDFOR
			
			IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
				SET_HEIST_AM_I_HEIST_LEADER_GLOBAL(TRUE)
				g_HeistPlanningClient.bForceHeistPlanningComplete = TRUE
				GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].iPreviousPropertyIndex = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
			
				TEXT_LABEL_23 tlContID = GET_HEIST_FINALE_STAT_DATA()
				#IF IS_DEBUG_BUILD
					PRINTLN("[AMEC][HEIST_LAUNCH] - MAINTAIN_HEIST_PLANNING_DEBUG_INPUTS - Heist leader: ",GET_PLAYER_NAME(PLAYER_ID())," got rContID: ",tlContID," from stats. Putting into playerDB...")
				#ENDIF
				GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].tlHeistFinaleRootContID = tlContID
				
			ELSE
				SET_HEIST_AM_I_HEIST_LEADER_GLOBAL(FALSE)
				GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].iPreviousPropertyIndex = INVALID_HEIST_DATA
			ENDIF
			
			PRINTLN("[AMEC][HEIST_LAUNCH] - MAINTAIN_HEIST_PLANNING_DEBUG_INPUTS - iPreviousPropertyIndex = ", GlobalplayerBD_FM_HeistPlanning[NETWORK_PLAYER_ID_TO_INT()].iPreviousPropertyIndex)
			
			// Check if outfit has been set for your local player. If not, pick the default one for your current team.
			FOR index = 0 TO (MAX_HEIST_PLAYER_SLOTS-1)
				IF GlobalServerBD_HeistPlanning.iPlayerOrder[index] != -1
					IF INT_TO_PLAYERINDEX(GlobalServerBD_HeistPlanning.iPlayerOrder[index]) = PLAYER_ID()
						IF IS_BIT_SET(g_HeistPlanningClient.iPlayerNetStatusBitset, index)
							IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
								
								g_HeistPlanningClient.m_ishost = TRUE // Telemetry / stat tracking.
								g_HeistPlanningClient.m_role = GET_HEIST_MEMBER_TEAM_NUMBER(ENUM_TO_INT(g_HeistPlanningClient.sHeistRoles.ePlayerRoles[index])) // Telemetry / stat tracking.
								g_HeistPlanningClient.m_cashcutpercentage = g_HeistPlanningClient.sHeistCut.iCutPercent[index] // Telemetry / stat tracking.
								PRINTLN("[AMEC][HEIST_LAUNCH] - MAINTAIN_HEIST_PLANNING_DEBUG_INPUTS - LEADER - Saving cut value: ", g_HeistPlanningClient.sHeistCut.iCutPercent[index], " to NonTransitionVars.")
								g_TransitionSessionNonResetVars.iHeistCutPercent = g_HeistPlanningClient.sHeistCut.iCutPercent[index]
								
							ELSE
								
								g_HeistPlanningClient.m_ishost = FALSE // Telemetry / stat tracking.
								g_HeistPlanningClient.m_role = GET_HEIST_MEMBER_TEAM_NUMBER(ENUM_TO_INT(GlobalServerBD_HeistPlanning.sHeistRoles.ePlayerRoles[index])) // Telemetry / stat tracking.
								g_HeistPlanningClient.m_cashcutpercentage = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[index] // Telemetry / stat tracking.
								PRINTLN("[AMEC][HEIST_LAUNCH] - MAINTAIN_HEIST_PLANNING_DEBUG_INPUTS - GUEST - Saving cut value: ", GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[index], " to NonTransitionVars.")
								g_TransitionSessionNonResetVars.iHeistCutPercent = GlobalServerBD_HeistPlanning.sHeistCut.iCutPercent[index]
							
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
						SAVE_HEIST_MEMBER_DATA_FOR_QUICK_RESTART(index)
					ENDIF
				ENDIF
			ENDFOR
			
			PRINTLN("[AMEC][HEIST_LAUNCH] - MAINTAIN_HEIST_PLANNING_DEBUG_INPUTS - Setting iLocalPlayerTeam to value: ", g_HeistPlanningClient.m_role)
			g_HeistPlanningClient.iLocalPlayerTeam = g_HeistPlanningClient.m_role
	
			CALCULATE_STATS_FOR_POST_HEIST()
			
			// Get the time spent on the finale planning board.
			INT iTimeOnBoard
			iTimeOnBoard = GET_TIME_DIFFERENCE(g_HeistPlanningClient.tsBoardTotalPlanningTime, GET_NETWORK_TIME())
			g_HeistPlanningClient.m_TimePlanningBoard = iTimeOnBoard
			PRINTLN("[AMEC][HEIST_LAUNCH] - MAINTAIN_HEIST_PLANNING_DEBUG_INPUTS - Setting m_TimePlanningBoard to: ", iTimeOnBoard)
			
			#IF IS_DEBUG_BUILD
			IF DOES_WIDGET_GROUP_EXIST(DEBUG_PlanningWidgets)
				DELETE_WIDGET_GROUP(DEBUG_PlanningWidgets)
			ENDIF
			#ENDIF
			
			SET_HIGHLIGHTER_TO_LOCAL_PLAYER()
			
			PRINTLN("[AMEC][HEIST_LAUNCH] - MAINTAIN_HEIST_PLANNING_DEBUG_INPUTS - Debug launch executed successfully, returning TRUE back to FMMC LAUNCHER.")
			
			bDoLaunch = TRUE
			
			EXIT
		
		ENDIF
		#ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Returns TRUE once the opening cutscene has finished
/// NOTES:
///    KGM 27/4/14: Accepting the RootContentID Hash as parameter is 'probably' temp
FUNC BOOL CONTROL_FM_HEIST_CUTSCENE(INT paramHashRCID)
	
	// If the blur effects are running then stop them
	// KGM NOTE: We may be able to move this into the 'start cutscene' stage of the Heist Cutscene maintenance routine.
	//			Currently including "FMMC_Corona_Controller.sch" in there causes problems.
	// AMEC: this needs to be moved into the correct state in the cutscene FSM soon.
	IF GET_HEIST_ENTER_CUTS_SUB_STATE() = HMS_SUB_STAGE_WAIT AND IS_HEIST_ANIM_PLAYING(g_HeistSharedClient.sHeistAnimFirst)
	OR g_heistMocap.hasSafetyTimedOut
	OR g_heistMocap.hasEstTimedOut
	OR g_heistMocap.bForceScreenCleanup
		IF g_heistMocap.bHaveCleanedScreen = FALSE
			PRINTLN(".KGM [HEIST][CUTSCENE] - CONTROL_FM_HEIST_CUTSCENE - Cleaning up Screen Effects before playing Heist Cutscene")
			SET_FRONTEND_ACTIVE(FALSE)
			CLEAN_UP_INTRO_STAGE_CAM(g_sTransitionSessionData.ciCam, g_FMMC_STRUCT.iMissionType, TRUE, FALSE, FALSE)			
			CORONA_CLEAN_UP_SCREEN_STATE(FALSE)
//			ANIMPOSTFX_PLAY(GET_CORONA_FX_OUT(TRUE), 0, TRUE) // Removed as per 2094482.

			IF HEIST_IS_NETWORK_VOICE_ACTIVE()
				HEIST_SET_NETWORK_VOICE_ACTIVE(FALSE)
			ENDIF
	
			g_heistMocap.bHaveCleanedScreen = TRUE
			g_heistMocap.hasEstTimedOut = FALSE
		ENDIF
	ENDIF
	
	TEXT_LABEL_23 heistCutsceneName = TEMP_Get_Mocap_Name_From_FinaleRCID_Hash(paramHashRCID, FALSE, TRUE, FALSE, FALSE, FALSE)
	INT iCutsceneSlots = TEMP_Get_Mocap_Participant_Number_From_FinaleRCID_Hash(paramHashRCID)
	
	IF (IS_STRING_NULL_OR_EMPTY(heistCutsceneName))
		PRINTLN(".KGM [HEIST][CUTSCENE] Heist Finale Cutscene Name not found - Ignoring")
		RETURN TRUE
	ENDIF
		
	// Allow skip if Heist Leader AND Heist has been completed at least once as Leader (the Leader will be in their own property)
	BOOL allowSkip = AM_I_LEADER_OF_HEIST()
	IF (allowSkip)
		allowSkip = (GET_NUMBER_OF_TIMES_HEIST_STRAND_COMPLETED(paramHashRCID, TRUE) > 0)
		
		IF NOT allowSkip
			IF AM_I_LEADER_OF_HEIST()
				IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_CUTS_DONE_FINALE)
					allowSkip = TRUE
					PRINTLN(".KGM [HEIST][CUTSCENE] Strand not already completed but cutscene was; allowing skippable.")
				ENDIF
			ENDIF
		ENDIF
			
		#IF IS_DEBUG_BUILD
			IF (GET_COMMANDLINE_PARAM_EXISTS("sc_HeistCutSkippable"))
				allowSkip = TRUE
			ENDIF
		#ENDIF
	ENDIF
	
	// Finale Cutscenes should fade at the end
	BOOL isFinale = TRUE
	
	// Monitor the setup of our camera which we will swoop to at the end of the cutscene
	IF (g_heistMocap.stage >= HMS_STAGE_CHECK_CUTSCENE_PLAYING)
		IF NOT IS_CORONA_BIT_SET(CORONA_FP_CAMERA_SET_UP_FOR_CUTSCENE)
			
			PRINTLN(".KGM [HEIST][CUTSCENE] Initialise finale FP Camera now for end of cutscene active cam transition")
			
			IF IS_PLAYER_LEADER_OF_HEIST(PLAYER_ID())
				g_HeistPlanningClient.bHaveDoneHelp = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_PLANNING_DONE_HELP_0)
				g_HeistPlanningClient.sBoardState.iSubHighlight = ci_HEIST_LEADER
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [HEIST][CUTSCENE] - CONTROL_FM_HEIST_CUTSCENE - LEADER Setting iSubHighlight to: ", ci_HEIST_LEADER)
				#ENDIF
			ELSE
				g_HeistPlanningClient.bHaveDoneHelp = GET_MP_BOOL_CHARACTER_STAT(MP_STAT_HEIST_PLANNING_DONE_HELP_1)
				g_HeistPlanningClient.sBoardState.iSubHighlight = INVALID_HEIST_INDEX
				#IF IS_DEBUG_BUILD
					PRINTLN(".KGM [HEIST][CUTSCENE] - CONTROL_FM_HEIST_CUTSCENE - CLIENT Setting iSubHighlight to: ", INVALID_HEIST_INDEX)
				#ENDIF
			ENDIF
			
			CREATE_HEIST_FINALE_BOARD_CAMERA()
			SET_CORONA_BIT(CORONA_FP_CAMERA_SET_UP_FOR_CUTSCENE)
		ENDIF
	ENDIF
	
	PRINTLN(".KGM [HEIST][CUTSCENE] Playing Heist Finale Cutscene: ", heistCutsceneName, ", available slots: ", iCutsceneSlots)
	RETURN (Maintain_Net_Heist_Cutscenes(heistCutsceneName, allowSkip, isFinale, DEFAULT, iCutsceneSlots))

ENDFUNC


/// PURPOSE: 
///    Trigger and maintain the "fake" heist finale planning board. This is the planning board that is used outside the corona/mission download system.
///    As a result, we need to use fake (abridged) mission data that has been downloaded outside the the standard flow.
PROC CONTROL_FM_HEIST_PLANNING_SCREEN_FAKE() // If debug != optional, and therefore must be above. This means the intellisense fucks up.
		
	IF IS_MP_HEIST_PRE_PLANNING_ACTIVE()
		PRINTLN("[AMEC][HEIST_FAKE] - CONTROL_FM_HEIST_PLANNING_SCREEN_FAKE - Waiting for cleaning out old pre-planning data.")
		CLEAN_HEIST_PRE_PLANNING_LITE(TRUE)
		CLEAN_ALL_HEIST_PRE_PLANNING(FALSE) // Make sure we don't reset the progress here. Doing so causes the prep board to
											// attempt to load again, leaving us in a bad situation as it unloads the scaleform.
	ENDIF
	
	IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
	OR IS_PLAYER_IN_MP_GARAGE(PLAYER_ID(), FALSE)
		IF GET_HEIST_FINALE_STATE() != HEIST_FLOW_NONE
			PRINTLN("[AMEC][HEIST_LAUNCH] - CONTROL_FM_HEIST_PLANNING_SCREEN_FAKE - Heist fake finale NOT clean and player NOT in apartment, force cleanup.")
			CLEAN_HEIST_FINALE_ALL(Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
			CLEAN_HEIST_SHARED_DATA()
		ENDIF
		
		EXIT
	ENDIF
	
	IF SHOULD_HEIST_JOB_INVITE_HOLD_PLANNING_BOARD()
		IF GET_HEIST_FINALE_STATE() != HEIST_FLOW_NONE
			PRINTLN("[TS][HJOBI][AMEC][HEIST_LAUNCH] - CONTROL_FM_HEIST_PLANNING_SCREEN_FAKE - SHOULD_HEIST_JOB_INVITE_HOLD_PLANNING_BOARD() = TRUE, exiting heist fake finale planning.")
			CLEAN_HEIST_FINALE_ALL(Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
			CLEAN_HEIST_SHARED_DATA()
		ENDIF
		
		EXIT
	ENDIF
	
	IF NOT IS_THIS_A_HEIST_PROPERTY(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		IF GET_HEIST_FINALE_STATE() != HEIST_FLOW_NONE
			PRINTLN("[AMEC][HEIST_LAUNCH] - CONTROL_FM_HEIST_PLANNING_SCREEN_FAKE - IS_THIS_A_HEIST_PROPERTY() = FALSE, property ID: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			CLEAN_HEIST_FINALE_ALL(Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
			CLEAN_HEIST_SHARED_DATA()
		ENDIF
	ENDIF
	
	IF AM_I_LEADER_OF_HEIST(TRUE)
		IF NOT IS_MP_HEIST_STRAND_ACTIVE()
			IF GET_HEIST_FINALE_STATE() != HEIST_FLOW_NONE
				PRINTLN("[AMEC][HEIST_LAUNCH] - CONTROL_FM_HEIST_PLANNING_SCREEN_FAKE - IS_MP_HEIST_STRAND_ACTIVE() = FALSE, exiting heist fake finale planning.")
				CLEAN_HEIST_FINALE_ALL(Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
				CLEAN_HEIST_SHARED_DATA()
			ENDIF
		ENDIF
	ENDIF
	
	// Hides the hud when needed
	PROCESS_HEIST_PLANNING_ROOM_HUD()
	
	// Heist planning board main maintenance procedure.
	MANAGE_PLAYER_START_HEIST_PLANNING_FAKE()
											
ENDPROC


/// PURPOSE: 
///    Returns TRUE once the heist planning is complete.
///    If bAllowHeistConfig is FALSE, this should be called as a procedure.
FUNC BOOL CONTROL_FM_HEIST_PLANNING_SCREEN(	CORONA_MENU_DATA&					coronaMenuData
											, MISSION_TO_LAUNCH_DETAILS&		sLaunchMissionDetails
											, SCALEFORM_INSTRUCTIONAL_BUTTONS&	scaleformStruct
											, INT								iHeistLeader
											, BOOL								bAllowHeistConfig
											#IF IS_DEBUG_BUILD , WIDGET_GROUP_ID wgParentWidgetGroup #ENDIF ) // If debug != optional, and therefore must be above. This means the intellisense fucks up.

//	// CHECK PREREQUISITES.
	INT iRootContentID = GET_STRAND_ROOT_CONTENT_ID_HASH()
	IF iRootContentID = 0
		iRootContentID = g_FMMC_STRUCT.iRootContentIdHash
	ENDIF

	IF NOT Is_This_A_Known_Heist_Finale(iRootContentID)
		RETURN TRUE
	ENDIF
	
	//get out of here is this is a quick restart corona
	IF IS_CORONA_INITIALISING_A_QUICK_RESTART()
		PRINTLN("[AMEC][HEIST_LAUNCH] - IS_CORONA_INITIALISING_A_QUICK_RESTART")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
		IF g_bReducedApartmentCreationTurnedOn
			PRINTLN("[AMEC][HEIST_LAUNCH] - CONTROL_FM_HEIST_PLANNING_SCREEN - FALSE g_bReducedApartmentCreationTurnedOn")
			RETURN FALSE
		ENDIF
	ENDIF
	
	IF NOT IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE)
	OR IS_PLAYER_IN_MP_GARAGE(PLAYER_ID(), FALSE)
		IF GET_HEIST_FINALE_STATE() != HEIST_FLOW_NONE
			PRINTLN("[AMEC][HEIST_LAUNCH] - CONTROL_FM_HEIST_PLANNING_SCREEN - Heist finale NOT clean and player NOT in apartment, force cleanup.")
			CLEAN_HEIST_FINALE_ALL(Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
			CLEAN_HEIST_SHARED_DATA()
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF NOT IS_THIS_A_HEIST_PROPERTY(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
		IF GET_HEIST_FINALE_STATE() != HEIST_FLOW_NONE
			PRINTLN("[AMEC][HEIST_LAUNCH] - CONTROL_FM_HEIST_PLANNING_SCREEN - IS_THIS_A_HEIST_PROPERTY() = FALSE, property ID: ", GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty)
			CLEAN_HEIST_FINALE_ALL(Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
			CLEAN_HEIST_SHARED_DATA()
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF GET_HEIST_EXIT_CUTS_FLOW() = HEIST_CUTS_FLOW_STATE_FINISHED
	OR GET_HEIST_EXIT_CUTS_FLOW() = HEIST_CUTS_FLOW_STATE_LOADING
	OR GET_HEIST_EXIT_CUTS_FLOW() = HEIST_CUTS_FLOW_STATE_WAIT_FOR_SCENE
		IF GET_HEIST_FINALE_STATE() != HEIST_FLOW_NONE
			PRINTLN("[AMEC][HEIST_LAUNCH] - CONTROL_FM_HEIST_PLANNING_SCREEN - Heist finale NOT clean and finale cutscene in progress, move to cleanup.")
			CLEAN_HEIST_FINALE_ALL(Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
			CLEAN_HEIST_SHARED_DATA()
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF GET_HEIST_QUICK_RESTART_IN_PROGRESS()
		IF GET_HEIST_FINALE_STATE() != HEIST_FLOW_NONE
			PRINTLN("[AMEC][HEIST_LAUNCH] - CONTROL_FM_HEIST_PLANNING_SCREEN - GET_HEIST_QUICK_RESTART_IN_PROGRESS() = TRUE, do not load finale data.")
			CLEAN_HEIST_FINALE_ALL(Is_Player_Currently_On_MP_Heist(PLAYER_ID()))
			CLEAN_HEIST_SHARED_DATA()
		ENDIF
		RETURN FALSE
	ENDIF
	
	IF GET_HEIST_SUPPRESS_HEIST_BOARD()
		IF GET_HEIST_FINALE_STATE() != HEIST_FLOW_NONE
			PRINTLN("[AMEC][HEIST_LAUNCH] - CONTROL_FM_HEIST_PLANNING_SCREEN - bSupressPlanningBoard = TRUE, do not load finale data.")
			CLEAN_HEIST_FINALE_ALL(TRUE, DEFAULT, TRUE)
			CLEAN_HEIST_SHARED_DATA()
		ENDIF
		RETURN FALSE
	ENDIF

	// FINISHED CHECKING PREREQUISITES. PROCEED TO HEIST PLANNING BOARD.
	IF g_HeistPlanningClient.iHeistLeaderIndex = INVALID_HEIST_DATA
		g_HeistPlanningClient.iHeistLeaderIndex = iHeistLeader
	ENDIF
	
	// Hides the hud when needed
	PROCESS_HEIST_PLANNING_ROOM_HUD()
	
	// bDoLaunch is passed ByRef into the planning state machine. The planning will do it's thing and spit out
	// whether we should launch or not.
	BOOL bDoLaunch = FALSE
	
	#IF IS_DEBUG_BUILD
		DEBUG_ParentWidgets = wgParentWidgetGroup
	
		// For the sake of cleanliness, move the debug inputs to their own procedure.
		MAINTAIN_HEIST_PLANNING_DEBUG_INPUTS(bDoLaunch)
		
		// Check if the planning board has signaled for the launch.
		IF bDoLaunch
			PRINTLN("[AMEC][HEIST_LAUNCH] - CONTROL_FM_HEIST_PLANNING_SCREEN - !! DEBUG !! - bDoLaunch = TRUE, returning TRUE to corona...")
			RETURN TRUE
		ENDIF
	#ENDIF
	
//	IF NOT bAllowHeistConfig
//		PRINTLN("[AMEC][HEIST_LAUNCH] - CONTROL_FM_HEIST_PLANNING_SCREEN - bAllowHeistConfig = FALSE, running in background.")
//	ENDIF

	IF AM_I_LEADER_OF_HEIST(TRUE)
		IF GET_HEIST_START_SENDING_MEMBER_DATA()
			PRINTLN("[AMEC][HEIST_LAUNCH] - CONTROL_FM_HEIST_PLANNING_SCREEN - GET_HEIST_START_SENDING_MEMBER_DATA() = TRUE, maintain member data transfer.")
		 	MAINTAIN_HEIST_MEMBER_DATA_TRANSFER_PROTOCOL()
		ENDIF
	ELSE
		MAINTAIN_REQUEST_FOR_HEIST_MEMBER_DATA()
	ENDIF
	
	IF DOWNLOAD_HEIST_PARTICIPANT_DATA(AM_I_LEADER_OF_HEIST(TRUE), g_HeistSharedClient.iMemberDataLoop, g_HeistSharedClient.iMemberDataInnerLoop)
		PROCESS_RESEND_OF_HEIST_MEDAL_DATA()
	ENDIF
	
	// Heist planning board main maintenance procedure.)
	MANAGE_PLAYER_START_HEIST_PLANNING(coronaMenuData, scaleformStruct, sLaunchMissionDetails, bAllowHeistConfig, bDoLaunch)
	
	// Check if the planning board has signaled for the launch.
	IF bDoLaunch
		PRINTLN("[AMEC][HEIST_LAUNCH] - CONTROL_FM_HEIST_PLANNING_SCREEN - WOOP WOOP! bDoLaunch = TRUE, returning TRUE to corona...")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC




// End of file.
