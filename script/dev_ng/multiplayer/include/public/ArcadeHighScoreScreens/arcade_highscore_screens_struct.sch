//////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// Name:        arcade_high_score_screens.sc																
/// Description: Struct used for running the arcade ineriors highscore screens
/// Written by:  Online Technical Team: Tom Turner,														
/// Date:  		04/11/2019																					
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"
USING "arcade_games_leaderboard.sch"

CONST_INT _ARCADE_HIGHSCORE_SCREENS_TIME_UNTIL_SWAP_MS 10000
CONST_INT _ARCADE_HIGHSCORE_SCREENS_FADE_TIME 2000
CONST_INT _ARCADE_HIGHSCORE_SCREENS_CABINET_COUNT_UPDATE_INTERVAL 360

/// PURPOSE: 
///    States for the arcade highscore screens
ENUM ARCADE_HIGHSCORE_SCREENS_STATE
	AHSS_FADING_OUT,
	AHSS_LOADING,
	AHSS_FADING_IN,
	AHSS_DISPLAYING
ENDENUM

/// PURPOSE:
///    Gets the debug name of the highscore screens state
/// RETURNS:
///    The debug name of the highscore screens state
DEBUGONLY FUNC STRING _DEBUG_GET_ARCADE_HIGHSCORE_SCREENS_STATE_AS_STRING(ARCADE_HIGHSCORE_SCREENS_STATE eEnum)
	SWITCH eEnum
		CASE AHSS_FADING_OUT	RETURN	"AHSS_FADING_OUT"
		CASE AHSS_LOADING		RETURN 	"AHSS_LOADING"
		CASE AHSS_FADING_IN		RETURN	"AHSS_FADING_IN"
		CASE AHSS_DISPLAYING	RETURN	"AHSS_DISPLAYING"
	ENDSWITCH

	ASSERTLN("GET_ARCADE_HIGHSCORE_SCREENS_STATE_AS_STRING - Missing name from lookup: ", ENUM_TO_INT(eEnum))
	RETURN ""
ENDFUNC

/// PURPOSE: 
///    Data used to run the arcade highscore screens
STRUCT ARCADE_HIGHSCORE_SCREENS_STRUCT
	ARCADE_HIGHSCORE_SCREENS_STATE eState = AHSS_LOADING// Start in load state for first leaderboard
	CASINO_ARCADE_GAME eCurrentLeaderboard = CASINO_ARCADE_GAME_DEGENATRON_DEFENDER
	SCRIPT_TIMER stSwapTimer
	TIME_DATATYPE tdFadeTimer
	PLAYER_INDEX piInteriorOwner = NULL
	BOOL bHasRegisteredRenderTarget = FALSE
	BOOL bHasInitializedCount = FALSE
	INT iRenderTargetID = -1
	INT iFadeOpacity = 255// Start faded out
	INT iCabinetCount = 0
ENDSTRUCT
