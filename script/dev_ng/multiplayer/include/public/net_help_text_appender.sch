USING "globals.sch"
USING "net_include.sch"
USING "net_ambience.sch"

CONST_INT MAX_LABELS_PER_MESSAGE 4


/// PURPOSE:
///    Type def for retrieving help texts that needs appending
/// PARAMS:
///    iHelpPrintedBS - the bitset used for checking if the help should display
///    iIndex - the index of the text label to get
/// RETURNS:
///    The text label of the help text if it can be displayed
TYPEDEF FUNC STRING GET_HELP_TO_APPEND(INT& iHelpPrintedBS[], INT iIndex)


/// PURPOSE:
///    Appends the provided help text into the next available help text slot
///    blank text labels will be ignored.
/// PARAMS:
///    iIndex - the index for keeping track of available slots
///    strTextLabelsToAdd - array to add text label to
///    strHelpTextLabel - text label to try and add
/// RETURNS:
///    TRUE if all help slots are filled
FUNC BOOL TRY_APPEND_HELP_TEXT_LABEL(INT& iIndex, STRING& strTextLabelsToAdd[], STRING strHelpTextLabel)
	
	strTextLabelsToAdd[iIndex] = strHelpTextLabel
	
	// Valid label so increment slot number
	IF COMPARE_STRINGS(strTextLabelsToAdd[iIndex], "") != 0
		++iIndex
	ENDIF
	
	IF iIndex >= MAX_LABELS_PER_MESSAGE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE:
///    Gets the correct text label for the amount of help
///    text that needs to be display
/// PARAMS:
///    iHelpLabelCount - how many substrings need to be added
/// RETURNS:
///    Help slot text label
FUNC STRING GET_HELP_STARTING_LABEL(INT iHelpLabelCount)
	
	SWITCH iHelpLabelCount
	CASE 1
		RETURN "AVMH_SLOT_ONE"
	CASE 2
		RETURN "AVMH_SLOT_TWO"
	CASE 3
		RETURN "AVMH_SLOT_THREE"
	CASE 4
		RETURN "AVMH_SLOT_FOUR"	
	ENDSWITCH
	
	RETURN "AVMH_SLOT_ONE"
	
ENDFUNC


/// PURPOSE:
///    Gets how many text labels are queued to be appended
/// PARAMS:
///    strTextLabelsToAdd - the array of appended labels
/// RETURNS:
///    The count of elements that contain a valid text label
FUNC INT GET_APPENDED_HELP_COUNT(STRING& strTextLabelsToAdd[])
	
	INT i
	INT iCount = 0
	
	REPEAT COUNT_OF(strTextLabelsToAdd) i
	
		// If not empty
		IF COMPARE_STRINGS(strTextLabelsToAdd[i], "") != 0
			++iCount
		ENDIF
		
	ENDREPEAT
	
	RETURN iCount
	
ENDFUNC


/// PURPOSE:
///    Adds a list of text labels to a help message
///    empty strings will be ignored
/// PARAMS:
///    iHelpCount - text label count
///    strTextLabelsToAdd - textlabels
PROC PRINT_HELP_STRINGS(INT iHelpCount, STRING& strTextLabelsToAdd[])
	
	// Display help text
	BEGIN_TEXT_COMMAND_DISPLAY_HELP(GET_HELP_STARTING_LABEL(iHelpCount))
	
	INT i
	REPEAT COUNT_OF(strTextLabelsToAdd) i
		
		IF COMPARE_STRINGS(strTextLabelsToAdd[i], "") != 0
		
			// Insert help text as substring
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(strTextLabelsToAdd[i])
		ENDIF
		
	ENDREPEAT
	
	END_TEXT_COMMAND_DISPLAY_HELP(HELP_TEXT_SLOT_STANDARD, FALSE, TRUE)
	
ENDPROC


/// PURPOSE:
///    Appends all vehicle mod help texts to current help print
///    if the vehicle has the mod
/// PARAMS:
///    viVehicle - the vehicle to check mods on
/// RETURNS:
///    TRUE if any help text was appended
FUNC BOOL TRY_APPEND_HELP_TEXT(INT iAppendCount, INT& iHelpDisplayCounts[], GET_HELP_TO_APPEND getHelpToAppend, STRING& strTextLabelsToAdd[])	
	
	INT iLabelsAdded = 0
	INT i = 0
	
	REPEAT iAppendCount i
		IF TRY_APPEND_HELP_TEXT_LABEL(iLabelsAdded, strTextLabelsToAdd, CALL getHelpToAppend(iHelpDisplayCounts, i))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
	
ENDFUNC


/// PURPOSE:
///    Trys to display help text for the mods on this vehicle.
///    Help text will only be shown once every boot
/// PARAMS:
///    viVehicle - the vehicle to check mods on
/// RETURNS:
///    TRUE if displaying mod help text
FUNC BOOL TRY_DISPLAY_APPENDED_HELP_TEXTS(INT iAppendCount, INT& iHelpDisplayCounts[], GET_HELP_TO_APPEND getHelpToAppend)
	
	STRING strTextLabelsToAdd[MAX_LABELS_PER_MESSAGE]
	
	INT i
	REPEAT COUNT_OF(strTextLabelsToAdd) i
		strTextLabelsToAdd[i] = ""
	ENDREPEAT
	
	// Append help texts together
	TRY_APPEND_HELP_TEXT(iAppendCount, iHelpDisplayCounts, getHelpToAppend, strTextLabelsToAdd)
	INT iHelpCount = GET_APPENDED_HELP_COUNT(strTextLabelsToAdd)
	
	// If no help text is appended leave
	IF iHelpCount <= 0
		RETURN FALSE
	ENDIF
	
	// Print all the help strings
	PRINT_HELP_STRINGS(iHelpCount, strTextLabelsToAdd)

	RETURN TRUE	
	
ENDFUNC

