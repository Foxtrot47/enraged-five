///    Author 	: 	Alexander Murphy
///    Team 	: 	Online Tech
///    Info		: 	This file contains functions that handle seating areas.

USING "context_control_public.sch"
USING "website_public.sch"
USING "freemode_header.sch"
USING "script_maths.sch"
USING "arena_box_seat_areas.sch"

USING "arena_spectator_turret_public.sch"
USING "net_arena_wars_career.sch"

#IF NOT DEFINED MAX_NUM_SEATS_FOR_SITTING_ACTIVITY
CONST_INT MAX_NUM_SEATS_FOR_SITTING_ACTIVITY 11
#ENDIF

CONST_INT LOCATE_CHECKS_PER_TICK 1

//Time before a new animation variation is chosen, this should be infrequent.
CONST_INT TIME_BEFORE_ANIM_VAR_CHANGE 90000

//There is a brief period of time while the player isn't allowed to sit down
//that is triggered every time the seat is blocked.
CONST_INT SEAT_BLOCK_TIMER			  		150

CONST_INT CAPSULE_SHRINK_LINGER_NUM_FRAMES	5

TWEAK_FLOAT SITTING_CAPSULE_SIZE 			0.13

ENUM SEAT_TYPE
	SEAT_INVALID
	,SEAT_TYPE_BASE_ANIM
	,SEAT_TYPE_TABLET_ANIM
ENDENUM

ENUM SEAT_STATE
	SS_INIT
	,SS_LOCATE_CHECK
	,SS_PROMPT
	,SS_REQUEST_ANIMATION
	,SS_REQUEST_SEAT
	,SS_MOVE_TO_ANIM_START
	,SS_ANIM_START_ENTER
	,SS_ANIM_SITTING
	,SS_ANIM_INTERUPT
	,SS_CLEANUP
ENDENUM

STRUCT LOCATE_STRUCT
	VECTOR v0
	VECTOR v1
	FLOAT fWidth = 0.75
ENDSTRUCT

STRUCT ANIM_STRUCT
	VECTOR vPos
	VECTOR vRot
ENDSTRUCT

//Randomly selected when player sits down. Specific selection for various chairs.
ENUM SEAT_ANIM_0
	ANIM_AMB_FACILITY_BRIEFING_ROOM_SEATING,
	ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
ENDENUM

//Randomly selected when player sits down, changes very infreuqently.
ENUM SEAT_ANIM_1
	/* Offfice, Clubhouse, and briefing room variations */
	VAR_A
	,VAR_B
	,VAR_C
	,VAR_D
	,VAR_E
ENDENUM

//Clip is Enter, then ping pongs between Base <-> Idle/Transition then Exit
ENUM SEAT_ANIM_2_CLIP
	/* Clips */
	IDLE_A
	,IDLE_B
	,IDLE_C
	,ENTER
	,LEAVE
	,BASE
	/* This is not a clip. Use as an offset for the transitions below. */
	,TRANSITIONS
	,A_TO_B
	,B_TO_C
	,C_TO_D
	,D_TO_E
	/* This is not a clip. Use as an offset for the transitions below. */
	,TRANSITION_LOOPS
	,B_TO_A	//Unused, but here to maintain consistency in offsets.. Please do not remove.
	,C_TO_A
	,D_TO_A
	,E_TO_A
ENDENUM

STRUCT ANIM_STATE_STRUCT
	SEAT_ANIM_0 eDictBase
	SEAT_ANIM_1 eDictVar
	SEAT_ANIM_2_CLIP eClip
	SEAT_ANIM_2_CLIP ePrevClip
	INT iVarChangeTime
ENDSTRUCT

STRUCT SEAT_DATA_STRUCT
	SEAT_TYPE eType
	LOCATE_STRUCT entryLocate
	ANIM_STRUCT animation
ENDSTRUCT

TYPEDEF FUNC BOOL ADDITIONAL_CHECK_BEFORE_PROMPT(SEAT_DATA_STRUCT& in_data, INT iSeatIndex)

TYPEDEF FUNC BOOL USE_ALTERNATIVE_HELP_TEXT(SEAT_DATA_STRUCT& in_data, INT iSeatIndex, TEXT_LABEL_15& out_txtHelp)

STRUCT SEATS_PRIVATE_DATA
	INT iBSGeneral = 0
	INT iNetSceneID = -1
	INT iInSeatLocate = 0
	INT iPlayerToCheck = 0
	INT iObjectStagger = 0
	SEAT_STATE eState = SS_INIT
	INT iContext = NEW_CONTEXT_INTENTION
	INT iSeatBlockedTimer = -1
	INT iFrameLastCapsuleChange =-1
 	ANIM_STATE_STRUCT animationState
	ADDITIONAL_CHECK_BEFORE_PROMPT cCanPlayerUseSeat
	USE_ALTERNATIVE_HELP_TEXT cAltHelpEnter
	USE_ALTERNATIVE_HELP_TEXT cAltHelpExit
	TURRET_LOCK_REQUEST_ID eTurretRequestID
	OBJECT_INDEX objTablet
ENDSTRUCT

#IF IS_DEBUG_BUILD
STRUCT SEATS_DEBUG_DATA
	INT iNextForcedAnimVar
	BOOL bForceAnimVarNow
ENDSTRUCT
#ENDIF

STRUCT SEATS_PUBLIC_DATA
	SEAT_DATA_STRUCT seats[MAX_NUM_SEATS_FOR_SITTING_ACTIVITY]
ENDSTRUCT

STRUCT SEATS_LOCAL_DATA
	SEATS_PUBLIC_DATA public
	SEATS_PRIVATE_DATA private
	
	#IF IS_DEBUG_BUILD
	SEATS_DEBUG_DATA debug
	#ENDIF
ENDSTRUCT

STRUCT SEAT_SERVER_BROADCAST_DATA
	BOOL bSeatsInitialised = FALSE
	
	INT iBS = 0
	INT iPlayerSeats[10]
	INT iPlayerSeatsBS = 0
	
	NETWORK_INDEX niTablet[10]
ENDSTRUCT

STRUCT SEAT_PLAYER_BROADCAST_DATA
	INT iBS
	INT iSeatBS
ENDSTRUCT

ENUM PRIVATE_SEAT_GENERAL_BS
	PSGB_BUSY_ON_PHONE
	,PSGB_BUSY_DEAD
	,PSGB_BUSY_RUNNING
	,PSGB_BUSY_IN_VEHICLE
	,PSGB_BUSY_BROWSER_OPEN
	,PSGB_ADDITIONAL_CHECK_BEFORE_PROMPT
	,PSGB_ALT_HELP_ENTER
	,PSGB_ALT_HELP_EXIT
	,PSGB_WAITING_FOR_NEXT_DICTIONARY
	,PSGB_BLOCK_EXIT_THIS_FRAME
	,PSGB_GET_OUT_OF_SEAT
	,PSGB_ENTRY_COMPLETE
	,PSGB_BLOCK_MANUAL_VAR_CHANGE
	,PSGB_LAUNCH_TURRET
	,PSGB_SPAWNING_IN_SEAT
	,PSGB_PC_INPUT_MAP_LOADED
ENDENUM

/*PSGB_BUSY_ON_PHONE || PSGB_BUSY_DEAD || PSGB_BUSY_RUNNING || PSGB_BUSY_IN_VEHICLE || PSGB_BUSY_BROWSER_OPEN*/
CONST_INT BUSY_MASK  1 + 2 + 4 + 8 + 16

PROC LOG_MAX_NUM_SEATS()
	PRINTLN("{OCH} [ARENA_BOX_SEATS] : MAX_NUM_SEATS_FOR_SITTING_ACTIVITY = ", MAX_NUM_SEATS_FOR_SITTING_ACTIVITY)
ENDPROC

PROC SET_BIT_VALUE_ENUM(INT& iBS, ENUM_TO_INT eBit, BOOL bValue)
	INT iBit = eBit
	
	IF bValue
		SET_BIT(iBS, iBit)
	ELSE
		CLEAR_BIT(iBS, iBit)
	ENDIF
ENDPROC

FUNC BOOL IS_ENTITY_IN_SEAT_ENTRY_LOCATE(ENTITY_INDEX entity, SEAT_DATA_STRUCT& seat)
	RETURN IS_ENTITY_IN_ANGLED_AREA(entity, seat.entryLocate.v0, seat.entryLocate.v1, seat.entryLocate.fWidth)
ENDFUNC

FUNC BOOL IS_ENTITY_FACING_SEAT(ENTITY_INDEX entity, VECTOR vRotation)
	IF DOES_ENTITY_EXIST(entity)
		IF IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(entity), vRotation.z - 180.0, 90.0)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL STAGGERED_UPDATE_LOCATE_CHECK(SEATS_LOCAL_DATA& data)
	INT i
	REPEAT LOCATE_CHECKS_PER_TICK i
		IF IS_ENTITY_IN_SEAT_ENTRY_LOCATE(PLAYER_PED_ID(), data.public.seats[data.private.iInSeatLocate])
		AND IS_ENTITY_FACING_SEAT(PLAYER_PED_ID(), data.public.seats[data.private.iInSeatLocate].animation.vRot)
			RETURN TRUE
		ELSE
			data.private.iInSeatLocate = (data.private.iInSeatLocate + 1) % MAX_NUM_SEATS_FOR_SITTING_ACTIVITY
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC SEAT_ANIM_0 RANDOM_ANIM_S0(SEAT_TYPE eType)
	SWITCH eType
		CASE SEAT_TYPE_BASE_ANIM		RETURN ANIM_AMB_FACILITY_BRIEFING_ROOM_SEATING
		CASE SEAT_TYPE_TABLET_ANIM		RETURN ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
	ENDSWITCH
	
	RETURN ANIM_AMB_FACILITY_BRIEFING_ROOM_SEATING
ENDFUNC

FUNC INT GET_ANIM_S1_VARIATIONS(SEAT_ANIM_0 eBase)
	SWITCH eBase
		CASE ANIM_AMB_FACILITY_BRIEFING_ROOM_SEATING 	RETURN 5
	ENDSWITCH
	
	RETURN 0
ENDFUNC

FUNC VECTOR GET_ANIM_ROT(SEATS_LOCAL_DATA& data)
	RETURN data.public.seats[data.private.iInSeatLocate].animation.vRot
ENDFUNC

FUNC VECTOR GET_ANIM_POS(SEATS_LOCAL_DATA& data)
	RETURN data.public.seats[data.private.iInSeatLocate].animation.vPos
ENDFUNC

FUNC SEAT_ANIM_1 RANDOM_ANIM_S1(SEATS_LOCAL_DATA& data)
	IF data.private.animationState.eDictBase = ANIM_AMB_FACILITY_BRIEFING_ROOM_SEATING
		INT iRand
		
		IF IS_PLAYER_FEMALE()
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			
			SWITCH iRand
				CASE 0	RETURN VAR_A
				CASE 1	RETURN VAR_D
				CASE 2	RETURN VAR_E
			ENDSWITCH
		ELSE
			iRand = GET_RANDOM_INT_IN_RANGE(0, 3)
			
			SWITCH iRand
				CASE 0	RETURN VAR_A
				CASE 1	RETURN VAR_C
				CASE 2	RETURN VAR_D
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN VAR_A
ENDFUNC

PROC SET_START_ANIMATION(SEATS_LOCAL_DATA& data)
	data.private.animationState.eDictBase = RANDOM_ANIM_S0(data.public.seats[data.private.iInSeatLocate].eType)
	data.private.animationState.eDictVar = RANDOM_ANIM_S1(data)
	data.private.animationState.eClip = ENTER
	data.private.animationState.ePrevClip = ENTER
	
	PRINTLN("[ARENA_BOX_SEATS][ANIM] Start base : ", ENUM_TO_INT(data.private.animationState.eDictBase))
	PRINTLN("[ARENA_BOX_SEATS][ANIM] Start var : ", ENUM_TO_INT(data.private.animationState.eDictVar))
	PRINTLN("[ARENA_BOX_SEATS][ANIM] Start clip : ", ENUM_TO_INT(data.private.animationState.eClip))
ENDPROC

PROC CONSTRUCT_ANIM_CLIP(SEATS_LOCAL_DATA& data, TEXT_LABEL_15& sClip, BOOL bTablet = FALSE)
	SWITCH data.private.animationState.eDictBase
		CASE ANIM_AMB_FACILITY_BRIEFING_ROOM_SEATING
			SWITCH data.private.animationState.eClip
				CASE IDLE_A		sClip = "IDLE_A"		BREAK
				CASE IDLE_B		sClip = "IDLE_B"		BREAK
				CASE IDLE_C		sClip = "IDLE_C"		BREAK
				CASE ENTER		sClip = "ENTER"	 		BREAK
				CASE BASE		sClip = "BASE"			BREAK
				CASE LEAVE		sClip = "EXIT"			BREAK
				CASE A_TO_B		sClip = "A_TO_B"		BREAK
				CASE B_TO_C		sClip = "B_TO_C"		BREAK
				CASE C_TO_D		sClip = "C_TO_D"		BREAK
				CASE D_TO_E		sClip = "D_TO_E"		BREAK
				CASE B_TO_A		sClip = "B_TO_E"		BREAK
				CASE C_TO_A		sClip = "C_TO_A"		BREAK
				CASE D_TO_A		sClip = "D_TO_A"		BREAK
				CASE E_TO_A		sClip = "E_TO_A"		BREAK
				DEFAULT 		sClip = "invalid_clip"	BREAK
			ENDSWITCH
		BREAK
		
		CASE ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
			SWITCH data.private.animationState.eClip
				CASE IDLE_A		sClip = "IDLE_A"		BREAK
				CASE IDLE_B		sClip = "IDLE_B"		BREAK
				CASE IDLE_C		sClip = "IDLE_C"		BREAK
				CASE ENTER		sClip = "ENTER"	 		BREAK
				CASE BASE		sClip = "BASE"			BREAK
				CASE LEAVE		sClip = "EXIT"			BREAK
				CASE A_TO_B		sClip = "A_TO_B"		BREAK
				CASE B_TO_C		sClip = "B_TO_C"		BREAK
				CASE C_TO_D		sClip = "C_TO_D"		BREAK
				CASE D_TO_E		sClip = "D_TO_E"		BREAK
				CASE B_TO_A		sClip = "B_TO_E"		BREAK
				CASE C_TO_A		sClip = "C_TO_A"		BREAK
				CASE D_TO_A		sClip = "D_TO_A"		BREAK
				CASE E_TO_A		sClip = "E_TO_A"		BREAK
				DEFAULT 		sClip = "invalid_clip"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	IF bTablet
		sClip += "_TABLET"
	ENDIF
	
	PRINTLN("[ARENA_BOX_SEATS] Constructed seat anim clip (", ENUM_TO_INT(data.private.animationState.eClip),"): ", sClip)
ENDPROC

FUNC STRING GET_TABLET_CREATION_DICT()
	IF IS_PLAYER_FEMALE()
		RETURN "anim@arena@amb@seat_drone_tablet@female@"
	ENDIF
	
	RETURN "anim@arena@amb@seat_drone_tablet@male@"
ENDFUNC

PROC CONSTRUCT_ANIM_DICT(SEATS_LOCAL_DATA& data, TEXT_LABEL_63& sDict)
	SWITCH data.private.animationState.eDictBase
		CASE ANIM_AMB_FACILITY_BRIEFING_ROOM_SEATING 	sDict = "ANIM@AMB@FACILITY@BRIEFING_ROOM@SEATING" 	BREAK
		CASE ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING	sDict = "ANIM@ARENA@AMB@SEAT_DRONE_TABLET"			BREAK
	ENDSWITCH
	
	IF IS_PLAYER_FEMALE()
		sDict += "@FEMALE"
	ELSE
		sDict += "@MALE"
	ENDIF
	
	SWITCH data.private.animationState.eDictBase
		CASE ANIM_AMB_FACILITY_BRIEFING_ROOM_SEATING
			SWITCH data.private.animationState.eDictVar
				CASE VAR_A 		sDict += "@VAR_A@"	BREAK
				CASE VAR_B		sDict += "@VAR_B@"	BREAK
				CASE VAR_C		sDict += "@VAR_C@"	BREAK
				CASE VAR_D		sDict += "@VAR_D@"	BREAK
				CASE VAR_E		sDict += "@VAR_E@"	BREAK
			ENDSWITCH
		BREAK
		
		CASE ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
			SWITCH data.private.animationState.eDictVar
				CASE VAR_A 		sDict += "@"	BREAK
			ENDSWITCH
		BREAK
	ENDSWITCH
	
	PRINTLN("[ARENA_BOX_SEATS] Constructed seat anim dict (", ENUM_TO_INT(data.private.animationState.eDictBase)," + ",ENUM_TO_INT(data.private.animationState.eDictVar) ,"): ", sDict)
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_SEAT_STATE_STRING(SEAT_STATE state)
	SWITCH state
		CASE SS_INIT 				RETURN "SS_INIT"
		CASE SS_LOCATE_CHECK 		RETURN "SS_LOCATE_CHECK"
		CASE SS_PROMPT 				RETURN "SS_PROMPT"
		CASE SS_ANIM_INTERUPT 		RETURN "SS_ANIM_INTERUPT"
		CASE SS_ANIM_SITTING 		RETURN "SS_ANIM_SITTING"
		CASE SS_ANIM_START_ENTER 	RETURN "SS_ANIM_START_ENTER"
		CASE SS_CLEANUP				RETURN "SS_CLEANUP"
		CASE SS_MOVE_TO_ANIM_START	RETURN "SS_MOVE_TO_ANIM_START"
		CASE SS_REQUEST_ANIMATION	RETURN "SS_REQUEST_ANIMATION"
		CASE SS_REQUEST_SEAT		RETURN "SS_REQUEST_SEAT"
	ENDSWITCH
	
	RETURN "INVALID_STATE"
ENDFUNC

PROC PRINT_BUSY_REASON(SEATS_LOCAL_DATA& data)
	IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BUSY_BROWSER_OPEN)
		PRINTLN("[ARENA_BOX_SEATS] PRINT_BUSY_REASON : Player has browswer open.")
	ENDIF
	
	IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BUSY_DEAD)
		PRINTLN("[ARENA_BOX_SEATS] PRINT_BUSY_REASON : Player is dead.")
	ENDIF
	
	IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BUSY_IN_VEHICLE)
		PRINTLN("[ARENA_BOX_SEATS] PRINT_BUSY_REASON : Player is in a vehicle.")
	ENDIF
	
	IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BUSY_ON_PHONE)
		PRINTLN("[ARENA_BOX_SEATS] PRINT_BUSY_REASON : Player has phone open.")
	ENDIF
	
	IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BUSY_RUNNING)
		PRINTLN("[ARENA_BOX_SEATS] PRINT_BUSY_REASON : Player is running.")
	ENDIF
ENDPROC
#ENDIF

FUNC FLOAT GET_PED_CAPSULE_SIZE()
	RETURN SITTING_CAPSULE_SIZE
ENDFUNC

PROC RUN_IGNORE_COLLISION_FOR_SIMPLE_POP_LOGIC(SEATS_LOCAL_DATA& data)
	IF data.private.eState = SS_ANIM_SITTING
		// Do nothing
	ELIF data.private.eState > SS_MOVE_TO_ANIM_START
		SET_GAMEPLAY_CAM_IGNORE_ENTITY_COLLISION_THIS_UPDATE(PLAYER_PED_ID())
		SET_PED_CAPSULE(PLAYER_PED_ID(), GET_PED_CAPSULE_SIZE())
		data.private.iFrameLastCapsuleChange = GET_FRAME_COUNT()
	//Capsule shrink linger so that it doesn't pop the player out during
	//transition from cleanup->locate check and when the player becomes
	//busy during the locate check state.
	ELIF GET_FRAME_COUNT() - data.private.iFrameLastCapsuleChange < CAPSULE_SHRINK_LINGER_NUM_FRAMES
		SET_PED_CAPSULE(PLAYER_PED_ID(), GET_PED_CAPSULE_SIZE())
	ENDIF
ENDPROC

PROC SET_SEAT_STATE(SEATS_LOCAL_DATA& data, SEAT_STATE state)
	data.private.eState = state
	PRINTLN("[ARENA_BOX_SEATS] SET_SEAT_STATE : Setting state to ", GET_SEAT_STATE_STRING(state))
ENDPROC

PROC UPDATE_ENTITY_CHECKS(SEATS_LOCAL_DATA& data)
	SET_BIT_VALUE_ENUM(data.private.iBSGeneral, PSGB_BUSY_BROWSER_OPEN, IS_BROWSER_OPEN())
	SET_BIT_VALUE_ENUM(data.private.iBSGeneral, PSGB_BUSY_DEAD, IS_ENTITY_DEAD(PLAYER_PED_ID()))
	SET_BIT_VALUE_ENUM(data.private.iBSGeneral, PSGB_BUSY_IN_VEHICLE, IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE))
	SET_BIT_VALUE_ENUM(data.private.iBSGeneral, PSGB_BUSY_ON_PHONE, IS_PHONE_ONSCREEN())
	SET_BIT_VALUE_ENUM(data.private.iBSGeneral, PSGB_BUSY_RUNNING, IS_PED_RUNNING(PLAYER_PED_ID()))
ENDPROC

FUNC BOOL ARE_THERE_TOO_MANY_PLAYERS_CLOSE_TO_SIMPLE_ACTIVITY_ANGLED_AREA(SEATS_LOCAL_DATA& data)
	INT iParticipant
	REPEAT NETWORK_GET_MAX_NUM_PARTICIPANTS() iParticipant
		IF NETWORK_IS_PARTICIPANT_ACTIVE(INT_TO_PARTICIPANTINDEX(iParticipant))
			PLAYER_INDEX playerID = NETWORK_GET_PLAYER_INDEX(INT_TO_PARTICIPANTINDEX(iParticipant))
			
			IF playerID <> PLAYER_ID()
			AND IS_NET_PLAYER_OK(playerID)
				IF IS_ENTITY_IN_SEAT_ENTRY_LOCATE(GET_PLAYER_PED(playerID), data.public.seats[data.private.iInSeatLocate])
				OR GET_PLAYER_USING_OFFICE_SEATID(playerID) = data.private.iInSeatLocate
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_BUSY(SEATS_LOCAL_DATA& data)
	IF IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Drone)
	OR IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_RC_Car)
	OR IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Trap_Cam)
	OR IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Turret_Cam)
		RETURN TRUE
	ENDIF
	
	RETURN (data.private.iBSGeneral & BUSY_MASK) > 0
ENDFUNC

FUNC INT GET_NEXT_VAR_FOR_SEAT(SEATS_LOCAL_DATA& data)
	RETURN ENUM_TO_INT(data.private.animationState.eDictVar)
ENDFUNC

PROC GENERATE_AND_STORE_NEXT_ANIM(SEATS_LOCAL_DATA& data, BOOL bForceVarChange = FALSE)
	INT iNewClip
	INT iNewVar
	
	#IF IS_DEBUG_BUILD
	IF data.debug.bForceAnimVarNow
		iNewClip = ENUM_TO_INT(BASE)
		iNewVar = data.debug.iNextForcedAnimVar
		
		PRINTLN("[ARENA_BOX_SEATS][ANIM] New var : ", iNewVar)
		PRINTLN("[ARENA_BOX_SEATS][ANIM] New clip : ", iNewClip)
		
		data.private.animationState.ePrevClip = data.private.animationState.eClip
		data.private.animationState.eClip = INT_TO_ENUM(SEAT_ANIM_2_CLIP, iNewClip)
		data.private.animationState.eDictVar = INT_TO_ENUM(SEAT_ANIM_1, iNewVar)
		EXIT
	ENDIF
	#ENDIF
	
	IF bForceVarChange
		iNewVar = GET_NEXT_VAR_FOR_SEAT(data)
		
		//Only set a transition if we actually changed var (Some seats cannot handle more than one var)
		IF iNewVar <> ENUM_TO_INT(data.private.animationState.eDictVar)
			IF iNewVar = 0
				iNewClip = ENUM_TO_INT(TRANSITION_LOOPS) + ENUM_TO_INT(data.private.animationState.eDictVar)
			ELSE
				iNewClip = ENUM_TO_INT(TRANSITIONS) + iNewVar
			ENDIF
		ELSE
			iNewClip = ENUM_TO_INT(data.private.animationState.eClip)
		ENDIF
		
		//Set it back because the transition forward is tied to the PREVIOUS DictVar.
		//Next time this function is called, this will change.
		iNewVar =  ENUM_TO_INT(data.private.animationState.eDictVar)
	ELSE
		//Should also switch between variants after a while...
		IF data.private.animationState.eClip = BASE
			iNewVar = ENUM_TO_INT(data.private.animationState.eDictVar)
			
			// idle_a, idle_b, idle_c = 3 variations
			INT iNumIdleVars = 3 
			
			iNewClip = GET_RANDOM_INT_IN_RANGE(0, iNumIdleVars)
			
			IF iNewClip = ENUM_TO_INT(data.private.animationState.ePrevClip)
				iNewClip = (iNewClip + 1) % iNumIdleVars
			ENDIF
		ELSE
			//If we just just used a transition anim to get to this Var then we now need to update the Var from previous
			IF data.private.animationState.eClip > TRANSITIONS
				iNewVar = GET_NEXT_VAR_FOR_SEAT(data)
				
				//Start the timer because we've completed the transition
				data.private.animationState.iVarChangeTime = GET_GAME_TIMER()
			ELSE
				//Otherwise keep it as is
				iNewVar = ENUM_TO_INT(data.private.animationState.eDictVar)
			ENDIF
			
			//Should change to a new variation of the base anim?
			IF GET_GAME_TIMER() - data.private.animationState.iVarChangeTime >= TIME_BEFORE_ANIM_VAR_CHANGE
				iNewVar = GET_NEXT_VAR_FOR_SEAT(data)
				
				//Only set a transition if we actually changed var (Some seats cannot handle more than one var)
				IF iNewVar <> ENUM_TO_INT(data.private.animationState.eDictVar)
					IF iNewVar = 0
						iNewClip = ENUM_TO_INT(TRANSITION_LOOPS) + ENUM_TO_INT(data.private.animationState.eDictVar)
					ELSE
						iNewClip = ENUM_TO_INT(TRANSITIONS) + iNewVar
					ENDIF
				ENDIF
				
				//Set it back because the transition forward is tied to the PREVIOUS DictVar.
				//Next time this function is called, this will change.
				iNewVar =  ENUM_TO_INT(data.private.animationState.eDictVar)
			ELSE
				iNewClip = ENUM_TO_INT(BASE)
			ENDIF
		ENDIF
	ENDIF
	
	PRINTLN("[ARENA_BOX_SEATS][ANIM] New var : ", iNewVar)
	PRINTLN("[ARENA_BOX_SEATS][ANIM] New clip : ", iNewClip)
	
	data.private.animationState.ePrevClip = data.private.animationState.eClip
	data.private.animationState.eClip = INT_TO_ENUM(SEAT_ANIM_2_CLIP, iNewClip)
	data.private.animationState.eDictVar = INT_TO_ENUM(SEAT_ANIM_1, iNewVar)
ENDPROC	

PROC MAINTAIN_SUPRESS_PHONE_AND_ACTIONS(SEATS_LOCAL_DATA& data)
	WEAPON_TYPE wtWeapon
	
	IF IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_RC_Car)
		DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
	ENDIF
	
	SWITCH data.private.eState
		CASE SS_MOVE_TO_ANIM_START
		CASE SS_CLEANUP
			DISABLE_CELLPHONE_THIS_FRAME_ONLY(TRUE)
		FALLTHRU
		
		CASE SS_ANIM_SITTING
			INVALIDATE_IDLE_CAM()
			HUD_FORCE_WEAPON_WHEEL(FALSE)
			DISPLAY_AMMO_THIS_FRAME(FALSE)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			HUD_SUPPRESS_WEAPON_WHEEL_RESULTS_THIS_FRAME()
			
			IF NOT IS_DRONE_SNAPMATIC_ACTIVE()
				DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
			ENDIF
			
			IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),wtWeapon)
			AND wtWeapon != WEAPONTYPE_UNARMED
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			ENDIF
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			SET_CLEAR_ON_CALL_HUD_THIS_FRAME()
			DISABLE_SELECTOR_THIS_FRAME()
		BREAK
	ENDSWITCH
ENDPROC

PROC LEAVE_SEAT(SEATS_LOCAL_DATA& data)
	data.private.iNetSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ANIM_POS(data), GET_ANIM_ROT(data), DEFAULT, TRUE)
	
	TEXT_LABEL_63 sDict
	
	CONSTRUCT_ANIM_DICT(data, sDict)
	
	NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), data.private.iNetSceneID, sDict, "exit", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE, RBF_PLAYER_IMPACT)
	
	NETWORK_START_SYNCHRONISED_SCENE(data.private.iNetSceneID)
	
	RELEASE_CONTEXT_INTENTION(data.private.iContext)
	
	SET_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_STANDING_UP)
	
	CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_SPAWNING_IN_SEAT)
	
	PRINTLN("[ARENA_BOX_SEATS] LEAVE_SEAT : Leaving seat")	
	SET_SEAT_STATE(data, SS_CLEANUP)
ENDPROC

PROC MAINTAIN_EXIT_INTERRUPT(SEATS_LOCAL_DATA& data)
	IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("EXIT_INTERRUPT"))
	AND data.private.eState = SS_CLEANUP
		PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_EXIT_INTERRUPT : HAS_ANIM_EVENT_FIRED(EXIT_INTERRUPT) : TRUE")
		
		VECTOR vLeftInput = <<GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_X), GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y), 0>>
		VECTOR vRightInput = <<GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X), GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y), 0>>
		
		IF VMAG(vLeftInput) >= 0.24
		OR VMAG(vRightInput) >= 0.24
			PRINTLN("[ARENA_BOX_SEATS] Input recieved, interupting seat anim.")
			
			SET_SEAT_STATE(data, SS_ANIM_INTERUPT)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_SHOWING_MODE_SPECIFIC_HELP_TEXT()
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARENA_SEAT_GM0")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARENA_SEAT_GM1")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TAGTM_HELP_2O")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TAGTM_HELP_2P")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TAGTM_HELP_2Pi")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("TAGTM_HELP_2G")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ARENA_SEAT_HELP_DISPLAYING()
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGameMastersMode)
	AND NOT IS_BIT_SET(g_iSpecialSpectatorBS2, g_ciSpecial_Spectator_BS2_Print_Help_For_Game_Masters_Finished)
	AND NOT IS_BIT_SET(g_iSpecialSpectatorBS2, g_ciSpecial_Spectator_BS2_Seat_Abilities_Blocked_Show_Context)
		RETURN FALSE
	ENDIF
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team)
	AND NOT IS_BIT_SET(g_iSpecialSpectatorBS2, g_ciSpecial_Spectator_BS2_Print_Help_For_Tag_Team_Finished)
	AND NOT IS_BIT_SET(g_iSpecialSpectatorBS2, g_ciSpecial_Spectator_BS2_Seat_Abilities_Blocked_Show_Context)
		RETURN FALSE
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOFSEAT_PCEXIT")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("MPOFSEAT_EXIT")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARENA_SEAT")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARENA_SEAT_PC")
	OR IS_THIS_HELP_MESSAGE_WITH_TWO_NUMBERS_BEING_DISPLAYED("ARENA_SEAT1", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST, g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
	OR IS_THIS_HELP_MESSAGE_WITH_TWO_NUMBERS_BEING_DISPLAYED("ARENA_SEAT_PC1", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST, g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ARENA_SEAT2", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ARENA_SEAT_PC2", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ARENA_SEAT3", g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
	OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ARENA_SEAT_PC3", g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARENA_SEAT4")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARENA_SEAT_PC4")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARENA_SEAT_EX")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC CLEAR_SEATS_HELP_TEXT(SEATS_LOCAL_DATA& data, BOOL bReleaseContextIntention)
	IF IS_ARENA_SEAT_HELP_DISPLAYING()
		CLEAR_HELP()
	ENDIF
	
	IF bReleaseContextIntention
	AND data.private.iContext != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(data.private.iContext)
		data.private.iContext = NEW_CONTEXT_INTENTION
	ENDIF
ENDPROC

FUNC BOOL IS_EXIT_BLOCKED_THIS_FRAME(SEATS_LOCAL_DATA& data)
	RETURN IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BLOCK_EXIT_THIS_FRAME)
ENDFUNC

FUNC BOOL ARE_TABLET_WEAPONS_ACTIVE()
	IF CONTENT_IS_USING_ARENA()
	AND NOT IS_THIS_MISSION_ROCKSTAR_CREATED()
		RETURN FALSE
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Spectator_Disable_Reward_Wheel)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL ARE_TABLET_WEAPONS_BLOCKED()

	IF IS_USING_ACTIVE_ROAMING_SPECTATOR_MODE()
	AND NOT IS_INSTANCED_CONTENT_COUNTDOWN_FINISHED()
		RETURN TRUE
	ENDIF

	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Turn_Rotor_Tag_Team)
	OR IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_EnableGameMastersMode)
		IF DID_I_JOIN_MISSION_AS_SPECTATOR()
			RETURN TRUE
		ENDIF
	ENDIF
	
	INT iTeam
	INT iRule
	
	iTeam = GET_PLAYER_TEAM(PLAYER_ID())
	
	IF iTeam > FMMC_MAX_TEAMS
	OR iTeam < 0
		iTeam = 0
	ENDIF
	
	iRule = gBG_MC_serverBD_VARS.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_ROAM_SPEC_DISABLE_DRONE)
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_ROAM_SPEC_DISABLE_RCCAR)
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_ROAM_SPEC_DISABLE_TRAPCAM)
		AND IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_ROAM_SPEC_DISABLE_TURRETCAM)
		AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Spectator_Disable_Reward_Wheel)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC ARENA_SEATS_INIT_PC_SCRIPTED_CONTROLS(STRING sSchemeName)
	DEBUG_PRINTCALLSTACK()
	CDEBUG3LN(DEBUG_NET_TURRET, "[ARENA_BOX_CAM] PC CONTROLS Load ", sSchemeName)
	INIT_PC_SCRIPTED_CONTROLS(sSchemeName)
ENDPROC

PROC ARENA_SEATS_SHUTDOWN_PC_SCRIPTED_CONTROLS()
	DEBUG_PRINTCALLSTACK()
	CDEBUG3LN(DEBUG_NET_TURRET, "[ARENA_BOX_CAM] PC CONTROLS Shutdown")
	SHUTDOWN_PC_SCRIPTED_CONTROLS()
ENDPROC

PROC CLEANUP_SEATS_ACTIVITY(SEATS_LOCAL_DATA& data, SEAT_SERVER_BROADCAST_DATA& serverBD)
	IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_PC_INPUT_MAP_LOADED)
		ARENA_SEATS_SHUTDOWN_PC_SCRIPTED_CONTROLS()
		
		CLEAR_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_LOADED_CONTROLS)
		
		CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_PC_INPUT_MAP_LOADED)
	ENDIF
	
	RELEASE_CONTEXT_INTENTION(data.private.iContext)
	
	CLEAR_SEATS_HELP_TEXT(data, TRUE)
	
	IF data.private.eState > SS_PROMPT
		IF data.private.iInSeatLocate >= 0
		AND data.private.iInSeatLocate < 10
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
			AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
				IF IS_ENTITY_ATTACHED_TO_ENTITY(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), PLAYER_PED_ID())
					DETACH_ENTITY(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), FALSE)
					
					DELETE_NET_ID(serverBD.niTablet[data.private.iInSeatLocate])
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_RC_Car)
		AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Using_RC_CAR)
		AND NOT IS_PLAYER_USING_RCBANDITO(PLAYER_ID())
			SET_PLAYER_USING_OFFICE_SEATID(-1)
			SET_PLAYER_USING_ARENA_BOX_SEATID(-1)
		ENDIF
		
		g_bPlayerFullySeated = FALSE
		
		CLEAR_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_STANDING_UP)
		
		PRINTLN("[ARENA_BOX_SEATS] CLEANUP_SEATS_ACTIVITY : seat in use, clearing player seat ID.")
		
		INT localScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(data.private.iNetSceneID)
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(localScene)
			NETWORK_STOP_SYNCHRONISED_SCENE(data.private.iNetSceneID)
		ENDIF
		
		IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
		AND NOT IS_NEW_LOAD_SCENE_ACTIVE()
		AND IS_SKYSWOOP_AT_GROUND()
			NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		
		ENABLE_INTERACTION_MENU()
	ENDIF
	
	PRINTLN("[ARENA_BOX_SEATS] CLEANUP_SEATS_ACTIVITY : Complete.")
ENDPROC

FUNC BOOL IS_TABLET_TURRET_CAM_ENABLED()
	BOOL bDisabledByRule = FALSE
	
	INT iTeam
	INT iRule
	
	iTeam = GET_PLAYER_TEAM(PLAYER_ID())
	
	IF iTeam > FMMC_MAX_TEAMS
	OR iTeam < 0
		iTeam = 0
	ENDIF
	
	iRule = gBG_MC_serverBD_VARS.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_ROAM_SPEC_DISABLE_TURRETCAM)
			bDisabledByRule = TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Enable_Spectator_TurretCam_Reward)
	AND NOT bDisabledByRule
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TABLET_TRAP_CAM_ENABLED()
	IF NOT IS_TRAP_CAM_AVAILABLE()
		RETURN FALSE
	ENDIF
	
	BOOL bDisabledByRule = FALSE
	
	INT iTeam
	INT iRule
	
	iTeam = GET_PLAYER_TEAM(PLAYER_ID())
	
	IF iTeam > FMMC_MAX_TEAMS
	OR iTeam < 0
		iTeam = 0
	ENDIF
	
	iRule = gBG_MC_serverBD_VARS.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_ROAM_SPEC_DISABLE_TRAPCAM)
			bDisabledByRule = TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwentyTwo, ciOptionsBS22_Enable_Spectator_TrapCam_Reward)
	AND NOT bDisabledByRule
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TABLET_RC_CAR_ENABLED()
	BOOL bDisabledByRule = FALSE
	
	INT iTeam
	INT iRule
	
	iTeam = GET_PLAYER_TEAM(PLAYER_ID())
	
	IF iTeam > FMMC_MAX_TEAMS
	OR iTeam < 0
		iTeam = 0
	ENDIF
	
	iRule = gBG_MC_serverBD_VARS.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_ROAM_SPEC_DISABLE_RCCAR)
			bDisabledByRule = TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Spectator_RCCar_Reward)
	AND NOT bDisabledByRule
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_TABLET_DRONE_ENABLED()
	BOOL bDisabledByRule = FALSE
	
	INT iTeam
	INT iRule
	
	iTeam = GET_PLAYER_TEAM(PLAYER_ID())
	
	IF iTeam > FMMC_MAX_TEAMS
	OR iTeam < 0
		iTeam = 0
	ENDIF
	
	iRule = gBG_MC_serverBD_VARS.iCurrentHighestPriority[iTeam]
	
	IF iRule < FMMC_MAX_RULES
		IF IS_BIT_SET(g_FMMC_STRUCT.sFMMCEndConditions[iTeam].iRuleBitsetTwelve[iRule], ciBS_RULE12_ROAM_SPEC_DISABLE_DRONE)
			bDisabledByRule = TRUE
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetTwenty, ciOptionsBS20_Enable_Spectator_Drone_Reward)
	AND NOT bDisabledByRule
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MAINTAIN_SITTING_INTERUPT(SEATS_LOCAL_DATA& data, SEAT_SERVER_BROADCAST_DATA& serverBD)
	IF IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_RC_Car)
	OR IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Using_RC_CAR)
	OR IS_PLAYER_USING_RCBANDITO(PLAYER_ID())
		IF data.private.eState = SS_ANIM_SITTING
		AND IS_SCREEN_FADED_OUT()
			CLEANUP_SEATS_ACTIVITY(data, serverBD)
			
			SET_SEAT_STATE(data, SS_CLEANUP)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_LAUNCH_TURRET)
		PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SITTING_INTERUPT : Attempting to launch turret")
		
		TURRET_LOCK_RESPONSE eLockResponse
		
		eLockResponse = ARENA_SPECTATOR_TURRET_LOCK(data.private.eTurretRequestID)
		
		IF eLockResponse = TLR_SUCCESS
			PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SITTING_INTERUPT : Turrent launch success")
			
			IF data.private.iInSeatLocate >= 0
			AND data.private.iInSeatLocate < 10
				INT iSoundID
				PLAY_SOUND_FROM_ENTITY(iSoundID, "Select_Turret", NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), "DLC_AW_Spectator_Tablet_Sounds", TRUE)
			ENDIF
			
			SET_BIT(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Turret_Cam)
			
			CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_LAUNCH_TURRET)
		ELIF eLockResponse = TLR_FAILED
			PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SITTING_INTERUPT : Turrent launch fail")
			
			IF NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_ABILITY)
				SET_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_ABILITY)
			ENDIF
			
			PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_LAUNCH_TURRET)
		ENDIF
	ENDIF
	
	IF data.private.eState = SS_ANIM_SITTING
		IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
			CLEAR_SEATS_HELP_TEXT(data, TRUE)
		ENDIF
		
		IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BLOCK_INTERRUPT"))
			PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SITTING_INTERUPT : HAS_ANIM_EVENT_FIRED(BLOCK_INTERRUPT) : TRUE")
			
			CLEAR_SEATS_HELP_TEXT(data, TRUE)
			
			IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_PC_INPUT_MAP_LOADED)
				ARENA_SEATS_SHUTDOWN_PC_SCRIPTED_CONTROLS()
				
				CLEAR_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_LOADED_CONTROLS)
				
				CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_PC_INPUT_MAP_LOADED)
			ENDIF
		ELIF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_LAUNCH_TURRET)
		OR (NOT g_bMissionClientGameStateRunning #IF IS_DEBUG_BUILD AND NOT g_bDebugPlacedArenaBox #ENDIF )
		OR (NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Ready_For_Help_Text) AND NOT IS_SHOWING_MODE_SPECIFIC_HELP_TEXT())
		OR g_bUsingCCTV
		OR g_bRequestingLiveStream
		OR IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_CLEANING_UP)
		OR IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Drone_Cleanup)
		OR IS_BIT_SET(g_sDroneGlobals.iDroneBS, DRONE_GLOBAL_BIT_RETURN_DONE_TO_PLAYER)
		OR IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_LAUNCH_DRONE)
		OR IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Drone)
		OR IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_RC_Car)
		OR IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Trap_Cam)
		OR IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Turret_Cam)
		OR IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Using_Drone)
		OR IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Using_RC_CAR)
		OR IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Using_Trap_Cam)
		OR IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Using_Turret_Cam)
		OR IS_PLAYER_USING_RCBANDITO(PLAYER_ID())
		OR IS_LOCAL_PLAYER_SPAWNING_IN_ARENA_BOX_SEAT()
		OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CHARACTER_WHEEL)
		OR NOT IS_SCREEN_FADED_IN()
		OR IS_PLAYER_USING_ANY_TURRET(PLAYER_ID())
		OR g_iTurretScriptLaunchUid != TLRI_NONE
			CLEAR_SEATS_HELP_TEXT(data, TRUE)
			
			IF NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Ready_For_Help_Text)
				IF NOT IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_PC_INPUT_MAP_LOADED)
				AND g_iTurretScriptLaunchUid = TLRI_NONE
					ARENA_SEATS_INIT_PC_SCRIPTED_CONTROLS("ARENA SPECTATOR BOX TABLETS")
					
					SET_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_LOADED_CONTROLS)
					
					SET_BIT_ENUM(data.private.iBSGeneral, PSGB_PC_INPUT_MAP_LOADED)
				ENDIF
			ELSE
				IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_PC_INPUT_MAP_LOADED)
					ARENA_SEATS_SHUTDOWN_PC_SCRIPTED_CONTROLS()
					
					CLEAR_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_LOADED_CONTROLS)
					
					CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_PC_INPUT_MAP_LOADED)
				ENDIF
			ENDIF
		ELIF IS_EXIT_BLOCKED_THIS_FRAME(data)
			PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SITTING_INTERUPT : IS_EXIT_BLOCKED_THIS_FRAME() : TRUE")
			
			CLEAR_SEATS_HELP_TEXT(data, TRUE)
			
			IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_PC_INPUT_MAP_LOADED)
				ARENA_SEATS_SHUTDOWN_PC_SCRIPTED_CONTROLS()
				
				CLEAR_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_LOADED_CONTROLS)
				
				CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_PC_INPUT_MAP_LOADED)
			ENDIF
		ELSE
			IF NOT IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_PC_INPUT_MAP_LOADED)
				ARENA_SEATS_INIT_PC_SCRIPTED_CONTROLS("ARENA SPECTATOR BOX TABLETS")
				
				SET_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_LOADED_CONTROLS)
				
				SET_BIT_ENUM(data.private.iBSGeneral, PSGB_PC_INPUT_MAP_LOADED)
			ENDIF
			
			ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
			
			BOOL bLiveStreamView = FALSE
			
			IF IS_PLAYER_IN_ARENA_BOX_TABLET_SEAT()
				bLiveStreamView = TRUE
			ENDIF
			
			IF IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ARENA_VIP_CASH1", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST)
				CLEAR_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_LIVE_STREAM)
			ENDIF
			
			IF IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ARENA_VIP_CASH2", g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
				CLEAR_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_DRONE)
			ENDIF
			
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARENA_VIP_ABIL")
				CLEAR_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_ABILITY)
			ENDIF
			
			IF IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_DRONE)
			AND data.private.iContext = NEW_CONTEXT_INTENTION
				PRINT_HELP_WITH_NUMBER("ARENA_VIP_CASH2", g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST, 2000)
			ENDIF
			
			IF IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_LIVE_STREAM)
			AND data.private.iContext = NEW_CONTEXT_INTENTION
				PRINT_HELP_WITH_NUMBER("ARENA_VIP_CASH1", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST, 2000)
			ENDIF
			
			IF IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_ABILITY)
			AND data.private.iContext = NEW_CONTEXT_INTENTION
				PRINT_HELP("ARENA_VIP_ABIL", 2000)
			ENDIF
			
			IF IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_DRONE)
			OR IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_LIVE_STREAM)
			OR IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_ABILITY)
				CLEAR_SEATS_HELP_TEXT(data, TRUE)
			ENDIF
			
			IF NOT IS_PAUSE_MENU_ACTIVE()
			AND NOT g_bCelebrationScreenIsActive
			AND NOT IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BUSY_ON_PHONE)
			AND NOT IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_BUSY_BROWSER_OPEN)
			AND NOT IS_INTERACTION_MENU_OPEN()
			AND NOT IS_SELECTOR_UI_BUTTON_PRESSED()
			AND data.private.animationState.eClip <> ENTER
			AND NOT IS_WARNING_MESSAGE_ACTIVE()
			AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				IF bLiveStreamView
					IF ARE_TABLET_WEAPONS_ACTIVE()
					AND NOT ARE_TABLET_WEAPONS_BLOCKED()
						IF IS_THIS_HELP_MESSAGE_WITH_TWO_NUMBERS_BEING_DISPLAYED("ARENA_SEAT_PC1", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST, g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
						OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ARENA_SEAT_PC2", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST)
						OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ARENA_SEAT_PC3", g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
						OR IS_THIS_HELP_MESSAGE_WITH_TWO_NUMBERS_BEING_DISPLAYED("ARENA_SEAT1", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST, g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
						OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ARENA_SEAT2", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST)
						OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ARENA_SEAT3", g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
							CLEAR_SEATS_HELP_TEXT(data, TRUE)
						ENDIF
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARENA_SEAT_PC")
						OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARENA_SEAT")
							CLEAR_SEATS_HELP_TEXT(data, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_ARENA_SEAT_HELP_DISPLAYING()
				AND NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_DRONE)
				AND NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_LIVE_STREAM)
				AND NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_ABILITY)
				AND NOT IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ARENA_VIP_CASH1", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST)
				AND NOT IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ARENA_VIP_CASH2", g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
				AND NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARENA_VIP_ABIL")
					IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)						
						IF bLiveStreamView
							IF ARE_TABLET_WEAPONS_ACTIVE()
							AND NOT ARE_TABLET_WEAPONS_BLOCKED()
								IF IS_THIS_HELP_MESSAGE_WITH_TWO_NUMBERS_BEING_DISPLAYED("ARENA_SEAT_PC1", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST, g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
								OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ARENA_SEAT_PC2", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST)
								OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ARENA_SEAT_PC3", g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
									CLEAR_SEATS_HELP_TEXT(data, TRUE)
								ENDIF
								
								REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "ARENA_SEAT_PC")
							ELSE
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARENA_SEAT_PC")
									CLEAR_SEATS_HELP_TEXT(data, TRUE)
								ENDIF
								
								IF NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_PAID_DRONE)
								AND NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_PAID_LIVE_STREAM)
									PRINT_HELP_WITH_TWO_NUMBERS("ARENA_SEAT_PC1", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST, g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
								ELIF IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_PAID_DRONE)
								AND NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_PAID_LIVE_STREAM)
									PRINT_HELP_WITH_NUMBER("ARENA_SEAT_PC2", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST)
								ELIF NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_PAID_DRONE)
								AND IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_PAID_LIVE_STREAM)
									PRINT_HELP_WITH_NUMBER("ARENA_SEAT_PC3", g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
								ELSE
									REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "ARENA_SEAT_PC4")
								ENDIF
							ENDIF
						ELSE
							REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "ARENA_SEAT_EX")
						ENDIF
					ELSE
						IF bLiveStreamView
							IF ARE_TABLET_WEAPONS_ACTIVE()
							AND NOT ARE_TABLET_WEAPONS_BLOCKED()
								IF IS_THIS_HELP_MESSAGE_WITH_TWO_NUMBERS_BEING_DISPLAYED("ARENA_SEAT1", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST, g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
								OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ARENA_SEAT2", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST)
								OR IS_THIS_HELP_MESSAGE_WITH_NUMBER_BEING_DISPLAYED("ARENA_SEAT3", g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
									CLEAR_SEATS_HELP_TEXT(data, TRUE)
								ENDIF
								
								REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "ARENA_SEAT")
							ELSE
								IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("ARENA_SEAT")
									CLEAR_SEATS_HELP_TEXT(data, TRUE)
								ENDIF
								
								IF NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_PAID_DRONE)
								AND NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_PAID_LIVE_STREAM)
									PRINT_HELP_WITH_TWO_NUMBERS("ARENA_SEAT1", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST, g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
								ELIF IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_PAID_DRONE)
								AND NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_PAID_LIVE_STREAM)
									PRINT_HELP_WITH_NUMBER("ARENA_SEAT2", g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST)
								ELIF NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_PAID_DRONE)
								AND IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_PAID_LIVE_STREAM)
									PRINT_HELP_WITH_NUMBER("ARENA_SEAT3", g_sMPTunables.iAW_TABLET_SPECTATOR_DRONE_COST)
								ELSE
									REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "ARENA_SEAT4")
								ENDIF
							ENDIF
						ELSE
							REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "ARENA_SEAT_EX")
						ENDIF
					ENDIF
				ENDIF
				
				IF data.private.eState = SS_ANIM_SITTING

					IF ARE_TABLET_WEAPONS_ACTIVE()
					AND NOT ARE_TABLET_WEAPONS_BLOCKED()
						IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Drone)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_RC_Car)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Trap_Cam)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Turret_Cam)
						AND NOT IS_PLAYER_USING_RCBANDITO(PLAYER_ID())
							PRINTLN("[ARENA_BOX_SEATS] Player wants to leave seat")
							
							IF data.private.iInSeatLocate >= 0
							AND data.private.iInSeatLocate < 10
								INT iSoundID
								PLAY_SOUND_FROM_ENTITY(iSoundID, "Exit_Menu", NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), "DLC_AW_Spectator_Tablet_Sounds", TRUE)
							ENDIF
							
							LEAVE_SEAT(data)
						ENDIF
						
						IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
						AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Drone)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_RC_Car)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Trap_Cam)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Turret_Cam)
						AND NOT IS_PLAYER_USING_RCBANDITO(PLAYER_ID())
						AND bLiveStreamView
							IF IS_TABLET_DRONE_ENABLED()
								CLEAR_SEATS_HELP_TEXT(data, TRUE)
								
								SET_BIT(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Drone)
								
								IF data.private.iInSeatLocate >= 0
								AND data.private.iInSeatLocate < 10
									INT iSoundID
									PLAY_SOUND_FROM_ENTITY(iSoundID, "Select_Drone", NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), "DLC_AW_Spectator_Tablet_Sounds", TRUE)
								ENDIF
							ELSE
								IF NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_ABILITY)
									SET_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_ABILITY)
								ENDIF
								
								PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							ENDIF
						ENDIF
						
						IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
						AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Drone)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_RC_Car)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Trap_Cam)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Turret_Cam)
						AND NOT IS_PLAYER_USING_RCBANDITO(PLAYER_ID())
						AND bLiveStreamView
							IF IS_TABLET_RC_CAR_ENABLED()
								CLEAR_SEATS_HELP_TEXT(data, TRUE)
								
								SET_BIT(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_RC_Car)
								
								IF data.private.iInSeatLocate >= 0
								AND data.private.iInSeatLocate < 10
									INT iSoundID
									PLAY_SOUND_FROM_ENTITY(iSoundID, "Select_RC_Car", NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), "DLC_AW_Spectator_Tablet_Sounds", TRUE)
								ENDIF
							ELSE
								IF NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_ABILITY)
									SET_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_ABILITY)
								ENDIF
								
								PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							ENDIF
						ENDIF
						
						IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
						AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Drone)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_RC_Car)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Trap_Cam)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Turret_Cam)
						AND NOT IS_PLAYER_USING_RCBANDITO(PLAYER_ID())
						AND bLiveStreamView
							IF IS_TABLET_TRAP_CAM_ENABLED()
								CLEAR_SEATS_HELP_TEXT(data, TRUE)
								
								SET_BIT(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Trap_Cam)
								
								IF data.private.iInSeatLocate >= 0
								AND data.private.iInSeatLocate < 10
									INT iSoundID
									PLAY_SOUND_FROM_ENTITY(iSoundID, "Select_Trap_Cam", NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), "DLC_AW_Spectator_Tablet_Sounds", TRUE)
								ENDIF
							ELSE
								IF NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_ABILITY)
									SET_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_ABILITY)
								ENDIF
								
								PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							ENDIF
						ENDIF
						
						IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RUP)
						AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Drone)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_RC_Car)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Trap_Cam)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_Turret_Cam)
						AND NOT IS_PLAYER_USING_RCBANDITO(PLAYER_ID())
						AND bLiveStreamView
							IF IS_TABLET_TURRET_CAM_ENABLED()
								CLEAR_SEATS_HELP_TEXT(data, TRUE)
								
								IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_PC_INPUT_MAP_LOADED)
									ARENA_SEATS_SHUTDOWN_PC_SCRIPTED_CONTROLS()
									
									CLEAR_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_LOADED_CONTROLS)
									
									CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_PC_INPUT_MAP_LOADED)
								ENDIF
								
								SET_BIT_ENUM(data.private.iBSGeneral, PSGB_LAUNCH_TURRET)
							ELSE
								IF NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_ABILITY)
									SET_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_ABILITY)
								ENDIF
								
								PLAY_SOUND_FRONTEND(-1, "ERROR", "HUD_FRONTEND_DEFAULT_SOUNDSET")
							ENDIF
						ENDIF
					ELSE
						IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
						AND NOT g_bRequestingLiveStream
						AND NOT g_bUsingCCTV
							PRINTLN("[ARENA_BOX_SEATS] Player wants to leave seat")
							
							IF data.private.iInSeatLocate >= 0
							AND data.private.iInSeatLocate < 10
								INT iSoundID
								PLAY_SOUND_FROM_ENTITY(iSoundID, "Exit_Menu", NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), "DLC_AW_Spectator_Tablet_Sounds", TRUE)
							ENDIF
							
							LEAVE_SEAT(data)
						ENDIF
						
						IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RDOWN)
						AND NOT IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
						AND NOT IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
						AND bLiveStreamView
						AND NOT g_sMPTunables.bAW_TABLET_DISABLE_SPECTATOR_CAMERA
							IF NETWORK_CAN_SPEND_MONEY(g_sMPTunables.iAW_TABLET_SPECTATOR_CAM_COST, FALSE, FALSE, TRUE)
							OR IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_PAID_LIVE_STREAM)
								CLEAR_SEATS_HELP_TEXT(data, TRUE)
								
								g_bRequestingLiveStream = TRUE
								g_bUsingCCTV = TRUE
								
								IF data.private.iInSeatLocate >= 0
								AND data.private.iInSeatLocate < 10
									INT iSoundID
									PLAY_SOUND_FROM_ENTITY(iSoundID, "Select_Live_Stream", NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), "DLC_AW_Spectator_Tablet_Sounds", TRUE)
								ENDIF
								
								INCREMENT_MP_INT_CHARACTER_AWARD(MP_AWARD_THROUGH_A_LENS)
								SET_LOCAL_PLAYER_ARENA_GARAGE_TROPHY_LENS_DATA()
								
								INCREMENT_ARENA_SPECTATOR_CAMS_USED()
							ELSE
								IF NOT IS_BIT_SET(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_LIVE_STREAM)
									SET_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_WARN_LIVE_STREAM)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_BLOCK_EXIT_THIS_FRAME)
ENDPROC

FUNC BOOL DO_ADDITIONAL_PROMPT_CHECK(SEATS_LOCAL_DATA& data)
	RETURN NOT IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_ADDITIONAL_CHECK_BEFORE_PROMPT)
	OR CALL data.private.cCanPlayerUseSeat(data.public.seats[data.private.iInSeatLocate], data.private.iInSeatLocate)
ENDFUNC

PROC MAINTAIN_SEATS_ACTIVITY(SEATS_LOCAL_DATA& data, SEAT_SERVER_BROADCAST_DATA& serverBD)
	UPDATE_ENTITY_CHECKS(data)
	RUN_IGNORE_COLLISION_FOR_SIMPLE_POP_LOGIC(data)
	MAINTAIN_SUPRESS_PHONE_AND_ACTIONS(data)
	
	TEXT_LABEL_63 sAnimDict
	TEXT_LABEL_15 sAnimClip
	
	INT localIdleScene
	
	SWITCH data.private.eState
		CASE SS_INIT
			CLEAR_BIT_ENUM(data.private.iBSGeneraL, PSGB_ENTRY_COMPLETE)
			
			SET_SEAT_STATE(data,SS_LOCATE_CHECK)
		BREAK
		
		CASE SS_LOCATE_CHECK
			IF STAGGERED_UPDATE_LOCATE_CHECK(data)
				data.private.iSeatBlockedTimer = GET_GAME_TIMER()
				SET_SEAT_STATE(data, SS_PROMPT)
			ELIF IS_LOCAL_PLAYER_SPAWNING_IN_ARENA_BOX_SEAT()
				data.private.iSeatBlockedTimer = GET_GAME_TIMER()
				SET_SEAT_STATE(data, SS_PROMPT)
			ENDIF
		BREAK
		
		CASE SS_PROMPT
			IF IS_ENTITY_IN_SEAT_ENTRY_LOCATE(PLAYER_PED_ID(), data.public.seats[data.private.iInSeatLocate])
			AND IS_ENTITY_FACING_SEAT(PLAYER_PED_ID(), data.public.seats[data.private.iInSeatLocate].animation.vRot)
			AND NOT IS_LOCAL_PLAYER_SPAWNING_IN_ARENA_BOX_SEAT()
				IF IS_PLAYER_BUSY(data)
				OR g_bCelebrationScreenIsActive
				OR ARE_THERE_TOO_MANY_PLAYERS_CLOSE_TO_SIMPLE_ACTIVITY_ANGLED_AREA(data)
				OR NOT DO_ADDITIONAL_PROMPT_CHECK(data)
				OR ((data.private.iInSeatLocate < 10) AND (serverBD.iPlayerSeats[data.private.iInSeatLocate] != -1)
				AND (serverBD.iPlayerSeats[data.private.iInSeatLocate] != NATIVE_TO_INT(PLAYER_ID())))
				OR ((data.private.iInSeatLocate <= 7) AND ARE_TABLET_WEAPONS_ACTIVE() AND ARE_TABLET_WEAPONS_BLOCKED())
				OR ((data.private.iInSeatLocate < 10) AND (g_sMPTunables.bAW_DISABLE_SPECTATOR_TABLET))
					PRINTLN("[ARENA_BOX_SEATS]IS_PLAYER_BUSY() : ", IS_PLAYER_BUSY(data))
					PRINTLN("[ARENA_BOX_SEATS]ARE_THERE_TOO_MANY_PLAYERS_CLOSE_TO_SIMPLE_ACTIVITY_ANGLED_AREA(data) : ", ARE_THERE_TOO_MANY_PLAYERS_CLOSE_TO_SIMPLE_ACTIVITY_ANGLED_AREA(data))
					PRINTLN("[ARENA_BOX_SEATS]NOT DO_ADDITIONAL_PROMPT_CHECK(data) : ", NOT DO_ADDITIONAL_PROMPT_CHECK(data))
					
					IF NOT g_bCelebrationScreenIsActive
					AND ((data.private.iInSeatLocate < 10) AND (serverBD.iPlayerSeats[data.private.iInSeatLocate] != -1)
					AND (serverBD.iPlayerSeats[data.private.iInSeatLocate] != NATIVE_TO_INT(PLAYER_ID())))
						PED_INDEX pedTemp
						
						pedTemp = GET_PLAYER_PED(INT_TO_NATIVE(PLAYER_INDEX, serverBD.iPlayerSeats[data.private.iInSeatLocate]))
						
						IF DOES_ENTITY_EXIST(pedTemp)
						AND NOT IS_ENTITY_IN_ANGLED_AREA(pedTemp, <<2797.987793,-3954.898438,181.000488>>, <<2797.949219,-3930.850342,187.411407>>, 30.0)
							IF data.private.iContext = NEW_CONTEXT_INTENTION
								REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "ARENA_SEAT_FULL", DEFAULT, GET_PLAYER_NAME(INT_TO_NATIVE(PLAYER_INDEX, serverBD.iPlayerSeats[data.private.iInSeatLocate])), DEFAULT, TRUE)
							ENDIF
						ELSE
							CLEAR_SEATS_HELP_TEXT(data, TRUE)
							
							SET_SEAT_STATE(data, SS_LOCATE_CHECK)
						ENDIF
					ELSE
						IF (data.private.iInSeatLocate <= 7)
						AND ARE_TABLET_WEAPONS_ACTIVE()
						AND ARE_TABLET_WEAPONS_BLOCKED()
							IF data.private.iContext = NEW_CONTEXT_INTENTION
								REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "ARENA_SEAT_NO")
							ENDIF
						ELSE
							CLEAR_SEATS_HELP_TEXT(data, TRUE)
							
							SET_SEAT_STATE(data, SS_LOCATE_CHECK)
						ENDIF
					ENDIF
				ELSE
					IF data.private.iContext = NEW_CONTEXT_INTENTION
						IF GET_GAME_TIMER() - data.private.iSeatBlockedTimer > SEAT_BLOCK_TIMER
							IF data.private.iInSeatLocate <= 9
								REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "AR_SEAT_PRMPT")
							ELSE
								REGISTER_CONTEXT_INTENTION(data.private.iContext, CP_HIGH_PRIORITY, "MPJAC_SIT")
							ENDIF
						ENDIF
					ELIF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
						RELEASE_CONTEXT_INTENTION(data.private.iContext)
						
						SET_START_ANIMATION(data)
						
						IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
							NET_SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, NSPC_LEAVE_CAMERA_CONTROL_ON)
							
							SET_SEAT_STATE(data, SS_REQUEST_SEAT)
						ELSE
							SET_SEAT_STATE(data, SS_REQUEST_ANIMATION)
						ENDIF
					ENDIF
				ENDIF
			ELIF IS_LOCAL_PLAYER_SPAWNING_IN_ARENA_BOX_SEAT()
				RELEASE_CONTEXT_INTENTION(data.private.iContext)
				
				SET_SEAT_STATE(data, SS_REQUEST_SEAT)
			ELSE
				CLEAR_SEATS_HELP_TEXT(data, TRUE)
				SET_SEAT_STATE(data, SS_LOCATE_CHECK)
			ENDIF
		BREAK
		
		CASE SS_REQUEST_SEAT
			IF GET_LOCAL_PLAYER_USING_OFFICE_SEATID() >= 0
				IF IS_LOCAL_PLAYER_SPAWNING_IN_ARENA_BOX_SEAT()
					data.private.iInSeatLocate = GET_LOCAL_PLAYER_USING_OFFICE_SEATID()
					
					IF IS_ENTITY_VISIBLE(PLAYER_PED_ID())
						NETWORK_FADE_OUT_ENTITY(PLAYER_PED_ID(), FALSE, TRUE)
					ENDIF
					
					SET_BIT_ENUM(data.private.iBSGeneral, PSGB_SPAWNING_IN_SEAT)
				ENDIF
				
				SET_START_ANIMATION(data)
				
				SET_SEAT_STATE(data, SS_REQUEST_ANIMATION)
			ENDIF
			
			IF NOT IS_LOCAL_PLAYER_SPAWNING_IN_ARENA_BOX_SEAT()
				IF data.private.iInSeatLocate < 10
				AND serverBD.iPlayerSeats[data.private.iInSeatLocate] != -1
				AND serverBD.iPlayerSeats[data.private.iInSeatLocate] != NATIVE_TO_INT(PLAYER_ID())
					CLEAR_SEATS_HELP_TEXT(data, TRUE)
					
					IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					ENDIF
					
					ENABLE_INTERACTION_MENU()
					
					SET_SEAT_STATE(data, SS_LOCATE_CHECK)
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_REQUEST_ANIMATION
			CONSTRUCT_ANIM_DICT(data, sAnimDict)
			
			IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
				IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
					EXIT
				ENDIF
				
				IF NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
				ENDIF
			ENDIF
			
			REQUEST_ANIM_DICT(sAnimDict)
			
			IF HAS_ANIM_DICT_LOADED(sAnimDict)
				DISABLE_INTERACTION_MENU()
				
				SET_SEAT_STATE(data, SS_MOVE_TO_ANIM_START)
			ENDIF
		BREAK
		
		CASE SS_MOVE_TO_ANIM_START
			CONSTRUCT_ANIM_DICT(data, sAnimDict)
			CONSTRUCT_ANIM_CLIP(data, sAnimClip)
			
			VECTOR vTriggerPos
			vTriggerPos = GET_ANIM_INITIAL_OFFSET_POSITION(sAnimDict, sAnimClip, GET_ANIM_POS(data), GET_ANIM_ROT(data))
			
			VECTOR vTriggerRotation
			vTriggerRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDict, sAnimClip, GET_ANIM_POS(data), GET_ANIM_ROT(data))
			
			IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_SPAWNING_IN_SEAT)
				BOOL bQuickWarp
				
				bQuickWarp = TRUE
				
				IF GET_LOCAL_PLAYER_USING_ARENA_BOX_SEATID() >= 0
					bQuickWarp = FALSE
				ENDIF
				
				IF NET_WARP_TO_COORD(vTriggerPos, vTriggerRotation.z, DEFAULT, FALSE, DEFAULT, DEFAULT, DEFAULT, bQuickWarp)
				AND g_bArenaBoxActivitiesLoaded
					SET_SEAT_STATE(data, SS_ANIM_START_ENTER)
				ELSE
					IF NOT g_bArenaBoxActivitiesLoaded
						PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SEATS_ACTIVITY - g_bArenaBoxActivitiesLoaded - FALSE 1")
					ENDIF
				ENDIF
			ELIF g_bArenaBoxActivitiesLoaded
				IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
					AND NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
						NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
					ENDIF
				ENDIF
				
				TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), vTriggerPos, PEDMOVEBLENDRATIO_WALK, 500, vTriggerRotation.Z, 0.05)
				
				SET_PLAYER_USING_OFFICE_SEATID(data.private.iInSeatLocate)
				
				PRINTLN("[ARENA_BOX_SEATS] WALKING TO COORDS")
				
				SET_SEAT_STATE(data, SS_ANIM_START_ENTER)
			ELSE
				IF NOT g_bArenaBoxActivitiesLoaded
					PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SEATS_ACTIVITY - g_bArenaBoxActivitiesLoaded - FALSE 2")
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_ANIM_START_ENTER
			IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
				AND NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
				ENDIF
			ENDIF
			
			PRINTLN("[OCH] WALKING TO COORDS")
			
			SCRIPTTASKSTATUS eTaskStatus
			
			CONSTRUCT_ANIM_DICT(data, sAnimDict)
			CONSTRUCT_ANIM_CLIP(data, sAnimClip)
			
			eTaskStatus = GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
			
			VECTOR vTempRotation2
			vTempRotation2 = GET_ANIM_INITIAL_OFFSET_ROTATION(sAnimDict, sAnimClip, GET_ANIM_POS(data), GET_ANIM_ROT(data))
			
			Float fTriggerHead2
			fTriggerHead2 = vTempRotation2.Z
			
			IF (eTaskStatus <> PERFORMING_TASK
			AND eTaskStatus <> WAITING_TO_START_TASK)
			OR IS_HEADING_ACCEPTABLE_CORRECTED(GET_ENTITY_HEADING(PLAYER_PED_ID()), fTriggerHead2, 5.0)
			OR IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_SPAWNING_IN_SEAT)
				FLOAT fStartRate
				fStartRate = 1.0
				
				FLOAT fStartPhase
				fStartPhase = 0.0
				
				VECTOR vPos
				vPos = GET_ANIM_POS(data)
				
				IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_SPAWNING_IN_SEAT)
					fStartRate = 2.0
					fStartPhase = 0.9
				ENDIF
				
				data.private.iNetSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(vPos, GET_ANIM_ROT(data), DEFAULT, TRUE, DEFAULT, DEFAULT, fStartPhase, fStartRate)
				
				NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), data.private.iNetSceneID, sAnimDict, sAnimClip, REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE, RBF_PLAYER_IMPACT)
				
				NETWORK_START_SYNCHRONISED_SCENE(data.private.iNetSceneID)
				
				//Need to set reset the animtion variation timer.
				data.private.animationState.iVarChangeTime = GET_GAME_TIMER()
				
				//Need to make sure we don't head straight to SET_SEAT_STATE(data, SS_CLEANUP) with any delays before next anim
				SET_BIT_ENUM(data.private.iBSGeneral, PSGB_WAITING_FOR_NEXT_DICTIONARY)
				
				// Temp for flow play
				SET_SEAT_STATE(data, SS_ANIM_SITTING)
			ENDIF
		BREAK
		
		CASE SS_ANIM_SITTING
			IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
				AND NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
				ENDIF
			ENDIF
			
			IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_GET_OUT_OF_SEAT)
				PRINTLN("[ARENA_BOX_SEATS] IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_GET_OUT_OF_SEAT) : External request for player to leave seat.")
				
				CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_GET_OUT_OF_SEAT)
				
				IF data.private.iInSeatLocate >= 0
				AND data.private.iInSeatLocate < 10
					INT iSoundID
					PLAY_SOUND_FROM_ENTITY(iSoundID, "Exit_Menu", NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), "DLC_AW_Spectator_Tablet_Sounds", TRUE)
				ENDIF
				
				LEAVE_SEAT(data)
			ENDIF
			
			MAINTAIN_SITTING_INTERUPT(data, serverBD)
			
			localIdleScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(data.private.iNetSceneID)
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(localIdleScene)
				IF GET_SYNCHRONIZED_SCENE_PHASE(localIdleScene) >= 1 // Has the previous finished yet?
					IF NOT g_bPlayerFullySeated
						IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
						AND NOT NETWORK_IS_ENTITY_FADING(PLAYER_PED_ID())
							IF g_bMissionClientGameStateRunning
								NETWORK_FADE_IN_ENTITY(PLAYER_PED_ID(), TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_PLAYER_IN_ARENA_BOX_SEAT()
						IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
							IF data.private.iInSeatLocate >= 0
							AND data.private.iInSeatLocate < 10
								INT iSoundID
								PLAY_SOUND_FROM_ENTITY(iSoundID, "Enter_Menu", NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), "DLC_AW_Spectator_Tablet_Sounds", TRUE)
							ENDIF
							
							SET_PLAYER_USING_ARENA_BOX_SEATID(data.private.iInSeatLocate)
						ELSE
							SET_PLAYER_USING_ARENA_BOX_SEATID(data.private.iInSeatLocate + 20)
						ENDIF
						
						g_bPlayerFullySeated = TRUE
					ELSE
						g_bPlayerFullySeated = TRUE
					ENDIF
					
					GENERATE_AND_STORE_NEXT_ANIM(data)
					
					CONSTRUCT_ANIM_DICT(data, sAnimDict)
					CONSTRUCT_ANIM_CLIP(data, sAnimClip)
					
					data.private.iNetSceneID = NETWORK_CREATE_SYNCHRONISED_SCENE(GET_ANIM_POS(data), GET_ANIM_ROT(data), DEFAULT, TRUE)
					
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), data.private.iNetSceneID, sAnimDict, sAnimClip, SLOW_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE, RBF_PLAYER_IMPACT)
					
					NETWORK_START_SYNCHRONISED_SCENE(data.private.iNetSceneID)
					
					//Need to make sure we don't head straight to SET_SEAT_STATE(data, SS_CLEANUP) with any delays before next anim
					SET_BIT_ENUM(data.private.iBSGeneral, PSGB_WAITING_FOR_NEXT_DICTIONARY)
					SET_BIT_ENUM(data.private.iBSGeneraL, PSGB_ENTRY_COMPLETE)
				ELSE
					IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
					AND ((HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("attachTablet"))
					OR GET_SYNCHRONIZED_SCENE_PHASE(localIdleScene) > 0.25))
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
						AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
						AND NOT IS_ENTITY_DEAD(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]))
							IF NOT IS_ENTITY_ATTACHED(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]))
								PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SEATS_ACTIVITY : Attaching tablet.")
								ATTACH_ENTITY_TO_ENTITY(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
							ENDIF
						ENDIF
					ENDIF
					
					CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_WAITING_FOR_NEXT_DICTIONARY)
				ENDIF
			ELSE
				//@@Maybe there should be a timeout timer on this.
				IF NOT IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_WAITING_FOR_NEXT_DICTIONARY)
				AND data.private.eState = SS_ANIM_SITTING
					PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SEATS_ACTIVITY : Not waiting for dictionary and no scene running.")
					
					IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
					AND data.private.iInSeatLocate >= 0
						PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SEATS_ACTIVITY : In tablet seat.")
						
						IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
						AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
							PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SEATS_ACTIVITY : Tablet exists.")
							
							IF IS_ENTITY_ATTACHED(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]))
								PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SEATS_ACTIVITY : Detaching tablet.")
								
								DETACH_ENTITY(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), FALSE)
							ENDIF
							
							DELETE_NET_ID(serverBD.niTablet[data.private.iInSeatLocate])
						ENDIF
					ENDIF
					
					SET_SEAT_STATE(data, SS_CLEANUP)
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_ANIM_INTERUPT
			IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
				AND NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
				ENDIF
			ENDIF
			
			CDEBUG1LN(DEBUG_SAFEHOUSE, "[ARENA_BOX_SEATS] MAINTAIN_SEATS_ACTIVITY : NETWORK_STOP_SYNCHRONISED_SCENE")
			
			NETWORK_STOP_SYNCHRONISED_SCENE(data.private.iNetSceneID)
			
			IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
			AND data.private.iInSeatLocate >= 0
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
				AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
					IF IS_ENTITY_ATTACHED(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]))
						DETACH_ENTITY(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), FALSE)
					ENDIF
					
					VECTOR vTabletPosition, vTabletRotation
					vTabletPosition = GET_ANIM_INITIAL_OFFSET_POSITION(GET_TABLET_CREATION_DICT(), "enter_tablet", data.public.seats[data.private.iInSeatLocate].animation.vPos, data.public.seats[data.private.iInSeatLocate].animation.vRot)
					vTabletRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(GET_TABLET_CREATION_DICT(), "enter_tablet", data.public.seats[data.private.iInSeatLocate].animation.vPos, data.public.seats[data.private.iInSeatLocate].animation.vRot)
					
					SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), vTabletPosition)
					SET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), vTabletRotation)
					
					FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), TRUE)
				ENDIF
			ENDIF
			
			SET_SEAT_STATE(data, SS_CLEANUP)
		BREAK
		
		CASE SS_CLEANUP
			IF IS_BIT_SET_ENUM(data.private.iBSGeneral, PSGB_PC_INPUT_MAP_LOADED)
				ARENA_SEATS_SHUTDOWN_PC_SCRIPTED_CONTROLS()
				
				CLEAR_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_LOADED_CONTROLS)
				
				CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_PC_INPUT_MAP_LOADED)
			ENDIF
			
			IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
				AND NOT NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
				ENDIF
			ENDIF
			
			INT localScene
			localScene	= NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(data.private.iNetSceneID)
			
			SCRIPTTASKSTATUS eExitSceneStatus
			eExitSceneStatus = GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_SYNCHRONIZED_SCENE)
			
			IF eExitSceneStatus = WAITING_TO_START_TASK
			OR eExitSceneStatus = PERFORMING_TASK
				IF IS_SYNCHRONIZED_SCENE_RUNNING(localScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(localScene) >= 0.7
					OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BREAKOUT_FINISH"))
					OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BREAK_OUT"))
						#IF IS_DEBUG_BUILD
						IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BREAKOUT_FINISH"))
							PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SEATS_ACTIVITY: Finishing anim early as BREAKOUT_FINISH anim event fired")
						ENDIF
						
						IF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("BREAK_OUT"))
							PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SEATS_ACTIVITY: Finishing anim early as BREAK_OUT anim event fired")
						ENDIF
						#ENDIF
						
						PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SEATS_ACTIVITY: NETWORK_STOP_SYNCHRONISED_SCENE")
						
						NETWORK_STOP_SYNCHRONISED_SCENE(data.private.iNetSceneID)
						
						IF NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_RC_Car)
						AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Using_RC_CAR)
						AND NOT IS_PLAYER_USING_RCBANDITO(PLAYER_ID())
							SET_PLAYER_USING_OFFICE_SEATID(-1)
							SET_PLAYER_USING_ARENA_BOX_SEATID(-1)
						ENDIF
						
						g_bPlayerFullySeated = FALSE
						
						CLEAR_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_STANDING_UP)
						
						CLEAR_SEATS_HELP_TEXT(data, TRUE)
						
						CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_SPAWNING_IN_SEAT)
						
						IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
						AND data.private.iInSeatLocate >= 0
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
							AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
								IF IS_ENTITY_ATTACHED(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]))
									DETACH_ENTITY(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), FALSE)
								ENDIF
								
								VECTOR vTabletPosition, vTabletRotation
								vTabletPosition = GET_ANIM_INITIAL_OFFSET_POSITION(GET_TABLET_CREATION_DICT(), "enter_tablet", data.public.seats[data.private.iInSeatLocate].animation.vPos, data.public.seats[data.private.iInSeatLocate].animation.vRot)
								vTabletRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(GET_TABLET_CREATION_DICT(), "enter_tablet", data.public.seats[data.private.iInSeatLocate].animation.vPos, data.public.seats[data.private.iInSeatLocate].animation.vRot)
								
								SET_ENTITY_COORDS_NO_OFFSET(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), vTabletPosition)
								SET_ENTITY_ROTATION(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), vTabletRotation)
								
								FREEZE_ENTITY_POSITION(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), TRUE)
							ENDIF
						ENDIF
						
						IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
							IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
								NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							ENDIF
						ENDIF
						
						ENABLE_INTERACTION_MENU()
						
						SET_SEAT_STATE(data, SS_INIT)
					ELIF HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), HASH("detachTablet"))
						IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
							IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
							AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
								IF IS_ENTITY_ATTACHED(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]))
									DETACH_ENTITY(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SEATS_ACTIVITY: Sync scene task status: ", ENUM_TO_INT(eExitSceneStatus))
				PRINTLN("[ARENA_BOX_SEATS] MAINTAIN_SEATS_ACTIVITY: Scene isn't running, cleanup anyway")
				
				IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
				AND data.private.iInSeatLocate >= 0
					IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
					AND NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.niTablet[data.private.iInSeatLocate])
						IF IS_ENTITY_ATTACHED(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]))
							DETACH_ENTITY(NET_TO_OBJ(serverBD.niTablet[data.private.iInSeatLocate]), FALSE)
						ENDIF
						
						DELETE_NET_ID(serverBD.niTablet[data.private.iInSeatLocate])
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Request_Use_RC_Car)
				AND NOT IS_BIT_SET(g_iSpecialSpectatorBS, g_ciSpecial_Spectator_BS_Using_RC_CAR)
				AND NOT IS_PLAYER_USING_RCBANDITO(PLAYER_ID())
					SET_PLAYER_USING_OFFICE_SEATID(-1)
					SET_PLAYER_USING_ARENA_BOX_SEATID(-1)
				ENDIF
				
				g_bPlayerFullySeated = FALSE
				
				CLEAR_BIT(g_iArenaSpectatorBS, GLOBAL_ARENA_SPECTATOR_BOX_BS_STANDING_UP)
				
				CLEAR_SEATS_HELP_TEXT(data, TRUE)
				
				CLEAR_BIT_ENUM(data.private.iBSGeneral, PSGB_SPAWNING_IN_SEAT)
				
				IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
					IF data.private.animationState.eDictBase = ANIM_AMB_ARENA_SPECTATOR_TABLET_SEATING
						NET_SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					ENDIF
				ENDIF
				
				ENABLE_INTERACTION_MENU()
				
				SET_SEAT_STATE(data, SS_INIT)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC MAINTAIN_SEAT_OBJECTS(SEATS_LOCAL_DATA& data, SEAT_SERVER_BROADCAST_DATA& serverBD)
	REQUEST_ANIM_DICT(GET_TABLET_CREATION_DICT())
	
	IF NOT HAS_ANIM_DICT_LOADED(GET_TABLET_CREATION_DICT())
		EXIT
	ENDIF
	
	IF serverBD.bSeatsInitialised
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.niTablet[data.private.iObjectStagger])
			MODEL_NAMES eTabletModel = INT_TO_ENUM(MODEL_NAMES, HASH("xs_prop_arena_tablet_drone_01"))
			
			REQUEST_MODEL(eTabletModel)
			
			IF NOT HAS_MODEL_LOADED(eTabletModel)
				EXIT
			ENDIF
			
			OBJECT_INDEX objTempTablet
			
			IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1, FALSE, TRUE)
				IF NOT IS_BIT_SET(serverBD.iBS, data.private.iObjectStagger)
					RESERVE_LOCAL_NETWORK_MISSION_OBJECTS(GET_NUM_RESERVED_MISSION_OBJECTS(FALSE, RESERVATION_LOCAL_ONLY) + 1)
					
					SET_BIT(serverBD.iBS, data.private.iObjectStagger)
				ELSE
					IF CAN_REGISTER_MISSION_OBJECTS(1)
						VECTOR vTabletPosition, vTabletRotation
						vTabletPosition = GET_ANIM_INITIAL_OFFSET_POSITION(GET_TABLET_CREATION_DICT(), "enter_tablet", data.public.seats[data.private.iObjectStagger].animation.vPos, data.public.seats[data.private.iObjectStagger].animation.vRot)
						vTabletRotation = GET_ANIM_INITIAL_OFFSET_ROTATION(GET_TABLET_CREATION_DICT(), "enter_tablet", data.public.seats[data.private.iObjectStagger].animation.vPos, data.public.seats[data.private.iObjectStagger].animation.vRot)
						
						objTempTablet = CREATE_OBJECT(eTabletModel, vTabletPosition)
						
						SET_MODEL_AS_NO_LONGER_NEEDED(eTabletModel)
						SET_ENTITY_CAN_BE_DAMAGED(objTempTablet, FALSE)
						SET_ENTITY_COORDS_NO_OFFSET(objTempTablet, vTabletPosition)
						SET_ENTITY_ROTATION(objTempTablet, vTabletRotation)
						FREEZE_ENTITY_POSITION(objTempTablet, TRUE)
						
						serverBD.niTablet[data.private.iObjectStagger] = OBJ_TO_NET(objTempTablet)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		data.private.iObjectStagger++
		
		IF data.private.iObjectStagger >= 10
			data.private.iObjectStagger = 0
		ENDIF
	ENDIF
ENDPROC
